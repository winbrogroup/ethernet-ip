unit PcommForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs;

type
  TPmacGetResponse         = function (DN : Dword; rx : pChar; max : word; cmd : pChar) : integer;stdcall;
  TPmacDevice              = function (DN : Dword): Boolean; stdcall;
  TPmacFlush               = procedure(DN : Dword); stdcall;

  TPcommForm = class(TForm)
    procedure FormCreate(Sender: TObject);
  private
    FDeviceNumber   : Integer;
    LibCode : THandle;
    OpenPMACDevice    : TPmacDevice;
    ClosePMACDevice   : TPmacDevice;
    PmacFlush         : TPmacFlush;
    GetResponse       : TPmacGetResponse;
    function GetProcAddressX(hModule: HMODULE; lpProcName: LPCSTR) : FARPROC; stdcall;
    function Converse(cmd : string) : string;
  public
    { Public declarations }
  end;

var
  Form6: TPcommForm;

const
  TurboPCommLibrary = 'PComm32.dll';
  CarRet            = #$d + #$a;

implementation

{$R *.dfm}

procedure TPcommForm.FormCreate(Sender: TObject);
begin
    LibCode := LoadLibrary(TurboPcommLibrary);
    OpenPmacDevice       := GetProcAddressX(LibCode, 'OpenPmacDevice');
    ClosePmacDevice      := GetProcAddressX(LibCode, 'ClosePmacDevice');
    PmacFlush            := GetProcAddressX(LibCode, 'PmacFlush');
    GetResponse          := GetProcAddressX(LibCode, 'PmacGetResponseA');

    if not OpenPMACDevice(FDeviceNumber) then
      raise Exception.Create('Unable to open Pmac');

    Converse('$$$');
    Sleep(3000);
    ClosePmacDevice(FDeviceNumber);
end;

{ returns a string resulting from the string command }
function TPcommForm.Converse(cmd : string) : string;
var  tx : array [0..4095] of char;
     rx : array [0..4095] of char;
begin
  Result := '';
  if FDeviceNumber < 0 then exit;

  strPLcopy(tx, cmd, 4095);
  GetResponse(FDeviceNumber, rx, 4095, Tx);
  Result := RX;
  if Length(Result) <> 0 then
    Log(TX + CarRet + Result)
  else
    Log(TX);

end;

function TPcommForm.GetProcAddressX(hModule: HMODULE; lpProcName: LPCSTR): FARPROC;
begin
  Result := GetProcAddress(hModule, lpProcName);
  if not Assigned(Result) then
    raise Exception.CreateFmt('Unable to find method %s', [lpProcName]);
end;

end.
