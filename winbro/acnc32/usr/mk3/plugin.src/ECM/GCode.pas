unit GCode;

interface

uses AbstractExtGCode, INamedInterface, NamedPlugin;

type
  TTestGCode = class (TAbstractNCExtGCode)
    function GetPattern: WideString; override; stdcall;
    function Compile(Compiler: INCCompiler; Line: WideString): Pointer; override; stdcall;
    procedure Execute(Embed: Pointer); override; stdcall;
    procedure Cleanup(Embed: Pointer); override; stdcall;
  end;

implementation

{ TTestGCode }

function TTestGCode.Compile(Compiler: INCCompiler; Line: WideString): Pointer; stdcall;
begin
  Result:= Pointer(123123);
end;

function TTestGCode.GetPattern: WideString; stdcall;
begin
  Result:= '67';
end;

procedure TTestGCode.Execute(Embed: Pointer); stdcall;
begin
  NamedPlugin.Named.EventLog('Were running');
end;

procedure TTestGCode.Cleanup(Embed: Pointer); stdcall;
begin

end;

initialization
  TAbstractNCExtGCode.AddNCExtGCode('67', TTestGCode.Create);
end.
