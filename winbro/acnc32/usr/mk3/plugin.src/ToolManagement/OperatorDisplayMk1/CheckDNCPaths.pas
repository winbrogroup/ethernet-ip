unit CheckDNCPaths;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, CellMode, FileCtrl;

type
  TCheckDNCPathForm = class(TForm)
    Panel1: TPanel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    LabelPanel: TPanel;
    procedure BitBtn2Click(Sender: TObject);
  private
    StaticText : array [0..1, 0..8] of TStaticText;
    procedure CheckPaths;
    procedure SetStaticTexts(Index : Integer; Res : Boolean);
  public
    class procedure Execute;
    constructor Create(aOwner : TComponent); override;
    procedure Resize; override;
    procedure DoShow; override;
  end;

var
  CheckDNCPathForm: TCheckDNCPathForm;

implementation

{$R *.DFM}

constructor TCheckDNCPathForm.Create(aOwner: TComponent);
var I, J : Integer;
begin
  inherited;
  for I := 0 to 1 do begin
    for J := 0 to 8 do begin
      StaticText[I, J] := TStaticText.Create(Self);
      StaticText[I, J].Parent := Self;
      StaticText[I, J].BorderStyle := sbsSunken;
      StaticText[I, J].Alignment := taLeftJustify;
    end;
  end;

  StaticText[0, 0].Caption := 'Production OCT Path';
  StaticText[0, 1].Caption := 'Production OPP Path';
  StaticText[0, 2].Caption := 'Buffered Path';
  StaticText[0, 3].Caption := 'Rework Path';
  StaticText[0, 4].Caption := 'Development Path';
  StaticText[0, 5].Caption := 'Remote CMM Path';
  StaticText[0, 6].Caption := 'Local CMM Path';
  StaticText[0, 7].Caption := 'Remote Signature Path';
  StaticText[0, 8].Caption := 'Local Signature Path';
  CheckPaths;
end;

procedure TCheckDNCPathForm.DoShow;
begin
  inherited;
end;

procedure TCheckDNCPathForm.SetStaticTexts(Index : Integer; Res : Boolean);
const OKFailLang : array [Boolean] of string = ( 'Fail', 'OK' );
var C : TColor;
begin
  StaticText[1, Index].Caption := OKFailLang[Res];
  if Res then
    C := clLime
  else
    C := clRed;

  StaticText[1, Index].Color := C;
  StaticText[0, Index].Color := C;
end;

procedure TCheckDNCPathForm.CheckPaths;
begin
  SetStaticTexts(0, DirectoryExists(CellOCTPath));
  SetStaticTexts(1, DirectoryExists(CellOPPPath));
  SetStaticTexts(2, DirectoryExists(BufferedPath));
  SetStaticTexts(3, DirectoryExists(ReworkPath));
  SetStaticTexts(4, DirectoryExists(DevelopmentPath));
  SetStaticTexts(5, DirectoryExists(RemoteCMMPath));
  SetStaticTexts(6, DirectoryExists(LocalCMMPath));
  SetStaticTexts(7, DirectoryExists(RemoteSigPath));
  SetStaticTexts(8, DirectoryExists(LocalSigPath));
end;

class procedure TCheckDNCPathForm.Execute;
begin
  if not Assigned(CheckDNCPathForm) then begin
    CheckDNCPathForm := TCheckDNCPathForm.Create(nil);
    try
      CheckDNCPathForm.ShowModal;
    finally
      CheckDNCPathForm.Free;
      CheckDNCPathForm := nil;
    end;
  end;
end;


procedure TCheckDNCPathForm.Resize;
var I : Integer;
begin
  inherited;
  for I := 0 to 8 do begin
    StaticText[0, I].Left := 0;
    StaticText[0, I].Height := LabelPanel.ClientHeight div 9;
    StaticText[0, I].Width := LabelPanel.ClientWidth * 4 div 5;
    StaticText[0, I].Top := StaticText[0, I].Height * I;
  end;

  for I := 0 to 8 do begin
    StaticText[1, I].Left := LabelPanel.ClientWidth * 4 div 5;
    StaticText[1, I].Height := LabelPanel.ClientHeight div 9;
    StaticText[1, I].Width := LabelPanel.ClientWidth div 5;
    StaticText[1, I].Top := StaticText[0, I].Height * I;
  end;
end;

procedure TCheckDNCPathForm.BitBtn2Click(Sender: TObject);
begin
  CheckPaths;
end;

end.
