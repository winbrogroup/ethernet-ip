unit OCTPopUp;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, AbstractFormPlugin, IFormInterface, NamedPlugin,
  ComCtrls, INamedInterface, Grids, ACNC_TouchSoftkey, IOCT, PluginInterface,
  ImgList, OCTTypes, OCTListBox, CNCTypes, CellMode, OCTUtils, OCTData,
  RequiredToolData, ToolData, ActionList;


type
  TOperatorDisplayModes = (odmReqToolLifeMode,odmActionListMode);

  TOCTTag = (
    oRunning,
    oMode,
    oIndex,
    oReqResume,
    oSingleStep,
    oCheckDNCPaths,
    oStatus1,
    oStatus2,
    oCellMode,
    oToolClamp,
    oPartClamp,
    oFaultAcknowledge,
    oNewElectrode,
    oFilename,
    oPartName
  );

  TOCTFormDlg = class(TInstallForm)
    OCTImages: TImageList;
    BottomPanel: TPanel;
    ToolManagementPanel: TPanel;
    Panel1: TPanel;
    BaseOCTPanel: TPanel;
    ListView: TListView;
    Panel14: TPanel;
    OCTStatus1: TLabel;
    OCTStatus2: TLabel;
    Panel12: TPanel;
    MessageLabel: TLabel;
    AcceptKey: TTouchSoftkey;
    RecoveryPanel: TPanel;
    Panel6: TPanel;
    RestartPanel: TPanel;
    RestartCurrentButton: TTouchSoftkey;
    Panel10: TPanel;
    SkipPanel: TPanel;
    SkipButton: TTouchSoftkey;
    AbandonOCTPanel: TPanel;
    AbandonOCTButton: TTouchSoftkey;
    Panel2: TPanel;
    Panel5: TPanel;
    ReworkButton: TTouchSoftkey;
    Panel8: TPanel;
    OCTRecoveryTitle: TPanel;
    ActionListPanel: TPanel;
    Panel15: TPanel;
    BtnToolclamp: TTouchSoftkey;
    BtnFaultAck: TTouchSoftkey;
    DisplayCloseKey: TTouchSoftkey;
    Panel3: TPanel;
    Panel7: TPanel;
    Panel9: TPanel;
    Panel11: TPanel;
    Panel13: TPanel;
    BtnPartClamp: TTouchSoftkey;
    BtnElectrode: TTouchSoftkey;
    procedure AbandonOCTButtonMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure BtnElectrodeMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure BtnElectrodeMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure BtnFaultAckMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure BtnFaultAckMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure BtnToolclampMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure BtnPartClampMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure BtnPartClampMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure BtnToolclampMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Button2Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure RestartCurrentButtonMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure SkipButtonMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure AcceptKeyMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure AbandonPartButtonMouseUp(Sender: TObject;
      Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure BtnCloseClick(Sender: TObject);
    procedure ReworkButtonMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
  private
    OCTDisplay : TOCTListBox;
    ActionList : TActionListPanel;
    FInRecovery    : Boolean;
    FRecoveryOption: TRecoveryOptions;
    CurrentRecoveryOPtion : TRecoveryOptions;
    RequiredTools : TRequiredToolList;
    Tools: TToolList;

    ToolSpent : Integer;
    ToolRemove : Integer;
    ToolRunning : Integer;
    ToolWanted : Integer;
    ToolNone : Integer;
    ToolFinished : Integer;

    OCTName: string;
    PartName: string;
    procedure DisplayRecoveryOptions;
    procedure SetButtonPanel(RecOption: TRecoveryOptions);
    procedure OCTModeChange(Sender : TObject);
    procedure RequiredToolChange(Sender: TObject);
    procedure CellModeChange(ATag: TTagRef);
    procedure UpdateRecoveryTitle;
  public
    DisplayMode :TOperatorDisplayModes;
    MytestRecovery : Boolean;
    Tags: array[TOCTTag] of TTagRef;
    procedure SetDisplayMode(DMode : TOperatorDisplayModes);
    procedure Install; override;
    procedure FinalInstall; override;
    procedure Execute; override;
    procedure CloseForm; override;
    procedure DisplaySweep; override;
    procedure TagChanged(Tag: TTagRef);
    procedure Resize; override;
    function OCTRunning: Boolean;
    function OCTIndex: Integer;
    function OCTMode: Boolean;


    procedure DoRecovery;

    property InRecovery: Boolean read FInRecovery write FInRecovery;
  end;

var
  Popup : TOCTFormDlg;

procedure TagChangeHandler(Tag : TTagRef);stdcall;

implementation

uses
  Confirm, CheckDNCPaths;

{$R *.DFM}

const
  TagDefinitions : array [TOCTTag] of TTagDefinitions = (
    ( Ini: NameOCTRunning; Tag: NameOCTRunning; Owned: False; TagType: 'TIntegerTag' ),
    ( Ini: NameOCTMode; Tag: NameOCTMode; Owned: False; TagType: 'TIntegerTag' ),
    ( Ini: NameOCTIndex; Tag: NameOCTIndex; Owned: False; TagType: 'TIntegerTag' ),
    ( Ini: NameOCTReqResume; Tag: NameOCTReqResume; Owned: True; TagType: 'TIntegerTag' ),
    ( Ini: NameOCTSingleStep; Tag: NameOCTSingleStep; Owned: False; TagType: 'TIntegerTag' ),
    ( Ini: NameCheckDNCPaths; Tag: NameCheckDNCPaths; Owned: False; TagType: 'TIntegerTag' ),
    ( Ini: 'OCTStatus1'; Tag: 'OCTStatus1'; Owned: False; TagType: 'TStringTag' ),
    ( Ini: 'OCTStatus2'; Tag: 'OCTStatus2'; Owned: False; TagType: 'TStringTag' ),
    ( Ini: NameCM_CellMode; Tag: NameCM_CellMode; Owned: False; TagType: 'TIntegerTag' ),
    ( Ini: 'OCT_ToolClamp'; Tag: 'OCT_ToolClamp'; Owned: True; TagType: 'TIntegerTag' ),
    ( Ini: 'OCT_PartClamp'; Tag: 'OCT_PartClamp'; Owned: True; TagType: 'TIntegerTag' ),
    ( Ini: 'OCT_FaultAck'; Tag: 'OCT_FaultAck'; Owned: True; TagType: 'TIntegerTag' ),
    ( Ini: 'OCT_NewElectrode'; Tag: 'OCT_NewElectrode'; Owned: True; TagType: 'TIntegerTag' ),
    ( Ini: NameOCTLoadedName; Tag: NameOCTLoadedName; Owned: False; TagType: 'TStringTag' ),
    ( Ini: NamePartSFC; Tag: NamePartSFC; Owned: False; TagType: 'TStringTag' )
  );

procedure TOCTFormDlg.CloseForm;
begin
  if Visible then
    ModalResult := mrOK;
end;

procedure TOCTFormDlg.DisplaySweep;
begin
end;

procedure TOCTFormDlg.Execute;
begin
  if not Visible and Named.GetAsBoolean(Tags[oMode]) then begin
    ShowModal;
  end else
    Close;
end;


procedure TagChangeHandler(Tag : TTagRef);stdcall;
begin
  Popup.TagChanged(Tag);
end;

function TOCTFormDlg.OCTMode: Boolean;
begin
  Result:= Named.GetAsBoolean(Tags[oMode]);
end;

function TOCTFormDlg.OCTRunning: Boolean;
begin
  Result:= Named.GetAsBoolean(Tags[oRunning]);
end;

function TOCTFormDlg.OCTIndex: Integer;
begin
  Result:= Named.GetAsInteger(Tags[oIndex]);
end;


procedure TOCTFormDlg.TagChanged(Tag: TTagRef);
var ctmp: array[0..1024] of Char;
begin
  if Tag = Tags[oStatus1] then begin
    Named.GetAsString(Tag, ctmp, Length(ctmp) - 1);
    OCTStatus1.Caption:= ctmp;
  end;

  if Tag = Tags[oStatus2] then begin
    Named.GetAsString(Tag, ctmp, Length(ctmp) - 1);
    OCTStatus2.Caption:= ctmp;
  end;

  if Tag = Tags[oCheckDNCPaths] then
    if not Named.GetAsBoolean(Tags[oCheckDNCPaths]) then
      TCheckDNCPathForm.Execute;

  if Tag = Tags[oMode] then
    Self.OCTModeChange(nil);

  if Tag = Tags[oCellMode] then
    Self.CellModeChange(Tag);

  if Tag = Tags[oFilename] then begin
    Named.GetAsString(Tags[oFilename], ctmp, Length(ctmp));
    OCTName:= ctmp;
    UpdateRecoveryTitle;
  end;

  if Tag = Tags[oPartname] then begin
    Named.GetAsString(Tags[oPartname], ctmp, Length(ctmp));
    PartName:= ctmp;
    UpdateRecoveryTitle;
  end;
end;



procedure TOCTFormDlg.FinalInstall;
var I: TOCTTag;
    BM : TBitMap;
begin
  inherited;

  ListView.SmallImages := TImageList.Create(Self);
  ListView.SmallImages.Height := 32;
  ListView.SmallImages.Width := 32;

  BM := TBitMap.Create;
  BM.LoadFromResourceName(HInstance, 'TOOL_SPENT');
   ToolSpent := ListView.SmallImages.Add(BM, nil);
  BM := TBitMap.Create;
  BM.LoadFromResourceName(HInstance, 'TOOL_REMOVE');
  ToolRemove := ListView.SmallImages.Add(BM, nil);
  BM := TBitMap.Create;
  BM.LoadFromResourceName(HInstance, 'TOOL_RUNNING');
  ToolRunning := ListView.SmallImages.Add(BM, nil);
  BM := TBitMap.Create;
  BM.LoadFromResourceName(HInstance, 'TOOL_WANTED');
  ToolWanted := ListView.SmallImages.Add(BM, nil);
  BM := TBitMap.Create;
  BM.LoadFromResourceName(HInstance, 'TOOL_NONE');
  ToolNone := ListView.SmallImages.Add(BM, nil);
  BM := TBitMap.Create;
  BM.LoadFromResourceName(HInstance, 'TOOL_FINISHED');
  ToolFinished := ListView.SmallImages.Add(BM, nil);

  for I := Low(I) to High(I) do begin
    if not TagDefinitions[I].Owned then
      Tags[I] := Named.AquireTag(PChar(TagDefinitions[I].Tag), PChar(TagDefinitions[I].TagType), TagChangeHandler);
  end;
end;

procedure TOCTFormDlg.Install;
var I: TOCTTag;
begin
  inherited;
  for I := Low(I) to High(I) do begin
    if TagDefinitions[I].Owned then
      Tags[I] := Named.AddTag(PChar(TagDefinitions[I].Tag), PChar(TagDefinitions[I].TagType));
  end;

  MytestRecovery := False;
  Popup := Self;
  Font.Name := NamedPlugin.FontName;
  RequiredTools:= TRequiredToolList.Create;
  Tools:= TToolList.Create;
  RequiredTools.OnChange:= RequiredToolChange;
  Tools.OnChange:= RequiredToolChange;
end;

procedure TOCTFormDlg.FormCreate(Sender: TObject);
begin
  OCTDisplay := TOCTListBox.Create(Self);
  OCTDisplay.ImageList := OCtImages;
  OCTDisplay.Parent := BottomPanel;
  OCTDisplay.Align := alClient;

  ActionList := TActionListPanel.Create(Self);
  with ActionList do
    begin
    Parent := ActionListPanel;
    Align := alClient;

    end;

  BaseOCTPanel.Visible:= False;
  Top := 70;
  left := 1;
  Height := 770 - Top;
  Width := 1024;
end;

procedure TOCTFormDlg.Resize;
begin
  inherited;
  if Inrecovery or MytestRecovery then begin
    OCTDisplay.Visible:= False;
    BaseOCTPanel.Visible:= True;
  end else begin
    BaseOCTPanel.Visible:= False;
    OCTDisplay.Visible:= True;
    ActionList.Height := BottomPanel.Height-OCTDisplay.Height-10;
  end;
  case DisplayMode of
    odmReqToolLifeMode:
    begin
    ActionListPanel.Visible := False;
    ToolManagementPanel.Visible := True;
    ToolManagementPanel.Align := alClient;
    end;

    odmActionListMode:
    begin
    ActionListPanel.Visible := True;
    ToolManagementPanel.Visible := False;
    ActionListPanel.Align := alClient;
    end;
  end; // case

  //FaultAckButton.Height := PartStatusButton.Height;
  //FaultAckButton.Left := PartStatusButton.Left + PartStatusButton.Width+2;
  //FaultAckButton.Top := PartStatusButton.Top;


end;

procedure TOCTFormDlg.SetButtonPanel(RecOption : TRecoveryOptions);
begin
  AcceptKey.Visible := False;

  RestartCurrentButton.ButtonColor := clWhite;
  SkipButton.ButtonColor := clWhite;
  AbandonOCTButton.ButtonColor := clWhite;
  //AbandonPartButton.ButtonColor := clWhite;
  ReworkButton.ButtonColor := clWhite;

  FRecoveryOption:= RecOption;

  ReworkButton.Enabled:= CellMode.LocalCellMode = cmProduction;
  if CellMode.LocalCellMode = cmProduction then
    ReworkButton.ButtonColor:= clWhite
  else
    ReworkButton.ButtonColor:= $a0a0a0;

  case RecOption of
    roNone: begin
      MessageLabel.Caption := '';
      AcceptKey.Visible := False;
    end;

    roRestartCurrent: begin
      RestartCurrentButton.ButtonColor := clYellow;
      MessageLabel.Caption := 'Recover any errors then Press accept key then Cycle Start to RESTART current OPP';
      AcceptKey.Visible := True;
    end;

    roAbandonCurrent: begin
      SkipButton.ButtonColor := clYellow;
      MessageLabel.Caption := 'Press Accept To Move to next OPP';
      AcceptKey.Visible := True;
    end;

    roAbandonOCT: begin
      AbandonOCTButton.ButtonColor := clYellow;
      MessageLabel.Caption := 'Press Accept To ABANDON OCT';
      AcceptKey.Visible := True;
    end;

    roAbandonPartInCell: begin
      //AbandonPartButton.ButtonColor := clYellow;
      //MessageLabel.Caption := 'Press Accept To ABANDON PART';
      //AcceptKey.Visible := True;
    end;

    roReworkOPP : begin
      ReworkButton.ButtonColor := clYellow;
      MessageLabel.Caption := 'Press Accept To REWORK OPP';
      AcceptKey.Visible := True;
    end;
  end;
end;

procedure TOCTFormDlg.OCTModeChange(Sender : TObject);
begin
  if not Named.GetAsBoolean(Tags[oMode]) and Visible then
    ModalResult := mrOK;
end;

procedure TOCTFormDlg.BtnCloseClick(Sender: TObject);
begin
  Close;
end;

procedure TOCTFormDlg.AbandonPartButtonMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  CurrentRecoveryOption := roAbandonPartInCell;
  SetButtonPanel(roAbandonPartInCell);
end;


procedure TOCTFormDlg.AbandonOCTButtonMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  CurrentRecoveryOption := roAbandonOCT;
  SetButtonPanel(roAbandonOCT);
end;


procedure TOCTFormDlg.AcceptKeyMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var I: Integer;
begin

  SetInterrupted(False);
  SetResume(False);
  case FRecoveryOption of
    roNone: begin
    end;
    roRestartCurrent: begin
      //OCTDisplay.OPPList[OCTIndex].Status:= osStarted;
      OCTDisplay.OPPList[OCTIndex].Status:= osNone;
      InRecovery:= False;
    end;
    roAbandonCurrent: begin
      OCTDisplay.OPPList[OCTIndex].Status:= osSkipped;
      InRecovery:= False;
    end;
    roAbandonOCT: begin
      for I:= OCTDisplay.OPPList.Count - 2 downto OCTIndex do
        OCTDisplay.OPPList[I].Status:= osAbandoned;
      InRecovery:= False;
    end;
    roAbandonPartInCell: begin
      for I:= OCTDisplay.OPPList.Count - 2 downto OCTIndex do
        OCTDisplay.OPPList[I].Status:= osAbandoned;
      InRecovery:= False;
    end;
    roReworkOPP: begin
      if (CellMode.LocalCellMode = cmProduction) and
         (CurrentRecoveryOption = roReworkOPP) then begin
        CellMode.CellModeForm.SetCellMode(cmRework);
        CellMode.SetInterrupted(True);
      end;
    end;
  end;
  Resize;
end;

procedure TOCTFormDlg.SkipButtonMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  CurrentRecoveryOption := roAbandonCurrent;
  SetButtonPanel(roAbandonCurrent);
end;

procedure TOCTFormDlg.ReworkButtonMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  CurrentRecoveryOption := roReworkOPP;
  SetButtonPanel(roReworkOPP);
end;

procedure TOCTFormDlg.RestartCurrentButtonMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  CurrentRecoveryOption := roRestartCurrent;
  SetButtonPanel(roRestartCurrent);
end;


procedure TOCTFormDlg.DisplayRecoveryOptions;
begin
  InRecovery := True;
  SetButtonPanel(roNone);
  if not Visible then
    ShowModal;
  Resize;
end;

procedure TOCTFormDlg.DoRecovery;
begin
  DisplayRecoveryOptions;
end;

procedure TOCTFormDlg.RequiredToolChange(Sender: TObject);
  function FindTool(Name: string): TToolData;
  var I: Integer;
  begin
    Tools.Lock;
    try
      for I:= 0 to Tools.Count - 1 do begin
        Result:= Tools[I];
        if CompareText(Result.ShuttleID, Name) = 0 then
          Exit;
      end;
    finally
      Tools.Unlock;
    end;
    Result:= nil;
  end;

var I: Integer;
    li: TListItem;
    T: TToolData;
begin
  if ListView.Items.Count <> RequiredTools.Count then
    ListView.Clear;

  for I:= 0 to RequiredTools.Count - 1 do begin
    if ListView.Items.Count <= I then begin
      li:= ListView.Items.Add;
      Li.SubItems.Add('--');
      Li.SubItems.Add('--');
      Li.SubItems.Add('--');
      Li.SubItems.Add('--');
      Li.SubItems.Add('--');
    end
    else
      li:= ListView.Items[I];

    Li.Caption:= RequiredTools[I].ToolName;
    Li.SubItems[1]:= FloatToStr(RequiredTools[I].Length);
    case RequiredTools[I].Priority of
      0: Li.ImageIndex:= ToolFinished;
      1: Li.ImageIndex:= ToolNone;
      2,3: Li.ImageIndex:= ToolWanted;
    end;

    T:= FindTool(RequiredTools[I].ToolName);
    if T <> nil then begin
      Li.SubItems[0]:= IntToStr(T.ShuttleSN);
      Li.SubItems[2]:= FloatToStr(T.ActToolLength - T.MinToolLength);
      Li.SubItems[3]:= IntToStr(T.Ex1);
      Li.SubItems[4]:= IntToStr(T.TPM1);
    end
    else begin
      Li.SubItems[0]:= '--';
      Li.SubItems[2]:= '--';
      Li.SubItems[3]:= '--';
      Li.SubItems[4]:= '--';
    end;
  end;
end;

procedure TOCTFormDlg.CellModeChange(ATag: TTagRef);
var Mode: TCellMode;
begin
  Mode:= TCellMode(Named.GetAsInteger(ATag));
  if (Mode = cmProduction) and not Visible then begin
    if CellMode.IsInterrupted then begin
      DisplayRecoveryOptions;
    end;
  end;
end;

procedure TOCTFormDlg.SetDisplayMode(DMode: TOperatorDisplayModes);
begin
  DisplayMode := Dmode;
  case DMode of
    odmReqToolLifeMode:
    begin

    end;

    odmActionListMode:
    begin
    end;

  end;// case

end;

procedure TOCTFormDlg.Button2Click(Sender: TObject);
begin
 if DisplayMode = odmReqToolLifeMode then
  begin
  DisplayMode := odmActionListMode;
  end
 else
  begin
  DisplayMode := odmReqToolLifeMode;
  end;
 resize;
end;

procedure TOCTFormDlg.BtnToolclampMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  Named.SetAsBoolean(Tags[oToolClamp], True);
end;

procedure TOCTFormDlg.BtnToolclampMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  Named.SetAsBoolean(Tags[oToolClamp], False);
end;


procedure TOCTFormDlg.BtnPartClampMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  Named.SetAsBoolean(Tags[oPartClamp], True);
end;

procedure TOCTFormDlg.BtnPartClampMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  Named.SetAsBoolean(Tags[oPartClamp], False);
end;

procedure TOCTFormDlg.BtnFaultAckMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  Named.SetAsBoolean(Tags[oFaultAcknowledge], True);
end;

procedure TOCTFormDlg.BtnFaultAckMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  Named.SetAsBoolean(Tags[oFaultAcknowledge], False);
end;

procedure TOCTFormDlg.BtnElectrodeMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var Tool: TToolData;
begin
  Named.SetAsBoolean(Tags[oNewElectrode], True);
  Tool:= Tools.ToolAtEx1(0);
  if Assigned(Tool) then
    Tool.ActToolLength:= Tool.MaxToolLength;
end;

procedure TOCTFormDlg.BtnElectrodeMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  Named.SetAsBoolean(Tags[oNewElectrode], False);
end;

procedure TOCTFormDlg.UpdateRecoveryTitle;
begin
  Self.OCTRecoveryTitle.Caption:= OCTname + ' - ' + PartName;
end;

initialization
  TAbstractFormPlugin.AddPlugin('OperatorDisplayMK1', TOCTFormDlg);
end.
