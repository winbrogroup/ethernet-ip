unit OCTListBox;

interface

uses Classes, SysUtils, Windows, ExtCtrls, Grids, ACNC_TouchSoftkey, ComCtrls,
     Controls, StdCtrls, Graphics, OCTData, NamedPlugin;

type
  TOCTListBox = class(TCustomPanel)
  private

    Grid: TStringGrid;
    InvertSelectionButton : TTouchSoftKey;
    TitlePanel  : TPanel;
    GridPanel   : TPanel;
    UnitHeight : Integer;

    SB : TStatusBar;

    procedure OPPListChange(Sender: TObject);
    procedure MyDrawCell(Sender: TObject; Col, Row: Integer; Rect: TRect;
                         State: TGridDrawState);
    procedure GridMouseDown(Sender: TObject; Button: TMouseButton;
                            Shift: TShiftState; X, Y: Integer);
    procedure SetTopRow(Value: Integer);
    procedure InvertSelection(Sender: TObject);
  public
    ImageList   : TImageList;
    OPPList: TOPPList;
    constructor Create(aOwner : TComponent); override;
    destructor Destroy; override;
    procedure Resize; override;
    procedure RefreshDisplay;
  end;

implementation

uses CellMode, OCTTypes, OCTPopUp;

{ TOCTListBox }

constructor TOCTListBox.Create(aOwner: TComponent);
begin
  inherited;

  OPPList:= TOPPList.Create;
  OPPList.OnChange:= OPPListChange;

  TitlePanel := TPanel.Create(Self);
  with TitlePanel do begin
    Parent := Self;
    Align := alTop;
    BevelInner := bvLowered;
  end;
  InvertSelectionButton := TTouchSoftKey.Create(Self);
  InvertSelectionButton.Parent:= TitlePanel;
  InvertSelectionButton.OnClick:= InvertSelection;

  GridPanel := TPanel.Create(Self);
  GridPanel.Parent := Self;
  GridPanel.ParentFont := True;
  GridPanel.Align := alTop;

  SB := TStatusBar.Create(self);
  SB.Parent := Self;
  SB.Align := alBottom;
  SB.SimplePanel := False;
  SB.Panels.Add;
  SB.Panels.Add;
  SB.Panels.Add;
  SB.Panels[0].Width := 110;
  SB.Panels[1].Width := 150;

  Grid := TStringGrid.Create(Self);
  with Grid do begin
    Parent := GridPanel;
    Align := alClient;
    RowCount := 2;
    FixedRows := 1;
    FixedCols := 0;
    ScrollBars := ssNone;

    Cells[1,0] := 'Name';
    Cells[2,0] := 'Status';
    Cells[3,0] := 'Start Time';
    Cells[4,0] := 'Duration';
    DefaultDrawing := True;
    OnMouseDown := GridMouseDown;
    //OnTopLeftChanged := TopRowChanged;
  end;
  Color := clWindow;

  Grid.OnDrawCell := MyDrawCell;
  UnitHeight := 30;
end;



procedure TOCTListBox.Resize;
var W : Integer;
begin
  inherited;

  InvertSelectionButton.Legend:= 'Enable~Disable';

  Grid.Width := GridPanel.Width-2;
  Grid.DefaultRowHeight := UnitHeight;
  Grid.GridLineWidth := 1;

  Grid.Height := (Grid.RowCount+3)*(UnitHeight+Grid.GridLineWidth);
  GridPanel.Height := Height-({TitlePanel.Height+}SB.Height+2);

  W := (GridPanel.Width + 12) div 30; // Plus 12 to account for rounding errors!
  Grid.ColWidths[0] := W;
  Grid.ColWidths[1] := W * 17;
  Grid.ColWidths[2] := W * 3;
  Grid.ColWidths[3] := W * 4;
  Grid.ColWidths[4] := W * 4;
  Grid.Invalidate;
end;

procedure TOCTListBox.MyDrawCell(Sender: TObject; Col, Row: Longint; Rect: TRect; State: TGridDrawState);
var
Contents : String;
begin
  Contents := Grid.Cells[Col,row];
  with Grid.Canvas do begin
    Font.Name := Self.Font.Name;
    Font.Size := Self.Font.Size;
    Font.Style := Self.Font.Style;

    if Row = 0 then begin
      Brush.Color := clSilver;
      Font.Color := clBlack;
      FillRect(Rect);
      TextOut(Rect.Left+3,Rect.Top,Contents);
      exit;
    end;

  Brush.Color := clWhite;
  Font.Color := clBlack;

  if Popup.OCTMode and (Col > 0) then begin
    if(Row = Popup.OCTIndex + 1)  then begin
      Brush.Color := clGreen;
      Font.Color := clYellow;
    end;
  end;

  if ((Row-1) <=  OPPList.Count) and (OPPList.Count > 0) then begin
    if not OPPList.OPP[Row-1].Enabled then begin
      Font.Color := clgray;
    end;
  end;

  FillRect(Rect);
  if Col > 0 then
     begin
     TextOut(Rect.Left+3,Rect.Top,Contents);
     end
  else
      begin
      if Contents = '1' then
          begin
          ImageList.Draw(Grid.Canvas,Rect.Left,Rect.Top,1,True);
          end
      else
          begin
          ImageList.Draw(Grid.Canvas,Rect.Left,Rect.Top,0,True);
          end;
      end;
  end;
end;

destructor TOCTListBox.Destroy;
begin
  inherited Destroy;
end;

procedure TOCTListBox.GridMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
end;

const
  StatusText: array[TOPPStatus] of string = (
    'Pend',
    'Started',
    'Complete',
    'Reco', // The base OCT Manager sets this status upon ResetRewind
    'Aban',
    'Skip',
    '?'
  );

procedure TOCTListBox.RefreshDisplay;
var I: Integer;
    Mins: Integer;
    Secs: Integer;
begin
  for I:= 0 to OPPList.Count - 1 do begin
    if  OPPList.OPP[I].Enabled then
      Grid.Cells[0, I + 1]:= '1'
    else
      Grid.Cells[0, I + 1]:= '0';

    Grid.Cells[1, I + 1]:= OPPList.OPP[I].FileName;
    Grid.Cells[2, I + 1]:= StatusText[OPPList.OPP[I].Status];

    if OPPList.OPP[I].StartTime < 1 then
      Grid.Cells[3, I + 1]:= '--'
    else
      Grid.Cells[3, I + 1]:= TimeToStr(OPPList.OPP[I].StartTime);


    Mins:= Round(OPPList.OPP[I].Duration);
    Secs:= Mins mod 60;
    Mins:= Mins div 60;
    Grid.Cells[4, I + 1]:= Format('%d:%.2d', [Mins, Secs]);
  end;
end;

procedure TOCTListBox.SetTopRow(Value :Integer);
begin
  if Value < 1 then
    Grid.TopRow := 1
  else
    Grid.TopRow := Value;
end;


procedure TOCTListBox.OPPListChange(Sender: TObject);
begin
  if OPPList.Count + 1 <> Grid.RowCount then begin
    Grid.RowCount:= OPPList.Count + 1;
  end;
  RefreshDisplay;

  if Popup.OCTMode and Popup.OCTRunning then begin
    if (OPPList[Popup.OCTIndex].Status = osInRecovery) and not Popup.InRecovery then
      Popup.DoRecovery;
  end;
end;

procedure TOCTListBox.InvertSelection(Sender: TObject);
begin
  if not Popup.OCTRunning then begin
    if Grid.Row > 0 then begin
      if Grid.Row - 1 <= OPPList.Count then begin
        OPPList[Grid.Row - 1].Enabled:= not OPPList[Grid.Row - 1].Enabled;
      end;
    end;
    SB.Panels[0].Text:= Format('OPP %d of %d', [Popup.OCTIndex, OPPList.Count]);
  end;
end;

end.
