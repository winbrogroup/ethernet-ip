unit PartLife;

interface

uses Classes, SysUtils, Windows, AbstractScript, INamedInterface, NamedPlugin,
     IScriptInterface, IStringArray, CncTypes, OppHeader, PartLifeEditor,
     IniFiles, FilenameDialogue, Forms;

type
  TPartLifeStart = class (TScriptMethod)
  public
    function Name: WideString; override; stdcall;
    function ParameterName(Index: Integer): WideString; override; stdcall;
    function ParameterCount: Integer; override; stdcall;
    procedure Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer); override; stdcall;
  end;

  TPartLifePrepare = class (TScriptMethod)
  public
    function Name: WideString; override; stdcall;
    function ParameterName(Index: Integer): WideString; override; stdcall;
    function ParameterCount: Integer; override; stdcall;
    procedure Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer); override; stdcall;
  end;

  TPartLifeComplete = class(TScriptMethod)
  public
    function Name: WideString; override; stdcall;
    function ParameterName(Index: Integer): WideString; override; stdcall;
    function ParameterCount: Integer; override; stdcall;
    procedure Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer); override; stdcall;
  end;

  TPartLifeRework = class(TScriptMethod)
  public
    function Name: WideString; override; stdcall;
    function ParameterName(Index: Integer): WideString; override; stdcall;
    function ParameterCount: Integer; override; stdcall;
    procedure Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer); override; stdcall;
  end;

  TPartLifeFlush = class(TScriptMethod)
  public
    function Name: WideString; override; stdcall;
    function ParameterName(Index: Integer): WideString; override; stdcall;
    function ParameterCount: Integer; override; stdcall;
    procedure Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer); override; stdcall;
  end;

  TPartLifePartEditor = class(TScriptMethod)
  public
    function Name: WideString; override; stdcall;
    function ParameterCount: Integer; override; stdcall;
    procedure Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer); override; stdcall;
  end;

  TPartLifePartExists = class(TScriptMethod)
  public
    function Name: WideString; override; stdcall;
    function IsFunction: Boolean; override; stdcall;
    procedure Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer); override; stdcall;
  end;

  TPartLifePartComplete = class(TScriptMethod)
  public
    function Name: WideString; override; stdcall;
    function IsFunction: Boolean; override; stdcall;
    function ParameterCount: Integer; override; stdcall;
    procedure Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer); override; stdcall;
  end;

  TPartLifeSetProperty = class(TScriptMethod)
  public
    function Name: WideString; override; stdcall;
    function ParameterCount: Integer; override; stdcall;
    procedure Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer); override; stdcall;
  end;

  TPartLifeGetProperty = class(TScriptMethod)
  public
    function Name: WideString; override; stdcall;
    function IsFunction: Boolean; override; stdcall;
    function ParameterCount: Integer; override; stdcall;
    procedure Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer); override; stdcall;
  end;

  TPartLifeEnd = class(TScriptMethod)
  public
    function Name: WideString; override; stdcall;
    procedure Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer); override; stdcall;
  end;

  TPartLifeReadName = class(TScriptMethod)
  private
    FilenameDlg: TFilenameDlg;
  public
    procedure Reset; override; stdcall;
    function Name: WideString; override; stdcall;
    function IsFunction: Boolean; override; stdcall;
    procedure Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer); override; stdcall;
  end;


  procedure SetPreparedStatus;
  procedure ResetRewind;
  procedure Editor;

  procedure ExportTable(Table: IAcnc32StringArray);
  function GetCSVFile(CreatePath: Boolean = false): string;

var
  LastMDTIndex: Integer;


const
  PartNominalsTable = 'Nominal';

  OpNumber = 'opp_index';
  Status = 'status';
  ToolSN = 'tool_sn';
  Machine = 'machine';
  Rework = 'rework';
  ManualIntervention = 'manual_intervention';
var
  LastPartName: string;
  CurrentPartName: string;
  CurrentPartType: string;
  CurrentIndex: Integer;
  PrepareStatus: Integer;

  { Enable for SetStatus }
  WaitingEvent: Boolean;
  FeatureName: string;

implementation

uses
  OCT;

var FID: Integer;

function GetCSVPath(CreatePath: Boolean = False): string;
var Buffer: array [0..1023] of Char;
begin
  Named.GetAsString(OCT.OCTManager.Tags[otPartLifeOutputPath], Buffer, 255);
  Result:= Buffer;
  //ProdPath:= NamedPlugin.DllLocalPath + '\Production\' + CurrentPartType;
  Result:= Result + CurrentPartType;
  if CreatePath then
    ForceDirectories(Result);
end;

function GetCSVFile(CreatePath: Boolean): string;
begin
  Result:= GetCSVPath(CreatePath);
  Result:= Result + '\' + CurrentPartName + '.csv';
end;

procedure ResetRewind;
begin
  WaitingEvent:= False;
  FID:= 0;
end;

function ResetFault(AFID : FHandle): Boolean; stdcall;
begin
  Result:= FID = 0;
end;

procedure SetPreparedStatus;
var Table: IAcnc32StringArray;
    SN: Integer;
begin
  if WaitingEvent then begin
    if StringArraySet.UseArray(PartNominalsTable, nil, Table) <> AAR_OK then begin
      FID:= Named.SetFaultA('PartLife', 'Unable to use MDT [Nominals]', FaultStopCycle, ResetFault);
      Exit;
    end;

    WaitingEvent:= False;
    Table.SetFieldValue(CurrentIndex, Status, IntToStr(PrepareStatus));
    Table.SetFieldValue(CurrentIndex, OpNumber, IntToStr(OppHeader.OPPHeaderConsumer.OPPFile.OpNumber));
    ToolLife.GetEx1ToolSN(0, SN);
    Table.SetFieldValue(CurrentIndex, ToolSN, IntToStr(SN));
    ExportTable(Table);
    StringArraySet.ReleaseArray(nil, Table);
  end;
end;

procedure Editor;
var Table: IAcnc32StringArray;
begin
  if StringArraySet.UseArray(PartNominalsTable, nil, Table) <> AAR_OK then begin
    FID:= Named.SetFaultA('PartLife', 'Unable to use MDT [Nominals]', FaultStopCycle, ResetFault);
    Exit;
  end;
  PartLifeEditor.TPartLifeEditorForm.Execute(Table, LastMDTIndex);
  ExportTable(Table);
  StringArraySet.ReleaseArray(nil, Table);
  Table:= nil;
end;

{ TPartLifeStart }

function TPartLifeStart.Name: WideString;
begin
  Result:= 'Start';
end;

function TPartLifeStart.ParameterCount: Integer;
begin
  Result:= 2;
end;

function TPartLifeStart.ParameterName(Index: Integer): WideString;
begin
  case Index of
    0: Result:= 'PartType';
    1: Result:= 'PartName';
  else
    Result:= 'Unknown';
  end;
end;

function CheckFieldName(Table: IAcnc32StringArray; AName: string): Boolean;
var I: Integer;
    Tmp: WideString;
begin
  Result:= True;
  for I:= 0 to Table.FieldCount - 1 do begin
    Table.FieldNameAtIndex(I, Tmp);
    if CompareText(Tmp, AName) = 0  then
      Exit;
  end;

  Result:= False;
end;

procedure TPartLifeStart.Execute(const Res: Pointer;
  const Params: array of Pointer; ParamCount: Integer);

var Buffer: array [0..1023] of Char;
    P: string;
    T, N, Conf: string;
    ProdPath: string;
    Table: IAcnc32StringArray;
begin
  Named.GetAsString(Params[0], Buffer, 255);
  T:= Buffer;
  Named.GetAsString(Params[1], Buffer, 255);
  N:= Buffer;

  LastPartName:= N;

  T:= CncTypes.ValidFilename(T);
  N:= CncTypes.ValidFilename(N);

  //if (CompareText(N, CurrentPartName) = 0) and (CompareText(T, CurrentPartType) = 0) then
  //  Exit;

  CurrentPartName:= N;
  CurrentPartType:= T;

  if Trim(N) = '' then
    Exit;

  Named.GetAsString(OCT.OCTManager.Tags[otPartLifeOutputPath], Buffer, 255);
  Conf:= Buffer;
  ProdPath:= Conf + '\' + T;

  ForceDirectories(ProdPath);

  if StringArraySet.UseArray(PartNominalsTable, nil, Table) <> AAR_OK then begin
    FID:= Named.SetFaultA('PartLife', 'Unable to load MDT [Nominals]', FaultStopCycle, ResetFault);
    Exit;
  end;

  try
    P:= ProdPath + '\' + N + '.csv';
    if FileExists(P) then begin
      if Table.ImportFromCSV(P, False) <> AAR_OK then begin
        FID:= Named.SetFaultA('PartLife', 'Unable to load MDT [Nominals] from file ' + P, FaultStopCycle, ResetFault);
      end else begin
        Named.EventLog('PartLife.Start - Loaded history file: ' + P);
      end;
      Exit;
    end;

    Named.GetAsString(OCT.OCTManager.Tags[otPartLifeSourcePath], Buffer, 255);
    Conf:= Buffer;

    if Conf = '' then begin
      Named.GetAsString(OCT.OCTManager.Tags[otLoadedFilename], Buffer, 1023);
      P:= Buffer;
      P:= SysUtils.ExtractFilePath(P);
    end else begin
      P:= Conf;
    end;

    P:= P + '\' + T + '.csv';

    if not FileExists(P) then begin
      FID:= Named.SetFaultA('PartLife', 'Nominals file does not exist ' + P, FaultStopCycle, ResetFault);
      Exit;
    end;

    if Table.ImportFromCSV(P, False) <> AAR_OK then begin
      FID:= Named.SetFaultA('PartLife', 'Unable to load MDT [Nominals] from file ' + P, FaultStopCycle, ResetFault);
    end;
    Named.EventLog('PartLife.Start - Loaded virgin file: ' + P);

    {
  OpNumber = 'opp_index';
  FeatureName = 'feature_name';
  Status = 'status';
  ToolSN = 'tool_sn';
  Machine = 'machine';
  Rework = 'rework';

  NOTE: not currently checking Machine, ToolSN and OpNumber as there not required for version
  1 funcionallity
  }

    if not CheckFieldName(Table, FeatureName) then begin
      FID:= Named.SetFaultA('PartLife', 'Required Field name not present: ' + FeatureName, FaultStopCycle, ResetFault);
      Exit;
    end;

    if not CheckFieldName(Table, Status) then begin
      FID:= Named.SetFaultA('PartLife', 'Required Field name not present: ' + Status, FaultStopCycle, ResetFault);
      Exit;
    end;

    if not CheckFieldName(Table, Rework) then begin
      FID:= Named.SetFaultA('PartLife', 'Required Field name not present: ' + Rework, FaultStopCycle, ResetFault);
      Exit;
    end;


  finally
    StringArraySet.ReleaseArray(nil, Table);
    Table:= nil;
  end
end;

{ TPartLifePrepare }

function TPartLifePrepare.Name: WideString;
begin
  Result:= 'Prepare';
end;

function TPartLifePrepare.ParameterCount: Integer;
begin
  Result:= 3;
end;

function TPartLifePrepare.ParameterName(Index: Integer): WideString;
begin
  case Index of
    0: Result:= 'MDTIndex';
    1: Result:= 'NewStatus';
    2: Result:= 'PreviousStatus';
  else
    Result:= 'Unknown';
  end;
end;

procedure TPartLifePrepare.Execute(const Res: Pointer;
  const Params: array of Pointer; ParamCount: Integer);

var Table: IAcnc32StringArray;
    CurrentStatus: WideString;
    PriorStatus: Integer;
    Trans: TTranslator;
begin
  if not NC.DryRun then begin
    CurrentIndex:= Named.GetAsInteger(Params[0]);
    PrepareStatus:= Named.GetAsInteger(Params[1]);
    PriorStatus:= Named.GetAsInteger(Params[2]);

    if StringArraySet.UseArray(PartNominalsTable, nil, Table) <> AAR_OK then begin
      FID:= Named.SetFaultA('PartLife', 'Unable to load MDT [Nominals]', FaultStopCycle, ResetFault);
      Exit;
    end;

    Table.GetFieldValue( CurrentIndex, 'status', CurrentStatus);
    if PriorStatus <> -1 then begin
      if StrToIntDef(CurrentStatus, 0) <> PriorStatus then begin
        // Attempting to cut a hole that has already started. ????
        Trans:= TTranslator.Create;
        try
          FID:= Named.SetFaultA('PartLife', Trans.GetString('$PARTLIFE_CUT_HOLE_ALREADY_STARTED'), FaultStopCycle, ResetFault);
        finally
          Trans.Free;
        end;
      end;
    end;

    Table.SetFieldValue(CurrentIndex, 'Opp_Index', IntToStr(OppHeader.OPPHeaderConsumer.OPPFile.OpNumber));
    ExportTable(Table);
    StringArraySet.ReleaseArray(nil, Table);
    PartLife.WaitingEvent:= True;
  end;
end;

{ TPartLifeComplete }

function TPartLifeComplete.Name: WideString;
begin
  Result:= 'Complete';
end;

function TPartLifeComplete.ParameterCount: Integer;
begin
  Result:= 2;
end;

function TPartLifeComplete.ParameterName(Index: Integer): WideString;
begin
  case Index of
  0: Result:= 'MDTIndex';
  1: Result:= 'Status';
  end;
  Result:= 'Unknown';
end;

procedure TPartLifeComplete.Execute(const Res: Pointer;
  const Params: array of Pointer; ParamCount: Integer);
var SN: Integer;
    Table: IAcnc32StringArray;
    MDTIndex, NewStatus: Integer;
    Buffer: array[0..255] of Char;
begin
  MDTIndex:= Named.GetASInteger(Params[0]);
  NewStatus:= Named.GetASInteger(Params[1]);
  LastMDTIndex:= MDTIndex;

  if not NC.DryRun then begin
    if StringArraySet.UseArray(PartNominalsTable, nil, Table) <> AAR_OK then begin
      FID:= Named.SetFaultA('PartLife', 'Unable to use MDT [Nominals]', FaultStopCycle, ResetFault);
      Exit;
    end;

    ToolLife.GetEx1ToolSN(0, SN);
    Named.GetAsString(OCT.OCTManager.Tags[otMachineID], Buffer, 255);

    Table.SetFieldValue(MDTIndex, OpNumber, IntToStr(OppHeader.OPPHeaderConsumer.OPPFile.OpNumber));
    Table.SetFieldValue(MDTIndex, Status, IntToStr(NewStatus));
    Table.SetFieldValue(MDTIndex, ToolSN, IntToStr(SN));
    Table.SetFieldValue(MDTIndex, Machine, Buffer);

    ExportTable(Table);
    StringArraySet.ReleaseArray(nil, Table);
  end;
end;

{ TPartLifeRework }

function TPartLifeRework.Name: WideString;
begin
  Result:= 'Rework';
end;

function TPartLifeRework.ParameterCount: Integer;
begin
  Result:= 1;
end;

function TPartLifeRework.ParameterName(Index: Integer): WideString;
begin
  Result:= 'MDTIndex';
end;

procedure TPartLifeRework.Execute(const Res: Pointer;
  const Params: array of Pointer; ParamCount: Integer);
var Table: IAcnc32StringArray;
    MDTIndex: Integer;
    Tmp: WideString;
    Trans: TTranslator;
begin
  if not NC.DryRun then begin
    MDTIndex:= Named.GetASInteger(Params[0]);

    if StringArraySet.UseArray(PartNominalsTable, nil, Table) <> AAR_OK then begin
      FID:= Named.SetFaultA('PartLife', 'Unable to use MDT [Nominals]', FaultStopCycle, ResetFault);
      Exit;
    end;

    Table.GetFieldValue(MDTIndex, Status, Tmp);
    if StrToIntDef(Tmp, 0) = 0 then begin
      Trans:= TTranslator.Create;
      try
        FID:= Named.SetFaultA('PartLife', Trans.GetString('$PARTLIFE_REWORK_VIRGIN'), FaultStopCycle, ResetFault);
      finally
        Trans.Free;
      end;
    end;

    Table.GetFieldValue(MDTIndex, Rework, Tmp);
    Tmp:= IntToStr(StrToIntDef(Tmp, 0) + 1);
    Table.SetFieldValue(MDTIndex, Rework, Tmp);

    ExportTable(Table);
    StringArraySet.ReleaseArray(nil, Table);
  end;
end;

{ TPartLifePartEditor }

function TPartLifePartEditor.Name: WideString;
begin
  Result:= 'PartEdit';
end;

function TPartLifePartEditor.ParameterCount: Integer;
begin
  Result:= 0;
end;

procedure TPartLifePartEditor.Execute(const Res: Pointer;
  const Params: array of Pointer; ParamCount: Integer);
var Table: IAcnc32StringArray;
    Tag: TTagRef;
begin
  Tag:= Named.AquireTag('NC1OperatorDialogue', 'TIntegerTag', nil);
  Named.SetAsInteger(Tag, 1);
  try
    if StringArraySet.UseArray(PartNominalsTable, nil, Table) <> AAR_OK then begin
      FID:= Named.SetFaultA('PartLife', 'Unable to use MDT [Nominals]', FaultStopCycle, ResetFault);
      Exit;
    end;
    PartLifeEditor.TPartLifeEditorForm.Execute(Table, LastMDTIndex);
    ExportTable(Table);
  finally
    Named.SetAsInteger(Tag, 0);
    StringArraySet.ReleaseArray(nil, Table);
    Table:= nil;
  end;
end;

procedure ExportTable(Table: IAcnc32StringArray);
var ProdPath: string;
    Tick: Cardinal;
begin
  ProdPath:= GetCSVFile;

  if CurrentPartName <> '' then begin
    Named.EventLog('Starting Export :' + ProdPath);
    Tick:= Windows.GetTickCount;
    Table.ExportToCSV(ProdPath);
    Tick:= Windows.GetTickCount - Tick;
    Named.EventLog('Export Complete: ' + IntToStr(Tick));
  end;
end;

{ TPartLifeFlush }

function TPartLifeFlush.Name: WideString;
begin
  Result:= 'Flush';
end;

function TPartLifeFlush.ParameterCount: Integer;
begin
  Result:= 0;
end;

function TPartLifeFlush.ParameterName(Index: Integer): WideString;
begin
  Result:= 'ErrorNoParameters';
end;

procedure TPartLifeFlush.Execute(const Res: Pointer;
  const Params: array of Pointer; ParamCount: Integer);
var Table: IAcnc32StringArray;
begin
  if StringArraySet.UseArray(PartNominalsTable, nil, Table) <> AAR_OK then begin
    FID:= Named.SetFaultA('PartLife', 'Unable to use MDT [Nominals]', FaultStopCycle, ResetFault);
    Exit;
  end;

  ExportTable(Table);
  StringArraySet.ReleaseArray(nil, Table);
end;

{ TPartLifePartExists }

function TPartLifePartExists.Name: WideString;
begin
  Result:= 'PartExists';
end;

procedure TPartLifePartExists.Execute(const Res: Pointer;
  const Params: array of Pointer; ParamCount: Integer);
begin
  Named.SetAsBoolean(Res, FileExists(GetCSVFile));
end;

function TPartLifePartExists.IsFunction: Boolean;
begin
  Result:= True;
end;

{ TPartLifePartComplete }

function TPartLifePartComplete.Name: WideString;
begin
  Result:= 'IsComplete';
end;

function TPartLifePartComplete.ParameterCount: Integer;
begin
  Result:= 3;
end;

function TPartLifePartComplete.IsFunction: Boolean;
begin
  Result:= True;
end;

procedure TPartLifePartComplete.Execute(const Res: Pointer;
  const Params: array of Pointer; ParamCount: Integer);
var C, T, S, E, I: Integer;
    Table: IAcnc32StringArray;
    Tmp: WideString;
begin
  if StringArraySet.UseArray(PartNominalsTable, nil, Table) <> AAR_OK then begin
    FID:= Named.SetFaultA('PartLife', 'Unable to use MDT [Nominals]', FaultStopCycle, ResetFault);
    Exit;
  end;

  T:= Named.GetAsInteger(Params[0]);
  S:= Named.GetAsInteger(Params[1]);
  E:= Named.GetAsInteger(Params[2]);

  if E = -1 then
    E:= Table.RecordCount - 1;

  for I:= S to E do begin
    Table.GetFieldValue( CurrentIndex, 'status', Tmp);
    C:= StrToIntDef(Tmp, 0);
    if (C <> T) and (C >= 0) then begin
      Named.SetAsBoolean(Res, False);
      Exit;
    end;
  end;

  Named.SetAsBoolean(Res, True);
end;

{ TPartLifeSetProperty }

function TPartLifeSetProperty.Name: WideString;
begin
  Result:= 'SetProperty';
end;

function TPartLifeSetProperty.ParameterCount: Integer;
begin
  Result:= 2;
end;

procedure TPartLifeSetProperty.Execute(const Res: Pointer;
  const Params: array of Pointer; ParamCount: Integer);
var Ini: TIniFile;
    P, V: string;
    Buffer: array [0..255] of Char;
begin
  Named.GetAsString(Params[0], Buffer, 255);
  P:= Buffer;
  Named.GetAsString(Params[1], Buffer, 255);
  V:= Buffer;

  Ini:= TIniFile.Create( GetCSVPath + '\' + CurrentPartName + '.ini' );
  try
    Ini.WriteString('Part', P, V);
  finally
    Ini.Free;
  end;
end;

{ TPartLifeGetProperty }

function TPartLifeGetProperty.Name: WideString;
begin
  Result:= 'GetProperty';
end;

function TPartLifeGetProperty.ParameterCount: Integer;
begin
  Result:= 1;
end;

function TPartLifeGetProperty.IsFunction: Boolean;
begin
  Result:= True;
end;

procedure TPartLifeGetProperty.Execute(const Res: Pointer;
  const Params: array of Pointer; ParamCount: Integer);
var Ini: TIniFile;
    P, V: string;
    Buffer: array [0..255] of Char;
begin
  Named.GetAsString(Params[0], Buffer, 255);
  P:= Buffer;

  Ini:= TIniFile.Create( GetCSVPath + '\' + CurrentPartName + '.ini' );
  try
    V:= Ini.ReadString('Part', P, V);
    Named.SetAsString(Res, PChar(V));
  finally
    Ini.Free;
  end;
end;

{ TPartLifeEnd }

function TPartLifeEnd.Name: WideString;
begin
  Result:= 'End';
end;

procedure TPartLifeEnd.Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer);
var Table: IAcnc32StringArray;
begin
  if StringArraySet.UseArray(PartNominalsTable, nil, Table) <> AAR_OK then begin
    FID:= Named.SetFaultA('PartLife', 'Unable to use MDT [Nominals]', FaultStopCycle, ResetFault);
    Exit;
  end;

  ExportTable(Table);
  Table.Clear;
  StringArraySet.ReleaseArray(nil, Table);

  CurrentPartName:= '';
  CurrentPartType:= '';
  WaitingEvent:= False;
end;

{ TPartLifeReadName }

function TPartLifeReadName.Name: WideString;
begin
  Result:= 'GetName';
end;

function TPartLifeReadName.IsFunction: Boolean;
begin
  Result:= True;
end;

procedure TPartLifeReadName.Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer);
var Tag: TTagRef;
begin
  Tag:= Named.AquireTag('NC1OperatorDialogue', 'TIntegerTag', nil);
  FilenameDlg:= TFilenameDlg.Create(Application);
  Named.SetAsInteger(Tag, 1);
  try
    CurrentPartname:= FilenameDlg.Execute;

    Named.SetAsString(Res, PChar(CurrentPartName));

  finally
    Named.SetAsInteger(Tag, 0);
    FilenameDlg.Free;
    FilenameDlg:= nil;
  end;
end;

procedure TPartLifeReadName.Reset;
begin
  if Assigned(FilenameDlg) then
    FilenameDlg.Close;
end;

initialization
  FeatureName:= 'feature';
  TDefaultScriptLibrary.SetName('PartLife');
  TDefaultScriptLibrary.AddMethod(TPartLifeStart);
  TDefaultScriptLibrary.AddMethod(TPartLifePrepare);
  TDefaultScriptLibrary.AddMethod(TPartLifeComplete);
  TDefaultScriptLibrary.AddMethod(TPartLifeRework);
  TDefaultScriptLibrary.AddMethod(TPartLifePartEditor);
  TDefaultScriptLibrary.AddMethod(TPartLifeFlush);
  TDefaultScriptLibrary.AddMethod(TPartLifePartComplete);
  TDefaultScriptLibrary.AddMethod(TPartLifePartExists);
  TDefaultScriptLibrary.AddMethod(TPartLifeSetProperty);
  TDefaultScriptLibrary.AddMethod(TPartLifeGetProperty);
  TDefaultScriptLibrary.AddMethod(TPartLifeEnd);
  TDefaultScriptLibrary.AddMethod(TPartLifeReadName);
end.
