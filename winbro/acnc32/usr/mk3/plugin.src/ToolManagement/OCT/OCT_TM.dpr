library OCT_TM;

{%File '..\..\..\ACNC32\Profile\cnc.ini'}
{%File 'ModelSupport\RequiredToolData\RequiredToolData.txvpck'}
{%File 'ModelSupport\OCTData\OCTData.txvpck'}
{%File 'ModelSupport\OCT\OCT.txvpck'}
{%File 'ModelSupport\OCTUtils\OCTUtils.txvpck'}
{%File 'ModelSupport\OCTDataWrapper\OCTDataWrapper.txvpck'}
{%File 'ModelSupport\ToolLife\ToolLife.txvpck'}
{%File 'ModelSupport\ToolData\ToolData.txvpck'}
{%File 'ModelSupport\OCTTypes\OCTTypes.txvpck'}
{%File 'ModelSupport\PartData\PartData.txvpck'}
{%File 'ModelSupport\default.txvpck'}

uses
  SysUtils,
  Classes,
  OCT in 'OCT.pas',
  ToolLife in 'ToolLife.pas',
  PartData in '..\lib\PartData.pas',
  ToolData in '..\lib\ToolData.pas',
  OCTData in '..\lib\OCTData.pas',
  OCTTypes in '..\lib\OCTTypes.pas',
  OCTUtils in '..\lib\OCTUtils.pas',
  RequiredToolData in '..\lib\RequiredToolData.pas',
  OCTDataWrapper in 'OCTDataWrapper.pas',
  PartLife in 'PartLife.pas',
  PartLifeEditor in 'PartLifeEditor.pas' {PartLifeEditorForm},
  FilenameDialogue in 'FilenameDialogue.pas' {FilenameDlg};

{$R *.RES}

begin
end.
 