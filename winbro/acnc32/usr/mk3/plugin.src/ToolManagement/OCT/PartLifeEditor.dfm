object PartLifeEditorForm: TPartLifeEditorForm
  Left = 0
  Top = 0
  Width = 729
  Height = 527
  Caption = 'Rework Feature'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -19
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 23
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 721
    Height = 129
    Align = alTop
    BevelInner = bvLowered
    TabOrder = 0
    object UpBtn: TThreeLineButton
      Left = 8
      Top = 24
      Width = 100
      Height = 50
      Legend = 'Up'
      TextSize = 0
      HasIndicator = False
      IndicatorOn = False
      IsEnabled = True
      ButtonColor = clWhite
      Layout = slGlyphLeft
      AutoTextSize = True
      KeyStyle = ssRectangle
      CornerRad = 10
      ButtonTravel = 2
      ShadowColor = clGray
      LegendFont.Charset = ANSI_CHARSET
      LegendFont.Color = clBlue
      LegendFont.Height = 40
      LegendFont.Name = 'Verdana'
      LegendFont.Style = []
      GlyphToEdgeSpacing = 1
      TextToEdgeBorder = 5
      MultiLineSpacing = 0
      Flash = False
      FlashOnTime = 1500
      FlashOffTime = 500
      MaxFontSize = 30
      OnMouseUp = BtnMouseUp
    end
    object DownBtn: TThreeLineButton
      Left = 112
      Top = 24
      Width = 100
      Height = 50
      Legend = 'Down'
      TextSize = 0
      HasIndicator = False
      IndicatorOn = False
      IsEnabled = True
      ButtonColor = clWhite
      Layout = slGlyphLeft
      AutoTextSize = True
      KeyStyle = ssRectangle
      CornerRad = 10
      ButtonTravel = 2
      ShadowColor = clGray
      LegendFont.Charset = ANSI_CHARSET
      LegendFont.Color = clBlue
      LegendFont.Height = 40
      LegendFont.Name = 'Verdana'
      LegendFont.Style = []
      GlyphToEdgeSpacing = 1
      TextToEdgeBorder = 5
      MultiLineSpacing = 0
      Flash = False
      FlashOnTime = 1500
      FlashOffTime = 500
      MaxFontSize = 30
      OnMouseUp = BtnMouseUp
    end
    object GroupBox1: TGroupBox
      Left = 216
      Top = 0
      Width = 353
      Height = 81
      Caption = 'Status'
      TabOrder = 2
      object CompleteBtn: TThreeLineButton
        Left = 8
        Top = 24
        Width = 100
        Height = 50
        Legend = 'Mark~Complete'
        TextSize = 0
        HasIndicator = False
        IndicatorOn = False
        IsEnabled = True
        ButtonColor = clWhite
        Layout = slGlyphLeft
        AutoTextSize = True
        KeyStyle = ssRectangle
        CornerRad = 10
        ButtonTravel = 2
        ShadowColor = clGray
        LegendFont.Charset = ANSI_CHARSET
        LegendFont.Color = clBlue
        LegendFont.Height = 20
        LegendFont.Name = 'Verdana'
        LegendFont.Style = []
        GlyphToEdgeSpacing = 1
        TextToEdgeBorder = 5
        MultiLineSpacing = 0
        Flash = False
        FlashOnTime = 1500
        FlashOffTime = 500
        MaxFontSize = 30
        OnMouseUp = BtnMouseUp
      end
      object ReworkBtn: TThreeLineButton
        Left = 120
        Top = 24
        Width = 100
        Height = 50
        Legend = 'Rework~Required'
        TextSize = 0
        HasIndicator = False
        IndicatorOn = False
        IsEnabled = True
        ButtonColor = clWhite
        Layout = slGlyphLeft
        AutoTextSize = True
        KeyStyle = ssRectangle
        CornerRad = 10
        ButtonTravel = 2
        ShadowColor = clGray
        LegendFont.Charset = ANSI_CHARSET
        LegendFont.Color = clBlue
        LegendFont.Height = 20
        LegendFont.Name = 'Verdana'
        LegendFont.Style = []
        GlyphToEdgeSpacing = 1
        TextToEdgeBorder = 5
        MultiLineSpacing = 0
        Flash = False
        FlashOnTime = 1500
        FlashOffTime = 500
        MaxFontSize = 30
        OnMouseUp = BtnMouseUp
      end
      object VirginBtn: TThreeLineButton
        Left = 232
        Top = 24
        Width = 100
        Height = 50
        Legend = 'Mark~Virgin'
        TextSize = 0
        HasIndicator = False
        IndicatorOn = False
        IsEnabled = True
        ButtonColor = clWhite
        Layout = slGlyphLeft
        AutoTextSize = True
        KeyStyle = ssRectangle
        CornerRad = 10
        ButtonTravel = 2
        ShadowColor = clGray
        LegendFont.Charset = ANSI_CHARSET
        LegendFont.Color = clBlue
        LegendFont.Height = 20
        LegendFont.Name = 'Verdana'
        LegendFont.Style = []
        GlyphToEdgeSpacing = 1
        TextToEdgeBorder = 5
        MultiLineSpacing = 0
        Flash = False
        FlashOnTime = 1500
        FlashOffTime = 500
        MaxFontSize = 30
        OnMouseUp = BtnMouseUp
      end
    end
    object BitBtn1: TBitBtn
      Left = 600
      Top = 8
      Width = 99
      Height = 73
      Cancel = True
      Caption = '&Close'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -19
      Font.Name = 'Tahoma'
      Font.Style = []
      ModalResult = 2
      ParentFont = False
      TabOrder = 3
      Glyph.Data = {
        DE010000424DDE01000000000000760000002800000024000000120000000100
        0400000000006801000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00388888888877
        F7F787F8888888888333333F00004444400888FFF444448888888888F333FF8F
        000033334D5007FFF4333388888888883338888F0000333345D50FFFF4333333
        338F888F3338F33F000033334D5D0FFFF43333333388788F3338F33F00003333
        45D50FEFE4333333338F878F3338F33F000033334D5D0FFFF43333333388788F
        3338F33F0000333345D50FEFE4333333338F878F3338F33F000033334D5D0FFF
        F43333333388788F3338F33F0000333345D50FEFE4333333338F878F3338F33F
        000033334D5D0EFEF43333333388788F3338F33F0000333345D50FEFE4333333
        338F878F3338F33F000033334D5D0EFEF43333333388788F3338F33F00003333
        4444444444333333338F8F8FFFF8F33F00003333333333333333333333888888
        8888333F00003333330000003333333333333FFFFFF3333F00003333330AAAA0
        333333333333888888F3333F00003333330000003333333333338FFFF8F3333F
        0000}
      NumGlyphs = 2
    end
    object PanelCaption: TPanel
      Left = 2
      Top = 86
      Width = 717
      Height = 41
      Align = alBottom
      BevelInner = bvLowered
      Caption = 'PanelCaption'
      TabOrder = 4
    end
  end
end
