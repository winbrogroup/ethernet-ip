unit FilenameDialogue;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls, NamedPlugin;

type
  TFilenameDlg = class(TForm)
    Panel1: TPanel;
    BitBtn1: TBitBtn;
    PartNameComboBox: TComboBox;
    LabelPartName: TLabel;
    ButtonUseCurrent: TButton;
    LabelCurrentPartname: TLabel;
    procedure ButtonUseCurrentClick(Sender: TObject);
  private
  public
    function Execute: string;
  end;

var
  FilenameDlg: TFilenameDlg;

implementation

{$R *.dfm}

uses PartLife;

{ TFilenameDlg }

function TFilenameDlg.Execute: string;
var Trans: TTranslator;
begin
  Trans:= TTranslator.Create;
  try
    LabelPartName.Caption:= Trans.GetString('$ENTER_PART_NAME');
    LabelCurrentPartname.Caption:= PartLife.LastPartName;
    ButtonUseCurrent.Caption:= Trans.GetString('$USE_CURRENT');
    Caption:= Trans.GetString('$PART_NAME_FORM');
    if Trim(PartLife.LastPartName) = '' then begin
      LabelCurrentPartname.Visible:= False;
      ButtonUseCurrent.Visible:= False;
    end;
    Self.ActiveControl:= PartNameComboBox;
    ShowModal;

   finally

    Trans.Free;
  end;
    Result:= PartNameComboBox.Text;
end;

procedure TFilenameDlg.ButtonUseCurrentClick(Sender: TObject);
begin
  PartNameComboBox.Text:= PartLife.LastPartName;
end;


end.
