unit PartLifeEditor;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, Buttons, ComCtrls, NamedPlugin, CommCtrl, IStringArray,
  ACNC_ThreeLineButton, StdCtrls;

type
  TPartLifeEditorForm = class(TForm{, IAcnc32StringArrayNotify})
    Panel1: TPanel;
    UpBtn: TThreeLineButton;
    DownBtn: TThreeLineButton;
    GroupBox1: TGroupBox;
    CompleteBtn: TThreeLineButton;
    ReworkBtn: TThreeLineButton;
    VirginBtn: TThreeLineButton;
    BitBtn1: TBitBtn;
    PanelCaption: TPanel;
    procedure BtnMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FormCreate(Sender: TObject);
  private
    ListView: TListView;
    Table: IAcnc32StringArray;

    OpNumber: Integer;
    Revirginized: Boolean;
    Timer1: TTimer;
    procedure UpdateOPNumber(Sender: TObject);

    procedure CustomDrawItem(Sender: TCustomListView; Item: TListItem; State: TCustomDrawState; var DefaultDraw: Boolean);
    procedure InitialiseData(Table : IACNC32StringArray; OpNumber: Integer);
    procedure UpdateListView(OpNumber: Integer);
  public
    procedure CellChange(Table : IACNC32StringArray; Row, Col : Integer); stdcall;
    procedure TableChange(Table : IACNC32StringArray); stdcall;

    public class procedure Execute(Table: IAcnc32StringArray; OpNumber: Integer);
  end;

implementation

uses PartLife;

{$R *.dfm}

procedure TPartLifeEditorForm.CellChange(Table: IACNC32StringArray; Row,
  Col: Integer);
begin

end;

procedure TPartLifeEditorForm.CustomDrawItem(Sender: TCustomListView;
  Item: TListItem; State: TCustomDrawState; var DefaultDraw: Boolean);
var Widths : array [0..10] of Integer;

  procedure DrawSub(const aRect : TRect; SubItem: Integer);
  var I : Integer;
      Left : Integer;
      Rect : TRect;
  begin
      Widths[SubItem] := ListView_GetColumnWidth(ListView.Handle, SubItem);
      Left := aRect.Left;
      for I := 0 to SubItem - 1 do
        Inc(Left, Widths[I]);

      Rect := Item.DisplayRect(drBounds);
      Rect.Left := Left;
      Rect.Right := Rect.Left + Widths[SubItem];

      ListView.Canvas.TextRect(Rect, Rect.Left + 5, Rect.Top, Item.SubItems[SubItem - 1]);
  end;
var I : Integer;
    Rect : TRect;
begin
  if Item.Selected { cdsSelected in State  } then
    ListView.Canvas.Brush.Color := clYellow
  else if Odd(Item.Index) then
    ListView.Canvas.Brush.Color := $e0e0e0
  else
    ListView.Canvas.Brush.Color := $e8e8e8;

  Widths[0] := ListView_GetColumnWidth(ListView.Handle, 0);
  Rect := Item.DisplayRect(drBounds);
//  Rect.Left := 0;
  Rect.Right := Rect.Left + Widths[0];
  ListView.Canvas.TextRect(Rect, Rect.Left + 5, Rect.Top, Item.Caption);

  for I := 0 to Item.SubItems.Count - 1 do begin
    DrawSub(Rect, I + 1);
  end;
  defaultdraw := False;
end;

class procedure TPartLifeEditorForm.Execute(Table: IAcnc32StringArray; OpNumber: Integer);
var Form: TPartLifeEditorForm;
begin
  Form:= TPartLifeEditorForm.Create(Application.MainForm);
  Form.Table:= Table;
  Form.OpNumber:= OpNumber;
  Form.InitialiseData(Table, OpNumber);
  Form.Caption:= PartLife.CurrentPartType + ' - ' + PartLife.CurrentPartName;
  Form.PanelCaption.Caption:= Form.Caption;
  Form.ShowModal;
  Form.Table:= nil;
  Form.Free;
end;

procedure TPartLifeEditorForm.FormCreate(Sender: TObject);
var UpLang, DownLang: string;
    lc : TListColumn;
    Trans: TTranslator;
begin
  Trans:= TTranslator.Create;

  UpLang:= Trans.GetString('$UP');
  DownLang:= Trans.GetString('$DOWN');

  ListView := TListView.Create(Self);
  ListView.Parent := Self;
  ListView.Align := alClient;
  ListView.ViewStyle := vsReport;
  ListView.HideSelection := False;
  ListView.GridLines := True;
  ListView.OnCustomDrawItem := CustomDrawItem;
  ListView.ReadOnly := True;
  ListView.Font.Size:= 14;
  ListView.RowSelect:= True;

  lc := ListView.Columns.Add;
  lc.Caption := Trans.GetString('$FEATURE');
  lc.Width := 350;

  lc := ListView.Columns.Add;
  lc.Caption := Trans.GetString('$STATUS');
  lc.Width := 300;

  //Timer used is a one shot so that the form updates the OPName
  Timer1:= TTimer.Create(Self);
  Timer1.OnTimer:= UpdateOPNumber;
  Timer1.Interval:= 1000;
  Timer1.Enabled:= True;
end;

procedure TPartLifeEditorForm.TableChange(Table: IACNC32StringArray);
begin

end;

procedure TPartLifeEditorForm.InitialiseData(Table : IACNC32StringArray; OpNumber: Integer);
var I: Integer;
    LI: TListItem;
    Tmp: WideString;
begin
  ListView.Clear;

  Table.BeginUpdate;
  try
    for I:= 0 to Table.RecordCount - 1 do begin

      LI := ListView.Items.Add;
      Table.GetFieldValue(I, PartLife.FeatureName, Tmp);
      LI.Caption:= Tmp;

      LI.SubItems.Add('-');
    end;

    UpdateListView(OpNumber);
  finally
    Table.EndUpdate{(Self)};
  end;
    //ListView.Items[OpNumber].MakeVisible(False);
    //ListView.Scroll(0, -ListView.Items.Count * 10);
    //ListView.RowSelect(OpNumber);

end;


procedure TPartLifeEditorForm.UpdateListView(OpNumber: Integer);
var I, Status, Rework: Integer;
    LI: TListItem;
    Tmp: WideString;
begin
  Table.BeginUpdate;
  try
    for I:= 0 to Table.RecordCount - 1 do begin
      LI := ListView.Items[I];
      Table.GetFieldValue(I, PartLife.FeatureName, Tmp);
      LI.Caption:= Tmp;

      Table.GetFieldValue(I, PartLife.Status, Tmp);
      Status:= StrToIntDef(Tmp, 0);
      Table.GetFieldValue(I, PartLife.Rework, Tmp);
      Rework:= StrToIntDef(Tmp, 0);

      if (Status = 0) and (Rework = 0) then
        LI.SubItems[0]:= '-'
      else if Status = 1 then
        LI.SubItems[0]:= 'Requires rework: ' + IntToStr(Rework)
      else if (Status >= 2) and (Rework = 0) then
        LI.SubItems[0]:= 'Complete'
      else if Status >= 2 then
        LI.SubItems[0]:= 'Complete: ' + IntToStr(Rework)
      else if Status = 0 then
        LI.SubItems[0]:= 'Reworked but not machined? ' + IntToStr(Rework);
    end;
  finally
    Table.EndUpdate{(Self)};
  end;
end;

procedure TPartLifeEditorForm.BtnMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var Tmp: WideString;
    //I: Integer;
begin
  if (Sender = UpBtn) then begin
    if Assigned(ListView.Selected) then begin
      if ListView.Selected.Index > 0 then
        ListView.Selected := ListView.Items[ListView.Selected.Index - 1];
    end else begin
      ListView.Selected := ListView.Items[0];
    end;

    //ListView.TopItem:= ListView.Selected;
    Exit;
  end;

  if (Sender = DownBtn) then begin
    if Assigned(ListView.Selected) then begin
      if ListView.Selected.Index < ListView.Items.Count - 1 then
        ListView.Selected := ListView.Items[ListView.Selected.Index + 1];
    end else begin
      ListView.Selected := ListView.Items[0];
    end;
    Exit;
  end;

  if Assigned(ListView.Selected) then begin
    if Sender = CompleteBtn then begin
      //Tmp:= ListView.Selected.Caption;
      //if Table.FindRecordByField(0, PartLife.FeatureName, Tmp, I) = AAR_OK then begin
        Table.SetFieldValue(ListView.Selected.Index, PartLife.Status, '2');
      //end;
    end;
    if Sender = ReworkBtn then begin
      //Tmp:= ListView.Selected.Caption;
      //if Table.FindRecordByField(0, PartLife.FeatureName, Tmp, I) = AAR_OK then begin
        Table.SetFieldValue(ListView.Selected.Index, PartLife.Status, '1');
      //end;
    end;
    if Sender = VirginBtn then begin
      //Tmp:= ListView.Selected.Caption;
      //if Table.FindRecordByField(0, PartLife.FeatureName, Tmp, I) = AAR_OK then begin

        Table.GetFieldValue(ListView.Selected.Index, PartLife.Status, Tmp);
        if StrToIntDef(Tmp, 0) <> 0 then begin
          if MessageDlg('Incorrectly marking a part as virgin may scrap the part \n are you sure you want to continue?', mtConfirmation, [mbCancel,mbOK], 0) <> mrOK then begin
            Exit;
          end;
        end;

        Table.GetFieldValue(ListView.Selected.Index, PartLife.ManualIntervention, Tmp);
        Tmp:= IntToStr(StrToIntDef(Tmp, 0) + 1);
        Table.SetFieldValue(ListView.Selected.Index, PartLife.ManualIntervention, Tmp);
        Table.SetFieldValue(ListView.Selected.Index, PartLife.Status, '0');
        Revirginized:= True;
      //end;
    end;

    UpdateListView(OpNumber);
  end;
end;

procedure TPartLifeEditorForm.UpdateOPNumber(Sender: TObject);
var TooMany: Integer;

var S: TListItem;
    SR: TRect;

begin
  if (OpNumber > 0) and (OpNumber < ListView.Items.Count) then begin
    ListView.ItemIndex:= OpNumber;
    S:= ListView.Selected;
    SR:= S.DisplayRect(drBounds);

    if SR.Top > ListView.Height div 2 then begin
      ListView.Scroll(0, SR.Top - (ListView.Height div 2));
    end;

    if SR.Top < 0 then
      ListView.Scroll(0, SR.Top);
  end;
  Timer1.Enabled:= False;
  Timer1.Free;
end;

end.

