unit OCTDataWrapper;

interface

uses SysUtils, Classes, OCTData, ToolLife, ToolData;

// This exists to ensure that OCTData does not have to include ToolLife
type
  TOCTDataWrapper = class(TOPPList)
  private
  public
    constructor Create; 
    procedure MarkDependencies;
    procedure Sort;
    function ToolIsStillDependent(AToolGroup : Integer) : Boolean;
  end;

implementation

var ODW : TOCTDataWrapper; // Only one instance, used by sort

constructor TOCTDataWrapper.Create;
begin
  inherited;
  ODW := Self;
end;


function SortOPPs(List: TStringList; Index1, Index2: Integer): Integer;
begin
  // Note: List = ODW.OPPList

  Result := ODW.OPP[Index1].SortScore - ODW.OPP[Index2].SortScore;

  if Result = 0 then
    Result := ODW.OPP[Index1].ToolGroup - ODW.OPP[Index2].ToolGroup;

  if Result = 0 then
    Result := CompareText(ODW.OPP[Index1].FileName, ODW.OPP[Index2].FileName)

end;

// Sorting criteria
// FirstOPP = 10000
// HasDependencies = 1000
// InHead and Capable = 100
// InManifest and Capable = 10
// Incapable = 0
// EndOPP = -10000


// Toolgroups must have been setup
procedure TOCTDataWrapper.MarkDependencies;
var I, J, K : Integer;
begin
  Lock;
  try

    for I := 0 to Count - 1 do begin
      OPP[I].IsDepend := False;
      OPP[I].HasDepend := False;
      OPP[I].IsToolDepend := False;
      OPP[I].HasToolDepend := False;
    end;

    for I := 0 to Count - 1 do begin
      if OPP[I].DependentOn <> '' then begin
        for J := 0 to Count - 1 do begin
          if (CompareText(OPP[I].DependentOn, OPP[J].FileName) = 0) and
             not (OPP[J].Status in CompleteStatus) then begin
            OPP[I].IsDepend := True;
            OPP[J].HasDepend := True;

            if OPP[I].ToolGroup <> OPP[J].ToolGroup then begin
              for K := 0 to Count - 1 do begin
                if OPP[K].ToolGroup = OPP[I].ToolGroup then
                  OPP[K].IsToolDepend := True;
                if OPP[K].ToolGroup = OPP[J].ToolGroup then
                  OPP[K].HasToolDepend := True;
              end;
            end;
          end;
        end;
      end;
    end;
  finally
    Unlock;
  end;
end;


procedure TOCTDataWrapper.Sort;
var Available : Double;
    I : Integer;
    ToolInHead : WideString;
begin
  Lock;
  try
    MarkDependencies;

    ToolLifeInstance.GetEx1ToolType(0, ToolInHead);
    for I := 0 to Count - 1 do begin
      OPP[I].SortScore := OPP[I].NaturalOrder;

      if OPP[I].FixedStart then
        OPP[I].SortScore := OPP[I].SortScore - 10000;

      if OPP[I].HasDepend and not OPP[I].IsDepend then
        OPP[I].SortScore := OPP[I].SortScore - 90;

      if OPP[I].HasToolDepend then
        OPP[I].SortScore := OPP[I].SortScore -10;

      if OPP[I].IsDepend then
        OPP[I].SortScore := OPP[I].SortScore; //1100;

      if OPP[I].IsToolDepend then
        OPP[I].SortScore := OPP[I].SortScore + 10;

      if OPP[I].FixedEnd then
        OPP[I].SortScore := 10000;

      Available := ToolLifeInstance.GetTotalAvailable(OPP[I].ToolType);
      if (CompareText(ToolInHead, OPP[I].ToolType) = 0) and
         (Available >= OPP[I].ToolUsage) then begin
        OPP[I].SortScore := OPP[I].SortScore - 100;
      end else if Available > OPP[I].ToolUsage then begin
        OPP[I].SortScore := OPP[I].SortScore - 10;

      end else if Available < OPP[I].ToolUsage then begin
        OPP[I].SortScore := OPP[I].SortScore + 100;
      end;

      if ODW.OPP[I].Status in CompleteStatus then
        OPP[I].SortScore := -1000000 + I
    end;

    OPPList.CustomSort(SortOPPs);
    RebuildIndex;
  finally
    Unlock;
  end;
end;


function TOCTDataWrapper.ToolIsStillDependent(AToolGroup: Integer): Boolean;
var I : Integer;
begin
  Lock;
  try
    for I := 0 to Count - 1 do begin
      if (OPP[I].ToolGroup = AToolGroup) and
         not (OPP[I].Status in CompleteStatus) and
         OPP[I].HasDepend then begin
        Result := True;
        Exit;
      end;
    end;
  finally
    Unlock;
  end;
  Result := False;
end;

end.
