unit ToolLife;

interface

uses Classes, SysUtils, AbstractToolLife, INamedInterface, IToolLife,
     NamedPlugin, StreamTokenizer, SyncObjs, CNCTypes, ToolData,
     OCTTypes, IStringArray;

type
  TToolManifest = class(TAbstractToolLifePlugin)
  private
    ToolLock : TCriticalSection;
    CallbackList : TList;
    ToolList : TToolList;
//    ToolListI : IACNC32StringArrayNotify;
    procedure ToolListChange(Sender : TObject);
    function GetTool(Index: Integer): TToolData;
  public
    constructor Create(Host : IFSDToolLifeHost); override;
    destructor Destroy; override;
    procedure FinalInstall; override;
    procedure Shutdown; override;
    function AddTool(Tool : WideString; SN : Integer; Total, Remaining : Double; Ex1, Ex2 : Integer) : TToolResult; override;
    function LocalAddTool(Tool : WideString; SN : Integer; Total, Remaining : Double; Ex1, Ex2 : Integer) : TToolData;
    function ReplenishTool(Tool : WideString; SN : Integer) : TToolResult; override;
    function RemoveTool(Tool : WideString; SN : Integer) : TToolResult; override;
    function SetRemaining(Tool : WideString; SN : Integer; ToolLife : Double) : TToolResult; override;
    function GetTotal(Tool : WideString; out Total : Double) : TToolResult; override;
    function GetRemaining(Tool : WideString; SN : Integer; out Remaining : Double) : TToolResult; override;
    function GetToolCount : Integer; override;
    function GetToolType(Index : Integer; out ToolType : WideString) : TToolResult; override;
    function GetToolSN(Index : Integer; out SN : Integer) : TToolResult; override;
    function GetEx1ToolType(Ex1 : Integer; out ToolType : WideString) : TToolResult; override;
    function GetEx1ToolSN(Ex1 : Integer; out SN : Integer) : TToolResult; override;
    function ChangeEx1(Ex1Old, Ex1New : Integer) : TToolResult; override;
    function ChangeEx2(Ex1, Ex2New : Integer) : TToolResult; override;
    function GetExt2(Ex1 : Integer; out Ex2 : Integer) : TToolResult; override;
    function GetMinimumTool(Tool : WideString; out Ex1 : Integer) : TToolResult; override;

    // NOT IN TOOLLIFE INTERFACE, YET!
    function GetAvailable(Tool : WideString) : Double; stdcall;

    procedure DoToolLifeChange;
    function Count : Integer;
    property Tool[Index : Integer] : TToolData read GetTool;
    procedure Lock;
    procedure Unlock;
    function FindTool(aName : string; SN : Integer) : TToolData;
    function FindToolEx1(Ex1 : Integer) : TToolData;
    procedure AddCallback(Callback : TNotifyEvent);
    procedure RemoveCallback(Callback : TNotifyEvent);
    procedure Ex1ZeroRemaining(Ex1 : Integer);

    function GetTotalAvailable(Tool : WideString) : Double;
  end;

var
  ToolLifeInstance : TToolManifest;

implementation

constructor TToolManifest.Create(Host : IFSDToolLifeHost);
begin
  inherited;
  ToolLock := TCriticalSection.Create;
  ToolList := TToolList.Create;
  ToolList.Clean;
  ToolList.OnChange := ToolListChange;

  CallbackList := TList.Create;
  ToolLifeInstance := Self;
end;

destructor TToolManifest.Destroy;
begin
  ToolList := nil;
  inherited;
end;

procedure TToolManifest.FinalInstall;
begin
  DoToolLifeChange;
end;

procedure TToolManifest.Shutdown;
begin
  ToolList.Close;
  inherited;
end;

function TToolManifest.FindTool(aName : string; SN : Integer) : TToolData;
var I : Integer;
begin
  for I := 0 to ToolList.Count - 1 do begin
    Result := ToolList[I];
    if (CompareText(Result.ShuttleID, aName) = 0) and
       ((SN = -1) or (SN = Result.ShuttleSN)) then
       Exit;
  end;
  Result := nil;
end;

function TToolManifest.FindToolEx1(Ex1 : Integer) : TToolData;
var I : Integer;
begin
  for I := 0 to ToolList.Count - 1 do begin
    Result := ToolList[I];
    if (Result.Ex1 = Ex1) then
       Exit;
  end;
  Result := nil;
end;

function TToolManifest.GetAvailable(Tool : WideString) : Double; stdcall;
var T : TToolData;
    I : Integer;
begin
  Result := 0;
  for I := 0 to ToolList.Count - 1 do begin
    T := ToolList[I];
    if CompareText(T.ShuttleID, Tool) = 0 then
      Result := Result + T.BarrelRemaining;
  end;
end;

function TToolManifest.GetTotalAvailable(Tool : WideString) : Double;
var T : TToolData;
    I : Integer;
begin
  Result := 0;
  for I := 0 to ToolList.Count - 1 do begin
    T := ToolList[I];
    if CompareText(T.ShuttleID, Tool) = 0 then
      if T.TotalRemaining > 0 then
        Result := Result + T.TotalRemaining;
  end;

  if Result < 0 then
    Result := 0;
end;

function TToolManifest.GetMinimumTool(Tool : WideString; out Ex1 : Integer) : TToolResult; stdcall;
var T : TToolData;
    Life : Double;
    Found : Boolean;
    ToolNumber : Integer;
    FoundEx1 : Integer;
begin
  Result := trOk;
  Ex1 := -1;
  FoundEX1 := -1;
  Found := False;
  // here to return -ex2 for tool of type 'Tool' with lowest tool life
  ToolNumber := 0;
  Life := 1000000000;
  while ToolNumber < ToolList.Count do begin
    try
      T := ToolList[ToolNumber];
      if T.ShuttleID = Tool then begin
        if (T.TotalRemaining < Life) and (T.TotalRemaining <> 0) then begin
           Found := True;
           Life := T.TotalRemaining;
           FoundEX1 := T.Ex1;
         end;
       end
    finally
      Inc(ToolNumber);
    end
  end;

  if Found then
     Ex1 := FoundEX1
  else
     //if found but not on toolchanger (ie om head) then return not on toolchanger
     Result := trNotFound;
end;

function TToolManifest.AddTool(Tool : WideString; SN : Integer;
   Total, Remaining : Double; Ex1, Ex2 : Integer) : TToolResult; stdcall;
begin
  if LocalAddTool(Tool, SN, Total, Remaining, Ex1, Ex2) <> nil then
    Result := trOK
  else
    Result := trNotFound;
end;

function TToolManifest.LocalAddTool(Tool : WideString; SN : Integer; Total, Remaining : Double; Ex1, Ex2 : Integer) : TToolData;
begin
  ToolLock.Enter;
  try
    if FindTool(Tool, SN) = nil then begin
      ToolList.BeginUpdate;
      try
        Result := ToolList.AddTool;
        Result.ShuttleID := Tool;
        Result.ShuttleSN := SN;
        Result.MaxToolLength := Total;
        Result.ActToolLength := Remaining;
        Result.MinToolLength := 0;
        Result.BarrelCount := 1;
        Result.RemainingBarrels := 0;
        Result.Ex1 := Ex1;
        Result.Ex2 := Ex2;
        Result.UniqueToolOffsets[0] := 0;
        Result.UniqueToolOffsets[1] := 0;
        Result.UniqueToolOffsets[2] := 0;
        Result.RebuildTime := Now + 33;
        Result.Expiry := Now + 33;
        Result.StatusFlags := 0;
      finally
        ToolList.EndUpdate;
      end;
    end else begin
      Result := nil;
    end
  finally
    ToolLock.Release;
  end;
  DoToolLifeChange;
end;

function TToolManifest.GetToolCount: Integer;
begin
  ToolLock.Enter;
  try
    Result := ToolList.Count;
  finally
    ToolLock.Release;
  end;
end;

function TToolManifest.GetTotal(Tool : WideString; out Total : Double) : TToolResult; stdcall;
var PT : TToolData;
begin
  Result := trOK;
  ToolLock.Enter;
  try
    PT := FindTool(Tool, -1);
    if PT = nil then
      Result := trNotFound
    else
      Total := PT.MaxToolLength;
  finally
    ToolLock.Release;
  end;
end;


function TToolManifest.GetRemaining(Tool : WideString; SN : Integer; out Remaining : Double) : TToolResult; stdcall;
var PT : TToolData;
begin
  Result := trOK;
  ToolLock.Enter;
  try
    PT := FindTool(Tool, SN);
    if PT = nil then
      Result := trNotFound
    else
      Remaining := PT.BarrelRemaining;
  finally
    ToolLock.Release;
  end;
end;

function TToolManifest.GetToolType(Index : Integer; out ToolType : WideString) : TToolResult; stdcall;
begin
  Result := trOK;
  ToolLock.Enter;
  try
    if Index < ToolList.Count then begin
      ToolType := ToolList[Index].ShuttleID;
    end else begin
      ToolType := 'Invalid Index';
      Result := trNotFound;
    end;
  finally
    ToolLock.Release;
  end;
end;

function TToolManifest.GetToolSN(Index : Integer; out SN : Integer) : TToolResult;
begin
  Result := trOK;
  ToolLock.Enter;
  try
    if Index < ToolList.Count then begin
      SN := ToolList[Index].ShuttleSN;
    end else begin
      SN := -1;
      Result := trNotFound;
    end;
  finally
    ToolLock.Release;
  end;
end;

function TToolManifest.ReplenishTool(Tool: WideString; SN : Integer): TToolResult;
var T : TToolData;
begin
  Result := trOK;
  ToolLock.Enter;
  try
    T := FindTool(Tool, SN);
    if T = nil then begin
      Result := trNotFound
    end else begin
      T.ActToolLength := T.MaxToolLength;
    end;
  finally
    ToolLock.Release;
  end;
  DoToolLifeChange;
end;

function TToolManifest.RemoveTool(Tool: WideString; SN : Integer): TToolResult;
var T : TToolData;
begin
  Result := trOK;
  ToolLock.Enter;
  try
    if CompareText(Tool, 'all') = 0 then begin
       ToolList.Clean;
    end else begin
      T := FindTool(Tool, SN);
      if T = nil then begin
        Result := trNotFound
      end else begin
        ToolList.RemoveTool(T);
      end;
    end;
  finally
    ToolLock.Release;
  end;
  DoToolLifeChange;
end;

function TToolManifest.SetRemaining(Tool: WideString; SN : Integer;
  ToolLife: Double): TToolResult;
var T : TToolData;
begin
  Result := trOK;
  ToolLock.Enter;
  try
    T := FindTool(Tool, SN);
    if T = nil then begin
      Result := trNotFound
    end else begin
      if ToolLife < 0 then
        ToolLife := 0;
      T.ActToolLength := ToolLife + T.MinToolLength;
{      if Assigned(OCTManager) then
        if not OCTManager.ToolUsable(Tool, ToolLife) then
          T.ActToolLength := 0; }
    end;
  finally
    ToolLock.Release;
  end;
  DoToolLifeChange;
end;

function TToolManifest.ChangeEx1(Ex1Old, Ex1New: Integer): TToolResult;
var T : TToolData;
begin
  Result := trOK;
  ToolLock.Enter;
  try
    T := FindToolEx1(Ex1Old);
    if T = nil then
      Result := trNotFound
    else
      T.Ex1 := Ex1New;
  finally
    ToolLock.Release;
  end;
  DoToolLifeChange;
end;

function TToolManifest.ChangeEx2(Ex1, Ex2New: Integer): TToolResult;
var T : TToolData;
begin
  Result := trOK;
  ToolLock.Enter;
  try
    T := FindToolEx1(Ex1);
    if T = nil then
      Result := trNotFound
    else
      T.Ex2 := Ex2New;
  finally
    ToolLock.Release;
  end;
  DoToolLifeChange;
end;

function TToolManifest.GetEx1ToolSN(Ex1: Integer;
  out SN: Integer): TToolResult;
var T : TToolData;
begin
  Result := trOK;
  ToolLock.Enter;
  SN := 0;
  try
    T := FindToolEx1(Ex1);
    if T = nil then
      Result := trNotFound
    else
      SN := T.ShuttleSN;
  finally
    ToolLock.Release;
  end;
end;

function TToolManifest.GetEx1ToolType(Ex1: Integer;
  out ToolType: WideString): TToolResult;
var T : TToolData;
begin
  ToolType := '';
  Result := trOK;
  ToolLock.Enter;
  try
    T := FindToolEx1(Ex1);
    if T = nil then
      Result := trNotFound
    else
      ToolType := T.ShuttleID;
  finally
    ToolLock.Release;
  end;
end;

function TToolManifest.GetExt2(Ex1 : Integer; out Ex2 : Integer) : TToolResult;
var T : TToolData;
begin
  Result := trOK;
  ToolLock.Enter;
  try
    T := FindToolEx1(Ex1);
    if T = nil then
      Result := trNotFound
    else
      Ex2 := T.Ex2;
  finally
    ToolLock.Release;
  end;
end;


procedure TToolManifest.Ex1ZeroRemaining(Ex1 : Integer);
var T : TToolData;
begin
  T := FindToolEx1(Ex1);
  if T <> nil then begin
//    T.ActToolLength := 0;
//    T.RemainingBarrels := 0;
    T.Ex2 := 1;
    DoToolLifeChange;
  end;
end;


procedure TToolManifest.DoToolLifeChange;
var I : Integer;
begin
  Host.ToolLifeChange(Self);
  // Can now expand to multiple callbacks
  for I := 0 to CallbackList.Count - 1 do
    TNotifyEvent(PMethod(CallbackList[I])^)(Self);
end;

procedure TToolManifest.AddCallback(Callback: TNotifyEvent);
var A : PMethod;
begin
  New(A);
  A^ := TMethod(Callback);
  CallbackList.Add(A);
end;

procedure TToolManifest.RemoveCallback(Callback: TNotifyEvent);
var A : PMethod;
    B : TMethod;
    I : Integer;
begin
  B := TMethod(Callback);
  for I := 0 to CallbackList.Count - 1 do begin
    A := CallbackList[I];
    if (A^.Code = B.Code) and (A^.Data = B.Data) then begin
      Dispose(CallbackList[I]);
      CallbackList.Delete(I);
      Exit;
    end;
  end;
end;

function TToolManifest.Count: Integer;
begin
  Result := ToolList.Count;
end;

function TToolManifest.GetTool(Index: Integer): TToolData;
begin
  Result := ToolList[Index];
end;

// Block callback and toollife changes
procedure TToolManifest.Lock;
begin
  ToolLock.Enter;
  ToolList.Lock;
end;

procedure TToolManifest.Unlock;
begin
  ToolList.Unlock;
  ToolLock.Leave;
end;

procedure TToolManifest.ToolListChange(Sender: TObject);
begin
  DoToolLifeChange; //Will this recurse!
end;

initialization
  TToolManifest.AddPlugin('ToolLife', TToolManifest);
end.
