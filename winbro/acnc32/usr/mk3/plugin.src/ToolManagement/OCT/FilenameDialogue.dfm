object FilenameDlg: TFilenameDlg
  Left = 0
  Top = 0
  Width = 601
  Height = 281
  Caption = 'FilenameDlg'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -19
  Font.Name = 'Tahoma'
  Font.Style = [fsBold]
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 23
  object LabelPartName: TLabel
    Left = 9
    Top = 40
    Width = 272
    Height = 33
    Alignment = taRightJustify
    AutoSize = False
    Caption = 'Enter Part Name'
  end
  object LabelCurrentPartname: TLabel
    Left = 248
    Top = 120
    Width = 297
    Height = 23
    AutoSize = False
    Caption = 'Current Part Name'
  end
  object Panel1: TPanel
    Left = 0
    Top = 184
    Width = 593
    Height = 63
    Align = alBottom
    TabOrder = 0
    object BitBtn1: TBitBtn
      Left = 189
      Top = 6
      Width = 108
      Height = 51
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -19
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      Kind = bkOK
    end
  end
  object PartNameComboBox: TComboBox
    Left = 304
    Top = 40
    Width = 257
    Height = 31
    ItemHeight = 23
    TabOrder = 1
  end
  object ButtonUseCurrent: TButton
    Left = 32
    Top = 112
    Width = 177
    Height = 33
    Caption = 'Use Current'
    TabOrder = 2
    OnClick = ButtonUseCurrentClick
  end
end
