unit OCT;
 {OCT operation is to look at the list of OPP used on a part and decide on which is the best way for the tools
  to be used to make sure that the machines are running as long as possible without pausing}
interface

uses Classes, SysUtils, IOCT, IToolLife, NamedPlugin, INamedInterface,
     IniFiles, StreamTokenizer, OCTTypes, AbstractOCTPlugin, CNCTypes,
     OCTDataWrapper, OCTData, OCTUtils, ToolLife, RequiredToolData,
     IStringArray, ToolData, OPPHeader, OpTable, PartData, PartLife;
type
  TOCTRunStage = (
    orsWaitRun,
    orsWaitInCycle,
    orsWaitEndProgram,
    orsCheckTooling,
    orsWaitForTooling,
    orsEndProcessing,
    orsEndDelay
  );

  TOCTTag = (
    otToolInHead,
    otToolSNInHead,
    otToolRemInHead,
    otToolHPInHead,
    otToolingRequired,
    otTotalToolCount,
    otRequiredToolIDString,

    // Added for PartLife
    otLoadedFilename,
    otProcessStarted,
    otMachineID,

    otPartLifeSourcePath,
    otPartLifeOutputPath,
    otPartLifeCompleteCount

  );

  TOCT = class(TAbstractOCTPlugin, IACNC32StringArrayNotify)
  private
    FOCTMode : Boolean;
    FRunEnable : Boolean;
    FOCTRunStage : TOCTRunStage;
    FDurationTimer : TDurationTimer;
    FCycleStartTimer : TDurationTimer;
    FCStartRetry : Boolean;
    Table: IAcnc32StringArray;

    OCTFileName : string;
    FActiveOPP : Integer;
    OCTProcessing : Boolean;
    DisableSorting : Boolean;
    OCTOpTable : TOpTable;
    PartData : TPartData;

    OCTLoadedTag : TTagref;
    OCTLoadedNameTag : TTagref;
    OCTResetRewindTag : TTagRef;
    ResetRewindTag: TTagRef;
    RequiredToolTag : TTagRef;
    PartLifeEditor: TTagRef;

    ToolingFID : FHandle;
    ToolingAvailable : Boolean;

    InRecovery : Boolean;

    FixedEndOPPName : string;
    FixedStartOPPName : string;
    CurrentStatus : TOCTPluginStatii;
    OPPList : TOCTDataWrapper;
    RequiredToolList : TRequiredToolList;
    TagNames : array [TOCTTag] of string;

    CannotLoadActiveText: string; //'The active OPP cannot be run, to recover reload the OCT '
    CannotAutomate_S_SText: string; //'Cannot automate the running of %s as it is dependent on %s'
    NoToolingAvailableText: string; //'No tooling available to select next OPP, Please add a required tool and then press cycle start'
    PleaseAddToolingText: string; // 'Please add required tooling'
    procedure SetRunEnable(aOn : Boolean);
    procedure LoadOCTTable(aFileName : string);
    procedure GenerateRequiredTools;
    procedure UpdateRequiredTools;
    procedure Clean;
    procedure ResetRewind;
    procedure Resume;
    procedure DoOCTChange(Sender : TObject);
    procedure ToolLifeChange(Sender : TObject);
    procedure DetermineEx1RequiredTool(ToolName : WideString; Usage : Double);
    procedure LoadedOPPChange(Sender : TObject);
    procedure ResetTable;
    procedure ReadConfig;
    function ValidOpNumber(OP : Integer) : Boolean;
    procedure UpdatePartData(Sender : TObject);
  protected
    function NextOPP: Boolean; virtual;// Retrurns False if no more
    function FirstOPP: Integer; virtual;
//    procedure LastOPP;
    procedure FirstTimeLoadUpdateRequiredTools;
  public
    Tags : array [TOCTTag] of TTagRef;
    constructor Create(Host : IFSDOCTHost); override;
    destructor Destroy; override;
    procedure LoadOCT(aFileName : WideString); override;
    procedure Install; override;
    procedure Run; override;
    procedure Update(Status : TOCTPluginStatii); override;
    procedure Reset; override;
    procedure FinalInstall; override;
    procedure Shutdown; override;
    function GetToolUsage(Tool : WideString) : Double; override;
    procedure Sort(PreferedTool : WideString); override;
    function GetMinimumToolUsage(Tool : WideString; out Value : Double) : Boolean; override; stdcall;
    property ActiveOPP : Integer read FActiveOPP;
    property RunEnable : Boolean read FRunEnable write SetRunEnable;
    property OCTMode : Boolean read FOCTMode;

    procedure CellChange(Table : IACNC32StringArray; Row, Col : Integer); stdcall;
    procedure TableChange(Table : IACNC32StringArray); stdcall;

  end;

var
  OCTManager : TOCT;

implementation

const
  ValidCycleStart = [ orsWaitInCycle, orsWaitEndProgram ];

  TagDefinitions : array [TOCTTag] of TTagDefinitions = (
    ( Ini: 'ToolInHead'; Tag: 'ToolInHead'; Owned: True; TagType: 'TStringTag' ),
    ( Ini: 'ToolSNInHead'; Tag: 'ToolSNInHead'; Owned: True; TagType: 'TIntegerTag' ),
    ( Ini: 'ToolRemInHead'; Tag: 'ToolRemInHead'; Owned: True; TagType: 'TDoubleTag' ),
    ( Ini: 'ToolHPInHead'; Tag: 'ToolHPInHead'; Owned: True; TagType: 'TIntegerTag' ),
    ( Ini: 'ToolingRequired'; Tag: 'OCT_ToolingRequired'; Owned: True; TagType: 'TIntegerTag' ),
    ( Ini: 'TotalToolCount'; Tag: 'OCT_TotalToolCount'; Owned: True; TagType: 'TIntegerTag' ),
    ( Ini: 'OPPRequiredTool'; Tag: 'OPP_ToolIDString'; Owned: False; TagType: 'TStringTag' ),
    ( Ini: 'OppFilename'; Tag: 'NewActiveFile'; Owned: False; TagType: 'TStringTag' ),
    ( Ini: 'PartLifeProcessStarted'; Tag: 'PartLifeProcessStarted'; Owned: False; TagType: 'TIntegerTag' ),
    ( Ini: 'MachineID'; Tag: 'MachineID'; Owned: False; TagType: 'TStringTag' ),
    ( Ini: 'PartLifeSourcePath'; Tag: 'PartLifeSourcePath'; Owned: False; TagType: 'TStringTag' ),
    ( Ini: 'PartLifeOutputPath'; Tag: 'PartLifeOutputPath'; Owned: False; TagType: 'TStringTag' ),
    ( Ini: 'PartLifeCompleteCount'; Tag: 'PartLifeCompleteCount'; Owned: False; TagType: 'TIntegerTag' )
  );

  ObjectName = 'OCT_TM';

procedure ResetRewind(aTag : TTagRef); stdcall;
begin
  if Named.GetAsBoolean(aTag) then
    OCTManager.ResetRewind;
end;

procedure OCTResume(aTag : TTagRef); stdcall;
begin
  if Named.GetAsBoolean(aTag) then begin
    OCTManager.Resume;
    Named.SetAsBoolean(aTag, False);
  end;
end;

procedure TagChange(aTag : TTagRef); stdcall;
var Buffer: array[0..255] of Char;
begin
  if aTag = OCTManager.Tags[otRequiredToolIDString] then begin
    Named.GetAsString(OCTManager.Tags[otRequiredToolIDString], Buffer, 255);
    OCTManager.DetermineEx1RequiredTool(Buffer, OPPHeaderConsumer.OPPFile.ToolUsage);
  end;

  if aTag = OCTManager.Tags[otProcessStarted] then begin
    if Named.GetAsBoolean(aTag) then
      PartLife.SetPreparedStatus;
  end;

  if aTag = OCTManager.PartLifeEditor then begin
    if not Named.GetAsBoolean(aTag) then
      PartLife.Editor;
  end;
end;

function ToolingFidReset(FID : FHandle) : Boolean; stdcall;
begin
  Result := OCTManager.ToolingFid <> 0;//False;
end;

{ TOCT - Object creation }

function ResetFault(AFID : FHandle): Boolean; stdcall;
begin
  Result:= false;
end;


constructor TOCT.Create(Host: IFSDOCTHost);
begin
  inherited;
  OCTManager := Self;
  OPPList := TOCTDataWrapper.Create;
  OPPList.OnChange := DoOCTChange;
  RequiredToolList := TRequiredToolList.Create;

  OCTOpTable := TOpTable.Create(OCTOpsName, OpTableSize);
  PartData := TPartData.Create;
  PartData.OnOpDoneChange := UpdatePartData;

  OPPHeaderConsumer.OnChange := LoadedOPPChange;
  FDurationTimer := TDurationTimer.Create;
  FCycleStartTimer := TDurationTimer.Create;

  if StringArraySet.UseArray(PartNominalsTable, Self, Table) <> AAR_OK then begin
    Named.SetFaultA('PartLife', 'Unable to load MDT [Nominals]', FaultStopCycle, ResetFault);
    Exit;
  end;


end;


destructor TOCT.Destroy;
begin
  OPPList.Close;
  OPPList := nil;
  RequiredToolList.Close;
  RequiredToolList := nil;
  OCTOpTable.Close;
  OCTOpTable := nil;
  PartData.Close;
  PartData := nil;
  inherited;
end;

{ This will be ran at the very end of the installation of the DLL. All final installs
  happen one after another. Using both the Insatll and final install will allow all relevant
  tags to be added into the system and then pick up the various tags which are required to be accessed
  at the end of the installs}
procedure TOCT.FinalInstall;
var I : TOCTTag;
    Trans: TTranslator;
begin
  inherited;
  OCTResetRewindTag := Named.AquireTag(NameOCTResetRewind, 'TMethodTag', OCT.ResetRewind);
  ResetRewindTag := Named.AquireTag('ResetRewind', 'TMethodTag', nil);
  Named.AquireTag(NameOCTReqResume, 'TMethodTag', OCT.OCTResume);

  for I := Low(I) to High(I) do begin
    if not TagDefinitions[I].Owned then
      Tags[I] := Named.AquireTag(PChar(TagNames[I]), PChar(TagDefinitions[I].TagType), OCT.TagChange);
  end;

  PartLifeEditor:= Named.AquireTag('PartLifeEditor', 'TStringTag', OCT.TagChange);

  Trans:= TTranslator.Create;
  try
    CannotLoadActiveText:= Trans.GetString('$OCT_CANT_BE_RUN');
    CannotAutomate_S_SText:= Trans.GetString('$OCT_DEPENDENCY_S_S');
    NoToolingAvailableText:= Trans.GetString('$OCT_NO_TOOLING');
    PleaseAddToolingText:= Trans.GetString('$ADD_REQUIRED_TOOLING');
  finally
    Trans.Free;
  end;

end;
{Intially this is ran when the DLL's is being installed and then the final install has to happen
 after it. This will assign and make the tags available to all other installing DLL's when the final install is
 happening for each of them}
procedure TOCT.Install;
var I : TOCTTag;
begin
  inherited;
  ReadConfig;
  OCTLoadedTag := Named.AquireTag(NameOCTLoadedOK, 'TIntegerTag', nil);
  OCTLoadedNameTag := Named.AddTag(NameOCTLoadedName, 'TStringTag');
  RequiredToolTag := Named.AddTag(NameOCTRequiredTool, 'TIntegerTag');
  ToolLifeInstance.AddCallback(ToolLifeChange);

  for I := Low(I) to High(I) do begin
    if TagDefinitions[I].Owned then
      Tags[I] := Named.AddTag(PChar(TagNames[I]), PChar(TagDefinitions[I].TagType));
  end;

end;

procedure TOCT.Shutdown;
{var Ini : TIniFile;
    I : TOCTTag; }
begin
{
  Ini := TIniFile.Create(DllProfilePath + 'oct.cfg');
  try
    Ini.WriteString('OCT_TM', 'FixedStartOPP', FixedStartOPPName);
    Ini.WriteString('OCT_TM', 'FixedEndOPP', FixedEndOPPName);
    Ini.WriteBool('OCT_TM', 'DisableSorting', DisableSorting);
    for I := Low(TOCTTag) to High(TOCTTag) do begin
      Ini.WriteString('OCT_TMTags', TagDefinitions[I].Ini, TagNames[I]);
    end;
  finally
    Ini.Free;
  end;
  }
end;

procedure TOCT.ResetRewind;
begin
  PartLife.ResetRewind;
  if Self.OCTFileName <> '' then begin
    if OPPList[FActiveOPP].Status = osStarted then begin
      OPPList.BeginUpdate;
      try
        OPPList[FActiveOPP].Status := osInRecovery;
        InRecovery := True;
      finally
        OPPList.EndUpdate;
      end;
    end;
  end;
  RunEnable := False;
  if FOCTRunStage <> orsWaitForTooling then
    FOCTRunStage := orsWaitRun;
end;

procedure TOCT.Resume;
begin
  Host.UpdateState(Self, opcSetOPPList, PChar(OPPList.Text));
  Host.UpdateState(Self, opcLoadOPP, PChar(OPPList[ActiveOPP].FullPath));
  Host.UpdateState(Self, opcSetIndex, Pointer(ActiveOPP));
end;

procedure TOCT.GenerateRequiredTools;
var I, J : Integer;
    T : TRequiredTool;
begin
  RequiredToolList.BeginUpdate;
  try
    RequiredToolList.Clear;
    for I := 0 to OPPList.Count - 1 do begin
      if (not OPPList[I].FixedStart and not OPPList[I].FixedEnd) then begin
        J := RequiredToolList.IndexOf(OPPList[I].ToolType);
        if J = -1 then begin
          T := RequiredToolList.AddTool;
          T.ToolName := OPPList[I].ToolType;
          T.Length := OPPList.RemainingToolUsage(T.ToolName);
          T.Priority := 1;
          // This enumerates the tools in the system, used for sorting
          // ToolGroup is the Index into the RequiredToolTable
          T.ToolGroup := RequiredToolList.Count - 1;
          OPPList[I].ToolGroup := T.ToolGroup;
        end else begin
          OPPList[I].ToolGroup := RequiredToolList.Tool[J].ToolGroup;
        end;
      end;
    end;
  finally
    RequiredToolList.EndUpdate;
  end;
end;

procedure TOCT.FirstTimeLoadUpdateRequiredTools;
var I : Integer;
begin
  RequiredToolList.BeginUpdate;
  OPPList.Lock;
  try
    for I := 0 to RequiredToolList.Count - 1 do begin
      RequiredToolList[I].HasDepencencies := False;
    end;

    for I := 0 to OPPList.Count - 1 do begin
      if OPPList[I].HasDepend then
        RequiredToolList[OPPList[I].ToolGroup].HasDepencencies := True;
    end;

    UpdateRequiredTools;
  finally
    OPPList.Unlock;
    RequiredToolList.EndUpdate;
  end;
end;

procedure TOCT.UpdateRequiredTools;
var RemainingUsage : Double;
    I : Integer;
    MinToolLength : Double;
    Index: Integer;
    _OPPIndex: Integer;

  function NextOPPIndex: Integer;
  begin
    for Result:= FActiveOPP + 1 to OPPList.Count - 1 do begin
      if OPPList[Result].Enabled then begin
        Exit;
      end;
    end;
    Result:= OPPList.Count - 1;
  end;

begin
  ToolingAvailable := False;
  RequiredToolList.BeginUpdate;
  OPPList.Lock;
  try
    if not DisableSorting then begin
      for I := 0 to RequiredToolList.Count - 1 do begin
        RemainingUsage := OPPList.RemainingToolUsage(RequiredToolList.Tool[I].ToolName);
        RequiredToolList.Tool[I].Length := RemainingUsage;

        if RemainingUsage > ToolLifeInstance.GetTotalAvailable(RequiredToolList.Tool[I].ToolName) then begin
          if RequiredToolList.Tool[I].HasDepencencies and
             OPPList.ToolIsStillDependent(RequiredToolList.Tool[I].ToolGroup) then
            RequiredToolList.Tool[I].Priority := 0
          else
            RequiredToolList.Tool[I].Priority := 1;

        end else begin
          if RemainingUsage > 0 then
            RequiredToolList.Tool[I].Priority := 2
          else
            RequiredToolList.Tool[I].Priority := 3;
        end;

        MinToolLength := OPPList.MinimumToolLength(RequiredToolList.Tool[I].ToolName);

        if ToolLifeInstance.GetTotalAvailable(RequiredToolList.Tool[I].ToolName) > MinToolLength then
          ToolingAvailable := True;

        if OPPList.OPPsRemaining <= 1 then
          ToolingAvailable := True;

      end;
    end
    else begin // Unsorted
      for I:= 0 to OPPList.Count - 1 do begin
        Index:= RequiredToolList.IndexOf(OPPList.OPP[I].ToolType);
        if Index <> -1 then begin
          if OPPList.OPP[I].Complete then begin
            RequiredToolList.Tool[Index].Priority:= 0;
          end
          else begin
            RequiredToolList.Tool[Index].Priority:= 1;
          end;
        end;
      end;

      if OPPList.OPPsRemaining <= 1 then begin
        ToolingAvailable := True;
      end
      else if (FOCTRunStage = orsWaitForTooling)  or (FOCTRUnStage = orsCheckTooling) then begin
        _OPPIndex:= NextOPPIndex;
        Index:= RequiredToolList.IndexOf(OPPList[_OPPIndex].ToolType);
        if Index <> -1 then begin

          MinToolLength := OPPList[_OPPIndex].ToolUsage;

          RequiredToolList.Tool[Index].Priority:= 2;
          if ToolLifeInstance.GetTotalAvailable(RequiredToolList.Tool[Index].ToolName) > MinToolLength then
            ToolingAvailable := True;
        end;
      end
    end;
  finally
    OPPList.Unlock;
    RequiredToolList.EndUpdate;
  end;
end;

procedure TOCT.DetermineEx1RequiredTool(ToolName : WideString; Usage : Double);
var I : Integer;
    T : TToolData;
    List : TList;
    RTool : Integer;
begin
  List := TList.Create;

  // Special catch for tool type -1
  if StrToIntDef(ToolName, 0) = -1 then begin
    Named.SetAsInteger(RequiredToolTag, 0);
    Exit;
  end;

  ToolLifeInstance.Lock;
  try
    RTool := -1;

    for I := 0 to ToolLifeInstance.Count - 1 do begin
      T := ToolLifeInstance.Tool[I];
      if CompareText(T.ShuttleID, ToolName) = 0 then begin
        //Named.EventLog(Format('Remaining = %.2f Current usage = %.2f', [T.TotalRemaining, Usage]));
        if (T.TotalRemaining >= Usage) then begin
          List.Add(Pointer(I));
          if T.Ex1 = 0 then
            RTool := 0;
        end;
      end;
    end;

    // If Its not in the head and there are some lying-about then use one of them
    // This policy may seem random but actually the earliest one in the list is the
    // oldest in the list.
    if (RTool <> 0) and (List.Count > 0) then
      RTool := ToolLifeInstance.Tool[Integer(List[0])].Ex1;

    Named.SetAsInteger(RequiredToolTag, RTool);
  finally
    ToolLifeInstance.Unlock;
    List.Free;
  end;
end;

procedure TOCT.Sort(PreferedTool : WideString);
begin
  UpdateRequiredTools;
  if not DisableSorting and OCTMode then begin
    OPPList.BeginUpdate;
    try
      if (not RunEnable or (FOCTRunStage = orsEndProcessing)) and
         Named.GetAsBoolean(OCTLoadedTag) and
         not InRecovery then begin
        OPPList.Sort;
        Host.UpdateState(Self, opcLoadOPP, PChar(OPPList[ActiveOPP].FullPath));
        Host.UpdateState(Self, opcSetIndex, Pointer(ActiveOPP));
      end;
    finally
      OPPList.EndUpdate;
    end;
  end;
//  DetermineEx1RequiredTool(OPPList[ActiveOPP].ToolType, OPPList[ActiveOPP].ToolUsage);
end;

function TOCT.GetMinimumToolUsage(Tool: WideString;
  out Value: Double): Boolean;
begin
  Value := OPPList.MinimumToolLength(Tool);
  Result := True;
end;


procedure TOCT.Clean;
begin
  OPPList.Clean;
end;

procedure TOCT.LoadOCT(aFileName: WideString);
var Ini : TCustomIniFile;
    I : Integer;
    S : TStringList;
    Tmp : Integer;
begin
  OPPList.BeginUpdate;
  OCTOpTable.BeginUpdate;
  try
    Named.SetAsInteger(OCTLoadedTag, 0);
    try
      Named.SetAsString(OCTLoadedNameTag,'');
      Clean;
      LoadOCTTable(aFileName);

      S := TStringList.Create;
      try
        for I := 0 to OPPList.Count - 1 do begin
          if not FileExists(OPPList[I].FullPath) then
            raise Exception.Create('File not found: ' + OPPList[I].FileName);
          Ini := TIniFile.Create(OPPList[I].FullPath);
          Ini.ReadSectionValues('OPERATIONDESCRIPTION', S);
          OPPList[I].ToolType := LowerCase(S.Values['ToolTypeString']);
          OPPList[I].StartTime := 0;
          OPPList[I].EndTime := 0;
          OPPList[I].Enabled := True;
          OPPList[I].Status := osNone;
          OPPList[I].EstimatedCycleTime := StrToFloatDef(S.Values['CycleTime'], 0);
          OPPList[I].ReworkStatus :='';
          Tmp := StrToIntDef(S.Values['OpNumber'], 0);

          if Tmp <> 0 then
            OCTOpTable[Tmp mod 128] := True;
          OPPList[I].OpNumber := Tmp;

          OPPList[I].ToolUsage := StrToFloatDef(S.Values['ToolUsage'], 0);
          S.Clear;
        end;
      finally
        S.Free;
      end;

      try
        GenerateRequiredTools;
      except
        on E : Exception do begin
          Named.SetFault(ObjectName, PChar(E.Message), FaultStopCycle);
          Exit;
        end;
      end;

      UpdatePartData(PartData);

      Host.UpdateState(Self, opcSetOPPList, PChar(OPPList.Text));
      Named.SetAsInteger(OCTLoadedTag, 1);
      Named.SetAsString(OCTLoadedNameTag, PChar(ExtractFileName(ExtractFileName(aFilename))));
      Reset;
    except
      Named.SetAsInteger(OCTLoadedTag, 0);
      Named.SetAsString(OCTLoadedNameTag, '');
      raise;
    end;

    if not DisableSorting then
      OPPList.Sort;

    FirstTimeLoadUpdateRequiredTools;
  finally
    OPPList.EndUpdate;
    OCTOpTable.EndUpdate;
  end;
end;


procedure TOCT.LoadOCTTable(aFileName : string);
var O : TOPP;
    S : TStreamQuoteTokenizer;
    FileName : string;
    DependsOn : string;
    Token : string;
    Natural: Integer;
begin
  OCTFilename := ExtractFileName(aFilename);
  OPPList.BeginUpdate;
  Natural := 0;
  try
    OPPList.Clean;

    S := TStreamQuoteTokenizer.Create;
    try
      if FixedStartOPPName <> '' then begin
        O := OPPList.AddOPP;
        O.FileName := UpperCase(ExtractFileName(FixedStartOPPName));
        O.FullPath := ExtractFilePath(FixedStartOPPName) + O.FileName;
        O.DependentOn := '';
        O.FixedStart := True;
        O.OpNumber := 0;
        O.NaturalOrder:= Natural;
      end;

      S.Seperator := '';
      S.QuoteChar := '''';
      S.SetStream(TFileStream.Create(aFileName, fmOpenRead));
      Token := S.Read;
      while not S.EndOfStream do begin
        FileName := Token;
        S.ExpectedToken('->');
        DependsOn := S.Read;
        Token := S.Read;
        O := OPPList.AddOPP;
        O.FullPath := ExtractFilePath(aFileName) + FileName;
        O.DependentOn := UpperCase(DependsOn);
        if O.DependentOn = '*' then
          O.DependentOn := '';
        O.FileName := UpperCase(ExtractFileName(FileName));
        O.NaturalOrder:= Natural;

        Inc(Natural);
      end;

      if FixedEndOPPName <> '' then begin
        O := OPPList.AddOPP;
        O.FileName := UpperCase(ExtractFileName(FixedEndOPPName));
        O.FullPath := ExtractFilePath(FixedEndOPPName) + O.FileName;
        O.DependentOn := '';
        O.FixedEnd := True;
        O.OpNumber := 0;
        O.NaturalOrder:= Natural;
      end;

    finally
      S.Free;
    end;
  finally
    OPPList.EndUpdate;
  end;
end;

// Assumes the last OPP cannot be disabled :: THIS IS NOT CORRECT
{procedure TOCT.LastOPP;
begin
  FActiveOPP := OPPList.Count - 1;
  Host.UpdateState(Self, opcLoadOPP, PChar(OPPList[ActiveOPP].FullPath));
  Host.UpdateState(Self, opcSetIndex, Pointer(ActiveOPP));
end; }

// Load first non-disabled OPP
function  TOCT.FirstOPP : Integer;
begin
  Result := -1;
  if OPPList.Count <> 0 then begin
    FActiveOPP := 0;
    while ActiveOPP < OPPList.Count do begin
      if OPPList[ActiveOPP].Enabled then begin
        Host.UpdateState(Self, opcLoadOPP, PChar(OPPList[ActiveOPP].FullPath));
        Host.UpdateState(Self, opcSetIndex, Pointer(ActiveOPP));
        Result := ActiveOPP;
        Exit;
      end else begin
        OPPList[FActiveOPP].Status := osSkipped;
        Inc(FActiveOPP);
      end;
    end;
  end;
  // This represents all OPP are disabled, not certain what this will do!
  FActiveOPP := 0;
end;

function TOCT.NextOPP: Boolean;
begin
  Result := True;
  InRecovery := False;
  Named.SetAsBoolean(Tags[otToolingRequired], False);

  // The following may not be in the right place
  OPPList.OPP[ActiveOPP].Duration := FDurationTimer.Duration;
  OPPList.OPP[ActiveOPP].EndTime := Now;
  FDurationTimer.Reset;

  Inc(FActiveOPP);
  while ActiveOPP < OPPList.Count do begin
    if OPPList[ActiveOPP].Enabled and
      (OPPList[FActiveOPP].Status < osComplete) and
      not OPPList[FActiveOPP].AlreadyComplete then begin
      Host.UpdateState(Self, opcLoadOPP, PChar(OPPList[ActiveOPP].FullPath));
      Host.UpdateState(Self, opcSetIndex, Pointer(ActiveOPP));
      Exit;
    end else if OPPList[FActiveOPP].AlreadyComplete then begin
      OPPList[FActiveOPP].Status := osComplete;
    end else { if OPPList[ActiveOPP].Enabled then } begin
      OPPList[FActiveOPP].Status := osSkipped;
    end;
    Inc(FActiveOPP);
  end;

  Result := False;
  FirstOpp;
  OCTProcessing := False;
  RunEnable := False;
//  Reset;
  Host.UpdateState(Self, opcOCTComplete, Pointer(1));
end;

function TOCT.GetToolUsage(Tool: WideString): Double;
begin
  Result := OPPList.RemainingToolUsage(Tool)
end;

procedure TOCT.ResetTable;
var I : Integer;
begin
  if OPPList.Count > 0 then begin
    OPPList.BeginUpdate;
    try
      for I := 0 to OPPList.Count - 1 do begin
        OPPList.OPP[I].Status := osNone;
        OPPList.OPP[I].StartTime := 0;
        OPPList.OPP[I].EndTime := 0;
        OPPList.OPP[I].Duration := 0;
      end;
    finally
      OPPList.EndUpdate;
    end;
  end;
  FOCTRunStage := orsWaitRun;
end;

procedure TOCT.Reset;
begin
  OPPList.BeginUpdate;
  try
//    Sort('');
    FirstOpp;
    RunEnable := False;
    OCTProcessing := False;
    ResetTable;
    UpdateRequiredTools;
    InRecovery := False;
    FDurationTimer.Reset;
    Named.SetAsBoolean(Tags[otToolingRequired], False);
  finally
    OPPList.EndUpdate;
  end;
end;

procedure TOCT.Run;
begin
  if opsAutoMode in CurrentStatus then begin
    RunEnable := True;
  end;
end;

procedure TOCT.DoOCTChange(Sender : TObject);
begin
  OPPList.Lock;
  try
    if (not NC.InCycle and not OCTProcessing and OCTMode) then begin
      OPPList.BeginUpdate;
      try
        ResetTable;
        UpdateRequiredTools;
//        Sort('');
        FirstOPP;
      finally
        OPPList.EndUpdate;
      end;
    end;

    if FOCTRunStage = orsWaitForTooling then begin
      UpdateRequiredTools;
    end;

    if InRecovery and OCTProcessing then begin
      if (OPPList.OPP[ActiveOPP].Status > osInRecovery) then begin
       if ValidOpNumber(OPPList[ActiveOPP].OpNumber) and
           PartData.Done[OPPList[ActiveOPP].OpNumber] then
          PartData.StatusFlags := PartData.StatusFlags + [psfInspectionRequired];
        InRecovery := False;
        NextOPP;
      end else if (OPPList.OPP[ActiveOPP].Status = osNone) then begin
        InRecovery := False;
        if ActiveOPP > 0 then begin
          Dec(FActiveOPP);
          Sort('');
          NextOPP;
        end else begin
          Reset;
        end;
      end;
    end;
  finally
    OPPList.Unlock;
  end;
end;

procedure TOCT.ToolLifeChange(Sender : TObject);
var Tmp : array [0..255] of Char;
    TD: TToolData;
begin
  TD:= ToolLifeInstance.FindToolEx1(0);

  if Assigned(TD) then begin
    StrPLCopy(Tmp, TD.ShuttleID, 255);
    Named.SetAsString(Tags[otToolInHead], Tmp);
    Named.SetAsInteger(Tags[otToolSNInHead], TD.ShuttleSN);
    Named.SetAsDouble(Tags[otToolRemInHead], TD.ActToolLength - TD.MinToolLength);
    Named.SetAsBoolean(Tags[otToolHPInHead], TD.HP);
  end
  else begin
    Named.SetAsString(Tags[otToolInHead], PChar(''));
    Named.SetAsInteger(Tags[otToolSNInHead], 0);
    Named.SetAsDouble(Tags[otToolRemInHead], 0);
    Named.SetAsBoolean(Tags[otToolHPInHead], False);
  end;

  Named.SetAsInteger( Self.Tags[otTotalToolCount], ToolLifeInstance.Count);

  Sort('');
  DetermineEx1RequiredTool(OPPHeaderConsumer.OPPFile.ToolTypeString, OPPHeaderConsumer.OPPFile.ToolUsage);
end;

{ This runs a state machine indicating it's running mode in operation. The variable used as the
  state is 'FOCTRunStage'. several functions and procedurea are linked to the state machine
  shown below and are placed before the actual CASE statements of the state machine is used.}
procedure TOCT.Update(Status : TOCTPluginStatii);
  function OKToRun : Boolean;
  var I : Integer;

  begin
    // The following is a catch all clause, create a cycle stop error
    // if a dependent operation has not completed.  This could occur when
    // there is a cyclic dependency or if the dependent operation has been
    // disabled.
    if OPPList[ActiveOPP].DependentOn <> '' then begin
      I := OPPList.IndexOf(OPPList[ActiveOPP].DependentOn);
      if (OPPList[I].Status <> osComplete) and OPPList[I].Enabled then begin
        Named.SetFaultA(ObjectName, Format(CannotAutomate_S_SText, [OPPList[ActiveOPP].FileName, OPPList[I].FileName]), FaultStopCycle, nil);
        RunEnable := False;
      end;
    end;

    if OPPList[ActiveOPP].Status > osStarted then begin
      Named.SetFaultA(ObjectName, CannotLoadActiveText, FaultInterlock, nil);
      RunEnable := False;
    end;

    Result := RunEnable;
  end;

  procedure DecrementBarrelCount;
  var T : TToolData;
  begin
    ToolLifeInstance.Lock;
    try
      T := ToolLifeInstance.FindToolEx1(0);
      if Assigned(T) then begin
        if T.RemainingBarrels > 0 then begin
          T.RemainingBarrels := T.RemainingBarrels - 1;
          T.ActToolLength := T.MaxToolLength;
          ToolLifeInstance.DoToolLifeChange;
//          ToolLifeChange(ToolLifeInstance);
        end else
          Named.SetFaultA(ObjectName, 'Cannot decrement barrels remaining, there are no more barrels', FaultStopCycle, nil);
      end;
    finally
      ToolLifeInstance.Unlock;
    end;
  end;

  procedure MarkOpDone;
  begin
    if ValidOpNumber(OPPHeaderConsumer.OPPFile.OpNumber) then
      PartData.Done[OPPHeaderConsumer.OPPFile.OpNumber] := True;
  end;

  procedure DecrementToolLife;
  var T : TToolData;
      MinToolLength : Double;
  begin
    ToolLifeInstance.Lock;

    MarkOpDone;
    try
      T := ToolLifeInstance.FindToolEx1(0);
      if Assigned(T) then begin
        if T.BarrelRemaining < OPPHeaderConsumer.OPPFile.ToolUsage then
          Named.SetFaultA(ObjectName, 'A problem has occured in tool management, requested more tool removal than is currently in the tool.', FaultInterlock, nil);
        T.ActToolLength := T.ActToolLength - OPPHeaderConsumer.OPPFile.ToolUsage;
        if FOCTMode then begin
          MinToolLength := OPPList.MinimumToolLength(T.ShuttleID);
          if MinToolLength > BigToolLength - 1 then
            MinToolLength := 0;

          if MinToolLength > T.TotalRemaining  then
            T.ActToolLength := 0;
        end else begin
          if OPPHeaderConsumer.OPPFile.ToolUsage > T.TotalRemaining then
            T.ActToolLength := 0;
        end;
      end else begin
        Named.SetFaultA(ObjectName, 'No tool in head, cannot decrement tool life.', FaultWarning, nil);
      end;
    finally
      ToolLifeInstance.Unlock;
      ToolLifeInstance.DoToolLifeChange;
    end;
  end;

   procedure ResetToolingFID;
  begin
    if ToolingFID <> 0 then begin
      Named.FaultResetID(ToolingFID);
      ToolingFID := 0;
    end;
  end;

begin
  {
  if Named.GetAsBoolean(Tags[otDecrementToolLifeReq]) then begin
    if not Named.GetAsBoolean(Tags[otDecrementToolLifeAck]) then begin
      DecrementToolLife;
      Named.SetAsBoolean(Tags[otDecrementToolLifeAck], True);
    end;
  end else begin
    if Named.GetAsBoolean(Tags[otDecrementToolLifeAck]) then begin
      Named.SetAsBoolean(Tags[otDecrementToolLifeAck], False);
    end;
  end;

  if Named.GetAsBoolean(Tags[otDecrementBarrelReq]) then begin
    if not Named.GetAsBoolean(Tags[otDecrementBarrelAck]) then begin
      DecrementBarrelCount;
      Named.SetAsBoolean(Tags[otDecrementBarrelAck], True);
    end;
  end else begin
    if Named.GetAsBoolean(Tags[otDecrementBarrelAck]) then begin
      Named.SetAsBoolean(Tags[otDecrementBarrelAck], False);
    end;
  end;
  }
  if OCTMode <> (opsOCTMode in Status) then begin
    FOCTMode := opsOCTMode in Status;

    if not OCTMode then begin
      Named.SetAsInteger(OCTLoadedTag, 0);
      Named.SetAsString(OCTLoadedNameTag, '');
    end;
  end;

  CurrentStatus := Status;

  if not (opsOCTMode in Status) and RunEnable then begin
    RunEnable := False;
  end;

  OPPList.Lock;
  try
  { CASE STATEMENTS USED FOR THE STATE MACHINE - This will be changing states in conjunction
    with other state machines and operations }
    case FOCTRunStage of
      orsWaitRun : begin
        if not Named.GetAsBoolean(ResetRewindTag) then begin
          ResetToolingFID;

          if RunEnable and
             (opsAutoMode in Status) and
             (opsCStartPermissive in Status) and
             not (opsInCycle in Status) and
             OKToRun then begin

            OCTProcessing := True;

            if not OPPList[FActiveOPP].Enabled then begin
              FOCTRunStage := orsEndProcessing;
              OPPList[FActiveOPP].Status := osSkipped;
              Exit;
            end;

            Host.UpdateState(Self, opcCycleStart, nil);
            FOCTRunStage := orsWaitInCycle;
            Host.UpdateState(Self, opcOCTComplete, nil);
            FCycleStartTimer.Reset;
            FCStartRetry := True;  // One shot retry
          end;
        end;
      end; //orsWaitRun

      orsWaitInCycle : begin
        FCycleStartTimer.Clock(True);
        if (opsInCycle in Status) or
           NC.SingleBlock or
           not (CNCModeAuto in NC.CNCMode) then begin
          FOCTRunStage := orsWaitEndProgram;
          OPPList.OPP[ActiveOPP].Status := osStarted;
          OPPList.OPP[ActiveOPP].StartTime := Now;
          FDurationTimer.Clock(True);
        end else begin
          if (FCycleStartTimer.Duration > 4) and FCStartRetry then begin
            FCycleStartTimer.Reset;
            FCStartRetry := False;
            Host.UpdateState(Self, opcCycleStart, nil);
          end;
        end;
      end; //orsWaitInCycle

      orsWaitEndProgram : begin
        if not (opsOperationComplete in Status) then begin
          FDurationTimer.Clock(NC.InCycle);
          if NC.InCycle then begin
            OPPList.OPP[ActiveOPP].Duration := FDurationTimer.Duration;
          end
        end else begin
          FOCTRunStage := orsCheckTooling;
          if OPPList.OPP[ActiveOPP].Enabled then begin
            OPPList.OPP[ActiveOPP].Status := osComplete;
          end;
        end;
      end; // orsWaitEndProgram

      orsCheckTooling : begin
        // Block load until a required tool is available.
        UpdateRequiredTools;
        if not ToolingAvailable then begin
          RunEnable := False;
          Named.SetFaultA(ObjectName, NoToolingAvailableText, FaultInterlock, nil);
          ToolingFID := Named.SetFaultA(ObjectName, PleaseAddToolingText, FaultWarning, ToolingFidReset);
          FOCTRunStage := orsWaitForTooling;
          Named.SetAsBoolean(Tags[otToolingRequired], True);
        end else begin
          FOCTRunStage := orsEndProcessing;
        end;
      end; //orsCheckTooling

      orsWaitForTooling : begin
        if ToolingAvailable then begin
          ResetToolingFID;
          FOCTRunStage := orsEndProcessing;
          Named.SetAsBoolean(Tags[otToolingRequired], False);
        end
      end; // orsWaitForTooling

      orsEndProcessing : begin
        if not Named.GetAsBoolean(ResetRewindTag) then begin
          FCycleStartTimer.Clock(True);
          if FCycleStartTimer.Duration > 0.5 then begin
            if (opsOperationComplete in Status) then begin
              if not (opsInCycle in Status) then begin
                Host.UpdateState(Self, opcOPPComplete, Pointer(ActiveOPP));

                Sort('');

                NextOPP;
                if (opsSingle in Status) then
                  RunEnable := False;
                FOCTRunStage := orsEndDelay;
                FCycleStartTimer.Reset
              end;
            end;
          end;
        end
        else begin
          FCycleStartTimer.Reset;
        end;
      end; //orsEndProcessing

      orsEndDelay : begin
        FCycleStartTimer.Clock(True);
        if FCycleStartTimer.Duration > 1.5 then
          if not Named.GetAsBoolean(ResetRewindTag) then begin
            FOCTRunStage := orsWaitRun;
          end;
      end; //OrsEndDelay
    end; // end case
  finally
    OPPList.Unlock;
  end;
end;

procedure TOCT.SetRunEnable(aOn: Boolean);
begin
  if not OCTProcessing and (NC.ProgramLine = 0) and not FRunEnable then begin
    ResetTable;
    Sort('');
  end;

  FRunEnable := aOn and (OPPList[ActiveOPP].Status <> osInRecovery);
  Host.UpdateState(Self, opcRunning, Pointer(Ord(RunEnable)));

  if RunEnable and
     not NC.InCycle  and
    (FOCTRunStage in ValidCycleStart)  then
    Host.UpdateState(Self, opcCycleStart, nil);
end;


procedure TOCT.LoadedOPPChange(Sender: TObject);
begin
  DetermineEx1RequiredTool(OPPHeaderConsumer.OPPFile.ToolTypeString, OPPHeaderConsumer.OPPFile.ToolUsage);
end;

{Configuration is the file that resides in the live profile}
procedure TOCT.ReadConfig;
var Ini : TCustomIniFile;
    I : TOCTTag;
begin
  Ini := NamedPlugin.CustomiseConfigurationIni(DllProfilePath + 'oct.cfg');
  {TIniFile.Create(DllProfilePath + 'oct.cfg'); }
  try
    Partlife.FeatureName:= Ini.ReadString('PartLife', 'FeatureNameColumn', 'feature');
    FixedStartOPPName := Ini.ReadString('OCT_TM', 'FixedStartOPP', '');
    FixedEndOPPName := Ini.ReadString('OCT_TM', 'FixedEndOPP', '');

    if(not FileExists(FixedStartOPPName)) then begin
      if (DllContSpecPath <> '') then begin
        FixedStartOppName:= IncludeTrailingPathDelimiter( DllContSpecPath) + FixedStartOppName;
      end;
    end;

    if(not FileExists(FixedEndOppName)) then begin
      if (DllContSpecPath <> '') then begin
        FixedEndOppName:= IncludeTrailingPathDelimiter( DllContSpecPath) + FixedEndOppName;
      end;
    end;

    DisableSorting := Ini.ReadBool('OCT_TM', 'DisableSorting', False);
    for I := Low(TOCTTag) to High(TOCTTag) do begin
      TagNames[I] := Ini.ReadString('OCT_TMTags', TagDefinitions[I].Ini, TagDefinitions[I].Tag);
    end;
  finally
    Ini.Free;
  end;
end;


procedure TOCT.UpdatePartData(Sender : TObject);
var I : Integer;
    Tmp : Integer;
begin
  if ActiveOPP = 0 then begin
    for I := 0 to OPPList.Count - 1 do begin
      Tmp := OPPList[I].OpNumber;
      if (Tmp <> 0) and (Tmp <= 128) then begin
        if PartData.Done[Tmp] then
          OPPList[I].AlreadyComplete := True
        else
          OPPList[I].AlreadyComplete := False;
      end;
    end;
  end;
end;

function TOCT.ValidOpNumber(OP: Integer): Boolean;
begin
  Result := (OP > 0) and (OP < OPTableSize);
end;


procedure TOCT.CellChange(Table: IACNC32StringArray; Row, Col: Integer);
var CName: WideString;
    I, J, Total: Integer;
    Publish : Boolean;
begin
  Total:= 0;
  Publish := False;
//  Table.BeginUpdate;
  try
    Table.FieldNameAtIndex(Col, CName);
    if CompareText(CName, Status) = 0 then begin
      Publish := true;
      for I:= 0 to Table.RecordCount - 1 do begin
        Table.GetValue(I, Col, CName);
        J := StrToIntDef(Trim(CName), 0);
        //The value of '2' means the feature is complete
        if J >= 2 then
          Total:= Total + 1;
      end;
    end;
  finally
//    Table.EndUpdate(Self);
  end;
  //Named.EventLog('Update complete');
  if Publish Then Named.SetAsInteger(Tags[otPartLifeCompleteCount], Total);
end;

procedure TOCT.TableChange(Table: IACNC32StringArray);
var I: Integer;
    CName: WideString;
begin

  for I:= 0 to Table.FieldCount -1 do begin
    Table.FieldNameAtIndex(I, CName);
    if CompareText(CName, Status) = 0 then begin
      CellChange(Table, 0, I);
    end;
  end;
end;

initialization
  TAbstractOCTPlugin.AddPlugin('OCTManager', TOCT);
  TOppHeaderConsumer.AddPlugin(SectionOperationDescription, TOppHeaderConsumer);
end.
