unit ScriptTest;

interface

uses Classes, SysUtils, AbstractScript, INamedInterface, NamedPlugin, IScriptInterface;

type
  TTestFunction = class(TScriptMethod)
    procedure Reset; override;
    function Name: WideString; override; stdcall;
    function IsFunction: Boolean; override; stdcall;
    function ParameterCount: Integer; override; stdcall;
    procedure Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer); override; stdcall;
  end;

implementation

{ TTestFunction }

procedure TTestFunction.Execute(const Res: Pointer; const Params: array of Pointer;
  ParamCount: Integer);
begin
   Named.SetAsInteger(Res, Named.GetAsInteger(Params[0]) * Named.getAsInteger(Params[1]));
end;

function TTestFunction.IsFunction: Boolean;
begin
  Result:= True;
end;

function TTestFunction.Name: WideString;
begin
  Result:= 'TestFunction';
end;

function TTestFunction.ParameterCount: Integer;
begin
  Result:= 2;
end;

procedure TTestFunction.Reset;
begin
  inherited;
end;

initialization
  TDefaultScriptLibrary.SetName('Play');
  TDefaultScriptLibrary.AddMethod(TTestFunction);
end.
