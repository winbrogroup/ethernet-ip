unit Unit4;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  IFormInterface, StdCtrls, INamedInterface, NamedPlugin, IStringArray,
  AbstractFormPlugin, Grids, Buttons, ExtCtrls;

type
  TTestForm = class(TInstallForm, IACNC32StringArrayNotify)
    ListBox: TListBox;
    Panel1: TPanel;
    BitBtn1: TBitBtn;
    Splitter1: TSplitter;
    SG: TStringGrid;
    procedure ListBoxClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure SGSetEditText(Sender: TObject; ACol, ARow: Integer;
      const Value: String);
  private
    Table : IACNC32StringArray;
    Updating : Boolean;
    procedure RedrawTable;
    procedure InitializeTable;
  public
    procedure Install; override;
    procedure Execute; override;
    procedure Shutdown; override;
    procedure CellChange(Table : IACNC32StringArray; Row, Col : Integer); stdcall;
    procedure TableChange(Table : IACNC32StringArray); stdcall;
    procedure Doug(Value: Integer);
  end;

var
  TestForm: TTestForm;

implementation

{$R *.DFM}

{ TForm4 }

procedure TTestForm.Install;
begin
  inherited;
end;

procedure TTestForm.Execute;
var S : WideString;
    I : Integer;
begin
  ListBox.Items.Clear;
  for I := 0 to StringArraySet.ArrayCount - 1 do begin
    StringArraySet.AliasAtIndex(I, S);
    ListBox.Items.Add(S);
  end;
  Self.Show;
end;

procedure TTestForm.RedrawTable;
var I, J : Integer;
    S : WideString;
begin
  for J := 0 to Table.RecordCount - 1 do begin
    SG.Cells[0, J+1] := IntToStr(J);
    for I := 0 to Table.FieldCount - 1 do begin
      Table.GetValue(J, I, S);
      SG.Cells[I + 1, J + 1] := S;
    end;
  end;
end;

procedure TTestForm.InitializeTable;
var I : Integer;
    S : WideString;
begin
  SG.ColCount := Table.FieldCount + 1;
  SG.FixedCols := 1;
  if Table.RecordCount = 0 then begin
    SG.RowCount := Table.RecordCount + 2;
    for I := 0 to Table.FieldCount - 1 do
      SG.Cells[I + 1, 1] := '';
  end else
    SG.RowCount := Table.RecordCount + 1;

//  SG.FixedCols := 0;
  SG.FixedRows := 1;
  for I := 0 to Table.FieldCount - 1 do begin
    Table.FieldNameAtIndex(I, S);
    SG.Cells[I + 1, 0] := S;
  end;

  RedrawTable;
end;

procedure TTestForm.ListBoxClick(Sender: TObject);
begin
  if ListBox.ItemIndex <> -1 then begin
    if Assigned(Table) then
      StringArraySet.ReleaseArray(Self, Table);

    StringArraySet.UseArray(ListBox.Items[ListBox.ItemIndex], Self, Table);
    InitializeTable;
  end;
end;

procedure TTestForm.CellChange(Table: IACNC32StringArray; Row,
  Col: Integer);
var V : WideString;
begin
  if not Updating then begin
    Table.GetValue(Row, Col, V);
    SG.Cells[Col + 1, Row + 1] := V;
  end;
end;

procedure TTestForm.TableChange(Table: IACNC32StringArray);
begin
  if not Updating then begin
    InitializeTable;
  end;
end;

procedure TTestForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  // NOTE to release a table just assign nil to it, the ReleaseArraySet method
  // will only stop callbacks from occuring. Assigning nil also stops callbacks.
  if Assigned(Table) then
    StringArraySet.ReleaseArray(Self, Table);
  Table := nil;
end;

procedure TTestForm.SGSetEditText(Sender: TObject; ACol, ARow: Integer;
  const Value: String);
begin
  if Assigned(Table) then begin
    Updating := True;
    try
      Table.SetValue(ARow - 1, ACol - 1, SG.Cells[ACol, ARow]);
    finally
      Updating := False;
    end;
  end;
end;

procedure TTestForm.Shutdown;
begin
  if Assigned(Table) then
    StringArraySet.ReleaseArray(Self, Table);
  Table := nil;
end;

procedure TTestForm.Doug(Value: Integer);
var I: Integer;
    Test: Integer;
begin
  for I:= 0 to 10 do begin
    Test:= 1 shl I;
    if(Test and Value <> 0) then begin
      //Set it to true
    end else begin
      // set display to false
    end;
  end;

end;

initialization
  TAbstractFormPlugin.AddPlugin('TestForm', TTestForm);
end.
