object TestForm: TTestForm
  Left = 261
  Top = 135
  Width = 478
  Height = 536
  Caption = 'MDT'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter
    Left = 0
    Top = 313
    Width = 470
    Height = 9
    Cursor = crVSplit
    Align = alTop
    Beveled = True
  end
  object ListBox: TListBox
    Left = 0
    Top = 0
    Width = 470
    Height = 313
    Align = alTop
    ItemHeight = 13
    TabOrder = 0
    OnClick = ListBoxClick
  end
  object Panel1: TPanel
    Left = 0
    Top = 461
    Width = 470
    Height = 41
    Align = alBottom
    TabOrder = 1
    object BitBtn1: TBitBtn
      Left = 96
      Top = 8
      Width = 75
      Height = 25
      TabOrder = 0
      Kind = bkClose
    end
  end
  object SG: TStringGrid
    Left = 0
    Top = 322
    Width = 470
    Height = 139
    Align = alClient
    Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goColSizing, goEditing]
    TabOrder = 2
    OnSetEditText = SGSetEditText
  end
end
