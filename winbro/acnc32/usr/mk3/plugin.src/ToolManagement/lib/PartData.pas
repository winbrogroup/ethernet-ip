unit PartData;

interface

uses SysUtils, Classes, IStringArray, NamedPlugin, Windows, OCTTypes, OpTable,
     OCTUtils;

type
  TPartData = class(TAbstractStringData)
  private
    FShuttleID : WideString;
    FShuttleSN : Integer;
    FRebuildTime : TDateTime;
    FStatusFlags : TTagStatusFlags;
    FPartID : WideString;
    FPartSN : Integer;
    FPartName : WideString;
    FProbeData : TProbeData;
    FProbeTime : TDateTime;
    FValidLocation : Integer;
    OPToDoTable : TOpTable;
    OpDoneTable : TOpTable;
    FStartTime : TDateTime;
    FEndTime : TDateTime;
    FOnOpDoneChange: TNotifyEvent;
    FOnOpToDoChange: TNotifyEvent;
    procedure CreateTable;
    procedure SetEndTime(const Value: TDateTime);
    procedure SetPartID(const Value: WideString);
    procedure SetPartName(const Value: WideString);
    procedure SetPartSN(const Value: Integer);
    procedure SetProbeData(const Value: TProbeData);
    procedure SetProbeTime(const Value: TDateTime);
    procedure SetRebuildTime(const Value: TDateTime);
    procedure SetShuttleID(const Value: WideString);
    procedure SetShuttleSN(const Value: Integer);
    procedure SetStartTime(const Value: TDateTime);
    procedure SetStatusFlags(const Value: TTagStatusFlags);
    procedure SetValidLocation(const Value: Integer);

    procedure DoToDoChange(Sender : TObject);
    procedure DoDoneChange(Sender : TObject);
    function GetDone(Index: Integer): Boolean;
    function GetToDo(Index: Integer): Boolean;
    procedure SetDone(Index: Integer; const Value: Boolean);
    procedure SetToDo(Index: Integer; const Value: Boolean);
  protected
    procedure RebuildSingleValue(Row, Col :Integer); override;
    procedure RebuildInternalValues; override;
  public
    constructor Create;

    property ShuttleID : WideString read FShuttleID write SetShuttleID;
    property ShuttleSN : Integer read FShuttleSN write SetShuttleSN;
    property RebuildTime : TDateTime read FRebuildTime write SetRebuildTime;
    property StatusFlags : TTagStatusFlags read FStatusFlags write SetStatusFlags;
    property PartID : WideString read FPartID write SetPartID;
    property PartSN : Integer read FPartSN write SetPartSN;
    property PartName : WideString read FPartName write SetPartName;
    property ProbeData : TProbeData read FProbeData write SetProbeData;
    property ProbeTime : TDateTime read FProbeTime write SetProbeTime;
    property ValidLocation : Integer read FValidLocation write SetValidLocation;
    property StartTime : TDateTime read FStartTime write SetStartTime;
    property EndTime : TDateTime read FEndTime write SetEndTime;
    property OnOpToDoChange : TNotifyEvent read FOnOpToDoChange write FOnOpToDoChange;
    property OnOpDoneChange : TNotifyEvent read FOnOpDoneChange write FOnOpDoneChange;
    property ToDo[Index : Integer] : Boolean read GetToDo write SetToDo;
    property Done[Index : Integer] : Boolean read GetDone write SetDone;
    property ToDoTable : TOpTable read OpToDoTable;
    property DoneTable : TOpTable read OpDoneTable;

    procedure Clear;
  end;

implementation

{ TPartData }

const
  PartIDField = 'PartID';
  PartSNField = 'PartSN';
  PartNameField = 'PartName';
  P1XField = 'P1X';
  P1YField = 'P1Y';
  P1ZField = 'P1Z';
  P2XField = 'P2X';
  P2YField = 'P2Y';
  P2ZField = 'P2Z';
  P3XField = 'P3X';
  P3YField = 'P3Y';
  P3ZField = 'P3Z';
  ProbeTimeField = 'ProbeTime';
  ValidLocationField = 'ValidLocation';
  StartTimeField = 'StartTime';
  EndTimeField = 'EndTime';



procedure TPartData.Clear;
const BlankProbeData : TProbeData = ( (0, 0, 0), (0, 0, 0), (0, 0, 0));
var I : Integer;
begin
  BeginUpdate;
  try
    ShuttleID := '';
    ShuttleSN := 0;
    RebuildTime := 0;
    StatusFlags := [];
    PartID := '';
    PartSN := 0;
    PartName := '';
    ProbeData := BlankProbeData;
    ProbeTime := 0;
    ValidLocation := 0;
    StartTime := 0;
    EndTime := 0;
  finally
    EndUpdate;
  end;

  OpDoneTable.BeginUpdate;
  OpToDoTable.BeginUpdate;
  try
    for I := 0 to OpDoneTable.Count - 1 do begin
      OpDoneTable.Checked[I] := False;
      OpToDoTable.Checked[I] := False;
    end;
  finally
    OpDoneTable.EndUpdate;
    OpToDoTable.EndUpdate;
  end;
end;

constructor TPartData.Create;
begin
  inherited Create;
  if StringArraySet.CreateArray('PartData', Self, ITable) <> AAR_OK then begin
    StringArraySet.UseArray('PartData', Self, ITable);
  end else begin
    CreateTable;
  end;

  OPToDoTable := TOPTable.Create(ToDoOpsName, OPTableSize);
  OpToDoTable.OnChange := DoToDoChange;
  OpDoneTable := TOPTable.Create(DoneOpsName, OPTableSize);
  OpDoneTable.OnChange := DoDoneChange;
end;

procedure TPartData.CreateTable;
begin
  ITable.Clear;
//  ITable.AddField(VersionField);
  ITable.AddField(ShuttleIDField);
  ITable.AddField(ShuttleSNField);
  ITable.AddField(RebuildTimeField);
  ITable.AddField(StatusFlagsField);
  ITable.AddField(PartIDField);
  ITable.AddField(PartSNField);
  ITable.AddField(PartNameField);
  ITable.AddField(P1XField);
  ITable.AddField(P1YField);
  ITable.AddField(P1ZField);
  ITable.AddField(P2XField);
  ITable.AddField(P2YField);
  ITable.AddField(P2ZField);
  ITable.AddField(P3XField);
  ITable.AddField(P3YField);
  ITable.AddField(P3ZField);
  ITable.AddField(ProbeTimeField);
  ITable.AddField(ValidLocationField);
  ITable.AddField(StartTimeField);
  ITable.AddField(EndTimeField);

  ITable.Append(1);
end;


procedure TPartData.DoDoneChange(Sender: TObject);
begin
  if Assigned(Self.OnOpDoneChange) then
    Self.OnOpDoneChange(Self);
end;

procedure TPartData.DoToDoChange(Sender: TObject);
begin
  if Assigned(Self.OnOpToDoChange) then
    Self.OnOpToDoChange(Self);
end;

function TPartData.GetDone(Index: Integer): Boolean;
begin
  Result := OpDoneTable[Index];
end;

function TPartData.GetToDo(Index: Integer): Boolean;
begin
  Result := OpToDoTable[Index];
end;

procedure TPartData.RebuildInternalValues;
begin
  FShuttleID := GetFieldString(0, ShuttleIDField);
  FShuttleSN := GetFieldInteger(0, ShuttleSNField);
  FRebuildTime := GetFieldDouble(0, RebuildTimeField);
  FStatusFlags := TTagStatusFlags(Byte(GetFieldInteger(0, StatusFlagsField)));
  FPartID := GetFieldString(0, PartIDField);
  FPartSN := GetFieldInteger(0, PartSNField);
  FPartName := GetFieldString(0, PartNameField);
  FProbeData[0, 0] := GetFieldDouble(0, P1XField);
  FProbeData[0, 1] := GetFieldDouble(0, P1YField);
  FProbeData[0, 2] := GetFieldDouble(0, P1ZField);
  FProbeData[1, 0] := GetFieldDouble(0, P2XField);
  FProbeData[1, 1] := GetFieldDouble(0, P2YField);
  FProbeData[1, 2] := GetFieldDouble(0, P2ZField);
  FProbeData[2, 0] := GetFieldDouble(0, P3XField);
  FProbeData[2, 1] := GetFieldDouble(0, P3YField);
  FProbeData[2, 2] := GetFieldDouble(0, P3ZField);
  FProbeTime := GetFieldDouble(0, ProbeTimeField);
  FValidLocation := GetFieldInteger(0, ValidLocationField);
  FStartTime := GetFieldDouble(0, StartTimeField);
  FEndTime := GetFieldDouble(0, EndTimeField);
end;

procedure TPartData.RebuildSingleValue(Row, Col: Integer);
begin
  RebuildInternalValues;
end;

procedure TPartData.SetDone(Index: Integer; const Value: Boolean);
begin
  OpDoneTable[Index] := Value;
end;

procedure TPartData.SetEndTime(const Value: TDateTime);
begin
  FEndTime := Value;
  SetFieldData(0, EndTimeField, FloatToStr(Value));
end;

procedure TPartData.SetPartID(const Value: WideString);
begin
  FPartID := UpperCase(Value);
  SetFieldData(0, PartIDField, FPartID);
end;

procedure TPartData.SetPartName(const Value: WideString);
begin
  FPartName := UpperCase(Value);
  SetFieldData(0, PartNameField, FPartName);
end;

procedure TPartData.SetPartSN(const Value: Integer);
begin
  FPartSN := Value;
  SetFieldData(0, PartSNField, IntToStr(Value));
end;

procedure TPartData.SetProbeData(const Value: TProbeData);
begin
  FProbeData := Value;
  BeginUpdate;
  try
    ITable.BeginUpdate;
    try
      ITable.SetFieldValue(0, P1XField, FloatToStr(Value[0, 0]));
      ITable.SetFieldValue(0, P1YField, FloatToStr(Value[0, 1]));
      ITable.SetFieldValue(0, P1ZField, FloatToStr(Value[0, 2]));
      ITable.SetFieldValue(0, P2XField, FloatToStr(Value[1, 0]));
      ITable.SetFieldValue(0, P2YField, FloatToStr(Value[1, 1]));
      ITable.SetFieldValue(0, P2ZField, FloatToStr(Value[1, 2]));
      ITable.SetFieldValue(0, P3XField, FloatToStr(Value[2, 0]));
      ITable.SetFieldValue(0, P3YField, FloatToStr(Value[2, 1]));
      ITable.SetFieldValue(0, P3ZField, FloatToStr(Value[2, 2]));
    finally
      ITable.EndUpdate;
    end;
  finally
    EndUpdate;
  end;
end;

procedure TPartData.SetProbeTime(const Value: TDateTime);
begin
  FProbeTime := Value;
  SetFieldData(0, ProbeTimeField, FloatToStr(Value));
end;

procedure TPartData.SetRebuildTime(const Value: TDateTime);
begin
  FRebuildTime := Value;
  SetFieldData(0, RebuildTimeField, FloatToStr(Value));
end;

procedure TPartData.SetShuttleID(const Value: WideString);
begin
  FShuttleID := Value;
  SetFieldData(0, ShuttleIDField, Value);
end;

procedure TPartData.SetShuttleSN(const Value: Integer);
begin
  FShuttleSN := Value;
  SetFieldData(0, ShuttleSNField, IntToStr(Value));
end;

procedure TPartData.SetStartTime(const Value: TDateTime);
begin
  FStartTime := Value;
  SetFieldData(0, StartTimeField, FloatToStr(Value));
end;

procedure TPartData.SetStatusFlags(const Value: TTagStatusFlags);
begin
  FStatusFlags := Value;
  SetFieldData(0, StatusFlagsField, IntToStr(Byte(Value)));
end;

procedure TPartData.SetToDo(Index: Integer; const Value: Boolean);
begin
  OpToDoTable[Index] := Value;
end;

procedure TPartData.SetValidLocation(const Value: Integer);
begin
  FValidLocation := Value;
  SetFieldData(0, ValidLocationField, IntToStr(Value));
end;

end.



