unit OCTUtils;

interface

uses NamedPlugin, SysUtils;

const
  OPTableSize = 129;
  OCTOpsName = 'OpOCT';
  DoneOpsName = 'OpDone';
  ToDoOpsName = 'OpToDo';

type
  TDurationTimer = class(TObject)
  private
    Counting : Boolean;
    Total : Int64;
    LastStart : Int64;
  public
    procedure Clock(AEn : Boolean);
    function Duration : Double;
    procedure Reset;
  end;

  TTagDefinitions = record
    Ini : string;
    Tag : string;
    Owned : Boolean;
    TagType : string;
  end;

function StorageTimeToDateTime(const St : string) : TDateTime;

implementation

{ TDurationTimer }


function StorageTimeToDateTime(const St : string) : TDateTime;
var OldDateSeparator : Char;
    OldShortDateFormat : string;
    OldShortTimeFormat : string;
//    TimeValue : WideString;
begin
  Result := Now + StrToFloatDef(St, -1);
  if(Result < Now) then begin
    OldDateSeparator := DateSeparator;
    OldShortDateFormat := ShortDateFormat;
    OldShortTimeFormat := ShortTimeFormat;

    ShortDateFormat := 'yyyy/mm/dd';
    ShortTimeFormat := 'hh:mm:ss';
    DateSeparator := '-';
    try
      try
        Result := StrToDateTime(ST);
      except
        Result := Now;
      end;
    finally
      DateSeparator := OldDateSeparator;
      ShortDateFormat := OldShortDateFormat;
      ShortTimeFormat := OldShortTimeFormat;
    end;
  end;
end;

procedure TDurationTimer.Clock(AEn: Boolean);
begin
  if Counting and not AEn then begin
    Total := Total + IOSweep^ - LastStart;
  end else if not Counting and AEn then begin
    LastStart := IOSweep^;
  end;
  Counting := AEn;
end;

function TDurationTimer.Duration: Double;
begin
  if Counting then begin
    Result := Total + IOSweep^ - LastStart;
  end else begin
    Result := Total;
  end;
  Result := Result / 1000;
end;

procedure TDurationTimer.Reset;
begin
  Counting := False;
  Total := 0;
end;

end.
