unit ToolDB;

interface

uses SysUtils, Classes, IStringArray, NamedPlugin, OCTTypes, ToolData;

type
  TToolDB = class;

  TToolDBItem = class(TObject)
  private
    Index : Integer;
    Host : TToolDB;

    FMinToolLength: Double;
    FMaxToolLength: Double;
    FBarrelCount: Integer;
    FShuttleID: WideString;
    procedure SetBarrelCount(const Value: Integer);
    procedure SetMaxToolLength(const Value: Double);
    procedure SetMinToolLength(const Value: Double);
    procedure SetShuttleID(const Value: WideString);
  protected
    procedure RebuildInternalValues(AIndex : Integer);
  public
    constructor Create(AHost : TToolDB; AIndex : Integer);
    destructor Destroy; override;
    property ShuttleID : WideString read FShuttleID write SetShuttleID;
    property BarrelCount : Integer read FBarrelCount write SetBarrelCount;
    property MaxToolLength : Double read FMaxToolLength write SetMaxToolLength;
    property MinToolLength : Double read FMinToolLength write SetMinToolLength;
    procedure Assign(T : TToolData);
  end;

  TToolDB = class(TAbstractStringData)
  private
    ToolList : TList;
    function GetTool(index: Integer): TToolDBItem;
    function IndexOf(Tool : TToolDBItem) : Integer;
  protected
    procedure RebuildSingleValue(Row, Col :Integer); override;
    procedure RebuildInternalValues; override;
  public
    procedure CreateTable;
    procedure RebuildIndex;
    constructor Create;
    destructor Destroy; override;
    function AddTool : TToolDBItem;
    function Count : Integer;
    property Tool[index : Integer] : TToolDBItem read GetTool; default;
    procedure Clean; override;
    procedure RemoveTool(T : TToolDBItem);
    function FindTool(AShuttleID : string) : TToolDBItem;
    function MaxUseableForTool(AShuttleID : string) : Double;
    function TotalUseableForTool(AShuttleID : string) : Double;
  end;

implementation

{ TToolDBItem }

procedure TToolDBItem.Assign(T: TToolData);
begin
  ShuttleID := T.ShuttleID;
  BarrelCount := T.BarrelCount;
  MaxToolLength := T.MaxToolLength;
  MinToolLength := T.MinToolLength;
end;

constructor TToolDBItem.Create(AHost: TToolDB; AIndex: Integer);
begin
  inherited Create;
  Index := AIndex;
  Host := AHost;
end;

destructor TToolDBItem.Destroy;
begin
  inherited;
end;

procedure TToolDBItem.RebuildInternalValues(AIndex: Integer);
begin
  Index := AIndex;
  FShuttleID := Host.GetFieldString(Index, ShuttleIDField);
  FBarrelCount := Host.GetFieldInteger(Index, BarrelCountField);
  FMaxToolLength := Host.GetFieldDouble(Index, MaxToolLengthField);
  FMinToolLength := Host.GetFieldDouble(Index, MinToolLengthField);
end;

procedure TToolDBItem.SetBarrelCount(const Value: Integer);
begin
  FBarrelCount := Value;
  Host.SetFieldData(Index, BarrelCountField, IntToStr(Value));
end;

procedure TToolDBItem.SetMaxToolLength(const Value: Double);
begin
  FMaxToolLength := Value;
  Host.SetFieldData(Index, MaxToolLengthField, FloatToStr(Value));
end;

procedure TToolDBItem.SetMinToolLength(const Value: Double);
begin
  FMinToolLength := Value;
  Host.SetFieldData(Index, MinToolLengthField, FloatToStr(Value));
end;

procedure TToolDBItem.SetShuttleID(const Value: WideString);
begin
  FShuttleID := Value;
  Host.SetFieldData(Index, ShuttleIDField, Value);
end;

{ TToolDB }

function TToolDB.AddTool: TToolDBItem;
begin
  Lock;
  try
    Result := TToolDBItem.Create(Self, -1);
    Result.Index := ITable.RecordCount;

    ToolList.Add(Result);

    ITable.Append(1);
  finally
    Unlock;
  end;
end;

procedure TToolDB.Clean;
begin
  BeginUpdate;
  try
    ITable.Clear;
    while ToolList.Count > 0 do begin
      TObject(ToolList[0]).Free;
      ToolList.Delete(0);
    end;
  finally
    EndUpdate;
  end;
end;

function TToolDB.Count: Integer;
begin
  Result := ToolList.Count;
end;

constructor TToolDB.Create;
begin
  inherited;

  if StringArraySet.CreateArray('ToolDB', Self, ITable) <> AAR_OK then begin
    StringArraySet.UseArray('ToolDB', Self, ITable);
  end else begin
    CreateTable;
  end;
  ToolList := TList.Create;
  RebuildInternalValues;
end;

procedure TToolDB.CreateTable;
begin
  BeginUpdate;
  try
    ITable.Clear;
    ITable.AddField(ShuttleIDField);
    ITable.AddField(BarrelCountField);
    ITable.AddField(MaxToolLengthField);
    ITable.AddField(MinToolLengthField);
  finally
    EndUpdate;
  end;
end;

destructor TToolDB.Destroy;
begin
  ITable := nil;
  inherited;
end;

function TToolDB.FindTool(AShuttleID: string): TToolDBItem;
var I : Integer;
begin
  for I := 0 to Count - 1 do begin
    Result := Tool[I];
    if CompareText(AShuttleID, Result.ShuttleID) = 0 then
      Exit;
  end;
  Result := nil;
end;

function TToolDB.GetTool(index: Integer): TToolDBItem;
begin
  Result := TToolDBItem(ToolList[Index]);
end;

function TToolDB.IndexOf(Tool: TToolDBItem): Integer;
begin
  Result := ToolList.IndexOf(Tool);
end;

procedure TToolDB.RebuildIndex;
var I : Integer;
begin
  for I := 0 to Count - 1 do begin
    Tool[I].Index := I;
  end;
end;

procedure TToolDB.RebuildInternalValues;
var I : Integer;
    Tmp : TToolDBItem;
begin
  for I := ITable.RecordCount to ToolList.Count - 1 do begin
    Tool[0].Free;
    ToolList.Delete(0);
  end;

  for I := 0 to ITable.RecordCount - 1 do begin
    if I >= ToolList.Count then begin
      Tmp := TToolDBItem.Create(Self, I);
      ToolList.Add(Tmp);
      Tmp.RebuildInternalValues(I);
    end else begin
      Tool[I].RebuildInternalValues(I);
    end;
  end;
end;

procedure TToolDB.RebuildSingleValue(Row, Col: Integer);
begin
  Tool[Row].RebuildInternalValues(Row);
end;

procedure TToolDB.RemoveTool(T: TToolDBItem);
var I : Integer;
begin
  BeginUpdate;
  try
    I := IndexOf(T);
    ITable.Delete(T.Index);
    T.Free;
    ToolList.Delete(I);
    RebuildIndex;
  finally
    EndUpdate;
  end;
end;

function TToolDB.MaxUseableForTool(AShuttleID : string) : Double;
var
ToolData : TToolDBItem;
begin
Result := 0;
ToolData :=  FindTool(AShuttleID);
if ToolData <> nil then
  begin
  Result := ToolData.FMaxToolLength-ToolData.MinToolLength;
  end;
end;


function TToolDB.TotalUseableForTool(AShuttleID: string): Double;
var
ToolData : TToolDBItem;
begin
Result := 0;
ToolData :=  FindTool(AShuttleID);

if ToolData <> nil then
  begin
  if ToolData.BarrelCount = 0 then
    ToolData.BarrelCount := 1;
  Result := (ToolData.FMaxToolLength-ToolData.MinToolLength) * ToolData.BarrelCount;
  end;
end;

end.
