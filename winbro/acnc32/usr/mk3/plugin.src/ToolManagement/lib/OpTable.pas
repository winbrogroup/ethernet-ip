unit OpTable;

interface

uses SysUtils, Classes, IStringArray, NamedPlugin, OCTTypes;

type
  TOpTable = class(TAbstractStringData)
  private
//    ITable : IAcnc32StringArray;
    FChecked : array of Boolean;
    Length : Integer;
    function GetChecked(Index: Integer): Boolean;
    procedure SetChecked(Index: Integer; const Value: Boolean);
    procedure CreateTable;
  protected
    procedure RebuildInternalValues; override;
    procedure RebuildSingleValue(Row, Col :Integer); override;
  public
    constructor Create(TableName : string; ALength : Integer);
    procedure Close;
    destructor Destroy; override;
    property Checked[Index : Integer] : Boolean read GetChecked write SetChecked; default;
    function Count : Integer;
  end;

implementation

{ TOpTable }

procedure TOpTable.Close;
begin
  ITable := nil;
end;

constructor TOpTable.Create(TableName: string; ALength : Integer);
begin
  if StringArraySet.CreateArray(TableName, Self, ITable) <> AAR_OK then
    StringArraySet.UseArray(TableName, Self, ITable);

  Length := ALength;
  SetLength(FChecked, Length);
  CreateTable;
end;

procedure TOpTable.CreateTable;
var I : Integer;
    FieldName : WideString;
begin
  ITable.Clear;
  ITable.AddField('OPS');
  for I := ITable.FieldCount - 1 downto 0 do begin
    ITable.FieldNameAtIndex(I, FieldName);
    if CompareText(FieldName, 'OPS') <> 0 then
      ITable.DeleteField(FieldName, Self)
  end;

  ITable.Append(Length, Self);

  for I := 0 to Length - 1 do begin
    ITable.SetValue(I, 0, '0', Self);
    FChecked[I] := False;
  end;
end;

destructor TOpTable.Destroy;
begin
  ITable := nil;
  inherited;
end;

function TOpTable.GetChecked(Index: Integer): Boolean;
begin
  Result := FChecked[Index];
end;

procedure TOpTable.SetChecked(Index: Integer; const Value: Boolean);
begin
  FChecked[Index] := Value;
  if Value then
    ITable.SetValue(Index, 0, '1', Self)
  else
    ITable.SetValue(Index, 0, '0', Self)
end;


procedure TOpTable.RebuildInternalValues;
var Value : WideString;
    I : Integer;
begin
  for I := 0 to Length - 1 do begin
    ITable.GetValue(I, 0, Value);
    FChecked[I] := Value <> '0';
  end;
end;

procedure TOpTable.RebuildSingleValue(Row, Col: Integer);
var Value : WideString;
begin
  ITable.GetValue(Row, 0, Value);
  FChecked[Row] := Value <> '0';
end;

function TOpTable.Count: Integer;
begin
  Result := ITable.RecordCount;
end;

end.
