unit ActionList;

interface
uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs,ExtCtrls,Grids,StdCtrls,IniFiles,ThreelineButton;


type

  tTLColIds = (tlcNumber,tlcGoto,TlcBladeUnload,tlcBladeLoad,
              tlcChangeElectrode,tlcChangeTool,tlcOperation,tlcTool,
              tlcTransferTo,tlcOp,tlcMC,tlcTick,tlcSpareTime);

  TActionLine  = record
    public
    ThisOpNumber : String;
    GoToMachine : String;
    BladeUnloadReqd : String;
    BladeLoadReqd : String;
    ChangeElectrode : String;
    ChangeTool : String;
    OperationText : String;
    ToolNumberText : String;
    TransferMCText : String;
    NextOpNumber : String;
    NextMachineID : String;
    IsDone : String;
    TimeSec : Integer;
    TimeString : String;
    OPPName : String;
    end;

  PAction = ^TactionLine;

  TActionListDisplay = class(TPanel)
  private
    TitleGrid : TStringGrid;
    DataGrid : TStringGrid;
    TaskTimer : TTimer;
    ActionList : Tlist;
    ListUpdating : Boolean;
    FCurrentAction : PAction;
    FCurrentTaskLine: Integer;
    FCurrentTaskTimeInSeconds: Integer;
    FTaskListComplete : Boolean;
    procedure DataCellDraw (Sender: TObject; ACol, ARow: Longint; Rect: TRect; State: TGridDrawState);
    procedure TitleCellDraw (Sender: TObject; ACol, ARow: Longint; Rect: TRect; State: TGridDrawState);
    procedure OutputTextForRowCol(ARow,ACol: Integer;Rect : Trect);
    procedure CleanList;
    function ANextToken(var Instring: String): String;
    function GetTaskCount: Integer;
    procedure StartActionList;
    procedure TaskTimerOut(Sender : TObject);
    function SecsToTimeStr(InSecs: Integer): String;
    procedure SetCurrentTaskLine(const Value: Integer);
    procedure RestartTimer;
  protected
  public
    function IsNamedOPPInhibited(OPPName : String): Boolean;
    procedure SetAllTasksNotDone;
    procedure AddTaskFromString(TaskString : String);
    procedure UpdateForNewList;
    procedure BeginUpdate;
    procedure EndUpdate;
    procedure Initialise;
    procedure TickCurrentAction;
    procedure UndoLastTaskDone;

    Constructor Create(AOwner : TComponent);override;
    Destructor Destroy; override;
    property CurrentTaskLine : Integer read FCurrentTaskLine write SetCurrentTaskLine;
    property TaskCount : Integer read GetTaskCount;
    property TaskListComplete : Boolean read FTaskListComplete;
  end;

  TActionListPanel = class(TPanel)
  private
    TaskDoneButton : TThreeLineButton;
    UndoButton : TThreeLineButton;
    FActionList : TActionListDisplay;
    ControlPanel : TPanel;
    FCurrentTaskLine: Integer;
    procedure Initialise;
    procedure TickCurrentAction(Sender: TObject; Button: TMouseButton;
    Shift: TShiftState; X, Y: Integer);
    procedure UndoLastTick(Sender: TObject; Button: TMouseButton;
    Shift: TShiftState; X, Y: Integer);
    procedure ResetAllActions(Sender: TObject; Button: TMouseButton;
    Shift: TShiftState; X, Y: Integer);


  protected
  public
    function IsNamedOPPInhibited(OPPName : String): Boolean;
    function LoadActionListFile(Filename : String): Boolean;
    procedure Resize;override;
    procedure InitialiseActions;
    procedure StartActionList;
    Constructor Create(AOwner : TComponent);override;
    property CurrentTaskLine : Integer read FCurrentTaskLine;
  end;


implementation

uses DBGrids;

{ TActionListPanel }

constructor TActionListPanel.Create(AOwner: TComponent);
begin
  inherited;
//  BevelInner :=bvLowered;
//  BevelOuter :=bvRaised;
  ControlPanel := TPanel.create(Self);
  ControlPanel.Parent := Self;
  with ControlPanel do
    begin
    Align := alBottom;
    BevelOuter :=bvRaised;
    BevelInner := bvLowered;
    Height := 55;
    end;


  FActionList := TActionListDisplay.create(Self);
  with FactionList do
    begin
    Align :=alClient;
    Parent := self;
    Ctl3D := False;

    end;

  TaskDoneButton := TThreeLineButton.Create(Self);
  with TaskDoneButton do
    begin
    Legend :='Task~Done';
    Width := 100;
    Height := 50;
    Top := 2;
    Parent := ControlPanel;
    IsEnabled := True;
    Color := clInfoBk;
    OnMouseUp := TickCurrentAction;
    end;

  UndoButton := TThreeLineButton.Create(Self);
  with UndoButton do
    begin
    Legend :='Re-acvtivate~Last Task';
    Width := 100;
    Height := 50;
    Top := 2;
    Parent := ControlPanel;
    IsEnabled := True;
    Color := clInfoBk;
    OnMouseUp := UndoLastTick;
    end;

end;


function TActionListPanel.LoadActionListFile(Filename: String): Boolean;
var
 TaskFile   : TIniFile;
 TaskCount  : Integer;
 TaskKey    : String;
 TaskNumber : Integer;
 TaskString : String;
begin
  Result := False;
  if FileExists(FileName) then
    begin
    FActionList.CleanList;
    TaskFile := TIniFile.Create(Filename);
    try
    TaskCount := TaskFile.ReadInteger('OCT','TaskCount',0);
    if TaskCount > 0 then
      begin
      For TaskNumber := 1 to TaskCount do
        begin
        TaskKey := Format('T%d',[TaskNumber]);
        TaskString := TaskFile.ReadString('TASKS',TaskKey,'');
        if TaskString <> '' then
          begin
          FActionList.AddTaskFromString(TaskString);
          end;
      end;
      FActionList.UpdateForNewList;
      end;
    finally
    TaskFile.Free;
    end;
    end;
 Initialise;
 Result := FActionList.TaskCount > 0;
end;

procedure TActionListPanel.Resize;
var
TotalWidth : Integer;
begin
  inherited;
  FActionList.BeginUpdate;
  TotalWidth := Width-4;;

  try
  with FActionList.DataGrid do
    begin
    try
    ColWidths[0]:= 45;
    ColWidths[4] := 70;
    ColWidths[5] := 70;
    ColWidths[8] := 110;
    ColWidths[6] := TotalWidth-812;
    finally
    end;
    end;

  with FActionList.TitleGrid do
    begin
    try
    ColWidths[0]:= 45;
    ColWidths[4] := 70;
    ColWidths[5] := 70;
    ColWidths[8] := 110;
    ColWidths[6] := TotalWidth-812;
    finally
    end;
    end;
  TaskDoneButton.Left := ControlPanel.Width-100;
  UndoButton.Left := 2;
  finally
    FActionList.EndUpdate;
  end;


end;


{ TActionList }

procedure TActionListDisplay.DataCellDraw(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
var
InnerRect : TRect;
OpPosLeft : Integer;
begin
if ListUpdating Then exit;
InnerRect.Top := Rect.Top+3;
InnerRect.Left := Rect.Left+3;
InnerRect.Bottom := Rect.Bottom-3;
InnerRect.Right := Rect.Right-3;
with DataGrid.Canvas do
begin
 Font.Size := 12;
 Brush.Color := clWhite;
 FillRect(InnerRect);
  // Here for text rows below fixed header
  if ARow < FCurrentTaskLine then
    begin
    Brush.Color := clSilver+$303030;
    end
  else if ARow = FCurrentTaskLine then
    begin
    Brush.Color := clMoneyGreen;
    end
  else
    begin
    Brush.Color := clwhite;
    end;
  FillRect(Rect);
  Pen.Width := 1;
   case ACol of
    0,1,2,4,6,7,8,9,10,11:
    begin
    MoveTo(Rect.Right,Rect.Top);
    Pen.Width :=1;
    LineTo(Rect.Left,Rect.Top);
    Pen.Width :=2;
    LineTo(Rect.Left,Rect.Bottom);
    Pen.Width :=3;
    if ARow = DataGrid.RowCount-1 then
      begin
      LineTo(Rect.Right,Rect.Bottom);
      end;
    end;

    3,5:
    begin
    MoveTo(Rect.Right,Rect.Top);
    Pen.Width :=1;
    LineTo(Rect.Left-1,Rect.Top);
    LineTo(Rect.Left-1,Rect.Bottom);
    Pen.Width :=3;
    if ARow = DataGrid.RowCount-1 then
      begin
      LineTo(Rect.Right,Rect.Bottom);
      end;
    end;

    12:
    begin
    Pen.Width :=1;
    MoveTo(Rect.Left,Rect.Top);
    LineTo(Rect.Right-1,Rect.Top);
    Pen.Width :=2;
    LineTo(Rect.Right-1,Rect.Bottom);
    MoveTo(Rect.Left,Rect.Top);
    LineTo(Rect.Left,Rect.Bottom);
    if ARow = DataGrid.RowCount-1 then
      begin
      LineTo(Rect.Right,Rect.Bottom);
      end;
    end;
   end; // case
 OutputTextForRowCol(ARow,ACol,Rect);
end; // with Canvas
end;

function TActionListDisplay.ANextToken(Var Instring : String):String;
var
CommaPos : Integer;
begin
  result := '';
  Commapos := Pos(',',Instring);
  if CommaPos > 0 then
    begin
    Result := Copy(InString,1,CommaPos-1);
    InString := Copy(Instring,CommaPOs+1,1000);
    end
  else
    begin
    Result := InString;
    InString := '';
    end
end;

procedure TActionListDisplay.TitleCellDraw(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
var
InnerRect : TRect;
OpPosLeft : Integer;
begin
if ListUpdating Then exit;
InnerRect.Top := Rect.Top+3;
InnerRect.Left := Rect.Left+3;
InnerRect.Bottom := Rect.Bottom-3;
InnerRect.Right := Rect.Right-3;
with TitleGrid.Canvas do
begin
 Font.Size := 12;
 Brush.Color := clWhite;
 FillRect(InnerRect);
 case ARow of
  0:
  begin
  Pen.Color := clBlack;
  Pen.Width := 2;
  case ACol of
    0:
    begin
    MoveTo(Rect.Right,Rect.Top);
    LineTo(Rect.Left,Rect.Top);
    LineTo(Rect.Left,Rect.Bottom);
    LineTo(Rect.Right,Rect.Bottom);
    end;

    1,2,3,5,6,7,9:
    begin
    MoveTo(Rect.Right,Rect.Top);
    LineTo(Rect.Left,Rect.Top);
    MoveTo(Rect.Left,Rect.Bottom);
    LineTo(Rect.Right,Rect.Bottom);
    end;

    4:
    begin
    MoveTo(Rect.Right,Rect.Top);
    LineTo(Rect.Left,Rect.Top);
    MoveTo(Rect.Left,Rect.Bottom);
    LineTo(Rect.Right,Rect.Bottom);
    TextOut(Rect.left-20,Rect.Top+2,'This Op');
    end;

    8:
    begin
    MoveTo(Rect.Right,Rect.Top);
    LineTo(Rect.Left,Rect.Top);
    LineTo(Rect.Left,Rect.Bottom);
    LineTo(Rect.Right,Rect.Bottom);
    end;

    10:
    begin
    MoveTo(Rect.Right,Rect.Top);
    LineTo(Rect.Left,Rect.Top);
    MoveTo(Rect.Left,Rect.Bottom);
    LineTo(Rect.Right,Rect.Bottom);
    TextOut(Rect.Left-40,Rect.Top+2,'Next Op');
    end;

    11:
    begin
    MoveTo(Rect.Left,Rect.Top);
    LineTo(Rect.Right,Rect.Top);
    LineTo(Rect.Right,Rect.Bottom);
    LineTo(Rect.Left,Rect.Bottom);
    end;

    12:
    begin
    MoveTo(Rect.Left,Rect.Top);
    LineTo(Rect.Right-1,Rect.Top);
    LineTo(Rect.Right-1,Rect.Bottom);
    TextOut(Rect.left+10,Rect.Top+2,'Spare');
    end;
    end; // Acol case
   end;
  1:
  begin
  Pen.Color := clBlack;
  Pen.Width := 2;
  case ACol of

    0,1,6,7,8,9,10:
    begin
    MoveTo(Rect.Left,Rect.Top);
    LineTo(Rect.Left,Rect.Bottom);
    end;

    2:
    begin
    MoveTo(Rect.Left,Rect.Top);
    LineTo(Rect.Left,Rect.Bottom);
    end;

    3:
    begin
    TextOut(Rect.Left-20,Rect.Top+2,'Blade');
    end;

    4:
    begin
    MoveTo(Rect.Left,Rect.Top);
    LineTo(Rect.Left,Rect.Bottom);
    end;

    5:
    begin
    TextOut(Rect.Left-30,Rect.Top+2,'Change');
    end;

    11:
    begin
    MoveTo(Rect.Left,Rect.Top);
    LineTo(Rect.Left,Rect.Bottom);
    TextOut(Rect.Left+15,Rect.Top+2,'Tick');
    end;

    12:
    begin
    MoveTo(Rect.Left,Rect.Top);
    LineTo(Rect.Left,Rect.Bottom);
    MoveTo(Rect.Right-1,Rect.Top);
    LineTo(Rect.Right-1,Rect.Bottom);
    TextOut(Rect.Left+10,Rect.Top+2,'Time');
    end;

  end;
  end;

  2:
  begin
  Pen.Color := clBlack;
  Pen.Width := 2;
  case ACol of
    0:
    begin
    MoveTo(Rect.Left,Rect.Top);
    LineTo(Rect.Left,Rect.Bottom);
    LineTo(Rect.Right,Rect.Bottom);
    TextOut(Rect.Left+10,Rect.Top+2,'No.');
    end;

    1:
    begin
    MoveTo(Rect.Left,Rect.Top);
    LineTo(Rect.Left,Rect.Bottom);
    LineTo(Rect.Right,Rect.Bottom);
    TextOut(Rect.Left+10,Rect.Top+2,'Go To');
    end;

    2:
    begin
    MoveTo(Rect.Left,Rect.Top);
    LineTo(Rect.Left,Rect.Bottom);
    LineTo(Rect.Right,Rect.Bottom);
    TextOut(Rect.Left+10,Rect.Top+2,'Unload');
    end;

    3:
    begin
    MoveTo(Rect.Right,Rect.Top);
    LineTo(Rect.Right,Rect.Bottom);
    LineTo(Rect.Left,Rect.Bottom);
    TextOut(Rect.Left+10,Rect.Top+2,'Load');
    end;

    4:
    begin
    MoveTo(Rect.Left,Rect.Top);
    LineTo(Rect.Left,Rect.Bottom);
    LineTo(Rect.Right,Rect.Bottom);
    TextOut(Rect.Left+5,Rect.Top+2,'Electrode');
    end;

    5:
    begin
    MoveTo(Rect.Right,Rect.Top);
    LineTo(Rect.Right,Rect.Bottom);
    LineTo(Rect.Left,Rect.Bottom);
    TextOut(Rect.Left+20,Rect.Top+2,'Tool');
    end;

    6:
    begin
    MoveTo(Rect.Right,Rect.Top);
    LineTo(Rect.Right,Rect.Bottom);
    LineTo(Rect.Left,Rect.Bottom);
    OpPosLeft := (TitleGrid.ColWidths[6]-65) div 2;
    TextOut(Rect.Left+OpPosLeft,Rect.Top+2,'Operation');

    end;

    7:
    begin
    MoveTo(Rect.Right,Rect.Top);
    LineTo(Rect.Right,Rect.Bottom);
    LineTo(Rect.Left,Rect.Bottom);
    TextOut(Rect.Left+15,Rect.Top+2,'Tool');
    end;

    8:
    begin
    MoveTo(Rect.Right,Rect.Top);
    LineTo(Rect.Right,Rect.Bottom);
    LineTo(Rect.Left,Rect.Bottom);
    TextOut(Rect.Left+15,Rect.Top+2,'Transfer To');
    end;

    9:
    begin
    MoveTo(Rect.Right,Rect.Top);
    LineTo(Rect.Right,Rect.Bottom);
    LineTo(Rect.Left,Rect.Bottom);
    TextOut(Rect.Left+25,Rect.Top+2,'Op');
    end;

    10:
    begin
    MoveTo(Rect.Right,Rect.Top);
    LineTo(Rect.Right,Rect.Bottom);
    LineTo(Rect.Left,Rect.Bottom);
    TextOut(Rect.Left+25,Rect.Top+2,'MC');
    end;

    11:
    begin
    MoveTo(Rect.Right,Rect.Top);
    LineTo(Rect.Right,Rect.Bottom);
    LineTo(Rect.Left,Rect.Bottom);
    end;

    12:
    begin
    MoveTo(Rect.Right-1,Rect.Top);
    LineTo(Rect.Right-1,Rect.Bottom);
    LineTo(Rect.Left,Rect.Bottom);
    LineTo(Rect.Left,Rect.Top);
    TextOut(Rect.Left+10,Rect.Top+2,'(min.)');
    end;
  end;//Col case
  end;// Row Case
  end; // with Canvas
  end;
end;


procedure TActionListDisplay.AddTaskFromString(TaskString: String);
var
ListEntry : PAction;
NextToken : String;
LineBuffer: String;
begin
  New(ListEntry);
  LineBuffer := TaskString;
  ListEntry.ThisOpNumber := ANextToken(LineBuffer);
  ListEntry.GoToMachine :=  ANextToken(LineBuffer);
  ListEntry.BladeUnloadReqd := ANextToken(LineBuffer);
  ListEntry.BladeLoadReqd := aNextToken(LineBuffer);
  ListEntry.ChangeElectrode := aNextToken(LineBuffer);
  ListEntry.ChangeTool := aNextToken(LineBuffer);
  ListEntry.OperationText := aNextToken(LineBuffer);
  ListEntry.ToolNumberText := aNextToken(LineBuffer);
  ListEntry.TransferMCText := aNextToken(LineBuffer);
  ListEntry.NextOpNumber := aNextToken(LineBuffer);
  ListEntry.NextMachineID:= aNextToken(LineBuffer);
  ListEntry.IsDone := aNextToken(LineBuffer);
  ListEntry.TimeSec := StrToIntDef(aNextToken(LineBuffer),0);
  ListEntry.OPPName := UpperCase(aNextToken(LineBuffer));
  ActionList.Add(ListEntry);
end;



procedure TActionListDisplay.CleanList;
var
ListEntry : PAction;
begin
 while ActionList.Count > 0 do
  begin
  ListEntry := ActionList[0];
  Dispose(ListEntry);
  ActionList.Delete(0);
  end;
end;

constructor TActionListDisplay.Create(AOwner: TComponent);
begin
  inherited;
  TitleGrid := TStringGrid.Create(Self);
  with TitleGrid do
    begin
    Parent := Self;
    Align := alTop;
    ColCount := 13;
    RowCount := 3;
    FixedRows := 2;
    OnDrawCell := TitleCellDraw;
    DefaultDrawing := False;
    ScrollBars := ssNone;
    OPtions := [];
    BevelInner := bvNone;
    BevelOuter := bvNone;
    ctl3D := False;
    Height := (DefaultRowHeight*3)+5;
    end;

  DataGrid := TStringGrid.Create(Self);
  with DataGrid do
    begin
    Parent := Self;
    ColCount := 13;
    RowCount := 1;
    FixedRows := 0;
    OPtions := [];
    Align := alClient;
    OnDrawCell := DataCellDraw;
    DefaultDrawing := False;
    ScrollBars := ssNone;
    BevelInner := bvNone;
    BevelOuter := bvNone;
    ctl3D := False;
    end;
  FCurrentAction := nil;
  TaskTimer := TTimer.Create(Self);

  with TaskTimer do
    begin
    Enabled := False;
    Interval := 1000;
    OnTimer := TaskTimerOut;
    end;

  ActionList := TList.Create;
  ListUpdating := False;
end;

destructor TActionListDisplay.Destroy;
begin
  CleanList;
  inherited;
end;

procedure TActionListDisplay.OutputTextForRowCol(ARow, ACol: Integer;Rect : Trect);
var
MidCell : Integer;
CentreTextStart : Integer;
begin
with DataGrid do
begin
 If ARow < FCurrentTaskLine then
  Canvas.Font.Color := clGray
 else
  Canvas.Font.Color := clBlack;

  MidCell :=Rect.Left+ ((Rect.Right-Rect.Left) div 2);
  CentreTextStart := MidCell-(Canvas.TextWidth(Cells[Acol,Arow])div 2);

  case ACol of
    2,3,4:
    begin
    if Cells[Acol,Arow] <> '' then
      begin
      Canvas.TextOut(MidCell-5,Rect.Top+2,'X');
      end
    end;

    11:
    // Tick if not empty
    begin
    if Cells[Acol,ARow] <> '' then
      begin
      DataGrid.Canvas.MoveTo(MidCell-5,Rect.Bottom-11);
      DataGrid.Canvas.LineTo(MidCell,Rect.Bottom-5);
      DataGrid.Canvas.LineTo(MidCell+10,Rect.Top+5);
      end;
    end;

    else
      begin
      Canvas.TextOut(CentreTextStart,Rect.Top+2,Cells[Acol,Arow]);
      end

  end; //case
end; // with DataGrid
end;


function TActionListDisplay.GetTaskCount: Integer;
begin
  Result := ActionList.Count;
end;
procedure TActionListDisplay.SetAllTasksNotDone;
var
Lentry : PAction;
EntryNumber : Integer;
begin
 if ActionList.Count > 0 then
  begin
  FTaskListComplete := False;
  For EntryNumber := 0 to ActionList.Count-1 do
    begin
    Lentry := ActionList[EntryNumber];
    Lentry.IsDone := '';
    end
  end;
end;


procedure TActionListDisplay.UpdateForNewList;
var
LEntry : PAction;
ActionNumber : Integer;
begin
with DataGrid do
  begin
  RowCount := ActionList.Count;
  For ActionNumber := 0 to ActionList.Count-1 do
    begin
    LEntry := ActionList[ActionNumber];
    Cells[0,ActionNumber] := Lentry.ThisOpNumber;
    Cells[1,ActionNumber] := Lentry.GoToMachine;
    Cells[2,ActionNumber] := Lentry.BladeUnloadReqd;
    Cells[3,ActionNumber] := Lentry.BladeloadReqd;
    Cells[4,ActionNumber] := Lentry.ChangeElectrode;
    Cells[5,ActionNumber] := Lentry.ChangeTool;
    Cells[6,ActionNumber] := Lentry.OperationText;
    Cells[7,ActionNumber] := Lentry.ToolNumberText;
    Cells[8,ActionNumber] := Lentry.TransferMCText;
    Cells[9,ActionNumber] := Lentry.NextOpNumber;
    Cells[10,ActionNumber] := Lentry.NextMachineID;
    end;
  end;
end;

procedure TActionListDisplay.EndUpdate;
begin
 ListUpdating := False;
 TitleGrid.Invalidate;
 DataGrid.Invalidate;
 Visible := False;
 Visible := True;

end;

procedure TActionListDisplay.BeginUpdate;
begin
ListUpdating := True;
end;

procedure TActionListDisplay.Initialise;
var
GridRow : Integer;
begin
BeginUpdate;
try
SetAllTasksNotDone;
CurrentTaskLine := -1;
For GridRow := 0 to DataGrid.RowCount-1 do
  begin
  with DataGrid do
    begin
    cells[11,GridRow] := '';
    cells[12,GridRow] := '';
    end;
  end;
FCurrentAction := nil;
finally
EndUpdate;
end;
end;

procedure TActionListDisplay.StartActionList;
begin
BeginUpdate;
try
Initialise;
if ActionList.Count > 0 then
  begin
  CurrentTaskLine := 0; // this is 0 based
  FCurrentAction := ActionList[FCurrentTaskLine];
  FCurrentTaskTimeInSeconds := FCurrentAction.TimeSec;
  RestartTimer;
  end;
finally
EndUpdate;
end;
end;


procedure TActionListPanel.Initialise;
begin
  FActionList.Initialise;
end;


procedure TActionListPanel.InitialiseActions;
begin
  FActionList.Initialise;
end;

procedure TActionListPanel.TickCurrentAction(Sender: TObject; Button: TMouseButton;
    Shift: TShiftState; X, Y: Integer);
begin
  FActionList.TickCurrentAction;
end;

procedure TActionListPanel.StartActionList;
begin
  FActionList.StartActionList;
end;

function TActionListDisplay.SecsToTimeStr(InSecs : Integer): String;
var
Mins,Secs : Integer;
begin
  Mins := Insecs div 60;
  Secs := Insecs-(Mins*60);
  if Secs < 10 then
    begin
    Result := Format('%d:0%d',[Mins,Secs]);
    end
  else
    begin
    Result := Format('%d:%d',[Mins,Secs]);
    end;
end;

procedure TActionListDisplay.TaskTimerOut(Sender: TObject);
var
TimeString : String;
begin
with DataGrid do
begin
if assigned(FCurrentAction) then
  begin
  if SEnder <> Self then dec(FCurrentTaskTimeInSeconds);
  if FCurrentTaskTimeInSeconds > 0 then
    begin
    FCurrentAction.TimeString := SecsToTimeStr(FCurrentTaskTimeInSeconds);
    Cells[12,FCurrentTaskLine] :=FCurrentAction.TimeString;
    Invalidate;
    end
  else if FCurrentTaskTimeInSeconds = 0 then
    begin
    FCurrentAction.TimeString := SecsToTimeStr(FCurrentTaskTimeInSeconds);
    Cells[12,FCurrentTaskLine] :=FCurrentAction.TimeString;
    Invalidate;
    end
  else
    begin
    end;
  end;
end;// with DataGrid
end;

procedure TActionListDisplay.TickCurrentAction;
begin
  if assigned(FCurrentAction) then
    begin
    FCurrentAction.IsDone := '1';
    DataGrid.Cells[11,FCurrentTaskLine] := '1';
    if FCurrentTaskLine < ActionList.Count-1 then
      begin

      CurrentTaskLine := CurrentTaskLine+1;
      FCurrentAction := ActionList[FCurrentTaskLine];
      FCurrentTaskTimeInSeconds := FCurrentAction.TimeSec;
      TaskTimer.Enabled := True;
      end
    else
      begin
      CurrentTaskLine := FCurrentTaskLine+1;
      FCurrentAction := nil;
      FTaskListComplete := True;
      end;
  RestartTimer;
  end;
end;

procedure TActionListDisplay.UndoLastTaskDone;
begin
if FCurrentTaskLine > 0 then
  begin
  if not assigned(FCurrentAction) then
    begin
    FCurrentAction := ActionList[ActionList.Count-1];
    CurrentTaskLine := ActionList.Count-1;
    end
  else
    begin
    CurrentTaskLine := FCurrentTaskLine-1;
    FCurrentAction := ActionList[FCurrentTaskLine];
    end;

  DataGrid.Cells[12,FCurrentTaskLine+1] := '';
  FCurrentAction.IsDone := '';
  DataGrid.Cells[11,FCurrentTaskLine] := '';
  FCurrentTaskTimeInSeconds := FCurrentAction.TimeSec;
  RestartTimer;
  end;

end;


procedure TActionListDisplay.SetCurrentTaskLine(const Value: Integer);
begin
  BeginUpdate;
  FCurrentTaskLine := Value;
  if FCurrentTaskLine > DataGrid.VisibleRowCount-2 then
    begin
    DataGrid.TopRow := FCurrentTaskLine-DataGrid.VisibleRowCount+1;
    end
  else if (FCurrentTaskLine < DataGrid.TopRow) and (FCurrentTaskLine >-1) then
    begin
    DataGrid.TopRow := FCurrentTaskLine;
    end
  else
    begin
    DataGrid.TopRow := 0;
    end;
 EndUpdate;
end;


procedure TActionListPanel.UndoLastTick(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  FActionList.UndoLastTaskDone;
end;


procedure TActionListDisplay.RestartTimer;
begin
 TaskTimer.Enabled := False;
 TaskTimerOut(Self);
 TaskTimer.Enabled := True;
end;

function TActionListPanel.IsNamedOPPInhibited(OPPName: String): Boolean;
begin
  Result := FActionList.IsNamedOppInhibited(OPPName);
end;

function TActionListDisplay.IsNamedOPPInhibited(OPPName: String): Boolean;
var
DotPos : Integer;
UpperOPPName : String;
begin
  Result := False;
  if assigned(FCurrentAction) then
    begin
    UpperOPPName := UpperCase(OPPName);
    DotPos := Pos('.',UpperOPPName );
    if DotPos > 0 then
      begin
      UpperOPPName := Copy(UpperOPPName,1,DotPos-1);
      end;
    Result := UpperOPPName = FCurrentAction.oppName;
    end;
end;

procedure TActionListPanel.ResetAllActions(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  FActionList.Initialise;
end;

end.

