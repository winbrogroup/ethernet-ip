unit OPPHeader;

interface

uses SysUtils, Classes, AbstractOPPPlugin, StreamTokenizer, PluginInterface;

const
  SectionOperationDescription = 'OPERATIONDESCRIPTION';

type
  TOPPFile = class
    PartType: String;
    Comment: String;
    ToolTypeString : String;
    ElectrodeCount: Integer;
    ElectrodeMap : String;
    ToolUsage: Double;
    CycleTime: Integer;
    OpNumber : Integer;
    ToolXOffset : Double;
    ToolYOffset : Double;
    ToolZOffset : Double;

    AEC_Enabled : INteger;
    AEC_Selected : INteger;
    AEC_InchEntry : Integer;
    AEC_Restroke: Double; // = -30          ; Restroke for tool used in electrode check and qualify
    AEC_StowFeed: Double; // = 2000
    AEC_CheckHoleProgram :Integer;// = 99
    AEC_QualifyStroke : Double; // = 25      ; Max U Forward during one sweep of electrode qualify
    AEC_QualifyRetryLoops : Integer; // = 3
    AEC_ElectrodeClearance : Double; // = 2
    AEC_LoadPositionU : Double; // = 0
    AEC_EjectLength : Double; // = 10
    AEC_DoubleIndex : Integer; // = 0
    AEC_FirstPulseDuration : Double; // = 1
    AEC_SecondPulseDuration : Double; // = 1
    AEC_EjectPulseCount : Integer; // = 1
    AEC_EjectPulse1Duration : Double; // = 1
    AEC_EjectPulse2Duration : Double; // = 1
    AEC_LoadPulseCount : INteger; // = 1

    procedure Load(S: TStreamTokenizer); {from strings}
    procedure Save(S: TStrings); {to strings}
  end;


  // IF you need the active OPPs header information but dont want to add a the
  // 10 or so named layer tags + you want to know when a new file is loaded
  // then use the following object, you will need to add the following line
  // in your local initialization section.
  ///
  //  TOppHeaderConsumer.AddPlugin(SectionOperationDescription, TOppHeaderConsumer);
  // Once this has been added you can access the methods via the global OPPHeaderConsumer

  TOppHeaderConsumer = class(TAbstractOPPPluginConsumer)
  private
    FOnChange : TNotifyEvent;
  public
    OPPFile : TOPPFile;
    constructor Create(aSectionName : string); override;
    procedure Consume(aSectionName, Buffer : PChar); override;
    property OnChange : TNotifyEvent read FOnChange write FOnChange;
  end;

var
  OPPHeaderConsumer : TOPPHeaderConsumer;

implementation

{ TOppHeaderConsumer }

procedure TOppHeaderConsumer.Consume(aSectionName, Buffer: PChar);
var S : TStreamQuoteTokenizer;
begin
  S := TStreamQuoteTokenizer.Create;
  try
    S.SetStream(TStringStream.Create(Buffer));
    S.Seperator := '=';
    S.QuoteChar := '''';
    S.Rewind;
    OppFile.Load(S);
  finally
    S.Free;
  end;

  if Assigned(OnChange) then
    OnChange(Self);
end;



constructor TOppHeaderConsumer.Create(aSectionName : string);
begin
  inherited;
  OPPHeaderConsumer := Self;
  OPPFile := TOPPFile.Create;
end;




{Each individual tool is identified on the Hypertac plug with a
 14 bit number, of which 9 bits identify the type and 5 bits are
 a type serial number. The total uniquely identifies the tool on
 the site}

procedure TOPPFile.Load(S: TStreamTokenizer);
var Token : string;

  function ReadDoubleAssign : Double;
  var Code : Integer;
  begin
    Val(S.ReadStringAssign, Result, Code);
  end;

begin
  PartType := 'None';
  Comment := 'No Ref';
  ToolTypeString := 'Not_Valid'; 
  ElectrodeMap := '000000000000';
  ToolUsage := 0;
  CycleTime := 0;
  ElectrodeCount := 0;
  ToolXOffset := 0;
  ToolYOffset := 0;
  ToolZOffset := 0;
  OpNumber := 1;
  Token := LowerCase(S.Read);
  while not S.EndOfStream do begin
    if Token = 'parttype' then
      PartType := UpperCase(S.ReadStringAssign)
    else if Token = 'comment' then
      Comment := S.ReadStringAssign
    else if Token = 'tooltypestring' then
      ToolTypeString := UpperCase(S.ReadStringAssign)
    else if Token = 'toolusage' then
      ToolUsage := ReadDoubleAssign
    else if Token = 'cycletime' then
      CycleTime := S.ReadIntegerAssign
    else if Token = 'electrodecount' then
      ElectrodeCount := S.ReadIntegerAssign
    else if Token = 'electrodemap' then
      ElectrodeMap := S.ReadStringAssign
    else if Token = 'toolxoffset' then
      ToolXOffset := ReadDoubleAssign
    else if Token = 'toolyoffset' then
      ToolYOffset := ReadDoubleAssign
    else if Token = 'toolzoffset' then
      ToolZOffset := ReadDoubleAssign
    else if Token = 'opnumber' then
      OpNumber := S.ReadIntegerAssign
    else if Token = 'toolzoffset' then
      ToolZOffset := ReadDoubleAssign
    else if Token = 'aec_selected' then
      AEC_Selected := S.ReadIntegerAssign
    else if Token = 'aec_inchentry' then
      AEC_InchEntry := S.ReadIntegerAssign
    else if Token = 'aec_restroke' then
      AEC_Restroke := ReadDoubleAssign
    else if Token = 'aec_stowfeed' then
      AEC_StowFeed := ReadDoubleAssign
    else if Token = 'aec_loadpositionu' then
      AEC_LoadPositionU := ReadDoubleAssign
    else if Token = 'aec_checkholeprogram' then
      AEC_CheckHoleProgram := S.ReadIntegerAssign
    else if Token = 'aec_qualifystroke' then
      AEC_QualifyStroke := ReadDoubleAssign
    else if Token = 'aec_qualifyretryloops' then
      AEC_QualifyRetryLoops := S.ReadIntegerAssign
    else if Token = 'aec_electrodeclearance' then
      AEC_ElectrodeClearance := ReadDoubleAssign
    else if Token = 'aec_ejectlength' then
      AEC_EjectLength := ReadDoubleAssign
    else if Token = 'aec_doubleindex' then
      AEC_DoubleIndex := S.ReadIntegerAssign
    else if Token = 'aec_firstpulseduration' then
      AEC_FirstPulseDuration := ReadDoubleAssign
    else if Token = 'aec_secondpulseduration' then
      AEC_SecondPulseDuration := ReadDoubleAssign
    else if Token = 'aec_ejectpulsecount' then
      AEC_EjectPulseCount := S.ReadIntegerAssign
    else if Token = 'aec_ejectpulse1duration' then
      AEC_EjectPulse1Duration := ReadDoubleAssign
    else if Token = 'aec_ejectpulse2duration' then
      AEC_EjectPulse2Duration := ReadDoubleAssign
    else if Token = 'aec_loadpulsecount' then
      AEC_LoadPulseCount := S.ReadIntegerAssign
    else if Token = 'aec_ejectpulsecount' then
      AEC_EjectPulseCount := S.ReadIntegerAssign
    else
      raise Exception.Create('Unexpected Token[' + Token + ']');

    Token := LowerCase(S.Read);
  end;
end;

procedure TOPPFile.Save(S: TStrings);
begin
  S.Clear;
  S.Add(Format('PartType = ''%s''',           [PartType]));
  S.Add(Format('Comment = ''%s''',            [Comment]));
  S.Add(Format('ToolTypeString = %s',         [ToolTypeString]));
  S.Add(Format('ToolUsage = %g',              [ToolUsage]));
  S.Add(Format('CycleTime = %d',              [CycleTime]));
  S.Add(Format('ElectrodeCount = %d',         [ElectrodeCount]));
  S.Add(Format('ElectrodeMap = %s',           [ElectrodeMap]));
  S.Add(Format('ToolXOffset = %g',            [ToolXOffset]));
  S.Add(Format('ToolYOffset = %g',            [ToolYOffset]));
  S.Add(Format('ToolZOffset = %g',            [ToolZOffset]));
  S.Add(Format('OpNumber = %d',               [OpNumber]));
  S.Add(Format('AEC_Selected = %d',           [AEC_Selected]));
  S.Add(Format('AEC_InchEntry = %d',          [AEC_InchEntry]));
  S.Add(Format('AEC_Restroke = %g',           [AEC_Restroke]));
  S.Add(Format('AEC_StowFeed = %g',           [AEC_StowFeed]));
  S.Add(Format('AEC_CheckHoleProgram = %d',   [AEC_CheckHoleProgram]));
  S.Add(Format('AEC_QualifyStroke = %g',      [AEC_QualifyStroke]));
  S.Add(Format('AEC_QualifyRetryLoops = %d',  [AEC_QualifyRetryLoops]));
  S.Add(Format('AEC_ElectrodeClearance = %g', [AEC_ElectrodeClearance]));
  S.Add(Format('AEC_LoadPositionU = %g',      [AEC_LoadPositionU]));
  S.Add(Format('AEC_EjectLength = %g',        [AEC_EjectLength]));
  S.Add(Format('AEC_DoubleIndex = %d',        [AEC_DoubleIndex]));
  S.Add(Format('AEC_FirstPulseDuration = %g', [AEC_FirstPulseDuration]));
  S.Add(Format('AEC_SecondPulseDuration = %g',[AEC_SecondPulseDuration]));
  S.Add(Format('AEC_EjectPulseCount = %d',    [AEC_EjectPulseCount]));
  S.Add(Format('AEC_EjectPulse1Duration = %g',[AEC_EjectPulse1Duration]));
  S.Add(Format('AEC_EjectPulse2Duration = %g',[AEC_EjectPulse2Duration]));
  S.Add(Format('AEC_LoadPulseCount = %d',     [AEC_LoadPulseCount]));

end;


end.
