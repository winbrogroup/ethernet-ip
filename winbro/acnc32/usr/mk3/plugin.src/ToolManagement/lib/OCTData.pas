unit OCTData;

interface

uses SysUtils, Classes, IStringArray, NamedPlugin, Windows, OCTTypes;

const BigToolLength = 1000000000;

type
  TOPPList = class;

  TOPPStatus = (
    osNone,
    osStarted,
    osComplete,
    osInRecovery, // The base OCT Manager sets this status upon ResetRewind
    osAbandoned,
    osSkipped,
    osUnknown
  );

  TOPPEnable = (
    oeEnabled,
    oeAlreadyComplete
  );

  TOPPEnables = set of TOPPEnable;

  TOPP = class(TObject)
  private
    FIndex : Integer;
    FParent : TOPPList;
    FDependentOn : string;
    FFullPath : string;
    FEnables : TOPPEnables;
    FStatus : TOPPStatus;
    FToolUsage : Double;
    FToolType : string;
    FFileName : string;
    FStartTime : TDateTime;
    FEndTime : Double;
    FDuration : Double;
    FReworkStatus : string;
    FEstimatedCycleTime: Double;
    FOpNumber : Integer;

    procedure RebuildInternalValues;
    procedure WriteRecord;
    procedure SetDependentOn(const Value: string);
    procedure SetDuration(const Value: Double);
    function GetEnabled : Boolean;
    function GetAlreadyComplete : Boolean;
    procedure SetEndTime(const Value: Double);
    procedure SetFileName(const Value: string);
    procedure SetFullPath(const Value: string);
    procedure SetReworkStatus(const Value: string);
    procedure SetStartTime(const Value: TDateTime);
    procedure SetStatus(const Value: TOPPStatus);
    procedure SetToolType(const Value: string);
    procedure SetToolUsage(const Value: Double);
    procedure SetEstimatedCycleTime(const Value: Double);
    procedure SetOpNumber(Value : Integer);
    procedure SetEnabled(const Value: Boolean);
    procedure SetAlreadyComplete(const Value : Boolean);
  protected
    FFixedStart : Boolean;
    FFixedEnd : Boolean;

    FToolGroup : Integer;
    FSortScore : Integer;
    FIsDepend : Boolean;
    FHasDepend : Boolean;
    FIsToolDepend : Boolean;
    FHasToolDepend : Boolean;

    FNaturalOrder: Integer;
  public
    constructor Create(AIndex : Integer; AParent : TOPPList);
    property ReworkStatus : string read FReworkStatus write SetReworkStatus;
    property DependentOn : string read FDependentOn write SetDependentOn;
    property FullPath : string read FFullPath write SetFullPath;
    property Enabled : Boolean read GetEnabled write SetEnabled;
    property AlreadyComplete : Boolean read GetAlreadyComplete write SetAlreadyComplete;
    property Status : TOPPStatus read FStatus write SetStatus;
    property ToolUsage : Double read FToolUsage write SetToolUsage;
    property ToolType : string read FToolType write SetToolType;
    property FileName : string read FFileName write SetFileName;
    property StartTime : TDateTime read FStartTime write SetStartTime;
    property EndTime : Double read FEndTime write SetEndTime;
    property Duration : Double read FDuration write SetDuration;
    property EstimatedCycleTime : Double read FEstimatedCycleTime write SetEstimatedCycleTime;
    property OpNumber : Integer read FOpNumber write SetOpNumber;
    property NaturalOrder: Integer read FNaturalOrder write FNaturalOrder;

    // Theses properties do not represent anything, they are used internally by
    // OCT_TM,  DO NOT ASSUME THEY WILL BE THERE IN THE FUTURE!
    property FixedStart : Boolean read FFixedStart write FFixedStart;
    property FixedEnd : Boolean read FFixedEnd write FFixedEnd;
    property ToolGroup : Integer read FToolGroup write FToolGroup;
    property SortScore : Integer read FSortScore write FSortScore;
    property HasDepend : Boolean read FHasDepend write FHasDepend;
    property IsDepend : Boolean read FIsDepend write FIsDepend;
    property IsToolDepend : Boolean read FIsToolDepend write FIsToolDepend;
    property HasToolDepend : Boolean read FHasToolDepend write FHasToolDepend;
    function Complete : Boolean;

  end;

  TOPPList = class(TAbstractStringData)
  private
    function GetOPP(index: Integer): TOPP;
    function GetText: string;
    function GetAllEnabled: Boolean;
    function GetSummedDuration: Double;
  protected
    OPPList : TStringList;
    procedure RebuildInternalValues; override;
    procedure RebuildSingleValue(Row, Col :Integer); override;
    procedure CreateTable;
    procedure RebuildIndex;
  public
    constructor Create;
    destructor Destroy; override;
    function AddOPP : TOPP;
    function Count : Integer;
    function RemainingToolUsage(ToolName : string) : Double;
    function TotalOCTUsageforTool(ToolName :String): Double;
    function IndexOf(Name : string) : Integer;

    procedure EnableAllOPPs;
    procedure InvertSelection;

    property OPP[index : Integer] : TOPP read GetOPP; default;
    procedure Clean; override;
    property Text : string read GetText;
    property AllEnabled : Boolean read GetAllEnabled;
    property SummedDuration : Double read GetSummedDuration;

    function MinimumToolLength(ToolType : string) : Double;
    function CanBeUSed(ToolType: string; ToolLength: Double): Boolean;
    function EstimatedCycleTime : Double;
    function SuperdependentToolID(HasFixedStart,HasFixedEnd : Boolean) : String;
    function GetToolForProgram(ProgID : String):String;
    function OPPsRemaining : Integer;
    function OppsEnabled: Integer;
    function OppsCompleted: Integer;
  end;

const
  CompleteStatus = [osComplete, osAbandoned, osSkipped];


implementation

const
  FileNameField = 'FileName';
  FullPathField = 'FullPath';
  DependentOnField = 'DependentOn';
  EnabledField = 'Enabled';
  StatusField = 'Status';
  ToolTypeField = 'ToolType';
  ToolUsageField = 'ToolUsage';
  StartTimeField = 'StartTime';
  EndTimeField = 'EndTime';
  DurationField = 'Duration';
  ReworkStatusField = 'ReworkStatus';
  EstimatedCycleTimeField = 'EstimatedCycleTime';
  OpNumberField = 'OpNumber';

{ TOPP }
constructor TOPP.Create(AIndex : Integer; AParent: TOPPList);
begin
  inherited Create;
  FIndex := AIndex;
  FParent := AParent;
end;

procedure TOPP.SetDependentOn(const Value: string);
begin
  FDependentOn := Value;
  FParent.SetFieldData(FIndex, DependentOnField, Value);
end;

procedure TOPP.SetDuration(const Value: Double);
begin
  FDuration := Value;
  FParent.SetFieldData(FIndex, DurationField, FloatToStr(Value));
end;

procedure TOPP.SetFileName(const Value: string);
begin
  FFileName := Value;
  FParent.SetFieldData(FIndex, FileNameField, Value);
  Self.FParent.OPPList[FIndex] := Value;
end;

procedure TOPP.SetFullPath(const Value: string);
begin
  FFullPath := Value;
  FParent.SetFieldData(FIndex, FullPathField, Value);
end;

procedure TOPP.SetReworkStatus(const Value: string);
begin
  FReworkStatus := Value;
  FParent.SetFieldData(FIndex, ReworkStatusField, Value);
end;

procedure TOPP.SetStartTime(const Value: TDateTime);
begin
  FStartTime := Value;
  FParent.SetFieldData(FIndex, StartTimeField, FParent.StorageDate(Value));
end;

procedure TOPP.SetStatus(const Value: TOPPStatus);
begin
  FStatus := Value;
  FParent.SetFieldData(FIndex, StatusField, IntToStr(Ord(Value)));
end;

procedure TOPP.SetToolType(const Value: string);
begin
  FToolType := Value;
  FParent.SetFieldData(FIndex, ToolTypeField, Value);
end;

procedure TOPP.SetToolUsage(const Value: Double);
begin
  FToolUsage := Value;
  FParent.SetFieldData(FIndex, ToolUsageField, FloatToStr(Value));
end;

procedure TOPP.SetEndTime(const Value: Double);
begin
  FEndTime := Value;
  FParent.SetFieldData(FIndex, EndTimeField, FParent.StorageDate(Value));
end;

procedure TOPP.SetEstimatedCycleTime(const Value: Double);
begin
  FEstimatedCycleTime := Value;
  FParent.SetFieldData(FIndex, EstimatedCycleTimeField, FloatToStr(Value));
end;

procedure TOPP.SetOpNumber(Value: Integer);
begin
  FOpNumber := Value;
  FParent.SetFieldData(FIndex, OpNumberField, IntToStr(Value));
end;

procedure TOPP.SetEnabled(const Value: Boolean);
begin
  if Value then
    Include(FEnables, oeEnabled)
  else
    Exclude(FEnables, oeEnabled);

  FParent.SetFieldData(FIndex, EnabledField, IntToStr(Byte(FEnables)));
end;


procedure TOPP.SetAlreadyComplete(const Value: Boolean);
begin
  if Value then
    Include(FEnables, oeAlreadyComplete)
  else
    Exclude(FEnables, oeAlreadyComplete);

  FParent.SetFieldData(FIndex, EnabledField, IntToStr(Byte(FEnables)));
end;


procedure TOPP.RebuildInternalValues;
begin
  FDependentOn := FParent.GetFieldString(FIndex, DependentOnField);
  FFullPath := FParent.GetFieldString(FIndex, FullPathField);
  FEnables := TOPPEnables(Byte(FParent.GetFieldInteger(FIndex, EnabledField)));
  FStatus := TOPPStatus(FParent.GetFieldInteger(FIndex, StatusField));
  FToolUsage := FParent.GetFieldDouble(FIndex, ToolUsageField);
  FToolType := FParent.GetFieldString(FIndex, ToolTypeField);
  FFileName := FParent.GetFieldString(FIndex, FileNameField);
  FStartTime := FParent.GetFieldDateTime(FIndex, StartTimeField);
  FEndTime := FParent.GetFieldDateTime(FIndex, EndTimeField);
  FDuration := FParent.GetFieldDouble(FIndex, DurationField);
  FReworkStatus := FParent.GetFieldString(FIndex, ReworkStatusField);
  FEstimatedCycleTime := FParent.GetFieldDouble(FIndex, EstimatedCycleTimeField);
  FOpNumber := FParent.GetFieldInteger(FIndex, OpNumberField);

  FParent.OPPList[FIndex] := FFileName;
end;

procedure TOPP.WriteRecord;
begin
  FParent.BeginUpdate;
  try
    DependentOn := FDependentOn;
    FullPath := FFullPath;
    Enabled := Enabled;
    AlreadyComplete := AlreadyComplete; // Seems silly but its required
    Status := FStatus;
    ToolUsage := FToolUsage;
    ToolType := FToolType;
    FileName := FFileName;
    StartTime := FStartTime;
    EndTime := FEndTime;
    Duration := FDuration;
    ReworkStatus := FReworkStatus;
    OpNumber := FOpNumber;
  finally
    FParent.EndUpdate;
  end;
end;


function TOPP.GetEnabled: Boolean;
begin
  Result := oeEnabled in FEnables;
end;

function TOPP.GetAlreadyComplete: Boolean;
begin
  Result := oeAlreadyComplete in FEnables;
end;

function TOPP.Complete: Boolean;
begin
  Result := (Status in CompleteStatus) or AlreadyComplete;
end;

{ TOPPList }

function TOPPList.AddOPP: TOPP;
begin
  Result := TOPP.Create(OPPList.Count, Self);
  OPPList.AddObject('', Result);
  ITable.Append(1);
end;

constructor TOPPList.Create;
begin
  inherited Create;

  OPPList := TStringList.Create;
  if StringArraySet.CreateArray('OCTData', Self, ITable) <> AAR_OK then begin
    StringArraySet.UseArray('OCTData', Self, ITable);
  end else begin
    CreateTable;
  end;
  RebuildInternalValues;
end;

destructor TOPPList.Destroy;
begin
  ITable := nil;
  while OPPList.Count > 0 do begin
    OPP[OPPList.Count - 1].Free;
    OPPList.Delete(OPPList.Count - 1);
  end;

  inherited;
end;
procedure TOPPList.CreateTable;
begin
  BeginUpdate;
  try
    ITable.AddField(FileNameField);
    ITable.AddField(FullPathField);
    ITable.AddField(DependentOnField);
    ITable.AddField(EnabledField);
    ITable.AddField(StatusField);
    ITable.AddField(ToolTypeField);
    ITable.AddField(ToolUsageField);
    ITable.AddField(StartTimeField);
    ITable.AddField(EndTimeField);
    ITable.AddField(DurationField);
    ITable.AddField(ReworkStatusField);
    ITable.AddField(EstimatedCycleTimeField);
    ITable.AddField(OpNumberField);
  finally
    EndUpdate;
  end;
end;

function TOPPList.GetOPP(index: Integer): TOPP;
begin
  Result := TOPP(OPPList.Objects[Index]);
end;

function TOPPList.Count: Integer;
begin
  Result := OPPList.Count;
end;

// For simplicity the entire row is read
procedure TOPPList.RebuildSingleValue(Row, Col :Integer);
begin
  if OPPList.Count > Row then
    OPP[Row].RebuildInternalValues;
end;


// It is absolutely vital that this routine does not cause a callback from the table
procedure TOPPList.RebuildInternalValues;
var I : Integer;
begin
  while ITable.RecordCount < OPPList.Count do begin
    OPP[OPPList.Count - 1].Free;
    OPPList.Delete(OPPList.Count - 1);
  end;

  // We cant use AddOPP to add an OPP as this causes callbacks and
  // appends records.
  for I := OPPList.Count to ITable.RecordCount - 1 do begin
    OPPList.AddObject('', TOPP.Create(OPPList.Count, Self));
  end;

  for I := 0 to ITable.RecordCount - 1 do begin
    OPP[I].RebuildInternalValues;
    OPP[I].FIndex := I;
  end;
end;

function TOPPList.IndexOf(Name: string): Integer;
begin
  Result := OPPList.IndexOf(Name);
end;

procedure TOPPList.Clean;
begin
  BeginUpdate;
  try
    ITable.Clear(Self);

    while OPPList.Count > 0 do begin
      OPP[0].Free;
      OPPList.Delete(0);
    end;
  finally
    EndUpdate;
  end;
//  inherited;
end;

function TOPPList.GetText: string;
begin
  Result := OPPList.Text;
end;

function TOPPList.RemainingToolUsage(ToolName: string): Double;
var I : Integer;
begin
  Result := 0;
  for I := 0 to Count - 1 do begin
    if (CompareText(OPP[I].ToolType, ToolName) = 0) and
       not OPP[I].Complete and
       OPP[I].Enabled then begin
      Result := Result + OPP[I].ToolUsage;
    end;
  end;
end;

function TOPPList.TotalOCTUsageforTool(ToolName: string): Double;
var I : Integer;
begin
  Result := 0;
  for I := 0 to Count - 1 do begin
    if (CompareText(OPP[I].ToolType, ToolName) = 0) and
//       not (OPP[I].Status in CompleteStatus) and
       OPP[I].Enabled then begin
      Result := Result + OPP[I].ToolUsage;
    end;
  end;
end;

procedure TOPPList.RebuildIndex;
var I : Integer;
begin
  for I := 0 to OPPList.Count - 1 do begin
    OPP[I].FIndex := I;
    OPP[I].WriteRecord;
  end;
end;


procedure TOPPList.EnableAllOPPs;
var I : Integer;
begin
  BeginUpdate;
  for I := 0 to OPPList.Count - 1 do begin
    OPP[I].Enabled := True;
  end;
  endUpdate;
end;

procedure TOPPList.InvertSelection;
var I : Integer;
begin
  BeginUpdate;
  for I := 0 to OPPList.Count - 1 do
    begin
    OPP[I].Enabled := not(OPP[I].Enabled)
    end;
  endUpdate;
end;


function TOPPList.GetAllEnabled: Boolean;
begin
Result := False;
if Count > 0 then
  begin
  Result := Count = OPPsEnabled;
  end;
end;



function TOPPList.GetSummedDuration: Double;
var I : Integer;
begin
  Result := 0;
  Lock;
  try
  for I := 0 to OPPList.Count - 1 do
    begin
    Result := Result+OPP[I].FDuration;
    end;
  finally
  UnLock;
  end;
end;

function TOPPList.MinimumToolLength(ToolType: string): Double;
var I : Integer;
begin
  Result := BigToolLength;
  Lock;
  try
    for I := 0 to Count - 1 do begin
      if (CompareText(ToolType, OPP[I].ToolType) = 0) and
          OPP[I].Enabled and
          not OPP[I].Complete then begin
        if OPP[I].ToolUsage < Result then begin
          Result := OPP[I].ToolUsage;
        end;
      end;
    end;
  finally
    Unlock;
  end;
end;

{ Determines is the specified tool is usable again in this or the
  next running of the OCT }
function TOPPList.CanBeUsed(ToolType: string; ToolLength: Double): Boolean;
var I: Integer;
begin
  Result:= False;
  Lock;
  try
    for I := 0 to Count - 1 do begin
      if (CompareText(ToolType, OPP[I].ToolType) = 0) and
          (OPP[I].ToolUsage < ToolLength) then begin

        Result:= True;
      end;
    end;
  finally
    Unlock;
  end;
end;

function TOPPList.EstimatedCycleTime: Double;
var I : Integer;
begin
  Result := 0;
  Lock;
  try
    for I := 0 to Count - 1 do begin
      if OPP[I].Enabled and not OPP[I].Complete then
        Result := Result + OPP[I].EstimatedCycleTime;
    end;
  finally
    Unlock;
  end;
end;

function TOPPList.SuperdependentToolID(HasFixedStart,HasFixedEnd : Boolean): string;
var
I : Integer;
DependencyCount : Integer;
DepString       : String;
CompareCount    : Integer;
begin
  Result := '';
  DependencyCount := 0;
  DepString := '';
  Lock;
  // First do all but one have the same dependency in
  try
    for I := 0 to Count - 1 do begin
      if OPP[I].DependentOn <> '' then
        begin
        if (DepString = '') then DepString := OPP[I].DependentOn;
        if (OPP[I].DependentOn = DepString) then
            begin
            DepString := OPP[I].DependentOn;
            inc(DependencyCount)
            end;
        end;
    end;

  // need to now if there is a fixed end and start opp here to make
  // decision
  CompareCount := Count-1;
  if HasFixedStart then dec(CompareCount);
  if HasFixedEnd then dec(CompareCount);
  if DependencyCount = CompareCount then
      begin
      // we now have the program but not the tool ID.
      Result := GetToolForProgram(DepString)
      end
  finally
    Unlock;
  end;

end;

function TOPPList.GetToolForProgram(ProgID: string): string;
var
I : Integer;
begin
Result := '';
For I := 0 To Count-1 do
  begin
  if UpperCase(ProgID) = UpperCase(ExtractFileName( OPP[I].FFullPath)) then
    begin
    Result := OPP[I].FToolType;
    exit;
    end;
  end;

end;

function TOPPList.OppsEnabled: Integer;
var I : Integer;
begin
  Result := 0;
  for I := 0 to Count - 1 do begin
    if OPP[I].Enabled then
      Inc(Result);
  end;
end;

function TOPPList.OppsCompleted: Integer;
var I : Integer;
begin
  Result := 0;
  for I := 0 to Count - 1 do begin
    if OPP[I].Status = osComplete then
      Inc(Result);
  end;
end;

function TOPPList.OPPsRemaining: Integer;
var I : Integer;
begin
  Result := 0;
  for I := 0 to OPPList.Count - 1 do begin
    if OPP[I].Enabled and not OPP[I].Complete then
      Inc(Result);
  end;
end;


end.
