unit CellMode;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, IFormInterface, INamedInterface, NamedPlugin,
  OCTTypes, AbstractFormPlugin, ACNC_TouchSoftkey, CNCTypes;

type
  TCellModeForm = class(TInstallForm)
    BtnProduction: TTouchSoftkey;
    BtnStandalone: TTouchSoftkey;
    BtnRework: TTouchSoftkey;
    Panel1: TPanel;
    BtnClose: TTouchSoftkey;
    BtnDevelopment: TTouchSoftkey;
    procedure BtnProductionClick(Sender: TObject);
    procedure BtnCloseClick(Sender: TObject);
  private
    Trans: TTranslator;
    CellModeTag : TTagRef;
    CellModeNameTag : TTagRef;
    RequestMode : TCellMode;
    AccessLevelsTag : TTagRef;
    procedure IndicateCellMode(aMode : TCellMode);
    function PermittedAccess(Mode : TCellMode) : Boolean;
  public
    procedure Install; override;
    procedure FinalInstall; override;
    procedure Execute; override;
    procedure SetCellMode(aMode : TCellMode);
  end;

var
  LocalPath : string;
  CellOPPPath : string;
  CellOCTPath : string;
  ReworkPath : string;
  DevelopmentPath : string;
  BufferedPath : string;

  RemoteCMMPath : string;
  LocalCMMPath : string;
  RemoteSigPath : string;
  LocalSigPath : string;
  CellPartTypeFromPath : string;
  XFerPath : string;

  CellOCT : string; // Recorded when an OCT is loaded in cellmode. buffered path

  CellModeForm : TCellModeForm;

function LocalCellMode : TCellMode;
procedure SetInterrupted(AVal: Boolean);
function IsInterrupted: Boolean;
procedure SetResume(AVal: Boolean);
procedure UpdatePathInfo(aTag : TTagRef); stdcall;

implementation

uses AccessAdministration;

const
  CellModeName : array [TCellMode] of string = (
    '$CM_Production',
    '$CM_Standalone',
    '$CM_Rework',
    '$CM_Development'
  );

var
  CellOCTPathTag : TTagRef;
  CellOPPPathTag : TTagRef;
  BufferedPathTag : TTagRef;
  ReworkPathTag : TTagRef;
  DevelopmentPathTag : TTagRef;

  RemoteCMMPathTag : TTagRef;
  LocalCMMPathTag : TTagRef;
  RemoteSigPathTag : TTagRef;
  LocalSigPathTag : TTagRef;
  OCTInterruptedTag: TTagRef; // Local state tag
  OCTReqResumeTag: TTagRef; // Input to OCT_TM

function LocalCellMode : TCellMode;
begin
  if Assigned(CellModeForm) then
    Result := TCellMode(Named.GetAsInteger(CellModeForm.CellModeTag))
  else
    Result:= cmStandalone;

end;

procedure UpdatePathInfo(aTag : TTagRef); stdcall;
var Buffer : array [0..1023] of Char;
begin
  Named.GetAsString(CellOPPPathTag, Buffer, 1023);
  CellOPPPath := IncludeTrailingPathDelimiter(Buffer);

  Named.GetAsString(CellOCTPathTag, Buffer, 1023);
  CellOCTPath := IncludeTrailingPathDelimiter(Buffer);

  Named.GetAsString(ReworkPathTag, Buffer, 1023);
  ReworkPath := IncludeTrailingPathDelimiter(Buffer);

  Named.GetAsString(DevelopmentPathTag, Buffer, 1023);
  DevelopmentPath := IncludeTrailingPathDelimiter(Buffer);

  Named.GetAsString(BufferedPathTag, Buffer, 1023);
  BufferedPath := IncludeTrailingPathDelimiter(Buffer);

  Named.GetAsString(RemoteCMMPathTag, Buffer, 1023);
  RemoteCMMPath := IncludeTrailingPathDelimiter(Buffer);

  Named.GetAsString(LocalCMMPathTag, Buffer, 1023);
  LocalCMMPath := IncludeTrailingPathDelimiter(Buffer);

  Named.GetAsString(RemoteSigPathTag, Buffer, 1023);
  RemoteSigPath := IncludeTrailingPathDelimiter(Buffer);

  Named.GetAsString(LocalSigPathTag, Buffer, 1023);
  LocalSigPath := IncludeTrailingPathDelimiter(Buffer);

  XFerPath := IncludeTrailingPathDelimiter(DllLocalPath) + 'xfer\';
  LocalPath := IncludeTrailingPathDelimiter(DllLocalPath) + 'programs\';
end;

procedure SetInterrupted(AVal: Boolean);
begin
  Named.SetAsBoolean(OCTInterruptedTag, AVal);
end;

procedure SetResume(AVal: Boolean);
begin
  Named.SetAsBoolean(OCTReqResumeTag, AVal);
  Named.EventLog('What the f is going on');
end;

function IsInterrupted: Boolean;
begin
  Result:= Named.GetAsBoolean(OCTInterruptedTag);
end;


{$R *.DFM}

procedure TCellModeForm.IndicateCellMode(aMode : TCellMode);
begin
  BtnProduction.IndicatorOn := False;
  BtnStandalone.IndicatorOn := False;
  BtnRework.IndicatorOn := False;
  BtnDevelopment.IndicatorOn := False;

  case aMode of
    cmProduction : BtnProduction.IndicatorOn := True;
    cmStandalone : BtnStandalone.IndicatorOn := True;
    cmRework : BtnRework.IndicatorOn := True;
    cmDevelopment : BtnDevelopment.IndicatorOn := True;
  end;
end;


procedure TCellModeForm.BtnProductionClick(Sender: TObject);
begin
  if Sender = BtnProduction then begin
    RequestMode := cmProduction;
  end else if Sender = BtnStandalone then begin
    RequestMode := cmStandalone;
  end else if Sender = BtnRework then begin
    RequestMode := cmRework;
  end else if (Sender = BtnDevelopment) then begin
    RequestMode := cmDevelopment;
  end;
  IndicateCellMode(RequestMode);
end;

function TCellModeForm.PermittedAccess(Mode : TCellMode) : Boolean;
var AccessLevels : TAccessLevels;
begin
  AccessLevels := TAccessLevels(Byte(Named.GetAsInteger(AccessLevelsTag)));
  case Mode of
    cmProduction : Result := (CellAccess.ProductionMode * AccessLevels) <> [];
    cmRework : Result := (CellAccess.ReworkMode * AccessLevels) <> [];
    cmDevelopment : Result := (CellAccess.DevelopmentMode * AccessLevels) <> [];
    cmStandalone : Result := (CellAccess.StandaloneMode * AccessLevels) <> [];
  else
    Result := False;
  end;
end;

procedure TCellModeForm.BtnCloseClick(Sender: TObject);
begin
  if (RequestMode <> LocalCellMode) then begin
     if not NC.InCycle and PermittedAccess(RequestMode) then begin
       SetCellMode(RequestMode);

       if IsInterrupted and (RequestMode = cmProduction) then begin
         SetResume(True);
       end;
       Close;
    end else begin
      RequestMode := LocalCellMode;
      IndicateCellMode(RequestMode);
    end;
  end else begin
    Close;
  end;
end;

procedure TCellModeForm.FinalInstall;
begin
  BufferedPathTag := Named.AquireTag('BufferedDirectory', 'TStringTag', UpdatePathInfo);
  CellOCTPathTag := Named.AquireTag('ProductionOCTDirectory', 'TStringTag', UpdatePathInfo);
  CellOPPPathTag := Named.AquireTag('ProductionOPPDirectory', 'TStringTag', UpdatePathInfo);
  ReworkPathTag := Named.AquireTag('ReworkDirectory', 'TStringTag', UpdatePathInfo);
  DevelopmentPathTag := Named.AquireTag('DevelopmentDirectory', 'TStringTag', UpdatePathInfo);
  RemoteCMMPathTag := Named.AquireTag('RemoteCMMDirectory', 'TStringTag', UpdatePathInfo);
  LocalCMMPathTag := Named.AquireTag('LocalCMMDirectory', 'TStringTag', UpdatePathInfo);
  RemoteSigPathTag := Named.AquireTag('RemoteSignatureDirectory', 'TStringTag', UpdatePathInfo);
  LocalSigPathTag := Named.AquireTag('LocalSignatureDirectory', 'TStringTag', UpdatePathInfo);

  AccessLevelsTag := Named.AquireTag('AccessLevels', 'TIntegerTag', nil);

  UpdatePathInfo(nil);
  Named.SetAsInteger(CellModeTag, Ord(cmStandalone));
  Named.SetAsString(CellModeNameTag, PChar(Trans.GetString( CellModeName[cmStandalone])));
end;

procedure TCellModeForm.Install;
begin
  CellModeForm := Self;
  CellModeTag := Named.AddTag(NameCM_CellMode, 'TIntegerTag');
  trans:= TTranslator.Create;
  CellModeNameTag := Named.AddTag(NameCM_CellModeName, 'TStringTag');
  OCTReqResumeTag:= Named.AddTag(NameOCTReqResume, 'TMethodTag');
  OCTInterruptedTag:= Named.AddTag(NameOCTInterrupted, 'TIntegerTag');

  BtnProduction.Caption:= trans.GetString(CellModeName[cmProduction]);
  BtnStandalone.Caption:= trans.GetString(CellModeName[cmStandalone]);
  BtnRework.Caption:= trans.GetString(CellModeName[cmRework]);
  BtnDevelopment.Caption:= trans.GetString(CellModeName[cmDevelopment]);
end;

procedure TCellModeForm.Execute;
begin
  RequestMode := LocalCellMode;
  IndicateCellMode(RequestMode);
  if not NC.InCycle then begin

    ShowModal;
  end;
end;

procedure TCellModeForm.SetCellMode(aMode : TCellMode);
begin
  Named.SetAsInteger(CellModeTag, Integer(aMode));
  Named.SetAsString(CellModeNameTag, PChar(trans.GetString( CellModeName[aMode])));
  Named.LoadOPP(DllProfilePath + 'empty.opp');
end;

initialization
  TAbstractFormPlugin.AddPlugin('CellModeSelect', TCellModeForm);
end.
