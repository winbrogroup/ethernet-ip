unit OffsetData;

interface

uses SysUtils, Classes, IStringArray, NamedPlugin, OCTTypes;

const
  XOffset = 'X';
  YOffset = 'Y';
  ZOffset = 'Z';
  AOffset = 'A';
  BOffset = 'B';
  COffset = 'C';

type
  TOffsetList = class;
  TOffsetData = class(TObject)
  private
    function GetOffset(Index: Integer): Double;
    procedure SetOffset(Index: Integer; const Value: Double);
  protected
    Host: TOffsetList;
    Index: Integer;
  public
    FOffsets : array [0..5] of Double;
    constructor Create(AHost: TOffsetList; AIndex: Integer);
    procedure UpdateRow;
    procedure RebuildInternalValues(AIndex : Integer);
    property Axis[Index: Integer]: Double read GetOffset write SetOffset; default;
  end;

  TOffsetList = class(TAbstractStringData)
  private
    List : TList;
    procedure CreateTable;
    function GetOffsetData(Index: Integer): TOffsetData;
  protected
    procedure RebuildInternalValues; override;
    procedure RebuildSingleValue(Row, Col :Integer); override;
  public
    constructor Create(TableName : string; ALength : Integer);
    property Offset[Index : Integer] : TOffsetData read GetOffsetData; default;
    function Count : Integer;
    function AddOffset : TOffsetData;
  end;

implementation

{ TOpTable }

{ TOffsetData }

constructor TOffsetData.Create(AHost: TOffsetList; AIndex: Integer);
begin
  Host:= AHost;
  Index:= AIndex;
  FOffsets[0] := 0;
  FOffsets[1] := 0;
  FOffsets[2] := 0;
  FOffsets[3] := 0;
  FOffsets[4] := 0;
  FOffsets[5] := 0;
end;

function TOffsetData.GetOffset(Index: Integer): Double;
begin
  Result := FOffsets[Index];
end;

procedure TOffsetData.RebuildInternalValues(AIndex: Integer);
begin
  FOffsets[0] := Host.GetFieldDouble(Index, XOffset);
  FOffsets[1] := Host.GetFieldDouble(Index, YOffset);
  FOffsets[2] := Host.GetFieldDouble(Index, ZOffset);
  FOffsets[3] := Host.GetFieldDouble(Index, AOffset);
  FOffsets[4] := Host.GetFieldDouble(Index, BOffset);
  FOffsets[5] := Host.GetFieldDouble(Index, COffset);
end;

procedure TOffsetData.SetOffset(Index: Integer; const Value: Double);
begin
  FOffsets[Index] := Value;
end;

procedure TOffsetData.UpdateRow;
begin
  Host.SetFieldData(Index, XOffset, FloatToStr(FOffsets[0]));
  Host.SetFieldData(Index, YOffset, FloatToStr(FOffsets[1]));
  Host.SetFieldData(Index, ZOffset, FloatToStr(FOffsets[2]));
  Host.SetFieldData(Index, AOffset, FloatToStr(FOffsets[3]));
  Host.SetFieldData(Index, BOffset, FloatToStr(FOffsets[4]));
  Host.SetFieldData(Index, COffset, FloatToStr(FOffsets[5]));
end;

{ TOffsetList }

function TOffsetList.AddOffset: TOffsetData;
begin
  Lock;
  try
    Result := TOffsetData.Create(Self, Count);
    List.Add(Result);
    ITable.Append(1);
    Result.UpdateRow;
  finally
    Unlock;
  end;
end;

function TOffsetList.Count: Integer;
begin
  Result := List.Count;
end;

constructor TOffsetList.Create(TableName: string; ALength: Integer);
var I: Integer;
begin
  inherited Create;

  if StringArraySet.CreateArray(TableName, Self, ITable) <> AAR_OK then begin
    StringArraySet.UseArray(TableName, Self, ITable);
  end else begin
    CreateTable;
  end;
  List:= TList.Create;
  ITable.Clear(Self);
  for I:= 0 to ALength - 1 do begin
    AddOffset;
  end;
end;

procedure TOffsetList.CreateTable;
begin
  BeginUpdate;
  try
    ITable.Clear;
    ITable.AddField(XOffset);
    ITable.AddField(YOffset);
    ITable.AddField(ZOffset);
    ITable.AddField(AOffset);
    ITable.AddField(BOffset);
    ITable.AddField(COffset);
  finally
    EndUpdate;
  end;
end;

function TOffsetList.GetOffsetData(Index: Integer): TOffsetData;
begin
  Result := TOffsetData(List[Index]);
end;

procedure TOffsetList.RebuildInternalValues;
var I : Integer;
    Tmp : TOffsetData;
begin
  for I := ITable.RecordCount to List.Count - 1 do begin
    Offset[0].Free;
    List.Delete(0);
  end;

  for I := 0 to ITable.RecordCount - 1 do begin
    if I >= List.Count then begin
      Tmp := TOffsetData.Create(Self, I);
      List.Add(Tmp);
      Tmp.RebuildInternalValues(I);
    end else begin
      Offset[I].RebuildInternalValues(I);
    end;
  end;
end;

procedure TOffsetList.RebuildSingleValue(Row, Col: Integer);
begin
  Offset[Row].RebuildInternalValues(Row);
end;

end.
