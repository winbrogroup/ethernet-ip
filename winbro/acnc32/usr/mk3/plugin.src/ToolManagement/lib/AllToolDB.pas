unit AllToolDB;

interface

uses SysUtils, Classes, IStringArray, NamedPlugin, OCTTypes, ToolData;

type
  TAllToolDB = class;

  TAllToolDBItem = class(TObject)
  private
    Index: Integer;
    Host: TAllToolDB;
    FUniqueToolX: Double;
    FUniqueToolY: Double;
    FUniqueToolZ: Double;
    FMinToolLength: Double;
    FToolCode: Integer;
    FToolSN: Integer;
    FToolName: string;
    FCorrelationX: Double;
    FCorrelationY: Double;
    procedure SetMinToolLength(const Value: Double);
    procedure SetUniqueToolX(const Value: Double);
    procedure SetUniqueToolY(const Value: Double);
    procedure SetUniqueToolZ(const Value: Double);
    procedure SetToolCode(const Value: Integer);
    procedure SetToolName(const Value: string);
    procedure SetToolSN(const Value: Integer);
    procedure SetCorrelationX(const Value: Double);
    procedure SetCorrelationY(const Value: Double);
  protected
    procedure RebuildInternalValues(AIndex: Integer);
  public
    constructor Create(AHost: TAllToolDB; AIndex: Integer);
    destructor Destroy; override;
    procedure Assign(T: TToolData);
    property MinToolLength: Double read FMinToolLength write SetMinToolLength;
    property UniqueToolX: Double read FUniqueToolX write SetUniqueToolX;
    property UniqueToolY: Double read FUniqueToolY write SetUniqueToolY;
    property UniqueToolZ: Double read FUniqueToolZ write SetUniqueToolZ;
    property ToolCode: Integer read FToolCode write SetToolCode;
    property ToolSN: Integer read FToolSN write SetToolSN;
    property ToolName: string read FToolName write SetToolName;
    property CorrelationX: Double read FCorrelationX write SetCorrelationX;
    property CorrelationY: Double read FCorrelationY write SetCorrelationY;
  end;

  TAllToolDB = class(TAbstractStringData)
  private
    ToolList : TList;
    function GetTool(index: Integer): TAllToolDBItem;
    function IndexOf(Tool : TAllToolDBItem) : Integer;
  protected
    procedure RebuildSingleValue(Row, Col :Integer); override;
    procedure RebuildInternalValues; override;
  public
    procedure CreateTable;
    procedure RebuildIndex;
    constructor Create;
    destructor Destroy; override;
    function AddTool: TAllToolDBItem;
    function Count: Integer;
    property Tool[index: Integer]: TAllToolDBItem read GetTool; default;
    procedure Clean; override;
    procedure RemoveTool(T : TAllToolDBItem);
    function FindTool(AToolCode: Integer; ASN: Integer): TAllToolDBItem; overload;
    function FindTool(AToolName: string; ASN: Integer): TAllToolDBItem; overload;
    function FindToolName(AToolCode: Integer): TAllToolDBItem;
    function UpdateTool(T: TToolData; AToolCode, AToolSN: Integer): Boolean;
  end;

implementation

{ TAllToolDBItem }

const
  ToolCodeField = 'ToolCode';
  ToolSNField = 'ToolSN';
  ToolNameField = 'ToolName';
  CorrelationXField = 'CorrelationX';
  CorrelationYField = 'CorrelationY';

procedure TAllToolDBItem.Assign(T: TToolData);
begin
end;

constructor TAllToolDBItem.Create(AHost: TAllToolDB; AIndex: Integer);
begin
  inherited Create;
  Index := AIndex;
  Host := AHost;
end;

destructor TAllToolDBItem.Destroy;
begin
  inherited;
end;

procedure TAllToolDBItem.RebuildInternalValues(AIndex: Integer);
begin
  Index := AIndex;
  FMinToolLength := Host.GetFieldDouble(Index, MinToolLengthField);
  FUniqueToolX := Host.GetFieldDouble(Index, UniqueToolXField);
  FUniqueToolY := Host.GetFieldDouble(Index, UniqueToolYField);
  FUniqueToolZ := Host.GetFieldDouble(Index, UniqueToolZField);
  FToolCode := Host.GetFieldInteger(Index, ToolCodeField);
  FToolSN := Host.GetFieldInteger(Index, ToolSNField);
  FToolName := Host.GetFieldString(Index, ToolNameField);
  FCorrelationX:= Host.GetFieldDouble(Index, CorrelationXField);
  FCorrelationY:= Host.GetFieldDouble(Index, CorrelationYField);
end;

procedure TAllToolDBItem.SetCorrelationX(const Value: Double);
begin
  FCorrelationX:= Value;
  Host.SetFieldData(Index, CorrelationXField, FloatToStr(Value));
end;

procedure TAllToolDBItem.SetCorrelationY(const Value: Double);
begin
  FCorrelationY:= Value;
  Host.SetFieldData(Index, CorrelationYField, FloatToStr(Value));
end;

procedure TAllToolDBItem.SetMinToolLength(const Value: Double);
begin
  FMinToolLength := Value;
  Host.SetFieldData(Index, MinToolLengthField, FloatToStr(Value));
end;

procedure TAllToolDBItem.SetToolCode(const Value: Integer);
begin
  FToolCode := Value;
  Host.SetFieldData(Index, ToolCodeField, IntToStr(FToolCode));
end;

procedure TAllToolDBItem.SetToolName(const Value: string);
begin
  FToolName := Value;
  Host.SetFieldData(Index, ToolNameField, FToolName);
end;

procedure TAllToolDBItem.SetToolSN(const Value: Integer);
begin
  FToolSN := Value;
  Host.SetFieldData(Index, ToolSNField, IntToStr(FToolSN));
end;

procedure TAllToolDBItem.SetUniqueToolX(const Value: Double);
begin
  FUniqueToolX := Value;
  Host.SetFieldData(Index, UniqueToolXField, FloatToStr(FUniqueToolX));
end;

procedure TAllToolDBItem.SetUniqueToolY(const Value: Double);
begin
  FUniqueToolY := Value;
  Host.SetFieldData(Index, UniqueToolYField, FloatToStr(FUniqueToolY));
end;

procedure TAllToolDBItem.SetUniqueToolZ(const Value: Double);
begin
  FUniqueToolZ := Value;
  Host.SetFieldData(Index, UniqueToolZField, FloatToStr(FUniqueToolZ));
end;

{ TAllToolDB }

function TAllToolDB.AddTool: TAllToolDBItem;
begin
  Lock;
  try
    Result := TAllToolDBItem.Create(Self, -1);
    Result.Index := ITable.RecordCount;

    ToolList.Add(Result);

    ITable.Append(1);
  finally
    Unlock;
  end;
end;

procedure TAllToolDB.Clean;
begin
  BeginUpdate;
  try
    ITable.Clear;
    while ToolList.Count > 0 do begin
      TObject(ToolList[0]).Free;
      ToolList.Delete(0);
    end;
  finally
    EndUpdate;
  end;
end;

function TAllToolDB.Count: Integer;
begin
  Result := ToolList.Count;
end;

constructor TAllToolDB.Create;
begin
  inherited;

  if StringArraySet.CreateArray('AllToolDB', Self, ITable) <> AAR_OK then begin
    StringArraySet.UseArray('AllToolDB', Self, ITable);
  end else begin
    CreateTable;
  end;

  if ITable.FieldCount = 7 then begin
    ITable.AddField(CorrelationXField);
    ITable.AddField(CorrelationYField);
  end;

  ToolList := TList.Create;
  RebuildInternalValues;
end;

procedure TAllToolDB.CreateTable;
begin
  BeginUpdate;
  try
    ITable.Clear;
    ITable.AddField(ToolNameField);
    ITable.AddField(ToolCodeField);
    ITable.AddField(ToolSNField);
    ITable.AddField(MinToolLengthField);
    ITable.AddField(UniqueToolXField);
    ITable.AddField(UniqueToolYField);
    ITable.AddField(UniqueToolZField);
    ITable.AddField(CorrelationXField);
    ITable.AddField(CorrelationYField);
  finally
    EndUpdate;
  end;
end;

destructor TAllToolDB.Destroy;
begin
  ITable := nil;
  inherited;
end;

function TAllToolDB.FindTool(AToolName: string; ASN : Integer): TAllToolDBItem;
var I : Integer;
begin
  for I := 0 to Count - 1 do begin
    Result := Tool[I];
    if (CompareText(AToolName, Result.ToolName) = 0) and
       (ASN = Result.ToolSN) then
      Exit;
  end;
  Result := nil;
end;

function TAllToolDB.FindTool(AToolCode: Integer; ASN : Integer): TAllToolDBItem;
var I : Integer;
begin
  for I := 0 to Count - 1 do begin
    Result := Tool[I];
    if (AToolCode = Result.ToolCode) and
       (ASN = Result.ToolSN) then
      Exit;
  end;
  Result := nil;
end;


function TAllToolDB.FindToolName(AToolCode: Integer): TAllToolDBItem;
var I : Integer;
begin
  for I := 0 to Count - 1 do begin
    Result := Tool[I];
    if AToolCode = Result.ToolCode then
      Exit;
  end;
  Result := nil;
end;

function TAllToolDB.GetTool(index: Integer): TAllToolDBItem;
begin
  Result := TAllToolDBItem(ToolList[Index]);
end;

function TAllToolDB.IndexOf(Tool: TAllToolDBItem): Integer;
begin
  Result := ToolList.IndexOf(Tool);
end;

procedure TAllToolDB.RebuildIndex;
var I : Integer;
begin
  for I := 0 to Count - 1 do begin
    Tool[I].Index := I;
  end;
end;

procedure TAllToolDB.RebuildInternalValues;
var I : Integer;
    Tmp : TAllToolDBItem;
begin
  if not Assigned(ToolList) then
    Exit;
    
  for I := ITable.RecordCount to ToolList.Count - 1 do begin
    Tool[0].Free;
    ToolList.Delete(0);
  end;

  for I := 0 to ITable.RecordCount - 1 do begin
    if I >= ToolList.Count then begin
      Tmp := TAllToolDBItem.Create(Self, I);
      ToolList.Add(Tmp);
      Tmp.RebuildInternalValues(I);
    end else begin
      Tool[I].RebuildInternalValues(I);
    end;
  end;
end;

procedure TAllToolDB.RebuildSingleValue(Row, Col: Integer);
begin
  Tool[Row].RebuildInternalValues(Row);
end;

procedure TAllToolDB.RemoveTool(T: TAllToolDBItem);
var I : Integer;
begin
  BeginUpdate;
  try
    I := IndexOf(T);
    ITable.Delete(T.Index);
    T.Free;
    ToolList.Delete(I);
    RebuildIndex;
  finally
    EndUpdate;
  end;
end;

// Given a tool in the machine (T) and the hypertac values ensure that the
// database is maintained with up to date values
// Returns false if the ToolSN or ToolName does not match the current tool
function TAllToolDB.UpdateTool(T: TToolData; AToolCode, AToolSN : Integer): Boolean;
var AT : TAllToolDBItem;
begin
  BeginUpdate;
  try
    AT := FindTool(AToolCode, AToolSN);
    if not Assigned(AT) then begin
      AT := FindToolName(AToolCode);
      if Assigned(AT) then begin
        if CompareText(AT.ToolName, T.ShuttleID) <> 0 then begin
          Result := False;
          Exit;
        end;
      end;
      AT := AddTool;
    end else begin
      if (CompareText(AT.ToolName, T.ShuttleID) <> 0) or
         (AToolSN <> T.ShuttleSN) then begin
        Result := False;
        Exit;
      end;
    end;

    AT.ToolName := T.ShuttleID;
    AT.ToolCode := AToolCode;
    AT.ToolSN := AToolSN;
    AT.UniqueToolX := T.UniqueToolOffsets[0];
    AT.UniqueToolY := T.UniqueToolOffsets[1];
    AT.UniqueToolZ := T.UniqueToolOffsets[2];
    AT.ToolCode := AToolCode;
    AT.MinToolLength := T.MinToolLength;
    Result := True;
  finally
    Endupdate;
  end;
end;

end.
