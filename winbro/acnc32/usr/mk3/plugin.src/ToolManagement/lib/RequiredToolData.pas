unit RequiredToolData;

interface

uses SysUtils, Classes, OCTTypes, NamedPlugin, IStringArray;

type
  TRequiredToolList = class;

  TRequiredTool = class(TObject)
  private
    FIndex : Integer;
    FLength: Double;
    FParent : TRequiredToolList;
    FPriority: Integer;
    FRecords: Integer;
    FToolName: string;
    FHasDependencies: Boolean;
    procedure SetLength(const Value: Double);
    procedure SetPriority(const Value: Integer);
    procedure SetRecords(const Value: Integer);
    procedure SetToolName(const Value: string);
    procedure RebuildInternalValues;
    procedure SetHasDependencies(const Value: Boolean);
  protected
    FToolGroup : Integer;
    FDependentOn : TList;
  public
    constructor Create(AIndex : Integer; AParent : TRequiredToolList);
    destructor Destroy; override;
    property ToolName : string read FToolName write SetToolName;
    property Length : Double read FLength write SetLength;
    property Priority : Integer read FPriority write SetPriority;
    property Count : Integer read FRecords write SetRecords;
    property HasDepencencies : Boolean read FHasDependencies write SetHasDependencies;

    property DependentOn : TList read FDependentOn;
    property ToolGroup : Integer read FToolGroup write FToolGroup;
  end;

  TRequiredToolList = class(TAbstractStringData)
  private
    function GetTool(Index: Integer): TRequiredTool;
    function GetToolsAreRequired: Boolean;
  protected
    ToolList : TList;
    procedure RebuildInternalValues; override;
    procedure RebuildSingleValue(Row, Col :Integer); override;
    procedure CreateTable;
  public
    constructor Create;
    destructor Destroy; override;
    function AddTool : TRequiredTool;
    function IndexOf(ToolName : string) : Integer;
    property Tool[Index : Integer] : TRequiredTool read GetTool; default;
    procedure Clear;
    function Count : Integer;
    property ToolsAreRequired : Boolean read GetToolsAreRequired;
    function PriorityofToolType(ToolType : String): Integer;
  end;

implementation

const
  ToolNameField = 'ToolName';
  LengthField = 'Length';
  PriorityField = 'Priority';
  HasDependenciesField = 'HasDependencies';

{ TRequiredToolList }

function TRequiredToolList.AddTool : TRequiredTool;
begin
  Result := TRequiredTool.Create(ToolList.Count, Self);
  ToolList.Add(Result);
  ITable.Append(1);
end;

procedure TRequiredToolList.Clear;
begin
  BeginUpdate;
  try
    ITable.Clear(Self);
    while ITable.RecordCount < ToolList.Count do begin
      Tool[ToolList.Count - 1].Free;
      ToolList.Delete(ToolList.Count - 1);
    end;
//    RebuildInternalValues;
  finally
    EndUpdate;
  end;
end;

function TRequiredToolList.Count: Integer;
begin
  Result := ToolList.Count;
end;

constructor TRequiredToolList.Create;
begin
  inherited Create;

  ToolList := TList.Create;
  if StringArraySet.CreateArray('RequiredTools', Self, ITable) <> AAR_OK then begin
    StringArraySet.UseArray('RequiredTools', Self, ITable);
  end else begin
    CreateTable;
  end;
end;

procedure TRequiredToolList.CreateTable;
begin
  BeginUpdate;
  try
    ITable.AddField(ToolNameField);
    ITable.AddField(LengthField);
    ITable.AddField(PriorityField);
    ITable.AddField(HasDependenciesField);
  finally
    EndUpdate;
  end;
end;

destructor TRequiredToolList.Destroy;
begin
  ITable := nil;
  while ToolList.Count > 0 do begin
    Tool[ToolList.Count - 1].Free;
    ToolList.Delete(ToolList.Count - 1);
  end;

  inherited;
end;


function TRequiredToolList.GetTool(Index: Integer): TRequiredTool;
begin
  if Index < ToolList.Count then
    Result := TRequiredTool(ToolList[Index])
  else
    Result := nil;
end;

function TRequiredToolList.GetToolsAreRequired: Boolean;
var EntryNumber : Integer;
begin
  Result := False;
  EntryNumber := 0;
  Lock;
  try
    while (EntryNumber < Count) and (Result = False) do begin
      if Tool[EntryNumber].Priority < 2 then
        Result := True;
      Inc(EntryNumber)
    end;
  finally
    Unlock;
  end;
end;

function TRequiredToolList.IndexOf(ToolName: string): Integer;
begin
  for Result := 0 to Count - 1 do begin
    if CompareText(ToolName, Tool[Result].ToolName) = 0 then
      Exit;
  end;
  Result := -1;
end;

// Returnd the current priority for the given tooltype
function TRequiredToolList.PriorityofToolType(ToolType: String): Integer;
var EntryNumber : Integer;
Found : Boolean;
begin
  Result := -1;
  Found := False;
  EntryNumber := 0;
  Lock;
  try
    while (EntryNumber < Count) and (Found = False) do begin
      try
      if UpperCase(Tool[EntryNumber].FToolName) = UpperCase(ToolTYpe) then
        begin
        Found  := True;
        Result :=  Tool[EntryNumber].Priority;
        end;
      finally
      inc(EntryNumber);
      end;
    end;
  finally
    Unlock;
  end;

end;

procedure TRequiredToolList.RebuildInternalValues;
var I : Integer;
begin
  while ITable.RecordCount < ToolList.Count do begin
    Tool[ToolList.Count - 1].Free;
    ToolList.Delete(ToolList.Count - 1);
  end;

  // We cant use AddTool to add an OPP as this appends records.
  for I := ToolList.Count to ITable.RecordCount - 1 do begin
    ToolList.Add(TRequiredTool.Create(ToolList.Count, Self));
  end;

  for I := 0 to ITable.RecordCount - 1 do begin
    Tool[I].RebuildInternalValues;
    Tool[I].FIndex := I;
  end;
end;

procedure TRequiredToolList.RebuildSingleValue(Row, Col: Integer);
begin
  if Assigned(Tool[Row]) then
    Tool[Row].RebuildInternalValues;
end;

{ TRequiredTool }

constructor TRequiredTool.Create(AIndex: Integer; AParent : TRequiredToolList);
begin
  FIndex := AIndex;
  FParent := AParent;
  FDependentOn := TList.Create;
end;

destructor TRequiredTool.Destroy;
begin
  FDependentOn.Free;
  inherited;
end;

procedure TRequiredTool.RebuildInternalValues;
begin
  FToolName := FParent.GetFieldString(FIndex, ToolNameField);
  FLength := FParent.GetFieldDouble(FIndex, LengthField);
  FPriority := FParent.GetFieldInteger(FIndex, PriorityField);
  FHasDependencies := FParent.GetFieldInteger(FIndex, HasDependenciesField) <> 0;
end;

procedure TRequiredTool.SetLength(const Value: Double);
begin
  FLength := Value;
  FParent.SetFieldData(FIndex, LengthField, Format('%.3f', [FLength]));
end;

procedure TRequiredTool.SetToolName(const Value: string);
begin
  FToolName := Value;
  FParent.SetFieldData(FIndex, ToolNameField, FToolName);
end;

procedure TRequiredTool.SetPriority(const Value: Integer);
begin
  FPriority := Value;
  FParent.SetFieldData(FIndex, PriorityField, IntToStr(FPriority));
end;

procedure TRequiredTool.SetRecords(const Value: Integer);
begin
  FRecords := Value;
  FParent.SetFieldData(FIndex, LengthField, Format('%.3f', [FLength]));
end;


procedure TRequiredTool.SetHasDependencies(const Value: Boolean);
begin
  FHasDependencies := Value;
  FParent.SetFieldData(FIndex, HasDependenciesField, IntToStr(Ord(FHasDependencies)));
end;

end.
