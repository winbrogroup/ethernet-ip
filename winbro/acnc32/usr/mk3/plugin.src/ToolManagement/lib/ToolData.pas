unit ToolData;

interface

uses SysUtils, Classes, IStringArray, NamedPlugin, OCTTypes;

const
  BarrelCountField = 'BarrelCount';
  RemainingBarrelsField = 'RemainingBarrels';
  MaxToolLengthField = 'MaxToolLength';
  MinToolLengthField = 'MinToolLength';
  ActToolLengthField = 'ActualLength';
  UniqueToolXField = 'UniqueToolX';
  UniqueToolYField = 'UniqueToolY';
  UniqueToolZField = 'UniqueToolZ';
  UniqueToolCField = 'UniqueToolC';

  Ex1Field = 'Ex1';
  Ex2Field = 'Ex2';
  ExpiryField = 'Expiry';
  TPM1Field = 'TPM1';
  TPM2Field = 'TPM2';

  NormalFields = 7;

  TOOL_HIGH_PRESSURE = 8;

type
  TToolList = class;
  TToolData = class(TObject)
  private
    Index : Integer;
    Host : TToolList;

//    FVersion : WideString;
    FShuttleID : WideString;
    FShuttleSN : Integer;
    FRebuildTime : TDateTime;
    FStatusFlags : Integer;
    FBarrelCount : Integer;
    FRemainingBarrels : Integer;
    FMaxToolLength : Double;
    FMinToolLength : Double;
    FActToolLength : Double;
    FEx1 : Integer;
    FEx2 : Integer;
    FExpiry : TDateTime;
    FUniqueToolOffsets : array [0..3] of Double;
    FTPM1: Integer;
    FTPM2: Integer;
    function GetUniqueToolOffsets(Index: Integer): Double;
    procedure SetActToolLength(const Value: Double);
    procedure SetBarrelCount(const Value: Integer);
    procedure SetMaxToolLength(const Value: Double);
    procedure SetMinToolLength(const Value: Double);
    procedure SetRebuildTime(const Value: TDateTime);
    procedure SetRemainingBarrels(const Value: Integer);
    procedure SetShuttleID(const Value: WideString);
    procedure SetShuttleSN(const Value: Integer);
    procedure SetStatusFlags(const Value: Integer);
    procedure SetUniqueToolOffsets(Index: Integer; const Value: Double);
//    procedure SetVersion(const Value: WideString);
    procedure SetEx1(const Value: Integer);
    procedure SetEx2(const Value: Integer);
    procedure SetExpiry(const Value: TDateTime);

    function GetTotalRemaining: Double;
    function GetBarrelRemaining: Double;
    procedure SetTPM1(const Value: Integer);
    procedure SetTPM2(const Value: Integer);
    procedure SetHP(const Value: Boolean);
    function GetHP: Boolean;
  protected
    procedure RebuildInternalValues(AIndex : Integer);
    procedure Copy(Src: TToolData);
  public
    constructor Create(AHost : TToolList; AIndex : Integer);
    destructor Destroy; override;
//    property Version : WideString read FVersion write SetVersion;
    property ShuttleID : WideString read FShuttleID write SetShuttleID;
    property ShuttleSN : Integer read FShuttleSN write SetShuttleSN;
    property RebuildTime : TDateTime read FRebuildTime write SetRebuildTime;
    property StatusFlags : Integer read FStatusFlags write SetStatusFlags;
    property BarrelCount : Integer read FBarrelCount write SetBarrelCount;
    property RemainingBarrels : Integer read FRemainingBarrels write SetRemainingBarrels;
    property MaxToolLength : Double read FMaxToolLength write SetMaxToolLength;
    property MinToolLength : Double read FMinToolLength write SetMinToolLength;
    property ActToolLength : Double read FActToolLength write SetActToolLength;
    property UniqueToolOffsets[Index : Integer] : Double read GetUniqueToolOffsets write SetUniqueToolOffsets;
    property Ex1 : Integer read FEx1 write SetEx1;
    property Ex2 : Integer read FEx2 write SetEx2;
    property Expiry : TDateTime read FExpiry write SetExpiry;
    property BarrelRemaining : Double read GetBarrelRemaining;
    property TotalRemaining : Double read GetTotalRemaining;
    property TPM1 : Integer read FTPM1 write SetTPM1;
    property TPM2 : Integer read FTPM2 write SetTPM2;
    property HP: Boolean read GetHP write SetHP;
  end;

  TToolList = class(TAbstractStringData)
  private
    ToolList : TList;
    function GetTool(index: Integer): TToolData;
    function IndexOf(Tool : TToolData) : Integer;
    procedure CleanTable;
    procedure CreateWithName(TableName: string; Clean: Boolean);
  protected
    procedure RebuildSingleValue(Row, Col :Integer); override;
    procedure RebuildInternalValues; override;
    procedure CreateTable;
    procedure RebuildIndex;
  public
    constructor Create; overload;
    constructor Create(TableName: string); overload;
    destructor Destroy; override;
    function AddTool : TToolData;
    function Count : Integer;
    property Tool[index : Integer] : TToolData read GetTool; default;
    procedure Clean; override;
    procedure RemoveTool(T : TToolData);
    function IsToolAtEx1(Ex1 : Integer) : Boolean;
    function ToolAtEx1(Ex1 : Integer) : TToolData;
    function ToolTypeInMachine(ToolType : String): Boolean;
    procedure CopyTool(Target, Src: TToolData);
    function Find(Id: string; Sn: Integer): TToolData;
  end;

implementation

{ TToolData }

constructor TToolData.Create(AHost : TToolList; AIndex : Integer);
begin
  inherited Create;
  Index := AIndex;
  Host := AHost;
end;

destructor TToolData.Destroy;
begin
  inherited;
end;

function TToolData.GetUniqueToolOffsets(Index: Integer): Double;
begin
  Result := FUniqueToolOffsets[Index mod 4];
end;

procedure TToolData.SetActToolLength(const Value: Double);
begin
  FActToolLength := Value;
  Host.SetFieldData(Index, ActToolLengthField, FloatToStr(Value));
end;

procedure TToolData.SetBarrelCount(const Value: Integer);
begin
  FBarrelCount := Value;
  Host.SetFieldData(Index, BarrelCountField, IntToStr(Value));
end;

procedure TToolData.SetMaxToolLength(const Value: Double);
begin
  FMaxToolLength := Value;
  Host.SetFieldData(Index, MaxToolLengthField, FloatToStr(Value));
end;

procedure TToolData.SetMinToolLength(const Value: Double);
begin
  FMinToolLength := Value;
  Host.SetFieldData(Index, MinToolLengthField, FloatToStr(Value));
end;

procedure TToolData.SetRebuildTime(const Value: TDateTime);
begin
  FRebuildTime := Value;
  Host.SetFieldData(Index, RebuildTimeField, FloatToStr(Value));
end;

procedure TToolData.SetRemainingBarrels(const Value: Integer);
begin
  FRemainingBarrels := Value;
  Host.SetFieldData(Index, RemainingBarrelsField, IntToStr(Value));
end;

procedure TToolData.SetShuttleID(const Value: WideString);
begin
  FShuttleID := UpperCase(Value);
  Host.SetFieldData(Index, ShuttleIDField, FShuttleID);
end;

procedure TToolData.SetShuttleSN(const Value: Integer);
begin
  FShuttleSN := Value;
  Host.SetFieldData(Index, ShuttleSNField, IntToStr(Value));
end;

procedure TToolData.SetStatusFlags(const Value: Integer);
begin
  FStatusFlags := Value;
  Host.SetFieldData(Index, StatusFlagsField, IntToStr(Value));
end;

procedure TToolData.SetUniqueToolOffsets(Index: Integer; const Value: Double);
begin
  FUniqueToolOffsets[Index mod 4] := Value;
  Host.SetFieldData(Self.Index, UniqueToolXField, FloatToStr(FUniqueToolOffsets[0]));
  Host.SetFieldData(Self.Index, UniqueToolYField, FloatToStr(FUniqueToolOffsets[1]));
  Host.SetFieldData(Self.Index, UniqueToolZField, FloatToStr(FUniqueToolOffsets[2]));
  Host.SetFieldData(Self.Index, UniqueToolCField, FloatToStr(FUniqueToolOffsets[3]));
end;

{procedure TToolData.SetVersion(const Value: WideString);
begin
  FVersion := Value;
  Host.SetFieldData(Index, VersionField, Value);
end; }

procedure TToolData.SetEx1(const Value: Integer);
begin
  FEx1 := Value;
  Host.SetFieldData(Index, Ex1Field, IntToStr(Value));
end;

procedure TToolData.SetEx2(const Value: Integer);
begin
  FEx2 := Value;
  Host.SetFieldData(Index, Ex2Field, IntToStr(Value));
end;

procedure TToolData.SetExpiry(const Value: TDateTime);
begin
  FExpiry := Value;
  Host.SetFieldData(Index, ExpiryField, FloatToStr(Value));
end;

function TToolData.GetTotalRemaining: Double;
begin
  Result := ActToolLength - MinToolLength + (RemainingBarrels * (MaxToolLength - MinToolLength));
end;

function TToolData.GetBarrelRemaining: Double;
begin
  Result := ActToolLength - MinToolLength;
end;

procedure TToolData.RebuildInternalValues(AIndex : Integer);
begin
  Index := AIndex;
  FShuttleID := Host.GetFieldString(Index, ShuttleIDField);
  FShuttleSN := Host.GetFieldInteger(Index, ShuttleSNField);
  FRebuildTime := Host.GetFieldDouble(Index, RebuildTimeField);
  FStatusFlags := Host.GetFieldInteger(Index, StatusFlagsField);
  FBarrelCount := Host.GetFieldInteger(Index, BarrelCountField);
  FRemainingBarrels := Host.GetFieldInteger(Index, RemainingBarrelsField);
  FMaxToolLength := Host.GetFieldDouble(Index, MaxToolLengthField);
  FMinToolLength := Host.GetFieldDouble(Index, MinToolLengthField);
  FActToolLength := Host.GetFieldDouble(Index, ActToolLengthField);
  FUniqueToolOffsets[0] := Host.GetFieldDouble(Index, UniqueToolXField);
  FUniqueToolOffsets[1] := Host.GetFieldDouble(Index, UniqueToolYField);
  FUniqueToolOffsets[2] := Host.GetFieldDouble(Index, UniqueToolZField);
  FUniqueToolOffsets[3] := Host.GetFieldDouble(Index, UniqueToolCField);
  FEx1 := Host.GetFieldInteger(Index, Ex1Field);
  FEx2 := Host.GetFieldInteger(Index, Ex2Field);
  FExpiry := Host.GetFieldDouble(Index, ExpiryField);
  FTPM1:= Host.GetFieldInteger(Index, TPM1Field);
  FTPM2:= Host.GetFieldInteger(Index, TPM2Field);
end;

procedure TToolData.SetTPM2(const Value: Integer);
begin
  FTPM2 := Value;
  Host.SetFieldData(Index, TPM2Field, IntToStr(Value));
end;

procedure TToolData.SetTPM1(const Value: Integer);
begin
  FTPM1 := Value;
  Host.SetFieldData(Index, TPM1Field, IntToStr(Value));
end;

procedure TToolData.SetHP(const Value: Boolean);
begin
  if Value then
    StatusFlags:= StatusFlags or TOOL_HIGH_PRESSURE
  else
    StatusFlags:= StatusFlags and not TOOL_HIGH_PRESSURE;
  Host.SetFieldData(Index, StatusFlagsField, IntToStr(StatusFlags));
end;

function TToolData.GetHP: Boolean;
begin
  Result:= (StatusFlags and TOOL_HIGH_PRESSURE) <> 0;
end;


procedure TToolData.Copy(Src: TToolData);
begin
  ShuttleID:= Src.ShuttleID;
  ShuttleSN:= Src.ShuttleSN;
  RebuildTime:= Src.RebuildTime;
  StatusFlags:= Src.StatusFlags;
  BarrelCount:= Src.BarrelCount;
  RemainingBarrels:= Src.RemainingBarrels;
  MaxToolLength:= Src.MaxToolLength;
  MinToolLength:= Src.MinToolLength;
  ActToolLength:= Src.ActToolLength;
  UniqueToolOffsets[0]:= Src.UniqueToolOffsets[0];
  UniqueToolOffsets[1]:= Src.UniqueToolOffsets[1];
  UniqueToolOffsets[2]:= Src.UniqueToolOffsets[2];
  UniqueToolOffsets[3]:= Src.UniqueToolOffsets[3];
  Ex1:= Src.Ex1;
  Ex2:= Src.Ex2;
  Expiry:= Src.Expiry;
  TPM1:= Src.TPM1;
  TPM2:= Src.TPM2;
  HP:= Src.HP;
end;

{ TToolList }

function TToolList.AddTool: TToolData;
begin
  Lock;
  try
    Result := TToolData.Create(Self, -1);
    Result.Index := ITable.RecordCount;

    ToolList.Add(Result);

    ITable.Append(1);
  finally
    Unlock;
  end;
end;


procedure TToolList.Clean;
begin
  inherited Clean;
end;

function TToolList.Count: Integer;
begin
  Result := ToolList.Count;
end;

constructor TToolList.Create;
begin
  inherited Create;

  CreateWithName('ToolData', True);
end;

constructor TToolList.Create(TableName: string);
begin
  inherited Create;

  CreateWithName(TableName, False);
end;

procedure TToolList.CreateWithName(TableName: string; Clean: Boolean);
begin
  if StringArraySet.CreateArray(TableName, Self, ITable) <> AAR_OK then begin
    StringArraySet.UseArray(TableName, Self, ITable);
    if ITable.FieldCount <> 18 then begin
      CleanTable;
      CreateTable;
    end;
  end else begin
    CreateTable;
  end;
  ToolList := TList.Create;
  RebuildInternalValues;
end;


procedure TToolList.CreateTable;
begin
  BeginUpdate;
  try
    ITable.Clear;
    ITable.AddField(ShuttleIDField);
    ITable.AddField(ShuttleSNField);
    ITable.AddField(RebuildTimeField);
    ITable.AddField(StatusFlagsField);
    ITable.AddField(BarrelCountField);
    ITable.AddField(RemainingBarrelsField);
    ITable.AddField(MaxToolLengthField);
    ITable.AddField(MinToolLengthField);
    ITable.AddField(ActToolLengthField);
    ITable.AddField(UniqueToolXField);
    ITable.AddField(UniqueToolYField);
    ITable.AddField(UniqueToolZField);
    ITable.AddField(UniqueToolCField);
    ITable.AddField(Ex1Field);
    ITable.AddField(Ex2Field);
    ITable.AddField(ExpiryField);
    ITable.AddField(TPM1Field);
    ITable.AddField(TPM2Field);
  finally
    EndUpdate;
  end;
end;

procedure TToolList.CleanTable;
var F: WideString;
begin
  BeginUpdate;
  try
    while ITable.FieldCount > 0 do begin
      ITable.FieldNameAtIndex(0, F);
      ITable.DeleteField(F);
    end;
  finally
    EndUpdate;
  end;
end;

destructor TToolList.Destroy;
begin
  ITable := nil;
  inherited;
end;

function TToolList.GetTool(Index: Integer): TToolData;
begin
  Result := TToolData(ToolList[Index]);
end;

function TToolList.IndexOf(Tool: TToolData): Integer;
begin
  Result := ToolList.IndexOf(Tool);
end;

// For simplicity rebuild the entire row
procedure TToolList.RebuildSingleValue(Row, Col :Integer);
begin
  Tool[Row].RebuildInternalValues(Row);
end;

procedure TToolList.RebuildInternalValues;
var I : Integer;
    Tmp : TToolData;
begin
  for I := ITable.RecordCount to ToolList.Count - 1 do begin
    Tool[0].Free;
    ToolList.Delete(0);
  end;

  for I := 0 to ITable.RecordCount - 1 do begin
    if I >= ToolList.Count then begin
      Tmp := TToolData.Create(Self, I);
      ToolList.Add(Tmp);
      Tmp.RebuildInternalValues(I);
    end else begin
      Tool[I].RebuildInternalValues(I);
    end;
  end;
end;

procedure TToolList.RemoveTool(T : TToolData);
var I : Integer;
begin
  BeginUpdate;
  try
    I := IndexOf(T);
    ITable.Delete(T.Index);
    T.Free;
    ToolList.Delete(I);
    RebuildIndex;
  finally
    EndUpdate;
  end;
end;

procedure TToolList.RebuildIndex;
var I : Integer;
begin
  for I := 0 to Count - 1 do begin
    Tool[I].Index := I;
  end;
end;

function TToolList.IsToolAtEx1(Ex1: Integer): Boolean;
begin
  Result := Assigned(ToolAtEx1(Ex1));
end;

// NOTE: if you keep a reference to the result it must be
// held inside a lock.
function TToolList.ToolAtEx1(Ex1: Integer): TToolData;
var I : Integer;
begin
  Result := nil;
  Lock;
  try
    for I := 0 to Count - 1 do begin
      if Tool[I].Ex1 = Ex1 then begin
        Result := Tool[I];
        Exit;
      end;
    end;
  finally
    Unlock;
  end;
end;


function TToolList.ToolTypeInMachine(ToolType: String): Boolean;
var I : Integer;
begin
  Result := False;
  Lock;
  try
    for I := 0 to Count - 1 do begin
      if Uppercase(Tool[I].FShuttleID) = UpperCase(ToolType) then
        begin
        Result := True;
        Exit;
        end;
      end;
  finally
    Unlock;
  end;

end;


procedure TToolList.CopyTool(Target, Src: TToolData);
begin
  BeginUpdate;
  try
    Target.Copy(Src);
  finally
    EndUpdate;
  end;
end;

function TToolList.Find(Id: string; Sn: Integer): TToolData;
var I : Integer;
begin
  Result := nil;
  Lock;
  try
    for I := 0 to Count - 1 do begin
      if (Uppercase(Tool[I].FShuttleID) = UpperCase(Id)) and (Tool[I].FShuttleSN = Sn) then begin
        Result := Tool[I];
        Exit;
      end;
    end;
  finally
    Unlock;
  end;
end;

end.
