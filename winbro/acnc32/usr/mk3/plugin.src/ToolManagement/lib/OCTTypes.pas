unit OCTTypes;

interface

uses SysUtils, Classes, IStringArray, Windows, NamedPlugin, OCTUtils;

const
//  VersionField = 'Version';
  ShuttleIDField = 'ShuttleID';
  ShuttleSNField = 'ShuttleSN';
  RebuildTimeField = 'RebuildTime';
  StatusFlagsField = 'StatusFlags';

type
  TTagStatusFlag = ( psfDataValid, psfProduction, psfDevelopment, psfInspectionRequired );
  TTagStatusFlags = set of TTagStatusFlag;

  TProbeData = array[0..2, 0..2] of Double;

  TOCTResultCode = (
    orcComplete,
    orcReworkSuccess,
    orcReworkRequired,
    orcReworkNonConform,
    orcOPPAbandoned,
    orcOCTAbandoned
  );

  TRecoveryOptions =(
    roNone,
    roRestartCurrent,
    roAbandonCurrent,
    roAbandonOCT,
    roAbandonPartInCell,
    roReworkOPP
  );

  TCellMode = (
    cmProduction,
    cmStandalone,
    cmRework,
    cmDevelopment
  );


  TTableCellEvent = procedure(Sender : TObject; Row : Integer; Field : WideString) of Object;
  TAbstractStringData = class(TInterfacedObject, IACNC32StringArrayNotify)
  protected
    RefCount : Integer;
    FOnChange : TNotifyEvent;
    FOnTableChange : TNotifyEvent;
    FOnCellChange : TTableCellEvent;

    ITable : IACNC32StringArray;
    UpdateActive : Integer;
    DeferredUpdate : Boolean;
    procedure CellChange(Table : IACNC32StringArray; Row, Col : Integer); stdcall;
    procedure TableChange(Table : IACNC32StringArray); stdcall;
    procedure RebuildInternalValues; virtual; abstract;
    procedure RebuildSingleValue(Row, Col :Integer); virtual; abstract;
  public
    procedure BeginUpdate;
    procedure EndUpdate;
//    constructor Create; virtual;
    destructor Destroy; override;
    procedure Clean; virtual;
    procedure SetFieldData(Index : Integer; Field : WideString; Value : WideString);
    function StorageDate(DT : TDateTime) : WideString;
    function GetFieldDateTime(Index : Integer; Field : string) : TDateTime;
    function GetFieldString(Index : Integer; Field : string) : string;
    function GetFieldInteger(Index : Integer; Field : string) : Integer;
    function GetFieldDouble(Index : Integer; Field : string) : Double;
    procedure Lock;
    procedure Unlock;
    procedure Close;
    procedure Import(const Filename : string);
    procedure Export(const Filename : string);
    function Locked : Boolean;
    property OnChange : TNotifyEvent read FOnChange write FOnChange;
    property OnCellChange : TTableCellEvent read FOnCellChange write FOnCellChange;
    property OnTableChange : TNotifyEvent read FOnTableChange write FOnTableChange;


{    function QueryInterface(const IID: TGUID; out Obj): HResult; stdcall;
    function _AddRef : Integer; stdcall;
    function _Release : Integer; stdcall; }
  end;

const
  NameOCTMode = 'OCTMode';
  NameOCTReqResume = 'OCTReqResume';
  NameCM_CellMode = 'CM_CellMode';
  NameCM_CellModeName = 'CM_CellModeName';
  NameOCTResetRewind = 'OCTResetRewind';
  NameOCTRequiredTool = 'OCTRequiredTool';
  NameOCTLoadedName = 'OCTLoadedName';
  NameOCTLoadedOK = 'OCTLoadedOK';
  NameOCTInterrupted = 'OCTInterrupted';

  NameOCTRunning = 'OCTRunning';
  NameOCTIndex = 'OCTIndex';
  NameOCTSingleStep = 'OCTSingleStep';

  NameCheckDNCPaths = 'CheckDNCPaths';


  NameOCT_PreCMM = 'OCT_PreCMM';
  NameOCT_PostCMM = 'OCT_PostCMM';
  NameOCT_Production = 'OCT_Production';
  NameOCT_Rework = 'OCT_Rework';
  NamePartShuttleID = 'PartShuttleID';
  NamePartType = 'PartType';
  NamePartSFC = 'PartSFC';
  NameCMMLocalFilename = 'CMMLocalFilename';
  NameCMMDNCFilename = 'CMMDNCFilename';
  NameCMMOffsetsPath = 'CMMOffsetsPath';
  NameCMMOffsetsFile = 'CMMOffsetsFile';
  NameOpIndex = 'OpIndex';

implementation


procedure TAbstractStringData.CellChange(Table: IACNC32StringArray; Row, Col: Integer);
var FieldName : WideString;
begin
  if(not Locked) then begin
    RebuildSingleValue(Row, Col);
    if Assigned(OnCellChange) then begin
      ITable.FieldNameAtIndex(Row, FieldName);
      OnCellChange(Self, Row, FieldName);
    end;
    if Assigned(OnChange) then
      OnChange(Self);
  end else begin
    DeferredUpdate := True;
  end;
end;

procedure TAbstractStringData.TableChange(Table: IACNC32StringArray);
begin
  if(not Locked) then begin
    RebuildInternalValues;
    if Assigned(OnChange) then
      OnChange(Self);
    if Assigned(OnTableChange) then
      OnTableChange(Self);
  end else begin
    DeferredUpdate := True;
  end;
end;

procedure TAbstractStringData.Lock;
begin
  Inc(UpdateActive);
end;

procedure TAbstractStringData.Unlock;
begin
  Dec(UpdateActive);
  if not Locked and DeferredUpdate then begin
    TableChange(ITable);
    DeferredUpdate := False;
  end;
end;

function TAbstractStringData.Locked : Boolean;
begin
  Result := UpdateActive <> 0;
end;

procedure TAbstractStringData.BeginUpdate;
begin
  if Assigned(ITable) then
    ITable.BeginUpdate;
end;

procedure TAbstractStringData.EndUpdate;
begin
  if Assigned(ITable) then
    ITable.EndUpdate(Self);
end;


procedure TAbstractStringData.SetFieldData(Index: Integer; Field, Value: WideString);
begin
  ITable.SetFieldValue(Index, Field, Value, Self);
end;

function TAbstractStringData.StorageDate(DT: TDateTime): WideString;
var ST : TSystemTime;
begin
  DateTimeToSystemTime(DT, ST);
  // YYYY-MM-DD hh:mm:ss  This seems to be an SQL standard as it Ascii / chronologically the same
  Result := Format('%4d-%.2d-%.2d %.2d:%.2d:%.2d',
     [ST.wYear, ST.wMonth, ST.wDay, ST.wHour, ST.wMinute, ST.wSecond ]  );
end;


function TAbstractStringData.GetFieldDateTime(Index: Integer;
  Field: string): TDateTime;
var TimeValue : WideString;
begin
  ITable.GetFieldValue(Index, Field, TimeValue);
  try
    Result := StorageTimeToDateTime(TimeValue);
  except
    Result := Now;
  end;
end;

function TAbstractStringData.GetFieldDouble(Index: Integer; Field: string): Double;
var Tmp : WideString;
begin
  ITable.GetFieldValue(Index, Field, Tmp);
  Result := StrToFloatDef(Tmp, 0);
end;

function TAbstractStringData.GetFieldInteger(Index: Integer; Field: string): Integer;
var Tmp : WideString;
begin
  ITable.GetFieldValue(Index, Field, Tmp);
  Result := StrToIntDef(Tmp, 0);
end;

function TAbstractStringData.GetFieldString(Index: Integer; Field: string): string;
var Tmp : WideString;
begin
  ITable.GetFieldValue(Index, Field, Tmp);
  Result := Tmp;
end;



// TEST CODE --------
{function TAbstractStringData.QueryInterface(const IID: TGUID; out Obj): HResult; stdcall;
const
  E_NOINTERFACE = HResult($80004002);
begin
  if GetInterface(IID, Obj) then Result := 0 else Result := E_NOINTERFACE;
end;


function TAbstractStringData._AddRef : Integer; stdcall;
begin
  Inc(RefCount);
  Result := RefCount;
end;

var TTT : Integer;
function TAbstractStringData._Release : Integer; stdcall;
begin
  Dec(RefCount);
  Result := RefCount;
  if RefCount = 0 then begin
    Inc(TTT);
  end;
end; }

{constructor TAbstractStringData.Create;
begin
  inherited Create;
end; }

procedure TAbstractStringData.Close;
begin
  StringArraySet.ReleaseArray(Self, ITable);
end;

destructor TAbstractStringData.Destroy;
begin
  ITable := nil;
  inherited;
end;

procedure TAbstractStringData.Clean;
begin
  ITable.Clear; // Explicit clear with callbacks to client.
end;

procedure TAbstractStringData.Import(const Filename: string);
begin
  BeginUpdate;
  try
    ITable.ImportFromCSV(Filename, False, Self);
    RebuildInternalValues;
  finally
    EndUpdate;
  end;
end;

procedure TAbstractStringData.Export(const Filename: string);
begin
  BeginUpdate;
  try
    ITable.ExportToCSV(Filename);
  finally
    EndUpdate;
  end;
end;

end.
