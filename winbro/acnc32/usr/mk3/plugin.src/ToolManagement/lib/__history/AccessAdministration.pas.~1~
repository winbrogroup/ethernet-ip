unit AccessAdministration;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  CNCTypes, Buttons, ExtCtrls, StdCtrls, ACNC_TouchSoftkey;

type
  TAccessAdministrator = class(TForm)
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Panel1: TPanel;
    BtnMECellPro: TSpeedButton;
    BtnMECellRew: TSpeedButton;
    BtnMECellDev: TSpeedButton;
    BtnMECellSta: TSpeedButton;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    BtnPDCellPro: TSpeedButton;
    BtnPDCellRew: TSpeedButton;
    BtnPDCellDev: TSpeedButton;
    BtnPDCellSta: TSpeedButton;
    BtnOPCellPro: TSpeedButton;
    BtnOPCellRew: TSpeedButton;
    BtnOPCellDev: TSpeedButton;
    BtnOPCellSta: TSpeedButton;
    GroupBox2: TGroupBox;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    BtnMEFilePro: TSpeedButton;
    BtnMEFileRew: TSpeedButton;
    BtnMEFileDevR: TSpeedButton;
    BtnMEFileDevW: TSpeedButton;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    BtnPDFilePro: TSpeedButton;
    BtnPDFileRew: TSpeedButton;
    BtnPDFileDevR: TSpeedButton;
    BtnPDFileDevW: TSpeedButton;
    BtnOPFilePro: TSpeedButton;
    BtnOPFileRew: TSpeedButton;
    BtnOPFileDevR: TSpeedButton;
    BtnOPFileDevW: TSpeedButton;
    Panel2: TPanel;
    Label15: TLabel;
    Label16: TLabel;
    Panel3: TPanel;
    BtnMEFileLocR: TSpeedButton;
    BtnMEFileLocW: TSpeedButton;
    BtnPDFileLocR: TSpeedButton;
    BtnPDFileLocW: TSpeedButton;
    BtnOPFileLocR: TSpeedButton;
    BtnOPFileLocW: TSpeedButton;
    TouchSoftOK: TTouchSoftkey;
    TouchSoftCancel: TTouchSoftkey;
    procedure BtnCellClick(Sender: TObject);
    procedure BtnFileClick(Sender: TObject);
    procedure TouchSoftOKClick(Sender: TObject);
    procedure TouchSoftCancelClick(Sender: TObject);
  private
    procedure ReadVisual;
    procedure WriteVisual;
  public
    class procedure Execute;
  end;

var
  AccessAdministrator: TAccessAdministrator;

type
  TCellAccess = record
    ProductionMode : TAccessLevels;
    ReworkMode : TAccessLevels;
    DevelopmentMode : TAccessLevels;
    StandaloneMode : TAccessLevels;
    ProductionRead : TAccessLevels;
    ReworkRead : TAccessLevels;
    DevelRead : TAccessLevels;
    DevelSave : TAccessLevels;
    LocalRead : TAccessLevels;
    LocalSave : TAccessLevels;
  end;

var
  CellAccess : TCellAccess = (
    ProductionMode : [AccessLevelOperator..AccessLevelOEM2];
    ReworkMode : [AccessLevelProgrammer..AccessLevelOEM2];
    DevelopmentMode : [AccessLevelProgrammer..AccessLevelOEM2];
    StandaloneMode : [AccessLevelProgrammer..AccessLevelOEM2];

    ProductionRead : [AccessLevelSupervisor..AccessLevelOEM2];
    ReworkRead : [AccessLevelSupervisor..AccessLevelOEM2];
    DevelRead : [AccessLevelSupervisor..AccessLevelOEM2];
    DevelSave : [AccessLevelProgrammer..AccessLevelOEM2];
    LocalRead : [AccessLevelSupervisor..AccessLevelOEM2];
    LocalSave : [AccessLevelProgrammer..AccessLevelOEM2];
  );


implementation

{$R *.DFM}

procedure AccessAssign(var A : TAccessLevels; V : TAccessLevel; B : TSpeedButton);
begin
  if B.Down then
    Include(A, V)
  else
    Exclude(A, V);

  Exclude(A, AccessLevelNone);
  Include(A, AccessLevelSupervisor);
  Include(A, AccessLevelMaintenance);
  Include(A, AccessLevelOEM1);
  Include(A, AccessLevelOEM2);
end;

procedure ButtonSet(A : TAccessLevels; V : TAccessLevel; B : TSpeedButton);
begin
  B.Down := V in A;
end;

class procedure TAccessAdministrator.Execute;
begin
  if not Assigned(AccessAdministrator) then begin
    AccessAdministrator := TAccessAdministrator.Create(nil);
    try
      AccessAdministrator.WriteVisual;
      if AccessAdministrator.ShowModal = mrOK then
        AccessAdministrator.ReadVisual;
    finally
      AccessAdministrator.Free;
      AccessAdministrator := nil;
    end;
  end;
end;

{
    AccessLevelNone,
    AccessLevelOperator,         OP
    AccessLevelSupervisor,
    AccessLevelProgrammer,       PD
    AccessLevelMaintenance,
    AccessLevelAdministrator,    ME
    AccessLevelOEM1,
    AccessLevelOEM2
 }

procedure TAccessAdministrator.BtnCellClick(Sender: TObject);
begin
//
end;

procedure TAccessAdministrator.BtnFileClick(Sender: TObject);
begin
//
end;

procedure TAccessAdministrator.TouchSoftOKClick(Sender: TObject);
begin
  ModalResult := mrOK;
end;

procedure TAccessAdministrator.TouchSoftCancelClick(Sender: TObject);
begin
  ModalResult := mrCancel;
end;

procedure TAccessAdministrator.ReadVisual;
begin
  AccessAssign(CellAccess.ProductionMode, AccessLevelAdministrator, btnMECellPro);
  AccessAssign(CellAccess.ProductionMode, AccessLevelProgrammer, btnPDCellPro);
  AccessAssign(CellAccess.ProductionMode, AccessLevelOperator, btnOPCellPro);
  AccessAssign(CellAccess.ReworkMode, AccessLevelAdministrator, btnMECellRew);
  AccessAssign(CellAccess.ReworkMode, AccessLevelProgrammer, btnPDCellRew);
  AccessAssign(CellAccess.ReworkMode, AccessLevelOperator, btnOPCellRew);
  AccessAssign(CellAccess.DevelopmentMode, AccessLevelAdministrator, btnMECellDev);
  AccessAssign(CellAccess.DevelopmentMode, AccessLevelProgrammer, btnPDCellDev);
  AccessAssign(CellAccess.DevelopmentMode, AccessLevelOperator, btnOPCellDev);
  AccessAssign(CellAccess.StandaloneMode, AccessLevelAdministrator, btnMECellSta);
  AccessAssign(CellAccess.StandaloneMode, AccessLevelProgrammer, btnPDCellSta);
  AccessAssign(CellAccess.StandaloneMode, AccessLevelOperator, btnOPCellSta);

  AccessAssign(CellAccess.ProductionRead, AccessLevelAdministrator, btnMEFilePro);
  AccessAssign(CellAccess.ProductionRead, AccessLevelProgrammer, btnPDFilePro);
  AccessAssign(CellAccess.ProductionRead, AccessLevelOperator, btnOPFilePro);

  AccessAssign(CellAccess.ReworkRead, AccessLevelAdministrator, btnMEFileRew);
  AccessAssign(CellAccess.ReworkRead, AccessLevelProgrammer, btnPDFileRew);
  AccessAssign(CellAccess.ReworkRead, AccessLevelOperator, btnOPFileRew);

  AccessAssign(CellAccess.DevelRead, AccessLevelAdministrator, btnMEFileDevR);
  AccessAssign(CellAccess.DevelRead, AccessLevelProgrammer, btnPDFileDevR);
  AccessAssign(CellAccess.DevelRead, AccessLevelOperator, btnOPFileDevR);

  AccessAssign(CellAccess.DevelSave, AccessLevelAdministrator, btnMEFileDevW);
  AccessAssign(CellAccess.DevelSave, AccessLevelProgrammer, btnPDFileDevW);
  AccessAssign(CellAccess.DevelSave, AccessLevelOperator, btnOPFileDevW);

  AccessAssign(CellAccess.LocalRead, AccessLevelAdministrator, btnMEFileLocR);
  AccessAssign(CellAccess.LocalRead, AccessLevelProgrammer, btnPDFileLocR);
  AccessAssign(CellAccess.LocalRead, AccessLevelOperator, btnOPFileLocR);

  AccessAssign(CellAccess.LocalSave, AccessLevelAdministrator, btnMEFileLocW);
  AccessAssign(CellAccess.LocalSave, AccessLevelProgrammer, btnPDFileLocW);
  AccessAssign(CellAccess.LocalSave, AccessLevelOperator, btnOPFileLocW);
end;

procedure TAccessAdministrator.WriteVisual;
begin
  ButtonSet(CellAccess.ProductionMode, AccessLevelAdministrator, btnMECellPro);
  ButtonSet(CellAccess.ProductionMode, AccessLevelProgrammer, btnPDCellPro);
  ButtonSet(CellAccess.ProductionMode, AccessLevelOperator, btnOPCellPro);
  ButtonSet(CellAccess.ReworkMode, AccessLevelAdministrator, btnMECellRew);
  ButtonSet(CellAccess.ReworkMode, AccessLevelProgrammer, btnPDCellRew);
  ButtonSet(CellAccess.ReworkMode, AccessLevelOperator, btnOPCellRew);
  ButtonSet(CellAccess.DevelopmentMode, AccessLevelAdministrator, btnMECellDev);
  ButtonSet(CellAccess.DevelopmentMode, AccessLevelProgrammer, btnPDCellDev);
  ButtonSet(CellAccess.DevelopmentMode, AccessLevelOperator, btnOPCellDev);
  ButtonSet(CellAccess.StandaloneMode, AccessLevelAdministrator, btnMECellSta);
  ButtonSet(CellAccess.StandaloneMode, AccessLevelProgrammer, btnPDCellSta);
  ButtonSet(CellAccess.StandaloneMode, AccessLevelOperator, btnOPCellSta);

  ButtonSet(CellAccess.ProductionRead, AccessLevelAdministrator, btnMEFilePro);
  ButtonSet(CellAccess.ProductionRead, AccessLevelProgrammer, btnPDFilePro);
  ButtonSet(CellAccess.ProductionRead, AccessLevelOperator, btnOPFilePro);

  ButtonSet(CellAccess.ReworkRead, AccessLevelAdministrator, btnMEFileRew);
  ButtonSet(CellAccess.ReworkRead, AccessLevelProgrammer, btnPDFileRew);
  ButtonSet(CellAccess.ReworkRead, AccessLevelOperator, btnOPFileRew);

  ButtonSet(CellAccess.DevelRead, AccessLevelAdministrator, btnMEFileDevR);
  ButtonSet(CellAccess.DevelRead, AccessLevelProgrammer, btnPDFileDevR);
  ButtonSet(CellAccess.DevelRead, AccessLevelOperator, btnOPFileDevR);

  ButtonSet(CellAccess.DevelSave, AccessLevelAdministrator, btnMEFileDevW);
  ButtonSet(CellAccess.DevelSave, AccessLevelProgrammer, btnPDFileDevW);
  ButtonSet(CellAccess.DevelSave, AccessLevelOperator, btnOPFileDevW);

  ButtonSet(CellAccess.LocalRead, AccessLevelAdministrator, btnMEFileLocR);
  ButtonSet(CellAccess.LocalRead, AccessLevelProgrammer, btnPDFileLocR);
  ButtonSet(CellAccess.LocalRead, AccessLevelOperator, btnOPFileLocR);

  ButtonSet(CellAccess.LocalSave, AccessLevelAdministrator, btnMEFileLocW);
  ButtonSet(CellAccess.LocalSave, AccessLevelProgrammer, btnPDFileLocW);
  ButtonSet(CellAccess.LocalSave, AccessLevelOperator, btnOPFileLocW);
end;

end.
