unit Confirm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, ACNC_TouchSoftkey;

type
  TAreYouSure = class(TForm)
    TouchSoftkey1: TTouchSoftkey;
    TouchSoftkey2: TTouchSoftkey;
    Label1: TLabel;
    procedure TouchSoftkey1Click(Sender: TObject);
    procedure TouchSoftkey2Click(Sender: TObject);
  private
    { Private declarations }
  public
    class function Execute(const Text : string) : Boolean;
  end;

var
  AreYouSure: TAreYouSure;

implementation

{$R *.DFM}

{ TForm5 }

class function TAreYouSure.Execute(const Text: string): Boolean;
var Form : TAreYouSure;
begin
  Form := TAreYouSure.Create(nil);
  try
    Form.Label1.Caption := Text;
    Result := Form.ShowModal = mrOK;
  finally
    Form.Free;
  end;
end;

procedure TAreYouSure.TouchSoftkey1Click(Sender: TObject);
begin
  ModalResult := mrOK;
end;

procedure TAreYouSure.TouchSoftkey2Click(Sender: TObject);
begin
  ModalResult := mrCancel;
end;

end.
