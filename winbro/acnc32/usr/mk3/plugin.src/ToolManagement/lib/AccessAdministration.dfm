object AccessAdministrator: TAccessAdministrator
  Left = 337
  Top = 229
  BorderStyle = bsSingle
  Caption = 'Cell User Access'
  ClientHeight = 468
  ClientWidth = 398
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -16
  Font.Name = 'Verdana'
  Font.Style = [fsBold]
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 18
  object GroupBox1: TGroupBox
    Left = 0
    Top = 0
    Width = 398
    Height = 185
    Align = alTop
    Caption = 'Enter Cell Mode'
    TabOrder = 0
    object Label1: TLabel
      Left = 16
      Top = 56
      Width = 95
      Height = 18
      Caption = 'Production'
    end
    object Label2: TLabel
      Left = 16
      Top = 80
      Width = 69
      Height = 18
      Caption = 'Rework'
    end
    object Label3: TLabel
      Left = 16
      Top = 104
      Width = 116
      Height = 18
      Caption = 'Development'
    end
    object Label4: TLabel
      Left = 16
      Top = 128
      Width = 99
      Height = 18
      Caption = 'Standalone'
    end
    object BtnMECellPro: TSpeedButton
      Left = 200
      Top = 56
      Width = 35
      Height = 22
      AllowAllUp = True
      GroupIndex = 1
      OnClick = BtnCellClick
    end
    object BtnMECellRew: TSpeedButton
      Left = 200
      Top = 80
      Width = 35
      Height = 22
      AllowAllUp = True
      GroupIndex = 2
      OnClick = BtnCellClick
    end
    object BtnMECellDev: TSpeedButton
      Left = 200
      Top = 104
      Width = 35
      Height = 22
      AllowAllUp = True
      GroupIndex = 3
      OnClick = BtnCellClick
    end
    object BtnMECellSta: TSpeedButton
      Left = 200
      Top = 128
      Width = 35
      Height = 22
      AllowAllUp = True
      GroupIndex = 4
      OnClick = BtnCellClick
    end
    object Label5: TLabel
      Left = 200
      Top = 24
      Width = 26
      Height = 18
      Caption = 'ME'
    end
    object Label6: TLabel
      Left = 256
      Top = 24
      Width = 25
      Height = 18
      Caption = 'PD'
    end
    object Label7: TLabel
      Left = 312
      Top = 24
      Width = 26
      Height = 18
      Caption = 'OP'
    end
    object BtnPDCellPro: TSpeedButton
      Left = 256
      Top = 56
      Width = 35
      Height = 22
      AllowAllUp = True
      GroupIndex = 5
      OnClick = BtnCellClick
    end
    object BtnPDCellRew: TSpeedButton
      Left = 256
      Top = 80
      Width = 35
      Height = 22
      AllowAllUp = True
      GroupIndex = 6
      OnClick = BtnCellClick
    end
    object BtnPDCellDev: TSpeedButton
      Left = 256
      Top = 104
      Width = 35
      Height = 22
      AllowAllUp = True
      GroupIndex = 7
      OnClick = BtnCellClick
    end
    object BtnPDCellSta: TSpeedButton
      Left = 256
      Top = 128
      Width = 35
      Height = 22
      AllowAllUp = True
      GroupIndex = 8
      OnClick = BtnCellClick
    end
    object BtnOPCellPro: TSpeedButton
      Left = 312
      Top = 56
      Width = 35
      Height = 22
      AllowAllUp = True
      GroupIndex = 9
      OnClick = BtnCellClick
    end
    object BtnOPCellRew: TSpeedButton
      Left = 312
      Top = 80
      Width = 35
      Height = 22
      AllowAllUp = True
      GroupIndex = 10
      OnClick = BtnCellClick
    end
    object BtnOPCellDev: TSpeedButton
      Left = 312
      Top = 104
      Width = 35
      Height = 22
      AllowAllUp = True
      GroupIndex = 11
      OnClick = BtnCellClick
    end
    object BtnOPCellSta: TSpeedButton
      Left = 312
      Top = 128
      Width = 35
      Height = 22
      AllowAllUp = True
      GroupIndex = 12
      OnClick = BtnCellClick
    end
    object Panel1: TPanel
      Left = 160
      Top = 48
      Width = 6
      Height = 113
      BevelOuter = bvLowered
      TabOrder = 0
    end
  end
  object GroupBox2: TGroupBox
    Left = 0
    Top = 185
    Width = 398
    Height = 223
    Align = alClient
    Caption = 'File Access'
    TabOrder = 1
    object Label8: TLabel
      Left = 16
      Top = 56
      Width = 99
      Height = 18
      Caption = 'Prod. Read'
    end
    object Label9: TLabel
      Left = 16
      Top = 80
      Width = 120
      Height = 18
      Caption = 'Rework Read'
    end
    object Label10: TLabel
      Left = 16
      Top = 104
      Width = 100
      Height = 18
      Caption = 'Devel Read'
    end
    object Label11: TLabel
      Left = 16
      Top = 128
      Width = 97
      Height = 18
      Caption = 'Devel Save'
    end
    object BtnMEFilePro: TSpeedButton
      Left = 200
      Top = 56
      Width = 35
      Height = 22
      AllowAllUp = True
      GroupIndex = 1
      OnClick = BtnFileClick
    end
    object BtnMEFileRew: TSpeedButton
      Left = 200
      Top = 80
      Width = 35
      Height = 22
      AllowAllUp = True
      GroupIndex = 2
      OnClick = BtnFileClick
    end
    object BtnMEFileDevR: TSpeedButton
      Left = 200
      Top = 104
      Width = 35
      Height = 22
      AllowAllUp = True
      GroupIndex = 3
      OnClick = BtnFileClick
    end
    object BtnMEFileDevW: TSpeedButton
      Left = 200
      Top = 128
      Width = 35
      Height = 22
      AllowAllUp = True
      GroupIndex = 4
      OnClick = BtnFileClick
    end
    object Label12: TLabel
      Left = 200
      Top = 24
      Width = 26
      Height = 18
      Caption = 'ME'
    end
    object Label13: TLabel
      Left = 256
      Top = 24
      Width = 25
      Height = 18
      Caption = 'PD'
    end
    object Label14: TLabel
      Left = 312
      Top = 24
      Width = 26
      Height = 18
      Caption = 'OP'
    end
    object BtnPDFilePro: TSpeedButton
      Left = 256
      Top = 56
      Width = 35
      Height = 22
      AllowAllUp = True
      GroupIndex = 7
      OnClick = BtnFileClick
    end
    object BtnPDFileRew: TSpeedButton
      Left = 256
      Top = 80
      Width = 35
      Height = 22
      AllowAllUp = True
      GroupIndex = 8
      OnClick = BtnFileClick
    end
    object BtnPDFileDevR: TSpeedButton
      Left = 256
      Top = 104
      Width = 35
      Height = 22
      AllowAllUp = True
      GroupIndex = 9
      OnClick = BtnFileClick
    end
    object BtnPDFileDevW: TSpeedButton
      Left = 256
      Top = 128
      Width = 35
      Height = 22
      AllowAllUp = True
      GroupIndex = 10
      OnClick = BtnFileClick
    end
    object BtnOPFilePro: TSpeedButton
      Left = 312
      Top = 56
      Width = 35
      Height = 22
      AllowAllUp = True
      GroupIndex = 13
      OnClick = BtnFileClick
    end
    object BtnOPFileRew: TSpeedButton
      Left = 312
      Top = 80
      Width = 35
      Height = 22
      AllowAllUp = True
      GroupIndex = 14
      OnClick = BtnFileClick
    end
    object BtnOPFileDevR: TSpeedButton
      Left = 312
      Top = 104
      Width = 35
      Height = 22
      AllowAllUp = True
      GroupIndex = 15
      OnClick = BtnFileClick
    end
    object BtnOPFileDevW: TSpeedButton
      Left = 312
      Top = 128
      Width = 35
      Height = 22
      AllowAllUp = True
      GroupIndex = 16
      OnClick = BtnFileClick
    end
    object Label15: TLabel
      Left = 16
      Top = 152
      Width = 96
      Height = 18
      Caption = 'Local Read'
    end
    object Label16: TLabel
      Left = 16
      Top = 176
      Width = 93
      Height = 18
      Caption = 'Local Save'
    end
    object BtnMEFileLocR: TSpeedButton
      Left = 200
      Top = 152
      Width = 35
      Height = 22
      AllowAllUp = True
      GroupIndex = 5
      OnClick = BtnFileClick
    end
    object BtnMEFileLocW: TSpeedButton
      Left = 200
      Top = 176
      Width = 35
      Height = 22
      AllowAllUp = True
      GroupIndex = 6
      OnClick = BtnFileClick
    end
    object BtnPDFileLocR: TSpeedButton
      Left = 256
      Top = 152
      Width = 35
      Height = 22
      AllowAllUp = True
      GroupIndex = 11
      OnClick = BtnFileClick
    end
    object BtnPDFileLocW: TSpeedButton
      Left = 256
      Top = 176
      Width = 35
      Height = 22
      AllowAllUp = True
      GroupIndex = 12
      OnClick = BtnFileClick
    end
    object BtnOPFileLocR: TSpeedButton
      Left = 312
      Top = 152
      Width = 35
      Height = 22
      AllowAllUp = True
      GroupIndex = 17
      OnClick = BtnFileClick
    end
    object BtnOPFileLocW: TSpeedButton
      Left = 312
      Top = 176
      Width = 35
      Height = 22
      AllowAllUp = True
      GroupIndex = 18
      OnClick = BtnFileClick
    end
    object Panel2: TPanel
      Left = 160
      Top = 48
      Width = 6
      Height = 137
      BevelOuter = bvLowered
      TabOrder = 0
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 408
    Width = 398
    Height = 60
    Align = alBottom
    TabOrder = 2
    object TouchSoftOK: TTouchSoftkey
      Left = 8
      Top = 8
      Width = 185
      Height = 41
      Caption = 'TouchSoftOK'
      TabOrder = 0
      OnClick = TouchSoftOKClick
      Legend = 'OK'
      TextSize = 14
      HasIndicator = False
      IndicatorOn = False
      IsEnabled = True
      ButtonStyle = skNormal
      ArrowColor = clBlack
      ButtonColor = clWhite
      LegendColor = clBlack
      IndicatorWidth = 20
      IndicatorHeight = 10
      IndicatorOnColor = clGreen
      IndicatorOffColor = clSilver
      MaxFontSize = 0
    end
    object TouchSoftCancel: TTouchSoftkey
      Left = 208
      Top = 8
      Width = 185
      Height = 41
      Caption = 'TouchSoftCancel'
      TabOrder = 1
      OnClick = TouchSoftCancelClick
      Legend = 'Cancel'
      TextSize = 14
      HasIndicator = False
      IndicatorOn = False
      IsEnabled = True
      ButtonStyle = skNormal
      ArrowColor = clBlack
      ButtonColor = clWhite
      LegendColor = clBlack
      IndicatorWidth = 20
      IndicatorHeight = 10
      IndicatorOnColor = clGreen
      IndicatorOffColor = clSilver
      MaxFontSize = 0
    end
  end
end
