unit PartProgramEditor;
// Moded 06/Jan/2003 for Translation Texts
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ComCtrls, ExtCtrls,  Buttons, OleCtrls, OKBox, AbstractOPPPlugin,
  NamedPlugin, StreamTokenizer, IniFiles, ACNC_SHeditor, ACNC_plusmemo, Clipbrd,
  ACNC_plusGutter, ImgList, ACNC_TouchSoftkey, ACNC_BaseKeyBoards, INamedInterface,
  OPPHeader, OPPDoc, ShellApi, Math, LinkedLookUp, ACNC_TouchGlyphSoftkey, CNCTypes;

type
TLimitRecord = record
    FormValue     : String;
    MaxVal        : Double;
    MinVal        : Double;
    IsInteger     : Boolean;
    HelpString    : String;
    HTML_HelpFile : String;
    Valid         : Boolean;
    ConvertLimit  : Boolean;
    end;
THelpRecord = record
  HTMLFile : String;
  Hint     : String;
  end;
pLimitRecord = ^TLimitRecord;

TFindModes = (fmNone,fmFind,fmReplace);

TOppEditMode = (oeGCodeEdit,oeHeaderEdit);


  TGCodeForm = class(TForm)
    ControlPanel: TPanel;
    FileOPen: TOpenDialog;
    FindPanel: TPanel;
    SavePanel: TPanel;
    FindModePanel: TPanel;
    UndoControls: TPanel;
    StartTimer: TTimer;
    OPPHeaderPanel: TPanel;
    pp1: TOPPSHEdit;
    OkButton: TTouchSoftkey;
    CancelButton: TTouchSoftkey;
    SearchPages: TPageControl;
    FindSheet: TTabSheet;
    UndoPanel: TPanel;
    UndoKey: TSpeedButton;
    RedoKey: TSpeedButton;
    topBottomPanel: TPanel;
    TopKey: TKBKey;
    BottomKey: TKBKey;
    BookMarkKey: TTouchSoftkey;
    FindTextEdit: TSHCursorEdit;
    ReplaceSheet: TTabSheet;
    ReplaceTextEdit: TSHCursorEdit;
    FindandReplaceButton: TSpeedButton;
    FindButton: TSpeedButton;
    SoptionsPanel: TPanel;
    FindWordButton: TSpeedButton;
    MatchCaseButton: TSpeedButton;
    FWdButton: TSpeedButton;
    BackwardButton: TSpeedButton;
    FindAgainButton: TSpeedButton;
    FindDoneKey: TTouchSoftkey;
    logoSheet: TTabSheet;
    PageControl1: TPageControl;
    TabOPPHeader: TTabSheet;
    TabAECParams: TTabSheet;
    OPPPartPanel: TPanel;
    PartTypeLabel: TLabel;
    ReferenceLabel: TLabel;
    ToolTYpeLabel: TLabel;
    ElectrodeCountLabel: TLabel;
    ToolUsageLabel: TLabel;
    CycleTimeLabel: TLabel;
    Label3: TLabel;
    HeaderOKButton: TSpeedButton;
    HeaderCancelButton: TSpeedButton;
    ElectrodeMapLabel: TLabel;
    Label6: TLabel;
    HexFault: TLabel;
    ToolFault: TLabel;
    OPNumberLabel: TLabel;
    OpNumberFaultLabel: TLabel;
    ECountFaultLabel: TLabel;
    PartTypeEdit: TEdit;
    ReferenceEdit: TEdit;
    ToolTypeEdit: TEdit;
    ElectrodeCountEdit: TEdit;
    ToolUsageEdit: TEdit;
    CycleTimeEdit: TEdit;
    ElectrodeMapEdit: TEdit;
    ToolOffsetPanel: TPanel;
    ToolXOffsetLabel: TLabel;
    ToolYOffsetLabel: TLabel;
    ToolZOffsetLabel: TLabel;
    ToolXOffsetEdit: TEdit;
    ToolYOffsetEdit: TEdit;
    ToolZOffsetEdit: TEdit;
    OpNumberEdit: TEdit;
    AECBackPanel: TPanel;
    AECHelpPanel: TPanel;
    AECGeneralPanel: TPanel;
    OPPHeaderTitle: TPanel;
    Label7: TLabel;
    Label8: TLabel;
    AECPanel: TPanel;
    AECCheck: TCheckBox;
    HeaderAECEdit: TSpeedButton;
    AECButtonPanel: TPanel;
    AECBackButton: TSpeedButton;
    AECDataError: TPanel;
    PanelKB: TPanel;
    MainKb: TBaseKB;
    KeyPad: TBaseFPad;
    BitBtn1: TBitBtn;
    Panel7: TPanel;
    BtnHideKB: TTouchGlyphSoftkey;
    BtnHelp: TTouchGlyphSoftkey;
    AECDefaultButton: TSpeedButton;
    AECLeftpanel: TPanel;
    AECGenPanTitle: TPanel;
    AECInchMetricButton: TTouchSoftkey;
    AECDataInchMetricDisplay: TEdit;
    AECLabelHoleProgram: TLabel;
    AECDataCheckHoleProgram: TEdit;
    AECLabelUFeed: TLabel;
    AECDataStowFeed: TEdit;
    AECLabelRestrokeDist: TLabel;
    AECDataRestroke: TEdit;
    AECDoubleIndexButton: TTouchSoftkey;
    AECDoubleIndexDisplay: TEdit;
    AECMiddlePanel: TPanel;
    Panel4: TPanel;
    AECLoadTitle: TPanel;
    AECLabelLoadPulseCount: TLabel;
    AECDataLoadPulseCount: TEdit;
    AECLabelULoadPos: TLabel;
    AECDataLoadPositionU: TEdit;
    AECLabelFirstQualifyPulseLength: TLabel;
    AECDataPulse1Duration: TEdit;
    AECLabelSecondQualifyPulseDuration: TLabel;
    AECDataQualifyPulse2Duration: TEdit;
    AECRightPanel: TPanel;
    AECLabelEjectPulseCount: TLabel;
    AECLabelFirstEjectPulse: TLabel;
    AECLabelSecondEjectPulse: TLabel;
    AECLabelEjectLength: TLabel;
    AECEjectTitle: TPanel;
    AECDataEjectPulseCount: TEdit;
    AECDataEjectPulse1Duration: TEdit;
    AECDataEjectPulse2Duration: TEdit;
    AECDataEjectLength: TEdit;
    Panel5: TPanel;
    AECLabelQualifyRetryCount: TLabel;
    AECLabelQualifyStroke: TLabel;
    AECLabelElectrodeClearance: TLabel;
    AECQualifyTitle: TPanel;
    AECDataQualifyRetryLoops: TEdit;
    AECDataQualifyStroke: TEdit;
    AECDataElectrodeClearance: TEdit;
    Timer1: TTimer;
    procedure FormCreate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FindButtonClick(Sender: TObject);
    procedure FindTextEditKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FindAgainButtonClick(Sender: TObject);
    procedure OKButtonClick(Sender: TObject);
    procedure CancelButtonClick(Sender: TObject);
    procedure FindWordButtonClick(Sender: TObject);
    procedure MatchCaseButtonClick(Sender: TObject);
    procedure TopKeyMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure BottomKeyClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure StartTimerTimer(Sender: TObject);
    procedure UndoKeyClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure PartTypeEditEnter(Sender: TObject);
    procedure HeaderCancelButtonClick(Sender: TObject);
    procedure ElectrodeMapEditEnter(Sender: TObject);
    procedure FWdButtonClick(Sender: TObject);
    procedure BackwardButtonClick(Sender: TObject);
    procedure RedoKeyClick(Sender: TObject);
    procedure BookMarkKeyMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FindDoneKeyMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure ElectrodeCountEditChange(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure pp1FindModeRequested(Sender: TObject);
    procedure AECCheckClick(Sender: TObject);
    procedure HeaderAECEditClick(Sender: TObject);
    procedure AEC_FieldEnter(Sender: TObject);
    procedure AECBackButtonClick(Sender: TObject);
    procedure AEC_FieldExit(Sender: TObject);
    procedure BtnDefaultClick(Sender: TObject);
    procedure AECDoubleIndexButtonMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure AECCheckEnter(Sender: TObject);
    procedure AECInchMetricButtonMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure pp1FindAgainRequested(Sender: TObject);
    procedure BtnHideKBMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure pp1F1HelpRequested(Sender: TObject);
    procedure BtnHelpMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure AECDefaultButtonClick(Sender: TObject);
    procedure AECDataKeyDown(Sender: TObject;
      var Key: Word; Shift: TShiftState);
    procedure Timer1Timer(Sender: TObject);
  private
    { Private declarations }
    HelpCommandTag : TTagref;
    HelpLaunchTag  : TTagref;
//    ShoeKBDefault := Boolean;

    HelpBase : String;
    ContextHelpString : String;
    ContextHelpDestination : String;
    ShowKB : Boolean;
    HelpLUp : TLinkedLookUp;
    AEC_OPtionTag : TTagref;
    InchEntry : Boolean;
    LineNumber : Integer;
    SearchOptions     : TSearchSwitches;
    MapList           : TStringList;
    FieldDataList : TList;

    ToolTypeIsNumeric   : Boolean;
    AlphaToolTypeLength : Integer;
    MaxNumericToolCode  : Integer;
    ShowToolOffsets     : Boolean;
    FindMode            : TFindModes;

    MinOpNumber : Integer;
    MaxOpNumber : Integer;
    iniWordwrap : Boolean;

    EnableAECEditor               : Boolean;

    Translator : TTranslator;
    // Translation Strings
    TranslatedPartType        :String;
    TranslatedReference       : String;
    TranslatedToolTYpe        : String;
    TranslatedElectrodeCount  : String;
    TranslatedElectrodeMap    : String;
    TranslatedToolXOffset     : String;
    TranslatedToolYOffset     : String;
    TranslatedToolZOffset     : String;
    TranslatedOPNumber        : String;
    TranslatedToolUsage       : String;
    TranslatedCycleTime       : String;
    TranslatedOK              : String;
    TranslatedCancel          : String;
    TranslatedHideBookMark    : String;
    TranslatedBookMarkControl : String;
    TranslatedFind            : String;
    TranslatedFindAgain       : String;
    TranslatedWholeWord       : String;
    TranslatedMatchCase       : String;
    TranslatedForwards        : String;
    TranslatedBackwards       : String;
    TranslatedFindAndReplace  : String;
    TranslatedTop             : String;
    TranslatedBottom          : String;
    TranslatedUndo            : String;
    TranslatedRedo            : String;
    TranslatedDone            : String;
    TranslatedFindWord        : String;
    TranslatedReplaceAgain    : String;
    TranslatedDataError       : String;
    TranslatedMaxValue        : String;
    TranslatedMaxof           : String;
    TranslatedMust_Be_0_or    : String;
    TranslatedTrue            : String;
    TranslatedFalse           : String;
    TranslatedOPPHeader       : String;
    TranslatedHeaderData      : String;
    TranslatedInch            : String;
    TranslatedMetric          : String;
    TranslatedShowKB          : String;
    TranslatedHideKB          : String;
    DisplayTop                : Integer;
    DisplayHeight             : Integer;

    AxisLabels: array [0..12] of TLabel;
    CopyAxisButton: TButton;


    procedure CopyAxisPosition(Sender: TObject);
    procedure FocusOnEditor;
    procedure KnownWordChange(Sender : TObject);
    procedure ContentsChanged(Sender : TObject);
    procedure ReadINiFile;
    procedure ReadConfigFile;
    procedure ShowFindPage(fMode : TFindModes);
    function AllAECFieldsValid: Boolean;
    procedure LaunchHelp;
    function BuildHTMLwordlist(Basepath : String): Boolean;
    procedure Win32MakeDirectoryList(const DirName: String;
                                           DList : TStringList;
                                           MaxCount : Integer;
                                           FilterExtension : String);

  public
    EditMode : TOppEditMode;
    //GCode stuff
    FileString : string;
    //OPP Header Stuff
    OPPData: TOPPFile;
    ContextHelpEnabled : Boolean;
    InchModeTag : ttagref;    //Added
    MetricToInch: Double;
    procedure DataToForm;
    function FormToData: Boolean;
    procedure AECSetDefault;

  end;

 TGCodeEditor = class(TAbstractOPPPluginEditor)
    class function Execute(var Section : string) : Boolean; override;
  end;

 TOPPHeaderEditor = class(TAbstractOPPPluginEditor)
  public
    class function Execute(var aSection: string): Boolean; override;
  end;

 TActiveOppServer = class(TAbstractOPPPluginConsumer)
  private
    PartTypeTag : TTagRef;
    CommentTag: TTagRef;
    ToolTypeStringTag : TTagRef;
    ElectrodeMapTag : TTagRef;
    ElectrodeCountTag: TTagRef;
    ToolUsageTag: TTagRef;  {mm}
    CycleTimeTag: TTagRef; {sec}
    ToolXOffsetTag : TTagRef;
    ToolYOffsetTag : TTagRef;
    ToolZOffsetTag : TTagRef;
    OpNumberTag : TTagRef;
    //AECTags
    AEC_SelectedTag : TTagRef;
    AEC_RestrokeTag : TTagRef;
    AEC_StowFeedTag : TTagRef;
    AEC_CheckHoleProgramTag : TTagRef;
    AEC_QualifyStrokeTag : TTagRef;
    AEC_QualifyRetryLoopsTag : TTagRef;
    AEC_ElectrodeClearanceTag : TTagRef;
    AEC_LoadPositionUTag : TTagRef;
    AEC_EjectLengthTag : TTagRef;
    AEC_DoubleIndexTag : TTagRef;
    AEC_FirstPulseDurationTag : TTagRef;
    AEC_SecondPulseDurationTag : TTagRef;
    AEC_EjectPulseCountTag : TTagRef;
    AEC_EjectPulse1DurationTag : TTagRef;
    AEC_EjectPulse2DurationTag : TTagRef;
    AEC_LoadPulseCountTag : TTagRef;


  public
    FActiveOppFile : TOppFile;
    constructor Create(aSectionName : string); override;
    procedure Consume(aSectionName, Buffer : PChar); override;
    procedure Install; override;
  end;

var
  GCodeForm: TGCodeForm;

function CSVField(FNumber: Integer;TextString : String):String;
function GetToken(var Buffer : String):String;
function SlashSep(const Path, S: String): String;


implementation

{$R *.DFM}
// Data flow is from the part program data in the part program to the
// header of the program.
procedure TGCodeForm.DataToForm;
var
edtToolUsage: Double;
edtToolXOffset: Double;
edtToolYOffset: Double;
edtToolZOffset: double;

begin

  with OPPData do begin
    PartTypeEdit.Text:= PartType;
    ReferenceEdit.Text:= Comment;
    try
    ToolTypeEdit.Text:= ToolTypeString;
    except
    ToolTypeEdit.Text:= 'DataError';
    end;

    try
      OpNumberEdit.Text := IntToStr(OpNumber);
    except
      OpNumberEdit.Text := '1';
    end;

    try
    ElectrodeMapEdit.Text:= ElectrodeMap;
    except
    ElectrodeMapEdit.Text:= TranslatedDataError;
    end;

    try
    ElectrodeCountEdit.Text:= IntToStr(ElectrodeCount);
    except
    ElectrodeCountEdit.Text:= TranslatedDataError;
    end;

    try
    edtToolUsage := ToolUsage / MetricToInch;
    ToolUsageEdit.Text:= Format('%6.3f', [edtToolUsage]);
    //ToolUsageEdit.Text:= Format('%6.3f', [ToolUsage]);
    except
    ToolUsageEdit.Text:= TranslatedDataError;
    end;

    try
    CycleTimeEdit.Text:= IntToStr(CycleTime);
    except
    CycleTimeEdit.Text:= TranslatedDataError;
    end;
    edtToolXOffset := ToolXOffset /MetricToInch;
    edtToolYOffset := ToolYOffset / MetricToInch;
    edtToolZOffset := ToolZOffset / MetricToInch;
    ToolXOffsetEdit.Text := Format('%.4f', [edtToolXOffset]);
    ToolYOffsetEdit.Text := Format('%.4f', [edtToolYOffset]);
    ToolZOffsetEdit.Text := Format('%.4f', [edtToolZOffset]);

    // AEC Data to Form
    if enableAECEditor then
      begin
      if AEC_Selected <> 0 then
        begin
        AECCheck.Checked := True;
        HeaderAECEdit.Enabled := True;
        end
      else
        begin
        AECCheck.Checked := False;
        HeaderAECEdit.Enabled := False;
        end;

      if   AEC_InchEntry = 0 then
        begin
        AECDataInchMetricDisplay.Text := 'Metric';
        InchEntry := False;
        end
      else
        begin
        AECDataInchMetricDisplay.Text := 'Inch';
        InchEntry := True;
        end;


      AECDataCheckHoleProgram.Text := Format(' %d',[AEC_CheckHoleProgram]);
      if InchEntry then
        AECDataStowFeed.Text := Format('%5.2f',[AEC_StowFeed])
      else
        AECDataStowFeed.Text := Format('%7.2f',[AEC_StowFeed]);

      if AEC_DoubleIndex <> 0 then
        AECDoubleIndexDisplay.Text := 'True'
      else
        AECDoubleIndexDisplay.Text := 'False';



      If InchEntry then
        AECDataRestroke.Text := Format('%5.3f',[AEC_Restroke])
      else
        AECDataRestroke.Text := Format('%7.3f',[AEC_Restroke]);

      AECDataEjectPulse1Duration.Text := Format('%4.2f',[AEC_EjectPulse1Duration]);
      AECDataEjectPulse2Duration.Text := Format('%4.2f',[AEC_EjectPulse2Duration]);
      AECDataEjectPulseCount.Text := IntToStr(AEC_EjectPulseCount);

      if Inchentry then
        AECDataEjectLength.Text := Format('%5.2f',[AEC_EjectLength])
      else
        AECDataEjectLength.Text := Format('%7.3f',[AEC_EjectLength]);

      AECDataLoadPulseCount.Text := Format(' %d',[AEC_LoadPulseCount]);
      if InchEntry then
        AECDataQualifyStroke.Text := Format('%7.3f',[AEC_QualifyStroke])
      else
        AECDataQualifyStroke.Text := Format('%5.3f',[AEC_QualifyStroke]);

      if InchEntry then
        AECDataLoadPositionU.Text := Format('%7.3f',[AEC_LoadPositionU])
      else
        AECDataLoadPositionU.Text := Format('%5.3f',[AEC_LoadPositionU]);

      AECDataPulse1Duration.Text := Format('%4.2f',[AEC_FirstPulseDuration]);
      AECDataQualifyPulse2Duration.Text := Format('%4.2f',[AEC_SecondPulseDuration]);
      AECDataQualifyRetryLoops.Text := Format(' %d',[AEC_QualifyRetryLoops]);
      if InchEntry then
        AECDataElectrodeClearance.Text :=Format('%5.3f',[AEC_ElectrodeClearance])
      else
        AECDataElectrodeClearance.Text :=Format('%7.3f',[AEC_ElectrodeClearance]);
      end;

  end
end;

procedure TGCodeForm.AECSetDefault;
begin
  with OPPData do begin
     AEC_InchEntry := 0;
     AECDataInchMetricDisplay.Text := 'Metric';
     AEC_CheckHoleProgram := 99;
     AEC_StowFeed := 2000;
     AEC_Restroke := 30;
     AEC_DoubleIndex := 0;
     AECDoubleIndexDisplay.Text := 'False';
     AEC_EjectPulseCount := 1;
     AEC_EjectPulse1Duration := 1;
     AEC_EjectPulse2Duration := 1;
     AEC_EjectLength := 10;
     AEC_LoadPulseCount := 1;
     AEC_QualifyStroke := 25;
     AEC_LoadPositionU := 0;
     AEC_FirstPulseDuration := 1;
     AEC_SecondPulseDuration := 1;
     AEC_QualifyRetryLoops := 3;
     AEC_ElectrodeClearance := 2;

      AECDataRestroke.Text := Format('%7.3f',[AEC_Restroke]);
      AECDataEjectPulse1Duration.Text := Format('%4.2f',[AEC_EjectPulse1Duration]);
      AECDataEjectPulse2Duration.Text := Format('%4.2f',[AEC_EjectPulse2Duration]);
      AECDataEjectPulseCount.Text := IntToStr(AEC_EjectPulseCount);
      AECDataEjectLength.Text := Format('%7.3f',[AEC_EjectLength]);
      AECDataLoadPulseCount.Text := Format(' %d',[AEC_LoadPulseCount]);
      AECDataQualifyStroke.Text := Format('%5.3f',[AEC_QualifyStroke]);
      AECDataLoadPositionU.Text := Format('%5.3f',[AEC_LoadPositionU]);
      AECDataPulse1Duration.Text := Format('%4.2f',[AEC_FirstPulseDuration]);
      AECDataQualifyPulse2Duration.Text := Format('%4.2f',[AEC_SecondPulseDuration]);
      AECDataQualifyRetryLoops.Text := Format(' %d',[AEC_QualifyRetryLoops]);
      AECDataElectrodeClearance.Text :=Format('%7.3f',[AEC_ElectrodeClearance]);
      AECDataStowFeed.Text := Format('%7.2f',[AEC_StowFeed]);
      end;

end;

// Data flow is from the form to the part program data in the header and is stored
// in the part program.
function TGCodeForm.FormToData : Boolean;
var
Code : Integer;
Buffer : String;
edtToolUsage : Double;
edtToolXOffset: Double;
edtToolYOffset: Double;
edtToolZOffset: double;
begin
Result := True;
  with OPPData do begin
    PartType:= PartTypeEdit.Text;
    Comment:= ReferenceEdit.Text;
    try
      ToolTypeString:= ToolTypeEdit.Text;
    except
      Result := False;
    end;

    try
      OpNumber := StrToInt(OpNumberEdit.Text);
    except
      Result := False;
    end;

    try
      ElectrodeCount:= StrToInt(ElectrodeCountEdit.Text);
    except
      Result := False;
    end;

    try
      ElectrodeMap:= ElectrodeMapEdit.Text;
    except
      Result := False;
    end;

    try
//      ToolUsage:= MetricToInch * StrToFloat(ToolUsageEdit.Text);
      EdtToolUsage:= StrToFloat(ToolUsageEdit.Text);
      ToolUsage:= EdtToolUsage * MetricToInch;
    except
      Result := False;
    end;

    try
    CycleTime:=  StrToInt(CycleTimeEdit.Text);
    except
    Result := False;
    end;
    Val(ToolXOffsetEdit.Text, edtToolXOffset, Code);
    Val(ToolYOffsetEdit.Text, edtToolYOffset, Code);
    Val(ToolZOffsetEdit.Text, edtToolZOffset, Code);
    ToolXOffset := edtToolXOffset * MetricToInch;
    ToolYOffset := edtToolYOffset * MetricToInch;
    ToolZOffset := edtToolZOffset * MetricToInch;
    //AEC Data

    if EnableAECEditor then
      begin
      if AECCheck.Checked then
        begin
        HeaderAECEdit.Enabled := True;
        AEC_Selected := 1;
        end
      else
        begin
        HeaderAECEdit.Enabled := False;
        AEC_Selected := 0;
        end;
      try

      if Length(AECDataInchMetricDisplay.Text) > 0 then
        begin
        if (AECDataInchMetricDisplay.Text[1] = 'M') or (AECDataInchMetricDisplay.Text[1]='m') then
          begin
          AEC_InchEntry := 0;
          end
        else
          begin
          AEC_InchEntry := 1;
          end;
        end;
      AEC_CheckHoleProgram := StrToInt(AECDataCheckHoleProgram.Text) ;
      AEC_StowFeed := StrToFloat(AECDataStowFeed.Text);
      AEC_Restroke := StrToFloat(AECDataRestroke.Text);
      Buffer :=trim(AECDoubleIndexDisplay.Text);
      if (buffer[1]='T') or (buffer[1] = 't') then
         begin
         AEC_DoubleIndex := 1;
         end
      else
        begin
         AEC_DoubleIndex := 0;
        end;
     AEC_EjectPulseCount := StrToInt(AECDataEjectPulseCount.Text);
     AEC_EjectPulse1Duration := StrToFloat(AECDataEjectPulse1Duration.Text);
     AEC_EjectPulse2Duration := StrToFloat(AECDataEjectPulse2Duration.Text);
     AEC_EjectLength := StrToFloat(AECDataEjectLength.Text);
     AEC_LoadPulseCount := StrToInt(AECDataLoadPulseCount.Text);
     AEC_QualifyStroke := StrToFloat(AECDataQualifyStroke.Text);
     AEC_LoadPositionU := StrToFloat(AECDataLoadPositionU.Text);
     AEC_FirstPulseDuration := StrToFloat(AECDataPulse1Duration.Text);
     AEC_SecondPulseDuration := StrToFloat(AECDataQualifyPulse2Duration.Text);
     AEC_QualifyRetryLoops := StrToInt(AECDataQualifyRetryLoops.Text);
     AEC_ElectrodeClearance := StrToFloat(AECDataElectrodeClearance.Text);







      except
      Result := False;
      end;
      end;




      end;
end;


class function TOPPHeaderEditor.Execute(var aSection : string): Boolean;
var
//  Form: TOPPHeaderForm;
  Form : TGCodeForm;
  S : TStreamQuoteTokenizer;
  SList : TStringList;
  tempint : Integer;

begin
  Form:= TGCodeForm.Create(Application);
  Form.AEC_OPtionTag :=Named.AquireTag('_OptionAECTooling','TIntegerTAG',Nil);
  TempInt := Named.GetAsInteger(Form.AEC_OPtionTag);

  if TempInt> 0 then
    begin
    Form.EnableAECEditor := True;
    end
  else
     begin
     Form.EnableAECEditor := False;
     end;

  S:= TStreamQuoteTokenizer.Create;
  SList := TStringList.Create;
  try
    S.SetStream(TStringStream.Create(aSection));
    S.Seperator := '=';
    S.QuoteChar := '''';
    S.Rewind;
    try
      Form.OPPData.Load(S);
    except
      on E : Exception do
        Application.ShowException(E);
    end;
    Form.DataToForm;
    Form.EditMode := oeHeaderEdit;
    Result:= Form.ShowModal = mrOK;
    if Result then begin
      if Form.FormToData then
         begin
         Form.OPPData.Save(SList);
         aSection := SList.Text;
         end
      else
          begin
          raise Exception.Create('Bad OPP Header Data. Changes NOT saved');
          end;
    end;
  finally
    S.Free;
    SList.Free;
    Form.Free
  end
end;

class function TGCodeEditor.Execute(var Section : string) : Boolean;
var
  Form : TGCodeForm;
  TempStream : TMemoryStream;
  TempStrings : TStringList;
begin
  Form := TGCodeForm.Create(Application);

  Form.FileString := Section;         
  Form.EditMode := oeGCodeEdit;
  Form.pp1.EditPage.WordWrap := Form.InIWordwrap;
  Form.pp1.EditPage.Cursor := crIBeam;
  Form.pp1.IndicateHelpAvailable := False;


  Result := Form.ShowModal = mrOK;
  if (Result) Then
     begin
     TempStream := TMemoryStream.Create;
     TempStrings := TStringList.Create;
     try
     TempStrings.Clear;
     Form.PP1.EditPage.Lines.SaveToStream(TempStream);
     TempStream.Position := 0;
     TempStrings.LoadFromStream(TempStream);
     Section := TempStrings.Text;
     finally
     TempStream.Free;
     TempStrings.Free;
     end;
     end;
  Form.Free;
end;



procedure TGCodeForm.FormCreate(Sender: TObject);
var I: Integer;
begin
  LineNumber := 0;
  InchEntry := False;
  AECDataError.Visible := False;
  InchModeTag := NAmed.AquireTag('NC1InchModeDefault', PChar('TMethodTag'), nil);
   if Named.GetAsBoolean(InchModeTag) then
      MetricToInch := 25.4
    else
      MetricToInch := 1;
  with pp1 do
    begin
    CurrentWordChange := KnownWordChange;
    EditPage.OnChange := ContentsChanged;
    end;
    ReadConfigFile;
    Translator := TTranslator.Create;
  try
  with Translator do
    begin
    TranslatedPartType := GetString('$PARTTYPE');
    TranslatedReference:= GetString('$REFERENCE');
    TranslatedToolTYpe := GetString('$TOOL_TYPE');
    TranslatedElectrodeCount  := GetString('$ELECTRODE_COUNT');
    TranslatedElectrodeMap := GetString('$ELECTRODE_MAP');
    TranslatedToolXOffset := GetString('$TOOL_X_OFFSET');
    TranslatedToolYOffset := GetString('$TOOL_Y_OFFSET');
    TranslatedToolZOffset := GetString('$TOOL_Z_OFFSET');
    TranslatedOPNumber    := GetString('$OP_NUMBER');
    TranslatedToolUsage   := GetString('$TOOL_USAGE');
    TranslatedCycleTime   := GetString('$CYCLE_TIME');
    TranslatedOK          := GetString('$OK');
    TranslatedCancel      := GetString('$CANCEL');
    TranslatedHideBookMark:= GetString('$HIDE_BOOKMARK');
    TranslatedBookMarkControl := GetString('$BOOKMARK_CONTROL');
    TranslatedFind            := GetString('$FIND');
    TranslatedFindAndReplace  := GetString('$FIND_AND_REPLACE');
    TranslatedTop             := GetString('$TOP');
    TranslatedBottom          := GetString('$BOTTOM');
    TranslatedUndo            := GetString('$UNDO');
    TranslatedRedo            := GetString('$REDO');
    TranslatedFindAgain       := GetString('$FIND_AGAIN');
    TranslatedWholeWord       := GetString('$WHOLE_WORD');
    TranslatedMatchCase       := GetString('$MATCH_CASE');
    TranslatedForwards        := GetString('$FORWARDS');
    TranslatedBackwards       := GetString('$BACKWARDS');
    TranslatedDone            := GetString('$DONE');
    TranslatedFindWord        := GetString('$FIND_WORD');
    TranslatedReplaceAgain    := GetString('$REPLACE_AGAIN');
    BookMarkKey.Legend        := GetString('$BOOKMARK_CONTROL');
    CancelButton.Legend       := GetString('$CANCEL');
    FindDoneKey.Legend        := gETsTRING('$DONE');
    TranslatedDataError       := GetString('$DATA_ERROR');
    TranslatedMaxValue        := GetString('$MAX_VALUE');
    TranslatedMaxof           := GetString('$MAX_OF_%D_CHAR');
    TranslatedMust_Be_0_or    := GetString('$MUST_BE_0_OR');
    TranslatedTrue            := GetString('$TRUE');
    TranslatedFalse           := GetString('$FALSE');
    TranslatedOPPHeader       := GetString('$OPP_HEADER');
    TranslatedHeaderData      := GetString('$OPP_HEADER_DATA');
    TranslatedInch            := GetString('$INCH');
    TranslatedMetric          := GetString('$METRIC');
    TranslatedShowKB          := GetString('$SHOW_KB');
    TranslatedHideKB          := GetString('$HIDE_KB');

    AECLabelEjectPulseCount.Caption := GetString('$ELECT_PULSE_COUNT');
    AECLabelFirstEjectPulse.Caption := GetString('$FIRST_PULSE_DURATION');
    AECLabelSecondEjectPulse.Caption:= GetString('$SECOND_PULSE_DURATION');
    AECLabelEjectLength.Caption := GetString('$EJECT_LENGTH');
    AECLabelLoadPulseCount.Caption := GetString('$LOAD_PULSE_COUNT');
    AECLabelULoadPos.Caption := GetString('$U_AXIS_POS');
    AECLabelQualifyRetryCount.Caption := GetString('$RETRY_COUNT');
    AECLabelQualifyStroke.Caption := GetString('$QUALIFY_STROKE');
    AECLabelElectrodeClearance.Caption := GetString('$ELECTRODE_CLEARANCE');
    AECLabelHoleProgram.Caption := GetString('$HOLE_PROGRAM');
    AECLabelUFeed.Caption  := GetString('$AXIS_FEEDRATE');
    AECLabelRestrokeDist.Caption := GetString('$RESTROKE_DISTANCE');
    AECLabelFirstQualifyPulseLength.Caption := GetString('$FIRST_PULSE_DURATION');
    AECLabelSecondQualifyPulseDuration.Caption := GetString('$SECOND_PULSE_DURATION');
    AECInchMetricButton.Legend := GetString('$INCH_METRIC');
    AECDoubleIndexButton.Legend := GetString('$DOUBLE_INDEX');
    AECBackButton.caption := GetString('$OK');
    AECDefaultButton.Caption := GetString('$DEFAULT');
    AECGenPanTitle.Caption := GetString('$GENERAL');
    AECEjectTitle.Caption :=  GetString('$EJECT');
    AECLoadTitle.Caption :=  GetString('$LOAD');
    AECQualifyTitle.Caption :=  GetString('$QUALIFY');
    AECCheck.Caption  :=  GetString('$AEC_ENABLE');
    HeaderAECEdit.Caption :=  GetString('$EDIT_AEC');

    PP1.BM1Legend := GetString('$SET_1');;
    PP1.BM2Legend := GetString('$SET_2');
    PP1.GBM1Legend := GetString('$GOTO_1');
    PP1.GBM2Legend := GetString('$GOTO_2');

    end;
  finally
  Translator.Free;
  end;

  PartTypeLabel.Caption  := TranslatedPartType;
  ReferenceLabel.Caption := TranslatedReference;
  ToolTypeLabel.Caption  := TranslatedToolTYpe;
  ElectrodeCountLabel.Caption := TranslatedElectrodeCount;
  ElectrodeMapLabel.Caption := TranslatedElectrodeMap;
  ToolXOffsetLabel.Caption :=  TranslatedToolXOffset;
  ToolYOffsetLabel.Caption :=  TranslatedToolYOffset;
  ToolZOffsetLabel.Caption :=  TranslatedToolZOffset;
  OPNumberLabel.Caption :=  TranslatedOPNumber;
  ToolUsageLabel.Caption :=  TranslatedToolUsage;
  CycleTimeLabel.Caption :=  TranslatedCycleTime;
  HeaderOKButton.Caption := TranslatedOK;
  HeaderCancelButton.Caption := TranslatedCancel;
  TopKey.MainCaption := TranslatedTop;
  BottomKey.MainCaption := TranslatedBottom;
  FindButton.Caption := TranslatedFind;
  FindandReplaceButton.Caption := TranslatedFindAndReplace;
  UndoKey.Caption := TranslatedUndo;
  RedoKey.Caption := TranslatedRedo;
  OkButton.Caption := TranslatedOK;
  FindWordButton.Caption := TranslatedFindWord;
  MatchCaseButton.Caption := TranslatedMatchCase;
  FWdButton.Caption := TranslatedForwards;
  BackwardButton.Caption := TranslatedBackwards;
  FindAgainButton.Caption := TranslatedFindAgain;
  FindDoneKey.Caption := TranslatedDone;

  OPPData:= TOPPFile.Create;
  MapList := TStringList.Create;
  ReadINiFile;
  Height := DisplayHeight;
  SearchOptions := [soFwd];
  ContentsChanged(Self);
  pp1.EditPage.ForceUpdate;
  pp1.UpdateStatus(Self);

  for I:= 0 to NamedPlugin.NC.AxisCount - 1 do begin
    AxisLabels[I]:= TLabel.Create(Self);
    AxisLabels[I].Parent:= LogoSheet;
    AxisLabels[I].Left := 8;
    AxisLabels[I].Top:= 8 + (24 * I);
    AxisLabels[I].Font.Name:= 'Courier New';
    AxisLabels[I].Font.Style:= [fsBold];
    AxisLabels[I].Font.Size:= 16;
  end;

  CopyAxisButton:= TButton.Create(Self);
  CopyAxisButton.Parent:= LogoSheet;
  CopyAxisButton.Caption:= 'Copy';
  CopyAxisButton.Left:= 8;
  CopyAxisButton.Top:= 12 + (24 * NC.AxisCount);
  CopyAxisButton.OnClick:= CopyAxisPosition;


end;

procedure TGCodeForm.CopyAxisPosition(Sender: TObject);
var I: Integer;
    Build: string;
begin
  Build:= '';
  for I:= 0 to NC.AxisCount - 1 do begin
    Build:= Build + NC.AxisName[I] + Format('%.3f', [NC.AxisPosition[I][ptDisplay]]) + ' ';
  end;
  Build:= Build + CarRet;
  ClipBoard.AsText:= Build;
  pp1.EditPage.PasteFromClipboard;
end;

procedure TGCodeForm.Timer1Timer(Sender: TObject);
var I: Integer;
begin
  for I:= 0 to NamedPlugin.NC.AxisCount - 1 do begin
    AxisLabels[I].Caption:= NC.AxisName[I] + Format('%.3f', [NC.AxisPosition[I][ptDisplay]]);
    AxisLabels[I].Parent:= LogoSheet;
  end;
end;


{
IniFile Format
Path is 'ProfilePath'/OPPEdit.ini
Example file is as follows
[GENERAL]
ToolTypeIsNumeric = False
ToolTypeLength=8
MaxNumericToolCode=9999
ShowToolOffsets=True
}
procedure TGCodeForm.ReadINiFile;
var
  IniFile : TInifile;
  Buffer  : String;
  BasePath : String;
  AECFields : Integer;
  FDLEntry : pLimitRecord;
  EntryNumber : Integer;
  FieldKey : String;
  FieldBuffer : String;
begin
  BasePath := DLLProfilePath+'OPPEdit.ini';
  IniFile := TIniFile.Create(BasePath);
  try
    with IniFile do begin

      Buffer := ReadString ('GENERAL','ShowKeyboardDefault','False');
      ShowKB := (Buffer[1]= 't') or (Buffer[1]= 'T');
      if ShowKB then
        begin
        BtnHideKB.Legend := TranslatedHideKB;
        end
      else
        begin
        BtnHideKB.Legend := TranslatedShowKB;
        end;
      DisplayTop := ReadInteger('GENERAL','DisplayTop',80);
      DisplayHeight := ReadInteger('GENERAL','DisplayHeight',600);
      Buffer := ReadString ('GENERAL','ToolTypeIsNumeric','False');
      ToolTypeIsNumeric := (Buffer[1]= 't') or (Buffer[1]= 'T');
      AlphaToolTypeLength := ReadInteger('GENERAL','ToolTypeLength',8);
      MaxNumericToolCode :=  ReadInteger('GENERAL','NumericToolTypeMax',9999);
      ShowToolOffsets := ReadBool ('GENERAL','ShowToolOffsets', True);
      MaxOpNumber := ReadInteger('GENERAL', 'MaxOpNumber', 32);
      MinOpNumber := ReadInteger('GENERAL', 'MinOpNumber', 1);

      AECFields := ReadInteger('AEC','FieldCount',0);
      FieldDataList := TList.Create;
      if AECFields > 0 then
        begin
        Translator := TTranslator.Create;
        try
        For EntryNumber := 0 to AECFields-1 do
          begin
          FieldKey := Format('Field%d',[EntryNumber]);
          FieldBuffer := ReadString('AEC',FieldKey,'');
          New(FDLEntry);
          try
          FDLEntry.Valid := True;
          FDLEntry.MinVal := StrToFloat(GetToken(FieldBuffer));
          FDLEntry.MaxVal := StrToFloat(GetToken(FieldBuffer));
          Buffer := GetToken(FieldBuffer);
          if Length(Buffer) > 0 then
            begin
            if Buffer[1] = '1' then
              begin
              FDLEntry.IsInteger := True;
              end
            else
              begin
              FDLEntry.IsInteger := False;
              end;
            end;

          Buffer := GetToken(FieldBuffer);
          if Length(Buffer) > 0 then
            begin
            if Buffer[1] = '1' then
              begin
              FDLEntry.ConvertLimit := True;
              end
            else
              begin
              FDLEntry.ConvertLimit := False;
              end;
            end;

            FDLEntry.HelpString := Translator.GetString(GetToken(FieldBuffer));

            FDLEntry.HTML_HelpFile := Translator.GetString(GetToken(FieldBuffer));

            FieldDataList.Add(FDLEntry);

          except
          end;
          end;
        finally

        Translator.Free;
        end;
        end;// if AECFilds > 0
    end;

    if ToolTypeIsNumeric then begin
      ToolFault.Caption := Format(TranslatedMaxValue,[MaxNumericToolCode]);
    end else begin
      ToolFault.Caption := Format(TranslatedMaxValue,[AlphaToolTypeLength]);
    end;

    OpNumberFaultLabel.Caption := Format(TranslatedMust_Be_0_or, [MinOpNumber, MaxOpNumber]);

    MapList.Clear;
    IniFile.ReadSection('ElectrodeMapData',MapList);

    ToolOffsetPanel.Visible := ShowToolOffsets;
  finally
    IniFile.Free;
  end;
end;

procedure TGCodeForm.KnownWordChange(Sender : TObject);
begin
pp1.InhibitUpdate := True;
try
if ContextHelpEnabled and assigned(HelpLUp) then
  begin
  ContextHelpString := HelpLUp.FindItem(pp1.CurrentWord);
  if ContextHelpString <> '' then
     begin
     pp1.IndicateHelpAvailable := True;
     end
  else
     begin
     pp1.IndicateHelpAvailable := False;
     end;
  end;
finally
pp1.InhibitUpdate := False;
pp1.EditPage.ForceUpdate
end;
end;

procedure TGCodeForm.FormResize(Sender: TObject);
var
TopHeight : Integer;
begin
if ShowKB then
  begin
  TopHeight := ((Height div 5)*3)+40;
  PanelKB.Visible := True;
  PanelKB.Height :=Height-TopHeight-2;
  end
else
  begin
  TopHeight := Height-2;
  PanelKB.Visible := False;
  end;
AECGeneralPanel.Width := AECBackPanel.Width-AECButtonPanel.Width-4;
AECLeftpanel.Width :=(AECGeneralPanel.Width div 3)-2;
AECMiddlePanel.Width := AECLeftpanel.Width;

pp1.Visible := False;
OPPHeaderPanel.Visible := False;

Top := DisplayTop;
Left := 0;
pp1.Left := 1;
pp1.Top := 1;
pp1.Height := TopHeight;
pp1.Width := ((Width div 4)*3);
OPPHeaderPanel.Align := alNone;
OPPHeaderPanel.Height := TopHeight;//(Height div 5)*3;



KeyPad.Height := MainKB.Height;
KeyPad.Top := MainKb.Top;
MainKb.Width := (Width div 4)*3;
KeyPad.Width := Width-pp1.Width-12;
KeyPad.Left := MainKb.Width+2;
//PP1.Width := (Width div 4)*3;
ControlPanel.Top := pp1.Top;
ControlPanel.Width := KeyPad.Width;
ControlPanel.Left := KeyPad.Left;
ControlPanel.Height := TopHeight;
FindTextEdit.Width := ControlPanel.Width -12;
FindTextEdit.Left := 3;


OkButton.Width := (SavePanel.Width div 2)-4;
OkButton.Left := 2;

CancelButton.Width := (SavePanel.Width div 2)-4;
CancelButton.Left := 4+OkButton.Width;


FindButton.Width := (FindPanel.Width div 2) -4;
FindButton.Left :=2;

FindandReplaceButton.Width := (FindPanel.Width div 2) -4;
FindandReplaceButton.Left :=FindButton.Width + FindButton.Left+ 2;

FindAgainButton.Width :=  (SoptionsPanel.Width div 2)-4;
FindAgainButton.Left := 2;
FindAgainButton.Top := 2;

FindDoneKey.Width := FindAgainButton.Width;
FindDoneKey.Top := 2;
FindDoneKey.Left := FindAgainButton.Width+  FindAgainButton.Left + 2;
FindDoneKey.Height := FindAgainButton.Height;


FindWordButton.Width := (SoptionsPanel.Width div 2) -2;
FindWordButton.Left := 2;
FindWordButton.Top := FindAgainButton.Top + FindAgainButton.Height + 10;

MatchCaseButton.Width := FindWordButton.Width;
MatchCaseButton.Left :=2+FindWordButton.Width;
MatchCaseButton.Top := FindWordButton.Top;

FWdButton.Width := FindWordButton.Width;
FWdButton.Left := FindWordButton.Left;
FWdButton.Top := FindWordButton.Top + FindWordButton.Height + 10;

BackwardButton.Width := FWdButton.Width;
BackwardButton.Left :=2+FWdButton.Width;
BackwardButton.Top := FWdButton.Top;

UndoKey.Width := (UndoPanel.Width div 2) -2;
UndoKey.align := alLeft;

RedoKey.Width := (UndoPanel.Width div 2) -2;
RedoKey.align := alRight;


HeaderOKButton.Left :=  OPPPartPanel.Width-((HeaderOKButton.Width*2)+5);
HeaderCancelButton.Left :=  HeaderOKButton.Left+(HeaderOKButton.Width+2);
HeaderOKButton.Top :=  OPPPartPanel.height-(HeaderCancelButton.Height+5);
HeaderCancelButton.Top :=  HeaderOKButton.Top;
if EnableAECEditor then
  begin
  AECPanel.Visible := True;                              
  AECPanel.Top := 6;
  AECPanel.Left := Self.Width - AECPanel.Width - 16;
  end
else
  begin
  AECPanel.Visible := False;
  end;

if Editmode = oeGCodeEdit then
   begin
   pp1.Visible := True;
   ControlPanel.Visible := True;
   end
else
   begin
   //ShowKB := True;
   ControlPanel.Visible := False;
   OPPHeaderPanel.Visible := True;
   OPPHeaderPanel.Align := alTop;
   HexFault.Visible := False;
   ToolFault.Visible := False;
   end;


end;





procedure TGCodeForm.FindButtonClick(Sender: TObject);
begin
if Sender= FindButton then
   begin
   if FindButton.Down then
      begin
      ShowFindPage(fmFind);
      with FindTextedit do
           begin
           FindAgainButton.Visible := True;
           FindAgainButton.Caption := TranslatedFindAgain;
           FwdButton.Down := True;
           Include(SearchOptions,soFwd);
           enabled := True;
           Color := clwhite;
           Text := '';
           SetFocus;
           end;

      MainKB.FocusControl := FindTextedit;
      Keypad.FocusControl := FindTextedit;
      end
   else
       begin
       FocusOnEditor;
       end;
   end else

if Sender= FindAndReplaceButton then
   begin
   if FindAndReplaceButton.Down then
      begin
      ShowFindPage(fmReplace);
      with FindTextedit do
           begin
           FindAgainButton.Visible := True;
           FindAgainButton.Caption := TranslatedReplaceAgain;
           FwdButton.Down := True;
           Include(SearchOptions,soFwd);
           enabled := True;
           Color := clwhite;
           Text := '';
           SetFocus;
           end;
      MainKB.FocusControl := FindTextedit;
      Keypad.FocusControl := FindTextedit;

      end
   else
       begin
       FocusOnEditor;
       end;
   end else

if Sender= pp1 then
   begin
      ShowFindPage(fmFind);
      with FindTextedit do
           begin
           FindAgainButton.Visible := True;
           FindAgainButton.Caption := TranslatedFindAgain;
           FwdButton.Down := True;
           Include(SearchOptions,soFwd);
           enabled := True;
           Color := clwhite;
           Text := pp1.EditPage.CurrentWord;
           SetFocus;
           end;
      MainKB.FocusControl := FindTextedit;
      Keypad.FocusControl := FindTextedit;
      end

end;


procedure TGCodeForm.FindTextEditKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
if Sender = FindTextEdit then
begin
if FindMode = fmFind then
   begin
   if key = VK_RETURN then
      begin
      pp1.EditPage.SearchText(FindTextEdit.Text,SearchOptions);
      FocusOnEditor;
      FindAgainButton.Visible := True;
      FindButton.Down := False;
      end;
   end;

if FindMode = fmReplace then
   begin
   if key = VK_RETURN then
      begin
      FindAndReplaceButton.Down := False;
      ReplaceTextEdit.Visible := True;
      ReplaceTextEdit.SetFocus;
      MainKB.FocusControl := ReplaceTextedit;
      Keypad.FocusControl := ReplaceTextedit;

      end;
   end;
end;

if Sender = ReplaceTextEdit then
   begin
   if key = VK_RETURN then
      begin
      if pp1.EditPage.SearchText(FindTextEdit.Text,SearchOptions) then
         begin
         pp1.EditPage.CutToClipboard;
         ClipBoard.SetTextBuf(PChar(ReplaceTextEdit.Text));
         pp1.EditPage.PasteFromClipboard;
         end;
      FocusOnEditor;
      FindAgainButton.Visible := True;
      FindAndReplaceButton.Down := False;
      end;
   end;
end;

procedure TGCodeForm.FocusOnEditor;
begin
if EditMode = oegCodeEdit then
   begin
   pp1.EditPage.setFocus;
   MainKb.FocusControl := pp1.EditPage;
   KeyPad.FocusControl := pp1.EditPage;
   FindTextEdit.Enabled := False;
   FindButton.Down := False;
   end
else
   begin
   PartTypeEdit.setFocus;
   FindTextEdit.Enabled := False;
   FindButton.Down := False;
   end

end;


procedure TGCodeForm.FindAgainButtonClick(Sender: TObject);
begin
if FindMode = fmFind then
   begin
   pp1.EditPage.SearchText(FindTextEdit.Text,SearchOptions);
   FocusOnEditor;
   end;

if FindMode = fmReplace then
   begin
   if pp1.EditPage.SearchText(FindTextEdit.Text,SearchOptions) then
      begin
      pp1.EditPage.CutToClipboard;
      ClipBoard.SetTextBuf(PChar(ReplaceTextEdit.Text));
      pp1.EditPage.PasteFromClipboard;
      end;
   FocusOnEditor;
   end;
end;


procedure TGCodeForm.OKButtonClick(Sender: TObject);
var MapString : String;
    TVal      : Integer;

  function IsValidHex(MaxLen : Integer;TestString : String): Boolean;
  var HexSet : set of Char;
      CharCount : Integer;
  begin
    Result := False;
    HexSet := ['0'..'9','a'..'f','A'..'F'];
    if Length(TestString) > MaxLen then begin
      Exit;
    end;
    Result  := True;

    for CharCount := 1 to Length(TestString) do begin
      if not (TestString[CharCount] in HexSet) then begin
        Result  := False;
      end;
    end;
  end;

begin
  if EditMode = oeGCodeEdit then begin
    ModalResult := mrOK;
    Exit;
  end;

  ToolFault.Visible := True;
  if ToolTypeIsNumeric then begin
    try
      TVal := StrToInt(ToolTypeEdit.Text);
      ToolFault.Visible := (TVal < 0) or (TVal > MaxNumericToolCode);
    except
      // Assume conversion error, caught later by doing nothing
    end;
  end else begin
    ToolFault.Visible := Length(ToolTypeEdit.Text) > AlphaToolTypeLength
  end;

  try
    OpNumberFaultLabel.Visible := True;
    TVal := StrToInt(OpNumberEdit.Text);
    OpNumberFaultLabel.Visible := (TVal <> 0) and ((TVal < MinOpNumber) or (TVal > MaxOpNumber));
  except
    // Assume conversion error, caught later by doing nothing
  end;

  try
    ECountFaultLabel.Visible := True;
    StrToInt(ElectrodeCountEdit.Text);
    ECountFaultLabel.Visible := False;
  except
    // Assume conversion error, caught later by doing nothing
  end;

  MapString := ElectrodeMapEdit.Text;
  HexFault.Visible := not (IsValidHex(12,MapString) and (Length(MapString)=12));

  if not HexFault.Visible and
     not OpNumberFaultLabel.Visible and
     not ToolFault.Visible and
     not ECountFaultLabel.Visible then
      ModalResult := mrOK;
end;

procedure TGCodeForm.CancelButtonClick(Sender: TObject);
var
DoExit : Boolean;
begin
if not pp1.EditPage.Modified then
        begin
        ModalResult := mrCancel;
        end
else
        begin
        CancelDialog := TCancelDialog.Create(Self);
        try
        CancelDialog.ShowModal;
        DoExit :=  CancelDialog.ModalResult = mrOK;
        finally
        CancelDialog.Free;
        end;
        if DoExit then
                begin
                ModalResult := mrCancel;
                end;
        end;
end;

procedure TGCodeForm.FWdButtonClick(Sender: TObject);
begin
Include(SearchOptions,soFwd);
end;
procedure TGCodeForm.BackwardButtonClick(Sender: TObject);
begin
Exclude(SearchOptions,soFwd);
end;


procedure TGCodeForm.FindWordButtonClick(Sender: TObject);
begin
if FindWordButton.Down then
   begin
   Include(SearchOptions,soWholeWord);
   end
else
   begin
   Exclude(SearchOptions,soWholeWord);
   end
end;

procedure TGCodeForm.MatchCaseButtonClick(Sender: TObject);
begin
if MatchCaseButton.Down then
   begin
   Include(SearchOptions,soCaseSensitive);
   end
else
   begin
   Exclude(SearchOptions,soCaseSensitive);
   end
end;


procedure TGCodeForm.TopKeyMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
FocusOnEditor;
pp1.EditPage.TopofFile;
end;

procedure TGCodeForm.BottomKeyClick(Sender: TObject);
begin
FocusOnEditor;
pp1.EditPage.BottomofFile;

end;




procedure TGCodeForm.FormShow(Sender: TObject);
begin
if not assigned(HelpCommandTag) then
  begin
  HelpCommandTag :=Named.AquireTag('HelpDestination','TStringTAG',Nil);
  end;

if not assigned(HelpLaunchTag) then
  begin
  HelpLaunchTag  :=Named.AquireTag('HelpLaunch','TIntegerTag',Nil);
  end;

Width := 1022;
Height := DisplayHeight;
Top := DisplayTop;
PageControl1.ActivePage := TabOPPHeader;
FormResize(Self);
ShowFindPage(fmNone);
StartTimer.Enabled := True;
AECDataError.Visible := False;
if Named.GetAsBoolean(InchModeTag) then
      MetricToInch := 25.4
    else
      MetricToInch := 1;

 if EnableAECEditor and (PageControl1.ActivePage = TabOPPHeader) and  AECCheck.Checked then
    begin
    if not AllAECFieldsValid then
      begin
      AECDataError.Visible := True;
      end;
    end;

end;

procedure TGCodeForm.StartTimerTimer(Sender: TObject);
begin
StartTimer.Enabled := False;

pp1.StartEdit(FileString);
FocusOnEditor;

end;

procedure TGCodeForm.UndoKeyClick(Sender: TObject);
begin
pp1.EditPage.Undo;
end;

procedure TGCodeForm.RedoKeyClick(Sender: TObject);
begin
pp1.EditPage.Redo;
end;

procedure TGCodeForm.FormDestroy(Sender: TObject);
begin
  OPPData.Free;
  MapList.Free;
  HelpLUp.Free;
  while FieldDataList.Count > 0 do
    begin
    Dispose(pLimitRecord(FieldDataList[0]));
    FieldDataList.Delete(0);
    end;
end;

procedure TGCodeForm.PartTypeEditEnter(Sender: TObject);
begin
   MainKb.FocusControl := TEdit(Sender);
   KeyPad.FocusControl := TEdit(Sender);
end;







procedure TGCodeForm.HeaderCancelButtonClick(Sender: TObject);
begin
ModalResult := mrCancel;
end;


constructor TActiveOppServer.Create(aSectionName : string);
begin
  inherited;
  FActiveOppFile := TOppFile.Create;
//  aParent.RegisterMemEvent(nameNewActiveFile, edgeChange, TStringTag, NewProgram);
end;

procedure TActiveOppServer.Install;
begin
  inherited;
  OpNumberTag := Named.AddTag('OPP_OpNumber', 'TIntegerTag');
  PartTypeTag := Named.AddTag('OPP_PartType', 'TStringTag');
  CommentTag := Named.AddTag('OPP_HeaderComment', 'TStringTag');
  ToolTypeStringTag := Named.AddTag('OPP_ToolIDString', 'TStringTag');
  ElectrodeMapTag := Named.AddTag('OPP_ElectrodeMap', 'TStringTag');
  ElectrodeCountTag:= Named.AddTag('OPP_ElectrodeCount', 'TIntegerTag');
  ToolUsageTag:= Named.AddTag('OPP_ToolUsage', 'TDoubleTag');
  CycleTimeTag:= Named.AddTag('OPP_CycleTime', 'TIntegerTag');
  ToolXOffsetTag := Named.AddTag('OPP_GenericToolXOffset', 'TDoubleTag');
  ToolYOffsetTag := Named.AddTag('OPP_GenericToolYOffset', 'TDoubleTag');
  ToolZOffsetTag := Named.AddTag('OPP_GenericToolZOffset', 'TDoubleTag');
  AEC_SelectedTag               := Named.AddTag('OPP_AEC_Selected', 'TIntegerTag');
  AEC_RestrokeTag               := Named.AddTag('OPP_AEC_Restroke', 'TDoubleTag');
  AEC_StowFeedTag               := Named.AddTag('OPP_AEC_StowFeed', 'TDoubleTag');
  AEC_CheckHoleProgramTag       := Named.AddTag('OPP_AEC_CheckHoleProgram', 'TIntegerTag');
  AEC_QualifyStrokeTag          := Named.AddTag('OPP_AEC_QualifyStroke', 'TDoubleTag');
  AEC_QualifyRetryLoopsTag      := Named.AddTag('OPP_AEC_QualifyRetryLoops', 'TIntegerTag');
  AEC_ElectrodeClearanceTag     := Named.AddTag('OPP_AEC_ElectrodeClearance', 'TDoubleTag');
  AEC_LoadPositionUTag          := Named.AddTag('OPP_AEC_LoadPositionU', 'TDoubleTag');
  AEC_EjectLengthTag            := Named.AddTag('OPP_AEC_EjectLength', 'TDoubleTag');
  AEC_DoubleIndexTag            := Named.AddTag('OPP_AEC_DoubleIndex', 'TIntegerTag');
  AEC_FirstPulseDurationTag     := Named.AddTag('OPP_AEC_FirstPulseDuration', 'TDoubleTag');
  AEC_SecondPulseDurationTag    := Named.AddTag('OPP_AEC_SecondPulseDuration', 'TDoubleTag');
  AEC_EjectPulseCountTag        := Named.AddTag('OPP_AEC_EjectPulseCount', 'TIntegerTag');
  AEC_EjectPulse1DurationTag    := Named.AddTag('OPP_AEC_EjectPulse1Duration', 'TDoubleTag');
  AEC_EjectPulse2DurationTag    := Named.AddTag('OPP_AEC_EjectPulse2Duration', 'TDoubleTag');
  AEC_LoadPulseCountTag         := Named.AddTag('OPP_AEC_LoadPulseCount', 'TIntegerTag');



end;

procedure TActiveOppServer.Consume(aSectionName, Buffer : PChar);
var S : TStreamQuoteTokenizer;
begin
  S := TStreamQuoteTokenizer.Create;
  try
    S.SetStream(TStringStream.Create(Buffer));
    S.Seperator := '=';
    S.QuoteChar := '''';
    S.Rewind;
    FActiveOppFile.Load(S);

    with FActiveOPPFile do begin
      Named.SetAsString(PartTypeTag, PChar(PartType));
      Named.SetAsString(CommentTag, PChar(Comment));
      Named.SetAsString(ToolTypeStringTag, PChar(ToolTypeString));
      Named.SetAsString(ElectrodeMapTag, PChar(ElectrodeMap));
      Named.SetAsInteger(ElectrodeCountTag, ElectrodeCount);
      Named.SetAsDouble(ToolUsageTag, ToolUsage);
      Named.SetAsInteger(CycleTimeTag, CycleTime);
      Named.SetAsDouble(ToolXOffsetTag, ToolXOffset);
      Named.SetAsDouble(ToolYOffsetTag, ToolYOffset);
      Named.SetAsDouble(ToolZOffsetTag, ToolZOffset);
      Named.SetAsInteger(OpNumberTag, OpNumber);
      // AEC Tags
      Named.SetAsInteger(AEC_SelectedTag,AEC_Selected);
      Named.SetAsDouble(AEC_RestrokeTag,AEC_Restroke);
      Named.SetAsDouble(AEC_StowFeedTag,AEC_StowFeed);
      Named.SetAsInteger(AEC_CheckHoleProgramTag,AEC_CheckHoleProgram);
      Named.SetAsDouble(AEC_QualifyStrokeTag,AEC_QualifyStroke);
      Named.SetAsInteger(AEC_QualifyRetryLoopsTag,AEC_QualifyRetryLoops);
      Named.SetAsDouble(AEC_ElectrodeClearanceTag,AEC_ElectrodeClearance);
      Named.SetAsDouble(AEC_LoadPositionUTag,AEC_LoadPositionU);
      Named.SetAsDouble(AEC_EjectLengthTag,AEC_EjectLength);
      Named.SetAsInteger(AEC_DoubleIndexTag,AEC_DoubleIndex);
      Named.SetAsDouble(AEC_FirstPulseDurationTag,AEC_FirstPulseDuration);
      Named.SetAsDouble(AEC_SecondPulseDurationTag,AEC_SecondPulseDuration);
      Named.SetAsInteger(AEC_EjectPulseCountTag,AEC_EjectPulseCount);
      Named.SetAsDouble(AEC_EjectPulse1DurationTag,AEC_EjectPulse1Duration);
      Named.SetAsDouble(AEC_EjectPulse2DurationTag,AEC_EjectPulse2Duration);
      Named.SetAsInteger(AEC_LoadPulseCountTag,AEC_LoadPulseCount);



    end;
//    NotifyClients;
  finally
    S.Free;
  end;
end;



procedure TGCodeForm.ElectrodeMapEditEnter(Sender: TObject);
begin
MainKb.FocusControl := ElectrodeMapEdit;
KeyPad.FocusControl := ElectrodeMapEdit;
end;



procedure TGCodeForm.ReadConfigFile;
var
ConfigFile :TIniFile;
Wordlist : TStringList;
FileName : String;
keywordcolour : LongInt;
commentcolour : LongInt;
FontColour    : Longint;
BackGroundColour : Longint;
CommentStart  : String;
C3Start       : String;
Buffer        : String;
WordNumber    : Integer;
FontStyle     : TExtFontStyles;
DelimeterSet  : Set of Char;
CharCount     : Integer;
htmlBasepath : String;
I : Integer;
LangID : String;
begin
FileName := DLLProfilePath+'OPPEditor.cfg';
if FileExists(FileName) then
   begin
   try
   ConfigFile := TInifile.Create(Filename);
   WordList := TStringList.Create;
   try
   with ConfigFile do
        begin
        // Set default font
        IniWordwrap := ReadBool('GENERAL', 'Wordwrap', False);
        Pp1.EditPage.WordWrap := IniWordwrap;

        Buffer := ReadString('colours','backgroundColour','clwhite');
        if not IdentToColor(Buffer,BackGroundColour) then
           begin
           BackGroundColour := clwhite;
           end;
        Pp1.EditPage.Color := BackGroundColour;

        Buffer := ReadString('colours','normalfontname','Verdana');
        pp1.Font.Name := Buffer;
        pp1.Font.Size := ReadInteger('colours','normalfontsize',12);
        Buffer := ReadString('colours','normalfontcolour','clblack,bold');
        if not IdentToColor(CSVField(0,Buffer),FontColour) then
           begin
           FontColour := clBlack;
           end;
        pp1.Font.Color := FontColour;
        FontStyle := [];
        if CSVField(1,Buffer)='bold' then include(FontStyle,fsbold);
        pp1.Font.Style := FontStyle;

        Buffer := ReadString('delimeters','Characters','$%&,./:;<=>(');
        DelimeterSet := [];
        include(DelimeterSet,Char(' '));
        For CharCount := 1 to Length(Buffer) do
            begin
            include(DelimeterSet,Char(Buffer[CharCount]));
            end;

        pp1.EditPage.Delimiters := Delimeterset;

        { This to be replaced by automatic update of the linked look-up based
         on the names of all the html files under a known directory.
         This can be overriden and or added to by the config file in order to
         target specific links within an html file or to make the name of
         the html file different to the known word}
        ContextHelpEnabled := False;
        htmlBasepath := '';
        Buffer := ReadString('contexthelp','AbsolutePath','');
        if Buffer = '' then
          begin
          Buffer := ReadString('contexthelp','ProfileRelativePath','');
          if Buffer <> '' then
            begin
            I := Languages.IndexOf(SysLocale.DefaultLCID);
            LangID := Copy(Languages.Ext[I], 1, 2);
            Buffer := Buffer+LangID;
            htmlBasepath := dllprofilePath + Buffer;
            end
          end
        else
          begin
          htmlBasepath := Buffer;
          end;
        if htmlBasepath <> '' then
          begin
          HelpLUp := TLinkedLookUp.Create(Self);
          ContextHelpEnabled := BuildHTMLwordlist(htmlBasepath);
          end;
        HelpBase := htmlBasepath;

        // Add all keywords to list
        ReadSection('keywords',Wordlist);
        if Wordlist.Count > 0 then
           begin
           pp1.EditPage.Keywords.BeginUpdate;
           For WordNumber := 0 to Wordlist.Count-1 do
               begin
               Buffer := ReadString('colours','keyword','clmaroon,bold');
               IdentToColor(CSVField(0,Buffer),keywordcolour);
               FontStyle := [];
               if CSVField(1,Buffer)='bold' then include(FontStyle,fsbold);
               if CSVField(2,Buffer)='italics' then include(FontStyle,fsitalic);
               pp1.EditPage.Keywords.AddKeyWord(WordList[WordNumber],[woWholeWordsOnly],FontStyle,1,crDefault,
                                       BackGroundColour,keywordcolour);
               end;
           pp1.EditPage.ReApplyKeywords;
           pp1.EditPage.ApplyKeyWords := True;
           pp1.EditPage.Keywords.EndUpdate;
           end;

        // Now add comment identifiers
        Buffer := ReadString('colours','comment','clblue,bold,italic');
        if not IdentToColor(CSVField(0,Buffer),commentcolour) then
           begin
           commentColour := clBlack;
           end;
        FontStyle := [];
        if CSVField(1,Buffer)='bold' then include(FontStyle,fsbold);
        if CSVField(2,Buffer)='italics' then include(FontStyle,fsitalic);
        CommentStart := ReadString('comments','starttext1','');
        if  CommentStart <> '' then
            begin
            pp1.EditPage.StartStopKeys.AddStartStopKey(CommentStart,'',[],FontStyle,1,crDefault,BackGroundColour,CommentColour,True);
            end;

        // Now add C3 colouring
        Buffer := ReadString('colours','C3Calls','clAqua,bold,italic');
        if not IdentToColor(CSVField(0,Buffer),commentcolour) then
           begin
           CommentColour := clBlack;
           end;
        FontStyle := [];
        if CSVField(1,Buffer)='bold' then include(FontStyle,fsbold);
        if CSVField(2,Buffer)='italics' then include(FontStyle,fsitalic);
        C3Start := ReadString('C3Calls','StartText','');
        if  CommentStart <> '' then
            begin
            pp1.EditPage.StartStopKeys.AddStartStopKey(C3Start,' ',[],FontStyle,1,crDefault,BackGroundColour,CommentColour,True);
            end;

        // Now add Label start character id
        Buffer := ReadString('colours','label','clgreen,bold');
        if not IdentToColor(CSVField(0,Buffer),commentcolour) then
           begin
           commentColour := clGreen;
           end;
        FontStyle := [];
        if CSVField(1,Buffer)='bold' then include(FontStyle,fsbold);
        if CSVField(2,Buffer)='italics' then include(FontStyle,fsitalic);
        CommentStart := ReadString('labels','startchar1','');
        if  CommentStart <> '' then
            begin
            pp1.EditPage.StartStopKeys.AddStartStopKey(CommentStart,' ',[],FontStyle,1,crDefault,BackGroundColour,CommentColour,True);
            end;
        CommentStart := ReadString('labels','startchar2','');
        if  CommentStart <> '' then
            begin
            pp1.EditPage.StartStopKeys.AddStartStopKey(CommentStart,' ',[],FontStyle,1,crDefault,BackGroundColour,CommentColour,True);
            end;
        end;
   finally
   ConfigFile.Free;
   end;
   finally
   WordList.Free;
   end;
   end;
end;


//*********************************************
// function CSVField(
// Result is then Commaseparated field specified by
// FNumber in input Line TextString
//*********************************************
function CSVField(FNumber: Integer;TextString : String):String;
var
Field      : Integer;
begin
Field := FNumber;
while Field >= 0 do
      begin
      Result :=GetToken(TextString);
      dec(Field);
      end;
end;

function SlashSep(const Path, S: String): String;
begin
  if AnsiLastChar(Path)^ <> '\' then
    Result := Path + '\' + S
  else
    Result := Path + S;
end;


//*********************************************
// function GetToken
// Result is next commaseparatred string
// var Buffer contains remainder of input string
//*********************************************
function GetToken(var Buffer : String):String;
var
CommaPos : Integer;
begin
Commapos := POs(',',Buffer);
if Commapos > 0 then
   begin
   Result := Copy(Buffer,1,CommaPos-1);
   Buffer := Copy(Buffer,CommaPos+1,Length(Buffer));
   end
else
    begin
    Result := Buffer;
    Buffer := '';
    end;
end;






procedure TGCodeForm.ContentsChanged(Sender: TObject);
begin
UndoKey.Enabled := pp1.EditPage.CanUndo;
RedoKey.Enabled := pp1.EditPage.CanRedo;
end;


procedure TGCodeForm.BookMarkKeyMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
pp1.ShowBookMarkPanel := not pp1.ShowBookMarkPanel;
if pp1.BookMarkPanel.Visible then
   begin
   BookMarkKey.Legend := 'Hide~Bookmark';
   end
else
   begin
   BookMarkKey.Legend := 'Bookmark~Control';
   end

end;

procedure TGCodeForm.ShowFindPage(fMode: TFindModes);
begin
FindMode := fMode;
if fMode = fmNone then
   begin
   SearchPages.ActivePage := LogoSheet;
   FindModePanel.Visible := True;
   UndoControls.Visible := True;
   end;


if fMode = fmFind then
   begin
   SearchPages.ActivePage := FindSheet;
   UndoControls.Visible := False;
   FindTextEdit.Parent := FindSheet;
   SOptionsPanel.Parent := FindSheet;
   FindModePanel.Visible := True;
   FindAgainButton.Visible := False;
   end;

if fMode = fmReplace then
   begin
   SearchPages.ActivePage := ReplaceSheet;
   UndoControls.Visible := False;
   SOptionsPanel.Parent := ReplaceSheet;
   FindTextEdit.Parent := ReplaceSheet;
   FindModePanel.Visible := True;
   FindAgainButton.Visible := False;
   end;



end;

procedure TGCodeForm.FindDoneKeyMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
FindButton.Down := False;
FindAndReplaceButton.Down := False;
ShowFindPage(fmNone);
end;

procedure TGCodeForm.ElectrodeCountEditChange(Sender: TObject);
var
ECount :integer;
begin
try
ECount := StrToInt(ElectrodeCountEdit.Text);
if (Ecount > 0) and (ECount <= MapList.Count) then
   begin
   ElectrodeMapEdit.Text := MapList[Ecount-1];
   end
else
   begin
   //ElectrodeMapEdit.Text := '000000000000';
   end

except
ElectrodeMapEdit.Text :=  '000000000000';
end
end;

procedure TGCodeForm.BitBtn1Click(Sender: TObject);
var Doc : TOPPDoc;
begin
  Doc:= TOPPDoc.Create;
  Doc.DestDir := NamedPlugin.DllLocalPath;
  Doc.C3 := NamedPlugin.DllProfilePath + 'c3.cfg';
  Doc.StyleSheet := NamedPlugin.DllProfilePath + 'stylesheet.css';

  Doc.Document(pp1.Lines.Text);
  ShellExecute(Self.Handle, 'open',
               PChar(NamedPlugin.DllLocalPath + 'Opp.html'),
                          nil,
                          nil, SW_SHOWNORMAL);

end;

procedure TGCodeForm.pp1FindModeRequested(Sender: TObject);
begin
FindButtonClick(pp1);
end;


procedure TGCodeForm.AECCheckClick(Sender: TObject);
begin
if AECCheck.Checked then
  begin
  HeaderAECEdit.Enabled := True;
  end
else
  begin
  HeaderAECEdit.Enabled := False;
  end;
end;

procedure TGCodeForm.HeaderAECEditClick(Sender: TObject);
begin
PageControl1.ActivePage := TabAECParams;
try
AECDataInchMetricDisplay.SetFocus;
except
end;

OPPHeaderTitle.Caption := TranslatedOPPHeader;

end;

procedure TGCodeForm.AEC_FieldEnter(Sender: TObject);
var
FDLEntry : pLimitRecord;
begin

if sender is TEdit then TEdit(Sender).Color := clMoneyGreen;
if not assigned(FieldDataList) then exit;
If Sender is Tedit then
  begin
  MainKb.FocusControl := Tedit(Sender);
  KeyPad.FocusControl := Tedit(Sender);
  with Sender as Tedit do
    begin
    if (Tag>=0) and (Tag<FieldDataList.Count) then
      begin
      FDLEntry := FieldDataList[Tag];
      AECHelpPanel.Caption := FDLEntry.HelpString;
      end;
    end;
  end
else
  begin
  MainKb.FocusControl := AECDataCheckHoleProgram;
  KeyPad.FocusControl := AECDataCheckHoleProgram;
  end
end;


procedure TGCodeForm.AECBackButtonClick(Sender: TObject);
begin
if AllAECFieldsValid then
  begin
  PageControl1.ActivePage := TabOPPHeader;
  OPPHeaderTitle.Caption := TranslatedHeaderData;
  AECDataError.Visible := False;
  end
else
  begin
  AECDataError.Visible := True;
  end;
end;



function TGCodeForm.AllAECFieldsValid : Boolean;
var
FDLEntry : pLimitRecord;
FieldNumber : Integer;

begin
Result := True;
FieldNumber := 0;
while (Result = True) and (FieldNumber < FieldDataList.Count) do
  begin
  try
  FDLEntry := FieldDataList[FieldNumber];
  REsult := FDLEntry.Valid;
  finally
  inc(FieldNumber);
  end;
  end;
end;

procedure TGCodeForm.AEC_FieldExit(Sender: TObject);
function ValueValid(ValStr:String;TagNumber : Integer): Boolean;
var
FDLEntry : pLimitRecord;
Value : Double;
MaxV,MinV : Double;
begin
Result := False;
if (TagNumber>=0) and (TagNumber<FieldDataList.count) then
  begin
  FDLEntry := FieldDataList[TagNumber];
  FDLEntry.Valid := False;
  try
  Value := StrToFloat(ValStr);
  if InchEntry and FDLEntry.ConvertLimit then
    begin
    MaxV := FDLentry.MaxVal/25.0;
    MinV := FDLEntry.MinVal/25.0;
    end
  else
    begin
    MaxV := FDLentry.MaxVal;
    MinV := FDLEntry.MinVal;
    end;

  if (Value <= MaxV) and (Value >= MinV) then
    begin
    if FDLEntry.IsInteger then
      begin
      if floor(Value) = Value then
        begin
        Result := True;
        FDLEntry.Valid := True;
        end;
      end
    else
      Result := True;
      FDLEntry.Valid := True;
    end;
  except
  end;



  end;
end;
begin
if sender is Tedit then
  begin
  if (TEdit(Sender).Tag = 0) or (TEdit(Sender).Tag = 4) then
    begin
    TEdit(Sender).Color := clwhite;
    end
  else if ValueValid(TEdit(Sender).Text,Tedit(Sender).Tag) then
    begin
    TEdit(Sender).Color := clwhite;
    end
  else
    begin
    TEdit(Sender).Color := clred;
    end;

  if TEdit(SEnder).TabOrder >= 15 then
    begin
    try
    AECDataInchMetricDisplay.SetFocus;
    except
    end
    end;
  end;
  AECDataError.Visible := not(AllAECFieldsValid);
end;

procedure TGCodeForm.BtnDefaultClick(Sender: TObject);
var
FieldCount : Integer;
begin
For FieldCount := 0 to FieldDataList.Count-1 do
  begin

  end;

end;


procedure TGCodeForm.AECDoubleIndexButtonMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
Buffer : String;
begin
Buffer := AECDoubleIndexDisplay.Text;
if (Buffer[1] = 'T') or (Buffer[1] = 't') then
  AECDoubleIndexDisplay.Text := TranslatedFalse
else
  AECDoubleIndexDisplay.Text := TranslatedTrue

end;

procedure TGCodeForm.AECCheckEnter(Sender: TObject);
begin
   MainKb.FocusControl := AECCheck;
   KeyPad.FocusControl := AECCheck;
end;


procedure TGCodeForm.AECInchMetricButtonMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
Buffer : String;
begin
Buffer := AECDataInchMetricDisplay.Text;
if (Buffer[1] = 'M') or (Buffer[1] = 'm') then
  begin
  AECDataInchMetricDisplay.Text := TranslatedInch;
  InchEntry := True;
  end
else
  begin
  AECDataInchMetricDisplay.Text := TranslatedMetric;
  InchEntry := False;
  end ;
 AECDataCheckHoleProgram.SetFocus;
 AECDataStowFeed.SetFocus;
 AECDataRestroke.SetFocus;
 AECDataEjectLength.SetFocus;
 AECDataLoadPositionU.SetFocus;
 AECDataQualifyStroke.SetFocus;
 AECDataElectrodeClearance.SetFocus;
 AECDataInchMetricDisplay.SetFocus;





end;

procedure TGCodeForm.pp1FindAgainRequested(Sender: TObject);
begin
FindAgainButtonClick(Self);
end;

procedure TGCodeForm.BtnHideKBMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
if ShowKB then
  begin
  BtnHideKB.Legend := TranslatedShowKB;
  ShowKB := False;
  end
else
  begin
  BtnHideKB.Legend := TranslatedHideKB;
  ShowKB := True;
  end;
FormResize(Self);

end;

procedure TGCodeForm.pp1F1HelpRequested(Sender: TObject);
begin
// here when F1 pressed
if BtnHelp.Enabled then
  begin
  LaunchHelp;
  end
end;

procedure TGCodeForm.BtnHelpMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
LaunchHelp;
end;

procedure TGCodeForm.LaunchHelp;
begin
if assigned(HelpCommandTag) then
  begin
  if FileExists(ContextHelpString) then
    begin

    Named.SetAsString(HelpCommandTag,PAnsiChar('"'+ContextHelpString+'"'));
    end
  else
    if FileExists('C:\Acnc32\profile\Live\Help\HTML\English\MainIndex.html') then
      begin
      Named.SetAsString(HelpCommandTag,PAnsiChar('C:\Acnc32\profile\Live\Help\HTML\English\MainIndex.html'));
      end
    else
      begin
      Named.SetAsString(HelpCommandTag,PAnsiChar(''));
      end

  end;
if assigned(HelpLaunchTag) then
  begin
  Named.SetAsInteger(HelpLaunchTag,1);
  Named.SetAsInteger(HelpLaunchTag,0);
  end;
end;

function TGCodeForm.BuildHTMLwordlist(Basepath: String): Boolean;
var
Entry : Integer;
DotPos : Integer;
KeyWord : String;
FileList : TStringList;
begin
Result := False;
FileList := TStringList.Create;
try
try
Win32MakeDirectoryList(BasePath,FileList,100,'.html');
if FileList.Count < 1 then exit;
// we now have a list of full paths of htmlfiles
// Now reduce to helpwordlist
For Entry := 0 to FileList.Count-1 do
  begin
  KeyWord := ExtractFileName(FileList[Entry]);
  DotPos := Pos('.',KeyWord);
  KeyWord := Copy(KeyWord,1,DotPos-1);
  HelpLUp.AddItem(KeyWord,FileList[Entry]);
  Result := True;
  end;
except
FileList.Clear;
Result := False;
end;
finally
FileList.Free;
end;
end;

procedure TGCodeForm.Win32MakeDirectoryList(const DirName: String;
                                                  DList : TStringList;
                                                  MaxCount : Integer;
                                                  FilterExtension : String);
var
  FileInfo: TWin32FindData;
  FindHandle: THandle;
  Temp: String;
  Temp1 : String;
begin
  if DirName = '' then exit;
  if not assigned(Dlist) then exit;
  if DList.Count > MaxCount then exit;
  FindHandle:= Windows.FindFirstFile(PChar(DirName + '\*'), FileInfo);
  if FindHandle <> INVALID_HANDLE_VALUE then
  repeat
    Temp:= DirName + '\' + FileInfo.cFileName;
    if (FileInfo.dwFileAttributes and FILE_ATTRIBUTE_DIRECTORY) = 0 then
    begin
      if FilterExtension = '' then
        begin
        DList.Add(Temp)
        end
      else
        begin
        Temp1 := Uppercase(ExtractFileExt(Temp));
        if Temp1 = (Uppercase(FilterExtension)) then
          begin
          DList.Add(Temp)
          end
        end
    end else
    if (StrComp(FileInfo.cFileName, '.') <> 0) and
       (StrComp(FileInfo.cFileName, '..') <> 0)  then
    begin
        Win32MakeDirectoryList(Temp,DList,MaxCount,FilterExtension); //!!!! recursion
        //Win32DeleteDirectory(Temp);
        //SysUtils.RemoveDir(Temp)
   //   end

    end;
  until not Windows.FindNextFile(FindHandle, FileInfo);
  Windows.FindClose(FindHandle);
  SysUtils.RemoveDir(DirName)
end;


procedure TGCodeForm.AECDefaultButtonClick(Sender: TObject);
begin
AECSetDefault;
end;


procedure TGCodeForm.AECDataKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
SenderTag : Integer;
FDLEntry  : pLimitRecord;
PosHash : Integer;
ContextBuffer : String;

begin
if ContextHelpEnabled  then
  begin
  ContextHelpString := '';
  if Sender is Tedit then
   begin
   SenderTag := Tedit(Sender).Tag;
   if assigned(FieldDataList) then
    begin
    if SenderTag < FieldDataList.Count then
      begin
      FDLEntry := FieldDataList[SenderTag];
      ContextBuffer := FDLEntry.HTML_HelpFile;
      if assigned(HelpLUp) then
        begin
        PosHash := Pos('#',ContextBuffer);
        if PosHash > 0 then
          begin
          ContextHelpString := Copy(ContextBuffer,1,PosHash-1);
          ContextHelpDestination := Copy(ContextBuffer,PosHash,1000);
          ContextHelpString := HelpLUp.FindItem(ContextHelpString)
          end
        else
          begin
          ContextHelpString := '';
          ContextHelpDestination := ''
          end;
        end;
      end;
    end;
  end;

  end;
if Key = VK_F1 then
  begin
  LaunchHelp;
  end;
end;


initialization
  // Add the OPPEditor to the interface and declare its section to be 'Floop'
  TAbstractOPPPluginEditor.AddPlugin('NC32GCODE', TGCodeEditor);
  {$IFNDEF NO_OPP_HEADER}
  TOPPHeaderEditor.AddPlugin(SectionOperationDescription, TOPPHeaderEditor);
  TActiveOppServer.AddPlugin(SectionOperationDescription, TActiveOppServer);
  {$ENDIF}
end.

