object GcodeForm: TGcodeForm
  Left = 62
  Top = 110
  BorderIcons = []
  BorderStyle = bsNone
  Caption = 'GcodeForm'
  ClientHeight = 741
  ClientWidth = 1016
  Color = clSilver
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = True
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label7: TLabel
    Left = 10
    Top = 102
    Width = 78
    Height = 13
    Caption = 'First Eject Pulse '
  end
  object Label8: TLabel
    Left = 10
    Top = 120
    Width = 40
    Height = 13
    Caption = 'Duration'
  end
  object OPPHeaderPanel: TPanel
    Left = 10
    Top = 16
    Width = 1000
    Height = 450
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    object PageControl1: TPageControl
      Left = 1
      Top = 1
      Width = 998
      Height = 448
      ActivePage = TabOPPHeader
      Align = alClient
      TabOrder = 0
      object TabOPPHeader: TTabSheet
        TabVisible = False
        object OPPPartPanel: TPanel
          Left = 0
          Top = 0
          Width = 990
          Height = 438
          Align = alClient
          BevelInner = bvLowered
          TabOrder = 0
          object PartTypeLabel: TLabel
            Left = 30
            Top = 16
            Width = 77
            Height = 20
            Alignment = taRightJustify
            Caption = 'Part Type'
            FocusControl = PartTypeEdit
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlue
            Font.Height = -16
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object ReferenceLabel: TLabel
            Left = 30
            Top = 58
            Width = 85
            Height = 20
            Alignment = taRightJustify
            Caption = 'Reference'
            FocusControl = ReferenceEdit
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlue
            Font.Height = -16
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object ToolTYpeLabel: TLabel
            Left = 30
            Top = 100
            Width = 78
            Height = 20
            Alignment = taRightJustify
            Caption = 'Tool Type'
            FocusControl = ToolTypeEdit
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlue
            Font.Height = -16
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object ToolUsageLabel: TLabel
            Left = 435
            Top = 100
            Width = 92
            Height = 20
            Alignment = taRightJustify
            Caption = 'Tool Usage'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -16
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object CycleTimeLabel: TLabel
            Left = 435
            Top = 141
            Width = 87
            Height = 20
            Alignment = taRightJustify
            Caption = 'Cycle Time'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -16
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label3: TLabel
            Left = 600
            Top = 141
            Width = 29
            Height = 20
            Alignment = taRightJustify
            Caption = 'sec'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -16
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object HeaderOKButton: TSpeedButton
            Left = 769
            Top = 361
            Width = 100
            Height = 48
            Caption = 'OK'
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -16
            Font.Name = 'Verdana'
            Font.Style = [fsBold]
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            OnClick = OKButtonClick
          end
          object HeaderCancelButton: TSpeedButton
            Left = 870
            Top = 361
            Width = 100
            Height = 48
            Caption = 'Cancel'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -16
            Font.Name = 'Verdana'
            Font.Style = [fsBold]
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            OnClick = HeaderCancelButtonClick
          end
          object ToolFault: TLabel
            Left = 260
            Top = 104
            Width = 120
            Height = 16
            Caption = '8 Characters Max'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clRed
            Font.Height = -13
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object OPNumberLabel: TLabel
            Left = 30
            Top = 148
            Width = 91
            Height = 20
            Alignment = taRightJustify
            Caption = 'Op Number'
            FocusControl = ToolTypeEdit
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlue
            Font.Height = -16
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object OpNumberFaultLabel: TLabel
            Left = 260
            Top = 152
            Width = 107
            Height = 16
            Caption = 'Must be integer'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clRed
            Font.Height = -13
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            Visible = False
          end
          object PartTypeEdit: TEdit
            Left = 140
            Top = 12
            Width = 450
            Height = 28
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -16
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 0
            Text = 'PartTypeEdit'
            OnEnter = PartTypeEditEnter
          end
          object ReferenceEdit: TEdit
            Left = 140
            Top = 54
            Width = 450
            Height = 28
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -16
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 1
            Text = 'ReferenceEdit'
            OnEnter = PartTypeEditEnter
          end
          object ToolTypeEdit: TEdit
            Left = 140
            Top = 96
            Width = 110
            Height = 28
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -16
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 2
            OnEnter = PartTypeEditEnter
          end
          object ToolUsageEdit: TEdit
            Left = 535
            Top = 96
            Width = 55
            Height = 28
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -16
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 3
            Text = '0'
            OnEnter = PartTypeEditEnter
          end
          object CycleTimeEdit: TEdit
            Left = 535
            Top = 137
            Width = 55
            Height = 28
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -16
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 4
            Text = '0'
            OnEnter = PartTypeEditEnter
          end
          object ToolOffsetPanel: TPanel
            Left = 640
            Top = 16
            Width = 273
            Height = 129
            BevelInner = bvLowered
            TabOrder = 5
            Visible = False
            object ToolXOffsetLabel: TLabel
              Left = 12
              Top = 9
              Width = 107
              Height = 20
              Caption = 'Tool X Offset'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -16
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object ToolYOffsetLabel: TLabel
              Left = 12
              Top = 49
              Width = 107
              Height = 20
              Caption = 'Tool Y Offset'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -16
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object ToolZOffsetLabel: TLabel
              Left = 12
              Top = 89
              Width = 106
              Height = 20
              Caption = 'Tool Z Offset'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -16
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object ToolXOffsetEdit: TEdit
              Left = 135
              Top = 5
              Width = 121
              Height = 32
              TabOrder = 0
              OnEnter = PartTypeEditEnter
            end
            object ToolYOffsetEdit: TEdit
              Left = 135
              Top = 45
              Width = 121
              Height = 32
              TabOrder = 1
              OnEnter = PartTypeEditEnter
            end
            object ToolZOffsetEdit: TEdit
              Left = 135
              Top = 85
              Width = 121
              Height = 32
              TabOrder = 2
              OnEnter = PartTypeEditEnter
            end
          end
          object OpNumberEdit: TEdit
            Left = 140
            Top = 145
            Width = 59
            Height = 28
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -16
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 6
            Text = '1'
            OnEnter = PartTypeEditEnter
          end
        end
      end
    end
  end
  object PanelKB: TPanel
    Left = 0
    Top = 528
    Width = 1016
    Height = 213
    Align = alBottom
    TabOrder = 1
    object MainKb: TBaseKB
      Left = 1
      Top = 1
      Width = 777
      Height = 211
      Align = alLeft
      BevelOuter = bvNone
      Caption = 'MainKb'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -19
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      KeyboardMode = kbmAlphaNumeric
      HighlightColor = clBlack
      Options = []
      SecondaryFont.Charset = DEFAULT_CHARSET
      SecondaryFont.Color = clWindowText
      SecondaryFont.Height = -11
      SecondaryFont.Name = 'MS Sans Serif'
      SecondaryFont.Style = []
      RepeatDelay = 500
      ExternalMode = False
      FocusByHandle = False
      FocusHandle = 0
    end
    object KeyPad: TBaseFPad
      Left = 776
      Top = 10
      Width = 233
      Height = 250
      ExternalMode = False
      ChangeCursorKeys = True
      EnableCursorKeys = True
    end
  end
  object FileOPen: TOpenDialog
    DefaultExt = 'opp'
    InitialDir = 'c:\acnc32\programs'
    Left = 944
    Top = 136
  end
  object StartTimer: TTimer
    Enabled = False
    Interval = 400
    OnTimer = StartTimerTimer
    Left = 972
    Top = 497
  end
end
