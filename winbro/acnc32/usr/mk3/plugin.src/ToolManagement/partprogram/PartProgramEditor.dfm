object GCodeForm: TGCodeForm
  Left = 62
  Top = 110
  BorderIcons = []
  BorderStyle = bsNone
  Caption = 'GCodeForm'
  ClientHeight = 741
  ClientWidth = 1016
  Color = clSilver
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = True
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label7: TLabel
    Left = 10
    Top = 102
    Width = 78
    Height = 13
    Caption = 'First Eject Pulse '
  end
  object Label8: TLabel
    Left = 10
    Top = 120
    Width = 40
    Height = 13
    Caption = 'Duration'
  end
  object ControlPanel: TPanel
    Left = 772
    Top = 9
    Width = 245
    Height = 550
    BevelInner = bvLowered
    TabOrder = 0
    object SavePanel: TPanel
      Left = 2
      Top = 2
      Width = 241
      Height = 50
      Align = alTop
      BevelInner = bvLowered
      TabOrder = 1
      object OkButton: TTouchSoftkey
        Left = 2
        Top = 2
        Width = 97
        Height = 45
        Caption = 'OkButton'
        Font.Charset = ANSI_CHARSET
        Font.Color = clGreen
        Font.Height = -19
        Font.Name = 'Comic Sans MS'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        OnClick = OKButtonClick
        Legend = 'OK'
        TextSize = 14
        HasIndicator = False
        IndicatorOn = False
        IsEnabled = True
        ButtonStyle = skNormal
        ArrowColor = clBlack
        ButtonColor = clWhite
        LegendColor = clGreen
        IndicatorWidth = 20
        IndicatorHeight = 10
        IndicatorOnColor = clGreen
        IndicatorOffColor = clSilver
        MaxFontSize = 0
      end
      object CancelButton: TTouchSoftkey
        Left = 142
        Top = 2
        Width = 97
        Height = 45
        Caption = 'CancelButton'
        Font.Charset = ANSI_CHARSET
        Font.Color = clRed
        Font.Height = -19
        Font.Name = 'Comic Sans MS'
        Font.Style = []
        ParentFont = False
        TabOrder = 1
        OnClick = CancelButtonClick
        Legend = 'Cancel'
        TextSize = 14
        HasIndicator = False
        IndicatorOn = False
        IsEnabled = True
        ButtonStyle = skNormal
        ArrowColor = clBlack
        ButtonColor = clWhite
        LegendColor = clRed
        IndicatorWidth = 20
        IndicatorHeight = 10
        IndicatorOnColor = clGreen
        IndicatorOffColor = clSilver
        MaxFontSize = 0
      end
    end
    object FindPanel: TPanel
      Left = 2
      Top = 52
      Width = 241
      Height = 50
      Align = alTop
      BevelInner = bvLowered
      TabOrder = 0
      object FindandReplaceButton: TSpeedButton
        Left = 112
        Top = 2
        Width = 114
        Height = 45
        AllowAllUp = True
        GroupIndex = 2
        Caption = 'Find and Replace'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        Glyph.Data = {
          EE050000424DEE05000000000000360400002800000014000000160000000100
          080000000000B801000000000000000000000001000000010000000000000000
          80000080000000808000800000008000800080800000C0C0C000C0DCC000F0CA
          A60004040400080808000C0C0C0011111100161616001C1C1C00222222002929
          2900555555004D4D4D004242420039393900807CFF005050FF009300D600FFEC
          CC00C6D6EF00D6E7E70090A9AD000000330000006600000099000000CC000033
          00000033330000336600003399000033CC000033FF0000660000006633000066
          6600006699000066CC000066FF00009900000099330000996600009999000099
          CC000099FF0000CC000000CC330000CC660000CC990000CCCC0000CCFF0000FF
          660000FF990000FFCC00330000003300330033006600330099003300CC003300
          FF00333300003333330033336600333399003333CC003333FF00336600003366
          330033666600336699003366CC003366FF003399000033993300339966003399
          99003399CC003399FF0033CC000033CC330033CC660033CC990033CCCC0033CC
          FF0033FF330033FF660033FF990033FFCC0033FFFF0066000000660033006600
          6600660099006600CC006600FF00663300006633330066336600663399006633
          CC006633FF00666600006666330066666600666699006666CC00669900006699
          330066996600669999006699CC006699FF0066CC000066CC330066CC990066CC
          CC0066CCFF0066FF000066FF330066FF990066FFCC00CC00FF00FF00CC009999
          000099339900990099009900CC009900000099333300990066009933CC009900
          FF00996600009966330099336600996699009966CC009933FF00999933009999
          6600999999009999CC009999FF0099CC000099CC330066CC660099CC990099CC
          CC0099CCFF0099FF000099FF330099CC660099FF990099FFCC0099FFFF00CC00
          000099003300CC006600CC009900CC00CC0099330000CC333300CC336600CC33
          9900CC33CC00CC33FF00CC660000CC66330099666600CC669900CC66CC009966
          FF00CC990000CC993300CC996600CC999900CC99CC00CC99FF00CCCC0000CCCC
          3300CCCC6600CCCC9900CCCCCC00CCCCFF00CCFF0000CCFF330099FF6600CCFF
          9900CCFFCC00CCFFFF00CC003300FF006600FF009900CC330000FF333300FF33
          6600FF339900FF33CC00FF33FF00FF660000FF663300CC666600FF669900FF66
          CC00CC66FF00FF990000FF993300FF996600FF999900FF99CC00FF99FF00FFCC
          0000FFCC3300FFCC6600FFCC9900FFCCCC00FFCCFF00FFFF3300CCFF6600FFFF
          9900FFFFCC006666FF0066FF660066FFFF00FF666600FF66FF00FFFF66002100
          A5005F5F5F00777777008686860096969600CBCBCB00B2B2B200D7D7D700DDDD
          DD00E3E3E300EAEAEA00F1F1F100F8F8F800F0FBFF00A4A0A000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00070707070707
          0707070707070707070707070707070707070707070707070707070707070707
          0707070707070707070707070707070707070707070707070707070707070707
          0707070707070707070707070707070707070707070707070707070707070707
          0707070707070707070707070707070707070707070707070707070707070707
          0707070707070707070707070707070707070707070707070707070000000000
          07070707070000000000070707070700FF000000070707070700FF0000000707
          07070700FF000000070707070700FF0000000707070707000000000000000700
          00000000000007070707070000FF000000000000FF0000000000070707070700
          00FF000000070000FF000000000007070707070000FF000000070000FF000000
          000007070707070700000000000000000000000000070707070707070700FF00
          00000700FF000000070707070707070707000000000007000000000007070707
          070707070707000000070707000000070707070707070707070700FF00070707
          00FF000707070707070707070707000000070707000000070707070707070707
          070707070707070707070707070707070707}
        Layout = blGlyphTop
        ParentFont = False
        OnClick = FindButtonClick
      end
      object FindButton: TSpeedButton
        Left = 2
        Top = 2
        Width = 103
        Height = 45
        AllowAllUp = True
        GroupIndex = 2
        Caption = 'Find'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        Glyph.Data = {
          EE050000424DEE05000000000000360400002800000014000000160000000100
          080000000000B801000000000000000000000001000000010000000000000000
          80000080000000808000800000008000800080800000C0C0C000C0DCC000F0CA
          A60004040400080808000C0C0C0011111100161616001C1C1C00222222002929
          2900555555004D4D4D004242420039393900807CFF005050FF009300D600FFEC
          CC00C6D6EF00D6E7E70090A9AD000000330000006600000099000000CC000033
          00000033330000336600003399000033CC000033FF0000660000006633000066
          6600006699000066CC000066FF00009900000099330000996600009999000099
          CC000099FF0000CC000000CC330000CC660000CC990000CCCC0000CCFF0000FF
          660000FF990000FFCC00330000003300330033006600330099003300CC003300
          FF00333300003333330033336600333399003333CC003333FF00336600003366
          330033666600336699003366CC003366FF003399000033993300339966003399
          99003399CC003399FF0033CC000033CC330033CC660033CC990033CCCC0033CC
          FF0033FF330033FF660033FF990033FFCC0033FFFF0066000000660033006600
          6600660099006600CC006600FF00663300006633330066336600663399006633
          CC006633FF00666600006666330066666600666699006666CC00669900006699
          330066996600669999006699CC006699FF0066CC000066CC330066CC990066CC
          CC0066CCFF0066FF000066FF330066FF990066FFCC00CC00FF00FF00CC009999
          000099339900990099009900CC009900000099333300990066009933CC009900
          FF00996600009966330099336600996699009966CC009933FF00999933009999
          6600999999009999CC009999FF0099CC000099CC330066CC660099CC990099CC
          CC0099CCFF0099FF000099FF330099CC660099FF990099FFCC0099FFFF00CC00
          000099003300CC006600CC009900CC00CC0099330000CC333300CC336600CC33
          9900CC33CC00CC33FF00CC660000CC66330099666600CC669900CC66CC009966
          FF00CC990000CC993300CC996600CC999900CC99CC00CC99FF00CCCC0000CCCC
          3300CCCC6600CCCC9900CCCCCC00CCCCFF00CCFF0000CCFF330099FF6600CCFF
          9900CCFFCC00CCFFFF00CC003300FF006600FF009900CC330000FF333300FF33
          6600FF339900FF33CC00FF33FF00FF660000FF663300CC666600FF669900FF66
          CC00CC66FF00FF990000FF993300FF996600FF999900FF99CC00FF99FF00FFCC
          0000FFCC3300FFCC6600FFCC9900FFCCCC00FFCCFF00FFFF3300CCFF6600FFFF
          9900FFFFCC006666FF0066FF660066FFFF00FF666600FF66FF00FFFF66002100
          A5005F5F5F00777777008686860096969600CBCBCB00B2B2B200D7D7D700DDDD
          DD00E3E3E300EAEAEA00F1F1F100F8F8F800F0FBFF00A4A0A000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00070707070707
          0707070707070707070707070707070707070707070707070707070707070707
          0707070707070707070707070707070707070707070707070707070707070707
          0707070707070707070707070707070707070707070707070707070707070707
          0707070707070707070707070707070707070707070707070707070707070707
          0707070707070707070707070707070707070707070707070707070000000000
          07070707070000000000070707070700FF000000070707070700FF0000000707
          07070700FF000000070707070700FF0000000707070707000000000000000700
          00000000000007070707070000FF000000000000FF0000000000070707070700
          00FF000000070000FF000000000007070707070000FF000000070000FF000000
          000007070707070700000000000000000000000000070707070707070700FF00
          00000700FF000000070707070707070707000000000007000000000007070707
          070707070707000000070707000000070707070707070707070700FF00070707
          00FF000707070707070707070707000000070707000000070707070707070707
          070707070707070707070707070707070707}
        Layout = blGlyphTop
        ParentFont = False
        OnClick = FindButtonClick
      end
    end
    object FindModePanel: TPanel
      Left = 2
      Top = 102
      Width = 241
      Height = 286
      Align = alClient
      BevelInner = bvLowered
      TabOrder = 2
      object SearchPages: TPageControl
        Left = 2
        Top = 2
        Width = 237
        Height = 282
        ActivePage = FindSheet
        Align = alClient
        TabHeight = 10
        TabOrder = 0
        object FindSheet: TTabSheet
          Caption = 'FindSheet'
          TabVisible = False
          object FindTextEdit: TSHCursorEdit
            Left = 4
            Top = 8
            Width = 225
            Height = 26
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -16
            Font.Name = 'Verdana'
            Font.Style = []
            ParentFont = False
            TabOrder = 0
            OnKeyUp = FindTextEditKeyUp
          end
          object SoptionsPanel: TPanel
            Left = 0
            Top = 74
            Width = 229
            Height = 198
            Align = alBottom
            TabOrder = 1
            object FindWordButton: TSpeedButton
              Left = 3
              Top = 65
              Width = 110
              Height = 50
              AllowAllUp = True
              GroupIndex = 3
              Caption = 'Whole Word'
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              Glyph.Data = {
                76010000424D7601000000000000760000002800000020000000100000000100
                04000000000000010000130B0000130B00001000000000000000000000000000
                800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
                FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
                333333333333333333FF33333333333330003FF3FFFFF3333777003000003333
                300077F777773F333777E00BFBFB033333337773333F7F33333FE0BFBF000333
                330077F3337773F33377E0FBFBFBF033330077F3333FF7FFF377E0BFBF000000
                333377F3337777773F3FE0FBFBFBFBFB039977F33FFFFFFF7377E0BF00000000
                339977FF777777773377000BFB03333333337773FF733333333F333000333333
                3300333777333333337733333333333333003333333333333377333333333333
                333333333333333333FF33333333333330003333333333333777333333333333
                3000333333333333377733333333333333333333333333333333}
              Layout = blGlyphTop
              NumGlyphs = 2
              ParentFont = False
              OnClick = FindWordButtonClick
            end
            object MatchCaseButton: TSpeedButton
              Left = 115
              Top = 65
              Width = 110
              Height = 50
              AllowAllUp = True
              GroupIndex = 4
              Caption = 'Match Case'
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              Glyph.Data = {
                76010000424D7601000000000000760000002800000020000000100000000100
                04000000000000010000130B0000130B00001000000000000000000000000000
                800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
                FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
                333333333333333333FF33333333333330003FF3FFFFF3333777003000003333
                300077F777773F333777E00BFBFB033333337773333F7F33333FE0BFBF000333
                330077F3337773F33377E0FBFBFBF033330077F3333FF7FFF377E0BFBF000000
                333377F3337777773F3FE0FBFBFBFBFB039977F33FFFFFFF7377E0BF00000000
                339977FF777777773377000BFB03333333337773FF733333333F333000333333
                3300333777333333337733333333333333003333333333333377333333333333
                333333333333333333FF33333333333330003333333333333777333333333333
                3000333333333333377733333333333333333333333333333333}
              Layout = blGlyphTop
              NumGlyphs = 2
              ParentFont = False
              OnClick = MatchCaseButtonClick
              OnDblClick = PartTypeEditEnter
            end
            object FWdButton: TSpeedButton
              Left = 3
              Top = 128
              Width = 110
              Height = 50
              GroupIndex = 5
              Down = True
              Caption = 'Forwards'
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              Glyph.Data = {
                76010000424D7601000000000000760000002800000020000000100000000100
                04000000000000010000120B0000120B00001000000000000000000000000000
                800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
                FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333033333
                33333333373F33333333333330B03333333333337F7F33333333333330F03333
                333333337F7FF3333333333330B00333333333337F773FF33333333330F0F003
                333333337F7F773F3333333330B0B0B0333333337F7F7F7F3333333300F0F0F0
                333333377F73737F33333330B0BFBFB03333337F7F33337F33333330F0FBFBF0
                3333337F7333337F33333330BFBFBFB033333373F3333373333333330BFBFB03
                33333337FFFFF7FF3333333300000000333333377777777F333333330EEEEEE0
                33333337FFFFFF7FF3333333000000000333333777777777F33333330000000B
                03333337777777F7F33333330000000003333337777777773333}
              Layout = blGlyphTop
              NumGlyphs = 2
              ParentFont = False
              OnClick = FWdButtonClick
            end
            object BackwardButton: TSpeedButton
              Left = 115
              Top = 128
              Width = 110
              Height = 50
              GroupIndex = 5
              Caption = 'Backwards'
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              Glyph.Data = {
                76010000424D7601000000000000760000002800000020000000100000000100
                04000000000000010000120B0000120B00001000000000000000000000000000
                800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
                FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333000000000
                333333777777777F33333330B00000003333337F7777777F3333333000000000
                333333777777777F333333330EEEEEE033333337FFFFFF7F3333333300000000
                333333377777777F3333333330BFBFB03333333373333373F33333330BFBFBFB
                03333337F33333F7F33333330FBFBF0F03333337F33337F7F33333330BFBFB0B
                03333337F3F3F7F7333333330F0F0F0033333337F7F7F773333333330B0B0B03
                3333333737F7F7F333333333300F0F03333333337737F7F33333333333300B03
                333333333377F7F33333333333330F03333333333337F7F33333333333330B03
                3333333333373733333333333333303333333333333373333333}
              Layout = blGlyphTop
              NumGlyphs = 2
              ParentFont = False
              OnClick = BackwardButtonClick
            end
            object FindAgainButton: TSpeedButton
              Left = 8
              Top = 10
              Width = 110
              Height = 50
              AllowAllUp = True
              Caption = 'Find Next'
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              Glyph.Data = {
                9E060000424D9E0600000000000036040000280000001B000000160000000100
                0800000000006802000000000000000000000001000000010000000000000000
                80000080000000808000800000008000800080800000C0C0C000C0DCC000F0CA
                A60004040400080808000C0C0C0011111100161616001C1C1C00222222002929
                2900555555004D4D4D004242420039393900807CFF005050FF009300D600FFEC
                CC00C6D6EF00D6E7E70090A9AD000000330000006600000099000000CC000033
                00000033330000336600003399000033CC000033FF0000660000006633000066
                6600006699000066CC000066FF00009900000099330000996600009999000099
                CC000099FF0000CC000000CC330000CC660000CC990000CCCC0000CCFF0000FF
                660000FF990000FFCC00330000003300330033006600330099003300CC003300
                FF00333300003333330033336600333399003333CC003333FF00336600003366
                330033666600336699003366CC003366FF003399000033993300339966003399
                99003399CC003399FF0033CC000033CC330033CC660033CC990033CCCC0033CC
                FF0033FF330033FF660033FF990033FFCC0033FFFF0066000000660033006600
                6600660099006600CC006600FF00663300006633330066336600663399006633
                CC006633FF00666600006666330066666600666699006666CC00669900006699
                330066996600669999006699CC006699FF0066CC000066CC330066CC990066CC
                CC0066CCFF0066FF000066FF330066FF990066FFCC00CC00FF00FF00CC009999
                000099339900990099009900CC009900000099333300990066009933CC009900
                FF00996600009966330099336600996699009966CC009933FF00999933009999
                6600999999009999CC009999FF0099CC000099CC330066CC660099CC990099CC
                CC0099CCFF0099FF000099FF330099CC660099FF990099FFCC0099FFFF00CC00
                000099003300CC006600CC009900CC00CC0099330000CC333300CC336600CC33
                9900CC33CC00CC33FF00CC660000CC66330099666600CC669900CC66CC009966
                FF00CC990000CC993300CC996600CC999900CC99CC00CC99FF00CCCC0000CCCC
                3300CCCC6600CCCC9900CCCCCC00CCCCFF00CCFF0000CCFF330099FF6600CCFF
                9900CCFFCC00CCFFFF00CC003300FF006600FF009900CC330000FF333300FF33
                6600FF339900FF33CC00FF33FF00FF660000FF663300CC666600FF669900FF66
                CC00CC66FF00FF990000FF993300FF996600FF999900FF99CC00FF99FF00FFCC
                0000FFCC3300FFCC6600FFCC9900FFCCCC00FFCCFF00FFFF3300CCFF6600FFFF
                9900FFFFCC006666FF0066FF660066FFFF00FF666600FF66FF00FFFF66002100
                A5005F5F5F00777777008686860096969600CBCBCB00B2B2B200D7D7D700DDDD
                DD00E3E3E300EAEAEA00F1F1F100F8F8F800F0FBFF00A4A0A000808080000000
                FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00070707070707
                070707070707070707070707070707A10707070707FF07070707070707070707
                070707070707070707070707A107070707000707070707070707070707070707
                070707070707070707A107070700070707070707070707070707070707070707
                070707070707A107070007070707070707A1A1A1A1A1A1A1A1A1A1A1A1A1A1A1
                A1A1A1A10700070707070707070707070707070707070707070707070707A107
                07000707070707070707070707070707070707070707070707A1070707000707
                0707070707070707070707070707070707070707A10707070700070700000000
                000707070707000000000007070707A1070707070700070700FF000000070707
                070700FF0000000707070707070707070700070700FF000000070707070700FF
                0000000707070707070707070700070700000000000000070000000000000007
                0707070707070707070007070000FF000000000000FF00000000000707070707
                07070707070007070000FF000000070000FF0000000000070707070707070707
                070007070000FF000000070000FF000000000007070707070707070707000707
                07000000000000000000000000000707070707070707070707000707070700FF
                0000000700FF0000000707070707070707070707070007070707000000000007
                0000000000070707070707070707070707000707070707000000070707000000
                0707070707070707070707070700070707070700FF0007070700FF0007070707
                0707070707070707070007070707070000000707070000000707070707070707
                0707070707000707070707070707070707070707070707070707070707070707
                0700}
              Layout = blGlyphTop
              ParentFont = False
              OnClick = FindAgainButtonClick
            end
            object FindDoneKey: TTouchSoftkey
              Left = 128
              Top = 16
              Width = 97
              Height = 41
              Font.Charset = ANSI_CHARSET
              Font.Color = clGreen
              Font.Height = -19
              Font.Name = 'Comic Sans MS'
              Font.Style = []
              ParentFont = False
              TabOrder = 0
              OnMouseUp = FindDoneKeyMouseUp
              Legend = 'DONE'
              TextSize = 14
              HasIndicator = False
              IndicatorOn = False
              IsEnabled = True
              ButtonStyle = skNormal
              ArrowColor = clBlack
              ButtonColor = clWhite
              LegendColor = clBlack
              IndicatorWidth = 20
              IndicatorHeight = 10
              IndicatorOnColor = clGreen
              IndicatorOffColor = clSilver
              MaxFontSize = 0
            end
          end
        end
        object ReplaceSheet: TTabSheet
          Caption = 'ReplaceSheet'
          ImageIndex = 1
          TabVisible = False
          object ReplaceTextEdit: TSHCursorEdit
            Left = 4
            Top = 48
            Width = 225
            Height = 26
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -16
            Font.Name = 'Verdana'
            Font.Style = []
            ParentFont = False
            TabOrder = 0
            OnKeyUp = FindTextEditKeyUp
          end
        end
        object logoSheet: TTabSheet
          ImageIndex = 2
          TabVisible = False
        end
      end
    end
    object UndoControls: TPanel
      Left = 2
      Top = 388
      Width = 241
      Height = 160
      Align = alBottom
      BevelInner = bvLowered
      TabOrder = 3
      object UndoPanel: TPanel
        Left = 2
        Top = 44
        Width = 237
        Height = 50
        Align = alBottom
        TabOrder = 0
        object UndoKey: TSpeedButton
          Left = 2
          Top = 4
          Width = 115
          Height = 45
          AllowAllUp = True
          Caption = 'UNDO'
          Enabled = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            04000000000000010000130B0000130B00001000000000000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
            3333333333FFFFF3333333333999993333333333F77777FFF333333999999999
            3333333777333777FF3333993333339993333377FF3333377FF3399993333339
            993337777FF3333377F3393999333333993337F777FF333337FF993399933333
            399377F3777FF333377F993339993333399377F33777FF33377F993333999333
            399377F333777FF3377F993333399933399377F3333777FF377F993333339993
            399377FF3333777FF7733993333339993933373FF3333777F7F3399933333399
            99333773FF3333777733339993333339933333773FFFFFF77333333999999999
            3333333777333777333333333999993333333333377777333333}
          Layout = blGlyphTop
          NumGlyphs = 2
          ParentFont = False
          OnClick = UndoKeyClick
        end
        object RedoKey: TSpeedButton
          Left = 119
          Top = 4
          Width = 115
          Height = 45
          AllowAllUp = True
          Caption = 'REDO'
          Enabled = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            04000000000000010000130B0000130B00001000000000000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
            3333333333FFFFF3333333333999993333333333F77777FFF333333999999999
            3333333777333777FF33339993707399933333773337F3777FF3399933000339
            9933377333777F3377F3399333707333993337733337333337FF993333333333
            399377F33333F333377F993333303333399377F33337FF333373993333707333
            333377F333777F333333993333101333333377F333777F3FFFFF993333000399
            999377FF33777F77777F3993330003399993373FF3777F37777F399933000333
            99933773FF777F3F777F339993707399999333773F373F77777F333999999999
            3393333777333777337333333999993333333333377777333333}
          Layout = blGlyphTop
          NumGlyphs = 2
          ParentFont = False
          OnClick = RedoKeyClick
        end
      end
      object topBottomPanel: TPanel
        Left = 2
        Top = 94
        Width = 237
        Height = 64
        Align = alBottom
        TabOrder = 1
        object TopKey: TKBKey
          Left = 1
          Top = 1
          Width = 60
          Height = 60
          Beveled = True
          BorderStyle = bsSingle
          ButtonDirection = bdBottomUp
          MainCaption = 'Top'
          Color = clTeal
          ColorLED = lcBlue
          Depth = 3
          Font.Charset = ANSI_CHARSET
          Font.Color = clYellow
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ShiftFont.Charset = DEFAULT_CHARSET
          ShiftFont.Color = clWindowText
          ShiftFont.Height = -11
          ShiftFont.Name = 'MS Sans Serif'
          ShiftFont.Style = []
          ShowLED = False
          StateOn = False
          Switching = True
          ShowShiftedCharacter = False
          CanRepeat = False
          Temp = False
          OnMouseDown = TopKeyMouseDown
        end
        object BottomKey: TKBKey
          Left = 61
          Top = 1
          Width = 60
          Height = 60
          Beveled = True
          BorderStyle = bsSingle
          ButtonDirection = bdBottomUp
          MainCaption = 'Bottom'
          Color = clTeal
          ColorLED = lcBlue
          Depth = 3
          Font.Charset = ANSI_CHARSET
          Font.Color = clYellow
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ShiftFont.Charset = DEFAULT_CHARSET
          ShiftFont.Color = clWindowText
          ShiftFont.Height = -11
          ShiftFont.Name = 'MS Sans Serif'
          ShiftFont.Style = []
          ShowLED = False
          StateOn = False
          Switching = True
          ShowShiftedCharacter = False
          CanRepeat = False
          Temp = False
          OnClick = BottomKeyClick
          OnMouseDown = TopKeyMouseDown
        end
        object BookMarkKey: TTouchSoftkey
          Left = 126
          Top = 2
          Width = 111
          Height = 60
          Caption = 'BookmarkKey'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Comic Sans MS'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 0
          OnMouseUp = BookMarkKeyMouseUp
          Legend = 'BookMark~Control'
          TextSize = 14
          HasIndicator = False
          IndicatorOn = False
          IsEnabled = True
          ButtonStyle = skNormal
          ArrowColor = clBlack
          ButtonColor = clWhite
          LegendColor = clBlack
          IndicatorWidth = 20
          IndicatorHeight = 10
          IndicatorOnColor = clGreen
          IndicatorOffColor = clSilver
          MaxFontSize = 0
        end
      end
      object Panel7: TPanel
        Left = 2
        Top = 2
        Width = 237
        Height = 41
        Align = alTop
        BevelInner = bvLowered
        TabOrder = 2
        object BtnHideKB: TTouchGlyphSoftkey
          Left = 2
          Top = 2
          Width = 100
          Height = 37
          Legend = 'Hide K/B'
          TextSize = 0
          HasIndicator = False
          IndicatorOn = False
          IsEnabled = True
          ButtonColor = clInfoBk
          Layout = slGlyphLeft
          AutoTextSize = True
          KeyStyle = ssRectangle
          CornerRad = 10
          ButtonTravel = 2
          ShadowColor = clGray
          LegendFont.Charset = DEFAULT_CHARSET
          LegendFont.Color = clBlue
          LegendFont.Height = 23
          LegendFont.Name = 'Comic Sans MS'
          LegendFont.Style = []
          GlyphToEdgeSpacing = 1
          TextToEdgeBorder = 7
          MultiLineSpacing = 0
          MaxFontSize = 24
          OnMouseUp = BtnHideKBMouseUp
          align = alLeft
        end
        object BtnHelp: TTouchGlyphSoftkey
          Left = 135
          Top = 2
          Width = 100
          Height = 37
          Legend = 'Help (F1)'
          TextSize = 0
          HasIndicator = False
          IndicatorOn = False
          IsEnabled = True
          ButtonColor = clInfoBk
          Layout = slGlyphLeft
          AutoTextSize = True
          KeyStyle = ssRectangle
          CornerRad = 10
          ButtonTravel = 2
          ShadowColor = clGray
          LegendFont.Charset = DEFAULT_CHARSET
          LegendFont.Color = clGreen
          LegendFont.Height = 23
          LegendFont.Name = 'Comic Sans MS'
          LegendFont.Style = []
          GlyphToEdgeSpacing = 1
          TextToEdgeBorder = 7
          MultiLineSpacing = 0
          MaxFontSize = 24
          OnMouseUp = BtnHelpMouseUp
          align = alRight
        end
      end
    end
  end
  object OPPHeaderPanel: TPanel
    Left = -500
    Top = 16
    Width = 1000
    Height = 450
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    object PageControl1: TPageControl
      Left = 1
      Top = 1
      Width = 998
      Height = 448
      ActivePage = TabOPPHeader
      Align = alClient
      TabOrder = 0
      object TabOPPHeader: TTabSheet
        TabVisible = False
        object OPPPartPanel: TPanel
          Left = 0
          Top = 0
          Width = 990
          Height = 438
          Align = alClient
          BevelInner = bvLowered
          TabOrder = 0
          object PartTypeLabel: TLabel
            Left = 36
            Top = 16
            Width = 80
            Height = 24
            Alignment = taRightJustify
            Caption = 'Part Type'
            FocusControl = PartTypeEdit
          end
          object ReferenceLabel: TLabel
            Left = 36
            Top = 58
            Width = 88
            Height = 24
            Alignment = taRightJustify
            Caption = 'Reference'
            FocusControl = ReferenceEdit
          end
          object ToolTYpeLabel: TLabel
            Left = 36
            Top = 100
            Width = 86
            Height = 24
            Alignment = taRightJustify
            Caption = 'Tool Type'
            FocusControl = ToolTypeEdit
          end
          object ElectrodeCountLabel: TLabel
            Left = 36
            Top = 141
            Width = 136
            Height = 24
            Alignment = taRightJustify
            Caption = 'Electrode Count'
            FocusControl = ElectrodeCountEdit
          end
          object ToolUsageLabel: TLabel
            Left = 536
            Top = 100
            Width = 97
            Height = 24
            Alignment = taRightJustify
            Caption = 'Tool Usage'
          end
          object CycleTimeLabel: TLabel
            Left = 536
            Top = 141
            Width = 95
            Height = 24
            Alignment = taRightJustify
            Caption = 'Cycle Time'
          end
          object Label3: TLabel
            Left = 699
            Top = 141
            Width = 30
            Height = 24
            Alignment = taRightJustify
            Caption = 'sec'
          end
          object HeaderOKButton: TSpeedButton
            Left = 497
            Top = 297
            Width = 100
            Height = 48
            Caption = 'OK'
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -16
            Font.Name = 'Verdana'
            Font.Style = [fsBold]
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            OnClick = OKButtonClick
          end
          object HeaderCancelButton: TSpeedButton
            Left = 598
            Top = 297
            Width = 100
            Height = 48
            Caption = 'Cancel'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -16
            Font.Name = 'Verdana'
            Font.Style = [fsBold]
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            OnClick = HeaderCancelButtonClick
          end
          object ElectrodeMapLabel: TLabel
            Left = 36
            Top = 183
            Width = 123
            Height = 24
            Alignment = taRightJustify
            Caption = 'Electrode Map'
            FocusControl = ElectrodeCountEdit
          end
          object Label6: TLabel
            Left = 178
            Top = 184
            Width = 10
            Height = 24
            Caption = '$'
          end
          object HexFault: TLabel
            Left = 384
            Top = 184
            Width = 313
            Height = 24
            Caption = 'Must be valid Hex Number (12 Digit)'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clRed
            Font.Height = -19
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsItalic]
            ParentFont = False
          end
          object ToolFault: TLabel
            Left = 336
            Top = 104
            Width = 157
            Height = 24
            Caption = '8 Characters Max'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clRed
            Font.Height = -19
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsItalic]
            ParentFont = False
          end
          object OPNumberLabel: TLabel
            Left = 358
            Top = 236
            Width = 100
            Height = 24
            Alignment = taRightJustify
            Caption = 'Op Number'
            FocusControl = ToolTypeEdit
          end
          object OpNumberFaultLabel: TLabel
            Left = 560
            Top = 240
            Width = 141
            Height = 24
            Caption = 'Must be integer'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clRed
            Font.Height = -19
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsItalic]
            ParentFont = False
            Visible = False
          end
          object ECountFaultLabel: TLabel
            Left = 272
            Top = 144
            Width = 141
            Height = 24
            Caption = 'Must be Integer'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clRed
            Font.Height = -19
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsItalic]
            ParentFont = False
            Visible = False
          end
          object PartTypeEdit: TEdit
            Left = 191
            Top = 12
            Width = 506
            Height = 32
            TabOrder = 0
            Text = 'PartTypeEdit'
            OnEnter = PartTypeEditEnter
          end
          object ReferenceEdit: TEdit
            Left = 191
            Top = 54
            Width = 506
            Height = 32
            TabOrder = 1
            Text = 'ReferenceEdit'
            OnEnter = PartTypeEditEnter
          end
          object ToolTypeEdit: TEdit
            Left = 191
            Top = 96
            Width = 138
            Height = 32
            TabOrder = 2
            OnEnter = PartTypeEditEnter
          end
          object ElectrodeCountEdit: TEdit
            Left = 191
            Top = 137
            Width = 59
            Height = 32
            TabOrder = 3
            Text = '1'
            OnChange = ElectrodeCountEditChange
            OnEnter = PartTypeEditEnter
          end
          object ToolUsageEdit: TEdit
            Left = 636
            Top = 96
            Width = 59
            Height = 32
            TabOrder = 4
            Text = '0'
            OnEnter = PartTypeEditEnter
          end
          object CycleTimeEdit: TEdit
            Left = 636
            Top = 137
            Width = 59
            Height = 32
            TabOrder = 5
            Text = '0'
            OnEnter = PartTypeEditEnter
          end
          object ElectrodeMapEdit: TEdit
            Left = 191
            Top = 179
            Width = 162
            Height = 32
            TabOrder = 6
            OnEnter = PartTypeEditEnter
          end
          object ToolOffsetPanel: TPanel
            Left = 24
            Top = 224
            Width = 305
            Height = 129
            BevelInner = bvLowered
            TabOrder = 7
            object ToolXOffsetLabel: TLabel
              Left = 12
              Top = 9
              Width = 109
              Height = 24
              Caption = 'Tool X Offset'
            end
            object ToolYOffsetLabel: TLabel
              Left = 12
              Top = 49
              Width = 107
              Height = 24
              Caption = 'Tool Y Offset'
            end
            object ToolZOffsetLabel: TLabel
              Left = 12
              Top = 89
              Width = 107
              Height = 24
              Caption = 'Tool Z Offset'
            end
            object ToolXOffsetEdit: TEdit
              Left = 168
              Top = 5
              Width = 121
              Height = 32
              TabOrder = 0
              OnEnter = PartTypeEditEnter
            end
            object ToolYOffsetEdit: TEdit
              Left = 168
              Top = 45
              Width = 121
              Height = 32
              TabOrder = 1
              OnEnter = PartTypeEditEnter
            end
            object ToolZOffsetEdit: TEdit
              Left = 168
              Top = 85
              Width = 121
              Height = 32
              TabOrder = 2
              OnEnter = PartTypeEditEnter
            end
          end
          object OpNumberEdit: TEdit
            Left = 479
            Top = 233
            Width = 59
            Height = 32
            TabOrder = 8
            Text = '1'
            OnEnter = PartTypeEditEnter
          end
          object AECPanel: TPanel
            Left = 822
            Top = 2
            Width = 160
            Height = 199
            BevelInner = bvLowered
            BevelWidth = 2
            TabOrder = 9
            object HeaderAECEdit: TSpeedButton
              Left = 25
              Top = 9
              Width = 100
              Height = 48
              Caption = 'Edit AEC'
              Enabled = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -16
              Font.Name = 'Verdana'
              Font.Style = [fsBold]
              ParentFont = False
              ParentShowHint = False
              ShowHint = True
              OnClick = HeaderAECEditClick
            end
            object AECCheck: TCheckBox
              Left = 10
              Top = 88
              Width = 135
              Height = 17
              Caption = 'AEC Enable'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -19
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 0
              OnClick = AECCheckClick
              OnEnter = AECCheckEnter
            end
          end
        end
      end
      object TabAECParams: TTabSheet
        ImageIndex = 1
        TabVisible = False
        object AECBackPanel: TPanel
          Left = 0
          Top = 0
          Width = 990
          Height = 438
          Align = alClient
          BevelInner = bvLowered
          BevelWidth = 2
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 0
          object AECHelpPanel: TPanel
            Left = 4
            Top = 404
            Width = 982
            Height = 30
            Align = alBottom
            BevelInner = bvLowered
            Color = clMoneyGreen
            TabOrder = 1
          end
          object AECGeneralPanel: TPanel
            Left = 4
            Top = 39
            Width = 829
            Height = 365
            Align = alLeft
            BevelInner = bvLowered
            BevelWidth = 2
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 0
            object AECLeftpanel: TPanel
              Left = 4
              Top = 4
              Width = 250
              Height = 357
              Align = alLeft
              TabOrder = 0
              object AECLabelHoleProgram: TLabel
                Left = 16
                Top = 120
                Width = 119
                Height = 16
                Caption = 'Hole Program ***'
              end
              object AECLabelUFeed: TLabel
                Left = 16
                Top = 180
                Width = 97
                Height = 16
                Caption = 'Axis Feedrate'
              end
              object AECLabelRestrokeDist: TLabel
                Left = 16
                Top = 240
                Width = 129
                Height = 16
                Caption = 'Restroke Distance'
              end
              object AECGenPanTitle: TPanel
                Left = 1
                Top = 1
                Width = 248
                Height = 30
                Align = alTop
                BevelInner = bvLowered
                Caption = 'General'
                Color = clSkyBlue
                TabOrder = 0
              end
              object AECInchMetricButton: TTouchSoftkey
                Tag = 13
                Left = 16
                Top = 48
                Width = 120
                Height = 41
                Caption = 'AECInchMetricButton'
                TabOrder = 1
                OnMouseUp = AECInchMetricButtonMouseUp
                Legend = 'Inch/Metric'
                TextSize = 14
                HasIndicator = False
                IndicatorOn = False
                IsEnabled = True
                ButtonStyle = skNormal
                ArrowColor = clBlack
                ButtonColor = clWhite
                LegendColor = clBlue
                IndicatorWidth = 20
                IndicatorHeight = 10
                IndicatorOnColor = clGreen
                IndicatorOffColor = clSilver
                MaxFontSize = 0
              end
              object AECDataInchMetricDisplay: TEdit
                Left = 168
                Top = 56
                Width = 70
                Height = 24
                ReadOnly = True
                TabOrder = 2
                Text = 'Metric'
                OnEnter = AEC_FieldEnter
                OnExit = AEC_FieldExit
                OnKeyDown = AECDataKeyDown
              end
              object AECDataCheckHoleProgram: TEdit
                Tag = 1
                Left = 168
                Top = 116
                Width = 70
                Height = 24
                TabOrder = 3
                OnEnter = AEC_FieldEnter
                OnExit = AEC_FieldExit
                OnKeyDown = AECDataKeyDown
              end
              object AECDataStowFeed: TEdit
                Tag = 2
                Left = 168
                Top = 176
                Width = 70
                Height = 24
                TabOrder = 4
                OnEnter = AEC_FieldEnter
                OnExit = AEC_FieldExit
                OnKeyDown = AECDataKeyDown
              end
              object AECDataRestroke: TEdit
                Tag = 3
                Left = 168
                Top = 236
                Width = 70
                Height = 24
                Hint = 'This is a hint'
                TabOrder = 5
                OnEnter = AEC_FieldEnter
                OnExit = AEC_FieldExit
                OnKeyDown = AECDataKeyDown
              end
              object AECDoubleIndexButton: TTouchSoftkey
                Tag = 13
                Left = 16
                Top = 289
                Width = 120
                Height = 41
                Caption = 'TouchSoftkey1'
                TabOrder = 6
                OnMouseUp = AECDoubleIndexButtonMouseUp
                Legend = 'Double Index'
                TextSize = 14
                HasIndicator = False
                IndicatorOn = False
                IsEnabled = True
                ButtonStyle = skNormal
                ArrowColor = clBlack
                ButtonColor = clWhite
                LegendColor = clBlue
                IndicatorWidth = 20
                IndicatorHeight = 10
                IndicatorOnColor = clGreen
                IndicatorOffColor = clSilver
                MaxFontSize = 0
              end
              object AECDoubleIndexDisplay: TEdit
                Tag = 4
                Left = 168
                Top = 297
                Width = 70
                Height = 24
                ReadOnly = True
                TabOrder = 7
                Text = 'No'
                OnEnter = AEC_FieldEnter
                OnExit = AEC_FieldExit
                OnKeyDown = AECDataKeyDown
              end
            end
            object AECMiddlePanel: TPanel
              Left = 254
              Top = 4
              Width = 320
              Height = 357
              Align = alLeft
              Caption = 'AECMiddlePanel'
              TabOrder = 1
              object Panel4: TPanel
                Left = 1
                Top = 1
                Width = 318
                Height = 200
                Align = alTop
                TabOrder = 0
                object AECLabelLoadPulseCount: TLabel
                  Left = 5
                  Top = 42
                  Width = 122
                  Height = 16
                  Caption = 'Load Pulse Count'
                end
                object AECLabelULoadPos: TLabel
                  Left = 5
                  Top = 84
                  Width = 105
                  Height = 16
                  Caption = 'U Axis Position'
                end
                object AECLabelFirstQualifyPulseLength: TLabel
                  Left = 5
                  Top = 127
                  Width = 136
                  Height = 16
                  Caption = 'First Pulse Duration'
                end
                object AECLabelSecondQualifyPulseDuration: TLabel
                  Left = 5
                  Top = 170
                  Width = 159
                  Height = 16
                  Caption = 'Second Pulse Duration'
                end
                object AECLoadTitle: TPanel
                  Left = 1
                  Top = 1
                  Width = 316
                  Height = 30
                  Align = alTop
                  BevelInner = bvLowered
                  Caption = 'Load'
                  Color = clSkyBlue
                  TabOrder = 0
                end
                object AECDataLoadPulseCount: TEdit
                  Tag = 9
                  Left = 186
                  Top = 38
                  Width = 70
                  Height = 24
                  TabOrder = 1
                  OnEnter = AEC_FieldEnter
                  OnExit = AEC_FieldExit
                  OnKeyDown = AECDataKeyDown
                end
                object AECDataLoadPositionU: TEdit
                  Tag = 10
                  Left = 186
                  Top = 80
                  Width = 70
                  Height = 24
                  TabOrder = 2
                  OnEnter = AEC_FieldEnter
                  OnExit = AEC_FieldExit
                  OnKeyDown = AECDataKeyDown
                end
                object AECDataPulse1Duration: TEdit
                  Tag = 11
                  Left = 186
                  Top = 123
                  Width = 70
                  Height = 24
                  TabOrder = 3
                  OnEnter = AEC_FieldEnter
                  OnExit = AEC_FieldExit
                  OnKeyDown = AECDataKeyDown
                end
                object AECDataQualifyPulse2Duration: TEdit
                  Tag = 12
                  Left = 186
                  Top = 166
                  Width = 70
                  Height = 24
                  TabOrder = 4
                  OnEnter = AEC_FieldEnter
                  OnExit = AEC_FieldExit
                  OnKeyDown = AECDataKeyDown
                end
              end
              object Panel5: TPanel
                Left = 1
                Top = 201
                Width = 318
                Height = 155
                Align = alClient
                TabOrder = 1
                object AECLabelQualifyRetryCount: TLabel
                  Left = 8
                  Top = 44
                  Width = 81
                  Height = 16
                  Caption = 'Retry Count'
                end
                object AECLabelQualifyStroke: TLabel
                  Left = 8
                  Top = 80
                  Width = 98
                  Height = 16
                  Caption = 'Qualify Stroke'
                end
                object AECLabelElectrodeClearance: TLabel
                  Left = 8
                  Top = 117
                  Width = 143
                  Height = 16
                  Caption = 'Electrode Clearance'
                end
                object AECQualifyTitle: TPanel
                  Left = 1
                  Top = 1
                  Width = 316
                  Height = 30
                  Align = alTop
                  BevelInner = bvLowered
                  Caption = 'Qualify'
                  Color = clSkyBlue
                  TabOrder = 0
                end
                object AECDataQualifyRetryLoops: TEdit
                  Tag = 13
                  Left = 187
                  Top = 40
                  Width = 70
                  Height = 24
                  TabOrder = 1
                  OnEnter = AEC_FieldEnter
                  OnExit = AEC_FieldExit
                  OnKeyDown = AECDataKeyDown
                end
                object AECDataQualifyStroke: TEdit
                  Tag = 14
                  Left = 187
                  Top = 76
                  Width = 70
                  Height = 24
                  TabOrder = 2
                  OnEnter = AEC_FieldEnter
                  OnExit = AEC_FieldExit
                  OnKeyDown = AECDataKeyDown
                end
                object AECDataElectrodeClearance: TEdit
                  Tag = 15
                  Left = 187
                  Top = 113
                  Width = 70
                  Height = 24
                  TabOrder = 3
                  OnEnter = AEC_FieldEnter
                  OnExit = AEC_FieldExit
                  OnKeyDown = AECDataKeyDown
                end
              end
            end
            object AECRightPanel: TPanel
              Left = 574
              Top = 4
              Width = 251
              Height = 357
              Align = alClient
              TabOrder = 2
              object AECLabelEjectPulseCount: TLabel
                Left = 13
                Top = 64
                Width = 122
                Height = 16
                Caption = 'Eject Pulse Count'
              end
              object AECLabelFirstEjectPulse: TLabel
                Left = 13
                Top = 120
                Width = 136
                Height = 16
                Caption = 'First Pulse Duration'
              end
              object AECLabelSecondEjectPulse: TLabel
                Left = 13
                Top = 176
                Width = 159
                Height = 16
                Caption = 'Second Pulse Duration'
              end
              object AECLabelEjectLength: TLabel
                Left = 13
                Top = 232
                Width = 86
                Height = 16
                Caption = 'Eject Length'
              end
              object AECEjectTitle: TPanel
                Left = 1
                Top = 1
                Width = 249
                Height = 30
                Align = alTop
                BevelInner = bvLowered
                Caption = 'Eject'
                Color = clSkyBlue
                TabOrder = 0
              end
              object AECDataEjectPulseCount: TEdit
                Tag = 5
                Left = 177
                Top = 60
                Width = 70
                Height = 24
                TabOrder = 1
                OnEnter = AEC_FieldEnter
                OnExit = AEC_FieldExit
                OnKeyDown = AECDataKeyDown
              end
              object AECDataEjectPulse1Duration: TEdit
                Tag = 6
                Left = 177
                Top = 116
                Width = 70
                Height = 24
                TabOrder = 2
                OnEnter = AEC_FieldEnter
                OnExit = AEC_FieldExit
                OnKeyDown = AECDataKeyDown
              end
              object AECDataEjectPulse2Duration: TEdit
                Tag = 7
                Left = 177
                Top = 172
                Width = 70
                Height = 24
                TabOrder = 3
                OnEnter = AEC_FieldEnter
                OnExit = AEC_FieldExit
                OnKeyDown = AECDataKeyDown
              end
              object AECDataEjectLength: TEdit
                Tag = 8
                Left = 177
                Top = 228
                Width = 70
                Height = 24
                TabOrder = 4
                OnEnter = AEC_FieldEnter
                OnExit = AEC_FieldExit
                OnKeyDown = AECDataKeyDown
              end
            end
          end
          object OPPHeaderTitle: TPanel
            Left = 4
            Top = 4
            Width = 982
            Height = 35
            Align = alTop
            BevelInner = bvLowered
            BevelWidth = 2
            Caption = 'OPP HEADER DATA'
            Color = clBtnShadow
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlue
            Font.Height = -19
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 2
          end
          object AECButtonPanel: TPanel
            Left = 821
            Top = 39
            Width = 165
            Height = 365
            Align = alRight
            BevelInner = bvLowered
            TabOrder = 3
            object AECBackButton: TSpeedButton
              Left = 13
              Top = 300
              Width = 100
              Height = 48
              Caption = 'OK'
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -16
              Font.Name = 'Verdana'
              Font.Style = [fsBold]
              ParentFont = False
              ParentShowHint = False
              ShowHint = True
              OnClick = AECBackButtonClick
            end
            object AECDefaultButton: TSpeedButton
              Left = 13
              Top = 60
              Width = 100
              Height = 48
              Caption = 'Default'
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -16
              Font.Name = 'Verdana'
              Font.Style = [fsBold]
              ParentFont = False
              ParentShowHint = False
              ShowHint = True
              OnClick = AECDefaultButtonClick
            end
            object AECDataError: TPanel
              Left = 2
              Top = 2
              Width = 161
              Height = 41
              Align = alTop
              BevelInner = bvLowered
              Caption = 'AEC Data Invalid'
              Color = clRed
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clYellow
              Font.Height = -16
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
              TabOrder = 0
            end
          end
        end
      end
    end
  end
  object pp1: TOPPSHEdit
    Left = 498
    Top = 426
    Width = 200
    Height = 100
    ShowBookMarkPanel = False
    FindModeRequested = pp1FindModeRequested
    FindAgainRequested = pp1FindAgainRequested
    F1HelpRequested = pp1F1HelpRequested
    EScapeKeyPressed = CancelButtonClick
    InhibitUpdate = False
    IndicateHelpAvailable = False
  end
  object PanelKB: TPanel
    Left = 0
    Top = 528
    Width = 1016
    Height = 213
    Align = alBottom
    TabOrder = 3
    object MainKb: TBaseKB
      Left = 1
      Top = 1
      Width = 777
      Height = 211
      Align = alLeft
      BevelOuter = bvNone
      Caption = 'MainKb'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -19
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      KeyboardMode = kbmAlphaNumeric
      HighlightColor = clBlack
      Options = []
      SecondaryFont.Charset = DEFAULT_CHARSET
      SecondaryFont.Color = clWindowText
      SecondaryFont.Height = -11
      SecondaryFont.Name = 'MS Sans Serif'
      SecondaryFont.Style = []
      RepeatDelay = 500
      ExternalMode = False
      FocusByHandle = False
      FocusHandle = 0
    end
    object KeyPad: TBaseFPad
      Left = 776
      Top = 10
      Width = 200
      Height = 250
      ExternalMode = False
      ChangeCursorKeys = True
      EnableCursorKeys = True
      object BitBtn1: TBitBtn
        Left = 136
        Top = 0
        Width = 64
        Height = 25
        Caption = 'Browse'
        TabOrder = 0
        TabStop = False
        OnClick = BitBtn1Click
      end
    end
  end
  object FileOPen: TOpenDialog
    DefaultExt = 'opp'
    InitialDir = 'c:\acnc32\programs'
    Left = 976
    Top = 472
  end
  object StartTimer: TTimer
    Enabled = False
    Interval = 400
    OnTimer = StartTimerTimer
    Left = 972
    Top = 497
  end
  object Timer1: TTimer
    OnTimer = Timer1Timer
    Left = 600
    Top = 112
  end
end
