library PartProgram;

{%File '..\..\..\hsd6ii\ExtendedToolManagement\profile\Live\oppedit.ini'}

uses
  SysUtils,
  Classes,
  PluginInterface,
  NamedPlugin,
  PartProgramEditor in 'PartProgramEditor.pas' {GCodeForm},
  FileLoadDialog in 'FileLoadDialog.pas' {FileLoadForm},
  OPPHeader in '..\lib\OPPHeader.pas',
  OPPDoc in 'OPPDoc.pas',
  AreYouSureDialog in 'AreYouSureDialog.pas' {AreYouSure},
  CSVHeader in 'CSVHeader.pas',
  CSVEditor in 'CSVEditor.pas' {CSVForm},
  ACNC_SHeditor in '..\..\DelphiComps\ACNC_SHeditor.pas',
  LinkedLookup in 'LinkedLookup.pas',
  OCTTypes in '..\lib\OCTTypes.pas',
  OCTUtils in '..\lib\OCTUtils.pas',
  Directory in 'Directory.pas',
  CellMode in '..\lib\CellMode.pas' {CellModeForm},
  AccessAdministration in '..\lib\AccessAdministration.pas' {AccessAdministrator};

{$R *.RES}

begin
end.
