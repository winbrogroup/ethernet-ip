object HelpBrowserForm: THelpBrowserForm
  Left = 212
  Top = 12
  Width = 721
  Height = 692
  HorzScrollBar.Visible = False
  Caption = 'ACNC32 Help system'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object HTMLPage: TWebBrowser
    Left = 0
    Top = 0
    Width = 713
    Height = 620
    Align = alClient
    TabOrder = 0
    ControlData = {
      4C000000B1490000144000000000000000000000000000000000000000000000
      000000004C000000000000000000000001000000E0D057007335CF11AE690800
      2B2E126208000000000000004C0000000114020000000000C000000000000046
      8000000000000000000000000000000000000000000000000000000000000000
      00000000000000000100000000000000000000000000000000000000}
  end
  object Panel1: TPanel
    Left = 0
    Top = 620
    Width = 713
    Height = 45
    Align = alBottom
    BevelInner = bvLowered
    TabOrder = 1
    object TouchGlyphSoftkey1: TTouchGlyphSoftkey
      Left = 152
      Top = 2
      Width = 100
      Height = 41
      Legend = 'Back'
      TextSize = 0
      HasIndicator = False
      IndicatorOn = False
      IsEnabled = True
      ButtonColor = clInfoBk
      Layout = slGlyphLeft
      AutoTextSize = True
      KeyStyle = ssRectangle
      CornerRad = 10
      ButtonTravel = 2
      ShadowColor = clGray
      LegendFont.Charset = DEFAULT_CHARSET
      LegendFont.Color = clBlue
      LegendFont.Height = 31
      LegendFont.Name = 'Comic Sans MS'
      LegendFont.Style = []
      GlyphToEdgeSpacing = 1
      TextToEdgeBorder = 5
      MultiLineSpacing = 0
      OnMouseUp = TouchGlyphSoftkey1MouseUp
      align = alLeft
    end
    object BtnBrowserClose: TTouchGlyphSoftkey
      Left = 2
      Top = 2
      Width = 100
      Height = 41
      Legend = 'Close'
      TextSize = 0
      HasIndicator = False
      IndicatorOn = False
      IsEnabled = True
      ButtonColor = clInfoBk
      Layout = slGlyphLeft
      AutoTextSize = True
      KeyStyle = ssRectangle
      CornerRad = 10
      ButtonTravel = 2
      ShadowColor = clGray
      LegendFont.Charset = DEFAULT_CHARSET
      LegendFont.Color = clBlue
      LegendFont.Height = 31
      LegendFont.Name = 'Comic Sans MS'
      LegendFont.Style = []
      GlyphToEdgeSpacing = 1
      TextToEdgeBorder = 5
      MultiLineSpacing = 0
      OnMouseUp = BtnBrowserCloseMouseUp
      align = alLeft
    end
    object Panel2: TPanel
      Left = 102
      Top = 2
      Width = 50
      Height = 41
      Align = alLeft
      BevelInner = bvLowered
      TabOrder = 2
    end
    object BtnScreenMode: TTouchGlyphSoftkey
      Left = 611
      Top = 2
      Width = 100
      Height = 41
      Legend = 'Full~Sceen'
      TextSize = 0
      HasIndicator = False
      IndicatorOn = False
      IsEnabled = True
      ButtonColor = clInfoBk
      Layout = slGlyphLeft
      AutoTextSize = True
      KeyStyle = ssRectangle
      CornerRad = 10
      ButtonTravel = 2
      ShadowColor = clGray
      LegendFont.Charset = DEFAULT_CHARSET
      LegendFont.Color = clBlue
      LegendFont.Height = 15
      LegendFont.Name = 'Comic Sans MS'
      LegendFont.Style = []
      GlyphToEdgeSpacing = 1
      TextToEdgeBorder = 5
      MultiLineSpacing = 0
      OnMouseUp = BtnScreenModeMouseUp
      align = alRight
    end
    object Button1: TButton
      Left = 20000
      Top = 8
      Width = 75
      Height = 25
      Cancel = True
      Caption = 'Button1'
      TabOrder = 4
      OnClick = Button1Click
    end
  end
end
