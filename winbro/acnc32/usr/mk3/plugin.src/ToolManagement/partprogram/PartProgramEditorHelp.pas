unit PartProgramEditorHelp;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ComCtrls, ExtCtrls,  Buttons, OleCtrls, OKBox, AbstractOPPPlugin,
  NamedPlugin, StreamTokenizer, IniFiles, ACNC_SHeditor, ACNC_plusmemo, Clipbrd,
  ACNC_plusGutter, ImgList, ACNC_TouchSoftkey, ACNC_BaseKeyBoards, INamedInterface,
  SHDocVw, OPPDoc, ShellApi, Math, ACNC_TouchGlyphSoftkey, CNCTypes;

type

TFindModes = (fmNone,fmFind,fmReplace);

TOppEditMode = (oeGCodeEdit,oeHeaderEdit);


  TGCodeForm = class(TForm)
    ControlPanel: TPanel;
    FileOPen: TOpenDialog;
    FindPanel: TPanel;
    SavePanel: TPanel;
    FindModePanel: TPanel;
    UndoControls: TPanel;
    StartTimer: TTimer;
    pp1: TOPPSHEdit;
    OkButton: TTouchSoftkey;
    CancelButton: TTouchSoftkey;
    SearchPages: TPageControl;
    FindSheet: TTabSheet;
    UndoPanel: TPanel;
    UndoKey: TSpeedButton;
    RedoKey: TSpeedButton;
    topBottomPanel: TPanel;
    TopKey: TKBKey;
    BottomKey: TKBKey;
    BookMarkKey: TTouchSoftkey;
    FindTextEdit: TSHCursorEdit;
    ReplaceSheet: TTabSheet;
    ReplaceTextEdit: TSHCursorEdit;
    FindandReplaceButton: TSpeedButton;
    FindButton: TSpeedButton;
    SoptionsPanel: TPanel;
    FindWordButton: TSpeedButton;
    MatchCaseButton: TSpeedButton;
    FWdButton: TSpeedButton;
    BackwardButton: TSpeedButton;
    FindAgainButton: TSpeedButton;
    FindDoneKey: TTouchSoftkey;
    logoSheet: TTabSheet;
    Label7: TLabel;
    Label8: TLabel;
    PanelKB: TPanel;
    MainKb: TBaseKB;
    KeyPad: TBaseFPad;
    BitBtn1: TBitBtn;
    Panel7: TPanel;
    BtnHideKB: TTouchGlyphSoftkey;
    BtnHelp: TTouchGlyphSoftkey;
    Timer1: TTimer;
    pp1panel: TPanel;
    HelpPanel: TPanel;
    WebBrowser: TWebBrowser;
    Panel2: TPanel;
    CloseHelpBtn: TButton;
    HomeBtn: TButton;
    NextBtn: TButton;
    PrevBtn: TButton;
    Button1: TButton;
    procedure Button1Click(Sender: TObject);
    procedure PrevBtnClick(Sender: TObject);
    procedure NextBtnClick(Sender: TObject);
    procedure HomeBtnClick(Sender: TObject);
    procedure CloseHelpBtnClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FindButtonClick(Sender: TObject);
    procedure FindTextEditKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FindAgainButtonClick(Sender: TObject);
    procedure OKButtonClick(Sender: TObject);
    procedure CancelButtonClick(Sender: TObject);
    procedure FindWordButtonClick(Sender: TObject);
    procedure MatchCaseButtonClick(Sender: TObject);
    procedure TopKeyMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure BottomKeyClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure StartTimerTimer(Sender: TObject);
    procedure UndoKeyClick(Sender: TObject);
    procedure PartTypeEditEnter(Sender: TObject);
    procedure HeaderCancelButtonClick(Sender: TObject);
    procedure FWdButtonClick(Sender: TObject);
    procedure BackwardButtonClick(Sender: TObject);
    procedure RedoKeyClick(Sender: TObject);
    procedure BookMarkKeyMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FindDoneKeyMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure BitBtn1Click(Sender: TObject);
    procedure pp1FindModeRequested(Sender: TObject);
    procedure pp1FindAgainRequested(Sender: TObject);
    procedure BtnHideKBMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure pp1F1HelpRequested(Sender: TObject);
    procedure BtnHelpMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Timer1Timer(Sender: TObject);
  private
    DisplayTop : Integer;
    DisplayHeight : Integer;
    ShowKB : Boolean;
    LineNumber : Integer;
    SearchOptions     : TSearchSwitches;

    FindMode            : TFindModes;

    iniWordwrap : Boolean;

    Translator : TTranslator;
    // Translation Strings
    TranslatedOK              : String;
    TranslatedCancel          : String;
    TranslatedHideBookMark    : String;
    TranslatedBookMarkControl : String;
    TranslatedFind            : String;
    TranslatedFindAgain       : String;
    TranslatedWholeWord       : String;
    TranslatedMatchCase       : String;
    TranslatedForwards        : String;
    TranslatedBackwards       : String;
    TranslatedFindAndReplace  : String;
    TranslatedTop             : String;
    TranslatedBottom          : String;
    TranslatedUndo            : String;
    TranslatedRedo            : String;
    TranslatedDone            : String;
    TranslatedFindWord        : String;
    TranslatedReplaceAgain    : String;
    TranslatedMaxValue        : String;
    TranslatedTrue            : String;
    TranslatedFalse           : String;
    TranslatedShowKB          : String;
    TranslatedHideKB          : String;

    AxisLabels: array [0..12] of TLabel;
    CopyAxisButton: TButton;
    HelpPrefix: string;
    HelpPath: string;
    HelpHome: string;

    procedure CopyAxisPosition(Sender: TObject);
    procedure FocusOnEditor;
    procedure ContentsChanged(Sender : TObject);
    procedure ReadINiFile;
    procedure ReadConfigFile;
    procedure ShowFindPage(fMode : TFindModes);
    procedure LaunchHelp;

  public
    FileString : string;

  end;

 TGCodeEditor = class(TAbstractOPPPluginEditor)
    class function Execute(var Section : string) : Boolean; override;
  end;

var
  GCodeForm: TGCodeForm;

function CSVField(FNumber: Integer;TextString : String):String;
function GetToken(var Buffer : String):String;
function SlashSep(const Path, S: String): String;


implementation

{$R *.DFM}


class function TGCodeEditor.Execute(var Section : string) : Boolean;
var
  Form : TGCodeForm;
  TempStream : TMemoryStream;
  TempStrings : TStringList;
begin
  Form := TGCodeForm.Create(Application);

  Form.FileString := Section;         
  //Form.EditMode := oeGCodeEdit;
  Form.pp1.EditPage.WordWrap := Form.InIWordwrap;
  Form.pp1.EditPage.Cursor := crIBeam;
  Form.pp1.IndicateHelpAvailable := False;
  Form.pp1.Visible := True;
  Form.ControlPanel.Visible := True;


  Result := Form.ShowModal = mrOK;
  if (Result) Then
     begin
     TempStream := TMemoryStream.Create;
     TempStrings := TStringList.Create;
     try
     TempStrings.Clear;
     Form.PP1.EditPage.Lines.SaveToStream(TempStream);
     TempStream.Position := 0;
     TempStrings.LoadFromStream(TempStream);
     Section := TempStrings.Text;
     finally
     TempStream.Free;
     TempStrings.Free;
     end;
     end;
  Form.Free;
end;



procedure TGCodeForm.FormCreate(Sender: TObject);
var I: Integer;
begin
  LineNumber := 0;
  with pp1 do
    begin
    EditPage.OnChange := ContentsChanged;
    end;
    ReadConfigFile;
    Translator := TTranslator.Create;
  try
  with Translator do
    begin
    TranslatedOK          := GetString('$OK');
    TranslatedCancel      := GetString('$CANCEL');
    TranslatedHideBookMark:= GetString('$HIDE_BOOKMARK');
    TranslatedBookMarkControl := GetString('$BOOKMARK_CONTROL');
    TranslatedFind            := GetString('$FIND');
    TranslatedFindAndReplace  := GetString('$FIND_AND_REPLACE');
    TranslatedTop             := GetString('$TOP');
    TranslatedBottom          := GetString('$BOTTOM');
    TranslatedUndo            := GetString('$UNDO');
    TranslatedRedo            := GetString('$REDO');
    TranslatedFindAgain       := GetString('$FIND_AGAIN');
    TranslatedWholeWord       := GetString('$WHOLE_WORD');
    TranslatedMatchCase       := GetString('$MATCH_CASE');
    TranslatedForwards        := GetString('$FORWARDS');
    TranslatedBackwards       := GetString('$BACKWARDS');
    TranslatedDone            := GetString('$DONE');
    TranslatedFindWord        := GetString('$FIND_WORD');
    TranslatedReplaceAgain    := GetString('$REPLACE_AGAIN');
    BookMarkKey.Legend        := GetString('$BOOKMARK_CONTROL');
    CancelButton.Legend       := GetString('$CANCEL');
    FindDoneKey.Legend        := gETsTRING('$DONE');
    TranslatedMaxValue        := GetString('$MAX_VALUE');
    TranslatedTrue            := GetString('$TRUE');
    TranslatedFalse           := GetString('$FALSE');
    TranslatedShowKB          := GetString('$SHOW_KB');
    TranslatedHideKB          := GetString('$HIDE_KB');

    PP1.BM1Legend := GetString('$SET_1');;
    PP1.BM2Legend := GetString('$SET_2');
    PP1.GBM1Legend := GetString('$GOTO_1');
    PP1.GBM2Legend := GetString('$GOTO_2');

    end;
  finally
  Translator.Free;
  end;

  TopKey.MainCaption := TranslatedTop;
  BottomKey.MainCaption := TranslatedBottom;
  FindButton.Caption := TranslatedFind;
  FindandReplaceButton.Caption := TranslatedFindAndReplace;
  UndoKey.Caption := TranslatedUndo;
  RedoKey.Caption := TranslatedRedo;
  OkButton.Caption := TranslatedOK;
  FindWordButton.Caption := TranslatedFindWord;
  MatchCaseButton.Caption := TranslatedMatchCase;
  FWdButton.Caption := TranslatedForwards;
  BackwardButton.Caption := TranslatedBackwards;
  FindAgainButton.Caption := TranslatedFindAgain;
  FindDoneKey.Caption := TranslatedDone;

  ReadINiFile;
  SearchOptions := [soFwd];
  ContentsChanged(Self);
  pp1.EditPage.ForceUpdate;
  pp1.UpdateStatus(Self);

  for I:= 0 to NamedPlugin.NC.AxisCount - 1 do begin
    AxisLabels[I]:= TLabel.Create(Self);
    AxisLabels[I].Parent:= LogoSheet;
    AxisLabels[I].Left := 8;
    AxisLabels[I].Top:= 8 + (24 * I);
    AxisLabels[I].Font.Name:= 'Courier New';
    AxisLabels[I].Font.Style:= [fsBold];
    AxisLabels[I].Font.Size:= 16;
  end;

  CopyAxisButton:= TButton.Create(Self);
  CopyAxisButton.Parent:= LogoSheet;
  CopyAxisButton.Caption:= 'Copy';
  CopyAxisButton.Left:= 8;
  CopyAxisButton.Top:= 12 + (24 * NC.AxisCount);
  CopyAxisButton.OnClick:= CopyAxisPosition;


end;

procedure TGCodeForm.CopyAxisPosition(Sender: TObject);
var I: Integer;
    Build: string;
begin
  Build:= '';
  for I:= 0 to NC.AxisCount - 1 do begin
    Build:= Build + NC.AxisName[I] + Format('%.3f', [NC.AxisPosition[I][ptDisplay]]) + ' ';
  end;
  Build:= Build + CarRet;
  ClipBoard.AsText:= Build;
  pp1.EditPage.PasteFromClipboard;
end;

procedure TGCodeForm.Timer1Timer(Sender: TObject);
var I: Integer;
begin
  for I:= 0 to NamedPlugin.NC.AxisCount - 1 do begin
    AxisLabels[I].Caption:= NC.AxisName[I] + Format('%.3f', [NC.AxisPosition[I][ptDisplay]]);
    AxisLabels[I].Parent:= LogoSheet;
  end;
end;


procedure TGCodeForm.ReadINiFile;
var
  IniFile : TInifile;
  Buffer  : String;
  BasePath : String;
begin
  BasePath := DLLProfilePath+'OPPEditor.cfg';
  IniFile := TIniFile.Create(BasePath);
  try
      DisplayTop := IniFile.ReadInteger('GENERAL','DisplayTop',75);
      DisplayHeight := IniFile.ReadInteger('GENERAL','DisplayHeight',690);
      Buffer := IniFile.ReadString ('GENERAL','ShowKeyboardDefault','False');
      ShowKB := (Buffer[1]= 't') or (Buffer[1]= 'T');
      HelpPrefix:= IniFile.ReadString('GENERAL', 'HelpPrefix', '');
      HelpPath:= IniFile.ReadString('GENERAL', 'HelpPath', '');
      HelpHome:= IniFile.ReadString('GENERAL', 'Helphome', '');

      if ShowKB then
        begin
        BtnHideKB.Legend := TranslatedHideKB;
        end
      else
        begin
        BtnHideKB.Legend := TranslatedShowKB;
        end;

  finally
    IniFile.Free;
  end;
end;

procedure TGCodeForm.FormResize(Sender: TObject);
var
TopHeight : Integer;
begin
if ShowKB then
  begin
  TopHeight := ((Height div 5)*3)+40;
  PanelKB.Visible := True;
  PanelKB.Height :=Height-TopHeight-2;
  end
else
  begin
  TopHeight := Height-2;
  PanelKB.Visible := False;
  end;

pp1.Visible := True;

Top := DisplayTop;
Left := 0;
pp1.Align:= alClient;
pp1panel.Left := 1;
pp1panel.Top := 1;
pp1panel.Height := TopHeight;
pp1panel.Width := ((Width div 4)*3);



KeyPad.Height := MainKB.Height;
KeyPad.Top := MainKb.Top;
MainKb.Width := (Width div 4)*3;
KeyPad.Width := Width-pp1.Width-12;
KeyPad.Left := MainKb.Width+2;
//PP1.Width := (Width div 4)*3;
ControlPanel.Top := pp1.Top;
ControlPanel.Width := KeyPad.Width;
ControlPanel.Left := KeyPad.Left;
ControlPanel.Height := TopHeight;
FindTextEdit.Width := ControlPanel.Width -12;
FindTextEdit.Left := 3;


OkButton.Width := (SavePanel.Width div 2)-4;
OkButton.Left := 2;

CancelButton.Width := (SavePanel.Width div 2)-4;
CancelButton.Left := 4+OkButton.Width;


FindButton.Width := (FindPanel.Width div 2) -4;
FindButton.Left :=2;

FindandReplaceButton.Width := (FindPanel.Width div 2) -4;
FindandReplaceButton.Left :=FindButton.Width + FindButton.Left+ 2;

FindAgainButton.Width :=  (SoptionsPanel.Width div 2)-4;
FindAgainButton.Left := 2;
FindAgainButton.Top := 2;

FindDoneKey.Width := FindAgainButton.Width;
FindDoneKey.Top := 2;
FindDoneKey.Left := FindAgainButton.Width+  FindAgainButton.Left + 2;
FindDoneKey.Height := FindAgainButton.Height;


FindWordButton.Width := (SoptionsPanel.Width div 2) -2;
FindWordButton.Left := 2;
FindWordButton.Top := FindAgainButton.Top + FindAgainButton.Height + 10;

MatchCaseButton.Width := FindWordButton.Width;
MatchCaseButton.Left :=2+FindWordButton.Width;
MatchCaseButton.Top := FindWordButton.Top;

FWdButton.Width := FindWordButton.Width;
FWdButton.Left := FindWordButton.Left;
FWdButton.Top := FindWordButton.Top + FindWordButton.Height + 10;

BackwardButton.Width := FWdButton.Width;
BackwardButton.Left :=2+FWdButton.Width;
BackwardButton.Top := FWdButton.Top;

UndoKey.Width := (UndoPanel.Width div 2) -2;
UndoKey.align := alLeft;

RedoKey.Width := (UndoPanel.Width div 2) -2;
RedoKey.align := alRight;

end;





procedure TGCodeForm.FindButtonClick(Sender: TObject);
begin
if Sender= FindButton then
   begin
   if FindButton.Down then
      begin
      ShowFindPage(fmFind);
      with FindTextedit do
           begin
           FindAgainButton.Visible := True;
           FindAgainButton.Caption := TranslatedFindAgain;
           FwdButton.Down := True;
           Include(SearchOptions,soFwd);
           enabled := True;
           Color := clwhite;
           Text := '';
           SetFocus;
           end;

      MainKB.FocusControl := FindTextedit;
      Keypad.FocusControl := FindTextedit;
      end
   else
       begin
       FocusOnEditor;
       end;
   end else

if Sender= FindAndReplaceButton then
   begin
   if FindAndReplaceButton.Down then
      begin
      ShowFindPage(fmReplace);
      with FindTextedit do
           begin
           FindAgainButton.Visible := True;
           FindAgainButton.Caption := TranslatedReplaceAgain;
           FwdButton.Down := True;
           Include(SearchOptions,soFwd);
           enabled := True;
           Color := clwhite;
           Text := '';
           SetFocus;
           end;
      MainKB.FocusControl := FindTextedit;
      Keypad.FocusControl := FindTextedit;

      end
   else
       begin
       FocusOnEditor;
       end;
   end else

if Sender= pp1 then
   begin
      ShowFindPage(fmFind);
      with FindTextedit do
           begin
           FindAgainButton.Visible := True;
           FindAgainButton.Caption := TranslatedFindAgain;
           FwdButton.Down := True;
           Include(SearchOptions,soFwd);
           enabled := True;
           Color := clwhite;
           Text := pp1.EditPage.CurrentWord;
           SetFocus;
           end;
      MainKB.FocusControl := FindTextedit;
      Keypad.FocusControl := FindTextedit;
      end

end;


procedure TGCodeForm.FindTextEditKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
if Sender = FindTextEdit then
begin
if FindMode = fmFind then
   begin
   if key = VK_RETURN then
      begin
      pp1.EditPage.SearchText(FindTextEdit.Text,SearchOptions);
      FocusOnEditor;
      pp1.EditPage.setfocus;
      FindAgainButton.Visible := True;
      FindButton.Down := False;
      end;
   end;

if FindMode = fmReplace then
   begin
   if key = VK_RETURN then
      begin
      FindAndReplaceButton.Down := False;
      ReplaceTextEdit.Visible := True;
      ReplaceTextEdit.SetFocus;
      MainKB.FocusControl := ReplaceTextedit;
      Keypad.FocusControl := ReplaceTextedit;

      end;
   end;
end;

if Sender = ReplaceTextEdit then
   begin
   if key = VK_RETURN then
      begin
      if pp1.EditPage.SearchText(FindTextEdit.Text,SearchOptions) then
         begin
         pp1.EditPage.CutToClipboard;
         ClipBoard.SetTextBuf(PChar(ReplaceTextEdit.Text));
         pp1.EditPage.PasteFromClipboard;
         end;
      FocusOnEditor;
      FindAgainButton.Visible := True;
      FindAndReplaceButton.Down := False;
      end;
   end;
end;

procedure TGCodeForm.FocusOnEditor;
begin
  MainKb.FocusControl := pp1.EditPage;
  KeyPad.FocusControl := pp1.EditPage;
  FindTextEdit.Enabled := False;
  FindButton.Down := False;
end;


procedure TGCodeForm.FindAgainButtonClick(Sender: TObject);
begin
if FindMode = fmFind then
   begin
   pp1.EditPage.SearchText(FindTextEdit.Text,SearchOptions);
   FocusOnEditor;
   end;

if FindMode = fmReplace then
   begin
   if pp1.EditPage.SearchText(FindTextEdit.Text,SearchOptions) then
      begin
      pp1.EditPage.CutToClipboard;
      ClipBoard.SetTextBuf(PChar(ReplaceTextEdit.Text));
      pp1.EditPage.PasteFromClipboard;
      end;
   FocusOnEditor;
   end;
end;


procedure TGCodeForm.OKButtonClick(Sender: TObject);
begin
  ModalResult := mrOK;
end;

procedure TGCodeForm.CancelButtonClick(Sender: TObject);
var
DoExit : Boolean;
begin
if not pp1.EditPage.Modified then
        begin
        ModalResult := mrCancel;
        end
else
        begin
        CancelDialog := TCancelDialog.Create(Self);
        try
        CancelDialog.ShowModal;
        DoExit :=  CancelDialog.ModalResult = mrOK;
        finally
        CancelDialog.Free;
        end;
        if DoExit then
                begin
                ModalResult := mrCancel;
                end;
        end;
end;

procedure TGCodeForm.FWdButtonClick(Sender: TObject);
begin
Include(SearchOptions,soFwd);
end;
procedure TGCodeForm.BackwardButtonClick(Sender: TObject);
begin
Exclude(SearchOptions,soFwd);
end;


procedure TGCodeForm.FindWordButtonClick(Sender: TObject);
begin
if FindWordButton.Down then
   begin
   Include(SearchOptions,soWholeWord);
   end
else
   begin
   Exclude(SearchOptions,soWholeWord);
   end
end;

procedure TGCodeForm.MatchCaseButtonClick(Sender: TObject);
begin
if MatchCaseButton.Down then
   begin
   Include(SearchOptions,soCaseSensitive);
   end
else
   begin
   Exclude(SearchOptions,soCaseSensitive);
   end
end;


procedure TGCodeForm.TopKeyMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
FocusOnEditor;
pp1.EditPage.TopofFile;
end;

procedure TGCodeForm.BottomKeyClick(Sender: TObject);
begin
FocusOnEditor;
pp1.EditPage.BottomofFile;

end;




procedure TGCodeForm.FormShow(Sender: TObject);
begin
Width := 1022;
Top := DisplayTop;
Height := DisplayHeight;
FormResize(Self);
ShowFindPage(fmNone);
StartTimer.Enabled := True;

pp1.EditPage.Enabled := True;
pp1.EditPage.SetFocus;

end;

procedure TGCodeForm.StartTimerTimer(Sender: TObject);
begin
StartTimer.Enabled := False;

pp1.StartEdit(FileString);
FocusOnEditor;

end;

procedure TGCodeForm.UndoKeyClick(Sender: TObject);
begin
pp1.EditPage.Undo;
end;

procedure TGCodeForm.RedoKeyClick(Sender: TObject);
begin
pp1.EditPage.Redo;
end;

procedure TGCodeForm.PartTypeEditEnter(Sender: TObject);
begin
   MainKb.FocusControl := TEdit(Sender);
   KeyPad.FocusControl := TEdit(Sender);
end;







procedure TGCodeForm.HeaderCancelButtonClick(Sender: TObject);
begin
ModalResult := mrCancel;
end;


procedure TGCodeForm.ReadConfigFile;
var
ConfigFile :TIniFile;
Wordlist : TStringList;
FileName : String;
keywordcolour : LongInt;
commentcolour : LongInt;
FontColour    : Longint;
BackGroundColour : Longint;
CommentStart  : String;
C3Start       : String;
Buffer        : String;
WordNumber    : Integer;
FontStyle     : TExtFontStyles;
DelimeterSet  : Set of Char;
CharCount     : Integer;
begin
FileName := DLLProfilePath+'OPPEditor.cfg';
if FileExists(FileName) then
   begin
   ConfigFile := TInifile.Create(Filename);
   try
   WordList := TStringList.Create;
   with ConfigFile do
        begin
        // Set default font
        IniWordwrap := ReadBool('GENERAL', 'Wordwrap', False);
        Pp1.EditPage.WordWrap := IniWordwrap;

        Buffer := ReadString('colours','backgroundColour','clwhite');
        if not IdentToColor(Buffer,BackGroundColour) then
           begin
           BackGroundColour := clwhite;
           end;
        Pp1.EditPage.Color := BackGroundColour;

        Buffer := ReadString('colours','normalfontname','Verdana');
        pp1.Font.Name := Buffer;
        pp1.Font.Size := ReadInteger('colours','normalfontsize',12);
        Buffer := ReadString('colours','normalfontcolour','clblack,bold');
        if not IdentToColor(CSVField(0,Buffer),FontColour) then
           begin
           FontColour := clBlack;
           end;
        pp1.Font.Color := FontColour;
        FontStyle := [];
        if CSVField(1,Buffer)='bold' then include(FontStyle,fsbold);
        pp1.Font.Style := FontStyle;

        Buffer := ReadString('delimeters','Characters','$%&,./:;<=>(');
        DelimeterSet := [];
        include(DelimeterSet,Char(' '));
        For CharCount := 1 to Length(Buffer) do
            begin
            include(DelimeterSet,Char(Buffer[CharCount]));
            end;

        pp1.EditPage.Delimiters := Delimeterset;

        // Add all keywords to list
        ReadSection('keywords',Wordlist);
        if Wordlist.Count > 0 then
           begin
           pp1.EditPage.Keywords.BeginUpdate;
           For WordNumber := 0 to Wordlist.Count-1 do
               begin
               Buffer := ReadString('colours','keyword','clmaroon,bold');
               IdentToColor(CSVField(0,Buffer),keywordcolour);
               FontStyle := [];
               if CSVField(1,Buffer)='bold' then include(FontStyle,fsbold);
               if CSVField(2,Buffer)='italics' then include(FontStyle,fsitalic);
               pp1.EditPage.Keywords.AddKeyWord(WordList[WordNumber],[woWholeWordsOnly],FontStyle,1,crDefault,
                                       BackGroundColour,keywordcolour);
               end;
           pp1.EditPage.ReApplyKeywords;
           pp1.EditPage.ApplyKeyWords := True;
           pp1.EditPage.Keywords.EndUpdate;
           end;

        // Now add comment identifiers
        Buffer := ReadString('colours','comment','clblue,bold,italic');
        if not IdentToColor(CSVField(0,Buffer),commentcolour) then
           begin
           commentColour := clBlack;
           end;
        FontStyle := [];
        if CSVField(1,Buffer)='bold' then include(FontStyle,fsbold);
        if CSVField(2,Buffer)='italics' then include(FontStyle,fsitalic);
        CommentStart := ReadString('comments','starttext1','');
        if  CommentStart <> '' then
            begin
            pp1.EditPage.StartStopKeys.AddStartStopKey(CommentStart,'',[],FontStyle,1,crDefault,BackGroundColour,CommentColour,True);
            end;

        // Now add C3 colouring
        Buffer := ReadString('colours','C3Calls','clAqua,bold,italic');
        if not IdentToColor(CSVField(0,Buffer),commentcolour) then
           begin
           CommentColour := clBlack;
           end;
        FontStyle := [];
        if CSVField(1,Buffer)='bold' then include(FontStyle,fsbold);
        if CSVField(2,Buffer)='italics' then include(FontStyle,fsitalic);
        C3Start := ReadString('C3Calls','StartText','');
        if  CommentStart <> '' then
            begin
            pp1.EditPage.StartStopKeys.AddStartStopKey(C3Start,' ',[],FontStyle,1,crDefault,BackGroundColour,CommentColour,True);
            end;

        // Now add Label start character id
        Buffer := ReadString('colours','label','clgreen,bold');
        if not IdentToColor(CSVField(0,Buffer),commentcolour) then
           begin
           commentColour := clGreen;
           end;
        FontStyle := [];
        if CSVField(1,Buffer)='bold' then include(FontStyle,fsbold);
        if CSVField(2,Buffer)='italics' then include(FontStyle,fsitalic);
        CommentStart := ReadString('labels','startchar1','');
        if  CommentStart <> '' then
            begin
            pp1.EditPage.StartStopKeys.AddStartStopKey(CommentStart,' ',[],FontStyle,1,crDefault,BackGroundColour,CommentColour,True);
            end;
        CommentStart := ReadString('labels','startchar2','');
        if  CommentStart <> '' then
            begin
            pp1.EditPage.StartStopKeys.AddStartStopKey(CommentStart,' ',[],FontStyle,1,crDefault,BackGroundColour,CommentColour,True);
            end;
        end;
   finally
   ConfigFile.Free;
   end;
   end;
end;


//*********************************************
// function CSVField(
// Result is then Commaseparated field specified by
// FNumber in input Line TextString
//*********************************************
function CSVField(FNumber: Integer;TextString : String):String;
var
Field      : Integer;
begin
Field := FNumber;
while Field >= 0 do
      begin
      Result :=GetToken(TextString);
      dec(Field);
      end;
end;

function SlashSep(const Path, S: String): String;
begin
  if AnsiLastChar(Path)^ <> '\' then
    Result := Path + '\' + S
  else
    Result := Path + S;
end;


//*********************************************
// function GetToken
// Result is next commaseparatred string
// var Buffer contains remainder of input string
//*********************************************
function GetToken(var Buffer : String):String;
var
CommaPos : Integer;
begin
Commapos := POs(',',Buffer);
if Commapos > 0 then
   begin
   Result := Copy(Buffer,1,CommaPos-1);
   Buffer := Copy(Buffer,CommaPos+1,Length(Buffer));
   end
else
    begin
    Result := Buffer;
    Buffer := '';
    end;
end;






procedure TGCodeForm.ContentsChanged(Sender: TObject);
begin
UndoKey.Enabled := pp1.EditPage.CanUndo;
RedoKey.Enabled := pp1.EditPage.CanRedo;
end;


procedure TGCodeForm.BookMarkKeyMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
pp1.ShowBookMarkPanel := not pp1.ShowBookMarkPanel;
if pp1.BookMarkPanel.Visible then
   begin
   BookMarkKey.Legend := 'Hide~Bookmark';
   end
else
   begin
   BookMarkKey.Legend := 'Bookmark~Control';
   end

end;

procedure TGCodeForm.ShowFindPage(fMode: TFindModes);
begin
FindMode := fMode;
if fMode = fmNone then
   begin
   SearchPages.ActivePage := LogoSheet;
   FindModePanel.Visible := True;
   UndoControls.Visible := True;
   end;


if fMode = fmFind then
   begin
   SearchPages.ActivePage := FindSheet;
   UndoControls.Visible := False;
   FindTextEdit.Parent := FindSheet;
   SOptionsPanel.Parent := FindSheet;
   FindModePanel.Visible := True;
   FindAgainButton.Visible := False;
   end;

if fMode = fmReplace then
   begin
   SearchPages.ActivePage := ReplaceSheet;
   UndoControls.Visible := False;
   SOptionsPanel.Parent := ReplaceSheet;
   FindTextEdit.Parent := ReplaceSheet;
   FindModePanel.Visible := True;
   FindAgainButton.Visible := False;
   end;



end;

procedure TGCodeForm.FindDoneKeyMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
FindButton.Down := False;
FindAndReplaceButton.Down := False;
ShowFindPage(fmNone);
end;

procedure TGCodeForm.BitBtn1Click(Sender: TObject);
var Doc : TOPPDoc;
begin
  Doc:= TOPPDoc.Create;
  Doc.DestDir := NamedPlugin.DllLocalPath;
  Doc.C3 := NamedPlugin.DllProfilePath + 'c3.cfg';
  Doc.StyleSheet := NamedPlugin.DllProfilePath + 'stylesheet.css';

  Doc.Document(pp1.Lines.Text);
  ShellExecute(Self.Handle, 'open',
               PChar(NamedPlugin.DllLocalPath + 'Opp.html'),
                          nil,
                          nil, SW_SHOWNORMAL);

end;

procedure TGCodeForm.pp1FindModeRequested(Sender: TObject);
begin
FindButtonClick(pp1);
end;


procedure TGCodeForm.pp1FindAgainRequested(Sender: TObject);
begin
FindAgainButtonClick(Self);
end;

procedure TGCodeForm.BtnHideKBMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
if ShowKB then
  begin
  BtnHideKB.Legend := TranslatedShowKB;
  ShowKB := False;
  end
else
  begin
  BtnHideKB.Legend := TranslatedHideKB;
  ShowKB := True;
  end;
FormResize(Self);

end;

procedure TGCodeForm.pp1F1HelpRequested(Sender: TObject);
begin
  LaunchHelp;
end;

procedure TGCodeForm.BtnHelpMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  LaunchHelp;
end;

procedure TGCodeForm.LaunchHelp;
var Tmp: string;
begin
  Tmp:= HelpPath + '\' + HelpPrefix + pp1.CurrentWord + '.html';
  if FileExists(Tmp) then begin
    HelpPanel.Visible:= True;
    try
      WebBrowser.Navigate('file:///' + Tmp);
      Exit;
    except
    end;
  end;

  Tmp:= HelpPath + '\' + pp1.CurrentWord + '.html';
  if FileExists(Tmp) then begin
    HelpPanel.Visible:= True;
    try
      WebBrowser.Navigate('file:///' + Tmp);
      Exit;
    except
    end;
  end;
end;

procedure TGCodeForm.CloseHelpBtnClick(Sender: TObject);
begin
  HelpPanel.Visible:= False;
end;

procedure TGCodeForm.HomeBtnClick(Sender: TObject);
begin
  try
    if HelpHome <> '' then
      WebBrowser.Navigate('file:///' + HelpHome);
  except
  end
end;

procedure TGCodeForm.NextBtnClick(Sender: TObject);
begin
  try
    WebBrowser.GoForward;
  except
  end;
end;

procedure TGCodeForm.PrevBtnClick(Sender: TObject);
begin
  try
    WebBrowser.GoBack;
  except
  end;
end;

procedure TGCodeForm.Button1Click(Sender: TObject);
begin
  WebBrowser.Refresh2;
end;

initialization
  TAbstractOPPPluginEditor.AddPlugin('NC32GCODE', TGCodeEditor);
end.

