unit Directory;

interface

uses SysUtils, Classes, Windows, FileCtrl;

type
  TDirectoryAction = procedure(const FileName : string; const Search : TSearchRec) of object;

  TDirectory = class(TObject)
  private
    BackupFilePath : string;
    SourceFilePath : string;
    ManifestList : TStringList;
    ManifestSize : Int64;
    ManifestPath : string;
    FRelativePaths : Boolean;
    procedure RecurseDirectory(const Path, Mask : string; Action : TDirectoryAction; Recurse : Boolean);
    procedure DeleteFile(const FileName : string; const Search : TSearchRec);
    procedure AddManifest(const FileName : string; const Search : TSearchRec);
    procedure BackupFile(const FileName : string; const Search : TSearchRec);
  public
    function Manifest(const Path : string; S : TStringList; Recurse : Boolean) : Int64;
    function AutoBackup(const Path : string) : string;
    procedure Delete(const Path : string);
    property RelativePaths : Boolean read FRelativePaths write FRelativePaths;
  end;

implementation

procedure TDirectory.BackupFile(const FileName : string; const Search : TSearchRec);
var Tmp : string;
begin
  Tmp := System.Copy(FileName, Length(SourceFilePath), Length(FileName));
  Tmp := BackupFilePath + Tmp;
  if FileName[Length(FileName)] <> '\' then
    CopyFile(PChar(FileName), PChar(Tmp), False)
  else
    CreateDirectory(PChar(Tmp), nil);
end;

function TDirectory.AutoBackup(const Path: string) : string;
var NewName, Tmp : string;
    Year, Month, Day : Word;
    Count : Integer;
begin
  Result := '';
  if DirectoryExists(Path) then begin
    Tmp := ExcludeTrailingBackslash(Path);

    DecodeDate(Now, Year, Month, Day);
    NewName := Format('%s.%d-%d-%d\', [Tmp, Year, Month, Day]);

    Count := 0;
    while GetFileAttributes(PChar(NewName)) <> $FFFFFFFF do begin
      Inc(Count);
      NewName := Format('%s.%d-%d-%d.%d\', [Tmp, Year, Month, Day, Count]);
    end;
    CreateDirectory(PChar(NewName), nil);
    BackupFilePath := NewName;
    SourceFilePath := Path;
    RecurseDirectory(SourceFilePath, '*.*', BackupFile, True);
    Result := NewName;
  end;
end;

procedure TDirectory.DeleteFile(const FileName : string; const Search : TSearchRec);
begin
end;

procedure TDirectory.Delete(const Path: string);
begin
  RecurseDirectory(Path, '*.*', DeleteFile, True);
end;

procedure TDirectory.AddManifest(const FileName : string; const Search : TSearchRec);
begin
  if RelativePaths then
    ManifestList.Add(Copy(FileName, Length(ManifestPath) + 1, Length(FileName)))
  else
    ManifestList.Add(FileName);
  Inc(ManifestSize, Search.Size);
end;

function TDirectory.Manifest(const Path: string; S: TStringList; Recurse : Boolean) : Int64;
var FileName : string;
begin
  ManifestList := S;
  ManifestSize := 0;
  FileName := ExtractFileName(Path);
  ManifestPath := ExtractFilePath(Path);
  if FileName = '' then
    FileNAme := '*.*';
  RecurseDirectory(ManifestPath, FileName, AddManifest, Recurse);
  Result := ManifestSize;
end;

procedure TDirectory.RecurseDirectory(const Path, Mask : string; Action : TDirectoryAction; Recurse : Boolean);
var Search : TSearchRec;
begin
  if FindFirst(Path + Mask, faAnyFile, Search) = 0 then begin
    repeat
      if not DirectoryExists(Path + Search.Name) then begin
        Action(Path + Search.Name, Search)
      end else if (Search.Name <> '.') and
                 (Search.Name <> '..') then begin
        Action(Path + IncludeTrailingBackSlash(Search.Name), Search);
        if Recurse then
          RecurseDirectory(Path + IncludeTrailingBackSlash(Search.Name), Mask, Action, True);
      end;
    until FindNext(Search) <> 0;
  end;
  SysUtils.FindClose(Search);
//  ForceDirectory
end;

end.
