object CSVForm: TCSVForm
  Left = -37
  Top = 307
  Width = 1022
  Height = 726
  Caption = 'CSVForm'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object TopPanel: TPanel
    Left = 0
    Top = 0
    Width = 1014
    Height = 699
    Align = alClient
    BevelInner = bvLowered
    BevelWidth = 2
    TabOrder = 0
    object TouchGlyphSoftkey1: TTouchGlyphSoftkey
      Left = 896
      Top = 8
      Width = 100
      Height = 50
      Legend = 'Import CSV'
      TextSize = 0
      HasIndicator = False
      IndicatorOn = False
      IsEnabled = True
      ButtonColor = clInfoBk
      Layout = slGlyphLeft
      AutoTextSize = True
      KeyStyle = ssRectangle
      CornerRad = 10
      ButtonTravel = 2
      ShadowColor = clGray
      LegendFont.Charset = DEFAULT_CHARSET
      LegendFont.Color = clBlue
      LegendFont.Height = 23
      LegendFont.Name = 'Comic Sans MS'
      LegendFont.Style = []
      GlyphToEdgeSpacing = 1
      TextToEdgeBorder = 5
      MultiLineSpacing = 0
      OnMouseUp = TouchGlyphSoftkey1MouseUp
    end
    object TouchGlyphSoftkey2: TTouchGlyphSoftkey
      Left = 896
      Top = 200
      Width = 100
      Height = 50
      Legend = 'Cancel'
      TextSize = 0
      HasIndicator = False
      IndicatorOn = False
      IsEnabled = True
      ButtonColor = clInfoBk
      Layout = slGlyphLeft
      AutoTextSize = True
      KeyStyle = ssRectangle
      CornerRad = 10
      ButtonTravel = 2
      ShadowColor = clGray
      LegendFont.Charset = DEFAULT_CHARSET
      LegendFont.Color = clRed
      LegendFont.Height = 40
      LegendFont.Name = 'Comic Sans MS'
      LegendFont.Style = []
      GlyphToEdgeSpacing = 1
      TextToEdgeBorder = 5
      MultiLineSpacing = 0
      OnMouseUp = TouchGlyphSoftkey2MouseUp
    end
    object TouchGlyphSoftkey3: TTouchGlyphSoftkey
      Left = 896
      Top = 144
      Width = 100
      Height = 50
      Legend = 'OK'
      TextSize = 0
      HasIndicator = False
      IndicatorOn = False
      IsEnabled = True
      ButtonColor = clInfoBk
      Layout = slGlyphLeft
      AutoTextSize = True
      KeyStyle = ssRectangle
      CornerRad = 10
      ButtonTravel = 2
      ShadowColor = clGray
      LegendFont.Charset = DEFAULT_CHARSET
      LegendFont.Color = clGreen
      LegendFont.Height = 40
      LegendFont.Name = 'Comic Sans MS'
      LegendFont.Style = []
      GlyphToEdgeSpacing = 1
      TextToEdgeBorder = 5
      MultiLineSpacing = 0
      OnMouseUp = TouchGlyphSoftkey3MouseUp
    end
    object EditGrid: TTouchIndexFormatGrid
      Left = 4
      Top = 4
      Width = 850
      Height = 691
      IndexTabWidth = 35
      IndexTabHeight = 50
      IndexTop = 5
      IndexStrings.Strings = (
        'A'
        'B'
        'C'
        'D'
        'E'
        'F'
        'G'
        'H'
        'I'
        'J'
        'K'
        'L'
        'M'
        'N'
        'O'
        'P'
        'Q'
        'R'
        'S'
        'T'
        'U'
        'V'
        'W'
        'X'
        'Y'
        'Z')
      IndexVisible = False
      ControlPanelHeight = 45
      UseIndex = True
      HoldTime = 250
      MaxButtonWidth = 50
      Options = [toUpButton, toDownButton, toLeftButton, toRightButton, toPageUpButton, toPageDownButton]
      AlwaysActivated = False
      ShowKeyboard = False
      KeyboardHight = 200
      Editable = False
      RowCount = 5
      ColCount = 5
      DefaultRowHeight = 24
      DefaultColWidth = 64
      FixedRows = 1
      FixedCols = 1
      FixedColor = clBtnFace
      Color = clWindow
      DisplayUnformatted = False
      isReadOnly = True
      Align = alLeft
    end
    object CSVMemo: TMemo
      Left = 883
      Top = 588
      Width = 111
      Height = 30
      Lines.Strings = (
        'CSVMemo')
      TabOrder = 4
      Visible = False
      WordWrap = False
    end
    object TouchGlyphSoftkey4: TTouchGlyphSoftkey
      Left = 896
      Top = 288
      Width = 100
      Height = 50
      Legend = 'CLEAR'
      TextSize = 0
      HasIndicator = False
      IndicatorOn = False
      IsEnabled = True
      ButtonColor = clInfoBk
      Layout = slGlyphLeft
      AutoTextSize = True
      KeyStyle = ssRectangle
      CornerRad = 10
      ButtonTravel = 2
      ShadowColor = clGray
      LegendFont.Charset = DEFAULT_CHARSET
      LegendFont.Color = clNavy
      LegendFont.Height = 40
      LegendFont.Name = 'Comic Sans MS'
      LegendFont.Style = []
      GlyphToEdgeSpacing = 1
      TextToEdgeBorder = 5
      MultiLineSpacing = 0
      OnMouseUp = TouchGlyphSoftkey4MouseUp
    end
  end
  object ImportFileDialog: TOpenDialog
    InitialDir = 'C:\'
    Left = 968
    Top = 72
  end
end
