unit FileLoadDialog;

interface

uses
  Windows, SysUtils, Messages, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Grids, NamedPlugin, StdCtrls, ACNC_BaseKeyboards, CNCTypes,
  ACNC_TouchSoftkey, ACNC_FileList,AreYouSureDialog, FileCtrl,
  ACNC_TouchGlyphSoftkey, OCTTypes, Directory, CellMode;

const
NoFilesText = 'No Files';

type
  TSortModes = (
    smNone,
    smByNameFwd,
    smByNameRev,
    smBySizeFwd,
    smBySizeRev,
    smByDateFwd,
    smByDateRev
  );

  TKeyBoardVisibility = (
    kbmShow,
    kbmHide
  );

  TFileLoadForm = class(TForm)
    Panel1: TPanel;
    BtnOK: TTouchSoftkey;
    BtnCancel: TTouchSoftkey;
    Panel2: TPanel;
    Panel4: TPanel;
    DirGrid: TStringGrid;
    Panel6: TPanel;
    PanelKeyboard: TPanel;
    Panel5: TPanel;
    FileGrid: TStringGrid;
    PathLabel: TLabel;
    BtnPathSwitch: TTouchSoftkey;
    Panel3: TPanel;
    SaveEdit: TEdit;
    Label1: TLabel;
    BtnNew: TTouchSoftkey;
    BtnDelete: TTouchSoftkey;
    BtnShowKeyboard: TTouchGlyphSoftkey;
    procedure DirGridDblClick(Sender: TObject);
    procedure BtnOKClick(Sender: TObject);
    procedure BtnCancelClick(Sender: TObject);
    procedure FileGridSelectCell(Sender: TObject; ACol, ARow: Integer;
      var CanSelect: Boolean);
    procedure FileGridClick(Sender: TObject);
    procedure DirGridEnter(Sender: TObject);
    procedure FileGridEnter(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure DirGridDrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure FileGridKeyPress(Sender: TObject; var Key: Char);
    procedure BtnDeleteClick(Sender: TObject);
    procedure SaveEditChange(Sender: TObject);
    function CopyandDelete(OPPPath : String): Boolean;
    function MakeFileTime:String;
    procedure BtnNewClick(Sender: TObject);
    procedure BtnShowKeyboardMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
  private
    FileSortList : TFileList;
    SortMode : TSortModes;
    FileCount : Integer;
    KBMode : TKeyBoardVisibility;
    AYS : TAreYouSure;
    FBasePath : string;
    FBasePathName : string;
    FileMask : string;
    CurrentPath : string;
    SelectedFile : string;
    FilePath : string;
    SElectedPath : String;
    KB : TBaseKB;
    FKB : TNumericPad;
    DirGridActive : Boolean;
    FSaveMode : Boolean;
    Trans: TTranslator;
    procedure SetSaveMode(aMode : Boolean);
    procedure DoKeyPress(Sender : TObject; Key: AnsiChar);
    procedure RefreshDirectory;
    procedure SetBasePath(const Name, Path : string);
    procedure SetSaveFileName(const Name : string);
    procedure SetCurrentPath(const Name: string);
    function GetSaveFileName : string;
    procedure UpdateOnSelectChange;
    procedure ConditionKeyboard;
    procedure SelectFirstFile;
//    function PermissiveAccess : Boolean;
  public
    constructor Create(AOwner : TComponent); override;
    destructor Destroy; override;
    property BasePath : string read FBasePath;
    property SaveMode : Boolean read FSaveMode write SetSaveMode;
    property SaveFileName : string read GetSaveFileName write SetSaveFileName;
  end;

var
  FileLoadForm: TFileLoadForm;
  FLastFile: string;

function GeneralPluginSelectProgram(ReqPath, RespPath : PChar; Load : Boolean) : Boolean; stdcall;
//function RefreshBufferedPrograms : Boolean;

exports GeneralPluginSelectProgram;

implementation
function SlashSep(const Path, S: String): String;
begin
  if AnsiLastChar(Path)^ <> '\' then
    Result := Path + '\' + S
  else
    Result := Path + S;
end;


{$R *.DFM}

const
  GridHeight = 32;

function GetManifestOfOCT(Path : string; S : TStringList) : Boolean;
var I : Integer;
    Tmp : string;
begin
  Result := True;
  try
    S.LoadFromFile(Path);
    for I := 0 to S.Count - 1 do begin
      Tmp := S[I];
      S[I] := ReadQuotedString(Tmp);
    end;
  except
    Result := False;
  end;
end;

procedure DeleteAllFilesFromBuffered(const Path : string; WithOCT : Boolean = True);
var Directory : TDirectory;
    S : TStringList;
begin
  Directory := TDirectory.Create;
  S := TStringList.Create;

  try
    Directory.Manifest(Path + '*.opp', S, False);
    if WithOCT then
      Directory.Manifest(Path + '*.oct', S, False);
    while(S.Count > 0) do begin
      if FileExists(S[0]) then
        DeleteFile(S[0]);
      S.Delete(0);
    end;
  finally
    Directory.Free;
    S.Free;
  end
end;

function RefreshBufferedPrograms : Boolean;
var S : TStringList;
    PP : string;
    I : Integer;
begin
  Result := True;

  if (CellMode.LocalCellMode = cmProduction) and
     (CellMode.CellOCT <> '') then begin
    Result := True;
    CreateDirectory(PChar(CellMode.XFerPath), nil);
    DeleteAllFilesFromBuffered(CellMode.XFerPath);

    S := TStringList.Create;
    try
      if GetManifestOfOCT(CellMode.CellOCT, S) then begin // this is a bit wrong

        PP := CellMode.CellOPPPath + CellMode.CellPartTypeFromPath + '\';
        Named.EventLog(Format('CellPartTypeFromPath = %s', [CellMode.CellPartTypeFromPath]));
        for I := 0 to S.Count - 1 do begin
          if not CopyFile(PChar(PP + S[I]), PChar(CellMode.XFerPath + S[I]), False) then
            Exit;
        end;

        PP := CellMode.XFerPath;

        DeleteAllFilesFromBuffered(CellMode.BufferedPath, False);

        for I := 0 to S.Count - 1 do begin
          if not CopyFile(PChar(PP + S[I]), PChar(CellMode.BufferedPath + S[I]), False) then
            raise Exception.Create('OH DEAR');
        end;

        Result := False;
      end;
    finally
      S.Free;
    end;
  end;
end;


function ClearAndCopyFiles(var SelectPath : string; const TargetPath : string; OPPMode : Boolean) : Boolean;
var Tmp : string;
    OPPPath : string;
    I : Integer;
    Directory : TDirectory;
    S : TStringList;
begin
  Result := True;
  // Stage 1 remove all files from the buffed path

  if CellMode.LocalCellMode <> cmRework then begin
    DeleteAllFilesFromBuffered(CellMode.BufferedPath);
    CellMode.CellOCT := ''; // Remove Cell OCT if not loading a rework program
  end;

  // Stage 2 determine mode and copy the selected files
  case CellMode.LocalCellMode of
    cmProduction : begin
      Tmp := SelectPath;
    // Determine the part type from the SelectPath and therefore calculate the
    // OPP path
    // The SelectPath = CellMode.CellOCTPath/parttype/...../filename.oct
    // Strip the CellMode.CellOCTPath from the beginning
      System.Delete(Tmp, 1, Length(CellMode.CellOCTPath));
    // This should include the trailing backslash of CellOCTPath, now find the
    // position of the first '\' and copy this to part type
      I := Pos('\', Tmp);
      if I = 0 then begin
        Named.SetFaultA('OCT', 'Invalid path to part configuration', FaultRewind, nil);
        Result := False;
        Exit;
      end;

      System.Delete(Tmp, I, Length(Tmp)); // remove trailing backslash
      CellMode.CellPartTypeFromPath := Tmp;

      OPPPath := CellMode.CellOPPPath + CellMode.CellPartTypeFromPath + '\';

      Directory := TDirectory.Create;
      Directory.RelativePaths := True;
      S := TStringList.Create;
      try
        Result := GetManifestOfOCT(SelectPath, S);
        if Result then begin
          while S.Count > 0 do begin
            if not CopyFile(PChar(OPPPath + S[0]), PChar(CellMode.BufferedPath + S[0]), False) then begin
              Result := False;
              Named.SetFaultA('OCT', Format('Could not copy configuration opp %s', [S[0]]), FaultRewind, nil);
              Named.SetFaultA('OCT', Format('Could not copy configuration opp %s', [S[0]]), FaultInterlock, nil);
            end;
            S.Delete(0);
          end;
        end;
      finally
        Directory.Free;
        S.Free;
      end;

      CopyFile(PChar(SelectPath), PChar(CellMode.BufferedPath + ExtractFileName(SelectPath)), False);
      SelectPath := CellMode.BufferedPath + ExtractFileName(SelectPath);
    end;

    cmStandalone,
    cmRework,
    cmDevelopment : Exit;
  end;
end;

{**********************************************************************************************************
  This is the called function from ACNC core that starts the whole process of displaying the form.
**********************************************************************************************************}
function GeneralPluginSelectProgram(ReqPath, RespPath : PChar; Load : Boolean) : Boolean;
var Form : TFileLoadForm;
    FileMask : string;
    BasePath : string;
    BasePathName : string;
    SelectedPath: string;
    OPPMode : Boolean;
    NewPath : string;
    DirName : string;
    Tmp1 : Integer;
    Trans: TTranslator;
begin
  FileMask := ExtractFileExt(ReqPath);
  OPPMode := FileMask = '.opp';
  SelectedPath:= '';
  Result := False;

  Trans:= TTranslator.Create;

  try
  { The initial case statements here are used to set the file path }
    case CellMode.LocalCellMode of
      cmProduction : begin
        BasePath := CellMode.CellOCTPath;
        BasePathName := Trans.GetString('$Cell');
        if OPPMode then begin
          Named.SetFault('FileLoad', 'Cannot load OPP in cell mode', FaultInterlock);
          Exit;
          {
        end else if OCT.RunEnable then begin
          Named.SetFault('FileLoad', 'Cannot load OCT unless current OCT is complete', FaultInterlock);
          Exit; }
        end;
      end;

      cmStandalone : begin
        if Pos(DllLocalPath + 'programs\', FLastFile) <> 0 then begin
          if FileExists(FLastFile) then begin
            SelectedPath:= ExtractFilePath(FLastFile);
            SelectedPath:= Copy(SelectedPath, Length(DllLocalPath + 'programs\') + 1, Length(SelectedPath));
          end;
        end;
        BasePath := DllLocalPath + 'programs\';
        BasePathName := Trans.GetString('$Local');
      end;

      cmRework : begin
        if CellMode.CellPartTypeFromPath = '' then begin
          BasePathName := Trans.GetString('$Rework');
          BasePath := CellMode.ReworkPath;
        end else begin
          BasePathName := Trans.GetString('$Rework') + '\' + CellPartTypeFromPath + '\';
          BasePath := CellMode.ReworkPath + CellPartTypeFromPath + '\';
        end;

        if not OPPMode then begin
          Named.SetFault('FileLoad', 'Cannot load OCT in rework mode', FaultInterlock);
          Exit;
        end;
      end;

      cmDevelopment : begin
        BasePath := DllLocalPath + 'programs\';
        BasePathName := Trans.GetString('$Local');
  //      BasePath := CellMode.DevelopmentPath;
  //      BasePathName := '$Devel';
      end;
    end;
    {  }
    if Load then begin
      Result := False;
      if not DirectoryExists(ExcludeTrailingBackslash(BasePath)) then begin
        Named.SetFault('FileLoad', PChar('Base path is not accessable: ' + CarRet + BasePath), FaultInterlock);
        Exit;
      end;

      Form := TFileLoadForm.Create(nil);
      try
        Form.FileMask := '*' + FileMask;
        Form.SetBasePath(BasePathName, BasePath);
        Form.SetCurrentPath(SelectedPath);
        Form.RefreshDirectory;

        Result := Form.ShowModal = mrOK;
        if Result then begin
          CellMode.CellPartTypeFromPath := '';
          NewPath := Form.FilePath;
          DirName := ExtractFileDir(NewPath);
          if CompareStr(DirName,Basepath + 'DeletedPrograms') = 0 then
            NewPath := FlastFile
          else
            FLastFile:= NewPath;

          Result := ClearAndCopyFiles(NewPath, CellMode.BufferedPath, OPPMode);

          if Result then begin
            FileLoadDialog.RefreshBufferedPrograms;
            case CellMode.LocalCellMode of
              cmProduction : begin
                if not OPPMode then
                  CellMode.CellOCT := NewPath; // Record the active cell program
                end;
            end;

            StrPLCopy(RespPath, NewPath, 256);
          end;
        end;

        finally
        Form.Free;
      end;
    end else begin
      Form := TFileLoadForm.Create(nil);
      try
        Form.FileMask := '*' + FileMask;
        Form.SetBasePath(BasePathName, BasePath);
        Form.RefreshDirectory;
        Form.SaveMode := True;
        Form.SaveFileName := ExtractFileName(ReqPath);

        Result := Form.ShowModal = mrOK;
        if Result then begin
          NewPath := Form.FilePath;
          if ExtractFileExt(NewPath) = '' then
            NewPath := NewPath + '.opp';

          StrPLCopy(RespPath, NewPath, 256);
        end;
      finally
        Form.Free;
      end;
    end;
  finally
    Trans.Free;
  end
{var Form : TFileLoadForm;
    FileMask : string;
    BasePath : string;
    BasePathName : string;
    NewPath : string;
begin
  FileMask := ExtractFileExt(ReqPath);

  BasePath := DllLocalPath + 'programs\';
  BasePathName := '$Local';

  if Load then begin
    Result := False;
    if not DirectoryExists(SysUtils.ExcludeTrailingBackslash(BasePath)) then begin
      Named.SetFault('FileLoad', 'Base path is not accessable', FaultInterlock);
      Exit;
    end;

    Form := TFileLoadForm.Create(nil);
    try
      Form.FileMask := '*' + FileMask;
      Form.SetBasePath(BasePathName, BasePath);
      Form.SortMode := smByNameFwd;
      Form.RefreshDirectory;
      Form.ConditionSortButtons;
      fORM.KBMode := kbmHide;
      Form.ConditionKeyboard;
      Form.SelectFirstFile;


      Result := Form.ShowModal = mrOK;

        if Result then begin
          NewPath := Form.FilePath;
          StrPLCopy(RespPath, NewPath, 256);
        end;
    finally
      Form.Free;
    end;
  end else begin
    Form := TFileLoadForm.Create(nil);
    try
      Form.FileMask := '*' + FileMask;
      Form.SetBasePath(BasePathName, BasePath);
      Form.RefreshDirectory;
      Form.SaveMode := True;
      Form.SaveFileName := ExtractFileName(ReqPath);

      Result := Form.ShowModal = mrOK;
      if Result then begin
        NewPath := Form.FilePath;
        if ExtractFileExt(NewPath) = '' then
          NewPath := NewPath + '.opp';
        StrPLCopy(RespPath, NewPath, 256);
      end;
    finally
      Form.Free;
    end;
  end; }
end;

{ ***********  TFileLoadForm  ************
 12/09/2013 - DCG Added the delete of files to OPP loading form.
}
constructor TFileLoadForm.Create(AOwner: TComponent);
begin
  inherited;
  trans:= TTranslator.Create;
  FileSortList := TFileList.create(Self);
   SortMode := smByNameFwd;

  CurrentPath := '';
  FileMask := '*.opp';

  KB := TBaseKB.Create(Self);
  KB.Parent := PanelKeyboard;
  KB.Align := alLeft;
  KB.Width := 740;
  KB.OnKey := DoKeyPress;

  FKB := TNumericPad.Create(Self);
  FKB.Parent := PanelKeyboard;
  FKB.Align := alClient;

  BtnPathSwitch.Visible := False; //(CellMode.LocalCellMode = rrmDevelopment) and SaveMode;

  FileGrid.ColCount := 3;
  FileGrid.ColWidths[0] := 470;
  FileGrid.ColWidths[1] := 100;
  FileGrid.ColWidths[2] := 100;
  FileGrid.Cells[0, 0] := trans.getString('$FILE_NAME');
  FileGrid.ColWidths[2] := 100;
  FileGrid.Cells[1, 0] := trans.getString('$TIME');
  FileGrid.Cells[2, 0] := trans.getString('$SIZE');
  //FileGrid.RowHeights[0]:=60;

  Left := 0;
  Top := 72;
  Height := 768-72;
  Width := 1024;

  KBMode:= kbmHide;
  ConditionKeyboard;
end;

procedure TFileLoadForm.RefreshDirectory;
var Search : TSearchRec;
    Count : Integer;
    FileNumber : Integer;
    DirectoryNameList : TStringList;
    RowNumber : Integer;
begin
  Count := 0;
  DirGrid.RowCount := 1;
  DirGrid.Cells[0, 0] := Trans.GetString('$NODIRECTORIES');

  PathLabel.Caption := FBasePathName + CurrentPath;

  try
    DirectoryNameList := TStringList.Create;
    DirectoryNameList.CaseSensitive := False;

    if FindFirst(BasePath + CurrentPath + '*', faAnyFile, Search) = 0 then begin
      repeat
        if (Search.Name <> '.') and ((Search.Attr and faDirectory) <> 0) then begin
          if (Search.Name <> '..') or (CurrentPath <> '') then begin
            //DirGrid.Cells[0, Count] := Search.Name;
            DirectoryNameList.Add(Search.Name);
            Inc(Count);
            DirGrid.RowCount := Count;
            SElectedPath := '';
            SElectedFile := '';
            UpdateOnSelectChange;
          end;
        end;
      until FindNext(Search) <> 0;
    end;
    DirGrid.RowCount := Count;
  finally
    FindClose(Search);
    DirectoryNameList.Sort;
    if Count > 0 then
      begin
      For RowNumber := 0 to Count-1 do
        begin
        DirGrid.Cells[0, RowNumber] := DirectoryNameList[RowNumber];
        end;
      end;

    DirectoryNameList.Free;
  end;

  FileGrid.RowCount := 2;
  FileGrid.FixedRows := 1;
  FileGrid.Cells[0, 1] := NoFilesText;
  FileGrid.Cells[1, 1] := '';
  FileGrid.Cells[2, 1] := '';
  try

  with FileSortList do
    begin
    ClearFiles;
    Path := BasePath + CurrentPath;
    Filter := FileMask;
    FullDate := True;
    UpdateList;
    FileCount := FileSortList.NumberFiles;
    If FileCount < 2 then
      begin
      SortMode := smNone;
      end;

    case SortMode of
     smNone     : SortbyName(False); // this required to condition date/time string
     smbyNameFwd: SortbyName(False);
     smbyNameRev: SortbyName(True);
     smbyDateFwd: SortbyDate(False);
     smbyDateRev: SortbyDate(True);
     smbySizeFwd: SortbySize(False);
     smbySizeRev: SortbySize(True);
     end;
    end;

  If FileCount < 1 then
      FileGrid.RowCount := 2
    else
      FileGrid.RowCount := FileCount+1;


  For FileNumber := 0 to FileCount-1 do
    begin
    FileGrid.Cells[0,FileNumber+1] :=  FileSortList.NameList[FileNumber];
    FileGrid.Cells[1,FileNumber+1] :=  FileSortList.DateList[FileNumber];
    FileGrid.Cells[2,Filenumber+1] :=  FileSortList.SizeList[FileNumber];
    end
  finally
    FindClose(Search);
  end;
end;

procedure TFileLoadForm.DirGridDblClick(Sender: TObject);
var Select : string;
    Tmp : string;

begin
  if DirGrid.Row <> -1 then begin
    Select := DirGrid.Cells[0, DirGrid.Row];
    if Select <> '' then begin
      if Select = '..' then
        Tmp := ExtractFilePath(SysUtils.ExcludeTrailingBackslash(CurrentPath))
      else
        Tmp := CurrentPath + Select + '\';
    end;
    if DirectoryExists(BasePath + Tmp) then
      CurrentPath := Tmp;
  end;
  SelectedFile := '';
  SElectedPath := '';
  UpdateOnSelectChange;
  SortMode := smByNameFwd;
  RefreshDirectory;
  SelectFirstFile;
   //Added to check if deleted programs directory is selected
  if CompareStr(Currentpath,'DeletedPrograms\') = 0 then begin
    btnNew.IsEnabled := false;
    btnOk.IsEnabled := false;
    end
  else begin
    btnNew.IsEnabled := true;
    btnDelete.IsEnabled := false;
    btnOk.IsEnabled := true;
   end;
end;

procedure TFileLoadForm.BtnOKClick(Sender: TObject);
begin
  if not SaveMode then begin
    FilePath := BasePath + CurrentPath + SelectedFile;
    if FileExists(FilePath) then
      ModalResult := mrOK;
  end else begin
    FilePath := BasePath + CurrentPath + SaveEdit.Text;
    ModalResult := mrOK;
  end;
end;

procedure TFileLoadForm.BtnCancelClick(Sender: TObject);
begin
  ModalResult := mrCancel;
end;

procedure TFileLoadForm.FileGridSelectCell(Sender: TObject; ACol,
  ARow: Integer; var CanSelect: Boolean);
begin
  SelectedFile := FileGrid.Cells[0, ARow];
 // SElectedPath := BasePath + CurrentPath + SelectedFile;
 // UpdateOnSelectChange;
end;

procedure TFileLoadForm.SelectFirstFile;
var
SelRect : TGridRect;
begin
  SelRect := FileGrid.selection;
  SelRect.Top := 1;
  FileGrid.Selection := SelRect;
  SelectedFile := FileGrid.Cells[0, 1];
  SElectedPath := BasePath + CurrentPath + SelectedFile;
  UpdateOnSelectChange;
end;

procedure TFileLoadForm.FileGridClick(Sender: TObject);
begin
  SelectedFile := FileGrid.Cells[0, FileGrid.Row];
  SElectedPath := BasePath + CurrentPath + SelectedFile;
  UpdateOnSelectChange;
{  if SaveMode then
    SaveEdit.Text := SelectedFile;}
end;

procedure TFileLoadForm.SetBasePath(const Name, Path : string);
begin
  FBasePath := Path;
  FBasePathName := Name + '\';
end;

procedure TFileLoadForm.SetCurrentPath(const Name: string);
begin
  CurrentPath:= Name;
end;

procedure TFileLoadForm.SetSaveFileName(const Name : string);
begin
  SaveEdit.Text := Name;
end;

function TFileLoadForm.GetSaveFileName : string;
begin
  Result := SaveEdit.Text;
end;

procedure TFileLoadForm.DoKeyPress(Sender : TObject; Key: AnsiChar);
begin
  if Key = #$d then begin
    if DirGridActive then
      DirGridDblClick(Self)
    else
      BtnOKClick(Self);
  end;
end;

procedure TFileLoadForm.SetSaveMode(aMode : Boolean);
begin
  FSaveMode := aMode;
  SaveEdit.Enabled := SaveMode;
  BtnNew.Visible := not SaveMode;
  BtnDelete.Visible := not SaveMode;
end;

procedure TFileLoadForm.DirGridEnter(Sender: TObject);
begin
  DirGridActive := True;
end;

procedure TFileLoadForm.FileGridEnter(Sender: TObject);
begin
  DirGridActive := False;

  SelectedFile := FileGrid.Cells[0, FileGrid.Row];
end;

procedure TFileLoadForm.FormCreate(Sender: TObject);
begin
  SaveEdit.Text := '';
  SaveMode := False;
  SelectedPath := '';
  SelectedFile := '';
  BtnDelete.IsEnabled := False;
  AYS := TAreYouSure.Create(Self);
  FileCount := 0;

  BtnNew.Legend:= Trans.GetString('$NEW');
  BtnDelete.Legend:= Trans.GetString('$DELETE');
  BtnOK.Legend:= Trans.GetString('$OK');
  BtnCancel.Legend:= Trans.GetString('$CANCEL');
  BtnPathSwitch.Legend:= Trans.GetString('$PATH_SWITCH');
end;

procedure TFileLoadForm.DirGridDrawCell(Sender: TObject; ACol,
  ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  Grid : TStringGrid;
  HalfRect : TRect;
  RectHeight : INteger;
  Buffer : String;
  SepPos : Integer;
begin
  Grid := TStringGrid(Sender);
  if gdFixed in State then begin
    Grid.Canvas.Brush.Color := clBtnFace;
    Grid.Canvas.Pen.Color := clBlack;
  end else begin
    if gdSelected in State then begin
      if gdFocused in State then begin
        Grid.Canvas.Brush.Color := clYellow;
        Grid.Canvas.Pen.Color := clBlack;
      end else begin
        Grid.Canvas.Brush.Color := $f0f0d0;
        Grid.Canvas.Pen.Color := clBlack;
      end;
    end else begin
      Grid.Canvas.Brush.Color := $d0d0a0;
      Grid.Canvas.Pen.Color := clBlack;
    end;
  end;

  Grid.Canvas.Font := Grid.Font;
  Grid.Canvas.FillRect(Rect);
  if (ACol = 1)and(Arow>0) then
     begin
     RectHeight := Rect.Bottom-Rect.Top;
     Halfrect := Rect;
     HalfRect.Bottom := HalfRect.Bottom-(RectHeight div 2);
     Grid.Canvas.Font.Size := 10;
     Buffer := Grid.Cells[ACol, ARow];
     SepPos := Pos('~',Buffer);
     Grid.Canvas.TextRect(HalfRect, HalfRect.Left, HalfRect.Top, Copy(Buffer,1,SepPos-1));
     HalfRect.Bottom := Rect.Bottom;
     HalfRect.Top := HalfRect.Top+(RectHeight div 2)+1;
     Grid.Canvas.TextRect(HalfRect, HalfRect.Left, HalfRect.Top, Copy(Buffer,SepPos+1,1000));
     end
  else
    begin
    Grid.Canvas.TextRect(Rect, Rect.Left, Rect.Top, Grid.Cells[ACol, ARow]);
    end;
end;

procedure TFileLoadForm.FileGridKeyPress(Sender: TObject; var Key: Char);
var
  RowStartChar : String;
  PartName : String;
  SearchKey : String;
  Found : Boolean;
  RowNumber : Integer;
  UpdateGrid: TStringGrid;
begin
  if Key = #$d then
    DoKeyPress(nil, Key);

  if Sender is TStringGrid then begin
    UpdateGrid:= TStringGrid(Sender);
    if UpdateGrid.RowCount > 1 then begin
      SearChKey := Uppercase(Key);
      Found := False;
      RowNumber := 0;
      while (not Found) and (RowNumber < UpdateGrid.RowCount) do begin
        try
          PartName := UpdateGrid.Cols[0][RowNumber];
          RowStartChar := UpperCase(PartName[1]);
          if RowStartChar = SearChKey then
            Found := True
          else
            inc(RowNumber);
        except
          inc(RowNumber)
        end
      end;
      if Found and (RowNumber <= UpdateGrid.RowCount) then
        UpdateGrid.Row := RowNumber;
    end;
  end;
end;

procedure TFileLoadForm.BtnDeleteClick(Sender: TObject);
begin
if SelectedFile <> '' then
   begin
   If FileExists(SelectedPath) then
      begin
      AYS.Mode := aysAreYouSure;
      AYS.Message1 := Trans.Getstring( '$AYSDELETEOPP');
      AYS.Message2 := SelectedFile;
      if AYS.ShowModal = mrOK then
         begin
         if FileExists(SelectedPath) then
             begin
             CopyandDelete(SelectedPath);
             RefreshDirectory;
             SelectedFile := FileGrid.Cells[0, FileGrid.Row];
             end;
         end
      end;
   end;
end;

procedure TFileLoadForm.UpdateOnSelectChange;
begin
 SaveEdit.Text := SelectedFile;
 BtnDelete.IsEnabled := FileExists(SelectedPath);
end;

procedure TFileLoadForm.SaveEditChange(Sender: TObject);
begin
//BtnDelete.IsEnabled := FileExists(SelectedPath);
end;

// When delete is pressed the file to be deleted is transferred to the deleted file folder.
function TFileLoadForm.CopyandDelete(OPPPath : String): Boolean;
var
PSource    : array[0..300] of Char;
PDest      : array[0..300] of Char;
DeleteName : String;
DestString : String;
DirName :string;

begin
Result := False;
if not FileExists(OPPPath) then exit;
if Not DirectoryExists(ExtractFilePath(OPPPath)) then exit;
DirName := ExtractFileDir(OPPPath);
DeleteName := ExtractFilename(OppPath);

DestString :=  SlashSep(Basepath + 'DeletedPrograms\',DeleteName)+'-'+MakeFileTime;
StrPCopy(PDest,DestString);
StrPCopy(PSource,OPPPath);
//Check that files directory for deletion is the deleted programs directory
if (CompareStr(DirName,Basepath + 'DeletedPrograms') = 0)  then
 begin
   DeleteFile(PSource);
   Result := true;
   exit;
 end;
if MoveFile(PSource,PDest) then Result := True;
end;

function TFileLoadForm.MakeFileTime:String;
var
  Hour,Min,Sec,mSec  : Word;
  Year,Month,Day     : Word;
  YStr,MStr,DStr     : String;
  HStr,MinStr,SecStr : String;
begin
  DecodeDate(Now,Year,Month,Day);
  if Year < 10 then YStr := Format('0%d-',[Year]) else YStr := Format('%d-',[Year]);
  if Month  < 10 then MStr := Format('0%d-',[Month]) else MStr := Format('%d-',[Month]);
  if Day  < 10 then DStr := Format('0%d-',[Day]) else DStr := Format('%d-',[Day]);

  DecodeTime(Now,Hour,Min,Sec,mSec);
  if Hour < 10 then HStr := Format('0%d-',[Hour]) else HStr := Format('%d-',[Hour]);
  if Min  < 10 then MinStr := Format('0%d-',[Min]) else MinStr := Format('%d-',[Min]);
  if Sec  < 10 then SEcStr := Format('0%d',[SEc]) else SEcStr := Format('%d',[SEc]);
  Result :=YStr+MStr+DStr+HStr+MinStr+SecStr;
end;


procedure TFileLoadForm.BtnNewClick(Sender: TObject);
var
  NewPath : String;
  NewFile : TStringList;
begin
  AYS.Mode := aysFileNameInput;
  AYS.Message1 := Trans.getstring('$INPUT_NEW_FILENAME');
  AYS.Message2 := '';
  AYS.ShowModal;
  if AYS.ModalResult = mrOK then
     begin
     SaveEdit.Text := AYS.SaveFile;
     end
  else
     begin
     SaveEdit.Text := '';
     end;
  if SaveEdit.Text = '' then exit;

  NewPath := BasePath+CurrentPath+SaveEdit.Text;
  if ExtractFileExt(NewPath) = '' then
     NewPath := NewPath + '.opp';
  if FileExists(NewPath) then
     begin
     AYS.Mode := aysMessageOnly;
     AYS.Message1 := Trans.GetString('$FILEEXISTS');
     AYS.Message2 := Trans.GetString('$CAN_NOT_CREATE');
     AYS.ShowModal;
     end
  else
     begin
     NewFile := TStringList.Create;
     try
     NewFile.Add('[NC32GCODE]');
     NewFile.Add('%Main');
     NewFile.Add('G04 F1');
     NewFile.Add('M30');
     NewFile.SaveToFile(NewPath);
     finally
     NewFile.Free;
     end;
     end;
  RefreshDirectory;
end;

procedure TFileLoadForm.BtnShowKeyboardMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
if KbMode = kbmShow then
  begin
  KBMode := kbmHide;
  end
else
  begin
  KBMode := kbmShow;
  end;
ConditionKeyboard;
end;

procedure TFileLoadForm.ConditionKeyboard;
begin
case kbMode of
  kbmHide:
  begin
  PanelKeyboard.Height := 0;
  BtnShowKeyboard.Legend := trans.GetString('$KEYBOARD');
  end;
  kbmShow:
  begin
  PanelKeyboard.Height := 242;
  BtnShowKeyboard.Legend := trans.GetString('$KEYBOARD');
  end;
end;

end;


destructor TFileLoadForm.Destroy;
begin
  trans.free;
  inherited;
end;


end.
