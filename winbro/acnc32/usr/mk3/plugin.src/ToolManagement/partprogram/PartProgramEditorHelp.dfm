object GCodeForm: TGCodeForm
  Left = 62
  Top = 110
  BorderIcons = []
  BorderStyle = bsNone
  Caption = 'GCodeForm'
  ClientHeight = 741
  ClientWidth = 1016
  Color = clSilver
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = True
  OnCreate = FormCreate
  OnKeyUp = FindTextEditKeyUp
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label7: TLabel
    Left = 10
    Top = 102
    Width = 78
    Height = 13
    Caption = 'First Eject Pulse '
  end
  object Label8: TLabel
    Left = 10
    Top = 120
    Width = 40
    Height = 13
    Caption = 'Duration'
  end
  object ControlPanel: TPanel
    Left = 772
    Top = 9
    Width = 245
    Height = 550
    BevelInner = bvLowered
    TabOrder = 0
    object SavePanel: TPanel
      Left = 2
      Top = 2
      Width = 241
      Height = 50
      Align = alTop
      BevelInner = bvLowered
      TabOrder = 1
      object OkButton: TTouchSoftkey
        Left = 2
        Top = 2
        Width = 97
        Height = 45
        Caption = 'OkButton'
        Font.Charset = ANSI_CHARSET
        Font.Color = clGreen
        Font.Height = -19
        Font.Name = 'Comic Sans MS'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        OnClick = OKButtonClick
        Legend = 'OK'
        TextSize = 14
        HasIndicator = False
        IndicatorOn = False
        IsEnabled = True
        ButtonStyle = skNormal
        ArrowColor = clBlack
        ButtonColor = clWhite
        LegendColor = clGreen
        IndicatorWidth = 20
        IndicatorHeight = 10
        IndicatorOnColor = clGreen
        IndicatorOffColor = clSilver
        MaxFontSize = 0
      end
      object CancelButton: TTouchSoftkey
        Left = 142
        Top = 2
        Width = 97
        Height = 45
        Caption = 'CancelButton'
        Font.Charset = ANSI_CHARSET
        Font.Color = clRed
        Font.Height = -19
        Font.Name = 'Comic Sans MS'
        Font.Style = []
        ParentFont = False
        TabOrder = 1
        OnClick = CancelButtonClick
        Legend = 'Cancel'
        TextSize = 14
        HasIndicator = False
        IndicatorOn = False
        IsEnabled = True
        ButtonStyle = skNormal
        ArrowColor = clBlack
        ButtonColor = clWhite
        LegendColor = clRed
        IndicatorWidth = 20
        IndicatorHeight = 10
        IndicatorOnColor = clGreen
        IndicatorOffColor = clSilver
        MaxFontSize = 0
      end
    end
    object FindPanel: TPanel
      Left = 2
      Top = 52
      Width = 241
      Height = 50
      Align = alTop
      BevelInner = bvLowered
      TabOrder = 0
      object FindandReplaceButton: TSpeedButton
        Left = 112
        Top = 2
        Width = 114
        Height = 45
        AllowAllUp = True
        GroupIndex = 2
        Caption = 'Find and Replace'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        Glyph.Data = {
          EE050000424DEE05000000000000360400002800000014000000160000000100
          080000000000B801000000000000000000000001000000010000000000000000
          80000080000000808000800000008000800080800000C0C0C000C0DCC000F0CA
          A60004040400080808000C0C0C0011111100161616001C1C1C00222222002929
          2900555555004D4D4D004242420039393900807CFF005050FF009300D600FFEC
          CC00C6D6EF00D6E7E70090A9AD000000330000006600000099000000CC000033
          00000033330000336600003399000033CC000033FF0000660000006633000066
          6600006699000066CC000066FF00009900000099330000996600009999000099
          CC000099FF0000CC000000CC330000CC660000CC990000CCCC0000CCFF0000FF
          660000FF990000FFCC00330000003300330033006600330099003300CC003300
          FF00333300003333330033336600333399003333CC003333FF00336600003366
          330033666600336699003366CC003366FF003399000033993300339966003399
          99003399CC003399FF0033CC000033CC330033CC660033CC990033CCCC0033CC
          FF0033FF330033FF660033FF990033FFCC0033FFFF0066000000660033006600
          6600660099006600CC006600FF00663300006633330066336600663399006633
          CC006633FF00666600006666330066666600666699006666CC00669900006699
          330066996600669999006699CC006699FF0066CC000066CC330066CC990066CC
          CC0066CCFF0066FF000066FF330066FF990066FFCC00CC00FF00FF00CC009999
          000099339900990099009900CC009900000099333300990066009933CC009900
          FF00996600009966330099336600996699009966CC009933FF00999933009999
          6600999999009999CC009999FF0099CC000099CC330066CC660099CC990099CC
          CC0099CCFF0099FF000099FF330099CC660099FF990099FFCC0099FFFF00CC00
          000099003300CC006600CC009900CC00CC0099330000CC333300CC336600CC33
          9900CC33CC00CC33FF00CC660000CC66330099666600CC669900CC66CC009966
          FF00CC990000CC993300CC996600CC999900CC99CC00CC99FF00CCCC0000CCCC
          3300CCCC6600CCCC9900CCCCCC00CCCCFF00CCFF0000CCFF330099FF6600CCFF
          9900CCFFCC00CCFFFF00CC003300FF006600FF009900CC330000FF333300FF33
          6600FF339900FF33CC00FF33FF00FF660000FF663300CC666600FF669900FF66
          CC00CC66FF00FF990000FF993300FF996600FF999900FF99CC00FF99FF00FFCC
          0000FFCC3300FFCC6600FFCC9900FFCCCC00FFCCFF00FFFF3300CCFF6600FFFF
          9900FFFFCC006666FF0066FF660066FFFF00FF666600FF66FF00FFFF66002100
          A5005F5F5F00777777008686860096969600CBCBCB00B2B2B200D7D7D700DDDD
          DD00E3E3E300EAEAEA00F1F1F100F8F8F800F0FBFF00A4A0A000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00070707070707
          0707070707070707070707070707070707070707070707070707070707070707
          0707070707070707070707070707070707070707070707070707070707070707
          0707070707070707070707070707070707070707070707070707070707070707
          0707070707070707070707070707070707070707070707070707070707070707
          0707070707070707070707070707070707070707070707070707070000000000
          07070707070000000000070707070700FF000000070707070700FF0000000707
          07070700FF000000070707070700FF0000000707070707000000000000000700
          00000000000007070707070000FF000000000000FF0000000000070707070700
          00FF000000070000FF000000000007070707070000FF000000070000FF000000
          000007070707070700000000000000000000000000070707070707070700FF00
          00000700FF000000070707070707070707000000000007000000000007070707
          070707070707000000070707000000070707070707070707070700FF00070707
          00FF000707070707070707070707000000070707000000070707070707070707
          070707070707070707070707070707070707}
        Layout = blGlyphTop
        ParentFont = False
        OnClick = FindButtonClick
      end
      object FindButton: TSpeedButton
        Left = 2
        Top = 2
        Width = 103
        Height = 45
        AllowAllUp = True
        GroupIndex = 2
        Caption = 'Find'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        Glyph.Data = {
          EE050000424DEE05000000000000360400002800000014000000160000000100
          080000000000B801000000000000000000000001000000010000000000000000
          80000080000000808000800000008000800080800000C0C0C000C0DCC000F0CA
          A60004040400080808000C0C0C0011111100161616001C1C1C00222222002929
          2900555555004D4D4D004242420039393900807CFF005050FF009300D600FFEC
          CC00C6D6EF00D6E7E70090A9AD000000330000006600000099000000CC000033
          00000033330000336600003399000033CC000033FF0000660000006633000066
          6600006699000066CC000066FF00009900000099330000996600009999000099
          CC000099FF0000CC000000CC330000CC660000CC990000CCCC0000CCFF0000FF
          660000FF990000FFCC00330000003300330033006600330099003300CC003300
          FF00333300003333330033336600333399003333CC003333FF00336600003366
          330033666600336699003366CC003366FF003399000033993300339966003399
          99003399CC003399FF0033CC000033CC330033CC660033CC990033CCCC0033CC
          FF0033FF330033FF660033FF990033FFCC0033FFFF0066000000660033006600
          6600660099006600CC006600FF00663300006633330066336600663399006633
          CC006633FF00666600006666330066666600666699006666CC00669900006699
          330066996600669999006699CC006699FF0066CC000066CC330066CC990066CC
          CC0066CCFF0066FF000066FF330066FF990066FFCC00CC00FF00FF00CC009999
          000099339900990099009900CC009900000099333300990066009933CC009900
          FF00996600009966330099336600996699009966CC009933FF00999933009999
          6600999999009999CC009999FF0099CC000099CC330066CC660099CC990099CC
          CC0099CCFF0099FF000099FF330099CC660099FF990099FFCC0099FFFF00CC00
          000099003300CC006600CC009900CC00CC0099330000CC333300CC336600CC33
          9900CC33CC00CC33FF00CC660000CC66330099666600CC669900CC66CC009966
          FF00CC990000CC993300CC996600CC999900CC99CC00CC99FF00CCCC0000CCCC
          3300CCCC6600CCCC9900CCCCCC00CCCCFF00CCFF0000CCFF330099FF6600CCFF
          9900CCFFCC00CCFFFF00CC003300FF006600FF009900CC330000FF333300FF33
          6600FF339900FF33CC00FF33FF00FF660000FF663300CC666600FF669900FF66
          CC00CC66FF00FF990000FF993300FF996600FF999900FF99CC00FF99FF00FFCC
          0000FFCC3300FFCC6600FFCC9900FFCCCC00FFCCFF00FFFF3300CCFF6600FFFF
          9900FFFFCC006666FF0066FF660066FFFF00FF666600FF66FF00FFFF66002100
          A5005F5F5F00777777008686860096969600CBCBCB00B2B2B200D7D7D700DDDD
          DD00E3E3E300EAEAEA00F1F1F100F8F8F800F0FBFF00A4A0A000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00070707070707
          0707070707070707070707070707070707070707070707070707070707070707
          0707070707070707070707070707070707070707070707070707070707070707
          0707070707070707070707070707070707070707070707070707070707070707
          0707070707070707070707070707070707070707070707070707070707070707
          0707070707070707070707070707070707070707070707070707070000000000
          07070707070000000000070707070700FF000000070707070700FF0000000707
          07070700FF000000070707070700FF0000000707070707000000000000000700
          00000000000007070707070000FF000000000000FF0000000000070707070700
          00FF000000070000FF000000000007070707070000FF000000070000FF000000
          000007070707070700000000000000000000000000070707070707070700FF00
          00000700FF000000070707070707070707000000000007000000000007070707
          070707070707000000070707000000070707070707070707070700FF00070707
          00FF000707070707070707070707000000070707000000070707070707070707
          070707070707070707070707070707070707}
        Layout = blGlyphTop
        ParentFont = False
        OnClick = FindButtonClick
      end
    end
    object FindModePanel: TPanel
      Left = 2
      Top = 102
      Width = 241
      Height = 286
      Align = alClient
      BevelInner = bvLowered
      TabOrder = 2
      object SearchPages: TPageControl
        Left = 2
        Top = 2
        Width = 237
        Height = 282
        ActivePage = logoSheet
        Align = alClient
        TabHeight = 10
        TabOrder = 0
        object FindSheet: TTabSheet
          Caption = 'FindSheet'
          TabVisible = False
          object FindTextEdit: TSHCursorEdit
            Left = 4
            Top = 8
            Width = 225
            Height = 26
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -16
            Font.Name = 'Verdana'
            Font.Style = []
            ParentFont = False
            TabOrder = 0
            OnKeyUp = FindTextEditKeyUp
          end
          object SoptionsPanel: TPanel
            Left = 0
            Top = 74
            Width = 229
            Height = 198
            Align = alBottom
            TabOrder = 1
            object FindWordButton: TSpeedButton
              Left = 3
              Top = 65
              Width = 110
              Height = 50
              AllowAllUp = True
              GroupIndex = 3
              Caption = 'Whole Word'
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              Glyph.Data = {
                76010000424D7601000000000000760000002800000020000000100000000100
                04000000000000010000130B0000130B00001000000000000000000000000000
                800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
                FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
                333333333333333333FF33333333333330003FF3FFFFF3333777003000003333
                300077F777773F333777E00BFBFB033333337773333F7F33333FE0BFBF000333
                330077F3337773F33377E0FBFBFBF033330077F3333FF7FFF377E0BFBF000000
                333377F3337777773F3FE0FBFBFBFBFB039977F33FFFFFFF7377E0BF00000000
                339977FF777777773377000BFB03333333337773FF733333333F333000333333
                3300333777333333337733333333333333003333333333333377333333333333
                333333333333333333FF33333333333330003333333333333777333333333333
                3000333333333333377733333333333333333333333333333333}
              Layout = blGlyphTop
              NumGlyphs = 2
              ParentFont = False
              OnClick = FindWordButtonClick
            end
            object MatchCaseButton: TSpeedButton
              Left = 115
              Top = 65
              Width = 110
              Height = 50
              AllowAllUp = True
              GroupIndex = 4
              Caption = 'Match Case'
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              Glyph.Data = {
                76010000424D7601000000000000760000002800000020000000100000000100
                04000000000000010000130B0000130B00001000000000000000000000000000
                800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
                FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
                333333333333333333FF33333333333330003FF3FFFFF3333777003000003333
                300077F777773F333777E00BFBFB033333337773333F7F33333FE0BFBF000333
                330077F3337773F33377E0FBFBFBF033330077F3333FF7FFF377E0BFBF000000
                333377F3337777773F3FE0FBFBFBFBFB039977F33FFFFFFF7377E0BF00000000
                339977FF777777773377000BFB03333333337773FF733333333F333000333333
                3300333777333333337733333333333333003333333333333377333333333333
                333333333333333333FF33333333333330003333333333333777333333333333
                3000333333333333377733333333333333333333333333333333}
              Layout = blGlyphTop
              NumGlyphs = 2
              ParentFont = False
              OnClick = MatchCaseButtonClick
              OnDblClick = PartTypeEditEnter
            end
            object FWdButton: TSpeedButton
              Left = 3
              Top = 128
              Width = 110
              Height = 50
              GroupIndex = 5
              Down = True
              Caption = 'Forwards'
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              Glyph.Data = {
                76010000424D7601000000000000760000002800000020000000100000000100
                04000000000000010000120B0000120B00001000000000000000000000000000
                800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
                FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333033333
                33333333373F33333333333330B03333333333337F7F33333333333330F03333
                333333337F7FF3333333333330B00333333333337F773FF33333333330F0F003
                333333337F7F773F3333333330B0B0B0333333337F7F7F7F3333333300F0F0F0
                333333377F73737F33333330B0BFBFB03333337F7F33337F33333330F0FBFBF0
                3333337F7333337F33333330BFBFBFB033333373F3333373333333330BFBFB03
                33333337FFFFF7FF3333333300000000333333377777777F333333330EEEEEE0
                33333337FFFFFF7FF3333333000000000333333777777777F33333330000000B
                03333337777777F7F33333330000000003333337777777773333}
              Layout = blGlyphTop
              NumGlyphs = 2
              ParentFont = False
              OnClick = FWdButtonClick
            end
            object BackwardButton: TSpeedButton
              Left = 115
              Top = 128
              Width = 110
              Height = 50
              GroupIndex = 5
              Caption = 'Backwards'
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              Glyph.Data = {
                76010000424D7601000000000000760000002800000020000000100000000100
                04000000000000010000120B0000120B00001000000000000000000000000000
                800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
                FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333000000000
                333333777777777F33333330B00000003333337F7777777F3333333000000000
                333333777777777F333333330EEEEEE033333337FFFFFF7F3333333300000000
                333333377777777F3333333330BFBFB03333333373333373F33333330BFBFBFB
                03333337F33333F7F33333330FBFBF0F03333337F33337F7F33333330BFBFB0B
                03333337F3F3F7F7333333330F0F0F0033333337F7F7F773333333330B0B0B03
                3333333737F7F7F333333333300F0F03333333337737F7F33333333333300B03
                333333333377F7F33333333333330F03333333333337F7F33333333333330B03
                3333333333373733333333333333303333333333333373333333}
              Layout = blGlyphTop
              NumGlyphs = 2
              ParentFont = False
              OnClick = BackwardButtonClick
            end
            object FindAgainButton: TSpeedButton
              Left = 8
              Top = 10
              Width = 110
              Height = 50
              AllowAllUp = True
              Caption = 'Find Next'
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              Glyph.Data = {
                9E060000424D9E0600000000000036040000280000001B000000160000000100
                0800000000006802000000000000000000000001000000010000000000000000
                80000080000000808000800000008000800080800000C0C0C000C0DCC000F0CA
                A60004040400080808000C0C0C0011111100161616001C1C1C00222222002929
                2900555555004D4D4D004242420039393900807CFF005050FF009300D600FFEC
                CC00C6D6EF00D6E7E70090A9AD000000330000006600000099000000CC000033
                00000033330000336600003399000033CC000033FF0000660000006633000066
                6600006699000066CC000066FF00009900000099330000996600009999000099
                CC000099FF0000CC000000CC330000CC660000CC990000CCCC0000CCFF0000FF
                660000FF990000FFCC00330000003300330033006600330099003300CC003300
                FF00333300003333330033336600333399003333CC003333FF00336600003366
                330033666600336699003366CC003366FF003399000033993300339966003399
                99003399CC003399FF0033CC000033CC330033CC660033CC990033CCCC0033CC
                FF0033FF330033FF660033FF990033FFCC0033FFFF0066000000660033006600
                6600660099006600CC006600FF00663300006633330066336600663399006633
                CC006633FF00666600006666330066666600666699006666CC00669900006699
                330066996600669999006699CC006699FF0066CC000066CC330066CC990066CC
                CC0066CCFF0066FF000066FF330066FF990066FFCC00CC00FF00FF00CC009999
                000099339900990099009900CC009900000099333300990066009933CC009900
                FF00996600009966330099336600996699009966CC009933FF00999933009999
                6600999999009999CC009999FF0099CC000099CC330066CC660099CC990099CC
                CC0099CCFF0099FF000099FF330099CC660099FF990099FFCC0099FFFF00CC00
                000099003300CC006600CC009900CC00CC0099330000CC333300CC336600CC33
                9900CC33CC00CC33FF00CC660000CC66330099666600CC669900CC66CC009966
                FF00CC990000CC993300CC996600CC999900CC99CC00CC99FF00CCCC0000CCCC
                3300CCCC6600CCCC9900CCCCCC00CCCCFF00CCFF0000CCFF330099FF6600CCFF
                9900CCFFCC00CCFFFF00CC003300FF006600FF009900CC330000FF333300FF33
                6600FF339900FF33CC00FF33FF00FF660000FF663300CC666600FF669900FF66
                CC00CC66FF00FF990000FF993300FF996600FF999900FF99CC00FF99FF00FFCC
                0000FFCC3300FFCC6600FFCC9900FFCCCC00FFCCFF00FFFF3300CCFF6600FFFF
                9900FFFFCC006666FF0066FF660066FFFF00FF666600FF66FF00FFFF66002100
                A5005F5F5F00777777008686860096969600CBCBCB00B2B2B200D7D7D700DDDD
                DD00E3E3E300EAEAEA00F1F1F100F8F8F800F0FBFF00A4A0A000808080000000
                FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00070707070707
                070707070707070707070707070707A10707070707FF07070707070707070707
                070707070707070707070707A107070707000707070707070707070707070707
                070707070707070707A107070700070707070707070707070707070707070707
                070707070707A107070007070707070707A1A1A1A1A1A1A1A1A1A1A1A1A1A1A1
                A1A1A1A10700070707070707070707070707070707070707070707070707A107
                07000707070707070707070707070707070707070707070707A1070707000707
                0707070707070707070707070707070707070707A10707070700070700000000
                000707070707000000000007070707A1070707070700070700FF000000070707
                070700FF0000000707070707070707070700070700FF000000070707070700FF
                0000000707070707070707070700070700000000000000070000000000000007
                0707070707070707070007070000FF000000000000FF00000000000707070707
                07070707070007070000FF000000070000FF0000000000070707070707070707
                070007070000FF000000070000FF000000000007070707070707070707000707
                07000000000000000000000000000707070707070707070707000707070700FF
                0000000700FF0000000707070707070707070707070007070707000000000007
                0000000000070707070707070707070707000707070707000000070707000000
                0707070707070707070707070700070707070700FF0007070700FF0007070707
                0707070707070707070007070707070000000707070000000707070707070707
                0707070707000707070707070707070707070707070707070707070707070707
                0700}
              Layout = blGlyphTop
              ParentFont = False
              OnClick = FindAgainButtonClick
            end
            object FindDoneKey: TTouchSoftkey
              Left = 128
              Top = 16
              Width = 97
              Height = 41
              Font.Charset = ANSI_CHARSET
              Font.Color = clGreen
              Font.Height = -19
              Font.Name = 'Comic Sans MS'
              Font.Style = []
              ParentFont = False
              TabOrder = 0
              OnMouseUp = FindDoneKeyMouseUp
              Legend = 'DONE'
              TextSize = 14
              HasIndicator = False
              IndicatorOn = False
              IsEnabled = True
              ButtonStyle = skNormal
              ArrowColor = clBlack
              ButtonColor = clWhite
              LegendColor = clBlack
              IndicatorWidth = 20
              IndicatorHeight = 10
              IndicatorOnColor = clGreen
              IndicatorOffColor = clSilver
              MaxFontSize = 0
            end
          end
        end
        object ReplaceSheet: TTabSheet
          Caption = 'ReplaceSheet'
          ImageIndex = 1
          TabVisible = False
          object ReplaceTextEdit: TSHCursorEdit
            Left = 4
            Top = 48
            Width = 225
            Height = 26
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -16
            Font.Name = 'Verdana'
            Font.Style = []
            ParentFont = False
            TabOrder = 0
            OnKeyUp = FindTextEditKeyUp
          end
        end
        object logoSheet: TTabSheet
          ImageIndex = 2
          TabVisible = False
        end
      end
    end
    object UndoControls: TPanel
      Left = 2
      Top = 388
      Width = 241
      Height = 160
      Align = alBottom
      BevelInner = bvLowered
      TabOrder = 3
      object UndoPanel: TPanel
        Left = 2
        Top = 44
        Width = 237
        Height = 50
        Align = alBottom
        TabOrder = 0
        object UndoKey: TSpeedButton
          Left = 2
          Top = 4
          Width = 115
          Height = 45
          AllowAllUp = True
          Caption = 'UNDO'
          Enabled = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            04000000000000010000130B0000130B00001000000000000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
            3333333333FFFFF3333333333999993333333333F77777FFF333333999999999
            3333333777333777FF3333993333339993333377FF3333377FF3399993333339
            993337777FF3333377F3393999333333993337F777FF333337FF993399933333
            399377F3777FF333377F993339993333399377F33777FF33377F993333999333
            399377F333777FF3377F993333399933399377F3333777FF377F993333339993
            399377FF3333777FF7733993333339993933373FF3333777F7F3399933333399
            99333773FF3333777733339993333339933333773FFFFFF77333333999999999
            3333333777333777333333333999993333333333377777333333}
          Layout = blGlyphTop
          NumGlyphs = 2
          ParentFont = False
          OnClick = UndoKeyClick
        end
        object RedoKey: TSpeedButton
          Left = 119
          Top = 4
          Width = 115
          Height = 45
          AllowAllUp = True
          Caption = 'REDO'
          Enabled = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            04000000000000010000130B0000130B00001000000000000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
            3333333333FFFFF3333333333999993333333333F77777FFF333333999999999
            3333333777333777FF33339993707399933333773337F3777FF3399933000339
            9933377333777F3377F3399333707333993337733337333337FF993333333333
            399377F33333F333377F993333303333399377F33337FF333373993333707333
            333377F333777F333333993333101333333377F333777F3FFFFF993333000399
            999377FF33777F77777F3993330003399993373FF3777F37777F399933000333
            99933773FF777F3F777F339993707399999333773F373F77777F333999999999
            3393333777333777337333333999993333333333377777333333}
          Layout = blGlyphTop
          NumGlyphs = 2
          ParentFont = False
          OnClick = RedoKeyClick
        end
      end
      object topBottomPanel: TPanel
        Left = 2
        Top = 94
        Width = 237
        Height = 64
        Align = alBottom
        TabOrder = 1
        object TopKey: TKBKey
          Left = 1
          Top = 1
          Width = 60
          Height = 60
          Beveled = True
          BorderStyle = bsSingle
          ButtonDirection = bdBottomUp
          MainCaption = 'Top'
          Color = clTeal
          ColorLED = lcBlue
          Depth = 3
          Font.Charset = ANSI_CHARSET
          Font.Color = clYellow
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ShiftFont.Charset = DEFAULT_CHARSET
          ShiftFont.Color = clWindowText
          ShiftFont.Height = -11
          ShiftFont.Name = 'MS Sans Serif'
          ShiftFont.Style = []
          ShowLED = False
          StateOn = False
          Switching = True
          ShowShiftedCharacter = False
          CanRepeat = False
          Temp = False
          OnMouseDown = TopKeyMouseDown
        end
        object BottomKey: TKBKey
          Left = 61
          Top = 1
          Width = 60
          Height = 60
          Beveled = True
          BorderStyle = bsSingle
          ButtonDirection = bdBottomUp
          MainCaption = 'Bottom'
          Color = clTeal
          ColorLED = lcBlue
          Depth = 3
          Font.Charset = ANSI_CHARSET
          Font.Color = clYellow
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ShiftFont.Charset = DEFAULT_CHARSET
          ShiftFont.Color = clWindowText
          ShiftFont.Height = -11
          ShiftFont.Name = 'MS Sans Serif'
          ShiftFont.Style = []
          ShowLED = False
          StateOn = False
          Switching = True
          ShowShiftedCharacter = False
          CanRepeat = False
          Temp = False
          OnClick = BottomKeyClick
          OnMouseDown = TopKeyMouseDown
        end
        object BookMarkKey: TTouchSoftkey
          Left = 126
          Top = 2
          Width = 111
          Height = 60
          Caption = 'BookmarkKey'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Comic Sans MS'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 0
          OnMouseUp = BookMarkKeyMouseUp
          Legend = 'BookMark~Control'
          TextSize = 14
          HasIndicator = False
          IndicatorOn = False
          IsEnabled = True
          ButtonStyle = skNormal
          ArrowColor = clBlack
          ButtonColor = clWhite
          LegendColor = clBlack
          IndicatorWidth = 20
          IndicatorHeight = 10
          IndicatorOnColor = clGreen
          IndicatorOffColor = clSilver
          MaxFontSize = 0
        end
      end
      object Panel7: TPanel
        Left = 2
        Top = 2
        Width = 237
        Height = 41
        Align = alTop
        BevelInner = bvLowered
        TabOrder = 2
        object BtnHideKB: TTouchGlyphSoftkey
          Left = 2
          Top = 2
          Width = 100
          Height = 37
          Legend = 'Hide K/B'
          TextSize = 0
          HasIndicator = False
          IndicatorOn = False
          IsEnabled = True
          ButtonColor = clInfoBk
          Layout = slGlyphLeft
          AutoTextSize = True
          KeyStyle = ssRectangle
          CornerRad = 10
          ButtonTravel = 2
          ShadowColor = clGray
          LegendFont.Charset = DEFAULT_CHARSET
          LegendFont.Color = clBlue
          LegendFont.Height = 23
          LegendFont.Name = 'Comic Sans MS'
          LegendFont.Style = []
          GlyphToEdgeSpacing = 1
          TextToEdgeBorder = 7
          MultiLineSpacing = 0
          MaxFontSize = 24
          OnMouseUp = BtnHideKBMouseUp
          align = alLeft
        end
        object BtnHelp: TTouchGlyphSoftkey
          Left = 135
          Top = 2
          Width = 100
          Height = 37
          Legend = 'Help (F1)'
          TextSize = 0
          HasIndicator = False
          IndicatorOn = False
          IsEnabled = True
          ButtonColor = clInfoBk
          Layout = slGlyphLeft
          AutoTextSize = True
          KeyStyle = ssRectangle
          CornerRad = 10
          ButtonTravel = 2
          ShadowColor = clGray
          LegendFont.Charset = DEFAULT_CHARSET
          LegendFont.Color = clGreen
          LegendFont.Height = 23
          LegendFont.Name = 'Comic Sans MS'
          LegendFont.Style = []
          GlyphToEdgeSpacing = 1
          TextToEdgeBorder = 7
          MultiLineSpacing = 0
          MaxFontSize = 24
          OnMouseUp = BtnHelpMouseUp
          align = alRight
        end
      end
    end
  end
  object PanelKB: TPanel
    Left = 0
    Top = 528
    Width = 1016
    Height = 213
    Align = alBottom
    TabOrder = 1
    object MainKb: TBaseKB
      Left = 1
      Top = 1
      Width = 777
      Height = 211
      Align = alLeft
      BevelOuter = bvNone
      Caption = 'MainKb'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -19
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      KeyboardMode = kbmAlphaNumeric
      HighlightColor = clBlack
      Options = []
      SecondaryFont.Charset = DEFAULT_CHARSET
      SecondaryFont.Color = clWindowText
      SecondaryFont.Height = -11
      SecondaryFont.Name = 'MS Sans Serif'
      SecondaryFont.Style = []
      RepeatDelay = 500
      ExternalMode = False
      FocusByHandle = False
      FocusHandle = 0
    end
    object KeyPad: TBaseFPad
      Left = 776
      Top = 10
      Width = 200
      Height = 250
      ExternalMode = False
      ChangeCursorKeys = True
      EnableCursorKeys = True
      object BitBtn1: TBitBtn
        Left = 136
        Top = 0
        Width = 64
        Height = 25
        Caption = 'Browse'
        TabOrder = 0
        TabStop = False
        OnClick = BitBtn1Click
      end
    end
  end
  object pp1panel: TPanel
    Left = 96
    Top = 68
    Width = 500
    Height = 500
    Caption = 'pp1panel'
    TabOrder = 2
    object pp1: TOPPSHEdit
      Left = 300
      Top = 85
      Width = 200
      Height = 100
      ShowBookMarkPanel = False
      FindModeRequested = pp1FindModeRequested
      FindAgainRequested = pp1FindAgainRequested
      F1HelpRequested = pp1F1HelpRequested
      EScapeKeyPressed = CancelButtonClick
      InhibitUpdate = False
      IndicateHelpAvailable = False
    end
    object HelpPanel: TPanel
      Left = 1
      Top = 99
      Width = 498
      Height = 400
      Align = alBottom
      Caption = 'HelpPanel'
      TabOrder = 1
      Visible = False
      object WebBrowser: TWebBrowser
        Left = 1
        Top = 1
        Width = 496
        Height = 378
        Align = alClient
        TabOrder = 0
        ControlData = {
          4C00000043330000112700000000000000000000000000000000000000000000
          000000004C000000000000000000000001000000E0D057007335CF11AE690800
          2B2E126208000000000000004C0000000114020000000000C000000000000046
          8000000000000000000000000000000000000000000000000000000000000000
          00000000000000000100000000000000000000000000000000000000}
      end
      object Panel2: TPanel
        Left = 1
        Top = 379
        Width = 496
        Height = 20
        Align = alBottom
        BevelOuter = bvLowered
        TabOrder = 1
        DesignSize = (
          496
          20)
        object CloseHelpBtn: TButton
          Left = 464
          Top = 0
          Width = 33
          Height = 25
          Anchors = [akTop, akRight]
          Caption = 'X'
          TabOrder = 0
          OnClick = CloseHelpBtnClick
        end
        object HomeBtn: TButton
          Left = 391
          Top = 0
          Width = 33
          Height = 25
          Anchors = [akTop, akRight]
          Caption = 'H'
          TabOrder = 1
          OnClick = HomeBtnClick
        end
        object NextBtn: TButton
          Left = 303
          Top = 0
          Width = 33
          Height = 25
          Anchors = [akTop, akRight]
          Caption = '>'
          TabOrder = 2
          OnClick = NextBtnClick
        end
        object PrevBtn: TButton
          Left = 271
          Top = 0
          Width = 33
          Height = 25
          Anchors = [akTop, akRight]
          Caption = '<'
          TabOrder = 3
          OnClick = PrevBtnClick
        end
        object Button1: TButton
          Left = 359
          Top = 0
          Width = 33
          Height = 25
          Anchors = [akTop, akRight]
          Caption = 'R'
          TabOrder = 4
          OnClick = Button1Click
        end
      end
    end
  end
  object FileOPen: TOpenDialog
    DefaultExt = 'opp'
    InitialDir = 'c:\acnc32\programs'
    Left = 976
    Top = 472
  end
  object StartTimer: TTimer
    Enabled = False
    Interval = 400
    OnTimer = StartTimerTimer
    Left = 972
    Top = 497
  end
  object Timer1: TTimer
    OnTimer = Timer1Timer
    Left = 600
    Top = 112
  end
end
