unit HSA5OPPHeaderEditor;
// Moded 06/Jan/2003 for Translation Texts
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ComCtrls, ExtCtrls,  Buttons, OleCtrls, OKBox, AbstractOPPPlugin,
  NamedPlugin, StreamTokenizer, IniFiles, ACNC_SHeditor, ACNC_plusmemo, Clipbrd,
  ACNC_plusGutter, ImgList, ACNC_TouchSoftkey, ACNC_BaseKeyBoards, INamedInterface,
  OPPHeader, OPPDoc, ShellApi, Math, LinkedLookUp, ACNC_TouchGlyphSoftkey, CNCTypes;

type
TLimitRecord = record
    FormValue     : String;
    MaxVal        : Double;
    MinVal        : Double;
    IsInteger     : Boolean;
    HelpString    : String;
    HTML_HelpFile : String;
    Valid         : Boolean;
    ConvertLimit  : Boolean;
    end;
THelpRecord = record
  HTMLFile : String;
  Hint     : String;
  end;
pLimitRecord = ^TLimitRecord;

TFindModes = (fmNone,fmFind,fmReplace);

TOppEditMode = (oeHeaderEdit);


  TGcodeForm = class(TForm)
    FileOPen: TOpenDialog;
    StartTimer: TTimer;
    OPPHeaderPanel: TPanel;
    PageControl1: TPageControl;
    TabOPPHeader: TTabSheet;
    Label7: TLabel;
    Label8: TLabel;
    PanelKB: TPanel;
    MainKb: TBaseKB;
    KeyPad: TBaseFPad;
    OPPPartPanel: TPanel;
    PartTypeLabel: TLabel;
    ReferenceLabel: TLabel;
    ToolTYpeLabel: TLabel;
    ToolUsageLabel: TLabel;
    CycleTimeLabel: TLabel;
    Label3: TLabel;
    HeaderOKButton: TSpeedButton;
    HeaderCancelButton: TSpeedButton;
    ToolFault: TLabel;
    OPNumberLabel: TLabel;
    OpNumberFaultLabel: TLabel;
    PartTypeEdit: TEdit;
    ReferenceEdit: TEdit;
    ToolTypeEdit: TEdit;
    ToolUsageEdit: TEdit;
    CycleTimeEdit: TEdit;
    ToolOffsetPanel: TPanel;
    ToolXOffsetLabel: TLabel;
    ToolYOffsetLabel: TLabel;
    ToolZOffsetLabel: TLabel;
    ToolXOffsetEdit: TEdit;
    ToolYOffsetEdit: TEdit;
    ToolZOffsetEdit: TEdit;
    OpNumberEdit: TEdit;
    procedure FormCreate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure OKButtonClick(Sender: TObject);
    procedure CancelButtonClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure StartTimerTimer(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure PartTypeEditEnter(Sender: TObject);
    procedure HeaderCancelButtonClick(Sender: TObject);
  private
    { Private declarations }

    ShowKB : Boolean;
    AEC_OPtionTag : TTagref;
    InchEntry : Boolean;
    LineNumber : Integer;
    SearchOptions     : TSearchSwitches;
    MapList           : TStringList;

    ToolTypeIsNumeric   : Boolean;
    AlphaToolTypeLength : Integer;
    MaxNumericToolCode  : Integer;
    ShowToolOffsets     : Boolean;
    FindMode            : TFindModes;

    MinOpNumber : Integer;
    MaxOpNumber : Integer;
    iniWordwrap : Boolean;

    EnableAECEditor               : Boolean;

    Translator : TTranslator;
    // Translation Strings
    TranslatedPartType        :String;
    TranslatedReference       : String;
    TranslatedToolTYpe        : String;
    TranslatedElectrodeCount  : String;
    TranslatedElectrodeMap    : String;
    TranslatedToolXOffset     : String;
    TranslatedToolYOffset     : String;
    TranslatedToolZOffset     : String;
    TranslatedOPNumber        : String;
    TranslatedToolUsage       : String;
    TranslatedCycleTime       : String;
    TranslatedOK              : String;
    TranslatedCancel          : String;
    TranslatedHideBookMark    : String;
    TranslatedBookMarkControl : String;
    TranslatedFind            : String;
    TranslatedFindAgain       : String;
    TranslatedWholeWord       : String;
    TranslatedMatchCase       : String;
    TranslatedForwards        : String;
    TranslatedBackwards       : String;
    TranslatedFindAndReplace  : String;
    TranslatedTop             : String;
    TranslatedBottom          : String;
    TranslatedUndo            : String;
    TranslatedRedo            : String;
    TranslatedDone            : String;
    TranslatedFindWord        : String;
    TranslatedReplaceAgain    : String;
    TranslatedDataError       : String;
    TranslatedMaxValue        : String;
    TranslatedMaxof           : String;
    TranslatedMust_Be_0_or    : String;
    TranslatedTrue            : String;
    TranslatedFalse           : String;
    TranslatedOPPHeader       : String;
    TranslatedHeaderData      : String;
    TranslatedInch            : String;
    TranslatedMetric          : String;
    TranslatedShowKB          : String;
    TranslatedHideKB          : String;
    DisplayTop                : Integer;
    DisplayHeight             : Integer;


    procedure CopyAxisPosition(Sender: TObject);
    procedure FocusOnEditor;
    procedure ReadINiFile;
    procedure ReadConfigFile;

  public
    EditMode : TOppEditMode;
    //GCode stuff
    FileString : string;
    //OPP Header Stuff
    OPPData: TOPPFile;
    ContextHelpEnabled : Boolean;
    procedure DataToForm;
    function FormToData: Boolean;

  end;


 TOPPHeaderEditor = class(TAbstractOPPPluginEditor)
  public
    class function Execute(var aSection: string): Boolean; override;
  end;



var
  GcodeForm: TGcodeForm;


implementation

{$R *.DFM}

procedure TGcodeForm.DataToForm;
begin

  with OPPData do begin
    PartTypeEdit.Text:= PartType;
    ReferenceEdit.Text:= Comment;
    try
    ToolTypeEdit.Text:= ToolTypeString;
    except
    ToolTypeEdit.Text:= 'DataError';
    end;

    try
      OpNumberEdit.Text := IntToStr(OpNumber);
    except
      OpNumberEdit.Text := '1';
    end;



    try
    ToolUsageEdit.Text:= Format('%6.2f', [ToolUsage]);
    except
    ToolUsageEdit.Text:= TranslatedDataError;
    end;

    try
    CycleTimeEdit.Text:= IntToStr(CycleTime);
    except
    CycleTimeEdit.Text:= TranslatedDataError;
    end;
    ToolXOffsetEdit.Text := Format('%.4f', [ToolXOffset]);
    ToolYOffsetEdit.Text := Format('%.4f', [ToolYOffset]);
    ToolZOffsetEdit.Text := Format('%.4f', [ToolZOffset]);
    end;

end;


function TGcodeForm.FormToData : Boolean;
var
Code : Integer;
Buffer : String;
begin
Result := True;
  with OPPData do begin
    PartType:= PartTypeEdit.Text;
    Comment:= ReferenceEdit.Text;
    try
      ToolTypeString:= ToolTypeEdit.Text;
    except
      Result := False;
    end;

    try
      OpNumber := StrToInt(OpNumberEdit.Text);
    except
      Result := False;
    end;

    try
      ToolUsage:= StrToFloat(ToolUsageEdit.Text);
    except
      Result := False;
    end;

    try
    CycleTime:=  StrToInt(CycleTimeEdit.Text);
    except
    Result := False;
    end;
    Val(ToolXOffsetEdit.Text, ToolXOffset, Code);
    Val(ToolYOffsetEdit.Text, ToolYOffset, Code);
    Val(ToolZOffsetEdit.Text, ToolZOffset, Code);
    end;
end;


class function TOPPHeaderEditor.Execute(var aSection : string): Boolean;
var
//  Form: TOPPHeaderForm;
  Form : TGCodeForm;
  S : TStreamQuoteTokenizer;
  SList : TStringList;
  tempint : Integer;
begin
  Form:= TGCodeForm.Create(Application);
  Form.AEC_OPtionTag :=Named.AquireTag('_OptionAECTooling','TIntegerTAG',Nil);
  TempInt := Named.GetAsInteger(Form.AEC_OPtionTag);
  if TempInt> 0 then
    begin
    Form.EnableAECEditor := True;
    end
  else
     begin
     Form.EnableAECEditor := False;
     end;

  S:= TStreamQuoteTokenizer.Create;
  SList := TStringList.Create;
  try
    S.SetStream(TStringStream.Create(aSection));
    S.Seperator := '=';
    S.QuoteChar := '''';
    S.Rewind;
    try
      Form.OPPData.Load(S);
    except
      on E : Exception do
        Application.ShowException(E);
    end;
    Form.DataToForm;
    Form.EditMode := oeHeaderEdit;
    Result:= Form.ShowModal = mrOK;
    if Result then begin
      if Form.FormToData then
         begin
         Form.OPPData.Save(SList);
         aSection := SList.Text;
         end
      else
          begin
          raise Exception.Create('Bad OPP Header Data. Changes NOT saved');
          end;
    end;
  finally
    S.Free;
    SList.Free;
    Form.Free
  end
end;




procedure TGcodeForm.FormCreate(Sender: TObject);
var I: Integer;
begin
  LineNumber := 0;
  InchEntry := False;
  ReadConfigFile;
  Translator := TTranslator.Create;
  try
  with Translator do
    begin
    TranslatedPartType := GetString('$PARTTYPE');
    TranslatedReference:= GetString('$REFERENCE');
    TranslatedToolTYpe := GetString('$TOOL_TYPE');
    TranslatedElectrodeCount  := GetString('$ELECTRODE_COUNT');
    TranslatedElectrodeMap := GetString('$ELECTRODE_MAP');
    TranslatedToolXOffset := GetString('$TOOL_X_OFFSET');
    TranslatedToolYOffset := GetString('$TOOL_Y_OFFSET');
    TranslatedToolZOffset := GetString('$TOOL_Z_OFFSET');
    TranslatedOPNumber    := GetString('$OP_NUMBER');
    TranslatedToolUsage   := GetString('$TOOL_USAGE');
    TranslatedCycleTime   := GetString('$CYCLE_TIME');
    TranslatedOK          := GetString('$OK');
    TranslatedCancel      := GetString('$CANCEL');
    TranslatedHideBookMark:= GetString('$HIDE_BOOKMARK');
    TranslatedBookMarkControl := GetString('$BOOKMARK_CONTROL');
    TranslatedFind            := GetString('$FIND');
    TranslatedFindAndReplace  := GetString('$FIND_AND_REPLACE');
    TranslatedTop             := GetString('$TOP');
    TranslatedBottom          := GetString('$BOTTOM');
    TranslatedUndo            := GetString('$UNDO');
    TranslatedRedo            := GetString('$REDO');
    TranslatedFindAgain       := GetString('$FIND_AGAIN');
    TranslatedWholeWord       := GetString('$WHOLE_WORD');
    TranslatedMatchCase       := GetString('$MATCH_CASE');
    TranslatedForwards        := GetString('$FORWARDS');
    TranslatedBackwards       := GetString('$BACKWARDS');
    TranslatedDone            := GetString('$DONE');
    TranslatedFindWord        := GetString('$FIND_WORD');
    TranslatedReplaceAgain    := GetString('$REPLACE_AGAIN');
    TranslatedDataError       := GetString('$DATA_ERROR');
    TranslatedMaxValue        := GetString('$MAX_VALUE');
    TranslatedMaxof           := GetString('$MAX_OF_%D_CHAR');
    TranslatedMust_Be_0_or    := GetString('$MUST_BE_0_OR');
    TranslatedTrue            := GetString('$TRUE');
    TranslatedFalse           := GetString('$FALSE');
    TranslatedOPPHeader       := GetString('$OPP_HEADER');
    TranslatedHeaderData      := GetString('$OPP_HEADER_DATA');
    TranslatedInch            := GetString('$INCH');
    TranslatedMetric          := GetString('$METRIC');
    TranslatedShowKB          := GetString('$SHOW_KB');
    TranslatedHideKB          := GetString('$HIDE_KB');
    end;
  finally
  Translator.Free;
  end;

  PartTypeLabel.Caption  := TranslatedPartType;
  ReferenceLabel.Caption := TranslatedReference;
  ToolTypeLabel.Caption  := TranslatedToolTYpe;
  ToolXOffsetLabel.Caption :=  TranslatedToolXOffset;
  ToolYOffsetLabel.Caption :=  TranslatedToolYOffset;
  ToolZOffsetLabel.Caption :=  TranslatedToolZOffset;
  OPNumberLabel.Caption :=  TranslatedOPNumber;
  ToolUsageLabel.Caption :=  TranslatedToolUsage;
  CycleTimeLabel.Caption :=  TranslatedCycleTime;
  HeaderOKButton.Caption := TranslatedOK;
  HeaderCancelButton.Caption := TranslatedCancel;

  OPPData:= TOPPFile.Create;
  MapList := TStringList.Create;
  ReadINiFile;
  Height := DisplayHeight;
  SearchOptions := [soFwd];
end;

procedure TGcodeForm.CopyAxisPosition(Sender: TObject);
var I: Integer;
    Build: string;
begin
  Build:= '';
  for I:= 0 to NC.AxisCount - 1 do begin
    Build:= Build + NC.AxisName[I] + Format('%.3f', [NC.AxisPosition[I][ptDisplay]]) + ' ';
  end;
  Build:= Build + CarRet;
  ClipBoard.AsText:= Build;
end;


procedure TGcodeForm.ReadINiFile;
var
  IniFile : TInifile;
  Buffer  : String;
  BasePath : String;
  AECFields : Integer;
  FDLEntry : pLimitRecord;
  EntryNumber : Integer;
  FieldKey : String;
  FieldBuffer : String;
begin
  BasePath := DLLProfilePath+'OPPEdit.ini';
  IniFile := TIniFile.Create(BasePath);
  try
    with IniFile do begin

      Buffer := ReadString ('GENERAL','ShowKeyboardDefault','False');
      ShowKB := False;
      DisplayTop := ReadInteger('GENERAL','DisplayTop',80);
      DisplayHeight := ReadInteger('GENERAL','DisplayHeight',600);
      Buffer := ReadString ('GENERAL','ToolTypeIsNumeric','False');
      ToolTypeIsNumeric := (Buffer[1]= 't') or (Buffer[1]= 'T');
      AlphaToolTypeLength := ReadInteger('GENERAL','ToolTypeLength',8);
      MaxNumericToolCode :=  ReadInteger('GENERAL','NumericToolTypeMax',9999);
      ShowToolOffsets := False;
      MaxOpNumber := ReadInteger('GENERAL', 'MaxOpNumber', 32);
      MinOpNumber := ReadInteger('GENERAL', 'MinOpNumber', 1);

    end;

    if ToolTypeIsNumeric then begin
      ToolFault.Caption := Format(TranslatedMaxValue,[MaxNumericToolCode]);
    end else begin
      ToolFault.Caption := Format(TranslatedMaxValue,[AlphaToolTypeLength]);
    end;

    OpNumberFaultLabel.Caption := Format(TranslatedMust_Be_0_or, [MinOpNumber, MaxOpNumber]);

    MapList.Clear;
    IniFile.ReadSection('ElectrodeMapData',MapList);

    ToolOffsetPanel.Visible := ShowToolOffsets;
  finally
    IniFile.Free;
  end;
end;


procedure TGcodeForm.FormResize(Sender: TObject);
var
TopHeight : Integer;
begin
 TopHeight := ((Height div 5)*3);
 PanelKB.Visible := True;
 PanelKB.Visible := ShowKb;
 PanelKB.Height :=Height-TopHeight-2;

OPPHeaderPanel.Visible := False;

Top := DisplayTop;
Left := 0;
OPPHeaderPanel.Align := alNone;
OPPHeaderPanel.Height := TopHeight;//(Height div 5)*3;

KeyPad.Height := MainKB.Height;
KeyPad.Top := MainKb.Top;
//KeyPad.Width := Width-pp1.Width-12;
KeyPad.Left := MainKb.Width+1;
KeyPad.Width := PanelKB.Width-(MainKb.Width+2);
HeaderOKButton.Left :=  OPPPartPanel.Width-((HeaderOKButton.Width*2)+5);
HeaderCancelButton.Left :=  HeaderOKButton.Left+(HeaderOKButton.Width+2);
HeaderOKButton.Top :=  OPPPartPanel.height-(HeaderCancelButton.Height+5);
HeaderCancelButton.Top :=  HeaderOKButton.Top;
ShowKB := True;
OPPHeaderPanel.Visible := True;
OPPHeaderPanel.Align := alTop;
ToolFault.Visible := False;
end;

procedure TGcodeForm.FocusOnEditor;
begin
   PartTypeEdit.setFocus;
end;




procedure TGcodeForm.OKButtonClick(Sender: TObject);
var MapString : String;
    TVal      : Integer;

  function IsValidHex(MaxLen : Integer;TestString : String): Boolean;
  var HexSet : set of Char;
      CharCount : Integer;
  begin
    Result := False;
    HexSet := ['0'..'9','a'..'f','A'..'F'];
    if Length(TestString) > MaxLen then begin
      Exit;
    end;
    Result  := True;

    for CharCount := 1 to Length(TestString) do begin
      if not (TestString[CharCount] in HexSet) then begin
        Result  := False;
      end;
    end;
  end;

begin

  ToolFault.Visible := True;
  if ToolTypeIsNumeric then begin
    try
      TVal := StrToInt(ToolTypeEdit.Text);
      ToolFault.Visible := (TVal < 0) or (TVal > MaxNumericToolCode);
    except
      // Assume conversion error, caught later by doing nothing
    end;
  end else begin
    ToolFault.Visible := Length(ToolTypeEdit.Text) > AlphaToolTypeLength
  end;

  try
    OpNumberFaultLabel.Visible := True;
    TVal := StrToInt(OpNumberEdit.Text);
    OpNumberFaultLabel.Visible := (TVal <> 0) and ((TVal < MinOpNumber) or (TVal > MaxOpNumber));
  except
    // Assume conversion error, caught later by doing nothing
  end;

  if not OpNumberFaultLabel.Visible and
     not ToolFault.Visible then
      ModalResult := mrOK;
end;

procedure TGcodeForm.CancelButtonClick(Sender: TObject);
var
DoExit : Boolean;
begin
  CancelDialog := TCancelDialog.Create(Self);
  try
  CancelDialog.ShowModal;
  DoExit :=  CancelDialog.ModalResult = mrOK;
  finally
  CancelDialog.Free;
  end;
  if DoExit then
          begin
          ModalResult := mrCancel;
          end;
end;


procedure TGcodeForm.FormShow(Sender: TObject);
begin
Width := 1022;
Height := DisplayHeight;
Top := DisplayTop;
PageControl1.ActivePage := TabOPPHeader;
FormResize(Self);
StartTimer.Enabled := True;
end;

procedure TGcodeForm.StartTimerTimer(Sender: TObject);
begin
StartTimer.Enabled := False;
FocusOnEditor;
end;


procedure TGcodeForm.FormDestroy(Sender: TObject);
begin
  OPPData.Free;
  MapList.Free;
  end;

procedure TGcodeForm.PartTypeEditEnter(Sender: TObject);
begin
   MainKb.FocusControl := TEdit(Sender);
   KeyPad.FocusControl := TEdit(Sender);
end;


procedure TGcodeForm.HeaderCancelButtonClick(Sender: TObject);
begin
ModalResult := mrCancel;
end;


procedure TGcodeForm.ReadConfigFile;
var
ConfigFile :TIniFile;
Wordlist : TStringList;
FileName : String;
keywordcolour : LongInt;
commentcolour : LongInt;
FontColour    : Longint;
BackGroundColour : Longint;
CommentStart  : String;
C3Start       : String;
Buffer        : String;
WordNumber    : Integer;
FontStyle     : TExtFontStyles;
DelimeterSet  : Set of Char;
CharCount     : Integer;
htmlBasepath : String;
I : Integer;
LangID : String;
begin
FileName := DLLProfilePath+'OPPEditor.cfg';
if FileExists(FileName) then
   begin
   try
   ConfigFile := TInifile.Create(Filename);
   WordList := TStringList.Create;
   try
   with ConfigFile do
        begin
        // Set default font

        { This to be replaced by automatic update of the linked look-up based
         on the names of all the html files under a known directory.
         This can be overriden and or added to by the config file in order to
         target specific links within an html file or to make the name of
         the html file different to the known word}

        // Now add comment identifiers
        end;
   finally
   ConfigFile.Free;
   end;
   finally
   WordList.Free;
   end;
   end;
end;



initialization
  // Add the OPPEditor to the interface and declare its section to be 'Floop'
  TOPPHeaderEditor.AddPlugin(SectionOperationDescription, TOPPHeaderEditor);
end.

