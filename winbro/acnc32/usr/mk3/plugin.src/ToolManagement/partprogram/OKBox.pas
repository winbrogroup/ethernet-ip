unit OKBox;

interface

uses Windows, SysUtils, Classes, Graphics, Forms, Controls, StdCtrls, 
  Buttons, ExtCtrls;

type
  TCancelDialog = class(TForm)
    OKBtn: TButton;
    CancelBtn: TButton;
    Panel1: TPanel;
    Memo1: TMemo;
    procedure OKBtnClick(Sender: TObject);
    procedure CancelBtnClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  CancelDialog: TCancelDialog;
implementation

{$R *.DFM}

procedure TCancelDialog.OKBtnClick(Sender: TObject);
begin
MOdalResult := mrOk;
end;

procedure TCancelDialog.CancelBtnClick(Sender: TObject);
begin
MOdalResult := mrCancel;
end;

end.
