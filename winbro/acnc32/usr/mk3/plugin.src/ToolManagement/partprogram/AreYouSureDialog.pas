unit AreYouSureDialog;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type

  TAYSModes = (aysMessageOnly,aysFileNameInput,aysAreYouSure);
  TAreYouSure = class(TForm)
    BtnOK: TButton;
    BtnCancel: TButton;
    Memo1: TMemo;
    NameInput: TEdit;
    procedure FormShow(Sender: TObject);
    procedure BtnOKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    Message1 : String;
    Message2 : String;
    Mode     : TAYSModes;
    SaveFile : String;
  end;

var
  AreYouSure: TAreYouSure;

implementation

{$R *.dfm}

procedure TAreYouSure.FormShow(Sender: TObject);
begin
Memo1.Lines.Clear;
Memo1.lines.Add('');
Memo1.lines.Add(Message1);
Memo1.lines.Add(Message2);
NameInput.Text := '';
BtnOK.Visible := True;
BtnCancel.Visible := True;
NameInput.Visible := (Mode = aysFileNameInput);
case Mode of
  aysAreYouSure:
  begin
  NameInput.Visible := False;
  BtnOK.Default := True;
  BtnCancel.Default := False;
  end;
  aysFileNameInput:
  begin
  NameInput.Visible := True;
  NameInput.setFocus;
  BtnOK.Default := True;
  BtnCancel.Default := False;
  end;
  aysMessageOnly:
  begin
  BtnCancel.Default := False;
  BtnOK.Default := True;
  BtnCancel.Visible := False;
  end
end; // case
end;
procedure TAreYouSure.BtnOKClick(Sender: TObject);
begin
case Mode of
  aysMessageOnly:
  begin
  end;
  aysAreYouSure:
  begin
  end;
  aysFileNameInput:
  begin
  SaveFile := NameInput.Text;
  end;
end; // case
MOdalResult := mrOk;
end;

end.
