object OCTFormDlg: TOCTFormDlg
  Left = -24
  Top = 70
  BorderStyle = bsNone
  ClientHeight = 715
  ClientWidth = 1016
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -19
  Font.Name = 'Verdana'
  Font.Style = []
  OldCreateOrder = False
  Position = poDefault
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 23
  object BottomPanel: TPanel
    Left = 0
    Top = 0
    Width = 746
    Height = 715
    Align = alClient
    TabOrder = 0
    object BaseOCTPanel: TPanel
      Left = 1
      Top = 41
      Width = 744
      Height = 376
      Align = alTop
      Caption = 'BaseOCTPanel'
      TabOrder = 0
      OnResize = BaseOCTPanelResize
      object RecoveryPanel: TPanel
        Left = 1
        Top = 1
        Width = 742
        Height = 374
        Align = alTop
        BevelInner = bvLowered
        TabOrder = 0
        object RecoveryBanner: TPanel
          Left = 2
          Top = 2
          Width = 738
          Height = 41
          Align = alTop
          BevelInner = bvLowered
          Caption = 'OCT Recovery Options'
          Color = clTeal
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -19
          Font.Name = 'Verdana'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
        end
        object Panel4: TPanel
          Left = 2
          Top = 98
          Width = 373
          Height = 8
          BevelInner = bvLowered
          TabOrder = 1
        end
        object Panel6: TPanel
          Left = 2
          Top = 89
          Width = 738
          Height = 8
          Align = alTop
          BevelInner = bvLowered
          TabOrder = 2
        end
        object Panel8: TPanel
          Left = 2
          Top = 43
          Width = 373
          Height = 8
          BevelInner = bvLowered
          TabOrder = 3
        end
        object RestartPanel: TPanel
          Left = 2
          Top = 43
          Width = 738
          Height = 46
          Align = alTop
          BevelInner = bvLowered
          Caption = 'Restart Current OPP'
          Color = clInfoBk
          TabOrder = 4
          object RestartCurrentButton: TTouchSoftkey
            Left = 2
            Top = 2
            Width = 122
            Height = 42
            Align = alLeft
            Caption = 'TouchSoftkey1'
            TabOrder = 0
            OnMouseUp = RestartCurrentButtonMouseUp
            TextSize = 14
            HasIndicator = False
            IndicatorOn = False
            IsEnabled = True
            ButtonStyle = skNormal
            ArrowColor = clBlack
            ButtonColor = clWhite
            LegendColor = clBlack
            IndicatorWidth = 20
            IndicatorHeight = 10
            IndicatorOnColor = clGreen
            IndicatorOffColor = clSilver
            MaxFontSize = 0
          end
        end
        object Panel10: TPanel
          Left = 2
          Top = 143
          Width = 738
          Height = 8
          Align = alTop
          BevelInner = bvLowered
          TabOrder = 5
        end
        object SkipPanel: TPanel
          Left = 2
          Top = 97
          Width = 738
          Height = 46
          Align = alTop
          BevelInner = bvLowered
          Caption = '     Skip To Next OPP'
          Color = clInfoBk
          TabOrder = 6
          object SkipButton: TTouchSoftkey
            Left = 2
            Top = 2
            Width = 122
            Height = 42
            Align = alLeft
            Caption = 'TouchSoftkey1'
            TabOrder = 0
            OnMouseUp = SkipButtonMouseUp
            TextSize = 14
            HasIndicator = False
            IndicatorOn = False
            IsEnabled = True
            ButtonStyle = skNormal
            ArrowColor = clBlack
            ButtonColor = clWhite
            LegendColor = clBlack
            IndicatorWidth = 20
            IndicatorHeight = 10
            IndicatorOnColor = clGreen
            IndicatorOffColor = clSilver
            MaxFontSize = 0
          end
        end
        object Panel9: TPanel
          Left = 2
          Top = 197
          Width = 738
          Height = 8
          Align = alTop
          BevelInner = bvLowered
          TabOrder = 7
        end
        object AbandonPartPanel: TPanel
          Left = 2
          Top = 205
          Width = 738
          Height = 46
          Align = alTop
          BevelInner = bvLowered
          Caption = 'Mark part as non conformance'
          Color = clInfoBk
          TabOrder = 8
          Visible = False
          object AbandonPartButton: TTouchSoftkey
            Left = 2
            Top = 2
            Width = 122
            Height = 42
            Align = alLeft
            Caption = 'TouchSoftkey1'
            TabOrder = 0
            OnMouseUp = AbandonPartButtonMouseUp
            TextSize = 14
            HasIndicator = False
            IndicatorOn = False
            IsEnabled = True
            ButtonStyle = skNormal
            ArrowColor = clBlack
            ButtonColor = clWhite
            LegendColor = clBlack
            IndicatorWidth = 20
            IndicatorHeight = 10
            IndicatorOnColor = clGreen
            IndicatorOffColor = clSilver
            MaxFontSize = 0
          end
        end
        object Panel11: TPanel
          Left = 2
          Top = 251
          Width = 738
          Height = 8
          Align = alTop
          BevelInner = bvLowered
          TabOrder = 9
          object Panel2: TPanel
            Left = 304
            Top = 8
            Width = 185
            Height = 41
            Caption = 'Panel1'
            TabOrder = 0
          end
        end
        object AbandonOCTPanel: TPanel
          Left = 2
          Top = 151
          Width = 738
          Height = 46
          Align = alTop
          BevelInner = bvLowered
          Caption = '     Abandon Entire OCT'
          Color = clInfoBk
          TabOrder = 10
          object AbandonOCTButton: TTouchSoftkey
            Left = 2
            Top = 2
            Width = 122
            Height = 42
            Align = alLeft
            Caption = 'AbandonOCTButton'
            TabOrder = 0
            OnMouseUp = AbandonOCTButtonMouseUp
            TextSize = 14
            HasIndicator = False
            IndicatorOn = False
            IsEnabled = True
            ButtonStyle = skNormal
            ArrowColor = clBlack
            ButtonColor = clWhite
            LegendColor = clBlack
            IndicatorWidth = 20
            IndicatorHeight = 10
            IndicatorOnColor = clGreen
            IndicatorOffColor = clSilver
            MaxFontSize = 0
          end
        end
        object ReworkPanel: TPanel
          Left = 2
          Top = 259
          Width = 738
          Height = 41
          Align = alTop
          Caption = 'Rework current OPP'
          Color = clInfoBk
          TabOrder = 11
          object ReworkButton: TTouchSoftkey
            Left = 0
            Top = 0
            Width = 122
            Height = 41
            Caption = 'ReworkButton'
            TabOrder = 0
            OnMouseUp = ReworkButtonMouseUp
            TextSize = 14
            HasIndicator = False
            IndicatorOn = False
            IsEnabled = True
            ButtonStyle = skNormal
            ArrowColor = clBlack
            ButtonColor = clWhite
            LegendColor = clBlack
            IndicatorWidth = 20
            IndicatorHeight = 10
            IndicatorOnColor = clGreen
            IndicatorOffColor = clSilver
            MaxFontSize = 0
          end
        end
        object Panel7: TPanel
          Left = 2
          Top = 308
          Width = 738
          Height = 64
          Align = alClient
          BevelInner = bvLowered
          TabOrder = 12
          object Panel12: TPanel
            Left = 2
            Top = 2
            Width = 634
            Height = 60
            Align = alClient
            Alignment = taLeftJustify
            Color = clInfoBk
            Font.Charset = ANSI_CHARSET
            Font.Color = clNavy
            Font.Height = -16
            Font.Name = 'Comic Sans MS'
            Font.Style = []
            ParentFont = False
            TabOrder = 0
            OnResize = Panel12Resize
            object MessageLabel: TLabel
              Left = 1
              Top = 1
              Width = 632
              Height = 32
              Align = alTop
              AutoSize = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clNavy
              Font.Height = -16
              Font.Name = 'Verdana'
              Font.Style = []
              ParentFont = False
              WordWrap = True
            end
            object MessageLabel2: TLabel
              Left = 1
              Top = 29
              Width = 632
              Height = 30
              Align = alBottom
              AutoSize = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clNavy
              Font.Height = -16
              Font.Name = 'Verdana'
              Font.Style = []
              ParentFont = False
              WordWrap = True
            end
          end
          object AcceptKey: TTouchSoftkey
            Left = 636
            Top = 2
            Width = 100
            Height = 60
            Align = alRight
            Caption = 'TouchSoftkey1'
            TabOrder = 1
            OnMouseUp = AcceptKeyMouseUp
            Legend = 'ACCEPT'
            TextSize = 14
            HasIndicator = False
            IndicatorOn = False
            IsEnabled = True
            ButtonStyle = skNormal
            ArrowColor = clBlack
            ButtonColor = clLime
            LegendColor = clMaroon
            IndicatorWidth = 20
            IndicatorHeight = 10
            IndicatorOnColor = clGreen
            IndicatorOffColor = clSilver
            MaxFontSize = 0
          end
        end
        object Panel13: TPanel
          Left = 2
          Top = 300
          Width = 738
          Height = 8
          Align = alTop
          BevelInner = bvLowered
          TabOrder = 13
        end
      end
    end
    object DisplayOptionsPanel: TPanel
      Left = 1
      Top = 660
      Width = 744
      Height = 54
      Align = alBottom
      BevelInner = bvLowered
      BorderWidth = 1
      TabOrder = 1
      object BtnClose: TTouchGlyphSoftkey
        Left = 640
        Top = 4
        Width = 98
        Height = 47
        Legend = '   Close    '
        TextSize = 0
        HasIndicator = False
        IndicatorOn = False
        IsEnabled = True
        ButtonColor = clInfoBk
        Layout = slGlyphLeft
        AutoTextSize = True
        KeyStyle = ssRectangle
        CornerRad = 10
        ButtonTravel = 2
        ShadowColor = clGray
        LegendFont.Charset = ANSI_CHARSET
        LegendFont.Color = clRed
        LegendFont.Height = 20
        LegendFont.Name = 'Verdana'
        LegendFont.Style = []
        GlyphToEdgeSpacing = 1
        TextToEdgeBorder = 5
        MultiLineSpacing = 0
        MaxFontSize = 20
        OnMouseDown = BtnCloseMouseDown
      end
      object BtnShowPartData: TTouchGlyphSoftkey
        Left = 211
        Top = 4
        Width = 110
        Height = 48
        Legend = 'Show~Part Data'
        TextSize = 0
        HasIndicator = False
        IndicatorOn = False
        IsEnabled = True
        ButtonColor = clInfoBk
        Layout = slGlyphLeft
        AutoTextSize = True
        KeyStyle = ssRectangle
        CornerRad = 10
        ButtonTravel = 2
        ShadowColor = clGray
        LegendFont.Charset = ANSI_CHARSET
        LegendFont.Color = clBlack
        LegendFont.Height = 17
        LegendFont.Name = 'Verdana'
        LegendFont.Style = []
        GlyphToEdgeSpacing = 1
        TextToEdgeBorder = 7
        MultiLineSpacing = 0
        MaxFontSize = 20
        OnMouseUp = BtnGeneralMouseUp
        OnMouseDown = BtnGeneralMouseDown
      end
      object BtnFaultAck: TTouchGlyphSoftkey
        Left = 427
        Top = 4
        Width = 110
        Height = 48
        Legend = 'Fault~Acknowledge'
        TextSize = 0
        HasIndicator = False
        IndicatorOn = False
        IsEnabled = True
        ButtonColor = clInfoBk
        Layout = slGlyphLeft
        AutoTextSize = True
        KeyStyle = ssRectangle
        CornerRad = 10
        ButtonTravel = 2
        ShadowColor = clGray
        LegendFont.Charset = ANSI_CHARSET
        LegendFont.Color = clBlack
        LegendFont.Height = 17
        LegendFont.Name = 'Verdana'
        LegendFont.Style = []
        GlyphToEdgeSpacing = 1
        TextToEdgeBorder = 7
        MultiLineSpacing = 0
        MaxFontSize = 20
        OnMouseUp = BtnGeneralMouseUp
        OnMouseDown = BtnGeneralMouseDown
      end
      object BtnResetTool: TTouchGlyphSoftkey
        Left = 11
        Top = 4
        Width = 110
        Height = 48
        Legend = 'Reset~Tool'
        TextSize = 0
        HasIndicator = False
        IndicatorOn = False
        IsEnabled = True
        ButtonColor = clInfoBk
        Layout = slGlyphLeft
        AutoTextSize = True
        KeyStyle = ssRectangle
        CornerRad = 10
        ButtonTravel = 2
        ShadowColor = clGray
        LegendFont.Charset = ANSI_CHARSET
        LegendFont.Color = clBlack
        LegendFont.Height = 17
        LegendFont.Name = 'Verdana'
        LegendFont.Style = []
        GlyphToEdgeSpacing = 1
        TextToEdgeBorder = 7
        MultiLineSpacing = 0
        MaxFontSize = 20
        OnMouseUp = BtnGeneralMouseUp
      end
    end
    object OctStatusPanel: TPanel
      Left = 1
      Top = 1
      Width = 744
      Height = 40
      Align = alTop
      BevelInner = bvLowered
      TabOrder = 2
      object LeftStatusPanel: TPanel
        Left = 2
        Top = 2
        Width = 390
        Height = 36
        Align = alLeft
        BevelInner = bvLowered
        TabOrder = 0
        object LabelStatus1: TLabel
          Left = 24
          Top = 8
          Width = 7
          Height = 23
        end
      end
      object RightStatusPanel: TPanel
        Left = 392
        Top = 2
        Width = 350
        Height = 36
        Align = alClient
        BevelInner = bvLowered
        TabOrder = 1
        object LabelStatus2: TLabel
          Left = 8
          Top = 8
          Width = 7
          Height = 23
        end
      end
    end
    object OCTLowerPageControl: TPageControl
      Left = 1
      Top = 417
      Width = 744
      Height = 243
      ActivePage = PDisplaySheet
      Align = alClient
      TabOrder = 3
      object ToolManSheet: TTabSheet
        Caption = 'ToolManSheet'
        TabVisible = False
        object ToolManagementPanel: TPanel
          Left = 0
          Top = 0
          Width = 736
          Height = 233
          Align = alClient
          TabOrder = 0
        end
      end
      object PDisplaySheet: TTabSheet
        Caption = 'PDisplaySheet'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Verdana'
        Font.Style = []
        ImageIndex = 1
        ParentFont = False
        TabVisible = False
        object Panel17: TPanel
          Left = 0
          Top = 0
          Width = 736
          Height = 24
          Align = alTop
          Alignment = taLeftJustify
          BevelInner = bvLowered
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Verdana'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          object ProgNameDisplay: TLabel
            Left = 4
            Top = 2
            Width = 5
            Height = 16
          end
          object ProgLineDisplay: TLabel
            Left = 620
            Top = 4
            Width = 5
            Height = 16
          end
        end
      end
    end
  end
  object MiscPanel: TPanel
    Left = 746
    Top = 0
    Width = 270
    Height = 715
    Align = alRight
    TabOrder = 1
    object ToolStatMainPanel: TPanel
      Left = 1
      Top = 1
      Width = 268
      Height = 568
      Align = alTop
      Color = clBtnShadow
      TabOrder = 0
      object TStatHeadPanel: TPanel
        Left = 1
        Top = 51
        Width = 266
        Height = 55
        Align = alTop
        Color = clAppWorkSpace
        TabOrder = 0
        object Panel16: TPanel
          Left = 1
          Top = 1
          Width = 64
          Height = 53
          Align = alLeft
          BevelInner = bvLowered
          Caption = 'Head'
          Color = clBtnShadow
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -19
          Font.Name = 'Verdana'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
        end
        object BtnHead: TThreeLineButton
          Left = 65
          Top = 1
          Width = 117
          Height = 53
          Legend = '???~????~????'
          TextSize = 0
          HasIndicator = False
          IndicatorOn = False
          IsEnabled = True
          ButtonColor = clYellow
          Layout = slGlyphLeft
          AutoTextSize = True
          KeyStyle = ssRectangle
          CornerRad = 10
          ButtonTravel = 2
          ShadowColor = clGray
          LegendFont.Charset = ANSI_CHARSET
          LegendFont.Color = clBlack
          LegendFont.Height = 14
          LegendFont.Name = 'Verdana'
          LegendFont.Style = [fsBold]
          GlyphToEdgeSpacing = 1
          TextToEdgeBorder = 5
          MultiLineSpacing = 0
          Flash = False
          FlashOnTime = 1500
          FlashOffTime = 500
          MaxFontSize = 1000
          align = alLeft
        end
        object InvalidateBlank: TPanel
          Left = 235
          Top = 1
          Width = 30
          Height = 53
          Align = alRight
          Color = clInfoBk
          TabOrder = 2
        end
        object BtnInvalidate: TTouchSoftkey
          Left = 155
          Top = 1
          Width = 80
          Height = 53
          Align = alRight
          Caption = 'TouchSoftkey3'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -19
          Font.Name = 'Verdana'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 3
          OnMouseUp = BtnGeneralMouseUp
          Legend = 'Zero Life~(Eject)'
          TextSize = 14
          HasIndicator = False
          IndicatorOn = False
          IsEnabled = True
          ButtonStyle = skNormal
          ArrowColor = clBlack
          ButtonColor = clWhite
          LegendColor = clBlack
          IndicatorWidth = 20
          IndicatorHeight = 10
          IndicatorOnColor = clGreen
          IndicatorOffColor = clSilver
          MaxFontSize = 0
        end
      end
      object TStatTS1Panel: TPanel
        Left = 1
        Top = 106
        Width = 266
        Height = 55
        Align = alTop
        TabOrder = 1
        object Panel1: TPanel
          Left = 1
          Top = 1
          Width = 64
          Height = 53
          Align = alLeft
          BevelInner = bvLowered
          Caption = 'STN1'
          Color = clBtnShadow
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -19
          Font.Name = 'Verdana'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
        end
        object BtnTS1: TThreeLineButton
          Left = 65
          Top = 1
          Width = 117
          Height = 53
          Legend = '???~????~????'
          TextSize = 0
          HasIndicator = False
          IndicatorOn = False
          IsEnabled = True
          ButtonColor = clYellow
          Layout = slGlyphLeft
          AutoTextSize = True
          KeyStyle = ssRectangle
          CornerRad = 10
          ButtonTravel = 2
          ShadowColor = clGray
          LegendFont.Charset = ANSI_CHARSET
          LegendFont.Color = clNavy
          LegendFont.Height = 14
          LegendFont.Name = 'Verdana'
          LegendFont.Style = [fsBold]
          GlyphToEdgeSpacing = 1
          TextToEdgeBorder = 5
          MultiLineSpacing = 0
          Flash = False
          FlashOnTime = 1500
          FlashOffTime = 500
          MaxFontSize = 1000
          OnMouseUp = BtnGeneralMouseUp
          OnMouseDown = BtnGeneralMouseDown
          align = alLeft
        end
        object STN1Location: TPanel
          Left = 185
          Top = 1
          Width = 80
          Height = 53
          Align = alRight
          BevelInner = bvLowered
          Caption = '----'
          Color = clInfoBk
          TabOrder = 2
        end
      end
      object TStatTS2Panel: TPanel
        Left = 1
        Top = 161
        Width = 266
        Height = 55
        Align = alTop
        TabOrder = 2
        object Panel5: TPanel
          Left = 1
          Top = 1
          Width = 64
          Height = 53
          Align = alLeft
          BevelInner = bvLowered
          Caption = 'STN2'
          Color = clBtnShadow
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -19
          Font.Name = 'Verdana'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
        end
        object btnTS2: TThreeLineButton
          Left = 65
          Top = 1
          Width = 117
          Height = 53
          Legend = '???~????~????'
          TextSize = 0
          HasIndicator = False
          IndicatorOn = False
          IsEnabled = True
          ButtonColor = clYellow
          Layout = slGlyphLeft
          AutoTextSize = True
          KeyStyle = ssRectangle
          CornerRad = 10
          ButtonTravel = 2
          ShadowColor = clGray
          LegendFont.Charset = ANSI_CHARSET
          LegendFont.Color = clNavy
          LegendFont.Height = 14
          LegendFont.Name = 'Verdana'
          LegendFont.Style = [fsBold]
          GlyphToEdgeSpacing = 1
          TextToEdgeBorder = 5
          MultiLineSpacing = 0
          Flash = False
          FlashOnTime = 1500
          FlashOffTime = 500
          MaxFontSize = 1000
          OnMouseUp = BtnGeneralMouseUp
          OnMouseDown = BtnGeneralMouseDown
          align = alLeft
        end
        object STN2Location: TPanel
          Left = 185
          Top = 1
          Width = 80
          Height = 53
          Align = alRight
          BevelInner = bvLowered
          Caption = '----'
          Color = clInfoBk
          TabOrder = 2
        end
      end
      object TStatTS3Panel: TPanel
        Left = 1
        Top = 216
        Width = 266
        Height = 55
        Align = alTop
        TabOrder = 3
        object Panel14: TPanel
          Left = 1
          Top = 1
          Width = 64
          Height = 53
          Align = alLeft
          BevelInner = bvLowered
          Caption = 'STN3'
          Color = clBtnShadow
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -19
          Font.Name = 'Verdana'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
        end
        object btnTS3: TThreeLineButton
          Left = 65
          Top = 1
          Width = 117
          Height = 53
          Legend = '???~????~????'
          TextSize = 0
          HasIndicator = False
          IndicatorOn = False
          IsEnabled = True
          ButtonColor = clYellow
          Layout = slGlyphLeft
          AutoTextSize = True
          KeyStyle = ssRectangle
          CornerRad = 10
          ButtonTravel = 2
          ShadowColor = clGray
          LegendFont.Charset = ANSI_CHARSET
          LegendFont.Color = clNavy
          LegendFont.Height = 14
          LegendFont.Name = 'Verdana'
          LegendFont.Style = [fsBold]
          GlyphToEdgeSpacing = 1
          TextToEdgeBorder = 5
          MultiLineSpacing = 0
          Flash = False
          FlashOnTime = 1500
          FlashOffTime = 500
          MaxFontSize = 1000
          OnMouseUp = BtnGeneralMouseUp
          OnMouseDown = BtnGeneralMouseDown
          align = alLeft
        end
        object STN3Location: TPanel
          Left = 185
          Top = 1
          Width = 80
          Height = 53
          Align = alRight
          BevelInner = bvLowered
          Caption = '----'
          Color = clInfoBk
          TabOrder = 2
        end
      end
      object TStatTS4Panel: TPanel
        Left = 1
        Top = 271
        Width = 266
        Height = 55
        Align = alTop
        TabOrder = 4
        object Panel15: TPanel
          Left = 1
          Top = 1
          Width = 64
          Height = 53
          Align = alLeft
          BevelInner = bvLowered
          Caption = 'STN4'
          Color = clBtnShadow
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -19
          Font.Name = 'Verdana'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
        end
        object btnTS4: TThreeLineButton
          Left = 65
          Top = 1
          Width = 117
          Height = 53
          Legend = '???~????~????'
          TextSize = 0
          HasIndicator = False
          IndicatorOn = False
          IsEnabled = True
          ButtonColor = clYellow
          Layout = slGlyphLeft
          AutoTextSize = True
          KeyStyle = ssRectangle
          CornerRad = 10
          ButtonTravel = 2
          ShadowColor = clGray
          LegendFont.Charset = ANSI_CHARSET
          LegendFont.Color = clNavy
          LegendFont.Height = 14
          LegendFont.Name = 'Verdana'
          LegendFont.Style = [fsBold]
          GlyphToEdgeSpacing = 1
          TextToEdgeBorder = 5
          MultiLineSpacing = 0
          Flash = False
          FlashOnTime = 1500
          FlashOffTime = 500
          MaxFontSize = 1000
          OnMouseUp = BtnGeneralMouseUp
          OnMouseDown = BtnGeneralMouseDown
          align = alLeft
        end
        object STN4Location: TPanel
          Left = 185
          Top = 1
          Width = 80
          Height = 53
          Align = alRight
          BevelInner = bvLowered
          Caption = '----'
          Color = clInfoBk
          TabOrder = 2
        end
      end
      object Panel22: TPanel
        Left = 1
        Top = 1
        Width = 266
        Height = 50
        Align = alTop
        Color = clBtnShadow
        Font.Charset = ANSI_CHARSET
        Font.Color = clWhite
        Font.Height = -19
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
        TabOrder = 5
        object RemTimeDisplay: TLabel
          Left = 84
          Top = 8
          Width = 85
          Height = 35
          Alignment = taCenter
          Caption = '00:00'
          Font.Charset = ANSI_CHARSET
          Font.Color = clYellow
          Font.Height = -29
          Font.Name = 'Verdana'
          Font.Style = []
          ParentFont = False
        end
        object Panel3: TPanel
          Left = 1
          Top = 1
          Width = 64
          Height = 48
          Align = alLeft
          BevelInner = bvLowered
          Color = clBtnShadow
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -16
          Font.Name = 'Verdana'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 0
          object Label2: TLabel
            Left = 5
            Top = 5
            Width = 40
            Height = 18
            Caption = 'Time'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -16
            Font.Name = 'Verdana'
            Font.Style = []
            ParentFont = False
          end
          object Label3: TLabel
            Left = 5
            Top = 24
            Width = 42
            Height = 18
            Caption = 'Rem.'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -16
            Font.Name = 'Verdana'
            Font.Style = []
            ParentFont = False
          end
        end
      end
      object TStatTS5Panel: TPanel
        Left = 1
        Top = 326
        Width = 266
        Height = 55
        Align = alTop
        TabOrder = 6
        object Panel19: TPanel
          Left = 1
          Top = 1
          Width = 64
          Height = 53
          Align = alLeft
          BevelInner = bvLowered
          Caption = 'STN5'
          Color = clBtnShadow
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -19
          Font.Name = 'Verdana'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
        end
        object btnTS5: TThreeLineButton
          Left = 65
          Top = 1
          Width = 117
          Height = 53
          Legend = '???~????~????'
          TextSize = 0
          HasIndicator = False
          IndicatorOn = False
          IsEnabled = True
          ButtonColor = clYellow
          Layout = slGlyphLeft
          AutoTextSize = True
          KeyStyle = ssRectangle
          CornerRad = 10
          ButtonTravel = 2
          ShadowColor = clGray
          LegendFont.Charset = ANSI_CHARSET
          LegendFont.Color = clNavy
          LegendFont.Height = 14
          LegendFont.Name = 'Verdana'
          LegendFont.Style = [fsBold]
          GlyphToEdgeSpacing = 1
          TextToEdgeBorder = 5
          MultiLineSpacing = 0
          Flash = False
          FlashOnTime = 1500
          FlashOffTime = 500
          MaxFontSize = 1000
          OnMouseUp = BtnGeneralMouseUp
          OnMouseDown = BtnGeneralMouseDown
          align = alLeft
        end
        object STN5Location: TPanel
          Left = 185
          Top = 1
          Width = 80
          Height = 53
          Align = alRight
          BevelInner = bvLowered
          Caption = '----'
          Color = clInfoBk
          TabOrder = 2
        end
      end
      object TStatTS8Panel: TPanel
        Left = 1
        Top = 491
        Width = 266
        Height = 55
        Align = alTop
        TabOrder = 7
        Visible = False
        object Panel24: TPanel
          Left = 1
          Top = 1
          Width = 64
          Height = 53
          Align = alLeft
          BevelInner = bvLowered
          Caption = 'STN8'
          Color = clBtnShadow
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -19
          Font.Name = 'Verdana'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
        end
        object btnTS8: TThreeLineButton
          Left = 65
          Top = 1
          Width = 117
          Height = 53
          Legend = '???~????~????'
          TextSize = 0
          HasIndicator = False
          IndicatorOn = False
          IsEnabled = True
          ButtonColor = clYellow
          Layout = slGlyphLeft
          AutoTextSize = True
          KeyStyle = ssRectangle
          CornerRad = 10
          ButtonTravel = 2
          ShadowColor = clGray
          LegendFont.Charset = ANSI_CHARSET
          LegendFont.Color = clNavy
          LegendFont.Height = 14
          LegendFont.Name = 'Verdana'
          LegendFont.Style = [fsBold]
          GlyphToEdgeSpacing = 1
          TextToEdgeBorder = 5
          MultiLineSpacing = 0
          Flash = False
          FlashOnTime = 1500
          FlashOffTime = 500
          MaxFontSize = 1000
          OnMouseUp = BtnGeneralMouseUp
          OnMouseDown = BtnGeneralMouseDown
          align = alLeft
        end
        object STN8Location: TPanel
          Left = 185
          Top = 1
          Width = 80
          Height = 53
          Align = alRight
          BevelInner = bvLowered
          Caption = '----'
          Color = clInfoBk
          TabOrder = 2
        end
      end
      object TStatTS7Panel: TPanel
        Left = 1
        Top = 436
        Width = 266
        Height = 55
        Align = alTop
        TabOrder = 8
        object Panel25: TPanel
          Left = 1
          Top = 1
          Width = 64
          Height = 53
          Align = alLeft
          BevelInner = bvLowered
          Caption = 'STN7'
          Color = clBtnShadow
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -19
          Font.Name = 'Verdana'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
        end
        object btnTS7: TThreeLineButton
          Left = 65
          Top = 1
          Width = 117
          Height = 53
          Legend = '???~????~????'
          TextSize = 0
          HasIndicator = False
          IndicatorOn = False
          IsEnabled = True
          ButtonColor = clYellow
          Layout = slGlyphLeft
          AutoTextSize = True
          KeyStyle = ssRectangle
          CornerRad = 10
          ButtonTravel = 2
          ShadowColor = clGray
          LegendFont.Charset = ANSI_CHARSET
          LegendFont.Color = clNavy
          LegendFont.Height = 14
          LegendFont.Name = 'Verdana'
          LegendFont.Style = [fsBold]
          GlyphToEdgeSpacing = 1
          TextToEdgeBorder = 5
          MultiLineSpacing = 0
          Flash = False
          FlashOnTime = 1500
          FlashOffTime = 500
          MaxFontSize = 1000
          OnMouseUp = BtnGeneralMouseUp
          OnMouseDown = BtnGeneralMouseDown
          align = alLeft
        end
        object STN7Location: TPanel
          Left = 185
          Top = 1
          Width = 80
          Height = 53
          Align = alRight
          BevelInner = bvLowered
          Caption = '----'
          Color = clInfoBk
          TabOrder = 2
        end
      end
      object TStatTS6Panel: TPanel
        Left = 1
        Top = 381
        Width = 266
        Height = 55
        Align = alTop
        TabOrder = 9
        object Panel31: TPanel
          Left = 1
          Top = 1
          Width = 64
          Height = 53
          Align = alLeft
          BevelInner = bvLowered
          Caption = 'STN6'
          Color = clBtnShadow
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -19
          Font.Name = 'Verdana'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
        end
        object btnTS6: TThreeLineButton
          Left = 65
          Top = 1
          Width = 117
          Height = 53
          Legend = '???~????~????'
          TextSize = 0
          HasIndicator = False
          IndicatorOn = False
          IsEnabled = True
          ButtonColor = clYellow
          Layout = slGlyphLeft
          AutoTextSize = True
          KeyStyle = ssRectangle
          CornerRad = 10
          ButtonTravel = 2
          ShadowColor = clGray
          LegendFont.Charset = ANSI_CHARSET
          LegendFont.Color = clNavy
          LegendFont.Height = 14
          LegendFont.Name = 'Verdana'
          LegendFont.Style = [fsBold]
          GlyphToEdgeSpacing = 1
          TextToEdgeBorder = 5
          MultiLineSpacing = 0
          Flash = False
          FlashOnTime = 1500
          FlashOffTime = 500
          MaxFontSize = 1000
          OnMouseUp = BtnGeneralMouseUp
          OnMouseDown = BtnGeneralMouseDown
          align = alLeft
        end
        object STN6Location: TPanel
          Left = 185
          Top = 1
          Width = 80
          Height = 53
          Align = alRight
          BevelInner = bvLowered
          Caption = '----'
          Color = clInfoBk
          TabOrder = 2
        end
      end
    end
    object OperatorButtonPanel: TPanel
      Left = 1
      Top = 568
      Width = 268
      Height = 146
      Align = alBottom
      TabOrder = 1
      object Panel18: TPanel
        Left = 1
        Top = 1
        Width = 132
        Height = 144
        Align = alLeft
        Caption = 'Panel18'
        TabOrder = 0
        object Panel20: TPanel
          Left = 1
          Top = 1
          Width = 130
          Height = 70
          Align = alTop
          BevelInner = bvLowered
          BevelWidth = 2
          Caption = 'Panel20'
          TabOrder = 0
          object BtnPartChuck: TTouchSoftkey
            Left = 4
            Top = 4
            Width = 122
            Height = 45
            Align = alTop
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -19
            Font.Name = 'Verdana'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 0
            OnMouseDown = BtnGeneralMouseDown
            OnMouseUp = BtnGeneralMouseUp
            Legend = 'Part~Chuck'
            TextSize = 14
            HasIndicator = False
            IndicatorOn = False
            IsEnabled = True
            ButtonStyle = skNormal
            ArrowColor = clBlack
            ButtonColor = clWhite
            LegendColor = clBlack
            IndicatorWidth = 20
            IndicatorHeight = 10
            IndicatorOnColor = clGreen
            IndicatorOffColor = clSilver
            MaxFontSize = 0
          end
          object PartChuckStatusPanel: TPanel
            Left = 4
            Top = 49
            Width = 122
            Height = 17
            Align = alClient
            BevelInner = bvLowered
            Color = clLime
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Verdana'
            Font.Style = []
            ParentFont = False
            TabOrder = 1
          end
        end
        object Panel21: TPanel
          Left = 1
          Top = 71
          Width = 130
          Height = 72
          Align = alClient
          BevelInner = bvLowered
          BevelWidth = 2
          Caption = 'Panel21'
          TabOrder = 1
          object BtnToolChuck: TTouchSoftkey
            Left = 4
            Top = 4
            Width = 122
            Height = 45
            Align = alTop
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -19
            Font.Name = 'Verdana'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 0
            OnMouseDown = BtnGeneralMouseDown
            OnMouseUp = BtnGeneralMouseUp
            Legend = 'Tool~Chuck'
            TextSize = 14
            HasIndicator = False
            IndicatorOn = False
            IsEnabled = True
            ButtonStyle = skNormal
            ArrowColor = clBlack
            ButtonColor = clWhite
            LegendColor = clBlack
            IndicatorWidth = 20
            IndicatorHeight = 10
            IndicatorOnColor = clGreen
            IndicatorOffColor = clSilver
            MaxFontSize = 0
          end
          object ToolChuckStatusPanel: TPanel
            Left = 4
            Top = 49
            Width = 122
            Height = 19
            Align = alClient
            BevelInner = bvLowered
            Color = clLime
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Verdana'
            Font.Style = []
            ParentFont = False
            TabOrder = 1
          end
        end
      end
      object Panel27: TPanel
        Left = 135
        Top = 1
        Width = 132
        Height = 144
        Align = alRight
        Caption = 'Panel27'
        TabOrder = 1
        object Panel28: TPanel
          Left = 1
          Top = 1
          Width = 130
          Height = 70
          Align = alTop
          BevelInner = bvLowered
          BevelWidth = 2
          Caption = 'Panel28'
          TabOrder = 0
          object BtnOpenMainDoor: TTouchSoftkey
            Left = 4
            Top = 4
            Width = 122
            Height = 45
            Align = alTop
            Enabled = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -19
            Font.Name = 'Verdana'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 0
            OnMouseDown = BtnGeneralMouseDown
            OnMouseUp = BtnGeneralMouseUp
            Legend = 'Main~Door'
            TextSize = 14
            HasIndicator = False
            IndicatorOn = False
            IsEnabled = False
            ButtonStyle = skNormal
            ArrowColor = clBlack
            ButtonColor = clWhite
            LegendColor = clBlack
            IndicatorWidth = 20
            IndicatorHeight = 10
            IndicatorOnColor = clGreen
            IndicatorOffColor = clSilver
            MaxFontSize = 0
          end
          object MainDoorStatusPanel: TPanel
            Left = 4
            Top = 49
            Width = 122
            Height = 17
            Align = alClient
            BevelInner = bvLowered
            Color = clLime
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Verdana'
            Font.Style = []
            ParentFont = False
            TabOrder = 1
          end
        end
        object Panel29: TPanel
          Left = 1
          Top = 71
          Width = 130
          Height = 72
          Align = alClient
          BevelInner = bvLowered
          BevelWidth = 2
          Caption = 'Panel29'
          TabOrder = 1
          object BtnOpenToolDoor: TTouchSoftkey
            Left = 4
            Top = 4
            Width = 122
            Height = 45
            Align = alTop
            Caption = 'BtnOpenToolDoor'
            Enabled = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -19
            Font.Name = 'Verdana'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 0
            OnMouseDown = BtnGeneralMouseDown
            OnMouseUp = BtnGeneralMouseUp
            Legend = 'Tool~Door'
            TextSize = 14
            HasIndicator = False
            IndicatorOn = False
            IsEnabled = False
            ButtonStyle = skNormal
            ArrowColor = clBlack
            ButtonColor = clWhite
            LegendColor = clBlack
            IndicatorWidth = 20
            IndicatorHeight = 10
            IndicatorOnColor = clGreen
            IndicatorOffColor = clSilver
            MaxFontSize = 0
          end
          object ToolDoorStatusPanel: TPanel
            Left = 4
            Top = 49
            Width = 122
            Height = 19
            Align = alClient
            BevelInner = bvLowered
            Color = clLime
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Verdana'
            Font.Style = []
            ParentFont = False
            TabOrder = 1
          end
        end
      end
    end
  end
  object OCTImages: TImageList
    Height = 32
    Width = 32
    Left = 256
    Top = 24
    Bitmap = {
      494C010102000400040020002000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000800000002000000001002000000000000040
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000DF00
      DF00EF00BF00FF00CF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000007F007F007F007F007F007F007F007F007F007F007F007F000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000DF00DF007F00
      4000DF000000DF001F00BF00BF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000CF00CF003F00
      3F003F003F000000BF000000BF000000BF000000BF000000BF000000BF003F00
      3F003F003F00CF00CF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000005F002F00DF00
      0000FF000000EF000000DF007F00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000CF00CF0000006F000000
      FF000000FF003F00FF003F00FF003F00FF003F00FF003F00FF003F00FF000000
      FF000000FF0000006F00BF00BF00DF00DF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000DF00DF005F002F00FF000000FF00
      0000FF2F0000FF0000009F000000CF00BF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000DF00DF007F00BF001F009F000000FF003F00
      FF007F00FF000000000000000000000000000000000000000000000000007F00
      FF003F00FF000000FF0000007F003F007F00DF00DF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000BF007F00DF000000FF000000FF3F
      0000FFDF0000FF000000FF0000009F005F000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000002F005F000000DF000000FF002F00FF00DF00
      FF00000000000000000000000000000000000000000000000000000000000000
      0000DF00FF002F00FF000000FF000000DF002F005F0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000CF00CF009F001F00FF000000FF2F0000FF7F
      5F00FF3FBF00FF8F0000FF000000CF0000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000CF00CF000000FF000000FF002F00FF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000002F00FF000000FF000000FF00CF00CF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000CF00CF006F000000FF000000FF2F0000FF8F6F000000
      000000000000FF9F2F00FF1F0000FF0000006F003F0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000003F009F000000FF003F00FF00DF00FF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000DF00FF003F00FF000000FF003F009F00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000BF00BF007F001F00FF000000FF3F0000FF7F5F00000000000000
      000000000000FF3FBF00FF7F0000FF000000DF001F00BF00BF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000003F00FF000000FF007F00FF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000007F00FF000000FF000F00CF009F009F000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000DF007F00EF000000FF2F0000FF7F5F00FF0FEF00000000000000
      000000000000FF0FEF00FF7F5F00FF2F0000EF000000DF007F00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000007F00FF000000FF003F00FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000003F00FF000000FF007F00FF000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FF5F9F00FFBF3F00FF8F6F000000000000000000000000000000
      0000000000000000000000000000FF2F3F00FF0000007F000000BF00BF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000007F00FF000000FF003F00FF00BF00BF007F007F007F007F007F00
      7F007F007F007F007F007F007F007F007F007F007F007F007F007F007F007F00
      7F007F007F007F007F007F007F00BF00BF003F00FF000000FF007F00FF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FFBF3F00FF000000BF000000DF00BF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000007F00FF000000FF003F00FF007F00DF000000BF000000BF000000
      BF000000BF000000BF000000BF000000BF000000BF000000BF000000BF000000
      BF000000BF000000BF000000BF007F00DF003F00FF000000FF007F00FF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FF2FCF00FF000000FF0000006F002F000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000007F00FF000000FF003F00FF007F00FF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF007F00FF003F00FF000000FF007F00FF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF9F2F00FF1F0000FF000000CF00
      CF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000007F00FF000000FF003F00FF00BF00FF007F00FF007F00FF007F00
      FF007F00FF007F00FF007F00FF007F00FF007F00FF007F00FF007F00FF007F00
      FF007F00FF007F00FF007F00FF00BE00FF003F00FF000000FF007F00FF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF1FDF00FF403F00FF0000009F00
      3F00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000007F00FF000000FF003F00FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000003F00FF000000FF007F00FF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FF5F7F00FF2F0000CF00
      0F009F009F000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000003F00FF000000FF005F00DF00EF00EF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000EF00EF005F00DF000000FF003F00FF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FF2F3F00FF00
      0000DF005F00EF00EF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000009F00FF000000FF000000BF005F007F00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000005F007F000000BE000000FF009F00FF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FF5F9F00FF5F
      0000BF0000007F005F0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FF000000FF000000CF006F006F000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000006F006F000000CF000000FF000000FF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FF5F
      8F00FF5F0000CF0000006F006F00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000CF00FF001F00FF000000FF000000CF005F00
      7F00BF00BF00000000000000000000000000000000000000000000000000EF00
      EF005F007F000000CF000000FF001F00FF00CF00FF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FF5F9F00FF5F0F00CF0000007F005F00EF00EF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000BF00FF007F00FF000000FF000000
      BF0000007F007F007F007F007F007F007F007F007F007F007F007F007F005F00
      DF000000BE000000FF007F00FF00BE00FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FF5F9F00FF5F0000BF0000007F005F0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000008F00FF000000
      FF000000FF000000BF000000BF000000BF000000BF000000BF000000BF000000
      FF000000FF008F00FF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000FF5F8F00FF5F0000CF0000006F006F00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000003F00FF003F00FF003F00FF003F00FF003F00FF003F00FF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FF5F9F00FFBF3F00CF2F00003F000000CF00
      BF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FF5F7F00FF007F00FF00
      DF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000080000000200000000100010000000000000200000000000000000000
      000000000000000000000000FFFFFF00FFFFFFFFFFFFFFFF0000000000000000
      FFFFFFFFFFFFFFFF0000000000000000FFFFFFFFFFFFFFFF0000000000000000
      FFFFFFFFFFFFFFFF0000000000000000FFFFFFFFFFE3FFFF0000000000000000
      FFF81FFFFFC1FFFF0000000000000000FFC003FFFFC1FFFF0000000000000000
      FF8000FFFF00FFFF0000000000000000FE07E07FFF00FFFF0000000000000000
      FE0FF07FFE00FFFF0000000000000000FC3FFC3FFC187FFF0000000000000000
      FC3FFC3FF8383FFF0000000000000000FC7FFE1FF8383FFF0000000000000000
      F8FFFF1FF8FE1FFF0000000000000000F800001FFFFE1FFF0000000000000000
      F800001FFFFE1FFF0000000000000000F800001FFFFF0FFF0000000000000000
      F800001FFFFF0FFF0000000000000000F8FFFF1FFFFF87FF0000000000000000
      FC3FFC3FFFFFC3FF0000000000000000FC3FFC3FFFFFC3FF0000000000000000
      FE1FF87FFFFFE1FF0000000000000000FE07E07FFFFFF07F0000000000000000
      FF0000FFFFFFF87F0000000000000000FFC003FFFFFFFC3F0000000000000000
      FFF81FFFFFFFFE0F0000000000000000FFFFFFFFFFFFFF8F0000000000000000
      FFFFFFFFFFFFFFFF0000000000000000FFFFFFFFFFFFFFFF0000000000000000
      FFFFFFFFFFFFFFFF0000000000000000FFFFFFFFFFFFFFFF0000000000000000
      FFFFFFFFFFFFFFFF000000000000000000000000000000000000000000000000
      000000000000}
  end
end
