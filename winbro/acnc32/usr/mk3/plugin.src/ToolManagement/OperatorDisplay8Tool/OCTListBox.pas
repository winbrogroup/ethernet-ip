unit OCTListBox;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, AbstractFormPlugin, IFormInterface, NamedPlugin,
  ComCtrls, INamedInterface, Grids, ACNC_TouchSoftkey, IOCT, PluginInterface,
  ImgList, CNCTypes, OPDisplayTypes, ACNC_ThreeLineButton, ACNC_TouchGlyphSoftkey,
  ACNC_Plusmemo,SyntaxHighProgDisplay,OCTData;

type
  TOCTListBox = class(TCustomPanel)
  private
//    CompleteRedraw : Boolean;
    OCTRunningTag : TTagref;
    DisplaySweepTag : TTagref;
    OPPIndexTag:    TTagRef;
    OCTItems : TList;
    FTopRow : Integer;
    Grid        : TStringGrid;
    ResetDisplayButton : TTouchSoftKey;
    SelectAllButton    : TTouchSoftKey;
    InvertSelectionButton : TTouchSoftKey;
    UpButton,DownButton: TTouchSoftKey;
    PageUpButton,PageDownButton : TTouchSoftKey;
    TopButton,BottomButton: TTouchSoftKey;
    TitlePanel  : TPanel;
    GridPanel   : TPanel;
    FClickedRow : Integer;
    FUnitHeight : Integer;
    FOnRowClicked : TNotifyEvent;
    FRequestDisplayReset: TNotifyEvent;
    FRequestOPPReset    : TNotifyEvent;
    FRequestInvert      : TNotifyEvent;
    FRequestReduce      : TNotifyEvent;
    FRunningRow  : Integer;

    SB : TStatusBar;

    Translator : TTranslator;

    // Translatable Strings
    TranslatedReady : String;
    TranslatedComplete : String;
    TranslatedRecovery : String;
    TranslatedAbandoned : String;
    TranslatedSkipped : String;
    TranslatedRunning : String;
    TranslatedRestarted : String;
    TranslatedReworked : String;
    TranslatedRestartReady : String;
    TranslatedUnknown : String;
    TranslatedDisabled : String;

    //FRecoveryIndex : Integer;
    FShowResetButton: Boolean;


    function VisibleItems : Integer;
   procedure SetSAButtonEnabled(Value : Boolean);
    function GetSAButtonEnabled: Boolean;
    procedure SetRunningRow(Value : Integer);


    procedure MyDrawCell(Sender: TObject; Col, Row: Integer; Rect: TRect;
                         State: TGridDrawState);
    procedure GridMouseDown(Sender: TObject; Button: TMouseButton;
                            Shift: TShiftState; X, Y: Integer);
    procedure TopRowChanged(Sender : Tobject);

    procedure GridButtonClick(Sender : Tobject);
    procedure SetTopRow(Value: Integer);
    procedure ConditionButtons;
    //procedure SetRecoveryIndex(const Value: Integer);
    procedure SetUnitHeight(const Value: INteger);
    procedure OnTagChange(Tag :TTagref);
    procedure UpDateOCTDisplay;
    function GetDRButtonEnabled: Boolean;
    procedure SetDRButtonEnabled(Value: Boolean);
    procedure SetShowResetButton(const Value: Boolean);

  protected
    function GetSText(Index :Integer):String;
    procedure SETSText(Index: Integer;SText : String);



  public
    ImageList   : TImageList;
    OCTList     : TOPPList;
    UpdateOCTDisplayTrigger : Boolean;

    constructor Create(aOwner : TComponent); override;
    destructor Destroy; override;
    procedure Resize; override;
    procedure RefreshDisplay;
    procedure ResetGrid;
    procedure FinalInstall;
    procedure CellUpdate(Row : Integer;Field : WideString);
    property ClickedRow : Integer read FClickedRow;
    property RequestDisplayReset : TNotifyEvent read FRequestDisplayReset write FRequestDisplayReset;
    property RequestSelectAllOPPs: TNotifyEvent read FRequestOPPReset write FRequestOPPReset;
    property RequestInvertSelection : TNotifyEvent read FRequestInvert write FRequestInvert ;
    property RequestReduce :TNotifyEvent read FRequestReduce write FRequestReduce;
    property OnRowClicked : TNotifyEvent read FOnRowClicked write FonRowClicked;
    property StatusText[Index : Integer]:String read GetSText write SetSText;
    property DisplayResetButtonEnabled : Boolean read GetDRButtonEnabled write SetDRButtonEnabled;
    property SelectAllButtonEnabled    : Boolean read GetSAButtonEnabled write SetSAButtonEnabled;
    property RunningRow : Integer  read FRunningRow  write SetRunningRow;
    //property RecoveryIndex : Integer read FRecoveryIndex write SetRecoveryIndex;
    property UnitHeight : INteger read FUnitHeight write SetUnitHeight;
    property ShowResetButton : Boolean read FShowResetButton write SetShowResetButton;
  end;

var
ThisOCTListBox : TOCTListBox;

procedure TagChangeHandler(Tag : TTagRef); stdcall;

implementation

uses OCTOperatorDisplay;

function SecondsToHMS(Seconds: Double): string;
var Tmp, Hours, Mins, Secs: Integer;
begin
  Tmp:= Round(Seconds);
  Hours:= Tmp div 3600;
  Mins:= Tmp mod 3600;
  Secs:= Mins mod 60;
  Mins:= Mins div 60;
  if Hours > 0 then
    Result:= Format('%.2d:%.2d:%.2d', [Hours, Mins, Secs])
  else
    Result:= Format('%.2d:%.2d', [Mins, Secs]);
end;

{ TOCTListBox }

constructor TOCTListBox.Create(aOwner: TComponent);
begin
  inherited;
  FTopRow := 1;
  FClickedRow := -1;
  //FRecoveryIndex := -1;
  TitlePanel := TPanel.Create(Self);
  with TitlePanel do
       begin
       Parent := Self;
       Align := alTop;
       BevelInner := bvLowered;
       Font.Size := 12;
       Font.Style := [fsbold];
       end;

  ResetDisplayButton    := TTouchSoftKey.Create(Self);
  SelectAllButton       := TTouchSoftKey.Create(Self);
  InvertSelectionButton := TTouchSoftKey.Create(Self);
  UpButton              := TTouchSoftKey.Create(Self);
  DownButton            := TTouchSoftKey.Create(Self);
  PageUpButton          := TTouchSoftKey.Create(Self);
  PageDownButton        := TTouchSoftKey.Create(Self);

  TopButton             := TTouchSoftKey.Create(Self);
  BottomButton          := TTouchSoftKey.Create(Self);

  GridPanel := TPanel.Create(Self);
  GridPanel.Parent := Self;
  GridPanel.ParentFont := True;
  GridPanel.Align := alTop;

  SB := TStatusBar.Create(self);
  SB.Parent := Self;
  SB.Align := alBottom;
  SB.SimplePanel := False;
  SB.Panels.Add;
  SB.Panels.Add;
  SB.Panels.Add;
  SB.Panels[0].Width := 150;
  SB.Panels[1].Width := 150;

  Grid := TStringGrid.Create(Self);
  with Grid do
       begin
       Parent := GridPanel;
       Align := alClient;
       RowCount := 2;
       FixedRows := 1;
       FixedCols := 0;
       ScrollBars := ssNone;

       Cells[1,0] := 'Name';
       Cells[2,0] := 'Status';
       Cells[3,0] := 'Start Time';
       Cells[4,0] := 'Duration';
       DefaultDrawing := False;
       OnMouseDown := GridMouseDown;
       OnTopLeftChanged := TopRowChanged;
       end;
  Color := clWindow;
  OCTItems := TList.Create;

  Grid.OnDrawCell := MyDrawCell;
  UnitHeight := 32;
  ThisOCTListBox := Self;
end;



procedure TOCTListBox.Resize;
var W : Integer;
begin
  inherited;
  Grid.Width := GridPanel.Width-2;
  Grid.DefaultRowHeight := UnitHeight;
  Grid.GridLineWidth := 1;

  Grid.Height := (Grid.RowCount+3)*(UnitHeight+Grid.GridLineWidth);
  GridPanel.Height := Height-(TitlePanel.Height+SB.Height+2);

  W := (GridPanel.Width - 48) div 6;
  Grid.ColWidths[0] := 34;
  Grid.ColWidths[1] := W * 3;
  Grid.ColWidths[2] := W;
  Grid.ColWidths[3] := W+25;
  Grid.ColWidths[4] := W-25;
  ConditionButtons;
  Grid.Invalidate;
  Grid.ScrollBars := ssNone;
end;

procedure TOCTListBox.MyDrawCell(Sender: TObject; Col, Row: Longint; Rect: TRect; State: TGridDrawState);
var
Contents : String;
RowIndex  : Integer;
begin
  RowIndex := Grid.TopRow+Row-1;
  try
  Contents := Grid.Cells[Col,row];
  with Grid.Canvas do
     begin
     Font.Name := Self.Font.Name;
     Font.Size := Self.Font.Size;
     Font.Style := Self.Font.Style;
     if Row = 0 then
        begin
        Brush.Color := clSilver;
        Font.Color := clBlack;
        FillRect(Rect);
        TextOut(Rect.Left+3,Rect.Top,Contents);
        exit;
        end;
  Brush.Color := clWhite;
  Font.Color := clBlack;

  if Named.GetAsBoolean(OCTRunningTag) and (Col > 0) then
     begin
     if(Row = RunningRow)  then
        begin
        Brush.Color := clMoneyGreen;
        Font.Color := clBlue;
        end;
     end
  else
     begin
     end;

  if ((Row-1) <=  OCTList.Count) and (OCTList.Count > 0) then
     begin
     If not OCTList[Row-1].Enabled then
        begin
        Font.Color := clgray;
        end;
     end;
{
  if (Row = RecoveryIndex+1) and (RecoveryIndex >= 0) then
    begin
    Brush.Color := clMoneyGreen;
    Font.Color := ClBlue;
    end;
 }
  FillRect(Rect);
  if Col > 0 then
     begin
     TextOut(Rect.Left+3,Rect.Top,Contents);
     end
  else
      begin
      if Contents = '1' then
          begin
          ImageList.Draw(Grid.Canvas,Rect.Left,Rect.Top,1,True);
          end
      else
          begin
          ImageList.Draw(Grid.Canvas,Rect.Left,Rect.Top,0,True);
          end;
      //Grid.Canvas.TextOut(Rect.Left,Rect.Top,IntToStr(Row));
      end;
  end;
  except
  Contents := 'Error';
  end;
end;

function TOCTListBox.VisibleItems: Integer;
begin
  Result := (GridPanel.Height div (UnitHeight+Grid.GridLineWidth))-1;
end;

procedure TOCTListBox.UpDateOCTDisplay;
var
  RCount : Integer;
  OPPData    : TOPP;
begin
  OCTList.Lock;
  try
//  CompleteRedraw := True;
  Grid.RowCount := OCTList.Count+1;
  if Grid.RowCount < 2 then Grid.RowCount := 2;

  if OCTList.Count <> 0 then begin
    for RCount := 1 to OCTList.Count do begin
      OPPData := OCTList.OPP[RCount - 1];
      if OPPData.Enabled then begin
        Grid.Cells[0,RCount] := '1';
      end else begin
        Grid.Cells[0,RCount] := '0';
      end;

      Grid.Cells[1,RCount] := OPPData.FileName;
      if OPPData.Enabled then begin
        if OPPData.AlreadyComplete then
          begin
          Grid.Cells[2,RCount] := TranslatedComplete;
          end
        else
          begin
          case OPPData.Status of
            osNone :  Grid.Cells[2,RCount] := TranslatedReady;
            osComplete : Grid.Cells[2,RCount] := TranslatedComplete;
            osInRecovery : Grid.Cells[2,RCount] := TranslatedRecovery;
            osAbandoned : Grid.Cells[2,RCount] := TranslatedAbandoned;
            osSkipped : Grid.Cells[2,RCount] := TranslatedSkipped;
            osStarted :
              begin
              Grid.Cells[2,RCount] := TranslatedRunning;
              SetRunningRow(Rcount);
              end;
            osUnknown : Grid.Cells[2,RCount] := TranslatedUnknown;
            end; // case
          end
        end else begin
            if OPPData.AlreadyComplete then
                begin
                Grid.Cells[2,RCount] := TranslatedComplete;
                end
            else
                begin
                Grid.Cells[2,RCount] := TranslatedDisabled;
                end
        end;
      if OPPData.StartTime = 0 then
        begin
        Grid.Cells[3,RCount] := '---'
        end
      else
        begin
        Grid.Cells[3,RCount] := TimeToStr(OPPData.StartTime);
        end;
      if OPPData.Duration = 0 then begin
        Grid.Cells[4,RCount] := '---';
      end else begin
        Grid.Cells[4,RCount] := SecondsToHMS(OppData.Duration); //Format('%5.1f',[OPPData.Duration]);
      end;
    end;
  end;
  finally
//  CompleteRedraw := False;
  SetTopRow(FTopRow);
  OCTList.Unlock;
  end;
end;

destructor TOCTListBox.Destroy;
begin
  OCTItems.Free;
  inherited Destroy;
end;

procedure TOCTListBox.GridMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
 if {(RecoveryIndex <=1 )
 and }(Named.GetAsBoolean(OCTRunningTag) = False)
 and (Named.GetAsInteger(OPPIndexTag ) = 0) then
    begin
    FClickedRow := (Grid.TopRow-1)+ (Y div (UnitHeight+Grid.GridLineWidth));
    if assigned(FOnRowClicked) then
      FonRowClicked(Self);
    ConditionButtons;
    end;
end;

procedure TOCTListBox.RefreshDisplay;
begin
  UpdateOCTDisplayTrigger := True;
end;

function TOCTListBox.GetSText(Index: Integer): String;
begin
  Result := '';
  if Index+1 < SB.Panels.Count then
    Result := SB.Panels[Index+1].Text;
end;

procedure TOCTListBox.SETSText(Index: Integer; SText: String);
begin
  if Index < SB.Panels.Count then
    SB.Panels[Index].Text := SText;
end;

function TOCTListBox.GetDRButtonEnabled: Boolean;
begin
  Result := False;
  if Assigned(ResetDisplayButton) then
    Result := ResetDisplayButton.Enabled;
end;

procedure TOCTListBox.SetDRButtonEnabled(Value: Boolean);
begin
  if Assigned(ResetDisplayButton) and  FShowResetButton then
    ResetDisplayButton.IsEnabled := Value;
end;

function TOCTListBox.GetSAButtonEnabled: Boolean;
begin
  Result := False;
  if Assigned(SelectAllButton) then
    Result := SelectAllButton.Enabled;
end;

procedure TOCTListBox.SetSAButtonEnabled(Value: Boolean);
begin
  if not assigned(SelectAllButton) then
    Exit;
  SelectAllButton.Enabled := Value;
end;

procedure TOCTListBox.SetTopRow(Value :Integer);
begin
  if Value < 1 then
    Grid.TopRow := 1
  else
    Grid.TopRow := Value;

  FTopRow := Value;
end;

procedure TOCTListBox.SetRunningRow(Value: Integer);
var
LastVisibleRow : Integer;
VisibleRows :Integer;
begin
VisibleRows := VisibleItems;
if VisibleItems <=1 then
   begin
   SetTopRow(Value);
   end
else
  begin
  if Value >= 1 then begin
    if Value < Grid.TopRow then
      Grid.TopRow := Value;
    LastVisibleRow := Grid.TopRow +VisibleItems-1;
    if Value >= LastVisibleRow then begin
      SetTopRow( Value- VisibleItems+2);
    end;
  end;
  end;
  FRunningRow := Value;
end;

procedure TOCTListBox.GridButtonClick(Sender: Tobject);
var
NewTop : Integer;
begin


  if Sender = SelectAllButton then
  begin
    if assigned(FRequestOPPReset) then FRequestOPPReset(Self);
  end else

  if Sender =  ResetDisplayButton then
  begin
    if assigned(FRequestDisplayReset) then FRequestDisplayReset(Self);
  end else

  if Sender =  PageUpButton then begin
      NewTop := Grid.TopRow-Grid.VisibleRowCount;
      if NewTop < 1 then NewTop := 1;
      SetTopRow(NewTop);
  end else

  if Sender =  PageDownButton then begin
      NewTop := Grid.TopRow+Grid.VisibleRowCount;
      if NewTop < (Grid.RowCount-Grid.VisibleRowCount+2) then
          SetTopRow(NewTop)
      else
          SetTopRow(Grid.RowCount- VisibleItems);

  end else

  if Sender =  UpButton then begin
    if Grid.TopRow > 1 then
       SetTopRow(Grid.TopRow-1);
  end else

  if Sender =  DownButton then begin
    if (Grid.TopRow + VisibleItems) < Grid.RowCount then
      SetTopRow(Grid.TopRow+1);
  end else

  if Sender =  TopButton then begin
    if Grid.TopRow > 1 then
       SetTopRow(1);
  end else

  if Sender =  BottomButton then begin
       SetTopRow(Grid.RowCount- VisibleItems);
  end else

  if Sender =  InvertSelectionButton then
  begin
    if assigned(FRequestInvert) then FRequestInvert(Self);
  end;
ConditionButtons;
end;

procedure TOCTListBox.ResetGrid;
begin
  Resize;
  SetTopRow(1);
  ConditionButtons;
end;

procedure TOCTListBox.TopRowChanged(Sender: Tobject);
begin
  ConditionButtons;
end;

procedure TOCTListBox.ConditionButtons;
begin
  if Named.GetAsBoolean(OCTRunningTag) then begin
    InvertSelectionButton.IsEnabled := False;
    SelectAllButton.IsEnabled := False;
    TopButton.IsEnabled  := False;
    BottomButton.IsEnabled  := False;
  end else begin
    InvertSelectionButton.IsEnabled := not OCTList.AllEnabled;
    SelectAllButton.IsEnabled := InvertSelectionButton.IsEnabled;
    TopButton.IsEnabled  := True;
    BottomButton.IsEnabled  := True;
  end;
  UpButton.IsEnabled := (Grid.TopRow > 1);
  DownButton.IsEnabled := ((VisibleItems+Grid.TopRow)< (Grid.RowCount));
end;


procedure TOCTListBox.FinalInstall;
begin
try
  OctRunningTag := Named.AquireTag('OCTRunning','TMethodTag',nil);
  OPPIndexTag :=   Named.AquireTag('OCTIndex','TIntegerTag',nil);
  DisplaySweepTag := Named.AquireTag('DisplaySweep','TMethodTag',TagChangeHandler);
  Translator := TTranslator.Create;
  TranslatedReady := Translator.GetString('$READY');
  TranslatedComplete := Translator.GetString('$COMPLETE');
  TranslatedRecovery := Translator.GetString('$RECOVERY');
  TranslatedAbandoned := Translator.GetString('$ABANDONED');
  TranslatedSkipped :=   Translator.GetString('$SKIPPED');
  TranslatedRunning  := Translator.GetString('$RUNNING');
  TranslatedRestarted  := Translator.GetString('$RESTARTED');
  TranslatedReworked  := Translator.GetString('$REWORKED');
  TranslatedUnknown  := Translator.GetString('$UNKNOWN');
  TranslatedDisabled  := Translator.GetString('$DISABLED');
  TranslatedRestartReady := Translator.GetString('$RESTARTREADY');


  with Grid do
       begin
       Cells[1,0] := Translator.GetString('$NAME');
       Cells[2,0] := Translator.GetString('$STATUS');
       Cells[3,0] := Translator.GetString('$START_TIME');
       Cells[4,0] := Translator.GetString('$DURATION');
       end;

  with InvertSelectionButton do
       begin
       Parent := TitlePanel;
       Legend := Translator.GetString('$Invert');
       Width := 95;
       align := alLeft;
       Visible := True;
       OnClick :=GridButtonClick;
       end;

  with SelectAllButton do
       begin
       Parent := TitlePanel;
       Legend := Translator.GetString('$SELECT_ALL');
       Width := 95;
       align := alLeft;
       Visible := True;
       OnClick :=GridButtonClick;
       end;

  with ResetDisplayButton do
       begin
       Parent := TitlePanel;
       Legend := Translator.GetString('$RESET');
       Width := 95;
       align := alLeft;
       Visible := True;
       OnClick :=GridButtonClick;
       end;




  with DownButton do
       begin
       Parent := TitlePanel;
       align := alRight;
       ButtonStyle := skDownArrow;
       ArrowColor := clBlue;
       Width := 70;
       Visible := True;
       OnClick :=GridButtonClick;
       end;

  with UpButton do
       begin
       Parent := TitlePanel;
       align := alRight;
       ButtonStyle := skUpArrow;
       ArrowColor := clBlue;
       Width := 70;
       Visible := True;
       OnClick :=GridButtonClick;
       end;

  with PageDownButton do
       begin
       Parent := TitlePanel;
       align := alRight;
       Legend := Translator.GetString('$PgDn');
       ArrowColor := clBlue;
       Width := 70;
       Visible := True;
       OnClick :=GridButtonClick;
       end;

  with PageUpButton do
       begin
       Parent := TitlePanel;
       align := alRight;
       Legend := Translator.GetString('$PgUp');
       ArrowColor := clBlue;
       Width := 70;
       Visible := True;
       OnClick :=GridButtonClick;
       end;

  with BottomButton do
       begin
       Parent := TitlePanel;
       align := alRight;
       Legend := Translator.GetString('$BOTTOM');
       Width := 70;
       Visible := True;
       OnClick :=GridButtonClick;
       end;

  with TopButton do
       begin
       Parent := TitlePanel;
       align := alRight;
       Legend := Translator.GetString('$TOP');
       Width := 70;
       Height := TitlePanel.Height-4;
       Visible := True;
       OnClick :=GridButtonClick;
       end;

   finally
   Translator.Free;
   end;
end;

procedure TagChangeHandler(Tag : TTagRef);
begin
  ThisOCTListBox.OnTagChange(Tag);
end;

{
procedure TOCTListBox.SetRecoveryIndex(const Value: Integer);
begin
  FRecoveryIndex := Value;
  SetTopRow(FRecoveryIndex+1);
end;
}

procedure TOCTListBox.CellUpdate(Row: Integer; Field: WideString);
var
ROPP : TOPP;
UpdateRow : Integer;
begin
ROPP := OCTList.OPP[Row];
UpdateRow := Row+1;
//if Row = RunningRow-1 then
  begin
  OCTList.Lock;
  try
  case ROPP.Status of
          osNone :  Grid.Cells[2,UpdateRow] := TranslatedReady;
          osComplete : Grid.Cells[2,UpdateRow] := TranslatedComplete;
          osInRecovery : Grid.Cells[2,UpdateRow] := TranslatedRecovery;
          osAbandoned : Grid.Cells[2,UpdateRow] := TranslatedAbandoned;
          osSkipped : Grid.Cells[2,UpdateRow] := TranslatedSkipped;
          osStarted : Grid.Cells[2,UpdateRow] := TranslatedRunning;
          osUnknown : Grid.Cells[2,UpdateRow] := TranslatedUnknown;
  end; // case
  Grid.Cells[3,UpdateRow] := TimeToStr(ROPP.StartTime);
  Grid.Cells[4,UpdateRow] := SecondsToHMS(ROPP.Duration); // Format('%5.1f',[ROPP.Duration]);
  finally
  OCTList.UnLock;
  end
  end

END;

procedure TOCTListBox.SetUnitHeight(const Value: INteger);
begin
  FUnitHeight := Value;

end;

procedure TOCTListBox.OnTagChange(Tag: TTagref);
begin
if Tag = DisplaySweepTag then
  begin
  If UpdateOCTDisplayTrigger then
    begin
    UpdateOCTDisplayTrigger := False;
    UpDateOCTDisplay;
    end;
  end;

end;

procedure TOCTListBox.SetShowResetButton(const Value: Boolean);
begin

  FShowResetButton := Value;
  if assigned(ResetDisplayButton) then
      ResetDisplayButton.Visible := Value;
end;

end.
