library Mk3_OperatorDisplay_8Tool;



{%File '..\..\..\ACNC32\profile\Live\language.csv'}
{%File 'C:\Acnc32\profile\Plc\PlcGlobal.H'}
{%File 'C:\hsd6Mk3\profile\cnc.ini'}
{%File 'C:\hsd6Mk3\profile\core_language.csv'}
{%File 'C:\hsd6Mk3\profile\Live\profile_language.csv'}

uses
  SysUtils,
  Classes,
  OCTData in '..\lib\OCTData.pas',
  PartData in '..\lib\PartData.pas',
  OpDisplayTypes in 'OpDisplayTypes.pas',
  SyntaxHighProgDisplay in 'SyntaxHighProgDisplay.pas',
  OCTOperatorDisplay in 'OCTOperatorDisplay.pas' {OCTFormDlg},
  ToolData in '..\lib\ToolData.pas',
  OCTListBox in 'OCTListBox.pas',
  ReqToolList in 'ReqToolList.pas',
  ToolDB in '..\lib\ToolDB.pas',
  RequiredToolData in '..\lib\RequiredToolData.pas';

//  ProductionHistory in 'ProductionHistory.pas';

{$R *.RES}

{ ACNC32PluginRevision is not longer used by ACNC32, it now gets the version
  information directly from the dll resource.
function ACNC32PluginRevision : Cardinal; stdcall;
begin
  Result := $01000000;
end;

exports
  ACNC32PluginRevision;                       }

begin
end.
