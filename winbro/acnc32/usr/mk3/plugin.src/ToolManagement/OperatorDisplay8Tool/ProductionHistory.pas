unit ProductionHistory;

interface

uses Classes, SysUtils, AbstractOCTPlugin, NamedPlugin, Windows, IOCT,
     IToolLife, StreamTokenizer,INamedInterface,CNCTypes,extctrls,
     TypInfo,OCTTypes,IniFiles,DateUtils;

type
TInternalParameters = (ipNone,ipDateEnglish,ipDateAmerican,ipTime,ipOCTName,ipOPPName,
                       ipOPPInterrupted,ipOPPAbandoned);
TFieldType = (ftStartField,ftEndField);

TPHFieldRef = class (TObject)
            public
            FieldType : TFieldType;
            TagName : String;
            TagRef  : TTagRef;
            Internal: Boolean;
            Fixed   : Boolean;
            FixedEntry : String;
            InternalParam : TInternalParameters;
            end;
PPHField  = ^TPHFieldRef;


TProductionHistory = class(TObject)
 private
 StartTag       : TTagref;
 EndTag         : TTagref;
 MCIDTag        : TTagref;
 OutFilePathTag : TTagref;
 CurrentFileTag : TTagref;

 TagRefList     : TList;
 UseOutFile     : Boolean;
 UseBufferFile  : Boolean;
 InprogFilePath : String;
 OutFilePath    : String;

 RegisteredPath : String;
 //RegisteredPartName : String;

 function AddTagValue(TagValue :String;FieldType : TPHFieldRef):String;
 procedure SaveRecord(RecordString: String);


 public
 Configured : Boolean;
 StartTagCount : Integer;
 StartTagName  : String;
 MCIDTagName   : String;
 EndTagName  : String;
 EndTagCount   : Integer;
 MachineID     : String;
 OCTName       : String;
 OPPName       : String;
 ToolTYpe      : String;
 ToolSerial    : String;
 StartTagValue : String;
 EndTagValue   : String;
 function Install: Boolean;
 function FinalInstall:Boolean;
 procedure MakeRecord(RecordType: TFieldType;OCTName,OPPName:String;
                      Interrupted,Abandoned,Sigwarn,SigFail: Boolean);
 function RegisterPart(PartName : String): String;
 procedure ClearTags;
 constructor Create;
 destructor Destroy; override;
 end;
var
  ProdHistory : TProductionHistory;

implementation

uses Math;

function SlashSep(const Path, S: String): String;
begin
  if AnsiLastChar(Path)^ <> '\' then
    Result := Path + '\' + S
  else
    Result := Path + S;
end;

function NoExtension(Filename : String):String;
var
PosDot : Integer;
begin
PosDot := Pos('.',Filename);
if PosDot > 0 then
   begin
   Result := Copy(Filename,1,PosDot-1);
   end
else
    Result := Filename;

end;


{ TProductionHistory }
function MakeTimeDateString(Time : TDateTime):String;
var
Year,Month,Day,Hour,Min,Sec,msec : Word;
HStr,MinStr,SEcStr : String;
MonStr : String;
begin
   DecodeDateTime(Time,Year,Month,Day,Hour,Min,Sec,msec);
   if Hour < 10 then HStr := '0'+IntToStr(Hour) else HStr := IntToStr(Hour);
   if Min < 10 then MinStr := '0'+IntToStr(Min) else MinStr := IntToStr(Min);
   if Sec < 10 then SEcStr := '0'+IntToStr(SEc) else SEcStr := IntToStr(SEc);
   case Month of
   1: MonStr := 'Jan';
   2: MonStr := 'Feb';
   3: MonStr := 'Mar';
   4: MonStr := 'Apr';
   5: MonStr := 'May';
   6: MonStr := 'Jun';
   7: MonStr := 'July';
   8: MonStr := 'Aug';
   9: MonStr := 'Sept';
   10: MonStr := 'Oct';
   11: MonStr := 'Nov';
   12: MonStr := 'Dec';
   end;

   Result := Format('%s\%d\%d  at %s:%s:%s',[MonStr,Day,Year,HStr,MinStr,SecStr]);
end;

function MakeTimeString(Time : TDateTime):String;
var
Hour,Min,Sec,msec : Word;
HStr,MinStr,SEcStr : String;
//MonStr : String;
begin
   DecodeTime(Time,Hour,Min,Sec,msec);
   if Hour < 10 then HStr := '0'+IntToStr(Hour) else HStr := IntToStr(Hour);
   if Min < 10 then MinStr := '0'+IntToStr(Min) else MinStr := IntToStr(Min);
   if Sec < 10 then SEcStr := '0'+IntToStr(SEc) else SEcStr := IntToStr(SEc);
   Result := Format('%s:%s:%s',[HStr,MinStr,SecStr]);
end;

function MakeFileTimeString(Time : TDateTime):String;
var
Hour,Min,Sec,msec : Word;
HStr,MinStr,SEcStr : String;
//MonStr : String;
begin
   DecodeTime(Time,Hour,Min,Sec,msec);
   if Hour < 10 then HStr := '0'+IntToStr(Hour) else HStr := IntToStr(Hour);
   if Min < 10 then MinStr := '0'+IntToStr(Min) else MinStr := IntToStr(Min);
   if Sec < 10 then SEcStr := '0'+IntToStr(SEc) else SEcStr := IntToStr(SEc);
   Result := Format('-%s_%s_%s',[HStr,MinStr,SecStr]);
end;

function MakeDateString(Date : TDateTime;isAmerican : Boolean):String;
var
Year, Month, Day: Word;
YStr,MStr,DStr : String;
begin
   DecodeDate(Date,Year, Month, Day);
   YStr := IntToStr(Year);
   if Year < 10 then YStr := '0'+IntToStr(Year);
   if Month < 10 then MStr := '0'+IntToStr(Month) else MStr := IntToStr(Month);
   if Day   < 10 then DStr := '0'+IntToStr(Day) else DStr := IntToStr(Day);
   if isAmerican then
      begin
      Result := Format('%s-%s-%s',[MStr,DStr,YStr]);
      end
   else
       begin
       Result := Format('%s-%s-%s',[DStr,MStr,YStr]);
       end;
end;

//*********************************************
// function GetToken
// Result is next commaseparatred string
// var Buffer contains remainder of input string
//*********************************************
function GetToken(var Buffer : String):String;
var
CommaPos : Integer;
begin
Commapos := POs(',',Buffer);
if Commapos > 0 then
   begin
   Result := Copy(Buffer,1,CommaPos-1);
   Buffer := Copy(Buffer,CommaPos+1,Length(Buffer));
   end
else
    begin
    Result := Buffer;
    Buffer := '';
    end;
end;

//*********************************************
// function CSVFieldByNumber(
// Result is then Commaseparated field specified by
// FNumber in input Line TextString
//*********************************************
function CSVFieldByNumber(FNumber: Integer;TextString : String):String;
var
Field      : Integer;
begin
Field := FNumber;
while Field >= 0 do
      begin
      Result :=GetToken(TextString);
      dec(Field);
      end;
end;

function TProductionHistory.AddTagValue(TagValue :String;FieldType : TPHFieldRef):String;
var
Buffer : PChar;
begin
Result := TagValue;
Buffer := AllocMem(1024);
try
if FieldType.Internal then
   begin
   Result := TagValue+',';
   end
else
    begin
    if assigned(FieldType.TagRef) then
       begin
       Named.GetAsString(FieldType.TagRef,Buffer,1024);
       Result :=  TagValue+Buffer+',';
       end
    else
        begin
        Result := TagValue+',';
        end;
    end;
finally
FreeMem(Buffer);
end;

end;

constructor TProductionHistory.Create;
begin
  inherited;
  TagRefList := TList.Create;
  If FileExists(DLLProfilePath+'ProductionHistory.cfg') then
     begin
     Configured := True;
     end
  else
      begin
      Configured := False;
      end;

end;


// This should be called from Install of Install item which
// includes production history
function TProductionHistory.Install:Boolean;
var
CFile       : TIniFile;

begin
RegisteredPath := '';
Cfile :=  TIniFile.Create(DLLProfilePath+'ProductionHistory.cfg');
try
StartTagName := Cfile.ReadString('OUTPUTTAGS','StartTagName','');
StartTag := Named.AquireTag(PChar(StartTagName),'TStringTag',nil);
EndTagName := Cfile.ReadString('OUTPUTTAGS','EndTagName','') ;
EndTag := Named.AquireTag(PChar(EndTagName),'TStringTag',nil);
finally
Cfile.Free;
if assigned(StartTag) and assigned(EndTag) then
   begin
   Result := True;
   end
else
   begin
   Result := False;
   end

end;
end;


function TProductionHistory.FinalInstall: Boolean;
var
CFile       : TIniFile;
NumstartFields,NumEndFields : Integer;
FieldRecord : TPHFieldRef;
FieldNumber : Integer;
Key         : String;
ConfigEntry : String;
InternalString : String;
TagType        : String;
Buffer         : String;
begin
ProdHistory := Self;

Result := True;
Cfile :=  TIniFile.Create(DLLProfilePath+'ProductionHistory.cfg');
try
NumStartFields := CFile.ReadInteger('OUTPUTTAGS','NumStartFields',0);
NumEndFields := CFile.ReadInteger('OUTPUTTAGS','NumEndFields',0);
for FieldNumber := 1 to NumStartFields do
    begin
    FieldRecord := TPHFieldRef.Create;
    FieldRecord.TagRef := nil;
    FieldRecord.FieldType := ftStartField;
    FieldRecord.InternalParam := ipNone;
    FieldRecord.Internal := False;
    FieldRecord.Fixed := False;
    Key := Format('F%d',[FieldNumber]);
    ConfigEntry := CFile.ReadString('STARTFIELDS',Key,'');
    if CSVFieldByNumber(0,ConfigEntry) = 'Fixed' then
       begin
       FieldRecord.Fixed := True;
       FieldRecord.FixedEntry := CSVFieldByNumber(1,ConfigEntry);
       end else
    if CSVFieldByNumber(0,ConfigEntry) = 'Internal' then
       begin
       FieldRecord.Internal := True;
       InternalString := CSVFieldByNumber(1,ConfigEntry);
       if InternalString = 'DateEnglish' then FieldRecord.InternalParam := ipDateEnglish;
       if InternalString = 'DateAmerican' then FieldRecord.InternalParam := ipDateAmerican;
       if InternalString = 'Time' then FieldRecord.InternalParam := ipTime;
       if InternalString = 'OCTName' then FieldRecord.InternalParam := ipOCTName;
       if InternalString = 'OPPName' then FieldRecord.InternalParam := ipOPPName;
       end
    else
       begin
       FieldRecord.Internal := False;
       TagType := CSVFieldByNumber(0,ConfigEntry);
       FieldRecord.TagName := CSVFieldByNumber(1,ConfigEntry);

       if (TagType = 'TStringTag') or (TagType = 'TIntegerTag') or (TagType = 'TDoubleTag')then
          begin
          FieldRecord.TagRef := Named.AquireTag(PChar(FieldRecord.TagName),PChar(TagType),nil);
          end;
       end;
    TagRefList.Add(FieldRecord);
    end;

for FieldNumber := 1 to NumEndFields do
    begin
    FieldRecord := TPHFieldRef.Create;
    FieldRecord.TagRef := nil;
    FieldRecord.FieldType := ftEndField;
    FieldRecord.InternalParam := ipNone;
    Key := Format('F%d',[FieldNumber]);
    ConfigEntry := CFile.ReadString('ENDFIELDS',Key,'');
    if CSVFieldByNumber(0,ConfigEntry) = 'Fixed' then
       begin
       FieldRecord.Fixed := True;
       FieldRecord.FixedEntry := CSVFieldByNumber(1,ConfigEntry);
       end else
    if CSVFieldByNumber(0,ConfigEntry) = 'Internal' then
       begin
       FieldRecord.Internal := True;
       InternalString := CSVFieldByNumber(1,ConfigEntry);
       if InternalString = 'DateEnglish' then FieldRecord.InternalParam := ipDateEnglish;
       if InternalString = 'DateAmerican' then FieldRecord.InternalParam := ipDateAmerican;
       if InternalString = 'Time' then FieldRecord.InternalParam := ipTime;
       if InternalString = 'OCTName' then FieldRecord.InternalParam := ipOCTName;
       if InternalString = 'OPPName' then FieldRecord.InternalParam := ipOPPName;
       if InternalString = 'OPPInterrupted' then FieldRecord.InternalParam := ipOPPInterrupted;
       if InternalString = 'OPPAbandoned' then FieldRecord.InternalParam := ipOPPAbandoned;
       end
    else
       begin
       FieldRecord.Internal := False;
       TagType := CSVFieldByNumber(0,ConfigEntry);
       FieldRecord.TagName := CSVFieldByNumber(1,ConfigEntry);

       if (TagType = 'TStringTag') or (TagType = 'TIntegerTag') or (TagType = 'TDoubleTag')then
          begin
          FieldRecord.TagRef := Named.AquireTag(PChar(FieldRecord.TagName),PChar(TagType),nil);
          end;
       end;
    TagRefList.Add(FieldRecord);
    end;

  MCIDTagName := Cfile.ReadString('INPUTTAGS','MCIDTagName','MachineID');
  if MCIDTagName <> '' then
    begin
    MCIDTag := Named.AquireTag(PChar(MCIDTagName),'TStringTag',nil);
    end;

  Buffer := CFile.ReadString('FILECONTROL','UseBufferFile','F');
  if (Buffer[1] = 'T') or (Buffer[1] = 't') then
    begin
    UseBufferFile := True;
    Buffer := CFile.ReadString('FILECONTROL','OutFilePath','c:\amchem\ProductionHistoryBuffer\');
    if not DirectoryExists(Buffer) then
      begin
      ForceDirectories(Buffer);
      end;
    OutFilePath := Buffer;
    end
  else
    begin
    UseBufferFile := False;
    end;
  Buffer := CFile.ReadString('FILECONTROL','UseOutputFile','F');
  if (Buffer[1] = 'T') or (Buffer[1] = 't') then
    begin
    UseOutFile := True;
    Buffer := CFile.ReadString('FILECONTROL','InProgressFilePath','c:\amchem\ProductionHistory');
    if not DirectoryExists(Buffer) then
      begin
      ForceDirectories(Buffer);
      end;
    InprogFilePath := Buffer;
    end
  else
    begin
    UseBufferFile := False;
    end;
  OutFilePathTag := Named.AquireTag('ProdHistOutPath','TStringTag',nil);
  if UseBufferFile then
     begin
     Named.SetAsString(OutFilePathTag,PChar(OutFilePath));
     end
  else
     begin
     Named.SetAsString(OutFilePathTag,PChar(InprogFilePath));
     end;

  CurrentFileTag := Named.AquireTag('ProdHistFilename','TStringTag',nil);

finally
Cfile.Free;
end;
end;


destructor TProductionHistory.Destroy;
begin
  TagRefList.Free;
  inherited;
end;

procedure TProductionHistory.SaveRecord(RecordString : String);
var
PHFile : TStringList;
begin
if RegisteredPath <> '' then
  begin
  try
  PHFile := TStringList.Create;
  try
  PHFile.LoadFromFile(RegisteredPath);
  PHFile.Add(RecordString);
  PHFile.SaveToFile(RegisteredPath);
  finally
  PHFile.Free;
  end;
  except
  end
  end
end;

procedure TProductionHistory.MakeRecord(RecordType: TFieldType;OCTName,OPPName:String;
                                        Interrupted,Abandoned,Sigwarn,SigFail: Boolean);
function StripTrailingComma(InStr : String) :String;
var
LStr : Integer;
begin
REsult := InStr;
LStr := Length(InStr);
If (LStr > 0) and (InStr[LStr]=',') then
  begin
  Result := Copy(Instr,1,LStr-1);
  end;
end;
var
ListEntryNumber : Integer;
ListEntry       : TPHFieldRef;
TagValue        : String;

begin
if RecordType = ftStartField then
   begin
   StartTagValue := '';
   end
else
    begin
    EndTagValue := '';
    end;
ListEntryNumber := 0;
while (ListEntryNumber < TagrefList.Count) do
      begin
      ListEntry :=  TagrefList[ListEntryNumber];
      if ListEntry.FieldType = RecordType then
         begin
         if ListEntry.Internal then
            begin
            case ListEntry.InternalParam of
                 ipNone:
                 begin
                 TagValue := TagValue +',';
                 end;
                 ipDateEnglish:
                 begin
                 TagValue := TagValue+MakeDateString(Now,False)+',';
                 end;
                 ipDateAmerican:
                 begin
                 TagValue := TagValue+MakeDateString(Now,True)+',';
                 end;
                 ipTime:
                 begin
                 TagValue := TagValue+MakeTimeString(Now)+',';
                 end;
                 ipOCTName:
                 begin
                 TagValue := TagValue+NoExtension(OCTName)+',';
                 end;
                 ipOPPName:
                 begin
                 TagValue := TagValue+NoExtension(OPPName)+',';
                 end;
                 ipOPPInterrupted:
                 begin
                 if Interrupted then
                    begin
                    TagValue := TagValue+'1,';
                    end
                 else
                    begin
                    TagValue := TagValue+'0,';
                    end
                 end;
                 ipOPPAbandoned:
                 begin
                 if Abandoned then
                    begin
                    TagValue := TagValue+'1,';
                    end
                 else
                    begin
                    TagValue := TagValue+'0,';
                    end
                 end;
              end; //case
            end else

         if ListEntry.Fixed then
            begin
            TagValue := TagValue+ListEntry.FixedEntry+',';
            end

         else
             begin
             if assigned(ListEntry.TagRef) then
                begin
                TagValue := AddTagValue(TagValue,ListEntry);
                end;
             end;
         end;
      Inc(ListEntryNumber);
      end;
TagValue := StripTrailingComma(TagValue);
if RecordType = ftStartField then
   begin
   ClearTags;
   StartTagValue := TagValue;
   if assigned(StartTag) then
      begin
      Named.SetAsString(StartTag,PChar(StartTagValue));
      end;
   if RegisteredPath <> '' then
      begin
      SaveRecord(StartTagValue);
      end;

   end
else
    begin
    EndTagValue := TagValue;
    if assigned(EndTag) then
      begin
      Named.SetAsString(EndTag,PChar(EndTagValue));
      end;
   if RegisteredPath <> '' then
      begin
      SaveRecord(EndTagValue);
      end;
    end;
end;

procedure TProductionHistory.ClearTags;
begin
if not Configured then exit;
if assigned(StartTag) then
      begin
      Named.SetAsString(StartTag,'');
      end;
if assigned(EndTag) then
      begin
      Named.SetAsString(StartTag,'');
      end;
end;

function TProductionHistory.RegisterPart(PartName: String): String;
var
FullPath : String;
NewFile  : TStringList;
OldFileName : String;
OldFilePath : String;
Buffer : array [0..1023] of Char;

begin
Result := '';
try
if UseOutFile then
  begin
  try
  if assigned(MCIDTag) then
    begin
    Named.GetAsString(MCIDTag,Buffer,1024);
    end
  else
    begin
    Buffer := 'MachineUndefined';
    end;
  FullPath := SlashSep(InprogFilePath,Format('%s_%s.phf',[PartName,Buffer]));
  if assigned(CurrentFileTag) then
     begin
     try
     Named.SetAsString(CurrentFileTag,PChar(ExtractFilename(FullPath)));
     except
     Named.SetAsString(CurrentFileTag,'PhistFileUnknown');
     end;
     end;
  if not FileExists(FullPath) then
    begin
    NewFile := TStringList.create;
    try
    NewFile.Add(Format('Part %s First Processed in Machine %s on %s',[PartName,Buffer,MakeTimeDateString(Now)]))
    finally
    Result := FullPath;
    NewFile.SaveToFile(FullPath);
    NewFile.Free;
    end; // try finally
    end
  else
    begin
    // Here if file exists
    Result := FullPath;
    end;
  except
  Result := '';
  end; // try except
  end;
if UseBufferFile then
  begin
  if (Result <> '') and (Result <> RegisteredPath) and (RegisteredPath<>'') then
    begin
    if DirectoryExists(OutFilePath) then
      begin
      // transfer Lastpart file to buffer directory
      OldFileName := ExtractFileName(RegisteredPath);
      OldFileName := OldFilename+MakeFileTimeString(Now);
      OldFilePath := SlashSep(OutFilePath,Format('%s.phf',[OldFileName]));
      If MoveFile(PChar(RegisteredPath),PChar(OldFilePath)) then
        begin

        end;
      end;
    end;
  end;
finally
RegisteredPath := Result;
end;

end;


end.
