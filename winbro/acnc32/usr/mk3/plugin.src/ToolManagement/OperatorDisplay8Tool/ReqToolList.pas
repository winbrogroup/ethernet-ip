unit ReqToolList;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls,  ExtCtrls, AbstractFormPlugin, IFormInterface, NamedPlugin,
  ComCtrls, INamedInterface, Grids, ACNC_TouchSoftkey, IOCT, PluginInterface,
  ImgList, CNCTypes, OPDisplayTypes, OCTData,RequiredToolData,ToolDB,ToolData;

type
  RTLEntry = record
    ToolType : String;
    UsagePerTAKT : Double;
    ReplenishedLength : Double;
    TAKTPerTool : Double;
    Priority  : Integer;
    HasDependent : Boolean;
    Present: Boolean;
  end;

  pTEntry = ^RTLEntry;

  TReqToolList = class(TCustomPanel)
  private
    Grid        : TStringGrid;
    TableList   : Tlist;
    TitlePanel  : TPanel;
    GridPanel   : TPanel;
    UnitHeight : Integer;

    procedure MyDrawCell(Sender: TObject; Col, Row: Integer; Rect: TRect;
                         State: TGridDrawState);

    procedure SetTopRow(Value: Integer);
    procedure CleanTableList;
    procedure OrderTableList;
    procedure RefreshDisplay;  // Updates the display without getting the raw data
  protected
  public
    RToolList     : TRequiredToolList;
    MasterTD      : TToolDB;
    ToolList      : TToolList;
    OCTList       : TOPPList;
    InchModeDefault: boolean;
    constructor Create(aOwner : TComponent); override;
    destructor Destroy; override;
    procedure Resize; override;
    procedure ResetGrid;
    procedure UpdateDisplay; // Updates the display after refreshing from raw data
    procedure FinalInstall;
    function GetUnrequiredToolLocation: Integer;
    procedure ShowToolAsPriority1(ToolID : String);




  end;

implementation

uses OCTOperatorDisplay;

{ TReqToolList }

constructor TReqToolList.Create(aOwner: TComponent);
VAR
Translator : TTranslator;
begin
  inherited;
  Translator := TTranslator.Create;
  try
  TitlePanel := TPanel.Create(Self);
  with TitlePanel do
       begin
       Parent := Self;
       Align := alTop;
       Height := 20;
       BevelInner := bvLowered;
       Font.Size := 12;
       Font.Style := [fsbold];
       Caption := Translator.GetString('$REQUIRED_TOOL_LIST');
       end;

  GridPanel := TPanel.Create(Self);
  GridPanel.Parent := Self;
  GridPanel.ParentFont := True;
  GridPanel.Align := alTop;

  Grid := TStringGrid.Create(Self);
  with Grid do
       begin
       Parent := GridPanel;
       Align := alClient;
       ColCount := 4;
       RowCount := 2;
       FixedRows := 1;
       FixedCols := 0;
       ScrollBars := ssNone;
       Font.Size := 12;

       Cells[0,0] := Translator.GetString('$TOOL_TYPE');
       Cells[1,0] := Translator.GetString('$USAGE_PER_OCT');
       Cells[2,0] := Translator.GetString('$Total_Length');
       Cells[3,0] := Translator.GetString('$OCTs_PER_TOOL');
       DefaultDrawing := False;
       end;
  Color := clWindow;

  Grid.OnDrawCell := MyDrawCell;
  UnitHeight := 30;
  TableList := TList.Create;
  finally
  Translator.Free;
  end;

end;



procedure TReqToolList.Resize;
var W : Integer;
begin
  inherited;
  Grid.Width := GridPanel.Width-2;
  Grid.DefaultRowHeight := UnitHeight;
  Grid.GridLineWidth := 1;
  Grid.Height := (Grid.RowCount+3)*(UnitHeight+Grid.GridLineWidth);
  GridPanel.Height := Height-(TitlePanel.Height+2);
  W := (GridPanel.Width - 7) div 5;
  Grid.ColWidths[0] := W * 2;
  Grid.ColWidths[1] := W;
  Grid.ColWidths[2] := W-1;
  Grid.ColWidths[3] := W-1;
  Grid.Invalidate;
end;

procedure TReqToolList.MyDrawCell(Sender: TObject; Col, Row: Longint; Rect: TRect; State: TGridDrawState);
var
Contents : String;
Priority : Integer;
Entry : pTEntry;
Dependent : Boolean;
begin
  try
    Contents := Grid.Cells[Col,row];
  except
    Contents := '??????';
  end;

  if assigned(TableList) and (Row > 0)  then begin
    if Row <= TableList.count then begin
      Entry := TableList[Row-1];
      Priority := Entry.Priority;
      Dependent := Entry.HasDependent;
    end else begin
      Priority := 2;
      Dependent := False;
    end;
  end;
  if (Col = 0) and Dependent then begin
    Contents := Contents+'*';
  end;

  with Grid.Canvas do begin
    Font.Name := Self.Font.Name;
    Font.Size := 12;
    Font.Color := clBlack;
    if Row = 0 then begin
      Font.Size := Self.Font.Size;
      Font.Style := Self.Font.Style;
      Brush.Color := clSilver;
      Font.Size := 10;
    end else begin
      if Entry.Present then begin
        Font.Color := clBlack;
        case Priority of
          0: begin
            Brush.Color := clCream;
          end;
          1:begin
            Brush.Color := clSkyBlue;
          end else begin
            Brush.Color := clBlue;
            Font.Color := clWhite
          end;
        end;
      end else begin
        Brush.Color := clRed;
        Font.Color := clWhite
      end;
    end;

    Font.Style := [fsbold];
    FillRect(Rect);
    TextOut(Rect.Left+5,Rect.Top+3,Contents);
  end;
end;

destructor TReqToolList.Destroy;
begin
  CleanTableList;
  TableList.Free;
  inherited Destroy;
end;

// Tools listed at the botton of the operator display page
procedure TReqToolList.RefreshDisplay;
var
RowNumber : Integer;
Entry : pTEntry;
begin
if assigned(TableList) then
  begin
  Grid.RowCount := TableList.Count+1;
  For RowNumber := 1 to Grid.RowCount-1 do
    begin
    Entry := TableList[RowNumber-1];
    Grid.Cells[0,RowNumber] := Entry.ToolType;
    if InchModeDefault then begin
      Grid.Cells[1,RowNumber] := Format('%6.3f"',[Entry.UsagePerTAKT / 25.4]);
      Grid.Cells[2,RowNumber] := Format('%6.3f"',[Entry.ReplenishedLength / 25.4]);
    end else begin
      Grid.Cells[1,RowNumber] := Format('%6.2fmm',[Entry.UsagePerTAKT]);
      Grid.Cells[2,RowNumber] := Format('%6.2fmm',[Entry.ReplenishedLength]);
    end;
    Grid.Cells[3,RowNumber] := Format('%6.2f',[Entry.TAKTPerTool]);
    end;

  end;
end;

procedure TReqToolList.SetTopRow(Value :Integer);
begin
  if Value < 1 then
    Grid.TopRow := 1
  else
    Grid.TopRow := Value;
end;



procedure TReqToolList.ResetGrid;
begin
  Resize;
  SetTopRow(1);
end;


procedure TReqToolList.FinalInstall;
begin
end;


// On OCT Load use the value in the Length field as the Usage per Takt
// This decreases dynamically as the OCT is performed but the displayed
// parameter must stay constant.

procedure TReqToolList.UpdateDisplay;
var
Entry : pTEntry;
EntryCount : Integer;

begin
if assigned(TableList) and assigned(RToolList) then
  begin
  CleanTableList;
  RToolList.Lock;
  try
  For EntryCount := 0 to RToolList.Count-1 do
    begin
    New(Entry);
    Entry.ToolType := UpperCase(RToolList.Tool[EntryCount].ToolName);
    Entry.UsagePerTAKT := OCTList.TotalOCTUsageforTool(Entry.ToolType);
    Entry.Priority := RToolList.Tool[EntryCount].Priority;
    Entry.ReplenishedLength := MasterTD.TotalUseableForTool(Entry.ToolType);
    Entry.HasDependent := RToolList.Tool[EntryCount].HasDepencencies;
    Entry.Present:= ToolList.ToolTypeInMachine(Entry.ToolType);
    if Entry.UsagePerTAKT > 0 then
      begin
      Entry.TAKTPerTool := Entry.ReplenishedLength/Entry.UsagePerTAKT;
      end
    else
      begin
      Entry.TAKTPerTool := 0;
      end;

    TableList.Add(Entry)
    end;
  finally
  RToolList.Unlock;
  end;
  OrderTableList;
  RefreshDisplay;
  end;
end;

procedure TReqToolList.CleanTableList;
var
Entry : pTEntry;
begin
while TableList.Count > 0 do
  begin
  Entry := TableList[0];
  dispose(Entry);
  TableList.Delete(0);
  end;
end;

procedure TReqToolList.OrderTableList;
var
RowNumber : Integer;
Entry : pTEntry;
NextEntry : pTEntry;
Changed : Boolean;
LoopCount : Integer;
Found : Boolean;
FirstRow : Integer;
begin
if assigned(TableList) then
  begin
  // Intial Sort in priority order only
  LoopCount := 0;
  if TableList.Count < 2 then exit;
  Changed := True;
  while Changed do
    begin
    Changed := False;
    For RowNumber := 0 to TableList.Count -2 do
      begin
      Entry := TableList[RowNumber];
      NextEntry := TableList[RowNumber+1];
      if NextEntry.Priority < Entry.Priority then
        begin
        TableList.Move(RowNumber+1,RowNumber);
        Changed := True;
        end;
      inc(LoopCount);
      if LoopCount > 2000 then Changed := False;
      end;
    end;
  //Find First non-required entry in sorted list
  Found := False;
  RowNumber := -1;
  while (RowNumber < TableList.Count-1) and (not Found) do
    begin
    inc(RowNumber);
    Entry := TableList[RowNumber];
    if Entry.Priority > 1 then Found := True;
    end;

  if Found then
    begin
    if RowNumber >= TableList.Count-1 then exit;
    LoopCount := 0;
    FirstRow := RowNumber;
    Changed := True;
    while Changed do
      begin
      Changed := False;
      For RowNumber := FirstRow to TableList.Count -2 do
        begin
        Entry := TableList[RowNumber];
        NextEntry := TableList[RowNumber+1];
        if (NextEntry.HasDependent) and Not Entry.HasDependent then
          begin
          TableList.Move(RowNumber+1,RowNumber);
          Changed := True;
          end;
        inc(LoopCount);
        if LoopCount > 2000 then Changed := False;
        end;
      end;
    end;

  end;

end;

// returns the EX1 of the first tool which has a priorirty > 1 or -1 if all required
// Also returns -1 if only unrequired tool is on the head.
function TReqToolList.GetUnrequiredToolLocation: Integer;
var I : Integer;
ToolData : TToolData;
Priority : Integer;
begin
  Result := -1;
  if not assigned(RToolList) then exit;
  if not assigned(ToolList) then exit;
  RToolList.Lock;
  ToolList.Lock;
  try
    For I := 1 to 6 do // for each tool location
      begin
      ToolData := ToolList.ToolatEX1(I);
      if ToolData <> nil then
        begin
        Priority := RToolList.PriorityofToolType(ToolData.ShuttleID);
        if Priority >2 then
          begin
          Result := I;
          exit;
          end
        end;
      end;
  finally
    ToolList.UnLock;
    RToolList.Unlock;
  end;

end;



procedure TReqToolList.ShowToolAsPriority1(ToolID : String);
var
EntryNumber : Integer;
Entry : pTEntry;
begin

For EntryNumber := 0 to TableList.Count-1 do
  begin
  Entry := TableList[EntryNumber];
  if Uppercase(Entry.ToolType) = UpperCase(ToolID) then
    begin
    if Entry.Priority >1 then
      begin
      Entry.Priority := 1;
      RefreshDisplay;
      exit;
      end;
    end;
  end;

end;



end.
