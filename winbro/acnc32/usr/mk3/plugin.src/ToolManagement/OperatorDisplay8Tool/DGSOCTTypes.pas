unit DGSOCTTypes;

interface

uses Classes, SysUtils, AbstractOCTPlugin, NamedPlugin, Windows, INamedInterface;

type
  TRRCellMode = (
    rrmProduction,
    rrmStandalone,
    rrmRework,
    rrmDevelopment
  );

//resourcestring
{  OCTAbandonText = 'OCTAbandon';
  PartAbandonText = 'PartAbandon';
  SkipOppText = 'SkipOpp';
  SkipLastOppText = 'SkipLastOpp';
  RestartOPPText = 'RestartOPP';

  MessageCantStartText = 'Unable to start OCT.';
  MessageAbandonOCTText= 'Abandon processing for this part in this machine only has been selected.';
  MessageAbandonPART   = 'Abandon processing for this part In all machines has been selected.';
  MessageAbandonCurrent= 'Restart at next OPP has been selected';
  MessageRestartCurrent= 'Restart of current OPP has been selected';
  MessageRecoveryTriggered  = 'Current program has stopped running';
  MessageAbandonLast   = 'Last operation abandoned.';
  MessageAck           = 'Please press OK to acknowledge';
  MessageCycleStart    = 'Please press Cycle Start';
  MessageGoToManual    = 'Please press OK to go to MANUAL Mode';
  MessageGoToRecovery  = 'Please press OK to see recovery options';
  MessageRequestResetRewind  = 'OPP has stopped due to a Cycle Stop Error';
  MessageAckToCNCSreen  = 'Please press OK to go to CNC screen and then press RESET REWIND for Recovery Options';

  ToolDoorLockedText = 'TOOL DOOR LOCKED';
  ToolDoorUnLockedText = 'TOOL DOOR UNLOCKED';
  {LeftText = 'Left';
  RightText = 'Right';
  BackText = 'Back';
  DoorText = 'At Door';
  NotInPositionText = '----';
  InHeadText = 'In Head';}

type
  TRecoceryOptions =(
    roNone,
    roRestartCurrent,
    roAbandonCurrent,
    roAbandonOCT,
    roAbandonPartInCell,
    roReworkOPP,
    roNotRework
  );

  TOCTResultCode = (
    orcComplete,
    orcReworkSuccess,
    orcReworkRequired,
    orcReworkNonConform,
    orcOPPAbandoned,
    orcOCTAbandoned
  );

function GetToken(var Buffer : String):String;
function CSVField(FNumber: Integer;TextString : String):String;
function RRDateFormat(Time : TDateTime):String;

//var
//  ToolInHead : TTagRef;

implementation




function RRDateFormat(Time : TDateTime):String;
var Hour,Min,Sec,msec : Word;
    Day, Month, Year : Word;
begin
   DecodeTime(Time,Hour,Min,Sec,msec);
   DecodeDate(Time,Year, Month, Day);

   Result := Format('%.2d-%.2d-%.4d %.2d:%.2d:%.2d', [
        Day, Month, Year, Hour, Min, Sec ]);
end;


//*********************************************
// function GetToken
// Result is next commaseparatred string
// var Buffer contains remainder of input string
//*********************************************
function GetToken(var Buffer : String):String;
var
CommaPos : Integer;
begin
Commapos := POs(',',Buffer);
if Commapos > 0 then
   begin
   Result := Copy(Buffer,1,CommaPos-1);
   Buffer := Copy(Buffer,CommaPos+1,Length(Buffer));
   end
else
    begin
    Result := Buffer;
    Buffer := '';
    end;
end;

//*********************************************
// function CSVField(
// Result is then Commaseparated field specified by
// FNumber in input Line TextString
//*********************************************
function CSVField(FNumber: Integer;TextString : String):String;
var
Field      : Integer;
begin
Field := FNumber;
while Field >= 0 do
      begin
      Result :=GetToken(TextString);
      dec(Field);
      end;
end;

end.
