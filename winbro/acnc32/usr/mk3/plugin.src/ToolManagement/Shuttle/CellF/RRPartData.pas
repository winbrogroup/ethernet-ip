unit RRPartData;

interface

uses Classes, SysUtils, IStringArray, NamedPlugin, Windows, CellMode, CNCTypes;

type
  TRREuchnerTag = record
    ShuttleID : array [0..15] of Char;
    PartType : array [0..20] of Char;
    PartSFC : array [0..12] of Char;
    PreCMM : Byte;
    PostCMM : Byte;
    OpDone : array [0..7] of Byte;
    Production : Byte;
    Rework : Byte;
  end;

  TEuchnerDataUnion = record
    case Byte of
      0 : ( Tag : TRREuchnerTag);
      1 : ( Data : array [0..63] of Byte);
  end;

  TPartData = class(TInterfacedObject, IACNC32StringArrayNotify)
  private
    OpTable : IACNC32StringArray;
    PartTable : IACNC32StringArray;
  public
    constructor Create;
    destructor Destroy; override;
    procedure CellChange(Table : IACNC32StringArray; Row, Col : Integer); stdcall;
    procedure TableChange(Table : IACNC32StringArray); stdcall;
    procedure UpdateTables(const Data : TRREuchnerTag);
    procedure GetTagData(var Data : TRREuchnerTag);
    procedure Close;
    procedure Clear;

    procedure CheckCMMFile;
  end;

var
  PartTables : TPartData;

implementation

{ TPartData }

uses PartStatusForm;

constructor TPartData.Create;
var I : Integer;
begin
  inherited Create;

  if StringArraySet.CreateArray('OPS', Self, OPTable) <> AAR_OK then
    StringArraySet.UseArray('OPS', Self, OPTable);

  OpTable.Clear;
  OpTable.AddField('OP');
  OpTable.Append(32);
  for I := 0 to 31 do begin
    OpTable.SetValue(I, 0, '0');
  end;

  if StringArraySet.CreateArray('RRPartData', Self, PartTable) <> AAR_OK then
    StringArraySet.UseArray('RRPartData', Self, PartTable);

  PartTable.Clear;
  PartTable.AddField('FixtureID');
  PartTable.AddField('PartType');
  PartTable.AddField('SN');
  PartTable.AddField('PreCMM');
  PartTable.AddField('PostCMM');
  PartTable.AddField('Production');
  PartTable.AddField('Rework');
  PartTable.Append(1);
end;

procedure TPartData.CellChange(Table: IACNC32StringArray; Row,
  Col: Integer);
begin

end;

procedure TPartData.GetTagData(var Data : TRREuchnerTag);
  function EncodeOperation(Index : Integer) : Byte;
  var S : WideString;
  begin
    OpTable.GetValue(Index, 0, S);
    Result := Ord(S <> '0');
    OpTable.GetValue(Index + 1, 0, S);
    Result := Result + Ord(S <> '0') shl 1;
    OpTable.GetValue(Index + 2, 0, S);
    Result := Result + Ord(S <> '0') shl 2;
    OpTable.GetValue(Index + 3, 0, S);
    Result := Result + Ord(S <> '0') shl 3;
    case Result of
      0..9 : Result := Result + Byte('0');
      10..15 : Result := Result + Byte('A') - 10;
    end;
  end;

var I : Integer;
begin
  Named.GetAsString(PartStatus.Tags[ptShuttleID], Data.ShuttleID, SizeOf(Data.ShuttleID));
  Named.GetAsString(PartStatus.Tags[ptPartType], Data.PartType, SizeOf(Data.PartType));

  Named.GetAsString(PartStatus.Tags[ptPartSFC], Data.PartSFC, SizeOf(Data.PartSFC));
  Data.PreCMM := Named.GetAsInteger(PartStatus.Tags[ptPreCMM]);
  Data.PostCMM := Named.GetAsInteger(PartStatus.Tags[ptPostCMM]);;

  for I := 0 to 7 do begin
    Data.OpDone[I] := EncodeOperation(I * 4);
  end;
  Data.Production := Named.GetAsInteger(PartStatus.Tags[ptProduction]);
  Data.Rework := Named.GetAsInteger(PartStatus.Tags[ptRework]);
end;

procedure TPartData.Clear;
begin
  Named.SetAsBoolean(PartStatus.Tags[ptPreCMM], False);
  Named.SetAsBoolean(PartStatus.Tags[ptPostCMM], False);
  Named.SetAsBoolean(PartStatus.Tags[ptProduction], False);
  Named.SetAsBoolean(PartStatus.Tags[ptRework], False);
  Named.SetAsString(PartStatus.Tags[ptShuttleID], '');
  Named.SetAsString(PartStatus.Tags[ptPartType], '');
  Named.SetAsString(PartStatus.Tags[ptPartSFC], '');

  Named.SetAsString(PartStatus.Tags[ptCMMLocalFileName], PChar(CellMode.LocalCMMPath + '???.cmm'));
  Named.SetAsString(PartStatus.Tags[ptCMMDNCFileName], PChar(CellMode.RemoteCMMPath + '???.cmm'));
end;


procedure TPartData.UpdateTables(const Data : TRREuchnerTag);
  procedure DecodeOperation(Index : Integer; Value : Byte);
  begin
    case Value of
      Byte('0')..Byte('9') : begin
        Value := Value - Byte('0');
      end;

      Byte('A')..Byte('F') : begin
        Value := Value - Byte('A') + 10;
      end;
    end;

    OpTable.BeginUpdate;
    try
      OpTable.SetValue(Index, 0, IntToStr(Ord((Value and 1) <> 0)));
      OpTable.SetValue(Index + 1, 0, IntToStr(Ord((Value and 2) <> 0)));
      OpTable.SetValue(Index + 2, 0, IntToStr(Ord((Value and 4) <> 0)));
      OpTable.SetValue(Index + 3, 0, IntToStr(Ord((Value and 8) <> 0)));
    finally
      OpTable.EndUpdate;
    end;
  end;

var PartType : array [0..40] of Char;
    ShuttleID : array [0..40] of Char;
    PartSFC : array [0..40] of Char;
    I : Integer;
    Tmp: string;
begin
// 0..14 FixtureID
// 16..35 PartType
// 37..48 SN
// 50 PreCMM
// 51 PostCMM
// 52..59 Op Done map
// 61 Prod / rework  1 prod 2 rework
  StrLCopy(ShuttleID, Data.ShuttleID, SizeOf(Data.ShuttleID));
  ShuttleID[10]:= #0;
  ShuttleID[SizeOf(Data.ShuttleID) - 1] := #0;
  Named.SetAsString(PartStatus.Tags[ptShuttleID], ShuttleID);

  StrLCopy(PartType, Data.PartType, SizeOf(Data.PartType));
  PartType[SizeOf(Data.PartType) - 1] := #0;
  Tmp:= PartType;
  Tmp:= Trim(Tmp);
  Named.SetAsString(PartStatus.Tags[ptPartType], PChar(Tmp));

  StrLCopy(PartSFC, Data.PartSFC, 10);//SizeOf(Data.PartSFC));
  PartSFC[9] := #0;
//  PartSFC[SizeOf(Data.PartSFC) - 1] := #0;
  Named.SetAsString(PartStatus.Tags[ptPartSFC], PartSFC);

  for I := 0 to 7 do begin
    DecodeOperation(I * 4, Data.OpDone[I]);
  end;

  Named.SetAsBoolean(PartStatus.Tags[ptPreCMM], Data.PreCMM <> 0);
  Named.SetAsBoolean(PartStatus.Tags[ptPostCMM], Data.PostCMM <> 0);
  Named.SetAsBoolean(PartStatus.Tags[ptProduction], Data.Production <> 0);
  Named.SetAsBoolean(PartStatus.Tags[ptRework], Data.Rework <> 0);

  CellMode.UpdatePathInfo(nil);

  Named.SetAsString(PartStatus.Tags[ptCMMLocalFileName], PChar(CellMode.LocalCMMPath + PartSFC + '.cmm'));
  Named.SetAsString(PartStatus.Tags[ptCMMDNCFileName], PChar(CellMode.RemoteCMMPath + PartSFC + '.cmm'));

  CheckCMMFile;
end;


destructor TPartData.Destroy;
begin
  OpTable := nil;
  PartTable := nil;
  inherited;
end;


procedure TPartData.TableChange(Table: IACNC32StringArray);
begin
end;

procedure TPartData.Close;
begin
  StringArraySet.ReleaseArray(Self, OPTable);
  StringArraySet.ReleaseArray(Self, PartTable);
  OpTable := nil;
  PartTable := nil;
end;

procedure TPartData.CheckCMMFile;
var FNameR, FNameL : array [0..1024] of Char;
    Search : TSearchRec;
    DT : TDateTime;
begin
  Named.GetAsString(PartStatus.Tags[ptCMMDNCFileName], FNameR, 1023);
  Named.GetAsString(PartStatus.Tags[ptCMMLocalFileName], FNameL, 1023);

  Named.SetAsString(PartStatus.Tags[ptCMMOffsetsPath], FNameL);
  Named.SetAsString(PartStatus.Tags[ptCMMOffsetsFile], PChar(ExtractFileName(FNameL)));

  if FileExists(FNameR) then begin
    CopyFile(FNameR, FNameL, False);
  end;


  if FindFirst(LocalCMMPath + '*.cmm', faAnyFile, Search) = 0 then begin
    repeat
      DT := FileDateToDateTime(Search.Time);
      if DT < Now - 4 then  // 4 Days old ????? should be a parameter
        DeleteFile(PChar(LocalCMMPath + Search.Name));
    until FindNext(Search) <> 0;
  end;
  SysUtils.FindClose(Search);

  if Named.GetAsBoolean( PartStatusForm.PartStatus.Tags[ptErrorOnNoCMM]) then begin
    if not FileExists(FNameL) then begin
      Named.SetFaultA('OCT', 'Unable to find CMM file', FaultWarning, nil);
      Exit;
    end;
  end;
end;

end.
