unit C6001Euchner;

interface

uses AbstractPlcPlugin, SysUtils, NamedPlugin, Windows, Classes, INamedInterface,
     SyncObjs, CNCTypes, Messages, IFormInterface, RRPartData, ExtCtrls;

const
  M_C6X1 = WM_USER;

type
  TIBBState = (
    IBB_IDLE,
    IBB_READ,
    IBB_WRITE,
    IBB_RESET,
    IBB_CONFIGURE,
    IBB_READ_COMPLETE,
    IBB_VERIFY,
    IBB_VERIFY_COMPLETE,
    IBB_VERIFY_FAIL,
    IBB_WRITE_COMPLETE,
    IBB_READ_ERROR,
    IBB_WRITE_ERROR,
    IBB_CONFIGURE_COMPLETE,
    IBB_RESET_COMPLETE,
    IBB_HEAD1,
    IBB_HEAD2
  );


  TC6x1Property = (
    c6Read,
    c6Write,
    c6Busy,
    c6Proximity1,
    c6Error,
    c6Reset,
    c6Configure
  );

  TC6x1Properties = set of TC6x1Property;

  TOutputHeaderBit = (
    ohbAV,           // Signal command present
    ohbSpare1,       // not used
    ohbGR,           // Sends BIS systen into ground state
    ohbSpare3,       // not used
    ohbHD,           // Head select 0 = Head 1; 1 = Head 2
    ohbLS,           // LED status, refer to pp16 of manual
    ohbTI,           // Toggle bit in, Flag controller is ready for data
    ohbCT            // Code Type 0 = 32Byte; 1 = 64Byte
  );
  TOutputHeaderBits = set of TOutputHeaderBit;

  TInputHeaderBit = (
    ihbCP,           // Codetag present
    ihbAA,           // Command start
    ihbAE,           // Command complete, no error
    ihbAF,           // Command Error
    ihbIN_KN,        // What the fuck!
    ihbTO,           // Toggle bit
    ihbHF,           // Head Error
    ihbBB            // Ready
  );
  TInputHeaderBits = set of TInputHeaderBit;


  PC6x1Input = ^TC6x1Input;
  TC6x1Input = packed record
    HeaderBits : TInputHeaderBits;
    Data : array [0..6] of Byte;
  end;


  PC6x1Output = ^TC6x1Output;
  TC6x1Output = packed record
    HeaderBits : TOutputHeaderBits;
    Data : array [0..6] of Byte;
  end;

  TBalluffDevice = record
    ISize, OSize : Integer;
    Input : PC6x1Input;
    Output : PC6x1Output;
  end;

  TBalluffState = (
    bsIdle,
    bsRead,
    bsWrite,
    bsConfigure,
    bsReset
  );

  TC6x1Euch = class(TAbstractPlcPlugin)
  private
    VCLUpdateTimer : TTimer;
    Values : TC6x1Properties;
    Device : TBalluffDevice;
    FHead : Boolean;
    PartTag: TEuchnerDataUnion;
    Ndx : Integer;
    Stage : Integer;
    FState : TBalluffState;
    TOState : Boolean;
    StandardHeader : TOutputHeaderBits;
    FDeviceIndex : Integer;
    FDeviceName : string;
    FReadBuffer : string;
    RetryCount : Integer;

    Debug: string;
    FReadData: Boolean;

    procedure SetState(aState : TBalluffState);
    procedure VCLUpdate(Sender: TObject);
  public
    constructor Create( Parameters : string;
                        ResolveDeviceEv : TPlcPluginResolveDeviceEv;
                        ResolveItemEv : TPlcPluginResolveItemEv); override;
    destructor Destroy; override;
    procedure Compile ( var Embedded : TEmbeddedData;
                        aReadEv : TPlcPluginReadEv;
                        ResolveItemEv : TPlcPluginResolveItemEv;
                        Write : Boolean);  override;
    procedure GatePre; override;
    function GatePost : Integer; override;
    function Read(var Embedded : TEmbeddedData) : Boolean; override;
    procedure Write(var Embedded : TEmbeddedData; Line, LastLine : Boolean); override;
    function GateName(var Embedded : TEmbeddedData) : string; override;
    function ItemProperties : string; override;
    property State : TBalluffState read FState write SetState;
    property DeviceName : string read FDeviceName;
    property ReadBuffer : string read FReadBuffer;
    property DeviceIndex : Integer read FDeviceIndex;
    property Head : Boolean read FHead;

    class procedure Install; override;
    class procedure FinalInstall; override;
//    class procedure Configure; override;

  end;

  TM_C6X1 = packed record
    Msg: Cardinal;
    Device: TC6x1Euch;
    Unused: Integer;
    Result: Longint;
  end;

var
  NotifyList : TList;

implementation

uses StreamTokenizer;

{ TC6x1 }

const
  PropertyName : array [TC6x1Property] of TPlcProperty = (
    ( Name : 'Read';        ReadOnly : False ),
    ( Name : 'Write';       ReadOnly : False ),
    ( Name : 'Busy';        ReadOnly : True ),
    ( Name : 'Proximity1';  ReadOnly : True ),
    ( Name : 'Error';       ReadOnly : True ),
    ( Name : 'Reset';       ReadOnly : False ),
    ( Name : 'Configure';   ReadOnly : False )
  );

  DeviceCount = 1;

var
  ReadTag : array [0..DeviceCount-1, Boolean] of TTagRef;
  WriteTag : array [0..DeviceCount-1, Boolean] of TTagRef;


const
  ReadStartAddress = 0;
  ReadLength = 63;
  WriteLength = 8;
  WriteStart = 52;

constructor TC6x1Euch.Create(Parameters: string;
  ResolveDeviceEv: TPlcPluginResolveDeviceEv;
  ResolveItemEv: TPlcPluginResolveItemEv);
var Tmp1 : string;
begin
  inherited;
  Tmp1 := ReadDelimited(Parameters, ';');
  if Tmp1 = '' then
    raise Exception.Create('Not enough parameters');
  FDeviceIndex := StrToInt(Tmp1) mod 4;

  Tmp1 := ReadDelimited(Parameters, ';');
  if Tmp1 = '' then
    raise Exception.Create('Not enough parameters');
  FHead := (StrToInt(Tmp1) mod 2) > 0;

  FDeviceName := ReadDelimited(Parameters, ';');
  if DeviceName = '' then
    raise Exception.Create('Not enough parameters');

  if not ResolveDeviceEv(Self, PChar(DeviceName),
                              Device.ISize,
                              Device.OSize,
                              Pointer(Device.Input),
                              Pointer(Device.Output)) then
    raise Exception.CreateFmt('Cannot find device "%s"', [Tmp1]);

  if (Device.OSize <> 8) or (Device.ISize <> 8) then
    raise Exception.CreateFmt('Device "%s" invalid size', [Tmp1]);

  StandardHeader := [ohbCT];

  VCLUpdateTimer := TTimer.Create(nil);
  VCLUpdateTimer.OnTimer := VCLUpdate;
  VCLUpdateTimer.Enabled := True;
end;

destructor TC6x1Euch.Destroy;
begin
  if Assigned(VCLUpdateTimer) then
    VCLUpdateTimer.Free;

  inherited;
end;

procedure TC6x1Euch.Compile(var Embedded: TEmbeddedData;
  aReadEv: TPlcPluginReadEv; ResolveItemEv: TPlcPluginResolveItemEv;
  Write: Boolean);

var Token : string;
    I : TC6x1Property;
begin
  if aReadEv(Self) <> '.' then
    raise Exception.Create('Period expected');

  Token := aReadEv(Self);
  for I := Low(TC6x1Property) to High(TC6x1Property) do begin
    if CompareText(PropertyName[I].Name, Token) = 0 then begin
      Embedded.Command := Ord(I);
      if Write and PropertyName[I].ReadOnly then
        raise Exception.CreateFmt('Property "%s" is read only', [Token]);
      Exit;
    end;
  end;
  raise Exception.CreateFmt('Unknown property "%s"', [Token]);
end;

function TC6x1Euch.GateName(var Embedded: TEmbeddedData): string;
begin
  Result := PropertyName[TC6x1Property(Embedded.Command)].Name;
end;

const
  bcNoCommand = 0;
  bcReadTag   = 1;
  bcWriteTag  = 2;
  bcReadWrite = 3;
  bcConfigure = 4;
  bcReadConfiguration = 5;


procedure TC6x1Euch.SetState(aState : TBalluffState);
begin
  FState := aState;
  if aState = bsIdle then begin
    Exclude(Values, c6Busy);
    Exclude(Device.Output.HeaderBits, ohbGR);

  end else begin
    Stage := 0;
    Include(Values, c6Busy);
    FillChar(PartTag.Data, Sizeof(PartTag), 0);
  end;
end;


function TC6x1Euch.GatePost: Integer;
  procedure ToggleTI;
  begin
    if ohbTI in Device.Output.HeaderBits then
      Exclude(Device.Output.HeaderBits, ohbTI)
    else
      Include(Device.Output.HeaderBits, ohbTI);
  end;

begin
  case State of
    bsIdle : begin
      Device.Output.Data[0] := bcNoCommand;
      Stage := 0;
      if ihbCP in Device.Input.HeaderBits then
        Include(Values, c6Proximity1)
      else
        Exclude(Values, c6Proximity1);
    end;

    bsRead : begin
      case Stage of
        0 : begin
          RetryCount:= 0;
          Ndx := 0;
          with Device.Output^ do begin
            Data[0] := bcReadTag;
            Data[1] := ReadStartAddress;
            Data[2] := 0;
            Data[3] := ReadLength mod $100;
            Data[4] := ReadLength div $100;
            HeaderBits := [ohbAV] + StandardHeader;
            Stage := 1;
          end;
        end;

        1 : begin
          with Device.Input^ do begin
            if ihbAA in HeaderBits then begin
              if ihbAE in HeaderBits then begin
                Stage := 2;
              end else if ihbAF in HeaderBits then begin
                Inc(RetryCount);
                if RetryCount < 3 then begin
                  Device.Output.HeaderBits := StandardHeader;
                  Device.Output.Data[0] := bcNoCommand;
                  Stage := 0
                end else begin
                  Include(Values, c6Error);
                  FValue := Device.Input.Data[0];
                  PartTag.Data[0] := Value + $30;
                  PartTag.Data[1] := $0;
                  Debug:= 'IBB_READ_ERROR';
                  Stage := 4;
                end;
              end;
            end;
          end;
        end;

        2 : begin
          if Ndx + 7 >= ReadLength then
            CopyMemory(@PartTag.Data[Ndx], @Device.Input.Data[0], ReadLength - Ndx)
          else
            CopyMemory(@PartTag.Data[Ndx], @Device.Input.Data, 7);
          Ndx := Ndx + 7;
          if Ndx < ReadLength then begin
            ToggleTI;
            TOState := ihbTO in Device.Input.HeaderBits;
            Stage := 3;
          end else begin
            Stage := 4;
          end;
        end;

        3 : begin
          if TOState <> (ihbTO in Device.Input.HeaderBits) then
            Stage := 2;
        end;

        4 : begin
          Exclude(Device.Output.HeaderBits, ohbAV);

{          if not(ihbAA in Device.Input.HeaderBits) and
             not(ihbAE in Device.Input.HeaderBits) and
             not(ihbAF in Device.Input.HeaderBits) then begin  }

            PartTag.Data[63]:= 0;
            Named.SetAsString(ReadTag[DeviceIndex, Head], PChar(@PartTag.Data));
            //Named.EventLog('Finished reading');
            FReadData:= True;
            State := bsIdle;
{          end; }
        end;
      end;
    end;

    bsWrite : begin
      case Stage of
        0 : begin
          PartTables.GetTagData(PartTag.Tag);
          Named.SetAsString(WriteTag[DeviceIndex, Head], PChar(@PartTag.Data));
          Ndx := 0;
          with Device.Output^ do begin
            Named.EventLog('Before write string 1');
            Data[0] := bcWriteTag;
            Data[1] := WriteStart;
            Data[2] := 0;
            Data[3] := WriteLength;
            Data[4] := 0;
            HeaderBits := [ohbAV] + StandardHeader;
            Stage := 1;
            TOState := ihbTO in Device.Input.HeaderBits;
            Named.EventLog('DeviceIndex: ' + IntToStr(DeviceIndex));
          end;
        end;

        1 : begin
          with Device.Input^ do begin
            if (ihbAA in HeaderBits) and
             (TOState <> (ihbTO in Device.Input.HeaderBits)) then begin
              Stage := 2;
            end else if ihbAF in Device.Input.HeaderBits then begin
              Inc(RetryCount);
              if RetryCount < 3 then begin
                Stage := 0
              end else begin
                Include(Values, c6Error);
                FValue := Device.Input.Data[0];
                PartTag.Data[0] := Value + $30;
                PartTag.Data[1] := $0;
                Debug:= 'IBB_WRITE_ERROR';
                State := bsIdle;
              end;
            end;
          end;
        end;

        2 : begin
          if Ndx + 7 >= WriteLength then
            CopyMemory(@Device.Output.Data[0], @PartTag.Tag.OpDone[Ndx], WriteLength - Ndx)
          else
            CopyMemory(@Device.Output.Data[0], @PartTag.Tag.Opdone[Ndx], 7);

          Named.EventLog('Output Buffer: ' + Char(Device.Output.Data[0]) + Char(Device.Output.Data[1]));
          Ndx := Ndx + 7;
          ToggleTI;
          TOState := ihbTO in Device.Input.HeaderBits;
          Stage := 3;

          if Ndx >= WriteLength then
            Stage := 4
        end;

        3 : begin
          if (TOState <> (ihbTO in Device.Input.HeaderBits)) then begin
            Stage := 2;
          end;
        end;

        4 : begin
          with Device.Input^ do begin
            if ihbAE in HeaderBits then begin
              Exclude(Device.Output.HeaderBits, ohbAV);
              Stage := 5;
            end
          end;
        end;

        5 : begin
          if not(ihbAA in Device.Input.HeaderBits) and
             not(ihbAE in Device.Input.HeaderBits) { and
             not(ihbAF in Device.Input.HeaderBits) } then
          Debug:= 'IBB_WRITE_COMPLETE';

          Device.Output.Data[0] := bcNoCommand;
          State := bsIdle;
        end;
      end;
    end;

    bsConfigure : begin
      case Stage of
        0 : begin
          Device.Output.Data[0] := bcConfigure;
          Device.Output.HeaderBits := [ohbAV] + StandardHeader;
          Device.Output.Data[6] := Byte(Device.Output.HeaderBits);
          TOState := ihbTO in Device.Input.HeaderBits;
          Stage := 1;
        end;

        1 : begin
            if (ihbAA in Device.Input.HeaderBits) and
             (TOState <> (ihbTO in Device.Input.HeaderBits)) then begin
             with Device.Output^ do begin

               Data[0] := $08;
               Data[1] := $80;
               Data[2] := $00;
               Data[3] := $00;
               Data[4] := $00;
               Data[5] := $00;

               ToggleTI;
               Data[6] := Byte(HeaderBits);
               Stage := 2;
            end;
          end;
        end;


        2 : begin
          with Device.Input^ do begin
            if ihbAE in HeaderBits then begin
              Exclude(Device.Output.HeaderBits, ohbAV);
              Device.Output.Data[6] := Byte(Device.Output.HeaderBits);
              Stage := 3;
            end;
          end;
        end;

        3 : begin
          if not(ihbAA in Device.Input.HeaderBits) and
             not(ihbAE in Device.Input.HeaderBits) { and
             not(ihbAF in Device.Input.HeaderBits) } then
          State := bsIdle;
          Debug:= 'IBB_CONFIGURE_COMPLETE';
        end;
      end;
    end;

    bsReset : begin
      case Stage of
        0 : begin
          FillChar(Device.Output.Data, 0, Sizeof(Device.Output.Data));
          Device.Output.HeaderBits := [ohbGR];
          Device.Output.Data[6] := Byte(Device.Output.HeaderBits);
          if not (ihbBB in Device.Input.HeaderBits) then begin
            Device.Output.HeaderBits := [];
            Device.Output.Data[6] := Byte(Device.Output.HeaderBits);
            Stage := 1;
          end;

          //if ihbAF in Device.Input.HeaderBits then
          //  State := bsIdle;
        end;

        1 : begin
          if ihbBB in Device.Input.HeaderBits then begin
            State := bsIdle;
            Debug:= 'IBB_RESET_COMPLETE';
            FValue := 0;
          end;
        end;
      end;
    end;
  end;
  Result := FValue;
end;

procedure TC6x1Euch.GatePre;
begin
end;

function TC6x1Euch.ItemProperties: string;
var I : TC6x1Property;
begin
  for I := Low(TC6x1Property) to High(TC6x1Property) do
    Result := Result + PropertyName[I].Name + CarRet + BooleanIdent[I in Values] + CarRet;

  Result := Result + '*State' + CarRet + IntToStr(Ord(State)) + CarRet;
  Result := Result + '*Stage' + CarRet + IntToStr(Stage) + CarRet;
  Result := Result + '*Ndx' + CarRet + IntToStr(Ndx) + CarRet;
  Result := Result + '*Header' + CarRet + IntToStr(Byte(Device.Input.HeaderBits)) + CarRet;
  Result := Result + '*ErrCode' + CarRet + IntToStr(Byte(Device.Input.Data[0])) + CarRet;
  Result := Result + '*Debug' + CarRet + Debug + CarRet;
end;

function TC6x1Euch.Read(var Embedded: TEmbeddedData): Boolean;
begin
  Result := TC6x1Property(Embedded.Command) in Values;
end;

procedure TC6x1Euch.Write(var Embedded: TEmbeddedData; Line, LastLine: Boolean);
begin
  if Line <> LastLine then begin
    RetryCount := 0;
    case TC6x1Property(Embedded.Command) of
      c6Read : begin
        if not (c6Busy in Values) and Line then begin
          Debug:= 'IBB_READ';
          State := bsRead;
        end;
      end;

      c6Write : begin
        if not (c6Busy in Values) and Line then begin
          Debug:= 'IBB_WRITE';
          State := bsWrite;
        end;
      end;

      c6Reset : begin
        if Line then begin
          Exclude(Values, c6Error);
          Debug:= 'IBB_RESET';
          State := bsReset;
          Device.Output.HeaderBits := StandardHeader;
        end;
      end;

      c6Configure : begin
        if Line and not (c6Busy in Values) then
          State := bsConfigure;
      end;

    end;
    if Line then begin
      Include(Values, TC6x1Property(Embedded.Command));
    end else begin
      Exclude(Values, TC6x1Property(Embedded.Command));
    end;
  end;
end;

class procedure TC6x1Euch.Install;
var I : Integer;
begin
  for I := 0 to DeviceCount - 1 do begin
    ReadTag[I, False]:= Named.AddTag(PChar(Format('BalluffRead_Euch%dH1', [I])), 'TStringTag');
    ReadTag[I, True] := Named.AddTag(PChar(Format('BalluffRead_Euch%dH2', [I])), 'TStringTag');
  end;
end;

class procedure TC6x1Euch.FinalInstall;
var I : Integer;
begin
  for I := 0 to DeviceCount - 1 do begin
    WriteTag[I, False]:= Named.AquireTag(PChar(Format('BalluffWrite_Euch%dH1', [I])), 'TStringTag', nil);
    WriteTag[I, True] := Named.AquireTag(PChar(Format('BalluffWrite_Euch%dH2', [I])), 'TStringTag', nil);
  end;

  PartTables:= TPartData.Create();
end;


procedure TC6x1Euch.VCLUpdate(Sender: TObject);
begin
  if FReadData then
    PartTables.UpdateTables(PartTag.Tag);

  FReadData := False;
end;

initialization
  TC6x1Euch.AddPlugin('IBBalluffEuch', TC6x1Euch);
end.
