unit PartStatusForm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, CheckLst, ExtCtrls, Buttons, IStringArray, NamedPlugin,
  IFormInterface, OCTUtils, INamedInterface, OCTTypes, AbstractFormPlugin,
  ManualPartForm, IniFiles;

type
  TPartTag = (
    ptPreCMM,
    ptPostCMM,
    ptProduction,
    ptRework,
    ptShuttleID,
    ptPartType,
    ptPartSFC,
    ptCMMLocalFilename,
    ptCMMDNCFilename,
    ptCMMOffsetsPath,
    ptCMMOffsetsFile,
    ptOpIndex,
    ptManualLoad,
    ptManualLoadAck,
    ptClearPartData,
    ptClearPartDataAck,
    ptErrorOnNoCMM
  );

  TPartStatus = class(TInstallForm, IACNC32StringArrayNotify)
    Panel1: TPanel;
    BitBtn1: TBitBtn;
    StaticText1: TStaticText;
    StaticText2: TStaticText;
    StaticText3: TStaticText;
    Bevel1: TBevel;
    Bevel2: TBevel;
    Bevel3: TBevel;
    PartTypeLabel: TStaticText;
    PartSFCLabel: TStaticText;
    ShuttleIDLabel: TStaticText;
    Odds: TCheckListBox;
    OPS0_15: TPaintBox;
    OPS16_31: TPaintBox;

    procedure OPS0_15Paint(Sender: TObject);
    procedure OPS16_31Paint(Sender: TObject);
    procedure TagChange(ATag: TTagRef);
  private
    FinalInstallComplete: Boolean;
    TagNames : array [TPartTag] of string;

    ManualLoadActive: Boolean;
    ClearPartActive: Boolean;
    Table : IACNC32StringArray;
    procedure UpdateVisuals;
    procedure DrawOpDone(PB : TPaintBox; I, B : Integer; Done : Boolean);
    procedure BuildManualPart(const Name: string);
    procedure LoadConfig;
  public
    Tags: array[TPartTag] of TTagRef;
    constructor Create(aOwner : TComponent); override;
    destructor Destroy; override;
    procedure CellChange(Table : IACNC32StringArray; Row, Col : Integer); stdcall;
    procedure TableChange(Table : IACNC32StringArray); stdcall;
    procedure Install; override;
    procedure FinalInstall; override;
    procedure Execute; override;
    procedure CloseForm; override;
    procedure DisplaySweep; override;
  end;

var
  PartStatus: TPartStatus;

implementation

uses PartData, CellMode, RRPartData;

{$R *.DFM}

const
  TagDefinitions : array [TPartTag] of TTagDefinitions = (
    ( Ini: NameOCT_PreCMM; Tag: NameOCT_PreCMM; Owned: True; TagType: 'TIntegerTag' ),
    ( Ini: NameOCT_PostCMM; Tag: NameOCT_PostCMM; Owned: True; TagType: 'TIntegerTag' ),
    ( Ini: NameOCT_Production; Tag: NameOCT_Production; Owned: True; TagType: 'TIntegerTag' ),
    ( Ini: NameOCT_Rework; Tag: NameOCT_Rework; Owned: True; TagType: 'TIntegerTag' ),
    ( Ini: NamePartShuttleID; Tag: NamePartShuttleID; Owned: False; TagType: 'TStringTag' ),
    ( Ini: NamePartType; Tag: NamePartType; Owned: False; TagType: 'TStringTag' ),
    ( Ini: NamePartSFC; Tag: NamePartSFC; Owned: False; TagType: 'TStringTag' ),
    ( Ini: NameCMMLocalFilename; Tag: NameCMMLocalFilename; Owned: True; TagType: 'TStringTag' ),
    ( Ini: NameCMMDNCFilename; Tag: NameCMMDNCFilename; Owned: True; TagType: 'TStringTag' ),
    ( Ini: NameCMMOffsetsPath; Tag: NameCMMOffsetsPath; Owned: True; TagType: 'TStringTag' ),
    ( Ini: NameCMMOffsetsFile; Tag: NameCMMOffsetsFile; Owned: True; TagType: 'TStringTag' ),
    ( Ini: NameOpIndex; Tag: NameOpIndex; Owned: True; TagType: 'TIntegerTag' ),
    ( Ini : 'PartManualLoad'; Tag: 'PartManualLoad'; Owned: False; TagType: 'TIntegerTag' ),
    ( Ini : 'PartManualLoadAck'; Tag: 'PartManualLoadAck'; Owned: True; TagType: 'TIntegerTag' ),
    ( Ini : 'PartDataClear'; Tag: 'PartDataClear'; Owned: True; TagType: 'TIntegerTag' ),
    ( Ini : 'PartDataClearAck'; Tag: 'PartDataClearAck'; Owned: True; TagType: 'TIntegerTag' ),
    ( Ini : 'ErrorOnNoCmm'; Tag: 'MTB1'; Owned : True; TagType : 'TIntegerTag' )

  );

{ TPartStatus }

procedure StaticTagChange(ATag: TTagRef); stdcall;
begin
  PartStatus.TagChange(ATag);
end;

procedure TPartStatus.CellChange(Table: IACNC32StringArray; Row, Col: Integer);
var T : WideString;
begin
  Table.GetValue(Row, 0, T);
  if Row < 16 then
    DrawOpDone(OPS0_15, Row, 1, T <> '0')
  else
    DrawOpDone(OPS16_31, Row - 16, 17, T <> '0')
end;

constructor TPartStatus.Create(aOwner: TComponent);
begin
  inherited;
  StringArraySet.UseArray('OPS', Self, Table);
  PartStatus:= Self;
end;

destructor TPartStatus.Destroy;
begin
  if Assigned(Table) then
    StringArraySet.ReleaseArray(Self, Table);
  Table := nil;
  inherited;
end;

procedure TPartStatus.TableChange(Table: IACNC32StringArray);
begin
  UpdateVisuals;
  Invalidate;
end;

procedure TPartStatus.UpdateVisuals;
var Buffer : array [0..1024] of Char;
begin
  if not FinalInstallComplete then
    Exit;
    
  Named.GetAsString(Tags[ptPartType], Buffer, 1024);
  PartTypeLabel.Caption := Buffer;

  Named.GetAsString(Tags[ptPartSFC], Buffer, 1024);
  PartSFCLabel.Caption := Buffer;

  Named.GetAsString(Tags[ptShuttleID], Buffer, 1024);
  ShuttleIDLabel.Caption := Buffer;

  Odds.Checked[0] := Named.GetAsBoolean(Tags[ptPreCMM]);
  Odds.Checked[1] := Named.GetAsBoolean(Tags[ptPostCMM]);
  Odds.Checked[2] := Named.GetAsBoolean(Tags[ptProduction]);
  Odds.Checked[3] := Named.GetAsBoolean(Tags[ptRework]);
end;

const
  BigFont = 12;
  LittleFont = 7;

procedure TPartStatus.DrawOpDone(PB : TPaintBox; I, B : Integer; Done : Boolean);
var L, IW, IH : Integer;
    Tmp : string;
    S : TSize;
begin
  L := PB.Width * I div 16;
  IW := PB.Width div 16;
  IH := PB.Height div 2;

  Tmp := Format('%2d', [I + B]);
  with PB.Canvas do begin
    Pen.Color := clBlack;
    Pen.Width := 1;
    if Done then
      Brush.Color := $ffff30
    else
      Brush.Color := clBtnFace;

    FillRect(Rect(L + 1, 1, L + IW - 1, PB.Height - 2));

    Font.Size := LittleFont;
    TextOut(L + 3, 2, Tmp[1]);
    TextOut(L + (IW div 2), (IH div 2) - 2, Tmp[2]);

    Font.Size := BigFont;
    S := TextExtent('X');
    if Done then
      TextOut(L + (IW - S.cx) div 2, IH, '1')
    else
      TextOut(L + (IW - S.cx) div 2, IH, '0')
  end;
end;

procedure TPartStatus.OPS0_15Paint(Sender: TObject);
var I : Integer;
    T : WideString;
begin
  if Assigned(Table) then begin
    for I := 0 to 15 do begin
      Table.GetValue(I, 0, T);
      DrawOpDone(OPS0_15, I, 1, T <> '0');
    end;
  end;
end;

procedure TPartStatus.OPS16_31Paint(Sender: TObject);
var I : Integer;
    T : WideString;
begin
  if Assigned(Table) then begin
    for I := 0 to 15 do begin
      Table.GetValue(I + 16, 0, T);
      DrawOpDone(OPS16_31, I, 17, T <> '0');
    end;
  end;
end;

procedure TPartStatus.CloseForm;
begin
  ModalResult:= mrOK;
end;

procedure TPartStatus.FinalInstall;
var I: TPartTag;
begin
  for I := Low(I) to High(I) do begin
    if not TagDefinitions[I].Owned then
      Tags[I] := Named.AquireTag(PChar(TagNames[I]), PChar(TagDefinitions[I].TagType), StaticTagChange);
  end;
  PartTables:= TPartData.Create();
  FinalInstallComplete:= True;
  UpdateVisuals;
end;

procedure TPartStatus.Install;
var I: TPartTag;
begin
  LoadConfig;
  for I := Low(I) to High(I) do begin
    if TagDefinitions[I].Owned then
      Tags[I] := Named.AddTag(PChar(TagNames[I]), PChar(TagDefinitions[I].TagType));
  end;
end;

procedure TPartStatus.Execute;
begin
  ShowModal;
end;

procedure TPartStatus.TagChange(ATag: TTagRef);
begin
  UpdateVisuals;
end;

procedure TPartStatus.DisplaySweep;
begin
  if Named.GetAsBoolean(Tags[ptManualLoad]) then begin
    if not ManualLoadActive then begin
      ManualLoadActive := True;
      BuildManualPart(TManualPart.Execute);
      Named.SetAsBoolean(Tags[ptManualLoadAck], True);
    end;
  end else if Named.GetAsBoolean(Tags[ptManualLoadAck]) then begin
    Named.SetAsBoolean(Tags[ptManualLoadAck], False);
    ManualLoadActive := False;
  end;

  if Named.GetAsBoolean(Tags[ptClearPartData]) then begin
    if not ClearPartActive then begin
      ClearPartActive:= True;
      RRPartData.PartTables.Clear;
      Named.SetAsBoolean(Tags[ptClearPartDataAck], True);
    end;
  end else if Named.GetAsBoolean(Tags[ptClearPartDataAck]) then begin
    Named.SetAsBoolean(Tags[ptClearPartDataAck], False);
    ClearPartActive:= False;
  end;
end;

procedure TPartStatus.BuildManualPart(const Name: string);
var Part: string;
    I: Integer;
begin
  Part:= Name;
  CellMode.UpdatePathInfo(nil);

  Named.SetAsString(Tags[ptCMMLocalFileName], PChar(CellMode.LocalCMMPath + Part + '.cmm'));
  Named.SetAsString(Tags[ptCMMDNCFileName], PChar(CellMode.RemoteCMMPath + Part + '.cmm'));

  Named.SetAsString(Tags[ptPartType], 'Unknown');
  Named.SetAsString(Tags[ptPartSFC], PChar(Part));
  Named.SetAsString(Tags[ptShuttleID], 'Unknown');
  Named.SetAsBoolean(Tags[ptProduction], True);
  Named.SetAsBoolean(Tags[ptPreCMM], True);
  Named.SetAsBoolean(Tags[ptPostCMM], False);
  Named.SetAsBoolean(Tags[ptRework], False);

  if Assigned(Table) then begin
    Table.BeginUpdate;
    try
      for I:= 0 to Table.RecordCount - 1 do begin
        Table.SetValue(I, 0, '0');
      end;
    finally
      Table.EndUpdate;
    end;
  end;
  RRPartData.PartTables.CheckCMMFile;
end;

procedure TPartStatus.LoadConfig;
var Ini : TIniFile;
    I : TPartTag;
begin
  Ini := TIniFile.Create(dllProfilePath + 'CellFShuttle.cfg');
  try
    for I := Low(TPartTag) to High(TPartTag) do begin
      TagNames[I] := Ini.ReadString('PartFormTags', TagDefinitions[I].Ini, TagDefinitions[I].Tag);
    end;
  finally
    Ini.Free;
  end;
end;


initialization
  TAbstractFormPlugin.AddPlugin('PartForm', TPartStatus);
end.
