unit PlcEuchner;

interface

uses AbstractPlcPlugin, SysUtils, NamedPlugin, Windows, Classes, INamedInterface,
     SyncObjs, CNCTypes, EuchnerPort, RRPartData, ExtCtrls;


type
  TEuchnerProperty = (
    epRead,
    epWrite,
    epInitialize,

    epBusy,
    epError,
    epFailure,
    epReadDone,
    epWriteDone,
    epInitialized
  );

  TEuchnerProperties = set of TEuchnerProperty;

  TPlcEuchner = class(TAbstractPlcPlugin)
  private
    Values : TEuchnerProperties;
    Euchner : TEuchnerPort;
    EuchnerTagData : TEuchnerDataUnion;
    VCLUpdateTimer : TTimer;
    procedure EuchnerOnRead(Sender : TObject);
    procedure EuchnerOnWrite(Sender : TObject);
    procedure EuchnerOnInitialize(Sender : TObject);
    procedure VCLUpdate(Sender : TObject);
  public
    FReadData : Boolean;
    constructor Create( Parameters : string;
                        ResolveDeviceEv : TPlcPluginResolveDeviceEv;
                        ResolveItemEv : TPlcPluginResolveItemEv); override;
    destructor Destroy; override;
    procedure Compile ( var Embedded : TEmbeddedData;
                        aReadEv : TPlcPluginReadEv;
                        ResolveItemEv : TPlcPluginResolveItemEv;
                        Write : Boolean);  override;
    procedure GatePre; override;
    function GatePost : Integer; override;
    function Read(var Embedded : TEmbeddedData) : Boolean; override;
    procedure Write(var Embedded : TEmbeddedData; Line, LastLine : Boolean); override;
    function GateName(var Embedded : TEmbeddedData) : string; override;
    function ItemProperties : string; override;
    class procedure Install; override;
    class procedure FinalInstall; override;
    class procedure Shutdown; override;
  end;


implementation

uses Proto;

var
  PlcEuchnerTag : TPlcEuchner;

const
  PropertyName : array [TEuchnerProperty] of TPlcProperty = (
    ( Name : 'Read';          ReadOnly : False ),
    ( Name : 'Write';         ReadOnly : False ),
    ( Name : 'Initialize';    ReadOnly : False ),
    ( Name : 'Busy';          ReadOnly : True  ),
    ( Name : 'Error';         ReadOnly : True  ),
    ( Name : 'Failure';       ReadOnly : True  ),
    ( Name : 'ReadDone';      ReadOnly : True  ),
    ( Name : 'WriteDone';     ReadOnly : True  ),
    ( Name : 'Initialized';   ReadOnly : True  )
  );

{ TPlcEuchner }

procedure TPlcEuchner.Compile(var Embedded: TEmbeddedData;
  aReadEv: TPlcPluginReadEv; ResolveItemEv: TPlcPluginResolveItemEv;
  Write: Boolean);
var Token : string;
    I : TEuchnerProperty;
begin
  Token := aReadEv(Self);
  if Token <> '.' then
    raise Exception.Create('Period [.] expected');

  Token := aReadEv(Self);
  for I := Low(TEuchnerProperty) to High(TEuchnerProperty) do begin
    if CompareText(PropertyName[I].Name, Token) = 0 then begin
      if Write and PropertyName[I].ReadOnly then
        raise Exception.CreateFmt('Property %s is read only', [Token]);
      Embedded.Command := Ord(I);
      Exit;
    end;
  end;
  raise Exception.CreateFmt('Unknown property [%s]', [Token]);
end;

constructor TPlcEuchner.Create(Parameters: string;
  ResolveDeviceEv: TPlcPluginResolveDeviceEv;
  ResolveItemEv: TPlcPluginResolveItemEv);
var Token : string;
    Port : Integer;
begin
  inherited;
  Token := ReadDelimited(Parameters, ';');
  Port := 1;
  if Token <> '' then begin
    try
      Port := StrToInt(Token);
    except
      raise Exception.Create('Euchner invalid comm port number ' + Token);
    end;
  end;

  Euchner := TEuchnerPort.Create(Port);
  Euchner.OnRead := EuchnerOnRead;
  Euchner.OnWrite := EuchnerOnWrite;
  Euchner.OnInitialize := EuchnerOnInitialize;
  PlcEuchnerTag := Self;;
  VCLUpdateTimer := TTimer.Create(nil);
  VCLUpdateTimer.OnTimer := VCLUpdate;
  VCLUpdateTimer.Enabled := True;
end;

destructor TPlcEuchner.Destroy;
begin

  if Assigned(VCLUpdateTimer) then
    VCLUpdateTimer.Free;

  PlcEuchnerTag := nil;
  if Assigned(Euchner) then begin
    Euchner.Terminate;
    Euchner.WaitFor;
  end;
  inherited;
end;

procedure DisplayProtocol(aTag : TTagRef); stdcall;
begin
  if not Named.GetAsBoolean(aTag)  then begin
    if Assigned(PlcEuchnerTag) and not Assigned(Protocol) then begin
      Protocol := TProtocol.Create(nil);
      try
        Protocol.Euchner := PlcEuchnerTag.Euchner;
        Protocol.ShowModal;
      finally
        Protocol.Free;
        Protocol := nil;
      end;
    end;
  end;
end;

class procedure TPlcEuchner.FinalInstall;
begin
  Named.AquireTag('EuchnerProtocol', 'TMethodTag', DisplayProtocol)
end;

class procedure TPlcEuchner.Shutdown;
begin
  PartTables.Close;
end;

function TPlcEuchner.GateName(var Embedded: TEmbeddedData): string;
begin
  Result := PropertyName[TEuchnerProperty(Embedded.Command)].Name;
end;

function TPlcEuchner.GatePost: Integer;
begin
  Result := 0;
end;

procedure TPlcEuchner.GatePre;
begin
  if Euchner.Busy then
    Include(Values, epBusy)
  else
    Exclude(Values, epBusy);

  if Euchner.ErrorState <> 0 then
    Include(Values, epFailure)
  else
    Exclude(Values, epFailure);
end;

class procedure TPlcEuchner.Install;
begin
  PartTables := TPartData.Create;
end;

function TPlcEuchner.ItemProperties: string;
var I : TEuchnerProperty;
begin
  Result := '';
  for I := Low(TEuchnerProperty) to High(TEuchnerProperty) do
    Result := Result + PropertyName[I].Name + CarRet + BooleanIdent[I in Values] + CarRet;
end;

function TPlcEuchner.Read(var Embedded: TEmbeddedData): Boolean;
begin
  Result := TEuchnerProperty(Embedded.Command) in Values;
end;

procedure TPlcEuchner.Write(var Embedded: TEmbeddedData; Line,
  LastLine: Boolean);
begin
  if Line <> LastLine then begin
    if Line then begin
      if not Euchner.Busy then begin
        case TEuchnerProperty(Embedded.Command) of
          epRead : begin
            Include(Values, epBusy);
            Exclude(Values, epReadDone);
            Euchner.ReadData;
          end;

          epWrite : begin
            Include(Values, epBusy);
            Exclude(Values, epWriteDone);
            PartTables.GetTagData(EuchnerTagData.Tag);
            Euchner.WriteData(EuchnerTagData.Data);
          end;

          epInitialize : begin
            Include(Values, epBusy);
            Exclude(Values, epInitialized);
            Euchner.Initialize;
          end;
        end;
      end;
      Include(Values, TEuchnerProperty(Embedded.Command));
    end else begin
      case TEuchnerProperty(Embedded.Command) of
        epRead : Exclude(Values, epReadDone);
        epWrite : Exclude(Values, epWriteDone);
        epInitialize : Exclude(Values, epInitialized);
      end;
      Exclude(Values, epError);
      Exclude(Values, TEuchnerProperty(Embedded.Command));
    end;
  end;
end;

procedure TPlcEuchner.EuchnerOnRead(Sender : TObject);
begin
  if not Euchner.ReadDataOK then begin
    Include(Values, epError);
  end else begin
    CopyMemory(@EuchnerTagData, @Euchner.InputBytes, Sizeof(EuchnerTagData));
    Include(Values, epReadDone);
    FReadData := True;
//    PartTables.UpdateTables(EuchnerTagData.Tag);
  end;
end;

procedure TPlcEuchner.EuchnerOnWrite(Sender : TObject);
begin
  if not Euchner.WriteDataOK then
    Include(Values, epError)
  else
    Include(Values, epWriteDone);
end;

procedure TPlcEuchner.EuchnerOnInitialize(Sender : TObject);
begin
  if not Euchner.ReadDataOK then begin
    Include(Values, epError);
    Include(Values, epInitialized);
  end;
end;


procedure TPlcEuchner.VCLUpdate(Sender: TObject);
begin
  if FReadData then
    PartTables.UpdateTables(EuchnerTagData.Tag);

  FReadData := False;
end;

initialization
  TPlcEuchner.AddPlugin('PartEuchner', TPlcEuchner);
end.
