unit ToolForm;
 { The purpose of this form is to help with the tool management The form created
   allows the data for a specific tool to be recorded in CSV files residing in
   the Data directory under DB. AllToolDB is one fo the database files that it
   populates. Tag data contained in CellFShuttle.CFG is also read in and written to
   via this unit}
interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, ValEdit, ToolData, OCTUtils, ExtCtrls, StdCtrls, Buttons,
  IFormInterface, INamedInterface, C6001, IniFiles, NamedPlugin, PluginInterface,
  AbstractFormPlugin, IToolLife, CNCTypes, OCTTypes, ACNC_BaseKeyBoards,
  ToolDB, AllToolDB, Math, ToolMonitor;

type
  TBalluffToolTag = (
    bttToolChangerWrite,
    bttToolChangerRemove,
    bttHeadWrite,
    bttHeadRemove,
    bttToolChangerPosition,
    bttHypertacToolCode,
    bttHypertacSerialNumber,
    bttHeadWriteString,
    bttToolChangerWriteString,
    bttUniqueToolOffsetX,
    bttUniqueToolOffsetY,
    bttUniqueToolOffsetZ,
    bttUniqueToolOffsetC,
    bttIBBReset,
    bttOperatorToolValidate,
    bttBlockUpdateDatabase,
    bttInhibitToolDialogue,
    bttToolStation0,
    bttToolStation1,
    bttToolStation2,
    bttToolStation3,
    bttToolStation4,
    bttToolStation5,
    bttToolStation6,
    bttToolStation7,
    bttToolStation8,
    bttExpectedTools,
    bttFake,
    bttHypertacMismatch,
    bttToolDialogueActive,
    bttCorrelationX,
    bttCorrelationY,
    bttToolTPM,
    bttHeadSelect,
    bttToolchangeSelect,
    bttToolFormActive,
    bttHeadBtnDisable,
    bttTCBtnDisable
  );

  TBalluffDevice = (
    bdHead,
    bdToolChanger,
    bdNone
  );

  TToolManager = class(TC6x1Client)
    Panel3: TPanel;
    SpeedButton1: TSpeedButton;
    Panel2: TPanel;
    BaseKB1: TBaseKB;
    BaseFPad1: TBaseFPad;
    ToolEditor: TValueListEditor;
    Panel6: TPanel;
    Panel7: TPanel;
    Panel5: TPanel;
    LogMemo: TMemo;
    InfoPanel: TPanel;
    Panel4: TPanel;
    btnToolChanger: TSpeedButton;
    btnHead: TSpeedButton;
    SpeedButton3: TSpeedButton;
    BitBtn1: TBitBtn;
    BtnWrite: TBitBtn;
    Panel1: TPanel;
    procedure btnToolChangerClick(Sender: TObject);
    procedure SpeedButton3MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure SpeedButton3MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure BtnWriteMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure BtnWriteMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FormShow(Sender: TObject);
  private
    LastPosition :integer;
    ReadAtToolDoorMcode : Integer;
    DataType : string;
    Device : array [TBalluffDevice] of string;
    Head : array [TBalluffDevice] of Boolean;
    StateBuffer : array [TBalluffDevice] of string;
    StateIBB : array [TBalluffDevice] of TIBBState;
    StateChange : array [TBalluffDevice] of Boolean;
    BtnSelect : TBalluffDevice;
    InvalidSerial: Integer;

    TagNames : array [TBalluffToolTag] of string;
    ToolList : TToolList;
    ToolDB : TToolDB;
    AllToolDB : TAllToolDB;
    FID : FHandle;
    Translator : TTranslator;
    ToolDefTags: array [0..8] of TTagRef;
    ToolMonitor: TToolMonitor;
    MetricToInch: Double;

    LastToolListCount : Integer;
    ToolChangerToolCount: Integer;
    function LogStrToFloat(Field : Integer) : Double;
    function LogStrToInt(Field : Integer) : Integer;
    function HighPressureField(Field : Integer) : Boolean;
    function OptionalTimeEntry(Field : Integer; Future : Double = 10) : TDateTime;
    procedure AddToolFromEditor(Ex1 : Integer;EditorEntry:Boolean; OffsetVerify: Boolean=False);
    procedure RemoveToolAtEx1(Ex1 : Integer);
    procedure UpdateDisplay;
    procedure AddTool(Ex1 : Integer; Buffer : string);
    procedure TagChanged(ATag : TTagRef);
    procedure ReadConfig;
    procedure WriteConfig;
    procedure DoStateChange(D : TBalluffDevice);
    procedure Logging(D : TBalluffDevice; const Msg : string);
    procedure ToolListChange(Sender : TObject);
    procedure UpdateWriteStrings;
    procedure CreateBalluffString(Ex1 : Integer; Tag : TTagRef);
    procedure LocalizeForm;
    function CheckToolAddition(T : TToolData):Boolean;
    procedure CreateDefinitionStrings;
    procedure UpdateExpectedTools;
    procedure AddInvalidTool(Ex1: Integer);
  protected
    procedure C6x1Notify(Service: TC6x1; Msg : TIBBState); override;
  public
    InchModeTag: TTagRef;
    Tags : array [TBalluffToolTag] of TTagRef;
    procedure Install; override;
    procedure FinalInstall; override;
    procedure Execute; override;
    procedure Shutdown; override;
    procedure DisplaySweep; override;
  end;

var
  ToolManager: TToolManager;

implementation

{$R *.dfm}

uses ToolOKDlg, ToolRegistration;

//var
//  HypMismatchFID : FHandle;
// When the Tag definition below has owned has true next to it then the tag gets created by the code in this
// module when the install is active.  If it has false then the tags are aquired by the code on the final install.
const
  TagDefinitions : array [TBalluffToolTag] of TTagDefinitions = (
     ( Ini : 'ToolChangerWrite'; Tag : 'ToolChangerWrite'; Owned : True; TagType : 'TMethodTag' ),
     ( Ini : 'ToolChangerRemove'; Tag : 'ToolChangerRemove'; Owned : False; TagType : 'TMethodTag' ),
     ( Ini : 'HeadToolWrite'; Tag : 'HeadToolWrite'; Owned : True; TagType : 'TMethodTag' ),
     ( Ini : 'HeadToolRemove'; Tag : 'HeadToolRemove'; Owned : False; TagType : 'TMethodTag' ),
     ( Ini : 'ToolChangerPosition'; Tag : 'ToolChangerPosition'; Owned : False; TagType : 'TIntegerTag' ),
     ( Ini : 'HypertacToolCode'; Tag : 'HypertacToolCode'; Owned : False; TagType : 'TIntegerTag' ),
     ( Ini : 'HypertacSerialNumber'; Tag : 'HypertacSerialNumber'; Owned : False; TagType : 'TIntegerTag' ),
     ( Ini : 'HeadWriteString'; Tag: 'HeadWriteString'; Owned : True; TagType : 'TStringTag' ),
     ( Ini : 'ToolChangerWriteString'; Tag: 'ToolChangerWriteString'; Owned : True; TagType : 'TStringTag' ),
     ( Ini : 'UniqueToolOffsetX'; Tag: 'UniqueToolOffsetX'; Owned : True; TagType : 'TDoubleTag' ),
     ( Ini : 'UniqueToolOffsetY'; Tag: 'UniqueToolOffsetY'; Owned : True; TagType : 'TDoubleTag' ),
     ( Ini : 'UniqueToolOffsetZ'; Tag: 'UniqueToolOffsetZ'; Owned : True; TagType : 'TDoubleTag' ),
     ( Ini : 'UniqueToolOffsetC'; Tag: 'UniqueToolOffsetC'; Owned : True; TagType : 'TDoubleTag' ),
     ( Ini : 'IBBResetPB'; Tag: 'IBBResetPB'; Owned : True; TagType : 'TMethodTag' ),
     ( Ini : 'OperatorToolValidate'; Tag: 'MTB2'; Owned : False; TagType : 'TIntegerTag' ),
     ( Ini : 'BlockUpdateDB'; Tag: 'ToolChangerBlockUpdateDB'; Owned : False; TagType : 'TMethodTag' ),
     ( Ini : 'InhibitToolDialogue'; Tag: 'InhibitToolDialogue'; Owned : False; TagType : 'TIntegerTag' ),
     ( Ini : 'ToolDefinition0'; Tag: 'ToolDefinition0'; Owned : True; TagType : 'TStringTag' ),
     ( Ini : 'ToolDefinition1'; Tag: 'ToolDefinition1'; Owned : True; TagType : 'TStringTag' ),
     ( Ini : 'ToolDefinition2'; Tag: 'ToolDefinition2'; Owned : True; TagType : 'TStringTag' ),
     ( Ini : 'ToolDefinition3'; Tag: 'ToolDefinition3'; Owned : True; TagType : 'TStringTag' ),
     ( Ini : 'ToolDefinition4'; Tag: 'ToolDefinition4'; Owned : True; TagType : 'TStringTag' ),
     ( Ini : 'ToolDefinition5'; Tag: 'ToolDefinition5'; Owned : True; TagType : 'TStringTag' ),
     ( Ini : 'ToolDefinition6'; Tag: 'ToolDefinition6'; Owned : True; TagType : 'TStringTag' ),
     ( Ini : 'ToolDefinition7'; Tag: 'ToolDefinition7'; Owned : True; TagType : 'TStringTag' ),
     ( Ini : 'ToolDefinition8'; Tag: 'ToolDefinition8'; Owned : True; TagType : 'TStringTag' ),
     ( Ini : 'ExpectedTools'; Tag: 'ToolChangerExpectedTools'; Owned : True; TagType : 'TIntegerTag' ),
     ( Ini : 'Fake'; Tag: 'Acnc32Fake'; Owned : False; TagType : 'TIntegerTag' ),
     ( Ini : 'HypertacMismatch'; Tag: 'HypertacMismatch'; Owned : True; TagType : 'TIntegerTag' ),
     ( Ini : 'ToolDialogueActive'; Tag: 'ToolDialogueActive'; Owned : True; TagType : 'TIntegerTag' ),
     ( Ini : 'ToolCorrelationX'; Tag: 'ToolCorrelationX'; Owned : False; TagType : 'TDoubleTag' ),
     ( Ini : 'ToolCorrelationY'; Tag: 'ToolCorrelationY'; Owned : False; TagType : 'TDoubleTag' ),
     ( Ini : 'ToolTPM'; Tag: 'ToolTPM'; Owned : False; TagType : 'TIntegerTag' ),
     ( Ini : 'HeadToolSelect'; Tag : 'TFHeadToolSelect'; Owned : True; TagType : 'TIntegerTag' ),
     ( Ini : 'ToolChangeSelect'; Tag : 'TFToolChangeSelect'; Owned : True; TagType : 'TIntegerTag' ),
     ( Ini : 'ToolFormActive'; Tag : 'ToolFormActive'; Owned : True; TagType : 'TIntegerTag' ),
     ( Ini : 'HeadBtnDisable'; Tag : 'ToolFormHeadBtnDisable'; Owned : False; TagType : 'TIntegerTag' ),
     ( Ini : 'TCBtnDisable'; Tag : 'ToolFormTCBtnDisable'; Owned : False; TagType : 'TIntegerTag' )
  );


  ShuttleIDField = 1;         
  ShuttleSNField = 2;
  RebuildTimeField = 3;
  StatusFlagsField = 4;
  BarrelCountField = 5;
  RemainingBarrelsField = 6;
  MaxToolLengthField = 7;
  MinToolLengthField = 8;
  ActualLengthField = 9;
  UniqueToolXField = 10;
  UniqueToolYField = 11;
  UniqueToolZField = 12;
  UniqueToolCField = 13;
  TPM1 = 14;
  //TPM2 = 15;
  ExpiryField = 15;
  OffsetVerifyField = 16;

  ObjectName = 'BalluffShuttle.ToolForm'; // Just used for parameter in  SetFaultA.

{ TToolManager }

procedure ToolTagChange(ATag : TTagRef); stdcall;
begin
  ToolManager.TagChanged(ATag);
end;


//TestToolAddition='0101,blog,1,10,20,30,40,50,60,70,80,90,1,1,1,1'
procedure TestToolAddition(ATag : TTagRef); stdcall;
begin
  ToolManager.AddTool(1, Named.GetAsOleVariant(ATag));
end;

{
function HypertacFaultReset(FID : FHandle) : Boolean; stdcall;
begin
  Result := HypMismatchFID = 0;
end;
}
// This process is ran initially when the system is powering up and any tags that are created
// by this unit is processed now.The use of the "readConfig" procedure prior to creating the tags
// is done to used the Balluff.cfg file names instead of the internal ones.
procedure TToolManager.Install;
var I : TBalluffToolTag;
begin                                  
  Translator := TTranslator.Create;
  LocalizeForm;

  ToolManager := Self;

  ReadConfig;
  for I := Low(TBalluffToolTag) to High(TBalluffToolTag) do begin
    if TagDefinitions[I].Owned then
      Tags[I] := Named.AddTag(PChar(TagNames[I]), PChar(TagDefinitions[I].TagType));
  end;
  btnHead.Down := True;

  ToolMonitor:= TToolMonitor.Create;
end;
// This process is ran after all elements of the system have been created and this allows any tags that are
// required to be aquired from the system are collected at this point in the power of the ACNC.
procedure TToolManager.FinalInstall;
var I : TBalluffToolTag;
    Tmp: Integer;
begin
  ToolManager := Self;
  ToolList := TToolList.Create;
  ToolDB := TToolDB.Create;
  AllToolDB := TAllToolDB.Create;
  ToolList.OnChange := ToolListChange;

  for I := Low(TBalluffToolTag) to High(TBalluffToolTag) do begin
    if not TagDefinitions[I].Owned then
      Tags[I] := Named.AquireTag(PChar(TagNames[I]), PChar(TagDefinitions[I].TagType), ToolTagChange);
  end;

  Tmp:= 0;
  for I := bttToolStation0 to bttToolStation8 do begin
    ToolDefTags[Tmp] := Tags[I];
    Inc(Tmp);
  end;

  Named.AquireTag('TestToolAddition', 'TStringTag', TestToolAddition);
  InchModeTag := NAmed.AquireTag('NC1InchModeDefault', PChar('TMethodTag'), ToolTagChange);
  if Named.GetAsBoolean(InchModeTag) then
    MetricToInch := 25.4
  else
    MetricToInch := 1;
end;

procedure TToolManager.Shutdown;
begin
  Named.SetAsInteger(Tags[bttToolFormActive],0);
  WriteConfig;
  ToolList.Close;
  ToolList := nil;

  ToolDB.Close;
  ToolDB := nil;
end;

// Reading of the Configuration file CellFShuttle.cfg that normally resides in the Live folder
procedure TToolManager.ReadConfig;
var Ini : TCustomIniFile;
    I :TBalluffToolTag;
begin
  Ini := NamedPlugin.CustomiseConfigurationIni(dllProfilePath + 'CellFShuttle.cfg');
  try
    DataType := Ini.ReadString('ToolForm', 'DataType', 'TM');
    Device[bdHead] := Ini.ReadString('ToolForm', 'HeadBalluffDevice', 'Balluff_1');
    Head[bdHead] := Ini.ReadBool('ToolForm', 'HeadBalluffHead', False);
    Device[bdToolChanger] := Ini.ReadString('ToolForm', 'ToolChangerBalluffDevice', 'Balluff_1');
    Head[bdToolChanger] := Ini.ReadBool('ToolForm', 'ToolChangerBalluffHead', True);
    ReadAtToolDoorMcode := Ini.ReadInteger('ToolForm', 'ReadAtDoorMcode', 79);
    ToolChangerToolCount:= Ini.ReadInteger('ToolForm', 'ToolChangerToolCount', 6 );

    Device[bdNone] := '*';

    for I := Low(TBalluffToolTag) to High(TBalluffToolTag) do begin
      TagNames[I] := Ini.ReadString('ToolFormTags', TagDefinitions[I].Ini, TagDefinitions[I].Tag);
    end;
  finally
    Ini.Free;
  end;
end;

{ WriteConfig removed due to auto configuration enabled }
procedure TToolManager.WriteConfig;
{var Ini : TIniFile;
    I :TBalluffToolTag; }
begin
{
  Ini := TIniFile.Create(dllProfilePath + 'CellFShuttle.cfg');
  try
    Ini.WriteString('ToolForm', 'DataType', DataType);
    Ini.WriteString('ToolForm', 'HeadBalluffDevice', Device[bdHead]);
    Ini.WriteBool('ToolForm', 'HeadBalluffHead', Head[bdHead]);
    Ini.WriteString('ToolForm', 'ToolChangerBalluffDevice', Device[bdToolChanger]);
    Ini.WriteBool('ToolForm', 'ToolChangerBalluffHead', Head[bdToolChanger]);

    for I := Low(TBalluffToolTag) to High(TBalluffToolTag) do begin
      Ini.WriteString('ToolFormTags', TagDefinitions[I].Ini, TagNames[I]);
    end;
  finally
    Ini.Free;
  end;
  }
end;


procedure TToolManager.TagChanged(ATag: TTagRef);
  procedure RemoveTool(TCPos : Integer);
  var ToolType : WideString;
      SN : Integer;
  begin
    if ToolLife.GetEx1ToolType(TCPos, ToolType) = trOK then begin
      if ToolLife.GetEx1ToolSN(TCPos, SN) = trOK then begin
        if ToolLife.RemoveTool(ToolType, SN) = trOK then begin
          Exit;
        end;
      end;
    end;
//    Named.SetFaultA(ObjectName, Format('Failed to removed tool %s from database', [ToolType]), FaultWarning, nil)
  end;

  var
      atdb: TAllToolDBItem;
      ToolType: WideString;
      SN: Integer;
      Tool: TToolData;
begin
  if Named.GetAsBoolean(ATag) then begin

    if ATag = Tags[bttToolChangerRemove] then begin
      RemoveTool(Named.GetAsInteger(Tags[bttToolChangerPosition]));
      Exit;
    end;

    if ATag = Tags[bttHeadRemove] then begin
      RemoveTool(0);
      Exit;
    end;
  end;

  if (ATag = Tags[bttToolChangerPosition]) then begin
    UpdateExpectedTools;

    if Named.GetAsInteger(ATag) <> LastPosition then begin
      if Visible then
        UpdateDisplay;
      LastPosition := Named.GetAsInteger(ATag);
    end;
    if Named.GetAsInteger(Tags[bttToolChangerPosition]) > 0 then
      UpdateWriteStrings;
  end;

  if ATag = Tags[bttCorrelationX] then begin
    if ToolLife.GetEx1ToolType(0, ToolType) = trOK then begin
      if ToolLife.GetEx1ToolSN(0, SN) = trOK then begin
        atdb:= AllToolDB.FindTool(ToolType, SN);
        if Assigned(atdb) then begin
          atdb.CorrelationX:= Named.GetAsDouble(Tags[bttCorrelationX]);
        end;
      end;
    end;
  end;
  if ATag = Tags[bttCorrelationY] then begin
    if ToolLife.GetEx1ToolType(0, ToolType) = trOK then begin
      if ToolLife.GetEx1ToolSN(0, SN) = trOK then begin
        atdb:= AllToolDB.FindTool(ToolType, SN);
        if Assigned(atdb) then begin
          atdb.CorrelationY:= Named.GetAsDouble(Tags[bttCorrelationY]);
        end;
      end;
    end;
  end;

  if ATag = Tags[bttToolTPM] then begin
    Tool:= ToolList.ToolAtEx1(0);
    if Assigned(Tool) then begin
      Tool.TPM1:= Named.GetAsInteger(ATag);
    end;
  end;

  if ATag = InchModeTag then begin
    if Named.GetAsBoolean(InchModeTag) then
      MetricToInch := 25.4
    else
      MetricToInch := 1;
  end;
end;
// when the Toolform is evoked from the PLC this method is ran
procedure TToolManager.Execute;
begin

  if not Visible then begin
    btnHead.Enabled :=  not Named.GetAsBoolean(Tags[bttHeadBtnDisable]);
    btnToolChanger.Enabled := not Named.GetAsBoolean(Tags[bttTCBtnDisable]);

    //If a there is no tool in the head then initialise the Toolchanger button
    if Named.GetAsBoolean(Tags[bttHeadBtnDisable]) then begin
       btnToolChanger.Down := true;
       btnHead.Down := False;
       btnSelect := bdToolchanger;
    end else begin
      btnHead.Down := True;
      btnToolChanger.Down := False;
      btnSelect := bdHead;
    end;

    InfoPanel.Caption := Translator.GetString('$MODIFY_TAG_DATA');
    if Named.GetAsBoolean(InchModeTag) then
      MetricToInch := 25.4
    else
      MetricToInch := 1;
    Named.SetAsInteger(Tags[bttToolFormActive],1);
    ShowModal;
    Named.SetAsInteger(Tags[bttToolFormActive],0);
  end;
end;


// This routine is not thread safe, so it just captures the data
// and inform the main loop that a change has occured
procedure TToolManager.C6x1Notify(Service: TC6x1; Msg : TIBBState);
var I : TBalluffDevice;
    StateDevice : TBalluffDevice;
begin
  StateDevice := bdNone;
  for I := Low(TBalluffDevice) to High(TBalluffDevice) do begin
    if (CompareText(Service.DeviceName, Device[I]) = 0) and
       (Head[I] = Service.Head) then begin
      StateDevice := I;
      Break;
    end;
  end;

  if StateDevice <> bdNone then begin
    if Msg = IBB_READ_COMPLETE then
      StateBuffer[StateDevice] := Service.ReadBuffer;
    StateIBB[StateDevice] := Msg;
    StateChange[StateDevice] := True;
  end;
end;

{ Converts a number to a TDateTime for storage }
function TToolManager.OptionalTimeEntry(Field : Integer; Future : Double = 10) : TDateTime;
var T : string;
begin
  T := ToolEditor.Cells[1, Field];
  if T <> '' then begin
    Result := StrToFloatDef(T, -1);
    if Result < 40000 then
       Result:= Result + Now;
    {if Result < Now then begin
      Result := StorageTimeToDateTime(T);
    end;}
  end else begin
    Result := Now + Future;
  end;
end;

function TToolManager.LogStrToFloat(Field : Integer) : Double;
var Msg : string;
begin
  try
    Result := StrToFloat(Trim(ToolEditor.Cells[1, Field]));
  except
    Msg := 'Invalid ' + ToolEditor.Cells[0, Field] + ' of "' + ToolEditor.Cells[1, Field] + '"';
    Logging(BtnSelect, Msg);
    raise Exception.Create(Msg);
  end;
end;

function TToolManager.LogStrToInt(Field : Integer) : Integer;
var Msg : string;
begin
  try
    Result := StrToIntDef(ToolEditor.Cells[1, Field], 0);
  except
    Msg := 'Invalid ' + ToolEditor.Cells[0, Field] + ' of "' + ToolEditor.Cells[1, Field] + '"';
    Logging(BtnSelect, Msg);
    raise Exception.Create(Msg);
  end;
end;

function TToolManager.HighPressureField(Field: Integer): Boolean;
begin
  Result:= False;
  if Trim(ToolEditor.Cells[1, Field]) = '1' then
    Result:= True;
end;

procedure TToolManager.RemoveToolAtEx1(Ex1 : Integer);
var Tmp : WideString;
    TmpSN : Integer;
begin
  if ToolLife.GetEx1ToolType(Ex1, Tmp) = trOK then begin
    ToolLife.GetEx1ToolSN(Ex1, TmpSN);
    ToolLife.RemoveTool(Tmp, TmpSN);
  end;
end;

//
// NOTE: This procedure may fail in the middle of adding a tool! Rather
//       than throwing a load of code at the problem I'll just raise an
//       error asking the operator to clean up the mess.  This is not
//       really acceptable but right now "needs must".
procedure TToolManager.AddToolFromEditor(Ex1 : Integer;EditorEntry:Boolean; OffsetVerify: Boolean=False);
  function Similar(A, B: Double): Boolean;
  begin
    Result:= Abs(A - B) < (0.0001 * MetricToInch);
  end;

var I : Integer;
    T : TToolDBItem;
    TD : TToolData;
    ToolReset :Boolean;
    RejectedTool: Boolean;
    Verify: Double;
begin

  //Named.SetAsInteger(Tags[bttToolAddFail],0);
  ToolReset := False;
  RejectedTool:= False;
  TD := ToolList.ToolAtEx1(Ex1);
  if  Assigned(TD) then
      begin
      // Remove the present tool
      RemoveToolAtEx1(Ex1);
      end;
  try
    if Trim(ToolEditor.Cells[1, ShuttleIDField]) = '' then
      raise Exception.Create('No tool name specified');

    if ToolLife.AddTool( Trim(ToolEditor.Cells[1, ShuttleIDField]),
                         LogStrToInt(ShuttleSNField),
                         LogStrToFloat(MaxToolLengthField) * MetricToInch,
                         LogStrToFloat(ActualLengthField) * MetricToInch, Ex1, 0) = trOK then begin
      for I := 0 to ToolList.Count - 1 do begin
        if ToolList[I].Ex1 = Ex1 then begin
          ToolList.BeginUpdate;
          try
            Logging(bdNone, 'Rebuild time = ' + ToolEditor.Cells[1, RebuildTimeField] );
            ToolList[I].RebuildTime := OptionalTimeEntry(RebuildTimeField);
            ToolList[I].HP := HighPressureField(StatusFlagsField);
            ToolList[I].BarrelCount := LogStrToInt(BarrelCountField);
            ToolList[I].RemainingBarrels := LogStrToInt(RemainingBarrelsField);
            ToolList[I].MinToolLength := LogStrToFloat(MinToolLengthField) * MetricToInch;
            ToolList[I].UniqueToolOffsets[0] := LogStrToFloat(UniqueToolXField) * MetricToInch;
            ToolList[I].UniqueToolOffsets[1] := LogStrToFloat(UniqueToolYField) * MetricToInch;
            ToolList[I].UniqueToolOffsets[2] := LogStrToFloat(UniqueToolZField) * MetricToInch;
            ToolList[I].Expiry := OptionalTimeEntry(ExpiryField);
            ToolList[I].TPM1:= LogStrToInt(TPM1);
            Verify:= StrToFloatDef(ToolEditor.Cells[1, OffsetVerifyField], 0);

            if Ex1 = 0 then begin
              Named.SetAsInteger(Tags[bttToolTPM], ToolList[I].TPM1);
            end;

            if ToolList[I].Expiry < Now then begin
              ToolList[I].RemainingBarrels := 0;
              ToolList[I].ActToolLength := 0;
              RejectedTool:= True;
              Named.SetFaultA(ObjectName, Translator.GetString('$TOOL_EXPIRED_ERROR'), FaultInterlock, nil);
            end;

            if ToolList[I].TPM1 <= 0 then begin
              ToolList[I].RemainingBarrels := 0;
              ToolList[I].ActToolLength := 0;
              RejectedTool:= True;
              Named.SetFaultA(ObjectName, Translator.GetString('$TOOL_TPM_COUNT'), FaultInterlock, nil);
            end;

            Logging(bdNone, 'Rebuild time = ' + FloatToStr(ToolList[I].RebuildTime) + ' ' + FloatToStr(Now) );
            if ToolList[I].RebuildTime < Now then begin
              ToolList[I].RemainingBarrels := 0;
              ToolList[I].ActToolLength := 0;
              RejectedTool:= True;
              Named.SetFaultA(ObjectName, Translator.GetString('$TOOL_REBUILD_EXCEEDED'), FaultInterlock, nil);
            end;

            if not Similar (ToolList[I].UniqueToolOffsets[0] +
                            ToolList[I].UniqueToolOffsets[1] +
                            ToolList[I].UniqueToolOffsets[2], (Verify * MetricToInch)) and OffsetVerify then begin
              ToolList[I].RemainingBarrels := 0;
              ToolList[I].ActToolLength := 0;
              Named.SetFaultA(ObjectName, Translator.GetString('$OFFSET_VERIFICATION_FAILED'), FaultInterlock, nil);
            end;

            ToolList[I].StatusFlags:= ToolList[I].StatusFlags or 1; // New tool loaded
            if ToolList[I].BarrelCount = 0 then
              ToolList[I].BarrelCount:= 1;

            if not EditorEntry and not RejectedTool then begin
              ToolReset := CheckToolAddition(ToolList[I]); //returns true if toollife reset
            end
          finally
            ToolList.EndUpdate;
          end;

          UpdateWriteStrings;

          ToolDB.BeginUpdate;
          try
            T := ToolDB.FindTool(ToolEditor.Cells[1, ShuttleIDField]);
            if not Assigned(T) then begin
              T := ToolDB.AddTool;
            end;
            T.Assign(ToolList[I]);
          finally
            ToolDB.EndUpdate;
          end;
          Self.UpdateDisplay;
        end;
      end; // End of For Loop?
    if ToolReset and (Ex1 > 0) then
           begin
           //Writedata to tag
           Named.SetAsInteger(Tags[bttToolChangerWrite], 1);
           end;

    end;
  except
    on E : Exception do begin
      if FID <> 0 then begin
        Named.FaultResetID(FID);
        FID := 0;
      end;
      FID := Named.SetFaultA(ObjectName, Translator.GetString('$FAILED_TO_ADD_TOOL') + E.Message, FaultInterlock, nil);
      RemoveToolAtEx1(Ex1);
      AddInvalidTool(Ex1);
    end;
  end;
end;


procedure TToolManager.AddTool(Ex1 : Integer; Buffer : string);
var TDB : TToolDBItem;
  //This function is only used in this procedure.
  function HighPressure(const Val: string): string;
  var Tmp: Integer;
  begin
    Tmp:= StrToIntDef(Val, 0);

    Result:= '0';
    if (Tmp and ToolData.TOOL_HIGH_PRESSURE) <> 0 then
      Result:= '1';
  end; //End of function

  function UnitConversion(Inp: string): string;
  var Tmp: Double;
  begin
    Tmp:= StrToFloatDef(Inp, 0);
    Tmp:= Tmp / MetricToInch;
    Result:= Format('%0.4f', [Tmp]);
  end;

//Start of the AddTool Procedures code
begin
  try
    try
      // 3 types of datatype are check for, TM, RR or hypertac.
      if DataType = 'TM' then begin
        if CncTypes.ReadDelimited(Buffer) <> '0101' then
          raise Exception.Create('Incorrect Version');
        ToolEditor.Cells[1, ShuttleIDField] := CncTypes.ReadDelimited(Buffer);
        ToolEditor.Cells[1, ShuttleSNField] := CncTypes.ReadDelimited(Buffer);
        ToolEditor.Cells[1, RebuildTimeField] := Format('%.3f', [ StorageTimeToDateTime(CncTypes.ReadDelimited(Buffer))]);
        ToolEditor.Cells[1, StatusFlagsField] := HighPressure(CncTypes.ReadDelimited(Buffer));
        ToolEditor.Cells[1, BarrelCountField] := CncTypes.ReadDelimited(Buffer);
        ToolEditor.Cells[1, RemainingBarrelsField] := CncTypes.ReadDelimited(Buffer);
        ToolEditor.Cells[1, MaxToolLengthField] := UnitConversion(CncTypes.ReadDelimited(Buffer));
        ToolEditor.Cells[1, MinToolLengthField] := UnitConversion(CncTypes.ReadDelimited(Buffer));
        ToolEditor.Cells[1, ActualLengthField] := UnitConversion(CncTypes.ReadDelimited(Buffer));
        ToolEditor.Cells[1, UniqueToolXField] := UnitConversion(CncTypes.ReadDelimited(Buffer));
        ToolEditor.Cells[1, UniqueToolYField] := UnitConversion(CncTypes.ReadDelimited(Buffer));
        ToolEditor.Cells[1, UniqueToolZField] := UnitConversion(CncTypes.ReadDelimited(Buffer));
        ToolEditor.Cells[1, ExpiryField] := Format('%.3f', [ StorageTimeToDateTime(CncTypes.ReadDelimited(Buffer))]);
        ToolEditor.Cells[1, UniqueToolCField] := '0';
        ToolEditor.Cells[1, TPM1] := '1';
        //ToolEditor.Cells[1, TPM2] := '0';
      end else if DataType = 'RR' then begin
        // Tool editor specifically for RR
        ToolEditor.Cells[1, ShuttleIDField] := CncTypes.ReadDelimited(Buffer);
        ToolEditor.Cells[1, ShuttleSNField] := CncTypes.ReadDelimited(Buffer);
        ToolEditor.Cells[1, MaxToolLengthField] := UnitConversion(CncTypes.ReadDelimited(Buffer));
        ToolEditor.Cells[1, ActualLengthField] := UnitConversion(CncTypes.ReadDelimited(Buffer));
        ToolEditor.Cells[1, RebuildTimeField] := CncTypes.ReadDelimited(Buffer);
        ToolEditor.Cells[1, MinToolLengthField] := UnitConversion(CncTypes.ReadDelimited(Buffer));
        ToolEditor.Cells[1, UniqueToolXField] := UnitConversion(CncTypes.ReadDelimited(Buffer));
        ToolEditor.Cells[1, UniqueToolYField] := UnitConversion(CncTypes.ReadDelimited(Buffer));
        ToolEditor.Cells[1, UniqueToolZField] := UnitConversion(CncTypes.ReadDelimited(Buffer));

        ToolEditor.Cells[1, TPM1] := CncTypes.ReadDelimited(Buffer);
        ToolEditor.Cells[1, ExpiryField] := CncTypes.ReadDelimited(Buffer);
        //ToolEditor.Cells[1, TPM2] := CncTypes.ReadDelimited(Buffer);
        ToolEditor.Cells[1, UniqueToolCField] := CncTypes.ReadDelimited(Buffer);

        ToolEditor.Cells[1, StatusFlagsField] := CNCTypes.ReadDelimited(Buffer);
        ToolEditor.Cells[1, BarrelCountField] := CncTypes.ReadDelimited(Buffer);
        ToolEditor.Cells[1, RemainingBarrelsField] := CncTypes.ReadDelimited(Buffer);

      end else if DataType = 'Hypertac' then begin

        if Ex1 = 0 then begin
          ToolEditor.Cells[1, ShuttleIDField] := IntToStr(Named.GetAsInteger(Tags[bttHypertacToolCode]));
          ToolEditor.Cells[1, ShuttleSNField] := IntToStr(Named.GetAsInteger(Tags[bttHypertacSerialNumber]));
        end else begin
          ToolEditor.Cells[1, ShuttleIDField] := '';
          ToolEditor.Cells[1, ShuttleSNField] := '0';
        end;


        ToolEditor.Cells[1, RebuildTimeField] := Format('%.3f', [100]);
        ToolEditor.Cells[1, StatusFlagsField] := '0';
        ToolEditor.Cells[1, BarrelCountField] := '1';
        ToolEditor.Cells[1, RemainingBarrelsField] := '1';

        ToolDB.Lock;
        try
          TDB := ToolDB.FindTool(IntToStr(Named.GetAsInteger(Tags[bttHypertacToolCode])));

          if Assigned(TDB) then begin
            ToolEditor.Cells[1, MaxToolLengthField] := Format('.3f', [TDB.MaxToolLength]);
            ToolEditor.Cells[1, MinToolLengthField] := Format('.3f', [TDB.MinToolLength]);
            ToolEditor.Cells[1, ActualLengthField] := Format('.3f', [TDB.MaxToolLength - TDB.MinToolLength]);
          end else begin
            ToolEditor.Cells[1, MaxToolLengthField] := '450';
            ToolEditor.Cells[1, MinToolLengthField] := '150';
            ToolEditor.Cells[1, ActualLengthField] := '300';
          end;
        finally
          ToolDB.Unlock;
        end;

        ToolEditor.Cells[1, UniqueToolXField] := '0';
        ToolEditor.Cells[1, UniqueToolYField] := '0';
        ToolEditor.Cells[1, UniqueToolZField] := '0';
        ToolEditor.Cells[1, ExpiryField] := Format('%.3f', [100]);

        btnHead.Enabled := False;
        btnToolChanger.Enabled := False;
        InfoPanel.Caption := 'Enter data for Hypertac tool';

        if not Visible then
          ShowModal

      end else begin
        Named.SetFaultA(ObjectName, 'Invalid Datatype programmed in ' + InstallName + '.cfg' + Datatype, FaultWarning, nil);
        Exit;
      end;

      AddToolFromEditor(Ex1,False, False);

    except
      Named.SetFaultA(ObjectName, Translator.GetString('$INVALID_TOOL_DATA'), FaultWarning, nil);
      AddInvalidTool(Ex1);
    end;
  finally
  end;
end; // End of Procedure


procedure TToolManager.AddInvalidTool(Ex1: Integer);
begin
  RemoveToolAtEx1(Ex1);
  InvalidSerial:= (InvalidSerial mod 32) + 1;
  ToolLife.AddTool('INVALID', InvalidSerial, 0, 0, Ex1, 0);
end;

const BIdent : array[Boolean] of string = (
  '0', '1'
);

// This procedure is used to upadte the form display with the data from the
// data base.
procedure TToolManager.UpdateDisplay;

var I : Integer;
    Ex1 : Integer;

begin
  for I := 1 to ToolEditor.RowCount - 1 do begin
    ToolEditor.Cells[1, I] := '';
  end;

  if btnSelect = bdHead then begin
    Ex1 := 0
  end else begin
    Ex1 := Named.GetAsInteger(Tags[bttToolChangerPosition]);
    if Ex1 = 0 then begin
      InfoPanel.Caption := Translator.getString('$TOOL_CHANGER_NOT_IN_POSITION');
      Exit;
    end;
  end;

  ToolList.Lock;
  try
    for I := 0 to ToolList.Count - 1 do begin
      if Toollist[I].Ex1 = Ex1 then begin
        ToolEditor.Cells[1, ShuttleIDField] := ToolList[I].ShuttleID;
        ToolEditor.Cells[1, ShuttleSNField] := IntToStr(ToolList[I].ShuttleSN);
        ToolEditor.Cells[1, RebuildTimeField] := Format('%.3f', [ToolList[I].RebuildTime - Now]);
        ToolEditor.Cells[1, StatusFlagsField] := BIdent[ToolList[I].HP];
        ToolEditor.Cells[1, BarrelCountField] := IntToStr(ToolList[I].BarrelCount);


        ToolEditor.Cells[1, RemainingBarrelsField] := FloatToStr(ToolList[I].RemainingBarrels);
        ToolEditor.Cells[1, MaxToolLengthField] := FloatToStr(ToolList[I].MaxToolLength / MetricToInch);
        ToolEditor.Cells[1, MinToolLengthField] := FloatToStr(ToolList[I].MinToolLength / MetricToInch);
        ToolEditor.Cells[1, ActualLengthField] := FloatToStr(ToolList[I].ActToolLength / MetricToInch);
        ToolEditor.Cells[1, UniqueToolXField] := FloatToStrF((ToolList[I].UniqueToolOffsets[0] / MetricToInch),Fffixed,6,4);
        ToolEditor.Cells[1, UniqueToolYField] := FloatToStrF((ToolList[I].UniqueToolOffsets[1] / MetricToInch),Fffixed,6,4);
        ToolEditor.Cells[1, UniqueToolZField] := FloatToStrf((ToolList[I].UniqueToolOffsets[2] / MetricToInch),Fffixed,6,4);
        ToolEditor.Cells[1, UniqueToolCField] := FloatToStr(ToolList[I].UniqueToolOffsets[3]);


{        ToolEditor.Cells[1, RemainingBarrelsField] := FloatToStr(ToolList[I].RemainingBarrels);
        ToolEditor.Cells[1, MaxToolLengthField] := FloatToStr(ToolList[I].MaxToolLength / MetricToInch);
        ToolEditor.Cells[1, MinToolLengthField] := FloatToStr(ToolList[I].MinToolLength / MetricToInch);
        ToolEditor.Cells[1, ActualLengthField] := FloatToStr(ToolList[I].ActToolLength / MetricToInch);
        ToolEditor.Cells[1, UniqueToolXField] := FloatToStr(ToolList[I].UniqueToolOffsets[0] / MetricToInch);
        ToolEditor.Cells[1, UniqueToolYField] := FloatToStr(ToolList[I].UniqueToolOffsets[1] / MetricToInch);
        ToolEditor.Cells[1, UniqueToolZField] := FloatToStr(ToolList[I].UniqueToolOffsets[2] / MetricToInch);
        ToolEditor.Cells[1, UniqueToolCField] := FloatToStr(ToolList[I].UniqueToolOffsets[3]);

 }
        ToolEditor.Cells[1, TPM1]:= FloatToStr(ToolList[I].TPM1);
        //ToolEditor.Cells[1, TPM2]:= FloatToStr(ToolList[I].TPM2);
        ToolEditor.Cells[1, ExpiryField] := Format('%.3f', [ToolList[I].Expiry - Now]);
        Exit;
      end;
    end;
  finally
    ToolList.Unlock;
  end;
end;

const
  LogPrefix : array[TBalluffDevice] of string = (
    'Head: ', 'TC:   ', ''
  );

procedure TToolManager.Logging(D : TBalluffDevice; const Msg : string);
var I : Integer;
begin
  if LogMemo.Lines.Count > 100 then begin // ???? Buffer size in ini.
    LogMemo.Lines.BeginUpdate;
    try
      for I := 0 to 20 do
        LogMemo.Lines.Delete(0);
    finally
      LogMemo.Lines.EndUpdate;
    end;
  end;
  LogMemo.Lines.Add(LogPrefix[D] + Msg);
end;

//****************************************************************************
// State MAchine for the Balluff operations - PLC from the ACNC accesses these
procedure TToolManager.DoStateChange(D : TBalluffDevice);
var Ex1 : Integer;
begin
  StateChange[D] := False;

  case StateIBB[D] of
    IBB_IDLE : begin
      Logging(D, 'Idle');
    end;

    IBB_READ : begin
      Logging(D, 'Read');
    end;

    IBB_WRITE : begin
      Named.SetAsInteger(Tags[bttHeadWrite], 0);
      Named.SetAsInteger(Tags[bttToolChangerWrite], 0);
      Logging(D, 'Write');
    end;

    IBB_RESET : begin
      Named.SetAsInteger(Tags[bttHeadWrite], 0);
      Named.SetAsInteger(Tags[bttToolChangerWrite], 0);
      Named.SetAsInteger(Tags[bttIBBReset], 0);
      Logging(D, 'Reset');
    end;

    IBB_CONFIGURE : begin
      Named.SetAsInteger(Tags[bttHeadWrite], 0);
      Named.SetAsInteger(Tags[bttToolChangerWrite], 0);
      Logging(D, 'Configure');
    end;

    IBB_READ_COMPLETE : begin
      Logging(D, 'Read Complete');
      if D = bdHead then
        Ex1 := 0
      else
        Ex1 := Named.GetAsInteger(Tags[bttToolChangerPosition]);

      if D = bdHead then
        Logging(D, '--HEAD = ' + IntToStr(Ex1))
      else
        Logging(D, '--TC   = ' + IntToStr(Ex1));

      Logging(D, 'String = "' + StateBuffer[D] + '"');
      AddTool(Ex1, StateBuffer[D]);
    end;

    IBB_WRITE_COMPLETE : begin
      Logging(D, 'Write Complete');
    end;

    IBB_READ_ERROR : begin
      Logging(D, 'Read Error');
    end;

    IBB_WRITE_ERROR : begin
      Logging(D, 'Write Error');
    end;

    IBB_CONFIGURE_COMPLETE : begin
      Logging(D, 'Configure Complete');
    end;

    IBB_RESET_COMPLETE : begin
      Logging(D, 'Reset Complete');
    end;

    IBB_HEAD1 : begin
      Logging(D, 'Head1');
    end;

    IBB_HEAD2 : begin
      Logging(D, 'Head2');
    end;

  end;
  if Visible and (BtnSelect = D) then
    UpdateDisplay;
end;
//************* END OF STATE MACHINE FOR BALLUFF OPERATIONS **********************************

procedure TToolManager.DisplaySweep;
var I : TBalluffDevice;
begin
  for I := Low(TBalluffDevice) to High(TBalluffDevice) do begin
    if StateChange[I] then begin
      DoStateChange(I);
    end;
  end;

end;

//The same method is used for the pressing of either the head or the toolchanger button.
procedure TToolManager.btnToolChangerClick(Sender: TObject);
begin
    if BtnHead.Down then  begin
      btnSelect := bdHead;
      Named.SetAsInteger(Tags[bttHeadSelect],1);
      Named.SetAsInteger(Tags[bttToolChangeSelect],0);
    end else begin
      btnSelect := bdToolChanger;
      Named.SetAsInteger(Tags[bttHeadSelect],0);
      Named.setAsInteger(Tags[bttToolChangeSelect],1);
    end;

  UpdateDisplay;
end;

// Any added tool is the last in the list?
function TToolManager.CheckToolAddition(T : TToolData):Boolean;
begin
  Result := False;
  ToolList.Lock;
  try
    if Named.GetAsBoolean(Tags[bttOperatorToolValidate])
       {and not Named.GetAsBoolean(Tags[bttBlockUpdateDatabase])} then begin

      if T.BarrelCount > 1 then begin
        if T.BarrelCount <> T.RemainingBarrels then begin
          Named.SetAsBoolean(Tags[bttToolDialogueActive], True);
          try
          case ToolOKDlg.TIsToolOK.Execute(T) of
            1: begin
              T.RemainingBarrels := T.BarrelCount;
              T.ActToolLength := 0;
              Result := True;
            end;
            2: begin
              T.ActToolLength := T.MaxToolLength;
              Result := True;
            end;
          end;

            //if ToolOKDlg.TIsToolOK.Execute(T) then begin
             // T.RemainingBarrels := T.BarrelCount;
              //T.ActToolLength := 0;
              //Result := True;
            //end;
          finally
            Named.SetAsBoolean(Tags[bttToolDialogueActive], False);
          end
        end;
      end else
      begin
        if T.ActToolLength < T.MaxToolLength then begin
          Named.SetAsBoolean(Tags[bttToolDialogueActive], True);
          try
            if ToolOKDlg.TIsToolOK.Execute(T) = 2 then begin
              T.ActToolLength := T.MaxToolLength;
              Result := True;
            end;
          finally
            Named.SetAsBoolean(Tags[bttToolDialogueActive], False);
          end
        end;
      end;
    end;
  finally
    ToolList.Unlock;
  end;
end;

procedure TToolManager.ToolListChange(Sender: TObject);
begin
  LastToolListCount := ToolList.Count;
  UpdateWriteStrings;
  UpdateExpectedTools;
end;

procedure TToolManager.CreateDefinitionStrings;
var I: Integer;
    Tmp: array [0..8] of string;
    Tool: TToolData;
begin
  for I:= 0 to 8 do begin
    Tmp[I]:= ',,';
  end;

  for I:= 0 to ToolList.Count - 1 do begin
    Tool:= ToolList.Tool[I];
    Tmp[Tool.Ex1 mod 9] := Tool.ShuttleID + ',' +
                           IntToStr(Tool.ShuttleSN) + ',' +
                           FloatToStr(Tool.TotalRemaining);
  end;

  for I:= 0 to 8 do begin
    Named.SetAsString(ToolDefTags[I], PChar(Tmp[I]));
  end;
end;

procedure TToolManager.UpdateWriteStrings;
var Tmp : Integer;
begin
  CreateDefinitionStrings;
  CreateBalluffString(0, Tags[bttHeadWriteString]);

  Tmp := Named.GetAsInteger(Tags[bttToolChangerPosition]);
  if Tmp > 0 then
    CreateBalluffString(Tmp, Tags[bttToolChangerWriteString]);
end;

// Method used to create and update the Balluf string that is written to the balluff tag
// There are several string formats required one is for RR another is for the std machines (TM)
// and the other is hypertac code only, this is for older machines.
procedure TToolManager.CreateBalluffString(Ex1: Integer; Tag: TTagRef);
  function ByteToBool(Tmp : Byte) : string;
  var I : Integer;
  begin
    Result := '';
    for I := 0 to 7 do begin
      if (Tmp and ($80 shr I)) <> 0 then
        Result := Result + '1'
      else
        Result := Result + '0';
    end;
  end;

  function FixedWidth(const Tmp : string; Width : Integer) : string;
  const Space = '               ';
  begin
    Result := Tmp + Copy(Space, 1, Width - Length(Tmp)) + ',';
  end;

  function FixedNWidth(const Tmp : string; Width : Integer) : string;
  const Space = '               ';
  begin
    Result := Copy(Space, 1, Width - Length(Tmp)) + Tmp + ',';
  end;

var I : Integer;
    Tmp : string;
    HTToolCode, HTToolSN : Integer;
    Ex1ErrorReset : Boolean;
    atdb: TAllToolDBItem;
begin
  Tmp := '';

  // if this is a request to update Ex1 then set a flag
  // reset the flag if a tool exists
  Ex1ErrorReset := Ex1 = 0;

  ToolList.Lock; //Command used to stop other threads from accessing the tool list.
  try
    for I := 0 to ToolList.Count - 1 do begin
      if ToolList[I].Ex1 = Ex1 then begin
        if Ex1 = 0 then begin
          Ex1ErrorReset := False;
          if not Named.GetAsBoolean(Tags[bttBlockUpdateDatabase]) then begin
            HTToolCode := Named.GetAsInteger(Tags[bttHypertacToolCode]);
            HTToolSN := Named.GetAsInteger(Tags[bttHypertacSerialNumber]);
            if (HTToolCode <> 128) and
              (HTToolCode <> 480) and
              (HTToolCode <> 426) and
              (HTToolSN <> 0) and
              not Named.GetAsBoolean(Tags[bttFake]) then begin
              if not AllToolDB.UpdateTool( ToolList[I], HTToolCode, HTToolSN) then begin
                Named.SetAsInteger(Tags[bttHypertacMismatch], 1);
                //if HypMismatchFID = 0 then
                //  HypMismatchFID := Named.SetFaultA( ObjectName, 'Hypertac does not match Balluff data "ToolCode / Serial number"', FaultRewind, HypertacFaultReset);
              end else begin
                Named.SetAsInteger(Tags[bttHypertacMismatch], 0);
                //if HypMismatchFID <> 0 then begin
                //  Named.FaultResetID(HypMismatchFID);
                //  HypMismatchFID := 0;
                //end;
              end;
            end;
          end;// Blockupdate end

          Named.SetAsDouble(Tags[bttUniqueToolOffsetX], ToolList[I].UniqueToolOffsets[0] / MetricToInch);
          Named.SetAsDouble(Tags[bttUniqueToolOffsetY], ToolList[I].UniqueToolOffsets[1] / MetricToInch);
          Named.SetAsDouble(Tags[bttUniqueToolOffsetZ], ToolList[I].UniqueToolOffsets[2] / MetricToInch);
          Named.SetAsInteger(Tags[bttToolTPM], ToolList[I].TPM1);
          atdb:= AllToolDB.FindTool(ToolList[I].ShuttleID, ToolList[I].ShuttleSN);
          if Assigned(atdb) then begin
            Named.SetAsDouble(Tags[bttCorrelationX], atdb.CorrelationX);
            Named.SetAsDouble(Tags[bttCorrelationY], atdb.CorrelationY);
          end else begin
            Named.SetAsDouble(Tags[bttCorrelationX], 0);
            Named.SetAsDouble(Tags[bttCorrelationY], 0);
          end;
        end;
        //************************************************************************************************************
        // Standard Machine = TM which is initialised from the CEllFShuttle.cfg file tool form datatype variable value
        if DataType = 'TM' then begin
          Tmp := '0101,';
          Tmp := Tmp + FixedWidth(ToolList[I].ShuttleID, 10);
          Tmp := Tmp + FixedNWidth(IntToStr(ToolList[I].ShuttleSN), 4);
          Tmp := Tmp + FixedWidth(ToolList.StorageDate(ToolList[I].RebuildTime), 19 ); //Format('%.3f', [), 19);
          //Tmp := Tmp + FixedWidth(ByteToBool(Byte(ToolList[I].StatusFlags)), 8);
          Tmp := Tmp + FixedWidth(IntToStr(ToolList[I].StatusFlags), 8);
          Tmp := Tmp + FixedNWidth(IntToStr(ToolList[I].BarrelCount), 2);
          Tmp := Tmp + FixedNWidth(IntToStr(ToolList[I].RemainingBarrels), 2);
          if NC.InchModeDefault then begin
            Tmp := Tmp + FixedNWidth(Format('%.3f', [ToolList[I].MaxToolLength]), 6);
            Tmp := Tmp + FixedNWidth(Format('%.3f', [ToolList[I].MinToolLength]), 6);
            Tmp := Tmp + FixedNWidth(Format('%.3f', [ToolList[I].ActToolLength]), 6);
            Tmp := Tmp + FixedNWidth(Format('%.4f', [ToolList[I].UniqueToolOffsets[0]]), 7);
            Tmp := Tmp + FixedNWidth(Format('%.4f', [ToolList[I].UniqueToolOffsets[1]]), 7);
            Tmp := Tmp + FixedNWidth(Format('%.4f', [ToolList[I].UniqueToolOffsets[2]]), 7);
          end else begin
            Tmp := Tmp + FixedNWidth(Format('%.1f', [ToolList[I].MaxToolLength]), 6);
            Tmp := Tmp + FixedNWidth(Format('%.1f', [ToolList[I].MinToolLength]), 6);
            Tmp := Tmp + FixedNWidth(Format('%.1f', [ToolList[I].ActToolLength]), 6);
            Tmp := Tmp + FixedNWidth(Format('%.3f', [ToolList[I].UniqueToolOffsets[0]]), 7);
            Tmp := Tmp + FixedNWidth(Format('%.3f', [ToolList[I].UniqueToolOffsets[1]]), 7);
            Tmp := Tmp + FixedNWidth(Format('%.3f', [ToolList[I].UniqueToolOffsets[2]]), 7);
          end;
          Tmp := Tmp + FixedWidth(ToolList.StorageDate(ToolList[I].Expiry), 19);
        end else begin
          Tmp:= Tmp + ToolList[I].ShuttleID + ',';
          Tmp:= Tmp + IntToStr(ToolList[I].ShuttleSN) + ',';
          Tmp:= Tmp + FloatToStr(ToolList[I].MaxToolLength) + ',';
          Tmp:= Tmp + FloatToStr(ToolList[I].ActToolLength) + ',';
          Tmp:= Tmp + FloatToStr(ToolList[I].RebuildTime) + ',';
          Tmp:= Tmp + FloatToStr(ToolList[I].MinToolLength) + ',';
          Tmp:= Tmp + FloatToStr(ToolList[I].UniqueToolOffsets[0]) + ',';
          Tmp:= Tmp + FloatToStr(ToolList[I].UniqueToolOffsets[1]) + ',';
          Tmp:= Tmp + FloatToStr(ToolList[I].UniqueToolOffsets[2]) + ',';
          Tmp:= Tmp + IntToStr(ToolList[I].TPM1) + ',';
          //Tmp:= Tmp + IntToStr(ToolList[I].TPM2) + ',';
          Tmp:= Tmp + FloatToStr(ToolList[I].Expiry) + ','; // TTS Change
          Tmp:= Tmp + FloatToStr(ToolList[I].UniqueToolOffsets[3]) + ',';
          Tmp:= Tmp + IntToStr(ToolList[I].StatusFlags) + ',';
          Tmp:= Tmp + IntToStr(ToolList[I].BarrelCount) + ',';
          Tmp:= Tmp + IntToStr(ToolList[I].RemainingBarrels) + ',';
          if Ex1 = 0 then begin
            Named.SetAsDouble(Tags[bttUniqueToolOffsetX], ToolList[I].UniqueToolOffsets[0] / MetricToInch);
            Named.SetAsDouble(Tags[bttUniqueToolOffsetY], ToolList[I].UniqueToolOffsets[1] / MetricToInch);
            Named.SetAsDouble(Tags[bttUniqueToolOffsetZ], ToolList[I].UniqueToolOffsets[2] / MetricToInch);
            Named.SetAsDouble(Tags[bttUniqueToolOffsetC], ToolList[I].UniqueToolOffsets[3]);
          end;
        end;
      end;
    end;
    Named.SetAsString(Tag, PChar(Tmp))
  finally
    ToolList.Unlock;
  end;
  if Ex1ErrorReset then begin
    // No tool in head so remove the error
    Named.SetAsInteger(Tags[bttHypertacMismatch], 0);
  end;
  {
  if Ex1ErrorReset and (HypMismatchFID <> 0) then begin
    // Here if we have an error but there is not tool in the head
    Named.FaultResetID(HypMismatchFID);
    HypMismatchFID := 0;
  end;
  }
end;


procedure TToolManager.LocalizeForm;
  procedure TranslateGrid(VLE : TValueListEditor);
  var I: Integer;
  begin
    for I := 1 to VLE.RowCount - 1 do begin
      VLE.Cells[0, I] := Translator.GetString(VLE.Cells[0, I]);
    end;
  end;

var I : Integer;
begin
  Caption:= Translator.GetString(Caption);
  
  for I := 0 to ComponentCount - 1 do begin
    if Components[I] is TLabel then
      TLabel(Components[I]).Caption := Translator.GetString(TLabel(Components[I]).Caption)
    else if Components[I] is TButton then
      TButton(Components[I]).Caption := Translator.GetString(TButton(Components[I]).Caption)
    else if Components[I] is TGroupBox then
      TGroupBox(Components[I]).Caption := Translator.GetString(TGroupBox(Components[I]).Caption)
    else if Components[I] is TSpeedButton then
      TSpeedButton(Components[I]).Caption := Translator.GetString(TSpeedButton(Components[I]).Caption)
    else if Components[I] is TBitBtn then
      TBitBtn(Components[I]).Caption := Translator.GetString(TBitBtn(Components[I]).Caption)
    else if Components[I] is TValueListEditor then
      TranslateGrid(TValueListEditor(Components[I]));
  end;
end;

procedure TToolManager.SpeedButton3MouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  Named.SetAsInteger(Tags[bttIBBReset], 1);
end;

procedure TToolManager.SpeedButton3MouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  Named.SetAsInteger(Tags[bttIBBReset], 0);
end;

procedure TToolManager.BtnWriteMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var  Ex1 : Integer;
begin
  if btnSelect = bdHead then begin
    AddToolFromEditor(0, True, True);
    Named.SetAsInteger(Tags[bttHeadWrite], 1)
  end else begin
    Ex1 := Named.GetAsInteger(Tags[bttToolChangerPosition]);
    if Ex1 > 0 then begin
      AddToolFromEditor(Ex1,True, True);
      Named.SetAsInteger(Tags[bttToolChangerWrite], 1);
    end;
  end;
end;

procedure TToolManager.BtnWriteMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  Named.SetAsInteger(Tags[bttHeadWrite], 0);
  Named.SetAsInteger(Tags[bttToolChangerWrite], 0);
end;


procedure TToolManager.FormShow(Sender: TObject);
begin
  UpdateDisplay;
end;


procedure TToolManager.UpdateExpectedTools;
var TCInf: Integer;
begin
  TCInf:= Named.GetAsInteger(Tags[bttToolChangerPosition]);
  TCInf:= ToolMonitor.Monitor(ToolList, ToolChangerToolCount, TCInf);
  Named.SetAsInteger(Tags[bttExpectedTools], TCInf);
end;


initialization
  TAbstractFormPlugin.AddPlugin('ToolForm', TToolManager);
end.
