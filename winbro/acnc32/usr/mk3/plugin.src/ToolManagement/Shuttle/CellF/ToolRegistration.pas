unit ToolRegistration;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, AbstractNCOperatorDialogue, INCInterface,
  Grids, ValEdit, ACNC_BaseKeyBoards, ExtCtrls, NamedPlugin, CncTypes,
  ToolData, AllToolDB, INamedInterface;

type
  TRegForm = class(TForm)
    Panel1: TPanel;
    BaseKB1: TBaseKB;
    BaseFPad1: TBaseFPad;
    Panel2: TPanel;
    Panel3: TPanel;
    ToolEditor: TValueListEditor;
    BtnOK: TBitBtn;
    BtnCancel: TBitBtn;
    Label1: TLabel;
    LabelToolCode: TLabel;
    StaticText1: TStaticText;
    procedure Button1Click(Sender: TObject);
    procedure BtnCancelClick(Sender: TObject);
    procedure BtnOKClick(Sender: TObject);
  private
    ToolCode, SN: Integer;
    ToolList: TToolList;
    AllToolDB: TAllToolDB;
    InchMode: Boolean;
    MetricToInch: Double;

  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure BuildDefaults;
  end;

  TToolRegistrationDialogue = class(TAbstractNCOperatorDialogue)
  public
    class function Execute(Item : string) : TNCDialogueResult; override;
    class procedure Close; override;
  end;


implementation

{$R *.dfm}

uses ToolForm;

const
  ToolNameField = 1;
  MinimumToolLengthField = 2;
  UniqueOffsetXField = 3;
  UniqueOffsetYField = 4;
  UniqueOffsetZField = 5;
  UniqueOffsetVerifyField = 6;

{ TSimpleDialogueTest }

var Form : TRegForm;

class procedure TToolRegistrationDialogue.Close;
begin
  if Assigned(Form) then
    Form.ModalResult := mrCancel;
end;

// Method that allows loading of a dialogue box.
class function TToolRegistrationDialogue.Execute(Item: string): TNCDialogueResult;
begin
  Form := TRegForm.Create(nil);
  try
    Form.InchMode := Named.GetAsBoolean(ToolForm.ToolManager.InchModeTag);
    Form.BuildDefaults;
    case Form.ShowModal of
      mrOK : begin
        Result := ncdrContinue;
      end;
    else
      Result := ncdrStop;
    end;
  finally
    Form.Free;
    Form := nil;
  end;

end;

procedure TRegForm.Button1Click(Sender: TObject);
begin
  ModalResult :=mrCancel
end;

procedure TRegForm.BtnCancelClick(Sender: TObject);
begin
  Named.SetFaultA('ToolRegistration', 'Tool registration cancelled', FaultRewind, nil)
end;

constructor TRegForm.Create(AOwner: TComponent);
begin
  inherited;
  ToolList := TToolList.Create;
  AllToolDB := TAllToolDB.Create;

end;

destructor TRegForm.Destroy;
begin
  ToolList.Close;
  ToolList := nil;
  AllToolDB.Close;
  AllToolDB := nil;
  inherited;
end;
//
procedure TRegForm.BuildDefaults;
var ATDB : TAllToolDBItem;
begin
  ToolCode := Named.GetAsInteger(ToolManager.Tags[bttHypertacToolCode]);
  SN := Named.GetAsInteger(ToolManager.Tags[bttHypertacSerialNumber]);
  LabelToolCode.Caption := Format('%d / %d', [ToolCode, SN]);

  if InchMode then
    MetricToInch := 25.4
  else
    MetricToInch := 1;


  ATDB := AllToolDB.FindTool(ToolCode, SN);
  if Assigned(ATDB) then begin
    ToolEditor.Cells[1, ToolNameField] := ATDB.ToolName;
    ToolEditor.Cells[1, MinimumToolLengthField] := Format('%.4f', [(ATDB.MinToolLength  / MetricToInch)]);
    ToolEditor.Cells[1, UniqueOffsetXField] := Format('%.4f', [ATDB.UniqueToolX / MetricToInch]);
    ToolEditor.Cells[1, UniqueOffsetYField] := Format('%.4f', [ATDB.UniqueToolY / MetricToInch]);
    ToolEditor.Cells[1, UniqueOffsetZField] := Format('%.4f', [ATDB.UniqueToolZ  / MetricToInch]);
    ToolEditor.Cells[1, UniqueOffsetVerifyField] := Format('%.4f', [(ATDB.UniqueToolX + ATDB.UniqueToolY + ATDB.UniqueToolZ)/MetricToInch]);


  end else begin
    ToolEditor.Cells[1, ToolNameField] := Format('%.d', [ToolCode]);
    ToolEditor.Cells[1, MinimumToolLengthField] := FloatToStr(100 / MetricToInch);
    ToolEditor.Cells[1, UniqueOffsetXField] := '0';
    ToolEditor.Cells[1, UniqueOffsetYField] := '0';
    ToolEditor.Cells[1, UniqueOffsetZField] := '0';
    ToolEditor.Cells[1, UniqueOffsetVerifyField] := '0';
  end;
end;


procedure TRegForm.BtnOKClick(Sender: TObject);
  function ApproximatelyEqual(V1, V2, Approx : Double) : Boolean;
  begin
    Result := Abs(V1 - V2) < Approx;
  end;

var Unix, Uniy, Uniz, Unic, Min : Double;
    I : Integer;
    ATDB : TAllToolDBItem;
begin
  try
    Unix := StrToFloat(ToolEditor.Cells[1, UniqueOffsetXField]) * MetricToInch;
    Uniy := StrToFloat(ToolEditor.Cells[1, UniqueOffsetYField]) * MetricToInch;
    Uniz := StrToFloat(ToolEditor.Cells[1, UniqueOffsetZField]) * MetricToInch;
    Unic := StrToFloat(ToolEditor.Cells[1, UniqueOffsetVerifyField]) * MetricToInch;
    Min := StrToFloat(ToolEditor.Cells[1, MinimumToolLengthField]) * MetricToInch;
  except
    Named.SetFaultA('ToolRegistration', 'Could not convert entry to a number', FaultInterlock, nil);
    Exit;
  end;

  if not ApproximatelyEqual(Unix + Uniy + Uniz, Unic, 0.0001 * MetricToInch) then begin
    Named.SetFaultA('ToolRegistration', 'Offset verification failed', FaultInterlock, nil);
    Exit;
  end;

  ToolList.BeginUpdate;
  try
    for I := 0 to ToolList.Count - 1 do begin
      if ToolList[I].Ex1 = 0 then begin
        ToolList[I].ShuttleID := ToolEditor.Cells[1, ToolNameField];
        ToolList[I].ShuttleSN := SN;
        ToolList[I].MinToolLength := Min;
        ToolList[I].UniqueToolOffsets[0] := Unix;
        ToolList[I].UniqueToolOffsets[1] := Uniy;
        ToolList[I].UniqueToolOffsets[2] := Uniz;
      end;
    end;

    AllToolDB.Lock;
    try
      ATDB := AllToolDB.FindTool(ToolCode, SN);
      if Assigned(ATDB) then begin
        ATDB.ToolName := ToolEditor.Cells[1, ToolNameField];
      end;
    finally
      AllToolDB.Unlock;
    end;
  finally
    ToolList.EndUpdate;
  end;
  ModalResult := mrOK;
end;


initialization
  TToolRegistrationDialogue.AddPlugin('ToolRegistration', TToolRegistrationDialogue);
end.
