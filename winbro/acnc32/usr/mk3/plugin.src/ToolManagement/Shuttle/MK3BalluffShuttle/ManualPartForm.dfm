object ManualPart: TManualPart
  Left = 200
  Top = 273
  Width = 709
  Height = 379
  Caption = 'Part load form'
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -16
  Font.Name = 'Verdana'
  Font.Style = [fsBold]
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 18
  object Label1: TLabel
    Left = 72
    Top = 17
    Width = 38
    Height = 18
    Caption = 'Part'
  end
  object kb1: TBaseKB
    Left = 0
    Top = 51
    Width = 701
    Height = 260
    Align = alBottom
    BevelOuter = bvNone
    Caption = 'kb1'
    TabOrder = 0
    KeyboardMode = kbmAlphaNumeric
    HighlightColor = clBlack
    Options = []
    SecondaryFont.Charset = DEFAULT_CHARSET
    SecondaryFont.Color = clWindowText
    SecondaryFont.Height = -11
    SecondaryFont.Name = 'MS Sans Serif'
    SecondaryFont.Style = []
    RepeatDelay = 500
    ExternalMode = False
    FocusByHandle = False
    FocusHandle = 0
  end
  object Edit1: TEdit
    Left = 224
    Top = 9
    Width = 257
    Height = 26
    TabOrder = 1
  end
  object Panel1: TPanel
    Left = 0
    Top = 311
    Width = 701
    Height = 41
    Align = alBottom
    TabOrder = 2
    object BitBtn1: TBitBtn
      Left = 48
      Top = 8
      Width = 90
      Height = 25
      TabOrder = 0
      Kind = bkOK
    end
    object BitBtn2: TBitBtn
      Left = 144
      Top = 8
      Width = 90
      Height = 25
      TabOrder = 1
      Kind = bkCancel
    end
  end
end
