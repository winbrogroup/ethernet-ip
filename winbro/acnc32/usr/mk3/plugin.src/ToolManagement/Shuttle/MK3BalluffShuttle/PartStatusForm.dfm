object PartStatus: TPartStatus
  Left = 231
  Top = 237
  Width = 501
  Height = 480
  Caption = 'Part Status'
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -16
  Font.Name = 'Verdana'
  Font.Style = [fsBold]
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 18
  object Bevel1: TBevel
    Left = 30
    Top = 48
    Width = 440
    Height = 6
  end
  object Bevel2: TBevel
    Left = 30
    Top = 88
    Width = 440
    Height = 6
  end
  object Bevel3: TBevel
    Left = 30
    Top = 128
    Width = 440
    Height = 6
  end
  object OPS0_15: TPaintBox
    Left = 32
    Top = 144
    Width = 441
    Height = 49
    OnPaint = OPS0_15Paint
  end
  object OPS16_31: TPaintBox
    Left = 32
    Top = 200
    Width = 441
    Height = 49
    OnPaint = OPS16_31Paint
  end
  object Panel1: TPanel
    Left = 0
    Top = 401
    Width = 493
    Height = 45
    Align = alBottom
    BevelOuter = bvLowered
    TabOrder = 0
    object BitBtn1: TBitBtn
      Left = 168
      Top = 8
      Width = 110
      Height = 30
      TabOrder = 0
      Kind = bkClose
    end
  end
  object StaticText1: TStaticText
    Left = 32
    Top = 24
    Width = 91
    Height = 22
    Caption = 'Part Type'
    TabOrder = 1
  end
  object StaticText2: TStaticText
    Left = 32
    Top = 64
    Width = 80
    Height = 22
    Caption = 'Part SFC'
    TabOrder = 2
  end
  object StaticText3: TStaticText
    Left = 32
    Top = 104
    Width = 92
    Height = 22
    Caption = 'Shuttle ID'
    TabOrder = 3
  end
  object PartTypeLabel: TStaticText
    Left = 136
    Top = 24
    Width = 321
    Height = 22
    AutoSize = False
    BorderStyle = sbsSunken
    TabOrder = 4
  end
  object PartSFCLabel: TStaticText
    Left = 136
    Top = 64
    Width = 321
    Height = 22
    AutoSize = False
    BorderStyle = sbsSunken
    TabOrder = 5
  end
  object ShuttleIDLabel: TStaticText
    Left = 136
    Top = 104
    Width = 321
    Height = 22
    AutoSize = False
    BorderStyle = sbsSunken
    TabOrder = 6
  end
  object Odds: TCheckListBox
    Left = 24
    Top = 296
    Width = 177
    Height = 94
    Enabled = False
    Flat = False
    ItemHeight = 18
    Items.Strings = (
      'Pre CMM'
      'Post CMM'
      'Production'
      'Rework')
    TabOrder = 7
  end
end
