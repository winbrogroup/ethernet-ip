object ToolManager: TToolManager
  Left = 67
  Top = 93
  Width = 885
  Height = 634
  Caption = '$TOOL_STATUS'
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Verdana'
  Font.Style = [fsBold]
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 16
  object Panel2: TPanel
    Left = 0
    Top = 369
    Width = 877
    Height = 231
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    object BaseKB1: TBaseKB
      Left = 0
      Top = 0
      Width = 649
      Height = 231
      Align = alLeft
      BevelOuter = bvNone
      Caption = 'BaseKB1'
      TabOrder = 0
      KeyboardMode = kbmAlphaNumeric
      HighlightColor = clBlack
      Options = []
      SecondaryFont.Charset = DEFAULT_CHARSET
      SecondaryFont.Color = clWindowText
      SecondaryFont.Height = -11
      SecondaryFont.Name = 'MS Sans Serif'
      SecondaryFont.Style = []
      RepeatDelay = 500
      ExternalMode = False
      FocusByHandle = False
      FocusHandle = 0
    end
    object BaseFPad1: TBaseFPad
      Left = 640
      Top = 0
      Width = 240
      Height = 233
      ExternalMode = False
      ChangeCursorKeys = True
      EnableCursorKeys = False
    end
  end
  object Panel6: TPanel
    Left = 0
    Top = 0
    Width = 877
    Height = 369
    Align = alClient
    Caption = 'Panel6'
    TabOrder = 1
    object Panel3: TPanel
      Left = 1
      Top = 1
      Width = 875
      Height = 326
      Align = alClient
      BevelOuter = bvLowered
      TabOrder = 0
      object SpeedButton1: TSpeedButton
        Left = 56
        Top = 16
        Width = 113
        Height = 49
        GroupIndex = 1
        Caption = 'Head'
        OnClick = btnToolChangerClick
      end
      object ToolEditor: TValueListEditor
        Left = 1
        Top = 1
        Width = 399
        Height = 324
        Align = alClient
        Strings.Strings = (
          '$TOOLTYPE='
          '$TOOL_SERIAL='
          '$REBUILD_TIME='
          '$HIGH_PRESSURE_TOOL='
          '$BARREL_COUNT='
          '$BARRELS_REMAINING='
          '$MAX_TOOL_LENGTH='
          '$MINIMUM_TOOL_LENGTH='
          '$ACTUAL_LENGTH='
          '$X_TOOL_OFFSET='
          '$Y_TOOL_OFFSET='
          '$Z_TOOL_OFFSET='
          '$C_TOOL_OFFSET='
          '$TPM1='
          '$TPM2='
          '$EXPIRY='
          '$OFFSET_VERITY=')
        TabOrder = 0
        TitleCaptions.Strings = (
          'Data'
          'Value')
        ColWidths = (
          150
          226)
      end
      object Panel7: TPanel
        Left = 400
        Top = 1
        Width = 474
        Height = 324
        Align = alRight
        Caption = 'Panel7'
        TabOrder = 1
        object Panel5: TPanel
          Left = 1
          Top = 1
          Width = 255
          Height = 322
          Align = alClient
          TabOrder = 0
          object LogMemo: TMemo
            Left = 1
            Top = 51
            Width = 253
            Height = 270
            Align = alClient
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Courier New'
            Font.Style = []
            ParentFont = False
            ScrollBars = ssVertical
            TabOrder = 0
          end
          object InfoPanel: TPanel
            Left = 1
            Top = 1
            Width = 253
            Height = 50
            Align = alTop
            BevelOuter = bvLowered
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlue
            Font.Height = -13
            Font.Name = 'Verdana'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 1
          end
        end
        object Panel4: TPanel
          Left = 256
          Top = 1
          Width = 217
          Height = 322
          Align = alRight
          BevelOuter = bvLowered
          TabOrder = 1
          object btnToolChanger: TSpeedButton
            Left = 10
            Top = 80
            Width = 100
            Height = 60
            AllowAllUp = True
            GroupIndex = 1
            Caption = '$TOOL_CHANGER'
            OnClick = btnToolChangerClick
          end
          object btnHead: TSpeedButton
            Left = 10
            Top = 144
            Width = 100
            Height = 60
            AllowAllUp = True
            GroupIndex = 1
            Caption = '$HEAD'
            OnClick = btnToolChangerClick
          end
          object SpeedButton3: TSpeedButton
            Left = 136
            Top = 48
            Width = 73
            Height = 60
            Caption = '$RESET'
            OnMouseDown = SpeedButton3MouseDown
            OnMouseUp = SpeedButton3MouseUp
          end
          object BitBtn1: TBitBtn
            Left = 10
            Top = 248
            Width = 100
            Height = 49
            Caption = '$DONE'
            TabOrder = 0
            Kind = bkOK
          end
          object BtnWrite: TBitBtn
            Left = 10
            Top = 3
            Width = 100
            Height = 60
            Caption = '$WRITE'
            TabOrder = 1
            OnMouseDown = BtnWriteMouseDown
            OnMouseUp = BtnWriteMouseUp
            Glyph.Data = {
              DE010000424DDE01000000000000760000002800000024000000120000000100
              0400000000006801000000000000000000001000000000000000000000000000
              80000080000000808000800000008000800080800000C0C0C000808080000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333444444
              33333333333F8888883F33330000324334222222443333388F3833333388F333
              000032244222222222433338F8833FFFFF338F3300003222222AAAAA22243338
              F333F88888F338F30000322222A33333A2224338F33F8333338F338F00003222
              223333333A224338F33833333338F38F00003222222333333A444338FFFF8F33
              3338888300003AAAAAAA33333333333888888833333333330000333333333333
              333333333333333333FFFFFF000033333333333344444433FFFF333333888888
              00003A444333333A22222438888F333338F3333800003A2243333333A2222438
              F38F333333833338000033A224333334422224338338FFFFF8833338000033A2
              22444442222224338F3388888333FF380000333A2222222222AA243338FF3333
              33FF88F800003333AA222222AA33A3333388FFFFFF8833830000333333AAAAAA
              3333333333338888883333330000333333333333333333333333333333333333
              0000}
            NumGlyphs = 2
          end
        end
      end
    end
    object Panel1: TPanel
      Left = 1
      Top = 327
      Width = 875
      Height = 41
      Align = alBottom
      BevelOuter = bvLowered
      TabOrder = 1
    end
  end
end
