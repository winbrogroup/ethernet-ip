library MK3BalluffShuttle;



{%File 'BalluffShuttle.dll.html'}

uses
  SysUtils,
  Classes,
  C6x1 in 'C6x1.pas',
  OpTable in '..\..\lib\OpTable.pas',
  ToolOKDlg in 'ToolOKDlg.pas' {IsToolOK},
  ManualPartForm in 'ManualPartForm.pas' {ManualPart},
  AllToolDB in '..\..\lib\AllToolDB.pas',
  OffsetData in '..\..\lib\OffsetData.pas',
  NumericOperatorDialogue in 'NumericOperatorDialogue.pas' {NumericEntry},
  PartStatusForm in 'PartStatusForm.pas' {PartStatus},
  RRPartData in 'RRPartData.pas',
  C6001Euchner in 'C6001Euchner.pas',
  ToolForm in 'ToolForm.pas' {ToolManager},
  ToolMonitor in 'ToolMonitor.pas',
  ToolRegistration in 'ToolRegistration.pas' {RegForm};

{$R *.res}

begin
end.
