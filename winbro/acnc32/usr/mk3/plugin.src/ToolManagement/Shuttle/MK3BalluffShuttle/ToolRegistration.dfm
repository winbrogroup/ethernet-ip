object RegForm: TRegForm
  Left = 133
  Top = 283
  Width = 884
  Height = 410
  Caption = 'Tool Registration'
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Verdana'
  Font.Style = [fsBold]
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 16
  object Panel1: TPanel
    Left = 0
    Top = 148
    Width = 876
    Height = 235
    Align = alBottom
    BevelOuter = bvLowered
    TabOrder = 0
    object BaseKB1: TBaseKB
      Left = 1
      Top = 1
      Width = 649
      Height = 233
      Align = alLeft
      BevelOuter = bvNone
      Caption = 'BaseKB1'
      TabOrder = 0
      KeyboardMode = kbmAlphaNumeric
      HighlightColor = clBlack
      Options = []
      SecondaryFont.Charset = ANSI_CHARSET
      SecondaryFont.Color = clWindowText
      SecondaryFont.Height = -13
      SecondaryFont.Name = 'Verdana'
      SecondaryFont.Style = [fsBold]
      RepeatDelay = 500
      ExternalMode = False
      FocusByHandle = False
      FocusHandle = 0
    end
    object BaseFPad1: TBaseFPad
      Left = 634
      Top = 0
      Width = 240
      Height = 233
      ExternalMode = False
      ChangeCursorKeys = True
      EnableCursorKeys = True
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 876
    Height = 148
    Align = alClient
    TabOrder = 1
    object Panel3: TPanel
      Left = 369
      Top = 1
      Width = 506
      Height = 146
      Align = alClient
      TabOrder = 0
      object Label1: TLabel
        Left = 72
        Top = 72
        Width = 201
        Height = 16
        Caption = 'Tool code / serial number ='
      end
      object LabelToolCode: TLabel
        Left = 280
        Top = 72
        Width = 44
        Height = 16
        Caption = '12 / 2'
      end
      object BtnOK: TBitBtn
        Left = 82
        Top = 96
        Width = 100
        Height = 41
        Caption = 'OK'
        Default = True
        TabOrder = 0
        OnClick = BtnOKClick
        Glyph.Data = {
          DE010000424DDE01000000000000760000002800000024000000120000000100
          0400000000006801000000000000000000001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
          3333333333333333333333330000333333333333333333333333F33333333333
          00003333344333333333333333388F3333333333000033334224333333333333
          338338F3333333330000333422224333333333333833338F3333333300003342
          222224333333333383333338F3333333000034222A22224333333338F338F333
          8F33333300003222A3A2224333333338F3838F338F33333300003A2A333A2224
          33333338F83338F338F33333000033A33333A222433333338333338F338F3333
          0000333333333A222433333333333338F338F33300003333333333A222433333
          333333338F338F33000033333333333A222433333333333338F338F300003333
          33333333A222433333333333338F338F00003333333333333A22433333333333
          3338F38F000033333333333333A223333333333333338F830000333333333333
          333A333333333333333338330000333333333333333333333333333333333333
          0000}
        NumGlyphs = 2
      end
      object BtnCancel: TBitBtn
        Left = 314
        Top = 96
        Width = 100
        Height = 41
        TabOrder = 1
        OnClick = BtnCancelClick
        Kind = bkCancel
      end
      object StaticText1: TStaticText
        Left = 1
        Top = 1
        Width = 504
        Height = 64
        Align = alTop
        Alignment = taCenter
        AutoSize = False
        BevelKind = bkSoft
        Caption = 
          'Verfy or update the tool code information, press OK when finishe' +
          'd, if in doubt press cancel. Warning the machine will continue i' +
          'n cycle when OK is pressed'
        Color = clCaptionText
        Font.Charset = ANSI_CHARSET
        Font.Color = clRed
        Font.Height = -16
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 2
      end
    end
    object ToolEditor: TValueListEditor
      Left = 1
      Top = 1
      Width = 368
      Height = 146
      Align = alLeft
      Strings.Strings = (
        'Tool Type='
        'Minimum tool length='
        'X Tool offset='
        'Y Tool offset='
        'Z Tool offset='
        '**Offset Verify**=')
      TabOrder = 1
      TitleCaptions.Strings = (
        'Data'
        'Value')
      ColWidths = (
        150
        212)
    end
  end
end
