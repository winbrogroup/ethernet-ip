unit ToolOKDlg;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, ACNC_TouchSoftkey, Buttons,
  ToolData, NamedPlugin;

type
  TIsToolOK = class(TForm)
    HeaderLabel: TLabel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    TotalBarrelsLabel: TLabel;
    FullBarrelsLabel: TLabel;
    CurrentBarrelLengthLabel: TLabel;
    TBEdit: TEdit;
    FBEdit: TEdit;
    CBEdit: TEdit;
    TTLabel: TLabel;
    TTEdit: TEdit;
  private
    { Private declarations }
  public
    class function Execute(const ToolData : TToolData) : Boolean;
  end;

implementation

{$R *.DFM}

var
  IsToolOK: TIsToolOK;


{ TIsToolOK }

class function TIsToolOK.Execute(const ToolData : TToolData ): Boolean;
var
Barrels   : Integer;
Remaining : Double;
Translator: TTranslator;
begin

  Translator := TTranslator.Create;
  Result := False;
  Barrels := ToolData.RemainingBarrels;
  Remaining := ToolData.ActToolLength - ToolData.MinToolLength;
  if not Assigned(IsToolOK) then begin
    IsToolOK := TIsToolOK.Create(nil);

    IsToolOK.TTLabel.Caption:= Translator.GetString('$TOOL_TYPE');
    IsToolOK.TotalBarrelsLabel.Caption:= Translator.GetString('$TOTAL_BARRELS');
    IsToolOK.FullBarrelsLabel.Caption:= Translator.GetString('$FULL_BARRELS');

    try
      if  ToolData.BarrelCount >1 then
        begin
        IsToolOK.TBEdit.Visible := True;
        IsToolOK.TotalBarrelsLabel.Visible := True;
        IsToolOK.FBEdit.Visible := True;
        IsToolOK.FullBarrelsLabel.Visible := True;
        IsToolOK.CurrentBarrelLengthLabel.Caption := Translator.GetString('$CURRENT_BARREL_LENGTH');

        end
      else
        begin
        IsToolOK.TBEdit.Visible := False;
        IsToolOK.TotalBarrelsLabel.Visible := False;
        IsToolOK.FBEdit.Visible := False;
        IsToolOK.FullBarrelsLabel.Visible := False;
        IsToolOK.CurrentBarrelLengthLabel.Caption := Translator.GetString('$CURRENT_TOOL_LENGTH');
        end;
      IsToolOK.TTEdit.Text := ToolData.ShuttleID;
      IsToolOK.TBEdit.Text := IntToStr(ToolData.BarrelCount);
      IsToolOK.FBEdit.Text := IntToStr(Barrels);
      IsToolOK.CBEdit.Text := FloatToStr(Remaining);
      IsToolOK.HeaderLabel.Caption := Translator.GetString('$Q_TOOL_LIFE_REPLENISH');
      Result := IsToolOK.ShowModal = mrOK;
    finally
      IsToolOK.Free;
      IsToolOK := nil;
      Translator.Free;
    end;
  end
end;


end.
