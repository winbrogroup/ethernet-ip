object InitialTool: TInitialTool
  Left = 0
  Top = 0
  Width = 648
  Height = 287
  Caption = 'Tool'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -21
  Font.Name = 'Tahoma'
  Font.Style = [fsBold]
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 25
  object ToolCodeLbl: TLabel
    Left = 16
    Top = 40
    Width = 233
    Height = 25
    AutoSize = False
    Caption = 'Tool Code'
  end
  object ToolSerialLbl: TLabel
    Left = 16
    Top = 88
    Width = 225
    Height = 25
    AutoSize = False
    Caption = 'Serial Number'
  end
  object ToolSerialEdit: TEdit
    Left = 256
    Top = 88
    Width = 121
    Height = 33
    TabOrder = 1
  end
  object ToolCodeEdit: TComboBox
    Left = 256
    Top = 32
    Width = 350
    Height = 33
    ItemHeight = 25
    TabOrder = 0
  end
  object OkBtn: TButton
    Left = 168
    Top = 168
    Width = 113
    Height = 49
    Caption = 'OK'
    Default = True
    TabOrder = 2
    OnClick = OkBtnClick
  end
  object CancelBtn: TButton
    Left = 312
    Top = 168
    Width = 105
    Height = 49
    Cancel = True
    Caption = 'Cancel'
    TabOrder = 3
    OnClick = CancelBtnClick
  end
end
