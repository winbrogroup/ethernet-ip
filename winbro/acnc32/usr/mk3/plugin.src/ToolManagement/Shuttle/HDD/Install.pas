unit Install;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, IFormInterface, INamedInterface, NamedPlugin, PluginInterface,
  AbstractFormPlugin, IToolLife, CNCTypes, ACNC_TouchSoftkey, ExtCtrls, StdCtrls,
  OCTUtils, IniFiles, ToolData, ManualPartForm;

type
  THDDToolTag = (
    bttToolChangerRemove,
    bttHeadRemove,
    bttToolChangerPosition,
    bttHypertacToolCode,
    bttHypertacSerialNumber,
    bttUniqueToolOffsetX,
    bttUniqueToolOffsetY,
    bttUniqueToolOffsetZ,
    bttToolStation0,
    bttToolStation1,
    bttToolStation2,
    bttToolStation3,
    bttToolStation4,
    bttToolStation5,
    bttToolStation6,
    bttToolStation7,
    bttToolStation8,
    bttFake,
    bttHypertacMismatch,
    bttLoadToolAtToolDoor,
    bttLoadToolAtHead,
    bttToolDialogueActive,
    bttManualLoad,
    bttManualLoadAck,
    bttPartId,
    bttPartOperationIndex

  );

  TToolForm = class(TInstallForm)
    Label1: TLabel;
    Panel1: TPanel;
    Button1: TButton;
    procedure TouchSoftkey1Click(Sender: TObject);
  private
    TagNames: array[THDDToolTag] of string;
    Tags : array [THDDToolTag] of TTagRef;
    ToolDefTags: array[0..8] of TTagRef;
    ToolList: TToolList;
    ToolHistory: TToolList;
    ManualLoadActive: Boolean;
    procedure TagChange(ATag: TTagRef);
    procedure WriteConfig;
    procedure RemoveToolAtEx1(Ex1: Integer);
    procedure CreatePersistence;
    procedure UpdateToolTags;
    procedure AddToolAtEx1(Ex1: Integer; ToolCode: string; ToolSerial: Integer);
  public
    procedure Execute; override;
    procedure CloseForm; override;
    procedure Install; override;
    procedure Shutdown; override;
    procedure FinalInstall; override;
    procedure DisplaySweep; override;
    procedure SetNumericEntry(AVal: Integer);
  end;

var
  ToolForm: TToolForm;

implementation

uses InitialToolForm, ToolOKDlg, NewToolForm;

{$R *.dfm}

const
  TagDefinitions : array [THDDToolTag] of TTagDefinitions = (
     ( Ini : 'ToolChangerRemove'; Tag : 'ToolChangerRemove'; Owned : False; TagType : 'TMethodTag' ),
     ( Ini : 'HeadToolRemove'; Tag : 'HeadToolRemove'; Owned : False; TagType : 'TMethodTag' ),
     ( Ini : 'ToolChangerPosition'; Tag : 'ToolChangerPosition'; Owned : False; TagType : 'TIntegerTag' ),
     ( Ini : 'HypertacToolCode'; Tag : 'HypertacToolCode'; Owned : False; TagType : 'TIntegerTag' ),
     ( Ini : 'HypertacSerialNumber'; Tag : 'HypertacSerialNumber'; Owned : False; TagType : 'TIntegerTag' ),
     ( Ini : 'UniqueToolOffsetX'; Tag: 'UniqueToolOffsetX'; Owned : True; TagType : 'TDoubleTag' ),
     ( Ini : 'UniqueToolOffsetY'; Tag: 'UniqueToolOffsetY'; Owned : True; TagType : 'TDoubleTag' ),
     ( Ini : 'UniqueToolOffsetZ'; Tag: 'UniqueToolOffsetZ'; Owned : True; TagType : 'TDoubleTag' ),
     ( Ini : 'ToolDefinition0'; Tag: 'ToolDefinition0'; Owned : True; TagType : 'TStringTag' ),
     ( Ini : 'ToolDefinition1'; Tag: 'ToolDefinition1'; Owned : True; TagType : 'TStringTag' ),
     ( Ini : 'ToolDefinition2'; Tag: 'ToolDefinition2'; Owned : True; TagType : 'TStringTag' ),
     ( Ini : 'ToolDefinition3'; Tag: 'ToolDefinition3'; Owned : True; TagType : 'TStringTag' ),
     ( Ini : 'ToolDefinition4'; Tag: 'ToolDefinition4'; Owned : True; TagType : 'TStringTag' ),
     ( Ini : 'ToolDefinition5'; Tag: 'ToolDefinition5'; Owned : True; TagType : 'TStringTag' ),
     ( Ini : 'ToolDefinition6'; Tag: 'ToolDefinition6'; Owned : True; TagType : 'TStringTag' ),
     ( Ini : 'ToolDefinition7'; Tag: 'ToolDefinition7'; Owned : True; TagType : 'TStringTag' ),
     ( Ini : 'ToolDefinition8'; Tag: 'ToolDefinition8'; Owned : True; TagType : 'TStringTag' ),
     ( Ini : 'Fake'; Tag: 'Acnc32Fake'; Owned : False; TagType : 'TIntegerTag' ),
     ( Ini : 'HypertacMismatch'; Tag: 'HypertacMismatch'; Owned : True; TagType : 'TIntegerTag' ),
     ( Ini : 'LoadToolAtToolDoor'; Tag: 'LoadToolAtToolDoor'; Owned : False; TagType : 'TIntegerTag' ),
     ( Ini : 'LoadToolAtHead'; Tag: 'LoadToolAtHead'; Owned : False; TagType : 'TIntegerTag' ),
     ( Ini : 'ToolDialogueActive'; Tag: 'ToolDialogueActive'; Owned : True; TagType : 'TIntegerTag' ),
     ( Ini : 'PartManualLoad'; Tag: 'PartManualLoad'; Owned: False; TagType: 'TIntegerTag' ),
     ( Ini : 'PartManualLoadAck'; Tag: 'PartManualLoadAck'; Owned: True; TagType: 'TIntegerTag' ),
     ( Ini : 'PartName'; Tag: 'PartName'; Owned: True; TagType: 'TStringTag' ),
     ( Ini : 'PartOperationIndex'; Tag: 'PartOperationIndex'; Owned: True; TagType: 'TIntegerTag' )
  );

procedure ToolTagChange(ATag: TTagRef); stdcall;
begin
  ToolForm.TagChange(ATag);
end;

{ TToolForm }

procedure TToolForm.CloseForm;
begin
  inherited;

end;

procedure TToolForm.Shutdown;
begin
  inherited;
  WriteConfig;
  ToolList.Close;
  ToolHistory.Close;
  ToolList := nil;
  ToolHistory := nil;
end;

procedure TToolForm.DisplaySweep;
begin
  inherited;
  if Named.GetAsBoolean(Tags[bttManualLoad]) then begin
    if not ManualLoadActive then begin
      ManualLoadActive := True;
      Named.SetAsString(Tags[bttPartID], PChar(TManualPart.Execute));
      Named.SetAsBoolean(Tags[bttManualLoadAck], True);
    end;
  end else if Named.GetAsBoolean(Tags[bttManualLoadAck]) then begin
    Named.SetAsBoolean(Tags[bttManualLoadAck], False);
    ManualLoadActive := False;
  end;
end;

procedure TToolForm.FinalInstall;
var I :THDDToolTag;
    Tmp: Integer;
begin
  inherited;
  for I := Low(I) to High(I) do begin
    if not TagDefinitions[I].Owned then
      Tags[I] := Named.AquireTag(PChar(TagNames[I]), PChar(TagDefinitions[I].TagType), ToolTagChange);
  end;

  Tmp:= 0;
  for I := bttToolStation0 to bttToolStation8 do begin
    ToolDefTags[Tmp] := Tags[I];
    Inc(Tmp);
  end;
end;

procedure TToolForm.Install;
var Ini : TIniFile;
    I :THDDToolTag;
begin
  inherited;
  ToolForm:= Self;
  ToolList := TToolList.Create;
  ToolHistory := TToolList.Create('ToolHistory');

  Ini := TIniFile.Create(dllProfilePath + 'HDDShuttle.cfg');
  try
    for I := Low(I) to High(I) do begin
      TagNames[I] := Ini.ReadString('ToolFormTags', TagDefinitions[I].Ini, TagDefinitions[I].Tag);
      Tags[I] := Named.AddTag(PChar(TagNames[I]), PChar(TagDefinitions[I].TagType));
    end;
  finally
    Ini.Free;
  end;
end;

procedure TToolForm.Execute;
begin
  inherited;
  if not Visible then
    ShowModal;
end;

procedure TToolForm.TouchSoftkey1Click(Sender: TObject);
begin
  if Visible then
    Close;
end;

{* Any changes to the tags that can be found in the HDDshuttle.cfg file will operate the equivalent
   tag in this part of the code and action the code associated with the tag that has changed *}
procedure TToolForm.TagChange(ATag: TTagRef);
var ToolSerial: Integer;
    ToolCode: string;
begin
  if not Named.GetAsBoolean(Tags[bttToolDialogueActive]) then begin
  if ATag = Tags[bttLoadToolAtToolDoor] then begin
    if Named.GetAsBoolean(ATag) then begin
      Named.SetAsBoolean(Tags[bttToolDialogueActive], True);
      try
        ToolCode:= '';
        ToolSerial:= 1;
        if InitialToolForm.TInitialTool.Execute(ToolCode, ToolSerial) then
          AddToolAtEx1(Named.GetAsInteger(Tags[bttToolChangerPosition]), ToolCode, ToolSerial)
      finally
        Named.SetAsBoolean(Tags[bttToolDialogueActive], False);
      end;
    end;
  end
  else if ATag = Tags[bttLoadToolAtHead] then begin
    if not Named.GetAsBoolean(ATag) then begin
      Named.SetAsBoolean(Tags[bttToolDialogueActive], True);
      try
        ToolCode:= '';
        ToolSerial:= 1;
        if InitialToolForm.TInitialTool.Execute(ToolCode, ToolSerial) then
          AddToolAtEx1(0, ToolCode, ToolSerial);
      finally
        Named.SetAsBoolean(Tags[bttToolDialogueActive], False);
      end;
    end;
  end;
  end;
  if ATag = Tags[bttToolChangerRemove] then begin
    if Named.GetAsInteger(Tags[bttToolChangerPosition]) > 0 then begin
      if not Named.GetAsBoolean(ATag) then begin
        RemoveToolAtEx1(Named.GetAsInteger(Tags[bttToolChangerPosition]));
      end;
    end;
  end
  else if ATag = Tags[bttHeadRemove] then begin
    if not Named.GetAsBoolean(ATag) then begin
      CreatePersistence;
      RemoveToolAtEx1(0);
    end;
  end
  else if ATag = Tags[bttToolChangerPosition] then begin
    UpdateToolTags;
    CreatePersistence;
  end;
end;

procedure TToolForm.WriteConfig;
var Ini : TIniFile;
    I :THDDToolTag;
begin
  Ini := TIniFile.Create(dllProfilePath + 'HDDShuttle-example.cfg');
  try

    for I := Low(I) to High(I) do begin
      Ini.WriteString('ToolFormTags', TagDefinitions[I].Ini, TagNames[I]);
    end;
  finally
    Ini.Free;
  end;
end;

procedure TToolForm.RemoveToolAtEx1(Ex1 : Integer);
var Tmp : WideString;
    TmpSN : Integer;
begin
  if ToolLife.GetEx1ToolType(Ex1, Tmp) = trOK then begin
    ToolLife.GetEx1ToolSN(Ex1, TmpSN);
    ToolLife.RemoveTool(Tmp, TmpSN);
  end;
end;

{ Copies data from live tools in machine into a backup database, updates
  any active status }
procedure TToolForm.CreatePersistence;
var I: Integer;
  Tool, TH: TToolData;
begin
  for I:= 0 to ToolList.Count - 1 do begin
    Tool:= ToolList.Tool[I];

    if Tool.Ex1 = 0 then begin
      Named.SetAsDouble(Tags[bttUniqueToolOffsetX], Tool.UniqueToolOffsets[0]);
      Named.SetAsDouble(Tags[bttUniqueToolOffsetY], Tool.UniqueToolOffsets[1]);
      Named.SetAsDouble(Tags[bttUniqueToolOffsetZ], Tool.UniqueToolOffsets[2]);
    end;

    if Tool.ShuttleID <> '0' then begin
      TH:= ToolHistory.Find(Tool.ShuttleID, Tool.ShuttleSN);
      if TH = nil then begin
        TH:= ToolHistory.AddTool;
      end;
      ToolHistory.CopyTool(TH, Tool)
    end;
  end;
end;

 procedure TToolForm.UpdateToolTags;
var I: Integer;
    Tmp: array [0..8] of string;
    Tool: TToolData;
begin
  for I:= 0 to 8 do begin
    Tmp[I]:= ',,';
  end;

  for I:= 0 to ToolList.Count - 1 do begin
    Tool:= ToolList.Tool[I];
    Tmp[Tool.Ex1 mod 9] := Tool.ShuttleID + ',' +
                           IntToStr(Tool.ShuttleSN) + ',' +
                           FloatToStr(Tool.TotalRemaining);
  end;

  for I:= 0 to 8 do begin
    Named.SetAsString(ToolDefTags[I], PChar(Tmp[I]));
  end;
end;

procedure TToolForm.AddToolAtEx1(Ex1: Integer; ToolCode: string; ToolSerial: Integer);
var I: Integer;
    TH: TToolData;
    Tool: TToolData;
begin
  if NewToolForm.NewTool <> nil then
    Exit;

  for I:= 0 to ToolHistory.Count - 1 do begin
    TH := ToolHistory.Tool[I];
    if (CompareText( TH.ShuttleID, ToolCode) = 0) and (TH.ShuttleSN = ToolSerial) then begin
      // Found tool
      ToolLife.AddTool(ToolCode, ToolSerial, TH.MaxToolLength, TH.ActToolLength, Ex1, 0);
      Tool:= ToolList.ToolAtEx1(Ex1);
      if Assigned(Tool) then begin
        ToolList.CopyTool(Tool, TH);
        Tool.Ex1:= Ex1; // reapply the x1 value
        NewToolForm.TNewTool.Execute(Tool);
        CreatePersistence;
        Exit;
      end
      else begin
        // We just added the tool where is it?
        Named.EventLog('TToolForm.AddToolAtEx1 - Added in toollife not found on ToolList?');
      end;
    end;
  end;

  // Tool not found
  ToolLife.AddTool(ToolCode, ToolSerial, 450, 450, Ex1, 0);
  Tool:= ToolList.ToolAtEx1(Ex1);
  NewToolForm.TNewTool.Execute(Tool);
  CreatePersistence;
end;


procedure TToolForm.SetNumericEntry(AVal: Integer);
begin
  Named.SetAsInteger(Tags[bttPartOperationIndex], AVal);
end;

initialization
  TAbstractFormPlugin.AddPlugin('ToolForm', TToolForm);
end.

