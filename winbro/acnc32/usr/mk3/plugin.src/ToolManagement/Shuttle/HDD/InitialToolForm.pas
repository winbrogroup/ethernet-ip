unit InitialToolForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, ACNC_TouchSoftkey, StdCtrls, NamedPlugin, ToolData;

type
  TInitialTool = class(TForm)
    ToolCodeLbl: TLabel;
    ToolSerialLbl: TLabel;
    ToolSerialEdit: TEdit;
    ToolCodeEdit: TComboBox;
    OkBtn: TButton;
    CancelBtn: TButton;
    procedure OkBtnClick(Sender: TObject);
    procedure CancelBtnClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    ToolCode: Integer;
    ToolSerial: Integer;
  public
    class function Execute(var ToolCode: string; var ToolSerial: Integer): Boolean;
  end;

var
  InitialTool: TInitialTool;

implementation

{$R *.dfm}

class function TInitialTool.Execute(var ToolCode: string; var ToolSerial: Integer): Boolean;
begin
  Result:= False;
  if InitialTool = nil then begin
    InitialTool:= TInitialTool.Create(nil);
    try
      Result:= InitialTool.ShowModal = mrOk;
      ToolCode:= InitialTool.ToolCodeEdit.Text;
      ToolSerial:= StrToIntDef(InitialTool.ToolSerialEdit.Text, 0);
    finally
      InitialTool.Free;
      InitialTool:= nil;
    end;
  end;
end;

procedure TInitialTool.FormCreate(Sender: TObject);
var
  Translator : TTranslator;
  ToolHistory: TToolList;
  I: Integer;
begin
  Translator:= TTranslator.Create;
  Caption:= Translator.GetString('$INITIAL_TOOL_FORM_CAPTION');
  ToolCodeLbl.Caption:= Translator.GetString('$TOOLTYPE');
  ToolSerialLbl.Caption:= Translator.GetString('$SERIALNUMBER');
  ToolSerialEdit.Text:= '1';
  OkBtn.Caption:= Translator.GetString('$OK');
  CancelBtn.Caption:= Translator.GetString('$CANCEL');
  Translator.Free;

  ToolHistory := TToolList.Create('ToolHistory');

  for I:= 0 to ToolHistory.Count - 1 do begin
    ToolCodeEdit.Items.Add(ToolHistory.Tool[I].ShuttleID);
  end;

  ToolHistory.Close;
  ToolHistory:= nil;

end;

procedure TInitialTool.CancelBtnClick(Sender: TObject);
begin
  Self.ModalResult:= mrCancel;
end;

procedure TInitialTool.OkBtnClick(Sender: TObject);
begin
  ModalResult:= mrOk;
end;

end.
