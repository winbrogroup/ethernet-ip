library HDDShuttle;

{ Important note about DLL memory management: ShareMem must be the
  first unit in your library's USES clause AND your project's (select
  Project-View Source) USES clause if your DLL exports any procedures or
  functions that pass strings as parameters or function results. This
  applies to all strings passed to and from your DLL--even those that
  are nested in records and classes. ShareMem is the interface unit to
  the BORLNDMM.DLL shared memory manager, which must be deployed along
  with your DLL. To avoid using BORLNDMM.DLL, pass string information
  using PChar or ShortString parameters. }

uses
  SysUtils,
  Classes,
  InitialToolForm in 'InitialToolForm.pas' {InitialTool},
  NewToolForm in 'NewToolForm.pas' {NewTool},
  ReplenishToolForm in 'ReplenishToolForm.pas' {Form3},
  Install in 'Install.pas' {ToolForm},
  ToolOKDlg in 'ToolOKDlg.pas' {IsToolOK},
  ManualPartForm in 'ManualPartForm.pas' {ManualPart},
  NumericOperatorDialogue in 'NumericOperatorDialogue.pas' {NumericEntry};

{$R *.res}

begin
end.
