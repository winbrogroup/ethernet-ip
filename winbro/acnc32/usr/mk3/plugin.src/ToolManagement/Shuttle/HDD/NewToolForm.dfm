object NewTool: TNewTool
  Left = 0
  Top = 0
  Width = 571
  Height = 569
  BiDiMode = bdLeftToRight
  Caption = 'BarrelRemainingLbl'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -21
  Font.Name = 'Tahoma'
  Font.Style = [fsBold]
  OldCreateOrder = False
  ParentBiDiMode = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 25
  object ToolCodeLbl: TLabel
    Left = 32
    Top = 16
    Width = 240
    Height = 25
    AutoSize = False
    Caption = 'Label1'
  end
  object MinLengthLbl: TLabel
    Left = 32
    Top = 96
    Width = 240
    Height = 25
    AutoSize = False
    Caption = 'Label1'
  end
  object MaxLengthLbl: TLabel
    Left = 32
    Top = 136
    Width = 240
    Height = 25
    AutoSize = False
    Caption = 'Label1'
  end
  object ToolSerialLbl: TLabel
    Left = 32
    Top = 56
    Width = 240
    Height = 25
    AutoSize = False
    Caption = 'Label1'
  end
  object BarrelCountLbl: TLabel
    Left = 32
    Top = 176
    Width = 240
    Height = 25
    AutoSize = False
    Caption = 'Label1'
  end
  object UniqueXLbl: TLabel
    Left = 32
    Top = 216
    Width = 240
    Height = 25
    AutoSize = False
    Caption = 'Label1'
  end
  object UniqueYLbl: TLabel
    Left = 32
    Top = 256
    Width = 240
    Height = 25
    AutoSize = False
    Caption = 'Label1'
  end
  object ActualRemainingLbl: TLabel
    Left = 32
    Top = 304
    Width = 240
    Height = 25
    AutoSize = False
    Caption = 'label 1'
  end
  object BarrelsRemainingLbl: TLabel
    Left = 32
    Top = 344
    Width = 240
    Height = 25
    AutoSize = False
    Caption = 'Label1'
  end
  object ActualRemainingEdit: TLabel
    Left = 300
    Top = 304
    Width = 193
    Height = 25
    AutoSize = False
    Caption = 'label1'
  end
  object BarrelsRemainingEdit: TLabel
    Left = 300
    Top = 344
    Width = 193
    Height = 25
    AutoSize = False
    Caption = 'Label1'
  end
  object MinLengthEdit: TEdit
    Left = 300
    Top = 96
    Width = 250
    Height = 33
    TabOrder = 0
  end
  object MaxLengthEdit: TEdit
    Left = 300
    Top = 136
    Width = 250
    Height = 33
    TabOrder = 1
  end
  object BarrelCountEdit: TEdit
    Left = 300
    Top = 176
    Width = 250
    Height = 33
    TabOrder = 2
  end
  object UniqueXEdit: TEdit
    Left = 300
    Top = 216
    Width = 250
    Height = 33
    TabOrder = 3
  end
  object UniqueYEdit: TEdit
    Left = 300
    Top = 256
    Width = 250
    Height = 33
    TabOrder = 4
  end
  object ToolCodeEdit: TEdit
    Left = 300
    Top = 16
    Width = 250
    Height = 33
    TabOrder = 5
  end
  object ToolSerialEdit: TEdit
    Left = 300
    Top = 56
    Width = 250
    Height = 33
    TabOrder = 6
  end
  object OkBtn: TButton
    Left = 96
    Top = 448
    Width = 129
    Height = 57
    Caption = 'Reset'
    ModalResult = 1
    TabOrder = 7
    OnClick = OkBtn1Click
  end
  object CancelBtn: TButton
    Left = 256
    Top = 448
    Width = 121
    Height = 57
    Caption = 'Accept'
    Default = True
    TabOrder = 8
    OnClick = CancelBtn1Click
  end
  object QualifyCheckBox: TCheckBox
    Left = 112
    Top = 384
    Width = 241
    Height = 41
    Alignment = taLeftJustify
    Caption = 'QualifyCheckBox'
    TabOrder = 9
  end
end
