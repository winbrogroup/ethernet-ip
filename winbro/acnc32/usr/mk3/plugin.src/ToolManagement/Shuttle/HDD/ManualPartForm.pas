unit ManualPartForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls, ACNC_BaseKeyBoards;

type
  TManualPart = class(TForm)
    kb1: TBaseKB;
    Edit1: TEdit;
    Label1: TLabel;
    Panel1: TPanel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
  private
  public
    class function Execute : string; 
  end;

var
  ManualPart: TManualPart;

implementation

{$R *.dfm}


{ TManualPart }

class function TManualPart.Execute: string;
begin
  Result := '';

  ManualPart := TManualPart.Create(nil);
  try
    if ManualPart.ShowModal = mrOK then
      Result:= ManualPart.Edit1.Text;
  finally
    ManualPart.Free;
    ManualPart := nil;
  end;
end;

end.
 