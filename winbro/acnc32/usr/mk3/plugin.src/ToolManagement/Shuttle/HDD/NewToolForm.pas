unit NewToolForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, ACNC_TouchSoftkey, StdCtrls, NamedPlugin, ToolData;

type
  TNewTool = class(TForm)
    ToolCodeLbl: TLabel;
    MinLengthLbl: TLabel;
    MaxLengthLbl: TLabel;
    ToolSerialLbl: TLabel;
    MinLengthEdit: TEdit;
    MaxLengthEdit: TEdit;
    BarrelCountLbl: TLabel;
    BarrelCountEdit: TEdit;
    UniqueXLbl: TLabel;
    UniqueYLbl: TLabel;
    UniqueXEdit: TEdit;
    UniqueYEdit: TEdit;
    ToolCodeEdit: TEdit;
    ToolSerialEdit: TEdit;
    ActualRemainingLbl: TLabel;
    BarrelsRemainingLbl: TLabel;
    ActualRemainingEdit: TLabel;
    BarrelsRemainingEdit: TLabel;
    OkBtn: TButton;
    CancelBtn: TButton;
    QualifyCheckBox: TCheckBox;
    procedure CancelBtn1Click(Sender: TObject);
    procedure OkBtn1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    class procedure Execute(Tool: TToolData);
  end;

var
  NewTool: TNewTool;

implementation

{$R *.dfm}

class procedure TNewTool.Execute(Tool: TToolData);
var MR: Integer;
begin
  if NewTool = nil then begin
    NewTool:= TNewTool.Create(nil);
    try
      NewTool.ToolCodeEdit.Text:= Tool.ShuttleID;
      NewTool.ToolSerialEdit.Text:= IntToStr(Tool.ShuttleSN);
      NewTool.BarrelCountEdit.Text:= IntToStr(Tool.BarrelCount);
      NewTool.MinLengthEdit.Text:= FloatToStr(Tool.MinToolLength);
      NewTool.MaxLengthEdit.Text:= FloatToStr(Tool.MaxToolLength);
      NewTool.UniqueXEdit.Text:= FloatToStr(Tool.UniqueToolOffsets[0]);
      NewTool.UniqueYEdit.Text:= FloatToStr(Tool.UniqueToolOffsets[1]);
      NewTool.ActualRemainingEdit.Caption:= FloatToStr(Tool.ActToolLength);
      NewTool.BarrelsRemainingEdit.Caption:= IntToStr(Tool.RemainingBarrels);
      NewTool.QualifyCheckBox.Checked:= True;

      MR:= NewTool.ShowModal;

      Tool.ShuttleID:= NewTool.ToolCodeEdit.Text;
      Tool.ShuttleSN:= StrToIntDef(NewTool.ToolSerialEdit.Text, 0);
      Tool.BarrelCount:= StrToIntDef(NewTool.BarrelCountEdit.Text, 1);
      Tool.MinToolLength:= StrToFloatDef(NewTool.MinLengthEdit.Text, 150);
      Tool.MaxToolLength:= StrToFloatDef(NewTool.MaxLengthEdit.Text, 450);
      Tool.UniqueToolOffsets[0]:= StrToFloatDef(NewTool.UniqueXEdit.Text, 0);
      Tool.UniqueToolOffsets[1]:= StrToFloatDef(NewTool.UniqueYEdit.Text, 0);
      Tool.StatusFlags:= Ord(NewTool.QualifyCheckBox.Checked);
      Tool.Expiry:= Now + 20000;
      Tool.RebuildTime:= Tool.Expiry;
      //Tool.RemainingBarrels:= Tool.BarrelCount;
      //if Tool.BarrelCount > 1 then
        //Tool.ActToolLength:= 0
      //else
        //Tool.ActToolLength:= Tool.MaxToolLength;

      Tool.UniqueToolOffsets[2]:= 0;
      Tool.UniqueToolOffsets[3]:= 0;

      if MR = mrOK then begin
        if Tool.BarrelCount <= 1 then begin
          Tool.RemainingBarrels:= 0;
          Tool.ActToolLength:= Tool.MaxToolLength;
        end else begin
          Tool.RemainingBarrels:= Tool.BarrelCount;
          Tool.ActToolLength:= 0;
        end;
      end

    finally
      NewTool.Free;
      NewTool:= nil;
    end;
  end;
end;

procedure TNewTool.FormCreate(Sender: TObject);
var
  Translator : TTranslator;
begin
  Translator:= TTranslator.Create;
  Caption:= Translator.GetString('$NEW_TOOL_FORM_CAPTION');
  ToolCodeLbl.Caption:= Translator.GetString('$TOOLTYPE');
  ToolSerialLbl.Caption:= Translator.GetString('$SERIALNUMBER');

  MinLengthLbl.Caption:= Translator.GetString('$MIN_LENGTH');
  MaxLengthLbl.Caption:= Translator.GetString('$MAX_LENGTH');
  BarrelCountLbl.Caption:= Translator.GetString('$BARREL_COUNT');
  UniqueXLbl.Caption:= Translator.GetString('$UNIQUEX');
  UniqueYLbl.Caption:= Translator.GetString('$UNIQUEY');

  BarrelsRemainingLbl.Caption:= Translator.GetString('$BARRELS_REMAINING');
  ActualRemainingLbl.Caption:= Translator.GetString('$ACTUAL_REMAINING');

  OkBtn.Caption:= Translator.GetString('$RESET');
  CancelBtn.Caption:= Translator.GetString('$ACCEPT');
  QualifyCheckBox.Caption:= Translator.GetString('$QUALIFY_TOOL');
  Translator.Free;
end;

procedure TNewTool.OkBtn1Click(Sender: TObject);
begin
  ModalResult:= mrOK;
end;

procedure TNewTool.CancelBtn1Click(Sender: TObject);
begin
  ModalResult:= mrCancel;
end;

end.
