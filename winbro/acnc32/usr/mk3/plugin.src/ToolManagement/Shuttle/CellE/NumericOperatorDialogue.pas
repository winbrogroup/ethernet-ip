unit NumericOperatorDialogue;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, AbstractNCOperatorDialogue, INCInterface,  NamedPlugin,
  CncTypes, Buttons;


type
  TNumericEntry = class(TForm)
    Edit1: TEdit;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    lblCaption: TLabel;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  TSimpleNumericDialogue = class(TAbstractNCOperatorDialogue)
    class function Execute(Item : string) : TNCDialogueResult; override;
    class procedure Close; override;
  end;

var
  NumericEntry: TNumericEntry;

implementation

uses PartStatusForm;

{$R *.dfm}

{ TSimpleNumericDialogue }

class procedure TSimpleNumericDialogue.Close;
begin
  if Assigned(NumericEntry) then
    NumericEntry.ModalResult := mrCancel;
end;

class function TSimpleNumericDialogue.Execute(
  Item: string): TNCDialogueResult;
begin
  NumericEntry:= TNumericEntry.Create(nil);
  try
    NumericEntry.lblCaption.Caption := Item;
    case NumericEntry.ShowModal of
      mrOK : begin
        Result := ncdrContinue;
        Named.SetAsInteger(PartStatus.Tags[ptOpIndex], StrToIntDef(NumericEntry.Edit1.Text, 0));
      end;
    else
      Result := ncdrStop;
      Named.SetAsInteger(PartStatus.Tags[ptOpIndex], 0);
    end;
  finally
    NumericEntry.Free;
    NumericEntry:= nil;
  end;
end;

initialization
  TAbstractNCOperatorDialogue.AddPlugin('NumericEntry', TSimpleNumericDialogue);
end.
