object ToolForm1: TToolForm1
  Left = 0
  Top = 0
  Width = 826
  Height = 605
  Caption = 'ToolForm1'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = [fsBold]
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Panel2: TPanel
    Left = 0
    Top = 313
    Width = 818
    Height = 258
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object BaseKB1: TBaseKB
      Left = 0
      Top = 0
      Width = 818
      Height = 258
      Align = alClient
      BevelOuter = bvNone
      Caption = 'BaseKB1'
      TabOrder = 0
      KeyboardMode = kbmAlphaNumeric
      HighlightColor = clBlack
      Options = []
      SecondaryFont.Charset = DEFAULT_CHARSET
      SecondaryFont.Color = clWindowText
      SecondaryFont.Height = -11
      SecondaryFont.Name = 'MS Sans Serif'
      SecondaryFont.Style = []
      RepeatDelay = 500
      ExternalMode = False
      FocusByHandle = False
      FocusHandle = 0
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 0
    Width = 818
    Height = 313
    Align = alTop
    BevelOuter = bvLowered
    TabOrder = 1
    object SpeedButton1: TSpeedButton
      Left = 56
      Top = 16
      Width = 113
      Height = 49
      GroupIndex = 1
      Caption = 'Head'
    end
    object SpeedButton2: TSpeedButton
      Left = 224
      Top = 16
      Width = 113
      Height = 49
      GroupIndex = 1
      Caption = 'Tool Changer'
    end
    object ToolEditor: TValueListEditor
      Left = 1
      Top = 1
      Width = 368
      Height = 311
      Align = alLeft
      Strings.Strings = (
        'Tool Type='
        'Tool Serial Number='
        'Actual Length='
        'Replenish Length='
        'Minimum Length='
        'Rebuild Date='
        'X Tool offset='
        'Y Tool offset='
        'Z Tool offset='
        'C Tool offset='
        'TPM1='
        'TPM2='
        '**Offset Verify**=')
      TabOrder = 0
      TitleCaptions.Strings = (
        'Data'
        'Value')
      ColWidths = (
        150
        212)
    end
    object Panel1: TPanel
      Left = 369
      Top = 1
      Width = 448
      Height = 311
      Align = alClient
      TabOrder = 1
      object Panel4: TPanel
        Left = 312
        Top = 1
        Width = 135
        Height = 309
        Align = alRight
        BevelOuter = bvLowered
        TabOrder = 0
        object SpeedButton3: TSpeedButton
          Left = 32
          Top = 216
          Width = 73
          Height = 60
          Caption = 'Reset'
          OnMouseDown = SpeedButton3MouseDown
          OnMouseUp = SpeedButton3MouseUp
        end
        object BitBtn1: TBitBtn
          Left = 18
          Top = 96
          Width = 100
          Height = 49
          Caption = 'Done'
          TabOrder = 0
          Kind = bkOK
        end
        object BtnWrite: TBitBtn
          Left = 18
          Top = 11
          Width = 100
          Height = 60
          Caption = '&Write Tag'
          TabOrder = 1
          OnMouseDown = BtnWriteMouseDown
          OnMouseUp = BtnWriteMouseUp
          Glyph.Data = {
            DE010000424DDE01000000000000760000002800000024000000120000000100
            0400000000006801000000000000000000001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333444444
            33333333333F8888883F33330000324334222222443333388F3833333388F333
            000032244222222222433338F8833FFFFF338F3300003222222AAAAA22243338
            F333F88888F338F30000322222A33333A2224338F33F8333338F338F00003222
            223333333A224338F33833333338F38F00003222222333333A444338FFFF8F33
            3338888300003AAAAAAA33333333333888888833333333330000333333333333
            333333333333333333FFFFFF000033333333333344444433FFFF333333888888
            00003A444333333A22222438888F333338F3333800003A2243333333A2222438
            F38F333333833338000033A224333334422224338338FFFFF8833338000033A2
            22444442222224338F3388888333FF380000333A2222222222AA243338FF3333
            33FF88F800003333AA222222AA33A3333388FFFFFF8833830000333333AAAAAA
            3333333333338888883333330000333333333333333333333333333333333333
            0000}
          NumGlyphs = 2
        end
      end
      object Panel5: TPanel
        Left = 1
        Top = 1
        Width = 311
        Height = 309
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 1
        object InfoPanel: TPanel
          Left = 0
          Top = 0
          Width = 311
          Height = 50
          Align = alTop
          BevelOuter = bvLowered
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlue
          Font.Height = -13
          Font.Name = 'Verdana'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 0
        end
        object LogMemo: TMemo
          Left = 0
          Top = 50
          Width = 311
          Height = 259
          Align = alClient
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Courier New'
          Font.Style = []
          ParentFont = False
          TabOrder = 1
        end
      end
    end
  end
end
