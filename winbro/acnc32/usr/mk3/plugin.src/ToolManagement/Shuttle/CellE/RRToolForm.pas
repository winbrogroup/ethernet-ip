unit RRToolForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, Grids, ValEdit, ACNC_BaseKeyBoards, ExtCtrls,
  OCTUtils, INamedInterface, IniFiles, CNCTypes, C6x1, ToolData, ToolDB,
  AllToolDB, NamedPlugin, IToolLife, AbstractFormPlugin, ToolOKDlg, OCTTypes,
  C6x1Client;

type
  TBalluffToolTag = (
    bttToolChangerWrite,
    bttToolChangerRemove,
    bttHeadWrite,
    bttHeadRemove,
    bttToolChangerPosition,
    bttHypertacToolCode,
    bttHypertacSerialNumber,
    bttHeadWriteString,
    bttToolChangerWriteString,
    bttUniqueToolOffsetX,
    bttUniqueToolOffsetY,
    bttUniqueToolOffsetZ,
    bttUniqueToolOffsetC,
    bttIBBReset,
    bttOperatorToolValidate,
    bttBlockUpdateDatabase,
    bttInhibitToolDialogue,
    bttToolStation0,
    bttToolStation1,
    bttToolStation2,
    bttToolStation3,
    bttToolStation4,
    bttToolStation5,
    bttToolStation6,
    bttToolStation7,
    bttToolStation8
  );

  TBalluffDevice = (
    bdHead,
    bdToolChanger,
    bdNone
  );

  TToolForm1 = class(TC6x1Client)
    Panel2: TPanel;
    Panel3: TPanel;
    SpeedButton1: TSpeedButton;
    SpeedButton2: TSpeedButton;
    ToolEditor: TValueListEditor;
    Panel1: TPanel;
    Panel4: TPanel;
    SpeedButton3: TSpeedButton;
    BitBtn1: TBitBtn;
    BtnWrite: TBitBtn;
    Panel5: TPanel;
    InfoPanel: TPanel;
    LogMemo: TMemo;
    BaseKB1: TBaseKB;
    procedure BtnWriteMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure BtnWriteMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure SpeedButton3MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure SpeedButton3MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
  private
    Device : array [TBalluffDevice] of string;
    Head : array [TBalluffDevice] of Boolean;
    StateBuffer : array [TBalluffDevice] of string;
    StateIBB : array [TBalluffDevice] of TIBBState;
    StateChange : array [TBalluffDevice] of Boolean;
    TagNames : array [TBalluffToolTag] of string;
    ToolList : TToolList;
    ToolDB : TToolDB;
    ToolDefTags: array [0..8] of TTagRef;
    AllToolDB : TAllToolDB;
    FID : FHandle;
    procedure AddToolFromEditor(Ex1 : Integer;EditorEntry:Boolean; OffsetVerify: Boolean=False);
    procedure AddTool(Ex1 : Integer; Buffer : string);
    procedure UpdateWriteStrings;
    procedure ToolListChange(Sender : TObject);
    procedure TagChanged(ATag : TTagRef);
    procedure ReadConfig;
    procedure WriteConfig;
    procedure DoStateChange(D : TBalluffDevice);
    procedure Logging(D : TBalluffDevice; const Msg : string);
    procedure UpdateDisplay;
    procedure CreateDefinitionStrings;
    procedure CreateBalluffString(Ex1 : Integer; Tag : TTagRef);
    function CheckToolAddition(T : TToolData):Boolean;
  protected
    procedure C6x1Notify(Service: TC6x1; Msg : TIBBState); override;
  public
    Tags : array [TBalluffToolTag] of TTagRef;
    procedure Install; override;
    procedure FinalInstall; override;
    procedure Execute; override;
    procedure Shutdown; override;
    procedure DisplaySweep; override;
  end;

var
  ToolForm1: TToolForm1;

implementation

var
  HypMismatchFID : FHandle;

const
  TagDefinitions : array [TBalluffToolTag] of TTagDefinitions = (
     ( Ini : 'ToolChangerWrite'; Tag : 'ToolChangerWrite'; Owned : True; TagType : 'TMethodTag' ),
     ( Ini : 'ToolChangerRemove'; Tag : 'ToolChangerRemove'; Owned : False; TagType : 'TMethodTag' ),
     ( Ini : 'HeadToolWrite'; Tag : 'HeadToolWrite'; Owned : True; TagType : 'TMethodTag' ),
     ( Ini : 'HeadToolRemove'; Tag : 'HeadToolRemove'; Owned : False; TagType : 'TMethodTag' ),
     ( Ini : 'ToolChangerPosition'; Tag : 'ToolChangerPosition'; Owned : False; TagType : 'TIntegerTag' ),
     ( Ini : 'HypertacToolCode'; Tag : 'HypertacToolCode'; Owned : False; TagType : 'TIntegerTag' ),
     ( Ini : 'HypertacSerialNumber'; Tag : 'HypertacSerialNumber'; Owned : False; TagType : 'TIntegerTag' ),
     ( Ini : 'HeadWriteString'; Tag: 'HeadWriteString'; Owned : True; TagType : 'TStringTag' ),
     ( Ini : 'ToolChangerWriteString'; Tag: 'ToolChangerWriteString'; Owned : True; TagType : 'TStringTag' ),
     ( Ini : 'UniqueToolOffsetX'; Tag: 'UniqueToolOffsetX'; Owned : True; TagType : 'TDoubleTag' ),
     ( Ini : 'UniqueToolOffsetY'; Tag: 'UniqueToolOffsetY'; Owned : True; TagType : 'TDoubleTag' ),
     ( Ini : 'UniqueToolOffsetZ'; Tag: 'UniqueToolOffsetZ'; Owned : True; TagType : 'TDoubleTag' ),
     ( Ini : 'UniqueToolOffsetC'; Tag: 'UniqueToolOffsetC'; Owned : True; TagType : 'TDoubleTag' ),
     ( Ini : 'IBBResetPB'; Tag: 'IBBResetPB'; Owned : True; TagType : 'TMethodTag' ),
     //( Ini : 'I_TCLOCKED'; Tag: 'I_TCLOCKED'; Owned : False; TagType : 'TMethodTag' ),
     //( Ini : 'ToolAddFail'; Tag: 'ToolAddFail'; Owned : True; TagType : 'TMethodTag' ),
     //( Ini : 'I_ToolPresentPosn1'; Tag: 'I_ToolPresentPosn1'; Owned : False; TagType : 'TMethodTag' ),
     ( Ini : 'OperatorToolValidate'; Tag: 'MTB2'; Owned : False; TagType : 'TIntegerTag' ),
     ( Ini : 'BlockUpdateDB'; Tag: 'ToolChangerBlockUpdateDB'; Owned : False; TagType : 'TMethodTag' ),
     ( Ini : 'InhibitToolDialogue'; Tag: 'InhibitToolDialogue'; Owned : False; TagType : 'TIntegerTag' ),
     ( Ini : 'ToolDefinition0'; Tag: 'ToolDefinition0'; Owned : True; TagType : 'TStringTag' ),
     ( Ini : 'ToolDefinition1'; Tag: 'ToolDefinition1'; Owned : True; TagType : 'TStringTag' ),
     ( Ini : 'ToolDefinition2'; Tag: 'ToolDefinition2'; Owned : True; TagType : 'TStringTag' ),
     ( Ini : 'ToolDefinition3'; Tag: 'ToolDefinition3'; Owned : True; TagType : 'TStringTag' ),
     ( Ini : 'ToolDefinition4'; Tag: 'ToolDefinition4'; Owned : True; TagType : 'TStringTag' ),
     ( Ini : 'ToolDefinition5'; Tag: 'ToolDefinition5'; Owned : True; TagType : 'TStringTag' ),
     ( Ini : 'ToolDefinition6'; Tag: 'ToolDefinition6'; Owned : True; TagType : 'TStringTag' ),
     ( Ini : 'ToolDefinition7'; Tag: 'ToolDefinition7'; Owned : True; TagType : 'TStringTag' ),
     ( Ini : 'ToolDefinition8'; Tag: 'ToolDefinition8'; Owned : True; TagType : 'TStringTag' )
  );

  ShuttleIDField = 1;
  ShuttleSNField = 2;
  MaxToolLengthField = 4;
  ActualLengthField = 3;
  RebuildTimeField = 6;
  MinToolLengthField = 5;
  UniqueToolXField = 7;
  UniqueToolYField = 8;
  UniqueToolZField = 9;
  TPM1Field = 11;
  TPM2Field = 12;
  UniqueToolCField = 10;
  OffsetVerifyField = 13;

  ObjectName = 'BalluffShuttle.RRToolForm'; // Just used for parameter in  SetFaultA.


{$R *.dfm}

{ TForm2 }

procedure ToolTagChange(ATag : TTagRef); stdcall;
begin
  ToolForm1.TagChanged(ATag);
end;

function HypertacFaultReset(FID : FHandle) : Boolean; stdcall;
begin
  Result := HypMismatchFID = 0;
end;


procedure TToolForm1.Shutdown;
begin
  WriteConfig;
  ToolList.Close;
  ToolList := nil;

  ToolDB.Close;
  ToolDB := nil;
end;

procedure TToolForm1.FinalInstall;
var I : TBalluffToolTag;
    Tmp: Integer;
begin
  ToolForm1 := Self;
  ToolList := TToolList.Create;
  ToolDB := TToolDB.Create;
  AllToolDB := TAllToolDB.Create;
  ToolList.OnChange := ToolListChange;

  for I := Low(TBalluffToolTag) to High(TBalluffToolTag) do begin
    if not TagDefinitions[I].Owned then
      Tags[I] := Named.AquireTag(PChar(TagNames[I]), PChar(TagDefinitions[I].TagType), ToolTagChange);
  end;
  //Named.SetAsInteger(Tags[bttUnknownToolAtDoor],0);

  Tmp:= 0;
  for I := bttToolStation0 to bttToolStation8 do begin
    ToolDefTags[Tmp] := Tags[I];
    Inc(Tmp);
  end;
end;

procedure TToolForm1.Install;
var I : TBalluffToolTag;
begin
  //LastMCode := 0;
  //Translator := TTranslator.Create;
  //LocalizeForm;

  ToolForm1 := Self;

  ReadConfig;
  for I := Low(TBalluffToolTag) to High(TBalluffToolTag) do begin
    if TagDefinitions[I].Owned then
      Tags[I] := Named.AddTag(PChar(TagNames[I]), PChar(TagDefinitions[I].TagType));
  end;
end;

procedure TToolForm1.Execute;
begin
  if not Visible then begin
    InfoPanel.Caption := 'Modify data for writing Balluff';
    UpdateDisplay;
    ShowModal;
  end;
end;

procedure TToolForm1.ReadConfig;
var Ini : TIniFile;
    I :TBalluffToolTag;
begin
  Ini := TIniFile.Create(dllProfilePath + 'BalluffShuttle.cfg');
  try
    Device[bdHead] := Ini.ReadString('ToolForm', 'HeadBalluffDevice', 'Balluff_1');
    Head[bdHead] := Ini.ReadBool('ToolForm', 'HeadBalluffHead', False);
    Device[bdToolChanger] := Ini.ReadString('ToolForm', 'ToolChangerBalluffDevice', 'Balluff_1');
    Head[bdToolChanger] := Ini.ReadBool('ToolForm', 'ToolChangerBalluffHead', True);

    Device[bdNone] := '*';

    for I := Low(TBalluffToolTag) to High(TBalluffToolTag) do begin
      TagNames[I] := Ini.ReadString('ToolFormTags', TagDefinitions[I].Ini, TagDefinitions[I].Tag);
    end;
  finally
    Ini.Free;
  end;
end;

procedure TToolForm1.WriteConfig;
var Ini : TIniFile;
    I :TBalluffToolTag;
begin
  Ini := TIniFile.Create(dllProfilePath + 'BalluffShuttle.cfg');
  try
    Ini.WriteString('ToolForm', 'HeadBalluffDevice', Device[bdHead]);
    Ini.WriteBool('ToolForm', 'HeadBalluffHead', Head[bdHead]);
    Ini.WriteString('ToolForm', 'ToolChangerBalluffDevice', Device[bdToolChanger]);
    Ini.WriteBool('ToolForm', 'ToolChangerBalluffHead', Head[bdToolChanger]);

    for I := Low(TBalluffToolTag) to High(TBalluffToolTag) do begin
      Ini.WriteString('ToolFormTags', TagDefinitions[I].Ini, TagNames[I]);
    end;
  finally
    Ini.Free;
  end;
end;

procedure TToolForm1.DoStateChange(D: TBalluffDevice);
var Ex1 : Integer;
begin
  case StateIBB[D] of
    IBB_IDLE : begin
      Logging(D, 'Idle');
    end;

    IBB_READ : begin
      Logging(D, 'Read');
    end;

    IBB_WRITE : begin
      Named.SetAsInteger(Tags[bttHeadWrite], 0);
      Named.SetAsInteger(Tags[bttToolChangerWrite], 0);
      Logging(D, 'Write');
    end;

    IBB_RESET : begin
      Named.SetAsInteger(Tags[bttHeadWrite], 0);
      Named.SetAsInteger(Tags[bttToolChangerWrite], 0);
      Named.SetAsInteger(Tags[bttIBBReset], 0);
      Logging(D, 'Reset');
    end;

    IBB_CONFIGURE : begin
      Named.SetAsInteger(Tags[bttHeadWrite], 0);
      Named.SetAsInteger(Tags[bttToolChangerWrite], 0);
      Logging(D, 'Configure');
    end;

    IBB_READ_COMPLETE : begin
      Logging(D, 'Read Complete');
      if D = bdHead then
        Ex1 := 0
      else
        Ex1 := Named.GetAsInteger(Tags[bttToolChangerPosition]);

      if D = bdHead then
        Logging(D, '--HEAD = ' + IntToStr(Ex1))
      else
        Logging(D, '--TC   = ' + IntToStr(Ex1));

      Logging(D, 'String = "' + StateBuffer[D] + '"');
      AddTool(Ex1, StateBuffer[D]);
    end;

    IBB_WRITE_COMPLETE : begin
            Logging(D, 'Write Complete');
    end;

    IBB_READ_ERROR : begin
      Logging(D, 'Read Error');
    end;

    IBB_WRITE_ERROR : begin
      Logging(D, 'Write Error');
    end;

    IBB_CONFIGURE_COMPLETE : begin
      Logging(D, 'Configure Complete');
    end;

    IBB_RESET_COMPLETE : begin
      Logging(D, 'Reset Complete');
    end;

    IBB_HEAD1 : begin
      Logging(D, 'Head1');
    end;

    IBB_HEAD2 : begin
      Logging(D, 'Head2');
    end;

    IBB_VERIFY: begin
      Logging(D, 'Verify write');
    end;

    IBB_VERIFY_COMPLETE: begin
      Logging(D, 'Verify complete');
    end;

    IBB_VERIFY_FAIL: begin
      Logging(D, 'Verify Fail');
    end;

  end;
  if Visible then
    UpdateDisplay;
  StateChange[D] := False;
end;

const
  LogPrefix : array[TBalluffDevice] of string = (
    'Head: ', 'TC:   ', ''
  );

procedure TToolForm1.Logging(D: TBalluffDevice; const Msg: string);
var I : Integer;
begin
  if LogMemo.Lines.Count > 100 then begin // ???? Buffer size in ini.
    LogMemo.Lines.BeginUpdate;
    try
      for I := 0 to 20 do
        LogMemo.Lines.Delete(0);
    finally
      LogMemo.Lines.EndUpdate;
    end;
  end;
  LogMemo.Lines.Add(LogPrefix[D] + Msg);
end;

procedure TToolForm1.ToolListChange(Sender: TObject);
begin
  UpdateWriteStrings;
end;

procedure TToolForm1.C6x1Notify(Service: TC6x1; Msg: TIBBState);
var I : TBalluffDevice;
    StateDevice : TBalluffDevice;
begin
  StateDevice := bdNone;
  for I := Low(TBalluffDevice) to High(TBalluffDevice) do begin
    if (CompareText(Service.DeviceName, Device[I]) = 0) and
       (Head[I] = Service.Head) then begin
      StateDevice := I;
      Break;
    end;
  end;

  if StateDevice <> bdNone then begin
    if Msg = IBB_READ_COMPLETE then
      StateBuffer[StateDevice] := Service.ReadBuffer;
    StateIBB[StateDevice] := Msg;
    StateChange[StateDevice] := True;
  end;
end;

procedure TToolForm1.CreateDefinitionStrings;
var I: Integer;
    Tmp: array [0..8] of string;
    Tool: TToolData;
begin
  for I:= 0 to 8 do begin
    Tmp[I]:= ',,';
  end;

  for I:= 0 to ToolList.Count - 1 do begin
    Tool:= ToolList.Tool[I];
    Tmp[Tool.Ex1 mod 9] := Tool.ShuttleID + ',' +
                           IntToStr(Tool.ShuttleSN) + ',' +
                           FloatToStr(Tool.TotalRemaining);
  end;

  for I:= 0 to 8 do begin
    Named.SetAsString(ToolDefTags[I], PChar(Tmp[I]));
  end;
end;


procedure TToolForm1.UpdateWriteStrings;
var Tmp : Integer;
begin
  CreateDefinitionStrings;
  CreateBalluffString(0, Tags[bttHeadWriteString]);

  Tmp := Named.GetAsInteger(Tags[bttToolChangerPosition]);
  if Tmp > 0 then
    CreateBalluffString(Tmp, Tags[bttToolChangerWriteString]);
end;

{  Reads the buffered string into the current Ex1 number
   although this is intended as a non tool changer machine it should
   be capable of reading in the appropriate Ex1 number }
procedure TToolForm1.AddTool(Ex1: Integer; Buffer: string);
var T: TToolData;
begin
  ToolList.BeginUpdate;
  try
    T:= ToolList.ToolAtEx1(Ex1);
    if T = nil then
      T:= ToolList.AddTool;

    T.ShuttleID:= ReadDelimited(Buffer);
    T.ShuttleSN:= StrToIntDef(ReadDelimited(Buffer), 1);
    T.MaxToolLength:= StrToFloatDef(ReadDelimited(Buffer), 0);
    T.ActToolLength:= StrToFloatDef(ReadDelimited(Buffer), 0);
    T.RebuildTime:= StrToFloatDef(ReadDelimited(Buffer), 0);
    T.MinToolLength:= StrToFloatDef(ReadDelimited(Buffer), 0);
    T.UniqueToolOffsets[0]:=  StrToFloatDef(ReadDelimited(Buffer), 0);
    T.UniqueToolOffsets[1]:=  StrToFloatDef(ReadDelimited(Buffer), 0);
    T.UniqueToolOffsets[2]:=  StrToFloatDef(ReadDelimited(Buffer), 0);
    T.TPM1:= StrToIntDef(ReadDelimited(Buffer), 1);
    T.TPM2:= StrToIntDef(ReadDelimited(Buffer), 1);
    T.UniqueToolOffsets[3]:=  StrToFloatDef(ReadDelimited(Buffer), 0);
    T.StatusFlags:= 7;
    T.BarrelCount:= 1;
    T.RemainingBarrels:= 0;

    CheckToolAddition(T);
    UpdateWriteStrings;

  finally
    ToolList.EndUpdate;
  end;
end;

procedure TToolForm1.CreateBalluffString(Ex1: Integer; Tag: TTagRef);
var T: TToolData;
    I: Integer;
    Tmp: string;
begin
  ToolList.Lock;
  try
    Tmp:= '';
    for I := 0 to ToolList.Count - 1 do begin
      if ToolList[I].Ex1 = Ex1 then begin
        T:= ToolList[I];
        Tmp:= Tmp + T.ShuttleID + ',';
        Tmp:= Tmp + IntToStr(T.ShuttleSN) + ',';
        Tmp:= Tmp + FloatToStr(T.MaxToolLength) + ',';
        Tmp:= Tmp + FloatToStr(T.ActToolLength) + ',';
        Tmp:= Tmp + FloatToStr(T.RebuildTime) + ',';
        Tmp:= Tmp + FloatToStr(T.MinToolLength) + ',';
        Tmp:= Tmp + FloatToStr(T.UniqueToolOffsets[0]) + ',';
        Tmp:= Tmp + FloatToStr(T.UniqueToolOffsets[1]) + ',';
        Tmp:= Tmp + FloatToStr(T.UniqueToolOffsets[2]) + ',';
        Tmp:= Tmp + IntToStr(T.TPM1) + ',';
        Tmp:= Tmp + IntToStr(T.TPM2) + ',';
        Tmp:= Tmp + FloatToStr(T.UniqueToolOffsets[3]) + ',';

        if Ex1 = 0 then begin
          Named.SetAsDouble(Tags[bttUniqueToolOffsetX], T.UniqueToolOffsets[0]);
          Named.SetAsDouble(Tags[bttUniqueToolOffsetY], T.UniqueToolOffsets[1]);
          Named.SetAsDouble(Tags[bttUniqueToolOffsetZ], T.UniqueToolOffsets[2]);
          Named.SetAsDouble(Tags[bttUniqueToolOffsetC], T.UniqueToolOffsets[3]);
        end;
      end;
    end;

    Named.SetAsString(Tag, PChar(Tmp));

  finally
    ToolList.Unlock;
  end;
end;

procedure TToolForm1.SpeedButton3MouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  Named.SetAsInteger(Tags[bttIBBReset], 1);
end;

procedure TToolForm1.SpeedButton3MouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  Named.SetAsInteger(Tags[bttIBBReset], 0);
end;

procedure TToolForm1.BtnWriteMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  AddToolFromEditor(0, True, True);
  Named.SetAsInteger(Tags[bttHeadWrite], 1)
end;

procedure TToolForm1.BtnWriteMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  Named.SetAsInteger(Tags[bttHeadWrite], 0)
end;

procedure TToolForm1.AddToolFromEditor(Ex1: Integer; EditorEntry,
  OffsetVerify: Boolean);

  function LogStrToFloat(Field : Integer) : Double;
  var Msg : string;
  begin
    try
      Result := StrToFloat(Trim(ToolEditor.Cells[1, Field]));
    except
      Msg := 'Invalid ' + ToolEditor.Cells[0, Field] + ' of "' + ToolEditor.Cells[1, Field] + '"';
      Logging(bdHead, Msg);
      raise Exception.Create(Msg);
    end;
  end;

  function LogStrToInt(Field : Integer) : Integer;
  var Msg : string;
  begin
    try
      Result := StrToInt(ToolEditor.Cells[1, Field]);
    except
      Msg := 'Invalid ' + ToolEditor.Cells[0, Field] + ' of "' + ToolEditor.Cells[1, Field] + '"';
      Logging(bdHead, Msg);
      raise Exception.Create(Msg);
    end;
  end;

  function Similar(A, B: Double): Boolean;
  begin
    Result:= Abs(A - B) < 0.0001;
  end;

var T: TToolData;
    Verify: Double;
    TDB : TToolDBItem;
begin
  T := ToolList.ToolAtEx1(Ex1);
  if  T <> nil then
    ToolLife.RemoveTool(T.ShuttleID, T.ShuttleSN);

  try
    if Trim(ToolEditor.Cells[1, ShuttleIDField]) = '' then
      raise Exception.Create('No tool name specified');

    if ToolLife.AddTool( Trim(ToolEditor.Cells[1, ShuttleIDField]),
                         LogStrToInt(ShuttleSNField),
                         LogStrToFloat(MaxToolLengthField),
                         LogStrToFloat(ActualLengthField), Ex1, 0) = trOK then begin

      T:= ToolList.ToolAtEx1(Ex1);
      if T = nil then
        raise Exception.Create('Serious problem with tool life');

      ToolList.BeginUpdate;
      try
        Logging(bdNone, 'Rebuild time = ' + ToolEditor.Cells[1, RebuildTimeField] );
        T.RebuildTime := LogStrToFloat(RebuildTimeField) + Now;
        T.StatusFlags := 7;
        T.BarrelCount := 1;
        T.RemainingBarrels := 0;
        T.MinToolLength := LogStrToFloat(MinToolLengthField);
        T.UniqueToolOffsets[0] := LogStrToFloat(UniqueToolXField);
        T.UniqueToolOffsets[1] := LogStrToFloat(UniqueToolYField);
        T.UniqueToolOffsets[2] := LogStrToFloat(UniqueToolZField);
        T.UniqueToolOffsets[3] := LogStrToFloat(UniqueToolCField);
        T.TPM1:= LogStrToInt(TPM1Field);
        T.TPM2:= LogStrToInt(TPM2Field);
        T.Expiry := (Now + 10000);
        Verify:= StrToFloatDef(ToolEditor.Cells[1, OffsetVerifyField], 0);

        if T.Expiry < Now then begin
          T.RemainingBarrels := 0;
          T.ActToolLength := 0;
          Named.SetFaultA(ObjectName, 'The expiry date for the tool has been exceeded: Tool life has been zeroed', FaultInterlock, nil);
        end;

        Logging(bdNone, 'Rebuild time = ' + FloatToStr(T.RebuildTime) + ' ' + FloatToStr(Now) );

        if T.RebuildTime < Now then begin
          T.RemainingBarrels := 0;
          T.ActToolLength := 0;
          Named.SetFaultA(ObjectName, 'The tool rebuild date has been exceeded. Tool requires checking by a tool maintenance engineer', FaultInterlock, nil);
        end;

        if not Similar (T.UniqueToolOffsets[0] +
                        T.UniqueToolOffsets[1] +
                        T.UniqueToolOffsets[2] +
                        T.UniqueToolOffsets[3], Verify) and OffsetVerify then begin
          T.RemainingBarrels := 0;
          T.ActToolLength := 0;
          Named.SetFaultA(ObjectName, 'Offset verification failed: Tool life has been zeroed', FaultInterlock, nil);
        end;

        if not EditorEntry then begin
          CheckToolAddition(T); //returns true if toollife reset
        end
      finally
        ToolList.EndUpdate;
      end;

      UpdateWriteStrings;


      ToolDB.BeginUpdate;
      try
        TDB := ToolDB.FindTool(T.ShuttleID);
        if not Assigned(TDB) then begin
          TDB := ToolDB.AddTool;
        end;
        TDB.Assign(T);
      finally
        ToolDB.EndUpdate;
      end;
    end;
  except
    on E : Exception do begin
      if FID <> 0 then begin
        Named.FaultResetID(FID);
        FID := 0;
      end;
      FID := Named.SetFaultA(ObjectName, 'Failed to add tool: ' + E.Message, FaultInterlock, nil);
      if T <> nil then
        ToolLife.RemoveTool(T.ShuttleID, T.ShuttleSN);
    end;
  end;
end;

function TToolForm1.CheckToolAddition(T : TToolData):Boolean;
begin
  Result := False;
  ToolList.Lock;
  try
    if Named.GetAsBoolean(Tags[bttOperatorToolValidate]) then begin

      if T.BarrelCount > 1 then begin
        if T.BarrelCount <> T.RemainingBarrels then begin
          if ToolOKDlg.TIsToolOK.Execute(T) then begin
            T.RemainingBarrels := T.BarrelCount;
            T.ActToolLength := 0;
            Result := True;
          end;
        end;
      end
    else begin
      if T.ActToolLength < T.MaxToolLength then begin
        if ToolOKDlg.TIsToolOK.Execute(T) then
          begin
          T.ActToolLength := T.MaxToolLength;
          Result := True;
          end;
        end;
      end;
    end;
  finally
    ToolList.Unlock;
  end;
end;

procedure TToolForm1.DisplaySweep;
var I : TBalluffDevice;
begin
  for I := Low(TBalluffDevice) to High(TBalluffDevice) do begin
    if StateChange[I] then begin
      DoStateChange(I);
    end;
  end;
end;

procedure TToolForm1.UpdateDisplay;
var T: TToolData;
begin
  T := ToolList.ToolAtEx1(0);

  if T <> nil then begin
    ToolEditor.Cells[1, ShuttleIDField] := T.ShuttleID;
    ToolEditor.Cells[1, ShuttleSNField] := IntToStr(T.ShuttleSN);
    ToolEditor.Cells[1, RebuildTimeField] := FloatToStr(T.RebuildTime - Now);
    //ToolEditor.Cells[1, BarrelCountField] := IntToStr(T.BarrelCount);
    //ToolEditor.Cells[1, RemainingBarrelsField] := FloatToStr(T.RemainingBarrels);
    ToolEditor.Cells[1, MaxToolLengthField] := FloatToStr(T.MaxToolLength);
    ToolEditor.Cells[1, MinToolLengthField] := FloatToStr(T.MinToolLength);
    ToolEditor.Cells[1, ActualLengthField] := FloatToStr(T.ActToolLength);
    ToolEditor.Cells[1, UniqueToolXField] := FloatToStr(T.UniqueToolOffsets[0]);
    ToolEditor.Cells[1, UniqueToolYField] := FloatToStr(T.UniqueToolOffsets[1]);
    ToolEditor.Cells[1, UniqueToolZField] := FloatToStr(T.UniqueToolOffsets[2]);
    ToolEditor.Cells[1, UniqueToolCField] := FloatToStr(T.UniqueToolOffsets[3]);
    ToolEditor.Cells[1, TPM1Field] := IntToStr(T.TPM1);
    ToolEditor.Cells[1, TPM2Field] := IntToStr(T.TPM2);
    ToolEditor.Cells[1, OffsetVerifyField]:= '';
  end;
end;

procedure TToolForm1.TagChanged(ATag: TTagRef);
var T: TToolData;
begin
  if Tags[bttHeadRemove] = ATag then
    if Named.GetAsBoolean(ATag) then begin
      T:= ToolList.ToolAtEx1(0);
      if T <> nil then
        ToolLife.RemoveTool(T.ShuttleID, T.ShuttleSN);
    end;

end;

initialization
  TAbstractFormPlugin.AddPlugin('RRToolForm', TToolForm1);
end.
