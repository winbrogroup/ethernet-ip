unit Proto;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  EuchnerPort, StdCtrls, Buttons, ExtCtrls;

type
  TProtocol = class(TForm)
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    Timer1: TTimer;
    procedure FormShow(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormPaint(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
  private
    IH, IW : Integer;
    FRefreshRequired : Boolean;
    procedure RefreshRequired(Sender : TObject);
    procedure DrawCommItem(B : Byte; T, L : Integer);
    procedure UpdateDisplay(Sender : TObject);
  public
    Euchner : TEuchnerPort;
  end;

var
  Protocol: TProtocol;

implementation

{$R *.DFM}

const
  BigFont = 14;
  LittleFont = 7;

procedure TProtocol.DrawCommItem(B : Byte; T, L : Integer);
  procedure LittleOut(const Tmp : string);
  begin
    Canvas.Font.Size := LittleFont;
    Canvas.TextOut(L, T, Tmp[1]);
    Canvas.TextOut(L + (IW div 2), T + (IH div 2) - 2, Tmp[2]);
  end;

var Tmp : string;
begin
  case B of
    Byte('A')..Byte('Z'),
    Byte('a')..Byte('z'),
    Byte('0')..Byte('9') :  begin
      Canvas.Font.Size := BigFont;
      Canvas.TextOut(L, T, Char(B));
    end;

    DLE : begin
      LittleOut('DL');
    end;

    STX : begin
      LittleOut('SX');
    end;

    NAK : begin
      LittleOut('NK');
    end;

    ETX : begin
      LittleOut('EX');
    end;

  else
    Tmp := IntToHex(B, 2);
    LittleOut(Tmp);
  end;
end;

procedure TProtocol.RefreshRequired;
begin
  FRefreshRequired := True;
end;

procedure TProtocol.UpdateDisplay(Sender : TObject);
var H, W, I, J, SoFar, P : Integer;
    T : Integer;
    List : TList;
begin
  if Assigned(Euchner) then begin
    List := Euchner.DataList; //Euchner.DataList.LockList;
    try
      Canvas.Font.Size := BigFont;
      IH := Canvas.TextHeight('Q');
      IW := Canvas.TextWidth('Q');
      H := (ClientHeight - 15) div (IH * 2);
      W := ClientWidth div IW;


      if (W * H) <= List.Count then
        SoFar := List.Count - (W * H)
      else
        SoFar := 0;

      for J := 0 to H - 1 do begin
        T := IH * J * 2;
        Canvas.Brush.Color := clBlue;
        Canvas.Font.Color := clWhite;
        Canvas.FillRect(Rect(0, T, ClientWidth, T + IH));
        for I := 0 to W - 1 do begin
          if I + SoFar >= List.Count then
            Break;
          P := Integer(List[SoFar + I]);
          if (P and $100) <> 0 then begin
            DrawCommItem(Byte(P), T, I * IW);
          end;
        end;

        T := IH * J * 2 + IH;
        Canvas.Brush.Color := clSilver;
        Canvas.Font.Color := clBlack;
        Canvas.FillRect(Rect(0, T, ClientWidth, T + IH));
        for I := 0 to W - 1 do begin
          if I + SoFar >= List.Count then
            Break;
          P := Integer(List[SoFar + I]);
          if (P and $100) = 0 then begin
            DrawCommItem(Byte(P), T, I * IW);
          end;
        end;
        SoFar := SoFar + W;
      end;
    finally
//      Euchner.DataList.UnlockList;
    end;
  end;
end;

procedure TProtocol.FormShow(Sender: TObject);
begin
  Euchner.OnDataChange := RefreshRequired;
end;

procedure TProtocol.FormResize(Sender: TObject);
begin
  Repaint;
end;

procedure TProtocol.FormPaint(Sender: TObject);
begin
  UpdateDisplay(Self);
end;

procedure TProtocol.BitBtn2Click(Sender: TObject);
begin
  Euchner.ClearHistory;
end;

procedure TProtocol.Timer1Timer(Sender: TObject);
begin
  if FRefreshRequired then
    UpdateDisplay(nil);

  FRefreshRequired := False;
end;

end.
