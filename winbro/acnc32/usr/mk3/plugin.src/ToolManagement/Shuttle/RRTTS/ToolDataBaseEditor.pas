unit ToolDataBaseEditor;
{
Need to check noseguide code on tool entry flag not the same.
IF NO NOSEGUIDE SERIAL THEN NO CHECK
-1 SIGNIFIES NO NOSEGUIDE SERIAL NUMBER
NEED TO FLAG IF OFFSETS DIFFER FROM LAST ENTRY AND ALLOW UPDATE ODF DB IF OK'D
}

interface
uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs,
  IFormInterface, INamedInterface, C6001, IniFiles, NamedPlugin, PluginInterface,
  AbstractFormPlugin, IToolLife, CNCTypes, OCTTypes, ACNC_BaseKeyBoards,
  ToolDB, AllToolDB, Math, ToolMonitor, ComCtrls, Grids,ToolEditGrids,
  OCTUtils,
  ToolData, StdCtrls, TTSByTypeData,TTSDataByUniqueTool, ExtCtrls, Buttons;
type

  TRRToolDataEditor = class(TInstallForm)
    BYTypeTitle: TPanel;
    MainPanel: TPanel;
    ByToolEditPage: TPageControl;
    UniqueToolEditPage: TTabSheet;
    ByTypeEditPage: TTabSheet;
    UniqueListPanel: TPanel;
    TypeListPanel: TPanel;
    UTTypeEdit: TEdit;
    UTTypeLabel: TLabel;
    Label1: TLabel;
    UTSerialEdit: TEdit;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    UTXoffEdit: TEdit;
    UTYoffEdit: TEdit;
    UTZoffEdit: TEdit;
    UTCoffEdit: TEdit;
    Label5: TLabel;
    Label6: TLabel;
    UTNGEdit: TEdit;
    Panel2: TPanel;
    UNavFirstBut: TSpeedButton;
    UnavNextBut: TSpeedButton;
    UnavPrevBut: TSpeedButton;
    UnavLastBut: TSpeedButton;
    Panel3: TPanel;
    BTNavFirstBut: TSpeedButton;
    BTNavNextBut: TSpeedButton;
    BTNavPrevBut: TSpeedButton;
    BTNavLastBut: TSpeedButton;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    BtTypeEdit: TEdit;
    BTElectrodeCountEdit: TEdit;
    BTEFlowMaxEdit: TEdit;
    BTEFlowMinEdit: TEdit;
    BTNGFlowMaxEdit: TEdit;
    BTNGFlowMinEdit: TEdit;
    BTPressureMaxEdit: TEdit;
    BTPressureMinEdit: TEdit;
    ToolTypeAddPage: TTabSheet;
    Label16: TLabel;
    AddTT_TYpeEdit: TEdit;
    Label17: TLabel;
    AddTT_ECountEdit: TEdit;
    Label18: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    RotLabel: TLabel;
    MotLabel: TLabel;
    Label23: TLabel;
    AddTT_MinEF_Edit: TEdit;
    AddTT_MinNGFlowEdit: TEdit;
    AddTT_MinPressureEdit: TEdit;
    AddTT_MinRotationEdit: TEdit;
    AddTT_MinMCurrentEdit: TEdit;
    Label24: TLabel;
    AddTT_Max_EFlowEdit: TEdit;
    AddTT_MaxNGFlowEdit: TEdit;
    AddTT_MaxPressureEdit: TEdit;
    AddTT_MaxRotationEdit: TEdit;
    AddTT_MaxMCurrentEdit: TEdit;
    Panel4: TPanel;
    ToolAddMemo: TMemo;
    TTAddTooTypelButton: TButton;
    IsRotaryCheckbox: TCheckBox;
    TTCancelButton: TButton;
    TTSetDefaultButton: TButton;
    HPCheckBox: TCheckBox;
    UTAddPage: TTabSheet;
    Panel5: TPanel;
    UTMemo: TMemo;
    Label25: TLabel;
    UTAddToolTypeEdit: TEdit;
    Label26: TLabel;
    UTAddSerialEdit: TEdit;
    Label27: TLabel;
    UTAddNGSerialEdit: TEdit;
    Label28: TLabel;
    UTAdd_XOffEdit: TEdit;
    Label29: TLabel;
    UTAdd_YOffEdit: TEdit;
    Label30: TLabel;
    Label31: TLabel;
    UTAdd_ZOffEdit: TEdit;
    UTAdd_COffEdit: TEdit;
    UTSetDefaultButton: TButton;
    UTCancelButton: TButton;
    UTAddButton: TButton;
    Label32: TLabel;
    Label33: TLabel;
    Label34: TLabel;
    AddTT_TPM1Edit: TEdit;
    AddTT_TPM2Edit: TEdit;
    AddTT_ExpiryHoursEdit: TEdit;
    Panel1: TPanel;
    Panel6: TPanel;
    Label35: TLabel;
    BTETPM1DefaultEdit: TEdit;
    Label36: TLabel;
    BTETPM2DefaultEdit: TEdit;
    Label37: TLabel;
    BTETPMHourstEdit: TEdit;
    Button1: TButton;
    ByTypeEditSaveBtn: TButton;
    RotSpeedPanel: TPanel;
    Label38: TLabel;
    AddTT_RotSpeedDemand: TEdit;
    NBTypeAddPage: TTabSheet;
    Label41: TLabel;
    Label42: TLabel;
    Label43: TLabel;
    Label44: TLabel;
    Label45: TLabel;
    NBRotLabel: TLabel;
    NBMotorLabel: TLabel;
    Panel7: TPanel;
    NonBallufToolAddMemo: TMemo;
    Label51: TLabel;
    Label52: TLabel;
    AddNBTT_MinEF_Edit: TEdit;
    AddNBTT_MaxEF_Edit: TEdit;
    AddNBTT_MinNGFlowEdit: TEdit;
    AddNBTT_MaxNGFlowEdit: TEdit;
    AddNBTT_MinPressureEdit: TEdit;
    AddNBTT_MaxPressureEdit: TEdit;
    NonBalluffAddToolNameEdit: TEdit;
    AddNBTT_ECountEdit: TEdit;
    AddNBTT_MinRotationEdit: TEdit;
    AddNBTT_MinMCurrentEdit: TEdit;
    AddNBTT_MaxRotationEdit: TEdit;
    AddNBTT_MaxMCurrentEdit: TEdit;
    NBIsRotaryCheck: TCheckBox;
    NBRotarySpeedPanel: TPanel;
    Label53: TLabel;
    AddNBTT_RotSpeedDemand: TEdit;
    NBToolISHPCheck: TCheckBox;
    Label54: TLabel;
    NonBalluffAddToolCodeEdit: TEdit;
    Button2: TButton;
    Button3: TButton;
    Button4: TButton;
    Label49: TLabel;
    Label50: TLabel;
    Label48: TLabel;
    Label55: TLabel;
    Label56: TLabel;
    Label57: TLabel;
    Label58: TLabel;
    Label59: TLabel;
    nbrpmLabel: TLabel;
    nbAmpLabel: TLabel;
    Label62: TLabel;
    Label63: TLabel;
    Label64: TLabel;
    Label65: TLabel;
    Label68: TLabel;
    Label69: TLabel;
    Label70: TLabel;
    rpmLabel: TLabel;
    ampLabel: TLabel;
    Label73: TLabel;
    RotationPanel: TPanel;
    Label12: TLabel;
    BTRotMinEdit: TEdit;
    BTRotMaxEdit: TEdit;
    Label66: TLabel;
    Label39: TLabel;
    BTRotDemandEdit: TEdit;
    Label40: TLabel;
    Label13: TLabel;
    BTMCurrentMinEdit: TEdit;
    BTMCurrentMaxEdit: TEdit;
    Label67: TLabel;
    Label21: TLabel;
    BTChanelCountEdit: TEdit;
    Label22: TLabel;
    AddTT_CCountEdit: TEdit;
    Label46: TLabel;
    AddNBTT_ChanelCountEdit: TEdit;
    procedure Button4Click(Sender: TObject);
    procedure NBIsRotaryCheckClick(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure ByTypeEditSaveBtnClick(Sender: TObject);
    procedure TTSetDefaultButtonClick(Sender: TObject);
    procedure TTCancelButtonClick(Sender: TObject);
    procedure IsRotaryCheckboxClick(Sender: TObject);
    procedure TTAddTooTypelButtonClick(Sender: TObject);
    procedure UTeditKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure UtEditChange(Sender: TObject);
    procedure BTNavButtonClick(Sender: TObject);
    procedure UtNavButtonClick(Sender: TObject);
    procedure SetUnavButtonEnables(CIndex : Integer);
    procedure SetTypeButtonEnables(CIndex : Integer);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    ByTypeData : TByToolTypeList;
    ByToolData : TUniqueToolDataList;
    ByTypeEditGrid    : TByToolTypeGrid;
    ByToolEditGrid : TUniqueToolGrid;
    ToolCode, SN: Integer;
    CurrentUniqueIndex : Integer;
    CurrentTypeIndex : Integer;
    CurrentToolEntry :TTTSByToolTypeData;

  procedure UpdateDisplay;
  procedure UpdateUniquePageForIndex(ToolIndex : Integer);
  procedure UpdateToolTypePageForIndex(ToolIndex : Integer);
  function CheckAddedToolDataValid(var ToolAddData :TToolAddData) : Boolean;
  procedure SetPageTo(DPage: TTabSheet);
  function CheckAddedNonBalluffToolDataValid(var ToolAddData: TToolAddData): Boolean;
    procedure SetRotationVisibility;

  public
    { Public declarations }
    ThenAddTool : Boolean;
    NewEntry : TTTSByToolTypeData;
    LastAddedTool : TTTSByToolTypeData;

    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure BuildDefaults;

    procedure Install; override;
    procedure FinalInstall; override;
    procedure Execute; override;
    procedure Shutdown; override;
    procedure DisplaySweep; override;
    function ExecuteForNonBalluffToolTypeAdd(TypeName : String;HTC : Integer) : TTTSByToolTypeData;
    function ExecuteForToolTypeAdd(ToolData : TToolData;Var IsRotary : Boolean): Boolean;
    procedure ExecuteforFollowOnTool(ToolData : TToolData);
    procedure ExecuteForByTypeEdit;
    function GetToolTypeDataFor(SToolType : String):TTTSByToolTypeData;
    function GetToolTypeDataForNonBallufType(HypertacCode : Integer):TTTSByToolTypeData;
    function GetUniqueToolDataFor(SToolType : String;SToolSerial : Integer):TTTSByUniqueToolData;

    function AddUniqueToolFromTagData(TData : TToolData;
                                      NGSerial : Integer;
                                      EDia : Double;
                                      IsRotary : Boolean):TTTSByUniqueToolData;
    function UpdateToolOffsetsFromTagData(TData : TToolData): Boolean;
    function UpdateNGSerialForTagData(TData : TToolData;NGSerial : Integer): Boolean;
    procedure FlushAllToolDataBaseToDisk;
    procedure FlushByTyeDataBaseToDisk;
  end;


var
RRToolDataEditor  : TRRToolDataEditor;
implementation

uses ToolForm;

{$R *.dfm}

{ TTRRToolDataEditor }

procedure TRRToolDataEditor.Shutdown;
begin
  inherited;

end;

procedure TRRToolDataEditor.DisplaySweep;
begin
  inherited;

end;

procedure TRRToolDataEditor.FinalInstall;
begin
  inherited;
  RRToolDataEditor := Self;
  ByTypeData := TByToolTypeList.Create;
  ByToolData := TUniqueToolDataList.Create;

end;

procedure TRRToolDataEditor.Install;
begin
  inherited;
  ByTypeEditGrid := TByToolTypeGrid.Create(nil);
  with ByTypeEditGrid do
    begin
    Parent := TypeListPanel;
    TabStop := False;
    EditorMode := False;
    Align := alClient;
    end;

  ByToolEditGrid := TUniqueToolGrid.Create(nil);
  with ByToolEditGrid do
    begin
    Parent := UniqueListPanel;
    TabStop := False;
    EditorMode := False;
    Align := alClient;
    end

end;

procedure TRRToolDataEditor.Execute;
begin
  inherited;
  if not Visible then
    begin
    BuildDefaults;
    ShowModal;
    end;
end;

constructor TRRToolDataEditor.Create(AOwner: TComponent);
begin
  inherited;
end;

destructor TRRToolDataEditor.Destroy;
begin
  ByTypeData.Close;
  ByTypeData := nil;
  ByToolData.Close;
  ByToolData := nil;
  inherited;
end;


procedure TRRToolDataEditor.BuildDefaults;
//var SelectedTool : ToolData;
begin
  ToolCode := Named.GetAsInteger(ToolManager.Tags[bttHypertacToolCode]);
  SN := Named.GetAsInteger(ToolManager.Tags[bttHypertacSerialNumber]);
(*//  LabelToolCode.Caption := Format('%d / %d', [ToolCode, SN]);
  SelectedTool := ToolList.Find(ToolCode,SN);
  if Assigned(SelectedTool) then begin
    DisToolType.Text := SelectedTool.ToolName;
    DisRecCount.Text := IntToStr(AllToolDB.Count);
//    DisToolSerial.Text := ATDB.ToolName;
{    ToolEditor.Cells[1, ToolNameField] := ATDB.ToolName;
    ToolEditor.Cells[1, MinimumToolLengthField] := Format('%.4f', [ATDB.MinToolLength]);
    ToolEditor.Cells[1, UniqueOffsetXField] := Format('%.4f', [ATDB.UniqueToolX]);
    ToolEditor.Cells[1, UniqueOffsetYField] := Format('%.4f', [ATDB.UniqueToolY]);
    ToolEditor.Cells[1, UniqueOffsetZField] := Format('%.4f', [ATDB.UniqueToolZ]);
    ToolEditor.Cells[1, UniqueOffsetVerifyField] := Format('%.4f', [ATDB.UniqueToolX + ATDB.UniqueToolY + ATDB.UniqueToolZ]);}
  end else begin
    DisToolType.Text := Format('%.d', [ToolCode]);
    DisRecCount.Text := IntToStr(AllToolDB.Count);
{    ToolEditor.Cells[1, ToolNameField] := Format('%.d', [ToolCode]);
    ToolEditor.Cells[1, MinimumToolLengthField] := '100';
    ToolEditor.Cells[1, UniqueOffsetXField] := '0';
    ToolEditor.Cells[1, UniqueOffsetYField] := '0';
    ToolEditor.Cells[1, UniqueOffsetZField] := '0';
    ToolEditor.Cells[1, UniqueOffsetVerifyField] := '0';}
  end;*)
end;

procedure TRRToolDataEditor.FormShow(Sender: TObject);
begin
UpdateDisplay;
UtNavButtonClick(UNavFirstBut);
BTNavButtonClick(BTNavFirstBut);
end;

procedure TRRToolDataEditor.UpdateDisplay;
begin
SetRotationVisibility;
if assigned(ByTypeData) then ByTypeEditGrid.LoadFromDB(ByTypeData);
if assigned(ByToolData) then ByToolEditGrid.LoadFromDB(ByToolData);
case ByToolEditPage.ActivePageIndex of
 0: UTTypeEdit.setFocus;
 1: BtTypeEdit.SetFocus;
 2: AddTT_ECountEdit.SetFocus;
 3: AddNBTT_ECountEdit.SetFocus;
end;
end ;

procedure TRRToolDataEditor.UtNavButtonClick(Sender: TObject);
begin
if Sender = UNavFirstBut then
  begin
  if ByToolData.Count > 0 then CurrentUniqueIndex := 0
  end else
if Sender = UNavNextBut then
  begin
  if CurrentUniqueIndex < ByToolData.Count-1 then
    begin
    inc(CurrentUniqueIndex);
    end;
  end else
if Sender = UNavPrevBut then
  begin
  if CurrentUniqueIndex > 0 then
    begin
    dec(CurrentUniqueIndex);
    end;
  end else
if Sender = UNavLastBut then
  begin
  CurrentUniqueIndex := ByToolData.Count-1
  end;
UpdateUniquePageForIndex(CurrentUniqueIndex);
ByToolEditGrid.ShowIndexAtTop(CurrentUniqueIndex);
SetUnavButtonEnables(CurrentUniqueIndex);
end;

procedure TRRToolDataEditor.UpdateUniquePageForIndex(ToolIndex: Integer);
var
ToolEntry :TTTSByUniqueToolData;
begin
If ToolIndex < ByToolData.Count then
  begin
  ToolEntry := ByToolData.Tool[ToolIndex];
  UTTypeEdit.Text := ToolEntry.ToolType;
  UTSerialEdit.Text := IntToStr(ToolEntry.SerialNumber);
  UTNGEdit.Text :=     IntToStr(ToolEntry.NGSerialNumber);
  UTXoffEdit.Text := Format('%7.3f',[ToolEntry.UniqueToolOffsets[0]]);
  UTYoffEdit.Text := Format('%7.3f',[ToolEntry.UniqueToolOffsets[1]]);
  UTZoffEdit.Text := Format('%7.3f',[ToolEntry.UniqueToolOffsets[2]]);
  UTCoffEdit.Text := Format('%7.3f',[ToolEntry.UniqueToolOffsets[3]]);
  end;
end;

procedure TRRToolDataEditor.UpdateToolTypePageForIndex(ToolIndex: Integer);
begin
If ToolIndex < ByTypeData.Count then
  begin
  CurrentToolEntry := ByTypeData.Tool[ToolIndex];
  RotationPanel.Visible := CurrentToolEntry.IsRotary;
  BtTypeEdit.Text := CurrentToolEntry.ToolType;
  BTElectrodeCountEdit.Text := IntToStr(CurrentToolEntry.ElectrodeCount);
  BTChanelCountEdit.Text := IntToStr(CurrentToolEntry.ChanelCount);
  BTEFlowMaxEdit.Text := Format('%7.3f',[CurrentToolEntry.ElectrodeFlowMax]);
  BTEFlowMinEdit.Text := Format('%7.3f',[CurrentToolEntry.ElectrodeFlowMin]);
  BTNGFlowMaxEdit.Text := Format('%7.3f',[CurrentToolEntry.NoseGuideFlowMax]);
  BTNGFlowMinEdit.Text := Format('%7.3f',[CurrentToolEntry.NoseGuideFlowMin]);
  BTPressureMaxEdit.Text := Format('%7.3f',[CurrentToolEntry.PressureMax]);
  BTPressureMinEdit.Text := Format('%7.3f',[CurrentToolEntry.PressureMin]);
  BTMCurrentMaxEdit.Text := Format('%7.3f',[CurrentToolEntry.MCurrentMax]);
  BTMCurrentMinEdit.Text := Format('%7.3f',[CurrentToolEntry.MCurrentMin]);
  BTRotDemandEdit.Text := IntToStr(CurrentToolEntry.RotationDemand);
  BTRotMaxEdit.Text := Format('%7.3f',[CurrentToolEntry.RotationMax]);
  BTRotMinEdit.Text := Format('%7.3f',[CurrentToolEntry.RotationMin]);
  BTETPM1DefaultEdit.Text := IntToStr(CurrentToolEntry.TPM1Default);
  BTETPM2DefaultEdit.Text := IntToStr(CurrentToolEntry.TPM2Default);
  BTETPMHourstEdit.Text := IntToStr(CurrentToolEntry.ExpiryHours);
  end;
end;

procedure TRRToolDataEditor.SetUnavButtonEnables(CIndex: Integer);
begin
UNavFirstBut.Enabled := Cindex > 0;
UNavNextBut.Enabled := Cindex < ByToolData.Count-1;
UNavPrevBut.Enabled := Cindex > 0;
UnavLastBut.Enabled := Cindex < ByToolData.Count-1;
end;

procedure TRRToolDataEditor.SetTypeButtonEnables(CIndex: Integer);
begin
BTNavFirstBut.Enabled := Cindex > 0;
BTNavNextBut.Enabled := Cindex < ByTypeData.Count-1;
BTNavPrevBut.Enabled := Cindex > 0;
BTNavLastBut.Enabled := Cindex < ByTypeData.Count-1;
end;

procedure TRRToolDataEditor.BTNavButtonClick(Sender: TObject);
begin
if Sender = BTNavFirstBut then
  begin
  CurrentTypeIndex := 0;
  end else
if Sender = BTNavNextBut then
  begin
  if CurrentTypeIndex < ByTypeData.Count-1 then
    begin
    inc(CurrentTypeIndex);
    end;
  end else
if Sender = BTNavPrevBut then
  begin
  if CurrentTypeIndex > 0 then
    begin
    dec(CurrentTypeIndex);
    end;
  end else
if Sender = BTNavLastBut then
  begin
  CurrentTypeIndex := ByTypeData.Count-1
  end;
UpdateToolTypePageForIndex(CurrentTypeIndex);
ByTypeEditGrid.ShowIndexAtTop(CurrentTypeIndex);
SetTypeButtonEnables(CurrentTypeIndex);
end;


procedure TRRToolDataEditor.UtEditChange(Sender: TObject);
begin
if sender =  UTTypeEdit  then
  begin
  end else

if sender =  UTSerialEdit  then
  begin
  end else

if sender =  UTNGEdit  then
  begin
  end else

if sender =  UTXoffEdit  then
  begin
  end else

if sender =  UTYoffEdit  then
  begin
  end else

if sender =  UTZoffEdit  then
  begin
  end else

if sender =  UTCoffEdit  then
  begin
  end;



end;

procedure TRRToolDataEditor.UTeditKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
if Key <> 13 then exit;

if sender =  UTTypeEdit  then
  begin
  end else

if sender =  UTSerialEdit  then
  begin
  end else

if sender =  UTNGEdit  then
  begin
  end else

if sender =  UTXoffEdit  then
  begin
  end else

if sender =  UTYoffEdit  then
  begin
  end else

if sender =  UTZoffEdit  then
  begin
  end else

if sender =  UTCoffEdit  then
  begin
  end;

end;


function TRRToolDataEditor.GetUniqueToolDataFor(SToolType: String; SToolSerial: Integer): TTTSByUniqueToolData;
var
Toolindex : Integer;
begin
Result := nil;
ToolIndex := ByToolData.IndexOf(SToolTYpe,SToolSerial);
if ToolIndex >=0 then  Result := ByToolData.Tool[ToolIndex];
end;


function TRRToolDataEditor.GetToolTypeDataForNonBallufType(HypertacCode: Integer): TTTSByToolTypeData;
begin
Result := ByTypeData.FindByHypertacCode(HypertacCode)
end;

function TRRToolDataEditor.GetToolTypeDataFor(SToolType: String): TTTSByToolTypeData;
begin
Result := ByTypeData.Find(SToolType)
end;

procedure TRRToolDataEditor.ExecuteforFollowOnTool(ToolData: TToolData);
begin

end;

procedure TRRToolDataEditor.ExecuteForByTypeEdit;
begin
SetPageTo(ByTypeEditPage);
ShowModal;
end;


function TRRToolDataEditor.ExecuteForNonBalluffToolTypeAdd(TypeName : String;HTC : Integer) : TTTSByToolTypeData;
begin
Result := Nil;
SetPageTo(NBTypeAddPage);
NonBalluffAddToolNameEdit.Text := TYpeName;
NonBalluffAddToolCodeEdit.Text := IntToStr(HTC);
AddNBTT_ECountEdit.Text := '???';
AddNBTT_ChanelCountEdit.Text := '???';
AddNBTT_MinEF_Edit.Text := '???';
AddNBTT_MinNGFlowEdit.Text := '???';
AddNBTT_MinPressureEdit.Text := '???';
AddNBTT_MinRotationEdit.Text := '???';
AddNBTT_MinMCurrentEdit.Text := '???';
AddNBTT_MaxEF_Edit.Text := '???';
AddNBTT_MaxNGFlowEdit.Text := '???';
AddNBTT_MaxPressureEdit.Text := '???';
AddNBTT_MaxRotationEdit.Text := '???';
AddNBTT_MaxMCurrentEdit.Text := '???';
NonBallufToolAddMemo.Lines.clear;
NonBallufToolAddMemo.Lines.Add('Please enter the Test Parameters below which will apply to all tools of this type');
NonBallufToolAddMemo.Lines.Add('and then press the Add Tool Type button');
ShowModal;
if ModalResult = mrOk then
  begin
  Result := NewEntry;
  end;
end;

function TRRToolDataEditor.ExecuteForToolTypeAdd(ToolData: TToolData;Var IsRotary : Boolean): Boolean;
begin
IsRotary := False;
Result := False;
SetPageTo(ToolTypeAddPage);
if assigned(ToolData) then
  begin
  AddTT_TYpeEdit.Text := ToolData.ShuttleID;
  end
else
  begin
  ToolAddMemo.Lines.Add('?????')
  end;
AddTT_ECountEdit.Text := '???';
AddTT_CCountEdit.Text := '???';
AddTT_MinEF_Edit.Text := '???';
AddTT_MinNGFlowEdit.Text := '???';
AddTT_MinPressureEdit.Text := '???';
AddTT_MinRotationEdit.Text := '???';
AddTT_MinMCurrentEdit.Text := '???';
AddTT_Max_EFlowEdit.Text := '???';
AddTT_MaxNGFlowEdit.Text := '???';
AddTT_MaxPressureEdit.Text := '???';
AddTT_MaxRotationEdit.Text := '???';
AddTT_MaxMCurrentEdit.Text := '???';
ToolAddMemo.Lines.clear;
ToolAddMemo.Lines.Add('Please enter the Test Parameters below which will apply to all tools of this type');
ToolAddMemo.Lines.Add('and then press the Add Tool Type button');
//AddTT_ECountEdit.SetFocus;
ShowModal;
if ModalResult = mrOk then
  begin
  Result := True;
  if assigned(LastAddedTool) then
    begin
    IsRotary := LastAddedTool.IsRotary;
    end;
  end;
end;

procedure TRRToolDataEditor.TTAddTooTypelButtonClick(Sender: TObject);
var
TypeData : TToolAddData;
begin
TYpeData.ToolType := AddTT_TYpeEdit.Text;
if CheckAddedToolDataValid(TypeData)  then
  begin
  TypeData.IsNonBalluff := False;
  ToolAddMemo.Lines.Clear;
  LastAddedTool := ByTypeData.AddToolFromAddData(TypeData);
  if assigned(LastAddedTool) then
    begin
    ModalResult := mrOk;
    end
  else
    begin
    ToolAddMemo.Lines.Add('Problem saving Data');
    ModalResult := mrAbort;
    end;
  end
else
  begin
  ToolAddMemo.Lines.Clear;
  ToolAddMemo.Lines.Add('Data InValid');
  ToolAddMemo.Lines.Add('Please check data entered and retry');
  ToolAddMemo.Lines.Add('or press Cancel');
  end

end;


function TRRToolDataEditor.CheckAddedNonBalluffToolDataValid(var ToolAddData :TToolAddData) : Boolean;
var
IntVal : Integer;
MinVal,MaxVal : Single;
Buffer : String;
begin
Result := True;
if NonBalluffAddToolNameEdit.Text <> '' then
  begin
  ToolAddData.TPM1Default := 0;
  ToolAddData.TPM2Default := 0;
  ToolAddData.IsRotaryTool := False;
  if ByTypeData.ToolExists(NonBalluffAddToolNameEdit.Text) then
    begin
    NonBallufToolAddMemo.Lines.clear;
    NonBallufToolAddMemo.Lines.Add('Tool Type name Already Exists In data base');
    NonBallufToolAddMemo.Lines.Add('Please check data Input or press Cancel');
    Result := False;
    exit;
    end;

    ToolAddData.NonBallufHypertacCode := StrToIntDef(NonBalluffAddToolCodeEdit.Text,-1);
    if ToolAddData.NonBallufHypertacCode < 0 then
      begin
      NonBallufToolAddMemo.Lines.clear;
      NonBallufToolAddMemo.Lines.Add('Should Not Get Here');
      Result := False;
      exit;
      end;
    Buffer := ByTypeData.ToolNameFromHypertacCode(ToolAddData.NonBallufHypertacCode);
    If Buffer = '' then
      begin
      Buffer := trim(NonBalluffAddToolNameEdit.Text);
      if (Buffer <> '') and (Buffer[1] <> '?') then
        begin
        ToolAddData.ToolType := trim(NonBalluffAddToolNameEdit.Text);
        ToolAddData.IsNonBalluff := True;
        end
      else
        begin
        NonBallufToolAddMemo.Lines.clear;
        NonBallufToolAddMemo.Lines.Add('Invalid Tool Name Entered');
        Result := False;
        exit;
        end;
      // Electrode Count
      ToolAddData.ElectrodeCount := StrToIntDef(AddNBTT_ECountEdit.Text,0);
      If ToolAddData.ElectrodeCount <=0 then
        begin
        NonBallufToolAddMemo.Lines.clear;
        NonBallufToolAddMemo.Lines.Add('Invalid Electrode Count');
        Result := False;
        exit;
        end;

      ToolAddData.ChanelCount := StrToIntDef(AddNBTT_ChanelCountEdit.Text,0);
      If ToolAddData.ElectrodeCount < ToolAddData.ElectrodeCount then
        begin
        NonBallufToolAddMemo.Lines.clear;
        NonBallufToolAddMemo.Lines.Add('Invalid Chanel Count');
        Result := False;
        exit;
        end;

      MaxVal := StrToFloatDef(AddNBTT_MaxEF_Edit.Text,-999999);
      MinVal := StrToFloatDef(AddNBTT_MinEF_Edit.Text,999999);
      if MaxVal<MinVal then
        begin
        NonBallufToolAddMemo.Lines.clear;
        NonBallufToolAddMemo.Lines.Add('Invalid Electrode Flow Max/Min');
        Result := False;
        exit;
        end;
      ToolAddData.ElectrodeFlowMin := MinVal;
      ToolAddData.ElectrodeFlowMax := MaxVal;

      MaxVal := StrToFloatDef(AddNBTT_MaxNGFlowEdit.Text,-999999);
      MinVal := StrToFloatDef(AddNBTT_MinNGFlowEdit.Text,999999);
      if MaxVal<MinVal then
        begin
        NonBallufToolAddMemo.Lines.clear;
        NonBallufToolAddMemo.Lines.Add('Invalid Noseguide Flow Max/Min');
        Result := False;
        exit;
        end;

      ToolAddData.NoseguideFlowMin := MinVal;
      ToolAddData.NoseguideFlowMax := MaxVal;

      MaxVal := StrToFloatDef(AddNBTT_MaxPressureEdit.Text,-999999);
      MinVal := StrToFloatDef(AddNBTT_MinPressureEdit.Text,999999);
      if MaxVal<MinVal then
        begin
        NonBallufToolAddMemo.Lines.clear;
        NonBallufToolAddMemo.Lines.Add('Invalid Pressure Max/Min');
        Result := False;
        exit;
        end;
      ToolAddData.PressureMin := MinVal;
      ToolAddData.PressureMax := MaxVal;

      ToolAddData.IsHpTool := HPCheckBox.checked;
      ToolAddData.IsRotaryTool := IsRotaryCheckbox.checked;
      ToolAddData.RotationDemand := 0;
      ToolAddData.RotationMin := 0;
      ToolAddData.RotationMax := 0;
      ToolAddData.MCurrentMin := 0;
      ToolAddData.MCurrentMax := 0;
      if NBIsRotaryCheck.checked then
          begin
          ToolAddData.IsRotaryTool := True;
          MaxVal := StrToFloatDef(AddNBTT_RotSpeedDemand.Text,-999999);
          if (MaxVal > 100) or (MaxVal < 1) then
            begin
            NonBallufToolAddMemo.Lines.clear;
            NonBallufToolAddMemo.Lines.Add('Invalid Rotataion Demand');
            Result := False;
            end;
          ToolAddData.RotationDemand := Round(MaxVal);

          MaxVal := StrToFloatDef(AddNBTT_MaxRotationEdit.Text,-999999);
          MinVal := StrToFloatDef(AddNBTT_MinRotationEdit.Text,999999);
          if MaxVal<MinVal then
            begin
            NonBallufToolAddMemo.Lines.clear;
            NonBallufToolAddMemo.Lines.Add('Invalid Rotataion Max/Min');
            Result := False;
            end
          else begin
          ToolAddData.RotationMin := MinVal;
          ToolAddData.RotationMax := MaxVal;
          MaxVal := StrToFloatDef(AddNBTT_MaxMCurrentEdit.Text,-999999);
          MinVal := StrToFloatDef(AddNBTT_MinMCurrentEdit.Text,999999);
          if MaxVal<MinVal then
            begin
            NonBallufToolAddMemo.Lines.clear;
            NonBallufToolAddMemo.Lines.Add('Invalid Motor Current Max/Min');
            Result := False;
            exit;
            end;
          ToolAddData.MCurrentMin := MinVal;
          ToolAddData.MCurrentMax := MaxVal;
          end; //if IsRotaryCheckbox.checked
        end
      end // If Buffer = ''
    else
      begin
      NonBallufToolAddMemo.Lines.clear;
      NonBallufToolAddMemo.Lines.Add('Hypertac Code already exists');
      NonBallufToolAddMemo.Lines.Add('Should Never Get here');
      Result := False;
      exit;
      end;
    end
  else
    begin
    Result := False;
    NonBallufToolAddMemo.Lines.clear;
    NonBallufToolAddMemo.Lines.Add('No Tooltype Name Specified');
    NonBallufToolAddMemo.Lines.Add('Please check data Input or press Cancel');
    end;
end;

function TRRToolDataEditor.CheckAddedToolDataValid(var ToolAddData :TToolAddData): Boolean;
var
IntVal : Integer;
MinVal,MaxVal : Single;
DateVal : Double;
begin
Result := True;
if AddTT_TYpeEdit.Text <> '' then
  begin
  ToolAddData.IsNonBalluff := False;
  ToolAddData.NonBallufHypertacCode := 0;
  if ByTypeData.ToolExists(AddTT_TYpeEdit.Text) then
    begin
    ToolAddMemo.Lines.clear;
    ToolAddMemo.Lines.Add('Tool Type Already Exists In Database. Can Not Add. ');
    ToolAddMemo.Lines.Add('Please check data Input or press Cancel');
    Result := False;
    exit;
    end
  else
    begin
    ToolAddData.ElectrodeCount := StrToIntDef(AddTT_ECountEdit.Text,0);
    If ToolAddData.ElectrodeCount <=0 then
      begin
      ToolAddMemo.Lines.clear;
      ToolAddMemo.Lines.Add('Invalid Electrode Count');
      Result := False;
      end
    else begin

    ToolAddData.ChanelCount := StrToIntDef(AddTT_CCountEdit.Text,0);
    If ToolAddData.ChanelCount < ToolAddData.ElectrodeCount then
      begin
      ToolAddMemo.Lines.clear;
      ToolAddMemo.Lines.Add('Invalid Chanel Count');
      Result := False;
      end
    else begin

    MaxVal := StrToFloatDef(AddTT_Max_EFlowEdit.Text,-999999);
    MinVal := StrToFloatDef(AddTT_MinEF_Edit.Text,999999);
    if MaxVal<MinVal then
      begin
      ToolAddMemo.Lines.clear;
      ToolAddMemo.Lines.Add('Invalid Electrode Flow Max/Min');
      Result := False;
      end
    else begin
    ToolAddData.ElectrodeFlowMin := MinVal;
    ToolAddData.ElectrodeFlowMax := MaxVal;

    MaxVal := StrToFloatDef(AddTT_MaxNGFlowEdit.Text,-999999);
    MinVal := StrToFloatDef(AddTT_MinNGFlowEdit.Text,999999);
    if MaxVal<MinVal then
      begin
      ToolAddMemo.Lines.clear;
      ToolAddMemo.Lines.Add('Invalid Noseguide Flow Max/Min');
      Result := False;
      end
    else begin

    ToolAddData.NoseguideFlowMin := MinVal;
    ToolAddData.NoseguideFlowMax := MaxVal;

    MaxVal := StrToFloatDef(AddTT_MaxPressureEdit.Text,-999999);
    MinVal := StrToFloatDef(AddTT_MinPressureEdit.Text,999999);
      if MaxVal<MinVal then
        begin
        ToolAddMemo.Lines.clear;
        ToolAddMemo.Lines.Add('Invalid Pressure Max/Min');
        Result := False;
        end
      else begin
      ToolAddData.PressureMin := MinVal;
      ToolAddData.PressureMax := MaxVal;
      end;
      ToolAddData.PressureMin := MinVal;
      ToolAddData.PressureMax := MaxVal;
    IntVal := StrToIntDef(AddTT_TPM1Edit.Text,1);
      if IntVal< 1 then
        begin
        ToolAddMemo.Lines.clear;
        ToolAddMemo.Lines.Add('Invalid TPM1 Value');
        Result := False;
        end
      else begin
      ToolAddData.TPM1Default := IntVal;
      end;
    IntVal := StrToIntDef(AddTT_TPM2Edit.Text,0);
      if IntVal< 1 then
        begin
        ToolAddMemo.Lines.clear;
        ToolAddMemo.Lines.Add('Invalid TPM2 Value');
        Result := False;
        end
      else begin
      ToolAddData.TPM2Default := Intval;
      end;
    end;
{ !!!!intVal := StrToIntDef(AddTT_TPM2Edit.Text,2);
      if IntVal< 1 then
        begin
        ToolAddMemo.Lines.clear;
        ToolAddMemo.Lines.Add('Invalid TPM2 Value');
        Result := False;
        end
      else begin
      ToolAddData.TPM2Default := IntVal;
      end;}
    IntVal := StrToIntDef(AddTT_ExpiryHoursEdit.Text,8);
      if IntVal< 1 then
        begin
        ToolAddMemo.Lines.clear;
        ToolAddMemo.Lines.Add('Invalid Expiry Hours Value');
        Result := False;
        end
      else begin
      ToolAddData.ExpiryHoursDefault := IntVal;
      end;
    end;
    ToolAddData.IsHpTool := HPCheckBox.checked;
    ToolAddData.IsRotaryTool := IsRotaryCheckbox.checked;
    ToolAddData.RotationDemand := 0;
    ToolAddData.RotationMin := 0;
    ToolAddData.RotationMax := 0;
    ToolAddData.MCurrentMin := 0;
    ToolAddData.MCurrentMax := 0;

     if IsRotaryCheckbox.checked then
        begin
        MaxVal := StrToFloatDef(AddTT_RotSpeedDemand.Text,-999999);
        if (MaxVal > 100) or (MaxVal < 1) then
          begin
          ToolAddMemo.Lines.clear;
          ToolAddMemo.Lines.Add('Invalid Rotataion Demand');
          Result := False;
          end;
        ToolAddData.RotationDemand := Round(MaxVal);

        MaxVal := StrToFloatDef(AddTT_MaxRotationEdit.Text,-999999);
        MinVal := StrToFloatDef(AddTT_MinRotationEdit.Text,999999);
        if MaxVal<MinVal then
          begin
          ToolAddMemo.Lines.clear;
          ToolAddMemo.Lines.Add('Invalid Rotataion Max/Min');
          Result := False;
          end
        else begin
        ToolAddData.RotationMin := MinVal;
        ToolAddData.RotationMax := MaxVal;
        MaxVal := StrToFloatDef(AddTT_MaxMCurrentEdit.Text,-999999);
        MinVal := StrToFloatDef(AddTT_MinMCurrentEdit.Text,999999);
        if MaxVal<MinVal then
          begin
          ToolAddMemo.Lines.clear;
          ToolAddMemo.Lines.Add('Invalid Motor Current Max/Min');
          Result := False;
          end
        else begin
        ToolAddData.MCurrentMin := MinVal;
        ToolAddData.MCurrentMax := MaxVal;
        end;
      end;
  end;
  end;
  end;
  end;
  end;
  If not Result then
    begin
    ToolAddMemo.Lines.Add('Please check data Input or press Cancel');
    end
end;
procedure TRRToolDataEditor.SetRotationVisibility;
begin
if not IsRotaryCheckbox.Checked then
  begin
  RotSpeedPanel.Visible := False;
  AddTT_MinRotationEdit.Visible := False;
  AddTT_MaxRotationEdit.Visible := False;
  AddTT_MinMCurrentEdit.Visible := False;
  AddTT_MaxMCurrentEdit.Visible := False;
  RotLabel.Visible := False;
  MotLabel.Visible := False;
  rpmLabel.Visible := False;
  ampLabel.Visible := False;
  end
else
  begin
  RotSpeedPanel.Visible := True;
  AddTT_MinRotationEdit.Visible := True;
  AddTT_MaxRotationEdit.Visible := True;
  AddTT_MinMCurrentEdit.Visible := True;
  AddTT_MaxMCurrentEdit.Visible := True;
  RotLabel.Visible := True;
  MotLabel.Visible := True;
  rpmLabel.Visible := True;
  ampLabel.Visible := True;
  end;

if not NBIsRotaryCheck.Checked then
  begin
  NBRotarySpeedPanel.Visible := False;
  AddNBTT_MinRotationEdit.Visible := False;
  AddNBTT_MaxRotationEdit.Visible := False;
  AddNBTT_MaxMCurrentEdit.Visible := False;
  AddNBTT_MinMCurrentEdit.Visible := False;
  NBMotorLabel.Visible := False;
  NBRotLabel.Visible := False;
  nbrpmLabel.Visible := False;
  nbAmpLabel.Visible := False;
  end
else
  begin
  NBRotarySpeedPanel.Visible := True;
  AddNBTT_MinRotationEdit.Visible := True;
  AddNBTT_MaxRotationEdit.Visible := True;
  AddNBTT_MaxMCurrentEdit.Visible := True;
  AddNBTT_MinMCurrentEdit.Visible := True;
  NBMotorLabel.Visible := True;
  NBRotLabel.Visible := True;
  nbrpmLabel.Visible := True;
  nbAmpLabel.Visible := True;
  end


end;

procedure TRRToolDataEditor.IsRotaryCheckboxClick(Sender: TObject);
begin
SetRotationVisibility;
if not IsRotaryCheckbox.Checked then
  begin
  AddTT_MinRotationEdit.Enabled := False;
  AddTT_MinRotationEdit.Text := '--';
  AddTT_MaxRotationEdit.Enabled := False;
  AddTT_MaxRotationEdit.Text := '--';
  AddTT_MaxMCurrentEdit.Enabled := False;
  AddTT_MaxMCurrentEdit.Text := '--';
  AddTT_MinMCurrentEdit.Enabled := False;
  AddTT_MinMCurrentEdit.Text := '--';
  end
else
  begin
  AddTT_RotSpeedDemand.Text := '???';
  AddTT_MinRotationEdit.Enabled := True;
  AddTT_MinRotationEdit.Text := '???';
  AddTT_MaxRotationEdit.Enabled := True;
  AddTT_MaxRotationEdit.Text := '???';
  AddTT_MaxMCurrentEdit.Enabled := True;
  AddTT_MaxMCurrentEdit.Text := '???';
  AddTT_MinMCurrentEdit.Enabled := True;
  AddTT_MinMCurrentEdit.Text := '???';
  end;
end;

procedure TRRToolDataEditor.TTCancelButtonClick(Sender: TObject);
begin
ModalResult := MRCancel;
Close;
end;

function TRRToolDataEditor.AddUniqueToolFromTagData(TData: TToolData;
                                                    NGSerial: Integer;
                                                    EDia : Double;
                                                    IsRotary : Boolean): TTTSByUniqueToolData;
begin
Result := ByToolData.AddToolFromTagData(TData,nGSerial,EDia,IsRotary); // return false if tool already exists
end;

function TRRToolDataEditor.UpdateToolOffsetsFromTagData(TData: TToolData): Boolean;
begin
Result := False;
if assigned(TData) then
  begin
  Result := ByToolData.UpdateOffsetsFor(TData);
  end;
end;

function TRRToolDataEditor.UpdateNGSerialForTagData(TData: TToolData;
  NGSerial: Integer): Boolean;
begin
Result := False;
if assigned(TData) then
  begin
  Result := ByToolData.UpdateNGSerialForTagData(TData,NGSerial);
  end;
end;

procedure TRRToolDataEditor.TTSetDefaultButtonClick(Sender: TObject);
begin
if IsRotaryCheckbox.Checked then
  begin
  if AddTT_ECountEdit.Text = '???' then
    begin
    AddTT_ECountEdit.Text := '1';
    AddTT_CCountEdit.Text := '6';
    end;
  if  AddTT_CCountEdit.Text = '???' then AddTT_CCountEdit.Text := AddTT_ECountEdit.Text;

  AddTT_MinEF_Edit.Text := Format('%7.3f',[ToolManager.DefaultRotarySettings.EFlowMin]);
  AddTT_Max_EFlowEdit.Text := Format('%7.3f',[ToolManager.DefaultRotarySettings.EFlowMax]);
  AddTT_MinNGFlowEdit.Text := Format('%7.3f',[ToolManager.DefaultRotarySettings.NGFlowMin]);
  AddTT_MaxNGFlowEdit.Text := Format('%7.3f',[ToolManager.DefaultRotarySettings.NGFlowMax]);
  AddTT_RotSpeedDemand.Text := Format('%7.3f',[ToolManager.DefaultRotarySettings.RotationDemand]);
  AddTT_MinRotationEdit.Text := Format('%7.3f',[ToolManager.DefaultRotarySettings.RotationMin]);
  AddTT_MaxRotationEdit.Text := Format('%7.3f',[ToolManager.DefaultRotarySettings.RotationMax]);
  AddTT_MinMCurrentEdit.Text := Format('%7.3f',[ToolManager.DefaultRotarySettings.MCurrentMin]);
  AddTT_MaxMCurrentEdit.Text := Format('%7.3f',[ToolManager.DefaultRotarySettings.MCurrentMax]);
  AddTT_TPM1Edit.Text := Format('%d',[ToolManager.DefaultRotarySettings.TPM1]);
  AddTT_TPM2Edit.Text := Format('%d',[ToolManager.DefaultRotarySettings.TPM2]);
  AddTT_ExpiryHoursEdit.Text := Format('%d',[ToolManager.DefaultRotarySettings.ExpiryHours]);
  if HPCheckBox.Checked then
    begin
    AddTT_MinPressureEdit.Text := Format('%7.3f',[ToolManager.DefaultRotarySettings.PressureMinHP]);
    AddTT_MaxPressureEdit.Text := Format('%7.3f',[ToolManager.DefaultRotarySettings.PressureMaxHP]);
    end
  else
    begin
    AddTT_MinPressureEdit.Text := Format('%7.3f',[ToolManager.DefaultRotarySettings.PressureMinLP]);
    AddTT_MaxPressureEdit.Text := Format('%7.3f',[ToolManager.DefaultRotarySettings.PressureMaxLP]);
    end;
  end
else
  begin
  if AddTT_ECountEdit.Text = '???' then
    begin
    AddTT_ECountEdit.Text := '1';
    AddTT_CCountEdit.Text := '6';
    end;
  if  AddTT_CCountEdit.Text = '???' then AddTT_CCountEdit.Text := AddTT_ECountEdit.Text;
  
  AddTT_MinEF_Edit.Text := Format('%7.3f',[ToolManager.DefaultStaticSettings.EFlowMin]);
  AddTT_Max_EFlowEdit.Text := Format('%7.3f',[ToolManager.DefaultStaticSettings.EFlowMax]);
  AddTT_MinNGFlowEdit.Text := Format('%7.3f',[ToolManager.DefaultStaticSettings.NGFlowMin]);
  AddTT_MaxNGFlowEdit.Text := Format('%7.3f',[ToolManager.DefaultStaticSettings.NGFlowMax]);
  AddTT_TPM1Edit.Text := Format('%d',[ToolManager.DefaultStaticSettings.TPM1]);
  AddTT_TPM2Edit.Text := Format('%d',[ToolManager.DefaultStaticSettings.TPM2]);
  AddTT_ExpiryHoursEdit.Text := Format('%d',[ToolManager.DefaultStaticSettings.ExpiryHours]);
  if HPCheckBox.Checked then
    begin
    AddTT_MinPressureEdit.Text := Format('%7.3f',[ToolManager.DefaultStaticSettings.PressureMinHP]);
    AddTT_MaxPressureEdit.Text := Format('%7.3f',[ToolManager.DefaultStaticSettings.PressureMaxHP]);
    end
  else
    begin
    AddTT_MinPressureEdit.Text := Format('%7.3f',[ToolManager.DefaultStaticSettings.PressureMinLP]);
    AddTT_MaxPressureEdit.Text := Format('%7.3f',[ToolManager.DefaultStaticSettings.PressureMaxLP]);
    end;
  end;


end;


procedure TRRToolDataEditor.SetPageTo(DPage: TTabSheet);
begin
SetRotationVisibility;
UniqueToolEditPage.TabVisible := False;
ToolTypeAddPage.TabVisible := False;
NBTypeAddPage.TabVisible := False;
ByTypeEditPage.TabVisible := False;
UTAddPage.TabVisible := False;
Dpage.TabVisible := True;
ByToolEditPage.ActivePage := DPage;
width := 710;
if DPage = ToolTypeAddPage then
  begin
  Width := 820;
  BYTypeTitle.Caption := 'ToolType Test Parameter Entry';
  end else
if DPage = UniqueToolEditPage then
  begin
  BYTypeTitle.Caption := 'Unique Tool Data Entry';
  end else
if DPage = UniqueToolEditPage then
  begin
  BYTypeTitle.Caption := 'ToolType Test Parameter Entry';
  end else
if DPage = UTAddPage then
  begin
  BYTypeTitle.Caption := 'ToolType Test Parameter Entry';
  end else
if DPage = NBTypeAddPage then
  begin
  BYTypeTitle.Caption := 'Non Balluff Tool Parameter Entry';
  Width := 860;
  end else
if DPage = ByTypeEditPage then
  begin
  BYTypeTitle.Caption := 'Tool Type Data edit';
  Top := 20;
  Left := 20;
  Width := 930;
  end;

end;



procedure TRRToolDataEditor.FlushAllToolDataBaseToDisk;
begin
  ByToolData.FlushDataToDisk;
end;

procedure TRRToolDataEditor.FlushByTyeDataBaseToDisk;
begin
  ByTypeData.FlushDataToDisk;
end;

procedure TRRToolDataEditor.ByTypeEditSaveBtnClick(Sender: TObject);
var
ToolName : String;
begin

if (assigned(CurrentToolEntry) and Assigned(ByTypeData)) then
  begin
    ToolName := CurrentToolEntry.ToolType;
    try
    CurrentToolEntry.ElectrodeFlowMax := StrToFloat(BTEFlowMaxEdit.Text);
    CurrentToolEntry.ElectrodeFlowMin := StrToFloat(BTEFlowMinEdit.Text);
    CurrentToolEntry.ElectrodeCount := StrToInt(BTElectrodeCountEdit.Text);
    CurrentToolEntry.ChanelCount := StrToInt(BTChanelCountEdit.Text);
    CurrentToolEntry.NoseGuideFlowMax := StrToFloat(BTNGFlowMaxEdit.Text);
    CurrentToolEntry.NoseGuideFlowMin := StrToFloat(BTNGFlowMinEdit.Text);
    CurrentToolEntry.PressureMax := StrToFloat(BTPressureMaxEdit.Text);
    CurrentToolEntry.PressureMin := StrToFloat(BTPressureMinEdit.Text);
    CurrentToolEntry.RotationDemand := StrToInt(BTRotDemandEdit.Text);
    CurrentToolEntry.RotationMax := StrToFloat(BTRotMaxEdit.Text);
    CurrentToolEntry.RotationMin := StrToFloat(BTRotMinEdit.Text);
    CurrentToolEntry.MCurrentMax := StrToFloat(BTMCurrentMaxEdit.Text);
    CurrentToolEntry.MCurrentMin := StrToFloat(BTMCurrentMinEdit.Text);
    CurrentToolEntry.TPM1Default := StrToInt(BTETPM1DefaultEdit.Text);
    CurrentToolEntry.TPM2Default := StrToInt(BTETPM2DefaultEdit.Text);
    CurrentToolEntry.ExpiryHours := StrToInt(BTETPMHourstEdit.Text);
    ByTypeEditGrid.UpdateRowForToolType(ByTypeData,ToolName);
    except
    Raise Exception.Create('Invalid Data');
    end;
   end;

end;

procedure TRRToolDataEditor.Button2Click(Sender: TObject);
begin
if NBIsRotaryCheck.Checked then
  begin
  if AddNBTT_ECountEdit.Text = '???' then
    begin
    AddNBTT_ECountEdit.Text := '1';
    AddNBTT_ChanelCountEdit.Text := '6';
    end;
  if AddNBTT_ChanelCountEdit.Text = '???' then AddNBTT_ChanelCountEdit.Text := AddNBTT_ECountEdit.Text;
  AddNBTT_MinEF_Edit.Text := Format('%7.3f',[ToolManager.DefaultRotarySettings.EFlowMin]);
  AddNBTT_MaxEF_Edit.Text := Format('%7.3f',[ToolManager.DefaultRotarySettings.EFlowMax]);
  AddNBTT_MinNGFlowEdit.Text := Format('%7.3f',[ToolManager.DefaultRotarySettings.NGFlowMin]);
  AddNBTT_MaxNGFlowEdit.Text := Format('%7.3f',[ToolManager.DefaultRotarySettings.NGFlowMax]);
  AddNBTT_RotSpeedDemand.Text := Format('%7.3f',[ToolManager.DefaultRotarySettings.RotationDemand]);
  AddNBTT_MinRotationEdit.Text := Format('%7.3f',[ToolManager.DefaultRotarySettings.RotationMin]);
  AddNBTT_MaxRotationEdit.Text := Format('%7.3f',[ToolManager.DefaultRotarySettings.RotationMax]);
  AddNBTT_MinMCurrentEdit.Text := Format('%7.3f',[ToolManager.DefaultRotarySettings.MCurrentMin]);
  AddNBTT_MaxMCurrentEdit.Text := Format('%7.3f',[ToolManager.DefaultRotarySettings.MCurrentMax]);
  if HPCheckBox.Checked then
    begin
    AddNBTT_MinPressureEdit.Text := Format('%7.3f',[ToolManager.DefaultRotarySettings.PressureMinHP]);
    AddNBTT_MaxPressureEdit.Text := Format('%7.3f',[ToolManager.DefaultRotarySettings.PressureMaxHP]);
    end
  else
    begin
    AddNBTT_MinPressureEdit.Text := Format('%7.3f',[ToolManager.DefaultRotarySettings.PressureMinLP]);
    AddNBTT_MaxPressureEdit.Text := Format('%7.3f',[ToolManager.DefaultRotarySettings.PressureMaxLP]);
    end;
  end
else
  begin
  if AddNBTT_ECountEdit.Text = '???' then AddNBTT_ECountEdit.Text := '1';
  AddNBTT_MinEF_Edit.Text := Format('%7.3f',[ToolManager.DefaultStaticSettings.EFlowMin]);
  AddNBTT_MaxEF_Edit.Text := Format('%7.3f',[ToolManager.DefaultStaticSettings.EFlowMax]);
  AddNBTT_MinNGFlowEdit.Text := Format('%7.3f',[ToolManager.DefaultStaticSettings.NGFlowMin]);
  AddNBTT_MaxNGFlowEdit.Text := Format('%7.3f',[ToolManager.DefaultStaticSettings.NGFlowMax]);
  if HPCheckBox.Checked then
    begin
    AddNBTT_MinPressureEdit.Text := Format('%7.3f',[ToolManager.DefaultStaticSettings.PressureMinHP]);
    AddNBTT_MaxPressureEdit.Text := Format('%7.3f',[ToolManager.DefaultStaticSettings.PressureMaxHP]);
    end
  else
    begin
    AddNBTT_MinPressureEdit.Text := Format('%7.3f',[ToolManager.DefaultStaticSettings.PressureMinLP]);
    AddNBTT_MaxPressureEdit.Text := Format('%7.3f',[ToolManager.DefaultStaticSettings.PressureMaxLP]);
    end;
  end;

end;

procedure TRRToolDataEditor.Button3Click(Sender: TObject);
var
TypeData : TToolAddData;
begin
if CheckAddedNonBalluffToolDataValid(TypeData)  then
  begin
  TypeData.IsNonBalluff := True;
  NonBallufToolAddMemo.Lines.Clear;
  NewEntry := ByTypeData.AddToolFromAddData(TypeData);
  if assigned(NewEntry) then
    begin
    ModalResult := mrOk;
    end
  else
    begin
    ToolAddMemo.Lines.Add('Problem saving Data');
    ModalResult := mrAbort;
    end;
  end
else
  begin
  ToolAddMemo.Lines.Clear;
  ToolAddMemo.Lines.Add('Data InValid');
  ToolAddMemo.Lines.Add('Please check data entered and retry');
  ToolAddMemo.Lines.Add('or press Cancel');
  end

end;


procedure TRRToolDataEditor.NBIsRotaryCheckClick(Sender: TObject);
begin
SetRotationVisibility;
if not NBIsRotaryCheck.Checked then
  begin
  NBRotarySpeedPanel.Visible := False;
  AddNBTT_MinRotationEdit.Enabled := False;
  AddNBTT_MinRotationEdit.Text := '--';
  AddNBTT_MaxRotationEdit.Enabled := False;
  AddNBTT_MaxRotationEdit.Text := '--';
  AddNBTT_MaxMCurrentEdit.Enabled := False;
  AddNBTT_MaxMCurrentEdit.Text := '--';
  AddNBTT_MinMCurrentEdit.Enabled := False;
  AddNBTT_MinMCurrentEdit.Text := '--';
  end
else
  begin
  NBRotarySpeedPanel.Visible := True;
  AddNBTT_RotSpeedDemand.Text := '???';
  AddNBTT_MinRotationEdit.Enabled := True;
  AddNBTT_MinRotationEdit.Text := '???';
  AddNBTT_MaxRotationEdit.Enabled := True;
  AddNBTT_MaxRotationEdit.Text := '???';
  AddNBTT_MaxMCurrentEdit.Enabled := True;
  AddNBTT_MaxMCurrentEdit.Text := '???';
  AddNBTT_MinMCurrentEdit.Enabled := True;
  AddNBTT_MinMCurrentEdit.Text := '???';
  end;
end;


procedure TRRToolDataEditor.Button4Click(Sender: TObject);
begin
ModalResult := MRCancel;
Close;
end;


initialization
  TAbstractFormPlugin.AddPlugin('RRToolDataEditor', TRRToolDataEditor);
end.
