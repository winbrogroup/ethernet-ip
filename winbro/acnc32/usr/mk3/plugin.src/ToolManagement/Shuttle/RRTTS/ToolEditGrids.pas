unit ToolEditGrids;

interface
uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, ValEdit, ToolData, OCTUtils, ExtCtrls, StdCtrls, Buttons,
  IFormInterface, INamedInterface, C6001, IniFiles, NamedPlugin, PluginInterface,
  AbstractFormPlugin, CNCTypes, OCTTypes, TTSByTypeData,TTSDataByUniqueTool,
  ToolMonitor, ComCtrls;
const
FloatFormat = '%.1f';
Type

TByToolTypeGrid = class(TStringGrid)
  private
    procedure UpDateCellsatIndex(I: INteger;ToolData : TTTSByToolTypeData);
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure LoadFromDB(ByTypeData : TByToolTypeList);
    procedure UpdateRowForToolType(ByTypeData : TByToolTypeList;ToolString : String);
    procedure UpdateRowAtIndex(ByTypeData : TByToolTypeList;TIndex : INteger);
    procedure ShowIndexAtTop(SIndex : Integer);
  end;

TUniqueToolGrid = class(TStringGrid)
   private

   public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure LoadFromDB(UniqueToolData : TUniqueToolDataList);
    procedure ShowIndexAtTop(SIndex : Integer);
   end;

implementation

{ TByToolTypeGrid }

procedure TByToolTypeGrid.UpDateCellsatIndex(I : INteger;ToolData : TTTSByToolTypeData);
begin
    Cells[0,I+2] := Tooldata.ToolType;
    Cells[1,I+2] := Format(FloatFormat,[Tooldata.NoseGuideFlowMin]);
    Cells[2,I+2] := Format(FloatFormat,[Tooldata.NoseGuideFlowMax]);
    Cells[3,I+2] := Format(FloatFormat,[Tooldata.ElectrodeFlowMin]);
    Cells[4,I+2] := Format(FloatFormat,[Tooldata.ElectrodeFlowMax]);
    Cells[5,I+2] := IntToStr(Tooldata.ElectrodeCount);
    Cells[6,I+2] := IntToStr(Tooldata.ChanelCount);
    Cells[7,I+2] := Format(FloatFormat,[Tooldata.PressureMin]);
    Cells[8,I+2] := Format(FloatFormat,[Tooldata.PressureMax]);
    Cells[9,I+2] := IntToStr(Tooldata.RotationDemand);
    Cells[10,I+2] := Format(FloatFormat,[Tooldata.RotationMin]);
    Cells[11,I+2] := Format(FloatFormat,[Tooldata.RotationMax]);
    Cells[12,I+2] := Format(FloatFormat,[Tooldata.MCurrentMin]);
    Cells[13,I+2] := Format(FloatFormat,[Tooldata.MCurrentMax]);
    Cells[14,I+2] := IntToStr(Tooldata.TPM1Default);
    Cells[15,I+2] := IntToStr(Tooldata.TPM2Default);
    Cells[16,I+2] := IntToStr(Tooldata.ExpiryHours);
end;

procedure TByToolTypeGrid.LoadFromDB(ByTypeData : TByToolTypeList);
var
I : Integer;
ToolData : TTTSByToolTypeData;
begin
RowCount := ByTypeData.Count+2 ;
if RowCount < 3 then RowCount := 3;
FixedRows := 2;
DefaultColWidth := 50;
ColWidths[5] := 35;
ColWidths[0] := 70;
ColWidths[11] := 55;
ColWidths[12] := 55;
OPtions := [goFixedVertLine,goFixedHorzLine,goVertLine,goHorzLine];
if assigned(ByTypeData) then
  begin
  For I := 0  to ByTypeData.Count-1 do
    begin
    ToolData := ByTypeData.Tool[i];
    UpdateCellsAtIndex(I,ToolData);
    end;
  end
end;

constructor TByToolTypeGrid.Create(AOwner: TComponent);
begin
  inherited;
  RowCount := 3;
  FixedRows := 2;
  Width := 655;
  ScrollBars := ssVertical;
  DefaultRowHeight := 20;
  ColCount := 17;
  Cells[0,0] := '      Tool';
  Cells[0,1] := '      Type';
  Cells[1,0] := ' NG Flow';
  Cells[1,1] := '     Min';
  Cells[2,0] := ' NG Flow';
  Cells[2,1] := '   Max';
  Cells[3,0] := 'Elec Flow';
  Cells[3,1] := '      Min';
  Cells[4,0] := 'Elec Flow';
  Cells[4,1] := '     Max';
  Cells[5,0] := ' Elect.';
  Cells[5,1] := 'Count';
  Cells[6,0] := ' Chanel';
  Cells[6,1] := '  Count';
  Cells[7,0] := ' Pressure';
  Cells[7,1] := '     Min';
  Cells[8,0] := ' Pressure';
  Cells[8,1] := '     Max';
  Cells[9,0] := '  Rotn.';
  Cells[9,1] := ' Demand';
  Cells[10,0] := '  Rotn.';
  Cells[10,1] := '    Min';
  Cells[11,0] := '  Rotn.';
  Cells[11,1] := '    Max';
  Cells[12,0] := 'Motor Cur';
  Cells[12,1] := '     Min';
  Cells[13,0] := 'Motor Cur';
  Cells[13,1] := '     Max';
  Cells[14,0] := '     TPM1';
  Cells[15,0] := '     TPM2';
  Cells[16,0] := '     TPM';
  Cells[16,1] := '    Hours';
end;
//Tooltype,n_min_flow,n_max_flow,e_min_flow,e_max_flow,e_Count,Chanel_count,Pressure_min,Pressure_max,rotation_min,rotation_max,MCurrent_min,MCurrent_max,TPM1,TPM2,ExpiryHours

destructor TByToolTypeGrid.Destroy;
begin

  inherited;
end;

procedure TByToolTypeGrid.ShowIndexAtTop(SIndex: Integer);
begin
if SIndex < RowCount then TopRow := SIndex+2;
end;

procedure TByToolTypeGrid.UpdateRowAtIndex(ByTypeData : TByToolTypeList;TIndex: INteger);
var
ToolData : TTTSByToolTypeData;
begin
  ToolData := ByTypeData.Tool[TIndex];
  if assigned(ToolData) then
    begin
    UpDateCellsatIndex(Tindex,ToolData);

    end;
end;

procedure TByToolTypeGrid.UpdateRowForToolType(ByTypeData : TByToolTypeList;ToolString: String);
var
TIndex : INteger;
begin
TIndex := ByTypeData.indexof(ToolString);
if Tindex >=0 then
  begin
  UpdateRowAtIndex(ByTypeData,Tindex);
  ByTypeData.FlushDataToDisk;
  end;
end;


{ TUniqueToolGrid }

constructor TUniqueToolGrid.Create(AOwner: TComponent);
begin
  inherited;
  RowCount := 3;
  FixedRows := 2;
  Width := 555;
  ScrollBars := ssVertical;
  DefaultRowHeight := 20;
  ColCount := 8;
  Cells[0,0] := '      Tool';
  Cells[0,1] := '      Type';
  Cells[1,0] := ' Serial';
  Cells[1,1] := ' Number';
  Cells[2,0] := ' X Tool';
  Cells[2,1] := ' Offset';
  Cells[3,0] := 'Y Tool';
  Cells[3,1] := 'Offset';
  Cells[4,0] := 'Z Tool';
  Cells[4,1] := 'Offset';
  Cells[5,0] := 'C Tool';
  Cells[5,1] := 'Offset';
  Cells[6,0] := 'Nose Guide';
  Cells[6,1] := 'Serial #';
  Cells[7,0] := 'Register';
  Cells[7,1] := '  Date';
end;

destructor TUniqueToolGrid.Destroy;
begin

  inherited;
end;

procedure TUniqueToolGrid.LoadFromDB(UniqueToolData: TUniqueToolDataList);
var
I : Integer;
ToolData : TTTSByUniqueToolData;
begin
RowCount := UniqueToolData.Count+2 ;
if RowCount < 3 then RowCount := 3;
FixedRows := 2;
DefaultColWidth := 50;
ColWidths[0] := 70;
ColWidths[6] := 60;
ColWidths[7] := 90;

OPtions := [goFixedVertLine,goFixedHorzLine,goVertLine,goHorzLine];
if assigned(UniqueToolData) then
  begin
  For I := 0  to UniqueToolData.Count-1 do
    begin
    ToolData := UniqueToolData.Tool[i];
    Cells[0,I+2] := Tooldata.ToolType;
    Cells[1,I+2] := IntToStr(Tooldata.SerialNumber);
    Cells[2,I+2] := Format(FloatFormat,[Tooldata.UniqueToolOffsets[0]]);
    Cells[3,I+2] := Format(FloatFormat,[Tooldata.UniqueToolOffsets[1]]);
    Cells[4,I+2] := Format(FloatFormat,[Tooldata.UniqueToolOffsets[2]]);
    Cells[5,I+2] := Format(FloatFormat,[Tooldata.UniqueToolOffsets[3]]);

    Cells[6,I+2] := IntToStr(Tooldata.NGSerialNumber);
    Cells[7,I+2] := DateTimeToStr(Tooldata.RegistrationTime)
    end;
  end
end;

procedure TUniqueToolGrid.ShowIndexAtTop(SIndex: Integer);
begin
if SIndex < RowCount then TopRow := SIndex+2;
end;

end.
