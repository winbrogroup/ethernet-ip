library RRTTS_Shuttle;



{%File 'C:\tts\Profile\Live\Softkey.cfg'}
{%File 'C:\tts\data\DB\IndividualToolData'}
{%File 'C:\tts\Profile\Live\RRTTS_Shuttle.cfg'}
{%File 'C:\tts\Profile\Live\TTSDisplay.Cfg'}
{%File 'C:\tts\Profile\Live\GeneralDisplay.Cfg'}
{%File 'C:\tts\Profile\Live\ManualJogMode.Cfg'}
{%File 'C:\tts\Profile\Live\ManualAppspec.Cfg'}
{%File 'C:\tts\data\DB\TTSResultData'}
{%File 'C:\tts\data\DB\ByToolTypeData'}
{%File 'C:\tts\Profile\Live\Softpanel.cfg'}
{%File 'C:\tts\Results\Results.txt'}
{%File 'C:\tts\data\DB\TestResults'}
{%File 'C:\tts\data\DB\Results'}
{%File 'C:\tts\data\DB\ResultStatsByType'}
{%File 'C:\tts\Profile\cnc.ini'}
{%File 'C:\tts\Profile\Live\TTSManualDisplay.Cfg'}
{%File 'C:\tts\Profile\language.csv'}
{%File 'C:\tts\Profile\Live\language.csv'}
{%File 'C:\tts\Profile\Plc\PlcGlobal.h'}
{%File 'C:\tts\data\DB\FTTResults'}
{%File 'C:\tts\Profile\Plc\Unit2.Plc'}

uses
  SysUtils,
  Classes,
  C6001 in 'C6001.pas',
  OpTable in '..\..\lib\OpTable.pas',
  ToolOKDlg in 'ToolOKDlg.pas' {IsToolOK},
  AllToolDB in '..\..\lib\AllToolDB.pas',
  OffsetData in '..\..\lib\OffsetData.pas',
  ToolForm in 'ToolForm.pas' {ToolManager},
  ToolMonitor in 'ToolMonitor.pas',
  ToolRegistration in 'ToolRegistration.pas' {RegForm},
  ToolDataBaseEditor in 'ToolDataBaseEditor.pas' {RRToolDataEditor},
  ToolEditGrids in 'ToolEditGrids.pas',
  TTSByTypeData in 'MDTHandlers\TTSByTypeData.pas',
  TTSDataByUniqueTool in 'MDTHandlers\TTSDataByUniqueTool.pas',
  ResultsForm in 'ResultsForm.pas' {ToolResultsForm},
  TTSResults in 'MDTHandlers\TTSResults.pas';

{$R *.res}

begin
end.
