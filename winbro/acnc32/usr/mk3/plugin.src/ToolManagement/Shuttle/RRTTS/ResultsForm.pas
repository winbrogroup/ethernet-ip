unit ResultsForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs,
  IFormInterface, INamedInterface, C6001, IniFiles, NamedPlugin, PluginInterface,
  AbstractFormPlugin, IToolLife, CNCTypes, OCTTypes, ACNC_BaseKeyBoards,
  ToolDB, AllToolDB, Math, ToolMonitor, ComCtrls, Grids,ToolEditGrids,
  OCTUtils,ToolData, StdCtrls, TTSByTypeData,TTSDataByUniqueTool,TTSResults,
  ExtCtrls, Buttons;
const
RagGreen  = clWebLightGreen;
RagRed    = clRed;
RagAmber  = clYellow;
RagPurple = $00F3D1F2;

type
  TResultShowModes = (trsNormalExecute,trsFilteredExecute);

  TRagCellObject = class(Tobject)
    RagStatus : Integer;
    constructor Create;
    end;

  TRagGrid = class(TStringGrid)
    private
    procedure AddRow(InitilColour : TColor);
    procedure RemoveRow;
    procedure DrawCell(ACol: Integer; ARow: Integer;ARect: TRect; AState: TGridDrawState);override;
    procedure SetRowCountTo(RCount : Integer;DefColor : TColor);
    public
    constructor Create(AOwner: TComponent); override;
    end;

  TFlowResultGrid = class(TRagGrid)
    private
      LastRow : Integer;
      FOnRowChange: TNotifyEvent;
      IsUpdating : Boolean;
      procedure RagCellSelected(Sender: TObject; ACol, ARow: Longint;var CanSelect: Boolean);

    public
      SelectedToolID : String;
      SelectedSN : Integer;
      constructor Create(AOwner: TComponent); override;
      function AddResult(FResult : TTTSToolResult): Boolean;
      procedure ClearDown;
      procedure InitForDisplay;
      property OnRowChange : TNotifyEvent read FOnRowChange write FOnRowChange;

    end;



  TToolResultsForm = class(TInstallForm)
    Panel1: TPanel;
    Panel2: TPanel;
    Button1: TButton;
    Button2: TButton;
    ToolTypeEdit: TEdit;
    Button3: TButton;
    Button4: TButton;
    ToolSerialEdit: TEdit;
    procedure Button4Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
     RagGrid : TFlowResultGrid;
     ShowMode : TResultShowModes;
     procedure ClearDown;
     procedure RagRowChanged(Sender : TObject);
    { Private declarations }
  public
    ResultsList : TToolResultList; // externally set
    FilterTool : String;
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure Install; override;
    procedure FinalInstall; override;
    procedure Execute; override;
    procedure Shutdown; override;
    procedure DisplaySweep; override;
    Function ShowWithFilter(FilterStr : String):Boolean;
    procedure Hide;
    function AddResultToGrid(FResult : TTTSToolResult): Boolean;
    function DisplayLastNResultsForToolType(ToolType : String; ResultCount : Integer): Integer; //
    function DisplayLastNResultsForToolTypeAndSerial(ToolType : String;SerialNumber,ResultCount : Integer): Integer; //
    function DisplayLastNResultsAnyTool(ResultCount : Integer): Integer; //
  end;

var
  ToolResultsForm: TToolResultsForm;

implementation

{$R *.dfm}

{ TToolResultsForm }

procedure TToolResultsForm.Shutdown;
begin
  inherited;

end;

procedure TToolResultsForm.DisplaySweep;
begin
  inherited;

end;

procedure TToolResultsForm.FinalInstall;
begin
  inherited;
  RagGrid := TFlowResultGrid.Create(nil);
  with RagGrid do
    begin
    Parent := Panel2;
    Height := 300;
    Align := alBottom;
    OnRowChange := RagRowChanged;
    end;

end;

procedure TToolResultsForm.Install;
begin
  inherited;
  ToolResultsForm := Self;
  end;

procedure TToolResultsForm.Execute;
begin
  inherited;
  ShowModal;
end;

constructor TToolResultsForm.Create(AOwner: TComponent);
begin
  inherited;
  FilterTool := '';
end;

destructor TToolResultsForm.Destroy;
begin
  inherited;

end;

procedure TToolResultsForm.FormShow(Sender: TObject);
begin
Width := 920;
Height := 650;
Left := 20;
Top := 80;
RagGrid.Height := 420;
case ShowMode of
  trsNormalExecute:
  begin
  DisplayLastNResultsAnyTool(15);
  end;

  trsFilteredExecute:
  begin
  DisplayLastNResultsForToolType(FilterTool,15)
  end;

  end;
  RagGrid.InitForDisplay;
end;

function TToolResultsForm.AddResultToGrid(FResult: TTTSToolResult): Boolean;
begin
Result := RagGrid.AddResult(FResult);
end;


function TToolResultsForm.DisplayLastNResultsForToolTypeAndSerial(ToolType: String; SerialNumber, ResultCount: Integer): Integer;
var
FResult: TTTSToolResult;
I : INteger;
begin
Result := 0; // Number of actual Results
if assigned(ResultsList) then
  begin
  ClearDown;
  Result := ResultsList.FilterForToolIDandSerial(ToolType,SerialNumber,ResultCount);
  if Result > 0 then
    begin
    For i := 0 to Result-1 do
      begin
      FResult := ResultsList.FilteredResults[i];
      AddResultToGrid(FResult);
      end
    end;
  end;
end;

function TToolResultsForm.DisplayLastNResultsForToolType(ToolType: String;ResultCount: Integer): Integer;
var
FResult: TTTSToolResult;
I : INteger;
begin
Result := 0; // Number of actual Results
if assigned(ResultsList) then
  begin
  ClearDown;
  if ToolType = '' then
    begin
    Result := DisplayLastNResultsAnyTool(ResultCount);
    end
  else
    begin
    Result := ResultsList.FilterForToolID(ToolType,ResultCount);
    end;

  if Result > 0 then
    begin
    For i := 0 to Result-1 do
      begin
      FResult := ResultsList.FilteredResults[i];
      AddResultToGrid(FResult);
      end
    end;
  end;
end;

function TToolResultsForm.DisplayLastNResultsAnyTool(ResultCount: Integer): Integer;
var
FResult: TTTSToolResult;
I : INteger;
begin
Result := 0; // Number of actual Results
if assigned(ResultsList) then
  begin
  ClearDown;
  Result := ResultsList.FilterLastNResults(ResultCount);
  if Result > 0 then
    begin
    For i := 0 to Result-1 do
      begin
      FResult := ResultsList.FilteredResults[i];
      AddResultToGrid(FResult);
      end
    end;
  end;
end;

procedure TToolResultsForm.ClearDown;
begin
RagGrid.ClearDown;
end;

function TToolResultsForm.ShowWithFilter(FilterStr: String):Boolean;
begin
Result := True;
try
if not Visible then
  begin
  FilterTool := FilterStr;
  ShowMode := trsFilteredExecute;
  ShowModal;
  ShowMode := trsNormalExecute;
  end;
except
Result := False;
end;
end;


procedure TToolResultsForm.RagRowChanged(Sender : TObject);
begin
ToolTypeEdit.Text := RagGrid.SelectedToolID;
ToolSerialEdit.Text := IntToStr(RagGrid.SelectedSN);
end;

procedure TToolResultsForm.Hide;
begin
ModalResult := mrCancel;
Close;
end;

{ TRagGrid }
procedure TRagGrid.AddRow(InitilColour: TColor);
var
I :integer;
NewObject : TRagCellObject;
begin
RowCount := RowCount+1;
For I := 0 to ColCount -1 do
  begin
  NewObject := TRagCellObject.Create;
  Objects[I,RowCount-1]:= NewObject;
  end;
If RowCount > 1 then FixedRows := 1;
end;

constructor TRagGrid.Create(AOwner: TComponent);
begin
inherited;
RowCount := 1;
FixedRows := 0;
FixedCols := 0;
end;

procedure TRagGrid.DrawCell(ACol, ARow: Integer; ARect: TRect;
  AState: TGridDrawState);
var
RagObject : TRagCellObject;
begin
  if (ARow > 0) and (ACol > 0) then
    begin
    RagObject := TRagCellObject(Objects[ACol,ARow]);
    if assigned(RagObject) then
      begin
      case RagObject.RagStatus of
      0:
      begin
      Canvas.Brush.Color := RagRed;
      end;

      1:
      begin
      Canvas.Brush.Color := RagGreen;
      end;

      2:
      begin
      Canvas.Brush.Color := RagAmber;
      end;

      3:
      begin
      Canvas.Brush.Color := RagPurple;
      end;

    else
      begin
      Canvas.Brush.Color := clCream;
      end;
    end; // case

  Canvas.FillRect(ARect);
  end
 else
    begin
    inherited;
    end
 end;
 inherited;
end;

procedure TRagGrid.SetRowCountTo(RCount: Integer; DefColor : TColor);
begin
if RowCount < RCount then
  begin
  while RowCount < RCount do
    begin
    AddRow(DefColor);
    end
  end;
if RowCount > RCount then
  begin
  while  RowCount > RCount do
    begin
    RemoveRow;
    end
  end;
end;

procedure TRagGrid.RemoveRow;
var
I :integer;
DelObject : TRagCellObject;
begin
For I := 0 to ColCount -1 do
  begin
  DelObject := TRagCellObject(Objects[I,RowCount-1]);
  DelObject.Free;
  end;
RowCount := RowCount-1;
end;

{ TRagCellObject }

constructor TRagCellObject.Create;
begin
    RagStatus := -1;
end;

{ TFlowResultGrid }
function TFlowResultGrid.AddResult(FResult: TTTSToolResult): Boolean;
var
RI : Integer;
RagObj : TRagCellObject;
DateStr,TimeStr : String;
begin
Result := False;
if assigned(FResult) then
  begin
  AddRow(clCream);
  RI := RowCount-1;
  Cells[0,RI] := FResult.ToolType;
  Cells[1,RI] := IntToStr(FResult.SerialNumber);
  // E Count
  Cells[2,RI] := Format('%d',[FResult.ElectrodeCount]);
  RagObj := TRagCellObject(Objects[2,RI]);
  RagObj.RagStatus := Fresult.ElectrodeCountPass;
  // Clamp Function

  RagObj := TRagCellObject(Objects[3,RI]);
  RagObj.RagStatus := FResult.ClampsOK;
  case FResult.ClampsOK of
  1:
    begin
    Cells[3,RI] := 'OK';
    end;
  3:
    begin
    Cells[3,RI] := 'N/A';
    end;
  else
    begin
    Cells[3,RI] := 'Fail';
    end;
  end; //case
  // E Flow
  Cells[4,RI] := Format('%7.3f',[FResult.ElectrodeFlow]);
  RagObj := TRagCellObject(Objects[4,RI]);
  RagObj.RagStatus := Fresult.ElectrodeFlowStatus;
  // NG Flow
  Cells[5,RI] := Format('%7.3f',[FResult.NoseGuideFlow]);
  RagObj := TRagCellObject(Objects[5,RI]);
  RagObj.RagStatus := Fresult.NoseGuideFlowStatus;
  // Rotation
  Cells[6,RI] := Format('%5.1f',[FResult.Rotation]);
  RagObj := TRagCellObject(Objects[6,RI]);
  RagObj.RagStatus := Fresult.RotationStatus;
  // Motor Current
  Cells[7,RI] := Format('%5.1f',[FResult.MCurrent]);
  RagObj := TRagCellObject(Objects[7,RI]);
  RagObj.RagStatus := Fresult.MCurrentStatus;
  // Date
  DateStr:= Format('  %d-%d-%d',[Fresult.Day,FResult.Month,FResult.Year]);
  Cells[8,RI] := DateStr;
  TimeStr:= Format('  %2d:%2d:%2d',[Fresult.Hour,FResult.Minute,FResult.Second]);
  Cells[9,RI] := TimeStr;
  end;
end;

procedure TFlowResultGrid.ClearDown;
var
I : INteger;
begin
IsUpdating := True;
try
SetRowCountTo(1,clCream);
For I := 0 to ColCount-1 do
  begin
  Cells[I,1] := '';
  end;
finally
IsUpdating := False;
end
end;

constructor TFlowResultGrid.Create(AOwner: TComponent);
begin
  inherited;
  ColCount := 10;
  DefaultColWidth := 90;
  Font.Size := 12;

  RowCount := 1;
  AddRow(clCream);
  FixedRows := 1;
  Cells[0,0] := 'Tool';
  Cells[1,0] := 'S/N';
  Cells[2,0] := 'Elctrds.';
  Cells[3,0] := 'Refeed';
  Cells[4,0] := 'E Flow';
  Cells[5,0] := 'NG Flow';
  Cells[6,0] := 'Rotn.';
  Cells[7,0] := 'M Current';
  Cells[8,0] := 'Date';
  Cells[9,0] := 'Time';
  ColWidths[0] := 120;
  ColWidths[1] := 50;
  ColWidths[2] := 60;
  ColWidths[3] := 60;
  ColWidths[8] := 120;
  ColWidths[9] := 120;

  OnSelectCell := RagCellSelected;
  SelectedToolID := '';
  SelectedSN := 0;
  LastRow := 0;
end;

procedure TToolResultsForm.Button2Click(Sender: TObject);
begin
  DisplayLastNResultsForToolType(ToolTypeEdit.Text,15);
end;

procedure TToolResultsForm.Button3Click(Sender: TObject);
begin
  DisplayLastNResultsAnyTool(15);
end;

procedure TToolResultsForm.Button4Click(Sender: TObject);
var
SN : Integer;
begin
  Sn := StrToIntDef(ToolSerialEdit.Text,1);
  ToolSerialEdit.Text := IntToStr(SN);
  DisplayLastNResultsForToolTypeAndSerial(ToolTypeEdit.Text,SN,15);
end;

procedure TFlowResultGrid.InitForDisplay;
var
Temp : Boolean;
begin
Temp := False;
if assigned(FOnRowChange) then
  begin
  if RowCount > 1 then
    begin
    RagCellSelected(nil,0,1,Temp);
    end;
  end
end;

procedure TFlowResultGrid.RagCellSelected(Sender: TObject; ACol, ARow: Integer; var CanSelect: Boolean);
begin
if ARow > 0 then
  begin
  if ARow <> LastRow then
    begin
    SelectedToolId := Cells[0,Arow];
    if SelectedToolId <> '' then
      begin
      SelectedSN := StrToIntDef(Cells[1,Arow],0);
      end;
    if assigned(FOnRowChange) and not IsUpdating then
      begin
      FOnRowChange(self);
      end;
    end;
  end
end;

initialization
  TAbstractFormPlugin.AddPlugin('ToolResultsForm', TToolResultsForm);

end.
