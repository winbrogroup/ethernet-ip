unit TTSByTypeData;

interface
uses SysUtils, Classes, IStringArray, NamedPlugin, OCTTypes,ToolData;

const
  FloatFormat = '%.1f';
  ToolTypeField = 'Tooltype';
  NoseGuideFlowMinField = 'n_min_flow';
  NoseGuideFlowMaxField = 'n_max_flow';
  ElectrodeFlowMinField = 'e_min_flow';
  ElectrodeFlowMaxField = 'e_max_flow';
  ElectrodeCountField = 'e_Count';
  ChanelCountField = 'Chanel_Count';
  PressureMinField = 'Pressure_min';
  PressureMaxField = 'Pressure_max';
  RotationDemandField = 'rotation_dmd';
  RotationMinField = 'rotation_min';
  RotationMaxField = 'rotation_max';
  MCurrentMinField = 'MCurrent_min';
  MCurrentMaxField = 'MCurrent_max';
  IsHPField        = 'IsHPTool';
  TPM1DefaultField = 'TPM1';
  TPM2DefaultField = 'TPM2';
  ExpiryHoursField = 'ExpiryHours';
  HypertacCodeField = 'HTCode';
  IsNonBalluffField = 'NB';
  IsRotaryField    = 'IsRotary';
Type

TToolAddData  = record
    ToolType : WideString;
    ElectrodeCount   : INteger;
    ChanelCount      : INteger;
    ElectrodeFlowMin : Double ;
    ElectrodeFlowMax : Double ;
    NoseGuideFlowMin : Double ;
    NoseGuideFlowMax : Double ;
    PressureMin      : Double ;
    PressureMax      : Double ;
    RotationDemand   : Integer ;
    RotationMin      : Double ;
    RotationMax      : Double ;
    MCurrentMin      : Double ;
    MCurrentMax      : Double ;
    IsHpTool         : Boolean;
    IsRotaryTool     : Boolean;
    TPM1Default      : Integer;
    TPM2Default      : Integer;
    ExpiryHoursDefault : Integer;
    NonBallufHypertacCode : Integer;
    IsNonBalluff : Boolean;
  end;

  TByToolTypeList = class;
  TTTSByToolTypeData = class(TObject)
  private
    Index : Integer;
    Host : TByToolTypeList;

    FToolType  : WideString;
    FNGFlowMin : Double;
    FNGFlowMax : Double;
    FEFlowMin : Double;
    FEFlowMax : Double;
    FElectrodeCount : Integer;
    FChanelCount: INteger;
    FPressureMin : Double;
    FPressureMax : Double;

    FRotationDemand: Integer;
    FRotationMin : Double;
    FRotationMax : Double;
    FMCurrentMax : Double;
    FMCurrentMin : Double;
    FTPM1 : INteger;
    FTPM2 : INteger;
    FExpiryHours : INteger;
    FIsRotary : Boolean;
    FIsHpTool : Boolean;
    FHypertacCode: INteger;
    FIsNonBalluff: Integer;
    procedure SetToolTYpe(const Value: WideString);
    procedure SetEFlowMax(const Value: Double);
    procedure SetEFlowMin(const Value: Double);
    procedure SetMCurrentMax(const Value: Double);
    procedure SetMCurrentMin(const Value: Double);
    procedure SetNGFlowMax(const Value: Double);
    procedure SetNGFlowMin(const Value: Double);
    procedure SetRotationMax(const Value: Double);
    procedure SetRotationDemand(const Value: Integer);
    procedure SetRotationMin(const Value: Double);
    procedure SetPressureMax(const Value: Double);
    procedure SetPressureMin(const Value: Double);
    procedure SetElectrodeCount(const Value: INteger);
    procedure SetIsHP(const Value: Boolean);
    procedure SetIsRotary(const Value: Boolean);
    procedure SetExpiryHours(const Value: Integer);
    procedure SetTPM1Default(const Value: Integer);
    procedure SetTPM2Default(const Value: Integer);
    procedure SetHypertacCode(const Value: INteger);
    procedure SetFIsNonBalluff(const Value: Integer);
    procedure SetChanelCount(const Value: INteger);

  protected
    procedure RebuildInternalValues(AIndex : Integer);
    procedure Copy(Src: TTTSByToolTypeData);
  public
    constructor Create(AHost : TByToolTypeList; AIndex : Integer);
    destructor Destroy; override;
    property ToolType : WideString read FToolType write SetToolTYpe;
    property NoseGuideFlowMin : Double read FNGFlowMin write SetNGFlowMin;
    property NoseGuideFlowMax : Double read FNGFlowMax write SetNGFlowMax;
    property ElectrodeFlowMin : Double read FEFlowMin write SetEFlowMin;
    property ElectrodeFlowMax : Double read FEFlowMax write SetEFlowMax;
    property ElectrodeCount   : INteger read FElectrodeCount write SetElectrodeCount;
    property ChanelCount      : INteger read FChanelCount write SetChanelCount;

    property PressureMin      : Double read FPressureMin write SetPressureMin;
    property PressureMax      : Double read FPressureMax write SetPressureMax;
    property RotationDemand   : Integer read FRotationDemand write SetRotationDemand;
    property RotationMin      : Double read FRotationMin write SetRotationMin;
    property RotationMax      : Double read FRotationMax write SetRotationMax;
    property MCurrentMin      : Double read FMCurrentMin write SetMCurrentMin;
    property MCurrentMax      : Double read FMCurrentMax write SetMCurrentMax;
    property IsRotary         : Boolean read FIsRotary write SetIsRotary;
    property IsHPTool         : Boolean read FIsHpTool write SetIsHP;
    property TPM1Default      : Integer read FTPM1 write SetTPM1Default;
    property TPM2Default      : Integer read FTPM2 write SetTPM2Default;
    property HypertacCode     : INteger read FHypertacCode write SetHypertacCode;
    property aIsNonBalluff     : Integer read FIsNonBalluff write SetFIsNonBalluff;
    property ExpiryHours      : Integer read FExpiryHours write SetExpiryHours;
  end;

 TByToolTypeList = class(TAbstractStringData)
    ToolList : TList;
  private
    procedure FlushData(Table: IACNC32StringArray);
    function GetTool(index: Integer): TTTSByToolTypeData;
    procedure CleanTable;
    procedure CreateWithName(TableName: string; Clean: Boolean);
  protected
    procedure RebuildSingleValue(Row, Col :Integer); override;
    procedure RebuildInternalValues; override;
    procedure CreateTable;
    procedure RebuildIndex;
  public
    constructor Create; overload;
    constructor Create(TableName: string); overload;
    destructor Destroy; override;
    function AddTool : TTTSByToolTypeData;
    function AddToolFromAddData(ToolData : TToolAddData):TTTSByToolTypeData;
    function IndexOf(ToolType : String) : Integer;
    function IndexofToolWithHypertacCode(HTCode: Integer) : Integer;

    function Count : Integer;
    property Tool[index : Integer] : TTTSByToolTypeData read GetTool; default;
    procedure Clean; override;
    procedure RemoveTool(T : String);
    procedure CopyTool(Target, Src: TTTSByToolTypeData);
    function Find(ToolType: string): TTTSByToolTypeData;
    function FindByHypertacCode(HTCode : Integer): TTTSByToolTypeData;
    function ToolExists(ToolType: string): Boolean;
    function ToolNameFromHypertacCode(HTCode : INteger):String;
    procedure FlushDataToDisk;
  end;

implementation
constructor TTTSByToolTypeData.Create(AHost: TByToolTypeList; AIndex: Integer);
begin
  inherited Create;
  Index := AIndex;
  Host := AHost;
end;

procedure TTTSByToolTypeData.RebuildInternalValues(AIndex: Integer);
var
tmp : Integer;
begin
  Index := AIndex;
  FToolType  := Host.GetFieldString(Index, ToolTypeField);
  FNGFlowMin := Host.GetFieldDouble(Index, NoseGuideFlowMinField);
  FNGFlowMax := Host.GetFieldDouble(Index, NoseGuideFlowMaxField);
  FEFlowMin := Host.GetFieldDouble(Index, ElectrodeFlowMinField);
  FEFlowMax := Host.GetFieldDouble(Index, ElectrodeFlowMaxField);
  FElectrodeCount := Host.GetFieldInteger(Index,ElectrodeCountField);
  FChanelCount := Host.GetFieldInteger(Index,ChanelCountField);
  FPressureMin := Host.GetFieldDouble(Index, PressureMinField);
  FPressureMax := Host.GetFieldDouble(Index, PressureMaxField);
  FRotationDemand := Host.GetFieldInteger(Index,RotationDemandField);
  FRotationMin := Host.GetFieldDouble(Index, RotationMinField);
  FRotationMax := Host.GetFieldDouble(Index, RotationMaxField);
  FMCurrentMin := Host.GetFieldDouble(Index, MCurrentMinField);
  FMCurrentMax := Host.GetFieldDouble(Index, MCurrentMaxField);
  FTPM1        := Host.GetFieldInteger(Index, TPM1Field);
  FTPM2        := Host.GetFieldInteger(Index, TPM2Field);
  FExpiryHours := Host.GetFieldInteger(Index, ExpiryHoursField);
  FHypertacCode:= Host.GetFieldInteger(Index, HypertacCodeField);
  tmp :=  Host.GetFieldInteger(Index, IsRotaryField);
  FIsRotary := tmp <> 0;
  tmp :=  Host.GetFieldInteger(Index, IsHPField);
  FIsHpTool := tmp <> 0;
  FIsNonBalluff :=  Host.GetFieldInteger(Index, IsNonBalluffField);

  end;

destructor TTTSByToolTypeData.Destroy;
begin
  inherited;
end;

procedure TTTSByToolTypeData.SetToolTYpe(const Value: WideString);
begin
  FToolType := Value;
  Host.SetFieldData(Index,ToolTypeField,Value);
end;

procedure TTTSByToolTypeData.SetNGFlowMax(const Value: Double);
begin
  FNGFlowMax := Value;
  Host.SetFieldData(Index,NoseGuideFlowMaxField,Format(FloatFormat,[Value]));
end;

procedure TTTSByToolTypeData.SetMCurrentMax(const Value: Double);
begin
  FMCurrentMax := Value;
  Host.SetFieldData(Index,MCurrentMaxField,Format(FloatFormat,[Value]));
end;

procedure TTTSByToolTypeData.SetEFlowMax(const Value: Double);
begin
  FEFlowMax := Value;
  Host.SetFieldData(Index,ElectrodeFlowMaxField,Format(FloatFormat,[Value]));
end;

procedure TTTSByToolTypeData.SetNGFlowMin(const Value: Double);
begin
  FNGFlowMin := Value;
  Host.SetFieldData(Index,NoseGuideFlowMinField,Format(FloatFormat,[Value]));
end;

procedure TTTSByToolTypeData.SetMCurrentMin(const Value: Double);
begin
  FMCurrentMin := Value;
  Host.SetFieldData(Index,MCurrentMinField,Format(FloatFormat,[Value]));
end;

procedure TTTSByToolTypeData.SetRotationMax(const Value: Double);
begin
  FRotationMax := Value;
  Host.SetFieldData(Index,RotationMaxField,Format(FloatFormat,[Value]));
end;

procedure TTTSByToolTypeData.SetRotationDemand(const Value: Integer);
begin
  FRotationDemand := Value;
  Host.SetFieldData(Index,RotationDemandField,IntToStr(Value));
end;

procedure TTTSByToolTypeData.SetRotationMin(const Value: Double);
begin
  FRotationMin := Value;
  Host.SetFieldData(Index,RotationMinField,Format(FloatFormat,[Value]));
end;

procedure TTTSByToolTypeData.SetEFlowMin(const Value: Double);
begin
  FEFlowMin := Value;
  Host.SetFieldData(Index,ElectrodeFlowMinField,Format(FloatFormat,[Value]));
end;

procedure TTTSByToolTypeData.SetPressureMax(const Value: Double);
begin
  FPressureMax := Value;
  Host.SetFieldData(Index,PressureMaxField,Format(FloatFormat,[Value]));
end;

procedure TTTSByToolTypeData.SetPressureMin(const Value: Double);
begin
  FPressureMin := Value;
  Host.SetFieldData(Index,PressureMinField,Format(FloatFormat,[Value]));
end;

procedure TTTSByToolTypeData.SetElectrodeCount(const Value: INteger);
begin
  FElectrodeCount := Value;
  Host.SetFieldData(Index,ElectrodeCountField,IntToStr(Value));
end;

procedure TTTSByToolTypeData.SetChanelCount(const Value: INteger);
begin
  FChanelCount := Value;
  Host.SetFieldData(Index,ChanelCountField,IntToStr(Value));
end;

procedure TTTSByToolTypeData.SetIsRotary(const Value: Boolean);
begin
  FIsRotary := Value;
  If FIsRotary then
    Host.SetFieldData(Index,IsRotaryField,'1')
  else
    Host.SetFieldData(Index,IsRotaryField,'0');
end;

procedure TTTSByToolTypeData.SetIsHP(const Value: Boolean);
begin
  FIsHpTool := Value;
  If FIsHpTool then
    Host.SetFieldData(Index,IsHPField,'1')
  else
    Host.SetFieldData(Index,IsHPField,'0');
end;


procedure TTTSByToolTypeData.SetTPM1Default(const Value: Integer);
begin
  FTPM1 := Value;
  Host.SetFieldData(Index,TPM1DefaultField,IntToStr(Value));
end;

procedure TTTSByToolTypeData.SetTPM2Default(const Value: Integer);
begin
  FTPM2 := Value;
  Host.SetFieldData(Index,TPM2DefaultField,IntToStr(Value));

end;

procedure TTTSByToolTypeData.SetFIsNonBalluff(const Value: Integer);
begin
  FIsNonBalluff := Value;
  Host.SetFieldData(Index,IsNonBalluffField,IntToStr(Value));
end;

procedure TTTSByToolTypeData.SetExpiryHours(const Value: Integer);
begin
  FExpiryHours := Value;
  Host.SetFieldData(Index,ExpiryHoursField,IntToStr(Value));
end;

procedure TTTSByToolTypeData.SetHypertacCode(const Value: INteger);
begin
  FHypertacCode := Value;
  Host.SetFieldData(Index,HypertacCodeField,IntToStr(Value));
end;

{ TByToolTypeList }

function TByToolTypeList.IndexOf(ToolType : String): Integer;
var
ToolEntry : TTTSByToolTypeData;
Found : Boolean;
begin
  Result := -1;
  Found := False;
  while (Result < ToolList.count-1) and (not Found) do
    begin
    Inc(Result);
    ToolEntry := ToolList[Result];
    Found := UpperCase(ToolEntry.ToolType) = Uppercase(ToolType);
    end;
  if not Found then  Result := -1;
end;

function TByToolTypeList.IndexofToolWithHypertacCode(HTCode: Integer): Integer;
var
ToolEntry : TTTSByToolTypeData;
Found : Boolean;
begin
  Result := -1;
  Found := False;
  while (Result < ToolList.count-1) and (not Found) do
    begin
    Inc(Result);
    ToolEntry := ToolList[Result];
    if ToolEntry.aIsNonBalluff <> 0 then  Found := ToolEntry.HypertacCode = HTCode;
    end;
  if not Found then  Result := -1;
end;

procedure TByToolTypeList.CopyTool(Target, Src: TTTSByToolTypeData);
begin
  BeginUpdate;
  try
    Target.Copy(Src);
  finally
    EndUpdate;
  end;
end;

function TByToolTypeList.GetTool(index: Integer): TTTSByToolTypeData;
begin
  Result := TTTSByToolTypeData(ToolList[Index]);
end;

procedure TByToolTypeList.RemoveTool( T : String);
var I : Integer;
Tool : TTTSByToolTypeData;
begin
  BeginUpdate;
  try
    I := IndexOf(T);
    if I >= 0 then
      begin
      Tool := ToolList[I];
      ITable.Delete(I);
      Tool.Free;
      ToolList.Delete(I);
      RebuildIndex;
      end;
  finally
    EndUpdate;
  end;
end;

constructor TByToolTypeList.Create(TableName: string);
begin
  inherited Create;
  CreateWithName(TableName, True);
end;

constructor TByToolTypeList.Create;
begin
  inherited Create;
  CreateWithName('ByToolTypeData', True);
end;

procedure TByToolTypeList.CreateTable;
begin
  BeginUpdate;
  try
  ITable.Clear;
  ITable.AddField(ToolTypeField);
  ITable.AddField(NoseGuideFlowMinField);
  ITable.AddField(NoseGuideFlowMaxField);
  ITable.AddField(ElectrodeFlowMinField);
  ITable.AddField(ElectrodeFlowMaxField);
  ITable.AddField(ElectrodeCountField);
  ITable.AddField(ChanelCountField);
  ITable.AddField(PressureMinField);
  ITable.AddField(PressureMaxField );
  ITable.AddField(RotationDemandField);
  ITable.AddField(RotationMinField);
  ITable.AddField(RotationMaxField);
  ITable.AddField(MCurrentMinField);
  ITable.AddField(MCurrentMaxField);
  ITable.AddField(IsHPField);
  ITable.AddField(TPM1DefaultField);
  ITable.AddField(TPM2DefaultField);
  ITable.AddField(ExpiryHoursField);
  ITable.AddField(HypertacCodeField);
  ITable.AddField(IsNonBalluffField);
  ITable.AddField(IsRotaryField);
  finally
    EndUpdate;
  end;
end;
{
  IsHPField        = 'IsHPTool';
  TPM1DefaultField = 'TPM1';
  TPM2DefaultField = 'TPM2';
  ExpiryHoursField = 'ExpiryHours';
  HypertacCodeField = 'HTCode';
  IsNonBalluffField = 'NB';
  IsRotaryField    = 'IsRotary';
}
procedure TByToolTypeList.RebuildInternalValues;
var I : Integer;
    Tmp : TTTSByToolTypeData;
begin
  for I := ITable.RecordCount to ToolList.Count - 1 do begin
    Tool[0].Free;
    ToolList.Delete(0);
  end;

  for I := 0 to ITable.RecordCount - 1 do begin
    if I >= ToolList.Count then begin
      Tmp := TTTSByToolTypeData.Create(Self, I);
      ToolList.Add(Tmp);
      Tmp.RebuildInternalValues(I);
    end else begin
      Tool[I].RebuildInternalValues(I);
    end;
  end;
end;

procedure TByToolTypeList.Clean;
begin
  inherited Clean;
end;

procedure TByToolTypeList.CleanTable;
var F: WideString;
begin
  BeginUpdate;
  try
    while ITable.FieldCount > 0 do begin
      ITable.FieldNameAtIndex(0, F);
      ITable.DeleteField(F);
    end;
  finally
    EndUpdate;
  end;

end;


function TByToolTypeList.Count: Integer;
begin
  Result := ToolList.Count;
end;

function TByToolTypeList.AddTool: TTTSByToolTypeData;
begin
  Lock;
  try
    Result := TTTSByToolTypeData.Create(Self, -1);
    Result.Index := ITable.RecordCount;
    ToolList.Add(Result);
    ITable.Append(1);
  finally
    Unlock;
  end;
end;

procedure TByToolTypeList.RebuildIndex;
var I : Integer;
begin
  for I := 0 to Count - 1 do begin
    Tool[I].Index := I;
  end;
end;

procedure TByToolTypeList.RebuildSingleValue(Row, Col: Integer);
begin
  inherited;
  Tool[Row].RebuildInternalValues(Row);
end;

function TByToolTypeList.FindByHypertacCode(HTCode: Integer): TTTSByToolTypeData;
var
I : Integer;
begin
Result := nil;
I := IndexofToolWithHypertacCode(HTCode);
if I >=0 then Result := ToolList[i];
end;

function TByToolTypeList.Find(ToolType: string): TTTSByToolTypeData;
var
I : Integer;
begin
Result := nil;
I := IndexOf(ToolType);
if I >=0 then Result := ToolList[i];
end;

function TByToolTypeList.ToolExists(ToolType: string): Boolean;
var
I : Integer;
begin
Result := False;
I := IndexOf(ToolType);
if I >=0 then Result := True;

end;

destructor TByToolTypeList.Destroy;
begin

  inherited;
end;

procedure TByToolTypeList.CreateWithName(TableName: string; Clean: Boolean);
begin
  if StringArraySet.CreateArray(TableName, Self, ITable) <> AAR_OK then begin
    StringArraySet.UseArray(TableName, Self, ITable);
    if ITable.FieldCount <> 21 then begin
      CleanTable;
      CreateTable;
    end;
  end else begin
    CreateTable;
  end;
  ToolList := TList.Create;
  RebuildInternalValues;
end;

procedure TTTSByToolTypeData.Copy(Src: TTTSByToolTypeData);
begin
  ToolType := Src.ToolType;
  NoseGuideFlowMin := Src.NoseGuideFlowMin;
  NoseGuideFlowMax := Src.NoseGuideFlowMax;
  ElectrodeFlowMin := Src.ElectrodeFlowMin;
  ElectrodeFlowMax := Src.ElectrodeFlowMax;
  ElectrodeCount   := Src.ElectrodeCount;
  ChanelCount      := Src.ChanelCount;
  PressureMin      := Src.PressureMin;
  PressureMax      := Src.PressureMax;
  RotationMin      := Src.RotationMin;
  RotationMax      := Src.RotationMax;
  MCurrentMin      := Src.MCurrentMin;
  MCurrentMax      := Src.MCurrentMax;
  TPM1Default      := Src.TPM1Default;
  TPM2Default      := Src.TPM2Default;
  ExpiryHours      := Src.ExpiryHours;
end;




function TByToolTypeList.AddToolFromAddData(ToolData : TToolAddData):TTTSByToolTypeData;
begin
 Result :=  AddTool;
 Result.ToolType := ToolData.ToolType;
 Result.NoseGuideFlowMin := ToolData.NoseGuideFlowMin;
 Result.NoseGuideFlowMax := ToolData.NoseGuideFlowMax;
 Result.ElectrodeFlowMin := ToolData.ElectrodeFlowMin;
 Result.ElectrodeFlowMax := ToolData.ElectrodeFlowMax;
 Result.ElectrodeCount := ToolData.ElectrodeCount;
 Result.ChanelCount := ToolData.ChanelCount;
 Result.PressureMin := ToolData.PressureMin;
 Result.PressureMax := ToolData.PressureMax;
 Result.RotationDemand := ToolData.RotationDemand;
 Result.RotationMin := ToolData.RotationMin;
 Result.RotationMax := ToolData.RotationMax;
 Result.MCurrentMin := ToolData.MCurrentMin;
 Result.MCurrentMax := ToolData.MCurrentMax;
 Result.IsRotary := ToolData.IsRotaryTool;
 Result.IsHPTool := ToolData.IsHpTool;
 Result.TPM1Default := ToolData.TPM1Default;
 Result.TPM2Default := ToolData.TPM2Default;
 Result.ExpiryHours := ToolData.ExpiryHoursDefault;
 Result.HypertacCode := ToolData.NonBallufHypertacCode;
 if ToolData.isNonBalluff then
  Result.aIsNonBalluff := 1
 else
  Result.aIsNonBalluff := 0;
 FlushData(ITable);
end;


procedure TByToolTypeList.FlushData(Table: IACNC32StringArray);
var
FName : WideString;
begin
If assigned(Table) then
  begin
  FName := DllLocalPath+'DB\'+Table.Alias;
  Table.ExportToCSV(FName)
  end;
end;

procedure TByToolTypeList.FlushDataToDisk;
begin
if assigned(ITable) then
  begin
  FlushData(ITable);
  end;
end;



function TByToolTypeList.ToolNameFromHypertacCode(HTCode: INteger): String;
begin
Result := '';
end;


end.
