unit ToolForm;

interface
uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, ValEdit, ToolData, OCTUtils, ExtCtrls, StdCtrls, Buttons,
  IFormInterface, INamedInterface, C6001, IniFiles, NamedPlugin, PluginInterface,
  AbstractFormPlugin, IToolLife, CNCTypes, OCTTypes, ACNC_BaseKeyBoards,
  ToolDB, AllToolDB, Math, ToolMonitor, ComCtrls,TTSResults,
  TTSDataByUniqueTool,TTSByTypeData,ToolDataBaseEditor;

type
  TMemoStlyes = (tmsNormal,tmsWarning);

  TDefaultTestSettings = record
    EFlowMax : Double;
    EFlowMin : Double;
    NGFlowMax : Double;
    NGFlowMin : Double;
    MCurrentMax : Double;
    MCurrentMin : Double;
    RotationDemand : Double;
    RotationMax : Double;
    RotationMin : Double;
    PressureMaxHP : Double;
    PressureMinHP : Double;
    PressureMaxLP : Double;
    PressureMinLP : Double;
    TPM1 : Integer;
    TPM2 : Integer;
    ExpiryHours : Integer;
    end;

  TBalluffToolTag = (
    bttToolChangerWrite,
    bttToolChangerRemove,
    bttHeadWrite,
    bttHeadRemove,
    bttToolChangerPosition,
    bttHypertacToolCode,
    bttHypertacSerialNumber,
    bttHeadWriteString,
    bttToolChangerWriteString,
    bttUniqueToolOffsetX,
    bttUniqueToolOffsetY,
    bttUniqueToolOffsetZ,
    bttUniqueToolOffsetC,
    bttIBBReset,
    bttOperatorToolValidate,
    bttBlockUpdateDatabase,
    bttInhibitToolDialogue,
    bttToolStation0,
    bttToolStation1,
    bttToolStation2,
    bttToolStation3,
    bttToolStation4,
    bttToolStation5,
    bttToolStation6,
    bttToolStation7,
    bttToolStation8,
    bttExpectedTools,
    bttFake,
    bttHypertacMismatch,
    bttToolDialogueActive,
    ttsNonBalluffPopUpReq,
    ttsPopUpReq,
    ttsByToolEditorPopUpReq,
    ttstOPenMode,
    ttsFormActive,
    ttsValidClose,
    ttsNGSerial,
    bttHeadRead,
    bttToolPassed,
    bttResultRecord,
    bttProcessResultRecord,
    bttToolResultFilter,
    bttShowResults,
    bttHideResults,
    bttNonBalluffToolType,
    bttNonBalluffSerial,
    bttDisplayToolType,
    bttDisplayToolSerial,
    bttAccessUserName,
    bttAccessLevels,
    bttRotaryToolFitted
  );

  TBalluffDevice = (
    bdHead,
    bdToolChanger,
    bdNone
  );

  TPopTypes = (tpuStandard,tpuOnToolLoad,tpuOnNonBalluffToolLoad,tpuExistingTool,TpuNewTool);



  TToolManager = class(TC6x1Client)
    Panel3: TPanel;
    KBBackPanel: TPanel;
    BaseKB1: TBaseKB;
    BaseFPad1: TBaseFPad;
    Panel6: TPanel;
    Panel7: TPanel;
    Panel1: TPanel;
    TagPages: TPageControl;
    TagEventsPage: TTabSheet;
    Panel5: TPanel;
    LogMemo: TMemo;
    InfoPanel: TPanel;
    OperatorPage: TTabSheet;
    Panel4: TPanel;
    btnToolChanger: TSpeedButton;
    btnHead: TSpeedButton;
    SpeedButton3: TSpeedButton;
    BitBtn1: TBitBtn;
    BtnWrite: TBitBtn;
    Panel8: TPanel;
    Memo1: TMemo;
    Panel9: TPanel;
    ToolEditor: TValueListEditor;
    SpeedButton1: TSpeedButton;
    Panel10: TPanel;
    AbandonButton: TSpeedButton;
    AddToolTypePage: TTabSheet;
    Panel11: TPanel;
    AddToolMemo: TMemo;
    AddToolTypeButton: TSpeedButton;
    SpeedButton2: TSpeedButton;
    DebugMemo: TMemo;
    NewToolPage: TTabSheet;
    Panel2: TPanel;
    NewToolMemo: TMemo;
    NewToolCancelBtn: TSpeedButton;
    NewToolAddButton: TSpeedButton;
    NGSerialEnter: TEdit;
    NewToolRequestLabel: TLabel;
    Panel12: TPanel;
    OffsetMatchCheckBox: TCheckBox;
    OffsetConfirmButton: TSpeedButton;
    Panel14: TPanel;
    ConfirmLabel: TLabel;
    NewNGInput: TEdit;
    Panel13: TPanel;
    ConfirmTitle: TPanel;
    NGModifyButton: TSpeedButton;
    NoseGuideConfirmButton: TSpeedButton;
    TPMPanel: TPanel;
    btnHeadRead: TBitBtn;
    Panel16: TPanel;
    NBTypePage: TTabSheet;
    NBTopPanel: TPanel;
    NonBalluffToolMemo: TMemo;
    SpeedButton4: TSpeedButton;
    NBToolTypeAddButton: TSpeedButton;
    NBUniquePage: TTabSheet;
    Label6: TLabel;
    NBToolNameEdit: TEdit;
    Label7: TLabel;
    NBHypertacDisplay: TEdit;
    Panel17: TPanel;
    NBUniqueMemo: TMemo;
    NBNewToolRequestLabel: TLabel;
    SpeedButton5: TSpeedButton;
    NBAddToolButton: TSpeedButton;
    NBNGSerialEdit: TEdit;
    NBConfirmPage: TTabSheet;
    Panel15: TPanel;
    NBConfirmMemo: TMemo;
    NBConfirmButton: TSpeedButton;
    SpeedButton7: TSpeedButton;
    NBConfirmLabel: TLabel;
    NBConfirmDisplay: TEdit;
    NBConfirmHTC: TEdit;
    NBConfirmToolType: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    NBConfirmHTSN: TEdit;
    Label8: TLabel;
    Label9: TLabel;
    NBAddHTC: TEdit;
    Label10: TLabel;
    NBAddHTSN: TEdit;
    Label11: TLabel;
    NBAddToolType: TEdit;
    StandardTPMPanel: TPanel;
    Label3: TLabel;
    TPM1Display: TEdit;
    Label5: TLabel;
    TPMHoursDisplay: TEdit;
    Label4: TLabel;
    TPM2Panel: TPanel;
    TPM2ValueEdit: TEdit;
    Label12: TLabel;
    TPM2Btn: TBitBtn;
    procedure TPM2BtnMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure TPM2BtnMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure NBConfirmButtonClick(Sender: TObject);
    procedure NBAddToolButtonClick(Sender: TObject);
    procedure NBToolTypeAddButtonClick(Sender: TObject);
    procedure NewToolAddButtonClick(Sender: TObject);
    procedure btnHeadReadMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure btnHeadReadMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure AbandonButtonClick(Sender: TObject);
    procedure NGModifyButtonClick(Sender: TObject);
    procedure NoseGuideConfirmButtonClick(Sender: TObject);
    procedure OffsetConfirmButtonClick(Sender: TObject);
    procedure AddToolTypeButtonClick(Sender: TObject);
    procedure btnToolChangerClick(Sender: TObject);
    procedure SpeedButton3MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure SpeedButton3MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure BtnWriteMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure BtnWriteMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FormShow(Sender: TObject);
  private
    LoadedUniqueToolEntry : TTTSByUniqueToolData;
    OffsetsChangeConfirmed : Boolean;
    ExpectedNGSerial : Integer;
    LoadedToolData : TToolData;
    ReadAtToolDoorMcode : Integer;
    DataType : string;
    Device : array [TBalluffDevice] of string;
    Head : array [TBalluffDevice] of Boolean;
    StateBuffer : array [TBalluffDevice] of string;
    StateIBB : array [TBalluffDevice] of TIBBState;
    StateChange : array [TBalluffDevice] of Boolean;
    BtnSelect : TBalluffDevice;

    TagNames : array [TBalluffToolTag] of string;
    ToolList : TToolList;
    ToolDB : TToolDB;
    AllToolDB : TAllToolDB;
    FID : FHandle;
    Translator : TTranslator;
    ToolDefTags: array [0..8] of TTagRef;
    ToolMonitor: TToolMonitor;

    LastToolListCount : Integer;
    ToolChangerToolCount: Integer;
    ExitEnable : Boolean;
    AddToolData : TToolData;
    NGSerialModified : Boolean;
    function LogStrToFloat(Field : Integer) : Double;
    function LogStrToInt(Field : Integer) : Integer;
    function HighPressureField(Field : Integer) : Boolean;
//    function OptionalTimeEntry(Field : Integer; Future : Double = 100) : TDateTime;
    function TTSOptionalTimeEntry(Field : Integer; FutureHours : Double) : TDateTime;
//    function TimeEntryAddHours(StartTime : TDateTime;AddedHours :Integer): TDateTime;
    procedure AddToolFromEditor(Ex1 : Integer;
                                EditorEntry:Boolean;
                                UseTPMData : Boolean;
                                OffsetVerify: Boolean=False);
    procedure RemoveToolAtEx1(Ex1 : Integer);
    procedure UpdateDisplay;
    procedure AddTool(Ex1 : Integer; Buffer : string);
    procedure TagChanged(ATag : TTagRef);
    procedure ReadConfig;
    procedure WriteConfig;
    procedure DoStateChange(D : TBalluffDevice);
    procedure Logging(D : TBalluffDevice; const Msg : string);
    procedure ToolListChange(Sender : TObject);
    procedure UpdateWriteStrings;
    procedure CreateBalluffString(Ex1 : Integer; Tag : TTagRef);
    procedure LocalizeForm;
    function CheckToolAddition(T : TToolData):Boolean;
    procedure CreateDefinitionStrings;
    procedure UpdateExpectedTools;
    procedure PopupForLoadedTool(LTool : String);
    function  GetTagString(ATag : TTagref): String;
    procedure ConfigureForExistingTool(UniqueTEntry : TTTSByUniqueToolData;
                                       LoadedTool: TToolData);
    procedure ConfigureForExistingNBTool(UniqueTEntry : TTTSByUniqueToolData;
                                       LoadedTool: TToolData
                                       ;HTC : INteger);
    procedure ConfigureForFollowOnTool(LoadedTool: TToolData;
                                       IsRotary : Boolean);

    procedure ConfigureForFollowOnNBTool(LoadedTool: TToolData;
                                       IsRotary : Boolean;HTC : INteger);

    procedure ConfigureForNewTool(LoadedTool: TToolData;IsRotary : Boolean);
    procedure ConfigureForNewNBTool(LoadedTool: TToolData;IsRotary : Boolean;HTC : Integer);
    procedure ConfigureForUnknownToolType(TypeData : TToolData;RotaryHardware :  Boolean);
    procedure ConfigureForUnknownNonBallufToolType(HTC,HTSN : Integer);
    procedure SetPageTo(DPage :TTabSheet;IsSmallFormat : Boolean);
    function LoadDummyTool(ToolType : Widestring;Serial : Integer): TToolData;
    procedure SetInitialVisibilityStatus;
    procedure SetMemoStyle(Memo : TMemo;MemoStyle : TMemoStlyes);
    procedure SetStringTag(aTag : TTagRef;StrVal : String);

  protected
    procedure C6x1Notify(Service: TC6x1; Msg : TIBBState); override;
  public
    NewToolIsRotary : Boolean;
    PopupType : TPopTypes;
    Tags : array [TBalluffToolTag] of TTagRef;
    DefaultRotarySettings : TDefaultTestSettings;
    DefaultStaticSettings :TDefaultTestSettings;
    AccessNameForTPM2 : String;

    procedure Install; override;
    procedure FinalInstall; override;
    procedure Execute; override;
    procedure Shutdown; override;
    procedure DisplaySweep; override;
    procedure AddDebugLine(InString : String);

  end;

var
  ToolManager: TToolManager;
  BenchDebug : Integer;
  ShowDebug : Integer;
  LoadedToolTPMHours : Integer;
  LoadedToolTPM1 : Integer;
  LoadedToolTPM2 : Integer;
  ResultsData : TToolResultList;
  StatsData   : TToolStatsList;

implementation

{$R *.dfm}

uses ToolOKDlg, ToolRegistration,ResultsForm;

//var
//  HypMismatchFID : FHandle;

const

  TagDefinitions : array [TBalluffToolTag] of TTagDefinitions = (
     ( Ini : 'ToolChangerWrite'; Tag : 'ToolChangerWrite'; Owned : True; TagType : 'TMethodTag' ),
     ( Ini : 'ToolChangerRemove'; Tag : 'ToolChangerRemove'; Owned : False; TagType : 'TMethodTag' ),
     ( Ini : 'HeadToolWrite'; Tag : 'HeadToolWrite'; Owned : True; TagType : 'TMethodTag' ),
     ( Ini : 'HeadToolRemove'; Tag : 'HeadToolRemove'; Owned : False; TagType : 'TMethodTag' ),
     ( Ini : 'ToolChangerPosition'; Tag : 'ToolChangerPosition'; Owned : False; TagType : 'TIntegerTag' ),
     ( Ini : 'HypertacToolCode'; Tag : 'HypertacToolCode'; Owned : False; TagType : 'TIntegerTag' ),
     ( Ini : 'HypertacSerialNumber'; Tag : 'HypertacSerialNumber'; Owned : False; TagType : 'TIntegerTag' ),
     ( Ini : 'HeadWriteString'; Tag: 'HeadWriteString'; Owned : True; TagType : 'TStringTag' ),
     ( Ini : 'ToolChangerWriteString'; Tag: 'ToolChangerWriteString'; Owned : True; TagType : 'TStringTag' ),
     ( Ini : 'UniqueToolOffsetX'; Tag: 'UniqueToolOffsetX'; Owned : True; TagType : 'TDoubleTag' ),
     ( Ini : 'UniqueToolOffsetY'; Tag: 'UniqueToolOffsetY'; Owned : True; TagType : 'TDoubleTag' ),
     ( Ini : 'UniqueToolOffsetZ'; Tag: 'UniqueToolOffsetZ'; Owned : True; TagType : 'TDoubleTag' ),
     ( Ini : 'UniqueToolOffsetC'; Tag: 'UniqueToolOffsetC'; Owned : True; TagType : 'TDoubleTag' ),
     ( Ini : 'IBBResetPB'; Tag: 'IBBResetPB'; Owned : True; TagType : 'TMethodTag' ),
     ( Ini : 'OperatorToolValidate'; Tag: 'MTB2'; Owned : False; TagType : 'TIntegerTag' ),
     ( Ini : 'BlockUpdateDB'; Tag: 'ToolChangerBlockUpdateDB'; Owned : False; TagType : 'TMethodTag' ),
     ( Ini : 'InhibitToolDialogue'; Tag: 'InhibitToolDialogue'; Owned : False; TagType : 'TIntegerTag' ),
     ( Ini : 'ToolDefinition0'; Tag: 'ToolDefinition0'; Owned : True; TagType : 'TStringTag' ),
     ( Ini : 'ToolDefinition1'; Tag: 'ToolDefinition1'; Owned : True; TagType : 'TStringTag' ),
     ( Ini : 'ToolDefinition2'; Tag: 'ToolDefinition2'; Owned : True; TagType : 'TStringTag' ),
     ( Ini : 'ToolDefinition3'; Tag: 'ToolDefinition3'; Owned : True; TagType : 'TStringTag' ),
     ( Ini : 'ToolDefinition4'; Tag: 'ToolDefinition4'; Owned : True; TagType : 'TStringTag' ),
     ( Ini : 'ToolDefinition5'; Tag: 'ToolDefinition5'; Owned : True; TagType : 'TStringTag' ),
     ( Ini : 'ToolDefinition6'; Tag: 'ToolDefinition6'; Owned : True; TagType : 'TStringTag' ),
     ( Ini : 'ToolDefinition7'; Tag: 'ToolDefinition7'; Owned : True; TagType : 'TStringTag' ),
     ( Ini : 'ToolDefinition8'; Tag: 'ToolDefinition8'; Owned : True; TagType : 'TStringTag' ),
     ( Ini : 'ExpectedTools'; Tag: 'ToolChangerExpectedTools'; Owned : True; TagType : 'TIntegerTag' ),
     ( Ini : 'Fake'; Tag: 'Acnc32Fake'; Owned : False; TagType : 'TIntegerTag' ),
     ( Ini : 'HypertacMismatch'; Tag: 'HypertacMismatch'; Owned : True; TagType : 'TIntegerTag' ),
     ( Ini : 'ToolDialogueActive'; Tag: 'ToolDialogueActive'; Owned : True; TagType : 'TIntegerTag' ),
     ( Ini : 'TTS_NonBalluffPopUpReq'; Tag: 'TTS_NonBalluffPopUpReq'; Owned : False; TagType : 'TIntegerTag' ),
     ( Ini : 'TTS_ToolFormPopUpReq'; Tag: 'TTS_ToolFormPopUpReq'; Owned : False; TagType : 'TIntegerTag' ),
     ( Ini : 'TTS_ByTooToolEditorPopUpReq'; Tag: 'TTS_ByTooToolEditorPopUpReq'; Owned : False; TagType : 'TIntegerTag' ),
     ( Ini : 'TTS_OpenMode'; Tag: 'TTS_OpenMode'; Owned : False; TagType : 'TIntegerTag' ),
     ( Ini : 'TTS_FormActive'; Tag: 'TTS_FormActive'; Owned : True; TagType : 'TIntegerTag' ),
     ( Ini : 'TTS_ValidClose'; Tag: 'TTS_ValidClose'; Owned : True; TagType : 'TIntegerTag' ),
     ( Ini : 'TTS_NGSerial'; Tag: 'TTS_NGSerial'; Owned : True; TagType : 'TIntegerTag' ),
     ( Ini : 'TTS_HeadRead'; Tag: 'TTS_HeadRead'; Owned : True; TagType : 'TIntegerTag' ),
     ( Ini : 'TTS_ToolPassed'; Tag: 'TTS_ToolPassed'; Owned : False; TagType : 'TIntegerTag' ),
     ( Ini : 'TTS_ResultRecord'; Tag: 'TTS_ResultRecord'; Owned : False; TagType : 'TStringTag' ),
     ( Ini : 'TTS_ProcessResult'; Tag: 'TTS_ProcessResult'; Owned : False; TagType : 'TIntegerTag' ),
     ( Ini : 'TTS_ToolResultFilter'; Tag: 'TTS_ToolResultFilter'; Owned : False; TagType : 'TStringTag' ),
     ( Ini : 'TTS_ShowResults'; Tag: 'TTS_ShowResults'; Owned : False; TagType : 'TIntegerTag' ),
     ( Ini : 'TTS_HideResults'; Tag: 'TTS_HideResults'; Owned : False; TagType : 'TIntegerTag' ),
     ( Ini : 'TTS_NonBallufToolType'; Tag: 'TTS_NonBallufToolType'; Owned : False; TagType : 'TStringTag' ),
     ( Ini : 'TTS_NonBallufSerial'; Tag: 'TTS_NonBallufSerial'; Owned : False; TagType : 'TIntegerTag' ),
     ( Ini : 'TTS_DisplayToolType'; Tag: 'TTS_DisplayToolType'; Owned : False; TagType : 'TStringTag' ),
     ( Ini : 'TTS_DisplayToolSerial'; Tag: 'TTS_DisplayToolSerial'; Owned : False; TagType : 'TIntegerTag' ),
     ( Ini : 'AccessUserName'; Tag: 'AccessUserName'; Owned : True; TagType : 'TStringTag' ),
     ( Ini : 'AccessLevels'; Tag: 'AccessLevels'; Owned : True; TagType : 'TIntegerTag' ),
     ( Ini : 'RotaryTool'; Tag: 'RotaryTool'; Owned : True; TagType : 'TIntegerTag' )
      );

  ShuttleIDField = 1;
  ShuttleSNField = 2;
  TPMTimeField = 3;       // Rebiuldtime Field
  StatusFlagsField = 4;
  BarrelCountField = 5;
  RemainingBarrelsField = 6;
  MaxToolLengthField = 7;
  MinToolLengthField = 8;
  ActualLengthField = 9;
  UniqueToolXField = 10;
  UniqueToolYField = 11;
  UniqueToolZField = 12;
  UniqueToolCField = 13;
  TPM1Field = 14;
  TPM2Field = 15;
  OffsetVerifyField = 16;
  ExpiryField = 17; // Not used fo RR TTS

  ObjectName = 'BalluffShuttle.ToolForm'; // Just used for parameter in  SetFaultA.

{ TToolManager }

procedure ToolTagChange(ATag : TTagRef); stdcall;
begin
  ToolManager.TagChanged(ATag);
end;


//TestToolAddition='0101,blog,1,10,20,30,40,50,60,70,80,90,1,1,1,1'
procedure TestToolAddition(ATag : TTagRef); stdcall;
begin
  ToolManager.AddTool(1, Named.GetAsOleVariant(ATag));
end;

{
function HypertacFaultReset(FID : FHandle) : Boolean; stdcall;
begin
  Result := HypMismatchFID = 0;
end;
}

procedure TToolManager.Install;
var I : TBalluffToolTag;
begin
  Translator := TTranslator.Create;
  LocalizeForm;

  ToolManager := Self;
//  ToolManager.HeadRegistered := PublishHead;

  ReadConfig;
  for I := Low(TBalluffToolTag) to High(TBalluffToolTag) do begin
    if TagDefinitions[I].Owned then
      Tags[I] := Named.AddTag(PChar(TagNames[I]), PChar(TagDefinitions[I].TagType));
  end;
  btnHead.Down := True;
  ToolMonitor:= TToolMonitor.Create;

end;

procedure TToolManager.FinalInstall;
var I : TBalluffToolTag;
    Tmp: Integer;
begin
  ToolManager := Self;
  ToolList := TToolList.Create;
  ToolDB := TToolDB.Create;
  AllToolDB := TAllToolDB.Create;
  ToolList.OnChange := ToolListChange;

  ResultsData := TToolResultList.create;
  StatsData   := TToolStatsList.create;
  ResultsData.StatsHandler := StatsData;
  if assigned(ToolResultsForm) then
    begin
    ToolResultsForm.ResultsList := ResultsData;
    end;

  for I := Low(TBalluffToolTag) to High(TBalluffToolTag) do begin
    if not TagDefinitions[I].Owned then
      Tags[I] := Named.AquireTag(PChar(TagNames[I]), PChar(TagDefinitions[I].TagType), ToolTagChange);
  end;

  Tmp:= 0;
  for I := bttToolStation0 to bttToolStation8 do begin
    ToolDefTags[Tmp] := Tags[I];
    Inc(Tmp);
  end;

  Named.AquireTag('TestToolAddition', 'TStringTag', TestToolAddition);
  Named.SetAsInteger(Tags[ttsFormActive],1);
  Named.SetAsInteger(Tags[bttHeadRead],0);
  PopupType := tpuStandard;
  DebugMemo.Visible := ShowDebug <> 0;
end;

procedure TToolManager.Shutdown;
begin
  WriteConfig;
  ToolList.Close;
  ToolList := nil;

  ToolDB.Close;
  ToolDB := nil;
end;

procedure TToolManager.ReadConfig;
var Ini : TIniFile;
    I :TBalluffToolTag;
begin
  Ini := TIniFile.Create(dllProfilePath + 'RRTTS_Shuttle.cfg');
  try
    DataType := Ini.ReadString('ToolForm', 'DataType', 'TM');
    Device[bdHead] := Ini.ReadString('ToolForm', 'HeadBalluffDevice', 'Balluff_1');
    Head[bdHead] := Ini.ReadBool('ToolForm', 'HeadBalluffHead', False);
    Device[bdToolChanger] := Ini.ReadString('ToolForm', 'ToolChangerBalluffDevice', 'Balluff_1');
    Head[bdToolChanger] := Ini.ReadBool('ToolForm', 'ToolChangerBalluffHead', True);
    ReadAtToolDoorMcode := Ini.ReadInteger('ToolForm', 'ReadAtDoorMcode', 79);
    ToolChangerToolCount:= Ini.ReadInteger('ToolForm', 'ToolChangerToolCount', 6 );

    Device[bdNone] := '*';

    for I := Low(TBalluffToolTag) to High(TBalluffToolTag) do begin
      TagNames[I] := Ini.ReadString('ToolFormTags', TagDefinitions[I].Ini, TagDefinitions[I].Tag);
    end;
    BenchDebug := Ini.ReadInteger('Debug','IsBenchDebug',0);
    ShowDebug :=  Ini.ReadInteger('Debug','ShowDebug',0);
    AccessNameForTPM2 := Ini.ReadString('ToolForm','AccessNameForTPM2','Winbro');
//    defa
    DefaultRotarySettings.EFlowMax := Ini.ReadFloat('DefaultRotarySettings','EFlowMax',100);
    DefaultRotarySettings.EFlowMin := Ini.ReadFloat('DefaultRotarySettings','EFlowMin',100);
    DefaultRotarySettings.NGFlowMax:=   Ini.ReadFloat('DefaultRotarySettings','NGFlowMax',100);
    DefaultRotarySettings.NGFlowMin:=   Ini.ReadFloat('DefaultRotarySettings','NGFlowMin',100);
    DefaultRotarySettings.MCurrentMax:= Ini.ReadFloat('DefaultRotarySettings','McurrentMax',100);
    DefaultRotarySettings.MCurrentMin:= Ini.ReadFloat('DefaultRotarySettings','McurrentMin',100);
    DefaultRotarySettings.RotationDemand:= Ini.ReadFloat('DefaultRotarySettings','RotationDemand',950);
    DefaultRotarySettings.RotationMax:= Ini.ReadFloat('DefaultRotarySettings','RotMax',100);
    DefaultRotarySettings.RotationMin:= Ini.ReadFloat('DefaultRotarySettings','RotMin',100);
    DefaultRotarySettings.PressureMaxHP  := Ini.ReadFloat('DefaultRotarySettings','PressureMaxHP',105);
    DefaultRotarySettings.PressureMinHP  := Ini.ReadFloat('DefaultRotarySettings','PressureMinHP',95);
    DefaultRotarySettings.PressureMaxLP  := Ini.ReadFloat('DefaultRotarySettings','PressureMaxLP',69);
    DefaultRotarySettings.PressureMinLP  := Ini.ReadFloat('DefaultRotarySettings','PressureMinLP',67);
    DefaultRotarySettings.TPM1  := Ini.ReadInteger('DefaultRotarySettings','TPM1',100);
    DefaultRotarySettings.TPM2  := Ini.ReadInteger('DefaultRotarySettings','TPM2',200);
    DefaultRotarySettings.ExpiryHours  := Ini.ReadInteger('DefaultRotarySettings','ExpiryHours',8);

    DefaultStaticSettings.EFlowMax := Ini.ReadFloat('DefaultStaticSettings','EFlowMax',100);
    DefaultStaticSettings.EFlowMin := Ini.ReadFloat('DefaultStaticSettings','EFlowMin',100);
    DefaultStaticSettings.NGFlowMax:=   Ini.ReadFloat('DefaultStaticSettings','NGFlowMax',100);
    DefaultStaticSettings.NGFlowMin:=   Ini.ReadFloat('DefaultStaticSettings','NGFlowMin',100);
    DefaultStaticSettings.PressureMaxHP  := Ini.ReadFloat('DefaultStaticSettings','PressureMaxHP',105);
    DefaultStaticSettings.PressureMinHP  := Ini.ReadFloat('DefaultStaticSettings','PressureMinHP',95);
    DefaultStaticSettings.PressureMaxLP  := Ini.ReadFloat('DefaultStaticSettings','PressureMaxLP',69);
    DefaultStaticSettings.PressureMinLP  := Ini.ReadFloat('DefaultStaticSettings','PressureMinLP',67);
    DefaultStaticSettings.TPM1  := Ini.ReadInteger('DefaultStaticSettings','TPM1',100);
    DefaultStaticSettings.TPM2  := Ini.ReadInteger('DefaultStaticSettings','TPM2',200);
    DefaultStaticSettings.ExpiryHours  := Ini.ReadInteger('DefaultStaticSettings','ExpiryHours',8);
  finally
    Ini.Free;
  end;
end;

{ WriteConfig removed due to auto configuration enabled }
procedure TToolManager.WriteConfig;
{var Ini : TIniFile;
    I :TBalluffToolTag; }
begin
{
  Ini := TIniFile.Create(dllProfilePath + 'CellFShuttle.cfg');
  try
    Ini.WriteString('ToolForm', 'DataType', DataType);
    Ini.WriteString('ToolForm', 'HeadBalluffDevice', Device[bdHead]);
    Ini.WriteBool('ToolForm', 'HeadBalluffHead', Head[bdHead]);
    Ini.WriteString('ToolForm', 'ToolChangerBalluffDevice', Device[bdToolChanger]);
    Ini.WriteBool('ToolForm', 'ToolChangerBalluffHead', Head[bdToolChanger]);

    for I := Low(TBalluffToolTag) to High(TBalluffToolTag) do begin
      Ini.WriteString('ToolFormTags', TagDefinitions[I].Ini, TagNames[I]);
    end;
  finally
    Ini.Free;
  end;
  }
end;


procedure TToolManager.TagChanged(ATag: TTagRef);


  procedure RemoveTool(TCPos : Integer);
  var ToolType : WideString;
      SN : Integer;
   begin
    if ToolLife.GetEx1ToolType(TCPos, ToolType) = trOK then begin
      if ToolLife.GetEx1ToolSN(TCPos, SN) = trOK then begin
        if ToolLife.RemoveTool(ToolType, SN) = trOK then begin
          Exit;
        end;
      end;
    end;
//    Named.SetFaultA(ObjectName, Format('Failed to removed toole %s from database', [ToolType]), FaultWarning, nil)
  end;

  var
  Buffer : String;
  ToolResult : TTTSToolResult;
  ToolFilter : String;
begin
  if Named.GetAsBoolean(ATag) then begin

    if ATag = Tags[bttToolChangerRemove] then begin
      RemoveTool(Named.GetAsInteger(Tags[bttToolChangerPosition]));
      Exit;
    end;

    if ATag = Tags[bttHeadRemove] then begin
      RemoveTool(0);
      Exit;
    end;
  end;

  if (ATag = Tags[bttToolChangerPosition]) then begin
    UpdateExpectedTools;

    if Visible then
      UpdateDisplay;

    if Named.GetAsInteger(Tags[bttToolChangerPosition]) > 0 then
      begin
      UpdateWriteStrings;
      end;
  end;

  if (ATag = Tags[ttsPopUpReq]) then
    begin
    if Named.GetAsInteger(ATag) = 0 then
      begin
      // pop up on falling edge of Tag
      PopupType := tpuOnToolLoad;
      Execute;
      end
    end;

  if (ATag = Tags[ttsNonBalluffPopUpReq]) then
    begin
    if Named.GetAsInteger(ATag) = 0 then
      begin
      // pop up on falling edge of Tag
      PopupType := tpuOnNonBalluffToolLoad;
      Execute;
      end
    end;

  if (ATag = Tags[ttsByToolEditorPopUpReq]) then
    begin
    if Named.GetAsInteger(ATag) = 0 then
      begin
      // pop up on falling edge of Tag
      RRToolDataEditor.ExecuteForByTypeEdit
      end
    end;

  if (ATag = Tags[bttProcessResultRecord]) then
    begin
    if Named.GetAsInteger(ATag) = 0 then
      begin
      AddDebugLine('Processing Results');
      if assigned(ResultsData) then
        begin
        Buffer := GetTagString(Tags[bttResultRecord]);
        ToolResult := ResultsData.AddResultFromString(Buffer);
        AddDebugLine('Processing Results Done');
        end;
      end
    end;

  if (ATag = Tags[bttShowResults]) then
    begin
    if Named.GetAsInteger(ATag) = 0 then
      begin
      AddDebugLine('Showing Results');
      ToolFilter := GetTagString(Tags[bttToolResultFilter]);
      ToolResultsForm.ShowWithFilter(ToolFilter);
      AddDebugLine('Showing Results Done');
      end;
    end;


  if (ATag = Tags[bttHideResults]) then
    begin
    if Named.GetAsInteger(ATag) = 0 then
      begin
      AddDebugLine('Hide Results');
      ToolResultsForm.Hide;
      AddDebugLine('Hide Results Done');
      end;
    end;

end;

procedure TToolManager.Execute;
begin
  if not Visible then begin
    btnHead.Enabled := True;
    btnToolChanger.Enabled := True;
    InfoPanel.Caption := Translator.GetString('$MODIFY_TAG_DATA');
    ShowModal;
  end;
end;


// This routine is not thread safe, so it just captures the data
// and inform the main loop that a change has occured
procedure TToolManager.C6x1Notify(Service: TC6x1; Msg : TIBBState);
var I : TBalluffDevice;
    StateDevice : TBalluffDevice;
begin
  StateDevice := bdNone;
  for I := Low(TBalluffDevice) to High(TBalluffDevice) do begin
    if (CompareText(Service.DeviceName, Device[I]) = 0) and
       (Head[I] = Service.Head) then begin
      StateDevice := I;
      Break;
    end;
  end;

  if StateDevice <> bdNone then begin
    if Msg = IBB_READ_COMPLETE then
      begin
      StateBuffer[StateDevice] := Service.ReadBuffer;
      end;
    StateIBB[StateDevice] := Msg;
    StateChange[StateDevice] := True;
  end;
end;

{function TToolManager.TimeEntryAddHours(StartTime: TDateTime;
  AddedHours: Integer): TDateTime;
begin
Result := StartTime+AddedHours/24;
end;}

function TToolManager.TTSOptionalTimeEntry(Field: Integer; FutureHours: Double): TDateTime;
begin
  Result := Now + FutureHours/24;

{  T := ToolEditor.Cells[1, Field];
  if T <> '' then begin
    Result := Now + StrToFloatDef(T, -1);
    if Result < Now then begin
      Result := StorageTimeToDateTime(T);
    end;
  end else begin
    Result := Now + Future;
  end;}
end;


{function TToolManager.OptionalTimeEntry(Field : Integer; Future : Double = 100) : TDateTime;
var T : string;
begin
  T := ToolEditor.Cells[1, Field];
  if T <> '' then begin
    Result := Now + StrToFloatDef(T, -1);
    if Result < Now then begin
      Result := StorageTimeToDateTime(T);
    end;
  end else begin
    Result := Now + Future;
  end;
end;}

function TToolManager.LogStrToFloat(Field : Integer) : Double;
var Msg : string;
begin
  try
    Result := StrToFloat(Trim(ToolEditor.Cells[1, Field]));
  except
    Msg := 'Invalid ' + ToolEditor.Cells[0, Field] + ' of "' + ToolEditor.Cells[1, Field] + '"';
    Logging(BtnSelect, Msg);
    raise Exception.Create(Msg);
  end;
end;

function TToolManager.LogStrToInt(Field : Integer) : Integer;
var Msg : string;
begin
  try
    Result := StrToIntDef(ToolEditor.Cells[1, Field], 0);
  except
    Msg := 'Invalid ' + ToolEditor.Cells[0, Field] + ' of "' + ToolEditor.Cells[1, Field] + '"';
    Logging(BtnSelect, Msg);
    raise Exception.Create(Msg);
  end;
end;

function TToolManager.HighPressureField(Field: Integer): Boolean;
begin
  Result:= False;
  if Trim(ToolEditor.Cells[1, Field]) = '1' then
    Result:= True;
end;

procedure TToolManager.RemoveToolAtEx1(Ex1 : Integer);
var Tmp : WideString;
    TmpSN : Integer;
begin
  if ToolLife.GetEx1ToolType(Ex1, Tmp) = trOK then begin
    ToolLife.GetEx1ToolSN(Ex1, TmpSN);
    ToolLife.RemoveTool(Tmp, TmpSN);
  end;
end;

//
// NOTE: This procedure may fail in the middle of adding a tool! Rather
//       than throwing a load of code at the problem I'll just raise an
//       error asking the operator to clean up the mess.  This is not
//       really acceptable but right now "needs must".
procedure TToolManager.AddToolFromEditor(Ex1 : Integer;EditorEntry:Boolean; UseTPMData : Boolean;OffsetVerify: Boolean=False);
  function Similar(A, B: Double): Boolean;
  begin
    Result:= Abs(A - B) < 0.0001;
  end;

var I : Integer;
    T : TToolDBItem;
    TD : TToolData;
    ToolReset :Boolean;
    Verify: Double;
begin

  //Named.SetAsInteger(Tags[bttToolAddFail],0);
  ToolReset := False;
  TD := ToolList.ToolAtEx1(Ex1);
  if  Assigned(TD) then
      begin
      // Remove the present tool
      RemoveToolAtEx1(Ex1);
      end;
  try
    if Trim(ToolEditor.Cells[1, ShuttleIDField]) = '' then
      raise Exception.Create('No tool name specified');

    if ToolLife.AddTool( Trim(ToolEditor.Cells[1, ShuttleIDField]),
                         LogStrToInt(ShuttleSNField),
                         LogStrToFloat(MaxToolLengthField),
                         LogStrToFloat(ActualLengthField), Ex1, 0) = trOK then begin

      for I := 0 to ToolList.Count - 1 do begin
        if ToolList[I].Ex1 = Ex1 then begin
          ToolList.BeginUpdate;
          try
            if UseTPMData then
               begin
//                Logging(bdNone, 'TPM Expiry Time Before = ' + ToolEditor.Cells[1, TPMTimeField] );
//                Logging(bdNone, 'Using TPM Data = ' + ToolEditor.Cells[1, TPMTimeField] );
//                Logging(bdNone, 'LoadedToolTPMHours = ' + IntToStr(LoadedToolTPMHours));
                ToolList[I].TPM1 := LoadedToolTPM1;
                ToolEditor.Cells[1,TPM1Field] := IntToStr(LoadedToolTPM1);
//                Logging(bdNone, 'TPM Expiry Time After = ' + ToolEditor.Cells[1, TPMTimeField] );
                ToolList[I].TPMTime := TTSOptionalTimeEntry(TPMTimeField,LoadedToolTPMHours);  // THIS DHOULD GO IN FIELD 3
//                Logging(bdNone, 'TPM Expiry Time After2 = ' + ToolEditor.Cells[1, TPMTimeField] );
               end
            else
               begin
//                Logging(bdNone, 'Not Using TPM Data = ' + ToolEditor.Cells[1, TPMTimeField] );
                ToolList[I].TPM1 := LogStrToInt(TPM1Field);
                ToolList[I].TPMTime := StorageTimeToDateTime(ToolEditor.Cells[1,TPMTimeField]);
//                Logging(bdNone, 'TPM Expiry Not Using TPMData = ' + ToolEditor.Cells[1, TPMTimeField] );
               end;
            ToolList[I].TPM2Time := StorageTimeToDateTime(ToolEditor.Cells[1,TPM2Field]);
            Logging(bdNone, 'TPM2Time = ' + ToolEditor.Cells[1,TPM2Field] );
            Logging(bdNone, 'ATFE TPM time = ' + FloatToStr(ToolList[I].TPMTime));
            ToolList[I].HP := HighPressureField(StatusFlagsField);
            ToolList[I].BarrelCount := LogStrToInt(BarrelCountField);
            ToolList[I].RemainingBarrels := LogStrToInt(RemainingBarrelsField);
            ToolList[I].MinToolLength := LogStrToFloat(MinToolLengthField);
            ToolList[I].UniqueToolOffsets[0] := LogStrToFloat(UniqueToolXField);
            ToolList[I].UniqueToolOffsets[1] := LogStrToFloat(UniqueToolYField);
            ToolList[I].UniqueToolOffsets[2] := LogStrToFloat(UniqueToolZField);
            Verify:= StrToFloatDef(ToolEditor.Cells[1, OffsetVerifyField], 0);

            if ToolList[I].TPMTime < Now then begin
              ToolList[I].RemainingBarrels := 0;
              ToolList[I].ActToolLength := 0;
//              Named.SetFaultA(ObjectName, '$TPM_TIME_EXCEEDED', FaultInterlock, nil);
            end;

            if not Similar (ToolList[I].UniqueToolOffsets[0] +
                            ToolList[I].UniqueToolOffsets[1] +
                            ToolList[I].UniqueToolOffsets[2], Verify) and OffsetVerify then begin
              ToolList[I].RemainingBarrels := 0;
              ToolList[I].ActToolLength := 0;
              Named.SetFaultA(ObjectName, '$OFFSET_VERIFICATION_FAILED', FaultInterlock, nil);
            end;

            if ToolList[I].BarrelCount = 0 then
              ToolList[I].BarrelCount:= 1;

            if not EditorEntry then begin
              ToolReset := CheckToolAddition(ToolList[I]); //returns true if toollife reset
            end
          finally
            ToolList.EndUpdate;
          end;

//          Logging(bdNone, 'UpdateWriteStrings in Add Tool' );
          UpdateWriteStrings;

          ToolDB.BeginUpdate;
          try
            T := ToolDB.FindTool(ToolEditor.Cells[1, ShuttleIDField]);
            if not Assigned(T) then begin
              T := ToolDB.AddTool;
            end;
            T.Assign(ToolList[I]);
          finally
            ToolDB.EndUpdate;
          end;
        end;
      end;
    if ToolReset and (Ex1 > 0) then
           begin
           //Writedata to tag
           Logging(bdNone, 'SettingWrite Tag' );
           Named.SetAsInteger(Tags[bttToolChangerWrite], 1);
           end;

    end;
  except
    on E : Exception do begin
      if FID <> 0 then begin
        Named.FaultResetID(FID);
        FID := 0;
      end;
      FID := Named.SetFaultA(ObjectName, '$FAILED_TO_ADD_TOOL' + E.Message, FaultInterlock, nil);
      RemoveToolAtEx1(Ex1);
    end;
  end;
end;

procedure TToolManager.AddTool(Ex1 : Integer; Buffer : string);
var TDB : TToolDBItem;
  function HighPressure(const Val: string): string;
  var Tmp: Integer;
  begin
    Tmp:= StrToIntDef(Val, 0);

    Result:= '0';
    if (Tmp and ToolData.TOOL_HIGH_PRESSURE) <> 0 then
      Result:= '1';
  end;

begin
  try
    try
      if DataType = 'TM' then begin
        if CncTypes.ReadDelimited(Buffer) <> '0101' then
          raise Exception.Create('Incorrect Version');
        ToolEditor.Cells[1, ShuttleIDField] := CncTypes.ReadDelimited(Buffer);
        ToolEditor.Cells[1, ShuttleSNField] := CncTypes.ReadDelimited(Buffer);
        ToolEditor.Cells[1, TPMTimeField] := CncTypes.ReadDelimited(Buffer);
        ToolEditor.Cells[1, StatusFlagsField] := HighPressure(CncTypes.ReadDelimited(Buffer));
        ToolEditor.Cells[1, BarrelCountField] := CncTypes.ReadDelimited(Buffer);
        ToolEditor.Cells[1, RemainingBarrelsField] := CncTypes.ReadDelimited(Buffer);
        ToolEditor.Cells[1, MaxToolLengthField] := CncTypes.ReadDelimited(Buffer);
        ToolEditor.Cells[1, MinToolLengthField] := CncTypes.ReadDelimited(Buffer);
        ToolEditor.Cells[1, ActualLengthField] := CncTypes.ReadDelimited(Buffer);
        ToolEditor.Cells[1, UniqueToolXField] := CncTypes.ReadDelimited(Buffer);
        ToolEditor.Cells[1, UniqueToolYField] := CncTypes.ReadDelimited(Buffer);
        ToolEditor.Cells[1, UniqueToolZField] := CncTypes.ReadDelimited(Buffer);
        ToolEditor.Cells[1, ExpiryField] := CncTypes.ReadDelimited(Buffer);
      end else if DataType = 'RR' then begin
        ToolEditor.Cells[1, ShuttleIDField] := CncTypes.ReadDelimited(Buffer);
        ToolEditor.Cells[1, ShuttleSNField] := CncTypes.ReadDelimited(Buffer);
        ToolEditor.Cells[1, MaxToolLengthField] := CncTypes.ReadDelimited(Buffer);
        ToolEditor.Cells[1, ActualLengthField] := CncTypes.ReadDelimited(Buffer);
        ToolEditor.Cells[1, TPMTimeField] := StorageDate(StrToFloatDef(CncTypes.ReadDelimited(Buffer),0));
        ToolEditor.Cells[1, MinToolLengthField] := CncTypes.ReadDelimited(Buffer);
        ToolEditor.Cells[1, UniqueToolXField] := CncTypes.ReadDelimited(Buffer);
        ToolEditor.Cells[1, UniqueToolYField] := CncTypes.ReadDelimited(Buffer);
        ToolEditor.Cells[1, UniqueToolZField] := CncTypes.ReadDelimited(Buffer);
        ToolEditor.Cells[1, TPM1Field] := CncTypes.ReadDelimited(Buffer);
        ToolEditor.Cells[1, TPM2Field] := StorageDate(StrToFloatDef(CncTypes.ReadDelimited(Buffer),0));
//        ToolEditor.Cells[1, TPM2Field] := CncTypes.ReadDelimited(Buffer);
        ToolEditor.Cells[1, UniqueToolCField] := CncTypes.ReadDelimited(Buffer);
        ToolEditor.Cells[1, StatusFlagsField] := CNCTypes.ReadDelimited(Buffer);
        ToolEditor.Cells[1, BarrelCountField] := CncTypes.ReadDelimited(Buffer);
        ToolEditor.Cells[1, RemainingBarrelsField] := CncTypes.ReadDelimited(Buffer);

      end else if DataType = 'Hypertac' then begin

        if Ex1 = 0 then begin
          ToolEditor.Cells[1, ShuttleIDField] := IntToStr(Named.GetAsInteger(Tags[bttHypertacToolCode]));
          ToolEditor.Cells[1, ShuttleSNField] := IntToStr(Named.GetAsInteger(Tags[bttHypertacSerialNumber]));
        end else begin
          ToolEditor.Cells[1, ShuttleIDField] := '';
          ToolEditor.Cells[1, ShuttleSNField] := '0';
        end;

//        Logging(bdNone, 'hypertac !!!!!!!!!' );

        ToolEditor.Cells[1, TPMTimeField] := ToolList.StorageDate(Now + 100);
        ToolEditor.Cells[1, StatusFlagsField] := '0';
        ToolEditor.Cells[1, BarrelCountField] := '1';
        ToolEditor.Cells[1, RemainingBarrelsField] := '1';

        ToolDB.Lock;
        try
          TDB := ToolDB.FindTool(IntToStr(Named.GetAsInteger(Tags[bttHypertacToolCode])));
          if Assigned(TDB) then begin
            ToolEditor.Cells[1, MaxToolLengthField] := Format('.3f', [TDB.MaxToolLength]);
            ToolEditor.Cells[1, MinToolLengthField] := Format('.3f', [TDB.MinToolLength]);
            ToolEditor.Cells[1, ActualLengthField] := Format('.3f', [TDB.MaxToolLength - TDB.MinToolLength]);
          end else begin
            ToolEditor.Cells[1, MaxToolLengthField] := '450';
            ToolEditor.Cells[1, MinToolLengthField] := '150';
            ToolEditor.Cells[1, ActualLengthField] := '300';
          end;
        finally
          ToolDB.Unlock;
        end;

        ToolEditor.Cells[1, UniqueToolXField] := '0';
        ToolEditor.Cells[1, UniqueToolYField] := '0';
        ToolEditor.Cells[1, UniqueToolZField] := '0';
//        Logging(bdNone, 'eXPIRYfIELD :' + IntToStr(ExpiryField) );

        ToolEditor.Cells[1, ExpiryField] := ToolList.StorageDate(Now + 100);

        btnHead.Enabled := False;
        btnToolChanger.Enabled := False;
        InfoPanel.Caption := 'Enter data for Hypertac tool';

        if not Visible then
          ShowModal

      end else begin
        Named.SetFaultA(ObjectName, 'Invalid Datatype programmed in ' + InstallName + '.cfg' + Datatype, FaultWarning, nil);
        Exit;
      end;

//    GlobalTpmData.UseTPMData := False;
    AddToolFromEditor(Ex1,False,False, False);
    except
      Named.SetFaultA(ObjectName, '$INVALID_TOOL_DATA', FaultWarning, nil);
    end;
  finally
  end;
end;

const BIdent : array[Boolean] of string = (
  '0', '1'
);
procedure TToolManager.UpdateDisplay;

var I : Integer;
    Ex1 : Integer;
    LoadedToolType : String;
    LoadedSerial : Integer;
    ToolTypeData : TTTSByToolTypeData;
    HTC : Integer;
    UserName : String;
    UserLevel : Integer;
    HardwareIsRotary : Integer;

begin
  Named.SetAsInteger(Tags[ttsValidClose],0);
  ExitEnable := False;
  for I := 1 to ToolEditor.RowCount - 1 do begin
    ToolEditor.Cells[1, I] := '';
  end;

  if btnSelect = bdHead then begin
    Ex1 := 0
  end else begin
    Ex1 := Named.GetAsInteger(Tags[bttToolChangerPosition]);
    if Ex1 = 0 then begin
      InfoPanel.Caption := Translator.getString('$TOOL_CHANGER_NOT_IN_POSITION');
      Exit;
    end;
  end;


  SetPageTo(TagEventsPage,False);
  LoadedTooltype := '';
  LoadedSerial := 0;
  ToolList.Lock;
  try
    for I := 0 to ToolList.Count - 1 do begin
      if Toollist[I].Ex1 = Ex1 then begin
        LoadedToolData := ToolList[I];
        ToolEditor.Cells[1, ShuttleIDField] := ToolList[I].ShuttleID;
        LoadedTooltype := ToolList[I].ShuttleID;
        ToolEditor.Cells[1, ShuttleSNField] := IntToStr(ToolList[I].ShuttleSN);
        LoadedSerial := ToolList[I].ShuttleSN;
        ToolEditor.Cells[1, TPMTimeField] := ToolList.StorageDate(ToolList[I].TPMTime);
        ToolEditor.Cells[1, StatusFlagsField] := BIdent[ToolList[I].HP];
        ToolEditor.Cells[1, BarrelCountField] := IntToStr(ToolList[I].BarrelCount);
        ToolEditor.Cells[1, RemainingBarrelsField] := FloatToStr(ToolList[I].RemainingBarrels);
        ToolEditor.Cells[1, MaxToolLengthField] := FloatToStr(ToolList[I].MaxToolLength);
        ToolEditor.Cells[1, MinToolLengthField] := FloatToStr(ToolList[I].MinToolLength);
        ToolEditor.Cells[1, ActualLengthField] := FloatToStr(ToolList[I].ActToolLength);
        ToolEditor.Cells[1, UniqueToolXField] := FloatToStr(ToolList[I].UniqueToolOffsets[0]);
        ToolEditor.Cells[1, UniqueToolYField] := FloatToStr(ToolList[I].UniqueToolOffsets[1]);
        ToolEditor.Cells[1, UniqueToolZField] := FloatToStr(ToolList[I].UniqueToolOffsets[2]);
        ToolEditor.Cells[1, UniqueToolCField] := FloatToStr(ToolList[I].UniqueToolOffsets[3]);
        ToolEditor.Cells[1, TPM1Field] := IntToStr(ToolList[I].TPM1);
        ToolEditor.Cells[1, TPM2Field] := ToolList.StorageDate(ToolList[I].TPM2Time);
      end;
    end;
  finally
    ToolList.Unlock;
  end;

  btnHeadRead.Visible := False;
  TPM2Panel.Visible := False;
  TPM2Panel.Align := AlNone;
  StandardTPMPanel.Visible := true;
  Panel16.Caption := 'On successful tool test, tpm will be set as follows:';
  StandardTPMPanel.Align := AlClient;
  Case PopUpType of
    tpuStandard:
    begin
    TagPages.ActivePage := TagEventsPage;
    SetPageTo(TagEventsPage,False);
    btnHeadRead.Visible := True;
    UserName := GetTagString(Tags[bttAccessUserName]);
    UserLevel := Named.GetAsInteger(Tags[bttAccessLevels]);
    if (UpperCase(UserName) = UpperCase(AccessNameForTPM2)) or (UserLevel > 126) then
      begin
      StandardTPMPanel.Visible := False;
      StandardTPMPanel.Align := AlNone;
      TPM2Panel.Visible := True;
      TPM2Panel.Align := AlClient;
      Panel16.Caption := 'Press TPM2 Reset to add the indicated number of Days';
      TPM2ValueEdit.Text := IntToStr(LoadedToolTPM2);
      end;
    end;

    tpuOnNonBalluffToolLoad:
    // NON BALLUFF TOOLs
    begin
    SetPageTo(NBTypePage,True);
    LoadedToolData := LoadDummyTool('AEC',1) ;
    if benchdebug =  1 then
      begin
      HTC :=  302;
      LoadedToolData.ShuttleSN :=  11;
      end
    else
      begin
      HTC :=  Named.GetAsInteger(Tags[bttHypertacToolCode]);
      end;

    ToolTypeData :=RRToolDataEditor.GetToolTypeDataForNonBallufType(HTC);
    if assigned(ToolTypeData) then
      begin
      LoadedToolData.ShuttleID := ToolTypeData.ToolType;
     //Here if known type of Non Balluff
      LoadedToolType := ToolTypeData.ToolType; // Make it look like data came from tag
      LoadedToolData.ShuttleSN :=  Named.GetAsInteger(Tags[bttHypertacSerialNumber]);
      SetStringTag(Tags[bttNonBalluffToolType],LoadedToolType);
      SetStringTag(Tags[bttDisplayToolType],LoadedToolType);
      Named.SetAsInteger(Tags[bttNonBalluffSerial],LoadedToolData.ShuttleSN);
      Named.SetAsInteger(Tags[bttDisplayToolSerial],LoadedToolData.ShuttleSN);
      LoadedSerial := LoadedToolData.ShuttleSN;
      LoadedUniqueToolEntry := RRToolDataEditor.GetUniqueToolDataFor(LoadedToolType,LoadedSerial);
      if assigned(LoadedUniqueToolEntry) then
         begin
         // Here if specific tool is known
         ConfigureForExistingNBTool(LoadedUniqueToolEntry,LoadedToolData,HTC);
         end
     else
         begin
         // Here if Type Is known bur serial number is not
         ConfigureForNewNBTool(LoadedToolData,ToolTypeData.IsRotary,HTC);
         end;
      end
    else
      begin
      ConfigureForUnknownNonBallufToolType(HTC,LoadedToolData.ShuttleSN);
      end;
   end;

    tpuOnToolLoad:
    begin
    btnHeadRead.Visible := False;
    TPM2Panel.Visible := False;
    TPM2Panel.Align := AlNone;
    StandardTPMPanel.Visible := true;
    Panel16.Caption := 'On successful tool test, tpm will be set as follows:';
    StandardTPMPanel.Align := AlClient;
    SetPageTo(OperatorPage,False);
    HardwareIsRotary := Named.GetAsInteger(Tags[bttRotaryToolFitted]);
    if benchdebug =  1 then
      begin
      LoadedToolType := 'NewAIM';
      LoadedSerial := 1;
      LoadedToolData := LoadDummyTool('NewAIM',1) ;
      end;
    ExpectedNGSerial := -1;

    if LoadedToolType <> '' then
        begin
        SetStringTag(Tags[bttDisplayToolType],LoadedToolType);
        ToolTypeData :=RRToolDataEditor.GetToolTypeDataFor(LoadedToolType);
        if assigned(ToolTypeData) then
          begin
          LoadedToolTPMHours := ToolTypeData.ExpiryHours;
//          Logging(bdNone,'Setting LoadedToolTPMHours'+ IntToStr(ToolTypeData.ExpiryHours));

          if assigned(ToolList[0]) then
            begin
//            ToolList[0].TPMHours := ToolTypeData.ExpiryHours;
            ToolList[0].TPM1 := ToolTypeData.TPM1Default;
            end
          else
            begin
            AddDebugLine('ToolList[0] did not exist');
            end;

          LoadedToolTPM1 := ToolTypeData.TPM1Default;
          LoadedToolTPM2 := ToolTypeData.TPM2Default;
          TPM1Display.Text := IntToStr(LoadedToolTPM1);
          TPMHoursDisplay.Text := IntToStr(LoadedToolTPMHours);
          LoadedUniqueToolEntry := RRToolDataEditor.GetUniqueToolDataFor(LoadedToolType,LoadedSerial);
          if assigned(LoadedUniqueToolEntry) then
            begin
            // here for existing tool
            ExpectedNGSerial := LoadedUniqueToolEntry.NGSerialNumber;
            Named.SetAsInteger(Tags[ttsNGSerial],ExpectedNGSerial);
            ConfigureForExistingTool(LoadedUniqueToolEntry,LoadedToolData);
            end
          else
            begin
            // here for Tool NOT In Unique Data Base
            ConfigureForNewTool(LoadedToolData,ToolTypeData.IsRotary);
            end
          end
        else
          begin
          // Here If tool type unknown
          if HardwareIsRotary <> 0 then
            begin
            ConfigureForUnknownToolType(LoadedToolData,True);
            end
          else
            begin
            ConfigureForUnknownToolType(LoadedToolData,False);
            end;
          end
        end
      else
        begin
        SetMemoStyle(Memo1,tmsWarning);
        Memo1.lines.add('Unable to read tool Data');
        Memo1.lines.add('Press OK and try to reload');
        ExitEnable := True;
        end;
      end;

    end;


  PopUpType := tpuStandard;
end;

const
  LogPrefix : array[TBalluffDevice] of string = (
    'Head: ', 'TC:   ', ''
  );

procedure TToolManager.Logging(D : TBalluffDevice; const Msg : string);
var I : Integer;
begin
  if LogMemo.Lines.Count > 100 then begin // ???? Buffer size in ini.
    LogMemo.Lines.BeginUpdate;
    try
      for I := 0 to 20 do
        LogMemo.Lines.Delete(0);
    finally
      LogMemo.Lines.EndUpdate;
    end;
  end;
  LogMemo.Lines.Add(LogPrefix[D] + Msg);
end;

procedure TToolManager.DoStateChange(D : TBalluffDevice);
var Ex1 : Integer;
begin
  StateChange[D] := False;

  case StateIBB[D] of
    IBB_IDLE : begin
      Logging(D, 'Idle');
    end;

    IBB_READ : begin
      Logging(D, 'Read');
    end;

    IBB_WRITE : begin
      Named.SetAsInteger(Tags[bttHeadWrite], 0);
      Named.SetAsInteger(Tags[bttToolChangerWrite], 0);
      Logging(D, 'Write');
    end;

    IBB_RESET : begin
      Named.SetAsInteger(Tags[bttHeadWrite], 0);
      Named.SetAsInteger(Tags[bttToolChangerWrite], 0);
      Named.SetAsInteger(Tags[bttIBBReset], 0);
      Logging(D, 'Reset');
    end;

    IBB_CONFIGURE : begin
      Named.SetAsInteger(Tags[bttHeadWrite], 0);
      Named.SetAsInteger(Tags[bttToolChangerWrite], 0);
      Logging(D, 'Configure');
    end;

    IBB_READ_COMPLETE : begin
      Logging(D, 'Read Complete');
      if D = bdHead then
        Ex1 := 0
      else
        Ex1 := Named.GetAsInteger(Tags[bttToolChangerPosition]);

      if D = bdHead then
        Logging(D, '--HEAD = ' + IntToStr(Ex1))
      else
        Logging(D, '--TC   = ' + IntToStr(Ex1));

      Logging(D, 'String = "' + StateBuffer[D] + '"');
      AddTool(Ex1, StateBuffer[D]);
    end;

    IBB_WRITE_COMPLETE : begin
      Logging(D, 'Write Complete');
    end;

    IBB_READ_ERROR : begin
      Logging(D, 'Read Error');
    end;

    IBB_WRITE_ERROR : begin
      Logging(D, 'Write Error');
    end;

    IBB_CONFIGURE_COMPLETE : begin
      Logging(D, 'Configure Complete');
    end;

    IBB_RESET_COMPLETE : begin
      Logging(D, 'Reset Complete');
    end;

    IBB_HEAD1 : begin
      Logging(D, 'Head1');
    end;

    IBB_HEAD2 : begin
      Logging(D, 'Head2');
    end;

  end;
  if Visible and (BtnSelect = D) then
    UpdateDisplay;
end;

procedure TToolManager.DisplaySweep;
var I : TBalluffDevice;
begin
  for I := Low(TBalluffDevice) to High(TBalluffDevice) do begin
    if StateChange[I] then begin
      DoStateChange(I);
    end;
  end;
end;


procedure TToolManager.btnToolChangerClick(Sender: TObject);
begin
  if BtnHead.Down then
    btnSelect := bdHead
  else
    begin
    btnSelect := bdToolChanger;
    //BtnWrite.Enabled :=  Named.GetAsBoolean(Tags[bttCanWriteAtToolDoor]);
    end;
  UpdateDisplay;
end;

// Any added tool is the last in the list?
function TToolManager.CheckToolAddition(T : TToolData):Boolean;
begin
  Result := False;
  ToolList.Lock;
  try
    if Named.GetAsBoolean(Tags[bttOperatorToolValidate])
       {and not Named.GetAsBoolean(Tags[bttBlockUpdateDatabase])} then begin

      if T.BarrelCount > 1 then begin
        if T.BarrelCount <> T.RemainingBarrels then begin
          Named.SetAsBoolean(Tags[bttToolDialogueActive], True);
          try
            if ToolOKDlg.TIsToolOK.Execute(T) then begin
              T.RemainingBarrels := T.BarrelCount;
              T.ActToolLength := 0;
              Result := True;
            end;
          finally
            Named.SetAsBoolean(Tags[bttToolDialogueActive], False);
          end
        end;
      end else
      begin
        if T.ActToolLength < T.MaxToolLength then begin
          Named.SetAsBoolean(Tags[bttToolDialogueActive], True);
          try
            if ToolOKDlg.TIsToolOK.Execute(T) then begin
              T.ActToolLength := T.MaxToolLength;
              Result := True;
            end;
          finally
            Named.SetAsBoolean(Tags[bttToolDialogueActive], False);
          end
        end;
      end;
    end;
  finally
    ToolList.Unlock;
  end;
end;

procedure TToolManager.ToolListChange(Sender: TObject);
begin
  LastToolListCount := ToolList.Count;
//  Logging(bdNone, 'UpdateWriteStrings in ToolListChange' );
  UpdateWriteStrings;
  UpdateExpectedTools;
end;

procedure TToolManager.CreateDefinitionStrings;
var I: Integer;
    Tmp: array [0..8] of string;
    Tool: TToolData;
begin
  for I:= 0 to 8 do begin
    Tmp[I]:= ',,';
  end;

  for I:= 0 to ToolList.Count - 1 do begin
    Tool:= ToolList.Tool[I];
    Tmp[Tool.Ex1 mod 9] := Tool.ShuttleID + ',' +
                           IntToStr(Tool.ShuttleSN) + ',' +
                           FloatToStr(Tool.TotalRemaining);
  end;

  for I:= 0 to 8 do begin
    Named.SetAsString(ToolDefTags[I], PChar(Tmp[I]));
  end;
end;

procedure TToolManager.UpdateWriteStrings;
var Tmp : Integer;
begin
  CreateDefinitionStrings;
  CreateBalluffString(0, Tags[bttHeadWriteString]);

  Tmp := Named.GetAsInteger(Tags[bttToolChangerPosition]);
  if Tmp > 0 then
    CreateBalluffString(Tmp, Tags[bttToolChangerWriteString]);
end;

procedure TToolManager.CreateBalluffString(Ex1: Integer; Tag: TTagRef);
  function ByteToBool(Tmp : Byte) : string;
  var I : Integer;
  begin
    Result := '';
    for I := 0 to 7 do begin
      if (Tmp and ($80 shr I)) <> 0 then
        Result := Result + '1'
      else
        Result := Result + '0';
    end;
  end;

  function FixedWidth(const Tmp : string; Width : Integer) : string;
  const Space = '               ';
  begin
    Result := Tmp + Copy(Space, 1, Width - Length(Tmp)) + ',';
  end;

  function FixedNWidth(const Tmp : string; Width : Integer) : string;
  const Space = '               ';
  begin
    Result := Copy(Space, 1, Width - Length(Tmp)) + Tmp + ',';
  end;

var I : Integer;
    Tmp : string;
    HTToolCode, HTToolSN : Integer;
    Ex1ErrorReset : Boolean;
begin
  Tmp := '';

  // if this is a request to update Ex1 then set a flag
  // reset the flag if a tool exists
  Ex1ErrorReset := Ex1 = 0;

  ToolList.Lock;
  try
    for I := 0 to ToolList.Count - 1 do begin
      if ToolList[I].Ex1 = Ex1 then begin
        if Ex1 = 0 then begin
          Ex1ErrorReset := False;
          if not Named.GetAsBoolean(Tags[bttBlockUpdateDatabase]) then begin
            HTToolCode := Named.GetAsInteger(Tags[bttHypertacToolCode]);
            HTToolSN := Named.GetAsInteger(Tags[bttHypertacSerialNumber]);
            if (HTToolCode <> 128) and
               (HTToolCode <> 480) and
               (HTToolCode <> 426) and
               (HTToolSN <> 0) and
               not Named.GetAsBoolean(Tags[bttFake]) then begin
              if not AllToolDB.UpdateTool( ToolList[I], HTToolCode, HTToolSN) then begin
                 Named.SetAsInteger(Tags[bttHypertacMismatch], 1);
                //if HypMismatchFID = 0 then
                //  HypMismatchFID := Named.SetFaultA( ObjectName, 'Hypertac does not match Balluff data "ToolCode / Serial number"', FaultRewind, HypertacFaultReset);
              end else begin
                Named.SetAsInteger(Tags[bttHypertacMismatch], 0);
                //if HypMismatchFID <> 0 then begin
                //  Named.FaultResetID(HypMismatchFID);
                //  HypMismatchFID := 0;
                //end;
              end;
            end;
          end;

          Named.SetAsDouble(Tags[bttUniqueToolOffsetX], ToolList[I].UniqueToolOffsets[0]);
          Named.SetAsDouble(Tags[bttUniqueToolOffsetY], ToolList[I].UniqueToolOffsets[1]);
          Named.SetAsDouble(Tags[bttUniqueToolOffsetZ], ToolList[I].UniqueToolOffsets[2]);
        end;
        if DataType = 'TM' then begin
          Tmp := '0101,';
          Tmp := Tmp + FixedWidth(ToolList[I].ShuttleID, 10);
          Tmp := Tmp + FixedNWidth(IntToStr(ToolList[I].ShuttleSN), 4);
          Tmp := Tmp + FixedWidth(ToolList.StorageDate(ToolList[I].TPMTime), 19);
          Tmp := Tmp + FixedWidth(ByteToBool(Byte(ToolList[I].StatusFlags)), 8);
          Tmp := Tmp + FixedNWidth(IntToStr(ToolList[I].BarrelCount), 2);
          Tmp := Tmp + FixedNWidth(IntToStr(ToolList[I].RemainingBarrels), 2);
          if NC.InchModeDefault then begin
            Tmp := Tmp + FixedNWidth(Format('%.3f', [ToolList[I].MaxToolLength]), 6);
            Tmp := Tmp + FixedNWidth(Format('%.3f', [ToolList[I].MinToolLength]), 6);
            Tmp := Tmp + FixedNWidth(Format('%.3f', [ToolList[I].ActToolLength]), 6);
            Tmp := Tmp + FixedNWidth(Format('%.4f', [ToolList[I].UniqueToolOffsets[0]]), 7);
            Tmp := Tmp + FixedNWidth(Format('%.4f', [ToolList[I].UniqueToolOffsets[1]]), 7);
            Tmp := Tmp + FixedNWidth(Format('%.4f', [ToolList[I].UniqueToolOffsets[2]]), 7);
          end else begin
            Tmp := Tmp + FixedNWidth(Format('%.1f', [ToolList[I].MaxToolLength]), 6);
            Tmp := Tmp + FixedNWidth(Format('%.1f', [ToolList[I].MinToolLength]), 6);
            Tmp := Tmp + FixedNWidth(Format('%.1f', [ToolList[I].ActToolLength]), 6);
            Tmp := Tmp + FixedNWidth(Format('%.3f', [ToolList[I].UniqueToolOffsets[0]]), 7);
            Tmp := Tmp + FixedNWidth(Format('%.3f', [ToolList[I].UniqueToolOffsets[1]]), 7);
            Tmp := Tmp + FixedNWidth(Format('%.3f', [ToolList[I].UniqueToolOffsets[2]]), 7);
          end;
          Tmp := Tmp + FixedWidth(ToolList.StorageDate(ToolList[I].Expiry), 19);
        end else begin
          Tmp:= Tmp + ToolList[I].ShuttleID + ',';
          Tmp:= Tmp + IntToStr(ToolList[I].ShuttleSN) + ',';
          Tmp:= Tmp + FloatToStr(ToolList[I].MaxToolLength) + ',';
          Tmp:= Tmp + FloatToStr(ToolList[I].ActToolLength) + ',';
//          Logging(bdNone,'BallufWtite TPMTime'+ FloatToStr(ToolList[I].TPMTime) + 'Ex1:'+ IntToStr(Ex1));
          Tmp:= Tmp + FloatToStr(ToolList[I].TPMTime) + ',';
          Tmp:= Tmp + FloatToStr(ToolList[I].MinToolLength) + ',';
          Tmp:= Tmp + FloatToStr(ToolList[I].UniqueToolOffsets[0]) + ',';
          Tmp:= Tmp + FloatToStr(ToolList[I].UniqueToolOffsets[1]) + ',';
          Tmp:= Tmp + FloatToStr(ToolList[I].UniqueToolOffsets[2]) + ',';
          Tmp:= Tmp + IntToStr(ToolList[I].TPM1) + ',';
          Tmp:= Tmp + FloatToStr(ToolList[I].TPM2Time) + ',';
          Tmp:= Tmp + FloatToStr(ToolList[I].UniqueToolOffsets[3]) + ',';
          Tmp:= Tmp + IntToStr(ToolList[I].StatusFlags) + ',';
          Tmp:= Tmp + IntToStr(ToolList[I].BarrelCount) + ',';
          Tmp:= Tmp + IntToStr(ToolList[I].RemainingBarrels) + ',';
          if Ex1 = 0 then begin
            Named.SetAsDouble(Tags[bttUniqueToolOffsetX], ToolList[I].UniqueToolOffsets[0]);
            Named.SetAsDouble(Tags[bttUniqueToolOffsetY], ToolList[I].UniqueToolOffsets[1]);
            Named.SetAsDouble(Tags[bttUniqueToolOffsetZ], ToolList[I].UniqueToolOffsets[2]);
            Named.SetAsDouble(Tags[bttUniqueToolOffsetC], ToolList[I].UniqueToolOffsets[3]);
          end;
        end;
      end;
    end;
    Named.SetAsString(Tag, PChar(Tmp));
  finally
    ToolList.Unlock;
  end;
  if Ex1ErrorReset then begin
    // No tool in head so remove the error
    Named.SetAsInteger(Tags[bttHypertacMismatch], 0);
  end;
  {
  if Ex1ErrorReset and (HypMismatchFID <> 0) then begin
    // Here if we have an error but there is not tool in the head
    Named.FaultResetID(HypMismatchFID);
    HypMismatchFID := 0;
  end;
  }
end;


procedure TToolManager.LocalizeForm;
  procedure TranslateGrid(VLE : TValueListEditor);
  var I: Integer;
  begin
    for I := 1 to VLE.RowCount - 1 do begin
      VLE.Cells[0, I] := Translator.GetString(VLE.Cells[0, I]);
    end;
  end;

var I : Integer;
begin
  Caption:= Translator.GetString(Caption);

  for I := 0 to ComponentCount - 1 do begin
    if Components[I] is TLabel then
      TLabel(Components[I]).Caption := Translator.GetString(TLabel(Components[I]).Caption)
    else if Components[I] is TButton then
      TButton(Components[I]).Caption := Translator.GetString(TButton(Components[I]).Caption)
    else if Components[I] is TGroupBox then
      TGroupBox(Components[I]).Caption := Translator.GetString(TGroupBox(Components[I]).Caption)
    else if Components[I] is TSpeedButton then
      TSpeedButton(Components[I]).Caption := Translator.GetString(TSpeedButton(Components[I]).Caption)
    else if Components[I] is TBitBtn then
      TBitBtn(Components[I]).Caption := Translator.GetString(TBitBtn(Components[I]).Caption)
    else if Components[I] is TValueListEditor then
      TranslateGrid(TValueListEditor(Components[I]));
  end;
end;

procedure TToolManager.SpeedButton3MouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  Named.SetAsInteger(Tags[bttIBBReset], 1);
end;

procedure TToolManager.SpeedButton3MouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  Named.SetAsInteger(Tags[bttIBBReset], 0);
end;

procedure TToolManager.BtnWriteMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
Ex1 : Integer;
begin
  Named.SetAsInteger(Tags[bttHeadRead],0);
  if btnSelect = bdHead then begin
    AddToolFromEditor(0, True, False,True);
    Named.SetAsInteger(Tags[bttHeadWrite], 1)
  end else begin
    Ex1 := Named.GetAsInteger(Tags[bttToolChangerPosition]);
    if Ex1 > 0 then begin
      AddToolFromEditor(Ex1,True,False, True);
      Named.SetAsInteger(Tags[bttToolChangerWrite], 1);
    end;
  end;
end;

procedure TToolManager.BtnWriteMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  Named.SetAsInteger(Tags[bttHeadRead],0);
  Named.SetAsInteger(Tags[bttHeadWrite], 0);
  Named.SetAsInteger(Tags[bttToolChangerWrite], 0);
end;


procedure TToolManager.FormShow(Sender: TObject);
begin
  Top := 40;
  Left := 50;
  NGModifyButton.Visible := True;
  NGSerialModified := False;
  Named.SetAsInteger(Tags[ttsFormActive],1);
  Named.SetAsInteger(Tags[ttsValidClose],0);
  SetInitialVisibilityStatus;
  UpdateDisplay;
end;


procedure TToolManager.UpdateExpectedTools;
var TCInf: Integer;
begin
  TCInf:= Named.GetAsInteger(Tags[bttToolChangerPosition]);
  TCInf:= ToolMonitor.Monitor(ToolList, ToolChangerToolCount, TCInf);
  Named.SetAsInteger(Tags[bttExpectedTools], TCInf);
end;


function TToolManager.GetTagString(ATag: TTagref): String;
var
Buffer  : array[0..256] of Char;
begin
Named.GetAsString(ATag,Buffer,255);
Result := Buffer;
end;


procedure TToolManager.ConfigureForNewNBTool( LoadedTool: TToolData;IsRotary : Boolean;HTC : Integer);
begin
SetPageTo(NBUniquePage,True);
OffsetConfirmButton.Visible := False;
OffsetMatchCheckBox.checked := False;
SetMemoStyle(NewToolMemo,tmsWarning);

NBAddHTC.Text          := IntToStr(HTC);
NBAddHTSN.Text         := IntToStr(LoadedTool.ShuttleSN);
NBAddToolType.Text     := LoadedTool.ShuttleID;

NBUniqueMemo.lines.Clear;
NBUniqueMemo.lines.add('This tool is not known to the Tool Test Station');
NBUniqueMemo.lines.add('');

if IsRotary then
  begin
  NewToolIsRotary := True;
  NBNewToolRequestLabel.Caption := 'Electrode Diameter';
  NBUniqueMemo.lines.add('Please Check the electrode diameter ');
  NBUniqueMemo.lines.add('and press Add Tool button');
  NBUniqueMemo.lines.add('or press Cancel to abort.');
  end
else
  begin
  NewToolIsRotary := False;
  NBNewToolRequestLabel.Caption := 'Noseguide Serial Number';
  NBUniqueMemo.lines.add('Please enter the serial number for the noseguide ');
  NBUniqueMemo.lines.add('and press Add Tool button');
  NBUniqueMemo.lines.add('or press Cancel to abort.');
  end;

NBNGSerialEdit.SetFocus;
end;

procedure TToolManager.PopupForLoadedTool(LTool: String);
begin
//First find NG Serial Number from Database in order to Display
end;
procedure TToolManager.ConfigureForFollowOnNBTool(LoadedTool: TToolData; IsRotary: Boolean;HTC : INteger);
begin
  SetPageTo(NBUniquePage,True);
  NBAddHTC.Text          := IntToStr(HTC);
  NBAddHTSN.Text         := IntToStr(LoadedTool.ShuttleSN);
  NBAddToolType.Text     := LoadedTool.ShuttleID;

  NBUniqueMemo.Lines.Clear;
  NBUniqueMemo.lines.add('This tool must now be added to database');
  NBUniqueMemo.lines.add('Check all Tag Data is correct and if Ok');
  NBUniqueMemo.lines.add('(These May Be edited if required)');
  NBUniqueMemo.lines.add('');
  if (IsRotary ) then
    begin
    NewToolIsRotary := True;
    NBNewToolRequestLabel.Caption := 'Electrode Diameter';
    NBUniqueMemo.lines.add('Then enter the electrode diameter ');
    NBUniqueMemo.lines.add('and press Add Tool Type button');
    NBUniqueMemo.lines.add('or press Cancel to abort.');
    end
  else
    begin
    NewToolIsRotary := False;
    NBNewToolRequestLabel.Caption := 'Noseguide Serial Number';
    NBUniqueMemo.lines.add('Then enter a serial number for the noseguide ');
    NBUniqueMemo.lines.add('and press Add Tool Type button');
    NBUniqueMemo.lines.add('or press Cancel to abort.');
    end;
  NBNGSerialEdit.SetFocus;
end;

procedure TToolManager.ConfigureForFollowOnTool(LoadedTool: TToolData;IsRotary : Boolean);
begin
  SetPageTo(NewToolPage,False);
  SetMemoStyle(NewToolMemo,tmsNormal);
  NewToolMemo.lines.add('This tool must now be added to database');
  NewToolMemo.lines.add('Check all Tag Data is correct and if Ok');
  NewToolMemo.lines.add('(These May Be edited if required)');
  NewToolMemo.lines.add('');
  if IsRotary then
    begin
    NewToolIsRotary := True;
    NewToolRequestLabel.Caption := 'Electrode Diameter';
    NewToolMemo.lines.add('Then enter the electrode diameter ');
    NewToolMemo.lines.add('and press Add Tool Type button');
    NewToolMemo.lines.add('or press Cancel to abort.');
    end
  else
    begin
    NewToolIsRotary := False;
    NewToolRequestLabel.Caption := 'Noseguide Serial Number';
    NewToolMemo.lines.add('Then enter a serial number for the noseguide ');
    NewToolMemo.lines.add('and press Add Tool Type button');
    NewToolMemo.lines.add('or press Cancel to abort.');
    end;



  NGSerialEnter.SetFocus;
end;

procedure TToolManager.ConfigureForExistingNBTool(UniqueTEntry: TTTSByUniqueToolData; LoadedTool: TToolData;HTC : INteger);
begin
  SetPageTo(NBConfirmPage,True);
  NBConfirmHTC.Text          := IntToStr(HTC);
  NBConfirmHTSN.Text         := IntToStr(UniqueTEntry.SerialNumber);
  NBConfirmToolType.Text     := UniqueTEntry.ToolType;
  Named.SetAsInteger(Tags[ttsNGSerial],UniqueTEntry.NGSerialNumber);
  NBConfirmMemo.Lines.Clear;
  if (UniqueTEntry.IsRotary = 1) then
    begin
    NBConfirmLabel.Caption := 'Electrode Diameter';
    NBConfirmDisplay.Text := Format('%5.3f',[UniqueTEntry.ElectrodeDiameter]);
    NBConfirmMemo.lines.add('Please Check Electrode Diameter');
    NBConfirmMemo.lines.add('and then press Confirm');
    NBConfirmMemo.lines.add('or press Cancel to abort.');
    end
  else
    begin
    NBConfirmLabel.Caption := 'Noseguide Serial Number';
    NBConfirmDisplay.Text := Format('%d',[UniqueTEntry.NGSerialNumber]);
    NBConfirmMemo.lines.add('Please Check NoseGuide Serial Number');
    NBConfirmMemo.lines.add('and then press Confirm');
    NBConfirmMemo.lines.add('or press Cancel to abort.');
    end;
end;

procedure TToolManager.ConfigureForExistingTool(UniqueTEntry : TTTSByUniqueToolData;LoadedTool: TToolData);
begin
  SetPageTo(OperatorPage,False);
  SetMemoStyle(Memo1,tmsNormal);
  NoseGuideConfirmButton.Visible := False;
  OffsetConfirmButton.Visible := True;
  OffsetsChangeConfirmed := False;
  if (abs(UniqueTEntry.UniqueToolOffsets[0] - LoadedTool.UniqueToolOffsets[0]) < 0.001)
  and (abs(UniqueTEntry.UniqueToolOffsets[1] - LoadedTool.UniqueToolOffsets[1]) < 0.001)
  and (abs(UniqueTEntry.UniqueToolOffsets[2] - LoadedTool.UniqueToolOffsets[2]) < 0.001) then
    begin
    // here if offsets Match
    OffsetMatchCheckBox.checked := True;
    OffsetMatchCheckBox.Caption := 'Offsets match database';
    Memo1.lines.add('Tool offsets match database');
    if (UniqueTEntry.IsRotary = 1)then
        begin
        ConfirmTitle.Caption := 'Electrode Diameter';
        ConfirmLabel.Caption := 'Electrode Dia.';
        Memo1.lines.add('Please Confirm Electrode Size');
        Memo1.lines.add(Format('Expected Electrode Size : %5.3f',[UniqueTEntry.ElectrodeDiameter]));
        NewNGInput.Text := Format('%5.3f',[UniqueTEntry.ElectrodeDiameter]);
        NGModifyButton.Visible := False;
        end
    else
        begin
        ConfirmTitle.Caption := 'Noseguide Serial Number';
        ConfirmLabel.Caption := 'Serial Number';
        NewNGInput.Text := IntToStr(UniqueTEntry.NGSerialNumber);
        Memo1.lines.add('Please Confirm NoseGuide Serial Number OK');
        Memo1.lines.add(Format('Expected S/N : %d',[UniqueTEntry.NGSerialNumber]));
        Memo1.lines.add('Otherwise press Change to update');
        end;
    OffsetConfirmButton.Visible := False;
    NoseGuideConfirmButton.Visible := True;
   end
  else
    begin
    OffsetMatchCheckBox.checked := False;
    SetMemoStyle(Memo1,tmsWarning);
    OffsetMatchCheckBox.Caption := 'New Offsets OK??';
    Memo1.lines.add('Tool offsets do not match database');
    Memo1.lines.add('If offsets OK please press Offset Confirm button');
    Memo1.lines.add('to update database');
    OffsetConfirmButton.Visible := True;
    end;
end;

procedure TToolManager.ConfigureForNewTool(LoadedTool: TToolData;IsRotary : Boolean);
begin

SetPageTo(NewToolPage,False);
OffsetConfirmButton.Visible := False;
OffsetMatchCheckBox.checked := False;
SetMemoStyle(NewToolMemo,tmsWarning);
NewToolIsRotary := IsRotary;
NewToolMemo.lines.add('This tool is not known to the Tool Test Station');
NewToolMemo.lines.add('Please check tag data is valid');
NewToolMemo.lines.add('and that offsets are correct.');
NewToolMemo.lines.add('(These May Be edited if required)');
NewToolMemo.lines.add('');
if not IsRotary then
  begin
  NewToolRequestLabel.Caption := 'Noseguide Serial Number';
  NewToolMemo.lines.add('Then enter a serial number for the noseguide ');
  NewToolMemo.lines.add('and press Add Tool button');
  NewToolMemo.lines.add('or press Cancel to abort.');
  end
else
  begin
  NewToolRequestLabel.Caption := 'Electrode Diameter';
  NewToolMemo.lines.add('Please Check electrode diameter is correct');
  NewToolMemo.lines.add('and press Add Tool button');
  NewToolMemo.lines.add('or press Cancel to abort.');
  end;
NGSerialEnter.SetFocus;
end;


procedure TToolManager.ConfigureForUnknownNonBallufToolType(HTC,HTSN : Integer);
begin
  SetPageTo(NBTypePage,True);
  NBToolNameEdit.Text := '???';
  NBHypertacDisplay.Text := IntToStr(HTC);
  SetMemoStyle(NonBalluffToolMemo,tmsWarning);
  NonBalluffToolMemo.lines.add('NON BALLUFF TAG TOOL LOADED');
  NonBalluffToolMemo.lines.add('No Data in Database for tool of this type');
  NonBalluffToolMemo.lines.add('Please Enter a Tool Type Name and then');
  NonBalluffToolMemo.lines.add('press the Add Tool button to configure tool type');
  NonBalluffToolMemo.lines.add('or Press Cancel');
  TPM1Display.Text := 'N/A';
  TPMHoursDisplay.Text := 'N/A';
  TagPages.ActivePage := NBTypePage;
end;

procedure TToolManager.ConfigureForUnknownToolType(TypeData: TToolData;RotaryHardware :  Boolean);
begin
  SetPageTo(AddToolTypePage,False);
  SetMemoStyle(AddToolMemo,tmsWarning);
  AddToolMemo.lines.add('No Data in Database for tool of this type');
  AddToolMemo.lines.add('Press Add Button to configure tool type');
  AddToolMemo.lines.add('or Press Cancel to access Manual Operations');
  TPM1Display.Text := '??';
  TPMHoursDisplay.Text := '??';
  TagPages.ActivePage := AddToolTypePage;
  AddToolData := TypeData;
end;

procedure TToolManager.AddToolTypeButtonClick(Sender: TObject);
var
AddedToolIsRotary : Boolean;
begin
if assigned(AddToolData) then
  begin
  if RRToolDataEditor.ExecuteForToolTypeAdd(AddToolData,AddedToolIsRotary) then
    begin
    ConfigureForFollowOnTool(LoadedToolData,AddedToolIsRotary);
    end
  end
else
  begin
  SetMemoStyle(AddToolMemo,tmsWarning);
  AddToolMemo.Lines.Add('No Data');
  end;
end;

procedure TToolManager.SetPageTo(DPage: TTabSheet;IsSmallFormat : Boolean);
begin
if (IsSmallFormat) then
  begin
  Width := 500;
  Height := 500;
  KBBackPanel.Visible := False;
  Panel9.Visible := False;
  end
else
  begin
  Width := 885;
  Height := 700;
  KBBackPanel.Visible := True;
  Panel9.Visible := True;
  end;

OperatorPage.TabVisible := False;
NBConfirmPage.TabVisible := False;
NBUniquePage.TabVisible := False;
NBTypePage.TabVisible := False;
TagEventsPage.TabVisible := False;
AddToolTypePage.TabVisible := False;
NewToolPage.TabVisible := False;
Dpage.TabVisible := True;
TagPages.ActivePage := DPage;
end;

procedure TToolManager.NBToolTypeAddButtonClick(Sender: TObject);
var
ToolTypeData : TTTSByToolTypeData;
HyperTacCode : Integer;
begin
  HyperTacCode := StrToIntDef(NBHypertacDisplay.Text,-1);
  if (NBToolNameEdit.Text <> '') and  (NBToolNameEdit.Text[1] <> '?')then
    begin
    ToolTypeData :=RRToolDataEditor.GetToolTypeDataFor(NBToolNameEdit.Text);
    if assigned(ToolTypeData) then
      begin
      SetMemoStyle(NonBalluffToolMemo,tmsWarning);
      NonBalluffToolMemo.Lines.Add('A tool type of that name aleady exists in the Database');
      NonBalluffToolMemo.Lines.Add('Please enter a different name and then press Add Tool again');
      NonBalluffToolMemo.Lines.Add('or press Cancel');
      end
    else
      begin
      ToolTypeData := RRToolDataEditor.ExecuteForNonBalluffToolTypeAdd(NBToolNameEdit.Text,HyperTacCode);

      if assigned(ToolTypeData) then //!!!!!!
        begin
        LoadedToolData.ShuttleID := ToolTypeData.ToolType;
        ConfigureForFollowOnNBTool(LoadedToolData,ToolTypeData.IsRotary,HyperTacCode);
        end
      end
    end
  else
    begin
    SetMemoStyle(NonBalluffToolMemo,tmsWarning);
    NonBalluffToolMemo.Lines.Add('No Tool TYpe Name entered');
    NonBalluffToolMemo.Lines.Add('Please enter a valid name and then press Add Tool again');
    NonBalluffToolMemo.Lines.Add('or press Cancel');
    end;
end;

procedure TToolManager.NewToolAddButtonClick(Sender: TObject);
var
NGS : Integer;
EDia : Double;
tmp : TTTSByUniqueToolData;

begin
//Here to add new tool to database
NGS := 0;
EDia := 0;
if assigned(LoadedToolData) then
  begin
  if not NewToolIsRotary then
    begin
    NGS := StrToIntDef(NGSerialEnter.Text,-1);
    if NGS < 1 then
      begin
      SetMemoStyle(NewToolMemo,tmsWarning);
      NewToolMemo.lines.add('Invalid noseguide serial number entered');
      NewToolMemo.lines.add('Please re-enter');
      NewToolMemo.lines.add('or press Cancel');
      exit
      end
    end
  else
    begin
    EDia := StrToFloatDef(NGSerialEnter.Text,-1);
    if EDia < 0 then
      begin
      SetMemoStyle(NewToolMemo,tmsWarning);
      NewToolMemo.lines.add('Electrode Diameter entered');
      NewToolMemo.lines.add('Please re-enter');
      NewToolMemo.lines.add('or press Cancel');
      exit
      end
    end;

  tmp := RRToolDataEditor.AddUniqueToolFromTagData(LoadedToolData,NGS,EDia,NewToolIsRotary);
  if not assigned(tmp) then
    begin
    SetMemoStyle(NewToolMemo,tmsWarning);
    NewToolMemo.lines.add('Error Adding New Tool');
    NewToolMemo.lines.add('A tool of this type already exists');
    end
  else
    begin
    Named.SetAsInteger(Tags[ttsValidClose],1);
    Close;
    end;
  end
else
  begin
  SetMemoStyle(NewToolMemo,tmsWarning);
  NewToolMemo.lines.add('Invalid Data ???');
  end;
end;

function TToolManager.LoadDummyTool(ToolType: Widestring; Serial : Integer): TToolData;
begin
Result :=TToolData.Create(ToolList,0);
Result.Ex1 := 0;
Result.ShuttleID := ToolType;
Result.ShuttleSN := Serial;
Result.BarrelCount := 1;
Result.UniqueToolOffsets[0] := 0;
Result.UniqueToolOffsets[1] := 0;
Result.UniqueToolOffsets[2] := 0;
Result.UniqueToolOffsets[3] := 0;
Result.UniqueToolOffsets[4] := 0;
ToolList.ToolList.Add(Result);
end;

procedure TToolManager.OffsetConfirmButtonClick(Sender: TObject);
begin
OffsetsChangeConfirmed := True;
OffsetMatchCheckBox.Caption := 'New Offsets Confirmed';
OffsetMatchCheckBox.Checked := True;
SetMemoStyle(NewToolMemo,tmsNormal);
Memo1.lines.add('Please Confirm NoseGuide Serial Number OK');
Memo1.lines.add(Format('Expected S/N : %d',[ExpectedNGSerial]));
Memo1.lines.add('Otherwise press Change to update');
OffsetConfirmButton.Visible     := False;
NoseGuideConfirmButton.Visible  := True;
end;

procedure TToolManager.NoseGuideConfirmButtonClick(Sender: TObject);
var
NewNGSerial : INteger;
begin
if OffsetsChangeConfirmed  then
  begin
  OffsetsChangeConfirmed := False;
  if RRToolDataEditor.UpdateToolOffsetsFromTagData(LoadedToolData) then
    begin
    Named.SetAsInteger(Tags[ttsValidClose],1);
    Close;
    end;
  end
else
  begin
  if OffsetMatchCheckBox.Checked then
    begin
    if NGSerialModified then
      begin
      NGSerialModified := False;
      NewNGSerial := StrToIntDef(NewNGInput.Text,-1);
      If NewNGSerial >0 then
        begin
        if RRToolDataEditor.UpdateNGSerialForTagData(LoadedToolData,NewNGSerial) then
          begin
          Named.SetAsInteger(Tags[ttsValidClose],1);
          RRToolDataEditor.FlushAllToolDataBaseToDisk;
          Named.SetAsInteger(Tags[ttsValidClose],1);
          Named.SetAsInteger(Tags[ttsNGSerial],NewNGSerial);
          Close;
          end
        else
          begin
          SetMemoStyle(Memo1,tmsWarning);
          Memo1.Lines.Add('Error Updating Data Base');
          end
        end
      else
          begin
          SetMemoStyle(Memo1,tmsWarning);
          Memo1.Lines.Add('Invalid Serial Number Input');
          Memo1.Lines.Add('Number must be greater than zero');
          Memo1.Lines.Add('Please Re-enter');
          end;
      end
    else
      begin
      Named.SetAsInteger(Tags[bttNonBalluffSerial],LoadedToolData.ShuttleSN);
      Named.SetAsInteger(Tags[bttDisplayToolSerial],LoadedToolData.ShuttleSN);
      Named.SetAsInteger(Tags[ttsValidClose],1);
      AddToolFromEditor(0,True,True,False);
//      Logging(bdNone, 'Add Tool Done from Confirm Button' );
      Close;
      end;
    end
  else
    begin
    SetMemoStyle(Memo1,tmsWarning);
    Memo1.Lines.Add('Change to Offsets Not Confirmed')
    end;
  end;
end;



procedure TToolManager.NGModifyButtonClick(Sender: TObject);
begin
  NGSerialModified := True;
  SetMemoStyle(Memo1,tmsWarning);
  Memo1.Lines.Add('Enter New Noseguide Serial Number');
  Memo1.Lines.Add('Then Press Confirm to start testing');
end;

procedure TToolManager.AbandonButtonClick(Sender: TObject);
begin
  Named.SetAsInteger(Tags[ttsValidClose],0);
  Close;
end;

procedure TToolManager.SetInitialVisibilityStatus;
begin
OffsetConfirmButton.Visible := False;
end;

procedure TToolManager.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Named.SetAsInteger(Tags[ttsFormActive],0);
end;

procedure TToolManager.SetMemoStyle(Memo: TMemo; MemoStyle: TMemoStlyes);
begin
if assigned(Memo) then
  begin
  Memo.Lines.Clear;
  case MemoStyle of
    tmsNormal:
    begin
    Memo.Font.Color := clBlack;
    Memo.Color := clWhite;
    end;

    tmsWarning:
    begin
    Memo.Font.Color := clYellow;
    Memo.Color := clRed;
    end;
  end;
  end;
end;

procedure TToolManager.AddDebugLine(InString: String);
begin
if ShowDebug <> 0 then
  begin
  DebugMemo.Lines.Add(InString);
  end;
end;


procedure TToolManager.btnHeadReadMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  Named.SetAsInteger(Tags[bttHeadRead],1);
end;

procedure TToolManager.btnHeadReadMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  Named.SetAsInteger(Tags[bttHeadRead],0);
end;


procedure TToolManager.NBAddToolButtonClick(Sender: TObject);
var
NGS : INteger;
EDia : Double;
begin
//Here to add new Non Balluff tool to database
NGS := 0;
EDia := 0;
SetStringTag(Tags[ttsNGSerial],' ');
if assigned(LoadedToolData) then
  begin
  if not NewToolIsRotary then
    begin
    NGS := StrToIntDef(NBNGSerialEdit.Text,-1);
    if NGS < 1 then
      begin
      SetMemoStyle(NBUniqueMemo,tmsWarning);
      NBUniqueMemo.lines.add('Invalid noseguide serial number entered');
      NBUniqueMemo.lines.add('Please re-enter');
      NBUniqueMemo.lines.add('or press Cancel');
      exit;
      end
    else
      begin
      SetStringTag(Tags[ttsNGSerial],IntToStr(NGS));
      end
    end
  else
    begin
    EDia := StrToFloatDef(NBNGSerialEdit.Text,-1);
    if EDia < 0 then
      begin
      SetMemoStyle(NBUniqueMemo,tmsWarning);
      NBUniqueMemo.lines.add('Electrode Diameter entered');
      NBUniqueMemo.lines.add('Please re-enter');
      NBUniqueMemo.lines.add('or press Cancel');
      exit;
      end
    end;

  LoadedUniqueToolEntry := RRToolDataEditor.AddUniqueToolFromTagData(LoadedToolData,NGS,EDia,NewToolIsRotary);
  if not assigned(LoadedUniqueToolEntry) then
    begin
    SetMemoStyle(NewToolMemo,tmsWarning);
    NBUniqueMemo.lines.add('Error Adding New Tool');
    NBUniqueMemo.lines.add('A tool of this type already exists');
    end
  else
    begin
    SetStringTag(Tags[bttNonBalluffToolType],LoadedUniqueToolEntry.ToolType);
    Named.SetAsInteger(Tags[bttNonBalluffSerial],LoadedUniqueToolEntry.SerialNumber);
    Named.SetAsInteger(Tags[ttsValidClose],1);
    Close;
    end;
  end
else
  begin
  SetMemoStyle(NewToolMemo,tmsWarning);
  NewToolMemo.lines.add('Invalid Data ???');
  end;

end;



procedure TToolManager.SetStringTag(aTag: TTagRef; StrVal: String);
var
buffer : array [0..255] of char;
I : INteger;
begin
For I := 0 to Length(StrVal)-1 do
  begin
  Buffer [I] := StrVal[i+1]
  end;
Buffer [I] := Char(0);
Named.SetAsString(ATag,Buffer);
end;

procedure TToolManager.NBConfirmButtonClick(Sender: TObject);
begin
  Named.SetAsInteger(Tags[ttsValidClose],1);
  Close;

end;

procedure TToolManager.TPM2BtnMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var
DayCount : INteger;
begin
DayCount := StrToIntDef(TPM2ValueEdit.Text,-1);
if DayCount > 0 then
  begin
  ToolEditor.Cells[1, TPM2Field] := IntToStr(DayCount);
  Named.SetAsInteger(Tags[bttHeadRead],0);
  AddToolFromEditor(0, True, False,True);
  Named.SetAsInteger(Tags[bttHeadWrite], 1)
  end
 else
  begin
  raise Exception.Create('Invalid TPM2 Value. Must be a positive Integer');
  end;
end;

procedure TToolManager.TPM2BtnMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  Named.SetAsInteger(Tags[bttHeadRead],0);
  Named.SetAsInteger(Tags[bttHeadWrite], 0);
  Named.SetAsInteger(Tags[bttToolChangerWrite], 0);
end;

initialization
  TAbstractFormPlugin.AddPlugin('ToolForm', TToolManager);
end.
