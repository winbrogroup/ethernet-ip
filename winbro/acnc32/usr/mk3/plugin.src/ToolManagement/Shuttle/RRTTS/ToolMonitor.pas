unit ToolMonitor;

interface

uses SysUtils, CncTypes, NamedPlugin, ToolData, AbstractPlcPlugin;

type
  TToolMonitor = class
  public
    function Monitor(ToolList: TToolList; Total: Integer; Position: Integer): Integer;
  end;

  TFixedStationValue = (
    fsv1,
    fsv2,
    fsv3,
    fsv4,
    fsv5,
    fsv6,
    fsv7,
    fsv8,
    fsvEx1_1,
    fsvEx1_2,
    fsvEx1_3,
    fsvEx1_4,
    fsvEx1_5,
    fsvEx1_6,
    fsvEx1_7,
    fsvEx1_8
  );

  TFixedStationValues = set of TFixedStationValue;

  TFixedStationToEx1 = class(TAbstractPlcPlugin)
  private
    Stations: TFixedStationValues;
    TotalStations: Integer;
  public
    constructor Create( Parameters : string;
                        ResolveDeviceEv : TPlcPluginResolveDeviceEv;
                        ResolveItemEv : TPlcPluginResolveItemEv); override;
    procedure Compile ( var Embedded : TEmbeddedData;
                        aReadEv : TPlcPluginReadEv;
                        ResolveItemEv : TPlcPluginResolveItemEv;
                        Write : Boolean);  override;
    function Read(var Embedded : TEmbeddedData) : Boolean; override;
    procedure Write(var Embedded : TEmbeddedData; Line, LastLine : Boolean); override;
    procedure GatePre; override;
    function GatePost : Integer; override;
    function GateName(var Embedded : TEmbeddedData) : string; override;
    function ItemProperties : string; override;
  end;

implementation

uses ToolForm;

const
  PropertyName : array [TFixedStationValue] of TPlcProperty = (
    ( Name : 'Station1';        ReadOnly : False ),
    ( Name : 'Station2';        ReadOnly : False ),
    ( Name : 'Station3';        ReadOnly : False ),
    ( Name : 'Station4';        ReadOnly : False ),
    ( Name : 'Station5';        ReadOnly : False ),
    ( Name : 'Station6';        ReadOnly : False ),
    ( Name : 'Station7';        ReadOnly : False ),
    ( Name : 'Station8';        ReadOnly : False ),
    ( Name : 'Ex1Mismatch1';    ReadOnly : True ),
    ( Name : 'Ex1Mismatch2';    ReadOnly : True ),
    ( Name : 'Ex1Mismatch3';    ReadOnly : True ),
    ( Name : 'Ex1Mismatch4';    ReadOnly : True ),
    ( Name : 'Ex1Mismatch5';    ReadOnly : True ),
    ( Name : 'Ex1Mismatch6';    ReadOnly : True ),
    ( Name : 'Ex1Mismatch7';    ReadOnly : True ),
    ( Name : 'Ex1Mismatch8';    ReadOnly : True )
  );

{ TToolMonitor6 }

const
  Lookup6: array [0..5,0..5] of Integer = (
    ($01, $02, $04, $08, $10, $20),
    ($20, $01, $02, $04, $08, $10),
    ($10, $20, $01, $02, $04, $08),
    ($08, $10, $20, $01, $02, $04),
    ($04, $08, $10, $20, $01, $02),
    ($02, $04, $08, $10, $20, $01)
  );

  Lookup8: array [0..7,0..7] of Integer = (
    ($01, $02, $04, $08, $10, $20, $40, $80),
    ($80, $01, $02, $04, $08, $10, $20, $40),
    ($40, $80, $01, $02, $04, $08, $10, $20),
    ($20, $40, $80, $01, $02, $04, $08, $10),
    ($10, $20, $40, $80, $01, $02, $04, $08),
    ($08, $10, $20, $40, $80, $01, $02, $04),
    ($04, $08, $10, $20, $40, $80, $01, $02),
    ($02, $04, $08, $10, $20, $40, $80, $01)
  );

function TToolMonitor.Monitor(ToolList: TToolList; Total: Integer; Position: Integer): Integer;
var TD : TToolData;
    Ex1: Integer;
    I: Integer;
begin
  Result:= 0;
  if Position <> 0 then begin
    if Total = 6 then begin
      for I:= 0 to ToolList.Count - 1 do begin
        TD:= ToolList.Tool[I];
        if Assigned(TD) then begin
          Ex1:= TD.Ex1;
          if (Ex1 >= 1) and (Ex1 <= 6) then
            Result:= Result + Lookup6[Ex1 - 1, Position - 1];
        end;
      end;
    end
    else if Total = 8 then begin
      for I:= 0 to ToolList.Count - 1 do begin
        TD:= ToolList.Tool[I];
        if Assigned(TD) then begin
          Ex1:= TD.Ex1;
          if (Ex1 >= 1) and (Ex1 <= 8) then
            Result:= Result + Lookup8[Ex1 - 1, Position - 1];
        end;
      end;
    end;
  end;
end;

{ TFixedStationToEx1 }

procedure TFixedStationToEx1.Write(var Embedded: TEmbeddedData; Line,
  LastLine: Boolean);
begin
  if Line <> LastLine then begin
    if Line and not LastLine then
      Include(Stations, TFixedStationValue(Embedded.Command))
    else
      Exclude(Stations, TFixedStationValue(Embedded.Command))
  end;
end;

procedure TFixedStationToEx1.Compile(var Embedded: TEmbeddedData;
  aReadEv: TPlcPluginReadEv; ResolveItemEv: TPlcPluginResolveItemEv;
  Write: Boolean);
var I: TFixedStationValue;
    Token: string;
begin
  if aReadEv(Self) <> '.' then
    raise Exception.Create('Period expected');

  Token := aReadEv(Self);
  for I := Low(I) to High(I) do begin
    if CompareText(PropertyName[I].Name, Token) = 0 then begin
      Embedded.Command := Ord(I);
      if Write and PropertyName[I].ReadOnly then
        raise Exception.CreateFmt('Property "%s" is read only', [Token]);
      Exit;
    end;
  end;
  raise Exception.CreateFmt('Unknown property "%d"', [Token]);
end;

function TFixedStationToEx1.GateName(var Embedded: TEmbeddedData): string;
begin
  Result := PropertyName[TFixedStationValue(Embedded.Command)].Name;
end;

constructor TFixedStationToEx1.Create(Parameters: string;
  ResolveDeviceEv: TPlcPluginResolveDeviceEv;
  ResolveItemEv: TPlcPluginResolveItemEv);
var Token: string;
begin
  Token:= ReadDelimited(Parameters, ';');
  TotalStations:= StrToInt(Token);
  if (TotalStations <> 6) and (TotalStations <> 8) then
    raise Exception.Create('On 6 or 8 tool changer supported at the moment');
end;

function TFixedStationToEx1.ItemProperties: string;
var I: TFixedStationValue;
begin
  for I := Low(I) to High(I) do
    Result := Result + PropertyName[I].Name + CarRet + BooleanIdent[I in Stations] + CarRet;
end;

function TFixedStationToEx1.Read(var Embedded: TEmbeddedData): Boolean;
begin
  Result:= TFixedStationValue(Embedded.Command) in Stations;
end;

function TFixedStationToEx1.GatePost: Integer;

  function Ex1IndexFromFixed(TC: Integer; fsv: TFixedStationValue): TFixedStationValue;
  begin
    // fsv [0..5]
    // TC [1..6]
    // Answer [8..13]
    TC:= (TC + TotalStations - 1 - Ord(fsv)) mod TotalStations;
    Result:= TFixedStationValue(TC + Ord(fsvEx1_1));
  end;

var I: TFixedStationValue;
    TC: Integer;
begin
  // Remove any outputs leaving behind the inputs
  Stations := Stations * [fsv1..fsv8];

  // If the inputs = <> our work is done
  if Stations <> [] then begin
    TC:= Named.GetAsInteger(ToolManager.Tags[bttToolChangerPosition]);
    if TC <> 0 then begin
      for I := fsv1 to fsv8 do begin
        if I in Stations then begin
          Include(Stations, Ex1IndexFromFixed(TC, I));
        end
      end;
    end;
  end;
  Result:= 0;
end;

procedure TFixedStationToEx1.GatePre;
begin
  // Do nothing
end;

initialization
  TFixedStationToEx1.AddPlugin('ToolFixedStationToEx1', TFixedStationToEx1);
end.
