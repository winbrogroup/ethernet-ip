object RRToolDataEditor: TRRToolDataEditor
  Left = 0
  Top = 0
  Width = 820
  Height = 607
  Caption = 'By Type Tool DB Editor'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object BYTypeTitle: TPanel
    Left = 0
    Top = 0
    Width = 812
    Height = 33
    Align = alTop
    BevelInner = bvLowered
    Caption = '----'
    Color = clInfoBk
    TabOrder = 0
  end
  object MainPanel: TPanel
    Left = 0
    Top = 33
    Width = 812
    Height = 540
    Align = alClient
    BevelInner = bvLowered
    TabOrder = 1
    object ByToolEditPage: TPageControl
      Left = 2
      Top = 2
      Width = 808
      Height = 536
      ActivePage = NBTypeAddPage
      Align = alClient
      TabOrder = 0
      TabStop = False
      object UniqueToolEditPage: TTabSheet
        Caption = 'Unique Tool Data'
        object UTTypeLabel: TLabel
          Left = 16
          Top = 32
          Width = 67
          Height = 18
          Caption = 'Tool Type'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object Label1: TLabel
          Left = 16
          Top = 67
          Width = 90
          Height = 18
          Caption = 'Serial Number'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object Label2: TLabel
          Left = 16
          Top = 134
          Width = 88
          Height = 18
          Caption = 'Tool X Offset'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object Label3: TLabel
          Left = 16
          Top = 206
          Width = 87
          Height = 18
          Caption = 'Tool Z Offset'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object Label4: TLabel
          Left = 16
          Top = 169
          Width = 89
          Height = 18
          Caption = 'Tool Y Offset'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object Label5: TLabel
          Left = 16
          Top = 241
          Width = 88
          Height = 18
          Caption = 'Tool C Offset'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object Label6: TLabel
          Left = 16
          Top = 99
          Width = 141
          Height = 18
          Caption = 'Noseguide Serial Num'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object UniqueListPanel: TPanel
          Left = 0
          Top = 322
          Width = 800
          Height = 186
          Align = alBottom
          BevelInner = bvLowered
          TabOrder = 7
        end
        object UTTypeEdit: TEdit
          Left = 192
          Top = 28
          Width = 225
          Height = 26
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          OnChange = UtEditChange
          OnKeyUp = UTeditKeyUp
        end
        object UTSerialEdit: TEdit
          Left = 192
          Top = 63
          Width = 97
          Height = 26
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 1
        end
        object UTXoffEdit: TEdit
          Left = 192
          Top = 130
          Width = 97
          Height = 26
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 3
        end
        object UTYoffEdit: TEdit
          Left = 192
          Top = 165
          Width = 97
          Height = 26
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 4
        end
        object UTZoffEdit: TEdit
          Left = 192
          Top = 202
          Width = 97
          Height = 26
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 5
        end
        object UTCoffEdit: TEdit
          Left = 192
          Top = 237
          Width = 97
          Height = 26
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 6
        end
        object UTNGEdit: TEdit
          Left = 192
          Top = 95
          Width = 97
          Height = 26
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 2
        end
        object Panel2: TPanel
          Left = 480
          Top = 27
          Width = 199
          Height = 28
          BevelInner = bvLowered
          TabOrder = 8
          object UNavFirstBut: TSpeedButton
            Left = 3
            Top = 3
            Width = 49
            Height = 22
            Caption = 'First'
            OnClick = UtNavButtonClick
          end
          object UnavNextBut: TSpeedButton
            Left = 51
            Top = 3
            Width = 49
            Height = 22
            Caption = 'Next'
            OnClick = UtNavButtonClick
          end
          object UnavPrevBut: TSpeedButton
            Left = 99
            Top = 3
            Width = 49
            Height = 22
            Caption = 'Prev'
            OnClick = UtNavButtonClick
          end
          object UnavLastBut: TSpeedButton
            Left = 147
            Top = 3
            Width = 49
            Height = 22
            Caption = 'Last'
            OnClick = UtNavButtonClick
          end
        end
      end
      object ByTypeEditPage: TTabSheet
        Caption = 'Tool Type Data'
        ImageIndex = 1
        object Label7: TLabel
          Left = 16
          Top = 16
          Width = 67
          Height = 18
          Caption = 'Tool Type'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object Label8: TLabel
          Left = 16
          Top = 179
          Width = 100
          Height = 18
          Caption = 'Noseguide Flow'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object Label9: TLabel
          Left = 16
          Top = 51
          Width = 102
          Height = 18
          Caption = 'Electrode Count'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object Label10: TLabel
          Left = 16
          Top = 147
          Width = 92
          Height = 18
          Caption = 'Electrode Flow'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object Label11: TLabel
          Left = 16
          Top = 211
          Width = 56
          Height = 18
          Caption = 'Pressure'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object Label14: TLabel
          Left = 128
          Top = 115
          Width = 58
          Height = 18
          Caption = 'Minimum'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object Label15: TLabel
          Left = 272
          Top = 115
          Width = 64
          Height = 18
          Caption = 'Maximum'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object Label63: TLabel
          Left = 376
          Top = 211
          Width = 21
          Height = 18
          Caption = 'bar'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object Label64: TLabel
          Left = 368
          Top = 147
          Width = 44
          Height = 18
          Caption = 'ml/min'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object Label65: TLabel
          Left = 368
          Top = 179
          Width = 44
          Height = 18
          Caption = 'ml/min'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object Label21: TLabel
          Left = 16
          Top = 83
          Width = 86
          Height = 18
          Caption = 'Chanel Count'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object TypeListPanel: TPanel
          Left = 0
          Top = 344
          Width = 800
          Height = 164
          Align = alBottom
          BevelInner = bvLowered
          TabOrder = 8
        end
        object Panel3: TPanel
          Left = 440
          Top = 11
          Width = 199
          Height = 28
          BevelInner = bvLowered
          TabOrder = 9
          object BTNavFirstBut: TSpeedButton
            Left = 3
            Top = 3
            Width = 49
            Height = 22
            Caption = 'First'
            OnClick = BTNavButtonClick
          end
          object BTNavNextBut: TSpeedButton
            Left = 51
            Top = 3
            Width = 49
            Height = 22
            Caption = 'Next'
            OnClick = BTNavButtonClick
          end
          object BTNavPrevBut: TSpeedButton
            Left = 99
            Top = 3
            Width = 49
            Height = 22
            Caption = 'Prev'
            OnClick = BTNavButtonClick
          end
          object BTNavLastBut: TSpeedButton
            Left = 147
            Top = 3
            Width = 49
            Height = 22
            Caption = 'Last'
            OnClick = BTNavButtonClick
          end
        end
        object BtTypeEdit: TEdit
          Left = 128
          Top = 12
          Width = 225
          Height = 26
          TabStop = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 7
        end
        object BTElectrodeCountEdit: TEdit
          Left = 128
          Top = 44
          Width = 65
          Height = 26
          TabStop = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
        end
        object BTEFlowMaxEdit: TEdit
          Left = 272
          Top = 140
          Width = 89
          Height = 26
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 2
        end
        object BTEFlowMinEdit: TEdit
          Left = 128
          Top = 140
          Width = 89
          Height = 26
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 1
        end
        object BTNGFlowMaxEdit: TEdit
          Left = 272
          Top = 172
          Width = 89
          Height = 26
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 4
        end
        object BTNGFlowMinEdit: TEdit
          Left = 128
          Top = 172
          Width = 89
          Height = 26
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 3
        end
        object BTPressureMaxEdit: TEdit
          Left = 272
          Top = 204
          Width = 89
          Height = 26
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 6
        end
        object BTPressureMinEdit: TEdit
          Left = 128
          Top = 204
          Width = 89
          Height = 26
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 5
        end
        object Panel1: TPanel
          Left = 440
          Top = 48
          Width = 345
          Height = 140
          BevelInner = bvLowered
          TabOrder = 10
          object Label35: TLabel
            Left = 16
            Top = 44
            Width = 38
            Height = 18
            Caption = 'TPM1'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -15
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object Label36: TLabel
            Left = 16
            Top = 74
            Width = 38
            Height = 18
            Caption = 'TPM2'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -15
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object Label37: TLabel
            Left = 16
            Top = 104
            Width = 74
            Height = 18
            Caption = 'TPM Expiry'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -15
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object Label49: TLabel
            Left = 216
            Top = 74
            Width = 33
            Height = 18
            Caption = 'Days'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -15
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object Label50: TLabel
            Left = 216
            Top = 104
            Width = 38
            Height = 18
            Caption = 'Hours'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -15
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object Panel6: TPanel
            Left = 2
            Top = 2
            Width = 341
            Height = 31
            Align = alTop
            BevelInner = bvLowered
            Caption = 'Panel6'
            TabOrder = 0
          end
          object BTETPM1DefaultEdit: TEdit
            Left = 104
            Top = 40
            Width = 89
            Height = 26
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -15
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 1
          end
          object BTETPM2DefaultEdit: TEdit
            Left = 104
            Top = 70
            Width = 89
            Height = 26
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -15
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 2
          end
          object BTETPMHourstEdit: TEdit
            Left = 104
            Top = 100
            Width = 89
            Height = 26
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -15
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 3
          end
        end
        object Button1: TButton
          Left = 528
          Top = 216
          Width = 120
          Height = 49
          Caption = 'Done'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 11
          OnClick = TTCancelButtonClick
        end
        object ByTypeEditSaveBtn: TButton
          Left = 664
          Top = 216
          Width = 120
          Height = 49
          Caption = 'Save'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 12
          OnClick = ByTypeEditSaveBtnClick
        end
        object RotationPanel: TPanel
          Left = 0
          Top = 240
          Width = 441
          Height = 97
          BevelOuter = bvNone
          TabOrder = 13
          object Label12: TLabel
            Left = 16
            Top = 3
            Width = 98
            Height = 18
            Caption = 'Rotation Speed'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -15
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object Label66: TLabel
            Left = 375
            Top = 3
            Width = 26
            Height = 18
            Caption = 'rpm'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -15
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object Label39: TLabel
            Left = 16
            Top = 35
            Width = 102
            Height = 18
            Caption = 'Rotation Dmnd.'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -15
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object Label40: TLabel
            Left = 208
            Top = 39
            Width = 86
            Height = 18
            Caption = '(0- 100 %)'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -15
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label13: TLabel
            Left = 16
            Top = 67
            Width = 91
            Height = 18
            Caption = 'Motor Current'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -15
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object Label67: TLabel
            Left = 379
            Top = 67
            Width = 30
            Height = 18
            Caption = 'Amp'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -15
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object BTRotMinEdit: TEdit
            Left = 128
            Top = -1
            Width = 89
            Height = 26
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -15
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 0
          end
          object BTRotMaxEdit: TEdit
            Left = 272
            Top = -1
            Width = 89
            Height = 26
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -15
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 1
          end
          object BTRotDemandEdit: TEdit
            Left = 128
            Top = 31
            Width = 65
            Height = 26
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -15
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 2
          end
          object BTMCurrentMinEdit: TEdit
            Left = 128
            Top = 63
            Width = 89
            Height = 26
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -15
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 3
          end
          object BTMCurrentMaxEdit: TEdit
            Left = 272
            Top = 63
            Width = 89
            Height = 26
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -15
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 4
          end
        end
        object BTChanelCountEdit: TEdit
          Left = 128
          Top = 76
          Width = 65
          Height = 26
          TabStop = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 14
        end
      end
      object ToolTypeAddPage: TTabSheet
        Caption = 'Add Tool Type'
        ImageIndex = 2
        object Label16: TLabel
          Left = 16
          Top = 120
          Width = 67
          Height = 18
          Caption = 'Tool Type'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object Label17: TLabel
          Left = 16
          Top = 155
          Width = 102
          Height = 18
          Caption = 'Electrode Count'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object Label18: TLabel
          Left = 16
          Top = 243
          Width = 92
          Height = 18
          Caption = 'Electrode Flow'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object Label19: TLabel
          Left = 16
          Top = 274
          Width = 100
          Height = 18
          Caption = 'Noseguide Flow'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object Label20: TLabel
          Left = 16
          Top = 306
          Width = 56
          Height = 18
          Caption = 'Pressure'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object RotLabel: TLabel
          Left = 16
          Top = 337
          Width = 98
          Height = 18
          Caption = 'Rotation Speed'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object MotLabel: TLabel
          Left = 16
          Top = 369
          Width = 91
          Height = 18
          Caption = 'Motor Current'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object Label23: TLabel
          Left = 208
          Top = 219
          Width = 58
          Height = 18
          Caption = 'Minimum'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object Label24: TLabel
          Left = 328
          Top = 219
          Width = 64
          Height = 18
          Caption = 'Maximum'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object Label32: TLabel
          Left = 16
          Top = 400
          Width = 120
          Height = 18
          Caption = 'TPM1 Reset Value'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object Label33: TLabel
          Left = 16
          Top = 432
          Width = 120
          Height = 18
          Caption = 'TPM2 Reset Value'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object Label34: TLabel
          Left = 16
          Top = 464
          Width = 74
          Height = 18
          Caption = 'TPM Expiry'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object Label48: TLabel
          Left = 300
          Top = 432
          Width = 33
          Height = 18
          Caption = 'Days'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object Label55: TLabel
          Left = 300
          Top = 464
          Width = 38
          Height = 18
          Caption = 'Hours'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object Label56: TLabel
          Left = 300
          Top = 400
          Width = 41
          Height = 18
          Caption = 'Cycles'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object Label68: TLabel
          Left = 424
          Top = 243
          Width = 44
          Height = 18
          Caption = 'ml/min'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object Label69: TLabel
          Left = 424
          Top = 243
          Width = 44
          Height = 18
          Caption = 'ml/min'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object Label70: TLabel
          Left = 424
          Top = 307
          Width = 21
          Height = 18
          Caption = 'bar'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object rpmLabel: TLabel
          Left = 424
          Top = 339
          Width = 26
          Height = 18
          Caption = 'rpm'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object ampLabel: TLabel
          Left = 424
          Top = 371
          Width = 30
          Height = 18
          Caption = 'Amp'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object Label22: TLabel
          Left = 16
          Top = 187
          Width = 86
          Height = 18
          Caption = 'Chanel Count'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object AddTT_TYpeEdit: TEdit
          Left = 128
          Top = 116
          Width = 225
          Height = 26
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 0
        end
        object AddTT_ECountEdit: TEdit
          Left = 128
          Top = 148
          Width = 65
          Height = 26
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 1
        end
        object AddTT_MinEF_Edit: TEdit
          Left = 200
          Top = 239
          Width = 89
          Height = 26
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 2
        end
        object AddTT_MinNGFlowEdit: TEdit
          Left = 200
          Top = 270
          Width = 89
          Height = 26
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 3
        end
        object AddTT_MinPressureEdit: TEdit
          Left = 200
          Top = 302
          Width = 89
          Height = 26
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 4
        end
        object AddTT_MinRotationEdit: TEdit
          Left = 200
          Top = 333
          Width = 89
          Height = 26
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 5
        end
        object AddTT_MinMCurrentEdit: TEdit
          Left = 200
          Top = 365
          Width = 89
          Height = 26
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 6
        end
        object AddTT_Max_EFlowEdit: TEdit
          Left = 320
          Top = 239
          Width = 89
          Height = 26
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 7
        end
        object AddTT_MaxNGFlowEdit: TEdit
          Left = 320
          Top = 270
          Width = 89
          Height = 26
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 8
        end
        object AddTT_MaxPressureEdit: TEdit
          Left = 320
          Top = 302
          Width = 89
          Height = 26
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 9
        end
        object AddTT_MaxRotationEdit: TEdit
          Left = 320
          Top = 333
          Width = 89
          Height = 26
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 10
        end
        object AddTT_MaxMCurrentEdit: TEdit
          Left = 320
          Top = 365
          Width = 89
          Height = 26
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 11
        end
        object Panel4: TPanel
          Left = 0
          Top = 0
          Width = 800
          Height = 105
          Align = alTop
          BevelInner = bvLowered
          Caption = 'Panel4'
          TabOrder = 12
          object ToolAddMemo: TMemo
            Left = 2
            Top = 2
            Width = 796
            Height = 101
            Align = alClient
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -16
            Font.Name = 'Tahoma'
            Font.Style = []
            Lines.Strings = (
              'ToolAddMemo')
            ParentFont = False
            TabOrder = 0
          end
        end
        object TTAddTooTypelButton: TButton
          Left = 504
          Top = 256
          Width = 249
          Height = 65
          Caption = 'Add Tool Type'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 13
          OnClick = TTAddTooTypelButtonClick
        end
        object IsRotaryCheckbox: TCheckBox
          Left = 384
          Top = 121
          Width = 153
          Height = 17
          Caption = 'Is Rotary Tool'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 14
          OnClick = IsRotaryCheckboxClick
        end
        object TTCancelButton: TButton
          Left = 504
          Top = 400
          Width = 120
          Height = 49
          Caption = 'Cancel'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 15
          OnClick = TTCancelButtonClick
        end
        object TTSetDefaultButton: TButton
          Left = 503
          Top = 200
          Width = 120
          Height = 49
          Caption = 'Set Default'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 16
          OnClick = TTSetDefaultButtonClick
        end
        object HPCheckBox: TCheckBox
          Left = 384
          Top = 160
          Width = 185
          Height = 25
          Caption = 'Is High Pressure Tool'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 17
        end
        object AddTT_TPM1Edit: TEdit
          Left = 200
          Top = 396
          Width = 89
          Height = 26
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 18
        end
        object AddTT_TPM2Edit: TEdit
          Left = 200
          Top = 428
          Width = 89
          Height = 26
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 19
        end
        object AddTT_ExpiryHoursEdit: TEdit
          Left = 200
          Top = 460
          Width = 89
          Height = 26
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 20
        end
        object RotSpeedPanel: TPanel
          Left = 536
          Top = 112
          Width = 257
          Height = 41
          BevelInner = bvLowered
          TabOrder = 21
          Visible = False
          object Label38: TLabel
            Left = 8
            Top = 11
            Width = 133
            Height = 18
            Caption = 'Rotation Test Speed'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -15
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object Label73: TLabel
            Left = 236
            Top = 11
            Width = 15
            Height = 18
            Caption = '%'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -15
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object AddTT_RotSpeedDemand: TEdit
            Left = 152
            Top = 7
            Width = 73
            Height = 26
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -15
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 0
          end
        end
        object AddTT_CCountEdit: TEdit
          Left = 128
          Top = 180
          Width = 65
          Height = 26
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 22
        end
      end
      object NBTypeAddPage: TTabSheet
        Caption = 'AddNonBalluffToolType'
        ImageIndex = 4
        object Label41: TLabel
          Left = 208
          Top = 120
          Width = 67
          Height = 18
          Caption = 'Tool Type'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object Label42: TLabel
          Left = 16
          Top = 155
          Width = 102
          Height = 18
          Caption = 'Electrode Count'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object Label43: TLabel
          Left = 16
          Top = 251
          Width = 92
          Height = 18
          Caption = 'Electrode Flow'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object Label44: TLabel
          Left = 16
          Top = 282
          Width = 100
          Height = 18
          Caption = 'Noseguide Flow'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object Label45: TLabel
          Left = 16
          Top = 314
          Width = 56
          Height = 18
          Caption = 'Pressure'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object NBRotLabel: TLabel
          Left = 16
          Top = 345
          Width = 98
          Height = 18
          Caption = 'Rotation Speed'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object NBMotorLabel: TLabel
          Left = 16
          Top = 377
          Width = 91
          Height = 18
          Caption = 'Motor Current'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object Label51: TLabel
          Left = 208
          Top = 227
          Width = 58
          Height = 18
          Caption = 'Minimum'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object Label52: TLabel
          Left = 328
          Top = 227
          Width = 64
          Height = 18
          Caption = 'Maximum'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object Label54: TLabel
          Left = 48
          Top = 120
          Width = 66
          Height = 18
          Caption = 'Tool Code'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object Label57: TLabel
          Left = 424
          Top = 251
          Width = 44
          Height = 18
          Caption = 'ml/min'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object Label58: TLabel
          Left = 424
          Top = 283
          Width = 44
          Height = 18
          Caption = 'ml/min'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object Label59: TLabel
          Left = 424
          Top = 315
          Width = 21
          Height = 18
          Caption = 'bar'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object nbrpmLabel: TLabel
          Left = 424
          Top = 347
          Width = 26
          Height = 18
          Caption = 'rpm'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object nbAmpLabel: TLabel
          Left = 424
          Top = 379
          Width = 30
          Height = 18
          Caption = 'Amp'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object Label46: TLabel
          Left = 16
          Top = 187
          Width = 86
          Height = 18
          Caption = 'Chanel Count'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object Panel7: TPanel
          Left = 0
          Top = 0
          Width = 800
          Height = 105
          Align = alTop
          BevelInner = bvLowered
          Caption = 'Panel4'
          TabOrder = 0
          object NonBallufToolAddMemo: TMemo
            Left = 2
            Top = 2
            Width = 796
            Height = 101
            Align = alClient
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -16
            Font.Name = 'Tahoma'
            Font.Style = []
            Lines.Strings = (
              'NonBallufToolAddMemo')
            ParentFont = False
            TabOrder = 0
          end
        end
        object AddNBTT_MinEF_Edit: TEdit
          Left = 200
          Top = 247
          Width = 89
          Height = 26
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 1
        end
        object AddNBTT_MaxEF_Edit: TEdit
          Left = 320
          Top = 247
          Width = 89
          Height = 26
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 2
        end
        object AddNBTT_MinNGFlowEdit: TEdit
          Left = 200
          Top = 278
          Width = 89
          Height = 26
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 3
        end
        object AddNBTT_MaxNGFlowEdit: TEdit
          Left = 320
          Top = 278
          Width = 89
          Height = 26
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 4
        end
        object AddNBTT_MinPressureEdit: TEdit
          Left = 200
          Top = 310
          Width = 89
          Height = 26
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 5
        end
        object AddNBTT_MaxPressureEdit: TEdit
          Left = 320
          Top = 310
          Width = 89
          Height = 26
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 6
        end
        object NonBalluffAddToolNameEdit: TEdit
          Left = 288
          Top = 116
          Width = 97
          Height = 26
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 7
        end
        object AddNBTT_ECountEdit: TEdit
          Left = 128
          Top = 148
          Width = 65
          Height = 26
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 8
        end
        object AddNBTT_MinRotationEdit: TEdit
          Left = 200
          Top = 341
          Width = 89
          Height = 26
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 9
        end
        object AddNBTT_MinMCurrentEdit: TEdit
          Left = 200
          Top = 373
          Width = 89
          Height = 26
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 11
        end
        object AddNBTT_MaxRotationEdit: TEdit
          Left = 320
          Top = 341
          Width = 89
          Height = 26
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 10
        end
        object AddNBTT_MaxMCurrentEdit: TEdit
          Left = 320
          Top = 373
          Width = 89
          Height = 26
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 12
        end
        object NBIsRotaryCheck: TCheckBox
          Left = 408
          Top = 121
          Width = 121
          Height = 17
          Caption = 'Is Rotary Tool'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 13
          OnClick = NBIsRotaryCheckClick
        end
        object NBRotarySpeedPanel: TPanel
          Left = 536
          Top = 112
          Width = 257
          Height = 41
          BevelInner = bvLowered
          TabOrder = 14
          Visible = False
          object Label53: TLabel
            Left = 8
            Top = 11
            Width = 133
            Height = 18
            Caption = 'Rotation Test Speed'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -15
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object Label62: TLabel
            Left = 228
            Top = 11
            Width = 15
            Height = 18
            Caption = '%'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -15
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object AddNBTT_RotSpeedDemand: TEdit
            Left = 152
            Top = 7
            Width = 65
            Height = 26
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -15
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 0
          end
        end
        object NBToolISHPCheck: TCheckBox
          Left = 408
          Top = 160
          Width = 185
          Height = 25
          Caption = 'Is High Pressure Tool'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 15
        end
        object NonBalluffAddToolCodeEdit: TEdit
          Left = 128
          Top = 116
          Width = 65
          Height = 26
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 16
        end
        object Button2: TButton
          Left = 527
          Top = 200
          Width = 120
          Height = 49
          Caption = 'Set Default'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 17
          OnClick = Button2Click
        end
        object Button3: TButton
          Left = 528
          Top = 256
          Width = 233
          Height = 65
          Caption = 'Add Tool Type'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 18
          OnClick = Button3Click
        end
        object Button4: TButton
          Left = 528
          Top = 328
          Width = 120
          Height = 49
          Caption = 'Cancel'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 19
          OnClick = Button4Click
        end
        object AddNBTT_ChanelCountEdit: TEdit
          Left = 128
          Top = 180
          Width = 65
          Height = 26
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 20
        end
      end
      object UTAddPage: TTabSheet
        Caption = 'Add Unique Tool'
        ImageIndex = 3
        object Label25: TLabel
          Left = 16
          Top = 168
          Width = 67
          Height = 18
          Caption = 'Tool Type'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object Label26: TLabel
          Left = 16
          Top = 203
          Width = 90
          Height = 18
          Caption = 'Serial Number'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object Label27: TLabel
          Left = 16
          Top = 235
          Width = 141
          Height = 18
          Caption = 'Noseguide Serial Num'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object Label28: TLabel
          Left = 16
          Top = 270
          Width = 88
          Height = 18
          Caption = 'Tool X Offset'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object Label29: TLabel
          Left = 16
          Top = 305
          Width = 89
          Height = 18
          Caption = 'Tool Y Offset'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object Label30: TLabel
          Left = 16
          Top = 342
          Width = 87
          Height = 18
          Caption = 'Tool Z Offset'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object Label31: TLabel
          Left = 16
          Top = 377
          Width = 88
          Height = 18
          Caption = 'Tool C Offset'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object Panel5: TPanel
          Left = 0
          Top = 0
          Width = 800
          Height = 121
          Align = alTop
          BevelInner = bvLowered
          Caption = 'Panel4'
          TabOrder = 0
          object UTMemo: TMemo
            Left = 2
            Top = 2
            Width = 796
            Height = 117
            Align = alClient
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -16
            Font.Name = 'Tahoma'
            Font.Style = []
            Lines.Strings = (
              'Unique ToolAddMemo')
            ParentFont = False
            TabOrder = 0
          end
        end
        object UTAddToolTypeEdit: TEdit
          Left = 192
          Top = 164
          Width = 225
          Height = 26
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 1
          OnChange = UtEditChange
          OnKeyUp = UTeditKeyUp
        end
        object UTAddSerialEdit: TEdit
          Left = 192
          Top = 199
          Width = 97
          Height = 26
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 2
        end
        object UTAddNGSerialEdit: TEdit
          Left = 192
          Top = 231
          Width = 97
          Height = 26
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 3
        end
        object UTAdd_XOffEdit: TEdit
          Left = 192
          Top = 266
          Width = 97
          Height = 26
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 4
        end
        object UTAdd_YOffEdit: TEdit
          Left = 192
          Top = 301
          Width = 97
          Height = 26
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 5
        end
        object UTAdd_ZOffEdit: TEdit
          Left = 192
          Top = 338
          Width = 97
          Height = 26
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 6
        end
        object UTAdd_COffEdit: TEdit
          Left = 192
          Top = 373
          Width = 97
          Height = 26
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 7
        end
        object UTSetDefaultButton: TButton
          Left = 575
          Top = 328
          Width = 120
          Height = 49
          Caption = 'Set Default'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 8
          OnClick = TTSetDefaultButtonClick
        end
        object UTCancelButton: TButton
          Left = 440
          Top = 400
          Width = 120
          Height = 49
          Caption = 'Cancel'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 9
          OnClick = TTCancelButtonClick
        end
        object UTAddButton: TButton
          Left = 576
          Top = 400
          Width = 120
          Height = 49
          Caption = 'Add Tool Type'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 10
          OnClick = TTAddTooTypelButtonClick
        end
      end
    end
  end
end
