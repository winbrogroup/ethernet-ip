object ToolManager: TToolManager
  Left = 67
  Top = 93
  Width = 885
  Height = 708
  Caption = '$TOOL_STATUS'
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Verdana'
  Font.Style = [fsBold]
  OldCreateOrder = False
  Position = poDesigned
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 16
  object KBBackPanel: TPanel
    Left = 0
    Top = 443
    Width = 877
    Height = 231
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    object BaseKB1: TBaseKB
      Left = 0
      Top = 0
      Width = 649
      Height = 231
      Align = alLeft
      BevelOuter = bvNone
      Caption = 'BaseKB1'
      TabOrder = 0
      KeyboardMode = kbmAlphaNumeric
      HighlightColor = clBlack
      Options = []
      SecondaryFont.Charset = DEFAULT_CHARSET
      SecondaryFont.Color = clWindowText
      SecondaryFont.Height = -11
      SecondaryFont.Name = 'MS Sans Serif'
      SecondaryFont.Style = []
      RepeatDelay = 500
      ExternalMode = False
      FocusByHandle = False
      FocusHandle = 0
      object DebugMemo: TMemo
        Left = 24
        Top = 16
        Width = 585
        Height = 129
        Lines.Strings = (
          'DebugMemo')
        TabOrder = 0
      end
    end
    object BaseFPad1: TBaseFPad
      Left = 640
      Top = 0
      Width = 240
      Height = 233
      ExternalMode = False
      ChangeCursorKeys = True
      EnableCursorKeys = False
    end
  end
  object Panel6: TPanel
    Left = 0
    Top = 0
    Width = 877
    Height = 443
    Align = alClient
    Caption = 'Panel6'
    TabOrder = 1
    object Panel3: TPanel
      Left = 1
      Top = 1
      Width = 875
      Height = 421
      Align = alClient
      BevelOuter = bvLowered
      TabOrder = 0
      object Panel7: TPanel
        Left = 446
        Top = 1
        Width = 428
        Height = 419
        Align = alClient
        Caption = 'Panel7'
        TabOrder = 0
        object TagPages: TPageControl
          Left = 1
          Top = 1
          Width = 426
          Height = 417
          ActivePage = NBUniquePage
          Align = alClient
          TabHeight = 16
          TabOrder = 0
          TabWidth = 90
          object OperatorPage: TTabSheet
            Caption = 'Controls'
            ImageIndex = 1
            object AbandonButton: TSpeedButton
              Left = 4
              Top = 336
              Width = 89
              Height = 44
              Caption = 'Cancel'
              OnClick = AbandonButtonClick
            end
            object Panel8: TPanel
              Left = 0
              Top = 0
              Width = 418
              Height = 155
              Align = alTop
              BevelInner = bvLowered
              Caption = 'Panel8'
              TabOrder = 0
              object Memo1: TMemo
                Left = 2
                Top = 2
                Width = 414
                Height = 151
                Align = alClient
                Font.Charset = ANSI_CHARSET
                Font.Color = clWindowText
                Font.Height = -16
                Font.Name = 'Verdana'
                Font.Style = [fsBold]
                Lines.Strings = (
                  'Memo1')
                ParentFont = False
                TabOrder = 0
              end
            end
            object Panel12: TPanel
              Left = 0
              Top = 155
              Width = 418
              Height = 75
              Align = alTop
              BevelInner = bvLowered
              TabOrder = 1
              object OffsetConfirmButton: TSpeedButton
                Left = 265
                Top = 28
                Width = 150
                Height = 44
                Caption = 'Confirm Offsets'
                Visible = False
                OnClick = OffsetConfirmButtonClick
              end
              object OffsetMatchCheckBox: TCheckBox
                Left = 8
                Top = 42
                Width = 217
                Height = 17
                Caption = 'Offsets Match Databse'
                Font.Charset = ANSI_CHARSET
                Font.Color = clWindowText
                Font.Height = -15
                Font.Name = 'Verdana'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 0
              end
              object Panel13: TPanel
                Left = 2
                Top = 2
                Width = 414
                Height = 26
                Align = alTop
                BevelInner = bvLowered
                Caption = 'Tool Offsets'
                Color = clInfoBk
                TabOrder = 1
              end
            end
            object Panel14: TPanel
              Left = 0
              Top = 230
              Width = 418
              Height = 100
              Align = alTop
              BevelInner = bvLowered
              TabOrder = 2
              object ConfirmLabel: TLabel
                Left = 8
                Top = 45
                Width = 106
                Height = 16
                Caption = ' Serial Number'
              end
              object NGModifyButton: TSpeedButton
                Left = 336
                Top = 31
                Width = 75
                Height = 66
                Caption = 'Change'
                OnClick = NGModifyButtonClick
              end
              object NoseGuideConfirmButton: TSpeedButton
                Left = 192
                Top = 31
                Width = 140
                Height = 66
                Caption = 'CONFIRM'
                Font.Charset = ANSI_CHARSET
                Font.Color = clWindowText
                Font.Height = -19
                Font.Name = 'Verdana'
                Font.Style = []
                ParentFont = False
                Visible = False
                OnClick = NoseGuideConfirmButtonClick
              end
              object NewNGInput: TEdit
                Left = 123
                Top = 41
                Width = 57
                Height = 24
                TabOrder = 0
                Text = '0'
              end
              object ConfirmTitle: TPanel
                Left = 2
                Top = 2
                Width = 414
                Height = 26
                Align = alTop
                BevelInner = bvLowered
                Caption = 'Noseguide Serial Number'
                Color = clInfoBk
                TabOrder = 1
              end
            end
          end
          object TagEventsPage: TTabSheet
            Caption = 'TagEvents'
            object Panel5: TPanel
              Left = 0
              Top = 0
              Width = 298
              Height = 391
              Align = alClient
              TabOrder = 0
              object LogMemo: TMemo
                Left = 1
                Top = 41
                Width = 296
                Height = 349
                Align = alClient
                Font.Charset = ANSI_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'Courier New'
                Font.Style = []
                ParentFont = False
                ScrollBars = ssVertical
                TabOrder = 0
              end
              object InfoPanel: TPanel
                Left = 1
                Top = 1
                Width = 296
                Height = 40
                Align = alTop
                BevelOuter = bvLowered
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlue
                Font.Height = -13
                Font.Name = 'Verdana'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 1
              end
            end
            object Panel4: TPanel
              Left = 298
              Top = 0
              Width = 120
              Height = 391
              Align = alRight
              BevelOuter = bvLowered
              TabOrder = 1
              object btnToolChanger: TSpeedButton
                Left = 72
                Top = 352
                Width = 20
                Height = 20
                GroupIndex = 1
                Caption = '$TOOL_CHANGER'
                Visible = False
                OnClick = btnToolChangerClick
              end
              object btnHead: TSpeedButton
                Left = 16
                Top = 352
                Width = 20
                Height = 20
                GroupIndex = 1
                Caption = '$HEAD'
                Visible = False
                OnClick = btnToolChangerClick
              end
              object SpeedButton3: TSpeedButton
                Left = 13
                Top = 320
                Width = 20
                Height = 20
                Caption = '$RESET'
                Visible = False
                OnMouseDown = SpeedButton3MouseDown
                OnMouseUp = SpeedButton3MouseUp
              end
              object SpeedButton1: TSpeedButton
                Left = 95
                Top = 356
                Width = 20
                Height = 20
                GroupIndex = 1
                Caption = 'Head'
                Visible = False
                OnClick = btnToolChangerClick
              end
              object BitBtn1: TBitBtn
                Left = 5
                Top = 264
                Width = 100
                Height = 49
                Caption = '$DONE'
                TabOrder = 0
                Kind = bkOK
              end
              object BtnWrite: TBitBtn
                Left = 5
                Top = 11
                Width = 100
                Height = 60
                Caption = '$WRITE'
                TabOrder = 1
                OnMouseDown = BtnWriteMouseDown
                OnMouseUp = BtnWriteMouseUp
                Glyph.Data = {
                  DE010000424DDE01000000000000760000002800000024000000120000000100
                  0400000000006801000000000000000000001000000000000000000000000000
                  80000080000000808000800000008000800080800000C0C0C000808080000000
                  FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333444444
                  33333333333F8888883F33330000324334222222443333388F3833333388F333
                  000032244222222222433338F8833FFFFF338F3300003222222AAAAA22243338
                  F333F88888F338F30000322222A33333A2224338F33F8333338F338F00003222
                  223333333A224338F33833333338F38F00003222222333333A444338FFFF8F33
                  3338888300003AAAAAAA33333333333888888833333333330000333333333333
                  333333333333333333FFFFFF000033333333333344444433FFFF333333888888
                  00003A444333333A22222438888F333338F3333800003A2243333333A2222438
                  F38F333333833338000033A224333334422224338338FFFFF8833338000033A2
                  22444442222224338F3388888333FF380000333A2222222222AA243338FF3333
                  33FF88F800003333AA222222AA33A3333388FFFFFF8833830000333333AAAAAA
                  3333333333338888883333330000333333333333333333333333333333333333
                  0000}
                NumGlyphs = 2
              end
              object btnHeadRead: TBitBtn
                Left = 5
                Top = 203
                Width = 100
                Height = 60
                Caption = '$READ'
                TabOrder = 2
                OnMouseDown = btnHeadReadMouseDown
                OnMouseUp = btnHeadReadMouseUp
                Glyph.Data = {
                  DE010000424DDE01000000000000760000002800000024000000120000000100
                  0400000000006801000000000000000000001000000000000000000000000000
                  80000080000000808000800000008000800080800000C0C0C000808080000000
                  FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333444444
                  33333333333F8888883F33330000324334222222443333388F3833333388F333
                  000032244222222222433338F8833FFFFF338F3300003222222AAAAA22243338
                  F333F88888F338F30000322222A33333A2224338F33F8333338F338F00003222
                  223333333A224338F33833333338F38F00003222222333333A444338FFFF8F33
                  3338888300003AAAAAAA33333333333888888833333333330000333333333333
                  333333333333333333FFFFFF000033333333333344444433FFFF333333888888
                  00003A444333333A22222438888F333338F3333800003A2243333333A2222438
                  F38F333333833338000033A224333334422224338338FFFFF8833338000033A2
                  22444442222224338F3388888333FF380000333A2222222222AA243338FF3333
                  33FF88F800003333AA222222AA33A3333388FFFFFF8833830000333333AAAAAA
                  3333333333338888883333330000333333333333333333333333333333333333
                  0000}
                NumGlyphs = 2
              end
            end
          end
          object AddToolTypePage: TTabSheet
            Caption = 'Add Tool Type'
            ImageIndex = 2
            object AddToolTypeButton: TSpeedButton
              Left = 232
              Top = 280
              Width = 169
              Height = 44
              Caption = 'Add Tool Type'
              OnClick = AddToolTypeButtonClick
            end
            object SpeedButton2: TSpeedButton
              Left = 24
              Top = 280
              Width = 89
              Height = 44
              Caption = 'Cancel'
              OnClick = AbandonButtonClick
            end
            object Panel11: TPanel
              Left = 0
              Top = 0
              Width = 418
              Height = 121
              Align = alTop
              BevelInner = bvLowered
              Caption = 'Panel8'
              TabOrder = 0
              object AddToolMemo: TMemo
                Left = 2
                Top = 2
                Width = 414
                Height = 117
                Align = alClient
                Font.Charset = ANSI_CHARSET
                Font.Color = clWindowText
                Font.Height = -16
                Font.Name = 'Verdana'
                Font.Style = [fsBold]
                Lines.Strings = (
                  ''
                  'Add Tool Memo....................'
                  '')
                ParentFont = False
                TabOrder = 0
              end
            end
          end
          object NewToolPage: TTabSheet
            Caption = 'New Tool'
            ImageIndex = 3
            object NewToolCancelBtn: TSpeedButton
              Left = 24
              Top = 280
              Width = 89
              Height = 44
              Caption = 'Cancel'
              OnClick = AbandonButtonClick
            end
            object NewToolAddButton: TSpeedButton
              Left = 232
              Top = 280
              Width = 169
              Height = 44
              Caption = 'Add Tool'
              OnClick = NewToolAddButtonClick
            end
            object NewToolRequestLabel: TLabel
              Left = 24
              Top = 240
              Width = 182
              Height = 16
              Caption = 'Noseguide Serial Number'
            end
            object Panel2: TPanel
              Left = 0
              Top = 0
              Width = 418
              Height = 217
              Align = alTop
              BevelInner = bvLowered
              Caption = 'Panel8'
              TabOrder = 0
              object NewToolMemo: TMemo
                Left = 2
                Top = 2
                Width = 414
                Height = 213
                Align = alClient
                Font.Charset = ANSI_CHARSET
                Font.Color = clWindowText
                Font.Height = -16
                Font.Name = 'Verdana'
                Font.Style = [fsBold]
                Lines.Strings = (
                  ''
                  'New Tool Memo....................'
                  '')
                ParentFont = False
                TabOrder = 0
              end
            end
            object NGSerialEnter: TEdit
              Left = 256
              Top = 232
              Width = 49
              Height = 24
              TabOrder = 1
              Text = '0'
            end
          end
          object NBTypePage: TTabSheet
            Caption = 'NBType'
            ImageIndex = 4
            object SpeedButton4: TSpeedButton
              Left = 16
              Top = 248
              Width = 89
              Height = 44
              Caption = 'Cancel'
              OnClick = AbandonButtonClick
            end
            object NBToolTypeAddButton: TSpeedButton
              Left = 144
              Top = 248
              Width = 169
              Height = 44
              Caption = 'Add Tool'
              OnClick = NBToolTypeAddButtonClick
            end
            object Label6: TLabel
              Left = 16
              Top = 212
              Width = 115
              Height = 16
              Caption = 'Tool Type Name'
            end
            object Label7: TLabel
              Left = 16
              Top = 172
              Width = 108
              Height = 16
              Caption = 'Hypertac Code'
            end
            object NBTopPanel: TPanel
              Left = 0
              Top = 0
              Width = 418
              Height = 153
              Align = alTop
              BevelInner = bvLowered
              Caption = 'Panel8'
              TabOrder = 0
              object NonBalluffToolMemo: TMemo
                Left = 2
                Top = 2
                Width = 414
                Height = 149
                Align = alClient
                Font.Charset = ANSI_CHARSET
                Font.Color = clWindowText
                Font.Height = -16
                Font.Name = 'Verdana'
                Font.Style = [fsBold]
                Lines.Strings = (
                  'New Balluff Too lMemo ...........')
                ParentFont = False
                TabOrder = 0
              end
            end
            object NBToolNameEdit: TEdit
              Left = 144
              Top = 208
              Width = 153
              Height = 24
              TabOrder = 1
            end
            object NBHypertacDisplay: TEdit
              Left = 144
              Top = 168
              Width = 89
              Height = 24
              ReadOnly = True
              TabOrder = 2
            end
          end
          object NBUniquePage: TTabSheet
            Caption = 'NBUnique'
            ImageIndex = 5
            object NBNewToolRequestLabel: TLabel
              Left = 24
              Top = 288
              Width = 182
              Height = 16
              Caption = 'Noseguide Serial Number'
            end
            object SpeedButton5: TSpeedButton
              Left = 24
              Top = 328
              Width = 89
              Height = 44
              Caption = 'Cancel'
              OnClick = AbandonButtonClick
            end
            object NBAddToolButton: TSpeedButton
              Left = 232
              Top = 328
              Width = 169
              Height = 44
              Caption = 'Add Tool'
              OnClick = NBAddToolButtonClick
            end
            object Label9: TLabel
              Left = 24
              Top = 188
              Width = 108
              Height = 16
              Caption = 'Hypertac Code'
            end
            object Label10: TLabel
              Left = 24
              Top = 220
              Width = 173
              Height = 16
              Caption = 'Hypertac Serial Number'
            end
            object Label11: TLabel
              Left = 24
              Top = 252
              Width = 69
              Height = 16
              Caption = 'Tool Type'
            end
            object Panel17: TPanel
              Left = 0
              Top = 0
              Width = 418
              Height = 169
              Align = alTop
              BevelInner = bvLowered
              Caption = 'Panel8'
              TabOrder = 0
              object NBUniqueMemo: TMemo
                Left = 2
                Top = 2
                Width = 414
                Height = 165
                Align = alClient
                Font.Charset = ANSI_CHARSET
                Font.Color = clWindowText
                Font.Height = -16
                Font.Name = 'Verdana'
                Font.Style = [fsBold]
                Lines.Strings = (
                  ''
                  'New Tool Memo....................'
                  '')
                ParentFont = False
                TabOrder = 0
              end
            end
            object NBNGSerialEdit: TEdit
              Left = 240
              Top = 280
              Width = 57
              Height = 24
              TabOrder = 1
              Text = '0'
            end
            object NBAddHTC: TEdit
              Left = 240
              Top = 184
              Width = 121
              Height = 24
              ReadOnly = True
              TabOrder = 2
              Text = 'NBAddHTC'
            end
            object NBAddHTSN: TEdit
              Left = 240
              Top = 216
              Width = 121
              Height = 24
              ReadOnly = True
              TabOrder = 3
              Text = 'NBConfirmHTSN'
            end
            object NBAddToolType: TEdit
              Left = 240
              Top = 248
              Width = 121
              Height = 24
              ReadOnly = True
              TabOrder = 4
              Text = 'NBConfirmToolType'
            end
          end
          object NBConfirmPage: TTabSheet
            Caption = 'NBConfirm'
            ImageIndex = 6
            object NBConfirmButton: TSpeedButton
              Left = 224
              Top = 328
              Width = 169
              Height = 44
              Caption = 'Confirm'
              OnClick = NBConfirmButtonClick
            end
            object SpeedButton7: TSpeedButton
              Left = 16
              Top = 328
              Width = 89
              Height = 44
              Caption = 'Cancel'
              OnClick = AbandonButtonClick
            end
            object NBConfirmLabel: TLabel
              Left = 16
              Top = 260
              Width = 183
              Height = 16
              Caption = 'NoseGuide Serial Number'
            end
            object Label1: TLabel
              Left = 16
              Top = 164
              Width = 108
              Height = 16
              Caption = 'Hypertac Code'
            end
            object Label2: TLabel
              Left = 16
              Top = 228
              Width = 69
              Height = 16
              Caption = 'Tool Type'
            end
            object Label8: TLabel
              Left = 16
              Top = 196
              Width = 173
              Height = 16
              Caption = 'Hypertac Serial Number'
            end
            object Panel15: TPanel
              Left = 0
              Top = 0
              Width = 418
              Height = 137
              Align = alTop
              BevelInner = bvLowered
              Caption = 'Panel8'
              TabOrder = 0
              object NBConfirmMemo: TMemo
                Left = 2
                Top = 2
                Width = 414
                Height = 133
                Align = alClient
                Font.Charset = ANSI_CHARSET
                Font.Color = clWindowText
                Font.Height = -16
                Font.Name = 'Verdana'
                Font.Style = [fsBold]
                Lines.Strings = (
                  'NBConfirmMemo'
                  ''
                  '')
                ParentFont = False
                TabOrder = 0
              end
            end
            object NBConfirmDisplay: TEdit
              Left = 232
              Top = 256
              Width = 90
              Height = 24
              ReadOnly = True
              TabOrder = 1
              Text = 'NBConfirmDisplay'
            end
            object NBConfirmHTC: TEdit
              Left = 232
              Top = 160
              Width = 121
              Height = 24
              ReadOnly = True
              TabOrder = 2
              Text = 'NBConfirmHTC'
            end
            object NBConfirmToolType: TEdit
              Left = 232
              Top = 224
              Width = 121
              Height = 24
              ReadOnly = True
              TabOrder = 3
              Text = 'NBConfirmToolType'
            end
            object NBConfirmHTSN: TEdit
              Left = 232
              Top = 192
              Width = 121
              Height = 24
              ReadOnly = True
              TabOrder = 4
              Text = 'NBConfirmHTSN'
            end
          end
        end
      end
      object Panel9: TPanel
        Left = 1
        Top = 1
        Width = 445
        Height = 419
        Align = alLeft
        Caption = 'Panel9'
        TabOrder = 1
        object ToolEditor: TValueListEditor
          Left = 1
          Top = 25
          Width = 443
          Height = 328
          Align = alTop
          Strings.Strings = (
            '$TOOLTYPE='
            '$TOOL_SERIAL='
            '$TPM_EXPIRY='
            '$HIGH_PRESSURE_TOOL='
            '$BARREL_COUNT='
            '$BARRELS_REMAINING='
            '$MAX_TOOL_LENGTH='
            '$MINIMUM_TOOL_LENGTH='
            '$ACTUAL_LENGTH='
            '$X_TOOL_OFFSET='
            '$Y_TOOL_OFFSET='
            '$Z_TOOL_OFFSET='
            '$C_TOOL_OFFSET='
            '$TPM1='
            '$TPM2='
            '$OFFSET_VERIFY='
            'DUMMY=')
          TabOrder = 0
          TitleCaptions.Strings = (
            'Data'
            'Value')
          ColWidths = (
            189
            231)
        end
        object Panel10: TPanel
          Left = 1
          Top = 1
          Width = 443
          Height = 24
          Align = alTop
          BevelInner = bvLowered
          Caption = 'Tag Data'
          TabOrder = 1
        end
        object TPMPanel: TPanel
          Left = 1
          Top = 353
          Width = 443
          Height = 65
          Align = alClient
          BevelInner = bvLowered
          TabOrder = 2
          object Panel16: TPanel
            Left = 2
            Top = 2
            Width = 439
            Height = 20
            Align = alTop
            BevelInner = bvLowered
            Caption = 'On successful tool test, tpm will be set as follows:'
            TabOrder = 0
          end
          object StandardTPMPanel: TPanel
            Left = 98
            Top = 22
            Width = 439
            Height = 41
            TabOrder = 1
            object Label3: TLabel
              Left = 8
              Top = 8
              Width = 43
              Height = 16
              Caption = 'TPM1:'
            end
            object Label5: TLabel
              Left = 160
              Top = 8
              Width = 113
              Height = 16
              Caption = 'TPMExpiry  Plus'
            end
            object Label4: TLabel
              Left = 352
              Top = 8
              Width = 43
              Height = 16
              Caption = 'Hours'
            end
            object TPM1Display: TEdit
              Left = 56
              Top = 4
              Width = 57
              Height = 24
              ReadOnly = True
              TabOrder = 0
            end
            object TPMHoursDisplay: TEdit
              Left = 280
              Top = 4
              Width = 57
              Height = 24
              ReadOnly = True
              TabOrder = 1
            end
          end
          object TPM2Panel: TPanel
            Left = 12
            Top = 22
            Width = 407
            Height = 41
            TabOrder = 2
            object Label12: TLabel
              Left = 280
              Top = 8
              Width = 36
              Height = 16
              Caption = 'Days'
            end
            object TPM2ValueEdit: TEdit
              Left = 200
              Top = 4
              Width = 57
              Height = 24
              TabOrder = 0
              Text = '0'
            end
            object TPM2Btn: TBitBtn
              Left = 37
              Top = 4
              Width = 100
              Height = 25
              Caption = 'TPM2 Reset'
              TabOrder = 1
              OnMouseDown = TPM2BtnMouseDown
              OnMouseUp = TPM2BtnMouseUp
              NumGlyphs = 2
            end
          end
        end
      end
    end
    object Panel1: TPanel
      Left = 1
      Top = 422
      Width = 875
      Height = 20
      Align = alBottom
      BevelOuter = bvLowered
      TabOrder = 1
    end
  end
end
