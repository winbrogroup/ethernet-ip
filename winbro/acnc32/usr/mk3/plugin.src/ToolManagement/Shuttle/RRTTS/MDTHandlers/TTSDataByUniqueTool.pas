unit TTSDataByUniqueTool;

interface
uses SysUtils, Classes, IStringArray, NamedPlugin, OCTTypes,ToolData;


const
  FloatFormat = '%.1';
  ToolTypeField = 'Tooltype';
  SerialNumberField = 'SerialNumber';
  UniqueToolXField = 'UniqueToolX';
  UniqueToolYField = 'UniqueToolY';
  UniqueToolZField = 'UniqueToolZ';
  UniqueToolCField = 'UniqueToolC';
  NoseGuideSerialNumField = 'NGSerial';
  OffsetChangeDateField = 'OffsetChangeDateTime';
  FirstRegisteredDateField = 'RegisterDateTime';
  IsNonBallufField = 'NonBalluff';
  ElectrodeDiameterField = 'Dia';
  IsRotaryField = 'Rotary';

  Type
  TUniqueToolDataList = class;

  TTTSByUniqueToolData = class(TObject)
  private
    Index : Integer;
    Host : TUniqueToolDataList;
    FUniqueToolOffsets : array [0..3] of Double;
    FOffsetChangeTime: TDateTime;
    FToolType: WideString;
    FSerialNumber: Integer;
    FRegistrationTime: TDateTime;
    FNGSerialNumber: Integer;
    FIsNonBalluff: Integer;
    FIsRotary: Integer;
    FElectrodeDiameter: Double;
    function GetUniqueToolOffsets(Index: Integer): Double;
    procedure SetNGSerialNumber(const Value: Integer);
    procedure SetOffsetChangeTime(const Value: TDateTime);
    procedure SetRegistrationTime(const Value: TDateTime);
    procedure SetSerialNumber(const Value: Integer);
    procedure SetIsNonBalluff(const Value: Integer);
    procedure SetToolTYpe(const Value: WideString);
    procedure SetUniqueToolOffsets(Index: Integer; const Value: Double);
    procedure SetElectrodeDiameter(const Value: Double);
    procedure SetIsRotary(const Value: Integer);
  protected
    procedure RebuildInternalValues(AIndex : Integer);
    procedure Copy(Src: TTTSByUniqueToolData);

  public
    constructor Create(AHost : TUniqueToolDataList; AIndex : Integer);
    destructor Destroy; override;
    property ToolType : WideString read FToolType write SetToolTYpe;
    property SerialNumber : Integer read FSerialNumber write SetSerialNumber;
    property UniqueToolOffsets[Index : Integer] : Double read GetUniqueToolOffsets write SetUniqueToolOffsets;
    property NGSerialNumber : Integer read FNGSerialNumber write SetNGSerialNumber;
    property OffsetChangeTime : TDateTime read FOffsetChangeTime write SetOffsetChangeTime;
    property RegistrationTime : TDateTime read FRegistrationTime write SetRegistrationTime;
    property IsNonBalluff : Integer read FIsNonBalluff write SetIsNonBalluff;
    property IsRotary     : Integer read FIsRotary write SetIsRotary;
    property ElectrodeDiameter : Double read FElectrodeDiameter write SetElectrodeDiameter;
  public
  end;

  TUniqueToolDataList = class(TAbstractStringData)
  private

    ToolList : TList;
    procedure FlushData(Table: IACNC32StringArray);
    function GetTool(index: Integer): TTTSByUniqueToolData;
    procedure CleanTable;
    procedure CreateWithName(TableName: string; Clean: Boolean);

  protected

    procedure RebuildSingleValue(Row, Col :Integer); override;
    procedure RebuildInternalValues; override;
    procedure CreateTable;
    procedure RebuildIndex;

  public
    constructor Create; overload;
    constructor Create(TableName: string); overload;
    destructor Destroy; override;
    function AddTool : TTTSByUniqueToolData;
    function AddToolFromTagData(ToolData : TToolData;
                                NGSerial : Integer;
                                ElectrodeDiameter : Double;
                                IsRotary : Boolean):TTTSByUniqueToolData;
    function Count : Integer;
    property Tool[index : Integer] : TTTSByUniqueToolData read GetTool; default;
    procedure Clean; override;
    function IndexOf(ToolType : String;SerialNumber : Integer) : Integer;
    procedure RemoveTool(T : TTTSByUniqueToolData);
    procedure CopyTool(Target, Src: TTTSByUniqueToolData);
    function Find(Id: string; Sn: Integer): TTTSByUniqueToolData;
    function UpdateOffsetsFor(Tool : TToolData): Boolean;
    function UpdateNGSerialForTagData(TData: TToolData;NGSerial : Integer): Boolean;
    procedure FlushDataToDisk;
  end;

implementation

{ TTTSByUniqueToolData }

procedure TTTSByUniqueToolData.SetOffsetChangeTime(const Value: TDateTime);
begin
  FOffsetChangeTime := Value;
  Host.SetFieldData(Index,OffsetChangeDateField,FloatToStr(Value));
end;

procedure TTTSByUniqueToolData.SetToolTYpe(const Value: WideString);
begin
  FToolType := Value;
  Host.SetFieldData(Index,ToolTypeField,Value);
end;

procedure TTTSByUniqueToolData.SetSerialNumber(const Value: Integer);
begin
  FSerialNumber := Value;
  Host.SetFieldData(Index,SerialNumberField,IntToStr(Value));
end;

procedure TTTSByUniqueToolData.SetIsNonBalluff(const Value: Integer);
begin
  FIsNonBalluff := Value;
  Host.SetFieldData(Index,IsNonBallufField,IntToStr(Value));
end;

procedure TTTSByUniqueToolData.SetElectrodeDiameter(const Value: Double);
begin
  FElectrodeDiameter := Value;
  Host.SetFieldData(Index,ElectrodeDiameterField,FloatToStr(Value));
end;

procedure TTTSByUniqueToolData.SetIsRotary(const Value: Integer);
begin
  FIsRotary := Value;
  Host.SetFieldData(Index,IsRotaryField,IntToStr(Value));
end;


procedure TTTSByUniqueToolData.SetRegistrationTime(const Value: TDateTime);
begin
  FRegistrationTime := Value;
  Host.SetFieldData(Index,FirstRegisteredDateField, FloatToStr(Value));
end;

function TTTSByUniqueToolData.GetUniqueToolOffsets(Index: Integer): Double;
begin
  Result := FUniqueToolOffsets[Index mod 4];
end;

procedure TTTSByUniqueToolData.SetUniqueToolOffsets(Index: Integer; const Value: Double);
begin
  FUniqueToolOffsets[Index mod 4] := Value;
  Host.SetFieldData(Self.Index, UniqueToolXField, FloatToStr(FUniqueToolOffsets[0]));
  Host.SetFieldData(Self.Index, UniqueToolYField, FloatToStr(FUniqueToolOffsets[1]));
  Host.SetFieldData(Self.Index, UniqueToolZField, FloatToStr(FUniqueToolOffsets[2]));
  Host.SetFieldData(Self.Index, UniqueToolCField, FloatToStr(FUniqueToolOffsets[3]));
end;

procedure TTTSByUniqueToolData.SetNGSerialNumber(const Value: Integer);
begin
  FNGSerialNumber := Value;
  Host.SetFieldData(Index,NoseGuideSerialNumField,IntToStr(Value));
end;

procedure TTTSByUniqueToolData.RebuildInternalValues(AIndex: Integer);
begin
  Index := AIndex;
  FToolType  := Host.GetFieldString(Index, ToolTypeField);
  FSerialNumber := Host.GetFieldInteger(Index, SerialNumberField);
  FNGSerialNumber := Host.GetFieldInteger(Index, NoseGuideSerialNumField);
  FUniqueToolOffsets[0] := Host.GetFieldDouble(Index,UniqueToolXField);
  FUniqueToolOffsets[1] := Host.GetFieldDouble(Index,UniqueToolYField);
  FUniqueToolOffsets[2] := Host.GetFieldDouble(Index,UniqueToolZField);
  FUniqueToolOffsets[3] := Host.GetFieldDouble(Index,UniqueToolCField);
  FRegistrationTime := Host.GetFieldDouble(Index,FirstRegisteredDateField);
  FOffsetChangeTime := Host.GetFieldDouble(Index,OffsetChangeDateField);
  FIsNonBalluff   := Host.GetFieldInteger(Index,IsNonBallufField);
  FIsRotary   := Host.GetFieldInteger(Index,IsRotaryField);
  FElectrodeDiameter   := Host.GetFieldDouble(Index,ElectrodeDiameterField);
end;

procedure TTTSByUniqueToolData.Copy(Src: TTTSByUniqueToolData);
begin

end;

constructor TTTSByUniqueToolData.Create(AHost: TUniqueToolDataList;  AIndex: Integer);
begin
  inherited Create;
  Index := AIndex;
  Host := AHost;
end;

destructor TTTSByUniqueToolData.Destroy;
begin

  inherited;
end;




{ TUniqueToolDataList }

function TUniqueToolDataList.IndexOf(ToolType : String;SerialNumber : Integer): Integer;
var
ToolEntry : TTTSByUniqueToolData;
Found : Boolean;
begin
  Result := -1;
  Found := False;
  while (Result < ToolList.count-1) and (not Found) do
    begin
    Inc(Result);
    ToolEntry := ToolList[Result];
    if UpperCase(ToolEntry.ToolType) = Uppercase(ToolType) then
      begin
      Found := Toolentry.SerialNumber = SerialNumber;
      end
    end;
  if not Found then  Result := -1;
end;



procedure TUniqueToolDataList.CopyTool(Target, Src: TTTSByUniqueToolData);
begin
//Not Implemented
end;

function TUniqueToolDataList.GetTool(index: Integer): TTTSByUniqueToolData;
begin
  Result := TTTSByUniqueToolData(ToolList[Index]);
end;

procedure TUniqueToolDataList.RemoveTool(T: TTTSByUniqueToolData);
begin

end;

constructor TUniqueToolDataList.Create(TableName: string);
begin
  inherited Create;
  CreateWithName(TableName, True);
end;

constructor TUniqueToolDataList.Create;
begin
  inherited Create;
  CreateWithName('IndividualToolData', True);
end;

procedure TUniqueToolDataList.CreateTable;
begin
  BeginUpdate;
  try
  ITable.Clear;
  ITable.AddField(ToolTypeField);
  ITable.AddField(SerialNumberField);
  ITable.AddField(UniqueToolXField);
  ITable.AddField(UniqueToolYField);
  ITable.AddField(UniqueToolZField);
  ITable.AddField(UniqueToolCField);
  ITable.AddField(NoseGuideSerialNumField);
  ITable.AddField(OffsetChangeDateField);
  ITable.AddField(FirstRegisteredDateField);
  ITable.AddField(IsNonBallufField);
  ITable.AddField(ElectrodeDiameterField);
  ITable.AddField(IsRotaryField);
  finally
    EndUpdate;
  end;
end;

procedure TUniqueToolDataList.RebuildInternalValues;
var I : Integer;
    Tmp : TTTSByUniqueToolData;
begin
  for I := ITable.RecordCount to ToolList.Count - 1 do begin
    Tool[0].Free;
    ToolList.Delete(0);
  end;
  for I := 0 to ITable.RecordCount - 1 do begin
    if I >= ToolList.Count then begin
      Tmp := TTTSByUniqueToolData.Create(Self, I);
      ToolList.Add(Tmp);
      Tmp.RebuildInternalValues(I);
    end else begin
      Tool[I].RebuildInternalValues(I);
    end;
  end;
end;

procedure TUniqueToolDataList.Clean;
begin
  inherited;

end;

procedure TUniqueToolDataList.CleanTable;
var F: WideString;
begin
  BeginUpdate;
  try
    while ITable.FieldCount > 0 do begin
      ITable.FieldNameAtIndex(0, F);
      ITable.DeleteField(F);
    end;
  finally
    EndUpdate;
  end;
end;

function TUniqueToolDataList.Count: Integer;
begin
  Result := ToolList.Count;
end;

function TUniqueToolDataList.AddTool: TTTSByUniqueToolData;
begin
 Lock;
  try
    Result := TTTSByUniqueToolData.Create(Self, -1);
    Result.Index := ITable.RecordCount;
    ToolList.Add(Result);
    ITable.Append(1);
  finally
    Unlock;
  end;
end;

procedure TUniqueToolDataList.RebuildIndex;
var I : Integer;
begin
  for I := 0 to Count - 1 do begin
    Tool[I].Index := I;
  end;
end;

procedure TUniqueToolDataList.RebuildSingleValue(Row, Col: Integer);
begin
  inherited;

end;

function TUniqueToolDataList.Find(Id: string; Sn: Integer): TTTSByUniqueToolData;
var
I : Integer;
begin
Result := nil;
I := IndexOf(Id,Sn);
if I >=0  then
  begin
  Result := ToolList[i];
  end;
end;

destructor TUniqueToolDataList.Destroy;
begin

  inherited;
end;

procedure TUniqueToolDataList.CreateWithName(TableName: string; Clean: Boolean);
begin
  if StringArraySet.CreateArray(TableName, Self, ITable) <> AAR_OK then begin
    StringArraySet.UseArray(TableName, Self, ITable);
    if ITable.FieldCount <> 12 then begin
      CleanTable;
      CreateTable;
    end;
  end else begin
    CreateTable;
  end;
  ToolList := TList.Create;
  RebuildInternalValues;
end;

function TUniqueToolDataList.AddToolFromTagData(ToolData: TToolData;
                                                NGSerial: Integer;
                                                ElectrodeDiameter : Double;
                                                IsRotary : Boolean): TTTSByUniqueToolData;
var
Tmp : TTTSByUniqueToolData;
begin
 Tmp := Find(ToolData.ShuttleID,ToolData.ShuttleSN);
 if not assigned(tmp) then
     begin
     Result :=  AddTool;
     Result.ToolType := ToolData.ShuttleID;
     Result.SerialNumber := ToolData.ShuttleSN;
     Result.UniqueToolOffsets[0] := ToolData.UniqueToolOffsets[0];
     Result.UniqueToolOffsets[1] := ToolData.UniqueToolOffsets[1];
     Result.UniqueToolOffsets[2] := ToolData.UniqueToolOffsets[2];
     Result.UniqueToolOffsets[3] := ToolData.UniqueToolOffsets[3];
     Result.NGSerialNumber := NGSerial;
     Result.OffsetChangeTime := Now;
     Result.RegistrationTime := Now;
     Result.IsNonBalluff := 0;

     if IsRotary then
      Result.IsRotary := 1
     else
      Result.IsRotary := 0;

     Result.ElectrodeDiameter := ElectrodeDiameter;
     RebuildInternalValues;
     FlushData(ITable);
    end
 else
   Result := nil;
end;
function TUniqueToolDataList.UpdateOffsetsFor(Tool: TToolData): Boolean;
var
tmp : TTTSByUniqueToolData;
begin
Result := False;
tmp := Find(Tool.ShuttleID,Tool.ShuttleSN);
if assigned(tmp) then
  begin
  Result := True;
  tmp.UniqueToolOffsets[0] := Tool.UniqueToolOffsets[0];
  tmp.UniqueToolOffsets[1] := Tool.UniqueToolOffsets[1];
  tmp.UniqueToolOffsets[2] := Tool.UniqueToolOffsets[2];
  tmp.UniqueToolOffsets[3] := Tool.UniqueToolOffsets[3];
  RebuildInternalValues;
  FlushData(ITable);
  end;
end;

function TUniqueToolDataList.UpdateNGSerialForTagData(TData: TToolData;
  NGSerial: Integer): Boolean;
var
tmp : TTTSByUniqueToolData;
begin
Result := False;
tmp := Find(TData.ShuttleID,TData.ShuttleSN);
if assigned(tmp) then
  begin
  Result := True;
  tmp.NGSerialNumber := NGSerial;
  RebuildInternalValues;
  FlushData(ITable);
  end;
end;

procedure TUniqueToolDataList.FlushData(Table: IACNC32StringArray);
var
FName : WideString;
begin
If assigned(Table) then
  begin
  FName := DllLocalPath+'DB\'+Table.Alias;
  Table.ExportToCSV(FName)
  end;
end;

procedure TUniqueToolDataList.FlushDataToDisk;
begin
if assigned(ITable) then
  begin
  FlushData(ITable);
  end;
end;


end.
