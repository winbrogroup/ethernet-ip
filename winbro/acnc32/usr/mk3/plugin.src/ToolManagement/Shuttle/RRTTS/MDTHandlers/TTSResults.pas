unit TTSResults;

interface
uses SysUtils, Classes, IStringArray, NamedPlugin, OCTTypes,ToolData,CNCTypes;

const
  FloatFormat = '%.1f';
  ToolTypeField = 'Tooltype';                   //1
  ToolSerialField = 'ToolSerial';               //2
  NGSerialField = 'NGSerial';                   //3
  ElectrodeCountField = 'ECount';               //4
  ElectrodePassField = 'ECountPass';            //5
  ClampsField = 'Refeed';                       //6
  ElectrodeFlowField = 'EFlow';                 //7
  EFlowColourField   = 'EFColour';              //8
  EFlowSigmaField = 'EFSigma';                  //9
  NGFlowField   = 'NGFLow';                     //10
  NFlowColourField   = 'NGFColour';             //11
  NGFlowSigmaField = 'NGFSigma';                //12
  PressureField = 'Pressure';                   //13
  PressureColourField = 'PColour';              //14
  PressureSigmaField = 'PSigma';                //15
  RotationField = 'Rotation';                   //16
  RotationColourField = 'RColour';              //17
  RotationSigmaField = 'RSigma';                //18
  MCurrentField = 'MCurrent';                   //19
  MCurrentColourField = 'MCColour';             //20
  MCurrentSigmaField = 'MCSigma';               //21
  YearField = 'Year';                           //22
  MonthField = 'Month';                         //23
  DayField = 'Day';                             //24
  HourField = 'Hour';                           //24
  MinuteField = 'Minute';                       //25
  SecondField = 'Second';                       //26
  TableFieldCount = 22;

  ElectrodeFlowSampleCountField = 'EFlowSamples';
  ElectrodeFlowSumField = 'EFlowSum';
  ElectrodeFlowSumSquaresField = 'EFlowSumSqs';
//ElectrodeFlowSigmaField = 'EFlowSig';
  NoseGuideFlowSampleCountField = 'NGFlowSamples';
  NoseGuideFlowSumField = 'NGFlowSum';
  NoseGuideFlowSumSquaresField = 'NGFlowSumSqs';
//NoseGuideFlowSigmaField = 'NGFlowSig';
  RotationSampleCountField = 'RotationSamples';
  RotationSumField = 'RotationSum';
  RotationSumSquaresField = 'RotationSumSqs';
//RotationSigmaField = 'RotationSig';
  MCurrentSampleCountField = 'MCurrentSamples';
  MCurrentSumField = 'MCurrentSum';
  MCurrentSumSquaresField = 'MCurrentSumSqs';
//MCurrentSigmaField = 'MCurrentSig';

  StatsFieldCount = 17;


  type

  TToolResultList = class;
  TToolStatsList = class;

  TTTSToolResult = class(TObject)
  private
    Index : Integer;
    Host : TToolResultList;
    FToolType  : WideString;          // 1
    FToolSerial  : Integer;           // 2
    FNGSerial  : Integer;             // 3
    FEFlow : Double;                  // 4
    FEFlowStatus : Integer;           // 5
    FNGFlow : Double;                 // 6
    FNGFlowStatus : Integer;          // 7
    FElectrodeCount : Integer;        // 8
    FElectrodeCountPass : Integer;    // 9
    FPressure : Double;               // 10
    FPressureStatus : Integer;        // 11
    FRotation : Double;               // 12
    FRotationStatus : Integer;        // 13
    FMCurrent : Double;               // 14
    FMCurrentStatus: Integer;         // 15
    FTestestDate : TDateTime;
    FYear: Integer;
    FSecond: Integer;
    FDay: Integer;
    FMonth: Integer;
    FMinute: Integer;
    FHour: Integer;
    FClampsOK: Integer;
    FTestWasAbandoned : Boolean;
    procedure SetToolTYpe(const Value: WideString);
    procedure SetToolSerial(const Value: Integer);
    procedure SetToolNGSerial(const Value: Integer);
    procedure SetEFlow(const Value: Double);
    procedure SetEFlowStatus(const Value: Integer);
    procedure SetMCurrentStatus(const Value: Integer);
    procedure SetNGFlow(const Value: Double);
    procedure SetNGFlowStatus(const Value: Integer);
    procedure SetRotation(const Value: Double);
    procedure SetRotationStatus(const Value: Integer);
    procedure SetPressure(const Value: Double);
    procedure SetPressureStatus(const Value: Integer);
    procedure SetElectrodeCount(const Value: INteger);
    procedure SetMCurrent(const Value: Double);
    procedure SetNGSerial(const Value: Integer);
    procedure SetDay(const Value: Integer);
    procedure SetHour(const Value: Integer);
    procedure SetMinute(const Value: Integer);
    procedure SetMonth(const Value: Integer);
    procedure SetSecond(const Value: Integer);
    procedure SetYear(const Value: Integer);
    procedure SetElectrodeCountPass(const Value: INteger);
    procedure SetClampsOK(const Value: Integer);
  protected
    procedure RebuildInternalValues(AIndex : Integer);
//    procedure Copy(Src: TTTSByToolTypeData);
  public
    constructor Create(AHost : TToolResultList; AIndex : Integer);
    destructor Destroy; override;
    function InStringValid(InString : String): Boolean;
    function BuildFromString(InString : String;Var AllPassed : Boolean): Boolean;
    property ToolType : WideString read FToolType write SetToolTYpe;
    property SerialNumber: Integer read FToolSerial write SetToolSerial;
    property ClampsOK         : Integer read FClampsOK write SetClampsOK;
    property ElectrodeCount   : INteger read FElectrodeCount write SetElectrodeCount;
    property ElectrodeCountPass: INteger read FElectrodeCountPass write SetElectrodeCountPass;
    property NGSerialNumber: Integer read FNGSerial write SetNGSerial;
    property NoseGuideFlow : Double read FNGFlow write SetNGFlow;
    property NoseGuideFlowStatus : Integer read FNGFlowStatus write SetNGFlowStatus;
    property ElectrodeFlow : Double read FEFlow write SetEFlow;
    property ElectrodeFlowStatus : Integer read FEFlowStatus write SetEFlowStatus;
    property Pressure      : Double read FPressure write SetPressure;
    property PressureStatus      : Integer read FPressureStatus write SetPressureStatus;
    property Rotation      : Double read FRotation write SetRotation;
    property RotationStatus      : Integer read FRotationStatus write SetRotationStatus;
    property MCurrent      : Double read FMCurrent write SetMCurrent;
    property MCurrentStatus      : Integer read FMCurrentStatus write SetMCurrentStatus;
    property Year : Integer read FYear write SetYear;
    property Month : Integer read FMonth write SetMonth;
    property Day : Integer read FDay write SetDay;
    property Hour : Integer read FHour write SetHour;
    property Minute : Integer read FMinute write SetMinute;
    property Second : Integer read FSecond write SetSecond;
    property TestWasAbandoned : Boolean read FTestWasAbandoned;
  end;




TToolResultList = class(TAbstractStringData)
    ResultList : TList;
  private
    FStatsHandler: TToolStatsList;
    procedure FlushData(Table: IACNC32StringArray);
    function GetResult(index: Integer): TTTSToolResult;
    procedure CleanTable;
    procedure CreateWithName(TableName: string; Clean: Boolean);
    function ParseResultString(RString : String;var NewResult:TTTSToolResult;Var AllTestsPassed: Boolean):Boolean;
  protected
    procedure RebuildSingleValue(Row, Col :Integer); override;
    procedure RebuildInternalValues; override;
    procedure CreateTable;
    procedure RebuildIndex;
  public
    FilteredResults : TList;
    constructor Create; overload;
    constructor Create(TableName: string); overload;
    destructor Destroy; override;
    property StatsHandler :  TToolStatsList read FStatsHandler write FStatsHandler;
    function AddResult :TTTSToolResult;
//    function MakeNewResult : TTTSToolResult;
//    function IndexOf(ToolType : String) : Integer;

    function Count : Integer;
    property TestResult[index : Integer] : TTTSToolResult read GetResult; default;
    procedure Clean; override;
    procedure FlushDataToDisk;
    function AddResultFromString(RString : String;UseSigmas : Boolean;var ErrorStr : String): TTTSToolResult;
    function FilterForToolID(TID : String;FindCount : Integer): Integer;
    function FilterLastNResults(FindCount : Integer): Integer;
    function FilterForToolIDandSerial(TID : String;SN : INteger;FindCount : Integer): Integer;
  end;





  TTTSToolStats = class(TObject)
  private
    Index : Integer;
    Host : TToolStatsList;
    FToolType  : WideString;
    FNoseGuideFlowSum: Double;
    FNoseGuideFlowSampleCount: Integer;
    FMCurrentSum: Double;
    FRotationSum: Double;
    FNoseGuideFlowSigma: Double;
    FMCurrentSampleCount: Integer;
    FRotationSampleCount: Integer;
    FElectrodeFlowSum: Double;
    FMCurrentSigma: Double;
    FRotationSigma: Double;
    FElectrodeFlowSigma: Double;
    FNoseGuideFlowSumSquares: Double;
    FElectrodeFlowSamples: Integer;
    FMCurrentSumSquares: Double;
    FRotationSumSquares: Double;
    FElectrodeFlowSumSquares: Double;
    procedure SetElectrodeFlowSamples(const Value: Integer);
    procedure SetElectrodeFlowSigma(const Value: Double);
    procedure SetElectrodeFlowSum(const Value: Double);
    procedure SetElectrodeFlowSumSquares(const Value: Double);
    procedure SetMCurrentSampleCount(const Value: Integer);
    procedure SetMCurrentSigma(const Value: Double);
    procedure SetMCurrentSum(const Value: Double);
    procedure SetMCurrentSumSquares(const Value: Double);
    procedure SetNoseGuideFlowSampleCount(const Value: Integer);
    procedure SetNoseGuideFlowSigma(const Value: Double);
    procedure SetNoseGuideFlowSum(const Value: Double);
    procedure SetNoseGuideFlowSumSquares(const Value: Double);
    procedure SetRotationSampleCount(const Value: Integer);
    procedure SetRotationSigma(const Value: Double);
    procedure SetRotationSum(const Value: Double);
    procedure SetRotationSumSquares(const Value: Double);
    procedure SetToolTYpe(const Value: WideString);          // 1
  public
    procedure RebuildInternalValues(AIndex : Integer);
    constructor Create(AHost : TToolStatsList; AIndex : Integer);
    destructor Destroy; override;

    property ToolType : WideString read FToolType write SetToolTYpe;
    property ElectrodeFlowSampleCount : Integer read FElectrodeFlowSamples write SetElectrodeFlowSamples;
    property ElectrodeFlowSum         : Double read FElectrodeFlowSum      write SetElectrodeFlowSum;
    property ElectrodeFlowSumSquares  : Double read FElectrodeFlowSumSquares  write SetElectrodeFlowSumSquares;
    property ElectrodeFlowSigma       : Double read FElectrodeFlowSigma  write   SetElectrodeFlowSigma;
    property NoseGuideFlowSampleCount : Integer read FNoseGuideFlowSampleCount  write SetNoseGuideFlowSampleCount;
    property NoseGuideFlowSum         : Double read FNoseGuideFlowSum  write SetNoseGuideFlowSum;
    property NoseGuideFlowSumSquares  : Double read FNoseGuideFlowSumSquares  write SetNoseGuideFlowSumSquares;
    property NoseGuideFlowSigma       : Double read FNoseGuideFlowSigma  write SetNoseGuideFlowSigma;
    property RotationSampleCount      : Integer read FRotationSampleCount  write SetRotationSampleCount;
    property RotationSum              : Double read FRotationSum  write SetRotationSum;
    property RotationSumSquares       : Double read FRotationSumSquares  write SetRotationSumSquares;
    property RotationSigma            : Double read FRotationSigma  write SetRotationSigma;
    property MCurrentSampleCount      : Integer read FMCurrentSampleCount  write SetMCurrentSampleCount;
    property MCurrentSum              : Double read FMCurrentSum  write SetMCurrentSum;
    property MCurrentSumSquares       : Double read FMCurrentSumSquares  write SetMCurrentSumSquares;
    property MCurrentSigma            : Double read FMCurrentSigma  write SetMCurrentSigma;
  end;


TToolStatsList = class(TAbstractStringData)

    ResultStatsList : TList;
  private
    procedure FlushData(Table: IACNC32StringArray);
    function GetStats(index: Integer): TTTSToolStats;
    procedure CleanTable;
    procedure CreateWithName(TableName: string; Clean: Boolean);
    function AddStatsEntry: TTTSToolStats;
    function CalculateSigma(Sum,SumSquares : Double;
                            Samples : Integer;
                            ResultValue : Double;
                            var SigmaVar : Double): Double;
  protected
    procedure RebuildSingleValue(Row, Col :Integer); override;
    procedure RebuildInternalValues; override;
    procedure CreateTable;
    procedure RebuildIndex;
  public
    constructor Create; overload;
    constructor Create(TableName: string); overload;
    destructor Destroy; override;
    function AddStatsRecordFromResult(TestResult : TTTSToolResult;UseSigmas : Boolean): TTTSToolStats;
    function UpdateStatsRecordFromResult(var TestResult : TTTSToolResult;UpdateSigmas : Boolean;UseSigmas : Boolean): TTTSToolStats;
    function ProcessResult(TestResult : TTTSToolResult;AllPassed : Boolean;UseSigmas : Boolean): TTTSToolStats;
    function Count : Integer;
    property StatsAtIndex[index : Integer] : TTTSToolStats read GetStats; default;
    procedure Clean; override;
    function FindStatsForTool(ToolType: string): TTTSToolStats;
    procedure FlushDataToDisk;
  end;




implementation

{ TTTSToolResult }


constructor TTTSToolResult.Create(AHost: TToolResultList; AIndex: Integer);
begin
  inherited Create;
  Index := AIndex;
  Host := AHost;
  FTestWasAbandoned := False;
end;


procedure TTTSToolResult.RebuildInternalValues(AIndex: Integer);
begin
  Index := AIndex;
  FToolType  := Host.GetFieldString(Index, ToolTypeField);               // 1
  FToolSerial  := Host.GetFieldInteger(Index,ToolSerialField);           // 2
  FNGSerial    := Host.GetFieldInteger(Index,NGSerialField);             // 3
  FElectrodeCount := Host.GetFieldInteger(Index,ElectrodeCountField);    // 4
  FElectrodeCountPass := Host.GetFieldInteger(Index,ElectrodePassField); // 5
  FClampsOK := Host.GetFieldInteger(Index,ClampsField);                  // 6
  FEFlow := Host.GetFieldDouble(Index,ElectrodeFlowField);               // 7
  FEFlowStatus := Host.GetFieldInteger(Index,EFlowColourField);          // 8
  FNGFlow := Host.GetFieldDouble(Index,NGFlowField);                     // 9
  FNGFlowStatus := Host.GetFieldInteger(Index,NFlowColourField);         // 10
  FPressure := Host.GetFieldDouble(Index,PressureField);                 // 11
  FPressureStatus := Host.GetFieldInteger(Index,PressureColourField);    // 12
  FRotation := Host.GetFieldDouble(Index,RotationField);                 // 13
  FRotationStatus := Host.GetFieldInteger(Index,RotationColourField);    // 14
  FMCurrent := Host.GetFieldDouble(Index,MCurrentField);                 // 15
  FMCurrentStatus := Host.GetFieldInteger(Index,MCurrentColourField);    // 16
  FYear := Host.GetFieldInteger(Index,YearField);                        // 17
  FMonth := Host.GetFieldInteger(Index,MonthField);                      // 18
  FDay := Host.GetFieldInteger(Index,DayField);                          // 19
  FHour := Host.GetFieldInteger(Index,HourField);                        // 20
  FMinute := Host.GetFieldInteger(Index,MinuteField);                    // 21
  FSecond := Host.GetFieldInteger(Index,SecondField);                    // 22

  end;
{
  ToolTypeField = 'Tooltype';                   //1
  ToolSerialField = 'ToolSerial';               //2
  NGSerialField = 'NGSerial';                   //3
  ElectrodeCountField = 'ECount';               //4
  ElectrodePassField = 'ECountPass';            //5
  ClampsField = 'Clamps';                       //6
  ElectrodeFlowField = 'EFlow';                 //7
  EFlowColourField   = 'EFlowColour';           //8
  NGFlowField   = 'NGFLow';                     //9
  NFlowColourField   = 'NGFlowColour';          //10
  PressureField = 'Pressure';                   //11
  PressureColourField = 'PressureColour';       //12
  RotationField = 'Rotation';                   //13
  RotationColourField = 'RotationColour';       //14
  MCurrentField = 'MCurrent';                   //15
  MCurrentColourField = 'MCurrentColour';       //16
  YearField = 'Year';                           //17
  MonthField = 'Month';                         //18
  DayField = 'Day';                             //19
  HourField = 'Hour';                           //20
  MinuteField = 'Minute';                       //21
  SecondField = 'Second';                       //22
}
procedure TTTSToolResult.SetMCurrent(const Value: Double);
begin
  FMCurrent := Value;
  Host.SetFieldData(Index,MCurrentField,Format(FloatFormat,[Value]));
end;

procedure TTTSToolResult.SetEFlow(const Value: Double);
begin
FEFlow := Value;
Host.SetFieldData(Index,ElectrodeFlowField,Format(FloatFormat,[Value]));
end;

procedure TTTSToolResult.SetClampsOK(const Value: Integer);
begin
  FClampsOK := Value;
  Host.SetFieldData(Index,ClampsField,IntToStr(Value));
end;

procedure TTTSToolResult.SetNGFlow(const Value: Double);
begin
  FNGFlow := Value;
  Host.SetFieldData(Index,NGFlowField,Format(FloatFormat,[Value]));
end;

procedure TTTSToolResult.SetToolTYpe(const Value: WideString);
begin
  FToolType := Value;
  Host.SetFieldData(Index,ToolTypeField,Value);
end;


procedure TTTSToolResult.SetPressureStatus(const Value: Integer);
begin
 FPressureStatus :=Value;
 Host.SetFieldData(Index,PressureColourField,IntToStr(Value));
end;

procedure TTTSToolResult.SetElectrodeCount(const Value: INteger);
begin
 FElectrodeCount :=Value;
 Host.SetFieldData(Index,ElectrodeCountField,IntToStr(Value));
end;

procedure TTTSToolResult.SetToolNGSerial(const Value: Integer);
begin
 FNGSerial :=Value;
 Host.SetFieldData(Index,NGSerialField,IntToStr(Value));
end;

procedure TTTSToolResult.SetRotation(const Value: Double);
begin
 FRotation := Value;
 Host.SetFieldData(Index,RotationField,Format(FloatFormat,[Value]));
end;

procedure TTTSToolResult.SetEFlowStatus(const Value: Integer);
begin
 FEFlowStatus :=Value;
 Host.SetFieldData(Index,EFlowColourField,IntToStr(Value));
end;

procedure TTTSToolResult.SetPressure(const Value: Double);
begin
 FPressure := Value;
 Host.SetFieldData(Index,PressureField,Format(FloatFormat,[Value]));
end;

procedure TTTSToolResult.SetNGFlowStatus(const Value: Integer);
begin
 FNGFlowStatus :=Value;
 Host.SetFieldData(Index,NFlowColourField,IntToStr(Value));
end;

procedure TTTSToolResult.SetToolSerial(const Value: Integer);
begin
 FToolSerial :=Value;
 Host.SetFieldData(Index,ToolSerialField,IntToStr(Value));
end;

destructor TTTSToolResult.Destroy;
begin
  inherited;
end;

procedure TTTSToolResult.SetRotationStatus(const Value: Integer);
begin
 FRotationStatus :=Value;
 Host.SetFieldData(Index,RotationColourField,IntToStr(Value));
end;

procedure TTTSToolResult.SetMCurrentStatus(const Value: Integer);
begin
 FMCurrentStatus :=Value;
 Host.SetFieldData(Index,MCurrentColourField,IntToStr(Value));
end;

procedure TTTSToolResult.SetNGSerial(const Value: Integer);
begin
  FNGSerial := Value;
  Host.SetFieldData(Index,NGSerialField,IntToStr(Value));
end;

procedure TTTSToolResult.SetYear(const Value: Integer);
begin
  FYear := Value;
  Host.SetFieldData(Index,YearField,IntToStr(Value));
end;

procedure TTTSToolResult.SetSecond(const Value: Integer);
begin
  FSecond := Value;
  Host.SetFieldData(Index,SecondField,IntToStr(Value));
end;

procedure TTTSToolResult.SetDay(const Value: Integer);
begin
  FDay := Value;
  Host.SetFieldData(Index,DayField,IntToStr(Value));
end;

procedure TTTSToolResult.SetMonth(const Value: Integer);
begin
  FMonth := Value;
  Host.SetFieldData(Index,MonthField,IntToStr(Value));
end;

procedure TTTSToolResult.SetMinute(const Value: Integer);
begin
  FMinute := Value;
  Host.SetFieldData(Index,MinuteField,IntToStr(Value));
end;

procedure TTTSToolResult.SetHour(const Value: Integer);
begin
  FHour := Value;
  Host.SetFieldData(Index,HourField,IntToStr(Value));
end;

procedure TTTSToolResult.SetElectrodeCountPass(const Value: INteger);
begin
  FElectrodeCountPass := Value;
  Host.SetFieldData(Index,ElectrodePassField,IntToStr(Value));
end;

{function TTTSToolResult.AddResultFromString(RString : String): Boolean;
begin
if ParseResultString then
  begin
  end;
end;}
function TTTSToolResult.InStringValid(InString: String): Boolean;
var
Buffer : String;
StrVal : String;
DblVal : Double;
IntVal : Integer;
begin
Result := False;
Buffer := InString;
// ToolType
Result := False;
strVal := ReadDelimited(Buffer,',');
if StrVal = '' then
  begin
  exit;
  end
else
  exit;

// ToolSerail
StrVal := ReadDelimited(Buffer,',');
if StrVal <> '' then
  begin
  IntVal := StrToIntDef(StrVal,-1);
  If Intval < 0 then  exit;
  end
else
  exit;

// NoseGuide Serail
StrVal := ReadDelimited(Buffer,',');
if StrVal <> '' then
  begin
  IntVal := StrToIntDef(StrVal,-1);
  If Intval < 0 then exit
  end
else
  exit;

// Clamps Pass/Fail
StrVal := ReadDelimited(Buffer,',');
if StrVal <> '' then
  begin
  IntVal := StrToIntDef(StrVal,-3);
  If Intval < 0 then exit
  end
else
  exit;

// Electrode Count
StrVal := ReadDelimited(Buffer,',');
if StrVal <> '' then
  begin
  IntVal := StrToIntDef(StrVal,-1);
  If Intval < 0 then exit
  end
else
  exit;

// Electrode Count Pass
StrVal := ReadDelimited(Buffer,',');
if StrVal <> '' then
  begin
  IntVal := StrToIntDef(StrVal,-3);
  If Intval >= 0 then
    ElectrodeCountPass := INtVal
  else if Intval = -1 then
      begin
      ElectrodeCountPass := 3;
      FTestWasAbandoned := True;
      end
  else
    exit;
  end
else
  exit;

// EFlow
StrVal := ReadDelimited(Buffer,',');
if StrVal <> '' then
  begin
  DblVal := StrToFloatDef(StrVal,-9999);
  if DBLVal > -9998 then
    ElectrodeFlow := DBLVal
  else
    exit;
  end
else
  exit;

// EFlow Pass/Fail
StrVal := ReadDelimited(Buffer,',');
if StrVal <> '' then
  begin
  IntVal := StrToIntDef(StrVal,-3);
  If Intval >= 0 then
    ElectrodeFlowStatus := INtVal
  else if Intval = -1 then
      begin
      ElectrodeFlowStatus := 3;
      FTestWasAbandoned := True;
      end
  else
    exit;
  end
else
  exit;

  // NG Flow
StrVal := ReadDelimited(Buffer,',');
if StrVal <> '' then
  begin
  DblVal := StrToFloatDef(StrVal,-9999);
  if DBLVal > -9998 then
    NoseGuideFlow := DBLVal
  else
    exit;
  end
else
  exit;

// NGFlow Pass/Fail
StrVal := ReadDelimited(Buffer,',');
if StrVal <> '' then
  begin
  IntVal := StrToIntDef(StrVal,-3);
  If Intval >= 0 then
    NoseGuideFlowStatus := INtVal
  else if Intval = -1 then
      begin
      NoseGuideFlowStatus := 3;
      FTestWasAbandoned := True;
      end
  else
    exit;
  end
else
  exit;

// Pressure
StrVal := ReadDelimited(Buffer,',');
if StrVal <> '' then
  begin
  DblVal := StrToFloatDef(StrVal,-9999);
  if DBLVal > -9998 then
    Pressure := DBLVal
  else
    exit;
  end
else
  exit;

// Pressure Pass/Fail
StrVal := ReadDelimited(Buffer,',');
if StrVal <> '' then
  begin
  IntVal := StrToIntDef(StrVal,-3);
  If Intval >= 0 then
    PressureStatus := INtVal
  else if Intval = -1 then
      begin
      PressureStatus := 3;
      FTestWasAbandoned := True;
      end
  else
    exit;
  end
else
  exit;

// Rotation
StrVal := ReadDelimited(Buffer,',');
if StrVal <> '' then
  begin
  DblVal := StrToFloatDef(StrVal,-9999);
  if DBLVal > -9998 then
    Rotation := DBLVal
  else
    exit;
  end
else
  exit;

// Rotation Pass/Fail
StrVal := ReadDelimited(Buffer,',');
if StrVal <> '' then
  begin
  IntVal := StrToIntDef(StrVal,-3);
  If Intval >= 0 then
    RotationStatus := INtVal
  else if Intval = -1 then
      begin
      RotationStatus := 3;
      FTestWasAbandoned := True;
      end
  else
    exit;
  end
else
  exit;

// Motor Current
StrVal := ReadDelimited(Buffer,',');
if StrVal <> '' then
  begin
  DblVal := StrToFloatDef(StrVal,-9999);
  if DBLVal > -9998 then
    MCurrent := DBLVal
  else
    exit;
  end
else
  exit;

// Motor Current Pass/Fail
StrVal := ReadDelimited(Buffer,',');
if StrVal <> '' then
  begin
  IntVal := StrToIntDef(StrVal,-3);
  If Intval >= 0 then
    MCurrentStatus := INtVal
  else if Intval = -1 then
      begin
      MCurrentStatus := 3;
      FTestWasAbandoned := True;
      end
  else
    exit;
  end
else
  exit;


// Year
StrVal := ReadDelimited(Buffer,',');
if StrVal <> '' then
  begin
  IntVal := StrToIntDef(StrVal,-1);
  If Intval >= 2010 then
    Year := INtVal
  else
    exit;
  end
else
  exit;

// Month
StrVal := ReadDelimited(Buffer,',');
if StrVal <> '' then
  begin
  IntVal := StrToIntDef(StrVal,-1);
  If ((Intval >= 1) and (Intval <= 12))  then
    Month := INtVal
  else
    exit;
  end
else
  exit;

// Day
StrVal := ReadDelimited(Buffer,',');
if StrVal <> '' then
  begin
  IntVal := StrToIntDef(StrVal,-1);
  If ((Intval >= 1) and (Intval <= 31))  then
    Day := INtVal
  else
    exit;
  end
else
  exit;

// Hour
StrVal := ReadDelimited(Buffer,',');
if StrVal <> '' then
  begin
  IntVal := StrToIntDef(StrVal,-1);
  If ((Intval >= 1) and (Intval <= 24))  then
    Hour := INtVal
  else
    exit;
  end
else
  exit;

// Minute
StrVal := ReadDelimited(Buffer,',');
if StrVal <> '' then
  begin
  IntVal := StrToIntDef(StrVal,-1);
  If ((Intval >= 1) and (Intval <= 60))  then
    Minute := INtVal
  else
    exit;
  end
else
  exit;

// Second
StrVal := ReadDelimited(Buffer,',');
if StrVal <> '' then
  begin
  IntVal := StrToIntDef(StrVal,-1);
  If ((Intval >= 1) and (Intval <= 60))  then
    Second := INtVal
  else
    exit;
  end
else
  exit;

StrVal := ReadDelimited(Buffer,',');
if StrVal <> '' then
  begin
  IntVal := StrToIntDef(StrVal,-1);
  end
else
  exit;

Result := True;

end;
function ReadDelimitedDef(var S : string; Delimiter : Char;Default:String) : string;
begin
Result := ReadDelimited(S,Delimiter);
if Result = '' then Result := Default;
end;

function TTTSToolResult.BuildFromString(InString: String;Var AllPassed : Boolean): Boolean;
var
Buffer : String;
StrVal : String;
DblVal : Double;
IntVal : Integer;
begin
Result := False;
Buffer := InString;
AllPassed := False;
FTestWasAbandoned := False;
// ToolType
Result := False;
strVal := ReadDelimitedDef(Buffer,',','???');
if StrVal <> '' then
  begin
  ToolType := StrVal;
  end
else
  exit;

// ToolSerail
StrVal := ReadDelimitedDef(Buffer,',','1');
if StrVal <> '' then
  begin
  IntVal := StrToIntDef(StrVal,-1);
  If Intval >= 0 then
    SerialNumber := INtVal
  else
    exit;
  end
else
  exit;

// NoseGuide Serail
StrVal := ReadDelimitedDef(Buffer,',','1');
if StrVal <> '' then
  begin
  IntVal := StrToIntDef(StrVal,-1);
  If Intval >= 0 then
    NGSerialNumber := INtVal
  else
    exit;
  end
else
  exit;

// Clamps Pass/Fail
StrVal := ReadDelimitedDef(Buffer,',','-1');
if StrVal <> '' then
  begin
  IntVal := StrToIntDef(StrVal,-3);
  If Intval >= 0 then
    ClampsOK := INtVal
  else if Intval = -1 then
      begin
      ClampsOk := 3;
      FTestWasAbandoned := True;
      end
  else
    exit;
  end
else
  exit;

// Electrode Count
StrVal := ReadDelimitedDef(Buffer,',','1');
if StrVal <> '' then
  begin
  IntVal := StrToIntDef(StrVal,-1);
  If Intval >= 0 then
    ElectrodeCount := INtVal
  else
    exit;
  end
else
  exit;

// Electrode Count Pass
StrVal := ReadDelimitedDef(Buffer,',','-1');
if StrVal <> '' then
  begin
  IntVal := StrToIntDef(StrVal,-3);
  If Intval >= 0 then
    ElectrodeCountPass := INtVal
  else if Intval = -1 then
      begin
      ElectrodeCountPass := 3;
      FTestWasAbandoned := True;
      end
  else
    exit;
  end
else
  exit;

// EFlow
StrVal := ReadDelimitedDef(Buffer,',','0');
if StrVal <> '' then
  begin
  DblVal := StrToFloatDef(StrVal,-9999);
  if DBLVal > -9998 then
    ElectrodeFlow := DBLVal
  else
    exit;
  end
else
  exit;

// EFlow Pass/Fail
StrVal := ReadDelimitedDef(Buffer,',','-1');
if StrVal <> '' then
  begin
  IntVal := StrToIntDef(StrVal,-3);
  If Intval >= 0 then
    ElectrodeFlowStatus := INtVal
  else if Intval = -1 then
      begin
      ElectrodeFlowStatus := 3;
      FTestWasAbandoned := True;
      end
  else
    exit;
  end
else
  exit;

// NG Flow
StrVal := ReadDelimitedDef(Buffer,',','0');
if StrVal <> '' then
  begin
  DblVal := StrToFloatDef(StrVal,-9999);
  if DBLVal > -9998 then
    NoseGuideFlow := DBLVal
  else
    exit;
  end
else
  exit;

// NGFlow Pass/Fail
StrVal := ReadDelimitedDef(Buffer,',','-1');
if StrVal <> '' then
  begin
  IntVal := StrToIntDef(StrVal,-3);
  If Intval >= 0 then
    NoseGuideFlowStatus := INtVal
  else if Intval = -1 then
      begin
      NoseGuideFlowStatus := 3;
      FTestWasAbandoned := True;
      end
  else
    exit;
  end
else
  exit;

// Pressure
StrVal := ReadDelimitedDef(Buffer,',','0');
if StrVal <> '' then
  begin
  DblVal := StrToFloatDef(StrVal,-9999);
  if DBLVal > -9998 then
    Pressure := DBLVal
  else
    exit;
  end
else
  exit;

// Pressure Pass/Fail
StrVal := ReadDelimitedDef(Buffer,',','-1');
if StrVal <> '' then
  begin
  IntVal := StrToIntDef(StrVal,-3);
  If Intval >= 0 then
    PressureStatus := INtVal
  else if Intval = -1 then
      begin
      PressureStatus := 3;
      FTestWasAbandoned := True;
      end
  else
    exit;
  end
else
  exit;

// Rotation
StrVal := ReadDelimitedDef(Buffer,',','0');
if StrVal <> '' then
  begin
  DblVal := StrToFloatDef(StrVal,-9999);
  if DBLVal > -9998 then
    Rotation := DBLVal
  else
    exit;
  end
else
  exit;

// Rotation Pass/Fail
StrVal := ReadDelimitedDef(Buffer,',','-1');
if StrVal <> '' then
  begin
  IntVal := StrToIntDef(StrVal,-3);
  If Intval >= 0 then
    RotationStatus := INtVal
  else if Intval = -1 then
      begin
      RotationStatus := 3;
      FTestWasAbandoned := True;
      end
  else
    exit;
  end
else
  exit;

// Motor Current
StrVal := ReadDelimitedDef(Buffer,',','0');
if StrVal <> '' then
  begin
  DblVal := StrToFloatDef(StrVal,-9999);
  if DBLVal > -9998 then
    MCurrent := DBLVal
  else
    exit;
  end
else
  exit;

// Motor Current Pass/Fail
StrVal := ReadDelimitedDef(Buffer,',','-1');
if StrVal <> '' then
  begin
  IntVal := StrToIntDef(StrVal,-3);
  If Intval >= 0 then
    MCurrentStatus := INtVal
  else if Intval = -1 then
      begin
      MCurrentStatus := 3;
      FTestWasAbandoned := True;
      end
  else
    exit;
  end
else
  exit;


// Year
StrVal := ReadDelimitedDef(Buffer,',','2010');
if StrVal <> '' then
  begin
  IntVal := StrToIntDef(StrVal,-1);
  If Intval >= 2010 then
    Year := INtVal
  else
    exit;
  end
else
  exit;

// Month
StrVal := ReadDelimitedDef(Buffer,',','1');
if StrVal <> '' then
  begin
  IntVal := StrToIntDef(StrVal,-1);
  If ((Intval >= 1) and (Intval <= 12))  then
    Month := INtVal
  else
    exit;
  end
else
  exit;

// Day
StrVal := ReadDelimitedDef(Buffer,',','1');
if StrVal <> '' then
  begin
  IntVal := StrToIntDef(StrVal,-1);
  If ((Intval >= 1) and (Intval <= 31))  then
    Day := INtVal
  else
    exit;
  end
else
  exit;

// Hour
StrVal := ReadDelimitedDef(Buffer,',','0');
if StrVal <> '' then
  begin
  IntVal := StrToIntDef(StrVal,-1);
  If ((Intval >= 0) and (Intval <= 24))  then
    Hour := INtVal
  else
    exit;
  end
else
  exit;

// Minute
StrVal := ReadDelimitedDef(Buffer,',','0');
if StrVal <> '' then
  begin
  IntVal := StrToIntDef(StrVal,-1);
  If ((Intval >= 0) and (Intval <= 60))  then
    Minute := INtVal
  else
    exit;
  end
else
  exit;

// Second
StrVal := ReadDelimitedDef(Buffer,',','0');
if StrVal <> '' then
  begin
  IntVal := StrToIntDef(StrVal,-1);
  If ((Intval >= 0) and (Intval <= 60))  then
    Second := INtVal
  else
    exit;
  end
else
  exit;

StrVal := ReadDelimitedDef(Buffer,',','0');
if StrVal <> '' then
  begin
  IntVal := StrToIntDef(StrVal,-1);
  AllPassed := IntVal > 0;
  end
else
  exit;
Result := True;
end;


{ TToolResultList }

function TToolResultList.GetResult(index: Integer): TTTSToolResult;
begin
  Result := TTTSToolResult(ResultList[Index]);
end;

function TToolResultList.AddResult :TTTSToolResult;
begin
  Lock;
  try
    Result := TTTSToolResult.Create(Self, -1);
    Result.Index := ITable.RecordCount;
    ResultList.Add(Result);
    ITable.Append(1);
  finally
    Unlock;
  end;
end;



constructor TToolResultList.Create(TableName: string);
begin
  if StringArraySet.CreateArray(TableName, Self, ITable) <> AAR_OK then begin
    StringArraySet.UseArray(TableName, Self, ITable);
    if ITable.FieldCount <> TableFieldCount then begin
      CleanTable;
      CreateTable;
    end;
  end else begin
    CreateTable;
  end;
  ResultList := TList.Create;
  RebuildInternalValues;
end;

constructor TToolResultList.Create;
begin
  inherited Create;
  CreateWithName('Results', True);
end;

procedure TToolResultList.CreateTable;
begin
  BeginUpdate;
  try
  ITable.Clear;
  ITable.AddField(ToolTypeField);
  ITable.AddField(ToolSerialField);
  ITable.AddField(NGSerialField);
  ITable.AddField(ElectrodeCountField);
  ITable.AddField(ElectrodeFlowField);
  ITable.AddField(ClampsField);
  ITable.AddField(EFlowColourField);
  ITable.AddField(NGFlowField);
  ITable.AddField(NFlowColourField);
  ITable.AddField(PressureField);
  ITable.AddField(PressureColourField);
  ITable.AddField(RotationField);
  ITable.AddField(RotationColourField);
  ITable.AddField(MCurrentField);
  ITable.AddField(MCurrentColourField);
  ITable.AddField(YearField);
  ITable.AddField(MonthField);
  ITable.AddField(DayField);
  ITable.AddField(HourField);
  ITable.AddField(MinuteField);
  ITable.AddField(SecondField);
  finally
    EndUpdate;
  end;
{
  ToolTypeField = 'Tooltype';                   //1
  ToolSerialField = 'ToolSerial';               //2
  NGSerialField = 'NGSerial';                   //3
  ElectrodeCountField = 'ECount';               //4
  ElectrodePassField = 'ECountPass';            //5
  ClampsField = 'Clamps';                       //6
  ElectrodeFlowField = 'EFlow';                 //7
  EFlowColourField   = 'EFlowColour';           //8
  NGFlowField   = 'NGFLow';                     //9
  NFlowColourField   = 'NGFlowColour';          //10
  PressureField = 'Pressure';                   //11
  PressureColourField = 'PressureColour';       //12
  RotationField = 'Rotation';                   //13
  RotationColourField = 'RotationColour';       //14
  MCurrentField = 'MCurrent';                   //15
  MCurrentColourField = 'MCurrentColour';       //16
  YearField = 'Year';                           //17
  MonthField = 'Month';                         //18
  DayField = 'Day';                             //19
  HourField = 'Hour';                           //20
  MinuteField = 'Minute';                       //21
  SecondField = 'Second';                       //22
}
end;

procedure TToolResultList.RebuildInternalValues;
var I : Integer;
    Tmp : TTTSToolResult;
begin
  for I := ITable.RecordCount to ResultList.Count - 1 do begin
    TestResult[0].Free;
    ResultList.Delete(0);
  end;

  for I := 0 to ITable.RecordCount - 1 do begin
    if I >= ResultList.Count then begin
      Tmp := TTTSToolResult.Create(Self, I);
      ResultList.Add(Tmp);
      Tmp.RebuildInternalValues(I);
    end else begin
      TestResult[I].RebuildInternalValues(I);
    end;
  end;
end;

procedure TToolResultList.FlushData(Table: IACNC32StringArray);
var
FName : WideString;
RCount : Integer;
begin
If assigned(Table) then
  begin
  FName := DllLocalPath+'DB\'+Table.Alias;
  RCount := Table.RecordCount;
  if Rcount > 0 then
    begin
    Table.ExportToCSV(FName)
    end;
  end;
end;

procedure TToolResultList.Clean;
begin
  inherited;

end;

procedure TToolResultList.CleanTable;
var F: WideString;
begin
  BeginUpdate;
  try
    while ITable.FieldCount > 0 do begin
      ITable.FieldNameAtIndex(0, F);
      ITable.DeleteField(F);
    end;
  finally
    EndUpdate;
  end;
end;

function TToolResultList.Count: Integer;
begin
  Result := ResultList.Count;
end;

procedure TToolResultList.FlushDataToDisk;
begin
if assigned(ITable) then
  begin
  FlushData(ITable);
  end;
end;

procedure TToolResultList.RebuildIndex;
begin

end;

procedure TToolResultList.RebuildSingleValue(Row, Col: Integer);
begin
  inherited;
  TestResult[Row].RebuildInternalValues(Row);
end;


destructor TToolResultList.Destroy;
begin
  ITable := Nil;
  FilteredResults.Clear;
  FilteredResults.Free;
  inherited;
end;

procedure TToolResultList.CreateWithName(TableName: string; Clean: Boolean);
begin
  if StringArraySet.CreateArray(TableName, Self, ITable) <> AAR_OK then begin
    StringArraySet.UseArray(TableName, Self, ITable);
    if ITable.FieldCount <> TableFieldCount then begin
      CleanTable;
      CreateTable;
    end;
  end else begin
    CreateTable;
  end;
  ResultList := TList.Create;
  FilteredResults := TList.Create;
  RebuildInternalValues;
end;

function TToolResultList.AddResultFromString(RString: String;UseSigmas : Boolean;var ErrorStr : String): TTTSToolResult;
var
AllPassed : Boolean;
begin
Result := nil;
ErrorStr := '';
try
if ParseResultString(Rstring,Result,AllPassed) then
  begin
    if assigned(StatsHandler) then
      begin
      StatsHandler.ProcessResult(Result,AllPassed,UseSigmas);
      end;
  end;
except
ErrorStr := 'Exception During Process result';
end
end;

function TToolResultList.ParseResultString(RString: String; var NewResult: TTTSToolResult;Var AllTestsPassed: Boolean): Boolean;
VAR
Buffer : String;
StrVal : String;
begin
Result := False;
Buffer := RString;
StrVal := ReadDelimited(Buffer,',');
if StrVal = '' then exit;
NewResult := AddResult;
if NewResult.BuildFromString(RString,AllTestsPassed) then
  begin
  FlushDataToDisk;
  Result := True;
  end
else
  begin
  NewResult.ToolType := 'BadResult';
  FlushDataToDisk;
  end
end;


function TToolResultList.FilterForToolID(TID: String; FindCount: Integer): Integer;
var
I : INteger;
ResultData : TTTSToolResult;
SearchString : String;
begin
FilteredResults.Clear;
if Count > 0 then
  begin
  SearchString := Uppercase(Trim(TID));
  I := Count-1;
  while (I >= 0) and (FilteredResults.Count < FindCount) do
    begin
    try
    ResultData := TestResult[I];
    if Uppercase(Trim(ResultData.ToolType)) = SearchString then
      begin
      FilteredResults.Add(ResultData);
      end;
    finally
    dec(I);
    end;
    end
  end;
Result := FilteredResults.Count;
end;

function TToolResultList.FilterLastNResults(FindCount: Integer): Integer;
var
I : INteger;
ResultData : TTTSToolResult;
begin
FilteredResults.Clear;
if Count > 0 then
  begin
  I := Count-1;
  while (I >= 0) and (FilteredResults.Count < FindCount) do
    begin
    try
    ResultData := TestResult[I];
    FilteredResults.Add(ResultData);
    finally
    dec(I);
    end;
    end
  end;
Result := FilteredResults.Count;
end;

function TToolResultList.FilterForToolIDandSerial(TID: String; SN,FindCount: Integer): Integer;
var
I : INteger;
ResultData : TTTSToolResult;
SearchString : String;
begin
FilteredResults.Clear;
if Count > 0 then
  begin
  SearchString := Uppercase(Trim(TID));
  I := Count-1;
  while (I >= 0) and (FilteredResults.Count < FindCount) do
    begin
    try
    ResultData := TestResult[I];
    if Uppercase(Trim(ResultData.ToolType)) = SearchString then
      begin
      if ResultData.SerialNumber = SN then FilteredResults.Add(ResultData);
      end;
    finally
    dec(I);
    end;
    end
  end;
Result := FilteredResults.Count;
end;

{ TTTSToolStats }


procedure TTTSToolStats.SetToolTYpe(const Value: WideString);
begin
  FToolType := Value;
  Host.SetFieldData(Index,ToolTypeField,FToolType);
end;

procedure TTTSToolStats.SetNoseGuideFlowSampleCount(const Value: Integer);
begin
  FNoseGuideFlowSampleCount := Value;
  Host.SetFieldData(Index,NoseGuideFlowSampleCountField,IntToStr(FNoseGuideFlowSampleCount));
end;

procedure TTTSToolStats.SetMCurrentSum(const Value: Double);
begin
  FMCurrentSum := Value;
  Host.SetFieldData(Index,MCurrentSumField,FloatToStr(FMCurrentSum));
end;
procedure TTTSToolStats.SetNoseGuideFlowSum(const Value: Double);
begin
  FNoseGuideFlowSum := Value;
  Host.SetFieldData(Index,NoseGuideFlowSumField,FloatToStr(FMCurrentSum));
end;

procedure TTTSToolStats.SetRotationSum(const Value: Double);
begin
  FRotationSum := Value;
  Host.SetFieldData(Index,RotationSumField,FloatToStr(FMCurrentSum));
end;

procedure TTTSToolStats.SetNoseGuideFlowSigma(const Value: Double);
begin
  FNoseGuideFlowSigma := Value;
  Host.SetFieldData(Index,NGFlowSigmaField,FloatToStr(FNoseGuideFlowSigma));
end;

procedure TTTSToolStats.SetMCurrentSampleCount(const Value: Integer);
begin
  FMCurrentSampleCount := Value;
  Host.SetFieldData(Index,MCurrentSampleCountField,FloatToStr(FMCurrentSampleCount));
end;

procedure TTTSToolStats.SetRotationSampleCount(const Value: Integer);
begin
  FRotationSampleCount := Value;
  Host.SetFieldData(Index,RotationSampleCountField,FloatToStr(FRotationSampleCount));
end;

procedure TTTSToolStats.SetElectrodeFlowSum(const Value: Double);
begin
  FElectrodeFlowSum := Value;
  Host.SetFieldData(Index,ElectrodeFlowSumField,FloatToStr(FElectrodeFlowSum));
end;

procedure TTTSToolStats.SetMCurrentSigma(const Value: Double);
begin
  FMCurrentSigma := Value;
  Host.SetFieldData(Index,MCurrentSigmaField,FloatToStr(FMCurrentSigma));
end;

procedure TTTSToolStats.SetRotationSigma(const Value: Double);
begin
  FRotationSigma := Value;
  Host.SetFieldData(Index,RotationSigmaField,FloatToStr(FRotationSigma));
end;

procedure TTTSToolStats.SetElectrodeFlowSigma(const Value: Double);
begin
  FElectrodeFlowSigma := Value;
  Host.SetFieldData(Index,EFlowSigmaField,FloatToStr(FElectrodeFlowSigma));
end;

procedure TTTSToolStats.SetNoseGuideFlowSumSquares(const Value: Double);
begin
  FNoseGuideFlowSumSquares := Value;
  Host.SetFieldData(Index,NoseGuideFlowSumSquaresField,FloatToStr(FNoseGuideFlowSumSquares));
end;

procedure TTTSToolStats.SetElectrodeFlowSamples(const Value: Integer);
begin
  FElectrodeFlowSamples := Value;
  Host.SetFieldData(Index,ElectrodeFlowSampleCountField,IntToStr(FElectrodeFlowSamples));
end;

procedure TTTSToolStats.SetMCurrentSumSquares(const Value: Double);
begin
  FMCurrentSumSquares := Value;
  Host.SetFieldData(Index,MCurrentSumSquaresField,FloatToStr(FMCurrentSumSquares));
end;

procedure TTTSToolStats.SetRotationSumSquares(const Value: Double);
begin
  FRotationSumSquares := Value;
  Host.SetFieldData(Index,RotationSumSquaresField,FloatToStr(FRotationSumSquares));
end;

procedure TTTSToolStats.SetElectrodeFlowSumSquares(const Value: Double);
begin
  FElectrodeFlowSumSquares := Value;
  Host.SetFieldData(Index,ElectrodeFlowSumSquaresField,FloatToStr(FElectrodeFlowSumSquares));
end;

constructor TTTSToolStats.Create(AHost: TToolStatsList; AIndex: Integer);
begin
  inherited Create;
  Index := AIndex;
  Host := AHost;
end;

destructor TTTSToolStats.Destroy;
begin

  inherited;
end;

procedure TTTSToolStats.RebuildInternalValues(AIndex: Integer);
begin
  Index := AIndex;
  FToolType  := Host.GetFieldString(Index, ToolTypeField);               // 1
  FNoseGuideFlowSum  := Host.GetFieldDouble(Index,NoseguideFlowSumField);
  FNoseGuideFlowSampleCount  := Host.GetFieldInteger(Index,NoseGuideFlowSampleCountField);
  FMCurrentSum  := Host.GetFieldDouble(Index,MCurrentSumField);
  FRotationSum  := Host.GetFieldDouble(Index,RotationSumField);
  FNoseGuideFlowSigma  := Host.GetFieldDouble(Index,NGFlowSigmaField);
  FMCurrentSampleCount:= Host.GetFieldInteger(Index,MCurrentSampleCountField);
  FRotationSampleCount:= Host.GetFieldInteger(Index,RotationSampleCountField);
  FElectrodeFlowSum:= Host.GetFieldDouble(Index,ElectrodeFlowSumField);
  FMCurrentSigma:= Host.GetFieldDouble(Index,MCurrentSigmaField);
  FRotationSigma:= Host.GetFieldDouble(Index,RotationSigmaField);
  FElectrodeFlowSigma:= Host.GetFieldDouble(Index,EFlowSigmaField);
  FNoseGuideFlowSumSquares:= Host.GetFieldDouble(Index,NoseGuideFlowSumSquaresField);
  FElectrodeFlowSamples:= Host.GetFieldInteger(Index,ElectrodeFlowSampleCountField);
  FMCurrentSumSquares:= Host.GetFieldDouble(Index,MCurrentSumSquaresField);
  FRotationSumSquares:= Host.GetFieldDouble(Index,RotationSumSquaresField);
  FElectrodeFlowSumSquares:= Host.GetFieldDouble(Index,ElectrodeFlowSumSquaresField);

end;

{ TToolStatsList }


constructor TToolStatsList.Create(TableName: string);
begin
  if StringArraySet.CreateArray(TableName, Self, ITable) <> AAR_OK then begin
    StringArraySet.UseArray(TableName, Self, ITable);
    if ITable.FieldCount <> StatsFieldCount then begin
      CleanTable;
      CreateTable;
    end;
  end else begin
    CreateTable;
  end;
  ResultStatsList := TList.Create;
  RebuildInternalValues;
end;

constructor TToolStatsList.Create;
begin
  inherited Create;
  CreateWithName('ResultStatsByType', True);
end;

procedure TToolStatsList.CreateTable;
begin
  BeginUpdate;
  try
  ITable.Clear;
  ITable.AddField(ToolTypeField);
  ITable.AddField(ElectrodeFlowSampleCountField);
  ITable.AddField(ElectrodeFlowSumField);
  ITable.AddField(ElectrodeFlowSumSquaresField);
  ITable.AddField(EFlowSigmaField);
  ITable.AddField(NoseGuideFlowSampleCountField);
  ITable.AddField(NoseGuideFlowSumField);
  ITable.AddField(NoseGuideFlowSumSquaresField);
  ITable.AddField(NGFlowSigmaField);
  ITable.AddField(RotationSampleCountField);
  ITable.AddField(RotationSumField );
  ITable.AddField(RotationSumSquaresField);
  ITable.AddField(RotationSigmaField );
  ITable.AddField(MCurrentSampleCountField);
  ITable.AddField(MCurrentSumField);
  ITable.AddField(MCurrentSumSquaresField);
  ITable.AddField(MCurrentSigmaField);
  finally
    EndUpdate;
  end;
end;

procedure TToolStatsList.RebuildInternalValues;
var I : Integer;
    Tmp : TTTSToolStats;
begin
  for I := ITable.RecordCount to ResultStatsList.Count - 1 do begin
    StatsAtIndex[0].Free;
    ResultStatsList.Delete(0);
  end;

  for I := 0 to ITable.RecordCount - 1 do begin
    if I >= ResultStatsList.Count then begin
      Tmp := TTTSToolStats.Create(Self, I);
      ResultStatsList.Add(Tmp);
      Tmp.RebuildInternalValues(I);
    end else begin
      StatsAtIndex[I].RebuildInternalValues(I);
    end;
  end;
end;

procedure TToolStatsList.FlushData(Table: IACNC32StringArray);
var
Rcount : Integer;
FName : String;
begin
If assigned(Table) then
  begin
  FName := DllLocalPath+'DB\'+Table.Alias;
  RCount := Table.RecordCount;
  if Rcount > 0 then
    begin
    Table.ExportToCSV(FName)
    end;
  end;
end;

procedure TToolStatsList.Clean;
begin
  inherited;

end;

procedure TToolStatsList.CleanTable;
var
F : Widestring;
begin
  BeginUpdate;
  try
    while ITable.FieldCount > 0 do begin
      ITable.FieldNameAtIndex(0, F);
      ITable.DeleteField(F);
    end;
  finally
    EndUpdate;
  end;
end;

function TToolStatsList.Count: Integer;
begin
Result := ResultStatsList.Count;
end;

procedure TToolStatsList.FlushDataToDisk;
begin
if assigned(ITable) then
  begin
  FlushData(ITable);
  end;
end;

procedure TToolStatsList.RebuildIndex;
begin

end;

procedure TToolStatsList.RebuildSingleValue(Row, Col: Integer);
begin
  inherited;
  StatsAtIndex[Row].RebuildInternalValues(Row);
end;


destructor TToolStatsList.Destroy;
begin
  ITable := Nil;
  inherited;
end;

procedure TToolStatsList.CreateWithName(TableName: string; Clean: Boolean);
begin
 if StringArraySet.CreateArray(TableName, Self, ITable) <> AAR_OK then begin
    StringArraySet.UseArray(TableName, Self, ITable);
    if ITable.FieldCount <> StatsFieldCount then begin
      CleanTable;
      CreateTable;
    end;
  end else begin
    CreateTable;
  end;
  ResultStatsList := TList.Create;
  RebuildInternalValues;
end;

function TToolStatsList.GetStats(index: Integer): TTTSToolStats;
begin
  Result := nil;
  if Index < ResultStatsList.count then
    begin
    Result := TTTSToolStats(ResultStatsList[Index]);
    end

end;

function TToolStatsList.FindStatsForTool(ToolType: string): TTTSToolStats;
var
I : Integer;
begin
  Result := nil;
  if Count > 0 then
  begin
  Lock;
  try
    for I := 0 to Count - 1 do begin
      if Uppercase(StatsAtIndex[I].ToolType) = UpperCase(ToolType) then begin
        Result := StatsAtIndex[I];
        Exit;
      end;
    end;
  finally
  Unlock;
  end;
  end;

end;


function TToolStatsList.ProcessResult(TestResult: TTTSToolResult;AllPassed : Boolean;UseSigmas : Boolean): TTTSToolStats;
var
CurrentStats : TTTSToolStats;
begin
Result := Nil;
// First Establish if this is new tool type
If TestResult.ToolType <> '' then
  begin
  CurrentStats := FindStatsForTool(TestResult.ToolType);
  if not assigned(CurrentStats) then
    begin
    // Add a new Entry  for the current Result
    Result := AddStatsRecordFromResult(TestResult,UseSigmas);
    end
  else
    begin
    // Add a new Entry  for the current Result
    Result := UpdateStatsRecordFromResult(TestResult,AllPassed,UseSigmas);
    end
  end
else
  begin
  end;
FlushDataToDisk;
end;

function TToolStatsList.UpdateStatsRecordFromResult(var TestResult: TTTSToolResult;UpdateSigmas : Boolean;UseSigmas : Boolean): TTTSToolStats;
var
SigmaVariance : Double;
begin
//TestResults enter with status of 1 or zero;
//on exit the status is set to 0 = red, 1 = green, 2 = amber
Result := FindStatsForTool(TestResult.ToolType);
if assigned(Result) then
with Result do
  begin
  if TestResult.NoseGuideFlowStatus > 0 then
    begin
    if UpdateSigmas and not TestResult.TestWasAbandoned then
      begin
      NoseGuideFlowSum := NoseGuideFlowSum + TestResult.NoseGuideFlow;
      NoseGuideFlowSumSquares := NoseGuideFlowSumSquares+(TestResult.NoseGuideFlow*TestResult.NoseGuideFlow);
      NoseGuideFlowSampleCount := NoseGuideFlowSampleCount+1;
      NoseGuideFlowSigma := CalculateSigma(NoseGuideFlowSum,NoseGuideFlowSumSquares,NoseGuideFlowSampleCount,TestResult.NoseGuideFlow,SigmaVariance);
      //Check For rag status
      if ((SigmaVariance > 3.0) and (NoseGuideFlowSampleCount > 15) and UseSigmas) then
         TestResult.NoseGuideFlowStatus := 2; // set to Amber only if was Green
      end;
    end;

  if TestResult.ElectrodeFlowStatus > 0 then
    begin
    if UpdateSigmas and not TestResult.TestWasAbandoned then
      begin
      ElectrodeFlowSum := ElectrodeFlowSum+ TestResult.ElectrodeFlow;
      ElectrodeFlowSumSquares := ElectrodeFlowSumSquares+(TestResult.ElectrodeFlow*TestResult.ElectrodeFlow);
      ElectrodeFlowSampleCount := ElectrodeFlowSampleCount+1;
      ElectrodeFlowSigma := CalculateSigma(ElectrodeFlowSum,ElectrodeFlowSumSquares,ElectrodeFlowSampleCount,TestResult.ElectrodeFlow,SigmaVariance);
      if ((SigmaVariance > 3.0) and (ElectrodeFlowSampleCount > 15) and UseSigmas) then
        TestResult.ElectrodeFlowStatus := 2; // set to Amber only if was Green
      end;

  end;

  if TestResult.MCurrentStatus > 0 then
    begin
    if UpdateSigmas and not TestResult.TestWasAbandoned then
      begin
      MCurrentSum := MCurrentSum+ TestResult.MCurrent;
      MCurrentSumSquares := MCurrentSumSquares+(TestResult.MCurrent*TestResult.MCurrent);
      MCurrentSampleCount := MCurrentSampleCount+1;
      MCurrentSigma := CalculateSigma(MCurrentSum,MCurrentSumSquares,MCurrentSampleCount,TestResult.MCurrent,SigmaVariance);
      if ((SigmaVariance > 3.0) and (MCurrentSampleCount > 15) and UseSigmas) then
          TestResult.MCurrentStatus := 2; // set to Amber only if was Green
      end;
  end;

  if TestResult.RotationStatus > 0 then
    begin
    if UpdateSigmas and not TestResult.TestWasAbandoned then
      begin
      RotationSum := RotationSum+ TestResult.Rotation;
      RotationSumSquares := RotationSumSquares+(TestResult.Rotation*TestResult.Rotation);
      RotationSampleCount := RotationSampleCount+1;
      RotationSigma := CalculateSigma(RotationSum,RotationSumSquares,RotationSampleCount,TestResult.Rotation,SigmaVariance);
      if ((SigmaVariance > 3.0) and (RotationSampleCount > 15) and UseSigmas) then
        TestResult.RotationStatus := 2; // set to Amber only if was Green
      end;
  end;

  end; // with result
end;

function TToolStatsList.AddStatsRecordFromResult(TestResult: TTTSToolResult;UseSigmas : Boolean): TTTSToolStats;
begin
Result := AddStatsEntry;
Result.ToolType :=TestResult.ToolType;
if TestResult.NoseGuideFlowStatus > 0 then
  begin
  Result.NoseGuideFlowSum := TestResult.NoseGuideFlow;
  Result.NoseGuideFlowSumSquares := TestResult.NoseGuideFlow*TestResult.NoseGuideFlow;
  Result.NoseGuideFlowSampleCount := 1;
  end
else
  begin
  Result.NoseGuideFlowSum := 0;
  Result.NoseGuideFlowSumSquares := 0;
  Result.NoseGuideFlowSampleCount := 0;
  end;
Result.NoseGuideFlowSigma := 0;

if TestResult.ElectrodeFlowStatus > 0 then
  begin
  Result.ElectrodeFlowSum := TestResult.ElectrodeFlow;
  Result.ElectrodeFlowSumSquares := TestResult.ElectrodeFlow*TestResult.ElectrodeFlow;
  Result.ElectrodeFlowSampleCount := 1;
  end
else
  begin
  Result.ElectrodeFlowSum := 0;
  Result.ElectrodeFlowSumSquares := 0;
  Result.ElectrodeFlowSampleCount := 0;
  end;
Result.ElectrodeFlowSigma := 0;

if TestResult.MCurrentStatus > 0 then
  begin
  Result.MCurrentSum := TestResult.MCurrent;
  Result.MCurrentSumSquares := TestResult.MCurrent*TestResult.MCurrent;
  Result.MCurrentSampleCount := 1;
  end
else
  begin
  Result.MCurrentSum := 0;
  Result.MCurrentSumSquares := 0;
  Result.MCurrentSampleCount := 0;
  end;
Result.MCurrentSigma := 0;

if TestResult.RotationStatus > 0 then
  begin
  Result.RotationSum     := TestResult.Rotation;
  Result.RotationSampleCount := 1;
  Result.RotationSumSquares := TestResult.Rotation * TestResult.Rotation;
  end
else
  begin
  Result.RotationSum     := 0;
  Result.RotationSampleCount := 0;
  Result.RotationSumSquares := 0;
  end;
end;

function TToolStatsList.AddStatsEntry: TTTSToolStats;
begin
  Lock;
  try
    Result := TTTSToolStats.Create(Self, -1);
    Result.Index := ITable.RecordCount;
    ResultStatsList.Add(Result);
    ITable.Append(1);
  finally
    Unlock;
  end;
end;


function TToolStatsList.CalculateSigma(Sum, SumSquares: Double;
                                       Samples: Integer;
                                       ResultValue : Double;
                                       var SigmaVar : Double): Double;
var
SigmaSquared : Double;
Multiplier : Double;
SumSquredOverN : Double;
Mean,Variance : Double;
begin
if Samples > 1 then
  begin
  Multiplier := 1/(Samples-1);
  SumSquredOverN := (Sum*Sum)/Samples;
  SigmaSquared := abs(Multiplier*(SumSquares-SumSquredOverN));

  Result := Sqrt(SigmaSquared);
  Mean := Sum/Samples;
  Variance := abs(Mean-ResultValue);
  if Result > 0.0000001 then
    SigmaVar := Variance/Result
  else
    SigmaVar := 0;

  end
else
  begin
  Result := 0;
  SigmaVar := 0;
  end
end;


end.
