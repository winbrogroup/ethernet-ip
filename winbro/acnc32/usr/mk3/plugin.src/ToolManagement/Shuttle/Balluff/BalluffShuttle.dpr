library BalluffShuttle;



{%File 'BalluffShuttle.dll.html'}

uses
  SysUtils,
  Classes,
  ToolForm in 'ToolForm.pas' {ToolManager},
  C6x1 in 'C6x1.pas',
  PartForm in 'PartForm.pas' {PartManager},
  OpTable in '..\..\lib\OpTable.pas',
  ToolOKDlg in 'ToolOKDlg.pas' {IsToolOK},
  ManualPartForm in 'ManualPartForm.pas' {ManualPart},
  AllToolDB in '..\..\lib\AllToolDB.pas',
  ToolRegistration in 'ToolRegistration.pas' {RegForm},
  OffsetData in '..\..\lib\OffsetData.pas',
  NumericOperatorDialogue in 'NumericOperatorDialogue.pas' {NumericEntry},
  ToolMonitor in 'ToolMonitor.pas';

{$R *.res}

begin
end.
