object PartManager: TPartManager
  Left = 242
  Top = 117
  BorderIcons = []
  BorderStyle = bsSingle
  Caption = 'Part Status'
  ClientHeight = 673
  ClientWidth = 642
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -15
  Font.Name = 'Verdana'
  Font.Style = [fsBold]
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 18
  object PaintBox: TPaintBox
    Left = 0
    Top = 281
    Width = 642
    Height = 347
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    OnPaint = PaintBoxPaint
  end
  object Panel1: TPanel
    Left = 0
    Top = 628
    Width = 642
    Height = 45
    Align = alBottom
    AutoSize = True
    BevelOuter = bvLowered
    BevelWidth = 2
    TabOrder = 0
    object TouchSoftkey1: TTouchSoftkey
      Left = 2
      Top = 2
      Width = 638
      Height = 41
      Align = alClient
      Caption = 'TouchSoftkey1'
      TabOrder = 0
      OnClick = TouchSoftkey1Click
      Legend = 'Close'
      TextSize = 14
      HasIndicator = False
      IndicatorOn = False
      IsEnabled = True
      ButtonStyle = skNormal
      ArrowColor = clBlack
      ButtonColor = clWhite
      LegendColor = clBlack
      IndicatorWidth = 20
      IndicatorHeight = 10
      IndicatorOnColor = clGreen
      IndicatorOffColor = clSilver
      MaxFontSize = 0
    end
  end
  object ValueListEditor: TValueListEditor
    Left = 0
    Top = 0
    Width = 642
    Height = 281
    Align = alTop
    DefaultDrawing = False
    DefaultRowHeight = 20
    FixedCols = 1
    Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goColSizing, goAlwaysShowEditor, goThumbTracking]
    Strings.Strings = (
      'Part Name='
      'Part ID='
      'Part SN='
      'Shuttle ID='
      'Shuttle #='
      'Start Time='
      'End Time='
      'Probe Time='
      'Production='
      'Development='
      'Inspection Reqd. (Failed OP)=')
    TabOrder = 1
    TitleCaptions.Strings = (
      'Item'
      'Value')
    OnDrawCell = ValueListEditorDrawCell
    ColWidths = (
      254
      382)
  end
end
