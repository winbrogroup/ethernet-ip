unit PartForm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Grids, ExtCtrls, ACNC_TouchSoftkey, ComCtrls, Commctrl, C6x1, PartData,
  IniFiles, OCTUtils, NamedPlugin, INamedInterface, AbstractFormPlugin,
  ValEdit, OpTable, OPPHeader, CncTypes, OctTypes, DateUtils, OffsetData, CellMode;

type
  TBalluffPartTag = (
    bptBalluffWrite,
    bptPartRemove,
    bptPartRemoveAck,
    bptBalluffWriteString,
    bptInspectionRequired,
    bptPartID,
    bptPartName,
    bptPartSN,
    bptManualLoad,
    bptManualLoadAck,
    btpPartOperationIndex
  );

  TPartManager = class(TC6x1Client)
    Panel1: TPanel;
    PaintBox: TPaintBox;
    TouchSoftkey1: TTouchSoftkey;
    ValueListEditor: TValueListEditor;
    procedure PaintBoxPaint(Sender: TObject);
    procedure TouchSoftkey1Click(Sender: TObject);
    procedure ValueListEditorDrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
  private
    PartData : TPartData;
    OCTOpData : TOpTable;
    Device : string;
    Head : Boolean;
    TagNames : array [TBalluffPartTag] of string;
    InputBuffer : string;
    PathToPartData : string;
    ManualLoadActive : Boolean;
    SavePartData : Boolean; // Flag determines when to save PartData.csv
    AltoData: TOffsetList;
    AltoBuffer: array [0..2048] of Byte;
    NewAltoData: Boolean;
    procedure DrawRectangle( L, T, W, H : Double;
                             IsRequired, ThisOCT, Done, Active : Boolean);
    procedure LoadConfig;
    procedure Saveconfig;
    procedure PartTableChange(Sender : TObject);
    procedure RebuildPartData;
    procedure RebuildOutputString;
    procedure TagChange(ATag: TTagRef);
    procedure UpdateDisplay;
    procedure BuildManualPart(const PartName : string);
    procedure BuildAltoData;
  public
    Tags : array [TBalluffPartTag] of TTagRef;
    procedure C6x1Notify(Service: TC6x1; Msg : TIBBState); override;
    procedure C6x1AltoNotify(Service: TC6x1; const Msg: array of Char); override;
    procedure Install; override;
    procedure FinalInstall; override;
    procedure Shutdown; override;
    procedure DisplaySweep; override;
    procedure Execute; override;
  end;

var
  PartManager: TPartManager;

implementation

{$R *.DFM}

uses ManualPartForm;

const
  TagDefinitions : array [TBalluffPartTag] of TTagDefinitions = (
     ( Ini : 'OCT_PartWrite'; Tag : 'OCT_PartWrite'; Owned : True; TagType : 'TMethodTag' ),
     ( Ini : 'OCT_PartRemove'; Tag : 'OCT_PartRemove'; Owned : False; TagType : 'TMethodTag' ),
     ( Ini : 'OCT_PartRemoveAck'; Tag : 'OCT_PartRemoveAck'; Owned : True; TagType : 'TMethodTag' ),
     ( Ini : 'BalluffWriteString'; Tag : 'BalluffWriteString'; Owned : False; TagType : 'TStringTag' ),
     ( Ini : 'PartInspectionRequired'; Tag: 'PartInspectionRequired'; Owned: False; TagType: 'TMethodTag' ),
     ( Ini : 'PartID'; Tag: 'PartID'; Owned: True; TagType: 'TStringTag' ),
     ( Ini : 'PartName'; Tag: 'PartName'; Owned: True; TagType: 'TStringTag' ),
     ( Ini : 'PartSN'; Tag: 'PartSN'; Owned: True; TagType: 'TIntegerTag' ),
     ( Ini : 'PartManualLoad'; Tag: 'PartManualLoad'; Owned: False; TagType: 'TIntegerTag' ),
     ( Ini : 'PartManualLoadAck'; Tag: 'PartManualLoadAck'; Owned: True; TagType: 'TIntegerTag' ),
     ( Ini : 'PartOperationIndex'; Tag: 'PartOperationIndex'; Owned: True; TagType: 'TIntegerTag' )
  );

  TenColumns = 13;
  ObjectName = 'BalluffShuttle.PartForm';

  AltoBaseAddress = 400;

procedure PartTagChange(ATag: TTagRef); stdcall;
begin
  PartManager.TagChange(ATag);
end;

procedure TPartManager.Execute;
begin
  if not Visible then
    ShowModal;
end;

procedure TPartManager.DrawRectangle( L, T, W, H : Double;
                                IsRequired, ThisOCT, Done, Active : Boolean);
begin
  H := T + H;
  W := L + W;

  with PaintBox.Canvas do begin
    Pen.Color := clBtnFace;
    if Active then begin
      Brush.Color := clYellow;
      Rectangle(Round(L), Round(T), Round(W), Round(H));
    end;
    if IsRequired then
      Brush.Color := clGreen
    else
      Brush.Color := clGray;

    if ThisOCT then begin
      if IsRequired then
        Brush.Color := clLime
      else
        Brush.Color := clRed;
    end;

    Rectangle(Round(L) + 4, Round(T) + 4, Round(W) - 4, Round(H) - 4);

    if Done then begin

      Brush.Color := clWhite;
      Rectangle(Round(L) + 8, Round(T) + 8, Round(W) - 8, Round(H) - 8);
    end;
  end;
end;

function BitField(const Data : array of Byte; Index : Integer) : Boolean;
var N, R : Integer;
begin
  N := Index div 8;
  R := Index mod 8;
  Result := (Data[N] and (1 shl R)) <> 0;
end;

procedure TPartManager.PaintBoxPaint(Sender: TObject);
var W, H, Xmargin, Ymargin,
    GridHeight, GridWidth,
    UnitHeight, UnitWidth : Double;
    x, y : Integer;
    Linear : Integer;
begin
  W := PaintBox.Width;
  H := PaintBox.Height;
  Xmargin := W / 10;
  YMargin := 20;
  GridHeight := H - (H / 4 + Ymargin);
  GridWidth := W - (Xmargin * 2);
  UnitHeight := GridHeight / 10;
  UnitWidth := GridWidth / TenColumns;

  with PaintBox.Canvas do begin
    Brush.Style := bsClear;
    for y := 0 to 9 do begin
      TextOut (Round(Xmargin - 20),
               Round(Ymargin  + y * UnitHeight), IntToStr(y));
    end;

    for x := 0 to TenColumns - 1 do begin
      TextOut (Round(Xmargin + x * UnitWidth +
         ( (UnitWidth - TextExtent(IntToStr(x * 10)).cx)) / 2),
            Round(Ymargin - 12), IntToStr(X * 10));
    end;

    for x := 0 to TenColumns - 1 do begin
      for y := 0 to 9 do begin
        Linear := X * 10 + Y;
        if (Linear <> 0) and (Linear < 129) then begin

          DrawRectangle( Xmargin + x * UnitWidth,
                         Ymargin + y * UnitHeight,
                         UnitWidth, UnitHeight,
                         PartData.ToDo[Linear],
                         OCTOpData[Linear],
                         PartData.Done[Linear],
                         Linear = OPPHeaderConsumer.OPPFile.OpNumber);
        end;
      end;
    end;


    Brush.Style := bsClear;
    x := 10;
    TextOut(Round(x + UnitWidth), Round(Ymargin + GridHeight + UnitHeight * 0 + 2), 'Required OP');
    TextOut(Round(x + UnitWidth), Round(Ymargin + GridHeight + UnitHeight * 1 + 2), 'Required in this OCT');
    TextOut(Round(x + UnitWidth), Round(Ymargin + GridHeight + UnitHeight * 2 + 2), 'Not required OP');

    DrawRectangle (x, Ymargin + GridHeight + UnitHeight * 0, UnitWidth, UnitHeight, True, False, False, False);
    DrawRectangle (x, Ymargin + GridHeight + UnitHeight * 1, UnitWidth, UnitHeight, True, True, False, False);
    DrawRectangle (x, Ymargin + GridHeight + UnitHeight * 2, UnitWidth, UnitHeight, False, False, False, False);

    Brush.Style := bsClear;
    x := Round(W / 2);
    TextOut(Round(x + UnitWidth), Round(Ymargin + GridHeight + UnitHeight * 0 + 2), 'Completed OP');
    TextOut(Round(x + UnitWidth), Round(Ymargin + GridHeight + UnitHeight * 1 + 2), 'Active OP');
    TextOut(Round(x + UnitWidth), Round(Ymargin + GridHeight + UnitHeight * 2 + 2), 'In OCT, but not required');

    DrawRectangle (x, Ymargin + GridHeight + UnitHeight * 0, UnitWidth, UnitHeight, True, True, True, False);
    DrawRectangle (x, Ymargin + GridHeight + UnitHeight * 1, UnitWidth, UnitHeight, True, True, False, True);
    DrawRectangle (x, Ymargin + GridHeight + UnitHeight * 2, UnitWidth, UnitHeight, False, True, False, False);
  end;
end;

procedure TPartManager.C6x1Notify(Service: TC6x1; Msg: TIBBState);
begin
  if (CompareText(Service.DeviceName, Device) = 0) and
     (Service.Head = Head) then begin
    if Msg = IBB_READ_COMPLETE then begin
      InputBuffer := Service.ReadBuffer;
    end;
  end;
end;

procedure TPartManager.C6x1AltoNotify(Service: TC6x1; const Msg: array of Char);
begin
  if (CompareText(Service.DeviceName, Device) = 0) and
     (Service.Head = Head) then begin

    CopyMemory(@AltoBuffer[0], @Msg[0], 2048);
    NewAltoData := True;
  end;
end;

procedure TPartManager.FinalInstall;
var I : TBalluffPartTag;
begin
  for I := Low(TBalluffPartTag) to High(TBalluffPartTag) do begin
    if not TagDefinitions[I].Owned then
      Tags[I] := Named.AquireTag(PChar(TagNames[I]), PChar(TagDefinitions[I].TagType), PartTagChange);
  end;
  PartTableChange(PartData);
end;

procedure TPartManager.Install;
var I : TBalluffPartTag;
begin
  PartManager := Self;

  PartData := TPartData.Create;
  PartData.OnChange := PartTableChange;
  PartData.OnOpToDoChange := PartTableChange;
  PartData.OnOpDoneChange := PartTableChange;

  AltoData := TOffsetList.Create('Alto', 100);

  OCTOpData := TOpTable.Create(OCTOpsName, OPTableSize);
  LoadConfig;

  for I := Low(TBalluffPartTag) to High(TBalluffPartTag) do begin
    if TagDefinitions[I].Owned then
      Tags[I] := Named.AddTag(PChar(TagNames[I]), PChar(TagDefinitions[I].TagType));
  end;
end;


procedure TPartManager.Shutdown;
begin
  SaveConfig;
  PartData.Close;
  PartData := nil;

  OctOpData.Close;
  OctOpData := nil;

  AltoData.Close;
  AltoData := nil;
end;

procedure TPartManager.LoadConfig;
var Ini : TIniFile;
    I : TBalluffPartTag;
begin
  Ini := TIniFile.Create(dllProfilePath + 'BalluffShuttle.cfg');
  try
    Device := Ini.ReadString('PartForm', 'PartBalluffDevice', 'Balluff_2');
    Head := Ini.ReadBool('PartForm', 'PartBalluffHead', False);
    PathToPartData := Ini.ReadString('PartForm', 'PathToPartData', DLLLocalPath);
    SavePartData := Ini.ReadBool('PartForm', 'SavePartData', False);
    PathToPartData := SysUtils.IncludeTrailingPathDelimiter(PathToPartData);

    for I := Low(TBalluffPartTag) to High(TBalluffPartTag) do begin
      TagNames[I] := Ini.ReadString('PartFormTags', TagDefinitions[I].Ini, TagDefinitions[I].Tag);
    end;
  finally
    Ini.Free;
  end;
end;

procedure TPartManager.Saveconfig;
var Ini : TIniFile;
    I :TBalluffPartTag;
begin
  Ini := TIniFile.Create(dllProfilePath + 'BalluffShuttle.cfg');
  try
    Ini.WriteString('PartForm', 'PartBalluffDevice', Device);
    Ini.WriteBool('PartForm', 'PartBalluffHead', Head);
    Ini.WriteBool('PartForm', 'SavePartData', SavePartData);
    Ini.WriteString('PartForm', 'PathToPartData', PathToPartData);

    for I := Low(TBalluffPartTag) to High(TBalluffPartTag) do begin
      Ini.WriteString('PartFormTags', TagDefinitions[I].Ini, TagNames[I]);
    end;
  finally
    Ini.Free;
  end;
end;

procedure TPartManager.PartTableChange(Sender: TObject);
begin
  RebuildOutputString;
  UpdateDisplay;
end;

type
  IBMix = record
    case Byte of
      0: ( B: array [0..3] of Byte );
      1: ( I: Integer );
  end;

procedure TPartManager.BuildAltoData;
var I, J, Start, Axis: Integer;
    TmpS: string;
    Tmp: IBMix;

    S: TStringList;
begin

  S := TStringList.Create;
  try
    for I := 0 to 79 do begin
      Tmps := IntToStr(I) + ':';
      for J := 0 to 14 do begin
        Tmps := Tmps + ' ' + IntToHex(AltoBuffer[AltoBaseAddress + (I * 15) + J], 2);
      end;
      S.Add(Tmps);
    end;
    S.SaveToFile('c:\amchem\balluff_dump.txt');
  finally
    S.Free;
  end;

  AltoData.BeginUpdate;
  try
    //Named.EventLog('Updating table');
    for I := 0 to 79 do begin
      for Axis:= 0 to 4 do begin
        Start:= AltoBaseAddress + (I * 15) + (Axis * 3);

        Tmp.B[2] := Self.AltoBuffer[Start];
        Tmp.B[1] := Self.AltoBuffer[Start + 1];
        Tmp.B[0] := Self.AltoBuffer[Start + 2];
        if (Tmp.B[2] and $80) <> 0 then
          Tmp.B[3] := $ff
        else
          Tmp.B[3] := 0;

        AltoData.Offset[I].Axis[Axis] := Tmp.I / 1000;
      end;
      AltoData.Offset[I].UpdateRow;
    end;
  finally
    AltoData.EndUpdate;
  end;
end;

procedure TPartManager.BuildManualPart(const PartName: string);
var I : Integer;
begin
  PartData.BeginUpdate;
  try
    PartData.Clear;
    if FileExists(PathToPartData + PartName + '.csv') then begin
      PartData.Import(PathToPartData + PartName + '.csv');
      PartData.DoneTable.Import(PathToPartData + PartName + 'Done.csv');
      for I := 0 to 128 do begin
        PartData.ToDo[I] := True;
      end;
    end else begin
      PartData.PartID := PartName;
      PartData.PartSN := 1;
      PartData.PartName := PartName;
      PartData.StatusFlags := [psfDataValid, psfProduction, psfDevelopment];
      PartData.ToDoTable.BeginUpdate;
      PartData.DoneTable.BeginUpdate;
      try
        for I := 0 to 128 do begin
          PartData.ToDo[I] := True;
          PartData.Done[I] := False;
        end;
      finally
        PartData.ToDoTable.EndUpdate;
        PartData.DoneTable.EndUpdate;
      end;
    end;
  finally
    PartData.EndUpdate;
  end;
  RebuildOutputString;
end;

procedure TPartManager.DisplaySweep;
begin
  if InputBuffer <> '' then begin
    RebuildPartData;
    RebuildOutputString;
    InputBuffer := '';
  end;

  if NewAltoData then begin
    BuildAltoData;
    NewAltoData := False;
  end;


  if Named.GetAsBoolean(Tags[bptManualLoad]) then begin
    if not ManualLoadActive then begin
      ManualLoadActive := True;
      BuildManualPart(TManualPart.Execute);
      Named.SetAsBoolean(Tags[bptManualLoadAck], True);
    end;
  end else if Named.GetAsBoolean(Tags[bptManualLoadAck]) then begin
    Named.SetAsBoolean(Tags[bptManualLoadAck], False);
    ManualLoadActive := False;
  end;

  if Named.GetAsBoolean(Tags[bptPartRemove]) then begin
    if not Named.GetAsBoolean(Tags[bptPartRemoveAck]) then begin
      if (PartData.PartID <> '') and SavePartData then begin
        PartData.Export(PathToPartData + PartData.PartID + '.csv');
        PartData.DoneTable.Export(PathToPartData + PartData.PartID + 'Done.csv');
      end;

      PartData.PartID := '';
      PartData.PartSN := 0;
      PartData.PartName := '';
      RebuildOutputString;
      UpdateDisplay;
      Named.SetAsBoolean(Tags[bptPartRemoveAck], True);
    end;
  end else if Named.GetAsBoolean(Tags[bptPartRemoveAck]) then begin
    Named.SetAsBoolean(Tags[bptPartRemoveAck], False);
  end;
end;


procedure TPartManager.RebuildOutputString;
  function ByteToBool(Tmp : Byte) : string;
  var I : Integer;
  begin
    Result := '';
    for I := 0 to 7 do begin
      if (Tmp and ($80 shr I)) <> 0 then
        Result := Result + '1'
      else
        Result := Result + '0';
    end;
  end;

  function FixedWidth(const Tmp : string; Width : Integer) : string;
  const Space = '               ';
  begin
    Result := Tmp + Copy(Space, 1, Width - Length(Tmp)) + ',';
  end;

  function FixedNWidth(const Tmp : string; Width : Integer) : string;
  const Space = '                     ';
  begin
    Result := Copy(Space, 1, Width - Length(Tmp)) + Tmp + ',';
  end;

  function OpString(Op : TOpTable) : string;
  var I : Integer;
      Tmp : Integer;
  begin
    Result := '';
    for I := 0 to 31 do begin
      Tmp := 0;
      if Op.Checked[I * 4 + 1] then
        Tmp := Tmp + 1;
      if Op.Checked[I * 4 + 2] then
        Tmp := Tmp + 2;
      if Op.Checked[I * 4 + 3] then
        Tmp := Tmp + 4;
      if Op.Checked[I * 4 + 4] then
        Tmp := Tmp + 8;

      Result := Result + IntToHex(Tmp, 1);
    end;
  end;

var Tmp : string;

begin
  Tmp := '0101,';
  Tmp := Tmp + FixedWidth(PartData.ShuttleID, 10);
  Tmp := Tmp + FixedNWidth(IntToStr(PartData.ShuttleSN), 4);
  Tmp := Tmp + FixedWidth(PartData.StorageDate(PartData.RebuildTime), 19);
  Tmp := Tmp + FixedWidth(ByteToBool(Byte(PartData.StatusFlags)), 8);
  Tmp := Tmp + FixedWidth(PartData.PartID, 10);
  Tmp := Tmp + FixedNWidth(IntToStr(PartData.PartSN), 4);
  Tmp := Tmp + FixedWidth(PartData.PartName, 14);
  if NC.InchModeDefault then begin
    Tmp := Tmp + FixedNWidth(Format('%.5f', [PartData.ProbeData[0, 0]]), 9);
    Tmp := Tmp + FixedNWidth(Format('%.5f', [PartData.ProbeData[0, 1]]), 9);
    Tmp := Tmp + FixedNWidth(Format('%.5f', [PartData.ProbeData[0, 2]]), 9);
    Tmp := Tmp + FixedNWidth(Format('%.5f', [PartData.ProbeData[1, 0]]), 9);
    Tmp := Tmp + FixedNWidth(Format('%.5f', [PartData.ProbeData[1, 1]]), 9);
    Tmp := Tmp + FixedNWidth(Format('%.5f', [PartData.ProbeData[1, 2]]), 9);
    Tmp := Tmp + FixedNWidth(Format('%.5f', [PartData.ProbeData[2, 0]]), 9);
    Tmp := Tmp + FixedNWidth(Format('%.5f', [PartData.ProbeData[2, 1]]), 9);
    Tmp := Tmp + FixedNWidth(Format('%.5f', [PartData.ProbeData[2, 2]]), 9);
  end else begin
    Tmp := Tmp + FixedNWidth(Format('%.4f', [PartData.ProbeData[0, 0]]), 9);
    Tmp := Tmp + FixedNWidth(Format('%.4f', [PartData.ProbeData[0, 1]]), 9);
    Tmp := Tmp + FixedNWidth(Format('%.4f', [PartData.ProbeData[0, 2]]), 9);
    Tmp := Tmp + FixedNWidth(Format('%.4f', [PartData.ProbeData[1, 0]]), 9);
    Tmp := Tmp + FixedNWidth(Format('%.4f', [PartData.ProbeData[1, 1]]), 9);
    Tmp := Tmp + FixedNWidth(Format('%.4f', [PartData.ProbeData[1, 2]]), 9);
    Tmp := Tmp + FixedNWidth(Format('%.4f', [PartData.ProbeData[2, 0]]), 9);
    Tmp := Tmp + FixedNWidth(Format('%.4f', [PartData.ProbeData[2, 1]]), 9);
    Tmp := Tmp + FixedNWidth(Format('%.4f', [PartData.ProbeData[2, 2]]), 9);
  end;

  Tmp := Tmp + PartData.StorageDate(PartData.ProbeTime) + ',';
  Tmp := Tmp + FixedNWidth(Format('%x', [PartData.ValidLocation]), 4);
  Tmp := Tmp + OpString(PartData.ToDoTable) + ',';
  Tmp := Tmp + OpString(PartData.DoneTable) + ',';
  Tmp := Tmp + PartData.StorageDate(PartData.StartTime) + ',';
  Tmp := Tmp + PartData.StorageDate(PartData.EndTime) + ',';

  Named.SetAsString(Tags[bptBalluffWriteString], PChar(Tmp));

  Tmp := PartData.PartID;
  Named.SetAsString(Tags[bptPartID], PChar(Tmp));
  Tmp := PartData.PartName;
  Named.SetAsString(Tags[bptPartName], PChar(Tmp));
  Named.SetAsInteger(Tags[bptPartSN], PartData.PartSN);
{  InputBuffer := Tmp;
  RebuildPartData; }
end;

procedure TPartManager.RebuildPartData;
var Tmp, OpToDoString, OpDoneString : string;
    ProbeData : TProbeData;

  procedure SetOpData(Table: TOpTable; const Data: string);
  var I, IVal : Integer;
  begin
    if Length(Data) <> 32 then
      raise Exception.Create('Op table length invalid');

    Table.BeginUpdate;
    try
      for I := 1 to Length(Data) do begin
        case Data[I] of
          '0'..'9' : begin
            IVal := Byte(Data[I]) -  Byte('0');
          end;

          'A'..'F' : begin
            IVal := Byte(Data[I]) - Byte('A') + 10;
          end;

          'a'..'f' : begin
            IVal := Byte(Data[I]) - Byte('a') + 10;
          end;
        else
          raise Exception.Create('Invalid character in Operation table');
        end;

        // The warning abount IVal is stupid
        Table[(I - 1) * 4 + 1] := (IVal and 1) <> 0;
        Table[(I - 1) * 4 + 2] := (IVal and 2) <> 0;
        Table[(I - 1) * 4 + 3] := (IVal and 4) <> 0;
        Table[(I - 1) * 4 + 4] := (IVal and 8) <> 0;
      end;
    finally
      Table.EndUpdate;
    end;
  end;

  function BoolToByte(const Bool : string) : Byte;
  var I : Integer;
  begin
    Result := 0;
    for I := 0 to Length(Bool) - 1 do begin
      if Bool[Length(Bool) - I] <> '0' then
        Result := Result + (1 shl I);
    end;
  end;

begin
  PartData.BeginUpdate;
  try
    try
      Tmp := ReadDelimited(InputBuffer);
      if Tmp <> '0101' then
         raise Exception.Create('Version incorrect ' + Tmp);

      PartData.ShuttleID := ReadDelimited(InputBuffer);
      PartData.ShuttleSN := StrToInt(ReadDelimited(InputBuffer));
      PartData.RebuildTime := StorageTimeToDateTime(ReadDelimited(InputBuffer));
      Partdata.StatusFlags := TTagStatusFlags(BoolToByte(ReadDelimited(InputBuffer)));
      PartData.PartID := ReadDelimited(InputBuffer);
      PartData.PartSN := StrToInt(ReadDelimited(InputBuffer));
      PartData.PartName := ReadDelimited(InputBuffer);
      ProbeData[0, 0] := StrToFloat(ReadDelimited(InputBuffer));
      ProbeData[0, 1] := StrToFloat(ReadDelimited(InputBuffer));
      ProbeData[0, 2] := StrToFloat(ReadDelimited(InputBuffer));
      ProbeData[1, 0] := StrToFloat(ReadDelimited(InputBuffer));
      ProbeData[1, 1] := StrToFloat(ReadDelimited(InputBuffer));
      ProbeData[1, 2] := StrToFloat(ReadDelimited(InputBuffer));
      ProbeData[2, 0] := StrToFloat(ReadDelimited(InputBuffer));
      ProbeData[2, 1] := StrToFloat(ReadDelimited(InputBuffer));
      ProbeData[2, 2] := StrToFloat(ReadDelimited(InputBuffer));
      PartData.ProbeData := ProbeData;

      PartData.ProbeTime := StorageTimeToDateTime(ReadDelimited(InputBuffer));
      PartData.ValidLocation := StrToInt('$' + ReadDelimited(InputBuffer));
      OpToDoString := ReadDelimited(InputBuffer);
      OpDoneString := ReadDelimited(InputBuffer);

      PartData.StartTime := StorageTimeToDateTime(ReadDelimited(InputBuffer));
      PartData.EndTime := StorageTimeToDateTime(ReadDelimited(InputBuffer));

      SetOpData(PartData.ToDoTable, OpToDoString);
      SetOpData(PartData.DoneTable, OpDoneString);
    except
      on E: Exception do begin
        Named.SetFaultA(ObjectName, 'Part shuttle tag contains invalid data', faultRewind, nil);
        Named.EventLog('Part Shuttle read failure ' + E.Message);
        PartData.StatusFlags := [];
      end;
    end;
  finally
    if not (psfDataValid in PartData.StatusFlags) then begin
      // Do something sensible here ???????
    end;
    PartData.EndUpdate;
  end;
  UpdateDisplay;
end;

procedure TPartManager.TagChange(ATag: TTagRef);
begin
end;

procedure TPartManager.TouchSoftkey1Click(Sender: TObject);
begin
  ModalResult := mrOK;
end;

{
Part Name
Part Type=
Part ID=
Shuttle ID=
Shuttle #=
Start time=
End time=
Probe time=
** removed Valid Location Map=
Production=
Development=
Inspection Reqd. (Failed OP)=
}
procedure TPartManager.UpdateDisplay;
begin
{
  ValueListEditor.Cells[1, 1] := PartData.PartName;
  ValueListEditor.Cells[1, 2] := PartData.PartID;
  ValueListEditor.Cells[1, 3] := IntToStr(PartData.PartSN);
  ValueListEditor.Cells[1, 4] := PartData.ShuttleID;
  ValueListEditor.Cells[1, 5] := IntToStr(PartData.ShuttleSN);
  ValueListEditor.Cells[1, 6] := FloatToStr(PartData.StartTime);
  ValueListEditor.Cells[1, 7] := FloatToStr(PartData.StartTime);
  ValueListEditor.Cells[1, 8] := FloatToStr(PartData.StartTime);

  ValueListEditor.Cells[1, 9] := IntToHex(PartData.ValidLocation, 4);
  ValueListEditor.Cells[1, 10] := BooleanEnum[psfProduction in PartData.StatusFlags];
  ValueListEditor.Cells[1, 11] := BooleanEnum[psfDevelopment in PartData.StatusFlags];
  ValueListEditor.Cells[1, 12] := BooleanEnum[psfInspectionRequired in PartData.StatusFlags]; }
  PaintBox.Repaint;
  ValueListEditor.Invalidate;
end;

function MakeTimeDateString(Time : TDateTime):String;
var
Year,Month,Day,Hour,Min,Sec,msec : Word;
HStr,MinStr,SEcStr : String;
MonStr : String;
begin
   DecodeDateTime(Time,Year,Month,Day,Hour,Min,Sec,msec);
   if Hour < 10 then HStr := '0'+IntToStr(Hour) else HStr := IntToStr(Hour);
   if Min < 10 then MinStr := '0'+IntToStr(Min) else MinStr := IntToStr(Min);
   if Sec < 10 then SEcStr := '0'+IntToStr(SEc) else SEcStr := IntToStr(SEc);
   case Month of
   1: MonStr := 'Jan';
   2: MonStr := 'Feb';
   3: MonStr := 'Mar';
   4: MonStr := 'Apr';
   5: MonStr := 'May';
   6: MonStr := 'Jun';
   7: MonStr := 'July';
   8: MonStr := 'Aug';
   9: MonStr := 'Sept';
   10: MonStr := 'Oct';
   11: MonStr := 'Nov';
   12: MonStr := 'Dec';
   end;

   Result := Format('%s/%d/%d  %s:%s:%s',[MonStr,Day,Year,HStr,MinStr,SecStr]);
end;

procedure TPartManager.ValueListEditorDrawCell(Sender: TObject; ACol,
  ARow: Integer; Rect: TRect; State: TGridDrawState);

  function TimeFormat(TD : TDateTime) : string;
  begin
    if TD < 1 then begin
      Result := '--/--/--  --:--:--';
    end else begin
      Result := MakeTimeDateString(TD);
    end;
  end;

var TextOut : string;
begin
{    if gdSelected in State then begin
      Canvas.Rectangle(Rect.Left + 2, Rect.Top + 2, Rect.Right - 2, Rect.Bottom - 2);
    end; }
    // gdFocused
  with ValueListEditor do begin
    if gdFixed in State then begin
      Canvas.Brush.Color := clBtnFace;
    end else  begin
      Canvas.Brush.Color := clWindow;
    end;

    Canvas.Font := ValueListEditor.Font;
    case ACol of
      0 : begin
        TextOut := Cells[ACol, ARow];
      end;

      1 : begin
        case ARow of
          0 : TextOut := Cells[ACol, ARow];
          1 : TextOut := PartData.PartName;
          2 : TextOut := PartData.PartID;
          3 : TextOut := IntToStr(PartData.PartSN);
          4 : TextOut := PartData.ShuttleID;
          5 : TextOut := IntToStr(PartData.ShuttleSN);
          6 : TextOut := TimeFormat(PartData.StartTime);
          7 : TextOut := TimeFormat(PartData.EndTime);
          8 : TextOut := TimeFormat(PartData.ProbeTime);
//          9 : TextOut := IntToHex(PartData.ValidLocation, 4);
          9 : TextOut := BooleanEnum[psfProduction in PartData.StatusFlags];
          10 : TextOut := BooleanEnum[psfDevelopment in PartData.StatusFlags];
          11 : begin
            if psfInspectionRequired in PartData.StatusFlags then begin
              Canvas.Brush.Color := clRed;
              Canvas.Font.Color := clWhite;
            end else begin
//              Canvas.Brush.Color := clWhite;
              Canvas.Font.Color := clBlue;
            end;
            TextOut := BooleanEnum[psfInspectionRequired in PartData.StatusFlags];
          end;
        end;
      end;
    end;
    Canvas.TextRect(Rect, Rect.Left + 5, Rect.Top, TextOut);
  end;
end;

initialization
  TOppHeaderConsumer.AddPlugin(SectionOperationDescription, TOppHeaderConsumer);
  TAbstractFormPlugin.AddPlugin('PartForm', TPartManager);
end.
