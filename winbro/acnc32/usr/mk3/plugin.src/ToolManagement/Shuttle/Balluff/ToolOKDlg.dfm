object IsToolOK: TIsToolOK
  Left = 164
  Top = 218
  Width = 683
  Height = 332
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -16
  Font.Name = 'Verdana'
  Font.Style = [fsBold]
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 18
  object HeaderLabel: TLabel
    Left = 0
    Top = 0
    Width = 675
    Height = 49
    Align = alTop
    AutoSize = False
    Caption = 'The tool added etc.'
    WordWrap = True
  end
  object TotalBarrelsLabel: TLabel
    Left = 32
    Top = 104
    Width = 113
    Height = 18
    Caption = 'Total Barrels'
  end
  object FullBarrelsLabel: TLabel
    Left = 32
    Top = 136
    Width = 97
    Height = 18
    Caption = 'Full Barrels'
  end
  object CurrentBarrelLengthLabel: TLabel
    Left = 32
    Top = 168
    Width = 193
    Height = 18
    Caption = 'Current Barrel Length'
  end
  object TTLabel: TLabel
    Left = 32
    Top = 72
    Width = 87
    Height = 18
    Caption = 'Tool Type'
    FocusControl = BitBtn1
  end
  object BitBtn1: TBitBtn
    Left = 16
    Top = 240
    Width = 217
    Height = 49
    Caption = 'Accept'
    TabOrder = 0
    Kind = bkIgnore
  end
  object BitBtn2: TBitBtn
    Left = 328
    Top = 240
    Width = 201
    Height = 49
    Caption = 'Reset'
    TabOrder = 1
    Kind = bkOK
  end
  object TBEdit: TEdit
    Left = 272
    Top = 96
    Width = 121
    Height = 26
    TabStop = False
    ReadOnly = True
    TabOrder = 2
    Text = 'TBEdit'
  end
  object FBEdit: TEdit
    Left = 272
    Top = 128
    Width = 121
    Height = 26
    TabStop = False
    ReadOnly = True
    TabOrder = 3
    Text = 'FBEdit'
  end
  object CBEdit: TEdit
    Left = 272
    Top = 160
    Width = 121
    Height = 26
    TabStop = False
    ReadOnly = True
    TabOrder = 4
    Text = 'CBEdit'
  end
  object TTEdit: TEdit
    Left = 272
    Top = 64
    Width = 121
    Height = 26
    TabStop = False
    ReadOnly = True
    TabOrder = 5
    Text = 'TTEdit'
  end
end
