unit FindDialogue;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls;

type
  TFindPopup = class(TForm)
    MessageMemo: TMemo;
    Panel1: TPanel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    Panel2: TPanel;
    procedure BitBtn2KeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure BitBtn1KeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FindPopup: TFindPopup;

implementation

{$R *.dfm}

procedure TFindPopup.FormShow(Sender: TObject);
begin
BitBtn1.SetFocus;
end;

procedure TFindPopup.FormKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
begin

if Key = VK_RETURN then
  begin
  if BitBtn1.Focused then
    begin
    ModalResult := mrYes;
    end
  else
    begin
    ModalResult := mrNo;
    end
  end;
end;

procedure TFindPopup.BitBtn1KeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
if Key = VK_RETURN then
  begin
  ModalResult := mrYes;
  end;
end;

procedure TFindPopup.BitBtn2KeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
if Key = VK_RETURN then
  begin
  ModalResult := mrNo;
  end;
end;

end.
