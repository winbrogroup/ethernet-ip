object FindPopup: TFindPopup
  Left = 0
  Top = 0
  BorderIcons = [biMinimize, biMaximize]
  BorderStyle = bsToolWindow
  Caption = 'FindPopup'
  ClientHeight = 140
  ClientWidth = 351
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poScreenCenter
  OnKeyUp = FormKeyUp
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object MessageMemo: TMemo
    Left = 0
    Top = 12
    Width = 351
    Height = 74
    Align = alClient
    Alignment = taCenter
    BorderStyle = bsNone
    Ctl3D = False
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Verdana'
    Font.Style = []
    Lines.Strings = (
      'MessageMemo')
    ParentCtl3D = False
    ParentFont = False
    TabOrder = 0
  end
  object Panel1: TPanel
    Left = 0
    Top = 86
    Width = 351
    Height = 54
    Align = alBottom
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    object BitBtn1: TBitBtn
      Left = 32
      Top = 8
      Width = 105
      Height = 41
      Caption = 'Yes'
      ModalResult = 6
      TabOrder = 0
      OnKeyDown = BitBtn1KeyDown
    end
    object BitBtn2: TBitBtn
      Left = 192
      Top = 8
      Width = 105
      Height = 41
      Caption = 'No'
      ModalResult = 7
      TabOrder = 1
      OnKeyDown = BitBtn2KeyDown
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 351
    Height = 12
    Align = alTop
    BevelOuter = bvNone
    Color = clWhite
    Ctl3D = False
    ParentCtl3D = False
    TabOrder = 2
  end
end
