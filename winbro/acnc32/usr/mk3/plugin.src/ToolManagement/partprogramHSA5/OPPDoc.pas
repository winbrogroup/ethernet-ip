unit OPPDoc;

interface

uses Classes, SysUtils, StreamTokenizer;
type
  TCommentObject = class(TObject)
  private
    Author: string;
    SeeList: TStringList;
    ParamList: TStringList;
    Comment: TStringList;
    ModList: TStringList;
    BugList: TStringList;
  public
    constructor Create;
    destructor Destroy; override;
    procedure AddSee(const Text: string);
    procedure AddParam(const Text: string);
    function HeaderComment : string;
  end;

  TCommentList = class(TObject)
  private
    FVars: TStringList;
    FSubs: TStringList;
    function GetSub(Index: Integer): TCommentObject;
    function GetVar(Index: Integer): TCommentObject;
  public
    constructor Create;
    destructor Destroy; override;
    procedure Clean;
    function AddVar(const name: string): TCommentObject;
    function AddSub(const name: string): TCommentObject;
    function VarCount: Integer;
    function SubCount: Integer;
    property Subs[Index: Integer]: TCommentObject read GetSub;
    property Vars[Index: Integer]: TCommentObject read GetVar;
  end;

  TOPPDoc = class(TObject)
  private
    FC3: string;
    FDestDir: string;
    FStyleSheet: string;
    C3CommentList: TCommentList;
    OPPCommentList: TCommentList;
    procedure ParseFile(const OPPFile: string; CommentList: TCommentList);
    procedure GenerateOutput(Src: TCommentList; const Filename, Intro: string);
  public
    constructor Create;
    destructor Destroy; override;
    procedure Clean;
    property DestDir: string read FDestDir write FDestDir;
    property C3: string read FC3 write FC3;
    property StyleSheet : string read FStyleSheet write FStyleSheet;
    procedure Document(const NC32: string);
  end;

implementation

{ TOPPDoc }

procedure TOPPDoc.Clean;
begin
  OPPCommentList.Clean;
  C3CommentList.Clean;
end;

constructor TOPPDoc.Create;
begin
  OPPCommentList := TCommentList.Create;
  C3CommentList := TCommentList.Create;
end;

destructor TOPPDoc.Destroy;
begin
  OPPCommentList.Free;
  C3CommentList.Free;
  inherited;
end;

const
  oppIntro = '<p>This file represents the current status of your part program and how it integrates to <a href="C3.html">C3</a></p>';
  c3Intro = '<p>This file represents the current status of the C3 library click the following to return to your <a href="opp.html">part program </a></p>';

procedure TOPPDoc.Document(const NC32: string);
var S : TStringList;
begin
  OPPCommentList.Clean;
  S:= TStringList.Create;
  try
    S.LoadFromFile(C3);
    ParseFile(S.Text, C3CommentList);
  finally
    S.Free;
  end;
  ParseFile(NC32, OPPCommentList);

  GenerateOutput(C3CommentList, DestDir + 'c3.html', c3Intro);
  GenerateOutput(OPPCommentList, DestDir + 'opp.html', oppIntro);
end;


procedure TOPPDoc.GenerateOutput(Src: TCommentList; const Filename, Intro: string);
var S: TStringList;
    I, J: Integer;
    C: TCommentObject;
begin
  S := TStringList.Create;
  try
    S.Add('<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN""http://www.w3.org/TR/REC-html40/loose.dtd">');
    S.Add('<html><head><title>OPPDoc</title>');
    S.Add('<LINK REL ="stylesheet" TYPE="text/css" HREF="' + StyleSheet + '" TITLE="Style">');
    S.Add('</head><body>');

    S.Add('<table border="1" width="80%"><tr><td>');

    S.Add('<h2>Programme Definition</h2>');
    S.Add(Intro);
    S.Add('<h2>Subroutine list</h2>');
    S.Add('<table width="100%" cellspacing="0">');
    S.Add('<tr class="TableHeadingColor"><th width="40%">Subroutine</th><th width="60%">Description</th></tr>');
    for I := 0 to Src.SubCount - 1 do begin
      C := Src.Subs[I];
      if Odd(I) then
        S.Add('<tr class="TableRowCell2"><td><font class="TableRowFont2">')
      else
        S.Add('<tr class="TableRowCell1"><td><font class="TableRowFont1">');

      S.Add('<a href="#' + Src.FSubs[I] + '">' + Src.FSubs[I] + '</a>');
      S.Add('</font></td><td>' + C.HeaderComment + '</td></tr>')
    end;
    S.Add('</table>');

    S.Add('<h2>Variable list</h2>');
    S.Add('<table width="100%" cellspacing="0">');
    S.Add('<tr class="TableHeadingColor"><th width="40%">Variable</th><th width="60%">Description</th></tr>');
    for I := 0 to Src.VarCount - 1 do begin
      C := Src.Vars[I];
      if Odd(I) then
        S.Add('<tr class="TableRowCell2"><td><font class="TableRowFont2">')
      else
        S.Add('<tr class="TableRowCell1"><td><font class="TableRowFont1">');

      S.Add('<a href="#' + Src.FVars[I] + '">' + Src.FVars[I] + '</a>');
      S.Add('</font></td><td>' + C.HeaderComment + '</td></tr>')
    end;
    S.Add('</table>');
    S.Add('<br><hr>');

    S.Add('<h2>Subroutine definitions</h2>');

    for I := 0 to Src.SubCount - 1 do begin
      C := Src.Subs[I];
      S.Add('<a name="' + Src.FSubs[I] + '"></a>');
      S.Add('<table width="100%" cellspacing="0">');
      S.Add('<tr><th class="TableHeadingColor" colspan="2">' + Src.FSubs[I] + '</th></tr>');
      S.Add('<tr><td colspan="2">');
      for J := 0 to C.Comment.Count - 1 do
        S.Add(C.Comment[J] + '<br>');
      S.Add('</td></tr>');
      if C.Author <> '' then begin
        S.Add('<tr><th class="TableSubHeadingColor" colspan="2">Author</th></tr>');
        S.Add('<tr><td colspan="2">' + C.Author + '</td></tr>');
      end;

      if C.ParamList.Count > 0 then begin
        S.Add('<tr><th class="TableSubHeadingColor" colspan="2">Parameters</th>');
        for J := 0 to C.ParamList.Count - 1 do begin
          S.Add('<tr><td width="10%" class="TableBoldFont">&nbsp' + UpperCase(C.ParamList[J][1]) + '</td>');
          S.Add('<td>' + Trim(Copy(C.ParamList[J], 2, Length(C.ParamList[J]))) + '</td></tr>');
        end;
      end;

      if C.ModList.Count > 0 then begin
        S.Add('<tr><th class="TableSubHeadingColor" colspan="2">Modified</th>');
        for J := 0 to C.ModList.Count - 1 do begin
        S.Add('<tr><td colspan="2">' + C.ModList[J] + '</td></tr>');
        end;
      end;

      if C.BugList.Count > 0 then begin
        S.Add('<tr><th class="TableSubHeadingColor" colspan="2">Known problems and issues</th>');
        for J := 0 to C.BugList.Count - 1 do begin
        S.Add('<tr><td colspan="2">' + C.BugList[J] + '</td></tr>');
        end;
      end;
      S.Add('</table>');
      S.Add('<hr>');
    end;

    S.Add('<br><hr>');

    for I := 0 to Src.VarCount - 1 do begin
      C := Src.Vars[I];
      S.Add('<a name="' + Src.FVars[I] + '"></a>');
      S.Add('<table width="100%" cellspacing="0">');
      S.Add('<tr><th class="TableHeadingColor" colspan="2">' + Src.FVars[I] + '</th></tr>');
      S.Add('<tr><td colspan="2">');
      for J := 0 to C.Comment.Count - 1 do
        S.Add(C.Comment[J] + '<br>');
      S.Add('</td></tr>');
      S.Add('</table>');
      S.Add('<hr>');
    end;

    S.Add('<center><i>Copyright Amchem Ltd 2003</i></center>');
    S.Add(Format('<center><i>Generated by Acnc32 on %s</i></center>', [DateTimeToStr(Now)]));;

    S.Add('</td></tr></table>');
    S.Add('</body></html>');
    S.SaveToFile(FileName);
  finally
    S.Free;
  end;
end;

procedure TOPPDoc.ParseFile(const OPPFile: string; CommentList: TCommentList);
var ST: TStreamTokenizer;
    Token: string;

  // Expecting ';'
  procedure ReadComment(Comment: TCommentObject);
  var PreviousLine : Integer;
  begin
    Token:= ST.Read;
    while not ST.EndOfStream do begin
      if Token = ';' then begin
        // Get the current line number
        PreviousLine := ST.Line;
        Token := ST.Read;
        if ST.Line <> PreviousLine then begin
          Comment.Comment.Add(''); // a blank line comment
          ST.Push;
        end else begin
          if(Length(Token) = 1) and (Token[1] = '@') then begin
            Token:= LowerCase(ST.Read);
            if Token = 'author' then
              Comment.Author := ST.ReadLine
            else if Token = 'see' then
              Comment.SeeList.Add(ST.ReadLine)
            else if Token = 'mod' then
              Comment.ModList.Add(ST.ReadLine)
            else if Token = 'bug' then
              Comment.BugList.Add(ST.ReadLine)
            else if Length(Token) = 1 then
              Comment.ParamList.Add(Token + ' ' + ST.ReadLine);
          end else begin
            ST.Push;
            Comment.Comment.Add(ST.ReadLine);
          end;
        end;
        Token := ST.Read;
      end else
        Exit;
    end;
  end;

  // The next token is the subroutine name
  procedure ReadSubroutine;
  var Comment: TCommentObject;
  begin
    Comment := CommentList.AddSub(ST.Read);
    ReadComment(Comment);
  end;

  // The next token is the variable name
  procedure ReadVar;
  var Comment: TCommentObject;
  begin
    Comment := CommentList.AddVar(ST.Read);
    if ST.Read = ':' then begin
      ST.Read; // Read the parameter type and dump
    end else begin
      ST.Push;
    end;
    ReadComment(Comment);
  end;

begin
  ST := TStreamTokenizer.Create;
  try
    ST.SetStream(TStringStream.Create(OPPFile));
    ST.Seperator := '%;@';

    Token := ST.Read;
    while not ST.EndOfStream do begin
      if Token = '%' then
        ReadSubroutine
      else if Token = 'var' then
        ReadVar;

      Token := ST.Read;
    end;
  finally
    ST.Free;
  end;
end;

{ TCommentObject }

procedure TCommentObject.AddParam(const Text: string);
begin
  ParamList.Add(Text);
end;

procedure TCommentObject.AddSee(const Text: string);
begin
  SeeList.Add(Text);
end;

constructor TCommentObject.Create;
begin
  SeeList:= TStringList.Create;
  ParamList := TStringList.Create;
  Comment := TStringList.Create;
  ModList := TStringList.Create;
  BugList := TStringList.Create;
end;

destructor TCommentObject.Destroy;
begin
  SeeList.Free;
  ParamList.Free;
  Comment.Free;
  ModList.Free;
  BugList.Free;
  inherited;
end;

function TCommentObject.HeaderComment: string;
begin
  Result := '';
  if Comment.Count > 0 then
    Result := Comment[0];
end;

{ TCommentList }

function TCommentList.AddSub(const name: string): TCommentObject;
begin
  Result := TCommentObject.Create;
  FSubs.AddObject(Name, Result);
end;

function TCommentList.AddVar(const name: string): TCommentObject;
begin
  Result := TCommentObject.Create;
  FVars.AddObject(Name, Result);
end;

procedure TCommentList.Clean;
begin
  while FVars.Count > 0 do begin
    FVars.Objects[0].Free;
    FVars.Delete(0);
  end;
  while FSubs.Count > 0 do begin
    FSubs.Objects[0].Free;
    FSubs.Delete(0);
  end;
end;

constructor TCommentList.Create;
begin
  FVars := TStringList.Create;
  FSubs := TStringList.Create;
end;

destructor TCommentList.Destroy;
begin
  FVars.Free;
  FSubs.Free;
  inherited;
end;

function TCommentList.GetSub(Index: Integer): TCommentObject;
begin
  Result := TCommentObject(FSubs.Objects[Index]);
end;

function TCommentList.GetVar(Index: Integer): TCommentObject;
begin
  Result := TCommentObject(FVars.Objects[Index]);
end;

function TCommentList.SubCount: Integer;
begin
  Result := FSubs.Count;
end;

function TCommentList.VarCount: Integer;
begin
  Result := FVars.Count;
end;

end.
