unit CSVHeader;

interface

uses SysUtils, Classes, AbstractOPPPlugin, StreamTokenizer, PluginInterface,
     IStringArray,NamedPlugin,PartProgramEditor ;

const
  CSVSEctionDescription = 'MDTData';

type
  TCSVSection = class
    procedure Load(S: String); {from strings}
    procedure Save(S: TStrings); {to strings}
  end;


  // IF you need the active OPPs header information but dont want to add a the
  // 10 or so named layer tags + you want to know when a new file is loaded
  // then use the following object, you will need to add the following line
  // in your local initialization section.
  ///
  //  TOppHeaderConsumer.AddPlugin(SectionOperationDescription, TOppHeaderConsumer);
  // Once this has been added you can access the methods via the global OPPHeaderConsumer


implementation

{ TOppHeaderConsumer }


{Each individual tool is identified on the Hypertac plug with a
 14 bit number, of which 9 bits identify the type and 5 bits are
 a type serial number. The total uniquely identifies the tool on
 the site}

procedure TCSVSection.Load(S: String);
var
FileList : TStringList;
Table    : IACNC32StringArray;
RecNum,Records  : Integer;
FieldCount,FieldNum : Integer;
Buffer : String;
FName,Value : String;
GotToken : Boolean;
begin
Filelist := TStringList.Create;
try
FileList.Text := S;
if FileList.count < 1 then
  begin
  FileList.Add('Field1,Field2');
  FileList.Add(' , ');
  end;
FileList.SaveToFile('C:\temp.txt');
if Assigned(Table) then StringArraySet.ReleaseArray(nil, Table);
if (StringArraySet.UseArray('OPPCSVData', nil, Table)) = AAR_OK then
  begin
  Table.BeginUpdate;
  Table.ImportFromCSV('C:\Temp.txt',False,nil);
{  Table.Clear();
  if FileList.Count > 0 then
    begin
    FieldCount := 0;
    Buffer := FileList[0];
    Fname := 'a';
    while Fname <> '' do
      begin
      FName := GetToken(Buffer);
      if Fname <> '' then
        begin
        inc(FieldCount);
        Table.AddField(Fname,nil);
        end
     end;
    if FileList.Count > 1 then
      begin
      Records := FileList.Count-1;
      For RecNum := 0 to Records-1 do
        begin
        Table.Append(1,nil);
        Buffer := FileList[RecNum+1];
        Value := 'a';
        FieldNum := 0;
        while (FieldNum < FieldCount) do
          begin
          try
          Value := GetToken(Buffer);
          begin
          Table.SetValue(RecNum,FieldNum,Value,nil);
          end
        finally
        inc(FieldNum);
        end;
      end;
    end;
    end // FileList.Count > 0
  else
    begin
    Table.AddField('Field1',nil);
    Table.AddField('Field2',nil);
    Table.AddField('Field3',nil);
    Table.Append(1,nil);
    end;  }
  Table.EndUpdate(nil);
  StringArraySet.ReleaseArray(nil,Table);
  end
//end;
finally
FileList.Free;
end;


end;

procedure TCSVSection.Save(S: TStrings);
begin

end;


end.
