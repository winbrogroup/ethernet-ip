unit HelpBrowser;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ACNC_TouchGlyphSoftkey, ExtCtrls, OleCtrls, SHDocVw, StdCtrls;

type
  TFDisplayMode = (fdmNormal,fdmFullScreen);

  THelpBrowserForm = class(TForm)
    HTMLPage: TWebBrowser;
    Panel1: TPanel;
    TouchGlyphSoftkey1: TTouchGlyphSoftkey;
    BtnBrowserClose: TTouchGlyphSoftkey;
    Panel2: TPanel;
    BtnScreenMode: TTouchGlyphSoftkey;
    Button1: TButton;
    procedure FormShow(Sender: TObject);
    procedure BtnBrowserCloseMouseUp(Sender: TObject;
      Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure TouchGlyphSoftkey1MouseUp(Sender: TObject;
      Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure BtnScreenModeMouseUp(Sender: TObject;
      Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure Button1Click(Sender: TObject);
  private
    procedure SetDisplayMode(DMode: TFDisplayMode);
    { Private declarations }
  public
    { Public declarations }
    Filename : String;
    DestinationLink : String;
    DefaultDestination : String;
    FormDisplayMode : TFDisplayMode;
//    constructor Create(Sender : TComponent);override;
  end;

var
  HelpBrowserForm: THelpBrowserForm;

implementation


{$R *.dfm}

procedure THelpBrowserForm.FormShow(Sender: TObject);
var
Destination : String;
begin
Destination := Filename;
if FileExists(Filename) then
  begin
  if DestinationLink <> '' then
    begin
    Destination := Format('%s#%s',[Destination,DestinationLink]);
    end;
  HTMLpage.Navigate(Destination);
  end
else
  begin
  HTMLpage.Navigate(DefaultDestination);
  end;
SetDisplayMode(fdmNormal);

end;

procedure THelpBrowserForm.BtnBrowserCloseMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
ModalResult := mrOK;
end;

procedure THelpBrowserForm.TouchGlyphSoftkey1MouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
try
HTMLPage.GoBack;
except
   if FileExists(DefaultDestination) then
    begin
    HTMLpage.Navigate(DefaultDestination);
    end;
end;
end;

{constructor THelpBrowserForm.Create(Sender: TComponent);
begin
  inherited;
  DefaultDestination := '';
end;}

procedure THelpBrowserForm.BtnScreenModeMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
case FormDisplayMode of
  fdmNormal:
  begin
  SetDisplayMode(fdmFullScreen);
  end;

  fdmFullScreen:
  begin
  SetDisplayMode(fdmNormal);
  end;
end; // case
end;
procedure THelpBrowserForm.SetDisplayMode(DMode : TFDisplayMode);
begin
case DMode of
  fdmNormal:
  begin
  FormDisplayMode := fdmNormal;
  Self.Width := 900;
  Self.Height := 700;
  Self.Top := 50;
  Self.Left := 80;
  BtnScreenMode.Legend := 'Full~Screen';

  end;

  fdmFullScreen:
  begin
  FormDisplayMode := fdmFullScreen;
  Self.Width := 1022;
  Self.Height := 766;
  Self.Top := 1;
  Self.Left := 1;
  BtnScreenMode.Legend := 'Normal~Display';
  end;
  end; // case
end;


procedure THelpBrowserForm.Button1Click(Sender: TObject);
begin
Close;
end;

end.
