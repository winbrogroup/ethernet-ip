library PartProgramSimpleHSA5;

{%File '..\..\..\hsd6ii\ExtendedToolManagement\profile\Live\oppedit.ini'}

uses
  SysUtils,
  Classes,
  PluginInterface,
  NamedPlugin,
  PartProgramEditorHelp in 'PartProgramEditorHelp.pas' {GCodeForm},
  FileLoadDialog in 'FileLoadDialog.pas' {FileLoadForm},
  OPPHeader in '..\lib\OPPHeader.pas',
  OPPDoc in 'OPPDoc.pas',
  AreYouSureDialog in 'AreYouSureDialog.pas' {AreYouSure},
  ACNC_SHeditor in '..\..\DelphiComps\ACNC_SHeditor.pas',
  OCTTypes in '..\lib\OCTTypes.pas',
  OCTUtils in '..\lib\OCTUtils.pas',
  Directory in 'Directory.pas',
  CellMode in '..\lib\CellMode.pas' {CellModeForm},
  AccessAdministration in '..\lib\AccessAdministration.pas' {AccessAdministrator},
  FindDialogue in 'FindDialogue.pas' {FindPopup};

{$R *.RES}
begin
end.
