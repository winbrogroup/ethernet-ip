object FileLoadForm: TFileLoadForm
  Left = -3
  Top = 211
  BorderStyle = bsNone
  Caption = 'File load dialog'
  ClientHeight = 703
  ClientWidth = 1016
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -19
  Font.Name = 'Verdana'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 23
  object Panel1: TPanel
    Left = 0
    Top = 650
    Width = 1016
    Height = 53
    Align = alBottom
    TabOrder = 0
    object BtnOK: TTouchSoftkey
      Left = 8
      Top = 4
      Width = 185
      Height = 41
      Caption = 'BtnOK'
      TabOrder = 0
      OnClick = BtnOKClick
      Legend = 'OK'
      TextSize = 14
      HasIndicator = False
      IndicatorOn = False
      IsEnabled = True
      ButtonStyle = skNormal
      ArrowColor = clBlack
      ButtonColor = clWhite
      LegendColor = clBlack
      IndicatorWidth = 20
      IndicatorHeight = 10
      IndicatorOnColor = clGreen
      IndicatorOffColor = clSilver
      MaxFontSize = 0
    end
    object BtnCancel: TTouchSoftkey
      Left = 200
      Top = 4
      Width = 185
      Height = 41
      Caption = 'BtnCancel'
      TabOrder = 1
      OnClick = BtnCancelClick
      Legend = 'Cancel'
      TextSize = 14
      HasIndicator = False
      IndicatorOn = False
      IsEnabled = True
      ButtonStyle = skNormal
      ArrowColor = clBlack
      ButtonColor = clWhite
      LegendColor = clBlack
      IndicatorWidth = 20
      IndicatorHeight = 10
      IndicatorOnColor = clGreen
      IndicatorOffColor = clSilver
      MaxFontSize = 0
    end
    object BtnPathSwitch: TTouchSoftkey
      Left = 392
      Top = 4
      Width = 273
      Height = 41
      Caption = 'BtnCancel'
      TabOrder = 2
      Legend = 'Path Switch'
      TextSize = 14
      HasIndicator = False
      IndicatorOn = False
      IsEnabled = True
      ButtonStyle = skNormal
      ArrowColor = clBlack
      ButtonColor = clWhite
      LegendColor = clBlack
      IndicatorWidth = 20
      IndicatorHeight = 10
      IndicatorOnColor = clGreen
      IndicatorOffColor = clSilver
      MaxFontSize = 0
    end
    object BtnNew: TTouchSoftkey
      Left = 712
      Top = 4
      Width = 140
      Height = 41
      Caption = 'BtnNew'
      TabOrder = 3
      OnClick = BtnNewClick
      Legend = 'New'
      TextSize = 14
      HasIndicator = False
      IndicatorOn = False
      IsEnabled = True
      ButtonStyle = skNormal
      ArrowColor = clBlack
      ButtonColor = clWhite
      LegendColor = clBlack
      IndicatorWidth = 20
      IndicatorHeight = 10
      IndicatorOnColor = clGreen
      IndicatorOffColor = clSilver
      MaxFontSize = 0
    end
    object BtnDelete: TTouchSoftkey
      Left = 856
      Top = 4
      Width = 140
      Height = 41
      Caption = 'BtnOK'
      Enabled = False
      TabOrder = 4
      OnClick = BtnDeleteClick
      Legend = 'Delete'
      TextSize = 14
      HasIndicator = False
      IndicatorOn = True
      IsEnabled = False
      ButtonStyle = skNormal
      ArrowColor = clBlack
      ButtonColor = clWhite
      LegendColor = clBlack
      IndicatorWidth = 20
      IndicatorHeight = 10
      IndicatorOnColor = clGreen
      IndicatorOffColor = clSilver
      MaxFontSize = 0
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 1016
    Height = 377
    Align = alClient
    TabOrder = 1
    object Panel4: TPanel
      Left = 1
      Top = 1
      Width = 320
      Height = 375
      Align = alLeft
      TabOrder = 0
      object DirGrid: TStringGrid
        Left = 1
        Top = 33
        Width = 318
        Height = 341
        Align = alClient
        ColCount = 1
        DefaultColWidth = 300
        DefaultRowHeight = 32
        DefaultDrawing = False
        FixedCols = 0
        FixedRows = 0
        Options = [goFixedVertLine, goFixedHorzLine, goHorzLine]
        TabOrder = 0
        OnDblClick = DirGridDblClick
        OnDrawCell = DirGridDrawCell
        OnEnter = DirGridEnter
        OnKeyPress = FileGridKeyPress
      end
      object Panel6: TPanel
        Left = 1
        Top = 1
        Width = 318
        Height = 32
        Align = alTop
        TabOrder = 1
        object PathLabel: TLabel
          Left = 1
          Top = 1
          Width = 316
          Height = 30
          Align = alClient
          Caption = 'PathLabel'
        end
      end
    end
    object Panel5: TPanel
      Left = 321
      Top = 1
      Width = 694
      Height = 375
      Align = alClient
      TabOrder = 1
      object FileGrid: TStringGrid
        Left = 1
        Top = 1
        Width = 692
        Height = 373
        Align = alClient
        ColCount = 3
        DefaultColWidth = 300
        DefaultRowHeight = 32
        DefaultDrawing = False
        FixedCols = 0
        Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goThumbTracking]
        TabOrder = 0
        OnClick = FileGridClick
        OnDblClick = BtnOKClick
        OnDrawCell = DirGridDrawCell
        OnEnter = FileGridEnter
        OnKeyPress = FileGridKeyPress
        OnSelectCell = FileGridSelectCell
        ColWidths = (
          300
          209
          300)
      end
    end
  end
  object PanelKeyboard: TPanel
    Left = 0
    Top = 418
    Width = 1016
    Height = 232
    Align = alBottom
    TabOrder = 2
  end
  object Panel3: TPanel
    Left = 0
    Top = 377
    Width = 1016
    Height = 41
    Align = alBottom
    TabOrder = 3
    object Label1: TLabel
      Left = 32
      Top = 8
      Width = 86
      Height = 23
      Caption = 'Filename'
    end
    object SaveEdit: TEdit
      Left = 128
      Top = 4
      Width = 345
      Height = 31
      TabOrder = 0
      Text = 'SaveEdit'
      OnChange = SaveEditChange
    end
    object BtnShowKeyboard: TTouchGlyphSoftkey
      Left = 776
      Top = 4
      Width = 100
      Height = 34
      Legend = 'Show K/B'
      TextSize = 0
      HasIndicator = False
      IndicatorOn = False
      IsEnabled = True
      ButtonColor = clInfoBk
      Layout = slGlyphLeft
      AutoTextSize = True
      KeyStyle = ssRoundedRectangle
      CornerRad = 10
      ButtonTravel = 2
      ShadowColor = clGray
      LegendFont.Charset = DEFAULT_CHARSET
      LegendFont.Color = clBlue
      LegendFont.Height = 24
      LegendFont.Name = 'Comic Sans MS'
      LegendFont.Style = []
      GlyphToEdgeSpacing = 1
      TextToEdgeBorder = 5
      MultiLineSpacing = 0
      MaxFontSize = 24
      OnMouseUp = BtnShowKeyboardMouseUp
    end
  end
end
