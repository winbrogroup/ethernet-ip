unit LinkedLookup;

interface

uses SysUtils, Classes   ;

const LLSectionCount = 61;

type
TLLSectionData = record
  FirstEntryIndex : Integer;
  NumberEntries : Integer;
  IsEmpty       : Boolean;
  end;
pLLSection = ^TLLSectionData;


TLinkedLookUp = class(TComponent)
  private
  SectionList : TList;
  NameList : TStringList;
  DataList : TStringList;
  public
  ItemCount : Integer;
  constructor Create(Owner : TComponent);
  destructor Destroy;
  function FindItem(FindName : String):String;
  function FirstCharacterIndex(Name : String):Integer;
  function AddItem(NewName : String; NewData :String):INteger;
end;

implementation

{ TLinkedLookUp }

function TLinkedLookUp.AddItem(NewName: String; NewData: String): INteger;
var
SectionIndex : Integer;
SectionEntry : pLLSection;
EntryIndex : Integer;
SecNumber : Integer;
begin
SectionIndex := FirstCharacterIndex(NewName);
if SectionIndex >=0 then
  begin
  if SectionIndex <= SectionList.Count then
    begin
    SectionEntry := SectionList[SectionIndex];
    if SectionEntry.IsEmpty then
      begin
      // use entry created in constructor
      EntryIndex := SectionEntry.FirstEntryIndex;
      NameList[EntryIndex] := UpperCase(NewName);
      DataList[EntryIndex] := NewData;
      SectionEntry.IsEmpty := False;
      SectionEntry.NumberEntries := SectionEntry.NumberEntries+1;
      end
    else
      begin
      // Here to create and insert a new entry
      EntryIndex := SectionEntry.FirstEntryIndex+SectionEntry.NumberEntries;
      DataList.Insert(EntryIndex,NewData);
      NameList.Insert(EntryIndex,UpperCase(NewName));
      SectionEntry.NumberEntries := SectionEntry.NumberEntries+1;
      For SecNumber := SectionIndex+1 to LLSectionCount-1 do
        begin
        SectionEntry := SectionList[SecNumber];
        SectionEntry.FirstEntryIndex := SectionEntry.FirstEntryIndex+1;
        end
      end;
    end;
  end;
end;

constructor TLinkedLookUp.Create(Owner : TComponent);
var
SectionNumber : Integer;
SectionEntry :  pLLSection;
begin
inherited;
 SectionList := TList.Create;
 NameList := TStringList.create;
 DataList := TStringList.create;
 For SectionNumber := 0 to LLSectionCount do
    begin
    New(SectionEntry);
    SectionEntry.IsEmpty := True;
    SectionEntry.NumberEntries := 0;
    SectionEntry.FirstEntryIndex := SectionNumber;
    SectionList.Add(SectionEntry);
    NameList.Add('');
    DataList.Add('');
    end;
end;

destructor TLinkedLookUp.Destroy;
var
Section : pLLSection;
begin
while SectionList.Count > 0 do
  begin
  Section := SectionList[0];
  dispose(Section);
  SectionList.Delete(0);
  end;
SectionList.Free;
NameList.Free;
DataList.Free;
inherited;
end;

function TLinkedLookUp.FindItem(FindName: String): String;
var
SectionIndex : Integer;
SectionEntry : pLLSection;
entryNumber : Integer;
DataListIndex : Integer;
Found : Boolean;
begin
Result := '';
SectionIndex := FirstCharacterIndex(FindName);
if SectionIndex >= 0 then
  begin
  SectionEntry := SectionList[SectionIndex];
  if not SectionEntry.IsEmpty then
    begin
    Found := False;
    EntryNumber := 0;
    while (not Found) and ( EntryNumber < SectionEntry.NumberEntries) do
      begin
      try
      DataListIndex := EntryNumber+SectionEntry.FirstEntryIndex;
      if UpperCase(FindName) = NameList[DataListIndex] then
        begin
        Found := True;
        Result := DataList[DataListIndex]
        end;
      finally
       inc(EntryNumber);
      end;
      end;
    end;
  end;
end;

function TLinkedLookUp.FirstCharacterIndex(Name: String): Integer;
var
FirstChar : Integer;
begin
Result := -1;
if Length(name) > 0 then
  begin
  FirstChar := Integer(Name[1]);
  if ((FirstChar>=48) and (FirstChar<=57)) then
    begin
    //0..9
    Result := FirstChar-48; // result 0 to 9
    end else
  if ((FirstChar>=65) and (FirstChar<=90)) then
    begin
    //10..
    Result := FirstChar-55; // result 10 to 35
    end else
  if ((FirstChar>=97) and (FirstChar<=122)) then
    begin
    //0..9
    Result := FirstChar-87; // result 10 to 35
    end;
  end;

end;

end.
