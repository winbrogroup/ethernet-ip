object CancelDialog: TCancelDialog
  Left = 446
  Top = 287
  Width = 319
  Height = 178
  BorderIcons = []
  BorderStyle = bsSizeToolWin
  Caption = 'Dialog'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = True
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object OKBtn: TButton
    Left = 65
    Top = 90
    Width = 75
    Height = 45
    Caption = 'YES'
    Default = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clGreen
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ModalResult = 1
    ParentFont = False
    TabOrder = 0
    OnClick = OKBtnClick
  end
  object CancelBtn: TButton
    Left = 180
    Top = 90
    Width = 75
    Height = 45
    Cancel = True
    Caption = 'NO'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ModalResult = 2
    ParentFont = False
    TabOrder = 1
    OnClick = CancelBtnClick
  end
  object Panel1: TPanel
    Left = 8
    Top = 8
    Width = 297
    Height = 75
    BevelInner = bvLowered
    Caption = 'Panel1'
    Color = clMaroon
    Font.Charset = ANSI_CHARSET
    Font.Color = clYellow
    Font.Height = -16
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 2
    object Memo1: TMemo
      Left = 2
      Top = 2
      Width = 293
      Height = 71
      Align = alClient
      Alignment = taCenter
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlue
      Font.Height = -16
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      Lines.Strings = (
        'Abandon all changes made'
        'to program ??')
      ParentFont = False
      TabOrder = 0
    end
  end
end
