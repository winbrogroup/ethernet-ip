library HSa5HeaderEdit;

{%File '..\..\..\hsd6ii\ExtendedToolManagement\profile\Live\oppedit.ini'}

uses
  SysUtils,
  Classes,
  PluginInterface,
  NamedPlugin,
  HSA5OPPHeaderEditor in 'HSA5OPPHeaderEditor.pas' {GcodeForm},
  OPPHeader in '..\lib\OPPHeader.pas',
  OPPDoc in 'OPPDoc.pas',
  ACNC_SHeditor in '..\..\DelphiComps\ACNC_SHeditor.pas',
  OCTTypes in '..\lib\OCTTypes.pas',
  OCTUtils in '..\lib\OCTUtils.pas',
  CellMode in '..\lib\CellMode.pas' {CellModeForm},
  AccessAdministration in '..\lib\AccessAdministration.pas' {AccessAdministrator};

{$R *.RES}

begin
end.
