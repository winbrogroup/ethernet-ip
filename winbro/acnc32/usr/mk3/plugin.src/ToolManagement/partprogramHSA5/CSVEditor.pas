unit CSVEditor;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ComCtrls, ExtCtrls,  Buttons, OleCtrls, AbstractOPPPlugin,
  NamedPlugin, StreamTokenizer, IniFiles, ACNC_TouchSoftkey, ACNC_BaseKeyBoards,
  INamedInterface,  OPPHeader, ShellApi, CSVHeader, Grids,ACNC_TouchGlyphSoftkey,ACNC_TouchFormatGrid,
  ACNC_TouchScroller, ACNC_TouchStringGrid,IStringArray,
  ACNC_TouchIndexContainer, ACNC_TouchIndexFormatGrid,PartProgramEditor;

type
TCSVDataTYpes = (csvdtDouble,csvdtInteger,csvdtString);
TEditFieldData = Record
  MDTFieldName : String;
  PromptToken : String;
  HelpToken   : String;
  DataTYpe    : TCsvDataTYpes;
  CanEdit     : Boolean;
  end;


pFieldData = ^TeditFieldData;
  TCSVForm = class(TForm)
    TopPanel: TPanel;
    TouchGlyphSoftkey1: TTouchGlyphSoftkey;
    ImportFileDialog: TOpenDialog;
    TouchGlyphSoftkey2: TTouchGlyphSoftkey;
    TouchGlyphSoftkey3: TTouchGlyphSoftkey;
    EditGrid: TTouchIndexFormatGrid;
    CSVMemo: TMemo;
    TouchGlyphSoftkey4: TTouchGlyphSoftkey;
    procedure FormCreate(Sender: TObject);
    procedure TouchGlyphSoftkey1MouseUp(Sender: TObject;
      Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure TouchGlyphSoftkey3MouseUp(Sender: TObject;
      Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure TouchGlyphSoftkey2MouseUp(Sender: TObject;
      Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure TouchGlyphSoftkey4MouseUp(Sender: TObject;
      Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
  private
    { Private declarations }
  Translator : TTranslator;
  FieldList : Tlist;
  procedure UpdateGrid(Lines : TStrings);
  public
    { Public declarations }
  end;

 TOPPCSVEditor = class(TAbstractOPPPluginEditor)
  private
    { Private declarations }
  public
    class function Execute(var aSection: string): Boolean; override;
  end;


 TActiveOppCSVServer = class(TAbstractOPPPluginConsumer)
  private


  public
    FActiveOppCSVFile : TCSVSection;
    constructor Create(aSectionName : string); override;
    procedure Consume(aSectionName, Buffer : PChar); override;
//    procedure Install; override;
  end;

var
  CSVForm: TCSVForm;

implementation


{$R *.dfm}

{ TOPPCSVEditor }


class function TOPPCSVEditor.Execute(var aSection: string): Boolean;
var
Form : TCSVForm;
TempStream : TMemoryStream;
TempStrings : TStringList;
Table    : IACNC32StringArray;
FieldCount,FieldNum,RecordCount,RecNum : Integer;
Value,Buffer,FName : String;

begin
  Form := TCSVForm.Create(Application);
  Form.CSVMemo.Lines.Text :=aSection;
  if Form.CSVMemo.Lines.Count = 0 then
    begin
    Form.CSVMemo.Lines.Add('Field1,Field2');
    Form.CSVMemo.Lines.Add(' , ');
    end;
  Form.UpdateGrid(Form.CSVMemo.Lines);

  Result := Form.ShowModal = mrOK;
  if (Result) Then
     begin
     TempStream := TMemoryStream.Create;
     TempStrings := TStringList.Create;
     try
     TempStrings.Clear;
     Form.CSVMemo.Lines.SaveToStream(TempStream);
     TempStream.Position := 0;
     TempStrings.LoadFromStream(TempStream);
     aSection := TempStrings.Text;
     finally
     TempStream.Free;
     TempStrings.Free;
     end;
     end;
  Form.Free;
end;



procedure TCSVForm.FormCreate(Sender: TObject);
begin
Top := 40;
Left := 0;
Width := 1022;
Height := 726;


end;

{ TActiveOppCSVServer }

procedure TActiveOppCSVServer.Consume(aSectionName, Buffer: PChar);
begin
  inherited;
  FActiveOppCSVFile.Load(Buffer);
end;


constructor TActiveOppCSVServer.Create(aSectionName: string);
begin
  inherited;
  FActiveOppCSVFile := TCSVSection.Create;
//  aParent.RegisterMemEvent(nameNewActiveFile, edgeChange, TStringTag, NewProgram);
end;


procedure TCSVForm.TouchGlyphSoftkey1MouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
ImportFileDialog.Execute;
if FileExists(ImportFileDialog.FileName) then
  begin
  CSVMemo.Lines.LoadFromFile(ImportFileDialog.FileName);
  UpdateGrid(CSVMemo.Lines);
  end;

end;

procedure TCSVForm.TouchGlyphSoftkey3MouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
 ModalResult := mrOk;
end;


procedure TCSVForm.TouchGlyphSoftkey2MouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
 ModalResult := mrCancel;
end;

procedure TCSVForm.TouchGlyphSoftkey4MouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
with CSVMemo.Lines do
  begin
  Clear;
  Add('Field1,Field2,Field3');
  Add(', , ,');
  end;
  UpdateGrid(CSVMemo.Lines);

end;

procedure TCSVForm.UpdateGrid(Lines: TStrings);
var
FieldCount,FieldNum,RecordCount,RecNum : Integer;
Value,Buffer,FName : String;

begin
      Buffer := Lines[0];
      Fname := 'a';
      FieldCount := 0;
      EditGrid.ColCount := 2;
      EditGrid.Cells[0,0] := 'Record';
      while Fname <> '' do
        begin
        FName := GetToken(Buffer);
        if Fname <> '' then
          begin
          inc(FieldCount);
          EditGrid.ColCount := FieldCount+1;
          EditGrid.Cells[FieldCount,0] := Fname;
          end;
        end;
       EditGrid.RowCount := 2;
       if Lines.Count > 1 then
         begin
         EditGrid.RowCount := Lines.Count;
         RecordCount := Lines.Count-1;
         For RecNum := 0 to RecordCount-1 do
            begin
            EditGrid.Cells[0,RecNum+1] := IntToStr(RecNum);
            FieldNum := 0;
            Buffer := Lines[RecNum+1];
            while FieldNum < FieldCount do
              begin
              try
              Value := GetToken(Buffer);
              EditGrid.Cells[FieldNum+1,RecNum+1] := Value;
              finally
              inc(FieldNum);
              end;
              end;
            end;
         end;
end;

initialization
  TOPPCSVEditor.AddPlugin(CSVSEctionDescription, TOPPCSVEditor);
  TActiveOppCSVServer.AddPlugin(CSVSEctionDescription, TActiveOppCSVServer);
end.
