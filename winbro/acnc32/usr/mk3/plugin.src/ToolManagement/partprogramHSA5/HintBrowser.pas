unit HintBrowser;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OleCtrls, SHDocVw;

type
  THintViewer = class(TForm)
    WebBrowser: TWebBrowser;
  private
    { Private declarations }
  public
    procedure SetPage(APage: string);
  end;

var
  HintViewer: THintViewer;

implementation

{$R *.dfm}

{ TForm5 }

procedure THintViewer.SetPage(APage: string);
begin
  WebBrowser.Navigate(APage);
end;

end.
