library HSA5_OperatorDisplay;

{%File 'C:\HSA5\profile\Live\C3.cfg'}
{%File 'C:\HSD_Mk3\profile\Live\C3.cfg'}
{%File 'C:\HSA5\profile\cnc.ini'}
{%File 'C:\HSA5\profile\Live\General_Data.Cfg'}
{%File 'C:\HSA5\profile\Live\Softpanel.cfg'}
{%File 'C:\HSA5\profile\Live\ManualData.Cfg'}
{%File 'C:\HSA5\profile\Live\StatusPanel.Cfg'}

uses
  SysUtils,
  Classes,
  OCTData in '..\lib\OCTData.pas',
  PartData in '..\lib\PartData.pas',
  OpDisplayTypes in 'OpDisplayTypes.pas',
  SyntaxHighProgDisplay in 'SyntaxHighProgDisplay.pas',
  HDSA5OCTOperatorDisplay in 'HDSA5OCTOperatorDisplay.pas' {OCTFormDlg},
  ToolData in '..\lib\ToolData.pas',
  OCTListBox in 'OCTListBox.pas',
  ReqToolList in 'ReqToolList.pas',
  ToolDB in '..\lib\ToolDB.pas',
  RequiredToolData in '..\lib\RequiredToolData.pas';

{$R *.RES}

{ ACNC32PluginRevision is not longer used by ACNC32, it now gets the version
  information directly from the dll resource.
function ACNC32PluginRevision : Cardinal; stdcall;
begin
  Result := $01000000;
end;

exports
  ACNC32PluginRevision;                       }

begin
end.
