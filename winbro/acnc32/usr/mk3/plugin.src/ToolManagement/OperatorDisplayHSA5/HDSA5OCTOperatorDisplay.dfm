object OCTFormDlg: TOCTFormDlg
  Left = -24
  Top = 70
  BorderStyle = bsNone
  ClientHeight = 715
  ClientWidth = 1016
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -19
  Font.Name = 'Verdana'
  Font.Style = []
  OldCreateOrder = False
  Position = poDefault
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 23
  object BottomPanel: TPanel
    Left = 0
    Top = 0
    Width = 1016
    Height = 715
    Align = alClient
    TabOrder = 0
    object BaseOCTPanel: TPanel
      Left = 1
      Top = 83
      Width = 1014
      Height = 376
      Align = alTop
      Caption = 'BaseOCTPanel'
      TabOrder = 0
      OnResize = BaseOCTPanelResize
      object RecoveryPanel: TPanel
        Left = 1
        Top = 1
        Width = 1012
        Height = 374
        Align = alTop
        BevelInner = bvLowered
        TabOrder = 0
        object RecoveryBanner: TPanel
          Left = 2
          Top = 2
          Width = 1008
          Height = 41
          Align = alTop
          BevelInner = bvLowered
          Caption = 'OCT Recovery Options'
          Color = clTeal
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -19
          Font.Name = 'Verdana'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
        end
        object Panel4: TPanel
          Left = 2
          Top = 98
          Width = 373
          Height = 8
          BevelInner = bvLowered
          TabOrder = 1
        end
        object Panel6: TPanel
          Left = 2
          Top = 89
          Width = 1008
          Height = 8
          Align = alTop
          BevelInner = bvLowered
          TabOrder = 2
        end
        object Panel8: TPanel
          Left = 2
          Top = 43
          Width = 373
          Height = 8
          BevelInner = bvLowered
          TabOrder = 3
        end
        object RestartPanel: TPanel
          Left = 2
          Top = 43
          Width = 1008
          Height = 46
          Align = alTop
          BevelInner = bvLowered
          Caption = 'Restart Current OPP'
          Color = clInfoBk
          TabOrder = 4
          object RestartCurrentButton: TTouchSoftkey
            Left = 2
            Top = 2
            Width = 122
            Height = 42
            Align = alLeft
            Caption = 'TouchSoftkey1'
            TabOrder = 0
            OnMouseUp = RestartCurrentButtonMouseUp
            TextSize = 14
            HasIndicator = False
            IndicatorOn = False
            IsEnabled = True
            ButtonStyle = skNormal
            ArrowColor = clBlack
            ButtonColor = clWhite
            LegendColor = clBlack
            IndicatorWidth = 20
            IndicatorHeight = 10
            IndicatorOnColor = clGreen
            IndicatorOffColor = clSilver
            MaxFontSize = 0
          end
        end
        object Panel10: TPanel
          Left = 2
          Top = 143
          Width = 1008
          Height = 8
          Align = alTop
          BevelInner = bvLowered
          TabOrder = 5
        end
        object SkipPanel: TPanel
          Left = 2
          Top = 97
          Width = 1008
          Height = 46
          Align = alTop
          BevelInner = bvLowered
          Caption = '     Skip To Next OPP'
          Color = clInfoBk
          TabOrder = 6
          object SkipButton: TTouchSoftkey
            Left = 2
            Top = 2
            Width = 122
            Height = 42
            Align = alLeft
            Caption = 'TouchSoftkey1'
            TabOrder = 0
            OnMouseUp = SkipButtonMouseUp
            TextSize = 14
            HasIndicator = False
            IndicatorOn = False
            IsEnabled = True
            ButtonStyle = skNormal
            ArrowColor = clBlack
            ButtonColor = clWhite
            LegendColor = clBlack
            IndicatorWidth = 20
            IndicatorHeight = 10
            IndicatorOnColor = clGreen
            IndicatorOffColor = clSilver
            MaxFontSize = 0
          end
        end
        object Panel9: TPanel
          Left = 2
          Top = 197
          Width = 1008
          Height = 8
          Align = alTop
          BevelInner = bvLowered
          TabOrder = 7
        end
        object AbandonPartPanel: TPanel
          Left = 2
          Top = 205
          Width = 1008
          Height = 46
          Align = alTop
          BevelInner = bvLowered
          Caption = 'Mark part as non conformance'
          Color = clInfoBk
          TabOrder = 8
          Visible = False
          object AbandonPartButton: TTouchSoftkey
            Left = 2
            Top = 2
            Width = 122
            Height = 42
            Align = alLeft
            Caption = 'TouchSoftkey1'
            TabOrder = 0
            OnMouseUp = AbandonPartButtonMouseUp
            TextSize = 14
            HasIndicator = False
            IndicatorOn = False
            IsEnabled = True
            ButtonStyle = skNormal
            ArrowColor = clBlack
            ButtonColor = clWhite
            LegendColor = clBlack
            IndicatorWidth = 20
            IndicatorHeight = 10
            IndicatorOnColor = clGreen
            IndicatorOffColor = clSilver
            MaxFontSize = 0
          end
        end
        object Panel11: TPanel
          Left = 2
          Top = 251
          Width = 1008
          Height = 8
          Align = alTop
          BevelInner = bvLowered
          TabOrder = 9
          object Panel2: TPanel
            Left = 304
            Top = 8
            Width = 185
            Height = 41
            Caption = 'Panel1'
            TabOrder = 0
          end
        end
        object AbandonOCTPanel: TPanel
          Left = 2
          Top = 151
          Width = 1008
          Height = 46
          Align = alTop
          BevelInner = bvLowered
          Caption = '     Abandon Entire OCT'
          Color = clInfoBk
          TabOrder = 10
          object AbandonOCTButton: TTouchSoftkey
            Left = 2
            Top = 2
            Width = 122
            Height = 42
            Align = alLeft
            Caption = 'AbandonOCTButton'
            TabOrder = 0
            OnMouseUp = AbandonOCTButtonMouseUp
            TextSize = 14
            HasIndicator = False
            IndicatorOn = False
            IsEnabled = True
            ButtonStyle = skNormal
            ArrowColor = clBlack
            ButtonColor = clWhite
            LegendColor = clBlack
            IndicatorWidth = 20
            IndicatorHeight = 10
            IndicatorOnColor = clGreen
            IndicatorOffColor = clSilver
            MaxFontSize = 0
          end
        end
        object ReworkPanel: TPanel
          Left = 2
          Top = 259
          Width = 1008
          Height = 41
          Align = alTop
          Caption = 'Rework current OPP'
          Color = clInfoBk
          TabOrder = 11
          object ReworkButton: TTouchSoftkey
            Left = 0
            Top = 0
            Width = 122
            Height = 41
            Caption = 'ReworkButton'
            TabOrder = 0
            OnMouseUp = ReworkButtonMouseUp
            TextSize = 14
            HasIndicator = False
            IndicatorOn = False
            IsEnabled = True
            ButtonStyle = skNormal
            ArrowColor = clBlack
            ButtonColor = clWhite
            LegendColor = clBlack
            IndicatorWidth = 20
            IndicatorHeight = 10
            IndicatorOnColor = clGreen
            IndicatorOffColor = clSilver
            MaxFontSize = 0
          end
        end
        object Panel7: TPanel
          Left = 2
          Top = 308
          Width = 1008
          Height = 64
          Align = alClient
          BevelInner = bvLowered
          TabOrder = 12
          object Panel12: TPanel
            Left = 2
            Top = 2
            Width = 904
            Height = 60
            Align = alClient
            Alignment = taLeftJustify
            Color = clInfoBk
            Font.Charset = ANSI_CHARSET
            Font.Color = clNavy
            Font.Height = -16
            Font.Name = 'Comic Sans MS'
            Font.Style = []
            ParentFont = False
            TabOrder = 0
            OnResize = Panel12Resize
            object MessageLabel: TLabel
              Left = 1
              Top = 1
              Width = 902
              Height = 32
              Align = alTop
              AutoSize = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clNavy
              Font.Height = -16
              Font.Name = 'Verdana'
              Font.Style = []
              ParentFont = False
              WordWrap = True
            end
            object MessageLabel2: TLabel
              Left = 1
              Top = 29
              Width = 902
              Height = 30
              Align = alBottom
              AutoSize = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clNavy
              Font.Height = -16
              Font.Name = 'Verdana'
              Font.Style = []
              ParentFont = False
              WordWrap = True
            end
          end
          object AcceptKey: TTouchSoftkey
            Left = 906
            Top = 2
            Width = 100
            Height = 60
            Align = alRight
            Caption = 'TouchSoftkey1'
            TabOrder = 1
            OnMouseUp = AcceptKeyMouseUp
            Legend = 'ACCEPT'
            TextSize = 14
            HasIndicator = False
            IndicatorOn = False
            IsEnabled = True
            ButtonStyle = skNormal
            ArrowColor = clBlack
            ButtonColor = clLime
            LegendColor = clMaroon
            IndicatorWidth = 20
            IndicatorHeight = 10
            IndicatorOnColor = clGreen
            IndicatorOffColor = clSilver
            MaxFontSize = 0
          end
        end
        object Panel13: TPanel
          Left = 2
          Top = 300
          Width = 1008
          Height = 8
          Align = alTop
          BevelInner = bvLowered
          TabOrder = 13
        end
      end
    end
    object ButtonPanel: TPanel
      Left = 1
      Top = 660
      Width = 1014
      Height = 54
      Align = alBottom
      BevelInner = bvLowered
      BorderWidth = 1
      TabOrder = 1
      object BtnClose: TTouchGlyphSoftkey
        Left = 913
        Top = 3
        Width = 98
        Height = 48
        Legend = '   Close    '
        TextSize = 0
        HasIndicator = False
        IndicatorOn = False
        IsEnabled = True
        ButtonColor = clInfoBk
        Layout = slGlyphLeft
        AutoTextSize = True
        KeyStyle = ssRectangle
        CornerRad = 10
        ButtonTravel = 2
        ShadowColor = clGray
        LegendFont.Charset = ANSI_CHARSET
        LegendFont.Color = clRed
        LegendFont.Height = 20
        LegendFont.Name = 'Verdana'
        LegendFont.Style = []
        GlyphToEdgeSpacing = 1
        TextToEdgeBorder = 5
        MultiLineSpacing = 0
        MaxFontSize = 20
        OnMouseDown = BtnCloseMouseDown
        align = alRight
      end
      object BtnShowPartData: TTouchGlyphSoftkey
        Left = 415
        Top = 3
        Width = 110
        Height = 48
        Legend = 'Show~Part Data'
        TextSize = 0
        HasIndicator = False
        IndicatorOn = False
        IsEnabled = True
        ButtonColor = clInfoBk
        Layout = slGlyphLeft
        AutoTextSize = True
        KeyStyle = ssRectangle
        CornerRad = 10
        ButtonTravel = 2
        ShadowColor = clGray
        LegendFont.Charset = ANSI_CHARSET
        LegendFont.Color = clBlack
        LegendFont.Height = 17
        LegendFont.Name = 'Verdana'
        LegendFont.Style = []
        GlyphToEdgeSpacing = 1
        TextToEdgeBorder = 7
        MultiLineSpacing = 0
        MaxFontSize = 20
        OnMouseUp = BtnGeneralMouseUp
        OnMouseDown = BtnGeneralMouseDown
        align = alLeft
      end
      object BtnFaultAck: TTouchGlyphSoftkey
        Left = 3
        Top = 3
        Width = 110
        Height = 48
        Legend = 'Fault~Acknowledge'
        TextSize = 0
        HasIndicator = False
        IndicatorOn = False
        IsEnabled = True
        ButtonColor = clInfoBk
        Layout = slGlyphLeft
        AutoTextSize = True
        KeyStyle = ssRectangle
        CornerRad = 10
        ButtonTravel = 2
        ShadowColor = clGray
        LegendFont.Charset = ANSI_CHARSET
        LegendFont.Color = clBlack
        LegendFont.Height = 17
        LegendFont.Name = 'Verdana'
        LegendFont.Style = []
        GlyphToEdgeSpacing = 1
        TextToEdgeBorder = 7
        MultiLineSpacing = 0
        MaxFontSize = 20
        OnMouseUp = BtnGeneralMouseUp
        OnMouseDown = BtnGeneralMouseDown
        align = alLeft
      end
      object PartChuckStatusPanel: TPanel
        Left = 283
        Top = 3
        Width = 72
        Height = 48
        Align = alLeft
        BevelInner = bvLowered
        Color = clLime
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
        TabOrder = 3
      end
      object BtnPartChuck: TTouchGlyphSoftkey
        Left = 173
        Top = 3
        Width = 110
        Height = 48
        Legend = 'Part~Chuck'
        TextSize = 0
        HasIndicator = False
        IndicatorOn = False
        IsEnabled = True
        ButtonColor = clInfoBk
        Layout = slGlyphLeft
        AutoTextSize = True
        KeyStyle = ssRectangle
        CornerRad = 10
        ButtonTravel = 2
        ShadowColor = clGray
        LegendFont.Charset = ANSI_CHARSET
        LegendFont.Color = clBlack
        LegendFont.Height = 17
        LegendFont.Name = 'Verdana'
        LegendFont.Style = []
        GlyphToEdgeSpacing = 1
        TextToEdgeBorder = 7
        MultiLineSpacing = 0
        MaxFontSize = 20
        OnMouseUp = BtnGeneralMouseUp
        OnMouseDown = BtnGeneralMouseDown
        align = alLeft
      end
      object Spacer1: TPanel
        Left = 113
        Top = 3
        Width = 60
        Height = 48
        Align = alLeft
        TabOrder = 5
      end
      object Spacer2: TPanel
        Left = 355
        Top = 3
        Width = 60
        Height = 48
        Align = alLeft
        TabOrder = 6
      end
    end
    object OctStatusPanel: TPanel
      Left = 1
      Top = 1
      Width = 1014
      Height = 82
      Align = alTop
      BevelInner = bvLowered
      TabOrder = 2
      object Panel1: TPanel
        Left = 2
        Top = 39
        Width = 1010
        Height = 41
        Align = alBottom
        Caption = 'TopStatusPanel'
        TabOrder = 0
        object LeftStatusPanel: TPanel
          Left = 1
          Top = 1
          Width = 390
          Height = 39
          Align = alLeft
          BevelInner = bvLowered
          TabOrder = 0
          object LabelStatus1: TLabel
            Left = 24
            Top = 8
            Width = 7
            Height = 23
          end
        end
        object RightStatusPanel: TPanel
          Left = 391
          Top = 1
          Width = 618
          Height = 39
          Align = alClient
          BevelInner = bvLowered
          TabOrder = 1
          object LabelStatus2: TLabel
            Left = 8
            Top = 8
            Width = 7
            Height = 23
          end
        end
      end
      object ToolPanel: TPanel
        Left = 2
        Top = 2
        Width = 250
        Height = 37
        Align = alLeft
        BevelInner = bvLowered
        TabOrder = 1
        object Label1: TLabel
          Left = 8
          Top = 10
          Width = 91
          Height = 18
          Caption = 'Active Tool'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Verdana'
          Font.Style = []
          ParentFont = False
        end
        object ActiveToolDisplay: TLabel
          Left = 112
          Top = 7
          Width = 103
          Height = 25
          Caption = 'SCANNER'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlue
          Font.Height = -21
          Font.Name = 'Verdana'
          Font.Style = []
          ParentFont = False
        end
      end
      object TimePanel: TPanel
        Left = 762
        Top = 2
        Width = 250
        Height = 37
        Align = alRight
        BevelInner = bvLowered
        TabOrder = 2
        object TRLabel: TLabel
          Left = 8
          Top = 10
          Width = 132
          Height = 18
          Caption = 'Time Remaining'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Verdana'
          Font.Style = []
          ParentFont = False
        end
        object RemTimeDisplay: TLabel
          Left = 160
          Top = 7
          Width = 68
          Height = 25
          Alignment = taCenter
          Caption = '00:00'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlue
          Font.Height = -21
          Font.Name = 'Verdana'
          Font.Style = [fsBold]
          ParentFont = False
        end
      end
      object ScannerPanel: TPanel
        Left = 462
        Top = 2
        Width = 300
        Height = 37
        Align = alRight
        BevelInner = bvLowered
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
        TabOrder = 3
        object Label2: TLabel
          Left = 24
          Top = 8
          Width = 6
          Height = 18
        end
        object Label3: TLabel
          Left = 340
          Top = 2
          Width = 39
          Height = 16
          Caption = 'Buffer'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Verdana'
          Font.Style = []
          ParentFont = False
        end
        object Label4: TLabel
          Left = 340
          Top = 18
          Width = 53
          Height = 16
          Caption = 'Program'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Verdana'
          Font.Style = []
          ParentFont = False
        end
        object Label5: TLabel
          Left = 4
          Top = 2
          Width = 94
          Height = 16
          Caption = 'Scanner Prog:'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Verdana'
          Font.Style = []
          ParentFont = False
        end
        object Label6: TLabel
          Left = 4
          Top = 18
          Width = 97
          Height = 16
          Caption = 'Program Lines:'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Verdana'
          Font.Style = []
          ParentFont = False
        end
        object ScanProgNameDisplay: TLabel
          Left = 108
          Top = 2
          Width = 171
          Height = 16
          Caption = '*******************'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Verdana'
          Font.Style = []
          ParentFont = False
        end
        object ProgLinesDisplay: TLabel
          Left = 108
          Top = 18
          Width = 126
          Height = 16
          Caption = '**************'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Verdana'
          Font.Style = []
          ParentFont = False
        end
        object BufferProgBar: TProgressBar
          Left = 400
          Top = 6
          Width = 100
          Height = 12
          TabOrder = 0
        end
        object ProgramProgBar: TProgressBar
          Left = 400
          Top = 22
          Width = 100
          Height = 12
          TabOrder = 1
        end
      end
      object ProbePanel: TPanel
        Left = 252
        Top = 2
        Width = 120
        Height = 37
        Align = alLeft
        BevelInner = bvLowered
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
        TabOrder = 4
        object Label7: TLabel
          Left = 24
          Top = 8
          Width = 6
          Height = 18
        end
        object Label8: TLabel
          Left = 8
          Top = 12
          Width = 74
          Height = 16
          Caption = 'Probe Fired'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Verdana'
          Font.Style = []
          ParentFont = False
        end
        object ProbeIndicator: TShape
          Left = 90
          Top = 12
          Width = 16
          Height = 16
          Brush.Color = clRed
          Shape = stCircle
        end
        object Highlight: TShape
          Left = 95
          Top = 16
          Width = 5
          Height = 5
          Brush.Color = clBtnFace
          Pen.Color = clSilver
          Shape = stCircle
        end
      end
    end
    object OCTLowerPageControl: TPageControl
      Left = 1
      Top = 459
      Width = 1014
      Height = 201
      ActivePage = PDisplaySheet
      Align = alClient
      TabOrder = 3
      object PDisplaySheet: TTabSheet
        Caption = 'PDisplaySheet'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Verdana'
        Font.Style = []
        ImageIndex = 1
        ParentFont = False
        TabVisible = False
        object Panel17: TPanel
          Left = 0
          Top = 0
          Width = 1006
          Height = 24
          Align = alTop
          Alignment = taLeftJustify
          BevelInner = bvLowered
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Verdana'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          object ProgNameDisplay: TLabel
            Left = 4
            Top = 2
            Width = 5
            Height = 16
          end
          object ProgLineDisplay: TLabel
            Left = 620
            Top = 4
            Width = 5
            Height = 16
          end
        end
      end
    end
  end
  object OCTImages: TImageList
    Height = 32
    Width = 32
    Left = 168
    Top = 48
    Bitmap = {
      494C010102000400040020002000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000800000002000000001002000000000000040
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000DF00
      DF00EF00BF00FF00CF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000007F007F007F007F007F007F007F007F007F007F007F007F000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000DF00DF007F00
      4000DF000000DF001F00BF00BF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000CF00CF003F00
      3F003F003F000000BF000000BF000000BF000000BF000000BF000000BF003F00
      3F003F003F00CF00CF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000005F002F00DF00
      0000FF000000EF000000DF007F00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000CF00CF0000006F000000
      FF000000FF003F00FF003F00FF003F00FF003F00FF003F00FF003F00FF000000
      FF000000FF0000006F00BF00BF00DF00DF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000DF00DF005F002F00FF000000FF00
      0000FF2F0000FF0000009F000000CF00BF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000DF00DF007F00BF001F009F000000FF003F00
      FF007F00FF000000000000000000000000000000000000000000000000007F00
      FF003F00FF000000FF0000007F003F007F00DF00DF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000BF007F00DF000000FF000000FF3F
      0000FFDF0000FF000000FF0000009F005F000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000002F005F000000DF000000FF002F00FF00DF00
      FF00000000000000000000000000000000000000000000000000000000000000
      0000DF00FF002F00FF000000FF000000DF002F005F0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000CF00CF009F001F00FF000000FF2F0000FF7F
      5F00FF3FBF00FF8F0000FF000000CF0000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000CF00CF000000FF000000FF002F00FF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000002F00FF000000FF000000FF00CF00CF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000CF00CF006F000000FF000000FF2F0000FF8F6F000000
      000000000000FF9F2F00FF1F0000FF0000006F003F0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000003F009F000000FF003F00FF00DF00FF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000DF00FF003F00FF000000FF003F009F00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000BF00BF007F001F00FF000000FF3F0000FF7F5F00000000000000
      000000000000FF3FBF00FF7F0000FF000000DF001F00BF00BF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000003F00FF000000FF007F00FF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000007F00FF000000FF000F00CF009F009F000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000DF007F00EF000000FF2F0000FF7F5F00FF0FEF00000000000000
      000000000000FF0FEF00FF7F5F00FF2F0000EF000000DF007F00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000007F00FF000000FF003F00FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000003F00FF000000FF007F00FF000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FF5F9F00FFBF3F00FF8F6F000000000000000000000000000000
      0000000000000000000000000000FF2F3F00FF0000007F000000BF00BF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000007F00FF000000FF003F00FF00BF00BF007F007F007F007F007F00
      7F007F007F007F007F007F007F007F007F007F007F007F007F007F007F007F00
      7F007F007F007F007F007F007F00BF00BF003F00FF000000FF007F00FF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FFBF3F00FF000000BF000000DF00BF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000007F00FF000000FF003F00FF007F00DF000000BF000000BF000000
      BF000000BF000000BF000000BF000000BF000000BF000000BF000000BF000000
      BF000000BF000000BF000000BF007F00DF003F00FF000000FF007F00FF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FF2FCF00FF000000FF0000006F002F000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000007F00FF000000FF003F00FF007F00FF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF007F00FF003F00FF000000FF007F00FF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF9F2F00FF1F0000FF000000CF00
      CF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000007F00FF000000FF003F00FF00BF00FF007F00FF007F00FF007F00
      FF007F00FF007F00FF007F00FF007F00FF007F00FF007F00FF007F00FF007F00
      FF007F00FF007F00FF007F00FF00BE00FF003F00FF000000FF007F00FF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF1FDF00FF403F00FF0000009F00
      3F00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000007F00FF000000FF003F00FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000003F00FF000000FF007F00FF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FF5F7F00FF2F0000CF00
      0F009F009F000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000003F00FF000000FF005F00DF00EF00EF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000EF00EF005F00DF000000FF003F00FF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FF2F3F00FF00
      0000DF005F00EF00EF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000009F00FF000000FF000000BF005F007F00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000005F007F000000BE000000FF009F00FF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FF5F9F00FF5F
      0000BF0000007F005F0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FF000000FF000000CF006F006F000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000006F006F000000CF000000FF000000FF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FF5F
      8F00FF5F0000CF0000006F006F00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000CF00FF001F00FF000000FF000000CF005F00
      7F00BF00BF00000000000000000000000000000000000000000000000000EF00
      EF005F007F000000CF000000FF001F00FF00CF00FF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FF5F9F00FF5F0F00CF0000007F005F00EF00EF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000BF00FF007F00FF000000FF000000
      BF0000007F007F007F007F007F007F007F007F007F007F007F007F007F005F00
      DF000000BE000000FF007F00FF00BE00FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FF5F9F00FF5F0000BF0000007F005F0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000008F00FF000000
      FF000000FF000000BF000000BF000000BF000000BF000000BF000000BF000000
      FF000000FF008F00FF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000FF5F8F00FF5F0000CF0000006F006F00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000003F00FF003F00FF003F00FF003F00FF003F00FF003F00FF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FF5F9F00FFBF3F00CF2F00003F000000CF00
      BF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FF5F7F00FF007F00FF00
      DF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000080000000200000000100010000000000000200000000000000000000
      000000000000000000000000FFFFFF00FFFFFFFFFFFFFFFF0000000000000000
      FFFFFFFFFFFFFFFF0000000000000000FFFFFFFFFFFFFFFF0000000000000000
      FFFFFFFFFFFFFFFF0000000000000000FFFFFFFFFFE3FFFF0000000000000000
      FFF81FFFFFC1FFFF0000000000000000FFC003FFFFC1FFFF0000000000000000
      FF8000FFFF00FFFF0000000000000000FE07E07FFF00FFFF0000000000000000
      FE0FF07FFE00FFFF0000000000000000FC3FFC3FFC187FFF0000000000000000
      FC3FFC3FF8383FFF0000000000000000FC7FFE1FF8383FFF0000000000000000
      F8FFFF1FF8FE1FFF0000000000000000F800001FFFFE1FFF0000000000000000
      F800001FFFFE1FFF0000000000000000F800001FFFFF0FFF0000000000000000
      F800001FFFFF0FFF0000000000000000F8FFFF1FFFFF87FF0000000000000000
      FC3FFC3FFFFFC3FF0000000000000000FC3FFC3FFFFFC3FF0000000000000000
      FE1FF87FFFFFE1FF0000000000000000FE07E07FFFFFF07F0000000000000000
      FF0000FFFFFFF87F0000000000000000FFC003FFFFFFFC3F0000000000000000
      FFF81FFFFFFFFE0F0000000000000000FFFFFFFFFFFFFF8F0000000000000000
      FFFFFFFFFFFFFFFF0000000000000000FFFFFFFFFFFFFFFF0000000000000000
      FFFFFFFFFFFFFFFF0000000000000000FFFFFFFFFFFFFFFF0000000000000000
      FFFFFFFFFFFFFFFF000000000000000000000000000000000000000000000000
      000000000000}
  end
  object TImageList
    Left = 240
    Top = 48
    Bitmap = {
      494C010102000400040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000001000000001002000000000000010
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000404080004040800040408000404080004040800000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000404080004040800040408000404080004040800000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000404
      080004040800ECECFA00ECECFA00ECECFA00ECECFA00ECECFA00040408000404
      0800000000000000000000000000000000000000000000000000000000000404
      080004040800001CEE00001CEE00001CEE00001CEE00001CEE00040408000404
      0800000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000004040800ECEC
      FA00ECECFA00ECECFA00ECECFA00ECECFA00ECECFA00ECECFA00ECECFA00ECEC
      FA0004040800000000000000000000000000000000000000000004040800001C
      EE00001CEE00001CEE00001CEE00001CEE00001CEE00001CEE00001CEE00001C
      EE00040408000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000004040800ECECFA00ECEC
      FA00ECECFA00ECECFA00ECECFA00ECECFA00ECECFA00ECECFA00ECECFA00ECEC
      FA00ECECFA000404080000000000000000000000000004040800001CEE00001C
      EE00001CEE00001CEE00001CEE00001CEE00001CEE00001CEE00001CEE00001C
      EE00001CEE000404080000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000004040800ECECFA00ECEC
      FA00ECECFA00ECECFA00ECECFA00ECECFA00ECECFA00ECECFA00ECECFA00ECEC
      FA00ECECFA000404080000000000000000000000000004040800001CEE00001C
      EE00001CEE00001CEE00001CEE00001CEE00001CEE00001CEE00001CEE00001C
      EE00001CEE000404080000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000004040800ECECFA00ECECFA00ECEC
      FA00ECECFA00ECECFA00ECECFA00ECECFA00ECECFA00ECECFA00ECECFA00ECEC
      FA00ECECFA00ECECFA00040408000000000004040800001CEE00001CEE00001C
      EE00001CEE00001CEE00001CEE00001CEE00001CEE00001CEE00001CEE00001C
      EE00001CEE00001CEE0004040800000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000004040800ECECFA00ECECFA00ECEC
      FA00ECECFA00ECECFA00ECECFA00ECECFA00ECECFA00ECECFA00ECECFA00ECEC
      FA00ECECFA00ECECFA00040408000000000004040800001CEE00001CEE00001C
      EE00001CEE00001CEE00001CEE00001CEE00001CEE00001CEE00001CEE00001C
      EE00001CEE00001CEE0004040800000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000004040800ECECFA00ECECFA00ECEC
      FA00ECECFA00C0C0C000C0C0C000C0C0C000ECECFA00ECECFA00ECECFA00ECEC
      FA00ECECFA00ECECFA00040408000000000004040800001CEE00001CEE00001C
      EE00001CEE0068F8F00068F8F00068F8F000001CEE00001CEE00001CEE00001C
      EE00001CEE00001CEE0004040800000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000004040800ECECFA00ECECFA00ECEC
      FA00C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000ECECFA00ECECFA00ECEC
      FA00ECECFA00ECECFA00040408000000000004040800001CEE00001CEE00001C
      EE0068F8F00068F8F00068F8F00068F8F00068F8F000001CEE00001CEE00001C
      EE00001CEE00001CEE0004040800000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000004040800ECECFA00ECECFA00ECEC
      FA00C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000ECECFA00ECECFA00ECEC
      FA00ECECFA00ECECFA00040408000000000004040800001CEE00001CEE00001C
      EE0068F8F00068F8F00068F8F00068F8F00068F8F000001CEE00001CEE00001C
      EE00001CEE00001CEE0004040800000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000004040800ECECFA00ECEC
      FA00C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000ECECFA00ECECFA00ECEC
      FA00ECECFA000404080000000000000000000000000004040800001CEE00001C
      EE0068F8F00068F8F00068F8F00068F8F00068F8F000001CEE00001CEE00001C
      EE00001CEE000404080000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000004040800ECECFA00ECEC
      FA00ECECFA00C0C0C000C0C0C000C0C0C000ECECFA00ECECFA00ECECFA00ECEC
      FA00ECECFA000404080000000000000000000000000004040800001CEE00001C
      EE00001CEE0068F8F00068F8F00068F8F000001CEE00001CEE00001CEE00001C
      EE00001CEE000404080000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000004040800ECEC
      FA00ECECFA00ECECFA00ECECFA00ECECFA00ECECFA00ECECFA00ECECFA00ECEC
      FA0004040800000000000000000000000000000000000000000004040800001C
      EE00001CEE00001CEE00001CEE00001CEE00001CEE00001CEE00001CEE00001C
      EE00040408000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000404
      080004040800ECECFA00ECECFA00ECECFA00ECECFA00ECECFA00040408000404
      0800000000000000000000000000000000000000000000000000000000000404
      080004040800001CEE00001CEE00001CEE00001CEE00001CEE00040408000404
      0800000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000404080004040800040408000404080004040800000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000404080004040800040408000404080004040800000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000100000000100010000000000800000000000000000000000
      000000000000000000000000FFFFFF00F83FF83F00000000E00FE00F00000000
      C007C00700000000800380030000000080038003000000000001000100000000
      0001000100000000000100010000000000010001000000000001000100000000
      80038003000000008003800300000000C007C00700000000E00FE00F00000000
      F83FF83F00000000FFFFFFFF0000000000000000000000000000000000000000
      000000000000}
  end
end
