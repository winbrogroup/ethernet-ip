object GeneralEntryForm: TGeneralEntryForm
  Left = 0
  Top = 0
  Width = 488
  Height = 281
  Caption = 'NC Dialogue'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -16
  Font.Name = 'Tahoma'
  Font.Style = [fsBold]
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 19
  object lblCaption: TLabel
    Left = 0
    Top = 0
    Width = 480
    Height = 112
    Align = alClient
    Alignment = taCenter
    AutoSize = False
    Caption = 'Please enter the pass number'
    WordWrap = True
  end
  object Panel1: TPanel
    Left = 0
    Top = 112
    Width = 480
    Height = 135
    Align = alBottom
    TabOrder = 0
    object BitBtn2: TBitBtn
      Left = 208
      Top = 79
      Width = 100
      Height = 40
      TabOrder = 0
      Kind = bkCancel
    end
    object BitBtn1: TBitBtn
      Left = 64
      Top = 79
      Width = 100
      Height = 40
      TabOrder = 1
      Kind = bkOK
    end
    object Edit1: TEdit
      Left = 80
      Top = 24
      Width = 321
      Height = 27
      TabOrder = 2
    end
  end
end
