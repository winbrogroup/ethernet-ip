unit GeneralOperatorDialogue;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls, AbstractNCOperatorDialogue, INCInterface,
  NamedPlugin, Main, CncTypes;

type
  {* General Entry Form - needs prettying up *}
  TGeneralEntryForm = class(TForm)
    lblCaption: TLabel;
    Panel1: TPanel;
    BitBtn2: TBitBtn;
    BitBtn1: TBitBtn;
    Edit1: TEdit;
  private
  public
  end;

  TGeneralNumericDialogue = class(TAbstractNCOperatorDialogue)
    class function Execute(Item : string) : TNCDialogueResult; override;
    class procedure Close; override;
  end;

var
  GeneralEntryForm: TGeneralEntryForm;

implementation

{$R *.dfm}

{ TGeneralNumericDialogue }

class procedure TGeneralNumericDialogue.Close;
begin
  if Assigned(GeneralEntryForm) then
    GeneralEntryForm.ModalResult := mrCancel;
end;

class function TGeneralNumericDialogue.Execute(Item: string): TNCDialogueResult;
begin
  GeneralEntryForm:= TGeneralEntryForm.Create(nil);
  try
    GeneralEntryForm.lblCaption.Caption := Item;
    case GeneralEntryForm.ShowModal of
      mrOK : begin
        Result := ncdrContinue;
        Main.SetValue(GeneralEntryForm.Edit1.Text);
      end;
    else
      Result := ncdrStop;
      Main.SetValue('');
    end;
  finally
    GeneralEntryForm.Free;
    GeneralEntryForm:= nil;
  end;
end;

initialization
  TAbstractNCOperatorDialogue.AddPlugin('General', TGeneralNumericDialogue);
end.
