unit NumericOperatorDialogue;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, AbstractNCOperatorDialogue, INCInterface,  NamedPlugin,
  CncTypes, Buttons, Main;


type
  (* Numeric entry form - needs prettification *)
  TNumericEntry = class(TForm)
    Edit1: TEdit;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    lblCaption: TLabel;
  private
  public
  end;

  TSimpleNumericDialogue = class(TAbstractNCOperatorDialogue)
    class function Execute(Item : string) : TNCDialogueResult; override;
    class procedure Close; override;
  end;

var
  NumericEntry: TNumericEntry;

implementation

{$R *.dfm}

{ TSimpleNumericDialogue }

class procedure TSimpleNumericDialogue.Close;
begin
  if Assigned(NumericEntry) then
    NumericEntry.ModalResult := mrCancel;
end;

class function TSimpleNumericDialogue.Execute(
  Item: string): TNCDialogueResult;
begin
  NumericEntry:= TNumericEntry.Create(nil);
  try
    NumericEntry.lblCaption.Caption := Item;
    case NumericEntry.ShowModal of
      mrOK : begin
        Result := ncdrContinue;
        Main.SetValue(IntToStr(StrToIntDef(NumericEntry.Edit1.Text, 0)));
      end;
    else
      Result := ncdrStop;
      Main.SetValue('0');
    end;
  finally
    NumericEntry.Free;
    NumericEntry:= nil;
  end;
end;

initialization
  TAbstractNCOperatorDialogue.AddPlugin('Numeric', TSimpleNumericDialogue);
end.
