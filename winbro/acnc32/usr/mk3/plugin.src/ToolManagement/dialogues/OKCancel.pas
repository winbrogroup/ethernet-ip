unit OKCancel;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, AbstractNCOperatorDialogue, INCInterface,
  NamedPlugin, Main, CncTypes, StdCtrls, Buttons, ExtCtrls;

type
  TOKCancelForm = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    Label1: TLabel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
  private
  public
  end;

  TYesNoDialogue = class(TAbstractNCOperatorDialogue)
    class function Execute(Item : string) : TNCDialogueResult; override;
    class procedure Close; override;
  end;


var
  OKCancelForm: TOKCancelForm;

implementation

{$R *.dfm}

{ TYesNoDialogue }

class procedure TYesNoDialogue.Close;
begin
  if Assigned(OKCancelForm) then
    OKCancelForm.ModalResult:= mrCancel;
end;

class function TYesNoDialogue.Execute(Item: string): TNCDialogueResult;
begin
  OKCancelForm:= TOKCancelForm.Create(nil);
  try
    OKCancelForm.Label1.caption:= Item;
    if OKCancelForm.ShowModal = mrYes then
      Main.SetValue('1')
    else
      Main.SetValue('0');
  finally
    OKCancelForm.Free;
    OKCancelForm:= nil;
  end;
  Result:= ncdrContinue;
end;

initialization
  TAbstractNCOperatorDialogue.AddPlugin('OkCancel', TYesNoDialogue);
end.
