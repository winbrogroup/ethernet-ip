object OKCancelForm: TOKCancelForm
  Left = 0
  Top = 0
  Width = 488
  Height = 281
  Caption = 'NC Dialogue'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -16
  Font.Name = 'Verdana'
  Font.Style = [fsBold]
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 18
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 480
    Height = 169
    Align = alTop
    Caption = ' '
    TabOrder = 0
    object Label1: TLabel
      Left = 1
      Top = 1
      Width = 478
      Height = 167
      Align = alClient
      Alignment = taCenter
      Caption = 'Ok?'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -21
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Layout = tlCenter
      WordWrap = True
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 169
    Width = 480
    Height = 78
    Align = alClient
    Caption = ' '
    TabOrder = 1
    object BitBtn1: TBitBtn
      Left = 128
      Top = 22
      Width = 100
      Height = 40
      Caption = 'OK'
      Default = True
      ModalResult = 6
      TabOrder = 0
      Glyph.Data = {
        DE010000424DDE01000000000000760000002800000024000000120000000100
        0400000000006801000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        3333333333333333333333330000333333333333333333333333F33333333333
        00003333344333333333333333388F3333333333000033334224333333333333
        338338F3333333330000333422224333333333333833338F3333333300003342
        222224333333333383333338F3333333000034222A22224333333338F338F333
        8F33333300003222A3A2224333333338F3838F338F33333300003A2A333A2224
        33333338F83338F338F33333000033A33333A222433333338333338F338F3333
        0000333333333A222433333333333338F338F33300003333333333A222433333
        333333338F338F33000033333333333A222433333333333338F338F300003333
        33333333A222433333333333338F338F00003333333333333A22433333333333
        3338F38F000033333333333333A223333333333333338F830000333333333333
        333A333333333333333338330000333333333333333333333333333333333333
        0000}
      NumGlyphs = 2
    end
    object BitBtn2: TBitBtn
      Left = 264
      Top = 22
      Width = 100
      Height = 40
      TabOrder = 1
      Kind = bkCancel
    end
  end
end
