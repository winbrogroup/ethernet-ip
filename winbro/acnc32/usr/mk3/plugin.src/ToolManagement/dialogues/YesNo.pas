unit YesNo;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, AbstractNCOperatorDialogue, INCInterface,
  NamedPlugin, Main, CncTypes, StdCtrls, Buttons, ExtCtrls;

type
  TYesNoForm = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    Label1: TLabel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
  private
  public
  end;

  TYesNoDialogue = class(TAbstractNCOperatorDialogue)
    class function Execute(Item : string) : TNCDialogueResult; override;
    class procedure Close; override;
  end;


var
  YesNoForm: TYesNoForm;

implementation

{$R *.dfm}

{ TYesNoDialogue }

class procedure TYesNoDialogue.Close;
begin
  if Assigned(YesNoForm) then
    YesNoForm.ModalResult:= mrCancel;
end;

class function TYesNoDialogue.Execute(Item: string): TNCDialogueResult;
begin
  YesNoForm:= TYesNoForm.Create(nil);
  try
    YesNoForm.Label1.caption:= Item;
    if YesNoForm.ShowModal = mrYes then
      Main.SetValue('1')
    else
      Main.SetValue('0');
  finally
    YesNoForm.Free;
    YesNoForm:= nil;
  end;
  Result:= ncdrContinue;
end;

initialization
  TAbstractNCOperatorDialogue.AddPlugin('YesNo', TYesNoDialogue);
end.
