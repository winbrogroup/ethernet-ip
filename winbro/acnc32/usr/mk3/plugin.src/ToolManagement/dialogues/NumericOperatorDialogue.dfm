object NumericEntry: TNumericEntry
  Left = 315
  Top = 241
  Width = 343
  Height = 235
  Caption = 'NC Dialogue'
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -16
  Font.Name = 'Verdana'
  Font.Style = [fsBold]
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 18
  object lblCaption: TLabel
    Left = 0
    Top = 0
    Width = 335
    Height = 97
    Align = alTop
    Alignment = taCenter
    AutoSize = False
    Caption = 'Please enter the pass number'
    WordWrap = True
  end
  object Edit1: TEdit
    Left = 96
    Top = 104
    Width = 121
    Height = 26
    TabOrder = 0
    Text = '0'
  end
  object BitBtn1: TBitBtn
    Left = 32
    Top = 152
    Width = 100
    Height = 40
    TabOrder = 1
    Kind = bkOK
  end
  object BitBtn2: TBitBtn
    Left = 168
    Top = 152
    Width = 100
    Height = 40
    TabOrder = 2
    Kind = bkCancel
  end
end
