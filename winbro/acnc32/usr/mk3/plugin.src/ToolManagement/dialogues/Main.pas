unit Main;

interface

uses
  SysUtils, Classes, IFormInterface, INamedInterface, NamedPlugin, AbstractPLCPlugin;

type
  TMain = class(TAbstractPlcPlugin)
    class procedure Install; override;
  end;

  procedure SetValue(const Value: string);

implementation

var
  StringTag: TTagRef;

procedure SetValue(const Value: string);
begin
  Named.SetAsString(StringTag, PChar(Value));
end;

{ TMain }

class procedure TMain.Install;
begin
  StringTag:= Named.AddTag('DialogueValue', 'TStringTag');
end;

initialization
  TAbstractPlcPlugin.AddPlugin(' _@_ ', TMain);
end.
