unit GeneralOCT;
interface
uses Classes, SysUtils, AbstractOCTPlugin, NamedPlugin, Windows, IOCT,
     IToolLife, StreamTokenizer, OCTOperatorDisplay, INamedInterface, CNCTypes, ExtCtrls,
     TypInfo, DGSOCTTypes, FileCtrl, IniFiles, INCInterface,OCT;

// Todo
// Buffered programs
//-- attempting to load a missing program
//-- List index out of bounds on part load
//-- String array for Euchner data
// plcs 32-48 require revisiting
// CMM file loading.
// Euchner tag reader writer.
//-- plc interlock for cycle start in cell mode.
//-- OEE PB to active plc which requests OEE.

type
  TOCTRunStage = (
    orsWaitRun,
    orsWaitInCycle,
    orsWaitEndProgram,
    orsEndCheckDelay,
    orsEndProcessing,
    orsWaitRecoveryOptions,
    orsWaitRestartSingleToContinuous,
    orsWaitReworkComplete
  );

  TOCTError = (
    oeWatchdog
  );
  TOCTErrors = set of TOCTError;

  TOCTReportingTags = (
    ortPartSFC,
    ortPartType,
    ortShuttleID,
    ortOCTReport,
    ortOPPReport,
    ortBuffered,
    ortOCTTimeRemaining,
    ortOCTTime,
    ortOPPTimeRemaining,
    ortOPPTime,
    ortOPPIndex,
    ortTotalOPPs,
    ortCycleTime,
    ortResetComplete,
    ortOpenToolDoor,
    ortOpenMainDoor,
    ortReqToolStation1,
    ortReqToolStation2,
    ortReqToolStation3,
    ortReqToolStation4,
    ortToolInHead,
    ortToolInStation1,
    ortToolInStation2,
    ortToolInStation3,
    ortToolInStation4,
    ortToolSNInHead,
    ortToolSNInStation1,
    ortToolSNInStation2,
    ortToolSNInStation3,
    ortToolSNInStation4,
    ortToolRemInHead,
    ortToolRemInStation1,
    ortToolRemInStation2,
    ortToolRemInStation3,
    ortToolRemInStation4,
    ortToolInHeadReport,
    ortToolInStation1Report,
    ortToolInStation2Report,
    ortToolInStation3Report,
    ortToolInStation4Report,
    ortToolDoorLocked,
    ortOCTError,
    ortProduction,
    ortRework,
    ortPreCMM,
    ortPostCMM,
    ortWatchdog,
    ortCheckDNCPaths,
    ortAdministerAccess,
    ortPartStatusForm,
    ortCMMLocalFileName,
    ortCMMDNCFileName,
    ortAccessLevels,
    ortCMMOffsetsPath,
    ortCMMOffsetsFile,
    ortRefreshBuffered,
    ortRunningBuffered,
    ortToolAtDoor,
    ortToolAtDoorAck,
    ortToolAtDoorString,
    ortToolAtHead,
    ortToolAtHeadAck,
    ortToolAtHeadString,
    ortToolChangerPosition,
    ortToolChangerStable,
    ortToolOffsetX,
    ortToolOffsetY,
    ortToolOffsetZ,
    ortToolDateValidate,
    ortReqOCTSort,
    ortOCTSorted,
    ortStatus1,
    ortStatus2,
    ortToolInHeadBalluff,
    ortToolInStation1Balluff,
    ortToolInStation2Balluff,
    ortToolInStation3Balluff,
    ortToolInStation4Balluff,
    ortToolInPosition1Proximity,
    ortToolInPosition2Proximity,
    ortToolInPosition3Proximity,
    ortToolInPosition4Proximity,
    ortTC_Retracted,
    ortTC_Advanced,
    ortMainDoorLocked,
    ortMainDoorOpenInhibit,
    ortToolDoorOpenInhibit,
    ortWrittenToolStr,
    ortReplaceToolLifeOnWrite,
    ortCyclesRemainingTag
    );

  TGeneralOCT = class(TOCT)
  private
    FRecoveryRequired : TNotifyEvent;
    FRecoveryFinished : TNotifyEvent;
    FOCTStageChanged : TNotifyEvent;
    FOnTagChange : TNamedCallbackEv;

    CheckEndCount : Integer;

    OCTOPPStartTime : TDateTime;
    OCTOPPName : string;
    OPPStartTime : TDateTime;
    LastOPPInCycle : Boolean;
    LastOCTInCycle : Boolean;

    LastOCTStage : TOCTRunStage;
    WasInSingle : Boolean;
    LastRecoveryOption : TRecoceryOptions;

    ReworkOPP : Integer;
    OCTResult : TOCTResultCode;
    OCTInterrupted : Boolean;

    OPPDuration : Double;
    OCTDuration : Double;
    OCTDurationSoFar : Double;
    OPPDurationSoFar : Double;
    OCTStartDuration : Int64;
    OPPStartDuration : Int64;

    WatchDogCount : Integer;
    LastWatchDog : Boolean;

    OCTErrors : TOCTErrors;

    ActiveFileNameTag : TTagRef;
    TagNames : array[TOCTReportingTags] of string;

    AddToolDoorStage : Integer;
    AddToolHeadStage : Integer;

    procedure IncludeOCTError(aError : TOCTError);
    procedure ExcludeOCTError(aError : TOCTError);
    procedure TagChanged(Tag: TTagRef);
    procedure DoRecoveryRequired;
    procedure DoStandAloneRecoveryFinished;
    procedure DoOCTStageChanged;
    procedure UpdateOPPModeReports;
    procedure ToolLifeChanged(Sender : TObject);

    procedure ResetRecoveryTags;
    procedure ReadConfiguration;
    procedure StageToolAddition;
    procedure AddTool(S : string; Ex1 : Integer);
    function BuildToolString(T : TToolLife) : string;
    procedure ReplaceToolAtDoorOnWrite;
  protected

  public
    OCTStage : TOCTRunStage;
    OCTStageText : String;
    SelectedRecoveryOption :   TRecoceryOptions;
    Tags : array [TOCTReportingTags] of TTagRef;

    constructor Create(Host : IFSDOCTHost); override;
    procedure Shutdown; override;
    procedure Install;override;
    procedure FinalInstall; override;
    procedure Reset; override;
    function NextOPP: Boolean; override;
    function FirstOPP: Integer; override;

    procedure Update(Status : TOCTPluginStatii); override;
    procedure ResetDisplayParameters;
    function OPPsEnabled: Integer;
    function AllEnabled : Boolean;

    procedure EnableAllOPPs;
    procedure InvertSelection;
    procedure RecoverCellOCT;
    procedure WriteOCTOPPStartString;
    procedure WriteOCTOPPEndString;
    procedure WriteOPPStartString;
    procedure WriteOPPEndString;
    procedure CheckCMMFile;
    function CreateReportString(OPPMode : Boolean) : string;

    property OnRecoveryRequired : TNotifyEvent read FRecoveryRequired write FRecoveryRequired;
    property OnRecoveryFinished : TNotifyEvent read FRecoveryFinished write FRecoveryFinished;
    property OnOCTStageChanged: TNotifyEvent read FOCTStageChanged write FOCTStageChanged;
    property OnTagChange : TNamedCallbackEv read FOnTagChange write FOnTagChange;
  end;

var
  aOCT : TGeneralOCT;

implementation

uses CellMode, ReworkStatus, CheckDNCPaths, ToolManagement, AccessAdministration,
     FileLoadDialog, PartStatusForm, ToolOKDlg;

type
  TTagDefinitions = record
    Ini : string;
    Tag : string;
    Owned : Boolean;
    TagType : string;
  end;


const
  TagDefinitions : array [TOCTReportingTags] of TTagDefinitions = (
     ( Ini : 'PartSFC'; Tag : 'PartSFC'; Owned : True; TagType : 'TStringTag'),
     ( Ini : 'PartType'; Tag : 'PartName'; Owned : True; TagType : 'TStringTag' ),
     ( Ini : 'PartShuttleID'; Tag : 'PartShuttleID'; Owned : True; TagType : 'TStringTag' ),
     ( Ini : 'OCTReport'; Tag : 'CM_OCTmodeOPPreport'; Owned : True; TagType : 'TStringTag' ),
     ( Ini : 'OPPReport'; Tag : 'CM_OPPmodeOPPreport'; Owned : True; TagType : 'TStringTag' ),
     ( Ini : 'BufferedPrograms'; Tag : 'CM_BufferedPrograms'; Owned : True; TagType : 'TMethodTag' ),
     ( Ini : 'OCTTimeRemaining'; Tag : 'CM_OCTTimeRemaining'; Owned : True; TagType : 'TDoubleTag' ),
     ( Ini : 'OCTCycleTime'; Tag : 'CM_OCTCycleTime'; Owned : True; TagType : 'TDoubleTag' ),
     ( Ini : 'OPPTimeRemaining'; Tag : 'CM_OPPTimeRemaining'; Owned : True; TagType : 'TDoubleTag' ),
     ( Ini : 'OPPCycleTime'; Tag : 'CM_OPPCycleTime'; Owned : True; TagType : 'TDoubleTag' ),
     ( Ini : 'OPPIndex'; Tag : 'CM_OPPIndex'; Owned : True; TagType : 'TIntegerTag' ),
     ( Ini : 'TotalOPPs'; Tag : 'CM_TotalOPPs'; Owned : True; TagType : 'TIntegerTag' ),
     ( Ini : 'CycleTime'; Tag : 'ACNCTimeRunning'; Owned : False; TagType : 'TIntegerTag' ),
     ( Ini : 'ResetComplete'; Tag : 'ResetComplete'; Owned : False; TagType : 'TMethodTag' ),
     ( Ini : 'ReqOpenToolDoor'; Tag : 'OCT_ReqOpenToolDoor'; Owned : True; TagType : 'TMethodTag' ),
     ( Ini : 'ReqOpenMainDoor'; Tag : 'OCT_ReqOpenMainDoor'; Owned : True; TagType : 'TMethodTag' ),
     ( Ini : 'ReqToolStation1'; Tag : 'OCT_ReqToolStation1'; Owned : True; TagType : 'TMethodTag' ),
     ( Ini : 'ReqToolStation2'; Tag : 'OCT_ReqToolStation2'; Owned : True; TagType : 'TMethodTag' ),
     ( Ini : 'ReqToolStation3'; Tag : 'OCT_ReqToolStation3'; Owned : True; TagType : 'TMethodTag' ),
     ( Ini : 'ReqToolStation4'; Tag : 'OCT_ReqToolStation4'; Owned : True; TagType : 'TMethodTag' ),
     ( Ini : 'ToolInHead'; Tag : 'ToolInHead'; Owned : True; TagType : 'TStringTag' ),
     ( Ini : 'ToolInStation1'; Tag : 'ToolInStation1'; Owned : True; TagType : 'TStringTag' ),
     ( Ini : 'ToolInStation2'; Tag : 'ToolInStation2'; Owned : True; TagType : 'TStringTag' ),
     ( Ini : 'ToolInStation3'; Tag : 'ToolInStation3'; Owned : True; TagType : 'TStringTag' ),
     ( Ini : 'ToolInStation4'; Tag : 'ToolInStation4'; Owned : True; TagType : 'TStringTag' ),
     ( Ini : 'ToolSNInHead'; Tag : 'ToolSNInHead'; Owned : True; TagType : 'TIntegerTag' ),
     ( Ini : 'ToolSNInStation1'; Tag : 'ToolSNInStation1'; Owned : True; TagType : 'TIntegerTag' ),
     ( Ini : 'ToolSNInStation2'; Tag : 'ToolSNInStation2'; Owned : True; TagType : 'TIntegerTag' ),
     ( Ini : 'ToolSNInStation3'; Tag : 'ToolSNInStation3'; Owned : True; TagType : 'TIntegerTag' ),
     ( Ini : 'ToolSNInStation4'; Tag : 'ToolSNInStation4'; Owned : True; TagType : 'TIntegerTag' ),
     ( Ini : 'ToolRemInHead'; Tag : 'ToolRemInHead'; Owned : True; TagType : 'TDoubleTag' ),
     ( Ini : 'ToolRemInStation1'; Tag : 'ToolRemInStation1'; Owned : True; TagType : 'TDoubleTag' ),
     ( Ini : 'ToolRemInStation2'; Tag : 'ToolRemInStation2'; Owned : True; TagType : 'TDoubleTag' ),
     ( Ini : 'ToolRemInStation3'; Tag : 'ToolRemInStation3'; Owned : True; TagType : 'TDoubleTag' ),
     ( Ini : 'ToolRemInStation4'; Tag : 'ToolRemInStation4'; Owned : True; TagType : 'TDoubleTag' ),
     ( Ini : 'ToolInHeadReport'; Tag : 'CM_ToolInHead'; Owned : True; TagType : 'TStringTag' ),
     ( Ini : 'ToolInStation1Report'; Tag : 'CM_ToolStore1'; Owned : True; TagType : 'TStringTag' ),
     ( Ini : 'ToolInStation2Report'; Tag : 'CM_ToolStore2'; Owned : True; TagType : 'TStringTag' ),
     ( Ini : 'ToolInStation3Report'; Tag : 'CM_ToolStore3'; Owned : True; TagType : 'TStringTag' ),
     ( Ini : 'ToolInStation4Report'; Tag : 'CM_ToolStore4'; Owned : True; TagType : 'TStringTag' ),
     ( Ini : 'ToolDoor'; Tag : 'CSC_TOOL_DOOR_LOCKED'; Owned : False; TagType : 'TMethodTag' ),
     ( Ini : 'Error'; Tag : 'OCT_Error'; Owned : True; TagType : 'TIntegerTag' ),
     ( Ini : 'Production'; Tag : 'OCT_Production'; Owned : True; TagType : 'TMethodTag' ),
     ( Ini : 'Rework'; Tag : 'OCT_Rework'; Owned : True; TagType : 'TMethodTag' ),
     ( Ini : 'PreCMM'; Tag : 'OCT_PreCMM'; Owned : True; TagType : 'TMethodTag' ),
     ( Ini : 'PostCMM'; Tag : 'OCT_PostCMM'; Owned : True; TagType : 'TMethodTag' ),
     ( Ini : 'Watchdog'; Tag : 'CB_Watchdog'; Owned : True; TagType : 'TMethodTag' ),
     ( Ini : 'CheckDNCPaths'; Tag : 'OCT_CheckDNCPaths'; Owned : False; TagType : 'TMethodTag' ),
     ( Ini : 'AdministerAccess'; Tag : 'OCT_AdministerAccess'; Owned : False; TagType : 'TMethodTag' ),
     ( Ini : 'PartStatusForm'; Tag : 'OCT_PartStatus'; Owned : False; TagType : 'TMethodTag' ),
     ( Ini : 'CMMDNCFileName'; Tag : 'CMMDNCFileName'; Owned : True; TagType : 'TStringTag' ),
     ( Ini : 'CMMLocalFileName'; Tag : 'CMMLocalFileName'; Owned : True; TagType : 'TStringTag' ),
     ( Ini : 'AccessLevels'; Tag : 'AccessLevels'; Owned : False; TagType : 'TIntegerTag' ),
     ( Ini : 'CmmOffsetsPath'; Tag : 'CMMOffsetsPath'; Owned : True; TagType : 'TStringTag' ),
     ( Ini : 'CmmOffsetsFile'; Tag : 'CMMOffsetsFile'; Owned : True; TagType : 'TStringTag' ),
     ( Ini : 'RefreshBuffered'; Tag : 'OCT_RefreshBuffered'; Owned : False; TagType : 'TMethodTag' ),
     ( Ini : 'RunningBuffered'; Tag : 'OCT_RunningBuffered'; Owned : True; TagType : 'TMethodTag' ),
     ( Ini : 'ToolAtDoor'; Tag : 'TagNewData1_2'; Owned : False; TagType : 'TMethodTag' ),
     ( Ini : 'ToolAtDoorAck'; Tag : 'TagNewData1_2Ack'; Owned : True; TagType : 'TMethodTag' ),
     ( Ini : 'ToolAtDoorString'; Tag : 'BalluffRead_ID0H2'; Owned : False; TagType : 'TStringTag' ),
     ( Ini : 'ToolAtHead'; Tag : 'TagNewData1_1'; Owned : False; TagType : 'TMethodTag' ),
     ( Ini : 'ToolAtHeadAck'; Tag : 'TagNewData1_1Ack'; Owned : True; TagType : 'TMethodTag' ),
     ( Ini : 'ToolAtHeadString'; Tag : 'BalluffRead_ID0H1'; Owned : False; TagType : 'TStringTag' ),
     ( Ini : 'ToolChangerPosition'; Tag : 'TC_POSITION'; Owned : False; TagType : 'TIntegerTag' ),
     ( Ini : 'ToolChangerStable'; Tag : 'TC_PositionStable'; Owned : False; TagType : 'TMethodTag' ),
     ( Ini : 'ToolOffsetX'; Tag : 'UniqueToolOffsetX'; Owned : True; TagType : 'TDoubleTag' ),
     ( Ini : 'ToolOffsetY'; Tag : 'UniqueToolOffsetY'; Owned : True; TagType : 'TDoubleTag' ),
     ( Ini : 'ToolOffsetZ'; Tag : 'UniqueToolOffsetZ'; Owned : True; TagType : 'TDoubleTag' ),
     ( Ini : 'ToolDateValidate'; Tag : 'MTB5'; Owned : False; TagType : 'TIntegerTag' ),
     ( Ini : 'ReqOCTSort'; Tag : 'OCTReqPLCSort'; Owned : False; TagType : 'TIntegerTag' ),
     ( Ini : 'OCTSorted'; Tag : 'OCTPLCSorted'; Owned : True; TagType : 'TIntegerTag' ),
     ( Ini : 'Status1'; Tag : 'OCTStatus1'; Owned : False; TagType : 'TStringTag' ),
     ( Ini : 'Status2'; Tag : 'OCTStatus2'; Owned : False; TagType : 'TStringTag' ),
     ( Ini : 'ToolInHeadBalluff'; Tag : 'ToolInHeadBalluff'; Owned : True; TagType : 'TStringTag' ),
     ( Ini : 'ToolInStation1Balluff'; Tag : 'ToolInStation1Balluff'; Owned : True; TagType : 'TStringTag' ),
     ( Ini : 'ToolInStation2Balluff'; Tag : 'ToolInStation2Balluff'; Owned : True; TagType : 'TStringTag' ),
     ( Ini : 'ToolInStation3Balluff'; Tag : 'ToolInStation3Balluff'; Owned : True; TagType : 'TStringTag' ),
     ( Ini : 'ToolInStation4Balluff'; Tag : 'ToolInStation4Balluff'; Owned : True; TagType : 'TStringTag' ),
     ( Ini : 'ToolInPosition1Proximity'; Tag : 'I_ToolPresentPosn1'; Owned : False; TagType : 'TIntegerTag' ),
     ( Ini : 'ToolInPosition2Proximity'; Tag : 'I_ToolPresentPosn2'; Owned : False; TagType : 'TIntegerTag' ),
     ( Ini : 'ToolInPosition3Proximity'; Tag : 'I_ToolPresentPosn3'; Owned : False; TagType : 'TIntegerTag' ),
     ( Ini : 'ToolInPosition4Proximity'; Tag : 'I_ToolPresentPosn4'; Owned : False; TagType : 'TIntegerTag' ),
     ( Ini : 'ToolChangerRetracted'; Tag : 'CSC_TOOLCHANGER_RETRACTED'; Owned : True; TagType : 'TIntegerTag' ),
     ( Ini : 'ToolChangerAdvanced'; Tag : 'CSC_TOOLCHANGER_ADVANCED'; Owned : True; TagType : 'TIntegerTag' ),
     ( Ini : 'MainDoorIsLocked'; Tag : 'MainDoorLocked'; Owned : False; TagType : 'TIntegerTag' ),
     ( Ini : 'MainDoorOpenInhibit'; Tag : 'MainDoorOpenInhibit'; Owned : False; TagType : 'TIntegerTag' ),
     ( Ini : 'ToolDoorOpenInhibit'; Tag : 'ToolDoorOpenInhibit'; Owned : False; TagType : 'TIntegerTag' ),
     ( Ini : 'WrittenToolStr'; Tag : 'BalluffWrite_ID0H2'; Owned : False; TagType : 'TStringTag' ),
     ( Ini : 'ReplaceToolOnWrite'; Tag : 'BalluffReplaceToolLifeonWrite'; Owned : False; TagType : 'TMethodTag' ),
     ( Ini : 'CyclesRemainingTag'; Tag : 'TOOLLIFE_TAKT_STATUS'; Owned : False; TagType : 'TStringTag' )
  );

resourcestring
  ToolTooOld = 'Tool time-stamp is older than permitted, the tool life for this tool has been zeroed, please recheck the tool at the tool set test station';

procedure TagChangeHandler(Tag : TTagRef); stdcall
begin
  OCT.TagChanged(Tag);
end;


{ TGeneralOCT }

procedure TGeneralOCT.WriteOCTOPPStartString;
begin
  OCTOPPStartTime := Now;
  OCTOPPName := OPP[ActiveOPP].FileName;
end;

procedure TGeneralOCT.WriteOPPStartString;
begin
  OPPStartTime := Now;
end;


procedure TGeneralOCT.WriteOCTOPPEndString;
begin
  Named.SetAsString(Tags[ortOCTReport], PChar(CreateReportString(False)));
  OCTResult := orcComplete;
end;

procedure TGeneralOCT.WriteOPPEndString;
begin
  Named.SetAsString(Tags[ortOPPReport], PChar(CreateReportString(True)));
end;


function TGeneralOCT.CreateReportString(OPPMode : Boolean) : string;
var SN, Interrupted, SigWarning, SigFailure, OCTResultI : Integer;
    ToolType : WideString;
    ToolRemaining : Double;
    TmpOCTFileName, StartTime : string;
    PartSFC, PartType, ShuttleID, ToolTypeS : string;
    Buffer : array [0..1023] of Char;
    TmpOCTOPPName : string;
begin
  ToolLife.GetEx1ToolSN(0, SN);
  ToolLife.GetEx1ToolType(0, ToolType);
  ToolLife.GetRemaining(ToolType, SN, ToolRemaining);
  ToolTypeS := ToolType;

  Named.GetAsString(Tags[ortPartSFC], Buffer, 1024);
  PartSFC := Buffer;
  Named.GetAsString(Tags[ortPartType], Buffer, 1024);
  PartType := Buffer;
  Named.GetAsString(Tags[ortShuttleID], Buffer, 1024);
  ShuttleID := Buffer;

  TmpOCTFileName := '';
  Interrupted := 0;
  SigWarning := 0;
  SigFailure := 0;
  OCTResultI := 0;

  if not OPPMode then begin
    TmpOCTFileName := OCTFileName;
    StartTime := RRDateFormat(OCTOPPStartTime);
    OCTResultI := Ord(OCTResult);
    Interrupted := Ord(OCTInterrupted);
    TmpOCTOPPName := OCTOPPName;
  end else begin
    StartTime := RRDateFormat(OPPStartTime);
    Named.GetAsString(ActiveFileNameTag, Buffer, 1024);
    TmpOCTOPPName := ExtractFileName(Buffer);
  end;


  Result := Format('%s,%s,%s,%s,%s,%s,%s,%s,%d,%f,%d,%d,%d,%d,%d', [
                      StartTime,
                      RRDateFormat(Now),
                      PartSFC,
                      PartType,
                      TmpOCTFileName,
                      TmpOCTOPPName,
                      ShuttleID,
                      ToolType,
                      SN,
                      ToolRemaining,
                      Ord(CellMode.LocalCellMode),
                      Interrupted,
                      SigWarning,
                      SigFailure,
                      OCTResultI ]);

end;

function TGeneralOCT.FirstOPP : Integer;
begin
  Result := inherited FirstOPP;
  Named.SetAsDouble(Tags[ortOPPTime], OPP[ActiveOPP].TotalTime);
  Named.SetAsInteger(Tags[ortOPPIndex], ActiveOPP);
  Named.SetAsInteger(Tags[ortTotalOPPs], OPPCount);
  OPPDuration := 0;
  OPPDurationSoFar := 0;
end;

function TGeneralOCT.NextOPP : Boolean;
begin
  Result := inherited NextOPP;
  Named.SetAsDouble(Tags[ortOPPTime], OPP[ActiveOPP].TotalTime);
  Named.SetAsInteger(Tags[ortOPPIndex], ActiveOPP);
  Named.SetAsInteger(Tags[ortTotalOPPs], OPPCount);
  OPPDuration := 0;
  OPPDurationSoFar := 0;
end;


procedure TGeneralOCT.Reset;
begin
//  DoOCTSort;
  inherited;
  WasInSingle := False;
  RunEnable := False;

  OCTStage := orsWaitRun;

  Named.SetAsDouble(Tags[ortOCTTime], TotalOCTTime);
  OCTDuration := 0;
  OCTDurationSoFar := 0;
end;

procedure TGeneralOCT.ResetRecoveryTags;
begin
end;

procedure TGeneralOCT.StageToolAddition;
var Buffer : array [0..1024] of Char;
begin
  case AddToolDoorStage of
    0 : if Named.GetAsBoolean(Tags[ortToolAtDoor]) then
           begin
           Named.GetAsString(Tags[ortToolAtDoorString], Buffer, 1024);
           Named.SetAsBoolean(Tags[ortToolAtDoorAck], True);
           AddTool(Buffer, Named.GetAsInteger(Tags[ortToolChangerPosition]));
           AddToolDoorStage := 1;
           end
    else
           begin
           /// Write Toolremove and add code here
           // Handshake required with Write dialogue
           // Tag required to say that tooldata has been written to Balluff
           // if there was a tool in the door position its data must be removed
           // then new tool data added to tool life
           // Tag to say tool life has been updated must then be written

           end;
    1 : if not Named.GetAsBoolean(Tags[ortToolAtDoor]) then begin
      Named.SetAsBoolean(Tags[ortToolAtDoorAck], False);
      AddToolDoorStage := 0;
    end;
  end;

  case AddToolHeadStage of
    0 : if Named.GetAsBoolean(Tags[ortToolAtHead]) then begin
          Named.GetAsString(Tags[ortToolAtHeadString], Buffer, 1024);
          Named.SetAsBoolean(Tags[ortToolAtHeadAck], True);
          AddTool(Buffer, 0);
          AddToolHeadStage := 1;
    end;
    1 : if not Named.GetAsBoolean(Tags[ortToolAtHead]) then begin
      Named.SetAsBoolean(Tags[ortToolAtHeadAck], False);
      AddToolHeadStage := 0;
    end;
  end;
end;

//------ original format
// Tool string
// Tooltype
// SN
// Total
// Actual
// Date
// RR
// Unix
// Uniy
// Uniz


procedure TGeneralOCT.AddTool(S : string; Ex1 : Integer);
var ToolType : WideString;
    SN : Integer;
    Total : Double;
    Actual : Double;
    Minimum : Double;
    Date : Double;
    UniX, UniY, UniZ : Double;
    T : TToolLife;
begin
  try
    if s = '' then
       begin
       Named.SetFaultA('OCT', 'Null Tool Tag data', FaultInterlock, nil);
       OffsToolLife.LocalAddTool('INVALID', 0, 0, 0, Ex1, 0);
       exit;
       end;
    ToolType := ReadDelimited(S, ',');
    SN := StrToInt(ReadDelimited(S, ','));
    Total := StrToFloat(ReadDelimited(S, ','));
    Actual := StrToFloat(ReadDelimited(S, ','));
    Date := StrToFloat(ReadDelimited(S, ','));
    Minimum := StrToFloat(ReadDelimited(S, ','));
    if Named.GetAsBoolean(Tags[ortToolDateValidate]) then begin
      if Date < Now then begin
        Actual := 0;
        Named.SetFaultA('OCT', 'Tool requires checking at Tool Test and Set station, its life has been zeroed', FaultInterlock, nil);
      end;
    end else if Actual < (Total - Minimum - 1) then begin
      if TIsToolOK.Execute(Format('%s with remaining of %f ', [ToolType, Actual])) then
        Actual := Total - Minimum;
    end;
    T := OffsToolLife.LocalAddTool(ToolType, SN, Total, Actual, Ex1, 0);
    T.Date := Date;
    T.Minimum := Minimum;

  except
    on E : Exception do begin
      Named.SetFaultA('OCT', 'Tool has bad tag data ' + E.Message + S, FaultInterlock, nil);
      OffsToolLife.LocalAddTool('INVALID', 0, 0, 0, Ex1, 0);
      Exit;
    end;
  end;


  try
    Unix := StrToFloat(ReadDelimited(S, ','));
    Uniy := StrToFloat(ReadDelimited(S, ','));
    Uniz := StrToFloat(ReadDelimited(S, ','));
  except
    Unix := 0;
    Uniy := 0;
    Uniz := 0;
    Named.SetFaultA('OCT', 'Tool offset could not be read from the tool, offsets set to zero', FaultInterlock, nil);
  end;

  T.UniqueX := Unix;
  T.UniqueY := UniY;
  T.UniqueZ := Uniz;
end;

//------ original format
// Tool string
// Tooltype
// SN
// Total
// Actual
// Date
// RR
// Unix
// Uniy
// Uniz

procedure TGeneralOCT.TagChanged(Tag: TTagRef);
begin
  if Tag = Tags[ortResetComplete] then begin
    if Named.GetAsBoolean(Tags[ortResetComplete]) then begin
      Named.SetAsString(Tags[ortStatus1],'');
      Named.SetAsString(Tags[ortStatus2],'');
      case OCTStage of
        orsWaitRestartSingleToContinuous: begin
          OPP[ActiveOPP].Status := osInRecovery;
          DoStatusChanged;
          DoRecoveryRequired;
          OCTStage := orsWaitRecoveryOptions;
        end;

        orsEndProcessing: begin
          if NC.SingleBlock then begin
             //Reset during Single Block Halt
             OPP[ActiveOPP].Status := osInRecovery;
             DoStatusChanged;
             DoRecoveryRequired;
             OCTStage := orsWaitRecoveryOptions;
          end;
        end;
      end
    end;
  end;


  if Tag = Tags[ortCheckDNCPaths] then begin
    if Named.GetAsBoolean(Tag) = False then begin
      TCheckDNCPathForm.Execute;
    end;
  end;

  if Tag = Tags[ortAdministerAccess] then begin
    if Named.GetAsBoolean(Tag) = False then
      TAccessAdministrator.Execute;
  end;

  if Tag = Tags[ortPartStatusForm] then begin
    if Named.GetAsBoolean(Tag) = False then
      TPartStatus.Execute;
  end;

  if Tag = Tags[ortRefreshBuffered] then begin
    if Named.GetAsBoolean(Tag) = True then
      try
        Named.SetAsBoolean( Tags[ortRunningBuffered],
                            FileLoadDialog.RefreshBufferedPrograms);
      except
        on E : Exception do begin
          CellMode.CellModeForm.SetCellMode(rrmStandalone);
          CellMode.CellModeForm.SetCellMode(rrmProduction);
          Named.SetAsBoolean( Tags[ortRunningBuffered], False);
        end;
      end;
  end;

  if Assigned(FOnTagChange) then
    FOnTagChange(Tag);

  if Assigned(ToolForm) and (Tag = OCT.Tags[ortToolChangerPosition]) then begin
    ToolForm.RedrawToolList;
  end;

  // This is a frig. Allows the plc to handshake
  if Tag = Tags[ortReqOCTSort] then begin
    Named.SetAsBoolean(Tags[ortOCTSorted], Named.GetAsBoolean(Tag))
  end;

  if Tag = Tags[ortReplaceToolLifeOnWrite] then begin
     if Named.GetAsBoolean(Tag) then
        begin
        ReplaceToolAtDoorOnWrite;
        Named.SetasBoolean(Tags[ortReplaceToolLifeOnWrite],False);
        end;
     end;
end;

function TGeneralOCT.BuildToolString(T : TToolLife) : string;
begin
  Result := Format('%s,%d,%.3f,%.3f,%.2f,%.3f,%.3f,%.3f,%.3f',
     [T.ToolType, T.SN, T.TotalLife, T.Remaining, T.Date, T.Minimum,
      T.UniqueX, T.UniqueY, T.UniqueZ]);
end;

procedure TGeneralOCT.ToolLifeChanged(Sender : TObject);
var I : Integer;
    ToolType : WideString;
    SN : Integer;
    Remaining : Double;
    TB, SNB, RemB, Rep, BlfStr : TOCTReportingTags;
    TL : TToolLife;
begin
  for I := 0 to 4 do begin
    TB := TOCTReportingTags(Ord(ortToolInHead) + I);
    SNB := TOCTReportingTags(Ord(ortToolSNInHead) + I);
    RemB := TOCTReportingTags(Ord(ortToolRemInHead) + I);
    Rep := TOCTReportingTags(Ord(ortToolInHeadReport) + I);
    BlfStr := TOCTReportingTags(Ord(ortToolInHeadBalluff) + I);
    if ToolLife.GetEx1ToolType(I, ToolType) = trOK then begin
      Named.SetAsString(Tags[TB], PChar(Format('%s', [ToolType])));
      ToolLife.GetEx1ToolSN(I, SN);
      ToolLife.GetRemaining(ToolType, SN, Remaining);
      Named.SetAsInteger(Tags[SNB], SN);
      Named.SetAsDouble(Tags[RemB], Remaining);
      TL := OffsToolLife.FindToolEx1(I);
      if Assigned(TL) then
        Named.SetAsString(Tags[BlfStr], PChar(BuildToolString(TL)));
      if Assigned(ToolForm) then
        Named.SetAsString(Tags[Rep], PChar(Format('%s,%d,%f,%d', [ToolType, SN, Remaining, Trunc(ToolForm.UsagePerTaKt(ToolType))])))
      else
        Named.SetAsString(Tags[Rep], PChar(Format('%s,%d,%f,%d', [ToolType, SN, Remaining, 0])))
    end else begin
      Named.SetAsString(Tags[TB], '');
      Named.SetAsInteger(Tags[SNB], 0);
      Named.SetAsDouble(Tags[RemB], 0);
      Named.SetAsString(Tags[Rep], ',,,');
      Named.SetAsString(Tags[BlfStr], '');
    end;
  end;
end;

constructor TGeneralOCT.Create(Host : IFSDOCTHost);
begin
  inherited;
  OCT := Self;
  WasInSingle := False;
  FOCTActive := False;
  CreateDirectory(PChar(NamedPlugin.DllProfilePath), nil);
  Self.Host := Host;
end;

procedure TGeneralOCT.Install;
var I : TOCTReportingTags;
begin
  inherited;
  ReadConfiguration;
  for I := Low(TOCTReportingTags) to High(TOCTReportingTags) do begin
    if TagDefinitions[I].Owned then
      Tags[I] := Named.AddTag(PChar(TagNames[I]), PChar(TagDefinitions[I].TagType));
  end;
end;

procedure TGeneralOCT.FinalInstall;
var I : TOCTReportingTags;
begin
  inherited;

  ActiveFileNameTag := Named.AquireTag('NewActiveFile', 'TStringTag', nil);
  for I := Low(TOCTReportingTags) to High(TOCTReportingTags) do begin
    if not TagDefinitions[I].Owned then
      Tags[I] := Named.AquireTag(PChar(TagNames[I]), PChar(TagDefinitions[I].TagType), TagChangeHandler);
  end;
  if assigned(OffsToolLife) then
     begin
     OffsToolLife.AddCallback(ToolLifeChanged);
     end;

end;

procedure TGeneralOCT.ReadConfiguration;
var I : TOCTReportingTags;
    Ini : TIniFile;
begin
  Ini := TIniFile.Create(DllProfilePath + InstallName + '.cfg');
  try
    for I := Low(TOCTReportingTags) to High(TOCTReportingTags) do
      TagNames[I] := Ini.ReadString('Tags', TagDefinitions[I].Ini, TagDefinitions[I].Tag);

    FixedEndOPPName := Ini.ReadString('FixedOPPs', 'FixedEndOPP', '');
    FixedStartOPPName := Ini.ReadString('FixedOPPs', 'FixedStartOPP', '');

    CellAccess.ProductionMode := TAccessLevels(Byte(Ini.ReadInteger('Access', 'ProductionMode', 0)));
    CellAccess.ReworkMode := TAccessLevels(Byte(Ini.ReadInteger('Access', 'ReworkMode', 0)));
    CellAccess.DevelopmentMode := TAccessLevels(Byte(Ini.ReadInteger('Access', 'DevelopmentMode', 0)));
    CellAccess.StandaloneMode := TAccessLevels(Byte(Ini.ReadInteger('Access', 'StandaloneMode', 0)));
    CellAccess.ProductionRead := TAccessLevels(Byte(Ini.ReadInteger('Access', 'ProductionRead', 0)));
    CellAccess.ReworkRead := TAccessLevels(Byte(Ini.ReadInteger('Access', 'ReworkRead', 0)));
    CellAccess.DevelRead := TAccessLevels(Byte(Ini.ReadInteger('Access', 'DevelRead', 0)));
    CellAccess.DevelSave := TAccessLevels(Byte(Ini.ReadInteger('Access', 'DevelSave', 0)));
    CellAccess.LocalRead := TAccessLevels(Byte(Ini.ReadInteger('Access', 'LocalRead', 0)));
    CellAccess.LocalSave := TAccessLevels(Byte(Ini.ReadInteger('Access', 'LocalSave', 0)));
  finally
    Ini.Free;
  end;
end;

procedure TGeneralOCT.Shutdown;
var I : TOCTReportingTags;
    Ini : TIniFile;
begin
  Ini := TIniFile.Create(DllProfilePath + InstallName + '.cfg');
  try
    for I := Low(TOCTReportingTags) to High(TOCTReportingTags) do
      Ini.WriteString('Tags', TagDefinitions[I].Ini, TagNames[I]);

    Ini.WriteString('FixedOPPs', 'FixedEndOPP', FixedEndOPPName);
    Ini.WriteString('FixedOPPs', 'FixedStartOPP', FixedStartOPPName);

    Ini.WriteInteger('Access', 'ProductionMode', Byte(CellAccess.ProductionMode));
    Ini.WriteInteger('Access', 'ReworkMode', Byte(CellAccess.ReworkMode));
    Ini.WriteInteger('Access', 'DevelopmentMode', Byte(CellAccess.DevelopmentMode));
    Ini.WriteInteger('Access', 'StandaloneMode', Byte(CellAccess.StandaloneMode));
    Ini.WriteInteger('Access', 'ProductionRead', Byte(CellAccess.ProductionRead));
    Ini.WriteInteger('Access', 'ReworkRead', Byte(CellAccess.ReworkRead));
    Ini.WriteInteger('Access', 'DevelRead', Byte(CellAccess.DevelRead));
    Ini.WriteInteger('Access', 'DevelSave', Byte(CellAccess.DevelSave));
    Ini.WriteInteger('Access', 'LocalRead', Byte(CellAccess.LocalRead));
    Ini.WriteInteger('Access', 'LocalSave', Byte(CellAccess.LocalSave));
  finally
    Ini.Free;
  end;
  inherited;
end;

procedure TGeneralOCT.RecoverCellOCT;
//var I : Integer;
begin
  OCTResult := ReworkStatusDialogue;
  WriteOCTOPPEndString;
  if FileExists(CellMode.CellOCT) then begin
{    FInhibitSort := True;
    try
      Named.LoadOCT(CellMode.CellOCT);
    finally
      FInhibitSort := False;
    end;
    FActiveOPP := ReworkOPP;
    NextOPP;

    for I := 0 to ActiveOPP - 1 do begin
      OPP[I].Status := osUnknown;
    end;
    OPP[ActiveOPP - 1].Status := osReworked;
         }

    OPP[ReworkOPP].Status := osReworked;
    FActiveOPP := ReworkOPP;
    NextOPP;


    Host.UpdateState(Self, opcSetOPPList, PChar(OCTList.Text));
    Named.SetAsInteger(OCTLoadedTag, 1);
    Named.SetAsString(OCTLoadedNameTag, PChar(FOCTLoadedName));


    Named.SetAsInteger(OCTLoadedTag, 1);
    DoStatusChanged;
    DoOCTStageChanged;
    FOCTActive := True;
    OCTStage := orsWaitRun;
  end;
end;

function  TGeneralOCT.AllEnabled : Boolean;
begin
Result := False;
if OCTList.Count > 0 then
   begin
   Result := OPPsEnabled >= OCTList.Count;
   end;
end;

function  TGeneralOCT.OPPsEnabled:Integer;
var
OPPNumber : Integer;
begin
  Result := 0;
  if OCTList.Count <> 0 then begin
    for OPPNumber := 0 to OCTList.Count-1 do begin
      if TOPP(OCTList.Objects[OPPNumber]).Enabled then begin
        Inc(Result);
      end;
    end;
  end;
end;


procedure TGeneralOCT.UpdateOPPModeReports;
begin
  if NC.InCycle and
     not LastOPPInCycle and
     not NC.SingleBlock then begin
    LastOPPInCycle := True;
    WriteOPPStartString;
  end;

  if not NC.InCycle and
     LastOPPInCycle then begin
    LastOPPInCycle := False;
    WriteOPPEndString
  end;
end;


procedure TGeneralOCT.Update(Status : TOCTPluginStatii);
var OPPNumber :Integer;
begin
  inherited;

  CurrentStatus := Status;
  if opsSkipOPP in Status then begin
    RunEnable := False;
    NextOPP;
    OCTStage := orsWaitRun;
  end;

  if Named.GetAsBoolean(Tags[ortWatchDog]) <> LastWatchDog then begin
    WatchDogCount := 0;
    LastWatchDog := not LastWatchDog;
  end;

  Inc(WatchDogCount);

  if not LastWatchDog then begin
    if WatchDogCount > 50 then
      Named.SetAsBoolean(Tags[ortWatchDog], True)
  end else begin
    if (WatchDogCount > 150) and (CellMode.LocalCellMode <> rrmStandalone) then
      IncludeOCTError(oeWatchdog)
    else
      ExcludeOCTError(oeWatchdog);
  end;

  StageToolAddition;

  if not (opsOCTMode in Status) then begin
    RunEnable := False;
    UpdateOPPModeReports;
  end else begin
    if NC.InCycle then begin
      if not LastOCTInCycle then begin
        OPPStartDuration := NamedPlugin.IOSweep^;
        OCTStartDuration := NamedPlugin.IOSweep^;
      end;
      OPPDuration := OPPDurationSoFar + (NamedPlugin.IOSweep^ - OPPStartDuration) / 1000;
      OCTDuration := OCTDurationSoFar + (NamedPlugin.IOSweep^ - OCTStartDuration) / 1000;
    end else if LastOCTInCycle then begin
      OPPDurationSoFar := OPPDuration;
      OCTDurationSoFar := OCTDuration;
    end;

    LastOCTInCycle := NC.InCycle;

    Named.SetAsDouble(Tags[ortOCTTimeRemaining], TotalOCTTime - OCTDuration);
    Named.SetAsDouble(Tags[ortOPPTimeRemaining], OPP[ActiveOPP mod OppCount].TotalTime - OPPDuration);
  end;

  case OCTStage of
    orsWaitRun : begin
      if RunEnable and
         (opsAutoMode in Status) and
         (opsCStartPermissive in Status) and
         not (opsInCycle in Status) then begin
//        if not OCTInterrupted then
          Host.UpdateState(Self, opcCycleStart, nil);
        if not FOCTActive then begin
           FOCTActive := True;
           ResetDisplayParameters;
        end;
        OCTStage := orsWaitInCycle;
        Named.SetAsBoolean(OCTInCycleTag,False);
      end;
    end;

    orsWaitInCycle : begin
      OCTInterrupted := False;
      if opsInCycle in Status then begin
        OCTResult := orcComplete; // default result to complete
        OCTStage := orsWaitEndProgram;
        WriteOCTOPPStartString;
        ResetRecoveryTags;

        OPPStartDuration := NamedPlugin.IOSweep^;
        Named.SetAsBoolean(OCTInCycleTag,True);
        OPP[ActiveOPP].Status := osStarted;
        OPP[ActiveOPP].StartTime := TimeFormat(Now);
        DoStatusChanged;
      end;
    end;

    orsWaitEndProgram : begin
      if not (opsInCycle in Status) then begin
        CheckEndCount := 6;
        OCTStage := orsEndCheckDelay;
      end;
    end;

    orsEndCheckDelay: begin
      if not (opsInCycle in Status) then begin
        Dec(CheckEndCount);
        if  CheckEndCount <= 0 then
          OCTStage := orsEndProcessing
      end else begin
        OCTStage := orsWaitEndProgram;
      end;
    end;

    orsEndProcessing : begin
      if (opsOperationComplete in Status) then begin
        if not (opsInCycle in Status) then begin
          // Normal OPP Termination
          // Step on to next
          Named.SetAsBoolean(OCTInCycleTag,False);
          Host.UpdateState(Self, opcOPPComplete, Pointer(ActiveOPP));
          OPP[ActiveOPP].Status := osComplete;
          OPP[ActiveOPP].Duration := Named.GetAsInteger(Tags[ortCycleTime]);// div 1000;
          NextOPP;
          if (opsSingle in Status) then
          RunEnable := False;
          OCTStage := orsWaitRun;
          WriteOCTOPPEndString; // ?????
        end;
      end else begin
        if not (opsInCycle in Status) then begin
          // Dont react unless in auto Mode
          if (opsAutoMode in Status) then begin
            Named.SetAsBoolean(OCTInCycleTag, False);
            if NC.SingleBlock then begin
              // Stopped In Single Block
              // Arm Single To Auto Trap
              WasInSingle:= True;
            end else begin
            // Here For abnormal Termination
              if WasInSingle then begin
                WasInSingle := False;
                OCTStage := orsWaitRestartSingleToContinuous;
              end else begin
                OPP[ActiveOPP].Status := osInRecovery;
                DoStatusChanged;
                DoRecoveryRequired;
                OCTStage := orsWaitRecoveryOptions;
              end;
            end;
          end;
        end;
      end;
    end;

    orsWaitRestartSingleToContinuous: begin
      if(opsInCycle in Status) then begin
        OCTStage := orsWaitEndProgram;
        Named.SetAsBoolean(OCTInCycleTag,True);
        DoStatusChanged;
      end;
    end;

    orsWaitRecoveryOptions: begin
      OCTInterrupted := True;
      if opsInCycle in Status then begin
        // here to restart current
        OCTStage := orsWaitEndProgram;
        OPP[ActiveOPP].Status := osRestarted;
        Named.SetAsBoolean(OCTInCycleTag,True);
        DoStatusChanged;
      end else begin
        case SelectedRecoveryOption of
          roNone: begin
          end;

          roRestartCurrent: begin
            OPP[ActiveOPP].Status := osInRecovery;
            OCTResult := orcOPPAbandoned;
            WriteOCTOPPEndString; // ?????
            RunEnable := False;
            OCTStage := orsWaitRun; // Added Misreporting
          end;

          roAbandonCurrent: begin
            OPP[ActiveOPP].Status := osAbandoned;
            NextOPP;
            RunEnable := False;
            OCTResult := orcOPPAbandoned;
            WriteOCTOPPEndString; // ?????
            //OCTInterrupted := False;
            RunEnable := False;
            OCTStage := orsWaitRun;
          end;

          roAbandonOCT,roAbandonPartInCell: begin
            OCTResult := orcOCTAbandoned;
            OPP[ActiveOPP].Status := osAbandoned;
            WriteOCTOPPEndString; // ?????
            OPPNumber := ActiveOPP;
            if Assigned(FixedEndOPP) then begin
              while OPPNumber < OCTList.Count - 1 do begin
                TOPP(OCTList.Objects[OPPNumber]).Status := osAbandoned;
                Inc(OPPNumber);
              end;
              LastOPP;
            end else begin
              while OPPNumber < OCTList.Count do begin
                TOPP(OCTList.Objects[OPPNumber]).Status := osAbandoned;
                Inc(OPPNumber);
              end;
              FirstOPP;
              DoOCTSort;  // remove me
              FOCTActive := False;
            end;
            RunEnable := False;
            OCTStage := orsWaitRun;
          end;

          roReworkOPP : begin
            OPP[ActiveOPP].Status := osAbandoned;
            ReworkOPP := ActiveOPP;
            OCTStage := orsWaitReworkComplete;
            RunEnable := False;
          end;
        end;
      end;
    end;

    orsWaitReworkComplete : begin
      if CellMode.CellOCT = '' then // External see RecoverCellOCT
        OCTStage := orsWaitRun;
    end;
  end;

  if (OCTStage <> LastOCTStage) then begin
    OCTStageText := GetEnumName(TYpeInfo(TOCTRunStage),Ord(OCTStage));
    if LastOCTStage = orsWaitRecoveryOptions then begin
      DoStandAloneRecoveryFinished;
      SelectedRecoveryOption := roNone;
    end;
    DoOCTStageChanged;
  end;

  if LastRecoveryOPtion <> SelectedRecoveryOption then begin
    DoStatusChanged;
  end;

  LastOCTStage := OCTStage;
  LastRecoveryOPtion := SelectedRecoveryOption;
end;


procedure TGeneralOCT.DoRecoveryRequired;
begin
  if Assigned(FRecoveryRequired) then
    FRecoveryRequired(Self);
end;

procedure TGeneralOCT.DoStandAloneRecoveryFinished;
begin
  if Assigned(FRecoveryFinished) then
    FRecoveryFinished(Self);
end;

procedure TGeneralOCT.DoOCTStageChanged;
begin
  if Assigned (FOCTStageChanged) then
    FOCTStageChanged(Self);
end;

procedure TGeneralOCT.ResetDisplayParameters;
var
I : Integer;
begin
  for I := 0 to OCTList.Count - 1 do begin
    OPP[I].StartTime := '--/--/--';
    OPP[I].Duration := 0;
    OPP[I].Status := osNone;
  end;
end;

procedure TGeneralOCT.InvertSelection;
var
  OPPNumber : Integer;
begin
  if not AllEnabled and (OCTList.Count <> 0) then begin
    for OPPNumber := 0 to OCTList.Count-1 do begin
      OPP[OPPNumber].Enabled := not OPP[OPPNumber].Enabled;
      OPP[OPPNumber].Checked := not OPP[OPPNumber].Checked;
    end;
    FirstOPP;
  end;
end;


procedure TGeneralOCT.EnableAllOPPs;
var
  OPPNumber : Integer;
begin
  if OCTList.Count <> 0 then begin
    for OPPNumber := 0 to OCTList.Count-1 do begin
      OPP[OPPNumber].Enabled := True;
      OPP[OPPNumber].Checked := True;
    end;
    FirstOPP;
  end;
end;

//
// Allow ObjectPLC to process run-time errors, it takes a little more
// setting up but does allow the error level and when the error is displayed
// to be changed without re-compiling.
//
// Faults arising from a looping state machine are better handled by the PLC
//
procedure TGeneralOCT.IncludeOCTError(aError : TOCTError);
begin
  Include(OCTErrors, aError);
  Named.SetAsInteger(Tags[ortOCTError], Byte(OCTErrors));
end;

procedure TGeneralOCT.ExcludeOCTError(aError : TOCTError);
begin
  Exclude(OCTErrors, aError);
  Named.SetAsInteger(Tags[ortOCTError], Byte(OCTErrors));
end;


procedure TGeneralOCT.CheckCMMFile;
var FNameR, FNameL : array [0..1024] of Char;
    Search : TSearchRec;
    DT : TDateTime;
begin
  Named.GetAsString(Tags[ortCMMDNCFileName], FNameR, 1023);
  Named.GetAsString(Tags[ortCMMLocalFileName], FNameL, 1023);

  Named.SetAsString(Tags[ortCMMOffsetsPath], FNameL);
  Named.SetAsString(Tags[ortCMMOffsetsFile], PChar(ExtractFileName(FNameL)));

  if FileExists(FNameR) then begin
    CopyFile(FNameR, FNameL, False);
  end;

  if FindFirst(LocalCMMPath + '*.cmm', faAnyFile, Search) = 0 then begin
    repeat
      DT := FileDateToDateTime(Search.Time);
      if DT < Now - 2 then  // 2 Days old ????? should be a parameter
        DeleteFile(PChar(LocalCMMPath + Search.Name));
    until FindNext(Search) <> 0;
  end;
  SysUtils.FindClose(Search);

  if not FileExists(FNameL) then begin
    Named.SetFaultA('OCT', 'Unable to find CMM file', FaultWarning, nil);
    Exit;
  end;
end;

procedure TGeneralOCT.ReplaceToolAtDoorOnWrite;
var
WriteStr : String;
Buffer : array [0..1024] of Char;
ReadStr  : String;
currentToolSN :Integer;
ToolTYpe : String;
SN : Integer;
begin
try
Named.GetAsString(Tags[ortToolAtDoorString], Buffer, 1024);
ReadStr := Buffer;
ToolType := ReadDelimited(ReadStr, ',');
SN := StrToInt(ReadDelimited(ReadStr, ','));
if (ToolType <> '') then
   begin
   OffsToolLife.RemoveTool(ToolTYpe,SN);
   end;
except
end;
named.GetAsString(Tags[ortWrittenToolStr], Buffer, 1024);
AddTool(Buffer, Named.GetAsInteger(Tags[ortToolChangerPosition]));
named.SetAsString(Tags[ortToolAtDoorString],Buffer);
end;

initialization
  TAbstractOCTPlugin.AddPlugin('OCTManager', TGeneralOCT);
end.
