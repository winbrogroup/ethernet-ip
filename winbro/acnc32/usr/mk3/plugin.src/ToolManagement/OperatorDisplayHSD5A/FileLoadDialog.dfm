object FileLoadForm: TFileLoadForm
  Left = 274
  Top = 119
  BorderStyle = bsNone
  Caption = 'File load dialog'
  ClientHeight = 697
  ClientWidth = 1012
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -19
  Font.Name = 'Verdana'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 23
  object Panel1: TPanel
    Left = 0
    Top = 644
    Width = 1012
    Height = 53
    Align = alBottom
    TabOrder = 0
    object BtnOK: TTouchSoftkey
      Left = 8
      Top = 4
      Width = 185
      Height = 41
      Caption = 'BtnOK'
      TabOrder = 0
      OnClick = BtnOKClick
      Legend = 'OK'
      TextSize = 14
      HasIndicator = False
      IndicatorOn = False
      IsEnabled = True
      ButtonStyle = skNormal
      ArrowColor = clBlack
      ButtonColor = clWhite
      LegendColor = clBlack
      IndicatorWidth = 20
      IndicatorHeight = 10
      IndicatorOnColor = clGreen
      IndicatorOffColor = clSilver
    end
    object BtnCancel: TTouchSoftkey
      Left = 200
      Top = 4
      Width = 185
      Height = 41
      Caption = 'BtnCancel'
      TabOrder = 1
      OnClick = BtnCancelClick
      Legend = 'Cancel'
      TextSize = 14
      HasIndicator = False
      IndicatorOn = False
      IsEnabled = True
      ButtonStyle = skNormal
      ArrowColor = clBlack
      ButtonColor = clWhite
      LegendColor = clBlack
      IndicatorWidth = 20
      IndicatorHeight = 10
      IndicatorOnColor = clGreen
      IndicatorOffColor = clSilver
    end
    object BtnPathSwitch: TTouchSoftkey
      Left = 440
      Top = 4
      Width = 273
      Height = 41
      Caption = 'BtnCancel'
      TabOrder = 2
      OnClick = BtnPathSwitchClick
      Legend = 'Path Switch'
      TextSize = 14
      HasIndicator = False
      IndicatorOn = False
      IsEnabled = True
      ButtonStyle = skNormal
      ArrowColor = clBlack
      ButtonColor = clWhite
      LegendColor = clBlack
      IndicatorWidth = 20
      IndicatorHeight = 10
      IndicatorOnColor = clGreen
      IndicatorOffColor = clSilver
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 1012
    Height = 377
    Align = alTop
    TabOrder = 1
    object Panel4: TPanel
      Left = 1
      Top = 1
      Width = 320
      Height = 375
      Align = alLeft
      TabOrder = 0
      object DirGrid: TStringGrid
        Left = 1
        Top = 33
        Width = 318
        Height = 341
        Align = alClient
        ColCount = 1
        DefaultColWidth = 300
        DefaultRowHeight = 32
        DefaultDrawing = False
        FixedCols = 0
        FixedRows = 0
        Options = [goFixedVertLine, goFixedHorzLine, goHorzLine]
        TabOrder = 0
        OnDblClick = DirGridDblClick
        OnDrawCell = DirGridDrawCell
        OnEnter = DirGridEnter
        OnKeyPress = FileGridKeyPress
      end
      object Panel6: TPanel
        Left = 1
        Top = 1
        Width = 318
        Height = 32
        Align = alTop
        TabOrder = 1
        object PathLabel: TLabel
          Left = 1
          Top = 1
          Width = 316
          Height = 30
          Align = alClient
          Caption = 'PathLabel'
        end
      end
    end
    object Panel5: TPanel
      Left = 321
      Top = 1
      Width = 690
      Height = 375
      Align = alClient
      TabOrder = 1
      object FileGrid: TStringGrid
        Left = 1
        Top = 1
        Width = 688
        Height = 373
        Align = alClient
        ColCount = 2
        DefaultColWidth = 300
        DefaultRowHeight = 32
        DefaultDrawing = False
        FixedCols = 0
        Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goThumbTracking]
        TabOrder = 0
        OnClick = FileGridClick
        OnDblClick = BtnOKClick
        OnDrawCell = DirGridDrawCell
        OnEnter = FileGridEnter
        OnKeyPress = FileGridKeyPress
        OnSelectCell = FileGridSelectCell
      end
    end
  end
  object PanelKeyboard: TPanel
    Left = 0
    Top = 415
    Width = 1012
    Height = 229
    Align = alBottom
    TabOrder = 2
  end
  object Panel3: TPanel
    Left = 0
    Top = 377
    Width = 1012
    Height = 38
    Align = alBottom
    TabOrder = 3
    object Label1: TLabel
      Left = 32
      Top = 4
      Width = 139
      Height = 23
      Caption = 'Save Filename'
    end
    object SaveEdit: TEdit
      Left = 184
      Top = 4
      Width = 329
      Height = 31
      TabOrder = 0
      Text = 'SaveEdit'
    end
  end
end
