unit OCTOperatorDisplay;

interface
uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, AbstractFormPlugin, IFormInterface, NamedPlugin,
  ComCtrls, INamedInterface, Grids, PluginInterface,  ImgList, OCTTypes, OCTListBox,
  CNCTypes, OpDisplayTypes, IToolLife, ACNC_Plusmemo,SyntaxHighProgDisplay, OCTData,ToolData,IniFiles,
  ReqToolList,RequiredToolData,ToolDB,Confirm, CellMode, ACNC_TouchGlyphSoftkey, ACNC_TouchSoftkey, ACNC_ThreeLineButton;

const
  MaxFontSize = 20;

type
  TODisplayReportingTags = (
    ortOpenToolDoor,
    ortOpenMainDoor,

    ortReqChuckChange,
    ortReqToolChuckChange,
    ortToolDoorLocked,
    ortStatus1,
    ortStatus2,
    oCellMode,
    ortMainDoorLocked,
    ortMainDoorOpenInhibit,
    ortToolDoorOpenInhibit,

    ortOCTComplete,
    ortNewProgTag,
    ortResetComplete,
    ortLoadedOPPName,

    ortTCFitted,
    ortFaultAcknowledgeTag,
    ortOCtRunning,
    ortOCTIndex,
    ortOCTReqReset,
    ortOCTMode,
    ortOCTLoaded,
    ortProgramLine,

    ortFakeSystem,
    ortLoadedOCTName,
    ortShuttlePartName,
    ortShuttleSerialNumber,
    ortPartClampComplete,
    ortPartClampPermissive,
    ortPartClampedAA,
    ortTotalToolLifeInHead,
    ortBarrelToolLifeInHead,
    ortInRecovery,
    ortToolRequired,
    ortInCycle,
    ortToolClampComplete,
    ortToolClampPermissive,
    ortToolClampedAA,
    ortPartStatusForm
    );

  TTagDefinitions = record
    Ini : string;
    Tag : string;
    Owned : Boolean;
    TagType : string;
  end;

const
  TagDefinitions : array [TODisplayReportingTags] of TTagDefinitions = (
     ( Ini : 'ReqOpenToolDoor'; Tag : 'OCT_ReqOpenToolDoor'; Owned : True; TagType : 'TMethodTag' ),
     ( Ini : 'ReqOpenMainDoor'; Tag : 'OCT_ReqOpenMainDoor'; Owned : True; TagType : 'TMethodTag' ),
     ( Ini : 'ReqChuckChange'; Tag : 'OCT_ReqChuckChange'; Owned : True; TagType : 'TMethodTag' ),
     ( Ini : 'ReqToolChange'; Tag : 'OCT_ReqToolChuckChange'; Owned : True; TagType : 'TMethodTag' ),
     ( Ini : 'ToolDoor'; Tag : 'ToolDoorLocked'; Owned : False; TagType : 'TMethodTag' ),

     ( Ini : 'Status1'; Tag : 'OCTStatus1'; Owned : False; TagType : 'TStringTag' ),
     ( Ini : 'Status2'; Tag : 'OCTStatus2'; Owned : False; TagType : 'TStringTag' ),
     ( Ini: NameCM_CellMode; Tag: NameCM_CellMode; Owned: False; TagType: 'TIntegerTag' ),
     ( Ini : 'MainDoorIsLocked'; Tag : 'DoorClosed'; Owned : False; TagType : 'TIntegerTag' ),
     ( Ini : 'MainDoorOpenInhibit'; Tag : 'MainDoorOpenInhibit'; Owned : False; TagType : 'TIntegerTag' ),
     ( Ini : 'ToolDoorOpenInhibit'; Tag : 'ToolDoorOpenInhibit'; Owned : False; TagType : 'TIntegerTag' ),

     ( Ini : 'OCTComplete'; Tag : 'OCTComplete'; Owned : False; TagType : 'TMethodTag' ),
     ( Ini : 'NewProgTag'; Tag : 'NewActiveFile'; Owned : False; TagType : 'TStringTag' ),
     ( Ini : 'ResetComplete'; Tag : 'ResetComplete'; Owned : False; TagType : 'TIntegerTag' ),
     ( Ini : 'LoadedOPPName'; Tag : 'NewActiveFile'; Owned : False; TagType : 'TStringTag' ),

     ( Ini : 'TCFitted'; Tag : '_OptionToolChanger'; Owned : False; TagType : 'TIntegerTag' ),
     ( Ini : 'FaultAcknowledgeTag'; Tag : 'OCT_ReqFaultAck'; Owned : False; TagType : 'TMethodTag' ),
     ( Ini : 'OCtRunning'; Tag : 'OCtRunning'; Owned : False; TagType : 'TMethodTag' ),
     ( Ini : 'OCTIndex'; Tag : 'OCTIndex'; Owned : False; TagType : 'TIntegerTag' ),
     ( Ini : 'OCTReqReset'; Tag : 'OCTReqReset'; Owned : False; TagType : 'TIntegerTag' ),
     ( Ini : 'OCTMode'; Tag : 'OCTMode'; Owned : False; TagType : 'TMethodTag' ),
     ( Ini : 'OCTLoadedOK'; Tag : 'OCTLoadedOK'; Owned : False; TagType : 'TIntegerTag' ),
     ( Ini : 'ProgramLine'; Tag : 'NC1ProgramLine'; Owned : False; TagType : 'TIntegerTag' ),

     ( Ini : 'ACNC32Fake'; Tag : 'ACNC32Fake'; Owned : False; TagType : 'TIntegerTag' ),
     ( Ini : 'OCTLoadedName'; Tag: 'OCTLoadedName'; Owned : False; TagType : 'TStringTag' ),
     ( Ini : 'PartID'; Tag: 'PartID'; Owned : False; TagType : 'TIntegerTag' ),
     ( Ini : 'PartSN'; Tag: 'PartSN'; Owned : False; TagType : 'TIntegerTag' ),
     ( Ini : 'PartClampComplete'; Tag: 'PartClampComplete'; Owned : False; TagType : 'TIntegerTag' ),
     ( Ini : 'PartClampPermissive'; Tag: 'PartClampPermissive'; Owned : False; TagType : 'TIntegerTag' ),
     ( Ini : 'PartClamped'; Tag: 'PartClamped'; Owned : False; TagType : 'TIntegerTag' ),
     ( Ini : 'OCT_TotalToolLifeInHead'; Tag: 'OCT_TotalToolLifeInHead'; Owned : True; TagType : 'TStringTag' ),
     ( Ini : 'OCT_BarrelToolLifeInHead'; Tag: 'OCT_BarrelToolLifeInHead'; Owned : True; TagType : 'TStringTag' ),
     ( Ini : 'OCT_InRecovery'; Tag: 'OCT_InRecovery'; Owned : True; TagType : 'TIntegerTag' ),
     ( Ini : 'OCT_ToolLoadRequested'; Tag: 'OCT_ToolLoadRequested'; Owned : True; TagType : 'TIntegerTag' ),
     ( Ini : 'NC1InCycle'; Tag: 'NC1InCycle'; Owned : False; TagType : 'TIntegerTag' ),
     ( Ini : 'ToolClampComplete'; Tag: 'ToolClampComplete'; Owned : False; TagType : 'TIntegerTag' ),
     ( Ini : 'ToolClampPermissive'; Tag: 'ToolClampPermissive'; Owned : False; TagType : 'TIntegerTag' ),
     ( Ini : 'ToolClamped'; Tag: 'ToolClamped'; Owned : False; TagType : 'TIntegerTag' ),
     ( Ini : 'PartStatusForm'; Tag: 'PartManagement'; Owned : False; TagType : 'TIntegerTag' )
  );

type
  TOctDisplayModes = (odmDefault,odmRecovery,odmAllOPP,odmNotOCT,odmPartProgram,odmToolreq );


  TOCTFormDlg = class(TInstallForm)
    BottomPanel: TPanel;
    BaseOCTPanel: TPanel;
    DisplayOptionsPanel: TPanel;
    BtnClose: TTouchGlyphSoftkey;
    OctStatusPanel: TPanel;
    LeftStatusPanel: TPanel;
    LabelStatus1: TLabel;
    RightStatusPanel: TPanel;
    LabelStatus2: TLabel;
    OCTLowerPageControl: TPageControl;
    PDisplaySheet: TTabSheet;
    Panel17: TPanel;
    ProgNameDisplay: TLabel;
    ProgLineDisplay: TLabel;
    RecoveryPanel: TPanel;
    RecoveryBanner: TPanel;
    Panel4: TPanel;
    Panel6: TPanel;
    Panel8: TPanel;
    RestartPanel: TPanel;
    RestartCurrentButton: TTouchSoftkey;
    Panel10: TPanel;
    SkipPanel: TPanel;
    SkipButton: TTouchSoftkey;
    Panel9: TPanel;
    AbandonPartPanel: TPanel;
    AbandonPartButton: TTouchSoftkey;
    Panel11: TPanel;
    Panel2: TPanel;
    AbandonOCTPanel: TPanel;
    AbandonOCTButton: TTouchSoftkey;
    ReworkPanel: TPanel;
    ReworkButton: TTouchSoftkey;
    Panel7: TPanel;
    Panel12: TPanel;
    MessageLabel: TLabel;
    MessageLabel2: TLabel;
    AcceptKey: TTouchSoftkey;
    Panel13: TPanel;
    MiscPanel: TPanel;
    ToolStatMainPanel: TPanel;
    TStatHeadPanel: TPanel;
    Panel16: TPanel;
    InvalidateBlank: TPanel;
    BtnInvalidate: TTouchSoftkey;
    TStatTS1Panel: TPanel;
    TStatTS2Panel: TPanel;
    TStatTS3Panel: TPanel;
    TStatTS4Panel: TPanel;
    Panel22: TPanel;
    RemTimeDisplay: TLabel;
    Panel3: TPanel;
    Label2: TLabel;
    Label3: TLabel;
    OperatorButtonPanel: TPanel;
    TStatTS5Panel: TPanel;
    TStatTS8Panel: TPanel;
    OCTImages: TImageList;
    Panel18: TPanel;
    Panel20: TPanel;
    Panel21: TPanel;
    BtnPartChuck: TTouchSoftkey;
    BtnToolChuck: TTouchSoftkey;
    Panel27: TPanel;
    Panel28: TPanel;
    BtnOpenMainDoor: TTouchSoftkey;
    Panel29: TPanel;
    BtnOpenToolDoor: TTouchSoftkey;
    PartChuckStatusPanel: TPanel;
    MainDoorStatusPanel: TPanel;
    ToolChuckStatusPanel: TPanel;
    ToolDoorStatusPanel: TPanel;
    BtnShowPartData: TTouchGlyphSoftkey;
    BtnFaultAck: TTouchGlyphSoftkey;
    TStatTS7Panel: TPanel;
    TStatTS6Panel: TPanel;
    BtnResetTool: TTouchGlyphSoftkey;
    BtnHead: TThreeLineButton;
    procedure ReworkButtonMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure RestartCurrentButtonMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure SkipButtonMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure AbandonOCTButtonMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure AcceptKeyMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure AbandonPartButtonMouseUp(Sender: TObject;
      Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure BtnCloseClick(Sender: TObject);
    procedure BtnGeneralMouseDown(Sender: TObject;
      Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure BtnGeneralMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure BtnCloseMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure BtnDefaultDisplayMouseUp(Sender: TObject;
      Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure Panel12Resize(Sender: TObject);
    procedure BaseOCTPanelResize(Sender: TObject);
  private
    DisplayTop : Integer;
    DisplayHeight : Integer;
    LazyUpdateFlag : Boolean;
    FixedStartOPP : String;
    FixedEndOPP : String;

    LastStatus   :TOPPStatus;
    CallCount               : Integer;
    CallCount2              : Integer;
    UpdateSweepCount        : Integer;
    CurrentUpdateCount      : Integer;
    UsesFixedEndOPP         : Boolean;
    UsesFixedStartOPP       : Boolean;
    ShowZeroLifeButton      : Boolean;
    ShowResetButton         : Boolean;
    SelfStartTag            : TTagref;
    OCTData                 : TOPPList;
    ToolData                : TToolList;
    MasterToolList          : TToolDB;

    RequiredToolData        : TRequiredToolList;
    OCTDisplay              : TOCTListBox;
    //RToolDisplay             : TReqToolList;
    InRecovery              : Boolean;
    CurrentRecoveryOPtion   : TRecoveryOptions;

    PrDisplay               : TSyntaxHighProgDisplay;
    DisplayMode             : TOctDisplayModes;
    LastSubIndex            : Integer;


    TagNames : array[TODisplayReportingTags] of string;
    Tags : array [TODisplayReportingTags] of TTagRef;
    SweepCount : Integer;
    InHeadText  : String;

    // Translation Strings
    EmptyText,
    DoorText,
    BackText,
    LeftText,
    RightText,
    HeadText,
    LockedText,
    AddText,
    RemoveText,
    OpenText,
    FaultyText,
    ClosedText,
    ClampedText,
    StatusOnlyText,
    vsOCTText,
    EMPTYButtonText,
    PleaseSelText,
    PressAcceptAndClearText,
    ThenPressCycleText,
    PAToABANDONOPPText,
    PAToABANDONOCTText,
    PAToABANDONPartText,
    ThenPressCycleStartToAbandonText,
    PAToREWORKText,
    ReworkPermittedText,
    AYSInvalidateText,
    ShowToolTableText,
    ShowPartProgramText,
    InvalidProgramText : string;
    Selected_DofD: string;
    Total_Duration_S: string;
    Index_DOfD: string;

    Stations: Integer;

    ButtonReleaseCount: Integer;

    //procedure UpdateToolingDisplay;
    procedure LazyUpdate(Sender : TObject);
    procedure ReleaseButtons;
    procedure OCTCellUpdate(Sender : TObject; Row : Integer; Field : WideString);
//    procedure UpdateOCTList(Sender : TOBject);
    procedure UpdateOCTStatus;
    procedure DisplayRecoveryOptions;
    procedure SetButtonPanel(RecOption: TRecoveryOptions);
    procedure ClearRecoveryOptions;
    procedure OCTRunningChanged(Running : Boolean);
    procedure DoResetDisplay(Sender: TOBject);
    procedure DoOctDisplayClicked(Sender: TOBject);
    procedure DoSelectAllOpps(Sender: TObject);
    procedure DoInvertSelection(Sender: TObject);
    procedure DoChangeSize(Sender: TObject);
    procedure SetToolDoorStatus;
    procedure SetMainDoorStatus;
    //procedure UpdateToolDisplayFor(ChangerPos : Integer);
    procedure UpdateButtonEnable;
    procedure UpdatePartProgDisplay(ForceProgLoad : Boolean);
    procedure SetDisplayMode(DMode : TOctDisplayModes);
    procedure SetAcceptKeyState(Enabled : Boolean);
    procedure SetSoftButtonEnables(InRec: Boolean;OCTMode : Boolean);
    procedure SetStatus(const Text : string; Index : Integer);
    procedure CellModeChange(ATag: TTagRef);

    procedure ReadConfiguration;
    procedure ResetOCT;
    function  OCTisRunning:Boolean;
    function  isOCTMode: Boolean;
    //function  isStatusOnlyDisplay: Boolean;
    function  OPPIndex: Integer;
    function  OCTFilename: String;
    //function UpdateToolStatus : Boolean;
    procedure HandleOCTStatusChange(Sender : TObject);
    function SecondsToHMS(Seconds: Integer): string;
    procedure ZeroToolLifeForToolInLocation(Location: Integer);
    procedure SetPartClampStatus;
    procedure SetToolClampStatus;
  public
    destructor Destroy; override;
    procedure Install; override;
    procedure FinalInstall; override;
    procedure Execute; override;
    procedure Shutdown; override;
    procedure CloseForm; override;
    procedure DisplaySweep; override;
    procedure TagChanged(Tag: TTagRef);
    procedure Resize; override;
    procedure UpdateStnButtons;

  end;

var

MyOCTPopup : TOCTFormDlg;

procedure TagChangeHandler(Tag : TTagRef);stdcall;

implementation

uses TypInfo;

{$R *.DFM}

procedure TOCTFormDlg.CloseForm;
begin
  if Visible then
    ModalResult := mrOK;
end;

procedure TOCTFormDlg.DisplaySweep;
begin
 if LazyUpdateFlag then
  begin
  Dec(CurrentUpdateCount);
  if CurrentUpdateCount < 0 then
    begin
    CurrentUpdateCount := UpdateSweepCount;
    LazyUpdateFlag := False;
    //RToolDisplay.UpdateDisplay;
    UpdateStnButtons;
    end;
  end;
  Dec(ButtonReleaseCount);
  if ButtonReleaseCount = 0 then begin
    ReleaseButtons;
  end;
end;

destructor TOCTFormDlg.Destroy;
begin
  OCTData.Close;
  OCTData := nil;
  ToolData.Close;
  ToolData := nil;
  RequiredToolData.Close;
  RequiredToolData := nil;
  MasterToolList.Close;
  MasterToolList := nil;

  inherited;
end;

procedure TOCTFormDlg.Execute;
begin
  if not Visible then
    begin
    if Named.GetASBoolean(Tags[ortTCFitted]) then begin
    end
    else
    begin
    end ;

    Height := 700;
    Width := 1024;
    ShowModal;
    UpdateButtonEnable;
    end;
end;


procedure TagChangeHandler(Tag : TTagRef);stdcall
begin
  MyOCTPopup.TagChanged(Tag);
end;

procedure TOCTFormDlg.ShutDown;
var I : TODisplayReportingTags;
    Ini : TCustomIniFile;
begin

  Ini := TIniFile.Create(DllProfilePath + InstallName + '.cfg');
  try
    for I := Low(TODisplayReportingTags) to High(TODisplayReportingTags) do
      Ini.WriteString('Tags', TagDefinitions[I].Ini, TagNames[I]);

  finally
    Ini.Free;
  end;

  inherited;
end;


procedure TOCTFormDlg.TagChanged(Tag: TTagRef);
var ctmp : array [0..128] of Char;
begin
  if Tag = Tags[ortOCTRunning] then begin
    // here when all complete or when recovery required
    OCTRunningChanged(Named.GetAsBoolean(Tag));
  end else

  if Tag = Tags[ortOCTIndex] then begin
     if OPPIndex = 0 then
        begin
        SetStatus('',1);
        SetStatus('',2);
        end;
     OCTDisplay.RunningRow := OPPIndex + 1;
     OCTDisplay.RefreshDisplay;
     OCTDisplay.DisplayResetButtonEnabled := True;
     OCTDisplay.SelectAllButtonEnabled := True;
     //RToolDisplay.UpdateDisplay;
     UpdateOCTStatus;

  end else

  if Tag = Tags[ortOCTLoaded] then begin
    if Named.GetAsBoolean(Tags[ortOCTLoaded]) then begin
      Inrecovery:= False;
      SetDisplayMode(odmDefault);
    end;
  end;

  if Tag = Tags[ortOCTComplete] then begin
     // Here at end of each OPP not just OCT Complete !!!
  end else

  if (Tag = Tags[ortNewProgTag]) or
     (Tag = Tags[ortResetComplete]) then begin
     UpdatePartProgDisplay(True);
     end else

  if Tag = Tags[ortStatus1] then begin
    Named.GetAsString(Tag, ctmp, Length(ctmp) - 1);
    SetStatus(ctmp, 1);
    end else

  if Tag = Tags[ortStatus2] then begin
     Named.GetAsString(Tag, ctmp, Length(ctmp) - 1);
     SetStatus(ctmp, 2);
     end else

  if Tag = Tags[ortToolDoorLocked] then begin
     SetToolDoorStatus;
     end else

  if Tag = Tags[ortMainDoorLocked] then begin
     SetMainDoorStatus
     end else

  if Tag = Tags[ortMainDoorOPenInhibit] then begin
     UpdateButtonEnable
     end else

  if Tag = Tags[ortToolDoorOPenInhibit] then begin
     UpdateButtonEnable;
     end else

  if Tag = Tags[ortProgramLine] then begin
     UpdatePartProgDisplay(False);
     end else

  if Tag = Tags[ortOCTMode]  then begin
    if Named.GetAsBoolean(Tag) then begin
        SetDisplayMode(odmDefault);
    end
    else begin
      SetDisplayMode(odmNotOCT);
    end;
    UpdateOCTStatus;//(self);
    LazyUpdateFlag:= True;
  end;

  if (Tag = Tags[ortPartClampComplete]) OR
   (Tag = Tags[ortPartClampedAA]) OR
   (Tag = Tags[ortPartClampPermissive]) then begin
     SetPartClampStatus;
  end;
  if (Tag = Tags[ortToolClampComplete]) OR
   (Tag = Tags[ortToolClampedAA]) OR
   (Tag = Tags[ortToolClampPermissive]) then begin
   SetToolClampStatus;
  end;

  if Tag = Tags[oCellMode] then
    Self.CellModeChange(Tag);
end;

procedure TOCTFormDlg.SetPartClampStatus;
var
Open    : Boolean;
Clamped : Boolean;

begin
  Open := not Named.GetAsBoolean(Tags[ortPartClampedAA]);
  Clamped := Named.GetAsBoolean(Tags[ortPartClampComplete]);
  if Open then
    begin
    PartChuckStatusPanel.Color := clred;
    PartChuckStatusPanel.Caption := OpenText;
    PartChuckStatusPanel.Font.Color := clWhite;
    end
  else if Clamped then
    begin
    PartChuckStatusPanel.Color := clLime;
    PartChuckStatusPanel.Caption := ClampedText;
    PartChuckStatusPanel.Font.Color := clBlack;
    end
  else
    begin
    PartChuckStatusPanel.Color := clYellow;
    PartChuckStatusPanel.Caption := ClosedText;
    PartChuckStatusPanel.Font.Color := clBlack;
    end;
  UpdateButtonEnable;
end;

procedure TOCTFormDlg.SetToolClampStatus;
var
Open    : Boolean;
Clamped : Boolean;
begin
  Open := not Named.GetAsBoolean(Tags[ortToolClampedAA]);
  Clamped := Named.GetAsBoolean(Tags[ortToolClampComplete]);
  if Open then
    begin
    ToolChuckStatusPanel.Color := clred;
    ToolChuckStatusPanel.Caption := OpenText;
    ToolChuckStatusPanel.Font.Color := clWhite;
    end
  else if Clamped then
    begin
    ToolChuckStatusPanel.Color := clLime;
    ToolChuckStatusPanel.Caption := ClampedText;
    ToolChuckStatusPanel.Font.Color := clBlack;
    end
  else
    begin
    ToolChuckStatusPanel.Color := clYellow;
    ToolChuckStatusPanel.Caption := ClosedText;
    ToolChuckStatusPanel.Font.Color := clBlack;
    end;
  UpdateButtonEnable;
end;

procedure TOCTFormDlg.SetToolDoorStatus;
begin
  if Assigned(Tags[ortToolDoorLocked]) then begin
    if Named.GetAsBoolean(Tags[ortToolDoorLocked]) then begin
    ToolDoorStatusPanel.Color := clLime;
    ToolDoorStatusPanel.Caption := LockedText;
    ToolDoorStatusPanel.Font.Color := clBlack;

    end else begin
      ToolDoorStatusPanel.Color := clRed;
      ToolDoorStatusPanel.Caption := OpenText;
      ToolDoorStatusPanel.Font.Color := clWhite;
    end;
  end;
end;

procedure TOCTFormDlg.SetMainDoorStatus;
begin
  if Assigned(Tags[ortMainDoorLocked]) then begin
    if Named.GetAsBoolean(Tags[ortMainDoorLocked]) then begin
      MainDoorStatusPanel.Color := clLime;
      MainDoorStatusPanel.Caption := LockedText;
      MainDoorStatusPanel.Font.Color := clBlack;
    end else begin
      MainDoorStatusPanel.Color := clRed;
      MainDoorStatusPanel.Caption := OpenText;
      MainDoorStatusPanel.Font.Color := clWhite;
    end;
  end
else
    begin
    MainDoorStatusPanel.Color := clYellow;
    MainDoorStatusPanel.Caption := FaultyText;
    MainDoorStatusPanel.Font.Color := clWhite;
    end
end;

procedure TOCTFormDlg.FinalInstall;
var
Translator : TTranslator;
I : TODisplayReportingTags;
begin
  inherited;
  for I := Low(TODisplayReportingTags) to High(TODisplayReportingTags) do begin
    if not TagDefinitions[I].Owned then
      Tags[I] := Named.AquireTag(PChar(TagNames[I]), PChar(TagDefinitions[I].TagType), TagChangeHandler);
    end;
  SelfStartTag := Named.AquireTag(PChar(InstallName),'TMethodTag',nil);

  OCtDisplay.FinalInstall;

  Translator := TTranslator.Create;
  try
  with Translator do
       begin
       EmptyText                   := GetString('$EMPTY');
       DoorText                    := GetString('$AT_DOOR');
       BackText                    := GetString('$BACK');
       LeftText                    := GetString('$LEFT');
       RightText                   := GetString('$RIGHT');
       HeadText                    := GetString('$HEAD');
       LockedText                  := GetString('$LOCKED');
       AddText                     := GetString('$ADD');
       RemoveText                  := GetString('$REMOVE');
       OpenText                    := GetString('$OPEN');
       ClosedText                  := GetString('$CLOSED');
       ClampedText                 := GetString('$CLAMPED');
       FaultyText                  := GetString('$FAULTY');
       StatusOnlyText              := GetString('$STATUS_ONLY');
       vsOCTText                   := GetString('$VS_OCT');
       ShowToolTableText           := GetString('$Show~Tool_Table');
       ShowPartProgramText         := GetString('$Show~Part_Program');
       InvalidProgramText          := GetString('$INVALID_PROGRAM');
       EMPTYButtonText             := GetString('$___EMPTY___');
       PleaseSelText               := GetString('$PLEASE_SELECT_A_RECOVERY_OPTION_FROM_ABOVE');
       PressAcceptAndClearText     := GetString('$PRESS_ACCEPT_AND_CLEAR_ANY_ERRORS_TO_RESATRT_CURRENT_OPP');
       ThenPressCycleText          := GetString('$THEN_PRESS_CYCLE_START_TO_CONTINUE');
       PAToABANDONOPPText          := GetString('$PRESS_ACCEPT_TO_ABANDON_CURRENT_OPP');
       PAToABANDONOCTText          := GetString('$PRESS_ACCEPT_TO_ABANDON_OCT');
       PAToABANDONPartText         := GetString('$PRESS_ACCEPT_TO_ABANDON_PART');
       ThenPressCycleStartToAbandonText := GetString('$THEN_PRESS_CYCLE_START_TO_ABANDON');
       PAToREWORKText              := GetString('$PRESS_ACCEPT_TO_REWORK_OPP');
       ReworkPermittedText         := GetString('$REWORK_IS_ONLY_PERMITTED_IN_PRODUCTION_MODE');
       AYSInvalidateText           := GetString('$ARE_YOU_SURE_YOU_WANT_TO_INVALIDATE_THE_TOOL_IN_THE_HEAD');
       BtnClose.Legend             := GetString('$___Close___');
       RecoveryBanner.Caption      := GetString('$OCT_RECOVERY_OPTIONS');
       RestartPanel.Caption        := GetString('$RESTART_CURRENT_OPP');
       SkipPanel.Caption           := GetString('$SKIP_TO_NEXT_OPP');
       AbandonOCTPanel.Caption     := GetString('$ABANDON_ENTIRE_OCT');
       AbandonPartPanel.Caption    := GetString('$MARK_PART_AS_NON_CONFORM');
       ReworkPanel.Caption         := GetString('$REWORK_CURRENT_OPP');
       AcceptKey.Legend            := GetString('$ACCEPT');
       BtnOpenToolDoor.Legend      := GetString('$OPEN_TOOL_DOOR');
       BtnOpenMainDoor.Legend      := GetString('$OPEN_MAIN_DOOR');
       BtnPartChuck.Legend         := GetString('$PART_CHUCK');
       BtnToolChuck.Legend         := GetString('$TOOL_CHUCK');
       BtnInvalidate.Legend        := GetString('$ZERO_LIFE');
       BtnFaultAck.Legend          := GetString('$FAULT_ACKNOWLEDGE');
       BtnShowPartData.Legend      := GetString('$SHOW_PART_DATA');

       Panel16.Caption             := GetString('$HEAD');
       //ToolDisplayToggle.Legend    := GetString('$DISPLAY_MODE');

       Selected_DofD:= GetString('$SELECTED_D_OF_D');
       Total_Duration_S:= GetString('$TOTAL_DURATION_S');
       Index_DOfD:= Translator.GetString('$INDEX_D_OF_D');
       end;

  finally
  Translator.Free;
  end;

  SetToolDoorStatus;
  SetMainDoorStatus;
  SetPartClampStatus;
  SetToolClampStatus;

  OctDisplay.ShowResetButton := ShowResetButton;
  Named.SetAsBoolean(Tags[ortInrecovery],False);
  SetDisplayMode(odmNotOCT);
end;

procedure TOCTFormDlg.Install;
var
Translator : TTranslator;
I : TODisplayReportingTags;
//octFile : TiniFile;
C: Integer;
begin

  LastStatus := osUnknown;
  SweepCount:= 25;
  CallCount := 0;
  CallCount2 := 0;
  Translator := TTranslator.Create;
  try

  with Translator do
       begin
       InHeadText     := GetString('$IN_HEAD');
       end

  finally
  Translator.Free;
  end;
  for I := Low(TODisplayReportingTags) to High(TODisplayReportingTags) do begin
    if TagDefinitions[I].Owned then
      Tags[I] := Named.AddTag(PChar(TagNames[I]), PChar(TagDefinitions[I].TagType));
  end;

  MyOCTPopup := Self;
  Font.Name := NamedPlugin.FontName;
  PrDisplay := TSyntaxHighProgDisplay.Create(Self);
  with PrDisplay do
       begin
       Parent :=  PDisplaySheet;
       readConfigFile(DLLProfilePath+'OPPEditor.cfg');
       Align := alClient;
       LastSubIndex := -10000;
       CurrentLineInCentre := True;
       end;

  C:= (55 * 8) div Stations;

  TStatTS1Panel.Height:= C;
  TStatTS2Panel.Height:= C;
  TStatTS3Panel.Height:= C;
  TStatTS4Panel.Height:= C;
  TStatTS5Panel.Height:= C;
  TStatTS6Panel.Height:= C;
  TStatTS7Panel.Height:= C;
  TStatTS8Panel.Height:= C;

  TStatTS1Panel.Visible:= Stations > 0;
  TStatTS2Panel.Visible:= Stations > 1;
  TStatTS3Panel.Visible:= Stations > 2;
  TStatTS4Panel.Visible:= Stations > 3;
  TStatTS5Panel.Visible:= Stations > 4;
  TStatTS6Panel.Visible:= Stations > 5;
  TStatTS7Panel.Visible:= Stations > 6;
  TStatTS8Panel.Visible:= Stations > 7;
end;

procedure TOCTFormDlg.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  ReleaseButtons;
end;

procedure TOCTFormDlg.ReleaseButtons;
begin
  Named.SetAsBoolean(Tags[ortOpenMainDoor], False);
  Named.SetAsBoolean(Tags[ortOpenToolDoor], False);
end;


const TSMaxFontSize = 16;

procedure TOCTFormDlg.FormCreate(Sender: TObject);
begin
  CurrentRecoveryOption := roNone;
  OCTData := TOPPList.create;
  OCTdata.OnChange := HandleOCTStatusChange;
  OCTData.OnCellChange := OCTCellUpdate;
  ToolData := TToolList.Create;
  ToolData.OnChange := LazyUpdate;
  MasterToolList := TToolDB.Create;
  RequiredToolData := TRequiredToolList.Create;
  RequiredToolData.OnChange  := LazyUpdate;
  OCTDisplay := TOCTListBox.Create(Self);
  ReadConfiguration;

  with OCTDisplay do
    begin
    OCTList := Self.OCTData;
    ImageList := OCtImages;
    Parent := BaseOCTPanel;
    Align := alClient;
    RequestDisplayReset := DoResetDisplay;
    OnRowClicked := DoOctDisplayClicked;
    RequestSelectAllOPPs := DoSelectAllOpps;
    RequestInvertSelection := DoInvertSelection;
    RequestReduce := DoChangeSize;
    end;

    {
  RToolDisplay := TReqToolList.Create(Self);
  with RToolDisplay do
    begin
    RToolList := Self.RequiredToolData;
    MasterTD := Self.MasterToolList;
    ToolList := SElf.ToolData;
    OCTList := Self.OCTData;
    Parent := ToolManSheet;
    Align := alClient;
    end;
    }
  Top := DisplayTop;
  left := 1;

  BtnShowPartData.MaxFontSize := MaxFontSize;
  BtnClose.MaxFontSize := MaxFontSize;
  BtnFaultAck.MaxFontSize := MaxFontSize;
  BtnToolChuck.MaxFontSize := MaxFontSize;
end;

procedure TOCTFormDlg.UpdateOCTStatus;//(Sender: TOBject);
var
  RemTime : Integer;
begin
  OCTDisplay.UpdateOCTDisplayTrigger := True;
  //   OCTDisplay.StatusText[0] := Format('Loaded OPP %d of %d',[OPPIndex + 1,
  //                                                    OCTData.Count]);

  try
    OCTDisplay.StatusText[0] := Format(Index_DofD,
                             [OCTData.OppsEnabled - OCTData.OPPsRemaining,
                              OCTData.OppsEnabled]);
  except
    OCTDisplay.StatusText[0] := 'Error';
  end;

  try
    OCTDisplay.StatusText[1] := Format(Selected_DofD,
                                     [OCTData.OPPsEnabled,
                                      OCTData.Count]);
  except
    OCTDisplay.StatusText[1]:= 'Error';
  end;

  try
    OCTDisplay.StatusText[2] := Format(Total_Duration_S,
                                     [SecondsToHMS(Round(OCTData.SummedDuration))]);
  except
    OCTDisplay.StatusText[2]:= 'Error';
  end;

  if isOCTMode then
    begin
    RemTime := Round(OCTData.EstimatedCycleTime);
    if RemTime < 0 then RemTime := 0;
       RemTimeDisplay.Caption := SecondsToHMS(RemTime);
    end
  else
       RemTimeDisplay.Caption := '';


end;

procedure TOCTFormDlg.ClearRecoveryOptions;
begin
  InRecovery := False;
  Named.SetAsBoolean(Tags[ortInrecovery],False);
  //OCTDisplay.RecoveryIndex := -1;
  CurrentRecoveryOPtion := roNone;
  SetDisplayMode(odmDefault);
  Resize;
end;

procedure TOCTFormDlg.DisplayRecoveryOptions;
begin
  InRecovery := True;
  Named.SetAsBoolean(Tags[ortInrecovery],True);
  SetButtonPanel(roNone);
  if (not Self.Visible) and (CNCModeAuto in NC.CNCMode) then begin
     Named.SetAsBoolean(SelfStartTag,True);
     Named.SetAsBoolean(SelfStartTag,False);
  end;
  Resize;
end;

procedure TOCTFormDlg.Resize;
begin
  inherited;
  SetDisplayMode(DisplayMode);
end;

procedure TOCTFormDlg.SetButtonPanel(RecOption : TRecoveryOptions);
begin
  RestartCurrentButton.ButtonColor := clWhite;
  SkipButton.ButtonColor := clWhite;
  AbandonOCTButton.ButtonColor := clWhite;
  AbandonPartButton.ButtonColor := clWhite;

  ReworkButton.Enabled:= CellMode.LocalCellMode = cmProduction;
  if CellMode.LocalCellMode = cmProduction then
    ReworkButton.ButtonColor:= clWhite
  else
    ReworkButton.ButtonColor:= $a0a0a0;

  SetAcceptKeyState(False);

  case RecOPtion of
    roNone: begin
      MessageLabel.Caption := PleaseSelText;
      MessageLabel2.Caption := '';
      SetAcceptKeyState(True);
    end;

    roRestartCurrent: begin
      RestartCurrentButton.SetFocus;
      RestartCurrentButton.IsEnabled := True;
      RestartCurrentButton.ButtonColor := clYellow;
      SetAcceptKeyState(True);
      MessageLabel.Caption := PressAcceptAndClearText;
      MessageLabel2.Caption := ThenPressCycleText;
    end;

    roAbandonCurrent: begin
      SkipButton.ButtonColor := clYellow;
      MessageLabel.Caption := PAToABANDONOPPText;
      MessageLabel2.Caption := ThenPressCycleText;
      SetAcceptKeyState(True);
    end;

    roAbandonOCT: begin
      AbandonOCTButton.ButtonColor := clYellow;
      if UsesFixedEndOPP then
         MessageLabel.Caption := Format('%s  %s',[PAToABANDONPartText,ThenPressCycleStartToAbandonText])
      else
         MessageLabel.Caption := PAToABANDONPartText;
      SetAcceptKeyState(True);
    end;

    roAbandonPartInCell: begin
      AbandonPartButton.ButtonColor := clYellow;
      if UsesFixedEndOPP then
         MessageLabel.Caption := Format('%s  %s',[PAToABANDONPartText,ThenPressCycleStartToAbandonText])
      else
         MessageLabel.Caption := PAToABANDONPartText;

      SetAcceptKeyState(True);
    end;

    roReworkOPP : begin
      ReworkButton.ButtonColor := clYellow;
      MessageLabel.Caption := patOReworkText;
      SetAcceptKeyState(True);
    end;

    roNotRework : begin
      MessageLabel.Caption := ReworkPermittedText;
      SetAcceptKeyState(False);
    end;
  end;
end;


procedure TOCTFormDlg.ResetOCT;
begin
  Named.SetAsInteger(Tags[ortOCTReqReset],1);
  Named.SetAsInteger(Tags[ortOCTReqReset],0);
end;

procedure TOCTFormDlg.DoResetDisplay(Sender : TObject);
begin
  //OCTDisplay.RecoveryIndex := -1;
  ResetOCT;
  OCTDisplay.RefreshDisplay;
  UpdateOCTStatus;
end;

procedure TOCTFormDlg.DoSelectAllOpps(Sender : TObject);
begin
  if not OCTisRunning then begin
    OCTData.EnableAllOPPs;
    OCTDisplay.RefreshDisplay;
  end;
end;

procedure TOCTFormDlg.DoInvertSelection(Sender: TObject);
begin
  if not OCTisRunning then begin
    OCTData.InvertSelection;
    OCTData.BeginUpdate;
    if UsesFixedEndOPP then OCTData.OPP[OCTData.Count-1].Enabled := True;
    if UsesFixedStartOPP then OCTData.OPP[0].Enabled := True;
    OCTData.EndUpdate;
    OCTDisplay.RefreshDisplay;
  end;
end;

procedure TOCTFormDlg.DoOctDisplayClicked(Sender: TOBject);
var OPPIndex : Integer;
OppName : String;
begin

  if not Named.GetAsBoolean(Tags[ortOCTRunning]) then begin
    if OCTDisplay.ClickedRow > 0 then begin
      OPPIndex := OCTDisplay.ClickedRow-1;
      if (OPPIndex = 0) and UsesFixedStartOPP then
        begin
        OppName :=  OCTData.OPP[OPPIndex].FileName;
        if UpperCase(OppName) = UpperCase(FixedStartOPP) then
          begin
          exit;
          end
        else
          begin
          //!!!
          end;
        end;
      if (OPPIndex = OCTData.Count-1) and UsesFixedEndOPP then
        begin
        OppName :=  OCTData.OPP[OPPIndex].FileName;
        if UpperCase(OppName) = UpperCase(FixedEndOPP) then
          begin
          exit;
          end
        else
          begin
          //!!!
          end;
        end;
      if OCTData.OPP[OPPIndex].Enabled then begin
        if OCTData.OPPsEnabled <= 1 then
          Exit; // Must always be one clicked
      end;
//      OCTData.Lock;
//      try
      OCTData.OPP[OPPIndex].Enabled := not (OCTData.OPP[OPPIndex].Enabled);
//      finally
//      OCTData.UnLock;
//      end;
//      ResetOCT;
    end;
    OCTDisplay.RefreshDisplay;
  end;
end;


procedure TOCTFormDlg.DoChangeSize(Sender: TObject);
begin
  Named.SetAsBoolean(SelfStartTag,True);
  Named.SetAsBoolean(SelfStartTag,False);
end;

procedure TOCTFormDlg.BtnCloseClick(Sender: TObject);
begin
  Close;
end;

procedure TOCTFormDlg.AbandonPartButtonMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  CurrentRecoveryOption := roAbandonPartInCell;
  SetButtonPanel(roAbandonPartInCell);
end;

procedure TOCTFormDlg.AcceptKeyMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  OPPNumber : Integer;
begin
  OCTdata.BeginUpdate;
  try
    case CurrentRecoveryOPtion of
      roNone: begin
      end;

      roRestartCurrent: begin
        OCTData.OPP[OppIndex].Status := osNone;
        OCTData.OPP[OppIndex].ReworkStatus := OCTData.OPP[OppIndex].ReworkStatus+'R';
      end;

      roAbandonCurrent: begin
        OCTData.OPP[OppIndex].Status := osAbandoned;
      end;

      roAbandonOCT: begin
        if UsesFixedEndOPP then begin
          if (OppIndex = OCTData.Count-1) then begin
           // dont set the last one to abandon  it is the unload part opp
           OCTData.OPP[OppIndex].Status := osNone;
          end else begin
            //for OppNumber := OCTDisplay.RecoveryIndex to OCTData.Count-2 do begin
            for OppNumber := OppIndex to OCTData.Count-2 do begin
              OCTData.OPP[OppNumber].Status := osAbandoned;
            end;
          end
        end else begin
          //for OppNumber := OCTDisplay.RecoveryIndex to OCTData.Count-1 do begin
          for OppNumber := OppIndex to OCTData.Count-1 do begin
            OCTData.OPP[OppNumber].Status := osAbandoned;
          end;
        end;
      end;

      roReworkOPP: begin
        if (CellMode.LocalCellMode = cmProduction) and
           (CurrentRecoveryOption = roReworkOPP) then begin
          CellMode.CellModeForm.SetCellMode(cmRework);
          CellMode.SetInterrupted(True);
        end;
      end;

      roAbandonPartInCell,
      roNotRework: begin
      end;
    end; // case
  finally
    ClearRecoveryOptions;
    OCTdata.EndUpdate;
    OCTDisplay.UpdateOCTDisplayTrigger := True;
  end;
end;

procedure TOCTFormDlg.RestartCurrentButtonMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  CurrentRecoveryOption := roRestartCurrent;
  SetButtonPanel(CurrentRecoveryOption);
end;

procedure TOCTFormDlg.SkipButtonMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  if OppIndex <> 0 then begin
    CurrentRecoveryOption := roAbandonCurrent;
    SetButtonPanel(CurrentRecoveryOption);
  end;
end;

procedure TOCTFormDlg.AbandonOctButtonMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
   CurrentRecoveryOption := roAbandonOCT;
   SetButtonPanel(CurrentRecoveryOption);
end;

procedure TOCTFormDlg.ReworkButtonMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if OppIndex <> 0 then begin
    CurrentRecoveryOption := roReworkOPP;
    SetButtonPanel(roReworkOPP);
  end;
end;

procedure TOCTFormDlg.BtnGeneralMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  if Sender = BtnOpenToolDoor then begin
    Named.SetAsBoolean(Tags[ortOpenToolDoor], True);
  end else if Sender = BtnOpenMainDoor then begin
    Named.SetAsBoolean(Tags[ortOpenMainDoor], True);
  end else if Sender = BtnFaultAck then begin
       Named.SetAsBoolean(Tags[ortFaultAcknowledgeTag], True);
  end else if Sender = BtnPartChuck then begin
    Named.SetAsBoolean(Tags[ortReqChuckChange], True);
  end else if Sender = BtnToolChuck then begin
    Named.SetAsBoolean(Tags[ortReqToolChuckChange], True);
  end;
  ButtonReleaseCount:= 10;
end;

procedure TOCTFormDlg.BtnGeneralMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  Tool: WideString;
  ToolSN: Integer;
begin
  if Sender = BtnOpenToolDoor then begin
    Named.SetAsBoolean(Tags[ortOpenToolDoor], False);
  end
  else if Sender = BtnOpenMainDoor then begin
    Named.SetAsBoolean(Tags[ortOpenMainDoor], False);
  end
  else if Sender = BtnPartChuck then begin
    Named.SetAsBoolean(Tags[ortReqChuckChange], False);
  end
  else if Sender = BtnToolChuck then begin
    Named.SetAsBoolean(Tags[ortReqToolChuckChange], False);
  end
  else if Sender = BtnResetTool then begin
   if not NC.InCycle {and (CNCModeManual in NC.CNCMode)} then begin
      if ToolLife.GetEx1ToolType(0, Tool) = trOK then begin
        ToolLife.GetEx1ToolSN(0, ToolSN);
        ToolLife.ReplenishTool(Tool, ToolSN);
      end;
    end
    else
      Named.SetFaultA('', 'Cannot reset in cycle', FaultInterlock, nil);
  end
  else if Sender = btnInvalidate then begin
    if ToolData.IsToolAtEx1(0)  then begin
       if TAreYouSure.Execute('Are you sure you want to invalidate the tool in head?') then
           ZeroToolLifeForToolInLocation(0);
    end;
  end
  else if Sender = BtnFaultAck then begin
       Named.SetAsBoolean(Tags[ortFaultAcknowledgeTag], True);
       Named.SetAsBoolean(Tags[ortFaultAcknowledgeTag], False);
  end
  else if Sender = BtnShowPartData then begin
    Named.SetAsBoolean(Tags[ortPartStatusForm], True);
    Named.SetAsBoolean(Tags[ortPartStatusForm], False);
  end;
end;


procedure TOCTFormDlg.UpdateStnButtons;
  procedure RemoveButton(Tool: TToolData; Button: TThreeLineButton; c: TColor; Text: string);
  begin
    if Tool.BarrelCount > 1 then begin
      Button.Legend := Tool.ShuttleID + '~' +
                  Text + '~' +
                  Format('%.1f', [Tool.BarrelRemaining]);
    end else begin
      Button.Legend := Tool.ShuttleID + '~' +
                  Text + '~' +
                  Format('TPM %d', [Tool.TPM1]);
    end;
    Button.ButtonColor:= c;
    Button.LegendFont.Color := clWhite;
  end;

  procedure EmptyButton(Tool: TToolData; Button: TThreeLineButton);
  begin
    Button.Legend := EMPTYButtonText;
    Button.ButtonColor := clBlack;
    Button.LegendFont.Color := clWhite;
  end;

  const DisplayTPM = True;

  procedure HealthyButton(Tool: TToolData; Button: TThreeLineButton; Color: TColor);
  var Tmp: string;
  begin
    if Tool.BarrelCount > 1 then
      Tmp:= Tool.ShuttleID + '~' +
               IntToStr(Tool.ShuttleSN) + '~' +
               Format('%d:%.1f', [Tool.RemainingBarrels, Tool.BarrelRemaining])
    else
      if not DisplayTPM then
        Tmp:= Tool.ShuttleID + '~' +
                 IntToStr(Tool.ShuttleSN) + '~' +
                 Format('%.1f', [Tool.BarrelRemaining])
      else
        Tmp:= Tool.ShuttleID + '~' +
                 '#' + IntToStr(Tool.ShuttleSN) + ' - (' + IntToStr(Tool.TPM1) + ')~' +
                 Format('%.1f', [Tool.BarrelRemaining]);
    Button.Legend := Tmp;
    Button.ButtonColor := Color;
    Button.LegendFont.Color := clBlack;
  end;

  procedure AddButton(Tool: TToolData; Button: TThreeLineButton);
  begin
    Button.Legend :=  AddText;
    Button.ButtonColor := clBlue;
    Button.LegendFont.Color := clWhite;
  end;

var Ex1: Integer;
    Button : TThreeLineButton;
    Tool: TToolData;
    AddLatch: Boolean;
begin
  if not Assigned(ToolData) then Exit;
  if not Assigned(RequiredToolData) then Exit;

  ToolData.Lock;
  try

    AddLatch:= RequiredToolData.ToolsAreRequired;
    Named.SetAsBoolean(Tags[ ortToolRequired ], AddLatch);
    Button:= btnHead;

    Tool:= ToolData.ToolAtEx1(Ex1);

    if isOCTMode then begin
      if Assigned(Tool) then begin
        if RequiredToolData.IndexOf(Tool.ShuttleID) <> -1 then begin
          if OCTData.MinimumToolLength(Tool.ShuttleID) < Tool.TotalRemaining then begin
            HealthyButton(Tool, Button, clLime);
          end else if OCTData.CanBeUSed(Tool.ShuttleID, Tool.TotalRemaining)  then begin
            HealthyButton(Tool, Button, $00B000);
          end else begin
            RemoveButton(Tool, Button, clRed, RemoveText); // Orange
          end;
        end else begin
          if AddLatch and (ToolData.Count >= Stations) then begin
            RemoveButton(Tool, Button, clRed, RemoveText);
            AddLatch:= False;
          end else
            RemoveButton(Tool, Button, $001176FF, 'Not Required'); // Orange
        end;
      end else begin
        if AddLatch and (Ex1 <> 0) and (ToolData.Count < Stations) then begin
          AddButton(Tool, Button);
          AddLatch:= False;
        end else begin
          EmptyButton(Tool, Button);
        end;
      end;
    end else begin
      Button.Flash := False;
      if Assigned(Tool) then begin
        HealthyButton(Tool, Button, clLime);
      end
      else begin
        EmptyButton(Tool, Button);
      end;
    end;

  finally
    ToolData.Unlock;
  end;
end;


procedure TOCTFormDlg.UpdatePartProgDisplay(ForceProgLoad : Boolean);
var
ProgramText: WideString;
ActSubIndex : Integer;
SubName     : array [0..256] of Char;
DisplayName : String;
ExtStart    : Integer;
Buffer      : String;
begin
ActSubIndex :=NC.ActiveSubRoutine;
if (ForceProgLoad or ((ActSubIndex <> LastSubIndex) and (ActSubIndex >= 0))) then
   begin
   LastSubIndex  := ActSubIndex;
   if assigned(PrDisplay) then
      begin
         try
         ProgramText := NC.SubRoutine[ActSubIndex];
         except
         ProgramText := InvalidProgramText;
         end;
      PrDisplay.LoadProgramText(ProgramText);
      end;
   end;
if ActSubIndex >= 0 then
   begin
   PrDisplay.CurrentLine := NC.GetProgramLine;
   ProgLineDisplay.Caption := Format('%5d : %5d',[PrDisplay.CurrentLine+1,PrDisplay.LineCount]);
   DisplayName := ExtractFileName(OCTFilename);
   ExtStart := Pos('.',DisplayName);
   If ExtStart > 0 then
      begin
      DisplayName := Copy(DisplayName,1,ExtStart-1);
      end;
   Named.GetAsString(Tags[ortLoadedOPPName],SubName,255);
   Buffer := Subname;
   Buffer := ExtractFileName(Buffer);
   ExtStart := Pos('.',Buffer);
   If ExtStart > 0 then
      begin
      Buffer := Copy(Buffer,1,ExtStart-1);
      end;

   DisplayName := Format('%s-%s',[DisplayName,Buffer]);
   ProgNameDisplay.Caption := DisplayName;
   end;
end;

procedure TOCTFormDlg.BtnCloseMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
Close;
end;

procedure TOCTFormDlg.BtnDefaultDisplayMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
SetDisplayMode(odmDefault);
end;

procedure TOCTFormDlg.SetSoftButtonEnables(InRec : Boolean;OCTMode : Boolean);
begin

  BtnInvalidate.IsEnabled := ToolData.IsToolAtEx1(0)
                             and not Named.GetAsBoolean(Tags[ortOCtRunning])
                             and not Named.GetAsBoolean(Tags[ortInCycle]);
end;

procedure TOCTFormDlg.CellModeChange(ATag: TTagRef);
var Mode: TCellMode;
begin
  Mode:= TCellMode(Named.GetAsInteger(ATag));
  if Mode = cmProduction then begin
    if CellMode.IsInterrupted then begin
      ClearRecoveryOptions;
      DisplayRecoveryOptions;
    end;
  end;
end;


procedure TOCTFormDlg.SetDisplayMode(DMode: TOctDisplayModes);
begin
  DisplayMode := DMode;
  SetSoftButtonEnables(InRecovery,isOCTMode);
  //ToolDisplayToggle.Visible := Named.GetASBoolean(Tags[ortTCFitted]);

  if Inrecovery then
    begin
     BaseOCTPanel.Align := alClient;
     OCTDisplay.Align := alTop;
     OCTDisplay.Height := 300;
     AcceptKey.IsEnabled := False;
     AcceptKey.Visible := True;

     RecoveryPanel.Visible := True;
     RecoveryPanel.Height := BaseOCTPanel.Height-OCTDisplay.Height-10;
     OCTDisplay.Width := Width- RecoveryPanel.Width - 5;
     OCTDisplay.Left := RecoveryPanel.Width+5;
     OCTLowerPageControl.Visible := False;
     end
  else
      begin
      case  DisplayMode of
         odmDefault:
         begin
         OCTLowerPageControl.Visible := True;
         BaseOCTPanel.Visible := True;
         RecoveryPanel.Visible := False;
         OCTDisplay.Align := alClient;
         BaseOCTPanel.Align := alTop;
         BaseOCTPanel.Height := 375;
         //OCTLowerPageControl.ActivePage := ToolManSheet;
         end;

         odmAllOPP:
         begin
         BaseOCTPanel.Visible := True;
         OCTLowerPageControl.Visible := False;
         BaseOCTPanel.Align := alClient;
         end;

         odmPartProgram:
         begin
         BaseOCTPanel.Visible := True;
         OCTLowerPageControl.Visible := True;
         OCTLowerPageControl.ActivePage := PDisplaySheet;
         end;

         odmToolreq:
         begin
         BaseOCTPanel.Visible := True;
         OCTLowerPageControl.Visible := True;
         //OCTLowerPageControl.ActivePage := ToolManSheet;
         end;

         odmNotOCT:
         begin
         BaseOCTPanel.Visible := False;
         OCTLowerPageControl.Visible := True;
         OCTLowerPageControl.ActivePage := PDisplaySheet;
         OCTLowerPageControl.Align := alClient;
         end;
      end;
  end;

end;

procedure TOCTFormDlg.Panel12Resize(Sender: TObject);
begin
MessageLabel.Height := (Panel12.Height div 2)-2;
MessageLabel2.Height := (Panel12.Height div 2)-2;

end;

procedure TOCTFormDlg.SetAcceptKeyState(Enabled: Boolean);
begin
AcceptKey.IsEnabled := Enabled;
if enabled then
   begin
   Acceptkey.ButtonColor := clLime;
   end
else
    begin
    Acceptkey.ButtonColor := clGray;
    end;
end;

procedure TOCTFormDlg.BaseOCTPanelResize(Sender: TObject);
begin
  if OCTisRunning then begin
    OCTDisplay.RunningRow := OPPIndex + 1;
    OCTDisplay.RefreshDisplay;
    end;
end;


procedure TOCTFormDlg.UpdateButtonEnable;
begin
 BtnOPenToolDoor.IsEnabled := not Named.GetAsBoolean(Tags[ortToolDoorOpenInhibit]);
 BtnOPenMainDoor.IsEnabled := not Named.GetAsBoolean(Tags[ortMainDoorOPenInhibit]);
 BtnToolChuck.IsEnabled :=  Named.GetAsBoolean(Tags[ortToolClampPermissive]);
 BtnPartChuck.IsEnabled :=  Named.GetAsBoolean(Tags[ortPartClampPermissive]);
end;


procedure TOCTFormDlg.SetStatus(const Text: string; Index: Integer);
begin
  case Index of
    1 : LabelStatus1.Caption := Text;
    2 : LabelStatus2.Caption := Text;
  end;
if (LabelStatus1.Caption = '') and ( LabelStatus2.Caption = '') then
   begin
   OctStatusPanel.Height := 4;
   end
else
   begin
   OctStatusPanel.Height := 40;
   end
end;

procedure TOCTFormDlg.ReadConfiguration;
var I : TODisplayReportingTags;
    Ini : TIniFile;
begin
if FileExists(DllProfilePath + 'oct.cfg') then
  begin
  Ini := TIniFile.Create(DllProfilePath + 'oct.cfg');
  try
    DisplayHeight := Ini.ReadInteger('OperatorDisplay','DisplayHeight',685);
    DisplayTop := Ini.ReadInteger('OperatorDisplay','DisplayTop',70);
    FixedEndOPP   := ExtractFileName(Ini.ReadString('OCT_TM', 'FixedEndOPP', ''));
    UsesFixedEndOPP := (FixedEndOPP <> '');
    FixedStartOPP := ExtractFileName(Ini.ReadString('OCT_TM', 'FixedStartOPP', ''));
    UsesFixedStartOPP := (FixedStartOPP <> '');

    for I := Low(TODisplayReportingTags) to High(TODisplayReportingTags) do
      TagNames[I] := Ini.ReadString('Tags', TagDefinitions[I].Ini, TagDefinitions[I].Tag);

    ShowZeroLifeButton := Ini.ReadBool('OperatorDisplay','ShowZeroLifeButton',False);
    ShowResetButton := Ini.ReadBool('OperatorDisplay','ShowResetButton',False);

    BtnInvalidate.Visible := ShowZeroLifeButton;
    if not ShowZeroLifeButton then  InvalidateBlank.Align := alClient;
    InvalidateBlank.Visible := not BtnInvalidate.Visible;

    UpdateSweepCount := Ini.ReadInteger('OperatorDisplay','UpdateDelayInterval',10);
    CurrentUpdateCount := 0;


    BtnShowPartData.Enabled:= True;

    Stations:= Ini.ReadInteger('OperatorDisplay', 'StationCount', 8);
  finally
    Ini.Free;
  end;
  end;
end;


function TOCTFormDlg.OCTisRunning: Boolean;
begin
  Result := Named.GetAsBoolean(Tags[ortOCTRunning])
end;


function TOCTFormDlg.isOCTMode: Boolean;
begin
  Result := Named.GetAsBoolean(Tags[ortOCTMode])
end;

function TOCTFormDlg.OPPIndex: Integer;
begin
  Result := Named.GetAsInteger(Tags[ortOCTIndex])
end;

function TOCTFormDlg.OCTFilename: String;
var ctmp : array [0..128] of Char;
begin
  Named.GetAsString(Tags[ortLoadedOCTName],ctmp,Length(ctmp) - 1);
  Result := ctmp;
end;



procedure TOCTFormDlg.LazyUpdate(Sender : TObject);
begin
  LazyUpdateFlag := True;
end;


procedure TOCTFormDlg.ZeroToolLifeForToolInLocation(Location : Integer);
var
  ToolNumber : Integer;
  Found : Boolean;
begin
ToolNumber := 0;
Found := False;
ToolData.Lock;
try
// First find what is at location
while (ToolData.Count > ToolNumber) and not Found do
  begin
  if ToolData[ToolNumber].Ex1 = Location then
      begin
      Found := True;
      ToolData[ToolNumber].RemainingBarrels := 0;
      ToolData[ToolNumber].ActToolLength := ToolData[ToolNumber].MinToolLength;
      end;
  inc(ToolNumber);
  end;
finally
ToolData.Unlock;
end;
end;


procedure TOCTFormDlg.HandleOCTStatusChange(Sender: TObject);
var
ChangedOPP : TOPP;
OPPIndex : Integer;
begin
// HERE TO CHECK FOR RECOVERY MODE
OPPIndex := Named.GetAsInteger(Tags[ortOCTIndex]);
  with OCTData do begin
    ChangedOPP := OPP[OPPIndex];
    if ChangedOPP.Status <> LastStatus then begin
      LastStatus := ChangedOPP.Status;
      OCTDisplay.UpdateOCTDisplayTrigger := True;
      if ChangedOPP.Status = osInRecovery then begin
        if not InRecovery then begin
          DisplayRecoveryOptions;
          //OCTDisplay.RecoveryIndex := OPPIndex;
        end;
      end else if ChangedOPP.Status = osStarted then begin
      end;
    end; // <> last Status
  end;   // With OCTData

end;

procedure TOCTFormDlg.OCTRunningChanged(Running: Boolean);
begin
if Running then
  begin
  OCTDisplay.DisplayResetButtonEnabled := False;
  OCTDisplay.SelectAllButtonEnabled := False;

  if InRecovery then
    begin
    ClearRecoveryOptions();
    end
  else
    begin
    CurrentRecoveryOption := roNone;
    InRecovery := False;
    //OCTDisplay.RecoveryIndex := -1;
    end;
  end
else
  begin
  OCTDisplay.DisplayResetButtonEnabled := True;
  OCTDisplay.SelectAllButtonEnabled := True;
  end;

end;

function TOCTFormDlg.SecondsToHMS(Seconds: Integer): string;
var Tmp, Hours, Mins, Secs: Integer;
begin
  Tmp:= Round(Seconds);
  Hours:= Tmp div 3600;
  Mins:= Tmp mod 3600;
  Secs:= Mins mod 60;
  Mins:= Mins div 60;
  if Hours > 0 then
    Result:= Format('%.2d:%.2d:%.2d', [Hours, Mins, Secs])
  else
    Result:= Format('%.2d:%.2d', [Mins, Secs]);
end;

procedure TOCTFormDlg.OCTCellUpdate(Sender: TObject; Row : Integer; Field : WideString);
begin
OCTDisplay.CellUPdate(Row,Field);
end;


procedure TOCTFormDlg.FormShow(Sender: TObject);
begin
  Top := DisplayTop;
  Height := DisplayHeight;
  Left :=1;
end;

initialization
  TAbstractFormPlugin.AddPlugin('OperatorDialog', TOCTFormDlg);
end.
