unit FileLoadDialog;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Grids, NamedPlugin, StdCtrls, ACNC_BaseKeyboards, OCTTypes, CNCTypes,
  ACNC_TouchSoftkey, FileCtrl, Directory;

type
  TFileLoadForm = class(TForm)
    Panel1: TPanel;
    BtnOK: TTouchSoftkey;
    BtnCancel: TTouchSoftkey;
    Panel2: TPanel;
    Panel4: TPanel;
    DirGrid: TStringGrid;
    Panel6: TPanel;
    PanelKeyboard: TPanel;
    Panel5: TPanel;
    FileGrid: TStringGrid;
    PathLabel: TLabel;
    BtnPathSwitch: TTouchSoftkey;
    Panel3: TPanel;
    SaveEdit: TEdit;
    Label1: TLabel;
    procedure DirGridDblClick(Sender: TObject);
    procedure BtnOKClick(Sender: TObject);
    procedure BtnCancelClick(Sender: TObject);
    procedure FileGridSelectCell(Sender: TObject; ACol, ARow: Integer;
      var CanSelect: Boolean);
    procedure FileGridClick(Sender: TObject);
    procedure BtnUploadClick(Sender: TObject);
    procedure BtnPathSwitchClick(Sender: TObject);
    procedure DirGridEnter(Sender: TObject);
    procedure FileGridEnter(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure DirGridDrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure FileGridKeyPress(Sender: TObject; var Key: Char);
  private
    FBasePath : string;
    FBasePathName : string;

    FileMask : string;
    CurrentPath : string;
    SelectedFile : string;
    FilePath : string;

    KB : TBaseKB;
    FKB : TNumericPad;
    DevelMode : Boolean;
    DirGridActive : Boolean;
    FSaveMode : Boolean;
    procedure SetSaveMode(aMode : Boolean);
    procedure DoKeyPress(Sender : TObject; Key: AnsiChar);
    procedure RefreshDirectory;
    procedure SetBasePath(const Name, Path : string);
    procedure SetSaveFileName(const Name : string);
    function GetSaveFileName : string;
    function PermissiveAccess : Boolean;
  public
    constructor Create(AOwner : TComponent); override;
    property BasePath : string read FBasePath;
    property SaveMode : Boolean read FSaveMode write SetSaveMode;
    property SaveFileName : string read GetSaveFileName write SetSaveFileName;
  end;

var
  FileLoadForm: TFileLoadForm;

function GeneralPluginSelectProgram(ReqPath, RespPath : PChar; Load : Boolean) : Boolean; stdcall;
function RefreshBufferedPrograms : Boolean;

exports GeneralPluginSelectProgram;

implementation

uses CellMode, AccessAdministration, StandAloneOCT;

{$R *.DFM}

const
  GridHeight = 32;

function GetManifestOfOCT(Path : string; S : TStringList) : Boolean;
var I : Integer;
    Tmp : string;
begin
  Result := True;
  try
    S.LoadFromFile(Path);
    for I := 0 to S.Count - 1 do begin
      Tmp := S[I];
      S[I] := ReadQuotedString(Tmp);
    end;
  except
    Result := False;
  end;
end;

procedure DeleteAllFilesFromBuffered(const Path : string; WithOCT : Boolean = True);
var Directory : TDirectory;
    S : TStringList;
begin
  Directory := TDirectory.Create;
  S := TStringList.Create;

  try
    Directory.Manifest(Path + '*.opp', S, False);
    if WithOCT then
      Directory.Manifest(Path + '*.oct', S, False);
    while(S.Count > 0) do begin
      if FileExists(S[0]) then
        DeleteFile(S[0]);
      S.Delete(0);
    end;
  finally
    Directory.Free;
    S.Free;
  end
end;

function RefreshBufferedPrograms : Boolean;
var S : TStringList;
    PP : string;
    I : Integer;
begin
  Result := True;

  if (CellMode.LocalCellMode = rrmProduction) and
     (CellMode.CellOCT <> '') then begin
    Result := True;
    CreateDirectory(PChar(CellMode.XFerPath), nil);
    DeleteAllFilesFromBuffered(CellMode.XFerPath);

    S := TStringList.Create;
    try
      if GetManifestOfOCT(CellMode.CellOCT, S) then begin // this is a bit wrong

        PP := CellMode.CellOPPPath + CellMode.CellPartTypeFromPath + '\';
        Named.EventLog(Format('CellPartTypeFromPath = %s', [CellMode.CellPartTypeFromPath]));
        for I := 0 to S.Count - 1 do begin
          if not CopyFile(PChar(PP + S[I]), PChar(CellMode.XFerPath + S[I]), False) then
            Exit;
        end;

        PP := CellMode.XFerPath;

        DeleteAllFilesFromBuffered(CellMode.BufferedPath, False);

        for I := 0 to S.Count - 1 do begin
          if not CopyFile(PChar(PP + S[I]), PChar(CellMode.BufferedPath + S[I]), False) then
            raise Exception.Create('OH DEAR');
        end;

        Result := False;
      end;
    finally
      S.Free;
    end;
  end;
end;

function ClearAndCopyFiles(var SelectPath : string; const TargetPath : string; OPPMode : Boolean) : Boolean;
var Tmp : string;
    OPPPath : string;
    I : Integer;
    Directory : TDirectory;
    S : TStringList;
begin
  Result := True;
  // Stage 1 remove all files from the buffed path

  if CellMode.LocalCellMode <> rrmRework then begin
    DeleteAllFilesFromBuffered(CellMode.BufferedPath);
    CellMode.CellOCT := ''; // Remove Cell OCT if not loading a rework program
  end;

  // Stage 2 determine mode and copy the selected files
  case CellMode.LocalCellMode of
    rrmProduction : begin
      Tmp := SelectPath;
    // Determine the part type from the SelectPath and therefore calculate the
    // OPP path
    // The SelectPath = CellMode.CellOCTPath/parttype/...../filename.oct
    // Strip the CellMode.CellOCTPath from the beginning
      System.Delete(Tmp, 1, Length(CellMode.CellOCTPath));
    // This should include the trailing backslash of CellOCTPath, now find the
    // position of the first '\' and copy this to part type
      I := Pos('\', Tmp);
      if I = 0 then begin
        Named.SetFaultA('OCT', 'Invalid path to part configuration', FaultRewind, nil);
        Result := False;
        Exit;
      end;

      System.Delete(Tmp, I, Length(Tmp)); // remove trailing backslash
      CellMode.CellPartTypeFromPath := Tmp;

      OPPPath := CellMode.CellOPPPath + CellMode.CellPartTypeFromPath + '\';

      Directory := TDirectory.Create;
      Directory.RelativePaths := True;
      S := TStringList.Create;
      try
        Result := GetManifestOfOCT(SelectPath, S);
        if Result then begin
          while S.Count > 0 do begin
            if not CopyFile(PChar(OPPPath + S[0]), PChar(CellMode.BufferedPath + S[0]), False) then begin
              Result := False;
              Named.SetFaultA('OCT', Format('Could not copy configuration opp %s', [S[0]]), FaultRewind, nil);
              Named.SetFaultA('OCT', Format('Could not copy configuration opp %s', [S[0]]), FaultInterlock, nil);
            end;
            S.Delete(0);
          end;
        end;
      finally
        Directory.Free;
        S.Free;
      end;

      CopyFile(PChar(SelectPath), PChar(CellMode.BufferedPath + ExtractFileName(SelectPath)), False);
      SelectPath := CellMode.BufferedPath + ExtractFileName(SelectPath);
    end;

    rrmStandalone,
    rrmRework,
    rrmDevelopment : Exit;
  end;
end;

function GeneralPluginSelectProgram(ReqPath, RespPath : PChar; Load : Boolean) : Boolean;
var Form : TFileLoadForm;
    FileMask : string;
    BasePath : string;
    BasePathName : string;
    OPPMode : Boolean;
    NewPath : string;
begin
  FileMask := ExtractFileExt(ReqPath);
  OPPMode := FileMask = '.opp';
  Result := False;
  
  case CellMode.LocalCellMode of
    rrmProduction : begin
      BasePath := CellMode.CellOCTPath;
      BasePathName := '$Cell';
      if OPPMode then begin
        Named.SetFault('FileLoad', 'Cannot load OPP in cell mode', FaultInterlock);
        Exit;
      end;
    end;
    rrmStandalone : begin
      BasePath := DllLocalPath + 'programs\';
      BasePathName := '$Local';
    end;
    rrmRework : begin
      if CellMode.CellPartTypeFromPath = '' then begin
        BasePathName := '$Rework';
        BasePath := CellMode.ReworkPath;
      end else begin
        BasePathName := '$Rework\' + CellPartTypeFromPath + '\';
        BasePath := CellMode.ReworkPath + CellPartTypeFromPath + '\';
      end;

      if not OPPMode then begin
        Named.SetFault('FileLoad', 'Cannot load OCT in rework mode', FaultInterlock);
        Exit;
      end;
    end;
    rrmDevelopment : begin
      BasePath := DllLocalPath + 'programs\';
      BasePathName := '$Local';
//      BasePath := CellMode.DevelopmentPath;
//      BasePathName := '$Devel';
    end;
  end;

  if Load then begin
    Result := False;
    if not DirectoryExists(ExcludeTrailingBackslash(BasePath)) then begin
      Named.SetFault('FileLoad', 'Base path is not accessable', FaultInterlock);
      Exit;
    end;

    Form := TFileLoadForm.Create(nil);
    try
      Form.FileMask := '*' + FileMask;
      Form.SetBasePath(BasePathName, BasePath);
      Form.RefreshDirectory;

      Result := Form.ShowModal = mrOK;
      if Result then begin
        CellMode.CellPartTypeFromPath := '';
        NewPath := Form.FilePath;
        Result := ClearAndCopyFiles(NewPath, CellMode.BufferedPath, OPPMode);

        if Result then begin
          Named.SetAsBoolean( OCT.Tags[ortRunningBuffered], False);
          case CellMode.LocalCellMode of
            rrmProduction : begin
              if not OPPMode then 
                CellMode.CellOCT := NewPath; // Record the active cell program
            end;
          end;

          StrPLCopy(RespPath, NewPath, 256);
        end;
      end;
    finally
      Form.Free;
    end;
  end else begin
    Form := TFileLoadForm.Create(nil);
    try
      Form.FileMask := '*' + FileMask;
      Form.SetBasePath(BasePathName, BasePath);
      Form.RefreshDirectory;
      Form.SaveMode := True;
      Form.SaveFileName := ExtractFileName(ReqPath);

      Result := Form.ShowModal = mrOK;
      if Result then begin
        NewPath := Form.FilePath;
        if ExtractFileExt(NewPath) = '' then
          NewPath := NewPath + '.opp';

        StrPLCopy(RespPath, NewPath, 256);
      end;
    finally
      Form.Free;
    end;
  end;
end;

{ TFileLoadForm }

constructor TFileLoadForm.Create(AOwner: TComponent);
begin
  inherited;

  CurrentPath := '';
  FileMask := '*.opp';

  KB := TBaseKB.Create(Self);
  KB.Parent := PanelKeyboard;
  KB.Align := alLeft;
  KB.Width := 740;
  KB.OnKey := DoKeyPress;

  FKB := TNumericPad.Create(Self);
  FKB.Parent := PanelKeyboard;
  FKB.Align := alClient;

  BtnPathSwitch.Visible := (CellMode.LocalCellMode = rrmDevelopment) and SaveMode;

  FileGrid.ColCount := 2;
  FileGrid.ColWidths[0] := 580;
  FileGrid.ColWidths[1] := 110;
  FileGrid.Cells[0, 0] := 'File name';
  FileGrid.Cells[1, 0] := 'Size';

  Left := 2;
  Top := 74;
  Width := 1020;
  Height := 768-76
end;

procedure TFileLoadForm.RefreshDirectory;
var Search : TSearchRec;
    Count, I : Integer;
    S1 : TStringList;
begin
  DirGrid.RowCount := 1;
  DirGrid.Cells[0, 0] := 'No directories';

  PathLabel.Caption := FBasePathName + CurrentPath;

  S1 := TStringList.Create;
  try
    if FindFirst(BasePath + CurrentPath + '*', faAnyFile, Search) = 0 then begin
      try
        repeat
          if (Search.Name <> '.') and ((Search.Attr and faDirectory) <> 0) then begin
            if (Search.Name <> '..') or (CurrentPath <> '') then begin
              S1.Add(Search.Name);
            end;
          end;
        until FindNext(Search) <> 0;
      finally
        FindClose(Search);
      end;
    end;;

    S1.Sort;
    if S1.Count > 0 then begin
      DirGrid.RowCount := S1.Count;
      DirGrid.Cols[0] := S1;
    end;

    S1.Clear;

    FileGrid.RowCount := 2;
    FileGrid.FixedRows := 1;
    FileGrid.Cells[0, 1] := 'No Files';
    FileGrid.Cells[1, 1] := '';
    FileGrid.Cells[2, 1] := '';
    if FindFirst(BasePath + CurrentPath + FileMask, faAnyFile, Search) = 0 then begin
      try
        repeat
          if (Search.Attr and faDirectory) = 0 then begin
            S1.AddObject(Search.Name, Pointer(Search.Size));
          end;
        until FindNext(Search) <> 0;
      finally
        FindClose(Search);
      end;
    end;

    S1.Sort;
    if S1.Count > 0 then begin
      FileGrid.RowCount := S1.Count + 1;
      for I := 0 to S1.Count - 1 do begin
        FileGrid.Cells[0, I + 1] := S1[I];
        FileGrid.Cells[1, I + 1] := IntToStr(Integer(S1.Objects[i]));
      end;
    end;

  finally
    S1.Free;
  end;
end;

procedure TFileLoadForm.DirGridDblClick(Sender: TObject);
var Select : string;
    Tmp : string;
begin
  if DirGrid.Row <> -1 then begin
    Select := DirGrid.Cells[0, DirGrid.Row];
    if Select <> '' then begin
      if Select = '..' then
        Tmp := ExtractFilePath(ExcludeTrailingBackslash(CurrentPath))
      else
        Tmp := CurrentPath + Select + '\';
    end;
    if DirectoryExists(BasePath + Tmp) then
      CurrentPath := Tmp;
  end;
  RefreshDirectory;
end;

procedure TFileLoadForm.BtnOKClick(Sender: TObject);
begin
  if not SaveMode then begin
    FilePath := BasePath + CurrentPath + SelectedFile;
    if FileExists(FilePath) then
      ModalResult := mrOK;
  end else begin
    FilePath := BasePath + CurrentPath + SaveEdit.Text;
    ModalResult := mrOK;
  end;
end;

procedure TFileLoadForm.BtnCancelClick(Sender: TObject);
begin
  ModalResult := mrCancel;
end;

procedure TFileLoadForm.FileGridSelectCell(Sender: TObject; ACol,
  ARow: Integer; var CanSelect: Boolean);
begin
  SelectedFile := FileGrid.Cells[0, ARow];
end;

procedure TFileLoadForm.FileGridClick(Sender: TObject);
begin
  SelectedFile := FileGrid.Cells[0, FileGrid.Row];
  if SaveMode then
    SaveEdit.Text := SelectedFile;
end;

procedure TFileLoadForm.BtnUploadClick(Sender: TObject);
var Tmp : string;
begin
  FilePath := BasePath + CurrentPath + SelectedFile;
  if FileExists(FilePath) then begin
    Tmp := CellMode.DevelopmentPath + SelectedFile;
    if FileExists(Tmp) then begin
      if MessageDlg('File exists in development, overwrite?', mtWarning, [mbOK, mbCancel], 0) <> mrOK then
        Exit;

      CopyFile(PChar(FilePath), PChar(Tmp), False);
    end;
  end;
end;

procedure TFileLoadForm.BtnPathSwitchClick(Sender: TObject);
begin
  if DevelMode then begin
    SetBasePath('$Local', CellMode.LocalPath);
  end else begin
    SetBasePath('$Devel', CellMode.DevelopmentPath);
  end;
  CurrentPath := '';
  RefreshDirectory;

  DevelMode := not DevelMode;
end;

procedure TFileLoadForm.SetBasePath(const Name, Path : string);
begin
  FBasePath := Path;
  FBasePathName := Name + '\';
end;

procedure TFileLoadForm.SetSaveFileName(const Name : string);
begin
  SaveEdit.Text := Name;
end;

function TFileLoadForm.GetSaveFileName : string;
begin
  Result := SaveEdit.Text;
end;

procedure TFileLoadForm.DoKeyPress(Sender : TObject; Key: AnsiChar);
begin
  if Key = #$d then begin
    if DirGridActive then
      DirGridDblClick(Self)
    else
      BtnOKClick(Self);
  end;
end;

procedure TFileLoadForm.SetSaveMode(aMode : Boolean);
begin
  FSaveMode := aMode;
  SaveEdit.Enabled := SaveMode;
end;

procedure TFileLoadForm.DirGridEnter(Sender: TObject);
begin
  DirGridActive := True;
end;

procedure TFileLoadForm.FileGridEnter(Sender: TObject);
begin
  DirGridActive := False;
end;

function TFileLoadForm.PermissiveAccess : Boolean;
var AL : TAccessLevels;
begin
  AL := TAccessLevels(Byte(Named.GetAsInteger(OCT.Tags[ortAccessLevels])));
  Result := (AL * AccessAdministration.CellAccess.DevelRead) <> [];
end;

procedure TFileLoadForm.FormCreate(Sender: TObject);
begin
  BtnPathSwitch.Visible := (LocalCellMode = rrmDevelopment) and PermissiveAccess;
  SaveEdit.Text := '';
  SaveMode := False;
end;

procedure TFileLoadForm.DirGridDrawCell(Sender: TObject; ACol,
  ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  Grid : TStringGrid;
begin
//  TGridDrawState = set of (gdSelected, gdFocused, gdFixed);
  Grid := TStringGrid(Sender);
  if gdFixed in State then begin
    Grid.Canvas.Brush.Color := clBtnFace;
    Grid.Canvas.Pen.Color := clBlack;
  end else begin
    if gdSelected in State then begin
      if gdFocused in State then begin
        Grid.Canvas.Brush.Color := clYellow;
        Grid.Canvas.Pen.Color := clBlack;
      end else begin
        Grid.Canvas.Brush.Color := $f0f0d0;
        Grid.Canvas.Pen.Color := clBlack;
      end;
    end else begin
      Grid.Canvas.Brush.Color := $d0d0a0;
      Grid.Canvas.Pen.Color := clBlack;
    end;
  end;

  Grid.Canvas.Font := Grid.Font;
  Grid.Canvas.FillRect(Rect);
  Grid.Canvas.TextRect(Rect, Rect.Left, Rect.Top, Grid.Cells[ACol, ARow]);
end;

procedure TFileLoadForm.FileGridKeyPress(Sender: TObject; var Key: Char);
begin
  if Key = #$d then
    DoKeyPress(nil, Key);
end;

end.
