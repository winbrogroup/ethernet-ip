{$A+,B-,C+,D+,E-,F-,G+,H+,I+,J+,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}

unit OCTOperatorDisplay;

interface
uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, AbstractFormPlugin, IFormInterface, NamedPlugin,
  ComCtrls, INamedInterface, Grids, ACNC_TouchSoftkey, IOCT, PluginInterface,
  ImgList, OCTTypes, OCTListBox, CNCTypes, OpDisplayTypes,
  ACNC_ThreeLineButton, ACNC_TouchGlyphSoftkey, ACNC_Plusmemo,SyntaxHighProgDisplay,
  OCTData,ToolData,IniFiles,ReqToolList,RequiredToolData,ToolDB,ProductionHistory,Confirm,
  jpeg;

const
OPPMin = 150;
OPPMax = 470;
MaxFontSize = 20;
ToolPosCount = 6;
RemoveColour = clRed;
AddColour = clBlue;

resourcestring
  NotInPositionText = '-----';
type
  tmToolStatus = (tmsEmpty,tmsOK,tmsPresentNotRegistered,tmsRegisteredNotPresent);

  TToolDataState = (
    tdsNoTool,         // Position is empty and no tooling required
    tdsADDTool,        // Empty Location but tool required
    tdsOk,             // Tool in POsition is required for this oct
    tdsCanRemove,      // Tool in this position can be removed
    tdsMustRemove,      // Blocking a tool load Tool must be removed to make way for new tool
    tdsNeverRequired,  // Tool is not referenced in the current OCT
    tdsInvalid,        // use this to indicate tool data not updated
    tdsStatusOnly      // use this for tool info only with no add or remove color Black
  );

  TStateColour = record
    Colour    : TColor;
    FontColor : TColor;
    Flash     : Boolean;
    end;

  TInMachineToolData = record
    State     : TToolDataState;
    Name      : String;
    Serial    : String;
    BString   : String;
    Remaining : String;
    end;

  TOctRecord = record
    StartCount : Integer;
    Abandoned  : Boolean;
    end;

  pTOCTRecord = ^TOctRecord;



  tmTagStatus = record
     TagPresent : Boolean;
     StationNumber : Integer;
     end;

  TODisplayReportingTags = (
    ortOpenToolDoor,
    ortOpenMainDoor,
    ortReqToolStation1,
    ortReqToolStation2,
    ortReqToolStation3,
    ortReqToolStation4,
    ortReqToolStation5,
    ortReqToolStation6,
    ortReqChuckChange,
    ortReqToolChuckChange,
    ortToolDoorLocked,
    ortToolChangerPosition,
    ortStatus1,
    ortStatus2,
    ortMainDoorLocked,
    ortMainDoorOpenInhibit,
    ortToolDoorOpenInhibit,
    ortOCTComplete,
    ortNewProgTag,
    ortSubName,
    ortLoadedOPPName,
    ortTCFitted,
    ortFaultAcknowledgeTag,
    ortOCtRunning,
    ortOCTIndex,
    ortOCTReqReset,
    ortOCTMode,
    ortOCTLoadedName,
    ortProgramLine,
    ortOCTDisplayMode,
    ortFakeSystem,
    ortDoFakeCycleStart,
    ortLoadedOCTName,
    ortSigWarn,
    ortSigFail,
    ortShuttlePartName,
    ortShuttleSerialNumber,
    ortPartClampComplete,
    ortPartClampPermissive,
    ortPartClampedAA,
    ortTotalToolLifeInHead,
    ortBarrelToolLifeInHead,
    ortToolLoadRequested,
    ortMustRemoveTool,
    ortTooManyTools,
    ortInRecovery,
    ortInCycle,
    ortToolClampComplete,
    ortToolClampPermissive,
    ortToolClampedAA
    );

  TTagDefinitions = record
    Ini : string;
    Tag : string;
    Owned : Boolean;
    TagType : string;
  end;

const
  TagDefinitions : array [TODisplayReportingTags] of TTagDefinitions = (
     ( Ini : 'ReqOpenToolDoor'; Tag : 'OCT_ReqOpenToolDoor'; Owned : True; TagType : 'TMethodTag' ),
     ( Ini : 'ReqOpenMainDoor'; Tag : 'OCT_ReqOpenMainDoor'; Owned : True; TagType : 'TMethodTag' ),
     ( Ini : 'ReqToolStation1'; Tag : 'OCT_ReqToolStation1'; Owned : True; TagType : 'TMethodTag' ),
     ( Ini : 'ReqToolStation2'; Tag : 'OCT_ReqToolStation2'; Owned : True; TagType : 'TMethodTag' ),
     ( Ini : 'ReqToolStation3'; Tag : 'OCT_ReqToolStation3'; Owned : True; TagType : 'TMethodTag' ),
     ( Ini : 'ReqToolStation4'; Tag : 'OCT_ReqToolStation4'; Owned : True; TagType : 'TMethodTag' ),
     ( Ini : 'ReqToolStation5'; Tag : 'OCT_ReqToolStation5'; Owned : True; TagType : 'TMethodTag' ),
     ( Ini : 'ReqToolStation6'; Tag : 'OCT_ReqToolStation6'; Owned : True; TagType : 'TMethodTag' ),
     ( Ini : 'ReqChuckChange'; Tag : 'OCT_ReqChuckChange'; Owned : True; TagType : 'TMethodTag' ),
     ( Ini : 'ReqToolChange'; Tag : 'OCT_ReqToolChuckChange'; Owned : True; TagType : 'TMethodTag' ),
     ( Ini : 'ToolDoor'; Tag : 'ToolDoorLocked'; Owned : False; TagType : 'TMethodTag' ),
     ( Ini : 'ToolChangerPosition'; Tag : 'TC_Index'; Owned : False; TagType : 'TIntegerTag' ),
     ( Ini : 'Status1'; Tag : 'OCTStatus1'; Owned : False; TagType : 'TStringTag' ),
     ( Ini : 'Status2'; Tag : 'OCTStatus2'; Owned : False; TagType : 'TStringTag' ),
     ( Ini : 'MainDoorIsLocked'; Tag : 'DoorClosed'; Owned : False; TagType : 'TIntegerTag' ),
     ( Ini : 'MainDoorOpenInhibit'; Tag : 'MainDoorOpenInhibit'; Owned : False; TagType : 'TIntegerTag' ),
     ( Ini : 'ToolDoorOpenInhibit'; Tag : 'ToolDoorOpenInhibit'; Owned : False; TagType : 'TIntegerTag' ),
     ( Ini : 'OCTComplete'; Tag : 'OCTComplete'; Owned : False; TagType : 'TMethodTag' ),
     ( Ini : 'NewProgTag'; Tag : 'NewActiveFile'; Owned : False; TagType : 'TStringTag' ),
     ( Ini : 'SubNameTag'; Tag : 'NC1ProgramLine'; Owned : False; TagType : 'TStringTag' ),
     ( Ini : 'LoadedOPPName'; Tag : 'NewActiveFile'; Owned : False; TagType : 'TStringTag' ),
     ( Ini : 'TCFitted'; Tag : '_OptionToolChanger'; Owned : False; TagType : 'TIntegerTag' ),
     ( Ini : 'FaultAcknowledgeTag'; Tag : 'OCT_ReqFaultAck'; Owned : False; TagType : 'TMethodTag' ),
     ( Ini : 'OCtRunning'; Tag : 'OCtRunning'; Owned : False; TagType : 'TMethodTag' ),
     ( Ini : 'OCTIndex'; Tag : 'OCTIndex'; Owned : False; TagType : 'TIntegerTag' ),
     ( Ini : 'OCTReqReset'; Tag : 'OCTReqReset'; Owned : False; TagType : 'TIntegerTag' ),
     ( Ini : 'OCTMode'; Tag : 'OCTMode'; Owned : False; TagType : 'TMethodTag' ),
     ( Ini : 'OCTLoadedOK'; Tag : 'OCTLoadedOK'; Owned : False; TagType : 'TIntegerTag' ),
     ( Ini : 'ProgramLine'; Tag : 'NC1ProgramLine'; Owned : False; TagType : 'TIntegerTag' ),
     ( Ini : 'OCTDisplayMode'; Tag : 'OCTDisplayMode'; Owned : True; TagType : 'TIntegerTag' ),
     ( Ini : 'ACNC32Fake'; Tag : 'ACNC32Fake'; Owned : False; TagType : 'TIntegerTag' ),
     ( Ini : 'OCTCycleStart'; Tag: 'OCTCycleStart'; Owned : False; TagType : 'TIntegerTag' ),
     ( Ini : 'OCTLoadedName'; Tag: 'OCTLoadedName'; Owned : False; TagType : 'TStringTag' ),
     ( Ini : 'SigMQIWarn'; Tag: 'SigMQIWarn'; Owned : False; TagType : 'TIntegerTag' ),
     ( Ini : 'SigMQIFail'; Tag: 'SigMQIWarn'; Owned : False; TagType : 'TIntegerTag' ),
     ( Ini : 'PartID'; Tag: 'PartID'; Owned : False; TagType : 'TIntegerTag' ),
     ( Ini : 'PartSN'; Tag: 'PartSN'; Owned : False; TagType : 'TIntegerTag' ),
     ( Ini : 'PartClampComplete'; Tag: 'PartClampComplete'; Owned : False; TagType : 'TIntegerTag' ),
     ( Ini : 'PartClampPermissive'; Tag: 'PartClampPermissive'; Owned : False; TagType : 'TIntegerTag' ),
     ( Ini : 'PartClamped'; Tag: 'PartClamped'; Owned : False; TagType : 'TIntegerTag' ),
     ( Ini : 'OCT_TotalToolLifeInHead'; Tag: 'OCT_TotalToolLifeInHead'; Owned : True; TagType : 'TStringTag' ),
     ( Ini : 'OCT_BarrelToolLifeInHead'; Tag: 'OCT_BarrelToolLifeInHead'; Owned : True; TagType : 'TStringTag' ),
     ( Ini : 'OCT_ToolLoadRequested'; Tag: 'OCT_ToolLoadRequested'; Owned : True; TagType : 'TIntegerTag' ),
     ( Ini : 'OCT_ToolUnLoadMandatory'; Tag: 'OCT_ToolUnLoadMandatory'; Owned : True; TagType : 'TIntegerTag' ),
     ( Ini : 'PLC_TooManyTools'; Tag: 'PLC_TooManyTools'; Owned : False; TagType : 'TIntegerTag' ),
     ( Ini : 'OCT_InRecovery'; Tag: 'OCT_InRecovery'; Owned : True; TagType : 'TIntegerTag' ),
     ( Ini : 'NC1InCycle'; Tag: 'NC1InCycle'; Owned : False; TagType : 'TIntegerTag' ),
     ( Ini : 'ToolClampComplete'; Tag: 'ToolClampComplete'; Owned : False; TagType : 'TIntegerTag' ),
     ( Ini : 'ToolClampPermissive'; Tag: 'ToolClampPermissive'; Owned : False; TagType : 'TIntegerTag' ),
     ( Ini : 'ToolClamped'; Tag: 'ToolClamped'; Owned : False; TagType : 'TIntegerTag' )
  );

type
  TOctDisplayModes = (odmDefault,odmRecovery,odmAllOPP,odmNotOCT,odmPartProgram,odmToolreq,odmShowPartPopUp);
  TTLDisplayMode = (tldStatusOnly,tldVersusOCT);
  pTToolDataState = ^TToolDataState;


  TOCTFormDlg = class(TInstallForm)
    BottomPanel: TPanel;
    BaseOCTPanel: TPanel;
    DisplayOptionsPanel: TPanel;
    BtnDefaultDisplay: TTouchGlyphSoftkey;
    BtnShowToolTable: TTouchGlyphSoftkey;
    BtnShowOPPOnly: TTouchGlyphSoftkey;
    BtnClose: TTouchGlyphSoftkey;
    BtnSplitUp: TTouchGlyphSoftkey;
    BtnSplitDown: TTouchGlyphSoftkey;
    OctStatusPanel: TPanel;
    LeftStatusPanel: TPanel;
    LabelStatus1: TLabel;
    RightStatusPanel: TPanel;
    LabelStatus2: TLabel;
    OCTLowerPageControl: TPageControl;
    ToolManSheet: TTabSheet;
    ToolManagementPanel: TPanel;
    PDisplaySheet: TTabSheet;
    Panel17
    : TPanel;
    ProgNameDisplay: TLabel;
    ProgLineDisplay: TLabel;
    RecoveryPanel: TPanel;
    RecoveryBanner: TPanel;
    Panel4: TPanel;
    Panel6: TPanel;
    Panel8: TPanel;
    RestartPanel: TPanel;
    RestartCurrentButton: TTouchSoftkey;
    Panel10: TPanel;
    SkipPanel: TPanel;
    SkipButton: TTouchSoftkey;
    Panel9: TPanel;
    AbandonPartPanel: TPanel;
    AbandonPartButton: TTouchSoftkey;
    Panel11: TPanel;
    Panel2: TPanel;
    AbandonOCTPanel: TPanel;
    AbandonOCTButton: TTouchSoftkey;
    ReworkPanel: TPanel;
    ReworkButton: TTouchSoftkey;
    Panel7: TPanel;
    Panel12: TPanel;
    MessageLabel: TLabel;
    MessageLabel2: TLabel;
    AcceptKey: TTouchSoftkey;
    Panel13: TPanel;
    MiscPanel: TPanel;
    ToolStatMainPanel: TPanel;
    TStatHeadPanel: TPanel;
    Panel16: TPanel;
    BtnHead: TThreeLineButton;
    InvalidateBlank: TPanel;
    BtnInvalidate: TTouchSoftkey;
    TStatTS1Panel: TPanel;
    Panel1: TPanel;
    BtnTS1: TThreeLineButton;
    STN1Location: TPanel;
    TStatTS2Panel: TPanel;
    Panel5: TPanel;
    btnTS2: TThreeLineButton;
    STN2Location: TPanel;
    TStatTS3Panel: TPanel;
    Panel14: TPanel;
    btnTS3: TThreeLineButton;
    STN3Location: TPanel;
    TStatTS4Panel: TPanel;
    Panel15: TPanel;
    btnTS4: TThreeLineButton;
    STN4Location: TPanel;
    Panel22: TPanel;
    RemTimeDisplay: TLabel;
    ToolDisplayToggle: TTouchSoftkey;
    Panel3: TPanel;
    Label2: TLabel;
    Label3: TLabel;
    OctDisplayModePanel: TPanel;
    OperatorButtonPanel: TPanel;
    Panel23: TPanel;
    BtnShowPartData: TTouchGlyphSoftkey;
    BtnFaultAck: TTouchGlyphSoftkey;
    WinLogoPanel: TPanel;
    Image1: TImage;
    TStatTS5Panel: TPanel;
    Panel19: TPanel;
    btnTS5: TThreeLineButton;
    STN5Location: TPanel;
    TStatTS6Panel: TPanel;
    Panel24: TPanel;
    btnTS6: TThreeLineButton;
    STN6Location: TPanel;
    OCTImages: TImageList;
    Panel18: TPanel;
    Panel20: TPanel;
    Panel21: TPanel;
    BtnPartChuck: TTouchSoftkey;
    BtnToolChuck: TTouchSoftkey;
    Panel27: TPanel;
    Panel28: TPanel;
    BtnOpenMainDoor: TTouchSoftkey;
    Panel29: TPanel;
    BtnOpenToolDoor: TTouchSoftkey;
    PartChuckStatusPanel: TPanel;
    MainDoorStatusPanel: TPanel;
    ToolChuckStatusPanel: TPanel;
    ToolDoorStatusPanel: TPanel;
    BULabel: TLabel;
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure RestartCurrentButtonMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure SkipButtonMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure AbandonOCTButtonMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure AcceptKeyMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure AbandonPartButtonMouseUp(Sender: TObject;
      Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure BtnCloseClick(Sender: TObject);
    procedure BtnGeneralMouseDown(Sender: TObject;
      Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure BtnGeneralMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure BtnCloseMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure BtnShowToolTableMouseUp(Sender: TObject;
      Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure BtnDefaultDisplayMouseUp(Sender: TObject;
      Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure BtnShowOPPOnlyMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure BtnSplitUpMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure BtnSplitDownMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Panel12Resize(Sender: TObject);
    procedure BaseOCTPanelResize(Sender: TObject);
    procedure BtnShowPartDataMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
  private
    BUCount : Integer;
    LazyUpdateFlag : Boolean;
    FixedStartOPP : String;
    FixedEndOPP : String;

    PHist : TProductionHistory;
    LastStatus   :TOPPStatus;
    CallCount               : Integer;
    CallCount2              : Integer;
    UpdateSweepCount        : Integer;
    CurrentUpdateCount      : Integer;
    UsesFixedEndOPP         : Boolean;
    UsesFixedStartOPP       : Boolean;
    ResetOCTOnAbandon       : Boolean;
    ShowZeroLifeButton      : Boolean;
    ShowPartButton          : Boolean;
    ShowResetButton         : Boolean;
    PartButtonTag           : TTagref;
    PartButtonTagName       : String;
//    SelectedRecoryOption    : TRecoveryoptions;
    SelfStartTag            : TTagref;
    MachineTools            : array[0..ToolPosCount] of TInMachineToolData;
    ButtonColourArray       : array[Low(TToolDataState)..high(TToolDataState)] of TStateColour;
    OCTData                 : TOPPList;
    ToolData                : TToolList;
    MasterToolList          : TToolDB;

    RequiredToolData        : TRequiredToolList;
    OCTDisplay              : TOCTListBox;
    RToolDisplay             : TReqToolList;
    InRecovery              : Boolean;
    CurrentRecoveryOPtion   : TRecoveryOptions;

    PrDisplay               : TSyntaxHighProgDisplay;
    DisplayMode             : TOctDisplayModes;
    LastSubIndex            : Integer;


    TagNames : array[TODisplayReportingTags] of string;
    Tags : array [TODisplayReportingTags] of TTagRef;
    FTCStnStatus: array [0..ToolPosCount] of tmToolStatus;
    SweepCount : Integer;
    ToolPosLang : array [0..3] of string;
    InHeadText  : String;
    MissmatchText  : String;
    MissmatchError1: String;
    MissmatchError2: String;
    LastMismatchState : Integer;


    // Translation Strings
    EmptyText,
    DoorText,
    BackText,
    LeftText,
    RightText,
    HeadText,
    LockedText,
    OpenText,
    FaultyText,
    ClosedText,
    ClampedText,
    StatusOnlyText,
    vsOCTText,
    EMPTYButtonText,
    NotRegisteredButtonText,
    NotPresentButtonText,
    PleaseSelText,
    PressAcceptAndClearText,
    ThenPressCycleText,
    PAToABANDONOPPText,
    PAToABANDONOCTText,
    PAToABANDONPartText,
    ThenPressCycleStartToAbandonText,
    PAToREWORKText,
    ReworkPermittedText,
    AYSInvalidateText,
    ShowToolTableText,
    ShowPartProgramText,
    AddText,
    RemoveText,
    InvalidProgramText : string;
    Selected_DofD: string;
    Total_Duration_S: string;
    Index_DOfD: string;

    procedure UpdateToolingDisplay;
    procedure LazyUpdate(Sender : TObject);
    procedure OCTCellUpdate(Sender : TObject; Row : Integer; Field : WideString);
//    procedure UpdateOCTList(Sender : TOBject);
    procedure UpdateOCTStatus;
    procedure DisplayRecoveyOptions(Sender: TOBject);
    procedure SetButtonPanel(RecOption: TRecoveryOptions);
    procedure ClearRecoveryOptions;
    procedure OCTRunningChanged(Running : Boolean);
    procedure DoResetDisplay(Sender: TOBject);
    procedure DoOctDisplayClicked(Sender: TOBject);
    procedure DoSelectAllOpps(Sender: TObject);
    procedure DoInvertSelection(Sender: TObject);
    procedure DoChangeSize(Sender: TObject);
    procedure SetToolDoorStatus;
    procedure SetMainDoorStatus;
    procedure UpdateToolDisplayFor(ChangerPos : Integer);
    procedure UpdateButtonEnable;
    procedure DecodeButtonColour(ColourCode: TToolDataState; var Colour,
      TextColour: TColor; var Flash: Boolean);
    procedure UpdatePartProgDisplay(ForceProgLoad : Boolean);
    procedure SetDisplayMode(DMode : TOctDisplayModes);
    procedure SetAcceptKeyState(Enabled : Boolean);
    procedure SetSoftButtonEnables(InRec: Boolean;OCTMode : Boolean);
    procedure SetStatus(const Text : string; Index : Integer);

    procedure ReadConfiguration;
    procedure ResetOCT;
    function  OCTisRunning:Boolean;
    function  isOCTMode: Boolean;
    function  isStatusOnlyDisplay: Boolean;
    function  OPPIndex: Integer;
    function  OCTFilename: String;
    //function UpdateToolStatus : Boolean;
    function BuildToolInMachineStatus(ToolLocation : Integer):Boolean;
    procedure HandleOCTStatusChange(Sender : TObject);
    function SecondsToMinutesString(Seconds: Integer): String;
    function SecondsToHMS(Seconds: Integer): string;
    procedure MakePHistEndRecord(OPPData :TOPP;Abandoned : Boolean);
    procedure ZeroToolLifeForToolInLocation(Location: Integer);
    procedure SetPartClampStatus;
    procedure SetToolClampStatus;
    procedure ResizeChuckandDoorButtons;

  public
    destructor Destroy; override;
    procedure Install; override;
    procedure FinalInstall; override;
    procedure Execute; override;
    procedure ShutDown; override;
    procedure CloseForm; override;
    procedure DisplaySweep; override;
    procedure TagChanged(Tag: TTagRef);
    procedure Resize; override;
    procedure UpdateStnButtons;
    procedure SetOCTDisplayMode(Mode :TTLDisplayMode);

  end;

var

MyOCTPopup : TOCTFormDlg;

procedure TagChangeHandler(Tag : TTagRef);stdcall;

implementation

uses TypInfo;

{$R *.DFM}

procedure TOCTFormDlg.CloseForm;
begin
  if Visible then
    ModalResult := mrOK;
end;

procedure TOCTFormDlg.DisplaySweep;
//var
//MismatchChanged : Boolean;
begin

 //MisMatchChanged := UpdateToolStatus;
 if LazyUpdateFlag then
  begin
  Dec(CurrentUpdateCount);
  if CurrentUpdateCount < 0 then
    begin
    CurrentUpdateCount := UpdateSweepCount;
    LazyUpdateFlag := False;
    UpdateToolingDisplay;
    end;
  end;
end;

destructor TOCTFormDlg.Destroy;
begin
  OCTData.Close;
  OCTData := nil;
  ToolData.Close;
  ToolData := nil;
  RequiredToolData.Close;
  RequiredToolData := nil;
  MasterToolList.Close;
  MasterToolList := nil;

  inherited;
end;

procedure TOCTFormDlg.Execute;
begin
  if not Visible then
    begin
    if Named.GetASBoolean(Tags[ortTCFitted]) then
      begin
      ToolStatMainPanel.Height := 475;
      TStatTS1Panel.Visible := True;
      TStatTS2Panel.Visible := True;
      TStatTS3Panel.Visible := True;
      TStatTS4Panel.Visible := True;
      OperatorButtonPanel.Align := alClient;
      WinlogoPanel.Visible := False;
      end
    else
      begin
      ToolStatMainPanel.Height := 115;
      TStatTS1Panel.Visible := False;
      TStatTS2Panel.Visible := False;
      TStatTS3Panel.Visible := False;
      TStatTS4Panel.Visible := False;
      OperatorButtonPanel.Align := alBottom;
      OperatorButtonPanel.Height := 322;
      WinlogoPanel.Visible := True;
      WinlogoPanel.Align := alClient;
      end ;

    BtnShowToolTable.Visible := True;
    if isOCTMode then
       begin
       SetOCTDisplayMode(tldVersusOCT);
       SetDisplayMode(odmDefault);
       end
    else
       begin
       SetOCTDisplayMode(tldStatusOnly);
       SetDisplayMode(odmNotOCT);
       end;
    Height := 700;
    Width := 1024;
    ShowModal;
    UpdateButtonEnable;
    end;
end;


procedure TagChangeHandler(Tag : TTagRef);stdcall
begin
  MyOCTPopup.TagChanged(Tag);

end;

procedure TOCTFormDlg.ShutDown;
var I : TODisplayReportingTags;
    Ini : TIniFile;
begin
  Ini := TIniFile.Create(DllProfilePath + InstallName + '.cfg');
  try
    for I := Low(TODisplayReportingTags) to High(TODisplayReportingTags) do
      Ini.WriteString('Tags', TagDefinitions[I].Ini, TagNames[I]);
  finally
    Ini.Free;
  end;
  inherited;

end;


procedure TOCTFormDlg.TagChanged(Tag: TTagRef);
var ctmp : array [0..128] of Char;
var TagFound : Boolean;
begin
  TagFound := True;
  if Tag = Tags[ortOCTRunning] then begin
    // here when all complete or when recovery required
    OCTRunningChanged(Named.GetAsBoolean(Tag));
  end else

  if Tag = Tags[ortOCTIndex] then begin
     if OPPIndex = 0 then
        begin
        SetStatus('',1);
        SetStatus('',2);
        end;
     OCTDisplay.RunningRow := OPPIndex + 1;
     OCTDisplay.RefreshDisplay;
     OCTDisplay.DisplayResetButtonEnabled := True;
     OCTDisplay.SelectAllButtonEnabled := True;
     RToolDisplay.UpdateDisplay;
     UpdateOCTStatus;

  end else

  if Tag = Tags[ortOCTComplete] then begin
     // Here at end of each OPP not just OCT Complete !!!
  end else

  if Tag = Tags[ortNewProgTag] then begin
     UpdatePartProgDisplay(True);
     end else

  if Tag = Tags[ortStatus1] then begin
    Named.GetAsString(Tag, ctmp, Length(ctmp) - 1);
    SetStatus(ctmp, 1);
    end else

  if Tag = Tags[ortStatus2] then begin
     Named.GetAsString(Tag, ctmp, Length(ctmp) - 1);
     SetStatus(ctmp, 2);
     end else

  if Tag = Tags[ortToolDoorLocked] then begin
     SetToolDoorStatus;
     end else

  if Tag = Tags[ortMainDoorLocked] then begin
     SetMainDoorStatus
     end else

  if Tag = Tags[ortMainDoorOPenInhibit] then begin
     UpdateButtonEnable
     end else

  if Tag = Tags[ortToolDoorOPenInhibit] then begin
     UpdateButtonEnable;
     end else

  if Tag = Tags[ortToolChangerPosition] then  begin
     UpdateToolDisplayFor(Named.GetAsInteger(Tags[ortToolChangerPosition]));
     end else

  if Tag = Tags[ortToolChangerPosition] then  begin
     UpdateToolDisplayFor(Named.GetAsInteger(Tags[ortToolChangerPosition]));
     end else

  if Tag = Tags[ortProgramLine] then begin
     UpdatePartProgDisplay(False);
     end else

  if Tag = Tags[ortOCTMode]  then begin
    if Named.GetAsBoolean(Tag) then
        begin
        SetDisplayMode(odmDefault);
        end
      else
        begin
        SetDisplayMode(odmNotOCT);
        end;
    UpdateOCTStatus;//(self);
    end else

  if (Tag = Tags[ortPartClampComplete]) OR
     (Tag = Tags[ortPartClampedAA]) OR
     (Tag = Tags[ortPartClampPermissive]) then begin
     SetPartClampStatus;
     end;
  if (Tag = Tags[ortToolClampComplete]) OR
     (Tag = Tags[ortToolClampedAA]) OR
     (Tag = Tags[ortToolClampPermissive]) then begin
     SetToolClampStatus;
     end else

  TagFound := False;

  If TagFound then
    begin
    UpdateButtonEnable;
    end;
end;

procedure TOCTFormDlg.SetPartClampStatus;
var
Inhibit : Boolean;
Open    : Boolean;
Clamped : Boolean;

begin
  Inhibit := Named.GetAsBoolean(Tags[ortPartClampPermissive]);
  Open := not Named.GetAsBoolean(Tags[ortPartClampedAA]);
  Clamped := Named.GetAsBoolean(Tags[ortPartClampComplete]);
  if Open then
    begin
    PartChuckStatusPanel.Color := clred;
    PartChuckStatusPanel.Caption := OpenText;
    PartChuckStatusPanel.Font.Color := clWhite;
    end
  else if Clamped then
    begin
    PartChuckStatusPanel.Color := clLime;
    PartChuckStatusPanel.Caption := ClampedText;
    PartChuckStatusPanel.Font.Color := clBlack;
    end
  else
    begin
    PartChuckStatusPanel.Color := clYellow;
    PartChuckStatusPanel.Caption := ClosedText;
    PartChuckStatusPanel.Font.Color := clBlack;
    end;
  UpdateButtonEnable;
end;

procedure TOCTFormDlg.SetToolClampStatus;
var
Inhibit : Boolean;
Open    : Boolean;
Clamped : Boolean;
begin
  Inhibit := not Named.GetAsBoolean(Tags[ortToolClampPermissive]);
  Open := not Named.GetAsBoolean(Tags[ortToolClampedAA]);
  Clamped := Named.GetAsBoolean(Tags[ortToolClampComplete]);
  if Open then
    begin
    ToolChuckStatusPanel.Color := clred;
    ToolChuckStatusPanel.Caption := OpenText;
    ToolChuckStatusPanel.Font.Color := clWhite;
    end
  else if Clamped then
    begin
    ToolChuckStatusPanel.Color := clLime;
    ToolChuckStatusPanel.Caption := ClampedText;
    ToolChuckStatusPanel.Font.Color := clBlack;
    end
  else
    begin
    ToolChuckStatusPanel.Color := clYellow;
    ToolChuckStatusPanel.Caption := ClosedText;
    ToolChuckStatusPanel.Font.Color := clBlack;
    end;
  UpdateButtonEnable;
end;

procedure TOCTFormDlg.SetToolDoorStatus;
begin
  if Assigned(Tags[ortToolDoorLocked]) then begin
    if Named.GetAsBoolean(Tags[ortToolDoorLocked]) then begin
    ToolDoorStatusPanel.Color := clLime;
    ToolDoorStatusPanel.Caption := LockedText;
    ToolDoorStatusPanel.Font.Color := clBlack;

    end else begin
      ToolDoorStatusPanel.Color := clRed;
      ToolDoorStatusPanel.Caption := OpenText;
      ToolDoorStatusPanel.Font.Color := clWhite;
    end;
  end;
end;

procedure TOCTFormDlg.SetMainDoorStatus;
begin
  if Assigned(Tags[ortMainDoorLocked]) then begin
    if Named.GetAsBoolean(Tags[ortMainDoorLocked]) then begin
      MainDoorStatusPanel.Color := clLime;
      MainDoorStatusPanel.Caption := LockedText;
      MainDoorStatusPanel.Font.Color := clBlack;
    end else begin
      MainDoorStatusPanel.Color := clRed;
      MainDoorStatusPanel.Caption := OpenText;
      MainDoorStatusPanel.Font.Color := clWhite;
    end;
  end
else
    begin
    MainDoorStatusPanel.Color := clYellow;
    MainDoorStatusPanel.Caption := FaultyText;
    MainDoorStatusPanel.Font.Color := clWhite;
    end
end;

procedure TOCTFormDlg.FinalInstall;
var
Translator : TTranslator;
I : TODisplayReportingTags;
begin
  inherited;
  for I := Low(TODisplayReportingTags) to High(TODisplayReportingTags) do begin
    if not TagDefinitions[I].Owned then
      Tags[I] := Named.AquireTag(PChar(TagNames[I]), PChar(TagDefinitions[I].TagType), TagChangeHandler);
  end;

    //Named.SetAsInteger(Tags[ortToolMismatch],0);
    SelfStartTag := Named.AquireTag(PChar(InstallName),'TMethodTag',nil);

  BtnShowToolTable.Visible := True;

  OCtDisplay.FinalInstall;

  Translator := TTranslator.Create;
  try
  with Translator do
       begin
       EmptyText                   := GetString('$EMPTY');
       DoorText                    := GetString('$AT_DOOR');
       BackText                    := GetString('$BACK');
       LeftText                    := GetString('$LEFT');
       RightText                   := GetString('$RIGHT');
       HeadText                    := GetString('$HEAD');
       LockedText                  := GetString('$LOCKED');
       OpenText                    := GetString('$OPEN');
       ClosedText                  := GetString('$CLOSED');
       ClampedText                 := GetString('$CLAMPED');
       FaultyText                  := GetString('$FAULTY');
       StatusOnlyText              := GetString('$STATUS_ONLY');
       vsOCTText                   := GetString('$VS_OCT');
       BtnShowOPPOnly.Legend       := GetString('$Show_OPP~Table_Only');
       ShowToolTableText           := GetString('$Show~Tool_Table');
       ShowPartProgramText         := GetString('$Show~Part_Program');
       InvalidProgramText          := GetString('$INVALID_PROGRAM');
       EMPTYButtonText             := GetString('$___EMPTY___');
       NotRegisteredButtonText     := GetString('$UNKNOWN_TOOL');
       NotPresentButtonText        := GetString('$TOOL_MISSING');
       PleaseSelText               := GetString('$PLEASE_SELECT_A_RECOVERY_OPTION_FROM_ABOVE');
       PressAcceptAndClearText     := GetString('$PRESS_ACCEPT_AND_CLEAR_ANY_ERRORS_TO_RESATRT_CURRENT_OPP');
       ThenPressCycleText          := GetString('$THEN_PRESS_CYCLE_START_TO_CONTINUE');
       PAToABANDONOPPText          := GetString('$PRESS_ACCEPT_TO_ABANDON_CURRENT_OPP');
       PAToABANDONOCTText          := GetString('$PRESS_ACCEPT_TO_ABANDON_OCT');
       PAToABANDONPartText         := GetString('$PRESS_ACCEPT_TO_ABANDON_PART');
       ThenPressCycleStartToAbandonText := GetString('$THEN_PRESS_CYCLE_START_TO_ABANDON');
       PAToREWORKText              := GetString('$PRESS_ACCEPT_TO_REWORK_OPP');
       ReworkPermittedText         := GetString('$REWORK_IS_ONLY_PERMITTED_IN_PRODUCTION_MODE');
       AYSInvalidateText           := GetString('$ARE_YOU_SURE_YOU_WANT_TO_INVALIDATE_THE_TOOL_IN_THE_HEAD');
       BtnDefaultDisplay.Legend    := GetString('$___DEFAULT___');
       BtnShowToolTable.Legend     := GetString('$Show~Tool_Table');
       BtnSplitUp.Legend           := GetString('$Move_Split~UP');
       BtnSplitDown.Legend         := GetString('$Move_Split~Down');
       BtnClose.Legend             := GetString('$___Close___');
       RecoveryBanner.Caption      := GetString('$OCT_RECOVERY_OPTIONS');
       RestartPanel.Caption        := GetString('$RESTART_CURRENT_OPP');
       SkipPanel.Caption           := GetString('$SKIP_TO_NEXT_OPP');
       AbandonOCTPanel.Caption     := GetString('$ABANDON_ENTIRE_OCT');
       AbandonPartPanel.Caption    := GetString('$MARK_PART_AS_NON_CONFORM');
       ReworkPanel.Caption         := GetString('$REWORK_CURRENT_OPP');
       AcceptKey.Legend            := GetString('$ACCEPT');
       BtnOpenToolDoor.Legend      := GetString('$OPEN_TOOL_DOOR');
       BtnOpenMainDoor.Legend      := GetString('$OPEN_MAIN_DOOR');
       BtnPartChuck.Legend         := GetString('$PART_CHUCK');
       BtnToolChuck.Legend         := GetString('$TOOL_CHUCK');
       BtnInvalidate.Legend        := GetString('$ZERO_LIFE');
       BtnFaultAck.Legend          := GetString('$FAULT_ACKNOWLEDGE');
       BtnShowPartData.Legend      := GetString('$SHOW_PART_DATA');

       Panel16.Caption             := GetString('$HEAD');
       ToolDisplayToggle.Legend    := GetString('$DISPLAY_MODE');
       AddText:= GetString('$ADD');
       RemoveText:= GetString('$REMOVE');

       Selected_DofD:= GetString('$SELECTED_D_OF_D');
       Total_Duration_S:= GetString('$TOTAL_DURATION_S');
       Index_DOfD:= Translator.GetString('$INDEX_D_OF_D');
       end;

  finally
  Translator.Free;
  end;

  SetToolDoorStatus;
  SetMainDoorStatus;
  SetPartClampStatus;
  SetToolClampStatus;
  if PHist.Configured then PHist.FinalInstall;

  if ShowPartButton then
    begin
    if PartButtonTagName <> '' then
      begin
      PartButtonTag := Named.AquireTag(PChar(PartButtonTagName),'TIntegerTag',nil)
      end;
    end;
  OctDisplay.ShowResetButton := ShowResetButton;
  Named.SetAsBoolean(Tags[ortInrecovery],False);
end;

procedure TOCTFormDlg.Install;
var
Translator : TTranslator;
I : TODisplayReportingTags;
//octFile : TiniFile;
begin

  PHist := TProductionHistory.Create;
  if PHist.Configured then PHist.Install;
  LastStatus := osUnknown;
  SweepCount:= 25;
  CallCount := 0;
  CallCount2 := 0;
  FTCStnStatus[1]:= tmsEmpty;
  FTCStnStatus[2]:= tmsEmpty;
  FTCStnStatus[3]:= tmsEmpty;
  FTCStnStatus[4]:= tmsEmpty;
  FTCStnStatus[5]:= tmsEmpty;
  FTCStnStatus[6]:= tmsEmpty;
  LastMismatchState := 0;
  Translator := TTranslator.Create;
  try

  with Translator do
       begin
       ToolPosLang[0] := GetString('$AT_DOOR');
       ToolPosLang[1] := GetString('$LEFT');
       ToolPosLang[2] := GetString('$BACK');
       ToolPosLang[3] := GetString('$RIGHT');
       InHeadText     := GetString('$IN_HEAD');
       MissmatchText  := GetString('$MISSMATCH');
       MissmatchError1:= GetString('$UNEXPECTED_TOOL_AT_TOOL_STATION_%D');
       MissmatchError2:= GetString('$TOOL_EXPECTED_AT_TOOL_STATION_%D');
       end

  finally
  Translator.Free;
  end;
  for I := Low(TODisplayReportingTags) to High(TODisplayReportingTags) do begin
    if TagDefinitions[I].Owned then
      Tags[I] := Named.AddTag(PChar(TagNames[I]), PChar(TagDefinitions[I].TagType));
  end;

  MyOCTPopup := Self;
  Font.Name := NamedPlugin.FontName;
  PrDisplay := TSyntaxHighProgDisplay.Create(Self);
  with PrDisplay do
       begin
       Parent :=  PDisplaySheet;
       readConfigFile(DLLProfilePath+'OPPEditor.cfg');
       Align := alClient;
       LastSubIndex := -10000;
       CurrentLineInCentre := True;
       end;

end;

procedure TOCTFormDlg.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Named.SetAsBoolean(Tags[ortOpenMainDoor], False);
  Named.SetAsBoolean(Tags[ortOpenToolDoor], False);
  Named.SetAsBoolean(Tags[ortReqToolStation1], False);
  Named.SetAsBoolean(Tags[ortReqToolStation2], False);
  Named.SetAsBoolean(Tags[ortReqToolStation3], False);
  Named.SetAsBoolean(Tags[ortReqToolStation4], False);
  Named.SetAsBoolean(Tags[ortReqToolStation5], False);
  Named.SetAsBoolean(Tags[ortReqToolStation6], False);

end;


procedure TOCTFormDlg.FormCreate(Sender: TObject);
var
ToolLocation : Integer;
begin
  BUCount := 0;
  CurrentRecoveryOption := roNone;
  OCTData := TOPPList.create;
  OCTdata.OnChange := HandleOCTStatusChange;
  OCTData.OnCellChange := OCTCellUpdate;
  ToolData := TToolList.Create;
  ToolData.OnChange := LazyUpdate;
  MasterToolList := TToolDB.Create;
  RequiredToolData := TRequiredToolList.Create;
  RequiredToolData.OnChange  := LazyUpdate;
  OCTDisplay := TOCTListBox.Create(Self);
  for ToolLocation := 0 to ToolPosCount do
      begin
      MachineTools[ToolLocation].State :=     tdsNoTool;
      MachineTools[ToolLocation].Name :=      '';
      MachineTools[ToolLocation].Serial :=    '';
      MachineTools[ToolLocation].Remaining := '';
      end;
  ReadConfiguration;

  with OCTDisplay do
    begin
    OCTList := Self.OCTData;
    ImageList := OCtImages;
    Parent := BaseOCTPanel;
    Align := alClient;
    RequestDisplayReset := DoResetDisplay;
    OnRowClicked := DoOctDisplayClicked;
    RequestSelectAllOPPs := DoSelectAllOpps;
    RequestInvertSelection := DoInvertSelection;
    RequestReduce := DoChangeSize;
    end;

  RToolDisplay := TReqToolList.Create(Self);
  with RToolDisplay do
    begin
    RToolList := Self.RequiredToolData;
    MasterTD := Self.MasterToolList;
    ToolList := SElf.ToolData;
    OCTList := Self.OCTData;
    Parent := ToolManSheet;
    Align := alClient;
    end;

  Top := 70;
  left := 1;

  BtnTS1.MaxFontSize := MaxFontSize;
  BtnTS2.MaxFontSize := MaxFontSize;
  BtnTS3.MaxFontSize := MaxFontSize;
  BtnTS4.MaxFontSize := MaxFontSize;
  BtnTS5.MaxFontSize := MaxFontSize;
  BtnTS6.MaxFontSize := MaxFontSize;
  BtnHead.MaxFontSize:= MaxFontSize;
  BtnDefaultDisplay.MaxFontSize := MaxFontSize;
  BtnShowToolTable.MaxFontSize := MaxFontSize;
  BtnShowOPPOnly.MaxFontSize := MaxFontSize;
  BtnShowPartData.MaxFontSize := MaxFontSize;
  BtnSplitUp.MaxFontSize := MaxFontSize;
  BtnSplitDown.MaxFontSize := MaxFontSize;
  BtnClose.MaxFontSize := MaxFontSize;
  BtnFaultAck.MaxFontSize := MaxFontSize;
  BtnToolChuck.MaxFontSize := MaxFontSize;

end;

{procedure TOCTFormDlg.UpdateOCTList(Sender: TOBject);
var
ActSubIndex : Integer;

begin
// Here when a new OCT Loaded
  if OCTData.Count >0 then begin
    OCTDisplay.UpDateOCTDisplayTrigger := True;
  end;
  OCTDisplay.ResetGrid;

ActSubIndex :=NC.ActiveSubRoutine;
if ActSubIndex >0 then
   begin
   PrDisplay.LoadProgramText(NC.SubRoutine[ActSubIndex]);
   end;

end; }

procedure TOCTFormDlg.UpdateOCTStatus;//(Sender: TOBject);
var
  RemTime : Integer;
begin
  OCTDisplay.UpdateOCTDisplayTrigger := True;
  //   OCTDisplay.StatusText[0] := Format('Loaded OPP %d of %d',[OPPIndex + 1,
  //                                                    OCTData.Count]);

  try
    OCTDisplay.StatusText[0] := Format(Index_DofD,
                             [OCTData.OppsEnabled - OCTData.OPPsRemaining,
                              OCTData.OppsEnabled]);
  except
    OCTDisplay.StatusText[0] := 'Error';
  end;

  try
    OCTDisplay.StatusText[1] := Format(Selected_DofD,
                                     [OCTData.OPPsEnabled,
                                      OCTData.Count]);
  except
    OCTDisplay.StatusText[1]:= 'Error';
  end;

  try
    OCTDisplay.StatusText[2] := Format(Total_Duration_S,
                                     [SecondsToHMS(Round(OCTData.SummedDuration))]);
  except
    OCTDisplay.StatusText[2]:= 'Error';
  end;

  if isOCTMode then
    begin
    RemTime := Round(OCTData.EstimatedCycleTime);
    if RemTime < 0 then RemTime := 0;
       RemTimeDisplay.Caption := SecondsToMinutesString(RemTime);
    end
  else
       RemTimeDisplay.Caption := '';


end;

procedure TOCTFormDlg.ClearRecoveryOptions;
begin
  InRecovery := False;
  Named.SetAsBoolean(Tags[ortInrecovery],False);
  OCTDisplay.RecoveryIndex := -1;
  CurrentRecoveryOPtion := roNone;
  SetDisplayMode(odmDefault);
  Resize;
end;

procedure TOCTFormDlg.DisplayRecoveyOptions(Sender: TOBject);
begin
  InRecovery := True;
  Named.SetAsBoolean(Tags[ortInrecovery],True);
  SetButtonPanel(roNone);
  if (not Self.Visible) and (CNCModeAuto in NC.CNCMode) then begin
     Named.SetAsBoolean(SelfStartTag,True);
     Named.SetAsBoolean(SelfStartTag,False);
  end;
  Resize;
end;

procedure TOCTFormDlg.Resize;
begin
  inherited;
  SetDisplayMode(DisplayMode);
end;

procedure TOCTFormDlg.SetButtonPanel(RecOption : TRecoveryOptions);
begin
  RestartCurrentButton.ButtonColor := clWhite;
  SkipButton.ButtonColor := clWhite;
  AbandonOCTButton.ButtonColor := clWhite;
  AbandonPartButton.ButtonColor := clWhite;
  ReworkButton.ButtonColor := clWhite;
  SetAcceptKeyState(False);

  case RecOPtion of
    roNone: begin
      MessageLabel.Caption := PleaseSelText;
      MessageLabel2.Caption := '';
      SetAcceptKeyState(False);
    end;

    roRestartCurrent: begin
      RestartCurrentButton.SetFocus;
      RestartCurrentButton.IsEnabled := True;
      RestartCurrentButton.ButtonColor := clYellow;
      SetAcceptKeyState(True);
      MessageLabel.Caption := PressAcceptAndClearText;
      MessageLabel2.Caption := ThenPressCycleText;
    end;

    roAbandonCurrent: begin
      SkipButton.ButtonColor := clYellow;
      MessageLabel.Caption := PAToABANDONOPPText;
      MessageLabel2.Caption := ThenPressCycleText;
      SetAcceptKeyState(True);
    end;

    roAbandonOCT: begin
      AbandonOCTButton.ButtonColor := clYellow;
      if UsesFixedEndOPP then
         MessageLabel.Caption := Format('%s  %s',[PAToABANDONPartText,ThenPressCycleStartToAbandonText])
      else
         MessageLabel.Caption := PAToABANDONPartText;
      SetAcceptKeyState(True);
    end;

    roAbandonPartInCell: begin
      AbandonPartButton.ButtonColor := clYellow;
      if UsesFixedEndOPP then
         MessageLabel.Caption := Format('%s  %s',[PAToABANDONPartText,ThenPressCycleStartToAbandonText])
      else
         MessageLabel.Caption := PAToABANDONPartText;

      SetAcceptKeyState(True);
    end;

    roReworkOPP : begin
      ReworkButton.ButtonColor := clYellow;
      MessageLabel.Caption := PAToREWORKText;
      SetAcceptKeyState(True);
    end;

    roNotRework : begin
      MessageLabel.Caption := ReworkPermittedText;
      SetAcceptKeyState(False);
    end;
  end;
end;


procedure TOCTFormDlg.ResetOCT;
begin
  Named.SetAsInteger(Tags[ortOCTReqReset],1);
  Named.SetAsInteger(Tags[ortOCTReqReset],0);
end;

procedure TOCTFormDlg.DoResetDisplay(Sender : TObject);
begin
  OCTDisplay.RecoveryIndex := -1;
  ResetOCT;
  OCTDisplay.RefreshDisplay;
  UpdateOCTStatus;
end;

procedure TOCTFormDlg.DoSelectAllOpps(Sender : TObject);
begin
  if not OCTisRunning then begin
    OCTData.EnableAllOPPs;
    OCTDisplay.RefreshDisplay;
  end;
end;

procedure TOCTFormDlg.DoInvertSelection(Sender: TObject);
begin
  if not OCTisRunning then begin
    OCTData.InvertSelection;
    OCTData.BeginUpdate;
    if UsesFixedEndOPP then OCTData.OPP[OCTData.Count-1].Enabled := True;
    if UsesFixedStartOPP then OCTData.OPP[0].Enabled := True;
    OCTData.EndUpdate;
    OCTDisplay.RefreshDisplay;
  end;
end;

procedure TOCTFormDlg.DoOctDisplayClicked(Sender: TOBject);
var OPPIndex : Integer;
OppName : String;
begin

  if not Named.GetAsBoolean(Tags[ortOCTRunning]) then begin
    if OCTDisplay.ClickedRow > 0 then begin
      OPPIndex := OCTDisplay.ClickedRow-1;
      if (OPPIndex = 0) and UsesFixedStartOPP then
        begin
        OppName :=  OCTData.OPP[OPPIndex].FileName;
        if UpperCase(OppName) = UpperCase(FixedStartOPP) then
          begin
          exit;
          end
        else
          begin
          //!!!
          end;
        end;
      if (OPPIndex = OCTData.Count-1) and UsesFixedEndOPP then
        begin
        OppName :=  OCTData.OPP[OPPIndex].FileName;
        if UpperCase(OppName) = UpperCase(FixedEndOPP) then
          begin
          exit;
          end
        else
          begin
          //!!!
          end;
        end;
      if OCTData.OPP[OPPIndex].Enabled then begin
        if OCTData.OPPsEnabled <= 1 then
          Exit; // Must always be one clicked
      end;
//      OCTData.Lock;
//      try
      OCTData.OPP[OPPIndex].Enabled := not (OCTData.OPP[OPPIndex].Enabled);
//      finally
//      OCTData.UnLock;
//      end;
//      ResetOCT;
    end;
    OCTDisplay.RefreshDisplay;
  end;
end;

{procedure TOCTFormDlg.OCTModeChange(Sender : TObject);
begin
if isOCTMode then
   begin
   SetDisplayMode(odmDefault);
   end
else
   begin
   SetDisplayMode(odmNotOCT);
   end

end;  }


{procedure TOCTFormDlg.OCTActiveChanged;
begin
  OCTDisplay.RunningRow := OPPIndex + 1;
  UpdateOCTStatus
end;}

procedure TOCTFormDlg.DoChangeSize(Sender: TObject);
begin
  Named.SetAsBoolean(SelfStartTag,True);
  Named.SetAsBoolean(SelfStartTag,False);
end;

procedure TOCTFormDlg.BtnCloseClick(Sender: TObject);
begin
  Close;
end;

procedure TOCTFormDlg.AbandonPartButtonMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  CurrentRecoveryOption := roAbandonPartInCell;
  SetButtonPanel(roAbandonPartInCell);
end;

procedure TOCTFormDlg.AcceptKeyMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
OPPNumber : Integer;
begin
OCTdata.BeginUpdate;
try

case CurrentRecoveryOPtion of
    roNone:
    begin
    end;
    roRestartCurrent:
    begin
    OCTData.OPP[OCTDisplay.RecoveryIndex].Status := osNone;
    OCTData.OPP[OCTDisplay.RecoveryIndex].ReworkStatus := OCTData.OPP[OCTDisplay.RecoveryIndex].ReworkStatus+'R';
    end;

    roAbandonCurrent:
    begin
    if Phist.Configured then
      begin
      MakePHistEndRecord(OCTData.OPP[OCTDisplay.RecoveryIndex],True);
      end;
    OCTData.OPP[OCTDisplay.RecoveryIndex].Status := osAbandoned;
    end;

    roAbandonOCT:
    begin
    if Phist.Configured then
        begin
        MakePHistEndRecord(OCTData.OPP[OCTDisplay.RecoveryIndex],True);
        end;
    if UsesFixedEndOPP then
      begin
      if (OCTDisplay.RecoveryIndex = OCTData.Count-1) then
         begin
         // dont set the last one to abandon  it is the unload part opp
         OCTData.OPP[OCTDisplay.RecoveryIndex].Status := osNone;
         end
      else
         begin
         For OppNumber := OCTDisplay.RecoveryIndex to OCTData.Count-2 do
            begin
            OCTData.OPP[OppNumber].Status := osAbandoned;
            end;
         end
      end
    else
      begin
      For OppNumber := OCTDisplay.RecoveryIndex to OCTData.Count-1 do
        begin
        OCTData.OPP[OppNumber].Status := osAbandoned;
        end;
      end;
    end;
    roAbandonPartInCell,
    roReworkOPP,
    roNotRework:
    begin
    end;
end; // case
finally
ClearRecoveryOptions;
OCTdata.EndUpdate;
if ResetOCTOnAbandon then
    begin
    ResetOCT;
    end;
OCTDisplay.UpdateOCTDisplayTrigger := True;
end;
end;

procedure TOCTFormDlg.RestartCurrentButtonMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  if OCTDisplay.RecoveryIndex >=0 then
     begin
     CurrentRecoveryOption := roRestartCurrent;
     SetButtonPanel(CurrentRecoveryOption);
     end;
end;

procedure TOCTFormDlg.SkipButtonMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  if OCTDisplay.RecoveryIndex >=0 then
     begin
     CurrentRecoveryOption := roAbandonCurrent;
     SetButtonPanel(CurrentRecoveryOption);
     end;
end;

procedure TOCTFormDlg.AbandonOctButtonMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  if OCTDisplay.RecoveryIndex >=0 then
     begin
     CurrentRecoveryOption := roAbandonOCT;
     SetButtonPanel(CurrentRecoveryOption);
     end;
end;


procedure TOCTFormDlg.BtnGeneralMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  if Sender = BtnOpenToolDoor then begin
    Named.SetAsBoolean(Tags[ortOpenToolDoor], True);
  end else if Sender = BtnOpenMainDoor then begin
    Named.SetAsBoolean(Tags[ortOpenMainDoor], True);
  end else if Sender = btnTS1 then begin
    Named.SetAsBoolean(Tags[ortReqToolStation1], True);
  end else if Sender = btnTS2 then begin
    Named.SetAsBoolean(Tags[ortReqToolStation2], True);
  end else if Sender = btnTS3 then begin
    Named.SetAsBoolean(Tags[ortReqToolStation3], True);
  end else if Sender = btnTS4 then begin
    Named.SetAsBoolean(Tags[ortReqToolStation4], True);
  end else if Sender = btnTS5 then begin
    Named.SetAsBoolean(Tags[ortReqToolStation5], True);
  end else if Sender = btnTS6 then begin
    Named.SetAsBoolean(Tags[ortReqToolStation6], True);
  end else if Sender = BtnFaultAck then begin
       Named.SetAsBoolean(Tags[ortFaultAcknowledgeTag], True);
  end else if Sender = BtnPartChuck then begin
    Named.SetAsBoolean(Tags[ortReqChuckChange], True);
  end else if Sender = BtnToolChuck then begin
    Named.SetAsBoolean(Tags[ortReqToolChuckChange], True);
  end;

  if Sender = ToolDisplayToggle then
     begin
     if Named.GetASInteger(Tags[ortOCTDisplayMode]) = ord(tldStatusOnly) then
        begin
        SetOCTDisplayMode(tldVersusOCT);
        end
     else
        begin
        SetOCTDisplayMode(tldStatusOnly);
        end

     end
end;

procedure TOCTFormDlg.BtnGeneralMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  if Sender = BtnOpenToolDoor then begin
    Named.SetAsBoolean(Tags[ortOpenToolDoor], False);
  end else if Sender = BtnOpenMainDoor then begin
    Named.SetAsBoolean(Tags[ortOpenMainDoor], False);
  end else if Sender = btnTS1 then begin
    Named.SetAsBoolean(Tags[ortReqToolStation1], False);
  end else if Sender = btnTS2 then begin
    Named.SetAsBoolean(Tags[ortReqToolStation2], False);
  end else if Sender = btnTS3 then begin
    Named.SetAsBoolean(Tags[ortReqToolStation3], False);
  end else if Sender = btnTS4 then begin
    Named.SetAsBoolean(Tags[ortReqToolStation4], False);
  end else if Sender = btnTS5 then begin
    Named.SetAsBoolean(Tags[ortReqToolStation5], False);
  end else if Sender = btnTS6 then begin
    Named.SetAsBoolean(Tags[ortReqToolStation6], False);
  end else if Sender = BtnPartChuck then begin
    Named.SetAsBoolean(Tags[ortReqChuckChange], False);
  end else if Sender = BtnToolChuck then begin
    Named.SetAsBoolean(Tags[ortReqToolChuckChange], False);
  end else if Sender = btnInvalidate then begin
    if ToolData.IsToolAtEx1(0)  then
       begin
       if TAreYouSure.Execute('Are you sure you want to invalidate the tool in head?') then
           ZeroToolLifeForToolInLocation(0);
       end;
  end else if Sender = BtnFaultAck then begin
       Named.SetAsBoolean(Tags[ortFaultAcknowledgeTag], True);
       Named.SetAsBoolean(Tags[ortFaultAcknowledgeTag], False);
  end else if Sender = BtnShowPartData then begin
       if assigned(PartButtonTag) then
          begin
          Named.SetAsBoolean(PartButtonTag,True);
          Named.SetAsBoolean(PartButtonTag,False);
          end
  end;
end;


procedure TOCTFormDlg.UpdateToolDisplayFor(ChangerPos : Integer);
begin
Stn1Location.Color :=  clInfobk;
Stn2Location.Color :=  clInfobk;
Stn3Location.Color :=  clInfobk;
Stn4Location.Color :=  clInfobk;
Stn5Location.Color :=  clInfobk;
Stn6Location.Color :=  clInfobk;
case ChangerPos of
     0:
     begin
     Stn1Location.Caption := NotInPositionText;
     Stn2Location.Caption := NotInPositionText;
     Stn3Location.Caption := NotInPositionText;
     Stn4Location.Caption := NotInPositionText;
     Stn5Location.Caption := NotInPositionText;
     Stn6Location.Caption := NotInPositionText;
     end;

     1:
     begin
     Stn1Location.Caption := DoorText;
     Stn1Location.Color := clYellow;
     end;

     2:
     begin
     Stn2Location.Caption := DoorText;
     Stn2Location.Color := clYellow;
     end;

     3:
     begin
     Stn3Location.Caption := DoorText;
     Stn3Location.Color := clYellow;
     end;

     4:
     begin
     Stn4Location.Caption := DoorText;
     Stn4Location.Color := clYellow;
     end;

     5:
     begin
     Stn5Location.Caption := DoorText;
     Stn5Location.Color := clYellow;
     end;

     6:
     begin
     Stn6Location.Caption := DoorText;
     Stn6Location.Color := clYellow;
     end;
     else
         begin
         Stn1Location.Caption := NotInPositionText;
         Stn2Location.Caption := NotInPositionText;
         Stn3Location.Caption := NotInPositionText;
         Stn4Location.Caption := NotInPositionText;
         Stn5Location.Caption := NotInPositionText;
         Stn6Location.Caption := NotInPositionText;
         BtnOpenToolDoor.IsEnabled := False;
         end;

end; // case
end;
procedure TOCTFormDlg.UpdateStnButtons;

var
BColor,TextColor : TColor;
Flash : Boolean;
Button : TThreeLineButton;
Index : Integer;
AddCount : Integer;
ToolPresent : Boolean;
EntryNumber : INteger;
Found : Boolean;
Location : Integer;
//RemoveCount : Integer;
SuperdependentToolID : String;
RemoveToolLocation : Integer;
AddToolLocation : Integer;
CanOverride : Boolean;
MustRemove : Boolean;
CanAdd : Boolean;
begin
AddCount := 0;
AddToolLocation := -1;
MustRemove := False;
CanAdd := False;
inc(BUCount);
BULabel.Caption := IntToStr(BUCount);
For Index := 0 to 6 do
begin
case Index of
   0:Button := BtnHead;
   1:Button := BtnTS1;
   2:Button := BtnTS2;
   3:Button := BtnTS3;
   4:Button := BtnTS4;
   5:Button := BtnTS5;
   6:Button := BtnTS6;
   end;
if (FTCStnStatus[Index] = tmsPresentNotRegistered) and (Index > 0)then
   begin
   Button.Legend := NotRegisteredButtonText;
   Button.ButtonColor := clYellow;
   Button.LegendFont.Color := clBlue;
   end
else
    begin
    ToolPresent := BuildToolInMachineStatus(Index);
    if not ToolPresent then
      begin
      if (ToolData.Count < ToolPosCount ) and (Index > 0) then
          begin
          AddToolLocation := Index;
          end;
      if MachineTools[Index].State = tdsADDTool then
          begin
          // here to trap too many adds
          if ADDCount+ToolData.Count > 3 then
             MachineTools[Index].State := tdsNoTool;
          end;
      end;
    if (FTCStnStatus[Index] = tmsRegisteredNotPresent) and (Index > 0) then
       begin
       Button.Legend := NotPresentButtonText;
       Button.ButtonColor := clYellow;
       Button.LegendFont.Color := clBlue;
       end
    else
        begin
        DecodeButtonColour(MachineTools[Index].State,BColor,TextColor,Flash);
        Button.ButtonColor := BColor;
        Button.LegendFont.Color := TextColor;
        Button.Flash := Flash;
        case MachineTools[Index].State of
        tdsNoTool:
             begin
             Button.Legend := EMPTYButtonText;
             end;
        tdsADDTool:
             begin
             Button.Legend := MachineTools[Index].BString;
             inc(AddCount);
             CanAdd := True;
             end;
        TdsOK,tdsStatusOnly:
             begin
             Button.Legend := MachineTools[Index].Name+ '~' + MachineTools[Index].Serial + '~' + MachineTools[Index].Remaining;
             end;

        tdsMustRemove:
             begin
             Button.Legend := MachineTools[Index].Name+ '~' + MachineTools[Index].BString + '~' + MachineTools[Index].Remaining;
             MustRemove := True;
             end;

        tdsCanRemove,
        tdsNeverRequired,
        tdsInvalid:
             begin
             Button.Legend := MachineTools[Index].Name+ '~' + MachineTools[Index].BString + '~' + MachineTools[Index].Remaining;
             end;
        end; // case;

        end
    end;
    Named.SetAsBoolean(Tags[ortMustRemoveTool],MustRemove);
end;
// Now Trap special case of mustload tool in required tool list
// and Max Tools already in machine and none is required to be removed

if (ToolData.Count > ToolPosCount-1) and isOCTMode and not isStatusOnlyDisplay then
  begin
  EntryNumber := 0;
  Found := False;
  while (EntryNumber < RequiredToolData.Count) and (not Found) do
    begin
    try
    if RequiredToolData.Tool[entryNumber].Priority = 0 then
      begin
      Found := True;
      end;
    except
    //Found := False;
    exit;
    end;
    inc(EntryNumber);
    end;
  if Found then
    begin
    // Now find best tool to remove ie lowest existing in sort order of OPP
    // that is not in head
    Found := False;
    EntryNumber := 0;
    while (EntryNumber < ToolData.Count) and (Not Found) do
        begin
        if ToolData.Tool[EntryNumber].Ex1 <> 0 then
            begin
            Found := True;
            Location := ToolData.Tool[EntryNumber].Ex1;
            case Location of
               1:Button := BtnTS1;
               2:Button := BtnTS2;
               3:Button := BtnTS3;
               4:Button := BtnTS4;
               5:Button := BtnTS5;
               6:Button := BtnTS6;
            end; // case
            Button.Legend := MachineTools[Location].Name+ '~' + RemoveText + '~' + MachineTools[Location].Remaining;
            Button.ButtonColor := RemoveColour;
            Button.Flash := True;
            end;
        inc(EntryNumber);
        end;
    end
  end;

// Now Trap special case of a tool on which all are dependent (i.e Probe)
// not in the machine with tools in the machine which are no longer required
// and all tooling satisfied. This shoul call for the tool with dependents
// to be loaded.

// First are all tools satisfied

if (not RequiredToolData.ToolsAreRequired) and isOCTMode and not isStatusOnlyDisplay then
  begin
  CanOverride := False;
  RemoveToolLocation := -1;
  SuperdependentToolID := OCTData.SuperdependentToolID(UsesFixedStartOPP,UsesFixedEndOPP);
  if SuperdependentToolID <> '' then
      begin
      // Is the superdependent already in the machine ???
      if not ToolData.ToolTypeInMachine(SuperdependentToolID) then
         begin
         // Is there a free space in the tool changer
         if (ToolData.Count < ToolPosCount) and (AddToolLocation > 0) then
              begin
              CanOverride := True;
              end
          else
              begin
              // Is there a tool that is no longer required on the tool changer
              RemoveToolLocation := RToolDisplay.GetUnrequiredToolLocation;
              if RemoveToolLocation > 0 then
                  begin
                  CanOverride := True;
                  end;
              end;
        if CanOverride then
           begin
           RToolDisplay.ShowToolAsPriority1(SuperdependentToolID);

           if AddToolLocation > 0 then
           begin
           case RemoveToolLocation of
               1:Button := BtnTS1;
               2:Button := BtnTS2;
               3:Button := BtnTS3;
               4:Button := BtnTS4;
               5:Button := BtnTS5;
               6:Button := BtnTS6;
           end; // case
           Button.Legend :=  ADDTEXT;
           Button.ButtonColor := AddColour;
           Button.Flash := False;
           CanAdd := True; // THis could be MustADD!!!
           end;

        if RemoveToolLocation > 0 then
          begin
          case RemoveToolLocation of
               1:Button := BtnTS1;
               2:Button := BtnTS2;
               3:Button := BtnTS3;
               4:Button := BtnTS4;
               5:Button := BtnTS5;
               6:Button := BtnTS6;
          end; // case
          Button.Legend := MachineTools[RemoveToolLocation].Name+ '~' + REMOVETEXT + '~' + MachineTools[RemoveToolLocation].Remaining;
          Button.ButtonColor := RemoveColour;
          Button.Flash := True;
          end;
        end;
      end;
  end;
end;
Named.SetAsBoolean(Tags[ortToolLoadRequested],CanADD);

end;

procedure TOCTFormDlg.DecodeButtonColour(ColourCode : TToolDataState;
                             var Colour : TColor;
                             var TextColour : TColor;
                             var Flash : Boolean);
begin
Colour := ButtonColourArray[ColourCode].Colour;
TextColour := ButtonColourArray[ColourCode].FontColor;
Flash :=  ButtonColourArray[ColourCode].Flash;
end;




procedure TOCTFormDlg.UpdatePartProgDisplay(ForceProgLoad : Boolean);
var
ProgramText: WideString;
ActSubIndex : Integer;
SubName     : array [0..256] of Char;
DisplayName : String;
ExtStart    : Integer;
Buffer      : String;
begin
ActSubIndex :=NC.ActiveSubRoutine;
if (ForceProgLoad or ((ActSubIndex <> LastSubIndex) and (ActSubIndex >= 0))) then
   begin
   LastSubIndex  := ActSubIndex;
   if assigned(PrDisplay) then
      begin
         try
         ProgramText := NC.SubRoutine[ActSubIndex];
         except
         ProgramText := InvalidProgramText;
         end;
      PrDisplay.LoadProgramText(ProgramText);
      end;
   end;
if ActSubIndex >= 0 then
   begin
   PrDisplay.CurrentLine := NC.GetProgramLine;
   ProgLineDisplay.Caption := Format('%5d : %5d',[PrDisplay.CurrentLine+1,PrDisplay.LineCount]);
   DisplayName := ExtractFileName(OCTFilename);
   ExtStart := Pos('.',DisplayName);
   If ExtStart > 0 then
      begin
      DisplayName := Copy(DisplayName,1,ExtStart-1);
      end;
   Named.GetAsString(Tags[ortLoadedOPPName],SubName,255);
   Buffer := Subname;
   Buffer := ExtractFileName(Buffer);
   ExtStart := Pos('.',Buffer);
   If ExtStart > 0 then
      begin
      Buffer := Copy(Buffer,1,ExtStart-1);
      end;

   DisplayName := Format('%s-%s',[DisplayName,Buffer]);
   ProgNameDisplay.Caption := DisplayName;
   end;
end;

procedure TOCTFormDlg.BtnCloseMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
Close;
end;

procedure TOCTFormDlg.BtnShowToolTableMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
if OCTLowerPageControl.ActivePage = ToolManSheet then
  begin
  SetDisplayMode(odmPartProgram);
  end
else
  begin
  SetDisplayMode(odmToolreq);
  end;
end;

procedure TOCTFormDlg.BtnDefaultDisplayMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
SetDisplayMode(odmDefault);
end;

procedure TOCTFormDlg.SetSoftButtonEnables(InRec : Boolean;OCTMode : Boolean);
begin

if OCtMode then
    begin
    BtnDefaultDisplay.IsEnabled := not InRec;
    BtnShowToolTable.IsEnabled := not InRec;
    BtnShowOPPOnly.IsEnabled := not InRec;

    if BaseOCTPanel.Height > OPPMin then
           begin
           BtnSplitUp.IsEnabled := not InRec;
           end
    else
           begin
           BtnSplitUp.IsEnabled := not False;
           end;

    if BaseOCTPanel.Height < OPPMax then
           begin
           BtnSplitDown.IsEnabled := not InRec;
           end
    else
           begin
           BtnSplitDown.IsEnabled := not False;
           end;
    end
else
    begin
    BtnSplitUp.IsEnabled := False;
    BtnSplitDown.IsEnabled := False;
    BtnDefaultDisplay.IsEnabled := False;
    BtnShowOPPOnly.IsEnabled := False;
    BtnShowToolTable.IsEnabled := False;
    BtnDefaultDisplay.IsEnabled := False;
    end;
  BtnInvalidate.IsEnabled := ToolData.IsToolAtEx1(0)
                             and not Named.GetAsBoolean(Tags[ortOCtRunning])
                             and not Named.GetAsBoolean(Tags[ortInCycle]);
end;

procedure TOCTFormDlg.ResizeChuckandDoorButtons;
begin
Panel20.Height := (Panel18.Height div 2)-2;
BtnPartChuck.Height := Panel20.Height-24;
BtnToolChuck.Height := Panel21.Height-24;
Panel28.Height := (Panel27.Height div 2)-2;
BtnOpenMainDoor.Height := Panel28.Height-24;
BtnOpenToolDoor.Height := Panel29.Height-24;

end;

procedure TOCTFormDlg.SetDisplayMode(DMode: TOctDisplayModes);
begin
  DisplayMode := DMode;
  SetSoftButtonEnables(InRecovery,isOCTMode);
  ToolDisplayToggle.Visible := Named.GetASBoolean(Tags[ortTCFitted]);

  if Inrecovery then
     begin
     BaseOCTPanel.Align := alClient;
     OCTDisplay.Align := alTop;
     OCTDisplay.Height := 300;
     AcceptKey.IsEnabled := False;
     AcceptKey.Visible := True;

     RecoveryPanel.Visible := True;
     RecoveryPanel.Height := BaseOCTPanel.Height-OCTDisplay.Height-10;
     OCTDisplay.Width := Width- RecoveryPanel.Width - 5;
     OCTDisplay.Left := RecoveryPanel.Width+5;
     OCTLowerPageControl.Visible := False;
     BtnSplitUp.IsEnabled := False;
     BtnSplitDown.IsEnabled := False;
     end
  else
      begin
      case  DisplayMode of
         odmDefault:
         begin
         OCTLowerPageControl.Visible := True;
         BaseOCTPanel.Visible := True;
         RecoveryPanel.Visible := False;
         OCTDisplay.Align := alClient;
         BaseOCTPanel.Align := alTop;
         BaseOCTPanel.Height := 300;
         OCTLowerPageControl.ActivePage := ToolManSheet;
         BtnSplitUp.IsEnabled := True;
         BtnSplitDown.IsEnabled := True;
         SetOCTDisplayMode(tldVersusOCT);
         BtnShowToolTable.Legend := ShowPartProgramText;
         end;

         odmAllOPP:
         begin
         BaseOCTPanel.Visible := True;
         OCTLowerPageControl.Visible := False;
         BaseOCTPanel.Align := alClient;
         BtnSplitUp.IsEnabled := False;
         BtnSplitDown.IsEnabled := False;
         end;

         odmPartProgram:
         begin
         BaseOCTPanel.Visible := True;
         OCTLowerPageControl.Visible := True;
         OCTLowerPageControl.ActivePage := PDisplaySheet;
         BtnSplitUp.IsEnabled := True;
         BtnSplitDown.IsEnabled := True;
         BtnShowToolTable.Legend := ShowToolTableText;
         end;

         odmToolreq:
         begin
         BaseOCTPanel.Visible := True;
         OCTLowerPageControl.Visible := True;
         OCTLowerPageControl.ActivePage := ToolManSheet;
         BtnSplitUp.IsEnabled := True;
         BtnSplitDown.IsEnabled := True;
         BtnShowToolTable.Legend := ShowPartProgramText;
         end;

         odmNotOCT:
         begin
         BaseOCTPanel.Visible := False;
         OCTLowerPageControl.Visible := True;
         OCTLowerPageControl.ActivePage := PDisplaySheet;
         OCTLowerPageControl.Align := alClient;
         SetOCTDisplayMode(tldStatusOnly);
         ToolDisplayToggle.Visible := False;
         end;
      end;
    ResizeChuckandDoorButtons;
  end;

end;

procedure TOCTFormDlg.BtnShowOPPOnlyMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
SetDisplayMode(odmAllOPP);
end;

procedure TOCTFormDlg.BtnShowPartDataMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
SetDisplayMode(odmShowPartPopUp);

end;


procedure TOCTFormDlg.BtnSplitUpMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
BtnSplitDown.IsEnabled := True;
if BaseOCTPanel.Height > OPPMin then
   begin
   BaseOCTPanel.Height := BaseOCTPanel.Height - OCTDisplay.UnitHeight;
{   if BaseOCTPanel.Height < OPPMin then
      begin
      BaseOCTPanel.Height := OPPMin;
      end;}
   end
else
   begin
   BtnSplitUp.IsEnabled := False;
   end;

end;

procedure TOCTFormDlg.BtnSplitDownMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
BtnSplitUp.IsEnabled := True;
if BaseOCTPanel.Height < OPPMax then
   begin
   BaseOCTPanel.Height := BaseOCTPanel.Height + OCTDisplay.UnitHeight;
{   if BaseOCTPanel.Height > OPPMax then
      begin
      BaseOCTPanel.Height := OPPMax;
      end;}
   end
else
   begin
   BtnSplitDown.IsEnabled := False;
   end;
end;


procedure TOCTFormDlg.Panel12Resize(Sender: TObject);
begin
MessageLabel.Height := (Panel12.Height div 2)-2;
MessageLabel2.Height := (Panel12.Height div 2)-2;

end;

procedure TOCTFormDlg.SetAcceptKeyState(Enabled: Boolean);
begin
AcceptKey.IsEnabled := Enabled;
if enabled then
   begin
   Acceptkey.ButtonColor := clLime;
   end
else
    begin
    Acceptkey.ButtonColor := clGray;
    end;
end;

procedure TOCTFormDlg.SetOCTDisplayMode(Mode: TTLDisplayMode);
begin
if (mode = tldVersusOCT) then
        begin
        Named.SetASInteger(Tags[ortOCTDisplayMode],ord(tldVersusOCT));
        OctDisplayModePanel.Caption := vsOCTText;
        end
     else
        begin
        Named.SetASInteger(Tags[ortOCTDisplayMode],ord(tldStatusOnly));
        OctDisplayModePanel.Caption := StatusOnlyText;
        end;
  LazyUpdateFlag := True;
end;

procedure TOCTFormDlg.BaseOCTPanelResize(Sender: TObject);
begin
  if OCTisRunning then begin
    OCTDisplay.RunningRow := OPPIndex + 1;
    OCTDisplay.RefreshDisplay;
    end;
end;


procedure TOCTFormDlg.UpdateButtonEnable;
var
Inhibit : Boolean;
Permissive : Boolean;
begin
 BtnOPenToolDoor.IsEnabled := not Named.GetAsBoolean(Tags[ortToolDoorOpenInhibit]);
 BtnOPenMainDoor.IsEnabled := not Named.GetAsBoolean(Tags[ortMainDoorOPenInhibit]);
 BtnToolChuck.IsEnabled :=  Named.GetAsBoolean(Tags[ortToolClampPermissive]);
 BtnPartChuck.IsEnabled :=  Named.GetAsBoolean(Tags[ortPartClampPermissive]);
end;


procedure TOCTFormDlg.SetStatus(const Text: string; Index: Integer);
begin
  case Index of
    1 : LabelStatus1.Caption := Text;
    2 : LabelStatus2.Caption := Text;
  end;
if (LabelStatus1.Caption = '') and ( LabelStatus2.Caption = '') then
   begin
   OctStatusPanel.Height := 4;
   end
else
   begin
   OctStatusPanel.Height := 40;
   end
end;

procedure TOCTFormDlg.ReadConfiguration;
var I : TODisplayReportingTags;
    Ini : TIniFile;
    ColourNumber : TToolDataState;
    KeyString : String;
    ColourDef : String;
    Buffer    : String;

begin
if FileExists(DllProfilePath + 'oct.cfg') then
  begin
  Ini := TIniFile.Create(DllProfilePath + 'oct.cfg');
  try
    FixedEndOPP   := ExtractFileName(Ini.ReadString('OCT_TM', 'FixedEndOPP', ''));
    UsesFixedEndOPP := (FixedEndOPP <> '');
    FixedStartOPP := ExtractFileName(Ini.ReadString('OCT_TM', 'FixedStartOPP', ''));
    UsesFixedStartOPP := (FixedStartOPP <> '');


    for I := Low(TODisplayReportingTags) to High(TODisplayReportingTags) do
      TagNames[I] := Ini.ReadString('Tags', TagDefinitions[I].Ini, TagDefinitions[I].Tag);

      For ColourNumber := Low(TToolDataState) to high(TToolDataState) do
          begin
          KeyString := GetEnumName(TYpeInfo(TToolDataState),Ord(ColourNumber));
          ColourDef := Ini.ReadString('OperatorDisplay',KeyString,'');
          if ColourDef <> '' then
              begin
              with ButtonColourArray[ColourNumber] do
                  begin
                  try
                  Colour := StringToColor(Format('cl%s',[GetToken(ColourDef)]));
                  FontColor := StringToColor(Format('cl%s',[GetToken(ColourDef)]));
                  Buffer := GetToken(ColourDef);
                  Flash := (Buffer[1] = 'f') or (Buffer[1] = 'F')
                  except
                  end;
                  end;
              end;
          end;
    Buffer := Ini.ReadString('OperatorDisplay','ResetOCTonAbandon','False');
    ResetOCTOnAbandon := (Buffer[1] = 'T') or (Buffer[1] = 't');

    Buffer := Ini.ReadString('OperatorDisplay','ShowZeroLifeButton','False');
    ShowZeroLifeButton := (Buffer[1] = 'T') or (Buffer[1] = 't');
    BtnInvalidate.Visible := ShowZeroLifeButton;
    if not ShowZeroLifeButton then  InvalidateBlank.Align := alClient;
    InvalidateBlank.Visible := not BtnInvalidate.Visible;

    UpdateSweepCount := Ini.ReadInteger('OperatorDisplay','UpdateDelayInterval',10);
    CurrentUpdateCount := 0;


    Buffer := Ini.ReadString('OperatorDisplay','ShowPartButton','False');
    ShowPartButton := (Buffer[1] = 'T') or (Buffer[1] = 't');

    if ShowPartButton then
      begin
      PartButtonTagName := Ini.ReadString('OperatorDisplay','PartButtonTag','');
      end;

    Buffer := Ini.ReadString('OperatorDisplay','ShowResetButton','False');
    ShowResetButton := (Buffer[1] = 'T') or (Buffer[1] = 't');

  finally
    Ini.Free;
  end;
  end;
end;


function TOCTFormDlg.OCTisRunning: Boolean;
begin
Result := Named.GetAsBoolean(Tags[ortOCTRunning])
end;


function TOCTFormDlg.isOCTMode: Boolean;
begin
Result := Named.GetAsBoolean(Tags[ortOCTMode])
end;

function  TOCTFormDlg.isStatusOnlyDisplay: Boolean;
begin
Result := Named.GetASInteger(Tags[ortOCTDisplayMode]) = ord(tldStatusOnly);
end;

function TOCTFormDlg.OPPIndex: Integer;
begin
Result := Named.GetAsInteger(Tags[ortOCTIndex])
end;

function TOCTFormDlg.OCTFilename: String;
var ctmp : array [0..128] of Char;
begin
Named.GetAsString(Tags[ortLoadedOCTName],ctmp,Length(ctmp) - 1);
REsult := ctmp;
end;


// returns true id there is a tool mismatch status chasnges either way
(*
function TOCTFormDlg.UpdateToolStatus: Boolean;
var
TCPos : Integer;
prox1,Prox2,Prox3,Prox4,Prox5,Prox6 : Boolean;
Tag1,Tag2,Tag3,Tag4,Tag5,Tag6 : tmTagStatus;
Mismatch : Integer;
begin
  inherited;
  try
  // here to monitor tool consistency on tool changer and head vs toollife table
  if Named.GetAsBoolean(Tags[ortToolChangerStable])
  and Named.GetAsBoolean(Tags[ortToolDoorLocked])then
   begin
   Dec(SweepCount);
   if SweepCount < 0 then
     begin
     SweepCount := 25;
     FTCStnStatus[1]:= tmsEmpty;
     FTCStnStatus[2]:= tmsEmpty;
     FTCStnStatus[3]:= tmsEmpty;
     FTCStnStatus[4]:= tmsEmpty;
     FTCStnStatus[5]:= tmsEmpty;
     FTCStnStatus[6]:= tmsEmpty;
     Mismatch := 0;


     Prox1 := Named.GetAsBoolean(Tags[ortToolInPosition1Proximity]);
     Prox2 := Named.GetAsBoolean(Tags[ortToolInPosition2Proximity]);
     Prox3 := Named.GetAsBoolean(Tags[ortToolInPosition3Proximity]);
     Prox4 := Named.GetAsBoolean(Tags[ortToolInPosition4Proximity]);
     Prox5 := Named.GetAsBoolean(Tags[ortToolInPosition5Proximity]);
     Prox6 := Named.GetAsBoolean(Tags[ortToolInPosition6Proximity]);

     TCPos := Named.GetAsInteger(Tags[ortToolChangerPosition]);

     case TCPos of
          1:
          begin
          Tag1.TagPresent :=  ToolData.IsToolAtEx1(1);
          Tag2.TagPresent :=  ToolData.IsToolAtEx1(2);
          Tag3.TagPresent :=  ToolData.IsToolAtEx1(3);
          Tag4.TagPresent :=  ToolData.IsToolAtEx1(4);
          Tag5.TagPresent :=  ToolData.IsToolAtEx1(5);
          Tag6.TagPresent :=  ToolData.IsToolAtEx1(6);
          Tag1.StationNumber := 1;
          Tag2.StationNumber := 2;
          Tag3.StationNumber := 3;
          Tag4.StationNumber := 4;
          Tag5.StationNumber := 5;
          Tag6.StationNumber := 6;
          end;

          2:
          begin
          Tag1.TagPresent :=  ToolData.IsToolAtEx1(2);
          Tag2.TagPresent :=  ToolData.IsToolAtEx1(3);
          Tag3.TagPresent :=  ToolData.IsToolAtEx1(4);
          Tag4.TagPresent :=  ToolData.IsToolAtEx1(5);
          Tag5.TagPresent :=  ToolData.IsToolAtEx1(6);
          Tag6.TagPresent :=  ToolData.IsToolAtEx1(1);
          Tag1.StationNumber := 2;
          Tag2.StationNumber := 3;
          Tag3.StationNumber := 4;
          Tag4.StationNumber := 5;
          Tag5.StationNumber := 6;
          Tag6.StationNumber := 1;
          end;
          3:
          begin
          Tag1.TagPresent :=  ToolData.IsToolAtEx1(3);
          Tag2.TagPresent :=  ToolData.IsToolAtEx1(4);
          Tag3.TagPresent :=  ToolData.IsToolAtEx1(5);
          Tag4.TagPresent :=  ToolData.IsToolAtEx1(6);
          Tag5.TagPresent :=  ToolData.IsToolAtEx1(1);
          Tag6.TagPresent :=  ToolData.IsToolAtEx1(2);
          Tag1.StationNumber := 3;
          Tag2.StationNumber := 4;
          Tag3.StationNumber := 5;
          Tag4.StationNumber := 6;
          Tag5.StationNumber := 1;
          Tag6.StationNumber := 2;
          end;
          4:
          begin
          Tag1.TagPresent :=  ToolData.IsToolAtEx1(4);
          Tag2.TagPresent :=  ToolData.IsToolAtEx1(5);
          Tag3.TagPresent :=  ToolData.IsToolAtEx1(6);
          Tag4.TagPresent :=  ToolData.IsToolAtEx1(1);
          Tag5.TagPresent :=  ToolData.IsToolAtEx1(2);
          Tag6.TagPresent :=  ToolData.IsToolAtEx1(3);
          Tag1.StationNumber := 4;
          Tag2.StationNumber := 5;
          Tag3.StationNumber := 6;
          Tag4.StationNumber := 1;
          Tag5.StationNumber := 2;
          Tag6.StationNumber := 3;
          end;
          5:
          begin
          Tag1.TagPresent :=  ToolData.IsToolAtEx1(5);
          Tag2.TagPresent :=  ToolData.IsToolAtEx1(6);
          Tag3.TagPresent :=  ToolData.IsToolAtEx1(1);
          Tag4.TagPresent :=  ToolData.IsToolAtEx1(2);
          Tag5.TagPresent :=  ToolData.IsToolAtEx1(3);
          Tag6.TagPresent :=  ToolData.IsToolAtEx1(4);
          Tag1.StationNumber := 5;
          Tag2.StationNumber := 6;
          Tag3.StationNumber := 1;
          Tag4.StationNumber := 2;
          Tag5.StationNumber := 3;
          Tag6.StationNumber := 4;
          end;
          6:
          begin
          Tag1.TagPresent :=  ToolData.IsToolAtEx1(6);
          Tag2.TagPresent :=  ToolData.IsToolAtEx1(1);
          Tag3.TagPresent :=  ToolData.IsToolAtEx1(2);
          Tag4.TagPresent :=  ToolData.IsToolAtEx1(3);
          Tag5.TagPresent :=  ToolData.IsToolAtEx1(4);
          Tag6.TagPresent :=  ToolData.IsToolAtEx1(5);
          Tag1.StationNumber := 6;
          Tag2.StationNumber := 1;
          Tag3.StationNumber := 2;
          Tag4.StationNumber := 3;
          Tag5.StationNumber := 4;
          Tag6.StationNumber := 5;
          end;
       end ; // case

       if (Prox1)     and (Tag1.TagPresent) then begin FTCStnStatus[Tag1.StationNumber] := tmsOk end else
       if (not Prox1) and (Tag1.TagPresent) then
                                 begin
                                 FTCStnStatus[Tag1.StationNumber] := tmsRegisteredNotPresent;
                                 Named.SetFaultA(MissmatchText,Format(MissmatchError2,[Tag1.StationNumber]) , FaultWarning, nil);
                                 inc(Mismatch);
                                 end else
       if (Prox1)     and (not Tag1.TagPresent) then
                                     begin
                                     FTCStnStatus[Tag1.StationNumber] := tmsPresentNotRegistered;
                                     Named.SetFaultA(MissmatchText,Format(MissmatchError1,[Tag1.StationNumber]) , FaultWarning, nil);
                                     inc(Mismatch);
                                     end;

       if (Prox2)     and (Tag2.TagPresent) then begin FTCStnStatus[Tag2.StationNumber] := tmsOk end else
       if (not Prox2) and (Tag2.TagPresent) then
                                 begin
                                 FTCStnStatus[Tag2.StationNumber] := tmsRegisteredNotPresent;
                                 Named.SetFaultA(MissmatchText,Format(MissmatchError2,[Tag2.StationNumber]) , FaultWarning, nil);
                                 inc(Mismatch);
                                 end else
       if (Prox2)     and (not Tag2.TagPresent) then
                                     begin
                                     FTCStnStatus[Tag2.StationNumber] := tmsPresentNotRegistered;
                                     Named.SetFaultA(MissmatchText,Format(MissmatchError1,[Tag2.StationNumber]) , FaultWarning, nil);
                                     inc(Mismatch);
                                     end;

       if (Prox3)     and (Tag3.TagPresent) then begin FTCStnStatus[Tag3.StationNumber] := tmsOk end else
       if (not Prox3) and (Tag3.TagPresent) then
                                 begin
                                 FTCStnStatus[Tag3.StationNumber] := tmsRegisteredNotPresent;
                                 Named.SetFaultA(MissmatchText,Format(MissmatchError2,[Tag3.StationNumber]) , FaultWarning, nil);
                                 inc(Mismatch);
                                 end else
       if (Prox3)     and (not Tag3.TagPresent) then
                                     begin
                                     FTCStnStatus[Tag3.StationNumber] := tmsPresentNotRegistered;
                                     Named.SetFaultA(MissmatchText,Format(MissmatchError1,[Tag3.StationNumber]) , FaultWarning, nil);
                                     inc(Mismatch);
                                     end;

       if (Prox4)     and (Tag4.TagPresent) then begin FTCStnStatus[Tag4.StationNumber] := tmsOk end else
       if (not Prox4) and (Tag4.TagPresent) then
                                 begin
                                 FTCStnStatus[Tag4.StationNumber] := tmsRegisteredNotPresent;
                                 Named.SetFaultA(MissmatchText,Format(MissmatchError2,[Tag4.StationNumber]) , FaultWarning, nil);
                                 inc(Mismatch);
                                 end else
       if (Prox4)     and (not Tag4.TagPresent) then
                                     begin
                                     FTCStnStatus[Tag4.StationNumber] := tmsPresentNotRegistered;
                                     Named.SetFaultA(MissmatchText,Format(MissmatchError1,[Tag4.StationNumber]) , FaultWarning, nil);
                                     inc(Mismatch);
                                     end;
       if (Prox5)     and (Tag5.TagPresent) then begin FTCStnStatus[Tag5.StationNumber] := tmsOk end else
       if (not Prox5) and (Tag5.TagPresent) then
                                 begin
                                 FTCStnStatus[Tag5.StationNumber] := tmsRegisteredNotPresent;
                                 Named.SetFaultA(MissmatchText,Format(MissmatchError2,[Tag5.StationNumber]) , FaultWarning, nil);
                                 inc(Mismatch);
                                 end else
       if (Prox5)     and (not Tag5.TagPresent) then
                                     begin
                                     FTCStnStatus[Tag5.StationNumber] := tmsPresentNotRegistered;
                                     Named.SetFaultA(MissmatchText,Format(MissmatchError1,[Tag5.StationNumber]) , FaultWarning, nil);
                                     inc(Mismatch);
                                     end;
       if (Prox6)     and (Tag6.TagPresent) then begin FTCStnStatus[Tag6.StationNumber] := tmsOk end else
       if (not Prox6) and (Tag6.TagPresent) then
                                 begin
                                 FTCStnStatus[Tag6.StationNumber] := tmsRegisteredNotPresent;
                                 Named.SetFaultA(MissmatchText,Format(MissmatchError2,[Tag6.StationNumber]) , FaultWarning, nil);
                                 inc(Mismatch);
                                 end else
       if (Prox6)     and (not Tag6.TagPresent) then
                                     begin
                                     FTCStnStatus[Tag6.StationNumber] := tmsPresentNotRegistered;
                                     Named.SetFaultA(MissmatchText,Format(MissmatchError1,[Tag6.StationNumber]) , FaultWarning, nil);
                                     inc(Mismatch);
                                     end;
       if (Mismatch <> LastMismatchState) then
          begin
          if MisMatch > 0 then
             begin
             if assigned(Tags[ortToolMismatch]) then Named.SetAsInteger(Tags[ortToolMismatch],1);
             end
          else
              begin
              if assigned(Tags[ortToolMismatch]) then Named.SetAsInteger(Tags[ortToolMismatch],0);
              end;
          LastMisMatchState := MisMatch;
          end;
       end;
     end;
   except
   on E : Exception do begin
   Named.SetFaultA(MissmatchText,Format('%s Exception %s',[MissmatchText,E.Message]), FaultWarning, nil);
   end;
   end;

end;
*)

procedure TOCTFormDlg.LazyUpdate(Sender : TObject);
begin
  LazyUpdateFlag := True;
end;



procedure TOCTFormDlg.UpdateToolingDisplay;
begin
// Here for 'LAZY' callback on any of the eventds which affect tool life etc
RToolDisplay.UpdateDisplay;
UpdateStnButtons;
end;

procedure TOCTFormDlg.ZeroToolLifeForToolInLocation(Location : Integer);
var
ToolNumber : Integer;
Found : Boolean;
begin
ToolNumber := 0;
Found := False;
ToolData.Lock;
try
// First find what is at location
while (ToolData.Count > ToolNumber) and not Found do
  begin
  if ToolData[ToolNumber].Ex1 = Location then
      begin
      Found := True;
      ToolData[ToolNumber].RemainingBarrels := 0;
      ToolData[ToolNumber].ActToolLength := ToolData[ToolNumber].MinToolLength;
      end;
  inc(ToolNumber);
  end;
finally
ToolData.Unlock;
end;
end;

function TOCTFormDlg.BuildToolInMachineStatus(ToolLocation : Integer):Boolean;
var
ToolTYpe : String;
Serial : String;
ToolNumber : Integer;
NoLongerRequired  : Boolean;
NeverRequired     : Boolean;
Blocking          : Boolean;
Found             : Boolean;
Spent             : Boolean;
LocationEmpty     : Boolean;
ToolDetails       : TToolData;
StatusOnly        : Boolean;
LBuffer : String;

begin

REsult := False;
Blocking := False;
NoLongerRequired := False;
NeverRequired := False;
Spent := False;
LocationEmpty := False;
//StatusOnly := False;

if not assigned(ToolData) then exit;
if not assigned(RequiredToolData) then exit;
RequiredToolData.Lock;
ToolData.Lock;

try
// First determine if the tool group to which the tool belongs
ToolType := '';
ToolNumber := 0;
Found := False;
// First find what is at location
while (ToolData.Count > ToolNumber) and not Found do
  begin
  if ToolData[ToolNumber].Ex1 = ToolLocation then
      begin
      Found := True;
      ToolDetails := ToolData[ToolNumber];
      ToolType := UpperCase(ToolDetails.ShuttleID);
      Serial   := IntToStr(ToolDetails.ShuttleSN);
      end;
  inc(ToolNumber);
  end;
StatusOnly := Named.GetASInteger(Tags[ortOCTDisplayMode]) = ord(tldStatusOnly);

// Use the location to determine if a tool exists at the toolstation
if Found then
  begin
  if ToolDetails.Ex1 = 0 then
    begin
    // here for tool in the head
    LBuffer := Format('%7.4f',[ToolDetails.TotalRemaining]);
    Named.SetAsString(Tags[ortTotalToolLifeInHead],PChar(LBuffer));
    LBuffer := Format('%7.4f',[ToolDetails.ActToolLength-ToolDetails.MinToolLength]);
    Named.SetAsString(Tags[ortBarrelToolLifeInHead],PChar(LBuffer));
    end;
  Spent := ToolDetails.TotalRemaining <= 0;
  Found := False;
  ToolNumber := 0;
  // Now look for tool in required tooling list
  while (ToolNumber < RequiredToolData.Count) and (not Found) do
      begin
      try
      try
      if UpperCase(RequiredToolData.Tool[ToolNumber].ToolName) = ToolTYpe then
          begin
          Found := True;
          if RequiredToolData.Tool[ToolNumber].Priority = 3 then
            begin
            NoLongerRequired := True;
            end;
          end;
      except
      Found := True;
      Result := True;
      end;
      finally
      inc(ToolNumber);
      end;
      end; // while
   NeverRequired := not Found;
   // if the location is occupied is the tool required or can it be removed or MUST it be removed?
   if NoLongerRequired or NeverRequired then
       begin
       // here if the tool group for this tool is done
       if ToolData.Count > 3 then // all stations occupied
           begin
           // ToolMust be removed if there are still tools with priority 1 in the required tools list
           If RequiredToolData.ToolsAreRequired then
               begin
               Blocking := True;
               end
           end
        end
   end
else
    begin
    LocationEmpty := True;
    end;
// set default
MachineTools[ToolLocation].State := tdsOK;
MachineTools[ToolLocation].BString := '';

if LocationEmpty then
  begin
  MachineTools[ToolLocation].Name := '';
  if StatusOnly then
      begin
      MachineTools[ToolLocation].State := tdsNoTool;
      MachineTools[ToolLocation].BString := EMPTYButtonText;
      exit;
      end;
  if (RequiredToolData.ToolsAreRequired) and (ToolLocation <> 0) and (ToolData.Count <ToolPosCount) then
      begin
      MachineTools[ToolLocation].State := tdsAddTool;
      MachineTools[ToolLocation].BString := AddText;
      end
  else
      begin
      MachineTools[ToolLocation].State := tdsNoTool;
      MachineTools[ToolLocation].BString := EMPTYButtonText;
      end;
  end
else
  begin
  MachineTools[ToolLocation].Name := ToolType;
  MachineTools[ToolLocation].Serial := Serial;
  if ToolDetails.BarrelCount > 1 then
    begin
    MachineTools[ToolLocation].Remaining := Format('%4.1f  /  %4.1f',[ToolDetails.TotalRemaining,ToolDetails.BarrelRemaining]);
    end
  else
    begin
    MachineTools[ToolLocation].Remaining := Format('%4.1f',[ToolDetails.TotalRemaining]);
    end;
  if StatusOnly then
      begin
      MachineTools[ToolLocation].State := tdsStatusOnly;
      MachineTools[ToolLocation].BString := '';
      exit;
      end;
  if NeverRequired then
      begin
      MachineTools[ToolLocation].State := tdsNeverRequired;
      if ToolLocation <> 0 then
        MachineTools[ToolLocation].BString := RemoveText
      else
        MachineTools[ToolLocation].BString := 'NOT REQUIRED';
      end;
  if NoLongerRequired then
      begin
      If RequiredToolData.Count > 0 then
          begin
          MachineTools[ToolLocation].State := tdsOK;
          MachineTools[ToolLocation].BString := '';
          end
      else
          begin
          // should stay on for next run
          MachineTools[ToolLocation].State := tdsCanRemove;
          if ToolLocation <> 0 then
              MachineTools[ToolLocation].BString := RemoveText
          else
              MachineTools[ToolLocation].BString := 'NOT REQUIRED';
          end;
      end;
  if Spent then
      begin
      MachineTools[ToolLocation].State := tdsCanRemove;
          if ToolLocation <> 0 then
              MachineTools[ToolLocation].BString := RemoveText
          else
              MachineTools[ToolLocation].BString := 'SPENT';
      end;
  if Blocking then
      begin
      // this overrides never required
      MachineTools[ToolLocation].State := tdsMustRemove;

      if ToolLocation <> 0 then
              MachineTools[ToolLocation].BString := RemoveText
      else
              MachineTools[ToolLocation].BString := 'NOT REQUIRED';
      end;
  end;

finally
RequiredToolData.Unlock;
ToolData.Unlock;
end;




end;

procedure TOCTFormDlg.MakePHistEndRecord(OPPData :TOPP;Abandoned : Boolean);
var
Buffer  : array [0..256] of Char;
SigWarn,SigFail : Boolean;
Interrupted : Boolean;
RecoveryStr : String;
OCtName,OPPName : String;
begin
Named.GetAsString(Tags[ortLoadedOCTName], Buffer, 254);
OctName := Buffer;
Named.GetAsString(Tags[ortLoadedOPPName], Buffer, 254);
OPPName := ExtractFilename(Buffer);
SigWarn := Named.GetAsBoolean(Tags[ortSigWarn]);
SigFail := Named.GetAsBoolean(Tags[ortSigFail]);
RecoveryStr := OPPData.ReworkStatus;
if Pos('R',RecoveryStr) > 0 then Interrupted := True else Interrupted := False;
PHist.MakeRecord(ftEndField,OctName,OppName,Interrupted,Abandoned,SigWarn,SigFail);
end;


procedure TOCTFormDlg.HandleOCTStatusChange(Sender: TObject);
var
ChangedOPP : TOPP;
OPPIndex : Integer;
OctName,OPPName : String;
Buffer  : array [0..256] of Char;
PartID : String;
PartSN : String;

begin
// HERE TO CHECK FOR RECOVERY MODE
OPPIndex := Named.GetAsInteger(Tags[ortOCTIndex]);
  with OCTData do
    begin
    ChangedOPP := OPP[OPPIndex];
    if ChangedOPP.Status <> LastStatus then
      begin
      LastStatus := ChangedOPP.Status;
      OCTDisplay.UpdateOCTDisplayTrigger := True;
      if ChangedOPP.Status = osInRecovery then
        begin
        if not InRecovery then
          begin
          DisplayRecoveyOptions(Self);
          OCTDisplay.RecoveryIndex := OPPIndex;
          end
        end else
      if ChangedOPP.Status = osStarted then
        begin
        // Start of OCT
        if OCTData.SummedDuration < 0.5 then
          begin
          // Register new part if based on Partnametag
          if PHist.Configured then
            begin
            Named.GetAsString(Tags[ortShuttlePartName], Buffer, 254);
            PartID:=Buffer;
            Named.GetAsString(Tags[ortShuttleSerialNumber], Buffer, 254);
            PartSN :=Buffer;
            If (PartID <>'') and (PartSN<>'') then
                begin
                PartID := Format('%s_%s',[PartID,PartSN]);
                PHist.RegisterPart(PartID);
                end;
            end;
          end;

        Named.GetAsString(Tags[ortLoadedOCTName], Buffer, 254);
        OctName := Buffer;
        Named.GetAsString(Tags[ortLoadedOPPName], Buffer, 254);
        OPPName := ExtractFileName(Buffer);
        if PHist.Configured then
          begin
          PHist.MakeRecord(ftStartField,OctName,OppName,False,False,False,False);
          end;
        end else
    if ChangedOPP.Status = osComplete then
        begin
        if PHist.Configured then
          begin
          MakePHistEndRecord(ChangedOPP,False);
          end;
        end else
    if ChangedOPP.Status = osNone then
        begin
        if OCTData.SummedDuration < 0.001 then
          begin
          // Register new part if based on Partnametag
          if PHist.Configured then
            begin
            Named.GetAsString(Tags[ortShuttlePartName], Buffer, 254);
            PartID:=Buffer;
            Named.GetAsString(Tags[ortShuttleSerialNumber], Buffer, 254);

            PartID := Format('%s_%s',[PartID,Buffer]);
            PHist.RegisterPart(PartID);
            end;
          end;
        end;
    end; // <> last Status
  end;   // With OCTData

end;

procedure TOCTFormDlg.OCTRunningChanged(Running: Boolean);
begin
if Running then
  begin
  OCTDisplay.DisplayResetButtonEnabled := False;
  OCTDisplay.SelectAllButtonEnabled := False;

  if InRecovery then
    begin
    ClearRecoveryOptions();
    end
  else
    begin
    CurrentRecoveryOption := roNone;
    InRecovery := False;
    OCTDisplay.RecoveryIndex := -1;
    end;
  end
else
  begin
  OCTDisplay.DisplayResetButtonEnabled := True;
  OCTDisplay.SelectAllButtonEnabled := True;
  end;

end;

function TOCTFormDlg.SecondsToHMS(Seconds: Integer): string;
var Tmp, Hours, Mins, Secs: Integer;
begin
  Tmp:= Round(OCTData.SummedDuration);
  Hours:= Tmp div 3600;
  Mins:= Tmp mod 3600;
  Secs:= Mins mod 60;
  Mins:= Mins div 60;
{
  Tmp:= Tmp - (Hours * 3600);
  Mins:= Tmp div 60;
  Mins:= Tmp mod 60;
 }
  Result:= Format('%.2d:%.2d:%.2d', [Hours, Mins, Secs]);
end;

function TOCTFormDlg.SecondsToMinutesString(Seconds : Integer):String;
var
Minutes : Integer;
RemSeconds : Integer;
MStr : String;
SecStr : String;
begin
try
Minutes := Seconds div 60;
RemSeconds := Seconds - (Minutes*60);
if Minutes < 10 then MStr := '0'+IntToStr(Minutes) else MStr := IntToStr(Minutes);
if  RemSeconds < 10 then SecStr := '0'+IntToStr(RemSeconds)else SecStr := IntToStr(RemSeconds);
Result := Format('%s:%s',[MStr,SecStr]);
except
Result := '00:00';
end;
end;

procedure TOCTFormDlg.OCTCellUpdate(Sender: TObject; Row : Integer; Field : WideString);
begin
OCTDisplay.CellUPdate(Row,Field);
end;


procedure TOCTFormDlg.FormShow(Sender: TObject);
begin
Top := 70;
Left :=1;
end;



initialization
  TAbstractFormPlugin.AddPlugin('OperatorDialog', TOCTFormDlg);
end.
