unit SyntaxHighProgDisplay;

interface
uses
  Windows,Classes,Controls,IniFiles,FileCtrl, ACNC_plusmemo,ACNC_plusGutter,
  SysUtils,INamedInterface,extctrls,Graphics,StdCtrls;

//TScrollStyle

Type
TSyntaxHighProgDisplay = class(TCustomPanel)
  private
  PDisplay : TPlusMemo;
  FCurrentLine: Integer;
  PGutter  : TPlusGutter;
  LineHighLighted : Boolean;
  FCLCentre: Boolean;
  LinesToCentre : Integer;

  procedure SetCurrentLine(const Value: Integer);
  procedure SetText(const Value: WideString);
  procedure SetCLCentre(const Value: Boolean);
  function GetLineCount: LongInt;
  procedure SetBackBitmap(const Value: TPicture);

  protected
  public
  keywords : TStringList;

  constructor Create(AOwner: TComponent); override;
  destructor Destroy; override;


  procedure readConfigFile(Filename : String);
  procedure LoadProgramText(ProgramString : String);

  property TextString : WideString write SetText;
  property CurrentLine : Integer read FCurrentLine write SetCurrentLine;
//  property Memo :TPlusMemo read PDisplay write PDisplay;
  property CurrentLineInCentre : Boolean read FCLCentre write SetCLCentre;
  property LineCount : LongInt read GetLineCount;
  property BackBitmap : TPicture write SetBackBitmap;

end;

implementation
//*********************************************
// function GetToken
// Result is next commaseparatred string
// var Buffer contains remainder of input string
//*********************************************
function GetToken(var Buffer : String):String;
var
CommaPos : Integer;
begin
Commapos := POs(',',Buffer);
if Commapos > 0 then
   begin
   Result := Copy(Buffer,1,CommaPos-1);
   Buffer := Copy(Buffer,CommaPos+1,Length(Buffer));
   end
else
    begin
    Result := Buffer;
    Buffer := '';
    end;
end;
//*********************************************
// function CSVField(
// Result is then Commaseparated field specified by
// FNumber in input Line TextString
//*********************************************
function CSVField(FNumber: Integer;TextString : String):String;
var
Field      : Integer;
begin
Field := FNumber;
while Field >= 0 do
      begin
      Result :=GetToken(TextString);
      dec(Field);
      end;
end;




{ TSyntaxHighProgDisplay }

constructor TSyntaxHighProgDisplay.Create(AOwner: TComponent);
begin
  inherited;
  PDisplay := TPlusMemo.Create(Self);
  with PDisplay do
       begin
       Parent := Self;
       align := alClient;
       HighlightBackgnd := clNavy;//$f0f0d0;//clSilver+$00333311;
//       HighlightBackgnd := $00333311;
       HighlightColor := clWhite;
       ScrollBars := ssNone;
       ReadOnly := True;
       //OnFindModeRequest := FindModeRequested;
       DisplayOnly := True;
       end;
  PGutter := TPlusGutter.Create(Self);
  with PGutter do
       begin
       Parent := Self;
       Pgutter.PlusMemo := PDisplay;
       align := alLeft;
       width := 44;
       end;
  LineHighLighted := False;
end;

destructor TSyntaxHighProgDisplay.Destroy;
begin
  inherited;
end;


procedure TSyntaxHighProgDisplay.SetCurrentLine(const Value: Integer);
begin
  with PDisplay do
       begin
       if LineHighLighted then
          begin
          // clear highlighting of current line first
          SelLine := FCurrentLine;
          SelCol := 0;
          SelLength := Length(Lines[FCurrentLine]);
          SetHighlight;
          SelLength := 0;
          end;

       FCurrentLine := Value;
       if FCLCentre then
          begin
          if (FCurrentLine > LinesToCentre) then
             begin
             FirstVisibleLine := FCurrentLine-LinesToCentre;
             end
          else
             begin
             FirstVisibleLine := 0;
             end
          end;
       SelLine := FCurrentLine;
       SelCol := 0;
       SelLength := Length(Lines[FCurrentLine]);
       SetHighlight;
       SelLength := 0;
       LineHighLighted := True;
       end;


end;

procedure TSyntaxHighProgDisplay.SetText(const Value: WideString);
begin

end;

procedure TSyntaxHighProgDisplay.ReadConfigFile(Filename : String);
var
ConfigFile :TIniFile;
wordList  : TStringList;
keywordcolour : LongInt;
commentcolour : LongInt;
FontColour    : Longint;
BackGroundColour : Longint;
CommentStart  : String;
C3Start       : String;
Buffer        : String;
WordNumber    : Integer;
FontStyle     : TExtFontStyles;
DelimeterSet  : Set of Char;
CharCount     : Integer;
begin
//FileName := DLLProfilePath+'OPPEditor.cfg';
if FileExists(FileName) then
   begin
   Wordlist := TStringList.Create;
   try
   ConfigFile := TInifile.Create(Filename);
   try
   with ConfigFile do
        begin
        // Set default font
        Buffer := ReadString('colours','backgroundColour','clwhite');
        if not IdentToColor(Buffer,BackGroundColour) then
           begin
           BackGroundColour := clwhite;
           end;
        //PDisplay.Color := BackGroundColour;

        Buffer := ReadString('colours','normalfontname','Verdana');
        PDisplay.Font.Name := Buffer;
        PDisplay.Font.Size := ReadInteger('colours','normalfontsize',12);
        Buffer := ReadString('colours','normalfontcolour','clblack,bold');
        if not IdentToColor(CSVField(0,Buffer),FontColour) then
           begin
           FontColour := clBlack;
           end;
        PDisplay.Font.Color := FontColour;
        FontStyle := [];
        if CSVField(1,Buffer)='bold' then include(FontStyle,fsbold);
        PDisplay.Font.Style := FontStyle;

        Buffer := ReadString('delimeters','Characters','$%&,./:;<=>(');
        DelimeterSet := [];
        include(DelimeterSet,Char(' '));
        For CharCount := 1 to Length(Buffer) do
            begin
            include(DelimeterSet,Char(Buffer[CharCount]));
            end;

        PDisplay.Delimiters := Delimeterset;


        // Add all keywords to list
        ReadSection('keywords',Wordlist);
        if Wordlist.Count > 0 then
           begin
           PDisplay.Keywords.BeginUpdate;
           For WordNumber := 0 to Wordlist.Count-1 do
               begin
               Buffer := ReadString('colours','keyword','clmaroon,bold');
               IdentToColor(CSVField(0,Buffer),keywordcolour);
               FontStyle := [];
               if CSVField(1,Buffer)='bold' then include(FontStyle,fsbold);
               if CSVField(2,Buffer)='italics' then include(FontStyle,fsitalic);
               PDisplay.Keywords.AddKeyWord(WordList[WordNumber],[woWholeWordsOnly],FontStyle,1,crDefault,
                                       BackGroundColour,keywordcolour);
               end;
           PDisplay.ReApplyKeywords;
           PDisplay.ApplyKeyWords := True;
           PDisplay.Keywords.EndUpdate;
           end;

        // Now add comment identifiers
        Buffer := ReadString('colours','comment','clblue,bold,italic');
        if not IdentToColor(CSVField(0,Buffer),commentcolour) then
           begin
           commentColour := clBlack;
           end;
        FontStyle := [];
        if CSVField(1,Buffer)='bold' then include(FontStyle,fsbold);
        if CSVField(2,Buffer)='italics' then include(FontStyle,fsitalic);
        CommentStart := ReadString('comments','starttext1','');
        if  CommentStart <> '' then
            begin
            PDisplay.StartStopKeys.AddStartStopKey(CommentStart,'',[],FontStyle,1,crDefault,BackGroundColour,CommentColour,True);
            end;

        // Now add C3 colouring
        Buffer := ReadString('colours','C3Calls','clAqua,bold,italic');
        if not IdentToColor(CSVField(0,Buffer),commentcolour) then
           begin
           CommentColour := clBlack;
           end;
        FontStyle := [];
        if CSVField(1,Buffer)='bold' then include(FontStyle,fsbold);
        if CSVField(2,Buffer)='italics' then include(FontStyle,fsitalic);
        C3Start := ReadString('C3Calls','StartText','');
        if  CommentStart <> '' then
            begin
            PDisplay.StartStopKeys.AddStartStopKey(C3Start,' ',[],FontStyle,1,crDefault,BackGroundColour,CommentColour,True);
            end;

        // Now add Label start character id
        Buffer := ReadString('colours','label','clgreen,bold');
        if not IdentToColor(CSVField(0,Buffer),commentcolour) then
           begin
           commentColour := clGreen;
           end;
        FontStyle := [];
        if CSVField(1,Buffer)='bold' then include(FontStyle,fsbold);
        if CSVField(2,Buffer)='italics' then include(FontStyle,fsitalic);
        CommentStart := ReadString('labels','startchar1','');
        if  CommentStart <> '' then
            begin
            PDisplay.StartStopKeys.AddStartStopKey(CommentStart,' ',[],FontStyle,1,crDefault,BackGroundColour,CommentColour,True);
            end;
        CommentStart := ReadString('labels','startchar2','');
        if  CommentStart <> '' then
            begin
            PDisplay.StartStopKeys.AddStartStopKey(CommentStart,' ',[],FontStyle,1,crDefault,BackGroundColour,CommentColour,True);
            end;
            end;
   finally
   ConfigFile.Free;
   end;
   finally
   WordList.Free;
   end;
   end;
end;




procedure TSyntaxHighProgDisplay.LoadProgramText(ProgramString: String);
begin
PDisplay.Lines.Text := ProgramString;
SetCLCentre(FCLCentre);
LineHighLighted := False;
end;

procedure TSyntaxHighProgDisplay.SetCLCentre(const Value: Boolean);
begin
  FCLCentre := Value;
  if PDisplay.LineHeight > 0 then
     begin
     LinesToCentre := (Self.Height div PDisplay.LineHeight) div 2;
     end
  else
     begin
     LinesToCentre := 1;
     end;
end;

function TSyntaxHighProgDisplay.GetLineCount: LongInt;
begin
Result := PDisplay.LineCount;
end;

procedure TSyntaxHighProgDisplay.SetBackBitmap(const Value: TPicture);
begin
PDisplay.BackgroundBmp.Assign(Value);
end;

end.
