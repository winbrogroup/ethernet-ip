object OCTFormDlg: TOCTFormDlg
  Left = -24
  Top = 70
  BorderStyle = bsNone
  ClientHeight = 715
  ClientWidth = 1016
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -19
  Font.Name = 'Verdana'
  Font.Style = []
  OldCreateOrder = False
  Position = poDefault
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 23
  object BottomPanel: TPanel
    Left = 0
    Top = 0
    Width = 746
    Height = 715
    Align = alClient
    TabOrder = 0
    object BaseOCTPanel: TPanel
      Left = 1
      Top = 41
      Width = 744
      Height = 376
      Align = alTop
      Caption = 'BaseOCTPanel'
      TabOrder = 0
      OnResize = BaseOCTPanelResize
      object RecoveryPanel: TPanel
        Left = 1
        Top = 1
        Width = 742
        Height = 374
        Align = alTop
        BevelInner = bvLowered
        TabOrder = 0
        object RecoveryBanner: TPanel
          Left = 2
          Top = 2
          Width = 738
          Height = 41
          Align = alTop
          BevelInner = bvLowered
          Caption = 'OCT Recovery Options'
          Color = clTeal
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -19
          Font.Name = 'Verdana'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
        end
        object Panel4: TPanel
          Left = 2
          Top = 98
          Width = 373
          Height = 8
          BevelInner = bvLowered
          TabOrder = 1
        end
        object Panel6: TPanel
          Left = 2
          Top = 89
          Width = 738
          Height = 8
          Align = alTop
          BevelInner = bvLowered
          TabOrder = 2
        end
        object Panel8: TPanel
          Left = 2
          Top = 43
          Width = 373
          Height = 8
          BevelInner = bvLowered
          TabOrder = 3
        end
        object RestartPanel: TPanel
          Left = 2
          Top = 43
          Width = 738
          Height = 46
          Align = alTop
          BevelInner = bvLowered
          Caption = 'Restart Current OPP'
          Color = clInfoBk
          TabOrder = 4
          object RestartCurrentButton: TTouchSoftkey
            Left = 2
            Top = 2
            Width = 122
            Height = 42
            Align = alLeft
            Caption = 'TouchSoftkey1'
            TabOrder = 0
            OnMouseUp = RestartCurrentButtonMouseUp
            TextSize = 14
            HasIndicator = False
            IndicatorOn = False
            IsEnabled = True
            ButtonStyle = skNormal
            ArrowColor = clBlack
            ButtonColor = clWhite
            LegendColor = clBlack
            IndicatorWidth = 20
            IndicatorHeight = 10
            IndicatorOnColor = clGreen
            IndicatorOffColor = clSilver
            MaxFontSize = 0
          end
        end
        object Panel10: TPanel
          Left = 2
          Top = 143
          Width = 738
          Height = 8
          Align = alTop
          BevelInner = bvLowered
          TabOrder = 5
        end
        object SkipPanel: TPanel
          Left = 2
          Top = 97
          Width = 738
          Height = 46
          Align = alTop
          BevelInner = bvLowered
          Caption = '     Skip To Next OPP'
          Color = clInfoBk
          TabOrder = 6
          object SkipButton: TTouchSoftkey
            Left = 2
            Top = 2
            Width = 122
            Height = 42
            Align = alLeft
            Caption = 'TouchSoftkey1'
            TabOrder = 0
            OnMouseUp = SkipButtonMouseUp
            TextSize = 14
            HasIndicator = False
            IndicatorOn = False
            IsEnabled = True
            ButtonStyle = skNormal
            ArrowColor = clBlack
            ButtonColor = clWhite
            LegendColor = clBlack
            IndicatorWidth = 20
            IndicatorHeight = 10
            IndicatorOnColor = clGreen
            IndicatorOffColor = clSilver
            MaxFontSize = 0
          end
        end
        object Panel9: TPanel
          Left = 2
          Top = 197
          Width = 738
          Height = 8
          Align = alTop
          BevelInner = bvLowered
          TabOrder = 7
        end
        object AbandonPartPanel: TPanel
          Left = 2
          Top = 205
          Width = 738
          Height = 46
          Align = alTop
          BevelInner = bvLowered
          Caption = 'Mark part as non conformance'
          Color = clInfoBk
          TabOrder = 8
          Visible = False
          object AbandonPartButton: TTouchSoftkey
            Left = 2
            Top = 2
            Width = 122
            Height = 42
            Align = alLeft
            Caption = 'TouchSoftkey1'
            TabOrder = 0
            OnMouseUp = AbandonPartButtonMouseUp
            TextSize = 14
            HasIndicator = False
            IndicatorOn = False
            IsEnabled = True
            ButtonStyle = skNormal
            ArrowColor = clBlack
            ButtonColor = clWhite
            LegendColor = clBlack
            IndicatorWidth = 20
            IndicatorHeight = 10
            IndicatorOnColor = clGreen
            IndicatorOffColor = clSilver
            MaxFontSize = 0
          end
        end
        object Panel11: TPanel
          Left = 2
          Top = 251
          Width = 738
          Height = 8
          Align = alTop
          BevelInner = bvLowered
          TabOrder = 9
          object Panel2: TPanel
            Left = 304
            Top = 8
            Width = 185
            Height = 41
            Caption = 'Panel1'
            TabOrder = 0
          end
        end
        object AbandonOCTPanel: TPanel
          Left = 2
          Top = 151
          Width = 738
          Height = 46
          Align = alTop
          BevelInner = bvLowered
          Caption = '     Abandon Entire OCT'
          Color = clInfoBk
          TabOrder = 10
          object AbandonOCTButton: TTouchSoftkey
            Left = 2
            Top = 2
            Width = 122
            Height = 42
            Align = alLeft
            Caption = 'AbandonOCTButton'
            TabOrder = 0
            OnMouseUp = AbandonOCTButtonMouseUp
            TextSize = 14
            HasIndicator = False
            IndicatorOn = False
            IsEnabled = True
            ButtonStyle = skNormal
            ArrowColor = clBlack
            ButtonColor = clWhite
            LegendColor = clBlack
            IndicatorWidth = 20
            IndicatorHeight = 10
            IndicatorOnColor = clGreen
            IndicatorOffColor = clSilver
            MaxFontSize = 0
          end
        end
        object ReworkPanel: TPanel
          Left = 2
          Top = 259
          Width = 738
          Height = 41
          Align = alTop
          Caption = 'Rework current OPP'
          Color = clInfoBk
          TabOrder = 11
          Visible = False
          object ReworkButton: TTouchSoftkey
            Left = 0
            Top = 0
            Width = 122
            Height = 41
            Caption = 'ReworkButton'
            TabOrder = 0
            TextSize = 14
            HasIndicator = False
            IndicatorOn = False
            IsEnabled = True
            ButtonStyle = skNormal
            ArrowColor = clBlack
            ButtonColor = clWhite
            LegendColor = clBlack
            IndicatorWidth = 20
            IndicatorHeight = 10
            IndicatorOnColor = clGreen
            IndicatorOffColor = clSilver
            MaxFontSize = 0
          end
        end
        object Panel7: TPanel
          Left = 2
          Top = 308
          Width = 738
          Height = 64
          Align = alClient
          BevelInner = bvLowered
          TabOrder = 12
          object Panel12: TPanel
            Left = 2
            Top = 2
            Width = 634
            Height = 60
            Align = alClient
            Alignment = taLeftJustify
            Color = clInfoBk
            Font.Charset = ANSI_CHARSET
            Font.Color = clNavy
            Font.Height = -16
            Font.Name = 'Comic Sans MS'
            Font.Style = []
            ParentFont = False
            TabOrder = 0
            OnResize = Panel12Resize
            object MessageLabel: TLabel
              Left = 1
              Top = 1
              Width = 632
              Height = 32
              Align = alTop
              AutoSize = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clNavy
              Font.Height = -16
              Font.Name = 'Verdana'
              Font.Style = []
              ParentFont = False
              WordWrap = True
            end
            object MessageLabel2: TLabel
              Left = 1
              Top = 29
              Width = 632
              Height = 30
              Align = alBottom
              AutoSize = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clNavy
              Font.Height = -16
              Font.Name = 'Verdana'
              Font.Style = []
              ParentFont = False
              WordWrap = True
            end
          end
          object AcceptKey: TTouchSoftkey
            Left = 636
            Top = 2
            Width = 100
            Height = 60
            Align = alRight
            Caption = 'TouchSoftkey1'
            TabOrder = 1
            OnMouseUp = AcceptKeyMouseUp
            Legend = 'ACCEPT'
            TextSize = 14
            HasIndicator = False
            IndicatorOn = False
            IsEnabled = True
            ButtonStyle = skNormal
            ArrowColor = clBlack
            ButtonColor = clLime
            LegendColor = clMaroon
            IndicatorWidth = 20
            IndicatorHeight = 10
            IndicatorOnColor = clGreen
            IndicatorOffColor = clSilver
            MaxFontSize = 0
          end
        end
        object Panel13: TPanel
          Left = 2
          Top = 300
          Width = 738
          Height = 8
          Align = alTop
          BevelInner = bvLowered
          TabOrder = 13
        end
      end
    end
    object DisplayOptionsPanel: TPanel
      Left = 1
      Top = 660
      Width = 744
      Height = 54
      Align = alBottom
      BevelInner = bvLowered
      BorderWidth = 1
      TabOrder = 1
      object BtnDefaultDisplay: TTouchGlyphSoftkey
        Left = 4
        Top = 4
        Width = 122
        Height = 47
        Legend = '    Default   '
        TextSize = 0
        HasIndicator = False
        IndicatorOn = False
        IsEnabled = True
        ButtonColor = clInfoBk
        Layout = slGlyphLeft
        AutoTextSize = True
        KeyStyle = ssRectangle
        CornerRad = 10
        ButtonTravel = 2
        ShadowColor = clGray
        LegendFont.Charset = ANSI_CHARSET
        LegendFont.Color = clBlack
        LegendFont.Height = 20
        LegendFont.Name = 'Verdana'
        LegendFont.Style = []
        GlyphToEdgeSpacing = 1
        TextToEdgeBorder = 5
        MultiLineSpacing = 0
        MaxFontSize = 20
        OnMouseUp = BtnDefaultDisplayMouseUp
      end
      object BtnShowToolTable: TTouchGlyphSoftkey
        Left = 128
        Top = 4
        Width = 120
        Height = 47
        Legend = 'Show~Part Program'
        TextSize = 0
        HasIndicator = False
        IndicatorOn = False
        IsEnabled = True
        ButtonColor = clInfoBk
        Layout = slGlyphLeft
        AutoTextSize = True
        KeyStyle = ssRectangle
        CornerRad = 10
        ButtonTravel = 2
        ShadowColor = clGray
        LegendFont.Charset = ANSI_CHARSET
        LegendFont.Color = clBlack
        LegendFont.Height = 18
        LegendFont.Name = 'Verdana'
        LegendFont.Style = []
        GlyphToEdgeSpacing = 1
        TextToEdgeBorder = 5
        MultiLineSpacing = 0
        MaxFontSize = 20
        OnMouseUp = BtnShowToolTableMouseUp
      end
      object BtnShowOPPOnly: TTouchGlyphSoftkey
        Left = 250
        Top = 4
        Width = 120
        Height = 47
        Legend = 'Show OPP~Table Only'
        TextSize = 0
        HasIndicator = False
        IndicatorOn = False
        IsEnabled = True
        ButtonColor = clInfoBk
        Layout = slGlyphLeft
        AutoTextSize = True
        KeyStyle = ssRectangle
        CornerRad = 10
        ButtonTravel = 2
        ShadowColor = clGray
        LegendFont.Charset = ANSI_CHARSET
        LegendFont.Color = clBlack
        LegendFont.Height = 18
        LegendFont.Name = 'Verdana'
        LegendFont.Style = []
        GlyphToEdgeSpacing = 1
        TextToEdgeBorder = 5
        MultiLineSpacing = 0
        MaxFontSize = 20
        OnMouseUp = BtnShowOPPOnlyMouseUp
      end
      object BtnClose: TTouchGlyphSoftkey
        Left = 616
        Top = 4
        Width = 122
        Height = 47
        Legend = '   Close    '
        TextSize = 0
        HasIndicator = False
        IndicatorOn = False
        IsEnabled = True
        ButtonColor = clInfoBk
        Layout = slGlyphLeft
        AutoTextSize = True
        KeyStyle = ssRectangle
        CornerRad = 10
        ButtonTravel = 2
        ShadowColor = clGray
        LegendFont.Charset = ANSI_CHARSET
        LegendFont.Color = clRed
        LegendFont.Height = 20
        LegendFont.Name = 'Verdana'
        LegendFont.Style = []
        GlyphToEdgeSpacing = 1
        TextToEdgeBorder = 5
        MultiLineSpacing = 0
        MaxFontSize = 20
        OnMouseDown = BtnCloseMouseDown
      end
      object BtnSplitUp: TTouchGlyphSoftkey
        Left = 372
        Top = 4
        Width = 120
        Height = 47
        Legend = 'Move Split~UP'
        TextSize = 0
        HasIndicator = False
        IndicatorOn = False
        IsEnabled = True
        ButtonColor = clInfoBk
        Glyph.Data = {
          36080000424D3608000000000000360400002800000020000000200000000100
          0800000000000004000000000000000000000001000000010000000000003300
          00006600000099000000CC000000FF0000000000330033003300660033009900
          3300CC003300FF00330000006600330066006600660099006600CC006600FF00
          660000009900330099006600990099009900CC009900FF0099000000CC003300
          CC006600CC009900CC00CC00CC00FF00CC000000FF003300FF006600FF009900
          FF00CC00FF00FF00FF0000330000333300006633000099330000CC330000FF33
          000000333300333333006633330099333300CC333300FF333300003366003333
          66006633660099336600CC336600FF3366000033990033339900663399009933
          9900CC339900FF3399000033CC003333CC006633CC009933CC00CC33CC00FF33
          CC000033FF003333FF006633FF009933FF00CC33FF00FF33FF00006600003366
          00006666000099660000CC660000FF6600000066330033663300666633009966
          3300CC663300FF66330000666600336666006666660099666600CC666600FF66
          660000669900336699006666990099669900CC669900FF6699000066CC003366
          CC006666CC009966CC00CC66CC00FF66CC000066FF003366FF006666FF009966
          FF00CC66FF00FF66FF0000990000339900006699000099990000CC990000FF99
          000000993300339933006699330099993300CC993300FF993300009966003399
          66006699660099996600CC996600FF9966000099990033999900669999009999
          9900CC999900FF9999000099CC003399CC006699CC009999CC00CC99CC00FF99
          CC000099FF003399FF006699FF009999FF00CC99FF00FF99FF0000CC000033CC
          000066CC000099CC0000CCCC0000FFCC000000CC330033CC330066CC330099CC
          3300CCCC3300FFCC330000CC660033CC660066CC660099CC6600CCCC6600FFCC
          660000CC990033CC990066CC990099CC9900CCCC9900FFCC990000CCCC0033CC
          CC0066CCCC0099CCCC00CCCCCC00FFCCCC0000CCFF0033CCFF0066CCFF0099CC
          FF00CCCCFF00FFCCFF0000FF000033FF000066FF000099FF0000CCFF0000FFFF
          000000FF330033FF330066FF330099FF3300CCFF3300FFFF330000FF660033FF
          660066FF660099FF6600CCFF6600FFFF660000FF990033FF990066FF990099FF
          9900CCFF9900FFFF990000FFCC0033FFCC0066FFCC0099FFCC00CCFFCC00FFFF
          CC0000FFFF0033FFFF0066FFFF0099FFFF00CCFFFF00FFFFFF00000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000808080808080
          8080808080808080808080808080808080808080808080808080808080808080
          8080808080808080808080808080808080808080808080808080800000000000
          00000000000000000000000000000000008080808080808080808000D7D7D7D7
          D7D7D7D7D7D7D7D7D7D7D7D7D7D7D7D7008080808080808080808000D7D7D7D7
          D7D7D7D7D7D7D7D7D7D7D7D7D7D7D7D700808080808080808080800000000000
          D7D7000000D7D700000000D7D7D7D7D7008080808080808080808000D7D7D7D7
          D7D7D7D7D7D7D7D7D7D7D7D7D7D7D7D70080808080808080808080000C0C0CD7
          000000D70C0C0C0CD70000D7D7D7D7D7008080808080808080808000D7D7D7D7
          D7D7D7D7D7D7D7D7D7D7D7D7D7D7D7D70080808080808080808080000B0B0BD7
          0B0B0B0B0B0B0B0B0BD70B0B0BD7D7D7008080808080808080808000D7D7D7D7
          D7D7D7D7D7D7D7D7D7D7D7D7D7D7D7D7008080808080808080808000D7000000
          D7000000D700000000D700000000D7D7008080808080808080808000D7D7D7D7
          D7D7D7D7D7D7D7D7D7D7D7D7D7D7D7D700808080808080808080800096969696
          96D7D7D7D7D7D7D7D7D7D7D7D7D7D7D7008080808080808080808000D7D7D7D7
          D7D7D7D7D7D7D7D7D7D7D7D7D7D7D7D700808080808080808080800000000000
          0000000000000000000000000000000000800000000000000080800000000000
          0000000000000000000000000000000000808080801E80808080800080808080
          8000808080808080808080808080808000808080801E80808080800080808080
          8000808080808080808080808080808000808080801E80808080800080808080
          8000808080808080808080808080808000808080801E80808080800000000000
          0000000000000000000000000000000000808080801E80808080800080808080
          8000808080808080808080808080808000808080801E80808080800080808080
          8000808080808080808080808080808000808080801E80808080800000000000
          0000000000000000000000000000000000808080801E80808080800080808080
          8000808080808080808080808080808000808080801E80808080800080808080
          8000808080808080808080808080808000801E1E1E1E1E1E1E80800000000000
          000000000000000000000000000000000080801E1E1E1E1E8080800080808080
          80008080808080808080808080808080008080801E1E1E808080800080808080
          8000808080808080808080808080808000808080801E80808080800000000000
          0000000000000000000000000000000000808080808080808080808080808080
          8080808080808080808080808080808080808080808080808080808080808080
          8080808080808080808080808080808080808080808080808080}
        Layout = slGlyphLeft
        AutoTextSize = True
        KeyStyle = ssRectangle
        CornerRad = 10
        ButtonTravel = 2
        ShadowColor = clGray
        LegendFont.Charset = ANSI_CHARSET
        LegendFont.Color = clBlack
        LegendFont.Height = 17
        LegendFont.Name = 'Verdana'
        LegendFont.Style = []
        GlyphToEdgeSpacing = 4
        TextToEdgeBorder = 6
        MultiLineSpacing = 0
        MaxFontSize = 20
        OnMouseUp = BtnSplitUpMouseUp
      end
      object BtnSplitDown: TTouchGlyphSoftkey
        Left = 494
        Top = 4
        Width = 120
        Height = 47
        Legend = 'Move Split~Down'
        TextSize = 0
        HasIndicator = False
        IndicatorOn = False
        IsEnabled = True
        ButtonColor = clInfoBk
        Glyph.Data = {
          36080000424D3608000000000000360400002800000020000000200000000100
          0800000000000004000000000000000000000001000000010000000000003300
          00006600000099000000CC000000FF0000000000330033003300660033009900
          3300CC003300FF00330000006600330066006600660099006600CC006600FF00
          660000009900330099006600990099009900CC009900FF0099000000CC003300
          CC006600CC009900CC00CC00CC00FF00CC000000FF003300FF006600FF009900
          FF00CC00FF00FF00FF0000330000333300006633000099330000CC330000FF33
          000000333300333333006633330099333300CC333300FF333300003366003333
          66006633660099336600CC336600FF3366000033990033339900663399009933
          9900CC339900FF3399000033CC003333CC006633CC009933CC00CC33CC00FF33
          CC000033FF003333FF006633FF009933FF00CC33FF00FF33FF00006600003366
          00006666000099660000CC660000FF6600000066330033663300666633009966
          3300CC663300FF66330000666600336666006666660099666600CC666600FF66
          660000669900336699006666990099669900CC669900FF6699000066CC003366
          CC006666CC009966CC00CC66CC00FF66CC000066FF003366FF006666FF009966
          FF00CC66FF00FF66FF0000990000339900006699000099990000CC990000FF99
          000000993300339933006699330099993300CC993300FF993300009966003399
          66006699660099996600CC996600FF9966000099990033999900669999009999
          9900CC999900FF9999000099CC003399CC006699CC009999CC00CC99CC00FF99
          CC000099FF003399FF006699FF009999FF00CC99FF00FF99FF0000CC000033CC
          000066CC000099CC0000CCCC0000FFCC000000CC330033CC330066CC330099CC
          3300CCCC3300FFCC330000CC660033CC660066CC660099CC6600CCCC6600FFCC
          660000CC990033CC990066CC990099CC9900CCCC9900FFCC990000CCCC0033CC
          CC0066CCCC0099CCCC00CCCCCC00FFCCCC0000CCFF0033CCFF0066CCFF0099CC
          FF00CCCCFF00FFCCFF0000FF000033FF000066FF000099FF0000CCFF0000FFFF
          000000FF330033FF330066FF330099FF3300CCFF3300FFFF330000FF660033FF
          660066FF660099FF6600CCFF6600FFFF660000FF990033FF990066FF990099FF
          9900CCFF9900FFFF990000FFCC0033FFCC0066FFCC0099FFCC00CCFFCC00FFFF
          CC0000FFFF0033FFFF0066FFFF0099FFFF00CCFFFF00FFFFFF00000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000808080808080
          8080808080808080808080808080808080808080808080808080808080808080
          8080808080808080808080808080808080808080808080808080800000000000
          0000000000000000000000000000000000808080801E808080808000D7D7D7D7
          D7D7D7D7D7D7D7D7D7D7D7D7D7D7D7D7008080801E1E1E8080808000D7D7D7D7
          D7D7D7D7D7D7D7D7D7D7D7D7D7D7D7D70080801E1E1E1E1E8080800000000000
          D7D7000000D7D700000000D7D7D7D7D700801E1E1E1E1E1E1E808000D7D7D7D7
          D7D7D7D7D7D7D7D7D7D7D7D7D7D7D7D700808080801E8080808080000C0C0CD7
          000000D70C0C0C0CD70000D7D7D7D7D700808080801E808080808000D7D7D7D7
          D7D7D7D7D7D7D7D7D7D7D7D7D7D7D7D700808080801E8080808080000B0B0BD7
          0B0B0B0B0B0B0B0B0BD70B0B0BD7D7D700808080801E808080808000D7D7D7D7
          D7D7D7D7D7D7D7D7D7D7D7D7D7D7D7D700808080801E808080808000D7000000
          D7000000D700000000D700000000D7D700808080801E808080808000D7D7D7D7
          D7D7D7D7D7D7D7D7D7D7D7D7D7D7D7D700808080801E80808080800096969696
          96D7D7D7D7D7D7D7D7D7D7D7D7D7D7D700808080801E808080808000D7D7D7D7
          D7D7D7D7D7D7D7D7D7D7D7D7D7D7D7D700808080801E80808080800000000000
          0000000000000000000000000000000000800000000000000080800000000000
          00000000000000000000000000000000008080808080808080808000D7D7D7D7
          D700D7D7D7D7D7D7D7D7D7D7D7D7D7D7008080808080808080808000D7D7D7D7
          D700D7D7D7D7D7D7D7D7D7D7D7D7D7D7008080808080808080808000D7D7D7D7
          D700D7D7D7D7D7D7D7D7D7D7D7D7D7D700808080808080808080800000000000
          00000000000000000000000000000000008080808080808080808000D7D7D7D7
          D700D7D7D7D7D7D7D7D7D7D7D7D7D7D7008080808080808080808000D7D7D7D7
          D700D7D7D7D7D7D7D7D7D7D7D7D7D7D700808080808080808080800000000000
          000000000000000000000000000000000080808080808080808080002E2E2E2E
          2E002E2E2E2E2E2E2E2E2E2E2E2E2E2E0080808080808080808080002E2E2E2E
          2E002E2E2E2E2E2E2E2E2E2E2E2E2E2E00808080808080808080800000000000
          00000000000000000000000000000000008080808080808080808000D7D7D7D7
          D700D7D7D7D7D7D7D7D7D7D7D7D7D7D7008080808080808080808000D7D7D7D7
          D700D7D7D7D7D7D7D7D7D7D7D7D7D7D700808080808080808080800000000000
          0000000000000000000000000000000000808080808080808080808080808080
          8080808080808080808080808080808080808080808080808080808080808080
          8080808080808080808080808080808080808080808080808080}
        Layout = slGlyphLeft
        AutoTextSize = True
        KeyStyle = ssRectangle
        CornerRad = 10
        ButtonTravel = 2
        ShadowColor = clGray
        LegendFont.Charset = ANSI_CHARSET
        LegendFont.Color = clBlack
        LegendFont.Height = 18
        LegendFont.Name = 'Verdana'
        LegendFont.Style = []
        GlyphToEdgeSpacing = 4
        TextToEdgeBorder = 5
        MultiLineSpacing = 0
        MaxFontSize = 20
        OnMouseUp = BtnSplitDownMouseUp
      end
    end
    object OctStatusPanel: TPanel
      Left = 1
      Top = 1
      Width = 744
      Height = 40
      Align = alTop
      BevelInner = bvLowered
      TabOrder = 2
      object LeftStatusPanel: TPanel
        Left = 2
        Top = 2
        Width = 390
        Height = 36
        Align = alLeft
        BevelInner = bvLowered
        TabOrder = 0
        object LabelStatus1: TLabel
          Left = 24
          Top = 8
          Width = 7
          Height = 23
        end
      end
      object RightStatusPanel: TPanel
        Left = 392
        Top = 2
        Width = 350
        Height = 36
        Align = alClient
        BevelInner = bvLowered
        TabOrder = 1
        object LabelStatus2: TLabel
          Left = 8
          Top = 8
          Width = 7
          Height = 23
        end
      end
    end
    object OCTLowerPageControl: TPageControl
      Left = 1
      Top = 417
      Width = 744
      Height = 243
      ActivePage = PDisplaySheet
      Align = alClient
      TabOrder = 3
      object ToolManSheet: TTabSheet
        Caption = 'ToolManSheet'
        TabVisible = False
        object ToolManagementPanel: TPanel
          Left = 0
          Top = 0
          Width = 736
          Height = 233
          Align = alClient
          TabOrder = 0
        end
      end
      object PDisplaySheet: TTabSheet
        Caption = 'PDisplaySheet'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Verdana'
        Font.Style = []
        ImageIndex = 1
        ParentFont = False
        TabVisible = False
        object Panel17: TPanel
          Left = 0
          Top = 0
          Width = 736
          Height = 24
          Align = alTop
          Alignment = taLeftJustify
          BevelInner = bvLowered
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Verdana'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          object ProgNameDisplay: TLabel
            Left = 4
            Top = 2
            Width = 5
            Height = 16
          end
          object ProgLineDisplay: TLabel
            Left = 620
            Top = 4
            Width = 5
            Height = 16
          end
        end
      end
    end
  end
  object MiscPanel: TPanel
    Left = 746
    Top = 0
    Width = 270
    Height = 715
    Align = alRight
    TabOrder = 1
    object ToolStatMainPanel: TPanel
      Left = 1
      Top = 1
      Width = 268
      Height = 510
      Align = alTop
      Color = clBtnShadow
      TabOrder = 0
      object TStatHeadPanel: TPanel
        Left = 1
        Top = 51
        Width = 266
        Height = 55
        Align = alTop
        Color = clAppWorkSpace
        TabOrder = 0
        object Panel16: TPanel
          Left = 1
          Top = 1
          Width = 64
          Height = 53
          Align = alLeft
          BevelInner = bvLowered
          Caption = 'Head'
          Color = clBtnShadow
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -19
          Font.Name = 'Verdana'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
        end
        object BtnHead: TThreeLineButton
          Left = 65
          Top = 1
          Width = 117
          Height = 53
          Legend = '???~????~????'
          TextSize = 0
          HasIndicator = False
          IndicatorOn = False
          IsEnabled = True
          ButtonColor = clYellow
          Layout = slGlyphLeft
          AutoTextSize = True
          KeyStyle = ssRectangle
          CornerRad = 10
          ButtonTravel = 2
          ShadowColor = clGray
          LegendFont.Charset = ANSI_CHARSET
          LegendFont.Color = clBlack
          LegendFont.Height = 14
          LegendFont.Name = 'Verdana'
          LegendFont.Style = [fsBold]
          GlyphToEdgeSpacing = 1
          TextToEdgeBorder = 5
          MultiLineSpacing = 0
          Flash = False
          FlashOnTime = 1500
          FlashOffTime = 500
          MaxFontSize = 1000
          align = alLeft
        end
        object InvalidateBlank: TPanel
          Left = 235
          Top = 1
          Width = 30
          Height = 53
          Align = alRight
          Color = clInfoBk
          TabOrder = 2
        end
        object BtnInvalidate: TTouchSoftkey
          Left = 155
          Top = 1
          Width = 80
          Height = 53
          Align = alRight
          Caption = 'TouchSoftkey3'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -19
          Font.Name = 'Verdana'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 3
          OnMouseUp = BtnGeneralMouseUp
          Legend = 'Zero Life~(Eject)'
          TextSize = 14
          HasIndicator = False
          IndicatorOn = False
          IsEnabled = True
          ButtonStyle = skNormal
          ArrowColor = clBlack
          ButtonColor = clWhite
          LegendColor = clBlack
          IndicatorWidth = 20
          IndicatorHeight = 10
          IndicatorOnColor = clGreen
          IndicatorOffColor = clSilver
          MaxFontSize = 0
        end
      end
      object TStatTS1Panel: TPanel
        Left = 1
        Top = 106
        Width = 266
        Height = 55
        Align = alTop
        TabOrder = 1
        object Panel1: TPanel
          Left = 1
          Top = 1
          Width = 64
          Height = 53
          Align = alLeft
          BevelInner = bvLowered
          Caption = 'STN1'
          Color = clBtnShadow
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -19
          Font.Name = 'Verdana'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
        end
        object BtnTS1: TThreeLineButton
          Left = 65
          Top = 1
          Width = 117
          Height = 53
          Legend = '???~????~????'
          TextSize = 0
          HasIndicator = False
          IndicatorOn = False
          IsEnabled = True
          ButtonColor = clYellow
          Layout = slGlyphLeft
          AutoTextSize = True
          KeyStyle = ssRectangle
          CornerRad = 10
          ButtonTravel = 2
          ShadowColor = clGray
          LegendFont.Charset = ANSI_CHARSET
          LegendFont.Color = clNavy
          LegendFont.Height = 14
          LegendFont.Name = 'Verdana'
          LegendFont.Style = [fsBold]
          GlyphToEdgeSpacing = 1
          TextToEdgeBorder = 5
          MultiLineSpacing = 0
          Flash = False
          FlashOnTime = 1500
          FlashOffTime = 500
          MaxFontSize = 1000
          OnMouseUp = BtnGeneralMouseUp
          OnMouseDown = BtnGeneralMouseDown
          align = alLeft
        end
        object STN1Location: TPanel
          Left = 185
          Top = 1
          Width = 80
          Height = 53
          Align = alRight
          BevelInner = bvLowered
          Caption = '----'
          Color = clInfoBk
          TabOrder = 2
        end
      end
      object TStatTS2Panel: TPanel
        Left = 1
        Top = 161
        Width = 266
        Height = 55
        Align = alTop
        TabOrder = 2
        object Panel5: TPanel
          Left = 1
          Top = 1
          Width = 64
          Height = 53
          Align = alLeft
          BevelInner = bvLowered
          Caption = 'STN2'
          Color = clBtnShadow
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -19
          Font.Name = 'Verdana'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
        end
        object btnTS2: TThreeLineButton
          Left = 65
          Top = 1
          Width = 117
          Height = 53
          Legend = '???~????~????'
          TextSize = 0
          HasIndicator = False
          IndicatorOn = False
          IsEnabled = True
          ButtonColor = clYellow
          Layout = slGlyphLeft
          AutoTextSize = True
          KeyStyle = ssRectangle
          CornerRad = 10
          ButtonTravel = 2
          ShadowColor = clGray
          LegendFont.Charset = ANSI_CHARSET
          LegendFont.Color = clNavy
          LegendFont.Height = 14
          LegendFont.Name = 'Verdana'
          LegendFont.Style = [fsBold]
          GlyphToEdgeSpacing = 1
          TextToEdgeBorder = 5
          MultiLineSpacing = 0
          Flash = False
          FlashOnTime = 1500
          FlashOffTime = 500
          MaxFontSize = 1000
          OnMouseUp = BtnGeneralMouseUp
          OnMouseDown = BtnGeneralMouseDown
          align = alLeft
        end
        object STN2Location: TPanel
          Left = 185
          Top = 1
          Width = 80
          Height = 53
          Align = alRight
          BevelInner = bvLowered
          Caption = '----'
          Color = clInfoBk
          TabOrder = 2
        end
      end
      object TStatTS3Panel: TPanel
        Left = 1
        Top = 216
        Width = 266
        Height = 55
        Align = alTop
        TabOrder = 3
        object Panel14: TPanel
          Left = 1
          Top = 1
          Width = 64
          Height = 53
          Align = alLeft
          BevelInner = bvLowered
          Caption = 'STN3'
          Color = clBtnShadow
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -19
          Font.Name = 'Verdana'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
        end
        object btnTS3: TThreeLineButton
          Left = 65
          Top = 1
          Width = 117
          Height = 53
          Legend = '???~????~????'
          TextSize = 0
          HasIndicator = False
          IndicatorOn = False
          IsEnabled = True
          ButtonColor = clYellow
          Layout = slGlyphLeft
          AutoTextSize = True
          KeyStyle = ssRectangle
          CornerRad = 10
          ButtonTravel = 2
          ShadowColor = clGray
          LegendFont.Charset = ANSI_CHARSET
          LegendFont.Color = clNavy
          LegendFont.Height = 14
          LegendFont.Name = 'Verdana'
          LegendFont.Style = [fsBold]
          GlyphToEdgeSpacing = 1
          TextToEdgeBorder = 5
          MultiLineSpacing = 0
          Flash = False
          FlashOnTime = 1500
          FlashOffTime = 500
          MaxFontSize = 1000
          OnMouseUp = BtnGeneralMouseUp
          OnMouseDown = BtnGeneralMouseDown
          align = alLeft
        end
        object STN3Location: TPanel
          Left = 185
          Top = 1
          Width = 80
          Height = 53
          Align = alRight
          BevelInner = bvLowered
          Caption = '----'
          Color = clInfoBk
          TabOrder = 2
        end
      end
      object TStatTS4Panel: TPanel
        Left = 1
        Top = 271
        Width = 266
        Height = 55
        Align = alTop
        TabOrder = 4
        object Panel15: TPanel
          Left = 1
          Top = 1
          Width = 64
          Height = 53
          Align = alLeft
          BevelInner = bvLowered
          Caption = 'STN4'
          Color = clBtnShadow
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -19
          Font.Name = 'Verdana'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
        end
        object btnTS4: TThreeLineButton
          Left = 65
          Top = 1
          Width = 117
          Height = 53
          Legend = '???~????~????'
          TextSize = 0
          HasIndicator = False
          IndicatorOn = False
          IsEnabled = True
          ButtonColor = clYellow
          Layout = slGlyphLeft
          AutoTextSize = True
          KeyStyle = ssRectangle
          CornerRad = 10
          ButtonTravel = 2
          ShadowColor = clGray
          LegendFont.Charset = ANSI_CHARSET
          LegendFont.Color = clNavy
          LegendFont.Height = 14
          LegendFont.Name = 'Verdana'
          LegendFont.Style = [fsBold]
          GlyphToEdgeSpacing = 1
          TextToEdgeBorder = 5
          MultiLineSpacing = 0
          Flash = False
          FlashOnTime = 1500
          FlashOffTime = 500
          MaxFontSize = 1000
          OnMouseUp = BtnGeneralMouseUp
          OnMouseDown = BtnGeneralMouseDown
          align = alLeft
        end
        object STN4Location: TPanel
          Left = 185
          Top = 1
          Width = 80
          Height = 53
          Align = alRight
          BevelInner = bvLowered
          Caption = '----'
          Color = clInfoBk
          TabOrder = 2
        end
      end
      object Panel22: TPanel
        Left = 1
        Top = 1
        Width = 266
        Height = 50
        Align = alTop
        Color = clBtnShadow
        Font.Charset = ANSI_CHARSET
        Font.Color = clWhite
        Font.Height = -19
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
        TabOrder = 5
        object RemTimeDisplay: TLabel
          Left = 84
          Top = 8
          Width = 85
          Height = 35
          Alignment = taCenter
          Caption = '00:00'
          Font.Charset = ANSI_CHARSET
          Font.Color = clYellow
          Font.Height = -29
          Font.Name = 'Verdana'
          Font.Style = []
          ParentFont = False
        end
        object ToolDisplayToggle: TTouchSoftkey
          Left = 185
          Top = 1
          Width = 80
          Height = 48
          Align = alRight
          Caption = 'BtnOpenToolDoor'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -19
          Font.Name = 'Verdana'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 0
          OnMouseDown = BtnGeneralMouseDown
          OnMouseUp = BtnGeneralMouseUp
          Legend = 'Display~Mode'
          TextSize = 14
          HasIndicator = False
          IndicatorOn = False
          IsEnabled = True
          ButtonStyle = skNormal
          ArrowColor = clBlack
          ButtonColor = clWhite
          LegendColor = clBlack
          IndicatorWidth = 20
          IndicatorHeight = 10
          IndicatorOnColor = clGreen
          IndicatorOffColor = clSilver
          MaxFontSize = 0
        end
        object Panel3: TPanel
          Left = 1
          Top = 1
          Width = 64
          Height = 48
          Align = alLeft
          BevelInner = bvLowered
          Color = clBtnShadow
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -16
          Font.Name = 'Verdana'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 1
          object Label2: TLabel
            Left = 5
            Top = 5
            Width = 40
            Height = 18
            Caption = 'Time'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -16
            Font.Name = 'Verdana'
            Font.Style = []
            ParentFont = False
          end
          object Label3: TLabel
            Left = 5
            Top = 24
            Width = 42
            Height = 18
            Caption = 'Rem.'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -16
            Font.Name = 'Verdana'
            Font.Style = []
            ParentFont = False
          end
        end
      end
      object OctDisplayModePanel: TPanel
        Left = 1
        Top = 472
        Width = 266
        Height = 37
        Align = alBottom
        BevelInner = bvLowered
        Color = clInfoBk
        TabOrder = 6
        object BULabel: TLabel
          Left = 224
          Top = 6
          Width = 78
          Height = 23
          Caption = 'BULabel'
        end
      end
      object TStatTS5Panel: TPanel
        Left = 1
        Top = 326
        Width = 266
        Height = 55
        Align = alTop
        TabOrder = 7
        object Panel19: TPanel
          Left = 1
          Top = 1
          Width = 64
          Height = 53
          Align = alLeft
          BevelInner = bvLowered
          Caption = 'STN5'
          Color = clBtnShadow
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -19
          Font.Name = 'Verdana'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
        end
        object btnTS5: TThreeLineButton
          Left = 65
          Top = 1
          Width = 117
          Height = 53
          Legend = '???~????~????'
          TextSize = 0
          HasIndicator = False
          IndicatorOn = False
          IsEnabled = True
          ButtonColor = clYellow
          Layout = slGlyphLeft
          AutoTextSize = True
          KeyStyle = ssRectangle
          CornerRad = 10
          ButtonTravel = 2
          ShadowColor = clGray
          LegendFont.Charset = ANSI_CHARSET
          LegendFont.Color = clNavy
          LegendFont.Height = 14
          LegendFont.Name = 'Verdana'
          LegendFont.Style = [fsBold]
          GlyphToEdgeSpacing = 1
          TextToEdgeBorder = 5
          MultiLineSpacing = 0
          Flash = False
          FlashOnTime = 1500
          FlashOffTime = 500
          MaxFontSize = 1000
          OnMouseUp = BtnGeneralMouseUp
          OnMouseDown = BtnGeneralMouseDown
          align = alLeft
        end
        object STN5Location: TPanel
          Left = 185
          Top = 1
          Width = 80
          Height = 53
          Align = alRight
          BevelInner = bvLowered
          Caption = '----'
          Color = clInfoBk
          TabOrder = 2
        end
      end
      object TStatTS6Panel: TPanel
        Left = 1
        Top = 381
        Width = 266
        Height = 55
        Align = alTop
        TabOrder = 8
        object Panel24: TPanel
          Left = 1
          Top = 1
          Width = 64
          Height = 53
          Align = alLeft
          BevelInner = bvLowered
          Caption = 'STN6'
          Color = clBtnShadow
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -19
          Font.Name = 'Verdana'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
        end
        object btnTS6: TThreeLineButton
          Left = 65
          Top = 1
          Width = 117
          Height = 53
          Legend = '???~????~????'
          TextSize = 0
          HasIndicator = False
          IndicatorOn = False
          IsEnabled = True
          ButtonColor = clYellow
          Layout = slGlyphLeft
          AutoTextSize = True
          KeyStyle = ssRectangle
          CornerRad = 10
          ButtonTravel = 2
          ShadowColor = clGray
          LegendFont.Charset = ANSI_CHARSET
          LegendFont.Color = clNavy
          LegendFont.Height = 14
          LegendFont.Name = 'Verdana'
          LegendFont.Style = [fsBold]
          GlyphToEdgeSpacing = 1
          TextToEdgeBorder = 5
          MultiLineSpacing = 0
          Flash = False
          FlashOnTime = 1500
          FlashOffTime = 500
          MaxFontSize = 1000
          OnMouseUp = BtnGeneralMouseUp
          OnMouseDown = BtnGeneralMouseDown
          align = alLeft
        end
        object STN6Location: TPanel
          Left = 185
          Top = 1
          Width = 80
          Height = 53
          Align = alRight
          BevelInner = bvLowered
          Caption = '----'
          Color = clInfoBk
          TabOrder = 2
        end
      end
    end
    object OperatorButtonPanel: TPanel
      Left = 1
      Top = 511
      Width = 268
      Height = 203
      Align = alClient
      TabOrder = 1
      object Panel23: TPanel
        Left = 1
        Top = 144
        Width = 266
        Height = 58
        Align = alBottom
        TabOrder = 0
        object BtnShowPartData: TTouchGlyphSoftkey
          Left = 1
          Top = 1
          Width = 130
          Height = 56
          Legend = 'Show~Part Data'
          TextSize = 0
          HasIndicator = False
          IndicatorOn = False
          IsEnabled = True
          ButtonColor = clInfoBk
          Layout = slGlyphLeft
          AutoTextSize = True
          KeyStyle = ssRectangle
          CornerRad = 10
          ButtonTravel = 2
          ShadowColor = clGray
          LegendFont.Charset = ANSI_CHARSET
          LegendFont.Color = clBlack
          LegendFont.Height = 20
          LegendFont.Name = 'Verdana'
          LegendFont.Style = []
          GlyphToEdgeSpacing = 1
          TextToEdgeBorder = 7
          MultiLineSpacing = 0
          MaxFontSize = 20
          OnMouseUp = BtnGeneralMouseUp
          OnMouseDown = BtnGeneralMouseDown
          align = alLeft
        end
        object BtnFaultAck: TTouchGlyphSoftkey
          Left = 135
          Top = 1
          Width = 130
          Height = 56
          Legend = 'Fault~Acknowledge'
          TextSize = 0
          HasIndicator = False
          IndicatorOn = False
          IsEnabled = True
          ButtonColor = clInfoBk
          Layout = slGlyphLeft
          AutoTextSize = True
          KeyStyle = ssRectangle
          CornerRad = 10
          ButtonTravel = 2
          ShadowColor = clGray
          LegendFont.Charset = ANSI_CHARSET
          LegendFont.Color = clBlack
          LegendFont.Height = 20
          LegendFont.Name = 'Verdana'
          LegendFont.Style = []
          GlyphToEdgeSpacing = 1
          TextToEdgeBorder = 7
          MultiLineSpacing = 0
          MaxFontSize = 20
          OnMouseUp = BtnGeneralMouseUp
          OnMouseDown = BtnGeneralMouseDown
          align = alRight
        end
      end
      object Panel18: TPanel
        Left = 1
        Top = 1
        Width = 132
        Height = 143
        Align = alLeft
        Caption = 'Panel18'
        TabOrder = 1
        object Panel20: TPanel
          Left = 1
          Top = 1
          Width = 130
          Height = 70
          Align = alTop
          BevelInner = bvLowered
          BevelWidth = 2
          Caption = 'Panel20'
          TabOrder = 0
          object BtnPartChuck: TTouchSoftkey
            Left = 4
            Top = 4
            Width = 122
            Height = 45
            Align = alTop
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -19
            Font.Name = 'Verdana'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 0
            OnMouseDown = BtnGeneralMouseDown
            OnMouseUp = BtnGeneralMouseUp
            Legend = 'Part~Chuck'
            TextSize = 14
            HasIndicator = False
            IndicatorOn = False
            IsEnabled = True
            ButtonStyle = skNormal
            ArrowColor = clBlack
            ButtonColor = clWhite
            LegendColor = clBlack
            IndicatorWidth = 20
            IndicatorHeight = 10
            IndicatorOnColor = clGreen
            IndicatorOffColor = clSilver
            MaxFontSize = 0
          end
          object PartChuckStatusPanel: TPanel
            Left = 4
            Top = 49
            Width = 122
            Height = 17
            Align = alClient
            BevelInner = bvLowered
            Color = clLime
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Verdana'
            Font.Style = []
            ParentFont = False
            TabOrder = 1
          end
        end
        object Panel21: TPanel
          Left = 1
          Top = 71
          Width = 130
          Height = 71
          Align = alClient
          BevelInner = bvLowered
          BevelWidth = 2
          Caption = 'Panel21'
          TabOrder = 1
          object BtnToolChuck: TTouchSoftkey
            Left = 4
            Top = 4
            Width = 122
            Height = 45
            Align = alTop
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -19
            Font.Name = 'Verdana'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 0
            OnMouseDown = BtnGeneralMouseDown
            OnMouseUp = BtnGeneralMouseUp
            Legend = 'Tool~Chuck'
            TextSize = 14
            HasIndicator = False
            IndicatorOn = False
            IsEnabled = True
            ButtonStyle = skNormal
            ArrowColor = clBlack
            ButtonColor = clWhite
            LegendColor = clBlack
            IndicatorWidth = 20
            IndicatorHeight = 10
            IndicatorOnColor = clGreen
            IndicatorOffColor = clSilver
            MaxFontSize = 0
          end
          object ToolChuckStatusPanel: TPanel
            Left = 4
            Top = 49
            Width = 122
            Height = 18
            Align = alClient
            BevelInner = bvLowered
            Color = clLime
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Verdana'
            Font.Style = []
            ParentFont = False
            TabOrder = 1
          end
        end
      end
      object Panel27: TPanel
        Left = 135
        Top = 1
        Width = 132
        Height = 143
        Align = alRight
        Caption = 'Panel27'
        TabOrder = 2
        object Panel28: TPanel
          Left = 1
          Top = 1
          Width = 130
          Height = 70
          Align = alTop
          BevelInner = bvLowered
          BevelWidth = 2
          Caption = 'Panel28'
          TabOrder = 0
          object BtnOpenMainDoor: TTouchSoftkey
            Left = 4
            Top = 4
            Width = 122
            Height = 45
            Align = alTop
            Enabled = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -19
            Font.Name = 'Verdana'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 0
            OnMouseDown = BtnGeneralMouseDown
            OnMouseUp = BtnGeneralMouseUp
            Legend = 'Main~Door'
            TextSize = 14
            HasIndicator = False
            IndicatorOn = False
            IsEnabled = False
            ButtonStyle = skNormal
            ArrowColor = clBlack
            ButtonColor = clWhite
            LegendColor = clBlack
            IndicatorWidth = 20
            IndicatorHeight = 10
            IndicatorOnColor = clGreen
            IndicatorOffColor = clSilver
            MaxFontSize = 0
          end
          object MainDoorStatusPanel: TPanel
            Left = 4
            Top = 49
            Width = 122
            Height = 17
            Align = alClient
            BevelInner = bvLowered
            Color = clLime
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Verdana'
            Font.Style = []
            ParentFont = False
            TabOrder = 1
          end
        end
        object Panel29: TPanel
          Left = 1
          Top = 71
          Width = 130
          Height = 71
          Align = alClient
          BevelInner = bvLowered
          BevelWidth = 2
          Caption = 'Panel29'
          TabOrder = 1
          object BtnOpenToolDoor: TTouchSoftkey
            Left = 4
            Top = 4
            Width = 122
            Height = 45
            Align = alTop
            Caption = 'BtnOpenToolDoor'
            Enabled = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -19
            Font.Name = 'Verdana'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 0
            OnMouseDown = BtnGeneralMouseDown
            OnMouseUp = BtnGeneralMouseUp
            Legend = 'Tool~Door'
            TextSize = 14
            HasIndicator = False
            IndicatorOn = False
            IsEnabled = False
            ButtonStyle = skNormal
            ArrowColor = clBlack
            ButtonColor = clWhite
            LegendColor = clBlack
            IndicatorWidth = 20
            IndicatorHeight = 10
            IndicatorOnColor = clGreen
            IndicatorOffColor = clSilver
            MaxFontSize = 0
          end
          object ToolDoorStatusPanel: TPanel
            Left = 4
            Top = 49
            Width = 122
            Height = 18
            Align = alClient
            BevelInner = bvLowered
            Color = clLime
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Verdana'
            Font.Style = []
            ParentFont = False
            TabOrder = 1
          end
        end
      end
    end
    object WinLogoPanel: TPanel
      Left = 1
      Top = 101
      Width = 268
      Height = 25
      BevelInner = bvLowered
      Caption = 'WinLogoPanel'
      TabOrder = 2
      object Image1: TImage
        Left = 2
        Top = 2
        Width = 264
        Height = 21
        Align = alClient
        Picture.Data = {
          0A544A504547496D616765305A0000FFD8FFE000104A46494600010100000100
          010000FFDB004300010101010101010101010101010101010101010101010101
          0101010101010101010101010101010101010101010101010101010101010101
          0101010101010101FFDB00430101010101010101010101010101010101010101
          0101010101010101010101010101010101010101010101010101010101010101
          01010101010101010101010101FFC0001108010C010503012200021101031101
          FFC4001F000100000505010000000000000000000000060708090A0102030405
          0BFFC40046100000050302040404030603060407000001020304050006110721
          0812314109135161142271811591F02332A1B1C1E10A24D11625263652621742
          72F134354344455475FFC4001E01010001040301010000000000000000000008
          0406070901050A0203FFC4003F11000104010303020305040805050000000201
          03040506000711081221133114415109152261713281A1D1162324334291E1F0
          174352C1F1263595B1B4FFDA000C03010002110311003F00CFE294A534D294A5
          34D294A534D294A534D294A534D294A534D294A534D294A534D294A534D294A5
          34D294A534D294A534D294A534D294A534D294A534D294A534D294A534D294A5
          34D294A534D294A534D294A534D294A534D294A534D294A534D294A534D294A5
          34D294A534D294A534D294A534D294A534D294A534D294A534D294A534D294A5
          34D294A534D294A534D294A534D294A534D294A534D294A534D294A534D294A5
          34D294A534D294A534D294A534D294A534D294A534D294A534D294A534D294A5
          34D294A534D294A534D294A534D294A534D294A534D294A534D294A534D29411
          0001111C006E223D003D46A9B75A78A0D35D1C887EE24A6593C966C9282947A2
          E0860058A51102AC729B6C0E004A5C88F4E60A69AA8193978C866C6792AFDAC7
          B627559D2C4449F40E710130FB14047DAA503FE23347E39C2AD97BC23CCA226E
          530A67298823B6794C631338DF3B074F41CD633FC60788EDE17B3B7A2CA75181
          80609AA755CFC71583066D533089965D653CB413210BB99454E01D79847AD586
          2EDE3435C7592664EDCE1D64A4DD372AC2D677582794748DA10A2654C4582DE4
          542F3CF4826990C744C0928880882856CAA5CAE03A7B9BEAAA08A52ED25B71DB
          445500554579E54FF0B4DF3DC4BCF08A5E005553B88794D648DB2DA5CFB77AFD
          9C7703C7E65C4C71C6C244906CC2BABC0D7FBE9F35455A60047B8FD34EF90E00
          1AB2CB9DA4899ECEAEF8907041A0716D65359388CD3DB005F9B92321A564177B
          754B987980BF83DA304DA5AE695298C4310146114E13E70E53180DB54AFB0FC5
          FBC3D750804D17AF69C1144E0544F7CE9FEA658A8B92984408B22E2E9B3E2DBF
          90700031545554804A20610001DB048B5B48A26D59679795D1332DA91A9F2C26
          566B50AF2727949A55650C273A519F1275CB12C931F91145B9C552A25F2CCB0A
          6054CB124938C89B71C000875C863AEDEF8CFA64473BD600B8DF79EDCA24A7A9
          82B1049504A7AC8379C145FDAE187981045E155394254E78545E395DC0EDB7D9
          3188CEA18EE6E3EE1E5619148680DD63120A68B5D05D24422694AD2B2D1C97D8
          ABD8440E3425DBDE243DDDA1F48DB0F51EC0D52806F7569BDEB6BDF76DBB02F9
          13769CE474F4698C62154048CEA35C384925C086298EDD531174C0439D328ED5
          1A57CE7B863E25B5AB870D56B6EE3D1BBFE6ECF75273D0B1D391CD570730171C
          739936A9ACC2E0B7DE02D132C88A6220999D353AED0C3E6335DBAB8503E815A8
          1AEDA27A4B316A5BDAA9ABDA69A6B3B7C848059B157EDF16D5A0FAEB3C49E3D2
          934EDF427E4A3CF2EB32565A35370831059521DF362F2655280E50DBEDC28F9B
          C19CF3B0FEEC9556715B9A84F01C435968F7A071DD2EC713B958704DA7411409
          4105C77B955207758BD1BDC74B395E295B07251CE2833D8D7D2F1971AAC91132
          18C98E2D62DA45B7AF63E2629AB216F10E3CE832482536129C7A1D7A3420E4D7
          A56C4D44D64D3552508AA4A90AA24AA6629D35133940C451339444A721CA2062
          98A2253144040440406B7D647F7F6D42854545545454545E1517C2A2A7BA2A7C
          9534A5294D71A5294A69A5294A69A5294A69A5294A69A5294A69A5294A69A529
          4A69A5294A69A5294A69A5294E9D69A695E4CDCEC45B91EB4A4DBF6D1CC50289
          9470E542A640C008F2972393184036294046A9BB5F78B0D37D0E87905642599B
          C9C6C918536045D31226A7288879C7036E62ED94CBDC4004C03F2D6389C5C789
          BCDDDF1F2CE9DDC0DA02D78EF8855D3B72FD1631ED1B26537CC639CC4483201C
          A4287328A9C40A981CE2051E08840488C9044514888950444513955255E11111
          3CAAAAF089EFAFD1965D90EB6C30D38FBEF380D32CB204E3AEBAE120836DB608
          466E192A0800A29112A2222AAA26AE83C5E7892C4DA81236A69CC8245548470D
          D6904D401596372F20F2090E02420E470528E4761111DB18B7F141C7428CE615
          693F332B775E93EB8920B4F6DB15E42E394597F30C973B748CB1639A9C77174E
          C0A024F99045C7298954B33BACFACBC4FCCBF65A1AD5C5B16528E9C3698D71BA
          D9AC922A940395C2563442E42AD24EB23849EAA51048E05329F8788156A9A9A6
          1A196068D24E9FC4A6EAE2BD253279FD40B955FC46E8985D42002E3F16B0A9F8
          7B55370F836629904A602B851C9CA07AC4B976E9D7D423B0E9BB27CE4EE1290B
          E62B25EDF8153FBE245F65FEED17854F544BC6C3BA76E81F2FDC4720E47B9692
          B13C55C56DF6AA1115BBEB46B912447857FF006D61C4544205FED8A8A604B09E
          015291886906A6EB83A6F73F11D26784B548A20F61F44ADB7EA918900A3E7247
          BD65DBAA0B49BB2898BE6B3494104CC07295567F337AA8423589B7E35B434147
          3289898F44ADD947473649A346C8A61829114112913207711C098C611318C261
          388C452925CDCD9377F6FE5D33FA1CD4BD927D9E6C0FB0607F9F7FAEFD31BEE1
          519EF6FEC6EA49CAB196EC974D79FC65F841138E100795411445E13844E07C27
          08889ADE6ED2ED161BB634912830CC7E152C0600049233008FC82FC3DEEC991D
          BEA3EEB8A9DEE1992AB87CBA7DC6444BE6C93BCF36FEB80C6DDFA887F50F7C00
          5406FDC64739FEFBE7A74DFDBA060315EA483B111306771DFAF4EBF60C74F70C
          ED5073D71B8E047A0F4E99DF1D7D3A77C08FE7673EEF3CF9FF007FE9EEBFB939
          D492A983DA83F853C71F97C93F87E5FE7F4D7661E4863AE38190D8458CDC53CD
          FA65B3F417E9B7FD0019FAEF55F7FE237E272DEE24D4F0FB9788295392827FAB
          212E44C7F640AC8496938A27280EE5138B25444BD031B6C2156DC22C22F5AEFD
          5D201B63611549DBA8E319EF81FA6F027886493A777270F2D1758CA20CE56E0F
          87298C610202B2569029D738E6F2C83B7A67AF4CBFB665CE1BB8CBEE9FFA77FF
          00D13B9FD7E7F97E7C79D6B83AE66BB3A97E8B47E6A9BCABE3DFCD4E269FCFF4
          5F7FCEEF7C0CF8C7711FC1DAB1366DCAEDD6B6687365516C7B0EEB9458676D96
          1E781D61B0EEB70576F2241248EA99084902485BE63095245A30111724CC5384
          5E3C786DE35AD62CE68BDF0D5C5C0CDAA4BDCBA733C64626FF00B5943F314E12
          500A2C751DB02AA439129B88524219C7CA047A0A98522FCDA15386447380DC00
          7EE221EDBFEB7AF72CABFAF5D32BAA22F8D3DBAA7ECABBEDF7893E85B92D9947
          70F2F1CE91382843B77AC954950218C402AC8184C838484C93849548E721ADDC
          2379321C495A85388EF28C3B4121C9757E2E1B68889FD8662A19080271C467D1
          D61107B1A48EAA47ACCDD527D9A7B35D43858E518B34C6D56E9C947642E47470
          5B5C7F229A5C9AFF004B31D60A3B121F90E297AB7756506DBD47164CE2B746C2
          2AFD50E958A27013FE213208C2E98F1C8C00A3FE523A3F5F2D68DC13711448B6
          A2DA31C88F260051339B86D74794001459DC00654761947D8F7E595A996B43DF
          1A7975405EB67DC0CD27F0B725B328CE621E45AAC429C8A367CC55591318A060
          2AA9098AB20A01915D34D5218812EF14CDB1CCCE2249A49E0EBA00252603DC33
          610D578F1223292976F2BDA8FB4AEC735F0DBC4A8BC79CCEA07A5CDE7E99F215
          A3DD2C4E4408521F71AA4CB6B7D4B1C3F23004524729EF01A6D927D5B4F55DAB
          9CDC1B98ADA894CAE8E860A5165294ABB351EB4A5294D34A5294D34A5294D34A
          5294D34A5294D34A5294D34A5294D34A56D39C8990CA287290840131CE730148
          5280644C6318400A001B8888800075AA31E2278CDD3DD138D7C822F9B4ACEA6D
          D61211254876EDD502980BCC2537ED0C53067A81031BF300D34D555DD577DBD6
          5C4B999B8A4DB47326A91D5399650A539F9433CA910440C730EC018D83391100
          AB2A716FE27AD2DF51D5B5A74B7949F21D151D26630AEA98798A6131D328F297
          D080382886F911C8DA5B8B2F124B82E0653B31745E285B96C3731C0EF241D19B
          A004364134104C14E770B9C039526CDD355754C184D33886D65192D4AD72E2B1
          EAA5D3C348E96E943931537FAA93ED8E4BAAE6682A98162D970EA9CAA3445748
          82524928A00979F985DA0A733635BF7F9354639195FB192226A3CB518150A43C
          BEC882DF3F84557C779F68F85445224ED5CBFB49B1BB8BBD572DD561548F488E
          2E8B73AEE489B14F5C3C8F794896A0A2E3A0242BF0EC238F22109382DB4AAE8D
          577143C7DC84BDCAE2D66412DA99A9D36A9CB1760DB0AA8F9F116504008B4EB9
          215446199904C0A2E671CCE01228A9E4152FDB9699ADEE1D6EAD4D93697CF149
          348CD19058CF20B47205CAA9D8D6F098C4144670E4504D70C91132815C019551
          BF3098AA3976888A259CBA5DA4BA77A2D16AB2B3E2F32AF404F39754AA9F885C
          F703831C55557939558BE718AA2A61501A23E4B420E0C4440F950D1B3D9911CF
          CD9EA03BFF002F7DB3EA3D07982A36E5BB93697FEA47609605773C2476497B9D
          4E7C2BE69C138BECA89C2022A22888AF3CEEEBA77E88704DA36E25CDB476B2CC
          D10049DB9B1604A3C1709050DBAB865DEDC504E487BD14DE21236DE90FB7D9DB
          EBA8E99473445847366B1EC19A246ED59324136AD1A209979088B76E81534504
          8850E52A6994A4280014002A0C91941309FE6F7FEDD7EBBFF2E83E63D9611E60
          E7DF18F7FD67A760EB9010C541CF64B39F9FD71BF6CFB8FD87B007AF50C57225
          F3CF9E7EBE557E8AABF2F7FE3FAF3CEC02A2805B40410444441444444E111384
          E1111138E13D938F64FAA26BB5212223CDBEC3B75C67AF7F7DF3B875F4C05412
          FDF73737CD9D876CFB8E37DF3D87DFE95B5E48098477FB760CFD476C7F3FAEF0
          ABC799CE447EA239C6FB6D81FCBAEDD002BA679FE79F3FC7F24FAFCFC7B7CFF4
          F1AC955755D882BDBFF4FCBCFCB9E7C7EEE5175B1E3BFDEDFB8EFD03F2FCC43A
          E37EE150B3A739E60CFF001E9B7D833B7B60077EF5CEE9C88E703D7A6FF988FA
          FD471D3EC30FB95FDF3D7EE39F7EC1D7AEF5D53AE795F3FAFBAAA794FE7E7F55
          F09ABF2043ED41F09FBB8E7C71EFFE9C7EE4D7220AFF009E678EEEDB86FD31E7
          103D3A63A8E36EFDEA08F102DAF0D030EA212B3838D873994B57B637CE31F501
          DBB044CD15019063FF0073D6A1DB7CAE4CE36DFF00F60E950C7881ED79681864
          73F89CE0FB6D296A06701D3B63DB1D32159AB6C0B9C2B71BF5C713E9FF003E6A
          FF001FFBEB585D7837DBD4F74541C2F90DE4FE3598A27FBF1C7EFE75A2A6CEC0
          3EDDBA6F9F5CE7D431EFEFD154C20021B76D87BE71B7D3DBEA3E98E539F00222
          3BFEB6C7EBAFBD74CE7CE47D3223EFD7D3DBF9D47E25FE0A9CA7EF15FF002F97
          EABADC0B41C71EDF4FDFE3CA78F64F6D719CD8CFA88FB67AFEB7C5565F07FC7E
          712DC11DD259CD17BE5CA56DBB768B8B9B4D6E132F2FA7B74A69F31540908051
          72118BF3A4739139B86563A610C900AF0C913C935189C723D73FD3DBF97AEFE9
          D2B84C21B87A87D7EE1B8673B63B00E771C0D5457D8CFAA98CCFAD99220CD8E5
          DECC98AE9B2F365F3E0C1515449391305E44C5544C4855517A7CB70BC4F7031D
          B1C4737C729F2BC66DD858F67477B023D9574B6F9EE45723496DC047593E1C8E
          FB682FC77841E8EE36F001A7D02B80BF197E1A78CC4A22CBB89EB5D11D757293
          76EA69F5DD2A81616E7911404CB8E9F5D8E0ACD9CD82AAA6A990847C9C7DC242
          09134993F280B93DE06BE4E64555414496455510591391549644E64D54544CC0
          74D54942881935086029CA72894E530731440C00357E6E01BC77B5EB86AFC0F4
          E7883095D7FD196BF0D1EDDFC83F036AAD971C9220DD3FC1AE17E7305D31ED08
          448490B72AC2E01220A2C67589391234A2C137FDB77D0ACCDDB465C5ED6C2FA2
          B4BE892F8442B186D0AAB4ABFE2911055BE553BA2B4084E6B439D58FD8FB360F
          DE59BF4B131CB18888ECC97B4990D80FDE0C0A7266DE1992CF70427B629E19A6
          C8E43735040BD1BEB07DC6616B3A6A5538F0D3C5A680717563217F682EA243DE
          717C8884BC4915065755AEF154935063AE9B65D0925A11E13CC0200B96FF0006
          E84A63B076ED100546A3AA4C4499127C66664192C4C892011C624C6741F61E05
          F636DD6C880C57EA2ABE7945F28BAD186438E5FE2575638DE534B6B8EE415124
          E1DAD2DDC09559695F29BFDB6264198D33263BA88A85DAE362AA2A243C8922AA
          94A554EBA6D294A534D294A534D294A534D294AEBBA76D98A0A3A78E116ADD12
          89D559750A9264286E22639C4003F3C8F40DE9A6BB15025F3A916869E45B994B
          9A61A31237454541B9D64C1C2A2428980854C4DCC1CDB604C00181CEFD2A8D38
          93E3BEC4D2262E585BAFDA4ACD024B145605082922A140403CA2E4798C03BF31
          83E8018011C5FF008B6F10E957249BB9AFCBE3F01803A8E811F3DD98EE1EAA02
          730338C64994CE5F3A38094A441B24A18398A271213260F871D6D96CDD79C069
          A6C548DC708400053DC88C9504513E6AAA89AAA85066D94B8F02BA2499D3A5BA
          2C45870D8724CA92F1AF00D30C3226EBAE12F84001225FA6AEDFC60F89A3F588
          E2DDD3F54EC1A819648146CA979D7EA50328A14E0636431F28EC1BE03AD6349C
          4371DD33705D4E2CEB31290D58D507A632436E442E2AC4411CC531CCEAE99B4C
          C76B1E8B71129976E0A02E50C91C28D3980E6A569ED40D6BE26975538E752DA4
          1A44E0C722934E47CBD43BC989C04A22C880201051EE88636140103993300F9E
          F0A2644B3874FEC8B2B4BE1C216CC876F1A91C4AA3D7C6105E5655C014A533A9
          492532E5E2C710E6C1C4114C444104922642B08663BC10A07AB031D50972D154
          0E71A731DA2F65F48553FAC2155F734E115147B13913D6D3FA69FB37326CBD2B
          F2EDE64918F5039E9498B8A306A16D62D2F698FDE2F82FF6464C55396639A38A
          2607F10AA2EC6D4136B68348DD136D350788D9E47506EA40C473116637E74F4F
          6D136054045A459B953967691CD851CBB219050E4E6508F479560AA63CAA68A4
          445004D14524C8924924529124524CA054D34C840021084280148428014A5280
          1436C0404A4B8E773EF8C6E381E990C6443F88E7D7A579AB4A8E373E71EFB07F
          2C6DDC3D6A3A595DCDB390E4A9D25C92FBA4A446E1A92F2BC784EE554E3DBE7C
          F8F755D6E8B09DAAC5F04A7874189D1C2A5AA84D36D31161C76D81E038F27D80
          2A65CAA92AAF088444A8288BC6A357531B8FCDDFD47A6FB6FD3A740CFF001A87
          1D4AE73F307D0719F6FB63EFEDB08D42CBCA08FF00E6C8EF91FEF90FCF71D807
          1BE2B64537733EFBE01B2C924B0A4AADCCB09C09CA972F37EE14E701F9B01800
          E82023541182558CB8F061345225CB78188EC36A9DEF3AE2A0800F72A0F24BE3
          CAF1F554444E2EFBA934185E3F6F95E4F3E3D363D8FD7C8B5B9B59486B1ABEBE
          2368EC994FA32DBAEAB6D368A47D8D19F09E05784D761DC97EF7CDEE3F4EA1FA
          E81D8007A432EA40473F37D73D803BF50C8FA88E37EC038A96F3BA86D62B57A4
          3479766E94978F8342715964CC8FE18745C3668E8A8A6431CAE85504DD94A631
          9229398A6C18C5C0D7B0E1E8880867F41EA3B67DF1B6C19AA4B6893AA65BB02C
          63B9165B2AA2EB2E282936A84A2A8AA0442BE417FC5F2F9785D5C1B7B91627B8
          58F57E5785DC45C831DB26DB720DA4409011E4B66CB2F8136325961E44265E69
          CFC6D8F834E7CF289DD74F3FEEEE39CE7B76F7DFB86C1D7BED0EB976239001C8
          FB0EC1F5F5DF1DF7C086C15D75DDE73F30F7DFE81D033B8E3F211EF5E3ACE3AE
          E2190C8808F5C7A888FF002FBF61AE8DC779F3CFEABF4F3F2FD7EBEFFBFDB2BC
          3808283F87CA7E5F5E3FD3FCFF005D6F5DC0EE39FA88751DBD7219FA07B066BC
          75D6CE47B74DFB7B7FA8FF0031AD165C4407BFF0DBB6FDBBE73D7E9D3CD515CF
          51DBAFD433FC83B00FDFA6D42E39F24FD38F6FA7BF1FB97E9E3DF94D5C71A320
          A0F8F6FE69CFD3F8FE9EFE13B8C54CC930111C65F35C7DDC27DF7DFA7B578DE2
          06611BD340C31B83F9B10DFB8CB5AC18FA0E007391EBD715DC8F309A4E3F1BFF
          009E698E9B8FC4278C0E3D760FF4DABABE20C92A95F1A060AA674F2F664439CA
          628887E2F6B86C021BE36CE33D703B8647396D72F3846E3AFCFBB1DFE0F4DFAF
          3FC75AAAEBD9BEDEA97A291F97A5BC6ABFFC76289F3FDDFEBE75E71CFBEDD7DF
          A7BF7CF5E95D6388E7AFA0FD4723F7CF41F4F5F41DC6367239DF7F4DB1FA00C7
          5DFD6B844723EDDBD83F5DAA3FAAF3FEFF00DFF3FE1ADBF00F09CFE49C7E9E15
          3F7EB688E3F87F1DAB88C3B80863D476EFF410DFAEC3D771F6ADC23D07D7E9B8
          64721E990FE422039DF3C4223D719EE3FDBDFDB60F7AE355023C79F9F94FFEBC
          7FE17F2FD34CFE7BE7019C60036DC7F96323E981AD8218EDB800F5C60447A8FD
          03E9E99C62B51DFA877DF7ED9C647EDB75EC3D3035A08EE3D3F875EB8DB023B8
          06E38C6E0219A6BEF534F46F5BF56B87DBE62F523462FEB8F4F2F3893819BCCD
          BAFD46A65D1367CC6126CC79D8CC45B829848EA2E55B3C62E4A26059B9F00219
          70F00DFE210D3BD49087D35E3419C6E965F0A792C59EB04322A974DAE15C4891
          1352E88D202CEAC57CBA80A8ACF5233DB644C2075148344013AC3247F3C07AFB
          FB8076EC1F5F50A7DBD43AF7FF00DBF5DAAF3C3F70325C264FA94F3398866872
          AAA5773D5D25555395267B9159795138F888E4D3FE0454CC108162F7521D1FEC
          7F5474AB0B7231906F238B18A3D1E7D43E8D6E6547FB4ADB6CD9A32E85957819
          11AD3DCB16159DC6E3AD4666528490FAC3444C445C116C26E06523A6E1A51B24
          F6325E21F36928B9166B940E8BB60FD92AB3476D962081925DBAAA24A1440C43
          886F5E8D7CE1F81DF149E287817936CC2C6B906F6D2751D79F33A397B3A76FAD
          35CAA093E217B71C732AFECC9450130107B066234594F9E4A3244BFB3ACD3F81
          BF150E1778E78D6917675C45B0B574AD45697D1BBD9E35657390E97960E16B5D
          E88A31F7A469054298AE2187F10452CA923131F810A98D82EEF6339A2350C9C4
          A7BC24445AB98E8F6C83E3CFDDD2D501B969CFB34A2D4A44E57D0501F517CD5F
          55DF6746F874C873F226219EE4ED5B266E379E63305F576A22F2BD9FD31A0139
          3331E214E10EC01D9F404A4D8FDECDC9752205CA694A5657D6BF34A5294D34A5
          3A75AB727189C70DBDA1CD55B7ADD7ED9C5C46051372B14C06F863720E134843
          200701CF39FA808001471B8B4D565DFDABD6469DB55D49C986857A9A473A71C9
          AC41707314A220530647CB011C00E7E60CE796AC6FC6171A9AA173337D1768A2
          11F0A90AE2076AE144CA08814DFB454E4200089481CC2639BE50C888E3A5A635
          B78D7977B3139A817FDDAB455A30C571212AF1570E040ADCAA1041341129C0CE
          1D38389506AD93013AEBA89A440E63642D33AD9C63EB4F194D56B7601C4868A7
          0E1CC0D528A8A58C96A16A8B34951E77972CE9444F1F0EF390A05888DF2D8090
          0C9AE32E601725B532BCCA9B1087F1566F72E9A2FC3C46D515F7CBCA22227951
          15545FC6A8BE04D444900F8905D3EF4D5B99D47E4C94182567F628CE0FDF1904
          C450ABAA63905709C7391479F06CC09181301EE718079E656431EA473AF7C70C
          EDC173C8595A62457547509351541FBD41F1CF655ACA0AA2928B4C4CA6706EED
          46C703F33468B0104E414D47455405035365B9A5ABC9CF93503586794D47BF04
          4CAB6F8D2E2D7B6F9D4054AD6DF86102B62950E52148E1640A3928285408AFED
          8D1158F6FDA967B78E84878E421E108E5B15E832290AE144855215C3959CA855
          1572EC53139856702A9B9B01FBA0001770E3F380363C33F87E5B7C65E984CCEC
          DBA8AD4AD3D69A8AC668C84A458E98DE665E1D5978B2B2651EB3174DEE390B69
          223C5967480B5905799128802811C6CB22CCF73E3DCC8AA723C4A6A516DD935C
          12959904CBA2E936E108A113EAA2CBBCAF7702A242ABDA483ADD5E11B33D3274
          2175B6951B851ADF22DC9DD09126BA8F317E886CAA1AB282ED6B32E134FB8EB2
          D5508BF6907D2156549E07197BD257D937F56E8FC4C403F7BA060003A63B0006
          7D0036EF5B0F27FF007FF71EFB88EDFCC07BF5A80D29322C926B24A82892C422
          89A85DCA622850310C51E6C0818A2060C67218ED590EF877F865E90F125A52EA
          6B5322E58B724D5AB26AC449A33330C538A949160E061647E119BD41B2C31EA2
          ED1D19059155258C90916495218E5363FC4712B9CD664B8552515B38519253EE
          4E71E699115701A06C49966412BC64AA422A028A2DB8AA69C222CC3EA2FA87DB
          5E97F1CC7726DC463209B1727BB5A3AA878BC2AEB0B071E6E0BB3E44D759B1B5
          A9646BA2B4D36DC87DB90F380F4B88D8C734714C2C287921CFEF0E3B6FF4CFDB
          B7D7BF6AE92923D7E60FAE43F2DF7C8EF9C08EE0201523F4F266EC45BDDB695F
          EE015BEF4E6FDBBF4FEED0145368A125EDA9770C5C90EDD24D14D2148E4337E5
          2A64DDB8E4BCE06ABA5F87B69D697DF57E5C133AC3091939645B91AA4848A52C
          902AC1B318C6EBC9CC3E54A26294C56EC50E63F3089409CF9EB90A7A5C66CEF7
          251C5D8718626AB92C1D90FAB891D9186CB8F38EB8A2DAB880A2DA207008AA4E
          022A222AAA771B9DBEB83ED46C7BDBF16712CEDF1948B8FBF5F575630D6EAD24
          6456302B2140880FCA6E22CB69D98AECA1596ADB6CC594606E7A6287428AC867
          7CE43D47001BE0400738FD76DF6EB2175CEDBABFE256F2110EE440A644129A55
          E26C8115700A9F998819715804A5F2CB829339E7100C00D3F593A82F354EFCD6
          8D506E5561ECBBAB522E25EC0B410016D096B5B2ACABD918E8A8A8D26126A8B0
          8D7B1B1A50214043E08E03F308D54CE9395BC85E283774822E91331787149CA4
          9AC9098BE5081853540C5E6288EC6C08867018CD29A04E8D9BD554575932CD88
          5BC48D16C81AF558664938DFA6F232EA72E0B44A9DC069E545478E35F3B9796E
          2975D2DE7FB899A611676586C8DB8C86E6FB08916235D696748C4491F1558B69
          048D21BB35868BD2951DC2EC0701C12E57C53C3DB72E29BD6991D619F7B06DDC
          485BC8C1290B08DE43C84C1B3566D12700E9FAE750C264DA01D52890004E7F93
          94A18A8ED574239C0FF3C671B88F4FBFBF6EF5C1C54269C271CAEA1A30A11F0E
          3A3763BC08A661F0B1C0E9CDB314AAEE41923E5B6F3D6504CA2AB0240739CC63
          1CC23CC35E3AAE07AE718F7C760DC338EFB8EC15F96E133670B289F1AE2C02D2
          781923D342384517891E75157D105ED1522122F1F544E3844D57F4716982E4FB
          1388DCEDCE1F2B04C4E530C1566332EE645F3B5CD9565638DB4B652505D745B6
          1C618445F9B2A7EE6BAF59F24F59B1249BB6AE5AC6A8722649074DD541899454
          82A24991DAA42B739D4214E7210AA098C5298C40100110F2DBA4F24562B78F6A
          E1F2EA000911668A8E5539444039808894C612EE002600E50C8647D72F3BDF87
          ED05F108F053D25D20D40D734387DB7F45F4D34EB556F1D491848E9D656F2BA5
          B63CD20FDC4FC5B87F12A3B871424DDAEF9368FDBCA1964104DA0ACA9C11571D
          7B5F8E2E12ADCB023B46B875E1CB5027EDEB7D04E26EBE26AF79D82B6EE6D519
          D603E5AD3B1FA7E0C261CC15BEB8153561ADB56ED3AD16C8E8124C49262E79AE
          F95B4F0EB16966DD65912B696CABA3CF913E4C54179B72436C18C28D15A94FB8
          E9AABAA1F10F7A0C0A0292091F0D2C70C7BED0AC9738FF0089B8CED974F190E7
          3BA1846656F89D4E254F7E4F56CE89533ACA1BD92DD641368AAE1D7C600AF47F
          EE5AF5B4B475D90DB0A71E2FA962DD1A5C109376D3741E5C518F219A3958ADD0
          712089DAB73AEA63CB43CD5709956504C52A4918C5514308153298DB54347539
          B39E9FC8037EDD7EF9E9EA3B5D8F40E1B4FF008929656C4966E595B56F463276
          B4F47BE6E5070D4924D4512AA6444C604DDB53289BE60E5354791C208AC8AA55
          12C92C89A78E25A355BD6C29B76A3E91D38BC26AD133C5B02B2E8453F74C5315
          4472263A6AB3729944C63181204C8263016BABDC2DB7818BD5D6DFD05DADDD35
          88A28BE68C9127E200F55B7A3AA34EB644E00F6FA626D977092AA82EAFBE8E3A
          DACBB7DB3BCDB68777B6BC76BB73B0C90A2FD6472B369B24F4644958536B6E00
          A7419CCC78526423DF14FC594C232EB22D8486F9A81D3C6A949EA158B1ABE3C8
          90BCAD864B07AA4EA718A0A6C3D72538FF002F5CDD37FC4CBC39D9BA01AC1C13
          059C52A6DAEE43525C394BCA2A46229117169CA6411294440404AFC400407B60
          43A55A86C4932C45F3664BA9B922EECB7644F9E8256530CDC9C31FFA521C8637
          FAD66DDADBC4A787D712DC3A5F7AF7C67E87585AA36E70D16ECADF312B5C36E2
          12170B17445638CDE0ED49413379360FEEB9D460E208C9B3D45848BB559A7248
          AC814FCB75ECA569DBE2DB85006433189E2A32590FF7232D8B2960F19B8A22A4
          8282DAF2A82BC7BF0A89A8FDF6A0E6AC6DCEFD74779648A9B1BC6EB83749A1A9
          A9F416CA7BD6258556C76218C871A64DF37E53680DB8E368E2FE1EE15545D618
          503A0DAA170C333B8D2B71C45C048A60BC6CB4E73C6359240C2205731E455333
          974D4E203E5BA49133557AA6A9833503EA258B75695BC826F7C43BB87637582E
          16A4FA89194B72E455B014CED9C54C143E11492685390CE229C1DB4915339162
          B5324729C6A4F53BC51F5DF8A9BD256F36FA67A43A51A45F1A763675830D072C
          F64DA42321F8664DD59E0996289946A8249207599C5318F29C874594522D9328
          55D3F87B89D28F122F0F3E31B86E9684422F5AF4A34A65F5AB4E4AE44EE16677
          458C92D356FDC76CC9A2541CA493C7AD4D685C2CCFCA6231B8808A83945D9144
          A8E3E05B5B662F6375593DAB99932CBC4D3925A46614E9719A275D8EC33F0CAC
          889081F647299F143DAA846642689715C756BD7C60EFD66F6E7DB178142E9A6C
          6CEB1A9F0E9A72D8E4D8BE3D77611EBEBADED6C52EC6C9D71A764C7F89B8671A
          5A0791E171A8CC30F477171EA11DB7FB8FEBF8FAEDDF399F313C3CDDB31C316A
          A7152DE66DB46C6D269A1829B825DDBD2DD724ECACE0DE99488669B03C7A8DBC
          A9E6A5155DC9B5302892E00410294C6A3ED2DB99C5D368317AF0FE6C8335158C
          7AA9B0632CB3604C5370700E5C1D66EAA275071F3282736E1D2E99C356A83AB5
          740F512D776621ED771724A5C534DD649270D9541BDB7164742B36580C92C506
          AC0044142890D80E9562EDF61F0EFB353C6AEFBD1A6E35AFA86CBA4DA0BF123B
          84CBDDC9DA44D23882E28F23DEDF8551E7C4B5EB13A90C93693A5E8DBE1B5BF0
          CED84DBDDBE4871EC60B3315EAAC86E2233675CAC3A8EB4CD83914DE822F763C
          916572E003AA088B49DC306915C1C4FA8C5C5B40A4243B97276AABE906E0E57F
          3C8E946E66ED5AB7580AE15E520AA7FF00309A2894E981CFCE702548D95B8221
          0D58D48D36845569B6160DC93B0485D89A69B767349424B2B106760C8545146D
          F12E1158CD808BBA4D645315814294E50A8E741B8CAD728ABB2E6B974ED4B3AC
          5B3127128D22AD86165C30B76EA4B79EA7C4A4BA49B77294B374D623D59F9171
          03BF3A6606FE50092A5159762B5B34F32AA4FDD4A389876472B3B7C5219E7290
          A73722CB94445750CBACBACA2C2520A865004C51317986BB2F85B774B56ED454
          0CC9D92B321C3916123E29B6445D79B28F1E3362F9C6F4588A64444F07C41988
          AB86844AD376974E793759FB9D9ED7EE36E33F8CE2DB23695302353E214C38FC
          F9EEBD06BE5356F6F7331FA98F76963677D15B69A0AC963511233EF3715871A8
          E1365475F9FE7F9FE7BD5E8F83EF051E227896D2636BDDDEE92D2BD3078C7F12
          B4993E62B3CBEEFC8D0454502621E154F21B455BEA0953F8596955C56904CC67
          11F14E5A011D296C6E1DED986BB359AC18DB919A5236D379F652B70C7AC02284
          84445AE9BC751CB86044CDE47CB4D8AE51C6517070C808D545F8A0F8A1F109C6
          8712B25A0D636A1DCBA55C34E88912B41A593A673B2169C5DD7296AB64A2A7A7
          EE22412B1C47E55668EE21ADF8A5C568A84856ADD564D41E28E153738062D407
          516199E5EDBF269604A5870AB1978E3AD8CB65809324E4BEDAA3A3163346C8A0
          305DF21F785B5241026DDFCFABBDFCDDE8DB8B87F4CBD394EACA1DCECC68DBC9
          B25CEACEBE3DBFF4371DB0B476928E353564C172BDFBEBB9F12C5E764D9B671E
          A69EBDC960D13D2999506AB24BC3FB401AC83FD3594BB7502CEBED5033688BBE
          4158F99856B2453F224696B6D38B8B51DC7AAAE08E53672CC5DA69098E82E272
          814D69ED53B3B53B85AD777FA45A8445ECDD49B6BE06E5B3EE7B6E4DE2315775
          BCE8CA2F6FDF1624EA5F06FC8DDE1105144C83E448C6BE6AF18AC2578C5C149C
          F666ADEA358CE235589BC2E378DE35C22E12633D37293ED54F2443F66709776F
          14215400103991512500444C5381B035752F17AB7A2F5C3C303836E36A21924C
          6F4D3BD4D67A6EE654A63FC79ADABFEDF9D973C3AEA94F870DE02F2B114F8329
          C045319792314401DAA51BCFE076E371A9AD8F16A27B15C82863B73100550424
          C4F541A57B965E701DF41D36FD6336DB7C7B8504CD4D0DB8C0394F5A7D16EE66
          DF31BF9BAB5FBF9B3FBB9732F197A5BAD9BB269722182ECF1AC51B1AE8B32BCA
          CE0B12C6B988B2E6553CAC3E6F311D23B91E5D6FF86EF8FA5FB684F5ABA33C6C
          4905E7A7EF966F0919AEAA904B7959CA2CA268B2717F020414AECB752130A6FE
          701246E28E4441F3C5669249521331764F59C93369231CEDB3F8F7ED5BBD62F9
          92E93A66F59BA488BB576D1CA063A2E1B3941422C82E89CE92C91C8A266310C0
          23F25E86902CB44464A14000B20C1A3D0287428B9413584A19FF00A44E25FB57
          D027C0675FA6B5C3805B621EE6915652E0D12BBE7B48CEEDC0A8774ADBD16D22
          A7ED22AEB28631961630570B68544E2611F878948A6C086F74EC6EE25CD9CF93
          8764329C9CF4788EC8AC9924BD4961F064DB72604878954E42201FACC38EA93A
          D8B4E813860AD0B5823ED54E8D36D706C468BA94D9CA3858AD7DC5EC1A8CEB1A
          A56062E3D217228D22652E59535CDA0C6A671C92C7DDB6B0A1035025B9615D2D
          8891A4B760ECEBD0D294A937AD17EA4F6BB6A1A3A67A6D70DC673F2B84D92E8B
          4DF020B1D23E4E0390C090B9C086E061010E9585DF11BAA521A937FCC49B972E
          14299E287289D5318044C26E81CC21EC1B0E3223DEB2EAE356D7757368ACDA6D
          44DCCD5358C729004444AAA260036C038028900047DF7ED585F5F716B43DD12A
          C5601E76EE4E41E60C0E73B64319F41C75E98C8E29A6AD6DC644F39BD351F4DB
          43D172A12105135F579228A8720BA6EDD572846B45F1821C852337A62944A21E
          73D6EA87CE8A621DA6C745B228B66E44D141BA49A08A2994089A4924402269A6
          52FCA5210850294A5D80A180C0F48735D18AD1DC5911FBA4C4119AD286678A54
          4000873B39551BBB4931C8E4E9F9263980072053E762880D7641C63D7F4239F5
          F6FF005ED506F7AACA5C9CDE6C67C891A860DB71DB5FD9405FC28628BEC86220
          ABC7855422F725E7D577D98584E3F49D2CE2F7352CB053F25952A65BCB040579
          C901E9B8519E7113B94A3487E4A2092A90B64D36ABDAD020C480EB71C8FF001F
          EE1B6FFCFED999F0A29C1F885F82D5FDC3F4B3845C5D2FB48F50B445E1565DB7
          C427795AF1E673A752A60380879A8AA95952E53A84E732C998C06E62F306155F
          138D807F580F4C874CFF001DFD2F25E133C5048695BDD5CD2C3C919B33BAA3A2
          EF7844B26280CCDBCA045CCA44381F94AA3D887EC97317972624367382603EB6
          5AF020662D56493448591C29350F0AAF00AF187AD1578F6570DD696307B2FF00
          6924E3CAEBF2FB4F36B24E5BD34CBCE6958E727D96C969371EADE69B529035D1
          640D65FB62489DC11E3C19C1792917C2A513644A9E9A2A5A0386087B835496D3
          3D38066F3FDAD91BA2374D642394261EB6996B329DBEE515D20C188B374CA0B3
          80FDE2001F3D33599BF0B1C7870FDC36F165A77E1C772DBD7833D54BEEC26772
          C05D4C5A452D65C73D7294EB88CB42E0515926D2F1920FE1A008EE3DE211EFDA
          2EE24E358280D8EE1358F6B6F0D5E02750D9F8BDDEDA8D33A5D3E870D2E266F8
          E23EC2BD88C555EC77D3972432292D6B164D3055AB49586BC6EF4944619D9907
          6B358655DB54946A98AA5B457107C4A3BBD3C5F78A0E2AED278A9986996BDB1B
          634F1C207F28A482D21978FB5A3C8C0086C26D24195904722521848A92595373
          0956111C954FEB6D557653712194076665B1A0B62EB7C2BD511101D70D842ED2
          50372C8DB030E5155A424FD94E60FEE4AD7F5FF9A6C26DBD359ACA878DF4EF73
          94CB91026A28D6EE36426E408916D4DB575B190C43C2589B2A348443019C4C97
          69385DBDFF00108B598E9578A4F18F6D458366D6FEA7DCCD75B2DF6E82408A20
          BDE091242E42A44029499FF695ECD14C04294A3F0FDC4A235CF7A6A92DA0BC18
          EAF483172AB3B9351ED54EC088513581B392AFA88E15632AA2262E141337B38B
          2EA072081CA648A226000DE34F107B76E2D7DE27EC0D73B05A25251C56CA5BB7
          5185CB66EF42D69B5927B152099573A60F528F0989070E1344EA384D32266492
          50A5100A1AE35668B726A0E8E68A31500F1AC142DD938D4993091300518B022C
          1B10BE4453197E401DF95F1473839407B7914898BE43B839938DAB70DEA665AA
          B91DAA8DBC56C6AE4A38C5C7691448D11BEE515554F5153C2AEB1E54EE9B9BF1
          B3BD1EF4D50E504CC96B3736C67E7953EA09CDAF8FB79198854112ED84257986
          EFEE7219A8DB4E880B8509A34E5045521CD2D860B5B4FEDA8914C137011C9BD7
          B8CE7E364445EB8E6E6EA64CCB82223B7CA914003019AA96D135C4F7E35289BF
          FC73F10DF38F913FA7D4371CE3BF4191E2AE0003200001800000E98E801E98FB
          636E98A9BBA14A01B509987AC7C874EA3FB320E7A67DFD47D76DE3AE0928E56E
          163321C5E4E464905D35E5553B9D9826BC73C78452544FDC89ADCDF561411687
          A38DF2A684083169F64B2AAF8C88889C31031C763B6AA89FE2506908BC793555
          5555F78478CBCA5C799C4071CDA21A7F8CF5FF00965806FEA222511C6F506995
          0DF2223EDFDBB74F4ED9C631519F1B65F2B8F0487001CFA1B6018473D7FE1D40
          B9D84303926301B636DF3BCBC31C03A8E47A7AFDBF5E83DEBB2DE1551CEED939
          5FDB25E13F3932179FDDE3F4F96AC9FB371BF53A4FDBE2E3FE4471FF002A3A3F
          E7F3F09CEAF31A8F764B5ADE085C452B1AE564549FB6F4AED570A10C60118C9E
          D56B21AC92002039224E5882CD5626400E8AA748C0253894D62DD356C8B0B02D
          44522949CD0ACDC9C0A005E659E260E9638F4C98CA2C63088F5CF5C06D7ABD6C
          281BC0E3598E21BF9BA2239C8F50D5AB3BFB00F6C603BEF657B20C23665A9911
          FF0097A203D7FF00B147AFEB6EF8ABBB7A5D3FBAB086FB8BB3FA3F4E4A9CF8EE
          486EF0BC7D7822F3E7F8EA397D9890A3FF00C42EA9652343F123BC3B8ACA3882
          9DDE91E495EA41DCA9CF6AAB0D78E513C73ABAF78612C7535C4ADC4C3E583986
          580B9DB9FCC7A9888074FDD100EF9C063D6AD1CD8DCBAE9C4F973B06B4DEDB74
          C8FF00B637506DF6EA19FB55DB3C2E3E6D7B00C86C68711C887FFB2EB61EDDB7
          EC1BE47B55A452DB5DB89FF41D6BBDF18C8F4BC6EBF6DF610F7D8476DF3F8DA9
          296C4631CAAAF6CAB514E7CF08991A709CFC913E9FCB5DAEDFB0DB3F6B36FAA3
          4023EAE3FB7AF388028884E1ECB8A112A27B91768AAAAF955F2BA991103FEF78
          AFFF00A4CBEF97496DDB223D36C66AE73C5EAB2C9F8647101F0265CAD96BEB49
          5197F2C4E0518D1BF2114295502E43CA191247018A6C14C612736470156E4D3A
          8D2CD6A0D890C710024BDE56BC61F3D8AFE699351CE76C7ED7D31D430359EDCF
          786670FD7870E9AE5C20DF375C33497D6CB05D94A564FA254BAAD849BBD2BDB6
          EFC8D84767076E5080BAA0DABF4D716E564E8F18EA38EED3132C29DC3D3FC639
          9419F4469451D9415B1DA522ED1571C8B6A202AABE1114951157E8ABF4D61DFB
          602EA3637BB5D21E4139B71D81452B36B69E8D364EB810A15E6DF3F25D101155
          226990270138F262288BE5175F3BEB052490B26D64D2E5F2C20E3D4C940373AA
          DC8B2A6C806E2650E730F5DC47AD5EC3C11274F09C62DD6A2EA72423BE197882
          4EE6138895B841B1B37F1A58CEF97E5F208F22998889C0C529F940039B15425A
          B3C04F10FC274CBFD34BDE3E12F6676CBF7B17097C69F4892621AE3856CF154E
          35F9A2D6042E08576AB5F2C1CC7C8C7141B1BE449DBB280AC6A85D3AB9A3F810
          E0EB8A4E20AF7926F0FADDC40E935C3C2F70E36491548F72364B528ED1B6A66A
          23D440E62C7B4B7ED16CBA4C8554C552BB74DD2501251FB429B1FE3989E4B57B
          891E4CFA9B0891E9ED5CB69B3644579A8890E3BA7210C6498232E7C67E18F1C4
          0CD5E79E16C5157BB8983BD1D42EC7675D195B52629B8786E456FB8FB7D036F3
          18C629EF2B675F96497306253930744C483B38838CA93F6F6EF498ACB75D5D58
          F4C74843D1F52CC7C3F980D037089404888CE73244DC409CCC90010C80F5028A
          603D7600C6E3BDC0ADC78A31E1975BDDA7F29D381BC80A60D8404F6A269E437D
          F75301F5DBBD515E91402D01653123A4C527726AAB2CB90C025390AE8A995B10
          E0381037C2228984A3B809CC510CF4AC6643CBC2AEB89831FF00C92EDF5C7FCB
          8D833BF5D87A643BEC219AEDB6EE4372774ED643248A095590A010AF82566BFD
          15345F09C2902921794545E51551758F7ACEA59749D02EDF5359B46DCA3CF367
          CE44775145C686CF316EC858744B85171A664834E012770189092210AF1441A1
          291496389C000056989050C380C88108D920C8FAFECF019C7A67035397F5FAFB
          F4A945A1E18B05B0E3F7A4A4873BED858A1EB8EDD07D87BD4DEDBB7F1C7AFF00
          A63EF9AC45929295FDBAAF9E2C2471E7CFE073B5113C7E5C7CFF00E9F1F2D8B6
          C8B62D6D0EDC88A2222E234CE7089C7E276203A4BFAA91AAAAFCD579D55E7043
          67CADF9AED176B41B633B9592877E0D512009943022EA3D458480529CC225440
          E730007EE14C23B00855196AB6935D9A07C68F12DA51A8118E61EEA88BD6E678
          9B7769A8899E43CD4E8DC50F22DFCD210576D2709311920DD627C87495110C88
          0F2D57F065C45BBE14389AD23D794638669858F73A0BDC9061E581A6AD393456
          89B9A392F34A6481DAB0CF5DA91E63801139145A2861294822197AF1C1A19E1A
          1E23965D91C42211ADEF2D4F7B6EB569666AD6974EAD6D5EAD62FCB54E9DB379
          11AA6E98CC9E19576B3656DFBB62245CC3A8759BB6335218A359BF6FAB5ACEB6
          DAD30E8332345BFAEB1953598F24D5B0930E6371481D4511371406547565D210
          24693D3473B55E6D75AAFEB0B379DD29F5B580F525956357979B4999E15438B5
          9DBD245094F536478CCEBB6E4C0217DD8F185F7A8AD82C20467E5463B0359651
          55DFBB6588E0D75783F1269E57473C0B382CD11B9122B3BC75B75A12D4B8E887
          83E4C9A1675A30B7BCC2F27F08712AC540EE350ED66C0A28981799D99328F310
          F8AB980F0CAE0B78508D5F894E37B5C9CDB3A2F6A2A79886B1EE67316D6E1D44
          771CA0B8696DC3DBB18812E2BD5EBF551235FC2E0D9B64571544B20AA0C45639
          6C15C76717D76F89A71441AB6EADB56C1E1FF4D635BD87A19A706C0236DE9FC2
          AE2AB46CB95030325EE3B91E7FBE6EB74C48464818EDE1DA8A8DA359A86E31FC
          625ED6D7E476D94BF0A34FB1AD1A8875D1E5B52DD48C7258952A4BAAD2936DFA
          CB1598F0DBEF275C371D3306C1B555FBDDFDF2C73AF6CC765B6FB60EA725BBC5
          30CCD4B70B25CCEE682C282B52EE2D1D9D0D1D256B7600D4C9435897D657192C
          F28ED4084C4284C47912DE9C029256CF66A47DA96E3257655BC2C6A6A80E404A
          A035484E5101DFE4308977F4ACEFBFC395A6B2B677035735E92882EDD2D56D6A
          BA6E08422C928902F096F425B96791EA227C0288AF2F0B32891420721BE14702
          6EB589E7031C0AEB1F1D5ABB13A7BA710EF19DA6C9EB35751B521C3453FD9AB0
          ADE1381DD3B78ECC0541CCCB86E53A7090481CEFA45D993C249B32397487D1D3
          46B49ACDD08D2AB034774FA3FF000CB374E2D78AB5605A88F32C66918DCA91DE
          3B53FF00AD2124E7CF91917038170FDD385C4005410AAED80C5EC26641619ACB
          60D9AF699971E13A624232E74E707D758EABE1C663308E03AE27E1F55E6DB155
          26DC10B53ED7CDF5C3F1ADA1C47A62A0B58B6797D8D9E3B7393C28CF34FBB8F6
          298BC3716A5BB616C8D62595EDA2D7CB8314D51FFBBEBA549780189505C91332
          94A54BBD79D1D78F3F0ACEE28691857E99546B22D556CA14C5E600F3082529F1
          EA4308183E98EF58B371D3C17DCF66DE5393F1514E1662E1755CA0B364143A27
          4C404C530090008218ED9D843DB35958D4337559F6EDEB16BC3DC918DA4992E4
          39048B26531D3E70C099238808907E9B0F701A69AF9BAF145A1D72DC2C60AEE8
          18A58D7C69D3C76E99B10295156E1817A98213D6FA475301F16BA242BA8C2A87
          290CF5B82192FC509CB478CE408F10238479C0A7010326B2674974152089556E
          E10540AA37708A80645C3754A555054864D4281CA215F405E21FC34ACFBAE39F
          48D8ED9107A26154188A29916111131BF66252094F8D8301811FFA76CD6379C5
          1F8774B405C124A3AB5DCC44918E63292F16DCC9ACA98A222077A8151F21E9B0
          181516282FCA3CA0E0A5297183B75F6A1CCC8DBBAA475962F1867D17A3C82F4D
          8B1643CB63EB222FA125B4EE16C8D3D2710905C26913D4D6D53ECFAFB41A1F4D
          11E5ED86E957DA5B6D5DB59158D75BD3B692EDB0AB394A2135E5AD23696D28E6
          2A03D3188CE258C371B37E1313D5CF835B23F9C3FA00DFA7F7FEFDA3DD33BD5D
          D897CDB973B458E89A3DF948E4487327CEC1E10EC9FA6712FEF14CD1C2DF28E4
          BCC0530E04A035E9EA0E8B5F7A76B38348C62EFA2913897F166682C2926018FF
          00E2DB9CBE7B41C0FCC6381D00128802E71EB28415FE81BFBE7DF3B6F9CF600F
          A8C45950AFB0EBA8DF78409557695D2A3CC602407672EC6781D69E65C4556A43
          3EA00A8BCC9B8D1F0BDA6BC2EBD16D0E53B4BD4A6D8DD2E1F955167B81669436
          B8DDB3F4D342428C2BCAD7A04FAEB2886813AA2C5224B711EAFB38D127C75245
          76382F6EB30BD09F10067C37F08DACB7ACCB848CA591A4379DC90E2438197527
          E32DB7EBDB68A22060E633A9A160DCA501C9BCEF97238AC29344D1781669E724
          D551CCB5D9332B7148BC58C2770ED778E4C98B85CE381328B8A0770630EE615C
          4D91C8623C9868FE7D8BD8A9BBBEFF009484900326F209E5FB76A906BA02A155
          068A4404B1581DA267290526C76E292609A7C85002171CD1CD19C4B069191E81
          1B3162826D9B205318C549148A052172731CE61DB73098C63644C61C8E6B256E
          86E943CEE25744AF873210463F51F1924D289912724824D992AA77A35C210F3F
          8117C73C2422E83BA09C9BA51C8F31C872FC971AC9E4DEC548556F5185836EC6
          65A70118292DCE86D201FA0E4F13565F345597D9DAA8DFA85709D2CBA1097B0E
          1567AF114D78E4061973B97099043F0EC20DB2650E5C80B2F87C677F7001DAD7
          033BFF00897C446ACEA27982E2362DF9ED88054C21E5FC232316310511E5110E
          555A4628E7E53087FBC0C22383EF174ADBF0338B24B4B4533915124FCB4CCE91
          057949913728147251F98C61DCBD47610000AE68C89898440EDA1E3594620AA9
          E72A9326C936228AF2949E628548A1CE6E4294BCC6C88000000E36AA7C87765D
          BEC2AAF15582EB1261C3871654F2900E04A58AC8475715B4002053644D0C488F
          B89D55E5384E7B6D9BFB3CABF69FAA2CE77F9BCAAB6CE9325C8F23BEA3C4DBA8
          7E348C746FEC64DCA42498721F624B70ECDD8C51CDB6A3284782D020AAB8481E
          F09F7C67B88EC3D047AEF9DBBF7F5F5DE61E965DF6D59977B79CBAE6A3E021D1
          66F5056424972B76C459C265220909CDB0A8A9B2522605113183019EA12BB9C7
          D7A67EDF7EBB7B88F6CEDD78CE243860E05380E07025010DB701C086007A63DF
          7AC674172743775574DB2125CAB9D1A703066A00E946741D46CCC45545094551
          54509539F65E3539F7736DE36ED6D767FB6132C5FA68B9EE27778AC8B68CC372
          5FAE66EA0BB09C98CC674DB6DE7181795C068DC11324ED2244E78F53897D49B6
          355F8C54EF1B2661A5C76EA7A4F6AC084C469553B017B130E46CEDB156512214
          CAA0A7C8712E4047F744C15E798FD771C7701DFA8FFAF4FD0570F3006C00001F
          F6E03A6DE9D360DB1D003B6D5B44C23D47F5EDF60FEB5599764CEE597726E9D8
          8109C93CA930DBA4E80AAB86E2A89100171C9F088A8BC71CF2ABED6CF4EBB1F0
          BA7BDAEA5DB081904AC9A1D22A247B59905AAE92F3630E143017A3B3224B5DE8
          3090C8C0C0495C5446C10539AB7D5DE2EACF9EF0F5BE384E80657A485F73AA69
          F240D91B4A60B062ADA979C1CFC8084D288918A8991BC6AA28AA919422EA010A
          437CC025A25B45BB8676ADBAD1D2276EE5B42C6A0E1054A245115926891144CE
          51DCA721C0C5300F4300D44390E9DFF5EBF9FD37EE15B79BB0EDD7EBF5F6F4C6
          E39DB6C6F5994E6D619646A98D3A345606A21C684C1C7F5509C6A2B2AC813A8E
          19A29922A9128F6A2AF1C22226ADAD82E96B0FE9EEEF70EEF15BCC82D9DDC6C9
          2E729B68F74500DA8761796016128202C48919C18CD9836DB20F2BA4208AA6E1
          1172950DC3AF1181C37DE06BB02CFB82EF51CFC2811081770ECCED4591D55399
          CA92EFD9140AB79D84C51054C514CFCE05012E68EADB6738ADEBAAB774C479A3
          0B7E5EB2D74326CA3C68ED749196969793322E4ED555532AC88482699C40DCA7
          38184822019A8F04DF900E703EB91CE76EFBE4338DFAF6AD823E819DFA076FD7
          E86A99ECC6DDFC5E1E22E2445A982E3EE30A8C924A429133E39CEE791CED2457
          9384456D780FC29E7F16BBFABE9AB6EEA77E723EA322B97E3B8394C4AB856ED9
          D932E50BAC53E3638B4146EB8A12BAC9856A779937307D494BEA9721FD5EA21B
          52E371685D36D5DAD5A24F9D5B170435C4DD8ACB19045EAF0B22DA4D168B2E52
          2C745270AB62A4A2C54D43244389C1338940A3541C7671F9C4071CBC54688715
          0BC15BFA07766885A8C2D4B793D3DBCA7E5E40C8B1BAA72EA2C9FE2CE20E0C4A
          E0EEA6946CA343222D166C894AB82A9AAA12A8F0076111CFB867EB90000C087A
          8F5DBA8F51ADB900E9BE31B671D04770F6CE0701F5DB154F51955D5141B1ADAD
          9431E35A9C77258FA604E139111D460DA7553D464DAF59C51268857B95095791
          1E3BCDC7D80DAFDD8CAF0BCD339A13B8BDC058B78B8E385364B70DA897CE57B9
          6D1675709AC1B28F352B2236F3535878559471A444175C45AB6D4DE3835AEFE4
          FE3DDC3DA92F7328872BB969790966CCD776252819F0C4316A6294CA1FF6AA20
          9BE45329C4C090153C265A13B862AF5D4EBB1ADE9ACB771EEF908C44ADA1211B
          9146F6F42342A9E7159C7B25309A2D4AB08AA7413452339583CE78AB93866A35
          11CE3E9BFE79F5FA77CEDBD323BED801EB8E9900EDB0FD7FAF7AEEACF73F37B8
          AF1ADB0BD7DF8E2281CA36C34EB8228808AEBACB6D93869ECAE9F2FAAA912B8A
          444AB8B705E853A58DB6CBDFCDF0DDA9AAA9BA7A41CA0129D6D3EBA23AE3AAF1
          8C181633A5B10E22B9C28D6B08158020DB610C5A69B007FE900C63A63DC43A07
          4C76C740FB88E92521753DB6E5ECE67774943DA9703674DA6A158B185392448F
          5206EEFCC78F639D3F47CE6E5492E56EE1202813241039846839F7C6E21F7EBD
          36FAFDEB4FD7EB3EBFA0AB42BAD6C69E4ACCAB96FC292ADB8C2BD1CFD33F45E1
          4179B554F2A0E8F831545451F74544F323335C0B0DDC6A51C773AC6EAB29A319
          F06D06AEE230CA8836358F7AF5F345B25441930DFE1D8EEA2A134E2218AA2A22
          EA1CB56D96368C3A50B1CAB959B24B2EB828ECE99D613B8381CE022924917940
          7F74009900CE447AD447FAFD7F7A52A8DF7DD92F3B21E709D79E3575D33FDA37
          0C9088D5513DC95785E13DFCF1F3D77D57595F4B5D06A2AA2B506B6B62B30A0C
          36515198D1638236CB0DA1291203602823C92AF09E5574A8B2DCBFAFDB391728
          59D7D5E967A4F0C0776169DD73F6C99C2852814AA2A783906073A8528014A713
          09C0A0000201B542753CF87BE1B35AF8A6D438DD2FD0CB0E62F9BADF9D117046
          290231302C1454125266E59A7029C640C336C98EBBF9170890408649B9577229
          A07FDEB92C0A6476EA525958B8EFA51460FADF166E9F0282C0C65F548CB9E3B4
          3925E7C2709C275398B987B18CDC4BCF8B1C6F0F850DC9B7CFE5BF76FF004763
          408BC3CE49B52B745AE6E330A28E1392B86C0904B94544D53DDC7105BEE6D39D
          BD5EDC37ECF8F2A28C95E9725C578490944F923722F7049492C7219410E54726
          28984305111AC88BC37FC07753F8834EDED54E275BCC68B687A8563230F65A48
          045EA6DFF1A7E570995160E5B182C8B75E37E42849C937199748AFCF1B148A62
          9C885EE3C39FC11B4538482426A7EB4961F5B788040893D6CEDF31F3B4FF004E
          9F0E0E525A10AF8A6096976980285D734DFE248AF3A9111F14200AA97CFA95B8
          2EC94C9451EEB71A5C89EEA76BB1E81C96EC8105F0A2567255C2EF25E114A1C7
          3ECE513D77CD09C8E9E7F3AABFB51B1DA06AE36CBA30A0A5C520388ED7DC6EE4
          1C760D33D2405480DBC129861C7F41944EE06F23B98DF13F89C3A9AB8A4112DC
          E5668DE89E9570FB60C3698E8DD8F0360593049723285816646E455730002F21
          24E8DCEF25A59D98A077B2B26E1D3F767C197707E52804D3A52A4B30C331996A
          3C665A8EC3202D32C32D8B4CB4D8220836DB608200022888222882288888889A
          D1F5A5A59DDD8CEB8BAB19D6F6D6729E9D6569672DF9D6161364B84EC897366C
          A71D932A4BEE1138F3EFB86EB864A46444AABA5294AFD7541A5294A69A54017A
          E98593A80D546D73C1337C650829FC48A442B9294404030AF28E700238E601F4
          E951FD29A6AC79C537863B49A632D3362229394D415150669204F88210F91310
          E9728F30006C225E6288633E818E2F107E1DD3B6F3976E1286756E4AF9AA08AC
          8361FC39710138E5CB121084298C610132AD4C89B01F39151C633FDEBD6A4FEA
          6E8769FEAA46AAC6E3866BE71C0DC8F5045322E0630080F3E0001401CEF9C0FB
          D74D798FD2E4908ABEEEBA358C52E5445F0FEB19254E3D48EF0A8BD19D44F675
          871B7113C7771CA6B256D66F0EE6ECA64CC65FB599A5DE197ACA823B22AA576C
          5B061B3EF4877158F8BD5975008BF1140B5872E211706ACF7A0927CC6EF9D35B
          BF4F5E2ADAE18A553408A9924E45B81968F5840400395702079473643F64E0A8
          AA03B720ED997FCFEDFDFF0086DFC7FA866C9C4CF855385979795B559232D12B
          15638A056E472432460CF96BB7500C025FFA81428944739110C0D63A9C417015
          2B65BB78E211BAF06F08AABCF1CE5158D12A989CDF2A22043ACC4C3CA0000405
          5B80EC0927F30D45DCD7A7D9F0FD69F86C92B28E9DC6B4F34C1B9ED8A79ED892
          97B1896829CF6B6F24779051044E4B8BE77CDD317DB0B88E4BF77E27D4BD2338
          55C9A3515ADC7C662CA97894E757B5B17720A205956D8F3AE2F0AE4CAD5B8AD2
          74CDD763D3440FC36C613E71D71D7EFE9EE15B79C7F5FD7EBF6C76C77882E6B4
          AE4B3DE8B0B8629D46AC226045458822D9C9404C1E635745E641C1079447F667
          31CA001CE5288880435CC3B06FDF3DC7DF1EA39C80760C088863A473990A5D7C
          9761CF8B221CB60941E8D2997187DA34F7171A7444C57E69CA2729C2A728A9AD
          D363791E3D985257E4989DE5464B8F5AB0326B2EE8AC225AD54E8E5ECEC49D05
          D7E33E1CF22BE9B84A2624048862A29C991F5EB8FF004FE39FE35A08F7DC7F31
          FD7D6B8C4DF907E623EBBE701DC339DF1EF8D39BBF7C0E436DF2001B8F41F5C0
          86C3B62A9B5DE202FCFF00DFE5F97EBEC9EDEFE35BF983B6E3BEDEB8FA67EA1E
          DF9869CDFC7A74C074C67A7DC33FC042B8B391DBB76EB8EFD3B07B568238EDDB
          F41F98806D9FEA2D7DA027E7E7EBF2FF00C7FBE75BC47B7A76E98EB80DBB7D77
          EB810ADA26F7E9B88643F3FAFB8FE75B4476C875DB6C7BEE3B800FAF510DB7F7
          AD0470181F4F4CE43D47BE3A676F500CF506BED138D6A238EA1DB3DBD804073E
          E21BFA0FB6FA67E8390CFDBA86F80C875D871B7A0646B688F4C06738C750101E
          BB8ED9DC76DFD07BE443E9BE072203D447D43BF5DBB6C38C8677A69AD4440407
          7C760F5C6036C0676C86F9EDB075DB6EDB07F70EA1EBB740EB9C76CE0002B4F5
          C7FA74FAF5CF510F5C7D281BF7E9BFEB6100FBFB57CF3F97F24F655E79E38E11
          79F3EFFAF8D35AE4473B6DE81FACE3A880676FA0569F4F7F6FEBE9FE9BD3AEDB
          06FF00EBD47D03381CFDB35A7F7EDF97E635C7D79F97E89C7ECFB73C7288BC2A
          2FEEE3DB96B511FD7E847B0EC23D3A00ED5A74FD7F2FBFA5295C22FEA89E3E7E
          113844F2BE39F0A8A9F3E7F4F2D2953074B74A35235B2F785D37D26B2E7EFEBE
          2E15FC889B72DC62A3E7EE0404A0A2EA81708B362D80C53BC917AAB760CD21F3
          5D394530138662FE1C9E023A7DA26682D5DE301384D54D556CA369386D2E4792
          4B4D2C776901164149B39C3C9BF279AAFF0038916482D768AA44F25ACB9808F0
          B7B617B7D916713118A98BD909A2119B6D27B8204445445512711155E7FB7856
          E34743757B90891B6BB9C18B9D4D757DB35D2B639F7A6E15E24BC9674675EC6B
          00A43625E5B9118F200E35089C10ACA947514245E5A9C6AF6BB1C698397391A8
          4F592FC3AFC17F5DF8CF562351350CB25A2BC3CAC25745BBE5A38C4BB6FA6A55
          4A51434FA01F264F35A38005402EB95227089149E6304A694FF2E19B5F0D1C2A
          684F08BA78D34D3422C48DB3E093045495900295E5CD74C8A2902433175DC2B1
          3F109B925039840EE1406CD40E7463DAB36DCA816A1104106A822D5AA29366CD
          924D06EDD04C88A0820890134914524CA54D24924CA5226990A5210850294A05
          000AE5A9B382ED9E3B82C71286CA4DB736D0255CCA015926AA89DEDC50FC430A
          312F3FD534AA669DA921E7D44493CBBF55DD71EF2F55F6EE3392D82E2FB75125
          ABF43B694329F1A38BE99AAC6997B215197B27BB6C5057EF0B06C2346715D2A8
          AEAB6DF79A35294AC8BA865A5294A69A5294A69A5294A69A5294A69A5294A69A
          D04004040400404040404320203B08080EC2021B080F5AA67D67E15B4C758E35
          66F270ED18482827303D6E814A0731C073E690A018C8887CC4C6033F28D54CD2
          9A6B190E2BFC2C578C6AF168B8142E3845525CE042B707490604440702207455
          000C9142F96A93A94C4AC74B59782ABA6D07D20B5B4D5C37F863A863C2498980
          3E536E562FCC06C6039B950781B8FCBF141B057D25D64117291D070926BA2A94
          48A24A90AA267288604A621C04A6010EC203547BAD1C176956AA47488A310D62
          A5DCA6A09154D22790758C0221CC00006264C23B808806436000AB4F29C231AC
          C637A1795CD3EE08A8B139AE18B08BCF3C2B12C13D441455EEF45CF5239970AE
          327C6A41EC27549BDFD35DDA5B6D4E6B3AA613EF83F6D8ACF55B4C3EFBB7B448
          6DB1E926B10DE36C5194B285F05711DA551876319579D7CD1E5A1E5A05EAB1B3
          51CEE31F2023E6357889D0503D0C5038014E41DB0A2663A66D84A610DEBCCCFB
          8E7A671B6738C76DFDF60C86D8DC2B2B1E303C2FAE08D51722B6CA735162550E
          DD66EDCC6510288983CD6AF52382A89C0371148E003810394C03CA3615D60E0E
          6FCB01D3D5E11ABB9260D8E713C63C29529640B911E444C504D07C5294794A00
          08B81C63CB58C39A8A39A6C4E4541EACDC7D4F22AB1EE3569A6D06DE387BF0E4
          41E46620A78F521F2E1F0A4B11A14D7A10E987ED65D98DDDFBBB17DE06A3ECC6
          78FF00A51C67CF964FEDD5D4A2E03BA1E42FF6BB8E1BC7DC690F2446E1471506
          5BC827BE482B46D9DB6C6DFC0403238E99F7DC7EE034E611D83BE3BEDF41DBD0
          37C0875C604735CCE1BB866BA8D5E3759AB844DE5ACDD74D445544E1FBC55125
          00A721800432530075C9837AEBE7A8750EA19FD6031DC76FA886C3828C49B326
          DC1203025130245130215E0848151084917C2A2A728A8BCF1C6B6BF1E447971D
          89511F6654592D37223498EE83D1E430F0238D3CC3CD9136EB4EB6426DB8D910
          181210928AA2EB76DB075D847E8038FC83EE018FFCD81ADB9D8447B87A77F5DB
          1D3A771DF38C0E283D00073EA3BF5EC03DC3D807FA62B4FCFDC37ED9D877E818
          C76D83A57CF3F2E5117E9EFF009F844545FAFCB9FF00BFEDA6436CEE3BE7AFDB
          3B87F0ED8EB5A8FB8EFDF39E9DB1F5EBBE3B7A0D6DFCFDBFAFD6B5FE9DF61EE3
          D7B7700EA3F7AE39E3F55F3C27CF9E3CA78FD57DFEBF9F0D6BD8476DF6EDB7DB
          B74DB6CF5E9DF6FEBF5FAF5AD47FB0EDDFEFF4F6EFB5695C2AA7B2A7B7089E11
          557DB9444E113E69F2FDDA694A57BD6B5AB72DEF7143DA36740CBDD1745C2FD0
          8C83B7E0583994979690726E545A3060CD355CB95CE391E44D330948531CDCA4
          218C1C809B860DB60AE19900800891191928A0888A771129AAA220A7925E78E5
          7DFF002912188AC3D2A53CD468D19A724489121C06586186409C79E79E7085B6
          9A69B12371C3210001222241455D7835741E003C29B88AE3C2619CDC3C72BA6F
          A1CD5FF913FAC374473A2C638221CA7791F63C698105AF09A294C547FCA2A8C3
          305940FC4E55B1C8281EF43E1CBFE1F9671C581D61E3A904A41F091A4AC1F0F5
          1EF08AC7B43184AE1236A8CCB154C57EB94BE582D68C2B8066998C7466251D61
          68D2E54507070B6CC446DBF6E44464040C3B3463E2616198B58C8A8C62D8809B
          7671F1EC9245A336A810008920DD24D24CA1829402A48EDE6C34CB2F42DB3447
          A0405EC719A41256E7CB1FC243F1C62BCC064B8E15915F8D24554358A488A5A4
          EEB27ED69C7309FBD76EFA6476BB30CB43D78367BA0FB6DCDC3F1F793969C4C5
          631A2B39659325DCADD9BC8B8CB0E034E32390B4E38DB348DC1B7019C3B70376
          392D4D17B49324EBF68DD2BBF51E74A8485F979BA4B075159699F25316B1FE7E
          556B03169B38664004F2DA1D7299C2959B4A54B5AFAE83550D8AFAD88C418518
          11B622C66C5A65B14F3F8405113925552325E4CCD54CC8895557CF0E5F98E559
          FE476B97E6D905B65393DDC92976B797735F9F6335F2E1115D90F999236D020B
          51D86FB188CC0371E3B4D32D8362A529559AB6F4A5294D34A5294D34A5294D34
          A5294D34A5294D34A5294D34A5294D34A5294D35D27F1CC251B9DAC8B36EF5BA
          85314E8B9488B1040C0251C01C0703811DC3021D86ADFF00C457005A79AB4C24
          DE40B4422E61C266391B600881D5EB84CE02024C8E4400E380E80239AB86D29A
          6B0C6E2B3C2D6660CAE5498B7563F96657E16619B7322FD02752826EC33E7240
          5297F60B82C808147F6791CD58BF54F869BEB4EDEBC168D57B8231B194131DB3
          739649B269F38985CB00E63AA52109F32ACC560CEE64D20D83E9DB3F6DC1DCEC
          548D9E8C6B26CD4010149CA453F2E430224308731071DC043B67A055B17881F0
          D2B12FE19299B49345A3E58AB2A46062813E73018C2448C5E529F23B0644077C
          01471B63BCCB6C315CD00DD9F0D21DA28F01715E80C4CEE44E07E253B55A9A09
          C2228C90331045165D6557B926674D3D777501D30BF16BF13C91725C09B750A5
          6DC65EE49B4C6BD223EE7969495E1B0C6251A299A3B492634575F217AC20D808
          234BF3BF314C4318862890C53094C5300818A20381298043202510C180400407
          202190C06DFCB6FD76DFF5DAAFF1C5AF86C48DB4F9CACE6D97D1121CEA804C47
          B7049454E51FDE7680FEC1E9471911500AB880E01628E2ACE9A87C3FEA469DAA
          E557D06F24E21BE4C7978D66E564114837E77C88262B32C6FCC6500C80094795
          C180006A24669B3B95E21EACA0656EE99B552FBCABDB32365B4E3829D0D14DF8
          9DA89C9382AFC51F65928AA829E893A61FB493A7FEA37EEFC7E559A6D76E54B4
          699FE84E65363331AD269F68FA38AE4EA31AAEF95C7091B8D05E1ABBE905DCAD
          D29363EAAC93CFD7F3EDE9D3F5E95A529589FCFF002F97FD289E39F3C73F3F9F
          8F1AD84694AECB264F249E358F8E68E5FBF7CE5066C58B241574F1E3B72A9516
          CD5AB6408A2CE1CB858E449041121D555539534CA6318007265F0E4F005BBB52
          42075838D64656C1B1541464E1344DA2C2C6FABA9B8E0EDD4BD9F246156CC877
          1B1CF0C87FC4EED1102383C1738194B9717C46FB309E15D4504E4383D8B224B9
          CB70E0B44A89EB4B92A8A0D0782ED04427DC515161A70BC2E0FDF8EA2B697A6E
          C41DCC775B288D4B14D1E0A7A663B6664B934D6810BEEFC7A941C1933DFE49B1
          7A42FA35D005D07ECE7428CAAF259EF826F0F0E23F8ECBC090DA4D6C291B64C7
          3D6C85E1AB372A0E995876AA0AA9FB521A40A909E766CA815455BDBB0A0EA496
          E5299C8326A71784CE4780DF0C3E1CB80CB7505ECA872DE7ABAF99791746B25D
          0D1056E67C2B10A574C2DC6F959BDA16F98404858C8A3FC4BA2088CB484929CA
          6257569F69D589A4F67C1D81A69694058D655B6C928F83B66DA8D6D15131CD51
          214852A2D5B108532A7E503B872A8A8E5D2C265DCACB2C73A868CEA69EDF6D15
          0E142D4E90816F90A0A29593EDF2CC325FDA0AC60FBBD0E3F65651F74A7139E0
          996CD594F311D60FDA2FBB5D503F3F16A7393B71B3CAE903185D54D35B1C8980
          2FEAA466F6EC7A456AA6A88F0D1C64668A217A486C58CB8C1666A529596F5AEE
          D294A534D294A534D294A534D294A534D294A534D294A534D294A534D294A534
          D294A534D294A534D294A534D294A534D41778E9EDA17EB216174C233934B020
          45154882BA79EBC8A09447BE70603173BE33BD53F9382AD082CA0C91AD845501
          1113365126E648E02020621F28E0C4300E0C512E0C511288606AAD294D728AA8
          A8A8AA8A8BCA2A785454F6545F92A6AC8FC607811F083C48A0F2E0D3764E3874
          D4E508A2817058CC937B684D3A12AC603DCB61B870D581CEAAC74C567D6EBCB7
          DE180A63B817A61E51B40A3FE193E228D340DDC7123A2C95BBE6881A55183BE5
          7990433B2810278E6EC85610DC511B8CA401D81C0F5ACCDA958D6EF68B00BE9A
          B613688199464A4F9D7C8915EDC8255E489E622B8DB04E1F953745B174D5554D
          C25E1526E6D87DA29D5D6D2E32DE218D6EACAB1A28AD23156C661514F984AA76
          843B1B66B6CEFE14DB26A2B008211A03F2E4574600108F11A0E456D33C05F83D
          F0CDC0EAEDAF44915F5875AC854C49A9F7B46B34C2DD3814C0725896D26776C6
          D81398E6E7943B9929F50A52902592432DEAECD4A55EB4D47518F416EB696BE3
          D7426B951623876F71AF084EBCE1293AFBC5C277BCF1B8E9F09DC6BC27118B73
          375371378F2B9B9B6E765D719964F3905B76CEDE4239E8476D495A855F0D9066
          0555730A44AC5756458905853356A382992AA94A576BAC7FA5294A69A5294A69
          A5294A69A5294A69A5294A69A5294A69A5294A69A5294A69A5294A69A5294A69
          A5294A69A5294A69A5294A69A5294A69A5294A69A5294A69A5294A69A5294A69
          A5294A69A5294A69A5294A69A5294A69A5294A69A5294A69A5294A69A5294A69
          A5294A69A5294A69A5294A69A5294A69A5294A69A5294A69A5294A69A5294A69
          A5294A69A5294A69A5294A69A5294A69A5294A69A5294A69A5294A69A5294A69
          A5294A69A5294A69A5294A69A5294A69A5294A69A5294A69A5294A69A5294A69
          A5294A69A5294A69A5294A69A5294A69A5294A69A5294A69A5294A69AFFFD9}
        Transparent = True
      end
    end
  end
  object OCTImages: TImageList
    Height = 32
    Width = 32
    Left = 256
    Top = 24
    Bitmap = {
      494C010102000400040020002000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000800000002000000001002000000000000040
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000DF00
      DF00EF00BF00FF00CF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000007F007F007F007F007F007F007F007F007F007F007F007F000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000DF00DF007F00
      4000DF000000DF001F00BF00BF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000CF00CF003F00
      3F003F003F000000BF000000BF000000BF000000BF000000BF000000BF003F00
      3F003F003F00CF00CF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000005F002F00DF00
      0000FF000000EF000000DF007F00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000CF00CF0000006F000000
      FF000000FF003F00FF003F00FF003F00FF003F00FF003F00FF003F00FF000000
      FF000000FF0000006F00BF00BF00DF00DF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000DF00DF005F002F00FF000000FF00
      0000FF2F0000FF0000009F000000CF00BF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000DF00DF007F00BF001F009F000000FF003F00
      FF007F00FF000000000000000000000000000000000000000000000000007F00
      FF003F00FF000000FF0000007F003F007F00DF00DF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000BF007F00DF000000FF000000FF3F
      0000FFDF0000FF000000FF0000009F005F000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000002F005F000000DF000000FF002F00FF00DF00
      FF00000000000000000000000000000000000000000000000000000000000000
      0000DF00FF002F00FF000000FF000000DF002F005F0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000CF00CF009F001F00FF000000FF2F0000FF7F
      5F00FF3FBF00FF8F0000FF000000CF0000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000CF00CF000000FF000000FF002F00FF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000002F00FF000000FF000000FF00CF00CF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000CF00CF006F000000FF000000FF2F0000FF8F6F000000
      000000000000FF9F2F00FF1F0000FF0000006F003F0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000003F009F000000FF003F00FF00DF00FF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000DF00FF003F00FF000000FF003F009F00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000BF00BF007F001F00FF000000FF3F0000FF7F5F00000000000000
      000000000000FF3FBF00FF7F0000FF000000DF001F00BF00BF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000003F00FF000000FF007F00FF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000007F00FF000000FF000F00CF009F009F000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000DF007F00EF000000FF2F0000FF7F5F00FF0FEF00000000000000
      000000000000FF0FEF00FF7F5F00FF2F0000EF000000DF007F00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000007F00FF000000FF003F00FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000003F00FF000000FF007F00FF000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FF5F9F00FFBF3F00FF8F6F000000000000000000000000000000
      0000000000000000000000000000FF2F3F00FF0000007F000000BF00BF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000007F00FF000000FF003F00FF00BF00BF007F007F007F007F007F00
      7F007F007F007F007F007F007F007F007F007F007F007F007F007F007F007F00
      7F007F007F007F007F007F007F00BF00BF003F00FF000000FF007F00FF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FFBF3F00FF000000BF000000DF00BF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000007F00FF000000FF003F00FF007F00DF000000BF000000BF000000
      BF000000BF000000BF000000BF000000BF000000BF000000BF000000BF000000
      BF000000BF000000BF000000BF007F00DF003F00FF000000FF007F00FF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FF2FCF00FF000000FF0000006F002F000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000007F00FF000000FF003F00FF007F00FF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF007F00FF003F00FF000000FF007F00FF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF9F2F00FF1F0000FF000000CF00
      CF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000007F00FF000000FF003F00FF00BF00FF007F00FF007F00FF007F00
      FF007F00FF007F00FF007F00FF007F00FF007F00FF007F00FF007F00FF007F00
      FF007F00FF007F00FF007F00FF00BE00FF003F00FF000000FF007F00FF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF1FDF00FF403F00FF0000009F00
      3F00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000007F00FF000000FF003F00FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000003F00FF000000FF007F00FF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FF5F7F00FF2F0000CF00
      0F009F009F000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000003F00FF000000FF005F00DF00EF00EF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000EF00EF005F00DF000000FF003F00FF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FF2F3F00FF00
      0000DF005F00EF00EF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000009F00FF000000FF000000BF005F007F00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000005F007F000000BE000000FF009F00FF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FF5F9F00FF5F
      0000BF0000007F005F0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FF000000FF000000CF006F006F000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000006F006F000000CF000000FF000000FF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FF5F
      8F00FF5F0000CF0000006F006F00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000CF00FF001F00FF000000FF000000CF005F00
      7F00BF00BF00000000000000000000000000000000000000000000000000EF00
      EF005F007F000000CF000000FF001F00FF00CF00FF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FF5F9F00FF5F0F00CF0000007F005F00EF00EF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000BF00FF007F00FF000000FF000000
      BF0000007F007F007F007F007F007F007F007F007F007F007F007F007F005F00
      DF000000BE000000FF007F00FF00BE00FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FF5F9F00FF5F0000BF0000007F005F0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000008F00FF000000
      FF000000FF000000BF000000BF000000BF000000BF000000BF000000BF000000
      FF000000FF008F00FF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000FF5F8F00FF5F0000CF0000006F006F00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000003F00FF003F00FF003F00FF003F00FF003F00FF003F00FF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FF5F9F00FFBF3F00CF2F00003F000000CF00
      BF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FF5F7F00FF007F00FF00
      DF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000080000000200000000100010000000000000200000000000000000000
      000000000000000000000000FFFFFF00FFFFFFFFFFFFFFFF0000000000000000
      FFFFFFFFFFFFFFFF0000000000000000FFFFFFFFFFFFFFFF0000000000000000
      FFFFFFFFFFFFFFFF0000000000000000FFFFFFFFFFE3FFFF0000000000000000
      FFF81FFFFFC1FFFF0000000000000000FFC003FFFFC1FFFF0000000000000000
      FF8000FFFF00FFFF0000000000000000FE07E07FFF00FFFF0000000000000000
      FE0FF07FFE00FFFF0000000000000000FC3FFC3FFC187FFF0000000000000000
      FC3FFC3FF8383FFF0000000000000000FC7FFE1FF8383FFF0000000000000000
      F8FFFF1FF8FE1FFF0000000000000000F800001FFFFE1FFF0000000000000000
      F800001FFFFE1FFF0000000000000000F800001FFFFF0FFF0000000000000000
      F800001FFFFF0FFF0000000000000000F8FFFF1FFFFF87FF0000000000000000
      FC3FFC3FFFFFC3FF0000000000000000FC3FFC3FFFFFC3FF0000000000000000
      FE1FF87FFFFFE1FF0000000000000000FE07E07FFFFFF07F0000000000000000
      FF0000FFFFFFF87F0000000000000000FFC003FFFFFFFC3F0000000000000000
      FFF81FFFFFFFFE0F0000000000000000FFFFFFFFFFFFFF8F0000000000000000
      FFFFFFFFFFFFFFFF0000000000000000FFFFFFFFFFFFFFFF0000000000000000
      FFFFFFFFFFFFFFFF0000000000000000FFFFFFFFFFFFFFFF0000000000000000
      FFFFFFFFFFFFFFFF000000000000000000000000000000000000000000000000
      000000000000}
  end
end
