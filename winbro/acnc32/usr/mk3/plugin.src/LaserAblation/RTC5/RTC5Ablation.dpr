library RTC5Ablation;

{ Important note about DLL memory management: ShareMem must be the
  first unit in your library's USES clause AND your project's (select
  Project-View Source) USES clause if your DLL exports any procedures or
  functions that pass strings as parameters or function results. This
  applies to all strings passed to and from your DLL--even those that
  are nested in records and classes. ShareMem is the interface unit to
  the BORLNDMM.DLL shared memory manager, which must be deployed along
  with your DLL. To avoid using BORLNDMM.DLL, pass string information
  using PChar or ShortString parameters. }

{%File 'C:\HSD_Mk3\profile\cnc.ini'}
{%File 'C:\HSD_Mk3\profile\Live\Ablation.cfg'}

uses
  SysUtils,
  Classes,
  PluginInterface,
  RTC5Methods in 'RTC5Methods.pas',
  RTC5Import in 'RTC5 Files\RTC5Import.pas',
  ScannerSequencer in 'ScannerSequencer.pas',
  ScanData in 'ScanData.pas',
  RTC5Form in 'RTC5Form.pas' {RTC5ScannerForm},
  RTC5ScanManager in 'RTC5ScanManager.pas',
  ScannerHead in 'ScannerHead.pas',
  GLGizmo in '..\..\..\..\GLScene\Source\GLGizmo.pas',
  RTCSoftkey in 'RTCSoftkey.pas',
  ScannerParameterFileReader in 'ScannerParameterFileReader.pas';

{$R *.res}

begin
end.
