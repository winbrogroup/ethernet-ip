unit ScanData;

interface
uses Classes,  Windows, SysUtils,CNCTYpes;

type
TScanFileLoadEvent = procedure(Filename : String) of object;
TScanLine = class(TObject)
  X : Double;
  Y : Double;
  Z : Double;
  IsOn : Boolean;
  Delay : Integer;
  constructor Create;
  procedure SetScanline(Inx , Iny ,InZ : Double; InOn : Boolean);
  function AsString : String;
  end;

TScannerSlice = class(Tobject)
  private
    function GetHCount: Integer;
    function GetPCount: Integer;
  public
    HatchList : TList;
    ProfileList : TList;
    Constructor  Create;
    destructor  Destroy ; override;
    procedure Clean;
    procedure AddHatchLine(Line : TScanline);
    procedure AddProfileLine(Line : TScanline);
    function ProfileLineAtIndex(Index : INteger): TScanLine;
    function HatchLineAtIndex(Index : INteger): TScanLine;
    property HatchCount : Integer read GetHCount;
    property ProfileCount : Integer read GetPCount;
  end;

// Contains a list of TScannerSlice;
TScannerSliceList = class(TList)
private
  FonClean: TNotifyEvent;
public
  procedure Clean;
  constructor Create;
  destructor Destroy; override;
  procedure AddSlice(InSlice : TScannerSlice);
  property OnClean : TNotifyEvent read FonClean write FonClean;
end;


// This holds all slice data in SliceList

TScanFile =class(TStringList)
  private
    FonSliceClean: TNotifyEvent;
    function DecodeScannerLine(InStr : String) : TScanLine;
    function GetSCount: INteger;
    procedure SliceListCleaned(Sender : Tobject);
  public
    SliceList : TScannerSliceList;
    function Parse : Boolean;
    procedure Clean;
    constructor Create;
    destructor Destroy; override;
    property SliceCount : INteger read GetSCount;
    property OnSliceClean : TNotifyEvent read FonSliceClean write FonSliceClean;
  end;

TScannerStockParameters = Class(TObject)
     ID : String;
     LaserFrequency       : INteger;
     LaserPower           : INteger;
     LaserPeriod          : INteger;
     mVectorExtension     : Boolean;
     mMarkSpeed           : Double;
     mJumpSpeed           : Double;
     mJumpDelay           : Integer;
     mMarkDelay           : Integer;
     mPolygonDelay        : Integer;
     mLaserFirstDelay     : Integer;
     mLaserShutterDelay   : Integer;
     mLaserOnDelay        : Integer;
     mLaserOffDelay       : Integer;
     mRotation            : Double;
     mGroupSliceStart     : Integer;
     mGroupSliceEnd       : Integer;
     mGroupSlicePasses    : Integer;
     mGroupDwell          : Integer;
     mHatchCount          : Integer;
     mProfileStartCount   : Integer;
     mProfileEndCount     : Integer;
     mProfileEvery        : Integer;
     mProfilePasses       : Integer;
     mProfileDwell        : Integer;
     SectionDepth         : Double;
     procedure CloneFrom(SParams : TScannerStockParameters);
end;

TScanFeatureData = class(TObject)
  private
     FOnFileLoad: TScanFileLoadEvent;
  public
     ScanFile             : TScanfile;
     StockParams          : TScannerStockParameters;
     procedure Init;
     constructor Create;
     destructor Destroy; override;
     function LoadFromFile(ScanFilename : String):Boolean;
     procedure ReportFileToStrings(OutList : TStrings;ClearFirst : Boolean);
     procedure ReportSettingsToStrings(OutList : TStrings;ClearFirst : Boolean);
     function SliceAtIndex(Index : Integer) : TScannerSlice;
     property OnFileLoad : TScanFileLoadEvent read FOnFileLoad write FOnFileLoad;
//     property Rotataion
 end;



implementation

{ TScanParameters }

constructor TScanFeatureData.Create;
begin
Scanfile := TScanfile.create;
StockParams := TScannerStockParameters.Create;
Init;
end;

destructor TScanFeatureData.Destroy;
begin
ScanFile.Clean;
ScanFile.Free;
StockParams.Free;
inherited;
end;

procedure TScanFeatureData.Init;
begin
with StockParams do
 begin
 mMarkSpeed := 10;
 mJumpSpeed := 10;
 mMarkDelay := 10;
 mJumpDelay := 10;
 mLaserFirstDelay := 10;
 mLaserShutterDelay := 10;
 mLaserOnDelay      := 10;
 mLaserOffDelay     := 10;
 mRotation          := 0;
 mGroupSliceStart        := 0;
 mGroupSliceEnd          := 1;
 mGroupSlicePasses       := 1;
 mGroupDwell        := 1;
 mHatchCount        := 10;
 mProfileStartCount := 10;
 mProfileEndCount   := 10;
 mProfileDwell      := 1;
 mProfileEvery       := 1;
 mProfilePasses      := 0;
 end;
end;

function TScanFeatureData.LoadFromFile(ScanFilename: String): Boolean;
var
Line : String;
LNum : Integer;
begin
Result := False;
ScanFile.Clean;
if FileExists(ScanFilename) then
  begin
  ScanFile.LoadFromFile(ScanFilename);
  if ScanFile.Count > 0 then
    begin
    if not ScanFile.Parse then
      begin
      ScanFile.Clear;
      raise Exception.Create(Format('Error Parsing Scan File %s',[ExtractFilename(ScanFilename)]));
      end;
    ScanFile.Clear;
    end;
  Result := True;
  if assigned(FOnFileLoad) Then FOnFileLoad(ScanFilename);
  end

end;

procedure TScanFeatureData.ReportSettingsToStrings(OutList: TStrings; ClearFirst: Boolean);
begin
if assigned(OutList) then
  begin
  if ClearFirst then OutList.Clear;
  with StockParams do
    begin
    Outlist.Add(Format('Mark Speed: %5.3f',[mMarkSpeed]));
    Outlist.Add(Format('Jump Speed: %5.3f',[mJumpSpeed]));
    Outlist.Add(Format('Mark Delay: %d',[mMarkDelay]));
    Outlist.Add(Format('Jump Delay: %d',[mMarkDelay]));
    Outlist.Add(Format('Laser First Delay: %d',[mLaserFirstDelay]));
    Outlist.Add(Format('Shutter Delay: %d',[mLaserShutterDelay]));
    Outlist.Add(Format('Laser Off Delay: %d',[mLaserOffDelay]));
    Outlist.Add(Format('Rotation: %5.3f',[mRotation]));
    Outlist.Add(Format('Group Slice Start: %d',[mGroupSliceStart]));
    Outlist.Add(Format('Group Slice End: %d',[mGroupSliceEnd]));
    Outlist.Add(Format('Group Slice Passes: %d',[mGroupSlicePasses]));
    Outlist.Add(Format('Group Dwell: %d',[mGroupDwell]));
    Outlist.Add(Format('Hatch Count: %d',[mHatchCount]));
    Outlist.Add(Format('Profile Start Count: %d',[mProfileStartCount]));
    Outlist.Add(Format('Profile End Count: %d',[mProfileEndCount]));
    Outlist.Add(Format('Profile Every: %d',[mProfileEvery]));
    Outlist.Add(Format('Profile Passes: %d',[mProfilePasses]));
    Outlist.Add(Format('Profile Dwell: %d',[mProfileDwell]));
    end;
  end;
end;

procedure TScanFeatureData.ReportFileToStrings(OutList: TStrings;ClearFirst : Boolean);
var
SNum : Integer;
CurrentSlice : TScannerSlice;
LNum : Integer;
CurrentLine : TScanLine;
begin
if assigned(OutList) then
  begin
  if ClearFirst then OutList.Clear;
  if ScanFile.SliceCount > 0 then
    begin
    OutlIst.Add(Format('SliceCount : %d',[ScanFile.SliceList.Count]));
    for SNum := 0 to ScanFile.SliceCount-1 do
      begin
      OutList.Add(';************');
      OutList.Add(Format('; Slice %d',[Snum]));
      OutList.Add(';************');
      CurrentSlice := TScannerSlice(ScanFile.SliceList[Snum]);
      if CurrentSlice.HatchCount > 0 then
        begin
        OutList.Add(';************');
        OutList.Add('; Hatch')       ;
        OutList.Add(';************');
        For LNum := 0 to CurrentSlice.HatchCount-1 do
          begin
          CurrentLine := TScanLine(CurrentSlice.HatchList[LNum]);
          OutList.Add(CurrentLine.AsString);
          end
        end;
      if CurrentSlice.ProfileCount > 0 then
        begin
        OutList.Add(';************');
        OutList.Add('; Profile')       ;
        OutList.Add(';************');
        For LNum := 0 to CurrentSlice.ProfileCount-1 do
          begin
          CurrentLine := TScanLine(CurrentSlice.ProfileList[LNum]);
          OutList.Add(CurrentLine.AsString);
          end
        end
      end
    end;
  end;
end;

{ TScanfile }

procedure TScanFile.Clean;
begin
Clear;
SliceList.Clean;
if assigned(FonSliceClean) then FonSliceClean(self);
// Clean all lists
end;

constructor TScanFile.Create;
begin
SliceList := TScannerSliceList.Create();
SliceList.OnClean := SliceListCleaned;
end;

function TScanFile.DecodeScannerLine(InStr: String): TScanLine;
var
Num : String;
Pass : Boolean;
begin
Pass := False;
Result := TScanLine.Create;
Num := ReadDelimited(InStr,' ');
if Num[1] = 'X' then
  begin
  Num := Copy(Num,2,Length(Num));
  Result.X := StrToFloatDef(Num,0);
  Num := ReadDelimited(InStr,' ');
  if Num[1] = 'Y' then
    begin
    Num := Copy(Num,2,Length(Num));
    Result.Y := StrToFloatDef(Num,0);
    Num := ReadDelimited(InStr,' ');
    If Num[1]='Z' then
      begin
      Num := Copy(Num,2,Length(Num));
      Result.Z := StrToFloatDef(Num,0);
      Num := ReadDelimited(InStr,' ');
      end
    else
      begin
      Result.Z := 0.0;
      end;
    if Num = 'M03' then
      begin
      Pass := True;
      Result.IsOn := True;
      end
    else if Num = 'M05' then
      begin
      Pass := True;
      Result.IsOn := False;
      end
    end;
  end;

if not Pass then
  begin
  Result.Free;
  Result := nil;
  end;

end;

destructor TScanFile.Destroy;
begin
  Clean;
  SliceList.Free;
  inherited;
end;


function CommentStrip(InStr : String): String;
var
SCPos : Integer;
begin
SCPOs := Pos(';',InStr);
if SCPos > 0 then
  begin
  Result := Copy(InStr,1,SCPOs-1);
  Result := Trim(UpperCase(Result));
  end
else
  Result := Trim(UpperCase(Instr));
end;

function TScanFile.GetSCount: INteger;
begin
if assigned(SliceList) then
  begin
  Result := SliceList.Count;
  end
else
  begin
  Result := -1;
  end
end;

function TScanFile.Parse: Boolean;
var
CurrentPattern : TScannerSlice;
LineBuffer : String;
LNum : INteger;
Token : String;
Hatch : Boolean;
CurrentLine : TScanLine;
begin
Hatch := False;
Result := False;
try
if Count > 0 then
  begin
  CurrentPattern := nil;
  For Lnum := 0 to Count-1 do
    begin
    LineBuffer := CommentStrip(Strings[lNum]);
    if Length(LineBuffer) > 0 then
      begin
      Token := ReadDelimited(LineBuffer,' ');
      if Token = 'SL' then
        begin
        if assigned(CurrentPattern) then
          begin
          CurrentLine := DecodeScannerLine(LineBuffer);
          if assigned(CurrentLine) then
            begin
            if Hatch then
              begin
              CurrentPattern.AddHatchLine(CurrentLine);
              end
            else
              begin
              CurrentPattern.AddProfileLine(CurrentLine);
              end
            end;
          end
        else
          Begin
          exit;
          end;
        end
      else if Token = 'HCH' then
        begin
        Hatch := True;
        if assigned(CurrentPattern) then
          begin
          if CurrentPattern.HatchCount > 0 then
            begin
            // Slice finished so store and create a new one
            SliceList.AddSlice(CurrentPattern);
            CurrentPattern := TScannerSlice.Create;
            end
          end
        else
          begin
          CurrentPattern := TScannerSlice.Create;
          end

        end
      else if Token = 'PFL' then
        begin
        if assigned(CurrentPattern) then
          begin
          Hatch := False;
          if CurrentPattern.ProfileCount > 0 then
            begin
            // Slice finished so store and create a new one
            SliceList.AddSlice(CurrentPattern);
            CurrentPattern := TScannerSlice.Create;
            end
          end
        else
          begin
          CurrentPattern := TScannerSlice.Create;
          end
        end
      end;
    end;
  if assigned(CurrentPattern) then
    begin
    if ((CurrentPattern.HatchCount = 0) and (CurrentPattern.ProfileCount = 0)) then
      begin
      CurrentPattern.Free;
      end
    else
      begin
      SliceList.AddSlice(CurrentPattern);
      end
    end;
  end; // if Count > 0
  Result := True;
 except
 Result := False;
 end;
end;

procedure TScanFile.SliceListCleaned(Sender: Tobject);
begin
if assigned(FonSliceClean) then FonSliceClean(Sender);
end;

{ TScannerSliceList }

constructor TScannerSliceList.Create;
begin

end;

procedure TScannerSliceList.Clean;
var
LEntry : TScannerSlice;
begin
while Count > 0 do
  begin
  LEntry := TScannerSlice(Items[Count-1]);
  LEntry.Free;
  Delete(Count-1);
  end;
end;

destructor TScannerSliceList.Destroy;
begin
Clean;
inherited;
end;

procedure TScannerSliceList.AddSlice(InSlice: TScannerSlice);
begin
Add(InSlice);
end;

{ TScannerSlice }

procedure TScannerSlice.AddHatchLine(Line: TScanline);
begin
HatchList.Add(Line);
end;

procedure TScannerSlice.AddProfileLine(Line: TScanline);
begin
ProfileList.Add(Line);
end;

procedure TScannerSlice.Clean;
var
LEntry : TScanLine;
begin
while HatchList.Count > 0 do
  begin
  LEntry := HatchList[HatchList.Count-1];
  Lentry.Free;
  HatchList.Delete(HatchList.Count-1);
  end;
while ProfileList.Count > 0 do
  begin
  LEntry := ProfileList[ProfileList.Count-1];
  Lentry.Free;
  ProfileList.Delete(ProfileList.Count-1);
  end;
end;

constructor TScannerSlice.Create;
begin
  HatchList   := TList.Create;
  ProfileList := TList.Create;
end;

destructor TScannerSlice.Destroy;
begin
  Clean;
  ProfileList.Free;
  HatchList.Free;
  inherited;
end;

function TScannerSlice.GetHCount: Integer;
begin
Result := HatchList.Count;
end;

function TScannerSlice.GetPCount: Integer;
begin
Result := ProfileList.Count;
end;

function TScannerSlice.HatchLineAtIndex(Index: INteger): TScanLine;
begin
Result := nil;
if (Index > 0) and (Index < HatchList.Count) then
  begin
  Result := TScanLine(HatchList[Index]);
  end;
end;

function TScannerSlice.ProfileLineAtIndex(Index: INteger): TScanLine;
begin
Result := nil;
if (Index > 0) and (Index < ProfileList.Count) then
  begin
  Result := TScanLine(ProfileList[Index]);
  end;
end;

{ TScanLine }
procedure TScanLine.SetScanline(Inx, Iny, InZ: Double; InOn: Boolean);
begin
x := Inx;
y := Iny;
z := Inz;
IsOn := InOn;
end;

function TScanLine.AsString: String;
var
MBuff : String;
begin
if IsOn then MBuff := 'M03' else MBuff := 'M05';
Result := Format('%6.3f,%6.3f,%6.3f,%s',[X,Y,Z,MBuff]);
end;


function TScanFeatureData.SliceAtIndex(Index: Integer): TScannerSlice;
begin
Result := nil;
if(Index >=0) and (Index < ScanFile.SliceList.Count) then
 begin
 Result := ScanFile.SliceList[Index];
 end;
end;

constructor TScanLine.Create;
begin
  inherited Create;
  IsOn := False;
end;



procedure TScannerStockParameters.CloneFrom(SParams: TScannerStockParameters);
begin
     ID                   := SParams.ID;
     LaserFrequency       := SParams.LaserFrequency;
     LaserPower           := SParams.LaserPower;
     LaserPeriod          := SParams.LaserPeriod;
     mVectorExtension     := SParams.mVectorExtension;
     mMarkSpeed           := SParams.mMarkSpeed;
     mJumpSpeed           := SParams.mJumpSpeed;
     mJumpDelay           := SParams.mJumpDelay;
     mMarkDelay           := SParams.mMarkDelay;
     mPolygonDelay        := SParams.mPolygonDelay;
     mLaserFirstDelay     := SParams.mLaserFirstDelay;
     mLaserShutterDelay   := SParams.mLaserShutterDelay;
     mLaserOnDelay        := SParams.mLaserOnDelay;
     mLaserOffDelay       := SParams.mLaserOffDelay;
     mRotation            := SParams.mRotation;
     mGroupSliceStart     := SParams.mGroupSliceStart;
     mGroupSliceEnd       := SParams.mGroupSliceEnd;
     mGroupSlicePasses    := SParams.mGroupSlicePasses;
     mGroupDwell          := SParams.mGroupDwell;
     mHatchCount          := SParams.mHatchCount;
     mProfileStartCount   := SParams.mProfileStartCount;
     mProfileEndCount     := SParams.mProfileEndCount;
     mProfileEvery        := SParams.mProfileEvery;
     mProfilePasses       := SParams.mProfilePasses;
     mProfileDwell        := SParams.mProfileDwell;
     SectionDepth         := SParams.SectionDepth;

end;


end.
