unit RTC5ScanManager;

interface
uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs,IFormInterface,INamedInterface,AbstractFormPlugin,ExtCtrls,
  ScanData, ScannerSequencer,ScannerHead;

type
TRTC5ScanManager = class(TObject)
  private

   // Tags
    TempCount : INteger;
    FDebugMode: Integer;
    FOnDebug : TScannerDebugEvent;
    FBinPath: String;
    FOnHeadStatusChange: TScannerStatusChangeEvent;
    FSpeedsAsString: String;
    FGroupSeqAsString: String;
    FRotationAsString: String;
    FDelaysAsString: String;
    FHatchAsString: String;
    FHeadInstalled : Boolean;
    FOnSeqHeadParansChange: TNotifyEvent;
    procedure SetDebugMode(const Value: Integer);
    procedure ReportDebug(Sender : Tobject;Level : Integer; DebugMess : String);
    procedure HeadStatusChanged(Sender : Tobject; Stat : Integer);
    Procedure HeadParamsHaveChanged(Sender : Tobject);
    function GetRotationAsString: String;

  public
  ScanSequencer : TScannerSequencer;
  RTC5ScannerHead : TScannerHead;
  constructor Create;
  destructor Destroy ; override;
  procedure InstallHead(BinPath : String);
  procedure SetScannerParameters;
  procedure SetStockScannerParameters(StockParams: TScannerStockParameters);
  procedure aInterruptRequest;
  property aDebugMode : Integer read FDebugMode write SetDebugMode;
  property OnDebug : TScannerDebugEvent read FOnDebug write FOnDebug;
  property OnHeadStatusChange : TScannerStatusChangeEvent read FOnHeadStatusChange write FOnHeadStatusChange;
  property BinaryPath : String read FBinPath;
  property SpeedsAsString : String read FSpeedsAsString;
  property DelaysAsString : String read FDelaysAsString;
  property HatchSequenceAsString :String read FHatchAsString;
  property RotationAsString :String read GetRotationAsString;
  property GroupSequenceAsString :String read FGroupSeqAsString;
  property OnHeadParamsChange : TNotifyEvent read FOnSeqHeadParansChange write FOnSeqHeadParansChange;
  property HeadInstalled : Boolean read FHeadInstalled;
//  property ManScanFileData : TScanFeatureData read ManScanFileData write
  end;
var
RTC5Manager : TRTC5ScanManager;

implementation

{ TRTC5ScanManager }

constructor TRTC5ScanManager.Create;
begin
  TempCount :=0;
  RTC5ScannerHead := nil; //TScannerHead.Create;
  RTC5Manager := Self;
  FSpeedsAsString := '--.--- , --.---';
  FDelaysAsString :=  '--,--,--,--,--';
  FRotationAsString := '--.---';
  FGroupSeqAsString :='-,-,-,-';
  FHatchAsString:=  '---,--,--,--,--';
  FHeadInstalled := False;

end;

destructor TRTC5ScanManager.Destroy;
begin
  ScanSequencer.Terminate;
  ScanSequencer.Free;
  inherited;
end;

procedure TRTC5ScanManager.HeadStatusChanged(Sender: Tobject; Stat: Integer);
begin
if assigned(FOnHeadStatusChange) then FOnHeadStatusChange(Sender,Stat);
end;

procedure TRTC5ScanManager.InstallHead(BinPath : String);
begin
ScanSequencer := TScannerSequencer.Create;
ScanSequencer.OnDebug := ReportDebug;
ScanSequencer.OnHeadParamsChange := HeadParamsHaveChanged;
RTC5ScannerHead := ScanSequencer.Head;
RTC5ScannerHead.OnStatusChange := HeadStatusChanged;
//ManScanFileData := ScanSequencer.ScanFileData;
RTC5ScannerHead.aDebugMode := FDebugMode;
RTC5ScannerHead.OnDebug := ReportDebug;
if RTC5ScannerHead.Install(BinPath) then
  begin
  FHeadInstalled := True;
  FBinPath := BinaryPath;
  end;
end;

procedure TRTC5ScanManager.aInterruptRequest;
begin
 if assigned(ScanSequencer) then
  begin
  if not ScanSequencer.ScanInterruptRequest then
    begin
    ReportDebug(Self,4,'Reset/Interrupt Requested');
    ScanSequencer.ScanInterruptRequest := True;
    ScanSequencer.ResetScanner;
    end;
  end;
end;

procedure TRTC5ScanManager.ReportDebug(Sender: Tobject;Level : Integer; DebugMess: String);
begin
if assigned(FOnDebug) then FonDebug(Sender,Level,DebugMess);
end;

procedure TRTC5ScanManager.SetDebugMode(const Value: Integer);
begin
  FDebugMode := Value;
end;

procedure TRTC5ScanManager.SetStockScannerParameters(StockParams : TScannerStockParameters);
begin
  RTC5ScannerHead.SetStockParameters(StockParams);
end;

procedure TRTC5ScanManager.SetScannerParameters;
begin
try
if assigned(ScanSequencer.BufferScanFileData) then
  begin
  if assigned(ScanSequencer.BufferScanFileData.ScanFile) then
    begin
    if ScanSequencer.BufferScanFileData.ScanFile.SliceCount > 0 then
      begin
      ReportDebug(Self,4,Format('Slice Count %d',[ScanSequencer.BufferScanFileData.ScanFile.SliceCount]));
      RTC5ScannerHead.SetParameters(ScanSequencer.BufferScanFileData);
      end
    else
      begin
      ReportDebug(Self,4,Format('No Slices %d',[ScanSequencer.BufferScanFileData.ScanFile.SliceCount]));
      end;
    end;
  end;
except
  ReportDebug(Self,4,'Exception in Set Scan Parameters');
end;
end;




procedure TRTC5ScanManager.HeadParamsHaveChanged(Sender: Tobject);
begin
if assigned(ScanSequencer.Head.ScanFileData) then
begin
with ScanSequencer.Head.ScanFileData.StockParams do
  begin
  ReportDebug(Self,2,'Head Params Changed');
  FSpeedsAsString := Format('%5.3f , %5.3f', [mMarkSpeed,mJumpSpeed]);
  FDelaysAsString :=  Format('%d,%d,%d,%d,%d,%d', [mMarkDelay,
                                                 mJumpDelay,
                                                 mPolygonDelay,
                                                 mLaserFirstDelay,
                                                 mLaserShutterDelay,
                                                 mLaserOnDelay,
                                                 mLaserOffDelay]);
  FRotationAsString := Format('%5.3f',[mRotation]);
//  FRotationAsString := Format('%5.3f',[TempCount+0.1]);
  inc(TempCount);
  FGroupSeqAsString := Format('%d,%d,%d,%d',[mGroupSliceStart,mGroupSliceEnd,mGroupSlicePasses,mGroupDwell]);
  FHatchAsString := Format('%d,%d,%d,%d',[mProfilePasses,mProfileStartCount,mProfileEndCount,mProfileEvery]);


  end;
If assigned(FOnSeqHeadParansChange) then FOnSeqHeadParansChange(Self);
end;
end;

function TRTC5ScanManager.GetRotationAsString: String;
begin
Result := '??';
if assigned(ScanSequencer) then
  if assigned(ScanSequencer.Head.ScanFileData) then
    Result := Format('%5.3f',[ScanSequencer.Head.ScanFileData.StockParams.mRotation]);
end;

end.
