unit ScannerHead;

interface

uses Classes, RTC5Import, Windows, ScanData, INamedInterface,SysUtils;
const
TERMINATE_MARKER : Integer = $0555;
    READY1 : Integer = 4;
    READY2 : Integer = 8;
    BUSY1 : Integer = 16;
    BUSY2 : Integer = 32;
    USED1 : Integer = 64;
    USED2 : Integer = 128;
    RTC5_BUSY : Integer = 1;
    RTC5_INTERNAL_BUSY: Integer = 128;
    RTC5_PAUSED : Integer = 1 shl 15;

type
TScannerErrorEvent = procedure(Sender : Tobject; ErrorNumber : Cardinal) of Object;
TScannerDebugEvent = procedure(Sender : Tobject;DisplayLevel : Integer; DebugMess : String) of Object;
TScannerStatusChangeEvent = procedure(Sender : Tobject; Stat : Integer) of Object;

TScannerHead = class(TObject)

  private
  FStatus : Integer;
  FInputPos : Integer;
  FInputPercent : Double;
  LastStatus : INteger;
  FSwapping : Boolean;
  FComplete : Boolean;
  FXOffset : Integer;
  FYOffset : Integer;
  FXSpeedScale : Double;
  FYSpeedScale : Double;
  FXPosScale : Double;
  FYPosScale : Double;
  FCurrentList : Integer;
  FOutListNumber : LongWord;
  FOutListPercent : Double;
//  FSwapToList  : Integer;
  FLastError : Cardinal;
  FOnError : TScannerErrorEvent;
  FOnDebug : TScannerDebugEvent;
  FDebugMode : Integer; // 0 = none, 1 = minimal , 2 = verbose
  FInstalled: TNotifyEvent;
  FScanFileData : TScanFeatureData;
  FLoadedOK : Boolean;
  FOnStatusChange: TScannerStatusChangeEvent;
  FListSize: Integer;
  FOnHeadParamsChange: TNotifyEvent; // externally Set
  FCurrentMarkSpeed : Double;
  FCurrentJumpSpeed : Double;
  procedure checkErrors;

  procedure DebugStatus(DebugMessage : String;Level : Integer);
    procedure SetFXOffset(const Value: Integer);
    procedure SetFXSpeedScale(const Value: Double);
    procedure SetFYOffset(const Value: Integer);
    procedure SetFYSpeedScale(const Value: Double);
    procedure SetFXPosScale(const Value: Double);
    procedure SetFYPosScale(const Value: Double);


  public
  constructor Create;
  destructor Destoy;
  // Scanner Commands
  procedure PrepareSequence;
  procedure Start;
  procedure Halt;
  procedure EndExecution;
  procedure Mark(X,Y : Double);
  procedure Jump(X,Y : Double);
  procedure Delay(MicroSecs : Integer);
  procedure Reset;
  function IsBlocking : Boolean;
  function IsRunning : Boolean;
  function CheckComplete : Boolean;
  function Install(InBinPath : String): Boolean;
  procedure SetParameters(SFileData : TScanFeatureData); // externally set and destroyed
  procedure SetStockParameters(SParams: TScannerStockParameters);
  procedure ReportSetttingsToStringList(OutList: TStrings;ClearFirst : Boolean);


  // Scanner Properties
  property aDebugMode : Integer read FDebugMode write FDebugMode;
  property LastError : Cardinal read FLastError;
  property OnError : TScannerErrorEvent read FOnError write FOnError;
  property OnDebug : TScannerDebugEvent read FOnDebug write FOnDebug;
  property OnInstalled : TNotifyEvent read FInstalled write FInstalled;
  property ScanFileData : TScanFeatureData read FScanFileData;
  property Status : Integer read FStatus;
  property OnStatusChange : TScannerStatusChangeEvent read FOnStatusChange write FOnStatusChange;
  property LoadedOK : Boolean read FLoadedOK;
  property ScanComplete : Boolean read FComplete;
  property XSpeedScale : Double read FXSpeedScale write SetFXSpeedScale;
  property YSpeedScale : Double read FYSpeedScale write SetFYSpeedScale;
  property XPosScale : Double read FXPosScale write SetFXPosScale;
  property YPosScale : Double read FYPosScale write SetFYPosScale;
  property XOffset : Integer read FXOffset write SetFXOffset;
  property YOffset : Integer read FYOffset write SetFYOffset;
  property ListSize : Integer read FListSize write FListSize;
  property OnHeadParamsChange : TNotifyEvent read FOnHeadParamsChange write FOnHeadParamsChange;
  property CurrentMarkSpeed : Double read FCurrentMarkSpeed;
  property CurrentJumpSpeed : Double read FCurrentJumpSpeed;
  property BufferPositionPercent : Double read FInputPercent;
  property OutBufferNumber : LongWord read FOutListNumber;
  property OutBufferPercent : Double read FOutListPercent;
  end;

implementation

{ TScannerHead }

{
procedure get_list_pointer(var ListNo: LongWord; var Pos: LongWord); stdcall; external RTC5DLL;
function  get_input_pointer(): LongWord; stdcall; external RTC5DLL;
}
constructor TScannerHead.Create;
begin
FScanFileData := TScanFeatureData.Create;
FListSize := 4000;
FInputPercent := 0;
FXSpeedScale := 476*16;
FYSpeedScale := 476*16;
FXOffset := 0;
FYOffset := 0;
FCurrentList := 0;
FDebugMode := 0;
LastStatus := -2;
FLoadedOK := False;
end;

procedure TScannerHead.PrepareSequence;
begin
 if Assigned(FScanFileData) then
  begin

  DebugStatus('START OF PREPARE',3);
  FSwapping := False;
  stop_execution();
  reset_error(Windows.MAXLONG);
  set_laser_mode(1);
  enable_laser();
  config_list(FListSize, FListSize);
  FInputPercent := 0.0;
  FCurrentList := 1;
  set_start_list(FCurrentList);
  set_angle_list(1, FScanFileData.StockParams.mRotation, 3);
  FCurrentMarkSpeed := FScanFileData.StockParams.mMarkSpeed * FXSpeedScale;
  FCurrentJumpSpeed := FScanFileData.StockParams.mJumpSpeed * FXSpeedScale;
  set_jump_speed(FScanFileData.StockParams.mJumpSpeed * FXSpeedScale);
  set_mark_speed(FScanFileData.StockParams.mMarkSpeed * FYSpeedScale);

  set_laser_delays(FScanFileData.StockParams.mLaserOnDelay * 2, FScanFileData.StockParams.mLaserOffDelay * 2);
  set_scanner_delays(FScanFileData.StockParams.mJumpDelay div 10, FScanFileData.StockParams.mMarkDelay div 10, FScanFileData.StockParams.mPolygonDelay div 10);

  DebugStatus('END OF PREPARE',3);
  FComplete := False;
  checkErrors();
  end;
end;

procedure TScannerHead.DebugStatus(DebugMessage: String;Level : Integer);
begin
if aDebugMode > 0 then
  begin
  if assigned(FOnDebug) then FOnDebug(Self,Level,DebugMessage);
  end;
if aDebugMode > 1 then
  begin
//  get_out_pointer();
  end;

end;


procedure TScannerHead.CheckErrors;

begin
FLastError := get_last_error;
if FLastError > 0 then
  begin
  if assigned(FOnError) then
    begin
    FOnError(Self,LastError);
    end;
  end;
end;


(*


    @Override
    public String getName() {
        return mName;
    }


    public void close() {
        logger.debug("Shutting down RTC5 scanner head");
        outPointer = null;
        status = null;
        if(rtc != null)
            rtc.free_rtc5_dll();
    }
*)

procedure TScannerHead.Start;
begin
 if Assigned(FScanFileData) then
  begin
  DebugStatus('START CALLED',3);
  execute_list(FCurrentList);
  checkErrors();
  DebugStatus('EXECUTE LIST',3);
  start_loop();
  end;
end;

procedure TScannerHead.Halt;
begin
 if Assigned(FScanFileData) then
  begin
  stop_list;
  debugStatus('STOP LIST',3);
  checkErrors();
  end;
end;

procedure TScannerHead.EndExecution;
begin
 if Assigned(FScanFileData) then
  begin
  set_wait(TERMINATE_MARKER);
  DebugStatus(Format('WaitMarker set to %d',[TERMINATE_MARKER]),4);
  set_end_of_list();
  DebugStatus('END_OF_LIST',3);
  checkErrors();
  end;
end;


procedure TScannerHead.Mark(X, Y: Double);
var
xc,yc : Integer;
begin
 if Assigned(FScanFileData) then
  begin
  xc := Round((FXPosScale * x) + FXOffset);
  yc := Round((FYPosScale * y) + FYOffset);
  mark_abs(xc, yc);
  checkErrors();
  end;
end;

procedure TScannerHead.Jump(X, Y: Double);
var
xc,yc : Integer;
begin
 if Assigned(FScanFileData) then
  begin
  xc := Round((FXPosScale * x) + FXOffset);
  yc := Round((FYPosScale * y) + FYOffset);
  jump_abs(xc, yc);
  checkErrors();
  end;
end;
procedure TScannerHead.Delay(MicroSecs: Integer);
begin
 if Assigned(FScanFileData) then
  begin
  long_delay(MicroSecs div 10);
  checkErrors();
  end;
end;

procedure TScannerHead.Reset;
begin
 if Assigned(FScanFileData) then
  begin
  DebugStatus('RESET',3);
  stop_execution();
  disable_laser();
  DebugStatus('AFTER RESET',3);
  checkErrors();
  end;
end;
(*    public boolean isBlocking() {
        if (rtc == null)
            return false;

        // Swapping occurs when this list goes into cycle
        //
        if (mSwapping) {
            //rtc.get_out_pointer(outPointer);
            int list = (rtc.read_status() & RTC5.BUSY1) != 0? 1: 2;
            if ((list & mCurrentList) != 0) {
                mCurrentList = mCurrentList == 1 ? 2 : 1;
                rtc.set_start_list(mCurrentList);
                mSwapping = false;
                return false;
            }
            return true;
        }

        int x = rtc.get_list_space();
        if (x > 50)
            return false;

        // initiate swapping
        rtc.set_end_of_list();
        mSwapping = true;
        debugStatus("STATUS BEFORE SWAP");
        // rtc.auto_change();
        checkErrors();
        return true;
    }
*)
function TScannerHead.IsBlocking: Boolean;
var
ActualList : Integer;
//Status : Integer;
x : Integer;
OutList,OutPointerPos : LongWord;
begin
Result := False;
if not Assigned(FScanFileData) then
  begin
  DebugStatus('No Scanfiledata',3);
  exit;
  end;
if (FSwapping) then
    begin
    //rtc.get_out_pointer(outPointer);
    FStatus := read_status();
    if FStatus <> LastStatus then
      begin
      if assigned(FOnStatusChange) then FOnStatusChange(Self,FStatus);
      LastStatus := FStatus;
      end;

{    if Status <> 0 then
      begin
      DebugStatus(Format('%d',[Status]));
      DebugStatus(Format('SWAP STATUS %d',[Status]));
      DebugStatus(Format('%d',[Status]));
      end;}
    if (Status and BUSY1) <> 0 then ActualList := 1  else ActualList := 2;
    if ActualList = FCurrentList then
        begin
        if FCurrentList = 1 then FCurrentList := 2 else FCurrentList := 1;
        set_start_list(FCurrentList);
        DebugStatus(Format('Swapped to List %d',[FCurrentList]),3);
        FSwapping := False;
        Result := false;    // not blocking anymore
        end
    else
      begin
      Result := True;
//      DebugStatus('Got Here');
      end
    end
else
    begin
    // Here if not swapping on entry
    x := get_list_space();
    FInputPos := FListSize-x;
    FInputPercent := (FInputPos/FListSize)*100;
    get_list_pointer(FOutListNumber,OutPointerPos);
    FOutListPercent := (OutPointerPos/FListSize)*100;
    // if more than 50 bananas left then Not blocking
    if (x > 50) then
      begin
      Result := False;
      end
    else
      begin
      // initiate swapping if less than 50 bananas
      set_end_of_list();
      FSwapping := True;
      DebugStatus(Format(' %d',[FCurrentList]),3);
      DebugStatus(Format('INITIATE SWAP FROM LIST %d',[FCurrentList]),3);
      DebugStatus(Format(' %d',[FCurrentList]),3);
      //auto_change();
      checkErrors();
      Result := true;
      end;
    end;
end;

function TScannerHead.IsRunning: Boolean;
var
Status,Pos : LongWord;
begin
Result := False;
if not Assigned(FScanFileData) then exit;
get_status(Status,Pos);
Result := ((Status and (RTC5_BUSY or RTC5_INTERNAL_BUSY)) <> 0);
end;

function TScannerHead.CheckComplete: Boolean;
var
Status,Pos,x : LongWord;
begin
Result := True;
if not Assigned(FScanFileData) then exit;
if (FComplete) then
  begin
  Result := True;
  end
else
  begin
  get_status(Status,Pos);
  x := get_wait_status();
  if x <> 0 then DebugStatus(Format('Status %d',[x]),3);
  FComplete := (x = TERMINATE_MARKER);
  if (FComplete) then
    begin
    disable_laser();
    DebugStatus('RX END MARKER',3);
    quit_loop();
    checkErrors();
    Result := True;
    end
  else
    begin
    Result := False
    end
  end;
end;
(*

    @Override
    public void install(ICNC cnc) {
        File datapath = cnc.getResolver("live").resolve("rtc5");
        rtc = new RTC5();
        if (RTC5.libraryLoaded) {
            int load = rtc.init_rtc5_dll();
            if (load > 0) {
                rtc.free_rtc5_dll();
                rtc = null;
                logger.error("RTC5 Failed to initialize with error code: " + load);
                return;
            }
            int count = rtc.rtc5_card_count();
            if (count == 0) {
                logger.error("No RTC5 card detected");
                rtc.free_rtc5_dll();
                return;
            }

            int x = rtc.get_dll_version();
            logger.debug("get_dll_version: " + x);
            x = rtc.acquire_rtc5(1);
            logger.debug("acquire_rtc5: " + x);
            x = rtc.load_program_file(datapath.getAbsolutePath());
            logger.debug("load_program_file: " + x);
            String cor = new File(datapath, "Cor_1to1.ct5").getAbsolutePath();
            x = rtc.load_correction_file(cor, 1, 2);
            logger.debug("load_correction_file: " + x);
            outPointer = ByteBuffer.allocateDirect(8);
            outPointer.order(ByteOrder.nativeOrder());
            status = ByteBuffer.allocateDirect(8);
            status.order(ByteOrder.nativeOrder());
            mComplete = true;

        } else {
            rtc = null;
        }
    }

*)

function TScannerHead.Install(InBinPath : String): Boolean;
var
LoadError : LongWord;
x : Integer;
CorPath : String;
BinPath16 : String;
begin
Result := False;
BinPath16 := Format('%sx86\',[InBinPath]);
CorPath :=  Format('%sCorrection Files\Cor_1to1.ct5',[InBinPath]);
if DirectoryExists(InBinPath) then
  begin
  LoadError :=  init_rtc5_dll;
  if LoadError > 0 then
    begin
    DebugStatus(Format('Load Error : %d ',[LoadError]),2);
    x := get_dll_version();
    DebugStatus(Format('get_dll_version: %d ',[x]),2);
    free_rtc5_dll();
    if assigned(FOnError) then FOnError(Self,LoadError);
    end
  else
    begin
    FLoadedOK := True;
    DebugStatus(Format('Load OK : %d ',[LoadError]),2);
    x := get_dll_version();
    DebugStatus(Format('get_dll_version: %d ',[x]),2);
    x := acquire_rtc(1);
    DebugStatus(Format('acquired rtc: %d ',[x]),2);
    x := load_program_file(PAnsiChar(BinPath16));
    DebugStatus(Format('load_program_file from: %s : %d ',[InBinPath,x]),2);
    if x = 0 then
      begin
      x := load_correction_file(PAnsiChar(CorPath), 1, 2);
      if x = 0 then
        begin
        DebugStatus(Format('Corretion File Load Success = %d',[x]),2);
        end
      else
        begin
        DebugStatus(Format('Corretion File Load Fail = %d',[x]),2);
        end;

      end;

{    outPointer = ByteBuffer.allocateDirect(8);
    outPointer.order(ByteOrder.nativeOrder());
    status = ByteBuffer.allocateDirect(8);
    status.order(ByteOrder.nativeOrder());
    FComplete = true;}

    if assigned(FInstalled) then FInstalled(Self);
    end;
  end
else
  begin
  DebugStatus('No Scanner SetupFiles',2);
  end;
end;

destructor TScannerHead.Destoy;
begin
  FScanFileData.Free;
  FScanFileData := nil;
end;


procedure TScannerHead.SetParameters(SFileData: TScanFeatureData);
begin
with FScanFileData do
  begin
   ScanFile := SFileData.ScanFile;
   StockParams.CloneFrom(SFileData.StockParams);
  end;
if assigned(FOnHeadParamsChange) then FOnHeadParamsChange(Self);
end;

procedure TScannerHead.SetStockParameters(SParams: TScannerStockParameters);
begin
with FScanFileData do
  begin
  StockParams.CloneFrom(SParams);
  end;
if assigned(FOnHeadParamsChange) then FOnHeadParamsChange(Self);
end;

procedure TScannerHead.ReportSetttingsToStringList(OutList: TStrings; ClearFirst: Boolean);
begin
if ClearFirst then OutList.Clear;
OutList.Add(Format('Head X Posn. Scale %5.3f',[FXPosScale]));
OutList.Add(Format('Head Y Posn. Scale %5.3f',[FYPosScale]));
OutList.Add(Format('Head X Posn. Offset %d',[FXOffset]));
OutList.Add(Format('Head Y Posn. Offset %d',[FYOffset]));
OutList.Add(Format('Head Buffer Sizes %d/%d',[ListSize,ListSize]));
end;

procedure TScannerHead.SetFXOffset(const Value: Integer);
begin
  FXOffset := Value;
  if assigned(FOnHeadParamsChange) then FOnHeadParamsChange(Self);
end;

procedure TScannerHead.SetFXSpeedScale(const Value: Double);
begin
  FXSpeedScale := Value;
 if assigned(FOnHeadParamsChange) then FOnHeadParamsChange(Self);
end;

procedure TScannerHead.SetFYOffset(const Value: Integer);
begin
  FYOffset := Value;
  if assigned(FOnHeadParamsChange) then FOnHeadParamsChange(Self);
end;

procedure TScannerHead.SetFYSpeedScale(const Value: Double);
begin
  FYSpeedScale := Value;
  if assigned(FOnHeadParamsChange) then FOnHeadParamsChange(Self);
end;

procedure TScannerHead.SetFXPosScale(const Value: Double);
begin
  FXPosScale := Value;
  if assigned(FOnHeadParamsChange) then FOnHeadParamsChange(Self);
end;

procedure TScannerHead.SetFYPosScale(const Value: Double);
begin
  FYPosScale := Value;
  if assigned(FOnHeadParamsChange) then FOnHeadParamsChange(Self);
end;

end.
