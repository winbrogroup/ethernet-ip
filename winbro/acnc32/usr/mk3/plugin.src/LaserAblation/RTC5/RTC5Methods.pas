unit RTC5Methods;

interface

uses Classes, SysUtils, AbstractScript, INamedInterface, NamedPlugin,
     IScriptInterface, IStringArray, AbstractOPPPlugin, IFormInterface,
     ScannerSequencer,RTC5ScanManager,IniFiles;

var
  GlobalConfigFileName : String;

implementation
type
  TScanForm =  class(TInstallForm);

  TRTC5LoadFileMethod = class(TScriptMethod)
    function  Name: WideString; override; stdcall;
    procedure Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer); override; stdcall;
    procedure Reset; override; stdcall;
    function ParameterCount: Integer; override;
    function IsFunction: Boolean; override;
  end;

  TRTC5SetSpeedMethod = class(TScriptMethod)
    function  Name: WideString; override; stdcall;
    procedure Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer); override; stdcall;
    procedure Reset; override; stdcall;
    function ParameterCount: Integer; override;
    function IsFunction: Boolean; override;
  end;

  TRTC5SetDelayMethod = class(TScriptMethod)
    function  Name: WideString; override; stdcall;
    procedure Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer); override; stdcall;
    procedure Reset; override; stdcall;
    function ParameterCount: Integer; override;
    function IsFunction: Boolean; override;
  end;

  TRTC5SetLaserDelayMethod = class(TScriptMethod)
    function  Name: WideString; override; stdcall;
    procedure Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer); override; stdcall;
    procedure Reset; override; stdcall;
    function ParameterCount: Integer; override;
    function IsFunction: Boolean; override;
  end;

  TRTC5SetRotationMethod = class(TScriptMethod)
    function  Name: WideString; override; stdcall;
    procedure Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer); override; stdcall;
    procedure Reset; override; stdcall;
    function ParameterCount: Integer; override;
    function IsFunction: Boolean; override;
  end;

  TRTC5SetGroupMethod = class(TScriptMethod)
    function  Name: WideString; override; stdcall;
    procedure Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer); override; stdcall;
    procedure Reset; override; stdcall;
    function ParameterCount: Integer; override;
    function IsFunction: Boolean; override;
  end;

   TRTC5SetHatchMethod  = class(TScriptMethod)
    function  Name: WideString; override; stdcall;
    procedure Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer); override; stdcall;
    procedure Reset; override; stdcall;
    function ParameterCount: Integer; override;
    function IsFunction: Boolean; override;
  end;

   TRTC5SetProfileMethod   = class(TScriptMethod)
    function  Name: WideString; override; stdcall;
    procedure Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer); override; stdcall;
    procedure Reset; override; stdcall;
    function ParameterCount: Integer; override;
    function IsFunction: Boolean; override;
  end;

   TRTC5StartMethod   = class(TScriptMethod)
    function  Name: WideString; override; stdcall;
    procedure Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer); override; stdcall;
    procedure Reset; override; stdcall;
    function ParameterCount: Integer; override;
    function IsFunction: Boolean; override;
  end;

   TRTC5ResetMethod   = class(TScriptMethod)
    function  Name: WideString; override; stdcall;
    procedure Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer); override; stdcall;
    procedure Reset; override; stdcall;
    function ParameterCount: Integer; override;
    function IsFunction: Boolean; override;
  end;

   TRTC5IsCompleteMethod   = class(TScriptMethod)
    function  Name: WideString; override; stdcall;
    procedure Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer); override; stdcall;
    procedure Reset; override; stdcall;
    function ParameterCount: Integer; override;
    function IsFunction: Boolean; override;
  end;

   TRTC5SetSpeedScaleMethod   = class(TScriptMethod)
    function  Name: WideString; override; stdcall;
    procedure Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer); override; stdcall;
    procedure Reset; override; stdcall;
    function ParameterCount: Integer; override;
    function IsFunction: Boolean; override;
  end;

   TRTC5SetPosScaleMethod   = class(TScriptMethod)
    function  Name: WideString; override; stdcall;
    procedure Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer); override; stdcall;
    procedure Reset; override; stdcall;
    function ParameterCount: Integer; override;
    function IsFunction: Boolean; override;
  end;

   TRTC5SetOffsetMethod   = class(TScriptMethod)
    function  Name: WideString; override; stdcall;
    procedure Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer); override; stdcall;
    procedure Reset; override; stdcall;
    function ParameterCount: Integer; override;
    function IsFunction: Boolean; override;
  end;

function HeadDataExists : Boolean;
begin
Result := false;
if assigned(RTC5Manager) then
     if assigned(RTC5Manager.ScanSequencer) then
        if assigned(RTC5Manager.ScanSequencer.BufferScanFileData) then
          Result := True;

end;

{ TRTC5LoadFileMethod }

function TRTC5LoadFileMethod.Name: WideString;
begin
Result := 'LoadFromFile';
end;

function TRTC5LoadFileMethod.ParameterCount: Integer;
begin
Result := 1;
end;

procedure TRTC5LoadFileMethod.Reset;
begin
  inherited;

end;

function TRTC5LoadFileMethod.IsFunction: Boolean;
begin
Result := True;
end;

procedure TRTC5LoadFileMethod.Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer);
var
Buffer  : array[0..256] of Char;
FName : String;
begin
  inherited;
  Named.GetAsString(Params[0], Buffer, 256);
  FName := Buffer;
  if FileExists(FName) then
    begin
    if HeadDataExists then
      begin
      RTC5Manager.ScanSequencer.BufferScanFileData.LoadFromFile(FName);
      end;
    end
  else
    begin
    raise Exception.Create(Format('Scan File Does not exist %s',[ExtractFilename(FName)]));
    end
end;

{ TRTC5SetSpeedMethod }

function TRTC5SetSpeedMethod.Name: WideString;
begin
Result := 'SetSpeed';
end;

function TRTC5SetSpeedMethod.ParameterCount: Integer;
begin
Result := 2;
end;

procedure TRTC5SetSpeedMethod.Reset;
begin
  inherited;

end;

function TRTC5SetSpeedMethod.IsFunction: Boolean;
begin
Result := False;
end;

procedure TRTC5SetSpeedMethod.Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer);

begin
  inherited;
  if HeadDataExists then
    begin
    with RTC5Manager.ScanSequencer.BufferScanFileData.StockParams do
      begin
      mMarkSpeed := Named.GetAsInteger(Params[0]);
      mJumpSpeed := Named.GetAsInteger(Params[1]);
      END;
    end;
end;

{ TRTC5SetDelayMethod }

function TRTC5SetDelayMethod.Name: WideString;
begin
Result := 'SetDelay';
end;

function TRTC5SetDelayMethod.ParameterCount: Integer;
begin
Result := 3;
end;

procedure TRTC5SetDelayMethod.Reset;
begin
  inherited;

end;

function TRTC5SetDelayMethod.IsFunction: Boolean;
begin
Result := False;
end;

procedure TRTC5SetDelayMethod.Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer);
begin
  inherited;
  if HeadDataExists then
    begin
    with RTC5Manager.ScanSequencer.BufferScanFileData.StockParams do
      begin
      mMarkDelay := Named.GetAsInteger(Params[0]);
      mJumpDelay := Named.GetAsInteger(Params[1]);
      mPolygonDelay := Named.GetAsInteger(Params[2]);
      end;
    end;

end;

{ TRTC5SetLaserDelayMethod }

function TRTC5SetLaserDelayMethod.Name: WideString;
begin
Result := 'SetLaserDelay';
end;

function TRTC5SetLaserDelayMethod.ParameterCount: Integer;
begin
Result := 4;
end;

procedure TRTC5SetLaserDelayMethod.Reset;
begin
  inherited;

end;

function TRTC5SetLaserDelayMethod.IsFunction: Boolean;
begin
Result := False;
end;

procedure TRTC5SetLaserDelayMethod.Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer);
begin
  inherited;
  if HeadDataExists then
    begin
    with RTC5Manager.ScanSequencer.BufferScanFileData.StockParams do
      begin
      mLaserFirstDelay := Named.GetAsInteger(Params[0]);
      mLaserShutterDelay := Named.GetAsInteger(Params[1]);
      mLaserOnDelay := Named.GetAsInteger(Params[2]);
      mLaserOffDelay := Named.GetAsInteger(Params[3]);
      end;
    end;
end;

{ TRTC5SetRotationMethod }

function TRTC5SetRotationMethod.Name: WideString;
begin
Result := 'SetRotation';
end;

function TRTC5SetRotationMethod.ParameterCount: Integer;
begin
Result := 1;
end;

procedure TRTC5SetRotationMethod.Reset;
begin
  inherited;

end;

function TRTC5SetRotationMethod.IsFunction: Boolean;
begin
Result := False;
end;

procedure TRTC5SetRotationMethod.Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer);
begin
  inherited;
  if HeadDataExists then
    begin
    RTC5Manager.ScanSequencer.BufferScanFileData.StockParams.mRotation := Named.GetAsDouble(Params[0]);
    end;
end;

{ TRTC5SetGroupMethod }

function TRTC5SetGroupMethod.Name: WideString;
begin
Result := 'SetGroup';
end;

function TRTC5SetGroupMethod.ParameterCount: Integer;
begin
Result := 4;
end;

procedure TRTC5SetGroupMethod.Reset;
begin
  inherited;

end;

function TRTC5SetGroupMethod.IsFunction: Boolean;
begin
Result := False;
end;

procedure TRTC5SetGroupMethod.Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer);
begin
  inherited;
  if HeadDataExists then
    begin
    with RTC5Manager.ScanSequencer.BufferScanFileData.StockParams do
      begin
      mGroupSliceStart := Named.GetAsInteger(Params[0]);
      mGroupSliceEnd := Named.GetAsInteger(Params[1]);
      mGroupSlicePasses := Named.GetAsInteger(Params[2]);
      mGroupDwell := Named.GetAsInteger(Params[3]);
      end;
    end;
end;

{ TRTC5SetHatchMethod }

function TRTC5SetHatchMethod.Name: WideString;
begin
Result := 'SetHatch';
end;

function TRTC5SetHatchMethod.ParameterCount: Integer;
begin
Result := 1;
end;

procedure TRTC5SetHatchMethod.Reset;
begin
  inherited;

end;

function TRTC5SetHatchMethod.IsFunction: Boolean;
begin
Result := False;
end;

procedure TRTC5SetHatchMethod.Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer);
begin
  inherited;
  if HeadDataExists then
    begin
    RTC5Manager.ScanSequencer.BufferScanFileData.StockParams.mHatchCount := Named.GetAsInteger(Params[0]);
    end;

end;

{ TRTC5SetProfileMethod }

function TRTC5SetProfileMethod.Name: WideString;
begin
Result := 'SetProfile';
end;

function TRTC5SetProfileMethod.ParameterCount: Integer;
begin
Result := 5;
end;

procedure TRTC5SetProfileMethod.Reset;
begin
  inherited;

end;

function TRTC5SetProfileMethod.IsFunction: Boolean;
begin
Result := False;
end;

procedure TRTC5SetProfileMethod.Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer);
begin
  inherited;
  if HeadDataExists then
    begin
    with RTC5Manager.ScanSequencer.BufferScanFileData.StockParams do
      begin
      mProfileStartCount := Named.GetAsInteger(Params[0]);
      mProfileEndCount := Named.GetAsInteger(Params[1]);
      mProfileEvery := Named.GetAsInteger(Params[2]);
      mProfilePasses := Named.GetAsInteger(Params[3]);
      mProfileDwell := Named.GetAsInteger(Params[4]);
      END;
    end;

end;

{ TRTC5StartMethod }

function TRTC5StartMethod.Name: WideString;
begin
Result := 'Start';
end;

function TRTC5StartMethod.ParameterCount: Integer;
begin
Result := 0;
end;

procedure TRTC5StartMethod.Reset;
begin
  inherited;
  if assigned(RTC5Manager) then
   begin
   if assigned(RTC5Manager.ScanSequencer) then
    begin
    RTC5Manager.aInterruptRequest;
    end;
   end;
end;

function TRTC5StartMethod.IsFunction: Boolean;
begin
Result := False;
end;


procedure TRTC5StartMethod.Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer);
begin
  inherited;
  RTC5Manager.SetScannerParameters;
  try
  RTC5Manager.ScanSequencer.StartScan;
  except

  end;
end;

{ TRTC5ResetMethod }

function TRTC5ResetMethod.Name: WideString;
begin
Result := 'Reset';
end;

function TRTC5ResetMethod.ParameterCount: Integer;
begin
Result := 0;
end;

procedure TRTC5ResetMethod.Reset;
begin
  inherited;

end;

function TRTC5ResetMethod.IsFunction: Boolean;
begin
Result := False;
end;

procedure TRTC5ResetMethod.Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer);
begin
  inherited;
  if assigned(RTC5Manager) then
    begin
    RTC5Manager.ScanSequencer.ResetScanner;
    end;
end;

{ TRTC5IsCompleteMethod }

function TRTC5IsCompleteMethod.Name: WideString;
begin
Result := 'IsComplete';
end;

function TRTC5IsCompleteMethod.ParameterCount: Integer;
begin
Result := 0;
end;

procedure TRTC5IsCompleteMethod.Reset;
begin
  inherited;

end;

function TRTC5IsCompleteMethod.IsFunction: Boolean;
begin
Result := True;
end;

procedure TRTC5IsCompleteMethod.Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer);
begin
  inherited;
  Named.SetAsBoolean(Res,True);
  if assigned(RTC5Manager.ScanSequencer) then
    begin
    if not RTC5Manager.ScanSequencer.Head.ScanComplete then
      begin
      if not RTC5Manager.ScanSequencer.Head.CheckComplete then
        begin
        Named.SetAsBoolean(Res,False);
        end;
      end;
    end;
{ TODO : Finish implementing }
end;

{ TRTC5SetScaleMethod }

function TRTC5SetSpeedScaleMethod.Name: WideString;
begin
Result := 'SetXYSpeedScale';
end;

function TRTC5SetSpeedScaleMethod.ParameterCount: Integer;
begin
Result := 2;
end;

procedure TRTC5SetSpeedScaleMethod.Reset;
begin
  inherited;
end;

function TRTC5SetSpeedScaleMethod.IsFunction: Boolean;
begin
Result := False;
end;

procedure TRTC5SetSpeedScaleMethod.Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer);
var
ConfigFile : TIniFile;
begin
  inherited;
  if assigned(RTC5Manager.ScanSequencer) then
    begin
    RTC5Manager.ScanSequencer.Head.XSpeedScale := Named.GetAsDouble(Params[0]);
    RTC5Manager.ScanSequencer.Head.YSpeedScale := Named.GetAsDouble(Params[1]);
    if FileExists(GlobalConfigFileName) then
      begin
      ConfigFile := TIniFile.Create(GlobalConfigFileName);
      ConfigFile.WriteFloat('GENERAL','XSpeedScale',RTC5Manager.ScanSequencer.Head.XSpeedScale);
      ConfigFile.WriteFloat('GENERAL','YSpeedScale',RTC5Manager.ScanSequencer.Head.YSpeedScale);
      ConfigFile.UpdateFile;
      try
      finally
      ConfigFile.Free;
      end;
      end
    else
     begin
     raise Exception.Create('Could not write Head Speed scale to config file');
     end;
    end;
end;

{ TRTC5SetOffsetMethod }

function TRTC5SetOffsetMethod.Name: WideString;
begin
Result := 'SetXYOffset'
end;

function TRTC5SetOffsetMethod.ParameterCount: Integer;
begin
Result := 2;
end;

procedure TRTC5SetOffsetMethod.Reset;
begin
  inherited;

end;

function TRTC5SetOffsetMethod.IsFunction: Boolean;
begin
Result := False;
end;

procedure TRTC5SetOffsetMethod.Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer);
var
ConfigFile : TIniFile;
begin
  inherited;
  if assigned(RTC5Manager.ScanSequencer) then
    begin
    RTC5Manager.ScanSequencer.Head.XOffset := Named.GetAsInteger(Params[0]);
    RTC5Manager.ScanSequencer.Head.YOffset := Named.GetAsInteger(Params[1]);
    if FileExists(GlobalConfigFileName) then
      begin
      ConfigFile := TIniFile.Create(GlobalConfigFileName);
      ConfigFile.WriteFloat('GENERAL','XOffset',RTC5Manager.ScanSequencer.Head.XOffset);
      ConfigFile.WriteFloat('GENERAL','YOffset',RTC5Manager.ScanSequencer.Head.YOffset);
      ConfigFile.UpdateFile;
      try
      finally
      ConfigFile.Free;
      end;
      end
    else
     begin
     raise Exception.Create('Could not write Head Offset to config file');
     end;
    end;

end;

{ TRTC5SetPosScaleMethod }

function TRTC5SetPosScaleMethod.Name: WideString;
begin
Result := 'SetXYPosScale';
end;

function TRTC5SetPosScaleMethod.ParameterCount: Integer;
begin
Result := 2;
end;

procedure TRTC5SetPosScaleMethod.Reset;
begin
  inherited;

end;

function TRTC5SetPosScaleMethod.IsFunction: Boolean;
begin
Result := False;
end;

procedure TRTC5SetPosScaleMethod.Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer);
var
ConfigFile : TIniFile;
begin
  inherited;
  if assigned(RTC5Manager.ScanSequencer) then
    begin
    RTC5Manager.ScanSequencer.Head.XPosScale := Named.GetAsDouble(Params[0]);
    RTC5Manager.ScanSequencer.Head.YPosScale := Named.GetAsDouble(Params[1]);
    if FileExists(GlobalConfigFileName) then
      begin
      ConfigFile := TIniFile.Create(GlobalConfigFileName);
      ConfigFile.WriteFloat('GENERAL','XPosScale',RTC5Manager.ScanSequencer.Head.XPosScale);
      ConfigFile.WriteFloat('GENERAL','YPosScale',RTC5Manager.ScanSequencer.Head.YPosScale);
      ConfigFile.UpdateFile;
      try
      finally
      ConfigFile.Free;
      end;
      end
    else
     begin
     raise Exception.Create('Could not write Head Pos scale to config file');
     end;
    end;
end;

initialization
  TDefaultScriptLibrary.SetName('Ablation');
  TDefaultScriptLibrary.AddMethod(TRTC5SetSpeedMethod);
  TDefaultScriptLibrary.AddMethod(TRTC5SetDelayMethod);
  TDefaultScriptLibrary.AddMethod(TRTC5SetLaserDelayMethod);
  TDefaultScriptLibrary.AddMethod(TRTC5SetRotationMethod);
  TDefaultScriptLibrary.AddMethod(TRTC5SetGroupMethod);
  TDefaultScriptLibrary.AddMethod(TRTC5SetHatchMethod);
  TDefaultScriptLibrary.AddMethod(TRTC5SetProfileMethod);
  TDefaultScriptLibrary.AddMethod(TRTC5StartMethod);
  TDefaultScriptLibrary.AddMethod(TRTC5ResetMethod);
  TDefaultScriptLibrary.AddMethod(TRTC5IsCompleteMethod);
  TDefaultScriptLibrary.AddMethod(TRTC5LoadFileMethod);
  TDefaultScriptLibrary.AddMethod(TRTC5SetSpeedScaleMethod);
  TDefaultScriptLibrary.AddMethod(TRTC5SetPosScaleMethod);
  TDefaultScriptLibrary.AddMethod(TRTC5SetOffsetMethod);
end.
