object RTC5ScannerForm: TRTC5ScannerForm
  Left = 0
  Top = 0
  BorderStyle = bsNone
  Caption = 'RTC5ScannerForm'
  ClientHeight = 526
  ClientWidth = 1160
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  OnClose = FormClose
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object DummyPageControl: TPageControl
    Left = 248
    Top = 0
    Width = 913
    Height = 526
    ActivePage = GeneralTabSheet
    TabOrder = 0
    Visible = False
    OnResize = DummyPageControlResize
    object GeneralTabSheet: TTabSheet
      Caption = 'General'
      object ParamsParentPanel: TPanel
        Left = 0
        Top = 0
        Width = 889
        Height = 498
        Align = alLeft
        Caption = 'ParamsParentPanel'
        TabOrder = 0
        object ParamsMainPanel: TPanel
          Left = 72
          Top = 1
          Width = 816
          Height = 496
          Align = alRight
          BevelInner = bvLowered
          TabOrder = 0
          object Label1: TLabel
            Left = 8
            Top = 8
            Width = 134
            Height = 13
            Caption = 'Active Scanner Parameters:'
          end
          object RunningIndicator: TShape
            Left = 640
            Top = 8
            Width = 10
            Height = 10
            Brush.Color = clRed
            Shape = stCircle
          end
          object Label26: TLabel
            Left = 152
            Top = 8
            Width = 465
            Height = 13
            Caption = 'Label26'
          end
          object GroupBox1: TGroupBox
            Left = 10
            Top = 25
            Width = 120
            Height = 70
            Caption = 'Laser'
            TabOrder = 0
            object Label2: TLabel
              Left = 10
              Top = 20
              Width = 26
              Height = 13
              Caption = 'Freq:'
            end
            object Label3: TLabel
              Left = 10
              Top = 44
              Width = 20
              Height = 13
              Caption = 'Pxr:'
            end
            object Label13: TLabel
              Left = 100
              Top = 20
              Width = 16
              Height = 13
              Caption = 'Hz.'
            end
            object Label14: TLabel
              Left = 100
              Top = 44
              Width = 10
              Height = 13
              Caption = 'W'
            end
            object LaserFreqEdit: TEdit
              Left = 40
              Top = 16
              Width = 55
              Height = 18
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -8
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
              ReadOnly = True
              TabOrder = 0
              Text = '--'
            end
            object LaserPowerEdit: TEdit
              Left = 40
              Top = 40
              Width = 55
              Height = 18
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -8
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
              ReadOnly = True
              TabOrder = 1
              Text = '--'
            end
          end
          object GroupBox2: TGroupBox
            Left = 135
            Top = 25
            Width = 120
            Height = 70
            Caption = 'Speeds- mm/sec'
            TabOrder = 1
            object Label4: TLabel
              Left = 10
              Top = 20
              Width = 27
              Height = 13
              Caption = 'Mark:'
            end
            object Label5: TLabel
              Left = 10
              Top = 44
              Width = 29
              Height = 13
              Caption = 'Jump:'
            end
            object MarkSpeedEdit: TEdit
              Left = 50
              Top = 16
              Width = 55
              Height = 18
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -8
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
              ReadOnly = True
              TabOrder = 0
              Text = '--'
            end
            object JumpSpeedEdit: TEdit
              Left = 50
              Top = 40
              Width = 55
              Height = 18
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -8
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
              ReadOnly = True
              TabOrder = 1
              Text = '--'
            end
          end
          object GroupBox3: TGroupBox
            Left = 260
            Top = 25
            Width = 200
            Height = 70
            Caption = 'Delays '
            TabOrder = 2
            object Label6: TLabel
              Left = 10
              Top = 20
              Width = 27
              Height = 13
              Caption = 'Mark:'
            end
            object Label7: TLabel
              Left = 10
              Top = 44
              Width = 29
              Height = 13
              Caption = 'Jump:'
            end
            object Label8: TLabel
              Left = 100
              Top = 20
              Width = 24
              Height = 13
              Caption = 'Poly:'
            end
            object Label9: TLabel
              Left = 100
              Top = 44
              Width = 44
              Height = 13
              Caption = 'Shuttter:'
            end
            object JumpDelayEdit: TEdit
              Left = 45
              Top = 40
              Width = 45
              Height = 18
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -8
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
              ReadOnly = True
              TabOrder = 0
              Text = '--'
            end
            object ShutterDelayEdit: TEdit
              Left = 146
              Top = 40
              Width = 45
              Height = 18
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -8
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
              ReadOnly = True
              TabOrder = 1
              Text = '--'
            end
            object MarkDelayEdit: TEdit
              Left = 45
              Top = 16
              Width = 45
              Height = 18
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -8
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
              ReadOnly = True
              TabOrder = 2
              Text = '--'
            end
            object PolyDelayEdit: TEdit
              Left = 146
              Top = 16
              Width = 45
              Height = 18
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -8
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
              ReadOnly = True
              TabOrder = 3
              Text = '--'
            end
          end
          object GroupBox4: TGroupBox
            Left = 465
            Top = 25
            Width = 190
            Height = 70
            Caption = 'Laser Delays'
            TabOrder = 3
            object Label10: TLabel
              Left = 10
              Top = 20
              Width = 27
              Height = 13
              Caption = 'Mark:'
            end
            object Label11: TLabel
              Left = 10
              Top = 44
              Width = 29
              Height = 13
              Caption = 'Jump:'
            end
            object Label12: TLabel
              Left = 100
              Top = 20
              Width = 15
              Height = 13
              Caption = '1st'
            end
            object LaserOnDelayEdit: TEdit
              Left = 45
              Top = 16
              Width = 45
              Height = 18
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -8
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
              ReadOnly = True
              TabOrder = 0
              Text = '--'
            end
            object LaserFirstDelayEdit: TEdit
              Left = 135
              Top = 16
              Width = 45
              Height = 18
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -8
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
              ReadOnly = True
              TabOrder = 1
              Text = '--'
            end
            object LaserOffDelayEdit: TEdit
              Left = 45
              Top = 40
              Width = 45
              Height = 18
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -8
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
              ReadOnly = True
              TabOrder = 2
              Text = '--'
            end
          end
          object GroupBox5: TGroupBox
            Left = 10
            Top = 100
            Width = 110
            Height = 93
            Caption = 'Geometry'
            TabOrder = 4
            object Label15: TLabel
              Left = 10
              Top = 19
              Width = 27
              Height = 13
              Caption = 'Rotn:'
            end
            object RotationEdit: TEdit
              Left = 45
              Top = 16
              Width = 55
              Height = 18
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -8
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
              ReadOnly = True
              TabOrder = 0
              Text = 'Target'
            end
          end
          object GroupBox6: TGroupBox
            Left = 130
            Top = 100
            Width = 167
            Height = 93
            Caption = 'Group Sequence'
            TabOrder = 5
            object Label16: TLabel
              Left = 10
              Top = 19
              Width = 27
              Height = 13
              Caption = 'ZSlice'
            end
            object Label17: TLabel
              Left = 95
              Top = 19
              Width = 10
              Height = 13
              Caption = 'to'
            end
            object Label18: TLabel
              Left = 10
              Top = 43
              Width = 86
              Height = 13
              Caption = 'Number of passes'
            end
            object Label19: TLabel
              Left = 10
              Top = 67
              Width = 60
              Height = 13
              Caption = 'Dwell (msec)'
            end
            object ZSliceEdit: TEdit
              Left = 45
              Top = 16
              Width = 35
              Height = 18
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -8
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
              ReadOnly = True
              TabOrder = 0
              Text = '--'
            end
            object ZSliceToEdit: TEdit
              Left = 120
              Top = 16
              Width = 35
              Height = 18
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -8
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
              ReadOnly = True
              TabOrder = 1
              Text = '--'
            end
            object PassesEdit: TEdit
              Left = 120
              Top = 40
              Width = 35
              Height = 18
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -8
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
              ReadOnly = True
              TabOrder = 2
              Text = '--'
            end
            object GroupDwellEdit: TEdit
              Left = 120
              Top = 64
              Width = 35
              Height = 18
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -8
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
              ReadOnly = True
              TabOrder = 3
              Text = '--'
            end
          end
          object GroupBox7: TGroupBox
            Left = 310
            Top = 100
            Width = 345
            Height = 93
            Caption = 'Hatch/Profile Sequence'
            TabOrder = 6
            object Label20: TLabel
              Left = 10
              Top = 19
              Width = 64
              Height = 13
              Caption = 'Hatch Passes'
            end
            object Label22: TLabel
              Left = 10
              Top = 43
              Width = 66
              Height = 13
              Caption = 'Start Profiles:'
            end
            object Label23: TLabel
              Left = 10
              Top = 67
              Width = 56
              Height = 13
              Caption = 'End Profiles'
            end
            object Label21: TLabel
              Left = 210
              Top = 19
              Width = 70
              Height = 13
              Caption = 'profiles every:'
            end
            object Label24: TLabel
              Left = 160
              Top = 43
              Width = 91
              Height = 13
              Caption = 'Target Depth (mm)'
            end
            object Label25: TLabel
              Left = 160
              Top = 67
              Width = 89
              Height = 13
              Caption = 'Zslice dwell (mSec)'
            end
            object NumProfilesEdit: TEdit
              Left = 160
              Top = 16
              Width = 35
              Height = 18
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -8
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
              ReadOnly = True
              TabOrder = 0
              Text = '--'
            end
            object HPassesEdit: TEdit
              Left = 80
              Top = 16
              Width = 35
              Height = 18
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -8
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
              ReadOnly = True
              TabOrder = 1
              Text = '--'
            end
            object StartProfileEdit: TEdit
              Left = 80
              Top = 40
              Width = 35
              Height = 18
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -8
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
              ReadOnly = True
              TabOrder = 2
              Text = '--'
            end
            object EndProfileEdit: TEdit
              Left = 80
              Top = 64
              Width = 35
              Height = 18
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -8
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
              ReadOnly = True
              TabOrder = 3
              Text = '--'
            end
            object EveryEdit: TEdit
              Left = 300
              Top = 16
              Width = 35
              Height = 18
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -8
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
              ReadOnly = True
              TabOrder = 4
              Text = '--'
            end
            object TargetDepthEdit: TEdit
              Left = 300
              Top = 40
              Width = 35
              Height = 18
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -8
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
              ReadOnly = True
              TabOrder = 5
              Text = '--'
            end
            object ZSliceDwellEdit: TEdit
              Left = 300
              Top = 64
              Width = 35
              Height = 18
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -8
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
              ReadOnly = True
              TabOrder = 6
              Text = '--'
            end
          end
          object DebugButton: TButton
            Left = 728
            Top = 8
            Width = 75
            Height = 25
            Caption = 'Debug'
            TabOrder = 7
            OnClick = DebugButtonClick
          end
        end
      end
    end
    object DebugTabSheet: TTabSheet
      Caption = 'Debug'
      ImageIndex = 1
      object DEbugPanel: TPanel
        Left = 0
        Top = 0
        Width = 905
        Height = 498
        Align = alClient
        Caption = 'DEbugPanel'
        TabOrder = 0
        object ReportMemo: TMemo
          Left = 91
          Top = 1
          Width = 813
          Height = 496
          Align = alClient
          Lines.Strings = (
            '')
          TabOrder = 0
        end
        object Panel1: TPanel
          Left = 1
          Top = 1
          Width = 90
          Height = 496
          Align = alLeft
          TabOrder = 1
          object Button1: TButton
            Left = 2
            Top = 4
            Width = 85
            Height = 25
            Caption = 'Report File'
            TabOrder = 0
            OnClick = Button1Click
          end
          object Button3: TButton
            Left = 2
            Top = 64
            Width = 85
            Height = 25
            Caption = 'Clear'
            TabOrder = 1
            OnClick = Button3Click
          end
          object Button4: TButton
            Left = 2
            Top = 32
            Width = 85
            Height = 25
            Caption = 'Report Settings'
            TabOrder = 2
            OnClick = Button4Click
          end
          object StatDisplay: TEdit
            Left = 2
            Top = 160
            Width = 80
            Height = 21
            ReadOnly = True
            TabOrder = 3
          end
          object ResetButton: TButton
            Left = 2
            Top = 96
            Width = 85
            Height = 25
            Caption = 'Reset'
            TabOrder = 4
            OnClick = ResetButtonClick
          end
          object DebugIn: TEdit
            Left = 2
            Top = 192
            Width = 30
            Height = 21
            TabOrder = 5
            Text = '0'
          end
          object Button6: TButton
            Left = 2
            Top = 216
            Width = 85
            Height = 25
            Caption = 'Set Debug'
            TabOrder = 6
            OnClick = Button6Click
          end
          object DebugBackButton: TButton
            Left = 8
            Top = 264
            Width = 75
            Height = 25
            Caption = 'Back'
            TabOrder = 7
            OnClick = DebugBackButtonClick
          end
        end
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'TabSheet2'
      ImageIndex = 2
      object Panel2: TPanel
        Left = 0
        Top = 0
        Width = 905
        Height = 498
        Align = alClient
        BevelInner = bvLowered
        Caption = 'Panel2'
        TabOrder = 0
      end
    end
  end
  object MainPanel: TPanel
    Left = 0
    Top = 0
    Width = 233
    Height = 526
    Align = alLeft
    Caption = 'MainPanel'
    TabOrder = 1
    OnResize = MainPanelResize
    object SoftkeyPanel: TPanel
      Left = 1
      Top = 1
      Width = 137
      Height = 524
      Align = alLeft
      BevelInner = bvLowered
      BevelOuter = bvLowered
      BorderWidth = 3
      Caption = 'SoftkeyPanel'
      TabOrder = 0
    end
  end
end
