unit ScannerSequencer;

interface

uses Classes, RTC5Import, Windows, ScannerHead,ScanData,SysUtils,ExtCtrls;

type
  TScanStages = (tssIdle,tssStartRequest,tssSequenceSent,tssFinished,tssError,tssUnrecoverableError);
  TScanRunChangeEvent = procedure(Sender : Tobject; Running : Boolean) of object;
  TDScanRunErrorEvent = procedure(Sender : Tobject; ErrorString : String) of object;

  TThreadNameInfo = record
    FType: LongWord;     // must be 0x1000
    FName: PChar;        // pointer to name (in user address space)
    FThreadID: LongWord; // thread ID (-1 indicates caller thread)
    FFlags: LongWord;    // reserved for future use, must be zero
  end;

  TScannerSequencer = class(TThread)
  private
    StartableTimer : TTimer;
    InterruptTimer : TTimer;
    StartCount : INteger;
    LastBlockState : Boolean;
    FScannerHead: TScannerHead;
    FOnDebug: TScannerDebugEvent;
    FScanStage : TScanStages;
    FLastStage : TScanStages;
    FStartable : Boolean;
    FOnTimeout: TNotifyEvent;
    FInterruptRequest : Boolean;
    FOnStartable: TNotifyEvent;
    FOnScannerReset : TNotifyEvent;
    FOnHeadParamsChange: TNotifyEvent;
    FOnRunStatusChange: TScanRunChangeEvent;
    FProgramRunning: Boolean;
    FScannerMoving: Boolean;
    FOnHeadInstalledOK: TNotifyEvent;
    procedure HeadInstalled(Sender : TObject);
    procedure SetName;
    procedure ReportDebug(DebugString : String;Level : Integer);
    procedure RunScanSequence;
    function RunSlice(index: Integer; Isprofile: boolean): Boolean;
    function CheckBlocking: Boolean;
    procedure SetInterruptRequest(const Value: Boolean);
    procedure StartTimerTimer(Sender : TObject);
    procedure InterruptDone(Sender : TObject);
    function GetScanStageText: String;
    procedure SliceListCleaned(Sender : TObject);
    procedure aHeadParamsChange(Sender : TObject);
    procedure Delay(DelMicroseconds : Integer);

  public
    BufferScanFileData : TScanFeatureData;
    procedure Execute; override;
    constructor Create;
    destructor Destroy; override;
    procedure StartScan;
    procedure ResetScanner;

    property Head : TScannerHead read FScannerHead;
    property OnDebug : TScannerDebugEvent read FOnDebug write FOnDebug;
    property OnTimeout : TNotifyEvent read FOnTimeout write FOnTimeout;
    property OnStartableRise : TNotifyEvent read FOnStartable write FOnStartable;
    property OnScannerReset : TNotifyEvent read FOnScannerReset write FOnScannerReset;
    property ScanStageText : String read GetScanStageText;
    property Startable : Boolean read FStartable;
    property ScanInterruptRequest : Boolean read FInterruptRequest write SetInterruptRequest;
    property OnHeadParamsChange : TNotifyEvent read FOnHeadParamsChange write FOnHeadParamsChange;
    property OnRunStatusChange : TScanRunChangeEvent read FOnRunStatusChange write FOnRunStatusChange;
    property ProgramRunning : Boolean read FProgramRunning;
    property ScannerMoving : Boolean read FScannerMoving;
    property OnHeadInstalledOK : TNotifyEvent read FOnHeadInstalledOK write FOnHeadInstalledOK;
  end;

implementation


{ TScannerSequencer }

constructor TScannerSequencer.Create;
begin
  inherited Create(True);
  FOnHeadInstalledOK := nil;
  FProgramRunning := False;
  FOnRunStatusChange := nil;
  StartableTimer := TTimer.Create(nil);
  with StartableTimer do
    begin
    Enabled := false;
    Interval := 50;
    OnTimer := StartTimerTimer;
    end;
  InterruptTimer := TTimer.Create(nil);
  with InterruptTimer do
    begin
    Enabled := false;
    Interval := 400;
    OnTimer := InterruptDone;
    end;

  // Do some initialization
  BufferScanFileData := TScanFeatureData.Create;
  FScannerHead := TScannerHead.Create;
  FScannerHead.OnInstalled := HeadInstalled;
  FScannerHead.OnHeadParamsChange := aHeadParamsChange;
  FScanStage := tssIdle;
  FLastStage := tssIdle;
  FStartable := False;
end;

procedure TScannerSequencer.Execute;
var
StageString : String;
count : Integer;
begin
  SetName;
  Count := 0;
  while(not Terminated) do begin
    // Do something sensible
    case FScanStage of
      tssIdle:
        begin
        ScanInterruptRequest := False;
        StageString := 'Idle';
        FProgramRunning := False;
        FScannerMoving := False;
        count := 0;
        end;

      tssStartRequest:
        begin
        // start loading slices and wait for runnable to become true
        count := 0;
        ScanInterruptRequest := False;
        StageString := 'Scanner Running Stage';
        try
        FProgramRunning := True;
        if assigned(FOnRunStatusChange) then FOnRunStatusChange(Self,True);
        ReportDebug('Set Running True',4);
        RunScanSequence;
        FScanStage := tssSequenceSent;
        if not FStartable then
          begin
          FStartable := True;
          if assigned(FOnStartable) then FOnStartable(Self);
          end;
        except
        FStartable := False;
        FScanStage := tssError;
        end;
        end;

      tssSequenceSent:
        begin
        if not Head.ScanComplete then
          begin
          inc(Count);
          FScannerMoving := True;
          if Count > 1000000000 then
              begin
              StageString := 'Error';
              FScanStage := tssError;
              end
          end
        else
          begin
          FScanStage := tssFinished;
          FScannerMoving := False;
          StageString := 'Finished 2';
          ReportDebug('Finished',4);
          end;
        end;

      tssFinished:
        begin
        count := 0;
        StageString := 'Finished 3';
        FScanStage := tssidle;
        // stays here until restarted
        end;

      tssError:
        begin
        StageString := 'Idle';
        FScanStage := tssIdle;
        end;

      tssUnrecoverableError:
        begin
        StageString := 'Unrecoverable Error';
        end;
     end; // case

    if FScanStage <> FLastStage then
      begin
      case FscanStage of
        tssIdle:
        begin
        if assigned(FOnRunStatusChange) then FOnRunStatusChange(Self,False);
        ReportDebug('Set Running False',4);
        end

      end; // case

//      ReportDebug(StageString,4);
      FLastStage := FScanStage;
      end;
    Sleep(100);
  end;
end;

{function TScannerSequencer.GetScanFileData: TScanFeatureData;
begin
Result := Head.FScanFileData;
end;}

procedure TScannerSequencer.HeadInstalled(Sender: TObject);
begin
  ReportDebug('Scanner Thread Starting',4);
  Resume;
  if assigned(FOnHeadInstalledOK) then FOnHeadInstalledOK(Self);
end;


procedure TScannerSequencer.ReportDebug(DebugString : String;Level : Integer);
begin
if assigned(FOnDebug) Then FOnDebug(Self,Level,DebugString);
end;

procedure TScannerSequencer.ResetScanner;
begin
if Head.LoadedOK then
  begin
  ReportDebug('Resetting Scanner Sequencer',4);
  BufferScanFileData.Init;
  Head.SetParameters(BufferScanFileData);
  Head.PrepareSequence;
  FStartable := False;
  if assigned(FOnScannerReset) then FOnScannerReset(self);
  end
else
  begin
  ReportDebug('Head not Loaded',4);
  end;
//ScanInterruptRequest := False;
end;


procedure TScannerSequencer.SetName;
var
  ThreadNameInfo: TThreadNameInfo;
begin
  ThreadNameInfo.FType := $1000;
  ThreadNameInfo.FName := 'ScannerSequencer';
  ThreadNameInfo.FThreadID := $FFFFFFFF;
  ThreadNameInfo.FFlags := 0;
  try
    RaiseException( $406D1388, 0, sizeof(ThreadNameInfo) div sizeof(LongWord), @ThreadNameInfo );
  except
  end;
end;


procedure TScannerSequencer.StartScan;
var
Interrupted : Boolean;
Count : Integer;
begin
ResetScanner;
Count := 0;
FScanStage := tssStartRequest;
ReportDebug('WAITING STARTABLE',4);
StartCount := 0;
StartableTimer.Enabled := true;
end;

function TScannerSequencer.RunSlice(index : Integer;Isprofile : boolean): Boolean;
var
CurrentSlice : TScannerSlice;
CurrentLine : TScanLine;
LineList : TList;
LNum : Integer;
place : INteger;
OnCount,OffCount : INteger;
begin

Result := False;
PLace := 0;
try
if not assigned(Head) then
  begin
  ReportDebug('Head not assigned',4);
  exit;
  end
else if not assigned(Head.ScanFileData) then
  begin
  ReportDebug('ScanFileData not assigned',4);
  exit;
  end
else if not assigned(Head.ScanFileData.ScanFile) then
  begin
  ReportDebug('ScanFile not assigned',4);
  exit;
  end;

//ReportDebug('ScanFile Data OK');

if (Index < -0) or (index > Head.ScanFileData.ScanFile.SliceCount) then
  begin
  ReportDebug(Format('Slice Index %d out of range of %d',[index,Head.ScanFileData.ScanFile.SliceCount]),4);
  exit;
  end;

//ReportDebug(Format('Slice Index %d of %d',[index,Head.ScanFileData.ScanFile.SliceCount]),4);
try
CurrentSlice := Head.ScanFileData.SliceAtIndex(Index);
except
ReportDebug('Exception Getting CurrentSlice',4);
exit;
end;
if not assigned(CurrentSlice) then
  begin
  ReportDebug(Format('No Slice Data for Slice %d',[CurrentSlice]),4);
  exit;
  end
else
  begin
//ReportDebug(Format('Got Slice at Index %d',[index]));
  if Isprofile then
    begin
//  ReportDebug('Is profile');
    LineList := CurrentSlice.ProfileList;
    end
  else
    begin
//  ReportDebug('Is Hatch');
    LineList := CurrentSlice.HatchList;
    end;
  if not assigned(LineList) then
    begin
    ReportDebug('Could not get line list',4);
    exit;
    end ;
  Lnum := 0;
//ReportDebug(Format('Line List Count %d',[LineList.Count]));
  OnCount := 0;
  OffCount := 0;

  while (LNum < LineList.Count) and (Not FInterruptRequest) do
    begin
    { TODO : Interrupt code required here }
    try
    CurrentLine := TScanLine(LineList[LNum]);
    if CurrentLine.IsOn then
      begin
      Head.Mark(CurrentLine.X,CurrentLine.Y);
      inc(OnCount);
      end
    else
      begin
      Head.Jump(CurrentLine.X,CurrentLine.Y);
      inc(OffCount);
      end;
//    ReportDebug('Checking Blocking');
    CheckBlocking; // Handles swapping and sets startable if list is full
    finally
    inc(LNum);
    end;
    end;

//  ReportDebug(Format('SliceData on:%d  off%d for %d Lines',[OnCount,OffCount,LineList.Count]),4);

  if FInterruptRequest then
    begin
//    ReportDebug(Format('Interrupted at Line %d of %d',[LNum,LineList.Count]),4);
    raise Exception.Create('Interrupt in Blocking');
    end;


  end;
except
//  ReportDebug(Format('Exception in RunSlice %d',[Place]),4);
  if FInterruptRequest then
    begin
    raise Exception.Create('Interrupt in Run Slice');
    end;

end;
end;

function TScannerSequencer.CheckBlocking : Boolean;
begin
Result := False;
while (Head.IsBlocking) and not(FInterruptRequest) do
{ TODO : Interrupt code required here }
  begin
  Result := True;
  if LastBlockState = False then
    begin
    LastBlockState := True;
//    ReportDebug('Blocking',4)
    end;
  if not FStartable then
    begin
//    ReportDebug('Is Now  Startable',4);
    FStartable := True;
    if assigned(FOnStartable) then FOnStartable(Self);
    end;
  FStartable := True; // allow main loop to start if holding at start request
  Sleep(100);
  end;

if FInterruptRequest then
  begin
//  ReportDebug('Interrupted while checking Block State',4);
  Result := False;
  end;

if Result <> LastBlockState then
  begin
  LastBlockState := Result;
//  ReportDebug('Not Blocking',4)
  end
end;

procedure TScannerSequencer.RunScanSequence;
var
GroupCount : Integer;
SliceNum : INteger;
HatchNum : INteger;
ProfileNum : INteger;
startp : Integer;
endp   : Integer;
begin
try
LastBlockState := False;
with Head.ScanFileData.StockParams do
  begin
  ReportDebug(Format('Passes :%d  SliceStart:%d SliceEnd: %d ',[mGroupSlicePasses,mGroupSliceStart,mGroupSliceEnd]),4);
  ReportDebug(Format('ProfileStartCount %d ',[mProfileStartCount]),4);
  ReportDebug(Format('ProfileEndCount %d ',[mProfileEndCount]),4);
  if mGroupSlicePasses > 0 then
    begin
    GroupCount := 0;
    while GroupCount <  mGroupSlicePasses do
      begin
      ReportDebug(Format('Running Group = %d ',[GroupCount]),2);
      try
      SliceNum := mGroupSliceStart;
      while SliceNum <= mGroupSliceEnd do
        begin
        Startp := 0;
        // **********************
        // Perform Start Profiles
        // **********************
        while (Startp < mProfileStartCount)  do
          begin
          ReportDebug(Format('RunningStart Profile %d ',[mProfileStartCount]),4);
          RunSlice(SliceNum,True);
          inc(startp);
          end; //while Startp < mProfileStartCount


        // **********************
        // Run Hatches interspersed with profiles
        // **********************
        For HatchNum := 0 to mHatchCount-1 do
          begin
          ReportDebug(Format('HatchCount =  %d ',[mHatchCount]),4);
          if mProfileEvery > 0 then
            begin
            ReportDebug(Format('ProfilEvery = %d ',[mProfileEvery]),4);
            if ((HatchNum and mProfileEvery) = 0) then
              begin
              for ProfileNum := 0 to mProfilePasses-1 do
                begin
                RunSlice(SliceNum,True);
                end
              end;
            end; // if mprofileEvery > 0
          RunSlice(SliceNum , False);  // Run Hatch
          end; // Hatches
        // **********************
        // Run Post Slices
        // **********************
        if mProfileEndCount > 0 then
          begin
          For Endp := 0 to mProfileEndCount-1 do
            begin
            RunSlice(SliceNum,True);
            ReportDebug(Format('Running end Profile = %d ',[Endp]),4);
            end;
          end;
        // **********************
        // Do SliceDelay
        // **********************
        if mProfileDwell > 0 then
          begin
          Delay(mProfileDwell);
          end;
        inc(SliceNum);
        end; //while SliceNum < mSliceEnd
      finally
      // **********************
      // Do Group Delay
      // **********************
      if mGroupDwell > 0 then
        begin
        Delay(mGroupDwell);
        end;
      inc(GroupCount);
      end;
      end; //while GroupCount
      ReportDebug(Format('Done All Groups = %d ',[mGroupSlicePasses]),2);

    end; //if mSlicePasses > 0 then
  end; // with
  Head.EndExecution;
 except
 if FInterruptRequest then
  begin
  ReportDebug('Interrrupt in Run Scan Sequence',2);
  end
 else
  begin
  ReportDebug('Exception in Run Scan Sequence',2);
  end;
 Head.EndExecution;
 end;
end;

procedure TScannerSequencer.SetInterruptRequest(const Value: Boolean);
begin
  if (Value <> FInterruptRequest) then
    begin
    FInterruptRequest := Value;
    if Value then InterruptTimer.Enabled := True;
    end;
end;

procedure TScannerSequencer.StartTimerTimer(Sender: TObject);
begin
if FStartable then
  begin
  StartableTimer.Enabled := False;
  Head.Start;
  end
else
  begin
  inc (StartCount);
  if StartCount > 100 then
    begin
    StartableTimer.Enabled := False;
    end
  end
end;


function TScannerSequencer.GetScanStageText: String;
begin
case FScanStage of
  tssIdle:         Result := 'tssIdle';
  tssStartRequest: Result := 'tssStartRequest';
  tssSequenceSent: Result := 'tssSequenceSent';
  tssFinished:     Result := 'tssFinished';
  tssError:        Result := 'tssError';
  tssUnrecoverableError: Result := 'tssUnrecoverableError';
end;
end;

procedure TScannerSequencer.SliceListCleaned(Sender: TObject);
begin
ReportDebug('Slice List Cleaned',4);
end;

procedure TScannerSequencer.InterruptDone(Sender: TObject);
begin
InterruptTimer.Enabled := False;
FInterruptRequest := False;
//ReportDebug('Lowering Interrupt',4);
ResetScanner;
FScanStage := tssIdle;
end;

destructor TScannerSequencer.Destroy;
begin
  Terminate;
  StartableTimer.Enabled := False;
  StartableTimer.Free;
  InterruptTimer.Enabled := False;
  InterruptTimer.free;
  inherited;
end;

procedure TScannerSequencer.aHeadParamsChange(Sender: TObject);
begin
if assigned(FOnHeadParamsChange) then FOnHeadParamsChange(Self);
end;

procedure TScannerSequencer.Delay(DelMicroseconds: Integer);
begin
CheckBlocking;
Head.Delay(DelMicroseconds);
end;

end.

