//----------------------------------------------------------------------------
//  NOTE
//      THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
//      KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR
//      PURPOSE.
//
//  Abstract
//      Defines the RTC5Wrap class that imports RTC5functions from the
//      RTC5DLL.dll.
//      If you intend to compile for the 64-bit platform: x64, you need to
//      define the conditional compilation symbol:
//
//                                  SL_x64
//
//      to import the RTC5 functions from the RTC5DLLx64.dll, which is the
//      64-bit version of RTC5's dynamic-link library.
//
//----------------------------------------------------------------------------
using System;
using System.Runtime.InteropServices;

namespace RTC5Import
{
  public class RTC5Wrap
  {
      // If you intend to compile for the 64-bit platform: x64
      // you need to define the preprocessor symbol: SL_x64
#if (SL_x64)
      const string DLL_NAME = "RTC5DLLx64.dll";
#else
      const string DLL_NAME = "RTC5DLL.dll";
#endif

    const int TableSize = 1024;
    const int SampleArraySize = 1024*1024;
    const int SignalSize = 4;
    const int TransformSize = 132130;
    const int SignalSize2 = 8;

    [DllImport(DLL_NAME)]
    public static extern uint init_rtc5_dll();

    [DllImport(DLL_NAME)]
    public static extern void free_rtc5_dll();

    [DllImport(DLL_NAME)]
    public static extern void set_rtc4_mode();

    [DllImport(DLL_NAME)]
    public static extern void set_rtc5_mode();

    [DllImport(DLL_NAME)]
    public static extern uint get_rtc_mode();

    [DllImport(DLL_NAME)]
    public static extern uint n_get_error(uint CardNo);

    [DllImport(DLL_NAME)]
    public static extern uint n_get_last_error(uint CardNo);

    [DllImport(DLL_NAME)]
    public static extern void n_reset_error(uint CardNo, uint Code);

    [DllImport(DLL_NAME)]
    public static extern uint n_set_verify(uint CardNo, uint Verify);

    [DllImport(DLL_NAME)]
    public static extern uint get_error();

    [DllImport(DLL_NAME)]
    public static extern uint get_last_error();

    [DllImport(DLL_NAME)]
    public static extern void reset_error(uint Code);

    [DllImport(DLL_NAME)]
    public static extern uint set_verify(uint Verify);

    [DllImport(DLL_NAME)]
    public static extern uint verify_checksum(string Name);

    [DllImport(DLL_NAME)]
    public static extern uint rtc5_count_cards();

    [DllImport(DLL_NAME)]
    public static extern uint acquire_rtc(uint CardNo);

    [DllImport(DLL_NAME)]
    public static extern uint release_rtc(uint CardNo);

    [DllImport(DLL_NAME)]
    public static extern uint select_rtc(uint CardNo);

    [DllImport(DLL_NAME)]
    public static extern uint get_dll_version();

    [DllImport(DLL_NAME)]
    public static extern uint n_get_serial_number(uint CardNo);

    [DllImport(DLL_NAME)]
    public static extern uint n_get_hex_version(uint CardNo);

    [DllImport(DLL_NAME)]
    public static extern uint n_get_rtc_version(uint CardNo);

    [DllImport(DLL_NAME)]
    public static extern uint get_serial_number();

    [DllImport(DLL_NAME)]
    public static extern uint get_hex_version();

    [DllImport(DLL_NAME)]
    public static extern uint get_rtc_version();

    [DllImport(DLL_NAME)]
    public static extern uint n_load_program_file(uint CardNo, string Path);

    [DllImport(DLL_NAME)]
    public static extern void n_sync_slaves(uint CardNo);

    [DllImport(DLL_NAME)]
    public static extern uint n_get_sync_status(uint CardNo);

    [DllImport(DLL_NAME)]
    public static extern uint n_load_correction_file(uint CardNo, string Name, uint No, uint Dim);

    [DllImport(DLL_NAME)]
    public static extern uint n_load_z_table(uint CardNo, double A, double B, double C);

    [DllImport(DLL_NAME)]
    public static extern void n_select_cor_table(uint CardNo, uint HeadA, uint HeadB);

    [DllImport(DLL_NAME)]
    public static extern uint n_set_dsp_mode(uint CardNo, uint Mode);

    [DllImport(DLL_NAME)]
    public static extern double n_get_head_para(uint CardNo, uint HeadNo, uint ParaNo);

    [DllImport(DLL_NAME)]
    public static extern double n_get_table_para(uint CardNo, uint TableNo, uint ParaNo);

    [DllImport(DLL_NAME)]
    public static extern uint load_program_file(string Path);

    [DllImport(DLL_NAME)]
    public static extern void sync_slaves();

    [DllImport(DLL_NAME)]
    public static extern uint get_sync_status();

    [DllImport(DLL_NAME)]
    public static extern uint load_correction_file(string Name, uint No, uint Dim);

    [DllImport(DLL_NAME)]
    public static extern uint load_z_table(double A, double B, double C);

    [DllImport(DLL_NAME)]
    public static extern void select_cor_table(uint HeadA, uint HeadB);

    [DllImport(DLL_NAME)]
    public static extern uint set_dsp_mode(uint Mode);

    [DllImport(DLL_NAME)]
    public static extern double get_head_para(uint HeadNo, uint ParaNo);

    [DllImport(DLL_NAME)]
    public static extern double get_table_para(uint TableNo, uint ParaNo);

    [DllImport(DLL_NAME)]
    public static extern void n_config_list(uint CardNo, uint Mem1, uint Mem2);

    [DllImport(DLL_NAME)]
    public static extern void n_get_config_list(uint CardNo);

    [DllImport(DLL_NAME)]
    public static extern uint n_save_disk(uint CardNo, string Name, uint Mode);

    [DllImport(DLL_NAME)]
    public static extern uint n_load_disk(uint CardNo, string Name, uint Mode);

    [DllImport(DLL_NAME)]
    public static extern uint n_get_list_space(uint CardNo);

    [DllImport(DLL_NAME)]
    public static extern void config_list(uint Mem1, uint Mem2);

    [DllImport(DLL_NAME)]
    public static extern void get_config_list();

    [DllImport(DLL_NAME)]
    public static extern uint save_disk(string Name, uint Mode);

    [DllImport(DLL_NAME)]
    public static extern uint load_disk(string Name, uint Mode);

    [DllImport(DLL_NAME)]
    public static extern uint get_list_space();

    [DllImport(DLL_NAME)]
    public static extern void n_set_start_list_pos(uint CardNo, uint ListNo, uint Pos);

    [DllImport(DLL_NAME)]
    public static extern void n_set_start_list(uint CardNo, uint ListNo);

    [DllImport(DLL_NAME)]
    public static extern void n_set_start_list_1(uint CardNo);

    [DllImport(DLL_NAME)]
    public static extern void n_set_start_list_2(uint CardNo);

    [DllImport(DLL_NAME)]
    public static extern void n_set_input_pointer(uint CardNo, uint Pos);

    [DllImport(DLL_NAME)]
    public static extern uint n_load_list(uint CardNo, uint ListNo, uint Pos);

    [DllImport(DLL_NAME)]
    public static extern void n_load_sub(uint CardNo, uint Index);

    [DllImport(DLL_NAME)]
    public static extern void n_load_char(uint CardNo, uint Char);

    [DllImport(DLL_NAME)]
    public static extern void n_load_text_table(uint CardNo, uint Index);

    [DllImport(DLL_NAME)]
    public static extern void n_get_list_pointer(uint CardNo, out uint ListNo, out uint Pos);

    [DllImport(DLL_NAME)]
    public static extern uint n_get_input_pointer(uint CardNo);

    [DllImport(DLL_NAME)]
    public static extern void set_start_list_pos(uint ListNo, uint Pos);

    [DllImport(DLL_NAME)]
    public static extern void set_start_list(uint ListNo);

    [DllImport(DLL_NAME)]
    public static extern void set_start_list_1();

    [DllImport(DLL_NAME)]
    public static extern void set_start_list_2();

    [DllImport(DLL_NAME)]
    public static extern void set_input_pointer(uint Pos);

    [DllImport(DLL_NAME)]
    public static extern uint load_list(uint ListNo, uint Pos);

    [DllImport(DLL_NAME)]
    public static extern void load_sub(uint Index);

    [DllImport(DLL_NAME)]
    public static extern void load_char(uint Char);

    [DllImport(DLL_NAME)]
    public static extern void load_text_table(uint Index);

    [DllImport(DLL_NAME)]
    public static extern void get_list_pointer(out uint ListNo, out uint Pos);

    [DllImport(DLL_NAME)]
    public static extern uint get_input_pointer();

    [DllImport(DLL_NAME)]
    public static extern void n_execute_list_pos(uint CardNo, uint ListNo, uint Pos);

    [DllImport(DLL_NAME)]
    public static extern void n_execute_at_pointer(uint CardNo, uint Pos);

    [DllImport(DLL_NAME)]
    public static extern void n_execute_list(uint CardNo, uint ListNo);

    [DllImport(DLL_NAME)]
    public static extern void n_execute_list_1(uint CardNo);

    [DllImport(DLL_NAME)]
    public static extern void n_execute_list_2(uint CardNo);

    [DllImport(DLL_NAME)]
    public static extern void n_get_out_pointer(uint CardNo, out uint ListNo, out uint Pos);

    [DllImport(DLL_NAME)]
    public static extern void execute_list_pos(uint ListNo, uint Pos);

    [DllImport(DLL_NAME)]
    public static extern void execute_at_pointer(uint Pos);

    [DllImport(DLL_NAME)]
    public static extern void execute_list(uint ListNo);

    [DllImport(DLL_NAME)]
    public static extern void execute_list_1();

    [DllImport(DLL_NAME)]
    public static extern void execute_list_2();

    [DllImport(DLL_NAME)]
    public static extern void get_out_pointer(out uint ListNo, out uint Pos);

    [DllImport(DLL_NAME)]
    public static extern void n_auto_change_pos(uint CardNo, uint Pos);

    [DllImport(DLL_NAME)]
    public static extern void n_start_loop(uint CardNo);

    [DllImport(DLL_NAME)]
    public static extern void n_quit_loop(uint CardNo);

    [DllImport(DLL_NAME)]
    public static extern void n_pause_list(uint CardNo);

    [DllImport(DLL_NAME)]
    public static extern void n_restart_list(uint CardNo);

    [DllImport(DLL_NAME)]
    public static extern void n_release_wait(uint CardNo);

    [DllImport(DLL_NAME)]
    public static extern void n_stop_execution(uint CardNo);

    [DllImport(DLL_NAME)]
    public static extern void n_auto_change(uint CardNo);

    [DllImport(DLL_NAME)]
    public static extern void n_stop_list(uint CardNo);

    [DllImport(DLL_NAME)]
    public static extern uint n_get_wait_status(uint CardNo);

    [DllImport(DLL_NAME)]
    public static extern uint n_read_status(uint CardNo);

    [DllImport(DLL_NAME)]
    public static extern void n_get_status(uint CardNo, out uint Status, out uint Pos);

    [DllImport(DLL_NAME)]
    public static extern void auto_change_pos(uint Pos);

    [DllImport(DLL_NAME)]
    public static extern void start_loop();

    [DllImport(DLL_NAME)]
    public static extern void quit_loop();

    [DllImport(DLL_NAME)]
    public static extern void pause_list();

    [DllImport(DLL_NAME)]
    public static extern void restart_list();

    [DllImport(DLL_NAME)]
    public static extern void release_wait();

    [DllImport(DLL_NAME)]
    public static extern void stop_execution();

    [DllImport(DLL_NAME)]
    public static extern void auto_change();

    [DllImport(DLL_NAME)]
    public static extern void stop_list();

    [DllImport(DLL_NAME)]
    public static extern uint get_wait_status();

    [DllImport(DLL_NAME)]
    public static extern uint read_status();

    [DllImport(DLL_NAME)]
    public static extern void get_status(out uint Status, out uint Pos);

    [DllImport(DLL_NAME)]
    public static extern void n_set_extstartpos(uint CardNo, uint Pos);

    [DllImport(DLL_NAME)]
    public static extern void n_set_max_counts(uint CardNo, uint Counts);

    [DllImport(DLL_NAME)]
    public static extern void n_set_control_mode(uint CardNo, uint Mode);

    [DllImport(DLL_NAME)]
    public static extern void n_simulate_ext_stop(uint CardNo);

    [DllImport(DLL_NAME)]
    public static extern void n_simulate_ext_start_ctrl(uint CardNo);

    [DllImport(DLL_NAME)]
    public static extern uint n_get_counts(uint CardNo);

    [DllImport(DLL_NAME)]
    public static extern uint n_get_startstop_info(uint CardNo);

    [DllImport(DLL_NAME)]
    public static extern void set_extstartpos(uint Pos);

    [DllImport(DLL_NAME)]
    public static extern void set_max_counts(uint Counts);

    [DllImport(DLL_NAME)]
    public static extern void set_control_mode(uint Mode);

    [DllImport(DLL_NAME)]
    public static extern void simulate_ext_stop();

    [DllImport(DLL_NAME)]
    public static extern void simulate_ext_start_ctrl();

    [DllImport(DLL_NAME)]
    public static extern uint get_counts();

    [DllImport(DLL_NAME)]
    public static extern uint get_startstop_info();

    [DllImport(DLL_NAME)]
    public static extern void n_copy_dst_src(uint CardNo, uint Dst, uint Src, uint Mode);

    [DllImport(DLL_NAME)]
    public static extern void n_set_char_pointer(uint CardNo, uint Char, uint Pos);

    [DllImport(DLL_NAME)]
    public static extern void n_set_sub_pointer(uint CardNo, uint Index, uint Pos);

    [DllImport(DLL_NAME)]
    public static extern void n_set_text_table_pointer(uint CardNo, uint Index, uint Pos);

    [DllImport(DLL_NAME)]
    public static extern void n_set_char_table(uint CardNo, uint Index, uint Pos);

    [DllImport(DLL_NAME)]
    public static extern uint n_get_char_pointer(uint CardNo, uint Char);

    [DllImport(DLL_NAME)]
    public static extern uint n_get_sub_pointer(uint CardNo, uint Index);

    [DllImport(DLL_NAME)]
    public static extern uint n_get_text_table_pointer(uint CardNo, uint Index);

    [DllImport(DLL_NAME)]
    public static extern void copy_dst_src(uint Dst, uint Src, uint Mode);

    [DllImport(DLL_NAME)]
    public static extern void set_char_pointer(uint Char, uint Pos);

    [DllImport(DLL_NAME)]
    public static extern void set_sub_pointer(uint Index, uint Pos);

    [DllImport(DLL_NAME)]
    public static extern void set_text_table_pointer(uint Index, uint Pos);

    [DllImport(DLL_NAME)]
    public static extern void set_char_table(uint Index, uint Pos);

    [DllImport(DLL_NAME)]
    public static extern uint get_char_pointer(uint Char);

    [DllImport(DLL_NAME)]
    public static extern uint get_sub_pointer(uint Index);

    [DllImport(DLL_NAME)]
    public static extern uint get_text_table_pointer(uint Index);

    [DllImport(DLL_NAME)]
    public static extern void n_time_update(uint CardNo);

    [DllImport(DLL_NAME)]
    public static extern void n_set_serial_step(uint CardNo, uint No, uint Step);

    [DllImport(DLL_NAME)]
    public static extern void n_set_serial(uint CardNo, uint No);

    [DllImport(DLL_NAME)]
    public static extern double n_get_serial(uint CardNo);

    [DllImport(DLL_NAME)]
    public static extern void time_update();

    [DllImport(DLL_NAME)]
    public static extern void set_serial_step(uint No, uint Step);

    [DllImport(DLL_NAME)]
    public static extern void set_serial(uint No);

    [DllImport(DLL_NAME)]
    public static extern double get_serial();

    [DllImport(DLL_NAME)]
    public static extern void n_write_io_port_mask(uint CardNo, uint Value, uint Mask);

    [DllImport(DLL_NAME)]
    public static extern void n_write_8bit_port(uint CardNo, uint Value);

    [DllImport(DLL_NAME)]
    public static extern uint n_read_io_port(uint CardNo);

    [DllImport(DLL_NAME)]
    public static extern uint n_read_io_port_buffer(uint CardNo, uint Index, out uint Value, out int XPos, out int YPos, out uint Time);

    [DllImport(DLL_NAME)]
    public static extern uint n_get_io_status(uint CardNo);

    [DllImport(DLL_NAME)]
    public static extern uint n_read_analog_in(uint CardNo);

    [DllImport(DLL_NAME)]
    public static extern void n_write_da_x(uint CardNo, uint x, uint Value);

    [DllImport(DLL_NAME)]
    public static extern void n_set_laser_off_default(uint CardNo, uint AnalogOut1, uint AnalogOut2, uint DigitalOut);

    [DllImport(DLL_NAME)]
    public static extern void n_set_port_default(uint CardNo, uint Port, uint Value);

    [DllImport(DLL_NAME)]
    public static extern void n_write_io_port(uint CardNo, uint Value);

    [DllImport(DLL_NAME)]
    public static extern void n_write_da_1(uint CardNo, uint Value);

    [DllImport(DLL_NAME)]
    public static extern void n_write_da_2(uint CardNo, uint Value);

    [DllImport(DLL_NAME)]
    public static extern void write_io_port_mask(uint Value, uint Mask);

    [DllImport(DLL_NAME)]
    public static extern void write_8bit_port(uint Value);

    [DllImport(DLL_NAME)]
    public static extern uint read_io_port();

    [DllImport(DLL_NAME)]
    public static extern uint read_io_port_buffer(uint Index, out uint Value, out int XPos, out int YPos, out uint Time);

    [DllImport(DLL_NAME)]
    public static extern uint get_io_status();

    [DllImport(DLL_NAME)]
    public static extern uint read_analog_in();

    [DllImport(DLL_NAME)]
    public static extern void write_da_x(uint x, uint Value);

    [DllImport(DLL_NAME)]
    public static extern void set_laser_off_default(uint AnalogOut1, uint AnalogOut2, uint DigitalOut);

    [DllImport(DLL_NAME)]
    public static extern void set_port_default(uint Port, uint Value);

    [DllImport(DLL_NAME)]
    public static extern void write_io_port(uint Value);

    [DllImport(DLL_NAME)]
    public static extern void write_da_1(uint Value);

    [DllImport(DLL_NAME)]
    public static extern void write_da_2(uint Value);

    [DllImport(DLL_NAME)]
    public static extern void n_disable_laser(uint CardNo);

    [DllImport(DLL_NAME)]
    public static extern void n_enable_laser(uint CardNo);

    [DllImport(DLL_NAME)]
    public static extern void n_laser_signal_on(uint CardNo);

    [DllImport(DLL_NAME)]
    public static extern void n_laser_signal_off(uint CardNo);

    [DllImport(DLL_NAME)]
    public static extern void n_set_standby(uint CardNo, uint HalfPeriod, uint PulseLength);

    [DllImport(DLL_NAME)]
    public static extern void n_set_laser_pulses_ctrl(uint CardNo, uint HalfPeriod, uint PulseLength);

    [DllImport(DLL_NAME)]
    public static extern void n_set_firstpulse_killer(uint CardNo, uint Length);

    [DllImport(DLL_NAME)]
    public static extern void n_set_qswitch_delay(uint CardNo, uint Delay);

    [DllImport(DLL_NAME)]
    public static extern void n_set_laser_mode(uint CardNo, uint Mode);

    [DllImport(DLL_NAME)]
    public static extern void n_set_laser_control(uint CardNo, uint Ctrl);

    [DllImport(DLL_NAME)]
    public static extern void n_set_laser_pin_out(uint CardNo, uint Pins);

    [DllImport(DLL_NAME)]
    public static extern uint n_get_laser_pin_in(uint CardNo);

    [DllImport(DLL_NAME)]
    public static extern void n_set_softstart_level(uint CardNo, uint Index, uint Level);

    [DllImport(DLL_NAME)]
    public static extern void n_set_softstart_mode(uint CardNo, uint Mode, uint Number, uint Delay);

    [DllImport(DLL_NAME)]
    public static extern uint n_set_auto_laser_control(uint CardNo, uint Ctrl, uint Value, uint Mode, uint MinValue, uint MaxValue);

    [DllImport(DLL_NAME)]
    public static extern uint n_set_auto_laser_params(uint CardNo, uint Ctrl, uint Value, uint MinValue, uint MaxValue);

    [DllImport(DLL_NAME)]
    public static extern int n_load_auto_laser_control(uint CardNo, string Name, uint No);

    [DllImport(DLL_NAME)]
    public static extern int n_load_position_control(uint CardNo, string Name, uint No);

    [DllImport(DLL_NAME)]
    public static extern void n_set_default_pixel(uint CardNo, uint PulseLength);

    [DllImport(DLL_NAME)]
    public static extern void n_get_standby(uint CardNo, out uint HalfPeriod, out uint PulseLength);

    [DllImport(DLL_NAME)]
    public static extern void n_set_pulse_picking(uint CardNo, uint No);

    [DllImport(DLL_NAME)]
    public static extern void n_set_pulse_picking_length(uint CardNo, uint Length);

    [DllImport(DLL_NAME)]
    public static extern void n_config_laser_signals(uint CardNo, uint Config);

    [DllImport(DLL_NAME)]
    public static extern void disable_laser();

    [DllImport(DLL_NAME)]
    public static extern void enable_laser();

    [DllImport(DLL_NAME)]
    public static extern void laser_signal_on();

    [DllImport(DLL_NAME)]
    public static extern void laser_signal_off();

    [DllImport(DLL_NAME)]
    public static extern void set_standby(uint HalfPeriod, uint PulseLength);

    [DllImport(DLL_NAME)]
    public static extern void set_laser_pulses_ctrl(uint HalfPeriod, uint PulseLength);

    [DllImport(DLL_NAME)]
    public static extern void set_firstpulse_killer(uint Length);

    [DllImport(DLL_NAME)]
    public static extern void set_qswitch_delay(uint Delay);

    [DllImport(DLL_NAME)]
    public static extern void set_laser_mode(uint Mode);

    [DllImport(DLL_NAME)]
    public static extern void set_laser_control(uint Ctrl);

    [DllImport(DLL_NAME)]
    public static extern void set_laser_pin_out(uint Pins);

    [DllImport(DLL_NAME)]
    public static extern uint get_laser_pin_in();

    [DllImport(DLL_NAME)]
    public static extern void set_softstart_level(uint Index, uint Level);

    [DllImport(DLL_NAME)]
    public static extern void set_softstart_mode(uint Mode, uint Number, uint Delay);

    [DllImport(DLL_NAME)]
    public static extern uint set_auto_laser_control(uint Ctrl, uint Value, uint Mode, uint MinValue, uint MaxValue);

    [DllImport(DLL_NAME)]
    public static extern uint set_auto_laser_params(uint Ctrl, uint Value, uint MinValue, uint MaxValue);

    [DllImport(DLL_NAME)]
    public static extern int load_auto_laser_control(string Name, uint No);

    [DllImport(DLL_NAME)]
    public static extern int load_position_control(string Name, uint No);

    [DllImport(DLL_NAME)]
    public static extern void set_default_pixel(uint PulseLength);

    [DllImport(DLL_NAME)]
    public static extern void get_standby(out uint HalfPeriod, out uint PulseLength);

    [DllImport(DLL_NAME)]
    public static extern void set_pulse_picking(uint No);

    [DllImport(DLL_NAME)]
    public static extern void set_pulse_picking_length(uint Length);

    [DllImport(DLL_NAME)]
    public static extern void config_laser_signals(uint Config);

    [DllImport(DLL_NAME)]
    public static extern void n_set_ext_start_delay(uint CardNo, int Delay, uint EncoderNo);

    [DllImport(DLL_NAME)]
    public static extern void n_set_rot_center(uint CardNo, int X, int Y);

    [DllImport(DLL_NAME)]
    public static extern void n_simulate_encoder(uint CardNo, uint EncoderNo);

    [DllImport(DLL_NAME)]
    public static extern uint n_get_marking_info(uint CardNo);

    [DllImport(DLL_NAME)]
    public static extern void n_set_encoder_speed_ctrl(uint CardNo, uint EncoderNo, double Speed, double Smooth);

    [DllImport(DLL_NAME)]
    public static extern void n_set_mcbsp_x(uint CardNo, double ScaleX);

    [DllImport(DLL_NAME)]
    public static extern void n_set_mcbsp_y(uint CardNo, double ScaleY);

    [DllImport(DLL_NAME)]
    public static extern void n_set_mcbsp_rot(uint CardNo, double Resolution);

    [DllImport(DLL_NAME)]
    public static extern void n_set_mcbsp_in(uint CardNo, uint Mode, double Scale);

    [DllImport(DLL_NAME)]
    public static extern void n_get_encoder(uint CardNo, out int Encoder0, out int Encoder1);

    [DllImport(DLL_NAME)]
    public static extern void n_read_encoder(uint CardNo, out int Encoder0_1, out int Encoder1_1, out int Encoder0_2, out int Encoder1_2);

    [DllImport(DLL_NAME)]
    public static extern int n_get_mcbsp(uint CardNo);

    [DllImport(DLL_NAME)]
    public static extern int n_read_mcbsp(uint CardNo, uint No);

    [DllImport(DLL_NAME)]
    public static extern void set_ext_start_delay(int Delay, uint EncoderNo);

    [DllImport(DLL_NAME)]
    public static extern void set_rot_center(int X, int Y);

    [DllImport(DLL_NAME)]
    public static extern void simulate_encoder(uint EncoderNo);

    [DllImport(DLL_NAME)]
    public static extern uint get_marking_info();

    [DllImport(DLL_NAME)]
    public static extern void set_encoder_speed_ctrl(uint EncoderNo, double Speed, double Smooth);

    [DllImport(DLL_NAME)]
    public static extern void set_mcbsp_x(double ScaleX);

    [DllImport(DLL_NAME)]
    public static extern void set_mcbsp_y(double ScaleY);

    [DllImport(DLL_NAME)]
    public static extern void set_mcbsp_rot(double Resolution);

    [DllImport(DLL_NAME)]
    public static extern void set_mcbsp_in(uint Mode, double Scale);

    [DllImport(DLL_NAME)]
    public static extern void get_encoder(out int Encoder0, out int Encoder1);

    [DllImport(DLL_NAME)]
    public static extern void read_encoder(out int Encoder0_1, out int Encoder1_1, out int Encoder0_2, out int Encoder1_2);

    [DllImport(DLL_NAME)]
    public static extern int get_mcbsp();

    [DllImport(DLL_NAME)]
    public static extern int read_mcbsp(uint No);

    [DllImport(DLL_NAME)]
    public static extern double n_get_time(uint CardNo);

    [DllImport(DLL_NAME)]
    public static extern void n_measurement_status(uint CardNo, out uint Busy, out uint Pos);

    [DllImport(DLL_NAME)]
    public static extern void n_get_waveform(uint CardNo, uint Channel, uint Number, [MarshalAs(UnmanagedType.LPArray, SizeConst=SampleArraySize)]int[] Ptr);

    [DllImport(DLL_NAME)]
    public static extern void n_bounce_supp(uint CardNo, uint Length);

    [DllImport(DLL_NAME)]
    public static extern void n_home_position_xyz(uint CardNo, int XHome, int YHome, int ZHome);

    [DllImport(DLL_NAME)]
    public static extern void n_home_position(uint CardNo, int XHome, int YHome);

    [DllImport(DLL_NAME)]
    public static extern void n_rs232_config(uint CardNo, uint BaudRate);

    [DllImport(DLL_NAME)]
    public static extern void n_rs232_write_data(uint CardNo, uint Data);

    [DllImport(DLL_NAME)]
    public static extern void n_rs232_write_text(uint CardNo, string pData);

    [DllImport(DLL_NAME)]
    public static extern uint n_rs232_read_data(uint CardNo);

    [DllImport(DLL_NAME)]
    public static extern uint n_set_mcbsp_freq(uint CardNo, uint Freq);

    [DllImport(DLL_NAME)]
    public static extern void n_mcbsp_init(uint CardNo, uint XDelay, uint RDelay);

    [DllImport(DLL_NAME)]
    public static extern uint n_get_overrun(uint CardNo);

    [DllImport(DLL_NAME)]
    public static extern uint n_get_master_slave(uint CardNo);

    [DllImport(DLL_NAME)]
    public static extern void n_get_transform(uint CardNo, uint Number, [MarshalAs(UnmanagedType.LPArray, SizeConst=SampleArraySize)]int[] Ptr1, [MarshalAs(UnmanagedType.LPArray, SizeConst=SampleArraySize)]int[] Ptr2, [MarshalAs(UnmanagedType.LPArray, SizeConst=TransformSize)]uint[] Ptr, uint Code);

    [DllImport(DLL_NAME)]
    public static extern void n_stop_trigger(uint CardNo);

    [DllImport(DLL_NAME)]
    public static extern void n_move_to(uint CardNo, uint Pos);

    [DllImport(DLL_NAME)]
    public static extern void n_set_enduring_wobbel(uint CardNo, uint CenterX, uint CenterY, uint CenterZ, uint LimitHi, uint LimitLo, double ScaleX, double ScaleY, double ScaleZ);

    [DllImport(DLL_NAME)]
    public static extern void n_set_free_variable(uint CardNo, uint VarNo, uint Value);

    [DllImport(DLL_NAME)]
    public static extern uint n_get_free_variable(uint CardNo, uint VarNo);

    [DllImport(DLL_NAME)]
    public static extern void n_set_mcbsp_out_ptr(uint CardNo, uint Number, [MarshalAs(UnmanagedType.LPArray, SizeConst=SignalSize2)]int[] SignalPtr);

    [DllImport(DLL_NAME)]
    public static extern double get_time();

    [DllImport(DLL_NAME)]
    public static extern void measurement_status(out uint Busy, out uint Pos);

    [DllImport(DLL_NAME)]
    public static extern void get_waveform(uint Channel, uint Number, [MarshalAs(UnmanagedType.LPArray, SizeConst=SampleArraySize)]int[] Ptr);

    [DllImport(DLL_NAME)]
    public static extern void bounce_supp(uint Length);

    [DllImport(DLL_NAME)]
    public static extern void home_position_xyz(int XHome, int YHome, int ZHome);

    [DllImport(DLL_NAME)]
    public static extern void home_position(int XHome, int YHome);

    [DllImport(DLL_NAME)]
    public static extern void rs232_config(uint BaudRate);

    [DllImport(DLL_NAME)]
    public static extern void rs232_write_data(uint Data);

    [DllImport(DLL_NAME)]
    public static extern void rs232_write_text(string pData);

    [DllImport(DLL_NAME)]
    public static extern uint rs232_read_data();

    [DllImport(DLL_NAME)]
    public static extern uint set_mcbsp_freq(uint Freq);

    [DllImport(DLL_NAME)]
    public static extern void mcbsp_init(uint XDelay, uint RDelay);

    [DllImport(DLL_NAME)]
    public static extern uint get_overrun();

    [DllImport(DLL_NAME)]
    public static extern uint get_master_slave();

    [DllImport(DLL_NAME)]
    public static extern void get_transform(uint Number, [MarshalAs(UnmanagedType.LPArray, SizeConst=SampleArraySize)]int[] Ptr1, [MarshalAs(UnmanagedType.LPArray, SizeConst=SampleArraySize)]int[] Ptr2, [MarshalAs(UnmanagedType.LPArray, SizeConst=TransformSize)]uint[] Ptr, uint Code);

    [DllImport(DLL_NAME)]
    public static extern void stop_trigger();

    [DllImport(DLL_NAME)]
    public static extern void move_to(uint Pos);

    [DllImport(DLL_NAME)]
    public static extern void set_enduring_wobbel(uint CenterX, uint CenterY, uint CenterZ, uint LimitHi, uint LimitLo, double ScaleX, double ScaleY, double ScaleZ);

    [DllImport(DLL_NAME)]
    public static extern void set_free_variable(uint VarNo, uint Value);

    [DllImport(DLL_NAME)]
    public static extern uint get_free_variable(uint VarNo);

    [DllImport(DLL_NAME)]
    public static extern void set_mcbsp_out_ptr(uint Number, [MarshalAs(UnmanagedType.LPArray, SizeConst=SignalSize2)]int[] SignalPtr);

    [DllImport(DLL_NAME)]
    public static extern void n_set_defocus(uint CardNo, int Shift);

    [DllImport(DLL_NAME)]
    public static extern void n_goto_xyz(uint CardNo, int X, int Y, int Z);

    [DllImport(DLL_NAME)]
    public static extern void n_goto_xy(uint CardNo, int X, int Y);

    [DllImport(DLL_NAME)]
    public static extern int n_get_z_distance(uint CardNo, int X, int Y, int Z);

    [DllImport(DLL_NAME)]
    public static extern void set_defocus(int Shift);

    [DllImport(DLL_NAME)]
    public static extern void goto_xyz(int X, int Y, int Z);

    [DllImport(DLL_NAME)]
    public static extern void goto_xy(int X, int Y);

    [DllImport(DLL_NAME)]
    public static extern int get_z_distance(int X, int Y, int Z);

    [DllImport(DLL_NAME)]
    public static extern void n_set_offset_xyz(uint CardNo, uint HeadNo, int XOffset, int YOffset, int ZOffset, uint at_once);

    [DllImport(DLL_NAME)]
    public static extern void n_set_offset(uint CardNo, uint HeadNo, int XOffset, int YOffset, uint at_once);

    [DllImport(DLL_NAME)]
    public static extern void n_set_matrix(uint CardNo, uint HeadNo, double M11, double M12, double M21, double M22, uint at_once);

    [DllImport(DLL_NAME)]
    public static extern void n_set_angle(uint CardNo, uint HeadNo, double Angle, uint at_once);

    [DllImport(DLL_NAME)]
    public static extern void n_set_scale(uint CardNo, uint HeadNo, double Scale, uint at_once);

    [DllImport(DLL_NAME)]
    public static extern void n_apply_mcbsp(uint CardNo, uint HeadNo, uint at_once);

    [DllImport(DLL_NAME)]
    public static extern uint n_upload_transform(uint CardNo, uint HeadNo, [MarshalAs(UnmanagedType.LPArray, SizeConst=TransformSize)]uint[] Ptr);

    [DllImport(DLL_NAME)]
    public static extern void set_offset_xyz(uint HeadNo, int XOffset, int YOffset, int ZOffset, uint at_once);

    [DllImport(DLL_NAME)]
    public static extern void set_offset(uint HeadNo, int XOffset, int YOffset, uint at_once);

    [DllImport(DLL_NAME)]
    public static extern void set_matrix(uint HeadNo, double M11, double M12, double M21, double M22, uint at_once);

    [DllImport(DLL_NAME)]
    public static extern void set_angle(uint HeadNo, double Angle, uint at_once);

    [DllImport(DLL_NAME)]
    public static extern void set_scale(uint HeadNo, double Scale, uint at_once);

    [DllImport(DLL_NAME)]
    public static extern void apply_mcbsp(uint HeadNo, uint at_once);

    [DllImport(DLL_NAME)]
    public static extern uint upload_transform(uint HeadNo, [MarshalAs(UnmanagedType.LPArray, SizeConst=TransformSize)]uint[] Ptr);

    [DllImport(DLL_NAME)]
    public static extern uint transform(out int Sig1, out int Sig2, [MarshalAs(UnmanagedType.LPArray, SizeConst=TransformSize)]uint[] Ptr, uint Code);

    [DllImport(DLL_NAME)]
    public static extern void n_set_delay_mode(uint CardNo, uint VarPoly, uint DirectMove3D, uint EdgeLevel, uint MinJumpDelay, uint JumpLengthLimit);

    [DllImport(DLL_NAME)]
    public static extern void n_set_jump_speed_ctrl(uint CardNo, double Speed);

    [DllImport(DLL_NAME)]
    public static extern void n_set_mark_speed_ctrl(uint CardNo, double Speed);

    [DllImport(DLL_NAME)]
    public static extern void n_set_sky_writing_para(uint CardNo, double Timelag, int LaserOnShift, uint Nprev, uint Npost);

    [DllImport(DLL_NAME)]
    public static extern void n_set_sky_writing_limit(uint CardNo, double CosAngle);

    [DllImport(DLL_NAME)]
    public static extern void n_set_sky_writing_mode(uint CardNo, uint Mode);

    [DllImport(DLL_NAME)]
    public static extern int n_load_varpolydelay(uint CardNo, string Name, uint No);

    [DllImport(DLL_NAME)]
    public static extern void n_set_hi(uint CardNo, uint HeadNo, double GalvoGainX, double GalvoGainY, int GalvoOffsetX, int GalvoOffsetY);

    [DllImport(DLL_NAME)]
    public static extern void n_get_hi_pos(uint CardNo, uint HeadNo, out int X1, out int X2, out int Y1, out int Y2);

    [DllImport(DLL_NAME)]
    public static extern uint n_auto_cal(uint CardNo, uint HeadNo, uint Command);

    [DllImport(DLL_NAME)]
    public static extern uint n_get_auto_cal(uint CardNo, uint HeadNo);

    [DllImport(DLL_NAME)]
    public static extern void n_set_sky_writing(uint CardNo, double Timelag, int LaserOnShift);

    [DllImport(DLL_NAME)]
    public static extern void n_get_hi_data(uint CardNo, out int X1, out int X2, out int Y1, out int Y2);

    [DllImport(DLL_NAME)]
    public static extern void set_delay_mode(uint VarPoly, uint DirectMove3D, uint EdgeLevel, uint MinJumpDelay, uint JumpLengthLimit);

    [DllImport(DLL_NAME)]
    public static extern void set_jump_speed_ctrl(double Speed);

    [DllImport(DLL_NAME)]
    public static extern void set_mark_speed_ctrl(double Speed);

    [DllImport(DLL_NAME)]
    public static extern void set_sky_writing_para(double Timelag, int LaserOnShift, uint Nprev, uint Npost);

    [DllImport(DLL_NAME)]
    public static extern void set_sky_writing_limit(double CosAngle);

    [DllImport(DLL_NAME)]
    public static extern void set_sky_writing_mode(uint Mode);

    [DllImport(DLL_NAME)]
    public static extern int load_varpolydelay(string Name, uint No);

    [DllImport(DLL_NAME)]
    public static extern void set_hi(uint HeadNo, double GalvoGainX, double GalvoGainY, int GalvoOffsetX, int GalvoOffsetY);

    [DllImport(DLL_NAME)]
    public static extern void get_hi_pos(uint HeadNo, out int X1, out int X2, out int Y1, out int Y2);

    [DllImport(DLL_NAME)]
    public static extern uint auto_cal(uint HeadNo, uint Command);

    [DllImport(DLL_NAME)]
    public static extern uint get_auto_cal(uint HeadNo);

    [DllImport(DLL_NAME)]
    public static extern void set_sky_writing(double Timelag, int LaserOnShift);

    [DllImport(DLL_NAME)]
    public static extern void get_hi_data(out int X1, out int X2, out int Y1, out int Y2);

    [DllImport(DLL_NAME)]
    public static extern void n_send_user_data(uint CardNo, uint Head, uint Axis, int Data0, int Data1, int Data2, int Data3, int Data4);

    [DllImport(DLL_NAME)]
    public static extern int n_read_user_data(uint CardNo, uint Head, uint Axis, out int Data0, out int Data1, out int Data2, out int Data3, out int Data4);

    [DllImport(DLL_NAME)]
    public static extern void n_control_command(uint CardNo, uint Head, uint Axis, uint Data);

    [DllImport(DLL_NAME)]
    public static extern int n_get_value(uint CardNo, uint Signal);

    [DllImport(DLL_NAME)]
    public static extern void n_get_values(uint CardNo, [MarshalAs(UnmanagedType.LPArray, SizeConst=SignalSize)]uint[] SignalPtr, [MarshalAs(UnmanagedType.LPArray, SizeConst=SignalSize)]int[] ResultPtr);

    [DllImport(DLL_NAME)]
    public static extern uint n_get_head_status(uint CardNo, uint Head);

    [DllImport(DLL_NAME)]
    public static extern int n_set_jump_mode(uint CardNo, int Flag, uint Length, int VA1, int VA2, int VB1, int VB2, int JA1, int JA2, int JB1, int JB2);

    [DllImport(DLL_NAME)]
    public static extern int n_load_jump_table_offset(uint CardNo, string Name, uint No, uint PosAck, int Offset, uint MinDelay, uint MaxDelay, uint ListPos);

    [DllImport(DLL_NAME)]
    public static extern uint n_get_jump_table(uint CardNo, [MarshalAs(UnmanagedType.LPArray, SizeConst=TableSize)]ushort[] Ptr);

    [DllImport(DLL_NAME)]
    public static extern uint n_set_jump_table(uint CardNo, [MarshalAs(UnmanagedType.LPArray, SizeConst=TableSize)]ushort[] Ptr);

    [DllImport(DLL_NAME)]
    public static extern int n_load_jump_table(uint CardNo, string Name, uint No, uint PosAck, uint MinDelay, uint MaxDelay, uint ListPos);

    [DllImport(DLL_NAME)]
    public static extern void send_user_data(uint Head, uint Axis, int Data0, int Data1, int Data2, int Data3, int Data4);

    [DllImport(DLL_NAME)]
    public static extern int read_user_data(uint Head, uint Axis, out int Data0, out int Data1, out int Data2, out int Data3, out int Data4);

    [DllImport(DLL_NAME)]
    public static extern void control_command(uint Head, uint Axis, uint Data);

    [DllImport(DLL_NAME)]
    public static extern int get_value(uint Signal);

    [DllImport(DLL_NAME)]
    public static extern void get_values([MarshalAs(UnmanagedType.LPArray, SizeConst=SignalSize)]uint[] SignalPtr, [MarshalAs(UnmanagedType.LPArray, SizeConst=SignalSize)]int[] ResultPtr);

    [DllImport(DLL_NAME)]
    public static extern uint get_head_status(uint Head);

    [DllImport(DLL_NAME)]
    public static extern int set_jump_mode(int Flag, uint Length, int VA1, int VA2, int VB1, int VB2, int JA1, int JA2, int JB1, int JB2);

    [DllImport(DLL_NAME)]
    public static extern int load_jump_table_offset(string Name, uint No, uint PosAck, int Offset, uint MinDelay, uint MaxDelay, uint ListPos);

    [DllImport(DLL_NAME)]
    public static extern uint get_jump_table([MarshalAs(UnmanagedType.LPArray, SizeConst=TableSize)]ushort[] Ptr);

    [DllImport(DLL_NAME)]
    public static extern uint set_jump_table([MarshalAs(UnmanagedType.LPArray, SizeConst=TableSize)]ushort[] Ptr);

    [DllImport(DLL_NAME)]
    public static extern int load_jump_table(string Name, uint No, uint PosAck, uint MinDelay, uint MaxDelay, uint ListPos);

    [DllImport(DLL_NAME)]
    public static extern void n_stepper_init(uint CardNo, uint No, uint Period, int Dir, int Pos, uint Tol, uint Enable, uint WaitTime);

    [DllImport(DLL_NAME)]
    public static extern void n_stepper_enable(uint CardNo, int Enable1, int Enable2);

    [DllImport(DLL_NAME)]
    public static extern void n_stepper_control(uint CardNo, int Period1, int Period2);

    [DllImport(DLL_NAME)]
    public static extern void n_stepper_abs_no(uint CardNo, uint No, int Pos, uint WaitTime);

    [DllImport(DLL_NAME)]
    public static extern void n_stepper_rel_no(uint CardNo, uint No, int dPos, uint WaitTime);

    [DllImport(DLL_NAME)]
    public static extern void n_stepper_abs(uint CardNo, int Pos1, int Pos2, uint WaitTime);

    [DllImport(DLL_NAME)]
    public static extern void n_stepper_rel(uint CardNo, int dPos1, int dPos2, uint WaitTime);

    [DllImport(DLL_NAME)]
    public static extern void n_get_stepper_status(uint CardNo, out uint Status1, out int Pos1, out uint Status2, out int Pos2);

    [DllImport(DLL_NAME)]
    public static extern void stepper_init(uint No, uint Period, int Dir, int Pos, uint Tol, uint Enable, uint WaitTime);

    [DllImport(DLL_NAME)]
    public static extern void stepper_enable(int Enable1, int Enable2);

    [DllImport(DLL_NAME)]
    public static extern void stepper_control(int Period1, int Period2);

    [DllImport(DLL_NAME)]
    public static extern void stepper_abs_no(uint No, int Pos, uint WaitTime);

    [DllImport(DLL_NAME)]
    public static extern void stepper_rel_no(uint No, int dPos, uint WaitTime);

    [DllImport(DLL_NAME)]
    public static extern void stepper_abs(int Pos1, int Pos2, uint WaitTime);

    [DllImport(DLL_NAME)]
    public static extern void stepper_rel(int dPos1, int dPos2, uint WaitTime);

    [DllImport(DLL_NAME)]
    public static extern void get_stepper_status(out uint Status1, out int Pos1, out uint Status2, out int Pos2);

    [DllImport(DLL_NAME)]
    public static extern void n_select_cor_table_list(uint CardNo, uint HeadA, uint HeadB);

    [DllImport(DLL_NAME)]
    public static extern void select_cor_table_list(uint HeadA, uint HeadB);

    [DllImport(DLL_NAME)]
    public static extern void n_list_nop(uint CardNo);

    [DllImport(DLL_NAME)]
    public static extern void n_list_continue(uint CardNo);

    [DllImport(DLL_NAME)]
    public static extern void n_long_delay(uint CardNo, uint Delay);

    [DllImport(DLL_NAME)]
    public static extern void n_set_end_of_list(uint CardNo);

    [DllImport(DLL_NAME)]
    public static extern void n_set_wait(uint CardNo, uint WaitWord);

    [DllImport(DLL_NAME)]
    public static extern void n_list_jump_pos(uint CardNo, uint Pos);

    [DllImport(DLL_NAME)]
    public static extern void n_list_jump_rel(uint CardNo, int Pos);

    [DllImport(DLL_NAME)]
    public static extern void n_set_list_jump(uint CardNo, uint Pos);

    [DllImport(DLL_NAME)]
    public static extern void list_nop();

    [DllImport(DLL_NAME)]
    public static extern void list_continue();

    [DllImport(DLL_NAME)]
    public static extern void long_delay(uint Delay);

    [DllImport(DLL_NAME)]
    public static extern void set_end_of_list();

    [DllImport(DLL_NAME)]
    public static extern void set_wait(uint WaitWord);

    [DllImport(DLL_NAME)]
    public static extern void list_jump_pos(uint Pos);

    [DllImport(DLL_NAME)]
    public static extern void list_jump_rel(int Pos);

    [DllImport(DLL_NAME)]
    public static extern void set_list_jump(uint Pos);

    [DllImport(DLL_NAME)]
    public static extern void n_set_extstartpos_list(uint CardNo, uint Pos);

    [DllImport(DLL_NAME)]
    public static extern void n_set_control_mode_list(uint CardNo, uint Mode);

    [DllImport(DLL_NAME)]
    public static extern void n_simulate_ext_start(uint CardNo, int Delay, uint EncoderNo);

    [DllImport(DLL_NAME)]
    public static extern void set_extstartpos_list(uint Pos);

    [DllImport(DLL_NAME)]
    public static extern void set_control_mode_list(uint Mode);

    [DllImport(DLL_NAME)]
    public static extern void simulate_ext_start(int Delay, uint EncoderNo);

    [DllImport(DLL_NAME)]
    public static extern void n_list_return(uint CardNo);

    [DllImport(DLL_NAME)]
    public static extern void n_list_call(uint CardNo, uint Pos);

    [DllImport(DLL_NAME)]
    public static extern void n_list_call_abs(uint CardNo, uint Pos);

    [DllImport(DLL_NAME)]
    public static extern void n_sub_call(uint CardNo, uint Index);

    [DllImport(DLL_NAME)]
    public static extern void n_sub_call_abs(uint CardNo, uint Index);

    [DllImport(DLL_NAME)]
    public static extern void list_return();

    [DllImport(DLL_NAME)]
    public static extern void list_call(uint Pos);

    [DllImport(DLL_NAME)]
    public static extern void list_call_abs(uint Pos);

    [DllImport(DLL_NAME)]
    public static extern void sub_call(uint Index);

    [DllImport(DLL_NAME)]
    public static extern void sub_call_abs(uint Index);

    [DllImport(DLL_NAME)]
    public static extern void n_list_call_cond(uint CardNo, uint Mask1, uint Mask0, uint Pos);

    [DllImport(DLL_NAME)]
    public static extern void n_list_call_abs_cond(uint CardNo, uint Mask1, uint Mask0, uint Pos);

    [DllImport(DLL_NAME)]
    public static extern void n_sub_call_cond(uint CardNo, uint Mask1, uint Mask0, uint Pos);

    [DllImport(DLL_NAME)]
    public static extern void n_sub_call_abs_cond(uint CardNo, uint Mask1, uint Mask0, uint Pos);

    [DllImport(DLL_NAME)]
    public static extern void n_list_jump_pos_cond(uint CardNo, uint Mask1, uint Mask0, uint Index);

    [DllImport(DLL_NAME)]
    public static extern void n_list_jump_rel_cond(uint CardNo, uint Mask1, uint Mask0, int Index);

    [DllImport(DLL_NAME)]
    public static extern void n_if_cond(uint CardNo, uint Mask1, uint Mask0);

    [DllImport(DLL_NAME)]
    public static extern void n_if_not_cond(uint CardNo, uint Mask1, uint Mask0);

    [DllImport(DLL_NAME)]
    public static extern void n_if_pin_cond(uint CardNo, uint Mask1, uint Mask0);

    [DllImport(DLL_NAME)]
    public static extern void n_if_not_pin_cond(uint CardNo, uint Mask1, uint Mask0);

    [DllImport(DLL_NAME)]
    public static extern void n_switch_ioport(uint CardNo, uint MaskBits, uint ShiftBits);

    [DllImport(DLL_NAME)]
    public static extern void n_list_jump_cond(uint CardNo, uint Mask1, uint Mask0, uint Pos);

    [DllImport(DLL_NAME)]
    public static extern void list_call_cond(uint Mask1, uint Mask0, uint Pos);

    [DllImport(DLL_NAME)]
    public static extern void list_call_abs_cond(uint Mask1, uint Mask0, uint Pos);

    [DllImport(DLL_NAME)]
    public static extern void sub_call_cond(uint Mask1, uint Mask0, uint Index);

    [DllImport(DLL_NAME)]
    public static extern void sub_call_abs_cond(uint Mask1, uint Mask0, uint Index);

    [DllImport(DLL_NAME)]
    public static extern void list_jump_pos_cond(uint Mask1, uint Mask0, uint Pos);

    [DllImport(DLL_NAME)]
    public static extern void list_jump_rel_cond(uint Mask1, uint Mask0, int Pos);

    [DllImport(DLL_NAME)]
    public static extern void if_cond(uint Mask1, uint Mask0);

    [DllImport(DLL_NAME)]
    public static extern void if_not_cond(uint Mask1, uint Mask0);

    [DllImport(DLL_NAME)]
    public static extern void if_pin_cond(uint Mask1, uint Mask0);

    [DllImport(DLL_NAME)]
    public static extern void if_not_pin_cond(uint Mask1, uint Mask0);

    [DllImport(DLL_NAME)]
    public static extern void switch_ioport(uint MaskBits, uint ShiftBits);

    [DllImport(DLL_NAME)]
    public static extern void list_jump_cond(uint Mask1, uint Mask0, uint Pos);

    [DllImport(DLL_NAME)]
    public static extern void n_select_char_set(uint CardNo, uint No);

    [DllImport(DLL_NAME)]
    public static extern void n_mark_text(uint CardNo, string Text);

    [DllImport(DLL_NAME)]
    public static extern void n_mark_text_abs(uint CardNo, string Text);

    [DllImport(DLL_NAME)]
    public static extern void n_mark_char(uint CardNo, uint Char);

    [DllImport(DLL_NAME)]
    public static extern void n_mark_char_abs(uint CardNo, uint Char);

    [DllImport(DLL_NAME)]
    public static extern void select_char_set(uint No);

    [DllImport(DLL_NAME)]
    public static extern void mark_text(string Text);

    [DllImport(DLL_NAME)]
    public static extern void mark_text_abs(string Text);

    [DllImport(DLL_NAME)]
    public static extern void mark_char(uint Char);

    [DllImport(DLL_NAME)]
    public static extern void mark_char_abs(uint Char);

    [DllImport(DLL_NAME)]
    public static extern void n_mark_serial(uint CardNo, uint Mode, uint Digits);

    [DllImport(DLL_NAME)]
    public static extern void n_mark_serial_abs(uint CardNo, uint Mode, uint Digits);

    [DllImport(DLL_NAME)]
    public static extern void n_mark_date(uint CardNo, uint Part, uint Mode);

    [DllImport(DLL_NAME)]
    public static extern void n_mark_date_abs(uint CardNo, uint Part, uint Mode);

    [DllImport(DLL_NAME)]
    public static extern void n_mark_time(uint CardNo, uint Part, uint Mode);

    [DllImport(DLL_NAME)]
    public static extern void n_mark_time_abs(uint CardNo, uint Part, uint Mode);

    [DllImport(DLL_NAME)]
    public static extern void n_time_fix_f_off(uint CardNo, uint FirstDay, uint Offset);

    [DllImport(DLL_NAME)]
    public static extern void n_time_fix_f(uint CardNo, uint FirstDay);

    [DllImport(DLL_NAME)]
    public static extern void n_time_fix(uint CardNo);

    [DllImport(DLL_NAME)]
    public static extern void mark_serial(uint Mode, uint Digits);

    [DllImport(DLL_NAME)]
    public static extern void mark_serial_abs(uint Mode, uint Digits);

    [DllImport(DLL_NAME)]
    public static extern void mark_date(uint Part, uint Mode);

    [DllImport(DLL_NAME)]
    public static extern void mark_date_abs(uint Part, uint Mode);

    [DllImport(DLL_NAME)]
    public static extern void mark_time(uint Part, uint Mode);

    [DllImport(DLL_NAME)]
    public static extern void mark_time_abs(uint Part, uint Mode);

    [DllImport(DLL_NAME)]
    public static extern void time_fix_f_off(uint FirstDay, uint Offset);

    [DllImport(DLL_NAME)]
    public static extern void time_fix_f(uint FirstDay);

    [DllImport(DLL_NAME)]
    public static extern void time_fix();

    [DllImport(DLL_NAME)]
    public static extern void n_clear_io_cond_list(uint CardNo, uint Mask1, uint Mask0, uint Mask);

    [DllImport(DLL_NAME)]
    public static extern void n_set_io_cond_list(uint CardNo, uint Mask1, uint Mask0, uint Mask);

    [DllImport(DLL_NAME)]
    public static extern void n_write_io_port_mask_list(uint CardNo, uint Value, uint Mask);

    [DllImport(DLL_NAME)]
    public static extern void n_write_8bit_port_list(uint CardNo, uint Value);

    [DllImport(DLL_NAME)]
    public static extern void n_read_io_port_list(uint CardNo);

    [DllImport(DLL_NAME)]
    public static extern void n_write_da_x_list(uint CardNo, uint x, uint Value);

    [DllImport(DLL_NAME)]
    public static extern void n_write_io_port_list(uint CardNo, uint Value);

    [DllImport(DLL_NAME)]
    public static extern void n_write_da_1_list(uint CardNo, uint Value);

    [DllImport(DLL_NAME)]
    public static extern void n_write_da_2_list(uint CardNo, uint Value);

    [DllImport(DLL_NAME)]
    public static extern void clear_io_cond_list(uint Mask1, uint Mask0, uint MaskClear);

    [DllImport(DLL_NAME)]
    public static extern void set_io_cond_list(uint Mask1, uint Mask0, uint MaskSet);

    [DllImport(DLL_NAME)]
    public static extern void write_io_port_mask_list(uint Value, uint Mask);

    [DllImport(DLL_NAME)]
    public static extern void write_8bit_port_list(uint Value);

    [DllImport(DLL_NAME)]
    public static extern void read_io_port_list();

    [DllImport(DLL_NAME)]
    public static extern void write_da_x_list(uint x, uint Value);

    [DllImport(DLL_NAME)]
    public static extern void write_io_port_list(uint Value);

    [DllImport(DLL_NAME)]
    public static extern void write_da_1_list(uint Value);

    [DllImport(DLL_NAME)]
    public static extern void write_da_2_list(uint Value);

    [DllImport(DLL_NAME)]
    public static extern void n_laser_signal_on_list(uint CardNo);

    [DllImport(DLL_NAME)]
    public static extern void n_laser_signal_off_list(uint CardNo);

    [DllImport(DLL_NAME)]
    public static extern void n_para_laser_on_pulses_list(uint CardNo, uint Period, uint Pulses, uint P);

    [DllImport(DLL_NAME)]
    public static extern void n_laser_on_pulses_list(uint CardNo, uint Period, uint Pulses);

    [DllImport(DLL_NAME)]
    public static extern void n_laser_on_list(uint CardNo, uint Period);

    [DllImport(DLL_NAME)]
    public static extern void n_set_laser_delays(uint CardNo, int LaserOnDelay, uint LaserOffDelay);

    [DllImport(DLL_NAME)]
    public static extern void n_set_standby_list(uint CardNo, uint HalfPeriod, uint PulseLength);

    [DllImport(DLL_NAME)]
    public static extern void n_set_laser_pulses(uint CardNo, uint HalfPeriod, uint PulseLength);

    [DllImport(DLL_NAME)]
    public static extern void n_set_firstpulse_killer_list(uint CardNo, uint Length);

    [DllImport(DLL_NAME)]
    public static extern void n_set_qswitch_delay_list(uint CardNo, uint Delay);

    [DllImport(DLL_NAME)]
    public static extern void n_set_laser_pin_out_list(uint CardNo, uint Pins);

    [DllImport(DLL_NAME)]
    public static extern void n_set_vector_control(uint CardNo, uint Ctrl, uint Value);

    [DllImport(DLL_NAME)]
    public static extern void n_set_default_pixel_list(uint CardNo, uint PulseLength);

    [DllImport(DLL_NAME)]
    public static extern void n_set_auto_laser_params_list(uint CardNo, uint Ctrl, uint Value, uint MinValue, uint MaxValue);

    [DllImport(DLL_NAME)]
    public static extern void n_set_pulse_picking_list(uint CardNo, uint No);

    [DllImport(DLL_NAME)]
    public static extern void n_set_laser_timing(uint CardNo, uint HalfPeriod, uint PulseLength1, uint PulseLength2, uint TimeBase);

    [DllImport(DLL_NAME)]
    public static extern void laser_signal_on_list();

    [DllImport(DLL_NAME)]
    public static extern void laser_signal_off_list();

    [DllImport(DLL_NAME)]
    public static extern void para_laser_on_pulses_list(uint Period, uint Pulses, uint P);

    [DllImport(DLL_NAME)]
    public static extern void laser_on_pulses_list(uint Period, uint Pulses);

    [DllImport(DLL_NAME)]
    public static extern void laser_on_list(uint Period);

    [DllImport(DLL_NAME)]
    public static extern void set_laser_delays(int LaserOnDelay, uint LaserOffDelay);

    [DllImport(DLL_NAME)]
    public static extern void set_standby_list(uint HalfPeriod, uint PulseLength);

    [DllImport(DLL_NAME)]
    public static extern void set_laser_pulses(uint HalfPeriod, uint PulseLength);

    [DllImport(DLL_NAME)]
    public static extern void set_firstpulse_killer_list(uint Length);

    [DllImport(DLL_NAME)]
    public static extern void set_qswitch_delay_list(uint Delay);

    [DllImport(DLL_NAME)]
    public static extern void set_laser_pin_out_list(uint Pins);

    [DllImport(DLL_NAME)]
    public static extern void set_vector_control(uint Ctrl, uint Value);

    [DllImport(DLL_NAME)]
    public static extern void set_default_pixel_list(uint PulseLength);

    [DllImport(DLL_NAME)]
    public static extern void set_auto_laser_params_list(uint Ctrl, uint Value, uint MinValue, uint MaxValue);

    [DllImport(DLL_NAME)]
    public static extern void set_pulse_picking_list(uint No);

    [DllImport(DLL_NAME)]
    public static extern void set_laser_timing(uint HalfPeriod, uint PulseLength1, uint PulseLength2, uint TimeBase);

    [DllImport(DLL_NAME)]
    public static extern void n_fly_return_z(uint CardNo, int X, int Y, int Z);

    [DllImport(DLL_NAME)]
    public static extern void n_fly_return(uint CardNo, int X, int Y);

    [DllImport(DLL_NAME)]
    public static extern void n_set_rot_center_list(uint CardNo, int X, int Y);

    [DllImport(DLL_NAME)]
    public static extern void n_set_ext_start_delay_list(uint CardNo, int Delay, uint EncoderNo);

    [DllImport(DLL_NAME)]
    public static extern void n_set_fly_x(uint CardNo, double ScaleX);

    [DllImport(DLL_NAME)]
    public static extern void n_set_fly_y(uint CardNo, double ScaleY);

    [DllImport(DLL_NAME)]
    public static extern void n_set_fly_z(uint CardNo, double ScaleZ, uint EndoderNo);

    [DllImport(DLL_NAME)]
    public static extern void n_set_fly_rot(uint CardNo, double Resolution);

    [DllImport(DLL_NAME)]
    public static extern void n_set_fly_x_pos(uint CardNo, double ScaleX);

    [DllImport(DLL_NAME)]
    public static extern void n_set_fly_y_pos(uint CardNo, double ScaleY);

    [DllImport(DLL_NAME)]
    public static extern void n_set_fly_rot_pos(uint CardNo, double Resolution);

    [DllImport(DLL_NAME)]
    public static extern void n_set_fly_limits(uint CardNo, int Xmin, int Xmax, int Ymin, int Ymax);

    [DllImport(DLL_NAME)]
    public static extern void n_set_fly_limits_z(uint CardNo, int Zmin, int Zmax);

    [DllImport(DLL_NAME)]
    public static extern void n_if_fly_x_overflow(uint CardNo, int Mode);

    [DllImport(DLL_NAME)]
    public static extern void n_if_fly_y_overflow(uint CardNo, int Mode);

    [DllImport(DLL_NAME)]
    public static extern void n_if_fly_z_overflow(uint CardNo, int Mode);

    [DllImport(DLL_NAME)]
    public static extern void n_if_not_fly_x_overflow(uint CardNo, int Mode);

    [DllImport(DLL_NAME)]
    public static extern void n_if_not_fly_y_overflow(uint CardNo, int Mode);

    [DllImport(DLL_NAME)]
    public static extern void n_if_not_fly_z_overflow(uint CardNo, int Mode);

    [DllImport(DLL_NAME)]
    public static extern void n_clear_fly_overflow(uint CardNo, uint Mode);

    [DllImport(DLL_NAME)]
    public static extern void n_set_mcbsp_x_list(uint CardNo, double ScaleX);

    [DllImport(DLL_NAME)]
    public static extern void n_set_mcbsp_y_list(uint CardNo, double ScaleY);

    [DllImport(DLL_NAME)]
    public static extern void n_set_mcbsp_rot_list(uint CardNo, double Resolution);

    [DllImport(DLL_NAME)]
    public static extern void n_set_mcbsp_in_list(uint CardNo, uint Mode, double Scale);

    [DllImport(DLL_NAME)]
    public static extern void n_wait_for_encoder_mode(uint CardNo, int Value, uint EncoderNo, int Mode);

    [DllImport(DLL_NAME)]
    public static extern void n_wait_for_mcbsp(uint CardNo, uint Axis, int Value, int Mode);

    [DllImport(DLL_NAME)]
    public static extern void n_set_encoder_speed(uint CardNo, uint EncoderNo, double Speed, double Smooth);

    [DllImport(DLL_NAME)]
    public static extern void n_get_mcbsp_list(uint CardNo);

    [DllImport(DLL_NAME)]
    public static extern void n_store_encoder(uint CardNo, uint Pos);

    [DllImport(DLL_NAME)]
    public static extern void n_wait_for_encoder(uint CardNo, int Value, uint EncoderNo);

    [DllImport(DLL_NAME)]
    public static extern void fly_return_z(int X, int Y, int Z);

    [DllImport(DLL_NAME)]
    public static extern void fly_return(int X, int Y);

    [DllImport(DLL_NAME)]
    public static extern void set_rot_center_list(int X, int Y);

    [DllImport(DLL_NAME)]
    public static extern void set_ext_start_delay_list(int Delay, uint EncoderNo);

    [DllImport(DLL_NAME)]
    public static extern void set_fly_x(double ScaleX);

    [DllImport(DLL_NAME)]
    public static extern void set_fly_y(double ScaleY);

    [DllImport(DLL_NAME)]
    public static extern void set_fly_z(double ScaleZ, uint EncoderNo);

    [DllImport(DLL_NAME)]
    public static extern void set_fly_rot(double Resolution);

    [DllImport(DLL_NAME)]
    public static extern void set_fly_x_pos(double ScaleX);

    [DllImport(DLL_NAME)]
    public static extern void set_fly_y_pos(double ScaleY);

    [DllImport(DLL_NAME)]
    public static extern void set_fly_rot_pos(double Resolution);

    [DllImport(DLL_NAME)]
    public static extern void set_fly_limits(int Xmin, int Xmax, int Ymin, int Ymax);

    [DllImport(DLL_NAME)]
    public static extern void set_fly_limits_z(int Zmin, int Zmax);

    [DllImport(DLL_NAME)]
    public static extern void if_fly_x_overflow(int Mode);

    [DllImport(DLL_NAME)]
    public static extern void if_fly_y_overflow(int Mode);

    [DllImport(DLL_NAME)]
    public static extern void if_fly_z_overflow(int Mode);

    [DllImport(DLL_NAME)]
    public static extern void if_not_fly_x_overflow(int Mode);

    [DllImport(DLL_NAME)]
    public static extern void if_not_fly_y_overflow(int Mode);

    [DllImport(DLL_NAME)]
    public static extern void if_not_fly_z_overflow(int Mode);

    [DllImport(DLL_NAME)]
    public static extern void clear_fly_overflow(uint Mode);

    [DllImport(DLL_NAME)]
    public static extern void set_mcbsp_x_list(double ScaleX);

    [DllImport(DLL_NAME)]
    public static extern void set_mcbsp_y_list(double ScaleY);

    [DllImport(DLL_NAME)]
    public static extern void set_mcbsp_rot_list(double Resolution);

    [DllImport(DLL_NAME)]
    public static extern void set_mcbsp_in_list(uint Mode, double Scale);

    [DllImport(DLL_NAME)]
    public static extern void wait_for_encoder_mode(int Value, uint EncoderNo, int Mode);

    [DllImport(DLL_NAME)]
    public static extern void wait_for_mcbsp(uint Axis, int Value, int Mode);

    [DllImport(DLL_NAME)]
    public static extern void set_encoder_speed(uint EncoderNo, double Speed, double Smooth);

    [DllImport(DLL_NAME)]
    public static extern void get_mcbsp_list();

    [DllImport(DLL_NAME)]
    public static extern void store_encoder(uint Pos);

    [DllImport(DLL_NAME)]
    public static extern void wait_for_encoder(int Value, uint EncoderNo);

    [DllImport(DLL_NAME)]
    public static extern void n_save_and_restart_timer(uint CardNo);

    [DllImport(DLL_NAME)]
    public static extern void n_set_wobbel(uint CardNo, uint Transversal, uint Longitudinal, double Freq);

    [DllImport(DLL_NAME)]
    public static extern void n_set_wobbel_mode(uint CardNo, uint Transversal, uint Longitudinal, double Freq, int Mode);

    [DllImport(DLL_NAME)]
    public static extern void n_set_trigger(uint CardNo, uint Period, uint Signal1, uint Signal2);

    [DllImport(DLL_NAME)]
    public static extern void n_set_pixel_line(uint CardNo, uint Channel, uint HalfPeriod, double dX, double dY);

    [DllImport(DLL_NAME)]
    public static extern void n_set_n_pixel(uint CardNo, uint PulseLength, uint AnalogOut, uint Number);

    [DllImport(DLL_NAME)]
    public static extern void n_set_pixel(uint CardNo, uint PulseLength, uint AnalogOut);

    [DllImport(DLL_NAME)]
    public static extern void n_rs232_write_text_list(uint CardNo, string pData);

    [DllImport(DLL_NAME)]
    public static extern void n_set_mcbsp_out(uint CardNo, uint Signal1, uint Signal2);

    [DllImport(DLL_NAME)]
    public static extern void n_camming(uint CardNo, uint FirstPos, uint NPos, uint No, uint Ctrl, double Scale, uint Code);

    [DllImport(DLL_NAME)]
    public static extern void n_micro_vector_abs(uint CardNo, int X, int Y, int LasOn, int LasOf);

    [DllImport(DLL_NAME)]
    public static extern void n_micro_vector_rel(uint CardNo, int dX, int dY, int LasOn, int LasOf);

    [DllImport(DLL_NAME)]
    public static extern void n_set_free_variable_list(uint CardNo, uint VarNo, uint Value);

    [DllImport(DLL_NAME)]
    public static extern void save_and_restart_timer();

    [DllImport(DLL_NAME)]
    public static extern void set_wobbel(uint Transversal, uint Longitudinal, double Freq);

    [DllImport(DLL_NAME)]
    public static extern void set_wobbel_mode(uint Transversal, uint Longitudinal, double Freq, int Mode);

    [DllImport(DLL_NAME)]
    public static extern void set_trigger(uint Period, uint Signal1, uint Signal2);

    [DllImport(DLL_NAME)]
    public static extern void set_pixel_line(uint Channel, uint HalfPeriod, double dX, double dY);

    [DllImport(DLL_NAME)]
    public static extern void set_n_pixel(uint PulseLength, uint AnalogOut, uint Number);

    [DllImport(DLL_NAME)]
    public static extern void set_pixel(uint PulseLength, uint AnalogOut);

    [DllImport(DLL_NAME)]
    public static extern void rs232_write_text_list(string pData);

    [DllImport(DLL_NAME)]
    public static extern void set_mcbsp_out(uint Signal1, uint Signal2);

    [DllImport(DLL_NAME)]
    public static extern void camming(uint FirstPos, uint NPos, uint No, uint Ctrl, double Scale, uint Code);

    [DllImport(DLL_NAME)]
    public static extern void micro_vector_abs(int X, int Y, int LasOn, int LasOf);

    [DllImport(DLL_NAME)]
    public static extern void micro_vector_rel(int dX, int dY, int LasOn, int LasOf);

    [DllImport(DLL_NAME)]
    public static extern void set_free_variable_list(uint VarNo, uint Value);

    [DllImport(DLL_NAME)]
    public static extern void n_timed_mark_abs_3d(uint CardNo, int X, int Y, int Z, double T);

    [DllImport(DLL_NAME)]
    public static extern void n_timed_mark_rel_3d(uint CardNo, int dX, int dY, int dZ, double T);

    [DllImport(DLL_NAME)]
    public static extern void n_timed_mark_abs(uint CardNo, int X, int Y, double T);

    [DllImport(DLL_NAME)]
    public static extern void n_timed_mark_rel(uint CardNo, int dX, int dY, double T);

    [DllImport(DLL_NAME)]
    public static extern void timed_mark_abs_3d(int X, int Y, int Z, double T);

    [DllImport(DLL_NAME)]
    public static extern void timed_mark_rel_3d(int dX, int dY, int dZ, double T);

    [DllImport(DLL_NAME)]
    public static extern void timed_mark_abs(int X, int Y, double T);

    [DllImport(DLL_NAME)]
    public static extern void timed_mark_rel(int dX, int dY, double T);

    [DllImport(DLL_NAME)]
    public static extern void n_mark_abs_3d(uint CardNo, int X, int Y, int Z);

    [DllImport(DLL_NAME)]
    public static extern void n_mark_rel_3d(uint CardNo, int dX, int dY, int dZ);

    [DllImport(DLL_NAME)]
    public static extern void n_mark_abs(uint CardNo, int X, int Y);

    [DllImport(DLL_NAME)]
    public static extern void n_mark_rel(uint CardNo, int dX, int dY);

    [DllImport(DLL_NAME)]
    public static extern void mark_abs_3d(int X, int Y, int Z);

    [DllImport(DLL_NAME)]
    public static extern void mark_rel_3d(int dX, int dY, int dZ);

    [DllImport(DLL_NAME)]
    public static extern void mark_abs(int X, int Y);

    [DllImport(DLL_NAME)]
    public static extern void mark_rel(int dX, int dY);

    [DllImport(DLL_NAME)]
    public static extern void n_timed_jump_abs_3d(uint CardNo, int X, int Y, int Z, double T);

    [DllImport(DLL_NAME)]
    public static extern void n_timed_jump_rel_3d(uint CardNo, int dX, int dY, int dZ, double T);

    [DllImport(DLL_NAME)]
    public static extern void n_timed_jump_abs(uint CardNo, int X, int Y, double T);

    [DllImport(DLL_NAME)]
    public static extern void n_timed_jump_rel(uint CardNo, int dX, int dY, double T);

    [DllImport(DLL_NAME)]
    public static extern void timed_jump_abs_3d(int X, int Y, int Z, double T);

    [DllImport(DLL_NAME)]
    public static extern void timed_jump_rel_3d(int dX, int dY, int dZ, double T);

    [DllImport(DLL_NAME)]
    public static extern void timed_jump_abs(int X, int Y, double T);

    [DllImport(DLL_NAME)]
    public static extern void timed_jump_rel(int dX, int dY, double T);

    [DllImport(DLL_NAME)]
    public static extern void n_jump_abs_3d(uint CardNo, int X, int Y, int Z);

    [DllImport(DLL_NAME)]
    public static extern void n_jump_rel_3d(uint CardNo, int dX, int dY, int dZ);

    [DllImport(DLL_NAME)]
    public static extern void n_jump_abs(uint CardNo, int X, int Y);

    [DllImport(DLL_NAME)]
    public static extern void n_jump_rel(uint CardNo, int dX, int dY);

    [DllImport(DLL_NAME)]
    public static extern void jump_abs_3d(int X, int Y, int Z);

    [DllImport(DLL_NAME)]
    public static extern void jump_rel_3d(int dX, int dY, int dZ);

    [DllImport(DLL_NAME)]
    public static extern void jump_abs(int X, int Y);

    [DllImport(DLL_NAME)]
    public static extern void jump_rel(int dX, int dY);

    [DllImport(DLL_NAME)]
    public static extern void n_para_mark_abs_3d(uint CardNo, int X, int Y, int Z, uint P);

    [DllImport(DLL_NAME)]
    public static extern void n_para_mark_rel_3d(uint CardNo, int dX, int dY, int dZ, uint P);

    [DllImport(DLL_NAME)]
    public static extern void n_para_mark_abs(uint CardNo, int X, int Y, uint P);

    [DllImport(DLL_NAME)]
    public static extern void n_para_mark_rel(uint CardNo, int dX, int dY, uint P);

    [DllImport(DLL_NAME)]
    public static extern void para_mark_abs_3d(int X, int Y, int Z, uint P);

    [DllImport(DLL_NAME)]
    public static extern void para_mark_rel_3d(int dX, int dY, int dZ, uint P);

    [DllImport(DLL_NAME)]
    public static extern void para_mark_abs(int X, int Y, uint P);

    [DllImport(DLL_NAME)]
    public static extern void para_mark_rel(int dX, int dY, uint P);

    [DllImport(DLL_NAME)]
    public static extern void n_para_jump_abs_3d(uint CardNo, int X, int Y, int Z, uint P);

    [DllImport(DLL_NAME)]
    public static extern void n_para_jump_rel_3d(uint CardNo, int dX, int dY, int dZ, uint P);

    [DllImport(DLL_NAME)]
    public static extern void n_para_jump_abs(uint CardNo, int X, int Y, uint P);

    [DllImport(DLL_NAME)]
    public static extern void n_para_jump_rel(uint CardNo, int dX, int dY, uint P);

    [DllImport(DLL_NAME)]
    public static extern void para_jump_abs_3d(int X, int Y, int Z, uint P);

    [DllImport(DLL_NAME)]
    public static extern void para_jump_rel_3d(int dX, int dY, int dZ, uint P);

    [DllImport(DLL_NAME)]
    public static extern void para_jump_abs(int X, int Y, uint P);

    [DllImport(DLL_NAME)]
    public static extern void para_jump_rel(int dX, int dY, uint P);

    [DllImport(DLL_NAME)]
    public static extern void n_timed_para_mark_abs_3d(uint CardNo, int X, int Y, int Z, uint P, double T);

    [DllImport(DLL_NAME)]
    public static extern void n_timed_para_mark_rel_3d(uint CardNo, int dX, int dY, int dZ, uint P, double T);

    [DllImport(DLL_NAME)]
    public static extern void n_timed_para_jump_abs_3d(uint CardNo, int X, int Y, int Z, uint P, double T);

    [DllImport(DLL_NAME)]
    public static extern void n_timed_para_jump_rel_3d(uint CardNo, int dX, int dY, int dZ, uint P, double T);

    [DllImport(DLL_NAME)]
    public static extern void n_timed_para_mark_abs(uint CardNo, int X, int Y, uint P, double T);

    [DllImport(DLL_NAME)]
    public static extern void n_timed_para_mark_rel(uint CardNo, int dX, int dY, uint P, double T);

    [DllImport(DLL_NAME)]
    public static extern void n_timed_para_jump_abs(uint CardNo, int X, int Y, uint P, double T);

    [DllImport(DLL_NAME)]
    public static extern void n_timed_para_jump_rel(uint CardNo, int dX, int dY, uint P, double T);

    [DllImport(DLL_NAME)]
    public static extern void timed_para_mark_abs_3d(int X, int Y, int Z, uint P, double T);

    [DllImport(DLL_NAME)]
    public static extern void timed_para_mark_rel_3d(int dX, int dY, int dZ, uint P, double T);

    [DllImport(DLL_NAME)]
    public static extern void timed_para_jump_abs_3d(int X, int Y, int Z, uint P, double T);

    [DllImport(DLL_NAME)]
    public static extern void timed_para_jump_rel_3d(int dX, int dY, int dZ, uint P, double T);

    [DllImport(DLL_NAME)]
    public static extern void timed_para_mark_abs(int X, int Y, uint P, double T);

    [DllImport(DLL_NAME)]
    public static extern void timed_para_mark_rel(int dX, int dY, uint P, double T);

    [DllImport(DLL_NAME)]
    public static extern void timed_para_jump_abs(int X, int Y, uint P, double T);

    [DllImport(DLL_NAME)]
    public static extern void timed_para_jump_rel(int dX, int dY, uint P, double T);

    [DllImport(DLL_NAME)]
    public static extern void n_set_defocus_list(uint CardNo, int Shift);

    [DllImport(DLL_NAME)]
    public static extern void set_defocus_list(int Shift);

    [DllImport(DLL_NAME)]
    public static extern void n_timed_arc_abs(uint CardNo, int X, int Y, double Angle, double T);

    [DllImport(DLL_NAME)]
    public static extern void n_timed_arc_rel(uint CardNo, int dX, int dY, double Angle, double T);

    [DllImport(DLL_NAME)]
    public static extern void timed_arc_abs(int X, int Y, double Angle, double T);

    [DllImport(DLL_NAME)]
    public static extern void timed_arc_rel(int dX, int dY, double Angle, double T);

    [DllImport(DLL_NAME)]
    public static extern void n_arc_abs_3d(uint CardNo, int X, int Y, int Z, double Angle);

    [DllImport(DLL_NAME)]
    public static extern void n_arc_rel_3d(uint CardNo, int dX, int dY, int dZ, double Angle);

    [DllImport(DLL_NAME)]
    public static extern void n_arc_abs(uint CardNo, int X, int Y, double Angle);

    [DllImport(DLL_NAME)]
    public static extern void n_arc_rel(uint CardNo, int dX, int dY, double Angle);

    [DllImport(DLL_NAME)]
    public static extern void n_set_ellipse(uint CardNo, uint A, uint B, double Phi0, double Phi);

    [DllImport(DLL_NAME)]
    public static extern void n_mark_ellipse_abs(uint CardNo, int X, int Y, double Alpha);

    [DllImport(DLL_NAME)]
    public static extern void n_mark_ellipse_rel(uint CardNo, int dX, int dY, double Alpha);

    [DllImport(DLL_NAME)]
    public static extern void arc_abs_3d(int X, int Y, int Z, double Angle);

    [DllImport(DLL_NAME)]
    public static extern void arc_rel_3d(int dX, int dY, int dZ, double Angle);

    [DllImport(DLL_NAME)]
    public static extern void arc_abs(int X, int Y, double Angle);

    [DllImport(DLL_NAME)]
    public static extern void arc_rel(int dX, int dY, double Angle);

    [DllImport(DLL_NAME)]
    public static extern void set_ellipse(uint A, uint B, double Phi0, double Phi);

    [DllImport(DLL_NAME)]
    public static extern void mark_ellipse_abs(int X, int Y, double Alpha);

    [DllImport(DLL_NAME)]
    public static extern void mark_ellipse_rel(int dX, int dY, double Alpha);

    [DllImport(DLL_NAME)]
    public static extern void n_set_offset_xyz_list(uint CardNo, uint HeadNo, int XOffset, int YOffset, int ZOffset, uint at_once);

    [DllImport(DLL_NAME)]
    public static extern void n_set_offset_list(uint CardNo, uint HeadNo, int XOffset, int YOffset, uint at_once);

    [DllImport(DLL_NAME)]
    public static extern void n_set_matrix_list(uint CardNo, uint HeadNo, uint Ind1, uint Ind2, double Mij, uint at_once);

    [DllImport(DLL_NAME)]
    public static extern void n_set_angle_list(uint CardNo, uint HeadNo, double Angle, uint at_once);

    [DllImport(DLL_NAME)]
    public static extern void n_set_scale_list(uint CardNo, uint HeadNo, double Scale, uint at_once);

    [DllImport(DLL_NAME)]
    public static extern void n_apply_mcbsp_list(uint CardNo, uint HeadNo, uint at_once);

    [DllImport(DLL_NAME)]
    public static extern void set_offset_xyz_list(uint HeadNo, int XOffset, int YOffset, int ZOffset, uint at_once);

    [DllImport(DLL_NAME)]
    public static extern void set_offset_list(uint HeadNo, int XOffset, int YOffset, uint at_once);

    [DllImport(DLL_NAME)]
    public static extern void set_matrix_list(uint HeadNo, uint Ind1, uint Ind2, double Mij, uint at_once);

    [DllImport(DLL_NAME)]
    public static extern void set_angle_list(uint HeadNo, double Angle, uint at_once);

    [DllImport(DLL_NAME)]
    public static extern void set_scale_list(uint HeadNo, double Scale, uint at_once);

    [DllImport(DLL_NAME)]
    public static extern void apply_mcbsp_list(uint HeadNo, uint at_once);

    [DllImport(DLL_NAME)]
    public static extern void n_set_mark_speed(uint CardNo, double Speed);

    [DllImport(DLL_NAME)]
    public static extern void n_set_jump_speed(uint CardNo, double Speed);

    [DllImport(DLL_NAME)]
    public static extern void n_set_sky_writing_para_list(uint CardNo, double Timelag, int LaserOnShift, uint Nprev, uint Npost);

    [DllImport(DLL_NAME)]
    public static extern void n_set_sky_writing_list(uint CardNo, double Timelag, int LaserOnShift);

    [DllImport(DLL_NAME)]
    public static extern void n_set_sky_writing_limit_list(uint CardNo, double CosAngle);

    [DllImport(DLL_NAME)]
    public static extern void n_set_sky_writing_mode_list(uint CardNo, uint Mode);

    [DllImport(DLL_NAME)]
    public static extern void n_set_scanner_delays(uint CardNo, uint Jump, uint Mark, uint Polygon);

    [DllImport(DLL_NAME)]
    public static extern void n_set_jump_mode_list(uint CardNo, int Flag);

    [DllImport(DLL_NAME)]
    public static extern void n_enduring_wobbel(uint CardNo);

    [DllImport(DLL_NAME)]
    public static extern void set_mark_speed(double Speed);

    [DllImport(DLL_NAME)]
    public static extern void set_jump_speed(double Speed);

    [DllImport(DLL_NAME)]
    public static extern void set_sky_writing_para_list(double Timelag, int LaserOnShift, uint Nprev, uint Npost);

    [DllImport(DLL_NAME)]
    public static extern void set_sky_writing_list(double Timelag, int LaserOnShift);

    [DllImport(DLL_NAME)]
    public static extern void set_sky_writing_limit_list(double CosAngle);

    [DllImport(DLL_NAME)]
    public static extern void set_sky_writing_mode_list(uint Mode);

    [DllImport(DLL_NAME)]
    public static extern void set_scanner_delays(uint Jump, uint Mark, uint Polygon);

    [DllImport(DLL_NAME)]
    public static extern void set_jump_mode_list(int Flag);

    [DllImport(DLL_NAME)]
    public static extern void enduring_wobbel();

    [DllImport(DLL_NAME)]
    public static extern void n_stepper_enable_list(uint CardNo, int Enable1, int Enable2);

    [DllImport(DLL_NAME)]
    public static extern void n_stepper_control_list(uint CardNo, int Period1, int Period2);

    [DllImport(DLL_NAME)]
    public static extern void n_stepper_abs_no_list(uint CardNo, uint No, int Pos);

    [DllImport(DLL_NAME)]
    public static extern void n_stepper_rel_no_list(uint CardNo, uint No, int dPos);

    [DllImport(DLL_NAME)]
    public static extern void n_stepper_abs_list(uint CardNo, int Pos1, int Pos2);

    [DllImport(DLL_NAME)]
    public static extern void n_stepper_rel_list(uint CardNo, int dPos1, int dPos2);

    [DllImport(DLL_NAME)]
    public static extern void n_stepper_wait(uint CardNo, uint No);

    [DllImport(DLL_NAME)]
    public static extern void stepper_enable_list(int Enable1, int Enable2);

    [DllImport(DLL_NAME)]
    public static extern void stepper_control_list(int Period1, int Period2);

    [DllImport(DLL_NAME)]
    public static extern void stepper_abs_no_list(uint No, int Pos);

    [DllImport(DLL_NAME)]
    public static extern void stepper_rel_no_list(uint No, int dPos);

    [DllImport(DLL_NAME)]
    public static extern void stepper_abs_list(int Pos1, int Pos2);

    [DllImport(DLL_NAME)]
    public static extern void stepper_rel_list(int dPos1, int dPos2);

    [DllImport(DLL_NAME)]
    public static extern void stepper_wait(uint No);

  }
}
