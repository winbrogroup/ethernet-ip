unit RTC5Form;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs,IFormInterface,INamedInterface,AbstractFormPlugin,RTC5Methods,AbstractScript,
  ScanData, RTC5ScanManager, StdCtrls, NamedPlugin,ExtCtrls,INIFiles, ComCtrls, Grids,
  ScannerParameterFileReader,RTCSoftkey;
//  GLScene,
//  BaseCoordinateDisplay;

  //AbstractPSD,PLuginInterface,GeoLibrary,LocalHost,GLCrossPLatform,PartIDInput,
//  CLSPreview,CNCTypes,RevMesh,FeatureNodeBuilder,
//  PSDEXMLTextTypes,VectorGeometry, PSDEproject,Grids, DPCoordCutterPacket,
//  DoubleVectorGeometry,LFEPostProcessor,MeshBuilder,
//  ContextSensitivePopUpMenu,DPCoordinateTransform,DPCoordinateApproach,
//  ProjectDialog,RotWidget,UndoListPicker,TargetMachineType,
//  PSDE_DOF_EditorForm,PSDE_Cono_EditorForm,PSDE_MLaser_EditorForm,
//  PSDE_PatternedMLaser_EditorForm,CSVirtualMachine,CSToolEditor,CSTool,
//  PSDE_TouchProbe_EditorForm,FindMemo,OpNameIDInputDialog,GLColor,
//  CompoundStaticDisplay;
  //PasswordIn,ACNC_PlusMemo,ContextFileMemo,ImportPopUp,
 // ;

type
  TScannerPanelShowModes = (ssmInit,ssmMode2,ssmMode3);
  TRTC5ScannerForm = class(TInstallForm)
    DummyPageControl: TPageControl;
    GeneralTabSheet: TTabSheet;
    DebugTabSheet: TTabSheet;
    TabSheet2: TTabSheet;
    Panel2: TPanel;
    MainPanel: TPanel;
    DEbugPanel: TPanel;
    ReportMemo: TMemo;
    Panel1: TPanel;
    Button1: TButton;
    Button3: TButton;
    Button4: TButton;
    StatDisplay: TEdit;
    ResetButton: TButton;
    DebugIn: TEdit;
    Button6: TButton;
    DebugBackButton: TButton;
    ParamsParentPanel: TPanel;
    ParamsMainPanel: TPanel;
    Label1: TLabel;
    RunningIndicator: TShape;
    GroupBox1: TGroupBox;
    Label2: TLabel;
    Label3: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    LaserFreqEdit: TEdit;
    LaserPowerEdit: TEdit;
    GroupBox2: TGroupBox;
    Label4: TLabel;
    Label5: TLabel;
    MarkSpeedEdit: TEdit;
    JumpSpeedEdit: TEdit;
    GroupBox3: TGroupBox;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    JumpDelayEdit: TEdit;
    ShutterDelayEdit: TEdit;
    MarkDelayEdit: TEdit;
    PolyDelayEdit: TEdit;
    GroupBox4: TGroupBox;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    LaserOnDelayEdit: TEdit;
    LaserFirstDelayEdit: TEdit;
    LaserOffDelayEdit: TEdit;
    GroupBox5: TGroupBox;
    Label15: TLabel;
    RotationEdit: TEdit;
    GroupBox6: TGroupBox;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    ZSliceEdit: TEdit;
    ZSliceToEdit: TEdit;
    PassesEdit: TEdit;
    GroupDwellEdit: TEdit;
    GroupBox7: TGroupBox;
    Label20: TLabel;
    Label22: TLabel;
    Label23: TLabel;
    Label21: TLabel;
    Label24: TLabel;
    Label25: TLabel;
    NumProfilesEdit: TEdit;
    HPassesEdit: TEdit;
    StartProfileEdit: TEdit;
    EndProfileEdit: TEdit;
    EveryEdit: TEdit;
    TargetDepthEdit: TEdit;
    ZSliceDwellEdit: TEdit;
    DebugButton: TButton;
    SoftkeyPanel: TPanel;
    Label26: TLabel;
    procedure Button10Click(Sender: TObject);
    procedure Button9Click(Sender: TObject);
    procedure Button8Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button7Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure MainPanelResize(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure DebugBackButtonClick(Sender: TObject);
    procedure DebugButtonClick(Sender: TObject);
//    procedure FormActivate(Sender: TObject);
    procedure DummyPageControlResize(Sender: TObject);
    procedure CloseButtonClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Button6Click(Sender: TObject);
    procedure ResetButtonClick(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure ReportDebug(Sender : Tobject; Level : Integer;DebugMess : String);
    procedure HeadStatusChanged(Sender : Tobject; Stat : INteger);
  private
    ParamsGrid: TScanParameterGrid;
    Softkeys : TRTC5SoftkeySet;
    FirstShow : Boolean;
    ShowMode : TScannerPanelShowModes;
    RTC5Manager : TRTC5ScanManager;
    FDebugLevel : Integer;
    LastDebugLevel : Integer;
    HeadXPosScale : Double;
    HeadYPosScale : Double;
    HeadXSpeedScale : Double;
    HeadYSpeedScale : Double;
    HeadXOffset : Integer;
    HeadYOffset : Integer;
    HeadListSize : Integer;
    PosTop : Integer;
    PosLeft : Integer;
    PosWidth : Integer;
    PosHeight : Integer;
    PosKeyWidth : Integer;
    ScanFileReader : TScannerParametersList;
    //
    // Tags
    //
    DelayStringsTag   : TTagRef;
    SpeedStringTag    : TTagRef;
    RotationStringTag : TTagRef;
    HatchSeqTag       : TTagRef;
    GroupSeqTag       : TTagRef;
    HeadRunningTag    : TTagRef;
    ProgramNameTag    : TTagRef;
    ProgramStateStringTag  : TTagref;
    ProgramRunningTag      : TTagref;
    ProgramRunningTextTag  : TTagref;
    ScannerMovingStateTag  : TTagref;
    ScannerInBufferPercent : TTagref;
    ScannerOutBufferPercent: TTagref;
    ScannerOutBufferNumber : TTagref;



    procedure UpdateVisuals;
    procedure SetShowMode;
    procedure UpdateStatusStringsandTags;
    procedure HeadParamsChanged(Sender: TObject);
    procedure ScanFileLoaded(Filename : String);
    procedure ClearVisuals;
    procedure ScannerHasReset(Sender : TObject);
//    procedure HeadParamsChanged(Sender: TObject);
    procedure RunningChanged(Sender: TObject ; IsRunning : Boolean);
    procedure SoftkeyPressed(Sender: TRTC5Softkey ; KIndex : Integer);
    procedure HandleScannerKeys(Sender: TRTC5Softkey; KIndex: Integer);
    procedure SetDebugLevel(const Value: Integer);
    procedure ParamsGridRowSelected(Sender : TObject; GridIndex : Integer);
   public
    procedure Execute;  override;
    destructor  Destroy; override;
    procedure   Install; override;
    procedure   FinalInstall; override;

    procedure   Shutdown; override;
    procedure   CloseForm; override;
    constructor Create(AOwner: TComponent); override;
    procedure   DisplaySweep; override; // called every display sweep
    procedure SetDLLPath(DLLPath : String);
    property DebugLevel : Integer read FDebugLevel write SetDebugLevel;
  end;

var
  RTC5ScannerForm: TRTC5ScannerForm;
  ProfileDLLPath : String;

implementation

function HeadDataExists : Boolean;
begin
Result := false;
if assigned(RTC5Manager) then
  if assigned(RTC5Manager.ScanSequencer) then
     if assigned(RTC5Manager.ScanSequencer.Head.ScanFileData) then
        Result := True;

end;

{$R *.dfm}
{ TRTC5ScannerForm }

constructor TRTC5ScannerForm.Create(AOwner: TComponent);
begin
  inherited Create(Application);
  DebugLevel := 0;
  LastDebugLevel := 0;
  FirstShow := True;
  RTC5ScannerForm := Self;

end;

procedure TRTC5ScannerForm.CloseForm;
begin
  Hide;
  inherited;
end;

procedure TRTC5ScannerForm.Shutdown;
begin
  inherited;
  ScanFileReader.Free;
  Softkeys.Free;
  Close;

end;

procedure TRTC5ScannerForm.FinalInstall;
var
BinPath : String;
FStrings : TStringList;
begin
  inherited;
  ProfileDLLPath := ExtractFilePath(Application.ExeName);
  ProfileDLLPath := Format('%sLib\',[ProfileDLLPath]);
  BinPath := Format('%sRTC5Bin\',[ProfileDLLPath]);

  with RTC5Manager do
    begin
    OnDebug := ReportDebug;
    OnHeadStatusChange := HeadStatusChanged;
//    OnHeadParamsChange := HeadParamsChanged;
    InstallHead(BinPath);
    OnHeadParamsChange := HeadParamsChanged;
    ScanSequencer.BufferScanFileData.OnFileLoad := ScanFileLoaded;
    end;

//  GlobalConfigFileName := ExtractFilePath(Application.ExeName);
//  GlobalConfigFileName := Format('%sLive\Ablation.cfg',[GlobalConfigFileName]);
  if not FileExists(GlobalConfigFileName) then
    begin
    FStrings :=  TStringList.Create;
    try
    FStrings.Add('[GENERAL]');
    FStrings.Add('XPosScale=7616');
    FStrings.Add('YPosScale=7616');
    FStrings.Add('XSpeedScale=1000');
    FStrings.Add('YSpeedScale=1000');
    FStrings.Add('XOffset=0');
    FStrings.Add('YOffset=0');
    FStrings.Add('ListSize=10000');
    FStrings.SaveToFile(GlobalConfigFileName);
    finally
    FStrings.Free;
    end;
    end;

{  ConfigFile := TIniFile.Create(GlobalConfigFileName);
  try
  HeadXSpeedScale := Configfile.ReadFloat('GENERAL','XSpeedScale',0);
  HeadYSpeedScale := Configfile.ReadFloat('GENERAL','YSpeedScale',0);
  HeadXPosScale := Configfile.ReadFloat('GENERAL','XPosScale',0);
  HeadYPosScale := Configfile.ReadFloat('GENERAL','YPosScale',0);
  HeadXOffset := Configfile.ReadInteger('GENERAL','XOffset',0);
  HeadYOffset := Configfile.ReadInteger('GENERAL','YOffset',0);
  HeadListSize := Configfile.ReadInteger('GENERAL','ListSize',4000);
  PosTop   := Configfile.ReadInteger('POSITION','PosTop',25);
  PosLeft  := Configfile.ReadInteger('POSITION','PosLeft',60);
  PosWidth := Configfile.ReadInteger('POSITION','PosWidth',670);
  PosHeight:= Configfile.ReadInteger('POSITION','PosHeight',500);
  PosKeyWidth := Configfile.ReadInteger('POSITION','KeyPanelWidth',90);
  LastDebugLevel := Configfile.ReadInteger('GENERAL','PowerUpDebugLevel',10);
  finally
  ConfigFile.Free;
  end;}


  if assigned(RTC5Manager.ScanSequencer) then
    begin
    RTC5Manager.ScanSequencer.OnScannerReset := ScannerHasReset;
    end;

  if assigned(RTC5Manager.ScanSequencer.Head) then
    begin
    RTC5Manager.ScanSequencer.Head.XSpeedScale := HeadXSpeedScale;
    RTC5Manager.ScanSequencer.Head.YSpeedScale := HeadYSpeedScale;
    RTC5Manager.ScanSequencer.Head.XSpeedScale := HeadXPosScale;
    RTC5Manager.ScanSequencer.Head.YSpeedScale := HeadYPosScale;
    RTC5Manager.ScanSequencer.Head.XOffset := HeadXOffset;
    RTC5Manager.ScanSequencer.Head.YOffset := HeadYOffset;
    RTC5Manager.ScanSequencer.Head.ListSize := HeadListSize;
    RTC5Manager.ScanSequencer.OnRunStatusChange := RunningChanged;
    end;


  Named.SetAsString(ProgramNameTag,PansiChar('Unknown'));
  Named.SetAsString(HeadRunningTag,PansiChar('False'));
  Named.SetAsInteger(ProgramRunningTag,0);
  Named.SetAsString(ProgramRunningTextTag,PansiChar('IDLE'));
  Named.SetAsInteger(ScannerMovingStateTag,0);
  Named.SetAsDouble(ScannerInBufferPercent,0.0);
  Named.SetAsDouble(ScannerOutBufferPercent,0.0);
  Named.SetAsInteger(ScannerOutBufferNumber,0);

  ClearVisuals;
  SoftkeyPanel.ClientWidth := PosKeyWidth;
  Softkeys.KeyCount := 9;
  Softkeys.Align := alclient;
  SoftKeys.KeyUpEvent := SoftkeyPressed;
  UpdateStatusStringsandTags;
  Softkeys.InitialiseToStrings('Previous,Next,Page~UP,Page~DOWN,Top,Bottom,,SELECT,CLOSE');


  ScanFileReader.LoadFromFile('C:\HSA5_WIP\LaseMill runtime support\Data\ParamFile.csv');
  ParamsGrid.LoadFromParamsList(ScanFileReader);

end;

procedure TRTC5ScannerForm.Install;
var
ConfigFile : TIniFile;
begin
  inherited;
  ScanFileReader := TScannerParametersList.Create;
  ParamsGrid := TScanParameterGrid.Create(nil);
  with ParamsGrid do
    begin
    Parent := ParamsMainPanel;
    RowCount := 27;
    Align := AlBottom;
    ScrollBars := ssHorizontal;
    OnRowSelect := ParamsGridRowSelected;
    end;

  GlobalConfigFileName := ExtractFilePath(Application.ExeName);
  GlobalConfigFileName := Format('%sLive\Ablation.cfg',[GlobalConfigFileName]);

  ConfigFile := TIniFile.Create(GlobalConfigFileName);
  try
  HeadXSpeedScale := Configfile.ReadFloat('GENERAL','XSpeedScale',0);
  HeadYSpeedScale := Configfile.ReadFloat('GENERAL','YSpeedScale',0);
  HeadXPosScale := Configfile.ReadFloat('GENERAL','XPosScale',0);
  HeadYPosScale := Configfile.ReadFloat('GENERAL','YPosScale',0);
  HeadXOffset := Configfile.ReadInteger('GENERAL','XOffset',0);
  HeadYOffset := Configfile.ReadInteger('GENERAL','YOffset',0);
  HeadListSize := Configfile.ReadInteger('GENERAL','ListSize',4000);
  PosTop   := Configfile.ReadInteger('POSITION','PosTop',25);
  PosLeft  := Configfile.ReadInteger('POSITION','PosLeft',60);
  PosWidth := Configfile.ReadInteger('POSITION','PosWidth',670);
  PosHeight:= Configfile.ReadInteger('POSITION','PosHeight',500);
  PosKeyWidth := Configfile.ReadInteger('POSITION','KeyPanelWidth',90);
  LastDebugLevel := Configfile.ReadInteger('GENERAL','PowerUpDebugLevel',10);
  finally
  ConfigFile.Free;
  end;

  RTC5Manager := TRTC5ScanManager.Create;
  DebugLevel := LastDebugLevel;
  RTC5Manager.aDebugMode := LastDebugLevel;

  DelayStringsTag   := Named.AddTag('ScannerDelays', 'TStringTag');
  SpeedStringTag    := Named.AddTag('ScannerSpeeds', 'TStringTag');
  RotationStringTag := Named.AddTag('ScannerRotation', 'TStringTag');
  HatchSeqTag       := Named.AddTag('ScannerHatchSequence', 'TStringTag');
  GroupSeqTag       := Named.AddTag('ScannerGroupSequence', 'TStringTag');
  HeadRunningTag    := Named.AddTag('ScannerRunning', 'TStringTag');
  ProgramNameTag    := Named.AddTag('CurrentScannerProgram', 'TStringTag');
  ProgramStateStringTag := Named.AddTag('ScannerProgramState', 'TStringTag');
  ProgramRunningTag     := Named.AddTag('ScannerProgramRunningState', 'TIntegerTag');
  ProgramRunningTextTag     := Named.AddTag('ScannerProgramRunningText', 'TStringTag');
  ScannerMovingStateTag := Named.AddTag('ScannerMoving', 'TIntegerTag');
  ScannerInBufferPercent:= Named.AddTag('ScannerInBufferPercent', 'TDoubleTag');
  ScannerOutBufferPercent:= Named.AddTag('ScannerOutBufferPercent', 'TDoubleTag');
  ScannerOutBufferNumber := Named.AddTag('ScannerOutBufferNumber', 'TIntegerTag');

  Softkeys := TRTC5SoftkeySet.Create(SoftkeyPanel);

end;

destructor TRTC5ScannerForm.Destroy;
begin
  RTC5Manager.Free;
  inherited;
end;

procedure TRTC5ScannerForm.Button1Click(Sender: TObject);
begin
if HeadDataExists then
  begin
  REportMemo.Lines.BeginUpdate;
  try
  RTC5Manager.ScanSequencer.Head.ScanFileData.ReportFileToStrings(REportMemo.Lines,True);
  finally
  REportMemo.Lines.EndUpdate;
  end;
  end;
end;

procedure TRTC5ScannerForm.Button4Click(Sender: TObject);
var
temp : INteger;
begin
if HeadDataExists then
  begin
  REportMemo.Lines.BeginUpdate;
  try
  RTC5Manager.ScanSequencer.BufferScanFileData.ReportSettingsToStrings(REportMemo.Lines,True);
  ReportMemo.Lines.Add(RTC5Manager.ScanSequencer.ScanStageText);
  RTC5Manager.ScanSequencer.Head.ReportSetttingsToStringList(REportMemo.Lines,False);
  if RTC5Manager.ScanSequencer.ScanInterruptRequest then
    begin
    ReportMemo.Lines.Add('Interrupt High');
    end
  else
    begin
    ReportMemo.Lines.Add('Interrupt Low');
    end;
  finally
  REportMemo.Lines.EndUpdate;
  end;
  end;
end;


procedure TRTC5ScannerForm.FormShow(Sender: TObject);
begin
//ClientWidth := 665;
//DummyPageControl.Visible := false;
SoftkeyPanel.Align := alLeft;
MainPanel.Align := alClient;
ClientWidth := PosWidth;
ClientHeight := PosHeight;
Top := PosTop;
Left := POsLeft;
//ReportMemo.Clear;
DebugLevel := LastDebugLevel;
StatDisplay.Text := IntToStr(DebugLevel);

UpdateVisuals;
//Resize;
ShowMode := ssmInit;
SetShowMode;
//CloseButton.SetFocus;

MainPanelResize(Self);
end;

procedure TRTC5ScannerForm.ClearVisuals;
begin
LaserFreqEdit.Text := '--';
LaserPowerEdit.Text := '--';
MarkSpeedEdit.Text := '--';
JumpSpeedEdit.Text := '--';
MarkDelayEdit.Text := '--';
JumpDelayEdit.Text := '--';
PolyDelayEdit.Text := '--';
ShutterDelayEdit.Text := '--';
LaserOnDelayEdit.Text := '--';
LaserOffDelayEdit.Text := '--';
LaserFirstDelayEdit.Text := '--';
RotationEdit.Text := '--';
ZSliceEdit.Text := '--';
ZSliceToEdit.Text := '--';
PassesEdit.Text := '--';
GroupDwellEdit.Text := '--';
HPassesEdit.Text := '--';
StartProfileEdit.Text := '--';
EndProfileEdit.Text := '--';
NumProfilesEdit.Text := '--';
EveryEdit.Text := '--';
TargetDepthEdit.Text := '--';
ZSliceDwellEdit.Text := '--';
end;

procedure TRTC5ScannerForm.UpdateStatusStringsandTags;
begin
with RTC5Manager.ScanSequencer.Head.ScanFileData do
  begin
  Named.SetAsString(DelayStringsTag,PAnsiChar(RTC5Manager.DelaysAsString));
  Named.SetAsString(SpeedStringTag,PAnsiChar(RTC5Manager.SpeedsAsString));
  Named.SetAsString(RotationStringTag,PAnsiChar(RTC5Manager.RotationAsString));
  Named.SetAsString(GroupSeqTag, PAnsiChar(RTC5Manager.GroupSequenceAsString));
  Named.SetAsString(HatchSeqTag, PAnsiChar(RTC5Manager.HatchSequenceAsString));
  if assigned(RTC5Manager.ScanSequencer) then
    begin
    Named.SetAsString(ProgramStateStringTag, PAnsiChar(RTC5Manager.ScanSequencer.ScanStageText));
    if RTC5Manager.ScanSequencer.ProgramRunning then
      begin
      Named.SetAsString(ProgramRunningTextTag,PAnsiChar('RUNNING'));
      Named.SetAsInteger(ProgramRunningTag,1);
      end
    else
      begin
      Named.SetAsString(ProgramRunningTextTag,PAnsiChar('IDLE'));
      Named.SetAsInteger(ProgramRunningTag,0);
      end;
    if RTC5Manager.ScanSequencer.ScannerMoving then
      begin
      Named.SetAsInteger(ScannerMovingStateTag,1)
      end
    else
      begin
      Named.SetAsInteger(ScannerMovingStateTag,0)
      end;
    end;
    end;
end;

procedure TRTC5ScannerForm.UpdateVisuals;
begin
if HeadDataExists then
  if Visible then
    begin
    with RTC5Manager.ScanSequencer.Head.ScanFileData.StockParams do
      begin
      LaserFreqEdit.Text := '--';
      LaserPowerEdit.Text := '--';
      MarkSpeedEdit.Text := Format('%5.3f',[mMarkSpeed]);
      JumpSpeedEdit.Text := Format('%5.3f',[mJumpSpeed]);
      MarkDelayEdit.Text := Format('%d',[mMarkDelay]);
      JumpDelayEdit.Text := Format('%d',[mJumpDelay]);
      PolyDelayEdit.Text := Format('%d',[mPolygonDelay]);
      ShutterDelayEdit.Text := Format('%d',[mLaserShutterDelay]);
      LaserOnDelayEdit.Text := Format('%d',[mLaserOnDelay]);
      LaserOffDelayEdit.Text := Format('%d',[mLaserOffDelay]);
      LaserFirstDelayEdit.Text := Format('%d',[mLaserFirstDelay]);
      RotationEdit.Text := Format('%5.3f',[mRotation]);
      ZSliceEdit.Text := Format('%d',[mGroupSliceStart]);
      ZSliceToEdit.Text := Format('%d',[mGroupSliceEnd]);
      PassesEdit.Text := Format('%d',[mGroupSlicePasses]);
      GroupDwellEdit.Text := Format('%d',[mGroupDwell]);
      HPassesEdit.Text := Format('%d',[mHatchCount]);
      StartProfileEdit.Text := Format('%d',[mProfileStartCount]);
      EndProfileEdit.Text := Format('%d',[mProfileEndCount]);
      NumProfilesEdit.Text := Format('%d',[mProfilePasses]);
      EveryEdit.Text := Format('%d',[mProfileEvery]);
      TargetDepthEdit.Text := '??';
      ZSliceDwellEdit.Text := Format('%d',[mProfileDwell]);
      end;
{    with ParamsGrid do
      begin
      Cells[1,2] := Format('%5.3f/%5.3f',[RTC5Manager.ScanSequencer.Head.XPosScale,RTC5Manager.ScanSequencer.Head.YSpeedScale]);
      Cells[1,3] := Format('%d/%d',[RTC5Manager.ScanSequencer.Head.XOffset,RTC5Manager.ScanSequencer.Head.YOffset]);
      Cells[1,4] := Format('%5.3f/%5.3f',[RTC5Manager.ScanSequencer.Head.XSpeedScale,RTC5Manager.ScanSequencer.Head.YSpeedScale]);
      Cells[1,5] := Format('%d/%d',[RTC5Manager.ScanSequencer.Head.ListSize,RTC5Manager.ScanSequencer.Head.ListSize]);
      end;}
  end;
end;

procedure TRTC5ScannerForm.SetDLLPath(DLLPath: String);
begin
ProfileDLLPath := DLLPath;
end;

procedure TRTC5ScannerForm.ReportDebug(Sender: Tobject;  Level : Integer;DebugMess: String);
begin
if Level <= DebugLevel then ReportMemo.Lines.Add(DebugMess)
end;

procedure TRTC5ScannerForm.Button3Click(Sender: TObject);
begin
ReportMemo.Lines.Clear;
end;


procedure TRTC5ScannerForm.HeadStatusChanged(Sender: Tobject; Stat: INteger);
begin
StatDisplay.Text := IntToStr(Stat);
if DebugLevel > 2 then ReportMemo.Lines.Add(Format('Head Status %d',[Stat]))
end;


{procedure TRTC5ScannerForm.HeadParamsChanged(Sender: TObject);
begin
Named.SetAsString(DelayStringsTag,PAnsiChar(RTC5Manager.DelaysAsString));
Named.SetAsString(SpeedStringTag,PAnsiChar(RTC5Manager.SpeedsAsString));
Named.SetAsString(RotationStringTag,PAnsiChar(RTC5Manager.RotationAsString));
Named.SetAsString(GroupSeqTag, PAnsiChar(RTC5Manager.GroupSequenceAsString));
end;}

procedure TRTC5ScannerForm.ResetButtonClick(Sender: TObject);
begin
if assigned(RTC5Manager) then
  begin
  RTC5Manager.ScanSequencer.ResetScanner;
  end;

end;

procedure TRTC5ScannerForm.Button6Click(Sender: TObject);
begin
DebugLevel := StrToIntDef(DebugIn.Text,0);
DebugIn.Text := IntToStr(DebugLevel);
LastDebugLevel := DebugLevel;
end;

procedure TRTC5ScannerForm.Execute;
begin
 ShowMode := ssmInit;
 ShowModal;
end;

procedure TRTC5ScannerForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
//;DebugLevel := 0;
end;

procedure TRTC5ScannerForm.CloseButtonClick(Sender: TObject);
begin
Close;
end;


procedure TRTC5ScannerForm.HeadParamsChanged(Sender: TObject);
begin
UpdateVisuals;
UpdateStatusStringsandTags;
end;

procedure TRTC5ScannerForm.ScanFileLoaded(Filename : String);
begin
if visible then
  begin
  ParamsGrid.Cells[1,1] := Filename;
  Named.SetAsString(ProgramNameTag,PAnsichar(Filename));
  end;
end;

procedure TRTC5ScannerForm.ScannerHasReset(Sender: TObject);
begin
//ClearVisuals;
end;

procedure TRTC5ScannerForm.DummyPageControlResize(Sender: TObject);
begin
ParamsGrid.Height := ParamsMainPanel.ClientHeight-(GroupBox5.Top+GroupBox5.Height+5);
RunningIndicator.Left := ParamsMainPanel.ClientWidth- RunningIndicator.Width - 10;
end;

procedure TRTC5ScannerForm.RunningChanged(Sender: TObject; IsRunning: Boolean);
begin
if IsRunning then
  begin
  Named.SetAsString(HeadRunningTag,'True');
  RunningIndicator.Brush.Color := clGreen;
  end
else
  begin
  Named.SetAsString(HeadRunningTag,'False');
  RunningIndicator.Brush.Color := clRed;
  end;
end;

{procedure TRTC5ScannerForm.FormActivate(Sender: TObject);
begin
//Resize;
end;}

procedure TRTC5ScannerForm.SetShowMode;
begin
case ShowMode of
  ssmInit:
  begin
  DebugPanel.Visible := False;
  if DEbugPanel.HasParent then
    begin
    DEbugPanel.Parent.RemoveControl(DEbugPanel);
    DEbugPanel.Parent := DebugTabSheet;
    end
  else
    begin
    DEbugPanel.Parent := DebugTabSheet;
    end;
  if ParamsMainPanel.Parent <> MainPanel then
    begin
    ParamsMainPanel.Parent.RemoveControl(ParamsMainPanel);
    ParamsMainPanel.Parent := MainPanel;
    end;
  ParamsMainPanel.Align := alClient;
  ParamsMainPanel.Visible := True;
  end;

  ssmMode2:
  begin
  ParamsMainPanel.Visible := false;
  if ParamsMainPanel.HasParent then
    begin
    ParamsMainPanel.Parent.RemoveControl(ParamsMainPanel);
    ParamsMainPanel.Parent := ParamsParentPanel;
    end
  else
    begin
    ParamsMainPanel.Parent := ParamsParentPanel;
    end;
  DebugPanel.Parent := MainPanel;
  DebugPanel.Visible := true;
  end;

  ssmMode3:
  begin
  end;
end;
end;

procedure TRTC5ScannerForm.DebugButtonClick(Sender: TObject);
begin
ShowMode := ssmMode2;
SetShowMode;
end;

procedure TRTC5ScannerForm.DebugBackButtonClick(Sender: TObject);
begin
ShowMode := ssmInit;
SetShowMode;
end;

procedure TRTC5ScannerForm.FormResize(Sender: TObject);
begin
SoftkeyPanel.ClientWidth := PosKeyWidth;
end;

procedure TRTC5ScannerForm.MainPanelResize(Sender: TObject);
begin
ParamsGrid.Height := ParamsMainPanel.ClientHeight-(GroupBox5.Top+GroupBox5.Height+5);
RunningIndicator.Left := ParamsMainPanel.ClientWidth- RunningIndicator.Width - 10;
end;

procedure TRTC5ScannerForm.SoftkeyPressed(Sender: TRTC5Softkey; KIndex: Integer);
begin
  case ShowMode of
      ssmInit: HandleScannerKeys(Sender,KIndex);
      ssmMode2:HandleScannerKeys(Sender,KIndex);
      ssmMode3:HandleScannerKeys(Sender,KIndex);
  end;
end;

procedure TRTC5ScannerForm.HandleScannerKeys(Sender: TRTC5Softkey; KIndex: Integer);
begin
   case KIndex of
    0: ParamsGrid.UpOneIndex;
    1: ParamsGrid.DownOneIndex;
    2: ParamsGrid.UpOnePage;
    3: ParamsGrid.DownOnePage;
    4: ParamsGrid.GoToTop;
    5: ParamsGrid.GoToBottom;
    6: ParamsGrid.SelectCurrentParameters;
    7: ParamsGrid.SelectCurrentParameters;
    8: Close
   end;
end;

procedure TRTC5ScannerForm.SetDebugLevel(const Value: Integer);
begin
  FDebugLevel := Value;
end;

procedure TRTC5ScannerForm.DisplaySweep;
begin
  inherited;
  if assigned(RTC5Manager) then
    begin
    if RTC5Manager.HeadInstalled then
      begin
      Named.SetAsDouble(ScannerInBufferPercent,RTC5Manager.ScanSequencer.Head.BufferPositionPercent);
      Named.SetAsDouble(ScannerOutBufferPercent,RTC5Manager.ScanSequencer.Head.OutBufferPercent);
      Named.SetAsInteger(ScannerOutBufferNumber,RTC5Manager.ScanSequencer.Head.OutBufferNumber);
      end;
    end;

  if Visible then
    begin
    with RTC5Manager.ScanSequencer do
      begin
//      Edit1.Text  := Format('%d,%4.1f',[Head.OutBufferNumber, Head.BufferPositionPercent]);
      end;
    end;

end;

procedure TRTC5ScannerForm.Button5Click(Sender: TObject);
begin
ParamsGrid.UpOneIndex;

end;

procedure TRTC5ScannerForm.Button7Click(Sender: TObject);
begin
ParamsGrid.DownOneIndex;
end;

procedure TRTC5ScannerForm.Button2Click(Sender: TObject);
begin
ParamsGrid.UpOnePage;
end;

procedure TRTC5ScannerForm.Button8Click(Sender: TObject);
begin
ParamsGrid.DownOnePage;
end;

procedure TRTC5ScannerForm.Button9Click(Sender: TObject);
begin
ParamsGrid.GoToTop;
end;

procedure TRTC5ScannerForm.Button10Click(Sender: TObject);
begin
ParamsGrid.GoToBottom;
end;

procedure TRTC5ScannerForm.ParamsGridRowSelected(Sender: TObject; GridIndex: Integer);
var
ManParams : TScannerStockParameters;
begin
 if GridIndex > 0 then
  begin
  ManParams := ScanFileReader[GridIndex-1];
  RTC5Manager.SetStockScannerParameters(ManParams);
  end;
 ScanFileReader.ChangeActiveToIndex(GridIndex-1);
 ParamsGrid.Reload(False);
end;

initialization
  TAbstractFormPlugin.AddPlugin('RTC5Form', TRTC5ScannerForm);
end.
