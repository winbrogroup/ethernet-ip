unit IPGLib;

interface

uses Classes, SysUtils, INamedInterface, AbstractScript, IScriptInterface, IStringArray, IPGPlc, NamedPlugin;

type

  TIPGLaserPower = class (TScriptMethod)
    function Name: WideString; override; stdcall;
    function ParameterCount: Integer; override; stdcall;
    procedure Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer); override; stdcall;
  end;

  TIPGPRR = class (TScriptMethod)
    function Name: WideString; override; stdcall;
    function ParameterCount: Integer; override; stdcall;
    procedure Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer); override; stdcall;
  end;

  TIPGHeNe = class (TScriptMethod)
    function Name: WideString; override; stdcall;
    function ParameterCount: Integer; override; stdcall;
    procedure Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer); override; stdcall;
  end;

  TIPGLaser = class (TScriptMethod)
    function Name: WideString; override; stdcall;
    function ParameterCount: Integer; override; stdcall;
    procedure Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer); override; stdcall;
  end;

  TIPGQueue = class (TScriptMethod)
    function Name: WideString; override; stdcall;
    function IsFunction: Boolean; override; stdcall;
    function ParameterCount: Integer; override; stdcall;
    procedure Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer); override; stdcall;
  end;

implementation

{ TIPGLaserPower }

function TIPGLaserPower.Name: WideString;
begin
  Result:= 'PercentageLaserPower';
end;

function TIPGLaserPower.ParameterCount: Integer;
begin
  Result:= 1;
end;

procedure TIPGLaserPower.Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer);
var Tmp: Double;
begin
  if Assigned(IPGPlc.Plc) then begin
    if Assigned(IPGPlc.Plc.Comms) then begin
      Tmp:= Named.GetAsDouble(Params[0]);
      if (Tmp >= 0) and (Tmp <= 100) then
        IPGPlc.Plc.Comms.Queue('$32;' + Format('%.1f', [Tmp]))
      else
        raise Exception.CreateFmt('Invalid Laser Power: %.1f', [Tmp]);
    end;
  end;
end;

{ TIPGPRR }

function TIPGPRR.Name: WideString;
begin
  Result:= 'PRR';
end;

function TIPGPRR.ParameterCount: Integer;
begin
  Result:= 1;
end;

const
  PRRMin = 1.99;
  PRRMax = 100;

procedure TIPGPRR.Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer);
var Tmp: Double;
begin
  if Assigned(IPGPlc.Plc) then begin
    if Assigned(IPGPlc.Plc.Comms) then begin
      Tmp:= Named.GetAsDouble(Params[0]);
      if (Tmp >= PRRMin) and (Tmp <= PRRMax) then
        IPGPlc.Plc.Comms.Queue('$28;' + Format('%.1f', [Tmp]))
      else
        raise Exception.CreateFmt('Invalud laser frequency: %.1f', [Tmp]);
    end;
  end;
end;

{ TIPGHeNe }

function TIPGHeNe.Name: WideString;
begin
  Result:= 'SetHeNeOn';
end;

function TIPGHeNe.ParameterCount: Integer;
begin
  Result:= 1;
end;

procedure TIPGHeNe.Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer);
begin
  if Assigned(IPGPlc.Plc) then begin
    if Assigned(IPGPlc.Plc.Comms) then begin
       if Named.GetAsBoolean(Params[0]) then
        IPGPlc.Plc.Comms.Queue('$40;')
       else
        IPGPlc.Plc.Comms.Queue('$41;');
    end;
  end;
end;

{ TIPGLaser }

function TIPGLaser.Name: WideString;
begin
  Result:= 'SetLaserOn';
end;

function TIPGLaser.ParameterCount: Integer;
begin
  Result:= 1;
end;

procedure TIPGLaser.Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer);
begin
  if Assigned(IPGPlc.Plc) then begin
    if Assigned(IPGPlc.Plc.Comms) then begin
       if Named.GetAsBoolean(Params[0]) and not (ipgDisableLaser in IPGPlc.Plc.Values) then
        IPGPlc.Plc.Comms.Queue('$30;')
       else
        IPGPlc.Plc.Comms.Queue('$31;');
    end;
  end;
end;

{ TIPGQueue }

function TIPGQueue.Name: WideString;
begin
  Result:= 'Queue';
end;

function TIPGQueue.ParameterCount: Integer;
begin
  Result:= 0;
end;

procedure TIPGQueue.Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer);
begin
  if Assigned(IPGPlc.Plc) then begin
    if Assigned(IPGPlc.Plc.Comms) then begin
      Named.SetAsInteger(Res, IPGPlc.Plc.Comms.CommandQueueLength);
    end;
  end;
end;

function TIPGQueue.IsFunction: Boolean;
begin
  Result:= True;
end;

initialization
  TDefaultScriptLibrary.SetName('IPG');
  TDefaultScriptLibrary.AddMethod(TIPGLaserPower);
  TDefaultScriptLibrary.AddMethod(TIPGPRR);
  TDefaultScriptLibrary.AddMethod(TIPGHeNe);
  TDefaultScriptLibrary.AddMethod(TIPGLaser);
  TDefaultScriptLibrary.AddMethod(TIPGQueue);
end.
