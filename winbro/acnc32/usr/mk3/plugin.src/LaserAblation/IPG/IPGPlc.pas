unit IPGPlc;

interface
uses SysUtils, Classes, AbstractPlcPlugin, NamedPlugin, Proto, INamedInterface, IPGComms, CncTypes;

type
  TIPGCommsProperty = (
    ipgDisableLaser,
    ipgHeNeEnable,
    ipgReset,
    ipgCommsEnable,
    ipgHeNeOn,
    ipgLaserOn,
    ipgBackReflectionAlarm,
    ipgTemperatureAlarm,
    ipgSystemAlarm,
    ipgFibreAlarm,
    ipgSafetyRelay,
    ipgWarning,
    ipgCommsActive,
    ipgReady,
    ipgExample
  );

  TIPGCommsProperties = set of TIPGCommsProperty;

  TIPGCommsPlc = class(TAbstractPlcPlugin)
  private
    ResolveEv: TPlcPluginResolveItemValueEv;
    function GetHeNeEnable: Boolean;
  public
    Values: TIPGCommsProperties;
    Comms: TIPGComms;
    constructor Create( Parameters : string;
                        ResolveDeviceEv : TPlcPluginResolveDeviceEv;
                        ResolveItemEv : TPlcPluginResolveItemEv); override;
    procedure Compile ( var Embedded : TEmbeddedData;
                        aReadEv : TPlcPluginReadEv;
                        ResolveItemEv : TPlcPluginResolveItemEv;
                        Write : Boolean);  override;
    destructor Destroy; override;
    procedure GatePre; override;
    function GatePost : Integer; override;
    function Read(var Embedded : TEmbeddedData) : Boolean; override;
    procedure Write(var Embedded : TEmbeddedData; Line, LastLine : Boolean); override;
    function GateName(var Embedded : TEmbeddedData) : string; override;
    function ItemProperties : string; override;
    property HeNeEnable: Boolean read GetHeNeEnable;
    class function Properties : string; override;
    class procedure FinalInstall; override;
    class procedure Install; override;
  end;

var
  Plc: TIPGCommsPlc;

implementation

procedure DisplayProtocol(ATag: TTagRef); stdcall;
begin
  if not Named.GetAsBoolean(aTag)  then begin
    if Assigned(Plc) then begin
      if not Assigned(Protocol) then
        Protocol := TProtocol.Create(Plc.Comms);
      try
        Protocol.ShowModal;
      finally
        Protocol.Free;
        Protocol := nil;
      end;
    end;
  end;
end;


const
  PropertyName : array [TIPGCommsProperty] of TPlcProperty = (
    ( Name : 'DisableLaser';        ReadOnly: False ),
    ( Name : 'HeNeEnable';          ReadOnly: False ),
    ( Name : 'Reset';               ReadOnly: False ),
    ( Name : 'CommsEnable';         ReadOnly: False ),
    ( Name : 'HeNeOn';              ReadOnly: True ),
    ( Name : 'LaserOn';             ReadOnly: True ),
    ( Name : 'BackReflectionAlarm'; ReadOnly: True ),
    ( Name : 'TemperatureAlarm';    ReadOnly: True ),
    ( Name : 'SystemAlarm';         ReadOnly: True ),
    ( Name : 'FibreAlarm';          ReadOnly: True ),
    ( Name : 'SafetyRelay';         ReadOnly: True ),
    ( Name : 'WarningActive';       ReadOnly: True ),
    ( Name : 'Comms';               ReadOnly: True ),
    ( Name : 'Ready';               ReadOnly: True ),
    ( Name : 'Example';             ReadOnly: False )
  );

{ TIPGCommsPlc }

procedure TIPGCommsPlc.Write(var Embedded: TEmbeddedData; Line, LastLine: Boolean);
begin
  if Line <> LastLine then begin  // Edge detect rising / falling
    case TIPGCommsProperty(Embedded.Command) of
      ipgHeNeOn: begin
        if GetHeNeEnable and Line and not (ipgDisableLaser in Values) then
           Comms.Queue('$40;')
        else
          Comms.Queue('$41;');
      end;

      ipgHeNeEnable: begin
        if not Line then
          Comms.Queue('$41;');
      end;

      ipgReset: begin
        if Line then
          Comms.Queue('$50;');
      end;

      ipgCommsEnable: begin
        Comms.Enable:= Line;
      end;

      ipgDisableLaser: begin
        if Line then begin
          Comms.Queue('$41;');
        end;
      end;

    end;
    if Line then
      Include(Values, TIPGCommsProperty(Embedded.Command))
    else
      Exclude(Values, TIPGCommsProperty(Embedded.Command));
  end;
end;

procedure TIPGCommsPlc.Compile(var Embedded: TEmbeddedData; aReadEv: TPlcPluginReadEv; ResolveItemEv: TPlcPluginResolveItemEv;
  Write: Boolean);
var I : TIPGCommsProperty;
    Token : string;
    Readonly: Boolean;
begin
  Token := aReadEv(Self);
  if Token <> '.' then
    raise Exception.Create('Period [.] expected');

  Token := aReadEv(Self);
  for I := Low(I) to High(I) do begin
    if CompareText(PropertyName[I].Name, Token) = 0 then begin
      Embedded.Command := Ord(I);
      if I = ipgExample then begin
        Token := aReadEv(Self);
        if Token <> '.' then
          raise Exception.Create('Period [.] expected after DCO');
        Token:= aReadEv(Self);

        Embedded.OData:= ResolveItemEv(Self, PChar(Token), ReadOnly, ResolveEv);
      end;
      Exit;
    end;
  end;
  raise Exception.CreateFmt('Unknown property [%s]', [Token]);

end;

function TIPGCommsPlc.GateName(var Embedded: TEmbeddedData): string;
begin
  Result := PropertyName[TIPGCommsProperty(Embedded.Command)].Name;
end;

constructor TIPGCommsPlc.Create(Parameters: string; ResolveDeviceEv: TPlcPluginResolveDeviceEv;
  ResolveItemEv: TPlcPluginResolveItemEv);
var Token, Port: string;
begin
  Plc:= Self;
  Token := ReadDelimited(Parameters, ';');
  Port := 'COM1';
  if Token <> '' then begin
    Port:= Token;
  end;
  Comms:= TIPGComms.Create(Port);
  Comms.Resume;
end;

function TIPGCommsPlc.ItemProperties: string;
var I : TIPGCommsProperty;
begin
  Result:= '';
  for I := Low(I) to High(I) do
    Result := Result + PropertyName[I].Name + CarRet + BooleanIdent[I in Values] + CarRet;
end;

class procedure TIPGCommsPlc.FinalInstall;
begin
  Named.AquireTag('IPGProtocol', 'TMethodTag', DisplayProtocol);
end;

class procedure TIPGCommsPlc.Install;
begin
  IPGDeviceStatusTag:= Named.AddTag('IPGDeviceStatus', 'TIntegerTag');
  IPGModuleTemperatureTag:= Named.AddTag('IPGModuleTemperature', 'TDoubleTag');
  IPGModuleExtendedStatusTag:= Named.AddTag('IPGModuleExtendedStatus', 'TIntegerTag');
  IPGBackReflectionCounterTag:= Named.AddTag('IPGBackReflectionCounter', 'TIntegerTag');
  IPGSessionBackReflectionCounterTag:= Named.AddTag('IPGSessionBackReflectionCounter', 'TIntegerTag');
  IPGAveragePowerTag:= Named.AddTag('IPGAveragePower', 'TDoubleTag');
  IPGPulseDurationTag:= Named.AddTag('IPGPulseDuration', 'TDoubleTag');
  IPGPulseEnergyTag:= Named.AddTag('IPGPulseEnergy', 'TDoubleTag');
  IPGPeakPowerTag:= Named.AddTag('IPGPeakPower', 'TDoubleTag');
  IPGOperatingModeTag:= Named.AddTag('IPGOperatingMode', 'TIntegerTag');
  IPGPRRTag:= Named.AddTag('IPGPRR', 'TDoubleTag');
  IPGOperatingPowerTag:= Named.AddTag('IPGOperatingPower', 'TDoubleTag');
  IPGOperatingPulseEnergyTag:= Named.AddTag('IPGOperatingPulseEnergy', 'TDoubleTag');
end;

function TIPGCommsPlc.GatePost: Integer;
begin
  Result:= 0;
end;

class function TIPGCommsPlc.Properties: string;
var I : TIPGCommsProperty;
begin
  Result := '';
  for I := Low(I) to High(I) do
    Result := Result + PropertyName[I].Name + CarRet;
end;

function TIPGCommsPlc.Read(var Embedded: TEmbeddedData): Boolean;
begin
  Result := TIPGCommsProperty(Embedded.Command) in Values;
end;

destructor TIPGCommsPlc.Destroy;
begin
  if Protocol <> nil then
    Protocol.Free;
  Comms.Free;
  Plc:= nil;
  inherited;
end;

procedure TIPGCommsPlc.GatePre;
begin
end;

function TIPGCommsPlc.GetHeNeEnable: Boolean;
begin
  Result:= ipgHeNeEnable in Values;
end;

initialization
  TIPGCommsPlc.AddPlugin('IPGComms', TIPGCommsPlc);
end.
