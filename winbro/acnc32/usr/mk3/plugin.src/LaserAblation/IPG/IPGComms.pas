unit IPGComms;

interface

uses Sysutils, Classes, SyncObjs, Windows, Cnctypes, INamedInterface, NamedPlugin;

const
  SOH = 1;
  STX = 2;
  ETX = 3;
  EOT = 4;
  ENQ = 5;
  ACK = 6;
  DLE = $10;
  NAK = $15;

type
  TIPGComms = class (TThread)
  private
    CommHandle: THandle;
    CommTimeouts: TCommTimeouts;
    FComms: string;
    FOutMessage: string;
    FStateChange: Boolean;
    SendIndex: Integer;
    FOnDataChange : TNotifyEvent;
    Lock: TCriticalSection;
    CommandQueue: TStringList;
    RoundRobin: Integer;
    procedure SendRoundRobinStatus;
    procedure SendCommand(Text: string);
    function ReceiveMessage: string;
    procedure GetStatus;
    procedure ReestablishComms;
    procedure SendChar(Char: Byte);
    function GetChar: Byte;
    procedure ReceiveChar(Char: Byte);
    function GetCommandQueueLength: Integer;
  public
    InputData: array [0..512] of Byte;
    OutputData: array [0..512] of Byte;
    CommsError: Boolean;
    FDataList: TList;

    Vendor: string;

    Enable: Boolean;

    procedure Queue(Command: string);
    constructor Create(AComms: string);
    destructor Destroy; override;
    procedure Execute; override;
    property OutMessage: string read FOutMessage write FOutMessage;
    procedure ClearHistory;
    procedure SetStateChange;
    property DataList : TList{TThreadList} read FDataList;
    property OnDataChange : TNotifyEvent read FOnDataChange write FOnDataChange;
    property CommandQueueLength: Integer read GetCommandQueueLength;
  end;

var
  IPGDeviceStatusTag: TTagRef;
  IPGModuleTemperatureTag: TTagRef;
  IPGModuleExtendedStatusTag: TTagRef;
  IPGBackReflectionCounterTag: TTagRef;
  IPGSessionBackReflectionCounterTag: TTagRef;
  IPGAveragePowerTag: TTagRef;
  IPGPulseDurationTag: TTagRef;
  IPGPulseEnergyTag: TTagRef;
  IPGPeakPowerTag: TTagRef;
  IPGOperatingModeTag: TTagRef;
  IPGPRRTag: TTagRef;
  IPGOperatingPowerTag: TTagRef;
  IPGOperatingPulseEnergyTag: TTagRef;

implementation

uses
  Proto, IPGPlc;

var
  DCB: TDCB =
    (DCBLength: SizeOf(TDCB);
     BaudRate: CBR_57600;
     Flags: DTR_CONTROL_DISABLE;
     wReserved: 0;
     XonLim: 0;
     XoffLim: 0;
     ByteSize: 8;
     Parity: NOPARITY;
     StopBits: ONESTOPBIT;
     XonChar: #0;
     XoffChar: #0;
     ErrorChar: #0;
     EoFChar: #0;
     EvtChar: #0;
     wReserved1: 0);

{ TPSUComms }

constructor TIPGComms.Create(AComms: string);
begin
  inherited Create(True);
  Lock:= TCriticalSection.Create;
  CommandQueue := TStringList.Create;
  // Prefeed initial state
  FDataList:= TList.Create;

  FComms:= AComms;
  CommHandle:= CreateFile(PChar(Format('%s:', [FComms])),
     GENERIC_READ or GENERIC_WRITE, 0, nil, OPEN_EXISTING, 0, 0);
  if CommHandle = INVALID_HANDLE_VALUE then
    RaiseLastOSError;

  CommTimeouts.ReadIntervalTimeout:= 1000;
  CommTimeouts.ReadTotalTimeoutMultiplier:= 100;
  CommTimeouts.ReadTotalTimeoutConstant:= 1000;
  CommTimeouts.WriteTotalTimeoutMultiplier:= 0;
  CommTimeouts.WriteTotalTimeoutConstant:= 0;

  if not SetCommState(CommHandle, DCB) then
    RaiseLastOSError;

  if not SetCommTimeouts(CommHandle, CommTimeouts) then
    RaiseLastOSError;

end;

destructor TIPGComms.Destroy;
begin
  if CommHandle <> INVALID_HANDLE_VALUE then
    CloseHandle(CommHandle);

  Lock.Free;
  CommandQueue.Free;
  inherited;
end;

procedure TIPGComms.Execute;
begin
  while not Terminated do begin
    try
      SendRoundRobinStatus;
      GetStatus;
      CommsError:= False;

      Lock.Enter;
      try
        if CommandQueue.Count > 0 then begin
          SendCommand(CommandQueue[CommandQueue.Count -1]);
          CommandQueue.Delete(0);
          GetStatus;
        end;
      finally
        Lock.Leave;
      end;
      while not Enable do
        Sleep(50);

    except
      ReestablishComms;
      Sleep(500);
    end;
  end;
end;

procedure TIPGComms.SendChar(Char: Byte);
var Buffer: array [0..1] of Byte;
    Count: Cardinal;
begin
  Buffer[0]:= Char;
  WriteFile( CommHandle, Buffer, 1, Count, nil);
  FlushFileBuffers( CommHandle);

  if Count <> 1 then
    raise Exception.Create('Failed to write character to comms');

  FDataList.Add(Pointer(Ord(Char) or $100));
  if (SendIndex mod 1) = 0 then begin
    Sleep(10);
    SendIndex:= 0;
  end;
  Inc(SendIndex);
  if DataList.Count > 4000 then begin
    while DataList.Count > 3000 do
      DataList.Delete(0);
  end;

  if Assigned(OnDataChange) then
    OnDataChange(Self);
end;

function TIPGComms.GetChar: Byte;
var Buffer: array [0..1] of Byte;
    Count: Cardinal;
begin
  ReadFile( CommHandle, Buffer, 1, Count, nil);
  if Count = 0 then
    raise Exception.Create('Rx timeout waiting for Ack');
  Result:= Buffer[0];
  DataList.Add(Pointer(Result));
  if Assigned(OnDataChange) then
    OnDataChange(Self);
end;

procedure TIPGComms.ReestablishComms;
begin
  try
    CommsError:= True;
    SendCommand('$1;');
    while GetChar <> 0 do;
  except
  end;
end;

const
  RoundRobinStatus: array [0..12] of Integer = ( 4, 5, 11, 12, 13, 14, 16, 17, 18, 23, 29, 34, 36  );

procedure TIPGComms.GetStatus;
var Buffer: string;
    I, IVal: Integer;
    Command: Integer;
    Data: string;
begin
  Buffer:= ReceiveMessage;
  I:= Pos(';', Buffer);
  if I <> 0 then begin
  //Named.EventLog('IPG: ' + Buffer);
    Buffer:= Trim(Buffer);
    Command:= StrToIntDef(Copy(Buffer, 1, I - 1), 0);
    Data:= Copy(Buffer, I + 1, Length(Buffer) - I);
  //Named.EventLog('IPG: ' + IntToStr(Command) + ' = ' + Data);
    case Command of
      0: begin
      end;
      4: begin
        IVal:= StrToIntDef(Data, 0);
        if (IVal and $1) <> 0 then
          Include(Plc.Values, ipgBackReflectionAlarm)
        else
          Exclude(Plc.Values, ipgBackReflectionAlarm);

        if (IVal and $2) <> 0 then
          Include(Plc.Values, ipgTemperatureAlarm)
        else
          Exclude(Plc.Values, ipgTemperatureAlarm);

        if (IVal and $8) <> 0 then
          Include(Plc.Values, ipgSystemAlarm)
        else
          Exclude(Plc.Values, ipgSystemAlarm);

        {
        if (IVal and $40) <> 0 then
          Include(Plc.Values, ipgReady)
        else
          Exclude(Plc.Values, ipgReady);
         }
        if (IVal and $80) <> 0 then
          Include(Plc.Values, ipgWarning)
        else
          Exclude(Plc.Values, ipgWarning);

        if ((IVal and $100) <> 0) or ((IVal and $200) <> 0) then
          Include(Plc.Values, ipgFibreAlarm)
        else
          Exclude(Plc.Values, ipgFibreAlarm);

        if (IVal and $400) <> 0 then
          Include(Plc.Values, ipgSafetyRelay)
        else
          Exclude(Plc.Values, ipgSafetyRelay);

        Named.SetAsInteger(IPGDeviceStatusTag, IVal);
      end;
      5: Named.SetAsString(IPGModuleTemperatureTag, PChar(Data));
      11: begin
        IVal:= StrToIntDef(Data, 0);
        if (IVal and $100) <> 0 then
          Include(Plc.Values, ipgLaserOn)
        else
          Exclude(Plc.Values, ipgLaserOn);

        if (IVal and $1000) <> 0 then
          Include(Plc.Values, ipgHeNeOn)
        else
          Exclude(Plc.Values, ipgHeNeOn);

        if (IVal and $2000) <> 0 then // Unofficial laser ready
          Include(Plc.Values, ipgReady)
        else
          Exclude(Plc.Values, ipgReady);

        Named.SetAsInteger(IPGModuleExtendedStatusTag, IVal);
      end;
      12: Named.SetAsString(IPGBackReflectionCounterTag, PChar(Data));
      13: Named.SetAsInteger(IPGSessionBackReflectionCounterTag, StrToIntDef(Data, 0));
      14: Named.SetAsDouble(IPGAveragePowerTag, StrToFloatDef(Data, 0));
      15: Named.SetAsDouble(IPGPulseDurationTag, StrToFloatDef(Data, 0));
      16: Named.SetAsDouble(IPGPulseEnergyTag, StrToFloatDef(Data, 0));
      17: Named.SetAsDouble(IPGPeakPowerTag, StrToFloatDef(Data, 0));
      23: Named.SetAsInteger(IPGOperatingModeTag, StrToIntDef(Data, 0));
      29: Named.SetAsDouble(IPGPRRTag, StrToFloatDef(Data, 0));
      34: Named.SetAsDouble(IPGOperatingPowerTag, StrToFloatDef(Data, 0));
      36: Named.SetAsDouble(IPGOperatingPulseEnergyTag, StrToFloatDef(Data, 0));
      99: Vendor:= Data;
    else
    end;
  end;
end;

procedure TIPGComms.SendRoundRobinStatus;
begin
  SendCommand(Format('$%d;', [RoundRobinStatus[RoundRobin]]));
  RoundRobin:= (RoundRobin + 1) mod Length(RoundRobinStatus);
end;


procedure TIPGComms.SendCommand(Text: string);
var I: Integer;
begin
  for I:= 1 to Length(Text) do begin
    SendChar(Ord(Text[I]));
  end;
  SendChar($0d);
  Exclude(Plc.Values, ipgCommsActive);
end;

function TIPGComms.ReceiveMessage: string;
var Tmp: Byte;
begin
  Tmp:= GetChar;
  while Tmp <> $0d do begin
    Result:= Result + Char(Tmp);

    if Length(Result) > 100 then begin
      raise Exception.Create('Buffer overrun');
    end;
    Tmp:= GetChar;
  end;
  Include(Plc.Values, ipgCommsActive);
end;



procedure TIPGComms.ReceiveChar(Char: Byte);
var Tmp: Byte;
begin
  Tmp:= GetChar;
  if Tmp <> Char then
    raise Exception.Create('Unexpected character: ' + IntToHex(Tmp, 2));
end;

procedure TIPGComms.ClearHistory;
begin
  DataList.Clear;
  if Assigned(OnDataChange) then
    OnDataChange(Self);
end;

procedure TIPGComms.SetStateChange;
begin
  FStateChange:= True;
end;


procedure TIPGComms.Queue(Command: string);
begin
  if Enable then begin
    Lock.Enter;
    try
      CommandQueue.Add(Command);
    finally
      Lock.Leave;
    end;
  end;
end;

function TIPGComms.GetCommandQueueLength: Integer;
begin
  Lock.Enter;
  try
    Result:= CommandQueue.Count;
  finally
    Lock.Leave;
  end;
end;

end.
