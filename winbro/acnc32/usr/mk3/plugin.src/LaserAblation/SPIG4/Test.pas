unit Test;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, SPIPacket,
  Dialogs, IdGlobal;

type
  TForm7 = class(TForm)
    procedure FormCreate(Sender: TObject);
  private
  public
  end;

var
  Form7: TForm7;

implementation

{$R *.dfm}

procedure TForm7.FormCreate(Sender: TObject);
var B: TBytes;
    P: TSPIPacket;
begin
  P:= TSPIPacket.Create;
  P.Q[0]:= 0;
  P.Q[1]:= 0;
  P.Q[2]:= $25;
  P.Q[3]:= $80;
  p.EncodePacket($a, 4);

  SetLength(B, 8);
  B[0]:= $1b;
  B[1]:= 4;
  B[2]:= 9;
  B[3]:= 0;
  B[4]:= $a;
  B[5]:= 0;
  B[6]:= $d;
  B[7]:= $3f;
  P.SetInputBuffer(B, 8);
end;

end.
