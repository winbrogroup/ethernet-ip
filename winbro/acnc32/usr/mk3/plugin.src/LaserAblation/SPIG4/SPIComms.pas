unit SPIComms;

interface

uses SysUtils, Classes, INamedInterface, SyncObjs, IdTCPServer, IdTCPClient,
     IdContext, CncTypes, NamedPlugin, IDComponent, Forms, IdGlobal, SPIMonitor, SPIPacket;

type

  TSPIComms = class(TThread)
  private
    FConnected: Boolean;
    IdTCPClient: TIdTCPClient;
    IP: string;
    AllMonitors: TList;
    StartupMonitors: TList;
    Packet: TSPIPacket;
    procedure ClientWork(Sender: TObject; WorkMode: TWorkMode; WorkCount: Integer);
    procedure ClientConnected(Sender: TObject);
    procedure ClientDisconnected(Sender: TObject);
    procedure ClientStatus(ASender: TObject; const AStatus: TIdStatus; const AStatusText: string);
  public
    constructor Create(Host: string);
    procedure Execute; override;
    procedure CloseAndExit;
    property Connected: Boolean read FConnected;

    procedure AddMonitor(AMon: TSPIMonitor);
    procedure AddStartupMonitor(AMon: TSPIMonitor);
  end;

implementation

{ TSPIComms }

constructor TSPIComms.Create(Host: string);
begin
  inherited Create(True);
  IP:= Host;
  AllMonitors:= TList.Create;
  StartupMonitors:= TList.Create;
  Packet:= TSPIPacket.Create;
end;

procedure TSPIComms.Execute;
var Monitor: TSPIMonitor;
    I: Integer;
begin
  while not Terminated do begin
    IdTCPClient:= TIdTCPClient.Create(Application);
    IdTCPClient.Host:= IP;
    IdTCPClient.Port:= 58174;
    IdTCPClient.OnWork:= ClientWork;
    IdTCPClient.OnConnected:= ClientConnected;
    IdTCPClient.OnDisconnected:= ClientDisconnected;
    IdTCPClient.OnStatus:= ClientStatus;
    if not IdTCPClient.Connected then begin
      try
        IdTCPClient.Connect;
      except
        on E: Exception do
          //Named.EventLog('Failed connection: ' + E.Message);
      end;
    end;
    IdTCPClient.Free;
    IdTCPClient:= nil;
    //Sleep(1000);
  end;
  for I:= 0 to StartupMonitors.Count -1 do begin
     Monitor:= TSPIMonitor(StartupMonitors[I]);
     Monitor.Free;
  end;

  for I:= 0 to AllMonitors.Count -1 do begin
     Monitor:= TSPIMonitor(AllMonitors[I]);
     Monitor.Free;
  end;
  StartupMonitors.Free;
  AllMonitors.Free;
end;

procedure TSPIComms.ClientWork(Sender: TObject;
  WorkMode: TWorkMode; WorkCount: Integer);
begin
end;


procedure TSPIComms.ClientConnected(Sender: TObject);
var I: Integer;
    Monitor: TSPIMonitor;
begin
  FConnected:= True;
  //Named.EventLog('TNetworkIOSubSystemThread: ' + IdTCPClient.IOHandler.ReadLn);
  try
    IdTCPClient.ReadTimeout:= 1000;

    for I:= 0 to StartupMonitors.Count -1 do begin
       Monitor:= TSPIMonitor(StartupMonitors[I]);
       Monitor.Read(Packet, IdTCPClient.IOHandler);
    end;

    while IdTCPClient.IOHandler.Connected do begin
      for I:= 0 to AllMonitors.Count -1 do begin
         Monitor:= TSPIMonitor(AllMonitors[I]);
         Monitor.Read(Packet, IdTCPClient.IOHandler);
         Sleep(10);
         if Terminated then begin
           IdTCPClient.Disconnect;
           Break; // Infiniate loop
         end;

      end;
{      IdTCPClient.IOHandler.WriteDirect(ABuffer);
      IdTCPClient.IOHandler.ReadBytes(ABuffer, 12, false);}
    end;
  except
    on E: Exception do
      Named.EventLog('Acnc32NetworkIOSubSystem: ' + E.Message);
  end;
  FConnected:= False;
end;


procedure TSPIComms.ClientStatus(ASender: TObject;
  const AStatus: TIdStatus; const AStatusText: string);
begin

end;

procedure TSPIComms.ClientDisconnected(Sender: TObject);
begin
  FConnected:= False;
end;


procedure TSPIComms.CloseAndExit;
begin
  Terminate;
  Self.WaitFor;
end;

procedure TSPIComms.AddMonitor(AMon: TSPIMonitor);
begin
  AllMonitors.Add(AMon);
end;

procedure TSPIComms.AddStartupMonitor(AMon: TSPIMonitor);
begin
  StartupMonitors.Add(AMon);
end;

end.
