unit SPIPlc;

interface

uses SysUtils, Classes, AbstractPlcPlugin, NamedPlugin, INamedInterface, SPIComms, CncTypes, Monitors;

type
  TSPIProperty = (
    spiConnected
  );

  TSPIProperties = set of TSPIProperty;

  TSPIPlc = class(TAbstractPlcPlugin)
  private
    Comms: TSPIComms;
  public
    Values: TSPIProperties;
    constructor Create( Parameters : string;
                        ResolveDeviceEv : TPlcPluginResolveDeviceEv;
                        ResolveItemEv : TPlcPluginResolveItemEv); override;
    procedure Compile ( var Embedded : TEmbeddedData;
                        aReadEv : TPlcPluginReadEv;
                        ResolveItemEv : TPlcPluginResolveItemEv;
                        Write : Boolean);  override;
    destructor Destroy; override;
    procedure GatePre; override;
    function GatePost : Integer; override;
    function Read(var Embedded : TEmbeddedData) : Boolean; override;
    procedure Write(var Embedded : TEmbeddedData; Line, LastLine : Boolean); override;

    function GateName(var Embedded : TEmbeddedData) : string; override;
    function ItemProperties : string; override;
    class function Properties : string; override;
    class procedure FinalInstall; override;
    class procedure Install; override;
    class procedure Shutdown; override;
  end;

implementation

var
  mLaserControlSignalsTag: TTagRef;
  mSimmerCurrentTag: TTagRef;
	mActiveCurrentTag: TTagRef;
	mBaudRateTag: TTagRef;
	mDigitagOutputsTag: TTagRef;
	mDigitagInputsTag: TTagRef;
	mActiveAnalogueInputTag: TTagRef;
	mSimmerAnalogueInputTag: TTagRef;
	mPreAmpCurrentTag: TTagRef;
	mPowerCurrentTag: TTagRef;
	mRatedPowerTag: TTagRef;
	mMaxPulseRateTag: TTagRef;
	mRatedEnergyTag: TTagRef;
	mInstalledFeaturesTag: TTagRef;
	mFPGAHardwareRevisionTag: TTagRef;
	mFPGAHardwareReleaseTag: TTagRef;
	mFPGAFirmwareRevisionTag: TTagRef;
	mFPGAFirmwareReleaseTag: TTagRef;
	mStellarisFirmwareVersionTag: TTagRef;
	mDriverFirmwareTag: TTagRef;
	mLaserPartNumberTag: TTagRef;
	mLaserSerialNumberTag: TTagRef;
	mLaserTemperatureTag: TTagRef;
	mBeamDelieveryTemperatureTag: TTagRef;
	mOperatingHoursTag: TTagRef;
	mLogicPowerSupplyVoltageTag: TTagRef;
	mDiodePowerSupplyVoltageTag: TTagRef;
	mPulseBurstLengthTag: TTagRef;
	mPulseRateTag: TTagRef;
	mSelectedPulseWaveformTag: TTagRef;
	mPRFTag: TTagRef;
	mPumpDutyFactorTag: TTagRef;
	mStatusLinesTag: TTagRef;
	mActiveAlarmCodesTag: TTagRef;
  Instance: TSPIPlc;

  const PropertyName : array [TSPIProperty] of TPlcProperty = (
    ( Name : 'Connected';   ReadOnly: False )
    );

{ TSPIPlc }

procedure TSPIPlc.Write(var Embedded: TEmbeddedData; Line, LastLine: Boolean);
begin
end;

procedure TSPIPlc.Compile(var Embedded: TEmbeddedData; aReadEv: TPlcPluginReadEv; ResolveItemEv: TPlcPluginResolveItemEv;
  Write: Boolean);
var I : TSPIProperty;
    Token : string;
begin
  Token := aReadEv(Self);
  if Token <> '.' then
    raise Exception.Create('Period [.] expected');

  Token := aReadEv(Self);
  for I := Low(I) to High(I) do begin
    if CompareText(PropertyName[I].Name, Token) = 0 then begin
      Embedded.Command := Ord(I);
      Exit;
    end;
  end;
  raise Exception.CreateFmt('Unknown property [%s]', [Token]);
end;

function TSPIPlc.GateName(var Embedded: TEmbeddedData): string;
begin
  Result := PropertyName[TSPIProperty(Embedded.Command)].Name;
end;

constructor TSPIPlc.Create(Parameters: string; ResolveDeviceEv: TPlcPluginResolveDeviceEv; ResolveItemEv: TPlcPluginResolveItemEv);
var Token, IPAddress: string;
begin
  Token := ReadDelimited(Parameters, ';');
  IPAddress := 'localhost';
  if Token <> '' then begin
    IPAddress:= Token;
  end;
  Instance:= Self;
  Comms:= TSPIComms.Create(IPAddress);
  Comms.AddMonitor(TSimmerCurrentMonitor.Create(mSimmerCurrentTag));
  Comms.AddMonitor(TActiveCurrentMonitor.Create(mActiveCurrentTag));
  Comms.AddMonitor(TBaudRateMonitor.Create(mBaudRateTag));
  Comms.AddMonitor(THardwareInterfaceMonitor.Create(mDigitagOutputsTag, mDigitagInputsTag, mActiveAnalogueInputTag,mSimmerAnalogueInputTag));
  Comms.AddMonitor(TLaserControlSignalsMonitor.Create(mLaserControlSignalsTag));
  Comms.AddMonitor(TLaserCurrentsMonitor.Create(mPreAmpCurrentTag, mPowerCurrentTag));
  Comms.AddMonitor(TLaserDescription.Create(mRatedPowerTag, mMaxPulseRateTag, mRatedEnergyTag,mInstalledFeaturesTag));
  Comms.AddMonitor(TLaserFirmwareDetails.Create(mFPGAHardwareRevisionTag, mFPGAHardwareReleaseTag, mFPGAFirmwareRevisionTag,mFPGAFirmwareReleaseTag,mStellarisFirmwareVersionTag,mDriverFirmwareTag));
  Comms.AddMonitor(TLaserPartNumber.Create(mLaserPartNumberTag));
  Comms.AddMonitor(TLaserSerialNumber.Create(mLaserSerialNumberTag));
  Comms.AddMonitor(TLaserTemperaturesMonitor.Create(mLaserTemperatureTag, mBeamDelieveryTemperatureTag));
  Comms.AddMonitor(TOperatingHoursMonitor.Create(mOperatingHoursTag));
  Comms.AddMonitor(TPowerSupplyVoltagesMonitor.Create(mLogicPowerSupplyVoltageTag, mDiodePowerSupplyVoltageTag));
  Comms.AddMonitor(TPulseBurstLengthMonitor.Create(mPulseBurstLengthTag));
  Comms.AddMonitor(TPulseRateMonitor.Create(mPulseRateTag));
  Comms.AddMonitor(TPulseWaveformMonitor.Create(mSelectedPulseWaveformTag, mPRFTag));
  Comms.AddMonitor(TPumpDutyFactorMonitor.Create(mPumpDutyFactorTag));
  Comms.AddMonitor(TStatusLinesAndAlamsMonitor.Create(mStatusLinesTag,mActiveAlarmCodesTag));

  Comms.Resume;
end;

function TSPIPlc.ItemProperties: string;
var I : TSPIProperty;
begin
  Result:= '';
  for I := Low(I) to High(I) do
    Result := Result + PropertyName[I].Name + CarRet + BooleanIdent[I in Values] + CarRet;
end;

class procedure TSPIPlc.FinalInstall;
begin

end;

class procedure TSPIPlc.Install;
begin
  mSimmerCurrentTag:= Named.AddTag('SPISimmerCurrent', 'TIntegerTag');
	mActiveCurrentTag:= Named.AddTag('SPIActiveCurrent', 'TIntegerTag');
	mBaudRateTag:= Named.AddTag('SPIBaudRate', 'TIntegerTag');
	mDigitagOutputsTag:= Named.AddTag('SPIDigitagOutputs', 'TIntegerTag');
	mDigitagInputsTag:= Named.AddTag('SPIDigitagInputs', 'TIntegerTag');
	mActiveAnalogueInputTag:= Named.AddTag('SPIActiveAnalogueInput', 'TIntegerTag');
	mSimmerAnalogueInputTag:= Named.AddTag('SPISimmerAnalogueInput', 'TIntegerTag');
  mLaserControlSignalsTag:= Named.AddTag('SPIControlSignals', 'TIntegerTag');
  mPreAmpCurrentTag:= Named.AddTag('SPIPreAmpCurrent', 'TIntegerTag');
	mPowerCurrentTag:= Named.AddTag('SPIPowerCurrent', 'TIntegerTag');
	mRatedPowerTag:= Named.AddTag('SPIRatedPower', 'TIntegerTag');
	mMaxPulseRateTag:= Named.AddTag('SPIMaxPulseRate', 'TIntegerTag');
	mRatedEnergyTag:= Named.AddTag('SPIRatedEnergy', 'TIntegerTag');
	mInstalledFeaturesTag:= Named.AddTag('SPIInstalledFeatures', 'TIntegerTag');
	mFPGAHardwareRevisionTag:= Named.AddTag('SPIFPGAHardwareRevision', 'TIntegerTag');
	mFPGAHardwareReleaseTag:= Named.AddTag('SPIFPGAHardwareRelease', 'TIntegerTag');
	mFPGAFirmwareRevisionTag:= Named.AddTag('SPIFPGAFirmwareRevision', 'TIntegerTag');
	mFPGAFirmwareReleaseTag:= Named.AddTag('SPIFPGAFirmwareRelease', 'TIntegerTag');
	mStellarisFirmwareVersionTag:= Named.AddTag('SPIStellarisFirmwareVersion', 'TIntegerTag');
	mDriverFirmwareTag:= Named.AddTag('SPIDriverFirmware', 'TIntegerTag');
	mLaserPartNumberTag:= Named.AddTag('SPILaserPartNumber', 'TStringTag');
	mLaserSerialNumberTag:= Named.AddTag('SPILaserSerialNumber', 'TIntegerTag');
	mLaserTemperatureTag:= Named.AddTag('SPILaserTemperature', 'TIntegerTag');
	mBeamDelieveryTemperatureTag:= Named.AddTag('SPIBeamDelieveryTemperature', 'TIntegerTag');
	mOperatingHoursTag:= Named.AddTag('SPIOperatingHours', 'TIntegerTag');
	mLogicPowerSupplyVoltageTag:= Named.AddTag('SPILogicPowerSupplyVoltage', 'TIntegerTag');
	mDiodePowerSupplyVoltageTag:= Named.AddTag('SPIDiodePowerSupplyVoltage', 'TIntegerTag');
	mPulseBurstLengthTag:= Named.AddTag('SPIPulseBurstLength', 'TIntegerTag');
	mPulseRateTag:= Named.AddTag('SPIPulseRate', 'TIntegerTag');
	mSelectedPulseWaveformTag:= Named.AddTag('SPISelectedPulseWaveform', 'TIntegerTag');
	mPRFTag:= Named.AddTag('SPIPRF', 'TIntegerTag');
	mPumpDutyFactorTag:= Named.AddTag('SPIPumpDutyFactor', 'TIntegerTag');
	mStatusLinesTag:= Named.AddTag('SPIStatusLines', 'TIntegerTag');
	mActiveAlarmCodesTag:= Named.AddTag('SPIActiveAlarmCodes', 'TIntegerTag');

end;

function TSPIPlc.GatePost: Integer;
begin
  if Comms.Connected then
    Include(Values, spiConnected)
  else
    Exclude(Values, spiConnected);
  Result:= 0;
end;

class function TSPIPlc.Properties: string;
var I : TSPIProperty;
begin
  Result := '';
  for I := Low(I) to High(I) do
    Result := Result + PropertyName[I].Name + CarRet;
end;

function TSPIPlc.Read(var Embedded: TEmbeddedData): Boolean;
begin
  Result:= TSPIProperty(Embedded.Command) in Values;
end;

destructor TSPIPlc.Destroy;
begin
  if Assigned(Comms) then begin
    Comms.CloseAndExit;
  end ;
  Instance:= nil;
  inherited;
end;

procedure TSPIPlc.GatePre;
begin
end;

class procedure TSPIPlc.Shutdown;
begin
  if Assigned(Instance) then begin
    if Assigned(Instance.Comms) then begin
      Instance.Comms.CloseAndExit;
    end ;
    //Sleep(2000);
  end;
end;

initialization
  TSPIPlc.AddPlugin('SPIComms', TSPIPlc);
end.
