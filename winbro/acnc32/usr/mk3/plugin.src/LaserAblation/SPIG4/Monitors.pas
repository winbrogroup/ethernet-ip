unit Monitors;

interface

uses SysUtils, Classes, idGlobal, IdIOHandler, SPIMonitor, SPIPacket, INamedInterface, NamedPlugin, IdTCPClient;

type

  TCommandByte = (
   BAUD_RATE = $10,
   LASER_CONTROL_INTERFACE_MODE = $15,
   LASER_CONTROL_SIGNALS = $1B,
   ANALOGUE_SIGNALS = $1D,
   PULSE_WAVEFORM = $21,
   PULSE_RATE = $23,
   PULSE_BURST_LENGTH = $25,
   PUMP_DUTY_FACTOR = $27,
   ALL_PULSE_GENERATOR_PARAMETERS = $2D,
   STATUS_LINES_AND_ALARMS = $50,
	 LASER_TEMPERATURES = $51,
	 LASER_CURRENTS = $52,
	 POWER_SUPPLY_VOLTAGES = $53,
	 HARDWARE_INTERFACE = $54,
	 OPERATING_HOURS = $55,
	 LASER_SERIAL_NUMBER = $62,
	 LASER_PART_NUMBER = $63,
	 LASER_FIRMWARE_DETAILS = $64,
	 LASER_DESCRIPTION = $65);

  TLaserControlSignalsMonitor = class(TSPIMonitor)
  private
    Tag: TTagRef;
  public
    constructor Create(Tag: TTagRef);
    procedure Read(packet: TSPIPacket; Handler: TIdIOHandler); override;
 end;

  TAllPulseGeneratorParameters = class(TSPIMonitor)
  private
    Tag, Tag2,Tag3,Tag4, Tag5, Tag6: TTagRef;
  public
    constructor Create(Tag: TTagRef);
    procedure Read(packet: TSPIPacket; Handler: TIdIOHandler); override;
  end;

  TSimmerCurrentMonitor = class(TSPIMonitor)
  private
    Tag: TTagRef;
  public
    constructor Create(Tag: TTagRef);
    procedure Read(packet: TSPIPacket; Handler: TIdIOHandler); override;
  end;

  TActiveCurrentMonitor = class(TSPIMonitor)
  private
    Tag: TTagRef;
  public
    constructor Create(Tag: TTagRef);
    procedure Read(packet: TSPIPacket; Handler: TIdIOHandler); override;
  end;

  TBaudRateMonitor = class(TSPIMonitor)
  private
    Tag: TTagRef;
  public
    constructor Create(Tag: TTagRef);
    procedure Read(packet: TSPIPacket; Handler: TIdIOHandler); override;
  end;

  THardwareInterfaceMonitor = class(TSPIMonitor)
  private
    Tag, Tag2, Tag3, Tag4: TTagRef;
  public
    constructor Create(Tag, Tag2, Tag3, Tag4: TTagRef);
    procedure Read(packet: TSPIPacket; Handler: TIdIOHandler); override;
  end;

  TLaserControlInterfaceMode = class(TSPIMonitor)
  private
    Tag: TTagRef;
  public
    constructor Create(Tag: TTagRef);
    procedure Read(packet: TSPIPacket; Handler: TIdIOHandler); override;
  end;

  TLaserCurrentsMonitor = class(TSPIMonitor)
  private
    Tag, Tag2: TTagRef;
  public
    constructor Create(Tag, Tag2: TTagRef);
    procedure Read(packet: TSPIPacket; Handler: TIdIOHandler); override;
  end;

  TLaserDescription = class(TSPIMonitor)
  private
    Tag, Tag2, Tag3, Tag4: TTagRef;
  public
    constructor Create(Tag, Tag2, Tag3, Tag4: TTagRef);
    procedure Read(packet: TSPIPacket; Handler: TIdIOHandler); override;
  end;

  TLaserFirmwareDetails = class(TSPIMonitor)
  private
    Tag, Tag2, Tag3, Tag4, Tag5, Tag6: TTagRef;
  public
    constructor Create(Tag, Tag2, Tag3, Tag4, Tag5, Tag6: TTagRef);
    procedure Read(packet: TSPIPacket; Handler: TIdIOHandler); override;
  end;

  TLaserPartNumber = class(TSPIMonitor)
  private
    Tag: TTagRef;
  public
    constructor Create(Tag: TTagRef);
    procedure Read(packet: TSPIPacket; Handler: TIdIOHandler); override;
  end;

  TLaserSerialNumber = class(TSPIMonitor)
  private
    Tag: TTagRef;
  public
    constructor Create(Tag: TTagRef);
    procedure Read(packet: TSPIPacket; Handler: TIdIOHandler); override;
  end;

  TLaserTemperaturesMonitor = class(TSPIMonitor)
  private
    Tag, Tag2: TTagRef;
  public
    constructor Create(Tag, Tag2: TTagRef);
    procedure Read(packet: TSPIPacket; Handler: TIdIOHandler); override;
  end;

  TOperatingHoursMonitor = class(TSPIMonitor)
  private
    Tag: TTagRef;
  public
    constructor Create(Tag: TTagRef);
    procedure Read(packet: TSPIPacket; Handler: TIdIOHandler); override;
  end;

  TPowerSupplyVoltagesMonitor = class(TSPIMonitor)
  private
    Tag, Tag2: TTagRef;
  public
    constructor Create(Tag, Tag2: TTagRef);
    procedure Read(packet: TSPIPacket; Handler: TIdIOHandler); override;
  end;

  TPulseBurstLengthMonitor = class(TSPIMonitor)
  private
    Tag: TTagRef;
  public
    constructor Create(Tag: TTagRef);
    procedure Read(packet: TSPIPacket; Handler: TIdIOHandler); override;
  end;

  TPulseRateMonitor = class(TSPIMonitor)
  private
    Tag: TTagRef;
  public
    constructor Create(Tag: TTagRef);
    procedure Read(packet: TSPIPacket; Handler: TIdIOHandler); override;
  end;

  TPulseWaveformMonitor = class(TSPIMonitor)
  private
    Tag, Tag2: TTagRef;
  public
    constructor Create(Tag, Tag2: TTagRef);
    procedure Read(packet: TSPIPacket; Handler: TIdIOHandler); override;
  end;

  TPumpDutyFactorMonitor = class(TSPIMonitor)
  private
    Tag: TTagRef;
  public
    constructor Create(Tag: TTagRef);
    procedure Read(packet: TSPIPacket; Handler: TIdIOHandler); override;
  end;

  TStatusLinesAndAlamsMonitor = class(TSPIMonitor)
  private
    Tag, Tag2: TTagRef;
  public
    constructor Create(Tag, Tag2: TTagRef);
    procedure Read(packet: TSPIPacket; Handler: TIdIOHandler); override;
  end;


implementation

{ TLaserControlSignalsMonitor }

constructor TLaserControlSignalsMonitor.Create(Tag: TTagRef);
begin
  Self.Tag:= Tag;
end;

procedure TLaserControlSignalsMonitor.Read(packet: TSPIPacket; Handler: TIdIOHandler);
begin
  Packet.EncodePacket(Ord(LASER_CONTROL_SIGNALS), 0);
  Packet.Write(Handler);
  Packet.Read(Handler);
  Packet.DecodePacket;

  // Do Something with Packet.X data

  Named.SetAsInteger(Tag, Packet.X[0]);
end;

{ TSimmerCurrentMonitor }

constructor TSimmerCurrentMonitor.Create(Tag: TTagRef);
begin
  Self.Tag := Tag;
end;

procedure TSimmerCurrentMonitor.Read(packet: TSPIPacket; Handler: TIdIOHandler);
begin
  Packet.Q[0] := Byte(1);
  Packet.EncodePacket(ord(ANALOGUE_SIGNALS), 0);
  Packet.Write(Handler);
  Packet.Read(Handler);
  Packet.DecodePacket;
  Named.SetAsInteger(Tag, Packet.X[0] * $100 + Packet.X[1]);
end;

{ TActiveCurrentMonitor }

constructor TActiveCurrentMonitor.Create(Tag: TTagRef);
begin
  Self.Tag := Tag;
end;

procedure TActiveCurrentMonitor.Read(packet: TSPIPacket; Handler: TIdIOHandler);
begin
  Packet.Q[0] := Byte(0);
  Packet.EncodePacket(ord(ANALOGUE_SIGNALS), 0);
  Packet.Write(Handler);
  Packet.Read(Handler);
  Packet.DecodePacket;
  Named.SetAsInteger(Tag, Packet.X[0] * $100 + Packet.X[1]);
end;

{ TTAllPulseGeneratorParameters }

constructor TAllPulseGeneratorParameters.Create(Tag: TTagRef);
begin
  Self.Tag := Tag;
  Self.Tag2 := Tag2;
  Self.Tag3 := Tag3;
  Self.Tag4 := Tag4;
  Self.Tag5 := Tag5;
  Self.Tag6 := Tag6;
end;

procedure TAllPulseGeneratorParameters.Read(packet: TSPIPacket; Handler: TIdIOHandler);
begin
  Packet.Q[0] := Byte(0);
  Packet.EncodePacket(ord(ALL_PULSE_GENERATOR_PARAMETERS), 0);
  Packet.Write(Handler);
  Packet.Read(Handler);
  Packet.DecodePacket;
  Named.SetAsInteger(Tag, Packet.X[0] * $100 + Packet.X[1]);
  Named.SetAsInteger(Tag2, Packet.X[2] * $100 + Packet.X[3]);
  Named.SetAsInteger(Tag3, Packet.X[4] * $1000000 + Packet.X[5] * $10000 + Packet.X[6] * $100 + Packet.X[7]);
  Named.SetAsInteger(Tag4, Packet.X[8] * $1000000 + Packet.X[9] * $10000 + Packet.X[10] * $100 + Packet.X[11]);
  Named.SetAsInteger(Tag5, Packet.X[12] * $1000000 + Packet.X[13] * $10000 + Packet.X[14] * $100 + Packet.X[15]);
  Named.SetAsInteger(Tag6, Packet.X[16] * $100 + Packet.X[17]);
end;

{ TBaudRateMonitor }

constructor TBaudRateMonitor.Create(Tag: TTagRef);
begin
  Self.Tag := Tag;
end;

procedure TBaudRateMonitor.Read(packet: TSPIPacket; Handler: TIdIOHandler);
begin
  Packet.Q[0] := Byte(0);
  Packet.EncodePacket(ord(BAUD_RATE), 0);
  Packet.Write(Handler);
  Packet.Read(Handler);
  Packet.DecodePacket;
  //Named.SetAsInteger(Tag, Packet.X[0] * $1000000 + Packet.X[1] * $10000 + Packet.X[2] * $100 + Packet.X[3]);
  Named.SetAsInteger(Tag, Packet.X[1] * $10000 + Packet.X[2] * $100 + Packet.X[3]);
end;

{ THardwareInterfaceMonitor }

constructor THardwareInterfaceMonitor.Create(Tag, Tag2, Tag3, Tag4: TTagRef);
begin
  Self.Tag:= Tag;
  Self.Tag2:= Tag2;
  Self.Tag3:= Tag3;
  Self.Tag4:= Tag4;
end;

procedure THardwareInterfaceMonitor.Read(packet: TSPIPacket; Handler: TIdIOHandler);
begin
  Packet.Q[0] := Byte(0);
  Packet.EncodePacket(ord(HARDWARE_INTERFACE), 0);
  Packet.Write(Handler);
  Packet.Read(Handler);
  Packet.DecodePacket;
  Named.SetAsInteger(Tag, Packet.X[0] * $100 + Packet.X[1]);
  Named.SetAsInteger(Tag2, Packet.X[2] * $100 + Packet.X[3]);
  Named.SetAsInteger(Tag3, Packet.X[4] * $100 + Packet.X[5]);
  Named.SetAsInteger(Tag4, Packet.X[6] * $100 + Packet.X[7]);
end;


{ TLaserControlInterfaceMode }

constructor TLaserControlInterfaceMode.Create(Tag: TTagRef);
begin
  Self.Tag:= Tag;
end;

procedure TLaserControlInterfaceMode.Read(packet: TSPIPacket; Handler: TIdIOHandler);
begin
  Packet.Q[0] := Byte(0);
  Packet.EncodePacket(ord(LASER_CONTROL_INTERFACE_MODE), 0);
  Packet.Write(Handler);
  Packet.Read(Handler);
  Packet.DecodePacket;
  Named.SetAsInteger(Tag, Packet.X[0] * $1000000 + Packet.X[1] * $10000 + Packet.X[2] * $100 + Packet.X[3]);
end;

{ TLaserCurrentsMonitor }

constructor TLaserCurrentsMonitor.Create(Tag, Tag2: TTagRef);
begin
  Self.Tag:= Tag;
  Self.Tag2:= Tag2;
end;

procedure TLaserCurrentsMonitor.Read(packet: TSPIPacket; Handler: TIdIOHandler);
begin
  Packet.Q[0] := Byte(0);
  Packet.EncodePacket(ord(LASER_CURRENTS), 0);
  Packet.Write(Handler);
  Packet.Read(Handler);
  Packet.DecodePacket;
  Named.SetAsInteger(Tag, Packet.X[0] * $100 + Packet.X[1]);
  Named.SetAsInteger(Tag2, Packet.X[2] * $100 + Packet.X[3]);
end;

{ TLaserDescription }

constructor TLaserDescription.Create(Tag, Tag2, Tag3, Tag4: TTagRef);
begin
  Self.Tag:= Tag;
  Self.Tag2:= Tag2;
  Self.Tag3:= Tag3;
  Self.Tag4:= Tag4;
end;

procedure TLaserDescription.Read(packet: TSPIPacket; Handler: TIdIOHandler);
begin
  Packet.Q[0] := Byte(0);
  Packet.EncodePacket(ord(LASER_DESCRIPTION), 0);
  Packet.Write(Handler);
  Packet.Read(Handler);
  Packet.DecodePacket;
  Named.SetAsInteger(Tag, Packet.X[0] * $100 + Packet.X[1]);
  Named.SetAsInteger(Tag2, Packet.X[2] * $100 + Packet.X[3]);
  Named.SetAsInteger(Tag3, Packet.X[4] * $100 + Packet.X[5]);
  Named.SetAsInteger(Tag4, Packet.X[6] * $1000000 + Packet.X[7] * $10000 + Packet.X[8] * $100 + Packet.X[9]);
end;

{ TLaserFirmwareDetails }

constructor TLaserFirmwareDetails.Create(Tag, Tag2, Tag3, Tag4, Tag5, Tag6: TTagRef);
begin
  Self.Tag:= Tag;
  Self.Tag2:= Tag2;
  Self.Tag3:= Tag3;
  Self.Tag4:= Tag4;
  Self.Tag5:= Tag5;
  Self.Tag6:= Tag6;
end;

procedure TLaserFirmwareDetails.Read(packet: TSPIPacket; Handler: TIdIOHandler);
begin
  Packet.Q[0] := Byte(0);
  Packet.EncodePacket(ord(LASER_FIRMWARE_DETAILS), 0);
  Packet.Write(Handler);
  Packet.Read(Handler);
  Packet.DecodePacket;
  Named.SetAsInteger(Tag, Packet.X[0] * $100 + Packet.X[1]);
  Named.SetAsInteger(Tag2, Packet.X[2] * $100 + Packet.X[3]);
  Named.SetAsInteger(Tag3, Packet.X[4] * $100 + Packet.X[5]);
  Named.SetAsInteger(Tag4, Packet.X[6] * $100 + Packet.X[7]);
  Named.SetAsInteger(Tag5, Packet.X[8] * $1000000 + Packet.X[9] * 10000 + Packet.X[10] * $100 + Packet.X[11]);
  Named.SetAsInteger(Tag6, Packet.X[12] * $100 + Packet.X[13]);
end;

{ TLaserPartNumber }

constructor TLaserPartNumber.Create(Tag: TTagRef);
begin
  Self.Tag:= Tag;
end;

procedure TLaserPartNumber.Read(packet: TSPIPacket; Handler: TIdIOHandler);
var Buffer: array[0..255] of Char;
    I : Integer;
begin
  Packet.Q[0] := Byte(0);
  Packet.EncodePacket(ord(LASER_PART_NUMBER), 0);
  Packet.Write(Handler);
  Packet.Read(Handler);
  Packet.DecodePacket;

  for I:= 0 to 32 do begin
    if Packet.X[I] < $20 then begin
      Buffer[I] := #0;
      Break;
    end;
    Buffer[I] := Char(Packet.X[I]);
  end;
  Buffer[32]:= #0;

  Named.SetAsString(Tag, Buffer);
end;

{ TLaserSerialNumber }

constructor TLaserSerialNumber.Create(Tag: TTagRef);
begin
  Self.Tag:= Tag;
end;

procedure TLaserSerialNumber.Read(packet: TSPIPacket; Handler: TIdIOHandler);
begin
  Packet.Q[0] := Byte(0);
  Packet.EncodePacket(ord(LASER_SERIAL_NUMBER), 0);
  Packet.Write(Handler);
  Packet.Read(Handler);
  Packet.DecodePacket;
  Named.SetAsInteger(Tag, Packet.X[0] * $1000000 + Packet.X[1] * $10000 + Packet.X[2] * $100 + Packet.X[3]);
end;

{ TLaserTemperaturesMonitor }

constructor TLaserTemperaturesMonitor.Create(Tag, Tag2: TTagRef);
begin
  Self.Tag:= Tag;
  Self.Tag2:= Tag2;
end;

procedure TLaserTemperaturesMonitor.Read(packet: TSPIPacket; Handler: TIdIOHandler);
begin
  Packet.Q[0] := Byte(0);
  Packet.EncodePacket(ord(LASER_TEMPERATURES), 0);
  Packet.Write(Handler);
  Packet.Read(Handler);
  Packet.DecodePacket;
  Named.SetAsInteger(Tag, Packet.X[0] * $100 + Packet.X[1]);
  Named.SetAsInteger(Tag2, Packet.X[2] * $100 + Packet.X[3]);

end;

{ TOperatingHoursMonitor }

constructor TOperatingHoursMonitor.Create(Tag: TTagRef);
begin
  Self.Tag:= Tag;
end;

procedure TOperatingHoursMonitor.Read(packet: TSPIPacket; Handler: TIdIOHandler);
begin
  Packet.Q[0] := Byte(0);
  Packet.EncodePacket(ord(OPERATING_HOURS), 0);
  Packet.Write(Handler);
  Packet.Read(Handler);
  Packet.DecodePacket;
  Named.SetAsInteger(Tag, Packet.X[0] * $1000000 + Packet.X[1] * $10000 + Packet.X[2] * $100 + Packet.X[3]);
end;

{ TPowerSupplyVoltagesMonitor }

constructor TPowerSupplyVoltagesMonitor.Create(Tag, Tag2: TTagRef);
begin
  Self.Tag:= Tag;
  Self.Tag2:= Tag2;
end;

procedure TPowerSupplyVoltagesMonitor.Read(packet: TSPIPacket; Handler: TIdIOHandler);
begin
  Packet.Q[0] := Byte(0);
  Packet.EncodePacket(ord(POWER_SUPPLY_VOLTAGES), 0);
  Packet.Write(Handler);
  Packet.Read(Handler);
  Packet.DecodePacket;
  Named.SetAsInteger(Tag, Packet.X[0] * $100 + Packet.X[1]);
  Named.SetAsInteger(Tag2, Packet.X[2] * $100 + Packet.X[3]);
end;

{ TPulseBurstLengthMonitor }

constructor TPulseBurstLengthMonitor.Create(Tag: TTagRef);
begin
  Self.Tag:= Tag;
end;

procedure TPulseBurstLengthMonitor.Read(packet: TSPIPacket; Handler: TIdIOHandler);
begin
  Packet.Q[0] := Byte(0);
  Packet.EncodePacket(ord(PULSE_BURST_LENGTH), 0);
  Packet.Write(Handler);
  Packet.Read(Handler);
  Packet.DecodePacket;
  Named.SetAsInteger(Tag, Packet.X[0] * $1000000 + Packet.X[1] * $10000 + Packet.X[2] * $100 + Packet.X[3]);
end;

{ TPulseRateMonitor }

constructor TPulseRateMonitor.Create(Tag: TTagRef);
begin
  Self.Tag := Tag;
end;

procedure TPulseRateMonitor.Read(packet: TSPIPacket; Handler: TIdIOHandler);
begin
 Packet.Q[0] := Byte(0);
  Packet.EncodePacket(ord(PULSE_RATE), 0);
  Packet.Write(Handler);
  Packet.Read(Handler);
  Packet.DecodePacket;
  Named.SetAsInteger(Tag, Packet.X[0] * $1000000 + Packet.X[1] * $10000 + Packet.X[2] * $100 + Packet.X[3]);

end;

{ TPulseWaveformMonitor }

constructor TPulseWaveformMonitor.Create(Tag, Tag2: TTagRef);
begin
  Self.Tag:= Tag;
  Self.Tag2:= Tag2;
end;

procedure TPulseWaveformMonitor.Read(packet: TSPIPacket; Handler: TIdIOHandler);
begin
  Packet.Q[0] := Byte(0);
  Packet.EncodePacket(ord(PULSE_WAVEFORM), 0);
  Packet.Write(Handler);
  Packet.Read(Handler);
  Packet.DecodePacket;
  Named.SetAsInteger(Tag, Packet.X[1]);
  Named.SetAsInteger(Tag2, Packet.X[2] * $1000000 + Packet.X[3] * $10000 + Packet.X[4] * $100 + Packet.X[5]);
end;

{ TPumpDutyFactorMonitor }

constructor TPumpDutyFactorMonitor.Create(Tag: TTagRef);
begin
  Self.Tag:= Tag;
end;

procedure TPumpDutyFactorMonitor.Read(packet: TSPIPacket; Handler: TIdIOHandler);
begin
  Packet.Q[0] := Byte(0);
  Packet.EncodePacket(ord(PUMP_DUTY_FACTOR), 0);
  Packet.Write(Handler);
  Packet.Read(Handler);
  Packet.DecodePacket;
  Named.SetAsInteger(Tag, Packet.X[0] * $100 + Packet.X[1]);
end;

{ TStatusLinesAndAlamsMonitor }

constructor TStatusLinesAndAlamsMonitor.Create(Tag, Tag2: TTagRef);
begin
  Self.Tag:= Tag;
  Self.Tag2:= Tag2;
end;

procedure TStatusLinesAndAlamsMonitor.Read(packet: TSPIPacket; Handler: TIdIOHandler);
//var I : Integer;

begin
  Packet.Q[0] := Byte(0);
  Packet.EncodePacket(ord(STATUS_LINES_AND_ALARMS), 0);
  Packet.Write(Handler);
  Packet.Read(Handler);
  Packet.DecodePacket;
  Named.SetAsInteger(Tag, Packet.X[0] * $100 + Packet.X[1]);

  if Packet.PacketSize > 5 then begin
    Named.SetAsInteger(Tag2, Packet.X[2] * $100 + Packet.X[3]);
  end else begin
    Named.SetAsInteger(Tag2, 0);
  end;

//  Named.SetAsInteger(Tag2, Packet.X[2] * $100 + Packet.X[3]);

end;

end.
