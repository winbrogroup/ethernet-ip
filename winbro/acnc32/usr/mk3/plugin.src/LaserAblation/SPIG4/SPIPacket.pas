unit SPIPacket;

interface

uses SysUtils, Classes, idGlobal, IdIOHandler;

type
  TSPIPacket = class
  private
    InBuffer: TBytes;
    OutBuffer: TBytes;
    OutBufferSize: Integer;
    Response: Byte;
    Command: Byte;

    FPacketSize: Integer;

    function Validate(s: Integer): Boolean;
  public
    Q: array[0..255] of Byte;
    X: array[0..255] of Byte;

    constructor Create;
    procedure EncodePacket(Command: Byte; datalength: Integer);
    procedure DecodePacket;

    procedure Write(AIOHandler: TIdIOHandler);
    procedure Read(AIOHandler: TIdIOHandler);
    property PacketSize: Integer read FPacketSize;


    // Debug only
    procedure SetInputBuffer(B: array of Byte; L: Integer);

  end;

  TSPIInvalidData = class(Exception)
  end;

implementation

{ TSPIPacket }

procedure TSPIPacket.Write(AIOHandler: TIdIOHandler);
begin
  AIOHandler.Write(outBuffer);
end;

constructor TSPIPacket.Create;
begin
end;

procedure TSPIPacket.EncodePacket(Command: Byte; datalength: Integer);
var I: Integer;
    CS: Byte;
begin
  outBufferSize:= datalength + 6;
  SetLength(OutBuffer, outBufferSize);
  outbuffer[0]:= $1b; // Fixed start
  outbuffer[1]:= datalength + 2; // [0x09] + [command] + data.length

  outbuffer[2]:= $09; // Fixed byte
  outbuffer[3]:= command;
  Self.command:= command;

  for i := 0 to datalength + 1 do begin
      outbuffer[4 + i] := Q[i];
  end;
  outbuffer[4 + datalength] := $0d; // fixed termination

  cs := 0;
  for i:=0 to outBufferSize -2 do begin
      cs := cs + outbuffer[i];
  end;
  outbuffer[5 + datalength] := cs;   // checksum of bytes 0 - outBufferSize - 1
end;

function TSPIPacket.Validate(S: Integer): Boolean;
begin
  Result:= False;
  if S < 4 then
    Exit;

  if S >= inbuffer[1] + 4 then
    Result:=  True;
end;

procedure TSPIPacket.DecodePacket;
var C: Byte;
    I: Integer;
    CR: Byte;
begin
  if(inbuffer[0] <> $1b) then
      raise TSPIInvalidData.Create('Expected 0x1b');

  FPacketSize := inbuffer[1];
  if(inbuffer[2] <> $09) then
      raise TSPIInvalidData.Create('Expected 0x9');

  response := inbuffer[3];

  if(response <> 0) then
      raise TSPIInvalidData.Create('R value was not zero');

  c := inbuffer[4];
  if(c <> command) then
      raise TSPIInvalidData.Create(Format('Response (%d) does not match command (%d)', [ c, command]));


  for i:=0 to packetSize - 1 do begin
      X[i] := inbuffer[5+i];
  end;

  if(inbuffer[packetSize + 2] <> $0d) then
      raise TSPIInvalidData.Create('Expected 0x0d');

  cr := 0;
  for i:=0 to packetSize + 2 do begin
      cr := cr + inbuffer[i];
  end;

  if(cr <> inbuffer[packetSize + 3]) then
      raise TSPIInvalidData.Create(Format('Invalid checksum expected %d rxed %d', [cr, inbuffer[packetSize + 3]]));

end;

procedure TSPIPacket.Read(AIOHandler: TIdIOHandler);
//var insize: Integer;
begin
  SetLength(InBuffer, 0);
  while not validate(Length(InBuffer)) do begin
    AIOHandler.ReadBytes(InBuffer, 1, True);
  end;
end;

procedure TSPIPacket.SetInputBuffer(B: array of Byte; L: Integer);
var I: Integer;
begin
  SetLength(InBuffer, L);
  for I:= 0 to L + 1 do
    InBuffer[I]:= B[I];
end;

end.
