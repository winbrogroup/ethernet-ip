unit SPIMonitor;

interface

uses SPIPacket, IdIOHandler, IdTCPClient;

type
  TSPIMonitor = class
  public
    procedure Read(packet: TSPIPacket; AClient: TIdIOHandler); virtual; abstract;
  end;

implementation

end.
