{-----------------------------------------------------------------------------
' WAGO Kontakttechnik GmbH       |                                           |
' Hansastr. 27                   |  Technical Support                        |
' D-32423 Minden                 |                                           |
' Tel.: +49(0)571 / 887 - 0      |  Tel.: +49(0)571 / 887 - 555              |
' Fax.: +49(0)571 / 887 - 169    |  Fax.: +49(0)571 / 887 - 8555             |
' Mail: info@wago.com            |  Mail: support@wago.com                   |
' www : http://www.wago.com      |                                           |
'-----------------------------------------------------------------------------
'              Copyright (C) WAGO 2001 - All Rights Reserved                 |
'-----------------------------------------------------------------------------
'  Filename    : MBTObj.pas
'  Version     : 1.00.release
'  Date        : 28-04-2002
'-----------------------------------------------------------------------------
'  Description : Interface of MODBUS/TCP DLL for Delpi 5
'
'-----------------------------------------------------------------------------
'  Required    :
'-----------------------------------------------------------------------------
' |Date    |Who |Ver  |Changes
'-----------------------------------------------------------------------------
'  12.03.02 CM   1.00  Init
'-----------------------------------------------------------------------------}
unit MBTObj;

interface

uses
  Windows, Classes, SysUtils, Dialogs, MBT;

type
  TModBusFunction = ( mbfREADREGISTER, mbfWRITEREGISTER, mbfREADCOIL, mbfWRITECOIL);

  TByteBuffer  = array[0..250] of Byte;
  pTByteBuffer    = ^TByteBuffer;
  TWordBuffer  = array[0..125] of Word;
  TpWordBuffer    = ^TWordBuffer;
  TDWordBuffer = array[0..62]  of LongWord;
  TpDWordBuffer    = ^TDWordBuffer; 

  TDynBoolArray = array of Boolean;
  TDynByteArray = array of Byte;
  TDynWordArray = array of Word;
  TDynDWordArray= array of LongWord;


  TModBusConnection= class(TComponent) /////////////////////////////////////////////////
  private
    f_IP                 : String;
    f_Port               : Integer;
    f_Adress             : Integer;
    f_Quantity           : Integer;
    f_UseTCP             : Boolean;
    f_RequestTimeOut     : Integer;
    f_FunctionCode       : TModBusFunction;
    f_Connected          : Boolean;
    f_Handle             : THandle;
    f_ReadBuffer         : array[0..250] of Byte; //TByteBuffer;
    f_WriteBuffer        : array[0..250] of Byte; //TByteBuffer;

    function  Execute: Cardinal;
    procedure ClearWriteBuffer;
    function  DecodeBitInByte(Data: array of Byte; BitNr: Integer):Boolean;
    function  EncodeBitInByte(Value: Boolean; BitNr: Integer): Byte;

  public
    property IP                 : String  read f_IP write f_IP;
    property Port               : Integer read f_Port write f_Port;
    property UseTCP             : Boolean read f_UseTCP write f_UseTCP;
    property RequestTimeOut     : Integer read f_RequestTimeOut write f_RequestTimeOut;
    property FunctionCode       : TModBusFunction read f_FunctionCode;
    property Connected          : Boolean read f_Connected;

    constructor Create(AOwner:TComponent); override;
    destructor Destroy; override;

    function Connect: Cardinal;
    procedure DisConnect;

    function ReadCoils(Adress: Cardinal; var Value: Boolean): Cardinal;  overload;
    function ReadCoils(Adress, Count: Cardinal; var Data: TDynBoolArray): Cardinal; overload;

    function WriteCoils(Adress: Cardinal; Value: Boolean): Cardinal; overload;
    function WriteCoils(Adress, Count: Cardinal; Data: TDynBoolArray): Cardinal; overload;

    function ReadRegister(Adress: Cardinal; High: Boolean; var Value: Byte): Cardinal; overload;
    function ReadRegister(Adress: Cardinal; var Value: Word): Cardinal; overload;
    function ReadRegister(Adress: Cardinal; var Value: LongWord): Cardinal; overload;
    function ReadRegister(Adress, Count: Cardinal; var Data: TDynByteArray): Cardinal; overload;
    function ReadRegister(Adress, Count: Cardinal; var Data: TDynWordArray): Cardinal; overload;
    function ReadRegister(Adress, Count: Cardinal; var Data: TDynDWordArray): Cardinal; overload;

    function WriteRegister(Adress: Cardinal; Value: Word): Cardinal; overload;
    function WriteRegister(Adress: Cardinal; Value: LongWord): Cardinal; overload;
    function WriteRegister(Adress, Count: Cardinal; Data: TDynByteArray): Cardinal; overload;
    function WriteRegister(Adress, Count: Cardinal; Data: TDynWordArray): Cardinal; overload;
    function WriteRegister(Adress, Count: Cardinal; Data: TDynDWordArray): Cardinal; overload;

    function GetErrorMsg(ErrorCode: Cardinal): String;
  end;


implementation

{ TModBusConnection }   ///////////////////////////////////////////////////

constructor TModBusConnection.Create(AOwner: TComponent);
begin
  inherited;
  f_IP                 := '10.1.6.205';
  f_Port               := 502;
  f_UseTCP             := True;
  f_RequestTimeOut     := 1000;
  f_Connected          := False;
  f_Handle             := 0;
end;

destructor TModBusConnection.Destroy;
begin
  DisConnect;
  inherited;
end;

function TModBusConnection.Connect: Cardinal;
begin
  result := MBTInit;
  if result = 0 then begin
    result := MBTConnect(PChar(f_IP),
                         Word(f_Port),
                         f_UseTCP,
                         f_RequestTimeOut,
                         f_Handle);
    if result = 0 then begin
      f_Connected := True;
    end else begin
      DisConnect;
    end;
  end;
end;

procedure TModBusConnection.DisConnect;
begin
  f_Connected := False;
  if (f_Handle <> 0) then begin
    MBTDisconnect(f_Handle);
    f_Handle := 0;
    MBTExit;
  end;
end;

function TModBusConnection.Execute: Cardinal;
begin
  result := 0;
  if not Connected then begin
    result := $80072750;
  end else begin
    case f_FunctionCode of
      mbfREADREGISTER : begin
        result := MBTReadRegisters(f_Handle, MODBUSTCP_TABLE_OUTPUT_REGISTER,
                                   f_Adress, f_Quantity, @f_ReadBuffer, nil, 0);
      end;
      mbfWRITEREGISTER : begin
        result := MBTWriteRegisters(f_Handle, f_Adress, f_Quantity,
                                    @f_WriteBuffer, nil, 0);
      end;
      mbfREADCOIL : begin
        result := MBTReadCoils(f_Handle, MODBUSTCP_TABLE_OUTPUT_COIL,
                               f_Adress, f_Quantity, @f_ReadBuffer, nil, 0);
      end;
      mbfWRITECOIL : begin
        result := MBTWriteCoils(f_Handle, f_Adress, f_Quantity,
                                @f_WriteBuffer, nil, 0);
      end;
    end;
  end;
end;

function TModBusConnection.ReadCoils(Adress: Cardinal;
         var Value: Boolean): Cardinal;
begin
  f_Adress   := Adress;
  f_Quantity := 1;
  f_FunctionCode := mbfREADCOIL;
  result := Execute;
  if result = 0 then begin
    Value := Boolean(f_ReadBuffer[0]);
  end;
end;

function TModBusConnection.ReadCoils(Adress, Count: Cardinal;
         var Data: TDynBoolArray): Cardinal;
var
  i: Integer;
begin
  f_Adress   := Adress;
  f_Quantity := Count;
  f_FunctionCode := mbfREADCOIL;
  result := Execute;
  if result = 0 then begin
    for i:=0 to Length(Data)-1 do begin
      Data[i] := DecodeBitInByte(f_ReadBuffer, i);
    end;
  end;
end;

function TModBusConnection.WriteCoils(Adress: Cardinal; Value: Boolean): Cardinal;
begin
  ClearWriteBuffer;
  f_Adress   := Adress;
  f_Quantity := 1;
  f_FunctionCode := mbfWRITECOIL;
  if Value then begin
    f_WriteBuffer[0] := $FF;
  end else begin
    f_WriteBuffer[0] := $00;
  end;
  result := Execute;
end;

function TModBusConnection.WriteCoils(Adress, Count: Cardinal;
         Data: TDynBoolArray): Cardinal;
var
  i: Integer;
begin
  ClearWriteBuffer;
  f_Adress   := Adress;
  f_Quantity := Count;
  f_FunctionCode := mbfWRITECOIL;
  for i:=0 to Length(Data)-1 do begin
    f_WriteBuffer[i div 8] := f_WriteBuffer[i div 8] xor EncodeBitInByte(Data[i], i);
  end;
  result := Execute;
end;

function TModBusConnection.ReadRegister(Adress: Cardinal; High: Boolean;
         var Value: Byte): Cardinal;
begin
  f_Adress   := Adress;
  f_Quantity := 1;
  f_FunctionCode := mbfREADREGISTER;
  result := Execute;
  if result = 0 then begin
    if High then begin
      Value := f_ReadBuffer[1];
    end else begin
      Value := f_ReadBuffer[0];
    end;
  end;
end;

function TModBusConnection.ReadRegister(Adress: Cardinal;
         var Value: Word): Cardinal;
var
  PointerToWordBuffer: TpWordBuffer;
begin
  f_Adress   := Adress;
  f_Quantity := 1;
  f_FunctionCode := mbfREADREGISTER;
  result := Execute;
  if result = 0 then begin
    PointerToWordBuffer := @f_ReadBuffer;
    Value := MBTSwapWord(PointerToWordBuffer[0]);
  end;
end;

function TModBusConnection.ReadRegister(Adress: Cardinal;
         var Value: LongWord): Cardinal;
var
  PointerToDWordBuffer: TpDWordBuffer;
begin
  f_Adress   := Adress;
  f_Quantity := 2;
  f_FunctionCode := mbfREADREGISTER;
  result := Execute;
  if result = 0 then begin
    PointerToDWordBuffer := @f_ReadBuffer;
    Value := MBTSwapDWord(PointerToDWordBuffer[0]);
  end;
end;

function TModBusConnection.ReadRegister(Adress, Count: Cardinal;
         var Data: TDynByteArray): Cardinal;
var
  i: Integer;
begin
  f_Adress   := Adress;
  f_Quantity := (Count div 2) + (Count mod 2);
  f_FunctionCode := mbfREADREGISTER;
  result := Execute;
  if result = 0 then begin
    for i:=0 to Length(Data) -1 do begin
      Data[i] := f_ReadBuffer[i];
    end;
  end;
end;

function TModBusConnection.ReadRegister(Adress, Count: Cardinal;
         var Data: TDynWordArray): Cardinal;
var
  i: Integer;
  PointerToWordBuffer: TpWordBuffer;
begin
  f_Adress   := Adress;
  f_Quantity := Count;
  f_FunctionCode := mbfREADREGISTER;
  result := Execute;
  if result = 0 then begin
    PointerToWordBuffer := @f_ReadBuffer;
    for i:=0 to Length(Data)-1 do begin
      Data[i] := MBTSwapWord(PointerToWordBuffer[i]);
    end;
  end;
end;

function TModBusConnection.ReadRegister(Adress, Count: Cardinal;
         var Data: TDynDWordArray): Cardinal;
var
  i: Integer;
  PointerToDWordBuffer: TpDWordBuffer;
begin
  f_Adress   := Adress;
  f_Quantity := Count * 2;
  f_FunctionCode := mbfREADREGISTER;
  result := Execute;
  if result = 0 then begin
    PointerToDWordBuffer := @f_ReadBuffer;
    for i:=0 to Length(Data) -1 do begin
      Data[i] := MBTSwapDWord(PointerToDWordBuffer[i]);
    end;
  end;
end;

function TModBusConnection.WriteRegister(Adress: Cardinal; Value: Word): Cardinal;
var
  help : Word;
begin
  ClearWriteBuffer;
  f_Adress   := Adress;
  f_Quantity := 1;
  f_FunctionCode := mbfWRITEREGISTER;
  help := MBTSwapWord(Value);
  Move(help, f_WriteBuffer, SizeOf(help));
  result := Execute;
end;

function TModBusConnection.WriteRegister(Adress: Cardinal; Value: LongWord): Cardinal;
var
  help : LongWord;
begin
  ClearWriteBuffer;
  f_Adress   := Adress;
  f_Quantity := 2;
  f_FunctionCode := mbfWRITEREGISTER;
  help := MBTSwapDWord(Value);
  Move(help, f_WriteBuffer, SizeOf(help));
  result := Execute;
end;

function TModBusConnection.WriteRegister(Adress, Count: Cardinal;
         Data: TDynByteArray): Cardinal;
var
  i: Integer;
begin
  ClearWriteBuffer;
  f_Adress   := Adress;
  f_Quantity := (Count div 2) + (Count mod 2);
  f_FunctionCode := mbfWRITEREGISTER;
  for i:=0 to Length(Data)-1 do begin
    f_WriteBuffer[i] := Data[i];
  end;
  result := Execute;
end;

function TModBusConnection.WriteRegister(Adress, Count: Cardinal;
         Data: TDynWordArray): Cardinal;
var
  i: Integer;
  PointerToWordBuffer: TpWordBuffer;
begin
  ClearWriteBuffer;
  f_Adress   := Adress;
  f_Quantity := Count;
  f_FunctionCode := mbfWRITEREGISTER;
  PointerToWordBuffer := @f_WriteBuffer;
  for i:=0 to Length(Data) -1 do begin
    PointerToWordBuffer[i] := MBTSwapWord(Data[i]);
  end;
  result := Execute;
end;

function TModBusConnection.WriteRegister(Adress, Count: Cardinal;
         Data: TDynDWordArray): Cardinal;
var
  i: Integer;
  PointerToDWordBuffer: TpDWordBuffer;
begin
  ClearWriteBuffer;
  f_Adress   := Adress;
  f_Quantity := Count *2;
  f_FunctionCode := mbfWRITEREGISTER;
  PointerToDWordBuffer := @f_WriteBuffer;
  for i:=0 to Length(Data) -1 do begin
    PointerToDWordBuffer[i] := MBTSwapDWord(Data[i]);
  end;
  result := Execute;
end;

function TModBusConnection.DecodeBitInByte(Data: Array of Byte; BitNr: Integer): Boolean;
var
  Byte, Bit : Integer;
begin
  result := False;
  Byte := BitNr div 8;
  Bit  := BitNr mod 8;
  case Bit of
    0 :  result := (Data[Byte] and $01) = $01;
    1 :  result := (Data[Byte] and $02) = $02;
    2 :  result := (Data[Byte] and $04) = $04;
    3 :  result := (Data[Byte] and $08) = $08;
    4 :  result := (Data[Byte] and $10) = $10;
    5 :  result := (Data[Byte] and $20) = $20;
    6 :  result := (Data[Byte] and $40) = $40;
    7 :  result := (Data[Byte] and $80) = $80;
  end;
end;

function TModBusConnection.EncodeBitInByte(Value: Boolean; BitNr: Integer): Byte;
var
  Bit : Integer;
begin
  result := $00;
  Bit  := BitNr mod 8;
  case Bit of
    0 :  if Value then result := $01;
    1 :  if Value then result := $02;
    2 :  if Value then result := $04;
    3 :  if Value then result := $08;
    4 :  if Value then result := $10;
    5 :  if Value then result := $20;
    6 :  if Value then result := $40;
    7 :  if Value then result := $80;
  end;
end;

function TModBusConnection.GetErrorMsg(ErrorCode: Cardinal): String;
begin
  result := 'Unknown Error';
  case ErrorCode of
    $80072750                  : result := 'Not connected to device';
    $80072751                  : result := 'Can''t connect to Device'; //if Connect failed
    MBT_ERROR_PREFIX + $00     : result := 'MBT_THREAD_CREATION_ERROR';
    MBT_ERROR_PREFIX + $01     : result := 'MBT_EXIT_TIMEOUT_ERROR';
    MBT_ERROR_PREFIX + $02     : result := 'MBT_UNKNOWN_THREAD_EXIT_ERROR';
    MBT_ERROR_PREFIX + $03     : result := 'MBT_UNAVAILABLE_CLOCK_ERROR';
    MBT_ERROR_PREFIX + $04     : result := 'MBT_NO_ENTRY_ADDABLE_ERROR';
    MBT_ERROR_PREFIX + $05     : result := 'MBT_NO_JOB_ADDABLE_ERROR';
    MBT_ERROR_PREFIX + $06     : result := 'MBT_HANDLE_INVALID_ERROR';
    MBT_ERROR_PREFIX + $07     : result := 'MBT_CLOSE_FLAG_SET_ERROR';
    MBT_ERROR_PREFIX + $08     : result := 'MBT_SOCKET_TIMEOUT_ERROR';
    MBT_ERROR_PREFIX + $09     : result := 'MBT_WRONG_RESPONSE_FC_ERROR';
    MBT_ERROR_PREFIX + $0A     : result := 'MBT_RESPONSE_FALSE_LENGTH_ERROR';
    MBT_ERROR_PREFIX + $0B     : result := 'MBT_EXIT_ERROR';
    MBT_EXCEPTION_PREFIX + $01 : result := 'MBT_ILLEGAL_FUNCTION';
    MBT_EXCEPTION_PREFIX + $02 : result := 'MBT_ILLEGAL_DATA_ADDRESS';
    MBT_EXCEPTION_PREFIX + $03 : result := 'MBT_ILLEGAL_DATA_VALUE';
    MBT_EXCEPTION_PREFIX + $04 : result := 'MBT_ILLEGAL_RESPONSE_LENGTH';
    MBT_EXCEPTION_PREFIX + $05 : result := 'MBT_ACKNOWLEDGE';
    MBT_EXCEPTION_PREFIX + $06 : result := 'MBT_SLAVE_DEVICE_BUSY';
    MBT_EXCEPTION_PREFIX + $07 : result := 'MBT_NEGATIVE_ACKNOWLEDGE';
    MBT_EXCEPTION_PREFIX + $08 : result := 'MBT_MEMORY_PARITY_ERROR';
    MBT_EXCEPTION_PREFIX + $0A : result := 'MBT_GATEWAY_PATH_UNAVAILABLE';
    MBT_EXCEPTION_PREFIX + $0B : result := 'MBT_GATEWAY_TARGET_DEVICE_FAILED_TO_RESPOND';
  end;
end;

procedure TModBusConnection.ClearWriteBuffer;
var
  i: Integer;
begin
  for i:=0 to Length(f_WriteBuffer)-1 do begin
    f_WriteBuffer[i] := $00;
  end;
end;

end.
