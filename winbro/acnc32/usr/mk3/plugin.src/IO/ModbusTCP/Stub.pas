unit Stub;

interface

uses Windows, Classes, INamedInterface, CNCTypes, SysUtils, Sequencer, SyncObjs,
     AbstractIOSystemAccessor, NamedPlugin, ExtCtrls;

type
  TModbusRunThread = class;
  TModbusTCP = class(TAbstractIOSystemAccessor)
  private
    FTextDef: string;
    FInputSize: Integer;
    FOutputSize: Integer;
    IP: string;
    Host: IIOSystemAccessorHost;
    Thread: TModbusRunThread;

    FInputArea: array [0..511] of Word;
    FOutputArea: array [0..511] of Word;
  public
    constructor Create; override;
    function GetInfoPageContent(aPage : Integer) : WideString; override;
    procedure Initialise(Host: IIOSystemAccessorHost; const Params : WideString); override;
    procedure UpdateShadowToReal; override;
    procedure UpdateRealToShadow; override;
    function Healthy : Boolean; override;
    procedure Reset; override;
    function LastError : WideString; override;
    function GetInputArea: Pointer; override;
    function GetOutputArea: Pointer; override;
    function GetInputSize: Integer; override;
    function GetOutputSize: Integer; override;
    function TextDefinition: WideString; override;
    procedure Shutdown; override;

    {procedure ReadRegister(Address: Integer);
    procedure WriteRegister(Address: Integer; Value: Integer);}
    function GetRegisterValue(Address: Integer): Integer;

    class function GetModbusTCPIOAccessor(IP: string): TModbusTCP;
  end;

  TModbusTCPRegisters = class(TAbstractIOSystemAccessor)
  private
    FTextDef: string;
    FInputSize: Integer;
    FOutputSize: Integer;
    IP: string;
    Modbus: TModbusTCP;
    FInputArea: array [0..63] of Integer;
    FOutputArea: array [0..63] of Integer;
    FUpdateRate: Integer;
  public
    constructor Create; override;
    function GetInfoPageContent(aPage : Integer) : WideString; override;
    procedure Initialise(Host: IIOSystemAccessorHost; const Params : WideString); override;
    procedure UpdateShadowToReal; override;
    procedure UpdateRealToShadow; override;
    function Healthy : Boolean; override;
    procedure Reset; override;
    function LastError : WideString; override;
    function GetInputArea: Pointer; override;
    function GetOutputArea: Pointer; override;
    function GetInputSize: Integer; override;
    function GetOutputSize: Integer; override;
    function TextDefinition: WideString; override;
    procedure Shutdown; override;
  end;

  TModbusRunThread = class(TThread)
  private
    Sequencer: TModbusSequencer;
    Accessor: TModbusTCP;
    FInReset: Boolean;
    Lock: TCriticalSection;
    InData: array[0..511] of Byte;
    OutData: array[0..511] of Byte;
    RunTag: TTagRef;
    RunningCount: Integer;
  public
    constructor Create(S: TModbusTCP; InSize, OutSize: Integer; const IP: string);
    procedure Execute; override;
    property InReset: Boolean read FInReset write FInReset;
  end;

implementation

var
  ModbusTCPIOAccessors: TStringList;
{ TFLIBS }

function TModbusTCP.GetOutputSize: Integer;
begin
  Result:= FOutputSize;
end;

function TModbusTCP.GetInfoPageContent(aPage: Integer): WideString;
begin
  Result := '';

  //Named.EventLog('We are asking for page: ' + IntToStr(aPage));
  case aPage of
    0 : begin
      Result := 'Update' + CarRet + IntToStr(Thread.Sequencer.UpdateCount);
      {
        'Defective Cycles' + CarRet + IntToStr(Thread.FLibsSequencer.CycleErrorCount) + CarRet +
        'Cycles' + CarRet + IntToStr(Thread.FLibsSequencer.CycleCount) + CarRet +
        'ID Cycle Count' + CarRet + IntToStr(Thread.FLibsSequencer.IDCycleCount) + CarRet +
        'ID Cycle Error Count' + CarRet + IntToStr(Thread.FLibsSequencer.IDCycleErrorCount) + CarRet +
        'Data Cycle Count' + CarRet + IntToStr(Thread.FLibsSequencer.DataCycleCount) + CarRet +
        'Data Cycle Error Count' + CarRet + IntToStr(Thread.FLibsSequencer.DataCycleErrorCount);
        }
    end;
  end;
end;

function TModbusTCP.GetOutputArea: Pointer;
begin
  Result:= @FOutputArea;
end;

procedure TModbusTCP.Shutdown;
begin
  Thread.Terminate;
  Thread.WaitFor;
end;

function TModbusTCP.GetInputSize: Integer;
begin
  Result:= FInputSize;
end;

procedure TModbusTCP.Reset;
begin
  if not Healthy then
    Thread.InReset:= True;
end;

function TModbusTCP.GetInputArea: Pointer;
begin
  Result:= @FInputArea;
end;

function TModbusTCP.TextDefinition: WideString;
begin
  Result:= FTextDef;
end;

function TModbusTCP.Healthy: Boolean;
begin
  Result:= Thread.Sequencer.State = eiosRunning;
end;

function TModbusTCP.LastError: WideString;
begin
  Result:= '';
end;

procedure TModbusTCP.Initialise(Host: IIOSystemAccessorHost; const Params: WideString);
var Tmp: string;
begin
  FTextDef:= Params;
  Tmp:= Params;
  Self.Host:= Host;
  FInputSize:= StrToInt(CncTypes.ReadDelimited(Tmp, ';'));
  FOutputSize:= StrToInt(CncTypes.ReadDelimited(Tmp, ';'));
  IP:= CncTypes.ReadDelimited(Tmp, ';');
  FInputSize:= CncTypes.RangeInt(FInputSize, 2, 512);
  FOutputSize:= CncTypes.RangeInt(FOutputSize, 2, 512);


  Thread:= TModbusRunThread.Create(Self, FInputSize, FOutputSize, IP);
  Thread.RunTag:= Named.AddTag('ModbusRefresh', 'TIntegerTag');

  Host.AddPage('State');

  Thread.Resume;
end;

procedure TModbusTCP.UpdateRealToShadow;
begin
  Thread.Lock.Acquire;
  try
    CopyMemory(@FInputArea, @Thread.InData, FInputSize);
  finally
    Thread.Lock.Release;
  end;
end;

procedure TModbusTCP.UpdateShadowToReal;
begin
  Thread.Lock.Acquire;
  try
    CopyMemory(@Thread.OutData, @FOutputArea, FOutputSize);
  finally
    Thread.Lock.Release;
  end;
end;

var Blog: Integer;
constructor TModbusTCP.Create;
begin
  inherited;
  Inc(Blog);
end;

{ TFLIBRunThread }

constructor TModbusRunThread.Create(S: TModbusTCP; InSize, OutSize: Integer; const IP: string);
begin
  inherited Create(True);
  Accessor:= S;
  Sequencer:= TModbusSequencer.Create;
  Sequencer.SetIP(IP);
  ModbusTCPIOAccessors.AddObject(IP, S);
  Sequencer.InputSize:= InSize;
  Sequencer.OutputSize:= OutSize;
  Lock:= TCriticalSection.Create;
end;

var SweepTime, Tmp: Integer;
procedure TModbusRunThread.Execute;
begin
  Self.Priority:= tpHighest;
  while not Terminated do begin

    // Reset state machine
    if InReset then begin
      try
        case Sequencer.State of
          eiosNoHandles: begin
            Sequencer.OpenHandles;
          end;
          eiosHandles: begin
            Sequencer.ConfigureModbus;
          end;
          eiosConfigured: begin
            Sequencer.RunModbus;
          end;
          eiosRunning: begin
            InReset:= False;
          end;
        end;
      except
        InReset:= False;
      end;
    end;

    Tmp:= Windows.GetTickCount;
    if Sequencer.State = eiosRunning then begin
      Sequencer.Sweep;
      Lock.Acquire;
      try
        CopyMemory(@InData, @Sequencer.InData, Sequencer.InputSize);
        CopyMemory(@Sequencer.OutData, @OutData, Sequencer.OutputSize);
        Named.SetAsInteger(RunTag, Sequencer.UpdateCount);
      finally
        Lock.Release;
      end;
    end;

    Sleep(1);
    SweepTime:= Windows.GetTickCount - Tmp;
  end;
  try
    Sequencer.StopModbus;
  except
  end;
  Sequencer.Destroy;
end;

class function TModbusTCP.GetModbusTCPIOAccessor(IP: string): TModbusTCP;
var I: Integer;
begin
  Result:= nil;
  for I:= 0 to ModbusTCPIOAccessors.Count - 1 do begin
    if IP = ModbusTCPIOAccessors[I] then begin
      Result:= TModbusTCP(ModbusTCPIOAccessors.Objects[I]);
    end;
  end;
end;

{procedure TModbusTCP.ReadRegister(Address: Integer);
begin
  Thread.Sequencer.ReadRegister[Address]:= True;
end;

procedure TModbusTCP.WriteRegister(Address, Value: Integer);
begin
  Thread.Sequencer.OutRegister[Address]:= Value;
  Thread.Sequencer.WriteRegister[Address]:= True;
end; }

function TModbusTCP.GetRegisterValue(Address: Integer): Integer;
begin
  Result:= Thread.Sequencer.InRegister[Address];
end;

{ TModbusTCPRegisters }

function TModbusTCPRegisters.GetOutputSize: Integer;
begin
  Result:= Self.FOutputSize;
end;

function TModbusTCPRegisters.GetInfoPageContent(aPage: Integer): WideString;
begin
  Result:= 'Update rate' + CarRet + '?';
end;

constructor TModbusTCPRegisters.Create;
begin
  inherited;
end;

function TModbusTCPRegisters.GetOutputArea: Pointer;
begin
  Result:= @FOutputArea;
end;

procedure TModbusTCPRegisters.Shutdown;
begin
  inherited;
end;

function TModbusTCPRegisters.GetInputSize: Integer;
begin
  Result:= Self.FInputSize;
end;

procedure TModbusTCPRegisters.Reset;
begin
end;

function TModbusTCPRegisters.GetInputArea: Pointer;
begin
  Result:= @FInputArea;
end;

function TModbusTCPRegisters.TextDefinition: WideString;
begin
  Result:= Self.FTextDef;
end;

function TModbusTCPRegisters.Healthy: Boolean;
begin
  Result:= Modbus.Healthy;
end;

function TModbusTCPRegisters.LastError: WideString;
begin
  Result:= Modbus.LastError;
end;

procedure TModbusTCPRegisters.Initialise(Host: IIOSystemAccessorHost; const Params: WideString);
var Tmp: string;
begin
  FTextDef:= Params;
  Tmp:= Params;
  FInputSize:= StrToInt(CncTypes.ReadDelimited(Tmp, ';'));
  FOutputSize:= StrToInt(CncTypes.ReadDelimited(Tmp, ';'));
  IP:= CncTypes.ReadDelimited(Tmp, ';');
  FUpdateRate:= StrToIntDef(ReadDelimited(Tmp, ';'), 5);
  FInputSize:= CncTypes.RangeInt(FInputSize, 2, 127);
  FOutputSize:= CncTypes.RangeInt(FOutputSize, 2, 127);
  FUpdateRate:= CncTypes.RangeInt(FUpdateRate, 2, 20);
  Modbus:= TModbusTCP.GetModbusTCPIOAccessor(IP);
  Modbus.Thread.Sequencer.SetInRegisterSize(FInputsize div 2);
  Modbus.Thread.Sequencer.SetOutRegisterSize(Foutputsize div 2);
  Modbus.Thread.Sequencer.SetRegisterUpdateInterval(FUpdateRate);

  Host.AddPage('State');
end;

procedure TModbusTCPRegisters.UpdateRealToShadow;
begin
  CopyMemory(@FInputArea, @Modbus.Thread.Sequencer.InRegister, Self.FInputSize * 2);
end;

procedure TModbusTCPRegisters.UpdateShadowToReal;
begin
  CopyMemory(@Modbus.Thread.Sequencer.OutRegister, @FOutputArea, Self.FOutputSize * 2);
end;

initialization
  ModbusTCPIOAccessors:= TStringList.Create;
  TModbusTCP.AddIOSystemAccessor('ModbusTCP', TModbusTCP);
  TModbusTCP.AddIOSystemAccessor('ModbusTCPRegisters', TModbusTCPRegisters);
end.
