object frmMain: TfrmMain
  Left = 289
  Top = 109
  Width = 291
  Height = 374
  Caption = 'Kommunikation mit TCP-Controller'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 8
    Top = 8
    Width = 265
    Height = 61
    Caption = ' settings '
    TabOrder = 0
    object Label1: TLabel
      Left = 12
      Top = 20
      Width = 45
      Height = 13
      Caption = 'ip-adress:'
    end
    object adresslabel: TLabel
      Left = 60
      Top = 20
      Width = 93
      Height = 13
      AutoSize = False
      Caption = 'adresslabel'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label3: TLabel
      Left = 171
      Top = 20
      Width = 21
      Height = 13
      Caption = 'port:'
    end
    object portlabel: TLabel
      Left = 195
      Top = 20
      Width = 57
      Height = 13
      AutoSize = False
      Caption = 'portlabel'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label5: TLabel
      Left = 16
      Top = 36
      Width = 41
      Height = 13
      Caption = 'protocol:'
    end
    object protocollabel: TLabel
      Left = 60
      Top = 36
      Width = 93
      Height = 13
      AutoSize = False
      Caption = 'protocollabel'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label7: TLabel
      Left = 155
      Top = 36
      Width = 37
      Height = 13
      Caption = 'timeout:'
    end
    object timeoutlabel: TLabel
      Left = 195
      Top = 36
      Width = 56
      Height = 13
      AutoSize = False
      Caption = 'timeoutlabel'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
  end
  object GroupBox2: TGroupBox
    Left = 8
    Top = 80
    Width = 265
    Height = 105
    Caption = ' coils '
    TabOrder = 1
    object Bevel1: TBevel
      Left = 36
      Top = 16
      Width = 65
      Height = 21
    end
    object Label2: TLabel
      Left = 52
      Top = 20
      Width = 31
      Height = 13
      Caption = 'adress'
    end
    object CoilAdress: TEdit
      Left = 112
      Top = 16
      Width = 60
      Height = 21
      TabOrder = 0
    end
    object CoilRead: TEdit
      Left = 112
      Top = 44
      Width = 120
      Height = 21
      ReadOnly = True
      TabOrder = 1
    end
    object CoilWrite: TEdit
      Left = 112
      Top = 72
      Width = 120
      Height = 21
      TabOrder = 2
    end
    object Button1: TButton
      Left = 36
      Top = 44
      Width = 65
      Height = 21
      Caption = 'read'
      TabOrder = 3
      OnClick = CoilBtnClick
    end
    object Button2: TButton
      Tag = 1
      Left = 36
      Top = 72
      Width = 65
      Height = 21
      Caption = 'write'
      TabOrder = 4
      OnClick = CoilBtnClick
    end
  end
  object GroupBox3: TGroupBox
    Left = 8
    Top = 196
    Width = 265
    Height = 105
    Caption = ' registers '
    TabOrder = 2
    object Bevel2: TBevel
      Left = 36
      Top = 16
      Width = 65
      Height = 21
    end
    object Label4: TLabel
      Left = 52
      Top = 20
      Width = 31
      Height = 13
      Caption = 'adress'
    end
    object RegisterAdress: TEdit
      Left = 112
      Top = 16
      Width = 60
      Height = 21
      TabOrder = 0
    end
    object RegisterRead: TEdit
      Left = 112
      Top = 44
      Width = 120
      Height = 21
      ReadOnly = True
      TabOrder = 1
    end
    object RegisterWrite: TEdit
      Left = 112
      Top = 72
      Width = 120
      Height = 21
      TabOrder = 2
    end
    object Button3: TButton
      Left = 36
      Top = 44
      Width = 65
      Height = 21
      Caption = 'read'
      TabOrder = 3
      OnClick = RegisterBtnClick
    end
    object Button4: TButton
      Tag = 1
      Left = 36
      Top = 72
      Width = 65
      Height = 21
      Caption = 'write'
      TabOrder = 4
      OnClick = RegisterBtnClick
    end
  end
  object CloseBtn: TBitBtn
    Left = 100
    Top = 312
    Width = 75
    Height = 25
    TabOrder = 3
    OnClick = CloseBtnClick
    Kind = bkClose
  end
end
