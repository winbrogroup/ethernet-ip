{-------------------------------------------------------------------------------
               TBB (Technisches B�ro Blankenhagen)
                         Sachsenweg 8
                        26209 Hatten
                   Tel. +49 (0)4482 9276-10
                   Fax. +49 (0)4482 9276-29
                email blankenhagen@tbb-hatten.de

                   Copyright (C) TBB from 2002
                      All Rights Reserved
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
                     OPC Project - WAGO MODBUS/TCP

 Filename    : main.pas
 Autor       : Karl-Heinz Blankenhagen
 Version     : 1.00.release
 Date        : 22.02.2002

 Description : Sample program for the call of the MODBUS/TCP DLL (beta release)

-------------------------------------------------------------------------------}
unit Main;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls;

type
  TfrmMain = class(TForm)
    GroupBox1: TGroupBox;
    Label1: TLabel;
    adresslabel: TLabel;
    Label3: TLabel;
    portlabel: TLabel;
    Label5: TLabel;
    protocollabel: TLabel;
    Label7: TLabel;
    timeoutlabel: TLabel;
    GroupBox2: TGroupBox;
    CoilAdress: TEdit;
    CoilRead: TEdit;
    CoilWrite: TEdit;
    Button1: TButton;
    Button2: TButton;
    Label2: TLabel;
    Bevel1: TBevel;
    GroupBox3: TGroupBox;
    Bevel2: TBevel;
    Label4: TLabel;
    RegisterAdress: TEdit;
    RegisterRead: TEdit;
    RegisterWrite: TEdit;
    Button3: TButton;
    Button4: TButton;
    CloseBtn: TBitBtn;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure CloseBtnClick(Sender: TObject);
    procedure CoilBtnClick(Sender: TObject);
    procedure RegisterBtnClick(Sender: TObject);
  private
    hSocket : THandle;
  public
    { Public-Deklarationen }
  end;

var
  frmMain: TfrmMain;

implementation

{$R *.dfm}

uses
  MBT;

const
  // settings of the tcp-controller
  DEFAULT_SERVER_NAME     : array[0..15] of Char = '192.168.1.4'; // ip-adress
  DEFAULT_PORT            : Word = 502;       // modbusport
  
  DEFAULT_PROTOCOL        : Boolean = True;   // True=tcp, False=udp
  DEFAULT_REQUEST_TIMEOUT : LongWord = 1000;  // in ms

var
  // the read/write buffers
  ReadBuffer  : array[0..250] of Byte;
  WriteBuffer : array[0..250] of Byte;


{-------------------------------------------------------------------------------
 on create we try to etablish a connection to the tcp-controller
-------------------------------------------------------------------------------}
procedure TfrmMain.FormCreate(Sender: TObject);
begin
  ConnectDll;
  // you can hand over the ip-adress as parameter when you start the application
  if (ParamCount > 0) then StrPCopy(DEFAULT_SERVER_NAME, ParamStr(1));
  // init the modbus-dll
  MBTInit;
  // if the connection could not be etablished then...
  if (MBTConnect(DEFAULT_SERVER_NAME, DEFAULT_PORT, DEFAULT_PROTOCOL,
                 DEFAULT_REQUEST_TIMEOUT, hSocket) <> S_OK) then begin
    // ...cancel everything...
    MBTExit;
    // ...and display an appropiate message
    MessageBeep(MB_ICONHAND);
    Application.MessageBox('No connection to wago tcp', 'socket error', MB_OK);
  end;
  // display the settings on the form
  adresslabel.Caption := StrPas(DEFAULT_SERVER_NAME);
  portlabel.Caption := IntToStr(DEFAULT_PORT);
  if DEFAULT_PROTOCOL then protocollabel.Caption := 'tcp' else protocollabel.Caption := 'udp';
  timeoutlabel.Caption := IntToStr(DEFAULT_REQUEST_TIMEOUT)+'ms';
end;

{-------------------------------------------------------------------------------
 on destroy we finish the connection to the tcp-controller
-------------------------------------------------------------------------------}
procedure TfrmMain.FormDestroy(Sender: TObject);
begin
  if (hSocket <> 0) then begin
    MBTDisconnect(hSocket);
    MBTExit;
  end;
end;

{-------------------------------------------------------------------------------
 close the application
-------------------------------------------------------------------------------}
procedure TfrmMain.CloseBtnClick(Sender: TObject);
begin
  Close;
end;

{-------------------------------------------------------------------------------
 read/write coils
-------------------------------------------------------------------------------}
procedure TfrmMain.CoilBtnClick(Sender: TObject);
var
  Adr : Word;
begin
  // the adress we want to write to or read from
  Adr := StrToInt(CoilAdress.Text);
  case (Sender as TButton).Tag of
    0: begin  // try to read coils from controller
      if (MBTReadCoils(hSocket, MODBUSTCP_TABLE_OUTPUT_COIL,
                       Adr, 1, @ReadBuffer, nil, 0) = S_OK) then begin
        // ...and display it if read succeeds
        CoilRead.Text := IntToStr(ReadBuffer[0]);
      end else begin
        // display a message if read fails
        MessageBeep(MB_ICONHAND);
        Application.MessageBox('read coils failed !', 'read error', MB_OK);
      end;
    end;
    1: begin // try to write coils
      // the value we want to write
      WriteBuffer[0] := StrToInt(CoilWrite.Text);
      WriteBuffer[1] := StrToInt(CoilWrite.Text);
      if (MBTWriteCoils(hSocket, Adr, 16, @WriteBuffer, nil, 0) <> S_OK) then begin
        // display a message if write fails
        MessageBeep(MB_ICONHAND);
        Application.MessageBox('write coils failed !', 'write error', MB_OK);
      end;
    end;
  end;
end;

{-------------------------------------------------------------------------------
 read/write registers
-------------------------------------------------------------------------------}
procedure TfrmMain.RegisterBtnClick(Sender: TObject);
var
  Adr   : Word;
  Value : Word;
begin
  // the adress we want to write to or read from
  Adr := StrToInt(RegisterAdress.Text);
  case (Sender as TButton).Tag of
    0: begin // try to read register from controller
      if (MBTReadRegisters(hSocket, MODBUSTCP_TABLE_OUTPUT_REGISTER,
                           Adr, 1, @ReadBuffer, nil, 0) = S_OK) then begin
        // if read was successfull get the readvalue...
        Move(ReadBuffer, Value, SizeOf(Value));
        // ...swap the bytes...
        Value := MBTSwapWord(Value);
        // ...and display it
        RegisterRead.Text := IntToStr(Value);
      end else begin
        // display a message if read fails
        MessageBeep(MB_ICONHAND);
        Application.MessageBox('read registers failed !', 'read error', MB_OK);
      end;
    end;
    1: begin // try to write registers
      // the value we want to write
      Value := MBTSwapWord(StrToInt(RegisterWrite.Text));
      // move it into the writebuffer
      Move(Value, WriteBuffer, SizeOf(Value));
      if (MBTWriteRegisters(hSocket, Adr, 1, @WriteBuffer, nil, 0) <> S_OK) then begin
        // display a message if write fails
        MessageBeep(MB_ICONHAND);
        Application.MessageBox('write registers failed !', 'write error', MB_OK);
      end;
    end;
  end;
end;

end.
