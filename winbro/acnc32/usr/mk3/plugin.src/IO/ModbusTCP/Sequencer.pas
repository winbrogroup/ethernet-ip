unit Sequencer;

interface

uses Sysutils, Classes, MBT;

type
  TModbusState = (
    eiosNoHandles,
    eiosHandles,
    eiosConfigured,
    eiosRunning
  );

  TModbusSequencer = class
  private
    FState: TModbusState;
    IP: string;
    FOutputSize: Integer;
    FInputSize: Integer;
    hSocket : THandle;
    InRegisterSize: Integer;
    OutRegisterSize: Integer;
    RegisterUpdateInterval: Integer;
    procedure WriteOutputs;
    procedure ReadInputs;
  public
    indata: array[0..255] of Byte;
    outdata: array[0..255] of Byte;
    OutRegister: array[0..63] of Word;
    InRegister: array[0..53] of Word;
    UpdateCount: Integer;
    constructor Create;
    destructor Destroy; override;
    procedure SetIP(IP: string);
    procedure OpenHandles;
    procedure ConfigureModbus;
    procedure SetState(AState: TModbusState);
    procedure Sweep;
    procedure StopModbus;
    property State: TModbusState read FState;
    property InputSize: Integer read FInputSize write FInputSize;
    property OutputSize: Integer read FOutputSize write FOutputSize;
    procedure SetInRegisterSize(ARS: Integer);
    procedure SetOutRegisterSize(ARS: Integer);
    procedure SetRegisterUpdateInterval(Upd: Integer);
    procedure RunModbus;
  end;

implementation

procedure IntlMBTReadCompleted   (hSocket: THandle;           // socket handle
   callbackContext: Longword;  // callback context, handed over at the call
   errorCode: LongInt;	       // result of the read operation
   tableType: Byte;            // type of MODBUS/TCP tables(MODBUSTCP_TABLE_xxx)
   dataStartAddress: Word;     // start address of the registers or coils to be read
   numRead: Word;              // number of the registers or coils to be read
   numBytes: Word;             // number of the bytes to be read
   pReadBuffer: LPBYTE         // memory section with the data to be written
   ); stdcall;
begin
  TModbusSequencer(callbackContext).WriteOutputs;
end;

procedure IntlMBTWriteCompleted (
   hSocket: THandle;           // socket handle
   callbackContext: Longword;  // callback context, handed over at the call
   errorCode: LongInt;	       // result of the write operation
   tableType: Byte;            // type of MODBUS/TCP tables(MODBUSTCP_TABLE_xxx)
                               // output registers or output coils
   dataStartAddress: Word;     // start address of the registers or coils to be written
   numWrite: Word;             // number of the registers or coils to be written
   pWriteBuffer: LPBYTE        // memory section with the data to be written
   ); stdcall;
begin
  TModbusSequencer(callbackContext).ReadInputs;
end;

{ TModbusSequencer }

procedure TModbusSequencer.ConfigureModbus;
begin
  FState:= eiosConfigured;
end;

constructor TModbusSequencer.Create;
begin
  RegisterUpdateInterval:= 1000000;
  ConnectDll;
  MBTInit;
end;

destructor TModbusSequencer.Destroy;
begin
  StopModbus;
  MBTExit;
  inherited;
end;

procedure TModbusSequencer.OpenHandles;
begin
  if (MBTConnect(PChar(IP), 502, False, 1000, hSocket) = S_OK) then begin
    FState:= eiosHandles;
  end else
    raise Exception.Create('Failure');
end;

procedure TModbusSequencer.SetInRegisterSize(ARS: Integer);
begin
  InRegisterSize:= ARS;
end;

procedure TModbusSequencer.SetIP(IP: string);
begin
  Self.IP:= IP;
end;

procedure TModbusSequencer.SetOutRegisterSize(ARS: Integer);
begin
  OutRegisterSize:= ARS;
end;

procedure TModbusSequencer.SetState(AState: TModbusState);
begin
  Self.FState:= AState;
end;

procedure TModbusSequencer.SetRegisterUpdateInterval(Upd: Integer);
begin
  RegisterUpdateInterval:= Upd;
end;

procedure TModbusSequencer.StopModbus;
begin
  if (hSocket <> 0) then begin
    MBTDisconnect(hSocket);
  end;
end;

function Bit(Map, value: Byte): Byte;
begin
  Result:= Ord((Map and value) <> 0);
end;

procedure TModbusSequencer.Sweep;
var I: Integer;
    Tmp: array[0..63] of Word;
begin
  try
    if State = eiosRunning then begin
      if (MBTWriteCoils(hSocket, 0, OutputSize * 8, @outData, nil, 0) <> S_OK) then begin
        raise Exception.Create('Failed');
      end;
      if (MBTReadCoils(hSocket, MODBUSTCP_TABLE_INPUT_COIL, 0, InputSize * 8, @inData, nil, 0) <> S_OK) then begin
        raise Exception.Create('Failed');
      end; 

      Inc(UpdateCount);
      if (UpdateCount mod RegisterUpdateInterval) = 0 then begin
        if MBTReadRegisters(hSocket, MODBUSTCP_TABLE_OUTPUT_REGISTER, 0, Self.InRegisterSize, @Tmp, nil, 0) <> S_OK then begin
          raise Exception.Create('Failed');
        end;

        // Reverse incoming and outgoing
        for I:= 0 to InRegisterSize - 1 do begin
          InRegister[I] := MBTSwapWord(Tmp[I]);
        end;
        for I:= 0 to OutRegisterSize - 1 do begin
          Tmp[I]:= MBTSwapWord(OutRegister[I]);
        end;

        if (MBTWriteRegisters(hSocket, 0, Self.OutRegisterSize, @Tmp, nil, 0) <> S_OK) then begin
          raise Exception.Create('Failed');
        end;
      end;
    end;
  except
    SetState(eiosNoHandles);
    hSocket:= 0;
  end;

end;

procedure TModbusSequencer.RunModbus;
begin
  SetState(eiosRunning);
  //WriteOutputs; uncomment for autorun
end;

procedure TModbusSequencer.WriteOutputs;
begin
  if (MBTWriteCoils(hSocket, 0, OutputSize * 8, @outData, IntlMBTWriteCompleted, Integer(Self)) <> S_OK) then begin
    SetState(eiosNoHandles);
  end;
end;

procedure TModbusSequencer.ReadInputs;
begin
  if (MBTReadCoils(hSocket, MODBUSTCP_TABLE_INPUT_COIL, 0, InputSize * 8, @inData, IntlMBTReadCompleted, Integer(Self)) <> S_OK) then begin
    SetState(eiosNoHandles);
  end;
end;

end.
