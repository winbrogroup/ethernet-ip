unit PlcLPERack;

interface

uses Sysutils, Classes, AbstractPlcPlugin, CncTypes, NamedPlugin, LPERackDevice;

type
  ByteArray = array[0..65535] of Byte;
  PByteArray = ^ByteArray;
  TLPERackValue = (
    lpervStrobe,
    lpervEnable,
    lpervRefeedDown,
    lpervRefeedUp,
    lpervPreset,
    lpervMonitor,
    lpervRefeedLow,
    lpervRefeedHigh,
    lpervRefeedInc,
    lpervPresetValue

  );
  TLPERackValues = set of TLPERackValue;

  TLPERack = class(TAbstractPlcPlugin)
  private
    ResolveItem: TPlcPluginREsolveItemValueEv;
    AddressTag, OutputTag, InputTag, StrobeTag: Pointer;
    InBuffer: PByteArray;
    OutBuffer: PByteArray;
    ISize, OSize, IOSize: Integer;
    MonitorEnable: array[0..255] of Boolean;
    ShadowOutput: array[0..255] of Byte;  // Change detection
    Values: TLPERackValues;
    Strobe, LastStrobe: Boolean;
    Address: Integer;
    NextAddress: Integer;
    AddressStrobe: Boolean;
    FHalted: Boolean;
    DeviceList: TStringList;
    Sweep: Integer;
    ExtraSweep: Boolean;

    RefeedLow: Integer;
    RefeedHigh: Integer;
    RefeedInc: Integer;
    WriteFirstByte: Boolean; //selects which byte to write to refeed dac
    RefeedDAC: Integer;
    PresetValue: Integer;
    procedure UpdateState;
    function InDeviceList(Add: Integer): Boolean;
  public
    constructor Create( Parameters : string;
                        ResolveDeviceEv : TPlcPluginResolveDeviceEv;
                        ResolveItemEv : TPlcPluginResolveItemEv); override;
    procedure Compile ( var Embedded : TEmbeddedData;
                        aReadEv : TPlcPluginReadEv;
                        ResolveItemEv : TPlcPluginResolveItemEv;
                        Write : Boolean);  override;
    destructor Destroy; override;
    procedure GatePre; override;
    function GatePost : Integer; override;
    function Read(var Embedded : TEmbeddedData) : Boolean; override;
    procedure Write(var Embedded : TEmbeddedData; Line, LastLine : Boolean); override;
    function GateName(var Embedded : TEmbeddedData) : string; override;
    function ItemProperties : string; override;
    property Halted: Boolean read FHalted write FHalted;
    procedure AddDevice(Device: ILPERackDevice);
  end;

var LPERack : TLPERack;


implementation

const
  PropertyName : array [TLPERackValue] of TPlcProperty = (
    ( Name : 'Strobe';          ReadOnly : False ),
    ( Name : 'Enable';          ReadOnly : False ),
    ( Name : 'RefeedDown';      ReadOnly : False ),
    ( Name : 'RefeedUp';        ReadOnly : False ),
    ( Name : 'Preset';          ReadOnly : False ),

    ( Name : 'Monitor';         ReadOnly : False ),
    ( Name : 'RefeedLow';       ReadOnly : False ),
    ( Name : 'RefeedHigh';      ReadOnly : False ),
    ( Name : 'RefeedInc';       ReadOnly : False ),
    ( Name : 'PresetValue';     ReadOnly : False )


  );

{ TLPERack }

procedure TLPERack.Compile(var Embedded: TEmbeddedData;
  aReadEv: TPlcPluginReadEv; ResolveItemEv: TPlcPluginResolveItemEv;
  Write: Boolean);
var Token: string;
    I: TLPERackValue;
    IsConstant: Boolean;
begin
  Token := aReadEv(Self);
  if Token <> '.' then
    raise Exception.Create('Period [.] expected');

  Token := aReadEv(Self);
  for I := Low(I) to High(I) do begin
    if CompareText(PropertyName[I].Name, Token) = 0 then begin
      Embedded.Command := Ord(I);
      if I >= lpervMonitor then begin
        if not (aReadEv(Self) = '.') then
          raise Exception.Create('Period [.] expected after Value item (monitor, refeed high low)');

        Token:= aReadEv(Self);
        Embedded.OData := ResolveItemEv(Self, PChar(Token), IsConstant, ResolveItem);
      end;
    end;
  end;
end;

constructor TLPERack.Create(Parameters: string;
  ResolveDeviceEv: TPlcPluginResolveDeviceEv;
  ResolveItemEv: TPlcPluginResolveItemEv);
var Address, Output, Input, Strobe, IODevice: string;
    IsConstant: Boolean;
    I: Integer;
begin
  DeviceList:= TStringList.Create;
  Address:= ReadDelimited(Parameters, ';');
  Output:= ReadDelimited(Parameters, ';');
  Input:= ReadDelimited(Parameters, ';');
  Strobe:= ReadDelimited(Parameters, ';');
  IODevice:= ReadDelimited(Parameters, ';');
  RefeedInc:= 2;
  for I:= 0 to 255 do
    MonitorEnable[I]:= False;

  if not ResolveDeviceEv(Self, PChar(IODevice), ISize, OSize, Pointer(InBuffer), Pointer(OutBuffer)) then begin
    raise Exception.Create('Device not registered: ' + IODevice);
  end;

  AddressTag:= ResolveItemEv(Self, PChar(Address), IsConstant, ResolveItem);
  OutputTag:= ResolveItemEv(Self, PChar(Output), IsConstant, ResolveItem);
  InputTag:= ResolveItemEv(Self, PChar(Input), IsConstant, ResolveItem);
  StrobeTag:= ResolveItemEv(Self, PChar(Strobe), IsConstant, ResolveItem);

  Self.Address:= 0;

  IOSize := ISize;
  if OSize > ISize then
    IOSize:= OSize;

  LPERack:= Self;
end;

destructor TLPERack.Destroy;
begin
  LPERack:= nil;
  DeviceList.Free;
  inherited;
end;

procedure TLPERack.Write(var Embedded: TEmbeddedData; Line, LastLine: Boolean);
var I: Integer;
begin
  if Line <> LastLine then begin  // Edge detect rising / falling
    case TLPERackValue(Embedded.Command) of
      lpervStrobe: Strobe:= Line;
      lpervEnable: begin end;
      lpervMonitor : begin
        I:= ResolveItem(Self, Embedded.OData);
        I:= I mod 256;
        MonitorEnable[I]:= Line;
      end;
      lpervRefeedLow : begin
        RefeedLow:= ResolveItem(Self, Embedded.OData);
      end;
      lpervRefeedHigh : begin
        RefeedHigh:= ResolveItem(Self, Embedded.OData);
      end;

      lpervPreset : begin
        WriteFirstByte:= True;
      end;

      lpervRefeedDown: begin
        WriteFirstByte:= True;
      end;

      lpervRefeedUp: begin
        WriteFirstByte:= True;
      end;

      lpervRefeedInc: begin
        RefeedInc:= ResolveItem(Self, Embedded.OData);
      end;

      lpervPresetValue: begin
        PresetValue:= ResolveItem(Self, Embedded.OData);
      end;
    end;
    if Line then
      Include(VAlues, TLPERackValue(Embedded.Command))
    else
      Exclude(Values, TLPERackValue(Embedded.Command));
  end;
end;

function TLPERack.GateName(var Embedded: TEmbeddedData): string;
var I: TLPERackValue;
begin
  I:= TLPERackValue(Embedded.Command);
  if (I <= High(I)) and
     (I >= Low(I)) then begin
    Result:= PropertyName[I].Name;
  end else
    Result:= 'Unknown';
end;

function TLPERack.ItemProperties: string;
var I: TLPERackValue;
begin
  for I := Low(I) to High(I) do
    Result := Result + PropertyName[I].Name + CarRet + BooleanIdent[I in Values] + CarRet;
end;

function TLPERack.GatePost: Integer;
begin
  if lpervEnable in Values then begin
    if Strobe <> LastStrobe then begin
      UpdateState;
      LastStrobe:= Strobe;
    end;
  end;
  Result:= 0;
end;

function TLPERack.Read(var Embedded: TEmbeddedData): Boolean;
var I: Integer;
begin
  case TLPERackValue(Embedded.Command) of
    lpervMonitor : begin
      I:= ResolveItem(Self, Embedded.OData);
      I:= I mod 256;
      Result:= MonitorEnable[I];
    end;
  else
    Result := TLPERackValue(Embedded.Command) in Values;
  end;
end;

procedure TLPERack.GatePre;
begin

end;

function Sign(D:double) : integer ;
begin
  if D>0.0 then Sign := +1
  else if D<0.0 then Sign := -1
  else Sign := 0
end;

procedure TLPERack.UpdateState;
var I: Integer;
    Device: ILPERackDevice;
    Data: Integer;
begin
  if ExtraSweep then begin
    AddressStrobe:= not AddressStrobe;
    Host.SetItemValue(StrobeTag, Ord(AddressStrobe));
    ExtraSweep:= False;
    Exit;
  end;

  Inc(Sweep);

  if not Halted then begin
    InBuffer[Address]:= Host.GetItemValue(InputTag);
    Address:= NextAddress;

    if lpervPreset in Values then begin
      RefeedDAC:= PresetValue;

      if not WriteFirstByte then begin
        Address:= 6;
        Data:= RefeedDAC;
      end else begin
        Address:= 7;
        Data:= RefeedDAC div 256;
      end;
      WriteFirstByte:= not WriteFirstByte;
    end else
    if ((lpervRefeedDown in Values) or
        (lpervRefeedUp in Values))
        and ((Sweep mod 2) = 0) then begin

      if WriteFirstByte then begin
        if lpervRefeedDown in Values then
          RefeedDAC:= RefeedDAC + RefeedInc
        else
          RefeedDAC:= RefeedDAC - RefeedInc;

        if RefeedDAC > RefeedHigh then
          RefeedDAC:= RefeedHigh;
        if RefeedDAC < RefeedLow then
          RefeedDAC:= RefeedLow;
      end;
      NextAddress:= Address;

      if not WriteFirstByte then begin
        Address:= 6;
        Data:= RefeedDAC;
      end else begin
        Address:= 7;
        Data:= RefeedDAC div 256;
      end;
      WriteFirstByte:= not WriteFirstByte;

    end else
    if (DeviceList.Count > 0) and ((Sweep mod 3) = 0) then begin
      // This intercepts the normal decoding and sets the next address to this
      // address so the system just picks up.
      NextAddress:= Address;
      Device:= ILPERackDevice(Pointer(DeviceList.Objects[0]));
      Device.NextOutput(Address, Data);
    end else begin

      for I:= 0 to IOSize - 1 do begin
        Address:= Address + 1;
        Address:= Address mod IOSize;
        NextAddress:= Address;

        if not InDeviceList(Address) then begin
          if ShadowOutput[Address] <> OutBuffer[Address] then begin
            ShadowOutput[Address]:= OutBuffer[Address];
            Data:= OutBuffer[Address];
            Break;
          end
        end;

        if MonitorEnable[Address] then begin
          Data:= OutBuffer[Address];
          Break;
        end;
      end;
    end;

    if MonitorEnable[Address] then begin
      ExtraSweep:= True;
    end else begin
      AddressStrobe:= not AddressStrobe;
      Host.SetItemValue(StrobeTag, Ord(AddressStrobe));
    end;
    Host.SetItemValue(OutputTag, Data);
    Host.SetItemValue(AddressTag, Address);
  end;
end;

procedure TLPERack.AddDevice(Device: ILPERackDevice);
begin
  DeviceList.AddObject('', TObject(Device));
end;

function TLPERack.InDeviceList(Add: Integer): Boolean;
var I: Integer;
    Device: ILPERackDevice;
begin
  Result:= True;
  for I:= 0 to DeviceList.Count - 1 do begin
    Device:= ILPERackDevice(Pointer(DeviceList.Objects[0]));

    if (Add >= Device.GetBaseAddress) and
       (Add < Device.GetBaseAddress + Device.GetLength) then begin
      Exit;
    end;
  end;
  Result:= False;
end;

initialization
  TAbstractPlcPlugin.AddPlugin('LPERack', TLPERack);
end.
