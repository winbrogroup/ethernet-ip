unit Methods;

interface

uses Classes, SysUtils, AbstractScript, INamedInterface, NamedPlugin,
     IScriptInterface, IStringArray, CncTypes, Dialogs, PlcLPERack;

type
  TRackHalt = class (TScriptMethod)
  public
    function Name: WideString; override; stdcall;
    procedure Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer); override; stdcall;
    procedure Reset; override; stdcall;
  end;

  TRackResume = class (TScriptMethod)
  public
    function Name: WideString; override; stdcall;
    procedure Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer); override; stdcall;
  end;

implementation

{ TRackHalt }

function TRackHalt.Name: WideString;
begin
  Result:= 'Halt';
end;

procedure TRackHalt.Reset;
begin
  if Assigned(PlcLPERack.LPERack) then
    LPERack.Halted:= False;
end;

procedure TRackHalt.Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer);
begin
  if Assigned(PlcLPERack.LPERack) then
    LPERack.Halted:= True;
end;

{ TRackResume }

function TRackResume.Name: WideString;
begin
  Result:= 'Resume';
end;

procedure TRackResume.Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer);
begin
  if Assigned(PlcLPERack.LPERack) then
    LPERack.Halted:= False;
end;

initialization
  TDefaultScriptLibrary.SetName('LPERack');
  TDefaultScriptLibrary.AddMethod(TRackHalt);
  TDefaultScriptLibrary.AddMethod(TRackResume);
end.
