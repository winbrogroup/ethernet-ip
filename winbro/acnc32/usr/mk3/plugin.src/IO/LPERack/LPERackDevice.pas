unit LPERackDevice;

interface

type
  ILPERackDevice = interface
    function GetBaseAddress: Integer; stdcall;
    function GetLength: Integer; stdcall;
    function NextInput: Integer; stdcall;
    procedure NextOutput(var Address, Data: Integer); stdcall;
  end;

implementation

end.
