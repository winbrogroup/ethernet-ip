unit LPE263;

interface

uses Sysutils, Classes, AbstractPlcPlugin, CncTypes, NamedPlugin, PlcLPERack, LPERackDevice;

type
  TLPE263Value = (
    lpe263on,
    lpe263off,
    lpe263son,
    lpe263soff,
    lpe263Suppression,
    lpe263Normal
  );
  TLPE263Values = set of TLPE263Value;

  TLPE263Plc = class(TAbstractPlcPlugin, ILPERackDevice)
  private
    BaseAddress: Integer;
    Values: TLPE263Values;
    ResolveItem: TPlcPluginREsolveItemValueEv;

    OnTime: Integer;
    OffTime: Integer;
    SOnTime: Integer;
    SOffTime: Integer;

    Normal: Boolean;
    Suppression: Boolean;

    Stage: Integer;
  public
    constructor Create( Parameters : string;
                        ResolveDeviceEv : TPlcPluginResolveDeviceEv;
                        ResolveItemEv : TPlcPluginResolveItemEv); override;
    procedure Compile ( var Embedded : TEmbeddedData;
                        aReadEv : TPlcPluginReadEv;
                        ResolveItemEv : TPlcPluginResolveItemEv;
                        Write : Boolean);  override;
    destructor Destroy; override;
    procedure GatePre; override;
    function GatePost : Integer; override;
    function Read(var Embedded : TEmbeddedData) : Boolean; override;
    procedure Write(var Embedded : TEmbeddedData; Line, LastLine : Boolean); override;
    function GateName(var Embedded : TEmbeddedData) : string; override;
    function ItemProperties : string; override;
    function GetBaseAddress: Integer; stdcall;
    function GetLength: Integer; stdcall;
    function NextInput: Integer; stdcall;
    procedure NextOutput(var Address, Data: Integer); stdcall;
  end;

implementation

const
  PropertyName : array [TLPE263Value] of TPlcProperty = (
    ( Name : 'On';          ReadOnly : False ),
    ( Name : 'Off';         ReadOnly : False ),
    ( Name : 'Son';         ReadOnly : False ),
    ( Name : 'Soff';        ReadOnly : False ),
    ( Name : 'Suppression'; ReadOnly : False ),
    ( Name : 'Normal';      ReadOnly : False )
  );

{ TLPE263Plc }

procedure TLPE263Plc.Write(var Embedded: TEmbeddedData; Line, LastLine: Boolean);
begin
  if Line <> LastLine then begin  // Edge detect rising / falling
    case TLPE263Value(Embedded.Command) of
      lpe263on: begin
        OnTime:= ResolveItem(Self, Embedded.OData);
      end;

      lpe263off: begin
        OffTime:= ResolveItem(Self, Embedded.OData);
      end;

      lpe263son: begin
        SOnTime:= ResolveItem(Self, Embedded.OData);
      end;

      lpe263soff: begin
        SOffTime:= ResolveItem(Self, Embedded.OData);
      end;

      lpe263Normal: begin
        Normal:= Line;
      end;

      lpe263Suppression: begin
        Suppression:= Line;
      end;
    end;
    if Line then
      Include(VAlues, TLPE263Value(Embedded.Command))
    else
      Exclude(Values, TLPE263Value(Embedded.Command));
  end;
end;

procedure TLPE263Plc.Compile(var Embedded: TEmbeddedData; aReadEv: TPlcPluginReadEv; ResolveItemEv: TPlcPluginResolveItemEv;
  Write: Boolean);
var Token: string;
    I: TLPE263Value;
    IsConstant: Boolean;
begin
  Token := aReadEv(Self);
  if Token <> '.' then
    raise Exception.Create('Period [.] expected');

  Token := aReadEv(Self);
  for I := Low(I) to High(I) do begin
    if CompareText(PropertyName[I].Name, Token) = 0 then begin
      Embedded.Command := Ord(I);

      if I < lpe263Suppression then begin
        if not (aReadEv(Self) = '.') then
          raise Exception.Create('Period [.] expected after Monitor');

        Token:= aReadEv(Self);
        Embedded.OData := ResolveItemEv(Self, PChar(Token), IsConstant, ResolveItem);
      end;
    end;
  end;
end;

function TLPE263Plc.GateName(var Embedded: TEmbeddedData): string;
begin

end;

constructor TLPE263Plc.Create(Parameters: string; ResolveDeviceEv: TPlcPluginResolveDeviceEv;
  ResolveItemEv: TPlcPluginResolveItemEv);
var
  OffsetText: string;
begin
  OffsetText:= ReadDelimited(Parameters, ';');
  BaseAddress:= StrToIntDef(OffsetText, 8);

  if not Assigned(LPERack) then
    raise Exception.Create('');

  LPERack.AddDevice(Self);
end;

function TLPE263Plc.ItemProperties: string;
var I: TLPE263Value;
begin
  for I := Low(I) to High(I) do
    Result := Result + PropertyName[I].Name + CarRet + BooleanIdent[I in Values] + CarRet;
end;

function TLPE263Plc.GatePost: Integer;
begin
  Result:= 0;
end;

function TLPE263Plc.Read(var Embedded: TEmbeddedData): Boolean;
begin
  Result := TLPE263Value(Embedded.Command) in Values;
end;

destructor TLPE263Plc.Destroy;
begin
  inherited;
end;

procedure TLPE263Plc.GatePre;
begin
end;

function TLPE263Plc.GetBaseAddress: Integer;
begin
  Result:= BaseAddress;
end;

function TLPE263Plc.GetLength: Integer;
begin
  Result:= 8;
end;

procedure TLPE263Plc.NextOutput(var Address, Data: Integer);
begin
  case Stage of
    0: begin
      Address:= 9;
      Data:= SoffTime div $100;
    end;
    1: begin
      Address:= 10;
      Data:= SoffTime;
    end;
    2: begin
      Address:= 9;
      Data:= OffTime div $100;
    end;
    3: begin
      Address:= 11;
      Data:= OffTime;
    end;
    4: begin
      Address:= 9;
      Data:= SonTime div $100;
    end;
    5: begin
      Address:= 12;
      Data:= SOnTime;
    end;
    6: begin
      Address:= 9;
      Data:= OnTime div $100;
    end;
    7: begin
      Address:= 13;
      Data:= OnTime;
    end;
    8: begin
      Address:= 8;
      Data:= (Ord(Normal) * 2) + Ord(Suppression)
    end;
  end;
  Stage:= (Stage + 1) mod 9;
end;

function TLPE263Plc.NextInput: Integer;
begin
  Result:= -1;
end;

initialization
  TAbstractPlcPlugin.AddPlugin('LPE263', TLPE263Plc);
end.
