unit TestStreamReadWrite;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, TcAdsAPI, TcAdsDef;

type
  TForm5 = class(TForm)
    Button1: TButton;
    Timer: TTimer;
    Label1: TLabel;
    IN1: TStaticText;
    IN2: TStaticText;
    IN3: TStaticText;
    IN4: TStaticText;
    IN5: TStaticText;
    IN6: TStaticText;
    OUT1: TEdit;
    OUT2: TEdit;
    OUT3: TEdit;
    OUT4: TEdit;
    OUT5: TEdit;
    OUT6: TEdit;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure TimerTimer(Sender: TObject);
  private
    clientPort : Integer;
    serverAddr  : TAmsAddr;
    ReadHandle: Integer;
    WriteHandle: Integer;
  public
    { Public declarations }
  end;

var
  Form5: TForm5;

implementation

{$R *.dfm}

const READ_LENGTH = 2;

procedure TForm5.TimerTimer(Sender: TObject);
var  InData : array[0..5] of Byte;
    OutData : array[0..5] of Byte;
    Res: LongInt;
    IState1, IState2: WORD;
begin

AdsSyncReadStateReq(@ServerAddr, @IState1, @IState2);

Exit;

  //if Res = tcAdsDef.ADSERR_NOERR then begin
    Res := AdsSyncReadReq(@serverAddr, ADSIGRP_SYM_VALBYHND, ReadHandle, sizeof(InData), @InData  );
  //end;

  if Res <> tcADSDEF.ADSERR_NOERR then
    Label1.Caption:= IntToStr(Res);

  Self.IN1.Caption:= IntToStr(InData[0]);
  Self.IN2.Caption:= IntToStr(InData[1]);
  Self.IN3.Caption:= IntToStr(InData[2]);
  Self.IN4.Caption:= IntToStr(InData[3]);
  Self.IN5.Caption:= IntToStr(InData[4]);
  Self.IN6.Caption:= IntToStr(InData[5]);

  OUTData[0] := StrToIntDef(OUT1.Text, 0);
  OUTData[1] := StrToIntDef(OUT2.Text, 0);
  OUTData[2] := StrToIntDef(OUT3.Text, 0);
  OUTData[3] := StrToIntDef(OUT4.Text, 0);
  OUTData[4] := StrToIntDef(OUT5.Text, 0);
  OUTData[5] := StrToIntDef(OUT6.Text, 0);


  //if Res = tcAdsDef.ADSERR_NOERR then begin
     Res := AdsSyncWriteReq(@serverAddr, ADSIGRP_SYM_VALBYHND, WriteHandle, sizeof(OutData), @OutData  );
  //end;

  if Res <> tcADSDEF.ADSERR_NOERR then
    Label1.Caption:= IntToStr(Res);

end;

procedure TForm5.FormCreate(Sender: TObject);
var Res: LongInt;
    sName: AnsiString;
    cbReturn : Longword;
begin
  clientPort := AdsPortOpen();
  serverAddr.port:= 801;          
  serverAddr.netid.b[0] := 192;
  serverAddr.netid.b[1] := 168;
  serverAddr.netid.b[2] := 1;
  serverAddr.netid.b[3] := 99;
  serverAddr.netid.b[4] := 1;
  serverAddr.netid.b[5] := 1;

  sName := 'MAIN.PCTOPLC';
  Res := AdsSyncReadWriteReqEx(@serverAddr,
            ADSIGRP_SYM_HNDBYNAME,
            $0000,
            sizeof(WriteHandle),
            @WriteHandle,
            Length(sName)+1,
            @sName[1], @cbReturn );

  if Res <> tcADSDEF.ADSERR_NOERR then
    Application.Terminate;

  sName := 'MAIN.PLCTOPC';
  Res := AdsSyncReadWriteReqEx(@serverAddr,
            ADSIGRP_SYM_HNDBYNAME,
            $0000,
            4,
            @ReadHandle,
            Length(sName)+1,
            @sName[1], @cbReturn );

  if Res <> tcADSDEF.ADSERR_NOERR then
    Application.Terminate;

    AdsSyncSetTimeout(100);
end;

procedure TForm5.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  AdsPortClose();
end;

end.
