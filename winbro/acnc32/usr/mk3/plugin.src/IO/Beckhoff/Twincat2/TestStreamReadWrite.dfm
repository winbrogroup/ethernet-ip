object Form5: TForm5
  Left = 0
  Top = 0
  Width = 488
  Height = 281
  Caption = 'Form5'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 280
    Top = 16
    Width = 31
    Height = 13
    Caption = 'Label1'
  end
  object Button1: TButton
    Left = 120
    Top = 104
    Width = 75
    Height = 25
    Caption = 'Button1'
    TabOrder = 0
  end
  object IN1: TStaticText
    Left = 32
    Top = 48
    Width = 30
    Height = 17
    AutoSize = False
    BevelKind = bkSoft
    Caption = 'IN1'
    TabOrder = 1
  end
  object IN2: TStaticText
    Left = 72
    Top = 48
    Width = 30
    Height = 17
    AutoSize = False
    BevelKind = bkSoft
    Caption = 'B1'
    TabOrder = 2
  end
  object IN3: TStaticText
    Left = 112
    Top = 48
    Width = 30
    Height = 17
    AutoSize = False
    BevelKind = bkSoft
    Caption = 'B1'
    TabOrder = 3
  end
  object IN4: TStaticText
    Left = 152
    Top = 48
    Width = 30
    Height = 17
    AutoSize = False
    BevelKind = bkSoft
    Caption = 'B1'
    TabOrder = 4
  end
  object IN5: TStaticText
    Left = 192
    Top = 48
    Width = 30
    Height = 17
    AutoSize = False
    BevelKind = bkSoft
    Caption = 'B1'
    TabOrder = 5
  end
  object IN6: TStaticText
    Left = 232
    Top = 48
    Width = 30
    Height = 17
    AutoSize = False
    BevelKind = bkSoft
    Caption = 'B1'
    TabOrder = 6
  end
  object OUT1: TEdit
    Left = 32
    Top = 72
    Width = 25
    Height = 21
    TabOrder = 7
    Text = '1'
  end
  object OUT2: TEdit
    Left = 72
    Top = 72
    Width = 25
    Height = 21
    TabOrder = 8
    Text = '1'
  end
  object OUT3: TEdit
    Left = 112
    Top = 72
    Width = 25
    Height = 21
    TabOrder = 9
    Text = '1'
  end
  object OUT4: TEdit
    Left = 152
    Top = 72
    Width = 25
    Height = 21
    TabOrder = 10
    Text = '1'
  end
  object OUT5: TEdit
    Left = 192
    Top = 72
    Width = 25
    Height = 21
    TabOrder = 11
    Text = '1'
  end
  object OUT6: TEdit
    Left = 232
    Top = 72
    Width = 25
    Height = 21
    TabOrder = 12
    Text = '1'
  end
  object Timer: TTimer
    Interval = 10
    OnTimer = TimerTimer
    Left = 288
    Top = 136
  end
end
