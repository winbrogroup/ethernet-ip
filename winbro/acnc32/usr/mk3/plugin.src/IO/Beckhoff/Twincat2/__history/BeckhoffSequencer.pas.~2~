unit BeckhoffSequencer;

interface

uses AbstractIOSystemAccessor, SysUtils, NamedPlugin, INamedInterface, CncTypes, TcAdsDef, TcAdsAPI;

type
  BeckhoffIO = class (TAbstractIOSystemAccessor)
  private
    InSize: Integer;
    OutSize: Integer;
    InName: String;
    OutName: String;
    IPAddress: String;

    InData : array[0..150] of Byte;
    OutData : array[0..150] of Byte;
    ServerAddr  : TAmsAddr;

    BeckhoffHealthy : Boolean;
    IOPort: Integer;

    ReadHandle: Integer;
    WriteHandle: Integer;
    CycleCount: Integer;

    Res: LongInt;
    function ADSErrorToString(Err: Integer) : string;

  public
    function GetInfoPageContent(aPage : Integer) : WideString; override;
    procedure Initialise(Host: IIOSystemAccessorHost; const Params : WideString); override;
    procedure UpdateShadowToReal; override;
    procedure UpdateRealToShadow; override;
    function Healthy : Boolean; override;
    procedure Reset; override;
    function LastError : WideString; override;
    function GetInputArea: Pointer; override;
    function GetOutputArea: Pointer; override;
    function GetInputSize: Integer; override;
    function GetOutputSize: Integer; override;
    function TextDefinition: WideString; override;
    procedure Shutdown; override;
  end;

implementation

{ BeckhoffIO }

function BeckhoffIO.GetOutputSize: Integer;
begin
  Result:= OutSize;
end;

function BeckhoffIO.GetInfoPageContent(aPage: Integer): WideString;
begin
  Result:= '';

  case aPage of
    0 : begin
      Result :=
        'Cycle Count' + CarRet + IntToStr(CycleCount) + CarRet +
        'Error State' + CarRet + ADSErrorToString(Res);
    end;
  end;end;

function BeckhoffIO.GetOutputArea: Pointer;
begin
  Result:= @OutData[0];
end;

procedure BeckhoffIO.Shutdown;
begin
  if IOPort <> 0 then
     AdsPortClose;
end;

function BeckhoffIO.GetInputSize: Integer;
begin
  Result:= InSize;
end;

procedure BeckhoffIO.Reset;
var
    sName: AnsiString;
    cbReturn : Longword;
begin
  if not BeckhoffHealthy then begin
     if IOPort <> 0 then begin
       AdsPortClose;
     end;

    IOPort := AdsPortOpen;

    if IOPort <> 0 then begin
      AdsSyncSetTimeout(100);

      sName := OutName;
      Res := AdsSyncReadWriteReq(@serverAddr,
                ADSIGRP_SYM_HNDBYNAME,
                $0000,
                sizeof(WriteHandle),
                @WriteHandle,
                Length(sName)+1,
                @sName[1], @cbReturn );

      if Res <> TcAdsDef.ADSERR_NOERR then begin
        Exit;
      end;

      sName := InName;
      Res := AdsSyncReadWriteReqEx(@serverAddr,
                ADSIGRP_SYM_HNDBYNAME,
                $0000,
                4,
                @ReadHandle,
                Length(sName)+1,
                @sName[1], @cbReturn );

      if Res <> TcAdsDef.ADSERR_NOERR then begin
        Exit;
      end;

      BeckhoffHealthy:= True;
    end;
  end;
end;

function BeckhoffIO.GetInputArea: Pointer;
begin
  Result:= @InData[0];
end;

function BeckhoffIO.TextDefinition: WideString;
begin
  Result:= '';
end;

function BeckhoffIO.Healthy: Boolean;
begin
  Result:= BeckhoffHealthy;
end;

function BeckhoffIO.LastError: WideString;
begin
  Result:= '';
end;

procedure BeckhoffIO.Initialise(Host: IIOSystemAccessorHost; const Params: WideString);
var x : string;
    tmp: string;
    I: Integer;
begin
  tmp := Params;

  try
    x := Trim(CncTypes.ReadDelimited(tmp, ';'));
    InSize:= StrToInt(x);
    x := Trim(CncTypes.ReadDelimited(tmp, ';'));
    OutSize:= StrToInt(x);
    InName := Trim(CncTypes.ReadDelimited(tmp, ';'));
    OutName := Trim(CncTypes.ReadDelimited(tmp, ';'));
    IPAddress := Trim(CncTypes.ReadDelimited(tmp, ';'));
    x:= IPAddress;
    for I := 0 to 5 do begin
      ServerAddr.netId.b[I]:= StrToInt(Trim(CNCTypes.ReadDelimited(x, '.')));
    end;
    ServerAddr.port:= 801;
  except
    on E: Exception do
      raise Exception.Create('Misconfiguration of Twincat2: ' + E.Message);
  end;

  if InName = '' then begin
    raise Exception.Create('Twincat2 input name cant be empty');
  end;
  if OutName = '' then begin
    raise Exception.Create('Twincat2 output name cant be empty');
  end;

  IOPort:= 0;
  BeckhoffHealthy := False;
  Host.AddPage('State');
end;

procedure BeckhoffIO.UpdateRealToShadow;
begin
  if BeckhoffHealthy then begin
    Res := AdsSyncReadReq(@serverAddr, ADSIGRP_SYM_VALBYHND, ReadHandle, InSize, @InData  );
    if Res <> TcAdsDef.ADSERR_NOERR then begin
      BeckhoffHealthy := False;
    end;
  end;
end;

procedure BeckhoffIO.UpdateShadowToReal;
begin
  if BeckhoffHealthy then begin
    Res := AdsSyncWriteReq(@serverAddr, ADSIGRP_SYM_VALBYHND, WriteHandle, OutSize, @OutData  );
    if Res <> TcAdsDef.ADSERR_NOERR then begin
      BeckhoffHealthy := False;
    end else
      Inc(CycleCount);
  end;
end;

function BeckhoffIO.ADSErrorToString(Err: Integer) : string;
begin
  case(Err) of
  ADSERR_NOERR: Result:= 'Healthy';
  ADSERR_DEVICE_ERROR: Result:= 'Device Error';

  ADSERR_DEVICE_SRVNOTSUPP: Result := 'Service is not supported by server';
  ADSERR_DEVICE_INVALIDGRP: Result := 'Invalid indexGroup';
  ADSERR_DEVICE_INVALIDOFFSET: Result := 'Invalid indexOffset';
  ADSERR_DEVICE_INVALIDACCESS: Result := 'Reading/writing not permitted';
  ADSERR_DEVICE_INVALIDSIZE: Result := 'Parameter size not correct';
  ADSERR_DEVICE_INVALIDDATA: Result := 'Invalid parameter value(s)';
  ADSERR_DEVICE_NOTREADY: Result := 'Device is not in a ready state';
  ADSERR_DEVICE_BUSY: Result := 'Device is busy';
  ADSERR_DEVICE_INVALIDCONTEXT: Result := 'Invalid context (must be InWindows)';
  ADSERR_DEVICE_NOMEMORY: Result := 'Out of memory';
  ADSERR_DEVICE_INVALIDPARM: Result := 'Invalid parameter value(s)';
  ADSERR_DEVICE_NOTFOUND: Result := 'Not found (files, ...)';
  ADSERR_DEVICE_SYNTAX: Result := 'Syntax error in comand or file';
  ADSERR_DEVICE_INCOMPATIBLE: Result := 'Objects do not match';
  ADSERR_DEVICE_EXISTS: Result := 'Object already exists';
  ADSERR_DEVICE_SYMBOLNOTFOUND: Result := 'Symbol not found';
  ADSERR_DEVICE_SYMBOLVERSIONINVALID: Result := 'Symbol version invalid';
  ADSERR_DEVICE_INVALIDSTATE: Result := 'Server is in invalid state';
  ADSERR_DEVICE_TRANSMODENOTSUPP: Result := 'AdsTransMode not supported';
  ADSERR_DEVICE_NOTIFYHNDINVALID: Result := 'Notification handle is invalid';
  ADSERR_DEVICE_CLIENTUNKNOWN: Result := 'Notification client not registered';
  ADSERR_DEVICE_NOMOREHDLS: Result := 'No more notification handles';
  ADSERR_DEVICE_INVALIDWATCHSIZE: Result := 'Size for watch to big';
  ADSERR_DEVICE_NOTINIT: Result := 'Device not initialized';
  ADSERR_DEVICE_TIMEOUT: Result := 'Device has a timeout';
  ADSERR_DEVICE_NOINTERFACE: Result := 'Query interface failed';
  ADSERR_DEVICE_INVALIDINTERFACE: Result := 'Wrong interface required';
  ADSERR_DEVICE_INVALIDCLSID: Result := 'Class ID is invalid';
  ADSERR_DEVICE_INVALIDOBJID: Result := 'Object ID is invalid';
  ADSERR_DEVICE_PENDING: Result := 'Request is pending';
  ADSERR_DEVICE_ABORTED: Result := 'Request is aborted';
  ADSERR_DEVICE_WARNING: Result := 'Signal warning';
  ADSERR_DEVICE_INVALIDARRAYIDX: Result := 'Invalid array index';
  //
  ADSERR_CLIENT_ERROR: Result := 'Error class < client error';
  ADSERR_CLIENT_INVALIDPARM: Result := 'Invalid parameter at service call';
  ADSERR_CLIENT_LISTEMPTY: Result := 'Polling list	is empty';
  ADSERR_CLIENT_VARUSED: Result := 'Variable already in use';
  ADSERR_CLIENT_DUPLINVOKEID: Result := 'Invoke id in use';
  ADSERR_CLIENT_SYNCTIMEOUT: Result := 'Timeout elapsed';
  ADSERR_CLIENT_W32ERROR: Result := 'Error in win32 subsystem';
  ADSERR_CLIENT_TIMEOUTINVALID: Result := 'Timeout invalid';
  ADSERR_CLIENT_PORTNOTOPEN: Result := 'Ads port not opened';
  ADSERR_CLIENT_NOAMSADDR: Result := 'Ads has no AMS Address';
  ADSERR_CLIENT_SYNCINTERNAL: Result := 'Internal error in ads sync';
  ADSERR_CLIENT_ADDHASH: Result := 'Hash table overflow';
  ADSERR_CLIENT_REMOVEHASH: Result := 'Key not found in hash table';
  ADSERR_CLIENT_NOMORESYM: Result := 'No more symbols in cache';
  ADSERR_CLIENT_SYNCRESINVALID: Result := 'Invalid response received';
  ADSERR_CLIENT_SYNCPORTLOCKED: Result := 'Sync port is locked';

  else
    Result:= 'Unknown Error';
  end;

  if Err <> ADSERR_NOERR then begin
    Result:= Result + ' - Error Code ' + IntToStr(Err);
  end;
end;


initialization
  BeckhoffIO.AddIOSystemAccessor('Twincat2', BeckhoffIO);
end.
