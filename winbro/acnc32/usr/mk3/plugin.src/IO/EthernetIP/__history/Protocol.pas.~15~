unit Protocol;
//Wrapper to "C" OpenerLibrary.dll

interface

uses Windows, Classes, INamedInterface, CNCTypes, SysUtils, SyncObjs,
     AbstractIOSystemAccessor, NamedPlugin, ExtCtrls;

type
  TInitialize = function(Host, Domain, IP, Subnet, Gateway, MAC: PCHAR): Integer; stdcall;
  TShutdown = function: Integer;
  TRunNetworkHandler = function: Integer;
  TSimple = function: Integer; stdcall;
  TProcessString = function(str_to_echo: PAnsiChar): Integer; stdcall;

  TEthernetIPProtocol = class
  private
    FInitialize: TInitialize;
    FShutdown: TShutdown;
    FRunNetworkHandler: TRunnetworkHandler;
    FSimple: TSimple;
    FProcessString: TProcessString;
  public
    constructor Create(Libname: string);
    function Initialize(Host, Domain, IP, Subnet, Gateway, MAC: string): Integer;
    function Shutdown: Integer;
    function RunNetworkHandler: Integer;
    function Simple: Integer;
    function ProcessString(str_to_echo: PAnsiChar): Integer;
  end;

implementation

{ TEthernetIPProtocol }

{ TEthernetIPProtocol }

constructor TEthernetIPProtocol.Create(Libname: string);
var HMod : HMODULE;

  function GetProcAddressX(hModule: HMODULE; lpProcName: LPCSTR): FARPROC;
  begin
    Result := GetProcAddress(hModule, lpProcName);
    if not Assigned(Result) then
      raise Exception.CreateFmt('Library interface missing [%s]', [lpProcName]);
  end;

begin
  HMod := LoadLibrary(PChar('lib\' + LibName));
  if HMod = 0 then
    raise Exception.CreateFmt('Library [%s] not found', [LibName]);

  FInitialize:= GetProcAddressX(HMod, 'Initialize');
  FShutdown:= GetProcAddressX(HMod, 'Shutdown');
  FSimple:= GetProcAddressX(HMod, 'Simple');
  FProcessString:= GetProcAddressX(HMod, 'ProcessString');

  FRunNetworkHandler:= GetProcAddressX(HMod, 'RunNetworkHandler');

end;

function TEthernetIPProtocol.Initialize(Host, Domain, IP, Subnet, Gateway, MAC: string): Integer;
begin
  Result:= FInitialize(PChar(Host), PChar(Domain), PChar(IP), PChar(Subnet), PChar(Gateway), PChar(MAC));
end;

function TEthernetIPProtocol.RunNetworkHandler: Integer;
begin
  Result:= FRunNetworkHandler;
end;

function TEthernetIPProtocol.Shutdown: Integer;
begin
  Result:= FShutdown;
end;

function TEthernetIPProtocol.Simple: Integer;
begin
  Result:= FSimple;
end;

function TEthernetIPProtocol.ProcessString(str_to_echo: PAnsiChar): Integer;
begin
  Result:= FProcessString(str_to_echo);
end;

end.
