unit Broker;
//Brokers information from EthIP.protocol to Acnc32.
//The procedures contained herein are designed to expose relevant
//I/O information to/from Acnc32.
//Do not implement customer/business logic in this class. Instead, use this class (via composition) in new
//entity dedicated to implementing business or customer logic.
//Or maybe not even in pascal but the plc units...probably yeah
interface

uses Windows, Classes, INamedInterface, CNCTypes, SysUtils, SyncObjs,
     AbstractIOSystemAccessor, NamedPlugin, ExtCtrls, ProtocolWrapper;


type
  TAcnc32EthernetIP = class(TAbstractIOSystemAccessor)
  private
    FTextDef: string;
    FInputSize: Cardinal;
    FOutputSize: Cardinal;
    Host: IIOSystemAccessorHost;
    EthIPErrorText: string;
    EthIpErrorCode: Integer;
    IsEthIpHealthy: Boolean;

    ResetTag: TTagRef;
    EthIpEnabledTag: TTagRef;
    FInputArea: array[0..255] of Byte;
    FOutputArea: array[0..255] of Byte;
    EtIPLibAdapter: TEthernetIPProtocol;
    EthIpErrorCount: Integer;

    DllSearchPath: string; //DLL search path used by this IO/Subsystem. Should be derived from acnc32 core's "DllProfilePath"...

    procedure HandleError(errorMsg: string; isFault: Boolean; isException: Boolean);

    procedure AddPublicEthIPTags();


  public
    constructor Create; override;
    function GetInfoPageContent(aPage : Integer) : WideString; override;
    procedure Initialise(Host: IIOSystemAccessorHost; const Params : WideString); override;
    procedure UpdateShadowToReal; override;
    procedure UpdateRealToShadow; override;
    function Healthy : Boolean; override;
    procedure Reset; override;
    function LastError : WideString; override;
    function GetInputArea: Pointer; override;
    function GetOutputArea: Pointer; override;
    function GetInputSize: Integer; override;
    function GetOutputSize: Integer; override;
    function TextDefinition: WideString; override;
    procedure Shutdown; override;


  end;



implementation

{ TAcnc32EthernetIP }

var Count: Byte;
function SetDllDirectory(lpPathName:PWideChar): Bool; stdcall; external 'kernel32.dll' name 'SetDllDirectoryW';

//Hardcoding the buffer I/O buffer size to 256 bytes.
//Since EIP can exchange both I/O buffers in one trip across the wire (default frame size is 512),
//nothing is gained by specifying smaller sizes. Of course, this is always subject to change =).
var IO_BUFFER_SIZE: Cardinal = 256;

function TAcnc32EthernetIP.GetOutputSize: Integer;
begin
  Result:= FOutputSize;
end;

function TAcnc32EthernetIP.GetInfoPageContent(aPage: Integer): WideString;
var
  sys_stats: TEtLibSystemStats;
  lib_status: PTCncEtLibStatus;
  version_info: PTCncEtLibVersionInfo;
  //error_msg: PChar;
begin
  case aPage of
    //Info page
    //0 : begin
    //<Name> <Carrriage Return> <Value> <Carriage Return>
    //Creates a row in the Info page table.
    //  Result :=
    //    'Signon' + CarRet + Signon + CarRet +
    //    'Count' + CarRet + IntToStr(Count) + CarRet;
    //end;

    //Ethernet/IP page
    0 : begin

      EtIPLibAdapter.GetEtLibSystemStats(@sys_stats); //System statistics from lower level EIP DLL. Direct pass-through call...
      lib_status := EtIPLibAdapter.GetCncEtLibStatus(); //Status of C <--> Pascal interfacing Library.
      version_info := EtIPLibAdapter.GetCncEtLibVersionInfo();

      Result :=
        'Product Name' + CarRet + lib_status.product_name + CarRet +
        'CncEtIP DLL Ver.' + CarRet + version_info.version + ' (' + version_info.timestamp + ')' + CarRet +
        'IP' + CarRet +  lib_status.claimed_ip_address + CarRet +
        'Last Error' + CarRet + lib_status.error_msg + CarRet +
        'Error Count' + CarRet + IntToStr(Self.EthIpErrorCount) + CarRet +
        'NetworkLinkStatus' + CarRet + lib_status.network_status_description + CarRet +
        'OpenClass1Connections' + CarRet + IntToStr(sys_stats.num_current_io_connections) + CarRet +
        'OpenTCPConnections' + CarRet + IntToStr(sys_stats.num_current_tcp_connections) + CarRet +
        'SentIOPkts' + CarRet + IntToStr(sys_stats.num_successful_sent_io_messages) + CarRet +
        'RecvIOPkts' + CarRet + IntToStr(sys_stats.num_successful_recv_io_messages) + CarRet +
        'FailedSentIOPkts' + CarRet + IntToStr(sys_stats.num_failed_sent_io_messages) + CarRet +
        'FailedRecvIOPkts' + CarRet + IntToStr(sys_stats.num_failed_recv_io_messages) + CarRet +
        'FailedConnections' + CarRet + IntToStr(sys_stats.num_failed_connections) + CarRet +
        'TimedOutConnections' + CarRet + IntToStr(sys_stats.num_timed_out_connections) + CarRet +
        'Enabled' + CarRet + IntToStr(Named.GetAsInteger(EthIpEnabledTag));

    end;
  else
  end;
end;

constructor TAcnc32EthernetIP.Create;
var
  wide_chars : array[0..2047] of WideChar;

begin
  EtIPLibAdapter := nil;
  //Set DLL Search path for this module...
  DllSearchPath := DllProfilePath + '..\lib'; //Not sure why dllprofilepath is hardcoded to ...\acnc32\profile\Live in cnc32.pas

  StringToWideChar(DllSearchPath, @wide_chars, Length(wide_chars)); //Marshall pascal data to win32.
  SetDllDirectory(wide_chars); //call win32 api hook to set application specific dll search path.

end;

function TAcnc32EthernetIP.GetOutputArea: Pointer;
begin
  Result:= @FOutputArea;  //Pointer to starting address of FOutputArea array.
end;

procedure TAcnc32EthernetIP.Shutdown;
var
  shutdown_result :Integer;
  shutdown_msg :string;
  eventlog_level :Word;
begin
  eventlog_level := EVENTLOG_INFORMATION_TYPE;
  Named.EventLog('EthernetIP IO System Shutting down...', eventlog_level);
  shutdown_result := EtIPLibAdapter.Shutdown();

  if (shutdown_result=0) then
  begin
    shutdown_msg := 'EthernetIP cleanly shutdown.';
    eventlog_level := EVENTLOG_INFORMATION_TYPE;
  end else begin
    shutdown_msg := 'Error during EthernetIP IO System shutdown.';
    eventlog_level := EVENTLOG_ERROR_TYPE;
  end;

  Named.EventLog(shutdown_msg, eventlog_level);
end;

function TAcnc32EthernetIP.GetInputSize: Integer;
begin
  Result:= FInputSize;
end;

procedure TAcnc32EthernetIP.Reset;
begin
  Named.EventLog('EthernetIP Reset');
end;

function TAcnc32EthernetIP.GetInputArea: Pointer;
begin
  Result:= @FInputArea; //Pointer to starting address of FInputArea array.
end;

function TAcnc32EthernetIP.TextDefinition: WideString;
begin
  Result:= FTextDef;
end;

function TAcnc32EthernetIP.Healthy: Boolean;
begin
  Result:= IsEthIpHealthy;
end;

function TAcnc32EthernetIP.LastError: WideString;
begin
  Result:= EthIPErrorText;
end;

procedure TAcnc32EthernetIP.Initialise(Host: IIOSystemAccessorHost; const Params: WideString);
var
  params_working_copy: string;
  ei_ip_address: string;

  product_name: string;
  error_msg: string;

  input_size_from_params: string;
  output_size_from_params: string;

begin
  //Self.Host:= Host;

  params_working_copy:= Params;

  //Since I haven't had any luck in figuring out how to pass dynamic arrays to C,
  //I'm removing the input/output size configuration values and just statically
  //setting the input and output buffers.

  //FInputSize:= StrToInt(CncTypes.ReadDelimited(Tmp, ';'));
  //FOutputSize:= StrToInt(CncTypes.ReadDelimited(Tmp, ';'));
  input_size_from_params  := CncTypes.ReadDelimited(params_working_copy, ';');
  output_size_from_params := CncTypes.ReadDelimited(params_working_copy, ';');
  //CncTypes.ReadDelimited(params_working_copy, ';');
  //CncTypes.ReadDelimited(params_working_copy, ';');
  product_name:= CncTypes.ReadDelimited(params_working_copy, ';');

  FInputSize := Length(FInputArea);
  FOutputSize := Length(FOutputArea);

  //This may seem a little odd but testing shows some versions of acnc32 have contractual obligations in the plugin interface
  //to have two numbers following the module name (ie. ethip; 256; 256). Here we up hold that agreement while enforcing our own.
  if (IntToStr(IO_BUFFER_SIZE) <> input_size_from_params) then
  begin
    error_msg := Format('Ethernet/IP Broker Error: Input buffer must be set to %d (currently %s). Please set this value in IOmap.ini.', [IO_BUFFER_SIZE, input_size_from_params]);
    HandleError(error_msg, true, true);
  end;

  if (IntToStr(IO_BUFFER_SIZE) <> output_size_from_params) then
  begin
    error_msg := Format('Ethernet/IP Broker Error: Output buffer must be set to %d (currently %s). Please set this value in IOmap.ini.', [IO_BUFFER_SIZE, output_size_from_params]);
    HandleError(error_msg, true, true);
  end;

  ei_ip_address := CncTypes.ReadDelimited(params_working_copy, ';');

  if CompareText('<disabled>', ei_ip_address)=0 then
  begin
    Exit;
  end;

  EtIPLibAdapter:= TEthernetIPProtocol.Create(DllSearchPath + '\CncEtIPAdapter.dll');

  Self.EthIpErrorCode := EtIPLibAdapter.Initialize(PChar(ei_ip_address), PChar(product_name));

  if (Self.EthIpErrorCode <> 0) then
  begin
    error_msg := 'Ethernet/IP initialization error. Ethernet/IP communication is un-available.';
    HandleError(error_msg, true, true);
  end
  else //No errors so far? Assume everything is good...
  begin
    IsEthIpHealthy := True;
    AddPublicEthIPTags();
    Host.AddPage('Ethernet/IP');
  end;


end;

procedure TAcnc32EthernetIP.AddPublicEthIPTags();
begin

  //ResetTag:= Named.AddTag('EthIPReset', 'TIntegerTag');
  EthIpEnabledTag := Named.AddTag('EthIpEnabled', 'TIntegerTag');
  //Default Ethernet/IP to the active state
  Named.SetAsInteger(EthIpEnabledTag, 1);
  //Named.SetAsInteger(ResetTag, 0);

end;

procedure TAcnc32EthernetIP.HandleError(errorMsg: string; isFault: Boolean; isException: Boolean);
var
  eip_lib_last_error : PChar;
begin
  IsEthIpHealthy := False;

  EthIPErrorText := 'EthIP.dll: ' + errorMsg;
  if (EtIPLibAdapter <> nil) then
  begin
      eip_lib_last_error := EtIPLibAdapter.GetCncEtLibStatus().error_msg;
      EthIPErrorText := Format('EthIP.dll: %s (Ethernet/IP library last error: %s)', [errorMsg, eip_lib_last_error]);
  end;

  if (isFault) then
  begin
    Named.SetFault('', @EthIPErrorText, faultFatal);
    Named.EventLog(EthIPErrorText, EVENTLOG_ERROR_TYPE);
  end;

  if (isException) then
  begin
    raise Exception.Create(EthIPErrorText);
  end;

  Named.EventLog(EthIPErrorText, EVENTLOG_WARNING_TYPE);
end;

procedure TAcnc32EthernetIP.UpdateRealToShadow();
//var
var
  bytes_copied : Cardinal;
begin
///"Shadow" refers to Acnc32's image of IO (or what is happening in the real world).
//likewise, "Real" refers to the physical IO or in this case Ethernet/IP's input buffer/assembly.
//(information inputed to the broker from physical IO)

//In this case, we need to populate Acnc32's "input buffer" with information shared from "Physical IO" or
//the orginator's output assembly via Ethernet/IP.

  if (Named.GetAsInteger(EthIpEnabledTag) <> 0) then
  begin
    bytes_copied := EtIPLibAdapter.GetInputAssembly(@FInputArea, FInputSize);
    if ( bytes_copied <> FInputSize) then
    begin
      HandleError(Format('Error copying InputAssembly. Expected to copy %d bytes but only copied %d.', [FInputSize, bytes_copied]), true, true);
    end;
  end;

end;

procedure TAcnc32EthernetIP.UpdateShadowToReal();
var
  bytes_copied : Cardinal;
begin
//"Shadow" refers to Acnc32's image of IO (or what is happening in the real world).
//likewise, "Real" refers to the physical IO or in this case Ethernet/IP's output buffer/assembly.

//We need to:
// 1. Update Acnc32's Output buffer/area with values internal to Acnc32 (expose acnc32's state to the "outside").
// 2. Overwrite or piecewise copy Acnc32's Output buffer to Ethernet/IP's output buffer.
// 3. Inform Ethernet/IP new data is available. (ie. Read from the network explicitly or have a thread auto poll/posix select).

//CopyMemory(FromEthernetIPOutputBuffer, Acnc32LocalOutputBuffer)

//Simulate info changing around in Acnc32 and writing that to the output buffer.
//FOutputArea[0] := Random(255);
//FOutputArea[1] := Random(255);
//FOutputArea[2] := Random(255);
//FOutputArea[3] := Random(255);
//FOutputArea[4] := Random(255);
//FOutputArea[5] := Random(255);


if (Named.GetAsInteger(EthIpEnabledTag) <> 0) then
  begin
    bytes_copied := EtIPLibAdapter.SetOutputAssembly(@FOutputArea, FOutputSize);
    if (bytes_copied <> FOutputSize) then
    begin
       HandleError(Format('Error copying into OutputAssembly. Expected to copy %d bytes but only copied %d.', [FOutputSize, bytes_copied]), true, true);
    end;
  end;

end;

initialization
//Must declare Ethip.DLL in cnc.ini
//Must declare IO System and "Device" in IOMap.ini
//See cnc.ini and IOMap for details.

  TAbstractIOSystemAccessor.AddIOSystemAccessor('EthIP', TAcnc32EthernetIP);
end.
