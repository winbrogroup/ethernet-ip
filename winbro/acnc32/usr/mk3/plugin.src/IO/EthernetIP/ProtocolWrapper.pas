unit ProtocolWrapper;
//Wrapper to "C" OpenerLibrary.dll

interface

uses Windows, Classes, SysUtils, SyncObjs, ExtCtrls;

type
  PTCncEtLibStatus = ^TCncEtLibStatus;
  TCncEtLibStatus = record
  error_msg: PChar;
  claimed_ip_address: PChar;
  network_status_description: PChar;
  claimed_ip_address_num: PInteger;
  product_name: PChar;
end;

type
  PTCncEtLibVersionInfo = ^TCncEtLibVersionInfo;
  TCncEtLibVersionInfo = record
  version: PChar;
  timestamp: PChar;
end;

type
TEtLibConnectionInfo = record
  sent_packets:  LongWord;                  // Number of sent packets
  recv_packets: LongWord;                   // Number of received packets
  max_observed_send_delay_in_ms: LongWord;  // The maximum delay between any two consecutive send packets
  max_observed_recv_delay_in_ms: LongWord;  // The maximum delay between any two consecutive receive packets
  lost_packets: LongWord;                   // The number of lost packets
  num_packets_failed_to_send: Word;         // Number of I/O packets that failed to send
  num_recv_packets_in_error: Word;          // Number of received I/O packets that resulted in error
  num_bytes_sent: LongWord;                 // Number of sent bytes
  num_bytes_recv: LongWord;                 // Number of received bytes
end;

TEtLibSystemStats = record //Please see EtIpApi.h for additional comments.
  max_active_io_connections: Word;
  num_current_io_connections: Word;
  max_active_explicit_connections: Word;
  num_current_explicit_connections: Word;
  num_failed_connections: Word;
  num_timed_out_connections: Word;
  num_tcp_connections_used: Word;
  num_max_tcp_connections_used: Word;
  num_current_tcp_connections: Word;
  num_successful_sent_io_messages: LongWord;
  num_successful_recv_io_messages: LongWord;
  num_failed_sent_io_messages: Word;
  num_failed_recv_io_messages: Word;
  num_sent_class3_messages: LongWord;
  num_recv_class3_messages: LongWord;
  num_sent_ucmm_messages: LongWord;
  num_recv_ucmm_messages: LongWord;
end;


type
  TInitialize = function(ip_address: PChar; product_name: PChar): Integer; cdecl;
  TSetOutputAssembly = function(new_output: PChar; size: Integer): Integer; cdecl;
  TGetInputAssembly = function(input_assembly: PChar; size: Integer): Integer; cdecl;
  TGetEtLibSystemStats = function(sys_stats: PChar): Integer; cdecl;
  TGetCncEtLibStatus = function(): PTCncEtLibStatus; cdecl;
  TGetCncEtLibVersionInfo = function(): PTCncEtLibVersionInfo; cdecl;
  TShutdown = function: Integer; cdecl;

  TEthernetIPProtocol = class
  private
    FInitialize: TInitialize;
    FSetOutputAssembly: TSetOutputAssembly;
    FGetInputAssembly: TGetInputAssembly;
    FGetEtLibSystemStats: TGetEtLibSystemStats;
    FGetCncEtLibStatus: TGetCncEtLibStatus;
    FGetCncEtLibVersionInfo: TGetCncEtLibVersionInfo;
    FShutdown: TShutdown;

  public
    constructor Create(Libname: string);
    function Initialize(ip_address: PChar; product_name: PChar): Integer; cdecl;
    function SetOutputAssembly(new_output: PChar; size: Integer): Integer; cdecl;
    function GetInputAssembly(input_assembly: PChar; size: Integer): Integer; cdecl;
    function GetEtLibSystemStats(system_stats : PChar): Integer; cdecl;
    function GetCncEtLibStatus(): PTCncEtLibStatus; cdecl;
    function GetCncEtLibVersionInfo(): PTCncEtLibVersionInfo; cdecl;
    function Shutdown: Integer; cdecl;
  end;

implementation

{ TEthernetIPProtocol }

{ TEthernetIPProtocol }

constructor TEthernetIPProtocol.Create(Libname: string);
var HMod : HMODULE;

  function GetProcAddressX(hModule: HMODULE; lpProcName: LPCSTR): FARPROC;
  begin
    Result := GetProcAddress(hModule, lpProcName);
    if not Assigned(Result) then
      raise Exception.CreateFmt('Library interface missing [%s]', [lpProcName]);
  end;

begin
  HMod := LoadLibrary(PChar(LibName));
  if HMod = 0 then
    raise Exception.CreateFmt('Library [%s] not found', [LibName]);

  FInitialize:= GetProcAddressX(HMod, 'Initialize');
  FSetOutputAssembly := GetProcAddressX(HMod, 'SetOutputAssembly');
  FGetInputAssembly := GetProcAddressX(HMod, 'GetInputAssembly');
  FGetEtLibSystemStats := GetProcAddressX(HMod, 'GetEtLibSystemStats');
  FGetCncEtLibStatus := GetProcAddressX(HMod, 'GetCncEtLibStatus');
  FGetCncEtLibVersionInfo := GetProcAddressX(HMod, 'GetCncEtLibVersionInfo');
  FShutdown:= GetProcAddressX(HMod, 'Shutdown');

end;

function TEthernetIPProtocol.Initialize(ip_address: PChar; product_name: PChar): Integer;
begin
  Result:= FInitialize(ip_address, product_name);
end;

function TEthernetIPProtocol.SetOutputAssembly(new_output: PChar; size: Integer): Integer;
begin
  Result := FSetOutputAssembly(new_output, size);
end;

function TEthernetIPProtocol.GetInputAssembly(input_assembly: PChar; size: Integer): Integer;
begin
  Result := FGetInputAssembly(input_assembly, size);
end;

function TEthernetIPProtocol.GetEtLibSystemStats(system_stats: PChar): Integer;
begin
  Result := FGetEtLibSystemStats(PChar(system_stats));
end;

function TEthernetIPProtocol.GetCncEtLibStatus(): PTCncEtLibStatus;
begin
  Result := FGetCncEtLibStatus();
end;

function TEthernetIPProtocol.GetCncEtLibVersionInfo(): PTCncEtLibVersionInfo;
begin
  Result := FGetCncEtLibVersionInfo();
end;

function TEthernetIPProtocol.Shutdown: Integer;
begin
  Result:= FShutdown;
end;

end.
