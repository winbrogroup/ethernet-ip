object Form3: TForm3
  Left = 0
  Top = 0
  Width = 694
  Height = 253
  Caption = 'Test CIF'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 64
    Width = 225
    Height = 25
    AutoSize = False
    Caption = 'Input data'
  end
  object BtnRead: TButton
    Left = 48
    Top = 24
    Width = 75
    Height = 25
    Caption = 'Read'
    Enabled = False
    TabOrder = 0
    OnClick = BtnReadClick
  end
  object BtnWrite: TButton
    Left = 144
    Top = 24
    Width = 75
    Height = 25
    Caption = 'Write'
    Enabled = False
    TabOrder = 1
    OnClick = BtnWriteClick
  end
  object LoggingBox: TListBox
    Left = 288
    Top = 0
    Width = 398
    Height = 219
    Align = alRight
    ItemHeight = 13
    TabOrder = 2
  end
end
