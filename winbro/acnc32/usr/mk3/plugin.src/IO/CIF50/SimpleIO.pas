unit SimpleIO;

interface

uses Classes, CNCTypes, INamedInterface, Hilscher, SysUtils, NamedPlugin,
     AbstractIOSystemAccessor, AcncLogger;

type
  SimpleHilscherIOSubSystem = class(TAbstractIOSystemAccessor)
  private
    Hdb : THilscherIO_DLL;
    DeviceOK : Boolean;
    LastDeviceError: Boolean;

    Host: IIOSystemAccessorHost;

    FInputArea: array [0..511] of Byte;
    FOutputArea: array [0..511] of Byte;

    procedure DoError(aText : string; Level : THilscherError);
  public
    constructor Create; override;
    procedure Initialise(Host: IIOSystemAccessorHost; const Params : WideString); override; stdcall;
    destructor Destroy; override;
    procedure UpdateShadowToReal; override;
    procedure UpdateRealToShadow; override;
    function Healthy: Boolean; override;
    procedure Reset; override;
    function LastError: WideString; override;
    function GetInfoPageContent(aPage : Integer) : WideString; override;

    function GetInputArea: Pointer; override;
    function GetOutputArea: Pointer; override;
    function GetInputSize: Integer; override;
    function GetOutputSize: Integer; override;
    function TextDefinition: WideString; override;
    procedure Shutdown; override;
  end;

implementation

{ HilscherIOSubSystem }

function SimpleHilscherIOSubSystem.GetOutputSize: Integer;
begin
  Result:= 4;
end;

function SimpleHilscherIOSubSystem.GetInfoPageContent(aPage: Integer): WideString;
begin
  Result:= '';
end;

constructor SimpleHilscherIOSubSystem.Create;
begin
  inherited;

end;

function SimpleHilscherIOSubSystem.GetOutputArea: Pointer;
begin
  Result:= @FOutputArea;
end;

procedure SimpleHilscherIOSubSystem.Shutdown;
begin
  inherited;

end;

function SimpleHilscherIOSubSystem.GetInputSize: Integer;
begin
  Result:= 4;
end;

procedure SimpleHilscherIOSubSystem.Reset;
begin
  if not DeviceOK then begin
    Hdb.Reset(hrtWarmStart);
    Sleep(2000);
    DeviceOK := True;
  end;
end;

function SimpleHilscherIOSubSystem.GetInputArea: Pointer;
begin
  Result:= @FInputArea;
end;

function SimpleHilscherIOSubSystem.TextDefinition: WideString;
begin
  Result:= '';
end;

function SimpleHilscherIOSubSystem.Healthy: Boolean;
begin
  Result:= DeviceOk;
end;

function SimpleHilscherIOSubSystem.LastError: WideString;
begin
  Result:= '';
end;

destructor SimpleHilscherIOSubSystem.Destroy;
begin
  HDb.Free;
  inherited;
end;

procedure SimpleHilscherIOSubSystem.Initialise(Host: IIOSystemAccessorHost;
  const Params: WideString);
begin
  try
    HDb := THilscherIO_DLL.Create(TLogger.Create, hdb1, False);
    HDb.OnErrorEvent := DoError;
  except
    on E : Exception do begin
      raise ECNCInstallFault.CreateFmt('Hilscher Driver failed [%s]', [E.Message]);
    end;
  end;
end;

procedure SimpleHilscherIOSubSystem.UpdateRealToShadow;
begin
  if Healthy then
    HDb.ReadFromDevice(@FInputArea, 4);
end;

procedure SimpleHilscherIOSubSystem.UpdateShadowToReal;
begin
  if Healthy then
    HDb.WriteToDevice(@FOutputArea, 4);

  if HDb.DeviceError <> LastDeviceError then
    Named.SetFaultA(ClassName, 'bHostFlags error bit set', faultEstop, nil);

  LastDeviceError := HDb.DeviceError;
end;

procedure SimpleHilscherIOSubSystem.DoError(aText: string; Level: THilscherError);
begin
  DeviceOK:= Level = heNone;
  if Level <> heNone then
    Named.EventLog('IO Fault: ' + aText);
end;

initialization
  SimpleHilscherIOSubSystem.AddIOSystemAccessor('Simple', SimpleHilscherIOSubSystem);
end.
