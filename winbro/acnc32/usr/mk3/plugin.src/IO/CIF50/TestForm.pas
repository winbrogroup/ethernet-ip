unit TestForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Hilscher;

type
  TForm3 = class(TForm)
    BtnRead: TButton;
    Label1: TLabel;
    BtnWrite: TButton;
    LoggingBox: TListBox;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BtnWriteClick(Sender: TObject);
    procedure BtnReadClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    Dll: THilscherIO_DLL;
  public
    { Public declarations }
    procedure ErrorEvent(Text: string; Level: THilscherError);
  end;

  TLogger = class (TLoggingDevice)
    function GetIOSweep: Int64; override;
    procedure EventLog(Value: WideString); override;
  end;

  TWriteThread = class(TThread)
    procedure Execute; override;
  end;

  TReadThread = class(TThread)
    procedure Execute; override;
  end;
var
  Form3: TForm3;

implementation

{$R *.dfm}

procedure TForm3.FormCreate(Sender: TObject);
begin
  Dll:= THilscherIO_DLL.Create(TLogger.Create, hdb1, True);
  BtnRead.Enabled:= True;
  BtnWrite.Enabled:= True;
end;

{ TLogger }

function TLogger.GetIOSweep: Int64;
begin
  Result:= Windows.GetTickCount;
end;

procedure TLogger.EventLog(Value: WideString);
begin
  Form3.LoggingBox.AddItem(Value, nil);
end;

procedure TForm3.BtnReadClick(Sender: TObject);
var Buffer: array[0..255] of Byte;
    I: Integer;
    Tmp: string;
begin
  Dll.ReadFromDevice(@Buffer, 16);
  Dll.OnErrorEvent:= ErrorEvent;

  Tmp:= '';
  for I:= 0 to 15 do begin
    Tmp:= Tmp + IntToStr(Buffer[I]);
    Tmp:= Tmp + ' ';
  end;
  Label1.Caption:= Tmp;

  TReadThread.Create(False);
end;

procedure TForm3.BtnWriteClick(Sender: TObject);
var T: TWriteThread;
begin
  T:= TWriteThread.Create(False);
end;

procedure TForm3.FormClose(Sender: TObject; var Action: TCloseAction);
begin
   if Assigned(Dll) then
     Dll.Free;
end;

procedure TForm3.ErrorEvent(Text: string; Level: THilscherError);
begin
  Self.LoggingBox.AddItem(Text, nil);
end;

{ TWriteThread }

procedure TWriteThread.Execute;
const Buffer: array [0..15] of Byte = ( $55, $55, $55, $55, $ff, $ff, $ff, $ff, $ff, $ff, $ff, $ff, $ff, $ff, $ff, $ff );
begin
  Form3.Dll.WriteToDevice(@Buffer, 16);
end;

{ TReadThread }

procedure TReadThread.Execute;
var Buffer: array[0..15] of Byte;
Tmp: string; I: Integer;
begin
  while(true) do begin
    Form3.Dll.ReadFromDevice(@Buffer, 16);
    Sleep(50);
  Tmp:= '';
  for I:= 0 to 15 do begin
    Tmp:= Tmp + IntToStr(Buffer[I]);
    Tmp:= Tmp + ' ';
  end;
  Form3.Label1.Caption:= Tmp;
  end;
end;

end.
