unit Hilscher;

interface

uses Windows, SysUtils, Classes; //, NamedPlugin;

type
  TLoggingDevice = class
    function GetIOSweep: Int64; virtual; abstract;
    procedure EventLog(Value: WideString); virtual; abstract;
    property IOSweep: int64 read GetIOSweep;
  end;

  THilscherError = (
    heNone,
    heWarning,
    heSevere,
    heCritical
  );

  THilscherErrorEvent = procedure(aText : string; Level : THilscherError) of Object;
  THilscherResetType = (
    hrtColdStart,
    hrtWarmStart,
    hrtBootStart
  );

  {$Z4}
  THilscherDevBoard = (
    hdb1,
    hdb2,
    hdb3,
    hdb4,
    hdbInvalid
  );

  THilscherBoard = packed record
    usNumber : Word;
    usAvailable : Word;
    usPhysicalBoardAddress : Cardinal;
    usIrqNumber : Word;
  end;

  THilscherBoardInfo = packed record
    abDriverVersion : array[0..15] of Char;
    Boards : array [hdb1..hdb4] of THilscherBoard;
  end;

{   else if ( (sRet = DevExtendedData(usBoardNumber, 1, 4, "SMSI") ) != DRV_NO_ERROR)
    printf( "SMSI failure        RetWert = %5d \n", sRet ); }

  PHilscherDriverInfo = ^THilscherDriverInfo;  // DevGetInfo(1)
  THilscherDriverInfo = packed record
    ulOpenCnt : Cardinal;
    ulCloseCnt : Cardinal;
    ulReadCnt : Cardinal;
    ulWriteCnt : Cardinal;
    ulIRQCnt : Cardinal;
    bInitMsgFlag : Byte;
    bReadMsgFlag : Byte;
    bWriteMsgFlag : Byte;
    bLastFunction : Byte;
    bWriteState : Byte;
    bReadState : Byte;
    bHostFlags : Byte;
    bMyDevFlags : Byte;
    bExIOFlag : Byte;
    ulExIOCnt : Cardinal;
  end;

  PHilscherBoardVersionInfo = ^THilscherBoardVersionInfo;  // DevGetInfo(2)
  THilscherBoardVersionInfo = packed record
    ulDate : Cardinal;
    ulDeviceNo : Cardinal;
    ulSerialNumber : Cardinal;
    ulReserved : Cardinal;
    abPcOsName0 : array [0..3] of Char;
    abPcOsName1 : array [0..3] of Char;
    abPcOsName2 : array [0..3] of Char;
    abOEMIdentifier : array [0..3] of Char;
  end;

  PHilscherFirmwareInfo = ^THilscherFirmwareInfo;  // DevGetInfo(3)
  THilscherFirmwareInfo = packed record
    abFirmwardName : array [0..15] of Char;
    abFirmwareVersion : array [0..15] of Char;
  end;

  PHilscherTaskInfo = ^THilscherTaskInfo;
  THilscherTaskInfo = packed record
    abTaskName : array [0..7] of Char;
    usTaskVersion : Word;
    bTaskCondition : Byte;
    abReserved : array [0..4] of Byte;
  end;

  PAHilscherTaskInfo = ^AHilscherTaskInfo; // DevGetInfo(4)
  AHilscherTaskInfo = array [0..6] of THilscherTaskInfo;

  PHilscherRCSInfo = ^THilscherRCSInfo;  // DevGetInfo(5)
  THilscherRCSInfo = packed record
    usRcsVersion : Word;
    bRcsError : Byte;
    bHostWatchDog : Byte;
    bDevWatchDog : Byte;
    bSegmentCount : Byte;
    bDeviceAddress : Byte;
    bDriverType : Byte;
  end;

  PHilscherDeviceInfo = ^THilscherDeviceInfo; // DevGetInfo(6)
  THilscherDeviceInfo = packed record
    bDpmSize : Byte;
    bDevType : Byte;
    bDevModel : Byte;
    abDevIndentifier : array [0..2] of Char;
  end;

  PHilscherIOInfo = ^THilscherIOInfo; // DevGetInfo(7)
  THilscherIOInfo = packed record
    bComBit : Byte;
    bIOExchangeMode : Byte;
    ulExchangeCnt : Cardinal;
  end;

  PHilscherMsg = ^THilscherMsg;
  THilscherMsg = packed record
    rx : byte;
    tx : byte;
    ln : byte;
    nr : byte;
    a : byte;
    f : byte;
    b : byte;
    e : byte;
    data : array[0..254] of byte;
    dummy : array[0..24] of byte;
  end;

  PHilscherTaskState = ^THilscherTaskState;
  THilscherTaskState = packed record
    abTaskState : array [0..63] of Byte;
  end;

  TGlobalBit = (
    bCtrl,
    bAClr,
    bNonExch,
    bPrh1Err,
    bEvent,
    bNRdy,
    bI1Err,
    bI2Err
  );

  TGlobalBits = set of TGlobalBit;

  PCommState = ^TCommState;
  TCommState = packed record
    usMode: Word;
    usStateFlag: Word;
    bGlobalBits: TGlobalBits;
    bIBM_State: Byte;
    bErr_Dev_Adr: Byte;
    bErr_Event: Byte;
    usNumOfDefectiveDataCycles: Word;
    usNumOfNetworkReinits: Word;
    abReserved: array [0..7] of Byte;
    abS1_Cfg: array [0..15] of Byte;
    abS1_State: array [0..15] of Byte;
    abS1_diag: array [0..15] of Byte;
  end;

  THilscherDrvResult = ( // Subract result from zero use
    hdrOK,
    hdrNotInitialized,
    hdrInitStateError,
    hdrReadStateError,
    hdrCommandActive,
    hdrParameterUnknown,
    hdrWrongDriverVersion,
    hdrPCISetConfigMode,
    hdrPCIReadConfigLength,
    hdrPCISetRunMode,
    hdrDpmAccessError,
    hdrNotReady,
    hdrNotRunning,
    hdrWatchDogFailed,
    hdrOsVersionError,
    hdrSysError,
    hdrMailboxFull,
    hdrPutTimeout,
    hdrGetTimeout,
    hdrGetNoMessage,
    hdrResetTimeout,
    hdrNoComFlag,
    hdrExchangeFailed,
    hdrExchangeTimeout,
    hdrComModeUnknown,
    hdrFunctionFailed,
    hdrDpmSizeMismatch,
    hdrStateModeUnknown
  );

  THilscherDrvUsrResult = ( // Subtract result from -30
    hdrUsrOpenError,
    hdrUsrInitDrvError,
    hdrUsrComError,
    hdrUsrDevNumberInvalid,
    hdrUsrInfoAreaInvalid,
    hdrUsrNumberInvalid,
    hdrUsrModeInvalid,
    hdrUsrMsgBufNullPointer,
    hdrUsrMsgBufTooShort,
    hdrUsrSizeInvalid,
    hdrUsrNOT_USED,
    hdrUsrSizeZero,
    hdrUsrSizeTooLong,
    hdrUsrDevPointerNull,
    hdrUsrBufPointerNull,
    hdrUsrTxSizeTooLong,
    hdrUsrRxSizeTooLong,
    hdrUsrTxBufPointerNull,
    hdrUsrRxBufPointerNull
  );

  THilscherDrvFileResult = ( // Subtract result from -100
    hdrFileOpenFailed,
    hdrFileSizeZero,
    hdrFileNoMemory,
    hdrFileReadFailed,
    hdrFileInvalidFileType,
    hdrFileFileNameInvalid
  );

  THlchRetVal = Integer;
  THilscher_I = function(Dev : THilscherDevBoard) : THlchRetVal; stdcall;
  THilscherWP_I = function (Dev : THilscherDevBoard; B : Word; P : Pointer) : THlchRetVal; stdcall;
  THilscherP_I = function (Dev : THilscherDevBoard; P : Pointer) : THlchRetVal; stdcall;
  THilscherWWP_I = function (Dev : THilscherDevBoard; B, C : Word; P : Pointer) : THlchRetVal; stdcall;
  THilscherW_I = function(Dev : THilscherDevBoard; B : Word) : THlchRetVal; stdcall;
  THilscherWI_I = function(Dev : THilscherDevBoard; B : Word; I : Integer) : THlchRetVal; stdcall;
  THilscherPP_I = function(Dev : THilscherDevBoard; P1, P2 : Pointer) : THlchRetVal; stdcall;
  THilscherWW_I = function(Dev : THilscherDevBoard; B, C : Word) : Integer; stdcall;
  THilscherPI_I = function(Dev : THilscherDevBoard; M : Pointer; B : Integer) : THlchRetVal; stdcall;
  THilscherWPI_I = function(Dev : THilscherDevBoard; B : Word; M : Pointer; C : Integer) : THlchRetVal; stdcall;
  THilscherWWWP_I = function(Dev : THilscherDevBoard; B, C, D : Word; P : Pointer) : THlchRetVal; stdcall;
  TDevExchangeIO = function(Dev : THilscherDevBoard; OOff, OSz : Word; ODat : Pointer; IOff, ISz : Word; IDat : Pointer; TmOut : Integer) : THlchRetVal; stdcall;
  TDevExchangeIOErr = function(Dev : THilscherDevBoard; OOff, OSz : Word; ODat : Pointer; IOff, ISz : Word; IDat, ComState : Pointer; TmOut : Integer) : THlchRetVal; stdcall;
  TDevExtendedData = function(Dev : THilscherDevBoard; A, B : Integer; Inst : PChar) : Integer;

  THilscherIO_DLL = class(TObject)
  private
    FDevice : THilscherDevBoard;
    LibCode : Integer;
    Initialised : Boolean;
    FCommState: TCommState;
    FRetryRate: Integer;

    NoReset: Boolean; // Prevent reset
    _RetryCount: Integer;
    _RetryTime: Int64;
    _RetryLastCount: Integer;
    FOnErrorEvent : THilscherErrorEvent;
    FIOTimeOut : Double;
    DevOpenDriver : THilscher_I;
    DevCloseDriver : THilscher_I;
    DevGetBoardInfo : THilscherWP_I;
    DevInitBoard : THilscherP_I;
    DevExitBoard : THilscher_I;
//    DevPutTaskParameter : THilscherWWP_I;
//    DevGetTaskParameter : THilscherWWP_I;
    DevReset : THilscherWW_I;
//    DevSetHostState : THilscherWI_I;
//    DevGetMBXState : THilscherPP_I;
//    DevPutMessage : THilscherPI_I;
//    DevGetMessage : THilscherWPI_I;
//    DevGetTaskState : THilscherWWP_I;
    DevGetInfo : THilscherWWP_I;
//    DevTriggerWatchDog : THilscherWP_I;
    DevExchangeIO : TDevExchangeIO;
    DevExchangeIOErr : TDevExchangeIOErr;
    DevReadWriteDPMRaw : THilscherWWWP_I;
    DevExtendedData : TDevExtendedData;

{    FDriverInfo : THilscherDriverInfo;
    FBoardVersionInfo : THilscherBoardVersionInfo;
    FFirmwareVersionInfo : THilscherFirmwareInfo;
    FTaskInfo : THilscherTaskInfo;
    FRCSInfo : THilscherRCSInfo;
    FDeviceInfo : THilscherDeviceInfo;
    FIOInfo : THilscherIOInfo; }

    Logger: TLoggingDevice;

    function GetProcAddressX(hModule: HMODULE; lpProcName: LPCSTR): FARPROC;
    function GetBoardInfo : THilscherBoardInfo;
    function Error(aText : string; Ret : Integer; Level : THilscherError) : string;
    procedure NoError;
    function GetDriverInfo : THilscherDriverInfo;
    function GetBoardVersionInfo : THilscherBoardVersionInfo;
    function GetFirmwareInfo : THilscherFirmwareInfo;
    function GetTaskInfo : THilscherTaskInfo;
    function GetRCSInfo : THilscherRcsInfo;
    function GetDeviceInfo : THilscherDeviceInfo;
    function GetIOInfo : THilscherIOInfo;
  public
    DeviceError: Boolean;
    constructor Create(ALogging: TLoggingDevice; aDevice : THilscherDevBoard; NoReset: Boolean = false);
    destructor Destroy; override;
    procedure Reset(ResetType : THilscherResetType);
    procedure ReadFromDevice(Data : Pointer; Size : Cardinal);
    procedure WriteToDevice(Data : Pointer; Size : Cardinal);
    property Device : THilscherDevBoard read FDevice;
    property BoardInfo : THilscherBoardInfo read GetBoardInfo;
    property DriverInfo : THilscherDriverInfo read GetDriverInfo;
    property BoardVersionInfo : THilscherBoardVersionInfo read GetBoardVersionInfo;
    property FirmwareVersionInfo : THilscherFirmwareInfo read GetFirmwareInfo;
    property TaskInfo : THilscherTaskInfo read GetTaskInfo;
    property RCSInfo : THilscherRCSInfo read GetRCSInfo;
    property DeviceInfo : THilscherDeviceInfo read GetDeviceInfo;
    property IOInfo : THilscherIOInfo read GetIOInfo;

    property OnErrorEvent : THilscherErrorEvent read FOnErrorEvent write FOnErrorEvent;
    property IOTimeOut : Double read FIOTimeOut write FIOTimeOut;
    property CommState: TCommState read FCommState;
    property RetryRate: Integer read FRetryRate;
  end;

implementation

const
   HilscherLibrary = 'cif32dll.dll';

resourcestring
   HilscherUnableToOpen = 'Unable to open hilscher device [Error %d]';
   HilcherUnableToInitDevice = 'Unable to initialize hilscher device [Error %d]';
   HilscherlibraryNotFound = 'Unable to open ' + HilscherLibrary;
   HilscherLibraryFunction = '[%s] not found in library';

function THilscherIO_DLL.GetProcAddressX(hModule: HMODULE; lpProcName: LPCSTR): FARPROC;
begin
  Result := GetProcAddress(hModule, lpProcName);
  if not Assigned(Result) then
    raise Exception.CreateFmt(HilscherLibraryFunction, [lpProcName]);
end;

constructor THilscherIO_DLL.Create(ALogging: TLoggingDevice; aDevice : THilscherDevBoard; NoReset: Boolean = False);
var Ret : Integer;
begin
  inherited Create;
  Logger:= ALogging;
  Logger.EventLog('Creating Stub');
  Self.NoReset:= NoReset;
  FDevice := hdbInvalid;
  Logger.EventLog('++ Loading DLL');
  LibCode := LoadLibrary(HilscherLibrary);
  if LibCode = 0 then
    raise Exception.Create(HilscherlibraryNotFound);
  Logger.EventLog('-- Loading DLL');

  Logger.EventLog('++ Mapping function');
  DevOpenDriver       := GetProcAddressX(LibCode, 'DevOpenDriver');
  DevCloseDriver      := GetProcAddressX(LibCode, 'DevCloseDriver');
  DevGetBoardInfo     := GetProcAddressX(LibCode, 'DevGetBoardInfo');
  DevInitBoard        := GetProcAddressX(LibCode, 'DevInitBoard');
  DevExitBoard        := GetProcAddressX(LibCode, 'DevExitBoard');
  DevReset            := GetProcAddressX(LibCode, 'DevReset');
  DevExchangeIO       := GetProcAddressX(LibCode, 'DevExchangeIO');
  DevExchangeIOErr    := GetProcAddressX(LibCode, 'DevExchangeIOErr');
  DevReadWriteDPMRaw  := GetProcAddressX(LibCode, 'DevReadWriteDPMRaw');
  DevGetInfo          := GetProcAddressX(LibCode, 'DevGetInfo');
  DevExtendedData     := GetProcAddressX(LibCode, 'DevExtendedData');
  Logger.EventLog('-- Mapping function');

  FDevice := aDevice;

  Logger.EventLog(Format('++ Opening Driver %d', [Ord(aDevice)]));
  Ret := DevOpenDriver(aDevice);
  Logger.EventLog(Format('// Driver returned %d', [Ret]));
  // Changed to less than zero, this is probably an API change
  // in the dll, have to check this out.
  if Ret < 0 then begin
    Logger.EventLog(Format('++ Resorting to DevExtendedData %d', [Ord(aDevice)]));
    Ret := DevExtendedData(aDevice, 1, 4, 'SMSI');
    if Ret <> 0 then
      raise Exception.CreateFmt(HilscherUnableToOpen, [Ret]);
    Logger.EventLog(Format('-- Resorting to DevExtendedData %d', [Ord(aDevice)]));
  end;
  Logger.EventLog(Format('-- Opening Driver %d', [Ord(aDevice)]));

  Logger.EventLog(Format('++ Initializing Board %d', [Ord(FDevice)]));
  Ret := DevInitBoard(Device, nil);
  if Ret <> 0 then
    raise Exception.CreateFmt(HilcherUnableToInitDevice, [Ret]);
  Logger.EventLog(Format('-- Opening Device %d', [Ord(FDevice)]));

  Logger.EventLog('++ WarmStart');
  Reset(hrtWarmStart);
  Logger.EventLog('-- WarmStart');
  Initialised := True;

  FIOTimeout:= 1.0;
end;

destructor THilscherIO_DLL.Destroy;
begin
  if Initialised then
    DevExitBoard(Device);

  if Device > hdbInvalid  then
    DevCloseDriver(hdb1);

  if(LibCode <> 0) then
    FreeLibrary(LibCode);

  inherited Destroy;
end;

function THilscherIO_DLL.GetBoardInfo : THilscherBoardInfo;
begin
  DevGetBoardInfo(Device, Sizeof(THilscherBoardInfo), @Result);
end;


procedure THilscherIO_DLL.Reset(ResetType : THilscherResetType);
var Ret : Integer;
begin
  if not NoReset then begin
    case ResetType of
      hrtColdStart : Ret := DevReset(Device, 2, 10000);
      hrtWarmStart : Ret := DevReset(Device, 3, 10000);
      hrtBootStart : Ret := DevReset(Device, 4, 10000);
    else
      Ret := 0;
    end;

    if Ret <> 0 then
      Error('Reset Failed', Ret, heWarning);
  end;
end;

function THilscherIO_DLL.Error(aText : string; Ret : Integer; Level : THilscherError) : string;
var Err : Integer;
begin
  Result := '';
  if Ret <> 0 then begin
    Err := Abs(Ret);
    if           Err >= 1000 then begin // RCS Error
      Result := aText + Format(': RCS Error [%d]', [Err + 1000]);
    end else if (Err >= 100) then begin
      Result := aText + Format(': Drive Error [%d]', [Ret]);
    end else if (Err >= 30) then begin
      Result := aText + Format(': User Error [%d]', [Ret]);
    end else begin
      Result := aText + Format(': File Error [%d]', [Ret]);
    end;

    if Assigned(OnErrorEvent) then
      OnErrorEvent(Result, Level);
  end;
end;

procedure THilscherIO_DLL.NoError;
begin
  if Assigned(OnErrorEvent) then
    OnErrorEvent('', heNone);
end;

procedure THilscherIO_DLL.ReadFromDevice(Data : Pointer; Size : Cardinal);
var Ret : Integer;
begin
  Ret := DevExchangeIOErr(Device, 0, 0, nil, 0, Size, Data, @FCommState, Round(FIOTimeOut * 1000));
  //Ret := DevExchangeIO(Device, 0, 0, nil, 0, Size, Data, Round(FIOTimeOut * 1000));

  if Ret <> 0 then
    Error('Data Exchange Problem', Ret, heSevere)
  else
    NoError;

  if CommState.usNumOfDefectiveDataCycles <> _RetryLastCount then begin
    _RetryCount:= _RetryCount + CommState.usNumOfDefectiveDataCycles - _RetryLastCount;
    _RetryLastCount:= CommState.usNumOfDefectiveDataCycles;
  end;

  if Logger.IOSweep > _RetryTime then begin
    _RetryTime:= Logger.IOSweep + 1000;
    FRetryRate:= _RetryCount;
    _RetryCount:= 0;
  end;
end;

procedure THilscherIO_DLL.WriteToDevice(Data : Pointer; Size : Cardinal);
var Ret : Integer;
begin
  //Ret := DevExchangeIOErr(Device, 0, Size, Data, 0, 0, nil, PComState, Round(FIOTimeOut * 1000));
  Ret := DevExchangeIO(Device, 0, Size, Data, 0, 0, nil, Round(FIOTimeOut * 1000));

  if Ret <> 0 then
    Error('Data Exchange Problem', Ret, heSevere)
  else
    NoError;

  // ADDED FOR TEST PURPOSES
  //DeviceError := (GetDriverInfo.bHostFlags and 8) <> 0;
end;

function THilscherIO_DLL.GetDriverInfo : THilscherDriverInfo;
var Ret : Integer;
begin
  Ret := DevGetInfo(Device, 1, Sizeof(Result), @Result);
  if Ret <> 0 then
    Error('Driver Info Failed', Ret, heWarning);
end;

function THilscherIO_DLL.GetBoardVersionInfo : THilscherBoardVersionInfo;
var Ret : Integer;
begin
  Ret := DevGetInfo(Device, 2, Sizeof(Result), @Result);
  if Ret <> 0 then
    Error('Board Version Info Failed', Ret, heWarning);
end;

function THilscherIO_DLL.GetFirmwareInfo : THilscherFirmwareInfo;
var Ret : Integer;
begin
  Ret := DevGetInfo(Device, 3, Sizeof(Result), @Result);
  if Ret <> 0 then
    Error('Firmware Version Info Failed', Ret, heWarning);
end;

function THilscherIO_DLL.GetTaskInfo : THilscherTaskInfo;
var Ret : Integer;
begin
  Ret := DevGetInfo(Device, 4, Sizeof(Result), @Result);
  if Ret <> 0 then
    Error('Task Info Failed', Ret, heWarning);
end;

function THilscherIO_DLL.GetRCSInfo : THilscherRcsInfo;
var Ret : Integer;
begin
  Ret := DevGetInfo(Device, 5, Sizeof(Result), @Result);
  if Ret <> 0 then
    Error('RCS Info Failed', Ret, heWarning);
end;

function THilscherIO_DLL.GetDeviceInfo : THilscherDeviceInfo;
var Ret : Integer;
begin
  Ret := DevGetInfo(Device, 6, Sizeof(Result), @Result);
  if Ret <> 0 then
    Error('Device Info Failed', Ret, heWarning);
end;

function THilscherIO_DLL.GetIOInfo : THilscherIOInfo;
var Ret : Integer;
begin
  Ret := DevGetInfo(Device, 7, Sizeof(Result), @Result);
  if Ret <> 0 then
    Error('IO Info Failed', Ret, heWarning);
end;

end.
