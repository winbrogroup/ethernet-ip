unit HilscherACNC;

interface

uses Classes, CNCTypes, INamedInterface, Hilscher, SysUtils, NamedPlugin,
     AbstractIOSystemAccessor, AcncLogger;

type
  HilscherIOSubSystem = class(TAbstractIOSystemAccessor)
  private
    Hdb : THilscherIO_DLL;
    BoardInfo : THilscherBoardInfo;
    DriverInfo : THilscherDriverInfo;
    BoardVersionInfo : THilscherBoardVersionInfo;
    FirmwareVersionInfo : THilscherFirmwareInfo;
    TaskInfo : THilscherTaskInfo;
    RCSInfo : THilscherRCSInfo;
    DeviceInfo : THilscherDeviceInfo;
    IOInfo : THilscherIOInfo;
    DeviceOK : Boolean;
    FLastError : string;
    LastDeviceError: Boolean;

    Host: IIOSystemAccessorHost;
    FTextDef: string;
    FBoard: Integer;
    FInputSize: Integer;
    FOutputSize: Integer;

    FInputArea: array [0..511] of Byte;
    FOutputArea: array [0..511] of Byte;

    NoReset: Boolean;
    procedure DoError(aText : string; Level : THilscherError);
  public
    constructor Create; override;
    procedure Initialise(Host: IIOSystemAccessorHost; const Params : WideString); override; stdcall;
    destructor Destroy; override;
    procedure UpdateShadowToReal; override;
    procedure UpdateRealToShadow; override;
    function Healthy: Boolean; override;
    procedure Reset; override;
    function LastError: WideString; override;
    function GetInfoPageContent(aPage : Integer) : WideString; override;

    function GetInputArea: Pointer; override;
    function GetOutputArea: Pointer; override;
    function GetInputSize: Integer; override;
    function GetOutputSize: Integer; override;
    function TextDefinition: WideString; override;
    procedure Shutdown; override;
  end;

  HilscherNoResetIOSubSystem = class(HilscherIOSubSystem)
  public
    constructor Create; override;
  end;


implementation

resourcestring
  BoardInfoLang = 'Board';
  DriverInfoLang = 'Driver';
  BoardVersionInfoLang = 'Board Version';
  FirmwareVersionInfoLang = 'Firmware version';
  TaskInfoLang = 'Tasks';
  RCSInfoLang = 'RCS';
  DeviceInfoLang = 'Devices';
  IOInfoLang = 'IO';
  CommStateLang = 'CommState';


constructor HilscherIOSubSystem.Create;
begin
  inherited;
end;

procedure HilscherIOSubSystem.Initialise(Host: IIOSystemAccessorHost; const Params : WideString); stdcall;
var Board: THilscherDevBoard;
    Tmp: string;
begin
  FTextDef:= Params;
  Tmp:= Params;
  Self.Host:= Host;
  FInputSize:= StrToInt(CncTypes.ReadDelimited(Tmp, ';'));
  FOutputSize:= StrToInt(CncTypes.ReadDelimited(Tmp, ';'));
  FInputSize:= CncTypes.RangeInt(FInputSize, 2, 512);
  FOutputSize:= CncTypes.RangeInt(FOutputSize, 2, 512);
  FBoard:= StrToIntDef(CncTypes.ReadDelimited(Tmp, ';'), 0);

  Named.EventLog('********** DO ME A LEMON ************');
  if (FBoard > Ord(High(THilscherDevBoard))) or
     (FBoard < Ord(Low(THilscherDevBoard))) then
    raise ECNCInstallFault.CreateFmt('Invalid board address for [%s]', [ClassName]);

  Board:= hdb1;

//  if Params <> '' then begin
//    Board:= THilscherDevBoard(StrToIntDef(ReadDelimited(Params), 0));
//  end;

  try
    HDb := THilscherIO_DLL.Create(TLogger.Create, Board, NoReset);
    HDb.OnErrorEvent := DoError;
  except
    on E : Exception do begin
      raise ECNCInstallFault.CreateFmt('Hilscher Driver failed [%s]', [E.Message]);
    end;
  end;

  //ParentInfoPageCount := FInfoPages.Count;
  Host.AddPage(BoardInfoLang);
  Host.AddPage(DriverInfoLang);
  Host.AddPage(BoardVersionInfoLang);
  Host.AddPage(FirmwareVersionInfoLang);
  Host.AddPage(TaskInfoLang);
  Host.AddPage(RCSInfoLang);
  Host.AddPage(DeviceInfoLang);
  Host.AddPage(IOInfoLang);
  Host.AddPage(CommStateLang);
  DeviceOK := True;
end;

destructor HilscherIOSubSystem.Destroy;
begin
  if Assigned(Hdb) then
    Hdb.Free;

  inherited Destroy;
end;

procedure HilscherIOSubSystem.Reset;
begin
  if not DeviceOK then begin
    Hdb.Reset(hrtWarmStart);
    Sleep(2000);
    FLastError := '';
    DeviceOK := True;
  end;
end;

function HilscherIOSubSystem.LastError : WideString;
begin
  Result := FLastError;
end;

procedure HilscherIOSubSystem.UpdateShadowToReal;
begin
  if Healthy then
    HDb.WriteToDevice(@FOutputArea, FOutputSize);

  if HDb.DeviceError <> LastDeviceError then
    Named.SetFaultA(ClassName, 'bHostFlags error bit set', faultEstop, nil);

  LastDeviceError := HDb.DeviceError;
end;

procedure HilscherIOSubSystem.UpdateRealToShadow;
begin
  if Healthy then
    HDb.ReadFromDevice(@FInputArea, FInputSize);
end;

{ changed to allow reseting of IO errors }
procedure HilscherIOSubSystem.DoError(aText : string; Level : THilscherError);
begin
  if Level <> heNone then begin
    FLastError := aText;
    if Healthy then begin
      case Level of
        heWarning : begin
          Named.EventLog(aText);
          DeviceOK := False;
        end;
        heSevere : begin
          Named.EventLog(aText);
          DeviceOK := False;
        end;

        heCritical : begin
          Named.EventLog(aText);
          DeviceOK := False;
        end;
      else
        DeviceOK := True;
      end;
    end;
  end else begin
    DeviceOK := True;
  end;
end;

function HilscherIOSubSystem.Healthy: Boolean;
begin
  Result := DeviceOK;
end;

function HilscherIOSubSystem.GetInfoPageContent(aPage : Integer) : WideString;
var Brd : THilscherDevBoard;
begin
  Result := '';

  Named.EventLog('We are asking for page: ' + IntToStr(aPage));
  case aPage of
    0 : begin
      BoardInfo := HDb.BoardInfo;
      Result :=
        'Last Error' + CarRet + LastError + CarRet +
        'abDriverVersion' + CarRet + BoardInfo.abDriverVersion + CarRet;
      for Brd := Low(THilscherDevBoard) to High(THilscherDevBoard) do begin
        Result := Result + 'Board' + CarRet + Format('%d', [Ord(Brd)]) + CarRet +
        'usNumber' + CarRet + IntToStr(BoardInfo.Boards[Brd].usNumber) + CarRet +
        'usAvailable' + CarRet + BooleanIdent[BoardInfo.Boards[Brd].usAvailable <> 0] + CarRet +
        'usPhysicalBoardAddress' + CarRet + IntToStr(BoardInfo.Boards[Brd].usPhysicalBoardAddress) + CarRet +
        'usIrqNumber' + CarRet + IntToStr(BoardInfo.Boards[Brd].usIrqNumber) + CarRet +
        CarRet + CarRet;
      end;
    end;

    1 : begin
      DriverInfo := HDb.DriverInfo;
      Result :=
        'ulOpenCnt'     + CarRet + IntToStr(DriverInfo.ulOpenCnt) + CarRet +
        'ulCloseCnt'    + CarRet + IntToStr(DriverInfo.ulCloseCnt) + CarRet +
        'ulReadCnt'     + CarRet + IntToStr(DriverInfo.ulReadCnt) + CarRet +
        'ulWriteCnt'    + CarRet + IntToStr(DriverInfo.ulWriteCnt) + CarRet +
        'ulIRQCnt'      + CarRet + IntToStr(DriverInfo.ulIRQCnt) + CarRet +
        'bInitMsgFlag'  + CarRet + IntToStr(DriverInfo.bInitMsgFlag) + CarRet +
        'bReadMsgFlag'  + CarRet + IntToStr(DriverInfo.bReadMsgFlag) + CarRet +
        'bWriteMsgFlag' + CarRet + IntToStr(DriverInfo.bWriteMsgFlag) + CarRet +
        'bLastFunction' + CarRet + IntToStr(DriverInfo.bLastFunction) + CarRet +
        'bWriteState'   + CarRet + IntToStr(DriverInfo.bWriteState) + CarRet +
        'bReadState'    + CarRet + IntToStr(DriverInfo.bReadState) + CarRet +
        'bHostFlags'    + CarRet + IntToStr(DriverInfo.bHostFlags) + CarRet +
        'bMyDevFlags'   + CarRet + IntToStr(DriverInfo.bMyDevFlags) + CarRet +
        'bExIOFlag'     + CarRet + IntToStr(DriverInfo.bExIOFlag) + CarRet +
        'ulExIOCnt'     + CarRet + IntToStr(DriverInfo.ulExIOCnt) + CarRet;
    end;

    2 : begin
      BoardVersionInfo := HDb.BoardVersionInfo;
      Result :=
        'ulOpenCnt'       + CarRet + IntToStr(BoardVersionInfo.ulDate) + CarRet +
        'ulDeviceNo'      + CarRet + IntToStr(BoardVersionInfo.ulDeviceNo) + CarRet +
        'ulSerialNumber'  + CarRet + IntToStr(BoardVersionInfo.ulSerialNumber) + CarRet +
        'abPcOsName0'     + CarRet + Copy(BoardVersionInfo.abPcOsName0, 1, 4) + CarRet +
        'abPcOsName1'     + CarRet + Copy(BoardVersionInfo.abPcOsName1, 1, 4) + CarRet +
        'abPcOsName2'     + CarRet + Copy(BoardVersionInfo.abPcOsName2, 1, 4) + CarRet +
        'abOEMIdentifier' + CarRet + Copy(BoardVersionInfo.abOEMIdentifier, 1, 4) + CarRet;
    end;

    3 : begin
      FirmwareVersionInfo := HDb.FirmwareVersionInfo;
      Result :=
        'abFirmwareName'    + CarRet + FirmwareVersionInfo.abFirmwardName + CarRet +
        'abFirmwareVersion' + CarRet + FirmwareVersionInfo.abFirmwareVersion + CarRet;
    end;

    4 : begin
      TaskInfo := HDb.TaskInfo;
      Result :=
        'abFirmwardName' + CarRet + TaskInfo.abTaskName + CarRet +
        'abFirmwardName' + CarRet + IntToStr(TaskInfo.bTaskCondition) + CarRet;
    end;

    5 : begin
      RCSInfo := HDb.RCSInfo;
      Result :=
        'usRcsVersion'   + CarRet + IntToStr(RCSInfo.usRcsVersion) + CarRet +
        'bRcsError'      + CarRet + IntToStr(RCSInfo.bRcsError) + CarRet +
        'bHostWatchDog'  + CarRet + IntToStr(RCSInfo.bHostWatchDog) + CarRet +
        'bDevWatchDog'   + CarRet + IntToStr(RCSInfo.bDevWatchDog) + CarRet +
        'bSegmentCount'  + CarRet + IntToStr(RCSInfo.bSegmentCount) + CarRet +
        'bDeviceAddress' + CarRet + IntToStr(RCSInfo.bDeviceAddress) + CarRet +
        'bDriverType'    + CarRet + IntToStr(RCSInfo.bDriverType) + CarRet;
    end;

    6 : begin
      DeviceInfo := HDb.DeviceInfo;
      Result :=
        'bDpmSize'         + CarRet + IntToStr(DeviceInfo.bDpmSize) + CarRet +
        'bDevType'         + CarRet + IntToStr(DeviceInfo.bDevType) + CarRet +
        'bDevModel'        + CarRet + IntToStr(DeviceInfo.bDevModel) + CarRet +
        'abDevIndentifier' + CarRet + DeviceInfo.abDevIndentifier + CarRet;
    end;

    7 : begin
      IOInfo := HDb.IOInfo;
      Result :=
        'bComBit'         + CarRet + IntToStr(IOInfo.bComBit) + CarRet +
        'bIOExchangeMode' + CarRet + IntToStr(IOInfo.bIOExchangeMode) + CarRet +
        'ulExchangeCnt'   + CarRet + IntToStr(IOInfo.ulExchangeCnt) + CarRet;
    end;

    8 : begin
      Result :=
        'usMode'          + CarRet + IntToStr(HDb.CommState.usMode) + CarRet +
        'usStateFlag'     + CarRet + IntToStr(HDb.CommState.usStateFlag) + CarRet +
        'bGlobalBits'     + CarRet + '$' + IntToHex(Byte(HDb.CommState.bGlobalBits), 2) + CarRet +
        'bIBM_State'      + CarRet + '$' + IntToHex(Byte(HDb.CommState.bIBM_State), 2) + CarRet +
        'bErr_Dev_Adr'    + CarRet + IntToStr(HDb.CommState.bErr_Dev_Adr) + CarRet +
        'bError_Event'    + CarRet + IntToStr(HDb.CommState.bErr_Event) + CarRet +
        'DefectiveCycles' + CarRet + IntToStr(HDb.CommState.usNumOfDefectiveDataCycles) + CarRet +
        'NetwordReinits'  + CarRet + IntToStr(HDb.CommState.usNumOfNetworkReinits) + CarRet +
        '*RetryRate'      + CarRet + IntToStr(HDb.RetryRate) + CarRet;
//          'abS1_cfg'        + CarRed + HexArray(HDb.CommState.
//          'abS1_state'      + CarRed + HexArray(HDb.CommState.
//          'abS1_diag'       + CarRed + HexArray(HDb.CommState.
    end;
  else
    Result := 'Unknown page' + CarRet + CarRet;
  end;
end;


{ HilscherNoResetIOSubSystem }

constructor HilscherNoResetIOSubSystem.Create;
begin
  NoReset:= True;
  inherited;
end;

function HilscherIOSubSystem.GetOutputSize: Integer;
begin
  Result:= FOutputSize;
end;

function HilscherIOSubSystem.GetOutputArea: Pointer;
begin
  Result:= @FOutputArea;
end;

procedure HilscherIOSubSystem.Shutdown;
begin
end;

function HilscherIOSubSystem.GetInputSize: Integer;
begin
  Result:= FInputSize;
end;

function HilscherIOSubSystem.GetInputArea: Pointer;
begin
  Result:= @FInputArea;
end;

function HilscherIOSubSystem.TextDefinition: WideString;
begin
  Result:= FTextDef;
end;


initialization
  HilscherNoResetIOSubSystem.AddIOSystemAccessor('CIF50', HilscherIOSubSystem);
  HilscherNoResetIOSubSystem.AddIOSystemAccessor('CIF50-no-reset', HilscherNoResetIOSubSystem);
end.
