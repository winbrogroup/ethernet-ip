unit AcncLogger;

interface

uses NamedPlugin, Hilscher;

type
  TLogger = class (TLoggingDevice)
  public
    function GetIOSweep: Int64; override;
    procedure EventLog(Value: WideString); override;
  end;


implementation

{ TLogger }

function TLogger.GetIOSweep: Int64;
begin
  Result:= NamedPlugin.IOSweep^;
end;

procedure TLogger.EventLog(Value: WideString);
begin
  Named.EventLog(Value);
end;

end.
