unit Sequencer;

interface

uses SysUtils, DDI_USR, ETH_MNG, StdTypes, DDI_ERR, IBS_UTIL, DDI_MACR, IOCTRL, NamedPlugin;

type
  TEthIOState = (
    eiosNoHandles,
    eiosHandles,
    eiosConfigured,
    eiosRunning
  );


  TFlibsSequencer = class
  private
    CurrentState: TEthIOState;
    DataHd: IBDDIHND;
    MailHd: IBDDIHND;
    HandlesOK: Boolean;
    Timeout: USIGN16;
    FInputSize: Integer;
    FOutputSize: Integer;
    IP: string;
    RunCount: Int64;

    procedure InitializeInterbus(Hnd: IBDDIHND);
    function SendRequestResponse(Hnd: IBDDIHND; Msg: PAWord): IBDDIRET;
    function MailBoxFunction(Hnd: IBDDIHND; Msg: PAWord; Rsp: PAWord; MaxResp: Integer = 512): IBDDIRET;
    procedure ReadDefectiveCycles;
  public
    indata: array[0..1024] of Byte;
    outdata: array[0..1024] of Byte;
    CycleCount: Integer;
    CycleErrorCount: Integer;
    IDCycleCount: Integer;
    IDCycleErrorCount: Integer;
    DataCycleCount: Integer;
    DataCycleErrorCount: Integer;
    constructor Create;
    procedure SetState(state: TEthIOState);
    procedure StopInterbus;
    procedure StartDataExchange;
    procedure StopDataExchange;
    procedure OpenHandles;
    procedure ConfigureInterbus;
    property State: TEthIOState read CurrentState;
    procedure Sweep;
    procedure SetIP(const AIP: string);
    property InputSize: Integer read FInputSize write FInputSize;
    property OutputSize: Integer read FOutputSize write FOutputSize;
  end;

implementation

{ TFlibsSequencer }

constructor TFlibsSequencer.Create;
begin
  Timeout:= 1000;
  CurrentState:= eiosNoHandles;
  RunCount:= 1;
end;

procedure TFlibsSequencer.InitializeInterbus(Hnd: IBDDIHND);
var TxFrame: array[0..30] of Word;
    RxFrame: array[0..30] of Word;
begin
  TxFrame[0]:= $710; // read configuration
  TxFrame[1]:= $1;
  TxFrame[2]:= $1;
  MailBoxFunction(Hnd, @TxFrame, @RxFrame);

  TxFrame[0]:= $711; // activate configuration
  TxFrame[1]:= $1;
  TxFrame[2]:= $1;
  MailBoxFunction(Hnd, @TxFrame, @RxFrame);

  TxFrame[0]:= $701; // Start Data Transfer
  TxFrame[1]:= $0;
  MailBoxFunction(Hnd, @TxFrame, @RxFrame);
end;

function TFlibsSequencer.MailBoxFunction(Hnd: IBDDIHND; Msg, Rsp: PAWord;
  MaxResp: Integer): IBDDIRET;
var con_ind: T_DDI_MXI_ACCESS;
    rcv_buffer: array [0..1023] of Byte;
    I, Retry: Integer;
    ret : IBDDIRET;
begin
  if SendRequestResponse(Hnd, Msg) = ERR_OK then begin
    con_ind.msgType:= 0;
    con_ind.DDIUserID:= 0;
    con_ind.msgLength:= MaxResp;
    con_ind.msgBlk:= @rcv_buffer;

    for Retry:= 0 to 20 do begin
      ret:= DDI_MXI_RcvMessage(Hnd, @con_ind);
      if Ret = ERR_OK then begin
        Rsp[0] := IB_GetMsgCode(@rcv_buffer);
        Rsp[1] := IB_GetParaCnt(@rcv_buffer);

        for I:= 1 to Rsp[1] do begin
          Rsp[I+1]:= IB_GetParaN(@rcv_buffer, I);
        end;
        Result:= Ret;
        Exit;
      end;
      Sleep(100);
    end;
  end;
  SetState(eiosNoHandles);
  raise Exception.Create('IO not responding');
end;

function TFlibsSequencer.SendRequestResponse(Hnd: IBDDIHND;
  Msg: PAWord): IBDDIRET;
var req_res: T_DDI_MXI_ACCESS;
    snd_buffer: array [0..1023] of Byte;
    I: Integer;
begin
  IB_SetCmdCode(@snd_buffer, Msg[0]);
  IB_SetParaCount(@snd_buffer, Msg[1]);

  for I:= 1 to Msg[1] do begin
    IB_SetParaN(@snd_buffer, Msg[I+1], I);
  end;

  req_res.msgType:= 0; // What is msgtype
  req_res.msgLength:= (Msg[1] + 2) * 2;
  req_res.msgBlk:= @snd_buffer;

  Result := DDI_MXI_SndMessage(Hnd, @req_res);
end;

procedure TFlibsSequencer.SetState(state: TEthIOState);
begin
  case(state) of
    eiosNoHandles: begin
      if CurrentState > eiosHandles then begin
        DDI_DevCloseNode(DataHd);
        DDI_DevCloseNode(MailHd);
      end;
      Named.EventLog('Flibs: no handles');
    end;

    eiosHandles: begin
      ETH_ClearDTITimeoutCtrl(DataHd);
      Named.EventLog('Flibs: Handles');
    end;

    eiosConfigured: begin
      ETH_ClearDTITimeoutCtrl(DataHd);
      ETH_ClrNetFailStatus(DataHd);
      Named.EventLog('Flibs: Configured');
    end;

    eiosRunning: begin
      {$IFNDEF NO_TIMEOUT}
      ETH_SetDTITimeoutCtrl(DataHd, Timeout);
      {$ENDIF}
      Named.EventLog('Flibs: Running');
    end;
  end;
  CurrentState := State;
end;

procedure TFlibsSequencer.StopInterbus;
var TxFrame: array[0..10] of Word;
    RxFrame: array[0..10] of Word;
begin
  TxFrame[0]:= $1303; // Alarm stop
  TxFrame[1]:= $0;
  MailBoxFunction(MailHd, @TxFrame, @RxFrame);
end;

procedure TFlibsSequencer.Sweep;
var ddidtiAccessRead, ddidtiAccessWrite: T_DDI_DTI_ACCESS;
    Ret: IBDDIRET;
    Diag: T_IBS_DIAG;
begin
  try
    if CurrentState = eiosRunning then begin
      ddidtiAccessRead.length:= FInputSize;
      ddidtiAccessRead.address:= 0;
      ddidtiAccessRead.data:= @indata;
      ddidtiAccessRead.dataCons:= {IB_EX_DTA or} IB_NODE_0 or DTI_DATA_BYTE;
      {
      Ret:= DDI_DTI_ReadData(DataHd, @ddidtiAccessRead);
      if Ret <> ERR_OK then begin
        raise Exception.Create('');
      end;
      }

      ddidtiAccessWrite.length:= FOutputSize;
      ddidtiAccessWrite.address:= 0;
      ddidtiAccessWrite.data:= @outdata;
      ddidtiAccessWrite.dataCons:= {IB_EX_DTA or} IB_NODE_0 or DTI_DATA_BYTE;
      {
      Ret:= DDI_DTI_WriteData(DataHd, @ddidtiAccessWrite);
      if Ret <> ERR_OK then begin
        raise Exception.Create('');
      end;
      }
      Ret:= DDI_DTI_ReadWriteData(DataHd, @ddidtiAccessWrite, @ddidtiAccessRead);
      if Ret <> ERR_OK then begin
        raise Exception.Create('');
      end;
    end;


    if CurrentState >= eiosConfigured then begin
      Ret:= GetIBSDiagnostic(DataHd, Diag);
      if Ret <> ERR_OK then
        raise Exception.Create('IO System unexpected error');
      if (Diag.state and READY_BIT) = 0 then begin
        setState(eiosHandles);
      end;

      if (Diag.state and (USER_BIT or PF_BIT or BUS_BIT or CTRL_BIT) <> 0) then begin
        setState(eiosHandles);
      end;

      if (Diag.state and RUN_BIT ) = 0 then begin
        setState(eiosHandles);
      end;

      if (RunCount mod 300) = 0 then begin
        ReadDefectiveCycles;
      end;
      Inc(RunCount);
    end;
  except
    SetState(eiosNoHandles);
  end;
end;


var Blog: Integer;
procedure TFlibsSequencer.ReadDefectiveCycles;
var TxFrame: array[0..30] of Word;
    RxFrame: array[0..30] of Word;
begin
  TxFrame[0]:= $032b; // Get_Diag_Info
  TxFrame[1]:= $2;
  TxFrame[2]:= $1;
  TxFrame[3]:= $4;  // Bit 2 Global_Count
  MailBoxFunction(MailHd, @TxFrame, @RxFrame);

  if RXFrame[0] = $832b then
    Blog:= RxFrame[1];

  CycleCount:= RxFrame[5] * $10000 + RxFrame[6];
  CycleErrorCount:= RxFrame[7] * $10000 + RxFrame[8];

  IDCycleCount:= RxFrame[9] * $10000 + RxFrame[10];
  IDCycleErrorCount:= RxFrame[11] * $10000 + RxFrame[12];
  DataCycleCount:= RxFrame[13] * $10000 + RxFrame[14];
  DataCycleErrorCount:= RxFrame[15] * $10000 + RxFrame[16];

end;

procedure TFlibsSequencer.ConfigureInterbus;
begin
  StopInterbus;
  InitializeInterbus(MailHd);
  //ReadConfiguration(MailHd);
  SetState(eiosConfigured);
end;

procedure TFlibsSequencer.StartDataExchange;
begin
  SetState(eiosRunning);
end;

procedure TFlibsSequencer.StopDataExchange;
begin
  SetState(eiosConfigured);
end;

procedure TFlibsSequencer.OpenHandles;
var Ret: IBDDIRET;
    Device: string;
begin
  setState(eiosNoHandles);
  HandlesOK:= False;
  //Ret:= DDI_DevOpenNode('IBETH01N1_M', DDI_RW, NodeHd);

  // Open the card for use as data exchange
  Device:= 'IBETHIP[' + IP + '|30,30]N1_D';
  Ret:= DDI_DevOpenNode(PChar(Device), DDI_RW, DataHd);
  if Ret <> ERR_OK then begin
    raise Exception.Create('Unable to open Data handle');
  end;


  // Open the card for use as mail exchange
  Device:= 'IBETHIP[' + IP + ']N1_M';
  Ret:= DDI_DevOpenNode(PChar(Device), DDI_RW, MailHd);
  if Ret <> ERR_OK then begin
    raise Exception.Create('Unable to open Mail handle');
  end;

  HandlesOK:= True;
  SetState(eiosHandles);
end;



procedure TFlibsSequencer.SetIP(const AIP: string);
begin
  IP:= AIP;
end;

end.
