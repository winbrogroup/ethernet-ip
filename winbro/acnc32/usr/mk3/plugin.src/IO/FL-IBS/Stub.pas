unit Stub;

interface

uses Windows, Classes, INamedInterface, CNCTypes, SysUtils, Sequencer, SyncObjs,
     AbstractIOSystemAccessor, NamedPlugin, ExtCtrls;

type
  TFLIBRunThread = class;
  TFLIBS = class(TAbstractIOSystemAccessor)
  private
    FTextDef: string;
    FInputSize: Integer;
    FOutputSize: Integer;
    IP: string;
    Host: IIOSystemAccessorHost;
    Thread: TFLIBRunThread;

    FInputArea: array [0..511] of Byte;
    FOutputArea: array [0..511] of Byte;
  public
    constructor Create; override;
    function GetInfoPageContent(aPage : Integer) : WideString; override;
    procedure Initialise(Host: IIOSystemAccessorHost; const Params : WideString); override;
    procedure UpdateShadowToReal; override;
    procedure UpdateRealToShadow; override;
    function Healthy : Boolean; override;
    procedure Reset; override;
    function LastError : WideString; override;
    function GetInputArea: Pointer; override;
    function GetOutputArea: Pointer; override;
    function GetInputSize: Integer; override;
    function GetOutputSize: Integer; override;
    function TextDefinition: WideString; override;
    procedure Shutdown; override;
  end;

  TFLIBRunThread = class(TThread)
  private
    FLibsSequencer: TFlibsSequencer;
    Accessor: TFLIBS;
    FInReset: Boolean;
    Lock: TCriticalSection;
    InData: array[0..511] of Byte;
    OutData: array[0..511] of Byte;
    RunTag: TTagRef;
    ResetTag: TTagRef;
    MaxUpdateTag: TTagRef;
    AverageUpdateTag: TTagRef;
    DefectiveCycleTag: TTagRef;

    RunningCount: Integer;

    StartTime: Cardinal;
    MaxTime: Cardinal;
    AverageTime: Cardinal;
  public
    constructor Create(S: TFLIBS; InSize, OutSize: Integer; const IP: string);
    procedure Execute; override;
    property InReset: Boolean read FInReset write FInReset;
  end;

implementation

{ TFLIBS }

function TFLIBS.GetOutputSize: Integer;
begin
  Result:= FOutputSize;
end;

function TFLIBS.GetInfoPageContent(aPage: Integer): WideString;
begin
  Result := '';

  //Named.EventLog('We are asking for page: ' + IntToStr(aPage));
  case aPage of
    0 : begin
      Result :=
        'Defective Cycles' + CarRet + IntToStr(Thread.FLibsSequencer.CycleErrorCount) + CarRet +
        'Cycles' + CarRet + IntToStr(Thread.FLibsSequencer.CycleCount) + CarRet +
        'ID Cycle Count' + CarRet + IntToStr(Thread.FLibsSequencer.IDCycleCount) + CarRet +
        'ID Cycle Error Count' + CarRet + IntToStr(Thread.FLibsSequencer.IDCycleErrorCount) + CarRet +
        'Data Cycle Count' + CarRet + IntToStr(Thread.FLibsSequencer.DataCycleCount) + CarRet +
        'Data Cycle Error Count' + CarRet + IntToStr(Thread.FLibsSequencer.DataCycleErrorCount);
    end;
  end;
end;

function TFLIBS.GetOutputArea: Pointer;
begin
  Result:= @FOutputArea;
end;

procedure TFLIBS.Shutdown;
begin
  Thread.Terminate;
  Thread.WaitFor;
end;

function TFLIBS.GetInputSize: Integer;
begin
  Result:= FInputSize;
end;

procedure TFLIBS.Reset;
begin
  if not Healthy then
    Thread.InReset:= True;
end;

function TFLIBS.GetInputArea: Pointer;
begin
  Result:= @FInputArea;
end;

function TFLIBS.TextDefinition: WideString;
begin
  Result:= FTextDef;
end;

function TFLIBS.Healthy: Boolean;
begin
  Result:= Thread.FLibsSequencer.State = eiosRunning;
end;

function TFLIBS.LastError: WideString;
begin
  Result:= '';
end;

procedure TFLIBS.Initialise(Host: IIOSystemAccessorHost; const Params: WideString);
var Tmp: string;
begin
  FTextDef:= Params;
  Tmp:= Params;
  Self.Host:= Host;
  FInputSize:= StrToInt(CncTypes.ReadDelimited(Tmp, ';'));
  FOutputSize:= StrToInt(CncTypes.ReadDelimited(Tmp, ';'));
  IP:= CncTypes.ReadDelimited(Tmp, ';');
  FInputSize:= CncTypes.RangeInt(FInputSize, 2, 512);
  FOutputSize:= CncTypes.RangeInt(FOutputSize, 2, 512);


  Thread:= TFLIBRunThread.Create(Self, FInputSize, FOutputSize, IP);
  Thread.RunTag:= Named.AddTag('EthIBSRefresh', 'TIntegerTag');
  Thread.ResetTag:= Named.AddTag('EthIBSUpdateReset', 'TIntegerTag');

  Thread.MaxUpdateTag:= Named.AddTag('EthIBSUpdateMax', 'TIntegerTag');
  Thread.AverageUpdateTag:= Named.AddTag('EthIBSUpdateAverage', 'TIntegerTag');
  Thread.DefectiveCycleTag:= Named.AddTag('EthIBSDefectiveCycles', 'TStringTag');

  Host.AddPage('State');

  Thread.Resume;
end;

procedure TFLIBS.UpdateRealToShadow;
begin
  Thread.Lock.Acquire;
  try
    CopyMemory(@FInputArea, @Thread.InData, FInputSize);
  finally
    Thread.Lock.Release;
  end;
end;

procedure TFLIBS.UpdateShadowToReal;
begin
  Thread.Lock.Acquire;
  try
    CopyMemory(@Thread.OutData, @FOutputArea, FOutputSize);
  finally
    Thread.Lock.Release;
  end;
end;

var Blog: Integer; // delete me
constructor TFLIBS.Create;
begin
  inherited;
  Inc(Blog);
  // Constructor for debug purposes
end;

{ TFLIBRunThread }

constructor TFLIBRunThread.Create(S: TFLIBS; InSize, OutSize: Integer; const IP: string);
begin
  inherited Create(True);
  Accessor:= S;
  FLibsSequencer:= TFLibsSequencer.Create;
  FLibsSequencer.SetIP(IP);
  FLibsSequencer.InputSize:= InSize;
  FlibsSequencer.OutputSize:= OutSize;
  Lock:= TCriticalSection.Create;
end;

var Tmp: Integer;
procedure TFLIBRunThread.Execute;
begin
  Self.Priority:= tpHighest;
  while not Terminated do begin

    // Reset state machine
    if InReset then begin
      try
        case FLibsSequencer.State of
          eiosNoHandles: begin
            FLibsSequencer.OpenHandles;
          end;
          eiosHandles: begin
            FLibsSequencer.ConfigureInterbus;
          end;
          eiosConfigured: begin
            FLibsSequencer.SetState(eiosRunning);
          end;
          eiosRunning: begin
            InReset:= False;
          end;
        end;
      except
        InReset:= False;
      end;
    end;

    Tmp:= Windows.GetTickCount;
    if FlibsSequencer.State = eiosRunning then begin
      StartTime:= Windows.GetTickCount;
      FLibsSequencer.Sweep;
      Lock.Acquire;
      try
        CopyMemory(@InData, @FLibsSequencer.InData, FLibsSequencer.InputSize);
        CopyMemory(@FLibsSequencer.OutData, @OutData, FLibsSequencer.OutputSize);
        Inc(RunningCount);
        Named.SetAsInteger(RunTag, RunningCount);
      finally
        Lock.Release;
      end;
      StartTime:= Windows.GetTickCount - StartTime;

      if(StartTime > MaxTime) then
        MaxTime:= StartTime;

      AverageTime:= AverageTime + StartTime;

      if Named.GetAsBoolean(ResetTag) then
        MaxTime:= 0;

      if(RunningCount mod 10 = 0) then begin
        AverageTime:= AverageTime div 10;
        Named.SetAsInteger(MaxUpdateTag, MaxTime);
        Named.SetAsInteger(AverageUpdateTag, AverageTime);
        Named.SetAsInteger(DefectiveCycleTag, FLibsSequencer.CycleErrorCount);
        AverageTime:= 0;
      end;
    end;

    Sleep(17);
  end;
  try
    FlibsSequencer.StopInterbus;
  except
  end;
  FlibsSequencer.SetState(eiosNoHandles);
end;

initialization
  TFLIBS.AddIOSystemAccessor('EthIBS', TFLIBS);
end.
