unit SVC_CODE;
{**************************************************************************}
{                                                                          }
{    This C DLL header file first (automatic) conversion generated by:     }
{    HeadConv 4.0 (c) 2000 by Bob Swart (aka Dr.Bob - www.drbob42.com)     }
{      Final Delphi-Jedi (Darth) command-line units edition                }
{                                                                          }
{    Generated Date: 13/02/2006                                            }
{    Generated Time: 13:35:13                                              }
{                                                                          }
{**************************************************************************}

interface
uses
{$IFDEF WIN32}
  Windows;
{$ELSE}
  Wintypes, WinProcs;
{$ENDIF}


{=> SVC_CODE.H <=}

{+//**************************************************************************** }
{-**** }
{-** PHOENIX CONTACT GmbH & Co., W-32819 Blomberg** }
{-** Copyright Phoenix Contact 1996** }
{-**** }
{-****************************************************************************** }
{-**** }
{-** Project : IBS Controller Boards** }
{-** Sub-Project : --** }
{-**** }
{=**************************************************************************** }
{+// }
{-** File : SVC_CODE.H }
{-** Component(s) : - }
{-** Device(s) : independent }
{-** : }
{-** $Date: 2006/12/11 10:44:01 $ }
{-** : }
{-** $Revision: 1.1.1.1 $ }
{-** Status : - }
{-** : }
{-** Description : Codes for ibs master services }
{-** : }
{-****************************************************************************** }
{-** Change Notes : }
{-** }
{-** Date Version Author Description }
{-** --------------------------------------------------------------------------- }
{-** }
{= }

{$IFNDEF INCL_SVC_CODES}
{$DEFINE INCL_SVC_CODES}

const
  S_GET_ERROR_INFO_REQ = $0316;
const
  S_GET_ERROR_INFO_CNF = $8316;
const
  S_CONFIRM_DIAG_REQ = $0760;
const
  S_CONFIRM_DIAG_CNF = $8760;
const
  S_GET_REVISION_INFO_REQ = $032A;
const
  S_GET_REVISION_INFO_CNF = $832A;
const
  S_GET_DIAG_INFO_REQ = $032B;
const
  S_GET_DIAG_INFO_CNF = $832B;

const
  S_START_DATA_TRANSFER_REQ = $0701;
const
  S_START_DATA_TRANSFER_CNF = $8701;
const
  S_STOP_DATA_TRANSFER_REQ = $0702;
const
  S_STOP_DATA_TRANSFER_CNF = $8702;
const
  S_ALARM_STOP_REQ = $1303;
const
  S_ALARM_STOP_CNF = $9303;

const
  S_ACTIVATE_CFG_REQ = $0711;
const
  S_ACTIVATE_CFG_CNF = $8711;
const
  S_DEACTIVATE_CFG_REQ = $0712;
const
  S_DEACTIVATE_CFG_CNF = $8712;
const
  S_CONTROL_ACTIVE_CFG_REQ = $0713;
const
  S_CONTROL_ACTIVE_CFG_CNF = $8713;
const
  S_CONTROL_DEVICE_FUNC_REQ = $0714;
const
  S_CONTROL_DEVICE_FUNC_CNF = $8714;
const
  S_READ_DEVICE_STATE_REQ = $0315;
const
  S_READ_DEVICE_STATE_CNF = $8315;

const
  S_INIT_LOAD_CFG_REQ = $0306;
const
  S_INIT_LOAD_CFG_CNF = $8306;
const
  S_LOAD_CFG_REQ = $0307;
const
  S_LOAD_CFG_CNF = $8307;
const
  S_TERM_LOAD_CFG_REQ = $0308;
const
  S_TERM_LOAD_CFG_CNF = $8308;
const
  S_READ_CFG_REQ = $0309;
const
  S_READ_CFG_CNF = $8309;
const
  S_COMPLETE_LOAD_CFG_REQ = $030A;
const
  S_COMPLETE_LOAD_CFG_CNF = $830A;
const
  S_COMPLETE_READ_CFG_REQ = $030B;
const
  S_COMPLETE_READ_CFG_CNF = $830B;
const
  S_DELETE_CFG_REQ = $030C;
const
  S_DELETE_CFG_CNF = $830C;
const
  S_SWITCH_CFG_REQ = $030D;
const
  S_SWITCH_CFG_CNF = $830D;
const
  S_CONTROL_PARAM_REQ = $030E;
const
  S_CONTROL_PARAM_CNF = $830E;
const
  S_CONTROL_STATISTICS_REQ = $030;
const
  S_CONTROL_STATISTICS_CNF = $830;

const
  S_INIT_PUT_PD_OV_REQ = $0320;
const
  S_INIT_PUT_PD_OV_CNF = $8320;
const
  S_PUT_PD_OV_REQ = $0321;
const
  S_PUT_PD_OV_CNF = $8321;
const
  S_TERM_PUT_PD_OV_REQ = $0322;
const
  S_TERM_PUT_PD_OV_CNF = $8322;
const
  S_GET_PD_OV_REQ = $0323;
const
  S_GET_PD_OV_CNF = $8323;
const
  S_INIT_LOAD_PDRL_REQ = $0324;
const
  S_INIT_LOAD_PDRL_CNF = $8324;
const
  S_LOAD_PDRL_REQ = $0325;
const
  S_LOAD_PDRL_CNF = $8325;
const
  S_TERM_LOAD_PDRL_REQ = $0326;
const
  S_TERM_LOAD_PDRL_CNF = $8326;
const
  S_READ_PDRL_REQ = $0327;
const
  S_READ_PDRL_CNF = $8327;
const
  S_COMPACT_LOAD_PDRL_REQ = $0328;
const
  S_COMPACT_LOAD_PDRL_CNF = $8328;
const
  S_COMPACT_READ_PDRL_REQ = $0329;
const
  S_COMPACT_READ_PDRL_CNF = $8329;

const
  S_SET_VALUE_REQ = $0750;
const
  S_SET_VALUE_CNF = $8750;
const
  S_READ_VALUE_REQ = $0351;
const
  S_READ_VALUE_CNF = $8351;

const
  S_CREATE_CFG_REQ = $0710;
const
  S_CREATE_CFG_CNF = $8710;

const
  S_RESET_CONTROLLER_REQ = $0956;

const
  S_GENERATE_ABORTS_REQ = $0957;

const
  S_INIT_LOAD_AH_OBJECT_REQ = $0140;
const
  S_INIT_LOAD_AH_OBJECT_CNF = $8140;
const
  S_LOAD_AH_OBJECT_REQ = $0141;
const
  S_LOAD_AH_OBJECT_CNF = $8141;
const
  S_TERMINATE_LOAD_AH_OBJECT_REQ = $0142;
const
  S_TERMINATE_LOAD_AH_OBJECT_CNF = $8142;
const
  S_READ_AH_OBJECT_REQ = $0143;
const
  S_READ_AH_OBJECT_CNF = $8143;
const
  S_DELETE_AH_OBJECT_REQ = $0144;
const
  S_DELETE_AH_OBJECT_CNF = $8144;
const
  S_AH_SET_VALUE_REQ = $0352;
const
  S_AH_SET_VALUE_CNF = $8352;
const
  S_AH_READ_VALUE_REQ = $0353;
const
  S_AH_READ_VALUE_CNF = $8353;

const
  S_INIT_LOAD_SH_OBJECT_REQ = $0145;
const
  S_INIT_LOAD_SH_OBJECT_CNF = $8145;
const
  S_LOAD_SH_OBJECT_REQ = $0146;
const
  S_LOAD_SH_OBJECT_CNF = $8146;
const
  S_TERMINATE_LOAD_SH_OBJECT_REQ = $0147;
const
  S_TERMINATE_LOAD_SH_OBJECT_CNF = $8147;
const
  S_READ_SH_OBJECT_REQ = $0148;
const
  S_READ_SH_OBJECT_CNF = $8148;
const
  S_DELETE_SH_OBJECT_REQ = $0149;
const
  S_DELETE_SH_OBJECT_CNF = $8149;

const
  S_INIT_LOAD_EH_OBJECT_REQ = $014A;
const
  S_INIT_LOAD_EH_OBJECT_CNF = $814A;
const
  S_LOAD_EH_OBJECT_REQ = $014B;
const
  S_LOAD_EH_OBJECT_CNF = $814B;
const
  S_TERMINATE_LOAD_EH_OBJECT_REQ = $014C;
const
  S_TERMINATE_LOAD_EH_OBJECT_CNF = $814C;
const
  S_READ_EH_OBJECT_REQ = $014D;
const
  S_READ_EH_OBJECT_CNF = $814D;
const
  S_DELETE_EH_OBJECT_REQ = $014E;
const
  S_DELETE_EH_OBJECT_CNF = $814E;

const
  S_MH_SET_VALUE_REQ = $0354;
const
  S_MH_SET_VALUE_CNF = $8354;
const
  S_MH_READ_VALUE_REQ = $0355;
const
  S_MH_READ_VALUE_CNF = $8355;

const
  S_CHANGE_EXCLUSIVE_RIGHTS_REQ = $014;
const
  S_CHANGE_EXCLUSIVE_RIGHTS_CNF = $814;
const
  S_FETCH_EXCLUSIVE_RIGHTS_REQ = $0156;
const
  S_FETCH_EXCLUSIVE_RIGHTS_CNF = $8156;

const
  S_SET_ONLINE_MODUS_REQ = $0150;
const
  S_SET_ONLINE_MODUS_CNF = $8150;

const
  S_DDI_V24_SET_VALUE_REQ = $0151;
const
  S_DDI_V24_SET_VALUE_CNF = $8151;
const
  S_DDI_READ_VALUE_REQ = $0155;
const
  S_DDI_READ_VALUE_CNF = $8155;

const
  S_SET_INDICATION_REQ = $0152;
const
  S_SET_INDICATION_CNF = $8152;

const
  S_WRITE_MPM_REQ = $0330;
const
  S_WRITE_MPM_CNF = $8330;
const
  S_READ_MPM_REQ = $0331;
const
  S_READ_MPM_CNF = $8331;
const
  S_WRITE_BYTE_MPM_REQ = $0153;
const
  S_WRITE_BYTE_MPM_CNF = $8153;
const
  S_READ_BYTE_MPM_REQ = $0154;
const
  S_READ_BYTE_MPM_CNF = $8154;

const
  S_SWITCH_VAR_ID_ACCESS_REQ = $0157;
const
  S_SWITCH_VAR_ID_ACCESS_CNF = $8157;
const
  S_SAVE_ACTION_REQ = $0158;
const
  S_SAVE_ACTION_CNF = $8158;

const
  S_CLEAR_RAM_CARD_REQ = $0159;
const
  S_CLEAR_RAM_CARD_CNF = $8159;
const
  S_SWITCH_ON_OFF_RAM_CARD_REQ = $015A;
const
  S_SWITCH_ON_OFF_RAM_CARD_CNF = $815A;

const
  S_L2_ACCESS_REQ = $0332;
const
  S_L2_ACCESS_CNF = $8332;
const
  S_PHYSICAL_READ_REQ = $0333;
const
  S_PHYSICAL_READ_CNF = $8333;
const
  S_MANUAL_DIAGNOSIS_REQ = $0334;
const
  S_MANUAL_DIAGNOSIS_CNF = $8334;
const
  S_INDICATION_START = $0390;

const
  S_PCP_INITIATE_REQ = $008B;
const
  S_PCP_INITIATE_IND = $408B;
const
  S_PCP_INITIATE_RSP = $C08B;
const
  S_PCP_INITIATE_CNF = $808B;
const
  S_PCP_ABORT_REQ = $088D;
const
  S_PCP_ABORT_IND = $488D;
const
  S_PCP_REJECT_IND = $488E;
const
  S_PCP_IDENTIFY_REQ = $0087;
const
  S_PCP_IDENTIFY_CNF = $8087;
const
  S_PCP_STATUS_REQ = $0086;
const
  S_PCP_STATUS_CNF = $8086;
const
  S_PCP_GET_OV_REQ = $0088;
const
  S_PCP_GET_OV_CNF = $8088;
const
  S_PCP_READ_REQ = $0081;
const
  S_PCP_READ_IND = $4081;
const
  S_PCP_READ_RSP = $C081;
const
  S_PCP_READ_CNF = $8081;
const
  S_PCP_WRITE_REQ = $0082;
const
  S_PCP_WRITE_IND = $4082;
const
  S_PCP_WRITE_RSP = $C082;
const
  S_PCP_WRITE_CNF = $8082;
const
  S_PCP_INFO_REPORT_REQ = $0885;
const
  S_PCP_INFO_REPORT_IND = $4885;
const
  S_PCP_START_REQ = $0083;
const
  S_PCP_START_IND = $4083;
const
  S_PCP_START_RSP = $C083;
const
  S_PCP_START_CNF = $8083;
const
  S_PCP_STOP_REQ = $0084;
const
  S_PCP_STOP_IND = $4084;
const
  S_PCP_STOP_RSP = $C084;
const
  S_PCP_STOP_CNF = $8084;
const
  S_PCP_RESUME_REQ = $0089;
const
  S_PCP_RESUME_IND = $4089;
const
  S_PCP_RESUME_RSP = $C089;
const
  S_PCP_RESUME_CNF = $8089;
const
  S_PCP_RESET_REQ = $008A;
const
  S_PCP_RESET_IND = $408A;
const
  S_PCP_RESET_RSP = $C08A;
const
  S_PCP_RESET_CNF = $808A;
const
  S_PCP_INIT_LOAD_KBL_LOC_REQ = $0200;
const
  S_PCP_INIT_LOAD_KBL_LOC_CNF = $8200;
const
  S_PCP_LOAD_KBL_LOC_REQ = $0201;
const
  S_PCP_LOAD_KBL_LOC_CNF = $8201;
const
  S_PCP_TERM_LOAD_KBL_LOC_REQ = $0202;
const
  S_PCP_TERM_LOAD_KBL_LOC_CNF = $8202;
const
  S_PCP_READ_KBL_LOC_REQ = $0203;
const
  S_PCP_READ_KBL_LOC_CNF = $8203;
const
  S_PCP_SET_VALUE_LOC_REQ = $0210;
const
  S_PCP_SET_VALUE_LOC_CNF = $8210;
const
  S_PCP_READ_VALUE_LOC_REQ = $0211;
const
  S_PCP_READ_VALUE_LOC_CNF = $8211;
const
  S_PCP_READ_STATUS_LOC_REQ = $0212;
const
  S_PCP_READ_STATUS_LOC_CNF = $8212;
const
  S_PCP_PNM7_RESET_REQ = $0A20;
const
  S_PCP_PNM7_RESET_CNF = $8A20;
const
  S_PCP_PNM7_EVENT_IND = $4A21;
const
  S_PCP_PNM7_INITIATE_REQ = $00A0;
const
  S_PCP_PNM7_INITIATE_CNF = $80A0;
const
  S_PCP_PNM7_ABORT_REQ = $08A1;
const
  S_PCP_PNM7_ABORT_IND = $48A1;
const
  S_PCP_INIT_LOAD_KBL_REM_REQ = $00A2;
const
  S_PCP_INIT_LOAD_KBL_REM_IND = $40A2;
const
  S_PCP_INIT_LOAD_KBL_REM_RSP = $C0A2;
const
  S_PCP_INIT_LOAD_KBL_REM_CNF = $80A2;
const
  S_PCP_LOAD_KBL_REM_REQ = $00A3;
const
  S_PCP_LOAD_KBL_REM_CNF = $80A3;
const
  S_PCP_TERM_LOAD_KBL_REM_REQ = $00A4;
const
  S_PCP_TERM_LOAD_KBL_REM_CNF = $80A4;
const
  S_PCP_READ_KBL_REM_REQ = $00A5;
const
  S_PCP_READ_KBL_REM_CNF = $80A5;
const
  S_PCP_AUTO_LOAD_KBL_LOC_REQ = $0240;
const
  S_PCP_AUTO_LOAD_KBL_LOC_CNF = $8240;
const
  S_PCP_COMP_READ_KBL_LOC_REQ = $0241;
const
  S_PCP_COMP_READ_KBL_LOC_CNF = $8241;
const
  S_PCP_AUTO_LOAD_KBL_REM_REQ = $00C0;
const
  S_PCP_AUTO_LOAD_KBL_REM_CNF = $80C0;
const
  S_PCP_LOAD_NET_KBL_LOC_REQ = $0260;
const
  S_PCP_LOAD_NET_KBL_LOC_CNF = $8260;
const
  S_PCP_INIT_PUT_OV_LOC_REQ = $0261;
const
  S_PCP_INIT_PUT_OV_LOC_CNF = $8261;
const
  S_PCP_PUT_OV_LOC_REQ = $0262;
const
  S_PCP_PUT_OV_LOC_CNF = $8262;
const
  S_PCP_TERM_PUT_OV_LOC_REQ = $0263;
const
  S_PCP_TERM_PUT_OV_LOC_CNF = $8263;
const
  S_PCP_LOAD_KBL_PAR_LOC_REQ = $0264;
const
  S_PCP_LOAD_KBL_PAR_LOC_CNF = $8264;

const
  S_API_FAULT_IND = $4B58;
const
  S_EXCEPTION_IND = $73FE;
const
  S_DEVICE_STATE_IND = $5340;
const
  S_FAULT_IND = $4341;
const
  S_BUS_ERROR_IND = $6342;


{$ENDIF /*INCL_SVC_CODES*/}

var
  DLLLoaded: Boolean { is DLL (dynamically) loaded already? }
    {$IFDEF WIN32} = False; {$ENDIF}

implementation

var
  SaveExit: pointer;
  DLLHandle: THandle;
{$IFNDEF MSDOS}
  ErrorMode: Integer;
{$ENDIF}

  procedure NewExit; far;
  begin
    ExitProc := SaveExit;
    FreeLibrary(DLLHandle)
  end {NewExit};

procedure LoadDLL;
begin
  if DLLLoaded then Exit;
{$IFNDEF MSDOS}
  ErrorMode := SetErrorMode($8000{SEM_NoOpenFileErrorBox});
{$ENDIF}
  DLLHandle := LoadLibrary('SVC_CODE.DLL');
  if DLLHandle >= 32 then
  begin
    DLLLoaded := True;
    SaveExit := ExitProc;
    ExitProc := @NewExit;
  end
  else
  begin
    DLLLoaded := False;
    { Error: SVC_CODE.DLL could not be loaded !! }
  end;
{$IFNDEF MSDOS}
  SetErrorMode(ErrorMode)
{$ENDIF}
end {LoadDLL};

begin
  LoadDLL;
end.
