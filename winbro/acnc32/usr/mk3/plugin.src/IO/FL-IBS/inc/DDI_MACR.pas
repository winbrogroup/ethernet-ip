unit DDI_MACR;

interface

uses
  Windows, StdTypes;


{=> DDI_MACR.H <=}

{+//**************************************************************************** }
{-**** }
{-** PHOENIX CONTACT GmbH & Co., 32819 Blomberg, Germany** }
{-**** }
{-****************************************************************************** }
{-**** }
{-** Project : --** }
{-** Sub-Project : --** }
{-**** }
{=**************************************************************************** }

{+// }
{-** File : DDI_MACR.H }
{-** Component(s) : - }
{-** Device(s) : - }
{-** Terminology : - }
{-** : }
{-** Definition : }
{-** Author : }
{-** $Date: 2006/12/11 10:44:01 $ }
{-** : }
{-** $Revision: 1.1.1.1 $ }
{-** }
{-** Status : }
{-** : }
{-** Description : Macros for processing commands, messages and }
{-** : process data. }
{-** : }
{-****************************************************************************** }
{-** Change Notes : }
{-** }
{-** Date Version Author Description }
{-** --------------------------------------------------------------------------- }
{-** }
{= }


procedure IB_SetCmdCode(n: PAByte; const m: Word);
procedure IB_SetParaCount(n: PAByte; const m: Word);
procedure IB_SetParaN(A: PAByte; const V: Word; I: Integer);
function IB_GetMsgCode(n: PAByte): Word;
function IB_GetParaCnt(n: PAByte): Word;
function IB_GetParaN(A: PAByte; I: Integer): Word;


function IBS_HIBYTE(m: Word): Byte;
function IBS_LOBYTE(m: Word): Byte;

implementation

procedure IB_SetCmdCode(n: PAByte; const m: Word);
begin
  n[0] := IBS_HIBYTE(m);
  n[1] := IBS_LOBYTE(m);
end;

procedure IB_SetParaCount(n: PAByte; const m: Word);
begin
  n[2] := IBS_HIBYTE(m);
  n[3] := IBS_LOBYTE(m);
end;

procedure IB_SetParaN(A: PAByte; const V: Word; I: Integer);
begin
  A[I * 2 + 2] := IBS_HIBYTE(V);
  A[I * 2 + 3] := IBS_LOBYTE(V);
end;

function IB_GetMsgCode(n: PAByte): Word;
begin
  Result:= n[1] + (n[0] * $100)
end;

function IB_GetParaCnt(n: PAByte): Word;
begin
  Result:= n[3] + (n[2] * $100)
end;

function IB_GetParaN(A: PAByte; I: Integer): Word;
begin
  Result:= A[I * 2 + 3] + (A[I * 2 + 2] * $100)
end;

function IBS_HIBYTE(m: Word): Byte;
begin
  Result:= m shr 8;
end;

function IBS_LOBYTE(m: Word): Byte;
begin
  Result:= m and $FF;
end;

end.
