unit LSV2CTRL_2Lib_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// PASTLWTR : 1.2
// File generated on 15/11/2006 12:15:14 from Type Library described below.

// ************************************************************************  //
// Type Lib: C:\WINDOWS\system32\lsv2ctrl_2.ocx (1)
// LIBID: {1B51A951-69A2-11D3-A692-0000C04372DC}
// LCID: 0
// Helpfile: C:\WINDOWS\system32\lsv2ctrl_2.hlp
// HelpString: LSV2ctrl_2 OLE Control module
// DepndLst: 
//   (1) v2.0 stdole, (C:\WINDOWS\system32\STDOLE2.TLB)
// Errors:
//   Hint: Parameter 'Type' of _DLSV2ctrl_2.TransmitMemBlock changed to 'Type_'
//   Hint: Parameter 'Type' of _DLSV2ctrl_2.ReceiveMemBlock changed to 'Type_'
//   Hint: Parameter 'Class' of _DLSV2ctrl_2.SetEventMask changed to 'Class_'
//   Hint: Parameter 'Type' of _DLSV2ctrl_2.SetEventMask changed to 'Type_'
//   Hint: Parameter 'Type' of _DLSV2ctrl_2.GetFileData changed to 'Type_'
//   Hint: Parameter 'Type' of _DLSV2ctrl_2.ProtectFile changed to 'Type_'
//   Hint: Parameter 'Type' of _DLSV2ctrl_2.GetDirData changed to 'Type_'
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
{$WARN SYMBOL_PLATFORM OFF}
{$WRITEABLECONST ON}
{$VARPROPSETTER ON}
interface

uses Windows, ActiveX, Classes, Graphics, OleCtrls, OleServer, StdVCL, Variants;
  


// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  LSV2CTRL_2LibMajorVersion = 1;
  LSV2CTRL_2LibMinorVersion = 0;

  LIBID_LSV2CTRL_2Lib: TGUID = '{1B51A951-69A2-11D3-A692-0000C04372DC}';

  DIID__DLSV2ctrl_2: TGUID = '{1B51A952-69A2-11D3-A692-0000C04372DC}';
  DIID__DLSV2ctrl_2Events: TGUID = '{1B51A953-69A2-11D3-A692-0000C04372DC}';
  CLASS_LSV2ctrl_2: TGUID = '{1B51A954-69A2-11D3-A692-0000C04372DC}';
type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary                    
// *********************************************************************//
  _DLSV2ctrl_2 = dispinterface;
  _DLSV2ctrl_2Events = dispinterface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                       
// (NOTE: Here we map each CoClass to its Default Interface)              
// *********************************************************************//
  LSV2ctrl_2 = _DLSV2ctrl_2;


// *********************************************************************//
// Declaration of structures, unions and aliases.                         
// *********************************************************************//
  PSmallint1 = ^Smallint; {*}


// *********************************************************************//
// DispIntf:  _DLSV2ctrl_2
// Flags:     (4112) Hidden Dispatchable
// GUID:      {1B51A952-69A2-11D3-A692-0000C04372DC}
// *********************************************************************//
  _DLSV2ctrl_2 = dispinterface
    ['{1B51A952-69A2-11D3-A692-0000C04372DC}']
    function ReceiveDSPData(Channel: Smallint; const ParaString: WideString; 
                            var DataBuffer: Smallint): Integer; dispid 64;
    procedure AboutBox; dispid -552;
    function TransmitMemBlock(Type_: Smallint; Address: Smallint; const MemTable: WideString): WordBool; dispid 52;
    function OpenChatWindow(Left: Smallint; Top: Smallint; Columns: Smallint; Rows: Smallint; 
                            Font: Smallint; Flags: Smallint; const Caption: WideString): WordBool; dispid 65;
    function Extract(Mode: Smallint; const FileName: WideString; const Source: WideString; 
                     const Target: WideString): WordBool; dispid 66;
    function ReceiveTableLine(const FileName: WideString; const SQLQuery: WideString; 
                              StartLine: Integer): WideString; dispid 59;
    function Backup(Mode: Smallint; const FileName: WideString; const Source: WideString): WordBool; dispid 62;
    function Restore(Mode: Smallint; const FileName: WideString; const Selection: WideString): WordBool; dispid 63;
    function ReceiveScreen(const FileName: WideString): WordBool; dispid 60;
    function ReceiveMemBlock(Type_: Smallint; Address: Smallint; Count: Smallint): WideString; dispid 53;
    function TransmitDSPString(Channel: Smallint; const CommandString: WideString): WordBool; dispid 56;
    function TransmitPlcString(const DataString: WideString): WordBool; dispid 55;
    function Logout(const KeyWord: WideString): WordBool; dispid 31;
    function Login(const KeyWord: WideString): WordBool; dispid 30;
    function SetEventMask(Mode: Smallint; Class_: Smallint; EventId: Smallint; Sender: Smallint; 
                          Priority: Smallint; const Type_: WideString): WordBool; dispid 58;
    function TransmitGraphicsCommand(Command: Smallint; ServerPort: Smallint): WordBool; dispid 51;
    function TransmitSysCommand(Command: Integer): WordBool; dispid 50;
    function RunProgram(const ProgramName: WideString; RunMode: Smallint): WordBool; dispid 57;
    function TestConnection(Mode: Smallint): WordBool; dispid 54;
    function BreakTransfer: WordBool; dispid 49;
    function GetFileData(const FileName: WideString; Type_: Smallint): WideString; dispid 48;
    function ActivateProgram(const ProgramName: WideString): WordBool; dispid 47;
    function ProtectFile(const FileName: WideString; Type_: Smallint): WordBool; dispid 46;
    function ReceiveLogFile(const FileName: WideString; StartDate: TDateTime; EndDate: TDateTime): WordBool; dispid 45;
    function TransmitKeycode(Keycode: Integer): WordBool; dispid 44;
    function TransmitPlcCommand(Command: Integer): WordBool; dispid 43;
    function TransmitFile(const FileName: WideString; const NewFileName: WideString): WordBool; dispid 42;
    function ReceiveFile(const FileName: WideString; const NewFileName: WideString): WordBool; dispid 41;
    function ChangeDir(const PathName: WideString): WordBool; dispid 34;
    function CopyFile(const FileName: WideString; const NewFileName: WideString): WordBool; dispid 37;
    function DisConnect: WordBool; dispid 40;
    function TransmitChatText(const Text: WideString): WordBool; dispid 61;
    function GetDirData(Type_: Smallint): WideString; dispid 38;
    function Connect: WordBool; dispid 39;
    function DeleteFile(const FileName: WideString): WordBool; dispid 32;
    function MakeDir(const PathName: WideString): WordBool; dispid 35;
    function DeleteDir(const PathName: WideString): WordBool; dispid 36;
    function RenameFile(const FileName: WideString; const NewFileName: WideString): WordBool; dispid 33;
    property TransferProgress: Integer dispid 29;
    property TNCPrivateTree: WordBool dispid 28;
    property LoginPasswordTNC: WideString dispid 15;
    property LoginPasswordSYS: WideString dispid 14;
    property LoginPasswordPLC: WideString dispid 13;
    property TNCKeyLock: WordBool dispid 27;
    property IPAddress: WideString dispid 12;
    property OnlineConverterPath: WideString dispid 11;
    property Medium: Smallint dispid 26;
    property ProgressDialogVisible: Smallint dispid 10;
    property DialogLanguage: Integer dispid 9;
    property BorderStyle: Smallint dispid -504;
    property Font: IFontDisp dispid -512;
    property BackColor: OLE_COLOR dispid -501;
    property BackgroundErrorString: WideString dispid 25;
    property TransferMode: Integer dispid 8;
    property FileServerFunction: WordBool dispid 24;
    property TransferState: Integer dispid 23;
    property BackgroundTransfer: WordBool dispid 7;
    property ProtocolFile: WideString dispid 6;
    property HostFunction: WordBool dispid 22;
    property CanOverwrite: Smallint dispid 5;
    property LastErrorString: WideString dispid 21;
    property LastError: Integer dispid 20;
    property VersionOPT: WideString dispid 19;
    property VersionPLC: WideString dispid 18;
    property VersionNC: WideString dispid 17;
    property VersionTNC: WideString dispid 16;
    property DirMask: WideString dispid 4;
    property AutoBaudRate: WordBool dispid 3;
    property BaudRate: Integer dispid 2;
    property _Port: WideString dispid 0;
    property Port: WideString dispid 1;
    property Caption: WideString dispid -518;
  end;

// *********************************************************************//
// DispIntf:  _DLSV2ctrl_2Events
// Flags:     (4096) Dispatchable
// GUID:      {1B51A953-69A2-11D3-A692-0000C04372DC}
// *********************************************************************//
  _DLSV2ctrl_2Events = dispinterface
    ['{1B51A953-69A2-11D3-A692-0000C04372DC}']
    procedure PLCStatusReceived(Status: Integer); dispid 1;
    procedure PLCMsgReceived(const Message: WideString); dispid 2;
    procedure TransferCompleted(TransferResult: Integer); dispid 3;
    procedure DSPMsgReceived(Channel: Smallint; const Message: WideString); dispid 4;
    procedure NCMsgReceived(Group: Smallint; Command: Smallint; const Message: WideString); dispid 5;
    procedure MsgErrorReceived(ErrorNumber: Integer); dispid 6;
    procedure EventReceived(const EventString: WideString); dispid 7;
  end;


// *********************************************************************//
// OLE Control Proxy class declaration
// Control Name     : TLSV2ctrl_2
// Help String      : HEIDENHAIN LSV2 Control Version 2
// Default Interface: _DLSV2ctrl_2
// Def. Intf. DISP? : Yes
// Event   Interface: _DLSV2ctrl_2Events
// TypeFlags        : (34) CanCreate Control
// *********************************************************************//
  TLSV2ctrl_2PLCStatusReceived = procedure(ASender: TObject; Status: Integer) of object;
  TLSV2ctrl_2PLCMsgReceived = procedure(ASender: TObject; const Message: WideString) of object;
  TLSV2ctrl_2TransferCompleted = procedure(ASender: TObject; TransferResult: Integer) of object;
  TLSV2ctrl_2DSPMsgReceived = procedure(ASender: TObject; Channel: Smallint; 
                                                          const Message: WideString) of object;
  TLSV2ctrl_2NCMsgReceived = procedure(ASender: TObject; Group: Smallint; Command: Smallint; 
                                                         const Message: WideString) of object;
  TLSV2ctrl_2MsgErrorReceived = procedure(ASender: TObject; ErrorNumber: Integer) of object;
  TLSV2ctrl_2EventReceived = procedure(ASender: TObject; const EventString: WideString) of object;

  TLSV2ctrl_2 = class(TOleControl)
  private
    FOnPLCStatusReceived: TLSV2ctrl_2PLCStatusReceived;
    FOnPLCMsgReceived: TLSV2ctrl_2PLCMsgReceived;
    FOnTransferCompleted: TLSV2ctrl_2TransferCompleted;
    FOnDSPMsgReceived: TLSV2ctrl_2DSPMsgReceived;
    FOnNCMsgReceived: TLSV2ctrl_2NCMsgReceived;
    FOnMsgErrorReceived: TLSV2ctrl_2MsgErrorReceived;
    FOnEventReceived: TLSV2ctrl_2EventReceived;
    FIntf: _DLSV2ctrl_2;
    function  GetControlInterface: _DLSV2ctrl_2;
  protected
    procedure CreateControl;
    procedure InitControlData; override;
  public
    function ReceiveDSPData(Channel: Smallint; const ParaString: WideString; 
                            var DataBuffer: Smallint): Integer;
    procedure AboutBox;
    function TransmitMemBlock(Type_: Smallint; Address: Smallint; const MemTable: WideString): WordBool;
    function OpenChatWindow(Left: Smallint; Top: Smallint; Columns: Smallint; Rows: Smallint; 
                            Font: Smallint; Flags: Smallint; const Caption: WideString): WordBool;
    function Extract(Mode: Smallint; const FileName: WideString; const Source: WideString; 
                     const Target: WideString): WordBool;
    function ReceiveTableLine(const FileName: WideString; const SQLQuery: WideString; 
                              StartLine: Integer): WideString;
    function Backup(Mode: Smallint; const FileName: WideString; const Source: WideString): WordBool;
    function Restore(Mode: Smallint; const FileName: WideString; const Selection: WideString): WordBool;
    function ReceiveScreen(const FileName: WideString): WordBool;
    function ReceiveMemBlock(Type_: Smallint; Address: Smallint; Count: Smallint): WideString;
    function TransmitDSPString(Channel: Smallint; const CommandString: WideString): WordBool;
    function TransmitPlcString(const DataString: WideString): WordBool;
    function Logout(const KeyWord: WideString): WordBool;
    function Login(const KeyWord: WideString): WordBool;
    function SetEventMask(Mode: Smallint; Class_: Smallint; EventId: Smallint; Sender: Smallint; 
                          Priority: Smallint; const Type_: WideString): WordBool;
    function TransmitGraphicsCommand(Command: Smallint; ServerPort: Smallint): WordBool;
    function TransmitSysCommand(Command: Integer): WordBool;
    function RunProgram(const ProgramName: WideString; RunMode: Smallint): WordBool;
    function TestConnection(Mode: Smallint): WordBool;
    function BreakTransfer: WordBool;
    function GetFileData(const FileName: WideString; Type_: Smallint): WideString;
    function ActivateProgram(const ProgramName: WideString): WordBool;
    function ProtectFile(const FileName: WideString; Type_: Smallint): WordBool;
    function ReceiveLogFile(const FileName: WideString; StartDate: TDateTime; EndDate: TDateTime): WordBool;
    function TransmitKeycode(Keycode: Integer): WordBool;
    function TransmitPlcCommand(Command: Integer): WordBool;
    function TransmitFile(const FileName: WideString; const NewFileName: WideString): WordBool;
    function ReceiveFile(const FileName: WideString; const NewFileName: WideString): WordBool;
    function ChangeDir(const PathName: WideString): WordBool;
    function CopyFile(const FileName: WideString; const NewFileName: WideString): WordBool;
    function DisConnect: WordBool;
    function TransmitChatText(const Text: WideString): WordBool;
    function GetDirData(Type_: Smallint): WideString;
    function Connect: WordBool;
    function DeleteFile(const FileName: WideString): WordBool;
    function MakeDir(const PathName: WideString): WordBool;
    function DeleteDir(const PathName: WideString): WordBool;
    function RenameFile(const FileName: WideString; const NewFileName: WideString): WordBool;
    property  ControlInterface: _DLSV2ctrl_2 read GetControlInterface;
    property  DefaultInterface: _DLSV2ctrl_2 read GetControlInterface;
  published
    property Anchors;
    property  ParentColor;
    property  ParentFont;
    property  TabStop;
    property  Align;
    property  DragCursor;
    property  DragMode;
    property  ParentShowHint;
    property  PopupMenu;
    property  ShowHint;
    property  TabOrder;
    property  Visible;
    property  OnDragDrop;
    property  OnDragOver;
    property  OnEndDrag;
    property  OnEnter;
    property  OnExit;
    property  OnStartDrag;
    property TransferProgress: Integer index 29 read GetIntegerProp write SetIntegerProp stored False;
    property TNCPrivateTree: WordBool index 28 read GetWordBoolProp write SetWordBoolProp stored False;
    property LoginPasswordTNC: WideString index 15 read GetWideStringProp write SetWideStringProp stored False;
    property LoginPasswordSYS: WideString index 14 read GetWideStringProp write SetWideStringProp stored False;
    property LoginPasswordPLC: WideString index 13 read GetWideStringProp write SetWideStringProp stored False;
    property TNCKeyLock: WordBool index 27 read GetWordBoolProp write SetWordBoolProp stored False;
    property IPAddress: WideString index 12 read GetWideStringProp write SetWideStringProp stored False;
    property OnlineConverterPath: WideString index 11 read GetWideStringProp write SetWideStringProp stored False;
    property Medium: Smallint index 26 read GetSmallintProp write SetSmallintProp stored False;
    property ProgressDialogVisible: Smallint index 10 read GetSmallintProp write SetSmallintProp stored False;
    property DialogLanguage: Integer index 9 read GetIntegerProp write SetIntegerProp stored False;
    property BorderStyle: Smallint index -504 read GetSmallintProp write SetSmallintProp stored False;
    property Font: TFont index -512 read GetTFontProp write SetTFontProp stored False;
    property BackColor: TColor index -501 read GetTColorProp write SetTColorProp stored False;
    property BackgroundErrorString: WideString index 25 read GetWideStringProp write SetWideStringProp stored False;
    property TransferMode: Integer index 8 read GetIntegerProp write SetIntegerProp stored False;
    property FileServerFunction: WordBool index 24 read GetWordBoolProp write SetWordBoolProp stored False;
    property TransferState: Integer index 23 read GetIntegerProp write SetIntegerProp stored False;
    property BackgroundTransfer: WordBool index 7 read GetWordBoolProp write SetWordBoolProp stored False;
    property ProtocolFile: WideString index 6 read GetWideStringProp write SetWideStringProp stored False;
    property HostFunction: WordBool index 22 read GetWordBoolProp write SetWordBoolProp stored False;
    property CanOverwrite: Smallint index 5 read GetSmallintProp write SetSmallintProp stored False;
    property LastErrorString: WideString index 21 read GetWideStringProp write SetWideStringProp stored False;
    property LastError: Integer index 20 read GetIntegerProp write SetIntegerProp stored False;
    property VersionOPT: WideString index 19 read GetWideStringProp write SetWideStringProp stored False;
    property VersionPLC: WideString index 18 read GetWideStringProp write SetWideStringProp stored False;
    property VersionNC: WideString index 17 read GetWideStringProp write SetWideStringProp stored False;
    property VersionTNC: WideString index 16 read GetWideStringProp write SetWideStringProp stored False;
    property DirMask: WideString index 4 read GetWideStringProp write SetWideStringProp stored False;
    property AutoBaudRate: WordBool index 3 read GetWordBoolProp write SetWordBoolProp stored False;
    property BaudRate: Integer index 2 read GetIntegerProp write SetIntegerProp stored False;
    property _Port: WideString index 0 read GetWideStringProp write SetWideStringProp stored False;
    property Port: WideString index 1 read GetWideStringProp write SetWideStringProp stored False;
    property Caption: WideString index -518 read GetWideStringProp write SetWideStringProp stored False;
    property OnPLCStatusReceived: TLSV2ctrl_2PLCStatusReceived read FOnPLCStatusReceived write FOnPLCStatusReceived;
    property OnPLCMsgReceived: TLSV2ctrl_2PLCMsgReceived read FOnPLCMsgReceived write FOnPLCMsgReceived;
    property OnTransferCompleted: TLSV2ctrl_2TransferCompleted read FOnTransferCompleted write FOnTransferCompleted;
    property OnDSPMsgReceived: TLSV2ctrl_2DSPMsgReceived read FOnDSPMsgReceived write FOnDSPMsgReceived;
    property OnNCMsgReceived: TLSV2ctrl_2NCMsgReceived read FOnNCMsgReceived write FOnNCMsgReceived;
    property OnMsgErrorReceived: TLSV2ctrl_2MsgErrorReceived read FOnMsgErrorReceived write FOnMsgErrorReceived;
    property OnEventReceived: TLSV2ctrl_2EventReceived read FOnEventReceived write FOnEventReceived;
  end;

procedure Register;

resourcestring
  dtlServerPage = 'ActiveX';

  dtlOcxPage = 'ActiveX';

implementation

uses ComObj;

procedure TLSV2ctrl_2.InitControlData;
const
  CEventDispIDs: array [0..6] of DWORD = (
    $00000001, $00000002, $00000003, $00000004, $00000005, $00000006,
    $00000007);
  CTFontIDs: array [0..0] of DWORD = (
    $FFFFFE00);
  CControlData: TControlData2 = (
    ClassID: '{1B51A954-69A2-11D3-A692-0000C04372DC}';
    EventIID: '{1B51A953-69A2-11D3-A692-0000C04372DC}';
    EventCount: 7;
    EventDispIDs: @CEventDispIDs;
    LicenseKey: nil (*HR:$80004005*);
    Flags: $00000015;
    Version: 401;
    FontCount: 1;
    FontIDs: @CTFontIDs);
begin
  ControlData := @CControlData;
  TControlData2(CControlData).FirstEventOfs := Cardinal(@@FOnPLCStatusReceived) - Cardinal(Self);
end;

procedure TLSV2ctrl_2.CreateControl;

  procedure DoCreate;
  begin
    FIntf := IUnknown(OleObject) as _DLSV2ctrl_2;
  end;

begin
  if FIntf = nil then DoCreate;
end;

function TLSV2ctrl_2.GetControlInterface: _DLSV2ctrl_2;
begin
  CreateControl;
  Result := FIntf;
end;

function TLSV2ctrl_2.ReceiveDSPData(Channel: Smallint; const ParaString: WideString; 
                                    var DataBuffer: Smallint): Integer;
begin
  Result := DefaultInterface.ReceiveDSPData(Channel, ParaString, DataBuffer);
end;

procedure TLSV2ctrl_2.AboutBox;
begin
  DefaultInterface.AboutBox;
end;

function TLSV2ctrl_2.TransmitMemBlock(Type_: Smallint; Address: Smallint; const MemTable: WideString): WordBool;
begin
  Result := DefaultInterface.TransmitMemBlock(Type_, Address, MemTable);
end;

function TLSV2ctrl_2.OpenChatWindow(Left: Smallint; Top: Smallint; Columns: Smallint; 
                                    Rows: Smallint; Font: Smallint; Flags: Smallint; 
                                    const Caption: WideString): WordBool;
begin
  Result := DefaultInterface.OpenChatWindow(Left, Top, Columns, Rows, Font, Flags, Caption);
end;

function TLSV2ctrl_2.Extract(Mode: Smallint; const FileName: WideString; const Source: WideString; 
                             const Target: WideString): WordBool;
begin
  Result := DefaultInterface.Extract(Mode, FileName, Source, Target);
end;

function TLSV2ctrl_2.ReceiveTableLine(const FileName: WideString; const SQLQuery: WideString; 
                                      StartLine: Integer): WideString;
begin
  Result := DefaultInterface.ReceiveTableLine(FileName, SQLQuery, StartLine);
end;

function TLSV2ctrl_2.Backup(Mode: Smallint; const FileName: WideString; const Source: WideString): WordBool;
begin
  Result := DefaultInterface.Backup(Mode, FileName, Source);
end;

function TLSV2ctrl_2.Restore(Mode: Smallint; const FileName: WideString; const Selection: WideString): WordBool;
begin
  Result := DefaultInterface.Restore(Mode, FileName, Selection);
end;

function TLSV2ctrl_2.ReceiveScreen(const FileName: WideString): WordBool;
begin
  Result := DefaultInterface.ReceiveScreen(FileName);
end;

function TLSV2ctrl_2.ReceiveMemBlock(Type_: Smallint; Address: Smallint; Count: Smallint): WideString;
begin
  Result := DefaultInterface.ReceiveMemBlock(Type_, Address, Count);
end;

function TLSV2ctrl_2.TransmitDSPString(Channel: Smallint; const CommandString: WideString): WordBool;
begin
  Result := DefaultInterface.TransmitDSPString(Channel, CommandString);
end;

function TLSV2ctrl_2.TransmitPlcString(const DataString: WideString): WordBool;
begin
  Result := DefaultInterface.TransmitPlcString(DataString);
end;

function TLSV2ctrl_2.Logout(const KeyWord: WideString): WordBool;
begin
  Result := DefaultInterface.Logout(KeyWord);
end;

function TLSV2ctrl_2.Login(const KeyWord: WideString): WordBool;
begin
  Result := DefaultInterface.Login(KeyWord);
end;

function TLSV2ctrl_2.SetEventMask(Mode: Smallint; Class_: Smallint; EventId: Smallint; 
                                  Sender: Smallint; Priority: Smallint; const Type_: WideString): WordBool;
begin
  Result := DefaultInterface.SetEventMask(Mode, Class_, EventId, Sender, Priority, Type_);
end;

function TLSV2ctrl_2.TransmitGraphicsCommand(Command: Smallint; ServerPort: Smallint): WordBool;
begin
  Result := DefaultInterface.TransmitGraphicsCommand(Command, ServerPort);
end;

function TLSV2ctrl_2.TransmitSysCommand(Command: Integer): WordBool;
begin
  Result := DefaultInterface.TransmitSysCommand(Command);
end;

function TLSV2ctrl_2.RunProgram(const ProgramName: WideString; RunMode: Smallint): WordBool;
begin
  Result := DefaultInterface.RunProgram(ProgramName, RunMode);
end;

function TLSV2ctrl_2.TestConnection(Mode: Smallint): WordBool;
begin
  Result := DefaultInterface.TestConnection(Mode);
end;

function TLSV2ctrl_2.BreakTransfer: WordBool;
begin
  Result := DefaultInterface.BreakTransfer;
end;

function TLSV2ctrl_2.GetFileData(const FileName: WideString; Type_: Smallint): WideString;
begin
  Result := DefaultInterface.GetFileData(FileName, Type_);
end;

function TLSV2ctrl_2.ActivateProgram(const ProgramName: WideString): WordBool;
begin
  Result := DefaultInterface.ActivateProgram(ProgramName);
end;

function TLSV2ctrl_2.ProtectFile(const FileName: WideString; Type_: Smallint): WordBool;
begin
  Result := DefaultInterface.ProtectFile(FileName, Type_);
end;

function TLSV2ctrl_2.ReceiveLogFile(const FileName: WideString; StartDate: TDateTime; 
                                    EndDate: TDateTime): WordBool;
begin
  Result := DefaultInterface.ReceiveLogFile(FileName, StartDate, EndDate);
end;

function TLSV2ctrl_2.TransmitKeycode(Keycode: Integer): WordBool;
begin
  Result := DefaultInterface.TransmitKeycode(Keycode);
end;

function TLSV2ctrl_2.TransmitPlcCommand(Command: Integer): WordBool;
begin
  Result := DefaultInterface.TransmitPlcCommand(Command);
end;

function TLSV2ctrl_2.TransmitFile(const FileName: WideString; const NewFileName: WideString): WordBool;
begin
  Result := DefaultInterface.TransmitFile(FileName, NewFileName);
end;

function TLSV2ctrl_2.ReceiveFile(const FileName: WideString; const NewFileName: WideString): WordBool;
begin
  Result := DefaultInterface.ReceiveFile(FileName, NewFileName);
end;

function TLSV2ctrl_2.ChangeDir(const PathName: WideString): WordBool;
begin
  Result := DefaultInterface.ChangeDir(PathName);
end;

function TLSV2ctrl_2.CopyFile(const FileName: WideString; const NewFileName: WideString): WordBool;
begin
  Result := DefaultInterface.CopyFile(FileName, NewFileName);
end;

function TLSV2ctrl_2.DisConnect: WordBool;
begin
  Result := DefaultInterface.DisConnect;
end;

function TLSV2ctrl_2.TransmitChatText(const Text: WideString): WordBool;
begin
  Result := DefaultInterface.TransmitChatText(Text);
end;

function TLSV2ctrl_2.GetDirData(Type_: Smallint): WideString;
begin
  Result := DefaultInterface.GetDirData(Type_);
end;

function TLSV2ctrl_2.Connect: WordBool;
begin
  Result := DefaultInterface.Connect;
end;

function TLSV2ctrl_2.DeleteFile(const FileName: WideString): WordBool;
begin
  Result := DefaultInterface.DeleteFile(FileName);
end;

function TLSV2ctrl_2.MakeDir(const PathName: WideString): WordBool;
begin
  Result := DefaultInterface.MakeDir(PathName);
end;

function TLSV2ctrl_2.DeleteDir(const PathName: WideString): WordBool;
begin
  Result := DefaultInterface.DeleteDir(PathName);
end;

function TLSV2ctrl_2.RenameFile(const FileName: WideString; const NewFileName: WideString): WordBool;
begin
  Result := DefaultInterface.RenameFile(FileName, NewFileName);
end;

procedure Register;
begin
  RegisterComponents(dtlOcxPage, [TLSV2ctrl_2]);
end;

end.
