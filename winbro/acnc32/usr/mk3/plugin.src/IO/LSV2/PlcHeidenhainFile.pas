unit PlcHeidenhainFile;

interface

uses AbstractPlcPlugin, CncTypes, HeidenhainTable, Stub, TableManager, SysUtils;

type
  TPlcHeidenhainValue = (
    hvTxFile,
    hvRxFile,
    hvReset,
    hvBusy,
    hvError
  );
  TPlcHeidenhainValues = set of TPlcHeidenhainValue;

  TPlcHeidenhainFile = class(TAbstractPlcPlugin)
  private
    ResolveEv: TPlcPluginResolveItemValueEv;
    FileManager: THeidenhainTableManager;
    Values: TPlcHeidenhainValues;
    SendFile: Integer;
    ReceiveFile: Integer;
  public
    constructor Create( Parameters : string;
                        ResolveDeviceEv : TPlcPluginResolveDeviceEv;
                        ResolveItemEv : TPlcPluginResolveItemEv); override;
    procedure Compile ( var Embedded : TEmbeddedData;
                        aReadEv : TPlcPluginReadEv;
                        ResolveItemEv : TPlcPluginResolveItemEv;
                        Write : Boolean);  override;
    procedure GatePre; override;
    function GatePost : Integer; override;
    function Read(var Embedded : TEmbeddedData) : Boolean; override;
    procedure Write(var Embedded : TEmbeddedData; Line, LastLine : Boolean); override;
    function GateName(var Embedded : TEmbeddedData) : string; override;
    function ItemProperties : string; override;
    class procedure Shutdown; override;
  end;

implementation

const
  PropertyName : array [TPlcHeidenhainValue] of TPlcProperty = (
    ( Name : 'TxFile'; ReadOnly : False ),
    ( Name : 'RxFile'; ReadOnly : False ),
    ( Name : 'Reset';  ReadOnly : False ),
    ( Name : 'Busy';   ReadOnly : True ),
    ( Name : 'Error';  ReadOnly : True )
  );

{ TPlcHeidenhainFile }

procedure TPlcHeidenhainFile.Write(var Embedded: TEmbeddedData; Line,
  LastLine: Boolean);
var HFile: THeidenhainTableFile;
begin

  if Line and not LastLine then begin  // Rising edge detect
    case TPlcHeidenhainValue(Embedded.Command) of
      hvTxFile : begin
        if (SendFile = 0) and (ReceiveFile = 0) then begin
          SendFile:= ResolveEv(Self, Embedded.OData);
          HFile:= FileManager.GetTableByIndex(SendFile);
          if Stub.LSV2 <> nil then begin
            if HFile <> nil then
              Stub.LSV2.Thread.SendFileDelayed(HFile)
            else
              Include(Values, hvError);
          end else begin
            HFile.HTableFromMDT;
          end;
        end
        else
          Include(Values, hvError);
      end;
      hvRxFile : begin
        if Stub.LSV2 <> nil then begin
          if (SendFile = 0) and (ReceiveFile = 0) then begin
            ReceiveFile:= ResolveEv(Self, Embedded.OData);
            HFile:= FileManager.GetTableByIndex(ReceiveFile);
            if HFile <> nil then
              Stub.LSV2.Thread.ReceiveFileDelayed(HFile)
            else
              Include(Values, hvError);
          end
          else
            Include(Values, hvError);
        end;
      end;

      hvReset: begin
        if Stub.LSV2 <> nil then begin
          Stub.LSV2.Thread.FileError:= False;
        end;
      end;
    end;
    Include(Values, TPlcHeidenhainValue(Embedded.Command));
  end
  else if not Line and LastLine then begin
    case TPlcHeidenhainValue(Embedded.Command) of
      hvTxFile : SendFile:= 0;
      hvRxFile : ReceiveFile:= 0;
    end;
    Exclude(Values, TPlcHeidenhainValue(Embedded.Command));
  end;
end;

procedure TPlcHeidenhainFile.Compile(var Embedded: TEmbeddedData;
  aReadEv: TPlcPluginReadEv; ResolveItemEv: TPlcPluginResolveItemEv;
  Write: Boolean);
var Token: string;
    I: TPlcHeidenhainValue;
    IsConstant : Boolean;
begin
  Token := aReadEv(Self);
  if Token <> '.' then
    raise Exception.Create('Period [.] expected');

  Token := aReadEv(Self);
  for I := Low(I) to High(I) do begin
    if CompareText(PropertyName[I].Name, Token) = 0 then begin
      Embedded.Command := Ord(I);
      if Write and PropertyName[I].Readonly then
        raise Exception.Create('Attempting to write to a readonly property: ' + PropertyName[I].Name);

      if Write and (I <> hvReset) then begin // All write values are . notated
        Token := aReadEv(Self);
        if Token <> '.' then
          raise Exception.Create('Period [.] expected');

        Token := aReadEv(Self);
        Embedded.OData := ResolveItemEv(Self, PChar(Token), IsConstant, ResolveEv);
        if Embedded.OData = nil then begin
          raise Exception.CreateFmt('Cannot resolve [%s] to a value', [Token]);
        end;
      end;
      Exit;
    end;
  end;
  raise Exception.CreateFmt('Unknown property [%s]', [Token]);
end;

function TPlcHeidenhainFile.GateName(var Embedded: TEmbeddedData): string;
begin
  Result := PropertyName[TPlcHeidenhainValue(Embedded.Command)].Name;
end;

constructor TPlcHeidenhainFile.Create(Parameters: string;
  ResolveDeviceEv: TPlcPluginResolveDeviceEv;
  ResolveItemEv: TPlcPluginResolveItemEv);
var
  Token: string;
begin
  Token:= ReadDelimited(Parameters, ';');
  FileManager:= THeidenhainTableManager.Create;
end;

class procedure TPlcHeidenhainFile.Shutdown;
begin
end;

function TPlcHeidenhainFile.ItemProperties: string;
var I: TPlcHeidenhainValue;
begin
  for I := Low(I) to High(I) do
    Result := Result + PropertyName[I].Name + CarRet + BooleanIdent[I in Values] + CarRet;

  Result := Result + 'Receive Index' + CarRet + IntToStr(ReceiveFile) + CarRet;
  Result := Result + 'Send Index' + CarRet + IntToStr(SendFile) + CarRet;
  if Stub.LSV2 <> nil then
    Result := Result + 'Last Error' + CarRet +  Stub.LSV2.Thread.ErrorString + CarRet;

end;

function TPlcHeidenhainFile.GatePost: Integer;
begin
  if Stub.LSV2 <> nil then begin
    if Stub.LSV2.Thread.FileBusy then
      Include(Values, hvBusy);

    if Stub.LSV2.Thread.FileError then
      Include(Values, hvError)
    else
      Exclude(Values, hvError);

    if (SendFile = 0) and (ReceiveFile = 0) then begin
      Exclude(Values, hvBusy);
    end;
  end;

  Result:= 0;
end;

function TPlcHeidenhainFile.Read(var Embedded: TEmbeddedData): Boolean;
var I: TPlcHeidenhainValue;
begin
  I:= TPlcHeidenhainValue(Embedded.Command);
  case I of
    hvBusy: begin
      if Stub.LSV2 <> nil then
        Result:= Stub.LSV2.Thread.FileBusy
      else
        Result:= False;
    end;
  else
    Result := I in Values;
  end;
end;

procedure TPlcHeidenhainFile.GatePre;
begin

end;

initialization
  TAbstractPlcPlugin.AddPlugin('HeidenhainFile', TPlcHeidenhainFile);
end.
