unit Stub;

interface

uses Windows, Classes, INamedInterface, CNCTypes, SysUtils, SyncObjs,
     AbstractIOSystemAccessor, NamedPlugin, LSV2CTRL_2Lib_TLB, TableManager;

type
  TLSV2RunThread = class;

  TLSV2 = class(TAbstractIOSystemAccessor)
  private
    FTextDef: string;
    FInputSize: Integer;
    FOutputSize: Integer;

    FInputBase: Integer;
    FOutputBase: Integer;
    IP: string;
    Password: string;

    Host: IIOSystemAccessorHost;
    FThread: TLSV2RunThread;
    ThreadKindness: Integer;

    FInputArea: array [0..511] of Byte;
    FOutputArea: array [0..511] of Byte;
  public
    constructor Create; override;
    function GetInfoPageContent(aPage : Integer) : WideString; override;
    procedure Initialise(Host: IIOSystemAccessorHost; const Params : WideString); override;
    procedure UpdateShadowToReal; override;
    procedure UpdateRealToShadow; override;
    function Healthy : Boolean; override;
    procedure Reset; override;
    function LastError : WideString; override;
    function GetInputArea: Pointer; override;
    function GetOutputArea: Pointer; override;
    function GetInputSize: Integer; override;
    function GetOutputSize: Integer; override;
    function TextDefinition: WideString; override;
    procedure Shutdown; override;
    property Thread: TLSV2RunThread read FThread;
  end;

  TLSV2RunThread = class(TThread)
  private
    Accessor: TLSV2;
    FInReset: Boolean;
    Lock: TCriticalSection;
    InData: array[0..511] of Byte;
    OutData: array[0..511] of Byte;
    TNC: TLSV2ctrl_2;
    FConnected: Boolean;
    FErrorString: string;

    FFileErrorMessage: string; // Active on error until next request
    FFileError: Boolean; // Active on error until next request
    FFileSend: Boolean; // Internal flag indicating action
    FFileReceive: Boolean; // Internal flag indicating action

    HFile: THeidenhainTableFile;

    FIP: string;

    OutStream: TStringStream;
    procedure CreateTNC;
    procedure ConnectTNC;
    procedure TranslateInputBuffers;
    procedure TranslateOutputBuffers;
    function SendFile(LocalPath, RemotePath: string): Boolean;
    function ReceiveFile(LocalPath, RemotePath: string): Boolean;
    function GetFileBusy: Boolean;
  public
    constructor Create(S: TLSV2; InSize, OutSize: Integer; const IP: string);
    procedure Execute; override;
    property InReset: Boolean read FInReset write FInReset;
    property Connected: Boolean read FConnected;
    property ErrorString: string read FErrorString;
    property IP: string read FIP write FIP;
    property FileErrorString: string read FFileErrorMessage;
    // Send and receive must only be called while !FileBusy
    procedure SendFileDelayed(AHFile: THeidenhainTableFile);
    procedure ReceiveFileDelayed(AHFile: THeidenhainTableFile);

    property FileBusy: Boolean read GetFileBusy;
    property FileError: Boolean read FFileError write FFileError;
  end;

var
  LSV2: TLSV2;

implementation

{ TLSV2 }

function TLSV2.GetOutputSize: Integer;
begin
  Result:= FOutputSize;
end;

function TLSV2.GetInfoPageContent(aPage: Integer): WideString;
begin
  Result:= '';
end;

constructor TLSV2.Create;
begin
  inherited;
  LSV2:= Self;
end;

function TLSV2.GetOutputArea: Pointer;
begin
  Result:= @FOutputArea;
end;

procedure TLSV2.Shutdown;
begin
  Thread.Terminate;
end;

function TLSV2.GetInputSize: Integer;
begin
  Result:= FInputSize;
end;

procedure TLSV2.Reset;
begin
  if not Healthy then
    Thread.InReset:= True;
end;

function TLSV2.GetInputArea: Pointer;
begin
  Result:= @FInputArea;
end;

function TLSV2.TextDefinition: WideString;
begin
  Result:= FTextDef;
end;

function TLSV2.Healthy: Boolean;
begin
  Result := Thread.Connected;
end;

function TLSV2.LastError: WideString;
begin
  if Assigned(Thread) then
    Result:= Thread.ErrorString
  else
    Result:= 'Not Configured';
end;

procedure TLSV2.Initialise(Host: IIOSystemAccessorHost;
  const Params: WideString);
var Tmp: string;
begin
  FTextDef:= Params;
  Tmp:= Params;
  Self.Host:= Host;
  FInputSize:= StrToIntDef(CncTypes.ReadDelimited(Tmp, ';'), 10);
  FOutputSize:= StrToIntDef(CncTypes.ReadDelimited(Tmp, ';'), 10);
  FInputBase:= StrToIntDef(CncTypes.ReadDelimited(Tmp, ';'), $2176);
  FOutputBase:= StrToIntDef(CncTypes.ReadDelimited(Tmp, ';'), $2048);
  IP:= CncTypes.ReadDelimited(Tmp, ';');
  Password:= CncTypes.ReadDelimited(Tmp, ';');
  ThreadKindness:= StrToIntDef(CncTypes.ReadDelimited(Tmp, ';'), 100);

  FInputSize:= CncTypes.RangeInt(FInputSize, 2, 512);
  FOutputSize:= CncTypes.RangeInt(FOutputSize, 2, 512);

  FThread:= TLSV2RunThread.Create(Self, FInputSize, FOutputSize, IP);
  Thread.IP:= IP;
  Thread.CreateTNC;
  Thread.Resume;
end;

procedure TLSV2.UpdateRealToShadow;
begin
  Thread.Lock.Acquire;
  try
    CopyMemory(@FInputArea, @Thread.InData, FInputSize);
  finally
    Thread.Lock.Release;
  end;
end;

procedure TLSV2.UpdateShadowToReal;
begin
  Thread.Lock.Acquire;
  try
    CopyMemory(@Thread.OutData, @FOutputArea, FOutputSize);
  finally
    Thread.Lock.Release;
  end;

end;

{ TLSV2RunThread }

constructor TLSV2RunThread.Create(S: TLSV2; InSize, OutSize: Integer;
  const IP: string);
begin
  inherited Create(True);
  Accessor:= S;
  Lock:= TCriticalSection.Create;
  OutStream:= TStringStream.Create('');
end;

procedure TLSV2RunThread.Execute;
begin
  Self.Priority:= tpHigher;

  FConnected:= False;

  Sleep(3000);
  //Synchronize(CreateTNC);

  while not Terminated do begin
    while Connected and not Terminated do begin
      TranslateInputBuffers;
      TranslateOutputBuffers;
      Sleep(Accessor.ThreadKindness);

      if FFileSend then begin
        FFileError:= False;
        try
          HFile.HTableFromMDT;
        except
          on E: Exception do begin
            Named.EventLog('Exception E:' + E.Message);
            FFileError:= True;
            FFileSend:= False;
            Self.FErrorString:= 'Transfer Failed: ' + E.Message;
            Exit;
          end;
        end;

        FFileError:= not Self.SendFile(HFile.LFilename, HFile.HFilename);
        FFileSend:= False;
      end
      else if FFileReceive then begin
        FFileError:= False;
        try
          FFileError:= not Self.ReceiveFile(HFile.LFilename, HFile.HFilename);
          HFile.MDTFromHTable;
        except
          on E: Exception do begin
            FFileError:= True;
            FFileReceive:= False;
            Self.FErrorString:= 'Transfer Failed: ' + E.Message;
            Named.EventLog('Exception E:' + E.Message);
          end;
        end;
        FFileReceive:= False;
      end;
    end;

    while not Connected and not Terminated do begin
      Sleep(3000);
      ConnectTNC;
    end;
  end;

  TNC.DisConnect;
  TNC:= nil;
end;

procedure TLSV2RunThread.CreateTNC;
begin
  try
    TNC := TLSV2ctrl_2.Create(nil)
  except
    FErrorString:= 'LSV2 Not Installed';
    FConnected:= False;
    Self.Terminate;
  end;

  TNC.Medium:= 1;
  TNC.IPAddress := IP;
  TNC.LoginPasswordPLC := Accessor.Password;
  TNC.ProtocolFile:= DllLoggingPath + 'LSV2.log';
end;

procedure TLSV2RunThread.ConnectTNC;
begin
  if TNC.Connect then begin
    FConnected:= True;
    TNC.BackgroundTransfer:= False;
  end;
end;

procedure TLSV2RunThread.TranslateInputBuffers;
var Inmap, Tmp: string;
    I, K: Integer;
begin
  InMap := TNC.ReceiveMemBlock(7, Accessor.FInputBase, Accessor.FInputSize div 4);
  if InMap = '' then begin
    FConnected:= False;
  end;

  Lock.Acquire;
  try
    I:= 0;
    while (InMap <> '') and (I < Accessor.FInputSize) do begin
      Tmp:= '$' + ReadDelimited(InMap, ' ');
      K:= StrToIntDef(Tmp, 0);
      InData[I]:= K;
      InData[I + 1]:= K shr 8;
      InData[I + 2]:= K shr 16;
      InData[I + 3]:= K shr 24;
      Inc(I, 4);
    end;
  finally
    Lock.Release;
  end;
end;

procedure TLSV2RunThread.TranslateOutputBuffers;
var OutMap: string;
    I: Integer;
    V: Integer;
begin
  OutStream.Seek(soFromBeginning, 0);
  I:= 0;
  Lock.Acquire;

  try
    while I< Accessor.FOutputSize do begin
      V:= OutData[I];
      V:= V + OutData[I+1] * $100;
      V:= V + OutData[I+2] * $10000;
      V:= V + OutData[I+3] * $1000000;
      OutStream.WriteString(IntToHex(V, 4) + ' ');
      Inc(I, 4);
    end;
  finally
    Lock.Release;
  end;
  OutMap:= Trim(OutStream.DataString);
  TNC.TransmitMemBlock(7, Accessor.FOutputBase, OutMap);
end;

function TLSV2RunThread.SendFile(LocalPath, RemotePath: string): Boolean;
begin
  HFile.HTableFromMDT;
  TNC.CanOverwrite:= SmallInt(True);
  Result:= TNC.TransmitFile(LocalPath, RemotePath);
  if not Result then begin
    FFileErrorMessage:= TNC.LastErrorString;
    Named.EventLog('TX HFile failed: ' + FFileErrorMessage);
    FFileError:= True;
  end;
end;

function TLSV2RunThread.ReceiveFile(LocalPath, RemotePath: string): Boolean;
begin
  TNC.CanOverwrite:= SmallInt(True);
  Result:= TNC.ReceiveFile(RemotePath, LocalPath);
  //Sleep(500);

  if not Result then begin
    FFileErrorMessage:= TNC.LastErrorString;
    Named.EventLog('RX HFile failed: ' + FFileErrorMessage);
    FFileError:= True;
  end else begin
    HFile.MDTFromHTable;
  end;
end;


procedure TLSV2RunThread.SendFileDelayed(AHFile: THeidenhainTableFile);
begin
  Self.HFile:= AHFile;
  FFileErrorMessage:= '';
  FFileSend:= True;
  FFileError:= False;
end;

procedure TLSV2RunThread.ReceiveFileDelayed(AHFile: THeidenhainTableFile);
begin
  Self.HFile:= AHFile;
  FFileErrorMessage:= '';
  FFileReceive:= True;
  FFileError:= False;
end;

function TLSV2RunThread.GetFileBusy: Boolean;
begin
  Result:= FFileSend or FFileReceive;
end;

initialization
  TLSV2.AddIOSystemAccessor('EthLSV2', TLSV2);
end.
