unit TableManager;

interface

uses Classes, SysUtils, CNCTypes, HeidenhainTable, NamedPlugin,
     StreamTokenizer, IStringArray;

type
  THeidenhainTableFile = class
  private
    FIndex: Integer;
    FHFilename: string; // Filename + path on heidenhain
    FLFilename: string; // Local Filename existing in local file store
    MDTName: string; // Name of MDT if this is a table

    HTable: THeidenhainTable; // nil unless MDTname specified
    MDT: IACNC32StringArray;
  public
    constructor Create;
    destructor Destroy; override;
    procedure MDTFromHTable;
    procedure HTableFromMDT;

    property Index: Integer read FIndex;
    property HFilename: string read FHFilename;
    property LFilename: string read FLFilename;
  end;

  THeidenhainTableManager = class
  private
    TableList: TList;
    LocalHTableStore: string; // Directory for MDT templates

    procedure CheckInstallation;
    procedure ReadConfig;
    function GetHeidenhainTableFile(Index: Integer): THeidenhainTableFile;
  public
    constructor Create;
    destructor Destroy; override;
    property Tables[Index: Integer]: THeidenhainTableFile read GetHeidenhainTableFile;
    function TableCount: Integer;
    function GetTableByIndex(Index: Integer): THeidenhainTableFile;
  end;

implementation

{ THeidenhainFileManager }

constructor THeidenhainTableManager.Create;
begin
  inherited;
  LocalHTableStore:= DllProfilePath + 'HeidenhainFile\';
  SysUtils.ForceDirectories(LocalHTableStore);
  TableList:= TList.Create;
  ReadConfig;
  CheckInstallation;
end;

destructor THeidenhainTableManager.Destroy;
begin
  if TableList <> nil then begin
    while TableList.Count > 0 do begin
      GetHeidenhainTableFile(0).Free;
      TableList.Delete(0);
    end;
  end;
  TableList.Free;
  inherited;
end;

procedure THeidenhainTableManager.CheckInstallation;
var I: Integer;
    Table: THeidenhainTableFile;
    LocalFile: string;
    //Stream: TFileStream;
begin
  for I:= 0 to TableCount - 1 do begin
    Table:= Tables[I];
    if Table.MDTName <> '' then begin
      Table.HTable:= THeidenhainTable.Create;
      StringArraySet.UseArray(Table.MDTName, nil, Table.MDT);
      if Table.MDT = nil then begin
        StringArraySet.CreateArray( Table.MDTName, nil, Table.MDT);
      end;

      Table.HTable.Filename:= Table.HFilename;
      LocalFile:= Self.LocalHTableStore + Table.LFilename;
      try
        //Stream:= TFileStream.Create(Table.LFilename, fmOpenRead);
        //Table.HTable.Read(Stream);
        Table.MDTFromHTable;
        //Stream.Free;
      except
        on E: Exception do
          raise Exception.Create('HeidenhainTable Unable to load ' + Table.LFilename + ' ' + E.Message);
      end;
    end;
  end;
end;

procedure THeidenhainTableManager.ReadConfig;
var Token: string;

  function AddFile(S: TStreamTokenizer): THeidenhainTableFile;
  begin
    Result:= THeidenhainTableFile.Create;
    Result.FIndex:= TableList.Count + 1;
    S.ExpectedTokens(['=', '{']);
    Token:= LowerCase(S.Read);
    while not S.EndOfStream do begin
      if Token = 'hfile' then
        Result.FHFilename:= S.ReadStringAssign
      else if Token = 'lfile' then
        Result.FLFilename:= LocalHTableStore + S.ReadStringAssign
      else if Token = 'index' then
        Result.FIndex:= S.ReadIntegerAssign
      else if Token = 'mdt' then
        Result.MDTName:= S.ReadStringAssign
      else if Token = '}' then
        Exit
      else
        raise Exception.CreateFmt('Unexpected Token [%s] at line [%d]', [Token, S.Line]);
      Token := LowerCase(S.Read);
    end;
    raise Exception.CreateFmt('Unexpected end of file at line [%d]', [S.Line]);
  end;

var S: TStreamQuoteTokenizer;
begin
  S := TStreamQuoteTokenizer.Create;
  S.Seperator := '[]{}';
  S.QuoteChar := '''';
  S.CommentChar := '#';

  try
    try
      S.SetStream( TFileStream.Create(DllProfilePath + 'HeidenhainFile.cfg', fmOpenReadWrite));
      S.ExpectedTokens(['HeidenhainFileManager', '=', '{']);
      Token := LowerCase(S.Read);
      while not S.EndOfStream do begin
        if Token = 'file' then
          TableList.Add(AddFile(S))
        else if Token = '}' then
          Exit
        else
          raise Exception.CreateFmt('Unexpected Token [%s] at line [%d]', [Token, S.Line]);
        Token := LowerCase(S.Read);
      end;
      raise Exception.CreateFmt('Unexpected end of file at line [%d]', [S.Line]);
    except
      on E : Exception do
        raise ECNCInstallFault.CreateFmt('%s failed to load configuration file [%s]', ['HeidenhainFileManager', E.Message]);
    end;
  finally
    S.Free;
  end;
end;

function THeidenhainTableManager.GetHeidenhainTableFile(Index: Integer): THeidenhainTableFile;
begin
  Result:= THeidenhainTableFile(TableList[Index]);
end;

function THeidenhainTableManager.TableCount: Integer;
begin
  Result:= TableList.Count;
end;

{ THeidenhainFile }

constructor THeidenhainTableFile.Create;
begin
  FIndex:= 0;

  HTable:= nil;
  MDT:= nil;

  FHFilename:= 'NOT_SET.TAB';
  FLFilename:= 'NOT_SET.TAB';
  MDTName:= '';
end;

destructor THeidenhainTableFile.Destroy;
begin
  HTable.Free;
  MDT:= nil;
  inherited;
end;

procedure THeidenhainTableFile.HTableFromMDT;
var I, J: Integer;
    Tmp: WideString;
    Stream: TFileStream;
begin
  if HTable.ReadOnly then
    raise Exception.Create('Cannot convert readonly table: ' + HTable.FileName);

  if Assigned(MDT) then begin
    HTable.RowCount:= MDT.RecordCount;

    for I:= 0 to HTable.RowCount - 1 do begin
      for J:= 0 to HTable.ColumnCount - 1 do begin
        //MDT.GetValue(I, J, Tmp);
        if MDT.GetFieldValue(I, HTable.ColumnName[J], Tmp) = AAR_OK then
          HTable.Decimal[HTable.ColumnName[J], I]:= StrToFloatDef(Tmp, 0);
      end;
    end;
  end;
  Stream:= TFileStream.Create(Self.LFilename, fmCreate);
  try
    HTable.Write(Stream);
  finally
    Stream.Free;
  end;
end;

var Blog: Integer;

procedure THeidenhainTableFile.MDTFromHTable;
var I, J: Integer;
    Tmp: WideString;
    Stream: TFileStream;
begin
  Stream:= TFileStream.Create(Self.LFilename, fmOpenRead);
  try
    HTable.Read(Stream);
  finally
    Stream.Free;
  end;

  if Assigned(MDT) then begin
    MDT.BeginUpdate;
    try
      for I:= MDT.FieldCount - 1 downto 0 do begin
        MDT.FieldNameAtIndex(I, Tmp);
        MDT.DeleteField(Tmp);
      end;

      if MDT.FieldCount <> 0 then
        Blog:= 12;

      for I:= 0 to HTable.ColumnCount - 1 do begin
        Tmp:= HTable.ColumnName[I];
        MDT.AddField(Tmp);
      end;

      MDT.Clear;
      MDT.Append(HTable.RowCount);

      for I:= 0 to HTable.ColumnCount - 1 do begin
        Tmp:= HTable.ColumnName[I];
        for J:= 0 to HTable.RowCount - 1 do begin
          MDT.SetValue(J, I, HTable.Text[Tmp, J], nil);
        end;
      end;

    finally
      MDT.EndUpdate;
    end;
  end;
end;


function THeidenhainTableManager.GetTableByIndex(
  Index: Integer): THeidenhainTableFile;
var I: Integer;
begin
  for I:= 0 to TableCount - 1 do begin
    if Tables[I].Index = Index then begin
      Result:= Tables[I];
      Exit;
    end;
  end;
  Result:= nil;
end;

end.
