unit Stub;

interface

uses Windows, Classes, INamedInterface, CNCTypes, SysUtils, SyncObjs,
     AbstractIOSystemAccessor, NamedPlugin, Sequencer;

type
  TEthIOSubsys = class(TAbstractIOSystemAccessor)
  private
    FTextDef: string;
    FInputSize: Integer;
    FOutputSize: Integer;
    IP: string;
    Host: IIOSystemAccessorHost;

    FInputArea: array [0..511] of Byte;
    FOutputArea: array [0..511] of Byte;
    Thread: TNetworkIOSubSystemThread;
  public
    constructor Create; override;
    function GetInfoPageContent(aPage : Integer) : WideString; override;
    procedure Initialise(Host: IIOSystemAccessorHost; const Params : WideString); override;
    procedure UpdateShadowToReal; override;
    procedure UpdateRealToShadow; override;
    function Healthy : Boolean; override;
    procedure Reset; override;
    function LastError : WideString; override;
    function GetInputArea: Pointer; override;
    function GetOutputArea: Pointer; override;
    function GetInputSize: Integer; override;
    function GetOutputSize: Integer; override;
    function TextDefinition: WideString; override;
    procedure Shutdown; override;
  end;

implementation

{ TEthIOSubsys }

function TEthIOSubsys.GetOutputSize: Integer;
begin
  Result:= FOutputSize;
end;

function TEthIOSubsys.GetInfoPageContent(aPage: Integer): WideString;
begin
  Result:= 'Bottom' + CarRet + 'Smells' + CarRet;
end;

constructor TEthIOSubsys.Create;
begin
  inherited;
end;

function TEthIOSubsys.GetOutputArea: Pointer;
begin
  Result:= @FOutputArea;
end;

procedure TEthIOSubsys.Shutdown;
begin
  Thread.Shutdown;
end;

function TEthIOSubsys.GetInputSize: Integer;
begin
  Result:= FInputSize;
end;

procedure TEthIOSubsys.Reset;
begin

end;

function TEthIOSubsys.GetInputArea: Pointer;
begin
  Result:= @FInputArea;
end;

function TEthIOSubsys.TextDefinition: WideString;
begin
  Result:= FTextDef;
end;

function TEthIOSubsys.Healthy: Boolean;
begin
  Result:= Thread.Healthy;
end;

function TEthIOSubsys.LastError: WideString;
begin

end;

procedure TEthIOSubsys.Initialise(Host: IIOSystemAccessorHost;
  const Params: WideString);
var Tmp: string;
begin
  FTextDef:= Params;
  Tmp:= Params;
  Self.Host:= Host;
  FInputSize:= StrToInt(CncTypes.ReadDelimited(Tmp, ';'));
  FOutputSize:= StrToInt(CncTypes.ReadDelimited(Tmp, ';'));
  IP:= CncTypes.ReadDelimited(Tmp, ';');
  FInputSize:= CncTypes.RangeInt(FInputSize, 2, 512);
  FOutputSize:= CncTypes.RangeInt(FOutputSize, 2, 512);

  Thread:= TNetworkIOSubSystemThread.Create(FInputSize, FOutputSize, IP);
  Thread.RunTag:= Named.AddTag('EthAcnc32Refresh', 'TIntegerTag');

  Host.AddPage('Whoop');
  Thread.Resume;
end;

procedure TEthIOSubsys.UpdateRealToShadow;
begin
  Thread.Lock.Acquire;
  try
    CopyMemory(@FInputArea, @Thread.InData, FInputSize);
  finally
    Thread.Lock.Release;
  end;
end;

procedure TEthIOSubsys.UpdateShadowToReal;
begin
  Thread.Lock.Acquire;
  try
    CopyMemory(@Thread.OutData, @FOutputArea, FOutputSize);
  finally
    Thread.Lock.Release;
  end;
end;

initialization
  TEthIOSubsys.AddIOSystemAccessor('Acnc32NetworkIOSubSystem', TEthIOSubsys);
end.
