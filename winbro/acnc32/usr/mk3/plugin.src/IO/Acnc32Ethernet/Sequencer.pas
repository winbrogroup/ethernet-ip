unit Sequencer;

interface

uses SysUtils, Classes, INamedInterface, SyncObjs, IdTCPServer, IdTCPClient,
     IdContext, CncTypes, NamedPlugin, IDComponent, Forms;

type
  TNetworkIOSubSystemThread = class(TThread)
  private
    FHealthy: Boolean;
    IP: string;
    InputSize: Integer;
    OutputSize: Integer;
    FRunTag: TTagRef;
    FLock: TCriticalSection;

    IdTCPClient: TIdTCPClient;
    IdTCPServer: TIdTCPServer;

    procedure RunServer(Port: Integer);
    procedure RunClient;

    procedure IdTCPServerConnect(AContext: TIdContext);
    procedure IdTCPServerExecute(AContext:TIdContext);
    procedure IdTCPServerDisconnect(AContext:TIdContext);

    procedure ClientWork(Sender: TObject; WorkMode: TWorkMode; WorkCount: Integer);
    procedure ClientConnected(Sender: TObject);
    procedure ClientDisconnected(Sender: TObject);
    procedure ClientStatus(ASender: TObject; const AStatus: TIdStatus;
   const AStatusText: string);

  public
    InData: array [0..511] of Byte;
    OutData: array [0..511] of Byte;
    constructor Create(InputSize, OutputSize: Integer; IP: string);
    procedure Execute; override;
    property Healthy: Boolean read FHealthy write FHealthy;
    property RunTag: TTagRef read FRunTag write FRunTag;
    property Lock: TCriticalSection read FLock write FLock;
    procedure Shutdown;
  end;

implementation

{ TNetworkIOSubSystemThread }

constructor TNetworkIOSubSystemThread.Create(InputSize, OutputSize: Integer;
  IP: string);
begin
  inherited Create(True);
  Self.IP:= IP;
  Self.InputSize:= InputSize;
  Self.OutputSize:= OutputSize;
  Lock:= TCriticalSection.Create;
end;

procedure TNetworkIOSubSystemThread.Execute;
begin
  if IP = '' then begin
    RunServer(12124);
  end
  else begin
    RunClient;
  end;
end;

procedure TNetworkIOSubSystemThread.IdTCPServerExecute(AContext: TIdContext);
var Msg: string;
    Index: Integer;
    Token: string;
    I: Integer;
begin
  try
    while AContext.Connection.IOHandler.Connected do begin
      Msg:= AContext.Connection.IOHandler.ReadLn;
      Token:= '?';
      Index:= 0;
      while Token <> '' do begin
        Token:= ReadDelimited(Msg, ' ');
        InData[Index]:= StrToIntDef('$' + Token, 0);
        Inc(Index);
      end;

      Msg:= '';
      for I:= 0 to OutputSize do begin
        Msg:= Msg + IntToHex(OutData[I], 2) + ' ';
      end;
      AContext.Connection.IOHandler.WriteLn(Msg);
    end;
  except
    on E: Exception do
      Named.EventLog('Acnc32NetworkIOSubSystem: ' + E.Message);
  end;
end;

procedure TNetworkIOSubSystemThread.IdTCPServerConnect(AContext: TIdContext);
begin
  with AContext.Connection do begin
    IOHandler.WriteLn('Connected: ' + AContext.Binding.PeerIP);
  end;
end;

procedure TNetworkIOSubSystemThread.IdTCPServerDisconnect(AContext: TIdContext);
begin

end;

procedure TNetworkIOSubSystemThread.RunClient;
begin
  while not Terminated do begin
    IdTCPClient:= TIdTCPClient.Create(Application);
    IdTCPClient.Host:= IP;
    IdTCPClient.Port:= 12124;
    IdTCPClient.OnWork:= ClientWork;
    IdTCPClient.OnConnected:= ClientConnected;
    IdTCPClient.OnDisconnected:= ClientDisconnected;
    IdTCPClient.OnStatus:= ClientStatus;
    if not IdTCPClient.Connected then begin
      try
        IdTCPClient.Connect;
      except
        on E: Exception do
          Named.EventLog('Failed connection: ' + E.Message);
      end;
    end;
    IdTCPClient.Free;
    IdTCPClient:= nil;
    Sleep(5000);
  end;
end;

procedure TNetworkIOSubSystemThread.RunServer(Port: Integer);
begin
  IdTCPServer:= TIdTCPServer.Create(Application);
  IdTCPServer .DefaultPort:= Port;

  IdTCPServer.OnConnect := IdTCPServerConnect;
  IdTCPServer.OnExecute := IdTCPServerExecute;
  IdTCPServer.OnDisconnect := IdTCPServerDisconnect;
  IdTCPServer.Active:= True;
  FHealthy:= True;
end;

procedure TNetworkIOSubSystemThread.ClientWork(Sender: TObject;
  WorkMode: TWorkMode; WorkCount: Integer);
begin
end;

procedure TNetworkIOSubSystemThread.ClientConnected(Sender: TObject);
var Msg, Token: string;
    Index, I: Integer;
begin
  FHealthy:= True;
  Named.EventLog('TNetworkIOSubSystemThread: ' + IdTCPClient.IOHandler.ReadLn);
  try
    IdTCPClient.ReadTimeout:= 1000;
    while IdTCPClient.IOHandler.Connected do begin
      Msg:= '';
      for I:= 0 to OutputSize do begin
        Msg:= Msg + IntToHex(OutData[I], 2) + ' ';
      end;
      IdTCPClient.IOHandler.WriteLn(Msg);

      Msg:= IdTCPClient.IOHandler.ReadLn;
      Token:= '?';
      Index:= 0;
      while Token <> '' do begin
        Token:= ReadDelimited(Msg, ' ');
        InData[Index]:= StrToIntDef('$' + Token, 0);
        Inc(Index);
      end;

    end;
  except
    on E: Exception do
      Named.EventLog('Acnc32NetworkIOSubSystem: ' + E.Message);
  end;
  FHealthy:= False;
end;

procedure TNetworkIOSubSystemThread.Shutdown;
begin
  if Assigned(IdTCPClient) then
    IdTCPClient.Free;

  if Assigned(IdTCPServer) then
    IdTCPServer.Free;

  Terminate;
end;

procedure TNetworkIOSubSystemThread.ClientDisconnected(Sender: TObject);
begin
  FHealthy:= False;
end;

procedure TNetworkIOSubSystemThread.ClientStatus(ASender: TObject;
  const AStatus: TIdStatus; const AStatusText: string);
begin
  Named.EventLog(AStatusText);
end;

end.
