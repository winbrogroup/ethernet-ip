object DutyCycleForm: TDutyCycleForm
  Left = 133
  Top = 288
  Width = 660
  Height = 301
  BorderStyle = bsSizeToolWin
  Caption = 'Duty Cycle Editor'
  Color = clBtnFace
  Constraints.MinHeight = 180
  Constraints.MinWidth = 250
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'MS Sans Serif'
  Font.Style = [fsBold]
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 16
  object OnOffSplitter: TSplitter
    Left = 313
    Top = 0
    Width = 12
    Height = 147
    Cursor = crHSplit
    Beveled = True
    Color = clBtnFace
    ParentColor = False
    OnCanResize = OnOffSplitterCanResize
    OnPaint = OnOffSplitterPaint
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 255
    Width = 652
    Height = 19
    Panels = <
      item
        Alignment = taCenter
        Width = 50
      end>
    SimplePanel = False
  end
  object Panel1: TPanel
    Left = 0
    Top = 188
    Width = 652
    Height = 67
    Align = alBottom
    BevelOuter = bvLowered
    TabOrder = 1
    object SamplePaint: TPaintBox
      Left = 273
      Top = 1
      Width = 255
      Height = 65
      Align = alClient
      OnPaint = SamplePaintPaint
    end
    object Panel3: TPanel
      Left = 528
      Top = 1
      Width = 123
      Height = 65
      Align = alRight
      TabOrder = 0
      object SuppressedBtn: TSpeedButton
        Left = 8
        Top = 8
        Width = 107
        Height = 41
        AllowAllUp = True
        Anchors = [akTop, akRight]
        GroupIndex = 1
        OnClick = SuppressedBtnClick
      end
    end
    object Panel5: TPanel
      Left = 1
      Top = 1
      Width = 272
      Height = 65
      Align = alLeft
      TabOrder = 1
      object BitBtn2: TBitBtn
        Left = 94
        Top = 8
        Width = 80
        Height = 41
        TabOrder = 0
        Kind = bkCancel
      end
      object BitBtn1: TBitBtn
        Left = 8
        Top = 8
        Width = 80
        Height = 41
        TabOrder = 1
        Kind = bkOK
      end
      object ApplyBtn: TBitBtn
        Left = 184
        Top = 8
        Width = 80
        Height = 41
        Caption = '&Apply'
        TabOrder = 2
        OnClick = ApplyBtnClick
        Glyph.Data = {
          DE010000424DDE01000000000000760000002800000024000000120000000100
          0400000000006801000000000000000000001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333444444
          33333333333F8888883F33330000324334222222443333388F3833333388F333
          000032244222222222433338F8833FFFFF338F3300003222222AAAAA22243338
          F333F88888F338F30000322222A33333A2224338F33F8333338F338F00003222
          223333333A224338F33833333338F38F00003222222333333A444338FFFF8F33
          3338888300003AAAAAAA33333333333888888833333333330000333333333333
          333333333333333333FFFFFF000033333333333344444433FFFF333333888888
          00003A444333333A22222438888F333338F3333800003A2243333333A2222438
          F38F333333833338000033A224333334422224338338FFFFF8833338000033A2
          22444442222224338F3388888333FF380000333A2222222222AA243338FF3333
          33FF88F800003333AA222222AA33A3333388FFFFFF8833830000333333AAAAAA
          3333333333338888883333330000333333333333333333333333333333333333
          0000}
        NumGlyphs = 2
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 147
    Width = 652
    Height = 41
    Align = alBottom
    TabOrder = 2
    object FreqTrackBar: TTrackBar
      Left = 1
      Top = 1
      Width = 650
      Height = 39
      Align = alClient
      Max = 500
      Min = 20
      Orientation = trHorizontal
      PageSize = 5
      Frequency = 10
      Position = 34
      SelEnd = 0
      SelStart = 0
      TabOrder = 0
      TickMarks = tmTopLeft
      TickStyle = tsAuto
      OnChange = FreqTrackBarChange
    end
  end
  object OnTimePanel: TPanel
    Left = 0
    Top = 0
    Width = 313
    Height = 147
    Align = alLeft
    Alignment = taRightJustify
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 3
    object OnTimePaintBox: TPaintBox
      Left = 0
      Top = 0
      Width = 313
      Height = 147
      Align = alClient
      Color = clBlack
      ParentColor = False
      OnPaint = OnTimePaintBoxPaint
    end
  end
  object OffTimePanel: TPanel
    Left = 325
    Top = 0
    Width = 327
    Height = 147
    Align = alClient
    Alignment = taLeftJustify
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 4
    OnResize = OffTimePanelResize
    object OffTimePaintBox: TPaintBox
      Left = 0
      Top = 0
      Width = 327
      Height = 147
      Align = alClient
      OnPaint = OffTimePaintBoxPaint
    end
  end
end
