unit IncControl;

interface

uses Classes, WinTypes, Windows, Graphics, SysUtils, WidgetTypes, WidgetInterface,
     NamedPlugin, CNCTypes;

type
  DirectionTypes = (dtUp,dtDown,dtLeft,dtRight);

  TIncControl = class(TPluginWidget)
  private
    FCallback : TPWEv;
    CallbackName : string;
    ArrowB, BGB : TBrush;
    ArrowP, BGP, UpP, DownP : TPen;
    FPosition : Integer;
    FArrowColor : TColor;
    FLabelColor : TColor;
    FColor : TColor;
    Test : Boolean;
    FModes : TList;
    ActiveMode : Integer;
    Triangle : array [0..2] of TPoint;
    UpRect, DownRect, LabelRect, ValueRect : TRect;

    FLegend : string;
    LegendFont : TFont;
    LegendTextWidth : Integer;

    ValueFont : TFont;
    ValueText : string;
    ValueTextWidth : Integer;

    UpBtnDown, DownBtnDown : Boolean;
    procedure SetLabelColor(aColor : TColor);
    procedure SetArrowColor(aColor : TColor);
    procedure SetPosition(aPosition : Integer);
    procedure SetColor(aColor : TColor);
    procedure PaintArrow(Direction : DirectionTypes;DownState : Boolean);
    procedure PaintLegend;
    procedure PaintValue;
  public
    constructor Create; override;
    destructor Destroy; override;
    procedure Paint; override;
    function SetProperty(aToken, aValue : PChar) : Boolean; override;
    procedure SetIntegerInput(const Name : string; aValue : Double); override;
    procedure MouseDown(Button: Integer; Shift: Byte; X, Y: Integer); override;
    procedure MouseUp(Button: Integer; Shift: Byte; X, Y: Integer); override;
    procedure SetCallback(aName : PChar; Callback : TPWEv); override;
    procedure SetBounds(aWidth, aHeight : Integer); override;
    class function GetProperties : PChar; override;
    property Position   : Integer      read FPosition write SetPosition;
    property Legend     : string       read FLegend write FLegend;
    property LabelColor  : TColor       read FLabelColor write SetLabelColor;
    property ArrowColor : TColor read FArrowColor write SetArrowColor;
    property Color : TColor read FColor write SetColor;
  end;

implementation

const IncControlProperties =
                  'position; ' +
                  'legend; ' +
                  'value; ' +
                  'test; ' +
                  'color; ' +
                  'arrowcolor; ' +
                  'textcolor; ' +
                  'additem; ';

constructor TIncControl.Create;
begin
  inherited Create;
  ArrowB := TBrush.Create;
  BGB := TBrush.Create;
  ArrowP := TPen.Create;
  DownP := TPen.Create;
  DownP.Color := clWhite;
  DownP.Width := 1;
  UpP := TPen.Create;
  UpP.Color := clSilver;
  UpP.Width := 2;
  BGP := TPen.Create;
  LegendFont := TFont.Create;
  LegendFont.Name := NamedPlugin.FontName;
  LegendFont.Color := clBlue;

  ValueFont := TFont.Create;
  ValueFont.Color := clBlue;
  ValueFont.Name := NamedPlugin.FontName;

  FModes := TList.Create;
  FModes.Add(TStringList.Create);

//  Items := TStringList.Create;
end;

destructor TIncControl.Destroy;
begin
  ArrowB.Free;
  BGB.Free;
  ArrowP.Free;
  BGP.Free;
  LegendFont.Free;
  ValueFont.Free;
  DownP := TPen.Create;
  UpP := TPen.Create;
  inherited Destroy;
end;

procedure TIncControl.SetIntegerInput(const Name : string; aValue : Double);
var I : Integer;
    Items : TStringList;
begin
  if CompareText(Name, 'mode') = 0 then begin
    ActiveMode := Round(aValue) mod FModes.Count;
    Self.SetIntegerInput('value', Value);
  end else begin
    Items := TStringList(FModes[ActiveMode]);
    try
      for I := 0 to Items.Count - 1 do begin
        if aValue = Integer(Items.Objects[I]) then begin
          FPosition := I;
          FValue:= AValue;
          ValueText := Items[I];
          Exit;
        end;
      end;
      ValueText := '?';
    finally
      PaintValue;
    end;
  end;
end;

procedure TIncControl.SetPosition(aPosition : Integer);
var Items : TStringList;
begin
  Items := TStringList(FModes[ActiveMode]);
  if (aPosition < Items.Count) and (aPosition >= 0) then begin
    FPosition := aPosition;
    ValueText := Items[FPosition];
    FValue := Integer(Items.Objects[FPosition]);
  end;
  SetBounds(Width, Height);
  PaintValue;
end;

procedure TIncControl.PaintLegend;
begin
  SelectObject(HDC, BGB.Handle);
  SelectObject(HDC, BGP.Handle);
  Rectangle(HDC, LabelRect.Left, LabelRect.Top, LabelRect.Right, LabelRect.Bottom);

  SelectObject(HDC, LegendFont.Handle);
  Windows.SetTextColor(HDC, LegendFont.Color);
  TextOut(HDC, (LabelRect.Left + LabelRect.Right - LegendTextWidth) div 2, LabelRect.Top, PChar(Legend), Length(Legend));;
end;

procedure TIncControl.PaintValue;
begin
  SelectObject(HDC, BGB.Handle);
  SelectObject(HDC, BGP.Handle);
  Rectangle(HDC, ValueRect.Left, ValueRect.Top, ValueRect.Right, ValueRect.Bottom);

  SelectObject(HDC, ValueFont.Handle);
  Windows.SetTextColor(HDC, ValueFont.Color);
  TextOut(HDC, (ValueRect.Left + ValueRect.Right - ValueTextWidth) div 2, ValueRect.Top, PChar(ValueText), Length(ValueText));
end;

procedure TIncControl.Paint;
begin
  SelectObject(HDC, BGB.Handle);
  SelectObject(HDC, DownP.Handle);
  Rectangle(HDC, 0, 0, Width, Height);

  PaintArrow(dtDown, False);
  PaintArrow(dtUp, False);
  PaintLegend;
  PaintValue;
end;

function TIncControl.SetProperty(aToken, aValue : PChar) : Boolean;
var Tmp : string;
Translator : TTranslator;

  procedure AddItem;
  var I, M : Integer;
      Tmp, Value : string;
      Items : TStringList;
  begin
    Value := aValue;
    Result := True;
    try
      M := StrToInt(ReadDelimited(Value, ',')) mod FModes.Count;
      Tmp := ReadDelimited(Value, ',');
      I := StrToInt(ReadDelimited(Value, ','));
      Items := TStringList(FModes[M]);
      Items.AddObject(Tmp, Pointer(I));
    except
      Result := True;
    end;
  end;

  procedure SetModeCount;
  var I, Tmp : Integer;
  begin
    Tmp := StrToInt(aValue);
    for I := FModes.Count to Tmp - 1 do begin
      FModes.Add(TStringList.Create);
    end;
  end;

begin
  Result := inherited SetProperty(aToken, aValue);
  Tmp := LowerCase(aToken);
  if not Result then begin
    Result := True;
    if Tmp = 'position' then
      Position := IntegerValue(aValue)
    else if Tmp = 'legend' then begin
      Translator := TTranslator.Create;
      try
        FLegend := Translator.GetString(aValue);
      finally
        Translator.Free;
      end;
    end else if Tmp = 'modecount' then
      SetModeCount
    else if Tmp = 'value' then
      SetIntegerInput('value', DoubleValue(aValue))
    else if Tmp = 'test' then
      Test := BooleanValue(aValue)
    else if Tmp = 'color' then
      Color := ColorValue(aValue)
    else if Tmp = 'arrowcolor' then
      ArrowColor := ColorValue(aValue)
    else if Tmp = 'textcolor' then
      LabelColor := ColorValue(aValue)
    else if Tmp = 'additem' then
      AddItem
    else
      Result := False;
  end;
end;

procedure TIncControl.MouseDown(Button: Integer; Shift: Byte; X, Y: Integer);
begin
  if XYInRect(X, Y, UpRect) then begin
    UPbtnDown := True;
    PaintArrow(dtUp, True);
  end else if XYInRect(X, Y, DownRect) then begin
    DownBtnDown := True;
    PaintArrow(dtDown, True);
  end;
end;

procedure TIncControl.MouseUp(Button: Integer; Shift: Byte; X, Y: Integer);
var P : Integer;
    Items : TStringList;
begin
  P := Position;
  if UpBtnDown then begin
    PaintArrow(dtUp, False);
    P := P + 1;
  end;

  if DownBtnDown then begin
    PaintArrow(dtDown, False);
    P := P - 1;
  end;

  Items := TStringList(FModes[ActiveMode]);
  P := (P + Items.Count) mod Items.Count;

  UpBtnDown := False;
  DownBtnDown := False;

  if Assigned(FCallback) then
//    FCallback(Integer(Self), PChar(CallbackName), PChar(IntToStr(Round(Value))));
    FCallback(Integer(Self), PChar(CallbackName), PChar(IntToStr(Integer(Items.Objects[P]))));
end;

procedure TIncControl.SetCallback(aName : PChar; Callback : TPWEv);
begin
  CallbackName := aName;
  FCallback := Callback;
end;

class function TIncControl.GetProperties : PChar;
begin
  Result := PChar(inherited GetProperties + IncControlProperties);
end;

procedure TIncControl.SetLabelColor(aColor : TColor);
begin
  ValueFont.Color := aColor;
  LegendFont.Color := aColor;
  PaintValue;
  PaintLegend;
end;

procedure TIncControl.SetArrowColor(aColor : TColor);
begin
  ArrowB.Color := aColor;
  ArrowP.Color := aColor;
  PaintArrow(dtUp, False);
  PaintArrow(dtDown, False);
end;

procedure TIncControl.SetColor(aColor : TColor);
begin
  BGB.Color := aColor;
  BGP.Color := aColor;
end;

procedure TIncControl.SetBounds(aWidth, aHeight : Integer);
begin
  Width := aWidth;
  Height := aHeight;
  UpRect.Left := 1;
  UpRect.Top := 1;
  UpRect.Bottom := Height div 2 - 1;
  UpRect.Right := Width div 4 - 1;

  DownRect := UpRect;
  DownRect.Top := Height div 2 + 1;
  DownRect.Bottom := Height - 1;

  LabelRect.Left := Width div 4 + 1;
  LabelRect.Top := 1;
  LabelRect.Right := Width - 1;
  LabelRect.Bottom := Height div 3 - 1;

  ValueRect := LabelRect;
  ValueRect.Top := Height div 3 + 1;
  ValueRect.Bottom := Height - 1;

  LegendTextWidth := Self.TextBoxFont(Legend, LabelRect, LegendFont).Cx;
  ValueTextWidth := Self.TextBoxFont(ValueText, ValueRect, ValueFont).Cx;
end;

type
  PPoints = ^TPoints;
  TPoints = array[0..0] of TPoint;

procedure TIncControl.PaintArrow(Direction : DirectionTypes;DownState : Boolean);
begin
  case Direction of
    dtUP: begin
      SelectObject(HDC, BGB.Handle);
      SelectObject(HDC, BGP.Handle);
      FillRect(HDC, UpRect, BGB.Handle);

      SelectObject(HDC, ArrowB.Handle);
      if DownState then
        SelectObject(HDC, DownP.Handle)
      else
        SelectObject(HDC, UpP.Handle);

      Triangle[0] := Point((UpRect.Right + UpRect.Left) div 2, UpRect.Top + 2);
      Triangle[1] := Point(UpRect.Left + 2, UpRect.Bottom - 2);
      Triangle[2] := Point(UpRect.Right - 2, UpRect.Bottom - 2);
      Polygon(HDC, PPoints(@Triangle)^, 3);
    end;

    dtDown: begin
      SelectObject(HDC, BGB.Handle);
      SelectObject(HDC, BGP.Handle);
      FillRect(HDC, DownRect, BGB.Handle);

      SelectObject(HDC, ArrowB.Handle);
      if DownState then
        SelectObject(HDC, DownP.Handle)
      else
        SelectObject(HDC, UpP.Handle);

      Triangle[0] := Point((DownRect.Right + DownRect.Left) div 2, DownRect.Bottom - 2);
      Triangle[1] := Point(DownRect.Left + 2, DownRect.Top + 2);
      Triangle[2] := Point(DownRect.Right - 2, DownRect.Top + 2);
      Polygon(HDC, PPoints(@Triangle)^, 3);
    end;
  end;
end;


initialization
  TIncControl.RegisterPanelMeterClass(TIncControl, 'IncrementControl');
end.
