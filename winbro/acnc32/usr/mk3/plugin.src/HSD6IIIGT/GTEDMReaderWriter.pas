unit GTEDMReaderWriter;

interface

uses Windows, Classes, Tokenizer, SysUtils, AbstractOPPPlugin, NamedPlugin,
     INamedInterface, SyncObjs, Math;

type

  TEDMSection = class;
  TEDMBlock = class(TObject)
    FUpdateRequired : Boolean; // Used by block processor to determine a parameter change
  public
    GapVolts : Double;
    Servo : Double;
    Peak : Integer;
    HighVoltage : Integer;
    PulseBoards : Integer;
    OnTime : Integer;
    OffTime : Integer;
    SuppOnTime : Integer;
    SuppOffTime : Integer;
    Depth : Integer;
    Negative : Boolean;
    Capacitance : Integer;
    ASV : Double;
    Coupling : Integer;
    BusVoltage : Integer;
    AltBits : Integer;
    SpindleSpeed : Double;
    RRDepth: Integer;
    RRServo: Double;
    HP: Double;

    constructor Create;
    procedure ReadFromStrings(S : TEDMSection);
    procedure WriteToStrings(S : TStrings);
    property UpdateRequired : Boolean read FUpdateRequired write FUpdateRequired;
    procedure Assign(Block : TEDMBlock);
  end;

  TEDMProgram = class(TObject)
  private
    FBlocks : TList;
    EDMSection : TEDMSection;
    function GetBlock(Index : Integer) : TEDMBlock;
    function GetBlockCount : Integer;
    procedure Clean;
  public
    ID : Integer;
    Comment : string;
    constructor Create(aEDMSection : TEDMSection);
    destructor Destroy; override;
    procedure ReadFromStrings(S : TEDMSection);
    procedure WriteToStrings(S : TStrings);
    property BlockCount : Integer read GetBlockCount;
    property Blocks [Index : Integer]: TEDMBlock read GetBlock; default;
    function NewBlock : TEDMBlock;
    procedure DeleteBlock(Index : Integer);
    function Verify(aBlock : Integer) : Boolean;
  end;

  TEDMSection = class(TQuoteTokenizer)
  private
    FCount : Integer;
    FPrograms : TList; //array of TEDMProgram;
    FDirty : Boolean;
    function GetProgram(Index : Integer) : TEDMProgram;
    procedure CleanProgram;
    function GetProgramCount : Integer;
  public
    constructor Create;
    procedure ReadFromStrings;
    procedure WriteToStrings;
    function NewProgram : TEDMProgram;
    function FindProgram(Index : Integer) : TEDMProgram;
    property ProgramCount : Integer read GetProgramCount;
    property Programs[Index : Integer] : TEDMProgram read GetProgram; default;
    procedure DeleteProgram(aProgram : TEDMProgram);
    property Dirty : Boolean read FDirty;
  end;

  TEDMSectionConsumer = class(TAbstractOPPPluginConsumer)
    FReading : Boolean;
  public
    EDMSection : TEDMSection;
    constructor Create(aSectionName : string); override;
    procedure Consume(aSectionName, Buffer : PChar); override;
    function Dirty : Boolean; override;
    property Reading : Boolean read FReading;
    procedure FinalInstall; override;
  end;

const
  HSD6IISectionName = 'HSD6IIEDM';

var
  EDMConsumer : TEDMSectionConsumer;
  MetricToInch : Double;
  BarToPSI : Double;
  InchModeTag : TTagRef;
  Lock : TCriticalSection;


implementation

uses EDMOptions;

const
  HoleProgramName = 'HoleProgram';
  BlockName = 'Block';

  GapVoltsName = 'GapVolts';
  ServoName = 'Servo';
  PeakName = 'Peak';
  HighVoltageName = 'HighVoltage';
  PulseBoardsName = 'PulseBoards';
  OnTimeName = 'OnTime';
  OffTimeName = 'OffTime';
  SuppOnTimeName = 'SuppOnTime';
  SuppOffTimeName = 'SuppOffTime';
  DepthName = 'Depth';
  NegativeName = 'Negative';
  CapacitanceName = 'Capacitance';
  ASVName = 'ASV';
  CouplingName = 'Coupling';
  BusVoltageName = 'BusVoltage';
  AltBitsName = 'AltBits';
  SpindleSpeedName = 'SpindleSpeed';
  RRDepthName = 'RRDepth';
  RRServoName = 'RRServo';
  HPName = 'HP';

  IDName = 'ID';
  CommentName = 'Comment';

  RRDepthDefault = 0;
  RRServoDefault = 3000;
  HPDefault = 1000 / 14.7;

function ApproxEqual(const A, B: double): Boolean;
begin
  Result:= Abs(A - B) < 0.001;
end;

constructor TEDMBlock.Create;
begin
  GapVolts := 80;
  Servo := 20;
  Peak := 1;
  HighVoltage := 150;
  PulseBoards := 1;
  OnTime := 1000;
  OffTime := 5000;
  SuppOnTime := 600;
  SuppOffTime := 10000;
  Depth := 0;
  Negative := True;
  Capacitance := 0;
  ASV := 0;
  Coupling := 0;
  BusVoltage := 86;
  AltBits := 0;
  SpindleSpeed := 0;
  FUpdateRequired := True;

  RRDepth:= RRDepthDefault;
  RRServo:= RRServoDefault;
  HP:= HPDefault;
end;

procedure TEDMBlock.Assign(Block : TEDMBlock);
begin
  GapVolts := Block.GapVolts;
  Servo := Block.Servo;
  Peak := Block.Peak;
  HighVoltage := Block.HighVoltage;
  PulseBoards := Block.PulseBoards;
  OnTime := Block.OnTime;
  OffTime := Block.OffTime;
  SuppOnTime := Block.SuppOnTime;
  SuppOffTime := Block.SuppOffTime;
  Depth := Block.Depth;
  Negative := Block.Negative;
  Capacitance := Block.Capacitance;
  ASV := Block.ASV;
  Coupling := Block.Coupling;
  BusVoltage := Block.BusVoltage;
  AltBits := Block.AltBits;
  SpindleSpeed := Block.SpindleSpeed;
  FUpdateRequired := Block.FUpdateRequired;
  RRDepth:= Block.RRDepth;
  RRServo:= Block.RRServo;
  HP:= Block.HP;
end;

procedure TEDMBlock.ReadFromStrings(S : TEDMSection);
  function ReadDoubleAssign : Double;
  var Code : Integer;
  begin
    S.ExpectedToken('=');
    Val(S.Read, Result, Code);
    if Code <> 0 then
      raise Exception.CreateFmt('Cannot convert to floating point [%s]', [S.LastToken]);
  end;

var Token : string;
begin
  S.ExpectedTokens([BlockName, '=', '{']);
  Token := S.Read;
  while not S.EndOfStream do begin
    if Token = GapVoltsName then
      GapVolts := ReadDoubleAssign
    else if Token = ServoName then
      Servo := ReadDoubleAssign
    else if Token = PeakName then
      Peak := S.ReadIntegerAssign
    else if Token = HighVoltageName then
      HighVoltage := S.ReadIntegerAssign
    else if Token = PulseBoardsName then
      PulseBoards := S.ReadIntegerAssign
    else if Token = OnTimeName then
      OnTime := S.ReadIntegerAssign
    else if Token = OffTimeName then
      OffTime:= S.ReadIntegerAssign
    else if Token = SuppOnTimeName then
      SuppOnTime := S.ReadIntegerAssign
    else if Token = SuppOffTimeName then
      SuppOffTime := S.ReadIntegerAssign
    else if Token = DepthName then
      Depth := S.ReadIntegerAssign
    else if Token = NegativeName then
      Negative := S.ReadBooleanAssign
    else if Token = CapacitanceName then
      Capacitance := S.ReadIntegerAssign
    else if Token = ASVName then
      ASV := ReadDoubleAssign
    else if Token = CouplingName then
      Coupling := S.ReadIntegerAssign
    else if Token = BusVoltageName then
      BusVoltage := S.ReadIntegerAssign
    else if Token = AltBitsName then
      AltBits := S.ReadIntegerAssign
    else if Token = SpindleSpeedName then
      SpindleSpeed := ReadDoubleAssign
    else if Token = RRServoName then
      RRServo := ReadDoubleAssign
    else if Token = RRDepthName then
      RRDepth := S.ReadIntegerAssign
    else if Token = HPName then
      HP := ReadDoubleAssign
    else if Token = '}' then
      Exit
    else
      raise Exception.CreateFmt('Unexpected Token [%s]', [Token]);
    Token := S.Read;
  end;
  raise Exception.Create('Unexpected end of stream');
end;

procedure TEDMBlock.WriteToStrings(S : TStrings);
  procedure SOut(const aText : string);
  begin
    S.Add('      ' + aText);
  end;
begin
  S.Add('    ' + BlockName + ' = {');
  SOut(Format('%s = %f', [GapVoltsName, GapVolts]));
  SOut(Format('%s = %f', [ServoName, Servo]));
  SOut(Format('%s = %d', [PeakName, Peak]));
  SOut(Format('%s = %d', [HighVoltageName, HighVoltage]));
  SOut(Format('%s = %d', [PulseBoardsName, PulseBoards]));
  SOut(Format('%s = %d', [OnTimeName, OnTime]));
  SOut(Format('%s = %d', [OffTimeName, OffTime]));
  SOut(Format('%s = %d', [SuppOnTimeName, SuppOnTime]));
  SOut(Format('%s = %d', [SuppOffTimeName, SuppOffTime]));
  SOut(Format('%s = %d', [DepthName, Depth]));
  SOut(Format('%s = %d', [NegativeName, Ord(Negative)]));
  SOut(Format('%s = %d', [CapacitanceName, Capacitance]));
  SOut(Format('%s = %f', [ASVName, ASV]));
  SOut(Format('%s = %d', [CouplingName, Coupling]));
  SOut(Format('%s = %d', [BusVoltageName, BusVoltage]));
  SOut(Format('%s = %d', [AltBitsName, AltBits]));
  SOut(Format('%s = %f', [SpindleSpeedName, SpindleSpeed]));

  if not ApproxEqual(RRServo, RRServoDefault) then
    SOut(Format('%s = %f', [RRServoName, RRServo]));

  if not ApproxEqual(RRDepth, RRDepthDefault) then
    SOut(Format('%s = %d', [RRDepthName, RRDepth]));

  if not ApproxEqual(HP, HPDefault) then
    SOut(Format('%s = %f', [HPName, HP]));

  S.Add('    }');
end;


constructor TEDMProgram.Create(aEDMSection : TEDMSection);
begin
  inherited Create;
  FBlocks := TList.Create;
  EDMSection := aEDMSection;
end;

procedure TEDMProgram.Clean;
begin
  while BlockCount > 0 do begin
    Blocks[0].Free;
    FBlocks.Delete(0);
  end;
end;

destructor TEDMProgram.Destroy;
begin
  Clean;
  inherited;
end;

function TEDMProgram.GetBlock(Index : Integer) : TEDMBlock;
begin
  Result := FBlocks[Index];
end;

function TEDMProgram.GetBlockCount : Integer;
begin
  Result := FBlocks.Count;
end;

function TEDMProgram.NewBlock : TEDMBlock;
begin
  Result := TEDMBlock.Create;
  FBlocks.Add(Result);
end;

procedure TEDMProgram.DeleteBlock(Index : Integer);
begin
  if (Index < BlockCount) and (BlockCount > 0) and (Index >= 0) then begin
    Blocks[Index].Free;
    FBlocks.Delete(Index);
  end;
end;

function TEDMProgram.Verify(aBlock : Integer) : Boolean;
var Duty : Double;
  function DoubleBounds(Current : Double; Min, Max : Double) : Double;
  begin
    Result := Current;
    if Result > Max then
      Result := Max
    else if Current < Min then
      Result := Min;
  end;

  function IntegerBounds(Current, Min, Max : Integer) : Integer;
  begin
    Result := Current;
    if Result > Max then
      Result := Max
    else if Current < Min then
      Result := Min;
  end;

var Opt: TEDMOptions;

begin
  EDMSection.FDirty := True;
  with Blocks[aBlock] do begin
// Bounds check each parameter
    GapVolts := DoubleBounds(GapVolts, 0, 150);
    Servo := DoubleBounds(Servo, 0, 12800);

    {
    if TGTOptions.getInstance.Lite then begin
      Peak := IntegerBounds(Peak, 4, 32);
      Peak := (Peak div 4) * 4;
    end else begin
      Peak := IntegerBounds(Peak, 2, 16);
      Peak := (Peak div 2) * 2;
    end;
    }
    Opt:= TEDMOptions.getInstance;

    Peak := IntegerBounds(Peak, 2, Opt.MaxCurrent);
    Peak := (Peak div 2) * 2;

    HighVoltage := IntegerBounds(HighVoltage, 88, 300);
    PulseBoards := IntegerBounds(PulseBoards, 0, 63);
    OnTime := IntegerBounds(OnTime, 500, 655360);
    OffTime := IntegerBounds(OffTime, 1000, 655360);
    SuppOnTime := IntegerBounds(SuppOnTime, 500 ,655360 );
    SuppOffTime := IntegerBounds(SuppOffTime,1000 ,655360 );
    Depth := IntegerBounds(Depth, 0, TEDMOptions.getInstance.MaxDepth);
    //  Negative := Boolean;
    Capacitance:= IntegerBounds(Capacitance, 0, Opt.MaxCapacitorBitField);

    ASV := DoubleBounds(ASV, 0, 50);
    Coupling := IntegerBounds(Coupling, 0, 63);
    AltBits := IntegerBounds(AltBits, 0, 15);

    SpindleSpeed := DoubleBounds(SpindleSpeed, 0, 1000);
    RRDepth:= IntegerBounds(RRDepth, 0, 65535);
    RRServo:= DoubleBounds(RRServo, 0, 12800);
    HP:= DoubleBounds(HP, Opt.MinPressure, Opt.MaxPressure);

    //if (BusVoltage <> 78) and (BusVoltage <> 100) then
    BusVoltage := 86;

    OnTime := (OnTime div 10) * 10;
    OffTime := (OffTime div 10) * 10;
    SuppOnTime := (SuppOnTime div 10) * 10;
    SuppOffTime := (SuppOffTime div 10) * 10;
  // Check Duty cycle is less than 90%
    Duty := OnTime / (OnTime + OffTime);
    if Duty > 0.9 then
      OffTime := OnTime div 9;

    if SuppOnTime > OnTime then
      SuppOnTime := OnTime;

    if HighVoltage - BusVoltage > 200 then
      HighVoltage := BusVoltage + 200;

    FUpdateRequired := True;
  end;
  Result := True;
end;

procedure TEDMProgram.ReadFromStrings(S : TEDMSection);
var Token : string;
  procedure AddBlock;
  begin
    S.Push;
    NewBlock.ReadFromStrings(S);
  end;

begin
  S.ExpectedTokens([HoleProgramName, '=', '{']);
  Clean;
  Token := S.Read;
  while not S.EndOfStream do begin
    if Token = IDName then
      ID := S.ReadIntegerAssign
    else if Token = CommentName then
      Comment := S.ReadStringAssign
    else if Token = BlockName then
      AddBlock
    else if Token = '}' then
      Exit;
    Token := S.Read;
  end;
  raise Exception.Create('Unexpected end of stream');
end;

procedure TEDMProgram.WriteToStrings(S : TStrings);
  procedure SOut(const aText : string);
  begin
    S.Add('    ' + aText);
  end;

var I : Integer;
begin
  S.Add('  ' + HoleProgramName + '= {');
  SOut(Format('%s = %d', [IDName, ID]));
  SOut(Format('%s = ''%s''', [CommentName, Comment]));
  for I := 0 to BlockCount - 1 do begin
    Blocks[I].WriteToStrings(S);
  end;
  S.Add('  }');
end;

constructor TEDMSection.Create;
begin
  inherited Create;
  FPrograms := TList.Create;
  Seperator := '{}=';
  Self.QuoteChar := '''';
  FCount := 0;
end;

procedure TEDMSection.ReadFromStrings;
var Token : string;
  procedure AddProgram;
  begin
    Push;
    NewProgram.ReadFromStrings(Self);
  end;

begin
  CleanProgram;
  if Self.Count = 0 then
    Exit;
  Rewind;
  Self.Read;
  try
    if not EndOfStream then begin // Detect an Empty program
      Self.Push;
      ExpectedTokens([HSD6IISectionName, '=', '{']);
      Token := Self.Read;
      while not EndOfStream do begin
        if Token = HoleProgramName then
          AddProgram
        else if Token = '}' then
          Exit
        else
          raise Exception.CreateFmt('Invalid token [%s]', [Token]);
        Token := Self.Read;
      end;
    end;
  except
    on E : Exception do
      raise Exception.CreateFmt('%s at line #%d in %s', [E.message, Self.Line, HSD6IISectionName]);
  end;
end;

procedure TEDMSection.WriteToStrings;
var I : Integer;
begin
  Clear;
  Add(HSD6IISectionName + ' = {');
  for I := 0 to ProgramCount - 1 do begin
    if Programs[I].BlockCount > 0 then
      Programs[I].WriteToStrings(Self);
  end;
  Add('}');
end;

function TEDMSection.NewProgram : TEDMProgram;
begin
  Result := TEDMProgram.Create(Self);
  FPrograms.Add(Result);
  Result.ID := 1;
  Result.Comment := '';
end;

function TEDMSection.FindProgram(Index : Integer) : TEDMProgram;
var I : Integer;
begin
  for I := 0 to ProgramCount - 1 do begin
    if Programs[I].ID = Index then begin
      Result := Programs[I];
      Exit;
    end;
  end;
  Result := nil;
end;

procedure TEDMSection.DeleteProgram(aProgram : TEDMProgram);
var I : Integer;
begin
  for I := 0 to ProgramCount - 1 do begin
    if aProgram = Programs[I] then begin
      aProgram.Free;
      FPrograms.Delete(I);
      Exit;
    end;
  end;
end;

function TEDMSection.GetProgram(Index : Integer) : TEDMProgram;
begin
  Result := FPrograms[Index];
end;

function TEDMSection.GetProgramCount : Integer;
begin
  Result := FPrograms.Count;
end;

procedure TEDMSection.CleanProgram;
begin
  FDirty := False;
  while FPrograms.Count > 0 do begin
    Programs[0].Free;
    FPrograms.Delete(0);
  end;
end;

constructor TEDMSectionConsumer.Create(aSectionName : string);
begin
  inherited Create(aSectionName);
  EDMSection := TEDMSection.Create;
  // Because only one will be created we can store this instance in a global
  // variable EDMConsumer
  EDMConsumer := Self;
end;

procedure TEDMSectionConsumer.Consume(aSectionName, Buffer : PChar);
begin
  Named.EventLog('Loading EDM Section');
  Lock.Enter;
  try
    EDMSection.Clear;
    EDMSection.Text := Buffer;
    FReading := True;
    if Named.GetAsBoolean(InchModeTag) then begin
      MetricToInch := 25.4;
      BarToPSI := 14.5;
    end else begin
      MetricToInch := 1;
      BarToPSI := 1;
    end;
    try
      try
        EDMSection.ReadFromStrings;
      except
        EDMSection.CleanProgram;
        raise;
      end;
    finally
      NotifyClients;
      FReading := False;
    end;
  finally
    Lock.Release;
  end;
end;

function TEDMSectionConsumer.Dirty : Boolean;
begin
  Result := EDMSection.Dirty;
end;

procedure TEDMSectionConsumer.FinalInstall;
begin
  InchModeTag := NAmed.AquireTag('NC1InchModeDefault', PChar('TMethodTag'), nil);
end;

initialization
  Lock := TCriticalSection.Create;
  MetricToInch := 1;
  BarToPSI := 1;
  TEDMSectionConsumer.AddPlugin(HSD6IISectionName, TEDMSectionConsumer);
end.
