unit VIPEditor;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Buttons, StdCtrls, ExtCtrls, ComCtrls, NamedPlugin;

type
  TVIPApply = procedure(Sender : TObject; var aHV, aBusV, aPeak : Integer; var aNegative : Boolean) of Object;
  TVIPForm = class(TForm)
    Panel1: TPanel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    Panel2: TPanel;
    HVUpDown: TUpDown;
    Panel3: TPanel;
    BusVoltageGroup: TRadioGroup;
    DrawPanel: TPanel;
    HVPanel: TPanel;
    BusPanel: TPanel;
    SpeedButton2: TSpeedButton;
    SpeedButton1: TSpeedButton;
    HVLabel: TLabel;
    PeakLabel: TLabel;
    NegativeBtn: TSpeedButton;
    ApplyBtn: TBitBtn;
    procedure HVUpDownClick(Sender: TObject; Button: TUDBtnType);
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure BusVoltageGroupClick(Sender: TObject);
    procedure NegativeBtnClick(Sender: TObject);
    procedure ApplyBtnClick(Sender: TObject);
  private
    BusV, Peak : Integer;
    FApply : TVIPApply;
    procedure SetParameters;
    property Apply : TVIPApply read FApply write FApply;
  public
    class procedure Execute(var aHV, aBusV, aPeak : Integer; var aNegative : Boolean; aApply : TVIPApply);
  end;

var
  VIPForm: TVIPForm;

implementation

resourcestring
  NegativeLang = 'Neg';
  PositiveLang = 'Pos';

class procedure TVIPForm.Execute(var aHV, aBusV, aPeak : Integer; var aNegative : Boolean; aApply : TVIPApply);
var Form : TVIPForm;
    HV, BusV, Peak : Integer;
    Negative : Boolean;
begin
  Form := TVIPForm.Create(Application);
  if FontName <> '' then
    Form.Font.Name := FontName;

  Form.HVUpDown.Position := aHV;
  Form.BusV := aBusV;
  Form.Peak := aPeak;
  Form.NegativeBtn.Down := aNegative;
  Form.BusVoltageGroup.ItemIndex := 0;
  Form.SetParameters;
  Form.NegativeBtnClick(Form);
  Form.Apply := aApply;

  HV := aHV;
  BusV := aBusV;
  Peak := aPeak;
  Negative := aNegative;

  if Form.ShowModal = mrOK then begin
    aHV := Form.HVUpDown.Position;
    aBusV := Form.BusV;
    aPeak := Form.Peak;
    aNegative := Form.NegativeBtn.Down;
  end else begin
    aHV := HV;
    aBusV := BusV;
    aPeak := Peak;
    aNegative := Negative;
  end;
end;

procedure TVIPForm.SetParameters;
begin
  if not NegativeBtn.Down then begin
    HVPanel.Top := DrawPanel.Height - ((DrawPanel.Height * HVUpDown.Position) div 300);
    BusPanel.Top := DrawPanel.Height - ((DrawPanel.Height * BusV) div 300);
    HVPanel.Height := DrawPanel.Height - HVPanel.Top;
    BusPanel.Height := DrawPanel.Height - BusPanel.Top;
  end else begin
    HVPanel.Top := 0;
    BusPanel.Top := 0;
    HVPanel.Height := (DrawPanel.Height * HVUpDown.Position) div 300;
    BusPanel.Height := (DrawPanel.Height * BusV) div 300;
  end;

  HVLabel.Caption := Format('HV:%dV', [HVUpDown.Position]);
  PeakLabel.Caption := Format('Peak = %d', [Peak]);
  BusPanel.Color := $40400F + ($10 * Peak);
  HVPanel.Color := BusPanel.Color;
end;

{$R *.DFM}

procedure TVIPForm.HVUpDownClick(Sender: TObject; Button: TUDBtnType);
begin
  SetParameters;
end;

procedure TVIPForm.SpeedButton1Click(Sender: TObject);
begin
  Peak := Peak + 1;
  if Peak > 16 then
    Peak := 16;

  SetParameters;
end;

procedure TVIPForm.SpeedButton2Click(Sender: TObject);
begin
  Peak := Peak - 1;
  if Peak < 2 then
    Peak := 2;

  SetParameters;
end;

procedure TVIPForm.BusVoltageGroupClick(Sender: TObject);
begin
  if BusVoltageGroup.ItemIndex = 0 then
    BusV := 78
  else
    BusV := 100;

  BusV := 86;
  SetParameters;
end;

procedure TVIPForm.NegativeBtnClick(Sender: TObject);
begin
  if NegativeBtn.Down then
    NegativeBtn.Caption := NegativeLang
  else
    NegativeBtn.Caption := PositiveLang;

  SetParameters;
end;

procedure TVIPForm.ApplyBtnClick(Sender: TObject);
var Negative : Boolean;
    HV : Integer;
begin
  Negative := NegativeBtn.Down;
  HV := HVUpDown.Position;
  if Assigned(Apply) then
    Apply(Self, HV, BusV, Peak, Negative);

  NegativeBtn.Down := Negative;
  HVUpDown.Position := HV;
  NegativeBtnClick(Self);
end;

end.
