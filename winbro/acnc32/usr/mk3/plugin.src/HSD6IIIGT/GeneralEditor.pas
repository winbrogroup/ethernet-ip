unit GeneralEditor;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, ComCtrls, Math, NamedPlugin;


type
  TValueEditLabels = 0..8;
  TValueApply = procedure (Sender : TObject; var aValue : Double) of Object;
  TNumericEditorForm = class(TForm)
    StatusBar1: TStatusBar;
    Panel1: TPanel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    BitBtn3: TBitBtn;
    PointLabel: TStaticText;
    UpDownValue: TUpDown;
    procedure FormDestroy(Sender: TObject);
    procedure UpDownValueClick(Sender: TObject; Button: TUDBtnType);
    procedure BitBtn3Click(Sender: TObject);
  private
    Decimals : array [TValueEditLabels] of TStaticText;
    Units : array [TValueEditLabels] of TStaticText;
    ThisPower : Integer;
    CurrentValue : Double;
    Upper, Lower : Double;
    FApply : TValueApply;
    procedure SetEditorType(var aValue : Double; aUpper, aLower : Double; Prec : Integer);
    procedure RenderValue;
    procedure ValueClick(Sender : TObject);
    property Apply : TValueApply  read FApply write FApply;
  public
    class procedure Execute(var Value : Double; Upper, Lower : Double; Prec : Integer; aApply : TValueApply);
  end;

var
  NumericEditorForm: TNumericEditorForm;

implementation

{$R *.DFM}

class procedure TNumericEditorForm.Execute(var Value : Double; Upper, Lower : Double; Prec : Integer; aApply : TValueApply);
var Form : TNumericEditorForm;
    Hold : Double;
begin
  Form := TNumericEditorForm.Create(Application);
  try
    if FontName <> '' then
      Form.Font.Name := FontName;
    Form.SetEditorType(Value, Upper, Lower, Prec);
    Form.CurrentValue := Value;
    Hold := Value;
    Form.RenderValue;
    Form.Apply := aApply;
    if Form.ShowModal = mrOK then
      Value := Form.CurrentValue
    else
      Value := Hold;

  finally
    Form.Free;
  end;
end;

procedure TNumericEditorForm.SetEditorType(var aValue : Double; aUpper, aLower : Double; Prec : Integer);
var IUp : Integer;
    I : Integer;
    MidPoint : Integer;

const TextTop = 20;
      TextWidth = 45;
      TextHeight = 45;
      TextSize = 22;
begin
  Upper := aUpper;
  Lower := aLower;

  if Upper >= 1000000 then
    IUp := 7
  else if Upper >= 100000 then
    IUp := 6
  else if Upper >= 10000 then
    IUp := 5
  else if Upper >= 1000 then
    IUp := 4
  else if Upper >= 100 then
    IUp := 3
  else if Upper >= 10 then
    IUp := 2
  else if Upper >= 1 then
    IUp := 1
  else
    IUp := 0;

  MidPoint := (ClientWidth - UpDownValue.Width) div 2;
  I := IUp - Prec;
  MidPoint := MidPoint + (I * TextWidth div 2);

  PointLabel.Top := TextTop;
  PointLabel.Left := MidPoint - (TextWidth div 2);
  PointLabel.Width := TextWidth;
  PointLabel.Height := TextHeight;

  for I := 0 to Prec - 1 do begin
    Decimals[I] := TStaticText.Create(Self);
    Decimals[I].Parent := Self;
    Decimals[I].Caption := IntToStr(I);
    Decimals[I].Font.Name := 'Courier New';
    Decimals[I].Font.Size := TextSize;
    Decimals[I].Font.Style := [fsBold];
    Decimals[I].Left := MidPoint + (TextWidth div 2) + (I * TextWidth);
    Decimals[I].Top := TextTop;
    Decimals[I].Width := TextWidth;
    Decimals[I].Height := TextHeight;
    Decimals[I].AutoSize := False;
    Decimals[I].Alignment := taCenter;
    Decimals[I].BorderStyle := sbsSunken;
    Decimals[I].Tag := -1 - I;
    Decimals[I].TabStop := True;
    Decimals[I].OnClick := ValueClick;
  end;

  if Prec = 0 then
    PointLabel.Visible := False;

  for I := 0 to IUp - 1 do begin
    Units[I] := TStaticText.Create(Self);
    Units[I].Parent := Self;
    Units[I].Caption := IntToStr(I);
    Units[I].Font.Name := 'Courier New';
    Units[I].Font.Size := TextSize;
    Units[I].Font.Style := [fsBold];
    Units[I].Left := MidPoint - (TextWidth * 3 div 2) - (I * TextWidth);
    Units[I].Top := TextTop;
    Units[I].Width := TextWidth;
    Units[I].Height := TextHeight;
    Units[I].AutoSize := False;
    Units[I].Alignment := taCenter;
    Units[I].BorderStyle := sbsSunken;
    Units[I].Tag := I;
    Units[I].TabStop := True;
    Units[I].OnClick := ValueClick;
  end;
  ValueClick(Units[0]);
end;


procedure TNumericEditorForm.FormDestroy(Sender: TObject);
var I : Integer;
begin
  for I := 0 to High(TValueEditLabels) do begin
    if Assigned(Units[I]) then
      Units[I].Free;
    if Assigned(Decimals[I]) then
      Decimals[I].Free;
  end;
end;

procedure TNumericEditorForm.ValueClick(Sender : TObject);
var I : Integer;
begin
  for I := 0 to High(TValueEditLabels) do begin
    if Assigned(Units[I]) then
      Units[I].Color := clBtnFace;
    if Assigned(Decimals[I]) then
      Decimals[I].Color := clBtnFace;
  end;

  with Sender as TStaticText do begin
    Color := clRed;
    ThisPower := Tag;
  end;
end;

procedure TNumericEditorForm.RenderValue;
var I : Integer;
    Tmp : Double;
begin
  for I := 0 to High(TValueEditLabels) do begin
    if Assigned(Units[I]) then begin
      Tmp := Power(10, Units[I].Tag);
      Tmp := CurrentValue / Tmp;
      Units[I].Caption := IntToStr(Trunc(Tmp) mod 10);
    end;
    if Assigned(Decimals[I]) then begin
      Tmp := Power(10, Decimals[I].Tag);
      Tmp := CurrentValue / Tmp;
      Decimals[I].Caption := IntToStr(Trunc(Tmp) mod 10);
    end;
  end;
end;

procedure TNumericEditorForm.UpDownValueClick(Sender: TObject;
  Button: TUDBtnType);
begin
  case Button of
    btNext : CurrentValue := CurrentValue + Power(10, ThisPower);
    btPrev : CurrentValue := CurrentValue - Power(10, ThisPower);
  end;
  if CurrentValue > Upper then
    CurrentValue := Upper;
  if CurrentValue < Lower then
    CurrentValue := Lower;

  RenderValue;
end;

procedure TNumericEditorForm.BitBtn3Click(Sender: TObject);
begin
  if Assigned(Apply) then
    Apply(Self, CurrentValue);

  RenderValue;
end;

end.


