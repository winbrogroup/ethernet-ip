unit EDMOptions;

interface

uses SysUtils, NamedPlugin, IniFiles, GTEDMReaderWriter;

type
  TEDMOptions = class
  private
    constructor Create;
  public
    Boards: Integer;
    Capacitors: String;
    MaxDepth: Integer;
    MaxCurrent: Integer;

    MinPressure: Double;
    MaxPressure: Double;
    function MaxCapacitorBitField: Integer;

    function GTCapacitorBox: Boolean;

    function DisplayDepth(ADepth: Integer): string;
    function StorageDepth(AValue: string; ADefault: Integer): Integer;

    function DisplayTime(AValue: Integer): string;
    function StorageTime(AValue: string; const ADefault: Integer): Integer;

    function DisplayServo(AValue: Double): string;
    function StorageServo(AValue: string; ADefault: Double): Double;

    class function GetInstance: TEDMOptions;
  end;

implementation

var Instance: TEDMOptions;
{ TEDMOptions }

constructor TEDMOptions.Create;
var Ini: TIniFile;
begin
  Ini:= TIniFile.Create(DllProfilePath + 'EDMOptions.ini');
  try
    Boards:= Ini.ReadInteger('global', 'Boards', 6);
    Capacitors:= Ini.ReadString('global', 'CapacitorBox', 'HSD');
    MaxDepth:= Ini.ReadInteger('global', 'MaxDepth', 100000);
    MaxCurrent:= Ini.ReadInteger('global', 'MaxCurrent', 16);
    MinPressure:= Ini.ReadInteger('global', 'MinPressure', 15);
    MaxPressure:= Ini.ReadInteger('global', 'MaxPressure', 68);
  finally
    Ini.Free;
  end;
end;

function TEDMOptions.MaxCapacitorBitField: Integer;
begin

end;

function TEDMOptions.StorageDepth(AValue: string; ADefault: Integer): Integer;
var Tmp: Double;
begin
  Tmp := StrToFloatDef(AValue, ADefault / 1000);
  Result:= Round(Tmp * 1000);
end;

function TEDMOptions.DisplayDepth(ADepth: Integer): string;
begin
  Result:= Format('%0.3f', [ADepth / (MetricToInch * 1000.0)])
end;

class function TEDMOptions.getInstance: TEDMOptions;
begin
  if Instance = nil then
    Instance:= TEDMOptions.Create;

  Result:= Instance;
end;

function TEDMOptions.GTCapacitorBox: Boolean;
begin
  Result:= Capacitors = 'GT';
end;


function TEDMOptions.StorageTime(AValue: string; const ADefault: Integer): Integer;
var Tmp: Double;
begin
  Tmp := StrToFloatDef(AValue, ADefault / 1000);
  Result:= Round(Tmp * 1000);
end;

function TEDMOptions.DisplayTime(AValue: Integer): string;
begin
  Result:= Format('%.2f', [AValue / 1000]);
end;

function TEDMOptions.StorageServo(AValue: string; ADefault: Double): Double;
var Tmp: Double;
begin
  Tmp := StrToFloatDef(AValue, ADefault / 1000);
  Result:= Round(Tmp * 1000);
end;

function TEDMOptions.DisplayServo(AValue: Double): string;
begin
  Result:= Format('%.3f', [AValue / 1000]);
end;

end.
