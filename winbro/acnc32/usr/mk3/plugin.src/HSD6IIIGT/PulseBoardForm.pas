unit PulseBoardForm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, ComCtrls, ImgList, NamedPlugin, EDMOptions;

type
  TPulseBoardApply = procedure(Sender : TObject; var aBoards, aCoupling : Integer) of Object;
  TPulseBoardEditor = class(TForm)
    StatusBar1: TStatusBar;
    Panel1: TPanel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    Label1: TLabel;
    Label2: TLabel;
    BitBtn3: TBitBtn;
    procedure FormCreate(Sender: TObject);
    procedure FormPaint(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
  private
    Boards : array [0..5] of TPanel;
    Coupling : array [0..5] of TPanel;
    Brd, Cpl : Integer;
    FApply : TPulseBoardApply;
    procedure BoardsClick(Sender : TObject);
    procedure CouplingClick(Sender : TObject);
    procedure UpdateDisplay;
    property Apply : TPulseBoardApply read FApply write FApply;
  public
    class procedure Execute(var aBoards, aCoupling : Integer; aApply : TPulseBoardApply);
  end;

var
  PulseBoardEditor: TPulseBoardEditor;

implementation

{$R *.DFM}

class procedure TPulseBoardEditor.Execute(var aBoards, aCoupling : Integer; aApply : TPulseBoardApply);
var Form : TPulseBoardEditor;
    HoldB, HoldC : Integer;
begin
  Form := TPulseBoardEditor.Create(Application);
  if FontName <> '' then
    Form.Font.Name := FontName;
  Form.Cpl := aCoupling;
  Form.Brd := aBoards;
  Form.Apply := aApply;
  HoldB := aBoards;
  HoldC := aCoupling;
  Form.UpdateDisplay;
  if Form.ShowModal = mrOK then begin
    aBoards := Form.Brd;
    aCoupling := Form.Cpl;
  end else begin
    aBoards := HoldB;
    aCoupling := HoldC;
  end;
  Form.Free;
end;

procedure TPulseBoardEditor.FormCreate(Sender: TObject);
var I, Total : Integer;
begin
  {
  if .Lite then
    Total:= 2
  else
    Total:= 5;
    }
  Total:= TEDMOptions.getInstance.Boards - 1;

  for I := 0 to Total do begin
    Boards[I] := TPanel.Create(Self);
    Boards[I].PArent := Self;
    Boards[I].Left := 80 + (80 * I);
    Boards[I].Top :=  25;
    Boards[I].Width := 45;
    Boards[I].Height := 60;
    Boards[I].Tag := I;
    Boards[I].OnClick := BoardsClick;
    Boards[I].BevelWidth := 3;

    Coupling[I] := TPanel.Create(Self);
    Coupling[I].Parent := Self;
    Coupling[I].Left := 120 + (80 * I);
    Coupling[I].Top := 115;
    Coupling[I].Width := 45;
    Coupling[I].Height := 60;
    Coupling[I].Tag := I;
    Coupling[I].OnClick := CouplingClick;
    Coupling[I].BevelWidth := 3;
  end;
end;

procedure TPulseBoardEditor.FormPaint(Sender: TObject);
var I, Total : Integer;
begin
  Canvas.Pen.Width := 3;

  {
  if TGTOptions.getInstance.Lite then
    Total:= 2
  else
    Total:= 5;
    }
  Total:= TEDMOptions.getInstance.Boards - 1;

  with Canvas do begin
    for I := 0 to Total - 1 do begin
      if ((1 shl I) and Cpl) = 0 then
        Pen.Color := clBlack
      else
        Pen.Color := clYellow;

      MoveTo(Boards[I].Left + Boards[I].Width - 20, Boards[I].Top + Boards[I].Height);
      LineTo(Boards[I].Left + Boards[I].Width - 20, Coupling[I].Top + 20);
      LineTo(Coupling[I].Left, Coupling[I].Top + 20);

      MoveTo(Coupling[I].Left + 20, Coupling[I].Top);
      LineTo(Coupling[I].Left + 20, Boards[I].Top + 20);
      LineTo(Boards[I+1].Left, Boards[I].Top + 20);
    end;

    if ((1 shl Total) and Cpl) = 0 then
      Pen.Color := clBlack
    else
      Pen.Color := clYellow;

    MoveTo(Boards[Total].Left + Boards[Total].Width - 20, Boards[Total].Top + Boards[Total].Height);
    LineTo(Boards[Total].Left + Boards[Total].Width - 20, Coupling[Total].Top + 20);
    LineTo(Coupling[Total].Left, Coupling[Total].Top + 20);

    MoveTo(Coupling[Total].Left + 20, Coupling[Total].Top);
    LineTo(Coupling[Total].Left + 20, Boards[Total].Top - 20);
    LineTo(Boards[0].Left + 20, Boards[Total].Top - 20);
    LineTo(Boards[0].Left + 20, Boards[Total].Top);
  end;

end;

procedure TPulseBoardEditor.BoardsClick(Sender : TObject);
begin
  with Sender as TPanel do
    Brd := Brd xor (1 shl Tag);
  UpdateDisplay;
end;

procedure TPulseBoardEditor.CouplingClick(Sender : TObject);
begin
  with Sender as TPanel do
    Cpl := Cpl xor (1 shl Tag);

  UpdateDisplay;
end;

procedure TPulseBoardEditor.UpdateDisplay;
var I, Total : Integer;
begin
  {
  if TGTOptions.getInstance.Lite then
    Total:= 2
  else
    Total:= 5;
    }
  Total:= TEDMOptions.getInstance.Boards - 1;

  for I := 0 to Total do begin
    if (Brd and (1 shl I) <> 0) then begin
      Boards[I].Color := clYellow;
      Boards[I].BevelOuter := bvLowered;
    end else begin
      Boards[I].Color := clBlack;
      Boards[I].BevelOuter := bvRaised;
    end;

    if (Cpl and (1 shl I) <> 0) then begin
      Coupling[I].Color := clYellow;
      Coupling[I].BevelOuter := bvLowered;
    end else begin
      Coupling[I].Color := clBlack;
      Coupling[I].BevelOuter := bvRaised;
    end;
  end;
  Refresh;
end;

procedure TPulseBoardEditor.BitBtn3Click(Sender: TObject);
begin
  if Assigned(Apply) then
    Apply(Self, Brd, Cpl);

  UpdateDisplay;
end;

end.
