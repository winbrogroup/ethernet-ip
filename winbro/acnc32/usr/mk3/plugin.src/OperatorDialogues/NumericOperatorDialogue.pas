unit NumericOperatorDialogue;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, AbstractNCOperatorDialogue, INCInterface,  NamedPlugin,
  CncTypes, Buttons, DialogueForm, INamedInterface, PluginInterface;


type
  TSimpleEntry = class(TForm)
    Edit1: TEdit;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    lblCaption: TLabel;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  TSimpleNumericDialogue = class(TAbstractNCOperatorDialogue)
    class function Execute(Item : string) : TNCDialogueResult; override;
    class procedure Close; override;
  end;

  TSimpleTextDialogue = class(TAbstractNCOperatorDialogue)
    class function Execute(Item : string) : TNCDialogueResult; override;
    class procedure Close; override;
  end;

var
  SimpleEntry: TSimpleEntry;
  LastEntry: string;

implementation

{$R *.dfm}

{ TSimpleNumericDialogue }

class procedure TSimpleNumericDialogue.Close;
begin
  if Assigned(SimpleEntry) then
    SimpleEntry.ModalResult := mrCancel;
end;

class function TSimpleNumericDialogue.Execute(Item: string): TNCDialogueResult;
var
  OperationIndex: TTagRef;
begin
  SimpleEntry:= TSimpleEntry.Create(nil);
  try
    OperationIndex:= Named.AquireTag('OD_NumericEntry', 'TIntegerTag', nil);
    SimpleEntry.lblCaption.Caption := Item;
    case SimpleEntry.ShowModal of
      mrOK : begin
        Result := ncdrContinue;
        Named.SetAsInteger(OperationIndex, StrToIntDef(SimpleEntry.Edit1.Text, 0));
      end;
    else
      Result := ncdrStop;
      Named.SetAsInteger(OperationIndex, 0);
      Named.SetFaultA( 'ProgramEntry', 'Operator Dialogue: Operation Canceled', faultRewind, nil);
    end;
  finally
    SimpleEntry.Free;
    SimpleEntry:= nil;
  end;
end;

{ TSimpleTextualEntry }

class procedure TSimpleTextDialogue.Close;
begin
  if Assigned(SimpleEntry) then
    SimpleEntry.ModalResult := mrCancel;
end;

class function TSimpleTextDialogue.Execute(Item: string): TNCDialogueResult;
var
  OperationIndex: TTagRef;
begin
  SimpleEntry:= TSimpleEntry.Create(nil);
  try
    OperationIndex:= Named.AquireTag('OD_TextEntry', 'TStringTag', nil);
    SimpleEntry.lblCaption.Caption := Item;
    SimpleEntry.Edit1.Text:= LastEntry;
    case SimpleEntry.ShowModal of
      mrOK : begin
        Result := ncdrContinue;
        LastEntry:= SimpleEntry.Edit1.Text;
        Named.SetAsString( OperationIndex, PChar(SimpleEntry.Edit1.Text));
      end;
    else
      Result := ncdrStop;
      Named.SetAsString(OperationIndex, 'Operation Canceled');
      Named.SetFaultA( 'ProgramEntry', 'Operator Dialogue: Operation Canceled', faultRewind, nil);
    end;
  finally
    SimpleEntry.Free;
    SimpleEntry:= nil;
  end;
end;

initialization
  TAbstractNCOperatorDialogue.AddPlugin('SimpleEntry', TSimpleNumericDialogue);
  TAbstractNCOperatorDialogue.AddPlugin('TextEntry', TSimpleTextDialogue);
end.
