unit GeneralOperatorDialogue;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, AbstractNCOperatorDialogue, INCInterface,  NamedPlugin,
  CncTypes, Buttons,  INamedInterface, PluginInterface,CommonStuff;


type
  TGeneralInputForm = class(TForm)
    Edit1: TEdit;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    Label2: TLabel;
    PromptMemo: TMemo;
    DummySizeLabel: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormShow(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
  private
    { Private declarations }
    TopPos,LeftPos : Integer;
    MaxVal,MinVal : Double;
    NoMax,NoMin : Boolean;
    MustHaveInput : Boolean;
    IsNumeric : Boolean;
    DefaultValue : String;
    PromptWidth : Integer;
    InputSupplied : Boolean;
    procedure ParsePrompt(InStr : String);
    procedure ParseInputString(InStr : String);
  public
    InputNum : Double;
    InputStr : String;
    { Public declarations }
  end;

  TGeneralDataEntryDialogue = class(TAbstractNCOperatorDialogue)
    class function Execute(Item : string) : TNCDialogueResult; override;
    class procedure Close; override;
  end;


var
  GeneralInputForm: TGeneralInputForm;
//  LastEntry: string;

implementation

{$R *.dfm}

{ TSimpleNumericDialogue }

class procedure TGeneralDataEntryDialogue.Close;
begin
  if Assigned(GeneralInputForm) then
    GeneralInputForm.ModalResult := mrCancel;
end;

//Input string has following sysntax
//PromptText,[Deafult],[IsNumeric],[MustHaveInput]
class function TGeneralDataEntryDialogue.Execute(Item: string): TNCDialogueResult;
var
  InputData: TTagRef;
begin

  GeneralInputForm := TGeneralInputForm.Create(nil);
  try
    with GeneralInputForm do
      begin
      InputSupplied := False;
      ParseInputString(Item);
      Top := TopPos;
      Left := LeftPos;
      Edit1.Text := DefaultValue;
      InputData:= Named.AquireTag('GeneralDataInput', 'TStringTag', nil);
//      PromptMemo.LineslblCaption.Caption := Prompt;
      if IsNumeric then
        begin
        GeneralInputForm.Caption := 'Numeric Input';
        case GeneralInputForm.ShowModal of
          mrOK :
          begin
          Named.SetAsDouble(InputData,InputNum);
          Result := ncdrContinue;
          end;
        else
          begin
          Result := ncdrStop;
          Named.SetAsString(InputData,'');
          Named.SetFaultA( 'ProgramEntry', 'Operator Dialogue: Operation Canceled', faultRewind, nil);
          end;
        end; // case
        end
      else
        begin
        GeneralInputForm.Caption := 'Alphanumeric Input';
        case GeneralInputForm.ShowModal of
          mrOK :
          begin
          Named.SetAsString(InputData,PAnsiChar(InputStr));
          Result := ncdrContinue;
          end;
        else
          begin
          Result := ncdrStop;
          Named.SetAsString(InputData,'');
          Named.SetFaultA( 'ProgramEntry', 'Operator Dialogue: Operation Canceled', faultRewind, nil);
          end;
        end; // case
        end
      end;
  finally
    GeneralInputForm.Free;
    GeneralInputForm:= nil;
  end;
end;



{ TSimpleEntry }

//Input string has following sysntax
//PromptText,[Deafult],[IsNumeric],[MustHaveInput],[Top],[Left].[Min],[Max]
procedure TGeneralInputForm.ParseInputString(InStr: String);
var
Buffer : String;
Temp : String;
Prompt : String;
begin
MustHaveInput := False;
IsNumeric := False;
DefaultValue := '';
TopPos := 100;
LeftPos := 100;
Prompt := 'Please Enter a value';
NoMax := True;
NoMin := True;
Label2.Visible := False;
if length(InStr) > 0 then
  begin
  Buffer := Instr;
  Prompt := getToken(Buffer);
  ParsePrompt(Prompt);
  if length(Buffer) > 0 then
    begin
    DefaultValue := GetToken(Buffer);
    if length(Buffer) > 0 then
      begin
      Temp := GetToken(Buffer);
      if Temp = '1' then IsNumeric := true;
      if length(Buffer) > 0 then
        begin
        Temp := GetToken(Buffer);
        if Temp = '1' then MustHaveInput := True;
        if length(Buffer) > 0 then
          begin
          Temp := GetToken(Buffer);
          TopPos := StrToIntDef(Temp,100);
          if length(Buffer) > 0 then
            begin
            Temp := GetToken(Buffer);
            LeftPos := StrToIntDef(Temp,100);
            end;
            if length(Buffer) > 0 then
              begin
              Temp := GetToken(Buffer);
              try
              MaxVal := StrToFloat(Temp);
              NoMax := False
              except
              MaxVal := -1;
              end
              end;
              if length(Buffer) > 0 then
                begin
                Temp := GetToken(Buffer);
                try
                MinVal := StrToFloat(Temp);
                NoMin := False
                except
                MinVal := -1;
                end
                end;
          end;
        end;
      end
    end;
  end;

end;

procedure TGeneralInputForm.BitBtn1Click(Sender: TObject);
begin
if IsNumeric then
  begin
  try
  InputNum := StrToFloat(Edit1.Text);
  InputSupplied := True;
  if not NoMax then
    begin
    if InputNum > MaxVal then
      begin
      InputSupplied := False;
      Label2.Caption := Format('Value too high (Max %5.3f)',[MaxVal]);
      Label2.Visible := True;
      exit;
      end;
    end;
  if not NoMin then
    begin
    if InputNum < MinVal then
      begin
      InputSupplied := False;
      Label2.Caption := Format('Value too low (Min %5.3f)',[MinVal]);
      Label2.Visible := True;
      exit;
      end;
    end;
  ModalResult := MrOk;
  except
  if MustHaveInput then
    begin
    InputSupplied := False;
    Label2.Caption := 'Input must be Numeric';
    Label2.Visible := True;
    end
  else
    begin
    if Edit1.Text = '' then
      begin
      InputNum := 0.0;
      ModalResult := MrOk;
      end
    else
      begin
      InputSupplied := False;
      Label2.Caption := 'Input must be Numeric';
      Label2.Visible := True;
      end
    end
  end;
  end
else
  begin
  InputStr := Edit1.Text;
  InputSupplied := (Length(InputStr)>0);
  if MustHaveInput and not InputSupplied then
    begin
    Label2.Caption := 'Input must be Supplied';
    Label2.Visible := True;
    end
  else
    begin
    ModalResult := mrOk;
    end
  end;
end;

procedure TGeneralInputForm.FormShow(Sender: TObject);
begin
Label2.Caption := '';
  ClientHeight := BitBtn1.Top+BitBtn1.Height+5;
  Label2.Visible := False;
Top := TopPos;
Left := LeftPOs;
end;

procedure TGeneralInputForm.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
CanClose := True;
if MustHaveInput and not InputSupplied then
  begin
//  Label1.Visible := True;
  Label2.Visible := True;
  if IsNumeric then
    begin
    Label2.Caption := 'Numeric input is required';
    end
  else
    begin
    Label2.Caption := 'Input is required';
    end;
  CanClose := False;
  end;

end;

procedure TGeneralInputForm.FormCreate(Sender: TObject);
begin
NoMax := True;
NoMin := True;
ClientWidth := 400;
end;

procedure TGeneralInputForm.ParsePrompt(InStr: String);
var
CPos : Integer;
Buffer : String;
Remainder : String;
Found : Boolean;
MaxWidth : Integer;
TWidth : Integer;
LNum : Integer;
begin
if InStr <> '' then
  begin
  Found := True;
  PromptMemo.Lines.Clear;
  Remainder := InStr;
  MaxWidth := 300;
  while Found do
    begin
    CPos := Pos('~',Remainder);
    If CPos > 0 then
      begin
      Buffer := Copy(Remainder,1,Cpos-1);
      PromptMemo.Lines.Add(Buffer);
      Remainder := Copy(Remainder,Cpos+1,1000);
      end
    else
      Found := False;
    end;
  end;
  If Length(Remainder) > 0 then PromptMemo.Lines.Add(Remainder);
  PromptMemo.Height := 2+(PromptMemo.Lines.Count*19);
  for LNum := 0 to PromptMemo.Lines.Count-1 do
    begin
    TWidth := DummySizeLabel.Canvas.TextWidth(PromptMemo.Lines[Lnum])+5;
    If TWidth > MaxWidth then
      begin
      MaxWidth := TWidth;
//      DummySizeLabel.Caption := PromptMemo.Lines[Lnum];
      end;
    end;
  ClientWidth := MaxWidth+10;
  Edit1.Top := PromptMemo.Height+Label2.Height+5;
  BitBtn1.Top := Edit1.Top+Edit1.Height+5;
  BitBtn2.Top := BitBtn1.Top;
end;


initialization
  TAbstractNCOperatorDialogue.AddPlugin('GeneralDataEntry', TGeneralDataEntryDialogue);
end.
