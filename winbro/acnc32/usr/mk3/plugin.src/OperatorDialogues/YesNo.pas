unit YesNo;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons,AbstractNCOperatorDialogue, INCInterface,  NamedPlugin,
  CncTypes,  INamedInterface, PluginInterface,GeneralOperatorDialogue,CommonStuff;

type
  TYesNoForm = class(TForm)
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    PromptMemo: TMemo;
    DummySizeLabel: TLabel;
    procedure FormShow(Sender: TObject);
  private
    procedure ParsePrompt(InStr: String);
    procedure ParseInputString(InStr: String);
    { Private declarations }
  public
    { Public declarations }
    TopPos,LeftPOs : Integer;
    prompt : String;
  end;

  TYesNoDialogue = class(TAbstractNCOperatorDialogue)
    class function Execute(Item : string) : TNCDialogueResult; override;
    class procedure Close; override;
  end;

var
  YesNoForm: TYesNoForm;

implementation


procedure TYesNoForm.ParseInputString(InStr: String);
var
Buffer : String;
Temp : String;
Prompt : String;
begin
TopPos := 100;
LeftPos := 100;
Prompt := 'Please Enter Yes or No';
if length(InStr) > 0 then
  begin
  Buffer := Instr;
  Prompt := getToken(Buffer);
  ParsePrompt(Prompt);
  if length(Buffer) > 0 then
    begin
    if length(Buffer) > 0 then
      begin
      Temp := GetToken(Buffer);
      TopPos := StrToIntDef(Temp,100);
      if length(Buffer) > 0 then
         begin
         Temp := GetToken(Buffer);
         LeftPos := StrToIntDef(Temp,100);
         end;
      end
    end;
  end;

end;


procedure TYesNoForm.ParsePrompt(InStr: String);
var
CPos : Integer;
Buffer : String;
Remainder : String;
Found : Boolean;
MaxWidth : Integer;
TWidth : Integer;
LNum : Integer;
begin
if InStr <> '' then
  begin
  Found := True;
  PromptMemo.Lines.Clear;
  Remainder := InStr;
  MaxWidth := 300;
  while Found do
    begin
    CPos := Pos('~',Remainder);
    If CPos > 0 then
      begin
      Buffer := Copy(Remainder,1,Cpos-1);
      PromptMemo.Lines.Add(Buffer);
      Remainder := Copy(Remainder,Cpos+1,1000);
      end
    else
      Found := False;
    end;
  end;
  If Length(Remainder) > 0 then PromptMemo.Lines.Add(Remainder);
  PromptMemo.Height := 2+(PromptMemo.Lines.Count*19);
  for LNum := 0 to PromptMemo.Lines.Count-1 do
    begin
    TWidth := DummySizeLabel.Canvas.TextWidth(PromptMemo.Lines[Lnum])+5;
    If TWidth > MaxWidth then
      begin
      MaxWidth := TWidth;
//      DummySizeLabel.Caption := PromptMemo.Lines[Lnum];
      end;
    end;
  ClientWidth := MaxWidth+10;
  BitBtn1.Top := PromptMemo.Height++5;
  BitBtn2.Top := BitBtn1.Top;
end;

{$R *.dfm}

{ TGeneralDataEntryDialogue }

class procedure TYesNoDialogue.Close;
begin
  inherited;
  if Assigned(YesNoForm) then
    YesNoForm.ModalResult := mrCancel;
end;

class function TYesNoDialogue.Execute(Item: string): TNCDialogueResult;
var
  OutputData: TTagRef;
begin
  YesNoForm := TYesNoForm.Create(nil);
  try
  with YesNoForm do
      begin
      ParseInputString(Item);
      Top := TopPos;
      Left := LeftPos;
      OutputData:= Named.AquireTag('YesNoDataOutput', 'TIntegerTag', nil);
        case YesNoForm.ShowModal of
          MrYes:
          begin
          Named.SetAsInteger(OutputData,1);
          end;

        else
          begin
          Named.SetAsInteger(OutputData,0);
          end;
        end;
      end;
  finally
  YesNoForm.Free;
  YesNoForm:= nil;
  end;
end;

procedure TYesNoForm.FormShow(Sender: TObject);
begin
ClientHeight := BitBtn1.Top+BitBtn1.Height+5;
Top := TopPos;
Left := LeftPOs;
end;

initialization
  TAbstractNCOperatorDialogue.AddPlugin('YesNoEntry', TYesNoDialogue);

end.
