unit DialogueForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, IFormInterface;

type
  TDialogue = class(TInstallForm)
  private
  public
    constructor Create(AOwner: TComponent); override;
  end;

var
  Dialogue: TDialogue;

implementation

{$R *.dfm}

{ TDialogue }

constructor TDialogue.Create(AOwner: TComponent);
begin
  inherited;
  Dialogue:= Self;
end;

end.
