unit OPPHeader;
{23-Jun-99 mgl ToolUsage is now an integer}
interface

uses Windows, SysUtils, Classes, Graphics, Forms, Controls, StdCtrls,
  Buttons, ExtCtrls, ComCtrls, AbstractOPPPlugin, Tokenizer, NamedPlugin;

type
  TOPPFile = class
    PartType: String;
    Comment: String;
    ToolType: Integer;
    ElectrodeCount: Integer;
    ToolUsage: Integer;  {mm}
    CycleTime: Integer; {sec}
    ToolXOffset : Double;
    ToolYOffset : Double;
    procedure Load(S: TStringTokenizer); {from strings}
    procedure Save(S: TStrings); {to strings}
  end;

  TOPPHeaderForm = class(TForm)
    OKBtn: TButton;
    CancelBtn: TButton;
    PartTypeLabel: TLabel;
    PartTypeEdit: TEdit;
    Label1: TLabel;
    ToolUsageEdit: TEdit;
    ToolUsageLabel: TLabel;
    ToolUsageUD: TUpDown;
    ElectrodeCountLabel: TLabel;
    ElectrodeCountEdit: TEdit;
    ElectrodeCountUD: TUpDown;
    ToolTypeEdit: TEdit;
    ToolTypeUD: TUpDown;
    ReferenceEdit: TEdit;
    ReferenceLabel: TLabel;
    CycleTimeLabel: TLabel;
    CycleTimeEdit: TEdit;
    CycleTimeUD: TUpDown;
    ToolXOffsetEdit: TEdit;
    ToolYOffsetEdit: TEdit;
    ToolXOffsetLabel: TLabel;
    ToolYOffsetLabel: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    OPPData: TOPPFile;
    procedure DataToForm;
    procedure FormToData;
  end;

  TOPPHeaderEditor = class(TAbstractOPPPluginEditor)
  public
    class function Execute(var aSection: string): Boolean; override;
  end;

  TActiveOppServer = class(TAbstractOPPPluginConsumer)
  private
  public
    FActiveOppFile : TOppFile;
    constructor Create(aSectionName : string); override;
    procedure Consume(aSectionName, Buffer : PChar); override;
  end;


const
  SectionOperationDescription = 'OPERATIONDESCRIPTION';

implementation

{$R *.DFM}

var
    PartTypeNDX : Integer;
    CommentNDX: Integer;
    ToolTypeNDX: Integer;
    ElectrodeCountNDX: Integer;
    ToolUsageNDX: Integer;  {mm}
    CycleTimeNDX: Integer; {sec}
    ToolXOffsetNDX : Integer;
    ToolYOffsetNDX : Integer;

{Each individual tool is identified on the Hypertac plug with a
 14 bit number, of which 9 bits identify the type and 5 bits are
 a type serial number. The total uniquely identifies the tool on
 the site}


procedure TOPPFile.Load(S: TStringTokenizer);
var Token : string;

  function ReadDoubleAssign : Double;
  var Code : Integer;
  begin
    Val(S.ReadStringAssign, Result, Code);
  end;

begin
  PartType := 'None';
  Comment := 'No Ref';
  ElectrodeCount := 10;

  Token := LowerCase(S.Read);
  while not S.EndOfStream do begin
    if Token = 'parttype' then
      PartType := S.ReadStringAssign
    else if Token = 'comment' then
      Comment := S.ReadStringAssign
    else if Token = 'tooltype' then
      ToolType := S.ReadIntegerAssign
    else if Token = 'toolusage' then
      ToolUsage := S.ReadIntegerAssign
    else if Token = 'cycletime' then
      CycleTime := S.ReadIntegerAssign
    else if Token = 'electrodecount' then
      ElectrodeCount := S.ReadIntegerAssign
    else if Token = 'toolxoffset' then
      ToolXOffset := ReadDoubleAssign
    else if Token = 'toolyoffset' then
      ToolYOffset := ReadDoubleAssign
    else
      raise Exception.Create('Unexpected token [' + Token + ']');
//      S.ReadStringAssign;

    Token := LowerCase(S.Read);
  end;
end;

procedure TOPPFile.Save(S: TStrings);
begin
  S.Clear;
  S.Add(Format('PartType = ''%s''',       [PartType]));
  S.Add(Format('Comment = ''%s''',      [Comment]));
  S.Add(Format('ToolType = %d',       [ToolType]));
  S.Add(Format('ToolUsage = %d',      [ToolUsage]));
  S.Add(Format('CycleTime = %d',      [CycleTime]));
  S.Add(Format('ElectrodeCount = %d', [ElectrodeCount]));
  S.Add(Format('ToolXOffset = %g',    [ToolXOffset]));
  S.Add(Format('ToolYOffset = %g',    [ToolYOffset]));
end;

class function TOPPHeaderEditor.Execute(var aSection : string): Boolean;
var
  Form: TOPPHeaderForm;
  S : TQuoteTokenizer;
begin
  Result:= false;
  Form:= TOPPHeaderForm.Create(Application);
  S:= TQuoteTokenizer.Create;
  try
    S.Text := aSection;
    S.Seperator := '=';
    S.QuoteChar := '''';
    S.Rewind;
    try
      Form.OPPData.Load(S);
    except
      on E : Exception do
        Application.ShowException(E);
    end;
    Form.DataToForm;
    Result:= Form.ShowModal = mrOK;
    if Result then begin
      Form.FormToData;
      Form.OPPData.Save(S);
      aSection := S.Text;
    end;
  finally
    S.Free;
    Form.Free
  end
end;


procedure TOPPHeaderForm.FormCreate(Sender: TObject);
begin
  OPPData:= TOPPFile.Create
end;

procedure TOPPHeaderForm.FormDestroy(Sender: TObject);
begin
  OPPData.Free
end;

procedure TOPPHeaderForm.DataToForm;
begin
  with OPPData do begin
    PartTypeEdit.Text:= PartType;
    ReferenceEdit.Text:= Comment;
    ToolTypeUD.Position:= ToolType;
    ElectrodeCountUD.Position:= ElectrodeCount;
    ToolUsageUD.Position:= ToolUsage {* 100};
    CycleTimeUD.Position:= CycleTime;
    ToolXOffsetEdit.Text := Format('%.4f', [ToolXOffset]);
    ToolYOffsetEdit.Text := Format('%.4f', [ToolYOffset]);
  end
end;

procedure TOPPHeaderForm.FormToData;
var Code : Integer;
begin
  with OPPData do begin
    PartType:= PartTypeEdit.Text;
    Comment:= ReferenceEdit.Text;
    ToolType:= ToolTypeUD.Position;
    ElectrodeCount:= ElectrodeCountUD.Position;
    ToolUsage:= ToolUsageUD.Position {/ 100};
    CycleTime:=  CycleTimeUD.Position;
    Val(ToolXOffsetEdit.Text, ToolXOffset, Code);
    Val(ToolYOffsetEdit.Text, ToolYOffset, Code);
  end
end;

constructor TActiveOppServer.Create(aSectionName : string);
begin
  inherited;
  FActiveOppFile := TOppFile.Create;
//  aParent.RegisterMemEvent(nameNewActiveFile, edgeChange, TStringTag, NewProgram);
end;

procedure TActiveOppServer.Consume(aSectionName, Buffer : PChar);
var S : TQuoteTokenizer;
begin
  S := TQuoteTokenizer.Create;
  try
    S.Text := Buffer;
    S.Seperator := '=';
    S.QuoteChar := '''';
    S.Rewind;
    FActiveOppFile.Load(S);

    with FActiveOPPFile do begin
      NamedPlugin.WriteString(PartTypeNDX, PartType);
      NamedPlugin.WriteString(CommentNDX, Comment);
      NamedPlugin.WriteDouble(ToolTypeNDX, ToolType);
      NamedPlugin.WriteDouble(ElectrodeCountNDX, ElectrodeCount);
      NamedPlugin.WriteDouble(ToolUsageNDX, ToolUsage);
      NamedPlugin.WriteDouble(CycleTimeNDX, CycleTime);
      NamedPlugin.WriteDouble(ToolXOffsetNDX, ToolXOffset);
      NamedPlugin.WriteDouble(ToolYOffsetNDX, ToolYOffset);
    end;
//    NotifyClients;
  finally
    S.Free;
  end;
end;

initialization
  TOPPHeaderEditor.AddPlugin(SectionOperationDescription, TOPPHeaderEditor);
  TActiveOppServer.AddPlugin(SectionOperationDescription, TActiveOppServer);

  PartTypeNDX := PublishItem('OPP_PartType', 'TStringTag', True);
  CommentNDX := PublishItem('OPP_HeaderComment', 'TStringTag', True);
  ToolTypeNDX := PublishItem('OPP_ToolIDCode', 'TIntegerTag', True);
  ElectrodeCountNDX:= PublishItem('OPP_ElectrodeCount', 'TIntegerTag', True);
  ToolUsageNDX:= PublishItem('OPP_ToolUsage', 'TIntegerTag', True);
  CycleTimeNDX:= PublishItem('OPP_CycleTime', 'TIntegerTag', True);
  ToolXOffsetNDX := PublishItem('OPP_GenericToolXOffset', 'TDoubleTag', True);
  ToolYOffsetNDX := PublishItem('OPP_GenericToolYOffset', 'TDoubleTag', True);
end.
