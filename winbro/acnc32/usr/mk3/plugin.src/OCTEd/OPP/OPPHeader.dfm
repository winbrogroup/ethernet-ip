object OPPHeaderForm: TOPPHeaderForm
  Left = 364
  Top = 297
  BorderStyle = bsDialog
  Caption = 'Operation Part Program Header'
  ClientHeight = 258
  ClientWidth = 380
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = True
  Position = poScreenCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object PartTypeLabel: TLabel
    Left = 54
    Top = 26
    Width = 46
    Height = 13
    Alignment = taRightJustify
    Caption = '&Part Type'
    FocusControl = PartTypeEdit
  end
  object Label1: TLabel
    Left = 52
    Top = 78
    Width = 48
    Height = 13
    Alignment = taRightJustify
    Caption = '&Tool Type'
    FocusControl = ToolTypeEdit
  end
  object ToolUsageLabel: TLabel
    Left = 194
    Top = 78
    Width = 55
    Height = 13
    Alignment = taRightJustify
    Caption = 'Tool &Usage'
    FocusControl = ToolUsageEdit
  end
  object ElectrodeCountLabel: TLabel
    Left = 24
    Top = 104
    Width = 76
    Height = 13
    Alignment = taRightJustify
    Caption = '&Electrode Count'
    FocusControl = ElectrodeCountEdit
  end
  object ReferenceLabel: TLabel
    Left = 50
    Top = 52
    Width = 50
    Height = 13
    Alignment = taRightJustify
    Caption = '&Reference'
    FocusControl = ReferenceEdit
  end
  object CycleTimeLabel: TLabel
    Left = 197
    Top = 104
    Width = 52
    Height = 13
    Alignment = taRightJustify
    Caption = 'C&ycle Time'
    FocusControl = CycleTimeEdit
  end
  object ToolXOffsetLabel: TLabel
    Left = 32
    Top = 136
    Width = 62
    Height = 13
    Caption = 'Tool X Offset'
  end
  object ToolYOffsetLabel: TLabel
    Left = 32
    Top = 160
    Width = 59
    Height = 13
    Caption = 'ToolY Offset'
  end
  object OKBtn: TButton
    Left = 171
    Top = 213
    Width = 76
    Height = 25
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 12
  end
  object CancelBtn: TButton
    Left = 258
    Top = 213
    Width = 75
    Height = 25
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 13
  end
  object PartTypeEdit: TEdit
    Left = 111
    Top = 20
    Width = 221
    Height = 21
    TabOrder = 0
    Text = 'PartTypeEdit'
  end
  object ToolUsageEdit: TEdit
    Left = 260
    Top = 72
    Width = 59
    Height = 21
    TabOrder = 4
    Text = '0'
  end
  object ToolUsageUD: TUpDown
    Left = 319
    Top = 72
    Width = 15
    Height = 21
    Associate = ToolUsageEdit
    Min = 0
    Max = 1000
    Position = 0
    TabOrder = 5
    Wrap = False
  end
  object ElectrodeCountEdit: TEdit
    Left = 111
    Top = 98
    Width = 59
    Height = 21
    TabOrder = 6
    Text = '1'
  end
  object ElectrodeCountUD: TUpDown
    Left = 170
    Top = 98
    Width = 15
    Height = 21
    Associate = ElectrodeCountEdit
    Min = 1
    Max = 48
    Position = 1
    TabOrder = 7
    Wrap = False
  end
  object ToolTypeEdit: TEdit
    Left = 111
    Top = 72
    Width = 59
    Height = 21
    TabOrder = 2
    Text = '0'
  end
  object ToolTypeUD: TUpDown
    Left = 170
    Top = 72
    Width = 15
    Height = 21
    Associate = ToolTypeEdit
    Min = 0
    Max = 511
    Position = 0
    TabOrder = 3
    Wrap = False
  end
  object ReferenceEdit: TEdit
    Left = 111
    Top = 46
    Width = 221
    Height = 21
    TabOrder = 1
    Text = 'ReferenceEdit'
  end
  object CycleTimeEdit: TEdit
    Left = 260
    Top = 98
    Width = 59
    Height = 21
    TabOrder = 8
    Text = '0'
  end
  object CycleTimeUD: TUpDown
    Left = 319
    Top = 98
    Width = 15
    Height = 21
    Associate = CycleTimeEdit
    Min = 0
    Max = 20000
    Position = 0
    TabOrder = 9
    Wrap = False
  end
  object ToolXOffsetEdit: TEdit
    Left = 112
    Top = 128
    Width = 121
    Height = 21
    TabOrder = 10
    Text = 'ToolXOffsetEdit'
  end
  object ToolYOffsetEdit: TEdit
    Left = 112
    Top = 160
    Width = 121
    Height = 21
    TabOrder = 11
    Text = 'ToolYOffsetEdit'
  end
end
