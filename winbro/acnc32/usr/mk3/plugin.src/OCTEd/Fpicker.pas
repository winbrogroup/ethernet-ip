unit Fpicker;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ACNC_FileList, StdCtrls, FileCtrl, ExtCtrls, Grids, Buttons,
  SelectGrid,NamedPlugin,PLuginInterface,ACNC_SHEditor, ACNC_BaseKeyBoards;

type
TFPModes = (fpLoadFile,fpSaveAs);
TfpDisplayModes = (fpdPart,fpdOPP);

TSearchKeys = set of Char;

  TFilePickerForm = class(TForm)
    MainKB: TBaseKB;
    pp1: TPanel;
    ControlPanel: TPanel;
    Flist: TFileList;
    Grid: TStringGrid;
    TitlePanel: TPanel;
    NamePanel: TPanel;
    SizePanel: TPanel;
    DatePanel: TPanel;
    ButtonPanel: TPanel;
    OKButton: TButton;
    CancelButton: TButton;
    UpButton: TBitBtn;
    DownButton: TBitBtn;
    TopButton: TBitBtn;
    EndButton: TBitBtn;
    FileNamePanel: TPanel;
    KPad: TBaseFPad;
    PartNamePanel: TPanel;
    PartNameDisplay: TEdit;
    DifferentPartButton: TButton;
    NewPartButton: TButton;
    NewPartPanel: TPanel;
    NewPartEdit: TEdit;
    NewPartOKButton: TButton;
    NewPartCancelButton: TButton;
    Label1: TLabel;
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure GridDrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure NamePanelClick(Sender: TObject);
    procedure DatePanelClick(Sender: TObject);
    procedure GridSelectCell(Sender: TObject; ACol, ARow: Integer;
      var CanSelect: Boolean);
    procedure UpButtonMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure DownButtonMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure GridTopLeftChanged(Sender: TObject);
    procedure GridMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure TopButtonMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure EndButtonMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure MainKBKey(Sender: TObject; Key: Char);
    procedure OKButtonClick(Sender: TObject);
    procedure CancelButtonClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure GridDblClick(Sender: TObject);
    procedure NewPartButtonClick(Sender: TObject);
    procedure DifferentPartButtonClick(Sender: TObject);
    procedure NewPartOKButtonClick(Sender: TObject);
    procedure NewPartCancelButtonClick(Sender: TObject);
    procedure GridMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
  private
    { Private declarations }
    FileInput              : TSHCursorEdit;
    UpdateTopArrows        : Boolean;
    UpdateBottomArrows     : Boolean;
    PartList               : TStringList;
    DisplayMode            : TfpDisplayModes;
    SelectedPartName       : String;
    OriginalName           : String;
//    OriginalPart           : String;
    OriginalBase           : String;
    SaveAsPart             : String;
    MyTopRow               : Integer;

    procedure SetSelectedRow(Value : Integer);
    procedure MakePartList(DList : TStringList;Base : String);
    procedure InitGridDisplay(Mode :TfpDisplayModes);
    function  NewPartValid(var NewPartPath : String; PName : String):String;
    procedure SetNewPartMode(ModeOn : Boolean);

  public
    { Public declarations }
    FPMode : TFPModes;
    SelectedFile : String;
    SelectedRow : INteger;
    BasePath    : String;
    FileFilter  : String;
  end;

var
  FilePickerForm: TFilePickerForm;

implementation
var
PartDirIndex :Integer;
{$R *.DFM}
procedure TFilePickerForm.SetSelectedRow(Value : Integer);
begin
SelectedRow :=Value;
if (FPmode = fpSaveAs) and (DisplayMode=fpdOPP) then
   begin
   FileInput.Text := Grid.Cols[0][SelectedRow];
   end;
end;


procedure TFilePickerForm.FormResize(Sender: TObject);
var
TotalGridHeight : Integer;
begin
   Top := 75;
   Left := 0;
   pp1.Left := 1;
   pp1.Top := TitlePanel.Height+1;
if FPMode = fpLoadFile then
   begin
   NewPartButton.Visible := False;
   DifferentPartButton.Enabled := False;
   MainKb.FocusControl := nil;
   MainKb.ExternalMode := True;
   Kpad.Visible := False;
   FilenamePanel.Visible := False;
   pp1.Height := 413-(TitlePanel.Height+1);// this gives an exact number of rows and stops scrolling when the last row is clicked;
   TotalGridHeight := pp1.Height+ TitlePanel.Height+1;
   pp1.Width := 862;
   pp1.Invalidate;
   Grid.Align := alclient;
   MainKB.Height := Height- TotalGridHeight;
   MainKb.Top := TotalGridHeight+2;
   MainKb.Width := width-8;
   MainKB.Left := 4;
   end
else
    begin
    // Here for Save as
    NewPartButton.Visible := True;
    DifferentPartButton.Enabled := False;

    FilenamePanel.Visible := True;
    MainKb.FocusControl := FileInput;
    MainKb.ExternalMode := False;
    Kpad.Visible := True;
    pp1.Height := 373-(TitlePanel.Height+1);// this gives an exact number of rows and stops scrolling when the last row is clicked;
    pp1.Width := 862;
    pp1.Invalidate;
    Grid.Align := alclient;
    with FilenamePanel do
         begin
         Top := 373;
         Height := 40;
         Width := pp1.Width;
         Left :=1;
         end;
    TotalGridHeight := 413;
    with FileInput do
         begin
         Top := 2;
         Left := 2;
         Height := FilenamePanel.Height-4;
         Width := FilenamePanel.Width-4;
         end;
    MainKB.Height := Height- (TotalGridHeight)-2;
    MainKb.Top := TotalGridHeight+2;
    MainKb.Width := (Width div 100)*78;
    MainKB.Left := 4;
    KPad.Height := Height- (TotalGridHeight)-2;
    KPad.Top := TotalGridHeight+2;
    KPad.Width := Width-MainKb.Width-2;
    KPad.Left := MainKb.Width+2;
    end;

ControlPanel.Top :=45;
ControlPanel.Width := Width-pp1.Width-4;
ControlPanel.Left := pp1.width+4;
ControlPanel.Height :=410-45;

ButtonPanel.Width := ControlPanel.Width-8;
ButtonPanel.Top := 280;

OKButton.width := ButtonPanel.Width-4;
CancelButton.width := ButtonPanel.Width-4;
TopButton.Top := 2;
TopButton.Left := 4;
TopButton.Width := ControlPanel.Width-8;

UpButton.Left := 4;
UpButton.Top := TopButton.Height+4;
UpButton.Width := ControlPanel.Width-8;

DownButton.Left := 4;
DownButton.Top := UpButton.Top+UpButton.Height+20;
DownButton.Width := UpButton.Width;

EndButton.Left := 4;
EndButton.Top := DownButton.Top+DownButton.Height+2;
EndButton.Width := UpButton.Width;


PartNameDisplay.Top := 4;
PartNameDisplay.Height := PartNamePanel.Height-6;
with DifferentPartButton do
     begin
     Top := 3;
     Left := PartNamePanel.Width-Width-2;
     Height := PartNamePanel.Height-6;
     end;

     with NewPartButton do
     begin
     Top := 3;
     Left := DifferentPartButton.Left-Width-2;
     Height := PartNamePanel.Height-6;
     end;
end;

procedure TFilePickerForm.FormCreate(Sender: TObject);
begin
MyTopRow := 0;
FileInput := TSHCursorEdit.Create(Self);
with FileInput do
     begin
     Parent :=  FileNamePanel;
     Font.Color := clYellow;
     Font.Size := 16;
     Color := clBlack;
     end;

KPad.FocusControl := FileInput;
KPad.ExternalMode := False;
Width := 1022;
Height := 690;
Grid.Canvas.Font := Grid.Font;
PartList := TStringList.create;

PartList.Sort;

end;

procedure TFilePickerForm.GridDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
var
Contents : String;
begin
Contents := Grid.Cells[ACol,Arow];

with Grid.Canvas do
     begin
     if ARow = SelectedRow then
        begin
        Brush.Color := clYellow;
        Font.Color := clMaroon;
        end
     else
        begin
        Brush.Color := clWhite;
        Font.Color := clBlack;
        end;
     FillRect(Rect);
     if ACol = 2 then
        begin
        TextOut(Rect.Left+30,Rect.Top+4,Contents);
        end
     else
        begin
        TextOut(Rect.Left+3,Rect.Top+4,Contents);
        end;
     exit;// !!!!!test
     if (Not UpdateTopArrows) and (Not UpdateBottomArrows) then exit;
     If (Arow = Grid.TopRow) and (Arow > 0) and (ACol=3) then
        begin
        Pen.Color := clRed;
        MoveTo(Rect.Right-30,Rect.Bottom-2);
        LineTo(Rect.Right-10,Rect.Bottom-2);
        LineTo(Rect.Right-20,Rect.Bottom-22);
        LineTo(Rect.Right-30,Rect.Bottom-2);
        Brush.color := clRed;
        FloodFill(Rect.Right-20,Rect.Bottom-10,clRed,fsBorder);
        UpdateTopArrows := False;
        end;

     If (Arow = Grid.TopRow+Grid.VisibleRowCount-1) and (Grid.RowCount-1 > Arow) and (ACol=3) then
        begin
        Pen.Color := clRed;
        MoveTo(Rect.Right-30,Rect.Top+2);
        LineTo(Rect.Right-10,Rect.Top+2);
        LineTo(Rect.Right-20,Rect.Top+22);
        LineTo(Rect.Right-30,Rect.Top+2);
        Brush.color := clRed;
        FloodFill(Rect.Right-20,Rect.Top+5,clRed,fsBorder);
        UpdateBottomArrows := False;
        end;
      end;
end;

procedure TFilePickerForm.NamePanelClick(Sender: TObject);
begin
// Here to sort by Name;
if DisplayMode = fpdPart then exit;
Flist.UpdateList;
Flist.SortByName;

Grid.RowCount := Flist.resultList.count;
if (Flist.resultList.count > 0) then
    begin
    Grid.RowCount := Flist.resultList.count;
    Grid.Cols[0] := Flist.NameList;
    Grid.Cols[1] := Flist.DateList;
    Grid.Cols[2] := Flist.SizeList;
    end
else
    begin
    Grid.RowCount := 1;
    Grid.Cells[0,0] := 'Nothing to Display !!!';
    end;
end;

procedure TFilePickerForm.DatePanelClick(Sender: TObject);
begin
if DisplayMode = fpdPart then exit;
Flist.UpdateList;
Flist.SortByDate;
Grid.RowCount := Flist.resultList.count;
Grid.Cols[0] := Flist.NameList;
Grid.Cols[1] := Flist.DateList;
Grid.Cols[2] := Flist.SizeList;
end;

procedure TFilePickerForm.GridSelectCell(Sender: TObject; ACol, ARow: Integer;
  var CanSelect: Boolean);
begin
if ARow <> SelectedRow then
   begin
   SetSelectedRow(ARow);
   if (Arow = Grid.TopRow+Grid.VisibleRowCount) and (Grid.RowCount>Arow) then exit;
   Grid.Invalidate;
   end;

end;

procedure TFilePickerForm.UpButtonMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
if SelectedRow = 0 then exit;
SetSelectedRow(SelectedRow-1);
if SelectedRow < Grid.TopRow then
   begin
   Grid.TopRow := SelectedRow;
   MyTopRow := SelectedRow;
   end;
Grid.Invalidate;
end;

procedure TFilePickerForm.DownButtonMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var
BottomRow : Integer;
begin
BottomRow := Grid.TopRow + Grid.VisibleRowCount;
if SelectedRow >= Grid.RowCount-1 then exit;
SetSelectedRow(SelectedRow+1);
if SelectedRow >= BottomRow then
   begin
   Grid.TopRow := SelectedRow-Grid.VisibleRowCount+1;
   MyTopRow := Grid.TopRow;
   end;
Grid.Invalidate;

end;

procedure TFilePickerForm.GridTopLeftChanged(Sender: TObject);
begin
//UpdateTopArrows := True;
//UpdateBottomArrows := True;
end;

procedure TFilePickerForm.GridMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var
Col,Row : Integer;
TopDif  : Integer;

begin
TopDif := Grid.TopRow - myTopRow;
Grid.MouseToCell(X, Y,Col,Row);
if Row = -1 then Row := Grid.RowCount;
Row := Row-TopDif;
if Row >= Grid.RowCount then Row := Grid.RowCount-1;
if Row <> SelectedRow then
   begin
   SetSelectedRow(Row);
   if (row = Grid.TopRow+Grid.VisibleRowCount) and (Grid.RowCount>row) then exit;
   Grid.Invalidate;
   end;
end;

procedure TFilePickerForm.TopButtonMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
if SelectedRow = 0 then exit;
SetSelectedRow(0);
if SelectedRow < Grid.TopRow then
   begin
   Grid.TopRow := SelectedRow;
   MyTopRow := Grid.TopRow;
   end;
Grid.Invalidate;

end;

procedure TFilePickerForm.EndButtonMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
SetSelectedRow(Grid.RowCount-1);
Grid.TopRow := SelectedRow-Grid.VisibleRowCount+1;
MyTopRow := Grid.TopRow;
Grid.Invalidate;
end;

procedure TFilePickerForm.MainKBKey(Sender: TObject;Key: Char);
var
SearchKeys : TSearchKeys;
SearchCode : Char;
RowCount   : Integer;
Found      : Boolean;
Buffer     : String;
BottomRow : Integer;

begin
if FPMode = fpSaveAs then exit;
BottomRow := Grid.TopRow + Grid.VisibleRowCount;

searchkeys := ['a'..'z','A'..'Z','0'..'9'];
Searchcode := Char(MainKb.ExternalKeyCode);
if (Searchcode in searchkeys) then
   begin
   RowCount := 0;
   Found := False;
   While (RowCount < Grid.RowCount) and ( not Found)do
       begin
       Buffer := AnsiUpperCase(Grid.Cols[0][Rowcount]);
       if Buffer[1] = SearchCode then Found := True;
       inc(RowCount);
       end;
   if Found then
      begin
      SetSelectedRow(RowCount-1);
      if SelectedRow >= BottomRow then
         begin
         Grid.TopRow := SelectedRow-Grid.VisibleRowCount+1;
         end
      else
          begin
          if SelectedRow < Grid.TopRow then
             begin
             Grid.TopRow := SelectedRow;
             end;
      end;
      Grid.Invalidate;
      end;
   end;

end;


procedure TFilePickerForm.OKButtonClick(Sender: TObject);
var
  sr: TSearchRec;
  FileAttrs: Integer;
  NewPartPath : String;

begin
if SelectedRow < 0 then exit;
if DisplayMode = fpdPart then
   begin
   BasePath := OriginalBase + Grid.Cols[0][SelectedRow];
   FList.Path := BasePath;
   DisplayMode := fpdOPP;
   SelectedPartName := Grid.Cols[0][SelectedRow];
   if FPMode = fpSaveAs then
      begin
      SaveAsPart := SelectedPartName;
      end;
   InitGridDisplay(DisplayMode);
   end
else
    begin
    if FPMode = fpLoadFile then
       begin
       NamedPlugin.WriteString(PartDirIndex, SelectedPartName);

       SelectedFile := BasePath+'\'+Grid.Cols[0][SelectedRow];
       if FileExists(SelectedFile) then
          begin
          ModalResult := mrOK;
          end
       else
           begin
           SelectedFile := '';
           ModalResult := mrCancel;
           end;
       end
    else
        begin
        // Here for save as
        SelectedFile := FileInput.Text;
        if SelectedFile = '' then
           begin
           ModalResult := mrCancel;
           end
        else
            begin
            if ExtractFileExt(SelectedFile) <>'.opp' then SelectedFile :=  SelectedFile+'.opp';
            FileAttrs := faDirectory;
            NewPartPath := OriginalBase + SaveAsPart;

            if FindFirst(NewPartPath, FileAttrs, sr) <> 0 then
                  begin
                  // If the save path does not exis (New Part Name)
                  // then create new directory to dtore opp
                  ForceDirectories(NewPartPath)
                  end;
            SelectedFile := NewPartPath + '\'+SelectedFile;
            ModalResult := mrOK;
            end;
        end;
    end;
end;

procedure TFilePickerForm.CancelButtonClick(Sender: TObject);
begin
ModalResult := mrCancel;
end;

procedure TFilePickerForm.FormShow(Sender: TObject);
var
OrgPath : String;
OPLength : Integer;
begin
if FPMode = fpLoadFile then
   begin
   OriginalBase := BasePath;
   FList.Path := BasePath;
   FList.Filter := FileFilter;
   FormResize(Self);
   MakePartList(PartList,BasePath);
   DisplayMode := fpdPart;
   InitGridDisplay(DisplayMode);
   end
else
    begin
    // Here For Save as
    OrgPath :=  ExtractFilePath(BasePath);
    FList.Path := OrgPath;
    OriginalName := ExtractFileName(BasePath);
    OPLength := Length(Orgpath);
    if OrgPath[OPLength] = '\' then OrgPath := Copy(OrgPath,1,OPLength-1);
    SaveAsPart := ExtractFileName(OrgPath);
    OriginalBase := ExtractFilePath(OrgPath);
    FList.Filter := FileFilter;
    FormResize(Self);
    DisplayMode := fpdOPP;
    InitGridDisplay(DisplayMode);
    FileInput.Text := OriginalName;
    end;
end;

procedure TFilePickerForm.MakePartList(DList : TStringList;Base : String);
var
  sr: TSearchRec;
  FileAttrs: Integer;
begin
 DList.Clear;
 FileAttrs := faDirectory;
 if FindFirst(Base+'*.*', FileAttrs, sr) = 0 then
  begin
  if (sr.name <> '.') and (sr.name <> '..') then
     begin
     Dlist.Add(sr.name);
     end;
  while FindNext(sr) = 0 do
        begin
        if (sr.Attr and FileAttrs) = sr.Attr then
           begin
           if (sr.name <> '.') and (sr.name <> '..') then
              begin
              Dlist.Add(sr.name);
              end;
           end;

        end;
  end;
  if (DList.Count > 1) then
    begin
    DList.Sort;
    end;
end;

procedure TFilePickerForm.InitGridDisplay(Mode :TfpDisplayModes);
begin
if Mode = fpdPart then
   begin
   with PartNameDisplay do
        begin
        Color := clInfoBk;
        Font.Color := clBlue;
        Text := 'Please Select Part Type';
        end;
   With Grid do
        begin
        ColCount := 1;
        ColWidths[0] := Width;
        RowCount := PartList.count;
        Cols[0] := PartList;
        end;
   end
else
   begin
   DifferentPartButton.Enabled := True;
   with PartNameDisplay do
        begin
        Color := clInfoBk;
        Font.Color := clNavy;
        if FPMode = fpSaveAs then
           begin
           Text := Format('Part: %s',[SaveAsPart]);
           end
        else
           begin
           Text := Format('Part: %s.  Now Select program',[SelectedPartName]);
           end

        end;
   With Grid do
        begin
        ColCount := 4;
        ColWidths[0] := 550;
        ColWidths[1] := 130;
        ColWidths[2] := ColWidths[1];
        ColWidths[3] := 45;
        RowCount := Flist.resultList.count;
        Invalidate;
        end;
   NamePanelClick(Self);
   UpdateTopArrows := True;
   UpdateBottomArrows := True;
   end;
SetSelectedRow(0);
MyTopRow := 0;
Grid.TopRow := 0;
if SelectedRow < Grid.TopRow then Grid.TopRow := SelectedRow;
Grid.Invalidate;

end;


procedure TFilePickerForm.FormDestroy(Sender: TObject);
begin
PartList.Free;
end;

procedure TFilePickerForm.GridDblClick(Sender: TObject);
begin
OKButtonClick(Self);
end;

procedure TFilePickerForm.NewPartButtonClick(Sender: TObject);
begin
   NewPartPanel.Left := 50;
   NewPartPanel.Top := 120;
   SetNewPartMode(True);


end;

procedure TFilePickerForm.NewPartOKButtonClick(Sender: TObject);
var
NewPartName : String;
PartBasePath    : String;
begin
  NewPartName := NewPartValid(PartBasePath,NewPartEdit.Text);
  if NewPartName <> '' then
     begin
     SaveAsPart := NewPartName;
     FList.Path := PartBasePath;
     FList.ClearFiles;
     Grid.RowCount := 1;
     Grid.Cells[0,0] := 'No Files';
     Grid.Cells[1,0] := '';
     Grid.Cells[2,0] := '';
     PartNameDisplay.Text := NewPartName;
     end;
SetNewPartMode(False);
end;

procedure TFilePickerForm.NewPartCancelButtonClick(Sender: TObject);
begin
SetNewPartMode(False);
end;

procedure TFilePickerForm.SetNewPartMode(ModeOn : Boolean);
begin
if ModeOn then
   begin
   NewPartPanel.Visible := True;
   NewPartEdit.Text := '';
   NewPartEdit.SetFocus;
   ControlPanel.Visible := False;
   DifferentPartButton.Enabled := False;
   MainKb.FocusControl := NewPartEdit;
   KPad.FocusControl := NewPartEdit;
   FileInput.Enabled := False;
   end
else
   begin
   NewPartPanel.Visible := False;
   ControlPanel.Visible := True;
   DifferentPartButton.Enabled := True;
   FileInput.Enabled := True;
   MainKb.FocusControl := FileInput;
   KPad.FocusControl := FileInput;
   end;


end;


function TFilePickerForm.NewPartValid(var NewPartPath : String; PName : String):String;
var
FullPath : String;
PLength  : Integer;
CurrentPartName : String;
CurrentBasePath : String;
sr: TSearchRec;
FileAttrs: Integer;


begin
// On entry to File Save as option the Path passed in is the fullpath
// of the opp being saved built up of BasePath+PartDirectory+Oppname.opp

// Remove OppName
FullPath := ExtractFilePath(BasePath);
PLength := Length(FullPath);
if FullPath[Plength]= '\' then
   begin
   FullPath := Copy(FullPath,0,PLength-1);
   end;
CurrentPartName := ExtractFileName(FullPath);
CurrentBasePath := ExtractFilePath(FullPath);
NewPartPath := CurrentBasePath+PName;

FileAttrs := faDirectory;
if FindFirst(NewPartPath, FileAttrs, sr) <> 0 then
   begin
   Result := PName;
   end
else
    begin
    Result := '';
    end;


PartNameDisplay.Text := NewPartPath;


end;

procedure TFilePickerForm.DifferentPartButtonClick(Sender: TObject);
begin
if FPmode = fpLoadFile then
   begin
   MakePartList(PartList,OriginalBase);
   DisplayMode := fpdPart;
   InitGridDisplay(DisplayMode);
   end
else
   begin
   MakePartList(PartList,OriginalBase);
   DisplayMode := fpdPart;
   InitGridDisplay(DisplayMode);
   end;

end;


procedure TFilePickerForm.GridMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
MyTopRow := Grid.TopRow;
end;

initialization

  PartDirIndex := PublishItem('ActivePartDirectory', 'TStringTag', True);



end.
