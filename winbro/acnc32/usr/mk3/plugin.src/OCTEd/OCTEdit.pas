unit OCTEdit;


{ TODO : Move part select button panel to left for consistency } // Done
{ TODO : Reduce size of initial part select panel } // Done
{ TODO : Include dependecy editor } // Done
{ TODO : make grid selected row persistent in InitGrid with part routine}  // Done
interface






uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ACNC_BaseKeyBoards, ExtCtrls,iniFiles, FileList, ACNC_TouchSoftkey, StdCtrls,
  Buttons, Grids, FileCtrl, Mk3OCTFileSelect, ComCtrls,OCTTouchFilePicker,
  NamedPlugin,IFormInterface,abstractFormPlugin,RecordLists,FileUtils,
  ACNC_FileList;


{NoOPPText         = 'NO OPPS IN OCT';
NoOctsText        = 'No Octs in Part';
NoPartsText       = 'No Parts Selected';
OPPExistsText     = 'An Opp of that name already exists';
DestExistsText    = 'Destination OCT already exists';
PartDirNExistText = 'Part Directory Does Not exist';
OCTNExistText     = 'OCT (%s) Does not exist in Selected Part';
IndexText         = 'Index';
NameText          = 'Name';
ValidText         = 'Valid';
DependencyText    = 'Dependency';
OKText            = 'OK';
FailedText        = 'Failed';
OCTNameText       = 'OCT Name';
PCDText           = 'Please Confirm Deletion of OCT  ';
PartExistsText    = 'A Part of that Name Already Exists';
PPOKText          = 'Please Press OK and try again';
InvalidPartText   = 'Invalid Part Name entered !!';
OKRefText         = '%-35s Referenced Only in %s and Exists. OK';
NotExistText      = '%-35s Referenced in %s does not exist in part Directory';
MultiRefText      = '%-35s Referenced in %d OCTS';
EmptyOCTText      = '%-35s has no OPPs (EMPTY OCT)';
NoRefText         = '%s is not referenced in any OCT';
DeletingText      = 'Deleting OPP .. %s ....';
PartGoodText      = 'Part Integrity is Good';
PartFailedText    = 'Part Integrity Failed';
OCTNotFoundText   = 'OCT File Not Found';
InvalidPartNameText = 'Invalid Part Name Entered';
PPAckTextandRetry   = 'Please press Acknowledge and Try Again';
CantCopyPartText    = 'Error Copying Part, Operation Failed';
PartCopySuccessText = 'Part Copied Successfully';
PPAckToCompText     = 'Please Press Acknowledge to Continue';
CantCopyExistsText  = 'An OPP of that name Already exists In the Part';
NotCopiedText       = 'File has NOT been copied';
PleaseConfirmText   = 'Please Confirm Deletetion of %s';
OrCancelText        = 'or Pres Cancel to Abort';
NoPartSelectedText  = 'Delete Failed....No Part Selected';
SuccesDeleteText    = 'Part Succesfully Deleted';
PPAckText           = 'Please Press Acknowledge';
SelectNewPartText   = 'and then select a different Part';
PartDeleteFailText  = 'Failed to Delete Part';
NoOCTText           = 'No OCT is Selected';
OCTExistsText       = 'An OCT of the specified Name already Exists';
RenameText          = 'Please input New Name For %s';
NoneText            = 'None';}

const
MainPanelHeight = 390;
SoftPanelHeight = 60;
DefaultKeyWidth = 95;
type
  TKeyModes = (kmOCT,kmPart);
  TEditModes = (emSelectPart,emOCT,emPart);
//  TCopyModes = (cmCopy,cmMakeNew);
  TIntegrityResults = set of (HasEmptyOCT,HasDuplicateOPPRef,HasRedundentOPPs,BadDirectory);
  TMessMode = (mmError,mmInfo);

  TAOCTForm = class(TInstallForm)

    MainKB: TBaseKB;
    KPad: TBaseFPad;
    MainPanel: TPanel;
    SoftKeyPanel: TPanel;
    PartTypePanel: TPanel;
    CursorPanel: TPanel;
    CUpKey: TBitBtn;
    CDownKey: TBitBtn;
    OKButton: TButton;
    PartEditKey: TTouchSoftkey;
    OpenDialog1: TOpenDialog;
    CPartLabel: TLabel;
    PartNameDisplay: TEdit;
    OCTLabel: TLabel;
    OCTNameDisplay: TEdit;
    Flist: TFileList;
    OPPKeyPanel: TPanel;
    AppendOPPKey: TTouchSoftkey;
    InsertOPPKey: TTouchSoftkey;
    DeleteOPPKey: TTouchSoftkey;
    MoveOPPUpKey: TTouchSoftkey;
    MoveOPPDownKey: TTouchSoftkey;
    PartKeyPanel: TPanel;
    CopyNewPartKey: TTouchSoftkey;
    DeletePartKey: TTouchSoftkey;
    CheckIntegrityKey: TTouchSoftkey;
    NewPartKey: TTouchSoftkey;
    ChangePartKey: TTouchSoftkey;
    ADDOCTKey: TTouchSoftkey;
    MainDialoguePanel: TPanel;
    Label2: TLabel;
    OCTCountDisplay: TEdit;
    DeleteOCTKey: TTouchSoftkey;
    DialogPages: TPageControl;
    RenameOCTDialog: TTabSheet;
    RenameMessage: TLabel;
    ReNameEdit: TEdit;
    CopyOKButton: TButton;
    CopyCancelButton: TButton;
    DeleteOCTDialog: TTabSheet;
    Panel1: TPanel;
    DeleteTextLabel: TLabel;
    NBLabel1: TLabel;
    NBLabel2: TLabel;
    DeleteOCTOKButton: TButton;
    DeleteOCTCancelButton: TButton;
    OCTEditKey: TTouchSoftkey;
    NewPartDialog: TTabSheet;
    PleaseInputLabel: TLabel;
    NewPartInput: TEdit;
    NewPartOKButton: TButton;
    NewPartCancelButton: TButton;
    PartErrorPage: TTabSheet;
    Panel2: TPanel;
    Button1: TButton;
    PartText1: TLabel;
    PartText2: TLabel;
    MainPages: TPageControl;
    GridSheet: TTabSheet;
    Grid: TStringGrid;
    ReportSheet: TTabSheet;
    ReportM: TMemo;
    DeleteUnusedButton: TTouchSoftkey;
    DeleteEmptyOCTButton: TTouchSoftkey;
    ErrorSheet: TTabSheet;
    ErrorPanel: TPanel;
    ErrorAckButton: TTouchSoftkey;
    ErrorLine1: TLabel;
    ErrorLine2: TLabel;
    ErrorLine3: TLabel;
    ErrorLine4: TLabel;
    CopyPartSheet: TTabSheet;
    CopyPartName: TEdit;
    Label8: TLabel;
    CopyPartButton: TButton;
    CopyPartCancelButton: TButton;
    DeletePartDialog: TTabSheet;
    Panel3: TPanel;
    DelPartOKButton: TButton;
    DelPartCancelButton: TButton;
    DeletePartMessage: TLabel;
    RenameOCTKey: TTouchSoftkey;
    NewOCTDialog: TTabSheet;
    PleaseInputOCTLabel: TLabel;
    NEWOCTEdit: TEdit;
    NewOCTOkButton: TButton;
    Button3: TButton;
    RemoveDependencyKey: TTouchSoftkey;
    EditDependencyKey: TTouchSoftkey;
    procedure FormShow(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure AppendOPPKeyClick(Sender: TObject);
    procedure GridDrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure GridSelectCell(Sender: TObject; ACol, ARow: Integer;
      var CanSelect: Boolean);
    procedure CUpKeyClick(Sender: TObject);
    procedure CDownKeyClick(Sender: TObject);
    procedure InsertOPPKeyClick(Sender: TObject);
    procedure DeleteOPPKeyClick(Sender: TObject);
//    procedure LoadOCTKeyClick(Sender: TObject);
    procedure MoveOPPUpKeyClick(Sender: TObject);
    procedure MoveOPPDownKeyClick(Sender: TObject);
//    procedure CopyOCTKeyClick(Sender: TObject);
    procedure CopyCancelButtonClick(Sender: TObject);
    procedure CopyOKButtonClick(Sender: TObject);
    procedure PartEditKeyClick(Sender: TObject);
    procedure SetGridStyle(Mode : TEditModes);
    procedure ChangePartKeyClick(Sender: TObject);
    procedure ADDOCTKeyClick(Sender: TObject);
    procedure DeleteOCTKeyClick(Sender: TObject);
    procedure NewPartOKButtonClick(Sender: TObject);
    procedure NewPartKeyClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure OCTEditKeyClick(Sender: TObject);
    procedure DeleteOCTOKButtonClick(Sender: TObject);
    procedure OKButtonClick(Sender: TObject);
    procedure CheckIntegrityKeyClick(Sender: TObject);
    procedure DeleteUnusedButtonClick(Sender: TObject);
    procedure DeleteEmptyOCTButtonClick(Sender: TObject);
    procedure CopyNewPartKeyClick(Sender: TObject);
    procedure DeletePartKeyClick(Sender: TObject);
    procedure ErrorAckButtonClick(Sender: TObject);
    procedure CopyPartButtonClick(Sender: TObject);
    procedure DelPartOKButtonClick(Sender: TObject);
    procedure RenameOCTKeyClick(Sender: TObject);
    procedure NewOCTOkButtonClick(Sender: TObject);
    procedure RemoveDependencyKeyClick(Sender: TObject);
    procedure EditDependencyKeyClick(Sender: TObject);


  private
    { Private declarations }
    IniFile : TIniFile;
    OPPBase        : String;
    OCTBase        : String;
    SelectedRow    : Integer;
    CurrentPartDir : String;
    FCurrentPart    : String;
    CurrentOPP     : String;
    FCurrentOCT     : String;
    FSelectedOCT    : String;
    VisibleRows    : Integer;
    DeleteDirectory : String;
    EditMode        : TEditModes;
//    CopyMode        : TCopyModes;
    CurrentOCTCount : Integer;
    NewPartOnAck    : Boolean;
    Translator      : TTranslator;

    NoOPPText,
    NoOctsText,
    NoPartsText,
    OPPExistsText,
    DestExistsText,
    PartDirNExistText,
    OCTNExistText ,
    IndexText     ,
    NameText      ,
    ValidText     ,
    DependencyText,
    OKText        ,
    FailedText    ,
    OCTNameText   ,
    PCDText       ,
    PartExistsText ,
    PPOKText       ,
    InvalidPartText,
    OKRefText      ,
    NotExistText   ,
    MultiRefText   ,
    EmptyOCTText   ,
    NoRefText      ,
    DeletingText   ,
    PartGoodText   ,
    PartFailedText ,
    OCTNotFoundText ,
    InvalidPartNameText ,
    PPAckTextandRetry   ,
    CantCopyPartText    ,
    PartCopySuccessText ,
    PPAckToCompText     ,
    CantCopyExistsText  ,
    NotCopiedText       ,
    PleaseConfirmText   ,
    OrCancelText        ,
    NoPartSelectedText  ,
    SuccesDeleteText    ,
    PPAckText           ,
    SelectNewPartText   ,
    PartDeleteFailText  ,
    NoOCTText           ,
    OCTExistsText       ,
    RenameText          ,
    NoneText            :String;
    procedure ReadIniFile;
    procedure InitGridwithPart(PartPath,OctName : String;KeepRow : Boolean);
    procedure ShowAllOCTsInGrid;

    procedure MakeOPPList(OCTFile: TStringList;
                                   var OCtList: TStringList;
                                   var ValidList: TStringList;
                                   var DependencyList : TStringList;
                                   OCTPath : String);
    procedure MakeOCTList (DList : TStringList;Base : String);
    procedure MakeAllOPPList (DList : TStringList;Base : String);

    procedure UpdateOCTCount;
    procedure SelectOPPRow(RowNumber : Integer);
    procedure SelectOCTRow(RowNumber : Integer);
    procedure ConditionPartKeys;
    procedure SetSelectedOCT(OCTName : String);
    procedure SetCurrentOCT(OCTName : String);
    procedure SetCurrentPart(PartName : String);
    function  ListAllReferencedOppsForPart(PartDirectory : String): TIntegrityResults;
    procedure MakeRefListForOPP(OPPName,PartDirectory : String;List : TStringList);
    function  MakeReferneceListForPart(PartDir : String;RefList : TOPPRefList) : Boolean;




    function  AppendOPP(CurrentPartPath,OCTName,OPPName : String):Boolean;



    function  InsertOPP(CurrentPartPath,OCTName,OPPName : String;Position : Integer):Boolean;
    function  EditDependency(CurrentPartPath,OCTName,OPPName : String;Position : Integer):Boolean;


//    function  DirectoriesMatch(Path1,Path2: String):Boolean;
    function  MakeFileTime:String;
//    function  SelectOCTDialog : String;
    function  SelectPartDialog: Boolean;

    procedure SetKeyMode(Mode :TKeyModes);
    procedure SetEditMode(Mode : TEditModes;ResetToRow1 : Boolean);

    function Octextended(NameofFile : String): String;
    function unextended (NameofFile : String): String;
    procedure ForceFirstOCT;
    function CopyandDelete(OPPPath: String): Boolean;
    procedure ShowPartGrid(ResetToRow1 : Boolean);
    procedure ShowOCTGrid(ResetToRow1 : Boolean);
    procedure ShowMessage(L1, L2, L3, L4: String;MessageMode : TMessMode;GetNewPart : Boolean);
    procedure FinalInstall ;override;

  public
    { Public declarations }
  constructor Create(AOwner : TComponent); override;
  procedure Execute; override;

  end;

function IsValidPartName(const Ident: string): Boolean;
function IsValidPartPath(const Ident: string): Boolean;

var
  AOCTForm: TAOCTForm;

implementation

uses CMDialogue;

{$R *.DFM}
//var
//BaseDirectory : String;

procedure TAOCTForm.FormShow(Sender: TObject);
begin
Width := 1022;
Height := 766;
Top := 1;
Left := 1;
DeleteUnusedButton.Visible := False;
DeleteEmptyOCTButton.Visible := False;
MainPages.ActivePage := GridSheet;
end;

procedure TAOCTForm.FormResize(Sender: TObject);
begin
    MainPanel.Height  := MainPanelHeight;
    SoftkeyPanel.Height := SoftPanelHeight;
    MainKB.Height := Height- (MainPanelHeight)-SoftKeyPanel.Height-5 ;
    MainKb.Top := MainPanelHeight+SoftPanelHeight+2;
    MainKb.Width := (Width div 100)*78;
    MainKB.Left := 2;
    KPad.Height := Height- (MainPanelHeight)-SoftKeyPanel.Height-5;
    KPad.Top := MainPanelHeight+SoftPanelHeight+2;
    KPad.Width := Width-MainKb.Width-10;
    KPad.Left := MainKb.Width+2;
    with CupKey do
         begin
         Height := 50;
         Top := 2;
         Width := CursorPanel.Width-4;
         Left := 2;
         end;

    with CDownKey do
         begin
         Height := 50;
         Width := CupKey.Width;
         Top := CupKey.Height+4;
         Left := 2;
         end;

    with OKButton do
         begin
         Height := 50;
         Left := 2;
         Width := CupKey.Width;
         Top := CursorPanel.Height-52;
         end;

   with OCtEditKey do
         begin
         Top := 4;
         Height := SoftKeyPanel.Height-8;
         Left := SoftKeyPanel.Width-104;
         Width := 100;
         end;
   with PartEditKey do
         begin
         Top := 4;
         Height := SoftKeyPanel.Height-8;
         Left := OCtEditKey.Left-104;
         Width := 100;
         end;
   with AppendOPPKey do
         begin
         Top := 0;
         Height := SoftKeyPanel.Height-8;
         Left := 2;
         Width := DefaultKeyWidth;
         end;
   with InsertOPPKey do
         begin
         Top := 0;
         Height := SoftKeyPanel.Height-8;
         Left := AppendOPPKey.Left+DefaultKeyWidth;
         Width := DefaultKeyWidth;
         end;
   with DeleteOPPKey do
         begin
         Top := 0;
         Height := SoftKeyPanel.Height-8;
         Left := InsertOPPKey.Left+DefaultKeyWidth;
         Width := DefaultKeyWidth;
         end;
   with MoveOPPUPKey do
         begin
         Top := 0;
         Height := SoftKeyPanel.Height-8;
         Left := DeleteOPPKey.Left+DefaultKeyWidth;
         Width := DefaultKeyWidth;
         end;
   with MoveOPPDownKey do
         begin
         Top := 0;
         Height := SoftKeyPanel.Height-8;
         Left := MoveOPPUPKey.Left+DefaultKeyWidth;
         Width := DefaultKeyWidth;
         end;

   with EditDependencyKey do
         begin
         Top := 0;
         Height := SoftKeyPanel.Height-8;
         Left := MoveOPPDownKey.Left+DefaultKeyWidth+30;
         Width := DefaultKeyWidth;
         end;

   with RemoveDependencyKey do
         begin
         Top := 0;
         Height := SoftKeyPanel.Height-8;
         Left := EditDependencyKey.Left+DefaultKeyWidth;
         Width := DefaultKeyWidth;
         end;


   with OCTEditKey do
         begin
         Top := 4;
         Height := SoftKeyPanel.Height-8;
         Left := SoftKeyPanel.Width-104;
         Width := 100;
         end;

   with PartEditKey do
         begin
         Top := 4;
         Height := SoftKeyPanel.Height-8;
         Left := OCtEditKey.Left-104;
         Width := 100;
         end;


   with ChangePartKey do
         begin
         Top := 0;
         Height := SoftKeyPanel.Height-8;
         Left := 2;
         Width := DefaultKeyWidth;
         end;

   with NewPartKey do
         begin
         Top := 0;
         Height := SoftKeyPanel.Height-8;
         Left := ChangePartKey.Left+DefaultKeyWidth;
         Width := DefaultKeyWidth;
         end;


   with CopyNewPartKey do
         begin
         Top := 0;
         Height := SoftKeyPanel.Height-8;
         Left := NewPartKey.Left+DefaultKeyWidth;
         Width := DefaultKeyWidth;
         end;

   with DeletePartKey do
         begin
         Top := 0;
         Height := SoftKeyPanel.Height-8;
         Left := CopyNewPartKey.Left+DefaultKeyWidth;
         Width := DefaultKeyWidth;
         end;

   with ADDOCTKey do
         begin
         Top := 0;
         Height := SoftKeyPanel.Height-8;
         Left := DeletePartKey.Left+DefaultKeyWidth;
         Width := DefaultKeyWidth;
         end;

   with RenameOCTKey do
         begin
         Top := 0;
         Height := SoftKeyPanel.Height-8;
         Left := ADDOCTKey.Left+DefaultKeyWidth;
         Width := DefaultKeyWidth;
         end;

   with DeleteOCTKey do
         begin
         Top := 0;
         Height := SoftKeyPanel.Height-8;
         Left := RenameOCTKey.Left+DefaultKeyWidth;
         Width := DefaultKeyWidth;
         end;

   with CheckIntegrityKey do
         begin
         Top := 0;
         Height := SoftKeyPanel.Height-8;
         Left := DeleteOCTKey.Left+DefaultKeyWidth;
         Width := DefaultKeyWidth;
         end;

VisibleRows := Grid.Height div Grid.DefaultRowHeight;

end;

procedure TAOCTForm.ReadIniFile;
var
AppBase : String;
begin
AppBase := ExtractFilePath(Application.ExeName);
IniFile := TIniFile.Create(AppBase+'Live\OCTEdit.CFG');
if assigned(IniFile) then
  begin
try
  AppBase := AppBase + '..\data\';
  OPPBase := IniFile.ReadString('GENERAL','OppBaseDir','c:\HSD6\Data\programs\');
  if OPPBase[1] = '\' then OppBase := AppBase+Copy(OPPBase,2,100);
  OCTBase := IniFile.ReadString('GENERAL','OCTDirBase','c:\HSD6\Data\programs\Part\');
  if OCTBase[1] = '\' then OCTBase := AppBase+Copy(OCTBase,2,100);
  DeleteDirectory := IniFile.ReadString('GENERAL','DeleteDir','c:\HSD6\Data\DeletedPrograms\');
  if DeleteDirectory[1] = '\' then DeleteDirectory := AppBase+Copy(DeleteDirectory,2,100);
  FileSelector.TFP1a.DelDirectory := DeleteDirectory;
finally
IniFile.Free;
end;
end;
end;


procedure TAOCTForm.FormCreate(Sender: TObject);
begin
ReadIniFile;
MainPages.ActivePage := GridSheet;
//DeleteDirectory := SlashSep(OCTBase,'Deleted\');
if not DirectoryExists(OPPBase) then
   begin
   ForceDirectories(OPPBase);
   end;
if not DirectoryExists(OCTBase) then
   begin
   ForceDirectories(OCTBase);
   end;
if not DirectoryExists(DeleteDirectory) then
   begin
   ForceDirectories(DeleteDirectory);
   end;
//EditModeKey.Legend := 'Edit~Part';
SetKeyMode(kmOCT);
SetGridStyle(emOCT);
end;

procedure TAOCTForm.MakeOPPList(OCTFile: TStringList;
                                var OCtList: TStringList;
                                var ValidList: TStringList;
                                var DependencyList : TStringList;
                                OCTPath : String);
var
OPPCount : Integer;
PointerPos : Integer;
OppName    : String;
FilePath   : String;
DependString : String;
begin
ValidList.Clear;
OCTList.Clear;
DependencyList.Clear;
For OPPCount := 0 to OCTFile.Count-1 do
    begin
    PointerPOs := Pos('->',OctFile[OppCount]);
    if PointerPos > 0 then
       begin
       DependString := StripQuotes(Trim(Copy(OctFile[OppCount],POinterPos+2,100)));
       if DependString = '*' then
          begin
          DependencyList.Add(NoneText);
          end
       else
           begin
           DependencyList.Add(StripQuotes(DependString));
           end;
       OppName := StripQuotes(Copy(OctFile[OppCount],1,PointerPos-1));
       OCTList.Add(OPPName);
       FilePath := SlashSep(OCTPath,OppName);
       if FileExists(FilePath) then
          begin
          ValidList.Add('OK');
          end
       else
          begin
          ValidList.Add('Missing');
          end;
       end;
    end;
end;

procedure TAOCTForm.SetGridStyle(Mode : TEditModes);
begin
case Mode of
     emSelectPart,emPart:
       begin
       with Grid do
         begin
         ColCount := 1;
         ColWidths[0] := self.Width-10;
         DefaultRowHeight := 30;
         Cells[0,0] := 'Name';
         OCTLabel.Visible := False;
         OCTNameDisplay.Visible := False;
         end;
       end;
     emOCT:
       begin
       with Grid do
         begin
         ColCount := 4;
         ColWidths[0] := 50;
         ColWidths[1] := 70;
         ColWidths[2] := (Grid.Width-130) div 2;
         ColWidths[3] := (Grid.Width-130) div 2;
         DefaultRowHeight := 30;
         Cells[0,0] := IndexText;
         Cells[1,0] := ValidText;
         Cells[2,0] := NameText;
         Cells[3,0] := DependencyText;

         OCTLabel.Visible := True;
         OCTNameDisplay.Visible := True;
         end;
       end;
end; // case

end;


function  TAOCTForm.SelectPartDialog: Boolean;
var
FormResult : TModalResult;
begin
MainPages.ActivePage := GridSheet;

Result := False;
with FileSelector do
     begin
     aBaseDir := OCTBase;
     SelectMode := smPart;
     MustSelectPart := False;
     MustSelectOCT  := False;
     SelectOctifExists := False;
     Left := 200;
     Top := 100;
     end;
FileSelector.TestForParts;
if FileSelector.PartCount >0 then
   begin
   FormResult := FileSelector.ShowModal;
   if FormResult = mrOK then
      begin
{      if FileSelector.NewPartMode then
        begin
        SetEditMode(emNewPart,True);
        Result := True;
        exit;
        end;}
      SetCurrentPart(FileSelector.SelectedPart);
      CurrentPartDir := FileSelector.SelectedPartPath;
      if FileSelector.NoOcts then
         begin
         SetCurrentOct('');
         OctNameDisplay.Text := NoOctsText;
         end
      else
          begin
          SetCurrentOct(octextended(FileSelector.SelectedOCT));
          OctNameDisplay.Text := Unextended(FCurrentOct);
          end;
      InitGridwithPart(CurrentPartDir,'',True);
      SetEditMode(empart,True);
      Result := True;
      end;
   end
else
    begin
    CurrentPartDir := OCTBase;
    SetEditMode(empart,True);
    Result := True;
    end;

end;



procedure TAOCTForm.InitGridwithPart(PartPath,OctName : String;KeepRow : Boolean);
var
OCTFile : TStringList;
OCTList : TStringList;
ValidList : TStringList;
DependList : TStringList;
OCtPath : String;
OPPCount  : Integer;
EntryRow  : Integer;
begin
EntryRow := 0;
if not DirectoryExists(PartPath) and (PartPath <> '') then
   begin
   ShowMessage(PartDirNExistText,'','','',mmError,False);
   exit;
   end;

if OCTName <> '' then
   begin
   if KeepRow then  EntryRow := SelectedRow;
   OCtPath :=  SlashSep(PartPath,octextended(OctName));

   if not FileExists(OCtPath) then
       begin
       ShowMessage(OCTNotFoundText,'','','',mmError,False);
       exit;
       end;

   OCTFile := TStringList.Create;
   OCTList := TStringList.Create;
   ValidList := TStringList.Create;
   DependList := TStringList.Create;
   OCTFile.LoadFromFile(OCtPath);
   MakeOPPList(OCTFile,OCtList,ValidList,DependList,PartPath);
   try
   with Grid do
        begin
        RowCount := OCTList.Count+1;
        For OPPCount := 0 to OCTList.Count-1 do
            begin
            Cells[0,OPPCount+1] := IntToStr(OPPCount+1);
            Cells[1,OPPCount+1] := ValidList[OPPCount];
            Cells[2,OPPCount+1] := OCTList[OPPCount];
            Cells[3,OPPCount+1] := DependList[OPPCount];
            end;
        end;
   If OCtList.Count > 0 then
      begin
      SelectOPPRow(1);
      end
   else
      begin
      SelectOPPRow(0);
      Grid.RowCount := 2;
      Grid.Cells[0,1] := '';
      Grid.Cells[1,1] := '';
      Grid.Cells[2,1] := NoOPPText;
      end;
   finally
   OCTFile.Free;
   OctList.Free;
   ValidList.Free;
   DependList.Free;
   if KeepRow then SelectOPPRow(EntryRow);
   end;
   end
else
    begin
    // Here if no OCTs
    Grid.RowCount := 1;
    end;


end;


procedure TAOCTForm.AppendOPPKeyClick(Sender: TObject);
begin
with FileSelector do
     begin
     Filter := '*.opp';
     aBaseDir := CurrentPartDir;
     SelectMode := smOPP;
     MustSelectPart := False;
     MustSelectOCT  := False;
     end;

if FileSelector.ShowModal = mrOK then
   begin
   if AppendOPP(CurrentPartDir,FCurrentOCT,FileSelector.SelectedFile) then
      begin
      InitGridwithPart(CurrentPartDir,FCurrentOCT,True);
      SelectOPPRow(SelectedRow);
      end
   end

end;

function TAOCTForm.AppendOPP(CurrentPartPath,OCTName,OPPName : String):Boolean;
var
OPPCount : Integer;
Exists   : Boolean;
DestFileName : String;
QuotedName : String;
OPPIndex : Integer;
SourcePath : String;
DestFile   : String;
Success    : Boolean;
PSource    : array[0..300] of Char;
PDest      : array[0..300] of Char;
OCTFile : TStringList;
begin
Exists := False;
DestFileName := RemoveDeleteDate(ExtractFilename(OPPName));
DestFile :=  SlashSep(CurrentPartDir,DestFilename);
QuotedName := WithQuotes(DestFileName);
SourcePath := ExtractFilePath(OPPName);
OPPCount := 1;
if (Grid.RowCount =2) and (Grid.Cells[2,1] = NoOPPText) then
   begin
   Grid.Cells[0,1]  := '1';
   Grid.Cells[1,1] := 'OK';
   Grid.Cells[2,1] := DestFilename;
   Result := True;
   end
else
    begin
    while (OPPCount<Grid.RowCount) and not(Exists) do
          begin
          Exists := (DestFileName = Grid.Cells[2,OPPCount]);
          OppCount := OppCount+1;
          end;
    if Not Exists then
       begin
       Grid.RowCount := Grid.RowCount+1;
       OPPIndex :=  Grid.RowCount-1;
       Grid.Cells[0,OPPIndex] := IntToStr(OPPIndex);
       Grid.Cells[1,OPPIndex] := OKText;
       Grid.Cells[2,OPPIndex] := DestFilename;
       Result := True;
       end
    else
        begin
        Result := False;
        ShowMessage(OPPExistsText,'','','',mmError,False);
        exit;
        end;
end;
if not DirectoriesMatch(SourcePath,CurrentPartPath) then
   begin
   // Copy Source OPP into current Part Directory
   StrPCopy(PDest,DestFile);
   StrPCopy(PSource,OPPName);
   Success := CopyFile(PSource,PDest,True);
   if Success then
      begin
      OCTFile := TStringList.Create;
      try
      OCtFile.LoadFromFile(SlashSep(CurrentPartPath,octextended(OCTName)));
      OCTFile.Add(QuotedName+'-> *');
      OctFile.SaveToFile(SlashSep(CurrentPartPath,octextended(OCTName)));
      finally
      OCTFile.Free;
      end;
      end
   else
     begin
     Result := False;
     ShowMessage(OPPExistsText,'','','',mmError,False);
     exit;
     end;
   end
else
    begin
    // Here if OPP is in this part directory already
    OCTFile := TStringList.Create;
    try
    OCtFile.LoadFromFile(SlashSep(CurrentPartPath,octextended(OCTName)));
    OCTFile.Add(QuotedName+'-> *');
    OctFile.SaveToFile(SlashSep(CurrentPartPath,octextended(OCTName)));
    finally
    OCTFile.Free;
    end;

   end;
end;


function TAOCTForm.InsertOPP(CurrentPartPath,OCTName,OPPName : String;Position : Integer):Boolean;
var
OPPCount : Integer;
Exists   : Boolean;
DestFileName : String;
QuotedName : String;
SourcePath : String;
DestFile   : String;
Success    : Boolean;
PSource    : array[0..300] of Char;
PDest      : array[0..300] of Char;
OCTFile : TStringList;
GPos    : Integer;
NoOPPs  : Boolean;
begin

Exists := False;
DestFileName := RemoveDeleteDate(ExtractFilename(OPPName));
DestFile :=  SlashSep(CurrentPartDir,DestFilename);
QuotedName := WithQuotes(DestFilename);
SourcePath := ExtractFilePath(OPPName);
OPPCount := 1;
if Position <1 then Position := 1;
NoOPPs  := False;

OCtFile := TStringList.create;
try
OCtFile.LoadFromFile(SlashSep(CurrentPartPath,octextended(OCTName)));
if OCtFile.Count = 0 then
   begin
   NoOpps := True;
   OppCount := 0;
   Exists := False;
   end;
finally
OCtFile.Free;
end;
if not NoOpps then
   begin
   while (OPPCount<Grid.RowCount) and not(Exists) do
         begin
         Exists := (DestFileName = Grid.Cells[2,OPPCount]);
         OppCount := OppCount+1;
         end;
   if Not Exists then
      begin
      Grid.RowCount := Grid.RowCount+1;
      For GPos := Grid.RowCount-1 downto (Position-1) do
          begin
          Grid.Cells[0,GPos+1] := IntToStr(GPos+1);
          Grid.Cells[1,GPos+1] := Grid.Cells[1,GPos];
          Grid.Cells[2,GPos+1] := Grid.Cells[2,GPos];
          end;
      end
   else
       begin
       Result := False;
       ShowMessage(OPPExistsText,'','','',mmError,False);
       exit;
       end;
   end;
Grid.Cells[0,Position] := IntToStr(Position);
Grid.Cells[1,Position] := OKText;
Grid.Cells[2,Position] := DestFileName;
Grid.Invalidate;
Result := True;
if not DirectoriesMatch(SourcePath,CurrentPartPath) then
   begin
   // Copy Source OPP into current Part Directory
   StrPCopy(PDest,DestFile);
   StrPCopy(PSource,OPPName);
   Success := CopyFile(PSource,PDest,True);
   if Success then
      begin
      OCTFile := TStringList.Create;
      try
      OCtFile.LoadFromFile(SlashSep(CurrentPartPath,octextended(OCTName)));
      if Position <1 then Position := 1;
      if OCTFile.Count > 0 then
         begin
         OCTFile.Insert(Position-1,QuotedName+'-> *');
         end
      else
          begin
          OCTFile.Add(QuotedName+'-> *');
          end;
      OctFile.SaveToFile(SlashSep(CurrentPartPath,octextended(OCTName)));

      finally
      OCTFile.Free;
      end
      end
   else
      begin
      Result := False;
      ShowMessage(OPPExistsText,'','','',mmError,False);
      exit;
      end;

   end
else
    begin
    // Here if OPP is in this part directory already
    OCTFile := TStringList.Create;
    try
    OCtFile.LoadFromFile(SlashSep(CurrentPartPath,octextended(OCTName)));
    OCTFile.Insert(Position-1,QuotedName+'-> *');
    OctFile.SaveToFile(SlashSep(CurrentPartPath,octextended(OCTName)));

    finally
    OCTFile.Free;
    end;

   end;

end;


procedure TAOCTForm.GridDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
var
Contents : String;
begin
Contents := Grid.Cells[ACol,Arow];

with Grid.Canvas do
     begin
     if ARow = 0 then
        begin
        Brush.Color := clSilver;
        Font.Color := clBlack;
        Font.Name := 'Verdana';
        Font.Size := 12;
        Font.Style := [fsBold];
        end
     else
         begin
         if ARow = SelectedRow then
            begin
            Brush.Color := clYellow;
            Font.Color := clMaroon;
            end
         else
             begin
             Brush.Color := clWhite;
             Font.Color := clBlack;
             end;
         end;
     FillRect(Rect);
     if ACol = 2 then
        begin
        TextOut(Rect.Left+30,Rect.Top+4,Contents);
        end
     else
        begin
        TextOut(Rect.Left+3,Rect.Top+4,Contents);
        end;
end;
end;

procedure TAOCTForm.GridSelectCell(Sender: TObject; ACol, ARow: Integer;
  var CanSelect: Boolean);
begin
case EditMode of
     emOCT:
     begin
     If ARow > 0 then
        begin
        SelectOPPRow(ARow);
        end;
     end;

     emPart:
     begin
     If ARow > 0 then
        begin
        SelectOCTRow(ARow);
        end;
     end;
     end; // case

end;

procedure TAOCTForm.ConditionPartKeys;
begin
if (EditMode = emPart) then
   begin
   OctEditKey.isEnabled := Grid.RowCount > 1;
   end
else
   OctEditKey.isEnabled := True;

end;

procedure TAOCTForm.SelectOCTRow(RowNumber : Integer);
begin
if Rownumber < 1 then exit;
if CurrentOCTCount < 1 then exit;
ConditionPartKeys;
SelectedRow := RowNumber;
if (Grid.TopRow+VisibleRows-2 <= SelectedRow) and (VisibleRows > 0) then
   begin
   Grid.TopRow := SelectedRow-VisibleRows+3;
   end
else
    begin
    if SelectedRow < Grid.TopRow then
       begin
       Grid.TopRow := SelectedRow;
       end;
    end;
Grid.Invalidate;

SetSelectedOCT(Grid.Cells[1,RowNumber]);

end;

procedure TAOCTForm.SelectOPPRow(RowNumber : Integer);
begin
SelectedRow := RowNumber;

DeleteOPPKey.IsEnabled := Grid.RowCount > 1;
MoveOPPDownKey.IsEnabled := Grid.RowCount > 1;
ConditionPartKeys;

if Grid.RowCount < 1 then
   begin
   MoveOPPUPKey.IsEnabled := False;
   end
else
   begin
   MoveOPPUPKey.IsEnabled := True;
   if (SelectedRow <= 1) then
      begin
      MoveOPPUpKey.IsEnabled := False;
      end
   else
       begin
       MoveOPPUpKey.IsEnabled := True;
       end;

   if (SelectedRow = Grid.RowCount-1) or (Grid.RowCount <=1)then
      begin
      MoveOPPDownKey.IsEnabled := False;
      end
   else
       begin
       MoveOPPDownKey.IsEnabled := True;
       end;
   end;


if (Grid.TopRow+VisibleRows-2 <= SelectedRow) and (VisibleRows > 0) then
   begin
   Grid.TopRow := SelectedRow-VisibleRows+3;
   end
else
    begin
    if SelectedRow < Grid.TopRow then
       begin
       Grid.TopRow := SelectedRow;
       end;
    end;
Grid.Invalidate;
CurrentOPP := Grid.Cells[2,RowNumber];
end;
procedure TAOCTForm.CUpKeyClick(Sender: TObject);
begin

case EditMode of
     emOCT:
     begin
     If SelectedRow > 1 then
        begin
        SelectOPPRow(SelectedRow-1);
        end;
     end;

     emPart:
     begin
     If SelectedRow > 1 then
        begin
        SelectOCTRow(SelectedRow-1);
        end;
     end;
     end; // case
end;

procedure TAOCTForm.CDownKeyClick(Sender: TObject);
begin

case EditMode of
     emOCT:
     begin
     // here if opps are in the grid
     If SelectedRow < Grid.Rowcount-1 then
        begin
        SelectOPPRow(SelectedRow+1);
        end;
     end;

     emPart:
     begin
     If SelectedRow < Grid.Rowcount-1 then
        begin
        SelectOCTRow(SelectedRow+1);
        end;
     end;
     end; // case

end;

procedure TAOCTForm.InsertOPPKeyClick(Sender: TObject);
begin
with FileSelector do
     begin
     Filter := '*.opp';
     aBaseDir := CurrentPartDir;
     SelectMode := smOPP;
     MustSelectPart := False;
     MustSelectOCT  := False;
     end;

if FileSelector.ShowModal = mrOK then
   begin
   if SelectedRow = 0 then SelectedRow := 1;

   if InsertOPP(CurrentPartDir,FCurrentOCT,FileSelector.SelectedFile,SelectedRow) then
      begin
      InitGridwithPart(CurrentPartDir,FCurrentOCT,True);
      SelectOPPRow(SelectedRow);
      end
   end

end;

function TAOCTForm.MakeFileTime:String;
var
Hour,Min,Sec,mSec  : Word;
Year,Month,Day     : Word;
YStr,MStr,DStr     : String;
HStr,MinStr,SecStr : String;
begin
DecodeDate(Now,Year,Month,Day);
if Year < 10 then YStr := Format('0%d-',[Year]) else YStr := Format('%d-',[Year]);
if Month  < 10 then MStr := Format('0%d-',[Month]) else MStr := Format('%d-',[Month]);
if Day  < 10 then DStr := Format('0%d-',[Day]) else DStr := Format('%d-',[Day]);

DecodeTime(Now,Hour,Min,Sec,mSec);
if Hour < 10 then HStr := Format('0%d-',[Hour]) else HStr := Format('%d-',[Hour]);
if Min  < 10 then MinStr := Format('0%d-',[Min]) else MinStr := Format('%d-',[Min]);
if Sec  < 10 then SEcStr := Format('0%d',[SEc]) else SEcStr := Format('%d',[SEc]);
Result :=YStr+MStr+DStr+HStr+MinStr+SecStr;
end;

function TAOCTForm.CopyandDelete(OPPPath : String): Boolean;
var
PSource    : array[0..300] of Char;
PDest      : array[0..300] of Char;
DeleteName : String;
DestString : String;
begin
Result := False;
if not FileExists(OPPPath) then exit;
if Not DirectoryExists(DeleteDirectory) then exit;
DeleteName := ExtractFilename(OppPath);
DestString :=  SlashSep(DeleteDirectory,DeleteName)+'-'+MakeFileTime;
StrPCopy(PDest,DestString);
StrPCopy(PSource,OPPPath);
if MoveFile(PSource,PDest) then Result := True;
end;

procedure TAOCTForm.DeleteOPPKeyClick(Sender: TObject);
var
OCTFile    : TStringList;
OCtPath    : String;
DeletePath : String;
GPos       : Integer;
EntryRow   : Integer;
RefList    : TOppRefList;
begin
EntryRow := SelectedRow;
DeletePath := SlashSep(CurrentPartDir,CurrentOPP);
// NB only delete opp file if it is not referenced by other OCts
RefList    := TOppRefList.Create;
try
if MakeReferneceListForPart(CurrentPartDir,Reflist) then
   begin
   if RefList.Occurences(CurrentOPP) < 2 then
      begin
      CopyandDelete(DeletePath);
      end;
   end;
finally
Reflist.Destoyit;
end;


OCTFile    := TStringList.create;
try
OCtPath :=  SlashSep(CurrentPartDir,octextended(FCurrentOCT));
OCtFile.LoadFromFile(OCtPath);
if OctFile.Count > 0 then
   begin
   Octfile.Delete(SelectedRow-1);
   end;
OCtFile.SaveToFile(OCtPath);
For GPos := SelectedRow+1 to Grid.RowCount-1 do
    begin
    Grid.Cells[1,GPos-1] := Grid.Cells[1,GPos];
    Grid.Cells[2,GPos-1] := Grid.Cells[2,GPos];
    end;
Grid.RowCount := Grid.RowCount-1;
InitGridwithPart(CurrentPartDir,FCurrentOCT,True);
if Grid.RowCount > 1 then
   begin
   if EntryRow < Grid.RowCount then
      begin
      SelectOPPRow(EntryRow);
      end
   else
      begin
      SelectOPPRow(Grid.RowCount-1);
      end
   end
else
   begin
   SelectOPPRow(0);
   end;
finally
OCTFile.Free;
end;


end;


procedure TAOCTForm.MoveOPPUpKeyClick(Sender: TObject);
var
CurrentOppName : String;
OCTFile     : TStringList;
Buffer      : String;
EntryRow    : Integer;
begin
if SelectedRow < 2 then exit;
EntryRow := SelectedRow;
CurrentOppName := Grid.Cells[2,SelectedRow];
OCTFile := TStringList.Create;
 try
 OCtFile.LoadFromFile(SlashSep(CurrentPartDir,octextended(FCurrentOCT)));
 if OCTFile.Count > SelectedRow-1 then
    begin
    Buffer := OCTFile[SelectedRow-1];
    OctFile.Delete(SelectedRow-1);
    OCtFile.Insert(SelectedRow-2,Buffer);
    OCTFile.SaveToFile(SlashSep(CurrentPartDir,octextended(FCurrentOCT)));
    end;
 finally
 OCTFile.Free;
 end;
InitGridwithPart(CurrentPartDir,FCurrentOCT,False);
SelectOPPRow(EntryRow-1);

end;

procedure TAOCTForm.MoveOPPDownKeyClick(Sender: TObject);
var
CurrentOppName : String;
NextOPP : String;
OCTFile     : TStringList;
Buffer      : String;
EntryRow    : Integer;
begin
if SelectedRow > Grid.Rowcount-1 then exit;
EntryRow := SelectedRow;
CurrentOppName := Grid.Cells[2,SelectedRow];
OCTFile := TStringList.Create;
 try
 OCtFile.LoadFromFile(SlashSep(CurrentPartDir,octextended(FCurrentOCT)));
 if OCTFile.Count > SelectedRow-1 then
    begin
    Buffer := OCTFile[SelectedRow-1];
    OctFile.Delete(SelectedRow-1);
    OCtFile.Insert(SelectedRow,Buffer);
    OCTFile.SaveToFile(SlashSep(CurrentPartDir,octextended(FCurrentOCT)));
    end;
 finally
 OCTFile.Free;
 end;
InitGridwithPart(CurrentPartDir,FCurrentOCT,False);
SelectOPPRow(EntryRow+1);
end;

procedure TAOCTForm.Button1Click(Sender: TObject);
begin
NewPartKeyClick(Self);
end;


procedure TAOCTForm.CopyCancelButtonClick(Sender: TObject);
begin
Grid.SetFocus;
MainKb.FocusControl := Grid;
KPad.FocusControl := Grid;
MainDialoguePanel.Visible := False;
end;


procedure TAOCTForm.NewPartOKButtonClick(Sender: TObject);
var
NewPartFullPath : String;
begin
if NewPartInput.Text <> '' then
   begin
   if isValidPartName(NewPartInput.Text) then
      begin
      NewPartFullPath := SlashSep(OCTBase,NewPartInput.Text);
      try
      if DirectoryExists(NewPartFullPath) then
         begin
         PartText1.Caption := PartExistsText;
         PartText2.Caption := PPOKText;
         DialogPages.ActivePage := PartErrorPage;
         end
      else
          begin
          //here to create the new directory
          if ForceDirectories(NewPartFullPath) then
             begin
             // Now make this the current part and close the dialog
             SetCurrentPart(NewPartInput.Text);
             CurrentPartDir := NewPartFullPath;
             SetCurrentOct('');
             OctNameDisplay.Text := NoOctsText;
             SetEditMode(emPart,True);
             end
          else
             begin
             end;
          end;
      except
      PartText1.Caption := InvalidPartText;
      PartText2.Caption := PPOKText;
      DialogPages.ActivePage := PartErrorPage;
      end;// try except
      end
   else
       begin
       PartText1.Caption := InvalidPartText;
       PartText2.Caption := PPOKText;
       DialogPages.ActivePage := PartErrorPage;
       end;
   end;
CopyCancelButtonClick(Self);
end;


procedure TAOCTForm.CopyOKButtonClick(Sender: TObject);
var
NewOCTName : String;
NewOCTFullPath : String;
SelectedOCTFullpath : String;
OCTFile       : TStringList;
ErrorDisplayed : Boolean;

begin
ErrorDisplayed := False;
try
if ReNameEdit.Text <> '' then
   begin
   SelectedOCTFullpath := SlashSep(CurrentPartDir,FSelectedOct);
   if not FileExists(SelectedOCTFullpath) then
       begin
       ShowMessage(OCTNotFoundText,PPAckTextandRetry,'','',mmError,False);
       ErrorDisplayed := True;
       end;
   NewOCTName := octextended(ReNameEdit.Text);
   NewOCTFullPath := SlashSep(CurrentPartDir,NewOCTName);
   if not FileExists(NewOCTFullPath) then
      begin
      RenameFile(SelectedOCTFullpath,NewOCTFullPath);
      end
   else
       begin
       ShowMessage(OCTExistsText,PPAckTextandRetry,'','',mmError,False);
       ErrorDisplayed := True;
       end;
   end;
// Now Update Grid to show new part if in OCT
finally
if not ErrorDisplayed then
   begin
   ShowPartGrid(True);
   CopyCancelButtonClick(Self);
   end;
end;
end;


procedure TAOCTForm.SetEditMode(Mode : TEditModes;ResetToRow1 : Boolean);
begin
EditMode := Mode;
case Mode of
     emOCT:
     begin
     OCtEditKey.ButtonColor := clYellow;
     OCtEditKey.IndicatorOn := True;
     PartEditKey.ButtonColor := clwhite;
     PartEditKey.IndicatorOn := False;
     SetKeyMode(kmOCT);
     SetGridStyle(emOCT);
     InitGridwithPart(CurrentPartDir,FCurrentOCT,True);
     UpdateOCTCount;
     SelectOPPRow(1);
     end;

     emPart:
     begin
     OCtEditKey.ButtonColor := clWhite;
     OCtEditKey.IndicatorOn := False;
     PartEditKey.ButtonColor := clYellow;
     PartEditKey.IndicatorOn := True;
     SetKeyMode(kmPart);
     ShowAllOCTsinGrid;
     UpdateOCTCount;
     if ResetToRow1 then SelectOCTRow(1);
     end;


end;


end;

procedure TAOCTForm.SetKeyMode(Mode :TKeyModes);
begin
case Mode of
     kmOCT:
     begin
     PartKeyPanel.visible := False;
     with OPPKeyPanel do
          begin
          Parent := SoftKeyPanel;
          Visible := True;
          Align := alLeft;
          end;

     end;

     kmPart:
     begin
     OPPKeyPanel.visible := False;
     with PartKeyPanel do
          begin
          Parent := SoftKeyPanel;
          Align := alLeft;
          Visible := True;
          end;
     end;
end;

end;


procedure TAOCTForm.OCTEditKeyClick(Sender: TObject);
begin
ShowOCTGrid(True);
if FSelectedOCT = '' then
   begin
   ShowMessage(NoOCTText,PPAckToCompText,'','',mmError,False);
   end;
If not FileExists(SlashSep(CurrentPartDir,FSelectedOCT)) then exit;
if EditMode <> emOct then
   begin
   SetCurrentOCT(FSelectedOCT);
   OctNameDisplay.Text := FCurrentOct;
   SetEditMode(emOct,True);
   end;
end;

procedure TAOCTForm.ShowOCTGrid;
begin
DeleteUnusedButton.Visible := False;
DeleteEmptyOCTButton.Visible := False;
MainPages.ActivePage := GridSheet;
end;


procedure TAOCTForm.ShowPartGrid(ResetToRow1 : Boolean);
begin
DeleteUnusedButton.Visible := False;
DeleteEmptyOCTButton.Visible := False;
MainPages.ActivePage := GridSheet;
SetEditMode(emPart,ResetToRow1);
end;

procedure TAOCTForm.PartEditKeyClick(Sender: TObject);
begin
ShowPartGrid(True);
end;

procedure TAOCTForm.ShowAllOCTsInGrid;
var
OCTList : TStringList;
OCTCount  : Integer;
begin
OCTList :=  TStringList.create;
try
MakeOCTList(OCTList,CurrentPartDir);
with Grid do
     begin
     RowCount := OCTList.Count+1;
     ColCount := 2;
     Cells[0,0] := IndexText;
     Cells[1,0] := OCTNameText;
     Colwidths[0] := 70;
     Colwidths[1] := Grid.Width-80;
     For OCTCount := 0 to OCTList.Count-1 do
         begin
         Cells[0,OCTCount+1] := IntToStr(OCTCount+1);
         Cells[1,OCTCount+1] := OCTList[OctCount];
         end;
     if  OCTList.Count > 0 then SelectedRow := 1 else SelectedRow := 0;
     end;

finally
OCTList.Free;
end;
end;



// ensures .oct is atb end of oct name
function TAOCTForm.octextended(NameofFile : String): String;
var
ext    : String;
begin
Result := NameofFile;
ext := ExtractFileExt( NameofFile );
if ext='' then Result  := NameofFile+'.oct';
end;

function TAOCTForm.unextended (NameofFile : String): String;
var
DotPOs : Integer;
begin
Result := NameofFile;
DotPos := Pos('.',NameofFile);
if DotPos <> 0 then
Result := Copy(NameofFile,0,Dotpos-1);
end;

procedure TAOCTForm.ChangePartKeyClick(Sender: TObject);
begin
ShowPartGrid(True);
SelectPartDialog;
end;

procedure TAOCTForm.ADDOCTKeyClick(Sender: TObject);
begin
ShowPartGrid(True);
NEWOCTEdit.Text := '';
MainDialoguePanel.Visible := True;
DialogPages.ActivePage := NewOCTDialog;
MainKb.FocusControl := NEWOCTEdit;
KPad.FocusControl := NEWOCTEdit;
NEWOCTEdit.SetFocus;
//CopyMode := cmMakeNew;
end;

procedure TAOCTForm.DeleteOCTKeyClick(Sender: TObject);
var
Buffer : String;
begin
//ShowPartGrid(False);
if FSelectedOCT = '' then exit;
if CurrentOCTCount < 1 then exit;
Buffer := PCDText+FSelectedOCT;
DeleteTextLabel.Caption := Buffer;
MainDialoguePanel.Visible := True;
DialogPages.ActivePage := DeleteOCTDialog;
end;

procedure TAOCTForm.UpdateOCTCount;
var
  sr         : TSearchRec;
  FileAttrs  : Integer;
//  OCTCount   : Integer;
begin
 if CurrentPartDir = '' then
    begin
    CurrentOCTCount := 0;
    OCTCountDisplay.Text := ''+IntToStr(CurrentOCTCount);
    PartNameDisplay.Text := NoPartsText;
    OctNameDisplay.Text := NoPartsText;
    FSelectedOCT := '';
    exit;
    end;
 FileAttrs := faAnyFile;
 CurrentOCTCount := 0;
 if FindFirst(SlashSep(CurrentPartDir,'*.oct'), FileAttrs, sr) = 0 then
    begin
    inc(CurrentOCTCount);
    while FindNext(sr) = 0 do
        begin
        if (sr.Attr and FileAttrs) = sr.Attr then
           begin
           inc(CurrentOCTCount);
           end;
        end;
    end;
  OCTCountDisplay.Text := ''+IntToStr(CurrentOCTCount);
  if CurrentOCTCount < 1 then
     begin
     FselectedOCT := '';
     OCTNameDisplay.Text := '';
     end;
end;

procedure TAOCTForm.MakeOCTList (DList : TStringList;Base : String);
var
  sr: TSearchRec;
  FileAttrs: Integer;
  FindString : String;
begin
 DList.Clear;
 FileAttrs := faAnyFile;
 FindString := SlashSep(Base,'*.oct');
 if FindFirst(FindString, FileAttrs, sr) = 0 then
    begin
    Dlist.Add(sr.name);
    while FindNext(sr) = 0 do
        begin
        if (sr.Attr and FileAttrs) = sr.Attr then
           begin
           Dlist.Add(sr.name);
           end;
        end;
  end;
end;

procedure TAOCTForm.MakeAllOPPList (DList : TStringList;Base : String);
var
  sr: TSearchRec;
  FileAttrs: Integer;
  FindString : String;

begin
 DList.Clear;
 FindString := SlashSep(Base,'*.opp');
 FileAttrs := faAnyFile;
 if FindFirst(FindString, FileAttrs, sr) = 0 then
    begin
    Dlist.Add(sr.name);
    while FindNext(sr) = 0 do
        begin
        if (sr.Attr and FileAttrs) = sr.Attr then
           begin
           Dlist.Add(sr.name);
           end;
        end;
  end;
end;




procedure TAOCTForm.NewPartKeyClick(Sender: TObject);
begin
ShowPartGrid(True);
NewPartInput.Text := '';
MainDialoguePanel.Visible := True;
DialogPages.ActivePage := NewPartDialog;
MainKb.FocusControl := NewPartInput;
KPad.FocusControl := NewPartInput;
NewPartInput.SetFocus;
//CopyMode := cmMakeNew;
end;



procedure TAOCTForm.DeleteOCTOKButtonClick(Sender: TObject);
var
FullOCTpath : String;

begin
FullOCTpath := SlashSep(CurrentPartDir,Octextended(FSelectedOCT));
if FileExists(FullOCTpath) then
   begin
   DeleteFile(FullOCTpath);
   end
else
    begin
    end;
SetEditMode(emPart,True);
CopyCancelButtonClick(Self);
ForceFirstOCT;
end;

procedure TAOCTForm.ForceFirstOCT;
begin
SelectOCTRow(1);
end;

procedure TAOCTForm.SetCurrentOCT(OCTName : String);
begin
FCurrentOCT := OCTName;
end;

procedure TAOCTForm.SetCurrentPart(PartName : String);
begin
FCurrentPart := PartName;
PartNameDisplay.Text := FCurrentPart;
end;


procedure TAOCTForm.SetSelectedOCT(OCTName: String);
begin
FSelectedOCT := OCTName;
OctNameDisplay.Text := OCTName;
end;


constructor TAOCTForm.Create(AOwner : TComponent);
begin
inherited Create(Application);
AOCTForm := Self;
FileSelector := TFileSelector.Create(Application);
CopyMakeDialogue := TCopyMakeDialogue.Create(Application);

end;


procedure TAOCTForm.OKButtonClick(Sender: TObject);
begin
ModalResult := mrOK
end;

procedure TAOCTForm.Execute;
begin
inherited;
FileSelector.TFP1a.SelectMode := smOPP;
FileSelector.TFP1a.Initialise;
if SelectPartDialog then
   begin
   ShowModal;
   end;
end;


function TAOCTForm.ListAllReferencedOppsForPart(PartDirectory : String): TIntegrityResults;
var
OCTList     : TStringList;
OPPList     : TStringList;
AllOppList  : TStringList;
ValidList   : TStringList;
DependList  : TStringList;

OCTEntry    : Integer;
OCTFileName : String;
OCTFile     : TStringList;
RefList     : TOPPRefList;
OPPNumber   : Integer;
Exists      : Boolean;
RefNumber   : Integer;
ReportLine  : String;
RefEntry    : pOPPRec;
RefCount    : Integer;
TestOPP     : String;
AllOK       : Boolean;
begin
AllOK := True;
Result := [];
if not DirectoryExists(PartDirectory) then
   begin
   ShowMessage(PartDirNExistText,'','','',mmError,False);
   exit;
   end;
OCTList    := TStringList.Create;
AllOppList := TStringList.Create;
OPPList    := TStringList.Create;
OCTFile    := TStringList.Create;
ValidList  := TStringList.Create;
DependList  := TStringList.Create;
RefList    := TOPPRefList.Create;
try
MakeOCTList(OCtList,PartDirectory);
ReportM.Lines.Clear;
if OCtList.Count < 1 then
   begin
   ReportM.Lines.Add(NoOctsText);
   MainPages.ActivePage := ReportSheet;
   exit;
   end;
For OCTEntry := 0 to OCtlist.Count-1 do
    begin
    OCTFileName := SlashSep(PartDirectory,OCtList[OctEntry]);
    OCtFile.LoadFromFile(OCTFileName);
    MakeOPPList(OctFile,OppList,ValidList,DependList,PartDirectory);
    if OppList.Count > 0 then
       begin
       for OPPNumber := 0 to OppList.Count-1 do
           begin
           Exists :=  ValidList[OppNumber] = 'OK';
           RefList.RegisterOPP(OppList[OPPNumber],OCtList[OctEntry],Exists);
           end
       end
    else
        begin
        ReportLine := Format(EmptyOCTText,[OCtList[OctEntry]]);
        ReportM.Lines.Add(ReportLine);
        Result := Result + [HasEmptyOCT];
        end;
    end;
For RefNumber := 0 to RefList.Count-1 do
    begin
    RefEntry := RefList.List[RefNumber];
    if (RefEntry.ReferenceCount = 1)  then
       begin
       if RefEntry.Exists then
          begin
          ReportLine := Format(OKRefText,[RefEntry.oppName,RefEntry.OCTName]);
          ReportM.Lines.Add(ReportLine);
          end
       else
           begin
           ReportLine := Format(NotExistText,[RefEntry.oppName,RefEntry.OCTName]);
           ReportM.Lines.Add(ReportLine);
           AllOK := False;
           end;
       end
    else
        begin
        AllOK := False;
        ReportLine := Format(MultiRefText,[RefEntry.oppName,RefEntry.ReferenceCount]);
        ReportM.Lines.Add(ReportLine);
        For RefCount := 0 to RefEntry.RefList.Count -1 do
            begin
            reportLine := Format('%-40s %s',['',RefEntry.RefList[RefCount]]);
            ReportM.Lines.Add(ReportLine);
            Result := Result + [HasDuplicateOPPRef];
            end;
        end;
    end;
// Now list any Opps which are not referenced
ReportM.Lines.Add('----------');

MakeAllOPPList(AllOppList,PartDirectory);
for OPPNumber := 0 to AllOppList.Count-1 do
    begin
    TestOPP := AllOppList[OppNumber];
    if not RefList.Contains(TestOPP) then
       begin
        reportLine := Format(NoRefText,[TestOPP]);
        ReportM.Lines.Add(ReportLine);
        Result := Result + [HasRedundentOPPs];
        AllOK := False;
       end
    end;
ReportM.Lines.Add('----------');
if AllOK then
   ReportM.Lines.Add(PartGoodText)
else
   ReportM.Lines.Add(PartFailedText);




finally
OCTList.Free;
OPPList.Free;
AllOppList.Free;
OCTFile.Free;
ValidList.Free;
DependList.Free;
RefList.Destoyit;
end;
MainPages.ActivePage := ReportSheet;

end;



procedure TAOCTForm.MakeRefListForOPP(OPPName, PartDirectory: String;
  List: TStringList);
begin
if not DirectoryExists(PartDirectory) then
   begin
   ShowMessage(PartDirNExistText,'','','',mmError,False);
   exit;
   end;
if not assigned(List) then exit;
List.clear;




end;

procedure TAOCTForm.CheckIntegrityKeyClick(Sender: TObject);
var
PartResult : TIntegrityResults;
//  TIntegrityResults = set of (IntegrityGood,HasEmptyOCT,HasDuplicateOPPRef,HasRedundentOPPs);

begin
PartResult :=  ListAllReferencedOppsForPart(CurrentPartDir);
if HasRedundentOPPs in PartResult then
   begin
   DeleteUnusedButton.Visible := True;
   end;
if HasEmptyOCT in PartResult then
   begin
   DeleteEmptyOCTButton.Visible := True;
   end;
end;


procedure TAOCTForm.DeleteEmptyOCTButtonClick(Sender: TObject);
var
OCTList : TStringList;
OCTFile : TStringList;
OCTEntry  : Integer;
OCTFileName : String;
begin
OCTList    := TStringList.Create;
OCTFile    := TStringList.Create;
try
MakeOCTList(OCtList,CurrentPartDir);
if OCtList.Count < 1 then
   begin
   exit;
   end;
For OCTEntry := 0 to OCtlist.Count-1 do
    begin
    OCTFileName := SlashSep(CurrentPartDir,OCtList[OctEntry]);
    OCtFile.LoadFromFile(OCTFileName);
    if (OCtFile.Count = 0) or  ((OCtFile.Count = 1) and (OCtFile[0] = ''))then
       begin
       // this is an empty OCT
       DeleteFile(OCTFileName);
       end;
    end;
finally
OCTList.Free;
OCTFile.Free;
DeleteEmptyOCTButton.Visible := False;
SetEditMode(emPart,True);
end;
end;


procedure TAOCTForm.DeleteUnusedButtonClick(Sender: TObject);
var
ALLOPPList  : TStringList;
RefList     : TOPPRefList;
OPPNUmber   : Integer;
TestOPP     : String;
ReportLine  : String;
DeletePath  : String;
begin
RefList    := TOPPRefList.Create;
AllOPPList := TStringList.Create;
try
MakeReferneceListForPart(CurrentPartDir,Reflist);
MakeAllOPPList(AllOppList,CurrentPartDir);
for OPPNumber := 0 to AllOppList.Count-1 do
    begin
    TestOPP := AllOppList[OppNumber];
    if not RefList.Contains(TestOPP) then
       begin
       ReportLine := Format(DeletingText,[TestOPP]);
       DeletePath := SlashSep(CurrentPartDir,TestOPP);
       if CopyandDelete(DeletePath) then
          begin
          ReportLine := ReportLine + OKText;
          end
        else
          begin
          ReportLine := ReportLine + FailedText;
          end;
        ReportM.Lines.Add(ReportLine);
       end
    end;
finally
AllOppList.Free;
RefList.Destoyit;
end;
DeleteUnusedButton.Visible := False;
SetEditMode(emPart,True);
end;

function TAOCTForm.MakeReferneceListForPart(PartDir: String;RefList: TOPPRefList):Boolean;
var
OCTList   : TStringList;
OCtFile   : TStringList;
OPPList   : TStringList;
ValidList : TStringList;
DependList : TStringList;
OCTEntry  : Integer;
OPPNumber : Integer;
OCTFileName : String;
begin
Result := False;
if not assigned(Reflist) then exit;
OCTList   := TStringList.Create;
OCtFile   := TStringList.Create;
OPPList   := TStringList.Create;
ValidList := TStringList.Create;
DependList:= TStringList.Create;
try
MakeOCTList(OCtList,CurrentPartDir);
For OCTEntry := 0 to OCtlist.Count-1 do
    begin
    OCTFileName := SlashSep(PartDir,OCtList[OctEntry]);
    OCtFile.LoadFromFile(OCTFileName);
    MakeOPPList(OctFile,OppList,ValidList,DependList,PartDir);
    for OPPNumber := 0 to OppList.Count-1 do
        begin
        RefList.RegisterOPP(OppList[OPPNumber],OCtList[OctEntry],True);
        end;
    end;
finally
OCTList.Free;
OCtFile.Free;
OPPList.Free;
DependList.Free;
ValidList.Free;
Result := True;
end;
end;



procedure TAOCTForm.CopyNewPartKeyClick(Sender: TObject);
begin
ShowPartGrid(True);
CopyPartName.Text := '';
MainDialoguePanel.Visible := True;
DialogPages.ActivePage := CopyPartSheet;
MainKb.FocusControl := CopyPartName;
KPad.FocusControl := CopyPartName;
CopyPartName.SetFocus;

end;

procedure TAOCTForm.DeletePartKeyClick(Sender: TObject);
var
Buffer : String;
begin
if FCurrentPart = '' then
   begin
   ShowMessage(NoPartSelectedText,'','','',mmError,False);
   exit;
   end;
ShowPartGrid(True);
Buffer := Format(PleaseConfirmText,[FCurrentPart]);
DeletePartMessage.Caption := Buffer;
DialogPages.ActivePage := DeletePartDialog;
MainDialoguePanel.Visible := True;
end;



procedure TAOCTForm.ShowMessage(L1,L2,L3,L4 : String;MessageMode : TMessMode;GetNewPart : Boolean);
begin
NewPartOnAck := GetNewPart;
case MessageMode of
     mmError:
     begin
     ErrorPanel.Color := clred;
     ErrorPanel.Font.Color := clYellow;
     end;

     mmInfo:
     begin
     ErrorPanel.Color := clGreen;
     ErrorPanel.Font.Color := clYellow;
     end;
end; // case;
MainDialoguePanel.Visible := True;
DialogPages.ActivePage := ErrorSheet;
ErrorLine1.Caption := L1;
ErrorLine2.Caption := L2;
ErrorLine3.Caption := L3;
ErrorLine4.Caption := L4;
end;

procedure TAOCTForm.ErrorAckButtonClick(Sender: TObject);
begin
Grid.SetFocus;
MainKb.FocusControl := Grid;
KPad.FocusControl := Grid;
MainDialoguePanel.Visible := False;
InitGridwithPart(CurrentPartDir,FCurrentOCT,True);
if NewPartOnAck then
   begin
   NewPartOnAck := False;
   SelectPartDialog;
   end;

end;

procedure TAOCTForm.CopyPartButtonClick(Sender: TObject);
var
NewPartName : String;
NewPartDir  : String;
SourceDirectory :String;
FileNumber      : Integer;

begin
NewPartName := CopyPartName.Text;
if IsValidPartName(NewPartName) then
   begin
   NewPartDir := SlashSep(OPPBase,NewPartName);
   if DirectoryExists(NewPartDir) then
      begin
      ShowMessage(PartExistsText,PPAckTextandRetry,'','',mmError,False);
      end
   else
       begin
       try
       FList.Path := CurrentPartDir;
       FList.Filter := '*.*';
       Flist.UpdateList;
       if not FList.CopyFilesTo(NewPartDir) then
          begin
          ShowMessage(CantCopyPartText,'','','',mmError,False);
          end
       else
           begin
           ShowMessage(PartCopySuccessText,PPAckToCompText,'','',mmError,False);
           end;
       except
       ShowMessage(CantCopyPartText,'','','',mmError,False);
       end;
       SetEditMode(emPart,True);
       end;
   end
else
    begin
    ShowMessage(InvalidPartNameText,'','','',mmError,False);
    end;
end;

procedure TAOCTForm.DelPartOKButtonClick(Sender: TObject);
var
PSource    : array[0..300] of Char;
PDest      : array[0..300] of Char;
Source,Dest : String;
begin
try
Source := RemoveTrailingSlash(CurrentPartDir);
Dest := SlashSep(DeleteDirectory,FCurrentPart);
Dest := RemoveTrailingSlash(Dest);
StrPCopy(PSource,Source);
StrPCopy(PDest,Dest);
if MoveFile(PSource,PDest)  then
   begin
   FCurrentPart := '';
   CurrentPartDir := '';
   PartNameDisplay.Text := '';

   ShowMessage(SuccesDeleteText,PPAckText,SelectNewPartText,'',mmError,True);
   end
else
   begin
   // Here if move failed
   if IsValidPartPath(CurrentPartDir) then
      begin
      Win32DeleteDirectory(CurrentPartDir);
      ShowMessage(SuccesDeleteText,PPAckText,SelectNewPartText,'',mmError,True);
      end
   else
       begin
       if Not DirectoryExists(CurrentPartDir) then
          begin
          ShowMessage(SuccesDeleteText,PPAckText,SelectNewPartText,'',mmError,True);
          end
       else
          begin
          end;
       ShowMessage(PartDeleteFailText,'','','',mmError,False);
       end;
   end


except
end;
end;

procedure TAOCTForm.RenameOCTKeyClick(Sender: TObject);
begin
 RenameMessage.Caption := Format(RenameText,[unextended(FCurrentOCT)]);
 ReNameEdit.Text := '';
 MainDialoguePanel.Visible := True;
 DialogPages.ActivePage := RenameOCTDialog;
 MainKb.FocusControl := ReNameEdit;
 KPad.FocusControl := ReNameEdit;
 ReNameEdit.SetFocus;
// CopyMode := cmCopy;
end;

procedure TAOCTForm.NewOCTOkButtonClick(Sender: TObject);
var
NewOCTName : String;
NewOCTFullPath : String;
CurrentOCTFullpath : String;
OCTFile       : TStringList;
begin
if NewOCTEdit.Text <> '' then
   begin
   NewOCTName := octextended(NewOCTEdit.Text);
   NewOCTFullPath := SlashSep(CurrentPartDir,NewOCTName);
   if FileExists(NewOCTFullPath) then
      begin
      ShowMessage(DestExistsText,'','','',mmError,False);
      exit;
      end
   else
       begin
       OCTFile := TStringList.Create;
       OctFile.Clear;
       OctFile.SaveToFile(NewOCTFullPath);
       SetCurrentOCT(NewOCTEdit.Text);
       FselectedOCT := unextended(FCurrentOCT);

       OCTNameDisplay.Text := ' ' + unextended(FCurrentOCT);
       InitGridwithPart(CurrentPartDir,octextended(FCurrentOCT),True);
       SetEditMode(emPart,True);
       end;
   end;
// Now Update Grid to show new part if in OCT
CopyCancelButtonClick(Self);
end;

function  TAOCTForm.EditDependency(CurrentPartPath,OCTName,OPPName : String;Position : Integer):Boolean;
var
OCTFile : TStringList;
OCTFilename : String;
SelectedOPP : String;
OCTLine     : Integer;
lineBuffer  : String;
PointerPos  : Integer;
begin
Result := False;
OCTFile := TStringList.Create;
try
if Position < 1 then exit;
OCTFileName := SlashSep(CurrentPartPath,OCTName);
OCtFile.LoadFromFile(OCTFileName);
OCTLine := Position-1;
if OCTLine < OCtFile.Count then
   begin
   LineBuffer := OCtFile[OCTLine];
   PointerPOs := Pos('->',LineBuffer);
    if PointerPos > 0 then
       begin
       LineBuffer := Copy(LineBuffer,1,POinterPos+1)+' '+WithQuotes(OPPName);
       OCtFile[OCTLine] := LineBuffer;
       OCtFile.SaveToFile(OCTFileName);
       Result := True;
       end
   end;
finally
OCTFile.Free;
end;
end;


procedure TAOCTForm.RemoveDependencyKeyClick(Sender: TObject);
var
OCTFile : TStringList;
OCTFilename : String;
SelectedOPP : String;
OPPName     : String;
OCTLine     : Integer;
lineBuffer  : String;
PointerPos  : Integer;
begin
SelectedOPP := Grid.Cells[2,SelectedRow];
OCTFile := TStringList.Create;
try
if SelectedRow < 1 then exit;
OCTFileName := SlashSep(CurrentPartDir,FSelectedOCT);
OCtFile.LoadFromFile(OCTFileName);
OCTLine := SelectedRow-1;
if OCTLine < OCtFile.Count then
   begin
   LineBuffer := OCtFile[OCTLine];
   PointerPOs := Pos('->',LineBuffer);
    if PointerPos > 0 then
       begin
       OppName := StripQuotes(Copy(OctFile[OCTLine],1,PointerPos-1));
       if CompareText(OPPName,SelectedOPP)= 0 then
          begin
          LineBuffer := Copy(LineBuffer,1,POinterPos+1)+' *';
          OCtFile[OCTLine] := LineBuffer;
          OCtFile.SaveToFile(OCTFileName);
          end
       end
   end;
finally
OCTFile.Free;
end;
InitGridwithPart(CurrentPartDir,FCurrentOCT,True);
SelectOPPRow(SelectedRow);
end;

procedure TAOCTForm.EditDependencyKeyClick(Sender: TObject);
var
OPPName : String;
CurrentOPP : String;
DepList : TStringList;
OCTFile : TStringList;
ValidList : TStringList;
DummyDependList : TStringList;
OCtPath : String;
SingleOPP : Boolean;
Found     : Boolean;
OppCount     : Integer;
begin
DepList := TStringList.Create;
OCTFile := TStringList.Create;
ValidList := TStringList.Create;
DummyDependList := TStringList.Create;

try
with FileSelector do
     begin
     aBaseDir := CurrentPartDir;
     SelectMode := smDependency;
     MustSelectPart := False;
     MustSelectOCT  := False;
     OCtPath :=  SlashSep(CurrentPartDir,FSelectedOCT);
     OCTFile.LoadFromFile(OCtPath);
     MakeOPPList(OCTFile,DepList,ValidList,DummyDependList,CurrentPartDir);
     if DepList.Count > 1 then
        begin
        // Now remove the selected OPP from the dependency list
        SingleOPP := False;
        OppCount := 0;
        Found := False;
        while (not Found) and (OppCount < DepList.Count) do
              begin
              CurrentOPP := Grid.Cells[2,SelectedRow];
              if CompareText(DepList[OppCount],CurrentOPP) = 0 then
                 begin
                 Found := True;
                 DepList.Delete(OppCount);
                 end;
              inc(OppCount);
              end;

        SetDependencyList(DepList);
        end
     else
         begin
         SingleOPP := True;
         end
     end;
if SingleOPP then exit;
if FileSelector.ShowModal = mrOK then
   begin
   if SelectedRow = 0 then SelectedRow := 1;
   OPPName := ExtractFileName(FileSelector.SelectedFile);

   if EditDependency(CurrentPartDir,FCurrentOCT,OPPName,SelectedRow) then
      begin
      InitGridwithPart(CurrentPartDir,FCurrentOCT,True);
      SelectOPPRow(SelectedRow);
      end
   end
finally
DePlist.Free;
ValidList.Free;
DummyDependList.Free;

end;

end;

function IsValidPartPath(const Ident: string): Boolean;
var
slashPos : Integer;
begin
Result := False;
if not DirectoryExists(Ident) then exit;
SlashPos := Pos ('\',Ident);
if SlashPos < 2 then exit;
if Length(Ident) = SlashPos then exit;
Result := True;
end;

function IsValidPartName(const Ident: string): Boolean;
const
  Alpha = ['A'..'Z', 'a'..'z', '_'];
  AlphaNumeric = Alpha + ['0'..'9'];
var
  I: Integer;
begin
  Result := False;
  if (Length(Ident) = 0)  then Exit;
  for I := 2 to Length(Ident) do if not (Ident[I] in AlphaNumeric) then Exit;
  Result := True;
end;
procedure TAOCTForm.FinalInstall;
begin
  inherited;
  Translator := TTranslator.Create;
  try
  with Translator do
        begin
        NoOPPText         := GetString('$NO_OPPS_IN_OCT');
        NoOctsText        := GetString('$NO_OCTS_IN_PART');
        NoPartsText       := GetString('$NO_PART_SELECTED');
        OPPExistsText     := GetString('$AN_OPP_OF_TAHT_NAME_ALREADY_EXISTS');
        DestExistsText    := GetString('$DESTINATION_OCT_ALREADY_EXISTS');
        PartDirNExistText := GetString('$PART_DIRECTORY_DOES_NOT_EXIST');
        OCTNExistText     := GetString('$OCT_S_DOES_NOT_EXIST');
        IndexText         := GetString('$INDEX');
        NameText          := GetString('$NAME');
        ValidText         := GetString('$VALID');
        DependencyText    := GetString('$DEPENDENCY');
        OKText            := GetString('$OK');
        FailedText        := GetString('$FAILED');
        OCTNameText       := GetString('$OCT_NAME');
        PCDText           := GetString('$PLEASE_CONFIRM_THE_DELETION_OF_THE_OCT');
        PartExistsText    := GetString('$A_PART_OF_THAT_NAME_ALREADY_EXIST');
        PPOKText          := GetString('$PLEASE_PRESS_OK_AND_TRY_AGAIN');
        InvalidPartText   := GetString('$INVALID_PART_NAME_ENTERED');
        OKRefText         := GetString('$S_REFERNCED_ONLY_IN_S_EXISTS_OK');
        NotExistText      := GetString('$S_REFERNCED_IN_PART_DIRECTORY_DOES_NOT_EXIST');
        MultiRefText      := GetString('$S_REFERENCED_IN_D_OCTS');
        EmptyOCTText      := GetString('$S_HAS_NO_OPPS_EMPTY_OCT');
        NoRefText         := GetString('$s_IS_NOT_REFERENCE_IN_ANY_OCT');
        DeletingText      := GetString('$DELETING OPP .. %s ....');
        PartGoodText      := GetString('$PART_INTEGRITY_IS_GOOD');
        PartFailedText    := GetString('$PART_INTEGRITY_FAILED');
        OCTNotFoundText   := GetString('$OCT_FILE_NOT_FOUND');
        InvalidPartNameText := GetString('$INVLAID_PART_NAME_ENTERD');
        PPAckTextandRetry   := GetString('$PLEASE_PRESS_ACK_AND_TRY_AGAIN');
        CantCopyPartText    := GetString('$ERROR_COPYING_PART');
        PartCopySuccessText := GetString('$PART_COPIED_SUCCESSFULLY');
        PPAckToCompText     := GetString('$PLEASE_PRESS_ACK_TO_CONTINUE');
        CantCopyExistsText  := GetString('$AN_OPP_OF_THAT_NAME_ALREADY_EXISTS_IN_THE_PART');
        NotCopiedText       := GetString('$FILE_HAS_NOT_BEEN_COPIED');
        PleaseConfirmText   := GetString('$PLEASE_CONFIRM_DELETION_OF_S');
        OrCancelText        := GetString('$OR_PRESS_CANCEL_TO_ABORT');
        NoPartSelectedText  := GetString('$DELETE_FAILED...NO_PART_SELECTED');
        SuccesDeleteText    := GetString('$PART_SUCCESSFULLY_DELETED');
        PPAckText           := GetString('$PLEASE_PRESS_ACK');
        SelectNewPartText   := GetString('$AND_THEN_SELECT_A_DIFFERENT_PART');
        PartDeleteFailText  := GetString('$FAILED_TO_DELETE_PART');
        NoOCTText           := GetString('$NO_OCT_IS_SELECTED');
        OCTExistsText       := GetString('$AN_OCT_OF_THE_SPECIFIED_NAME_ALREADY_EXISTS');
        RenameText          := GetString('$PLEASE_INPUT_NEW_NAME_FOR_S');
        NoneText            := GetString('$NONE');

        NBLabel1.Caption := GetString ('$ONLY_THE_OCT_WILL_BE_DELETED');
        NBLabel2.Caption := GetString ('$ALL_REFERNCED_OPPS_WILL_STILL_EXIST');
        PleaseInputLabel.Caption := GetString ('$PLEASE_INPUT_NAME_FOR_NEW_PART');
        PleaseInputOCTLabel.Caption := GetString ('$PLEASE_INPUT_NAME_FOR_NEW_OCT');
        CPartLabel.Caption  := GetString ('$CURRENT_PART');
        AppendOPPKey.Legend := GetString ('$APPEND~OPP');
        DeleteOPPKey.Legend := GetString ('$DELETE~OPP');
        InsertOPPKey.Legend := GetString ('$INSERT~OPP');
        MoveOPPUpKey.Legend := GetString ('$MOVE_OPP~UP');
        MoveOPPDownKey.Legend := GetString ('$MOVE_OPP~DOWN');
        EditDependencyKey.Legend := GetString ('$EDIT~DEPENDENCY');
        RemoveDependencyKey.Legend := GetString ('$REMOVE~DEPENDENCY');
        ChangePartKey.Legend       := GetString ('$CHANGE~PART');
        NewPartKey.Legend          := GetString ('$MAKE_NEW~PART');
        CopyNewPartKey.Legend      := GetString ('$COPY_PART~TO_NEW_PART');
        DeletePartKey.Legend       := GetString ('$DELETE~PART');
        DeleteOCTKey.Legend        := GetString ('$DELETE~OCT');
        ADDOCTKey.Legend           := GetString ('$ADD~OCT');
        RenameOCTKey.Legend        := GetString ('$RENAME~OCT');
        CheckIntegrityKey.Legend   := GetString ('$CHECK_PART~INTEGRITY');
        ErrorAckButton.Legend      := GetString ('$ACKNOWLEDGE');
        OCTEditKey.Legend          := GetString ('$OCT~EDIT');
        PartEditKey.Legend         := GetString ('$PART~EDIT');
        OKButton.Caption           := GetString ('$EXIT');
        CUpKey.Caption             := GetString ('$UP');
        CDownKey.Caption           := GetString ('$DOWN');
        DeleteEmptyOCTButton.Legend := GetString('$DELETE~EMPTY_OCTS');
        DeleteUnusedButton.Legend  :=  GetString('$DELETE~UNUSED_OCTS');
        FileSelector.PSOPPText     :=  GetString('$PLEASE_SELECT_OPP');
        FileSelector.PSOCTText     :=  GetString('$PLEASE_SELECT_OCT');//'Please Select an OCT and Press Open';
        FileSelector.PSPartText    :=  GetString('$PLEASE_SELECT_PART');// 'Please Select a Part and Press Open';

        end;
        finally
        Translator.Free;
        end;


end;

initialization
begin
TabstractFormPlugin.AddPlugin('OCTEditPlugin',TAOCTForm);
end;





end.
