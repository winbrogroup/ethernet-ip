 unit PPedit;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ComCtrls, ExtCtrls,  Buttons, OleCtrls,OKBox,AbstractOPPPlugin,NamedPlugin,FPicker,Tokenizer,IniFiles,
  ACNC_SHeditor,ACNC_plusmemo,ACNC_plusGutter, ImgList, ACNC_TouchSoftkey,clipbrd,
  ACNC_BaseKeyBoards,INamedInterface;

const
  SectionOperationDescription = 'OPERATIONDESCRIPTION';
type
TFindModes = (fmNone,fmFind,fmReplace);

TOppEditMode = (oeGCodeEdit,oeHeaderEdit);

  TOPPFile = class
    PartType: String;
    Comment: String;
    ToolTypeString : String;
    ElectrodeCount: Integer;
    ElectrodeMap : String;
    ToolUsage: Double;  {mm}
    CycleTime: Integer; {sec}
    ToolXOffset : Double;
    ToolYOffset : Double;
    ToolZOffset : Double;
    procedure Load(S: TStringTokenizer); {from strings}
    procedure Save(S: TStrings); {to strings}
  end;

  TGCodeForm = class(TForm)
    MainKb: TBaseKB;
    ControlPanel: TPanel;
    FileOPen: TOpenDialog;
    FindPanel: TPanel;
    SavePanel: TPanel;
    FindModePanel: TPanel;
    UndoControls: TPanel;
    KeyPad: TBaseFPad;
    StartTimer: TTimer;
    OPPHeaderPanel: TPanel;
    OPPPartPanel: TPanel;
    PartTypeLabel: TLabel;
    ReferenceLabel: TLabel;
    PartTypeEdit: TEdit;
    ReferenceEdit: TEdit;
    Panel3: TPanel;
    Label1: TLabel;
    ElectrodeCountLabel: TLabel;
    ToolTypeEdit: TEdit;
    ElectrodeCountEdit: TEdit;
    ToolUsageLabel: TLabel;
    ToolUsageEdit: TEdit;
    CycleTimeEdit: TEdit;
    CycleTimeLabel: TLabel;
    UsageLabel: TLabel;
    Label3: TLabel;
    HeaderOKButton: TSpeedButton;
    HeaderCancelButton: TSpeedButton;
    Label5: TLabel;
    ElectrodeMapEdit: TEdit;
    Label6: TLabel;
    HexFault: TLabel;
    ToolFault: TLabel;
    ToolOffsetPanel: TPanel;
    ToolXOffsetLabel: TLabel;
    ToolXOffsetEdit: TEdit;
    ToolYOffsetLabel: TLabel;
    ToolYOffsetEdit: TEdit;
    Label4: TLabel;
    ToolZOffsetEdit: TEdit;
    pp1: TOPPSHEdit;
    OkButton: TTouchSoftkey;
    CancelButton: TTouchSoftkey;
    SearchPages: TPageControl;
    FindSheet: TTabSheet;
    UndoPanel: TPanel;
    UndoKey: TSpeedButton;
    RedoKey: TSpeedButton;
    topBottomPanel: TPanel;
    KBKey1: TKBKey;
    KBKey2: TKBKey;
    BookMarkKey: TTouchSoftkey;
    FindTextEdit: TSHCursorEdit;
    ReplaceSheet: TTabSheet;
    ReplaceTextEdit: TSHCursorEdit;
    FindandReplaceButton: TSpeedButton;
    FindButton: TSpeedButton;
    SoptionsPanel: TPanel;
    FindWordButton: TSpeedButton;
    MatchCaseButton: TSpeedButton;
    FWdButton: TSpeedButton;
    BackwardButton: TSpeedButton;
    FindAgainButton: TSpeedButton;
    FindDoneKey: TTouchSoftkey;
    logoSheet: TTabSheet;
    Image1: TImage;
    procedure FormCreate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FindButtonClick(Sender: TObject);
    procedure FindTextEditKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FindAgainButtonClick(Sender: TObject);
    procedure OKButtonClick(Sender: TObject);
    procedure CancelButtonClick(Sender: TObject);
    procedure FindWordButtonClick(Sender: TObject);
    procedure MatchCaseButtonClick(Sender: TObject);
    procedure KBKey1MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure KBKey2Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure StartTimerTimer(Sender: TObject);
    procedure UndoKeyClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure PartTypeEditEnter(Sender: TObject);
    procedure ReferenceEditEnter(Sender: TObject);
    procedure ToolTypeEditEnter(Sender: TObject);
    procedure ElectrodeCountEditEnter(Sender: TObject);
    procedure ToolXOffsetEditEnter(Sender: TObject);
    procedure ToolYOffsetEditEnter(Sender: TObject);
    procedure ToolUsageEditEnter(Sender: TObject);
    procedure CycleTimeEditEnter(Sender: TObject);
    procedure HeaderCancelButtonClick(Sender: TObject);
    procedure ElectrodeMapEditEnter(Sender: TObject);
    procedure ToolZOffsetEditEnter(Sender: TObject);
    procedure FWdButtonClick(Sender: TObject);
    procedure BackwardButtonClick(Sender: TObject);
    procedure RedoKeyClick(Sender: TObject);
    procedure BookMarkKeyMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FindDoneKeyMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure ElectrodeCountEditChange(Sender: TObject);
  private
    { Private declarations }
    LineNumber : Integer;
    SearchOptions     : TSearchSwitches;
    MapList           : TStringList;

    ToolTypeIsNumeric   : Boolean;
    AlphaToolTypeLength : Integer;
    MaxNumericToolCode  : Integer;
    ShowToolOffsets     : Boolean;
    FindMode            : TFindModes;


    procedure FocusOnEditor;
    procedure UndoPermissionChange(Sender : TObject);
    procedure KnownWordChange(Sender : TObject);
    procedure ContentsChanged(Sender : TObject);
    procedure ReadINiFile;
    procedure ReadConfigFile;
    procedure ShowFindPage(fMode : TFindModes);
  public
    EditMode : TOppEditMode;
    //GCode stuff
    FileString        : String;
    //OPP Header Stuff
    OPPData: TOPPFile;
    procedure DataToForm;
    function FormToData: Boolean;

  end;

 TGCodeEditor = class(TAbstractOPPPluginEditor)
    class function Execute(var Section : string) : Boolean; override;
    class procedure FinalInstall; override;
  end;

 TOPPHeaderEditor = class(TAbstractOPPPluginEditor)
  public
    class function Execute(var aSection: string): Boolean; override;
  end;

 TActiveOppServer = class(TAbstractOPPPluginConsumer)
  private
  public
    FActiveOppFile : TOppFile;
    constructor Create(aSectionName : string); override;
    procedure Consume(aSectionName, Buffer : PChar); override;
  end;


function GeneralPluginSelectProgram(ReqPath, RespPath : PChar; Load : Boolean) : Boolean;stdcall;


exports
  GeneralPluginSelectProgram name 'GeneralPluginSelectProgram';


var
  GCodeForm: TGCodeForm;
  InchModeTag         : TTagref;
  InchMode            : Boolean;

function CSVField(FNumber: Integer;TextString : String):String;
function GetToken(var Buffer : String):String;
implementation

{$R *.DFM}

var
    PartTypeNDX : Integer;
    CommentNDX: Integer;
    ToolTypeStringNDX : Integer;
    ElectrodeMapNDX : Integer;
    ElectrodeCountNDX: Integer;
    ToolUsageNDX: Integer;  {mm}
    CycleTimeNDX: Integer; {sec}
    ToolXOffsetNDX : Integer;
    ToolYOffsetNDX : Integer;
    ToolZOffsetNDX : Integer;

{Each individual tool is identified on the Hypertac plug with a
 14 bit number, of which 9 bits identify the type and 5 bits are
 a type serial number. The total uniquely identifies the tool on
 the site}

procedure TOPPFile.Load(S: TStringTokenizer);
var Token : string;

  function ReadDoubleAssign : Double;
  var Code : Integer;
  begin
    Val(S.ReadStringAssign, Result, Code);
  end;

begin
  PartType := 'None';
  Comment := 'No Ref';
  ToolTypeString := 'Not Valid';
  ElectrodeMap := '000000000000';
  ToolUsage := 0;
  CycleTime := 0;
  ElectrodeCount := 0;
  ToolXOffset := 0;
  ToolYOffset := 0;
  ToolZOffset := 0;

  Token := LowerCase(S.Read);
  while not S.EndOfStream do begin
    if Token = 'parttype' then
      PartType := S.ReadStringAssign
    else if Token = 'comment' then
      Comment := S.ReadStringAssign
    else if Token = 'tooltypestring' then
      ToolTypeString := S.ReadStringAssign
    else if Token = 'toolusage' then
      ToolUsage := ReadDoubleAssign
    else if Token = 'cycletime' then
      CycleTime := S.ReadIntegerAssign
    else if Token = 'electrodecount' then
      ElectrodeCount := S.ReadIntegerAssign
    else if Token = 'electrodemap' then
      ElectrodeMap := S.ReadStringAssign
    else if Token = 'toolxoffset' then
      ToolXOffset := ReadDoubleAssign
    else if Token = 'toolyoffset' then
      ToolYOffset := ReadDoubleAssign
    else if Token = 'toolzoffset' then
      ToolZOffset := ReadDoubleAssign
    else
      raise Exception.Create('Unexpected token [' + Token + ']');
//      S.ReadStringAssign;

    Token := LowerCase(S.Read);
  end;
end;

procedure TOPPFile.Save(S: TStrings);
begin
  S.Clear;
  S.Add(Format('PartType = ''%s''',       [PartType]));
  S.Add(Format('Comment = ''%s''',      [Comment]));
  S.Add(Format('ToolTypeString = %s',       [ToolTypeString]));
  S.Add(Format('ToolUsage = %g',      [ToolUsage]));
  S.Add(Format('CycleTime = %d',      [CycleTime]));
  S.Add(Format('ElectrodeCount = %d', [ElectrodeCount]));
  S.Add(Format('ElectrodeMap = %s', [ElectrodeMap]));
  S.Add(Format('ToolXOffset = %g',    [ToolXOffset]));
  S.Add(Format('ToolYOffset = %g',    [ToolYOffset]));
  S.Add(Format('ToolZOffset = %g',    [ToolZOffset]));
end;


procedure TGCodeForm.DataToForm;
begin
  with OPPData do begin
    PartTypeEdit.Text:= PartType;
    
    ReferenceEdit.Text:= Comment;
    try
    ToolTypeEdit.Text:= ToolTypeString;
    except
    ToolTypeEdit.Text:= 'DataError';
    end;



    try
    ElectrodeCountEdit.Text:= IntToStr(ElectrodeCount);
    except
    ElectrodeCountEdit.Text:= 'DataError';
    end;


    try
    ElectrodeMapEdit.Text:= ElectrodeMap;
    except
    ElectrodeMapEdit.Text:= 'DataError';
    end;


    try
    if InchMode then
        begin
        ToolUsageEdit.Text:= Format('%-d', [Round(ToolUsage)]);
        end
    else
        begin
        ToolUsageEdit.Text:= Format('%6.2f', [ToolUsage]);
        end;

    except
    ToolUsageEdit.Text:= 'DataError';
    end;

    try
    CycleTimeEdit.Text:= IntToStr(CycleTime);
    except
    CycleTimeEdit.Text:= 'DataError';
    end;
    ToolXOffsetEdit.Text := Format('%.4f', [ToolXOffset]);
    ToolYOffsetEdit.Text := Format('%.4f', [ToolYOffset]);
    ToolZOffsetEdit.Text := Format('%.4f', [ToolZOffset]);
  end
end;

function TGCodeForm.FormToData : Boolean;
var Code : Integer;
begin
Result := True;
  with OPPData do begin
    PartType:= PartTypeEdit.Text;
    Comment:= ReferenceEdit.Text;
    try
    ToolTypeString:= ToolTypeEdit.Text;
    except
    Result := False;
    end;


    try
    ElectrodeCount:= StrToInt(ElectrodeCountEdit.Text);
    except
    Result := False;
    end;

    try
    ElectrodeMap:= ElectrodeMapEdit.Text;
    except
    Result := False;
    end;

    try
    ToolUsage:= StrToFloat(ToolUsageEdit.Text);
    except
    Result := False;
    end;

    try
    CycleTime:=  StrToInt(CycleTimeEdit.Text);
    except
    Result := False;
    end;
    Val(ToolXOffsetEdit.Text, ToolXOffset, Code);
    Val(ToolYOffsetEdit.Text, ToolYOffset, Code);
    Val(ToolZOffsetEdit.Text, ToolZOffset, Code);
  end
end;


class function TOPPHeaderEditor.Execute(var aSection : string): Boolean;
var
//  Form: TOPPHeaderForm;
  Form : TGCodeForm;
  S : TQuoteTokenizer;
begin
  Result:= false;
  Form:= TGCodeForm.Create(Application);
  S:= TQuoteTokenizer.Create;
  try
    S.Text := aSection;
    S.Seperator := '=';
    S.QuoteChar := '''';
    S.Rewind;
    try
      Form.OPPData.Load(S);
    except
      on E : Exception do
        Application.ShowException(E);
    end;
    Form.DataToForm;
    Form.EditMode := oeHeaderEdit;
    Result:= Form.ShowModal = mrOK;
    if Result then begin
      if Form.FormToData then
         begin
         Form.OPPData.Save(S);
         aSection := S.Text;
         end
      else
          begin
          raise Exception.Create('Bad OPP Header Data. Changes NOT saved');
          end;
    end;
  finally
    S.Free;
    Form.Free
  end
end;

function GeneralPluginSelectProgram(ReqPath, RespPath : PChar; Load : Boolean) : Boolean;stdcall;
var
Form : TFilePickerForm;
begin
Form := TFilePickerForm.Create(Application);
try
Form.FileFilter := ExtractFileName(ReqPath);
if Load then
   begin
   Form.FileFilter := ExtractFileName(ReqPath);
   Form.BasePath := ExtractFilePath(ReqPath);
   Form.FPMode := fpLoadFile;
   end
else
   begin
   Form.FileFilter := '*' + ExtractFileExt(ReqPath);
   Form.BasePath := ReqPath;
   Form.FPMode := fpSaveAs;
   end;

Result := Form.ShowModal = mrOK;
if Result then StrPLCopy(RespPath,Form.SelectedFile,1023);

finally
Form.Free;
end;
end;

class function TGCodeEditor.Execute(var Section : string) : Boolean;
var
Form : TGCodeForm;
TempStream : TMemoryStream;
TempStrings : TStringList;

begin
  Form := TGCodeForm.Create(Application);

  Form.FileString := Section;
  Form.EditMode := oeGCodeEdit;

  Result := Form.ShowModal = mrOK;
  if (Result) Then
     begin
     TempStream := TMemoryStream.Create;
     TempStrings := TStringList.Create;
     try
     TempStrings.Clear;
     Form.PP1.EditPage.Lines.SaveToStream(TempStream);
     TempStream.Position := 0;
     TempStrings.LoadFromStream(TempStream);
     Section := TempStrings.Text;
     finally
     TempStream.Free;
     TempStrings.Free;
     end;
     end;
  Form.Free;
end;



procedure TGCodeForm.FormCreate(Sender: TObject);
begin
LineNumber := 0;
pp1.EditPage.OnKnownWordChange := KnownWordChange;
pp1.EditPage.OnChange := ContentsChanged;
ReadConfigFile;
OPPData:= TOPPFile.Create;
MapList := TStringList.Create;
ReadINiFile;
SearchOptions := [soFwd];
ContentsChanged(Self);
end;
{
IniFile Format
Path is 'ProfilePath'/OPPEdit.ini
Example file is as follows
[GENERAL]
ToolTypeIsNumeric = False
ToolTypeLength=8
MaxNumericToolCode=9999
ShowToolOffsets=True
}
procedure TGCodeForm.ReadINiFile;
var
IniFile : TInifile;
Buffer  : String;
BasePath : String;
begin
BasePath := DLLProfilePath+'OPPEdit.ini';
IniFile := TIniFile.Create(BasePath);
try
with IniFile do
     begin
     Buffer := ReadString ('GENERAL','ToolTypeIsNumeric','False');
     ToolTypeIsNumeric := (Buffer[1]= 't') or (Buffer[1]= 'T');
     AlphaToolTypeLength := ReadInteger('GENERAL','ToolTypeLength',8);
     MaxNumericToolCode :=  ReadInteger('GENERAL','NumericToolTypeMax',9999);
     Buffer := ReadString ('GENERAL','ShowToolOffsets','True');
     ShowToolOffsets   := (Buffer[1]= 't') or (Buffer[1]= 'T');
     end;
if  ToolTypeIsNumeric then
    begin
    ToolFault.Caption := Format('Max Value = %d',[MaxNumericToolCode]);
    end
else
    begin
    ToolFault.Caption := Format('Max of %d Characters',[AlphaToolTypeLength]);
    end;
if  assigned(MapList) then
     begin
     MapList.Clear;
     IniFile.ReadSection('ElectrodeMapData',MapList);
     end;
ToolOffsetPanel.visible := ShowToolOffsets;
finally
IniFile.Free;
end;
end;

procedure TGCodeForm.KnownWordChange(Sender : TObject);
begin
//edit1.Text := pp1.EditPage.KnownWord;
end;


procedure TGCodeForm.UndoPermissionChange(Sender : TObject);
begin
if pp1.Editpage.CanUndo then
        begin
        UndoKey.Enabled := True;
        end
else
        begin
        UndoKey.Enabled := False;
        end;
end;

procedure TGCodeForm.FormResize(Sender: TObject);
begin

pp1.Visible := False;
OPPHeaderPanel.Visible := False;

Width := 1022;
Height := 690;
Top := 75;
Left := 0;
pp1.Left := 1;
pp1.Top := 1;
pp1.Height := (Height div 5)*3;
pp1.Width := ((Width div 4)*3);
OPPHeaderPanel.Align := alNone;
OPPHeaderPanel.Height := (Height div 5)*3;



MainKB.Height := Height- pp1.Height-2;
KeyPad.Height := MainKB.Height;
MainKb.Top := pp1.Height+2;
KeyPad.Top := MainKb.Top;
MainKb.Width := (Width div 4)*3;
MainKB.Left := 1;
KeyPad.Width := Width-pp1.Width-12;
KeyPad.Left := MainKb.Width+2;
PP1.Width := (Width div 4)*3;
ControlPanel.Top := pp1.Top;
ControlPanel.Width := KeyPad.Width;
ControlPanel.Left := KeyPad.Left;
ControlPanel.Height := pp1.Height;
FindTextEdit.Width := ControlPanel.Width -12;
FindTextEdit.Left := 3;


OkButton.Width := (SavePanel.Width div 2)-4;
OkButton.Left := 2;

CancelButton.Width := (SavePanel.Width div 2)-4;
CancelButton.Left := 4+OkButton.Width;


FindButton.Width := (FindPanel.Width div 2) -4;
FindButton.Left :=2;

FindandReplaceButton.Width := (FindPanel.Width div 2) -4;
FindandReplaceButton.Left :=FindButton.Width + FindButton.Left+ 2;

FindAgainButton.Width :=  (SoptionsPanel.Width div 2)-4;
FindAgainButton.Left := 2;
FindAgainButton.Top := 2;

FindDoneKey.Width := FindAgainButton.Width;
FindDoneKey.Top := 2;
FindDoneKey.Left := FindAgainButton.Width+  FindAgainButton.Left + 2;
FindDoneKey.Height := FindAgainButton.Height;


FindWordButton.Width := (SoptionsPanel.Width div 2) -2;
FindWordButton.Left := 2;
FindWordButton.Top := FindAgainButton.Top + FindAgainButton.Height + 10;

MatchCaseButton.Width := FindWordButton.Width;
MatchCaseButton.Left :=2+FindWordButton.Width;
MatchCaseButton.Top := FindWordButton.Top;

FWdButton.Width := FindWordButton.Width;
FWdButton.Left := FindWordButton.Left;
FWdButton.Top := FindWordButton.Top + FindWordButton.Height + 10;

BackwardButton.Width := FWdButton.Width;
BackwardButton.Left :=2+FWdButton.Width;
BackwardButton.Top := FWdButton.Top;

UndoKey.Width := (UndoPanel.Width div 2) -2;
UndoKey.align := alLeft;

RedoKey.Width := (UndoPanel.Width div 2) -2;
RedoKey.align := alRight;


HeaderOKButton.Left :=  OPPPartPanel.Width-((HeaderOKButton.Width*2)+5);
HeaderCancelButton.Left :=  HeaderOKButton.Left+(HeaderOKButton.Width+2);
HeaderOKButton.Top :=  OPPPartPanel.height-(HeaderCancelButton.Height+5);
HeaderCancelButton.Top :=  HeaderOKButton.Top;

if Editmode = oeGCodeEdit then
   begin
   pp1.Visible := True;
   ControlPanel.Visible := True;
   end
else
   begin
   ControlPanel.Visible := False;
   OPPHeaderPanel.Visible := True;
   OPPHeaderPanel.Align := alTop;
   HexFault.Visible := False;
   ToolFault.Visible := False;
   end;

   if InchMode then
     begin
     UsageLabel.Caption := 'thou.'
     end
   else
     begin
     UsageLabel.Caption := 'mm'
     end;



end;





procedure TGCodeForm.FindButtonClick(Sender: TObject);
begin
if Sender= FindButton then
   begin
   if FindButton.Down then
      begin
      ShowFindPage(fmFind);
      with FindTextedit do
           begin
           FindAgainButton.Visible := True;
           FindAgainButton.Caption := 'Find Again';
           FwdButton.Down := True;
           Include(SearchOptions,soFwd);
           enabled := True;
           Color := clwhite;
           Text := '';
           SetFocus;
           end;

      MainKB.FocusControl := FindTextedit;
      Keypad.FocusControl := FindTextedit;
      end
   else
       begin
       FocusOnEditor;
       end;
   end;

if Sender= FindAndReplaceButton then
   begin
   if FindAndReplaceButton.Down then
      begin
      ShowFindPage(fmReplace);
      with FindTextedit do
           begin
           FindAgainButton.Visible := True;
           FindAgainButton.Caption := 'Replace Again';
           FwdButton.Down := True;
           Include(SearchOptions,soFwd);
           enabled := True;
           Color := clwhite;
           Text := '';
           SetFocus;
           end;
      MainKB.FocusControl := FindTextedit;
      Keypad.FocusControl := FindTextedit;

      end
   else
       begin
       FocusOnEditor;
       end;
   end;


end;


procedure TGCodeForm.FindTextEditKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
if Sender = FindTextEdit then
begin
if FindMode = fmFind then
   begin
   if key = VK_RETURN then
      begin
      pp1.EditPage.SearchText(FindTextEdit.Text,SearchOptions);
      FocusOnEditor;
      FindAgainButton.Visible := True;
      FindButton.Down := False;
      end;
   end;

if FindMode = fmReplace then
   begin
   if key = VK_RETURN then
      begin
      FindAndReplaceButton.Down := False;
      ReplaceTextEdit.Visible := True;
      ReplaceTextEdit.SetFocus;
      MainKB.FocusControl := ReplaceTextedit;
      Keypad.FocusControl := ReplaceTextedit;

      end;
   end;
end;

if Sender = ReplaceTextEdit then
   begin
   if key = VK_RETURN then
      begin
      if pp1.EditPage.SearchText(FindTextEdit.Text,SearchOptions) then
         begin
         pp1.EditPage.CutToClipboard;
         ClipBoard.SetTextBuf(PChar(ReplaceTextEdit.Text));
         pp1.EditPage.PasteFromClipboard;
         end;
      FocusOnEditor;
      FindAgainButton.Visible := True;
      FindAndReplaceButton.Down := False;
      end;
   end;
end;

procedure TGCodeForm.FocusOnEditor;
begin
if EditMode = oegCodeEdit then
   begin
   pp1.EditPage.setFocus;
   MainKb.FocusControl := pp1.EditPage;
   KeyPad.FocusControl := pp1.EditPage;
   FindTextEdit.Enabled := False;
   FindButton.Down := False;
   end
else
   begin
   PartTypeEdit.setFocus;
   FindTextEdit.Enabled := False;
   FindButton.Down := False;
   end

end;


procedure TGCodeForm.FindAgainButtonClick(Sender: TObject);
begin
if FindMode = fmFind then
   begin
   pp1.EditPage.SearchText(FindTextEdit.Text,SearchOptions);
   FocusOnEditor;
   end;

if FindMode = fmReplace then
   begin
   if pp1.EditPage.SearchText(FindTextEdit.Text,SearchOptions) then
      begin
      pp1.EditPage.CutToClipboard;
      ClipBoard.SetTextBuf(PChar(ReplaceTextEdit.Text));
      pp1.EditPage.PasteFromClipboard;
      end;
   FocusOnEditor;
   end;
end;


procedure TGCodeForm.OKButtonClick(Sender: TObject);
var
MapString : String;
TVal      : Integer;

function isvalidHex(MaxLen : Integer;TestString : String): Boolean;
var
HexSet : set of Char;
CharCount : Integer;
begin
Result := False;
HexSet := ['0'..'9','a'..'f','A'..'F'];
if Length(TestString) > MaxLen then
   begin
   exit;
   end;
Result  := True;
for CharCount := 1 to Length(TestString) do
    begin
    if not (TestString[CharCount] in HexSet) then
       begin
       Result  := False;
       end;
    end;
end;
begin
if EditMode = oeGCodeEdit then
   begin
   ModalResult := mrOK;
   exit;
   end;
if ToolTypeIsNumeric then
   begin
   try
   TVal := StrToInt(ToolTypeEdit.Text);
   if (TVal < 0) or (TVal > MaxNumericToolCode) then
       begin
       ToolFault.Visible := True;
       exit;
       end;
   except

   end; // try except
   end
else
    begin
    if Length(ToolTypeEdit.Text) > AlphaToolTypeLength then
       begin
       ToolFault.Visible := True;
       exit;
       end;
    end;
MapString := ElectrodeMapEdit.Text;
if isvalidHex(12,MapString) and (Length(MapString)=12) then
      begin
      ModalResult := mrOK;
      end
else
     begin
     HexFault.Visible := True;
     end;
end;

procedure TGCodeForm.CancelButtonClick(Sender: TObject);
var
DoExit : Boolean;
begin
if not pp1.EditPage.Modified then
        begin
        ModalResult := mrCancel;
        end
else
        begin
        CancelDialog := TCancelDialog.Create(Self);
        try
        CancelDialog.ShowModal;
        DoExit :=  CancelDialog.ModalResult = mrOK;
        finally
        CancelDialog.Free;
        end;
        if DoExit then
                begin
                ModalResult := mrCancel;
                end;
        end;
end;

procedure TGCodeForm.FWdButtonClick(Sender: TObject);
begin
Include(SearchOptions,soFwd);
end;
procedure TGCodeForm.BackwardButtonClick(Sender: TObject);
begin
Exclude(SearchOptions,soFwd);
end;


procedure TGCodeForm.FindWordButtonClick(Sender: TObject);
begin
if FindWordButton.Down then
   begin
   Include(SearchOptions,soWholeWord);
   end
else
   begin
   Exclude(SearchOptions,soWholeWord);
   end
end;

procedure TGCodeForm.MatchCaseButtonClick(Sender: TObject);
begin
if MatchCaseButton.Down then
   begin
   Include(SearchOptions,soCaseSensitive);
   end
else
   begin
   Exclude(SearchOptions,soCaseSensitive);
   end
end;


procedure TGCodeForm.KBKey1MouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
FocusOnEditor;
pp1.EditPage.TopofFile;
end;

procedure TGCodeForm.KBKey2Click(Sender: TObject);
begin
FocusOnEditor;
pp1.EditPage.BottomofFile;

end;




procedure TGCodeForm.FormShow(Sender: TObject);
begin
FormResize(Self);
ShowFindPage(fmNone);

StartTimer.Enabled := True;
end;

procedure TGCodeForm.StartTimerTimer(Sender: TObject);
begin
StartTimer.Enabled := False;

pp1.StartEdit(FileString);
FocusOnEditor;

end;

procedure TGCodeForm.UndoKeyClick(Sender: TObject);
begin
pp1.EditPage.Undo;
end;

procedure TGCodeForm.RedoKeyClick(Sender: TObject);
begin
pp1.EditPage.Redo;
end;

procedure TGCodeForm.FormDestroy(Sender: TObject);
begin
  OPPData.Free;
  MapList.Free;
end;

procedure TGCodeForm.PartTypeEditEnter(Sender: TObject);
begin
   MainKb.FocusControl := PartTypeEdit;
   KeyPad.FocusControl := PartTypeEdit;
end;


procedure TGCodeForm.ReferenceEditEnter(Sender: TObject);
begin
MainKb.FocusControl := ReferenceEdit;
KeyPad.FocusControl := ReferenceEdit;
end;

procedure TGCodeForm.ToolTypeEditEnter(Sender: TObject);
begin
MainKb.FocusControl := ToolTypeEdit;
KeyPad.FocusControl := ToolTypeEdit;
end;

procedure TGCodeForm.ElectrodeCountEditEnter(Sender: TObject);
begin
MainKb.FocusControl := ElectrodeCountEdit;
KeyPad.FocusControl := ElectrodeCountEdit;
end;

procedure TGCodeForm.ToolXOffsetEditEnter(Sender: TObject);
begin
MainKb.FocusControl := ToolXOffsetEdit;
KeyPad.FocusControl := ToolXOffsetEdit;
end;



procedure TGCodeForm.ToolYOffsetEditEnter(Sender: TObject);
begin
MainKb.FocusControl := ToolYOffsetEdit;
KeyPad.FocusControl := ToolYOffsetEdit;
end;


procedure TGCodeForm.ToolZOffsetEditEnter(Sender: TObject);
begin
MainKb.FocusControl := ToolZOffsetEdit;
KeyPad.FocusControl := ToolZOffsetEdit;

end;

procedure TGCodeForm.ToolUsageEditEnter(Sender: TObject);
begin
MainKb.FocusControl := ToolUsageEdit;
KeyPad.FocusControl := ToolUsageEdit;
end;


procedure TGCodeForm.CycleTimeEditEnter(Sender: TObject);
begin
MainKb.FocusControl := CycleTimeEdit;
KeyPad.FocusControl := CycleTimeEdit;
end;

procedure TGCodeForm.HeaderCancelButtonClick(Sender: TObject);
begin
ModalResult := mrCancel;
end;


constructor TActiveOppServer.Create(aSectionName : string);
begin
  inherited;
  FActiveOppFile := TOppFile.Create;
//  aParent.RegisterMemEvent(nameNewActiveFile, edgeChange, TStringTag, NewProgram);
end;

procedure TActiveOppServer.Consume(aSectionName, Buffer : PChar);
var S : TQuoteTokenizer;
begin
  S := TQuoteTokenizer.Create;
  try
    S.Text := Buffer;
    S.Seperator := '=';
    S.QuoteChar := '''';
    S.Rewind;
    FActiveOppFile.Load(S);

    with FActiveOPPFile do begin
      NamedPlugin.WriteString(PartTypeNDX, PartType);
      NamedPlugin.WriteString(CommentNDX, Comment);
      NamedPlugin.WriteString(ToolTypeStringNDX, ToolTypeString);
      NamedPlugin.WriteString(ElectrodeMapNDX, ElectrodeMap);
      NamedPlugin.WriteDouble(ElectrodeCountNDX, ElectrodeCount);
      NamedPlugin.WriteDouble(ToolUsageNDX, ToolUsage);
      NamedPlugin.WriteDouble(CycleTimeNDX, CycleTime);
      NamedPlugin.WriteDouble(ToolXOffsetNDX, ToolXOffset);
      NamedPlugin.WriteDouble(ToolYOffsetNDX, ToolYOffset);
      NamedPlugin.WriteDouble(ToolZOffsetNDX, ToolZOffset);
    end;
//    NotifyClients;
  finally
    S.Free;
  end;
end;



procedure TGCodeForm.ElectrodeMapEditEnter(Sender: TObject);
begin
MainKb.FocusControl := ElectrodeMapEdit;
KeyPad.FocusControl := ElectrodeMapEdit;
end;



procedure TGCodeForm.ReadConfigFile;
var
ConfigFile :TIniFile;
FileName : String;
wordList  : TStringList;
keywordcolour : LongInt;
commentcolour : LongInt;
FontColour    : Longint;
BackGroundColour : Longint;
CommentStart  : String;
C3Start       : String;
Buffer        : String;
WordNumber    : Integer;
FontStyle     : TExtFontStyles;
DelimeterSet  : Set of Char;
CharCount     : Integer;
begin
FileName := DLLProfilePath+'OPPEditor.cfg';
if FileExists(FileName) then
   begin
   Wordlist := TStringList.Create;
   try
   ConfigFile := TInifile.Create(Filename);
   try
   with ConfigFile do
        begin
        // Set default font
        Buffer := ReadString('colours','backgroundColour','clwhite');
        if not IdentToColor(Buffer,BackGroundColour) then
           begin
           BackGroundColour := clwhite;
           end;
        Pp1.EditPage.Color := BackGroundColour;

        Buffer := ReadString('colours','normalfontname','Verdana');
        pp1.Font.Name := Buffer;
        pp1.Font.Size := ReadInteger('colours','normalfontsize',12);
        Buffer := ReadString('colours','normalfontcolour','clblack,bold');
        if not IdentToColor(CSVField(0,Buffer),FontColour) then
           begin
           FontColour := clBlack;
           end;
        pp1.Font.Color := FontColour;
        FontStyle := [];
        if CSVField(1,Buffer)='bold' then include(FontStyle,fsbold);
        pp1.Font.Style := FontStyle;

        Buffer := ReadString('delimeters','Characters','$%&,./:;<=>(');
        DelimeterSet := [];
        include(DelimeterSet,Char(' '));
        For CharCount := 1 to Length(Buffer) do
            begin
            include(DelimeterSet,Char(Buffer[CharCount]));
            end;

        pp1.EditPage.Delimiters := Delimeterset;


        // Add all keywords to list
        ReadSection('keywords',Wordlist);
        if Wordlist.Count > 0 then
           begin
           pp1.EditPage.Keywords.BeginUpdate;
           For WordNumber := 0 to Wordlist.Count-1 do
               begin
               Buffer := ReadString('colours','keyword','clmaroon,bold');
               IdentToColor(CSVField(0,Buffer),keywordcolour);
               FontStyle := [];
               if CSVField(1,Buffer)='bold' then include(FontStyle,fsbold);
               if CSVField(2,Buffer)='italics' then include(FontStyle,fsitalic);
               pp1.EditPage.Keywords.AddKeyWord(WordList[WordNumber],[woWholeWordsOnly],FontStyle,1,crDefault,
                                       BackGroundColour,keywordcolour);
               end;
           pp1.EditPage.ReApplyKeywords;
           pp1.EditPage.ApplyKeyWords := True;
           pp1.EditPage.Keywords.EndUpdate;
           end;

        // Now add comment identifiers
        Buffer := ReadString('colours','comment','clblue,bold,italic');
        if not IdentToColor(CSVField(0,Buffer),commentcolour) then
           begin
           commentColour := clBlack;
           end;
        FontStyle := [];
        if CSVField(1,Buffer)='bold' then include(FontStyle,fsbold);
        if CSVField(2,Buffer)='italics' then include(FontStyle,fsitalic);
        CommentStart := ReadString('comments','starttext1','');
        if  CommentStart <> '' then
            begin
            pp1.EditPage.StartStopKeys.AddStartStopKey(CommentStart,'',[],FontStyle,1,crDefault,BackGroundColour,CommentColour,True);
            end;

        // Now add C3 colouring
        Buffer := ReadString('colours','C3Calls','clAqua,bold,italic');
        if not IdentToColor(CSVField(0,Buffer),commentcolour) then
           begin
           CommentColour := clBlack;
           end;
        FontStyle := [];
        if CSVField(1,Buffer)='bold' then include(FontStyle,fsbold);
        if CSVField(2,Buffer)='italics' then include(FontStyle,fsitalic);
        C3Start := ReadString('C3Calls','StartText','');
        if  CommentStart <> '' then
            begin
            pp1.EditPage.StartStopKeys.AddStartStopKey(C3Start,' ',[],FontStyle,1,crDefault,BackGroundColour,CommentColour,True);
            end;

        // Now add Label start character id
        Buffer := ReadString('colours','label','clgreen,bold');
        if not IdentToColor(CSVField(0,Buffer),commentcolour) then
           begin
           commentColour := clGreen;
           end;
        FontStyle := [];
        if CSVField(1,Buffer)='bold' then include(FontStyle,fsbold);
        if CSVField(2,Buffer)='italics' then include(FontStyle,fsitalic);
        CommentStart := ReadString('labels','startchar1','');
        if  CommentStart <> '' then
            begin
            pp1.EditPage.StartStopKeys.AddStartStopKey(CommentStart,' ',[],FontStyle,1,crDefault,BackGroundColour,CommentColour,True);
            end;
        CommentStart := ReadString('labels','startchar2','');
        if  CommentStart <> '' then
            begin
            pp1.EditPage.StartStopKeys.AddStartStopKey(CommentStart,' ',[],FontStyle,1,crDefault,BackGroundColour,CommentColour,True);
            end;
            end;
   finally
   ConfigFile.Free;
   end;
   finally
   WordList.Free;
   end;
   end;
end;


//*********************************************
// function CSVField(
// Result is then Commaseparated field specified by
// FNumber in input Line TextString
//*********************************************
function CSVField(FNumber: Integer;TextString : String):String;
var
Field      : Integer;
begin
Field := FNumber;
while Field >= 0 do
      begin
      Result :=GetToken(TextString);
      dec(Field);
      end;
end;



//*********************************************
// function GetToken
// Result is next commaseparatred string
// var Buffer contains remainder of input string
//*********************************************
function GetToken(var Buffer : String):String;
var
CommaPos : Integer;
begin
Commapos := POs(',',Buffer);
if Commapos > 0 then
   begin
   Result := Copy(Buffer,1,CommaPos-1);
   Buffer := Copy(Buffer,CommaPos+1,Length(Buffer));
   end
else
    begin
    Result := Buffer;
    Buffer := '';
    end;
end;






procedure TGCodeForm.ContentsChanged(Sender: TObject);
begin
UndoKey.Enabled := pp1.EditPage.CanUndo;
RedoKey.Enabled := pp1.EditPage.CanRedo;
end;


procedure TGCodeForm.BookMarkKeyMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
pp1.ShowBookMarkPanel := not pp1.ShowBookMarkPanel;
if pp1.BookMarkPanel.Visible then
   begin
   BookMarkKey.Legend := 'Hide~Bookmark';
   end
else
   begin
   BookMarkKey.Legend := 'Bookmark~Control';
   end

end;

procedure TGCodeForm.ShowFindPage(fMode: TFindModes);
begin
FindMode := fMode;
if fMode = fmNone then
   begin
   SearchPages.ActivePage := LogoSheet;
//   FindModePanel.Visible := False;
   FindModePanel.Visible := True;
   UndoControls.Visible := True;
   end;


if fMode = fmFind then
   begin
   SearchPages.ActivePage := FindSheet;
   UndoControls.Visible := False;
   FindTextEdit.Parent := FindSheet;
   SOptionsPanel.Parent := FindSheet;
   FindModePanel.Visible := True;
   FindAgainButton.Visible := False;
   end;

if fMode = fmReplace then
   begin
   SearchPages.ActivePage := ReplaceSheet;
   UndoControls.Visible := False;
   SOptionsPanel.Parent := ReplaceSheet;
   FindTextEdit.Parent := ReplaceSheet;
   FindModePanel.Visible := True;
   FindAgainButton.Visible := False;
   end;



end;

procedure TGCodeForm.FindDoneKeyMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
FindButton.Down := False;
FindAndReplaceButton.Down := False;
ShowFindPage(fmNone);
end;

procedure TGCodeForm.ElectrodeCountEditChange(Sender: TObject);
var
ECount :integer;
begin
try
ECount := StrToInt(ElectrodeCountEdit.Text);
if (Ecount > 0) and (ECount <= MapList.Count) then
   begin
   ElectrodeMapEdit.Text := MapList[Ecount-1];
   end
else
   begin
   ElectrodeMapEdit.Text := '000000000000';
   end

except
ElectrodeMapEdit.Text :=  '000000000000';
end
end;

class procedure TGCodeEditor.FinalInstall;
begin
  inherited;
  InchModeTag := Named.AquireTag('NC1InchModeDefault', 'TMethodTag', nil);
  InchMode := Named.GetAsBoolean(InchModeTag);


end;

initialization
  // Add the OPPEditor to the interface and declare its section to be 'Floop'
  TAbstractOPPPluginEditor.AddPlugin('NC32GCODE', TGCodeEditor);
  TOPPHeaderEditor.AddPlugin(SectionOperationDescription, TOPPHeaderEditor);
  TActiveOppServer.AddPlugin(SectionOperationDescription, TActiveOppServer);

  PartTypeNDX := PublishItem('OPP_PartType', 'TStringTag', True);
  CommentNDX := PublishItem('OPP_HeaderComment', 'TStringTag', True);
  ToolTypeStringNDX := PublishItem('OPP_ToolIDString', 'TStringTag', True);
  ElectrodeMapNDX := PublishItem('OPP_ElectrodeMap', 'TStringTag', True);
  ElectrodeCountNDX:= PublishItem('OPP_ElectrodeCount', 'TIntegerTag', True);
  ToolUsageNDX:= PublishItem('OPP_ToolUsage', 'TDoubleTag', True);
  CycleTimeNDX:= PublishItem('OPP_CycleTime', 'TIntegerTag', True);
  ToolXOffsetNDX := PublishItem('OPP_GenericToolXOffset', 'TDoubleTag', True);
  ToolYOffsetNDX := PublishItem('OPP_GenericToolYOffset', 'TDoubleTag', True);
  ToolZOffsetNDX := PublishItem('OPP_GenericToolZOffset', 'TDoubleTag', True);

end.

