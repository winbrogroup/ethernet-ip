unit NewPart;

interface

uses Windows, SysUtils, Classes, Graphics, Forms, Controls, StdCtrls,
  Buttons, ExtCtrls;

type
  TNewPartDialog = class(TForm)
    OKBtn: TButton;
    CancelBtn: TButton;
    NameIn: TEdit;
    Label1: TLabel;
    procedure OKBtnClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure CancelBtnClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    PartName : String;
  end;

var
  NewPartDialog: TNewPartDialog;

implementation

{$R *.DFM}

procedure TNewPartDialog.OKBtnClick(Sender: TObject);
begin
PartName := NameIn.Text;
ModalResult := mrOK;
Close;
end;

procedure TNewPartDialog.FormShow(Sender: TObject);
begin
NameIn.SetFocus;

end;

procedure TNewPartDialog.CancelBtnClick(Sender: TObject);
begin
PartName := '';
ModalResult := mrCancel;
close;
end;

end.
