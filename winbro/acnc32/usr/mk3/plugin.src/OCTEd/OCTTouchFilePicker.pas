unit OCTTouchFilePicker;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls,filectrl,ACNC_TouchSoftkey,Grids,stdctrls,NamedPlugin;


type
  fpDModes = (fpdDrives,fpdDirectory);
  TOPPSelectModes =(smOPP,smOCT,smPart,smDependency);

  TTouchOPPPicker = class(TCustomPanel)
  private
    { Private declarations }
    FBaseDirectory      : String;
    FFilter             : String;
    FSelectedFile       : String;
    FOnFileSelect       : TNotifyEvent;
    FOnCancel           : TNotifyEvent;
    FOnOCTSelect        : TNotifyEvent;
    FonPartSelect       : TNotifyEvent;
    FOnNewPart          : TNotifyEvent;
    FOnEmptyPartSelect  : TNotifyEvent;
    FOnSelectionChange  : TNotifyEvent;
    FSelectMode         : TOPPSelectModes;
    FSelectedPart       : String;
    FSelectedOCT        : String;
    FSelectedPartPath    : String;
    FMustSelectPart      : Boolean;
    FMustSelectOCT       : Boolean;
    FNoOcts              : Boolean;
    FPartCount           : Integer;
    FDeleteDirectoryName : String;
    FIsDelDirectory      : Boolean;
    FHighlightedFile     : String;
    FDisplayControl      : TEdit;
    FDependencyList      : TStringList;
    SelectedDependency   : String;
    IsInitialised        : Boolean;
    DownText : String; // = 'Down';
    UpText : String; //  = 'Up';
    OPenText : String; // = 'Open';
    CancelText : String; // = 'Cancel';
    NewText    : String; // = 'New';
    RootText : String; // = 'Root';
    DeleteText : String; // = 'Deleted';
    AnyFileText : String; // Any File



    DTree               : TDirectoryListBox;
    ListBox             : TFileListBox;
    DependencyBox       : TStringGrid;
    ListButtonPanel     : TPanel;
    TreeButtonPanel     : TPanel;
    DriveListBox        : TStringGrid;
    PartListBox         : TStringGrid;
    RootButton          : TTouchSoftKey;
    UpDirectoryButton   : TTouchSoftKey;
    DownDirectoryButton : TTouchSoftKey;
    UpFileButton        : TTouchSoftKey;
    DownFileButton      : TTouchSoftKey;
    OpenFileButton      : TTouchSoftKey;
    CancelButton        : TTouchSoftKey;
    NewButton           : TTouchSoftKey;

    DirectoryOpenButton : TTouchSoftKey;
    DeletedFilesButton  : TTouchSoftKey;
    AnyFileButton       : TTouchSoftKey;
    FilterButton        : TTouchSoftKey;
    DCombo              : TDriveComboBox;
    Parts               : TStringList;
    OCTs                : TStringList;
    Drives              : TStringList;
    DriveIndex          : Integer;
    DisplayMode         : fpDModes;
    SelectedRow         : Integer;
    FPartDirectory: String;


    Procedure TreeButtonClick(Sender : TObject);
    Procedure PartButtonClick(Sender : TObject);
    Procedure OCTButtonClick(Sender : TObject);
    Procedure FileButtonClick(Sender : TObject);
    procedure DependencyButtonClick(Sender : TObject);
    procedure GetDrivesList(var Drives: TStringList);
    procedure NewDirectoryEvent(Sender: TObject);
    procedure NewFileHighlighted(Sender: TObject);
    procedure DoFileSelected;
    procedure DoPartSelected;
    procedure DoNewPart;
    procedure DoEmptyPartSelected;
    procedure DoOCTSelected;
    procedure DoCancel;
    procedure MakePartList(DList : TStringList;Base : String);
    procedure MakeOCTList(DList : TStringList;Base : String);
    procedure GridDrawCell(Sender: TObject; ACol, ARow: Integer;
              Rect: TRect; State: TGridDrawState);
    procedure GridSelectCell(Sender: TObject; ACol, ARow: Integer;
                             var CanSelect: Boolean);
    procedure PrepareButtonsForOPPSelect(ClickRoutine :TNotifyEvent);
    procedure NewDependencyHighlighted(Sender: TObject);
    procedure BoxDrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure BoxSelectCell(Sender: TObject; ACol, ARow: Integer;
      var CanSelect: Boolean);

  protected
    { Protected declarations }
    procedure SetBaseDir(Dir : String);
    procedure SetPartDir(const Dir : String);
    procedure SetFilter(FString : String);
    procedure SetOnFileSelect(Value: TNotifyEvent);
    procedure SetOnPartSelect(Value: TNotifyEvent);
    procedure SetOnEmptyPartSelect(Value: TNotifyEvent);
    procedure SetOnOCTSelect(Value: TNotifyEvent);
    procedure SetOnCancel(Value: TNotifyEvent);
    procedure SetSelectMode(Mode : TOPPSelectModes);
    procedure InitforOpp;
    procedure InitforOCT;
    procedure InitforPart;
    procedure InitForDependency;


  public
    { Public declarations }
    constructor Create(AOwner:TComponent); override;
    destructor Destroy; override;
    procedure Initialise;
    procedure Update;

  published
    { Published declarations }
    property aBaseDirectory : String read FBaseDirectory write SetBaseDir;
    property PartDirectory : String read FPartDirectory write SetPartDir;
    property Filter        : String read FFilter write SetFilter;
    property Filename      : String read FSelectedFile;
    property OnFileSelect  : TNotifyEvent read FOnFileSelect write SetOnFileSelect;
    property OnCancel      : TNotifyEvent read FOnCancel     write SetOnCancel;
    property OnPartSelect  : TNotifyEvent read FOnPartSelect write SetOnPartSelect;
    property OnNewPart     : TNotifyEvent read FOnNewPart write FOnNewPart;
    property OnEmptyPartSelect  : TNotifyEvent read FOnEmptyPartSelect write SetOnEmptyPartSelect;
    property OnSelectionChange : TNotifyEvent read FOnSelectionChange write FOnSelectionChange;
    property OnOCTSelect   : TNotifyEvent read FOnOCTSelect  write SetOnOctSelect;
    property DelDirectory  : String  read FDeleteDirectoryName write FDeleteDirectoryName;
    property DelDirSelected: Boolean read FIsDelDirectory;
    property HighlightedFile : String read FHighLightedFile;
    property DisplayControl : TEdit read FDisplayControl write FDisplayControl;
    property DependencyList : TStringList read FDependencyList write FDependencyList;

    property SelectMode    : TOPPSelectModes read FSelectMode   write FSelectMode;
    property SelectedPart  : String       read FSelectedPart;
    property SelectedOct   : String       read FSelectedOCT;
    property SelectedPartPath : String    read FSelectedPartPath;
    property MustSelectPart : Boolean read FMustSelectPart write FMustSelectPart;
    property MustSelectOCT : Boolean read FMustSelectOCT write FMustSelectOCT;
    property NoOCTs        : Boolean read FNoOcts;
    property PartCount     : Integer read FPartCount;
    property Visible;

  end;

function SlashSep(const Path, S: String): String;
function RemoveTrailingSlash(const Path : String): String;

function WithQuotes(InStr : String):String;
Function StripQuotes(InStr : String):String;
function DirectoriesMatch(Path1,Path2: String):Boolean;
function SimplifyDirectory(DirStr : String) :String;


procedure Register;

implementation
constructor TTouchOPPPicker.Create(AOwner:TComponent);
var
Translator : TTranslator;
begin
inherited;
IsInitialised := False;
FIsDelDirectory := False;
FPartCount := 0;
Width := 745;
Height := 400;
FSelectedPart := '';
FSelectedOCT := '';
DTree := TDirectoryListBox.Create(Self);
ListBox := TFileListBox.Create(Self);
DependencyBox := TStringGrid.Create(Self);
DependencyBox.OnDrawCell := BoxDrawCell;
DependencyBox.OnSelectCell := BoxSelectCell;

ListBox.OnChange := NewFileHighlighted;
//DependencyBox.OnChange := NewFileHighlighted;
ListButtonPanel := TPanel.Create(Self);
TreeButtonPanel := TPanel.Create(Self);
RootButton      := TTouchSoftKey.Create(Self);
Font.Name := 'Verdana';
Font.Size := 12;
Font.Style := [fsBold];
UpDirectoryButton  := TTouchSoftKey.Create(Self);
DownDirectoryButton:= TTouchSoftKey.Create(Self);
DirectoryOpenButton:= TTouchSoftKey.Create(Self);
DeletedFilesButton := TTouchSoftKey.Create(Self);
AnyFileButton      := TTouchSoftKey.Create(Self);
FilterButton       := TTouchSoftKey.Create(Self);


UpFileButton       := TTouchSoftKey.Create(Self);
DownFileButton     := TTouchSoftKey.Create(Self);
OPenFileButton     := TTouchSoftKey.Create(Self);
CancelButton       := TTouchSoftKey.Create(Self);
NewButton          := TTouchSoftKey.Create(Self);



DCombo             := TDriveComboBox.Create(Self);
DriveListBox       := TStringGrid.Create(Self);
PartListBox        := TStringGrid.Create(Self);
Drives             := TStringList.Create;
Parts              := TStringList.Create;
OCTs               := TStringList.Create;
Visible := False;
FBaseDirectory := 'c:\';
DisplayMode := fpdDirectory;
FFilter := '*.*';

Translator := TTranslator.Create;
try
  with Translator do
    begin
    DownText := GetString('$DOWN');
    UpText := GetString('$UP');
    OPenText :=  GetString('$Open');
    CancelText :=  GetString('$Cancel');
    RootText   :=  GetString('$Root');
    DeleteText :=  GetString('$Deleted');
    AnyFileText:=  GetString('$ANYFILE');
    NewText :=  GetString('$NEW');
    end;

finally

Translator.Free;
end;

end;

destructor TTouchOPPPicker.Destroy;
begin
Drives.Free;
Parts.Free;
OCTs.Free;
inherited;
end;


procedure TTouchOPPPicker.Update;
begin
if assigned(DTree) then
   begin
   DTree.Update;
   end;
if assigned(ListBox) then
   begin
   if FSelectMode <> smDependency then
      begin
      ListBox.Update;
      end;
   if ListBox.Items.Count > 0 then ListBox.ItemIndex := 0;
   NewFileHighlighted(Self);
   end;

end;

//**********************************************
// 
procedure TTouchOPPPicker.Initialise;
begin
Visible := True;
BringToFront;

with ListButtonPanel do
     begin
     Parent := Self;
     Width := 98;
     Height := Self.Height-4;
     Top := 2;
     Left := 1;//Self.Width -100;
     Font.Name := 'Verdana';
     Font.Size := 12;
     Font.Style := [fsBold];
     Font.color := clNavy;
     Visible := True;
     end;

with TreeButtonPanel do
     begin
     Parent := Self;
     Width := 98;
     Align := alLeft;
     Font.Name := 'Verdana';
     Font.Size := 14;
     Font.Style := [fsBold];
     Font.color := clNavy;
     Visible := True;
     end;

case FSelectMode of
     smOPP: InitforOPP;
     smOCT: InitforOCT;
     smPart: InitForPart;
     smDependency : InitForDependency;
     end;
end;
//**********************************************
// Initialisation for OCT Selection
procedure TTouchOPPPicker.InitforOCT;
begin
MakeOCTList(OCTs,FSelectedPartPath);
if Octs.Count = 0 then
   begin
   FSelectedOCT := '';
   FNoOcts := True;
   DoEmptyPartSelected;
   exit;
   end;

RootButton.Visible := False;
ListBox.Visible := False;
DependencyBox.Visible := False;
//ListButtonPanel.Visible := False;
DTRee.Visible := False;
TreeButtonPanel.Visible := False;
SelectedRow := 0;
with PartListBox do
     begin
     Parent := Self;
     Height := Self.Height-4;
     Top := 2;
     Left := 100;
     Width := Self.Width-220;
     Font.Size :=12;
     Font.Style := [fsBold];
     Visible := False;
     FixedCols := 0;
     FixedRows := 0;
     ColCount := 1;
     ColWidths[0] := Self.Width-5;
     RowCount := Octs.Count;
     Cols[0] := Octs;
     Visible := True;
     OnDrawCell := GridDrawCell;
     OnSelectCell := GridSelectCell;
     end;
with UpFileButton do
     begin
     Parent := ListButtonPanel;
     Height := 50;
     Top :=  2;
     left := 2;
     Width := ListButtonPanel.Width-4;
     Legend := UpText;
     Color := clInfoBk;
     OnClick := OCTButtonClick;
     Tag := 1;
     HasIndicator := False;
     Font.Name := 'Verdana';
     Font.Size := 12;
     Font.Style := [fsBold];
     end;


with DownFileButton do
     begin
     Parent := ListButtonPanel;
     Height := 50;
     Top :=  52;
     left := 2;
     Width := ListButtonPanel.Width-4;
     Legend := DownText;
     Color := clInfoBk;
     OnClick := OCTButtonClick;
     Tag := 2;
     HasIndicator := False;
     Font.Name := 'Verdana';
     Font.Size := 12;
     Font.Style := [fsBold];
     end;


with OPenFileButton do
     begin
     Parent := ListButtonPanel;
     Height := 50;
     Top :=  102;
     left := 2;
     Width := ListButtonPanel.Width-4;
     Legend := OpenText;
     Color := clRed;
     OnClick := OCTButtonClick;
     Tag := 3;
     HasIndicator := False;
     Font.Name := 'Verdana';
     Font.Size := 12;
     Font.Style := [fsBold];
     end;

with CancelButton do
     begin
     Parent := ListButtonPanel;
     Height := 50;
     Top :=  152;
     left := 2;
     Width := ListButtonPanel.Width-4;
     Legend := CancelText;
     Color := clInfoBk;
     OnClick := OCTButtonClick;
     Tag := 4;
     HasIndicator := False;
     Font.Name := 'Verdana';
     Font.Size := 12;
     Font.Style := [fsBold];
     Enabled := not FMustSelectOCT;
     end;

  NewButton.Visible := False;

if not IsInitialised then

  begin
  SetBaseDir(FBaseDirectory);
  end;

end;

//*****************************************
// Initialisation for the Part selection. This is used when the part
// Folder selection is activated.
procedure TTouchOPPPicker.InitforPart;
begin

RootButton.Visible := False;
ListBox.Visible := False;
DependencyBox.Visible := False;

ListButtonPanel.Left := 2;
Self.Width := 410;

DTRee.Visible := False;
TreeButtonPanel.Visible := False;
MakePartList(Parts,FBaseDirectory);
SelectedRow := 0;
with PartListBox do
     begin
     Parent := Self;
     Width := 300;
     Height := Self.Height-4;
     Top := 2;
     Left := 100;
     Font.Size :=12;
     Font.Style := [fsBold];
     Visible := False;
     FixedCols := 0;
     FixedRows := 0;
     ColCount := 1;
     ColWidths[0] := 200;
     RowCount := Parts.Count;
     Cols[0] := Parts;
     Visible := True;
     OnDrawCell := GridDrawCell;
     OnSelectCell := GridSelectCell;
     RowCount := Parts.Count;
     end;

with UpFileButton do
     begin
     Parent := ListButtonPanel;
     Height := 50;
     Top :=  2;
     left := 2;
     Width := ListButtonPanel.Width-4;
     Legend := UpText;
     Color := clInfoBk;
     OnClick := PartButtonClick;
     Tag := 1;
     HasIndicator := False;
     Font.Name := 'Verdana';
     Font.Size := 12;
     Font.Style := [fsBold];
     end;


with DownFileButton do
     begin
     Parent := ListButtonPanel;
     Height := 50;
     Top :=  52;
     left := 2;
     Width := ListButtonPanel.Width-4;
     Legend := DownText;
     Color := clInfoBk;
     OnClick := PartButtonClick;
     Tag := 2;
     HasIndicator := False;
     Font.Name := 'Verdana';
     Font.Size := 12;
     Font.Style := [fsBold];
     end;


with OPenFileButton do
     begin
     Parent := ListButtonPanel;
     isEnabled := True;
     Height := 50;
     Top :=  102;
     left := 2;
     Width := ListButtonPanel.Width-4;
     Legend := OpenText;
     Color := clInfoBk;
     OPenFileButton.OnClick := PartButtonClick;
     Tag := 3;
     HasIndicator := False;
     Font.Name := 'Verdana';
     Font.Size := 12;
     Font.Style := [fsBold];
     end;

with NewButton do
     begin
     Parent := ListButtonPanel;
     Height := 50;
     Top :=  152;
     left := 2;
     Width := ListButtonPanel.Width-4;
     Legend := NewText;
     Color := clInfoBk;
     OnClick := PartButtonClick;
     Tag := 5;
     HasIndicator := False;
     Font.Name := 'Verdana';
     Font.Size := 12;
     Font.Style := [fsBold];
     Enabled := True;
     Visible := True;
     end;

with CancelButton do
     begin
     Parent := ListButtonPanel;
     Height := 50;
     Top :=  202;
     left := 2;
     Width := ListButtonPanel.Width-4;
     Legend := CancelText;
     Color := clInfoBk;
     OnClick := PartButtonClick;
     Tag := 4;
     HasIndicator := False;
     Font.Name := 'Verdana';
     Font.Size := 12;
     Font.Style := [fsBold];
     Enabled := True;
     end;


end;


//***********************************************
// Initialsation for the Dependancy selection
procedure TTouchOPPPicker.InitForDependency;
var
OPPNumber : Integer;
begin
// in this mode the filelistbox should onl show OPPs in the current Part
// except the OPP being edited
if not assigned(FDependencyList) then exit;
if DependencyList.Count < 1 then exit;
ListBox.Visible := False;
DTRee.Visible := False;
TreeButtonPanel.Visible := False;
PartListBox.Visible := False;
Self.Width := 400;
DCombo.Visible := False;
DriveListBox.Visible := False;
with DependencyBox do
     begin
     ColCount := 1;
     RowCount := FDependencyList.Count;
     FixedRows := 0;
     FixedCols := 0;
     Parent := Self;
     Height := Self.Height-4;
     Width :=  SElf.Width - 110;
     ColWidths[0] := DependencyBox.Width-2;
     Top := 2;
     Left := 1;
     for OPPNumber := 0 to FDependencyList.Count-1 do
         begin
         DependencyBox.Cells[0,OPPNumber] := FDependencyList[OPPNumber];
         end;
     SelectedDependency := DependencyBox.Cells[0,0];
     DependencyBox.Row := 0;
     NewDependencyHighlighted(Self);
     Visible := True;
     end;

ListButtonPanel.Left := Self.Width -100;
ListButtonPanel.Visible := True;
PrepareButtonsForOPPSelect(DependencyButtonClick);
end;

//*****************************************************
// Initialisation for the OPP selection.
procedure TTouchOPPPicker.InitforOpp;
var
DriveCount : Integer;
FoundDrive : Boolean;

begin
PartListBox.Visible := False;
RootButton.Visible := True;
ListBox.Visible := True;
Self.Width := 745;



//ListButtonPanel.Visible := False;
DTRee.Visible := True;
TreeButtonPanel.Visible := True;
DependencyBox.Visible := False;
ListButtonPanel.Left := Self.Width -100;
with DTree do
     begin
     Parent := Self;
     Width := 200;//(SElf.Width - 210) div 3;
     Height := Self.Height-4;
     Top := 2;
     Left := 100;
     Directory := FBaseDirectory;
     Font.Size :=12;
     Font.Style := [fsBold];
     OnChange := NewDirectoryEvent;
     //Color := clred;
     end;

with DCombo do
     begin
     Parent := Self;
     Visible := False;
     DriveCount := 0;
     FoundDrive := False;
     while  (DriveCount < DCombo.Items.Count) and (not FoundDrive) do
            begin
            if DCombo.Items[DriveCount][1] = FBaseDirectory[1] then
               begin
               FoundDrive := True;
//               ItemIndex := DriveCount;
               Drive := DCombo.Items[DriveCount][1];
               end;
            inc(DriveCount);
            end;
     end;

with ListBox do
     begin
     Parent := Self;
     Height := Self.Height-4;
     Width := (SElf.Width - 215) - DTree.Width;
     Top := 2;
     Left := DTree.Left+DTRee.Width+2;
     Visible := True;
     end;

with DriveListBox do
     begin
     Parent := Self;
     Width := 200;
     Height := Self.Height-4;
     Top := 2;
     Left := 100;
     Font.Size :=12;
     Visible := False;
     FixedCols := 0;
     FixedRows := 0;
     ColCount := 1;
     ColWidths[0] := 200;
     RowCount :=  DCombo.Items.Count;
     Cols[0] := DCombo.Items;
     end;

with RootButton do
     begin
     Parent := TreeButtonPanel;
     Top := 2;
     left := 2;
     Width := TreeButtonPanel.Width-4;
     Height := 50;
     Legend := RootText + '[' + DCombo.Drive + ']';
     Tag := 0;
     Color := clInfoBk;
     OnClick := TreeButtonClick;
     Font.Name := 'Verdana';
     Font.Size := 12;
     Font.Style := [fsBold];
     end;

with UpDirectoryButton do
     begin
     Parent := TreeButtonPanel;
     Height := 50;
     Top := 52;
     left := 2;
     Width := TreeButtonPanel.Width-4;
     Legend := UpText;
     Tag := 1;
     Color := clInfoBk;
     OnClick := TreeButtonClick;
     Font.Name := 'Verdana';
     Font.Size := 12;
     Font.Style := [fsBold];
     end;

with DownDirectoryButton do
     begin
     Parent := TreeButtonPanel;
     Height := 50;
     Top := 102;
     left := 2;
     Width := TreeButtonPanel.Width-4;
     Legend := DownText;
     Tag := 2;
     Color := clInfoBk;
     OnClick := TreeButtonClick;
     Font.Name := 'Verdana';
     Font.Size := 12;
     Font.Style := [fsBold];
     end ;

with DirectoryOpenButton do
     begin
     Parent := TreeButtonPanel;
     Height := 50;
     Top := 152;
     left := 2;
     Width := TreeButtonPanel.Width-4;
     Legend := OpenText;
     Color := clInfoBk;
     OnClick := TreeButtonClick;
     Tag := 3;
     Font.Name := 'Verdana';
     Font.Size := 12;
     Font.Style := [fsBold];
     end ;

with AnyFileButton do
     begin
     Parent := TreeButtonPanel;
     Height := 50;
     Top := 212;
     left := 2;
     Width := TreeButtonPanel.Width-4;
     Legend := AnyFileText;//'Any File';
     Color := clInfoBk;
     OnClick := TreeButtonClick;
     Tag := 4;
     HasIndicator := True;
     Indicatoron := False;
     Font.Name := 'Verdana';
     Font.Size := 12;
     Font.Style := [fsBold];
     end;

with DeletedFilesButton do
     begin
     Parent := TreeButtonPanel;
     Height := 50;
     Top := 312;
     left := 2;
     Width := TreeButtonPanel.Width-4;
     Legend := DeleteText;
     Color := clInfoBk;
     OnClick := TreeButtonClick;
     Tag := 6;
     Font.Name := 'Verdana';
     Font.Size := 12;
     Font.Style := [fsBold];
     end;

with FilterButton do
     begin
     Parent := TreeButtonPanel;
     Height := 50;
     Top := 262;
     left := 2;
     Width := TreeButtonPanel.Width-4;
     Legend := FFilter;
     Color := clInfoBk;
     OnClick := TreeButtonClick;
     Tag := 5;
     HasIndicator := True;
     IndicatorOn := True;
     ListBox.Mask := Filter;
     Self.Update;
     Font.Name := 'Verdana';
     Font.Size := 12;
     Font.Style := [fsBold];

     end;
NewButton.Visible := False;
PrepareButtonsForOPPSelect(FileButtonClick);
end;

//*************************************************************************************
// This Code is ran NOT JUST FOR OPP file selection although the name suggests that it is.
procedure TTouchOPPPicker.PrepareButtonsForOPPSelect(ClickRoutine :TNotifyEvent);
begin
with UpFileButton do
     begin
     Parent := ListButtonPanel;
     Height := 50;
     Top :=  2;
     left := 2;
     Width := ListButtonPanel.Width-4;
     Legend := UpText;
     Color := clInfoBk;
     OnClick := ClickRoutine;
     Tag := 1;
     HasIndicator := False;
     Font.Name := 'Verdana';
     Font.Size := 12;
     Font.Style := [fsBold];
     end;


with DownFileButton do
     begin
     Parent := ListButtonPanel;
     Height := 50;
     Top :=  52;
     left := 2;
     Width := ListButtonPanel.Width-4;
     Legend := DownText;
     Color := clInfoBk;
     OnClick := ClickRoutine;
     Tag := 2;
     HasIndicator := False;
     Font.Name := 'Verdana';
     Font.Size := 12;
     Font.Style := [fsBold];
     end;


with OPenFileButton do
     begin
     Parent := ListButtonPanel;
     Height := 50;
     Top :=  102;
     left := 2;
     Width := ListButtonPanel.Width-4;
     Legend := OpenText;
     Color := clInfoBk;
     OnClick := ClickRoutine;
     Tag := 3;
     HasIndicator := False;
     Font.Name := 'Verdana';
     Font.Size := 12;
     Font.Style := [fsBold];
     end;

with CancelButton do
     begin
     Parent := ListButtonPanel;
     Height := 50;
     Top :=  152;
     left := 2;
     Width := ListButtonPanel.Width-4;
     Legend := CancelText;
     Color := clInfoBk;
     OnClick := ClickRoutine;
     Tag := 4;
     HasIndicator := False;
     Font.Name := 'Verdana';
     Font.Size := 12;
     Enabled := True;
     Font.Style := [fsBold];
     end;

end;

//***************************************************************************
procedure TTouchOPPPicker.DependencyButtonClick(Sender: TObject);
begin
case TTouchSoftkey(Sender).tag of

     1:
     begin
     // File Up
     if DependencyBox.Row > 0 then
        begin
        DependencyBox.Row := DependencyBox.Row-1;
        SelectedDependency := DependencyBox.Cells[0,DependencyBox.Row];
        NewDependencyHighlighted(Self);
        end;

     end;

     2:
     begin
     // File Down
     if DependencyBox.Row < (DependencyBox.RowCount-1) then
        begin
        DependencyBox.Row := DependencyBox.Row+1;
        SelectedDependency := DependencyBox.Cells[0,DependencyBox.Row];
        NewDependencyHighlighted(Self);
        end;
     end;

     3:
     begin
     // File Open
     if SelectedDependency <> '' then
        begin
        FSelectedFile := SelectedDependency;
        DoFileSelected;
        end;
     end;

     4:
     begin
     // File Cancel
     FSelectedFile := '';
     DoCancel;
     end;
end; // case
end;


Procedure TTouchOPPPicker.FileButtonClick(Sender : TObject);
begin
case TTouchSoftkey(Sender).tag of

     1:
     begin
     // File Up
     if ListBox.ItemIndex > 0 then
        begin
        ListBox.ItemIndex := ListBox.ItemIndex-1;
        NewFileHighlighted(Self);
        end;

     end;

     2:
     begin
     // File Down
     if ListBox.ItemIndex < (ListBox.Items.count-1) then
        begin
        ListBox.ItemIndex := ListBox.ItemIndex+1;
        NewFileHighlighted(Self);
        end;
     end;

     3:
     begin
     // File Open
     if ListBox.Filename <> '' then
        begin
        FSelectedFile := ListBox.Filename;
        DoFileSelected;
        end;
     end;

     4:
     begin
     // File Cancel
     FSelectedFile := '';
     DoCancel;
     end;
end; // case
end;


Procedure TTouchOPPPicker.OCTButtonClick(Sender : TObject);
begin
case TTouchSoftkey(Sender).tag of
     1:
     begin
     //UPPart
     if SelectedRow > 0 then
        begin
        SelectedRow := SelectedRow-1;
        PartListBox.Row := SelectedRow;
        end;
     end;

     2:
     begin
     //Down Part
     if SelectedRow < PartListBox.RowCount-1 then
        begin
        SelectedRow := SelectedRow+1;
        PartListBox.Row := SelectedRow;
        end;
     end;

     3:
     begin
     // Select OCT
     FSelectedOCT := PartListBox.Cells[0,SelectedRow];
     DoOCtSelected;
     end;

     4:
     begin
     // Cancel Part
     DoCancel;
     end;


end;


end;

Procedure TTouchOPPPicker.PartButtonClick(Sender : TObject);
begin
case TTouchSoftkey(Sender).tag of
     1:
     begin
     //UPPart
     if SelectedRow > 0 then
        begin
        SelectedRow := SelectedRow-1;
        PartListBox.Row := SelectedRow;                     
        end;
     end;
     2:
     begin
     //Down Part
     if SelectedRow < PartListBox.RowCount-1 then
        begin
        SelectedRow := SelectedRow+1;
        PartListBox.Row := SelectedRow;
        end;
     end;

     3:
     begin
     FSelectedPart := PartListBox.Cells[0,SelectedRow];
     FSelectedPartPath := SlashSep(FBaseDirectory,FSelectedPart);
        begin
        FSelectedOCT := '';
        MakeOCTList(OCTs,FSelectedPartPath);
        if Octs.Count = 0 then
           begin
           FSelectedOCT := '';
           FNoOcts := True;
           DoEmptyPartSelected;
           exit;
           end
        else
            begin
            FSelectedOCT := OCTs[0];
            FNoOcts := False;
            end;
        DoPartSelected;
        end
     end;


     4 :
     begin
     // Cancel Part
     if FMustSelectPart then
        begin
        DoCancel;
        end
     else
         begin
         DoCancel;
         end;
     end;

     5:
     begin
     // New Part
     DoNewPart;
     end;

end; // case
end;

Procedure TTouchOPPPicker.TreeButtonClick(Sender : TObject);
var
DriveName : String;
begin
case TTouchSoftkey(Sender).tag of
     0:
     begin
     // RootButtom Processing
     case DisplayMode of
          fpdDirectory:
            begin
            DTree.ItemIndex := 0;
            DTree.OpenCurrent;
            end;
          fpdDrives:
            begin
            end;
     end;
     end;

     1:
     begin
     //UpButton Processing
     case DisplayMode of
          fpdDirectory:
            begin
            if DTree.ItemIndex > 0 then
               begin
               DTree.ItemIndex := DTree.ItemIndex-1;
               //DTree.OpenCurrent;
               end
            else
                begin
                DTree.Visible := False;
                DriveListBox.Visible := True;
                DisplayMode := fpdDrives;
                RootButton.Enabled := False;
                RootButton.Legend := '';
                end;
            end;

          fpdDrives:
            begin
            if DriveListBox.Row > 0 then
               DriveListBox.Row := DriveListBox.Row-1;
            end;
     end;

     end;

     2:
     begin
     //DownButton Processing
     case DisplayMode of
          fpdDirectory:
            begin
            DTree.ItemIndex := DTree.ItemIndex+1;
            end;
          fpdDrives:
            begin
            if DriveListBox.Row < (DriveListBox.RowCount-1) then
               DriveListBox.Row := DriveListBox.Row+1;
            end;
     end;
     end;

     3:
     begin
     //OpenButton Processing
     case DisplayMode of
          fpdDirectory:
            begin
            DTree.OpenCurrent;
            end;
          fpdDrives:
            begin
            DriveIndex := DriveListBox.Row;
            DriveName :=  DriveListBox.Cells[0,DriveIndex];
            try
            DTree.Directory := DriveName[1]+':\';
            DriveListBox.Visible := False;
            DTree.Visible := True;
            DisplayMode := fpdDirectory;
            RootButton.Enabled := True;
            RootButton.Legend := RootText + '[' + DriveName[1] + ']';;
            except
            DTree.Directory := 'C:\';
            DriveListBox.Visible := False;
            DTree.Visible := True;
            DisplayMode := fpdDirectory;
            RootButton.Enabled := True;
            RootButton.Legend := RootText + '[C:\]';;
            end;
            end;
     end;
     end;

     4:
     begin
     // AnyFile Button
     ListBox.Mask := '*.*';
     Update;
     AnyFileButton.IndicatorOn := True;
     FilterButton.IndicatorOn := False;
     ListBox.ItemIndex := 0;
     end;

     5:
     begin
     // Filter Button
     ListBox.Mask := FFilter;
     Update;
     AnyFileButton.IndicatorOn := False;
     FilterButton.IndicatorOn := True;
     ListBox.ItemIndex := 0;
     end;

     6:
     begin
     // go to deleted directory Button
     if DirectoryExists(FDeleteDirectoryName) then
        DTree.Directory := FDeleteDirectoryName;
     end;

end; // case
end;



procedure TTouchOPPPicker.SetBaseDir(Dir : String);
begin
FBaseDirectory := Dir;
if not (handleAllocated) then exit;

if assigned(DTree) then
   begin

   DTree.Directory := Dir;
   Invalidate;
   end;
if assigned(ListBox) then
   begin
   ListBox.Directory := Dir;
   ListBox.Update;
   if ListBox.Items.Count > 0 then
      begin
      ListBox.ItemIndex := 0;
      end;
   IsInitialised := True;
   Invalidate;
   end;

end;

procedure TTouchOPPPicker.SetFilter(FString : String);
begin
FFilter := FString;
end;


procedure TTouchOPPPicker.GetDrivesList(var Drives: TStringList);
var
Buffer : array [0..100] of Char;
BCount : Integer;
NPos   : Integer;
StringBuffer : String;
EndofData    : Boolean;

begin
if not assigned(Drives) then exit;
GetLogicalDriveStrings(100,Buffer);
Drives.Clear;
BCount := 0;
NPos := 0;
EndofData := False;
StringBuffer := '';
while (BCount < 100) and not EndofData do
      begin
      While Buffer[BCount+Npos] <>  #0  do
            begin
            StringBuffer:= StringBuffer+ Buffer[BCount+Npos];;
            inc(NPos);
            end;
      Drives.Add(StringBuffer);
      StringBuffer := '';
      inc(NPos);
      if Buffer[BCount+Npos] = #0 then
         begin
         EndofData := True;
         end
      else
          begin
          BCount := BCount+NPos;
          Npos := 0;
          end;


      end;

end;


procedure TTouchOPPPicker.NewDirectoryEvent(Sender: TObject);

begin
  ListBox.Directory := DTree.Directory;
  ListBox.ItemIndex := 0;

  if DirectoriesMatch(DTree.Directory,FDeleteDirectoryName) then
     begin
     FIsDelDirectory := True;
     end
  else
     begin
     FIsDelDirectory := False;
     end;


end;

procedure TTouchOPPPicker.SetSelectMode(Mode : TOPPSelectModes);
begin
FSelectMode := Mode;
end;


procedure TTouchOPPPicker.SetOnCancel(Value: TNotifyEvent);
begin
FOnCancel := Value;
end;

procedure TTouchOPPPicker.SetOnFileSelect(Value: TNotifyEvent);
begin
FOnFileSelect := Value;
end;

procedure TTouchOPPPicker.SetOnPartSelect(Value: TNotifyEvent);
begin
FOnPartSelect := Value;
end;

procedure TTouchOPPPicker.SetOnEmptyPartSelect(Value: TNotifyEvent);
begin
FOnEmptyPartSelect := Value;
end;

procedure TTouchOPPPicker.SetOnOCTSelect(Value: TNotifyEvent);
begin
FOnOCTSelect := Value;
end;

procedure TTouchOPPPicker.DoFileSelected;
begin
if assigned(FOnFileSelect) then FOnFileSelect(Self);
end;


procedure TTouchOPPPicker.DoNewPart;
Var
NewName : String;
begin
if assigned(FOnNewPart) then FOnNewPart(Self);
end;

procedure TTouchOPPPicker.DoPartSelected;
begin

if assigned(FOnPartSelect) then FOnPartSelect(Self);
end;

procedure TTouchOPPPicker.DoEmptyPartSelected;
begin
if assigned(FOnEmptyPartSelect) then FOnEmptyPartSelect(Self);
end;

procedure TTouchOPPPicker.DoOCTSelected;
begin
if assigned(FOnOCTSelect) then FOnOCTSelect(Self);
end;


procedure TTouchOPPPicker.DoCancel;
begin
if assigned(FOnCancel) then FOnCancel(Self);
end;

procedure TTouchOPPPicker.MakeOCTList (DList : TStringList;Base : String);
var
  sr: TSearchRec;
  FileAttrs: Integer;
begin
 DList.Clear;
 FileAttrs := faAnyFile;
 if FindFirst(SlashSep(Base,'*.oct'), FileAttrs, sr) = 0 then
    begin
    Dlist.Add(sr.name);
    while FindNext(sr) = 0 do
        begin
        if (sr.Attr and FileAttrs) = sr.Attr then
           begin
           Dlist.Add(sr.name);
           end;
        end;
  end;
end;

procedure TTouchOPPPicker.MakePartList(DList : TStringList;Base : String);
var
  sr: TSearchRec;
  FileAttrs: Integer;
begin
 DList.Clear;
 FileAttrs := faDirectory;
 if FindFirst(SlashSep(Base,'*.*'), FileAttrs, sr) = 0 then
  begin
  if (sr.name <> '.') and (sr.name <> '..') then
     begin
     Dlist.Add(sr.name);
     end;
  while FindNext(sr) = 0 do
        begin
        if (sr.Attr and FileAttrs) = sr.Attr then
           begin
           if (sr.name <> '.') and (sr.name <> '..') then
              begin
              Dlist.Add(sr.name);
              end;
           end;

        end;
  end;
FPartCount := Dlist.Count;
end;

procedure TTouchOPPPicker.GridSelectCell(Sender: TObject; ACol, ARow: Integer;
  var CanSelect: Boolean);
begin
SelectedRow :=Arow;
PartListBox.Invalidate;
end;

procedure TTouchOPPPicker.GridDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
var
Contents : String;
begin
Contents := PartListBox.Cells[ACol,Arow];

with PartListBox.Canvas do
     begin
     if ARow = SelectedRow then
            begin
            Brush.Color := clYellow;
            Font.Color := clMaroon;
            end
     else
             begin
             Brush.Color := clWhite;
             Font.Color := clBlack;
             end;
     FillRect(Rect);
     if ACol = 2 then
        begin
        TextOut(Rect.Left+30,Rect.Top,Contents);
        end
     else
        begin
        TextOut(Rect.Left+3,Rect.Top,Contents);
        end;
end;
end;

function RemoveTrailingSlash(const Path : String): String;
begin
  if AnsiLastChar(Path)^ = '\' then
     Result := Copy(Path,1,Length(Path)-1)
  else
    Result := Path;
end;


function SlashSep(const Path, S: String): String;
begin
  if Path = '' then
     begin
     Result := '\';
     exit;
     end;
  if AnsiLastChar(Path)^ <> '\' then
    Result := Path + '\' + S
  else
    Result := Path + S;
end;

function WithQuotes(InStr : String):String;
var
SLength : Integer;
begin
Result := InStr;
if InStr = '' then exit;
Slength := Length(InStr);
if Result[Slength] <> '''' then
   begin
   Result  := Instr + '''';
   end;
if Result[1] <> '''' then
   begin
   Result := '''' + Result ;
   end;
end;

Function StripQuotes(InStr : String):String;
var
SLength : Integer;
Temp    : String;
begin
Temp := InStr;
Result := Instr;
if InStr = '' then exit;
Temp := trim(Temp);
Slength := Length(Temp);
if Temp[Slength] = '''' then
   begin
   Result  := Copy(Temp,1,Slength-1);
   end;
Temp := Result;
if Result[1] = '''' then
   begin
   Temp := Copy(Result,2,Slength);
   end;
Result := Temp;
end;



function SimplifyDirectory(DirStr : String) :String;
var
ddPos : Integer;
StartPos : INteger;
SlashCount : Integer;
Found : Boolean;
Buffer : String;
begin
Buffer := DirStr;
Result := DirStr;
Found := True;
while Found do
  begin
  ddpos := Pos('..',Buffer);
  Found := ddPOs > 0;
  if Found Then
    begin
    SlashCount := 0;
    StartPOs := ddpos-1;
    while (ddpos > 0) and (SlashCount < 2) do
      begin
      if Buffer[StartPos] = '\' then inc(SlashCount);
      dec(StartPos)
      end;
    if SlashCount = 2 then
      begin
      Result := Copy(Buffer,1,StartPos);
      Result := Result+Copy(Buffer,ddpos+2);
      Buffer := Result;
      end;
    end
  end;
end;

function DirectoriesMatch(Path1,Path2: String):Boolean;
var
P1,P2 : String;
begin
P1 := SlashSep(Path1,'');
P2 := SlashSep(Path2,'');
P1 := SimplifyDirectory(P1);
P2 := SimplifyDirectory(P2);
Result := False;

if Comparetext(P1,P2) = 0 then
   begin
   Result := True;
   end;
end;


procedure Register;
begin
  RegisterComponents('ACNCComps', [TTouchOPPPicker]);
end;

procedure TTouchOPPPicker.NewFileHighlighted(Sender: TObject);
begin
if  (FSelectMode = smOPP) or (FSelectMode = smDependency) then
    begin
    FHighlightedFile := ListBox.FileName;
    end
else
    begin
    FHighlightedFile := '';
    end;

if assigned(FDisplayControl) then FDisplayControl.Text := FHighlightedFile;
if assigned(FOnSelectionChange) then
   begin
   FonSelectionChange(Self);
   end;
end;

procedure TTouchOPPPicker.NewDependencyHighlighted(Sender: TObject);
begin
if assigned(FDisplayControl) then FDisplayControl.Text := SelectedDependency;
end;


procedure TTouchOPPPicker.BoxDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);

var
Contents : String;
begin
Contents := DependencyBox.Cells[ACol,Arow];

with DependencyBox.Canvas do
     begin
     if ARow = DependencyBox.Row then
            begin
            Brush.Color := clYellow;
            Font.Color := clMaroon;
            end
     else
             begin
             Brush.Color := clWhite;
             Font.Color := clBlack;
             end;
     FillRect(Rect);
     if ACol = 2 then
        begin
        TextOut(Rect.Left+30,Rect.Top+4,Contents);
        end
     else
        begin
        TextOut(Rect.Left+3,Rect.Top+4,Contents);
        end;
end;
end;


procedure TTouchOPPPicker.BoxSelectCell(Sender: TObject; ACol, ARow: Integer;
  var CanSelect: Boolean);
begin
SelectedDependency := DependencyBox.Cells[0,Arow];
NewDependencyHighlighted(Self);
end;




procedure TTouchOPPPicker.SetPartDir(const Dir: String);
begin
  FPartDirectory := Dir;
  if not (handleAllocated) then exit;

 if assigned(DTree) then
    begin

   DTree.Directory := Dir;
   Invalidate;
   end;
 if assigned(ListBox) then
   begin
   ListBox.Directory := Dir;
   ListBox.Update;
   if ListBox.Items.Count > 0 then
      begin
      ListBox.ItemIndex := 0;
      end;
   IsInitialised := True;
   Invalidate;
   end;
end;


end.
