object AOCTForm: TAOCTForm
  Left = 30
  Top = 16
  BorderIcons = []
  BorderStyle = bsNone
  Caption = 'OCT Editor'
  ClientHeight = 613
  ClientWidth = 1014
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object MainKB: TBaseKB
    Left = 0
    Top = 440
    Width = 633
    Height = 169
    BevelOuter = bvNone
    Caption = 'MainKB'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    KeyboardMode = kbmAlphaNumeric
    HighlightColor = clBlack
    Options = []
    SecondaryFont.Charset = DEFAULT_CHARSET
    SecondaryFont.Color = clWindowText
    SecondaryFont.Height = -11
    SecondaryFont.Name = 'MS Sans Serif'
    SecondaryFont.Style = []
    RepeatDelay = 500
    ExternalMode = False
    FocusByHandle = False
    FocusHandle = 0
  end
  object KPad: TBaseFPad
    Left = 640
    Top = 440
    Width = 200
    Height = 169
    ExternalMode = False
    ChangeCursorKeys = True
    EnableCursorKeys = False
  end
  object MainPanel: TPanel
    Left = 0
    Top = 0
    Width = 1014
    Height = 340
    Align = alTop
    BevelInner = bvLowered
    BevelWidth = 2
    TabOrder = 2
    object PartTypePanel: TPanel
      Left = 4
      Top = 4
      Width = 1006
      Height = 41
      Align = alTop
      BevelInner = bvLowered
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Comic Sans MS'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object CPartLabel: TLabel
        Left = 8
        Top = 8
        Width = 122
        Height = 18
        Caption = 'Current Part :'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object OCTLabel: TLabel
        Left = 488
        Top = 8
        Width = 44
        Height = 18
        Caption = 'OCT:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label2: TLabel
        Left = 832
        Top = 8
        Width = 82
        Height = 18
        Caption = 'No. OCTs'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object PartNameDisplay: TEdit
        Left = 136
        Top = 4
        Width = 300
        Height = 26
        TabStop = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        Text = 'PartNameDisplay'
      end
      object OCTNameDisplay: TEdit
        Left = 540
        Top = 4
        Width = 280
        Height = 26
        TabStop = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 1
        Text = 'OCTNameDisplay'
      end
      object OCTCountDisplay: TEdit
        Left = 920
        Top = 4
        Width = 70
        Height = 26
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
        TabOrder = 2
      end
    end
    object CursorPanel: TPanel
      Left = 890
      Top = 45
      Width = 120
      Height = 291
      Align = alRight
      BevelInner = bvLowered
      TabOrder = 1
      object CUpKey: TBitBtn
        Left = 8
        Top = 8
        Width = 110
        Height = 50
        Caption = '  Up  '
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlue
        Font.Height = -24
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        OnClick = CUpKeyClick
        Glyph.Data = {
          76040000424D7604000000000000760000002800000040000000200000000100
          0400000000000004000000000000000000001000000010000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
          33333333333333333333333333333333333333333333333333333CCC33333333
          33333333333337744433773333333333333333333333337777333CCCCC333333
          33333333333374444433777777333333333333333333777777F33CCCCCC33333
          33333333337744444433777777333333333333333333777777F33CCCCCCCC333
          333333337744444447333777F77733333333333333773377FFF33CCCCCCCCC33
          333333377444444447333777FF7733333333333337773377FFF333CCCCCCCCCC
          333337744444444473333377FF337777333333777733FF77FFF333CCCCCCCCCC
          CC3373444444444473333377FF337777333333777733FF77FFF3333CCCCCCCCC
          CCC444444444444733333337773333FF77777733FFFF7737FF33333CCCCCCCCC
          CCC444444444444733333337773333FF77777733FFFF7773FF333333CCCCCCCC
          CCC44444444444733333333377FF33333377FFFF3333777FF3333333CCCCCCCC
          CCC44444444444733333333377FF33333377FFFF333377FFF33333333CCCCCCC
          CCC444444444473333333333377733333377FF33337737FF333333333CCCCCCC
          CCC444444444473333333333377733333377FF33337773FF3333333333CCCCCC
          CCC4444444447333333333333377FF333377FF3333777FF33333333333CCCCCC
          CCC4444444447333333333333377FF333377FF333377FF3333333333333CCCCC
          CCC444444447333333333333333777333377FF337773FF3333333333333CCCCC
          CCC444444447333333333333333777333377FF337773FF33333333333333CCCC
          CCC444444473333333333333333377FF3377FF3377FFF333333333333333CCCC
          CCC444444473333333333333333377FF3377FF3377FF33333333333333333CCC
          CCC444444733333333333333333337773377FF7773FF33333333333333333CCC
          CCC444444733333333333333333333773377FF7733FF333333333333333333CC
          CCC44444733333333333333333333377FF77FF77FF33333333333333333333CC
          CCC44444733333333333333333333377FF77FF77FF333333333333333333333C
          CCC4444733333333333333333333333777337773FF333333333333333333333C
          CCC444473333333333333333333333337733773FFF3333333333333333333333
          CCC44473333333333333333333333333773377FF333333333333333333333333
          CCC44473333333333333333333333333773377FF333333333333333333333333
          3CC447333333333333333333333333333777FFF3333333333333333333333333
          3CC447333333333333333333333333333377FF33333333333333333333333333
          33C473333333333333333333333333333377FF33333333333333333333333333
          3333333333333333333333333333333333333333333333333333}
        NumGlyphs = 2
      end
      object CDownKey: TBitBtn
        Left = 8
        Top = 64
        Width = 110
        Height = 50
        Caption = 'Down'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlue
        Font.Height = -24
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
        TabOrder = 1
        OnClick = CDownKeyClick
        Glyph.Data = {
          76040000424D7604000000000000760000002800000040000000200000000100
          0400000000000004000000000000000000001000000010000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
          3333333333333333333333333333333333333333333333333333333333333333
          33C473333333333333333333333333333377FF33333333333333333333333333
          3CC447333333333333333333333333333377FF33333333333333333333333333
          3CC447333333333333333333333333333777FFF3333333333333333333333333
          CCC44473333333333333333333333333773377FF333333333333333333333333
          CCC44473333333333333333333333333773377FF33333333333333333333333C
          CCC444473333333333333333333333337733773FFF333333333333333333333C
          CCC4444733333333333333333333333777337773FF33333333333333333333CC
          CCC44444733333333333333333333377FF77FF77FF33333333333333333333CC
          CCC44444733333333333333333333377FF77FF77FF3333333333333333333CCC
          CCC444444733333333333333333333773377FF7733FF33333333333333333CCC
          CCC444444733333333333333333337773377FF7773FF3333333333333333CCCC
          CCC444444473333333333333333377FF3377FF3377FF3333333333333333CCCC
          CCC444444473333333333333333377FF3377FF3377FFF33333333333333CCCCC
          CCC444444447333333333333333777333377FF337773FF3333333333333CCCCC
          CCC444444447333333333333333777333377FF337773FF333333333333CCCCCC
          CCC4444444447333333333333377FF333377FF333377FF333333333333CCCCCC
          CCC4444444447333333333333377FF333377FF3333777FF3333333333CCCCCCC
          CCC444444444473333333333377733333377FF33337773FF333333333CCCCCCC
          CCC444444444473333333333377733333377FF33337737FF33333333CCCCCCCC
          CCC44444444444733333333377FF33333377FFFF333377FFF3333333CCCCCCCC
          CCC44444444444733333333377FF33333377FFFF3333777FF333333CCCCCCCCC
          CCC444444444444733333337773333FF77777733FFFF7773FF33333CCCCCCCCC
          CCC444444444444733333337773333FF77777733FFFF7737FF3333CCCCCCCCCC
          CC3373444444444473333377FF337777333333777733FF77FFF333CCCCCCCCCC
          333337744444444473333377FF337777333333777733FF77FFF33CCCCCCCCC33
          333333377444444447333777FF7733333333333337773377FFF33CCCCCCCC333
          333333337744444447333777F77733333333333333773377FFF33CCCCCC33333
          33333333337744444433777777333333333333333333777777F33CCCCC333333
          33333333333374444433777777333333333333333333777777F33CCC33333333
          3333333333333774443377333333333333333333333333777733333333333333
          3333333333333333333333333333333333333333333333333333}
        NumGlyphs = 2
      end
      object OKButton: TButton
        Left = 2
        Top = 232
        Width = 114
        Height = 50
        Caption = 'EXIT'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -24
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
        TabOrder = 2
        OnClick = OKButtonClick
      end
      object DeleteUnusedButton: TTouchSoftkey
        Left = 2
        Top = 176
        Width = 114
        Height = 50
        Color = clOlive
        Font.Charset = ANSI_CHARSET
        Font.Color = clWhite
        Font.Height = -19
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 3
        Visible = False
        OnClick = DeleteUnusedButtonClick
        Legend = 'Delete~Unused OPPs'
        TextSize = 14
        HasIndicator = False
        IndicatorOn = False
        IsEnabled = True
        ButtonStyle = skNormal
        ArrowColor = clBlack
        ButtonColor = clRed
        LegendColor = clWhite
        IndicatorWidth = 20
        IndicatorHeight = 10
        IndicatorOnColor = clGreen
        IndicatorOffColor = clSilver
        MaxFontSize = 0
      end
      object DeleteEmptyOCTButton: TTouchSoftkey
        Left = 2
        Top = 128
        Width = 114
        Height = 50
        Color = clOlive
        Font.Charset = ANSI_CHARSET
        Font.Color = clWhite
        Font.Height = -19
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 4
        Visible = False
        OnClick = DeleteEmptyOCTButtonClick
        Legend = 'Delete~Empty OCTs'
        TextSize = 14
        HasIndicator = False
        IndicatorOn = False
        IsEnabled = True
        ButtonStyle = skNormal
        ArrowColor = clBlack
        ButtonColor = clRed
        LegendColor = clWhite
        IndicatorWidth = 20
        IndicatorHeight = 10
        IndicatorOnColor = clGreen
        IndicatorOffColor = clSilver
        MaxFontSize = 0
      end
    end
    object MainPages: TPageControl
      Left = 4
      Top = 45
      Width = 886
      Height = 291
      ActivePage = GridSheet
      Align = alClient
      TabOrder = 2
      object GridSheet: TTabSheet
        Caption = 'Grid'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabVisible = False
        object Grid: TStringGrid
          Left = 0
          Top = 0
          Width = 878
          Height = 281
          Align = alClient
          ColCount = 3
          DefaultDrawing = False
          FixedCols = 0
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Verdana'
          Font.Style = [fsBold]
          ParentFont = False
          ScrollBars = ssNone
          TabOrder = 0
          OnDrawCell = GridDrawCell
          OnSelectCell = GridSelectCell
        end
      end
      object ReportSheet: TTabSheet
        Caption = 'Report'
        ImageIndex = 1
        TabVisible = False
        object ReportM: TMemo
          Left = 0
          Top = 0
          Width = 878
          Height = 281
          Align = alClient
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Courier New'
          Font.Style = []
          Lines.Strings = (
            'ReportM')
          ParentFont = False
          TabOrder = 0
        end
      end
    end
  end
  object SoftKeyPanel: TPanel
    Left = 0
    Top = 340
    Width = 1014
    Height = 64
    Align = alTop
    BevelInner = bvLowered
    BevelWidth = 2
    TabOrder = 3
    object PartEditKey: TTouchSoftkey
      Left = 896
      Top = 2
      Width = 100
      Height = 55
      Color = clWhite
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -19
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      OnClick = PartEditKeyClick
      Legend = 'Part~Edit'
      TextSize = 14
      HasIndicator = True
      IndicatorOn = False
      IsEnabled = True
      ButtonStyle = skNormal
      ArrowColor = clBlack
      ButtonColor = clWhite
      LegendColor = clBlack
      IndicatorWidth = 20
      IndicatorHeight = 10
      IndicatorOnColor = clGreen
      IndicatorOffColor = clSilver
      MaxFontSize = 0
    end
    object OCTEditKey: TTouchSoftkey
      Left = 792
      Top = 1
      Width = 100
      Height = 55
      Color = clWhite
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -19
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
      OnClick = OCTEditKeyClick
      Legend = 'OCT~Edit'
      TextSize = 14
      HasIndicator = True
      IndicatorOn = False
      IsEnabled = True
      ButtonStyle = skNormal
      ArrowColor = clBlack
      ButtonColor = clWhite
      LegendColor = clBlack
      IndicatorWidth = 20
      IndicatorHeight = 10
      IndicatorOnColor = clGreen
      IndicatorOffColor = clSilver
      MaxFontSize = 0
    end
  end
  object PartKeyPanel: TPanel
    Left = 16
    Top = 368
    Width = 777
    Height = 50
    BevelOuter = bvNone
    TabOrder = 5
    object CheckIntegrityKey: TTouchSoftkey
      Left = 642
      Top = 0
      Width = 100
      Height = 50
      Color = clOlive
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -19
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 2
      OnClick = CheckIntegrityKeyClick
      Legend = 'Check~Integrity'
      TextSize = 14
      HasIndicator = False
      IndicatorOn = False
      IsEnabled = True
      ButtonStyle = skNormal
      ArrowColor = clBlack
      ButtonColor = clWhite
      LegendColor = clBlack
      IndicatorWidth = 20
      IndicatorHeight = 10
      IndicatorOnColor = clGreen
      IndicatorOffColor = clSilver
      MaxFontSize = 0
    end
    object ADDOCTKey: TTouchSoftkey
      Left = 538
      Top = 0
      Width = 100
      Height = 50
      Color = clOlive
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -19
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 5
      OnClick = ADDOCTKeyClick
      Legend = 'Add~OCT'
      TextSize = 14
      HasIndicator = False
      IndicatorOn = False
      IsEnabled = True
      ButtonStyle = skNormal
      ArrowColor = clBlack
      ButtonColor = clWhite
      LegendColor = clBlack
      IndicatorWidth = 20
      IndicatorHeight = 10
      IndicatorOnColor = clGreen
      IndicatorOffColor = clSilver
      MaxFontSize = 0
    end
    object CopyNewPartKey: TTouchSoftkey
      Left = 218
      Top = 0
      Width = 100
      Height = 50
      Color = clOlive
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -19
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      OnClick = CopyNewPartKeyClick
      Legend = 'Copy PART~To New PART'
      TextSize = 14
      HasIndicator = False
      IndicatorOn = False
      IsEnabled = True
      ButtonStyle = skNormal
      ArrowColor = clBlack
      ButtonColor = clWhite
      LegendColor = clBlack
      IndicatorWidth = 20
      IndicatorHeight = 10
      IndicatorOnColor = clGreen
      IndicatorOffColor = clSilver
      MaxFontSize = 0
    end
    object DeletePartKey: TTouchSoftkey
      Left = 322
      Top = 0
      Width = 100
      Height = 50
      Color = clOlive
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -19
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
      OnClick = DeletePartKeyClick
      Legend = 'Delete~PART'
      TextSize = 14
      HasIndicator = False
      IndicatorOn = False
      IsEnabled = True
      ButtonStyle = skNormal
      ArrowColor = clBlack
      ButtonColor = clWhite
      LegendColor = clBlack
      IndicatorWidth = 20
      IndicatorHeight = 10
      IndicatorOnColor = clGreen
      IndicatorOffColor = clSilver
      MaxFontSize = 0
    end
    object NewPartKey: TTouchSoftkey
      Left = 106
      Top = 0
      Width = 100
      Height = 50
      Color = clOlive
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -19
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 3
      OnClick = NewPartKeyClick
      Legend = 'Make New~PART'
      TextSize = 14
      HasIndicator = False
      IndicatorOn = False
      IsEnabled = True
      ButtonStyle = skNormal
      ArrowColor = clBlack
      ButtonColor = clWhite
      LegendColor = clBlack
      IndicatorWidth = 20
      IndicatorHeight = 10
      IndicatorOnColor = clGreen
      IndicatorOffColor = clSilver
      MaxFontSize = 0
    end
    object ChangePartKey: TTouchSoftkey
      Left = 2
      Top = 0
      Width = 100
      Height = 50
      Color = clOlive
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -19
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 4
      OnClick = ChangePartKeyClick
      Legend = 'Change~PART'
      TextSize = 14
      HasIndicator = False
      IndicatorOn = False
      IsEnabled = True
      ButtonStyle = skNormal
      ArrowColor = clBlack
      ButtonColor = clWhite
      LegendColor = clBlack
      IndicatorWidth = 20
      IndicatorHeight = 10
      IndicatorOnColor = clGreen
      IndicatorOffColor = clSilver
      MaxFontSize = 0
    end
    object DeleteOCTKey: TTouchSoftkey
      Left = 426
      Top = 0
      Width = 100
      Height = 50
      Color = clOlive
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -19
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 6
      OnClick = DeleteOCTKeyClick
      Legend = 'Delete~OCT'
      TextSize = 14
      HasIndicator = False
      IndicatorOn = False
      IsEnabled = True
      ButtonStyle = skNormal
      ArrowColor = clBlack
      ButtonColor = clWhite
      LegendColor = clBlack
      IndicatorWidth = 20
      IndicatorHeight = 10
      IndicatorOnColor = clGreen
      IndicatorOffColor = clSilver
      MaxFontSize = 0
    end
    object RenameOCTKey: TTouchSoftkey
      Left = 594
      Top = 0
      Width = 100
      Height = 50
      Color = clInfoBk
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -19
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 7
      OnClick = RenameOCTKeyClick
      Legend = 'Rename~OCT'
      TextSize = 14
      HasIndicator = False
      IndicatorOn = False
      IsEnabled = True
      ButtonStyle = skNormal
      ArrowColor = clBlack
      ButtonColor = clWhite
      LegendColor = clBlack
      IndicatorWidth = 20
      IndicatorHeight = 10
      IndicatorOnColor = clGreen
      IndicatorOffColor = clSilver
      MaxFontSize = 0
    end
  end
  object MainDialoguePanel: TPanel
    Left = 176
    Top = 98
    Width = 499
    Height = 220
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Comic Sans MS'
    Font.Style = []
    ParentFont = False
    TabOrder = 6
    Visible = False
    object DialogPages: TPageControl
      Left = 1
      Top = 1
      Width = 497
      Height = 218
      ActivePage = RenameOCTDialog
      Align = alClient
      TabHeight = 12
      TabOrder = 0
      object RenameOCTDialog: TTabSheet
        Caption = 'RenameOCTDialog'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        ParentFont = False
        TabVisible = False
        object RenameMessage: TLabel
          Left = 11
          Top = 24
          Width = 73
          Height = 18
          Caption = 'Rename'
        end
        object ReNameEdit: TEdit
          Left = 8
          Top = 64
          Width = 449
          Height = 26
          TabOrder = 0
        end
        object CopyOKButton: TButton
          Left = 310
          Top = 105
          Width = 85
          Height = 45
          Caption = 'OK'
          TabOrder = 1
          OnClick = CopyOKButtonClick
        end
        object CopyCancelButton: TButton
          Left = 398
          Top = 105
          Width = 85
          Height = 45
          Caption = 'Cancel'
          TabOrder = 2
          OnClick = CopyCancelButtonClick
        end
      end
      object DeleteOCTDialog: TTabSheet
        Caption = 'DeleteOCTDialog'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        ImageIndex = 1
        ParentFont = False
        TabVisible = False
        object Panel1: TPanel
          Left = 0
          Top = 0
          Width = 489
          Height = 208
          Align = alClient
          BevelInner = bvLowered
          Color = clRed
          TabOrder = 0
          object DeleteTextLabel: TLabel
            Left = 19
            Top = 16
            Width = 157
            Height = 18
            Caption = 'variable text here'
            Font.Charset = ANSI_CHARSET
            Font.Color = clYellow
            Font.Height = -16
            Font.Name = 'Verdana'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object NBLabel1: TLabel
            Left = 19
            Top = 56
            Width = 128
            Height = 18
            Caption = 'LanguageText'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -16
            Font.Name = 'Verdana'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object NBLabel2: TLabel
            Left = 19
            Top = 80
            Width = 128
            Height = 18
            Caption = 'LanguageText'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -16
            Font.Name = 'Verdana'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object DeleteOCTOKButton: TButton
            Left = 304
            Top = 129
            Width = 85
            Height = 45
            Caption = 'OK'
            Font.Charset = ANSI_CHARSET
            Font.Color = clNavy
            Font.Height = -16
            Font.Name = 'Verdana'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 0
            OnClick = DeleteOCTOKButtonClick
          end
          object DeleteOCTCancelButton: TButton
            Left = 392
            Top = 129
            Width = 85
            Height = 45
            Caption = 'Cancel'
            Font.Charset = ANSI_CHARSET
            Font.Color = clNavy
            Font.Height = -16
            Font.Name = 'Verdana'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 1
            OnClick = CopyCancelButtonClick
          end
        end
      end
      object NewPartDialog: TTabSheet
        Caption = 'NewPartDialog'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        ImageIndex = 2
        ParentFont = False
        TabVisible = False
        object PleaseInputLabel: TLabel
          Left = 11
          Top = 24
          Width = 286
          Height = 18
          Caption = 'Please Input Name for New Part'
        end
        object NewPartInput: TEdit
          Left = 8
          Top = 64
          Width = 449
          Height = 26
          TabOrder = 0
        end
        object NewPartOKButton: TButton
          Left = 310
          Top = 105
          Width = 85
          Height = 45
          Caption = 'OK'
          TabOrder = 1
          OnClick = NewPartOKButtonClick
        end
        object NewPartCancelButton: TButton
          Left = 398
          Top = 105
          Width = 85
          Height = 45
          Caption = 'Cancel'
          TabOrder = 2
          OnClick = CopyCancelButtonClick
        end
      end
      object PartErrorPage: TTabSheet
        Caption = 'PartErrorPage'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        ImageIndex = 3
        ParentFont = False
        TabVisible = False
        object Panel2: TPanel
          Left = 0
          Top = 28
          Width = 489
          Height = 180
          Align = alBottom
          BevelInner = bvLowered
          Color = clRed
          Font.Charset = ANSI_CHARSET
          Font.Color = clYellow
          Font.Height = -16
          Font.Name = 'Comic Sans MS'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          object PartText1: TLabel
            Left = 16
            Top = 16
            Width = 73
            Height = 23
            Caption = 'PartText1'
          end
          object PartText2: TLabel
            Left = 16
            Top = 40
            Width = 76
            Height = 23
            Caption = 'PartText2'
          end
          object Button1: TButton
            Left = 392
            Top = 128
            Width = 75
            Height = 41
            Caption = 'OK'
            TabOrder = 0
            OnClick = Button1Click
          end
        end
      end
      object ErrorSheet: TTabSheet
        Caption = 'ErrorSheet'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        ImageIndex = 4
        ParentFont = False
        TabVisible = False
        object ErrorPanel: TPanel
          Left = 0
          Top = 0
          Width = 489
          Height = 208
          Align = alClient
          Color = clRed
          Font.Charset = ANSI_CHARSET
          Font.Color = clYellow
          Font.Height = -16
          Font.Name = 'Comic Sans MS'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          object ErrorLine1: TLabel
            Left = 16
            Top = 16
            Width = 82
            Height = 18
            Caption = 'ErrorLine1'
            Font.Charset = ANSI_CHARSET
            Font.Color = clYellow
            Font.Height = -16
            Font.Name = 'Verdana'
            Font.Style = []
            ParentFont = False
          end
          object ErrorLine2: TLabel
            Left = 16
            Top = 48
            Width = 82
            Height = 18
            Caption = 'ErrorLine2'
            Font.Charset = ANSI_CHARSET
            Font.Color = clYellow
            Font.Height = -16
            Font.Name = 'Verdana'
            Font.Style = []
            ParentFont = False
          end
          object ErrorLine3: TLabel
            Left = 16
            Top = 80
            Width = 82
            Height = 18
            Caption = 'ErrorLine3'
            Font.Charset = ANSI_CHARSET
            Font.Color = clYellow
            Font.Height = -16
            Font.Name = 'Verdana'
            Font.Style = []
            ParentFont = False
          end
          object ErrorLine4: TLabel
            Left = 16
            Top = 112
            Width = 82
            Height = 18
            Caption = 'ErrorLine4'
            Font.Charset = ANSI_CHARSET
            Font.Color = clYellow
            Font.Height = -16
            Font.Name = 'Verdana'
            Font.Style = []
            ParentFont = False
          end
          object ErrorAckButton: TTouchSoftkey
            Left = 376
            Top = 152
            Width = 105
            Height = 50
            Color = clInfoBk
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -13
            Font.Name = 'Verdana'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 0
            OnClick = ErrorAckButtonClick
            Legend = 'Acknowledge'
            TextSize = 14
            HasIndicator = False
            IndicatorOn = False
            IsEnabled = True
            ButtonStyle = skNormal
            ArrowColor = clBlack
            ButtonColor = clWhite
            LegendColor = clBlack
            IndicatorWidth = 20
            IndicatorHeight = 10
            IndicatorOnColor = clGreen
            IndicatorOffColor = clSilver
            MaxFontSize = 0
          end
        end
      end
      object CopyPartSheet: TTabSheet
        Caption = 'CopyPartSheet'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        ImageIndex = 5
        ParentFont = False
        TabVisible = False
        object Label8: TLabel
          Left = 11
          Top = 24
          Width = 286
          Height = 18
          Caption = 'Please Input Name for New Part'
        end
        object CopyPartName: TEdit
          Left = 16
          Top = 56
          Width = 441
          Height = 26
          TabOrder = 0
          Text = 'CopyPartName'
        end
        object CopyPartButton: TButton
          Left = 310
          Top = 105
          Width = 85
          Height = 45
          Caption = 'OK'
          TabOrder = 1
          OnClick = CopyPartButtonClick
        end
        object CopyPartCancelButton: TButton
          Left = 398
          Top = 105
          Width = 85
          Height = 45
          Caption = 'Cancel'
          TabOrder = 2
          OnClick = CopyCancelButtonClick
        end
      end
      object DeletePartDialog: TTabSheet
        Caption = 'DeletePartDialog'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        ImageIndex = 6
        ParentFont = False
        TabVisible = False
        object Panel3: TPanel
          Left = 0
          Top = 0
          Width = 489
          Height = 208
          Align = alClient
          Color = clRed
          Font.Charset = ANSI_CHARSET
          Font.Color = clYellow
          Font.Height = -16
          Font.Name = 'Verdana'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 0
          object DeletePartMessage: TLabel
            Left = 19
            Top = 16
            Width = 157
            Height = 18
            Caption = 'variable text here'
            Font.Charset = ANSI_CHARSET
            Font.Color = clYellow
            Font.Height = -16
            Font.Name = 'Verdana'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object DelPartOKButton: TButton
            Left = 304
            Top = 129
            Width = 85
            Height = 45
            Caption = 'OK'
            Font.Charset = ANSI_CHARSET
            Font.Color = clNavy
            Font.Height = -16
            Font.Name = 'Verdana'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 0
            OnClick = DelPartOKButtonClick
          end
          object DelPartCancelButton: TButton
            Left = 392
            Top = 129
            Width = 85
            Height = 45
            Caption = 'Cancel'
            Font.Charset = ANSI_CHARSET
            Font.Color = clNavy
            Font.Height = -16
            Font.Name = 'Verdana'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 1
            OnClick = CopyCancelButtonClick
          end
        end
      end
      object NewOCTDialog: TTabSheet
        Caption = 'NewOCTDialog'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        ImageIndex = 7
        ParentFont = False
        TabVisible = False
        object PleaseInputOCTLabel: TLabel
          Left = 11
          Top = 24
          Width = 286
          Height = 18
          Caption = 'Please Input Name for New OCT'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Verdana'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object NEWOCTEdit: TEdit
          Left = 16
          Top = 56
          Width = 441
          Height = 26
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Verdana'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          Text = 'NEWOCTEdit'
        end
        object NewOCTOkButton: TButton
          Left = 310
          Top = 105
          Width = 85
          Height = 45
          Caption = 'OK'
          TabOrder = 1
          OnClick = NewOCTOkButtonClick
        end
        object Button3: TButton
          Left = 398
          Top = 105
          Width = 85
          Height = 45
          Caption = 'Cancel'
          TabOrder = 2
          OnClick = CopyCancelButtonClick
        end
      end
    end
  end
  object OPPKeyPanel: TPanel
    Left = 8
    Top = 344
    Width = 713
    Height = 50
    BevelOuter = bvNone
    Caption = 'OPPKeyPanel'
    TabOrder = 4
    object AppendOPPKey: TTouchSoftkey
      Left = 2
      Top = 0
      Width = 100
      Height = 50
      Color = clInfoBk
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -19
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      OnClick = AppendOPPKeyClick
      Legend = 'Append~OPP'
      TextSize = 14
      HasIndicator = False
      IndicatorOn = False
      IsEnabled = True
      ButtonStyle = skNormal
      ArrowColor = clBlack
      ButtonColor = clWhite
      LegendColor = clBlack
      IndicatorWidth = 20
      IndicatorHeight = 10
      IndicatorOnColor = clGreen
      IndicatorOffColor = clSilver
      MaxFontSize = 0
    end
    object InsertOPPKey: TTouchSoftkey
      Left = 98
      Top = 0
      Width = 100
      Height = 50
      Color = clInfoBk
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -19
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
      OnClick = InsertOPPKeyClick
      Legend = 'Insert~OPP'
      TextSize = 14
      HasIndicator = False
      IndicatorOn = False
      IsEnabled = True
      ButtonStyle = skNormal
      ArrowColor = clBlack
      ButtonColor = clWhite
      LegendColor = clBlack
      IndicatorWidth = 20
      IndicatorHeight = 10
      IndicatorOnColor = clGreen
      IndicatorOffColor = clSilver
      MaxFontSize = 0
    end
    object DeleteOPPKey: TTouchSoftkey
      Left = 194
      Top = 0
      Width = 100
      Height = 50
      Color = clInfoBk
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -19
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 2
      OnClick = DeleteOPPKeyClick
      Legend = 'Delete~OPP'
      TextSize = 14
      HasIndicator = False
      IndicatorOn = False
      IsEnabled = True
      ButtonStyle = skNormal
      ArrowColor = clBlack
      ButtonColor = clWhite
      LegendColor = clBlack
      IndicatorWidth = 20
      IndicatorHeight = 10
      IndicatorOnColor = clGreen
      IndicatorOffColor = clSilver
      MaxFontSize = 0
    end
    object MoveOPPUpKey: TTouchSoftkey
      Left = 290
      Top = 0
      Width = 100
      Height = 50
      Color = clInfoBk
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -19
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 3
      OnClick = MoveOPPUpKeyClick
      Legend = 'Move  OPP~UP'
      TextSize = 14
      HasIndicator = False
      IndicatorOn = False
      IsEnabled = True
      ButtonStyle = skNormal
      ArrowColor = clBlack
      ButtonColor = clWhite
      LegendColor = clBlack
      IndicatorWidth = 20
      IndicatorHeight = 10
      IndicatorOnColor = clGreen
      IndicatorOffColor = clSilver
      MaxFontSize = 0
    end
    object MoveOPPDownKey: TTouchSoftkey
      Left = 386
      Top = 0
      Width = 100
      Height = 50
      Color = clInfoBk
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -19
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 4
      OnClick = MoveOPPDownKeyClick
      Legend = 'Move OPP~Down'
      TextSize = 14
      HasIndicator = False
      IndicatorOn = False
      IsEnabled = True
      ButtonStyle = skNormal
      ArrowColor = clBlack
      ButtonColor = clWhite
      LegendColor = clBlack
      IndicatorWidth = 20
      IndicatorHeight = 10
      IndicatorOnColor = clGreen
      IndicatorOffColor = clSilver
      MaxFontSize = 0
    end
    object RemoveDependencyKey: TTouchSoftkey
      Left = 602
      Top = 0
      Width = 103
      Height = 50
      Color = clInfoBk
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -19
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 5
      OnClick = RemoveDependencyKeyClick
      Legend = 'Remove~Dependency'
      TextSize = 14
      HasIndicator = False
      IndicatorOn = False
      IsEnabled = True
      ButtonStyle = skNormal
      ArrowColor = clBlack
      ButtonColor = clWhite
      LegendColor = clBlack
      IndicatorWidth = 20
      IndicatorHeight = 10
      IndicatorOnColor = clGreen
      IndicatorOffColor = clSilver
      MaxFontSize = 0
    end
    object EditDependencyKey: TTouchSoftkey
      Left = 496
      Top = 0
      Width = 102
      Height = 50
      Color = clInfoBk
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -19
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 6
      OnClick = EditDependencyKeyClick
      Legend = 'Edit~Dependency'
      TextSize = 14
      HasIndicator = False
      IndicatorOn = False
      IsEnabled = True
      ButtonStyle = skNormal
      ArrowColor = clBlack
      ButtonColor = clWhite
      LegendColor = clBlack
      IndicatorWidth = 20
      IndicatorHeight = 10
      IndicatorOnColor = clGreen
      IndicatorOffColor = clSilver
      MaxFontSize = 0
    end
  end
  object OpenDialog1: TOpenDialog
    Left = 32
    Top = 416
  end
  object Flist: TFileList
    FullDate = False
    Left = 864
    Top = 408
  end
end
