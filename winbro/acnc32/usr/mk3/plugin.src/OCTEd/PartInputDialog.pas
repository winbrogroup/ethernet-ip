{*********************************************************************************
 This module is a dialogue box that is used when a new part folder is created.
**********************************************************************************}
unit PartInputDialog;


interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls;

type
  TPartNameInput = class(TForm)
    PartNameInDialog: TPanel;
    PleaseInputOCTLabel: TLabel;
    PartNameEdit: TEdit;
    NewOCTOkButton: TButton;
    Button3: TButton;
    procedure Button3Click(Sender: TObject);
    procedure NewOCTOkButtonClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    PartName : String;
  end;

var
  PartNameInput: TPartNameInput;

implementation

{$R *.dfm}

procedure TPartNameInput.FormShow(Sender: TObject);
begin
PartName := '';
end;

procedure TPartNameInput.NewOCTOkButtonClick(Sender: TObject);
begin
if PartNameEdit.Text <> '' then
  begin
  PartName := PartNameEdit.Text;
  ModalResult := mrOk;
  end
end;

procedure TPartNameInput.Button3Click(Sender: TObject);
begin
ModalResult := mrCancel;
end;

end.
