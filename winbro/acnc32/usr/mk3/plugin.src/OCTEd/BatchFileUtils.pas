unit BatchFileUtils;

interface
uses
  SysUtils, Windows,classes;

procedure Win32DeleteDirectory(const DirName: String);
{if this is a directory then delete all contents and clean up}

procedure Win32CopyDirectory( const Source, Dest: String);
{params without trailing '\' Win32API must have this somewhere!
fully recursive}

function Win32GetFileAge(const FN: String): Int64;

procedure MakeDirectoryList(DList : TStringList;Base : String);

procedure CheckTrailingBackslash( var Path: String);

function SlashSep(const Path, S: String): String;

implementation
uses
  FileCtrl;

procedure Win32DeleteDirectory(const DirName: String);
var
  FileInfo: TWin32FindData;
  FindHandle: THandle;
  Temp: String;
begin
  if DirName = '' then exit;
  if Pos('PartData',DirName) = 0 then exit;
  FindHandle:= Windows.FindFirstFile(PChar(DirName + '\*'), FileInfo);
  if FindHandle <> INVALID_HANDLE_VALUE then
  repeat
    Temp:= DirName + '\' + FileInfo.cFileName;
    if (FileInfo.dwFileAttributes and FILE_ATTRIBUTE_DIRECTORY) = 0 then
    begin
      SysUtils.DeleteFile(Temp)
    end else
    if (StrComp(FileInfo.cFileName, '.') <> 0) and
       (StrComp(FileInfo.cFileName, '..') <> 0)  then
    begin
      {attempt to remove before recursing in - it might be empty!}
      if not RemoveDir(Temp) then
      begin
        Win32DeleteDirectory(Temp);
        SysUtils.RemoveDir(Temp)
      end
    end;
  until not Windows.FindNextFile(FindHandle, FileInfo);
  Windows.FindClose(FindHandle);
  SysUtils.RemoveDir(DirName)
end;

procedure Win32CopyDirectory(const Source, Dest: String);
var
  FileInfo: TWin32FindData;
  FindHandle: THandle;
  SrcFile, DestFile: String;
begin
  if DirectoryExists(Source) then
  begin
    ForceDirectories(Dest);
    FindHandle:= Windows.FindFirstFile(PChar(Source+ '\*.*'), FileInfo);
    if FindHandle <> INVALID_HANDLE_VALUE then
    repeat
      SrcFile:= Source + '\' + FileInfo.cFilename;
      DestFile:= Dest + '\' + FileInfo.cFilename;
      if (FileInfo.dwFileAttributes and FILE_ATTRIBUTE_DIRECTORY) = 0 then
      begin
        if not Windows.CopyFile(PChar(SrcFile), PChar(DestFile), false) then
          RaiseLastWin32Error;
      end else
      if (StrComp(FileInfo.cFileName, '.') <> 0) and
         (StrComp(FileInfo.cFileName, '..') <> 0)  then
        Win32CopyDirectory(SrcFile, DestFile);
    until not Windows.FindNextFile(FindHandle, FileInfo);
    Windows.FindClose(FindHandle)
  end
end;

type
  TFileTimeRec =
  packed record
    case Byte of
      0: (Ft: TFileTime);
      1: (I64: Int64);
  end;

function Win32GetFileAge(const FN: String): Int64;
var
  fh: THandle;
  FI: TWin32FindData;
  st, ft: TFileTimeRec;
begin
  fh:= Windows.FindFirstFile(PChar(FN), FI);
  try
    if fh <> INVALID_HANDLE_VALUE then
    begin
      ft.Ft:= FI.ftCreationTime;
      GetSystemTimeAsFileTime(st.Ft);
      Result:= st.I64 - ft.I64
    end else
    begin
      Result:= -1
    end
  finally
    Windows.FindClose(fh)
  end
end;

procedure CheckTrailingBackslash( var Path: String);
begin
  if (Path = '') or (Path[Length(Path)] <> '\') then
     Path:= Path + '\'
end;


procedure MakeDirectoryList(DList : TStringList;Base : String);
var
  sr: TSearchRec;
  FileAttrs: Integer;
begin
 DList.Clear;
 FileAttrs := faDirectory;
 if FindFirst(SlashSep(Base,'*.*'), FileAttrs, sr) = 0 then
  begin
  if (sr.name <> '.') and (sr.name <> '..') then
     begin
     Dlist.Add(sr.name);
     end;
  while FindNext(sr) = 0 do
        begin
        if (sr.Attr and FileAttrs) = sr.Attr then
           begin
           if (sr.name <> '.') and (sr.name <> '..') then
              begin
              Dlist.Add(sr.name);
              end;
           end;

        end;
  end;
end;

// function to put slash at end of path if it doesn't exist
function SlashSep(const Path, S: String): String;
begin
  if AnsiLastChar(Path)^ <> '\' then
    Result := Path + '\' + S
  else
    Result := Path + S;
end;



end.
