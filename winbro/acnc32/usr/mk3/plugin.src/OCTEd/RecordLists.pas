unit RecordLists;
interface
uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Grids,StdCtrls,filectrl;


type

TOPPReference = object
       public
       OPPName : String;
       OCTName : String;
       Exists  : Boolean;
       RefList : TStringList;
       ReferenceCount : Integer;
       end;

pOPPRec = ^TOPPReference;

TBaseList  = class(TObject)
   public
   List : TList;
   Count : Integer;
   procedure FreeAllAndClear; virtual; abstract;
   procedure Delete(LIndex : Integer);
   constructor Create;
   procedure Destoyit;
   end;

TOPPRefList = class(TBaseList)
   public
   procedure FreeAllAndClear; override;
   procedure Add(OPPRef :pOPPRec);
   procedure RegisterOPP(OPPName,OCTName : String;Exists : Boolean);
   function Contains(OPPName : String):Boolean;
   function Occurences(OPPName : String):Integer;
   end;



implementation

{TBatchRequestList}

procedure TOPPRefList.FreeAllAndClear;
var
Entry : pOPPRec;
begin
while List.Count > 0 do
   begin
   Entry := List[0];
   Entry.RefList.Free;
   Dispose(pOPPRec(List[0]));
   List.Delete(0);
   end;
Count := 0;
end;

procedure TOPPRefList.Add(OPPRef: pOPPRec);
begin
List.Add(OPPRef);
Count := List.Count;
end;


{ TBaseList }


constructor TBaseList.Create;
begin
List := Tlist.Create;
Count := 0;
end;

procedure TBaseList.Delete(LIndex: Integer);
begin
List.Delete(Lindex);
Count := List.Count;
end;

procedure TBaseList.Destoyit;
begin
FreeAllandClear;
List.Free;
Free;
end;




procedure TOPPRefList.RegisterOPP(OPPName, OCTName: String;
  Exists: Boolean);
var
EntryNumber : Integer;
Found : Boolean;
Entry : pOPPRec;
EntryName : String;
begin
Found := False;
EntryNumber := 0;
while (not Found) and (EntryNumber < Count) do
      begin
      Entry := List[EntryNumber];
      EntryName := Entry.OPPName;
      if CompareText(OPPName,EntryName) = 0 then
         begin
         Found := True;
         Entry.ReferenceCount := Entry.ReferenceCount+1;
         Entry.RefList.Add(OCTName);
         end;
      inc(EntryNumber);
      end;
if Not Found then
   begin
   New(Entry);
   Entry.RefList := TStringList.Create;
   Entry.Reflist.Add(OCTName);
   Entry.OPPName := OppName;
   Entry.OCTName := OCTName;
   Entry.Exists := Exists;
   Entry.ReferenceCount := 1;
   Add(Entry);
   end;

end;

function TOPPRefList.Contains(OPPName: String): Boolean;
var
SCount : Integer;
Found  : Boolean;
Entry  : pOPPRec;
begin
Result := False;
if List.Count <1 then exit;
Found := False;
SCount := 0;
while (not Found) and (SCount < List.Count) do
      begin
      Entry := List[SCount];
      if CompareText(Entry.OppName,OPPName) = 0 then
         begin
         Found := True;
         end;
      inc(Scount);
      end;
Result := Found;

end;


function TOPPRefList.Occurences(OPPName: String): Integer;
var
EntryNumber : Integer;
ListEntry   : pOPPRec;
Found       : Boolean;
begin
result := 0;
if List.Count =0 then exit;
Found := False;
EntryNumber := 0;
while (EntryNumber <List.Count) and (not Found) do
    begin
    ListEntry := List[EntryNumber];
    if ListEntry.OPPName = OPPName then
       begin
       Found := True;
       Result := ListEntry.ReferenceCount;
       end;
    inc(EntryNumber);
    end;

end;

end.
