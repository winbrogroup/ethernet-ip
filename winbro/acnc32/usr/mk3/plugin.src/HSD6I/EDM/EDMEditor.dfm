object EDMEditorForm: TEDMEditorForm
  Left = 157
  Top = 200
  Width = 829
  Height = 454
  Caption = 'HSD6 II EDM Parameters'
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'MS Sans Serif'
  Font.Style = [fsBold]
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 821
    Height = 54
    Align = alTop
    BevelOuter = bvLowered
    TabOrder = 0
    object CopyBlockBtn: TSpeedButton
      Left = 166
      Top = 2
      Width = 84
      Height = 50
      Caption = 'Copy Block'
      OnClick = CopyBlockBtnClick
    end
    object PasteBlockBtn: TSpeedButton
      Left = 252
      Top = 2
      Width = 84
      Height = 50
      Caption = 'Paste Block'
      OnClick = PasteBlockBtnClick
    end
    object NewProgramBtn: TSpeedButton
      Left = 596
      Top = 2
      Width = 84
      Height = 50
      Caption = 'New Prog'
      OnClick = NewProgramBtnClick
    end
    object PasteProgramBtn: TSpeedButton
      Left = 424
      Top = 2
      Width = 84
      Height = 50
      Caption = 'Paste Prog'
      OnClick = PasteProgramBtnClick
    end
    object CopyProgramBtn: TSpeedButton
      Left = 338
      Top = 2
      Width = 84
      Height = 50
      Caption = 'Copy Prog'
      OnClick = CopyProgramBtnClick
    end
    object DeleteProgramBtn: TSpeedButton
      Left = 510
      Top = 2
      Width = 84
      Height = 50
      Caption = 'Delete Prog'
      OnClick = DeleteProgramBtnClick
    end
    object SpeedButton1: TSpeedButton
      Left = 78
      Top = 2
      Width = 84
      Height = 50
      Caption = 'Use Live Cell'
      OnClick = SpeedButton1Click
    end
    object CopyLiveBtn: TSpeedButton
      Left = 682
      Top = 2
      Width = 84
      Height = 50
      Caption = 'Copy Live'
      OnClick = CopyLiveBtnClick
    end
    object IDUpDown: TUpDown
      Left = 8
      Top = 2
      Width = 50
      Height = 50
      Hint = 'Program Number Change'
      Max = 32000
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = IDUpDownClick
    end
    object BlockUpDown: TUpDown
      Left = 768
      Top = 2
      Width = 50
      Height = 50
      Hint = 'Block count change'
      Max = 10000
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = BlockUpDownClick
    end
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 393
    Width = 821
    Height = 27
    Panels = <
      item
        Width = 150
      end
      item
        Width = 150
      end
      item
        Width = 50
      end>
    ParentFont = True
    UseSystemFont = False
  end
  object Panel2: TPanel
    Left = 0
    Top = 359
    Width = 821
    Height = 34
    Align = alBottom
    BevelOuter = bvLowered
    TabOrder = 2
    object Label2: TLabel
      Left = 176
      Top = 8
      Width = 52
      Height = 13
      Caption = 'Comment'
    end
    object OPPLiveBtn: TSpeedButton
      Left = 656
      Top = 2
      Width = 90
      Height = 30
      AllowAllUp = True
      GroupIndex = 1
      Caption = 'OPP / Live'
      OnClick = OPPLiveBtnClick
    end
    object EditCellSpeedButton: TSpeedButton
      Left = 544
      Top = 2
      Width = 90
      Height = 30
      Caption = 'Edit Cell'
      OnClick = EditCellSpeedButtonClick
    end
    object BitBtn2: TBitBtn
      Left = 88
      Top = 4
      Width = 75
      Height = 25
      TabOrder = 0
      Kind = bkCancel
    end
    object BitBtn1: TBitBtn
      Left = 8
      Top = 4
      Width = 75
      Height = 25
      TabOrder = 1
      Kind = bkOK
    end
    object CommentEdit: TEdit
      Left = 248
      Top = 4
      Width = 281
      Height = 21
      TabOrder = 2
      Text = 'CommentEdit'
      OnChange = CommentEditChange
    end
  end
  object EDMTab: TTabControl
    Left = 0
    Top = 54
    Width = 821
    Height = 305
    Align = alClient
    HotTrack = True
    TabOrder = 3
    Tabs.Strings = (
      '1'
      '2'
      '3')
    TabIndex = 0
    OnChange = ProgramSelectChange
  end
end
