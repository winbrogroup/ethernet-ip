unit DutyCycleEditor;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, ExtCtrls, Buttons, StdCtrls, NamedPlugin;

type
  TDutyCycleApply = procedure(Sender : TObject; var OnT, OffT, SOnT, SOffT : Integer) of Object;
  TDutyCycleForm = class(TForm)
    StatusBar: TStatusBar;
    Panel1: TPanel;
    Panel2: TPanel;
    OnTimePanel: TPanel;
    OnOffSplitter: TSplitter;
    OffTimePanel: TPanel;
    FreqTrackBar: TTrackBar;
    OnTimePaintBox: TPaintBox;
    OffTimePaintBox: TPaintBox;
    Panel3: TPanel;
    SuppressedBtn: TSpeedButton;
    Panel5: TPanel;
    BitBtn2: TBitBtn;
    BitBtn1: TBitBtn;
    SamplePaint: TPaintBox;
    ApplyBtn: TBitBtn;
    procedure SuppressedBtnClick(Sender: TObject);
    procedure OnTimePaintBoxPaint(Sender: TObject);
    procedure OffTimePaintBoxPaint(Sender: TObject);
    procedure OnOffSplitterCanResize(Sender: TObject; var NewSize: Integer;
      var Accept: Boolean);
    procedure OffTimePanelResize(Sender: TObject);
    procedure FreqTrackBarChange(Sender: TObject);
    procedure SamplePaintPaint(Sender: TObject);
    procedure ApplyBtnClick(Sender: TObject);
    procedure OnOffSplitterPaint(Sender: TObject);
  private
    OnTime, OffTime, SuppOnTime, SuppOffTime : Integer;
    EditOnTime, EditOffTime : Integer;
    EditingSuppressed : Boolean;
    Blocking : Boolean;
    FApply : TDutyCycleApply;
    procedure UpdateDisplay;
    procedure UpdateEditValues;
    procedure SetTrackBar;
    procedure UpdateValues;
    procedure DisplayDutyCycle;
    property Apply : TDutyCycleApply read FApply write FApply;
  public
    class procedure Execute(var OnT, OffT, SOnT, SOffT : Integer; Supp : Boolean; aApply : TDutyCycleApply);
  end;

var
  DutyCycleForm: TDutyCycleForm;

implementation

{$R *.DFM}

const TraceWidth = 6;

resourcestring
  EditingSuppLang = 'Editing Supp.';
  EditingNormalLang = 'Editing Normal';
  DutyCycleFormatLang = 'Duty Cycle = %f%%';
  FrequenctyFormatLang = 'Frequency = %fKHz';

class procedure TDutyCycleForm.Execute(var OnT, OffT, SOnT, SOffT : Integer; Supp : Boolean; aApply : TDutyCycleApply);
var Form : TDutyCycleForm;
    HoldOnT, HoldOffT, HoldSOnT, HoldSOffT : Integer;
begin
  Form := TDutyCycleForm.Create(Application);
  if FontName <> '' then
    Form.Font.Name := FontName;
  Form.OnTime := OnT;
  Form.OffTime := OffT;
  Form.SuppOnTime := SOnT;
  Form.SuppOffTime := SOffT;
  Form.Apply := aApply;

  HoldOnT := OnT;
  HoldOffT := OffT;
  HoldSOnT := SOnT;
  HoldSOffT := SOffT;


  Form.EditingSuppressed := Supp;
  Form.SuppressedBtn.Down := Supp;
  if Form.EditingSuppressed then begin
    Form.EditOnTime := Form.SuppOnTime;
    Form.EditOffTime := Form.SuppOffTime;
  end else begin
    Form.EditOnTime := Form.OnTime;
    Form.EditOffTime := Form.OffTime;
  end;

  Form.UpdateDisplay;
  Form.SetTrackBar;
  if Form.ShowModal = mrOK then begin
    Form.SuppressedBtnClick(Form);
    OnT := Form.OnTime div 10 * 10;
    OffT := Form.OffTime div 10 * 10;
    SOnT := Form.SuppOnTime div 10 * 10;
    SOffT := Form.SuppOffTime div 10 * 10;
  end else begin
    OnT := HoldOnT;
    OffT := HoldOffT;
    SOnT := HoldSOnT;
    SOffT := HoldSOffT;
  end;
  Form.Free;
end;

procedure TDutyCycleForm.UpdateValues;
begin
  if EditingSuppressed then begin
    SuppOnTime := EditOnTime;
    SuppOffTime := EditOffTime;
  end else begin
    OnTime := EditOnTime;
    OffTime := EditOffTime;
  end;
end;

procedure TDutyCycleForm.UpdateEditValues;
begin
  if EditingSuppressed then begin
    EditOnTime := SuppOnTime;
    EditOffTime := SuppOffTime;
  end else begin
    EditOnTime := OnTime;
    EditOffTime := OffTime;
  end;
end;

procedure TDutyCycleForm.SuppressedBtnClick(Sender: TObject);
begin
  UpdateValues;
  EditingSuppressed := SuppressedBtn.Down;
  UpdateEditValues;
  UpdateDisplay;
  SetTrackBar;
end;

procedure TDutyCycleForm.OnTimePaintBoxPaint(Sender: TObject);
begin
  with OnTimePaintBox do begin
    Canvas.Pen.Color := clBlue;
    Canvas.Pen.Width := TraceWidth;
    Canvas.MoveTo(0, Height - Height div 5);
    Canvas.LineTo(15, Height - Height div 5);
    Canvas.Pen.Color := clBlack;
    Canvas.LineTo(15, Height div 5);
    Canvas.LineTo(Width, Height div 5);

    Canvas.Brush.Color := clSilver;
    Canvas.TextOut(10, 10, Format('%fuS On Time', [EditOnTime / 1000]));
  end;
end;

procedure TDutyCycleForm.SetTrackBar;
begin
//  FreqTrackBar.Position := Trunc(100000 / (EditOnTime + EditOffTime));
  FreqTrackBar.Position := Trunc(1000000 / (EditOnTime + EditOffTime));
end;

procedure TDutyCycleForm.OffTimePaintBoxPaint(Sender: TObject);
begin
  with OffTimePaintBox do begin
    Canvas.Pen.Color := clBlack;
    Canvas.Pen.Width := TraceWidth;
    Canvas.MoveTo(0, Height - Height div 5);
    Canvas.LineTo(Width - 15, Height - Height div 5);
    Canvas.LineTo(Width - 15, Height div 5);
    Canvas.Pen.Color := clBlue;
    Canvas.LineTo(Width, Height div 5);
    Canvas.Brush.Color := clSilver;
    Canvas.TextOut(10, 10, Format('%fuS Off Time', [EditOffTime / 1000]));
  end;
end;

procedure TDutyCycleForm.OnOffSplitterCanResize(Sender: TObject;
  var NewSize: Integer; var Accept: Boolean);
begin
  if NewSize > Width - 70 then
    NewSize := Width - 70;

  if NewSize < 30 then
    NewSize := 30;
end;

procedure TDutyCycleForm.OffTimePanelResize(Sender: TObject);
var Time, Ratio : Double;
begin
  if not Blocking then begin
    // Using the current time base determine the On / Off
//    Time := (1 / (FreqTrackBar.Position * 1000)) * 100000000;
    Time := (1 / (FreqTrackBar.Position * 100)) * 100000000;

    // Tmp now represents the total time in nS
    // The ratio of On to Off
    Ratio := (OnTimePanel.Width - 15) / (OnTimePanel.Width + OffTimePanel.Width - 30);
    EditOnTime := Round(Ratio * Time);
    EditOffTime := Round(Time - EditOnTime);
  end;
  DisplayDutyCycle;
  SamplePaint.Invalidate
end;

procedure TDutyCycleForm.DisplayDutyCycle;
begin
  StatusBar.Panels[0].Text := Format(DutyCycleFormatLang + ' : ' + FrequenctyFormatLang,
       [EditOnTime * 100 / (EditOnTime + EditOffTime), FreqTrackBar.Position * 1.000001]);
end;

procedure TDutyCycleForm.FreqTrackBarChange(Sender: TObject);
var Ratio, Time : Double;
begin
  // Total time for display = (1 / (FreqTrackBar.Position * 1000)) * 100000000
//  Time := (1 / (FreqTrackBar.Position * 1000)) * 100000000;
  Time := (1 / (FreqTrackBar.Position * 100)) * 100000000;
  Ratio := (OnTimePanel.Width - 15) / (OnTimePanel.Width + OffTimePanel.Width - 30);
  EditOnTime := Round(Ratio * Time);
  EditOffTime := Round(Time - EditOnTime);

  UpdateDisplay;
  OnTimePanel.Repaint;
  OffTimePanel.Repaint;
end;

// Changes the trackbar and panels to match the settings of On / Off
procedure TDutyCycleform.UpdateDisplay;
var Ratio : Double;
begin
  if EditingSuppressed then
    SuppressedBtn.Caption := EditingSuppLang
  else
    SuppressedBtn.Caption := EditingNormalLang;

// Given our frequency domain we can now determine the scale of view
  Blocking := True;
  try
    Ratio := EditOnTime / (EditOnTime + EditOffTime);
    OnTimePanel.Width := Round((ClientWidth - OnOffSplitter.Width - 30) * Ratio) + 15;
  finally
    Blocking := False;
  end;

  SamplePaint.Invalidate;
  DisplayDutyCycle
end;


procedure TDutyCycleForm.SamplePaintPaint(Sender: TObject);
var XPos, YUnit : Integer;
begin
  UpdateValues;
  YUnit := SamplePaint.Height div 12;
  with SamplePaint.Canvas do begin
    if not EditingSuppressed then
      SamplePaint.Canvas.Pen.Color := clRed
    else
      SamplePaint.Canvas.Pen.Color := clBlack;
    XPos := 0;
    MoveTo(XPos, YUnit);
    while XPos < SamplePaint.Width do begin
      LineTo(XPos + Round(OnTime / 500), YUnit);
      LineTo(XPos + Round(OnTime / 500), YUnit * 5);
      LineTo(XPos + Round((OnTime + OffTime) / 500), YUnit * 5);
      LineTo(XPos + Round((OnTime + OffTime) / 500), YUnit);
      XPos := XPos + Round((OnTime + OffTime) / 500);
    end;

    if EditingSuppressed then
      SamplePaint.Canvas.Pen.Color := clRed
    else
      SamplePaint.Canvas.Pen.Color := clBlack;
    XPos := 0;
    MoveTo(XPos, YUnit * 7);
    while XPos < SamplePaint.Width do begin
      LineTo(XPos + Round(SuppOnTime / 500), YUnit * 7);
      LineTo(XPos + Round(SuppOnTime / 500), YUnit * 11);
      LineTo(XPos + Round((SuppOnTime + SuppOffTime) / 500), YUnit * 11);
      LineTo(XPos + Round((SuppOnTime + SuppOffTime) / 500), YUnit * 7);
      XPos := XPos + Round((SuppOnTime + SuppOffTime) / 500);
    end
  end;
end;

procedure TDutyCycleForm.ApplyBtnClick(Sender: TObject);
begin
  UpdateValues;
  if Assigned(Apply) then
    Apply(Self, OnTime, OffTime, SuppOnTime, SuppOffTime);

  UpdateEditValues;
  UpdateDisplay;
end;

procedure TDutyCycleForm.OnOffSplitterPaint(Sender: TObject);
var Gap : Integer;
    UpperPoint, LowerPoint : Integer;
begin
  with OnOffSplitter do begin
    UpperPoint := Height div 5 - (TraceWidth div 2);
    LowerPoint := Height - Height div 5 + (TraceWidth div 2);
    Gap := (Width - TraceWidth) div 2;
    Canvas.Brush.Color := clBlack;
    Canvas.Rectangle(Gap, UpperPoint, Width - Gap, LowerPoint);
    Canvas.Rectangle(0, UpperPoint, Width - Gap, UpperPoint + TraceWidth);
    Canvas.Rectangle(Gap, LowerPoint, Width, LowerPoint - TraceWidth);
  end;
end;

end.
