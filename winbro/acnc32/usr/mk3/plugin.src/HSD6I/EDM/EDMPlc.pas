unit EDMPlc;

interface

uses
  Classes, AbstractPlcPlugin, MK1EDMReaderWriter, SysUtils, PmacServo, AbstractOPPPlugin,
  LPE477, LPE482, Windows, NamedPlugin, CNCTypes, INamedInterface, IFSDServer;

type
  TPlcEDMHeadProperty = (
    pepEnable,            // Disables all communication to devices
    pepEDM,               // Runs EDM program while true
    pepContinue,          // EDM processing uses last first spark position
    pepStoreFirstSpark,   // EDM processing uses last first spark position
    pepRedress,           // Block processing uses rules  for redress
    pepServo,             // Enables EDM servo
    pepDCM,               // Puts the EDM head into depth control mode
    pepHoleProgram,       // Use to assign active hole program number.
    pepStandOff,          // Distance to pullback in microns
    pepFromFirstSpark,    // Standoff applies to first spark position
    pepFromDepth,         // Standoff applied to end of cut
    pepResetIDetect,      // Reset LPE482 IDetect
    pepSuppression,       // Set suppression enable in lpe577
//    pepElectrodeCount,    // Sets the number of electrodes in the cartrige
    pepRemoveOffset,      // Remove head offset
    pepNextBlock,         // Rising edge forces action of next block
    pepRefresh,
    pepForceFirstSpark,
    pepReverseDrilling,
    pepTFReset,
    pepPTEnable,
    pepRapidRecovery,

    pepFrozen,            // In servo mode the head locks in its current position
    pepEDMRelay,          // True when EDM relay is required
    pepComplete,          // True when while EDM is demanded and block-processing complete
    pepRetracted,         // True when the Servo has retracted.
    pepFirstSpark,        // True when EDM demanded and First spark found.
    pepAltBit0,           // Bit 0 of OEM bits
    pepAltBit1,           // Bit 1 of OEM bits
    pepAltBit2,           // Bit 2 of OEM bits
    pepAltBit3,           // Bit 3 of OEM bits
    pepInvalidProgram,    // Program number is invalid
    pepFailedFirstSpark,  // Program failed to find first spark
    pepFailedRedress,     // Program failed to redress
    pepCapacitance,       // Capacitance is selected
    pepOffsetPresent,
    //pepDCSuppon,
    pepTempHealthy,
    //pepFPGA,
    pepTFail,
    //pepOverpower,
    pepSupplyHealthy,
    pepElectrodeSense,
    pepRetractError,
    pepInSuppression,
    pepElectrodeOverSense
  );

  TPlcEDMHeadProperties = set of TPlcEdmHeadProperty;

  TElectrodeMask = record
    case Byte of
      0 : (
        W1, W2, W3 : Word;
      );
      1 : (
        B : array [0..5] of Byte;
      );
  end;

  TIndexMap = record
    Electrode, Offset, Mask, Bit : Integer;
  end;
  PIndexMap = ^TIndexMap;

  TDataCapture = class(TObject)
  private
    Data : array [0..50] of Integer;
    Ndx : Integer;
    FIntegration : Integer;
    FMin, FMax, FMean : Integer;
    procedure SetIntegration(aInt : Integer);
  public
    constructor Create;
    procedure AddData(aValue : Integer);
    procedure UpdateMinMax;
    procedure UpdateExtended;
    property Min : Integer read FMin;
    property Max : Integer read FMax;
    property Mean : Integer read FMean;
    property Integration : Integer read FIntegration write SetIntegration;
  end;

  TEDMStage = (
    esWaitServoOn,
    esWaitFirstSpark,
    esWaitForFeedBack1,
    esWaitRedress,
    esWaitForFeedBack2,
    esBlockProcess,
    esWaitRetracted,
    esComplete
  );

  THSD6IEDMHeadPlcItem = class(TAbstractPlcPlugin)
  private
    Inputs : TPlcEDMHeadProperties;
    Index : Integer;
    Pmac : TPmacDevice;
    LPE477 : T477Device;
    FLPE482 : T482Device;
    ProgNumEv : TPlcPluginResolveItemValueEv; // Slightly out of spec (multiple compile instances)
    ProgramNumber : Integer;
    CorrectProgramNumber : Integer;
    FBlockProcessing : Boolean;
    FActiveProgram : TMK1EDMProgram;
    FActiveBlock : Integer;
    ThisUpdate : Integer;
    EDMStage : TEDMStage;
    Debug : Integer;
    LPE577UpdateStage : Integer;
    ActualElectrodeMap : Int64;
    LastActualElectrodeMap : Int64;

    // All below are used for safe parameter switching
    LastBoards : Integer;
    LastCapacitance : Integer;
    LastBusVoltage : Integer;
    LastCoupling : Integer;
    LastNegative : Boolean;

    Delay : Cardinal; // Used for off delay in safe parameter change
    FSafeChange : Boolean;
    ThisTick : Cardinal;

    StartTime: TDateTime;
    HadFirstSpark: Boolean;
    FirstSparkPos : Double;
    FeedbackPos: Integer; // Used to check update has occured

    procedure Update577;
    procedure SetActiveProgram(ProgID : Integer);
    procedure SetBlock(aBlock : Integer);
    function GetDepth : Double;
    procedure NewProgram(Sender : TObject);
  public
    ICapture : TDataCapture;
    VCapture : TDataCapture;
    constructor Create( Parameters : string;
                        ResolveDeviceEv : TPlcPluginResolveDeviceEv;
                        ResolveItemEv : TPlcPluginResolveItemEv); override;
    procedure Compile ( var Embedded : TEmbeddedData;
                        aReadEv : TPlcPluginReadEv;
                        ResolveItemEv : TPlcPluginResolveItemEv;
                        Write : Boolean);  override;
    destructor Destroy; override;
    procedure GatePre; override;
    function GatePost : Integer; override;
    function Read(var Embedded : TEmbeddedData) : Boolean; override;
    procedure Write(var Embedded : TEmbeddedData; Line, LastLine : Boolean); override;
    function GateName(var Embedded : TEmbeddedData) : string; override;
    function ItemProperties : string; override;
    class function Properties : string; override;
    property ActiveProgram : TMK1EDMProgram read FActiveProgram;
    property ActiveBlock : Integer read FActiveBlock;
    property BlockProcessing : Boolean read FBlockProcessing;
    property Depth : Double read GetDepth;
    property LPE482 : T482Device read FLPE482;
    property SafeChange : Boolean read FSafeChange;
    class procedure FinalInstall; override;
    class procedure Install; override;
  end;

  THsd6Web = class(TInterfacedObject, IHTTPServerComponent)
  protected
    function HTTPGeneratePage(HFile : THANDLE; var CGIPath : WideString; CGIRequest: ICGIStringList) : Boolean; stdcall;
    function HTTPAddStreaming(Stream : THandle; CGIR : WideString; Method : IHTTPServer; DT : Integer) : Boolean; stdcall;
    procedure HTTPRemoveStreaming(Stream : THandle; CGIR : WideString); stdcall;
  public
    constructor Create;
  end;

  TElectrodeMap = record
    case Byte of
      0 : ( I : Int64 );
      1 : ( W : array [0..3] of Word );
  end;

var
  EDMHead : THSD6IEDMHeadPlcItem;
  EMapTag : TTagRef;
  ActEMapTag : TTagRef;
  EmapCofNTag : TTagRef;
  ElectrodeMap : TElectrodeMap;

  EMapTotal : Integer;
  EMapTotalAct : Integer;
  LoggingTag : TTagRef;
  FeatureIDTag: TTagRef;
  CutIDTag: TTagRef;

implementation

resourcestring
  SenseLang = 'Sense';

const
  PropertyName : array [TPlcEDMHeadProperty] of TPlcProperty = (
    ( Name : 'Enable';          ReadOnly : False ),
    ( Name : 'EDM';             ReadOnly : False ),
    ( Name : 'Continue';        ReadOnly : False ),
    ( Name : 'StoreFirstSpark'; ReadOnly : False ),
    ( Name : 'Redress';         ReadOnly : False ),
    ( Name : 'Servo';           ReadOnly : False ),
    ( Name : 'DCM';             ReadOnly : False ),
    ( Name : 'HoleProgram';     ReadOnly : False ),
    ( Name : 'StandOff';        ReadOnly : False ),
    ( Name : 'FromFirstSpark';  ReadOnly : False ),
    ( Name : 'FromDepth';       ReadOnly : False ),
    ( Name : 'ResetIDetect';    ReadOnly : False ),
    ( Name : 'Suppression';     ReadOnly : False ),
    ( Name : 'RemoveOffset';    ReadOnly : False ),
    ( Name : 'NextBlock';       ReadOnly : False ),
    ( Name : 'Refresh';         ReadOnly : False ),
    ( Name : 'ForceFirstSpark'; ReadOnly : False ),
    ( Name : 'ReverseDrilling'; ReadOnly : False ),
    ( Name : 'TFReset';         ReadOnly : False ),
    ( Name : 'PTEnable';        ReadOnly : False ),
    ( Name : 'RapidRecovery';   ReadOnly : False ),


    ( Name : 'Frozen';          ReadOnly : True ),
    ( Name : 'EDMRelay';        ReadOnly : True ),
    ( Name : 'Complete';        ReadOnly : True ),
    ( Name : 'Retracted';       ReadOnly : True ),
    ( Name : 'FirstSpark';      ReadOnly : True ),
    ( Name : 'AltBit0';         ReadOnly : True ),
    ( Name : 'AltBit1';         ReadOnly : True ),
    ( Name : 'AltBit2';         ReadOnly : True ),
    ( Name : 'AltBit3';         ReadOnly : True ),
    ( Name : 'InvalidProgram';  ReadOnly : True ),
    ( Name : 'FailedFirstSpark';ReadOnly : True ),
    ( Name : 'FailedRedress';   ReadOnly : True ),
    ( Name : 'Capacitance';     ReadOnly : True ),
    ( Name : 'OffsetPresent';   ReadOnly : True ),
    //( Name : 'DCSuppon';        ReadOnly : True ),
    ( Name : 'TempHealthy';       ReadOnly : True ),
    //( Name : 'FPGA';            ReadOnly : True ),
    ( Name : 'TFail';           ReadOnly : True ),
    //( Name : 'Overpower';       ReadOnly : True ),
    ( Name : 'SupplyHealthy';   ReadOnly : True ),
    ( Name : 'ElectrodeSense';  ReadOnly : True ),
    ( Name : 'RetractError';    ReadOnly : True ),
    ( Name : 'InSuppression';   ReadOnly : True ),
    ( Name : 'ElectrodeOverSense'; ReadOnly : True )
  );


function CountBits(I : Int64) : Integer;
begin
  Result := 0;

  while I <> 0 do begin
    if (I and 1) <> 0 then
      Inc(Result);
    I := I shr 1;
  end;
end;

procedure UpdateEmapCofN;
var A, B : Integer;
begin
  A := EmapTotalAct;
  B := EmapTotal;

  if A > B then
    A := B;

  Named.SetAsString(EmapCofNTag, PChar(Format('%s %d/%d', [SenseLang, A, B])));
end;

procedure EMapChange(aTag : TTagRef); stdcall;
var Buffer : array [0..63] of Char;
begin
  Named.GetAsString(EMapTag, Buffer, 64);
  try
    ElectrodeMap.I := StrToInt64('$' + Buffer);
  except
    ElectrodeMap.I := $100;
  end;
  EMapTotal := CountBits(ElectrodeMap.I);
  UpdateEmapCofN;
end;


constructor THSD6IEDMHeadPlcItem.Create( Parameters : string;
                    ResolveDeviceEv : TPlcPluginResolveDeviceEv;
                    ResolveItemEv : TPlcPluginResolveItemEv);
var
  Token : string;
begin
  try
    Token := ReadDelimited(Parameters, ';');
    Index := StrToInt(Token);
  except
    raise Exception.CreateFmt('Incorrect index value for EDMHead [%s]', [Token]);
  end;
  if Index <> 1 then
    raise Exception.Create('Currently Index for edm head must be ''1''');

  Token := ReadDelimited(Parameters, ';');
  if not ResolveDeviceEv(Self, PChar(Token), Pmac.ISize, Pmac.OSize, Pointer(Pmac.Input), Pointer(Pmac.Output)) then
    raise Exception.CreateFmt('Cannot find device [%s] in IO', [Token]);

  if (Pmac.ISize < Sizeof(TPmacPlcIn)) or (Pmac.OSize <> Sizeof(TPmacPlcOut)) then
    raise Exception.CreateFmt('Wrong size of device [%s] expected [I%d, O%d]', [Token, Sizeof(TPmacPlcIn), Sizeof(TPmacPlcOut)]);

  Token := ReadDelimited(Parameters, ';');
  if not ResolveDeviceEv(Self, PChar(Token), LPE477.ISize, LPE477.OSize, Pointer(LPE477.Input), Pointer(LPE477.Output)) then
    raise Exception.CreateFmt('Cannot find device [%s] in IO', [Token]);

  if (LPE477.ISize < Sizeof(TLPE477In)) or (LPE477.OSize <> Sizeof(TLPE477Out)) then
    raise Exception.CreateFmt('Wrong size of device [%s] expected [I%d, O%d]', [Token, Sizeof(TLPE477In), Sizeof(TLPE477Out)]);


  Token := ReadDelimited(Parameters, ';');
  if not ResolveDeviceEv(Self, PChar(Token), FLPE482.ISize, FLPE482.OSize, Pointer(FLPE482.Input), Pointer(FLPE482.Output)) then
    raise Exception.CreateFmt('Cannot find device [%s] in IO', [Token]);

  if (LPE482.ISize < Sizeof(TLPE482In)) or (LPE482.OSize <> Sizeof(TLPE482Out)) then
    raise Exception.CreateFmt('Wrong size of device [%s] expected [I%d, O%d]', [Token, Sizeof(TLPE482In), Sizeof(TLPE482Out)]);

  SetActiveProgram(0);
  Pmac.Output.Command := [];
  //LPE477.Output.Control := [Lpe577CntlSuppEna];
  Inputs := [pepSuppression];
  Pmac.Output.VoltageScaling := 50;
  Pmac.Output.ReverseThreshhold := 10;
  Pmac.Output.ForwardThreshold := 10;
  Pmac.Output.ShortVelocity := 10;
  Pmac.Output.ShortInterval := 2;
  FSafeChange := False;
  {
  Exclude(Lpe577.Output.Control, Lpe577CntlSupp_RE);

  if (lpe577StatNot577 in LPE577.Input^.Status) or
     not (lpe577Stat577 in LPE577.Input^.Status) then
     raise Exception.Create('Not an LPE577');

  }
  Include(Inputs, pepInvalidProgram);
  TAbstractOPPPluginConsumer.AddCallback(HSD6IISectionName, NewProgram);
  ICapture := TDataCapture.Create;
  VCapture := TDataCapture.Create;
  EDMHead := Self;
end;

destructor THSD6IEDMHeadPlcItem.Destroy;
begin
  EDMHead := nil;

  if Assigned(ICapture) then
    ICapture.Free;

  if Assigned(VCapture) then
    VCapture.Free;

  TAbstractOPPPluginConsumer.RemoveCallback(NewProgram);

{  if Assigned(Lock) then
    Lock.Free; }

  inherited Destroy;
end;

procedure THSD6IEDMHeadPlcItem.Compile ( var Embedded : TEmbeddedData;
                    aReadEv : TPlcPluginReadEv;
                    ResolveItemEv : TPlcPluginResolveItemEv;
                    Write : Boolean);
var I : TPlcEDMHeadProperty;
    Token : string;
    IsConstant : Boolean;
begin
  Token := aReadEv(Self);
  if Token <> '.' then
    raise Exception.Create('Period [.] expected');

  Token := aReadEv(Self);
  for I := Low(TPlcEDMHeadProperty) to High(TPlcEDMHeadProperty) do begin
    if CompareText(PropertyName[I].Name, Token) = 0 then begin
      Embedded.Command := Ord(I);
      if (I = pepHoleProgram) or (I = pepStandoff){ or (I = pepElectrodeCount)} then begin
        Token := aReadEv(Self);
        if Token <> '.' then
          raise Exception.Create('Period [.] expected');
        Token := aReadEv(Self);
        Embedded.OData := ResolveItemEv(Self, PChar(Token), IsConstant, ProgNumEv);
        if Embedded.OData = nil then begin
          raise Exception.CreateFmt('Cannot resolve [%s] to a value', [Token]);
        end;
      end;
      Exit;
    end;
  end;
  raise Exception.CreateFmt('Unknown property [%s]', [Token]);
end;

procedure THSD6IEDMHeadPlcItem.GatePre;
begin
end;

procedure THSD6IEDMHeadPlcItem.SetBlock(aBlock : Integer);
begin
  FValue := aBlock;
  if Assigned(ActiveProgram) then begin
    FActiveBlock := aBlock;

    if ActiveBlock >= ActiveProgram.BlockCount then begin
      FActiveBlock := ActiveProgram.BlockCount - 1;
      EDMStage := esWaitRetracted;
      Named.EventLog('EDM Terminated by Final Depth');
    end;
    ActiveProgram[ActiveBlock].UpdateRequired := True;
  end;
end;

function THSD6IEDMHeadPlcItem.GatePost : Integer;
var A, B : Int64;
begin
  MK1EDMReaderWriter.Lock.Enter;
  try
    A := LPE482.Input^.Sense.W1 or $00E0;
    B := LPE482.Input^.Sense.W2;

    ActualElectrodeMap := (A shl 32) +
                          (B shl 16) +
                           LPE482.Input^.Sense.W3;
    ActualElectrodeMap := (not ActualElectrodeMap) and $FFFFFFFFFFFF;

    if LastActualElectrodeMap <> ActualElectrodeMap then begin
      Named.SetAsString(ActEMapTag, PChar('$' + IntToHex(ActualElectrodeMap, 12)));
      LastActualElectrodeMap := ActualElectrodeMap;
      EmapTotalAct := CountBits(ActualElectrodeMap and ElectrodeMap.I);
      UpdateEmapCofN;
    end;

    if pehcFreeze in Pmac.Output.Command then
      Include(Inputs, pepFrozen)
    else
      Exclude(Inputs, pepFrozen);

    if ActualElectrodeMap and ElectrodeMap.I = ElectrodeMap.I then
      Include(Inputs, pepElectrodeSense)
    else
      Exclude(Inputs, pepElectrodeSense);

    if (ActualElectrodeMap and not ElectrodeMap.I) > 0 then
      Include(Inputs, pepElectrodeOverSense)
    else
      Exclude(Inputs, pepElectrodeOverSense);

    VCapture.AddData(Pmac.Input^.GapVolts);
    ICapture.AddData(Pmac.Input^.GapCurrent);

    with LPE477.Input^ do begin
      if lpe477pulseTempOK in RecpulseStat then
        Include(Inputs, pepTempHealthy)
      else
        Exclude(Inputs, pepTempHealthy);

      if lpe477pulseSplyOK in RecpulseStat then
        Include(Inputs, pepSupplyHealthy)
      else
        Exclude(Inputs, pepSupplyHealthy);

      if lpe477oscTFail in RecOscStat then
        Include(Inputs, pepTFail)
      else
        Exclude(Inputs, pepTFail);

      {
      if lpe577Stat1Suppression in Status1 then
        Include(Inputs, pepInSuppression)
      else
        Exclude(Inputs, pepInSuppression);
        }
    end;

    if pehsRetracted in Pmac.Input.Status then
      Include(Inputs, pepRetracted)
    else
      Exclude(Inputs, pepRetracted);

    if pehsOffsetPresent in Pmac.Input^.Status then
      Include(Inputs, pepOffsetPresent)
    else
      Exclude(Inputs, pepOffsetPresent);

    if pehsRetractError in Pmac.Input.Status then
      Include(Inputs, pepRetractError)
    else
      Exclude(Inputs, pepRetractError);

    if pehsFirstSpark in Pmac.Input^.Status then begin
      Include(Inputs, pepFirstSpark);
      HadFirstSpark := True;
    end else
      Exclude(Inputs, pepFirstSpark);

    if (pepEnable in Inputs) then begin
      if Assigned(ActiveProgram) then begin
        if BlockProcessing then begin
          if pehsEDMOffRequest in Pmac.Input.Status then
            EDMStage := esWaitRetracted;

          case EDMStage of
            esWaitServoOn : begin
              Exclude(Pmac.Output^.Command, pehcForceFirstSpark);
              if pepServo in Inputs then
                EDMStage := esWaitFirstSpark;
            end;

            esWaitFirstSpark : begin
              if FirstSparkPos < Pmac.Input.Position then
                FirstSparkPos := Pmac.Input.Position;

              if (pepContinue in Inputs) or (pehsFirstSpark in Pmac.Input.Status) then begin
                SetBlock(1);
                FeedbackPos:= Pmac.Input.Position;
                EDMStage:= esWaitForFeedBack1;
              end;

              if ActiveProgram[0].Depth < (Pmac.Input.Position div 10) then
                Include(Inputs, pepFailedFirstSpark)
              else
                Exclude(Inputs, pepFailedFirstSpark);
            end;

            esWaitForFeedBack1: begin
              if FeedbackPos <> Pmac.Input.Position then begin
                if pepRedress in Inputs then
                  EDMStage := esWaitRedress
                else
                  EDMStage := esBlockProcess;
              end;
            end;

            esWaitRedress : begin
              if (pepElectrodeSense in Inputs) then begin
                Include(Pmac.Output^.Command, pehcForceFirstSpark);
                FeedbackPos:= Pmac.Input.Position;
                EDMStage:= esWaitForFeedback2;
                SetBlock(2);
              end;
              if ActiveProgram[1].Depth < (Pmac.Input.Position div 10) then
                Include(Inputs, pepFailedRedress);
            end;

            esWaitForFeedback2: begin
              // This waits until the position if forced to zero by the servo
              if Pmac.Input.Position = 0 then begin
                EDMStage := esBlockProcess;
              end;
            end;

            esBlockProcess : begin
              Exclude(Pmac.Output^.Command, pehcForceFirstSpark);
              if pehsFinalDepth in Pmac.Input.Status then begin
                Named.EventLog('EDM turned off due to HW request: pehsFinalDepth');
                EDMStage := esWaitRetracted;
                SetBlock(0);
              end;

              if not (pepDCM in Inputs) then
                if ActiveProgram[ActiveBlock].Depth < (Pmac.Input.Position div 10) then begin
                  SetBlock(ActiveBlock + 1);
                end;
            end;

            esWaitRetracted : begin
              Debug := 10;
              Exclude(Pmac.Output.Command, pehcServo);
              Include(Inputs, pepComplete);
              if pehsRetracted in Pmac.Input.Status then
                EDMStage := esComplete;
            end;

            esComplete : begin
              Debug := 11;
              FBlockProcessing := False;
              EDMStage := Low(EDMStage);
              Self.SetActiveProgram(CorrectProgramNumber);
              SetBlock(0);
            end;
          end;
        end;
        Update577;
      end;
    end;
  finally
    MK1EDMReaderWriter.Lock.Leave;
  end;
  Result := FValue;
end;

function THSD6IEDMHeadPlcItem.GetDepth : Double;
begin
  if BlockProcessing then
    Result := Pmac.Input.Position / 10000
  else
    Result := 0;
end;

procedure THSD6IEDMHeadPlcItem.Update577;
  function BitReverse(normal : byte) : byte;
  begin
    asm
      MOV AL,NORMAL
      XOR AH,0
      RCR AL, 1
      RCL AH, 1
      RCR AL, 1
      RCL AH, 1
      RCR AL, 1
      RCL AH, 1
      RCR AL, 1
      RCL AH, 1
      RCR AL, 1
      RCL AH, 1
      RCR AL, 1
      RCL AH, 1
      RCR AL, 1
      RCL AH, 1
      RCR AL, 1
      RCL AH, 1
      MOV result, AH
    end
  end;

  procedure LPE577Voltage(Voltage : Double);
  var tmp : double;
      SV : Byte;
  begin
    tmp    := Abs(voltage) - 88;
    if tmp < 0 then tmp := 0;
    SV := Round(tmp * (255 / (300 - 88)));
    LPE477.Output^.Volts := BitReverse(SV);
  end;

  procedure LPE577Peak(aPeak : Byte; Negative : Boolean);
  begin
      with LPE477.Output^ do begin
        if not Negative then Include(Control, lpe477cntlPositive)
                        else Exclude(Control, lpe477cntlPositive);

        Peak := BitReverse(aPeak and $0F);
      end;
    LastNegative := Negative;
  end;

  procedure LPE577PulseBoardsAndCoupling(aBoards : Integer; aCoupling : Integer);
  var aPeak, aPlc : Byte;
  begin
    aPeak := 0;
    aPlc := 0;

    if (1 and aCoupling) <> 0 then
      aPlc := aPlc or $80;

    if (2 and aCoupling) <> 0 then
      aPlc := aPlc or $40;

    if (4 and aCoupling) <> 0 then
      aPlc := aPlc or $20;

    if (8 and aCoupling) <> 0 then
      aPlc := aPlc or $10;

    if (16 and aCoupling) <> 0 then
      aPlc := aPlc or $8;

    if (32 and aCoupling) <> 0 then
      aPlc := aPlc or $4;


    if (1 and aBoards) <> 0 then
      aPeak := aPeak or 4;

    if (2 and aBoards) <> 0 then
      aPeak := aPeak or 8;

    if (4 and aBoards) <> 0 then
      aPeak := aPeak or 2;

    if (8 and aBoards) <> 0 then
      aPeak := aPeak or 1;

    if (16 and aBoards) <> 0 then
      aPlc := aPlc or 2;

    if (32 and aBoards) <> 0 then
      aPlc := aPlc or 1;

    with LPE477.Output^ do begin
      aPeak:= aPeak or $f0;
      Peak := (Peak or $0f) and aPeak;
      Plc := aPlc;
    end;

    LastBoards := aBoards;
    LastCoupling := aCoupling;
  end;

  procedure LPE577Cutoff(aCutoff : Double);
  var Tmp : Double;
  begin
    if aCutoff >= 0.1 then begin
      Tmp := aCutoff * (255 / 100); // Note the scaling of ASV is infact 0.0943
      LPE477.Output^.Cutoff := BitReverse(Round(Tmp));
      if pepSuppression in Inputs then
        Include(Lpe477.Output.Control, lpe477cntlCoffEna);
    end else begin
      LPE477.Output^.Cutoff := 0;
      Exclude(Lpe477.Output.Control, lpe477cntlCoffEna);
    end;
  end;

  procedure SetCapacitance(Capacitance : Integer);
  begin
    if (1 and Capacitance <> 0) then
      Include(LPE482.Output.Control, lpe482CtrlHeadCap0)
    else
      Exclude(LPE482.Output.Control, lpe482CtrlHeadCap0);

    if (2 and Capacitance <> 0) then
      Include(LPE482.Output.Control, lpe482CtrlHeadCap1)
    else
      Exclude(LPE482.Output.Control, lpe482CtrlHeadCap1);

    if (4 and Capacitance <> 0) then
      Include(LPE482.Output.Control, lpe482CtrlHeadCap2)
    else
      Exclude(LPE482.Output.Control, lpe482CtrlHeadCap2);

    if (8 and Capacitance <> 0) then
      Include(LPE482.Output.Control, lpe482CtrlHeadCap3)
    else
      Exclude(LPE482.Output.Control, lpe482CtrlHeadCap3);

    if (16 and Capacitance <> 0) then
      Include(LPE482.Output.Control, lpe482ctrlCapSelect)
    else
      Exclude(LPE482.Output.Control, lpe482ctrlCapSelect);

    if (Capacitance and $F) <> 0 then
      Include(Inputs, pepCapacitance)
    else
      Exclude(Inputs, pepCapacitance);

    LastCapacitance := Capacitance;
  end;

  procedure SetBusVoltage(aBusVoltage : Integer);
  begin
    if aBusVoltage = 78 then begin
      Include(LPE482.Output.Control, lpe482ctrlBusVoltage78V);
      Exclude(LPE482.Output.Control, lpe482ctrlBusVoltage100V);
    end else begin
      Exclude(LPE482.Output.Control, lpe482ctrlBusVoltage78V);
      Include(LPE482.Output.Control, lpe482ctrlBusVoltage100V);
    end;
    LastBusVoltage := aBusVoltage;
  end;

begin
  Exclude(Lpe477.Output.Control, lpe477cntlTrgEna);
  //Include(Lpe477.Output.Control, Lpe577CntlMode);
  if (ThisUpdate <> ActiveBlock) and (LPE577UpdateStage = 0) then
    ThisUpdate := ActiveBlock;
  with ActiveProgram[ThisUpdate] do begin
    if UpdateRequired then begin
      case LPE577UpdateStage of
        0 : begin
          FSafeChange := False;
          Include(Lpe477.Output.Control, lpe477cntlPTEna);
          LPE477.Output^.Mask   := $ff;
          LPE577Voltage(HighVoltage);
          LPE577Cutoff(ASV);

          Pmac.Output^.Gap := Round(GapVolts * 10);
          Pmac.Output^.Servo := Round(Servo * 5);
          Pmac.Output^.Spindle := Round(SpindleSpeed);
          if (1 and AltBits) <> 0 then
            Include(Inputs, pepAltBit0)
          else
            Exclude(Inputs, pepAltBit0);

          if (2 and AltBits) <> 0 then
            Include(Inputs, pepAltBit1)
          else
            Exclude(Inputs, pepAltBit1);

          if (4 and AltBits) <> 0 then
            Include(Inputs, pepAltBit2)
          else
            Exclude(Inputs, pepAltBit2);

          if (8 and AltBits) <> 0 then
            Include(Inputs, pepAltBit3)
          else
            Exclude(Inputs, pepAltBit3);

          LPE477.Output^.OnTimeL := OnTime div 10;
          LPE477.Output^.OnTimeH := OnTime div (10 * $100);

          LPE477.Output^.OffTimeL := OffTime div 10;
          LPE477.Output^.OffTimeH := OffTime div (10 * $100);

          Inc(LPE577UpdateStage);
        end;

        1, 2, 3 : begin
          LPE577UpdateStage:= 4;
        end;

        4 : begin
          Delay := 0;
          if BlockProcessing then begin
            // Starting at the highest delay and moving to the least
            if Capacitance <> LastCapacitance then
              Delay := 3000
            else if BusVoltage <> LastBusVoltage then
              Delay := 3000
            else if Negative <> LastNegative then
              Delay := 3000
            else if PulseBoards <> LastBoards then
              Delay := 3000
            else if Coupling <> LastCoupling then
              Delay := 3000;

            if Delay = 0 then begin
              LPE577UpdateStage := 6;
            end else begin
              Exclude(Lpe477.Output.Control, lpe477cntlPTEna);
              ThisTick := Windows.GetTickCount;
              LPE577UpdateStage := 5;
              FSafeChange := True;
              if pehcServo in Pmac.Output.Command then
                Include(Pmac.Output.Command, pehcFreeze);
            end;
          end else begin
            LPE577UpdateStage := 6;
          end;
        end;

        5 : begin // Wait delay
          if (ThisTick + Delay) < Windows.GetTickCount then
            LPE577UpdateStage := 6;
        end;

        6 : begin
          LPE577Peak(Peak, Negative);
          LPE577PulseBoardsAndCoupling(PulseBoards, Coupling);
          SetCapacitance(Capacitance);
          SetBusVoltage(BusVoltage);
          LPE577UpdateStage := 7;
          ThisTick := Windows.GetTickCount;
          Delay := 95;
        end;

        7 : begin
          if (ThisTick + Delay) < Windows.GetTickCount then begin
            FSafeChange := False;
            LPE577UpdateStage := 8;
            ThisTick := Windows.GetTickCount;
            Delay := 90;
          end;
        end;

        8 : begin
          if (ThisTick + Delay) < Windows.GetTickCount then begin
            LPE577UpdateStage := 9;
            Include(Lpe477.Output.Control, Lpe477CntlPTEna);
          end;
        end;

        9 : begin
          UpdateRequired := False;
          ThisUpdate := ActiveBlock;
          Exclude(Pmac.Output.Command, pehcFreeze);
          LPE577UpdateStage := 0;
        end;
      end;
    end;
  end;
  if pepEDM in Inputs then begin
    if not SafeChange then
      Include(Inputs, pepEDMRelay)
    else
      Exclude(Inputs, pepEDMRelay)
  end else begin
    Exclude(LPE482.Output.Control, lpe482CtrlHeadCap0);
    Exclude(LPE482.Output.Control, lpe482CtrlHeadCap1);
    Exclude(LPE482.Output.Control, lpe482CtrlHeadCap2);
    Exclude(LPE482.Output.Control, lpe482CtrlHeadCap3);
    Exclude(LPE482.Output.Control, lpe482ctrlCapSelect);
    Exclude(Inputs, pepCapacitance);
    Exclude(Inputs, pepEDMRelay)
  end;
end;

function THSD6IEDMHeadPlcItem.Read(var Embedded : TEmbeddedData) : Boolean;
begin
  Result := TPlcEDMHeadProperty(Embedded.Command) in Inputs;
end;

procedure THSD6IEDMHeadPlcItem.Write(var Embedded : TEmbeddedData; Line, LastLine : Boolean);
  procedure PmacCommand(pehc : TPmacEDMHeadCommand);
  begin
    if Line then
      Include(Pmac.Output.Command, pehc)
    else
      Exclude(Pmac.Output.Command, pehc);
  end;

  procedure SetElectrodeCount(aCnt : Integer);
  begin
{    ElectrodeCount := aCnt;
    if ElectrodeCount > 45 then
      ElectrodeCount := 45;
    if ElectrodeCount < 0 then
      ElectrodeCount := 0; }
  end;

  procedure EDMStateChange;
  var I, Start, Depth : Integer;
      FeatureIDStr, CutIDStr: array [0..256] of Char;
  begin
    if not (pepInvalidProgram in Inputs) then begin
      if Line then begin
        Include(Inputs, pepEDM);
        StartTime := Now;
        FBlockProcessing := True;
        EDMStage := Low(TEDMStage);
        Exclude(Inputs, pepComplete);
        HadFirstSpark := False;
        FirstSparkPos := 0;

        // Cheap and cheerful EDM on sequencing
        FSafeChange := True;
        ActiveProgram[ActiveBlock].UpdateRequired := True;
        LPE577UpdateStage := 6;
      end else begin
        Named.EventLog('Terminated by PLC request');
        Debug := 9;
        EDMStage := esWaitRetracted;
        Exclude(LPE482.Output.Control, lpe482CtrlCapSelect);
        Exclude(Inputs, pepEDM);
        if HadFirstSpark then begin
          Named.GetAsString(CutIDTag, CutIDStr, 255);
          Named.GetAsString(FeatureIDTag, FeatureIDStr, 255);
          Named.SetAsString(LoggingTag, PChar(StorageDate(StartTime) + ',' +
                                              StorageDate(Now) + ',' +
                                              IntToStr(ProgramNumber) + ',' +
                                              Format('%.3f', [FirstSparkPos / 10000]) + ',' +
                                              CNCTypes.CSVField(CutIDStr) + ','  +
                                              CNCTypes.CSVField(FeatureIDStr)
                                              ));
          HadFirstSpark := False;
        end;
      end;

      SetBlock(0);
      Exclude(Inputs, pepFailedFirstSpark);
      Exclude(Inputs, pepFailedRedress);
      Exclude(Pmac.Output.Command, pehcFreeze);

      Pmac.Output.FinalDepth := ActiveProgram[0].Depth * 10;
      if pepRedress in Inputs then
        Start := 2
      else
        Start := 1;

      Depth := 0;

      FSafeChange := False;

      for I := Start to ActiveProgram.BlockCount - 1 do
        if ActiveProgram[I].Depth > Depth then begin
          Pmac.Output.FinalDepth := ActiveProgram[I].Depth * 10;
          Depth := ActiveProgram[I].Depth;
        end;
    end;
  end;

begin
  if Line <> LastLine then begin  // Edge detect rising / falling
    if pepEnable in Inputs then begin
      case TPlcEDMHeadProperty(Embedded.Command) of
        pepEDM :         EDMStateChange;
        pepContinue :    PmacCommand(pehcLastFirstSpark);
        pepStoreFirstSpark : PmacCommand(pehcStoreFirstSpark);
        pepServo :       PmacCommand(pehcServo);
        pepDCM :         PmacCommand(pehcDCM);
        pepHoleProgram : if Line then SetActiveProgram(ProgNumEv(Self, Embedded.OData));
        pepStandoff :    if Line then Pmac.Output.Standoff := ProgNumEv(Self, Embedded.OData);
//        pepElectrodeCount : if Line then SetElectrodeCount(ProgNumEv(Self, Embedded.OData));
        pepFromFirstSpark : PmacCommand(pehcFromFirstSpark);
        pepFromDepth :   PmacCommand(pehcFromDepth);
        pepRemoveOffset : PmacCommand(pehcRemoveOffset);
        pepNextBlock : if Line and BlockProcessing then
                         SetBlock(ActiveBlock + 1);
        pepRefresh :
         if Line and Assigned(ActiveProgram) then
                         ActiveProgram[ActiveBlock].UpdateRequired := True;

        pepForceFirstSpark : PmacCommand(pehcForceFirstSpark);
        pepReverseDrilling : PmacCommand(pehcReverseDrilling);
        pepRapidRecovery : PmacCommand(pehcRapidRecovery);
        pepResetIDetect :
          if Line then
            Include(LPE482.Output.Control, lpe482ctrlResetIDetect)
          else
            Exclude(LPE482.Output.Control, lpe482ctrlResetIDetect);

        pepSuppression : if Line then
            Include(LPE477.Output.Control, lpe477cntlCoffEna)
          else
            Exclude(LPE477.Output.Control, lpe477cntlCoffEna);

        pepTFReset : if Line then
            Include(LPE477.Output.Control, lpe477cntlTFReset)
          else
            Exclude(LPE477.Output.Control, lpe477cntlTFReset);

        pepPTEnable : if Line then
            Include(LPE477.Output.Control, lpe477cntlPTEna)
          else
            Exclude(LPE477.Output.Control, lpe477cntlPTEna);

      end;
    end;
    if Line then
      Include(Inputs, TPlcEDMHeadProperty(Embedded.Command))
    else
      Exclude(Inputs, TPlcEDMHeadProperty(Embedded.Command));
  end;
end;

procedure THSD6IEDMHeadPlcItem.SetActiveProgram(ProgID : Integer);
begin
  Lock.Enter;
  try
    CorrectProgramNumber:= ProgID;
    if not BlockProcessing then begin
      ProgramNumber := ProgID;
      FActiveProgram := EDMConsumer.EDMSection.FindProgram(ProgramNumber);
      if not Assigned(ActiveProgram) then begin
        Include(Inputs, pepInvalidProgram)
      end else begin
        Exclude(Inputs, pepInvalidProgram);
        SetBlock(0);
      end;
    end else
      Named.EventLog('Failed program selection');
  finally
    Lock.Leave;
  end;
end;

procedure THSD6IEDMHeadPlcItem.NewProgram(Sender : TObject);
begin
  SetActiveProgram(CorrectProgramNumber);
end;


function THSD6IEDMHeadPlcItem.GateName(var Embedded : TEmbeddedData) : string;
begin
  Result := PropertyName[TPlcEDMHeadProperty(Embedded.Command)].Name;
end;

function THSD6IEDMHeadPlcItem.ItemProperties : string;
var I : TPlcEDMHeadProperty;
begin
  Result := 'Program' + CarRet + IntToStr(ProgramNumber) + CarRet;
  for I := Low(TPlcEDMHeadProperty) to High(TPlcEDMHeadProperty) do
    Result := Result + PropertyName[I].Name + CarRet + BooleanIdent[I in Inputs] + CarRet;

  Result := Result + 'Debug' + CarRet + IntToStr(Debug) + CarRet;
  Result := Result + 'EDMStage' + CarRet + IntToStr(Ord(EDMStage)) + CarRet;
  Result := Result + 'ParamStage' + CarRet + IntToStr(LPE577UpdateStage) +CarRet;
  Result := Result + 'Delay' + CarRet + IntToStr(Delay) + CarRet;
  Result := Result + 'Elect. Map' + CarRet + IntToHex(ElectrodeMap.I, 12) + CarRet;
  Result := Result + 'Act. Elect. Map' + CarRet + IntToHex(ActualElectrodeMap, 12) + CarRet;
  Result := Result + 'VMean' + CarRet + IntToStr(VCapture.Mean) + CarRet;
  Result := Result + 'IMean' + CarRet + IntToStr(ICapture.Mean) + CarRet;
  Result := Result + 'GapV' + CarRet + IntToStr(Pmac.Input^.GapVolts);
end;

class procedure THSD6IEDMHeadPlcItem.FinalInstall;
begin
  EMapTag := Named.AquireTag('OPP_ElectrodeMap', 'TStringTag', EMapChange);
  FeatureIDTag:= Named.AquireTag('FeatureID', 'TStringTag', nil);
  CutIDTag:= Named.AquireTag('CutID', 'TStringTag', nil);

  THsd6Web.Create;
end;

class procedure THSD6IEDMHeadPlcItem.Install;
begin
  ActEMapTag := Named.AddTag('HSD6IIEDMHeadActElectrodeMask', 'TStringTag');
  EmapCofNTag := Named.AddTag('HSD6IISenseCofN', 'TStringTag');

  LoggingTag := Named.AddTag('EDMRecord1', 'TStringTag');
end;

class function THSD6IEDMHeadPlcItem.Properties : string;
var I : TPlcEDMHeadProperty;
begin
  Result := '';
  for I := Low(TPlcEDMHeadProperty) to High(TPlcEDMHeadProperty) do
    Result := Result + PropertyName[I].Name + CarRet;
end;

{ TDataCapture }

constructor TDataCapture.Create;
begin
  FIntegration := 10;
end;

procedure TDataCapture.AddData(aValue: Integer);
begin
//  Data[0] := aValue;
//  FMean := aValue;
  Data[Ndx] := aValue;
  Ndx := (Ndx + 1) mod Integration;
end;

procedure TDataCapture.UpdateMinMax;
var I : Integer;
begin
  FMin := Data[0];
  FMax := FMin;
  FMean := FMin;
  for I := 1 to Integration - 1 do begin
    if FMin > Data[I] then
      FMin := Data[I];
    if FMax < Data[I] then
      FMax := Data[I];
    FMean := FMean + Data[I];
  end;

  FMean := FMean div Integration;
//  FMean := Data[0];
//  FMean := 40 + (IOSweep^ mod 30);
//  FMax := 60;
//  FMin := 40;
end;

procedure TDataCapture.UpdateExtended;
begin

end;


procedure TDataCapture.SetIntegration(aInt: Integer);
begin
  if aInt > 50 then
    aInt := 50;
  if aInt < 2 then
    aInt := 2;
  FIntegration := aInt;
end;

{ THsd6Web }

constructor THsd6Web.Create;
begin
  HttpServerHost.AddComponentPath(HttpHelpSection, 'hsd6ii', 'Edm control', Self);
end;

function THsd6Web.HTTPAddStreaming(Stream: THandle; CGIR: WideString;
  Method: IHTTPServer; DT: Integer): Boolean;
begin

end;

function THsd6Web.HTTPGeneratePage(HFile: THANDLE; var CGIPath: WideString;
  CGIRequest: ICGIStringList): Boolean;
begin
  WriteACNC32HTTPTitle(hFile, 'HSD6II EDM Controller');
  WriteACNC32HTTPHeading(hFile, 2, 'EDM Control');
end;

procedure THsd6Web.HTTPRemoveStreaming(Stream: THandle; CGIR: WideString);
begin

end;

initialization
  THSD6IEDMHeadPlcItem.AddPlugin('HSD6IEDMHead', THSD6IEDMHeadPlcItem);
end.


