unit MK1EDMReaderWriter;

interface

uses Windows, Classes, Tokenizer, SysUtils, AbstractOPPPlugin, NamedPlugin,
     INamedInterface, SyncObjs;

type

  TMK1EDMSection = class;
  TMK1EDMBlock = class(TObject)
    FUpdateRequired : Boolean; // Used by block processor to determine a parameter change
  public
    GapVolts : Double;
    Servo : Double;
    Peak : Integer;
    HighVoltage : Integer;
    PulseBoards : Integer;
    OnTime : Integer;
    OffTime : Integer;
    SuppOnTime : Integer;
    SuppOffTime : Integer;
    Depth : Integer;
    Negative : Boolean;
    Capacitance : Integer;
    ASV : Double;
    Coupling : Integer;
    BusVoltage : Integer;
    AltBits : Integer;
    SpindleSpeed : Double;
    RRDepth: Integer;
    RRServo: Double;
    HP: Double;

    constructor Create;
    procedure ReadFromStrings(S : TMK1EDMSection);
    procedure WriteToStrings(S : TStrings);
    property UpdateRequired : Boolean read FUpdateRequired write FUpdateRequired;
    procedure Assign(Block : TMK1EDMBlock);
  end;

  TMK1EDMProgram = class(TObject)
  private
    FBlocks : TList;
    EDMSection : TMK1EDMSection;
    function GetBlock(Index : Integer) : TMK1EDMBlock;
    function GetBlockCount : Integer;
    procedure Clean;
  public
    ID : Integer;
    Comment : string;
    constructor Create(aEDMSection : TMK1EDMSection);
    destructor Destroy; override;
    procedure ReadFromStrings(S : TMK1EDMSection);
    procedure WriteToStrings(S : TStrings);
    property BlockCount : Integer read GetBlockCount;
    property Blocks [Index : Integer]: TMK1EDMBlock read GetBlock; default;
    function NewBlock : TMK1EDMBlock;
    procedure DeleteBlock(Index : Integer);
    function Verify(aBlock : Integer) : Boolean;
  end;

  TMK1EDMSection = class(TQuoteTokenizer)
  private
    FCount : Integer;
    FPrograms : TList; //array of TEDMProgram;
    FDirty : Boolean;
    function GetProgram(Index : Integer) : TMK1EDMProgram;
    procedure CleanProgram;
    function GetProgramCount : Integer;
  public
    constructor Create;
    procedure ReadFromStrings;
    procedure WriteToStrings;
    function NewProgram : TMK1EDMProgram;
    function FindProgram(Index : Integer) : TMK1EDMProgram;
    property ProgramCount : Integer read GetProgramCount;
    property Programs[Index : Integer] : TMK1EDMProgram read GetProgram; default;
    procedure DeleteProgram(aProgram : TMK1EDMProgram);
    property Dirty : Boolean read FDirty;
  end;

  TMK1EDMSectionConsumer = class(TAbstractOPPPluginConsumer)
    FReading : Boolean;
  public
    EDMSection : TMK1EDMSection;
    constructor Create(aSectionName : string); override;
    procedure Consume(aSectionName, Buffer : PChar); override;
    function Dirty : Boolean; override;
    property Reading : Boolean read FReading;
    procedure FinalInstall; override;
  end;

const
  HSD6IISectionName = 'HSD6IIEDM';

var
  EDMConsumer : TMK1EDMSectionConsumer;
  MetricToInch : Double;
  InchModeTag : TTagRef;
  Lock : TCriticalSection;


implementation

const
  HoleProgramName = 'HoleProgram';
  BlockName = 'Block';

  GapVoltsName = 'GapVolts';
  ServoName = 'Servo';
  PeakName = 'Peak';
  HighVoltageName = 'HighVoltage';
  PulseBoardsName = 'PulseBoards';
  OnTimeName = 'OnTime';
  OffTimeName = 'OffTime';
  SuppOnTimeName = 'SuppOnTime';
  SuppOffTimeName = 'SuppOffTime';
  DepthName = 'Depth';
  NegativeName = 'Negative';
  CapacitanceName = 'Capacitance';
  ASVName = 'ASV';
  CouplingName = 'Coupling';
  BusVoltageName = 'BusVoltage';
  AltBitsName = 'AltBits';
  SpindleSpeedName = 'SpindleSpeed';
  RRDepthName = 'RRDepth';
  RRServoName = 'RRServo';
  HPName = 'HP';

  IDName = 'ID';
  CommentName = 'Comment';

  RRDepthDefault = 0;
  RRServoDefault = 3000;
  HPDefault = 1000 / 14.7;

function ApproxEqual(const A, B: double): Boolean;
begin
  Result:= Abs(A - B) < 0.001;
end;

constructor TMK1EDMBlock.Create;
begin
  GapVolts := 80;
  Servo := 20;
  Peak := 1;
  HighVoltage := 150;
  PulseBoards := 1;
  OnTime := 1000;
  OffTime := 5000;
  SuppOnTime := 600;
  SuppOffTime := 10000;
  Depth := 0;
  Negative := True;
  Capacitance := 0;
  ASV := 0;
  Coupling := 0;
  BusVoltage := 78;
  AltBits := 0;
  SpindleSpeed := 0;
  FUpdateRequired := True;

  RRDepth:= RRDepthDefault;
  RRServo:= RRServoDefault;
  HP:= HPDefault;
end;

procedure TMK1EDMBlock.Assign(Block : TMK1EDMBlock);
begin
  GapVolts := Block.GapVolts;
  Servo := Block.Servo;
  Peak := Block.Peak;
  HighVoltage := Block.HighVoltage;
  PulseBoards := Block.PulseBoards;
  OnTime := Block.OnTime;
  OffTime := Block.OffTime;
  SuppOnTime := Block.SuppOnTime;
  SuppOffTime := Block.SuppOffTime;
  Depth := Block.Depth;
  Negative := Block.Negative;
  Capacitance := Block.Capacitance;
  ASV := Block.ASV;
  Coupling := Block.Coupling;
  BusVoltage := Block.BusVoltage;
  AltBits := Block.AltBits;
  SpindleSpeed := Block.SpindleSpeed;
  FUpdateRequired := Block.FUpdateRequired;
  RRDepth:= Block.RRDepth;
  RRServo:= Block.RRServo;
  HP:= Block.HP;
end;

procedure TMK1EDMBlock.ReadFromStrings(S : TMK1EDMSection);
  function ReadDoubleAssign : Double;
  var Code : Integer;
  begin
    S.ExpectedToken('=');
    Val(S.Read, Result, Code);
    if Code <> 0 then
      raise Exception.CreateFmt('Cannot convert to floating point [%s]', [S.LastToken]);
  end;

var Token : string;
begin
  S.ExpectedTokens([BlockName, '=', '{']);
  Token := S.Read;
  while not S.EndOfStream do begin
    if Token = GapVoltsName then
      GapVolts := ReadDoubleAssign
    else if Token = ServoName then
      Servo := ReadDoubleAssign
    else if Token = PeakName then
      Peak := S.ReadIntegerAssign
    else if Token = HighVoltageName then
      HighVoltage := S.ReadIntegerAssign
    else if Token = PulseBoardsName then
      PulseBoards := S.ReadIntegerAssign
    else if Token = OnTimeName then
      OnTime := S.ReadIntegerAssign
    else if Token = OffTimeName then
      OffTime:= S.ReadIntegerAssign
    else if Token = SuppOnTimeName then
      SuppOnTime := S.ReadIntegerAssign
    else if Token = SuppOffTimeName then
      SuppOffTime := S.ReadIntegerAssign
    else if Token = DepthName then
      Depth := S.ReadIntegerAssign
    else if Token = NegativeName then
      Negative := S.ReadBooleanAssign
    else if Token = CapacitanceName then
      Capacitance := S.ReadIntegerAssign
    else if Token = ASVName then
      ASV := ReadDoubleAssign
    else if Token = CouplingName then
      Coupling := S.ReadIntegerAssign
    else if Token = BusVoltageName then
      BusVoltage := S.ReadIntegerAssign
    else if Token = AltBitsName then
      AltBits := S.ReadIntegerAssign
    else if Token = SpindleSpeedName then
      SpindleSpeed := ReadDoubleAssign
    else if Token = RRServoName then
      RRServo := ReadDoubleAssign
    else if Token = RRDepthName then
      RRDepth := S.ReadIntegerAssign
    else if Token = HPName then
      HP := ReadDoubleAssign
    else if Token = '}' then
      Exit
    else
      raise Exception.CreateFmt('Unexpected Token [%s]', [Token]);
    Token := S.Read;
  end;
  raise Exception.Create('Unexpected end of stream');
end;

procedure TMK1EDMBlock.WriteToStrings(S : TStrings);
  procedure SOut(const aText : string);
  begin
    S.Add('      ' + aText);
  end;
begin
  S.Add('    ' + BlockName + ' = {');
  SOut(Format('%s = %f', [GapVoltsName, GapVolts]));
  SOut(Format('%s = %f', [ServoName, Servo]));
  SOut(Format('%s = %d', [PeakName, Peak]));
  SOut(Format('%s = %d', [HighVoltageName, HighVoltage]));
  SOut(Format('%s = %d', [PulseBoardsName, PulseBoards]));
  SOut(Format('%s = %d', [OnTimeName, OnTime]));
  SOut(Format('%s = %d', [OffTimeName, OffTime]));
  SOut(Format('%s = %d', [SuppOnTimeName, SuppOnTime]));
  SOut(Format('%s = %d', [SuppOffTimeName, SuppOffTime]));
  SOut(Format('%s = %d', [DepthName, Depth]));
  SOut(Format('%s = %d', [NegativeName, Ord(Negative)]));
  SOut(Format('%s = %d', [CapacitanceName, Capacitance]));
  SOut(Format('%s = %f', [ASVName, ASV]));
  SOut(Format('%s = %d', [CouplingName, Coupling]));
  SOut(Format('%s = %d', [BusVoltageName, BusVoltage]));
  SOut(Format('%s = %d', [AltBitsName, AltBits]));
  SOut(Format('%s = %f', [SpindleSpeedName, SpindleSpeed]));

  if not ApproxEqual(RRServo, RRServoDefault) then
    SOut(Format('%s = %f', [RRServoName, RRServo]));

  if not ApproxEqual(RRDepth, RRDepthDefault) then
    SOut(Format('%s = %d', [RRDepthName, RRDepth]));

  if not ApproxEqual(HP, HPDefault) then
    SOut(Format('%s = %f', [HPName, HP]));

  S.Add('    }');
end;


constructor TMK1EDMProgram.Create(aEDMSection : TMK1EDMSection);
begin
  inherited Create;
  FBlocks := TList.Create;
  EDMSection := aEDMSection;
end;

procedure TMK1EDMProgram.Clean;
begin
  while BlockCount > 0 do begin
    Blocks[0].Free;
    FBlocks.Delete(0);
  end;
end;

destructor TMK1EDMProgram.Destroy;
begin
  Clean;
  inherited;
end;

function TMK1EDMProgram.GetBlock(Index : Integer) : TMK1EDMBlock;
begin
  Result := FBlocks[Index];
end;

function TMK1EDMProgram.GetBlockCount : Integer;
begin
  Result := FBlocks.Count;
end;

function TMK1EDMProgram.NewBlock : TMK1EDMBlock;
begin
  Result := TMK1EDMBlock.Create;
  FBlocks.Add(Result);
end;

procedure TMK1EDMProgram.DeleteBlock(Index : Integer);
begin
  if (Index < BlockCount) and (BlockCount > 0) and (Index >= 0) then begin
    Blocks[Index].Free;
    FBlocks.Delete(Index);
  end;
end;

function TMK1EDMProgram.Verify(aBlock : Integer) : Boolean;
var Duty : Double;
  function DoubleBounds(Current : Double; Min, Max : Double) : Double;
  begin
    Result := Current;
    if Result > Max then
      Result := Max
    else if Current < Min then
      Result := Min;
  end;

  function IntegerBounds(Current, Min, Max : Integer) : Integer;
  begin
    Result := Current;
    if Result > Max then
      Result := Max
    else if Current < Min then
      Result := Min;
  end;

begin
  EDMSection.FDirty := True;
  with Blocks[aBlock] do begin
// Bounds check each parameter
    GapVolts := DoubleBounds(GapVolts, 0, 150);
    Servo := DoubleBounds(Servo, 0, 12800);
    Peak := IntegerBounds(Peak, 0, 15);
    HighVoltage := IntegerBounds(HighVoltage, 88, 300);
    PulseBoards := IntegerBounds(PulseBoards, 0, 63);
    OnTime := IntegerBounds(OnTime, 500, 655360);
    OffTime := IntegerBounds(OffTime, 1000, 655360);
    SuppOnTime := IntegerBounds(SuppOnTime, 500 ,655360 );
    SuppOffTime := IntegerBounds(SuppOffTime,1000 ,655360 );
    Depth := IntegerBounds(Depth, 0, 100000);
    //  Negative := Boolean;
    Capacitance := IntegerBounds(Capacitance, 0, 31);
    ASV := DoubleBounds(ASV, 0, 50);
    Coupling := IntegerBounds(Coupling, 0, 63);
    AltBits := IntegerBounds(AltBits, 0, 15);
    SpindleSpeed := DoubleBounds(SpindleSpeed, 0, 1000);
    RRDepth:= IntegerBounds(RRDepth, 0, 65535);
    RRServo:= DoubleBounds(RRServo, 0, 12800);
    HP:= DoubleBounds(HP, 500 / 14.7, 1500 / 14.7);

    if (BusVoltage <> 78) and (BusVoltage <> 100) then
      BusVoltage := 78;

    OnTime := (OnTime div 10) * 10;
    OffTime := (OffTime div 10) * 10;
    SuppOnTime := (SuppOnTime div 10) * 10;
    SuppOffTime := (SuppOffTime div 10) * 10;
  // Check Duty cycle is less than 90%
    Duty := OnTime / (OnTime + OffTime);

    if Peak >= 6 then begin
      if Duty > 0.9 then
        OffTime := OnTime div 9;
    end else begin
      if Duty > 0.3 then
        OffTime := OnTime * 7 div 3;
    end;

    if SuppOnTime > OnTime then
      SuppOnTime := OnTime;

    if HighVoltage - BusVoltage > 200 then
      HighVoltage := BusVoltage + 200;

    FUpdateRequired := True;
  end;
  Result := True;
end;

procedure TMK1EDMProgram.ReadFromStrings(S : TMK1EDMSection);
var Token : string;
  procedure AddBlock;
  begin
    S.Push;
    NewBlock.ReadFromStrings(S);
  end;

begin
  S.ExpectedTokens([HoleProgramName, '=', '{']);
  Clean;
  Token := S.Read;
  while not S.EndOfStream do begin
    if Token = IDName then
      ID := S.ReadIntegerAssign
    else if Token = CommentName then
      Comment := S.ReadStringAssign
    else if Token = BlockName then
      AddBlock
    else if Token = '}' then
      Exit;
    Token := S.Read;
  end;
  raise Exception.Create('Unexpected end of stream');
end;

procedure TMK1EDMProgram.WriteToStrings(S : TStrings);
  procedure SOut(const aText : string);
  begin
    S.Add('    ' + aText);
  end;

var I : Integer;
begin
  S.Add('  ' + HoleProgramName + '= {');
  SOut(Format('%s = %d', [IDName, ID]));
  SOut(Format('%s = ''%s''', [CommentName, Comment]));
  for I := 0 to BlockCount - 1 do begin
    Blocks[I].WriteToStrings(S);
  end;
  S.Add('  }');
end;

constructor TMK1EDMSection.Create;
begin
  inherited Create;
  FPrograms := TList.Create;
  Seperator := '{}=';
  Self.QuoteChar := '''';
  FCount := 0;
end;

procedure TMK1EDMSection.ReadFromStrings;
var Token : string;
  procedure AddProgram;
  begin
    Push;
    NewProgram.ReadFromStrings(Self);
  end;

begin
  CleanProgram;
  if Self.Count = 0 then
    Exit;
  Rewind;
  Self.Read;
  try
    if not EndOfStream then begin // Detect an Empty program
      Self.Push;
      ExpectedTokens([HSD6IISectionName, '=', '{']);
      Token := Self.Read;
      while not EndOfStream do begin
        if Token = HoleProgramName then
          AddProgram
        else if Token = '}' then
          Exit
        else
          raise Exception.CreateFmt('Invalid token [%s]', [Token]);
        Token := Self.Read;
      end;
    end;
  except
    on E : Exception do
      raise Exception.CreateFmt('%s at line #%d in %s', [E.message, Self.Line, HSD6IISectionName]);
  end;
end;

procedure TMK1EDMSection.WriteToStrings;
var I : Integer;
begin
  Clear;
  Add(HSD6IISectionName + ' = {');
  for I := 0 to ProgramCount - 1 do begin
    if Programs[I].BlockCount > 0 then
      Programs[I].WriteToStrings(Self);
  end;
  Add('}');
end;

function TMK1EDMSection.NewProgram : TMK1EDMProgram;
begin
  Result := TMK1EDMProgram.Create(Self);
  FPrograms.Add(Result);
  Result.ID := 1;
  Result.Comment := '';
end;

function TMK1EDMSection.FindProgram(Index : Integer) : TMK1EDMProgram;
var I : Integer;
begin
  for I := 0 to ProgramCount - 1 do begin
    if Programs[I].ID = Index then begin
      Result := Programs[I];
      Exit;
    end;
  end;
  Result := nil;
end;

procedure TMK1EDMSection.DeleteProgram(aProgram : TMK1EDMProgram);
var I : Integer;
begin
  for I := 0 to ProgramCount - 1 do begin
    if aProgram = Programs[I] then begin
      aProgram.Free;
      FPrograms.Delete(I);
      Exit;
    end;
  end;
end;

function TMK1EDMSection.GetProgram(Index : Integer) : TMK1EDMProgram;
begin
  Result := FPrograms[Index];
end;

function TMK1EDMSection.GetProgramCount : Integer;
begin
  Result := FPrograms.Count;
end;

procedure TMK1EDMSection.CleanProgram;
begin
  FDirty := False;
  while FPrograms.Count > 0 do begin
    Programs[0].Free;
    FPrograms.Delete(0);
  end;
end;

constructor TMK1EDMSectionConsumer.Create(aSectionName : string);
begin
  inherited Create(aSectionName);
  EDMSection := TMK1EDMSection.Create;
  // Because only one will be created we can store this instance in a global
  // variable EDMConsumer
  EDMConsumer := Self;
end;

procedure TMK1EDMSectionConsumer.Consume(aSectionName, Buffer : PChar);
begin
  Lock.Enter;
  try
    EDMSection.Clear;
    EDMSection.Text := Buffer;
    FReading := True;
    if Named.GetAsBoolean(InchModeTag) then
      MetricToInch := 25.4
    else
      MetricToInch := 1;

    Sleep(10);
    try
      EDMSection.ReadFromStrings;
      NotifyClients;
    finally
      FReading := False;
    end;
  finally
    Lock.Release;
  end;
end;

function TMK1EDMSectionConsumer.Dirty : Boolean;
begin
  Result := EDMSection.Dirty;
end;

procedure TMK1EDMSectionConsumer.FinalInstall;
begin
  InchModeTag := NAmed.AquireTag('NC1InchModeDefault', PChar('TMethodTag'), nil);
end;

initialization
  Lock := TCriticalSection.Create;
  MetricToInch := 1;
  TMK1EDMSectionConsumer.AddPlugin(HSD6IISectionName, TMK1EDMSectionConsumer);
end.
