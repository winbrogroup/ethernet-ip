unit SimpleMeter;

interface

uses Classes, WinTypes, Graphics, SysUtils, WidgetTypes, WidgetInterface,
     NamedPlugin, EDMPlc;
type
  TLedColors = 0..2;

  TPlcCaptureDataType = (
    pmdtVoltage,
    pmdtCurrent
  );

  TSimplePanelMeter = class(TPluginWidget)
  private
    FVertical : Boolean;
    FLeds : Integer;
    FLowLeds : Integer;
    FHighLeds : Integer;
    FMaxValue : Double;
    FMinValue : Double;
    FGap : Integer;
    FTest : Boolean;
    Brushs : array [TLedColors, Boolean] of TBrush;
    Pens : array [TLedColors, Boolean] of TPen;
    BGBrush : TBrush;
    BGPen : TPen;
    FCallback : TPWEv;
    Colors : array [TLedColors] of TColor;
    CallbackName : string;
    Color : TColor;
    DataType : TPlcCaptureDataType;
    procedure SetColors;
    function DimColor(aCol : TColor; Pc : Double) : TColor;
    function LedIsOn(aValue : Double; aLed : Integer) : Boolean;
    procedure PaintLed(aLed : Integer);
  public
    constructor Create; override;
    destructor Destroy; override;
    procedure Paint; override;
    function SetProperty(aToken, aValue : PChar) : Boolean; override;
    procedure MouseDown(Button: Integer; Shift: Byte; X, Y: Integer); override;
    procedure MouseUp(Button: Integer; Shift: Byte; X, Y: Integer); override;
    procedure SetCallback(aName : PChar; Callback : TPWEv); override;
    procedure SetIntegerInput(const Name : string; aValue : Double); override;
    class function GetProperties : PChar; override;
    property Vertical : Boolean read FVertical write FVertical;
    property Leds : Integer read FLeds write FLeds;
    property LowLeds : Integer read FLowLeds write FLowLeds;
    property HighLeds : Integer read FHighLeds write FHighLeds;
    property MaxValue : Double read FMaxValue write FMaxValue;
    property MinValue : Double read FMinValue write FMinValue;
    property Gap : Integer read FGap write FGap;
    property Test : Boolean read FTest write FTest;
    procedure SetBounds(aWidth, aHeight : Integer); override;
  end;

  TIntegratedPanelMeter = class(TSimplePanelMeter)
  private
    Min, Max : Double;
    BlackB : TBrush;
    BlackP : TPen;
    FExtendedCapture : Boolean;
    Font : TFont;
  protected
    procedure PaintLed(aLEd : Integer);
    procedure PaintCaptureBar(aValue, aLastValue : Double; B : TBrush; P : TPen);
  public
    constructor Create; override;
    procedure Paint; override;
    function SetProperty(aToken, aValue : PChar) : Boolean; override;
    procedure SetIntegerInput(const Name : string; aValue : Double); override;
    class function GetProperties : PChar; override;
    property ExtendedCapture : Boolean read FExtendedCapture write FExtendedCapture;
  end;

implementation

uses Math;

const
  SimpleProperties =
  'vertical; ' +
  'gap; ' +
  'leds; ' +
  'value; ' +
  'maxvalue; ' +
  'minvalue; ' +
  'lowleds; ' +
  'highleds; ' +
  'test;' +
  'lowcolor; ' +
  'normalcolor; ' +
  'highcolor; ' +
  'color; ' +
  'datatype; ';

  IntegratedProperties =
  'integration; ' + 'extendedcapture; ';

//procedure SimpleMeterDispatch(); stdcall;
constructor TSimplePanelMeter.Create;
var I : Boolean;
    lt : Integer;

begin
  inherited;
  for lt := 0 to 2 do begin
    for I := False to True do begin
      Brushs[lt, I] := TBrush.Create;
      Pens[lt, I] := TPen.Create;
      Pens[lt, I].Width := 1;
    end;
  end;

  BGBrush := TBrush.Create;
  BGPen := TPen.Create;
  BGPen.Width := 2;

  FVertical := True;
  FLeds := 20;
  FLowLeds := 2;
  FHighLeds := 2;
  FValue := 7;
  FMaxValue := 10;
  FMinValue := 0;
  Colors[0] := clYellow;
  Colors[1] := clLime;
  Colors[2] := clRed;
  FGap := 2;
  SetColors;
end;

destructor TSimplePanelMeter.Destroy;
var I : Boolean;
    lt : Integer;
begin
  for lt := 0 to 2 do begin
    for I := False to True do begin
      if Assigned(Pens[lt, I]) then
        Pens[lt, I].Free;
      if Assigned(Brushs[lt, I]) then
        Brushs[lt, I].Free;
    end;
  end;
  inherited Destroy;
end;

function TSimplePanelMeter.DimColor(aCol : TColor; Pc : Double) : TColor;
var R, B, G : Byte;
begin
  R := Byte(aCol);
  G := Byte(aCol shr 8);
  B := Byte(aCol shr 16);

  R := Round((R * Pc) / 100);
  G := Round((G * Pc) / 100);
  B := Round((B * Pc) / 100);

  Result := R + (G shl 8) + (B shl 16);
end;

procedure TSimplePanelMeter.SetColors;
var lt : Integer;
begin
  for lt := 0 to 2 do begin
    Pens[lt, True].Color := DimColor(Colors[lt], 30);
    Brushs[lt, True].Color := Colors[lt];
  end;
  for lt := 0 to 2 do begin
    Pens[lt, False].Color := DimColor(Colors[lt], 30);
    Brushs[lt, False].Color := Pens[lt, False].Color;
  end;

  BGBrush.Color := Color;
  BGPen.Color := DimColor(Color, 50);
end;

function TSimplePanelMeter.LedIsOn(aValue : Double; aLed : Integer) : Boolean;
begin
  Result := ((aValue - MinValue) / (MaxValue - MinValue) * Leds) -0.5 > aLed;
end;

procedure TSimplePanelMeter.PaintLed(aLed : Integer);
var
    R : TRect;
    lt, TH : Integer;
begin
  R.Left := Gap;
  R.Right := Width - Gap;

  if aLed < LowLeds then
    lt := 0
  else if aLed >= (Leds - HighLeds) then
    lt := 2
  else
    lt := 1;

  TH := Height;
  SelectObject(HDC, Pens[lt, LedIsOn(Value, aLed)].Handle);
  SelectObject(HDC, Brushs[lt, LedIsOn(Value, aLed)].Handle);
  if Vertical then begin
    R.Bottom := TH - (TH * aLed div Leds);
    R.Top := TH - (TH * (aLed+1) div Leds - Gap);
    Rectangle(HDC, R.Left, R.Top, R.Right, R.Bottom );
  end else begin
    R.Bottom := TH - (TH * aLed div Leds);
    R.Top := TH - (TH * (aLed+1) div Leds - Gap);
    Rectangle(HDC, R.Left, R.Top, R.Right, R.Bottom );
  end;
end;

procedure TSimplePanelMeter.SetIntegerInput(const Name : string; aValue : Double);
var I : Integer;
    OldValue : Double;
begin
  if Value <> aValue then begin
    OldValue := Value;
    FValue := Round(aValue) mod Round(FMaxValue);
    for I := 0 to Leds - 1 do
      if LedIsOn(FValue, I) <> LedIsOn(OldValue, I) then
        PaintLed(I);
  end;
end;

procedure TSimplePanelMeter.Paint;
var I : Integer;
begin
  SelectObject(HDC, BGBrush.Handle);
  SelectObject(HDC, BGPen.Handle);
  Rectangle(HDC, 0, 0, Width, Height);
  for I := 0 to Leds - 1 do
    PaintLed(I);
end;

function TSimplePanelMeter.SetProperty(aToken, aValue : PChar) : Boolean;
  function DataTypeValue(aVal : string) : TPlcCaptureDataType;
  begin
    if LowerCase(aVal) = 'current' then
      Result := pmdtCurrent
    else
      Result := pmdtVoltage;
  end;

var Tmp : string;
begin
  Result := inherited SetProperty(aToken, aValue);
  Tmp := LowerCase(aToken);
  if not Result then begin
    Result := True;
    if Tmp = 'vertical' then
      Vertical := BooleanValue(aValue)
    else if Tmp = 'gap' then
      Gap := IntegerValue(aValue)
    else if Tmp = 'leds' then
      Leds := IntegerValue(aValue)
    else if Tmp = 'value' then
      SetIntegerInput('value', DoubleValue(aValue))
    else if Tmp = 'maxvalue' then
      MaxValue := DoubleValue(aValue)
    else if Tmp = 'minvalue' then
      MinValue := DoubleValue(aValue)
    else if Tmp = 'lowleds' then
      LowLeds := IntegerValue(aValue)
    else if Tmp = 'highleds' then
      HighLeds := IntegerValue(aValue)
    else if Tmp = 'test' then
      Test := BooleanValue(aValue)
    else if Tmp = 'lowcolor' then
      Colors[0] := ColorValue(aValue)
    else if Tmp = 'normalcolor' then
      Colors[1] := ColorValue(aValue)
    else if Tmp = 'highcolor' then
      Colors[2] := ColorValue(aValue)
    else if Tmp = 'color' then
      Color := ColorValue(aValue)
    else if Tmp = 'datatype' then
      DataType := DataTypeValue(aValue)
    else
      Result := False;
  end;

  SetColors;
  if MaxValue <= MinValue then
    MaxValue := MinValue + 0.0001;
end;

procedure TSimplePanelMeter.MouseDown(Button: Integer; Shift: Byte; X, Y: Integer);
begin
end;

procedure TSimplePanelMeter.MouseUp(Button: Integer; Shift: Byte; X, Y: Integer);
begin
  if Assigned(FCallback) then
    FCallback(Integer(Self), PChar(CallbackName), PChar(IntToStr(Round(Value))));
end;

procedure TSimplePanelMeter.SetCallback(aName : PChar; Callback : TPWEv);
begin
  CallbackName := aName;
  FCallback := Callback;
end;

procedure TSimplePanelMeter.SetBounds(aWidth, aHeight : Integer);
begin
  Width := aWidth;
  Height := aHeight;
end;

class function TSimplePanelMeter.GetProperties : PChar;
begin
  Result := PChar(inherited GetProperties + SimpleProperties);
end;


constructor TIntegratedPanelMeter.Create;
begin
  inherited Create;
  BlackB := TBrush.Create;
  BlackB.Color := clBlack;

  BlackP := TPen.Create;
  BlackP.Width := 2;
  BlackP.Color := clBlack;

  Font := TFont.Create;
  Font.Name := 'Verdana';
  Font.Size := 10;
  Font.Style := [fsBold];
end;

procedure TIntegratedPanelMeter.PaintCaptureBar(aValue, aLastValue : Double; B : TBrush; P : TPen);
var R : TRect;
begin
  R.Left := 2;
  R.Right := Width div 3 - 2;

  // Unpaint the old
  R.Top := Round(Height - (Height * (aLastValue / MaxValue)));
  R.Bottom := R.Top + 8;
  SelectObject(HDC, BlackB.Handle);
  SelectObject(HDC, BlackP.Handle);
  Rectangle(HDC, R.Left, R.top, R.Right, R.Bottom);

  R.Top := Round(Height - (Height * (aValue / MaxValue)));
  R.Bottom := R.Top + 8;
  SelectObject(HDC, B.Handle);
  SelectObject(HDC, P.Handle);
  Rectangle(HDC, R.Left, R.top, R.Right, R.Bottom);
end;


procedure TIntegratedPanelMeter.PaintLed(aLed : Integer);
var
    R : TRect;
    lt, TH : Integer;
begin
  R.Left := Width div 3 + Gap;
  R.Right := Width - Gap;

  if aLed < LowLeds then
    lt := 0
  else if aLed >= (Leds - HighLeds) then
    lt := 2
  else
    lt := 1;

  TH := Height - 16;

  SelectObject(HDC, Pens[lt, LedIsOn(Value, aLed)].Handle);
  SelectObject(HDC, Brushs[lt, LedIsOn(Value, aLed)].Handle);
  if Vertical then begin
    R.Bottom := TH - (TH * aLed div Leds);
    R.Top := TH - (TH * (aLed+1) div Leds - Gap);
    Rectangle(HDC, R.Left, R.Top, R.Right, R.Bottom );
  end else begin
    R.Bottom := TH - (TH * aLed div Leds);
    R.Top := TH - (TH * (aLed+1) div Leds - Gap);
    Rectangle(HDC, R.Left, R.Top, R.Right, R.Bottom );
  end;
end;

procedure TIntegratedPanelMeter.SetIntegerInput(const Name : string; aValue : Double);
var I : Integer;
    LastValue : Integer;
    LastMin, LastMax : Integer;
    Capture : TDataCapture;
begin
  if Assigned(EDMHead) then begin
    case DataType of
      pmdtVoltage : Capture := EDMHead.VCapture;
      pmdtCurrent : Capture := EDMHead.ICapture;
    else
      Capture := EDMHead.VCapture;
    end;

    LastValue := Round(FValue);
    LastMin := Capture.Min;
    LastMax := Capture.Max;

    Capture.UpdateMinMax;

    Min := Capture.Min;
    Max := Capture.Max;
    FValue := Capture.Mean;

    if LastValue <> FValue then begin
      for I := 0 to Leds - 1 do
        if LedIsOn(LastValue, I) <> LedIsOn(FValue, I) then
          PaintLed(I);
    end;

    if Min <> LastMin then
      PaintCaptureBar(Min, LastMin, Brushs[0, True], BGPen);

    if Max <> LastMax then
      PaintCaptureBar(Max, LastMax, Brushs[2, True], BGPen);

    if ExtendedCapture then begin
  {    UpperAv := 0;
      LowerAv := 0;
      UpperC := 0;
      LowerC := 0;
      for I := 0 to Integration do begin
        if Last[I] > Value then begin
          UpperAv := UpperAv + Last[I];
          Inc(UpperC);

        end else if Last[I] < Value then begin
          LowerAv := LowerAv + Last[I];
          Inc(LowerC);
        end;
      end;
      if LowerC > 1 then begin
        LowerAV := LowerAV / LowerC;
        PaintCaptureBar(LowerAv, LastLowerAv, Brushs[1, True], BGPen);
        LastLowerAv := LowerAv;
      end;
      if UpperC > 1 then begin
        UpperAv := UpperAv / UpperC;
        PaintCaptureBar(UpperAv, LastUpperAv, Brushs[1, True], BGPen);
        LastUpperAv := UpperAv;
      end; }
    end;
  end;
end;

procedure TIntegratedPanelMeter.Paint;
var I : Integer;
    R : TRect;
    TE : TSize;
    T : string;
begin
  SelectObject(HDC, BlackB.Handle);
  SelectObject(HDC, BGPen.Handle);
  Rectangle(HDC, 0, 0, Width div 3, Height);

  SelectObject(HDC, BGBrush.Handle);
  SelectObject(HDC, BGPen.Handle);

  Rectangle(HDC, Width div 3, 0, Width, Height);
  for I := 0 to Leds - 1 do
    PaintLed(I);

  PaintCaptureBar(Min, 0, Brushs[0, True], BGPen);
  PaintCaptureBar(Max, 0, Brushs[2, True], BGPen);

  R := Rect(0, Height - 16, Width, Height);
  SelectObject(HDC, Font.Handle);
  SetTextColor(HDC, clBlack);
  SelectObject(HDC, BlackB.Handle);

//  FillRect(HDC, R, BlackB.Handle);
  if DataType = pmdtVoltage then
    T := 'V'
  else
    T := 'I';

  GetTextExtentPoint(HDC, PChar(T), Length(T), TE);
  ExtTextOut(HDC, (R.Right - TE.cx) div 2, R.Top, ETO_CLIPPED or ETO_OPAQUE, @R, PChar(T), Length(T), nil)
end;

function TIntegratedPanelMeter.SetProperty(aToken, aValue : PChar) : Boolean;
var Tmp : string;
begin
  Result := inherited SetProperty(aToken, aValue);
  Tmp := LowerCase(aToken);
  if not Result then begin
    Result := True;
    if Tmp = 'extendedcapture' then
      FExtendedCapture := BooleanValue(aValue)
    else
      Result := False;
  end;
end;

class function TIntegratedPanelMeter.GetProperties : PChar;
begin
  Result := PChar(inherited GetProperties + IntegratedProperties);
end;

initialization
  TIntegratedPanelMeter.RegisterPanelMeterClass(TIntegratedPanelMeter, 'IntegratedMeter');
  TSimplePanelMeter.RegisterPanelMeterClass(TSimplePanelMeter, 'SimpleMeter');
end.

