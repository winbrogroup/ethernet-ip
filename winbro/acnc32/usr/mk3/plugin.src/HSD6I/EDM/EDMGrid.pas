unit EDMGrid;

interface

uses AbstractGridPlugin, AbstractOPPPlugin, Windows, MK1EDMReaderWriter, EDMEditor,
     SysUtils, EDMPlc, Graphics, IGridPlugin;

type
  THSD6IIEDMGrid = class(TAbstractGridPlugin)
  private
    UpdateAll : Boolean;
    UpdateRow : Integer;
    Visible : Boolean;
    ThisProg : TMK1EDMProgram;
    ThisBlock : Integer;
    HighLight : array [0..16] of TPoint;

    TitleFont : TFont;
    LegendBrush : TBrush;
    TitleBrush : TBrush;
    TextBrush : TBrush;
    TextFont : TFont;
    FocusPen : TPen;

    EditPos : TPoint;
    procedure NewProgram(Sender : TObject);
    procedure ApplyValue(Sender : TObject; var aValue : Double);
    procedure DCApply(Sender : TObject; var OnT, OffT, SOnT, SOffT : Integer);
    procedure PBApply(Sender : TObject; var aBoards, aCoupling : Integer);
    procedure VIPApply(Sender : TObject; var aHV, aBusV, aPeak : Integer; var aNegative : Boolean);
    procedure CapacitanceApply(Sender : TObject; var Cap : Integer);
  public
    constructor Create(aHost : IFSDGridPluginHost); override;
    procedure Update; override;
    function CanEdit(X, Y : Integer) : Boolean; override;
    procedure Edit(X, Y : Integer; var Req : WideString); override;
    procedure DrawCell(aHDC : HDC; Rect : TRect; X, Y : Integer; State : TGPGridState); override;
    procedure ComboSelect(Index : Integer); override;
    procedure InPlaceEdit(X, Y : Integer); override;
  end;

implementation

uses PulseBoardForm, DutyCycleEditor, VIPEditor, GeneralEditor, CapacitanceEditForm;

constructor THSD6IIEDMGrid.Create(aHost : IFSDGridPluginHost);
var I : Integer;
begin
  inherited Create(aHost);

  TitleFont := TFont.Create;
  TitleFont.Style := [fsBold];
  LegendBrush := TBrush.Create;
  TitleBrush := TBrush.Create;
  TextBrush := TBrush.Create;
  TextFont := TFont.Create;

  FocusPen := TPen.Create;
  FocusPen.Width := 3;
  FocusPen.Color := $a0a000;


  Host.UpdateEv(guSelfDraw, 0, 0, nil);

  Host.UpdateEv(guCount, 16, 2, nil);
  Host.UpdateEv(guFixed, 1, 1, nil);
  Host.UpdateEv(guVisible, 0, 0, nil);

  Host.UpdateEv(guCell, 0, 0, PChar(BlockLang));
  Host.UpdateEv(guCell, 1, 0, PChar(DepthLang));
  Host.UpdateEv(guCell, 2, 0, PChar(PolarityLang));
  Host.UpdateEv(guCell, 3, 0, PChar(GapVoltsLang));
  Host.UpdateEv(guCell, 4, 0, PChar(ServoLang));
  Host.UpdateEv(guCell, 5, 0, PChar(HighVoltageLang));
  Host.UpdateEv(guCell, 6, 0, PChar(PeakLang));
  Host.UpdateEv(guCell, 7, 0, PChar(OnTimeLang));
  Host.UpdateEv(guCell, 8, 0, PChar(OffTimeLang));
  //Host.UpdateEv(guCell, 9, 0, PChar(SuppOnTimeLang));
  //Host.UpdateEv(guCell, 10, 0, PChar(SuppOffTimeLang));
  Host.UpdateEv(guCell, 9, 0, PChar(CapacitanceLang));
  Host.UpdateEv(guCell, 10, 0, PChar(ASVLang));
  Host.UpdateEv(guCell, 11, 0, PChar(PulseBoardsLang));
  Host.UpdateEv(guCell, 12, 0, PChar(CouplingLang));
  Host.UpdateEv(guCell, 13, 0, PChar(BusVoltsLang));
  Host.UpdateEv(guCell, 14, 0, PChar(AltBitsLang));
  Host.UpdateEv(guCell, 15, 0, PChar(SpindleSpeedLang));
  Visible := False;
  for I := 0 to 15 do begin
    HighLight[I].X := I;
    HighLight[I].Y := 1;
  end;
  TAbstractOPPPluginConsumer.AddCallback(HSD6IISectionName, NewProgram);
  Visible := True;
end;

procedure THSD6IIEDMGrid.Update;
  procedure RedrawBlock(I : Integer);
  begin
    with EDMHead.ActiveProgram.Blocks[I] do begin
      Host.UpdateEv(guCell, 0, I + 1, PChar(IntToStr(I)));
      Host.UpdateEv(guCell, 1, I + 1, PChar(IntToStr(Round(Depth / MetricToInch))));
      Host.UpdateEv(guCell, 2, I + 1, PChar(PolarityText[Negative]));
      Host.UpdateEv(guCell, 3, I + 1, PChar(Format('%.1f', [GapVolts])));
      Host.UpdateEv(guCell, 4, I + 1, PChar(Format('%.1f', [Servo])));
      Host.UpdateEv(guCell, 5, I + 1, PChar(Format('%d', [HighVoltage])));
      Host.UpdateEv(guCell, 6, I + 1, PChar(Format('%d', [Peak])));
      Host.UpdateEv(guCell, 7, I + 1, PChar(Format('%d', [OnTime])));
      Host.UpdateEv(guCell, 8, I + 1, PChar(Format('%d', [OffTime])));
      //Host.UpdateEv(guCell, 9, I + 1, PChar(Format('%d', [SuppOnTime])));
      //Host.UpdateEv(guCell, 10, I + 1, PChar(Format('%d', [SuppOffTime])));
      Host.UpdateEv(guCell, 9, I + 1, PChar(Format('%d', [Capacitance])));
      Host.UpdateEv(guCell, 10, I + 1, PChar(Format('%.1f', [ASV])));
      Host.UpdateEv(guCell, 11, I + 1, PChar(BitField(PulseBoards, 6)));
      Host.UpdateEv(guCell, 12, I + 1, PChar(BitField(Coupling, 6)));
      Host.UpdateEv(guCell, 13, I + 1, PChar(Format('%d', [BusVoltage])));
      Host.UpdateEv(guCell, 14, I + 1, PChar(BitField(AltBits, 4)));
      Host.UpdateEv(guCell, 15, I + 1, PChar(Format('%.1f', [SpindleSpeed])));
    end;
  end;

  procedure SetActiveBlock(aBlock : Integer);
  var I : Integer;
  begin
    for I := 0 to 15 do begin
      HighLight[I].X := I;
      HighLight[I].Y := aBlock + 1;
    end;
    Host.UpdateEv(guHighLight, 16, 0, Addr(HighLight));
  end;

  function StatusText : string;
  begin
    if ThisBlock <> -1 then
      with ThisProg[ThisBlock] do
        Result := Format('Depth %.3f; NDC %.1f ', [
           EDMHead.Depth / MetricToInch,
           (OnTime / (OnTime + OffTime)) * 100 ]);
  end;

var I : Integer;
begin
  if Assigned(EDMHead) and Assigned(EDMHead.ActiveProgram) then begin
//    if not EDMConsumer.Reading then begin
    Lock.Enter;
    try
      if EDMHead.BlockProcessing then begin
        Host.UpdateEv(guStatus, 0, 0, PChar(StatusText));
        if ThisBlock <> EDMHead.ActiveBlock then begin
          ThisBlock := EDMHead.ActiveBlock;
          SetActiveBlock(ThisBlock);
        end;
      end else begin
        if ThisBlock <> -1 then begin
          Host.UpdateEv(guStatus, 0, 0, PChar('EDM Inactive'));
          Host.UpdateEv(guHighLight, 0, 0, Addr(HighLight));
          ThisBlock := -1;
        end;
      end;

      if ThisProg <> EDMHead.ActiveProgram then begin
        UpdateAll := True;
        ThisProg := EDMHead.ActiveProgram;
      end;

      if not Visible then begin
        Host.UpdateEv(guVisible, 1, 0, nil);
        Visible := True;
      end;

      if UpdateAll then begin
  //      UpdateEv(Self, guHighLight, 19, 0, Addr(HighLight));

        Host.UpdateEv(guTitle, 0, 0, PChar(Format('Program %d', [ThisProg.ID])));
        Host.UpdateEv(guCount, 16, ThisProg.BlockCount + 1, nil);
        for I := 0 to ThisProg.BlockCount - 1 do
          RedrawBlock(I);
        UpdateAll := False;
      end;

      if UpdateRow >= 0 then begin
        RedrawBlock(UpdateRow);
        UpdateRow := -1;
      end;
    finally
      Lock.Release;
    end;
  end else begin
    if Visible then begin
      Host.UpdateEv(guVisible, 0, 0, nil);
      Host.UpdateEv(guTitle, 0, 0, PChar('No program loaded'));
      Host.UpdateEv(guTitle, 0, 0, PChar('EDM Inactive'));
      Visible := False;
    end;
  end;
end;

function THSD6IIEDMGrid.CanEdit(X, Y : Integer) : Boolean;
begin
(*  case X of
    2, 11, {13, 14,} 15 : Result := False;
  else
    Result := True;
  end; *)
  Result := True;
end;

procedure THSD6IIEDMGrid.Edit(X, Y : Integer; var Req : WideString);
begin
  if Assigned(EDMHead.ActiveProgram) then begin
    SetParameter(EDMHead.ActiveProgram, X, Y, Req);
    EDMHead.ActiveProgram.Verify(Y - 1);
    UpdateRow := Y - 1;
  end;
end;

procedure THSD6IIEDMGrid.DrawCell(aHDC : HDC; Rect : TRect; X, Y : Integer; State : TGPGridState);

  function GetTitleText : string;
  begin
    if Y = 0 then begin
      case X of
        0 : Result := BlockLang;
        1 : Result := DepthLang;
        2 : Result := PolarityLang;
        3 : Result := GapVoltsLang;
        4 : Result := ServoLang;
        5 : Result := HighVoltageLang;
        6 : Result := PeakLang;
        7 : Result := OnTimeLang;
        8 : Result := OffTimeLang;
        //9 : Result := SuppOnTimeLang;
        //10 : Result := SuppOffTimeLang;
        9 : Result := CapacitanceLang;
        10 : Result := ASVLang;
        11 : Result := PulseBoardsLang;
        12 : Result := CouplingLang;
        13 : Result := BusVoltsLang;
        14 : Result := AltBitsLang;
        15 : Result := SpindleSpeedLang;
      end;
    end else begin
      Result := IntToStr(Y - 1);
    end;
  end;

  function GetTextXY : string;
  begin
    with EDMHead.ActiveProgram.Blocks[Y - 1] do begin
      case X of
        0 : Result := IntToStr(Y - 1);
        1 : Result := IntToStr(Round(Depth / MetricToInch));
        2 : Result := PolarityText[Negative];
        3 : Result := Format('%.1f', [GapVolts]);
        4 : Result := Format('%.1f', [Servo]);
        5 : Result := Format('%d', [HighVoltage]);
        6 : Result := Format('%d', [Peak]);
        7 : Result := Format('%d', [OnTime]);
        8 : Result := Format('%d', [OffTime]);
        //9 : Result := Format('%d', [SuppOnTime]);
        //10: Result := Format('%d', [SuppOffTime]);
        9: Result := Format('%d', [Capacitance]);
        10: Result := Format('%.1f', [ASV]);
        11: Result := BitField(PulseBoards, 6);
        12: Result := BitField(Coupling, 6);
        13: Result := Format('%d', [BusVoltage]);
        14: Result := BitField(AltBits, 4);
        15: Result := Format('%.1f', [SpindleSpeed]);
      else
        Result := '';
      end;
    end;
  end;
var TE : TSize;
    Text : string;
    L, T : Integer;
    Options : Integer;
begin
  Options := ETO_CLIPPED + ETO_OPAQUE;
  if OgdFixed in State then begin
    Text := GetTitleText;
    SelectObject(aHDC, TitleBrush.Handle);
    SelectObject(aHDC, TitleFont.Handle);
    Windows.SetTextColor(aHDC, TitleFont.Color);
    GetTextExtentPoint(aHDC, PChar(Text), Length(Text), TE);

    L := Rect.Left + ((Rect.Right - Rect.Left) div 2) - (TE.cx div 2);
    T := Rect.Top + ((Rect.Bottom - Rect.Top) div 2) - (TE.cy div 2);
    Windows.ExtTextOut(aHDC, L, T, Options, @Rect, PChar(Text), Length(Text), nil);
  end else begin
    Text := GetTextXY;
    SelectObject(aHDC, TextBrush.Handle);
    SelectObject(aHDC, TextFont.Handle);
    Windows.SetTextColor(aHDC, TextFont.Color);
    Windows.ExtTextOut(aHDC, Rect.Left + 3, Rect.Top + 3, Options, @Rect, PChar(Text), Length(Text), nil);

    if OgdFocused in State then begin
      SelectObject(aHDC, FocusPen.Handle);
      with Rect do begin
        MoveToEx(aHDC, Left+2, Top+2, nil);
        LineTo(aHDC, Right-2, Top+2);
        LineTo(aHDC, Right-2, Bottom-2);
        LineTo(aHDC, Left+2, Bottom-2);
        LineTo(aHDC, Left+2, Top+2);
      end;
    end;
  end;
end;

procedure THSD6IIEDMGrid.ComboSelect(Index : Integer);
begin
end;

procedure THSD6IIEDMGrid.NewProgram(Sender : TObject);
begin
  UpdateAll := True;
end;

procedure THSD6IIEDMGrid.InPlaceEdit(X, Y : Integer);
var Tmp : Double;
    C : Integer;
begin
  EditPos.X := X;
  EditPos.Y := Y;
  if Assigned(EDMHead) and
     Assigned(EDMHead.ActiveProgram) and
     (Y > 0)then begin
    with EDMHead.ActiveProgram.Blocks[Y - 1] do begin
      case X of
        11, 12 : TPulseBoardEditor.Execute(PulseBoards, Coupling, PBApply);

        7, 8 : TDutyCycleForm.Execute(OnTime, OffTime, SuppOnTime, SuppOffTime, False, DCApply);

        //9, 10 : TDutyCycleForm.Execute(OnTime, OffTime, SuppOnTime, SuppOffTime, True, DCApply);

        2, 5, 6, 13 : TVIPForm.Execute(HighVoltage, BusVoltage, Peak, Negative, VIPApply);

        1 : begin
          Tmp := Depth / MetricToInch;
          TNumericEditorForm.Execute(Tmp, 100000, 0, 0, ApplyValue);
          Depth := Round(Tmp * MetricToInch);
        end;

        3 : TNumericEditorForm.Execute(GapVolts, 250, 0, 1, ApplyValue);
        4 : TNumericEditorForm.Execute(Servo, 65000, 0, 1, ApplyValue);

        9 : begin
          C := Capacitance;
//          TNumericEditorForm.Execute(Tmp, 22, 0, 0, ApplyValue);
          TCapacitanceEditor.Execute(C, CapacitanceApply);
          Capacitance := C;
        end;

        10 : TNumericEditorForm.Execute(ASV, 50, 0, 1, ApplyValue);

        14 : begin
          Tmp := AltBits;
          TNumericEditorForm.Execute(Tmp, 15, 0, 0, ApplyValue);
        end;

        15 : TNumericEditorForm.Execute(SpindleSpeed, 3000, 0, 1, ApplyValue);
      end;
    end;
  end;
  if Assigned(EDMHead) and Assigned(EDMHead.ActiveProgram) then begin
    EDMHead.ActiveProgram.Verify(Y - 1);
    UpdateAll := True;
  end;
end;

procedure THSD6IIEDMGrid.ApplyValue(Sender : TObject; var aValue : Double);
begin
  if Assigned(EDMHead) and Assigned(EDMHead.ActiveProgram) then begin
    with EDMHead.ActiveProgram.Blocks[EditPos.Y - 1] do begin
      case EditPos.X of
        1 : Depth := Round(aValue * MetricToInch);
        3 : GapVolts := aValue;
        4 : Servo := aValue;
        9 : Capacitance := Round(aValue);
        10 : ASV := aValue;
        14 : AltBits := Round(aValue);
        15 : SpindleSpeed := aValue;
      end;
      EDMHead.ActiveProgram.Verify(EditPos.Y - 1);
      case EditPos.X of
        1 : aValue := Depth / MetricToInch;
        3 : aValue := GapVolts;
        4 : aValue := Servo;
        9 : aValue := Capacitance;
        10 : aValue := ASV;
        14 : aValue := AltBits;
        15 : aValue := SpindleSpeed;
      end;
    end;
  end;
  UpdateAll := True;
end;

procedure THSD6IIEDMGrid.DCApply(Sender : TObject; var OnT, OffT, SOnT, SOffT : Integer);
begin
  if Assigned(EDMHead) and Assigned(EDMHead.ActiveProgram) then begin
    with EDMHead.ActiveProgram.Blocks[EditPos.Y - 1] do begin
      OnTime := OnT;
      OffTime := OffT;
      SuppOnTime := SOnT;
      SuppOffTime := SOffT;

      EDMHead.ActiveProgram.Verify(EditPos.Y - 1);

      OnT := OnTime;
      OffT := OffTime;
      SOnT := SuppOnTime;
      SOffT := SuppOffTime;
    end;
  end;
  UpdateAll := True;
end;

procedure THSD6IIEDMGrid.PBApply(Sender : TObject; var aBoards, aCoupling : Integer);
begin
  if Assigned(EDMHead) and Assigned(EDMHead.ActiveProgram) then begin
    with EDMHead.ActiveProgram.Blocks[EditPos.Y - 1] do begin
      PulseBoards := aBoards;
      Coupling := aCoupling;
      EDMHead.ActiveProgram.Verify(EditPos.Y - 1);
      aBoards := PulseBoards;
      aCoupling := Coupling;
    end;
  end;
  UpdateAll := True;
end;


procedure THSD6IIEDMGrid.VIPApply(Sender : TObject; var aHV, aBusV, aPeak : Integer; var aNegative : Boolean);
begin
  if Assigned(EDMHead) and Assigned(EDMHead.ActiveProgram) then begin
    with EDMHead.ActiveProgram.Blocks[EditPos.Y - 1] do begin
      HighVoltage := aHV;
      BusVoltage := aBusV;
      Peak := aPeak;
      Negative := aNegative;
      EDMHead.ActiveProgram.Verify(EditPos.Y - 1);
      aHV := HighVoltage;
      aBusV := BusVoltage;
      aPeak := Peak;
      aNegative := Negative;
    end;
  end;
  UpdateAll := True;
end;

procedure THSD6IIEDMGrid.CapacitanceApply(Sender : TObject; var Cap : Integer);
begin
  if Assigned(EDMHead) and Assigned(EDMHead.ActiveProgram) then begin
    with EDMHead.ActiveProgram.Blocks[EditPos.Y - 1] do begin
      Capacitance := Cap;
      EDMHead.ActiveProgram.Verify(EditPos.Y - 1);
      Cap := Capacitance;
    end;
  end;
  UpdateAll := True;
end;

initialization
  THSD6IIEDMGrid.AddPlugin('HSD6IEDMGrid', THSD6IIEDMGrid);
end.
