library HSD6I;

uses
  EDMEditor in 'EDMEditor.pas' {EDMEditorForm},
  EDMGrid in 'EDMGrid.pas',
  EDMPlc in 'EDMPlc.pas',
  PulseBoardForm in 'PulseBoardForm.pas' {PulseBoardEditor},
  DutyCycleEditor in 'DutyCycleEditor.pas' {DutyCycleForm},
  VIPEditor in 'VIPEditor.pas' {VIPForm},
  GeneralEditor in 'GeneralEditor.pas' {NumericEditorForm},
  CapacitanceEditForm in 'CapacitanceEditForm.pas' {CapacitanceEditor},
  IncControl in 'IncControl.pas',
  SimpleMeter in 'SimpleMeter.pas',
  IDetectGrid in 'IDetectGrid.pas';

{$R *.RES}


begin

end.
