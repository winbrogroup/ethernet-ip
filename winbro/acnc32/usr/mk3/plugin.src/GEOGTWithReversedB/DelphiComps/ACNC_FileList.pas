unit ACNC_FileList;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, StdCtrls
  ,FileCtrl;

type

  TResultPointer = ^TSearchRec;
  TFileList = class(TComponent)
  private
    { Private declarations }
    FFileList : TStringList;
    FDateList : TStringList;
    FSizeList : TStringList;
    FPath     : String;
    FFilter   : String;
    FFiles    : Integer;
    FCopyCount: Integer;
    FDateFull : Boolean;
    function DateString(DateNumber : Integer): String; 

  protected
    { Protected declarations }
  public
    { Public declarations }

    ResultList : TList;
    constructor Create(AOwner:TComponent); override;
    destructor Destroy; override;

    procedure UpdateList;
    procedure SortByDate(Reverse : Boolean);
    procedure SortByName(Reverse : Boolean);
    procedure SortbySize(Reverse : Boolean);
    procedure ClearFiles;
    function CopyFilesTo(DestDirectory : String):Boolean;


  published
    { Published declarations }
    property Path            : String  read FPath write FPath;
    property Filter          : String  read FFilter write FFilter;
    property NumberFiles     : Integer read FFiles;
    Property NameList        : TStringList read FFileList;
    Property DateList        : TStringList read FDateList;
    Property SizeList        : TStringList read FSizeList;
    Property CopyCount       : Integer read FCopyCount;
    Property FullDate        : Boolean read FDateFull write FDateFull; 



  end;

function SizeCompare(List: TStringList; Index1, Index2: Integer): Integer;
function SlashSep(const Path, S: String): String;

procedure Register;

implementation

procedure Register;
begin
  RegisterComponents('ACNCComps', [TFileList]);
end;

constructor TFileList.Create(AOwner:TComponent);
begin
inherited Create(AOwner);           {Create as inherited from Tcomponent}
FFileList := TStringList.Create;
FDateList := TStringList.Create;
FSizeList := TStringList.Create;
ResultList := TList.Create;
FFileList.Clear;
FFiles := 0;
FCopyCount := 0;
FDateFull := False;
end;

destructor TFileList.Destroy;
begin
  FFileList.Free;
  FDateList.Free;
  FSizeList.Free;
  ResultList.Free;
  
  inherited Destroy;
end;


procedure TFileList.SortByDate(Reverse : Boolean);
var
Sortlist : TStringList;
FileCount: Integer;
Buffer   : String;
Resp     : TResultPointer;
//DosDate  : TDateTime;
StarPos  : Integer;
TimeVal  : Integer;

begin
if ResultList.Count < 1 then exit;
SortList := TStringList.Create;
try
For FileCount := 0 to ResultList.Count-1 do
   begin
   Resp := ResultList[FileCount];
   Buffer := IntToStr(Resp.Time);
   Buffer := Buffer+ '***' +  Resp.Name;
   Buffer := Buffer+ '***' + IntTostr(Resp.size);
   Sortlist.Add(Buffer);
   Sortlist.Sort;
   end;
FFileList.Clear;
FSizeList.Clear;
FDateList.Clear;

if Reverse then
  begin
  For FileCount := 0 to SortList.Count-1 do
   begin
   Buffer := SortList[FileCount];
   StarPos := Pos('***',Buffer);
   TimeVal := StrToInt(Copy(Buffer,1,StarPos-1));
   DateList.Insert(0,DateString(TimeVal));
   Buffer := Copy(Buffer,StarPos+3,Length(Buffer));
   StarPos := Pos('***',Buffer);
   NameList.Insert(0,Copy(Buffer,1,StarPos-1));
   SizeList.Insert(0,Copy(Buffer,StarPos+3,Length(Buffer)));
   end;
  end
else
  begin
  For FileCount := 0 to SortList.Count-1 do
   begin
   Buffer := SortList[FileCount];
   StarPos := Pos('***',Buffer);
   TimeVal := StrToInt(Copy(Buffer,1,StarPos-1));
   DateList.Add(DateString(TimeVal));
   Buffer := Copy(Buffer,StarPos+3,Length(Buffer));
   StarPos := Pos('***',Buffer);
   NameList.Add(Copy(Buffer,1,StarPos-1));
   SizeList.Add(Copy(Buffer,StarPos+3,Length(Buffer)));
   end;
  end;



finally
SortList.Free;
end;

end;
procedure TFileList.SortByName(Reverse : Boolean);
var
Sortlist : TStringList;
FileCount: Integer;
Buffer   : String;
Resp     : TResultPointer;
//DosDate  : TDateTime;
StarPos  : Integer;
TimeVal  : Integer;
begin
if ResultList.Count < 1 then exit;
SortList := TStringList.Create;
try
For FileCount := 0 to ResultList.Count-1 do
   begin
   Resp := ResultList[FileCount];
   Buffer := Resp.Name;
   Buffer := Buffer+ '***' +  IntToStr(Resp.Time);
   Buffer := Buffer+ '***' + IntTostr(Resp.size);
   Sortlist.Add(Buffer);
   Sortlist.Sort;
   end;
FFileList.Clear;
FSizeList.Clear;
FDateList.Clear;
if Reverse then
  begin
  For FileCount := 0 to SortList.Count-1 do
   begin
   Buffer := SortList[FileCount];
   StarPos := Pos('***',Buffer);
   NameList.Insert(0,Copy(Buffer,1,StarPos-1));
   Buffer := Copy(Buffer,StarPos+3,Length(Buffer));
   StarPos := Pos('***',Buffer);
   TimeVal := StrToInt(Copy(Buffer,1,StarPos-1));
   DateList.Insert(0,DateString(TimeVal));
   SizeList.Insert(0,Copy(Buffer,StarPos+3,Length(Buffer)));
   end;
  end
else
  begin

  For FileCount := 0 to SortList.Count-1 do
   begin
   Buffer := SortList[FileCount];
   StarPos := Pos('***',Buffer);
   NameList.Add(Copy(Buffer,1,StarPos-1));
   Buffer := Copy(Buffer,StarPos+3,Length(Buffer));
   StarPos := Pos('***',Buffer);
   TimeVal := StrToInt(Copy(Buffer,1,StarPos-1));
//   DosDate := FileDateToDateTime(TimeVal);
   DateList.Add(DateString(TimeVal));
   SizeList.Add(Copy(Buffer,StarPos+3,Length(Buffer)));
   end;
  end;
finally
SortList.Free;
end;

end;
function SizeCompare(List: TStringList; Index1, Index2: Integer): Integer;
var
Buffer1,Buffer2 : String;
StarPos : Integer;
Size1,Size2 : Integer;
begin
Result := 0;
Buffer1 := List[index1];
Buffer2 := List[index2];
StarPos := Pos('***',Buffer1);
Size1 := StrToInt(Copy(Buffer1,1,StarPos-1));
StarPos := Pos('***',Buffer2);
Size2 := StrToInt(Copy(Buffer2,1,StarPos-1));
if Size1 < Size2 then Result := -1
 else
if Size2 < Size1 then Result := 1



end;

procedure TFileList.ClearFiles;
begin
ResultList.Clear;
FFileList.Clear;
FDateList.Clear;
FSizeList.Clear;
FFiles := 0;
end;



procedure TFileList.UpdateList;
var
FullPath  : String;
SResult   : TSearchRec;
Found     : Integer;
DosDate   : TDateTime;
ResRec    : TResultPointer;
begin
if (Fpath = '') or (FFilter = '') then
   begin
   exit;
   end;
FFiles := 0;
FFileList.Clear;
FSizeList.Clear;
FDateList.Clear;
ResultList.Clear;
FullPath := SlashSep(Fpath,Filter);
Found := FindFirst(FullPath,faArchive+faReadOnly,SResult);
if Found = 0 then
   begin
   New(ResRec);
   ResRec^ := SResult;
   ResultList.Add(ResRec);
   FFileList.Add(SResult.Name);
   DosDate := FileDateToDateTime(SResult.Time);
   FDateList.Add(Copy(DateTimeToStr(DosDate),1,8));
   FSizeList.Add(IntToStr(SResult.Size));
   FFiles := 1;
   end
else
   begin
   exit
   end;
While Found = 0 do
      begin
      Found := FindNext(SResult);
      if Found = 0 then
         begin
        New(ResRec);
        ResRec^ := SResult;
        ResultList.Add(ResRec);
         FFileList.Add(SResult.Name);
         DosDate := FileDateToDateTime(SResult.Time);
         FDateList.Add(Copy(DateTimeToStr(DosDate),1,8));
         FSizeList.Add(IntToStr(SResult.Size));
         FFiles := FFiles+1
         end;
      end;

FindClose(SResult);


end;

function TFileList.CopyFilesTo(DestDirectory: String): Boolean;
var
FileNumber : Integer;
Source,Dest     : String;
PSource    : array[0..300] of Char;
PDest      : array[0..300] of Char;

begin
FCopyCount := 0;
Result := False;
if FFileList.Count < 1 then exit;
if DirectoryExists(DestDirectory) then exit;
try
ForceDirectories(DestDirectory);
for FileNumber := 0 to FFileList.Count-1 do
    begin
    Source := SlashSep(FPath,FFileList[FileNumber]);
    StrPCopy(PSource,Source);
    Dest := SlashSep(DestDirectory,FFileList[FileNumber]);
    StrPCopy(PDest,Dest);
    if CopyFile(PSource,PDest,True) then
       begin
       inc(FCopyCount);
       end
    else
        begin
        exit;
        end;
    end;


except
Result := False;
end;
if FCopyCount = FFileList.Count then Result := True;
end;

function SlashSep(const Path, S: String): String;
begin
  if AnsiLastChar(Path)^ <> '\' then
    Result := Path + '\' + S
  else
    Result := Path + S;
end;


function TFileList.DateString(DateNumber: Integer): String;
var
Buffer : String;
begin
Result := '';
try
Buffer := DateTimeToStr(FileDateToDateTime(DateNumber));
Result :=Copy(Buffer,1,10);
if FDateFull then
  begin
  Result := Result+'~';
  Result := Result+ Copy(Buffer,12,1000);
  end;
except
end;

end;

procedure TFileList.SortBySize(Reverse : Boolean);
var
Sortlist : TStringList;
FileCount: Integer;
Buffer   : String;
Resp     : TResultPointer;
//DosDate  : TDateTime;
StarPos  : Integer;
TimeVal  : Integer;

begin
if ResultList.Count < 1 then exit;
SortList := TStringList.Create;
try
For FileCount := 0 to ResultList.Count-1 do
   begin
   Resp := ResultList[FileCount];
   Buffer := IntToStr(Resp.Size);
   Buffer := Buffer+ '***' +  Resp.Name;
   Buffer := Buffer+ '***' + IntTostr(Resp.Time);
   Sortlist.Add(Buffer);
   Sortlist.CustomSort(SizeCompare);
   end;
FFileList.Clear;
FSizeList.Clear;
FDateList.Clear;

if Reverse then
  begin
  For FileCount := 0 to SortList.Count-1 do
   begin
   Buffer := SortList[FileCount];
   StarPos := Pos('***',Buffer);
   SizeList.Insert(0,Copy(Buffer,1,StarPos-1));
   Buffer := Copy(Buffer,StarPos+3,Length(Buffer));
   StarPos := Pos('***',Buffer);
   NameList.Insert(0,Copy(Buffer,1,StarPos-1));
   Buffer := Copy(Buffer,StarPos+3,Length(Buffer));
   TimeVal := StrToInt(Copy(Buffer,1,StarPos-1));
   DateList.Insert(0,DateString(TimeVal));
   end;
  end
else
  begin
  For FileCount := 0 to SortList.Count-1 do
   begin
   Buffer := SortList[FileCount];
   StarPos := Pos('***',Buffer);
   SizeList.Add(Copy(Buffer,1,StarPos-1));
   Buffer := Copy(Buffer,StarPos+3,Length(Buffer));
   StarPos := Pos('***',Buffer);
   NameList.Add(Copy(Buffer,1,StarPos-1));
   Buffer := Copy(Buffer,StarPos+3,Length(Buffer));
   TimeVal := StrToInt(Copy(Buffer,1,StarPos-1));
   DateList.Add(DateString(TimeVal));
   end;
  end;



finally
SortList.Free;
end;

end;

end.
