{****************************************************}
{            KeyState component for Delphi 16/32     }
{ Version:   2.0                                     }
{ Authors:   Aleksey Xacker (Ukraine)                }
{ E-mail:    xacker@phreaker.net                     }
{ Home page: http://www.angen.net/~xacker/           }
{ Created:   March 16, 1999                          }
{ Modified:  April 25, 1999                          }
{****************************************************}
{                 IMPORTANT NOTE:                    }
{  This code may be used and modified by anyone so   }
{ long as this header and copyright information      }
{ remains intact. By using this code you agree to    }
{ indemnify Aleksey Xacker from any liability that   }
{ might arise from its use. You must obtain written  }
{ consent before selling or redistributing this code }
{****************************************************}
{  Properties:                                       }
{    NumLock    - State of NumLock key               }
{    ScrollLock - State of ScrollLock key            }
{    CapsLock   - State of CapsLock key              }
{    Insert     - State of Insert key                }
{  Methods:                                          }
{    SaveState - Save full key state                 }
{    RestoreState - Restore full key state           }
{****************************************************}
{  Updated version by Pontus Bredin:                 }
{  e-mail: pontus@acacia.se                          }
{                                                    }
{  1999-04-25                                        }
{    - Component now sets the states SystemWide      }
{      under NT.                                     }
{    - The properties now checks the actual state.   }
{    - Removed the published properties, since they  }
{      where not very useful.                        }
{    - Added EventHandlers.                          }
{    - Added Init method. Used to call all events.   }
{                                                    }
{  This version has not been tested in Delphi 1 !!!  }
{                                                    }
{****************************************************}

unit KeyState;

interface

uses {$IFDEF WIN32} Windows, {$ELSE} WinTypes, WinProcs, {$ENDIF}
     Classes, Forms, Messages;

type
  TKeyState = class(TComponent)
  private
    KeyState      : TKeyboardState;
    SavedKeyState : TKeyboardState;

    FOnOldAppMessage : TMessageEvent;
    FOnOldActive  : TNotifyEvent;
    FOnNumLock    : TNotifyEvent;
    FOnScrollLock : TNotifyEvent;
    FOnCapsLock   : TNotifyEvent;
    FOnInsert     : TNotifyEvent;

    procedure SetState(Ctrl: Word; stOn: Boolean);
    procedure SetNumLock(stOn: Boolean);
    function  GetNumLock : boolean;
    procedure SetScrollLock(stOn: Boolean);
    function  GetScrollLock : boolean;
    procedure SetCapsLock(stOn: Boolean);
    function  GetCapsLock : boolean;
    procedure SetInsert(stOn: Boolean);
    function  GetInsert : boolean;
    procedure AppMessage(var Msg: TMsg; var Handled: Boolean);
    procedure AppActive(Sender : TObject);
    function IsNT : boolean;
    function State(Ctrl : Word) : boolean;
  public
    constructor Create(AOwner : TComponent); override;
    destructor Destroy; override;
    procedure Init;
    procedure SaveState;
    procedure RestoreState;
    property NumLock    : Boolean read GetNumLock write SetNumLock;
    property ScrollLock : Boolean read GetScrollLock write SetScrollLock;
    property CapsLock   : Boolean read GetCapsLock write SetCapsLock;
    property Insert     : Boolean read GetInsert write SetInsert;
  published
    property OnNumLock  : TNotifyEvent read FOnNumLock write FOnNumLock;
    property OnScrollLock  : TNotifyEvent read FOnScrollLock write FOnScrollLock;
    property OnCapsLock  : TNotifyEvent read FOnCapsLock write FOnCapsLock;
    property OnInsert  : TNotifyEvent read FOnInsert write FOnInsert;
  end;

procedure Register;

implementation

constructor TKeyState.Create(AOwner : TComponent);
begin
  inherited;
  FillChar(KeyState, SizeOf(TKeyboardState), 0); {Initialize}
  if not (csDesigning in ComponentState) then
  begin
    { Save the old MessageHandler }
    FOnOldAppMessage:=Application.OnMessage;
    FOnOldActive:=Application.OnActivate;
    { Set the new MessageHandler }
    Application.OnMessage:=AppMessage;
    Application.OnActivate:=AppActive;
  end;
end;

destructor TKeyState.Destroy;
begin
  if not (csDesigning in ComponentState) then
  begin
    { Restore the old MessageHandler }
    Application.OnMessage:=FOnOldAppMessage;
    Application.OnActivate:=FOnOldActive;
  end;
  inherited;
end;

function TKeyState.IsNT : boolean;
begin
  Result:=(GetVersion<$80000000);
end;

function TKeyState.State(Ctrl : Word) : boolean;
begin
  Result:=((KeyState[ctrl] and 1)=1);
end;

procedure TKeyState.SetState(Ctrl : word; stOn : boolean);
begin
  GetKeyboardState(KeyState);
  { Toggle KeyState if changed }
  if (State(ctrl) or stOn) then
  begin
    { Toggle KeyState SystemWide }
    keybd_event(Ctrl, 0, 0, 0);
    keybd_event(Ctrl, 0, KEYEVENTF_KEYUP, 0);
  end;
  { if not Windows NT this has to be done. }
  if not IsNT then
  begin
    Application.ProcessMessages; { Has to be here. Otherwise Win95 lose control. }
    { Set KeyState }
    KeyState[Ctrl]:=Byte(stOn);
    SetKeyboardState(KeyState);
  end;
end;

procedure TKeyState.SetNumLock(stOn: Boolean);
begin
  SetState(vk_NumLock, stOn);
end;

function TKeyState.GetNumLock : boolean;
begin
  GetKeyboardState(KeyState);
  Result:=State(vk_NumLock);
end;

procedure TKeyState.SetScrollLock(stOn: Boolean);
begin
  SetState(vk_Scroll, stOn);
end;

function TKeyState.GetScrollLock : boolean;
begin
  GetKeyboardState(KeyState);
  Result:=State(vk_Scroll);
end;

procedure TKeyState.SetCapsLock(stOn: Boolean);
begin
  SetState(vk_Capital, stOn);
end;

function TKeyState.GetCapsLock : boolean;
begin
  GetKeyboardState(KeyState);
  Result:=State(vk_Capital);
end;

procedure TKeyState.SetInsert(stOn: Boolean);
begin
  SetState(vk_Insert,stOn);
end;

function TKeyState.GetInsert : boolean;
begin
  GetKeyboardState(KeyState);
  Result:=State(vk_Insert);
end;

procedure TKeyState.SaveState;
begin
  GetKeyboardState(SavedKeyState);
end;

procedure TKeyState.RestoreState;
begin
  SetKeyboardState(SavedKeyState);
end;

procedure TKeyState.AppMessage(var Msg: TMsg; var Handled: Boolean);
begin
  if Msg.message = WM_KEYDOWN then
  begin
    if not (csDesigning in ComponentState) then
    begin
      { Call the EventHandlers }
      case Msg.wParam of
        vk_NumLock : if Assigned(OnNumLock) then OnNumLock(self);
        vk_Scroll  : if Assigned(OnScrollLock) then OnScrollLock(self);
        vk_Capital : if Assigned(OnCapsLock) then OnCapsLock(self);
        vk_Insert  : if Assigned(OnInsert) then OnInsert(self);
      end;
      { Call the old MessageHandler }
      if Assigned(FOnOldAppMessage) then FOnOldAppMessage(Msg, Handled)
    end;
  end;
end;

procedure TKeyState.AppActive(Sender : TObject);
begin
  Init;
end;

procedure TKeyState.Init;
begin
  if not (csDesigning in ComponentState) then
  begin
    { Call all the EventHandlers }
    if Assigned(OnNumLock) then OnNumLock(self);
    if Assigned(OnScrollLock) then OnScrollLock(self);
    if Assigned(OnCapsLock) then OnCapsLock(self);
    if Assigned(OnInsert) then OnInsert(self);
  end;
end;

procedure Register;
begin
  RegisterComponents('Touch', [TKeyState]);
end;

end.
