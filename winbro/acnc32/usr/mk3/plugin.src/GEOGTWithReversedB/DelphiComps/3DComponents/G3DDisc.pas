unit G3DDisc;

interface


uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, GMath3D,G3DDefs,G3DPaper, StdCtrls,G3DObjects;

  type
  TG3DDisc = class(TG3DObject)

  private
  FRadius       : Double;
  FHeight       : Double;

  procedure SetRadius(nvalue : Double);
  procedure SetHeight(nvalue : Double);

  protected
  procedure CreateRealPoints;override;
  public
  procedure CreateDatumPoints(NoRotate : Boolean); override;
  constructor create(AOwner : Tcomponent); override;
  procedure DrawOn(Surface : TG3DPaper;DisplayMode : TDisplayModes); override;
  procedure MakeDescription(var Description: TStringList;Mode : TDescriptionMode);override;

  published
  property Radius     : Double read FRadius write SetRadius;
  property Height      : Double read FHeight  write SetHeight;
  end;

procedure Register;

implementation
constructor TG3DDisc.Create(AOwner : TComponent);
begin
inherited;
FObjectType :=  t3oDisc;
end;

procedure TG3DDisc.DrawOn(Surface : TG3DPaper;DisplayMode : TDisplayModes);
var
Count :Integer;
begin
inherited;
if not assigned(Surface) then exit;
if FVirtualOnly then exit;
if DisplayMode <> dmFullDisplay then exit;
CalculateScreenPoints(Surface);
with Surface.Offscreen.Canvas do
        begin
        MoveTo(ScreenPoint[0].x,ScreenPoint[0].y);
        For Count := 1 to 17 do
            begin
            LineTo(ScreenPoint[Count].x,ScreenPoint[Count].y);
            end;

        LineTo(ScreenPoint[0].x,ScreenPoint[0].y);

       For Count := 18 to NumberVerteces-1 do
            begin
            LineTo(ScreenPoint[Count].x,ScreenPoint[Count].y);
            end;
        LineTo(ScreenPoint[18].x,ScreenPoint[18].y);

        for Count := 1 to 17 do
            begin
            MoveTo(ScreenPoint[Count].x,ScreenPoint[Count].y);
            LineTo(ScreenPoint[Count+18].x,ScreenPoint[Count+18].y);
            end;
        end;
end;

procedure TG3DDisc.SetRadius(nValue : Double);
begin
FRadius  := nValue;
end;


procedure TG3DDisc.SetHeight(nValue : Double);
begin
FHeight  := nValue;
end;



procedure TG3DDisc.CreateDatumPoints(NoRotate : Boolean);
var
Count : Integer;
TemPverts : TDiscVerts;
begin
if not NoRotate then inherited;
// First Calculate Points aligned to x,y,z
Tempverts := PGetDiscVerteces(FReferencePoint,FRadius,FHeight);
For Count := 0 to NumberVerteces-1 do
    begin
    Verteces[Count] := TempVerts[Count];
    end;

// Now rotate all points relative to DatumRotation

if Not NoRotate then
   begin
   if not (RotationIsZero(FPositionData.AbsoluteRotation)) then
      begin
      For Count := 0 to NumberVerteces-1 do
          begin
          RotatePointAboutReference(Verteces[Count],FPOsitionData);
          end;
      end;
   end
else
   begin
    FpositionData.AbsoluteRotation := SetPRotation(0,0,0);
   end;
end;





procedure TG3DDisc.MakeDescription(var Description: TStringList;Mode : TDescriptionMode);
begin
inherited   MakeDescription(Description,Mode);
Description.Add(Format('Radius=%f',[FRadius]));
Description.Add(Format('Height=%f',[FHeight]));
end;

procedure TG3DDisc.CreateRealPoints;
begin
FRadius := 100.0;
NumberVerteces := 36;
SetLength(Verteces,36);
SetLength(ScreenPoint,36);
CreateDatumPoints(False);
SetCORtoDatum;
end;

procedure Register;
begin
  RegisterComponents('G3D', [TG3DDisc]);
end;


end.
