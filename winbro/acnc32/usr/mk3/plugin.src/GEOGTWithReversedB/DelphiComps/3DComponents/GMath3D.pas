unit GMath3d;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls,Math,G3DDefs,G3Math2d,G3DPaper;
const
nearzero       =0.0000001;

function ZCos(Angle : Double):Double;
function ZSin(Angle : Double):Double;
function LineLength3D(Point1,Point2 : TIntLocation): Integer;
function PPreciseLineLength2D(Point1,Point2 : TPrecisePoint): Double;


// 3D Functions
function PreciseLineLength3D(Point1,Point2 : TIntLocation): Double;
function PPreciseLineLength3D(Point1,Point2 : TPLocation): Double;
function PPreciseMidPoint3D(Point1,Point2:TPLocation): TPLocation;

function PRotateAboutY3D(Point,Origin :TPLocation;Angle : Double) :TPLocation;
function PRotateAboutX3D(Point,Origin :TPLocation;Angle : Double) :TPLocation;
function PRotateAboutZ3D(Point,Origin :TPLocation;Angle : Double) :TPLocation;

function SetPrecisePoint3D(x,y,z : Double):TPLocation;
function SetScreenLocation(x,y: Integer):TScreenLocation;

function SetPRotation(x,y,z : Double):TRotation;
function AddLocation(Loc1,Loc2 : TPlocation):TPlocation;
function AddScreenLocation(Loc1,Loc2 : TScreenLocation):TScreenLocation;

function AddRotation(Rot1,Rot2 : TRotation):TRotation;
function SubtractLocation(Loc1,Loc2 : TPlocation):TPlocation;
function SubtractRotation(Rot1,Rot2 : TRotation):TRotation;
function NegateRotation(Rot : TRotation):TRotation;
function LocationsEqual(Loc1,Loc2: TPlocation):Boolean;

function PGetDiscVerteces(Centre:TPLocation;Radius :Double;Height : Double):TDiscVerts;
function PCirclePoint(Centre:TPLocation;Radius : Double;Angle: Double):TPLocation;
function Anglesbetween3DPoints(Point1,Point2:TPLocation):TRotation;
function GetCirclePoints(Centre:TIntLocation;Radius : Integer):TCircle12;
function XYAngleBetweenPoints3D(Point1,Point2 : TIntLocation): Double;
function CirclePoint(Centre:TIntLocation;Radius : Integer;Angle: Double):TIntLocation;
procedure IntersectPoint(Angle1,Angle2,Offset1,Offset2: Double;y : Integer; var x,z:Integer);

function PointToScreen(Point:TPlocation;Surface:TG3DPaper):TScreenLocation;

function PXZVectorAngle(Point,Origin :TPLocation;var Hypot: Double):Double;
function InverseCompoundRotation(ProjectedAngleAxis2,AngleAxis1 : Double):Double;
function XYCompoundAngle(XAngle,RequiredYAngle: Single): Single;
function XZCompoundAngle(XAngle, ZAngle: Single): Single;
function OrtogonalXdisplacement(AxisAngle,NormalRotation:Double;Displacement : TPLocation):Double;
function OrtogonalYdisplacement(AxisAngle,NormalRotation:Double;Displacement : TPLocation):Double;
function OrtogonalZdisplacement(AxisAngle,NormalRotation:Double;Displacement : TPLocation):Double;


implementation


function SetPrecisePoint3D(x,y,z : Double):TPLocation;
begin
Result.x :=x;
Result.y :=y;
Result.z :=z;
end;

function SetScreenLocation(x,y: Integer):TScreenLocation;
begin
Result.x :=x;
Result.y :=y;
end;

function SetPRotation(x,y,z : Double):TRotation;
begin
Result.x :=x;
Result.y :=y;
Result.z :=z;
end;

function AddLocation(Loc1,Loc2 : TPlocation):TPlocation;
begin
Result.x := Loc1.x+Loc2.x;
Result.y := Loc1.y+Loc2.y;
Result.z := Loc1.z+Loc2.z;
end;

function AddScreenLocation(Loc1,Loc2 : TScreenLocation):TScreenLocation;
begin
Result.x := Loc1.x+Loc2.x;
Result.y := Loc1.y+Loc2.y;
end;

function AddRotation(Rot1,Rot2 : TRotation):TRotation;
begin
Result.x := Rot1.x+Rot2.x;
Result.y := Rot1.y+Rot2.y;
Result.z := Rot1.z+Rot2.z;
end;

function SubtractLocation(Loc1,Loc2 : TPlocation):TPlocation;
begin
Result.x := Loc1.x-Loc2.x;
Result.y := Loc1.y-Loc2.y;
Result.z := Loc1.z-Loc2.z;
end;

function SubtractRotation(Rot1,Rot2 : TRotation):TRotation;
begin
Result.x := Rot1.x-Rot2.x;
Result.y := Rot1.y-Rot2.y;
Result.z := Rot1.z-Rot2.z;
end;

function NegateRotation(Rot : TRotation):TRotation;
begin
Result.x := -Rot.x;
Result.y := -Rot.y;
Result.x := -Rot.z;
end;


function LocationsEqual(Loc1,Loc2: TPlocation):Boolean;
begin
Result := True;
if Loc1.x<>Loc2.x then Result := False;
if Loc1.y<>Loc2.y then Result := False;
if Loc1.z<>Loc2.z then Result := False;
end;


function PointToScreen(Point:TPlocation;Surface:TG3DPaper):TScreenLocation;
var
PointBuffer : TPLocation;
begin
PointBuffer := Point;
// Now Scale the Point in x and z
POintBuffer.y := POintBuffer.y / Surface.Scale;
POintBuffer.x := POintBuffer.x / Surface.Scale;
POintBuffer.z := -POintBuffer.z / Surface.Scale;
// Now Offset Location on screen
Result.x := Round(POintBuffer.x)+Surface.XCentre+Surface.Translation.x;
Result.y := Round(POintBuffer.z)+Surface.ZCentre+Surface.Translation.y;
end;


// This function returns the angle of the vector from 3dOrigin to 3Dpoint
// in the XY plane
function PXYVectorAngle(Point,Origin :TPLocation;var Hypot: Double):Double;
var
Point2D : TPrecisePoint;
Origin2D: TPrecisePOint;
SideRatio    : Double;
Xpos , YPOs   : Boolean;
begin
POint2D.x := Point.x;
POint2D.y := Point.y;
Origin2D.x  := Origin.x;
Origin2D.y  := Origin.y;
Xpos := (Point2D.x >= Origin2D.x);
Ypos := (Point2D.y >= Origin2D.y);
try
Hypot := PPreciseLineLength2D(Point2D,Origin2D);
if Abs(Hypot) < nearzero then
   begin
   Result := 0.0;
   exit;
   end;
SideRatio := abs((Point.x-Origin.x)/Hypot);
if SideRatio >1 then SideRatio := 1.0;
if SideRatio <-1 then SideRatio := -1.0;
if Xpos and Ypos then
   begin
   Result := arcsin(SideRatio);
   exit;
   end;
if Xpos and (not Ypos) then
   begin
   Result := (Pi/2.0)+arccos(SideRatio);
   exit;
   end;
if (not Xpos) and (not Ypos) then
   begin
   Result := Pi+arcsin(SideRatio);
   exit;
   end
else
   begin
   Result := (Pi*1.5)+arccos(SideRatio);
   exit;
   end;

except
   Result := 0.0;
   exit;
end; // try except

end;


// This function returns the angle of the vector from 3dOrigin to 3Dpoint
// in the XZ plane
function PXZVectorAngle(Point,Origin :TPLocation;var Hypot: Double):Double;
var
Point2D : TPrecisePoint;
Origin2D: TPrecisePoint;
SideRatio    : Double;
Xpos , ZPOs   : Boolean;
begin
POint2D.x := Point.x;
POint2D.y := Point.z;
Origin2D.x  := Origin.x;
Origin2D.y  := Origin.z;
Xpos := (Point2D.x >= Origin2D.x);
Zpos := (Point2D.y >= Origin2D.y);

try
Hypot := PPreciseLineLength2D(Point2D,Origin2D);
if abs(Hypot) < nearzero then
   begin
   Result := 0.0;
   exit;
   end;
SideRatio := abs((Point.x-Origin.x)/Hypot);
if SideRatio >1 then SideRatio := 1.0;
if SideRatio <-1 then SideRatio := -1.0;

if Xpos and Zpos then
   begin
   Result := arcsin(SideRatio);
   exit;
   end;
if Xpos and (not Zpos) then
   begin
   Result := (Pi/2.0)+arccos(SideRatio);
   exit;
   end;
if (not Xpos) and (not ZPos) then
   begin
   Result := Pi+arcsin(SideRatio);
   exit;
   end
else
   begin
   Result := (Pi*1.5)+arccos(SideRatio);
   exit;
   end;


except
   Result := 0.0;
   exit;
end;


end;


// This function returns the angle of the vector from 3dOrigin to 3Dpoint
// in the YZ plane
function PYZVectorAngle(Point,Origin :TPLocation;var Hypot: Double):Double;
var
Point2D : TPrecisePoint;
Origin2D: TPrecisePoint;
SideRatio      : Double;
Xpos , ZPOs   : Boolean;
begin
POint2D.x := Point.y;
POint2D.y := Point.z;
Origin2D.x  := Origin.y;
Origin2D.y  := Origin.z;
//Xpos := (Point2D.x >= Origin.x);
//Zpos := (Point2D.y >= Origin.z);
Xpos := (Point2D.x > Origin2D.x);
Zpos := (Point2D.y > Origin2D.y);
try
Hypot := PPreciseLineLength2D(Point2D,Origin2D);
if abs(Hypot) < nearzero then
   begin
   Result := 0.0;
   exit;
   end;
SideRatio := abs((Point.y-Origin.y)/Hypot);
if SideRatio >1 then SideRatio := 1.0;
if SideRatio <-1 then SideRatio := -1.0;

if Xpos and Zpos then
   begin
   Result := arcsin(SideRatio);
   exit;
   end;
if Xpos and not Zpos then
   begin
   Result := (Pi/2.0)+arccos(SideRatio);
   exit;
   end;
if (not Xpos) and (not ZPos) then
   begin
   Result := Pi+arcsin(SideRatio);
   exit;
   end
else
   begin
   Result := (Pi*1.5)+arccos(SideRatio);
   exit;
   end;
except
   Result := 0.0;
   exit;
end; // try except;

end;

{function ZVectorCos(Point,Origin :TIntLocation):Double;
var
Hypot : Double;
begin
Hypot :=  PreciseLineLength3D(Point,Origin);
Result := (Point.z - Origin.z)/Hypot;
end;}


function PRotateAboutZ3D(Point,Origin :TPLocation;Angle : Double) :TPLocation;
var
Radius : Double;
TAngle  : Double;
DeltaX,DeltaY   : Double;
begin
{If LocationsEqual(Point,Origin) or (Angle = 0.0) then
  begin
  Result :=Point;
  exit;
  end;}

Tangle := DegToRad(Angle)+ PXYVectorAngle(Point,Origin,Radius);
DeltaY := Radius*ZCos(Tangle);
DeltaX := Radius*ZSin(Tangle);
Result.x := Origin.x+Deltax;
Result.y := Origin.y+Deltay;
Result.z := Point.z;
end;


function PRotateAboutY3D(Point,Origin :TPLocation;Angle : Double) :TPLocation;
var
Radius : Double;
TAngle  : Double;
DeltaX,DeltaZ   : Double;
begin
If LocationsEqual(Point,Origin) or (Angle = 0.0) then
  begin
  Result :=Point;
  exit;
  end;
// Total angle after rotation is current angle + RequestedAngle
Tangle := DegToRad(Angle)+ PXZVectorAngle(Point,Origin,Radius);
//Delatax and DeltaZ are new distances from the origin
DeltaZ := Radius*ZCos(Tangle);
DeltaX := Radius*ZSin(Tangle);
Result.x := Origin.x+Deltax;
Result.y := Point.y;
Result.z := Origin.z+Deltaz;
end;


function PRotateAboutX3D(Point,Origin :TPLocation;Angle : Double) :TPLocation;
var
Radius : Double;
TAngle  : Double;
Deltay,DeltaZ   : Double;
begin
If LocationsEqual(Point,Origin) or (Angle = 0.0) then
  begin
  Result :=Point;
  exit;
  end;
Tangle := DegToRad(Angle)+ PYZVectorAngle(Point,Origin,Radius);
DeltaZ:= Radius*ZCos(Tangle);
Deltay := Radius*ZSin(Tangle);
//Result := Point;
Result.x := Point.x;
Result.y := Origin.y+Deltay;
Result.z := Origin.z+DeltaZ;
end;





function PPreciseLineLength2D(Point1,Point2 : TPrecisePoint): Double;
var
dx,dy : Double;
begin
dx := Point2.x-Point1.x;
dy := Point2.y-Point1.y;
Result := sqrt((dx*dx)+(dy*dy));
end;

function XYAngleBetweenPoints3D(Point1,Point2 : TIntLocation): Double;
var
dx,dy : Double;
begin
dx := Point2.x-Point1.x;
dy := Point2.y-Point1.y;
if dy  = 0.0 then
   begin
   if Point2.x < Point1.x then Result := Pi else Result := 0;
   end
else
    begin
    if dx = 0.0 then
       begin
       if Point2.y > Point1.y then Result := (Pi/2.0) else Result := (Pi*1.5);
       end
    else
        begin
        Result := arcTan(dy/dx);
        end;
    end;

if dx >0 then
   begin
   if dy > 0 then
      begin
      // first quadrant answer must be > 0 and Pi/2
{      if Result > (Pi/2) then
         begin
         // This never happens
         Result := Result+0.1;
         end
      else
          begin
          if Result < 0 then
             begin
             // This never happens
             Result := Result+0.1;
             end;
          end;}
      end
   else
       begin
       // Fourth Quadrant answer must be betwen 3pi/2 and 2Pi
       if (Result < (Pi*1.5)) and (Result <> 0) then
          begin
          Result := (Pi*2.0)+Result;
          end;
{       else
           begin
           if Result > 2*Pi then
              begin
              // This never happens
              Result := Result+0.1;
              end;
           end}
       end
   end
else
    begin
    // dx <0
    if dy > 0  then
       begin
       // Second Quad Result between pi/2 and Pi
       if Result <0 then
          begin
          Result := Pi+Result;
          end;
       end
    else
        begin
        //Third Quad Result between pi and 3*Pi/2
        if Result < {PiOver2}(Pi/2.0) then
           begin
           Result := Result + Pi;
           end;
        end
    end;
end;

function PreciseLineLength3D(Point1,Point2 : TIntLocation): Double;
var
dx,dy,dz : Integer;
begin
dx := abs(Point2.x-Point1.x);
dy := abs(Point2.y-Point1.y);
dz := abs(Point2.z-Point1.z);

Result := sqrt((dx*dx)+(dy*dy)+(dz*dz));
end;


function PPreciseLineLength3D(Point1,Point2 : TPLocation): Double;
var
dx,dy,dz : Double;
begin
dx := abs(Point2.x-Point1.x);
dy := abs(Point2.y-Point1.y);
dz := abs(Point2.z-Point1.z);

Result := sqrt((dx*dx)+(dy*dy)+(dz*dz));
end;

function PPreciseMidPoint3D(Point1,Point2:TPLocation): TPLocation;
begin
Result.x := (Point1.x+Point2.x)/2;
Result.y := (Point1.y+Point2.y)/2;
Result.z := (Point1.z+Point2.z)/2;
end;


function LineLength3D(Point1,Point2 : TIntLocation): Integer;
var
dx,dy,dz : Integer;
begin
dx := Point2.x-Point1.x;
dy := Point2.y-Point1.y;
dz := Point2.z-Point1.z;

Result := Round(sqrt((dx*dx)+(dy*dy)+(dz*dz)));
end;





function CirclePoint(Centre:TIntLocation;Radius : Integer;Angle: Double):TIntLocation;
begin
    Result.x := Centre.x+Round(Radius*ZCos(Angle));
    Result.y := Centre.y+Round(Radius*ZSin(Angle));
    Result.z := Centre.z;
end;


function GetCirclePoints(Centre:TIntLocation;Radius : Integer):TCircle12;
var
Count : Integer;
Angle : Double;
Increment : Double;
begin
Angle := 0.0;
Increment := Pi/6.0;
For Count := 0 to 11 do
    begin
    Result[Count] :=CirclePoint(Centre,Radius,Angle);
    Angle := Angle+Increment;
    end;
end;

function PGetDiscVerteces(Centre:TPLocation;Radius :Double;Height : Double):TDiscVerts;
var
PCount : Integer;
Angle : Double;
Increment : Double;
HalfHeight : Double;
FaceCentre : TPlocation;
begin
HalfHeight := Height /2;
Angle := 0.0;
Increment := Pi/9;
FaceCentre := Centre;
FaceCentre.z := FaceCentre.z+HalfHeight;
For PCount := 0 to 17 do
    begin
    Result[PCount] :=PCirclePoint(FaceCentre,Radius,Angle);
    Angle := Angle+Increment;
    end;
FaceCentre.z := FaceCentre.z-Height;
For PCount := 18 to 35 do
    begin
    Result[PCount] :=PCirclePoint(FaceCentre,Radius,Angle);
    Angle := Angle+Increment;
    end;

end;

function PCirclePoint(Centre:TPLocation;Radius : Double;Angle: Double):TPLocation;
begin
    Result.x := Radius*ZCos(Angle);
    Result.x := Result.x+Centre.x;

    Result.y := Radius*ZSin(Angle);
    Result.y := Result.y+Centre.y;
    Result.z := Centre.z;
end;




procedure IntersectPoint(Angle1,Angle2,Offset1,Offset2: Double;y : Integer; var x,z:Integer);
(*
x= (Offset1*tan(A1)-(Offset2*Tan(A2))+(y*tan(A2)/(tanA1)
z= (y-Offset2)*Tan(A2)
*)
var
TanA1,TanA2  : Double;
begin
TanA1 := Tan(RadToDeg(Angle1));
TanA2 := Tan(RadToDeg(Angle2));

x:= Round(((Offset1*TanA1)-(Offset2*TanA2)+(y*TanA2))/TanA1);
z:= Round((y-Offset2)*TanA2);
end;

function ZCos(Angle : Double):Double;
begin
Result := Cos(Angle);
if abs(Result) < nearzero then Result := 0.0;
end;

function ZSin(Angle : Double):Double;
begin
Result := Sin(Angle);
if abs(Result) < nearzero then Result := 0.0;
end;


function Anglesbetween3DPoints(Point1,Point2:TPLocation):TRotation;
var
Point12D,Point22D : TPrecisePoint;
begin
// Get a Rotation by considering Y and Z co-ordinates
Point12D.x := Point1.y;
Point12D.y := Point1.z;
Point22D.x := Point2.y;
Point22D.y := Point2.z;
Result.x := AngleBetweenPrecisePoints2DInDegrees(Point12D,Point22D);
// Get b Rotation by considering X and Z co-ordinates
Point12D.x := Point1.x;
Point12D.y := Point1.z;
Point22D.x := Point2.x;
Point22D.y := Point2.z;
Result.y := AngleBetweenPrecisePoints2DInDegrees(Point12D,Point22D);
// Get c Rotation by considering X and Y co-ordinates
Point12D.x := Point1.x;
Point12D.y := Point1.y;
Point22D.x := Point2.x;
Point22D.y := Point2.y;
Result.z := AngleBetweenPrecisePoints2DInDegrees(Point12D,Point22D);
end;


// This function calculates the actual A rotation of an axis mounted on a second
// rotary axis given the projected angle of the first angle at right angles to
// the second axis  and the angle of the first axis
// and the B axis angle
// Input and Output are in Degrees
// In the case of an HSD6 this gives the actual A axis rotation of a line
// on the facxe of the A axis table given inputs of the A axis and then B axis
// It does not work for vectors NOT lying onn the A axis table.
function InverseCompoundRotation(ProjectedAngleAxis2,AngleAxis1 : Double):Double;
var
RadA,RadB : Double;
begin
RadA := DegToRad(ProjectedAngleAxis2);
RadB := DegToRad(AngleAxis1);
Result := RadToDeg(arcTan(Sin(RadB)*Tan(RadA)));
end;

function XYCompoundAngle(XAngle,RequiredYAngle: Single): Single;
var
RadX,RadY : Double;
TanResult : Double;
begin
RadX := DegToRad(XAngle);
RadY := DegToRad(RequiredYAngle);
TanResult := Tan(RadY)*Cos(RadX);
Result := RadToDeg(ArcTan(TanResult));
end;



function XZCompoundAngle(XAngle, ZAngle: Single): Single;
begin
Result := XYCompoundAngle(Xangle,90-Zangle);
end;

function OrtogonalZdisplacement(AxisAngle,NormalRotation:Double;Displacement : TPLocation):Double;
var
RadSumAngle               : Double;
RAD                       : Double;
Denominator               : Double;
begin
RadSumAngle := DegToRad(NormalRotation-AxisAngle);
DeNominator := sin(DegToRad(NormalRotation)) ;
if Denominator <> 0.0 then
   begin
   Rad :=Displacement.x/sin(DegToRad(NormalRotation));
   end
else
    begin
    Rad :=Displacement.z;
    end;
Result :=Rad*Cos(RadSumAngle);
end;

function OrtogonalXdisplacement(AxisAngle,NormalRotation:Double;Displacement : TPLocation):Double;
var
RadSumAngle               : Double;
RAD                       : Double;
Denominator               : Double;
begin
RadSumAngle := DegToRad(NormalRotation-AxisAngle);
DeNominator := sin(DegToRad(NormalRotation)) ;
if Denominator <> 0.0 then
   begin
   Rad :=Displacement.x/Denominator;
   end
else
    begin
    Rad :=Displacement.z;
    end;
Result :=Rad*Sin(RadSumAngle);
end;


function OrtogonalYdisplacement(AxisAngle,NormalRotation:Double;Displacement : TPLocation):Double;
var
RadSumAngle               : Double;
RAD                       : Double;
Denominator               : Double;
begin
RadSumAngle := DegToRad(NormalRotation-AxisAngle);
DeNominator := cos(DegToRad(NormalRotation)) ;
if Denominator <> 0.0 then
   begin
   Rad :=Displacement.y/Denominator;
   end
else
    begin
    Rad :=Displacement.x;
    end;
Result :=Rad*Cos(RadSumAngle);
end;


end.
