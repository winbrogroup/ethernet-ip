unit TouchIndexContainer;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls,comctrls,buttons,TouchGlyphSoftkey,TouchDot,ACNC_BaseKeyBoards;

{$resource TouchBitmaps.res}

//const
//ContainedControlMinimum = 3;

type

TTabStyles = (tsEmpty,tsLegend,tsUpArrow,tsDownArrow);
TTouchIndexOption = (toUpButton, toDownButton,toLeftButton,toRightButton,
                     toPageUpButton,toPageDownButton);
TIndexOptions = set of TTouchIndexOption;

  TTouchIndexContainer = class(TCustomPanel)
  private
    { Private declarations }
    FContainer : TPanel;
    FOnIndexSelect: TNotifyEvent;
    FOnScrollUpButton :TNotifyEvent;
    FOnScrollDownButton :TNotifyEvent;
    FOnPageUpButton :TNotifyEvent;
    FOnPageDownButton :TNotifyEvent;
    FOnScrollRightButton: TNotifyEvent;
    FOnScrollLeftButton: TNotifyEvent;
    FONTopOfPage       : TNotifyEvent;
    FONBottomOfPage       : TNotifyEvent;
    FMaxButtonWidth       : Integer;
    ContainedCount        : Integer;

    FSelectedIndexTabIndex: Integer;
    FSelectedIndexTabLegend: String;
    HoldTimer :TTimer;
    StatusBar : TStatusBar;
    NavButtons : array [toUpButton..toPageDownButton] of TTouchGlyphSoftkey;

    FCPHeight: Integer;
    FUseIndex: Boolean;
    FIndexTop: Integer;
    FOptions: TIndexOptions;
    FContains: TControl;
    FHoldTime: Cardinal;
    ButtonHeld : Boolean;


    IndexBuffer          : TStringList;


    IndexPallet : TPaintBox;
    Offscreen   : TBitmap;
    Activated   : Boolean;
    ActiveButton : TTouchGlyphSoftkey;
    ActiveColour : TColor;


//    ParentsAssigned  : Boolean;
    FShowIndex       : Boolean;
    FIndexTabWidth   : Integer;
    FIndexTabHeight  : Integer;
    FirstIndexIndex  : Integer;
    DownArrowIndex   : Integer;
    UpArrowIndex     : Integer;
    VisibleTabs      : Integer;

    InhibitResize    : Boolean;
//    SelectedIndexIndex : Integer;

    ChildList : TList;
    CapturingChildren : Boolean;
    FShowKeyboard: Boolean;

    procedure SetUseIndex(const Value: Boolean);
    procedure DoScrollUp(Sender : TObject;Target : TWinControl);
    procedure DoPageUp(Sender : TObject; Target :TWinControl);
    procedure DoTopofPage(Sender : TObject;Target : TWinControl);
    procedure DoBottomofPage(Sender : TObject;Target : TWinControl);
    procedure DoScrollDown(Sender : TObject;Target : TWinControl);
    procedure DoPageDown(Sender : TObject;Target : TWinControl);
    procedure SetIndexTop(const Value: Integer);
    procedure ActivatorTouched(Sender : TObject);
    procedure SetOptions(const Value: TIndexOptions);
    procedure SetHoldTime(const Value: Cardinal);



    function GetIndexStrings: TStrings;
    procedure SetIndexStrings(const Value: TStrings);
    procedure SetIndexVisibility(const Value: Boolean);

    procedure PaintIndexTabs(Sender : TObject);
    procedure SetIndexTabWidth(const Value: Integer);
    procedure SetIndexTabHeight(const Value: Integer);
    procedure DrawATabOn(Canvas : TCanvas;TabRect : TRect; Legend: String;TabStyle : TTabStyles;Index : Integer);
    procedure IndexMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure NavigateKeyMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure ShowIndexMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure PageUpMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure PageDownMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure ButtonHeldTimeout(Sender : Tobject);
//    procedure WndProc(var Message: TMessage);override;
    procedure DoScrollLeft(Sender: TObject; Target: TWinControl);
    procedure DoScrollRight(Sender: TObject; Target: TWinControl);
    procedure CaptureChildrenatCreate;
    function  ControlIsContained(Control : TWinControl) : Boolean;
    procedure SetMaxButtonWidth(const Value: Integer);
    procedure GetContainedControlsList;
    procedure ResizeButtons;
    procedure InitButtonData;
    procedure SetShowKeyboard(const Value: Boolean);



  protected
    { Protected declarations }
    BottomPanel : TPanel;
    ShowIndexButton : TTouchGlyphSoftkey;
//    ScrollUpButton,ScrollDownButton,PageUpButton,PageDownButton : TTouchGlyphSoftkey;
//    ScrollLeftButton,ScrollRightButton : TTouchGlyphSoftkey;
    IndexPanel : TPanel;
    Activator : TTouchDot;
    procedure Loaded; override;


    procedure Resize; override;

  public
    { Public declarations }
    procedure SetContainedComponent(Value: TControl);
    procedure DoOnShowControls; dynamic;
    procedure DoOnHideControls; dynamic;
    procedure Activate;
    procedure Deactivate;
    procedure BeginUpdate;
    procedure EndUpdate;
    function EnabledButtonCount: Integer;

//    procedure ReleaseContainedControls;





  published
    { Published declarations }
    constructor Create(AOwner:TComponent); override;

    destructor Destroy; override;


    property IndexTabWidth : Integer read FIndexTabWidth write SetIndexTabWidth;
    property IndexTabHeight : Integer read FIndexTabHeight write SetIndexTabHeight;
    property IndexTop : Integer read FIndexTop write SetIndexTop;
    property IndexStrings : TStrings read GetIndexStrings write SetIndexStrings;
    property IndexVisible : Boolean read FShowIndex write SetIndexVisibility;
    property SelectedIndexTabIndex : Integer read FSelectedIndexTabIndex ;
    property SelectedIndexTabLegend : String read FSelectedIndexTabLegend ;
    property ControlPanelHeight : Integer read FCPHeight write FCPHeight;
    property UseIndex : Boolean read FUseIndex write SetUseIndex;
    property HoldTime : Cardinal read FHoldTime write SetHoldTime;
    property MaxButtonWidth : Integer read FMaxButtonWidth write SetMaxButtonWidth;


    property  OnIndexSelect : TNotifyEvent read FOnIndexSelect write FOnIndexSelect;
    property  OnScrollUpButton :TNotifyEvent read FOnScrollUpButton write FOnScrollUpButton;
    property  OnScrollDownButton :TNotifyEvent read FOnScrollDownButton write FOnScrollDownButton;
    property  OnScrollLeftButton :TNotifyEvent read FOnScrollLeftButton write FOnScrollLeftButton;
    property  OnScrollRightButton :TNotifyEvent read FOnScrollRightButton write FOnScrollRightButton;
    property  OnTopofPageRequest :TNotifyEvent read FOnTopofPage write FOnTopofPage;
    property  FOnBottomofPageRequest :TNotifyEvent read FOnBottomofPage write FOnBottomofPage;

    property  OnPageUpButton :TNotifyEvent read FOnPageUpButton write FOnPageUpButton;
    property  OnPageDownButton :TNotifyEvent read FOnPageDownButton write FOnPageDownButton;
    property  Options :TIndexOptions read FOptions write SetOptions;
    property  ShowKeyboard : Boolean read FShowKeyboard write SetShowKeyboard;
//  property  ContainedControl : TWinControl read GetContainedContained write SetContains;


  end;

procedure Register;
function FocussedControl : TWinControl;

implementation

procedure Register;
begin
  RegisterComponents('Touch', [TTouchIndexContainer]);
end;

{ TTouchIndexContainer }

constructor TTouchIndexContainer.Create(AOwner: TComponent);
begin
  inherited;
//  Created := False;
  FMaxButtonWidth := 50;
  CapturingChildren := False;
  InhibitResize := False;
  Activated := False;
  ActiveButton := nil;
  FContainer := TPanel.Create(self);
  ContainedCount := 0;
 with FContainer do
       begin
       Parent := Self;
       Top := 2;
       end;
  ChildList := TList.Create;
  IndexBuffer := TStringList.Create;
  IndexPanel := TPanel.Create(Self);
  IndexPallet := TPaintBox.Create(Self);
  BottomPanel:= TPanel.Create(Self);
  OffScreen := TBitmap.Create;
  FirstIndexIndex := 0;
  DownArrowIndex := -1;
  FCPHeight := 45;
  UpArrowIndex := -1;
  FSelectedIndexTabIndex := 0;
  FIndexTabHeight := 50;
  FIndexTabWidth := 40;
  FIndexTop := 5;

  Height := 100;
  Width := 200;
  BevelOuter := bvNone;
  BevelInner := bvNone;
  HoldTimer := TTimer.Create(Self);
  HoldTimer.Enabled := False;
  HoldTimer.Interval := 250;
  HoldTimer.OnTimer := ButtonHeldTimeout;
  FHoldTime := 250;
  include (FOptions,toUpButton);
  include (FOptions,toDownButton);
  include (FOptions,toLeftButton);
  include (FOptions,toRightButton);
  include (FOptions,toPageUpButton);
  include (FOptions,toPageDownButton);


  with IndexPanel do
       begin
       Parent := Self;
       Left := 1;
       Visible := False;
       BevelInner := bvNone;
       BevelOuter := BVNone;
       end;
  Activator := TTouchDot.Create(Self);
  with activator do
       begin
       Parent := BottomPanel;
       Top := 5;
       Left := BottomPanel.Width-50;
       OnTouched := ActivatorTouched;
       end;

  with IndexPallet do
       begin
       Parent := IndexPanel;
       align := alClient;
       Visible := True;
       OnPaint := PaintIndexTabs;
       OnMouseUp := IndexMouseUp;
       end;

  with BottomPanel do
       begin
       Parent := Self;
       Align := alBottom;
       BevelInner := bvNone;
       Color := clGray;
       end;


  ShowIndexButton := TTouchGlyphSoftkey.Create(Self);
  with ShowIndexButton do
       begin
       Parent := BottomPanel;
       BevelInner := bvSpace;
       Top := 2;
       Left := 5;
       Width := 50;
       OnMouseUp := ShowIndexMouseUp;
       KeyStyle := ssElipse;
       ButtonColor := clGreen;
       Legend := 'i';
       Layout := slCentreOverlay;
       ButtonTravel := 1;
       AutoTextSize := true;
       with LegendFont do
            begin
            Style := [fsBold];
            Name := 'Comic Sans MS';
            Color := clYellow;
            end;
       Visible := False;
       end;

  NavButtons[toUpButton] := TTouchGlyphSoftkey.Create(Self);
  with NavButtons[toUpButton] do
       begin
       Parent := BottomPanel;
       BevelInner := bvSpace;
       Top := 2;
       Left := 80;
       Width := 50;
       OnMouseUp := NavigateKeyMouseUp;
       Layout := slCentreOverlay;
       ButtonTravel := 1;
       KeyStyle := ssElipse;
       AutoTextSize := true;
       ButtonColor := clInfoBk;
       with LegendFont do
            begin
            Style := [fsBold];
            Name := 'MS Comic Sans';
            Color := clwhite;
            end;
       Visible := False;
       Glyph.LoadFromResourceName(hInstance,'UPCHEVRON');
       end;

  NavButtons[toDownButton] := TTouchGlyphSoftkey.Create(Self);
  with NavButtons[toDownButton] do
       begin
       Parent := BottomPanel;
       Top := 2;
       Left := 140;
       Width := 50;
       KeyStyle := ssElipse;
       OnMouseUp := NavigateKeyMouseUp;
       Layout := slCentreOverlay;
       ButtonTravel := 1;
       AutoTextSize := true;
       ButtonColor := clInfoBk;
       with LegendFont do
            begin
            Style := [fsBold];
            Name := 'MS Comic Sans';
            Color := clwhite;
            end;
       Visible := False;
       Glyph.LoadFromResourceName(hInstance,'DOWNCHEVRON');
       end;

  NavButtons[toLeftButton] := TTouchGlyphSoftkey.Create(Self);
  with NavButtons[toLeftButton] do
       begin
       Parent := BottomPanel;
       BevelInner := bvSpace;
       Top := 2;
       Left := 200;
       Width := 50;
       Layout := slCentreOverlay;
       ButtonTravel := 1;
       KeyStyle := ssElipse;
       OnMouseUp := NavigateKeyMouseUp;
       AutoTextSize := true;
       ButtonColor := clInfoBk;
       with LegendFont do
            begin
            Style := [fsBold];
            Name := 'MS Comic Sans';
            Color := clwhite;
            end;
       Visible := False;
       Glyph.LoadFromResourceName(hInstance,'LEFTCHEVRON');
       end;

  NavButtons[toRightButton] := TTouchGlyphSoftkey.Create(Self);
  with NavButtons[toRightButton] do
       begin
       Parent := BottomPanel;
       BevelInner := bvSpace;
       Top := 2;
       Left := 260;
       Width := 50;
       Layout := slCentreOverlay;
       ButtonTravel := 1;
       OnMouseUp := NavigateKeyMouseUp;
       KeyStyle := ssElipse;
       AutoTextSize := true;
       ButtonColor := clInfoBk;
       with LegendFont do
            begin
            Style := [fsBold];
            Name := 'MS Comic Sans';
            Color := clwhite;
            end;
       Visible := False;
       Glyph.LoadFromResourceName(hInstance,'RIGHTCHEVRON');
       end;

  NavButtons[toPageUpButton] := TTouchGlyphSoftkey.Create(Self);
  with NavButtons[toPageUpButton] do
       begin
       Parent := BottomPanel;
       BevelInner := bvSpace;
       Top := 2;
       Left := 330;
       Width := 50;
       KeyStyle := ssElipse;
       OnMouseUp := NavigateKeyMouseUp;
       OnMouseDown := PageUpMouseDown;
       Layout := slCentreOverlay;
       ButtonTravel := 1;
       AutoTextSize := true;
       ButtonColor := clInfoBk;
       with LegendFont do
            begin
            Style := [fsBold];
            Name := 'MS Comic Sans';
            Color := clwhite;
            end;
       Visible := False;
       Glyph.LoadFromResourceName(hInstance,'UPUPCHEVRON');
       end;

  NavButtons[toPageDownButton] := TTouchGlyphSoftkey.Create(Self);
  with NavButtons[toPageDownButton] do
       begin
       Parent := BottomPanel;
       BevelInner := bvSpace;
       Top := 2;
       Left := 390;
       Width := 50;
       KeyStyle := ssElipse;
       OnMouseUp := NavigateKeyMouseUp;
       OnMouseDown := PageDownMouseDown;

       Layout := slCentreOverlay;
       ButtonTravel := 1;
       AutoTextSize := true;
       ButtonColor := clInfoBk;
       with LegendFont do
            begin
            Style := [fsBold];
            Name := 'MS Comic Sans';
            Color := clwhite;
            end;
       Glyph.LoadFromResourceName(hInstance,'downdownCHEVRON');
       Visible := False;
       end;

  StatusBar := TStatusBar.Create(Self);
  with StatusBar do
       begin
       Parent := BottomPanel;
       Align := alLeft;
       end;

SetUseIndex(True);
IndexTabWidth := 35;
end;

destructor TTouchIndexContainer.Destroy;
var
ChildNumber : Integer;
ChildControl : TWinControl;
begin
  inherited;
  IndexBuffer.Free;
  OffScreen.Free;
  ChildList.Free;
end;

function TTouchIndexContainer.GetIndexStrings: TStrings;
begin
if assigned(IndexBuffer) then
   begin
   Result := IndexBuffer;
   end
else
    begin
    Result := nil;
    end

end;


procedure TTouchIndexContainer.DrawATabOn(Canvas : TCanvas;TabRect : TRect;Legend: String;TabStyle : TTabStyles;Index : Integer);
var
ArcCentre : Integer;
TabHeight : Integer;
TabCentreX,TabCentreY : Integer;
StartColor : TColor;
TextTop    : Integer;
TextLeft   : Integer;
Reduction  : Integer;
ArrowHalfHeight: Integer;
ArrowHalfWidth :Integer;
AHeadDepth     : Integer;
ArrowXCentre   : Integer;
ArrowHalfStemWidth : Integer;
begin
with Canvas do
     begin
     Reduction := 1;
     Pen.Color := clWhite;
     TabHeight := TabRect.Bottom-TabRect.Top;
     ArcCentre := TabRect.Top+(TabHeight div 2);
     TabCentreY := (TabRect.Top+TabRect.Bottom) div 2;
     TabCentreX := (TabRect.Left+TabRect.Right) div 2;


     with TabRect do
          begin
          Pen.Color := clWhite;
          Arc(Right-2,Top+Reduction,Left-Right,Bottom-4,Left,ArcCentre+TabHeight,Left,ArcCentre-TabHeight);
          MoveTo(Left,Top+Reduction);
          LineTo(Left,Bottom-Reduction);
          Brush.Color := clWhite;
          StartColor := Pixels[Left+4,ArcCentre];
          FloodFill(Left+4,ArcCentre,StartColor,fsSurface);
          Pen.Color := clSilver+$00111111;
          Pen.width := 2;
          Arc(Right-2,Top+Reduction,Left-Right,Bottom-Reduction,Right,ArcCentre,Left,ArcCentre-TabHeight);
          Pen.Color := clGray+$00111111;
          Pen.width := 1;
          Arc(Right-2,Top+Reduction,Left-Right,Bottom-Reduction,Right,ArcCentre,Left,ArcCentre-TabHeight);
          end;
     case Tabstyle of
     tsLegend:
     begin
     if Index = FSelectedIndexTabIndex then Font.Color := clRed else Font.Color := clBlack;
     Font.Height := - Round(TabHeight /2.5 );
     TextTop := ArcCentre-(TabHeight div 4);
     TextLeft := TabRect.Left+6;
     TextOut(TextLeft,TextTop,Legend);
     end;

     tsUpArrow:
     begin
     UpArrowIndex := Index;
     ArrowHalfHeight := TabHeight div 4;
     ArrowHalfWidth := (TabRect.Right-TabRect.Left) div 6;
     AHeadDepth     := (ArrowHalfHeight div 2)+2 ;
     ArrowXCentre   := TabCentreX-ArrowHalfWidth;
     ArrowHalfStemWidth := 2;
     Pen.Width := 1;
     Pen.Color := clGray;
     MoveTo(ArrowXCentre,TabCentreY-ArrowHalfHeight);
     LineTo(ArrowXCentre-ArrowHalfWidth,TabCentreY-ArrowHalfHeight+AHeadDepth);
     LineTo(ArrowXCentre- ArrowHalfStemWidth,TabCentreY-ArrowHalfHeight+AHeadDepth);
     LineTo(ArrowXCentre- ArrowHalfStemWidth,TabCentreY+ArrowHalfHeight);
     LineTo(ArrowXCentre+ ArrowHalfStemWidth,TabCentreY+ArrowHalfHeight);
     Pen.Color := clSilver;
     Pen.Width := 1;
     LineTo(ArrowXCentre+ ArrowHalfStemWidth,TabCentreY-ArrowHalfHeight+AHeadDepth);
     LineTo(ArrowXCentre+ ArrowHalfWidth,TabCentreY-ArrowHalfHeight+AHeadDepth);
     LineTo(ArrowXCentre,TabCentreY-ArrowHalfHeight);
     Brush.Color := clBlue;
     FloodFill(ArrowXCentre,TabCentreY,clwhite,fsSurface)

     end;

     tsDownArrow:
     begin
     DownArrowIndex := Index;
     ArrowHalfHeight := TabHeight div 4;
     ArrowHalfWidth := (TabRect.Right-TabRect.Left) div 6;
     AHeadDepth     := (ArrowHalfHeight div 2)+2 ;
     ArrowXCentre   := TabCentreX-ArrowHalfWidth;
     ArrowHalfStemWidth := 2;
     Pen.Width := 1;
     Pen.Color := clSilver;
     MoveTo(ArrowXCentre,TabCentreY+ArrowHalfHeight);
     LineTo(ArrowXCentre-ArrowHalfWidth,TabCentreY+ArrowHalfHeight-AHeadDepth);
     LineTo(ArrowXCentre- ArrowHalfStemWidth,TabCentreY+ArrowHalfHeight-AHeadDepth);
     LineTo(ArrowXCentre- ArrowHalfStemWidth,TabCentreY-ArrowHalfHeight);
     LineTo(ArrowXCentre+ ArrowHalfStemWidth,TabCentreY-ArrowHalfHeight);
     Pen.Color := clGray;
     Pen.Width := 1;
     LineTo(ArrowXCentre+ ArrowHalfStemWidth,TabCentreY+ArrowHalfHeight-AHeadDepth);
     LineTo(ArrowXCentre+ ArrowHalfWidth,TabCentreY+ArrowHalfHeight-AHeadDepth);
     LineTo(ArrowXCentre,TabCentreY+ArrowHalfHeight);
     Brush.Color := clBlue;
     FloodFill(ArrowXCentre,TabCentreY,clwhite,fsSurface)
     end;
     end; // case

     end;
end;


procedure TTouchIndexContainer.PaintIndexTabs(Sender : TObject);
var
IndexIndex    : Integer;
PRect         : TRect;

begin
PRect.Top := 0;
PRect.Left := 0;
Prect.Right := IndexPanel.Width;
PRect.Bottom  := IndexPanel.Height;
Offscreen.Canvas.Brush.Color := clSilver;
Offscreen.Canvas.FillRect(PRect);
if not FShowIndex then exit;
VisibleTabs := IndexPallet.Height div FIndexTabHeight;
if VisibleTabs < IndexBuffer.Count then
   begin
   with Offscreen.Canvas do
   begin
   PRect.Top := 0;
   PRect.Left := 0;
   PRect.Right := IndexPallet.Width;
   PRect.Bottom :=  IndexPallet.Height;
   Pen.color := clGray;
   Pen.Width := 1;
   DownArrowIndex := -1;
   UpArrowIndex := -1;

   //DrawFirstTab
   PRect.Top := 0;
   PRect.Bottom := FIndexTabHeight;
   if FirstIndexIndex > 0 then
       begin
       DrawATabOn(Offscreen.Canvas,PRect,'',tsUpArrow,FirstIndexIndex)
       end
   else
       begin
       DrawATabOn(Offscreen.Canvas,PRect,IndexBuffer[FirstIndexIndex],tsLegend,FirstIndexIndex);
       end;

   //Draw intermediate Tabs

   begin
   For IndexIndex := FirstIndexIndex+1 to FirstIndexIndex+VisibleTabs-2 do
       begin
       PRect.Top := (IndexIndex-FirstIndexIndex)*FIndexTabHeight;
       PRect.Bottom := PRect.Top+ FIndexTabHeight;
       DrawATabOn(Offscreen.Canvas,PRect,IndexBuffer[IndexIndex],tsLegend,IndexIndex)
       end;
   end;

   //Draw Last Tabs
   IndexIndex := FirstIndexIndex+VisibleTabs-1;
   PRect.Top := (IndexIndex-FirstIndexIndex)*FIndexTabHeight;
   PRect.Bottom := PRect.Top+ FIndexTabHeight;
   If IndexIndex < IndexBuffer.Count-1 then
       begin
       DrawATabOn(Offscreen.Canvas,PRect,'',tsDownArrow,IndexIndex)
       end
   else
       begin
       DrawATabOn(Offscreen.Canvas,PRect,IndexBuffer[IndexIndex],tsLegend,IndexIndex);
       end;
   end;
   end;

   PRect.Top := 0;
   PRect.Left := 0;
   PRect.Right := IndexPallet.Width;
   PRect.Bottom :=  IndexPallet.Height;
   IndexPallet.Canvas.CopyRect(Prect,Offscreen.Canvas,Prect);
end;

procedure TTouchIndexContainer.InitButtonData;
begin
{ButtonData[toUpButton].Name := ScrollUpButton.Name;
if toUpButton in FOptions then inc(Result);
if toDownButton in FOptions then inc(Result);
if toRightButton in FOptions then inc(Result);
if toLeftButton in FOptions then inc(Result);
if toPageUpButton in FOptions then inc(Result);
if toPageDownButton in FOptions then inc(Result);}
end;


function TTouchIndexContainer.EnabledButtonCount : Integer;
begin
Result := 0;
if toUpButton in FOptions then inc(Result);
if toDownButton in FOptions then inc(Result);
if toRightButton in FOptions then inc(Result);
if toLeftButton in FOptions then inc(Result);
if toPageUpButton in FOptions then inc(Result);
if toPageDownButton in FOptions then inc(Result);
end;

procedure TTouchIndexContainer.ResizeButtons;
var
ButtonCount : Integer;
BWidth       : Integer;
AvailableWidth : Integer;
ButtonID       : TTouchIndexOption;
begin
ButtonCount := EnabledButtonCount;
if ButtonCount = 0 then exit;
AvailableWidth := BottomPanel.Width - 150;
if AvailableWidth > (ButtonCount*2) then
   begin
   BWidth := (AvailableWidth div ButtonCount)-4;
   end;
if Bwidth > FMaxButtonWidth then BWidth := FMaxButtonWidth;
ButtonCount:=0;
for ButtonID := toUpButton to toPageDownButton do
    begin
    if NavButtons[ButtonID].Visible then
       begin
       NavButtons[ButtonID].Left := 70 + (ButtonCount*(BWidth+4));
       NavButtons[ButtonID].Width := BWidth;
       inc(ButtonCount);
       end;
    end;
end;


procedure TTouchIndexContainer.Resize;
var
ButtonNumber : TTouchIndexOption;

begin
  inherited;
  if InhibitResize then exit;
  if not HandleAllocated then exit;
  if Height < 50 then
     begin

     end
  else
      begin
      Offscreen.Width := IndexPanel.Width;
      Offscreen.Height := IndexPanel.Height;
      BottomPanel.Height := FCPHeight;
      ResizeButtons;
      IndexPanel.Height := Height- BottomPanel.Height-FIndexTop-4;
      ShowIndexButton.Height := BottomPanel.Height-6;
      Activator.Left := Width-40;
      if activated then
         begin
         BottomPanel.Height := FCPHeight;
         for ButtonNumber := toUpButton to toPageDownButton do
             begin
             NavButtons[ButtonNumber].Height := FCPHeight-4;
             NavButtons[ButtonNumber].Visible := True;
             end;
         end
      else
         begin
         for ButtonNumber := toUpButton to toPageDownButton do
             begin
             NavButtons[ButtonNumber].Visible := False;
             end;
         BottomPanel.Height := 30;
         end;
      end;
PaintIndexTabs(Self);
try
if FContains.Parent = Self then
   begin
   end
except
FContains := nil;
end;

if csDesigning in ComponentState then
//if 1 = 2 then
    begin
    FContainer.Width := 1;
    FContainer.Height:= 1;
    FContainer.Left := -2;

    end
else
    begin
    if  FShowIndex then
        begin
        FContainer.Width := Self.Width-IndexPanel.Width-4;
        FContainer.Left := IndexPanel.Width+ 2;
        end
    else
        begin
        FContainer.Width := Self.Width-4;
        FContainer.Left := 2;
        end;
    FContainer.Height := Height- BottomPanel.Height-4;//-FIndexTop;
end;
if not Activated then
   begin
   StatusBar.Width := Width-60;
   end
end;


procedure TTouchIndexContainer.SetIndexStrings(const Value: TStrings);
begin
{if assigned(IndexTabs) then
   begin
   IndexBuffer.Assign(Value);
   end;
 Update;}
end;


procedure TTouchIndexContainer.SetIndexTabHeight(const Value: Integer);
begin
  FIndexTabHeight := Value;
end;

procedure TTouchIndexContainer.SetIndexTabWidth(const Value: Integer);
begin
  FIndexTabWidth := Value;
  if FIndexTabWidth > 0 then
     IndexPanel.Width := FIndexTabWidth
  else
     IndexPanel.Width := 40;

end;

procedure TTouchIndexContainer.SetIndexVisibility(const Value: Boolean);
begin
  FShowIndex := Value;
  if FShowIndex then
     begin
     FirstIndexIndex := 0;
     IndexPanel.Visible := True;
     end
  else
      begin
      IndexPanel.Visible := False;
      end;
  Resize;
end;





procedure TTouchIndexContainer.IndexMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
TabNumber : Integer;
begin
TabNumber := Y div FIndexTabHeight;
if TabNumber+FirstIndexIndex = DownArrowIndex then
   begin
   FirstIndexIndex := FirstIndexIndex+VisibleTabs;
   if FirstIndexIndex+VisibleTabs > IndexBuffer.Count then
      begin
      FirstIndexIndex := IndexBuffer.Count- VisibleTabs;
      end;
   end
else
    begin
    if TabNumber+FirstIndexIndex = UpArrowIndex then
       begin
       FirstIndexIndex := FirstIndexIndex-VisibleTabs;
       if FirstIndexIndex < 0 then
          begin
          FirstIndexIndex := 0;
          end;
       end
    else
        begin
        if FirstIndexIndex+ TabNumber < IndexBuffer.Count then
           begin
           FSelectedIndexTabIndex := FirstIndexIndex+TabNumber;
           FSelectedIndexTabLegend := IndexBuffer[FSelectedIndexTabIndex];
           end;
        if assigned(FOnIndexSelect) then FOnIndexSelect(Self);
        end;
    end;
PaintIndexTabs(Self);
end;

procedure TTouchIndexContainer.ShowIndexMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
BeginUpdate ;
IndexVisible := not (IndexVisible);
if IndexVisible then
   begin
   end
else
   begin
   end;
EndUpdate;


end;

procedure TTouchIndexContainer.SetUseIndex(const Value: Boolean);
begin
  FUseIndex := Value;
//  ShowIndexButton.Visible := FUseIndex;
end;

procedure TTouchIndexContainer.DoScrollLeft(Sender: TObject;Target : TWinControl);
begin
if assigned(Target) then
   begin
   keybd_event(VK_LEFT,0,0,0);
   keybd_event(VK_LEFT,0,KEYEVENTF_KEYUP,0);
   end;
if assigned(FOnScrollLeftButton) then FOnScrollLeftButton(Self);
end;

procedure TTouchIndexContainer.DoScrollRight(Sender: TObject;Target : TWinControl);
begin
if assigned(Target) then
   begin
   keybd_event(VK_RIGHT,0,0,0);
   keybd_event(VK_RIGHT,0,KEYEVENTF_KEYUP,0);
   end;
if assigned(FOnScrollRightButton) then FOnScrollRightButton(Self);
end;

procedure TTouchIndexContainer.DoScrollUp(Sender: TObject;Target : TWinControl);
begin
if assigned(Target) then
   begin
   keybd_event(VK_UP,0,0,0);
   keybd_event(VK_UP,0,KEYEVENTF_KEYUP,0);
   end;
if assigned(FOnScrollUpButton) then FOnScrollUpButton(Self);
end;

procedure TTouchIndexContainer.DoPageDown(Sender: TObject;Target : TWinControl);
begin
if assigned(Target) then
   begin
   keybd_event(VK_NEXT,0,0,0);
   keybd_event(VK_NEXT,0,KEYEVENTF_KEYUP,0);
   end;
if assigned(FOnPageDownButton) then FOnPageDownButton(Self);
end;

procedure TTouchIndexContainer.DoPageUp(Sender: TObject;Target : TWinControl);
begin
if assigned(Target) then
   begin
   keybd_event(VK_PRIOR,0,0,0);
   keybd_event(VK_PRIOR,0,KEYEVENTF_KEYUP,0);
   end;
if assigned(FOnPageUpButton) then FOnPageUpButton(Self);
end;

procedure TTouchIndexContainer.DoScrollDown(Sender: TObject;Target : TWinControl);
begin
if assigned(Target) then
   begin
   keybd_event(VK_DOWN,0,0,0);
   keybd_event(VK_DOWN,0,KEYEVENTF_KEYUP,0);
   end;
if assigned(FOnScrollDownButton) then FOnScrollDownButton(Self);
end;


procedure TTouchIndexContainer.DoBottomofPage(Sender: TObject;Target : TWinControl);
begin
if assigned(Target) then
   begin
   keybd_event(VK_CONTROL,0,0,0);
   keybd_event(VK_END,0,0,0);
   keybd_event(VK_END,0,KEYEVENTF_KEYUP,0);
   keybd_event(VK_CONTROL,0,KEYEVENTF_KEYUP,0);
   end;
if assigned(FONTopOfPage) then FONTopOfPage(Self);
end;

procedure TTouchIndexContainer.DoTopofPage(Sender: TObject;Target : TWinControl);
begin
if assigned(Target) then
   begin
   keybd_event(VK_CONTROL,0,0,0);
   keybd_event(VK_HOME,0,0,0);
   keybd_event(VK_HOME,0,KEYEVENTF_KEYUP,0);
   keybd_event(VK_CONTROL,0,KEYEVENTF_KEYUP,0);
   end;
if assigned(FOnBottomOfPage) then FOnBottomOfPage(Self);
end;


procedure TTouchIndexContainer.SetIndexTop(const Value: Integer);
begin
  FIndexTop   := Value;
  IndexPanel.Top := Value;
  if assigned(IndexPanel) then
     begin
     IndexPanel.Height := Height- BottomPanel.Height-FIndexTop;
     end;
  Resize;
end;




procedure TTouchIndexContainer.ActivatorTouched(Sender: TObject);
begin
if Activated then Deactivate else Activate;
end;

procedure TTouchIndexContainer.SetOptions(const Value: TIndexOptions);
begin
  if FOptions <> Value then
     begin
     FOptions := Value;
{     ButtonData[toUpButton].Enabled             := toUpButton in FOptions;
     ButtonData[toDownButton].Enabled           := toDownButton in FOptions;
     ButtonData[toRightButton].Enabled          := toRightButton in FOptions;
     ButtonData[toLeftButton].Enabled           := toLeftButton in FOptions;
     ButtonData[toPageUpButton].Enabled         := toPageUpButton in FOptions;
     ButtonData[toPageDownButton].Enabled       := toPageDownButton in FOptions;}
     Resize;
     end;
end;


procedure TTouchIndexContainer.SetContainedComponent(Value :TControl);
begin
  try
  if assigned(FContains) then
  begin
  if FContains.Parent = Self then
     begin
     FContains.Parent := nil;
     end
  end;
  except
  FContains := nil
  end;
  FContains := Value;
  if assigned(Value) then
     begin
     try
     FContains.Align := alRight;
     FContains.Parent := Self;
     except
     end;
     end;
Resize;

end;

procedure TTouchIndexContainer.SetHoldTime(const Value: Cardinal);
begin
  FHoldTime := Value;
  HoldTimer.Interval := Value;
end;

procedure TTouchIndexContainer.PageUpMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
ActiveButton := NavButtons[toPageUpButton];
ActiveColour := NavButtons[toPageUpButton].ButtonColor;
HoldTimer.Enabled := True;
ButtonHeld := False;
end;

procedure TTouchIndexContainer.PageDownMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
ActiveButton := NavButtons[toPageDownButton];
ActiveColour := NavButtons[toPageDownButton].ButtonColor;
HoldTimer.Enabled := True;
ButtonHeld := False;
end;

procedure TTouchIndexContainer.ButtonHeldTimeout(Sender: Tobject);
begin
if ActiveButton = NavButtons[toPageUpButton] then
   begin
   NavButtons[toPageUpButton].ButtonColor := clGreen;
   end;
if ActiveButton = NavButtons[toPageDownButton] then
   begin
   NavButtons[toPageDownButton].ButtonColor := clGreen;
   end;
ButtonHeld := True;
end;



procedure TTouchIndexContainer.DoOnShowControls;
begin

end;

procedure TTouchIndexContainer.DoOnHideControls;
begin

end;

procedure TTouchIndexContainer.BeginUpdate;
begin
InhibitResize := True;
end;

procedure TTouchIndexContainer.EndUpdate;
begin
InhibitResize := False;
Resize;
end;

{procedure TTouchIndexContainer.ReleaseContainedControls;
var
ChildCount : Integer;
ChildNumber : Integer;
ChildControl : TControl;
pChild       : ^TControl;
begin
if not assigned(ChildList) then exit;
if not assigned(FContainer) then exit;

ChildCount := FContainer.ControlCount;
while ChildCount > 0 do
      begin
      ChildControl := FContainer.Controls[0];
      ChildControl.Parent := Self;
      ChildCount := FContainer.ControlCount;
      end
end;}

procedure TTouchIndexContainer.GetContainedControlsList;
var
ChildCount : Integer;
ChildNumber : Integer;
ChildControl : TControl;
begin
if not assigned(ChildList) then exit;
ChildCount := ControlCount;
//ChildList.Clear;
For ChildNumber := 0 to ChildCount-1 do
    begin
    ChildControl := Controls[ChildNumber];
    if (ChildControl <> IndexPanel) and
       (ChildControl <> FContainer) and
       (ChildControl <> BottomPanel) then
       begin
       ChildList.Add(ChildControl);
       end;
    end;
ContainedCount := ChildList.Count;
end;

procedure TTouchIndexContainer.CaptureChildrenatCreate;
var
ChildNumber : Integer;
ChildControl : TWinCOntrol;
begin
if ChildList.Count = 0 then exit;
CapturingChildren := False;
try
For ChildNumber := 0 to ContainedCount-1 do
    begin
    ChildControl := ChildList[ChildNumber];
    ChildControl.Parent := FContainer;
    end;
finally
CapturingChildren := False;
end;
end;

{procedure TTouchIndexContainer.WndProc(var Message: TMessage);
begin
  inherited;
  if not (csDestroying in ComponentState)then
     begin
     case Message.Msg of
     CM_CONTROLLISTCHANGE  :
       BEGIN
        if not (csDesigning in ComponentState)  then
           begin
           if assigned(FContainer) and assigned(ChildList) and not(CapturingChildren) then
              begin
//              GetContainedControlsList;
//              FContainer.Caption := IntToStr(ChildList.Count);
              end;
           end
        else
            begin
//            ReleaseContainedControls;
            end
       END;
     end;// case
     end;
end;}

procedure TTouchIndexContainer.Loaded;
var
CharNum : Integer;
IndexChar : Char;
AlphaCount : Integer;

begin
  inherited;
  if not (csDesigning in ComponentState) then
     begin
     GetContainedControlsList;
     CaptureChildrenatCreate;
     end;
  // Initialise Index Tags
  IndexBuffer.Clear;
  CharNum := Integer(Char('A'));
  for AlphaCount := 0 to 25 do
      begin
      IndexChar := Char(CharNum+AlphaCount);
      IndexBuffer.Add(IndexChar);
      end;

  Resize;
end;

function FocussedControl : TWinControl;
begin
Result := nil;
if Application.MainForm.Active then
   begin
   Result := Application.Mainform.ActiveControl;
   end;
end;

function TTouchIndexContainer.ControlIsContained(Control : TWinControl): Boolean;
var
ParentValid : Boolean;
TestObject      : TWinControl;
begin
Result := False;
ParentValid := True;
TestObject := Control;
try
while ParentValid and (Result= False) do
      begin
      ParentValid := assigned(TestObject.Parent);
      if parentValid then
         begin
         if TestObject.Parent = Self then
            Result := True
         else
             TestObject := TestObject.Parent;
         end;
      end;
except
Result := False;
end;
end;

procedure TTouchIndexContainer.NavigateKeyMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
FControl : TWinControl;
begin
FControl := FocussedControl;
if assigned (FControl) then
   begin
   if not ControlIsContained(FControl) then
      begin
      HoldTimer.Enabled := False;
      if (Sender = NavButtons[toPageUpButton]) or (Sender = NavButtons[toPageDownButton])  then
         begin
         if ButtonHeld then
            begin
            ButtonHeld := False;
            NavButtons[toPageUpButton].ButtonColor := ActiveColour;
            NavButtons[toPageDownButton].ButtonColor := ActiveColour;
            end
         end;
      exit;
      end;
   end;

if Sender = NavButtons[toUpButton] then
   begin
   DoScrollUp(Self,FControl);
   end;

if Sender = NavButtons[toDownButton] then
   begin
   DoScrollDown(Self,FControl);
   end;

if Sender = NavButtons[toPageUpButton] then
   begin
   HoldTimer.Enabled := False;
   if ButtonHeld then
      begin
      ButtonHeld := False;
      NavButtons[toPageUpButton].ButtonColor := ActiveColour;
      DoTopofPage(self,FControl);
      end
   else
       begin
       DoPageUp(Self,FControl);
       end;
   end;
if Sender = NavButtons[toPageDownButton] then
   begin
   HoldTimer.Enabled := False;
   if ButtonHeld then
      begin
      ButtonHeld := False;
      NavButtons[toPageDownButton].ButtonColor := ActiveColour;
      DoBottomofPage(Self,FControl);
      end
   else
       begin
       DoPageDown(Self,FControl);
       end;
   end;
if Sender = NavButtons[toLeftButton] then
   begin
   DoScrollLeft(Self,FControl);
   end;
if Sender = NavButtons[toRightButton] then
   begin
   DoScrollRight(Self,FControl);
   end;
end;

procedure TTouchIndexContainer.SetMaxButtonWidth(const Value: Integer);
begin
  FMaxButtonWidth := Value;
  Resize;
end;

procedure TTouchIndexContainer.Activate;
var
ButtonNumber : TTouchIndexOption;
begin
BeginUpdate;
ShowIndexButton.Visible := True;
for ButtonNumber := toUpButton to toPageDownButton do
       begin
       NavButtons[ButtonNumber].Visible :=  ButtonNumber in FOptions;
       end;
   IndexVisible := False;
   StatusBar.Visible := False;
   DoOnShowControls;
   Activated := True;
EndUpdate;
end;

procedure TTouchIndexContainer.Deactivate;
begin
BeginUpdate;
   ShowIndexButton.Visible := False;
   DoOnHideControls;
   StatusBar.Visible := True;
   Activated := False;
EndUpdate;
end;


{Activated := not Activated;

ShowIndexButton.Visible := Activated;
for ButtonNumber := toUpButton to toPageDownButton do
       begin
       NavButtons[ButtonNumber].Visible :=  ButtonNumber in FOptions;
       end;
if not Activated then
   begin
   DoOnHideControls;
   StatusBar.Visible := True;
   end
else
   begin
   DoOnShowControls;
   StatusBar.Visible := False;
   end;}

procedure TTouchIndexContainer.SetShowKeyboard(const Value: Boolean);
begin
  FShowKeyboard := Value;

end;

end.
