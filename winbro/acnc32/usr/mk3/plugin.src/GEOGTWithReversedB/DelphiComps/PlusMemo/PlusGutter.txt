PlusGutter 4.1b released August 12, 1999
� Electro-Concept Mauricie, 1998-1999
=====================================

PLEASE NOTE: To use this component, you need to have Electro-Concept Mauricie's
             TPlusMemo component as well! You can download this component from
             Electro-Concept Mauricie's homepage. You will find the address below.



Introduction
------------
  TPlusGutter is a panel that can be placed next to a TPlusMemo component and
  be linked to it.  It then automatically shows line numbers or paragraph
  numbers at the appropriate vertical position. It also allows to bookmark
  lines using Shift+Ctrl+0..9, and to jump from any point to one of the
  bookmarks using Ctrl+0..9. Great thanks to Thorsten Dittmar, who provided
  most of the bookmarking functionality.


License agreement:
------------------
  You have the right to use and modify this source code, but you may not sell it.
  The component files in archive PlusGut.zip are freeware. You may copy those
  files AS LONG AS YOU COPY ALL OF THEM and don't charge any kind of fee for
  it. If you want to change the source code in order to improve the component's
  features, performance, etc. send us the new source code so that we can have a
  look at it. The changed source code should contain descriptions what you have
  changed, and of course your name. The only thing you MAY NOT CHANGE is the
  ORIGINAL COPYRIGHT INFORMATION.

Disclaimer
----------
  Electro-Concept Mauricie is not liable for any damages resulting from the 
  use and misuse of the component.

Programming interface
---------------------
  property PlusMemo: TPlusMemo
    - The instance of TPlusMemo that the TPlusGutter is attached to.

  property Alignment: TAlignment 
    - This property is used to specify the horizontal position
      of line numbers and bookmark indicators inside the control's
      border.
  property Bookmarks: Boolean
    - This property controls whether the bookmark feature is enabled.
      When True, Ctrl-Shift-0..9 will toggle a bookmark on the current
      line.  Ctrl-0..9 permits reaching a previously defined bookmark.
      When False, these keys have no effect.

  property LineNumbers: Boolean;
    - This property controls whether the TPlusGutter displays numbers
      (either line numbers or paragraph numbers, depending on property below)

  property ParagraphNumbers: Boolean; 
    - To specify if you want line numbers (when set to False) or
      paragraph numbers (when set to True).  Property LineNumbers must
      be set to True to display any kind of number.


  function BookmarkLine(ALine: Integer; BookmarkIndex: TBookmarkRange): Boolean;
     - This function toggles the bookmark at line ALine.  If this line is
       already under BookmarkIndex, it will be unbookmarked and the result
       returns False.  If not, ALine will be bookmarked and the result is True

  procedure ClearBookmarks;  
     - This procedure clears all bookmarks currently defined.

  procedure JumpToBookmark(BookmarkIndex: TBookmarkRange);  self explanatory.

  property BookmarksArray[BookmarkIndex: TBookmarkRange]: Longint;
     - A read only property that returns the line number for a given bookmark index.
       If said bookmark is not defined, this property returns -1. 


Creating new bookmark icons
---------------------------
  A set of bookmark icons is provided in the file PlusGutter.res. You are free
  to modify those icons, but you may as well create a entirely new set of
  icons. In that case you may have to change the source code  of the
  constructor Create to adapt to the new resource name or transparent color.

  PLEASE NOTE: The icons must be stored in one single bitmap (horizontally).
  The icons need to be 10x10 pixels (thus you get a bitmap of 100x10 pixels).
  The order is also important: 0123456789.


Contents of the archive
-----------------------
  PLUSGUTTER.DCR - component resource file
  PLUSGUTTER.PAS - component source code
  PLUSGUTTER.RES - resource file with bookmark icons
  PLUSGUTTER.TXT - this file



Installation
------------
  Copy all the files into the Delphi "LIB" folder or a new folder you've
  created. Make sure that this folder is included in the Delphi Library search
  path!!  In Delphi select "Components/Install component..." to install the
  component. If you don�t want the component to be installed to the "ECM" page,
  modify the Register procedure as needed BEFORE you install the component.



Contact us
----------
  If you have questions, suggestions or comments, feel free to send an E-Mail to

  Electro-Concept Mauricie     ecm@ecmqc.com


Updates
-------
  For latest updates, just visit the internet site under

  Electro-Concept Mauricie	   http://www.ecmqc.com/

Thank you