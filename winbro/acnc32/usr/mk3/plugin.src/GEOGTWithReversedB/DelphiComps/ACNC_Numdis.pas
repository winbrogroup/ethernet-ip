unit ACNC_Numdis;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls;

type Jerror  =  (NOERROR,
                 ERRORBEFORE,
                 ERRORAFTER,
                 ERRORFIELDLENGTH);



type
  TNumDis = class(TLabel)
  private
    { Private declarations }
    FBeforeCount : Integer;
    FAfterCount  : Integer;
    FFieldLength : Integer;
    FValue       : Double;
    FError       : jerror;
    FPosColor    : TColor;
    FNegColor    : TColor;
    FSelPosColor : TColor;
    FSelNegColor : TColor;
    FTextSize    : Integer;
    FSelected    : Boolean;

    function     GetFValue: Double;
    procedure    SetFValue(Value:Double);
    function     GetPosColor: TColor;
    procedure    SetPosColor(Color:TColor);
    function     GetSelected: Boolean;
    procedure    SetSelected (Select : Boolean);
    procedure    SetBeforeCount (Count : Integer);
    procedure    SetAfterCount (Count : Integer);
    procedure    SetFieldLength(Count : Integer);

    {procedure    SetWidth(Fwidth:Integer);override;}



  protected
    { Protected declarations }

  public
    { Public declarations }
    property Error            : Jerror   write FError;
    constructor Create(AOwner:TComponent); override;
    destructor Destroy; override;

  published
    { Published declarations }
    property BeforeCount    : Integer read FBeforeCount     write SetBeforeCount;
    property AfterCount     : Integer read FAfterCount      write SetAfterCount;
    property FieldLength    : Integer read FFieldLength     write SetFieldLength;
    property Value          : Double  read FValue        write SetFValue;
    property PosColor       : TColor  read GetPosColor      write SetPosColor  default clYellow;
    property NegColor       : TColor  read FNegColor        write FNegColor    default clYellow;
    property SelNegColor    : TColor  read FSelNegColor     write FSelNegColor default clWhite;
    property SelPosColor    : TColor  read FSelPosColor     write FSelPosColor default clWhite;
    property Selected       : Boolean read GetSelected      write SetSelected  default False;


    property Top;
    property Left;
    property Visible;

  end;
  

procedure Register;

implementation

procedure Register;
begin
  RegisterComponents('ACNCComps', [TNumDis]);
end;


constructor TNumDis.Create(AOwner:TComponent);
begin
inherited Create(AOwner);
Color := clBlack;
FBeforeCount := 3;
FAfterCount := 5;
FFieldLength := 12;
FError := NOERROR;
FPosColor := clYellow;
FSelPosColor := clWhite;
FSelNegColor := clWhite;
Font.Color := FposColor;
FNegColor := ClRed;
Font.Name  := 'Courier New';
FTextSize := 22;
Font.Size  := FTextSize;
Width      := 250;
Height     := 45;
Color      := clBlack;
FSelected  := False;
SetFValue(FValue);
end;


destructor TNumDis.Destroy;
begin
  inherited Destroy;
end;


{****************************************************
**   Manipulation performed when a new value is sent
**************************************************}
function TNumDis.GetFValue : Double;

begin
Result   := FValue;
end;

procedure TNumDis.SetFValue(Value :Double);
var
FString       : String;
TempStr       : String;
SignStr       : String;
UseLength     : Integer;

begin
FValue := Value;
{check validity of formatting data}
FError := NOERROR;
if FBeforeCount < 1 then
   FError := ERRORBEFORE;

if FAfterCount < 0 then
   FError := ERRORAFTER;

if FFieldLength < (FBeforeCount + FAfterCount +1)  then
   FError := ERRORFIELDLENGTH;


if FError = NOERROR then
   begin

     if FValue < 0.0 then
        begin
        SignStr := '-';
        {FValue := -FValue;}
        if Selected then
           begin
           Font.Color := FSelNegColor;
           end
        else
           begin
           Font.Color := FNegColor;
           end;
        end
     else
        begin
        SignStr := '+';
        if Selected then
            begin
            Font.Color := FSelPosColor;
            end
        else
            begin
            Font.Color := FPosColor;
            end;
     end;

     UseLength := FBeforeCount + FAfterCount;
     Fstring := SignStr + '%'+IntToStr(UseLength)+'.'+IntToStr(FAfterCount)+'f';
     FmtStr (TempStr,Fstring,[abs(FValue)]);
     Caption := TempStr;
   end
else
    begin
    Caption := 'ERROR';
    end;

end;

{****************************************************
**   Get and set Positive Text Colour
**************************************************}
procedure TNumDis.SetBeforeCount(Count :Integer);
begin
FBeforeCount := Count;
SetFValue(FValue);
end;

procedure TNumDis.SetAfterCount(Count :Integer);
begin
FAfterCount := Count;
SetFValue(FValue);
end;

procedure TNumDis.SetFieldLength(Count :Integer);
begin
FFieldLength := Count;
SetFValue(FValue);
end;

function TNumDis.GetPosColor : TColor;
begin
Result := FPosColor;
end;

procedure TNumDis.SetPosColor(Color :TColor);
begin
FPosColor := Color;
SetFValue(FValue);
end;

function TNumDis.GetSelected : Boolean;
begin
Result := FSelected;
end;

procedure TNumDis.SetSelected(Select :Boolean);
begin
FSelected := Select;
SetFValue(FValue);
end;

end.
