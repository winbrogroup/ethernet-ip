unit GT1;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls,G3DPaper,G3DObjects,G3DLine,G3DDefs,GMath3D,G3DBox,G3DDisc, Buttons,
  ExtCtrls, ComCtrls,Menus, G3DOTree, G3DDisplay,  CheckSelect,INamedInterface,
  Grids,GReportPoint,VectorMaths,G3dNavigator,PCMPlugin,
  NamedPlugin, IFormInterface, ACNC_TouchGlyphSoftkey, CNCTypes, AbstractFormPlugin,
  ACNC_NumEdit;

const
cLargePaperWidth = 550;
cLargePaperHeight = 560;
cLargeTreeWidth = 450; // actually edit panel width
cLargeTreeHeight = 565;
cLargeCPanelWidth = 1020;
cLargeCPanelHeight = 170;

cSmallPaperWidth = 450;
cSmallPaperHeight = 420;
cSmallTreeWidth = 150;
cSmallTreeHeight = 380;
cSmallCPanelWidth = 780;
cSmallCPanelHeight = 110;
cMaxNumInput = 1000000;
type
TExtentsModes = (emUnLatched,emLatched);
PTXYZ = ^TXYZ;
TDisplayModes = (dmNone,dmFull,dmOnMachine,dmInvisible);

EObjectNameException = class(Exception);
EObjectValueException = class(Exception);
TJogModes = (jmPlus,jmMinus);
  TGEO = class(TInstallForm)
    FOpenDia: TOpenDialog;
    FileSaveDia: TSaveDialog;
    MachinePanel: TPanel;
    MachinePages: TPageControl;
    GEOPositionSheet: TTabSheet;
    GEOPosGrid: TStringGrid;
    ShowPCMButton: TBitBtn;
    HidePCM: TBitBtn;
    MachineDisplay: TTabSheet;
    MachineDisplayPanel: TPanel;
    MachineFitButton: TTouchGlyphSoftkey;
    PointFitButton: TTouchGlyphSoftkey;
    GEOZoomIn: TTouchGlyphSoftkey;
    GEOZoomOut: TTouchGlyphSoftkey;
    GEOCloseBtn: TTouchGlyphSoftkey;
    MachinePoints: TTabSheet;
    ReportPointGrid: TStringGrid;
    GEOPointsUP: TTouchGlyphSoftkey;
    GEOPointsDown: TTouchGlyphSoftkey;
    MachineVectors: TTabSheet;
    VectorPanel: TPanel;
    Label10: TLabel;
    PositionTitle: TPanel;
    VectorDataGrid: TStringGrid;
    VectorChoosePanel: TPanel;
    VectorSelectList: TComboBox;
    VectorRotationGrid: TStringGrid;
    RotationTitle: TPanel;
    VectorLengthDisplay: TEdit;
    TabSheet4: TTabSheet;
    RunVecDiffGrid: TStringGrid;
    Panel2: TPanel;
    Paper: TG3DDisplay;
    GEOVectorUp: TTouchGlyphSoftkey;
    GeoVectorDown: TTouchGlyphSoftkey;
    DebugSheet: TTabSheet;
    DebufStartButton: TButton;
    DebugStepButton: TButton;
    DebugStageDisplay: TEdit;
    DebugMemo: TMemo;
    TV1: TG3DOTree;
    VersionLabel: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure RotateSelectClick(Sender: TObject);
    procedure TranslateSelectClick(Sender: TObject);
    procedure TV1Change(Sender: TObject; Node: TTreeNode);

    procedure EditRunPagesChange(Sender: TObject);
    procedure AxisSelectGroupClick(Sender: TObject);

    procedure VectorSelectListChange(Sender: TObject);
    procedure DebugStepButtonClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure ShowPCMButtonClick(Sender: TObject);
    procedure GEOPointsDownClick(Sender: TObject);
    procedure HidePCMClick(Sender: TObject);
    procedure ReportPointGridSelectCell(Sender: TObject; ACol,
      ARow: Integer; var CanSelect: Boolean);
    procedure GEOCloseBtnClick(Sender: TObject);
    procedure PointFitButtonMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure MachineFitButtonMouseUp(Sender: TObject;
      Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure GEOZoomInMouseUp(Sender: TObject;
      Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure GEOZoomOutMouseUp(Sender: TObject;
      Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure GEOCloseBtnMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FormActivate(Sender: TObject);
    procedure GEOPointsUPMouseUp(Sender: TObject;
      Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure GEOPointsDownMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure GEOVectorUpMouseUp(Sender: TObject;
      Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure GeoVectorDownMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure DebufStartButtonClick(Sender: TObject);

  private
    { Private declarations }
    DebugStartStage     : Integer;
    SingleStep          : Boolean;
    DebugStartPositions : TAxisPosn;
    DrawColor           : Tcolor;
    MoveMode            : TMoveModes;
    FormWidth           : Integer;
    FormHeight          : Integer;
    LastObject          : TG3DObject;
    SelectedObject      : TG3DObject;

    JogObject           : TG3DObject;
    JogAxis             : Integer;
    JogNode             : TTreeNode;
    SelectedJogAxis    : Integer;
    SelectedDisplayVector : Integer;
    MachineNavigator      : TG3DNavigator;
    SelectedPointIndex    : Integer;
    GeoReady              : Boolean;
    InchModeTag           : TTagref;
    UserLevel             : TTagref;
    GEONavOPtionTag       : TTagref;

    function  FloatDisplay(Value : Double): String;
    function  isValidAxis(AxisNumber : Integer):Boolean;
    procedure UpdateDisplayedVectorData(ListPosition: Integer);
    procedure UpdateActToNomVectorDiffDisplay;
    procedure MachineNavButtonPressed(Sender : TObject);
    procedure MoveWorld(Motion :TMoveModes;Axis :Integer;Increment : Integer);
    procedure UpdateReportGrid;
    procedure UpdateEverythingAndDisplay;
    procedure ShowDebugState(Sender : TObject);
    procedure TagChange(ATag : TTagRef); stdcall;
    procedure SetExtentsMode(Mode : TExtentsModes);


  public
    { Public declarations }
    GEONavInstalled        : Boolean;
    FDMode                 : TDisplayModes;
    InhibitExternalUpdate  : Boolean;
    PCM                    : TPCMPlugin;
    InchMode               : Boolean;
    ExtentsMode            : TExtentsModes;
    constructor Create(AOwner : TComponent); override;
    procedure FinalInstall; override;
    procedure DisplaySweep; override;
    procedure ExternalBeginUpdate;
    procedure ExternalEndUpdate;

    procedure SetDisplayMode(DMode :TDisplayModes);
    procedure LoadPCMandinitialise(afile :PChar; Sqr, Retry : Integer; Fit : Double);
    procedure OnMachineShow;
    procedure UpdateMachineReportDataAndDisplay;
    procedure GetPCMProbeVector(VectorName : Pchar; var P1 :TPlocation;var P2:TPlocation);
    procedure SetToolOffset(P1 : TPlocation);
    procedure UpdateVectorSelectList;
    procedure UpdateGeoPositionGrid;
    procedure ProbePosnToPartRelativePoint(PointName : PChar;X,Y,Z,A,B,C : Double);
    function  GetPointData(PointName : String; var P1: TPlocation): Boolean;
    function  GetAxisPosForPoint(PointName : PChar; A,B,C : Double):TPlocation;
    procedure CorrectForStackingAxis(UseCAxis : Boolean; var X,Y,Z,A,B,C : Double);
    procedure  GetMachineNDP(Crot :Double;var ResLinear,ResRotary :TXYZ;Par1,Par2 : Integer);

  end;

var
  GEO: TGEO;
  procedure TagChangeHandler(Tag : TTagRef);stdcall

implementation

{$R *.DFM}

function InchToMetric(InValue : Double):Double;
begin
Result := InValue*25.4;
end;

function MetricToInch(InValue : Double):Double;
begin
Result := InValue/25.4;
end;

function UnitModified(Posn : Double): String;
begin
Result := Format('%7.3f',[Posn]);
if not assigned(GEO) then exit;
if GEO.InchMode then
   begin
   Result := Format('%7.4f',[MetricToInch(Posn)]);
   end;
end;

procedure  TGEO.CorrectForStackingAxis(UseCAxis : Boolean; var X,Y,Z,A,B,C : Double);
var
StartPositions : TAxisPosn;
StartStage     : Integer;
NewPOsition    : TAxisPosn;
begin

StartPositions := TV1.GetAllAxisPOsitions;
InhibitExternalUpdate := True;
try
TV1.AxisMoves[0].Position := x;
TV1.AxisMoves[1].Position := y;
TV1.AxisMoves[2].Position := z;
TV1.AxisMoves[3].Position := a;
TV1.AxisMoves[4].Position := b;
TV1.AxisMoves[5].Position := c;
TV1.AxisMoves[0].Move := True;
TV1.AxisMoves[1].Move := True;
TV1.AxisMoves[2].Move := True;
TV1.AxisMoves[3].Move := True;
TV1.AxisMoves[4].Move := True;
TV1.AxisMoves[5].Move := True;
TV1.MoveAllAxes(6,TV1.AxisMoves);
UpdateEverythingAndDisplay;

StartStage := 0;

NewPosition := GEO.TV1.NewGetAxisOffsets(StartStage,UseCAxis,False);
X := NewPOsition[0];
Y := NewPOsition[1];
Z := NewPOsition[2];
A := NewPOsition[3];
B := NewPOsition[4];
C := NewPOsition[5];
Tv1.RestoreAllAxisPOsitions(StartPositions);
finally
GEO.InhibitExternalUpdate := False;
end;
end;


// This procedure returns the absolute axis positions which place the current
// Electrode datum Point co-incident with the named point;
function  TGEO.GetAxisPosForPoint(PointName : PChar; A,B,C : Double):TPlocation;
var
PIndex : Integer;
ElectrodePosition : TPlocation;
StartPosition     : TAxisPosn;
PointPosition     : TPlocation;
PointDifference   : TPlocation;
begin
PIndex := Tv1.GetReportPointIndexfor(PointName);
try
if Pindex >=0 then
   begin
   InhibitExternalUpdate:= True;
   StartPosition := TV1.GetAllAxisPOsitions;
   TV1.AxisMoves[0].Position := 0;
   TV1.AxisMoves[1].Position := 0;
   TV1.AxisMoves[2].Position := 0;
   TV1.AxisMoves[3].Position := A;
   TV1.AxisMoves[4].Position := B;
   TV1.AxisMoves[5].Position := C;
   TV1.AxisMoves[0].Move := True;
   TV1.AxisMoves[1].Move := True;
   TV1.AxisMoves[2].Move := True;
   TV1.AxisMoves[3].Move := True;
   TV1.AxisMoves[4].Move := True;
   TV1.AxisMoves[5].Move := True;
   TV1.MoveAllAxes(6,TV1.AxisMoves);

   TV1.UpdateReportData(False);
   ElectrodePosition := TV1.ELectrodePosition;
   PointPosition := TV1.GetReportPointFromList(Pindex);
   PointDifference := SubtractLocation(PointPosition,ElectrodePosition);
   Result.X := - PointDifference.x;
   Result.Y := - PointDifference.y;
   Result.Z := PointDifference.z;
   end
else
    begin
    raise EGEOError.Create(Format('Invalid Point Name Specified (%s)',[PointName]));
    end;
finally
Tv1.RestoreAllAxisPOsitions(StartPosition);
InhibitExternalUpdate := false;
end;
end;


procedure  TGEO.ProbePosnToPartRelativePoint(PointName : PChar;X,Y,Z,A,B,C : Double);
begin
InhibitExternalUpdate  :=True;
try
TV1.ProbePosnToPartRelativePoint(PointName,X,Y,Z,A,B,C);
finally
InhibitExternalUpdate  :=False;
end;

end;

// This used for PP interface Instruction GEOGetPoint
function TGEO.GetPointData(PointName : String; var P1: TPlocation): Boolean;
var
Pindex : Integer;
begin
Result := True;
TV1.UpdateReportData(False);

if PointName = 'ELECTRODEDATUM' then
   begin
   P1 := TV1.ELectrodePosition;
   exit;
   end;
if PointName = 'TOOLDATUM' then
   begin
   P1 := TV1.ToolDatumPosition;
   exit;
   end;
if PointName = 'PARTDATUM' then
   begin
   P1 := TV1.PartDatumPosition;
   exit;
   end;
PIndex := Tv1.GetReportPointIndexfor(PointName);
if Pindex >=0 then
   begin
   try
   P1 := TV1.GetReportPointFromList(PIndex);
   except
   P1 := TV1.Zero3D;
   Result := False;
   raise EGEOError.Create(Format('Invalid Point Name Specified (%s)',[PointName]));
   end;
   end
else
    begin
    P1 := TV1.Zero3D;
    Result := False;
    raise EGEOError.Create(Format('Invalid Point Name Specified (%s)',[PointName]));
    end;

end;

procedure TGEO.SetToolOffset(P1 : TPlocation);
begin
TV1.SetElectrodeOffsetToToolDatum(P1);
end;

procedure TGEO.GetPCMProbeVector(VectorName : Pchar; var P1 :TPlocation;var P2:TPlocation);
var
PCMP1,PCMP2 : TXYZ;
begin
PCM.GetProbeVector(VectorName,PCMP1,PCMP2);
P1 := TPLocation(PCMP1);
P2 := TPLocation(PCMP2);
end;

procedure TGEO.OnMachineShow;
begin
with GEO do
     begin
     //formstyle := fsStayOnTop;
     Width := 430;
     height := 520;
     Left := 100;
     Top := 77;
     end;
//MachinePages.ActivePage := GEOPositionSheet;
MachinePages.ActivePage := MachineDisplay;
FormResize(Self);
Show;

end;

procedure TGEO.UpdateMachineReportDataAndDisplay;
begin

UpdateReportGrid;
if (SelectedDisplayVector >=0) and (SelectedDisplayVector < Length(TV1.FReportVectorList)) then
   begin
   UpdateDisplayedVectorData(SelectedDisplayVector);
   UpdateActToNomVectorDiffDisplay;
   end;
TV1.Update3DDisplay(Paper,umCustomOnly);
end;

procedure TGEO.LoadPCMandinitialise(afile :PChar; Sqr, Retry : Integer; Fit : Double);
var
NSAPPoint,NSAendPoint,NSARotPoint : TXYZ;
NameList : TStringList;
ConstraintCount : Integer;
P1              : TXYZ;
P1Pos           : TPlocation;
Constraint : array[0..1023] of Char;

begin
PCM.LoadConfig(afile,Sqr,Retry,Fit);
PCM.GetStackingAxis(NSAPPoint,NSAendPoint,NSARotPoint);
Tv1.SetNominalStackingAxis(NSAPPoint.x,
                           NSAPPoint.y,
                           NSAPPoint.z,
                           NSAEndPoint.x,
                           NSAEndPoint.y,
                           NSAEndPoint.z,
                           NSARotPoint.x,
                           NSARotPoint.y,
                           NSARotPoint.z);
Tv1.SetActualStackingAxis(NSAPPoint.x,
                           NSAPPoint.y,
                           NSAPPoint.z,
                           NSAEndPoint.x,
                           NSAEndPoint.y,
                           NSAEndPoint.z,
                           NSARotPoint.x,
                           NSARotPoint.y,
                           NSARotPoint.z);
PCM.GetConstraintNames(Constraint,1024);
NameList := TStringList.Create;
try
NameList.Text := Constraint;
for ConstraintCount := 0 to NameList.Count-1 do
    begin
    PCM.GetProbePoint(PChar(NameList[ConstraintCount]),P1);
    P1POs.x := P1.X;
    P1POs.y := P1.Y;
    P1POs.z := P1.Z;

    P1Pos := TV1.LinearCorrected(P1Pos);
    TV1.AddReportPointat(P1Pos.x,-P1Pos.y,P1Pos.z,NameList[ConstraintCount],rpPartRelative,True,False);
    end;
finally
NameList.Free;
end;
if FDmode = dmOnmachine then
   begin
   tV1.Update3DDisplay(Paper,umCustomOnly);
   end;
end;


procedure TGEO.SetDisplayMode(DMode :TDisplayModes);
begin
if FDMode = DMode then exit;
case DMode of
     dmOnMachine:
     begin
     TV1.UserMode := umRuntime;
     end;
     dmInvisible:
     begin
     TV1.UserMode := umRuntime;
     end;
end; // case
FDmode := Dmode;
FormResize(Self);
end;



procedure TGEO.FormCreate(Sender: TObject);
var
SelectedData :TGridRect;
PCMdllPath : String;
SPos : Integer;
Translator : TTranslator;
begin
GeoReady := False;
SelectedJogAxis := 0;
TV1.NumberofAxes := 7;
tV1.DisplayMode := dmFullDisplay;
LastObject := nil;
if Screen.Width >=1024 then
//if 1=2 then
   begin
   FormWidth := 1024;
   FormHeight := 768;
   end
else
   begin
   FormWidth := 800;
   FormHeight := 600;
   end;
Width := FormWidth;
Height := FormHeight;
Top :=0;
Left := 0;
DrawColor := clBlue;
TV1.ClearAllObjects;

FormResize(self);

With GEOPosGrid do
      begin
      ColWidths[0] := 35;
      ColWidths[1] := 100;
      Cells[0,0] := 'Axis';
      Cells[1,0] := 'Position';
      Cells[0,1] := 'X';
      Cells[0,2] := 'Y';
      Cells[0,3] := 'Z';
      Cells[0,4] := 'A';
      Cells[0,5] := 'B';
      Cells[0,6] := 'C';
      END;

With ReportPointGrid do
     begin
     ColWidths[0]:=100;
     Cells[0,0] := 'Pnt. ID';
     Cells[1,0] := 'X';
     Cells[2,0] := 'Y';
     Cells[3,0] := 'Z';
     Cells[0,1] := 'PartDatum';
     Cells[0,2] := 'ToolDatum';
     Cells[0,3] := 'Electrode';
     Row := 1;
     SelectedData .TopLeft.y := ReportPointGrid.Row;
     SelectedData .TopLeft.x := 1;
     SelectedData .BottomRight.y := ReportPointGrid.Row;
     SelectedData .BottomRight.x := 3;
     ReportPointGrid.Selection := SelectedData;
     end;

With RunVecDiffGrid do
     begin
     ColWidths[0]:=100;
     Cells[0,1] := 'Translation';
     Cells[0,2] := 'Rotation';
     Cells[1,0] := 'X';
     Cells[2,0] := 'Y';
     Cells[3,0] := 'Z';
     Row := 0;
     Col := 0;
     end;

With VectorDataGrid do
     begin
     ColWidths[0]:=55;
     Cells[0,1] := 'Cent.Pos.';
     Cells[0,2] := 'Pnt1 Pos.';
     Cells[0,3] := 'Pnt2 Pos.';
     Cells[1,0] := 'X';
     Cells[2,0] := 'Y';
     Cells[3,0] := 'Z';
     Row := 0;
     Col := 0;
     end;

With VectorRotationGrid do
     begin
     ColWidths[0]:=55;
     Row := 0;
     Col := 0;
     end;


TV1.UserMode := umEditor;
if FileExists(DLLProfilePath + '\GeoFiles\hsd62.geo') then
   begin
   Tv1.LoadTreeFromDisc(DLLProfilePath + '\GeoFiles\hsd62.geo',False);
   end
else
   begin
   raise EObjectNameException.create('Geo Model File Not Found');
   end;
UpdateEverythingAndDisplay;
Paper.ShowMode := smCustom;
tV1.Update3DDisplay(Paper,umCustomOnly);

MachineNavigator := TG3DNavigator.Create(Self);
with MachineNavigator do
     begin
     Parent := MachineDisplay;
     Align := alLeft;
     OnMotionButton := MachineNavButtonPressed;
     end;
PCMdllPath := Uppercase(DllProfilePath);
SPos := Pos('\LIVE',PCMdllPath);
if SPos > 0 then
  begin
  PCMdllPath := Copy(PCMdllPath,1,SPos);
  PCMdllPath := PCMdllPath+'lib\pcm.dll';
  end
else
  begin
  pcmdllPath := 'C:\ACNC32\profile\lib\pcm.dll';
  end;
PCM := TPCMPlugin.Create(pcmdllPath);
FDMode := dmNone;
SetDisplayMode(dmOnMachine);
MoveMode := mmRotate;
TV1.DebugReported := ShowDebugState;
if not assigned(TV1.NDPDatumNode) then
   begin
   TV1.AddObjectatNode(TV1.PartDatumNode,t3oNDPDatumNode,False);
   end;
Translator := TTranslator.Create;
try
with Translator do
  begin
  MachineFitButton.Legend := GetString('$FITMACHINE');
  PointFitButton.Legend := GetString('$FITPOINTS');
  GEOZoomIn.Legend := GetString('$GEOZOOMIN');
  GEOZoomOut.Legend := GetString('$GEOZOOMOUT');
  GEOCloseBtn.Legend := GetString('$GEOCLOSE');
  GEOPositionSheet.Caption := GetString('$GEOPosistion');
  MachineDisplay.Caption := GetString('$GEODisplay');
  MachinePoints.Caption := GetString('$GEOPoints');
  VectorPanel.Caption := GetString('$GEOVectors');
  TabSheet4.Caption := GetString('$GEOVECDiff');
  Panel2.Caption := GetString('$GEODiffTitle');
  HidePCM.Caption := GetString('$HIDEPCM');
  ShowPCMButton.Caption := GetString('$SHOWPCM');
  end;

finally
Translator.Free;
end;
MachinePages.ActivePage := MachineDisplay;
end;




procedure TGEO.RotateSelectClick(Sender: TObject);
begin
MoveMode := mmRotate;
end;

procedure TGEO.TranslateSelectClick(Sender: TObject);
begin
MoveMode := mmTranslate;

end;






procedure TGEO.FormResize(Sender: TObject);
begin
try
if not GeoReady then exit;
if FDMode = dmOnMachine then
   begin
   if Geo.Width < 500 then
      begin
      MachineNavigator.Left := MachineDisplayPanel.Width +2;
      MachinePanel.align := alNone;
      MachinePanel.Height := 250;
      MachinePanel.align := alBottom;
      MachineNavigator.Width := MachinePanel.Width div 2;
      MachinePanel.Visible := True;
      MachineDisplayPanel.Width := (MachinePanel.Width div 2)-15;
      MachineDisplayPanel.Align := alRight;
      MachineNavigator.Top := 1;
      MachineNavigator.Left := 2;
      With GEOPosGrid do
           begin
           Width := MachinePanel.Width div 2;
           ColWidths[1] := width- 40;
           end;
      Paper.align := alClient;
      end
else
    begin
      MachinePanel.align := alNone;
      MachinePanel.Width := (Width div 2)-2;
      MachinePanel.align := alRight;
      MachinePanel.Width := (Width div 2)-2;
      MachineNavigator.Width := MachinePanel.Width div 2;
      MachinePanel.Visible := True;
      MachineNavigator.Top := 1;
      MachineNavigator.Left := 2;
      MachineDisplayPanel.Width := (MachinePanel.Width div 2)-15;

      MachineDisplayPanel.Align := alNone;
      MachineDisplayPanel.Top := 2;
      MachineDisplayPanel.Height := 200;
      MachineDisplayPanel.Left := MachineNavigator.Width+2;

      With GEOPosGrid do
           begin
           Width := MachinePanel.Width div 2;
           ColWidths[1] := width- 40;
           end;
      Paper.Width := MachinePanel.Width;
      Paper.align := alLeft;

    end;

   Paper.align := alClient;
//   MachineDisplayPanel.Width := (MachinePanel.Width div 2)-15;
   GeoZoomIn.Height := MachineDisplayPanel.Height div 3;
   GeoZoomOut.Height := MachineDisplayPanel.Height div 3;
   MachineFitButton.Height := MachineDisplayPanel.Height div 3;
   PointFitButton.Height := MachineDisplayPanel.Height div 3;
   ShowPCMButton.Height := MachineDisplayPanel.Height div 3;
   HidePCM.Height := MachineDisplayPanel.Height div 3;
   MachineFitButton.Width := (MachineDisplayPanel.Width div 2) -2;
   PointFitButton.Width := (MachineDisplayPanel.Width div 2) -2;
   ShowPCMButton.Width := (MachineDisplayPanel.Width-2)div 2;
   HidePCM.Width := (MachineDisplayPanel.Width-2)div 2;
   GeoZoomIn.Width := (MachineDisplayPanel.Width div 2) -2;
   GeoZoomOut.Width := (MachineDisplayPanel.Width div 2) -2;
   MachineFitButton.Left := 2;
   PointFitButton.Left := (MachineDisplayPanel.Width div 2)+1;
   GeoZoomIn.Left := 2;
   GeoZoomOut.Left := (MachineDisplayPanel.Width div 2)+1;

   ShowPCMButton.Left := MachinePanel.Width - ShowPCMButton.Width-10;
   ShowPCMButton.Top := MachineDisplayPanel.Height-ShowPCMButton.Height-5;
   HidePCM.Left := MachinePanel.Width - ShowPCMButton.Width-10;
   HidePCM.Top := MachineDisplayPanel.Height-ShowPCMButton.Height- ShowPCMButton.Height-10;
   MachineFitButton.Top := 0;
   PointFitButton.Top := 0;
   GeoZoomIn.Top := (MachineDisplayPanel.Height div 3)+3;
   GeoZoomOut.Top := (MachineDisplayPanel.Height div 3)+3;
   with GEOCloseBtn do
        begin
        Width := GeoZoomIn.Width;
        Height := GeoZoomIn.Height-8;
        Top := GeoZoomIn.Top+ GeoZoomIn.Height+2;
        end;
   GEOCloseBtn.Left :=   (MachineDisplayPanel.Width div 2)- (GEOCloseBtn.Width div 2); 



//   GeoVectorDown.Height := MachineDisplayPanel.Height div 3;
//   GeoVectorUp.Height := MachineDisplayPanel.Height div 3;
   GeoVectorUp.Top := VectorChoosePanel.Height+10;
   GeoVectorDown.Top := GeoVectorUp.Top+ GeoVectorUp.Height+5;

   GEOVectorUP.Width := GeoZoomIn.Width;
   GEOVectorDown.Width := GeoZoomIn.Width;
   GEOVectorUP.Left := MachinePanel.Width-GEOVectorUP.Width-15;
   GEOVectorDown.Left := MachinePanel.Width-GEOVectorDown.Width-15;

   ReportPointGrid.Width := 275;
   GEOPointsUP.Left := ReportPointGrid.Width+10;
   GEOPointsDown.Left := ReportPointGrid.Width+10;
   exit;
   end;

Paper.align := alNone;
MachinePanel.align := alNone;
MachinePanel.Visible := False;

//MachinePanel.Visible := False;

if FormWidth = 1024 then
   begin
   tv1.Width := cLargeTreeWidth-170;
   with Paper do
        begin
        top := 2;
        Left := 2;
        Width := cLargePaperWidth;
        Height := cLargePaperHeight;
        end;
   end
else
   begin
   with Paper do
        begin
        top := 2;
        Left := 2;
        Width := cSmallPaperWidth;
        Height := cSmallPaperHeight;
        end;
   end;
finally
//if assigned(MachineNavigator) then MachineNavigator.Width := MachinePage.width-2;
Tv1.FitAllObjectsOnSurface(Paper.CustomView);
tV1.Update3DDisplay(Paper,umAll);
end;

end;






procedure TGEO.TV1Change(Sender: TObject; Node: TTreeNode);
var
ObjectType :  T3DObjects;
HintBuffer :  String;
begin
if LastObject <> nil then
   begin
   LastObject.Selected := False;
   if LastObject.ObjectType <> t3oReportPoint then LastObject.DisplayReferencePoint := False;
   end;
if TV1.Selected = nil then exit;
if  TV1.Selected.Data = nil then exit;
   begin
   SelectedObject := TV1.Selected.Data;
   ObjectType := TG3DObject(SelectedObject).ObjectType;
   case ObjectType of

      t3oLine,t3oBox,t3oDisc:
        begin
        with selectedObject as TG3dObject do
           begin
           HintBuffer := Format('Rot: %f,%f,%f',[AbsoluteRotation.x,AbsoluteRotation.y,AbsoluteRotation.z])
           end;
      TV1.Hint := HintBuffer;
      TV1.ShowHint := True;
        end;
      else
          begin
          TV1.Hint := '';
          TV1.ShowHint := False;
          end;
   end; // CASE

{   case ObjectType of
     t3oLine:
     begin
     BufferLine := TG3dLine(SelectedObject);
     end;


     t3oBox:
     begin
     BufferBox := TG3dBox(SelectedObject);
     end;
     t3oDisc:
     begin
     BufferDisc := TG3dDisc(SelectedObject);
     end;
   end;}

LastObject := TG3DObject(SelectedObject);
Tv1.Update3DDisplay(Paper,umAll);


   end;
end;






function  TGEO.FloatDisplay(Value : Double): String;
begin
Result := Format('%9.4f',[Value]);
end;


















function TGEO.isValidAxis(AxisNumber : Integer):Boolean;
begin
Result := (AxisNumber>=0) and (AxisNumber<=TV1.NumberofAxes-1)
end;


procedure TGEO.EditRunPagesChange(Sender: TObject);
begin
//DatumAllBtnClick(Self);


FormResize(Self);
end;

procedure TGEO.AxisSelectGroupClick(Sender: TObject);
begin
JogObject := TV1.GetJogObject(SelectedJogAxis,JogNode);
JogAxis := TV1.GetJogAxis(SelectedJogAxis);
//UpdateAxisPOsitionGrid;
end;



procedure TGEO.UpdateReportGrid;
procedure UpdateRow(RowNum,PointNum : Integer);
begin
ReportPointGrid.Cells[0,RowNum] := TV1.ReportPointName[PointNum];
ReportPointGrid.Cells[1,RowNum] := UnitModified(TV1.ReportPointPosition[PointNum].x);
ReportPointGrid.Cells[2,RowNum] := UnitModified(TV1.ReportPointPosition[PointNum].y);
ReportPointGrid.Cells[3,RowNum] := UnitModified(TV1.ReportPointPosition[PointNum].z);
end;
var
PointCount : Integer;
begin
if ReportPointGrid.RowCount <> TV1.NumberofReportPoints +4 then
   begin
   ReportPointGrid.RowCount := TV1.NumberofReportPoints +4;
   end;
Tv1.UpdateReportData(False);
if assigned(TV1.PartDatumNode) then
   begin
   ReportPointGrid.Cells[1,1] := UnitModified(Tv1.PartDatumPosition.x);
   ReportPointGrid.Cells[2,1] := UnitModified(Tv1.PartDatumPosition.y);
   ReportPointGrid.Cells[3,1] := UnitModified(Tv1.PartDatumPosition.z);
   end;
if assigned(TV1.ToolDatumNode) then
   begin
   ReportPointGrid.Cells[1,2] := UnitModified(Tv1.ToolDatumPosition.x);
   ReportPointGrid.Cells[2,2] := UnitModified(Tv1.ToolDatumPosition.y);
   ReportPointGrid.Cells[3,2] := UnitModified(Tv1.ToolDatumPosition.z);
   end;
if assigned(TV1.ELectrodeNode) then
   begin
   ReportPointGrid.Cells[1,3] := UnitModified(Tv1.ELectrodePosition.x);
   ReportPointGrid.Cells[2,3] := UnitModified(Tv1.ELectrodePosition.y);
   ReportPointGrid.Cells[3,3] := UnitModified(Tv1.ELectrodePosition.z);
   end;
For PointCount := 0 to TV1.NumberofReportPoints-1 do
    begin
    UpdateRow(4+PointCount,PointCount);
    end;

end;



procedure TGEO.UpdateEverythingAndDisplay;
BEGIN
UpdateReportGrid;
UpdateDisplayedVectorData(SelectedDisplayVector);
UpdateVectorSelectList;
//UpdateVectorDiffDisplay;
UpdateActToNomVectorDiffDisplay;
UpdateGeoPositionGrid;
Tv1.Update3DDisplay(Paper,umAll);
END;


procedure TGEO.UpdateVectorSelectList;
var
vCount    : Integer;
ListEntry : TVectorData;
begin
TV1.UpdateReportData(True);
if Length(Tv1.FReportVectorList) < 1 then exit;
with VectorSelectList do
     begin
     Items.Clear;
     For VCount := 0 to Length(Tv1.FReportVectorList)-1 do
         begin
         ListEntry  := TV1.ReportVectorData[VCount];
         Items.Add(ListEntry.Name);
         end;
     Text := Items[0];
     end;
end;

procedure TGEO.UpdateActToNomVectorDiffDisplay;
var
//v1Index,V2Index : Integer;
DifData : TVectorDifference;
begin
if (TV1.NominalVectorIndex < 0) or (TV1.ActualVectorIndex < 0) then exit;
if TV1.GetVectorDifference(TV1.NominalVectorIndex,TV1.ActualVectorIndex,DifData) then
   begin
   with  RunVecDiffGrid do
         begin
         Cells[1,1] := UnitModified(DifData.Translation.x);
         Cells[2,1] := UnitModified(DifData.Translation.y);
         Cells[3,1] := UnitModified(DifData.Translation.z);
         Cells[1,2] := UnitModified(DifData.Rotation.x);
         Cells[2,2] := UnitModified(DifData.Rotation.y);
         Cells[3,2] := UnitModified(DifData.Rotation.z);
         end;
   end;

end;




procedure TGEO.UpdateDisplayedVectorData(ListPosition: Integer);
var
ListEntry : TVectorData;

begin
ListEntry := TV1.ReportVectorData[ListPosition];
with  VectorDataGrid do
      begin
      Cells[1,1] := UnitModified(ListEntry.CentrePosition.x);
      Cells[2,1] := UnitModified(ListEntry.CentrePosition.y);
      Cells[3,1] := UnitModified(ListEntry.CentrePosition.z);
      Cells[1,2] := UnitModified(ListEntry.Point1Position.x);
      Cells[2,2] := UnitModified(ListEntry.Point1Position.y);
      Cells[3,2] := UnitModified(ListEntry.Point1Position.z);
      Cells[1,3] := UnitModified(ListEntry.Point2Position.x);
      Cells[2,3] := UnitModified(ListEntry.Point2Position.y);
      Cells[3,3] := UnitModified(ListEntry.Point2Position.z);
      end;
with  VectorRotationGrid do
      begin
      Cells[1,0] := Format('%7.3f',[ListEntry.Rotation.x]);
      Cells[2,0] := Format('%7.3f',[ListEntry.Rotation.y]);
      Cells[3,0] := Format('%7.3f',[ListEntry.Rotation.z]);
      end;

VectorLengthDisplay.Text := FloatToStr(ListEntry.Length);
end;




procedure TGEO.VectorSelectListChange(Sender: TObject);
begin
if TV1.NumberofReportVectors > 0 then
   begin
   if (VectorSelectList.Itemindex >= 0)and(VectorSelectList.ItemIndex<Length(TV1.FReportVectorList)) then
      begin
      SelectedDisplayVector := VectorSelectList.ItemIndex;
      Tv1.ShowSelectedVector(SelectedDisplayVector);
      UpdateDisplayedVectorData(SelectedDisplayVector);
      end;
   end ;
end;


procedure TGEO.MachineNavButtonPressed(Sender : TObject);
var
MMode : TMoveModes;
begin
MMode := MachineNavigator.MotionMode;
case TSpeedButton(Sender).Tag of
     1: MoveWorld(MMode,0,-5);   //UP
     2: MoveWorld(MMode,0,5);   //down
     3: MoveWorld(MMode,1,-5);   //left
     4: MoveWorld(MMode,1,5);   // right
     5: MoveWorld(MMode,2,-5);   // upright
     6: MoveWorld(MMode,2,5);   // downleft
     7:
       begin
       Paper.Rotation := SetPRotation(90,0,0);   // TOP
       Tv1.FitAllObjectsOnSurface(Paper.CustomView);
       end;
     8:
       begin
       Paper.Rotation := SetPRotation(0,0,-90);     // side
       Tv1.FitAllObjectsOnSurface(Paper.CustomView);
       end;
     9:
       begin
       Paper.Rotation := SetPRotation(0,0,0);    // front
       Tv1.FitAllObjectsOnSurface(Paper.CustomView);
       end;
     end; // case;
tV1.Update3DDisplay(Paper,umCustomOnly);

end;

procedure TGEO.MoveWorld(Motion :TMoveModes;Axis :Integer;Increment : Integer);
begin
if Motion = mmTranslate then
   begin
   case Axis of
        0: Paper.Translation := AddScreenLocation(Paper.Translation,SetScreenLocation(0,Increment));
        1: Paper.Translation := AddScreenLocation(Paper.Translation,SetScreenLocation(Increment,0));
        2: Paper.Translation := AddScreenLocation(Paper.Translation,SetScreenLocation(-Increment,Increment));
        end; // case
   end
else

   begin
   case Axis of
        0: Paper.Rotation := AddRotation(Paper.Rotation,SetPRotation(Increment,0,0));
        2: Paper.Rotation := AddRotation(Paper.Rotation,SetPRotation(0,Increment,0));
        1: Paper.Rotation := AddRotation(Paper.Rotation,SetPRotation(0,0,Increment));
        end; // case
   end;

end;

procedure TGEO.ShowPCMButtonClick(Sender: TObject);
begin
PCM.Show;
end;

procedure TGEO.UpdateGeoPositionGrid;
var
Count : Integer;
Val   : Double;
begin
if assigned(GEO) then
   begin
   For Count := 0 to TV1.NumberofAxes do
    begin
    if (GEO.InchMode) and  ((Count < 3) or (Count > 5)) then
       begin
       GEOPosGrid.Cells[1,Count+1] := Format(' %-7.4f',[MetricToInch(Tv1.AxisPosition[Count])]);
       end
    else
        begin
        GEOPosGrid.Cells[1,Count+1] := Format(' %-7.3f',[Tv1.AxisPosition[Count]]);
        end;
    end;
   end;
end;




procedure TGEO.GEOPointsDownClick(Sender: TObject);
var
SelectedData :TGridRect;
begin

if  ReportPointGrid.Row < (ReportPointGrid.RowCount-1) then
    begin
    ReportPointGrid.Row := ReportPointGrid.Row+1;
    SelectedData .TopLeft.y := ReportPointGrid.Row;
    SelectedData .TopLeft.x := 1;
    SelectedData .BottomRight.y := ReportPointGrid.Row;
    SelectedData .BottomRight.x := 3;
    ReportPointGrid.Selection := SelectedData;
    end;

end;

procedure TGEO.HidePCMClick(Sender: TObject);
begin
PCM.Close;
end;

procedure TGEO.ReportPointGridSelectCell(Sender: TObject; ACol,
  ARow: Integer; var CanSelect: Boolean);
var
PointName : String;
PName     : String;
PointCount : Integer;
Point      : TG3dReportPoint;
TNode      : TTreeNode;
begin
PointName := ReportPointGrid.Cells[0,ARow];
SelectedPointIndex := Tv1.GetReportPointIndexfor(PointName);

For PointCount := 0 to Length(TV1.FReportPointList)-1 do
    begin
    PName := TV1.GetReportPointName(PointCount);
    Point := TV1.FindObjectandNode(PName,TNode);
    Point.Selected := False;
    end;
if SelectedPointIndex >=0 then
   begin
   PName := TV1.GetReportPointName(SelectedPointIndex);
   Point := TV1.FindObjectandNode(PName,TNode);
   Point.Selected := True;
   end
else
   begin
   // here for Part,Tool and Electrode Points
   if assigned(TV1.PartDatumNode) then
         begin
         Point := TV1.PartDatumNode.Data;
         Point.Selected := True;
         if PointName = 'PartDatum' then
            begin
            Point.Selected := True;
            end
         else
            begin
            Point.Selected := False;
            end
         end;
   if assigned(TV1.ToolDatumNode) then
         begin
         Point := TV1.ToolDatumNode.Data;
         if PointName = 'ToolDatum' then
            begin
            Point.Selected := True;
            end
         else
            begin
            Point.Selected := False;
            end
         end;
   if assigned(TV1.ElectrodeNode) then
         begin
         Point := TV1.ElectrodeNode.Data;
         if PointName = 'Electrode' then
            begin
            Point.Selected := True;
            end
         else
            begin
            Point.Selected := False;
            end
         end;
   end;
end;

constructor TGEO.Create(AOwner : TComponent);
begin
inherited Create(Application);
GEO := Self;
GeoReady := True;
TV1.GlobalUpdateCount := 0;
SetExtentsMode(emUnLatched);
end;

procedure TGEO.GEOCloseBtnClick(Sender: TObject);
begin
Host.ClientClose;
end;


procedure TGEO.PointFitButtonMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
case ExtentsMode of
  emUnlatched:
  begin
  SetExtentsMode(emlatched);
  TV1.ExtentsOverscale := 1.1;
  TV1.FitToReportPoints(Paper.CustomView);
  tV1.Update3DDisplay(Paper,umCustomOnly);
  end;

  emLatched:
  begin
  SetExtentsMode(emUnlatched);
  TV1.ExtentsOverscale := 1.1;
  TV1.FitToReportPoints(Paper.CustomView);
  tV1.Update3DDisplay(Paper,umCustomOnly);
  end;
end;
end;

procedure TGEO.MachineFitButtonMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
SetExtentsMode(emUnLatched);
TV1.FitAllObjectsOnSurface(Paper.CustomView);
tV1.Update3DDisplay(Paper,umCustomOnly);
end;

procedure TGEO.GEOZoomInMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
case ExtentsMode of
  emUnlatched:
  begin
  Paper.Scale := Paper.Scale*0.9;
  tV1.Update3DDisplay(Paper,umCustomOnly);
  end;
  emlatched:
  begin
  TV1.ExtentsOverscale := TV1.ExtentsOverscale*0.9;
  if TV1.ExtentsOverscale < 1.1 then TV1.ExtentsOverscale := 1.1;
  tV1.Update3DDisplay(Paper,umCustomOnly);
  end;
end;
end;

procedure TGEO.GEOZoomOutMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
case ExtentsMode of
  emUnlatched:
  begin
  Paper.Scale := Paper.Scale*1.1;
  tV1.Update3DDisplay(Paper,umCustomOnly);
  end;
  emlatched:
  begin
  TV1.ExtentsOverscale := TV1.ExtentsOverscale*1.1;
  tV1.Update3DDisplay(Paper,umCustomOnly);
  end;
end;
end;

procedure TGEO.GEOCloseBtnMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
SetExtentsMode(emUnLatched);
Host.ClientClose;
end;

procedure TGEO.FormActivate(Sender: TObject);
begin
SetExtentsMode(emUnLatched);
FormResize(Self);
end;

procedure TGEO.GEOPointsUPMouseUp(Sender: TObject;Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var
SelectedData :TGridRect;
begin
if  ReportPointGrid.Row > 1 then
    begin
    ReportPointGrid.Row := ReportPointGrid.Row-1;
    SelectedData .TopLeft.y := ReportPointGrid.Row;
    SelectedData .TopLeft.x := 1;
    SelectedData .BottomRight.y := ReportPointGrid.Row;
    SelectedData .BottomRight.x := 3;
    ReportPointGrid.Selection := SelectedData;
    end;
end;

procedure TGEO.GEOPointsDownMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var
SelectedData :TGridRect;
begin
if  ReportPointGrid.Row < (ReportPointGrid.RowCount-1) then
    begin
    ReportPointGrid.Row := ReportPointGrid.Row+1;
    SelectedData .TopLeft.y := ReportPointGrid.Row;
    SelectedData .TopLeft.x := 1;
    SelectedData .BottomRight.y := ReportPointGrid.Row;
    SelectedData .BottomRight.x := 3;
    ReportPointGrid.Selection := SelectedData;
    end;

end;

procedure TGEO.GEOVectorUpMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
if VectorSelectList.ItemIndex > 0 then
   begin
   VectorSelectList.ItemIndex := VectorSelectList.ItemIndex-1;
   VectorSelectListChange(Self)
   end;
end;

procedure TGEO.GeoVectorDownMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
if VectorSelectList.ItemIndex < (VectorSelectList.Items.Count-1) then
   begin
   VectorSelectList.ItemIndex := VectorSelectList.ItemIndex+1;
   VectorSelectListChange(Self)
   end;
end;


procedure TGEO.ShowDebugState(Sender: TObject);
begin
end;

procedure TGEO.DebufStartButtonClick(Sender: TObject);
var
ResLinear,ResRotary : TXYZ;
begin
DebugStartPositions := TV1.GetAllAxisPOsitions;
DEbugStartStage := 0;
GEO.TV1.DebugStep := True;
GEO.TV1.GetNDP(0,ResLinear,ResRotary,0,0);
DebugStageDisplay.Text := IntToStr(DEbugStartStage);
UpdateEverythingAndDisplay;
end;

procedure TGEO.DebugStepButtonClick(Sender: TObject);
var
ResLinear,ResRotary : TXYZ;
begin
GEO.TV1.DebugStep := True;
GEO.TV1.GetNDP(0,ResLinear,ResRotary,0,0);
UpdateEverythingAndDisplay;
DebugStageDisplay.Text := IntToStr(DEbugStartStage);
end;




procedure TGEO.FinalInstall;
begin
  inherited;
  InchModeTag := Named.AquireTag('NC1InchModeDefault', 'TMethodTag', nil);
  UserLevel   := Named.AquireTag('AccessLevel', 'TIntegerTag', TagChangeHandler);
  InchMode := Named.GetAsBoolean(InchModeTag);
  GEONavOPtionTag := Named.AquireTag('_OptionGEONav', 'TIntegerTag', nil);
  GEONavInstalled := Named.GetAsBoolean(GEONavOPtionTag);

end;

procedure TagChangeHandler(Tag : TTagRef);stdcall
begin
GEO.TagChange(Tag);
end;


procedure TGEO.TagChange(ATag: TTagRef);
begin
if Atag = UserLevel then
   begin
   if Named.GetAsInteger(ATag) > 5 then
      begin
      ShowPCMButton.Visible := True;
      HidePCM.Visible := True;
      VersionLabel.Visible := True;

      end
   else
       begin
       ShowPCMButton.Visible := False;
       HidePCM.Visible := False;
      VersionLabel.Visible := False;
       end;
   end

end;

procedure  TGEO.GetMachineNDP(Crot :Double;var ResLinear,ResRotary :TXYZ;Par1,Par2 : Integer);
var
StartPositions : TAxisPosn;
CurrentStage     : Integer;
NewPOsition    : TAxisPosn;
NDPError       : Integer;
begin

StartPositions := TV1.GetAllAxisPOsitions;
InhibitExternalUpdate := True;
try
DEbugStartStage := 0;
DebugMemo.Lines.Clear;
NDPError := GEO.TV1.GetNDP(CRot,ResLinear,ResRotary,Par1,Par2);
if NDPError = 0 then
  begin
  Tv1.RestoreAllAxisPOsitions(StartPositions);
  end
else
  begin
  case  NDPError of
  1: raise EGEOError.Create('No Nominal Drill vetor defined');
  2: raise EGEOError.Create('Too many itterations to find Drill position');
  end; // case
  end
finally
GEO.InhibitExternalUpdate := False;
end;

end;

procedure TGEO.DisplaySweep;
begin
  inherited;
  try
  TV1.RuntimePaint(Paper.CustomView);
  except
  end;
end;

procedure TGEO.SetExtentsMode(Mode: TExtentsModes);
begin
ExtentsMode := Mode;
case Mode of
  emLatched:
  begin
  PointFitButton.IndicatorOn := True;
  TV1.ExtentsOverscale := 1.1;
  end;

  emUnLatched:
  begin
  PointFitButton.IndicatorOn := False;
  end;
end;

end;



// N.B. At present any EndUpdate will enable update.
// THey are no nested so the update count is not as yet actually implimented
procedure TGEO.ExternalBeginUpdate;
begin
if TV1.GlobalUpdateCount <=0 then
  begin
  TV1.GlobalUpdateCount := 1;
  end
else
  begin
  inc(TV1.GlobalUpdateCount)
  end;


end;

// N.B. At present any EndUpdate will enable update.
// THey are no nested so the update count is not as yet actually implimented
procedure TGEO.ExternalEndUpdate;
begin
  if TV1.GlobalUpdateCount > 0 then
    begin
    TV1.MatchObjectListToTree;
    TV1.GlobalUpdateCount := 0;
    end;
end;


Initialization
  TAbstractFormPlugin.AddPlugin('GEOPlugin',TGEO);
end.
