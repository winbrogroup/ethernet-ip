unit G3dNavigator;

interface
{$R  NavImages.res}

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls,commctrl,Menus,extctrls,buttons,ACNC_TouchGlyphSoftkey;


  
type
TMoveModes = (mmRotate,mmTranslate);
TG3DNavigator = class(TCustomPanel)
  private
  UpButton : TTouchGlyphSoftkey;
  DownButton : TTouchGlyphSoftkey;
  LeftButton : TTouchGlyphSoftkey;
  RightButton : TTouchGlyphSoftkey;
  UpRightButton : TTouchGlyphSoftkey;
  DownLeftButton : TTouchGlyphSoftkey;
  FrontButton : TTouchGlyphSoftkey;
  SideButton : TTouchGlyphSoftkey;
  TopButton : TTouchGlyphSoftkey;
  RotateButton : TTouchGlyphSoftkey;
  TranslateButton : TTouchGlyphSoftkey;
  PressedButton   : TTouchGlyphSoftkey;

  ButtonTimer : TTimer;

procedure MotionModeButPressed(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);


  procedure ButtonMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
  procedure ButtonMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);

  procedure DisplayMove(Sender: TObject);

  Procedure Resize; override;

  public
  OnMotionButton           : TNotifyEvent;
  MotionMode               : TMoveModes;
  constructor Create(AOwner: TComponent);override;
  destructor Destroy; override;
  end;

implementation
constructor TG3DNavigator.Create(AOwner: TComponent);
var
Image : TBitmap;
begin
inherited;
MotionMode := mmRotate;
BevelInner := bvNone;
BevelOuter := bvNone;
ButtonTimer := TTimer.create(Self);
with ButtonTimer do
     begin
     Interval := 50;
     OnTimer := DisplayMove;
     Enabled := False;
     end;
Image := TBitMap.Create;
try
UpButton := TTouchGlyphSoftkey.Create(Self);
with UpButton do
     begin
     Parent := Self;
     OnMouseDown := ButtonMouseDown;
     Layout :=  slCentreOverlay;
     ButtonColor := clBtnFace;
     ButtonTravel :=2;
     OnMouseUp := ButtonMouseUp;
     Image.LoadFromResourceName(HInstance,'UPARROW');
     Glyph := Image;
     Tag := 1;
     end;
DownButton := TTouchGlyphSoftkey.Create(Self);
with DownButton do
     begin
     Parent := Self;
     Layout :=  slCentreOverlay;
     ButtonColor := clBtnFace;
     ButtonTravel :=2;
     OnMouseDown := ButtonMouseDown;
     OnMouseUp := ButtonMouseUp;
     Image.LoadFromResourceName(HInstance,'DOWNARROW');
     Glyph := Image;
     Tag := 2;
     end;
LeftButton := TTouchGlyphSoftkey.Create(Self);
with LeftButton do
     begin
     Parent := Self;
     Layout :=  slCentreOverlay;
     ButtonTravel :=2;
     ButtonColor := clBtnFace;
     OnMouseDown := ButtonMouseDown;
     OnMouseUp := ButtonMouseUp;
     Image.LoadFromResourceName(HInstance,'LEFTARROW');
     Glyph := Image;
     Tag := 3;
     end;
RightButton := TTouchGlyphSoftkey.Create(Self);
with RightButton do
     begin
     Parent := Self;
     Layout :=  slCentreOverlay;
     ButtonTravel :=2;
     ButtonColor := clBtnFace;
     OnMouseDown := ButtonMouseDown;
     OnMouseUp := ButtonMouseUp;
     Image.LoadFromResourceName(HInstance,'RIGHTARROW');
     Glyph := Image;
     Tag := 4;
     end;
UpRightButton := TTouchGlyphSoftkey.Create(Self);
with UpRightButton do
     begin
     Layout :=  slCentreOverlay;
     ButtonTravel :=2;
     ButtonColor := clBtnFace;
     Parent := Self;
     OnMouseDown := ButtonMouseDown;
     OnMouseUp := ButtonMouseUp;
     Image.LoadFromResourceName(HInstance,'UPRIGHTARROW');
     Glyph := Image;
     Tag := 5;
     end;
DownLeftButton := TTouchGlyphSoftkey.Create(Self);
with DownLeftButton do
     begin
     Layout :=  slCentreOverlay;
     ButtonColor := clBtnFace;
     ButtonTravel :=2;
     Parent := Self;
     OnMouseDown := ButtonMouseDown;
     OnMouseUp := ButtonMouseUp;
     Image.LoadFromResourceName(HInstance,'DOWNLEFTARROW');
     Glyph := Image;
     Tag := 6;
     end;
TopButton := TTouchGlyphSoftkey.Create(Self);
with TopButton do
     begin
     Parent := Self;
     Layout :=  slCentreOverlay;
     ButtonColor := clBtnFace;
     ButtonTravel :=2;
     OnMouseDown := ButtonMouseDown;
     OnMouseUp := ButtonMouseUp;
     Image.LoadFromResourceName(HInstance,'BOXTOP');
     Glyph := Image;
     Tag := 7;
     end;
SideButton := TTouchGlyphSoftkey.Create(Self);
with SideButton do
     begin
     Parent := Self;
     Layout :=  slCentreOverlay;
     ButtonColor := clBtnFace;
     ButtonTravel :=2;
     OnMouseDown := ButtonMouseDown;
     OnMouseUp := ButtonMouseUp;
     Image.LoadFromResourceName(HInstance,'RIGHTSIDE');
     Glyph := Image;
     Tag := 8;
     end;
FrontButton := TTouchGlyphSoftkey.Create(Self);
with FrontButton do
     begin
     Parent := Self;
     Layout :=  slCentreOverlay;
     ButtonColor := clBtnFace;
     ButtonTravel :=2;
     OnMouseDown := ButtonMouseDown;
     OnMouseUp := ButtonMouseUp;
     Image.LoadFromResourceName(HInstance,'BOXFRONT');
     Glyph := Image;
     Tag := 9;
     end;

TranslateButton := TTouchGlyphSoftkey.Create(Self);
with TranslateButton do
     begin
     Parent := Self;
     Layout :=  slCentreOverlay;
     ButtonColor := clBtnFace;
     ButtonTravel :=2;
     OnMouseDown := ButtonMouseDown;
     OnMouseUp := ButtonMouseUp;
     HasIndicator := True;
     Image.LoadFromResourceName(HInstance,'TRANSLATE');
     NumGlyphs := 1;
     Glyph := Image;
     end;
RotateButton := TTouchGlyphSoftkey.Create(Self);
with RotateButton do
     begin
     Parent := Self;
     Layout :=  slCentreOverlay;
     ButtonColor := clBtnFace;
     ButtonTravel :=2;
     HasIndicator := True;
     OnMouseDown := ButtonMouseDown;
     OnMouseUp := ButtonMouseUp;
     Image.LoadFromResourceName(HInstance,'ROTATE');
     NumGlyphs := 4;
     Glyph := Image;
     end;
MotionMode := mmRotate;
TranslateButton.IndicatorOn := False;
RotateButton.IndicatorOn := True;

finally
Image.Free;
end;

end;


Procedure TG3DNavigator.Resize;
var
ButtonWidth,ButtonHeight : Integer;
leftMargin                   : Integer;
TopMargin                    : Integer;

begin
inherited;
ButtonWidth := (Width div 3) -3;
LeftMargin := ((width - (3*(ButtonWidth+1))) div 2)-1;
TopMargin := 2;
ButtonHeight := (((Height div 5)*4) div 3)-3;
with TopButton do
     begin
     Top := TopMargin;
     Left := LeftMargin;
     Width := ButtonWidth;
     Height := ButtonHeight;
     end;
with UpButton do
     begin
     Top := TopMargin;
     Left := ButtonWidth+LeftMargin+1;
     Width := ButtonWidth;
     Height := ButtonHeight;
     end;
with UpRightButton do
     begin
     Top := TopMargin;
     Left := (2*(ButtonWidth+1))+LeftMargin;
     Width := ButtonWidth;
     Height := ButtonHeight;
     end;
with LeftButton do
     begin
     Top := ButtonHeight+1+TopMargin;
     Left := LeftMargin;
     Width := ButtonWidth;
     Height := ButtonHeight;
     end;

with FrontButton do
     begin
     Top := ButtonHeight+1+TopMargin;
     Left := ButtonWidth+LeftMargin+1;
     Width := ButtonWidth;
     Height := ButtonHeight;
     end;

with RightButton do
     begin
     Top := ButtonHeight+1+TopMargin;
     Left := 2*(ButtonWidth+1)+LeftMargin;
     Width := ButtonWidth;
     Height := ButtonHeight;
     end;

with DownLeftButton do
     begin
     Top := 2*(ButtonHeight+1)+TopMargin;
     Left := LeftMargin;
     Width := ButtonWidth;
     Height := ButtonHeight;
     end;

with DownButton do
     begin
     Top := 2*(ButtonHeight+1)+TopMargin;
     Left := ButtonWidth+LeftMargin+1;
     Width := ButtonWidth;
     Height := ButtonHeight;
     end;

with SideButton do
     begin
     Top := 2*(ButtonHeight+1)+TopMargin;
     Left := 2*(ButtonWidth+1)+LeftMargin;
     Width := ButtonWidth;
     Height := ButtonHeight;
     end;
with TranslateButton do
     begin
     Top := 3*(ButtonHeight+1)+TopMargin;
     Height := Self.Height-(3*ButtonHeight)-4-TopMargin;
     Left := LeftMargin;
     Width := ((Self.Width-(2*LeftMargin)) div 2)-2;
     OnMouseUp := MotionModeButPressed;
     end;
with RotateButton do
     begin
     Top := 3*(ButtonHeight+1)+TopMargin;
     Height := Self.Height-(3*ButtonHeight)-4-TopMargin;
     Width := ((Self.Width-(2*LeftMargin)) div 2)-2;
     Left := LeftMargin+TranslateButton.Width;
     OnMouseUp := MotionModeButPressed;
     end;

end;

destructor TG3DNavigator.Destroy;
begin
inherited;

end;


procedure TG3DNavigator.DisplayMove(Sender: TObject);
begin
if assigned(OnMotionButton) then OnMotionButton(PressedButton);
end;

procedure TG3DNavigator.ButtonMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
begin
PressedButton := TTouchGlyphSoftkey(Sender);
if assigned(OnMotionButton) then OnMotionButton(PressedButton);
if assigned(ButtonTimer) then
   ButtonTimer.Enabled := True;
end;

procedure TG3DNavigator.ButtonMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
begin
if assigned(ButtonTimer) then
   ButtonTimer.Enabled := False;
end;


procedure TG3DNavigator.MotionModeButPressed(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
begin
if sender = TranslateButton then MotionMode := mmTranslate else MotionMode := mmRotate;
case MotionMode of
     mmTranslate:
     begin
     TranslateButton.IndicatorOn := True;
     RotateButton.IndicatorOn := False;
     end;

     mmRotate:
     begin
     TranslateButton.IndicatorOn := False;
     RotateButton.IndicatorOn := True;

     end;
end; //case

end;




end.
