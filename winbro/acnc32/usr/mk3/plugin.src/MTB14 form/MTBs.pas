unit MTBs;
 {
   The purpose of this form is help with the selection of Commissioning processes required when testing
   the machines. It uses MTB14 from the ACNC IO layer and allows certain operations to be completed while
   the machine is not is a finished state. Every time the ACNC is powered up then the MTB14 word is set to
   zero and this form will be required to be activated until the machine is in a state of completeness.
   It is a DLL that will be required to be installed into the ACNC using the plugin interfaces and should be
   placed in the Library folder of the profile for the ACNC, the CNC.INI file will need to be altered to allow
   the plugin to be installed.

 04/11/2014 - Initial creation (DCG)

 }
interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, Buttons, AbstractFormPlugin, IFormInterface, NamedPlugin,
  ComCtrls, INamedInterface, PluginInterface, ImgList,  CNCTypes,  IToolLife,  IniFiles;


type
  TMTB14Form =  class(TInstallForm)
    Panel1: TPanel;
    chk14b0: TCheckBox;
    chk14b1: TCheckBox;
    chk14b2: TCheckBox;
    chk14b3: TCheckBox;
    chk14b4: TCheckBox;
    chk14b5: TCheckBox;
    chk14b6: TCheckBox;
    chk14b7: TCheckBox;
    chk14b8: TCheckBox;
    chk14b9: TCheckBox;
    chk14b10: TCheckBox;
    chk14b11: TCheckBox;
    chk14b12: TCheckBox;
    chk14b13: TCheckBox;
    chk14b14: TCheckBox;
    txtAnswer: TEdit;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    btnActivate: TButton;
    btnTest: TButton;
    //btnCancel: TButton;
    chk14b15: TCheckBox;
    chk14b16: TCheckBox;
    chk14b17: TCheckBox;
    chk14b18: TCheckBox;
    procedure btnTestClick(Sender: TObject);
    procedure btnActivateClick(Sender: TObject);

  private
    { Private declarations }
    btnCancel: TButton;
    MTBActValue : TTagRef;
    MTB14FormActive : TTagref;
    MacroDrives : TTagRef;
    MacroDriveUsed : Boolean;
    MTBValue : Integer;
    MTB14Value : Integer;
    MTBChecked : array [0..18] of TCheckBox;
    procedure DecodeMTB (value :integer);
    procedure btnCancelClick(Sender: TObject);
  public
    { Public declarations }

    constructor Create(AOwner : TComponent); override;
    destructor  Destroy; override;
    procedure   Install; override;
    procedure   FinalInstall; override;
    procedure   Execute; override;    // Called for Dialogue Plugin ....Not called for Panel Plugin
    procedure   Shutdown; override;
    procedure   CloseForm; override;
    procedure   DisplaySweep; override; // called every display sweep
    procedure   TagChanged(Tag: TTagRef);
  end;

var
  MTB14Form: TMTB14Form;
  procedure TagChangeHandler(Tag : TTagRef);stdcall;

implementation

{$R *.dfm}
procedure TagChangeHandler(Tag : TTagRef);stdcall;
begin
  MTB14Form.TagChanged(Tag);

end;

constructor TMTB14Form.Create(AOwner: TComponent);
begin
  inherited Create(application);
  MTB14Form := self;
  //The following code and "the with" statement are associated with the
  //creation of a button at form creation instead of design time.
  btnCancel := Tbutton.Create(nil);
  with btnCancel do
  begin
    Parent := Panel1;   //Indicates which element it will be placed on
    //Button position properties
    Top := btnActivate.Top;
    Left := btnActivate.left + btnActivate.width + 10;
    Width := btnActivate.width;
    Height := btnActivate.height;
    //Button font information
    font.Height := btnActivate.font.Height;
    font.Style := btnActivate.font.style;
    font.Name := btnActivate.Font.Name;
    Caption := 'Cancel';
    //Event designations
    OnClick := btnCancelClick;
  end;
end;

procedure TMTB14Form.CloseForm;
begin
  inherited;
  //MTBValue := strToInt(txtAnswer.Text);
  Named.SetAsInteger(MTBActValue,MTB14Value);
  Named.SetAsInteger(MTB14FormActive,0);
  self.Close;
end;

procedure TMTB14Form.Shutdown;
begin

  inherited;

end;

procedure TMTB14Form.DisplaySweep;
begin
  inherited;

end;
// Assign MTB 14 from the ACNC IO layer to be used in this form.
procedure TMTB14Form.FinalInstall;
begin
  inherited;
  MTBActValue := Named.AquireTag('MTB14','TIntegerTag', TagChangeHandler);
  MacroDrives := Named.AquireTag('_OptionMACRODrives','TIntegerTag', TagChangeHandler);
end;

procedure TMTB14Form.Install;
begin
  inherited;
    MTB14FormActive := Named.AddTag('MTB14FormActive','TIntegerTag'); //, TagChangeHandler);
end;

procedure TMTB14Form.TagChanged(Tag: TTagRef);
begin
 MTBValue := Named.GetAsInteger(MTBActValue);
 //txtAnswer.Text := intToStr(MTBValue);
end;

destructor TMTB14Form.Destroy;
begin
  if assigned(btnCancel) then btnCancel.Free;
  inherited;
end;

// on initial execution of the form the check boxes that have been created on the form by the
// RAD are assigned to an array to help with the manipulations of the checked items.
procedure TMTB14Form.Execute;

begin
  inherited;
  //Assign an array to all the check boxes on the form.
  MTBChecked[0] := chk14b0;
  MTBChecked[1] := chk14b1;
  MTBChecked[2] := chk14b2;
  MTBChecked[3] := chk14b3;
  MTBChecked[4] := chk14b4;
  MTBChecked[5] := chk14b5;
  MTBChecked[6] := chk14b6;
  MTBChecked[7] := chk14b7;
  MTBChecked[8] := chk14b8;
  MTBChecked[9] := chk14b9;
  MTBChecked[10] := chk14b10;
  MTBChecked[11] := chk14b11;
  MTBChecked[12] := chk14b12;
  MTBChecked[13] := chk14b13;
  MTBChecked[14] := chk14b14;
  MTBChecked[15] := chk14b15;
  MTBChecked[16] := chk14b16;
  MTBChecked[17] := chk14b17;
  MTBChecked[18] := chk14b18;
  // Set the check boxes for the MTB value from the ACNC IO layer.
  if MTBValue <> 0 then  DecodeMTB(MTBValue);

  //Unsed check boxes are disabled.
  MTBChecked[1].Enabled := false;
  MTBChecked[13].Enabled := false;
  MTBChecked[15].Enabled := false;
  MTBChecked[16].Enabled := false;
  MTBChecked[17].Enabled := false;
  MTBChecked[18].Enabled := false;

   //When the code initially runs the value that the variables have are taken
  //into the DLL.
  //MTBValue := Named.GetAsInteger(MTBActValue);
  MacroDriveUsed := Named.GetAsBoolean(MacroDrives);
  Named.SetAsBoolean(MTB14FormActive,True);

  //If the macro digital drives are not used then disable the check boxes an option in
  //ACNC32Option file requires to be set if MACRO drives are used.
  if MacroDriveUsed then
  begin
    MTBChecked[7].Enabled := True;
    MTBChecked[8].Enabled := True;
    MTBChecked[9].Enabled := True;
  end else begin
    MTBChecked[7].Enabled := False;
    MTBChecked[8].Enabled := False;
    MTBChecked[9].Enabled := False;
    MTBChecked[7].Checked := False;
    MTBChecked[8].Checked := False;
    MTBChecked[9].Checked := False;
  end;
  Self.Show;
end;

//Button to activate the value of MTB14 is pressed the value of the
//ticked elements are sent through to the ACNC32 named layer.
procedure TMTB14Form.btnActivateClick(Sender: TObject);
var I: integer;
    Test: Integer;
begin
  MTB14Value := 0;
  for I:=0 to 18 do begin
    Test:= 1 shl I;
    if (MTBChecked[I].Checked ) then MTB14Value := MTB14Value + test;
  end;
  //self.EncodeMTB;
  self.CloseForm;
end;

// Decodes the Integer value of the MTB14 variable into the binary and set the checked variable
// of the check boxes used to determine the commisioning bit requirements.
procedure TMTB14Form.DecodeMTB(value: integer);
var I: integer;
    Test: Integer;
begin
   for I:=0 to 18 do begin
     Test:= 1 shl I;
     if (test and value <> 0 ) then begin
      MTBChecked[I].Checked := true;
     end else begin
      MTBChecked[I].Checked := false;
     end
   end;
end;

//!!! Test purposes only !!!
procedure TMTB14Form.btnTestClick(Sender: TObject);
//var I: integer;
//    Test: Integer;

begin
{  MTB14Value := 0;
   for I:=0 to 18 do begin
     Test:= 1 shl I;
     if (MTBChecked[I].Checked ) then MTB14Value := MTB14Value + test;
   end;
   txtAnswer.Text := intToStr(MTB14Value);
}
end;
//The Cancel button for the form has been pressed no writing to the #
//MTB14 value takes place
procedure TMTB14Form.btnCancelClick(Sender: TObject);
begin
  Named.SetAsInteger(MTB14FormActive,0);
  self.Close;
end;


initialization
  TAbstractFormPlugin.AddPlugin('CommissionForm',TMTB14Form );

end.
