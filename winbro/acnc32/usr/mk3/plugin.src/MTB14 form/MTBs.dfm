object MTB14Form: TMTB14Form
  Left = 0
  Top = 0
  BorderIcons = []
  BorderStyle = bsSingle
  Caption = 'MTB14 - MACHINE COMMISIONING SETTINGS'
  ClientHeight = 475
  ClientWidth = 770
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poMainFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 16
    Top = 16
    Width = 737
    Height = 441
    TabOrder = 0
    object Label2: TLabel
      Left = 400
      Top = 128
      Width = 141
      Height = 25
      Caption = 'Clamp Settings'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -21
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label3: TLabel
      Left = 400
      Top = 16
      Width = 236
      Height = 25
      Caption = 'MACRO Machine Settings'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -21
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label4: TLabel
      Left = 8
      Top = 16
      Width = 238
      Height = 25
      Caption = 'General Machine Settings'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -21
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object chk14b0: TCheckBox
      Left = 8
      Top = 45
      Width = 369
      Height = 17
      Caption = 'HV Board Error Message Disable'
      TabOrder = 0
    end
    object chk14b1: TCheckBox
      Left = 8
      Top = 285
      Width = 369
      Height = 17
      Caption = 'Not Used'
      TabOrder = 1
    end
    object chk14b2: TCheckBox
      Left = 400
      Top = 165
      Width = 325
      Height = 17
      Caption = 'Forces Re-feed Clamp on With Maintenance switch'
      TabOrder = 2
    end
    object chk14b3: TCheckBox
      Left = 400
      Top = 195
      Width = 325
      Height = 17
      Caption = 'Stops Re-Feed and EDM clamp to be on'
      TabOrder = 3
    end
    object chk14b4: TCheckBox
      Left = 8
      Top = 135
      Width = 369
      Height = 17
      Caption = 'Generator Self Test Running'
      TabOrder = 4
    end
    object chk14b5: TCheckBox
      Left = 8
      Top = 165
      Width = 369
      Height = 17
      Caption = 'RR Machines Only - Tool Form Active.'
      TabOrder = 5
    end
    object chk14b6: TCheckBox
      Left = 8
      Top = 225
      Width = 369
      Height = 17
      Caption = 'Part Type Selected  from OPP header active'
      TabOrder = 6
    end
    object chk14b7: TCheckBox
      Left = 398
      Top = 45
      Width = 325
      Height = 17
      Caption = 'Disable MACRO Ring Error'
      TabOrder = 7
    end
    object chk14b8: TCheckBox
      Left = 398
      Top = 75
      Width = 325
      Height = 17
      Caption = 'Disable Chiller Errors '
      TabOrder = 8
    end
    object chk14b9: TCheckBox
      Left = 398
      Top = 105
      Width = 325
      Height = 17
      Caption = 'AB Axis Chiller Errors OFF for Lasering'
      TabOrder = 9
    end
    object chk14b10: TCheckBox
      Left = 400
      Top = 225
      Width = 325
      Height = 17
      Caption = 'Tool Clamp error 9013 can be reset if the tool is not clamped'
      TabOrder = 10
    end
    object chk14b11: TCheckBox
      Left = 8
      Top = 195
      Width = 369
      Height = 17
      Caption = 'Test High pressure tools'
      TabOrder = 11
    end
    object chk14b12: TCheckBox
      Left = 8
      Top = 75
      Width = 369
      Height = 17
      Caption = 'Air Conditioners ESTOP Error Disable'
      TabOrder = 12
    end
    object chk14b13: TCheckBox
      Left = 8
      Top = 255
      Width = 369
      Height = 17
      Caption = 'Not Used'
      TabOrder = 13
    end
    object chk14b14: TCheckBox
      Left = 8
      Top = 105
      Width = 369
      Height = 17
      Caption = 'Buzzer ON for Setting Tool Changer and Current Feedback'
      TabOrder = 14
    end
    object btnActivate: TButton
      Left = 464
      Top = 352
      Width = 121
      Height = 57
      Caption = 'Activate Settings'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 15
      WordWrap = True
      OnClick = btnActivateClick
    end
    object chk14b15: TCheckBox
      Left = 8
      Top = 312
      Width = 97
      Height = 17
      Caption = 'Not Used'
      TabOrder = 16
    end
    object chk14b16: TCheckBox
      Left = 8
      Top = 344
      Width = 97
      Height = 17
      Caption = 'Not Used'
      TabOrder = 17
    end
    object chk14b17: TCheckBox
      Left = 8
      Top = 376
      Width = 97
      Height = 17
      Caption = 'Not Used'
      TabOrder = 18
    end
    object chk14b18: TCheckBox
      Left = 8
      Top = 408
      Width = 97
      Height = 17
      Caption = 'Not Used'
      TabOrder = 19
    end
  end
  object txtAnswer: TEdit
    Left = 552
    Top = 424
    Width = 113
    Height = 25
    AutoSize = False
    Enabled = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    ReadOnly = True
    TabOrder = 1
    Visible = False
  end
  object btnTest: TButton
    Left = 448
    Top = 424
    Width = 75
    Height = 25
    Caption = 'Test Button'
    Enabled = False
    TabOrder = 2
    Visible = False
    OnClick = btnTestClick
  end
end
