unit G3DObjects;
{
CASE1 UNLINKED OBJECTS
Datum Position is design time position for object.
AdjustRotation and translation are Runtime offsets to this.
Reference Point and Datum Point Coincide when object is at Datum Position.
Position of Object at design time is defined by its datum point Locationand Datum Rotation



CASE 2
lINKED OBJECTS.
For objects which have a parent Object..........
DatumPOsition is the relative position of the objects datum to the parents Datum


When an object is moved at run-time its Datum Position does NOT change but its
reference position does.

All objects can therfore bew returned to datum easily.

}

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, GMath3D,G3DDefs, StdCtrls,G3DPaper,Typinfo,comctrls;

//interface
type
TDescriptionMode = (dmVerbose,dmFileSave);
TDisplayModes = (dmFullDisplay,dmReferenceOnly,dmReportPointOnly,dmVirtual);
TRPointType = (rpNormal,rpFirstVector,rpSecondVector,rpPartDatum,rpToolDatum,rpElectrodeDatum,rpNDPDatum,rpHolePoint,rpNomPartDatum);
TVectorOwner = (voNone,voNominal,voActual,voRuntime,voApproach,voInvApproach,voNDP,InvvoNDP);
  TG3DObject = class (TComponent)

  private
  procedure CalculateRefScreenpoints(Surface :TG3DPaper);
    procedure SetPointDisplayOnly(const Value: Boolean);

  protected
  Verteces         : array of TPlocation;
  ZDatumPOints     : array[0..1] of TPlocation;
  NumberVerteces   : Integer;
  FObjectType      : T3DObjects;
  FCreateUnits     : TUnits;
  FReferencePoint  : TPLocation;
  FShowRef         : Boolean;
  FObjectValid     : Boolean;
  FColour          : TColor;
  FRefColour       : TColor;
  FSelectColour    : TColor;
  FSelectWidth     : Integer;
  FSelected        : Boolean;
  FPointDisplayOnly: Boolean;
  FHasAParent       : Boolean;
  FHasChild        : Boolean;
  FIsFrozen         : Boolean;
  FMovePermission   : TMovePermission;
  FVirtualOnly      : Boolean;

  FMoved           : TNotifyEvent;
  ZeroRot          : TRotation;
  RotInc           : Double;
  TransInc         : Double;

  BaseTranslation : TPLocation;
  BaseRotation : TRotation;

  procedure SetReferencePOint(Position : TPLocation); virtual;
  procedure SetShowRef(show : Boolean);
  procedure SetVirtualOnly(Value : Boolean);
  procedure CreateRealPoints; virtual; abstract;

  procedure SetDatumRotation(Rotation : TRotation); virtual;
  procedure CreateDatumPoints(NoRotate: Boolean); virtual;
  procedure SetCOR(Position : TPLocation);
  procedure SetSelected(Nvalue : Boolean);

  procedure SetMoved(Value: TNotifyEvent);

  procedure RotatePointAboutReference(var Point :TPLocation;PosData : TPositionData);
  procedure PRotatePoint(var Point:TPLocation; Rotation : TRotation; COR :TPLocation);
  procedure CalculateScreenpoints(Surface :TG3DPaper);virtual;
  function RotationIsZero(Value : TRotation):Boolean;

  ///


  public
  RefScreenPoint   : TScreenLocation;
  ScreenPoint      : array of TScreenLocation;
  Surface : TCanvas;
  FPositionData    : TPositionData;
  constructor create(AOwner : Tcomponent); override;
  destructor destroy; override;
  procedure Update;
  procedure CreateAbsoluteDatumPoints;virtual;
  procedure DrawOn(Surface : TG3DPaper;DisplayMode : TDisplayModes); dynamic;
  procedure SetCORtoDatum; dynamic;
  function  CalculateExtents(Surface :TG3DPaper):TExtents;virtual;
  property VirtualOnly : Boolean read FVirtualOnly write SetVirtualOnly;
  property Moved : TNotifyEvent  read FMoved write SetMoved;

  property ObjectType  : T3DObjects read FObjectType;
  property ReferencePoint : TPLocation read FReferencePoint write SetReferencePOint;
  property DisplayReferencePoint : Boolean read FShowRef write SetShowRef;

  property CreateUnits : TUnits read FCreateUnits write FCreateUnits;
  property DatumRotation : TRotation read FPositionData.DesignTimeRotation write SetDatumRotation;

  property AbsoluteRotation : TRotation read  FPositionData.AbsoluteRotation;
  property AbsoluteTranslation : TPLocation read  FPositionData.AbsoluteTranslation;
  property COR      : TPLocation read FPositionData.COR write SetCOR;

  property Selected : Boolean read FSelected write SetSelected;
  property PointDisplayOnly : Boolean read FPointDisplayOnly write SetPointDisplayOnly;
  property Colour   : TColor read FColour write FColour;
  property SelectColour :TColor read FSelectColour write FSelectColour;
  property SelectWidth : Integer read FselectWidth write FSelectWidth;
  property HasAParent : Boolean  read FHasAParent write FHasAParent;
  property HasChild : Boolean  read FHasChild write FHasChild;
  property IsFrozen : Boolean  read FIsFrozen write FIsFrozen;
  property ObjectValid : Boolean read FObjectValid;
  property ParentObjectPtr : Pointer Read FPositionData.OParent write FPositionData.OParent;


  procedure ReportMoved(Sender: TObject); dynamic;
  procedure SetMovePermission(Permission : TMovePermission;x,y,z,a,b,c : Boolean);
  procedure MakeDescription(var Description: TStringList;Mode : TDescriptionMode); dynamic;

  procedure RotateAllPointsInXAbout(LCOR : TPLocation;Rotation : Double;DesignMode : TDesignModes; var RInc : Double);virtual;
  procedure RotateAllPointsInYAbout(LCOR : TPLocation;Rotation : Double;DesignMode : TDesignModes; var RInc : Double);virtual;
  procedure RotateAllPointsInZAbout(LCOR : TPLocation;Rotation : Double;DesignMode : TDesignModes; var RInc : Double);virtual;

  procedure TranslateAllPointsInX(Translation : Double;DesignMode : TDesignModes;var TInc : Double);virtual;
  procedure TranslateAllPointsInY(Translation : Double;DesignMode : TDesignModes;var TInc : Double);virtual;
  procedure TranslateAllPointsInZ(Translation : Double;DesignMode : TDesignModes;var TInc : Double);virtual;

  end;


implementation


constructor TG3DObject.Create(AOwner : TComponent);
begin
inherited Create(AOwner);
FVirtualOnly := True;
FSelectColour := clRed;
FSelectWidth := 1;
FObjectType := t3oAbstract;
FCreateUnits := uMetric;
with FPositionData do
     begin
     DesignTimeTranslation := SetPrecisePoint3D(0,0,0);
     DatumRotation := SetPRotation(0,0,0);
     COR := DesignTimeTranslation;
     SetMovePermission(FMovePermission,False,False,False,False,False,False);
     FIsFrozen := False;
     FHasChild := False;
     end;
Colour := clBlack;
Surface := nil;
FShowRef := False;
FRefColour := clRed;
FPointDisplayOnly := False;
FSelected := False;
end;

destructor TG3DObject.Destroy;
begin
inherited Destroy;
end;

procedure TG3DObject.CreateAbsoluteDatumPoints;
begin
FReferencePoint := SetPrecisePoint3D(0,0,0);
CreateDatumPoints(True);// create with no Rotation or Translation;
end;


procedure TG3DObject.CreateDatumPoints(NoRotate : Boolean);
var
ParentObject      : TG3DObject;
begin
// if Master is self then the base rotation and Translation are those of the parent
// or if no Parent then they are zero
// If Mater is no this object then the Base Rotataion and Translation are the
// Absolute rotation and translation of the master.
   begin
   if HasAParent then
      begin
      ParentObject := FPositionData.OParent;
      BaseTranslation := AddLocation(ParentObject.ReferencePoint,FPositionData.DesignTimeTranslation);
      BaseRotation := AddRotation(ParentObject.AbsoluteRotation,FPositionData.DesignTimeRotation);
      end
   else
      begin
      BaseTranslation := FPositionData.DesignTimeTranslation;
      BaseRotation := FpositionData.DesignTimeRotation;
      end;
   end;
FReferencePoint := BaseTranslation;
FPositionData.AbsoluteRotation := BaseRotation;
end;

procedure TG3DObject.SetReferencePOint(Position : TPLocation);
begin
FPositionData.DesignTimeTranslation := Position;
FPositionData.COR := Position;
FReferencePOint := Position;
FObjectValid := false;
end;


procedure TG3DObject.SetShowRef(show : Boolean);
begin
FShowRef := show;
end;

procedure TG3DObject.SetDatumRotation(Rotation : TRotation);
begin
FPositionData.DesignTimeRotation := Rotation;
FObjectValid := false;
end;


procedure TG3DObject.SetCOR(Position : TPLocation);
begin
FPositionData.COR := Position;
end;


procedure TG3DObject.SetSelected(Nvalue : Boolean);
begin
FSelected := NValue;
end;


procedure TG3DObject.RotatePointAboutReference(var Point :TPLocation;PosData : TPositionData);
begin
Point := PRotateAboutZ3D(Point,FreferencePoint,PosData.AbsoluteRotation.z);
Point := PRotateAboutX3D(Point,FreferencePoint,PosData.AbsoluteRotation.x);
Point := PRotateAboutY3D(Point,FreferencePoint,PosData.AbsoluteRotation.y);
end;


procedure TG3DObject.PRotatePoint(var Point:TPLocation; Rotation : TRotation;COR :TPLocation);
begin
Point := PRotateAboutZ3D(Point,COR,Rotation.z);
Point := PRotateAboutX3D(Point,COR,Rotation.x);
Point := PRotateAboutY3D(Point,COR,Rotation.y);
end;


procedure TG3DObject.CalculateRefScreenPoints(Surface : TG3DPaper);
var
DRefP         : TPlocation;
begin
// First Account for World Rotation;
DRefP  := FReferencePoint;
//DRefP := AddScreenLocation(DRefP,Surface.Translation);

PRotatePoint(DRefP,Surface.Rotation,Surface.COR);
RefScreenPoint:= PointToScreen(DRefP,Surface);
end;

procedure TG3DObject.Update;
begin
if not FVirtualOnly then CreateDatumPoints(False);
FObjectValid := True;
end;

procedure TG3DObject.DrawOn(Surface : TG3DPaper;DisplayMode : TDisplayModes);
var
WidthBuffer : Integer;
begin
if not assigned(Surface) then exit;
// In the Base class only the screen refurence point id defined and drawn
// the physical embodiment of ancesters is calculated and drawn ion the ancester code.
if FVirtualOnly or (DisplayMode = dmVirtual)then exit;
CalculateRefScreenPoints(Surface);

if FShowRef or (DisplayMode = dmReferenceOnly) then
   begin
   with Surface.Offscreen.Canvas do
        begin
        Pen.Color := FRefColour;
        WidthBuffer := Pen.Width;
        if PointDisplayOnly then
          begin
          if FSelected then
            begin
            Pen.color := clRed;
            Pen.Width := 4;
            end
          else
            begin
            Pen.Color := clNavy;
            Pen.Width := 1;
            end
          end
        else
          Pen.Width := 1;
        MoveTo(RefScreenPoint.x,RefScreenPoint.y);
        LineTo(RefScreenPoint.x,RefScreenPoint.y);
        Pen.Width := WidthBuffer;
        end;
   end;
with Surface.Offscreen.Canvas do
   begin
   If FSelected then
      begin
      Pen.Color := FSelectColour;
      end
   else
       begin
       Pen.Color := FColour;
       end;
   end;
end;



procedure TG3DObject.SetMoved(Value: TNotifyEvent);
begin
FMoved := Value;
end;


procedure TG3DObject.ReportMoved(Sender: TObject);
begin
if assigned(FMoved) then FMoved(Sender);
end;

procedure TG3DObject.SetMovePermission(Permission : TMovePermission;x,y,z,a,b,c : Boolean);
begin
Permission.x := x;
Permission.y := y;
Permission.z := z;
Permission.a := a;
Permission.b := b;
Permission.c := c;
end;

procedure TG3DObject.SetCORtoDatum;
begin
FPositionData.COR := FReferencePoint;
end;


procedure TG3DObject.MakeDescription(var Description: TStringList;Mode : TDescriptionMode);
function FormatPos(Pos :TPLocation):String;
begin
Result := '';
Result := Format('%6.3f,%6.3f,%6.3f',[Pos.x,Pos.y,Pos.z]);
end;
function FormatRot(Pos :TRotation):String;
begin
Result := '';
Result := Format('%f,%f,%f',[Pos.x,Pos.y,Pos.z]);
end;
var
SList : TStringList;
begin
SList := TStringList.Create;
try
SList.Add(Format('[%s]',[Name]));
case FObjectType of
     t3oLine :
     begin
     SList.Add('ObjectType=Line');
     end;
     t3oBox  :
     begin
     SList.Add('ObjectType=Box');
     end;
     t3oDisc :
     begin
     SList.Add('ObjectType=Disc');
     end;
     t3oReportPoint :
     begin
     SList.Add('ObjectType=ReportPoint');
     end;
end; // case
SList.Add(Format('ReferencePoint=%s',[FormatPos(FReferencePOint)]));
SList.Add(Format('DesignTimeTranslation=%s',[FormatPos(FPositionData.DesignTimeTranslation)]));
SList.Add(Format('DesignTimeRotation=%s',[FormatRot(FPositionData.DesignTimeRotation)]));
Slist.Add(Format('FColour=,%d',[Ord(FColour)]));
Slist.Add(Format('FRefColour=,%d',[Ord(FRefColour)]));
Slist.Add(Format('FSelectColour=,%d',[Ord(FSelectColour)]));

if Mode = dmVerbose then
        begin
        SList.Add(Format('AbsoluteRotation=%s',[FormatRot(FPositionData.AbsoluteRotation)]));
        SList.Add(Format('COR=%s',[FormatPos(FPositionData.COR)]));

{        Slist.Add(Format('RelTransLimits=,%f,%f,%f,%f.%f.%f',[FPositionData.RelTransLimits.xPlus,
                                                      FPositionData.RelTransLimits.xMinus,
                                                      FPositionData.RelTransLimits.yPlus,
                                                      FPositionData.RelTransLimits.yMinus,
                                                      FPositionData.RelTransLimits.zPLus,
                                                      FPositionData.RelTransLimits.zMinus]));

        Slist.Add(Format('RelRotateLimits=,%f,%f,%f,%f.%f.%f',[FPositionData.RelRotateLimits.xPlus,
                                                      FPositionData.RelRotateLimits.xMinus,
                                                      FPositionData.RelRotateLimits.yPlus,
                                                      FPositionData.RelRotateLimits.yMinus,
                                                      FPositionData.RelRotateLimits.zPLus,
                                                      FPositionData.RelRotateLimits.zMinus]));}
        end;



except
SList.Free;
Description := nil;
end; // try finally
Description := SList;
end;

function TG3DObject.RotationIsZero(Value : TRotation):Boolean;
begin
Result := (Value.x=0) and (Value.y=0) and (Value.z=0);
end;

procedure TG3DObject.TranslateAllPointsInX(Translation : Double;DesignMode : TDesignModes;var TInc : Double);
var
Count : Integer;

begin
if FIsFrozen then exit;

case DesignMode of
   dmEditInc:
   begin
   FReferencePoint.x := FReferencePoint.x+Translation;
   with FPositionData do
        begin
        DesignTimeTranslation.x := DesignTimeTranslation.x+Translation;
        TransInc := Translation;
        end;
   end;
   dmEditAbs:
   begin
   with FPositionData do
        begin
        DesignTimeTranslation.x := Translation;
        TransInc := Translation-FReferencePoint.x;
        end;
   FReferencePoint.x := Translation;
   end;

   dmRuntimeInc:
   begin
   FReferencePoint.x := FReferencePoint.x+Translation;
   TransInc := Translation;
   end;

   dmRuntimeAbs:
   begin
   TransInc := Translation-FReferencePoint.x;
   FReferencePoint.x := Translation;
   end;
end; //case
Tinc := TransInc;
For Count := 0 to NumberVerteces-1 do
    begin
    Verteces[Count].x := Verteces[Count].x+TransInc;
    end;

end;

procedure TG3DObject.TranslateAllPointsInY(Translation : Double;DesignMode : TDesignModes;var TInc : Double);
var
Count : Integer;
begin
if FIsFrozen then exit;

case DesignMode of
   dmEditInc:
   begin
   FReferencePoint.y := FReferencePoint.y+Translation;
   with FPositionData do
        begin
        DesignTimeTranslation.y := DesignTimeTranslation.y+Translation;
        TransInc := Translation;
        end;
   end;
   dmEditAbs:
   begin
   with FPositionData do
        begin
        DesignTimeTranslation.y := Translation;
        TransInc := Translation-FReferencePoint.y;
        end;
   FReferencePoint.y := Translation;
   end;

   dmRuntimeInc:
   begin
   FReferencePoint.y := FReferencePoint.y+Translation;
   TransInc := Translation;
   end;

   dmRuntimeAbs:
   begin
   TransInc := Translation-FReferencePoint.y;
   FReferencePoint.y := Translation;
   end;
end; //case
Tinc := TransInc;
For Count := 0 to NumberVerteces-1 do
    begin
    Verteces[Count].y := Verteces[Count].y+TransInc;
    end;
end;

procedure TG3DObject.TranslateAllPointsInZ(Translation : Double;DesignMode : TDesignModes;var TInc : Double);
var
Count : Integer;
begin
if FIsFrozen then exit;

case DesignMode of
   dmEditInc:
   begin
   FReferencePoint.z := FReferencePoint.z+Translation;
   with FPositionData do
        begin
        DesignTimeTranslation.z := DesignTimeTranslation.z+Translation;
        TransInc := Translation;
        end;
   end;
   dmEditAbs:
   begin
   with FPositionData do
        begin
        DesignTimeTranslation.z := Translation;
        TransInc := Translation-FReferencePoint.z;
        end;
   FReferencePoint.z := Translation;
   end;

   dmRuntimeInc:
   begin
   FReferencePoint.z := FReferencePoint.z+Translation;
   TransInc := Translation;
   end;

   dmRuntimeAbs:
   begin
   TransInc := Translation-FReferencePoint.z;
   FReferencePoint.z := Translation;
   end;
end; //case
Tinc := TransInc;
For Count := 0 to NumberVerteces-1 do
    begin
    Verteces[Count].z := Verteces[Count].z+TransInc;
    end;

end;


procedure TG3DObject.RotateAllPointsInXAbout(LCOR : TPLocation;Rotation : Double;DesignMode : TDesignModes; var RInc : Double);
var
Count : Integer;
begin
if FIsFrozen then exit;
case DesignMode of
   dmEditInc:
   begin
   with FPositionData do
        begin
        RotInc := Rotation;
        DesignTimeRotation.x := DesignTimeRotation.x+Rotation;
        AbsoluteRotation.x := AbsoluteRotation.x+Rotation;
        FReferencePoint :=  PRotateAboutX3D(FReferencePoint,LCOR,Rotinc);
        end;
   end;
   dmEditAbs:
   begin
   with FPositionData do
        begin
        RotInc := Rotation-AbsoluteRotation.x;
        DesignTimeRotation.x := DesignTimeRotation.x+RotInc;
        AbsoluteRotation.x := DesignTimeRotation.x;
        FReferencePoint :=  PRotateAboutX3D(FReferencePoint,LCOR,Rotinc);
        end;
   end;
   dmRuntimeInc:
   begin
   RotInc := Rotation;
   FReferencePoint :=  PRotateAboutX3D(FReferencePoint,LCOR,RotInc);
   FPositionData.AbsoluteRotation.x := AbsoluteRotation.x+Rotation;
   end;
   dmRuntimeAbs:
   begin
   with FPositionData do
        begin
        RotInc := Rotation-AbsoluteRotation.x;
        FReferencePoint :=  PRotateAboutX3D(FReferencePoint,LCOR,RotInc);
        FPositionData.AbsoluteRotation.x := RotInc; ///!!!!!
        end;
   end;

end;
if Rotinc <> 0 then
   begin
   For Count := 0 to NumberVerteces-1 do
       begin
       Verteces[Count] := PRotateAboutX3D(Verteces[Count],LCOR,Rotinc);
       end;
   end;
Rinc := RotInc;
end;

procedure TG3DObject.RotateAllPointsInYAbout(LCOR : TPLocation;Rotation : Double;DesignMode : TDesignModes; var RInc : Double);
var
Count : Integer;
begin
if FIsFrozen then exit;

case DesignMode of
   dmEditInc:
   begin
   with FPositionData do
        begin
        RotInc := Rotation;
        DesignTimeRotation.y := DesignTimeRotation.y+Rotation;
        AbsoluteRotation.y := DesignTimeRotation.y;
        FReferencePoint :=  PRotateAboutY3D(FReferencePoint,LCOR,Rotinc);
        end;
   end;
   dmEditAbs:
   begin
   with FPositionData do
        begin
        RotInc := Rotation-AbsoluteRotation.y;
        DesignTimeRotation.y := DesignTimeRotation.y+RotInc;
        AbsoluteRotation.y := DesignTimeRotation.y;
        FReferencePoint :=  PRotateAboutY3D(FReferencePoint,LCOR,Rotinc);
        end;
   end;
   dmRuntimeInc:
   begin
   RotInc := Rotation;
   FReferencePoint :=  PRotateAboutY3D(FReferencePoint,LCOR,RotInc);
   FPositionData.AbsoluteRotation.y := AbsoluteRotation.y+Rotation;
   end;
   dmRuntimeAbs:
   begin
   with FPositionData do
        begin
        RotInc := Rotation-AbsoluteRotation.y;
        FReferencePoint :=  PRotateAboutY3D(FReferencePoint,LCOR,RotInc);
        FPositionData.AbsoluteRotation.y := RotInc; ///!!!!!
        end;
   end;

end;
For Count := 0 to NumberVerteces-1 do
    begin
    Verteces[Count] := PRotateAboutY3D(Verteces[Count],LCOR,Rotinc);
    end;
Rinc := RotInc;
end;

procedure TG3DObject.RotateAllPointsInZAbout(LCOR : TPLocation;Rotation : Double;DesignMode : TDesignModes; var RInc : Double);
var
Count : Integer;
begin
if FIsFrozen then exit;

case DesignMode of
   dmEditInc:
   begin
   with FPositionData do
        begin
        RotInc := Rotation;
        DesignTimeRotation.z := DesignTimeRotation.z+Rotation;
        AbsoluteRotation.z := DesignTimeRotation.z;
        FReferencePoint :=  PRotateAboutZ3D(FReferencePoint,LCOR,Rotinc);
        end;
   end;
   dmEditAbs:
   begin
   with FPositionData do
        begin
        RotInc := Rotation-AbsoluteRotation.z;
        DesignTimeRotation.z := DesignTimeRotation.z+RotInc;
        AbsoluteRotation.z := DesignTimeRotation.z;
        FReferencePoint :=  PRotateAboutZ3D(FReferencePoint,LCOR,Rotinc);
        end;
   end;
   dmRuntimeInc:
   begin
   RotInc := Rotation;
   FReferencePoint :=  PRotateAboutZ3D(FReferencePoint,LCOR,RotInc);
   FPositionData.AbsoluteRotation.z := AbsoluteRotation.z+Rotation;
   end;
   dmRuntimeAbs:
   begin
   with FPositionData do
        begin
        RotInc := Rotation-AbsoluteRotation.z;
        FReferencePoint :=  PRotateAboutZ3D(FReferencePoint,LCOR,RotInc);
        FPositionData.AbsoluteRotation.z := RotInc; ///!!!!!
        end;
   end;

end;
For Count := 0 to NumberVerteces-1 do
    begin
    Verteces[Count] := PRotateAboutZ3D(Verteces[Count],LCOR,Rotinc);
    end;
Rinc := RotInc;
end;



procedure TG3DObject.CalculateScreenPoints(Surface : TG3DPaper);
var
DrawPOints : array of TPlocation;
Count : Integer;
begin
if FObjectType = t3oReportPoint then
   begin
   ScreenPoint[0].x := RefScreenPoint.x+5;
   ScreenPoint[0].y := RefScreenPoint.y-5;
   ScreenPoint[1].x := RefScreenPoint.x-5;
   ScreenPoint[1].y := RefScreenPoint.y-5;
   ScreenPoint[2].x := RefScreenPoint.x;
   ScreenPoint[2].y := RefScreenPoint.y;
   end
else
    begin
    SetLength(DrawPOints,NumberVerteces);
    For Count := 0 To NumberVerteces-1 do
        begin
        DrawPOints[Count] := Verteces[Count];

        // Add rotation due to World Rotation
        PRotatePoint(DrawPOints[Count],Surface.Rotation,Surface.COR);

        ScreenPoint[Count] := PointToScreen(DrawPOints[Count],Surface);
        end;
    end;
end;

function TG3DObject.CalculateExtents(Surface : TG3DPaper):TExtents;
var
DrawPOints : array of TPlocation;
ScreenLocation :TScreenLocation;
Count : Integer;
Location       : TPlocation;
begin
Result.MaxX := -10000000;
Result.MaxY := -10000000;
Result.MinX := 10000000;
Result.MinY := 10000000;
SetLength(DrawPOints,NumberVerteces);
if NumberVerteces = 0 then
   begin
   Location := FReferencePoint;
   PRotatePoint(Location,Surface.Rotation,Surface.COR);
   ScreenLocation := PointToScreen(Location,Surface);
   Result.MaxX := ScreenLocation.x;
   Result.MaxY := ScreenLocation.y;
   Result.MinX := ScreenLocation.x;
   Result.MinY := ScreenLocation.y;
   end
else
   begin
   For Count := 0 To NumberVerteces-1 do
    begin
    DrawPOints[Count] := Verteces[Count];
    // Add Translation due to World Translation
    //    DrawPOints[Count] := AddLocation(DrawPOints[Count],Surface.Translation);
    // Add rotation due to World Rotation
   PRotatePoint(DrawPOints[Count],Surface.Rotation,Surface.COR);
   ScreenLocation := PointToScreen(DrawPOints[Count],Surface);
   if ScreenLocation.x > Result.MaxX then Result.MaxX := ScreenLocation.x;
   if ScreenLocation.y > Result.MaxY then Result.MaxY := ScreenLocation.y;
   if ScreenLocation.x < Result.MinX then Result.MinX := ScreenLocation.x;
   if ScreenLocation.y < Result.MinY then Result.MinY := ScreenLocation.y;
   end;
end;
end;

procedure TG3DObject.SetVirtualOnly(Value : Boolean);
begin
FVirtualOnly := Value;
if FVirtualOnly then
   begin
   SetLength(Verteces,0);
   end
else
    begin
    CreateRealPoints;
    end;
end;



procedure TG3DObject.SetPointDisplayOnly(const Value: Boolean);
begin
  FPointDisplayOnly := Value;
end;

end.
