unit G3DDisplay;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls,G3DPaper,GMath3D,G3DDefs;
const
PanelTopBorder = 0;
type
TShowMode =(smAll,smFront,smSide,smTop,smCustom);
  TG3DDisplay = class(TCustomPanel)
  private
    { Private declarations }
    FCustomVisible : Boolean;
    FSideVisible : Boolean;
    FFrontVisible : Boolean;
    FTopVisible : Boolean;
    FShowMode   : TShowMode;
    TopPanel : TCustomPanel;
    FrontPanel : TCustomPanel;
    SidePanel : TCustomPanel;
    CustomPanel : TCustomPanel;
    procedure SetRotataion( nValue : TRotation);
    function GetRotation:TRotation;
    procedure SetTranslation( nValue : TScreenLocation);
    function GetTranslation:TScreenLocation;
    procedure SetScale( nValue : Double);
    procedure SetShowmode( nValue : TShowmode);

    function GetScale:Double;


  protected
    { Protected declarations }
    procedure Resize; override;

  public
    { Public declarations }
    TopView : TG3DPaper;
    FrontView : TG3DPaper;
    SideView : TG3DPaper;
    CustomView : TG3DPaper;
  published
    { Published declarations }
    constructor Create(AOwner: TComponent);override;
    destructor Destroy; override;
    procedure Invalidate; override;

    Property Rotation : TRotation read GetRotation write SetRotataion;
    Property Translation :TScreenLocation read GetTranslation write SetTranslation;
    Property Scale : Double read GetScale write SetScale;
    property ShowMode : TShowMode read FshowMode write setShowmode;
    property FrontVisible : Boolean read FFrontVisible;
    property SideVisible  : Boolean read FsideVisible;
    property TopVisible  : Boolean read FTopVisible;
    property CustomVisible  : Boolean read FCustomVisible;
//    Property COR      : TPlocation read FCOR;
  end;



procedure Register;

implementation
procedure TG3DDisplay.Invalidate;
begin
if not assigned(FrontView) then exit;
FrontView.Paint;
SideView.Paint;
TopView.Paint;
CustomView.Paint;
end;

constructor TG3DDisplay.Create(AOwner: TComponent);
begin
inherited;
Caption := '';
TopPanel := TCustomPanel.Create(Self);
with TopPanel do
     begin
     parent := Self;
     end;
FrontPanel := TCustomPanel.Create(Self);
with FrontPanel do
     begin
     parent := Self;
     end;
SidePanel := TCustomPanel.Create(Self);
with SidePanel do
     begin
     parent := Self;
     end;
CustomPanel := TCustomPanel.Create(Self);
with CustomPanel do
     begin
     parent := Self;
     end;

TopView := TG3DPaper.Create(Self);
with TopView do
     begin
     parent := TopPanel;
     Align := alBottom;
     Rotation := SetPRotation(90,0,0);
     ShowFloor := True;
     FloorWidth := 2000;
     FloorLength := 2000;
     DrawFloor;

     end;

FrontView := TG3DPaper.Create(Self);
with FrontView do
     begin
     parent := FrontPanel;
     Align := alBottom;
     Rotation := SetPRotation(0,0,0);
     ShowFloor := True;
     FloorWidth := 2000;
     FloorLength := 2000;
     DrawFloor;
     end;

SideView := TG3DPaper.Create(Self);
with SideView do
     begin
     parent := SidePanel;
     Align := alBottom;
     Rotation := SetPRotation(0,0,90);
     ShowFloor := True;
     FloorWidth := 2000;
     FloorLength := 2000;
     DrawFloor;
     end;

CustomView := TG3DPaper.Create(Self);
with CustomView do
     begin
     parent := CustomPanel;
     Align := alBottom;
     Rotation := SetPRotation(45,0,45);
     ShowFloor := True;
     FloorWidth := 2000;
     FloorLength := 2000;
     DrawFloor;
     end;
Fshowmode := smAll;

Resize;

end;
destructor TG3DDisplay.Destroy;
begin
inherited;
end;

procedure TG3DDisplay.Resize;
var
PanelWidth : Integer;
PanelHeight : Integer;
begin
inherited;
if FShowMode = smAll then
    begin
    PanelHeight := (Height div 2)-5;
    PanelWidth := (Width div 2) -5;
    With TopPanel do
        begin
        Top := 2;
        Left := 2;
        Height := PanelHeight;
        Width := PanelWidth;
        end;
    if PanelHeight > PanelTopBorder then
        TopView.Height := PanelHeight-PanelTopBorder
    else
        TopView.Height := 5;

    With SidePanel do
        begin
        Top := 2;
        Left := PanelWidth+7;
        Height := PanelHeight;
        Width := PanelWidth;
        end;
    if PanelHeight > PanelTopBorder then
        SideView.Height := PanelHeight-PanelTopBorder
    else
        SideView.Height := 5;

    With FrontPanel do
        begin
        Top := PanelHeight+7;
        Left := 2;
        Height := PanelHeight;
        Width := PanelWidth;
        end;
    if PanelHeight > PanelTopBorder then
        FrontView.Height := PanelHeight-PanelTopBorder
    else
        FrontView.Height := 5;

    With CustomPanel do
        begin
        Top := PanelHeight+7;
        Left := PanelWidth+7;
        Height := PanelHeight;
        Width := PanelWidth;
        end;
    if PanelHeight > PanelTopBorder then
        CustomView.Height := PanelHeight-PanelTopBorder
    else
        CustomView.Height := 5;
    if not (Parent = nil) then
        begin
        FrontView.Paint;
        SideView.Paint;
        TopView.Paint;
        CustomView.Paint;
        end;
    FCustomVisible := True;
    FTopVisible := True;
    FSideVisible := True;
    FFrontVisible := True;
    end
else
    begin
    // here for single window display
    FCustomVisible := False;
    FTopVisible := False;
    FSideVisible := False;
    FFrontVisible := False;
    case showMode of

        smCustom:
        begin
        FCustomVisible := True;
        CustomPanel.Width := Width-4;
        CustomPanel.Height := Height-4;
        CustomPanel.top := 2;
        CustomPanel.Left :=2;
        CustomView.Height := CustomPanel.Height- PanelTopBorder;
        CustomView.Paint;
        end;

        smFront:
        begin
        FFrontVisible := True;
        FrontPanel.Width := Width;
        FrontPanel.Height := Height;
        FrontPanel.top := 2;
        FrontPanel.Left :=2;
        FrontView.Height := CustomPanel.Height- PanelTopBorder;
        FrontView.Paint;
        end;

        smSide:
        begin
        FSideVisible := True;
        SidePanel.Width := Width;
        SidePanel.Height := Height;
        SidePanel.top := 2;
        SidePanel.Left :=2;
        SideView.Height := CustomPanel.Height- PanelTopBorder;
        SideView.Paint;
        end;

        smTop:
        begin
        FTopVisible := True;
        TopPanel.Width := Width;
        TopPanel.Height := Height;
        TopPanel.top := 2;
        TopPanel.Left :=2;
        TopView.Height := CustomPanel.Height- PanelTopBorder;
        TopView.Paint;
        end;
    end; // case
    end;
FrontPanel.Visible := FFrontVisible;
SidePanel.Visible := FSideVisible;
TopPanel.Visible := FTopVisible;
CustomPanel.Visible := CustomVisible;

end;

procedure TG3DDisplay.SetShowmode( nValue : TShowmode);
begin
FShowmode := nValue;
Resize;
end;


procedure TG3DDisplay.SetRotataion( nValue : TRotation);
begin
if assigned(CustomView) then CustomView.Rotation:= Nvalue;
CustomView.Paint;
end;

function TG3DDisplay.GetRotation:TRotation;
begin
if assigned(CustomView) then Result := CustomView.Rotation
   else Result := SetPRotation(0,0,0);
CustomView.Paint;
end;

procedure TG3DDisplay.SetTranslation( nValue : TScreenLocation);
begin
if assigned(CustomView) then CustomView.Translation:= Nvalue;
CustomView.Paint;
end;

function TG3DDisplay.GetTranslation:TScreenLocation;
begin
if assigned(CustomView) then Result := CustomView.Translation
   else Result := SetScreenLocation(0,0);
CustomView.Paint;
end;

procedure TG3DDisplay.SetScale( nValue : Double);
begin
if assigned(CustomView) then CustomView.Scale:= Nvalue;
CustomView.Paint;
end;

function TG3DDisplay.GetScale:Double;
begin
if assigned(CustomView) then Result := CustomView.Scale
   else Result := 0.0;
CustomView.Paint;
end;

procedure Register;
begin
  RegisterComponents('G3D', [TG3DDisplay]);
end;


end.
