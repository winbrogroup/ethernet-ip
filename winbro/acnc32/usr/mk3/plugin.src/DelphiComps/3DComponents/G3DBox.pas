unit G3DBox;

interface
// Datum Point for a box is at the centre of the diagonals Not At the surface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, GMath3D,G3DDefs,G3DPaper, StdCtrls,G3DObjects;

type
  TG3DBox = class(TG3DObject)

  private
  FLength       : Double;
  FWidth        : Double;
  FHeight       : Double;

  procedure SetBoxLength(nvalue : Double);
  procedure SetWidth(nvalue : Double);
  procedure SetHeight(nvalue : Double);
  procedure CreateRealPoints; override;

  protected

  public
  // No rotate is set for objects   which can never rotate (such as fixed non axis objects)
  
  procedure CreateDatumPoints(NoRotate : Boolean); override;
  constructor create(AOwner : Tcomponent); override;
  procedure DrawOn(Surface : TG3DPaper;DisplayMode : TDisplayModes); override;
  procedure MakeDescription(var Description: TStringList;Mode : TDescriptionMode); override;

  published
//  property Verteces   : TBoxVerts read FVerteces ;
  property Length     : Double read FLength write SetBoxLength;
  property Width      : Double read FWidth  write SetWidth;
  property Height      : Double read FHeight  write SetHeight;
  end;

procedure Register;

implementation
constructor TG3DBox.Create(AOwner : TComponent);
begin
inherited;
FObjectType :=  t3OBox;
end;

procedure TG3DBox.DrawOn(Surface : TG3DPaper;DisplayMode : TDisplayModes);
begin
inherited;
if not assigned(Surface) then exit;
if FVirtualOnly then exit;
if DisplayMode <> dmFullDisplay then exit;
CalculateScreenPoints(Surface);
with Surface.Offscreen.Canvas do
        begin
        MoveTo(ScreenPoint[0].x,ScreenPoint[0].y);
        LineTo(ScreenPoint[1].x,ScreenPoint[1].y);
        LineTo(ScreenPoint[2].x,ScreenPoint[2].y);
        LineTo(ScreenPoint[3].x,ScreenPoint[3].y);
        LineTo(ScreenPoint[0].x,ScreenPoint[0].y);
        LineTo(ScreenPoint[4].x,ScreenPoint[4].y);
        LineTo(ScreenPoint[5].x,ScreenPoint[5].y);
        LineTo(ScreenPoint[6].x,ScreenPoint[6].y);
        LineTo(ScreenPoint[7].x,ScreenPoint[7].y);
        LineTo(ScreenPoint[4].x,ScreenPoint[4].y);
        MoveTo(ScreenPoint[1].x,ScreenPoint[1].y);
        LineTo(ScreenPoint[5].x,ScreenPoint[5].y);
        MoveTo(ScreenPoint[2].x,ScreenPoint[2].y);
        LineTo(ScreenPoint[6].x,ScreenPoint[6].y);
        MoveTo(ScreenPoint[3].x,ScreenPoint[3].y);
        LineTo(ScreenPoint[7].x,ScreenPoint[7].y);
        end;
end;

procedure TG3DBox.SetBoxLength(nValue : Double);
begin
FLength  := nValue;
end;

procedure TG3DBox.SetWidth(nValue : Double);
begin
FWidth  := nValue;
end;

procedure TG3DBox.SetHeight(nValue : Double);
begin
FHeight  := nValue;
end;


procedure TG3DBox.CreateDatumPoints(NoRotate : Boolean);
var
HalfHeight,HalfWidth,HalfLength : Double;
Count : Integer;
begin
If FVirtualOnly then exit;
if not NoRotate then inherited;
HalfWidth := Fwidth/2.0;
HalfHeight := FHeight/2.0;
HalfLength := FLength/2.0;

//First Create POints relative to x,y,z about reference point
with Verteces[0] do
     begin
     x :=  FReferencePOint.x-HalfWidth;
     y :=  FReferencePOint.y+HalfLength;
     z :=  FReferencePOint.z+HalfHeight;
     end;
with Verteces[1] do
     begin
     x :=  FReferencePOint.x-HalfWidth;
     y :=  FReferencePOint.y-HalfLength;
     z :=  FReferencePOint.z+HalfHeight;
     end;
with Verteces[2] do
     begin
     x :=  FReferencePOint.x+HalfWidth;
     y :=  FReferencePOint.y-HalfLength;
     z :=  FReferencePOint.z+HalfHeight;
     end;
with Verteces[3] do
     begin
     x :=  FReferencePOint.x+HalfWidth;
     y :=  FReferencePOint.y+HalfLength;
     z :=  FReferencePOint.z+HalfHeight;
     end;
with Verteces[4] do
     begin
     x :=  FReferencePOint.x-HalfWidth;
     y :=  FReferencePOint.y+HalfLength;
     z :=  FReferencePOint.z-HalfHeight;
     end;
with Verteces[5] do
     begin
     x :=  FReferencePOint.x-HalfWidth;
     y :=  FReferencePOint.y-HalfLength;
     z :=  FReferencePOint.z-HalfHeight;
     end;
with Verteces[6] do
     begin
     x :=  FReferencePOint.x+HalfWidth;
     y :=  FReferencePOint.y-HalfLength;
     z :=  FReferencePOint.z-HalfHeight;
     end;
with Verteces[7] do
     begin
     x :=  FReferencePOint.x+HalfWidth;
     y :=  FReferencePOint.y+HalfLength;
     z :=  FReferencePOint.z-HalfHeight;
     end;

// Now rotate all points relative to DatumRotation
if Not NoRotate then
   begin
   if (not RotationIsZero(FPositionData.AbsoluteRotation)) then
      begin
      For Count := 0 to NumberVerteces-1 do
          begin
          RotatePointAboutReference(Verteces[Count],FPOsitionData);
          end;
      end;
   end
else
    begin
    FpositionData.AbsoluteRotation := SetPRotation(0,0,0);
    end;

end;


procedure TG3DBox.MakeDescription(var Description: TStringList;Mode : TDescriptionMode);
begin
inherited   MakeDescription(Description,Mode);
Description.Add(Format('Length=%f',[FLength]));
Description.Add(Format('Width=%f',[FWidth]));
Description.Add(Format('Height=%f',[FHeight]));

end;

procedure TG3DBox.CreateRealPoints;
begin
FLength := 100.0;
NumberVerteces := 8;
SetLength(Verteces,8);
SetLength(ScreenPoint,8);
CreateDatumPoints(False);
SetCORtoDatum;
end;

procedure Register;
begin
  RegisterComponents('G3D', [TG3DBox]);
end;




end.
