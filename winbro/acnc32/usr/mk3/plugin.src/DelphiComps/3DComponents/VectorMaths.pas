unit VectorMaths;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls,commctrl,Menus,extctrls,G3DDefs,GMath3D,GReportPoint,G3Math2d;
const
VerySmall = 0.0001;
type
TReportVectorType =(rvPartRelative,rvToolRelative,rvMachineRelative,rvDrillNominal,rvToolApproach);
TAlignIndex = (alaxNone,alaxX,alaxY,alaxZ,alaxVert);

TPointRelationship = record
                   PerpLengthX : Double;
                   PerpStartX  : Double;
                   PerpLengthY : Double;
                   PerpStartY  : Double;
                   PerpLengthZ : Double;
                   PerpStartZ  : Double;
                   end;

TVectorDifference= record
         Translation : TPlocation;
         Rotation    : TRotation;
         end;

 TVectorData = record
         VectorType       : TReportVectorType;
         Point1NodeId     : HTREEITEM;
         Point2NodeId     : HTREEITEM;
         Name             : String;
         CentrePosition   : TPlocation;
         Length           : Double;
         Rotation         : TRotation;
         Point1Position   : TPlocation;
         Point2Position   : TPlocation;
         isFrozen         : Boolean;
         DontDisplay      : Boolean;
         end;

TRotationCodes = (xRed,yRed,zRed);
TReducingCode = set of TRotationCodes;
TReductionData = record
    ReducingAxes   : TReducingCode;
    ErrorChangedSign : TReducingCode;
    NumberReducing : Integer;
    Reduction      : TRotation;
    end;


TAxisPOsn = array[0..8] of Double;
function AlignmentIndex(Vector : TVectorData):TAlignIndex;
function TotalXYError(Delta : TRotation):Double;
function MaxError(Rotations : TRotation;var Index : Integer):Double;
function RotationDeltaInLimits(Delta,Limits:TRotation):Boolean;
function SameSign(Value1,Value2:Double):Boolean;
//procedure GetReducingAxes(StartRotation,EndRotation: TRotation;var ReductionData:TReductionData);
//function GetComaparativePoint(P1 : TPlocation;Vector1,Vector2 : TVectorData):TPlocation;

implementation

{Returns the location of ythe point with the same relation ship to vector 2
 as the point P1 has to Vector 1;
This is done for each of the x,y and z views in turn by finding thr perpendicular
through the point to the line of the vector and recording the length of the
perpendicular and the distance of its intersect from point 1 of the vector.
the relationship is then transfered to vector 2.}

{function GetComaparativePoint(P1 : TPlocation;Vector1,Vector2 : TVectorData):TPlocation;
var
V1P1,V1P2 : TPrecisePoint; // N.B. 2D
P2DX,P2DY,P2DZ :TPrecisePoint; // N.B. 2D
V1xFormula : TLineFormula;
V1yFormula : TLineFormula;
V1zFormula : TLineFormula;
Relationship : TPointRelationship;
Perpendicular : TLineFormula;
PerpIntersect : TPrecisePoint;
begin
//X View
V1P1.x := Vector1.Point1Position.y;
V1P1.y := Vector1.Point1Position.z;
V1P2.x := Vector1.Point2Position.y;
V1P2.y := Vector1.Point2Position.z;
p2Dx.x := p1.Y;
p2Dx.Y := p1.Z;

// Find formula of vector line in x View using y/z co-ordinates
V1xFormula := PreciseTwoPointLineEquation2D(V1P1,V1P2);

Perpendicular := PerpendicurToLineThroughPoint(V1xFormula,P2DX);
PerpIntersect := PreciseIntersect(V1xFormula,Perpendicular);
Relationship.PerpLengthX := PPreciseLineLength2D(PerpIntersect,P2DX);
Relationship.PerpStartX := PPreciseLineLength2D(V1P1,PerpIntersect);
//Y View
V1P1.x := Vector1.Point1Position.x;
V1P1.y := Vector1.Point1Position.z;
V1P2.x := Vector1.Point2Position.x;
V1P2.y := Vector1.Point2Position.z;
p2DY.x := p1.X;
p2DY.Y := p1.Z;
V1yFormula := PreciseTwoPointLineEquation2D(V1P1,V1P2);

Perpendicular := PerpendicurToLineThroughPoint(V1yFormula,P2DY);
PerpIntersect := PreciseIntersect(V1yFormula,Perpendicular);
Relationship.PerpLengthY := PPreciseLineLength2D(PerpIntersect,P2DY);
Relationship.PerpStartY := PPreciseLineLength2D(V1P1,PerpIntersect);

//Z View
V1P1.x := Vector1.Point1Position.x;
V1P1.y := Vector1.Point1Position.y;
V1P2.x := Vector1.Point2Position.x;
V1P2.y := Vector1.Point2Position.y;
p2DZ.x := p1.X;
p2DZ.Y := p1.Y;
V1zFormula := PreciseTwoPointLineEquation2D(V1P1,V1P2);

Perpendicular := PerpendicurToLineThroughPoint(V1zFormula,P2DZ);
PerpIntersect := PreciseIntersect(V1zFormula,Perpendicular);
Relationship.PerpLengthZ := PPreciseLineLength2D(PerpIntersect,P2DZ);
Relationship.PerpStartZ := PPreciseLineLength2D(V1P1,PerpIntersect);

Result.x := P2dx.x;
end;}

function TotalXYError(Delta : TRotation):Double;
begin
Result := 0.0;
Result :=Result+abs(Delta.x);
Result :=Result+abs(Delta.y);
end;

function AlignmentIndex(Vector : TVectorData):TAlignIndex;
begin
result := alaxNone;
if (abs(Vector.Point1Position.x -Vector.Point2Position.x)<VerySmall) and (abs(Vector.Point1Position.y -Vector.Point2Position.y)<VerySmall) then
  begin
  Result := alaxVert;
  end
else if (abs(Vector.Point1Position.x -Vector.Point2Position.x)<VerySmall) then
  begin
  Result := alaxx
  end
else if (abs(Vector.Point1Position.z -Vector.Point2Position.z)<VerySmall) then
  begin
  Result := alaxz
  end
else if (abs(Vector.Point1Position.y -Vector.Point2Position.y)<VerySmall)  then
  begin
  Result := alaxy
  end
end;

function MaxError(Rotations : TRotation;var Index : Integer):Double;
begin
with Rotations do
begin
if abs(x) > abs(y) then
   begin
   If abs(x) > abs(z) then
      begin
      Result := x;
      Index := 0;
      end
   else
      begin
      Result := z;
      Index := 2;
      end
   end
else
    begin
    // y > x here
    if abs(y) > abs(z) then
       begin
       Result := y;
       Index := 1;
       end
    else
       begin
       Result := z;
       Index := 2;
       end      ;
    end
end;
end;


function RotationDeltaInLimits(Delta,Limits:TRotation):Boolean;
begin
Result := False;
if abs(Delta.x)>Limits.x then exit;
if abs(Delta.y)>Limits.y then exit;
if abs(Delta.z)>Limits.z then exit;
Result := True
end;


function SameSign(Value1,Value2:Double):Boolean;
begin
Result := False;
if (Abs(Value1)+Abs(Value2))= abs(Value1+Value2) then Result := True;
end;

{procedure GetReducingAxes(StartRotation,EndRotation: TRotation;var ReductionData:TReductionData);
begin
ReductionData.ReducingAxes := [];
ReductionData.ErrorChangedSign := [];
ReductionData.NumberReducing := 0;
if not SameSign(StartRotation.x,EndRotation.x) then
   begin
   ReductionData.ErrorChangedSign := ReductionData.ErrorChangedSign + [XRed];
   end;
if abs(EndRotation.x) < abs(StartRotation.x)then
   begin
   ReductionData.ReducingAxes := ReductionData.ReducingAxes + [XRed];
   end;
if not SameSign(StartRotation.y,EndRotation.y) then
   begin
   ReductionData.ErrorChangedSign := ReductionData.ErrorChangedSign + [YRed];
   end;
if abs(EndRotation.y) < abs(StartRotation.y)then
   begin
   ReductionData.ReducingAxes := ReductionData.ReducingAxes + [YRed];
   end;
if not SameSign(StartRotation.z,EndRotation.z) then
   begin
   ReductionData.ErrorChangedSign := ReductionData.ErrorChangedSign + [ZRed];
   end;
if abs(EndRotation.z) < abs(StartRotation.z)then
   begin
   ReductionData.ReducingAxes := ReductionData.ReducingAxes + [ZRed];
   end;
end;}


end.
