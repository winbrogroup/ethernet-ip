unit G3DLine;

interface


uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, GMath3D,G3DDefs,G3DPaper, StdCtrls,G3DObjects;

type
  TG3DLine = class(TG3DObject)

  private
  FLength       : Double;
  procedure SetLineLength(value : Double);
  procedure CreateRealPoints; override;
  protected

  public
  procedure CreateDatumPoints(NoRotate : Boolean); override;
  constructor create(AOwner : Tcomponent); override;
  procedure DrawOn(Surface : TG3DPaper;DisplayMode : TDisplayModes); override;
  procedure MakeDescription(var Description: TStringList;Mode : TDescriptionMode); override;

  published
  property Length     : Double read FLength write SetLineLength;
  end;

procedure Register;

implementation
constructor TG3DLine.Create(AOwner : TComponent);
begin
inherited;
FObjectType :=  t3oLine;
end;

procedure TG3DLine.DrawOn(Surface : TG3DPaper;DisplayMode : TDisplayModes);
begin
inherited;
if not assigned(Surface) then exit;
if FVirtualOnly then exit;
CalculateScreenPoints(Surface);
with Surface.Offscreen.Canvas do
        begin
        MoveTo(ScreenPoint[0].x,ScreenPoint[0].y);
        LineTo(ScreenPoint[1].x,ScreenPoint[1].y);
        end;
end;

procedure TG3DLine.SetLineLength(Value : Double);
begin
FLength  := Value;
end;



procedure TG3DLine.CreateDatumPoints(NoRotate : Boolean);
begin
if not NoRotate then inherited;
// First Calculate Points aligned to x,y,z
Verteces[0] := SetPrecisePoint3D(FReferencePoint.x-(FLength/2.0),FReferencePoint.y,FReferencePoint.z);
Verteces[1] := SetPrecisePoint3D(FReferencePoint.x+(FLength/2.0),FReferencePoint.y,FReferencePoint.z);
// Now rotate all points relative to DatumRotation
if Not NoRotate then
   begin
   if not RotationIsZero(FPositionData.AbsoluteRotation) then
      begin
      RotatePointAboutReference(Verteces[0],FPOsitionData);
      RotatePointAboutReference(Verteces[1],FPOsitionData);
      end;
   end
else
   begin
   FpositionData.AbsoluteRotation := SetPRotation(0,0,0);
   end;
end;


procedure TG3DLine.MakeDescription(var Description: TStringList;Mode : TDescriptionMode);
begin
inherited   MakeDescription(Description,Mode);
Description.Add(Format('Length=%f',[FLength]));


end;


procedure TG3DLine.CreateRealPoints;
begin
FLength := 100.0;
NumberVerteces := 2;
SetLength(Verteces,2);
SetLength(ScreenPoint,2);
CreateDatumPoints(False);
SetCORtoDatum;
end;

procedure Register;
begin
  RegisterComponents('G3D', [TG3DLine]);
end;


end.
