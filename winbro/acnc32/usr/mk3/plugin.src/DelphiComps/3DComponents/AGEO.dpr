library AGEO;
//
// Version 1.1 29-5-2001 ---  modified GEOPluginCorrectForStackingAxis to pass
// back original C position (Not Zero as is returned from PCM) if the C axis
// is not used;
//
// Version 1.2 14/6-2001 ---  Changed signs of X and Y on set stacking axis

// Version 1.3 6/8/2001 Take out all calls to go to DisplayMode dmFull in
// order to stop Runtime change to edit mode
// NB the editor will not work until these are changed back and recompiled
// Changes are marked with !!!1.3


uses
  Forms,
  SysUtils,
  NamedPlugin in '..\..\sdk1.4\NamedPlugin.pas',
  PluginInterface in '..\..\sdk1.4\PluginInterface.pas',
  AbstractFormPlugin in '..\..\sdk1.4\AbstractFormPlugin.pas',
  AbstractGridPlugin in '..\..\sdk1.4\AbstractGridPlugin.pas',
  AbstractOPPPlugin in '..\..\sdk1.4\AbstractOPPPlugin.pas',
  AbstractPlcPlugin in '..\..\sdk1.4\AbstractPlcPlugin.pas',
  G3DDefs in 'G3DDefs.pas',
  VectorMaths in 'VectorMaths.pas',
  G3DOTree in 'G3DOTree.pas',
  G3DObjects in 'G3DObjects.pas';

{$R *.RES}

// not in the interface?
function DeleteReportPoint(PName : PChar): Boolean;stdcall;
begin
  if Assigned(GEO) then begin
  end;
end;


function ACNC32PluginRevision : Cardinal; stdcall;
begin
  Result := $01000000;
end;

procedure GEOPluginCreate; stdcall;
begin
{  if Assigned(GEO) then
    raise Exception.Create('Only one instance of GT may be created per client');
  GEO := TGEO.Create(Application);
  ReportPointEdit := TReportPointEdit.Create(Application);
  ReportVectorEdit := TReportVectorEdit(Application);}
end;

procedure GEOPluginDestroy; stdcall;
begin
  if Assigned(GEO) then begin
    ReportPointEdit.Free;
    ReportVectorEdit.Free;
    GEO.Free;
    GEO := nil;
  end;
end;


procedure GEOPluginGetFitQuality(var Nominal, Mount :  array of Double;var Max : Integer);stdcall
begin
  if Assigned(GEO) then
     begin
     GEO.PCM.GetFitError(Nominal, Mount,Max);
     end;
end;


// Clear all offsets and vectors first
// call PCM LoadConfig with file name and then get Nominal stacking axis from PCM
// then set corrected stacking axis to Nominal
procedure GEOPluginLoadConfig(aFile : PChar; Sqr, Retry : Integer; Fit : Double); stdcall;
begin
  if Assigned(GEO) then
     begin
     GEO.TV1.ClearAllOffsetsandVectors;
     GEO.LoadPCMandInitialise(afile,Sqr,Retry,Fit);
     end;
end;


procedure GEOPluginShow;
begin
  if Assigned(GEO) then
     begin
     if not GEO.Visible then
        begin
        GEO.OnMachineShow
        end
     else
         begin
         GEO.Visible := False;
         end
     end;

end;

procedure GEOPluginHide;
begin
  if Assigned(GEO) then
    GEO.Hide;
end;

procedure GEOPluginReset;
begin
       if Assigned(GEO) then
     GEO.TV1.ClearAllOffsetsandVectors;
end;


// acnc gives machine xyz machine axis position
// create a probe point machine relative with given name
// get position of the point at machine datum and then pass this to PCM
// using PCM setprobe point in the PCM object on GEO form
procedure GEOPluginSetProbePoint(aName : PChar; P1 : TPLocation); stdcall;
var
ProbePoint : TPlocation;
PointLocation : TPlocation;
StartPositions : TAxisPosn;
PIndex         : Integer;
begin
if Assigned(GEO) then
     begin
     GEO.InhibitExternalUpdate := True;
     try
     // first attach a point to the part datum which is the position of the
     // tooling ball for the current axis position
     StartPositions := GEO.TV1.GetAllAxisPositions;

     GEO.ProbePosnToPartRelativePoint('SPtemp',P1.x,
                                               P1.y,
                                               P1.z,
                                               StartPositions[3],
                                               StartPositions[4],
                                               StartPositions[5]);

     PIndex := GEO.Tv1.GetReportPointIndexfor('SPtemp');
     GEO.TV1.UpdateReportData(True);
     PointLocation := GEO.TV1.GetReportPointFromList(Pindex);
     ProbePoint := GEO.TV1.GetPointAtDatum(PointLocation.x,PointLocation.y,PointLocation.z);

     GEO.PCM.SetProbePoint(aName,-ProbePoint.x,-ProbePoint.y,ProbePoint.z);
     finally
     GEO.TV1.DeleteReportPointCalled('SPtemp');
     GEO.InhibitExternalUpdate := False;
     end;
     end;
end;


// Sets axis position of machine model and updates all report points etc
procedure GEOPluginSetAxisPositions(X,Y,Z,A,B,C : Double);stdcall;
begin
  if Assigned(GEO) then
     begin
//  if GEO.DebugStopAxisUpdate then exit;   
  if GEO.InhibitExternalUpdate then exit;
     GEO.TV1.UserMode := umRuntime;
     GEO.TV1.AxisPosition[0] := X;
     GEO.TV1.AxisPosition[1] := Y;
     GEO.TV1.AxisPosition[2] := Z;
     GEO.TV1.AxisPosition[3] := A;
     GEO.TV1.AxisPosition[4] := B;
     GEO.TV1.AxisPosition[5] := C;
     if (GEO.FDMode = dmOnMachine) then
        begin
        GEO.UpdateMachineReportDataAndDisplay;
        GEO.UpdateGEOPositionGrid;
        end;
     end;
end;

// Add a report point at a position P1 relative to the Part datum
function GEOPluginAddPartRelativePoint(PName :PChar;  P1 : TPLocation): Integer; stdcall;
begin
  if Assigned(GEO) then
     begin
     Result := GEO.TV1.AddPartRelativePoint(P1.X, P1.Y, P1.Z, PName);
     end
  else
     begin
     Result := -1;
     end
end;

// Add a report point at a position P1 to 0,0,0 of machine and attach it to the part datum
function GEOPluginAddMachineRelativePoint(PName :PChar;  P1 : TPLocation): Integer;stdcall;
begin
  if Assigned(GEO) then
     begin
     Result := GEO.TV1.AddMachineRelativePoint(P1.X, P1.Y, P1.Z, PName);
     end
     else
     begin
     Result := -1;
     end
end;


// Returns the Real World position of the point in space
// (relative to Part Datum being 0,0,0) in te var P1
function GEOPluginGetPoint(PName :PChar; var P1 : TPLocation): Boolean;stdcall;
begin
Result := false;
if Assigned(GEO) then
     begin
     Result := GEO.GetPointData(PName,P1);
     end;
end;

// Add a report point at a position P1 relative to the Tool datum
function GEOPluginAddToolRelativePoint(PName :PChar; P1 : TPLocation): Integer;stdcall;
begin
  if Assigned(GEO) then
     begin
     Result := GEO.TV1.AddToolRelativePoint(P1.X, P1.Y, P1.Z, PName);
     end
     else
     begin
     Result := -1;
     end
end;


{This function affects the position of the 'Electrode' or Probe Datum in the
machine model and moves the displayed and reported ElectrodeDatum Point}
procedure GEOPluginSetToolOffset(P1 : TPLocation)stdcall;
begin
  if Assigned(GEO) then
     begin
     GEO.SetToolOffset(P1);
     end
end;

// Get named vector from PCM
// pass to ACN the position of vector points when rotary axes are at given
// ABC in the vars P1 and P2
procedure GEOPluginGetProbeVector(aName : PChar;A,B,C :Double; var P1, P2 : TPLocation); stdcall;
var
PCMP1,PCMP2 : TPlocation;
begin
if Assigned(GEO) then
     begin
     // Get Vecto Points from PCM into PCMP1 and PCMP2
     GEO.GetPCMProbeVector(aName,PCMP1,PCMP2);
     // Add the points to the model
     // Note x and Y are negated as they are on the Part . Z is on the tool
     GEO.TV1.AddVector(-PCMP1.x,-PCMP1.y,PCMP1.z,-PCMP2.x,-PCMP2.y,PCMP2.z, rvPartRelative, aName);
     try
     // make temporary points in order to find axis position for point usung current tool offset
     GEO.TV1.AddReportPointat(-PCMP1.x,-PCMP1.y,PCMP1.z,'_temp1',rpPartRelative,True);
     P1 := GEO.GetAxisPosForPoint('_temp1',A,B,C);
     GEO.TV1.AddReportPointat(-PCMP2.x,-PCMP2.y,PCMP2.z,'_temp2',rpPartRelative,True);
     P2 := GEO.GetAxisPosForPoint('_temp2',A,B,C);
     finally
     // Delete the temporary points
     GEO.TV1.DeleteReportPointCalled('_temp1');
     GEO.TV1.DeleteReportPointCalled('_temp2');
     end;
     if (GEO.FDMode = dmOnMachine) then
        begin
        GEO.UpdateMachineReportDataAndDisplay;
        GEO.UpdateVectorSelectList;
        end;
     end;
end;

// This procedure returns the absolute XYZ axis positions which place the current
// Electrode datum Point co-incident with the named point given the ABC axis positions;
procedure GEOPluginMachinePosForPoint(aName : PChar; A,B,C : Double; var XYZPos :TPLocation); stdcall;
begin

if assigned(GEO) then XYZPos := GEO.GetAxisPosForPoint(aName,A,B,C);

end;

{ This procedure is used to create a PartRelative Point of the given name
at the Electrode Datum Position for the given axis positions
It assumes the passed in positions hve been corrcted for tool ball radius.
This is used to 'attach' points to the Part Dtaum from  probe data after it has
been corrected for tool ball radius.
}
procedure GEOPluginProbePositionToMachineRelativePoint(PName :PChar;X,Y,Z,A,B,C : Double);stdcall;
var
StartPoint : TPlocation;
begin
if assigned(GEO) then GEO.ProbePosnToPartRelativePoint(PName,X,Y,Z,A,B,C);
end;

// Calculate corrected axis position starting at position defined by XYZABC and returning
// answer in XYZABC. Use of C axis for comp is controled by UseCAxis Parameter.
procedure GEOPluginCorrectForStackingAxis(UseCAxis : Boolean; var X,Y,Z,A,B,C : Double); stdcall;
var
AxisPosition : TAxisPosn;
StartStage     : Integer;
begin
if Assigned(GEO) then begin
  GEO.CorrectForStackingAxis(UseCAxis,X,Y,Z,A,B,C);
  end;
end;

// START OF NOT CURRENTLY IMPLEMENTED
function GEOPluginGetPointAtDatum( P1 : TPLocation):TPLocation; stdcall;
begin
  if Assigned(GEO) then begin
  Result := GEO.TV1.GetPointAtDatum(P1.x, P1.y, P1.z);
     end
     else
     begin
     Result.X := 0;
     Result.Y := 0;
     Result.Z := 0;
     end
end;


// Get Nominal vector points by name from PCm
// Return data after correction for stacking axis in var parameters P1 , P2
procedure GEOPluginGetAdjustProbeVector (aName : PChar; P1, P2 : TXYZ); stdcall;
begin
end;



procedure GEOPluginAddReportPoint(VName : PChar; P1 : TPLocation); stdcall;
begin
  if Assigned(GEO) then
     begin
     end;
end;

procedure GEOPluginAddVector(VName : PChar;  P1, P2 : TPLocation); stdcall;
begin
  if Assigned(GEO) then
     begin
     GEO.TV1.AddVector(P1.x, P1.y, P1.z, P2.x, P2.y, P2.z, rvPartRelative, VName);
     if (GEO.FDMode = dmOnMachine) then GEO.TV1.Update3DDisplay(GEO.Paper,umCustomOnly);
     end;
end;

procedure GetStackingVectors(var Pa1, Pa2, RotaP,Pn1, Pn2, RotnP : TXYZ)  ;
var
NomVec,ActVec,NomTiming,ActTiming  :TVectorData;
begin
if not assigned(GEO) then exit;
if (GEO.TV1.NominalVectorIndex >= 0) and (GEO.TV1.ActualVectorIndex >= 0) then
   begin
   NomVec := GEO.TV1.FReportVectorList[GEO.TV1.NominalVectorIndex];
   ActVec := GEO.TV1.FReportVectorList[GEO.TV1.ActualVectorIndex];
   end;

if (GEO.TV1.ATVectorIndex >= 0) and (GEO.TV1.NTVectorIndex >= 0) then
   begin
   NomTiming := GEO.TV1.FReportVectorList[GEO.TV1.NTVectorIndex];
   ActTiming := GEO.TV1.FReportVectorList[GEO.TV1.ATVectorIndex];
   end;
Pn1 := TXYZ(NomVec.Point1Position);
Pn2 := TXYZ(NomVec.Point2Position);
Pa1 := TXYZ(ActVec.Point1Position);
Pa2 := TXYZ(ActVec.Point2Position);
RotaP := TXYZ(ActTiming.Point2Position);
RotnP := TXYZ(NomTiming.Point2Position);
end;

procedure   GEOPluginGetStackingAxis(var Pa1, Pa2, RotaP,Pn1, Pn2, RotnP : TXYZ); stdcall;
var
Vec1,Vec2 :TVectorData;
begin
  if Assigned(GEO) then
     begin
//     GetStackingVectors(Pa1, Pa2, RotaP,Pn1, Pn2, RotnP);
     GEO.PCM.GetAdjustedStackingAxis(Pa1, Pa2, RotaP);
     GEO.PCM.GetStackingAxis(Pn1, Pn2, RotnP);
     GEO.TV1.SetNominalStackingAxis(-Pn1.x, -Pn1.y, Pn1.z,
                                   -Pn2.x, -Pn2.y, Pn2.z,
                                   -RotnP.x, -RotnP.y, RotnP.z);
     GEO.TV1.SetActualStackingAxis (-Pa1.x, -Pa1.y, Pa1.z,
                                   -Pa2.x, -Pa2.y, Pa2.z,
                                   -RotaP.x, -RotaP.y, RotaP.z);
     if (GEO.FDMode = dmOnMachine) then
        begin
        GEO.UpdateMachineReportDataAndDisplay;
        end;
     end;

end;

procedure   GEOPluginSetStackingAxis(Pa1, Pa2, RotaP,Pn1, Pn2, RotnP : TXYZ); stdcall;
begin
  if Assigned(GEO) then
     begin
     GEO.TV1.SetNominalStackingAxis(-Pn1.x, -Pn1.y, Pn1.z, -pn2.x, -pn2.y, pn2.z, -RotnP.x,-RotnP.y, RotnP.z);
     GEO.TV1.SetActualStackingAxis(-Pa1.x, -Pa1.y, Pa1.z, -pa2.x, -pa2.y, pa2.z, -RotaP.x,-RotaP.y, RotaP.z);
     if (GEO.FDMode = dmOnMachine) then
        begin
        GEO.UpdateMachineReportDataAndDisplay;
        end;
     end;
end;


// END OF NOT CURRENTLY IMPLEMENTED

exports
// These are used by acnc32
  GEOPluginCreate,
  GEOPluginDestroy,

  GEOPluginReset,
  GEOPluginLoadConfig,
  GEOPluginAddPartRelativePoint,
  GEOPluginAddMachineRelativePoint,
  GEOPluginAddToolRelativePoint,
  GEOPluginGetPoint,
  GEOPluginShow,
  GEOPluginHide,
  GEOPluginSetToolOffset,
  GEOPluginProbePositionToMachineRelativePoint,
  GEOPluginMachinePosForPoint,
  GEOPluginCorrectForStackingAxis,
  GEOPluginGetStackingAxis,
  GEOPluginSetStackingAxis,
  ACNC32PluginRevision,
  GEOPluginSetProbePoint,
  GEOPluginGetFitQuality,



// Are these  currently used ?????
  GEOPluginGetPointAtDatum,
  GEOPluginSetAxisPositions,
  GEOPluginGetProbeVector,

  GEOPluginAddVector,
  GEOPluginAddReportPoint,
  GEOPluginGetAdjustProbeVector;

begin
end.

