unit G3Math2d;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls,Math,G3DDefs;
const
nearzero       =0.000000001;

function AngleBetweenPrecisePoints2DInDegrees(Point1,Point2 : TPrecisePoint): Double;

function ZCos(Angle : Double):Double;
function ZSin(Angle : Double):Double;
function TwoPointLineEquation2D(Point1,Point2 : TPoint): TLineFormula;
function PreciseTwoPointLineEquation2D(Point1,Point2 : TPrecisePoint): TLineFormula;
function PointAndAngleEquation(Point: TPoint; Angle : Double): TLineFormula;
function PrecisePointAndAngleEquation(Point: TPrecisePoint; Angle : Double): TLineFormula;
function OnePointGradLineEquation2D(Point: TPrecisePoint;Gradient: Double): TLineFormula;
function PerpendicularToLineAtPoint(Line :TLineFormula; Point : TPrecisePoint):TLineFormula;
function Intersect(Line1,Line2:TLineFormula): TPoint;
function PreciseIntersect(Line1,Line2:TLineFormula): TPrecisePoint;
function LeftMost(Point1,Point2,Point3: TPoint): TPoint;
function RightMost(Point1,Point2,Point3: TPoint): TPoint;
function ThreePointCircleCenter2D(Point1,Point2,Point3:Tpoint):TPoint;
function LineLength2D(Point1,Point2 : Tpoint): Integer;
function LineLength3D(Point1,Point2 : TIntLocation): Integer;
function PreciseLineLength2D(Point1,Point2 : Tpoint): Double;
function PPreciseLineLength2D(Point1,Point2 : TPrecisePoint): Double;
function PerpendicurToLineThroughPoint(Line1 : TLineFormula;Point : TPrecisePoint):TLineFormula;

function TestLineandCircleIntersect(Line : TLineFormula;Circle : TCircleFormula;var Delta : Double):TCircleIntersectType;
function LineandCircleIntersects(Line : TLineFormula;Circle : TCircleFormula; var Intersect1,Intersect2 : TPrecisePoint):TCircleIntersectType;
function RightMostofTwo(Point1,POint2:TPrecisePoint):TprecisePoint;
function LeftMostofTwo(Point1,POint2:TPrecisePoint):TprecisePoint;
function TopMostofTwo(Point1,POint2:TPrecisePoint):TprecisePoint;



function AngleBetweenPoints2D(Point1,Point2 : Tpoint): Double;
function IntegerToPrecise(X,Y:Integer):TPrecisePoint;
function PreciseMidPoint(Point1,Point2:TPoint): TPrecisePoint;
function PPreciseMidPoint(Point1,Point2:TPrecisePoint): TPrecisePoint;
function FanToTPoint(FanP : Fanpoint):TPoint;
function TPointToFanPOint(TPt : TPoint):FanPoint;
function TPointToPrecise(Point : TPoint):TPrecisePoint;



// 3D Functions
function PreciseLineLength3D(Point1,Point2 : TIntLocation): Double;
function PRotateAboutY3D(Point,Origin :TPLocation;Angle : Double) :TPLocation;
function PRotateAboutX3D(Point,Origin :TPLocation;Angle : Double) :TPLocation;
function PRotateAboutZ3D(Point,Origin :TPLocation;Angle : Double) :TPLocation;

function SetPrecisePoint3D(x,y,z : Double):TPLocation;
function SetScreenLocation(x,y: Integer):TScreenLocation;
function InvertPos(InPos : TPLocation):TPLocation;
function PosNormalise(InPos : TPLocation;VLength : Double):TPLocation;
function PosVecLength(InPos : TPLocation): Double;



function SetPRotation(x,y,z : Double):TRotation;
function AddLocation(Loc1,Loc2 : TPlocation):TPlocation;
function AddScreenLocation(Loc1,Loc2 : TScreenLocation):TScreenLocation;

function AddRotation(Rot1,Rot2 : TRotation):TRotation;
function SubtractLocation(Loc1,Loc2 : TPlocation):TPlocation;
function SubtractRotation(Rot1,Rot2 : TRotation):TRotation;
function LocationsEqual(Loc1,Loc2: TPlocation):Boolean;

function PGetDiscVerteces(Centre:TPLocation;Radius :Double;Height : Double):TDiscVerts;
function PCirclePoint(Centre:TPLocation;Radius : Double;Angle: Double):TPLocation;

procedure IntersectPoint(Angle1,Angle2,Offset1,Offset2: Double;y : Integer; var x,z:Integer);


function PXZVectorAngle(Point,Origin :TPLocation;var Hypot: Double):Double;

implementation



{TLineFormula = record
             Yval : Double;
             Shift: Double;
             isX  : Boolean;
             isY  : Boolean;
             XVal : Double;
             end;}

// Returns the formula for the line which passes through 'Point' and
// is perpenducular to Line 1;
function PerpendicurToLineThroughPoint(Line1 : TLineFormula;Point : TPrecisePoint):TLineFormula;
var
Slope : Double;
begin
// n.b. Slope of a line perpendicular to a line of slope m = -1/m
if Line1.YVal <> 0 then
   begin
   Slope := -1.0/Line1.Yval;
   Result := OnePointGradLineEquation2D(Point,Slope);
   end
else
   begin
   //Here if input line is vertical
   Result.isY := True;
   Result.isX := False;
   Result.Yval := 0;
   Result.XVal := 0;
   Result.Shift := Point.y;
   end

end;

function PosVecLength(InPos : TPLocation): Double;
begin
Result := Sqrt((Inpos.x*InPOs.x)+(Inpos.y*InPOs.y)+(Inpos.z*InPOs.z));
end;

function PosNormalise(InPos : TPLocation;VLength : Double):TPLocation;
var
OrgLength : Double;
begin
OrgLength := PosVecLength(InPos);
if VLength > 0.00001 then
  begin
  Result.x := (Inpos.x/OrgLength)*VLength;
  Result.y := (Inpos.y/OrgLength)*VLength;
  Result.z := (Inpos.z/OrgLength)*VLength;
  end
else
  begin
  Result.x := Inpos.x;
  Result.y := Inpos.y;
  Result.z := Inpos.z;
  end
end;

function InvertPos(InPos : TPLocation):TPLocation;
begin
Result.x := -Inpos.x;
Result.y := -Inpos.y;
Result.z := -Inpos.z;
end;

function SetPrecisePoint3D(x,y,z : Double):TPLocation;
begin
Result.x :=x;
Result.y :=y;
Result.z :=z;
end;

function SetScreenLocation(x,y: Integer):TScreenLocation;
begin
Result.x :=x;
Result.y :=y;
end;

function SetPRotation(x,y,z : Double):TRotation;
begin
Result.x :=x;
Result.y :=y;
Result.z :=z;
end;

function AddLocation(Loc1,Loc2 : TPlocation):TPlocation;
begin
Result.x := Loc1.x+Loc2.x;
Result.y := Loc1.y+Loc2.y;
Result.z := Loc1.z+Loc2.z;
end;

function AddScreenLocation(Loc1,Loc2 : TScreenLocation):TScreenLocation;
begin
Result.x := Loc1.x+Loc2.x;
Result.y := Loc1.y+Loc2.y;
end;

function AddRotation(Rot1,Rot2 : TRotation):TRotation;
begin
Result.x := Rot1.x+Rot2.x;
Result.y := Rot1.y+Rot2.y;
Result.z := Rot1.z+Rot2.z;
end;

function SubtractLocation(Loc1,Loc2 : TPlocation):TPlocation;
begin
Result.x := Loc1.x-Loc2.x;
Result.y := Loc1.y-Loc2.y;
Result.z := Loc1.z-Loc2.z;
end;

function SubtractRotation(Rot1,Rot2 : TRotation):TRotation;
begin
Result.x := Rot1.x-Rot2.x;
Result.y := Rot1.y-Rot2.y;
Result.z := Rot1.z-Rot2.z;
end;

function LocationsEqual(Loc1,Loc2: TPlocation):Boolean;
begin
Result := True;
if Loc1.x<>Loc2.x then Result := False;
if Loc1.y<>Loc2.y then Result := False;
if Loc1.z<>Loc2.z then Result := False;
end;



// This function returns the angle of the vector from 3dOrigin to 3Dpoint
// in the XY plane
function PXYVectorAngle(Point,Origin :TPLocation;var Hypot: Double):Double;
var
Point2D : TPrecisePoint;
Origin2D: TPrecisePOint;
SideRatio    : Double;
Xpos , YPOs   : Boolean;
begin
POint2D.x := Point.x;
POint2D.y := Point.y;
Origin2D.x  := Origin.x;
Origin2D.y  := Origin.y;
Xpos := (Point2D.x >= Origin2D.x);
Ypos := (Point2D.y >= Origin2D.y);
try
Hypot := PPreciseLineLength2D(Point2D,Origin2D);
if Abs(Hypot) < nearzero then
   begin
   Result := 0.0;
   exit;
   end;
SideRatio := abs((Point.x-Origin.x)/Hypot);
if SideRatio >1 then SideRatio := 1.0;
if SideRatio <-1 then SideRatio := -1.0;
if Xpos and Ypos then
   begin
   Result := arcsin(SideRatio);
   exit;
   end;
if Xpos and (not Ypos) then
   begin
   Result := (Pi/2.0)+arccos(SideRatio);
   exit;
   end;
if (not Xpos) and (not Ypos) then
   begin
   Result := Pi+arcsin(SideRatio);
   exit;
   end
else
   begin
   Result := (Pi*1.5)+arccos(SideRatio);
   exit;
   end;

except
   Result := 0.0;
   exit;
end; // try except

end;


// This function returns the angle of the vector from 3dOrigin to 3Dpoint
// in the XZ plane
function PXZVectorAngle(Point,Origin :TPLocation;var Hypot: Double):Double;
var
Point2D : TPrecisePoint;
Origin2D: TPrecisePoint;
SideRatio    : Double;
Xpos , ZPOs   : Boolean;
begin
POint2D.x := Point.x;
POint2D.y := Point.z;
Origin2D.x  := Origin.x;
Origin2D.y  := Origin.z;
Xpos := (Point2D.x >= Origin2D.x);
Zpos := (Point2D.y >= Origin2D.y);

try
Hypot := PPreciseLineLength2D(Point2D,Origin2D);
if abs(Hypot) < nearzero then
   begin
   Result := 0.0;
   exit;
   end;
SideRatio := abs((Point.x-Origin.x)/Hypot);
if SideRatio >1 then SideRatio := 1.0;
if SideRatio <-1 then SideRatio := -1.0;

if Xpos and Zpos then
   begin
   Result := arcsin(SideRatio);
   exit;
   end;
if Xpos and (not Zpos) then
   begin
   Result := (Pi/2.0)+arccos(SideRatio);
   exit;
   end;
if (not Xpos) and (not ZPos) then
   begin
   Result := Pi+arcsin(SideRatio);
   exit;
   end
else
   begin
   Result := (Pi*1.5)+arccos(SideRatio);
   exit;
   end;


except
   Result := 0.0;
   exit;
end;


end;


// This function returns the angle of the vector from 3dOrigin to 3Dpoint
// in the YZ plane
function PYZVectorAngle(Point,Origin :TPLocation;var Hypot: Double):Double;
var
Point2D : TPrecisePoint;
Origin2D: TPrecisePoint;
SideRatio      : Double;
Xpos , ZPOs   : Boolean;
begin
POint2D.x := Point.y;
POint2D.y := Point.z;
Origin2D.x  := Origin.y;
Origin2D.y  := Origin.z;
//Xpos := (Point2D.x >= Origin.x);
//Zpos := (Point2D.y >= Origin.z);
Xpos := (Point2D.x > Origin2D.x);
Zpos := (Point2D.y > Origin2D.y);
try
Hypot := PPreciseLineLength2D(Point2D,Origin2D);
if abs(Hypot) < nearzero then
   begin
   Result := 0.0;
   exit;
   end;
SideRatio := abs((Point.y-Origin.y)/Hypot);
if SideRatio >1 then SideRatio := 1.0;
if SideRatio <-1 then SideRatio := -1.0;

if Xpos and Zpos then
   begin
   Result := arcsin(SideRatio);
   exit;
   end;
if Xpos and not Zpos then
   begin
   Result := (Pi/2.0)+arccos(SideRatio);
   exit;
   end;
if (not Xpos) and (not ZPos) then
   begin
   Result := Pi+arcsin(SideRatio);
   exit;
   end
else
   begin
   Result := (Pi*1.5)+arccos(SideRatio);
   exit;
   end;
except
   Result := 0.0;
   exit;
end; // try except;

end;

{function ZVectorCos(Point,Origin :TIntLocation):Double;
var
Hypot : Double;
begin
Hypot :=  PreciseLineLength3D(Point,Origin);
Result := (Point.z - Origin.z)/Hypot;
end;}


function PRotateAboutZ3D(Point,Origin :TPLocation;Angle : Double) :TPLocation;
var
Radius : Double;
TAngle  : Double;
DeltaX,DeltaY   : Double;
begin
{If LocationsEqual(Point,Origin) or (Angle = 0.0) then
  begin
  Result :=Point;
  exit;
  end;}

Tangle := DegToRad(Angle)+ PXYVectorAngle(Point,Origin,Radius);
DeltaY := Radius*ZCos(Tangle);
DeltaX := Radius*ZSin(Tangle);
Result.x := Origin.x+Deltax;
Result.y := Origin.y+Deltay;
Result.z := Point.z;
end;


function PRotateAboutY3D(Point,Origin :TPLocation;Angle : Double) :TPLocation;
var
Radius : Double;
TAngle  : Double;
DeltaX,DeltaZ   : Double;
begin
If LocationsEqual(Point,Origin) or (Angle = 0.0) then
  begin
  Result :=Point;
  exit;
  end;
// Total angle after rotation is current angle + RequestedAngle
Tangle := DegToRad(Angle)+ PXZVectorAngle(Point,Origin,Radius);
//Delatax and DeltaZ are new distances from the origin
DeltaZ := Radius*ZCos(Tangle);
DeltaX := Radius*ZSin(Tangle);
Result.x := Origin.x+Deltax;
Result.y := Point.y;
Result.z := Origin.z+Deltaz;
end;


function PRotateAboutX3D(Point,Origin :TPLocation;Angle : Double) :TPLocation;
var
Radius : Double;
TAngle  : Double;
Deltay,DeltaZ   : Double;
begin
If LocationsEqual(Point,Origin) or (Angle = 0.0) then
  begin
  Result :=Point;
  exit;
  end;
Tangle := DegToRad(Angle)+ PYZVectorAngle(Point,Origin,Radius);
DeltaZ:= Radius*ZCos(Tangle);
Deltay := Radius*ZSin(Tangle);
//Result := Point;
Result.x := Point.x;
Result.y := Origin.y+Deltay;
Result.z := Origin.z+DeltaZ;
end;

function TPointToPrecise(Point : TPoint):TPrecisePoint;
begin
Result.x := Point.x;
Result.y := Point.y;
end;

// returns the equation of a line passing through a point and with a given angle from Vertical
function PointAndAngleEquation(Point: TPoint; Angle : Double): TLineFormula;
var
Slope : Double;
begin
Result.isY := False;
Result.isX := False;
Slope := tan(angle);
Result := OnePointGradLineEquation2D(TPointToPrecise(Point),Slope)
end;

// returns the equation of a line passing through a precise point and with a given angle from Vertical
function PrecisePointAndAngleEquation(Point: TPrecisePoint; Angle : Double): TLineFormula;
var
Slope : Double;
begin
Result.isY := False;
Result.isX := False;
if abs(angle) < nearzero then
   begin
   Result.isY := True;
   Result.XVal := 1;
//!q!   Result.XVal := POint.x;
   Result.YVal := 0;
   Result.Shift := -POint.y;
   exit;
   end;
Slope := 1/Tan(angle);
Result := OnePointGradLineEquation2D(Point,Slope)
end;

function PreciseTwoPointLineEquation2D(Point1,Point2 : TPrecisePoint): TLineFormula;
var
ShiftX : Double;
Slope : Double;
Dx,Dy : Double;
begin
try
Result.isY := False;
Result.isX := False;
Result.XVal := 0;
Result.YVal := 0;

// dx is change in X point1 to point 2
dx := Point1.x-POint2.x;
// dy is change in Y point1 to point 2
dy := Point1.y-POint2.y;
if abs(dy) > nearzero then
   begin
   if abs(dx) > nearzero then
      begin
      //If there is change in both x and y then get slope
      Slope := dx/dy;
      end
   else
       begin
       //if change in Y only then line has formula X=constant
       // so set is X and X val to non changing X
       Result.isX := True;
//!q!       Result.XVal := Point1.X;
       Result.Shift := Point1.X;
       exit;
       end;
   end
else
    begin
    //if change in X only then line has formula Y =constant
    // so set isY and Y val to non changing Y
    Result.isY := True;
//!q!Result.YVal := Point1.Y;
    Result.Shift := Point1.Y;
    exit;
    end;

//
ShiftX := Slope*Point1.Y;
//YVal is the slope number in the formula X = YVal(Y)+Shift +. X=NY+C
Result.YVal := Slope;
// shift is crossing point of X axis
Result.Shift :=  Point1.X - ShiftX;;
finally
if Abs(Result.XVal) < nearzero then Result.XVal := 0.0;
if Abs(Result.YVal) < nearzero then Result.YVal := 0.0;
end;
end;

// returns the equation of a line passing through two given points
function TwoPointLineEquation2D(Point1,Point2 : TPoint): TLineFormula;
var
ShiftX : Double;
Slope : Double;
Dx,Dy : Integer;
begin
try
Result.isY := False;
Result.isX := False;
// dx is change in X point1 to point 2
dx := Point1.x-POint2.x;
// dy is change in Y point1 to point 2
dy := Point1.y-POint2.y;
if abs(dy) > nearzero then
   begin
   if abs(dx) > nearzero then
      begin
      //If there is change in both x and y then get slope
      Slope := dx/dy;
      end
   else
       begin
       //if change in Y only then line has formula X=constant
       // so set is X and X val to non changing X
       Result.isX := True;
//!q!       Result.XVal := Point1.X;
       Result.Shift := Point1.X;
       exit;
       end;
   end
else
    begin
    //if change in X only then line has formula Y =constant
    // so set isY and Y val to non changing Y
    Result.isY := True;
//!q!    Result.YVal := Point1.Y;
    Result.Shift := Point1.Y;
    exit;
    end;

//
ShiftX := Slope*Point1.Y;
//YVal is the slope number in the formula X = YVal(Y)+Shift +. X=NY+C
Result.YVal := Slope;
// shift is crossing point of X axis
Result.Shift :=  Point1.X - ShiftX;;
finally
if Abs(Result.XVal) < nearzero then Result.XVal := 0.0;
if Abs(Result.YVal) < nearzero then Result.YVal := 0.0;
end;
end;

function  OnePointGradLineEquation2D(Point: TPrecisePoint;Gradient: Double): TLineFormula;
begin
try
if abs(Gradient)< NearZero then
   begin
   Result.Xval := 0;
   Result.YVal := 1;
   Result.Shift := - Point.y;
   exit;
   end;

Result.Xval := 1;
Result.YVal := -Gradient;
// Sustitute point data to get C value (Shift)
//Ax+By+c = 0 Therefore C = -Ax-By
Result.Shift := -Point.x*Result.Xval - Point.Y*Result.YVal;
finally
if Abs(Result.XVal) < nearzero then Result.XVal := 0.0;
if Abs(Result.YVal) < nearzero then Result.YVal := 0.0;
end;

end;


function PerpendicularToLineAtPoint(Line :TLineFormula; Point : TPrecisePoint):TLineFormula;
var
PerpSlope : Double;
begin
if Line.isX then
   begin
   Result.isX := False;
   Result.IsY := True;
   Result.Yval := 0;
   Result.XVal := 0;
   Result.Shift := Point.Y;
   exit;
   end;
if Line.isY then
   begin
   Result.isY := False;
   Result.IsX := True;
//!q!   Result.Xval := Point.X;
   Result.YVal := 0;
   Result.XVal := 0;
   Result.Shift := Point.X;
   exit;
   end;
if abs(Line.YVal) < nearzero then exit;
PerpSlope := -1/Line.Yval; // slope of perpenicular = -1/Slope of line
Result := OnePointGradLineEquation2D(Point,PerpSlope)
end;
{If A1x + B1y + C1 = 0 and A2x + B2y + C2 = 0 are two lines,
then their slopes are given by m1 = -A1/B1 and m2 = -A2/B2.

If they intersect, their intersection point has coordinates


x = (-C1B2+C2B1)/(A1B2-A2B1),
y = (-A1C2+A2C1)/(A1B2-A2B1).}

//2d
// Returns the Precise point which is the intersect of two 2dLines
function PreciseIntersect(Line1,Line2:TLineFormula): TPrecisePoint;
Var
C1B2,C2B1,A1B2,A2B1 : Double;
A1C2,A2C1           : Double;
Denominator         : Double;
begin
C1B2 := Line1.Shift*Line2.Yval;
C2B1  :=Line2.Shift*Line1.Yval;
A1B2  := Line1.XVal*Line2.Yval;
A2B1 := Line2.XVal*Line1.Yval;
A1C2 := Line1.XVal*Line2.Shift;
A2C1 := Line2.XVal*Line1.Shift;
Denominator := (A1B2-A2B1);
if Denominator = 0.0 then
   begin
   if (Line1.isX) and (Line2.isY) then
      begin
      Result.x := Line1.Shift;
      Result.y := Line2.Shift;
      exit;
      end;
   if (Line1.isY) and (Line2.isY) then
      begin
      Result.x := Line2.Shift;
      Result.y := Line1.Shift;
      exit;
      end;
   Result.x := 0.0;
   Result.y := 0.0;
   end
else
    begin
    Result.x := (-C1B2+C2B1)/(A1B2-A2B1);
    Result.y := (-A1C2+A2C1)/(A1B2-A2B1);
    end;
{x = (-C1B2+C2B1)/(A1B2-A2B1),
y = (-A1C2+A2C1)/(A1B2-A2B1).}

end;

//2d
// Returns the Integerpoint which is the intersect of two 2dLines
function Intersect(Line1,Line2:TLineFormula): TPoint;
Var
X1,X2,shift1,Shift2 : Double;
XSum,ShiftSum     : Double;
nl1,nl2     : TLineFormula;
Xresult ,Yresult : Double;
function IsSimpleLine(Line : TLineFormula): Boolean;
begin
Result := Line.isX or Line.isY;
end;
begin

nl1 := Line1;
nl2 := Line2;
//xyRatio := Line1.Yval/Line2.Yval;
if IsSimpleLine(Line1) then
   begin
   if IsSimpleLine(Line2) then
       begin
       // Here for both lines parallele to an axis
       if Line1.isX and Line2.isX then
          begin
          // if both the same axis then they never meet!!!
          Result.x := 0;
          Result.y := 0;
          end
       else
           begin
           if Line1.isX then
              begin
              //Here if line 1 parallel to Y
              Result.x := Round(Line1.XVal);
              Result.y := Round(Line2.YVal);
              end
           else
              begin
              //Here if line 2 parallel to Y
              Result.x := Round(Line1.XVal);
              Result.y := Round(Line2.YVal);
              end;
           end
       end
   else
       begin
       //Here if Line1 is simple and Line 2 has gradient
       if Line1.isX then
          begin
          Result.x := Round(Line1.xVal);
          // substitute for x in Line 2
          // Line is of the Form x= YVal*Y+Shift;
          // therfore Y*YVal= x-Shift;
          // therfore Y=x-shift/Yval
          Result.y := Round((Line1.XVal-Line2.Shift)/Line2.YVal);
          end
       else
           begin
           Result.Y := Round(Line1.YVal);
           // substitute for Y in Line 2
           Result.x := Round((Line2.YVal*Line1.YVal)+Line2.shift);
           end;
       end;
   end
else
    begin
    // here id line 1 has a gradient
    if isSimpleLine(Line2) then
       begin
       if line2.isX then
          begin
          //Here if line 1 has gradient and line 2 is parallel to Y
          Result.x := Round(Line2.XVal);
          Result.y := Round((Line2.XVal-Line1.Shift)/Line1.YVal);
          end
       else
           begin
          //Here if line 1 has gradient and line 2 is parallel to X
           Result.y := Round(Line2.YVal);
           Result.x := Round((Line1.YVal*Line2.YVal)+Line1.shift);
           end;
       end
    else
        begin
        // Here if both have a gradient
        X1 := Line2.Yval;
        Shift1 := Line1.Shift* Line2.Yval;
        X2 := Line1.Yval;
        Shift2 := Line2.Shift* Line1.Yval;
        XSum := X1-X2;
        ShiftSum := Shift1-Shift2;
        if abs(XSum) > nearzero then
           begin
           XResult := ShiftSum/XSum;
           end
        else
            begin
            XResult := 0.0;
            end;

        Result.x := Round(XResult);
        YResult := (XResult-Line1.Shift)/Line1.YVal;
        Result.y := Round(YResult);
        end;
    end;
end;
//2d
// Returns the Precise (Double) midpoint of two Points
function PreciseMidPoint(Point1,Point2:TPoint): TPrecisePoint;
begin
Result.x := (Point1.x+Point2.x)/2;
Result.y := (Point1.y+Point2.y)/2;
end;

function PPreciseMidPoint(Point1,Point2:TPrecisePoint): TPrecisePoint;
begin
Result.x := (Point1.x+Point2.x)/2;
Result.y := (Point1.y+Point2.y)/2;
end;

//2d
// Returns the Integer midpoint of two Points
function MidPoint(Point1,Point2:TPoint): TPoint;
begin
Result.x := (Point1.x+Point2.x) div 2;
Result.y := (Point1.y+Point2.y) div 2;
end;

{This function returns the centre point of the circle which passes through
the three defined point}
function  ThreePointCircleCenter2D(Point1,Point2,Point3:Tpoint):TPoint;
var
LineEqu1,LineEqu2 : TLineFormula;
PerpLine1,PerpLine2 : TLineFormula;
Slope : Double;

begin
// get equation of line between point 1 and 2
LineEqu1 := TwoPointLineEquation2D(Point1,Point2);
// get equation of line between point 2 and 3
LineEqu2 := TwoPointLineEquation2D(Point2,Point3);
// n.b. Slope of a line perpendicular to a line of slope m = -1/m
Slope := -1.0/LineEqu1.Yval;
// get equation of the line which is equidistant point 1 and 2
Perpline1 := OnePointGradLineEquation2D(PreciseMidPoint(Point1,Point2),Slope);
Slope := -1.0/LineEqu2.YVal;
// get equation of the line which is equidistant point 2 and 3
Perpline2 := OnePointGradLineEquation2D(PreciseMidPoint(Point2,Point3),Slope);
//The intersect of these lines is the centre of the circle;
Result := Intersect(Perpline1,Perpline2);
end;

function LineLength2D(Point1,Point2 : Tpoint): Integer;
var
dx,dy : Integer;
begin
dx := Point2.x-Point1.x;
dy := Point2.y-Point1.y;
Result := Round(sqrt((dx*dx)+(dy*dy)));
end;

function PreciseLineLength2D(Point1,Point2 : Tpoint): Double;
var
dx,dy : Double;
begin
dx := Point2.x-Point1.x;
dy := Point2.y-Point1.y;
Result := sqrt((dx*dx)+(dy*dy));
end;

function PPreciseLineLength2D(Point1,Point2 : TPrecisePoint): Double;
var
dx,dy : Double;
begin
dx := Point2.x-Point1.x;
dy := Point2.y-Point1.y;
Result := sqrt((dx*dx)+(dy*dy));
end;

function IntegerToPrecise(X,Y:Integer):TPrecisePoint;
begin
Result.x := X;
Result.y := Y;
end;

function TPointToFanPOint(TPt : TPoint):FanPoint;
begin
Result.x := TPt.X;
Result.z := TPt.Y;
end;

function FanToTPoint(FanP : Fanpoint):TPoint;
begin
Result.x := FanP.X;
Result.y := FanP.z;
end;

function AngleBetweenPrecisePoints2DInDegrees(Point1,Point2 : TPrecisePoint): Double;
var
dx,dy : Double;
begin
dx := Point2.x-Point1.x;
dy := Point2.y-Point1.y;
Result := 0;
try
if(dx = 0)and(dy=0) then
    begin
    exit;
    end;
if dx = 0 then
   begin
   if dy > 0 then Result := 0 else Result := 180;
   exit;
   end;
if dy = 0 then
   begin
   if dx > 0 then Result := 90 else Result := -90;
   end
else
    Result := RadToDeg(arcTan(dx/dy));
finally
if (dx>0) and (dy<0) and (Result < 0) then Result := 180+Result;
if (dx<0) and (dy<0) and (Result > 0) then Result := Result-180;
end;
end;

function AngleBetweenPoints2D(Point1,Point2 : Tpoint): Double;
var
dx,dy : Double;
begin
dx := Point2.x-Point1.x;
dy := Point2.y-Point1.y;
Result := arcTan(dx/dy);
// N.B. Conditioning required to get result between 0 and 360
end;


function PreciseLineLength3D(Point1,Point2 : TIntLocation): Double;
var
dx,dy,dz : Integer;
begin
dx := abs(Point2.x-Point1.x);
dy := abs(Point2.y-Point1.y);
dz := abs(Point2.z-Point1.z);

Result := sqrt((dx*dx)+(dy*dy)+(dz*dz));
end;

function LineLength3D(Point1,Point2 : TIntLocation): Integer;
var
dx,dy,dz : Integer;
begin
dx := Point2.x-Point1.x;
dy := Point2.y-Point1.y;
dz := Point2.z-Point1.z;

Result := Round(sqrt((dx*dx)+(dy*dy)+(dz*dz)));
end;

function TopMostofTwo(Point1,POint2:TPrecisePoint):TprecisePoint;
begin
if Point1.y > Point2.y then
   begin
   Result := Point2;
   end
else
   begin
   Result := Point1;
   end

end;
function LeftMostofTwo(Point1,POint2:TPrecisePoint):TprecisePoint;
begin
if Point1.x < Point2.x then
   begin
   Result := Point1;
   end
else
   begin
   Result := Point2;
   end

end;


function RightMostofTwo(Point1,POint2:TPrecisePoint):TprecisePoint;
begin
if Point1.x > Point2.x then
   begin
   Result := Point1;
   end
else
   begin
   Result := Point2;
   end

end;

function RightMost(Point1,Point2,Point3: TPoint): TPoint;
begin
if Point1.x > Point2.x then
   begin
   if Point1.x > Point3.x then
      begin
      Result := Point1;
      end
   else
       begin
       Result := Point3;
       end
   end
else
    begin
    If Point2.x > Point3.x then
       begin
       Result := Point2;
       end
    else
        begin
        Result := Point3;
        end
    end;

end;


function LeftMost(Point1,Point2,Point3: TPoint): TPoint;
begin
if Point1.x < Point2.x then
   begin
   if Point1.x < Point3.x then
      begin
      Result := Point1;
      end
   else
       begin
       Result := Point3;
       end
   end
else
    begin
    If Point2.x < Point3.x then
       begin
       Result := Point2;
       end
    else
        begin
        Result := Point3;
        end
    end;

end;


function PGetDiscVerteces(Centre:TPLocation;Radius :Double;Height : Double):TDiscVerts;
var
PCount : Integer;
Angle : Double;
Increment : Double;
HalfHeight : Double;
FaceCentre : TPlocation;
begin
HalfHeight := Height /2;
Angle := 0.0;
Increment := Pi/9;
FaceCentre := Centre;
FaceCentre.z := FaceCentre.z+HalfHeight;
For PCount := 0 to 17 do
    begin
    Result[PCount] :=PCirclePoint(FaceCentre,Radius,Angle);
    Angle := Angle+Increment;
    end;
FaceCentre.z := FaceCentre.z-Height;
For PCount := 18 to 35 do
    begin
    Result[PCount] :=PCirclePoint(FaceCentre,Radius,Angle);
    Angle := Angle+Increment;
    end;

end;

function PCirclePoint(Centre:TPLocation;Radius : Double;Angle: Double):TPLocation;
begin
    Result.x := Radius*ZCos(Angle);
    Result.x := Result.x+Centre.x;

    Result.y := Radius*ZSin(Angle);
    Result.y := Result.y+Centre.y;
    Result.z := Centre.z;
end;




procedure IntersectPoint(Angle1,Angle2,Offset1,Offset2: Double;y : Integer; var x,z:Integer);
(*
x= (Offset1*tan(A1)-(Offset2*Tan(A2))+(y*tan(A2)/(tanA1)
z= (y-Offset2)*Tan(A2)
*)
var
TanA1,TanA2  : Double;
begin
TanA1 := Tan(RadToDeg(Angle1));
TanA2 := Tan(RadToDeg(Angle2));

x:= Round(((Offset1*TanA1)-(Offset2*TanA2)+(y*TanA2))/TanA1);
z:= Round((y-Offset2)*TanA2);
end;

function ZCos(Angle : Double):Double;
begin
Result := Cos(Angle);
if abs(Result) < nearzero then Result := 0.0;
end;

function ZSin(Angle : Double):Double;
begin
Result := Sin(Angle);
if abs(Result) < nearzero then Result := 0.0;
end;


{


A circle is an ellipse with e = 0, a = b = r, coincident foci, and directrices at infinity.

Center radius form of circle equation:

(x-x1)^2 + (y-y1)^2 = r^2 (r > 0).
x1 and y1 are the co-ordianetes of the centre of the circle

A circle (x-x1)2 + (y-y1)2 = r2 and a line Ax + By + C = 0 are tangent if and only if
Delta = r^2(A^2+B^2) - (Ax+By+C)^2 = 0.

The line and circle do not intersect if Delta < 0,
and they intersect in two points if Delta > 0.

In the latter case, the two points of intersection have coordinates
x = (B2x1-ABy1-AC+B sqrt[Delta])/(A2+B2),
y = (A2y1-ABx1-BC-A sqrt[Delta])/(A2+B2)     and
x = (B2x1-ABy1-AC-B sqrt[Delta])/(A2+B2),
y = (A2y1-ABx1-BC+A sqrt[Delta])/(A2+B2).}

function TestLineandCircleIntersect(Line : TLineFormula;Circle : TCircleFormula; var Delta : Double):TCircleIntersectType;
var
A2PlusB2,AxPlussByPlusC : Double;


begin
A2PlusB2 :=  Sqr(Line.XVal)+Sqr(Line.YVal);
AxPlussByPlusC := (Line.XVal*Circle.Centre.x)+(Line.YVal*Circle.Centre.y)+Line.Shift;
Delta := (Sqr(Circle.Radius )*A2PlusB2) -(sqr(AxPlussByPlusC));

if abs(Delta) < nearzero then
   begin
   Result := tciTangent;
   end
else
    begin
    if Delta < 0 then
       begin
       Result := tciNone;
       end
    else
       begin
       Result := tciTwopoints;
       end
    end;

end;

{In the latter case, the two points of intersection have coordinates
x = (B2x1-ABy1-AC+B sqrt[Delta])/(A2+B2),
y = (A2y1-ABx1-BC-A sqrt[Delta])/(A2+B2)     and
x = (B2x1-ABy1-AC-B sqrt[Delta])/(A2+B2),
y = (A2y1-ABx1-BC+A sqrt[Delta])/(A2+B2).}

//x1 and y1 are the co-ordianetes of the centre of the circle

function LineandCircleIntersects(Line : TLineFormula;Circle : TCircleFormula; var Intersect1,Intersect2 : TPrecisePoint):TCircleIntersectType;
var
Delta : Double;
B2x1,ABy1,AC : Double;
BrootDelta : Double;
A2y1,ABx1,BC : Double;
ArootDelta : Double;
Denominator : Double;
begin
Result := TestLineandCircleIntersect(Line,Circle,Delta);

case  Result  of
      tciTwopoints:
      begin
      //Intersect1
      //x = (B2x1-ABy1-AC+B sqrt[Delta])/(A2+B2),
      //y = (A2y1-ABx1-BC-A sqrt[Delta])/(A2+B2)     and
      B2x1 := sqr(Line.Yval)*Circle.Centre.x;
      ABy1 := Line.XVal*Line.Yval*Circle.Centre.y;
      AC   := Line.XVal*Line.Shift;
      BRootDelta := Line.Yval* sqrt(Delta);
      Denominator := sqr(Line.XVal)+sqr(Line.Yval);
      if abs(Denominator) < nearzero then
         begin
         Intersect1.x := 0;
         Intersect1.y := 0;
         Intersect2.x := 0;
         Intersect2.y := 0;
         exit;
         end
      else
          begin
          Intersect1.x := (B2x1-ABy1-AC+BRootDelta)/Denominator;
          end;
      A2y1 := sqr(Line.XVal)*Circle.Centre.y;
      ABx1 := Line.XVal*Line.Yval*Circle.Centre.x;
      BC   := Line.Yval*Line.Shift;
      ARootDelta := Line.Xval*sqrt(Delta);
      Intersect1.y := (A2y1-ABx1-BC-ARootDelta)/Denominator;

      //Intersect2
      //x = (B2x1-ABy1-AC-B sqrt[Delta])/(A2+B2),
      //y = (A2y1-ABx1-BC+A sqrt[Delta])/(A2+B2).}
      Intersect2.x := (B2x1-Aby1-AC-BRootDelta)/Denominator;
      Intersect2.y := (A2y1-Abx1-BC+ARootDelta)/Denominator;

      end;
      tciTangent:
      begin
      B2x1 := sqr(Line.Yval)*Circle.Centre.x;
      ABy1 := Line.XVal*Line.Yval*Circle.Centre.y;
      AC   := Line.XVal*Line.Shift;
      BRootDelta := Line.Yval* sqrt(Delta);
      Denominator := sqr(Line.XVal)+sqr(Line.Yval);
      if abs(Denominator) < nearzero then
         begin
         Intersect1.x := 0;
         Intersect1.y := 0;
         Intersect2.x := 0;
         Intersect2.y := 0;
         exit;
         end
      else
          begin
          Intersect1.x := (B2x1-ABy1-AC+BRootDelta)/Denominator;
          end;
      A2y1 := sqr(Line.XVal)*Circle.Centre.y;
      ABx1 := Line.XVal*Line.Yval*Circle.Centre.x;
      BC   := Line.Yval*Line.Shift;
      ARootDelta := Line.Xval*sqrt(Delta);
      Intersect1.y := (A2y1-ABx1-BC-ARootDelta)/Denominator;
      Intersect2.x := 0;
      Intersect2.y := 0;
      end;
      tciNone:
      begin
      Intersect1.x := 0;
      Intersect1.y := 0;
      Intersect2.x := 0;
      Intersect2.y := 0;
      end;
end; // case;

end;


end.
