unit FileSelect;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, ACNC_TouchOPPPicker, StdCtrls,Grids;


type

  TFileSelector = class(TForm)
    MessagePanel: TPanel;
    FileDisplay: TEdit;
    procedure FormShow(Sender: TObject);
    procedure TFP1FileSelect(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure TFP1Cancel(Sender: TObject);
    procedure TFP1OCTSelect(Sender: TObject);
    procedure TFP1PartSelect(Sender: TObject);
    procedure TFP1EmptyPartSelect(Sender: TObject);
    procedure TFP1SelectionChange(Sender: TObject);
  private
    { Private declarations }

  public
    { Public declarations }
    SelectedFile : String;
    MessageText  : String;
    BaseDir      : String;
    Filter       : String;
    SelectMode   : TOPPSelectModes;
    SelectOCTifExists : Boolean;
    MustSelectPart    : Boolean;
    MustSelectOCT     : Boolean;

    SelectedOCT       : String;
    SelectedPart      : String;
    SelectedPartPath  : String;
    NoOcts            : Boolean;
    PartCount         : Integer;
    IsDeletedFile     : Boolean;
    DeletedFilename   : String;
    TFP1a: TTouchOPPPicker;
    PSOPPText   : String; // set externally from translator
    PSOCTText   : String; // set externally from translator
    PSPartText  : String;// set externally from translator

    procedure     Update;
    procedure     TestForParts;
    procedure     SetDependencyList(DList : TStringList);

  end;


function RemoveDeleteDate(FileName : String):String;

var
  FileSelector: TFileSelector;

implementation

{$R *.DFM}

procedure TFileSelector.TestForParts;
begin
TFP1a.Filter := Filter;
TFP1a.BaseDirectory := BaseDir;
TFP1a.SelectMode := SelectMode;
TFP1a.Update;
TFP1a.Initialise;
PartCount := TFP1a.PartCount;
end;

procedure TFileSelector.FormShow(Sender: TObject);
begin
TFP1a.Filter := Filter;
TFP1a.BaseDirectory := BaseDir;
TFP1a.SelectMode := SelectMode;
TFP1a.Update;
TFP1a.Initialise;
Left := 200;
Top := 70;
PartCount := TFP1a.PartCount;
if PartCount > 0 then
   begin
   case SelectMode of
     smOPP :
           begin
           MessageText := PSOPPText;
           Width := 750;
           with FileDisplay do
                begin
                Visible := True;
                Top := Self.Height-30;
                Height := 26;
                Left := 2;
                Width := Self.Width-4;
                end;
           end;
     smOCT :
           begin
           MessageText := PSOCTText;
           Width := 750;
           FileDisplay.Visible := False;
           end;
     smPart:
           begin
           MessageText := PSPartText;
           Width := 410;
           FileDisplay.Visible := False;
           end;
     smDependency :
           begin
           MessageText := PSOPPText;
           Width := 410;
           with FileDisplay do
                begin
                Visible := True;
                Top := Self.Height-30;
                Height := 26;
                Left := 2;
                Width := Self.Width-4;
                end;
           end;
     end; //case
   MessagePanel.Caption := MessageText;
   end;
   TFP1a.SetFocus;
end;

function RemoveDeleteDate(FileName : String):String;
var
oppPos : Integer;
begin
Result := Filename;
OppPos := Pos('.OPP-',UpperCase(Filename));
if OppPos > 0 then
      begin
      Result := Copy(Filename,1,OppPos+3);
      end
end;

procedure TFileSelector.TFP1FileSelect(Sender: TObject);
begin
IsDeletedFile := TFP1a.DelDirSelected;
if IsDeletedFile then
   begin
   DeletedFilename := RemoveDeleteDate(TFP1a.Filename);
   end
else
   begin
   DeletedFilename := TFP1a.Filename;
   end;
SelectedFile := TFP1a.Filename;
ModalResult :=mrOk;
end;


procedure TFileSelector.FormCreate(Sender: TObject);
begin
TFP1a := TTouchOPPPicker.Create(Self);
with TFP1a do
     begin
     Parent := Self;
     Width := 750;
     Height := 400;
     Top := 45;
     Left := 5;
     OnCancel := TFP1Cancel;
     OnFileSelect := TFP1FileSelect;
     OnEmptyPartSelect := TFP1EmptyPartSelect;
     OnOCTSelect := TFP1OCTSelect;
     OnPartSelect := TFP1PartSelect;
     OnSelectionChange := TFP1SelectionChange;
     BaseDirectory := 'C:\';
     Filter := '*.*';
     SelectMode := smOPP;
     end;
Width := 760;
Height := 475;
Top := 50;
Left := 50;
TFp1a.Width := 750;
TFP1a.Height := 400;
TFP1a.Top := 45;
TFP1a.Left := 5;
end;

procedure TFileSelector.TFP1Cancel(Sender: TObject);
begin
SelectedFile := TFP1a.Filename;
ModalResult :=mrCancel;
close;
end;


procedure TFileSelector.Update;
begin
TFP1a.Update;
end;

procedure TFileSelector.TFP1OCTSelect(Sender: TObject);
begin
NoOcts := TFP1a.NoOCTs;
SelectedOCT := TFP1a.SelectedOct;
SelectedPart := TFP1a.SelectedPart;
SelectedPartPath := TFP1a.SelectedPartPath+'\'; ;
NoOcts := False;
ModalResult :=mrOk;
end;

procedure TFileSelector.TFP1PartSelect(Sender: TObject);
begin
SelectedPartPath := TFP1a.SelectedPartPath+'\' ;
SelectedPart := TFP1a.SelectedPart;
NoOcts := TFP1a.NoOCTs;
if SelectOCTifExists then
   begin
   MessageText := PSOCTText;
   MessagePanel.Caption := MessageText;
   TFP1a.MustSelectOCT := True;
   TFP1a.SelectMode := smOCT;
   TFP1a.Update;
   TFP1a.Initialise;
   end
else
    begin

    if NoOcts then
       begin
       SelectedOCT :=  '';
       end
    else
       begin
       SelectedOCT :=  TFP1a.SelectedOct;
       end;
    ModalResult :=mrOk;
    end;
end;

procedure TFileSelector.TFP1EmptyPartSelect(Sender: TObject);
begin
SelectedPartPath := TFP1a.SelectedPartPath+'\' ;
SelectedPart := TFP1a.SelectedPart;
NoOcts := True;
ModalResult :=mrOk;
end;


procedure TFileSelector.TFP1SelectionChange(Sender: TObject);
begin
FileDisplay.Text := TFP1a.HighlightedFile;
end;

procedure TFileSelector.SetDependencyList(DList: TStringList);
begin
TFP1a.DependencyList := DList;
end;

end.
