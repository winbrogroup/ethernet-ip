object FileSelector: TFileSelector
  Left = 180
  Top = 317
  BorderIcons = []
  BorderStyle = bsNone
  Caption = 'FileSelector'
  ClientHeight = 479
  ClientWidth = 750
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object MessagePanel: TPanel
    Left = 0
    Top = 0
    Width = 750
    Height = 41
    Align = alTop
    BevelInner = bvLowered
    Caption = 'MessagePanel'
    Color = clInfoBk
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlue
    Font.Height = -19
    Font.Name = 'Arial'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
  end
  object FileDisplay: TEdit
    Left = 0
    Top = 456
    Width = 665
    Height = 21
    ReadOnly = True
    TabOrder = 1
    Text = 'FileDisplay'
  end
end
