unit ACNC_SHeditor;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ComCtrls, extctrls, ACNC_Plusmemo,ACNC_PlusGutter,ACNC_TouchSoftkey;

const
BackGroundColour = clBlack;
NormalFont       = clBlack;
MessageFont      = clBlue;
NameFont         = Clred;
LabelFont        = clGreen;
KeywordFont      = clMaroon;
type
Drect = record
      Left : Integer;
      Top  : Integer;
      Right: Integer;
      Bottom : Integer;
      end;
TWhitespacechars = set of Char;
TNumbers         = set of Char;
TLetterAdresses  = set of Char;
TSOPtions        = (soFwd,soWholeWord,soCaseSensitive);
TSearchSwitches  = set of TSOPtions;

  TSHCursorEdit  = class(TEdit)
  private
  protected
  procedure WndProc(var Message: TMessage); override;
  public
  end;

  TBookMarkPanel = class(TCustomPanel)
  private
  FBM1Pressed   : TNotifyEvent;
  FBM2Pressed   : TNotifyEvent;
  FgBM1Pressed  : TNotifyEvent;
  FgBM2Pressed  : TNotifyEvent;

  procedure BMKeyHandler(Sender: TObject; Button: TMouseButton;Shift: TShiftState; X, Y: Integer);

  public
  BM1    : TTouchSoftkey;
  BM2    : TTouchSoftkey;
  GBM1   : TTouchSoftkey;
  GBM2   : TTouchSoftkey;
  Gutter : TPlusGutter;
  bm1Assigned : Boolean;
  bm2Assigned : Boolean;
  bm1Line : Integer;
  bm2Line : Integer;
  procedure RefreshButtonDisplay;
  procedure Resize; override;
  procedure SetKeys;
  constructor Create(AOwner: TComponent); override;

  published
  property align;
  property BM1Pressed : TNotifyEvent read  FBM1Pressed write FBM1Pressed;
  property BM2Pressed : TNotifyEvent read  FBM2Pressed write FBM2Pressed;
  property GBM1Pressed : TNotifyEvent read FgBM1Pressed write FgBM1Pressed;
  property GBM2Pressed : TNotifyEvent read FgBM2Pressed write FgBM2Pressed;

  end;


  TSHPartProgEdit = class(TPlusMemo)
  private
    { Private declarations }
    FFileName : String;
    FCurrentLine : Integer;
    FCurrentCol  : Integer;
    FKnownWordStart : Integer;
    FKnownWordEnd : Integer;
    FLineChanged   : TNotifyEvent;
    FOnKnownWordChange :TNotifyEvent;
    FFindModeRequest : TNotifyEvent;
    FKnownWord     : String;

    FileLoading    : Boolean;
    LastLine       : INteger;
    TempCanvas       : TPaintBox;
    FFirstVisibleLine: Integer;

    function GetKnownWord(LineNumber,LinePosition : Integer; var KnownWord : String): Boolean;

    procedure DoNewCurrentLine;
    procedure DoKnownWordChange;
//    function GetFirstVisibleLine: Integer;

  protected
    { Protected declarations }
    function GetLineCount:Integer;
    function GetLineSize:Integer;
    procedure SetOnLineChange(Value : TNotifyEvent);
    function GetOnLineChange: TNotifyEvent;
    function GetCurrentLine:Integer;

    function GetKnownWordChange: TNotifyEvent;
    procedure SetKnownWordChange(Value :TNotifyEvent);


    procedure WndProc(var Message: TMessage); override;

  public
    { Public declarations }
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure StartEdit(FileText : String);
    function SearchText(FindStr : String;SOptions : TSearchSwitches): Boolean;
    Procedure GetFormatRectSize(var Rectangle:DRect);
    Procedure SetFormatRectSize(Rectangle:DRect);

  published
    { Published declarations }
    procedure TopofFile;
    procedure BottomofFile;
    function CharPosatLineStart(LineNumber : Integer): Integer;

    property Filename : String read FFileName write FFileName;
    property LineCount : Integer read GetLineCount;
    property CurrentLine : Integer read FCurrentLine;
    property CurrentCol : Integer read FCurrentCol;
//    property FirstVisible : Integer read GetFirstVisibleLine;
    property LineSize     : Integer read GetLineSize;
    property OnLineChange : TNotifyEvent read  GetOnLineChange write SetOnLineChange;
    property OnKnownWordChange   : TNotifyEvent read GetKnownWordChange write SetKnownWordChange;
    property KnownWord : String read FKnownWord;
    property KnownWordStartPos : Integer read FKnownWordStart;
    property KnownWordEndPos : Integer read FKnownWordEnd;
    property OnFindModeSelected : TNotifyEvent read FFindModeRequest write FFindModeRequest;

    property HideSelection;
    property OnChange;
    property Color;
    property Align;


  end;

  TOPPSHEdit = class(TCustomPanel)
  private
  FInhibitUpdate        : Boolean;
  LastWord             : String;
  Gutter               : TPlusGutter;
  StatusPanel          : TPanel;
  StatusBar            : TStatusBar;
  HelpLight            : TPanel;
  FCurrentLine         : Integer;
  LastLine             : Integer;
  FCurrentWord         : String;
  FCurrentWordChanged  : TNotifyEvent;
  DisplayLines         : Integer;
  FShowBMPanel: Boolean;
    FBM1Legend: String;
    FGBM1Legend: String;
    FGBM2Legend: String;
    FBM2Legend: String;
  fExecuteFindMode : TNotifyEvent;
  fFindAgain       : TNotifyEvent;
  fHelpRequested   : TNotifyEvent;
  fEscapeRequested : TNotifyEvent;
  FOnLineChanged   : TNotifyEvent;
  FHelpAvailable: Boolean;
  procedure BM1KeyPressed(Sender : TObject);
  procedure BM2KeyPressed(Sender : TObject);
  procedure gBM1KeyPressed(Sender : TObject);
  procedure gBM2KeyPressed(Sender : TObject);
  procedure DoFindModeRequested(Sender : TObject);
  procedure DoFindAgainRequested(Sender : TObject);
  procedure DoWordWrapChange(Sender : TObject);
  procedure HelpRequested(Sender : TObject);

  procedure SetShowBookMarkPanel(const Value: Boolean);
    procedure SetBM1Legend(const Value: String);
    procedure SetBM2Legend(const Value: String);
    procedure SetGBM1Legend(const Value: String);
    procedure SetGBM2Legend(const Value: String);
    procedure EscapeRequested(Sender: TObject);
    procedure SetHelpAvailable(const Value: Boolean);
  protected
  function GetLines: TStrings;
  function GetFilename: string;
  procedure SetFilename(Value : String);
  procedure LinehasChanged(Sender : TObject);

  public
  EditPage                 : TSHPartProgEdit;
  BookMarkPanel            : TBookMarkPanel;
  constructor Create(AOwner: TComponent); override;
  destructor Destroy; override;
  procedure Resize; override;
  procedure UpdateStatus(Sender : TObject);
  procedure StartEdit( FileText : String); overload;
  procedure StartEdit( FileText : String; POsition : INteger); overload;


  published
    property Lines : TStrings read GetLines;
    property FileName : String read GetFilename write SetFilename;
    property CurrentLine : Integer read FCurrentLine;
    property Font;
    property ShowBookMarkPanel : Boolean read FShowBMPanel write SetShowBookMarkPanel;
    property Visible;
    property BM1Legend : String read FBM1Legend write SetBM1Legend;
    property BM2Legend : String read FBM2Legend write SetBM2Legend;
    property GBM1Legend : String read FGBM1Legend write SetGBM1Legend;
    property GBM2Legend : String read FGBM2Legend write SetGBM2Legend;
    property FindModeRequested : TNotifyEvent read fExecuteFindMode write fExecuteFindMode;
    property FindAgainRequested : TNotifyEvent read fFindAgain write fFindAgain;
    property F1HelpRequested : TNotifyEvent read fHelpRequested write fHelpRequested;
    property EScapeKeyPressed : TNotifyEvent read fEscapeRequested write fEscapeRequested;
    property CurrentWord : String read FCurrentWord;
    property CurrentWordChange : TNotifyEvent read FCurrentWordChanged write FCurrentWordChanged;
    property InhibitUpdate : Boolean read FInhibitUpdate write FInhibitUpdate;
    property IndicateHelpAvailable : Boolean read FHelpAvailable write SetHelpAvailable;
    property OnLineChanged : TNotifyEvent read FOnLineChanged write FOnLineChanged;
  end;


procedure Register;

implementation


constructor TOPPSHEdit.Create(AOwner: TComponent);
var
Buffer : String;
begin
inherited;
InhibitUpdate := False;
Color := clwhite;
FCurrentWord := '';
Caption := 'Loading File';
EditPage := TSHPartProgEdit.Create(Self);
with EditPage do
     begin
     Parent := Self;
     OnLineChange := LineHasChanged;
     OnSelMove := UpdateStatus;
//     ProgressInterval := 500;
//     OnProgress := UpdateProgressBar;
     align := alNone;
     OnFindModeRequest := DoFindModeRequested;
     OnFindAgainRequest := DoFindAgainRequested;
     OnHelpRequest := HelpRequested;
     OnEscapeRequest := EscapeRequested;
     OnWordwrapChange := DoWordWrapChange;
     end;

Gutter := TPlusGutter.create(Self);
with Gutter do
     begin
     Parent := Self;
     Top := 1;
     Left := 1;
     Width := 45;
     PlusMemo := EditPage;
     Align := alLeft;
     Color := clSilver;
     LineNumbers := True;
     ParagraphNumbers := True;
     Font.style := [];
     end;


StatusPanel := TPanel.Create(Self);
with StatusPanel do
     begin
     Parent := Self;
     height := 25;
     align := albottom;
     end;

BookMarkPanel := TBookMarkPanel.Create(Self);
with BookMarkPanel do
     begin
     Parent := Self;
     Align := alBottom;
     Gutter := Self.Gutter;
     Gutter.Bookmarks := True;
     Gutter.ClearBookmarks;
     Visible := False;
     Height := 54;
     BM1Pressed := BM1KeyPressed;
     BM2Pressed := BM2KeyPressed;
     GBM1Pressed := gBM1KeyPressed;
     GBM2Pressed := gBM2KeyPressed;
     end;

StatusBar := TStatusBar.create(Self);
with StatusBar do
     begin
     Parent := StatusPanel;
     Font.Size := 10;
     SizeGrip := False;
     align := alLeft;
     Panels.Add;
     Panels.Add;
     Panels.add;
     Panels.add;
     Panels[0].Width := 70;
     Panels[1].Width := 90;
     Panels[2].Width := 150;
     Panels[3].Width := 350;
     end;

HelpLight := TPanel.Create(Self);
with HelpLight do
  begin
  Parent := StatusPanel;
  align := alLeft;
  Width := 100;
  BevelInner := bvlowered;
  BevelWidth :=1;
  BevelOuter := bvRaised;
  Font.Color := Clgreen;
  Caption := ''
  end;

if assigned (StatusBar.Panels[3]) then
   begin
   if Editpage.WordWrap then
      StatusBar.Panels[2].Text := 'WordWrap ON'
   else
      StatusBar.Panels[2].Text := 'WordWrap OFF'
   end;

end;


destructor TOPPSHEdit.Destroy;
begin
inherited;
end;

procedure TOPPSHEdit.Resize;
begin
inherited;
// First determine Line size from current Font
Editpage.Font := Font;
EditPage.Width := Width-10-Gutter.Width;
EditPage.Top := 0;
if FShowBMPanel then
   begin
   BookMarkPanel.Visible := True;
   BookMarkPanel.RefreshButtonDisplay;
   BookMarkPanel.Top := StatusPanel.Top-55;
   BookMarkPanel.Left := StatusPanel.Left;
   BookMarkPanel.Width := StatusPanel.Width;
   EditPage.Height := Height- BookMarkPanel.Height-4;
   end
else
    begin
    BookMarkPanel.Visible := False;
    EditPage.Height := Height- 4;
    end;

DisplayLines := (Height-26) div EditPage.LineSize;
StatusBar.Width := StatusPanel.Width -(HelpLight.Width+4);
end;


procedure TOPPSHEdit.UpdateStatus(Sender : TObject);
var
Buffer : String;
begin
if InhibitUpdate then exit;
if assigned(StatusBar.Panels[0]) then
   begin
   StatusBar.Panels[0].Text := Format(' %d : %d',[EditPage.SelLine+1,EditPage.SelCol+1]);
   end;
if assigned(StatusBar.Panels[1]) then
   begin
   StatusBar.Panels[1].Text := Format('Lines = %d',[EditPage.LineCount]);
   end;

If EditPage.SelLine <> LastLine then
  begin
  LastLine := EditPage.SelLine;
  FCurrentLine := EditPage.SelLine;
  if assigned(FOnLineChanged) then FOnLineChanged(Self);
  end;

if EditPage.GetKnownWord(EditPage.SelLine,EditPage.SelCol,Buffer) then
        begin
        FCurrentWord := Buffer;
        if assigned(StatusBar.Panels[3]) then StatusBar.Panels[3].Text := Buffer;
        end
else
        begin
        FCurrentWord := '';
        if assigned(StatusBar.Panels[3]) then StatusBar.Panels[3].Text := '';
        end;

if CurrentWord <> LastWord then
          begin
          if assigned(FCurrentWordChanged) then FCurrentWordChanged(Self);
          end;

end;

procedure TOPPSHEdit.LinehasChanged(Sender : TObject);
//var
//ScrollFirst : Integer;
//DistFromTop : Integer;
begin
FCurrentLine := EditPage.CurrentLine;
if assigned(FOnLineChanged) then FOnLineChanged(Self);
end;

function TOPPSHEdit.GetLines: TStrings;
begin
Result := EditPage.Lines;
end;

function TOPPSHEdit.GetFilename: string;
begin
Result := EditPage.Filename;
end;
procedure TOPPSHEdit.SetFilename(Value : String);
begin
EditPage.Filename := Value;
end;

{procedure TOPPSHEdit.LoadRawFile(FileList : TStringList);
begin
EditPage.LoadRawFile(FileList);
EditPage.Modified := False;
end;}

procedure TOPPSHEdit.StartEdit( FileText : String);
begin
//Progress.Position := 5;
EditPage.StartEdit(FileText);
end;


procedure TOPPSHEdit.StartEdit( FileText : String; POsition : INteger);
begin
EditPage.StartEdit(FileText);
Editpage.SelStart := Position;
EditPage.ScrollInView;
end;

{******************************
******************************
******************************
******************************}
constructor TSHPartProgEdit.Create(AOwner: TComponent);
begin
inherited;
//Height := 100000; // ensure aLL LINES SYNTAX HIGHLIGHTED BY INCREASING PHYSICAL SIZE
FCurrentLine := 0;
Font.Size := 14;
Font.Name := 'Courier New';
Font.Color := NormalFont;
Font.style := [fsbold];
AltFont.Color := clred;
AltFont.Size := 14;
AltFont.Name := 'Courier New';
LastLine := -1;
Scrollbars := ssVertical;
FileLoading :=False;
Brush.Color := ClWhite;

end;

destructor TSHPartProgEdit.Destroy;
begin
inherited;
end;

function TSHPartProgEdit.GetLineSize:Integer;

begin
TempCanvas := TPaintBox.Create(Self);
try
TempCanvas.Parent := Self;
{with TempCanvas.Canvas do
     begin
     Font.Size := 14;
     Font.style := [fsbold];
     Font.Name := 'Tahoma';
     end;
TempCanvas.Font.Assign(Self.Font);}
TempCanvas.Canvas.Font.Size := Font.Size;
TempCanvas.Canvas.Font.style :=Font.style;
TempCanvas.Canvas.Font.Name := Font.Name;
Result := TempCanvas.Canvas.TextHeight('123');
finally
TempCanvas.Free;
end;
end;

function TSHPartProgEdit.GetLineCount:Integer;
begin
Result := Lines.Count;
end;

function TSHPartProgEdit.GetCurrentLine:Integer;
begin
Result := SelLine;
end;

function TSHPartProgEdit.CharPOsatLineStart(LineNumber : Integer): Integer;
var
LineCount,CharCount : Integer;
LineLength          : Integer;
begin
if (LineNumber < 0) or (LineNumber > Lines.Count) then
   begin
   result := -1;
   exit;
   end;
CharCount := 0;
For LineCount := 0 to LineNumber do
    begin
    LineLength := Length(Lines[LineCount]);
    CharCount := CharCount+ LineLength+2;
//    inc(CharCount,Length(Lines[LineCount]));
    end;
Result := CharCount;
end;

Procedure TSHPartProgEdit.GetFormatRectSize(var Rectangle:DRect);
begin
perform(EM_GETRECT,0,Integer(@Rectangle));
end;

Procedure TSHPartProgEdit.SetFormatRectSize(Rectangle:DRect);
begin
perform(EM_SETRECTNP,0,Integer(@Rectangle));
end;


{procedure TSHPartProgEdit.Change;
var
Buffer : String;
begin
inherited;
If FileLoading then exit;
//CursorDataForPOsition(SelStart,FCurrentLine,FCurrentCol);
if GetKnownWord(FCurrentLine,FCurrentCol+1,Buffer) then
        begin
        if Buffer <> FKnownWord then
                begin
                FKnownWord := Buffer;
                DoKnownWordChange;
                end;
        end
else
        begin
        if FKnownWord <> '' then
                begin
                FKnownWord := '';
                DoKnownWordChange;
                end;
        end;
inherited;
end;}



// this function is passed a contiguos String without white space and searches
// for Letterfolowed by numbers starting from a position within then string

{function TSHPartProgEdit.EmbeddedCode(Line : String;Posn : Integer;var Code : String):Boolean;
var
StartPos : Integer;
CodeStart : Integer;
Found : Boolean;
CodePos : Integer;
Numerics : TNumbers;

begin
Numerics  := ['0'..'9','.'];
LetterAdresses   := ['A'..'Z','a'..'z'];
Result := False;
Code := '';
StartPos := Posn;
CodePos := StartPos;
Found := False;
if Line[StartPos] in LetterAdresses then
    begin
    If StartPos > 1 then
       begin
       if Line[StartPos-1] in LetterAdresses then
          begin
          Result := False;
          exit
          end;
       end;
    CodeStart := StartPos;
    if not (Line[StartPos+1] in Numbers) then
       begin
       Result := False;
       exit
       end;
    CodePos := StartPos+1;
    while (CodePos <= Length(Line)) and (Line[CodePos] in Numerics) do
          begin
          inc(CodePos);
          end;
    Code := Copy(Line,CodeStart,CodePos-CodeStart);
    Result := True;
    end
else
    begin
    // Here to start on a  Number
    if Line[StartPos] in Numerics then
      begin
      if StartPos = 1 then
         begin
         Result := False;
         exit
         end;
      Found := False;
      while (CodePos >= 1) and (not Found) do
            begin
            if Line[CodePos] in LetterAdresses then
               begin
               CodeStart := CodePos;
               Found := True;
               end;
            dec(CodePos);
            end;
      if (CodeStart>1) and (Line[CodeStart-1] in LetterAdresses) then
          begin
          Result := False;
          exit
          end;
      CodePos := CodeStart+1;
      while (CodePos <= Length(Line)) and (Line[CodePos] in Numerics) do
            begin
            inc(CodePos);
            end;
      Code := Copy(Line,CodeStart,CodePos-CodeStart);
      Result := True;
      end;
    end;
end;}


function TSHPartProgEdit.GetKnownWord(LineNumber,LinePosition : Integer; var KnownWord : String): Boolean;
var
Line : String;
LinePos  : Integer;
StartOffset : Integer;
EndOffset : Integer;

//StartPos : Integer;
Found : Boolean;
WordStart : Integer;
WordEnd : Integer;
CommentStart : Integer;
Buffer       : String;
Whitespacechars : Set of char;
Numbers :Set of char;
begin
Whitespacechars := [',',' ','(',')',';','<','>',':','=','/','\','+','-','[',']'];
Numbers  := ['0'..'9','.'];
StartOffset := 0;
EndOffset := 0;
Line := Lines[LineNumber];
if Length(Line) <=1 then
        begin
        Result := False;
        exit;
        end;
KnownWord := '';
//StartPOs := LinePosition;
LinePOs := LinePosition;
Found := False;
WordStart := 1;
CommentStart :=  Pos(';',Line);
If LinePosition > Length(Line) then
        begin
        KnownWord := '';
        Result := False;
        exit;
        end;
if (CommentStart > 0) and (LinePosition >= CommentStart) then
        begin
        // Here if cursor in a comment!!!
        KnownWord := '';
        Result := False;
        exit;
        end;
If Line[LinePosition] in Whitespacechars then
        begin
        KnownWord := '';
        Result := False;
        exit;
        end;

try
While (LinePos > 0) and (not Found) do
   begin
   // Find start of potential Word
   if (Line[LinePos] in Whitespacechars) or (Line[LinePos] = #9) then
        begin
        Found := True;
        WordStart := LinePos+1;
        Inc(StartOffset);
        end
   else
        begin
        if (LinePOs <= 1) then
          begin
          Found := True;
          WordStart := LinePos;
          end
        end;
   dec(LinePos);
   dec(StartOffset);
   end;
except
Result := False;
KnownWord := 'Except';
exit;
end; // try except;

LinePos := WordStart;
FKnownWordStart := SelStart+StartOffset;
Found := False;
WordEnd := WordStart;
try
While (LinePos <= Length(Line)) and (not Found) do
   begin
   // Find end of potential Word
   if (Line[LinePos] in Whitespacechars) or (Line[LinePos] = #9) then
        begin
        Found := True;
        WordEnd := LinePos-1;
        end
   else
        begin
        if  LinePOs >= Length(Line) then
                begin
                Found := True;
                WordEnd := LinePos;
                end;
        end;
   inc(LinePos);
   inc(EndOffset);
   end;
except
Result := False;
KnownWord := 'Except';
exit;
end; // try except;

KnownWord := Copy(Line,WordStart,(WordEnd-WordStart)+1);

//FKnownWordStart := WordStart;
FKnownWordEnd := FKnownWordStart+Length(Trim(Knownword));

{if EmbeddedCode(KnownWord,LinePOsition-WordStart,Buffer) then
   begin
   KnownWord := Buffer;
   end;}
Result := True;
end;


{
procedure TSHPartProgEdit.InitLineFont(LineNumber: Integer);
var
Buffer : String;
begin
if LineNumber > Lines.Count then exit;
if LineNumber < 0 then exit;
try
Buffer := Lines[LineNumber];
SelStart := perform(EM_LINEINDEX,LineNumber,0);
SelLength := length(Buffer);
//SelAttributes.Color := NormalFont;
//SelAttributes.Style := [fsbold];

finally
//InitSelAttributes;
end;
end;
 }
procedure TSHPartProgEdit.TopofFile;
begin
SelLine := 0;
ScrollInView;
end;

procedure TSHPartProgEdit.BottomofFile;
begin
SelLine := Lines.Count-1;
ScrollInView;
end;


procedure TSHPartProgEdit.StartEdit(FileText : String);
begin
Lines.Clear;
align := alclient; // RESIZE TO BOUNDS OF WINDOW
Visible := True;
Lines.Text := FileText;
Modified := False;
SelStart := 0;
end;


function TSHPartProgEdit.SearchText(FindStr : String;SOptions : TSearchSwitches): Boolean;
var
Fwd,Whole,CaseSense : Boolean;
begin
Fwd := soFwd in SOPtions;
Whole := soWholeWord in SOptions;
CaseSense := soCaseSensitive in SOptions;
Result  := FindText(FindStr,Fwd,CaseSense,Whole);
ScrollInView;
end;

function  TSHPartProgEdit.GetKnownWordChange: TNotifyEvent;
begin
Result := FOnKnownWordChange;
end;

procedure TSHPartProgEdit.SetKnownWordChange(Value :TNotifyEvent);
begin
FOnKnownWordChange := Value;
end;


procedure TSHPartProgEdit.SetOnLineChange(Value : TNotifyEvent);
begin
FLineChanged := Value;
end;

function TSHPartProgEdit.GetOnLineChange: TNotifyEvent;
begin
Result := FLineChanged;
end;

procedure TSHPartProgEdit.WndProc(var Message: TMessage);
begin

if Message.Msg = WM_KEYDOWN then
   begin
   case Message.WParam of
        VK_F13:Message.WParam := VK_UP;
        VK_F14:Message.WParam := VK_DOWN;
        VK_F15:Message.WParam := VK_LEFT;
        VK_F16:Message.WParam := VK_RIGHT;
        end; //case

   end;

if Message.Msg = WM_KEYUP then
   begin
   case Message.WParam of
        VK_F13:Message.WParam := VK_UP;
        VK_F14:Message.WParam := VK_DOWN;
        VK_F15:Message.WParam := VK_LEFT;
        VK_F16:Message.WParam := VK_RIGHT;
        end; //case
   end;

inherited;
end;


procedure TSHPartProgEdit.DoKnownWordChange;
begin
if assigned(FOnKnownWordChange) then FOnKnownWordChange(Self);
end;


procedure TSHPartProgEdit.DoNewCurrentLine;
begin
if assigned(FLineChanged) then FLineChanged(Self);
end;

{
function TSHPartProgEdit.UPos(SubStr,InputStr : String):Integer;
var
Buffer : String;

begin
Buffer := AnsiUpperCase(InputStr);
Result := POs(SubStr,Buffer);
if Result > 0 then exit;
Buffer := AnsiLowerCase(InputStr);
Result := Pos(Substr,Buffer);

end;
}

procedure TSHCursorEdit.WndProc(var Message: TMessage);
begin

if Message.Msg = WM_KEYDOWN then
   begin
   case Message.WParam of
        VK_F13:Message.WParam := VK_UP;
        VK_F14:Message.WParam := VK_DOWN;
        VK_F15:Message.WParam := VK_LEFT;
        VK_F16:Message.WParam := VK_RIGHT;
        end; //case
   end;

if Message.Msg = WM_KEYUP then
   begin
   case Message.WParam of
        VK_F13:Message.WParam := VK_UP;
        VK_F14:Message.WParam := VK_DOWN;
        VK_F15:Message.WParam := VK_LEFT;
        VK_F16:Message.WParam := VK_RIGHT;
        end; //case
   end;

inherited;
end;


procedure Register;
begin
  RegisterComponents('ACNCComps', [TSHPartProgEdit]);
  RegisterComponents('ACNCComps', [TOPPSHEdit]);
  RegisterComponents('ACNCComps', [TSHCursorEdit]);
end;

{ TBookMarkPanel }


procedure TBookMarkPanel.BMKeyHandler(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
if Sender = BM1 then
   begin
   if Gutter.BookmarksArray[1] <> Gutter.PlusMemo.SelLine then
      begin
      Gutter.BookmarkLine(Gutter.PlusMemo.SelLine,1);
      end;
   RefreshButtonDisplay;
   if assigned(FBM1Pressed) then FBM1Pressed(Self);
   end;

if Sender = BM2 then
   begin
   if Gutter.BookmarksArray[2] <> Gutter.PlusMemo.SelLine then
      begin
      Gutter.BookmarkLine(Gutter.PlusMemo.SelLine,2);
      end;
   RefreshButtonDisplay;
   if assigned(FBM2Pressed) then FBM2Pressed(Self);
   end;

if Sender = GBM1 then
   begin
   if Gutter.BookmarksArray[1] <> -1 then
      begin
      Gutter.JumpToBookmark(1);
      end;
   if assigned(FGBM1Pressed) then FGBM1Pressed(Self);
   end;
if Sender = GBM2 then
   begin
   if Gutter.BookmarksArray[2] <> -1 then
      begin
      Gutter.JumpToBookmark(2);
      end;
   if assigned(FGBM2Pressed) then FGBM2Pressed(Self);
   end;

end;

constructor TBookMarkPanel.Create(AOwner: TComponent);
begin
  inherited;

  BM1  := TTouchSoftkey.Create(self);
  with BM1 do
       begin
       Parent := Self;
       OnMouseUp := BMKeyHandler;
       visible := False;
       end;
  BM2  := TTouchSoftkey.Create(self);
  with BM2 do
       begin
       Parent := Self;
       OnMouseUp := BMKeyHandler;
       end;
  GBM1 := TTouchSoftkey.Create(Self);
  with GBM1 do
       begin
       Parent := Self;
       OnMouseUp := BMKeyHandler;
       visible := False;
       end;
  GBM2 := TTouchSoftkey.Create(self);
  with GBM2 do
       begin
       Parent := Self;
       OnMouseUp := BMKeyHandler;
       visible := False;
       end;
end;


procedure TBookMarkPanel.RefreshButtonDisplay;
begin
if assigned (Gutter) then
   begin

   BM1.Visible := True;
   BM2.Visible := Gutter.BookmarksArray[1] >= 0;
   GBM1.Visible := BM2.Visible;
   GBM2.Visible := Gutter.BookmarksArray[2] >= 0;
//   SetKeys;
   Resize;
   end;
end;

procedure TBookMarkPanel.Resize;
begin
  inherited;
  BM1.Width := Width div 8;
  BM2.Width := BM1.Width;
  GBM2.Width := BM1.Width;
  GBM1.Width := BM1.Width;

  Bm1.Height := Height-4;
  Bm1.Top := 2;

  BM2.Height := BM1.Height;
  GBM2.Width := BM1.Width;
  GBM1.Width := BM1.Width;
  GBM2.Height := BM1.Height;
  GBM1.Height := BM1.Height;

  Bm1.Left := 2;
  Bm2.Left := Bm1.Left + BM1.Width+1;
  GBm1.Left := Bm2.Left + BM1.Width+1;
  GBm2.Left := GBm1.Left + BM1.Width+1;


end;

procedure TOPPSHEdit.SetShowBookMarkPanel(const Value: Boolean);
begin
  FShowBMPanel := Value;
  Resize;
  EditPage.ScrollInView;
end;


procedure TBookMarkPanel.SetKeys;
begin
  with BM1 do
       begin
       Legend := 'Set 1';
       end;
  with BM2 do
       begin
       Legend := 'Set 2';
       end;
  with GBM1 do
       begin
       Legend := 'Goto 1';
       end;
  with GBM2 do
       begin
       Legend := 'Goto 2';
       end;
end;

procedure TOPPSHEdit.BM1KeyPressed(Sender: TObject);
begin

end;

procedure TOPPSHEdit.BM2KeyPressed(Sender: TObject);
begin

end;

procedure TOPPSHEdit.gBM1KeyPressed(Sender: TObject);
begin

end;

procedure TOPPSHEdit.gBM2KeyPressed(Sender: TObject);
begin

end;

procedure TOPPSHEdit.SetBM1Legend(const Value: String);
begin
  FBM1Legend := Value;
  BookMarkPanel.BM1.Legend := Value;
end;

procedure TOPPSHEdit.SetBM2Legend(const Value: String);
begin
  FBM2Legend := Value;
  BookMarkPanel.BM2.Legend := Value;
end;

procedure TOPPSHEdit.SetGBM1Legend(const Value: String);
begin
  FGBM1Legend := Value;
  BookMarkPanel.GBM1.Legend := Value;
end;

procedure TOPPSHEdit.SetGBM2Legend(const Value: String);
begin
  FGBM2Legend := Value;
  BookMarkPanel.GBM2.Legend := Value;
end;

procedure TOPPSHEdit.DoFindModeRequested(Sender: TObject);
begin
if assigned(fExecuteFindMode) then fExecuteFindMode(self);
end;

procedure TOPPSHEdit.DoWordWrapChange(Sender: TObject);
begin
if assigned (StatusBar.Panels[2]) then
   begin
   if editpage.WordWrap then
      StatusBar.Panels[2].Text := 'WordWrap ON'
   else
      StatusBar.Panels[2].Text := 'WordWrap OFF'
   end;
end;

procedure TOPPSHEdit.DoFindAgainRequested(Sender: TObject);
begin
if assigned(fFindAgain) then fFindAgain(self);
end;

procedure TOPPSHEdit.HelpRequested(Sender: TObject);
begin
if assigned(fHelpRequested) then fHelpRequested(self);
end;

procedure TOPPSHEdit.EscapeRequested(Sender: TObject);
begin
if assigned(fEscapeRequested) then fEscapeRequested(self);
end;


procedure TOPPSHEdit.SetHelpAvailable(const Value: Boolean);
begin
  FHelpAvailable := Value;
  if FHelpAvailable then
    begin
    HelpLight.Color :=clInfoBk;
    HelpLight.Caption := 'HELP (F1)';
    end
  else
    begin
    HelpLight.Color :=clSilver;
    HelpLight.Caption := '';
    end;

end;

end.
