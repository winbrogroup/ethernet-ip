unit ACNC_TouchFilePicker;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls,filectrl,ACNC_TouchSoftkey,ACNC_TouchGlyphSoftkey,Grids,stdctrls;

resourcestring
DownText = 'Down';
UpText = 'Up';
OPenDirText = 'Open';
OpenFileText = 'Open';
CancelText = 'Cancel';
RootText = 'Root';
DeleteText = 'Deleted';

type
  fpDModes = (fpdDrives,fpdDirectory);
  fpOptions = (fpShowRootButton,fpshowAnyFileButton,fpShowDeletedFilesButton,fpShowFilterButton);
  fpNavoptions = (fpUnrestricted,fpLowerDirectoriesOnly,fpfixedDirectory);


  TButtonOptions = set of fpOptions;

  TTouchFilePicker = class(TPanel)
  private
    { Private declarations }
    FBaseDirectory      : String;
    FFilter             : String;
    FSelectedFile       : String;
    FOnFileSelect       : TNotifyEvent;
    FOnCancel           : TNotifyEvent;
    FOnSelectionChange  : TNotifyEvent;
    FDeleteDirectoryName : String;
    FIsDelDirectory      : Boolean;
    FHighlightedFile     : String;
    FOptions             : TButtonOptions;
    FNOptions            : fpNavoptions;



    DTree               : TDirectoryListBox;
    ListBox             : TFileListBox;
    ListButtonPanel     : TPanel;
    TreeButtonPanel     : TPanel;
    DriveListBox        : TStringGrid;
    RootButton          : TTouchGlyphSoftKey;
    UpDirectoryButton   : TTouchGlyphSoftKey;
    DownDirectoryButton : TTouchGlyphSoftKey;
    UpFileButton        : TTouchGlyphSoftKey;
    DownFileButton      : TTouchGlyphSoftKey;
    OpenFileButton      : TTouchGlyphSoftKey;
    CancelButton        : TTouchGlyphSoftKey;
    FileDisplay         : TEdit;

    DirectoryOpenButton : TTouchGlyphSoftKey;
    DeletedFilesButton  : TTouchGlyphSoftKey;
    AnyFileButton       : TTouchGlyphSoftKey;
    FilterButton        : TTouchGlyphSoftKey;
    DCombo              : TDriveComboBox;
    Drives              : TStringList;
    DriveIndex          : Integer;
    DisplayMode         : fpDModes;
    FButtonWidth: Integer;
    FButtonFont: TFont;
    FListFont: TFont;
    FResult    : TModalResult;


    Procedure TreeButtonClick(Sender : TObject;Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    Procedure TreeMouseUp(Sender : TObject;Button: TMouseButton; Shift: TShiftState; X, Y: Integer);

    Procedure FileButtonClick(Sender : TObject;Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure GetDrivesList(var Drives: TStringList);
    procedure NewDirectoryEvent(Sender: TObject);
    procedure NewFileHighlighted(Sender: TObject);
    procedure DoFileSelected;
    procedure DoCancel;
    procedure SetDeleteDirectoryName(const Value: String);
    procedure SetButtonWidth(const Value: Integer);
    procedure SetButtonFont(const Value: TFont);
    procedure SetListFont(const Value: TFont);
    procedure PositionLeftButons;
    procedure SetButtonVisibility;

    procedure SetOPtions(const Value: TButtonOPtions);
    procedure SetNavigateRestriction(const Value: fpNavoptions);
    procedure ConditionNavigateButtons;

  protected
    { Protected declarations }
    procedure SetBaseDir(Dir : String);
    procedure SetFilter(FString : String);
    procedure SetOnFileSelect(Value: TNotifyEvent);
    procedure SetOnCancel(Value: TNotifyEvent);


    procedure Resize; override;
//    procedure Loaded; override;

  public
    { Public declarations }
    constructor Create(AOwner:TComponent); override;
    destructor Destroy; override;
    procedure Execute;
    procedure TreeUpdate;
    procedure Initialise;

  published
    { Published declarations }
    property BaseDirectory : String read FBaseDirectory write SetBaseDir;
    property Filter        : String read FFilter write SetFilter;
    property Filename      : String read FSelectedFile;
    property OnFileSelect  : TNotifyEvent read FOnFileSelect write SetOnFileSelect;
    property OnCancel      : TNotifyEvent read FOnCancel     write SetOnCancel;
    property OnSelectionChange : TNotifyEvent read FOnSelectionChange write FOnSelectionChange;
    property DelDirectory  : String  read FDeleteDirectoryName write SetDeleteDirectoryName;
    property DelDirSelected: Boolean read FIsDelDirectory;
    property HighlightedFile : String read FHighLightedFile;
    property ButtonWidth : Integer read FButtonWidth write SetButtonWidth;
    property ButtonFont : TFont read FButtonFont write SetButtonFont;
    property ListFont : TFont read FListFont write SetListFont;
    property Result :TModalResult read FResult;
    property ButtonOptions : TButtonOPtions read FOPtions write SetOPtions;
    property NavigateRestrictions : fpNavoptions read FNOptions write SetNavigateRestriction;
    property Visible;

  end;

function SlashSep(const Path, S: String): String;
function WithQuotes(InStr : String):String;
Function StripQuotes(InStr : String):String;
function DirectoriesMatch(Path1,Path2: String):Boolean;
function IsaRootDirectory(Path : String):Boolean;

procedure Register;

implementation
constructor TTouchFilePicker.Create(AOwner:TComponent);

begin
inherited;
Font.Name := 'Comic Sans MS';
Font.Size := 12;
FButtonFont := TFont.Create;
FListFont := TFont.Create;
FButtonFont.Name := 'Comic Sans MS';
FListFont.Name := 'Comic Sans MS';
FButtonFont.Size := 12;
FListFont.Size := 12;
FButtonWidth := 80;
FIsDelDirectory := False;
Width := 650;
Height := 400;
DTree := TDirectoryListBox.Create(Self);
ListBox := TFileListBox.Create(Self);
ListBox.OnChange := NewFileHighlighted;
ListBox.Parent := Self;
ListButtonPanel := TPanel.Create(Self);
ListButtonPanel.Parent := Self;
TreeButtonPanel := TPanel.Create(Self);
TreeButtonPanel.Parent := Self;
FileDisplay := Tedit.Create(Self);
FileDisplay.Parent := Self;
RootButton      := TTouchGlyphSoftKey.Create(Self);
Font.Name := 'Comic Sans MS';
Font.Size := 12;
UpDirectoryButton  := TTouchGlyphSoftKey.Create(Self);
DownDirectoryButton:= TTouchGlyphSoftKey.Create(Self);
DirectoryOpenButton:= TTouchGlyphSoftKey.Create(Self);
DeletedFilesButton := TTouchGlyphSoftKey.Create(Self);
AnyFileButton      := TTouchGlyphSoftKey.Create(Self);
FilterButton       := TTouchGlyphSoftKey.Create(Self);

UpFileButton       := TTouchGlyphSoftKey.Create(Self);
DownFileButton     := TTouchGlyphSoftKey.Create(Self);
OPenFileButton     := TTouchGlyphSoftKey.Create(Self);
CancelButton       := TTouchGlyphSoftKey.Create(Self);

DCombo             := TDriveComboBox.Create(Self);
DCombo.Parent := Self;
DriveListBox       := TStringGrid.Create(Self);
DriveListBox.Parent := Self;
Drives             := TStringList.Create;
Visible := False;
FBaseDirectory := 'c:\';
DisplayMode := fpdDirectory;
FFilter := '*.*';
FOPtions := [fpShowRootButton];
end;

destructor TTouchFilePicker.Destroy;
begin
Drives.Free;
inherited;
end;


procedure TTouchFilePicker.TreeUpdate;
begin
if assigned(DTree) then
   begin
   DTree.Update;
   end;
if assigned(ListBox) then
   begin
   ListBox.Update;
   end;
end;


procedure TTouchFilePicker.Execute;

begin
Dtree.Directory := BaseDirectory;
BringToFront;
FResult := mrNone;

Resize;
if ListBox.Items.Count > 0 then
   ListBox.ItemIndex := 0;
NewFileHighlighted(Self);

Visible := True;
//ShowModal;

end;

Procedure TTouchFilePicker.FileButtonClick(Sender : TObject;Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
case TTouchSoftkey(Sender).tag of

     1:
     begin
     // File Up
     if ListBox.ItemIndex > 0 then
        begin
        ListBox.ItemIndex := ListBox.ItemIndex-1;
        NewFileHighlighted(Self);
        end
     end;

     2:
     begin
     // File Down
     if ListBox.ItemIndex < (ListBox.Items.count-1) then
        begin
        ListBox.ItemIndex := ListBox.ItemIndex+1;
        NewFileHighlighted(Self);
        end;

     end;

     3:
     begin
     // File Open
     FSelectedFile := ListBox.Filename;
     DoFileSelected;
     end;

     4:
     begin
     // File Cancel
     FSelectedFile := '';
     DoCancel;
     end;
end; // case
end;



procedure TTouchFilePicker.ConditionNavigateButtons;
var
UpEnable   : Boolean;
DownEnable : Boolean;
CurrentPath : String;
begin
UpEnable := True;
DownEnable := True;
CurrentPath := Dtree.GetItemPath(Dtree.ItemIndex);

if FNOptions = fpLowerDirectoriesOnly then
   begin
   if DirectoriesMatch(CurrentPath,BaseDirectory) then
      begin
      UpEnable := False;
      end;
   if not IsARootDirectory(BaseDirectory) then
      begin
      RootButton.IsEnabled := False;
      end
   else
      begin
      RootButton.IsEnabled := True;
      end
   end
else
    begin
    if FNOptions = fpfixedDirectory then
          begin
          UpEnable := False;
          DownEnable := False;
          end
    end;
UpDirectoryButton.IsEnabled := UpEnable;
DownDirectoryButton.IsEnabled := DownEnable;
end;


Procedure TTouchFilePicker.TreeButtonClick(Sender : TObject;Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
DriveName : String;
begin
     case TTouchGlyphSoftkey(Sender).tag of
     0:
     begin
     // RootButtom Processing
     case DisplayMode of
          fpdDirectory:
            begin
            DTree.ItemIndex := 0;
            DTree.OpenCurrent;
            ConditionNavigateButtons;
            end;
          fpdDrives:
            begin
            end;
     end;
     end;

     1:
     begin
     //UpButton Processing
     case DisplayMode of
          fpdDirectory:
            begin
            if DTree.ItemIndex > 0 then
               begin
               DTree.ItemIndex := DTree.ItemIndex-1;
               ConditionNavigateButtons;
               //DTree.OpenCurrent;
               end
            else
                begin
                // here if request to up from root
                DriveListBox.Visible := True;
                DisplayMode := fpdDrives;
                RootButton.Enabled := False;
                RootButton.Legend := '';
                SetButtonVisibility;
                Resize;
                end;
            end;

          fpdDrives:
            begin
            if DriveListBox.Row > 0 then
               DriveListBox.Row := DriveListBox.Row-1;
            end;
     end;

     end;

     2:
     begin
     //DownButton Processing
     case DisplayMode of
          fpdDirectory:
            begin
            DTree.ItemIndex := DTree.ItemIndex+1;
            UpDirectoryButton.IsEnabled := True;;
            end;
          fpdDrives:
            begin
            if DriveListBox.Row < (DriveListBox.RowCount-1) then
               DriveListBox.Row := DriveListBox.Row+1;
            end;
     end;
     end;

     3:
     begin
     //OpenButton Processing
     case DisplayMode of
          fpdDirectory:
            begin
            DTree.OpenCurrent;
            NewFileHighlighted(Self);
            end;
          fpdDrives:
            begin
            DriveIndex := DriveListBox.Row;
            DriveName :=  DriveListBox.Cells[0,DriveIndex];
            try
            DTree.Directory := DriveName[1]+':\';
            DriveListBox.Visible := False;
            DTree.Visible := True;
            DisplayMode := fpdDirectory;
            SetButtonVisibility;
            RootButton.Enabled := True;
            RootButton.Legend := RootText + '[' + DriveName[1] + ']';;
            NewFileHighlighted(Self);
            except
            DTree.Directory := 'C:\';
            DriveListBox.Visible := False;
            DTree.Visible := True;
            DisplayMode := fpdDirectory;
            SetButtonVisibility;
            RootButton.Enabled := True;
            RootButton.Legend := RootText + '[C:\]';;
            FileDisplay.Text := 'DRIVE NOT AVAILABLE';
            FileDisplay.Font.Color := clRed;
            OPenFileButton.IsEnabled := False;
            end;
            ListButtonPanel.Visible := True;
            end;
     end;
     end;

     4:
     begin
     // AnyFile Button
     ListBox.Mask := '*.*';
     AnyFileButton.IndicatorOn := True;
     FilterButton.IndicatorOn := False;
     ListBox.ItemIndex := 0;
     end;

     5:
     begin
     // Filter Button
     ListBox.Mask := FFilter;
     AnyFileButton.IndicatorOn := False;
     FilterButton.IndicatorOn := True;
     ListBox.ItemIndex := 0;
     end;

     6:
     begin
     // go to deleted directory Button
     if DirectoryExists(FDeleteDirectoryName) then
        DTree.Directory := FDeleteDirectoryName;
     end;

end; // case
end;


procedure TTouchFilePicker.SetBaseDir(Dir : String);
begin
FBaseDirectory := Dir;
if  csDesigning in ComponentState then exit;
if assigned(DTree) then
   begin
   DTree.Directory := Dir;
   end;
if assigned(ListBox) then
   begin
   ListBox.Directory := Dir;
   end;
Initialise;
end;

procedure TTouchFilePicker.SetFilter(FString : String);
begin
FFilter := FString;
end;


procedure TTouchFilePicker.GetDrivesList(var Drives: TStringList);
var
Buffer : array [0..100] of Char;
BCount : Integer;
NPos   : Integer;
StringBuffer : String;
EndofData    : Boolean;

begin
if not assigned(Drives) then exit;
GetLogicalDriveStrings(100,Buffer);
Drives.Clear;
BCount := 0;
NPos := 0;
EndofData := False;
StringBuffer := '';
while (BCount < 100) and not EndofData do
      begin
      While Buffer[BCount+Npos] <>  #0  do
            begin
            StringBuffer:= StringBuffer+ Buffer[BCount+Npos];;
            inc(NPos);
            end;
      Drives.Add(StringBuffer);
      StringBuffer := '';
      inc(NPos);
      if Buffer[BCount+Npos] = #0 then
         begin
         EndofData := True;
         end
      else
          begin
          BCount := BCount+NPos;
          Npos := 0;
          end;
      end;

end;


procedure TTouchFilePicker.NewDirectoryEvent(Sender: TObject);

begin
  ListBox.Directory := DTree.Directory;
  ListBox.ItemIndex := 0;
  NewFileHighlighted(Self);
  if FDeleteDirectoryName = '' then exit;
  if DirectoriesMatch(DTree.Directory,FDeleteDirectoryName) then
     begin
     FIsDelDirectory := True;
     end
  else
     begin
     FIsDelDirectory := False;
     end;
end;



procedure TTouchFilePicker.SetOnCancel(Value: TNotifyEvent);
begin
FOnCancel := Value;
end;

procedure TTouchFilePicker.SetOnFileSelect(Value: TNotifyEvent);
begin
FOnFileSelect := Value;
end;



procedure TTouchFilePicker.DoFileSelected;
begin
if assigned(FOnFileSelect) then FOnFileSelect(Self);
Visible := False;
FResult := mrOK;
end;



procedure TTouchFilePicker.DoCancel;
begin
if assigned(FOnCancel) then FOnCancel(Self);
Visible := False;
FResult := mrCancel;
end;





function SlashSep(const Path, S: String): String;
begin
  if AnsiLastChar(Path)^ <> '\' then
    Result := Path + '\' + S
  else
    Result := Path + S;
end;

function WithQuotes(InStr : String):String;
var
SLength : Integer;
begin
Result := InStr;
if InStr = '' then exit;
Slength := Length(InStr);
if Result[Slength] <> '''' then
   begin
   Result  := Instr + '''';
   end;
if Result[1] <> '''' then
   begin
   Result := '''' + Result ;
   end;
end;

Function StripQuotes(InStr : String):String;
var
SLength : Integer;
Temp    : String;
begin
Temp := InStr;
Result := Instr;
if InStr = '' then exit;
Slength := Length(InStr);
if Temp[Slength] = '''' then
   begin
   Result  := Copy(Temp,1,Slength-1);
   end;
Temp := Result;
if Result[1] = '''' then
   begin
   Temp := Copy(Result,2,Slength);
   end;
Result := Temp;
end;

function DirectoriesMatch(Path1,Path2: String):Boolean;
var
P1,P2 : String;
begin
P1 := SlashSep(Path1,'');
P2 := SlashSep(Path2,'');

Result := False;

if Comparetext(P1,P2) = 0 then
   begin
   Result := True;
   end;
end;

function IsARootDirectory(Path : String):Boolean;
var
SlashPath : String;
PosColon : Integer;
begin
Result := False;
SlashPath := SlashSep(Path,'');
PosColon := Pos(':',SlashPath);
if (PosColon = 2) and (Length(SlashPath) = 3) then Result := True;
end;

function isLowerDirectory(Path1,Path2: String):Boolean;
var
P1,P2 : String;
begin
P1 := SlashSep(Path1,'');
P2 := SlashSep(Path2,'');

Result := True;
if Length(P1) >= Length(P2) then Result := False;
end;

procedure Register;
begin
  RegisterComponents('ACNCComps', [TTouchFilePicker]);
end;

procedure TTouchFilePicker.NewFileHighlighted(Sender: TObject);
begin
OPenFileButton.IsEnabled := True;
FHighlightedFile := ListBox.FileName;
FileDisplay.Text := HighLightedFile;
FileDisplay.Font.Color := clblack;
FileDisplay.Hint := HighLightedFile;

if assigned(FOnSelectionChange) then
   begin
   FonSelectionChange(Self);
   end;
end;

procedure TTouchFilePicker.SetDeleteDirectoryName(const Value: String);
begin
  FDeleteDirectoryName := Value;
end;


procedure TTouchFilePicker.SetButtonVisibility;
begin

if DisplayMode = fpdDirectory then
   begin
   RootButton.Visible := fpShowRootButton in Foptions;
   AnyFileButton.Visible := fpshowAnyFileButton  in Foptions;
   DeletedFilesButton.Visible := fpShowDeletedFilesButton in Foptions;
   FilterButton.Visible := fpShowFilterButton in Foptions;
   end
else
   begin
   RootButton.Visible := False;
   AnyFileButton.Visible := False;
   DeletedFilesButton.Visible := False;
   FilterButton.Visible := False;
   end;

end;

procedure   TTouchFilePicker.PositionLeftButons;
var
CurrentTop : Integer;
begin
CurrentTop := 2;
if fpShowRootButton in Foptions then
   begin
   RootButton.Top :=CurrentTop;
   CurrentTop := CurrentTop+50;
   end;
UpDirectoryButton.Top := CurrentTop;
CurrentTop := CurrentTop+50;

DownDirectoryButton.Top := CurrentTop;
CurrentTop := CurrentTop+50;

DirectoryOpenButton.Top := CurrentTop;
CurrentTop := CurrentTop+50;

if fpshowAnyFileButton in Foptions then
   begin
   AnyFileButton.Top :=CurrentTop;
   CurrentTop := CurrentTop+50;
   end;

if fpShowDeletedFilesButton  in Foptions then
   begin
   DeletedFilesButton.Top :=CurrentTop;
   CurrentTop := CurrentTop+50;
   end;

if fpShowFilterButton   in Foptions then
   begin
   FilterButton.Top :=CurrentTop;
   CurrentTop := CurrentTop+50;
   end
end;

procedure TTouchFilePicker.Resize;
var
BothPanelWidths : Integer;
begin
  inherited;
  with ListButtonPanel do
     begin
     Width := ButtonWidth+2;
     Height := Self.Height-30;
     end;

  with TreeButtonPanel do
     begin
     Width := ButtonWidth+2;
     Height := Self.Height-30;
     end;

  BothPanelWidths := (TreeButtonPanel.Width+ListButtonPanel.Width);

  RootButton.Width := FButtonWidth;
  UpDirectoryButton.Width := FButtonWidth;
  DownDirectoryButton.Width := FButtonWidth;
  UpFileButton.Width := FButtonWidth;
  DownFileButton.Width := FButtonWidth;
  OpenFileButton.Width := FButtonWidth;
  CancelButton.Width := FButtonWidth;
  DirectoryOpenButton.Width := FButtonWidth;
  DeletedFilesButton.Width := FButtonWidth;
  AnyFileButton.Width := FButtonWidth;
  FilterButton.Width := FButtonWidth;

   with FileDisplay do
        begin
        Parent := Self;
        ReadOnly := True;
        AutoSize := False;
        Height := 24;
        align := alBottom;
        Font.Size := 10;
        ShowHint := True;
        end;

  if FNoptions <> fpfixedDirectory then
     begin
     DTree.Visible := True;
     case DisplayMode of
          fpdDirectory:
             begin
             TreeButtonPanel.Visible := True;
             end
          else
             begin
             DTree.Visible := False;
             end;
            end;
     PositionLeftButons;
     with DTree do
          begin
          Width :=  (SElf.Width - BothPanelWidths) div 3;
          Left := TreeButtonPanel.Width+2;
          Height := Self.Height-30;
          end;
     with DriveListBox do
          begin
          Width :=  (SElf.Width - BothPanelWidths) div 3;
          Left := TreeButtonPanel.Width+2;
          Height := Self.Height-30;
          Top := 2;
          end;

     with ListBox do
          begin
          Height := Self.Height-30;
          Width := Self.Width-(TreeButtonPanel.Width+DTree.Width+ListButtonPanel.Width+8);
          Top := 2;
          Left := DTree.Left+DTRee.Width+2;
          Visible := True;
          end;
      ListButtonPanel.Left := ListBox.Left+ListBox.Width+2;
      end
  else
      begin
      // Here for Single Directory Mode
      //Only look at file list and display current Directory
      TreeButtonPanel.Visible := False;
      DTree.Visible := False;
      with ListBox do
          begin
          Height := Self.Height-30;
          Width := Self.Width-(ListButtonPanel.Width+4);
          Top := 2;
          Left := 2;
          Visible := True;
          end;
      ListButtonPanel.Left := ListBox.Left+ListBox.Width+2;
      end;

     with DriveListBox do
     begin
     if DisplayMode = fpdDrives then
        begin
        ListButtonPanel.Visible := False;
        Width :=  Self.Width-(TreeButtonPanel.Width);
        Height := Self.Height-30;
        Top := 2;
        Left := TreeButtonPanel.Width+2;
        Colwidths[0] := Width-4;
        end
     else
        begin
        ListButtonPanel.Visible := True;
        end;
     end;
end;


procedure TTouchFilePicker.SetButtonWidth(const Value: Integer);
begin
  FButtonWidth := Value;
  Resize;
end;


procedure TTouchFilePicker.SetButtonFont(const Value: TFont);
begin
FButtonFont.Assign(Value);
RootButton.LegendFont.Assign(Value);
UpDirectoryButton.LegendFont.Assign(Value);
DownDirectoryButton.LegendFont.Assign(Value);
UpFileButton.LegendFont.Assign(Value);
DownFileButton.LegendFont.Assign(Value);
OpenFileButton.LegendFont.Assign(Value);
CancelButton.LegendFont.Assign(Value);
DirectoryOpenButton.LegendFont.Assign(Value);
DeletedFilesButton.LegendFont.Assign(Value);
AnyFileButton.LegendFont.Assign(Value);
FilterButton.LegendFont.Assign(Value);
end;

procedure TTouchFilePicker.SetListFont(const Value: TFont);
begin
FListFont.Assign(Value);
DTree.Font.Assign(Value);
ListBox.Font.Assign(Value);
FileDisplay.Font.Assign(Value);
DriveListBox.Font.Assign(Value);
end;

procedure TTouchFilePicker.Initialise;
var
DriveCount : Integer;
FoundDrive : Boolean;

begin
  inherited;
  FoundDrive := False;
with ListButtonPanel do
     begin
     Parent := Self;
     Top := 2;
     Left := Self.Width -100;
     Font.Name := 'Comic Sans MS';
     Font.Size := 12;
     Font.color := clNavy;
     Visible := True;
     end;

with TreeButtonPanel do
     begin
     Parent := Self;
     Align := alNone;
     Font.Name := 'Arial';
     Font.Size := 14;
     Font.color := clNavy;
     Visible := True;
     end;


with ListBox do
     begin
     Parent := Self;
     Visible := True;
     end;

with DTRee do
     begin
     Parent := Self;
     Visible := True;
     end;

TreeButtonPanel.Visible := True;

with DTree do
     begin
     Parent := Self;
     Top := 2;
     Left := 100;
     Directory := FBaseDirectory;
     Font.Size :=12;
     OnChange := NewDirectoryEvent;
     OnMouseUp := TreeMouseUp;
     end;

with DCombo do
     begin
     Visible := False;
     DriveCount := 0;
     while  (DriveCount < DCombo.Items.Count) and (not FoundDrive) do
            begin
            if DCombo.Items[DriveCount][1] = FBaseDirectory[1] then
               begin
               FoundDrive := True;
               Drive := DCombo.Items[DriveCount][1];
               end;
            inc(DriveCount);
            end;
     end;

with DriveListBox do
     begin
     Font.Size :=12;
     Visible := False;
     FixedCols := 0;
     FixedRows := 0;
     ColCount := 1;
     ColWidths[0] := 200;
     RowCount :=  DCombo.Items.Count;
     Cols[0] := DCombo.Items;
     end;



with RootButton do
     begin
     Parent := TreeButtonPanel;
     left := 2;
     Height := 50;
     Legend := RootText + '[' + DCombo.Drive + ']';
     Tag := 0;
     ButtonColor := clInfoBk;
     KeyStyle := ssRoundedRectangle;
     TextToEdgeBorder := 10;
     Layout := slCentreOverlay;
     OnMouseUp := TreeButtonClick;
     Font.Name := 'Comic Sans MS';
     end;

with UpDirectoryButton do
     begin
     Parent := TreeButtonPanel;
     Height := 50;
     left := 2;
     Legend := UpText;
     Tag := 1;
     OnMouseUp := TreeButtonClick;
     Font.Name := 'Comic Sans MS';
     ButtonColor := clInfoBk;
     KeyStyle := ssRoundedRectangle;
     TextToEdgeBorder := 12;
     Layout := slCentreOverlay;
     end;

with DownDirectoryButton do
     begin
     Parent := TreeButtonPanel;
     Height := 50;
     left := 2;
     Legend := DownText;
     Tag := 2;
     OnMouseUp := TreeButtonClick;
     Font.Name := 'Comic Sans MS';
     ButtonColor := clInfoBk;
     KeyStyle := ssRoundedRectangle;
     TextToEdgeBorder := 12;
     Layout := slCentreOverlay;
     end ;

with DirectoryOpenButton do
     begin
     Parent := TreeButtonPanel;
     AutoTextSize := True;
     left := 2;
     Legend := OPenDirText;
     Height := 50;
     OnMouseUp := TreeButtonClick;
     Tag := 3;
     Font.Name := 'Comic Sans MS';
     ButtonColor := clInfoBk;
     KeyStyle := ssRoundedRectangle;
     TextToEdgeBorder := 12;
     Layout := slCentreOverlay;
     end ;

with AnyFileButton do
     begin
     Parent := TreeButtonPanel;
     Height := 50;
     left := 2;
     Legend := 'Any File';
     OnMouseUp := TreeButtonClick;
     Tag := 4;
     HasIndicator := True;
     Indicatoron := False;
     Font.Name := 'Comic Sans MS';
     ButtonColor := clInfoBk;
     KeyStyle := ssRoundedRectangle;
     TextToEdgeBorder := 10;
     Layout := slCentreOverlay;
     end;

with DeletedFilesButton do
     begin
     Parent := TreeButtonPanel;
     Height := 50;
     left := 2;
     Legend := DeleteText;
     OnMouseUp := TreeButtonClick;
     Tag := 6;
     Font.Name := 'Comic Sans MS';
     ButtonColor := clInfoBk;
     KeyStyle := ssRoundedRectangle;
     TextToEdgeBorder := 12;
     Layout := slCentreOverlay;
     end;

with FilterButton do
     begin
     Parent := TreeButtonPanel;
     Height := 50;
     left := 2;
     Legend := FFilter;
     OnMouseUp := TreeButtonClick;
     Tag := 5;
     HasIndicator := True;
     IndicatorOn := True;
     ListBox.Mask := Filter;
     Font.Name := 'Comic Sans MS';
     ButtonColor := clInfoBk;
     KeyStyle := ssRoundedRectangle;
     TextToEdgeBorder := 12;
     Visible := False;
     end;


with UpFileButton do
     begin
     Parent := ListButtonPanel;
     Height := 50;
     Top :=  2;
     left := 2;
     Legend := UpText;
     OnMouseUp := FileButtonClick;
     Tag := 1;
     HasIndicator := False;
     Font.Name := 'Comic Sans MS';
     ButtonColor := clInfoBk;
     KeyStyle := ssRoundedRectangle;
     TextToEdgeBorder := 12;
     end;

with DownFileButton do
     begin
     Parent := ListButtonPanel;
     Height := 50;
     Top :=  52;
     left := 2;
     Legend := DownText;
     OnMouseUp := FileButtonClick;
     Tag := 2;
     HasIndicator := False;
     Font.Name := 'Comic Sans MS';
     ButtonColor := clInfoBk;
     KeyStyle := ssRoundedRectangle;
     TextToEdgeBorder := 12;
     end;


with OPenFileButton do
     begin
     Parent := ListButtonPanel;
     Height := 50;
     Top :=  102;
     left := 2;
     Legend := OpenFileText;
     OnMouseUp := FileButtonClick;
     Tag := 3;
     HasIndicator := False;
     Font.Name := 'Comic Sans MS';
     ButtonColor := clInfoBk;
     TextToEdgeBorder := 12;
     KeyStyle := ssRoundedRectangle;
     Layout := slCentreOverlay;
     end;

with CancelButton do
     begin
     Parent := ListButtonPanel;
     Height := 50;
     Top :=  152;
     left := 2;
     Legend := CancelText;
     OnMouseUp := FileButtonClick;
     Tag := 4;
     HasIndicator := False;
     Font.Name := 'Comic Sans MS';
     ButtonColor := clInfoBk;
     KeyStyle := ssRoundedRectangle;
     TextToEdgeBorder := 12;
     Enabled := True;
     end;
SetButtonVisibility;
ConditionNavigateButtons;
Resize;
end;
procedure TTouchFilePicker.SetNavigateRestriction(const Value: fpNavoptions);
begin
  FNOptions := Value;
  SetButtonVisibility;
  resize;
end;

procedure TTouchFilePicker.SetOPtions(const Value: TButtonOPtions);
begin
  FOPtions := Value;
  SetButtonVisibility;
  resize;
end;


procedure TTouchFilePicker.TreeMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
SDirectory : String;
begin
SDirectory := Dtree.GetItemPath(Dtree.ItemIndex);
if FNOptions <> fpUnrestricted then
   begin
   if isLowerDirectory(SDirectory,BaseDirectory) then
      begin
      Dtree.Directory := BaseDirectory;
      Dtree.Update;
      end;
   end;
ConditionNavigateButtons;
end;

end.
