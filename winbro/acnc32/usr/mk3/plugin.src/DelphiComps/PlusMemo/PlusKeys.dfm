object DlgKeysEditor: TDlgKeysEditor
  Left = 586
  Top = 90
  ActiveControl = StrGKeys
  BorderIcons = [biMinimize]
  BorderStyle = bsDialog
  Caption = 'Keywords editor'
  ClientHeight = 405
  ClientWidth = 463
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clBlack
  Font.Height = 14
  Font.Name = 'Arial'
  Font.Style = []
  OldCreateOrder = True
  Position = poScreenCenter
  Scaled = False
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 14
  object LblHint: TLabel
    Left = 8
    Top = 176
    Width = 137
    Height = 17
    Alignment = taCenter
    AutoSize = False
    Caption = 'Press Enter or F2 to edit'
    Color = clYellow
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = 12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentColor = False
    ParentFont = False
  end
  object OKBtn: TBitBtn
    Left = 368
    Top = 96
    Width = 81
    Height = 27
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = 11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 0
    Kind = bkOK
    Margin = 2
    Spacing = -1
    IsControl = True
  end
  object CancelBtn: TBitBtn
    Left = 368
    Top = 136
    Width = 81
    Height = 27
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = 11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    Kind = bkCancel
    Margin = 2
    Spacing = -1
    IsControl = True
  end
  object gbTitle: TGroupBox
    Left = 8
    Top = 8
    Width = 345
    Height = 169
    Caption = 'Keywords'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = 12
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 2
    object StrGKeys: TStringGrid
      Left = 10
      Top = 16
      Width = 319
      Height = 137
      ColCount = 1
      DefaultColWidth = 320
      FixedCols = 0
      RowCount = 1
      FixedRows = 0
      Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goDrawFocusSelected, goEditing, goThumbTracking]
      ScrollBars = ssVertical
      TabOrder = 0
      OnEnter = StrGKeysEnter
      OnExit = StrGKeysExit
      OnKeyPress = StrGKeysKeyPress
      OnSelectCell = StrGKeysSelectCell
      OnSetEditText = StrGKeysSetEditText
    end
  end
  object gbOptions: TGroupBox
    Left = 8
    Top = 208
    Width = 441
    Height = 185
    Caption = 'Options for selected keyword'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = 15
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 3
    object ShpForegnd: TShape
      Left = 256
      Top = 104
      Width = 17
      Height = 17
    end
    object ShpBackgnd: TShape
      Left = 256
      Top = 128
      Width = 17
      Height = 17
    end
    object Label1: TLabel
      Left = 16
      Top = 100
      Width = 93
      Height = 15
      Caption = 'Context &number: '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = 15
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object LblMouse: TLabel
      Left = 320
      Top = 108
      Width = 78
      Height = 15
      Caption = 'Mouse &pointer'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = 15
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object ChkBMatchCase: TCheckBox
      Left = 16
      Top = 24
      Width = 89
      Height = 17
      Caption = ' &Match case'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = 15
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      OnClick = ChkBxClick
    end
    object ChkBWholeWords: TCheckBox
      Left = 16
      Top = 48
      Width = 121
      Height = 17
      Caption = ' &Whole words only'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = 15
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      OnClick = ChkBxClick
    end
    object ChkBHighlight: TCheckBox
      Left = 320
      Top = 72
      Width = 81
      Height = 17
      Caption = ' &Highlight'
      Color = clYellow
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = 15
      Font.Name = 'Arial'
      Font.Style = []
      ParentColor = False
      ParentFont = False
      TabOrder = 2
      OnClick = ChkBxClick
    end
    object ChkBAltFont: TCheckBox
      Left = 168
      Top = 72
      Width = 121
      Height = 17
      Caption = ' &Alternate font'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = 15
      Font.Name = 'Courier New'
      Font.Style = []
      ParentFont = False
      TabOrder = 3
      OnClick = ChkBxClick
    end
    object ChkBItalic: TCheckBox
      Left = 168
      Top = 48
      Width = 57
      Height = 17
      Caption = ' &Italic'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = 15
      Font.Name = 'Arial'
      Font.Style = [fsItalic]
      ParentFont = False
      TabOrder = 4
      OnClick = ChkBxClick
    end
    object ChkBBold: TCheckBox
      Left = 168
      Top = 24
      Width = 73
      Height = 17
      Caption = ' &Bold'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = 15
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 5
      OnClick = ChkBxClick
    end
    object ChkBUnderline: TCheckBox
      Left = 320
      Top = 24
      Width = 81
      Height = 17
      Caption = ' &Underline'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = 15
      Font.Name = 'Arial'
      Font.Style = [fsUnderline]
      ParentFont = False
      TabOrder = 6
      OnClick = ChkBxClick
    end
    object ChkBStrikeOut: TCheckBox
      Left = 320
      Top = 48
      Width = 81
      Height = 17
      Caption = ' &Strike out'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = 15
      Font.Name = 'Arial'
      Font.Style = [fsStrikeOut]
      ParentFont = False
      TabOrder = 7
      OnClick = ChkBxClick
    end
    object BtnForegnd: TButton
      Left = 164
      Top = 104
      Width = 84
      Height = 18
      Caption = '&Foreground...'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = 15
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 8
      OnClick = BtnForegndClick
    end
    object BtnBackgnd: TButton
      Left = 164
      Top = 128
      Width = 84
      Height = 18
      Caption = 'Back&ground...'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = 15
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 9
      OnClick = BtnBackgndClick
    end
    object BtnResetColors: TButton
      Left = 164
      Top = 152
      Width = 112
      Height = 20
      Caption = '&Reset to default'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = 15
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 10
      OnClick = BtnResetColorsClick
    end
    object EdContext: TEdit
      Left = 16
      Top = 120
      Width = 73
      Height = 23
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = 15
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 11
      Text = 'EdContext'
      OnExit = EdContextExit
    end
    object cbCursors: TComboBox
      Left = 320
      Top = 128
      Width = 97
      Height = 22
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = 14
      Font.Name = 'Arial'
      Font.Style = []
      ItemHeight = 14
      ParentFont = False
      TabOrder = 12
      Text = 'cbCursors'
      OnChange = cbCursorsChange
    end
    object ChkBCrossPar: TCheckBox
      Left = 16
      Top = 72
      Width = 121
      Height = 17
      Caption = '&Cross paragraphs'
      Checked = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = 15
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      State = cbChecked
      TabOrder = 13
      Visible = False
      OnClick = ChkBxClick
    end
  end
  object BtnRemove: TButton
    Left = 368
    Top = 32
    Width = 81
    Height = 25
    Caption = 'Remo&ve'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = 15
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 4
    OnClick = BtnRemoveClick
  end
  object ColorDialog1: TColorDialog
    Ctl3D = True
    Left = 360
    Top = 152
  end
end
