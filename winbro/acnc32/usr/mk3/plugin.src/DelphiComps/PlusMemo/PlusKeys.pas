unit PlusKeys;
{ TPlusMemo.Keywords and StartStopKeys property editor
  This file must be accompanied by PlusKeys.dfm, which is the form
  resource file for these property editors (TDlgKeysEditor) }

{ � Electro-Concept Mauricie, 1997-1998-1999-2000 }

interface

uses WinTypes, WinProcs, Classes, Graphics, Forms, Controls, Buttons,
  StdCtrls, Grids, ExtCtrls, PlusMemo, {$IFNDEF WIN32} PlusSup, {$ENDIF} Dialogs;

type
  TDlgKeysEditor = class(TForm)
    OKBtn: TBitBtn;
    CancelBtn: TBitBtn;
    gbTitle: TGroupBox;
    StrGKeys: TStringGrid;
    gbOptions: TGroupBox;
    ChkBMatchCase: TCheckBox;
    ChkBWholeWords: TCheckBox;
    ChkBHighlight: TCheckBox;
    ChkBAltFont: TCheckBox;
    ChkBItalic: TCheckBox;
    ChkBBold: TCheckBox;
    ChkBUnderline: TCheckBox;
    ChkBStrikeOut: TCheckBox;
    LblHint: TLabel;
    ColorDialog1: TColorDialog;
    ShpForegnd: TShape;
    BtnForegnd: TButton;
    BtnBackgnd: TButton;
    ShpBackgnd: TShape;
    BtnResetColors: TButton;
    Label1: TLabel;
    EdContext: TEdit;
    BtnRemove: TButton;
    LblMouse: TLabel;
    cbCursors: TComboBox;
    ChkBCrossPar: TCheckBox;
    procedure FormShow(Sender: TObject);
    procedure StrGKeysSelectCell(Sender: TObject; Col, Row: Longint;
      var CanSelect: Boolean);
    procedure StrGKeysSetEditText(Sender: TObject; ACol, ARow: Longint;
      const Value: String);
    procedure ChkBxClick(Sender: TObject);
    procedure BtnRemoveClick(Sender: TObject);
    procedure StrGKeysEnter(Sender: TObject);
    procedure StrGKeysExit(Sender: TObject);
    procedure StrGKeywordsGetEditText(Sender: TObject; ACol, ARow: Longint;
      var Value: OpenString);
    procedure StrGKeysKeyPress(Sender: TObject; var Key: Char);
    procedure BtnForegndClick(Sender: TObject);
    procedure BtnBackgndClick(Sender: TObject);
    procedure BtnResetColorsClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure cbCursorsChange(Sender: TObject);
    procedure EdContextExit(Sender: TObject);
  private
    { Private declarations }
    inSelect: Boolean;
    Modified: Boolean;
    woList: TList;
    procedure AddCursor(const s: string);
  public
    { Public declarations }
  end;

var
  DlgKeysEditor: TDlgKeysEditor;

function EditKeywordList(KeywordList: TKeywordList): Boolean;
function EditStartStopList(StartStopList: TStartStopKeyList): Boolean;

implementation
uses SysUtils, Messages;
{$R *.DFM}

type ByteSet = {$IFDEF BCB} set of 0..7; {$ELSE} TExtFontStyles; {$ENDIF}

function EditKeywordList(KeywordList: TKeywordList): Boolean;
  var i: Integer;
      pk: pKeyInfoLen;
      kw: string;
  begin
  with TDlgKeysEditor.Create(Application) do
    begin
    woList:= TList.Create;
    StrGKeys.RowCount:= KeywordList.Count+1;

    for i:= 0 to KeywordList.Count-1 do
      begin
      new(pk);
      pk^:= pKeyInfoLen(KeywordList.Objects[i])^;
      woList.Add(pk);
      StrGKeys.Cells[0, i]:= KeywordList[i]
      end;
    new(pk);
    if KeywordList.Count>0 then pk^:= pKeyInfoLen(KeywordList.Objects[KeywordList.Count-1])^
    else with pk^.BasicPart do
           begin
           Options:= [];
           Style:= [];
           ContextNumber:= 1;
           Backgnd:= 0; Foregnd:= 0;
           Cursor:= crDefault
           end;
    woList.Add(pk);

    if (ShowModal=mrOk) and Modified then
      with Keywordlist do
        begin
        Clear;
        BeginUpdate;
        for i:= 0 to StrGKeys.RowCount-2 do
          begin
          kw:= StrGKeys.Cells[0,i];
          if kw<>'' then AddObject(kw, woList[i])
                    else Dispose(pKeyInfoLen(woList[i]))
          end;
        EndUpdate;
        Result:= True
      end
    else begin
         for i:= 0 to woList.Count-1 do Dispose(pKeyInfoLen(woList[i]));
         Result:= False
         end;
    woList.Free;
    Free
    end
  end;

function EditStartStopList(StartStopList: TStartStopKeyList): Boolean;
  var i: Integer;
      pk: pKeyInfoLen;
      ssinfo: pStartStopInfo;
      ps: Boolean;
      startkey: string;
  begin
  with TDlgKeysEditor.Create(Application) do
    begin
    Caption:= 'Start-Stop keys Editor';
    StrGKeys.ColCount:= 2;
    StrGKeys.DefaultColWidth:= 130;
    gbTitle.Caption:= '   Start key                    Stop key';
    gbOptions.Caption:= 'Options for selected start-stop key';
    ChkBCrossPar.Show;

    woList:= TList.Create;
    StrGKeys.RowCount:= StartStopList.Count+1;

    for i:= 0 to StartStopList.Count-1 do
      begin
      new(pk);
      ssinfo:= StartStopList.Pointers[i];
      pk^.BasicPart:= ssinfo^.Attributes;
      woList.Add(pk);
      if ssinfo^.ParStop then Byte(pk^.BasicPart.Options):= Byte(pk^.BasicPart.Options) or $80;
      StrGKeys.Cells[0, i]:= StrPas(ssinfo^.StartKey);
      StrGKeys.Cells[1, i]:= StrPas(ssinfo^.StopKey)
      end;
    new(pk);
    if StartStopList.Count>0 then
      begin
      ssinfo:= StartStopList.Pointers[StartStopList.Count-1];
      pk^.BasicPart:= ssinfo^.Attributes;
      if ssinfo^.ParStop then Byte(pk^.BasicPart.Options):= Byte(pk^.BasicPart.Options) or $80;
      end
    else with pk^.BasicPart do
           begin
           Options:= [];
           Style:= [];
           ContextNumber:= 1;
           Backgnd:= 0; Foregnd:= 0;
           Cursor:= crDefault
           end;
    woList.Add(pk);

    if (ShowModal=mrOk) and Modified then
      with StartStopList do
        begin
        Clear;
        for i:= 0 to StrGKeys.RowCount-2 do
          begin
          pk:= woList[i];
          ps:= Byte(pk^.BasicPart.Options) and $80<>0;
          Byte(pk^.BasicPart.Options):= Byte(pk^.BasicPart.Options) and $7f;
          startkey:= StrGKeys.Cells[0,i];
          if startkey<>'' then
            AddStartStopKey(startkey, StrGKeys.Cells[1, i],
                            pk^.BasicPart.Options, pk^.BasicPart.Style, pk^.BasicPart.ContextNumber,
                            pk^.BasicPart.Cursor, pk^.BasicPart.Backgnd, pk^.BasicPart.Foregnd, ps);
          Dispose(pk)
          end;
        Result:= True
        end
    else begin
         for i:= 0 to woList.Count-1 do Dispose(pKeyInfoLen(woList[i]));
         Result:= False
         end;
    woList.Free;
    Free
    end
  end;


procedure TDlgKeysEditor.FormShow(Sender: TObject);
var dummy: Boolean;
begin
  StrGKeysSelectCell(Self, 0, StrGKeys.Row, dummy)
end;

procedure TDlgKeysEditor.StrGKeysSelectCell(Sender: TObject; Col,
  Row: Longint; var CanSelect: Boolean);
begin
CanSelect:= True;
inSelect:= True;   { avoid OnClick updating info }
with pKeyInfoLen(woList[Row])^.BasicPart do
  begin
  ChkBMatchCase.Checked:= woMatchCase in Options;
  ChkBWholeWords.Checked:= woWholeWordsOnly in Options;
  ChkBBold.Checked:= fsBold in Style;
  ChkBItalic.Checked:= fsItalic in Style;
  ChkBUnderline.Checked:= fsUnderline in Style;
  ChkBStrikeOut.Checked:= fsStrikeOut in Style;
  ChkBHighlight.Checked:= fsHighlight in ByteSet(Style);
  ChkBAltFont.Checked:= fsAltFont in ByteSet(Style);
  ChkBCrossPar.Checked:= (Byte(Options) and $80) = 0;
  if (Backgnd=0) and (Foregnd=0) then begin ShpForegnd.Hide; ShpBackgnd.Hide end
  else begin
       ShpForegnd.Brush.Color:= Foregnd;
       ShpBackgnd.Brush.Color:= Backgnd;
       ShpForegnd.Visible:= Foregnd<>-1;
       ShpBackgnd.Visible:= Backgnd<>-1
       end;
  EdContext.Text:= IntToStr(ContextNumber);
  EdContext.Modified:= False;
  LblMouse.Cursor:=Cursor;
  cbCursors.ItemIndex:= cbCursors.Items.IndexOf(CursorToString(Cursor))
  end;

inSelect:= False
end;

procedure TDlgKeysEditor.StrGKeysSetEditText(Sender: TObject; ACol,
  ARow: Longint; const Value: String);
begin
Modified:= True;
LblHint.Show;
end;

procedure TDlgKeysEditor.ChkBxClick(Sender: TObject);
begin
if not inSelect then
  with pKeyInfoLen(woList[StrGKeys.Row])^.BasicPart do
    begin
    Modified:= True;
    if ChkBMatchCase.Checked then Options:= [woMatchCase]
                             else Options:= [];
    if ChkBWholeWords.Checked then Options:= Options + [woWholeWordsOnly];
    if ChkBBold.Checked then Style:= [fsBold]
                        else Style:= [];
    if ChkBItalic.Checked then Style:= Style + [fsItalic];
    if ChkBUnderline.Checked then Style:= Style + [fsUnderline];
    if ChkBStrikeOut.Checked then Style:= Style + [fsStrikeOut];
    {$IFDEF BCB}
    if ChkBHighlight.Checked then ByteSet(Style):= ByteSet(Style) + [fsHighlight];
    if ChkBAltFont.Checked then ByteSet(Style):= ByteSet(Style) + [fsAltFont];
    {$ELSE}
    if ChkBHighlight.Checked then Style:= Style + [TExtFontStyle(fsHighlight)];
    if ChkBAltFont.Checked then Style:= Style + [TExtFontStyle(fsAltFont)];
    {$ENDIF}
    if not ChkBCrossPar.Checked then Byte(Options):= Byte(Options) or $80
                                else Byte(Options):= Byte(Options) and $7f
    end;
end;

procedure TDlgKeysEditor.BtnRemoveClick(Sender: TObject);
var i: Integer;  dummy: Boolean;
var pk: pKeyInfoLen;
begin
with StrGKeys do
  if Row<RowCount-1 then
    begin
    pk:= woList[Row];
    Dispose(pk);
    for i:= Row to RowCount-2 do
        begin
        Rows[i]:= Rows[i+1];
        woList[i]:= woList[i+1];
        end;
    RowCount:= RowCount-1;
    woList.Count:= woList.Count-1;
    StrGKeysSelectCell(Self, 0, Row, dummy);
    Modified:= True
    end
end;

procedure TDlgKeysEditor.StrGKeysEnter(Sender: TObject);
begin
LblHint.Show
end;

procedure TDlgKeysEditor.StrGKeysExit(Sender: TObject);
begin
LblHint.Hide
end;

procedure TDlgKeysEditor.StrGKeywordsGetEditText(Sender: TObject; ACol,
  ARow: Longint; var Value: OpenString);
begin
{LblHint.Hide }
end;

procedure TDlgKeysEditor.StrGKeysKeyPress(Sender: TObject; var Key: Char);
var pk: pKeyInfoLen;
begin
with StrGKeys do
  if Row=RowCount-1 then
    begin
    RowCount:= RowCount+1;
    new(pk);
    woList.Add(pk);
    pk^:= pKeyInfoLen(woList[Row])^
    end;

end;


procedure TDlgKeysEditor.BtnForegndClick(Sender: TObject);
var saved: TColor;
begin
if ShpForegnd.Visible then ColorDialog1.Color:= ShpForegnd.Brush.Color
                      else ColorDialog1.Color:= clBlack;
saved:= ColorDialog1.Color;
if ColorDialog1.Execute then
  if (ColorDialog1.Color<>saved) or (not ShpForegnd.Visible) then
    begin
    ShpForegnd.Brush.Color:= ColorDialog1.Color;
    ShpForegnd.Show;
    with pKeyInfoLen(woList[StrGKeys.Row])^.BasicPart do
      begin
      Foregnd:= ColorDialog1.Color;
      if not ShpBackgnd.Visible then Backgnd:= -1;
      end;
    Modified:= True
    end
end;

procedure TDlgKeysEditor.BtnBackgndClick(Sender: TObject);
var saved: TColor;
begin
if ShpBackgnd.Visible then ColorDialog1.Color:= ShpBackgnd.Brush.Color
                      else ColorDialog1.Color:= clWhite;
saved:= ColorDialog1.Color;
if ColorDialog1.Execute then
  if (ColorDialog1.Color<>saved) or (not ShpBackgnd.Visible) then
    begin
    ShpBackgnd.Brush.Color:= ColorDialog1.Color;
    ShpBackgnd.Show;
    with pKeyInfoLen(woList[StrGKeys.Row])^.BasicPart do
      begin
      Backgnd:= ColorDialog1.Color;
      if not ShpForegnd.Visible then Foregnd:= -1;
      end;
    Modified:= True
    end
end;



procedure TDlgKeysEditor.BtnResetColorsClick(Sender: TObject);
begin
if ShpBackgnd.Visible or ShpForegnd.Visible then
  begin
  with pKeyInfoLen(woList[StrGKeys.Row])^.BasicPart do begin Backgnd:= -1; Foregnd:= -1 end;
  Modified:= True;
  ShpBackgnd.Hide;
  ShpForegnd.Hide;
  end
end;

procedure TDlgKeysEditor.FormCreate(Sender: TObject);
begin
GetCursorValues(AddCursor);
end;

procedure TDlgKeysEditor.AddCursor(const s: string);
begin
cbCursors.Items.Add(s)
end;

procedure TDlgKeysEditor.cbCursorsChange(Sender: TObject);
begin
if not inSelect then
  with pKeyInfoLen(woList[StrGKeys.Row])^.BasicPart do
    begin
    Modified:= True;
    Cursor:= StringToCursor(cbCursors.Items[cbCursors.ItemIndex]);
    LblMouse.Cursor:= Cursor
    end
end;


procedure TDlgKeysEditor.EdContextExit(Sender: TObject);
begin
if EdContext.Modified then
  begin
  with pKeyInfoLen(woList[StrGKeys.Row])^.BasicPart do
    begin
    Modified:= True;
    try ContextNumber:= StrToInt(EdContext.Text)
    except
      on E: Exception do
        begin
        ShowMessage(E.Message);
        EdContext.SetFocus
        end
      end
    end
  end
end;

end.
