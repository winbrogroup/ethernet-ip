unit TouchDot;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls,TouchGlyphSoftkey,Grids;

type

  TTouchDot = class(TImage)
  private
    FOnTouched: TNotifyEvent;
    { Private declarations }


  protected
    { Protected declarations }

    procedure MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer);override;
    procedure MouseUp(Button: TMouseButton; Shift: TShiftState; X, Y: Integer);override;
    procedure DotClick(Sender : TObject);

  public
    { Public declarations }
    constructor Create(AOwner:TComponent); override;
    destructor Destroy; override;

  published
    { Published declarations }
    property OnTouched : TNotifyEvent read FOnTouched write FOntouched;
  end;

procedure Register;

implementation


{ TTouchDot }

constructor TTouchDot.Create(AOwner: TComponent);
begin
  inherited;
  Picture.Bitmap.LoadFromResourceName(HInstance,'ACTIVATORBUTTON');
  Width := Picture.Width;
  Height := Picture.Height;
  Transparent := True;
  OnClick := DotClick;

end;

destructor TTouchDot.Destroy;
begin
  inherited;

end;

procedure TTouchDot.DotClick(Sender: TObject);
begin
if assigned(FOnTouched) then FOnTouched(Self);
end;

procedure TTouchDot.MouseDown(Button: TMouseButton; Shift: TShiftState; X,
  Y: Integer);
begin
  inherited;
end;

procedure TTouchDot.MouseUp(Button: TMouseButton; Shift: TShiftState; X,
  Y: Integer);
begin
  inherited;
end;

procedure Register;
begin
  RegisterComponents('Touch', [TTouchDot]);
end;

end.
