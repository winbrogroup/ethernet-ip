unit ACNC_ThreeLineButton;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls,Commctrl,buttons;

type
  TSoftKeyShapes = (ssRectangle,ssRoundedRectangle,ssElipse);
  TSoftKeyStates = (skUp,skDown);
  TSoftButtonLayout = (slGlyphLeft, slGlyphRight, slGlyphTop, slGlyphBottom,slCentreOverlay);

 TOffsetData = record
    GXoff,GYOff : Integer;
    TextRect : TRect;
    TextBoxWidth : Integer;
    TextBoxHeight : Integer;
    end;

      TGlyphList = class(TImageList)
  private
    Used: TBits;
    FCount: Integer;
    function AllocateIndex: Integer;
  public
    constructor CreateSize(AWidth, AHeight: Integer);
    destructor Destroy; override;
    function AddMasked(Image: TBitmap; MaskColor: TColor): Integer;
    procedure Delete(Index: Integer);
    property Count: Integer read FCount;
  end;

  TGlyphCache = class
  private
    GlyphLists: TList;
  public
    constructor Create;
    destructor Destroy; override;
    function GetList(AWidth, AHeight: Integer): TGlyphList;
    procedure ReturnList(List: TGlyphList);
    function Empty: Boolean;
  end;

  TButtonGlyph = class
  private
    FOriginal: TBitmap;
    FGlyphList: TGlyphList;
    FIndexs: array[TButtonState] of Integer;
    FTransparentColor: TColor;
    FNumGlyphs: TNumGlyphs;
    FOnChange: TNotifyEvent;
    procedure GlyphChanged(Sender: TObject);
    procedure SetGlyph(Value: TBitmap);
    procedure SetNumGlyphs(Value: TNumGlyphs);
    procedure Invalidate;
    function CreateButtonGlyph(State: TButtonState): Integer;
  public
    constructor Create;
    destructor Destroy; override;
    { return the text rectangle }
    procedure DrawButtonGlyph(Canvas: TCanvas; const GlyphPos: TPoint;
      State: TButtonState; Transparent: Boolean);
    property Glyph: TBitmap read FOriginal write SetGlyph;
    property NumGlyphs: TNumGlyphs read FNumGlyphs write SetNumGlyphs;
    property OnChange: TNotifyEvent read FOnChange write FOnChange;
  end;





  TThreeLineButton = class(TCustomControl)
  private
    { Private declarations }
    Offscreen : TBitmap;
    FLegend : String;
    FState  : TSoftKeyStates;
    FlashTimer : TTimer;
    FlashState : Boolean;

    FTextSize : Integer;
    FHasIndicator : Boolean;
    FIndicatorOn  : Boolean;
    FIsEnabled    : Boolean;
    FButtonColor  : TColor;
    FCurrentColour : TColor;
    FGlyph         : Pointer;
    FLayout        : TSoftButtonLayout;
    OffsetData    : TOffsetData;
    FAutoTextSize: Boolean;
    FKeyStyle      : TSoftKeyShapes;
    FCornerRad: Integer;
    FBTravel: Integer;
    FSColour: TColor;
    FLFont: TFont;
    FGTedge: Integer;
    FMultilIneSpacing : Integer;
    FTTEdge: Integer;
    ClearFirst : Boolean;
    Line1,Line2,Line3 : String;
    LongestLine : String;
    LineCount : Integer;
    FFlash: Boolean;
    StandardFontColour : TColor;
    FFlashOn: Integer;
    FFlashOff: Integer;
    FMaxFont: Integer;

    procedure PaintSurface;
    procedure Paint; override;
    procedure DoMyMouseDown(Sender : TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure DoMyMouseUp(Sender : TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure DoMyClick(Sender : TObject);
    procedure SetGlyph(const Value: TBitmap);
    function  GetGlyph: TBitmap;
    procedure SetLayout(const Value: TSoftButtonLayout);
    procedure CalculateGlyphPosition(Glyph : TBitmap;GlyphCount : Integer;layout:TSoftButtonLayout;BState:TSoftKeyStates;var OData: TOffsetData);
    procedure SetAutoTextSize(const Value: Boolean);
    procedure SetKeyStyle(const Value: TSoftKeyShapes);
    procedure SetCornerRad(const Value: Integer);

    procedure DrawBasicShape(BorderWidth : Integer);
    procedure PaintRoundRectangle(OnCanvas: TCanvas; PRect: Trect;
              MColour : TColor ;CornerRadius: Integer);
//    procedure SetTransColor(const Value: TColor);
    procedure SetBTravel(const Value: Integer);
    procedure SetShadowColour(const Value: TColor);
    function SetLegendDimensions(OnCanvas : TCanvas;Legend: String; TextBoxHeight,
      TextBoxWidth: Integer;AutoSize : Boolean): Integer;
    procedure SetFLFont(const Value: TFont);
    Procedure  SetFontHeight(FHeight : Integer);
    procedure SetGlyphToEdgeSpacing(const Value: Integer);
    procedure SetMultiLineSpacing (const Value: Integer);
    procedure SetTextToEdgeBorder(const Value: Integer);
    procedure UpdateFont(Sender : Tobject);
    function GetNumGlyphs: TNumGlyphs;
    procedure SetNumGlyphs(Value: TNumGlyphs);
    procedure RefreshButton;
    procedure SetFlash(const Value: Boolean);
    procedure FlashButton(Sender : TObject);
    procedure SetMaxFont(const Value: Integer);



  protected
    { Protected declarations }
    procedure MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer);override;
    procedure MouseUp(Button: TMouseButton; Shift: TShiftState; X, Y: Integer);override;

    procedure SetLegend(Text : String);
    procedure SetIndicator(State : Boolean);
    procedure SetSEnabled(State : Boolean);
    procedure SetButtonColor(AColor : TColor);
    procedure SetTextSize(Size : Integer);
    procedure ButtonPaint(Sender :TObject);


  public
    { Public declarations }
    constructor Create(AOwner:TComponent); override;
    destructor Destroy; override;
    procedure Resize; override;

  published
    { Published declarations }
    property Legend : String read FLegend write SetLegend;
    property TextSize : Integer read FTextSize write SetTextSize;
    property HasIndicator : Boolean read FHasIndicator write FHasIndicator;
    property IndicatorOn  : Boolean read FIndicatorOn write SetIndicator;
    property IsEnabled    : Boolean read FIsEnabled write SetSEnabled;
    property ButtonColor : TColor read FButtonColor write SetButtonColor;
    property Glyph       : TBitmap read GetGlyph write SetGlyph;
    property Layout      : TSoftButtonLayout read FLayout write SetLayout;
    property AutoTextSize: Boolean read FAutoTextSize write SetAutoTextSize;
    property KeyStyle     : TSoftKeyShapes read FKeyStyle write SetKeyStyle;
    property CornerRad    : Integer read FCornerRad write SetCornerRad;
    property ButtonTravel : Integer read FBTravel write SetBTravel;
    property ShadowColor  : TColor read FSColour write SetShadowColour;
    property LegendFont   : TFont read FLFont write SetFLFont;
    property GlyphToEdgeSpacing : Integer read FGTedge write SetGlyphToEdgeSpacing;
    property TextToEdgeBorder   : Integer read FTTEdge write SetTextToEdgeBorder;
    property NumGlyphs    : TNumGlyphs read GetNumGlyphs write SetNumGlyphs default 1;
    property MultiLineSpacing : Integer read FMultilIneSpacing write SetMultiLineSpacing;
    property Flash : Boolean read FFlash write SetFlash;
    property FlashOnTime : Integer read FFlashOn write FFlashOn;
    property FlashOffTime : Integer read FFlashOff write FFlashOff;
    property MaxFontSize  : Integer read FMaxFont write SetMaxFont;

    property OnMouseUp;
    property OnMouseDown;
    property align;




  end;

procedure Register;

implementation

constructor TThreeLineButton.Create(AOwner:TComponent);
begin
inherited;
FMaxFont := 1000;
FlashTimer := TTimer.Create(Self);
with FlashTimer do
    begin
    Interval := 1000;
    Enabled := False;
    OnTimer := FlashButton;
    end;
FFlashOff := 500;
FFlashOn := 1500;
FKeyStyle := ssRectangle;
FLFont := TFont.Create;
FLFont.OnChange := UpdateFont;
FGlyph := TButtonGlyph.Create;
FCornerRad := 10;
Width := 100;
Height := 50;
FGTedge := 10;
FMultilIneSpacing := 0;
FBTravel := 1;
Offscreen := TBitmap.Create;
with Offscreen do
     begin
     //TransparentMode := tmAuto
     Offscreen.Transparent := True;
     Offscreen.TransparentColor := clOlive;
     end;

Layout := slGlyphLeft;
SetShadowColour(ClGray);
SetSEnabled(True);
FButtonColor := clwhite;
Layout := slGlyphLeft;
SetAutoTextSize(True);
LegendFont.Name := 'Verdana';
LegendFont.Color := clblue;

LegendFont.Size := 14;
ButtonTravel := 2;
GlyphToEdgeSpacing := 1;
TextToEdgeBorder := 5;
SetButtonColor(clWhite);

ButtonPaint(Self);
end;


destructor TThreeLineButton.Destroy;
begin
inherited;
TButtonGlyph(FGlyph).Free;
Offscreen.Free;
FLFont.Free;
end;



procedure TThreeLineButton.SetButtonColor(AColor : TColor);
begin
if FButtonColor = AColor then exit;
FButtonColor := AColor;

if IsEnabled then
   begin
   FCurrentColour := Acolor;
   end;

ButtonPaint(Self);
end;

procedure TThreeLineButton.SetSEnabled(State : Boolean);
begin
if not assigned(Offscreen) then exit;
FIsEnabled := State;
if FIsEnabled then
   begin
   FCurrentColour := FButtonColor;
   Enabled := True;
   end
else
   begin
   FCurrentColour := clSilver;
   Enabled := False;
   end;
if assigned(Parent) then ButtonPaint(Self);
end;


procedure TThreeLineButton.SetIndicator(State : Boolean);
begin
FIndicatorOn := State;
ButtonPaint(Self);
end;

procedure TThreeLineButton.SetLegend(Text : String);
begin
FLegend := Text;
if assigned(Parent) then
   begin
   ButtonPaint(Self);
   end;
end;


procedure TThreeLineButton.SetTextSize(Size : Integer);
begin
FTextSize := Size;
if assigned(Parent) then
   begin
   ButtonPaint(Self);
   end;
end;

procedure TThreeLineButton.MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
inherited;
FState := skDown;
ButtonPaint(Self);
end;

procedure TThreeLineButton.MouseUp(Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
inherited;
FState := skUp;
ButtonPaint(Self);
end;


procedure  TThreeLineButton.CalculateGlyphPosition(Glyph : TBitmap;GlyphCount : Integer;
                                                     layout:TSoftButtonLayout;BState:TSoftKeyStates;  var OData: TOffsetData);
var
GWidth : Integer;
begin
   if GlyphCount > 1 then
      begin
      GWidth := TButtonGlyph(FGlyph).Glyph.Width div GlyphCount;
      end
   else
      begin
      GWidth := TButtonGlyph(FGlyph).Glyph.Width;
      end;

   case Layout of
        slGlyphLeft:
        begin
        OData.GXoff := FGTedge;
        OData.GYOff := (Height - Glyph.Height) div 2;
        With Odata.TextRect do
             begin
             Top := FTTEdge;
             Bottom := Height-FTTEdge;
             Left := OData.GXoff+GWidth+FGTedge;
             Right :=  Width-FTTEdge;
             end;

        end;

        slGlyphRight:
        begin
        OData.GXoff := Width-(FGTedge+GWidth);
        OData.GYOff := (Self.Height - Glyph.Height) div 2;
        With Odata.TextRect do
             begin
             Top := FTTEdge;
             Bottom := Height-FTTEdge;
             Left := FTTEdge;
             Right :=  OData.GXOff-5;
             end;
        end;

        slGlyphTop:
        begin
        OData.GXOff := (Width-GWidth) div 2;
        OData.GYOff := FGTedge;
        Odata.TextRect.Top := OData.GYOff+TButtonGlyph(FGlyph).Glyph.Height+5;
        Odata.TextRect.Bottom := Height-FTTEdge;
        Odata.TextRect.Left := FTTEdge;
        Odata.TextRect.Right :=  Width - FTTEdge;
        end;

        slGlyphBottom:
        begin
        OData.GXOff := (Width-GWidth) div 2;
        OData.GYOff := Height-(FGTedge+Glyph.Height);
        Odata.TextRect.Top := FTTEdge;
        Odata.TextRect.Bottom := OData.GYOff-5;
        Odata.TextRect.Left := FTTEdge;
        Odata.TextRect.Right :=  (Width) - FTTEdge;
        end;

        slCentreOverlay:
        begin
        OData.GXOff := (Width-GWidth) div 2;
        OData.GYOff := (Height-TButtonGlyph(FGlyph).Glyph.Height) div 2;
        Odata.TextRect.Top := FTTEdge;
        Odata.TextRect.Bottom := Height-FTTEdge;
        Odata.TextRect.Left := FTTEdge;
        Odata.TextRect.Right :=  (Width) - FTTEdge;
        end;

   end; //case
Odata.TextBoxWidth := OData.TextRect.Right- OData.TextRect.Left;
Odata.TextBoxHeight := OData.TextRect.Bottom - OData.TextRect.Top;
if Bstate = skDown then
   begin
   OData.GXOff := OData.GXOff+FBTravel;
   OData.GYOff := OData.GYOff+FBTravel;
   end;

end;


Procedure  TThreeLineButton.SetFontHeight(FHeight : Integer);
begin
FLFont.Height := FHeight;
Offscreen.Canvas.Font.Assign(FLFont);
end;

function  TThreeLineButton.SetLegendDimensions(OnCanvas : TCanvas;Legend : String;TextBoxHeight,TextBoxWidth: Integer;AutoSize : Boolean): Integer;
var
TildPos : Integer;
FitFound : Boolean;
Retry    : Integer;
TLength  : Integer;
LRatio   : Double;
FontHt   : Integer;
begin
   Line1 := '';
   Line2 := '';
   Line3 := '';
   Result := 0;
   LineCount := 1;

   FitFound := False;
   Retry := 0;
   TildPos := Pos('~',FLegend);
   if TildPos > 0 then
      begin
      // MultiLine
      LineCount := 2;
      Line1 := Copy(FLegend,1,TildPos-1);
      Line2 := Copy(FLegend,TildPOs+1,Length(FLegend));
      TildPos := Pos('~',Line2);
      if TildPos > 0 then
        begin
        LineCount := 3;
        Line3 := Copy(Line2,TildPOs+1,Length(Line2));
        Line2 := Copy(Line2,1,TildPos-1);
        end;
      if Length(Line1) > Length(Line2) then
         begin
         if Length(Line3) > Length(Line1) then
            LongestLine := Line3
         else
            LongestLine := Line1
         end
      else
         begin
         if Length(Line3) > Length(Line2) then
            LongestLine := Line3
         else
            LongestLine := Line2
         end;
      if not AutoSize then exit;
      FontHt := (OffsetData.TextBoxHeight-FMultilIneSpacing) div LineCount;
      SetFontHeight(FontHt);
      while (Not FitFound) and (Retry < 8) do
            begin
            TLength := OnCanvas.TextWidth(LongestLine);
            if TLength < TextBoxWidth then
               begin
               FitFound := True;
               end
            else
                begin
                if TLength < 10 then
                   begin
                   FitFound := True;
                   end
                else
                    begin
                    LRatio := TextBoxWidth/TLength;
                    FontHt := Round(OnCanvas.Font.Height*LRatio);
                    SetFontHeight(FontHt);
                    end;
                end;
            inc(Retry);
            end;
      If FontHt > FMaxFont then
        begin
        SetFontHeight(FMaxFont);
        end;
      end
   else
      begin
      // SingleLine;
      LineCount := 1;
      Line1 := Legend;
      if not AutoSize then exit;
      SetFontHeight(OffsetData.TextBoxHeight);
      while (Not FitFound) and (Retry < 8) do
            begin
            TLength := OnCanvas.TextWidth(Legend);
            if TLength < TextBoxWidth then
               begin
               FitFound := True;
               end
            else
                begin
                if TLength < 10 then
                   begin
                   FitFound := True;
                   end
                else
                    begin
                    LRatio := TextBoxWidth/TLength;
                    SetFontHeight(Round(OnCanvas.Font.Height*LRatio));
                    end;
                end;
            inc(Retry);
            end;
      end;

end;


procedure TThreeLineButton.PaintSurface;
var
BOffset : Integer;
BottomColor     : TColor;
LegendOffset    : Integer;
TextIndent      : Integer;
TLength      : Integer;
THeight      : Integer;
IndicatorRect : TRect;
GRect         : Trect;
Drect         : Trect;
TextTop       : Integer;
Gwidth       : Integer;
BColor      : TColor;
TextColor    : TColor;

begin
if FlashState then
    begin
    BColor := clSilver;
    TextColor := FButtonColor;
    end
else
    begin
    BColor := FButtonColor;
    TextColor := StandardFontColour;
    end;

if not assigned(Offscreen) then exit;
GWidth := 50;
BOffset :=FBTravel;
if Fstate = skUp then
   begin
   BottomColor := clGray;
   LegendOffset := 0;
   end
else
   begin
   BottomColor := clWhite;
   LegendOffset := FBTravel;
   end;

CalculateGlyphPosition(TButtonGlyph(FGlyph).Glyph,NumGlyphs,Flayout,FState,OffsetData);

if assigned(TButtonGlyph(FGlyph).Glyph) and assigned(Parent)and (FLayout <> slcentreOverlay) then
   begin
   if NumGlyphs > 1 then
      begin
      GWidth := TButtonGlyph(FGlyph).Glyph.Width div NumGlyphs;
      end
   else
      begin
      GWidth := TButtonGlyph(FGlyph).Glyph.Width;
      end;

   GRect.Top := OffsetData.GYOff;
   GRect.Left := OffsetData.GXOff;
   GRect.Right := GRect.Left+GWidth;
   GRect.Bottom := GRect.Top+TButtonGlyph(FGlyph).Glyph.Height;
   DRect.Top := OffsetData.GYOff+LegendOffset;
   DRect.Left := OffsetData.GXOff+LegendOffset;
   DRect.Right := TButtonGlyph(FGlyph).Glyph.Width+OffsetData.GXOff+LegendOffset;
   DRect.Bottom := TButtonGlyph(FGlyph).Glyph.Height+OffsetData.GYOff+LegendOffset;
   end;

with Offscreen.Canvas do
     begin
     if csDesigning in ComponentState then
        begin
        Brush.color := clSilver;
        FrameRect(OffsetData.TextRect);
        Brush.color := clGray;
        FrameRect(GRect);
        end;

     Pen.Color := BottomColor;
     Pen.Width := 1;
     MoveTo(Width-BOffset,BOffset);
     if FHasIndicator then
        begin
        IndicatorRect.Top := 4+LegendOffset;
        IndicatorRect.Left := 4+LegendOffset;
        IndicatorRect.Right := 14+LegendOffset;
        IndicatorRect.Bottom := 10+LegendOffset;
        if FIndicatorOn then
           begin
           Brush.Color := clRed;
           end
        else
           begin
           Brush.Color := clSilver;
           end;
        FillRect(IndicatorRect);
        end;
     end;

If FLegend <> '' then
   begin
   SetLegendDimensions(Offscreen.Canvas,FLegend,OffsetData.TextBoxHeight,OffsetData.TextBoxWidth,FAutoTextSize);
   if LineCount > 1 then
      begin
      with Offscreen.Canvas do
           begin
           if FIsEnabled then
              begin
              Brush.Color := BColor;
              Font.Color := TextColor;
              end
           else
               begin
               Brush.Color := clSilver;
               Font.Color := clGray;
               end;
           Pen.Color := BColor;
           TLength := TextWidth(Line1);
           TextIndent := (OffsetData.TextBoxWidth-TLength) div 2;
//           THeight := TextHeight(Buffer1)*2;
           TextTop := LegendOffset+OffsetData.TextRect.Top;//+TextGap;
           TextOut(OffsetData.TextRect.Left+ TextIndent+LegendOffset,TextTop,Line1);
           TLength := TextWidth(Line2);
           if Line1 = '' then
              begin
              THeight := TextHeight('W')
              end
           else
              begin
              THeight := TextHeight(Line1);
              end;
           TextTop := TextTop+THeight+FMultilIneSpacing;
           TextIndent := (OffsetData.TextBoxWidth-TLength) div 2;
           TextOut(OffsetData.TextRect.Left+ TextIndent+LegendOffset,TextTop,Line2);
           if LineCount = 3 then
              begin
              TLength := TextWidth(Line3);
//              THeight := TextHeight(Line1);
              TextTop := TextTop+THeight+FMultilIneSpacing;
              TextIndent := (OffsetData.TextBoxWidth-TLength) div 2;
              TextOut(OffsetData.TextRect.Left+ TextIndent+LegendOffset,TextTop,Line3);
              end
           end;
      end
   else
      begin
      // Here for single line text
      with Offscreen.Canvas do
           begin
           if FIsEnabled then
              begin
              Brush.Color := BColor;
              Font.Color := TextColor;
              end
           else
               begin
               Brush.Color := clSilver;
               Font.Color := clGray;
               end;
           Pen.Color := BColor;
           TLength := TextWidth(FLegend);
           TextIndent := (OffsetData.TextBoxWidth-TLength) div 2;
           THeight := TextHeight(FLegend);
           TextTop := LegendOffset+ OffsetData.TextRect.Top+ ((OffsetData.TextBoxHeight-THeight) div 2);
           Offscreen.Canvas.TextOut(OffsetData.TextRect.Left+ TextIndent+LegendOffset,TextTop,Legend);
           end;
      end;
   end;
if assigned(FGlyph) and assigned(Parent)and (FLayout = slcentreOverlay) then
   begin
   GRect.Top := OffsetData.GYOff;
   GRect.Left := OffsetData.GXOff;
   GRect.Right := GRect.Left+GWidth;
   GRect.Bottom := GRect.Top+TButtonGlyph(FGlyph).Glyph.Height;
   GRect.Right := TButtonGlyph(FGlyph).Glyph.Width;
   GRect.Bottom := TButtonGlyph(FGlyph).Glyph.Height;
   DRect.Top := OffsetData.GYOff+LegendOffset;
   DRect.Left := OffsetData.GXOff+LegendOffset;
   DRect.Right := TButtonGlyph(FGlyph).Glyph.Width+OffsetData.GXOff+LegendOffset;
   DRect.Bottom := TButtonGlyph(FGlyph).Glyph.Height+OffsetData.GYOff+LegendOffset;
   end;
if isEnabled then
   TButtonGlyph(FGlyph).DrawButtonGlyph(OffScreen.Canvas, Point(Grect.Left,Grect.Top), bsUp, True)
else
   TButtonGlyph(FGlyph).DrawButtonGlyph(OffScreen.Canvas, Point(Grect.Left,Grect.Top), bsDisabled, True);
end;

procedure  TThreeLineButton.PaintRoundRectangle(OnCanvas : TCanvas;PRect : Trect;
           MColour : TColor; CornerRadius:Integer);
begin
if not assigned(Offscreen) then exit;
     Offscreen.Canvas.Pen.Color := MColour;
     Offscreen.Canvas.Pen.Width := 1;
     Offscreen.Canvas.Brush.Color := MColour;
     Offscreen.Canvas.RoundRect(Prect.Left,Prect.Top,Prect.Right,Prect.Bottom,CornerRadius,CornerRadius);
end;

procedure TThreeLineButton.DrawBasicShape(BorderWidth : Integer);
var
BoundsRect : TRect;
MainRect : TRect;
LightRect: TRect;
DarkRect: TRect;
Bwidth,BHeight : Integer;
FixedBorder : Integer;
Rad         : Integer;
BColor      : TColor;
begin
if not assigned(Offscreen) then exit;

if FlashState then
    begin
    BColor := clSilver;
    end
else
    begin
    BColor := FCurrentColour;
    end;


FixedBorder := 1;
BoundsRect.Left := 0;
BoundsRect.Right:= Width;
BoundsRect.Top :=  0;
BoundsRect.Bottom := Height;
Bwidth := Width-(FixedBorder*2);
BHeight := Height-(FixedBorder*2);

Offscreen.Width := Width;
Offscreen.Height := Height;
if FState = skUp then
   begin
   LightRect.Left := FixedBorder;
   LightRect.Right:= BWidth;
   LightRect.Top :=  FixedBorder;
   LightRect.Bottom := BHeight;

   DarkRect.Left := FixedBorder+1 ;
   DarkRect.Right:= Width-FixedBorder;
   DarkRect.Top :=  FixedBorder+1;
   DarkRect.Bottom := Height-FixedBorder;

   MainRect.Left := FixedBorder+1;
   MainRect.Right := bWidth-BorderWidth ;
   MainRect.Top :=  FixedBorder+1;
   MainRect.Bottom := BHeight-BorderWidth;
   end
else
   begin
   DarkRect.Left := FixedBorder;
   DarkRect.Right:= Width-FixedBorder;
   DarkRect.Top :=  FixedBorder;
   DarkRect.Bottom := Height-FixedBorder;

   MainRect.Left := BorderWidth+FixedBorder;
   MainRect.Right := Bwidth;
   MainRect.Top :=  BorderWidth+FixedBorder;
   MainRect.Bottom := Bheight;
   end;
with Offscreen.Canvas do
begin
if ClearFirst then
   begin
   Brush.Color := clOlive;
   FillRect(BoundsRect);
   ClearFirst := False;
   end;
case FKeyStyle of
     ssRectangle:
     begin
     Pen.Width := FixedBorder;
     Pen.Color := clBlack;
     Rectangle(0,0,Width,Height);
     if FState = skUp then
        begin
        Pen.Color := clWhite;
        Brush.Color := clWhite;
        Rectangle(LightRect);
        end;
     Pen.Color := FSColour;
     Brush.Color := FSColour;
     Rectangle(DarkRect);
     Pen.Color := BColor;
     Brush.Color := BColor;
     Rectangle(MainRect);
     end;

     ssRoundedRectangle,ssElipse:
     begin
     if FKeyStyle=ssElipse then
        Rad := Width
     else
         Rad := FCornerRad;

     Pen.Width := FixedBorder;
     Pen.Color := clBlue;
     PaintRoundRectangle(Offscreen.Canvas,BoundsRect,clBlack,Rad);
     if FState = skUp then
        begin
        PaintRoundRectangle(Offscreen.Canvas,LightRect,clWhite,Rad);
        end;
     PaintRoundRectangle(Offscreen.Canvas,DarkRect,FSColour,Rad);
     PaintRoundRectangle(Offscreen.Canvas,MainRect,BColor,Rad);
     end;
end; // case
end; // with
end;

procedure TThreeLineButton.ButtonPaint(Sender :TObject);
var
BRect    : TRect;

begin
if not HandleAllocated then exit;
Flfont.OnChange := nil;
try
BRect.Top := 0;
BRect.Left := 0;
BRect.Right := Width;
BRect.Bottom := Height;

Offscreen.Width := Width;
Offscreen.Height := Height;
Offscreen.Transparent := False;
DrawBasicShape(FBTravel);
PaintSurface;
try
Offscreen.Transparent := True;
Self.Canvas.Draw(0,0,Offscreen);
except
end;
finally
FLFont.OnChange := UpdateFont;
end;
end;

procedure TThreeLineButton.DoMyMouseDown(Sender : TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
MouseDown(Button,Shift,X, Y);
if HandleAllocated then ButtonPaint(Self);
end;

procedure TThreeLineButton.DoMyMouseUp(Sender : TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
MouseUp(Button,Shift,X, Y);
if HandleAllocated then ButtonPaint(Self);
end;

procedure TThreeLineButton.DoMyClick(Sender : TObject);
begin
Click;
end;

procedure Register;
begin
  RegisterComponents('ACNCComps', [TThreeLineButton]);
end;

procedure TThreeLineButton.SetGlyph(const Value: TBitmap);

begin
if assigned(Value) then
   begin
   TButtonGlyph(FGlyph).Glyph := Value;
   end;
  GlyphToEdgeSpacing := 5;
end;

function TThreeLineButton.GetGlyph: TBitmap;
begin
  Result := TButtonGlyph(FGlyph).Glyph;
end;

procedure TThreeLineButton.SetLayout(const Value: TSoftButtonLayout);
begin
  FLayout := Value;
  RefreshButton;
end;


procedure TThreeLineButton.SetAutoTextSize(const Value: Boolean);
begin
  FAutoTextSize := Value;
  RefreshButton;
end;

procedure TThreeLineButton.SetKeyStyle(const Value: TSoftKeyShapes);
begin
  FKeyStyle := Value;
  RefreshButton;
end;

procedure TThreeLineButton.SetCornerRad(const Value: Integer);
begin
  FCornerRad := Value;
  RefreshButton;
end;


procedure TThreeLineButton.SetBTravel(const Value: Integer);
begin
  FBTravel := Value;
  if HandleAllocated then ButtonPaint(Self);
end;

procedure TThreeLineButton.SetShadowColour(const Value: TColor);
begin
  FSColour := Value;
  RefreshButton;
end;

procedure TThreeLineButton.UpdateFont(Sender : Tobject);
begin
  StandardFontColour := FLFont.Color;
  if HandleAllocated then
    begin
    ButtonPaint(Self);
    end;
end;


procedure TThreeLineButton.SetFLFont(const Value: TFont);
begin
StandardFontColour := FLFont.Color;
if not assigned(Offscreen) then exit;
  FLFont.Assign(Value);
  Offscreen.Canvas.Font.Assign(FLFont);
  RefreshButton;
end;

procedure TThreeLineButton.SetGlyphToEdgeSpacing(const Value: Integer);
begin
  FGTedge := Value;
  RefreshButton;
end;


procedure TThreeLineButton.SetTextToEdgeBorder(const Value: Integer);
begin
  FTTedge := Value;
  RefreshButton;
end;


function TThreeLineButton.GetNumGlyphs: TNumGlyphs;
begin
  Result := TButtonGlyph(FGlyph).NumGlyphs;
end;

procedure TThreeLineButton.SetNumGlyphs(Value: TNumGlyphs);
begin
  if Value < 0 then Value := 1
  else if Value > 4 then Value := 4;
  if Value <> TButtonGlyph(FGlyph).NumGlyphs then
  begin
    TButtonGlyph(FGlyph).NumGlyphs := Value;
    RefreshButton;
  end;
end;

{ TGlyphList }

constructor TGlyphList.CreateSize(AWidth, AHeight: Integer);
begin
  inherited CreateSize(AWidth, AHeight);
  Used := TBits.Create;
end;

destructor TGlyphList.Destroy;
begin
  Used.Free;
  inherited Destroy;
end;
    
function TGlyphList.AllocateIndex: Integer;
begin
  Result := Used.OpenBit;
  if Result >= Used.Size then
  begin
    Result := inherited Add(nil, nil);
    Used.Size := Result + 1;
  end;
  Used[Result] := True;
end;
    
function TGlyphList.AddMasked(Image: TBitmap; MaskColor: TColor): Integer;
begin
  Result := AllocateIndex;
  ReplaceMasked(Result, Image, MaskColor);
  Inc(FCount);
end;
    
procedure TGlyphList.Delete(Index: Integer);
begin
  if Used[Index] then
  begin
    Dec(FCount);
    Used[Index] := False;
  end;
end;
    
{ TGlyphCache }
    
constructor TGlyphCache.Create;
begin
  inherited Create;
  GlyphLists := TList.Create;
end;
    
destructor TGlyphCache.Destroy;
begin
  GlyphLists.Free;
  inherited Destroy;
end;
    
function TGlyphCache.GetList(AWidth, AHeight: Integer): TGlyphList;
var
  I: Integer;
begin
  for I := GlyphLists.Count - 1 downto 0 do
  begin
    Result := GlyphLists[I];
    with Result do
      if (AWidth = Width) and (AHeight = Height) then Exit;
  end;
  Result := TGlyphList.CreateSize(AWidth, AHeight);
  GlyphLists.Add(Result);
end;
    
procedure TGlyphCache.ReturnList(List: TGlyphList);
begin
  if List = nil then Exit;
  if List.Count = 0 then
  begin
    GlyphLists.Remove(List);
    List.Free;
  end;
end;

function TGlyphCache.Empty: Boolean;
begin
  Result := GlyphLists.Count = 0;
end;
    
var
  GlyphCache: TGlyphCache = nil;
  ButtonCount: Integer = 0;

{ TButtonGlyph }
    
constructor TButtonGlyph.Create;
var
  I: TButtonState;
begin
  inherited Create;
  FOriginal := TBitmap.Create;
  FOriginal.OnChange := GlyphChanged;
  FTransparentColor := clOlive;
  FNumGlyphs := 1;
  for I := Low(I) to High(I) do
    FIndexs[I] := -1;
  if GlyphCache = nil then GlyphCache := TGlyphCache.Create;
end;
    
destructor TButtonGlyph.Destroy;
begin
  FOriginal.Free;
  Invalidate;
  if Assigned(GlyphCache) and GlyphCache.Empty then
  begin
    GlyphCache.Free;
    GlyphCache := nil;
  end;
  inherited Destroy;
end;
    
procedure TButtonGlyph.Invalidate;
var
  I: TButtonState;
begin
  for I := Low(I) to High(I) do
  begin
    if FIndexs[I] <> -1 then FGlyphList.Delete(FIndexs[I]);
    FIndexs[I] := -1;
  end;
  GlyphCache.ReturnList(FGlyphList);
  FGlyphList := nil;
end;
    
procedure TButtonGlyph.GlyphChanged(Sender: TObject);
begin
  if Sender = FOriginal then
  begin
    FTransparentColor := FOriginal.TransparentColor;
    Invalidate;
    if Assigned(FOnChange) then FOnChange(Self);
  end;
end;
    
procedure TButtonGlyph.SetGlyph(Value: TBitmap);
var
  Glyphs: Integer;
begin
  Invalidate;
  FOriginal.Assign(Value);
  if (Value <> nil) and (Value.Height > 0) then
  begin
    FTransparentColor := Value.TransparentColor;
    if Value.Width mod Value.Height = 0 then
    begin
      Glyphs := Value.Width div Value.Height;
      if Glyphs > 4 then Glyphs := 1;
      SetNumGlyphs(Glyphs);
    end;
  end;
end;
    
procedure TButtonGlyph.SetNumGlyphs(Value: TNumGlyphs);
begin
  if (Value <> FNumGlyphs) and (Value > 0) then
  begin
    Invalidate;
    FNumGlyphs := Value;
    GlyphChanged(Glyph);
  end;
end;
    
function TButtonGlyph.CreateButtonGlyph(State: TButtonState): Integer;
const
  ROP_DSPDxax = $00E20746;
var
  TmpImage, DDB, MonoBmp: TBitmap;
  IWidth, IHeight: Integer;
  IRect, ORect: TRect;
  I: TButtonState;
  DestDC: HDC;
begin
  if (State = bsDown) and (NumGlyphs < 3) then State := bsUp;
  Result := FIndexs[State];
  if Result <> -1 then Exit;
  if (FOriginal.Width or FOriginal.Height) = 0 then Exit;
  IWidth := FOriginal.Width div FNumGlyphs;
  IHeight := FOriginal.Height;
  if FGlyphList = nil then
  begin
    if GlyphCache = nil then GlyphCache := TGlyphCache.Create;
    FGlyphList := GlyphCache.GetList(IWidth, IHeight);
  end;
  TmpImage := TBitmap.Create;
  try
    TmpImage.Width := IWidth;
    TmpImage.Height := IHeight;
    IRect := Rect(0, 0, IWidth, IHeight);
    TmpImage.Canvas.Brush.Color := clBtnFace;
    TmpImage.Palette := CopyPalette(FOriginal.Palette);
    I := State;
    if Ord(I) >= NumGlyphs then I := bsUp;
    ORect := Rect(Ord(I) * IWidth, 0, (Ord(I) + 1) * IWidth, IHeight);
    case State of
      bsUp, bsDown,
      bsExclusive:
        begin
          TmpImage.Canvas.CopyRect(IRect, FOriginal.Canvas, ORect);
          if FOriginal.TransparentMode = tmFixed then
            FIndexs[State] := FGlyphList.AddMasked(TmpImage, FTransparentColor)
          else
            FIndexs[State] := FGlyphList.AddMasked(TmpImage, clDefault);
        end;
      bsDisabled:
        begin
          MonoBmp := nil;
          DDB := nil;
          try
            MonoBmp := TBitmap.Create;
            DDB := TBitmap.Create;
            DDB.Assign(FOriginal);
            DDB.HandleType := bmDDB;
            if NumGlyphs > 1 then
            with TmpImage.Canvas do
            begin    { Change white & gray to clBtnHighlight and clBtnShadow }
              CopyRect(IRect, DDB.Canvas, ORect);
              MonoBmp.Monochrome := True;
              MonoBmp.Width := IWidth;
              MonoBmp.Height := IHeight;
    
              { Convert white to clBtnHighlight }
              DDB.Canvas.Brush.Color := clWhite;
              MonoBmp.Canvas.CopyRect(IRect, DDB.Canvas, ORect);
              Brush.Color := clBtnHighlight;
              DestDC := Handle;
              SetTextColor(DestDC, clBlack);
              SetBkColor(DestDC, clWhite);
              BitBlt(DestDC, 0, 0, IWidth, IHeight,
                     MonoBmp.Canvas.Handle, 0, 0, ROP_DSPDxax);
    
              { Convert gray to clBtnShadow }
              DDB.Canvas.Brush.Color := clGray;
              MonoBmp.Canvas.CopyRect(IRect, DDB.Canvas, ORect);
              Brush.Color := clBtnShadow;
              DestDC := Handle;
              SetTextColor(DestDC, clBlack);
              SetBkColor(DestDC, clWhite);
              BitBlt(DestDC, 0, 0, IWidth, IHeight,
                     MonoBmp.Canvas.Handle, 0, 0, ROP_DSPDxax);
    
              { Convert transparent color to clBtnFace }
              DDB.Canvas.Brush.Color := ColorToRGB(FTransparentColor);
              MonoBmp.Canvas.CopyRect(IRect, DDB.Canvas, ORect);
              Brush.Color := clBtnFace;
              DestDC := Handle;
              SetTextColor(DestDC, clBlack);
              SetBkColor(DestDC, clWhite);
              BitBlt(DestDC, 0, 0, IWidth, IHeight,
                     MonoBmp.Canvas.Handle, 0, 0, ROP_DSPDxax);
            end
            else
            begin
              { Create a disabled version }
              with MonoBmp do
              begin
                Assign(FOriginal);
                HandleType := bmDDB;
                Canvas.Brush.Color := clBlack;
                Width := IWidth;
                if Monochrome then
                begin
                  Canvas.Font.Color := clWhite;
                  Monochrome := False;
                  Canvas.Brush.Color := clWhite;
                end;
                Monochrome := True;
              end;
              with TmpImage.Canvas do
              begin
                Brush.Color := clBtnFace;
                FillRect(IRect);
                Brush.Color := clBtnHighlight;
                SetTextColor(Handle, clBlack);
                SetBkColor(Handle, clWhite);
                BitBlt(Handle, 1, 1, IWidth, IHeight,
                  MonoBmp.Canvas.Handle, 0, 0, ROP_DSPDxax);
                Brush.Color := clBtnShadow;
                SetTextColor(Handle, clBlack);
                SetBkColor(Handle, clWhite);
                BitBlt(Handle, 0, 0, IWidth, IHeight,
                  MonoBmp.Canvas.Handle, 0, 0, ROP_DSPDxax);
              end;
            end;
          finally
            DDB.Free;
            MonoBmp.Free;
          end;
          FIndexs[State] := FGlyphList.AddMasked(TmpImage, clDefault);
        end;
    end;
  finally
    TmpImage.Free;
  end;
  Result := FIndexs[State];
  FOriginal.Dormant;
end;
    
procedure TButtonGlyph.DrawButtonGlyph(Canvas: TCanvas; const GlyphPos: TPoint;
  State: TButtonState; Transparent: Boolean);
var
  Index: Integer;
begin
  if FOriginal = nil then Exit;
  if (FOriginal.Width = 0) or (FOriginal.Height = 0) then Exit;
  Index := CreateButtonGlyph(State);
  with GlyphPos do
    if Transparent or (State = bsExclusive) then
      ImageList_DrawEx(FGlyphList.Handle, Index, Canvas.Handle, X, Y, 0, 0,
        clNone, clNone, ILD_Transparent)
    else
      ImageList_DrawEx(FGlyphList.Handle, Index, Canvas.Handle, X, Y, 0, 0,
        ColorToRGB(clBtnFace), clNone, ILD_Normal);
end;


procedure TThreeLineButton.Paint;
begin
  inherited;
  ButtonPaint(Self);
end;

procedure TThreeLineButton.RefreshButton;
begin
  inherited;
  Visible := False;
  if HandleAllocated then
     begin
     ClearFirst := True;
     ButtonPaint(Self);
     end;
  Visible := True;
end;

procedure TThreeLineButton.Resize;
begin
  inherited;
  RefreshButton
end;

procedure TThreeLineButton.SetMultiLineSpacing(const Value: Integer);
begin
if Value <> FMultilIneSpacing then
   begin
   FMultilIneSpacing := Value;
   RefreshButton
   end;
end;

procedure TThreeLineButton.SetFlash(const Value: Boolean);
begin
  FFlash := Value;
  FlashState := Value;
  if assigned(FlashTimer) then
    begin
    FlashTimer.Enabled := Value;
    FlashTimer.Interval := FFlashOff;
    end;
  //RefreshButton
end;

procedure TThreeLineButton.FlashButton(Sender: TObject);
begin
FlashState := not FlashState;
if FlashState then
    begin
    FlashTimer.Interval := FFlashOff;
    end
else
    begin
    FlashTimer.Interval := FFlashOn
    end;

ButtonPaint(Self);
end;

procedure TThreeLineButton.SetMaxFont(const Value: Integer);
begin
  FMaxFont := Value;
  RefreshButton;

end;

end.
