unit WaterMimic;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, AbstractFormPlugin, IFormInterface, NamedPlugin,
  ComCtrls, INamedInterface, Grids, ACNC_TouchSoftkey, PluginInterface,
  ImgList,  CNCTypes,  IToolLife, MimicCanvasComponents,FlowPipes,
  ACNC_ThreeLineButton, ACNC_TouchGlyphSoftkey,
  IniFiles;

type
  TWaterMimicTags = (
    wit_AccessLevel,
    wit_NC_ACTUAL_PRESSURE,
    wit_NC_DEMANDED_PRESSURE,
    wit_I_DIFeed5uFilterPressure,
    wit_I_DIResistivity,
    wit_I_DirtyPumpRunning,
    wit_I_DirtyTankWaterLevel,
    wit_I_DirtyWater15uFilterPressure,
    wit_I_DirtyWater2uFilterPressure,
    wit_I_DirtyWaterPreFilterPressure,
    wit_I_ChillerHealthy,
    wit_I_ChillerSupplyOn,
    wit_I_CleanPumpRunning,
    wit_I_CleanTankWaterLevel,
    wit_I_DITemperature,
    wit_I_HPPumpInverterFault,
    wit_I_HPPumpSupplyOn,
    wit_I_NotResistivityMeterAlarm,
    wit_I_NotResistivityMeterWarning,
    wot_O_HPDump,
    wot_O_HPMachineFeed,
    wot_O_HPPressureDump,
    wot_O_HPPumpRun,
    wot_O_HPPumpSupply,
    wot_O_PressureDemand,
    wot_O_SumpPump,
    wot_O_WaterWashGun);


  TWaterTagDefinitions = record
    Ini : string;
    Tag : string;
    Owned : Boolean;
    TagType : string;
  end;

const
  TagDefinitions : array [TWaterMimicTags] of TWaterTagDefinitions = (
     ( Ini : 'AccessLevel'; Tag : 'AccessLevel'; Owned : False; TagType : 'TIntegerTag' ),
     ( Ini : 'NCActualDemand'; Tag : 'NC_ACTUAL_PRESSURE'; Owned : False; TagType : 'TIntegerTag' ),
     ( Ini : 'NCDemandedPressure'; Tag : 'NC_DEMANDED_PRESSURE'; Owned : False; TagType : 'TIntegerTag' ),
     ( Ini : 'FilterPressure'; Tag : 'I_DIFeed5uFilterPressure'; Owned : False; TagType : 'TIntegerTag' ),
     ( Ini : 'DIResistivity'; Tag : 'I_DIResistivity'; Owned : False; TagType : 'TIntegerTag' ),
     ( Ini : 'DirtyPumpRunning,'; Tag : 'I_DirtyPumpRunning,'; Owned : False; TagType : 'TIntegerTag' ),
     ( Ini : 'DirtyTankWaterLevel'; Tag : 'I_DirtyTankWaterLevel'; Owned : False; TagType : 'TIntegerTag' ),
     ( Ini : 'DirtyWater15uFilterPressure'; Tag : 'I_DirtyWater15uFilterPressure'; Owned : False; TagType : 'TIntegerTag' ),
     ( Ini : 'DirtyWater2uFilterPressure'; Tag : 'I_DirtyWater2uFilterPressure'; Owned : False; TagType : 'TIntegerTag' ),
     ( Ini : 'DirtyWaterPreFilterPressure'; Tag : 'I_DirtyWaterPreFilterPressure'; Owned : False; TagType : 'TIntegerTag' ),
     ( Ini : 'ChillerHealthy'; Tag : 'I_ChillerHealthy'; Owned : False; TagType : 'TIntegerTag' ),
     ( Ini : 'ChillerSupplyOn'; Tag : 'I_ChillerSupplyOn'; Owned : False; TagType : 'TIntegerTag' ),
     ( Ini : 'CleanPumpRunning,'; Tag : 'I_CleanPumpRunning,'; Owned : False; TagType : 'TIntegerTag' ),
     ( Ini : 'CleanTankWaterLevel'; Tag : 'I_CleanTankWaterLevel'; Owned : False; TagType : 'TIntegerTag' ),
     ( Ini : 'DITemperature'; Tag : 'I_DITemperature'; Owned : False; TagType : 'TIntegerTag' ),
     ( Ini : 'HPPumpInverterFault'; Tag : 'I_HPPumpInverterFault'; Owned : False; TagType : 'TIntegerTag' ),
     ( Ini : 'HPPumpSupplyOn'; Tag : 'I_HPPumpSupplyOn'; Owned : False; TagType : 'TIntegerTag' ),
     ( Ini : 'NotResistivityMeterAlarm'; Tag : 'I_NotResistivityMeterAlarm'; Owned : False; TagType : 'TIntegerTag' ),
     ( Ini : 'NotResistivityMeterWarning'; Tag : 'I_NotResistivityMeterWarning'; Owned : False; TagType : 'TIntegerTag' ),
     ( Ini : 'HPDump_Out'; Tag : 'O_HPDump'; Owned : False; TagType : 'TIntegerTag' ),
     ( Ini : 'MachineFeed_Out'; Tag : 'O_HPMachineFeed'; Owned : False; TagType : 'TIntegerTag' ),
     ( Ini : 'HPPressureDump_Out'; Tag : 'O_HPPressureDump'; Owned : False; TagType : 'TIntegerTag' ),
     ( Ini : 'HPPumpRun_Out'; Tag : 'O_HPPumpRun'; Owned : False; TagType : 'TIntegerTag' ),
     ( Ini : 'HPPumpSupply_Out'; Tag : 'O_HPPumpSupply'; Owned : False; TagType : 'TIntegerTag' ),
     ( Ini : 'PressureDemand_Out'; Tag : 'O_PressureDemand'; Owned : False; TagType : 'TIntegerTag'),
     ( Ini : 'SumpPump_Out'; Tag : 'O_SumpPump'; Owned : False; TagType : 'TIntegerTag'),
     ( Ini : 'WaterWashGun_Out'; Tag : 'O_WaterWashGun'; Owned : False; TagType : 'TIntegerTag')
    );


type

  TWaterMimicPanel = class(TInstallForm)
    Panel1: TPanel;
    Panel2: TPanel;
    Button1: TButton;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TagGrid: TStringGrid;
    Edit1: TEdit;
    TabSheet2: TTabSheet;
    Panel3: TPanel;
    Panel4: TPanel;
    Button2: TButton;
    procedure Button2Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    FirstShow : Boolean;
    Initialised : Boolean;
    // Tag names to be used if tags to be cofigurable via cfg file.
    Tags : array [TWaterMimicTags] of TTagRef;
    MainMimic : TMimicPaper;
    procedure FormCreate(Sender: TObject);


  public
    { Public declarations }

    destructor Destroy; override;
    procedure Install; override;
    procedure FinalInstall; override;
    procedure Execute; override;
    procedure Shutdown; override;
    procedure CloseForm; override;
    procedure DisplaySweep; override;
    procedure TagChanged(Tag: TTagRef);

  end;

var
  WaterMimicPanel: TWaterMimicPanel;
  procedure TagChangeHandler(Tag : TTagRef);stdcall;

implementation

{$R *.dfm}

{ TWaterMimicPanel }

procedure TWaterMimicPanel.CloseForm;
begin
  inherited;

end;

procedure TWaterMimicPanel.Shutdown;
begin
  inherited;

end;

procedure TWaterMimicPanel.DisplaySweep;
var
I : TWaterMimicTags;
Value : Integer;
RowNum : Integer;
begin

  inherited;
  Edit1.Text := '--';
  if not Initialised or (FirstShow = False) Then exit;
  RowNum := 1;
  for I := Low(TWaterMimicTags) to High(TWaterMimicTags) do begin
      TagGrid.Cells[1,RowNum] := IntToStr(Named.GetAsInteger(Tags[I]));
      Inc(RowNum);
  end;
      Edit1.Text := IntToStr(RowNum);

end;

procedure TWaterMimicPanel.FinalInstall;
var
I : TWaterMimicTags;
RowNum : Integer;
begin
  inherited;
{  for I := Low(TWaterMimicTags) to High(TWaterMimicTags) do
      TagNames[I] := Ini.ReadString('Tags', TagDefinitions[I].Ini, TagDefinitions[I].Tag);}

with TagGrid do
  begin
  Width :=304;
  ColCount := 2;
  ColWidths[0] := 150;
  ColWidths[1] := 150;
  RowCount := Integer(High(TWaterMimicTags))+1;
  end;
RowNum := 1;
  for I := Low(TWaterMimicTags) to High(TWaterMimicTags) do begin
    if not TagDefinitions[I].Owned then
//      Tags[I] := Named.AquireTag(PChar(TagNames[I]), PChar(TagDefinitions[I].TagType), TagChangeHandler);
      Tags[I] := Named.AquireTag(PChar(TagDefinitions[I].Tag), PChar(TagDefinitions[I].TagType), TagChangeHandler);
      TagGrid.Cells[0,RowNum] := TagDefinitions[I].Tag;
      inc(RowNum);
  end;
 Initialised := True;

end;

procedure TWaterMimicPanel.Install;
begin
  inherited;
  Initialised := False;
  FirstShow := False;
  MainMimic := TMimicPaper.Create(nil);
  with MainMimic do
    begin
    Parent := Panel3;
    Align := alClient;
    Initialize(clWhite);
    end;


end;

procedure TWaterMimicPanel.TagChanged(Tag: TTagRef);
begin
  exit;
  if Tag = Tags[wit_I_DIFeed5uFilterPressure] then begin
    // here when all complete or when recovery required
//    OCTRunningChanged(Named.GetAsBoolean(Tag));
  end else

  if Tag = Tags[wit_I_DIResistivity] then begin
  end else

  if Tag = Tags[wit_I_DIResistivity] then begin
  end else
  if Tag = Tags[wit_I_DIFeed5uFilterPressure] then begin
  end;
end;

destructor TWaterMimicPanel.Destroy;
begin

  inherited;
end;

procedure TWaterMimicPanel.Execute;
begin
  inherited;
  Height := 700;
  Width := 1024;
  ShowModal;

end;

procedure TWaterMimicPanel.FormCreate(Sender: TObject);
begin
Top := 70;
Left := 1;
end;

procedure TWaterMimicPanel.FormShow(Sender: TObject);
var
AL : Integer;
begin
  Top := 70;
  Left :=1;
  FirstShow := True;
//  AL := Named.GetAsInteger(Tags[wit_AccessLevel]);
end;


procedure TagChangeHandler(Tag : TTagRef);stdcall
begin
  WaterMimicPanel.TagChanged(Tag);
end;

procedure TWaterMimicPanel.Button1Click(Sender: TObject);
begin
Close;
end;

procedure TWaterMimicPanel.Button2Click(Sender: TObject);
var
FPos : TPoint;
begin
FPos.X := 300;
FPOs.Y := 200;
if assigned(MainMimic) then
  MainMimic.AddHorizontalPipe(FPos,100,3,thpeOPenUp,thpeOPenUp);
end;

initialization
  TAbstractFormPlugin.AddPlugin('WaterPlantMimicPanel', TWaterMimicPanel);

end.
