object Form2: TForm2
  Left = 0
  Top = 0
  Width = 798
  Height = 622
  Caption = 'Form2'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 790
    Height = 41
    Align = alTop
    BevelInner = bvLowered
    TabOrder = 0
    object Button1: TButton
      Left = 16
      Top = 8
      Width = 75
      Height = 25
      Caption = 'Button1'
      TabOrder = 0
      OnClick = Button1Click
    end
    object Button2: TButton
      Left = 96
      Top = 8
      Width = 75
      Height = 25
      Caption = 'Start/Stop'
      TabOrder = 1
      OnClick = Button2Click
    end
    object Button3: TButton
      Left = 424
      Top = 8
      Width = 75
      Height = 25
      Caption = 'Button3'
      TabOrder = 2
      OnClick = Button3Click
    end
    object Edit1: TEdit
      Left = 504
      Top = 10
      Width = 81
      Height = 21
      TabOrder = 3
      Text = 'Edit1'
    end
    object Button4: TButton
      Left = 176
      Top = 8
      Width = 75
      Height = 25
      Caption = 'Colour'
      TabOrder = 4
      OnClick = Button4Click
    end
    object Button5: TButton
      Left = 616
      Top = 8
      Width = 75
      Height = 25
      Caption = 'Width'
      TabOrder = 5
      OnClick = Button5Click
    end
    object Edit2: TEdit
      Left = 696
      Top = 10
      Width = 73
      Height = 21
      TabOrder = 6
      Text = '10'
    end
    object Button6: TButton
      Left = 264
      Top = 8
      Width = 75
      Height = 25
      Caption = 'Button6'
      TabOrder = 7
      OnClick = Button6Click
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 41
    Width = 790
    Height = 547
    Align = alClient
    BevelInner = bvLowered
    TabOrder = 1
  end
end
