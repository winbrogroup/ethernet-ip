unit MimTestMain;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs,MimicCanvasComponents, ExtCtrls, StdCtrls,FlowPipes,BitLibrary;

type
  TForm2 = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    Edit1: TEdit;
    Button4: TButton;
    Button5: TButton;
    Edit2: TEdit;
    Button6: TButton;
    procedure Button6Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Button4Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    PipeGroup1 : TFlowPipeGroup;
    MimicPaper : TMimicPaper;
    ATimer : TTimer;
    TestPipe,VTestPipe: TFlowPipe;
    procedure ClockPaper(Sender : TObject);
  public
    { Public declarations }
  end;

var
  Form2: TForm2;

implementation

{$R *.dfm}

procedure TForm2.FormCreate(Sender: TObject);
begin
MimicPaper := TMimicPaper.Create(nil);
MimicPaper.Parent := Panel2;
with MimicPaper do
  begin
  Align := alClient;
  Initialize(clYellow);
  end;
ATimer := TTimer.Create(nil);
with ATimer do
  begin
  Interval := 150;
  OnTimer := ClockPaper;
  Enabled := False;
  end;

PipeGroup1 := TFlowPipeGroup.Create;
PipeGroup1.InitialiseOnPanel(MimicPaper);

end;

procedure TForm2.Button1Click(Sender: TObject);
var
Pwidth : INteger;
LastPipe : TFlowPipe;
begin
PipeGroup1.Clean;
with PipeGroup1 do
  begin
  BeginUpdate;
  SetPoint(PipeStartPos,50,10);
  AddVerticalDownPipe(PipeStartPos,100,tvpeFlat,tvpeOPenRight);
  AddHorizontalPipe(GroupEndPos,100,thpeOPenUp,thpeOPenDown);
  AddVerticalDownPipe(GroupEndPos,100,tvpeOPenLeft,tvpeOPenRight);
  AddHorizontalPipe(GroupEndPos,100,thpeOPenUp,thpeOPenDown);
  AddVerticalDownPipe(GroupEndPos,100,tvpeOPenLeft,tvpeOPenRight);
  AddHorizontalPipe(GroupEndPos,100,thpeOPenUp,thpeOPenUp);
  AddVerticalUpPipe(GroupEndPos,100,tvpeOPenLeft,tvpeOPenRight);
  EndUpdate;
  end;


end;

procedure TForm2.ClockPaper(Sender: TObject);
begin
PipeGroup1.ClockAnimation(False);
end;

procedure TForm2.Button2Click(Sender: TObject);
begin
ATimer.Enabled := not ATimer.Enabled
end;

procedure TForm2.Button3Click(Sender: TObject);
begin
PipeGroup1.ClockAnimation(False);
end;

procedure TForm2.Button4Click(Sender: TObject);
begin
PipeGroup1.PipeColour := PipeGroup1.PipeColour+ 1100;
end;

procedure TForm2.FormClose(Sender: TObject; var Action: TCloseAction);
begin
PipeGroup1.Free;
end;

procedure TForm2.Button5Click(Sender: TObject);
var
PW : Integer;
begin
PW := StrToIntDef(Edit2.Text,10);
Edit2.Text := IntToStr(Pw);
PipeGroup1.PipeWidth := Pw;
end;

procedure TForm2.Button6Click(Sender: TObject);
begin
if PipeGroup1.Flow = fdForward then
  begin
  PipeGroup1.Flow := fdReverse
  end
else
  begin
  PipeGroup1.Flow := fdForward
  end

end;

end.
