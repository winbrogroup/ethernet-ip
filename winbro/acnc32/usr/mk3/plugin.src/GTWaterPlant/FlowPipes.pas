
unit FlowPipes;

interface
uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls,BitLibrary;

Type
  TPOrient = (tpoHorizontal,tpoVerticalUp,tpoVerticalDown);
  THPipeEndTypes = (thpeOPenUp,thpeOPenDown,thpeflat);
  TVPipeEndTypes = (tvpeOPenRight,tvpeOPenLeft,tvpeflat);
  TFlowDirection = (fdNotFlowing,fdForward,fdReverse);

  TPipePosData = record
    StartPos         : TPoint;
    EndPos           : TPoint;
    PipeOrientataion : TPOrient;
  end;

  THorizontalPipeMetrics = record
    PLength : Integer;
    PWidth  : Integer;
    LeftTriangle : TTrianglePoints;
    RightTriangle : TTrianglePoints;
    StartType,EndType : THPipeEndTypes;
    YposAdjust : Integer;
    XposAdjust : Integer;
    end;

  TVerticalPipeMetrics = record
    PLength : Integer;
    PWidth  : Integer;
    TopTriangle : TTrianglePoints;
    BottomTriangle : TTrianglePoints;
    StartType,EndType : TVPipeEndTypes;
    YposAdjust : Integer;
    XposAdjust : Integer;
  end;

 type TFlowPipe =  class(TImage)
  private
    FPipeWidth: Integer;
    FPipeOrientataion: TPOrient;
    FStartPos,FEndPos, FLength : Integer;
    AniBitMap : TBitMap;
    FFlowColour: TColor;
    FFlowDirection: TFlowDirection;
    UpdateCount : Integer;
    procedure SetPipeWidth(const Value: Integer);
    procedure SetPipeOrientataion(const Value: TPOrient);
    procedure DrawHPipe(PMetrics: THorizontalPipeMetrics);
    procedure DrawVDownPipe(PMetrics: TVerticalPipeMetrics);
    procedure DrawVUpPipe(PMetrics: TVerticalPipeMetrics);
    procedure ReCreateAniBitmap;
    procedure SetFlowColour(const Value: TColor);
    procedure SetFlowDirection(const Value: TFlowDirection);
  public
    PHMetrics : THorizontalPipeMetrics;
    PVMetrics : TVerticalPipeMetrics;
    AnimationStage : Integer;
    PosData : TPipePosData;
    procedure BeginUpdate;
    procedure EndUpdate;
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure ClockAnimation(ForceDraw : Boolean);
    procedure InitialiseGraphics();

    function CalculateHorizontalPipeMetrics(InPLength,InPWidth: Integer;InStartType,InEndType: THPipeEndTypes):THorizontalPipeMetrics;
    function CalculateDownVerticalPipeMetrics(InPLength,InPWidth: Integer;InStartType,InEndType: TVPipeEndTypes):TVerticalPipeMetrics;
    function CalculateUpVerticalPipeMetrics(InPLength, InPWidth: Integer;InStartType, InEndType: TVPipeEndTypes): TVerticalPipeMetrics;

    property PipeWidth : Integer read FPipeWidth write SetPipeWidth;
    property PipeOrientataion : TPOrient  read FPipeOrientataion write SetPipeOrientataion;
    property FlowColour : TColor read FFlowColour write SetFlowColour;
    property FlowDirection : TFlowDirection read FFlowDirection write SetFlowDirection;
  end;

TPipeData = record
    StartPos : TPoint;
    PLength : Integer;
    EndPos   : TPoint;
    Orientataion : TPOrient;
    HStartType,HEndType : THPipeEndTypes;
    VStartType,VEndType : TVPipeEndTypes;
    PipeWidth : Integer;
end;

pPipeData = ^TPipeData;

TFlowPipeGroup = Class(Tlist)
  private
    DataList : TList;
    FGroupPipeColour: TColor;
    FGroupFlowDirection: TFlowDirection;
    FGroupPipeWidth: Integer;
    FGroupEndPos: TPOint;
    DrawParent : TPanel;
    UpdateCount : Integer;
    procedure UpdateGroup;
    procedure SetPipeColor(const Value: TColor);
    procedure SetFlowDirection(const Value: TFlowDirection);
    procedure SetPipeWidth(const Value: Integer);
    procedure CleanDataList;
    procedure  AddPipeFromPipeData(PipeData : pPipeData);
    function HorizontalPipeAdd(PipeData : pPipeData;PColour : TColor) : TFlowPipe;
    function VerticalDownPipeAdd(PipeData: pPipeData;PColour : TColor): TFlowPipe;
    function VerticalUPPipeAdd(PipeData: pPipeData;PColour : TColor): TFlowPipe;


  public
    PipeStartPos : TPoint;
    procedure BeginUpdate;
    procedure EndUpdate;
    constructor Create;
    destructor Destroy; override;
    procedure Clean;
    procedure InitialiseOnPanel(MPaper : TPanel);
    procedure ClockAnimation(Force : Boolean);
    procedure RebuildGroupFromPipeData;

    procedure AddHorizontalPipe(FromPos : TPoint;InLength : Integer;InStartType, InEndType: THPipeEndTypes);
    procedure AddVerticalDownPipe(FromPos : TPoint;InLength : Integer;InStartType, InEndType: TVPipeEndTypes);
    procedure AddVerticalUPPipe(FromPos : TPoint;InLength : Integer;InStartType, InEndType: TVPipeEndTypes);

    property PipeColour : TColor read FGroupPipeColour write SetPipeColor;
    property Flow : TFlowDirection read FGroupFlowDirection write SetFlowDirection;
    property PipeWidth : Integer read FGroupPipeWidth write SetPipeWidth;
    property GroupEndPos : TPOint read FGroupEndPos;
  end;


implementation

{ TFlowPipe }

constructor TFlowPipe.Create(AOwner: TComponent);
begin
  inherited;
  FPipeOrientataion :=tpoHorizontal;
  AniBitMap := TBitmap.Create;
  Transparent := True;
  FFlowColour := clRed;
  FFlowDirection:=fdForward;
  UpdateCount := 0;
end;

destructor TFlowPipe.Destroy;
begin
  if assigned(AniBitMap) then
    AniBitMap.Free;
  inherited;
end;


procedure TFlowPipe.SetPipeOrientataion(const Value: TPOrient);
begin
  FPipeOrientataion := Value;
end;

procedure TFlowPipe.SetPipeWidth(const Value: Integer);
begin
  FPipeWidth := Value;
//  if UpdateCount < 1 then InitialiseGraphics;
end;



function TFlowPipe.CalculateDownVerticalPipeMetrics(InPLength, InPWidth: Integer; InStartType, InEndType: TVPipeEndTypes): TVerticalPipeMetrics;
begin
 with result do
   begin
   StartType := InStartType;
   EndType := InEndType;
   PLength:= InPLength+InPWidth+1;
   PWidth := InPWidth;
   case StartType of
    tvpeopenLeft:
      begin
      SetPoint(TopTriangle.P1,0,0);
      SetPoint(TopTriangle.P2,PWidth-1,0);
      SetPoint(TopTriangle.P3,0,Pwidth-1);
      SetPoint(TopTriangle.Midpoint,(Pwidth div 2),2);
      end;
    tvpeopenRight:
      begin
      SetPoint(TopTriangle.P1,0,0);
      SetPoint(TopTriangle.P2,0,Pwidth-1);
      SetPoint(TopTriangle.P3,Pwidth-1,Pwidth-1);
      SetPoint(TopTriangle.Midpoint,(Pwidth div 2),2);
      end
    end;

   case EndType of
    tvpeopenRight:
      begin
      SetPoint(BottomTriangle.P1,PWidth-1,PLength-1);
      SetPoint(BottomTriangle.P2,PWidth-1,PLength-Pwidth-1);
      SetPoint(BottomTriangle.P3,0,PLength-1);
      SetPoint(BottomTriangle.Midpoint,(Pwidth div 2),PLength-(Pwidth div 2));
      end;

    tvpeopenLeft:
      begin
      SetPoint(BottomTriangle.P1,0,PLength-1);
      SetPoint(BottomTriangle.P2,0,Plength-PWidth-1);
      SetPoint(BottomTriangle.P3,PWidth-1,Plength-1);
      SetPoint(BottomTriangle.Midpoint,(Pwidth div 2),PLength-2);
      end;
   end; // case
 YposAdjust := -PWidth div 2;
 XposAdjust := -PWidth div 2
 end;

end;

function TFlowPipe.CalculateUpVerticalPipeMetrics(InPLength, InPWidth: Integer; InStartType, InEndType: TVPipeEndTypes): TVerticalPipeMetrics;
begin
 with result do
   begin
   StartType := InStartType;
   EndType := InEndType;
   PLength:= InPLength+InPWidth+1;
   PWidth := InPWidth;
   case EndType of
    tvpeopenLeft:
      begin
      SetPoint(TopTriangle.P1,0,0);
      SetPoint(TopTriangle.P2,PWidth-1,0);
      SetPoint(TopTriangle.P3,0,Pwidth-1);
      SetPoint(TopTriangle.Midpoint,(Pwidth div 2),2);
      end;

    tvpeopenRight:
      begin
      SetPoint(TopTriangle.P1,0,0);
      SetPoint(TopTriangle.P2,PWidth-1,0);
      SetPoint(TopTriangle.P3,Pwidth-1,Pwidth-1);
      SetPoint(TopTriangle.Midpoint,(Pwidth div 2),2);
      end
    end;

   case StartType of
    tvpeopenRight:
      begin
      SetPoint(BottomTriangle.P1,PWidth-1,PLength-1);
      SetPoint(BottomTriangle.P2,PWidth-1,PLength-Pwidth-1);
      SetPoint(BottomTriangle.P3,0,PLength-1);
      SetPoint(BottomTriangle.Midpoint,(Pwidth div 2),PLength-(Pwidth div 2));
      end;

    tvpeopenLeft:
      begin
      SetPoint(BottomTriangle.P1,PWidth-1,PLength-1);
      SetPoint(BottomTriangle.P2,0,Plength-PWidth-1);
      SetPoint(BottomTriangle.P3,PWidth-1,Plength-1);
      SetPoint(BottomTriangle.Midpoint,(Pwidth div 2),PLength-2);
      end;
   end; // case
 YposAdjust := -PLength+PWidth div 2;
 XposAdjust := -PWidth div 2
 end;

end;

function TFlowPipe.CalculateHorizontalPipeMetrics(InPLength,InPWidth: Integer;InStartType,InEndType: THPipeEndTypes):THorizontalPipeMetrics;
begin
 with result do
   begin
   StartType := InStartType;
   EndType := InEndType;
   PLength:= InPLength+(InPWidth)+1;
   PWidth := InPWidth;
   case StartType of
    thpeopenUp:
      begin
      SetPoint(LeftTriangle.P1,0,0);
      SetPoint(LeftTriangle.P2,PWidth-1,0);
      SetPoint(LeftTriangle.P3,0,Pwidth-1);
      SetPoint(LeftTriangle.Midpoint,(Pwidth div 2)-2,(Pwidth div 2));
      end;
    thpeopenDown:
      begin
      SetPoint(LeftTriangle.P1,0,0);
      SetPoint(LeftTriangle.P2,0,Pwidth-1);
      SetPoint(LeftTriangle.P3,Pwidth,Pwidth-1);
      SetPoint(LeftTriangle.Midpoint,(Pwidth div 2)-2,(Pwidth div 2));
      end;
    thpeFlat:
      begin
      end;
    end;

   case EndType of
    thpeopenDown:
      begin
      SetPoint(RightTriangle.P1,Plength-1,0);
      SetPoint(RightTriangle.P2,Plength-1,PWidth-1);
      SetPoint(RightTriangle.P3,Plength-PWidth-1,PWidth-1);
      SetPoint(RightTriangle.Midpoint,PLength-(Pwidth div 2),(Pwidth div 2));
      end;

    thpeopenUp:
      begin
      SetPoint(RightTriangle.P1,Plength-PWidth,0);
      SetPoint(RightTriangle.P2,Plength,0);
      SetPoint(RightTriangle.P3,Plength,PWidth);
      SetPoint(RightTriangle.Midpoint,PLength-(Pwidth div 2)+1,(Pwidth div 2));
      end;
    thpeFlat:
      begin
      end;
    end;
   YposAdjust := -PWidth div 2;
   XposAdjust := PWidth div 2
   end; // with result

end;

procedure TFlowPipe.ReCreateAniBitmap;
var
Lpos,HPos : Integer;
begin
 If assigned(AniBitMap) then
   begin
   AniBitMap.Free;
   end;
 AniBitmap := TBitmap.Create;

 case FPipeOrientataion of
   tpoHorizontal:
   begin
   with AniBitMap do
     begin
     Width :=PHMetrics.Plength+(PHMetrics.Pwidth*2);
     Height := PHMetrics.PWidth;
     Canvas.Pen.Color := FFlowColour;
     end;
   Lpos := 0;
   while Lpos < PHMetrics.PLength do
    begin
    Hpos := 0;
    while Hpos < PHMetrics.PWidth do
      begin
      AniBitmap.Canvas.MoveTo(Lpos+HPos,0);
      AniBitmap.Canvas.LineTo(Lpos+Hpos+PHMetrics.PWidth,PHMetrics.PWidth);
      inc(Hpos);
      end;
    Lpos := LPos+(PHMetrics.PWidth*2)
    end;
   Canvas.Draw(-Height,1,AniBitmap);
   end;

   tpoVerticalDown,tpoVerticalUp:
   begin
   with AniBitMap do
     begin
     Height :=PVMetrics.Plength+(PVMetrics.Pwidth*2);
     Width := PVMetrics.PWidth;
     Canvas.Pen.Color := FFlowColour;
     end;
   Lpos := 0;
   while Lpos < PVMetrics.PLength do
    begin
    Hpos := 0;
    while Hpos < PVMetrics.PWidth do
      begin
      AniBitmap.Canvas.MoveTo(0,Lpos+HPos);
      AniBitmap.Canvas.LineTo(PVMetrics.PWidth,Lpos+Hpos+PVMetrics.PWidth);
      inc(Hpos);
      end;
    Lpos := LPos+(PVMetrics.PWidth*2)
    end;
   Canvas.Draw(-Height,1,AniBitmap);
   end;

 end; // case;


end;

procedure TFlowPipe.DrawVUpPipe(PMetrics: TVerticalPipeMetrics);
begin
 with Canvas do
  begin
  Pen.Color := clBlack;
  Pen.Width := 1;
  MoveTo(0,0);
  LineTo(0,Pmetrics.PLength+1);
  MoveTo(Pmetrics.PWidth-1,0);
  LineTo(Pmetrics.PWidth-1,Pmetrics.PLength+1);
  end;
  DrawTriangleOn(Canvas,Pmetrics.TopTriangle,clGreen,True);
  DrawTriangleOn(Canvas,Pmetrics.BottomTriangle,clGreen,True);
end;

procedure TFlowPipe.DrawVDownPipe(PMetrics: TVerticalPipeMetrics);
begin
with Canvas do
  begin
  Pen.Color := clBlack;
  Pen.Width := 1;
  MoveTo(0,0);
  LineTo(0,Pmetrics.PLength-1);
  MoveTo(Pmetrics.PWidth-1,1);
  LineTo(Pmetrics.PWidth-1,Pmetrics.PLength-1);
  end;
  DrawTriangleOn(Canvas,Pmetrics.TopTriangle,clGreen,True);
  DrawTriangleOn(Canvas,Pmetrics.BottomTriangle,clGreen,True);
end;

procedure TFlowPipe.DrawHPipe(PMetrics : THorizontalPipeMetrics);
begin
 with Canvas do
  begin
  Pen.Color := clBlack;
  Pen.Width := 1;
  MoveTo(0,0);
  LineTo(Pmetrics.PLength,0);
  MoveTo(0,Pmetrics.PWidth-1);
  LineTo(Pmetrics.PLength,Pmetrics.PWidth-1);
  end;
  if PMetrics.StartType <> thpeflat then
     DrawTriangleOn(Canvas,Pmetrics.LeftTriangle,clGreen,True)
  else
     begin
     Canvas.Pen.Color := clGreen;
     Canvas.MoveTo(0,Pmetrics.PWidth-1);
     Canvas.LineTo(0,Pmetrics.PWidth);
     end;
  DrawTriangleOn(Canvas,Pmetrics.RightTriangle,clGreen,True);
end;

procedure TFlowPipe.InitialiseGraphics();
begin
  case PipeOrientataion of
    tpoHorizontal:
    begin
    Width := PHMetrics.PLength;
    Height := PHMetrics.PWidth;
    ReCreateAniBitmap;
    ClockAnimation(True);
    end;

    tpoVerticalDown,tpoVerticalUp:
    begin
    Width := PVMetrics.PWidth;
    Height := PVMetrics.PLength;
    ReCreateAniBitmap;
    ClockAnimation(True);
    end;
  end; //case
end;

procedure TFlowPipe.ClockAnimation(ForceDraw : Boolean);
begin
if (FlowDirection <> fdNotFlowing) or ForceDraw  then
Case FPipeOrientataion of
  tpoHorizontal:
  begin
  Canvas.Draw(Animationstage-(PHMetrics.PWidth*2),0,AniBitmap);
  DrawHPipe(PHMetrics);
  if (FlowDirection = fdForward) or ForceDraw then
    begin
    Inc(AnimationStage);
    if AnimationStage >= (PHMetrics.PWidth*2) then AnimationStage := 0;
    end
  else
    begin
    Dec(AnimationStage);
    if AnimationStage < 0 then AnimationStage := (PHMetrics.PWidth*2);
    end;
  end;

  tpoVerticalDown:
  begin
  Canvas.Draw(0,Animationstage-(PVMetrics.PWidth*2),AniBitmap);
  DrawVDownPipe(PVMetrics);
  if (FlowDirection = fdForward) or ForceDraw then
    begin
    Inc(AnimationStage);
    if AnimationStage >= (PVMetrics.PWidth*2) then AnimationStage := 0;
    end
  else
    begin
    Dec(AnimationStage);
    if AnimationStage < 0 then AnimationStage := (PVMetrics.PWidth*2);
    end;
  end;

  tpoVerticalUp:
  begin
  Canvas.Draw(0,-Animationstage+(PVMetrics.PWidth*2),AniBitmap);
  DrawVUpPipe(PVMetrics);
  if (FlowDirection = fdForward) or ForceDraw then
    begin
    Inc(AnimationStage);
    if AnimationStage >= (PVMetrics.PWidth*2) then AnimationStage := 0;
    end
  else
    begin
    Dec(AnimationStage);
    if AnimationStage < 0 then AnimationStage := (PVMetrics.PWidth*2);
    end;
  end;
end; //Case
end;



procedure TFlowPipe.SetFlowColour(const Value: TColor);
begin
  FFlowColour := Value;
//  if UpdateCount < 1 then InitialiseGraphics;
//  InitialiseGraphics;
end;

procedure TFlowPipe.SetFlowDirection(const Value: TFlowDirection);
begin
  FFlowDirection := Value;
end;
{
    PLength : Integer;
    PWidth  : Integer;
    LeftTriangle : TTrianglePoints;
    RightTriangle : TTrianglePoints;
    StartType,EndType : THPipeEndTypes;
    YposAdjust : Integer;
    XposAdjust : Integer;
}
procedure TFlowPipe.EndUpdate;
begin
UpdateCount := 0;
InitialiseGraphics;
end;

procedure TFlowPipe.BeginUpdate;
begin
inc(UpdateCount);
end;

{ TFlowPipeGroup }

procedure TFlowPipeGroup.AddHorizontalPipe(FromPos: TPoint; InLength: Integer; InStartType, InEndType: THPipeEndTypes);
var
PipeData : pPipeData;
begin
New(PipeData);
PipeData.StartPos := FromPos;
PipeData.PLength := InLength;
PipeData.Orientataion := tpoHorizontal;
PipeData.PipeWidth := FGroupPipeWidth;
PipeData.HStartType := InStartType;
PipeData.HEndType := InEndType;
PipeData.EndPos := FromPos;
PipeData.EndPos.x := FromPos.x+InLength;
DataList.Add(PipeData);
FGroupEndPos := PipeData.EndPos;
end;


function TFlowPipeGroup.HorizontalPipeAdd(PipeData : pPipeData;PColour : TColor) : TFlowPipe;
var
NewPipe : TFlowPipe;
begin
Result := nil;
NewPipe := TFlowPipe.Create(nil);
with NewPipe do
  begin
  PipeWidth := FGroupPipeWidth;
  PipeOrientataion := tpoHorizontal;
  PHMetrics := CalculateHorizontalPipeMetrics(PipeData.PLength,PipeWidth,PipeData.HStartType,PipeData.HEndType);
  FlowColour := PColour;
  FlowDirection := FGroupFlowDirection;
  InitialiseGraphics;
  PosData.StartPos := PipeData.StartPos;
  PosData.EndPos := PipeData.EndPos;
  PosData.PipeOrientataion := tpoHorizontal;
  end;

if assigned(DrawParent) then
  begin
  NewPipe.Parent := DrawParent;
  NewPipe.Top := NewPipe.PosData.StartPos.y;
  NewPipe.Left:= NewPipe.PosData.StartPos.x;
  end;

Add(NewPipe);
Result := NewPipe;
end;

procedure TFlowPipeGroup.AddPipeFromPipeData(PipeData: pPipeData);
begin
case PipeData.Orientataion of

   tpoHorizontal:
    begin
    HorizontalPipeAdd(PipeData,FGroupPipeColour);
    end;

   tpoVerticalUp:
    begin
    VerticalUpPipeAdd(PipeData,FGroupPipeColour);
    end;

   tpoVerticalDown:
    begin
    VerticalDownPipeAdd(PipeData,FGroupPipeColour);
    end;
end; // case
end;

procedure TFlowPipeGroup.AddVerticalDownPipe(FromPos: TPoint; InLength: Integer;InStartType, InEndType: TVPipeEndTypes);
var
PipeData : pPipeData;
begin
New(PipeData);
PipeData.StartPos := FromPos;
PipeData.PLength := InLength;
PipeData.Orientataion := tpoVerticalDown;
PipeData.PipeWidth := FGroupPipeWidth;
PipeData.VStartType := InStartType;
PipeData.VEndType := InEndType;
PipeData.EndPos := FromPos;
PipeData.EndPos.y := FromPos.y+InLength;
DataList.Add(PipeData);
FGroupEndPos := PipeData.EndPos;
end;


function TFlowPipeGroup.VerticalDownPipeAdd(PipeData: pPipeData;PColour : TColor): TFlowPipe;
var
NewPipe : TFlowPipe;
begin
Result := nil;
NewPipe := TFlowPipe.Create(nil);
with NewPipe do
  begin
  PipeWidth := FGroupPipeWidth;
  PipeOrientataion := tpoVerticalDown;
  PVMetrics := CalculateDownVerticalPipeMetrics(PipeData.PLength,PipeWidth,PipeData.VStartType,PipeData.VEndType);
  FlowColour := PColour;
  FlowDirection := FGroupFlowDirection;
  InitialiseGraphics;
  PosData.StartPos := PipeData.StartPos;
  PosData.EndPos := PipeData.EndPos;
  PosData.PipeOrientataion := tpoVerticalDown;
  end;
if assigned(DrawParent) then
  begin
  NewPipe.Parent := DrawParent;
  NewPipe.Top := NewPipe.PosData.StartPos.y;
  NewPipe.Left:= NewPipe.PosData.StartPos.x;
  end;

  Add(NewPipe);
Result := NewPipe;
end;

procedure TFlowPipeGroup.AddVerticalUPPipe(FromPos: TPoint; InLength: Integer; InStartType, InEndType: TVPipeEndTypes);
var
PipeData : pPipeData;
begin
New(PipeData);
PipeData.StartPos := FromPos;
PipeData.PLength := InLength;
PipeData.Orientataion := tpoVerticalUp;
PipeData.PipeWidth := FGroupPipeWidth;
PipeData.VStartType := InStartType;
PipeData.VEndType := InEndType;
PipeData.EndPos := FromPos;
PipeData.EndPos.y := FromPos.y-InLength;
DataList.Add(PipeData);
FGroupEndPos := PipeData.EndPos;
end;


function TFlowPipeGroup.VerticalUPPipeAdd(PipeData: pPipeData;PColour : TColor): TFlowPipe;
var
NewPipe : TFlowPipe;
begin
Result := nil;
NewPipe := TFlowPipe.Create(nil);
with NewPipe do
  begin
  PipeWidth := FGroupPipeWidth;
  PipeOrientataion := tpoVerticalUp;
  PVMetrics := CalculateUpVerticalPipeMetrics(PipeData.PLength,PipeWidth,PipeData.VStartType,PipeData.VEndType);
  FlowColour := PColour;
  FlowDirection := FGroupFlowDirection;
  InitialiseGraphics;
  PosData.StartPos := PipeData.StartPos;
  PosData.EndPos := PipeData.EndPos;
  PosData.PipeOrientataion := tpoVerticalUp;
  end;

 if assigned(DrawParent) then
  begin
  NewPipe.Parent := DrawParent;
  NewPipe.Top := NewPipe.PosData.EndPos.y;
  NewPipe.Left:= NewPipe.PosData.EndPos.x;
  end;

Add(NewPipe);
Result := NewPipe;
end;

procedure TFlowPipeGroup.Clean;
var
Pipe : TFlowPipe;
begin
while Count > 0 do
  begin
  Pipe := Items[0];
  Pipe.Free;
  Delete(0);
  end
end;

procedure TFlowPipeGroup.CleanDataList;
var
DataEntry : pPipeData;
begin
while DataList.Count > 0 do
  begin
  DataEntry := DataList[0];
  Dispose(DataEntry);
  DataList.Delete(0);
  end;
end;

procedure TFlowPipeGroup.ClockAnimation(Force : Boolean);
var
Pipe : TFlowPipe;
PNum : Integer;
begin
if Count > 0 then
  begin
  For PNum := 0 to Count-1 do
    begin
    Pipe := Items[Pnum];
    Pipe.ClockAnimation(Force);
    end;
  end
end;

constructor TFlowPipeGroup.Create;
begin
  inherited;
  SetPOint(PipeStartPos,0,0);
  SetPOint(FGroupEndPos,0,0);
  FGroupPipeWidth := 10;
  FGroupPipeColour := clred;
  DrawParent := nil;
  DataList := TList.Create;
  UpdateCount := 0;
  FGroupFlowDirection := fdForward;
end;

destructor TFlowPipeGroup.Destroy;
begin
  CleanDataList;
  Clean;
  inherited;
end;

procedure TFlowPipeGroup.InitialiseOnPanel(MPaper: TPanel);
begin
DrawParent := MPaper;
end;

procedure TFlowPipeGroup.RebuildGroupFromPipeData;
var
PNum : Integer;
Pipe : TFlowPipe;
PipeData : pPipeData;
begin
Clean;
if DataList.Count > 0 then
  begin
  For PNum:= 0 to DataList.Count-1 do
    begin
    AddPipeFromPipeData(Datalist[Pnum]);
    end;
  end;
end;

procedure TFlowPipeGroup.SetFlowDirection(const Value: TFlowDirection);
begin
  FGroupFlowDirection := Value;
  UpdateGroup;
end;

procedure TFlowPipeGroup.SetPipeColor(const Value: TColor);
begin
  FGroupPipeColour := Value;
  UpdateGroup;
end;

procedure TFlowPipeGroup.SetPipeWidth(const Value: Integer);
begin
  FGroupPipeWidth := Value;
  UpdateGroup;
end;

procedure TFlowPipeGroup.UpdateGroup;
var
Pipe : TFlowPipe;
PNum : Integer;
begin
RebuildGroupFromPipeData;
end;

procedure TFlowPipeGroup.EndUpdate;
begin
if UpdateCount > 0 then
  begin
  RebuildGroupFromPipeData;
  UpdateCount := 0;
  end;
end;

procedure TFlowPipeGroup.BeginUpdate;
begin
inc(UpdateCount);
end;

end.
