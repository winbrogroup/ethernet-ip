unit MimicCanvasComponents;

interface
uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls,BitLibrary,FlowPipes;

type

  TMimicPaper = class(TPanel)
    private
    AniTimer : TTimer;
    OffscreenCanvas : TBitmap;
    PipeList : TList;
    procedure CleanPipeList;

    public
    Constructor Create(AOwner: TComponent); override;
    Destructor Destroy; override;
    procedure Initialize(BColor : TColor);
    function AddHorizontalPipe(FromPos:TPoint;PLength,PWidth: Integer;StartType,EndType: THPipeEndTypes):TFlowPipe;
    function AddDownVerticalPipe(FromPos:TPoint;PLength,PWidth: Integer;StartType,EndType: TVPipeEndTypes):TFlowPipe;
    function AddUpVerticalPipe(FromPos:TPoint;PLength,PWidth: Integer;StartType,EndType: TVPipeEndTypes):TFlowPipe;
    procedure ClockAnimation;
    procedure SetAlPipeToColour(PColour : TColor);
  end;

implementation


{ TMimicPaper }
function TMimicPaper.AddUpVerticalPipe(FromPos: TPoint; PLength, PWidth: Integer;
  StartType, EndType: TVPipeEndTypes): TFlowPipe;
begin
Result := TFlowPipe.Create(nil);
PipeList.Add(Result);
with Result do
  begin
  PipeWidth := PWidth;
  PipeOrientataion := tpoVerticalUp;
  PVMetrics := CalculateUpVerticalPipeMetrics(PLength,PipeWidth,StartType,EndType);
  InitialiseGraphics;
  end;
 Result.Parent := Self;
 with Result do
  begin
  Top := FromPos.y+Result.PVMetrics.YposAdjust;
  Left := FromPos.x+Result.PVMetrics.XposAdjust ;
  PosData.StartPos := FromPos;
  PosData.EndPos.x := FromPos.x;
  PosData.EndPos.y := FromPos.y-Plength;
  PosData.PipeOrientataion := tpoVerticalUp;
  end;
end;

function TMimicPaper.AddDownVerticalPipe(FromPos: TPoint; PLength, PWidth: Integer;
  StartType, EndType: TVPipeEndTypes): TFlowPipe;
begin
Result := TFlowPipe.Create(nil);
PipeList.Add(Result);
with Result do
  begin
  PipeWidth := PWidth;
  PipeOrientataion := tpoVerticalDown;
  PVMetrics := CalculateDownVerticalPipeMetrics(PLength,PipeWidth,StartType,EndType);
  InitialiseGraphics;
  end;
 Result.Parent := Self;
 with Result do
  begin
  Top := FromPos.y+Result.PVMetrics.YposAdjust;
  Left := FromPos.x+Result.PVMetrics.XposAdjust ;
  PosData.StartPos := FromPos;
  PosData.EndPos.x := FromPos.x;
  PosData.EndPos.y := FromPos.y+Plength;
  PosData.PipeOrientataion := tpoVerticalDown;
  end;
end;

function TMimicPaper.AddHorizontalPipe(FromPos: TPoint; PLength,PWidth: Integer; StartType, EndType: THPipeEndTypes):TFlowPipe;
begin
Result := TFlowPipe.Create(nil);
PipeList.Add(Result);
with Result do
  begin
  PipeWidth := PWidth;
  PipeOrientataion := tpoHorizontal;
  PHMetrics := CalculateHorizontalPipeMetrics(PLength,PipeWidth,StartType,EndType);
  InitialiseGraphics;
  end;

 Result.Parent := Self;
 with Result do
  begin
  Top := FromPos.y-(Pwidth div 2);
  Left := FromPos.x-(Pwidth div 2);
  PosData.StartPos := FromPos;
  PosData.EndPos.y := FromPos.y;
  PosData.EndPos.x := FromPos.X+Plength;
  PosData.PipeOrientataion := tpoHorizontal;
  end;
end;


procedure TMimicPaper.CleanPipeList;
var
Pipe: TFlowPipe;
begin
while PipeList.Count > 0 do
  begin
  Pipe := PipeList[0];
  Pipe.Free;
  PipeList.Delete(0);
  end;
end;

procedure TMimicPaper.ClockAnimation;
var
Pipe : TFlowPipe;
PNum : INteger;
begin
if PipeList.Count > 0 then
  begin
  For PNum := 0 to PipeList.Count-1 do
    begin
    Pipe := PipelIst[PNum];
    Pipe.ClockAnimation(False);
    end;
  end
end;

constructor TMimicPaper.Create(AOwner: TComponent);
begin
  inherited;
  OffscreenCanvas := TBitmap.Create;
  PipeList := TList.Create;

  AniTimer := TTimer.create(nil);
  with Anitimer do
    begin
    Enabled := False;
    Interval := 2000;
    end;

end;

destructor TMimicPaper.Destroy;
begin
  Anitimer.Enabled := false;
  AniTimer.Free;
  CleanPipeList;
  PipeList.Free;
  inherited;
end;

procedure TMimicPaper.Initialize(BColor: TColor);
begin
  Color := BColor;
  CleanPipeList;
end;

procedure TMimicPaper.SetAlPipeToColour(PColour: TColor);

var
PNum : Integer;
Pipe : TFlowPipe;
begin
if PipeList.Count > 0 then
  begin
  For Pnum := 0 to PipeList.Count-1 do
    begin
    Pipe := PipeList[Pnum];
    Pipe.FlowColour := Pcolour;
    end;
   end;
end;

end.




