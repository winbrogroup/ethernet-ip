unit BitLibrary;

interface
uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls;

type
TTrianglePoints = record
  P1 : TPoint;
  P2 : TPoint;
  P3 : TPoint;
  MidPOint : TPoint;
end;

procedure DrawTriangleOn(DCanvas : TCanvas;TriPoints :TTrianglePoints;DColour : TColor;IsFilled : Boolean);
procedure SetPoint(var Point : TPoint;X,Y : INteger);

implementation
procedure DrawTriangleOn(DCanvas : TCanvas;TriPoints :TTrianglePoints;DColour : TColor;IsFilled : Boolean);
var
MidPint : TPoint;
begin
 with DCanvas do
  begin
  Pen.Color := DColour;
  Pen.Width := 1;
  MoveTo(TriPoints.P1.x,TriPoints.P1.y);
  LineTo(TriPoints.P2.x,TriPoints.P2.y);
  LineTo(TriPoints.P3.x,TriPoints.P3.y);
  LineTo(TriPoints.P1.x,TriPoints.P1.y);
  if IsFilled then
     begin
     Brush.Color := DColour;
     FloodFill(TriPoints.MidPoint.x,TriPoints.MidPoint.y,DColour,fsBorder);
     end;
  end;

end;

procedure  SetPoint(var Point : TPoint;X,Y : INteger);
begin
Point.x := X;
Point.y := Y;
end;


end.
