object WaterMimicPanel: TWaterMimicPanel
  Left = 0
  Top = 0
  BorderStyle = bsNone
  Caption = 'WaterPlanyMimicPanel'
  ClientHeight = 460
  ClientWidth = 643
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 643
    Height = 35
    Align = alTop
    BevelInner = bvLowered
    Caption = 'Water Plant Data'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    object Button1: TButton
      Left = 2
      Top = 3
      Width = 75
      Height = 30
      Caption = 'Close'
      TabOrder = 0
      OnClick = Button1Click
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 35
    Width = 643
    Height = 425
    Align = alClient
    BevelInner = bvLowered
    Caption = 'Panel2'
    TabOrder = 1
    object PageControl1: TPageControl
      Left = 2
      Top = 2
      Width = 639
      Height = 421
      ActivePage = TabSheet2
      Align = alClient
      TabOrder = 0
      object TabSheet1: TTabSheet
        Caption = 'TagGrid'
        object TagGrid: TStringGrid
          Left = 0
          Top = 0
          Width = 465
          Height = 393
          Align = alLeft
          RowCount = 6
          TabOrder = 0
        end
        object Edit1: TEdit
          Left = 512
          Top = 32
          Width = 121
          Height = 21
          TabOrder = 1
          Text = 'Edit1'
        end
      end
      object TabSheet2: TTabSheet
        Caption = 'TabSheet2'
        ImageIndex = 1
        object Panel3: TPanel
          Left = 0
          Top = 0
          Width = 631
          Height = 393
          Align = alClient
          BevelInner = bvLowered
          Caption = 'Panel3'
          TabOrder = 0
          object Panel4: TPanel
            Left = 2
            Top = 2
            Width = 627
            Height = 25
            Align = alTop
            Caption = 'Panel4'
            TabOrder = 0
            object Button2: TButton
              Left = 16
              Top = 2
              Width = 75
              Height = 20
              Caption = 'test'
              TabOrder = 0
              OnClick = Button2Click
            end
          end
        end
      end
    end
  end
end
