object GEO: TGEO
  Left = 251
  Top = 200
  BorderIcons = []
  BorderStyle = bsNone
  Caption = 'Amchem Geometry Engine Object (GEO) V1.4'
  ClientHeight = 456
  ClientWidth = 694
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = True
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Paper: TG3DDisplay
    Left = 8
    Top = 0
    Width = 537
    Height = 521
    Scale = 10.000000000000000000
    ShowMode = smAll
  end
  object MachinePanel: TPanel
    Left = 24
    Top = 152
    Width = 400
    Height = 208
    Caption = 'MachinePanel'
    TabOrder = 0
    object MachinePages: TPageControl
      Left = 1
      Top = 1
      Width = 398
      Height = 206
      ActivePage = GEOPositionSheet
      Align = alClient
      TabHeight = 40
      TabOrder = 0
      object GEOPositionSheet: TTabSheet
        Caption = 'Position'
        ImageIndex = 4
        object VersionLabel: TLabel
          Left = 300
          Top = 0
          Width = 40
          Height = 13
          Caption = 'V1.7.0.0'
          Visible = False
        end
        object GEOPosGrid: TStringGrid
          Left = 0
          Top = 0
          Width = 200
          Height = 156
          Align = alLeft
          ColCount = 2
          RowCount = 7
          TabOrder = 0
          RowHeights = (
            24
            24
            24
            24
            24
            24
            24)
        end
        object ShowPCMButton: TBitBtn
          Left = 280
          Top = 74
          Width = 96
          Height = 50
          Caption = 'Show PCM'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlue
          Font.Height = -13
          Font.Name = 'Comic Sans MS'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 1
          Visible = False
          OnClick = ShowPCMButtonClick
        end
        object HidePCM: TBitBtn
          Left = 280
          Top = 18
          Width = 96
          Height = 50
          Caption = 'Hide PCM'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlue
          Font.Height = -13
          Font.Name = 'Comic Sans MS'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 2
          Visible = False
          OnClick = HidePCMClick
        end
      end
      object MachineDisplay: TTabSheet
        Caption = 'Display'
        object MachineDisplayPanel: TPanel
          Left = 205
          Top = 0
          Width = 185
          Height = 156
          BevelOuter = bvNone
          TabOrder = 0
          object MachineFitButton: TTouchGlyphSoftkey
            Left = 0
            Top = 0
            Width = 89
            Height = 50
            Legend = 'Fit~Machine'
            TextSize = 0
            HasIndicator = False
            IndicatorOn = False
            IsEnabled = True
            ButtonColor = clBtnFace
            Layout = slGlyphLeft
            AutoTextSize = True
            KeyStyle = ssRectangle
            CornerRad = 8
            ButtonTravel = 2
            ShadowColor = clGray
            LegendFont.Charset = ANSI_CHARSET
            LegendFont.Color = clBtnText
            LegendFont.Height = 20
            LegendFont.Name = 'Tahoma'
            LegendFont.Style = []
            GlyphToEdgeSpacing = 1
            TextToEdgeBorder = 5
            MultiLineSpacing = 0
            MaxFontSize = 24
            OnMouseUp = MachineFitButtonMouseUp
          end
          object PointFitButton: TTouchGlyphSoftkey
            Left = 88
            Top = 0
            Width = 100
            Height = 50
            Legend = 'Fit~Points'
            TextSize = 0
            HasIndicator = True
            IndicatorOn = False
            IsEnabled = True
            ButtonColor = clBtnFace
            Layout = slGlyphLeft
            AutoTextSize = True
            KeyStyle = ssRectangle
            CornerRad = 8
            ButtonTravel = 2
            ShadowColor = clGray
            LegendFont.Charset = ANSI_CHARSET
            LegendFont.Color = clBtnText
            LegendFont.Height = 20
            LegendFont.Name = 'Tahoma'
            LegendFont.Style = []
            GlyphToEdgeSpacing = 1
            TextToEdgeBorder = 5
            MultiLineSpacing = 0
            MaxFontSize = 24
            OnMouseUp = PointFitButtonMouseUp
          end
          object GEOZoomIn: TTouchGlyphSoftkey
            Left = 0
            Top = 56
            Width = 89
            Height = 50
            Legend = 'Zoom~In'
            TextSize = 0
            HasIndicator = False
            IndicatorOn = False
            IsEnabled = True
            ButtonColor = clBtnFace
            Glyph.Data = {
              76010000424D7601000000000000760000002800000020000000100000000100
              04000000000000010000130B0000130B00001000000000000000000000000000
              800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
              33033333333333333F7F3333333333333000333333333333F777333333333333
              000333333333333F777333333333333000333333333333F77733333333333300
              033333333FFF3F777333333700073B703333333F7773F77733333307777700B3
              33333377333777733333307F8F8F7033333337F333F337F3333377F8F9F8F773
              3333373337F3373F3333078F898F870333337F33F7FFF37F333307F99999F703
              33337F377777337F3333078F898F8703333373F337F33373333377F8F9F8F773
              333337F3373337F33333307F8F8F70333333373FF333F7333333330777770333
              333333773FF77333333333370007333333333333777333333333}
            Layout = slCentreOverlay
            AutoTextSize = True
            KeyStyle = ssRectangle
            CornerRad = 8
            ButtonTravel = 2
            ShadowColor = clGray
            LegendFont.Charset = ANSI_CHARSET
            LegendFont.Color = clBtnText
            LegendFont.Height = 17
            LegendFont.Name = 'Tahoma'
            LegendFont.Style = []
            GlyphToEdgeSpacing = 5
            TextToEdgeBorder = 5
            NumGlyphs = 2
            MultiLineSpacing = 6
            MaxFontSize = 24
            OnMouseUp = GEOZoomInMouseUp
          end
          object GEOZoomOut: TTouchGlyphSoftkey
            Left = 88
            Top = 56
            Width = 100
            Height = 50
            Legend = 'Zoom~Out'
            TextSize = 0
            HasIndicator = False
            IndicatorOn = False
            IsEnabled = True
            ButtonColor = clBtnFace
            Glyph.Data = {
              76010000424D7601000000000000760000002800000020000000100000000100
              04000000000000010000130B0000130B00001000000000000000000000000000
              800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
              33033333333333333F7F3333333333333000333333333333F777333333333333
              000333333333333F777333333333333000333333333333F77733333333333300
              033333333FFF3F777333333700073B703333333F7773F77733333307777700B3
              333333773337777333333078F8F87033333337F3333337F33333778F8F8F8773
              333337333333373F333307F8F8F8F70333337F33FFFFF37F3333078999998703
              33337F377777337F333307F8F8F8F703333373F3333333733333778F8F8F8773
              333337F3333337F333333078F8F870333333373FF333F7333333330777770333
              333333773FF77333333333370007333333333333777333333333}
            Layout = slCentreOverlay
            AutoTextSize = True
            KeyStyle = ssRectangle
            CornerRad = 8
            ButtonTravel = 2
            ShadowColor = clGray
            LegendFont.Charset = ANSI_CHARSET
            LegendFont.Color = clBtnText
            LegendFont.Height = 17
            LegendFont.Name = 'Tahoma'
            LegendFont.Style = []
            GlyphToEdgeSpacing = 5
            TextToEdgeBorder = 5
            NumGlyphs = 2
            MultiLineSpacing = 6
            MaxFontSize = 24
            OnMouseUp = GEOZoomOutMouseUp
          end
          object GEOCloseBtn: TTouchGlyphSoftkey
            Left = 48
            Top = 104
            Width = 100
            Height = 50
            Legend = 'Close~GEO'
            TextSize = 0
            HasIndicator = False
            IndicatorOn = False
            IsEnabled = True
            ButtonColor = clBtnFace
            Layout = slCentreOverlay
            AutoTextSize = True
            KeyStyle = ssRectangle
            CornerRad = 8
            ButtonTravel = 2
            ShadowColor = clGray
            LegendFont.Charset = ANSI_CHARSET
            LegendFont.Color = clRed
            LegendFont.Height = 17
            LegendFont.Name = 'Tahoma'
            LegendFont.Style = [fsBold]
            GlyphToEdgeSpacing = 0
            TextToEdgeBorder = 8
            MultiLineSpacing = 0
            MaxFontSize = 24
            OnMouseUp = GEOCloseBtnMouseUp
          end
        end
      end
      object MachinePoints: TTabSheet
        Caption = 'Points'
        ImageIndex = 1
        object ReportPointGrid: TStringGrid
          Left = 0
          Top = 0
          Width = 262
          Height = 156
          Align = alLeft
          ColCount = 4
          DefaultColWidth = 50
          DefaultRowHeight = 20
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -8
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goDrawFocusSelected]
          ParentFont = False
          TabOrder = 0
          OnSelectCell = ReportPointGridSelectCell
          RowHeights = (
            20
            20
            20
            20
            20)
        end
        object GEOPointsUP: TTouchGlyphSoftkey
          Left = 280
          Top = 16
          Width = 100
          Height = 50
          TextSize = 0
          HasIndicator = False
          IndicatorOn = False
          IsEnabled = True
          ButtonColor = clBtnFace
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            04000000000000010000120B0000120B00001000000000000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333000333
            3333333333777F33333333333309033333333333337F7F333333333333090333
            33333333337F7F33333333333309033333333333337F7F333333333333090333
            33333333337F7F33333333333309033333333333FF7F7FFFF333333000090000
            3333333777737777F333333099999990333333373F3333373333333309999903
            333333337F33337F33333333099999033333333373F333733333333330999033
            3333333337F337F3333333333099903333333333373F37333333333333090333
            33333333337F7F33333333333309033333333333337373333333333333303333
            333333333337F333333333333330333333333333333733333333}
          Layout = slCentreOverlay
          AutoTextSize = True
          KeyStyle = ssRectangle
          CornerRad = 10
          ButtonTravel = 2
          ShadowColor = clGray
          LegendFont.Charset = DEFAULT_CHARSET
          LegendFont.Color = clBlue
          LegendFont.Height = -19
          LegendFont.Name = 'Comic Sans MS'
          LegendFont.Style = []
          GlyphToEdgeSpacing = 5
          TextToEdgeBorder = 5
          NumGlyphs = 2
          MultiLineSpacing = 0
          MaxFontSize = 24
          OnMouseUp = GEOPointsUPMouseUp
        end
        object GEOPointsDown: TTouchGlyphSoftkey
          Left = 280
          Top = 80
          Width = 100
          Height = 50
          TextSize = 0
          HasIndicator = False
          IndicatorOn = False
          IsEnabled = True
          ButtonColor = clBtnFace
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            04000000000000010000120B0000120B00001000000000000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333303333
            333333333337F33333333333333033333333333333373F333333333333090333
            33333333337F7F33333333333309033333333333337373F33333333330999033
            3333333337F337F33333333330999033333333333733373F3333333309999903
            333333337F33337F33333333099999033333333373333373F333333099999990
            33333337FFFF3FF7F33333300009000033333337777F77773333333333090333
            33333333337F7F33333333333309033333333333337F7F333333333333090333
            33333333337F7F33333333333309033333333333337F7F333333333333090333
            33333333337F7F33333333333300033333333333337773333333}
          Layout = slCentreOverlay
          AutoTextSize = True
          KeyStyle = ssRectangle
          CornerRad = 10
          ButtonTravel = 2
          ShadowColor = clGray
          LegendFont.Charset = DEFAULT_CHARSET
          LegendFont.Color = clBlue
          LegendFont.Height = -19
          LegendFont.Name = 'Comic Sans MS'
          LegendFont.Style = []
          GlyphToEdgeSpacing = 5
          TextToEdgeBorder = 5
          NumGlyphs = 2
          MultiLineSpacing = 0
          MaxFontSize = 24
          OnMouseUp = GEOPointsDownMouseUp
        end
      end
      object MachineVectors: TTabSheet
        Caption = 'Vectors'
        ImageIndex = 2
        object VectorPanel: TPanel
          Left = 0
          Top = 0
          Width = 390
          Height = 341
          Align = alTop
          TabOrder = 0
          object Label10: TLabel
            Left = 8
            Top = 294
            Width = 42
            Height = 19
            Caption = 'Length'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Comic Sans MS'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object PositionTitle: TPanel
            Left = 2
            Top = 36
            Width = 215
            Height = 20
            BevelInner = bvLowered
            Caption = 'Position'
            Color = clAqua
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlue
            Font.Height = -13
            Font.Name = 'Comic Sans MS'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 0
          end
          object VectorDataGrid: TStringGrid
            Left = 2
            Top = 56
            Width = 215
            Height = 80
            TabStop = False
            ColCount = 4
            DefaultColWidth = 52
            DefaultRowHeight = 18
            Enabled = False
            RowCount = 4
            Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine]
            ScrollBars = ssNone
            TabOrder = 1
          end
          object VectorChoosePanel: TPanel
            Left = 1
            Top = 1
            Width = 388
            Height = 32
            Align = alTop
            TabOrder = 2
            object VectorSelectList: TComboBox
              Left = 5
              Top = 1
              Width = 210
              Height = 28
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -16
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ItemHeight = 20
              ParentFont = False
              TabOrder = 0
              OnChange = VectorSelectListChange
              Items.Strings = (
                'ITEM1'
                'ITEM2')
            end
          end
          object VectorRotationGrid: TStringGrid
            Left = 2
            Top = 152
            Width = 215
            Height = 20
            TabStop = False
            ColCount = 4
            DefaultColWidth = 52
            DefaultRowHeight = 17
            RowCount = 1
            FixedRows = 0
            ScrollBars = ssNone
            TabOrder = 3
          end
          object RotationTitle: TPanel
            Left = 2
            Top = 134
            Width = 215
            Height = 20
            BevelInner = bvLowered
            Caption = 'Rotation'
            Color = clAqua
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlue
            Font.Height = -13
            Font.Name = 'Comic Sans MS'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 4
          end
          object VectorLengthDisplay: TEdit
            Left = 56
            Top = 292
            Width = 97
            Height = 21
            TabStop = False
            ReadOnly = True
            TabOrder = 5
          end
          object GEOVectorUp: TTouchGlyphSoftkey
            Left = 264
            Top = 40
            Width = 100
            Height = 50
            TextSize = 0
            HasIndicator = False
            IndicatorOn = False
            IsEnabled = True
            ButtonColor = clBtnFace
            Glyph.Data = {
              76010000424D7601000000000000760000002800000020000000100000000100
              04000000000000010000120B0000120B00001000000000000000000000000000
              800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333000333
              3333333333777F33333333333309033333333333337F7F333333333333090333
              33333333337F7F33333333333309033333333333337F7F333333333333090333
              33333333337F7F33333333333309033333333333FF7F7FFFF333333000090000
              3333333777737777F333333099999990333333373F3333373333333309999903
              333333337F33337F33333333099999033333333373F333733333333330999033
              3333333337F337F3333333333099903333333333373F37333333333333090333
              33333333337F7F33333333333309033333333333337373333333333333303333
              333333333337F333333333333330333333333333333733333333}
            Layout = slCentreOverlay
            AutoTextSize = True
            KeyStyle = ssRectangle
            CornerRad = 10
            ButtonTravel = 2
            ShadowColor = clGray
            LegendFont.Charset = DEFAULT_CHARSET
            LegendFont.Color = clBlue
            LegendFont.Height = -19
            LegendFont.Name = 'Comic Sans MS'
            LegendFont.Style = []
            GlyphToEdgeSpacing = 5
            TextToEdgeBorder = 5
            NumGlyphs = 2
            MultiLineSpacing = 0
            MaxFontSize = 24
            OnMouseUp = GEOVectorUpMouseUp
          end
          object GeoVectorDown: TTouchGlyphSoftkey
            Left = 264
            Top = 96
            Width = 100
            Height = 50
            TextSize = 0
            HasIndicator = False
            IndicatorOn = False
            IsEnabled = True
            ButtonColor = clBtnFace
            Glyph.Data = {
              76010000424D7601000000000000760000002800000020000000100000000100
              04000000000000010000120B0000120B00001000000000000000000000000000
              800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333303333
              333333333337F33333333333333033333333333333373F333333333333090333
              33333333337F7F33333333333309033333333333337373F33333333330999033
              3333333337F337F33333333330999033333333333733373F3333333309999903
              333333337F33337F33333333099999033333333373333373F333333099999990
              33333337FFFF3FF7F33333300009000033333337777F77773333333333090333
              33333333337F7F33333333333309033333333333337F7F333333333333090333
              33333333337F7F33333333333309033333333333337F7F333333333333090333
              33333333337F7F33333333333300033333333333337773333333}
            Layout = slCentreOverlay
            AutoTextSize = True
            KeyStyle = ssRectangle
            CornerRad = 10
            ButtonTravel = 2
            ShadowColor = clGray
            LegendFont.Charset = DEFAULT_CHARSET
            LegendFont.Color = clBlue
            LegendFont.Height = -19
            LegendFont.Name = 'Comic Sans MS'
            LegendFont.Style = []
            GlyphToEdgeSpacing = 5
            TextToEdgeBorder = 5
            NumGlyphs = 2
            MultiLineSpacing = 0
            MaxFontSize = 24
            OnMouseUp = GeoVectorDownMouseUp
          end
        end
      end
      object TabSheet4: TTabSheet
        Caption = 'Vector Difference'
        ImageIndex = 4
        object RunVecDiffGrid: TStringGrid
          Left = 0
          Top = 24
          Width = 390
          Height = 80
          TabStop = False
          Align = alTop
          ColCount = 4
          DefaultColWidth = 52
          DefaultRowHeight = 18
          Enabled = False
          RowCount = 3
          Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine]
          ScrollBars = ssNone
          TabOrder = 0
          RowHeights = (
            18
            18
            18)
        end
        object Panel2: TPanel
          Left = 0
          Top = 0
          Width = 390
          Height = 24
          Align = alTop
          BevelInner = bvLowered
          Caption = 'Difference betwwen Nominal and Actual Stacking Axis'
          Color = clAqua
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlue
          Font.Height = -13
          Font.Name = 'Comic Sans MS'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 1
        end
      end
      object DebugSheet: TTabSheet
        Caption = 'Debug'
        ImageIndex = 5
        TabVisible = False
        object DebufStartButton: TButton
          Left = 0
          Top = 0
          Width = 75
          Height = 25
          Caption = 'Start'
          TabOrder = 0
          OnClick = DebufStartButtonClick
        end
        object DebugStepButton: TButton
          Left = 8
          Top = 48
          Width = 75
          Height = 25
          Caption = 'Step'
          TabOrder = 1
          OnClick = DebugStepButtonClick
        end
        object DebugStageDisplay: TEdit
          Left = 8
          Top = 112
          Width = 81
          Height = 21
          Enabled = False
          TabOrder = 2
        end
        object DebugMemo: TMemo
          Left = 272
          Top = 0
          Width = 118
          Height = 156
          Align = alRight
          Lines.Strings = (
            'DebugMemo')
          TabOrder = 3
        end
        object TV1: TG3DOTree
          Left = 127
          Top = 0
          Width = 145
          Height = 156
          Align = alRight
          HideSelection = False
          Indent = 19
          TabOrder = 4
          OnChange = TV1Change
        end
      end
    end
  end
  object FOpenDia: TOpenDialog
    DefaultExt = '*.geo'
    Filter = 'GeometryFiles|*.geo'
    InitialDir = 'd:\GeoFiles'
    Left = 552
    Top = 208
  end
  object FileSaveDia: TSaveDialog
    DefaultExt = 'geo'
    InitialDir = 'd:\geoFiles'
    Left = 552
    Top = 240
  end
end
