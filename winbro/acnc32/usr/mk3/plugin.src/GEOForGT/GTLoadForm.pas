unit GTLoadForm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls;

type
  TAGEPluginCreate = procedure; stdcall;
  TAGEPluginDestroy = procedure; stdcall;
  TAGEPluginAddVector = procedure(x1,y1,z1,x2,y2,z2 : Double; VName : PChar); stdcall;
  TAGEPluginSetNominalStackingAxis = procedure(Px,Py,Pz,x2,y2,z2,tx,ty,tz  : Double); stdcall;
  TAGEPluginSetActualStackingAxis  = procedure(Px,Py,Pz,x2,y2,z2,tx,ty,tz  : Double); stdcall;
  TAGESetAxisPositions = procedure(X,Y,Z,A,B,C : Double); stdcall;
  TAGEPluginShow = procedure; stdcall;
  TGeneralPluginSetProfilePath = procedure(ProfilePath : PChar); stdcall;

  TForm1 = class(TForm)
    Button1: TButton;
    Button2: TButton;
    procedure FormCreate(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
    AGEPluginCreate : TAGEPluginCreate;
    AGEPluginDestroy : TAGEPluginDestroy;
    AGEPluginAddVector : TAGEPluginAddVector;
    AGEPluginSetNominalStackingAxis : TAGEPluginSetNominalStackingAxis;
    AGEPluginSetActualStackingAxis : TAGEPluginSetActualStackingAxis;
    AGEPluginSetAxisPOsitions :TAGESetAxisPOsitions;
    AGEPluginShow : TAGEPluginShow;
    AGEPluginHide : TAGEPluginShow;
    GeneralPluginSetProfilePath : TGeneralPluginSetProfilePath;
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.DFM}

procedure TForm1.FormCreate(Sender: TObject);
var Flib : Longword;
begin
  Flib := LoadLibrary('c:\acnc32\profile\lib\gt.dll');
  AGEPluginCreate := GetProcAddress(FLib, 'GEOPluginCreate');
  AGEPluginDestroy := GetProcAddress(FLib, 'GEOPluginCreate');
  AGEPluginAddVector := GetProcAddress(FLib, 'GEOPluginAddVector');
  AGEPluginSetNominalStackingAxis := GetProcAddress(FLib,'GEOPluginSetNominalStackingAxis');
  AGEPluginSetActualStackingAxis := GetProcAddress(FLib,'GEOPluginSetActualStackingAxis');
  AGEPluginSetAxisPositions := GetProcAddress(FLib,'GEOPluginSetAxisPositions');
  AGEPluginShow := GetProcAddress(FLib, 'GEOPluginShow');
  AGEPluginHide := GetProcAddress(FLib, 'GEOPluginShow');
  GeneralPluginSetProfilePath := GetProcAddress(FLib, 'GeneralPluginSetProfilePath');

  GeneralPluginSetProfilePath('c:\acnc32\profile\');
  AGEPluginCreate;
end;

procedure TForm1.Button1Click(Sender: TObject);
begin
AGEPluginSetNominalStackingAxis(5,5,20,4,9,120,30,50,20);
AGEPluginSetActualStackingAxis(15,15,20,-14,29,130,41,55,20);
AGEPluginSetAxisPositions(130,5,-195,10,89,2);
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
  AGEPluginShow;
end;

end.
