unit GTGEOMain;

interface
uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls,G3DPaper,G3DObjects,G3DLine,G3DDefs,GMath3D,G3DBox,G3DDisc, Buttons,
  ExtCtrls, ComCtrls,Menus, G3DOTree, G3DDisplay,  CheckSelect,INamedInterface,
  Grids,GReportPoint,VectorMaths,G3dNavigator,PCMPlugin,
  NamedPlugin, IFormInterface, ACNC_TouchGlyphSoftkey, CNCTypes, AbstractFormPlugin,
  ACNC_NumEdit,AbstractScript,IScriptInterface,IStringArray;

const
cLargePaperWidth = 550;
cLargePaperHeight = 560;
cLargeTreeWidth = 450; // actually edit panel width
cLargeTreeHeight = 565;
cLargeCPanelWidth = 1020;
cLargeCPanelHeight = 170;

cSmallPaperWidth = 450;
cSmallPaperHeight = 420;
cSmallTreeWidth = 150;
cSmallTreeHeight = 380;
cSmallCPanelWidth = 780;
cSmallCPanelHeight = 110;
cMaxNumInput = 1000000;


type
TExtentsModes = (emUnLatched,emLatched);
PTXYZ = ^TXYZ;
PLoc  = ^TPLocation;
TDisplayModes = (dmNone,dmFull,dmOnMachine,dmInvisible);

EObjectNameException = class(Exception);
EObjectValueException = class(Exception);
TJogModes = (jmPlus,jmMinus);
  TGEO = class(TInstallForm)
    FOpenDia: TOpenDialog;
    FileSaveDia: TSaveDialog;
    MachinePanel: TPanel;
    MachinePages: TPageControl;
    GEOPositionSheet: TTabSheet;
    GEOPosGrid: TStringGrid;
    ShowPCMButton: TBitBtn;
    HidePCM: TBitBtn;
    MachineDisplay: TTabSheet;
    MachineDisplayPanel: TPanel;
    MachineFitButton: TTouchGlyphSoftkey;
    PointFitButton: TTouchGlyphSoftkey;
    GEOZoomIn: TTouchGlyphSoftkey;
    GEOZoomOut: TTouchGlyphSoftkey;
    GEOCloseBtn: TTouchGlyphSoftkey;
    MachinePoints: TTabSheet;
    ReportPointGrid: TStringGrid;
    GEOPointsUP: TTouchGlyphSoftkey;
    GEOPointsDown: TTouchGlyphSoftkey;
    MachineVectors: TTabSheet;
    VectorPanel: TPanel;
    Label10: TLabel;
    PositionTitle: TPanel;
    VectorDataGrid: TStringGrid;
    VectorChoosePanel: TPanel;
    VectorSelectList: TComboBox;
    VectorRotationGrid: TStringGrid;
    RotationTitle: TPanel;
    VectorLengthDisplay: TEdit;
    TabSheet4: TTabSheet;
    RunVecDiffGrid: TStringGrid;
    Panel2: TPanel;
    Paper: TG3DDisplay;
    GEOVectorUp: TTouchGlyphSoftkey;
    GeoVectorDown: TTouchGlyphSoftkey;

    DebugSheet: TTabSheet;
    DebufStartButton: TButton;
    DebugStepButton: TButton;
    DebugStageDisplay: TEdit;
    VersionLabel: TLabel;
    Panel1: TPanel;
    DebugMemo: TMemo;
    TV1: TG3DOTree;
    Button1: TButton;
    procedure Button1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure RotateSelectClick(Sender: TObject);
    procedure TranslateSelectClick(Sender: TObject);
    procedure TV1Change(Sender: TObject; Node: TTreeNode);

    procedure EditRunPagesChange(Sender: TObject);
    procedure AxisSelectGroupClick(Sender: TObject);

    procedure VectorSelectListChange(Sender: TObject);
    procedure DebugStepButtonClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure ShowPCMButtonClick(Sender: TObject);
    procedure GEOPointsDownClick(Sender: TObject);
    procedure HidePCMClick(Sender: TObject);
    procedure ReportPointGridSelectCell(Sender: TObject; ACol,
      ARow: Integer; var CanSelect: Boolean);
    procedure GEOCloseBtnClick(Sender: TObject);
    procedure PointFitButtonMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure MachineFitButtonMouseUp(Sender: TObject;
      Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure GEOZoomInMouseUp(Sender: TObject;
      Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure GEOZoomOutMouseUp(Sender: TObject;
      Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure GEOCloseBtnMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FormActivate(Sender: TObject);
    procedure GEOPointsUPMouseUp(Sender: TObject;
      Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure GEOPointsDownMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure GEOVectorUpMouseUp(Sender: TObject;
      Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure GeoVectorDownMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure DebufStartButtonClick(Sender: TObject);
    function Shortestanglebetween(Angle1,Angle2 : Double):Double;
    Function InterpolateMDTCol(MDTName,MDTCol : String;FirstRow,LastRow : Integer): Integer;


  private
    { Private declarations }
    DebugStartStage     : Integer;
    SingleStep          : Boolean;
    DebugStartPositions : TAxisPosn;
    DrawColor           : Tcolor;
    MoveMode            : TMoveModes;
    FormWidth           : Integer;
    FormHeight          : Integer;
    LastObject          : TG3DObject;
    SelectedObject      : TG3DObject;

    JogObject           : TG3DObject;
    JogAxis             : Integer;
    JogNode             : TTreeNode;
    SelectedJogAxis    : Integer;
    SelectedDisplayVector : Integer;
    MachineNavigator      : TG3DNavigator;
    SelectedPointIndex    : Integer;
    GeoReady              : Boolean;
    InchModeTag           : TTagref;
    UserLevel             : TTagref;
    GEONavOPtionTag       : TTagref;

    function  FloatDisplay(Value : Double): String;
    function  isValidAxis(AxisNumber : Integer):Boolean;
    procedure UpdateDisplayedVectorData(ListPosition: Integer);
    procedure UpdateActToNomVectorDiffDisplay;
    procedure MachineNavButtonPressed(Sender : TObject);
    procedure MoveWorld(Motion :TMoveModes;Axis :Integer;Increment : Integer);
    procedure UpdateReportGrid;
    procedure UpdateEverythingAndDisplay;
    procedure ShowDebugState(Sender : TObject);
    procedure TagChange(ATag : TTagRef); stdcall;
    procedure SetExtentsMode(Mode : TExtentsModes);


  public
    { Public declarations }
    GEONavInstalled        : Boolean;
    FDMode                 : TDisplayModes;
    InhibitExternalUpdate  : Boolean;
    PCM                    : TPCMPlugin;
    InchMode               : Boolean;
    ExtentsMode            : TExtentsModes;
    constructor Create(AOwner : TComponent); override;
    procedure FinalInstall; override;
    procedure DisplaySweep; override;
    procedure ExternalBeginUpdate;
    procedure ExternalEndUpdate;

    procedure SetDisplayMode(DMode :TDisplayModes);
    procedure LoadPCMandinitialise(afile :PChar; Sqr, Retry : Integer; Fit : Double);
    procedure OnMachineShow;
    procedure UpdateMachineReportDataAndDisplay;
    procedure GetPCMProbeVector(VectorName : Pchar; var P1 :TPlocation;var P2:TPlocation);
    procedure SetToolOffset(P1 : TPlocation);
    procedure UpdateVectorSelectList;
    procedure UpdateGeoPositionGrid;
    procedure ProbePosnToPartRelativePoint(PointName : PChar;X,Y,Z,A,B,C : Double);
    function  GetPointData(PointName : String; var P1: TPlocation): Boolean;
    function  GetAxisPosForPoint(PointName : PChar; A,B,C : Double):TPlocation;
    procedure CorrectForStackingAxis(UseCAxis : Boolean; var X,Y,Z,A,B,C : Double);
    procedure GetMachineNDP(Crot :Double;var ResLinear,ResRotary :TXYZ;Par1,Par2 : Integer);
    procedure GetGTMachineNDPForDefinedAppVecZRotation(Zrot :Double;var ResLinear : TXYZ;var ResRotary: TXYZ;Solution : Integer;InverseVector: Boolean);
    function NewGetGTMachineNDPForDefinedAppVecZRotation(Zrot :Double;var ResLinear : TXYZ;var ResRotary: TXYZ;Solution : Integer;InverseVector: Boolean): Integer;

    procedure GetGTMachineNDPForFixedRotaryPos(FixedRot : Double;
                                               FixedAxisIndex : INteger;
                                               var ResLinear : TXYZ;
                                               var ResRotary: TXYZ;
                                               Solution : Integer;
                                               InverseVector: Boolean;
                                               IgnoreLimits : Boolean);

    procedure GetGTGetErrowaOffset(ResIndex : Integer;var OffsetResult : TXYZ);
    procedure GetGTMachineToolRetractVector(VectorLength : Double;var IncResult: TXYZ);

  end;
//***********************************************************************
// GT
//***********************************************************************
  TGTAdjustToolDatumLocation = class (TScriptMethod)
  public
    procedure Reset; override; stdcall;
    function Name: WideString; override; stdcall;
    function IsFunction: Boolean; override; stdcall;
    function ParameterPassing(Index: Integer): Integer; override; stdcall;
    function ParameterType(Index: Integer): Integer; override; stdcall;
    function ParameterName(Index: Integer): WideString; override; stdcall;
    function ReturnType: Integer; override; stdcall;
    function ParameterCount: Integer; override; stdcall;
    procedure Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer); override; stdcall;
  end;

  TGTReturnCurrentStandoff = class (TScriptMethod)
  public
    procedure Reset; override; stdcall;
    function Name: WideString; override; stdcall;
    function IsFunction: Boolean; override; stdcall;
    function ParameterPassing(Index: Integer): Integer; override; stdcall;
    function ParameterType(Index: Integer): Integer; override; stdcall;
    function ParameterName(Index: Integer): WideString; override; stdcall;
    function ReturnType: Integer; override; stdcall;
    function ParameterCount: Integer; override; stdcall;
    procedure Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer); override; stdcall;
  end;

  TGTProbePointResolve = class (TScriptMethod)
  public
    procedure Reset; override; stdcall;
    function Name: WideString; override; stdcall;
    function IsFunction: Boolean; override; stdcall;
    function ParameterPassing(Index: Integer): Integer; override; stdcall;
    function ParameterType(Index: Integer): Integer; override; stdcall;
    function ParameterName(Index: Integer): WideString; override; stdcall;
    function ReturnType: Integer; override; stdcall;
    function ParameterCount: Integer; override; stdcall;
    procedure Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer); override; stdcall;
  end;

  TModifyNDPRotationFrom4Points = class (TScriptMethod)
  public
    procedure Reset; override; stdcall;
    function Name: WideString; override; stdcall;
    function IsFunction: Boolean; override; stdcall;
    function ParameterPassing(Index: Integer): Integer; override; stdcall;
    function ParameterType(Index: Integer): Integer; override; stdcall;
    function ParameterName(Index: Integer): WideString; override; stdcall;
    function ReturnType: Integer; override; stdcall;
    function ParameterCount: Integer; override; stdcall;
    procedure Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer); override; stdcall;
  end;


  TCentreFromNPoints = class (TScriptMethod)
  public
    MeasuredPoints : TList;
    FMaxXMeasured : Double;
    FMaxYMeasured : Double;
    FMinXMeasured : Double;
    FMinYMeasured : Double;
    CentreGravity : TPlocation;
    procedure CalcMeasuredMaxMin;
    procedure  FindXYCentreofGravity;

    procedure Reset; override; stdcall;
    function Name: WideString; override; stdcall;
    function IsFunction: Boolean; override; stdcall;
    function ParameterPassing(Index: Integer): Integer; override; stdcall;
    function ParameterType(Index: Integer): Integer; override; stdcall;
    function ParameterName(Index: Integer): WideString; override; stdcall;
    function ReturnType: Integer; override; stdcall;
    function ParameterCount: Integer; override; stdcall;
    procedure Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer); override; stdcall;


  end;

  TInterpolateMDTColumn = class (TScriptMethod)
  public
    procedure Reset; override; stdcall;
    function Name: WideString; override; stdcall;
    function IsFunction: Boolean; override; stdcall;
    function ParameterPassing(Index: Integer): Integer; override; stdcall;
    function ParameterType(Index: Integer): Integer; override; stdcall;
    function ParameterName(Index: Integer): WideString; override; stdcall;
    function ReturnType: Integer; override; stdcall;
    function ParameterCount: Integer; override; stdcall;
    procedure Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer); override; stdcall;
  end;


  TGetToolRetractPoint = class (TScriptMethod)
  public
    procedure Reset; override; stdcall;
    function Name: WideString; override; stdcall;
    function IsFunction: Boolean; override; stdcall;
    function ParameterPassing(Index: Integer): Integer; override; stdcall;
    function ParameterType(Index: Integer): Integer; override; stdcall;
    function ParameterName(Index: Integer): WideString; override; stdcall;
    function ReturnType: Integer; override; stdcall;
    function ParameterCount: Integer; override; stdcall;
    procedure Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer); override; stdcall;
  end;

  TCheckAxiesInLimits = class (TScriptMethod)
  public
    procedure Reset; override; stdcall;
    function Name: WideString; override; stdcall;
    function IsFunction: Boolean; override; stdcall;
    function ParameterPassing(Index: Integer): Integer; override; stdcall;
    function ParameterType(Index: Integer): Integer; override; stdcall;
    function ParameterName(Index: Integer): WideString; override; stdcall;
    function ReturnType: Integer; override; stdcall;
    function ParameterCount: Integer; override; stdcall;
    procedure Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer); override; stdcall;
  end;

  TApplyUniqueElectrodeOffsets = class (TScriptMethod)
  public
    procedure Reset; override; stdcall;
    function Name: WideString; override; stdcall;
    function IsFunction: Boolean; override; stdcall;
    function ParameterPassing(Index: Integer): Integer; override; stdcall;
    function ParameterType(Index: Integer): Integer; override; stdcall;
    function ParameterName(Index: Integer): WideString; override; stdcall;
    function ReturnType: Integer; override; stdcall;
    function ParameterCount: Integer; override; stdcall;
    procedure Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer); override; stdcall;
  end;

  TGetGTNDPAlignHoleVector = class (TScriptMethod)
  public
    procedure Reset; override; stdcall;
    function Name: WideString; override; stdcall;
    function IsFunction: Boolean; override; stdcall;
    function ParameterPassing(Index: Integer): Integer; override; stdcall;
    function ParameterType(Index: Integer): Integer; override; stdcall;
    function ParameterName(Index: Integer): WideString; override; stdcall;
    function ReturnType: Integer; override; stdcall;
    function ParameterCount: Integer; override; stdcall;
    procedure Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer); override; stdcall;
  end;

  TGetGTNDPAlignHoleVectorClosestBPos = class (TScriptMethod)
  public
    procedure Reset; override; stdcall;
    function Name: WideString; override; stdcall;
    function IsFunction: Boolean; override; stdcall;
    function ParameterPassing(Index: Integer): Integer; override; stdcall;
    function ParameterType(Index: Integer): Integer; override; stdcall;
    function ParameterName(Index: Integer): WideString; override; stdcall;
    function ReturnType: Integer; override; stdcall;
    function ParameterCount: Integer; override; stdcall;
    procedure Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer); override; stdcall;
  end;


  TNewGetGTNDPAlignHoleVector = class (TScriptMethod)
  public
    procedure Reset; override; stdcall;
    function Name: WideString; override; stdcall;
    function IsFunction: Boolean; override; stdcall;
    function ParameterPassing(Index: Integer): Integer; override; stdcall;
    function ParameterType(Index: Integer): Integer; override; stdcall;
    function ParameterName(Index: Integer): WideString; override; stdcall;
    function ReturnType: Integer; override; stdcall;
    function ParameterCount: Integer; override; stdcall;
    procedure Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer); override; stdcall;
  end;

  TGetGTNDPWithFixedRotary = class (TScriptMethod)
  public
    procedure Reset; override; stdcall;
    function Name: WideString; override; stdcall;
    function IsFunction: Boolean; override; stdcall;
    function ParameterPassing(Index: Integer): Integer; override; stdcall;
    function ParameterType(Index: Integer): Integer; override; stdcall;
    function ParameterName(Index: Integer): WideString; override; stdcall;
    function ReturnType: Integer; override; stdcall;
    function ParameterCount: Integer; override; stdcall;
    procedure Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer); override; stdcall;
  end;

  TGetGTNDPWithFixedCandClosestBPosition = class (TScriptMethod)
  public
    procedure Reset; override; stdcall;
    function Name: WideString; override; stdcall;
    function IsFunction: Boolean; override; stdcall;
    function ParameterPassing(Index: Integer): Integer; override; stdcall;
    function ParameterType(Index: Integer): Integer; override; stdcall;
    function ParameterName(Index: Integer): WideString; override; stdcall;
    function ReturnType: Integer; override; stdcall;
    function ParameterCount: Integer; override; stdcall;
    procedure Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer); override; stdcall;
  end;

  TGetGTErrowaOffset = class (TScriptMethod)
  public
    procedure Reset; override; stdcall;
    function Name: WideString; override; stdcall;
    function IsFunction: Boolean; override; stdcall;
    function ParameterPassing(Index: Integer): Integer; override; stdcall;
    function ParameterType(Index: Integer): Integer; override; stdcall;
    function ParameterName(Index: Integer): WideString; override; stdcall;
    function ReturnType: Integer; override; stdcall;
    function ParameterCount: Integer; override; stdcall;
    procedure Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer); override; stdcall;
  end;


var
  GEO: TGEO;
  procedure TagChangeHandler(Tag : TTagRef);stdcall
  procedure GTTXform(var XYZPOs : TPlocation);

implementation

{$R *.DFM}

function Mod360(InputAngle :Double): Double;
begin
Result := InputAngle;
while Result < 0 do
   begin
   Result := Result+360;
   end;
while Result >= 360 do
   begin
   Result := Result-360;
   end
end;

function InchToMetric(InValue : Double):Double;
begin
Result := InValue*25.4;
end;

function MetricToInch(InValue : Double):Double;
begin
Result := InValue/25.4;
end;

function UnitModified(Posn : Double): String;
begin
Result := Format('%7.3f',[Posn]);
if not assigned(GEO) then exit;
if GEO.InchMode then
   begin
   Result := Format('%7.4f',[MetricToInch(Posn)]);
   end;
end;

Function TGEO.InterpolateMDTCol(MDTName,MDTCol : String;FirstRow,LastRow : Integer): Integer;
var
CurrentMDTIndex : Integer;
Table: IACNC32StringArray;
FoundData : Boolean;
LastRecordIndex : Integer;
NextValue : Double;
CurrentValue : Double;
NextValueIndex : Integer;
FirstValueIndex : Integer;
ThisValueIndex : Integer;
InterpolationIncrement : Double;
InterpolationMultiplier : Integer;
InterpolationSpan : Double;
ValCount : Integer;
Val : String;
Done : Boolean;
isAllDone : Boolean;
HaveWrapped : Boolean;
CurrentValueIndex : Integer;
LCount : Integer;
TotalRows : Integer;
FirstFoundIndex : Integer;
FirstSweep : Boolean;

  function ReadField(Table: IACNC32StringArray; Row: Integer; Field: string ): Double;
  var Tmp: WideString;
  begin
    if Table.GetFieldValue(Row, Field, Tmp) <> AAR_OK then
      raise Exception.CreateFmt(MDTName + 'Field name or record index out of range [%d:%s]', [Row, Field]);
    Result:= StrToFloatDef(Tmp, 0);
  end;

  function WriteField(Table: IACNC32StringArray; Row: Integer; Field: string;Value : Double ): Boolean;
  var Tmp: WideString;
  begin
    tmp := FloatToStr(Value);
    if Table.SetFieldValue(Row, Field, Tmp) <> AAR_OK then Result := False
  end;

  function FieldIsNull(Table: IACNC32StringArray; Row: Integer; Field: string ): Boolean;
  var Tmp: WideString;
  begin
    if Table.GetFieldValue(Row, Field, Tmp) <> AAR_OK then
      raise Exception.CreateFmt(MDTName + 'Field name or record index out of range [%d:%s]', [Row, Field]);
    Result:= Tmp = '';
  end;

  function GetNextValueIndex(Table: IACNC32StringArray;
                             FieldN: string ;
                             StartRow : Integer;
                             var Value: Double;
                             var Wrapped : Boolean;
                             var AllDone : Boolean;
                             FirstRow,LastRow : Integer;
                             WrapStartIndex : Integer): Integer;
  var
  Tmp: WideString;
  Found : Boolean;
  CRow : Integer;
  begin
    Wrapped := False;
    Found := False;
    AllDone := False;
    CRow  := StartRow+1;
    if Crow = LastRow then
      begin
      Wrapped := True;
      CRow := WrapStartIndex;
      end;
    while not Found do
      begin
      if Table.GetFieldValue(Crow, FieldN, Tmp) = AAR_OK then
        begin
        if tmp <> '' then
          begin
          Found := True;
          Value := StrToFloatDef(Tmp, 0);
          Result := CRow;
          end
        else
          begin
          CRow  := CRow+1;
          if Crow = LastRow then
            begin
            Wrapped := True;
            CRow := WrapStartIndex;
            end;
          end
        end
      else
        begin
        Done := True;
        AllDone := True;
        end;
      end;
  end;

begin

Result := 0;
  if StringArraySet.UseArray(MDTName, nil, Table) = AAR_OK then
    begin
    try
    LastRecordIndex := LastRow+1;
    CurrentMDTIndex := FirstRow;
    FirstSweep := True;
    FoundData := False;
    ValCount := 0;
    // Check Limits of rows
    TotalRows := Table.RecordCount;
    if (FirstRow <0) or (LastRow >= Table.RecordCount) then
      begin
      Result := -1;
      exit
      end ;

    // Check at least two values

    while (FoundData = False) and (CurrentMDTIndex <= LastRecordIndex) do
      begin
      if not (FieldIsNull(Table, CurrentMDTIndex, MDTCol)) then
        begin
        ValCount := ValCount+1;
        if ValCount = 1 then FirstValueIndex := CurrentMDTIndex;
        end;
      FoundData := ValCount >= 2;
      CurrentMDTIndex := CurrentMDTIndex+1;
      end;

    if FoundData then
      begin
      IsAllDone := False;
      if FirstSweep then
        begin
        FirstSweep := False;
        FirstFoundIndex := FirstValueIndex;
        end;
      CurrentValueIndex := FirstValueIndex;
      CurrentValue := ReadField(Table,FirstValueIndex,MDTCol);
      NextValueIndex := GetNextValueIndex(Table,MDTCol,CurrentValueIndex,NextValue,HaveWrapped,IsAllDone,FirstRow,LastRecordIndex,FirstRow);
      if HaveWrapped then
        InterpolationSpan := (LastRecordIndex-CurrentValueIndex)+(NextValueIndex-FirstRow)
      else
        InterpolationSpan := NextValueIndex-CurrentValueIndex;

      Done := False;
      LCount := 0;
      while not Done do
        begin
        InterpolationIncrement := (NextValue-CurrentValue)/InterpolationSpan;
        CurrentValueIndex := CurrentValueIndex+1;
        if CurrentValueIndex >= LastRecordIndex then CurrentValueIndex := FirstRow;
        InterpolationMultiplier := 1;
        while CurrentValueIndex <> NextValueIndex do
          begin
          inc(Lcount);
          if (LCount > (TotalRows+10)) then
            begin
            Done := True;
            Result := -1;
            exit;
            end;
          WriteField(Table,CurrentValueIndex,MDTCol,(CurrentValue+(InterpolationIncrement*InterpolationMultiplier)));
          InterpolationMultiplier := InterpolationMultiplier+1;
          CurrentValueIndex := CurrentValueIndex+1;
          if CurrentValueIndex >= LastRecordIndex then CurrentValueIndex := FirstRow;
          if CurrentValueIndex = FirstValueIndex then
            begin
            Done := true;
            exit;
            end
          end;
        CurrentValueIndex := NextValueIndex;
        CurrentValue := ReadField(Table,CurrentValueIndex,MDTCol);
        if HaveWrapped and (CurrentValueIndex >= FirstFoundIndex) then
          begin
          Done := True;
          exit;
          end;
        NextValueIndex := GetNextValueIndex(Table,MDTCol,CurrentValueIndex,NextValue,HaveWrapped,Done,FirstRow,LastRecordIndex,FirstRow);
        if HaveWrapped then
          InterpolationSpan := (LastRecordIndex-CurrentValueIndex)+(NextValueIndex-FirstRow)
        else
          InterpolationSpan := NextValueIndex-CurrentValueIndex;
          end;
      end
    else
      begin
      Result :=-3;
      end;
    finally
    StringArraySet.ReleaseArray(nil, Table);
    end
    end;
end;


procedure  TGEO.CorrectForStackingAxis(UseCAxis : Boolean; var X,Y,Z,A,B,C : Double);
var
StartPositions : TAxisPosn;
StartStage     : Integer;
NewPOsition    : TAxisPosn;
begin

StartPositions := TV1.GetAllAxisPOsitions;
InhibitExternalUpdate := True;
try
TV1.AxisMoves[0].Position := x;
TV1.AxisMoves[1].Position := y;
TV1.AxisMoves[2].Position := z;
TV1.AxisMoves[3].Position := a;
TV1.AxisMoves[4].Position := b;
TV1.AxisMoves[5].Position := c;
TV1.AxisMoves[0].Move := True;
TV1.AxisMoves[1].Move := True;
TV1.AxisMoves[2].Move := True;
TV1.AxisMoves[3].Move := True;
TV1.AxisMoves[4].Move := True;
TV1.AxisMoves[5].Move := True;
TV1.MoveAllAxes(6,TV1.AxisMoves);
UpdateEverythingAndDisplay;

StartStage := 0;

NewPosition := GEO.TV1.NewGetAxisOffsets(StartStage,UseCAxis,False);
X := NewPOsition[0];
Y := NewPOsition[1];
Z := NewPOsition[2];
A := NewPOsition[3];
B := NewPOsition[4];
C := NewPOsition[5];
Tv1.RestoreAllAxisPOsitions(StartPositions);
finally
GEO.InhibitExternalUpdate := False;
end;
end;


// This procedure returns the absolute axis positions which place the current
// Electrode datum Point co-incident with the named point;
function  TGEO.GetAxisPosForPoint(PointName : PChar; A,B,C : Double):TPlocation;
var
PIndex : Integer;
ElectrodePosition : TPlocation;
StartPosition     : TAxisPosn;
PointPosition     : TPlocation;
PointDifference   : TPlocation;
begin
PIndex := Tv1.GetReportPointIndexfor(PointName);
try
if Pindex >=0 then
   begin
   InhibitExternalUpdate:= True;
   StartPosition := TV1.GetAllAxisPOsitions;
   TV1.AxisMoves[0].Position := 0;
   TV1.AxisMoves[1].Position := 0;
   TV1.AxisMoves[2].Position := 0;
   TV1.AxisMoves[3].Position := A;
   TV1.AxisMoves[4].Position := B;
   TV1.AxisMoves[5].Position := C;
   TV1.AxisMoves[0].Move := True;
   TV1.AxisMoves[1].Move := True;
   TV1.AxisMoves[2].Move := True;
   TV1.AxisMoves[3].Move := True;
   TV1.AxisMoves[4].Move := True;
   TV1.AxisMoves[5].Move := True;
   TV1.MoveAllAxes(6,TV1.AxisMoves);

   TV1.UpdateReportData(False);
   ElectrodePosition := TV1.ELectrodePosition;
   PointPosition := TV1.GetReportPointFromList(Pindex);
   PointDifference := SubtractLocation(PointPosition,ElectrodePosition);
   Result.X := PointDifference.x;
   Result.Y := PointDifference.y;
   Result.Z := PointDifference.z;
   end
else
    begin
    raise EGEOError.Create(Format('Invalid Point Name Specified (%s)',[PointName]));
    end;
finally
Tv1.RestoreAllAxisPOsitions(StartPosition);
InhibitExternalUpdate := false;
end;
end;


procedure  TGEO.ProbePosnToPartRelativePoint(PointName : PChar;X,Y,Z,A,B,C : Double);
begin
InhibitExternalUpdate  :=True;
try
TV1.ProbePosnToPartRelativePoint(PointName,X,Y,Z,A,B,C);
finally
InhibitExternalUpdate  :=False;
end;

end;

// This used for PP interface Instruction GEOGetPoint
function TGEO.GetPointData(PointName : String; var P1: TPlocation): Boolean;
var
Pindex : Integer;
begin
Result := True;
TV1.UpdateReportData(False);

if PointName = 'ELECTRODEDATUM' then
   begin
   P1 := TV1.ELectrodePosition;
   exit;
   end;
if PointName = 'TOOLDATUM' then
   begin
   P1 := TV1.ToolDatumPosition;
   exit;
   end;
if PointName = 'PARTDATUM' then
   begin
   P1 := TV1.PartDatumPosition;
   exit;
   end;
PIndex := Tv1.GetReportPointIndexfor(PointName);
if Pindex >=0 then
   begin
   try
   P1 := TV1.GetReportPointFromList(PIndex);
   except
   P1 := TV1.Zero3D;
   Result := False;
   raise EGEOError.Create(Format('Invalid Point Name Specified (%s)',[PointName]));
   end;
   end
else
    begin
    P1 := TV1.Zero3D;
    Result := False;
    raise EGEOError.Create(Format('Invalid Point Name Specified (%s)',[PointName]));
    end;

end;

procedure TGEO.SetToolOffset(P1 : TPlocation);
begin
TV1.SetElectrodeOffsetToToolDatum(P1);
end;

procedure TGEO.GetPCMProbeVector(VectorName : Pchar; var P1 :TPlocation;var P2:TPlocation);
var
PCMP1,PCMP2 : TXYZ;
begin
PCM.GetProbeVector(VectorName,PCMP1,PCMP2);
P1 := TPLocation(PCMP1);
P2 := TPLocation(PCMP2);
end;

procedure TGEO.OnMachineShow;
begin
with GEO do
     begin
     //formstyle := fsStayOnTop;
     Width := 430;
     height := 520;
     Left := 100;
     Top := 77;
     end;
//MachinePages.ActivePage := GEOPositionSheet;
MachinePages.ActivePage := MachineDisplay;
FormResize(Self);
Show;

end;

procedure TGEO.UpdateMachineReportDataAndDisplay;
begin

UpdateReportGrid;
if (SelectedDisplayVector >=0) and (SelectedDisplayVector < Length(TV1.FReportVectorList)) then
   begin
   UpdateDisplayedVectorData(SelectedDisplayVector);
   UpdateActToNomVectorDiffDisplay;
   end;
TV1.Update3DDisplay(Paper,umCustomOnly);
end;

procedure TGEO.LoadPCMandinitialise(afile :PChar; Sqr, Retry : Integer; Fit : Double);
var
NSAPPoint,NSAendPoint,NSARotPoint : TXYZ;
NameList : TStringList;
ConstraintCount : Integer;
P1              : TXYZ;
P1Pos           : TPlocation;
Constraint : array[0..1023] of Char;

begin
PCM.LoadConfig(afile,Sqr,Retry,Fit);
PCM.GetStackingAxis(NSAPPoint,NSAendPoint,NSARotPoint);
Tv1.SetNominalStackingAxis(NSAPPoint.x,
                           NSAPPoint.y,
                           NSAPPoint.z,
                           NSAEndPoint.x,
                           NSAEndPoint.y,
                           NSAEndPoint.z,
                           NSARotPoint.x,
                           NSARotPoint.y,
                           NSARotPoint.z);
Tv1.SetActualStackingAxis(NSAPPoint.x,
                           NSAPPoint.y,
                           NSAPPoint.z,
                           NSAEndPoint.x,
                           NSAEndPoint.y,
                           NSAEndPoint.z,
                           NSARotPoint.x,
                           NSARotPoint.y,
                           NSARotPoint.z);
PCM.GetConstraintNames(Constraint,1024);
NameList := TStringList.Create;
try
NameList.Text := Constraint;
for ConstraintCount := 0 to NameList.Count-1 do
    begin
    PCM.GetProbePoint(PChar(NameList[ConstraintCount]),P1);
    P1POs.x := P1.X;
    P1POs.y := P1.Y;
    P1POs.z := P1.Z;

    P1Pos := TV1.LinearCorrected(P1Pos);
    TV1.AddReportPointat(P1Pos.x,-P1Pos.y,P1Pos.z,NameList[ConstraintCount],rpPartRelative,True,False);
    end;
finally
NameList.Free;
end;
if FDmode = dmOnmachine then
   begin
   tV1.Update3DDisplay(Paper,umCustomOnly);
   end;
end;


procedure TGEO.SetDisplayMode(DMode :TDisplayModes);
begin
if FDMode = DMode then exit;
case DMode of
     dmOnMachine:
     begin
     TV1.UserMode := umRuntime;
     end;
     dmInvisible:
     begin
     TV1.UserMode := umRuntime;
     end;
end; // case
FDmode := Dmode;
FormResize(Self);
end;



procedure TGEO.FormCreate(Sender: TObject);
var
SelectedData :TGridRect;
PCMdllPath : String;
SPos : Integer;
Translator : TTranslator;
begin
GeoReady := False;
SelectedJogAxis := 0;
TV1.NumberofAxes := 7;
tV1.DisplayMode := dmFullDisplay;
LastObject := nil;
if Screen.Width >=1024 then
//if 1=2 then
   begin
   FormWidth := 1024;
   FormHeight := 768;
   end
else
   begin
   FormWidth := 800;
   FormHeight := 600;
   end;
Width := FormWidth;
Height := FormHeight;
Top :=0;
Left := 0;
DrawColor := clBlue;
TV1.ClearAllObjects;

FormResize(self);

With GEOPosGrid do
      begin
      ColWidths[0] := 35;
      ColWidths[1] := 100;
      Cells[0,0] := 'Axis';
      Cells[1,0] := 'Position';
      Cells[0,1] := 'X';
      Cells[0,2] := 'Y';
      Cells[0,3] := 'Z';
      Cells[0,4] := 'A';
      Cells[0,5] := 'B';
      Cells[0,6] := 'C';
      END;

With ReportPointGrid do
     begin
     ColWidths[0]:=100;
     Cells[0,0] := 'Pnt. ID';
     Cells[1,0] := 'X';
     Cells[2,0] := 'Y';
     Cells[3,0] := 'Z';
     Cells[0,1] := 'PartDatum';
     Cells[0,2] := 'ToolDatum';
     Cells[0,3] := 'Electrode';
     Row := 1;
     SelectedData .TopLeft.y := ReportPointGrid.Row;
     SelectedData .TopLeft.x := 1;
     SelectedData .BottomRight.y := ReportPointGrid.Row;
     SelectedData .BottomRight.x := 3;
     ReportPointGrid.Selection := SelectedData;
     end;

With RunVecDiffGrid do
     begin
     ColWidths[0]:=100;
     Cells[0,1] := 'Translation';
     Cells[0,2] := 'Rotation';
     Cells[1,0] := 'X';
     Cells[2,0] := 'Y';
     Cells[3,0] := 'Z';
     Row := 0;
     Col := 0;
     end;

With VectorDataGrid do
     begin
     ColWidths[0]:=55;
     Cells[0,1] := 'Cent.Pos.';
     Cells[0,2] := 'Pnt1 Pos.';
     Cells[0,3] := 'Pnt2 Pos.';
     Cells[1,0] := 'X';
     Cells[2,0] := 'Y';
     Cells[3,0] := 'Z';
     Row := 0;
     Col := 0;
     end;
With VectorRotationGrid do
     begin
     ColWidths[0]:=55;
     Row := 0;
     Col := 0;
     end;

TV1.UserMode := umEditor;
if FileExists(DLLProfilePath + '\GeoFiles\GT.geo') then
   begin
   Tv1.LoadTreeFromDisc(DLLProfilePath + '\GeoFiles\GT.geo',False);
   end
else
   begin
   raise EObjectNameException.create('Geo Model File Not Found');
   end;
UpdateEverythingAndDisplay;
Paper.ShowMode := smCustom;
tV1.Update3DDisplay(Paper,umCustomOnly);

MachineNavigator := TG3DNavigator.Create(Self);
with MachineNavigator do
     begin
     Parent := MachineDisplay;
     Align := alLeft;
     OnMotionButton := MachineNavButtonPressed;
     end;
PCMdllPath := Uppercase(DllProfilePath);
SPos := Pos('\LIVE',PCMdllPath);
if SPos > 0 then
  begin
  PCMdllPath := Copy(PCMdllPath,1,SPos);
  PCMdllPath := PCMdllPath+'lib\pcm.dll';
  end
else
  begin
  pcmdllPath := 'C:\ACNC32\profile\lib\pcm.dll';
  end;
PCM := TPCMPlugin.Create(pcmdllPath);
FDMode := dmNone;
SetDisplayMode(dmOnMachine);
MoveMode := mmRotate;
TV1.DebugReported := ShowDebugState;
if not assigned(TV1.NDPDatumNode) then
   begin
   TV1.AddObjectatNode(TV1.PartDatumNode,t3oNDPDatumNode,False);
   end;
Translator := TTranslator.Create;
try
with Translator do
  begin
  MachineFitButton.Legend := GetString('$FITMACHINE');
  PointFitButton.Legend := GetString('$FITPOINTS');
  GEOZoomIn.Legend := GetString('$GEOZOOMIN');
  GEOZoomOut.Legend := GetString('$GEOZOOMOUT');
  GEOCloseBtn.Legend := GetString('$GEOCLOSE');
  GEOPositionSheet.Caption := GetString('$GEOPosistion');
  MachineDisplay.Caption := GetString('$GEODisplay');
  MachinePoints.Caption := GetString('$GEOPoints');
  VectorPanel.Caption := GetString('$GEOVectors');
  TabSheet4.Caption := GetString('$GEOVECDiff');
  Panel2.Caption := GetString('$GEODiffTitle');
  HidePCM.Caption := GetString('$HIDEPCM');
  ShowPCMButton.Caption := GetString('$SHOWPCM');
  end;

finally
Translator.Free;
end;
MachinePages.ActivePage := MachineDisplay;
end;




procedure TGEO.RotateSelectClick(Sender: TObject);
begin
MoveMode := mmRotate;
end;

procedure TGEO.TranslateSelectClick(Sender: TObject);
begin
MoveMode := mmTranslate;

end;






procedure TGEO.FormResize(Sender: TObject);
begin
try
if not GeoReady then exit;
if FDMode = dmOnMachine then
   begin
   if Geo.Width < 500 then
      begin
      MachineNavigator.Left := MachineDisplayPanel.Width +2;
      MachinePanel.align := alNone;
      MachinePanel.Height := 250;
      MachinePanel.align := alBottom;
      MachineNavigator.Width := MachinePanel.Width div 2;
      MachinePanel.Visible := True;
      MachineDisplayPanel.Width := (MachinePanel.Width div 2)-15;
      MachineDisplayPanel.Align := alRight;
      MachineNavigator.Top := 1;
      MachineNavigator.Left := 2;
      With GEOPosGrid do
           begin
           Width := MachinePanel.Width div 2;
           ColWidths[1] := width- 40;
           end;
      Paper.align := alClient;
      end
else
    begin
      MachinePanel.align := alNone;
      MachinePanel.Width := (Width div 2)-2;
      MachinePanel.align := alRight;
      MachinePanel.Width := (Width div 2)-2;
      MachineNavigator.Width := MachinePanel.Width div 2;
      MachinePanel.Visible := True;
      MachineNavigator.Top := 1;
      MachineNavigator.Left := 2;
      MachineDisplayPanel.Width := (MachinePanel.Width div 2)-15;

      MachineDisplayPanel.Align := alNone;
      MachineDisplayPanel.Top := 2;
      MachineDisplayPanel.Height := 200;
      MachineDisplayPanel.Left := MachineNavigator.Width+2;

      With GEOPosGrid do
           begin
           Width := MachinePanel.Width div 2;
           ColWidths[1] := width- 40;
           end;
      Paper.Width := MachinePanel.Width;
      Paper.align := alLeft;

    end;

   Paper.align := alClient;
//   MachineDisplayPanel.Width := (MachinePanel.Width div 2)-15;
   GeoZoomIn.Height := MachineDisplayPanel.Height div 3;
   GeoZoomOut.Height := MachineDisplayPanel.Height div 3;
   MachineFitButton.Height := MachineDisplayPanel.Height div 3;
   PointFitButton.Height := MachineDisplayPanel.Height div 3;
   ShowPCMButton.Height := MachineDisplayPanel.Height div 3;
   HidePCM.Height := MachineDisplayPanel.Height div 3;
   MachineFitButton.Width := (MachineDisplayPanel.Width div 2) -2;
   PointFitButton.Width := (MachineDisplayPanel.Width div 2) -2;
   ShowPCMButton.Width := (MachineDisplayPanel.Width-2)div 2;
   HidePCM.Width := (MachineDisplayPanel.Width-2)div 2;
   GeoZoomIn.Width := (MachineDisplayPanel.Width div 2) -2;
   GeoZoomOut.Width := (MachineDisplayPanel.Width div 2) -2;
   MachineFitButton.Left := 2;
   PointFitButton.Left := (MachineDisplayPanel.Width div 2)+1;
   GeoZoomIn.Left := 2;
   GeoZoomOut.Left := (MachineDisplayPanel.Width div 2)+1;

   ShowPCMButton.Left := MachinePanel.Width - ShowPCMButton.Width-10;
   ShowPCMButton.Top := MachineDisplayPanel.Height-ShowPCMButton.Height-5;
   HidePCM.Left := MachinePanel.Width - ShowPCMButton.Width-10;
   HidePCM.Top := MachineDisplayPanel.Height-ShowPCMButton.Height- ShowPCMButton.Height-10;
   MachineFitButton.Top := 0;
   PointFitButton.Top := 0;
   GeoZoomIn.Top := (MachineDisplayPanel.Height div 3)+3;
   GeoZoomOut.Top := (MachineDisplayPanel.Height div 3)+3;
   with GEOCloseBtn do
        begin
        Width := GeoZoomIn.Width;
        Height := GeoZoomIn.Height-8;
        Top := GeoZoomIn.Top+ GeoZoomIn.Height+2;
        end;
   GEOCloseBtn.Left :=   (MachineDisplayPanel.Width div 2)- (GEOCloseBtn.Width div 2); 



//   GeoVectorDown.Height := MachineDisplayPanel.Height div 3;
//   GeoVectorUp.Height := MachineDisplayPanel.Height div 3;
   GeoVectorUp.Top := VectorChoosePanel.Height+10;
   GeoVectorDown.Top := GeoVectorUp.Top+ GeoVectorUp.Height+5;

   GEOVectorUP.Width := GeoZoomIn.Width;
   GEOVectorDown.Width := GeoZoomIn.Width;
   GEOVectorUP.Left := MachinePanel.Width-GEOVectorUP.Width-15;
   GEOVectorDown.Left := MachinePanel.Width-GEOVectorDown.Width-15;

   ReportPointGrid.Width := 275;
   GEOPointsUP.Left := ReportPointGrid.Width+10;
   GEOPointsDown.Left := ReportPointGrid.Width+10;
   exit;
   end;

Paper.align := alNone;
MachinePanel.align := alNone;
MachinePanel.Visible := False;

//MachinePanel.Visible := False;

if FormWidth = 1024 then
   begin
   tv1.Width := cLargeTreeWidth-170;
   with Paper do
        begin
        top := 2;
        Left := 2;
        Width := cLargePaperWidth;
        Height := cLargePaperHeight;
        end;
   end
else
   begin
   with Paper do
        begin
        top := 2;
        Left := 2;
        Width := cSmallPaperWidth;
        Height := cSmallPaperHeight;
        end;
   end;
finally
//if assigned(MachineNavigator) then MachineNavigator.Width := MachinePage.width-2;
Tv1.FitAllObjectsOnSurface(Paper.CustomView);
tV1.Update3DDisplay(Paper,umAll);
end;

end;






procedure TGEO.TV1Change(Sender: TObject; Node: TTreeNode);
var
ObjectType :  T3DObjects;
HintBuffer :  String;
begin
if LastObject <> nil then
   begin
   LastObject.Selected := False;
   if LastObject.ObjectType <> t3oReportPoint then LastObject.DisplayReferencePoint := False;
   end;
if TV1.Selected = nil then exit;
if  TV1.Selected.Data = nil then exit;
   begin
   SelectedObject := TV1.Selected.Data;
   ObjectType := TG3DObject(SelectedObject).ObjectType;
   case ObjectType of

      t3oLine,t3oBox,t3oDisc:
        begin
        with selectedObject as TG3dObject do
           begin
           HintBuffer := Format('Rot: %f,%f,%f',[AbsoluteRotation.x,AbsoluteRotation.y,AbsoluteRotation.z])
           end;
      TV1.Hint := HintBuffer;
      TV1.ShowHint := True;
        end;
      else
          begin
          TV1.Hint := '';
          TV1.ShowHint := False;
          end;
   end; // CASE

{   case ObjectType of
     t3oLine:
     begin
     BufferLine := TG3dLine(SelectedObject);
     end;


     t3oBox:
     begin
     BufferBox := TG3dBox(SelectedObject);
     end;
     t3oDisc:
     begin
     BufferDisc := TG3dDisc(SelectedObject);
     end;
   end;}

LastObject := TG3DObject(SelectedObject);
Tv1.Update3DDisplay(Paper,umAll);


   end;
end;






function  TGEO.FloatDisplay(Value : Double): String;
begin
Result := Format('%9.4f',[Value]);
end;


















function TGEO.isValidAxis(AxisNumber : Integer):Boolean;
begin
Result := (AxisNumber>=0) and (AxisNumber<=TV1.NumberofAxes-1)
end;


procedure TGEO.EditRunPagesChange(Sender: TObject);
begin
//DatumAllBtnClick(Self);


FormResize(Self);
end;

procedure TGEO.AxisSelectGroupClick(Sender: TObject);
begin
JogObject := TV1.GetJogObject(SelectedJogAxis,JogNode);
JogAxis := TV1.GetJogAxis(SelectedJogAxis);
//UpdateAxisPOsitionGrid;
end;



procedure TGEO.UpdateReportGrid;
procedure UpdateRow(RowNum,PointNum : Integer);
begin
ReportPointGrid.Cells[0,RowNum] := TV1.ReportPointName[PointNum];
ReportPointGrid.Cells[1,RowNum] := UnitModified(TV1.ReportPointPosition[PointNum].x);
ReportPointGrid.Cells[2,RowNum] := UnitModified(TV1.ReportPointPosition[PointNum].y);
ReportPointGrid.Cells[3,RowNum] := UnitModified(TV1.ReportPointPosition[PointNum].z);
end;
var
PointCount : Integer;
begin
if ReportPointGrid.RowCount <> TV1.NumberofReportPoints +4 then
   begin
   ReportPointGrid.RowCount := TV1.NumberofReportPoints +4;
   end;
Tv1.UpdateReportData(False);
if assigned(TV1.PartDatumNode) then
   begin
   ReportPointGrid.Cells[1,1] := UnitModified(Tv1.PartDatumPosition.x);
   ReportPointGrid.Cells[2,1] := UnitModified(Tv1.PartDatumPosition.y);
   ReportPointGrid.Cells[3,1] := UnitModified(Tv1.PartDatumPosition.z);
   end;
if assigned(TV1.ToolDatumNode) then
   begin
   ReportPointGrid.Cells[1,2] := UnitModified(Tv1.ToolDatumPosition.x);
   ReportPointGrid.Cells[2,2] := UnitModified(Tv1.ToolDatumPosition.y);
   ReportPointGrid.Cells[3,2] := UnitModified(Tv1.ToolDatumPosition.z);
   end;
if assigned(TV1.ELectrodeNode) then
   begin
   ReportPointGrid.Cells[1,3] := UnitModified(Tv1.ELectrodePosition.x);
   ReportPointGrid.Cells[2,3] := UnitModified(Tv1.ELectrodePosition.y);
   ReportPointGrid.Cells[3,3] := UnitModified(Tv1.ELectrodePosition.z);
   end;
For PointCount := 0 to TV1.NumberofReportPoints-1 do
    begin
    UpdateRow(4+PointCount,PointCount);
    end;

end;



procedure TGEO.UpdateEverythingAndDisplay;
BEGIN
UpdateReportGrid;
UpdateDisplayedVectorData(SelectedDisplayVector);
UpdateVectorSelectList;
//UpdateVectorDiffDisplay;
UpdateActToNomVectorDiffDisplay;
UpdateGeoPositionGrid;
Tv1.Update3DDisplay(Paper,umAll);
END;


procedure TGEO.UpdateVectorSelectList;
var
vCount    : Integer;
ListEntry : TVectorData;
begin
TV1.UpdateReportData(True);
if Length(Tv1.FReportVectorList) < 1 then exit;
with VectorSelectList do
     begin
     Items.Clear;
     For VCount := 0 to Length(Tv1.FReportVectorList)-1 do
         begin
         ListEntry  := TV1.ReportVectorData[VCount];
         Items.Add(ListEntry.Name);
         end;
     Text := Items[0];
     end;
end;

procedure TGEO.UpdateActToNomVectorDiffDisplay;
var
//v1Index,V2Index : Integer;
DifData : TVectorDifference;
begin
if (TV1.NominalVectorIndex < 0) or (TV1.ActualVectorIndex < 0) then exit;
if TV1.GetVectorDifference(TV1.NominalVectorIndex,TV1.ActualVectorIndex,DifData) then
   begin
   with  RunVecDiffGrid do
         begin
         Cells[1,1] := UnitModified(DifData.Translation.x);
         Cells[2,1] := UnitModified(DifData.Translation.y);
         Cells[3,1] := UnitModified(DifData.Translation.z);
         Cells[1,2] := UnitModified(DifData.Rotation.x);
         Cells[2,2] := UnitModified(DifData.Rotation.y);
         Cells[3,2] := UnitModified(DifData.Rotation.z);
         end;
   end;

end;




procedure TGEO.UpdateDisplayedVectorData(ListPosition: Integer);
var
ListEntry : TVectorData;

begin
ListEntry := TV1.ReportVectorData[ListPosition];
with  VectorDataGrid do
      begin
      Cells[1,1] := UnitModified(ListEntry.CentrePosition.x);
      Cells[2,1] := UnitModified(ListEntry.CentrePosition.y);
      Cells[3,1] := UnitModified(ListEntry.CentrePosition.z);
      Cells[1,2] := UnitModified(ListEntry.Point1Position.x);
      Cells[2,2] := UnitModified(ListEntry.Point1Position.y);
      Cells[3,2] := UnitModified(ListEntry.Point1Position.z);
      Cells[1,3] := UnitModified(ListEntry.Point2Position.x);
      Cells[2,3] := UnitModified(ListEntry.Point2Position.y);
      Cells[3,3] := UnitModified(ListEntry.Point2Position.z);
      end;
with  VectorRotationGrid do
      begin
      Cells[1,0] := Format('%7.3f',[ListEntry.Rotation.x]);
      Cells[2,0] := Format('%7.3f',[ListEntry.Rotation.y]);
      Cells[3,0] := Format('%7.3f',[ListEntry.Rotation.z]);
      end;

VectorLengthDisplay.Text := FloatToStr(ListEntry.Length);
end;




procedure TGEO.VectorSelectListChange(Sender: TObject);
begin
if TV1.NumberofReportVectors > 0 then
   begin
   if (VectorSelectList.Itemindex >= 0)and(VectorSelectList.ItemIndex<Length(TV1.FReportVectorList)) then
      begin
      SelectedDisplayVector := VectorSelectList.ItemIndex;
      Tv1.ShowSelectedVector(SelectedDisplayVector);
      UpdateDisplayedVectorData(SelectedDisplayVector);
      end;
   end ;
end;


procedure TGEO.MachineNavButtonPressed(Sender : TObject);
var
MMode : TMoveModes;
begin
MMode := MachineNavigator.MotionMode;
case TSpeedButton(Sender).Tag of
     1: MoveWorld(MMode,0,-5);   //UP
     2: MoveWorld(MMode,0,5);   //down
     3: MoveWorld(MMode,1,-5);   //left
     4: MoveWorld(MMode,1,5);   // right
     5: MoveWorld(MMode,2,-5);   // upright
     6: MoveWorld(MMode,2,5);   // downleft
     7:
       begin
       Paper.Rotation := SetPRotation(90,0,0);   // TOP
       Tv1.FitAllObjectsOnSurface(Paper.CustomView);
       end;
     8:
       begin
       Paper.Rotation := SetPRotation(0,0,-90);     // side
       Tv1.FitAllObjectsOnSurface(Paper.CustomView);
       end;
     9:
       begin
       Paper.Rotation := SetPRotation(0,0,0);    // front
       Tv1.FitAllObjectsOnSurface(Paper.CustomView);
       end;
     end; // case;
tV1.Update3DDisplay(Paper,umCustomOnly);

end;

procedure TGEO.MoveWorld(Motion :TMoveModes;Axis :Integer;Increment : Integer);
begin
if Motion = mmTranslate then
   begin
   case Axis of
        0: Paper.Translation := AddScreenLocation(Paper.Translation,SetScreenLocation(0,Increment));
        1: Paper.Translation := AddScreenLocation(Paper.Translation,SetScreenLocation(Increment,0));
        2: Paper.Translation := AddScreenLocation(Paper.Translation,SetScreenLocation(-Increment,Increment));
        end; // case
   end
else

   begin
   case Axis of
        0: Paper.Rotation := AddRotation(Paper.Rotation,SetPRotation(Increment,0,0));
        2: Paper.Rotation := AddRotation(Paper.Rotation,SetPRotation(0,Increment,0));
        1: Paper.Rotation := AddRotation(Paper.Rotation,SetPRotation(0,0,Increment));
        end; // case
   end;

end;

procedure TGEO.ShowPCMButtonClick(Sender: TObject);
begin
PCM.Show;
end;

procedure TGEO.UpdateGeoPositionGrid;
var
Count : Integer;
Val   : Double;
begin
if assigned(GEO) then
   begin
   For Count := 0 to TV1.NumberofAxes do
    begin
    if (GEO.InchMode) and  ((Count < 3) or (Count > 5)) then
       begin
       GEOPosGrid.Cells[1,Count+1] := Format(' %-7.4f',[MetricToInch(Tv1.AxisPosition[Count])]);
       end
    else
        begin
        GEOPosGrid.Cells[1,Count+1] := Format(' %-7.3f',[Tv1.AxisPosition[Count]]);
        end;
    end;
   end;
end;




procedure TGEO.GEOPointsDownClick(Sender: TObject);
var
SelectedData :TGridRect;
begin

if  ReportPointGrid.Row < (ReportPointGrid.RowCount-1) then
    begin
    ReportPointGrid.Row := ReportPointGrid.Row+1;
    SelectedData .TopLeft.y := ReportPointGrid.Row;
    SelectedData .TopLeft.x := 1;
    SelectedData .BottomRight.y := ReportPointGrid.Row;
    SelectedData .BottomRight.x := 3;
    ReportPointGrid.Selection := SelectedData;
    end;

end;

procedure TGEO.HidePCMClick(Sender: TObject);
begin
PCM.Close;
end;

procedure TGEO.ReportPointGridSelectCell(Sender: TObject; ACol,
  ARow: Integer; var CanSelect: Boolean);
var
PointName : String;
PName     : String;
PointCount : Integer;
Point      : TG3dReportPoint;
TNode      : TTreeNode;
begin
PointName := ReportPointGrid.Cells[0,ARow];
SelectedPointIndex := Tv1.GetReportPointIndexfor(PointName);

For PointCount := 0 to Length(TV1.FReportPointList)-1 do
    begin
    PName := TV1.GetReportPointName(PointCount);
    Point := TV1.FindObjectandNode(PName,TNode);
    Point.Selected := False;
    end;
if SelectedPointIndex >=0 then
   begin
   PName := TV1.GetReportPointName(SelectedPointIndex);
   Point := TV1.FindObjectandNode(PName,TNode);
   Point.Selected := True;
   end
else
   begin
   // here for Part,Tool and Electrode Points
   if assigned(TV1.PartDatumNode) then
         begin
         Point := TV1.PartDatumNode.Data;
         Point.Selected := True;
         if PointName = 'PartDatum' then
            begin
            Point.Selected := True;
            end
         else
            begin
            Point.Selected := False;
            end
         end;
   if assigned(TV1.ToolDatumNode) then
         begin
         Point := TV1.ToolDatumNode.Data;
         if PointName = 'ToolDatum' then
            begin
            Point.Selected := True;
            end
         else
            begin
            Point.Selected := False;
            end
         end;
   if assigned(TV1.ElectrodeNode) then
         begin
         Point := TV1.ElectrodeNode.Data;
         if PointName = 'Electrode' then
            begin
            Point.Selected := True;
            end
         else
            begin
            Point.Selected := False;
            end
         end;
   end;
end;

constructor TGEO.Create(AOwner : TComponent);
begin
inherited Create(Application);
GEO := Self;
GeoReady := True;
TV1.GlobalUpdateCount := 0;
SetExtentsMode(emUnLatched);
end;

procedure TGEO.GEOCloseBtnClick(Sender: TObject);
begin
Host.ClientClose;
end;


procedure TGEO.PointFitButtonMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
case ExtentsMode of
  emUnlatched:
  begin
  SetExtentsMode(emlatched);
  TV1.ExtentsOverscale := 1.1;
  TV1.FitToReportPoints(Paper.CustomView);
  tV1.Update3DDisplay(Paper,umCustomOnly);
  end;

  emLatched:
  begin
  SetExtentsMode(emUnlatched);
  TV1.ExtentsOverscale := 1.1;
  TV1.FitToReportPoints(Paper.CustomView);
  tV1.Update3DDisplay(Paper,umCustomOnly);
  end;
end;
end;

procedure TGEO.MachineFitButtonMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
SetExtentsMode(emUnLatched);
TV1.FitAllObjectsOnSurface(Paper.CustomView);
tV1.Update3DDisplay(Paper,umCustomOnly);
end;

procedure TGEO.GEOZoomInMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
case ExtentsMode of
  emUnlatched:
  begin
  Paper.Scale := Paper.Scale*0.9;
  tV1.Update3DDisplay(Paper,umCustomOnly);
  end;
  emlatched:
  begin
  TV1.ExtentsOverscale := TV1.ExtentsOverscale*0.9;
  if TV1.ExtentsOverscale < 1.1 then TV1.ExtentsOverscale := 1.1;
  tV1.Update3DDisplay(Paper,umCustomOnly);
  end;
end;
end;

procedure TGEO.GEOZoomOutMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
case ExtentsMode of
  emUnlatched:
  begin
  Paper.Scale := Paper.Scale*1.1;
  tV1.Update3DDisplay(Paper,umCustomOnly);
  end;
  emlatched:
  begin
  TV1.ExtentsOverscale := TV1.ExtentsOverscale*1.1;
  tV1.Update3DDisplay(Paper,umCustomOnly);
  end;
end;
end;

procedure TGEO.GEOCloseBtnMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
SetExtentsMode(emUnLatched);
Host.ClientClose;
end;

procedure TGEO.FormActivate(Sender: TObject);
begin
SetExtentsMode(emUnLatched);
FormResize(Self);
end;

procedure TGEO.GEOPointsUPMouseUp(Sender: TObject;Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var
SelectedData :TGridRect;
begin
if  ReportPointGrid.Row > 1 then
    begin
    ReportPointGrid.Row := ReportPointGrid.Row-1;
    SelectedData .TopLeft.y := ReportPointGrid.Row;
    SelectedData .TopLeft.x := 1;
    SelectedData .BottomRight.y := ReportPointGrid.Row;
    SelectedData .BottomRight.x := 3;
    ReportPointGrid.Selection := SelectedData;
    end;
end;

procedure TGEO.GEOPointsDownMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var
SelectedData :TGridRect;
begin
if  ReportPointGrid.Row < (ReportPointGrid.RowCount-1) then
    begin
    ReportPointGrid.Row := ReportPointGrid.Row+1;
    SelectedData .TopLeft.y := ReportPointGrid.Row;
    SelectedData .TopLeft.x := 1;
    SelectedData .BottomRight.y := ReportPointGrid.Row;
    SelectedData .BottomRight.x := 3;
    ReportPointGrid.Selection := SelectedData;
    end;

end;

procedure TGEO.GEOVectorUpMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
if VectorSelectList.ItemIndex > 0 then
   begin
   VectorSelectList.ItemIndex := VectorSelectList.ItemIndex-1;
   VectorSelectListChange(Self)
   end;
end;

procedure TGEO.GeoVectorDownMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
if VectorSelectList.ItemIndex < (VectorSelectList.Items.Count-1) then
   begin
   VectorSelectList.ItemIndex := VectorSelectList.ItemIndex+1;
   VectorSelectListChange(Self)
   end;
end;


procedure TGEO.ShowDebugState(Sender: TObject);
begin
end;

procedure TGEO.DebufStartButtonClick(Sender: TObject);
var
ResLinear,ResRotary : TXYZ;
begin
DebugStartPositions := TV1.GetAllAxisPOsitions;
DEbugStartStage := 0;
GEO.TV1.DebugStep := True;
GEO.TV1.GetNDP(0,ResLinear,ResRotary,0,0);
DebugStageDisplay.Text := IntToStr(DEbugStartStage);
UpdateEverythingAndDisplay;
end;

procedure TGEO.DebugStepButtonClick(Sender: TObject);
var
ResLinear,ResRotary : TXYZ;
begin
GEO.TV1.DebugStep := True;
GEO.TV1.GetNDP(0,ResLinear,ResRotary,0,0);
UpdateEverythingAndDisplay;
DebugStageDisplay.Text := IntToStr(DEbugStartStage);
end;




procedure TGEO.FinalInstall;
begin
  inherited;
  InchModeTag := Named.AquireTag('NC1InchModeDefault', 'TMethodTag', nil);
  UserLevel   := Named.AquireTag('AccessLevel', 'TIntegerTag', TagChangeHandler);
  InchMode := Named.GetAsBoolean(InchModeTag);
  GEONavOPtionTag := Named.AquireTag('_OptionGEONav', 'TIntegerTag', nil);
  GEONavInstalled := Named.GetAsBoolean(GEONavOPtionTag);
end;

procedure GTTXform(var XYZPOs : TPlocation);
begin
XYZPOs.x := -XYZPOs.x;
end;
{ TCentreFromNPoints }

function TCentreFromNPoints.Name: WideString;
begin
Result := 'FindPointsCentre'
end;

function TCentreFromNPoints.ParameterCount: Integer;
begin
Result := 3
end;

procedure TCentreFromNPoints.Reset;
begin
  inherited;

end;

function TCentreFromNPoints.ReturnType: Integer;
begin
  Result:= dtString;
end;

function TCentreFromNPoints.ParameterType(Index: Integer): Integer;
begin
  Result:= dtString;
end;

function TCentreFromNPoints.IsFunction: Boolean;
begin
Result := False;
end;

function TCentreFromNPoints.ParameterName(Index: Integer): WideString;
begin

end;

procedure TCentreFromNPoints.Execute(const Res: Pointer;
  const Params: array of Pointer; ParamCount: Integer);
var
PStr    : String;
Buffer  : array[0..256] of Char;
P1Index : Integer;
NumberPoints : Integer;
Location : TPLocation;
ResIndex : Integer;
P : Integer;
LocPoint : PLoc;
begin
  MeasuredPOints := TList.create;

  try
    Named.GetAsString(Params[0], Buffer, 256);
    PStr := Buffer;
    P1Index := StrToIntDef(PStr,0);

    Named.GetAsString(Params[1], Buffer, 256);
    PStr := Buffer;
    NumberPoints := StrToIntDef(PStr,0);

    Named.GetAsString(Params[2], Buffer, 256);
    PStr := Buffer;
    ResIndex := StrToIntDef(PStr,0);

    if NumberPoints > 0 then
      begin
      For P := 0  to NumberPoints-1 do
        begin
        New(LocPOint);
        LocPOint.x := Host.GetTransferData(P1Index+P,0);
        LocPOint.y := Host.GetTransferData(P1Index+P,1);
        LocPOint.z := 0;
        MeasuredPOints.Add(LocPoint);
        end;
      FindXYCentreofGravity;
      Host.SetTransferData(ResIndex,0,CentreGravity.X);
      Host.SetTransferData(ResIndex,1,CentreGravity.Y);
      Host.SetTransferData(ResIndex,2,0);
      end;

   Finally
   while (MeasuredPoints.Count > 0) do
     begin
     LocPoint := MeasuredPoints[0];
     Dispose(LocPoint);
     MeasuredPoints.Delete(0);
     end;
   MeasuredPoints.Free;
   end;
end;

function TCentreFromNPoints.ParameterPassing(Index: Integer): Integer;
begin

end;


function TInterpolateMDTColumn.Name: WideString;
begin
Result := 'InterpolateMDTCol';
end;

function TInterpolateMDTColumn.ParameterCount: Integer;
begin
Result := 4
end;

procedure TInterpolateMDTColumn.Reset;
begin
  inherited;
end;

function TInterpolateMDTColumn.ReturnType: Integer;
begin
  Result:= dtString;
end;

function TInterpolateMDTColumn.ParameterType(Index: Integer): Integer;
begin
  Result:= dtString;
end;

function TInterpolateMDTColumn.IsFunction: Boolean;
begin
Result := True;
end;

function TInterpolateMDTColumn.ParameterName(Index: Integer): WideString;
begin

end;

procedure TInterpolateMDTColumn.Execute(const Res: Pointer;
  const Params: array of Pointer; ParamCount: Integer);
var
GEOResultRow  : INteger;
Buffer  : array[0..256] of Char;
MDTName : String;
ColName : String;
FirstRow,LastRow,Result : Integer;
SubSet : Boolean;
begin

  Named.GetAsString(Params[0], Buffer, 256);
  MDTName := Buffer;

  Named.GetAsString(Params[1], Buffer, 256);
  ColName := Buffer;

  Named.GetAsString(Params[2], Buffer, 256);
  FirstRow := StrToIntDef(Buffer,0);

  Named.GetAsString(Params[3], Buffer, 256);
  LastRow := StrToIntDef(Buffer,0);

  if (LastRow > FirstRow) then
    begin
    Result := GEO.InterpolateMDTCol(MDTName,ColName,FirstRow,LastRow);
    Named.SetAsInteger(Res,REsult);
    end
  else
    Named.SetAsInteger(Res,-2);
end;

function TInterpolateMDTColumn.ParameterPassing(Index: Integer): Integer;
begin

end;


function TApplyUniqueElectrodeOffsets.Name: WideString;
begin
  Result:= 'ApplyUniqueElectrodeOffsets';
end;

function TApplyUniqueElectrodeOffsets.ParameterCount: Integer;
begin
Result := 4;
end;

procedure TApplyUniqueElectrodeOffsets.Reset;
begin
  inherited;

end;

function TApplyUniqueElectrodeOffsets.ReturnType: Integer;
begin
  Result:= dtString;
end;

function TApplyUniqueElectrodeOffsets.ParameterType(Index: Integer): Integer;
begin
  Result:= dtString;
end;

function TApplyUniqueElectrodeOffsets.IsFunction: Boolean;
begin
Result := False;
end;

function TApplyUniqueElectrodeOffsets.ParameterName(Index: Integer): WideString;
begin

end;

procedure TApplyUniqueElectrodeOffsets.Execute(const Res: Pointer;
  const Params: array of Pointer; ParamCount: Integer);
var
PStr    : String;
Buffer  : array[0..256] of Char;
XYZOffset: TPLocation;
Standoff : Double;

begin

  Named.GetAsString(Params[0], Buffer, 256);
  PStr:= Buffer;
  XYZOffset.x := StrToFloatDef(PStr,0);

  Named.GetAsString(Params[1], Buffer, 256);
  PStr:= Buffer;
  XYZOffset.y := StrToFloatDef(PStr,0);

  Named.GetAsString(Params[2], Buffer, 256);
  PStr:= Buffer;
  XYZOffset.z := StrToFloatDef(PStr,0);

  Named.GetAsString(Params[3], Buffer, 256);
  PStr:= Buffer;
  Standoff := StrToFloatDef(PStr,0);

  GEO.TV1.GTSetUniqueElectrodeOffsets(XYZOffset,Standoff);

end;

function TApplyUniqueElectrodeOffsets.ParameterPassing(Index: Integer): Integer;
begin

end;



procedure TGEO.GetGTMachineToolRetractVector(VectorLength: Double; var IncResult: TXYZ);
begin
GEO.TV1.GetGTMachineToolRetractVector(VectorLength,IncResult);
end;


function TGetToolRetractPoint.Name: WideString;
begin
 Result := 'GetToolRetractVector'
end;

function TGetToolRetractPoint.ParameterCount: Integer;
begin
Result := 2;
end;

procedure TGetToolRetractPoint.Reset;
begin
  inherited;

end;

function TGetToolRetractPoint.ReturnType: Integer;
begin
  Result:= dtString;
end;

function TGetToolRetractPoint.ParameterType(Index: Integer): Integer;
begin
  Result:= dtString;
end;

function TGetToolRetractPoint.IsFunction: Boolean;
begin
Result := False;
end;

function TGetToolRetractPoint.ParameterName(Index: Integer): WideString;
begin

end;

// First Parameter is distance along ToolVector with Negative being away from part
// Second Parameter is Index In Geo array for XYZ incremental moves to move along vector
procedure TGetToolRetractPoint.Execute(const Res: Pointer;
  const Params: array of Pointer; ParamCount: Integer);
var
PStr    : String;
GEOResultRow  : INteger;
Buffer  : array[0..256] of Char;
VectorLength : Double;
IncResult : TXYZ;
begin
  Named.GetAsString(Params[0], Buffer, 256);
  PStr := Buffer;
  VectorLength := StrToFloatDef(PStr,0);
  Named.GetAsString(Params[1], Buffer, 256);
  PStr:= Buffer;
  GEOResultRow := StrToIntDef(PStr,0);

  GEO.GetGTMachineToolRetractVector(VectorLength,IncResult);

  Host.SetTransferData(GEOResultRow,0,IncResult.X);
  Host.SetTransferData(GEOResultRow,1,IncResult.Y);
  Host.SetTransferData(GEOResultRow,2,IncResult.Z);
end;

function TGetToolRetractPoint.ParameterPassing(Index: Integer): Integer;
begin

end;


function TCheckAxiesInLimits.Name: WideString;
begin

end;

function TCheckAxiesInLimits.ParameterCount: Integer;
begin

end;

procedure TCheckAxiesInLimits.Reset;
begin
  inherited;

end;

function TCheckAxiesInLimits.ReturnType: Integer;
begin

end;

function TCheckAxiesInLimits.ParameterType(Index: Integer): Integer;
begin

end;

function TCheckAxiesInLimits.IsFunction: Boolean;
begin

end;

function TCheckAxiesInLimits.ParameterName(Index: Integer): WideString;
begin

end;

procedure TCheckAxiesInLimits.Execute(const Res: Pointer;
  const Params: array of Pointer; ParamCount: Integer);
begin
  inherited;
end;

function TCheckAxiesInLimits.ParameterPassing(Index: Integer): Integer;
begin

end;





procedure TGEO.GetGTGetErrowaOffset(ResIndex: Integer; var OffsetResult: TXYZ);
var
StartPositions : TAxisPosn;
begin
StartPositions := TV1.GetAllAxisPOsitions;
InhibitExternalUpdate := True;
  try
  OffsetResult := GEO.TV1.GetGTErrowsOfset;
  finally
  Tv1.RestoreAllAxisPOsitions(StartPositions);
  GEO.InhibitExternalUpdate := False;
  end;

end;

{ TGetGTErrowaOffset }

function TGetGTErrowaOffset.Name: WideString;
begin
  Result:= 'GetErrowaOffset';
end;

function TGetGTErrowaOffset.ParameterCount: Integer;
begin
Result := 1;
end;

procedure TGetGTErrowaOffset.Reset;
begin
  inherited;

end;

function TGetGTErrowaOffset.ReturnType: Integer;
begin
  Result:= dtString;
end;

function TGetGTErrowaOffset.ParameterType(Index: Integer): Integer;
begin
  Result:= dtString;
end;

function TGetGTErrowaOffset.IsFunction: Boolean;
begin
Result := False;
end;

function TGetGTErrowaOffset.ParameterName(Index: Integer): WideString;
begin

end;

procedure TGetGTErrowaOffset.Execute(const Res: Pointer;
  const Params: array of Pointer; ParamCount: Integer);
var
PStr    : String;
Buffer  : array[0..256] of Char;
Offset  : TXYZ;
ResIndex : Integer;
begin
  Named.GetAsString(Params[0], Buffer, 256);
  PStr := Buffer;
  ResIndex := StrToIntDef(PStr,0);

  GEO.GetGTGetErrowaOffset(ResIndex,Offset);

  Host.SetTransferData(ResIndex,0,Offset.X);
  Host.SetTransferData(ResIndex,1,Offset.Y);
  Host.SetTransferData(ResIndex,2,Offset.Z);

end;

function TGetGTErrowaOffset.ParameterPassing(Index: Integer): Integer;
begin

end;


{ TGetGTNDPWithFixedRotary }

function TGetGTNDPWithFixedRotary.Name: WideString;
begin
  Result:= 'GetNDPWithFixedRotary';
end;

{
Works On current Drill Vector as set by GeonavsetHoleVector
P[0] = Fixed value of Fixed Rotary Angle
P[1] = Axis to Fix = a or b or c
P[2] = GEO Index for XYZ axis pos result (Integer)
P[3] = GEO Index for ABC axis pos result (Integer)
p[4] = Solution Number
}
function TGetGTNDPWithFixedRotary.ParameterCount: Integer;
begin
Result := 6;
end;

procedure TGetGTNDPWithFixedRotary.Reset;
begin
  inherited;
end;

function TGetGTNDPWithFixedRotary.ReturnType: Integer;
begin
  Result:= dtString;
end;

function TGetGTNDPWithFixedRotary.ParameterType(Index: Integer): Integer;
begin
  Result:= dtString;
end;

function TGetGTNDPWithFixedRotary.IsFunction: Boolean;
begin
Result := True;
end;

function TGetGTNDPWithFixedRotary.ParameterName(Index: Integer): WideString;
begin

end;

procedure TGetGTNDPWithFixedRotary.Execute(const Res: Pointer;  const Params: array of Pointer; ParamCount: Integer);
var
PStr    : String;
GEORow  : INteger;
Buffer  : array[0..256] of Char;
RotAxisPos : Double;
LinResIndex,RotResIndex : Integer;
SolNum,FixedIndex : Integer;
LinPos,RotPos : TXYZ;
InverseVecNum : INteger;
InverseVec : Boolean;
begin
  Named.GetAsString(Params[0], Buffer, 256);
  PStr := Buffer;
  RotAxisPos := StrToFloatDef(PStr,0);

  Named.GetAsString(Params[1], Buffer, 256);
  PStr:= Buffer;
  PStr := UpperCase(PStr);
  if PStr = 'A' then
    FixedIndex := 0
  else if PStr = 'B' then
    FixedIndex := 1
  else if PStr = 'C' then
    FixedIndex := 2
  else
    FixedIndex := 0;

  Named.GetAsString(Params[2], Buffer, 256);
  PStr:= Buffer;
  LinResIndex := StrToIntDef(PStr,0);

  Named.GetAsString(Params[3], Buffer, 256);
  PStr:= Buffer;
  RotResIndex := StrToIntDef(PStr,0);

  Named.GetAsString(Params[4], Buffer, 256);
  PStr:= Buffer;
  SolNum := StrToIntDef(PStr,0);

  Named.GetAsString(Params[5], Buffer, 256);
  PStr:= Buffer;
  InverseVecNum := StrToIntDef(PStr,0);
  InverseVec :=  InverseVecNum <> 0;

  GEO.GetGTMachineNDPForFixedRotaryPos(RotAxisPos,FixedIndex,LinPos,RotPos,SolNum,InverseVec,False);

  Host.SetTransferData(LinResIndex,0,Linpos.X);
  Host.SetTransferData(LinResIndex,1,Linpos.Y);
  Host.SetTransferData(LinResIndex,2,Linpos.Z);

  Host.SetTransferData(RotResIndex,0,Rotpos.X);
  Host.SetTransferData(RotResIndex,1,Rotpos.Y);
  Host.SetTransferData(RotResIndex,2,Rotpos.Z);
end;

function TGetGTNDPWithFixedRotary.ParameterPassing(Index: Integer): Integer;
begin
end;

procedure TGEO.GetGTMachineNDPForFixedRotaryPos(FixedRot: Double;
                                                FixedAxisIndex: Integer;
                                                var ResLinear, ResRotary: TXYZ;
                                                Solution: Integer;
                                                InverseVector: Boolean;
                                                IgnoreLimits : Boolean);
var
StartPositions : TAxisPosn;
CurrentStage     : Integer;
NewPOsition    : TAxisPosn;
NDPError       : TNDPErrorTypes;
begin

StartPositions := TV1.GetAllAxisPOsitions;
InhibitExternalUpdate := True;
try
DEbugStartStage := 0;
DebugMemo.Lines.Clear;
NDPError := GEO.TV1.GetGTNDPForFixedRotary(FixedRot,FixedAxisIndex,ResLinear,ResRotary,Solution,InverseVector,IgnoreLimits);
if NDPError = ndpOk then
  begin
  Tv1.RestoreAllAxisPOsitions(StartPositions);
  end
else
  begin
  case  NDPError of
   ndpNoPartVector           : Named.SetFaultA(Self.Name,'No Nominal Drill vetor defined',faultStopCycle,nil);
   ndpRequiredXOutsidePosLimits : Named.SetFaultA(Self.Name,'X axis outside positive limits for required Machine Nominal',faultStopCycle,nil);
   ndpRequiredXOutsideNegLimits : Named.SetFaultA(Self.Name,'X axis outside negative limits for required Machine Nominal',faultStopCycle,nil);
   ndpRequiredYOutsidePosLimits : Named.SetFaultA(Self.Name,'Y axis outside positive limits for required Machine Nominal',faultStopCycle,nil);
   ndpRequiredYOutsideNegLimits : Named.SetFaultA(Self.Name,'Y axis outside negative limits for required Machine Nominal',faultStopCycle,nil);
   ndpRequiredZOutsidePosLimits : Named.SetFaultA(Self.Name,'Z axis outside positive limits for required Machine Nominal',faultStopCycle,nil);
   ndpRequiredZOutsideNegLimits : Named.SetFaultA(Self.Name,'Z axis outside negative limits for required Machine Nominal',faultStopCycle,nil);
   ndpRequiredAOutsidePosLimits : Named.SetFaultA(Self.Name,'A axis outside positive limits for required Machine Nominal',faultStopCycle,nil);
   ndpRequiredAOutsideNegLimits : Named.SetFaultA(Self.Name,'A axis outside negative limits for required Machine Nominal',faultStopCycle,nil);
   ndpRequiredBOutsidePosLimits : Named.SetFaultA(Self.Name,'B axis outside positive limits for required Machine Nominal',faultStopCycle,nil);
   ndpRequiredBOutsideNegLimits : Named.SetFaultA(Self.Name,'B axis outside negative limits for required Machine Nominal',faultStopCycle,nil);
  end; // case
  end
finally
GEO.InhibitExternalUpdate := False;
end;
end;

procedure TGEO.GetGTMachineNDPForDefinedAppVecZRotation(Zrot: Double; var ResLinear, ResRotary: TXYZ; Solution: Integer;InverseVector: Boolean);
var
StartPositions : TAxisPosn;
CurrentStage     : Integer;
NewPOsition    : TAxisPosn;
NDPError       : TNDPErrorTypes;
begin

StartPositions := TV1.GetAllAxisPOsitions;
InhibitExternalUpdate := True;
try
DEbugStartStage := 0;
DebugMemo.Lines.Clear;
NDPError := GEO.TV1.GetGTNDPForZAlignedHole(ZRot,ResLinear,ResRotary,Solution,InverseVector);
if NDPError = ndpOk then
  begin
  Tv1.RestoreAllAxisPOsitions(StartPositions);
  end
else
  begin
  case  NDPError of
   ndpNoPartVector           : Named.SetFaultA(Self.Name,'No Nominal Drill vetor defined',faultStopCycle,nil);
   ndpRequiredXOutsidePosLimits : Named.SetFaultA(Self.Name,'X axis outside positive limits for required Machine Nominal',faultStopCycle,nil);
   ndpRequiredXOutsideNegLimits : Named.SetFaultA(Self.Name,'X axis outside negative limits for required Machine Nominal',faultStopCycle,nil);
   ndpRequiredYOutsidePosLimits : Named.SetFaultA(Self.Name,'Y axis outside positive limits for required Machine Nominal',faultStopCycle,nil);
   ndpRequiredYOutsideNegLimits : Named.SetFaultA(Self.Name,'Y axis outside negative limits for required Machine Nominal',faultStopCycle,nil);
   ndpRequiredZOutsidePosLimits : Named.SetFaultA(Self.Name,'Z axis outside positive limits for required Machine Nominal',faultStopCycle,nil);
   ndpRequiredZOutsideNegLimits : Named.SetFaultA(Self.Name,'Z axis outside negative limits for required Machine Nominal',faultStopCycle,nil);
   ndpRequiredAOutsidePosLimits : Named.SetFaultA(Self.Name,'A axis outside positive limits for required Machine Nominal',faultStopCycle,nil);
   ndpRequiredAOutsideNegLimits : Named.SetFaultA(Self.Name,'A axis outside negative limits for required Machine Nominal',faultStopCycle,nil);
   ndpRequiredBOutsidePosLimits : Named.SetFaultA(Self.Name,'B axis outside positive limits for required Machine Nominal',faultStopCycle,nil);
   ndpRequiredBOutsideNegLimits : Named.SetFaultA(Self.Name,'B axis outside negative limits for required Machine Nominal',faultStopCycle,nil);
  end; // case
  end
finally
GEO.InhibitExternalUpdate := False;
end;
end;

function TGEO.NewGetGTMachineNDPForDefinedAppVecZRotation(Zrot: Double;
                                                          var ResLinear, ResRotary: TXYZ;
                                                          Solution: Integer;
                                                          InverseVector: Boolean): Integer;
var
StartPositions : TAxisPosn;
CurrentStage     : Integer;
NewPOsition    : TAxisPosn;
NDPError       : TNDPErrorTypes;
begin
Result := 0;
StartPositions := TV1.GetAllAxisPOsitions;
InhibitExternalUpdate := True;
try
DEbugStartStage := 0;
DebugMemo.Lines.Clear;
NDPError := GEO.TV1.NewGetGTNDPForZAlignedHole(ZRot,ResLinear,ResRotary,Solution,InverseVector);
if NDPError = ndpOk then
  begin
  Tv1.RestoreAllAxisPOsitions(StartPositions);
  Result := 1;
  end
else
  begin
  Result := 0;
  case  NDPError of
   ndpItterateError :Named.SetFaultA(Self.Name,'Itteration Error',faultStopCycle,nil);
//   ndpNoPartVector           : Named.SetFaultA(Self.Name,'No Nominal Drill vetor defined',faultStopCycle,nil);
   ndpRequiredXOutsidePosLimits : Named.SetFaultA(Self.Name,'X axis outside positive limits for required Machine Nominal',faultStopCycle,nil);
   ndpRequiredXOutsideNegLimits : Named.SetFaultA(Self.Name,'X axis outside negative limits for required Machine Nominal',faultStopCycle,nil);
   ndpRequiredYOutsidePosLimits : Named.SetFaultA(Self.Name,'Y axis outside positive limits for required Machine Nominal',faultStopCycle,nil);
   ndpRequiredYOutsideNegLimits : Named.SetFaultA(Self.Name,'Y axis outside negative limits for required Machine Nominal',faultStopCycle,nil);
   ndpRequiredZOutsidePosLimits : Named.SetFaultA(Self.Name,'Z axis outside positive limits for required Machine Nominal',faultStopCycle,nil);
   ndpRequiredZOutsideNegLimits : Named.SetFaultA(Self.Name,'Z axis outside negative limits for required Machine Nominal',faultStopCycle,nil);
   ndpRequiredAOutsidePosLimits : Named.SetFaultA(Self.Name,'A axis outside positive limits for required Machine Nominal',faultStopCycle,nil);
   ndpRequiredAOutsideNegLimits : Named.SetFaultA(Self.Name,'A axis outside negative limits for required Machine Nominal',faultStopCycle,nil);
   ndpRequiredBOutsidePosLimits : Named.SetFaultA(Self.Name,'B axis outside positive limits for required Machine Nominal',faultStopCycle,nil);
   ndpRequiredBOutsideNegLimits : Named.SetFaultA(Self.Name,'B axis outside negative limits for required Machine Nominal',faultStopCycle,nil);
  end; // case
  end
finally
GEO.InhibitExternalUpdate := False;
end;
end;

procedure TGEO.TagChange(ATag: TTagRef);
begin
if Atag = UserLevel then
   begin
   if Named.GetAsInteger(ATag) > 5 then
      begin
      ShowPCMButton.Visible := True;
      HidePCM.Visible := True;
      VersionLabel.Visible := True;

      end
   else
       begin
       ShowPCMButton.Visible := False;
       HidePCM.Visible := False;
      VersionLabel.Visible := False;
       end;
   end

end;

{procedure  TGEO.GetGTMachineNDP(FixedRots :TXYZ;var ResLinear : TXYZ;var ResRotary : TXYZ; AxArray : TXYZ; Par1,Par2 : Integer);
var
StartPositions : TAxisPosn;
CurrentStage     : Integer;
NewPOsition    : TAxisPosn;
NDPError       : Integer;
begin

StartPositions := TV1.GetAllAxisPOsitions;
InhibitExternalUpdate := True;
try
DEbugStartStage := 0;
DebugMemo.Lines.Clear;
NDPError := GEO.TV1.GetGTNDP(FixedRots,ResLinear,ResRotary,AxArray,Par1,Par2);
if NDPError = 0 then
  begin
  Tv1.RestoreAllAxisPOsitions(StartPositions);
  end
else
  begin
  case  NDPError of
  1: raise EGEOError.Create('No Nominal Drill vetor defined');
  2: raise EGEOError.Create('Too many itterations to find Drill position');
  end; // case
  end
finally
GEO.InhibitExternalUpdate := False;
end;
end;}

procedure  TGEO.GetMachineNDP(Crot :Double;var ResLinear,ResRotary :TXYZ;Par1,Par2 : Integer);
var
StartPositions : TAxisPosn;
CurrentStage     : Integer;
NewPOsition    : TAxisPosn;
NDPError       : Integer;
begin

StartPositions := TV1.GetAllAxisPOsitions;
InhibitExternalUpdate := True;
try
DEbugStartStage := 0;
DebugMemo.Lines.Clear;
NDPError := GEO.TV1.GetNDP(CRot,ResLinear,ResRotary,Par1,Par2);
if NDPError = 0 then
  begin
  Tv1.RestoreAllAxisPOsitions(StartPositions);
  end
else
  begin
  case  NDPError of
  1: Named.SetFaultA(Self.Name,'No Nominal Drill vetor defined',faultStopCycle,nil);
  2: Named.SetFaultA(Self.Name,'Too many itterations to find Drill position',faultStopCycle,nil);
  end; // case
  end
finally
GEO.InhibitExternalUpdate := False;
end;
end;

procedure TGEO.DisplaySweep;
begin
  inherited;
  try
  TV1.RuntimePaint(Paper.CustomView);
  except
  end;
end;

procedure TGEO.SetExtentsMode(Mode: TExtentsModes);
begin
ExtentsMode := Mode;
case Mode of
  emLatched:
  begin
  PointFitButton.IndicatorOn := True;
  TV1.ExtentsOverscale := 1.1;
  end;

  emUnLatched:
  begin
  PointFitButton.IndicatorOn := False;
  end;
end;

end;



// N.B. At present any EndUpdate will enable update.
// THey are no nested so the update count is not as yet actually implimented
procedure TGEO.ExternalBeginUpdate;
begin
if TV1.GlobalUpdateCount <=0 then
  begin
  TV1.GlobalUpdateCount := 1;
  end
else
  begin
  inc(TV1.GlobalUpdateCount)
  end;


end;

// N.B. At present any EndUpdate will enable update.
// THey are no nested so the update count is not as yet actually implimented
procedure TGEO.ExternalEndUpdate;
begin
  if TV1.GlobalUpdateCount > 0 then
    begin
    TV1.MatchObjectListToTree;
    TV1.GlobalUpdateCount := 0;
    end;
end;

{ TMyScriptMethod }

procedure TagChangeHandler(Tag : TTagRef);stdcall
begin
GEO.TagChange(Tag);
end;

function TGEO.shortestanglebetween(Angle1, Angle2: Double): Double;
var
A1,A2 : Double;
begin
A1 := Mod360(Angle1);
A2 := Mod360(Angle2);
if A1 >= A2 then
  Result := A1-A2
else
  Result := A2-A1;
if Result > 180 then
    begin
    Result := 360-Result;
    end
end;

{ TGetGTNDPWithFixedCandClosestBPosition }

function TGetGTNDPWithFixedCandClosestBPosition.Name: WideString;
begin
  Result:= 'GetNDPWithFixedCAndBClosest';
end;

function TGetGTNDPWithFixedCandClosestBPosition.ParameterCount: Integer;
begin
Result := 5;
end;

procedure TGetGTNDPWithFixedCandClosestBPosition.Reset;
begin
  inherited;

end;

function TGetGTNDPWithFixedCandClosestBPosition.ReturnType: Integer;
begin
  Result:= dtString;
end;

function TGetGTNDPWithFixedCandClosestBPosition.ParameterType(
  Index: Integer): Integer;
begin
  Result:= dtString;
end;

function TGetGTNDPWithFixedCandClosestBPosition.IsFunction: Boolean;
begin
Result := True;
end;

function TGetGTNDPWithFixedCandClosestBPosition.ParameterName(
  Index: Integer): WideString;
begin

end;

procedure TGetGTNDPWithFixedCandClosestBPosition.Execute(const Res: Pointer;
  const Params: array of Pointer; ParamCount: Integer);
var
PStr    : String;
GEORow  : INteger;
Buffer  : array[0..256] of Char;
RotAxisPos : Double;
LinResIndex,RotResIndex : Integer;
FixedIndex : Integer;
LinPos0,RotPos0 : TXYZ;
LinPos1,RotPos1 : TXYZ;
InverseVecNum : INteger;
InverseVec : Boolean;
Bangle : Double;
Bsol0,Bsol1 : Double;
begin

  // C position
  Named.GetAsString(Params[0], Buffer, 256);
  PStr := Buffer;
  RotAxisPos := StrToFloatDef(PStr,0);

  //Linear result index
  Named.GetAsString(Params[1], Buffer, 256);
  PStr:= Buffer;
  LinResIndex := StrToIntDef(PStr,0);

  //Rotary result index
  Named.GetAsString(Params[2], Buffer, 256);
  PStr:= Buffer;
  RotResIndex := StrToIntDef(PStr,0);

  //Bangle
  Named.GetAsString(Params[3], Buffer, 256);
  PStr:= Buffer;
  Bangle := StrToFloatDef(PStr,0.0);
  Bangle := Mod360(Bangle);

  //Use Inverse Vector
  Named.GetAsString(Params[4], Buffer, 256);
  PStr:= Buffer;
  InverseVecNum := StrToIntDef(PStr,0);
  InverseVec :=  InverseVecNum <> 0;

  GEO.GetGTMachineNDPForFixedRotaryPos(RotAxisPos,2,LinPos0,RotPos0,0,InverseVec,True);
  GEO.GetGTMachineNDPForFixedRotaryPos(RotAxisPos,2,LinPos1,RotPos1,1,InverseVec,True);


  Bsol0 := Mod360(RotPos0.Y);
  Bsol1 := Mod360(RotPos1.Y);

  if Geo.Shortestanglebetween(Bsol0,Bangle) < Geo.Shortestanglebetween(Bsol1,Bangle) then
    begin
    Host.SetTransferData(LinResIndex,0,Linpos0.X);
    Host.SetTransferData(LinResIndex,1,Linpos0.Y);
    Host.SetTransferData(LinResIndex,2,Linpos0.Z);
    Host.SetTransferData(RotResIndex,0,Rotpos0.X);
    Host.SetTransferData(RotResIndex,1,Rotpos0.Y);
    Host.SetTransferData(RotResIndex,2,Rotpos0.Z);
    end
  else
    begin
    Host.SetTransferData(LinResIndex,0,Linpos1.X);
    Host.SetTransferData(LinResIndex,1,Linpos1.Y);
    Host.SetTransferData(LinResIndex,2,Linpos1.Z);
    Host.SetTransferData(RotResIndex,0,Rotpos1.X);
    Host.SetTransferData(RotResIndex,1,Rotpos1.Y);
    Host.SetTransferData(RotResIndex,2,Rotpos1.Z);
    end;
end;

function TGetGTNDPWithFixedCandClosestBPosition.ParameterPassing(
  Index: Integer): Integer;
begin

end;



function TNewGetGTNDPAlignHoleVector.Name: WideString;
begin
  Result:= 'GetNDPWithHoleAlignedV2';
end;

function TNewGetGTNDPAlignHoleVector.ParameterCount: Integer;
begin
  Result:= 5
end;

procedure TNewGetGTNDPAlignHoleVector.Reset;
begin
end;

function TNewGetGTNDPAlignHoleVector.ReturnType: Integer;
begin
  Result:= dtString;
end;

function TNewGetGTNDPAlignHoleVector.ParameterType(Index: Integer): Integer;
begin
  Result:= dtString;
end;

function TNewGetGTNDPAlignHoleVector.IsFunction: Boolean;
begin
Result := True
end;

function TNewGetGTNDPAlignHoleVector.ParameterName(Index: Integer): WideString;
begin

end;

procedure TNewGetGTNDPAlignHoleVector.Execute(const Res: Pointer;
  const Params: array of Pointer; ParamCount: Integer);
var
PStr    : String;
GEORow  : INteger;
Buffer  : array[0..256] of Char;
ZRot : Double;
LinResIndex,RotResIndex : Integer;
SolNum : Integer;
LinPos,RotPos : TXYZ;
InverseVec : Boolean;
InverseVecNum : Integer;
Result : Integer;
begin
  Named.GetAsString(Params[0], Buffer, 256);
  PStr := Buffer;
  ZRot := StrToFloatDef(PStr,0);

  Named.GetAsString(Params[1], Buffer, 256);
  PStr:= Buffer;
  LinResIndex := StrToIntDef(PStr,0);

  Named.GetAsString(Params[2], Buffer, 256);
  PStr:= Buffer;
  RotResIndex := StrToIntDef(PStr,0);

  Named.GetAsString(Params[3], Buffer, 256);
  PStr:= Buffer;
  SolNum := StrToIntDef(PStr,0);

  Named.GetAsString(Params[4], Buffer, 256);
  PStr:= Buffer;
  InverseVecNum := StrToIntDef(Pstr,0);
  InverseVec := InverseVecNum <> 0;

  GEO.NewGetGTMachineNDPForDefinedAppVecZRotation(Zrot,LinPos,RotPos,SolNum,InverseVec);


  Named.SetAsInteger(Res,Result);

  Host.SetTransferData(LinResIndex,0,Linpos.X);
  Host.SetTransferData(LinResIndex,1,Linpos.Y);
  Host.SetTransferData(LinResIndex,2,Linpos.Z);

  Host.SetTransferData(RotResIndex,0,Rotpos.X);
  Host.SetTransferData(RotResIndex,1,Rotpos.Y);
  Host.SetTransferData(RotResIndex,2,Rotpos.Z);
end;




function TNewGetGTNDPAlignHoleVector.ParameterPassing(Index: Integer): Integer;
begin

end;

{ TGetGTNDPAlignHoleVectorClosestBPos }

function TGetGTNDPAlignHoleVectorClosestBPos.Name: WideString;
begin
  Result:= 'GetNDPHoleAlignedAndBClosest';
end;

function TGetGTNDPAlignHoleVectorClosestBPos.ParameterCount: Integer;
begin
  Result:= 5
end;

procedure TGetGTNDPAlignHoleVectorClosestBPos.Reset;
begin
  inherited;

end;

function TGetGTNDPAlignHoleVectorClosestBPos.ReturnType: Integer;
begin
  Result:= dtString;
end;

function TGetGTNDPAlignHoleVectorClosestBPos.ParameterType(
  Index: Integer): Integer;
begin
  Result:= dtString;
end;

function TGetGTNDPAlignHoleVectorClosestBPos.IsFunction: Boolean;
begin
Result := True
end;

function TGetGTNDPAlignHoleVectorClosestBPos.ParameterName(Index: Integer): WideString;
begin

end;

procedure TGetGTNDPAlignHoleVectorClosestBPos.Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer);
var
PStr    : String;
GEORow  : INteger;
Buffer  : array[0..256] of Char;
ZRot : Double;
LinResIndex,RotResIndex : Integer;
Bangle : Double;
LinPos0,RotPos0 : TXYZ;
LinPos1,RotPos1 : TXYZ;
InverseVec : Boolean;
InverseVecNum : Integer;
Result : Integer;
Bsol0,Bsol1 : Double;
begin
  //alignment angle
  Named.GetAsString(Params[0], Buffer, 256);
  PStr := Buffer;
  ZRot := StrToFloatDef(PStr,0);

  //Linear result index
  Named.GetAsString(Params[1], Buffer, 256);
  PStr:= Buffer;
  LinResIndex := StrToIntDef(PStr,0);

  //Rotary result index
  Named.GetAsString(Params[2], Buffer, 256);
  PStr:= Buffer;
  RotResIndex := StrToIntDef(PStr,0);

  //Bangle
  Named.GetAsString(Params[3], Buffer, 256);
  PStr:= Buffer;
  Bangle := StrToFloatDef(PStr,0.0);
  Bangle := Mod360(Bangle);

  //Use Inverse Vector
  Named.GetAsString(Params[4], Buffer, 256);
  PStr:= Buffer;
  InverseVecNum := StrToIntDef(Pstr,0);
  InverseVec := InverseVecNum <> 0;

  // get Solution Zero
  GEO.GetGTMachineNDPForDefinedAppVecZRotation(Zrot,LinPos0,RotPos0,0,InverseVec);
  GEO.GetGTMachineNDPForDefinedAppVecZRotation(Zrot,LinPos1,RotPos1,1,InverseVec);

  Bsol0 := Mod360(RotPos0.Y);
  Bsol1 := Mod360(RotPos1.Y);

  Named.SetAsInteger(Res,Result);

  if Geo.Shortestanglebetween(Bsol0,Bangle) < Geo.Shortestanglebetween(Bsol1,Bangle) then
    begin
    Host.SetTransferData(LinResIndex,0,Linpos0.X);
    Host.SetTransferData(LinResIndex,1,Linpos0.Y);
    Host.SetTransferData(LinResIndex,2,Linpos0.Z);

    Host.SetTransferData(RotResIndex,0,Rotpos0.X);
    Host.SetTransferData(RotResIndex,1,Rotpos0.Y);
    Host.SetTransferData(RotResIndex,2,Rotpos0.Z);
    end
  else
    begin
    Host.SetTransferData(LinResIndex,0,Linpos1.X);
    Host.SetTransferData(LinResIndex,1,Linpos1.Y);
    Host.SetTransferData(LinResIndex,2,Linpos1.Z);

    Host.SetTransferData(RotResIndex,0,Rotpos1.X);
    Host.SetTransferData(RotResIndex,1,Rotpos1.Y);
    Host.SetTransferData(RotResIndex,2,Rotpos1.Z);
    end;
end;

function TGetGTNDPAlignHoleVectorClosestBPos.ParameterPassing(
  Index: Integer): Integer;
begin

end;

{ TGTAdjustToolDatumLocation }
function TGTAdjustToolDatumLocation.Name: WideString;
begin
Result := 'AdjustToolDatum';
end;

function TGTAdjustToolDatumLocation.ParameterCount: Integer;
begin
Result := 3;
end;

procedure TGTAdjustToolDatumLocation.Reset;
begin
  inherited;

end;

function TGTAdjustToolDatumLocation.ReturnType: Integer;
begin
  Result:= dtString;
end;

function TGTAdjustToolDatumLocation.ParameterType(Index: Integer): Integer;
begin

end;

function TGTAdjustToolDatumLocation.IsFunction: Boolean;
begin
Result := False;
end;

function TGTAdjustToolDatumLocation.ParameterName(Index: Integer): WideString;
begin

end;

procedure TGTAdjustToolDatumLocation.Execute(const Res: Pointer;
  const Params: array of Pointer; ParamCount: Integer);
var
PStr    : String;
FPRow  : Integer;
Adjust : TPlocation;
ResIndex : Integer;
Buffer  : array[0..256] of Char;
XYZ,ABC : TPlocation;
PResult : TPlocation;
begin
  Named.GetAsString(Params[0], Buffer, 256);
  PStr := Buffer;
  Adjust.x := StrToIntDef(PStr,0);

  Named.GetAsString(Params[1], Buffer, 256);
  PStr := Buffer;
  Adjust.y := StrToIntDef(PStr,0);

  Named.GetAsString(Params[2], Buffer, 256);
  PStr := Buffer;
  Adjust.z := StrToIntDef(PStr,0);

  GEO.TV1.RuntimeToolDatumAdjustby(Adjust);


end;

function TGTAdjustToolDatumLocation.ParameterPassing(Index: Integer): Integer;
begin

end;

{ TGTReturnCurrentStandoff }

function TGTReturnCurrentStandoff.Name: WideString;
begin
Result := 'GetStandoff'
end;

function TGTReturnCurrentStandoff.ParameterCount: Integer;
begin
Result := 0
end;

procedure TGTReturnCurrentStandoff.Reset;
begin
  inherited;

end;

function TGTReturnCurrentStandoff.ReturnType: Integer;
begin
  Result:= dtDouble;
end;

function TGTReturnCurrentStandoff.ParameterType(Index: Integer): Integer;
begin
  Result:= dtString;
end;

function TGTReturnCurrentStandoff.IsFunction: Boolean;
begin
Result := True;
end;

function TGTReturnCurrentStandoff.ParameterName(Index: Integer): WideString;
begin

end;

procedure TGTReturnCurrentStandoff.Execute(const Res: Pointer;
  const Params: array of Pointer; ParamCount: Integer);
var
SOff : Double;
begin
    SOff := GEo.TV1.AppliedStanfoff;
    Named.SetAsDouble(Res,Soff);
end;

function TGTReturnCurrentStandoff.ParameterPassing(Index: Integer): Integer;
begin

end;


function TGTProbePointResolve.Name: WideString;
begin
Result := 'ResolveProbePoint'
end;
//P[0] =  GEOIndex 0f XYZ Axis position when probe hit
//P[1] =  GEOIndex 0f ABC Axis position when probe hit
//P[2] = Approach vector for ball radius compenstation
//P[4] = GeoIndex of result XYZ Position
function TGTProbePointResolve.ParameterCount: Integer;
begin
Result := 3;
end;

procedure TGTProbePointResolve.Reset;
begin
  inherited;

end;

function TGTProbePointResolve.ReturnType: Integer;
begin
  Result:= dtString;
end;

function TGTProbePointResolve.ParameterType(Index: Integer): Integer;
begin
  Result:= dtString;
end;

function TGTProbePointResolve.IsFunction: Boolean;
begin
Result := False;
end;

function TGTProbePointResolve.ParameterName(Index: Integer): WideString;
begin

end;

procedure TGTProbePointResolve.Execute(const Res: Pointer;
  const Params: array of Pointer; ParamCount: Integer);
var
PStr    : String;
FPRow  : Integer;
XYZIndex,ABCIndex,IJKIndex : Integer;
ResIndex : Integer;
Buffer  : array[0..256] of Char;
XYZ,ABC : TPlocation;
PResult : TPlocation;
begin
  Named.GetAsString(Params[0], Buffer, 256);
  PStr := Buffer;
  XYZIndex := StrToIntDef(PStr,0);

  Named.GetAsString(Params[1], Buffer, 256);
  PStr := Buffer;
  ABCIndex := StrToIntDef(PStr,0);

  Named.GetAsString(Params[2], Buffer, 256);
  PStr := Buffer;
  ResIndex := StrToIntDef(PStr,0);

  XYZ.x := Host.GetTransferData(XYZIndex,0);
  XYZ.y := Host.GetTransferData(XYZIndex,1);
  XYZ.z := Host.GetTransferData(XYZIndex,2);

  ABC.x := Host.GetTransferData(ABCIndex,0);
  ABC.y := Host.GetTransferData(ABCIndex,1);
  ABC.z := Host.GetTransferData(ABCIndex,2);

  PResult := GEO.TV1.ResolveProbePoint(XYZ,ABC);

  Host.SetTransferData(ResIndex,0,PResult.x);
  Host.SetTransferData(ResIndex,1,PResult.y);
  Host.SetTransferData(ResIndex,2,PResult.z);

end;

function TGTProbePointResolve.ParameterPassing(Index: Integer): Integer;
begin

end;


{ TModifyNDPRotationFrom4Points }

function TModifyNDPRotationFrom4Points.Name: WideString;
begin
Result := 'CorrectNDPPlaneFrom4Points'
end;
// One Parameter .. GEO Index of first XYZ pont (Z is ignored)
function TModifyNDPRotationFrom4Points.ParameterCount: Integer;
begin
  Result := 2;
end;

procedure TModifyNDPRotationFrom4Points.Reset;
begin
  inherited;

end;

function TModifyNDPRotationFrom4Points.ReturnType: Integer;
begin
  Result:= dtString;
end;

function TModifyNDPRotationFrom4Points.ParameterType(Index: Integer): Integer;
begin
  Result:= dtString;
end;

function TModifyNDPRotationFrom4Points.IsFunction: Boolean;
begin
 Result := False;
end;

function TModifyNDPRotationFrom4Points.ParameterName(
  Index: Integer): WideString;
begin

end;

procedure TModifyNDPRotationFrom4Points.Execute(const Res: Pointer;
  const Params: array of Pointer; ParamCount: Integer);
var
PStr    : String;
FPRow  : Integer;
ResIndex : Integer;
Buffer  : array[0..256] of Char;
P1,P2,P3,P4 : TPlocation;
RotResult :  TPlocation;
begin
  Named.GetAsString(Params[0], Buffer, 256);
  PStr := Buffer;
  FPRow := StrToIntDef(PStr,0);

  Named.GetAsString(Params[1], Buffer, 256);
  PStr := Buffer;
  ResIndex := StrToIntDef(PStr,0);

  P1.x := Host.GetTransferData(FPRow,0);
  P1.y := Host.GetTransferData(FPRow,1);
  P1.z := Host.GetTransferData(FPRow,2);

  P2.x := Host.GetTransferData(FPRow+1,0);
  P2.y := Host.GetTransferData(FPRow+1,1);
  P2.z := Host.GetTransferData(FPRow+1,2);

  P3.x := Host.GetTransferData(FPRow+2,0);
  P3.y := Host.GetTransferData(FPRow+2,1);
  P3.z := Host.GetTransferData(FPRow+2,2);

  P4.x := Host.GetTransferData(FPRow+3,0);
  P4.y := Host.GetTransferData(FPRow+3,1);
  P4.z := Host.GetTransferData(FPRow+3,2);



  GEO.TV1.ModifyNDPOffsetFor4Points(P1,P2,P3,P4,RotResult);

  Host.SetTransferData(ResIndex,0,RotResult.x);
  Host.SetTransferData(ResIndex,1,RotResult.y);
  Host.SetTransferData(ResIndex,2,RotResult.z);








end;

function TModifyNDPRotationFrom4Points.ParameterPassing( Index: Integer): Integer;
begin

end;





procedure TCentreFromNPoints.CalcMeasuredMaxMin;
var
PointNumber : Integer;
MaxX,MinX : Double;
MaxY,MinY : Double;
PointData : PLoc;
POint :TPlocation;
begin
if MeasuredPoints.Count < 1 then exit;
MaxX := -100000;
MinX := 100000;
MaxY := -100000;
MinY := 100000;
For PointNumber := 0 to MeasuredPoints.Count-1 do
  begin
  PointData := MeasuredPoints[PointNumber];
  Point.x := PointData.x;
  Point.y := PointData.y;
  Point.z := PointData.z;
  with Point  do
    begin
    if X > MaxX then MaxX := X;
    if Y > MaxY then MaxY := Y;
    if X < MinX then MinX := X;
    if Y < MinY then MinY := Y;
    end
  end;

FMaxXMeasured := MaxX;
FMaxYMeasured := MaxY;
FMinXMeasured := MinX;
FMinYMeasured := MinY;
end;


procedure  TCentreFromNPoints.FindXYCentreofGravity;
var
PointNumber : Integer;
pPoint : PLoc;
PointData : TPLocation;
ItterationCount : Integer;
Found : Boolean;
XMoments : Double;
YMoments : Double;
SumXMoments : Double;
SumYMoments : Double;
LastSum : Double;
CentreX : Double;
CentreY : Double;
MaxAbsXMoment : Double;
MaxAbsYMoment : Double;
begin
Found := False;
if MeasuredPoints.Count < 1 then exit;
CalcMeasuredMaxMin;
ItterationCount := 0;
CentreX := (FMaxXMeasured+FMinXMeasured)/2;
CentreY := (FMaxYMeasured+FMinYMeasured)/2;
while (ItterationCount < 100) and (not Found) do
  begin
  try
  MaxAbsXMoment :=0;
  MaxAbsYMoment :=0;
  SumXMoments := 0;
  SumYMoments := 0;
  For PointNumber := 0 to MeasuredPoints.Count-1 do
    begin
    pPoint := MeasuredPoints[PointNumber];
    PointData.x := pPoint.x;
    PointData.y := pPoint.y;
    XMoments := PointData.X-CentreX;
    YMoments := PointData.Y-CentreY;
    if Abs(XMoments) > MaxAbsXMoment then MaxAbsXMoment := Abs(XMoments);
    if Abs(YMoments) > MaxAbsYMoment then MaxAbsYMoment := Abs(YMoments);
    SumXMoments := SumXMoments+XMoments;
    SumYMoments := SumYMoments+YMoments;
    end;
  Found := (abs(SumXMoments) < 0.01) and (abs(SumYMoments) < 0.01);

  if Found Then
    begin
    CentreGravity.X := CentreX*2;
    CentreGravity.Y := CentreY*2;
    end
  else
    begin
    CentreX := CentreX+(SumXMoments/MeasuredPoints.Count);
    CentreY := CentreY+(SumYMoments/MeasuredPoints.Count);
    end
  finally
  inc(ItterationCount);
  end;
  end;
end;


function TGetGTNDPAlignHoleVector.Name: WideString;
begin
  Result:= 'GetNDPWithHoleAligned';
end;


{
Works On current Drill Vector as set by GeonavsetHoleVector
P[0] = required Z rotation of Hole vector if not vertical
       OR Radial angle of hole at drill position if vertical (0 = on Y0 with X positive and anticlock angle)
P[1] = GEO Index for XYZ axis pos result (Integer)
P[2] = GEO Index for ABC axis pos result (Integer)
p[3] = Solution Number
}
function TGetGTNDPAlignHoleVector.ParameterCount: Integer;
begin
  Result:= 5
end;

procedure TGetGTNDPAlignHoleVector.Reset;
begin
  // Called on reset rewind
end;

// not required
function TGetGTNDPAlignHoleVector.ReturnType: Integer;
begin
  Result:= dtString;
end;

function TGetGTNDPAlignHoleVector.ParameterType(Index: Integer): Integer;
begin
  Result:= dtString;
end;

function TGetGTNDPAlignHoleVector.IsFunction: Boolean;
begin
  Result:= True; // if this has a result
end;

function TGetGTNDPAlignHoleVector.ParameterName(Index: Integer): WideString;
begin
end;

{
Works On current Drill Vector as set by GeonavsetHoleVector
P[0] = required Z rotation of Hole vector if not vertical
       OR Radial angle of hole at drill position if vertical (0 = on Y0 with X positive and anticlock angle)
P[1] = GEO Index for XYZ axis pos result (Integer)
P[2] = GEO Index for ABC axis pos result (Integer)
p[3] = Solution Number
}
procedure TGetGTNDPAlignHoleVector.Execute(const Res: Pointer;
  const Params: array of Pointer; ParamCount: Integer);
var
PStr    : String;
GEORow  : INteger;
Buffer  : array[0..256] of Char;
ZRot : Double;
LinResIndex,RotResIndex : Integer;
SolNum : Integer;
LinPos,RotPos : TXYZ;
InverseVec : Boolean;
InverseVecNum : Integer;
begin
  Named.GetAsString(Params[0], Buffer, 256);
  PStr := Buffer;
  ZRot := StrToFloatDef(PStr,0);

  Named.GetAsString(Params[1], Buffer, 256);
  PStr:= Buffer;
  LinResIndex := StrToIntDef(PStr,0);

  Named.GetAsString(Params[2], Buffer, 256);
  PStr:= Buffer;
  RotResIndex := StrToIntDef(PStr,0);

  Named.GetAsString(Params[3], Buffer, 256);
  PStr:= Buffer;
  SolNum := StrToIntDef(PStr,0);

  Named.GetAsString(Params[4], Buffer, 256);
  PStr:= Buffer;
  InverseVecNum := StrToIntDef(Pstr,0);
  InverseVec := InverseVecNum <> 0;

  GEO.GetGTMachineNDPForDefinedAppVecZRotation(Zrot,LinPos,RotPos,SolNum,InverseVec);

  Host.SetTransferData(LinResIndex,0,Linpos.X);
  Host.SetTransferData(LinResIndex,1,Linpos.Y);
  Host.SetTransferData(LinResIndex,2,Linpos.Z);

  Host.SetTransferData(RotResIndex,0,Rotpos.X);
  Host.SetTransferData(RotResIndex,1,Rotpos.Y);
  Host.SetTransferData(RotResIndex,2,Rotpos.Z);
end;

function TGetGTNDPAlignHoleVector.ParameterPassing(Index: Integer): Integer;
begin

end;



procedure TGEO.Button1Click(Sender: TObject);
begin
GEO.DebugMemo.Lines.Clear;
end;

Initialization
  TAbstractFormPlugin.AddPlugin('GEOPlugin',TGEO);
  TDefaultScriptLibrary.SetName('GT');
  TDefaultScriptLibrary.AddMethod(TGetToolRetractPoint);
  TDefaultScriptLibrary.AddMethod(TGTReturnCurrentStandoff);
  TDefaultScriptLibrary.AddMethod(TGetGTNDPAlignHoleVector);
  TDefaultScriptLibrary.AddMethod(TNewGetGTNDPAlignHoleVector);
  TDefaultScriptLibrary.AddMethod(TGetGTNDPWithFixedRotary);
  TDefaultScriptLibrary.AddMethod(TGetGTErrowaOffset);
  TDefaultScriptLibrary.AddMethod(TApplyUniqueElectrodeOffsets);
  TDefaultScriptLibrary.AddMethod(TInterpolateMDTColumn);
  TDefaultScriptLibrary.AddMethod(TCentreFromNPoints);
  TDefaultScriptLibrary.AddMethod(TModifyNDPRotationFrom4Points);
  TDefaultScriptLibrary.AddMethod(TGTProbePointResolve);
  TDefaultScriptLibrary.AddMethod(TGTAdjustToolDatumLocation);
  TDefaultScriptLibrary.AddMethod(TGetGTNDPAlignHoleVectorClosestBPos);
  TDefaultScriptLibrary.AddMethod(TGetGTNDPWithFixedCandClosestBPosition);

end.
