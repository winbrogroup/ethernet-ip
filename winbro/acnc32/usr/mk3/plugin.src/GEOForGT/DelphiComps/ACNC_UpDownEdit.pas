// UpDownEdit
// Version 1.0 24/11/98

unit ACNC_UpDownEdit;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls,ACNC_Numdis,ExtCtrls, comCtrls;


type
  TUpDownEdit = class(TCustomPanel)
  private
  FValue      : Double;
  FMax        : Double;
  FMin        : Double;
  FDigitsAfter: Integer;
  FOnChange   : TNotifyEvent;
  FOnEdit     : TNotifyEvent;
  SelfEdit    : Boolean;

  FEditEnable : Boolean;


  IntegerPart : Integer;
  FloatPart   : Integer;
  IntegerSpin : TUpDown;
  FloatSpin   : TUpDown;
  EditBox     : TEdit;
  FormString  : String;

  Divider      : Integer;

  procedure SetMax(MaxVal : Double);
  procedure SetMin(MinVal : Double);
  procedure SetValue(Val : Double);
  procedure SetDigitsAfter(Digits : Integer);
  procedure SetEditEnable(Enabled : Boolean);
  function GetHasFocus:Boolean;

  procedure SetOnChange(Value: TNotifyEvent);
  procedure SetOnEdit(Value: TNotifyEvent);
  procedure ChangeEvent(Sender: TObject);
  procedure KeyEvent(Sender: TObject; var Key: Char);

  procedure IntegerClick(Sender: TObject; Button: TUDBtnType);
  procedure FloatClick(Sender: TObject; Button: TUDBtnType);

  procedure MakeNumber;

   { Private declarations }



  protected
    { Protected declarations }
    procedure DoChange;    dynamic;
    procedure DoEditEvent; dynamic;

  public
    { Public declarations }
    constructor Create(AOwner:TComponent); override;
    destructor Destroy; override;
    procedure Resize; override;
    procedure DoExit; override;
    procedure DoEnter; override;



  published
    { Published declarations }
     property Value          : Double        read FValue           write SetValue;
     property Max            : Double        read FMax             write SetMax;
     property Min            : Double        read FMin             write SetMin;
     property DigitsAfter    : Integer       read FDigitsAfter     write SetDigitsAfter;
     property EditEnable     : Boolean       read FEditEnable      write SetEditEnable;
     Property HasFocus       : Boolean       read GetHasFocus;

     property OnChange       : TNotifyEvent  read FOnChange        write SetOnChange;
     property OnEdit         : TNotifyEvent  read FOnEdit          write SetOnEdit;
     property OnExit;
     property TabOrder;
     property TabStop;
     property ShowHint;
     Property OnEnter;

    end;



procedure Register;

implementation

procedure Register;
begin
  RegisterComponents('ACNCComps', [TUpDownEdit]);
end;


constructor TUpDownEdit.Create(AOwner:TComponent);
begin
inherited Create(AOwner);
SelfEdit := False;
Width := 60;
IntegerSpin := TUpDown.Create(Self);
with IntegerSpin do
     begin
     Parent := Self;
     Position := 0;
     Max := 10001;
     Min := -1;
     OnClick := IntegerClick;
     end;
FloatSpin := TUpDown.Create(Self);
with FloatSpin do
     begin
     Parent := Self;
     Position := 0;
     Max := 10001;
     Min := -1;
     OnClick := FloatClick;
     end;

EditBox := TEdit.Create(Self);
EditBox.Parent := Self;
EditBox.Color := clLime;
//EditBox.OnChange := ChangeEvent;
EditBox.OnKeyPress := KeyEvent;
IntegerPart := 0;
FloatPart   := 0;
FMax := 100;
FMin := 0.0;
FormString := '%5.2f';
FDigitsAfter := 2;
Divider := 100;
SetEditEnable(True);

MakeNumber;
end;

destructor TUpDownEdit.Destroy;
begin
  inherited Destroy;
end;

procedure TUpDownEdit.Resize;
Var
Size : Integer;
Done : Boolean;
begin
Inherited Resize;
Height := EditBox.Height+4;
with IntegerSpin do
     begin
     Height := EditBox.Height;
     Width  := 16;
     Left := 1;
     Top := 1;
     end;
with FloatSpin do
     begin
     Height :=  EditBox.Height;
     Width  := 16;
     Left := Self.Width-18;
     Top := 1;
     end;

with EditBox do
     begin
     Width := Self.Width -34;
     Left := 18;
     Top := 1;
     end;
end;

procedure TUpDownEdit.DoEnter;
begin
if FeditEnable then EditBox.Color := clYellow;
end;

procedure TUpDownEdit.DoExit;
var
Buffer    : Double;
begin
Buffer := FValue;
try
FValue := StrToFloat(EditBox.Text);
SetValue(FValue);
except
FValue := Buffer;
SetValue(FValue);
end;
EditBox.Color := clWhite;

inherited DoExit;
end;


procedure TUpDownEdit.SetMax(MaxVal : Double);
begin
FMax := MaxVal;
IntegerSpin.Max := Round(MaxVal);
end;

procedure TUpDownEdit.SetMin(MinVal : Double);
begin
FMin := MinVal;
IntegerSpin.Min := Round(MinVal);
end;

procedure TUpDownEdit.SetValue(Val : Double);
begin
FValue := Val;
IntegerSpin.Position := Trunc(Val);
IntegerPart := Trunc(Val);
FloatSpin.Position := Round((Val-IntegerSpin.Position)*Divider);
FloatPart := FloatSpin.Position;
MakeNumber;

end;

procedure TUpDownEdit.SetEditEnable(Enabled : Boolean);
begin
FEditEnable := Enabled;
EditBox.Enabled := Enabled;
IntegerSpin.Enabled := Enabled;
FloatSpin.Enabled := Enabled;
if Enabled then
   begin
   EditBox.Color := clWindow;
   EditBox.Font.Color := clBlack;
   EditBox.Font.Style := [];
   end
else
   begin
   EditBox.Color := clSilver;
   EditBox.Font.Color := clBlue;
   EditBox.Font.Style := [fsBold];

   end;

end;


procedure TUpDownEdit.SetDigitsAfter(Digits : Integer);
var
FieldLength : Integer;
Output      : String;
begin
if Digits < 1 then Digits := 1;
if Digits > 4 then Digits := 4;
FDigitsAfter := Digits;
FieldLength := Digits+4;
FormString := '%'+Format('%d.%d',[FieldLength,Digits])+'f';
case Digits of
     1: Divider := 10;
     2: Divider := 100;
     3: Divider := 1000;
     4: Divider := 10000;
     end;

end;

procedure TUpDownEdit.IntegerClick(Sender: TObject; Button: TUDBtnType);
begin
if Button = btNext then
   begin
   If FMax - FValue >= 1.0 then
      begin
      inc(IntegerPart);
      end;
   end
else
   begin
   If FValue-FMin >= 1.0 then
      begin
      IntegerPart := IntegerSpin.Position;
      end;
   end;


SelfEdit := True;
MakeNumber;


end;

procedure TUpDownEdit.FloatClick(Sender: TObject; Button: TUDBtnType);
begin
if Button = btNext then
   begin
   If FMax > FValue then
      begin
      FloatPart := FloatSpin.Position;
      if FloatPart >= Divider then
         begin
         FloatPart := 0;
         FloatSpin.Position := 0;
         inc(IntegerPart);
         IntegerSpin.Position := IntegerPart;
         end;
      end;
   end
else
   begin
   If FValue > FMin  then
      begin
      FloatPart := FloatSpin.Position;
      //dec(FloatPart);
      if FloatPart < 0 then
         begin
         FloatPart := Trunc(0.99999999*Divider);
         FloatSpin.Position := Trunc(0.999999*Divider);
         dec(IntegerPart);
         IntegerSpin.Position := IntegerPart;
         end;
      end;
   end;
SelfEdit := True;
MakeNumber;


end;

procedure TUpDownEdit.MakeNumber;
var
DisplayString : String;
begin
FValue := IntegerPart + FloatPart/Divider;
if FValue > FMax then FValue := Fmax;
DisplayString := Format(FormString,[FValue]);
EditBox.Text := DisplayString;
DoChange;
if SelfEdit then
   begin
   DoEditEvent;
   SelfEdit := False;
   end;
end;

procedure TUpDownEdit.SetOnEdit(Value: TNotifyEvent);
begin
FOnEdit := Value;
end;


procedure TUpDownEdit.SetOnChange(Value: TNotifyEvent);
begin
FOnChange := Value;
end;

procedure TUpDownEdit.DoChange;
begin
  if Assigned(FOnChange) then FOnChange(Self);
end;

procedure TUpDownEdit.DoEditEvent;
begin
     if Assigned(FOnEdit) then FOnEdit(Self);
end;

procedure TUpDownEdit.ChangeEvent(Sender: TObject);
begin
DoChange;
end;


procedure TUpDownEdit.KeyEvent(Sender: TObject; var Key: Char);
var
Buffer : Double;
begin
SelfEdit := True;
if Key = #13 then
   begin
   Buffer := FValue;
   try
   FValue := StrToFloat(EditBox.Text);
   SetValue(FValue);
   except
   FValue := Buffer;
   SetValue(FValue);
   end;

   end;
end;

function TUpDownEdit.GetHasFocus:Boolean;
begin
Result := IntegerSpin.Focused or FloatSpin.Focused or EditBox.Focused;
end;
end.
