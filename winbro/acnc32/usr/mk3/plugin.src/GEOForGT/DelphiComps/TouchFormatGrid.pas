unit TouchFormatGrid;
{ TODO : incremental Search by letter on first row }
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, TouchScroller, TouchStringGrid,Grids;

type
  TColFormat = record
      Formatted         : Boolean;
      FitToTitleWidth   : Boolean;
      FitToMaxString    : Boolean;
      MaxWidth          : Integer;
      NumberFormat       : String;
      FontModified      : Boolean;
      FontColour        : TColor;
      FontSize          : Integer;
      FontStyle         : String;
      end;

  pColFormat = ^TColFormat;

  TTouchFormatGrid = class(TTouchStringGrid)
  private
    { Private declarations }
    Ftest : Integer;
    ColFormatList : TList;
    FDefNumFormat: String;
    FDisplayUnformatted: Boolean;
    FOnSelectCell: TSelectCellEvent;
    procedure FormatDrawCell(Sender: TObject; ACol, ARow: Longint; Rect: TRect; State: TGridDrawState);
    procedure ClearAllFormatting;
    procedure FreeFormatList;
    procedure SetDefNumFormat(const Value: String);
    procedure FormatSetColCount(const Value: Integer);
    function FormatCellData(var InBuff: String; FormatRec: pColFormat): Boolean;
    procedure SetDisplayUnformatted(const Value: Boolean);

  protected
    { Protected declarations }
    procedure UpdateCurrentCell(Sender: TObject; ACol, ARow: Integer;var CanSelect: Boolean);override;

  public
    { Public declarations }
    procedure SetColFormat(Column : Integer; CFormat : pColFormat);
    function  GetColFormat(Column : Integer): pColFormat;
    procedure FormatAll;
    procedure FormatColumn(ColNumber: Integer);
  published
    { Published declarations }
    constructor Create(AOwner:TComponent); override;
    destructor Destroy; override;
    property DefaultColNumberFormat : String read FDefNumFormat write SetDefNumFormat;
    property ColCount write FormatSetColCount;
    property DisplayUnformatted : Boolean read FDisplayUnformatted write SetDisplayUnformatted;
    property OnSelectCell : TSelectCellEvent read FOnSelectCell write FOnSelectCell;
  end;

procedure Register;

implementation

function IsValidNumber(InString : String;var Num : Double) : Boolean;
begin
Result := True;
try
Num := StrToFloat(InString);
except
Result := False
end;
end;

{procedure CopyColFormat(Source: pColFormat;Dest : TColFormat);
begin
Dest.Formatted          := Source.Formatted;
Dest.FitToTitleWidth    := Source.FitToTitleWidth;
Dest.FitToMaxString     := Source.FitToMaxString;
Dest.MaxWidth           := Source.MaxWidth;
Dest.NumberFormat       := Source.NumberFormat;
Dest.FontModified       := Source.FontModified;
Dest.FontColour         := Source.FontColour;
Dest.FontSize           := Source.FontSize;
Dest.FontStyle          := Source.FontStyle;
end;}


procedure Register;
begin
  RegisterComponents('Touch', [TTouchFormatGrid]);
end;

{ TTouchFormatGrid }

procedure TTouchFormatGrid.ClearAllFormatting;
var
ColNumber : Integer;
FormatEntry :  pColFormat;

begin
FreeFormatList;
For ColNumber := Grid.FixedCols to Grid.ColCount do
  begin
  New(FormatEntry);
  FormatEntry.Formatted := False;
  FormatEntry.FontModified := False;
  FormatEntry.FontColour := Grid.Font.Color;
  FormatEntry.FontSize := Grid.Font.Size;
  FormatEntry.FontStyle := Grid.Font.Name;
  ColFormatList.Add(FormatEntry)
  end;
end;

constructor TTouchFormatGrid.Create(AOwner: TComponent);
begin
  inherited;
  ColFormatList := TList.Create;
  FDisplayUnformatted := False;
  ClearAllFormatting;
  with Grid do
        begin
        DefaultDrawing := False;
        OnDrawCell := FormatDrawCell;
        end;
end;

destructor TTouchFormatGrid.Destroy;
begin
  inherited;
  FreeFormatList;
end;


procedure TTouchFormatGrid.FormatDrawCell(Sender: TObject; ACol,
  ARow: Integer; Rect: TRect; State: TGridDrawState);
var
FormatString    : String;
IsFixed         : Boolean;
Fentry          : pColFormat;
DisplayBuffer   : String;
begin
IsFixed := False;
with Grid.Canvas do
begin
FEntry := ColFormatList[ACol];
if (ACol < Grid.FixedCols) or (ARow < Grid.FixedRows) then
        begin
        Brush.Color := Grid.FixedColor;
        IsFixed := True;
        end
else
        begin
        if ARow = SelectedRow then
                begin
                if ACol = SelectedColumn then
                    begin
                    Brush.Color := Clwhite - $00202030;
                    end
                else
                    begin
                    Brush.Color := Clwhite - $00101010;
                    end
                end
        else
                begin
                Brush.Color := Self.Color;
                end;


        if Fentry.FontModified then
                begin
                Grid.Canvas.Font.Size := Fentry.FontSize;
                Grid.Canvas.Font.Name := Fentry.FontStyle;
                Grid.Canvas.Font.Color := Fentry.FontColour;
                end
        else
                begin
                Grid.Canvas.Font.Assign(Grid.Font);
                end

        end;

FillRect(Rect);

if (IsFixed or FDisplayUnformatted) then
   begin
   Font.assign(Grid.Font);
   TextOut(Rect.Left+2,Rect.Top+2,Grid.Cells[Acol,Arow]);
   end
else
   begin
   if Acol < ColFormatList.Count then
        begin
        if Fentry.Formatted then
                FormatString := FEntry.NumberFormat
        else
                FormatString := DefaultColNumberFormat;
        end;
   if FormatString <> '' then
        begin
        DisplayBuffer := Grid.Cells[Acol,Arow];
        if FormatCellData(DisplayBuffer,Fentry) then
                begin
                TextOut(Rect.Left+2,Rect.Top+2,DisplayBuffer);
                end
        else
                begin
                TextOut(Rect.Left+2,Rect.Top+2,Grid.Cells[Acol,Arow]);
                end;
        end
   else
        begin
        TextOut(Rect.Left+2,Rect.Top+2,Grid.Cells[Acol,Arow]);
        end
   end
end; // with canvas
end;



procedure TTouchFormatGrid.FreeFormatList;
begin
while ColFormatList.Count > 0 do
   begin
   Dispose(pColFormat(ColFormatList[0]));
   ColFormatList.Delete(0);
   end;
end;

function TTouchFormatGrid.GetColFormat(Column: Integer): pColFormat;
var
ReturnListEntry : TColFormat;
ListEntry       : pColFormat;
begin
Result := ColFormatList[Column];
end;

procedure TTouchFormatGrid.FormatSetColCount(const Value: Integer);
var
ListEntry : pColFormat;
begin
TTouchStringGrid(SELF).ColCount := Value;
//exit;
if Grid.ColCount > ColFormatList.Count then
        begin
        while ColFormatList.Count < Grid.ColCount do
                begin
                New(ListEntry);
                ListEntry.Formatted := False;
                ListEntry.FontModified := False;
                ListEntry.FontColour := Grid.Font.Color;
                ListEntry.FontSize := Grid.Font.Size;
                ListEntry.FontStyle := Grid.Font.Name;;
                ColFormatList.Add(ListEntry);
                end;
        end;

if ColCount < ColFormatList.Count then
        begin
        while ColFormatList.Count > ColCount do
                begin
                Dispose(pColFormat(ColFormatList[ColFormatList.Count-1]));
                ColFormatList.Delete(ColFormatList.Count-1);
                end;
        end;
end;


procedure TTouchFormatGrid.SetColFormat(Column: Integer;
  CFormat: pColFormat);
var
FListEntry : pColFormat;
begin
if Column < ColFormatList.Count then
   begin
   FListEntry := ColFormatList[Column];
   FListEntry.Formatted := CFormat.Formatted;
   if FListEntry.Formatted then
        begin
        FListEntry.FitToTitleWidth := CFormat.FitToTitleWidth;
        FListEntry.FitToMaxString  := CFormat.FitToMaxString;
        FListEntry.MaxWidth        := CFormat.MaxWidth;
        FListEntry.NumberFormat    := CFormat.NumberFormat;
        end;
   FListEntry.FontModified := CFormat.FontModified;
   if FListEntry.FontModified then
        begin
        FListEntry.FontColour := CFormat.FontColour;
        FListEntry.FontSize  :=  CFormat.FontSize;
        FListEntry.FontStyle :=  CFormat.FontStyle;
        end;
   end;
end;

procedure TTouchFormatGrid.SetDefNumFormat(const Value: String);
var
EntryNumber : Integer;
FormatEntry : pColFormat;
begin
  FDefNumFormat := Value;
end;

function TTouchFormatGrid.FormatCellData(var InBuff : String;FormatRec : pColFormat): Boolean;
var
SNumber : Double;
Buffer  : String;
begin
Result := True;
Buffer := InBuff;
if IsValidNumber(Inbuff,SNumber) then
   begin
   if FormatRec.NumberFormat <> '' then
        begin
        try
        Buffer := Format(FormatRec.NumberFormat,[SNumber]);
        except
        Buffer := InBuff;
        Result := False;
        end;
        end
   else
        begin
        if DefaultColNumberFormat <> '' then
           begin
           try
           Buffer := Format(DefaultColNumberFormat,[SNumber]);
           except
           Buffer := InBuff;
           Result := False;
           end;
           end
        end;


   end;
InBuff := Buffer;
end;
procedure TTouchFormatGrid.FormatColumn(ColNumber : Integer);
var
Buffer     : String;
RowNumber  : Integer;
FormatData : pColFormat;
MaxWidth   : INteger;
SWidth     : Integer;
BufferExtent    : TSize;
DoFormatting    : Boolean;
TitleSize       : TSize;
begin
MaxWidth := 0;
if ColNumber < ColFormatList.Count then
    begin
    FormatData := ColFormatList[ColNumber];
    if (FormatData.FontModified) and (not DisplayUnformatted) then
        begin
        Grid.Canvas.Font.Size := FormatData.FontSize;
        Grid.Canvas.Font.Name := FormatData.FontStyle;
        end
    else
       begin
       Grid.Canvas.Font.Assign(Grid.Font);
       end

    end;
    DoFormatting := (FormatData.Formatted) or (FDefNumFormat <> '') and (not DisplayUnformatted);
    if DoFormatting then
       begin
        For RowNumber := Grid.FixedRows to Grid.RowCount-1 do
            begin
            Buffer := Grid.Cells[ColNumber,RowNumber];
            if Buffer <> '' then
                begin
                if FormatCellData(Buffer,FormatData) then
                    begin
                    BufferExtent := Grid.Canvas.TextExtent(Buffer);
                    if (BufferExtent.cx > MaxWidth) then MaxWidth := BufferExtent.cx;
                    end;
                end;
            end;
      end
    else
       begin
       if DisplayUnformatted then
           begin
           For RowNumber := Grid.FixedRows to Grid.RowCount-1 do
            begin
            Buffer := Grid.Cells[ColNumber,RowNumber];
            if Buffer <> '' then
                begin
                BufferExtent := Grid.Canvas.TextExtent(Buffer);
                if (BufferExtent.cx > MaxWidth) then MaxWidth := BufferExtent.cx;
                end;
            end;
           end;
       end;
// Finally check if column width formatting is in place and adjust if required
if  DoFormatting then
begin
if FormatData.FitToMaxString then
     begin
     if FormatData.FitToTitleWidth then
         begin
         TitleSize := Grid.Canvas.TextExtent(Grid.Cells[ColNumber,0]);
         if TitleSize.cx > Maxwidth then MaxWidth := TitleSize.cx;
         end;
     end
else
     begin
     if FormatData.FitToTitleWidth then
         begin
         TitleSize := Grid.Canvas.TextExtent(Grid.Cells[ColNumber,0]);
         MaxWidth := TitleSize.cx;
         end;
     end;
end;
Grid.ColWidths[ColNumber] := MaxWidth+10;
end;

procedure TTouchFormatGrid.FormatAll;
var
ColNumber : Integer;
begin
For ColNumber := Grid.FixedCols to Grid.ColCount-1 do
        begin
        FormatColumn(ColNumber);
        end;
end;

procedure TTouchFormatGrid.SetDisplayUnformatted(const Value: Boolean);
begin
  FDisplayUnformatted := Value;
  Grid.Invalidate;
end;


procedure TTouchFormatGrid.UpdateCurrentCell(Sender: TObject; ACol,
  ARow: Integer; var CanSelect: Boolean);
begin
  inherited;
  Grid.Invalidate;
  if Assigned(FOnSelectCell) then FOnSelectCell(Self,Acol,Arow,CanSelect);
end;

end.


