unit ACNC_NumEdit;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls;
type
TNumberKeys = set of Char;
TValidChars = set of Char;

type
  TNumEdit = class(TCustomEdit)
  private
    { Private declarations }
    FMaxValue     : Double;
    FMinValue     : Double;
    FAfterMax     : Integer;
    FBeforeMax    : Integer;
    FIntegerOnly  : Boolean;
    FCheckLimits  : Boolean;
    FEntryValid   : Boolean;
    FPosOnly      : Boolean;

    JustEntered   : Boolean;

    MaxDigits     : Integer;
    MEntryValue  : String;
    PointEntered: Boolean;

    function LimitCheck:Boolean;
    function IsValidNumber(InString : String) : Boolean;

    procedure KeyPress(var Key: Char); override;
    procedure Change; Override;
    procedure DoEnter; override;

    procedure SetIntOnly(IntOnly : Boolean);
    procedure SetAfterMax(Max : Integer);
    procedure SetBeforeMax(Max : Integer);


  protected
    { Protected declarations }
  public
    { Public declarations }

  published
    { Published declarations }
    constructor Create(AOwner:TComponent); override;
    destructor  Destroy; override;

    property MaxValue    : Double      read FMaxValue       write FMaxValue;
    property MinValue    : Double      read FMinValue       write FMinValue;
//    property FieldLength : Integer     read FFieldLength    write SetFieldLength;
    property BeforeMax   : Integer     read FBeforeMax      write SetBeforeMax;
    property AfterMax    : Integer     read FAfterMax       write SetAfterMax;
    property IntegerOnly : Boolean     read FIntegerOnly    write SetIntOnly;
    property PositiveOnly: Boolean     read FPOsOnly        write FposOnly;
    property CheckLimits : Boolean     read FCheckLimits    write FCheckLimits;
    property EntryValid  : Boolean     read FEntryValid     write FEntryValid;




    property AutoSize;
    property Color;
    property BorderStyle;
    Property Ctl3D;
    Property Font;
    property Height;
    property Width;
    property ParentFont;
    property ShowHint;
    property Hint;
    Property TabOrder;
    property TabStop;
    property ReadOnly;

    property OnKeyDown;
    property OnEnter;
    property OnExit;
    property OnClick;
    property OnDblClick;
    property OnKeyPress;
    property OnMouseDown;
    property OnMouseUp;
    property OnMouseMove;
    property OnChange;


  end;

procedure Register;

implementation

procedure Register;
begin
  RegisterComponents('ACNCComps', [TNumEdit]);
end;

constructor TNumEdit.Create(AOwner:TComponent);
begin
inherited Create(AOwner);
MEntryValue  := '';
PointEntered:= False;
FCheckLimits := False;
//FFieldLength := 10;


SetAfterMax(2);
SetBeforeMax(4);
SetIntOnly(False);
end;

destructor TNumEdit.Destroy;
begin
  inherited Destroy;

end;


procedure TNumEdit.KeyPress(var Key: Char);
var
NumberKeys : TNumberKeys;
Temp       : Word;
BufferP     : PChar;
Buffer      : array [1..100] of Char;
decPoint    : Pointer;
After       : Integer;
CPosition   : Integer;
PPosition   : Integer;
Count       : Integer;
NumCount    : Integer;
begin
inherited KeyPress(Key);
NumberKeys := ['1','2','3','4','5','6','7','8','9','0'];
BufferP := @Buffer;
Temp := Word(Key);
// Calculate position of Cursor to Cposition
Cposition := SelStart;
if JustEntered then
   begin
   //Save entry value if newly entered the component
   MentryValue := Text;
   JustEntered := False;
   end;
StrPCopy(BufferP,Text);
NumCount := 0;

// Count the number of numeric digits innthe string
For Count :=1 to Length(Text) do
    begin
    if Buffer[Count] in NumberKeys then inc(NumCount);
    end;
// Search to see if decimal point has already been entered
DecPoint := StrScan(BufferP,'.');
if assigned(DecPoint) then
   begin
   // Here if dec point IS entered
   PointEntered := True;
   // Calculte number of digits after decimalpoint
   After := StrLen(DecPoint);
   // Calculate position of actual decimal point
   PPosition := StrLen(BufferP)-After;
   if (After > FAfterMax) and (Cposition > PPosition) then
      begin
      // only allow + - and backspace if all digits after dec point are already entered
      if (temp <> 8) and (temp <> 43) and (temp <> 45)  then
         begin
         key := #0;
         exit;
         end;
      end;
   end
else
   begin
   // Here if dec point NOT entered
   PointEntered := False;
   // Check that nuber digits prior to dec point not too large
   if (NumCount >= FBeforeMax) and (key in NumberKeys) then
      begin
      key := #0;
      exit;
      end;
   end;

if (NumCount >= MaxDigits) then
   begin
   //Check Total Number digits entered ids not too great
   if (temp <> 8) and (temp <> 43) and (temp <> 45) then
      begin
      key := #0;
      exit;
      end;
   end;

if not(Key in NumberKeys) then
   begin
   // Non numeric key entered then   check for other valid keys
   case Temp of
        8,13,27: ;
        46: //decimal; point
            begin
            // block dec point if already exists or the number is forced to Integer
            if PointEntered or FIntegerOnly then
               begin
               key := #0;
               beep;
               end;
            end;

        43:    // PLus
            begin
            // Here if Plus Key Pressed
            if Length(Text) > 0 then
               begin
               case Text[1] of
                    '-':
                        begin
                        //  swap plus sign for minus sign
                        StrPCopy(BufferP,Text);
                        Buffer[1] := key;
                        Text := Buffer;
                        key := #0;
                        end;
                    '+':
                        begin
                        Key := #0; // block duplication
                        end;
                    else
                        begin
                        // add PLus to START of value;
                        Text:= key+Text;
                        key := #0;
                        end;
                    end; {inner case}
               end;
            end;

        45: // minus key pressed
            begin
            if (Length(Text) > 0) and (not FPosOnly) then
               begin
               case Text[1] of
                    '+':
                        begin
                        // swap plus for minus if plus already entered
                        StrPCopy(BufferP,Text);
                        Buffer[1] := key;
                        Text := Buffer;
                        key := #0;
                        end;
                    '-':
                        begin
                        Key := #0; // block Duplication of existing minus
                        end;
                    else
                        begin
                        // Otherwise Add Minus to front of Value
                        Text:= key+Text;
                        key := #0;
                        end;
                    end; {inner case}
               end;

            end;
        else
            begin
            key := #0;
            end;
        end;{ outer case}
     end
   else
       begin
       // here for number key entry
       if Length(Text) > 0 then
          begin
          if (CPosition = 0) and ((Text[1] = '-') or (Text[1] = '+')) then
             begin
             // here if try to put number before sign !!
             key := #0;
             beep;
             end;
          end;
       end;
JustEntered := False;
end;


procedure TNumEdit.DoEnter;
begin
inherited DoEnter;
Change;
JustEntered := True;
end;

procedure TNumEdit.Change;
begin
inherited Change;
if LimitCheck then
   begin
   FEntryValid := True;
   ParentFont := True;
//   Font.Color := clBlack;
   end
else
   begin
   FEntryValid := False;
   Font.Color := clRed;
   end;

end;


procedure TNumEdit.SetIntOnly(IntOnly : Boolean);
begin
FIntegerOnly := Intonly;

if Intonly then
   begin
   FAfterMax:= 0;
   MaxDigits := FBeforeMax;
   end;
end;


procedure TNumEdit.SetAfterMax(Max : Integer);
begin

if Max > 98 then exit;
{if (Max+FMaxBefore >=  FFieldLength then
   begin
   FFieldLength := Max+2;
   end;} //!!!!
if Max = 0 then FintegerOnly := True else FintegerOnly := False;
FAfterMax := Max;
end;

procedure TNumEdit.SetBeforeMax(Max : Integer);
begin
if Max > 100 then exit;
if (Max+FAfterMax) > 100 then FAfterMax := 98-Max;
if FintegerOnly then
   begin
   MaxDigits := Max; // allow for posible plus or minus sign
   end
else
   begin
   MaxDigits := Max+FAfterMax; // allow for posible plus or minus sign
   end;
FBeforeMax := Max;
end;


function TNumEdit.LimitCheck:Boolean;
var
value : Double;
begin

If Not FCheckLimits then
   begin
   Result := True;
   exit;
   end;

if IsValidNumber(Text) then
   begin
   Value := StrToFloat(Text);
   if (Value > FMaxValue) Or (Value < FMinValue) then
      begin
      Result := False;
      end
   else
      begin
      Result := True;;
      end;
   end
else
    begin
    Result := False;
    end;
end;

function TNumEdit.IsValidNumber(InString : String) : Boolean;
var
ValidChars : TValidChars;
Numberkeys : TNumberKeys;
Count,Len  : Integer;
begin
Len := Length(InString);
Result := False;
ValidChars := ['1','2','3','4','5','6','7','8','9','0','.','+','-'];
NumberKeys := ['1','2','3','4','5','6','7','8','9','0','.'];


if Len > 0 then
   begin
   if not (InString[1] in ValidChars) then exit;
   if Len > 1 then
      begin
      For Count := 2 to Len do
           begin
           if not (InString[Count] in NumberKeys) then
              begin
              exit;
              end;
           end;
      Result := True;
      end
   else
       begin
       if (InString[1] = '-') or (InString[1] = '+') or (InString[1] = '.') then
          begin
          Result := False;
          end
       else
          begin
          Result := True;
          end;

       end;

   end
else
    begin
    Result := False;
    end;


end;


end.
