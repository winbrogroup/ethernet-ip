unit TouchScroller;
interface
uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls,extctrls;

type

  TVerticalFramework = record
        SmearLength : Integer;
        TopRect     : TRect;
        BotRect     : TRect;
        BodyRect    : TRect;
        TopHighRect : TRect;
        TopCentre   : TPoint;
        BottomCentre: TPoint;
        end;

  THorizontalFramework = record
        SmearLength : Integer;
        LeftRect     : TRect;
        RightRect     : TRect;
        BodyRect    : TRect;
        HIghRect    : TRect;
        LeftHighRect : TRect;
        LeftCentre   : TPoint;
        RightCentre: TPoint;
        end;

  TVSliderData = record
        PixelHeight     : Integer ;
        PixelWidth      : Integer ;
        PixelBallRadius : Integer ;
        PixelBodyLength : Integer ;
        PixelMin        : Integer ; // where it is
        PixelMax        : Integer ; // where it is
        PixelMid        : Integer ; // where it is
        PixelsPerUnit   : Double  ;
        PageRatio       : Double  ; // PageSize/Range
        PixelMaxCentre  : Integer ; // where it can go
        PixelMinCentre  : Integer ; // where it can go
        UnitsMaxPos     : Integer ; // where it can go
        ButtonTouched   : Boolean ;
        ButtonTouchOffset : Integer;
        end;

  TTouchScrollStyles = (tsHorizontal,tsVertical);
  TTouchScroller = class(TCustomPanel)
  private
    { Private declarations }
    ForcePosUpdate      : Boolean;
    DrawCount           : Integer;
    SliderData          : TVSliderData;
    VFrameData          : TVerticalFramework;
    HFrameData          : THorizontalFramework;

    FMax,Fmin           : Integer;
    FPosition           : Integer;
    FPageSize           : Integer;
    SliderArea          : TPaintBox;
    Offscreen           : TBitmap;

    DragRect            : Trect;
    FStyle              : TTouchScrollStyles;
    LastPixelPosition   : Integer;
    LastPosition        : Integer;
    FPositionChanged    : TNotifyEvent;
    FPositionChangedByTouch : TNotifyEvent;
    FTestMode           : Integer;
    FButtonColor        : TColor;
    FSlideColor: TColor;
    FAutoPageSize: Boolean;


    procedure DrawVerticalStretchButton(SlideData :TVSliderData;MyCanvas :TCanvas);
    procedure DrawHorizontalStretchButton(SlideData :TVSliderData;MyCanvas :TCanvas);
    procedure CalculateSliderMetricsFromPosition(RequestedPosition : Integer);

    procedure PaintSliderArea(Sender : Tobject);
    procedure CalculatePositionFromMouse(MPosX, MposY : Integer);
    procedure ScrollerMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure ScrollerMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure CalculateVerticalRectangles(SliderData :TVSliderData;var FrameworkData : TVerticalFramework);
    procedure CalculateHorizontalRectangles(SliderData :TVSliderData;var FrameworkData : THorizontalFramework);

    procedure ScrollerMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
    procedure SetStyle(const Value: TTouchScrollStyles);
    function GetMax: Integer;
    function GetMin: Integer;
    procedure SetMax(const Value: Integer);
    procedure SetMin(const Value: Integer);
    function GetPageSize: Integer;
    procedure SetPageSize(const Value: Integer);
    function GetPosition: Integer;
    procedure SetPOsition(const Value: Integer);
    procedure InitSliderMetrics;
    procedure SetSliderMetrics;
    procedure UpdatePositionData;
    procedure SetTestMode(const Value: Integer);
    procedure RedrawOnExternalPositionChange;
    procedure SetButtonColor(const Value: TColor);
    procedure SetSlideColor(const Value: TColor);

  protected
    { Protected declarations }
    Procedure Resize; override;
    Procedure Loaded; override;
  public
    { Public declarations }
  procedure SetParameters(POsition,PageSize,Max,Min : Integer);
  published
    { Published declarations }
    constructor Create(AOwner : TComponent); override;
    destructor Destroy; override;
    property Max : Integer read GetMax write SetMax;
    property Min : Integer read GetMin write SetMin;
    property PageSize : Integer read GetPageSize write SetPageSize;
    property Position : Integer read GetPosition write SetPOsition;

    property Style :TTouchScrollStyles read FStyle write SetStyle;
    property OnPositionChanged : TNotifyEvent read FpositionChanged write FPositionChanged;
    property PositionChangedByTouch : TNotifyEvent read  FPositionChangedByTouch write FPositionChangedByTouch;
    property TestMode : Integer read FTestMode write SetTestMode;
    property ButtonColor : TColor read FButtonColor write SetButtonColor;
    property SlideColor : TColor read FSlideColor write SetSlideColor;
    property AutoPageSize : Boolean read FAutoPageSize write FAutoPageSize;
  end;

//---------------------------------------------------//
//--------- TTouchScrollContainer -------------------//
//---------------------------------------------------//
  TTouchScrollContainer = class(TCustomPanel)
  private
    { Private declarations }
    procedure VPositionChanged(Sender : Tobject);
    procedure HPositionChanged(Sender : Tobject);
  protected
    { Protected declarations }
    procedure HPosModified(var Position : Integer);virtual;
    procedure VPosModified(var Position : Integer);virtual;

  public
    { Public declarations }
    vsb : TTouchScroller;
    hsb : TTouchScroller;
  published
    { Published declarations }
    constructor Create(AOwner : TComponent); override;
    destructor Destroy; override;
    procedure Resize; override;
    procedure SetVerticalScrollerVisible(VerticalVis : Boolean);
    procedure SetHorizontalScrollerVisible(HorizVis : Boolean);
    property align;
  end;


procedure Register;

implementation

procedure Register;
begin
  RegisterComponents('Touch', [TTouchScroller]);
  RegisterComponents('Touch', [TTouchScrollContainer]);

end;



{ TTouchScroller }

constructor TTouchScroller.Create(AOwner: TComponent);
begin
inherited;
    FButtonColor := clBlue;
    FSlideColor  := clSilver;
    BevelInner := bvLowered;
    BevelOuter := bvNone;
    Height := 50;
    Width := 200;
    FStyle := tsHorizontal;
    FPosition := 50;
    FMax := 100;
    FMin := 1;
    FPageSize := 10;

   SliderArea := TPaintBox.Create(Self);

   with SliderArea do
        begin
        Parent := Self;
        Visible := True;
        OnMouseMove := ScrollerMouseMove;
        OnMouseDown := ScrollerMouseDown;
        OnMouseUp := ScrollerMouseUp;
        OnPaint := PaintSliderArea;
       end;


   Offscreen := TBitmap.Create;
   Offscreen.Transparent := True;
   Offscreen.TransparentColor := clBlack;
   SliderData.ButtonTouched := False;
end;

destructor TTouchScroller.Destroy;
begin
  inherited;
  Offscreen.Free;
end;





procedure TTouchScroller.CalculateHorizontalRectangles (SliderData : TVSliderData;
                                                      var FrameworkData : THorizontalFramework);
var
X,Y : Integer;
begin
with FrameworkData do
begin
Y := SliderArea.Height div 2;
X := SliderData.PixelMid;

SmearLength := SliderData.PixelBodyLength;

With LeftRect do
    begin
    Top         := 1;
    Bottom      := Self.Height;
    Right := X - (SmearLength div 2) + SliderData.PixelBallRadius ;
    Left := Right - (SliderData.PixelBallRadius*2);
    end;

LeftCentre.x := LeftRect.Left + SliderData.PixelBallRadius;   //!!!
LeftCentre.y := Height div 2;

With RightRect do
     begin
     Top := 1;
     Bottom      := Self.Height;
     Left := X + (SmearLength div 2) - SliderData.PixelBallRadius;
     Right := Left + (SliderData.PixelBallRadius*2);
     end;

With LeftHighRect do
    begin
    Top         := Height div 4;
    Bottom      := Top + (Height div 2);
    Left := LeftRect.Left+(SliderData.PixelBallRadius div 2);
    Right := Left + (SliderData.PixelBallRadius);
    end;


With BodyRect do
      begin
      Top := 1;
      Bottom := Self.Height;
      Left := X- (SmearLength div 2);
      Right := X + (SmearLength div 2);
      end;

RightCentre.x := RightRect.Left;
RightCentre.y := RightRect.Left + SliderData.PixelBallRadius;
end; // with framework data

end;

procedure TTouchScroller.CalculateVerticalRectangles ({X,Y : Integer;}SliderData : TVSliderData;
                                                      var FrameworkData : TVerticalFramework);

var
X,Y : Integer;
begin
X:= SliderArea.Width div 2;
Y := SliderData.PixelMid;

//if not isloaded then exit;
with FrameworkData do
   begin
   SmearLength := SliderData.PixelBodyLength;
   With TopRect do
    begin
    Top := Y-(SliderData.PixelBallRadius+(SmearLength div 2));
    Bottom := Top + (SliderData.PixelBallRadius*2);
    Left := 1;
    Right := Self.Width;
    end;

TopCentre.x := Width div 2;
TopCentre.y := Y- (SmearLength div 2);

With TopHighRect do
    begin
    Top         := TopRect.Top + (SliderData.PixelBallRadius div 2);
    Bottom      := TopCentre.y + (SliderData.PixelBallRadius div 2);
    Left        := Self.Width div 4;
    Right       := Self.Width ;
    end;

With BotRect do
     begin
     Top := Y+(SmearLength div 2)-SliderData.PixelBallRadius;
     Bottom :=Top + (SliderData.PixelBallRadius*2);
     Left := 1;
     Right := TopRect.Right;
     end;


BottomCentre.x := X;
BottomCentre.y := BotRect.Top+SliderData.PixelBallRadius;

With BodyRect do
      begin
      Top := TopCentre.y;
      Bottom := BottomCentre.y;
      Left := 1;
      Right := Self.Width;
      end;
end; // with FrameworkData
end;

procedure TTouchScroller.DrawHorizontalStretchButton(SlideData :TVSliderData; MyCanvas: TCanvas);
var
ColorDark : TColor;
LeftCentre,RightCentre : TPoint;
HighWidth : Integer;
begin

CalculateHorizontalRectangles(SliderData,HFrameData);

with MyCanvas do
     begin
     with HFrameData do
     begin
     if (FTestMode = 1) then
        begin
        Brush.Color := FButtonColor;
        FillRect(LeftRect);
        Brush.Color := clBlue;
        FillRect(RightRect);
        Brush.Color := clWhite;
        FillRect(LeftHighRect);
        exit;
        end;
     Brush.Color := FButtonColor;
     Pen.Color   := FButtonColor;
     Pen.Width := 1;

     with LeftRect do
          Ellipse(Right,Bottom,Left,Top);

     with RightRect do
        Ellipse(Right,Bottom,Left,Top);

     FillRect(BodyRect);

     Pen.Color := clWhite;
     Pen.width := 2;
     MoveTo(BodyRect.Left,Height div 4);
     LineTo(BodyRect.Right,Height div 4);

     with LeftHighRect do
        Arc(Right,Bottom,Left,Top,LeftCentre.x,Top-10,LeftRect.Left,LeftCentre.y);
     end; // with FrameData
     if (FTestMode=3) then TextOut(5,5,Format('%d,%d',[FPageSize,FPosition]));
     if (FTestMode=3) then TextOut(5,25,Format('%d,%d',[FMax,FMin]));
     end; // with MyCanvas;}
end;


procedure TTouchScroller.DrawVerticalStretchButton(SlideData :TVSliderData;
                                                   MyCanvas :TCanvas);
var
HighWidth : Integer;
begin
inc(DrawCount);
CalculateVerticalRectangles(SlideData,VFrameData);
with MyCanvas do
   begin
   with VFrameData do begin
   if (FTestMode=1) then
        begin
        Brush.Color := FButtonColor;
        FillRect(TopRect);
        Brush.Color := clBlue;
        FillRect(BotRect);
        Brush.Color := clYellow;
        FillRect(BodyRect);
        exit;
        end;
   // First paint Background;
   Brush.Color := FSlideColor;
   FillRect(TopRect);
   FillRect(BodyRect);
   FillRect(BotRect);
   Pen.Color := FButtonColor;
   Pen.Width := 1;
   Brush.Color := FButtonColor;
   //Now draw slider
   with TopRect do
          Ellipse(Right,Bottom,Left,Top);

   with BotRect do
        Ellipse(Right,Bottom,Left,Top);

   FillRect(BodyRect);
   Pen.Color := clWhite;
   Pen.width := 2;
   MoveTo(BodyRect.Left+(Width div 4),TopCentre.y);
   LineTo(BodyRect.Left+(Width div 4),BottomCentre.y);

   with TopHighRect do
        Arc(Right,Bottom,Left,Top,TopCentre.x,Top-10,TopRect.Left,TopCentre.y);
   end; // with FrameData
   if (FTestMode=3) then TextOut(5,5,Format('%d,%d',[FPageSize,FPosition]));
   if (FTestMode=3) then TextOut(5,25,Format('%d,%d',[FMax,FMin]));
   end; // with my Canvas
end;



procedure TTouchScroller.Loaded;
begin
  inherited;
  Resize;
  SetPageSize(FPageSize);
end;

procedure TTouchScroller.PaintSliderArea(Sender: Tobject);
begin
DragRect.Top := 0;
DragRect.Left := 0;
// Note DargButton is the entire swept area of the drag dot
DragRect.Bottom := SliderArea.Height+1;
DragRect.Right := SliderArea.Width+1;

with Offscreen.Canvas do
        begin
        Pen.Color := FSlideColor;
        Brush.Color :=FSlideColor;
        Rectangle(DragRect);
        Pen.Color := clwhite;
        Pen.Width := 2;

        case FStyle of
             tsHorizontal:
             begin
             MoveTo(10,SliderArea.Height div 2);
             LineTo(SliderArea.Width-10,SliderArea.Height div 2);
             Pen.Color := clGray;
             MoveTo(10,1+SliderArea.Height div 2);
             LineTo(SliderArea.Width-10,1+SliderArea.Height div 2);
             end;
        tsVertical:
             begin
             MoveTo(SliderArea.Width div 2,10);
             LineTo(SliderArea.Width div 2,SliderArea.Height-10);
             Pen.Color := clGray;
             MoveTo(1+SliderArea.Width div 2,10);
             LineTo(1+SliderArea.Width div 2,SliderArea.Height-10);
             end;
        end; //case
        end;
case FStyle of
  tsHorizontal:
  begin
  DrawHorizontalStretchButton(SliderData,Offscreen.Canvas);
  end;
  tsVertical:
  begin
  DrawVerticalStretchButton(SliderData,Offscreen.Canvas);
  end;
end;
SliderArea.Canvas.CopyRect(DragRect,Offscreen.Canvas,DragRect);
end;

procedure TTouchScroller.Resize;
begin
  inherited;
  case FStyle of
       tsVertical:
       begin
       with SliderArea do
            begin
            // Note Drag button is a paintbox of the entire drag area
            align := alRight;
            Width := Self.Width;
            end;
       end;

       tsHorizontal:
       begin
       with SliderArea do
            begin
            align := alBottom;
            Height := Self.Height;
            end;
       end;
  end; // case

  with Offscreen do
       begin
       Width := SliderArea.Width;
       Height := SliderArea.Height;
       end;

SetPageSize(FPageSize);
end;


procedure TTouchScroller.CalculatePositionFromMouse(MPosX,MPosY : Integer);
var
PixelPos           : Integer;
PosRange           : Integer;
MoveRatio          : Double;
UnitRange          : Integer;
begin
with SliderData do
begin
case FStyle of
  tsHorizontal:
  begin
  PosRange := SliderArea.Width - (PixelBallRadius*2);
  PIxelMid := MPosX + ButtonTouchOffset;
  if (PIxelMid >  SliderData.PixelMaxCentre) then
        PIxelMid := SliderData.PixelMaxCentre;
  if (PIxelMid <  SliderData.PixelMInCentre) then
        PIxelMid := SliderData.PixelMinCentre;
  end;

  tsVertical:
  begin
  PosRange := SliderArea.Height - (PixelBallRadius*2);
  PIxelMid := MPosY + ButtonTouchOffset;
  if (PIxelMid >  SliderData.PixelMaxCentre) then
        PIxelMid := SliderData.PixelMaxCentre;
  if (PIxelMid <  SliderData.PixelMInCentre) then
        PIxelMid := SliderData.PixelMinCentre;
  end;
// Now Determine Output Position Data in Units
end;
if PosRange < 1 then PosRange := 1;
PixelPos := (PixelMid - (PixelBodyLength div 2))-(PixelBallRadius);
MoveRatio := PixelPos/PosRange;
UnitRange := Max-Min+1;
if PixelMid = PixelMaxCentre then
        begin
        FPosition := FMax-FpageSize+1;
        end
else
        begin
        if PixelMid = PixelMinCentre then
                begin
                FPosition := FMin;
                end
        else
                begin
                FPOsition := Min + Round(UnitRange*MoveRatio);
                end;
        end;
end; // with slider data

end;

procedure TTouchScroller.ScrollerMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
var
NewPosition : Integer;
begin
if SliderData.ButtonTouched then
        begin
        CalculatePositionFromMouse(x,y);
        If (SliderData.PixelMid <> LastPixelPOsition) or (LastPosition <> FPosition) then
            begin
            PaintSliderArea(Self);
            if assigned(FPositionChanged) then FPOsitionChanged(Self);
            if assigned(FPositionChangedByTouch) then FPositionChangedByTouch(Self);
            UpdatePositionData;
            end;
        LastPixelPosition := SliderData.PixelMid;
        LastPosition := FPOsition;
        end;
end;

procedure TTouchScroller.ScrollerMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
TouchPos : Integer;
begin
case FStyle of
     tsHorizontal:
     begin
     SliderData.ButtonTouched := (x > SliderData.PixelMin) and (x<SliderData.PixelMax);
     end;

     tsVertical:
     begin
     SliderData.ButtonTouched := (y > SliderData.PixelMin) and (y<SliderData.PixelMax);
     end
end; //case

if SliderData.ButtonTouched then
   begin
   case FStyle of
        tsHorizontal:
        begin
        SliderData.ButtonTouchOffset := SliderData.PixelMid - X;
        end;

        tsVertical:
        begin
        SliderData.ButtonTouchOffset := SliderData.PixelMid - Y;
        end;
        end; // case
end
end;

procedure TTouchScroller.ScrollerMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
SliderData.ButtonTouched := False;
end;

procedure TTouchScroller.SetStyle(const Value: TTouchScrollStyles);
begin
  FStyle := Value;
  InitSliderMetrics;
  SetPageSize(FPageSize);
  Resize;
end;


function TTouchScroller.GetMax: Integer;
begin
Result := FMax;
end;

function TTouchScroller.GetMin: Integer;
begin
Result := FMin;
end;

procedure TTouchScroller.SetMax(const Value: Integer);
begin
FMax := Value;
Resize;
end;

procedure TTouchScroller.SetMin(const Value: Integer);
begin
FMin := Value;
end;


function TTouchScroller.GetPageSize: Integer;
begin
Result := FPageSize;
end;

procedure TTouchScroller.SetPageSize(const Value: Integer);
var
PageRange : Integer;
BodyRatio : Double;
begin
PageRange := Max-Min;
if Value > PageRange then
        begin
        FPageSize := PageRange;
        end
else
        begin
        if FPageSize < 1 then
                begin
                FPageSize := 1;
                end
        else
                begin
                FPageSize := Value;
                end;
        end  ;
SetSliderMetrics;
ForcePosUpdate := True;
SetPosition(Fposition);
Invalidate;
end;

// This only calculates Slder Size Not Button Position
procedure TTouchScroller.InitSliderMetrics;
begin
  with SliderData do
        begin
        PixelBodyLength := 0;
        PixelMin        := 0;
        if FStyle = tsVertical then
                begin
                PixelHeight     := SliderArea.Height;
                PixelWidth      := SliderArea.Width;
                PixelBallRadius := SliderArea.Width div 2;
                PixelsPerUnit   := (Max-Min)/SliderArea.Height;
                PageRatio       := 0;
                PixelMaxCentre  := SliderArea.Height - PixelBallRadius ; // where it can go
                PixelMinCentre  := PixelBallRadius ; // where it can go
                end
        else
                begin
                PixelHeight     := SliderArea.Width;
                PixelWidth      := SliderArea.Height;
                PixelBallRadius := SliderArea.Height div 2;
                PixelsPerUnit   := (Max-Min)/SliderArea.Width;
                PageRatio       := 0;
                PixelMaxCentre  := SliderArea.Width - PixelBallRadius ; // where it can go
                PixelMinCentre  := PixelBallRadius ; // where it can go
                end;
        PixelMid        := PixelBallRadius;
        PixelMax        := PixelBallRadius*2;
        end;
end;

procedure TTouchScroller.UpdatePositionData;
begin
  with SliderData do
    begin
        PixelMin        := PixelMid -  (PixelHeight div 2);
        PixelMax        := PixelMid +  (PixelHeight div 2);
    end ; // with sliderdata

end;

procedure TTouchScroller.SetSliderMetrics;
var
MoveableStroke   : Integer;
URange           : Integer;
SliderLength     : Integer;
MaxPageSize      : Integer;
begin

URange := Max-Min+1;
MaxPageSize := Max-Min;
 if URange < 1 then URange := 1;
with SliderData do
begin
case FStyle of

  tsHorizontal:
  begin
  PixelBallRadius := (SliderArea.Height div 2);
  MoveableStroke  := SliderArea.Width-(2*PixelBallRadius);
  PageRatio := FPageSize / URange;
  PixelBodyLength := Round(PageRatio * MoveableStroke);
  PixelHeight := (PixelBallRadius*2) + PixelBodyLength;
  PixelMaxCentre := SliderArea.Width - (PixelHeight div 2);
  PixelMinCentre := (PixelHeight div 2);
  UnitsMaxPos    := FMax - FPageSize;
  end;

  tsVertical:
  begin
    PixelBallRadius := (SliderArea.Width div 2);
    MoveableStroke  := SliderArea.Height-(2*PixelBallRadius);
    PageRatio := FPageSize / URange;
    PixelBodyLength := Round(PageRatio * MoveableStroke);
    SliderLength := PixelBodyLength+(2*PixelBallRadius);
    PixelHeight := (PixelBallRadius*2) + PixelBodyLength;
    PixelMaxCentre := SliderArea.Height - (PixelHeight div 2);
    PixelMinCentre := (PixelHeight div 2);
    UnitsMaxPos    := FMax - FPageSize;
  end;
  end; //case
end; // with SliderData
end;

function TTouchScroller.GetPosition: Integer;
begin
Result := FPosition;
end;

procedure TTouchScroller.SetPosition(const Value: Integer);
var
StartPos : Integer;
begin
StartPos := FPOsition;
If Value > FMax then exit;
if Value < FMin then exit;
FPosition := Value;
if (FPOsition <> StartPOs) or ForcePosUpdate then
   begin
   RedrawOnExternalPositionChange;
   if assigned(FPositionChanged) then FPOsitionChanged(Self);
   UpdatePositionData;
   ForcePosUpdate := False;
   end;
end;

procedure TTouchScroller.RedrawOnExternalPositionChange;
begin
CalculateSliderMetricsFromPosition(Fposition);
end;

procedure TTouchScroller.SetTestMode(const Value: Integer);
begin
  FTestMode := Value;
  Resize;
end;


procedure TTouchScroller.CalculateSliderMetricsFromPosition(RequestedPosition : Integer);
var
PixelPos           : Integer;
PixelPosRange      : Integer;
UnitsPosRange      : Integer;
PosRange           : Integer;
UnitsPosRatio      : Double;
UnitRange          : Integer;
PixelPosTopCentre  : Integer;
PixelPosMid        : Integer;
begin
UnitsPosRange := (FMax-Fmin)+1;
UnitsPosRatio := (FPosition-FMin)/UnitsPosRange;
case FStyle of
   tsHorizontal:
   begin
   PixelPosRange := SliderArea.Width - (SliderData.PixelBallRadius*2);
   PixelPosTopCentre := SliderData.PixelBallRadius+ Round(PixelPosRange* UnitsPosRatio);
   PixelPosMid := PixelPosTopCentre + (SliderData.PixelBodyLength div 2);
   with SliderData do
     begin
     PixelMid := PixelPosTopCentre + (SliderData.PixelBodyLength div 2);
     if (PIxelMid >  SliderData.PixelMaxCentre) then
           PIxelMid := SliderData.PixelMaxCentre;
     if (PIxelMid <  SliderData.PixelMInCentre) then
           PIxelMid := SliderData.PixelMinCentre;
     end; // with slider data
   end;

   tsVertical:
   begin
   PixelPosRange := SliderArea.Height - (SliderData.PixelBallRadius*2);
   PixelPosTopCentre := SliderData.PixelBallRadius+ Round(PixelPosRange* UnitsPosRatio);
   PixelPosMid := PixelPosTopCentre + (SliderData.PixelBodyLength div 2);
   with SliderData do
     begin
     PixelMid := PixelPosTopCentre + (SliderData.PixelBodyLength div 2);
     if (PIxelMid >  SliderData.PixelMaxCentre) then
           PIxelMid := SliderData.PixelMaxCentre;
     if (PIxelMid <  SliderData.PixelMInCentre) then
           PIxelMid := SliderData.PixelMinCentre;
     end; // with slider data
   end;
end; // case
end;

procedure TTouchScroller.SetButtonColor(const Value: TColor);
begin
  FButtonColor := Value;
  InValidate;
end;

procedure TTouchScroller.SetSlideColor(const Value: TColor);
begin
  FSlideColor := Value;
  InValidate;
end;




procedure TTouchScroller.SetParameters(POsition, PageSize, Max,
  Min: Integer);
begin
FPosition := Position;
FPageSize := PageSize;
FMax := Max;
FMin := Min;
Resize;
end;

{ TTouchScrollContainer }

constructor TTouchScrollContainer.Create(AOwner: TComponent);
begin
  inherited;
  vsb := TTouchScroller.Create(Self);
  with vsb do
        begin
        Visible := False;
        Parent := Self;
        Style := tsVertical;
        Width := 25;
        ButtonColor := clBlue;
        PositionChangedByTouch := VPositionChanged;
        end;
  hsb := TTouchScroller.Create(Self);
  with Hsb do
        begin
        Visible := False;
        Parent := Self;
        Height := 25;
        Style := tsHorizontal;
        ButtonColor := clBlue;
        PositionChangedByTouch := HPositionChanged;
        end;

end;

destructor TTouchScrollContainer.Destroy;
begin
  inherited;

end;


procedure TTouchScrollContainer.HPositionChanged(Sender: Tobject);
var
Position : Integer;
begin
Position := HSB.Position;
HPosModified(Position);
end;



procedure TTouchScrollContainer.Resize;
begin
  inherited;
  if hsb.Visible then
        begin
        with hsb do
            begin
            if vsb.Visible then
                Width := SElf.Width-vsb.Width-2
            else
                Width := SElf.Width-2;
            Top := Self.Height- Height;
            Left := 1;
            end;
        end;
  if vsb.Visible then
        begin
        with vsb do
            begin
            if hsb.Visible then
                Height  := SElf.Height-hsb.Height-2
            else
                Height  := SElf.Height-2;
            Top := 1;
            Left := Self.Width-vsb.Width;
            end;
        end;
end;


procedure TTouchScrollContainer.SetHorizontalScrollerVisible(
  HorizVis: Boolean);
begin
if  HorizVis then
        begin
        if VSb.Visible then HSB.Width := Self.Width - VSB.Width;
        end;
if HSB.Visible <> HorizVis then
        begin
        HSB.Visible := HorizVis;
        end;

end;

procedure TTouchScrollContainer.SetVerticalScrollerVisible(
  VerticalVis: Boolean);
begin
if  VerticalVis then
        begin
        if HSb.Visible then VSB.Height := Self.Height - HSB.Height;
        end;
if VSB.Visible <> VerticalVis then
        begin
        VSB.Visible := VerticalVis;
        end;
end;


procedure TTouchScrollContainer.VPositionChanged(Sender: Tobject);
var
Position : Integer;
begin
Position := VSB.Position;
VPosModified(Position);

end;
// VIRTUAL METHODS FOR 
procedure TTouchScrollContainer.VPosModified(var Position: Integer);
begin
end;
procedure TTouchScrollContainer.HPosModified(var Position: Integer);
begin
end;


end.
