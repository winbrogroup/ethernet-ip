unit G3DPaper;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls,G3DDefs,comctrls,menus;

type

  TG3DPaper = class(TCustomPanel)
  private
    { Private declarations }
    FScale    : Double; // Pixels per unit
    FUnits    : TUnits;
    FPerspective : Double;
    FRotation : TRotation; // World Rotation
    FTranslation :TScreenLocation;
    FCOR      : TPLocation; // World Centre of Rotation
    FFloorWidth : Double;
    FFloorLength : Double;
    FFloorTileWidth : Double;
    FFloorTileLength : Double;
    FShowFloor : Boolean;
    FFloorColour : TColor;
    Surface : TPaintBox;
    BackGround : TPanel;
    FOffscreen :TBitmap;
    FCentreIsZero : Boolean;
    FXCentre       : Integer;
    FZCentre       : Integer;
    ClearRect      : TRect;
    FloorList      : TList;
//    FViewShift     : TScreenLocation;

    procedure SetUnits (newUnits : TUnits);
    procedure SetScale (NValue : Double);
    procedure SetPerspective (NValue : Double);
    procedure SetFloorWidth (NValue : Double);
    procedure SetFloorTileWidth (NValue : Double);
    procedure SetFloorLength (NValue : Double);
    procedure SetFloorTileLength (NValue : Double);
    procedure SetFloorColour(nValue : TColor);
    procedure SetShowFloor(NValue : Boolean);
    procedure SetWorldRotation(NValue : Trotation);
    procedure SetWorldTranslation(NValue : TScreenLocation);

    procedure CreateFloor(FLength,FWidth,TileWidth,TileLength:Double);
    procedure PaintObjects(Sender : TObject);

  protected
    { Protected declarations }
    procedure Resize; override;
  public
    { Public declarations }
  published
    { Published declarations }
    constructor Create(AOwner: TComponent);override;
    destructor Destroy; override;

    Property Scale   : Double read FScale   write SetScale;
    Property Units   : TUnits  read FUnits   write SetUnits;
    Property Perspective : Double read FPerspective write SetPerspective;
    Property Rotation : TRotation read FRotation write SetWorldRotation;
    Property Translation :TScreenLocation read FTranslation write SetWorldTranslation;
    Property COR      : TPlocation read FCOR;
    Property Offscreen : TBitmap read FOffscreen write Foffscreen;
    Property CentreIsZero : Boolean read FCentreIsZero write FCentreIsZero;
    Property XCentre : Integer read FXCentre write FXCentre;
    Property ZCentre : Integer read FZCentre write FZCentre;
    Property FloorWidth : Double read FFloorWidth write SetFloorWidth;
    Property FloorLength : Double read FFloorLength write SetFloorLength;
    Property FloorTileWidth : Double read FFloorTileWidth write SetFloorTileWidth;
    Property FloorTileLength : Double read FFloorTileLength write SetFloorTileLength;
    Property FloorColour     : TColor read FFloorColour write SetFloorColour;
    Property ShowFloor : Boolean read FShowFloor write SetShowFloor;
//    Property ViewShift : TScreenLocation read FViewShift write FViewShift;
    procedure DrawFloor;
    procedure CleanSheet(Color : Tcolor);
    procedure AddBackGround(Color : Tcolor);
    procedure Paint; override;

  end;





procedure Register;

implementation

uses G3DLine,G3DBox,G3DDisc,G3DObjects,GMath3D;


procedure TG3DPaper.CreateFloor(FLength,FWidth,TileWidth,TileLength:Double);
var
LeftSide,RightSide,FrontSide,BackSide : Double;
NewLine : TG3DLine;
Pos     : Double;
Incr    : Double;
begin
//create LinesInXFirst
FloorList.Clear;
LeftSide := -FWidth/2.0;
RightSide := FWidth/2.0;
FrontSide := -FLength/2.0;
BackSide := FLength/2.0;
NewLine :=  TG3DLine.create(Self);
Newline.VirtualOnly := False;
with NewLine do
     begin
     ReferencePoint := SetPrecisePoint3D(LeftSide,0,0);
     Length := FLength;
     Update;
     RotateAllPointsInZAbout(ReferencePoint,90.0,dmEditAbs,incR);
     Colour := FFloorColour;
     end;
FloorList.Add(NewLine);
Pos := LeftSide;

While Pos < RightSide do
      begin
      Pos := Pos + TileWidth;
      NewLine :=  TG3DLine.create(Self);
      with NewLine do
           begin
           VirtualOnly := False;
           ReferencePoint := SetPrecisePoint3D(Pos,0,0);
           Length := FLength;
           SetCorToDatum;
           Update;
           RotateAllPointsInZAbout(ReferencePoint,90.0,dmEditAbs,incr);
           Colour := FFloorColour;
           end;
      FloorList.Add(NewLine);
      end;

//create LinesIn Y

NewLine :=  TG3DLine.create(Self);
with NewLine do
     begin
     VirtualOnly := False;
     ReferencePoint := SetPrecisePoint3D(0,FrontSide,0);
     Length := FLength;
     DatumRotation := SetPRotation(0,0,0);
     SetCorToDatum;
     Colour := FFloorColour;
     Update;
     end;
FloorList.Add(NewLine);
Pos := FrontSide;

While Pos < BackSide do
      begin
      Pos := Pos + TileLength;
      NewLine :=  TG3DLine.create(Self);
      with NewLine do
           begin
           VirtualOnly := False;
           ReferencePoint := SetPrecisePoint3D(0,Pos,0);
           Length := FLength;
           DatumRotation := SetPRotation(0,0,0);
           Colour := FFloorColour;
           SetCorToDatum;
           Update;
           end;
      FloorList.Add(NewLine);
      end;
// Now show 0,0,0 in red cross
NewLine :=  TG3DLine.create(Self);
with NewLine do
      begin
      VirtualOnly := False;
      Colour := clGreen;
      ReferencePoint := SetPrecisePoint3D(0,0,0);
      Length := TileLength*4;
      DatumRotation := SetPRotation(0,0,0);
      SetCorToDatum;
      Update;
      end;
FloorList.Add(NewLine);
NewLine :=  TG3DLine.create(Self);
with NewLine do
      begin
      VirtualOnly := False;
      Colour := clGreen;
      ReferencePoint := SetPrecisePoint3D(0,0,0);
      Length := TileLength*4;
      RotateAllPointsInZAbout(ReferencePoint,90.0,dmEditAbs,incr);
      Update;
      end;
FloorList.Add(NewLine);

NewLine :=  TG3DLine.create(Self);
with NewLine do
      begin
      VirtualOnly := False;
      Colour := clGreen;
      ReferencePoint := SetPrecisePoint3D(0,0,0);
      Length := TileLength*2;
      RotateAllPointsInYAbout(ReferencePoint,90.0,dmEditAbs,Incr);
      Update;
      end;
FloorList.Add(NewLine);

end;


procedure TG3DPaper.Resize;
begin
inherited Resize;
with Background do
        begin
        Width := Self.width-4;
        Height := Self.Height-4;
        Top := 2;
        Left := 2;
        Color := clSilver;
        end;

with Surface do
        begin
        Width := Self.width-4;
        Height := Self.Height-4;
        Top := 0;
        Left := 0;
        OnPaint := PaintObjects;
        end;


with ClearRect do
        begin
        Top := 0;
        left := 0;
        Right := Surface.Width;
        Bottom := Surface.Height;
        end;
Offscreen.Width := Surface.Width;
Offscreen.Height := Surface.Height;
if FCentreIsZero then
   begin
   FXCentre := Surface.Width div 2;
   FZCentre := Surface.Height div 2;
   end;
if not (Parent = nil) then Paint;
end;

constructor TG3DPaper.Create(AOwner: TComponent);
begin
inherited;
BackGround := TPanel.Create(Self);
BackGround.Parent := Self;
Background.Color := clWhite;
FRotation := SetPRotation(0,0,0);
FCOR      := SetPrecisePoint3D(0,0,0);
Surface := Tpaintbox.Create(Self);
Surface.parent := BackGround;
Offscreen :=TBitmap.Create;


FFloorColour := clSilver;
FloorList := TList.Create;
FloorList.Clear;
FFloorTileWidth := 100;
FFloorTileLength := 100;
FFloorLength := 1000;
SetFloorWidth(1000);
SetFloorWidth(1000);
FPerspective := 0.0;  // reduction per unit length;
FCentreIsZero := True;
FUnits :=  uMetric; // unit = 1mm
FScale := 10.0; // Pixels Per Unit
end;
destructor TG3DPaper.Destroy;
begin
//if assigned(offscreen) then offscreen.Free;
inherited Destroy;
end;
procedure TG3DPaper.Paint;
begin
//inherited;
Surface.Canvas.copyrect(ClearRect,Offscreen.Canvas,ClearRect);
end;

procedure TG3DPaper.PaintObjects(Sender : TObject);
begin
Paint;
end;

procedure TG3DPaper.CleanSheet(Color : Tcolor);
begin

with Offscreen.Canvas do
        begin
        Brush.Color := Color;
        FillRect(ClearRect);
        end;
If FShowFloor then
   begin
   DrawFloor;
   end;
end;


procedure TG3DPaper.SetUnits (newUnits : TUnits);
begin
Funits := NewUnits;
end;

procedure TG3DPaper.SetScale (NValue : Double);
begin
FScale := NValue;
// Repaint all objects now
end;


procedure TG3DPaper.SetWorldTranslation(NValue : TScreenLocation);
begin
FTranslation := NValue;
end;

procedure TG3DPaper.SetWorldRotation(nValue : TRotation);
begin
FRotation := nValue;
end;

procedure TG3DPaper.SetPerspective (NValue : Double);
begin
if NValue = FPerspective then exit;
FPerspective := NValue;
// Repaint all objects now
end;
procedure TG3DPaper.SetFloorWidth (NValue : Double);
begin
if NValue = FFloorWidth then exit;
FFloorWidth := NValue;
FloorList.Clear;
CreateFloor(FFloorLength,FFloorWidth,FFloorTileWidth,FFloorTileLength);
end;
procedure TG3DPaper.SetFloorTileWidth (NValue : Double);
begin
if NValue = FFloorTileWidth then exit;
FFloorTileWidth := NValue;
end;
procedure TG3DPaper.SetFloorLength (NValue : Double);
begin
if NValue = FFloorLength then exit;
FFloorLength := NValue;
FloorList.Clear;
CreateFloor(FFloorLength,FFloorWidth,FFloorTileWidth,FFloorTileLength);
end;
procedure TG3DPaper.SetFloorTileLength (NValue : Double);
begin
if NValue = FFloorTileLength then exit;
FFloorTileLength := NValue;
end;

procedure TG3DPaper.SetFloorColour(nValue : TColor);
var
Count : Integer;
begin
FFloorColour := nValue;
if not assigned(FloorList) then exit;
For Count := 0 to FloorList.Count-4 do // dont colour the datum cross (last three entries)
    begin
    TG3DLine(FloorList[Count]).Colour := FFloorColour;
    end;
//PaperPaint(Self);
end;

procedure TG3DPaper.SetShowFloor(NValue : Boolean);
begin
if NValue = FShowFloor then exit;
FShowFloor := NValue;
CreateFloor(FFloorLength,FFloorWidth,FFloorTileWidth,FFloorTileLength);
end;


procedure TG3DPaper.AddBackGround(Color : Tcolor);
begin
Offscreen.Canvas.Brush.Color := Color;
Offscreen.Canvas.Floodfill(1,1,clwhite,fsSurface);
Offscreen.Canvas.Floodfill(Offscreen.Width-1,1,clwhite,fsSurface);
Offscreen.Canvas.Floodfill(Offscreen.Width-1,Offscreen.Height-1,clwhite,fsSurface);
Offscreen.Canvas.Floodfill(1,Offscreen.Height-1,clwhite,fsSurface);
end;



procedure TG3DPaper.DrawFloor;
var
FListEntry : ^TG3DLine;
FCount     : Integer;
begin
if not assigned(FloorList) then exit;
if FloorList.Count = 0 then exit;
For Fcount := 0 to FloorList.Count-1 do
    begin
    FListEntry := FloorList[FCount];
    with TG3DLine(FListEntry) do
         begin
         DrawOn(Self,dmFullDisplay);
         end;
    end;

end;

procedure Register;
begin
  RegisterComponents('G3D', [TG3DPaper]);
end;

end.
