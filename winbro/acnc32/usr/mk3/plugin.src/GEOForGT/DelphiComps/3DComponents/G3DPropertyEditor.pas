unit G3DPropertyEditor;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls,G3DDefs,GMath3D,G3DLine,G3DBox;

type
  TG3DPropertyEditor = class(TCustomPanel)
  private
    { Private declarations }
  protected
    { Protected declarations }
  public
    { Public declarations }
  published
  constructor Create(AOwner: TComponent);override;
  destructor Destroy; override;

    { Published declarations }
  end;

procedure Register;

implementation

constructor TG3DPropertyEditor.Create(AOwner: TComponent);
begin
inherited;
Width := 300;
Height := 100;

end;

destructor TG3DPropertyEditor.Destroy;
begin
inherited;
end;

procedure Register;
begin
  RegisterComponents('G3D', [TG3DPropertyEditor]);
end;

end.
