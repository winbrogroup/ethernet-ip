unit ACNC_TouchIndexFormatGrid;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls,comctrls,ACNC_TouchIndexContainer,ACNC_TouchGlyphSoftkey,
  ACNC_TouchFormatGrid,Grids;

const
VerticalTabWidth = 50;
DefaultTabHeight = 40;

type
  TTouchIndexFormatGrid = class(TTouchIndexContainer)
  private

    FCellEdited: TSetEditEvent;
    FSelectedCellChange : TSelectCellEvent;
    FGridDblClick       : TnotifyEvent;
    function GetColCount: Integer;
    function GetColor: TColor;
    function GetDefaultColWidth: Integer;
    function GetDefaultRowHeight: Integer;
    function GetFixedColor: TColor;
    function GetFixedColumns: Integer;
    function GetFixedRows: Integer;
    function GetRowCount: Integer;
    procedure SetColCount(const Value: Integer);
    procedure SetColor(const Value: TColor);
    procedure SetDefaultColWidth(const Value: Integer);
    procedure SetDefaultRowHeight(const Value: Integer);
    procedure SetFixedColor(const Value: TColor);
    procedure SetFixedColumns(const Value: Integer);
    procedure SetFixedRows(const Value: Integer);
    procedure SetRowCount(const Value: Integer);
    procedure SetDefNumFormat(const Value: String);
    procedure SetDisplayUnformatted(const Value: Boolean);
    function GetDefNumFormat: String;
    function GetDisplayUnformatted: Boolean;
    function GetCells(ACol, ARow: Integer): string;
    function GetColWidths(Index: Integer): Integer;
    function GetRowHeights(Index: Integer): Integer;
    procedure SetCells(ACol, ARow: Integer; const Value: string);
    procedure SetColWidths(Index: Integer; const Value: Integer);
    procedure SetRowHeights(Index: Integer; const Value: Integer);
    function GetSelectedCol: Integer;
    function GetSelectedRow: Integer;
    function GetSelectedString: String;
    function GetisReadOnly: Boolean;
    procedure GridCellEdited(Sender: TObject; ACol, ARow: Longint; const Value: string);
    procedure SelectedCellChanged  (Sender: TObject; ACol, ARow: Longint;var CanSelect: Boolean);
    procedure GridDbleClick (Sender : Tobject);
    procedure SetGridDoubleClick(const Value: TNotifyEvent);

  protected
    { Protected declarations }
    procedure Resize; override;
//    procedure DoSetEditable(const Value: Boolean) ; OVERRIDE;
    procedure SetisReadOnly(const Value: Boolean);
  public
    { Public declarations }
    FormatGrid : TTouchFormatGrid;
    property ColWidths[Index: Longint]: Integer read GetColWidths write SetColWidths;
    property RowHeights[Index: Longint]: Integer read GetRowHeights write SetRowHeights;
    property Cells[ACol, ARow: Integer]: string read GetCells write SetCells;
  published
    { Published declarations }
    constructor Create(AOwner:TComponent); override;
    destructor Destroy; override;

    // pass through properties for the Grid
    Property RowCount : Integer read GetRowCount write SetRowCount;
    Property ColCount : Integer read GetColCount write SetColCount;
    Property DefaultRowHeight : Integer read GetDefaultRowHeight write SetDefaultRowHeight;
    Property DefaultColWidth : Integer read GetDefaultColWidth write SetDefaultColWidth;
    property FixedRows : Integer read GetFixedRows write SetFixedRows;
    property FixedCols : Integer read GetFixedColumns write SetFixedColumns;
    property FixedColor : TColor read GetFixedColor write SetFixedColor;
    property Color : TColor read GetColor write SetColor;
    property DefaultColNumberFormat : String read GetDefNumFormat write SetDefNumFormat;
    property DisplayUnformatted : Boolean read GetDisplayUnformatted write SetDisplayUnformatted;
    property SelectedRow : Integer read GetSelectedRow ;
    property SelectedCol : Integer read GetSelectedCol ;
    property SelectedString : String read GetSelectedString ;
    property isReadOnly : Boolean read GetisReadOnly write SetisReadOnly;
    property CellEdited : TSetEditEvent read FCellEdited write FCellEdited;
    property OnSelectionChanged : TSelectCellEvent read FSelectedCellChange write FSelectedCellChange;
    property OnGridDblClick : TNotifyEvent read FGridDblClick write SetGridDoubleClick;

//    property Font : TFont read GetFont write SetFont;


    property Align;



  end;

procedure Register;

implementation

procedure Register;
begin
  RegisterComponents('ACNCComps', [TTouchIndexFormatGrid]);
end;

{ TTouchIndexFormatGrid }



constructor TTouchIndexFormatGrid.Create(AOwner: TComponent);
begin
  inherited;
  Height := 100;
  Width := 200;
  BevelOuter := bvNone;
  BevelInner := bvNone;
  FormatGrid  := TTouchFormatGrid.Create(Self);
  FormatGrid.Name := 'FormatGrid';
  with FormatGrid do
       begin
       Parent := Self;
       Align := alNone;
       CellEdited := GridCellEdited;
       OnSelectCell := SelectedCellChanged;
       OnGridDoubleClick := GridDbleClick;
       end;
  SetContainedComponent(FormatGrid);
  ControlToFocus := FormatGrid.Grid;
Height := 300;
Width := 400;
Resize;
end;

destructor TTouchIndexFormatGrid.Destroy;
begin
  inherited;

end;





function TTouchIndexFormatGrid.GetCells(ACol, ARow: Integer): string;
begin
Result := FormatGrid.Cells[ACol,ARow];
end;

function TTouchIndexFormatGrid.GetColCount: Integer;
begin
Result := FormatGrid.ColCount;
end;

function TTouchIndexFormatGrid.GetColor: TColor;
begin
Result := FormatGrid.Color;

end;

function TTouchIndexFormatGrid.GetColWidths(Index: Integer): Integer;
begin
Result := FormatGrid.ColWidths[Index];
end;

function TTouchIndexFormatGrid.GetDefaultColWidth: Integer;
begin
Result := FormatGrid.DefaultColWidth;
end;

function TTouchIndexFormatGrid.GetDefaultRowHeight: Integer;
begin
Result := FormatGrid.DefaultRowHeight;
end;

function TTouchIndexFormatGrid.GetDefNumFormat: String;
begin
Result := FormatGrid.DefaultColNumberFormat;
end;

function TTouchIndexFormatGrid.GetDisplayUnformatted: Boolean;
begin
Result := FormatGrid.DisplayUnformatted;
end;

function TTouchIndexFormatGrid.GetFixedColor: TColor;
begin
Result := FormatGrid.FixedColor;
end;

function TTouchIndexFormatGrid.GetFixedColumns: Integer;
begin
Result := FormatGrid.FixedCols;
end;

function TTouchIndexFormatGrid.GetFixedRows: Integer;
begin
Result := FormatGrid.FixedRows;
end;


function TTouchIndexFormatGrid.GetisReadOnly: Boolean;
begin
Result := FormatGrid.isReadOnly;

end;

function TTouchIndexFormatGrid.GetRowCount: Integer;
begin
Result := FormatGrid.RowCount;
end;

function TTouchIndexFormatGrid.GetRowHeights(Index: Integer): Integer;
begin
Result := FormatGrid.RowHeights[Index];
end;

function TTouchIndexFormatGrid.GetSelectedCol: Integer;
begin
Result := FormatGrid.SelectedCol;
end;

function TTouchIndexFormatGrid.GetSelectedRow: Integer;
begin
Result := FormatGrid.SelectedRow;
end;

function TTouchIndexFormatGrid.GetSelectedString: String;
begin
Result := FormatGrid.SelectedString;
end;

procedure TTouchIndexFormatGrid.GridCellEdited(Sender: TObject; ACol,
  ARow: Integer; const Value: string);
begin
if assigned(FCellEdited) then FCellEdited(Sender,ACol,ARow,Value);
end;


procedure TTouchIndexFormatGrid.Resize;
begin
  inherited;
  if not assigned(FormatGrid) then exit;
{  if FormatGrid.EditableAtRuntime then
    begin
    Self.Editable := True;
    end
  else
    begin
    Self.Editable := False;
    end;}

  FormatGrid.Height := Height-BottomPanel.Height;
  if IndexVisible then
     begin
     FormatGrid.Width := Width-IndexPanel.Width-5;
     FormatGrid.Left := IndexPanel.Width+1;
     end
  else
      begin
      FormatGrid.Width := Width-5;
      FormatGrid.Left := 1;
      end;
end;

procedure TTouchIndexFormatGrid.SelectedCellChanged(Sender: TObject; ACol, ARow: Longint;var CanSelect: Boolean);
begin
if assigned(FSelectedCellChange) then FSelectedCellChange(Sender,ACol,Arow,CanSelect);
end;

procedure TTouchIndexFormatGrid.SetCells(ACol, ARow: Integer;
  const Value: string);
begin
FormatGrid.Cells[ACol,ARow] := Value;
end;

procedure TTouchIndexFormatGrid.SetColCount(const Value: Integer);
begin
FormatGrid.ColCount := Value;
end;

procedure TTouchIndexFormatGrid.SetColor(const Value: TColor);
begin
FormatGrid.Color := Value;
end;

procedure TTouchIndexFormatGrid.SetColWidths(Index: Integer;
  const Value: Integer);
begin
FormatGrid.ColWidths[Index] := Value;
end;

procedure TTouchIndexFormatGrid.SetDefaultColWidth(const Value: Integer);
begin
FormatGrid.DefaultColWidth := Value;
end;

procedure TTouchIndexFormatGrid.SetDefaultRowHeight(const Value: Integer);
begin
FormatGrid.DefaultRowHeight := Value;

end;

procedure TTouchIndexFormatGrid.SetDefNumFormat(const Value: String);
begin
FormatGrid.DefaultColNumberFormat := Value;
end;

procedure TTouchIndexFormatGrid.SetDisplayUnformatted(
  const Value: Boolean);
begin
FormatGrid.DisplayUnformatted := Value;
end;

procedure TTouchIndexFormatGrid.SetFixedColor(const Value: TColor);
begin
FormatGrid.FixedColor := Value;
end;

procedure TTouchIndexFormatGrid.SetFixedColumns(const Value: Integer);
begin
FormatGrid.FixedCols := Value;
end;

procedure TTouchIndexFormatGrid.SetFixedRows(const Value: Integer);
begin
FormatGrid.FixedRows := Value;
end;


procedure TTouchIndexFormatGrid.SetGridDoubleClick(
  const Value: TNotifyEvent);
begin
  FGridDblClick := Value;
end;

procedure TTouchIndexFormatGrid.GridDbleClick(Sender: Tobject);
begin
if assigned(FGridDblClick) then FGridDblClick(Sender)
end;

procedure TTouchIndexFormatGrid.SetisReadOnly(const Value: Boolean);
begin
FormatGrid.isReadOnly := Value;
KBToggleVisible := not(Value);
end;

procedure TTouchIndexFormatGrid.SetRowCount(const Value: Integer);
begin
FormatGrid.RowCount := Value;
end;

procedure TTouchIndexFormatGrid.SetRowHeights(Index: Integer;
  const Value: Integer);
begin
FormatGrid.RowHeights[Index] := Value;
end;




{procedure TTouchIndexFormatGrid.DoSetEditable(const Value: Boolean);
begin
  inherited;
  FormatGrid.EditableAtRuntime := Value;
  FormatGrid.isReadOnly := not Value;
end;}

end.
