unit SetKey;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs;

type
  TKeyState = ( kbOn, kbOff );

  TSetKey = class(TComponent)
  private
    FCapsLock : TKeyState;
    FNumLock : TKeyState;
    FScrollLock : TKeyState;
    FSystemKey : TKeyState;

    procedure SetCapsLock( value : TKeyState );
    procedure SetNumLock( value : TKeyState );
    procedure SetScrollLock( value : TKeyState );
    procedure SetSystemKey( value : TKeyState );
    { Private declarations }
  protected
    { Protected declarations }
  public
    constructor Create(AOwner :TComponent);override;
    { Public declarations }
  published
    property CapsLock: TKeyState read FCapsLock write SetCapsLock;
    property NumLock: TKeyState read FNumLock write SetNumLock;
    property ScrollLock: TKeyState read FScrollLock write SetScrollLock;
    property SystemKey : TkeyState read FSystemKey write SetSystemKey;
    { Published declarations }
  end;

procedure Register;

implementation

procedure Register;
begin
  RegisterComponents('CNC', [TSetKey]);
end;

constructor TSetKey.Create(AOwner :TComponent);
Var
  KeyState : TKeyBoardState;
  OldValue : integer;
begin
  inherited Create(AOwner);
  GetKeyboardState(KeyState);
  Case KeyState[VK_CAPITAL] of
    1 : FCapsLock := kbOn;
    0 : FCapsLock := kbOff;
  end;
  case KeyState[VK_NUMLOCK] of
    1 : FNumLock := kbOn;
    0 : FNumLock := kbOff;
  end;
  case KeyState[VK_SCROLL] of
    1 : FScrollLock := kbOn;
    0 : FScrollLock := kbOff;
  end;
  SystemParametersInfo( SPI_SCREENSAVERRUNNING, 0, @OldValue, 0);
  { Set the SystemKey to its original value, who can teach me a good way? :) }
  case OldValue of
    1 : FSystemKey := kbOff;
    0 : FSystemKey := kbOn;
  end;
  SystemParametersInfo( SPI_SCREENSAVERRUNNING, OldValue, @OldValue, 0);
end;

procedure TSetKey.SetCapsLock( value : TKeyState );
Var
  KeyState : TKeyBoardState;
begin
  GetKeyboardState(KeyState);
  case value of
    kbOn : KeyState[VK_CAPITAL] := 1;
    kbOff : KeyState[VK_CAPITAL] := 0;
  end;
  SetKeyboardState(KeyState);
  FCapsLock := Value;
end;

procedure TSetKey.SetNumLock( value : TKeyState );
Var
  KeyState : TKeyBoardState;
begin
  GetKeyboardState(KeyState);
  case value of
    kbOn : KeyState[VK_NUMLOCK] := 1;
    kbOff : KeyState[VK_NUMLOCK] := 0;
  end;
  SetKeyboardState(KeyState);
  FNumLock := value;
end;

procedure TSetKey.SetScrollLock( value : TKeyState );
Var
  KeyState : TKeyBoardState;
begin
  GetKeyboardState(KeyState);
  case value of
    kbOn : KeyState[VK_SCROLL] := 1;
    kbOff : KeyState[VK_SCROLL] := 0;
  end;
  SetKeyboardState(KeyState);
  FScrollLock := value;
end;

procedure TSetkey.SetSystemKey( value : TKeyState );
var
  NoUse : integer;
begin
  case value of
    kbOn : SystemParametersInfo( SPI_SCREENSAVERRUNNING, 0, @NoUse, 0);
    kbOff : SystemParametersInfo( SPI_SCREENSAVERRUNNING, 1, @NoUse, 0);
  end;
  FSystemKey := value;
end;

end.
