unit ACNC_TouchSoftkey;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls;


type
  TSoftKeyStates = (skUp,skDown);
  TSoftKeyStyles = (skNormal,skUpArrow,skDownArrow,skLeftArrow,skRightArrow);

  TTouchSoftkey = class(TPanel)
  private
    { Private declarations }
    Picture : TPaintBox;
    FLegend : String;
    FState  : TSoftKeyStates;
    FTextSize : Integer;
    FHasIndicator : Boolean;
    FIndicatorOn  : Boolean;
    FIsEnabled    : Boolean;
    FStyle        : TSoftKeyStyles;
    FArrowColor   : TColor;
    FButtonColor  : TColor;
    FLegendColor  : TColor;
    FIndicatorOnColor : TColor;
    FIndicatorOffColor : TColor;

    FIndicatorWidth : Integer;
    FIndicatorHeight : Integer;
    FMaxFont: Integer;


    procedure PaintNormal;
    procedure PaintArrow(KeyStyle : TSoftKeyStyles);
    procedure DoMyMouseDown(Sender : TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure DoMyMouseUp(Sender : TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure DoMyClick(Sender : TObject);
    procedure SetMaxFont(const Value: Integer);



  protected
    { Protected declarations }

    procedure MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer);override;
    procedure MouseUp(Button: TMouseButton; Shift: TShiftState; X, Y: Integer);override;

    procedure SetLegend(Text : String);
    procedure SetIndicator(State : Boolean);
    procedure SetSEnabled(State : Boolean);
    procedure SetArrowColor(AColor : TColor);
    procedure SetButtonColor(AColor : TColor);
    procedure SetLegendColor(AColor : TColor);
    procedure SetStyle(KeyStyle :TSoftKeyStyles);
    procedure SetTextSize(Size : Integer);
    procedure ButtonPaint(Sender :TObject);


  public
    { Public declarations }
    constructor Create(AOwner:TComponent); override;
    destructor Destroy; override;

  published
    { Published declarations }
    property Legend : String read FLegend write SetLegend;
    property TextSize : Integer read FTextSize write SetTextSize;
    property HasIndicator : Boolean read FHasIndicator write FHasIndicator;
    property IndicatorOn  : Boolean read FIndicatorOn write SetIndicator;
    property IsEnabled    : Boolean read FIsEnabled write SetSEnabled;
    property ButtonStyle  : TSoftKeyStyles read FStyle write SetStyle;
    property ArrowColor  : TColor read FArrowColor write SetArrowColor;
    property ButtonColor : TColor read FButtonColor write SetButtonColor;
    property LegendColor : TColor read FLegendColor write SetLegendColor;
    property IndicatorWidth :Integer read FIndicatorWidth write FIndicatorWidth;
    property IndicatorHeight :Integer read FIndicatorHeight write FIndicatorHeight;
    property IndicatorOnColor : TColor read FIndicatorOnColor write FIndicatorONColor;
    property IndicatorOffColor : TColor read FIndicatorOffColor write FIndicatorOffColor;
    property MaxFontSize : Integer read FMaxFont write SetMaxFont;

  end;

procedure Register;

implementation

constructor TTouchSoftkey.Create(AOwner:TComponent);
begin
inherited;
FIndicatorHeight := 10;
FIndicatorwidth := 20;
FIndicatorOnColor := clGreen;
FIndicatorOffColor := clSilver;
Picture := TPaintBox.Create(Self);
Picture.OnMouseDown := DoMyMouseDown;
Picture.OnMouseUp := DoMyMouseUp;
Picture.OnClick := DoMyClick;
Picture.Parent := Self;
Picture.Align := alClient;
FStyle := skNormal;
Picture.OnPaint := ButtonPaint;
FTextSize := 14;
if assigned(Parent) then
   begin
   SetLegend('Text~Here');
   end;
SetSEnabled(True);
FButtonColor := clwhite;
end;

procedure TTouchSoftkey.SetStyle(KeyStyle :TSoftKeyStyles);
begin
FStyle := KeyStyle;
ButtonPaint(Self);
end;


destructor TTouchSoftkey.Destroy;
begin
inherited;
end;

procedure TTouchSoftkey.SetArrowColor(AColor : TColor);
begin
FArrowColor := AColor;
ButtonPaint(Self);
end;


procedure TTouchSoftkey.SetLegendColor(AColor : TColor);
begin
FLegendColor := AColor;
Picture.Canvas.Font.Color := AColor;
ButtonPaint(Self);
end;

procedure TTouchSoftkey.SetButtonColor(AColor : TColor);
begin
FButtonColor := AColor;
Picture.Color := AColor;
ButtonPaint(Self);
end;

procedure TTouchSoftkey.SetSEnabled(State : Boolean);
begin
FIsEnabled := State;
if not assigned(Picture) then exit;
if FIsEnabled then
   begin
   Enabled := True;
   Picture.Canvas.Font.Color := FLegendColor;
   end
else
   begin
   Enabled := False;
   Picture.Canvas.Font.Color := clSilver;
   end;
if assigned(Parent) then ButtonPaint(Self);
end;


procedure TTouchSoftkey.SetIndicator(State : Boolean);
begin
FIndicatorOn := State;
ButtonPaint(Self);
end;

procedure TTouchSoftkey.SetLegend(Text : String);
begin
FLegend := Text;
ButtonPaint(Self);
end;


procedure TTouchSoftkey.SetTextSize(Size : Integer);
begin
FTextSize := Size;
ButtonPaint(Self);
end;

procedure TTouchSoftkey.MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
inherited;
FState := skDown;
ButtonPaint(Self);
end;

procedure TTouchSoftkey.MouseUp(Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
inherited;
FState := skUp;
ButtonPaint(Self);
end;
procedure TTouchSoftkey.PaintArrow(KeyStyle : TSoftKeyStyles);
var
BOffset : Integer;
TopColor,BottomColor : TColor;
LegendTextSize : TSize;
Textx    : Integer;
Texty    : Integer;

begin

BOffset :=3;
if Fstate = skUp then
   begin
   TopColor := clWhite;
   BottomColor := clGray;
   end
else
   begin
   TopColor := clGray;
   BottomColor := clWhite;
   end;
with Picture.Canvas do
begin
case KeyStyle of
     skUpArrow:
     begin
     if FIsEnabled then
        Brush.Color := FArrowColor
     else
        Brush.Color := clSilver;

     Polygon([Point(Width div 2,Boffset),
              Point(Boffset,Height-Boffset),
              Point(Width-Boffset,Height-Boffset)]);
     Pen.Color := TopColor;
     Pen.Width := 2;
     MoveTo(Width div 2,Boffset);
     LineTo(Boffset,Height-Boffset);
     Pen.Color := BottomColor;
     LineTo(Width-Boffset,Height-Boffset);
     LineTo(Width div 2,Boffset);
     Pen.Width := 1;
     if FLegend <> '' then
        begin
        LegendTextSize := TextExtent(Flegend);
        if LegendTextSize.cx < Width then
           begin
           Textx := (Width- LegendTextSize.cx) div 2;
           end
        else
            begin
            Textx := 2;
            end;
        Texty := ((Height- LegendTextSize.cy) div 2) + Height div 5;
        TextOut(Textx,Texty,FLegend);
        end;
     end;

     skDownArrow:
     begin
     if FIsEnabled then
        Brush.Color := FArrowColor
     else
        Brush.Color := clSilver;
     Polygon([Point(Boffset,Boffset),
              Point(Width-Boffset,Boffset),
              Point(Width div 2,Height-Boffset)]);
     Pen.Color := TopColor;
     Pen.Width := 2;
     MoveTo(Boffset,Boffset);
     LineTo(Width-Boffset,Boffset);
     Pen.Color := BottomColor;
     LineTo(Width div 2,Height-Boffset);
     Pen.Color := TopColor;
     LineTo(Boffset,Boffset);
     Pen.Width := 1;
     if FLegend <> '' then
        begin
        LegendTextSize := TextExtent(Flegend);
        if LegendTextSize.cx < Width then
           begin
           Textx := (Width- LegendTextSize.cx) div 2;
           end
        else
            begin
            Textx := 2;
            end;
        Texty := ((Height- LegendTextSize.cy) div 2) - Height div 5;
        TextOut(Textx,Texty,FLegend);
        end;
     end;
end; // case
end; // with Canvas


end;

procedure TTouchSoftkey.PaintNormal;
var
BOffset : Integer;
BottomColor : TColor;
LegendOffset         : Integer;
TextPos              : Integer;
TextIndent           : Integer;
TildPos : Integer;
Buffer1  : String;
Buffer2  : String;
LegendHeight : Integer;
TLength      : iNTEGER;
IndicatorRect : TRect;
begin
BOffset :=3;
if Fstate = skUp then
   begin
   BottomColor := clGray;
   LegendOffset := 0;
   end
else
   begin
   BottomColor := clWhite;
   LegendOffset := 3;
   end;

TextPos := BOffset * 2;
with Picture.Canvas do
     begin
     Pen.Color := BottomColor;
     Pen.Width := 1;
     MoveTo(Width-BOffset,BOffset);
     if FHasIndicator then
        begin
        IndicatorRect.Top := 4+LegendOffset;
        IndicatorRect.Left := 4+LegendOffset;
        IndicatorRect.Right := 4+LegendOffset+FIndicatorWidth;
        IndicatorRect.Bottom := 4+LegendOffset+ FIndicatorHeight;
        if FIndicatorOn then
           begin
           Brush.Color := FIndicatorOnColor;
           end
        else
           begin
           Brush.Color := FIndicatorOffColor;
           end;
        if Not FIsEnabled then Brush.Color := clSilver;
        FillRect(IndicatorRect);
        end
     else
        begin
        MoveTo(BOffset+4,BOffset+4);
        LineTo(BOffset+4,BOffset+14);
        MoveTo(BOffset+4,BOffset+4);
        LineTo(BOffset+14,BOffset+4);
        end;

     end;

Picture.Canvas.Brush.Color := FButtonColor;

If FLegend <> '' then
   begin
   TildPos := Pos('~',FLegend);
   if TildPos > 0 then
      begin
      LegendHeight :=Height div 4;
      Buffer1 := Copy(FLegend,1,TildPos-1);
      Buffer2 := Copy(FLegend,TildPOs+1,Length(FLegend));

      Brush.Color := FButtonColor;
      with Picture.Canvas do
           begin
           if FIsEnabled then
              begin
              Font.Color := FLegendColor;
              end
           else
               begin
               Font.Color := clGray;
               end;

           Pen.Color := FButtonColor;
           Font.Height := -LegendHeight;
           TLength := TextWidth(Buffer1);
           TextIndent := (Width-TLength) div 2;
           TextOut(TextIndent+LegendOffset,TextPos+LegendOffset,Buffer1);
           TLength := TextWidth(Buffer2);
           TextIndent := (Width-TLength) div 2;
           TextOut(TextIndent+LegendOffset,TextPos+LegendHeight+LegendOffset+6,Buffer2);
           end;

      end
   else
      begin
//      LegendHeight :=Height div 2;
      with Picture.Canvas do
           begin
           Brush.Color := FButtoncolor;
           Pen.Color := FButtoncolor;
           if FIsEnabled then
              begin
              Font.Color := FLegendColor;
              end
           else
               begin
               Font.Color := clGray;
               end;
           TLength := TextWidth(FLegend);
           TextIndent := (Width-TLength) div 2;
           TLength := TextHeight(FLegend);
           TextPos := (Height-TLength) div 2;
           TextOut(TextIndent+LegendOffset,TextPos+LegendOffset,Legend);
           end;
      end;
   end;
end;
procedure TTouchSoftkey.ButtonPaint(Sender :TObject);
var
BOffset : Integer;
PWidth  : Integer;
TopColor,BottomColor : TColor;
BRect    : TRect;
InnerRect : TRect;

begin
if not assigned(Parent) then exit;
BOffset :=3;
PWidth := 3;

InnerRect.Top := BOffset;
InnerRect.Left := BOffset;
InnerRect.Right := Width-(Boffset*2);
InnerRect.Bottom := Height-(Boffset*2);
BRect.Top := 0;
BRect.Left := 0;
BRect.Right := Width;
BRect.Bottom := Height;
if Fstate = skUp then
   begin
   TopColor := clWhite;
   BottomColor := clGray;
   end
else
   begin
   TopColor := clGray;
   BottomColor := clWhite;
   end;

with Picture.Canvas do
     begin
     Brush.Color := clSilver;
     FillRect(Brect);
     Brush.Color := FButtonColor;
     FillRect(InnerRect);
     Pen.color := TopColor;
     Pen.Width :=PWidth;
     MoveTo(BOffset,BOffset);
     LineTo(Width-Boffset,BOffset);
     MoveTo(BOffset,BOffset);
     LineTo(BOffset,Height-BOffset);
     MoveTo(Width-BOffset,Height-BOffset);
     Pen.Color := BottomColor;
     LineTo(BOffset,Height-BOffset);
     MoveTo(Width-BOffset,Height-BOffset);
     LineTo(Width-BOffset,BOffset);
     Pen.Width := 1;
     end;

case FStyle of
     skNormal: PaintNormal;
     skUpArrow:PaintArrow(FStyle);
     skDownArrow:PaintArrow(FStyle);
     skLeftArrow:PaintArrow(FStyle);
     skRightArrow:PaintArrow(FStyle);
     end; //case;

end;

procedure TTouchSoftkey.DoMyMouseDown(Sender : TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
MouseDown(Button,Shift,X, Y);
ButtonPaint(Self);
end;

procedure TTouchSoftkey.DoMyMouseUp(Sender : TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
MouseUp(Button,Shift,X, Y);
ButtonPaint(Self);
end;

procedure TTouchSoftkey.DoMyClick(Sender : TObject);
begin
Click;
end;

procedure Register;
begin
  RegisterComponents('ACNCComps', [TTouchSoftkey]);
end;

procedure TTouchSoftkey.SetMaxFont(const Value: Integer);
begin
  FMaxFont := Value;
  ButtonPaint(Self);
end;

end.
