unit TouchPageControl;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls,comctrls,buttons,TouchIndexContainer,TouchGlyphSoftkey;

const
VerticalTabWidth = 50;
DefaultTabHeight = 40;

type
  TTouchPageControl = class(TTouchIndexContainer)
  private
    FTopTabWidth: Integer;
    FTopTabHeight: Integer;

    ShowTabsButton  : TTouchGlyphSoftkey;
    FTabsVisible: Boolean;
    SelectedPageIndex : Integer;


    procedure SetTopTabHeight(const Value: Integer);
    procedure SetTopTabWidth(const Value: Integer);
    function GetIndexStrings: TStrings;
    function GetPageControl: TPageControl;
    procedure ShowTabsMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure SetTabsVisible(const Value: Boolean);
    function GetNumberPages: Integer;
    procedure PageChangeHandler(Sender : TObject);
    procedure ResizeIndexForm;
    procedure DoOnShowControls; override;
    procedure DoOnHideControls; override;

  protected
    { Protected declarations }
    procedure Resize; override;
  public
    { Public declarations }
    TabPageControl : TPageControl;
  published
    { Published declarations }
    constructor Create(AOwner:TComponent); override;
    destructor Destroy; override;

    function AddPage(PageName,PCaption:String;UsesIndex : Boolean): Integer;
    function PageByName(PageName : String) : TTabSheet;
    procedure ClearPages;

    property TopTabHeight : Integer read FTopTabHeight write SetTopTabHeight;
    property TopTabWidth  : Integer read FTopTabWidth write SetTopTabWidth;
    property TabsVisible : Boolean read FTabsVisible write SetTabsVisible;
    property NumberPages : Integer read GetNumberPages;
    property ActivePageIndex : Integer read SelectedPageIndex;
  end;

procedure Register;

implementation

procedure Register;
begin
  RegisterComponents('Touch', [TTouchPageControl]);
end;

{ TTouchPageControl }

function TTouchPageControl.AddPage(PageName,PCaption: String; UsesIndex: Boolean): Integer;
var
NewPage :TTabSheet;
begin
BeginUpdate;
Result := -1;
NewPage := TTabSheet.Create(Self);
with NewPage do
     begin
     Name := PageName;
     Caption := PCaption;
     TabVisible := True;
     PageControl := TabPageControl;
     align := alClient;
     Result := TabPageControl.PageCount-1;
     if UsesIndex then Tag := 1 else Tag := 0;
     end;
TabPageControl.ActivePageIndex := Result;
IndexTop := TabPageControl.ActivePage.Top;
EndUpdate;
end;

procedure TTouchPageControl.ClearPages;
begin

end;

constructor TTouchPageControl.Create(AOwner: TComponent);
begin
  inherited;
  Height := 100;
  Width := 200;
  BevelOuter := bvNone;
  BevelInner := bvNone;
  TabPageControl  := TPageControl.Create(Self);
  with TabPageControl do
       begin
       Parent := Self;
       Align := alNone;
       BevelInner := bvNone;
       BevelOuter := bvNone;
       MultiLine := True;
       TabPageControl.OnChange := PageChangeHandler;
       end;

  ShowTabsButton  := TTouchGlyphSoftkey.Create(Self);
  with ShowTabsButton do
       begin
       Parent := BottomPanel;
       BevelInner := bvSpace;
       Top := 2;
       Width := 50;
       KeyStyle := ssElipse;
       ButtonColor := clRed;
       Legend := 'Tabs';
       Layout := slCentreOverlay;
       ButtonTravel := 1;
       AutoTextSize := true;
       with LegendFont do
            begin
            Style := [fsBold];
            Name := 'Comic Sans MS';
            Color := clYellow;
            TextToEdgeBorder := 9;
            end;
       Visible := False;
       OnMouseUp := ShowTabsMouseUp;
       Visible := False;
       end;

SetTabsVisible(False);
SetTopTabHeight(50);
Height := 300;
Width := 400;
FTopTabHeight := 24;
SetContainedComponent(TabPageControl);
Resize;
end;

destructor TTouchPageControl.Destroy;
begin
  inherited;
end;


function TTouchPageControl.GetIndexStrings: TStrings;
begin

end;

function TTouchPageControl.GetNumberPages: Integer;
begin
Result := TabPageControl.PageCount;
end;

function TTouchPageControl.GetPageControl: TPageControl;
begin
if assigned(TabPageControl) then
   begin
   Result := TabPageControl;
   end
else
   begin
   Result := nil;
   end

end;

procedure TTouchPageControl.DoOnHideControls;
begin
  inherited;
  ShowTabsButton.Visible := False;
  TabsVisible := False;
end;


procedure TTouchPageControl.DoOnShowControls;
begin
  inherited;
  ShowTabsButton.Visible := True;


end;

procedure TTouchPageControl.PageChangeHandler(Sender: TObject);
var
ActPage : TTabsheet;
begin
if TabPageControl.PageCount > 0 then
   begin
   if TabPageControl.ActivePage.Tag > 0 then
      begin
      IndexTop := TabPageControl.ActivePage.Top;
      end;
   end;

end;

procedure TTouchPageControl.Resize;
begin
  inherited;
  if not assigned(TabPageControl) then exit;
  TabPageControl.Height := Height-BottomPanel.Height;
  if IndexVisible then
     begin
     TabPageControl.Width := Width-IndexPanel.Width-5;
     TabPageControl.Left := IndexPanel.Width+1;
     end
  else
      begin
      TabPageControl.Width := Width-5;
      TabPageControl.Left := 1;
      end;
  ShowTabsButton.Left := BottomPanel.Width - ShowTabsButton.Width -70;
  if ShowTabsButton.Visible then
       begin
       ShowTabsButton.Height := ControlPanelHeight-4;
       end;

end;

procedure TTouchPageControl.ResizeIndexForm;
begin
if TabPageControl.PageCount > 0 then
   begin
   if TabPageControl.ActivePage.Tag = 1 then
      begin
      IndexTop := TabPageControl.ActivePage.Top;
      end
   end;
end;

procedure TTouchPageControl.SetTabsVisible(const Value: Boolean);
begin


  FTabsVisible := Value;

  if not (HandleAllocated) then exit;
  if FTabsVisible then
     begin
     TabPageControl.TabHeight := FTopTabHeight;
     TabPageControl.TabWidth := 0;
     end
  else
      begin
      TabPageControl.TabHeight := 1;
      TabPageControl.TabWidth := 1;
      end;
ResizeIndexForm;
end;

procedure TTouchPageControl.SetTopTabHeight(const Value: Integer);
begin
  if assigned(Parent) then
     begin
     FTopTabHeight := Value;
     TabPageControl.TabHeight := FTopTabHeight;
     end;
end;

procedure TTouchPageControl.SetTopTabWidth(const Value: Integer);
begin
  FTopTabWidth := Value;
end;




procedure TTouchPageControl.ShowTabsMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
BeginUpdate;
TabsVisible := not TabsVisible;
if TabsVisible then
   begin
   ShowTabsButton.ButtonColor := clYellow;
   ShowTabsButton.LegendFont.Color := clRed;
   end
else
   begin
   ShowTabsButton.ButtonColor := clRed;
   ShowTabsButton.LegendFont.Color := clYellow;
   end;
EndUpdate;
end;


function TTouchPageControl.PageByName(PageName: String): TTabSheet;
var
PageNumber : Integer;
Found      : Boolean;

begin
Found := False;
PageNumber := 0;
Result := nil;
while (not Found) and (PageNumber<NumberPages) do
    begin
    try
    if TabPageControl.Pages[PageNumber].Name = PageName then
       begin
       Found := True;
       Result := TabPageControl.Pages[PageNumber];
       end
    finally
    inc(PageNumber);
    end;
    end;
end;
end.
