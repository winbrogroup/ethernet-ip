unit CheckSelect;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls;

type
  TCheckSelect = class(TCustomCheckBox)
  private
    { Private declarations }
  protected
    { Protected declarations }
  public
    { Public declarations }
  published
    { Published declarations }
    Property Caption;
    Property State;
    Property Checked;



    Procedure Click; override;
    Procedure Toggle; override;
  end;

procedure Register;

implementation

Procedure TCheckSelect.Click;
begin
inherited;
end;

Procedure TCheckSelect.Toggle; 
begin
inherited;
end;


procedure Register;
begin
  RegisterComponents('G3D', [TCheckSelect]);
end;

end.
