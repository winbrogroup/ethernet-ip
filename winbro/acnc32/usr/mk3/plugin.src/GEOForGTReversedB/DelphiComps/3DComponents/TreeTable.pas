unit TreeTable;

interface
uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls,commctrl,Menus,extctrls,g3DObjects,G3DPaper,G3DDefs,G3DDisplay,GReportPoint,GMath3D,
  VectorMaths,Math,iniFiles,G3Math2d;


  type
  TTableEntry = record

     GObjectName: String;
     GObject    : ^TG3dObject;
     Nodeid     : HTREEITEM;
     ChildList  : array of ^TG3dObject;
     IsAxis     : Boolean;
     end;

  TTreeTable = class(TObject)
  private
  procedure ListAllNodeObjects(Tree : TTreeView;BaseNode : TTreeNode; NList : TList);
  procedure ListAllNodes(Tree : TTreeView;BaseNode : TTreeNode; NList : TList);

  protected
  public
  ObjectTable : Array of TTableEntry;
  WorldIndex  : Integer;
  ObjectCount : Integer;
  procedure BuildTableFrom(TreeView : TTreeView;Node : TTreeNode);
  function TableIndexforObjectName(ObjectName : String): Integer;
  procedure ClearObjects;


  end;

implementation

procedure TTreeTable.BuildTableFrom(TreeView : TTreeView;Node : TTreeNode);
var
ObjectList : TList;
NodeList   : TList;
NodeCount  : Integer;
LocalObjectCount : Integer;
begin
if not assigned (TreeView) then exit;
SetLength(ObjectTable,0);
ObjectList := TList.Create;
NodeList := TList.Create;
try
ListAllNodes(TreeView,Node,NodeList);
SetLength(ObjectTable,NodeList.Count);
For NodeCount := 0 to NodeList.count-1 do
   begin
   Node := NodeList[NodeCount];
//   if assigned (TG3DObject(Node.Data)) then ObjectTable[NodeCount].GObjectName := TG3DObject(Node.Data).Name;
   ObjectTable[NodeCount].GObjectName := Node.Text;
   ObjectTable[NodeCount].GObject := Node.Data;
   ObjectTable[NodeCount].Nodeid := Node.ItemId;

   if Node.Text = 'World' then
      begin
      WorldIndex := NodeCount;
      end;
   ObjectList.clear;
   ListAllNodeObjects(TreeView,Node,ObjectList);
   if Node.Text = 'World' then
      begin
      ObjectCount := OBjectList.Count;
      end;
   SetLength(ObjectTable[NodeCount].ChildList,ObjectList.Count);
   For LocalObjectCount := 0 to ObjectList.Count-1  do
       begin
       ObjectTable[NodeCount].ChildList[LocalObjectCount] := Objectlist[LocalObjectCount];
       end;
   end;
finally
ObjectList.Free;
NodeList.Free;
end;
end;


// This creates a list of pointers to all objects below a node (including the node)
// in REVERSE order for updating position etc
// Note this is iterative
procedure TTreeTable.ClearObjects;
begin
SetLength(ObjectTable,0);
ObjectCount := 0;
end;


procedure TTreeTable.ListAllNodeObjects(Tree : TTreeView;BaseNode : TTreeNode; NList : TList);
// NB internal procedure to add a node entry to the list.
var
NewNode     : TTreenode;
begin
if BaseNode.HasChildren then
      begin
      NewNode := BaseNode.GetFirstChild;
      ListAllNodeObjects(Tree,NewNode,NList);
      NewNode := BaseNode.GetNextChild(NewNode);
      while assigned(NewNode) do
         begin
         ListAllNodeObjects(Tree,NewNode,NList);
         NewNode := BaseNode.GetNextChild(NewNode);
         end
      end;
//else
    begin
    // This is the end of the branch so just add this to the list
    if assigned(BaseNode.Data) then NList.Add(BaseNode.Data);
    end;
end;

// This creates a list of pointers to all nodes below a node (including the node)
// in REVERSE order for updating position etc
procedure TTreeTable.ListAllNodes(Tree : TTreeView;BaseNode : TTreeNode; NList : TList);
var
NewNode     : TTreenode;
begin
with Tree do
begin
if BaseNode.HasChildren then
      begin
      NewNode := BaseNode.GetFirstChild;
      ListAllNodes(Tree,NewNode,NList);
      NewNode := BaseNode.GetNextChild(NewNode);
      while assigned(NewNode) do
         begin
         ListAllNodes(Tree,NewNode,NList);
         NewNode := BaseNode.GetNextChild(NewNode);
         end
      end;
//else
    begin
    // This is the end of the branch so just add this to the list
    NList.Add(BaseNode);
    end;
end;
end;


function TTreeTable.TableIndexforObjectName(ObjectName : String): Integer;
var
ListIndex : Integer;
Found : Boolean;
TestName : String;
begin
Result := -1;
Found := False;
ListIndex := 0;
TestName := UpperCase(ObjectName);
while (ListIndex < Length(ObjectTable)) and not Found do
    begin
    if UpperCase(ObjectTable[ListIndex].GObjectName) = TestName then
      begin
      Result := ListIndex;
      Found := True;
      end;
    inc(ListIndex);
    end;
end;

end.
