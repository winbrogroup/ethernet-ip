unit G3DDefs;

interface
type

//*************************************************
// 3D3D 3D 3D 3D 3D 3D 3D 3D 3D 3D 3D 3D 3D 3D 3D
//*************************************************
TPLocation = record
           x : Double;
           y : Double;
           z : Double;
           end;
TSegType = (stLine,stArc);
T3DObjects =(t3oAbstract,t3oLine,t3oBox,t3oDisc,t3oReportPoint,t3oReportVector,t3oPartDatum,t3oToolDatum,t3oELectrodeNode,t3oNDPDatumNode,t3oHoleNode,t3oNominalPartDatum);
TUnits = (uInch,uMetric);
TDesignModes = (dmEditAbs,dmEditInc,dmRuntimeAbs,dmRuntimeInc);
TUserModes   =(umEditor,umRuntime);
//TTreeUpdateSwitch = (tuUpdateTreelist,tuDontUpdateTreelist);
TPLimit = record
             xPlus : Double;
             xMinus: Double;
             yPlus : Double;
             yMinus: Double;
             zPlus : Double;
             zMinus: Double;
             end;

TScreenLocation = record
             x : Integer;
             y : Integer;
             end;
             
TMovePermission = record
                x: Boolean;
                y: Boolean;
                z: Boolean;
                a: Boolean;
                b: Boolean;
                c: Boolean;
                end;
TIntLocation = record
             x : Integer;
             y : Integer;
             z : Integer;
             end;

TRotation = record
            x : Double;
            y : Double;
            z : Double;
            end;

TPositionData = record
        DesignTimeTranslation     : TPLocation;       //Design Position Relative to Parent
        DesignTimeRotation        : TRotation;        //Design Position Relative to Parent
        AbsoluteTranslation       : TPLocation;
        AbsoluteRotation          : TRotation;
        COR                       : TPLocation;       // Centre of Rotation relative to parent (Design Time)
//        RelTransLimits            : TPLimit;          // Limits of Allowable runtime Linear movements relative to parent
//        RelRotateLimits           : TPLimit;          // Limits of Allowable runtime Rotary movements relative to parent
        OParent                   : Pointer;
        end;


TRelativePosition = record
        Translation : TPlocation;
        Rotation    : TRotation;
        end;

TRectVerts = array[0..3] of TIntLocation;
TBoxVerts  = array[0..7] of TPlocation;
TDiscVerts  = array[0..35] of TPlocation;


TBox = record
             Top1 : TIntLocation;
             Top2 : TIntLocation;
             Top3 : TIntLocation;
             Top4 : TIntLocation;
             Bottom1 : TIntLocation;
             Bottom2 : TIntLocation;
             Bottom3 : TIntLocation;
             Bottom4 : TIntLocation;
             end;




TLineSegment = record
             Point1 : TIntLocation;
             Point2 : TIntLocation;
             ControlNode : TIntLocation;
             SegmentType : TSegTYpe;
             end;

{TPrecisePoint = record
              x : Double;
              y : Double;
              end;}

TExtents      = record
              MaxX :Integer;
              MaxY :Integer;
              MinX : Integer;
              MinY : Integer;
              OffCentreX : Double;
              OffCentreY : Double;
              Centre :TScreenLocation;
              end;


TCircle12   = array[0..11] of TIntLocation;
TPCircle12   = array[0..11] of TPLocation;
TPCircle18   = array[0..17] of TPLocation;


//*************************************************
// 2D 2D 2D 2D 2D 2D 2D 2D 2D 2D 2D 2D 2D 2D 2D 2D
//*************************************************

FanPoint =  record
      x  : Integer;
      z  : Integer;
      end;

{      TPLocation = record
           x : Double;
           y : Double;
           z : Double;
           end;}





TCircleIntersectType = (tciNone,tciTangent,tciTwopoints);



// Formula for a line is x=Ny+c => X = YVal(Y) + Shift
TLineFormula = record
             Yval : Double;
             Shift: Double;
             isX  : Boolean;
             isY  : Boolean;
             XVal : Double;
             end;


TPrecisePoint = record
              x : Double;
              y : Double;
              end;


TCircleFormula = record
               Centre  : TPrecisePoint;
               Radius  : Double;
               end;

TPointDisplayStyle = (pdsDefault,pdscross,pdsdiagcross,pdspointsqaureselect,pdsSinglepoint);


implementation
end.
