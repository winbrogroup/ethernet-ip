unit G3DReportVector;

interface


uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, GMath3D,G3DDefs,G3DPaper, StdCtrls,G3DObjects;

type
// A 3d Line has a reference point, rotation, translation and length
// A ReportVector has all of above plus a point at each end of the line.
// The ReportVector can be adjusted not only by modifying the normal line parameters
// but also be changing the location of either or both of the two end points

  TG3DReportVector = class(TG3DObject)


  private
  FLength       : Double;
  FPoint1       : TPlocation;
  FPoint2       : TPlocation;
  procedure SetLineLength(value : Double);
  procedure CreateRealPoints; override;
  function GetPoint1:TPlocation;
  function GetPoint2:TPlocation;

  public
  procedure CreateDatumPoints(NoRotate : Boolean); override;
  constructor create(AOwner : Tcomponent); override;
  procedure DrawOn(Surface : TG3DPaper;DisplayMode : TDisplayModes); override;
  procedure MakeDescription(var Description: TStringList;Mode : TDescriptionMode); override;

  published
  property Length     : Double read FLength write SetLineLength;
  property Point1     : TPLocation read GetPoint1;
  property Point2     : TPLocation read GetPoint2;

  procedure MakeFromPoints(Point1,Point2 : TPlocation);
  end;

procedure Register;

implementation
constructor TG3DReportVector.Create(AOwner : TComponent);
begin
inherited;
FObjectType :=  t3oReportVector;
// Unlike the 3DLine, the report vevtor creates its endpoints even in virtual mode
// Therefor the create real points routine does nowt.
FLength := 100.0;
NumberVerteces := 2;
SetLength(Verteces,2);
SetLength(ScreenPoint,2);
CreateDatumPoints(False);
SetCORtoDatum;
end;

procedure TG3DReportVector.DrawOn(Surface : TG3DPaper;DisplayMode : TDisplayModes);
begin
inherited;
if not assigned(Surface) then exit;
if FVirtualOnly then exit;
if (DisplayMode = dmVirtual) then exit;
CalculateScreenPoints(Surface);
with Surface.Offscreen.Canvas do
        begin
        MoveTo(ScreenPoint[0].x,ScreenPoint[0].y);
        LineTo(ScreenPoint[1].x,ScreenPoint[1].y);
        end;
end;

procedure TG3DReportVector.SetLineLength(Value : Double);
begin
FLength  := Value;
end;



procedure TG3DReportVector.CreateDatumPoints(NoRotate : Boolean);
begin
if not NoRotate then inherited;
// First Calculate Points aligned to x,y,z
Verteces[0] := SetPrecisePoint3D(FReferencePoint.x,FReferencePoint.y,FReferencePoint.z-(FLength/2.0));
Verteces[1] := SetPrecisePoint3D(FReferencePoint.x,FReferencePoint.y,FReferencePoint.z+(FLength/2.0));
// Now rotate all points relative to DatumRotation
if Not NoRotate then
   begin
   if not RotationIsZero(FPositionData.AbsoluteRotation) then
      begin
      RotatePointAboutReference(Verteces[0],FPOsitionData);
      RotatePointAboutReference(Verteces[1],FPOsitionData);
      end;
   end
else
   begin
   FpositionData.AbsoluteRotation := SetPRotation(0,0,0);
   end;
end;


procedure TG3DReportVector.MakeDescription(var Description: TStringList;Mode : TDescriptionMode);
begin
inherited   MakeDescription(Description,Mode);
Description.Add(Format('Length=%f',[FLength]));


end;


procedure TG3DReportVector.CreateRealPoints;
begin
//Real points created in virtual and daw mode during construction
end;

procedure TG3DReportVector.MakeFromPoints(Point1,Point2 : TPlocation);

var
NewRotation : TRotation;
NewLength   : Double;
NewReferncePoint : TPlocation;
NewPoint         : TPlocation;
begin
NewRotation := Anglesbetween3DPoints(Point1,Point2);
NewLength := PPreciseLineLength3D(Point1,Point2);
NewReferncePoint:= PPreciseMidPoint3D(Point1,Point2);
SetReferencePoint(NewReferncePoint);
FpositionData.AbsoluteRotation := NewRotation;
FLength := NewLength;
//FPositionData.DesignTimeRotation := NewRotation;

end;




function TG3DReportVector.GetPoint1:TPlocation;
begin
Result := Verteces[0];
end;

function TG3DReportVector.GetPoint2:TPlocation;
begin
Result := Verteces[1];
end;



procedure Register;
begin
  RegisterComponents('G3D', [TG3DReportVector]);
end;


end.
