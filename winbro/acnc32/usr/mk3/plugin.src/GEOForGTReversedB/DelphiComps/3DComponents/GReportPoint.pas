unit GReportPoint;

interface
uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, GMath3D,G3DDefs,G3DPaper, StdCtrls,G3DObjects;
const
PRad = 25;
type
  TG3dReportPoint = class(TG3DObject)
  private
  FRpType : TRPointType;
  FLinkPoint  : TG3DReportPoint;
  FVectorOwner : TVectorOwner;
  FPointStyle: TPointDisplayStyle;
  protected
  procedure CreateRealPoints; override;

  public
  property PointType : TRPointType read FRPType write FRpType;
  property VectorOwner : TVectorOwner read FVectorOwner write FVectorOwner;
  property LinkPoint : TG3dReportPoint read FLinkPoint write FLinkPoint;
  property PointStyle : TPointDisplayStyle read FPointStyle write FPointStyle;
  procedure CreateDatumPoints(NoRotate : Boolean); override;
  constructor create(AOwner : Tcomponent); override;
  procedure DrawOn(Surface : TG3DPaper;DisplayMode : TDisplayModes); override;
  procedure MakeDescription(var Description: TStringList;Mode : TDescriptionMode); override;
  published
//  property Verteces   : TBoxVerts read FVerteces ;
  end;

procedure Register;

implementation
constructor TG3DReportPoint.Create(AOwner : TComponent);
begin
inherited;
FObjectType :=  t3oReportPoint;
FRpType := rpNormal;
FVectorOwner := voNone;
NumberVerteces := 0;
//SetLength(Verteces,5);
SetLength(ScreenPoint,3);
CreateDatumPoints(False);
SetCORtoDatum;
FShowRef := True;
end;

procedure TG3DReportPoint.DrawOn(Surface : TG3DPaper;DisplayMode : TDisplayModes);
var
BufferPenWidth : Integer;
begin
inherited;
 BufferPenWidth := 1;
if not assigned(Surface) then exit;
if (DisplayMode = dmVirtual) then exit;
if FVirtualOnly then exit;
CalculateScreenPoints(Surface);
try
with Surface.Offscreen.Canvas do
   begin
   BufferPenWidth := Pen.Width;
   if Selected then Pen.Color := clRed else Pen.Color := FColour;
   if FPointDisplayOnly then
      begin
      case PointStyle of
         pdsDefault:
           begin
           Pen.Width := 3;
           MoveTo(RefScreenPoint.x,RefScreenPoint.y);
           LineTo(RefScreenPoint.x,RefScreenPoint.y);
           end;
         pdscross:
           begin
           MoveTo(RefScreenPoint.x,RefScreenPoint.y+2);
           LineTo(RefScreenPoint.x,RefScreenPoint.y-3);
           MoveTo(RefScreenPoint.x+2,RefScreenPoint.y);
           LineTo(RefScreenPoint.x-3,RefScreenPoint.y);
           end;
         pdsdiagcross:
           begin
           MoveTo(RefScreenPoint.x+2,RefScreenPoint.y+2);
           LineTo(RefScreenPoint.x-3,RefScreenPoint.y-3);
           MoveTo(RefScreenPoint.x-2,RefScreenPoint.y+2);
           LineTo(RefScreenPoint.x+3,RefScreenPoint.y-3);
           end;
         pdsSinglePoint:
           begin
           Pen.Color := clwhite;
           Pen.Width := 3;
           MoveTo(RefScreenPoint.x,RefScreenPoint.y);
           LineTo(RefScreenPoint.x,RefScreenPoint.y);
           if Selected then Pen.Color := clRed else Pen.Color := FColour;
           Pen.Width := 2;
           MoveTo(RefScreenPoint.x,RefScreenPoint.y);
           LineTo(RefScreenPoint.x,RefScreenPoint.y);
           end;
         pdspointsqaureselect:
           begin
           if Selected then
           MoveTo(RefScreenPoint.x+3,RefScreenPoint.y+3);
           LineTo(RefScreenPoint.x+3,RefScreenPoint.y-3);
           LineTo(RefScreenPoint.x-3,RefScreenPoint.y-3);
           LineTo(RefScreenPoint.x-3,RefScreenPoint.y+3);
           LineTo(RefScreenPoint.x+3,RefScreenPoint.y+3);
           end;
         end;

      end
   else
       begin
       if FRpType = rpSecondVector then Pen.color := clSilver;
       MoveTo(ScreenPoint[0].x,ScreenPoint[0].y);
       LineTo(ScreenPoint[1].x,ScreenPoint[1].y);
       LineTo(ScreenPoint[2].x,ScreenPoint[2].y);
       LineTo(ScreenPoint[0].x,ScreenPoint[0].y);
       if FRPType = rpHolePoint then
          begin
          Brush.color := clYellow;
          Floodfill(ScreenPoint[2].x,ScreenPoint[2].y-3,Pen.Color,fsBorder);
          end;
       if FRPType = rpElectrodeDatum then
          begin
          Brush.color := clRed;
          Floodfill(ScreenPoint[2].x,ScreenPoint[2].y-3,Pen.Color,fsBorder);
          end;
       if Selected then
          begin
          Brush.color := FSelectColour;
          Floodfill(ScreenPoint[2].x,ScreenPoint[2].y-3,Pen.Color,fsBorder);
          end;
       if (FRpType = rpSecondVector){ or (FRpType = rpFirstVector) }then
          begin
          Pen.Width := 2;
          if FSelected then
             begin
             Pen.Color := clred;
             end
          else
              begin
              case FVectorOwner of
                   voActual     : Pen.Color := clGreen;
                   voNominal    : Pen.Color := $0022aaff; // Orange
                   voNone       : Pen.Color := clBlack;
                   voApproach   : Pen.Color := clSkyBlue;
                   voNDP        : Pen.Color := clAqua;
                   end;
              end;
          MoveTo(ScreenPoint[2].x,ScreenPoint[2].y);
          LineTo(LinkPoint.screenPoint[2].x,LinkPoint.screenPoint[2].y);
          Pen.Width := 1;
          end;
        end;
   end;
  finally
  Surface.Offscreen.Canvas.Pen.Width := BufferPenWidth;
  end;

end;

procedure TG3DReportPoint.CreateDatumPoints(NoRotate : Boolean);
begin
if not NoRotate then inherited;

 FpositionData.AbsoluteRotation := SetPRotation(0,0,0);
end;


procedure TG3DReportPoint.MakeDescription(var Description: TStringList;Mode : TDescriptionMode);
begin
inherited   MakeDescription(Description,Mode);
end;

procedure TG3DReportPoint.CreateRealPoints;
begin
NumberVerteces := 0;
//SetLength(Verteces,5);
SetLength(ScreenPoint,3);
CreateDatumPoints(False);
SetCORtoDatum;
FShowRef := True;
end;



procedure Register;
begin
  RegisterComponents('G3DOpen', [TG3DReportPoint])
end;


end.
