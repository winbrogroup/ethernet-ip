unit G3DOTree;
{
V1.1 1/6/2001 Corrected Get axis offsets to pass back C axis position even if IncludeCaxis is not set
Note:
 Rotary Axis strokes
 From Front of Machine.  B = 0 has tool vertical to left of B centrline
                         Negative rotation is clockwise
                         Stroke -60 to + 240
 From Left of machine
                         A = 0 has tool vertical (with B at 0)
                         Negative rotation is Clockwise
                         Stroke -210 to +30
V2.0.0 2/4/2014 B axis is reversed !!!!

{ TODO : make sure BAxis  limits are set in machine thewse are checked by transpose B 

}
interface
{$R  TreeImages.res}
uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls,commctrl,Menus,extctrls,g3DObjects,G3DPaper,G3DDefs,G3DDisplay,GReportPoint,GMath3D,
  VectorMaths,Math,iniFiles,G3Math2d,Treetable,G3dBox,CNCTypes,IStringArray;

const
NumObjectPopItems=2;
ReportPointSelectColour = clGreen;
ToolApproachDisplayLength = 100;
NDPErrorLimt = 0.0002;
MaxIterations = 500;

resourcestring
NominalVectorName       = 'NominalVector';
NVTimingName            = 'NVTiming';
ActualVectorName        = 'ActualVector';
AVTimingName            = 'AVTiming';
PartDatumID             = 'PartDatum';
NomPartDatumID          = 'NominalPartDatum';
ToolDatumID             = 'ToolDatum';
WorldNodeID             = 'World';
NDPName                 = 'NDPVector';
InvNDPName              = 'InvNDPVector';
ApproachVectorName      = 'ApproachV';
AttachElectrodeName     = 'AttElect';

type
TBallDescriptor = record
 XPlusRad : Double;
 YPlusRad : Double;
 XMinusRad : Double;
 YMinusRad : Double;
 ZPlusRad : Double;
 ZMinusRad : Double;
 end;

TAlignmentData = record
  PartVectorRotation :TRotation;
  ApproachVectorRotation :TRotation;
  RotationDelta : TRotation;
  AllAxesAligned : Boolean;
end;


TNDPErrorTypes = (ndpOk,ndpNoPartVector,
                  ndpRequiredXOutsidePosLimits,
                  ndpRequiredXOutsideNegLimits,
                  ndpRequiredYOutsidePosLimits,
                  ndpRequiredYOutsideNegLimits,
                  ndpRequiredZOutsidePosLimits,
                  ndpRequiredZOutsideNegLimits,
                  ndpRequiredAOutsidePosLimits,
                  ndpRequiredAOutsideNegLimits,
                  ndpRequiredBOutsidePosLimits,
                  ndpRequiredBOutsideNegLimits,
                  ndpRequiredCOutsidePosLimits,
                  ndpRequiredCOutsideNegLimits,
                  ndpItterateError,
                  ndpDRillVectVertical);

TStackTransform = record
 XYZShift : TPLocation;
 Rotation : TRotation;
 end;

TAxisDestintation = record
  Position : Double;
  Move     : Boolean;
  end;
TAxisMoveArray = array[0..11] of TAxisDestintation;

EGEOError = class(Exception);
TUpdateMode = (umAll,umCustomOnly);
TReportPointType= (rpPartRelative,rpToolRelative,rpMachineRelOnPartDatum,rpNPDRelative,rpToolApproach,rpFixedPartRelative);
TTreeErrors = (teDataOK,teNoPartDatum,teNoToolDatum,teAxisSetupDataError);

  TOffsetResult = record
    X,Y,Z,A,B,C : Double;
    END;

  TAxisDefinition = record
         ObjectName : String;
         isLinear   : Boolean;
         MotionAxisIndex  : Integer;
         isReversed : Boolean;
         AffectsPartDatum : Boolean;
         AffectsToolDatum : Boolean;
         DatumTranslation : TPlocation;
         DatumRotation    : TRotation;
         DisplayTextIndex : Integer;
         AxisPosition     : Double;
         AxisObject       : TG3dObject;
         end;

  TReportPointPosition = record
         ReportPointType : TReportPointType;
         NodeId          : HTREEITEM;
         CurrentPosition : TPlocation;
         end;

  THolePointPosition = record
         ReportPointType : TReportPointType;
         NodeId          : HTREEITEM;
         CurrentPosition : TPlocation;
         end;

  pHoleEntry = ^THolePointPosition;

  TG3DOTree = class(TTreeView)
  private
  NDPItterations : Integer;
  AOK,BOK : Boolean;
  RetryCount : Integer;
  { Private declarations }
  ObjectPopUp : TPopUpMenu;
  ObjectPopList : array[0..NumObjectPopItems-1] of TMenuItem;
  WorldNode : TTreeNode;
  FNumberofAxes          : Integer;
  FUserMode              : TUserModes;
  FDisplayMode           : TDisplayModes;
  FPartDatumLocation     : TPLocation;
  FToolDatumLocation     : TPLocation;
  FPartDatumNode         : TTreeNode;
  FFixedPartDatumNode    : TTreeNode;
  FToolDatumNode         : TTreeNode;
  FElectrodeNode         : TTreeNode;
  FHoleNode              : TTreeNode;
  FNDPDatumNode          : TTreeNode;
  FDebug                 : String;
  FCorrectionStartPosn   : TAxisPosn;

  AbsolutePartDatum        : TPlocation;
  AbsoluteToolDatum        : TPlocation;
  AbsoluteNominalPartDatum : TPlocation;
  FPartDatumStandardPos    : TPlocation;


  FDebugReported       : TNotifyEvent;
  XtoNom,YToNom,ZToNom  : Double;
  XtoRotated,YToRotated,ZToRotated  : Double;

  FExtentsOverScale      : Double;
  FEditorMode             : Boolean;
  FAVFixed: Boolean;
  FAppliedStandoff: Double;
  LoadedMachineFile : String;

  procedure RotateObjectandChildrenAtIndexAboutX(Index: Integer;Rotation : Double;COR : TPLocation;DesignMode : TDesignModes);
  procedure RotateObjectandChildrenAtIndexAboutY(Index: Integer;Rotation : Double;COR : TPLocation;DesignMode : TDesignModes);
  procedure RotateObjectandChildrenAtIndexAboutZ(Index: Integer;Rotation : Double;COR : TPLocation;DesignMode : TDesignModes);
  procedure RotateObjectandChildrenAtIndex(Index: Integer;Rotation : TRotation;COR : TPLocation;DesignMode : TDesignModes);
  procedure TranslateObjectandChildrenAtIndexAboutX(Index: Integer;Translation: Double;DesignMode : TDesignModes);
  procedure TranslateObjectandChildrenAtIndexAboutY(Index: Integer;Translation: Double;DesignMode : TDesignModes);
  procedure TranslateObjectandChildrenAtIndexAboutZ(Index: Integer;Translation: Double;DesignMode : TDesignModes);
  procedure TranslateObjectandChildrenAtIndex(Index: Integer;Translation : TPLocation;DesignMode : TDesignModes);
  procedure SetNumberofAxes(Value : Integer);
  procedure SetUserMode(Value : TUsermodes);
  procedure SetAxisPosition(AxisNumber : Integer;Position : Double);
  function  GetAxisPosition(AxisNumber : Integer): Double;
  function  isValidAxis(AxisNumber : Integer):Boolean;
  function  ReportPointExtentsOnSurface(Paper : TG3DPaper;var Centre :TScreenLocation):Textents;
  function  ReportPointExtentsOnSurfaceRanged(Index1,Index2: Integer;Paper : TG3DPaper;var Centre :TScreenLocation):Textents;

  function GetNumberReportPoints:Integer;
  function GetNumberReportVectors:Integer;

  procedure TreePopupDeleteClick(Sender : TObject);
  procedure TreeShowPointsClick(Sender : TObject);
  function GetCorrectedPositionRelativeTo(RPoint :TPLocation;DatumLocation : TPLocation):TPlocation;
  function DecodeFileLocation(FileEntry : String):TPLocation;
  function DecodeFileRotation(FileEntry : String):TRotation;
  function CalculatePartDatumRelativeLocationofPointInSpace(Position :TPLocation):TPLocation;
  procedure CreateNominalandActualVectors;
  function CalculateElectrodeXYZShift:TPlocation;



  function MakeObjectFromFile(FileName :TIniFile;NodeName : String;MakeVirtual : Boolean):TG3DObject;
    procedure SetExtentsOverscale(const Value: Double);
    function GetObjectCount: Integer;
    function Normalise180(RotPos: Double): Double;
    function Normalise360(RotPos: Double): Double;
    function ComplimentAroundZero(RotPos: Double): Double;
    function PlusMinus360(RotPos: Double): Double;
    procedure FastSetAxisPosition(AxisNumber: Integer; Position: Double);
    function HOleExtentsOnSurface(Paper: TG3DPaper;
      var Centre: TScreenLocation;First,Last: Integer): Textents;
    procedure UpdateHoleListPOsition;
    function TransposeCRotation(var InC : Double;PosLim,NegLim : Double):Boolean;
    function TransposeARotation(var InA : Double;PosLim,NegLim : Double):Boolean;
    function TransposeBRotation(var InB : Double;PosLim,NegLim : Double):Boolean;
    function LinearCondition(LinearVal: Double): Double;
    function RotaryCondition(axIndex : Integer;RotaryVal: Double): Double;
    procedure CompensateVFunc(xyz, ABC: TXYZ; Ball: TBallDescriptor);
    function CheckAxisLimits(Position : TAxisPosn): TNDPErrorTypes;
    Function UpdateAxisAndVectorData(var AlignmentData : TAlignmentData;
                                           UseInverseApproach : Boolean):Boolean;
    Function DoABIterate(var APos: Double;
                         var BPos: Double;
                         PartVector : TVectorData;
                         UseInverseApproach : Boolean;
                         isReItterate : Boolean):Boolean;

    Function ItterateForAB(var APos: Double;
                                  var BPos: Double;
                                  PartVector : TVectorData;
                                  UseInverseApproach : Boolean;
                                  GetInverseofStartPos : Boolean;
                                  ElectrodeAngle : Double): Boolean;

  protected
    { Protected declarations }
    TreeTable            : TTreeTable;
    function GetReportVectorFromList(VectorNumber: Integer):TVectorData;
    function ZeroOffsets:TAxisPosn;
    procedure SetDebugReported(Value: TNotifyEvent);
    procedure DebugChanged;

  public
    { Public declarations }
    StackingAxisTransform : TStackTransform;
    SavedNDPOffsetLinear : TPLocation;
    SavedNDPOffsetRotary : TRotation;

    GlobalUpdateCount : Integer;
    AxisMoves : TAxisMoveArray;
    DebugStep : Boolean;
    DebugCount : Integer;

    InhibitTreeTableUpdate : Boolean;
    InhibitObjectListMatch : Boolean;
    AxisDefs               : array[0..8] of TaxisDefinition;
    FReportPointList       : Array of TReportPointPosition;
    FHoleList              : TList;
    FReportVectorList      : Array of TVectorData;
    FElectrodeOffset       : TPLocation;
    FElectrodeLocation     : TPlocation;
    FHoleLocation          : TPlocation;
    FCurrentHoleLocation   : TPlocation;
    FNDPOffset             : TPlocation;
    WorldObject            : TG3DBox;
    Zero3D                 : TPLocation;
    NominalVectorIndex     : Integer;
    ActualVectorIndex      : Integer;
    ATVectorIndex          : Integer;
    NTVectorIndex          : Integer;
    NDPVectorIndex         : Integer;
    InverseNDPVectorIndex  : Integer;
    ApproachVectorIndex    : Integer;
    AttachedElecIndex      : Integer;
    FPartDatumOffset       : TPlocation;
    FToolDatumOffset       : TPlocation;
    FXLimitsPositive       : Double;
    FXLimitsNegative       : Double;
    FYLimitsPositive       : Double;
    FYLimitsNegative       : Double;
    FZLimitsPositive       : Double;
    FZLimitsNegative       : Double;
    FALimitsPositive       : Double;
    FALimitsNegative       : Double;
    FBLimitsPositive       : Double;
    FBLimitsNegative       : Double;
    FCLimitsPositive       : Double;
    FCLimitsNegative       : Double;
    procedure Update3DDisplay(Display : TG3DDisplay;Mode : TUpdateMode); dynamic;
    procedure CreateFixedReportPoints;

    function GetAllAxisPositions:TAxisPosn;
    procedure SaveTreeToDisc(FName :String);
    procedure BuildObjectTable;
    function GetReportPointFromList(PointNumber: Integer):TPLocation;

    //// Procedures and functions for graphical editor
    function  GetJogAxis(AxisIndex: Integer):Integer;
    function  GetJogObject(AxisIndex: Integer;var Node : TTreeNode):Pointer;
    procedure SetObjectasAxisBase(ObjectNode : TTreeNode;AxisNumber :Integer);
    procedure DatumAxis(AxisIndex : Integer);
    procedure DatumAllAxes;
    procedure FastDatumAxes(AxisCount : Integer);
    procedure CentreObjectsOnSurface(Paper : TG3DPaper);
    procedure FitAllObjectsOnSurface(Paper : TG3DPaper);
    procedure FitToReportPoints(Paper : TG3DPaper);
    procedure FitToPointsBetweenIndexes(Index1, Index2: Integer;Paper: TG3DPaper);
    procedure FitToHolePoints(Paper : TG3DPaper;First,Last: Integer);

    procedure FitStackingAxes(Paper : TG3DPaper);



    procedure CentreObjectsOnReportPoints(Paper : TG3DPaper);
    procedure ClearAllObjects;
    procedure DrawAllObjectsOn(Paper : TG3DPaper;DoPaint :Boolean);
    procedure CentreOnDisplay(Paper : TG3DPaper);
    procedure MoveAllAxes(AxisCount : Integer;AxisArray : TAxisMoveArray);

    procedure RestoreAllAxisPOsitions(SavedPosn : TAxisPosn);
    procedure RedrawToExtents(Paper : TG3DDisplay);
    procedure RuntimePaint(Paper : TG3DPaper);
    procedure FixedCentreRescale(Paper : TG3DPaper;ScaleRatio : Double);

    // Tree Management
    function  AddObjectatNode(Node :TTreeNode;NewObject :T3DObjects;IsVirtual : Boolean): String;
    function  AddObjectatNode2(Node :TTreeNode;NewObject :T3DObjects;IsVirtual : Boolean
                              ;var NewNode: TTreeNode): Pointer;
    function  FindObjectListIndexFor(ObjectName : String):Integer;
    function  FindObject(Name : String):POinter;
    function  FindObjectandNode(Name : String; var Node: TTreeNode):POinter;
    function  NameExists(Name : String):Boolean;
    function  CreateChildNodeListFor(BaseNode : TTreeNode):TList;
    procedure ListAllNodeObjects(BaseNode : TTreeNode; NList : TList);
    procedure ListAllNodes(BaseNode : TTreeNode; NList : TList);
    procedure BuildAllObjectsAndChildren;
    procedure DeleteObjectandChildrenAtNode(Node: TTreeNode);
    procedure FastDeleteObjectandChildrenAtNode(Node: TTreeNode);

    procedure UpdateObjectAndChildrenAtNode(Node: TTreeNode);
    function  AllObjectExtentsOnSurface(Paper : TG3DPaper;var Centre :TScreenLocation):TExtents;
    function LinearCorrected(Position: TPlocation):TPlocation;
    function GTLinearCorrected(Position: TPlocation):TPlocation;
    function RenameSelected(NewName : String):Boolean;
    procedure MatchObjectListToTree;
    procedure ConditionalMatchObjectListToTree; // depedent on GlobalUpdateCount

    procedure AdjustPartDatumBy(Translation : TPLocation;Rotation : TRotation);
    procedure AdjustFixedPartDatumBy(Translation: TPLocation;Rotation: TRotation);

    procedure ResetActualVectorToNominal;



    // Point and Vector Management

    // Points
    function  AddReportPointat(x,y,z : Double;PointName : String;RPType : TReportPointType;
                               LinearCorrect : Boolean;DisplayPointOnly : Boolean): Integer;
    function  AddHoleToHoleListat(x,y,z : Double;PointName:String;LinearCorrect: Boolean;
                    PColour :TColor;
                    PStyle :TPointDisplayStyle): Integer;

    procedure DeleteAllHolePoints;

    procedure DeleteReportPoint(Index: Integer);
    procedure FastDeleteReportPoint(Index: Integer);
//    function FastDeleteReportPointifDisplayOnly(Index: Integer):Boolean;


    function GetReportPointCalled(aName : String): TPlocation;
    procedure DeleteReportPointCalled(aName : String);
    procedure UpdateReportData(RebuildFirst : Boolean);
    procedure UpdateReportKnownDataPoints(RebuildFirst: Boolean);
    procedure UpdatePartDatumPosition;
    procedure UpdateToolDatumPosition;
    procedure DeleteRuntimeReportPoints;
    function  GetReportPointIndexfor(PointName : String):Integer;
    function GetReportPointName(PointNumber: Integer):String;
    procedure UpdateFixedIndexes;

    function  GetObjectListIndexfor(PointName : String):Integer;


    // Vectors
    function  AddReportVectorbetween(Point1,Point2 : TPlocation;VectorName : String;RVType : TReportVectorType):Integer;
    function  DeleteVectorCalled(VectorName : String):Boolean;
    function  GetVectorDifference(Vector1Index,Vector2Index: Integer;var Difference:TVectorDifference):Boolean;
    function  CopyVectoratIndex(Index : Integer;VectorName : String):Integer;
    function  GetVectorIndexfor(VectorName : String):Integer;
    function  GetVectorPoint(PointNum : Integer;VecData : TVectorData) : TG3DReportPoint;
    function  GetPointDataForIndex(Index : INteger) : TG3DReportPoint;
    function  GetPointDataPointCalled(PointName : String) : TG3DReportPoint;
    procedure FreezeVectorat(VIndex: Integer);
    procedure DeleteVectorat(Index: Integer);
    procedure UnFreezeVectorat(VIndex: Integer);
    procedure DeSelectAllVectors;
    procedure ShowSelectedVector(VIndex : Integer);
    procedure DeleteAllVectors;
    procedure SetNDPVectorPositionBetween(SurfacePoint,ApproachPoint : TPLocation);
    procedure MoveVectorAtIndex(Index : Integer; Point1Pos,Point2Pos : TPLocation);


//**************** Interface to Machine **********************************
    // Properties
    property NumberofAxes : Integer read FNumberofAxes write SetNumberofAxes; //**
    property AxisPosition[AxisIndex: Integer]: Double read GetAxisPosition  write SetAxisPosition;//**
    property UserMode : TUserModes read FUserMode write SetUserMode;
    property PartDatumPosition:TPlocation read FPartDatumLocation;
    property ToolDatumPosition:TPlocation read FToolDatumLocation;
    property ELectrodePosition : TPlocation read FElectrodeLocation; // **
    property HoleLocation     :TPlocation read FHoleLocation;
    property CurrentHoleLocation :TPlocation read FCurrentHoleLocation;
    property NDPOffset        :TPlocation read FNDPOffset;
    property ReportPointPosition[PointNumber: Integer]: TPLocation read GetReportPointFromList;
    property ReportPointName[PointNumber: Integer]: String read GetReportPointName;
    property NumberofReportPoints : Integer read GetNumberReportPoints;
    property NumberofReportVectors: Integer read GetNumberReportVectors;
    property ReportVectorData[PointNumber: Integer]: TVectorData read GetReportVectorFromList;
    property PartDatumNode : TTreeNode read FPartDatumNode;
    property NomPartDatumNode : TTreeNode read FFixedPartDatumNode;
    property ELectrodeNode: TTreeNode read FElectrodeNode;
    property HoleNode     : TTreeNode read FHoleNode;
    property ToolDatumNode : TTreeNode read FToolDatumNode write FToolDatumNode;
    property NDPDatumNode  : TTreeNode read FNDPDatumNode write FNDPDatumNode;
    property DisplayMode : TDisplayModes read FDisplayMode write FDisplayMode;
    property Debug : String read FDebug;
    property DebugReported : TNotifyEvent read FDebugReported write SetDebugReported;
    Property ExtentsOverscale : Double read FExtentsOverscale write SetExtentsOverscale;
    property EditorMode : Boolean read FEditorMode write FEditorMode;
    property ObjectCount : Integer read GetObjectCount;
    property ActualVectorisFixed : Boolean read FAVFixed write FAVFixed;
    property PartDatumOffset : TPlocation read FPartDatumOffset;
    property ToolDatumOffset : TPLocation read FToolDatumOffset;
    property AppliedStanfoff : Double read FAppliedStandoff;


/// *** dll INTERFACE ROUTINES
    function LoadTreeFromDisc(FName :String;LoadVirtual : Boolean):TTreeErrors;
    function NewGetAxisOffsets(var Stage :Integer; IncludeCAxis : Boolean;SingleStage : Boolean):TAxisPosn;
    function GetPointAtDatum(x,y,z:Double):TPlocation;
    procedure ClearAllOffsetsandVectors;
    procedure SetElectrodeOffsetToToolDatum(EPosition : TPLocation);
    procedure SetNominalStackingAxis(PX,PY,PZ,X2,Y2,Z2,TX,TY,TZ : Double);
    procedure SetActualStackingAxis(PX,PY,PZ,X2,Y2,Z2,TX,TY,TZ : Double);
    procedure AddVector(x1,y1,z1,x2,y2,z2 : Double;VType :TReportVectorType;VName : String);
    procedure AddActualStackingAxisRelativeVector(x1,y1,z1,x2,y2,z2 : Double;VType :TReportVectorType;VName : String);
    procedure TransformToStackingAxis(var Point : TPLocation; TXForm : TStackTransform);
    procedure SetDatumAxisPOsitions(X,Y,Z,A,B,C: Double);
    function AddPartRelativePoint(X,Y,Z: Double;PName :String): Integer;
    function AddMachineRelativePoint(X,Y,Z: Double;PName :String):Integer;
    function AddToolRelativePoint(X,Y,Z: Double;PName :String):Integer;
    procedure ProbePosnToPartRelativePoint(PointName :String;X,Y,Z,A,B,C :Double);
    procedure NominalsReset;
    procedure SetNominalOffset(Index : INteger;X,Y,Z,A,B,C : Double); // For NDP only
    procedure SetAndSaveNominalOffset(Index : INteger;X,Y,Z,A,B,C : Double); // For NDP only
    procedure ClearNominalOffsets;
    procedure SetToolApproach(I,J,K : Double);
    procedure PutHoleRelativeToPartDatumAt(HolePosition : TPLocation);
    procedure SetPartDatumOffset(Index : INteger;X,Y,Z,A,B,C : Double);
    procedure SetPartDatumRotaryOffset(Index: INteger; A, B, C: Double);
    procedure RuntimeToolDatumAdjustby(Adjustment : TPlocation);



//    function GetNDP(CRot:Double;var ResLinear,ResRotary: TXYZ;Par1,Par2:Integer): Integer;
    function GetGTNDPForZAlignedHole(Zrot: Double; var ResLinear, ResRotary: TXYZ; Solution: Integer;InverseVector: Boolean):TNDPErrorTypes;
    function NewGetGTNDPForZAlignedHole(Zrot: Double; var ResLinear, ResRotary: TXYZ; Solution: Integer;InverseVector: Boolean):TNDPErrorTypes;
    function GetGTNDPForFixedRotary(FixedRot: Double;FixedAxisIndex: Integer; var ResLinear, ResRotary: TXYZ; Solution: Integer;InverseVector: Boolean;IgnoreLimits : Boolean):TNDPErrorTypes;
    function CalculateStackingAxisTransform : TStackTransform;
    function GetGTErrowsOfset : TXYZ;
    procedure GTSetUniqueElectrodeOffsets(XYZOffset: TPLocation;Standoff : Double);
    procedure GetGTMachineToolRetractVector(VectorLength: Double; var IncResult: TXYZ);
    procedure ModifyNDPOffsetFor4Points(P1,P2,P3,P4: TPlocation; var Rotations : TPlocation);
    function ResolveProbePoint(XYZ,ABC : TPLocation): TPlocation;


//**********
//Are these needed???
    procedure CorrectAxesFreezingVector(VectorIndex,AxisIndex: Integer; Correction : Double);


//***********
  published
    { Published declarations }

   constructor Create(AOwner: TComponent);override;
   destructor Destroy; override;


  end;
function isvalidPointName(Pname : String):Boolean;

procedure Register;

implementation

uses G3DLine,G3DDisc,GTGEOMain;

procedure TG3DOTree.SetDebugReported(Value: TNotifyEvent);
begin
FDebugReported := Value;
end;

procedure TG3DOTree.DebugChanged;
begin
if assigned(FDebugReported) then
   begin
   FDebugReported(Self);
   end;
end;

procedure TG3DOTree.ClearAllOffsetsandVectors;
begin
MatchObjectListToTree;
DeleteAllVectors;
DeleteRuntimeReportPoints;
if not Editormode Then
  begin
  CreateNominalandActualVectors;
  SetNominalStackingAxis(0,0,0,  0,0,100, 25,0,0);
  SetActualStackingAxis (0,0,0,  0,0,100, 25,0,0);
  end;
with StackingAxisTransform do
  begin
  Rotation := SetPRotation(0,0,0);
  XYZShift := SetPrecisePoint3D(0,0,0);
  end;
FAppliedStandoff := 0;
UpdateReportData(True);
end;

// Note : Vetor points are all set relative to the Part Datum
// NOT relative to machine Datum. They take no account of current
// Axis Position.
procedure TG3DOTree.SetNominalStackingAxis(PX,PY,PZ,X2,Y2,Z2,TX,TY,TZ : Double);
var
Point : TG3dReportPoint;
begin
//NGT!!!
if NominalVectorIndex >= 0 then
   begin
   Point := GetVectorPoint(1,FReportVectorList[NominalVectorIndex]);
   POint.FPositionData.DesignTimeTranslation := GTLinearCorrected(SetPrecisePOint3D(-PX,-PY,PZ));
   Point := GetVectorPoint(2,FReportVectorList[NominalVectorIndex]);
   POint.FPositionData.DesignTimeTranslation := GTLinearCorrected(SetPrecisePOint3D(-X2,-Y2,Z2));
   if NTVectorIndex >= 0 then
      begin
      Point := GetVectorPoint(1,FReportVectorList[NTVectorIndex]);
      POint.FPositionData.DesignTimeTranslation := GTLinearCorrected(SetPrecisePOint3D(-PX,-PY,PZ));
      Point := GetVectorPoint(2,FReportVectorList[NTVectorIndex]);
      POint.FPositionData.DesignTimeTranslation := GTLinearCorrected(SetPrecisePOint3D(-TX,-TY,TZ))
      end;
   end;
UpdateReportData(True);
StackingAxisTransform := CalculateStackingAxisTransform;
end;


procedure TG3DOTree.SetActualStackingAxis(PX,PY,PZ,X2,Y2,Z2,TX,TY,TZ : Double);
var
Point : TG3dReportPoint;
begin
//NGT!!!
if ActualVectorIndex >= 0 then
   begin
   Point := GetVectorPoint(1,FReportVectorList[ActualVectorIndex]);
   POint.FPositionData.DesignTimeTranslation := GTLinearCorrected(SetPrecisePOint3D(-PX,-PY,PZ));
   Point := GetVectorPoint(2,FReportVectorList[ActualVectorIndex]);
   POint.FPositionData.DesignTimeTranslation := GTLinearCorrected(SetPrecisePOint3D(-X2,-Y2,Z2));
   if ATVectorIndex >= 0 then
      begin
      Point := GetVectorPoint(1,FReportVectorList[ATVectorIndex]);
      POint.FPositionData.DesignTimeTranslation := GTLinearCorrected(SetPrecisePOint3D(-PX,-PY,PZ));
      Point := GetVectorPoint(2,FReportVectorList[ATVectorIndex]);
      POint.FPositionData.DesignTimeTranslation := GTLinearCorrected(SetPrecisePOint3D(-TX,-TY,TZ))
      end;
   end;
UpdateReportData(True);
StackingAxisTransform := CalculateStackingAxisTransform;
end;




function TG3DOTree.NewGetAxisOffsets(var Stage :Integer; IncludeCAxis : Boolean;SingleStage : Boolean):TAxisPosn;
var
DoAllStages          : Boolean;
VecDiff              : TVectorDifference;
NominalRotation,ActualRotation : Double;
NomAPoint             : TPlocation;
ActAPoint             : TPlocation;
Deltax,DeltaY,Deltaz  : Double;
NomVectorData         : TVectorData;
ActVectorData         : TVectorData;
ActBRot               : Double;
RotOrigin             : TPlocation;
RotValue              : Double;
CRotatedElectrode     : TPlocation;
UnRotatedElectrode    : TPlocation;


begin
// N.B. Stage Driven for debug purposes
DoAllStages := True;
try
if (NominalVectorIndex < 0) or (ActualVectorIndex < 0) or (ATVectorIndex < 0) or (NTVectorIndex <0) then exit;
while DoAllStages do
begin
case Stage of
     0:
     begin
     // Save Axis Positions
     UpdateReportData(True);
     Result := ZeroOffsets;
     FCorrectionStartPosn := GetAllAxisPositions;
     FDebug := Format('Start: X%7.3f, Y%7.3f, Z%7.3f, A%7.3f, B%7.3f, C%7.3f',
                       [FCorrectionStartPosn[0],
                        FCorrectionStartPosn[1],
                        FCorrectionStartPosn[2],
                        FCorrectionStartPosn[3],
                        FCorrectionStartPosn[4],
                        FCorrectionStartPosn[5]
                        ]);
     UpdateReportData(True);

     NomVectorData := FReportVectorList[NominalVectorIndex];
     NomAPoint := NomVectorData.Point1Position;
     // Record the delta x,y,z between A Point of Nom vecor and electrode position
     XtoNom := NomAPoint.x - FElectrodeLocation.X;
     YtoNom := NomAPoint.y - FElectrodeLocation.Y;
     ZtoNom := NomAPoint.z - FElectrodeLocation.Z;
     end;


     1:
     begin
     //Datum axes to detemine difference between timing Vectors and
     // calculate A rotation
     DatumAllAxes;
     UpdateReportData(True);
     //Correct For Rotation Parameter by incrementing A axis
     NominalRotation :=  FReportVectorList[NTVectorIndex].Rotation.z;
     ActualRotation :=  FReportVectorList[ATVectorIndex].Rotation.z;
     Result[3] := NominalRotation-ActualRotation;
     // Not final result 3 but delta a on final position
     end;

     2:
     begin
     // Go back to start position (Nominal)
     RestoreAllAxisPOsitions(FCorrectionStartPosn);
     Result[3] := Result[3] + GetAxisPosition(3);
     // rotate a by calculated delta A
     SetAxisPOsition(3,Result[3]);

     UpdateReportData(True);

     // AFTER A Rotation at Nominal Position add a tracking electrode 'TrackElectrode'
     AddMachineRelativePoint(FElectrodeLocation.X,FElectrodeLocation.Y,FElectrodeLocation.Z,'TrackElectrode');

     // Correct B axis For difference in Actual and Nominal stacking Axis
     UpdateReportData(True);
     GetVectorDifference(NominalVectorIndex,ActualVectorIndex,VecDiff);
     // rotate B to bring Actual to Nominal
     if AxisDefs[4].isReversed then
        begin
        Result[4]:= GetAxisPosition(4)+VecDiff.Rotation.y;
        FDebug := Format('B Rot := %7.3f',[VecDiff.Rotation.y]);
        end
     else
        begin
        Result[4] := GetAxisPosition(4)-VecDiff.Rotation.y;
        FDebug := Format('B Rot := %7.3f',[-VecDiff.Rotation.y]);
        end;
     if Result[4] < -180 then Result[4] := Result[4]+360;
     SetAxisPosition(4,Result[4]);
     // Moded 1/6/2001 V1.1
     if not IncludeCAxis then
        begin
        Result[5] :=  GetAxisPosition(5);
        inc(Stage); // skip next stage
        end;
     end;
     // End of Mod 1/6/2001 V1.1

     3:
     begin
     // Here ONLY if Caxis Correction Required
     UpdateReportData(True);
     GetVectorDifference(NominalVectorIndex,ActualVectorIndex,VecDiff);
     if AxisDefs[5].isReversed then
        begin
        Result[5] := GetAxisPosition(5)-VecDiff.Rotation.z;
        end
     else
        begin
        Result[5] := GetAxisPosition(5)+VecDiff.Rotation.z;
        end;
    SetAxisPosition(5,Result[5]);
     end;


     4:
     begin
     // Now determine delta x y z of tracked electrode to A point of rotated Vector

     UpdateReportData(True);
     ActVectorData := FReportVectorList[ActualVectorIndex];
     // Get A point of Actual Vector after A and B rotational Corrections
     ActAPoint := ActVectorData.Point1Position;

     // Now find Delta x,y,z of the A point to the elctrode datum
     XtoRotated := ActAPoint.x - FElectrodeLocation.X;
     YtoRotated := ActAPoint.y - FElectrodeLocation.Y;
     ZtoRotated := ActAPoint.z - FElectrodeLocation.Z;

     // Now find difference between current distance to A point and original distance to A Point
     // from the nominal drill position as defined by the electrode datum

     Deltax := XtoRotated-XToNom;
     Deltay := YtoRotated-YToNom;
     Deltaz := ZtoRotated-ZToNom;

     Result[0] := GetAxisPosition(0)-Deltax;
     Result[1] := GetAxisPosition(1)-Deltay;
     Result[2] := GetAxisPosition(2)+Deltaz;

     // This now leaves then nominal and actual vecors coincident on their A point (lower point)
     // dependent on the nominal B position there is still a rotational error between the vectors
     // which can not be taken out by the rotary axes

     SetAxisPosition(0,Result[0]);
     SetAxisPosition(1,Result[1]);
     SetAxisPosition(2,Result[2]);
     // This now leaves then nominal and actual vecors coincident on their A point (lower point)
     // dependent on the nominal B position there is still a rotational error between the vectors
     // which can not be taken out by the rotary axes


     // Rotate B to bring then new actual vector VERTICAL looking from front
     // Then rotate attached tracking electrode by error in X rotation to Vertical
     // Rotation is about the common A point
     // Attach this to axes and go back to found nominal
     // The deltas should be the required extra x,y,z
     UpdateReportData(True);
     ActBRot:= ActVectorData.Rotation.y;
     if ActBRot > 180 then ActBRot := ActBRot-360;

     // Rotate B to Get Actual Vector VERTICAL
     SetAxisPosition(4,GetAxisPosition(4)-ActBRot);
     UpdateReportData(True);
     GetVectorDifference(NominalVectorIndex,ActualVectorIndex,VecDiff);
     RotOrigin := GetReportPointCalled('ActualVector_A');
     // Get Rotational difference looking along X axis
     RotValue  := VecDiff.Rotation.x;

     UnRotatedElectrode := GetReportPointCalled('TrackElectrode');
     // Get a copy of the drilling position
     CRotatedElectrode := GetReportPointCalled('TrackElectrode');
     // Rotate the new electrode about the A point by the vector difference
     CRotatedElectrode := PRotateAboutX3D(CRotatedElectrode,RotOrigin,RotValue);
     // Attach this point in space to the machine axes so that it rotates with them
     AddMachineRelativePoint(CRotatedElectrode.X,CRotatedElectrode.Y,CRotatedElectrode.Z,'TrackElectrode2');
     UpdateReportData(True);
     // Rotate B axis back to Final Position

     SetAxisPosition(4,Result[4]);
     UpdateReportData(True);

     // Now Find where Corrected Electrod has gone and add delta to X,y,z
     CRotatedElectrode := GetReportPointCalled('TrackElectrode2');
     UnRotatedElectrode := GetReportPointCalled('TrackElectrode');

     Deltax := CRotatedElectrode.x-UnRotatedElectrode.x;
     Deltay := CRotatedElectrode.y-UnRotatedElectrode.y;
     Deltaz := CRotatedElectrode.z-UnRotatedElectrode.z;

     Result[0] := GetAxisPosition(0)+Deltax;
     Result[1] := GetAxisPosition(1)+Deltay;
     Result[2] := GetAxisPosition(2)+Deltaz;
     end;

     5:
     begin
     RestoreAllAxisPOsitions(FCorrectionStartPosn);
     SingleStage := True;
     end;

end; //case
DoAllStages := not SingleStage;
inc (Stage);
end;
finally;
DeleteReportPointCalled('TrackElectrode');
DeleteReportPointCalled('TrackElectrode2');
end;

end;


function TG3DOTree.CalculateElectrodeXYZShift:TPlocation;
begin
end;

constructor TG3DOTree.Create(AOwner: TComponent);
var
Count : Integer;
Image : TBitmap;
begin

inherited;
FHoleList := TList.Create;
FPartDatumOffset := SetPrecisePoint3D(0,0,0);
FPartDatumOffset :=SetPrecisePoint3D(0,0,0);
For Count := 0 to 11 do
  begin
  AxisMoves[Count].Move := False;
  end;
DebugCount := 0;
InhibitTreeTableUpdate := False;
InhibitObjectListMatch :=False;
FEditorMode := False;
ExtentsOverScale :=1.1;
Zero3D := SetPrecisePoint3D(0,0,0);
ActualVectorIndex := -1;
NominalVectorIndex := -1;
NDPVectorIndex :=-1;
InverseNDPVectorIndex := -1;
ApproachVectorIndex := -1;
AttachedElecIndex := -1;
ATVectorIndex := -1;
NTVectorIndex := -1;
TreeTable := TTreeTable.Create;
FDisplayMode := dmVirtual;
SetLength(FReportPointList,0);
SetLength(FReportVectorList,0);
ObjectPopup := TPopupMenu.create(Self);
ObjectPopup.AutoPopup := False;
//Parent := Application.MainForm;
Image := TBitmap.Create;
try
Images := TImageList.Create(Self);
Images.Width := 12;
Images.Height := 12;
with Images do
        begin
        Image.LoadFromResourceName(HInstance,'GWORLD');                  //0
        Images.Add(Image,Image);
        Image.LoadFromResourceName(HInstance,'GLINE');                   //1
        Images.Add(Image,Image);
        Image.LoadFromResourceName(HInstance,'GBOX');                    //2
        Images.Add(Image,Image);
        Image.LoadFromResourceName(HInstance,'GDISC');                   //3
        Images.Add(Image,Image);
        Image.LoadFromResourceName(HInstance,'GSELWORLD');               //4
        Images.Add(Image,Image);
        Image.LoadFromResourceName(HInstance,'GSELLINE');                //5
        Images.Add(Image,Image);
        Image.LoadFromResourceName(HInstance,'GSELBOX');                 //6
        Images.Add(Image,Image);
        Image.LoadFromResourceName(HInstance,'GSELDISC');                //7
        Images.Add(Image,Image);
        Image.LoadFromResourceName(HInstance,'GREPORTPOINT');            //8
        Images.Add(Image,Image);
        Image.LoadFromResourceName(HInstance,'GSELREPORTPOINT');        //9
        Images.Add(Image,Image);
        Image.LoadFromResourceName(HInstance,'DATUMPOINT');             //10
        Images.Add(Image,Image);
        Image.LoadFromResourceName(HInstance,'GSELDATUMPOINT');          //11
        Images.Add(Image,Image);
        Image.LoadFromResourceName(HInstance,'GREPORTVECTOR');          //12
        Images.Add(Image,Image);
        Image.LoadFromResourceName(HInstance,'GSELREPORTVECTOR');       //13
        Images.Add(Image,Image);
        end;
finally
Image.Free;
end;
For Count := 0 to NumObjectPopItems-1 do
    begin
    ObjectPopList[Count] := TMenuItem.create(Self);
    ObjectPopup.Items.add(ObjectPopList[Count]);

    end;
ObjectPopList[0].Caption := 'DELETE';
ObjectPopList[0].OnClick := TreePopupDeleteClick;
ObjectPopList[1].Caption := 'Show Points';
ObjectPopList[1].OnClick := TreeShowPointsClick;
;

//ObjectPopList[1].Caption := 'View';

PopupMenu := ObjectPopup;
For Count := 0 to 8 do
     begin
     if Count<3 then AxisDefs[Count].isLinear := True;
     AxisDefs[Count].DatumTranslation := Zero3D;
     AxisDefs[Count].DatumRotation := SetPRotation(0,0,0);
     AxisDefs[Count].MotionAxisIndex := Count;
     AxisDefs[Count].DisplayTextIndex := Count;
     AxisDefs[Count].ObjectName := 'None Selected';
     AxisDefs[Count].AxisObject := nil;
     end;
HideSelection := False;
FPartDatumNode := nil;
FFixedPartDatumNode := nil;
FToolDatumNode := nil;
FElectrodeNode := nil;
FHoleNode := nil;
FNDPDatumNode := nil;
FElectrodeOffset := Zero3D;

end;

destructor TG3DOTree.Destroy;
begin
if assigned(FHoleList) then FHoleList.Free; // !!! clearObjects first
if assigned(TreeTable) then TreeTable.Free;
inherited Destroy;
end;

procedure TG3DOTree.ClearAllObjects;
begin
if assigned(WorldNode) then
   begin
   DeleteObjectandChildrenAtNode(WorldNode);
   end
else
    begin
    WorldNode := Items.AddChildFirst(WorldNode,WorldNodeID);
    end;
WorldObject := TG3dBox.Create(Self);
WorldNode.Data := WorldObject;
ActualVectorIndex := -1;
NDPVectorIndex := -1;
InverseNDPVectorIndex := -1;
ApproachVectorIndex := -1;
AttachedElecIndex := -1;
NominalVectorIndex := -1;
ATVectorIndex := -1;
NTVectorIndex := -1;
SetLength(FReportVectorList,0);
SetLength(FReportPOintList,0);
FPartDatumNode := nil;
FFixedPartDatumNode := nil;
FToolDatumNode := nil;
FElectrodeNode := nil;
FHoleNode := nil;
FNDPDatumNode  := nil;
wORLDnODE.ImageIndex := 0;
WorldNode.SelectedIndex := 4;
TreeTable.BuildTableFrom(Self,WorldNode);
end;



function TG3DOTree.AddObjectatNode(Node :TTreeNode;NewObject :T3DObjects;IsVirtual : Boolean): String;
var
NewLine : TG3DLine;
NewBox  : TG3DBox;
NewDisc : TG3DDisc;
NewNode : TTreeNode;
NewPOint: TG3DReportPoint;
NameCount : Integer;

begin
if not assigned(TreeTable) then exit;
result := '';
case NewObject of
     t3oLine:
     begin
     Newline := TG3DLine.Create(Self);
     Newline.VirtualOnly := IsVirtual;
     NameCount := TreeTable.ObjectCount;
     while NameExists(Format('Line%d',[NameCount])) do
           begin
           inc (nameCount);
           end;
     NewLine.Tag := NameCount;
     NewLine.Name := Format('Line%d',[NameCount]);
     Result := NewLine.Name;
     NewNode := Items.AddChild(Node,NewLine.Name);
     NewNode.ImageIndex := 1;
     NewNode.SelectedIndex := 5;
     NewNode.Data := Newline;
     TreeTable.BuildTableFrom(Self,WorldNode);
     end;

     t3oBox:
     begin
     NewBox := TG3DBox.Create(Self);
     NewBox.VirtualOnly := IsVirtual;
     NameCount := Treetable.ObjectCount;
     while NameExists(Format('Box%d',[NameCount])) do
           begin
           inc (nameCount);
           end;
     NewBox.Tag := NameCount;
     NewBox.Name :=  Format('Box%d',[NameCount]);
     Result := NewBox.Name;
     NewNode := Items.AddChild(Node,NewBox.Name);
     NewNode.Data := NewBox;
     NewNode.ImageIndex := 2;
     NewNode.SelectedIndex := 6;
     TreeTable.BuildTableFrom(Self,WorldNode);
     end;

     t3oDisc:
     begin
     NewDisc := TG3DDisc.Create(Self);
     NewDisc.VirtualOnly := IsVirtual;
     NameCount := Treetable.ObjectCount;
     while NameExists(Format('Disc%d',[NameCount])) do
           begin
           inc (nameCount);
           end;
     NewDisc.Tag  := NameCount;
     NewDisc.Name :=  Format('Disc%d',[NameCount]);
     Result := NewDisc.Name;
     NewNode := Items.AddChild(Node,NewDisc.Name);
     NewNode.ImageIndex := 3;
     NewNode.SelectedIndex := 7;
     NewNode.Data := NewDisc;
     TreeTable.BuildTableFrom(Self,WorldNode);
     end;

     t3oReportPoint:
     begin
     //
     NewPoint := TG3DReportPoint.Create(Self);
     NewPoint.VirtualOnly := IsVirtual;
     NameCount := Treetable.ObjectCount;
     while NameExists(Format('ReportPoint%d',[NameCount])) do
           begin
           inc (nameCount);
           end;
     NewPoint.Tag  := NameCount;
     NewPoint.Name :=  Format('ReportPoint%d',[NameCount]);
     Result := NewPoint.Name;
     NewPoint.Colour := clSilver;
     NewPoint.SelectColour := ReportPointSelectColour;
     NewNode := Items.AddChild(Node,NewPoint.Name);
     NewNode.ImageIndex := 8;
     NewNode.SelectedIndex := 9;
     NewNode.Data := NewPoint;
     TreeTable.BuildTableFrom(Self,WorldNode);
     end;

     t3oPartDatum:
     begin
     NewPoint := TG3DReportPoint.Create(Self);
     NameCount := Treetable.ObjectCount;
     NewPoint.Tag  := NameCount;
     NewPoint.Name :=  PartDatumID;
     NewPoint.PointType := rpPartDatum;
     Result := NewPoint.Name;
     NewPoint.Colour := clSilver;
     NewPoint.SelectColour := ReportPointSelectColour;
     NewNode := Items.AddChild(Node,NewPoint.Name);
     NewNode.ImageIndex := 10;
     NewNode.SelectedIndex := 11;
     NewNode.Data := NewPoint;
     FPartDatumNode := NewNode;
     TreeTable.BuildTableFrom(Self,WorldNode);
     end;

     t3oNominalPartDatum:
     begin
     NewPoint := TG3DReportPoint.Create(Self);
     NameCount := Treetable.ObjectCount;
     NewPoint.Tag  := NameCount;
     NewPoint.Name :=  NomPartDatumID;
     NewPoint.PointType := rpNomPartDatum;
     Result := NewPoint.Name;
     NewPoint.Colour := clSilver;
     NewPoint.SelectColour := ReportPointSelectColour;
     NewNode := Items.AddChild(Node,NewPoint.Name);
     NewNode.ImageIndex := 10;
     NewNode.SelectedIndex := 11;
     NewNode.Data := NewPoint;
     FFixedPartDatumNode := NewNode;
     TreeTable.BuildTableFrom(Self,WorldNode);
     end;

     t3oToolDatum:
     begin
     NewPoint := TG3DReportPoint.Create(Self);
     NameCount := Treetable.ObjectCount;
     NewPoint.Tag  := NameCount;
     NewPoint.Name :=  ToolDatumID;
     NewPoint.PointType := rpToolDatum;
     NewPoint.Colour := clSilver;
     NewPoint.SelectColour := ReportPointSelectColour;
     Result := NewPoint.Name;
     NewNode := Items.AddChild(Node,NewPoint.Name);
     NewNode.ImageIndex := 10;
     NewNode.SelectedIndex := 11;
     NewNode.Data := NewPoint;
     FToolDatumNode := NewNode;
     TreeTable.BuildTableFrom(Self,WorldNode);
     end;

     t3oHoleNode:
     begin
     NewPoint := TG3DReportPoint.Create(Self);
     NewPoint.VirtualOnly := IsVirtual;
     NameCount := Treetable.ObjectCount;
     NewPoint.Tag  := NameCount;
     try
     NewPoint.Name :=  'HolePoint';
     Result := 'HolePoint';
     NewPoint.PointType := rpHolePoint;
     NewPoint.Colour := clred;
     NewPoint.SelectColour := clRed;
     NewNode := Items.AddChild(Node,NewPoint.Name);
     NewNode.ImageIndex := 10;
     NewNode.SelectedIndex := 11;
     NewNode.Data := NewPoint;
     FHoleNode := NewNode;
     TreeTable.BuildTableFrom(Self,WorldNode);
     except
     Result := '';
     end;
     end;

     t3oELectrodeNode:
     begin
     NewPoint := TG3DReportPoint.Create(Self);
     NewPoint.VirtualOnly := IsVirtual;
     NameCount := Treetable.ObjectCount;
     NewPoint.Tag  := NameCount;
     NewPoint.Name :=  'ElectrodeDatum';
     NewPoint.PointType := rpElectrodeDatum;
     NewPoint.Colour := clSilver;
     NewPoint.SelectColour := clRed;
     Result := NewPoint.Name;
     NewNode := Items.AddChild(Node,NewPoint.Name);
     NewNode.ImageIndex := 10;
     NewNode.SelectedIndex := 11;
     NewNode.Data := NewPoint;
     FELectrodeNode := NewNode;
     TreeTable.BuildTableFrom(Self,WorldNode);
     end;

     t3oNDPDatumNode:
     begin
     NewPoint := TG3DReportPoint.Create(Self);
     NewPoint.VirtualOnly := IsVirtual;
     NameCount := Treetable.ObjectCount;
     NewPoint.Tag  := NameCount;
     NewPoint.Name :=  'NDPDatum';
     NewPoint.PointType := rpNDPDatum;
     NewPoint.Colour := clSilver;
     NewPoint.SelectColour := clAqua;
     Result := NewPoint.Name;
     NewNode := Items.AddChild(Node,NewPoint.Name);
     NewNode.ImageIndex := 10;
     NewNode.SelectedIndex := 11;
     NewNode.Data := NewPoint;
     FNDPDatumNode := NewNode;
     TreeTable.BuildTableFrom(Self,WorldNode);
     end;

end; //case
end;
//Returns true if an object of the given name already exists in the ObjectList

function TG3DOTree.NameExists(Name : String):Boolean;
var
count : Integer;
begin
result := false;
if not assigned(TreeTable) then exit;
if TreeTable.ObjectCount = 0 then exit;
For Count := 0 to TreeTable.ObjectCount-1 do
    begin
    if Name = TreeTable.ObjectTable[Count].GObjectName then
       begin
       Result := True;
       exit;
       end;
    end;
end;

// Returns the index into the ObjectList for the object of the given name
function TG3DOTree.FindObjectListIndexFor(ObjectName : String):Integer;
var
count : Integer;
begin
result := -1;
if not assigned(TreeTable) then exit;
if TreeTable.ObjectCount = 0 then exit;
For Count := 0 to TreeTable.ObjectCount-1 do
    begin
    if ObjectName = TreeTable.ObjectTable[Count].GObjectName then
       begin
       Result := Count;
       exit;
       end;
    end;
end;


// Returns a pointer to the object with the given name
function TG3DOTree.FindObject(Name : String):POinter;
var
count : Integer;
begin
result := nil;
if not assigned(TreeTable) then exit;
if TreeTable.ObjectCount = 0 then exit;
For Count := 0 to TreeTable.ObjectCount-1 do
    begin
    if Name = TreeTable.ObjectTable[Count].GObjectName then
       begin
       Result := TreeTable.ObjectTable[Count].GObject;
       exit;
       end;
    end;

end;

function TG3DOTree.FindObjectandNode(Name : String; var Node: TTreeNode):POinter;
var
count : Integer;
begin
result := nil;
if not assigned(TreeTable) then exit;
if TreeTable.ObjectCount = 0 then exit;
For Count := 0 to TreeTable.ObjectCount-1 do
    begin
    if Name = TreeTable.ObjectTable[Count].GObjectName then
       begin
       Node := Items.GetNode(TreeTable.ObjectTable[Count].NodeId);
       Result := Node.Data;
       exit;
       end;
    end;

end;



function TG3DOTree.GetJogObject(AxisIndex: Integer;var Node : TTreeNode):Pointer;

begin
Result := AxisDefs[AxisIndex].AxisObject;
end;


procedure TG3DOTree.DrawAllObjectsOn(Paper : TG3DPaper;DoPaint :Boolean);
var
OCount     : Integer;
begin
if not assigned(TreeTable) then exit;
if TreeTable.ObjectCount = 0 then exit;
if FDisplayMode = dmVirtual then exit;
Paper.CleanSheet(clWhite);
For OCount := 0 to TreeTable.ObjectCount-1 do
    begin
    with TG3DObject(TreeTable.ObjectTable[OCount].GObject) do
         begin
         DrawOn(Paper,FDisplayMode);
         end;
    end;
if DoPaint then Paper.Paint;
end;



procedure TG3DOTree.MatchObjectListToTree;
begin
//if GlobalUpdateCount > 0 then exit;
if InhibitObjectListMatch then
  begin
  InhibitObjectListMatch := False;
  end
else
  begin
  TreeTable.BuildTableFrom(Self,WorldNode);
  end;
end;

procedure TG3DOTree.ConditionalMatchObjectListToTree;
begin
if GlobalUpdateCount > 0 then exit;
if InhibitObjectListMatch then
  begin
  InhibitObjectListMatch := False;
  end
else
  begin
  TreeTable.BuildTableFrom(Self,WorldNode);
  end;
end;


procedure TG3DOTree.Update3DDisplay(Display : TG3DDisplay;Mode : TUpdateMode);
begin
if not assigned(Display) then exit;
if not assigned(TreeTable) then exit;
if GlobalUpdateCount > 0 then exit;
if TreeTable.ObjectCount = 0 then
   begin
   Display.CustomView.CleanSheet(clwhite);
   Display.FrontView.CleanSheet(clwhite);
   exit;
   end;
if Display.CustomVisible then
   begin
   DrawAllObjectsOn(Display.CustomView,EditorMode);
   end;
if Mode=umCustomOnly then exit;
if Display.TopVisible then
   begin
   DrawAllObjectsOn(Display.TopView,EditorMode);
   end;
if Display.SideVisible then
   begin
   DrawAllObjectsOn(Display.SideView,EditorMode);
   end;
if Display.FrontVisible then
   begin
   DrawAllObjectsOn(Display.FrontView,EditorMode);
   end;

end;


function TG3DOTree.CreateChildNodeListFor(BaseNode : TTreeNode):TList;
var
ChildList : TList;
begin
ChildList := TList.Create;
try
If BaseNode.HasChildren then
   begin
   ListAllNodes(BaseNode,ChildList);
   end
else
    ChildList.Add(BaseNode);
Result := ChildList;
except
Result := nil;
ChildList.Free;
exit;
end;
end;

// This creates a list of pointers to all objects below a node (including the node)
// in REVERSE order for updating position etc
// Note this is iterative
procedure TG3DOTree.ListAllNodeObjects(BaseNode : TTreeNode; NList : TList);
// NB internal procedure to add a node entry to the list.
var
NewNode     : TTreenode;
begin
if BaseNode.HasChildren then
      begin
      NewNode := BaseNode.GetFirstChild;
      ListAllNodeObjects(NewNode,NList);
      NewNode := BaseNode.GetNextChild(NewNode);
      while assigned(NewNode) do
         begin
         ListAllNodeObjects(NewNode,NList);
         NewNode := BaseNode.GetNextChild(NewNode);
         end
      end;
//else
    begin
    // This is the end of the branch so just add this to the list
    if assigned(BaseNode.Data) then NList.Add(BaseNode.Data);
    end;
end;

// This creates a list of pointers to all nodes below a node (including the node)
// in REVERSE order for updating position etc
procedure TG3DOTree.ListAllNodes(BaseNode : TTreeNode; NList : TList);
var
NewNode     : TTreenode;
begin
if BaseNode.HasChildren then
      begin
      NewNode := BaseNode.GetFirstChild;
      ListAllNodes(NewNode,NList);
      NewNode := BaseNode.GetNextChild(NewNode);
      while assigned(NewNode) do
         begin
         ListAllNodes(NewNode,NList);
         NewNode := BaseNode.GetNextChild(NewNode);
         end
      end;
//else
    begin
    // This is the end of the branch so just add this to the list
    NList.Add(BaseNode);
    end;
end;



procedure TG3DOTree.UpdateObjectAndChildrenAtNode(Node: TTreeNode);
begin
BuildAllObjectsAndChildren;
end;

procedure TG3DOTree.DeleteObjectandChildrenAtNode(Node: TTreeNode);
var
GObject : TG3DObject;
NodeList : TList;
Count : Integer;
NewNode  : TTreeNode;

begin
inhibitTreeTableUpdate := True;
NodeList := CreateChildNodeListFor(Node);
try
// Ensure Objects are deleted and entry in ObjectList is removed
// prior to deleting node from the tree
For Count := 0 to NodeList.Count-1 do
    begin
    NewNode :=  NodeList[Count];
    if assigned(NewNode.data) then
       begin
       GObject := NewNode.data;
       Gobject.Free;
       TreeTable.BuildTableFrom(Self,WorldNode);
       end;
    end;
if Node <> WorldNode then
   begin
   Node.Delete;
   end;

MatchObjectListToTree;
finally
inhibitTreeTableUpdate := False;
NodeList.Free;
end;
end;

procedure TG3DOTree.FastDeleteObjectandChildrenAtNode(Node: TTreeNode);
var
GObject : TG3DObject;
NodeList : TList;
Count : Integer;
NewNode  : TTreeNode;

begin
inhibitTreeTableUpdate := True;
NodeList := CreateChildNodeListFor(Node);
try
// Ensure Objects are deleted and entry in ObjectList is removed
// prior to deleting node from the tree
For Count := 0 to NodeList.Count-1 do
    begin
    NewNode :=  NodeList[Count];
    if assigned(NewNode.data) then
       begin
       GObject := NewNode.data;
       Gobject.Free;
       if GlobalUpdateCount < 1 then TreeTable.BuildTableFrom(Self,WorldNode);
       end;
    end;
if Node <> WorldNode then
   begin
   Node.Delete;
   end;
finally
inhibitTreeTableUpdate := False;
NodeList.Free;
end;

end;




procedure TG3DOTree.BuildAllObjectsAndChildren;
var
GObject : TG3DObject;
Count : Integer;
ObjectIndex : Integer;
begin
if GlobalUpdateCount > 0 then exit;
inc(DebugCount);
DebugChanged;
For Count := 0 to Length(TreeTable.ObjectTable[TreeTable.worldIndex].ChildList)-1 do
    begin
    GObject := TG3DObject(TreeTable.ObjectTable[TreeTable.WorldIndex].ChildList[Count]);

    If assigned(GObject) then
       begin
       if not GObject.IsFrozen then
          begin
          // BuildObject relative to its Parent
          GOBject.CreateAbsoluteDatumPoints;
          OBjectIndex := TReeTable.TableIndexforObjectName(GObject.Name);

          //Translateand Rotate any children based on objects relationship to its Parent
          // n.B rotation ids done about 0,0,0
          RotateObjectandChildrenAtIndex(OBjectIndex,
                                  GObject.FPositionData.DesignTimeRotation,
                                  Zero3D,
                                  dmRuntimeAbs);
          TranslateObjectandChildrenAtIndex(OBjectIndex,
                                  GObject.FPositionData.DesignTimeTranslation,
                                  dmRuntimeAbs);
          GOBject.FpositionData.AbsoluteRotation := GOBject.FpositionData.DesignTimeRotation;
{          if GOBject.Name = ToolDatumID then
            begin
            GOBject.FpositionData.DesignTimeTranslation := AddLocation(FToolDatumOffset,GOBject.FpositionData.DesignTimeTranslation);
            end
          else if GOBject.Name = PartDatumID  then
            begin
            GOBject.FpositionData.DesignTimeTranslation := AddLocation(FPartDatumOffset,GOBject.FpositionData.DesignTimeTranslation);
            end;}
          GOBject.FpositionData.AbsoluteTranslation := GOBject.FpositionData.DesignTimeTranslation;
          end;
       end;

    end;
end;
{procedure TG3DOTree.BuildAllObjectsAndChildren;
var
GObject : TG3DObject;
Count : Integer;
ObjectIndex : Integer;
begin
if GlobalUpdateCount > 0 then exit;
inc(DebugCount);
DebugChanged;
For Count := 0 to Length(TreeTable.ObjectTable[TreeTable.worldIndex].ChildList)-1 do
    begin
    GObject := TG3DObject(TreeTable.ObjectTable[TreeTable.WorldIndex].ChildList[Count]);

    If assigned(GObject) then
       begin
       if not GObject.IsFrozen then
          begin
          // BuildObject relative to its Parent
          GOBject.CreateAbsoluteDatumPoints;
          OBjectIndex := TReeTable.TableIndexforObjectName(GObject.Name);

          //Translateand Rotate any children based on objects relationship to its Parent
          // n.B rotation ids done about 0,0,0
          RotateObjectandChildrenAtIndex(OBjectIndex,
                                  GObject.FPositionData.DesignTimeRotation,
                                  Zero3D,
                                  dmRuntimeAbs);
          TranslateObjectandChildrenAtIndex(OBjectIndex,
                                  GObject.FPositionData.DesignTimeTranslation,
                                  dmRuntimeAbs);
          GOBject.FpositionData.AbsoluteRotation := GOBject.FpositionData.DesignTimeRotation;
          GOBject.FpositionData.AbsoluteTranslation := GOBject.FpositionData.DesignTimeTranslation;
          end;
       end;

    end;
end;}

procedure TG3DOTree.RotateObjectandChildrenAtIndexAboutX(Index: Integer;Rotation : Double;COR : TPLocation;DesignMode : TDesignModes);
var
GObject : TG3DObject;
Count : Integer;
RotInc  : Double;
BaseRotInc : Double;
begin
Count := Length(TreeTable.ObjectTable[Index].ChildList)-1;

GObject := TG3DObject(TreeTable.ObjectTable[Index].ChildList[Count]);
GObject.RotateAllPointsInXAbout(COR,Rotation,DesignMode,Rotinc);
Count := Count-1;
if Count>= 0 then
   begin
   while Count >= 0 do
      begin
      GObject := TG3DObject(TreeTable.ObjectTable[Index].ChildList[Count]);
      GObject.RotateAllPointsInXAbout(COR,Rotinc,dmRuntimeInc,BaseRotinc);
      Count := Count-1
      end; // while
   end;
end;

procedure TG3DOTree.RotateObjectandChildrenAtIndexAboutY(Index: Integer;Rotation : Double;COR : TPLocation;DesignMode : TDesignModes);
var
GObject : TG3DObject;
Count : Integer;
RotInc  : Double;
BaseRotInc : Double;
begin
Count := Length(TreeTable.ObjectTable[Index].ChildList)-1;
GObject := TG3DObject(TreeTable.ObjectTable[Index].ChildList[Count]);
GObject.RotateAllPointsInYAbout(COR,Rotation,DesignMode,Rotinc);
Count := Count-1;
if Count>= 0 then
   begin
   while Count >=0 do
         begin
         GObject := TG3DObject(TreeTable.ObjectTable[Index].ChildList[Count]);
         GObject.RotateAllPointsInYAbout(COR,Rotinc,dmRuntimeInc,BaseRotinc);
         Count := Count-1
         end;
   end; // while
end;

procedure TG3DOTree.RotateObjectandChildrenAtIndexAboutZ(Index: Integer;Rotation : Double;COR : TPLocation;DesignMode : TDesignModes);
var
GObject : TG3DObject;
Count : Integer;
RotInc  : Double;
BaseRotInc : Double;
begin
Count := Length(TreeTable.ObjectTable[Index].ChildList)-1;
GObject := TG3DObject(TreeTable.ObjectTable[Index].ChildList[Count]);
GObject.RotateAllPointsInZAbout(COR,Rotation,DesignMode,Rotinc);
Count := Count-1;
if Count>= 0 then
   begin
   while Count >=0 do
         begin
         GObject := TG3DObject(TreeTable.ObjectTable[Index].ChildList[Count]);
         GObject.RotateAllPointsInZAbout(COR,Rotinc,dmRuntimeInc,BaseRotinc);
         Count := Count-1
         end;
   end; // while
end;

procedure TG3DOTree.RotateObjectandChildrenAtIndex(Index: Integer;Rotation : TRotation;COR : TPLocation;DesignMode : TDesignModes);
begin
  if Rotation.x <> 0 then RotateObjectandChildrenAtIndexAboutX(Index,Rotation.x ,COR,DesignMode);
  if Rotation.y <> 0 then RotateObjectandChildrenAtIndexAboutY(Index,Rotation.y ,COR,DesignMode);
  if Rotation.z <> 0 then RotateObjectandChildrenAtIndexAboutZ(Index,Rotation.z ,COR,DesignMode);
end;


procedure TG3DOTree.TranslateObjectandChildrenAtIndexAboutX(Index: Integer;Translation: Double;DesignMode : TDesignModes);
var
GObject : TG3DObject;
Count : Integer;
DummyInc :Double;
TrInc : Double;
begin
Count := Length(TreeTable.ObjectTable[Index].ChildList)-1;
GObject := TG3DObject(TreeTable.ObjectTable[Index].ChildList[Count]);
//TranslateFirstObject in SpecifiedDesignMode which should return required inc translation for children
GObject.TranslateAllPointsInX(Translation,DesignMode,TrInc);
Count := Count-1;
while Count >= 0 do
      begin
      GObject := TG3DObject(TreeTable.ObjectTable[Index].ChildList[Count]);
      GObject.TranslateAllPointsInX(TrInc,dmRuntimeInc,DummyInc);
      Count := Count-1
      end; // while
end;

procedure TG3DOTree.TranslateObjectandChildrenAtIndexAboutY(Index: Integer;Translation: Double;DesignMode : TDesignModes);
var
GObject : TG3DObject;
Count : Integer;
Trinc,DummyInc : Double;
begin
Count := Length(TreeTable.ObjectTable[Index].ChildList)-1;
GObject := TG3DObject(TreeTable.ObjectTable[Index].ChildList[Count]);
GObject.TranslateAllPointsInY(Translation,DesignMode,TrInc);
Count := Count-1;
while Count >= 0 do
      begin
      GObject := TG3DObject(TreeTable.ObjectTable[Index].ChildList[Count]);
      GObject.TranslateAllPointsInY(Trinc,dmRuntimeInc,DummyInc);
      Count := Count-1
      end; // while
end;

procedure TG3DOTree.TranslateObjectandChildrenAtIndexAboutZ(Index: Integer;Translation: Double;DesignMode : TDesignModes);
var
GObject : TG3DObject;
Count : Integer;
Trinc,DummyInc : Double;
begin
Count := Length(TreeTable.ObjectTable[Index].ChildList)-1;
GObject := TG3DObject(TreeTable.ObjectTable[Index].ChildList[Count]);
GObject.TranslateAllPointsInZ(Translation,DesignMode,TrInc);
Count := Count-1;
while Count >= 0 do
      begin
      GObject := TG3DObject(TreeTable.ObjectTable[Index].ChildList[Count]);
      GObject.TranslateAllPointsInZ(TrInc,dmRuntimeInc,DummyInc);
      Count := Count-1
      end; // while
end;
procedure TG3DOTree.TranslateObjectandChildrenAtIndex(Index: Integer;Translation : TPLocation;DesignMode : TDesignModes);
begin
    if Translation.x <> 0 then TranslateObjectandChildrenAtIndexAboutX(Index,Translation.x,DesignMode);
    if Translation.y <> 0 then TranslateObjectandChildrenAtIndexAboutY(Index,Translation.y,DesignMode);
    if Translation.z <> 0 then TranslateObjectandChildrenAtIndexAboutZ(Index,Translation.z,DesignMode);
end;


procedure TG3DOTree.SaveTreeToDisc(FName :String);
function FormatPos(Pos :TPLocation):String;
begin
Result := '';
Result := Format('%6.3f,%6.3f,%6.3f',[Pos.x,Pos.y,Pos.z]);
end;
function FormatRot(Pos :TRotation):String;
begin
Result := '';
Result := Format('%f,%f,%f',[Pos.x,Pos.y,Pos.z]);
end;
var
ObjectCount        : Integer;
LineCount          : Integer;
StringBuffer       : TStringList;
DescriptionBuffer  : TStringList;
AxisCount          : Integer;
AxisDef            : TAxisDefinition;

begin
StringBuffer := TStringList.Create;
DescriptionBuffer := TStringList.Create;
try
MatchObjectListToTree;
SaveToFile(FName);
StringBuffer.LoadFromFile(FName);
StringBuffer.Add('EndofTree');
StringBuffer.Add('');
StringBuffer.Add('');
For OBjectCount := 0 to TreeTable.ObjectCount-1 do
        begin
        with TG3DObject(TreeTable.ObjectTable[OBjectCount].GObject) do
                begin
                MakeDescription(DescriptionBuffer,dmFileSave);
                //DrawOn(Paper);
                end;

        For LineCount := 0 to DescriptionBuffer.Count-1 do
                begin
                StringBuffer.Add(DescriptionBuffer[LineCount]);
                end;
        StringBuffer.Add('End of Object');
        end;
StringBuffer.Add('');
StringBuffer.Add('[AxisData]');
StringBuffer.Add(Format('NumAxes=%d',[NumberofAxes]));
For AxisCount := 0 to NumberofAxes-1 do
    begin
    AxisDef := AxisDefs[AxisCount];
    StringBuffer.Add(Format('[Axis%d]',[AxisCount]));
    StringBuffer.Add(Format('ObjectName=%s',[AxisDef.ObjectName]));
    if AxisDef.isLinear then
       begin
       StringBuffer.Add('isLinear=True');
       end
    else
       begin
       StringBuffer.Add('isLinear=False');
       end;

    StringBuffer.Add(Format('MotionAxisIndex=%d',[AxisDef.MotionAxisIndex]));
    if AxisDef.isReversed then
       begin
       StringBuffer.Add('isReversed=True');
       end
    else
       begin
       StringBuffer.Add('isReversed=False');
       end;
    if AxisDef.AffectsPartDatum then
       begin
       StringBuffer.Add('AffectsPartDatum=True');
       end
    else
       begin
       StringBuffer.Add('AffectsPartDatum=False');
       end;
    if AxisDef.AffectsToolDatum then
       begin
       StringBuffer.Add('AffectsToolDatum=True');
       end
    else
       begin
       StringBuffer.Add('AffectsToolDatum=False');
       end;

    StringBuffer.Add(Format('DatumTranslation=%s',[FormatPos(AxisDef.DatumTranslation)]));
    StringBuffer.Add(Format('DatumRotation=%s',[FormatRot(AxisDef.DatumRotation)]));
    StringBuffer.Add(Format('DisplayTextIndex=%d',[AxisDef.DisplayTextIndex]));
    end;


StringBuffer.Add('EndofFile');
StringBuffer.SaveToFile(FName);
finally
StringBuffer.Free;
DescriptionBuffer.Free;
end;
end;


(*procedure TG3DOTree.ClearTreeAndObjects;
var
LineCount   : Integer;
StringBuffer : TStringList;
TreeList     :TStringList;
DescriptionBuffer : TStringList;
NodeList    : TList;
Buffer  : String;
Node    : TTreeNode;
ObjectFile : TIniFile;
NodeCount : Integer;
NodeName : String;
GObject : TG3DObject;
ReportPointCount : Integer;
AxisDataError    : Boolean;

begin
StringBuffer := TStringList.create;
TreeList     := TStringList.create;
DescriptionBuffer := TStringList.create;
NodeList := TList.Create;
try
AxisDataError := False;
MatchObjectListToTree;
//DescriptionBuffer.LoadFromFile(FName);
LineCount := 0;
Buffer := '';
ClearAllObjects;
TreeList.Clear;
SetLength(FReportPointList,0);
SetLength(FReportVectorList,0);
ReportPointCount := 0;
TreeList.Add('World');
TreeList.SaveToFile('Treetemp');
LoadFromFile('Treetemp');
TreeTable.ClearObjects;

// Now Update each object in the tree;

WorldNode := Items[0];
wORLDnODE.ImageIndex := 0;
WorldNode.SelectedIndex := 4;
ListAllNodes(WorldNode,NodeList);

// Now go through each node and get data from File as if it were an ini file
//ObjectFile := TIniFile.Create(FName);
For NodeCount := 0 to NodeList.Count-1 do
  begin
  Node := NodeList[NodeCount];
  NodeName := Node.Text;
  if NodeName <> WorldNodeID then
     begin
     GObject := MakeObjectFromFile(ObjectFile,NodeName,False);
//   GObject.VirtualOnly := LoadVirtual;
     Node.Data := GObject;
     end;
  end;
finally
StringBuffer.Free;
TreeList.Free;
DescriptionBuffer.Free;
NodeList.Free;
end;
end;*)



function TG3DOTree.LoadTreeFromDisc(FName :String;LoadVirtual : Boolean):TTreeErrors;
//Internal function
var
LineCount   : Integer;
StringBuffer : TStringList;
TreeList     :TStringList;
DescriptionBuffer : TStringList;
NodeList    : TList;
Buffer  : String;
Node    : TTreeNode;
ObjectFile : TIniFile;
NodeCount : Integer;
NodeName : String;
GObject : TG3DObject;
AxisCount : Integer;
SectionName : String;
ReportPointCount : Integer;
AxisDataError    : Boolean;
begin
Result := teDataOK;
StringBuffer := TStringList.create;
TreeList     := TStringList.create;
DescriptionBuffer := TStringList.create;
NodeList := TList.Create;
AxisDataError := False;
try
if not FileExists(FName) then exit;
//MatchObjectListToTree;
DescriptionBuffer.LoadFromFile(FName);
LoadedMachineFile := FName;
LineCount := 0;
Buffer := '';
ClearAllObjects;
TreeList.Clear;
SetLength(FReportPointList,0);
SetLength(FReportVectorList,0);
ReportPointCount := 0;

while (LineCount < DescriptionBuffer.Count) and (Buffer <> 'EndofTree') do
        begin
        try
        Buffer := DescriptionBuffer[LineCount];
        if Buffer <> 'EndofTree' then Treelist.Add(Buffer);
        finally
        inc(LineCount);
        end;
        end;
TreeList.SaveToFile('Treetemp');
LoadFromFile('Treetemp');



// Now Update each object in the tree;

WorldNode := Items[0];
wORLDnODE.ImageIndex := 0;
WorldNode.SelectedIndex := 4;
ListAllNodes(WorldNode,NodeList);

// Now go through each node and get data from File as if it were an ini file
ObjectFile := TIniFile.Create(FName);
try
FToolDatumOffset.x := ObjectFile.ReadFloat('DatumOffsets','ToolDatumXOffset',0.0);
FToolDatumOffset.y := -ObjectFile.ReadFloat('DatumOffsets','ToolDatumYOffset',0.0);
FToolDatumOffset.z := ObjectFile.ReadFloat('DatumOffsets','ToolDatumZOffset',0.0);
FPartDatumOffset.x := ObjectFile.ReadFloat('DatumOffsets','PartDatumXOffset',0.0);
FPartDatumOffset.y := ObjectFile.ReadFloat('DatumOffsets','PartDatumYOffset',0.0);
FPartDatumOffset.z := ObjectFile.ReadFloat('DatumOffsets','PartDatumZOffset',0.0);

    FXLimitsPositive       := ObjectFile.ReadFloat('AxisLimits','XPos',0.0);
    FXLimitsNegative       := ObjectFile.ReadFloat('AxisLimits','XNeg',0.0);
    FYLimitsPositive       := ObjectFile.ReadFloat('AxisLimits','YPos',0.0);
    FYLimitsNegative       := ObjectFile.ReadFloat('AxisLimits','YNeg',0.0);
    FZLimitsPositive       := ObjectFile.ReadFloat('AxisLimits','ZPos',0.0);
    FZLimitsNegative       := ObjectFile.ReadFloat('AxisLimits','ZNeg',0.0);
    FALimitsPositive       := ObjectFile.ReadFloat('AxisLimits','APos',0.0);
    FALimitsNegative       := ObjectFile.ReadFloat('AxisLimits','ANeg',0.0);
    FBLimitsPositive       := ObjectFile.ReadFloat('AxisLimits','BPos',0.0);
    FBLimitsNegative       := ObjectFile.ReadFloat('AxisLimits','BNeg',0.0);
    FCLimitsPositive       := ObjectFile.ReadFloat('AxisLimits','CPos',0.0);
    FCLimitsNegative       := ObjectFile.ReadFloat('AxisLimits','CNeg',0.0);


For NodeCount := 0 to NodeList.Count-1 do
begin
Node := NodeList[NodeCount];
NodeName := Node.Text;
if NodeName <> WorldNodeID then
   begin
   GObject := MakeObjectFromFile(ObjectFile,NodeName,LoadVirtual);
   Node.Data := GObject;
   case GObject.ObjectType of
     t3oLine:
     begin
     Node.ImageIndex := 1;
     Node.SelectedIndex := 5;
     end;
     t3oBox:
     begin
     Node.ImageIndex := 2;
     Node.SelectedIndex := 6;
     end;
     t3oDisc:
     begin
     Node.ImageIndex := 3;
     Node.SelectedIndex := 7;
     end;

     t3oReportVector:
     begin
     Node.ImageIndex := 12;
     Node.SelectedIndex := 13;
     end;

     t3oReportPoint:
     begin
     Node.ImageIndex := 8;
     Node.SelectedIndex := 9;
     if TG3Dobject(GObject).ObjectType = t3oReportPoint then
        begin
        if Node.Text = NomPartDatumID then
           begin
           FFixedPartDatumNode := Node;
           AbsoluteNominalPartDatum := Gobject.ReferencePoint;
           GObject.FPositionData.DesignTimeTranslation := AddLocation(GObject.FPositionData.DesignTimeTranslation,FPartDatumOffset);
           AbsoluteNominalPartDatum := Gobject.ReferencePoint;
//         AbsolutePartDatum := AddLocation(AbsoluteNominalPartDatum,PartDatumOffset);
           Node.ImageIndex := 10;
           Node.SelectedIndex := 11;
           end
        else
            begin
          if Node.Text = PartDatumID then
             begin
             FPartDatumNode := Node;
             // save standard position for runtime adjustment
             FPartDatumStandardPos :=GObject.FPositionData.DesignTimeTranslation;
             GObject.FPositionData.DesignTimeTranslation := AddLocation(GObject.FPositionData.DesignTimeTranslation,FPartDatumOffset);
             AbsolutePartDatum := Gobject.ReferencePoint;
//           AbsolutePartDatum := AddLocation(AbsolutePartDatum,PartDatumOffset);
             Node.ImageIndex := 10;
             Node.SelectedIndex := 11;
             end
          else
              begin
              if Node.Text = ToolDatumID then
                 begin
                 FToolDatumNode := Node;
                 GObject.FPositionData.DesignTimeTranslation := AddLocation(GObject.FPositionData.DesignTimeTranslation,FToolDatumOffset);
                 AbsoluteToolDatum := Gobject.ReferencePoint;
//                 AbsoluteToolDatum := AddLocation(AbsoluteToolDatum,ToolDatumOffset);
                 Node.ImageIndex := 10;
                 Node.SelectedIndex := 11;
                 end
              else
                  begin
                  // Here to add report points to ReportPoint List
                  // N.B. Tag is the number of the original name
                  inc(ReportPointCount);
                  SetLength(FReportPointList,ReportPointCount);
                  FReportPointList[ReportPointCount-1].ReportPointType := rpPartRelative;
                  FReportPointList[ReportPointCount-1].CurrentPosition := Gobject.ReferencePoint;
                  FReportPointList[ReportPointCount-1].NodeId := Node.ItemId;
                  end;
              end;
          end;
        end;
     end;
   end;//case
   end;

end;
TreeTable.BuildTableFrom(Self,WorldNode);
FullExpand;
MatchObjectListToTree;
BuildAllObjectsAndChildren;
// Now Update Axis Information
NumberofAxes := ObjectFile.ReadInteger('AxisData','NumAxes',0);
FNumberofAxes := NumberofAxes;
if NumberofAxes>0 then
   begin

   For AxisCount := 0 to NumberofAxes-1 do
       begin
       with AxisDefs[AxisCount] do
         begin
         SectionName := Format('Axis%d',[AxisCount]);
         ObjectName := ObjectFile.ReadString(SectionName,'ObjectName','None Selected');
         if OBjectName = 'None Selected' then
            begin
            AxisDataError := True;
            end
         else
             begin
             AXisObject := FindObject(ObjectName);
             Buffer   := ObjectFile.ReadString(SectionName,'isLinear','True');
             isLinear := (Buffer[1] = 't') or (Buffer[1]='T');
             MotionAxisIndex := ObjectFile.ReadInteger(SectionName,'MotionAxisIndex',0);
             Buffer   := ObjectFile.ReadString(SectionName,'isReversed','False');
             isReversed := (Buffer[1] = 't') or (Buffer[1]='T');
             Buffer   := ObjectFile.ReadString(SectionName,'AffectsPartDatum','False');
             AffectsPartDatum := (Buffer[1] = 't') or (Buffer[1]='T');
             Buffer   := ObjectFile.ReadString(SectionName,'AffectsToolDatum','False');
             AffectsToolDatum := (Buffer[1] = 't') or (Buffer[1]='T');
             Buffer := ObjectFile.ReadString(SectionName,'DatumTranslation','0.0,0.0,0.0');
             DatumTranslation := DecodeFileLocation(Buffer);
             Buffer := ObjectFile.ReadString(SectionName,'DatumRotation','0.0,0.0,0.0');
             DatumRotation := DecodeFileRotation(Buffer);
             DisplayTextIndex := ObjectFile.ReadInteger(SectionName,'DisplayTextIndex',0);
             end;
         end;
       end;
   end;

finally
ObjectFile.Free;
end;

FElectrodeOffset := Zero3D;
SetElectrodeOffsetToToolDatum(FElectrodeOffset);
if assigned(FPartDatumNode) then CreateNominalandActualVectors;

finally
StringBuffer.Free;
TreeList.Free;
DescriptionBuffer.Free;
NodeList.Free;
Result := teDataOK;
if not assigned(FPartDatumNode) then Result := teNoPartDatum;
if not assigned(FToolDatumNode) then Result := teNoToolDatum;
if AxisDataError then Result := teAxisSetupDataError;
end;
end;

function TG3DOTree.MakeObjectFromFile(FileName :TIniFile;NodeName : String;MakeVirtual : Boolean):TG3DObject;
var
GObject : TG3DObject;
OType   : T3DObjects;
Buffer  : String;

begin
OTYpe := t3oLine;
GObject := nil;
with FileName do
     begin
     Buffer := ReadString(NodeName,'ObjectType','None');
     if Buffer = 'Line' then Otype := t3oLine;
     if Buffer = 'Box' then Otype := t3oBox;
     if Buffer = 'Disc' then Otype := t3oDisc;
     if Buffer = 'ReportPoint' then Otype := t3oReportPoint;
     end;

Case Otype of
     t3oLine:
        begin
        GObject := TG3DLine.Create(Self);
        GObject.VirtualOnly := MakeVirtual;
        TG3DLine(GObject).Length := FileName.ReadFloat(NodeName,'Length',100);
        end;
     t3oBox:
        begin
        GObject := TG3DBox.Create(Self);
        GObject.VirtualOnly := MakeVirtual;
        TG3dBox(GObject).Length := FileName.ReadFloat(NodeName,'Length',123);
        TG3dBox(GObject).Width := FileName.ReadFloat(NodeName,'Width',123);
        TG3dBox(GObject).Height := FileName.ReadFloat(NodeName,'Height',123);
       end;
     t3oDisc:
        begin
        GObject := TG3DDisc.Create(Self);
        GObject.VirtualOnly := MakeVirtual;
        TG3dDisc(GObject).Radius := FileName.ReadFloat(NodeName,'Radius',100);
        TG3dDisc(GObject).Height := FileName.ReadFloat(NodeName,'Height',100);
        end;
     t3oReportPoint:
        begin
        GObject := TG3DReportPoint.Create(Self);
        GObject.VirtualOnly := MakeVirtual;
        end;

end; // case
if assigned(GObject) then
   begin
   Buffer := FileName.ReadString(NodeName,'ReferencePoint','0,0,0');
   Gobject.ReferencePoint := DecodeFileLocation(Buffer);
   Buffer := FileName.ReadString(NodeName,'DesignTimeTranslation','0,0,0');
   Gobject.FPositionData.DesignTimeTranslation := DecodeFileLocation(Buffer);
   Buffer := FileName.ReadString(NodeName,'DesignTimeRotation','0,0,0');
   Gobject.FPositionData.DesignTimeRotation := DecodeFileRotation(Buffer);
   Gobject.Colour := FileName.ReadInteger(NodeName,'FColour',16711680);
   Gobject.SelectColour := FileName.ReadInteger(NodeName,'FSelectColour',255);
   GObject.Name := NodeName;
   end;

Result :=Gobject;
end;



function TG3DOTree.DecodeFileLocation(FileEntry : String):TPLocation;
var
SLength : Integer;
ValStr  : String;
CommaPos : Integer;
begin
Result := Zero3D;
SLength := Length(FileEntry);
CommaPOs := Pos(',',FileEntry);
ValStr := Copy(FileEntry,1,CommaPos-1);
try
Result.x := StrToFloat(ValStr);
except
Result.x := 0.0;
end;
FileEntry := Copy(FileEntry,CommaPos+1,SLength);
CommaPOs := Pos(',',FileEntry);
ValStr := Copy(FileEntry,1,CommaPos-1);
try
Result.y := StrToFloat(ValStr);
except
Result.y := 0.0;
end;

FileEntry := Copy(FileEntry,CommaPos+1,SLength);
try
Result.z := StrToFloat(FileEntry);
except
Result.z := 0.0;
end;

end;

function TG3DOTree.DecodeFileRotation(FileEntry : String):TRotation;
var
SLength : Integer;
ValStr  : String;
CommaPos : Integer;
begin
Result := SetPRotation(0,0,0);
SLength := Length(FileEntry);
CommaPOs := Pos(',',FileEntry);
ValStr := Copy(FileEntry,1,CommaPos-1);
try
Result.x := StrToFloat(ValStr);
except
Result.x := 0.0;
end;
FileEntry := Copy(FileEntry,CommaPos+1,SLength);
CommaPOs := Pos(',',FileEntry);
ValStr := Copy(FileEntry,1,CommaPos-1);
try
Result.y := StrToFloat(ValStr);
except
Result.y := 0.0;
end;

FileEntry := Copy(FileEntry,CommaPos+1,SLength);
try
Result.z := StrToFloat(FileEntry);
except
Result.z := 0.0;
end;

end;



procedure TG3DOTree.SetObjectasAxisBase(ObjectNode : TTreeNode;AxisNumber :Integer);
var
ListIndex  : Integer;
GObject    : TG3DObject;
//TempNode   : TTreeNode;
begin
ListIndex := FindObjectListIndexFor(ObjectNode.Text);
if ListIndex = -1 then exit;
AxisDefs[AxisNumber].ObjectName := ObjectNode.Text;
TreeTable.ObjectTable[ListIndex].IsAxis:= True;
GObject := FindObject(ObjectNode.Text);
with AxisDefs[AxisNumber] do
     begin
     DatumTranslation := GObject.FPositionData.DesignTimeTranslation;
     DatumRotation :=  GObject.FPositionData.DesignTimeRotation;
     AxisObject := GObject;
     end;
end;


procedure TG3DOTree.SetNumberofAxes(Value : Integer);
begin
if (Value < 1) or (Value > 9) then exit;
FNumberofAxes := Value;
end;

procedure TG3DOTree.FastSetAxisPosition(AxisNumber : Integer;Position : Double);
var
AxisObject : TG3DObject;
Node       : TTreeNode;
MotionIndex  : Integer;
begin
if FUserMode = umEditor then exit;
if isValidAxis(AxisNumber) then
   begin
   AxisObject := GetJogObject(AxisNumber,Node);
   if AxisObject = nil then exit;
   MotionIndex := AxisDefs[AxisNumber].MotionAxisIndex;

   AxisDefs[AxisNumber].AxisPosition := Position;
   if AxisDefs[AxisNumber].isReversed then Position := -Position;

   with  AxisObject.FPositionData do
         begin
         case MotionIndex of
              0: DesignTimeTranslation.x := AxisDefs[AxisNumber].DatumTranslation.x+Position;
              1: DesignTimeTranslation.y := AxisDefs[AxisNumber].DatumTranslation.y+Position;
              2: DesignTimeTranslation.z := AxisDefs[AxisNumber].DatumTranslation.z+Position;
              3: DesignTimeRotation.x := AxisDefs[AxisNumber].DatumRotation.x+Position;
              4: DesignTimeRotation.y := AxisDefs[AxisNumber].DatumRotation.y+Position;
              5: DesignTimeRotation.z := AxisDefs[AxisNumber].DatumRotation.z+Position;
              end; //case
         end; // with
   end;
end;

procedure TG3DOTree.SetAxisPosition(AxisNumber : Integer;Position : Double);
var
AxisObject : TG3DObject;
Node       : TTreeNode;
MotionIndex  : Integer;
begin
if FUserMode = umEditor then exit;
if isValidAxis(AxisNumber) then
   begin
   AxisObject := GetJogObject(AxisNumber,Node);
   if AxisObject = nil then exit;
   MotionIndex := AxisDefs[AxisNumber].MotionAxisIndex;

   AxisDefs[AxisNumber].AxisPosition := Position;
   if AxisDefs[AxisNumber].isReversed then Position := -Position;

   with  AxisObject.FPositionData do
         begin
         case MotionIndex of
              0: DesignTimeTranslation.x := AxisDefs[AxisNumber].DatumTranslation.x+Position;
              1: DesignTimeTranslation.y := AxisDefs[AxisNumber].DatumTranslation.y+Position;
              2: DesignTimeTranslation.z := AxisDefs[AxisNumber].DatumTranslation.z+Position;
              3: DesignTimeRotation.x := AxisDefs[AxisNumber].DatumRotation.x+Position;
              4: DesignTimeRotation.y := AxisDefs[AxisNumber].DatumRotation.y+Position;
              5: DesignTimeRotation.z := AxisDefs[AxisNumber].DatumRotation.z+Position;
              end; //case
         end; // with
      UpdateObjectAndChildrenAtNode(Node);
   end;
end;

function  TG3DOTree.GetAxisPosition(AxisNumber : Integer): Double;
begin
if isValidAxis(AxisNumber) then
   begin
   Result := AxisDefs[AxisNumber].AxisPosition;
   end
else
   begin
   Result := 0.0;
   end

end;

function TG3DOTree.ReportPointExtentsOnSurface(Paper : TG3DPaper;var Centre :TScreenLocation):Textents;
var
Count : Integer;
Objectextents : TExtents;

begin
Result.MaxX := -11100000;
Result.MaxY := -11100000;
Result.MinX := 100000000;
Result.MinY := 100000000;

if Length(FReportPointList)<1 then
   begin
   Result.MaxX := 0;
   Result.MaxY := 0;
   Result.MinX := 0;
   Result.MinY := 0;
   end
else
    begin
    try
    For Count := 0 to Length(FReportPointList)-1 do
        begin
        with TG3DObject(TreeTable.ObjectTable[Count].GObject) do
             begin
             if (TG3DObject(TreeTable.ObjectTable[Count].GObject).Name <> PartDatumID)
            and (TG3DObject(TreeTable.ObjectTable[Count].GObject).Name <> NomPartDatumID)
            and (TG3DObject(TreeTable.ObjectTable[Count].GObject).Name <> 'NDPDatum') then
               begin
               ObjectExtents := CalculateExtents(Paper);
               if ObjectExtents.MaxX > Result.MaxX then Result.MaxX := ObjectExtents.MaxX;
               if ObjectExtents.MaxY > Result.MaxY then Result.MaxY:= ObjectExtents.MaxY;
               if ObjectExtents.MinX < Result.MinX then Result.MinX := ObjectExtents.MinX;
               if ObjectExtents.MinY < Result.MinY then Result.MinY := ObjectExtents.MinY;
               end;
             end;
        end;
    except
     Result.MaxX := 1;
     Result.MaxY := 1;
     Result.MinX := -1;
     Result.MinY := -1;
    end;
    end;
Centre.x := (Result.MaxX+Result.MinX)div 2;
Centre.y := (Result.MaxY+Result.MinY)div 2;
end;



function  TG3DOTree.AllObjectExtentsOnSurface(Paper : TG3DPaper;var Centre :TScreenLocation):Textents;
var
Count : Integer;
Objectextents : TExtents;

begin
Result.MaxX := -11100000;
Result.MaxY := -11100000;
Result.MinX := 100000000;
Result.MinY := 100000000;
For Count := 1 to TreeTable.ObjectCount-1 do
    begin
     with TG3DObject(TreeTable.ObjectTable[Count].GObject) do
         begin
         ObjectExtents := CalculateExtents(Paper);
         if ObjectExtents.MaxX > Result.MaxX then Result.MaxX := ObjectExtents.MaxX;
         if ObjectExtents.MaxY > Result.MaxY then Result.MaxY:= ObjectExtents.MaxY;
         if ObjectExtents.MinX < Result.MinX then Result.MinX := ObjectExtents.MinX;
         if ObjectExtents.MinY < Result.MinY then Result.MinY := ObjectExtents.MinY;
         end;
    end;
Centre.x := (Result.MaxX+Result.MinX)div 2;
Centre.y := (Result.MaxY+Result.MinY)div 2;
Result.Centre := Centre;
Result.OffCentreX := Centre.x/(Paper.Width div 2);
Result.OffCentreY := Centre.y/(Paper.Height div 2);
end;


procedure TG3DOTree.FitToHolePoints(Paper : TG3DPaper;First,Last: Integer);
var
Extent : TExtents;
Centre : TscreenLocation;
RangeX,RangeY  : Integer;
Scalex,Scaley  : Double;
begin
if not assigned(FHoleList) then exit;
If FHoleList.Count < 1 then exit;
Paper.Scale := 1;
Extent := HOleExtentsOnSurface(Paper,Centre,First,Last);
RangeX := Extent.Maxx-Extent.Minx;
RangeY := Extent.Maxy-Extent.Miny;
if RangeX = 0 then
 begin
 RangeX := 1;
 end;
if RangeY = 0 then
 begin
 RangeY := 1;
 end;
Scalex := Paper.Width/(RangeX); // the bigger this value the smaller the image
Scaley := Paper.Height/(RangeY); //the bigger this value the smaller the image
if Scalex < Scaley then
   begin
   // scale is in pixels per unit as it gets bigger the image gets smaller
   // the corrction must be less than 1 to increase image size
   Paper.Scale := (Paper.Scale/Scalex)*ExtentsOverScale;
   end
else
    begin
    Paper.Scale := (Paper.Scale/Scaley)*ExtentsOverScale;
    end;
CentreOnDisplay(Paper);
Extent := HOleExtentsOnSurface(Paper,Centre,First,Last);
Paper.XCentre := (Paper.Width) - Centre.x;
Paper.ZCentre := (Paper.Height) - Centre.y;
DrawAllObjectsOn(Paper,False);

end;

procedure TG3DOTree.FitToPointsBetweenIndexes(Index1,Index2:Integer;Paper : TG3DPaper);
var
Extent : TExtents;
Centre : TscreenLocation;
RangeX,RangeY  : Integer;
Scalex,Scaley  : Double;
begin
Paper.Scale := 1;
Extent := ReportPointExtentsOnSurfaceRanged(Index1,Index2,Paper,Centre);
RangeX := Extent.Maxx-Extent.Minx;
RangeY := Extent.Maxy-Extent.Miny;
if RangeX = 0 then
 begin
 RangeX := 1;
 end;
if RangeY = 0 then
 begin
 RangeY := 1;
 end;
Scalex := Paper.Width/(RangeX); // the bigger this value the smaller the image
Scaley := Paper.Height/(RangeY); //the bigger this value the smaller the image
if Scalex < Scaley then
   begin
   // scale is in pixels per unit as it gets bigger the image gets smaller
   // the corrction must be less than 1 to increase image size
   Paper.Scale := (Paper.Scale/Scalex)*ExtentsOverScale;
   end
else
    begin
    Paper.Scale := (Paper.Scale/Scaley)*ExtentsOverScale;
    end;
CentreOnDisplay(Paper);
Extent := ReportPointExtentsOnSurfaceRanged(Index1,Index2,Paper,Centre);
Paper.XCentre := (Paper.Width) - Centre.x;
Paper.ZCentre := (Paper.Height) - Centre.y;
DrawAllObjectsOn(Paper,False);
end;

procedure TG3DOTree.FitStackingAxes(Paper : TG3DPaper);
function SAExtents(Paper : TG3DPaper;var Centre :TScreenLocation):Textents;
var
FirstIndex,LastIndex : Integer;
Objectextents : TExtents;
Count : Integer;
OName : String;
begin
FirstIndex := GetObjectListIndexfor('NominalVector_A');
LastIndex  := GetObjectListIndexfor('AVTiming_B');
if (FirstIndex > -1) and (LastIndex > FirstIndex) then
  begin
Result.MaxX := -11100000;
Result.MaxY := -11100000;
Result.MinX := 100000000;
Result.MinY := 100000000;
For Count := FirstIndex to LastIndex do
    begin
    with TG3DObject(TreeTable.ObjectTable[Count].GObject) do
         begin
         OName :=TG3DObject(TreeTable.ObjectTable[Count].GObject).Name;
         if (Pos('Vec',OName) > 0) or (Pos('Timing',Oname) > 0) then
             begin
             ObjectExtents := CalculateExtents(Paper);
             if ObjectExtents.MaxX > Result.MaxX then Result.MaxX := ObjectExtents.MaxX;
             if ObjectExtents.MaxY > Result.MaxY then Result.MaxY:= ObjectExtents.MaxY;
             if ObjectExtents.MinX < Result.MinX then Result.MinX := ObjectExtents.MinX;
             if ObjectExtents.MinY < Result.MinY then Result.MinY := ObjectExtents.MinY;
             end;
         end;
    end;
    end;
Centre.x := (Result.MaxX+Result.MinX)div 2;
Centre.y := (Result.MaxY+Result.MinY)div 2;
end;
var
RangeX,RangeY  : Integer;
Scalex,Scaley  : Double;
Fextents : Textents;
Centre : TScreenLocation;
begin
Fextents := SAExtents(Paper,Centre);
RangeX := Fextents.Maxx-Fextents.Minx;
RangeY := Fextents.Maxy-Fextents.Miny;
if RangeX = 0 then
 begin
 RangeX := 1;
 end;
if RangeY = 0 then
 begin
 RangeY := 1;
 end;
Scalex := Paper.Width/(RangeX); // the bigger this value the smaller the image
Scaley := Paper.Height/(RangeY); //the bigger this value the smaller the image
if Scalex < Scaley then
   begin
   // scale is in pixels per unit as it gets bigger the image gets smaller
   // the corrction must be less than 1 to increase image size
   Paper.Scale := (Paper.Scale/Scalex)*ExtentsOverScale;
   end
else
    begin
    Paper.Scale := (Paper.Scale/Scaley)*ExtentsOverScale;
    end;
CentreOnDisplay(Paper);
Fextents := SAExtents(Paper,Centre);
Paper.XCentre := (Paper.Width) - Centre.x;
Paper.ZCentre := (Paper.Height) - Centre.y;
DrawAllObjectsOn(Paper,False);

end;

procedure TG3DOTree.FitToReportPoints(Paper : TG3DPaper);
var
Extent : TExtents;
Centre : TscreenLocation;
RangeX,RangeY  : Integer;
Scalex,Scaley  : Double;
begin
if GlobalUpdateCount > 0 then exit;
Paper.Scale := 1;
Extent := ReportPointExtentsOnSurface(Paper,Centre);
RangeX := Extent.Maxx-Extent.Minx;
RangeY := Extent.Maxy-Extent.Miny;
if RangeX = 0 then RangeX := 1;
if RangeY = 0 then RangeY := 1;
Scalex := Paper.Width/(RangeX); // the bigger this value the smaller the image
Scaley := Paper.Height/(RangeY); //the bigger this value the smaller the image
if Scalex < Scaley then
   begin
   // scale is in pixels per unit as it gets bigger the image gets smaller
   // the corrction must be less than 1 to increase image size
   Paper.Scale := (Paper.Scale/Scalex)*ExtentsOverScale;
   end
else
    begin
    Paper.Scale := (Paper.Scale/Scaley)*ExtentsOverScale;
    end;
CentreOnDisplay(Paper);
Extent := ReportPointExtentsOnSurface(Paper,Centre);
Paper.XCentre := (Paper.Width) - Centre.x;
Paper.ZCentre := (Paper.Height) - Centre.y;
DrawAllObjectsOn(Paper,EditorMode);
end;

procedure TG3DOTree.FitAllObjectsOnSurface(Paper : TG3DPaper);
var
Extent : TExtents;
Centre : TscreenLocation;
RangeX,RangeY  : Integer;
Scalex,Scaley  : Double;
begin
Paper.Scale := 10;
DrawAllObjectsOn(Paper,EditorMode);
If TreeTable.ObjectCount > 2 then
    begin
    Extent := AllObjectExtentsOnSurface(Paper,Centre);
    RangeX := Extent.Maxx-Extent.Minx;
    RangeY := Extent.Maxy-Extent.Miny;
    Scalex := Paper.Width/(RangeX); // the bigger this value the smaller the image
    Scaley := Paper.Height/(RangeY); //the bigger this value the smaller the image
    if Scalex < Scaley then
       begin
       // scale is in pixels per unit as it gets bigger the image gets smaller
       // the corrction must be less than 1 to increase image size
       Paper.Scale := (Paper.Scale/Scalex)*1.1;
       end
    else
        begin
        Paper.Scale := (Paper.Scale/Scaley)*1.1;
        end;
    DrawAllObjectsOn(Paper,EditorMode);
    CentreOnDisplay(Paper);
    Extent := AllObjectExtentsOnSurface(Paper,Centre);
    Paper.XCentre := (Paper.Width) - Centre.x;
    Paper.ZCentre := (Paper.Height) - Centre.y;
    end;
end;

procedure TG3DOTree.CentreObjectsOnSurface(Paper : TG3DPaper);
var
CurrentCentre : TScreenLocation;
Offset        : TScreenLocation;
DummyExtents  : TExtents;
begin
if TreeTable.ObjectCount < 2 then exit;
DummyExtents := AllObjectExtentsOnSurface(Paper,CurrentCentre);
Offset.x := -(CurrentCentre.x-Paper.XCentre);
Offset.y := -(CurrentCentre.y-Paper.ZCentre);
Paper.Translation := AddScreenLocation(Offset,Paper.Translation);

end;

procedure TG3DOTree.CentreObjectsOnReportPoints(Paper : TG3DPaper);
var
CurrentCentre : TScreenLocation;
Offset        : TScreenLocation;
DummyExtents  : TExtents;
begin
DummyExtents := ReportPointExtentsOnSurface(Paper,CurrentCentre);
Offset.x := -(CurrentCentre.x-Paper.XCentre);
Offset.y := -(CurrentCentre.y-Paper.ZCentre);
Paper.Translation := AddScreenLocation(Offset,Paper.Translation);

end;

function TG3DOTree.isValidAxis(AxisNumber : Integer):Boolean;
begin
Result := (AxisNumber>=0) and (AxisNumber<=FNumberofAxes-1)
end;

procedure TG3DOTree.SetUserMode(Value : TUsermodes);
begin
FUserMode := Value;
// n.b. reset all positions to datum or runtime for each axis based on mode
end;


procedure TG3DOTree.DatumAllAxes;
var
AxisCount : Integer;
begin
for AxisCount := 0 to NumberofAxes-1 do
    begin
    DatumAxis(AxisCount);
    end
end;

procedure TG3DOTree.DatumAxis(AxisIndex : Integer);
begin
if isValidAxis(AxisIndex) then
   begin
   SetAxisPosition(AxisIndex,0.0)
   end;
end;

procedure TG3DOTree.CentreOnDisplay(Paper : TG3DPaper);
var
DisplayCentre : TScreenLocation;
begin
DisplayCentre.x := 0;
DisplayCentre.y := 0;
Paper.ZCentre := Paper.Height div 2;
Paper.XCentre := Paper.Width div 2;
Paper.Translation := DisplayCentre;
end;

function TG3DOTree.GetReportPointName(PointNumber: Integer):String;
var
PointNode : TTreeNode;
begin
if PointNumber < Length(FReportPointList) then
   begin
   PointNode := Items.GetNode(FReportPointList[PointNumber].NodeId);
   if assigned(PointNode) then
      Result := PointNode.Text
   else
      Result :='';
   end
else
   begin
   Result := '';
   end         ;
end;



function TG3DOTree.GetReportVectorFromList(VectorNumber: Integer):TVectorData;
begin
UpdateReportData(False);
if VectorNumber < Length(FReportVectorList) then
   begin
   Result := FReportVectorList[VectorNumber]
   end;
end;

procedure TG3DOTree.FreezeVectorat(VIndex: Integer);
var
Point   : TG3DReportPoint;
begin
     Point := GetVectorPoint(1,FReportVectorList[Vindex]);
     Point.IsFrozen := True;
     Point := GetVectorPoint(2,FReportVectorList[Vindex]);
     Point.IsFrozen := True;
     FReportVectorList[Vindex].isFrozen := True;
end;

procedure TG3DOTree.UnFreezeVectorat(VIndex: Integer);
var
VecData : TVectorData;
Point1,Point2 : TG3DReportPoint;
DummyPoint1,DummyPoint2 : TPlocation;

begin
if VIndex >= Length(FReportVectorList) then exit;
VecData := GetReportVectorFromList(VIndex);
if not  VecData.isFrozen then exit;
case VecData.VectorType of
     rvPartRelative:
     begin
     if assigned(FPartDatumNode) then
        begin
        Point1 := GetVectorPoint(1,VecData);
        DummyPoint1 := Point1.ReferencePoint;
        Point2 := GetVectorPoint(2,VecData);
        DummyPoint2 := Point2.ReferencePoint;
        DummyPoint1 := LinearCorrected(SUbTractLocation(DummyPoint1,AbsolutePartDatum));
        DummyPoint1 := CalculatePartDatumRelativeLocationOfPointInSpace(DummyPoint1);

        DummyPoint2 := LinearCorrected(SUbTractLocation(DummyPoint2,AbsolutePartDatum));
        DummyPoint2 := CalculatePartDatumRelativeLocationOfPointInSpace(DummyPoint2);
        Point1.FPositionData.DesignTimeTranslation := LinearCorrected(DummyPoint1);
        Point2.FPositionData.DesignTimeTranslation := LinearCorrected(DummyPoint2);
        Point1.IsFrozen := False;
        Point2.IsFrozen := False;
        FReportvectorList[VIndex].isFrozen := False;
       end;
     end;
     rvToolRelative:
     begin
     end;
     end; // CASE

end;


function TG3DOTree.GetReportPointFromList(PointNumber: Integer):TPLocation;
begin
if PointNumber < Length(FReportPointList) then
   begin
   Result := FReportPointList[PointNumber].CurrentPosition;
   end
else
   begin
   Result := Zero3D;
   end         ;
end;


function TG3DOTree.CopyVectoratIndex(Index : Integer;VectorName : String):Integer;
var
OldVectorData  :TVectorData;
VectorIndex : Integer;
P1,P2       : TG3dReportPoint;
OldP1Pos,OldP2Pos : TPLocation;
AxisCount : Integer;
begin
Result := -1;
if Index >= Length(FReportVectorList) then exit;
OldVectorData := FReportVectorList[Index];
P1 := GetVectorPoint(1,OldVectorData);
Oldp1Pos := P1.FPositionData.DesignTimeTranslation;
P2 := GetVectorPoint(2,OldVectorData);
OlDP2Pos := P2.FPositionData.DesignTimeTranslation;
For AxisCount := 0 to 2 do
     begin
     if AxisDefs[AxisCount].isReversed then
        begin
        case AxisDefs[AxisCount].MotionAxisIndex of
          0:
            begin
            OldP1Pos.x := -OldP1Pos.x;
            OldP2Pos.x := -OldP2Pos.x;
            end;
          1:
            begin
            OldP1Pos.y := -OldP1Pos.y;
            OldP2Pos.y := -OldP2Pos.y;
            end;
          2:
            begin
            OldP1Pos.z := -OldP1Pos.z;
            OldP2Pos.z := -OldP2Pos.z;
            end;
          end; //case
        end; // if is reversed
     end;
VectorIndex := AddReportVectorBetween(OldP1Pos,OldP2Pos,VectorName,rvPartRelative);
UpdateReportData(True);
Result := VectorIndex;
end;


function TG3DOTree.AddReportVectorbetween(Point1,Point2 : TPlocation;VectorName : String;
                                          RVType : TReportVectorType):Integer;
var
ListPos : Integer;
ONode1    : TTreeNode;
ONode2    : TTreeNode;
FirstPoint : TG3dReportPoint;
SecondPoint : TG3dReportPoint;
TP1,Tp2 : TPlocation;
//Index1,Index2 : Integer;
begin
//TP1 := GTTransform(Point1);
//TP2 := GTTransform(Point2);
TP1 := Point1;
TP2 := Point2;
ListPos := Length(FReportVectorList);
if VectorName = '' then
   begin
   VectorName := Format('RVevtor%d',[ListPos]);
   end;
//SetLength(FReportVectorList,ListPos+1);
case RVType  of
   rvPartRelative:
   begin
   if not (FpartDatumNode = nil) then
      begin
      if ((VectorName = ActualVectorName) or (VectorName = AVTimingName)) and ActualVectorisFixed then
        begin
        AddReportPointat(TP1.x,TP1.y,TP1.z,'dgsTempPoint1',rpFixedPartRelative,True,False);
        AddReportPointat(TP2.x,TP2.y,TP2.z,'dgsTempPoint2',rpFixedPartRelative,True,False);
        end
      else
        begin
        AddReportPointat(TP1.x,TP1.y,TP1.z,'dgsTempPoint1',rpPartRelative,True,False);
        AddReportPointat(TP2.x,TP2.y,TP2.z,'dgsTempPoint2',rpPartRelative,True,False);
        end;
      end;
   end;

   rvDrillNominal:
   if not (FToolDatumNode = nil) then
      begin
      {Index1 :=}AddReportPointat(TP1.x,TP1.y,TP1.z,'dgsTempPoint1',rpNPDRelative,True,False);
      {Index2 :=}AddReportPointat(TP2.x,TP2.y,TP2.z,'dgsTempPoint2',rpNPDRelative,True,False);
      end;

   rvToolApproach:
   if not (FToolDatumNode = nil) then
      begin
      {Index1 :=}AddReportPointat(TP1.x,TP1.y,TP1.z,'dgsTempPoint1',rpToolApproach,True,False);
      {Index2 :=}AddReportPointat(TP2.x,TP2.y,TP2.z,'dgsTempPoint2',rpToolApproach,True,False);
      end;
   rvToolRelative:
   begin
   if not (FToolDatumNode = nil) then
      begin
      {Index1 :=}AddReportPointat(TP1.x,TP1.y,TP1.z,'dgsTempPoint1',rpToolRelative,True,False);
      {Index2 :=}AddReportPointat(TP2.x,TP2.y,TP2.z,'dgsTempPoint2',rpToolRelative,True,False);
      end;
   end;

   rvMachineRelative:
      begin
      {Index1 :=}AddReportPointat(TP1.x,TP1.y,TP1.z,'dgsTempPoint1',rpMachineRelOnPartDatum,True,False);
      {Index2 :=}AddReportPointat(TP2.x,TP2.y,TP2.z,'dgsTempPoint2',rpMachineRelOnPartDatum,True,False);
      end;

   end;
SetLength(FReportVectorList,ListPos+1);

MatchObjectListToTree;
FirstPoint := FindObjectandNode('dgsTempPoint1',Onode1);
FirstPoint.HasAParent := True;
FirstPoint.PointType := rpFirstVector;
if VectorName = NominalVectorName then
   begin
   FirstPOint.VectorOwner := voNominal;
   end;
if VectorName = ActualVectorName then
   begin
   FirstPOint.VectorOwner := voActual;
   end;


FirstPoint.Name := VectorName+'_A';
ONode1.Text :=  FirstPoint.Name;

SecondPoint := FindObjectandNode('dgsTempPoint2',Onode2);
SecondPoint.HasAParent := True;
SecondPoint.PointType := rpSecondVector;
SecondPoint.LinkPoint := FirstPoint;
SecondPoint.Name := VectorName+'_B';
if VectorName = NominalVectorName then
   begin
   SecondPoint.VectorOwner := voNominal;
   end;
if VectorName = ActualVectorName then
   begin
   SecondPoint.VectorOwner := voActual;
   end;

if VectorName = NDPName then
   begin
   NDPVectorIndex := ListPos;
   SecondPoint.VectorOwner := voNDP;
   end;

if VectorName = InvNDPName then
   begin
   NDPVectorIndex := ListPos;
   SecondPoint.VectorOwner := InvvoNDP;
   end;

if VectorName = ApproachVectorName then
   begin
   ApproachVectorIndex := ListPos;
   SecondPoint.VectorOwner := voApproach;
   end;


if VectorName = AttachElectrodeName then
   begin
   AttachedElecIndex := ListPos;
   end;

ONode2.Text :=  SecondPoint.Name;

with FReportVectorList[ListPos] do
     begin
     VectorType      := RVType;
     Point1NodeId    := ONode1.ItemId;
     Point2NodeId    := ONode2.ItemId;
     Name            := VectorName;
     isFrozen          := False;
     end;

MatchObjectListToTree;
UpdateObjectAndChildrenAtNode(FPartDatumNode);
Result := ListPos;
end;

function isvalidPointName(Pname : String):Boolean;
 var temp : Integer;
 begin
 Result := False;
 if Length(PName)< 2 then
  begin
  exit;
  end
 else
  begin
  try
  temp := StrtoInt(PName[1]);
  except
  Result := True;
  end;
  end;
 end;

function TG3DOTree.AddHoleToHoleListat(x,y,z : Double;PointName:String;LinearCorrect: Boolean;
                    PColour :TColor;PStyle :TPointDisplayStyle): Integer;
var
ListPos : Integer;
NewPoint : TG3DReportPoint;
TempName : String;
ONode    : TTreeNode;
PLocation : TPLocation;
HoleEntry : pHoleEntry;
begin
try
Result := -1;
NewPOint := nil;
if Objectcount < 2 then exit;
if not isvalidPointName(PointName) then
  begin
  Result := -1;
  raise EGEOError.Create(Format('Error Adding Hole to HoleList (%s)',[PointName]));
  exit;
  end;

except
raise EGEOError.Create(Format('Error Adding Hole to HoleList (%s)',[PointName]));
Result := -1;
end;
if not (FpartDatumNode = nil) then
      begin
      NewPOint := AddObjectAtNode2(FPartDatumNode,t3oReportPoint,False,Onode);
      NewPOint.HasAParent := True;
      NewPOint.Colour := PColour;
      NewPoint.Name := PointName;
      Newpoint.PointDisplayOnly := True;
      NewPoint.PointStyle := PStyle;
      PLocation := SetPrecisePoint3d(x,y,z);
      end;
if assigned(NewPoint) then
   begin
   New(HoleEntry);
   NewPoint.FPositionData.DesignTimeTranslation :=PLocation;
   HoleEntry.ReportPointType := rpPartRelative;
   HoleEntry.NodeId := ONode.ItemId;
   NewPoint.Tag := FHoleList.Count;
   ONode.Text := NewPoint.Name;
   FHoleList.Add(HoleEntry);
   end;

end;


function TG3DOTree.AddReportPointat(x,y,z : Double;PointName : String;
                   RPType : TReportPointType;LinearCorrect : Boolean;
                   DisplayPointOnly : Boolean): Integer;

function isvalidPointName(var Pname : String):Boolean;
 var temp : Integer;
 FirstChar : Char;
 begin
 Result := False;
 if Length(PName)< 2 then
  begin
  exit;
  end
 else
  begin
  FirstChar := PName[1];
  case FirstChar of
    'a'..'z','A'..'Z': Result := True;
  end; // case
 end;
end;

var
ListPos : Integer;
NewPoint : TG3DReportPoint;
ONode    : TTreeNode;
PLocation : TPLocation;
begin
try
NewPOint := nil;
Result := -1;
if Objectcount < 2 then exit;
if not isvalidPointName(PointName) then
  begin
  Result := -1;
  raise EGEOError.Create(Format('Invalid point name added (%s)',[PointName]));
  exit;
  end;
ListPos := Length(FReportPointList);
Result := ListPos;
SetLength(FReportPointList,ListPos+1);
case RPType of
   rpPartRelative:
   begin
   // Here if Parent of ReportPoint is the Part Datum at it current POsition
   if not (FpartDatumNode = nil) then
      begin
      NewPOint := AddObjectAtNode2(FPartDatumNode,t3oReportPoint,False,Onode);
      NewPOint.HasAParent := True;
      NewPoint.Name := PointName;
      Newpoint.PointDisplayOnly :=DisplayPointOnly;
      PLocation := SetPrecisePoint3d(x,y,z);
      end;
   end;

   rpFixedPartRelative:
   begin
   if not (FFixedpartDatumNode = nil) then
      begin
      NewPOint := AddObjectAtNode2(FFixedPartDatumNode,t3oReportPoint,False,Onode);
      NewPOint.HasAParent := True;
      NewPoint.Name := PointName;
      Newpoint.PointDisplayOnly :=DisplayPointOnly;
      PLocation := SetPrecisePoint3d(x,y,z);
      end;
   end;

   rpToolApproach:
   Begin
   if not (FToolDatumNode = nil) then
      begin
   // Here if Parent of ReportPoint is the Tool Datum at is currentPosition
      NewPOint := AddObjectAtNode2(FElectrodeNode,t3oReportPoint,False,Onode);
      NewPOint.HasAParent := True;
      NewPoint.Name := PointName;
      Newpoint.PointDisplayOnly :=DisplayPointOnly ;
      PLocation := SetPrecisePoint3d(x,y,z);
   end;
   end;

   rpNPDRelative:
   begin
   // Here if Parent of ReportPoint is the Part Datum at it current POsition
   if not (FNDPDatumNode = nil) then
      begin
      NewPOint := AddObjectAtNode2(FNDPDatumNode,t3oReportPoint,False,Onode);
      NewPOint.HasAParent := True;
      NewPoint.Name := PointName;
      Newpoint.PointDisplayOnly :=DisplayPointOnly;
      PLocation := SetPrecisePoint3d(x,y,z);
      end;
   end;

   rpToolRelative:
   begin
   if not (FToolDatumNode = nil) then
      begin
   // Here if Parent of ReportPoint is the Tool Datum at is currentPosition
      NewPOint := AddObjectAtNode2(FToolDatumNode,t3oReportPoint,False,Onode);
      NewPOint.HasAParent := True;
      NewPoint.Name := PointName;
      Newpoint.PointDisplayOnly :=DisplayPointOnly;
      PLocation := SetPrecisePoint3d(x,y,z);
   end;
   end;

   rpMachineRelOnPartDatum:
   begin
   // here to add a point relative to the machine datum regardless of axis position
   UpdateReportData(True);

   NewPOint := AddObjectAtNode2(FPartDatumNode,t3oReportPoint,False,Onode);
   NewPOint.HasAParent := True;
   NewPoint.Name := PointName;
   Newpoint.PointDisplayOnly :=DisplayPointOnly;
   PLocation := SetPrecisePoint3d(x,y,z);
   PLocation :=CalculatePartDatumRelativeLocationOfPointInSpace(PLocation);
   end;

   end; // case
   if LinearCorrect then PLocation := GTLinearCorrected(PLocation);
//NGT!!!   if LinearCorrect then PLocation := LinearCorrected(PLocation);
if assigned(NewPoint) then
   begin
   NewPoint.FPositionData.DesignTimeTranslation :=GTTransform(PLocation);
   FReportPointList[ListPos].ReportPointType := RPType;
   FReportPointList[ListPos].NodeId := ONode.ItemId;
   NewPoint.Tag := ListPos;
   ONode.Text := NewPoint.Name;
   Newpoint.PointDisplayOnly :=DisplayPointOnly;
   end;
ConditionalMatchObjectListToTree;
except
Result := -1;
raise;
end;
end;

procedure TG3DOTree.DeleteAllVectors;
begin
while Length(FReportVectorList) > 0 do
      begin
      DeleteVectorat(Length(FReportVectorList)-1);
      end;
ActualVectorIndex      := -1;
ATVectorIndex          := -1;
NominalVectorIndex     := -1;
NTVectorIndex          := -1;
end;


procedure  TG3DOTree.DeleteVectorat(Index: Integer);
var
ListPos : Integer;
StartLength : Integer;
Point       : TG3DReportPoint;
PointNode   : TTreeNode;
PointIndex  : Integer;

VecData     : TVectorData;

begin
StartLength := Length(FReportVectorList);
If Index >= StartLength then exit;
//First remove the two nodes from the tree;
VecData := FReportVectorList[Index];
PointNode := Items.GetNode(VecData.Point1NodeId);
Point := TG3DReportPoint(PointNode.Data);
PointIndex := GetReportPointIndexfor(Point.Name);
DeleteReportPoint(PointIndex);
PointNode := Items.GetNode(VecData.Point2NodeId);
Point := TG3DReportPoint(PointNode.Data);
PointIndex := GetReportPointIndexfor(Point.Name);
DeleteReportPoint(PointIndex);
if Index < (Startlength-1) then
   begin
   For ListPos := Index to StartLength-2 do
     begin
     FReportVectorList[ListPos] := FReportVectorList[ListPos+1];
     end;
   end;
SetLength(FReportVectorList,StartLength-1);
UpdateReportData(True);
end;

procedure TG3DOTree.DeleteRuntimeReportPoints;
begin
while Length(FReportPointList) > 0 do
      begin
      FastDeleteReportPoint(Length(FReportPointList)-1);
      end;
MatchObjectListToTree;
end;


function TG3DOTree.GetReportPointCalled(aName : String): TPlocation;
var
PIndex :Integer;
begin
PIndex := GetReportPointIndexfor(aName);
if PIndex >=0 then
   begin
   Result := GetReportPointFromList(PIndex);
   end;
end;

procedure TG3DOTree.DeleteReportPointCalled(aName : String);
var
PIndex :Integer;
begin
PIndex := GetReportPointIndexfor(aName);
if PIndex >=0 then
   begin
   DeleteReportPoint(PIndex);
   end;
end;

procedure TG3DOTree.DeleteReportPoint(Index: Integer);
var
ListPos : Integer;
StartLength : Integer;
PointNode    : TTreeNode;
begin
StartLength := Length(FReportPointList);
If Index >= StartLength then exit;
//First remove the node from the tree;
PointNode := Items.GetNode(FReportPointList[Index].NodeId);
DeleteObjectandChildrenAtNode(PointNode);

if Index < Startlength-1 then
   begin
   For ListPos := Index to StartLength-2 do
     begin
     FReportPointList[ListPos] := FReportPointList[ListPos+1];
     end;
   end;
SetLength(FReportPointList,StartLength-1);

end;

procedure TG3DOTree.FastDeleteReportPoint(Index: Integer);
var
ListPos : Integer;
StartLength : Integer;
PointNode    : TTreeNode;
begin
StartLength := Length(FReportPointList);
If Index >= StartLength then exit;
//First remove the node from the tree;
PointNode := Items.GetNode(FReportPointList[Index].NodeId);
FastDeleteObjectandChildrenAtNode(PointNode);
if Index < Startlength-1 then
   begin
   For ListPos := Index to StartLength-2 do
     begin
     FReportPointList[ListPos] := FReportPointList[ListPos+1];
     end;
   end;
SetLength(FReportPointList,StartLength-1);
end;

{function TG3DOTree.FastDeleteReportPointifDisplayOnly(Index: Integer):Boolean;
var
ListPos : Integer;
StartLength : Integer;
PointNode    : TTreeNode;
Point : TG3DReportPoint;
begin
Result := False;
StartLength := Length(FReportPointList);
If Index >= StartLength then exit;
//First remove the node from the tree;
PointNode := Items.GetNode(FReportPointList[Index].NodeId);
Point := PointNode.Data;
if Point.PointDisplayOnly then
  begin
  FastDeleteObjectandChildrenAtNode(PointNode);
  Result := True;
  if Index < Startlength-1 then
   begin
   For ListPos := Index to StartLength-2 do
     begin
     FReportPointList[ListPos] := FReportPointList[ListPos+1];
     end;
   end;
  SetLength(FReportPointList,StartLength-1);
  end;
end;}

function TG3DOTree.GetNumberReportVectors:Integer;
begin
Result := Length(FReportVectorList);
end;

function TG3DOTree.GetNumberReportPoints:Integer;
begin
Result := Length(FReportPointList);
end;

function TG3DOTree.LinearCorrected(Position: TPlocation):TPlocation;
var
AxisCount : Integer;
begin
 Result := GTLinearCorrected(Position);
 exit;
 Result := Position;
 For AxisCount := 0 to FNumberofAxes do
     begin
     if AxisDefs[AxisCount].isReversed then
        begin
        case AxisDefs[AxisCount].MotionAxisIndex of
          0:Result.x := -Result.x;
          1:Result.y := -Result.y;
          2:Result.z := -Result.z;
          end; //case
        end; // if is reversed
     end;
end;

function TG3DOTree.GTLinearCorrected(Position: TPlocation):TPlocation;
var
AxisCount : Integer;
begin
 Result := Position;
 Result.x := -Result.x;
{ For AxisCount := 0 to FNumberofAxes do
     begin
     if AxisDefs[AxisCount].isReversed then
        begin
        case AxisDefs[AxisCount].MotionAxisIndex of
          0:Result.x := -Result.x;
          1:Result.y := -Result.y;
          2:Result.z := -Result.z;
          end; //case
        end; // if is reversed
    end;}
end;



procedure TG3DOTree.CreateNominalandActualVectors;
begin
If EditorMode Then exit;
NominalVectorIndex := GetVectorIndexfor(NominalVectorName);
if NominalVectorIndex = -1 then
   begin
   NominalVectorIndex := AddReportVectorBetween(Zero3D,SetPrecisePoint3D(0,0,100),NominalVectorName,rvPartRelative);
   end;
if NTVectorIndex = -1 then
   begin
   NTVectorIndex := AddReportVectorBetween(Zero3D,SetPrecisePoint3D(50,0,0),NVTimingName,rvPartRelative);
   end;


ActualVectorIndex := GetVectorIndexfor(ActualVectorName);
if ActualVectorIndex = -1 then
   begin
   ActualVectorIndex := AddReportVectorBetween(Zero3D,SetPrecisePoint3D(0,0,100),ActualVectorName,rvPartRelative);
   end;
if ATVectorIndex = -1 then
   begin
   ATVectorIndex := AddReportVectorBetween(Zero3D,SetPrecisePoint3D(50,0,0),AVTimingName,rvPartRelative);
   end;

{FixedActualVectorIndex := GetVectorIndexfor(FixedActualVectorName);
if FixedActualVectorIndex = -1 then
   begin
   FixedActualVectorIndex := AddReportVectorBetween(Zero3D,SetPrecisePoint3D(0,0,100),FixedActualVectorName,rvFixedPartRelative,tuUpdateTreelist);
   end;
FixedATVectorIndex  := GetVectorIndexfor(FixedAVTimingName);
if FixedATVectorIndex = -1 then
   begin
   FixedATVectorIndex := AddReportVectorBetween(Zero3D,SetPrecisePoint3D(50,0,0),FixedAVTimingName,rvFixedPartRelative,tuUpdateTreelist);
   end;}
end;


function TG3DOTree.GetCorrectedPositionRelativeTo(RPoint :TPLocation;DatumLocation : TPLocation):TPlocation;
var
AxisCount : Integer;
begin
Result := SubtractLocation(RPoint,DatumLocation);
 For AxisCount := 0 to FNumberofAxes do
     begin
     if AxisDefs[AxisCount].isReversed then
        begin
        case AxisDefs[AxisCount].MotionAxisIndex of
          0:Result.x := -Result.x;
          1:Result.y := -Result.y;
          2:Result.z := -Result.z;
          end; //case
        end; // if is reversed
     end;
end;

procedure TG3DOTree.UpdateHoleListPOsition;
var
HoleEntry : pHoleEntry;
HoleNumber : INteger;
PointNode : TTreeNode;
CurrentPoint : TG3dReportPoint;
begin
if not assigned(FHoleList) then exit;
if FHoleList.Count < 1 then exit;
for HoleNumber := 0 to FHoleList.Count-1 do
  begin
  HoleEntry := FHoleList[HoleNumber];
  PointNode := Items.GetNode(HoleEntry.NodeId);
   if assigned(PointNode) then
      begin
      CurrentPoint := PointNode.Data;
      HoleEntry.CurrentPosition := GetCorrectedPositionRelativeTo(CurrentPoint.ReferencePoint,AbsolutePartDatum);
      end;
  end;

end;

procedure TG3DOTree.UpdateReportData(RebuildFirst : Boolean);
function AlwaysPositive(InputAngle :Double): Double;
begin
Result := InputAngle;
while Result < 0 do
   begin
   Result := Result+360;
   end
end;
var
ReportCount  : Integer;
CurrentPoint : TG3dReportPoint;
PointNode    : TTreeNode;
VPoint1,Vpoint2 : TG3dReportPoint;
CPLocation :   TPlocation;
begin
if RebuildFirst then
   begin
   BuildAllObjectsAndChildren;
   end;
  if assigned(FPartDatumNode) then
     begin
     CurrentPoint := FPartDatumNode.Data;
     CPLocation := CurrentPoint.ReferencePoint;
     FPartDatumLocation := GTTransform(GetCorrectedPositionRelativeTo(CurrentPoint.ReferencePoint,AbsolutePartDatum));
     end;
  if assigned(FToolDatumNode) then
     begin
     CurrentPoint := FToolDatumNode.Data;
     FToolDatumLocation := GTTransform(GetCorrectedPositionRelativeTo(CurrentPoint.ReferencePoint,AbsolutePartDatum));
     end;

  if assigned(FElectrodeNode) then
     begin
     CurrentPoint := FElectrodeNode.Data;
     FElectrodeLocation := GTTransform(GetCorrectedPositionRelativeTo(CurrentPoint.ReferencePoint,AbsolutePartDatum));
     end;


  if assigned(FNDPDatumNode) then
     begin
     CurrentPoint := FNDPDatumNode.Data;
     FHoleLocation := GTTransform(GetCorrectedPositionRelativeTo(CurrentPoint.ReferencePoint,AbsolutePartDatum));
     end;

  if assigned(FHoleNode) then
     begin
     CurrentPoint := FHoleNode.Data;
     FCurrentHoleLocation := GTTransform(GetCorrectedPositionRelativeTo(CurrentPoint.ReferencePoint,AbsolutePartDatum));
     end;

if Length(FReportPointList) > 0 then
   begin
   For ReportCount := 0 to Length(FReportPointList)-1 do
       begin
       PointNode := Items.GetNode(FReportPointList[ReportCount].NodeId);
       if assigned(PointNode) then
          begin
          CurrentPoint := PointNode.Data;
          FReportPointList[ReportCount].CurrentPosition := GTTransform(GetCorrectedPositionRelativeTo(CurrentPoint.ReferencePoint,AbsolutePartDatum));
          end;
       end;
   end;
// UpdateHoleListPOsition;
if Length(FReportVectorList) > 0 then
   begin
   For ReportCount := 0 to Length(FReportVectorList)-1 do
      begin
      VPoint1 := GetVectorPoint(1,FReportVectorList[ReportCount]);
      VPoint2 := GetVectorPoint(2,FReportVectorList[ReportCount]);
      with
      FReportVectorList[ReportCount] do
           begin
           Point1Position  := GTTransform(GetCorrectedPositionRelativeTo(VPoint1.ReferencePoint,AbsolutePartDatum));
           Point2Position  := GTTransform(GetCorrectedPositionRelativeTo(VPoint2.ReferencePoint,AbsolutePartDatum));
           CentrePosition  := PPreciseMidPoint3D(Point1Position,Point2Position);
           Length          := PPreciseLineLength3D(Point1Position,Point2Position);
           Rotation        := Anglesbetween3DPoints(Point1Position,Point2Position);
           Rotation.x      := AlwaysPositive(Rotation.x);
           Rotation.y      := AlwaysPositive(Rotation.y);
           Rotation.z      := AlwaysPositive(Rotation.z);
           end;
      end;
   end;
end;

procedure TG3DOTree.UpdateReportKnownDataPoints(RebuildFirst : Boolean);
function AlwaysPositive(InputAngle :Double): Double;
begin
Result := InputAngle;
while Result < 0 do
   begin
   Result := Result+360;
   end
end;
var
ReportCount  : Integer;
CurrentPoint : TG3dReportPoint;
begin
if RebuildFirst then
   begin
   BuildAllObjectsAndChildren;
   end;
  if assigned(FPartDatumNode) then
     begin
     CurrentPoint := FPartDatumNode.Data;
     FPartDatumLocation := GetCorrectedPositionRelativeTo(CurrentPoint.ReferencePoint,AbsolutePartDatum);
     end;
  if assigned(FToolDatumNode) then
     begin
     CurrentPoint := FToolDatumNode.Data;
     FToolDatumLocation := GetCorrectedPositionRelativeTo(CurrentPoint.ReferencePoint,AbsolutePartDatum);
     end;

  if assigned(FElectrodeNode) then
     begin
     CurrentPoint := FElectrodeNode.Data;
     FElectrodeLocation := GetCorrectedPositionRelativeTo(CurrentPoint.ReferencePoint,AbsolutePartDatum);
     end;

  if assigned(FHoleNode) then
     begin
     CurrentPoint := FHoleNode.Data;
     FHoleLocation := GetCorrectedPositionRelativeTo(CurrentPoint.ReferencePoint,AbsolutePartDatum);
     end;
end;


procedure TG3DOTree.TreePopupDeleteClick(Sender : TObject);
var
SelectedObject : TG3DObject;
begin
if not EditorMode then exit;
if not (selected =nil) then
   begin
   if Selected.Text =WorldNodeID then exit;
   SelectedObject := Selected.Data;
   if SelectedObject.Name = PartDatumID then
      begin
      DeleteObjectandChildrenAtNode(Selected);
      FPartDatumNode := nil;
      exit;
      end;

   if SelectedObject.Name = NomPartDatumID then
      begin
      DeleteObjectandChildrenAtNode(Selected);
      FFixedPartDatumNode := nil;
      exit;
      end;

   if Selected.Text =ToolDatumID then
      begin
      DeleteObjectandChildrenAtNode(Selected);
      FToolDatumNode := nil;
      exit;
      end;
   DeleteObjectandChildrenAtNode(Selected);
   end;
end;

procedure TG3DOTree.TreeShowPointsClick(Sender : TObject);
begin
if not (selected =nil) then
   begin
   if Selected.Text <>WorldNodeID then
      begin
      //DeleteObjectandChildrenAtNode(Selected);
      end;
   end;
end;

procedure TG3DOTree.UpdatePartDatumPosition;
var
DatumObject : TG3Dobject;
begin
if FPartDatumNode <> nil then
   begin
   DatumObject := FPartDatumNode.Data;
   AbsolutePartDatum := GTTransForm(DatumObject.ReferencePoint);
   DatumObject := FFixedPartDatumNode.Data;
   AbsoluteNominalPartDatum := GTTransForm(DatumObject.ReferencePoint);
   end;
end;

procedure TG3DOTree.UpdateToolDatumPosition;
var
DatumObject : TG3Dobject;
begin
if FToolDatumNode <> nil then
   begin
   DatumObject := FToolDatumNode.Data;
   AbsoluteToolDatum := GTTransform(DatumObject.ReferencePoint);
   end;
end;

function TG3DOTree.GetVectorDifference(Vector1Index,Vector2Index: Integer;var Difference:TVectorDifference):Boolean;
begin
Result := False;
UpdateReportData(False);
if Vector1Index >= Length(FReportVectorList) then exit;
if Vector2Index >= Length(FReportVectorList) then exit;
Difference.Rotation := SubtractRotation(FReportVectorList[Vector1Index].Rotation,FReportVectorList[Vector2Index].Rotation);
Difference.Translation := SubtractLocation(FReportVectorList[Vector1Index].CentrePosition,FReportVectorList[Vector2Index].CentrePosition);
Result:=True;
end;



function TG3DOTree.GetAllAxisPositions:TAxisPosn;
var
AxisCount : Integer;
begin
For AxisCount := 0 to FNumberofAxes-1 do
   begin
   Result[AxisCount] := AxisPosition[AxisCount];
   end;
end;

procedure TG3DOTree.MoveAllAxes(AxisCount : Integer;AxisArray : TAxisMoveArray);
var
AxNum : Integer;
begin

For AxNum := 0 to AxisCount-1 do
  begin
  if AxisArray[AxNum].Move then
     begin
     FastSetAxisPosition(AxNum,AxisArray[AxNum].Position)
     end
  end;
BuildAllObjectsAndChildren;
end;

procedure TG3DOTree.RestoreAllAxisPOsitions(SavedPosn : TAxisPosn);
var
AxisCount : Integer;
begin
For AxisCount := 0 to FNumberofAxes-1 do
   begin
   if SavedPOsn[AxisCount] <> GetAxisPosition(AxisCount) then FastSetAxisPosition(AxisCount,SavedPOsn[AxisCount]);
   end;
 BuildAllObjectsAndChildren;
end;




procedure TG3DOTree.SetElectrodeOffsetToToolDatum(EPosition : TPLocation);
var
ELectrodeObject : TG3DReportPoint;
begin
if EditorMode then exit;
if not assigned(FToolDatumNode) then exit;
if not assigned(FELectrodeNode) and (not EditorMode) then
   begin
   AddObjectatNode(FToolDatumNode,t3oELectrodeNode,False);
   end;
FElectrodeOffset := EPOsition;
ELectrodeObject := FELectrodeNode.Data;
EPosition := LinearCorrected(EPosition);
ELectrodeObject.FPositionData.DesignTimeTranslation := EPosition;
MatchObjectListToTree;
UpdateReportData(True);
end;

procedure TG3DOTree.GTSetUniqueElectrodeOffsets(XYZOffset: TPLocation;Standoff : Double);
var
ELectrodeObject : TG3DReportPoint;
CurrentOffset : TPLocation;
TransposedOffset: TPLocation;
NewOffset     : TPLocation;
AppVec :  TVectordata;
VecRot : Double;
RadVecRot : Double;
OffsetMagnitude : Double;
DeltaVec : TPLocation;
StandoffDelta: TXYZ;
begin
if not assigned(FELectrodeNode) then exit;
DatumAllAxes;
UpdateFixedIndexes;
if ApproachVectorIndex <0  then exit ;
AppVec := FReportVectorList[ApproachVectorIndex];
VecRot := AppVec.Rotation.y;
RadVecRot := DegToRad(VecRot);
CurrentOffset := FElectrodeOffset;
TransposedOffset.x := XYZOffset.x*Cos(RadVecRot);
TransposedOffset.y := -XYZOffset.y;
TransposedOffset.z := XYZOffset.x*Sin(RadVecRot);
//Invoke the Offfsets
NewOffset := AddLocation(TransposedOffset,CurrentOffset);
ELectrodeObject := FELectrodeNode.Data;
NewOffset := LinearCorrected(NewOffset);
// Now add standoff along tool vector

GetGTMachineToolRetractVector(Standoff, StandoffDelta);
NewOffset.x := NewOffset.x+StandoffDelta.X;
NewOffset.y := NewOffset.Y+StandoffDelta.Y;
NewOffset.z := NewOffset.z+StandoffDelta.Z;

ELectrodeObject.FPositionData.DesignTimeTranslation := NewOffset;
MatchObjectListToTree;
UpdateReportData(True);
FAppliedStandoff := Standoff;
end;


function TG3DOTree.GetReportPointIndexfor(PointName : String):Integer;
var
ListCount : Integer;
Pname     : String;
TestName  : String;
begin
Result := -1;
TestName := AnsiUpperCase(PointName);
for ListCount := 0 to Length(FReportPointList)-1 do
    begin
    PName := AnsiUpperCase(GetReportPointName(ListCount));
    if PName = TestName then
       begin
       Result := ListCount;
       exit;
       end;
    end;
end;

function TG3DOTree.GetObjectListIndexfor(PointName: String): Integer;
var
TestName  : String;
begin
Result := -1;
TestName := AnsiUpperCase(PointName);
Result := TreeTable.TableIndexforObjectName(TestName);
end;


function TG3DOTree.GetVectorIndexfor(VectorName : String):Integer;
var
ListCount : Integer;
begin
Result := -1;
for ListCount := 0 to Length(FReportVectorList)-1 do
    begin
    if TVectorData(FReportVectorList[ListCount]).Name = VectorName then
       begin
       Result := ListCount;
       exit;
       end;
    end;
end;

function TG3DOTree.DeleteVectorCalled(VectorName : String):Boolean;
var
VIndex :Integer;
begin
Result := False;
VIndex := GetVectorIndexfor(VectorName);
if Vindex >= 0 then
   begin
   DeleteVectorat(VIndex);
   Result := True;
   end;
UpdateFixedIndexes;
end;

procedure Register;
begin
  RegisterComponents('G3D', [TG3DOTree]);
end;





procedure TG3DOTree.DeSelectAllVectors;
var
VectorCount : Integer;
Point       : TG3dReportPoint;
begin
For VectorCount := 0 to Length(FReportVectorList)-1 do
    begin
    Point := GetVectorPoint(1,FReportVectorList[VectorCount]);
    Point.Selected := false;
    Point := GetVectorPoint(2,FReportVectorList[VectorCount]);
    Point.Selected := false;
    end;
end;
procedure TG3DOTree.ShowSelectedVector(VIndex : Integer);
var
Point       : TG3dReportPoint;
begin
DeSelectAllVectors;
Point := GetVectorPoint(1,FReportVectorList[VIndex]);
Point.Selected := True;
Point := GetVectorPoint(2,FReportVectorList[VIndex]);
Point.Selected := True;

end;

function  TG3DOTree.GetPointDataForIndex(Index : INteger) : TG3DReportPoint;
var
PointNode : TTreeNode;
begin
Result := nil;
if Index > Items.Count then exit;
if Index < 0 then exit;
PointNode := Items.GetNode(FReportPointList[Index].NodeId);
Result := PointNode.Data;
end;

function  TG3DOTree.GetPointDataPointCalled(PointName : String) : TG3DReportPoint;
var
PointNode : TTreeNode;
Index : Integer;
begin
Result := nil;
Index := GetReportPointIndexfor(PointName);
if Index < 0  then exit;
PointNode := Items.GetNode(FReportPointList[Index].NodeId);
Result := PointNode.Data;
end;


function TG3DOTree.GetVectorPoint(PointNum : Integer;VecData : TVectorData) : TG3DReportPoint;
var
PointNode : TTreeNode;
begin
if PointNum = 1 then
   PointNode:= Items.GetNode(VecData.Point1NodeId)
else
   PointNode:= Items.GetNode(VecData.Point2NodeId);
Result := PointNode.Data;
end;


procedure TG3DOTree.CorrectAxesFreezingVector(VectorIndex,AxisIndex: Integer; Correction : Double);
begin
FreezeVectorat(VectorIndex);
SetAxisPosition(AxisIndex,GetAxisPosition(AxisIndex)+Correction);
UnFreezeVectorat(VectorIndex);
UpdateReportData(True);
end;


procedure TG3DOTree.BuildObjectTable;
begin
if assigned(WorldNode) then TreeTable.BuildTableFrom(Self,WorldNode);
end;

function TG3DOTree.CalculatePartDatumRelativeLocationOfPointInSpace(Position :TPLocation):TPLocation;
function RemoveRotationForAxis(AxisNum : Integer;CurrentLocation : TPLocation):TPLocation;
var
AxisRotation    : TRotation;
CORPOsition     : TPLocation;

begin

Result := CurrentLocation;
with AxisDefs[AXisNum] do
     begin
     AxisRotation   := SubtractRotation(AxisObject.FPositionData.DesignTimeRotation,DatumRotation);
     CORPosition := AxisObject.cOR;
     if AffectsPartDatum and (not isLinear) then
      begin
      case MotionAxisIndex of
           3:Result := PRotateAboutX3D(Result,CORPosition,AxisRotation.x);
           4:Result := PRotateAboutY3D(Result,CORPosition,AxisRotation.y);
           5:Result := PRotateAboutZ3D(Result,CORPosition,AxisRotation.z);
      end; // case
      SetAxisPosition(AxisNum,0);
     end; // if
     end; // with
end;

function RemoveRotationForPartDatum(CurrentLocation : TPLocation):TPLocation;
var
CORPOsition     : TPLocation;
PartDatumObj    : TG3DReportPoint;
begin
Result := CurrentLocation;
PartDatumObj := FPartDatumNode.Data;
CORPosition := PartDatumObj.cOR;
Result := PRotateAboutX3D(Result,CORPosition,PartDatumObj.FPositionData.DesignTimeRotation.x);
Result := PRotateAboutY3D(Result,CORPosition,PartDatumObj.FPositionData.DesignTimeRotation.y);
Result := PRotateAboutZ3D(Result,CORPosition,PartDatumObj.FPositionData.DesignTimeRotation.z);
end;

var
AxisCount : Integer;
AxisData  : TAxisDefinition;
ResultLocation  : TPLocation;
AxisTranslation : TPlocation;
CurrentPositions: TAxisPOsn;
CurrentPartDatumLocation : TPLocation;
RelativePosition         : TPLocation;
PartDatumObject : TG3DReportPoint;
begin
CurrentPositions:=GetAllAxisPOsitions;

Result := Zero3D;

ResultLocation := AddLocation(Position,AbsolutePartDatum);
try
// Find current Location of Part Datum In Real Cordinates
CurrentPartDatumLocation := FPartDatumLocation;

// Calculate difference between Part datum and requested Location
RelativePosition :=  SubtractLocation(Position,CurrentPartDatumLocation);
RelativePosition := RemoveRotationforAxis(5,RelativePosition);
RelativePosition := RemoveRotationForPartDatum(RelativePosition)
finally
Result := RelativePosition;
RestoreAllAxisPOsitions(CurrentPositions);
end;
end;


function TG3DOTree.ZeroOffsets:TAxisPosn;
begin
Result[0] := 0;
Result[1] := 0;
Result[2] := 0;
Result[3] := 0;
Result[4] := 0;
Result[5] := 0;
end;

procedure TG3DOTree.AddVector(x1,y1,z1,x2,y2,z2 : Double;VType :TReportVectorType;VName : String);
var
Point1,Point2 : TPlocation;
VIndex : Integer;
begin
Point1 := SetPrecisePoint3D(x1,y1,z1);
Point2 := SetPrecisePoint3D(x2,y2,z2);
// Check if vector already exists and if so then delete
VIndex := GetVectorIndexfor(VName);
if VIndex >= 0 then
   begin
   DeleteVectorat(VIndex)
   end;
AddReportVectorbetween(Point1,Point2,VName,VType);
end;


procedure TG3DOTree.AddActualStackingAxisRelativeVector(x1,y1,z1,x2,y2,z2 : Double;VType :TReportVectorType;VName : String);
var
Point1,Point2 : TPlocation;
InversePoint2 : TPlocation;
VP1,Vp2 : TG3DReportPoint;
VIndex : Integer;
VDelta : TPLocation;
begin
FCorrectionStartPosn := GetAllAxisPositions;
DatumAllAxes;
try
Point1 := SetPrecisePoint3D(x1,y1,z1);
Point2 := SetPrecisePoint3D(x2,y2,z2);
VDelta := SubtractLocation(Point1,Point2);
VDelta := PosNormalise(VDelta,10);
InversePoint2 := AddLocation(Point1,VDelta);

VIndex := GetVectorIndexfor(VName);
if VIndex >= 0 then
   begin
   DeleteVectorat(VIndex)
   end;
AddReportVectorbetween(Point1,Point2,VName,VType);
UpdateReportData(True);
// THIS MAY HAVE TRANSFORMATIONS DUE TO ndp oFFSET SO NOW GET ABSOLUTE POSITION
VIndex := GetVectorIndexfor(VName);
Vp1 := GetVectorPoint(1,FReportVectorList[Vindex]);
Vp2 := GetVectorPoint(2,FReportVectorList[Vindex]);
Point1 := VP1.ReferencePoint;
Point2 := VP2.ReferencePoint;
Point1 := GetCorrectedPositionRelativeTo(Point1,AbsolutePartDatum);
Point2 := GetCorrectedPositionRelativeTo(Point2,AbsolutePartDatum);

TransformToStackingAxis(Point1,StackingAxisTransform);
TransformToStackingAxis(Point2,StackingAxisTransform);
DeleteVectorat(VIndex);
AddReportVectorbetween(Point1,Point2,VName,rvPartRelative);

If VName = NDPName then
  begin
  Point1 := SetPrecisePoint3D(x1,y1,z1);
  VIndex := GetVectorIndexfor(InvNDPName);
  if VIndex >= 0 then
     begin
     DeleteVectorat(VIndex)
     end;
  AddReportVectorbetween(Point1,InversePoint2,InvNDPName,VTYpe);
  UpdateReportData(True);
  // THIS MAY HAVE TRANSFORMATIONS DUE TO ndp oFFSET SO NOW GET ABSOLUTE POSITION
  VIndex := GetVectorIndexfor(InvNDPName);
  Vp1 := GetVectorPoint(1,FReportVectorList[Vindex]);
  Vp2 := GetVectorPoint(2,FReportVectorList[Vindex]);
  Point1 := VP1.ReferencePoint;
  Point2 := VP2.ReferencePoint;
  Point1 := GetCorrectedPositionRelativeTo(Point1,AbsolutePartDatum);
  Point2 := GetCorrectedPositionRelativeTo(Point2,AbsolutePartDatum);

  TransformToStackingAxis(Point1,StackingAxisTransform);
  TransformToStackingAxis(Point2,StackingAxisTransform);
  DeleteVectorat(VIndex);
  AddReportVectorbetween(Point1,Point2,InvNDPName,rvPartRelative);
  end;

finally
RestoreAllAxisPOsitions(FCorrectionStartPosn);
end;
end;

function TG3DOTree.GetPointAtDatum(x,y,z:Double):TPlocation;
var
StartPositions : TAxisPosn;
PIndex         : Integer;
begin
Result := SetPrecisePoint3D(0,0,0);
StartPositions := GetAllAxisPOsitions;
// First create a temporary report point at desired position/
AddReportPointat(x,y,z,'dgstemp1',rpMachineRelOnPartDatum,True,False);
DatumAllAxes;
UpdateReportData(True);
PIndex := GetReportPointIndexfor('dgstemp1');
Result := GetReportPointFromList(PIndex);
try
finally
PIndex := GetReportPointIndexfor('dgstemp1');
if PIndex >= 0 then
   begin
   DeleteReportPoint(PIndex)
   end;
RestoreAllAxisPositions(StartPositions);
end;

end;

procedure TG3DOTree.SetDatumAxisPOsitions(X,Y,Z,A,B,C: Double);
begin
end;


function TG3DOTree.AddPartRelativePoint(X,Y,Z: Double;PName :String): Integer;
var
PIndex : Integer;
begin
// First delete existing report point if it exists
PIndex := GetReportPointIndexfor(PName);
if PIndex >= 0 then
   begin
   DeleteReportPoint(PIndex);
   end;
Result := AddReportPointat(x,y,z,PName,rpPartRelative,True,False);
end;

Function TG3DOTree.AddMachineRelativePoint(X,Y,Z: Double;PName :String): Integer;
var
PIndex : Integer;
begin
// First delete exisating report point if it exists
PIndex := GetReportPointIndexfor(PName);
if PIndex >= 0 then
   begin
   DeleteReportPoint(PIndex);
   end;
Result := AddReportPointat(x,y,z,PName,rpMachineRelOnPartDatum,True,False);
end;

procedure TG3DOTree.ProbePosnToPartRelativePoint(PointName :String; X,Y,Z,A,B,C :Double);

var
StartPosition : TAxisPosn;
NewPosition : TAxisMoveArray;
begin
StartPosition := GetAllAxisPositions;//gds!!!
NewPOsition[0].Position := X;
NewPOsition[1].Position := Y;
NewPOsition[2].Position := Z;
NewPOsition[3].Position := A;
NewPOsition[4].Position := B;
NewPOsition[5].Position := C;
NewPOsition[0].Move := True;
NewPOsition[1].Move := True;
NewPOsition[2].Move := True;
NewPOsition[3].Move := True;
NewPOsition[4].Move := True;
NewPOsition[5].Move := True;

{SetAxisPOsition(0,X);
SetAxisPOsition(1,Y);
SetAxisPOsition(2,Z);
SetAxisPOsition(3,A);
SetAxisPOsition(4,B);
SetAxisPOsition(5,C);}
MoveAllAxes(6,NewPosition);
UpdateReportData(True);
AddMachineRelativePoint(FElectrodeLocation.x,FElectrodeLocation.y,FElectrodeLocation.z,PointName);

//AddReportPointat(FElectrodeLocation.x,FElectrodeLocation.y,FElectrodeLocation.z,PointName,rpPartRelative,True);
RestoreAllAxisPOsitions(StartPosition);

end;


function TG3DOTree.AddToolRelativePoint(X,Y,Z: Double;PName :String): Integer;
var
PIndex : Integer;
begin
// First delete exisating report point if it exists
PIndex := GetReportPointIndexfor(PName);
if PIndex >= 0 then
   begin
   DeleteReportPoint(PIndex);
   end;
Result := AddReportPointat(x,y,z,PName,rpToolRelative,True,False);
end;

//*******************************************************
// New Rooutines for Nomianls Generation
//*******************************************************
procedure TG3DOTree.NominalsReset;
begin
ClearNominalOffsets;
if NDPVectorIndex <> -1 then DeleteVectorat(NDPVectorIndex);
if InverseNDPVectorIndex <> -1 then DeleteVectorat(InverseNDPVectorIndex);
NDPVectorIndex := -1;
InverseNDPVectorIndex := -1;
SetToolApproach(0,0,1);
CalculateStackingAxisTransform;
end;

function TG3DOTree.Normalise180(RotPos : Double):Double;
begin
Result := RotPos;
 if RotPos < -180 then
    begin
    Result := RotPos+360;
    end;
 if RotPos > 180 then
    begin
    Result := RotPos-360;
    end;
end;

function TG3DOTree.PlusMinus360(RotPos: Double): Double;
begin
while RotPos>= 360 do
  begin
  RotPOs := RotPOs-360;
  end;
while RotPos <= -360 do
  begin
  RotPOs := RotPOs+360;
  end;
Result := RotPos;
end;

function TG3DOTree.ComplimentAroundZero(RotPos : Double):Double;
begin
Result := -Normalise180(RotPos);
end;


function TG3DOTree.Normalise360(RotPos : Double):Double;
begin
Result := RotPos;
 if abs(RotPos)  < ExtremlySmall then
  begin
  Result := 0;
  exit;
  end;
 if RotPos < 0 then
    begin
    Result := RotPos+360;
    end;
 if RotPos > 360 then
    begin
    Result := RotPos-360;
    end;
end;

procedure TG3DOTree.SetAndSaveNominalOffset(Index: INteger; X, Y, Z, A, B, C: Double);
begin
SavedNDPOffsetLinear.x := X;
SavedNDPOffsetLinear.y := Y;
SavedNDPOffsetLinear.z := Z;
SavedNDPOffsetRotary.x := A;
SavedNDPOffsetRotary.x := B;
SavedNDPOffsetRotary.x := C;
SetNominalOffset(Index, X, Y, Z, A, B, C);
end;

procedure TG3DOTree.SetNominalOffset(Index: INteger; X, Y, Z, A, B,  C: Double);
var
LinearOffset :TPlocation;
NDPDatumObject : TG3DReportPoint;
begin
if not assigned(FPartDatumNode) then exit;
if not assigned(FELectrodeNode) then
   begin
   AddObjectatNode(FToolDatumNode,t3oELectrodeNode,False);
   end;
LinearOffset.x := X;
LinearOffset.Y := Y;
LinearOffset.z := Z;
NDPDatumObject := FNDPDatumNode.Data;
LinearOffset := LinearCorrected(LinearOffset);
NDPDatumObject.FPositionData.DesignTimeTranslation := LinearOffset;
NDPDatumObject.FPositionData.DesignTimeRotation.x := A;
NDPDatumObject.FPositionData.DesignTimeRotation.y := B;
NDPDatumObject.FPositionData.DesignTimeRotation.z := C;
MatchObjectListToTree;
UpdateReportData(True);
end;

procedure TG3DOTree.ClearNominalOffsets;
var
LinearOffset :TPlocation;
NDPDatumObject : TG3DReportPoint;
begin
if not assigned(FPartDatumNode) then exit;
LinearOffset.x := 0;
LinearOffset.Y := 0;
LinearOffset.z := 0;
NDPDatumObject := FNDPDatumNode.Data;
LinearOffset := LinearCorrected(LinearOffset);
NDPDatumObject.FPositionData.DesignTimeTranslation := LinearOffset;
NDPDatumObject.FPositionData.DesignTimeRotation.x := 0;
NDPDatumObject.FPositionData.DesignTimeRotation.y:=  0;
NDPDatumObject.FPositionData.DesignTimeRotation.z := 0;
MatchObjectListToTree;
UpdateReportData(True);
end;


procedure TG3DOTree.SetToolApproach(I,J,K : Double);
var
ListPos : INteger;
VName : String;
NFactor : Double;
Divisor : Double;
begin

 // First Normalise Vector to Display Length
 Divisor := Max(abs(I),abs(J));
 Divisor := Max(Divisor,abs(K));
 if Divisor <> 0 then
   begin
   NFactor := abs(100/Divisor);
   end
 else
   begin
   NFactor := 1;
   end;

 I := I*NFactor;
 J := J*NFactor;
 K := K*NFactor;
 ListPos := Length(FReportVectorList);
 Vname := ApproachVectorName;
 AddVector(0,0,0,I,J,K, rvToolApproach, PAnsiChar(VName)); // Note J Not Used at present
end;


procedure TG3DOTree.UpdateFixedIndexes;

var
ListCount : Integer;
VectorName     : String;
VectorData     : TVectorData;
begin
NominalVectorIndex := -1;
ActualVectorIndex := -1;
NDPVectorIndex := -1;
InverseNDPVectorIndex := -1;
ApproachVectorIndex := -1;
AttachedElecIndex := -1;

for ListCount := 0 to Length(FReportVectorList)-1 do
    begin
    VectorData := GetReportVectorFromList(ListCount);
    VectorName := AnsiUpperCase(VectorData.Name);
    if VectorName = AnsiUpperCase(NominalVectorName) then
       begin
       NominalVectorIndex := ListCount;
       end else
    if VectorName = AnsiUpperCase(ActualVectorName) then
       begin
       ActualVectorIndex := ListCount;
       end else

    if VectorName = AnsiUpperCase(NDPName) then
       begin
       NDPVectorIndex := ListCount;
       end else

    if VectorName = AnsiUpperCase(InvNDPName) then
       begin
       InverseNDPVectorIndex := ListCount;
       end else

    if VectorName = AnsiUpperCase(ApproachVectorName) then
       begin
       ApproachVectorIndex := ListCount;
       end else

    if VectorName = AnsiUpperCase(AttachElectrodeName) then
       begin
       AttachedElecIndex := ListCount;
       end;

    end;

end;

procedure TG3DOTree.SetExtentsOverscale(const Value: Double);
begin
  FExtentsOverscale := Value;
end;

procedure TG3DOTree.RedrawToExtents(Paper : TG3DDisplay);
begin
  FitToReportPoints(Paper.CustomView);
end;

procedure TG3DOTree.RuntimePaint(Paper : TG3DPaper);
begin
Paper.Paint;
end;

function TG3DOTree.RenameSelected(NewName: String): Boolean;
begin
Result := True;
try
if assigned(Selected) then
  begin
  Selected.Text := NewName;
  MatchObjectListToTree;
  end;
except
Result := False;
end;
end;

function TG3DOTree.GetObjectCount: Integer;
begin
Result := -1;
if assigned(TreeTable) then
  Result := TreeTable.ObjectCount;
end;

function TG3DOTree.AddObjectatNode2(Node: TTreeNode; NewObject: T3DObjects;
  IsVirtual: Boolean;var NewNode: TTreeNode): Pointer;
var
NewLine : TG3DLine;
NewBox  : TG3DBox;
NewDisc : TG3DDisc;
//NewNode : TTreeNode;
NewPOint: TG3DReportPoint;
NameCount : Integer;

begin
Result := nil;
if not assigned(TreeTable) then exit;
case NewObject of
     t3oLine:
     begin
     Newline := TG3DLine.Create(Self);
     Newline.VirtualOnly := IsVirtual;
     NameCount := TreeTable.ObjectCount;
     while NameExists(Format('Line%d',[NameCount])) do
           begin
           inc (nameCount);
           end;
     NewLine.Tag := NameCount;
     NewLine.Name := Format('Line%d',[NameCount]);
     NewNode := Items.AddChild(Node,NewLine.Name);
     NewNode.ImageIndex := 1;
     NewNode.SelectedIndex := 5;
     NewNode.Data := Newline;
     Result := NewLine;
     TreeTable.BuildTableFrom(Self,WorldNode);
     end;

     t3oBox:
     begin
     NewBox := TG3DBox.Create(Self);
     NewBox.VirtualOnly := IsVirtual;
     NameCount := Treetable.ObjectCount;
     while NameExists(Format('Box%d',[NameCount])) do
           begin
           inc (nameCount);
           end;
     NewBox.Tag := NameCount;
     NewBox.Name :=  Format('Box%d',[NameCount]);
//     Result := NewBox.Name;
     NewNode := Items.AddChild(Node,NewBox.Name);
     NewNode.Data := NewBox;
     NewNode.ImageIndex := 2;
     NewNode.SelectedIndex := 6;
     Result := NewBox;
     TreeTable.BuildTableFrom(Self,WorldNode);
     end;

     t3oDisc:
     begin
     NewDisc := TG3DDisc.Create(Self);
     NewDisc.VirtualOnly := IsVirtual;
     NameCount := Treetable.ObjectCount;
     while NameExists(Format('Disc%d',[NameCount])) do
           begin
           inc (nameCount);
           end;
     NewDisc.Tag  := NameCount;
     NewDisc.Name :=  Format('Disc%d',[NameCount]);
//     Result := NewDisc.Name;
     NewNode := Items.AddChild(Node,NewDisc.Name);
     NewNode.ImageIndex := 3;
     NewNode.SelectedIndex := 7;
     NewNode.Data := NewDisc;
     TreeTable.BuildTableFrom(Self,WorldNode);
     Result := NewDisc;
     end;

     t3oReportPoint:
     begin
     //
     NewPoint := TG3DReportPoint.Create(Self);
     NewPoint.VirtualOnly := IsVirtual;
     NameCount := Treetable.ObjectCount;
     while NameExists(Format('ReportPoint%d',[NameCount])) do
           begin
           inc (nameCount);
           end;
     NewPoint.Tag  := NameCount;
     NewPoint.Name :=  Format('ReportPoint%d',[NameCount]);
     NewPoint.Colour := clSilver;
     NewPoint.SelectColour := ReportPointSelectColour;
     NewNode := Items.AddChild(Node,NewPoint.Name);
     NewNode.ImageIndex := 8;
     NewNode.SelectedIndex := 9;
     NewNode.Data := NewPoint;
     Result := NewPoint;
     if (GlobalUpdateCount < 1) then TreeTable.BuildTableFrom(Self,WorldNode);
     end;

     t3oPartDatum:
     begin
     NewPoint := TG3DReportPoint.Create(Self);
     NameCount := Treetable.ObjectCount;
     NewPoint.Tag  := NameCount;
     NewPoint.Name :=  PartDatumID;
     NewPoint.PointType := rpPartDatum;
//     Result := NewPoint.Name;
     NewPoint.Colour := clSilver;
     NewPoint.SelectColour := ReportPointSelectColour;
     NewNode := Items.AddChild(Node,NewPoint.Name);
     NewNode.ImageIndex := 10;
     NewNode.SelectedIndex := 11;
     NewNode.Data := NewPoint;
     FPartDatumNode := NewNode;
     Result := NewPoint;
     TreeTable.BuildTableFrom(Self,WorldNode);
     end;

     t3oNominalPartDatum:
     begin
     NewPoint := TG3DReportPoint.Create(Self);
     NameCount := Treetable.ObjectCount;
     NewPoint.Tag  := NameCount;
     NewPoint.Name :=  NomPartDatumID;
     NewPoint.PointType := rpNomPartDatum;
//     Result := NewPoint.Name;
     NewPoint.Colour := clSilver;
     NewPoint.SelectColour := ReportPointSelectColour;
     NewNode := Items.AddChild(Node,NewPoint.Name);
     NewNode.ImageIndex := 10;
     NewNode.SelectedIndex := 11;
     NewNode.Data := NewPoint;
     FFixedPartDatumNode := NewNode;
     Result := NewPoint;
     TreeTable.BuildTableFrom(Self,WorldNode);
     end;

     t3oToolDatum:
     begin
     NewPoint := TG3DReportPoint.Create(Self);
     NameCount := Treetable.ObjectCount;
     NewPoint.Tag  := NameCount;
     NewPoint.Name :=  ToolDatumID;
     NewPoint.PointType := rpToolDatum;
     NewPoint.Colour := clSilver;
     NewPoint.SelectColour := ReportPointSelectColour;
//     Result := NewPoint.Name;
     NewNode := Items.AddChild(Node,NewPoint.Name);
     NewNode.ImageIndex := 10;
     NewNode.SelectedIndex := 11;
     NewNode.Data := NewPoint;
     FToolDatumNode := NewNode;
     Result := NewPoint;
     TreeTable.BuildTableFrom(Self,WorldNode);
     end;


     t3oHoleNode:
     begin
     NewPoint := TG3DReportPoint.Create(Self);
     NewPoint.VirtualOnly := IsVirtual;
     NameCount := Treetable.ObjectCount;
     NewPoint.Tag  := NameCount;
     NewPoint.Name :=  'HolePoint';
     NewPoint.PointType := rpHolePoint;
     NewPoint.Colour := clred;
     NewPoint.SelectColour := clRed;
     NewNode := Items.AddChild(Node,NewPoint.Name);
     NewNode.ImageIndex := 10;
     NewNode.SelectedIndex := 11;
     NewNode.Data := NewPoint;
     FHoleNode := NewNode;
     Result := NewPoint;
     TreeTable.BuildTableFrom(Self,WorldNode);
     end;

     t3oELectrodeNode:
     begin
     NewPoint := TG3DReportPoint.Create(Self);
     NewPoint.VirtualOnly := IsVirtual;
     NameCount := Treetable.ObjectCount;
     NewPoint.Tag  := NameCount;
     NewPoint.Name :=  'ElectrodeDatum';
     NewPoint.PointType := rpElectrodeDatum;
     NewPoint.Colour := clSilver;
     NewPoint.SelectColour := clRed;
//     Result := NewPoint.Name;
     NewNode := Items.AddChild(Node,NewPoint.Name);
     NewNode.ImageIndex := 10;
     NewNode.SelectedIndex := 11;
     NewNode.Data := NewPoint;
     FELectrodeNode := NewNode;
     Result := NewPoint;
     TreeTable.BuildTableFrom(Self,WorldNode);
     end;

     t3oNDPDatumNode:
     begin
     NewPoint := TG3DReportPoint.Create(Self);
     NewPoint.VirtualOnly := IsVirtual;
     NameCount := Treetable.ObjectCount;
     NewPoint.Tag  := NameCount;
     NewPoint.Name :=  'NDPDatum';
     NewPoint.PointType := rpNDPDatum;
     NewPoint.Colour := clSilver;
     NewPoint.SelectColour := clAqua;
     NewNode := Items.AddChild(Node,NewPoint.Name);
     NewNode.ImageIndex := 10;
     NewNode.SelectedIndex := 11;
     NewNode.Data := NewPoint;
     FNDPDatumNode := NewNode;
     Result := NewPoint;
     TreeTable.BuildTableFrom(Self,WorldNode);
     end;

end; //case
end;
//Returns true if an object of the given name already exists in the ObjectList


procedure TG3DOTree.PutHoleRelativeToPartDatumAt(HolePosition : TPLocation);
var
HoleObject : TG3DReportPoint;
Update : Boolean;
begin
Update :=False;
if not assigned(FPartDatumNode) then exit;
if not assigned(FHoleNode)  then
   begin
   AddObjectatNode(FPartDatumNode,t3oHoleNode,False);
   Update :=True
   end;
HolePosition := LinearCorrected(HolePosition);
HoleObject := FHoleNode.Data;
HoleObject.FPositionData.DesignTimeTranslation := HolePosition;
if Update then
  begin
  MatchObjectListToTree;
  UpdateReportData(True);
  end;
end;

procedure TG3DOTree.SetPartDatumOffset(Index: INteger; X, Y, Z, A, B,
  C: Double);
var
LinearOffset :TPlocation;
PartDatumObject : TG3DReportPoint;
begin
if not assigned(FPartDatumNode) then exit;
LinearOffset.x := X;
LinearOffset.Y := Y;
LinearOffset.z := Z;
PartDatumObject := FPartDatumNode.Data;
PartDatumObject.FPositionData.DesignTimeTranslation := LinearOffset;
PartDatumObject.FPositionData.DesignTimeRotation.x := A;
PartDatumObject.FPositionData.DesignTimeRotation.y:=  B;
PartDatumObject.FPositionData.DesignTimeRotation.z := C;
//MatchObjectListToTree;
UpdateReportData(True);
end;


procedure TG3DOTree.RuntimeToolDatumAdjustby(Adjustment: TPlocation);
var
PartDatumObject : TG3DReportPoint;
MachineFile : TIniFile;
begin
if FileExists(LoadedMachineFile) then
  begin
  MachineFile := TIniFile.Create(LoadedMachineFile);
  try
  MachineFile.WriteFloat('DatumOffsets','ToolDatumXOffset',Adjustment.x);
  MachineFile.WriteFloat('DatumOffsets','ToolDatumYOffset',Adjustment.y);
  MachineFile.WriteFloat('DatumOffsets','ToolDatumZOffset',Adjustment.z);
  finally
  MachineFile.Free;
  end;
  end;
 LoadTreeFromDisc(LoadedMachineFile,False)
end;

procedure TG3DOTree.SetPartDatumRotaryOffset(Index: INteger; A, B,
  C: Double);
var
PartDatumObject : TG3DReportPoint;
begin
if not assigned(FPartDatumNode) then exit;
PartDatumObject := FPartDatumNode.Data;
PartDatumObject.FPositionData.DesignTimeRotation.x := A;
PartDatumObject.FPositionData.DesignTimeRotation.y:=  B;
PartDatumObject.FPositionData.DesignTimeRotation.z := C;
MatchObjectListToTree;
UpdateReportData(True);
end;

function TG3DOTree.GetJogAxis(AxisIndex: Integer): Integer;
begin
Result := AxisDefs[AxisIndex].MotionAxisIndex;
end;

procedure TG3DOTree.FixedCentreRescale(Paper : TG3DPaper;ScaleRatio: Double);
var
StartExtents : TExtents;
StartCentre  : TScreenLocation;
EndExtents  : TExtents;
EndCentre  : TScreenLocation;
EndCentre1  : TScreenLocation;
NewScale     : Double;
begin
  NewScale := Paper.Scale*ScaleRatio;
  StartExtents :=AllObjectExtentsOnSurface(Paper,StartCentre);
  Paper.Scale := NewScale;
  EndExtents :=AllObjectExtentsOnSurface(Paper,EndCentre);
  EndExtents :=AllObjectExtentsOnSurface(Paper,EndCentre1);
end;

procedure TG3DOTree.CreateFixedReportPoints;
begin
if not assigned(FElectrodeNode) then
   begin
   AddObjectatNode(FToolDatumNode,t3oELectrodeNode,False);
   end;
if not assigned(FHoleNode) then
   begin
   AddObjectatNode(FToolDatumNode,t3oHoleNode,False);
   end;
if not assigned(NDPDatumNode) then
   begin
   AddObjectatNode(PartDatumNode,t3oNDPDatumNode,False);
   end;

end;

function TG3DOTree.HoleExtentsOnSurface(Paper: TG3DPaper;
                                        var Centre: TScreenLocation;
                                        First,Last: Integer): Textents;
var
HoleEntry : pHoleEntry;
HoleNumber : INteger;
Objectextents : TExtents;
CurrentPoint : TG3dReportPoint;
PointNode : TTreeNode;

begin
Result.MaxX := -11100000;
Result.MaxY := -11100000;
Result.MinX := 100000000;
Result.MinY := 100000000;
if FHoleList.Count < 1 then
   begin
   Result.MaxX := 0;
   Result.MaxY := 0;
   Result.MinX := 0;
   Result.MinY := 0;
   exit;
   end;
HoleNumber := 0;

if (First < 0) or (Last<0)
   or (Last <= First)
   or (First > FHoleList.Count)
   or (Last  > FHoleList.Count) then
  begin
  First := 0;
  Last := FHoleList.Count-1;
  end;
if Last >= FHoleList.Count then Last := FHoleList.Count-1; 

for HoleNumber := First to Last do
   begin
   HoleEntry := FHoleList[HoleNumber];
   PointNode := Items.GetNode(HoleEntry.NodeId);
   if assigned(PointNode) then
    begin
    CurrentPoint := PointNode.Data;
    ObjectExtents := CurrentPoint.CalculateExtents(Paper);
    if ObjectExtents.MaxX > Result.MaxX then Result.MaxX := ObjectExtents.MaxX;
    if ObjectExtents.MaxY > Result.MaxY then Result.MaxY:= ObjectExtents.MaxY;
    if ObjectExtents.MinX < Result.MinX then Result.MinX := ObjectExtents.MinX;
    if ObjectExtents.MinY < Result.MinY then Result.MinY := ObjectExtents.MinY;
    end;
   end;
Centre.x := (Result.MaxX+Result.MinX)div 2;
Centre.y := (Result.MaxY+Result.MinY)div 2;

end;

function TG3DOTree.ReportPointExtentsOnSurfaceRanged(Index1,
  Index2: Integer; Paper: TG3DPaper;
  var Centre: TScreenLocation): Textents;
var
Count : Integer;
Objectextents : TExtents;

begin
Result.MaxX := -11100000;
Result.MaxY := -11100000;
Result.MinX := 100000000;
Result.MinY := 100000000;

if (Length(FReportPointList)<1) or (Length(FReportPointList)<(Index2-1))then
   begin
   Result.MaxX := 0;
   Result.MaxY := 0;
   Result.MinX := 0;
   Result.MinY := 0;
   end
else
    begin
    For Count := Index1 to Index2 do
        begin
        with TG3DObject(TreeTable.ObjectTable[Count].GObject) do
             begin
             ObjectExtents := CalculateExtents(Paper);
             if ObjectExtents.MaxX > Result.MaxX then Result.MaxX := ObjectExtents.MaxX;
             if ObjectExtents.MaxY > Result.MaxY then Result.MaxY:= ObjectExtents.MaxY;
             if ObjectExtents.MinX < Result.MinX then Result.MinX := ObjectExtents.MinX;
             if ObjectExtents.MinY < Result.MinY then Result.MinY := ObjectExtents.MinY;
             end;
        end;
    end;
Centre.x := (Result.MaxX+Result.MinX)div 2;
Centre.y := (Result.MaxY+Result.MinY)div 2;
end;

{This procedure moves the FixedActualVector to the same position as the reported actual vector.
The Fixed actual Vector does NOT move with the Part Datum as it is on the Fixed Part Datum.
Thus when the Part Datum is moved to match the NOMINAL vector with the Fixed Actual Vector
the holes a re in the real position for drilling
}

procedure TG3DOTree.ResetActualVectorToNominal;
var
Point : TG3dReportPoint;
NomPos : TPlocation;
ActPOint :TG3dReportPoint;
begin
if (NominalVectorIndex >= 0) and (ActualVectorIndex >= 0)then
  begin
  Point := GetVectorPoint(1,FReportVectorList[NominalVectorIndex]);
  NomPos := POint.FPositionData.DesignTimeTranslation;
  ActPOint := GetVectorPoint(1,FReportVectorList[ActualVectorIndex]);
  ActPOint.FPositionData.DesignTimeTranslation := NomPos;

  Point := GetVectorPoint(2,FReportVectorList[NominalVectorIndex]);
  NomPos := POint.FPositionData.DesignTimeTranslation;
  ActPOint := GetVectorPoint(2,FReportVectorList[ActualVectorIndex]);
  ActPOint.FPositionData.DesignTimeTranslation := NomPos;

  Point := GetVectorPoint(1,FReportVectorList[NTVectorIndex]);
  NomPos := POint.FPositionData.DesignTimeTranslation;
  ActPOint := GetVectorPoint(1,FReportVectorList[ATVectorIndex]);
  ActPOint.FPositionData.DesignTimeTranslation := NomPos;

  Point := GetVectorPoint(2,FReportVectorList[NTVectorIndex]);
  NomPos := POint.FPositionData.DesignTimeTranslation;
  ActPOint := GetVectorPoint(2,FReportVectorList[ATVectorIndex]);
  ActPOint.FPositionData.DesignTimeTranslation := NomPos;

  UpdateReportData(True);
  end;
end;

procedure TG3DOTree.AdjustPartDatumBy(Translation : TPLocation; Rotation: TRotation);
var
PartDatumObject : TG3DReportPoint;
CorrectedTranslation : TPLocation;
begin
if not assigned(FPartDatumNode) then exit;
CorrectedTranslation := LinearCorrected(Translation);
PartDatumObject := FPartDatumNode.Data;
with PartDatumObject do
  begin
  FPositionData.DesignTimeTranslation.x := FPositionData.DesignTimeTranslation.x+CorrectedTranslation.x;
  FPositionData.DesignTimeTranslation.y := FPositionData.DesignTimeTranslation.y+CorrectedTranslation.y;
  FPositionData.DesignTimeTranslation.z := FPositionData.DesignTimeTranslation.z+CorrectedTranslation.z;
  FPositionData.DesignTimeRotation.x := FPositionData.DesignTimeRotation.x+Rotation.x;
  FPositionData.DesignTimeRotation.y := FPositionData.DesignTimeRotation.y+Rotation.y;
  FPositionData.DesignTimeRotation.z := FPositionData.DesignTimeRotation.z+Rotation.z;
  end;
UpdateReportData(True);
end;

(*procedure TG3DOTree.ResetPartDatum();
var
PartDatumObject : TG3DReportPoint;
FixedPartDatumObject : TG3DReportPoint;
begin
if not assigned(FPartDatumNode) then exit;
if not assigned(FixedPartDatumObject) then exit;
PartDatumObject := FPartDatumNode.Data;
FixedPartDatumObject := FFixedPartDatumNode.Data;
with PartDatumObject do
  begin
  FPositionData.DesignTimeTranslation.x := FixedPartDatumObject.FPositionData.DesignTimeTranslation.x;
  FPositionData.DesignTimeTranslation.y := FixedPartDatumObject.FPositionData.DesignTimeTranslation.y;
  FPositionData.DesignTimeTranslation.z := FixedPartDatumObject.FPositionData.DesignTimeTranslation.z;
  FPositionData.DesignTimeRotation.x := FixedPartDatumObject.FPositionData.DesignTimeRotation.x;
  FPositionData.DesignTimeRotation.y := FixedPartDatumObject.FPositionData.DesignTimeRotation.y;
  FPositionData.DesignTimeRotation.z := FixedPartDatumObject.FPositionData.DesignTimeRotation.z;
  end;
UpdateReportData(True);

end;*)


procedure TG3DOTree.AdjustFixedPartDatumBy(Translation : TPLocation; Rotation: TRotation);
var
PartDatumObject : TG3DReportPoint;
CorrectedTranslation : TPLocation;
begin
if not assigned(FFixedPartDatumNode) then exit;
CorrectedTranslation := LinearCorrected(Translation);
PartDatumObject := FFixedPartDatumNode.Data;
with PartDatumObject do
  begin
  FPositionData.DesignTimeTranslation.x := FPositionData.DesignTimeTranslation.x+CorrectedTranslation.x;
  FPositionData.DesignTimeTranslation.y := FPositionData.DesignTimeTranslation.y+CorrectedTranslation.y;
  FPositionData.DesignTimeTranslation.z := FPositionData.DesignTimeTranslation.z+CorrectedTranslation.z;
  FPositionData.DesignTimeRotation.x := FPositionData.DesignTimeRotation.x+Rotation.x;
  FPositionData.DesignTimeRotation.y := FPositionData.DesignTimeRotation.y+Rotation.y;
  FPositionData.DesignTimeRotation.z := FPositionData.DesignTimeRotation.z+Rotation.z;
  end;
UpdateReportData(True);
end;


procedure TG3DOTree.SetNDPVectorPositionBetween(SurfacePoint,ApproachPoint: TPLocation);
var
InvApp : TPLocation;
begin
if NDPVectorIndex < 0 then
  begin
  AddVector(SurfacePoint.x, SurfacePoint.y, SurfacePoint.z,
                 ApproachPoint.x, ApproachPoint.y, ApproachPoint.z,
                 rvDrillNominal, PAnsiChar(NDPName));
  end;
// Note this repettition is intended do not modify
if NDPVectorIndex <> -1 then
  begin
  MoveVectorAtIndex(NDPVectorIndex,SurfacePoint,ApproachPoint);
  end;

// AddUnitLengthInversevector
InvApp.x := -ApproachPoint.x;
InvApp.y := -ApproachPoint.y;
InvApp.z := -ApproachPoint.z;
InvApp := PosNormalise(InvApp,10); // creates Unit vector
if InverseNDPVectorIndex < 0 then
  begin
  AddVector(SurfacePoint.x, SurfacePoint.y, SurfacePoint.z,
                 InvApp.x, InvApp.y, InvApp.z,
                 rvDrillNominal, PAnsiChar(InvNDPName));
  end;
// Note this repettition is intended do not modify
if InverseNDPVectorIndex <> -1 then
  begin
  MoveVectorAtIndex(InverseNDPVectorIndex,SurfacePoint,InvApp);
  end;
end;

procedure TG3DOTree.MoveVectorAtIndex(Index: Integer; Point1Pos,
  Point2Pos: TPLocation);
var
VecData : TVectorData;
P1,P2 :TG3DReportPoint;

begin
VecData := GetReportVectorFromList(Index);
P1 := GetVectorPoint(1,VecData);
P2 := GetVectorPoint(2,VecData);
P1.FPositionData.DesignTimeTranslation := Point1Pos;
P2.FPositionData.DesignTimeTranslation := Point2Pos;
end;

procedure TG3DOTree.FastDatumAxes(AxisCount: Integer);
var
LastAxisIndex : Integer;
AxisNum : Integer;
begin
if AxisCount >= 11 then
  begin
  LastAxisIndex := 11;
  end
else
  begin
  LastAxisIndex := AxisCount-1
  end;
For AxisNum := 0 to LastAxisIndex do
  begin
  AxisMoves[AxisNum].Move := True;
  AxisMoves[AxisNum].Position := 0;
  end;
MoveAllAxes(AxisCount,AxisMoves);
end;


procedure TG3DOTree.DeleteAllHolePoints;
var
HoleEntry : pHoleEntry;
PointNode    : TTreeNode;
GObject : TG3DObject;

begin
if not assigned(FHoleList) then exit;
while FHoleList.Count > 0 do
  begin
  HoleEntry := FHoleList[FHoleList.Count-1];
  PointNode := Items.GetNode(HoleEntry.NodeId);
  GObject := PointNode.Data;
  Gobject.Free;
  PointNode.Delete;
  FHoleList.Delete(FHoleList.Count-1);
  end;
MatchObjectListToTree;
end;


function TG3DOTree.CalculateStackingAxisTransform: TStackTransform;

procedure ShiftPoint(var InPoint : TPlocation; const Shift : TPLocation);
begin
Inpoint.x := Inpoint.x+Shift.x;
Inpoint.y := Inpoint.y+Shift.y;
Inpoint.z := Inpoint.z+Shift.z;
end;

var
ActP1,ActP2,ActP3 : TPLocation;
NomP1,NomP2,NomP3 : TPLocation;
Point1 : TG3dReportPoint;
Point2 : TG3dReportPoint;
Point3 : TG3dReportPoint;
InitialDelta : TPLocation;
NomRotation,ActRotation : TRotation;
XRot,YRot,ZRot : Double;
TotalRot : TRotation;
P1PosError,P2PosError,P3PosError : TPLocation;
begin
FCorrectionStartPosn := GetAllAxisPositions;
DatumAllAxes;
try
try
Result.XYZShift.x := 0;
Result.XYZShift.y := 0;
Result.XYZShift.z := 0;
Result.Rotation.x  := 0;
Result.Rotation.y := 0;
Result.Rotation.z := 0;
if ActualVectorIndex >= 0 then
   begin
   Point1 := GetVectorPoint(1,FReportVectorList[ActualVectorIndex]);
   Point2 := GetVectorPoint(2,FReportVectorList[ActualVectorIndex]);
   Point3 := GetVectorPoint(2,FReportVectorList[ATVectorIndex]);
//NGT!!!
   ActP1 :=GTLinearCorrected(Point1.AbsoluteTranslation);
   ActP2 :=GTLinearCorrected(Point2.AbsoluteTranslation);
   ActP3 :=GTLinearCorrected(Point3.AbsoluteTranslation);

   Point1 := GetVectorPoint(1,FReportVectorList[NominalVectorIndex]);
   Point2 := GetVectorPoint(2,FReportVectorList[NominalVectorIndex]);
   Point3 := GetVectorPoint(2,FReportVectorList[NTVectorIndex]);
//NGT!!!
   NomP1 :=GTLinearCorrected(Point1.AbsoluteTranslation);
   NomP2 :=GTLinearCorrected(Point2.AbsoluteTranslation);
   NomP3 :=GTLinearCorrected(Point3.AbsoluteTranslation);
   end;

InitialDelta.x := ActP1.x-NomP1.x;
InitialDelta.y := ActP1.y-NomP1.y;
InitialDelta.z := ActP1.z-NomP1.z;
ShiftPoint(NomP1,InitialDelta);
ShiftPoint(NomP2,InitialDelta);
ShiftPoint(NomP3,InitialDelta);

NomRotation :=Anglesbetween3DPoints(NomP1,NomP2);
ActRotation :=Anglesbetween3DPoints(ActP1,ActP2);


// first correct rotation about Y axis
YRot := NomRotation.y-ActRotation.y;
ActP2 :=PRotateAboutY3D(ActP2,ActP1,YRot);
ActP3 :=PRotateAboutY3D(ActP3,ActP1,YRot);


// Now find residual rotataion about X
NomRotation :=Anglesbetween3DPoints(NomP1,NomP2);
ActRotation :=Anglesbetween3DPoints(ActP1,ActP2);
XRot := NomRotation.x-ActRotation.x;
ActP2 :=PRotateAboutX3D(ActP2,ActP1,XRot);
ActP3 :=PRotateAboutX3D(ActP3,ActP1,XRot);

NomRotation :=Anglesbetween3DPoints(NomP1,NomP2);
ActRotation :=Anglesbetween3DPoints(ActP1,ActP2);

// Now find residual rotataion about Z as actual vetor is now alligned along Z axis
NomRotation :=Anglesbetween3DPoints(NomP1,NomP3);
ActRotation :=Anglesbetween3DPoints(ActP1,ActP3);
ZRot := NomRotation.z - ActRotation.z;
ActP3 :=PRotateAboutZ3D(ActP3,ActP1,ZRot);

Result.Rotation.x := XRot;
Result.Rotation.y := YRot;
Result.Rotation.z := ZRot;
Result.XYZShift := InitialDelta;
finally
RestoreAllAxisPOsitions(FCorrectionStartPosn);
end;
except
RestoreAllAxisPOsitions(FCorrectionStartPosn);
Result.XYZShift := SetPrecisePoint3D(0,0,0);
Result.Rotation := SetPRotation(0,0,0);
end;
end;


procedure TG3DOTree.TransformToStackingAxis(var Point : TPLocation; TXForm : TStackTransform);
procedure ReverseShiftPoint(var InPoint : TPlocation; const Shift : TPLocation);
begin
Inpoint.x := Inpoint.x-Shift.x;
Inpoint.y := Inpoint.y-Shift.y;
Inpoint.z := Inpoint.z+Shift.z;
end;

var
Point1 : TG3DReportPoint;
NomP1: TPLocation;
begin
if NominalVectorIndex >= 0 then
   begin

   Point1 := GetVectorPoint(1,FReportVectorList[NominalVectorIndex]);
   NomP1 := LinearCorrected(Point1.AbsoluteTranslation);

   //FirstRotate about z
   Point :=PRotateAboutZ3D(Point,NomP1,-TXForm.Rotation.z);

  // Now correct rotation about X axis
   Point :=PRotateAboutX3D(Point,NomP1,TXForm.Rotation.x);

  // Now correct rotation about Y axis
   Point :=PRotateAboutY3D(Point,NomP1,TXForm.Rotation.y);


  // shift point
   ReverseShiftPoint(Point,TXForm.XYZShift);
   end;


end;

function TG3DOTree.TransposeCRotation(var InC : Double;PosLim,NegLim : Double):Boolean;
begin
InC := Normalise360(90-InC);
Result := (InC <=PosLim) and (InC >= NegLim);
end;

function TG3DOTree.TransposeBRotation(var InB : Double;PosLim,NegLim : Double):Boolean;
begin
InB := PlusMinus360(-(360-InB));

Result := (InB <=PosLim) and (InB >= NegLim);
if Not Result then
  begin
  if InB > POsLim then
    begin
    InB := Inb-360;
    Result := (InB <=PosLim) and (InB >= NegLim);
    end
  else if InB < NegLim then
    begin
    InB := Inb+360;
    Result := (InB <=PosLim) and (InB >= NegLim);
    end
  end;
end;

function TG3DOTree.TransposeARotation(var InA : Double;PosLim,NegLim : Double):Boolean;
begin
InA := -Normalise180(InA);
Result := (InA <=PosLim) and (InA >= NegLim);
end;



// Solution 0 =  B <= 90
// Solution 1 = B > 90
// Solution 1 : B between 270 podsitive to + 90 throgh 360
// Solution 0 : B between 90.00001 and 270

function TG3DOTree.LinearCondition(LinearVal : Double):Double;
begin
Result := LinearVal;
if abs(LinearVal) < VerySmall then Result := 0.0
end;

function TG3DOTree.RotaryCondition(axIndex : Integer;RotaryVal : Double):Double;
begin
Result := Normalise360(RotaryVal);
if abs(RotaryVal) < VerySmall then Result := 0.0
else if abs(360-RotaryVal) < VerySmall then Result := 0.0;
case axIndex of
  0:
  // A axis must be Between 31 and -211
  begin
  while Result > 31 do
    Result := Result-360;
  while Result < -211 do
    Result := Result +360
  end;

  1:
  // B axis must between - 90 and + 270
  begin
  while Result > 270 do
    Result := Result-360;
  while Result < -90 do
    Result := Result +360
  end

  else
  begin
  end
  end;
end;

// Solution 1 : B between 270 podsitive to + 90 throgh 360
// Solution 0 : B between 90.00001 and 270

function TG3DOTree.GetGTNDPForFixedRotary(FixedRot: Double; FixedAxisIndex: Integer; var ResLinear, ResRotary: TXYZ;  Solution: Integer;InverseVector: Boolean;IgnoreLimits : Boolean): TNDPErrorTypes;
var
NDPVectorData        : TVectordata;
NDPResult            : TaxisPosn;
RunContinuous        : Boolean;
Stage                : Integer;
SurfacePosition      : TPLocation;
PointDifference      : TPLocation;
SavePosition         : TAxisPosn;
SimpleCase           : Boolean;
ZrotDiff          : Double;
AAdjust,BAdjust   : Double;
PartVectorIndex : Integer;
CurrentRot,RotDiff : Double;
DeltaPos : TPLocation;
ElectrodePos : TPLocation;
Vpos         : TPLocation;
PartVector : TVectorData;
Alignment : TAlignIndex;
CurrentSolution : Integer;
begin
// N.B. Stage Driven for debug purposes
ResRotary.X := 0;
ResRotary.Y := 0;
ResRotary.Z := 0;
ResLinear.X := 0;
ResLinear.Y := 0;
ResLinear.Z := 0;
Result := ndpOk;
Stage := 0;
RunContinuous := True;
SimpleCase := False;
//try
// Pass start position to result for initialisation
while  RunContinuous  do
begin
UpdateReportData(True);
UpdateFixedIndexes;
PartVectorIndex := NDPVectorIndex;
 if (PartVectorIndex < 0)  then
  begin
  Result := ndpNoPartVector;
  RunContinuous := False;
  exit;
  end;
if (InverseVector) and (InverseNDPVectorIndex >=0) then
 begin
 PartVectorIndex := InverseNDPVectorIndex;
 end
else
  begin
  PartVector := FReportVectorList[PartVectorIndex];
  end;

case Stage of
     0:
     begin
     NDPItterations := 0;
     NDPResult := GetAllAxisPositions;
     SavePosition := NDPResult;
     DatumAllAxes;
     UpdateFixedIndexes;
     end; //case 0

     1: // Stage =1
     //Get required Delta in C axis fro Datumed Zero Position
     begin
      case FixedAxisIndex of
        1: // Baxis
         begin
         NDPResult[4] := PlusMinus360(FixedRot);
         SetAxisPOsition(4,NDPResult[4]);
         end;

        2: // Caxis
         begin
         NDPResult[5] := PlusMinus360(FixedRot);
         SetAxisPOsition(5,NDPResult[5]);
         end

        else   // Axis
         begin
         NDPResult[3] := PlusMinus360(FixedRot);
         SetAxisPOsition(3,NDPResult[3]);
         end
      end; // case
     end; //case 1

     2: // Stage 2
     //Rotate B axis to Y axis Rotataion of ApproachVector
     begin
      case FixedAxisIndex of
        1: // Baxis
         begin
         end;

        2: // Caxis
         begin
         CurrentRot := (FReportVectorList[ApproachVectorIndex].Rotation.y-PartVector.Rotation.y); // Not Y rotatoin is reported zero vertical and cw is posative.
         if TransposeBRotation(CurrentRot,240,-60) or IgnoreLimits then
          begin
          NDPResult[4] := CurrentRot;
          SetAxisPOsition(4,NDPResult[4]);
          end
         else
          begin
          Result := ndpRequiredBOutsidePOsLimits;
          RunContinuous := False;
          end
         end
        else   // Axis
         begin
         end
      end; // case
     end; //case 2

     3: // Stage 3
      case FixedAxisIndex of
        1: // Baxis
         begin
         end;

        2: // Caxis
         begin
         //Rotate A Axis to match X rotataion
         UpdateReportData(True);
         CurrentRot := (FReportVectorList[ApproachVectorIndex].Rotation.x-PartVector.Rotation.x);

         if TransposeARotation(CurrentRot,180,-180) then
          begin
          CurrentRot := XYCompoundAngle(NDPResult[4],CurrentRot);
          NDPResult[3] := CurrentRot;
          SetAxisPOsition(3,NDPResult[3]);
          end
         else
          begin
          Result := ndpRequiredAOutsidePosLimits;
          RunContinuous := False;
          end;
          end
        else   // Axis
         begin
         end
     end; //case 3

     4:
     begin
     // Now look at Rotary error along X axis and get A adjust
     if 0 <> 1 then
        begin
        AAdjust :=  FReportVectorList[PartVectorIndex].Rotation.x-FReportVectorList[ApproachVectorIndex].Rotation.x;
        end
     else
        begin
        AAdjust :=  -(FReportVectorList[PartVectorIndex].Rotation.x-FReportVectorList[ApproachVectorIndex].Rotation.x);
        end;
     if abs(AAdjust) > NDPErrorLimt then
        begin
        NDPResult[3] := NDPResult[3]+AAdjust;
        NDPResult[3] := Normalise180(NDPResult[3]);
        SetAxisPOsition(3,NDPResult[3]);
        AOK := False;
        end
     else
        begin
        AOK := True;
        end;
     UpdateReportData(True);
     end;

     5:
     begin
     // Now look at Rotary error along Y axis and get B adjust
     BAdjust :=  FReportVectorList[ApproachVectorIndex].Rotation.y-FReportVectorList[PartVectorIndex].Rotation.y;
     BAdjust := Normalise180(BAdjust);
     if abs(BAdjust) > NDPErrorLimt then
        begin
        BOK := False;
        NDPResult[4] := NDPResult[4]+BAdjust;
        NDPResult[4] := Normalise180(NDPResult[4]);
        SetAxisPOsition(4,NDPResult[4]);
        end
     else
        begin
        BOK := True;
        end;
     UpdateReportData(True);
     if not (AOK and BOK) then
        begin
        inc(NDPItterations);
        if (NDPItterations < MaxIterations) then
          begin
          stage := stage-2;
          end
        end
     else
        begin
        NDPResult[4] := Normalise360(NDPResult[4]);
        // Solution 1 : B between 270 podsitive to + 90 throgh 360
        // Solution 0 : B between 90.00001 and 270
        if ((NDPResult[4] > 90)and(NDPResult[4] < 270)) then CurrentSolution := 0 else CurrentSolution := 1;
        If Solution <> CurrentSolution  then
          begin
          NDPResult[3] := 180-NDPResult[3];
          NDPResult[4] := Normalise180(NDPResult[4]+180);
          SetAxisPOsition(4,NDPResult[4]);
          SetAxisPOsition(3,NDPResult[3]);
          end
        end;
     end;

     6: // Stage 6
     begin
     UpdateFixedIndexes;
     Vpos := FReportVectorList[PartVectorIndex].Point1Position;
     ElectrodePos := FElectrodeLocation;
     DeltaPos := SubtractLocation(Vpos,ElectrodePos);
     NDPResult[0] := DeltaPos.x;
     NDPResult[1] := DeltaPos.y;
     NDPResult[2] := DeltaPos.z;
     UpdateReportData(True);
     end; //case 6

     7:
     begin
     RestoreAllAxisPOsitions(SavePosition);
     ResLinear.X := LinearCondition(NDPResult[0]);
     ResLinear.Y := LinearCondition(NDPResult[1]);
     ResLinear.Z := LinearCondition(NDPResult[2]);
     ResRotary.X := RotaryCondition(0,NDPResult[3]);
     ResRotary.Y := RotaryCondition(1,NDPResult[4]);
     ResRotary.Z := RotaryCondition(2,NDPResult[5]);
     RunContinuous := False;
     end;

   end; //case stage of
   inc(Stage);
  end; // while  RunContinuous  do
end;



function TG3DOTree.GetGTErrowsOfset: TXYZ;
begin
Result.X := 0;
Result.Y := 0;
Result.Z := 0;
DatumAllAxes;
UpdateReportData(False);
Result.X := ToolDatumPosition.x;
Result.Y := ToolDatumPosition.y;
Result.Z := ToolDatumPosition.z;
end;

procedure TG3DOTree.CompensateVFunc(xyz : TXYZ;ABC : TXYZ;Ball : TBallDescriptor);
var I, J, R : INteger;
    P1, P2 : TXYZ;
    M : Double;
    ProbeRadius : Double;
begin
{  I := aSMTag.StackParameter[0].AsInteger;
  J := aSMTag.StackParameter[1].AsInteger;
  R := aSMTag.StackParameter[2].AsInteger;

  P1.X := GEO.DataTransfer.D2[I, 0];
  P1.Y := GEO.DataTransfer.D2[I, 1];
  P1.Z := GEO.DataTransfer.D2[I, 2];

  P2.X := GEO.DataTransfer.D2[J, 0];
  P2.Y := GEO.DataTransfer.D2[J, 1];
  P2.Z := GEO.DataTransfer.D2[J, 2];}

{  if not Assigned(ActiveProbe) then begin
    ErrorMessageString := Format('Probe Offset "%s" not found', [aSMTag.StackParameter[0].AsString]);
    PostMessage(Handle, CNCM_MESSAGE, Ord(FaultRewind), 0);
    Sleep(10);
    Exit;
  end;}

(*  try
    ProbeRadius := ActiveProbe.GetProbeRadius(P2);
  except
    on E : Exception do begin
      ErrorMessageString := Format('Probe Offset failed due to "%s"', [E.Message]);
      PostMessage(Handle, CNCM_MESSAGE, Ord(FaultRewind), 0);
      Exit;
    end;
  end;

  if ProbeRadius <> 0 then begin
    M := (P2.X*P2.X)+(P2.Y*P2.Y)+(P2.Z*P2.Z);
    M := System.Sqrt(M);
    M := M / ProbeRadius;
   P2.X := P2.X / M;
    P2.Y := P2.Y / M;
    P2.Z := P2.Z / M;
  end else begin
    P2.X := 0;
    P2.Y := 0;
    P2.Z := 0;
  end;
  GEO.DataTransfer.D2[R, 0] := P1.X + P2.X;
  GEO.DataTransfer.D2[R, 1] := P1.Y + P2.Y;
  GEO.DataTransfer.D2[R, 2] := P1.Z + P2.Z;*)
end;

function TG3DOTree.CheckAxisLimits(Position: TAxisPosn): TNDPErrorTypes;
begin
  Result := ndpOk;
  exit;
  if (Position[0] > FXLimitsPositive) then Result := ndpRequiredXOutsidePosLimits
  else if (POsition[0] < FXLimitsNegative) then Result := ndpRequiredXOutsideNegLimits
  else if (Position[1] > FYLimitsPositive) then Result := ndpRequiredYOutsidePosLimits
  else if (POsition[1] < FYLimitsNegative) then Result := ndpRequiredYOutsideNegLimits
  else if (Position[2] > FZLimitsPositive) then Result := ndpRequiredZOutsidePosLimits
  else if (POsition[2] < FZLimitsNegative) then Result := ndpRequiredZOutsideNegLimits
  else if (Position[3] > FALimitsPositive) then Result := ndpRequiredAOutsidePosLimits
  else if (POsition[3] < FALimitsNegative) then Result := ndpRequiredAOutsideNegLimits
  else if (Position[4] > FBLimitsPositive) then Result := ndpRequiredBOutsidePosLimits
  else if (POsition[4] < FBLimitsNegative) then Result := ndpRequiredBOutsideNegLimits;
end;

procedure TG3DOTree.GetGTMachineToolRetractVector(VectorLength: Double;var  IncResult: TXYZ);
var
ApVec :  TVectorData;
UnitVectorApp : TPLocation;
LocResult     : TPLocation;
begin
UpdateFixedIndexes;
IncResult.X := 0;
IncResult.Y := 0;
IncResult.Z := 0;
if ApproachVectorIndex >= 0 then
  begin
  ApVec := FReportVectorList[ApproachVectorIndex];
  UnitVectorApp := VectorNormalise(ApVec);
  LocResult := VectorScale(UnitVectorApp,VectorLength);
  IncResult.X := LocResult.X;
  IncResult.Y := LocResult.Y;
  IncResult.Z := LocResult.Z;

  end;

end;
{
 TVectorData = record
         VectorType       : TReportVectorType;
         Point1NodeId     : HTREEITEM;
         Point2NodeId     : HTREEITEM;
         Name             : String;
         CentrePosition   : TPlocation;
         Length           : Double;
         Rotation         : TRotation;
         Point1Position   : TPlocation;
         Point2Position   : TPlocation;
         isFrozen         : Boolean;
         DontDisplay      : Boolean;
         end;
}


procedure TG3DOTree.ModifyNDPOffsetFor4Points(P1, P2, P3, P4 : TPlocation; var Rotations : TPlocation);
var
XAngle,YAngle : Double;
MaxX,MinX,MaxY,MinY :Double;
MaxMinXDist,MaxMinYDist : Double;
ZatMaxX,ZatMinX,ZatMaxY,ZatMinY : Double;
DeltaZinX,DeltaZinY : Double;
XViewedangle,YViewedangle : Double;
YCompoundAngle : Double;
procedure TestPoint(Point : TPlocation);
begin
if Point.x > MaxX then
  begin
  MaxX := Point.x;
  ZatMaxX := POint.z;
  end;
if Point.y > MaxY then
  begin
  MaxY := Point.y;
  ZatMaxY := POint.z;
  end;
if Point.x < MinX then
  begin
  MinX := Point.x;
  ZatMinX := Point.z;
  end;
if Point.y < MinY then
  begin
  MinY := Point.y;
  ZatMiny := Point.z;
  end;
end;
begin
MaxX := -1000000000;
MinX := 10000000000;
MaxY := -1000000000;
MinY := 10000000000;
TestPoint(P1);
TestPoint(P2);
TestPoint(P3);
TestPoint(P4);
MaxMinXDist := MaxX-MinX;
MaxMinYDist := MaxY-MinY;
DeltaZinX := ZatMaxX-ZatMinX;
DeltaZinY := ZatMaxY-ZatMinY;
XViewedAngle := 0;
if DeltaZinX <> 0 then
 begin
 XViewedAngle := RadToDeg(ArcTan(MaxMinXDist/DeltaZinX));
 end;
YViewedAngle := 0;
if DeltaZinY <> 0 then
 begin
 YViewedAngle := RadToDeg(ArcTan(MaxMinYDist/DeltaZinY));
 end;

YCompoundAngle := XYCompoundAngle(XViewedAngle,YViewedangle);

SetAndSaveNominalOffset(0,0,0,0,XViewedangle,YCompoundAngle,0);

Rotations.x := XViewedangle;
Rotations.y := YCompoundAngle;
Rotations.z := 0;


end;

{
This function returns the touch point for a probing position
x,y,z,a,b,c are input together with ijk of approach vector
uses known position of 'electrode' in GEO to calculate centre of ball
Note XYZ,ABC has already been externally compensated for ball radius
and is in effect the axis positions when the centre of the ball would have hit the surface
This is the eqiavalent of the elctrode location in GEO
So all we need to do is put the machine to the
}

function TG3DOTree.ResolveProbePoint(XYZ, ABC: TPLocation): TPlocation;
var
CurrentPositions: TAxisPOsn;

begin
CurrentPositions:=GetAllAxisPOsitions;

SetAxisPosition(0,XYZ.x);
SetAxisPosition(1,XYZ.y);
SetAxisPosition(2,XYZ.z);
SetAxisPosition(3,ABC.x);
SetAxisPosition(4,ABC.y);
SetAxisPosition(5,ABC.z);
UpdateReportData(True);
// Electrode position in machine coordinates is now touch position
Result := SubtractLocation(FElectrodeLocation,FPartDatumLocation);
try
finally
RestoreAllAxisPOsitions(CurrentPositions);
end;
end;

function TG3DOTree.GetGTNDPForZAlignedHole(Zrot: Double; var ResLinear,ResRotary: TXYZ; Solution: Integer;InverseVector: Boolean): TNDPErrorTypes;
var
NDPVectorData        : TVectordata;
NDPResult            : TaxisPosn;
RunContinuous        : Boolean;
Stage                : Integer;
SurfacePosition      : TPLocation;
PointDifference      : TPLocation;
SavePosition         : TAxisPosn;
SimpleCase           : Boolean;
ZrotDiff          : Double;
AAdjust,BAdjust   : Double;
PartVectorIndex : Integer;
CurrentRot,RotDiff : Double;
DeltaPos : TPLocation;
ElectrodePos : TPLocation;
Vpos         : TPLocation;
PartVector : TVectorData;
Alignment : TAlignIndex;
PRadius : Double;
AxisLimitResult : TNDPErrorTypes;
CurrentSolution : Integer;
ElectrodeAngle : Double;
begin
// N.B. Stage Driven for debug purposes
ResRotary.X := 0;
ResRotary.Y := 0;
ResRotary.Z := 0;
ResLinear.X := 0;
ResLinear.Y := 0;
ResLinear.Z := 0;
Result := ndpOK;
Stage := 0;
RunContinuous := True;
SimpleCase := False;
//try
// Pass start position to result for initialisation
while  RunContinuous  do
begin
UpdateReportData(True);
UpdateFixedIndexes;
PartVectorIndex := NDPVectorIndex;
 if (PartVectorIndex < 0)  then
  begin
  Result := ndpNoPartVector;
  RunContinuous := False;
  exit;
  end;
if (InverseVector) and (InverseNDPVectorIndex >=0) then
 begin
 PartVectorIndex := InverseNDPVectorIndex;
 PartVector := FReportVectorList[PartVectorIndex];
 end
else
  begin
  PartVector := FReportVectorList[PartVectorIndex];
  end;

case Stage of
     0:
     begin
     NDPItterations := 0;
     NDPResult := GetAllAxisPositions;
     SavePosition := NDPResult;

     DatumAllAxes;
     UpdateFixedIndexes;
     ElectrodeAngle := Normalise180(FReportVectorList[ApproachVectorIndex].Rotation.y);
     end; //case 0

     1: // Stage =1
     //Get required Delta in C axis fro Datumed Zero Position
     begin
     Alignment := AlignmentIndex(PartVector);
     if Alignment = alaxVert then
      begin
      //This is a special case as there is no X/Y component of the vector so
      // just put the surface point at theradial angle required
      if (abs(PartVector.Point1Position.x) < verySmall) and (abs(PartVector.Point1Position.y) < verySmall) then
        begin
       // if XY values are zero the put C axis at zero
        NDPResult[5] := 0;
        SetAxisPOsition(5,NDPResult[5]);
        end
      else
        begin
        // place surface point at desired radial angle
        CurrentRot := RadToDeg(ArcTan2(PartVector.Point1Position.y,PartVector.Point1Position.x));
        RotDiff := Normalise360(Zrot- CurrentRot);
        NDPResult[5] := -RotDiff;
        SetAxisPOsition(5,NDPResult[5]);
        end;
      end
     else
      begin
      CurrentRot := PartVector.Rotation.z; // NOTE REPORTED ROTATION IS ZERO ALIGNED WITH y+
      TransposeCRotation(CurrentRot,360,0);
      RotDiff := (-Zrot)-CurrentRot;
      NDPResult[5] := 360-RotDiff;
      if NDPResult[5] = 360 then NDPResult[5] := 0;
      SetAxisPOsition(5,NDPResult[5]);
      end;
     end; //case 1

     2: // Stage 2
     //Rotate B axis to Y axis Rotataion of ApproachVector
     begin
     UpdateFixedIndexes;
     CurrentRot := (FReportVectorList[ApproachVectorIndex].Rotation.y-PartVector.Rotation.y); // Not Y rotatoin is reported zero vertical and cw is posative.
     TransposeBRotation(CurrentRot,FBLimitsPositive,FBLimitsNegative);
//REVB!!!     NDPResult[4] := CurrentRot;
     NDPResult[4] := -CurrentRot;
     SetAxisPOsition(4,NDPResult[4])
     end; //case 2

     3: // Stage 3
     //Rotate A Axis to match X rotataion
     begin
     UpdateReportData(True);
     CurrentRot := (FReportVectorList[ApproachVectorIndex].Rotation.x-PartVector.Rotation.x);
     TransposeARotation(CurrentRot,FALimitsPositive,FALimitsNegative);
      begin
      try
      Alignment := AlignmentIndex(PartVector);
//      if Alignment = alaXHorizontal then
//        begin
//        NDPResult[3] := 0 ;
//        end
        if (Alignment = alHorizontal) or (Alignment = alaXHorizontal) then
        begin
///REVB!!!        if NDPResult[4] = 90 then
        if NDPResult[4] = -90 then
          begin
          NDPResult[3] := PartVector.Rotation.z-270;
          end
        else
          begin
          NDPResult[3] := -PartVector.Rotation.z+90;
          end;
//        SetAxisPOsition(3,NDPResult[3]);
        end
      else
        begin
        CurrentRot := XYCompoundAngle(NDPResult[4],CurrentRot);
        NDPResult[3] := CurrentRot;
        end;
      except
      Result := ndpRequiredAOutsidePosLimits;
      RunContinuous := False;
      end;
      end;
      NDPResult[3] := RotaryCondition(0,NDPResult[3]);
      SetAxisPOsition(3,NDPResult[3]);
      SetAxisPOsition(4,NDPResult[4]);
      end;

     4:
     begin
     UpdateFixedIndexes;
     CurrentRot := (FReportVectorList[ApproachVectorIndex].Rotation.y-PartVector.Rotation.y); // Not Y rotatoin is reported zero vertical and cw is posative.
     TransposeBRotation(CurrentRot,FBLimitsPositive,FBLimitsNegative);
      if abs(CurrentRot) > 90 then
        begin
        NDPResult[3] := -(180+NDPResult[3]);
        end ;
     SetAxisPOsition(3,NDPResult[3]);
     SetAxisPOsition(4,0);
     end;

     5:
     begin
     UpdateFixedIndexes;
     CurrentRot := (FReportVectorList[ApproachVectorIndex].Rotation.y-PartVector.Rotation.y); // Not Y rotatoin is reported zero vertical and cw is posative.
     TransposeBRotation(CurrentRot,FBLimitsPositive,FBLimitsNegative);
///REVB!!!     NDPResult[4] := CurrentRot;
     NDPResult[4] := -CurrentRot;
     SetAxisPOsition(4,NDPResult[4]);
     end;

     6:
     begin
     // Now look at Rotary error along X axis and get A adjust
     if 0 <> 1 then
        begin
        AAdjust :=  FReportVectorList[PartVectorIndex].Rotation.x-FReportVectorList[ApproachVectorIndex].Rotation.x;
        end
     else
        begin
        AAdjust :=  -(FReportVectorList[PartVectorIndex].Rotation.x-FReportVectorList[ApproachVectorIndex].Rotation.x);
        end;
     if abs(AAdjust) > NDPErrorLimt then
        begin
        NDPResult[3] := NDPResult[3]+AAdjust;
        NDPResult[3] := Normalise180(NDPResult[3]);
        SetAxisPOsition(3,NDPResult[3]);
        AOK := False;
        end
     else
        begin
        AOK := True;
        end;
     UpdateReportData(True);
     end;

     7:
     begin
     // Now look at Rotary error along Y axis and get B adjust
//REVB!!!     BAdjust :=  FReportVectorList[ApproachVectorIndex].Rotation.y-FReportVectorList[PartVectorIndex].Rotation.y;
     BAdjust :=  -(FReportVectorList[ApproachVectorIndex].Rotation.y-FReportVectorList[PartVectorIndex].Rotation.y);
     BAdjust := Normalise180(BAdjust);
     if abs(BAdjust) > NDPErrorLimt then
        begin
        BOK := False;
        NDPResult[4] := NDPResult[4]+BAdjust;
        NDPResult[4] := Normalise180(NDPResult[4]);
        SetAxisPOsition(4,NDPResult[4]);
        end
     else
        begin
        BOK := True;
        end;
     UpdateReportData(True);
     if not (AOK and BOK) then
        begin
        inc(NDPItterations);
        if (NDPItterations < MaxIterations) then
          begin
          stage := stage-2;
          end
        end;
     NDPResult[4] := Normalise360(NDPResult[4]);
     if (AOK and BOK) then
       begin
       if ((NDPResult[4] < -90)and(NDPResult[4] < 270)) then CurrentSolution := 0 else CurrentSolution := 1;
          If Solution <> CurrentSolution  then
            begin
            // THis only works for approach vector of 0,0,1!!!!
//            NDPResult[3] := 180-NDPResult[3];
//            NDPResult[4] := Normalise180(NDPResult[4]-180-(ElectrodeAngle*2));
            NDPResult[3] := 180-NDPResult[3];
            NDPResult[4] := Normalise180(NDPResult[4]-180+(ElectrodeAngle*2));
            SetAxisPOsition(4,NDPResult[4]);
            SetAxisPOsition(3,NDPResult[3]);
            end
       end;
     end;


     8:// Stage 8
     begin
     UpdateFixedIndexes;
     Vpos := FReportVectorList[PartVectorIndex].Point1Position;
     ElectrodePos := FElectrodeLocation;
     DeltaPos := SubtractLocation(Vpos,ElectrodePos);
     NDPResult[0] := DeltaPos.x;
     NDPResult[1] := DeltaPos.y;
     NDPResult[2] := DeltaPos.z;
     UpdateReportData(True);
     end; //case 3

     9: // stage = 9
     begin
     RestoreAllAxisPOsitions(SavePosition);
     while NDPResult[5] >= 360 do  NDPResult[5] := NDPResult[5]-360;
     while NDPResult[4] >= 360 do  NDPResult[4] := NDPResult[4]-360;
     while NDPResult[3] >= 360 do  NDPResult[3] := NDPResult[3]-360;
     Result := CheckAxisLimits(NDPResult);
     ResLinear.X := LinearCondition(NDPResult[0]);
     ResLinear.Y := LinearCondition(NDPResult[1]);
     ResLinear.Z := LinearCondition(NDPResult[2]);
     ResRotary.X := RotaryCondition(0,NDPResult[3]);
     ResRotary.Y := RotaryCondition(1,NDPResult[4]);
     ResRotary.Z := RotaryCondition(2,NDPResult[5]);
     RunContinuous := False;
     end;

   end; //case stage of
   inc(Stage);
  end; // while  RunContinuous  do
end;

Function TG3DOTree.UpdateAxisAndVectorData(var AlignmentData : TAlignmentData;
                                           UseInverseApproach : Boolean):Boolean;
var
PVIndex,AVIndex : Integer;
PV :TVectorData;
AV :TVectorData;
begin
Result := True;
UpdateReportData(True);
UpdateFixedIndexes;
UpdateReportData(True);
PVIndex := NDPVectorIndex;
AVIndex := ApproachVectorIndex;
 if (PVIndex < 0)  then
    begin
    Result := False;
    exit;
    end;
 if (UseInverseApproach) and (InverseNDPVectorIndex >=0) then
    begin
    PVIndex := InverseNDPVectorIndex;
    end;
 PV := FReportVectorList[PVIndex];
 AV := FReportVectorList[AVIndex];
 With AlignmentData do
  begin
  PartVectorRotation := PV.Rotation;
  ApproachVectorRotation := AV.Rotation;
  RotationDelta.x := Normalise360(ApproachVectorRotation.x-PartVectorRotation.x);
  RotationDelta.y := Normalise360(ApproachVectorRotation.y-PartVectorRotation.y);
  RotationDelta.z := Normalise360(ApproachVectorRotation.z-PartVectorRotation.z);
  AllAxesAligned := ((abs(RotationDelta.x)<0.0005) and (abs(RotationDelta.y)<0.0005) and (abs(RotationDelta.z)<0.0005));
//  Geo.DebugMemo.Lines.add(Format('RE: %7.3f,%7.3f,%7.3f',[RotationDelta.x,RotationDelta.y,RotationDelta.z]));
  end;
end;

Function TG3DOTree.DoABIterate(   var APos: Double;
                                  var BPos: Double;
                                  PartVector : TVectorData;
                                  UseInverseApproach : Boolean;
                                  isReItterate : Boolean):Boolean;
var
ItterationsDone  : Boolean;
FailedItterations: Boolean;
CurrentRot : Double;
AAdjust,BAdjust : Double;
AIsOK,BIsOk : Boolean;
ItterateCount : Integer;
AlignmentData : TAlignmentData;
begin
Result := False;
ItterationsDone := False;
FailedItterations:= False;
ItterateCount := 0;
    while (ItterationsDone = False) and (FailedItterations = False) do
      begin
      // --------------------------
      UpdateAxisAndVectorData(AlignmentData,UseInverseApproach);
      // Note Y rotatoin is reported zero vertical and cw is posative.
//REVB!!!      BAdjust := AlignmentData.RotationDelta.y;
      BAdjust := -(AlignmentData.RotationDelta.y);
      GEo.DebugMemo.Lines.Add(Format('Itterate1 InitB Adj: %5.3f,,I:%d',[BAdjust,ItterateCount]));
      TransposeBRotation(BAdjust,FBLimitsPositive,FBLimitsNegative);
{      //what does this do???
      if (abs(BAdjust) > 90) and (not isReItterate) then
         begin
         APos := -(180+APos);
         end ;}
      SetAxisPOsition(3,APos);
      BPos := BPos+BAdjust;
      SetAxisPOsition(4,BPos);
      GEo.DebugMemo.Lines.Add(Format('Itterate1 A: %5.3f,B: %5.3f,I:%d',[Apos,Bpos,ItterateCount]));

     // --------------------------
     // Now look at Rotary error along X axis and get A adjust
      UpdateAxisAndVectorData(AlignmentData,UseInverseApproach);
      //AAdjust :=  PartVector.Rotation.x-FReportVectorList[ApproachVectorIndex].Rotation.x;
      AAdjust :=  Normalise360(AlignmentData.RotationDelta.x);
      GEo.DebugMemo.Lines.Add(Format('Itterate2 A Adj: %5.3f,,I:%d',[AAdjust,ItterateCount]));
      if abs(AAdjust) > NDPErrorLimt then
         begin
         APos := APos+(AAdjust*0.1) ;
         APos := Normalise180(APos);
         SetAxisPOsition(3,APos);
         GEo.DebugMemo.Lines.Add(Format('Itterate2 A: %5.3f,B: %5.3f,I:%d',[Apos,Bpos,ItterateCount]));
         AIsOK := False;
         end
      else
         begin
         AIsOK := True;
         end;

      // --------------------------
      // Now look at Rotary error along Y axis and get B adjust
      UpdateAxisAndVectorData(AlignmentData,UseInverseApproach);
//REVB!!!      BAdjust := AlignmentData.RotationDelta.y;
      BAdjust := -AlignmentData.RotationDelta.y;
      BAdjust := Normalise180(BAdjust);
      GEo.DebugMemo.Lines.Add(Format('Itterate3 B Adj: %5.3f,,I:%d',[BAdjust,ItterateCount]));
      if abs(BAdjust) > NDPErrorLimt then
         begin
         BIsOK := False;
         BPos := BPos+(BAdjust*0.1);
         BPos := Normalise180(BPos);
         SetAxisPOsition(4,BPos);
         end
      else
         begin
         BIsOK := True;
         end;

      GEo.DebugMemo.Lines.Add(Format('Itterate3 A: %5.3f,B: %5.3f,I:%d',[Apos,Bpos,ItterateCount]));
      UpdateAxisAndVectorData(AlignmentData,UseInverseApproach);
      APos := Normalise360(APos);
      if not (AIsOK and BIsOK) then
        begin
        inc(ItterateCount);
        if (ItterateCount > MaxIterations) then
          begin
          FailedItterations := True;
          end
        end
      else
        begin
        ItterationsDone := True;
        end
    end; // End of Itterations
  Result := Not FailedItterations;

end;

Function TG3DOTree.ItterateForAB(var APos: Double;
                                  var BPos: Double;
                                  PartVector : TVectorData;
                                  UseInverseApproach : Boolean;
                                  GetInverseofStartPos : Boolean;
                                  ElectrodeAngle : Double): Boolean;
var
CurrentRot : Double;
Alignment : TAlignIndex;
AAdjust,BAdjust : Double;
AIsOK, BIsOK : Boolean;
ItterationsDone : Boolean;
FailedItterations : Boolean;
ItterateCount : Integer;
BufferApos,BufferBpos : Double;
AlignData : TAlignmentData;
AllAligned : Boolean;
CItterateCount : Integer;
LastCError : Double;
CurrentCError : Double;
ACGain : Double;
MiddleBit : Double;
begin
   Geo.DebugMemo.Lines.add('itterating for A/B');
   BufferApos := APos;
   BufferBpos := BPos;
   UpdateAxisAndVectorData(AlignData,UseInverseApproach);
   CurrentRot := AlignData.RotationDelta.x;
//   CurrentRot := (FReportVectorList[ApproachVectorIndex].Rotation.x-PartVector.Rotation.x);
   TransposeARotation(CurrentRot,FALimitsPositive,FALimitsNegative);
    begin
    try
    GEo.DebugMemo.Lines.Add('IStart');
    Alignment := AlignmentIndex(PartVector);
    if (Alignment = alHorizontal) or (Alignment = alaXHorizontal) then
      begin
//REVB!!!!      if BPos = 90 then
      if BPos = -90 then
        begin
        Geo.DebugMemo.Lines.add('Horiz B90');
        APos := PartVector.Rotation.z-270;
        end
      else
        begin
        Geo.DebugMemo.Lines.add('Horiz B NOT 90');
        APos := -PartVector.Rotation.z+90;
        end;
      end
    else
      begin
      // this is an attempt to get the correct  A angle
      // but is failing for bent noseguide .... why
      Geo.DebugMemo.Lines.add(Format('Not Horiz, Compound of B:%5.3f,Current %5.3f = %5.3f',[Bpos,CurrentRot,APos]));
      CurrentRot := XYCompoundAngle(BPos,CurrentRot);
      APos := CurrentRot;
      Geo.DebugMemo.Lines.add(Format('Not Horiz, Compound of B:%5.3f,Current %5.3f = %5.3f',[Bpos,CurrentRot,APos]));
      end;
    except
    Result := False;
    exit;
    end;
    end;

    // Set A axis to Zero in order to measure required angle
    APos := RotaryCondition(0,Apos);
    SetAxisPOsition(3,APos);
    SetAxisPOsition(4,BPos);
    GEo.DebugMemo.Lines.Add(Format('A: %5.3f,B: %5.3f',[Apos,Bpos]));
    UpdateAxisAndVectorData(AlignData,UseInverseApproach);

    AllAligned := False;
    FailedItterations  := False;
    CItterateCount := 0;
    LastCerror := AlignData.RotationDelta.z;

    while (AllAligned = False) and (FailedItterations = False) do
      begin
      if not DoABIterate(APos,Bpos,PartVector,UseInverseApproach,False) then
        begin
        GEo.DebugMemo.Lines.Add('Failed A/B ittreate');
        Result := False;
        exit;
        end
      else
        begin
        UpdateAxisAndVectorData(AlignData,UseInverseApproach);
        AllAligned := AlignData.AllAxesAligned;
        if not AllAligned then
          begin
          CurrentCError := AlignData.RotationDelta.z;
          if CurrentCError > 0.0001 then
            begin
            SetAxisPosition(3,Apos+(CurrentCError/2));
            UpdateAxisAndVectorData(AlignData,UseInverseApproach);
            ACGain := AlignData.RotationDelta.z/CurrentCError;
            end;
          If AcGain > 0 then
            begin
            GEO.DebugMemo.Lines.Add('+')
            end
          else
            begin
            GEO.DebugMemo.Lines.Add('-')
            end;
          inc(CItterateCount);
          FailedItterations := CitterateCount > 100;
          end;
        end;
      end;

   if not GetInverseofStartPos then
      begin
      Result := not FailedItterations;
      end
   else
      begin
      If ((Abs(Apos)-Abs(BufferApos)) < 0.0001) and ((Abs(Bpos)-Abs(BufferBpos)) < 0.0001) then
         begin
         // Need to Find the other solution
         // Try negating B and adding 180 to A
         MiddleBit := (180-(2*ElectrodeAngle))/180;
         if abs(Apos) < 1 then Apos := -180 else Apos := 0;
         BPos := BPos+180+(2*ElectrodeAngle);
         APos := RotaryCondition(0,Apos);
         BPos := RotaryCondition(1,Bpos);
         SetAxisPOsition(3,APos);
         SetAxisPOsition(4,BPos);
         GEo.DebugMemo.Lines.Add(Format('Pre Itterate A: %5.3f,B: %5.3f',[Apos,Bpos]));
         Result := DoABIterate(APos,Bpos,PartVector,UseInverseApproach,True);
         end;
      end;

end;

function TG3DOTree.NewGetGTNDPForZAlignedHole(Zrot: Double;
                                              var ResLinear,
                                              ResRotary: TXYZ;
                                              Solution: Integer;
                                              InverseVector: Boolean
                                              ): TNDPErrorTypes;
var
NDPVectorData        : TVectordata;
NDPResult            : TaxisPosn;
RunContinuous        : Boolean;
Stage                : Integer;
SurfacePosition      : TPLocation;
PointDifference      : TPLocation;
SavePosition         : TAxisPosn;
SimpleCase           : Boolean;
ZrotDiff          : Double;
AAdjust,BAdjust   : Double;
PartVectorIndex : Integer;
CurrentRot,RotDiff : Double;
DeltaPos : TPLocation;
ElectrodePos : TPLocation;
Vpos         : TPLocation;
PartVector : TVectorData;
Alignment : TAlignIndex;
PRadius : Double;
AxisLimitResult : TNDPErrorTypes;
CurrentSolution : Integer;
ElectrodeAngle : Double;
InternalItterationCount : Integer;
Success : Boolean;
BSolPos : Double;
ALignData : TAlignmentData;
begin
// N.B. Stage Driven for debug purposes
ResRotary.X := 0;
ResRotary.Y := 0;
ResRotary.Z := 0;
ResLinear.X := 0;
ResLinear.Y := 0;
ResLinear.Z := 0;
Result := ndpOK;
Stage := 0;
RunContinuous := True;
SimpleCase := False;
Geo.DebugMemo.Lines.Clear;

//try
// Pass start position to result for initialisation
while  RunContinuous  do
begin
UpdateReportData(True);
UpdateAxisAndVectorData(ALignData,InverseVector);

PartVectorIndex := NDPVectorIndex;
 if (PartVectorIndex < 0)  then
  begin
  Result := ndpNoPartVector;
  RunContinuous := False;
  exit;
  end;

 if (InverseVector) and (InverseNDPVectorIndex >=0) then
   begin
   PartVectorIndex := InverseNDPVectorIndex;
   PartVector := FReportVectorList[PartVectorIndex];
   end
  else
    begin
    PartVector := FReportVectorList[PartVectorIndex];
    end;

  Geo.DebugMemo.Lines.add(Format('Stage %d',[Stage]));

  case Stage of
     0:
     begin
     NDPItterations := 0;
     NDPResult := GetAllAxisPositions;
     SavePosition := NDPResult;
     DatumAllAxes;
     UpdateAxisAndVectorData(ALignData,InverseVector);
     ElectrodeAngle := Normalise180(FReportVectorList[ApproachVectorIndex].Rotation.y);
     end; //case 0

     1: // Stage =1
     //Get required Delta in C axis fro Datumed Zero Position
     begin
     Alignment := AlignmentIndex(PartVector);
     if Alignment = alaxVert then
      begin
      Geo.DebugMemo.Lines.add('Alignment alaxVert');
      //This is a special case as there is no X/Y component of the vector so
      // just put the surface point at theradial angle required
      if (abs(PartVector.Point1Position.x) < verySmall) and (abs(PartVector.Point1Position.y) < verySmall) then
        begin
       // if XY values are zero the put C axis at zero
        Geo.DebugMemo.Lines.add('XY values are zero the put C axis at zero');
        NDPResult[5] := 0;
        SetAxisPOsition(5,NDPResult[5]);
        end
      else
        begin
        // place surface point at desired radial angle
        CurrentRot := RadToDeg(ArcTan2(PartVector.Point1Position.y,PartVector.Point1Position.x));
        RotDiff := Normalise360(Zrot- CurrentRot);
        NDPResult[5] := -RotDiff;
        SetAxisPOsition(5,NDPResult[5]);
        Geo.DebugMemo.Lines.add(Format('C axis set to %5.4f',[NDPResult[5]]));
        end;
      end
     else
      begin
      Geo.DebugMemo.Lines.add('Alignment NOT alaxVert');
      CurrentRot := PartVector.Rotation.z; // NOTE REPORTED ROTATION IS ZERO ALIGNED WITH y+
      TransposeCRotation(CurrentRot,360,0);
      RotDiff := (-Zrot)-CurrentRot;
//      NDPResult[5] := -(360-RotDiff);
      NDPResult[5] := (360-RotDiff);
      if NDPResult[5] = 360 then NDPResult[5] := 0;
      SetAxisPOsition(5,NDPResult[5]);
      Geo.DebugMemo.Lines.add(Format('C axis set to %5.4f',[NDPResult[5]]));
      RetryCount := 0;
      end;
     end; //case 1

     2: //Itterate Caxis
      begin
      inc(RetryCount);
      if RetryCount < 10 then
        begin
        CurrentRot := PartVector.Rotation.z; // NOTE REPORTED ROTATION IS ZERO ALIGNED WITH y+
        TransposeCRotation(CurrentRot,360,0);
        RotDiff := (-Zrot)-CurrentRot;
        if RotDiff >  0.002 then
          begin
          CurrentRot := PartVector.Rotation.z; // NOTE REPORTED ROTATION IS ZERO ALIGNED WITH y+
          TransposeCRotation(CurrentRot,360,0);
          RotDiff := (-Zrot)-CurrentRot;
          NDPResult[5] := (360-RotDiff);
//      NDPResult[5] := -(360-RotDiff);
          if NDPResult[5] = 360 then NDPResult[5] := 0;
          SetAxisPOsition(5,NDPResult[5]);
          Geo.DebugMemo.Lines.add(Format('C axis set to %5.4f',[NDPResult[5]]));
          end
        end
      else
        begin
        // here For error
        end;
      end;


     3: // Stage 3
     //Rotate B axis to Y axis Rotataion of ApproachVector
     begin
     Geo.DebugMemo.Lines.add('Rotate B axis to Y axis Rotataion of ApproachVector');
     UpdateAxisAndVectorData(ALignData,InverseVector);
     CurrentRot := ALignData.RotationDelta.y;
     TransposeBRotation(CurrentRot,FBLimitsPositive,FBLimitsNegative);
//REVB!!!     NDPResult[4] := CurrentRot;
     NDPResult[4] := -CurrentRot;
     SetAxisPOsition(4,NDPResult[4]) ;
     Geo.DebugMemo.Lines.add(Format('B axis set to %5.4f',[NDPResult[4]]));
     end; //case 2

     4: // Stage 3
     //Rotate A Axis to match X rotataion
     begin
     UpdateAxisAndVectorData(ALignData,InverseVector);
     Success := ItterateForAB(NDPResult[3],NDPResult[4],PartVector,InverseVector,False,ElectrodeAngle);
     if Success then
       begin
        SetAxisPOsition(3,NDPResult[3]);
        SetAxisPOsition(4,NDPResult[4]);
       end
     else
       begin
       Result := ndpItterateError;
       exit;
       end;

     end;

     5: // Stage 3
     begin
       BSolPos := Normalise360(NDPResult[4]);
       case Solution of
         0:
         begin
         if not ((BSolPos > 90)and(BSolPos <= 270)) then
           begin
           SetAxisPOsition(3,NDPResult[3]);
           SetAxisPOsition(4,NDPResult[4]);
           Success := ItterateForAB(NDPResult[3],NDPResult[4],PartVector,InverseVector,True,ElectrodeAngle);
           if Success then
              begin
              SetAxisPOsition(3,NDPResult[3]);
              SetAxisPOsition(4,NDPResult[4]);
              end;
           end
         end;

         1:
         begin
         if ((BSolPos > 90)and(BSolPos <= 270)) then
           begin
           SetAxisPOsition(3,NDPResult[3]);
           SetAxisPOsition(4,NDPResult[4]);
           Success := ItterateForAB(NDPResult[3],NDPResult[4],PartVector,InverseVector,True,ElectrodeAngle);
           if Success then
              begin
              SetAxisPOsition(3,NDPResult[3]);
              SetAxisPOsition(4,NDPResult[4]);
              end;
           end
         end;

         2:
         begin
         if ((BSolPos >= 0)and(BSolPos <= 180)) then
           begin
           SetAxisPOsition(3,NDPResult[3]);
           SetAxisPOsition(4,NDPResult[4]);
           Success := ItterateForAB(NDPResult[3],NDPResult[4],PartVector,InverseVector,True,ElectrodeAngle);
           if Success then
              begin
              SetAxisPOsition(3,NDPResult[3]);
              SetAxisPOsition(4,NDPResult[4]);
              end;
           end
         end;

         3:
         begin
         if not ((BSolPos >= 0)and(BSolPos <= 180)) then
           begin
           SetAxisPOsition(3,NDPResult[3]);
           SetAxisPOsition(4,NDPResult[4]);
           Success := ItterateForAB(NDPResult[3],NDPResult[4],PartVector,InverseVector,True,ElectrodeAngle);
           if Success then
              begin
              SetAxisPOsition(3,NDPResult[3]);
              SetAxisPOsition(4,NDPResult[4]);
              end;
           end
         end;
       end; // case Solution
     end;


     6:// Stage 4
     begin
     UpdateFixedIndexes;
     Vpos := FReportVectorList[PartVectorIndex].Point1Position;
     ElectrodePos := FElectrodeLocation;
     DeltaPos := SubtractLocation(Vpos,ElectrodePos);
     NDPResult[0] := DeltaPos.x;
     NDPResult[1] := DeltaPos.y;
     NDPResult[2] := DeltaPos.z;
     UpdateReportData(True);
     end; //case 3

     7: // stage = 5
     begin
     RestoreAllAxisPOsitions(SavePosition);
     while NDPResult[5] >= 360 do  NDPResult[5] := NDPResult[5]-360;
     while NDPResult[4] >= 360 do  NDPResult[4] := NDPResult[4]-360;
     while NDPResult[3] >= 360 do  NDPResult[3] := NDPResult[3]-360;
     Result := CheckAxisLimits(NDPResult);
     ResLinear.X := LinearCondition(NDPResult[0]);
     ResLinear.Y := LinearCondition(NDPResult[1]);
     ResLinear.Z := LinearCondition(NDPResult[2]);
     ResRotary.X := RotaryCondition(0,NDPResult[3]);
     ResRotary.Y := RotaryCondition(1,NDPResult[4]);
     ResRotary.Z := RotaryCondition(2,NDPResult[5]);
     RunContinuous := False;
     end;

   end; //case stage of
   inc(Stage);
  end; // while  RunContinuous  do
end;

end.

