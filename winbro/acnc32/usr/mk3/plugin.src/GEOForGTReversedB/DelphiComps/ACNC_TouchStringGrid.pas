unit ACNC_TouchStringGrid;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Grids,stdCtrls,ACNC_TouchScroller;

type
  TSBarData = record
        Pos     : Integer;
        Max     : Integer;
        Min     : Integer;
        PageSize: Integer;
        DataChanged : Boolean;
        VisibilityChange : Boolean;
        RequiredVisible  : Boolean;
        end;

  TTouchStringGrid = class(TTouchScrollContainer)
  private
    { Private declarations }
    FDisplayCount : Integer;
    FHSHeight: Integer;
    FVSWidth: Integer;
    FSelectedString: String;
    FSelectionChange: TSelectCellEvent;
    FSelectedRow : Integer;
    FSelectedCol :Integer;
    FReadOnly: Boolean;
    FCellEdited : TSetEditEvent;
    FGridDblClick : TNotifyEvent;

    function SBVisibilityChanged: Boolean;
    procedure InternalResize;
    procedure ConditionScrollBars;
    procedure RedrawIfRequired(Sender: TObject);
    procedure OnGridChange (Sender: TObject; ACol, ARow: Longint; const Value: string);
    procedure OnGridDoubleClick(Sender: TObject);
    procedure UpdateHSB;
    procedure UpdateVSB;
    procedure SetHSHeight(const Value: Integer);
    procedure SetVSWidth(const Value: Integer);


    procedure SetDefaultRowHeight(const Value: Integer);
    function GetDefaultRowHeight: Integer;
    function GetDefaultColWidth: Integer;
    procedure SetDefaultColWidth(const Value: Integer);
    function GetColWidths(Index: Integer): Integer;
    procedure SetColWidths(Index: Integer; const Value: Integer);
    function GetRowHeights(Index: Integer): Integer;
    procedure SetRowHeights(Index: Integer; const Value: Integer);
    procedure SetRowCount(const Value: Integer);
    procedure SetColCount(const Value: Integer);
    function GetRowCount: Integer;
    function GetColCount: Integer;
    function GetFixedRows: Integer;
    procedure SetFixedRows(const Value: Integer);
    function GetFixedColumns: Integer;
    procedure SetFixedColumns(const Value: Integer);
    function GetFixedColor: TColor;
    procedure SetFixedColor(const Value: TColor);
    function GetColor: TColor;
    procedure SetColor(const Value: TColor);
    function GetVSColor: Tcolor;
    procedure SetVSColor(const Value: Tcolor);
    function GetHSColor: Tcolor;
    procedure SetHSColor(const Value: Tcolor);
    function GetCells(ACol, ARow: Integer): string;
    procedure SetCells(ACol, ARow: Integer; const Value: string);
    procedure SetReadOnly(const Value: Boolean);


  protected
  { Protected declarations }

    procedure Loaded; override;
    procedure HPosModified(var Position : Integer); override;
    procedure VPosModified(var Position : Integer); override;
    procedure UpdateCurrentCell(Sender: TObject; ACol, ARow: Integer;var CanSelect: Boolean);dynamic;
    function SearchColumnFor(ColNumber : Integer;SearchStr : String):Integer;

  { Public declarations }

  public
    HsbInfo   : TSBarData;
    VsbInfo   : TSBarData;
    Range     : Integer;
    PageSize  : Integer;
    Pos       : Integer;
    Max,Min   : Integer;
    Grid      : TStringGrid;

    // Pass through Grid properties that require scroll calculation
    procedure Resize;override;
    property ColWidths[Index: Longint]: Integer read GetColWidths write SetColWidths;
    property RowHeights[Index: Longint]: Integer read GetRowHeights write SetRowHeights;
    property Cells[ACol, ARow: Integer]: string read GetCells write SetCells;

  published
    { Published declarations }
    constructor Create(AOwner:TComponent); override;
    destructor Destroy; override;
    property DisplayCount : Integer read FDisplayCount;
    property OnSelectionChange : TSelectCellEvent read FSelectionChange write FSelectionChange;
    property HorizontalScrollHeight : Integer read FHSHeight write SetHSHeight;
    property VerticalScrollWidth : Integer read FVSWidth write SetVSWidth;
    Property SelectedString : String read FSelectedString;
    Property VerticalSliderColor :Tcolor read GetVSColor write SetVSColor;
    Property HorizontalSliderColor :Tcolor read GetHSColor write SetHSColor;
    Property SelectedRow : Integer read FSelectedRow;
    Property SelectedCol : Integer read FSelectedCol;
    property isReadOnly : Boolean read FReadOnly write SetReadOnly;
    property CellEdited : TSetEditEvent read FCellEdited write FCellEdited;
    property GridDoubleClick : TNotifyEvent read FGridDblClick write FGridDblClick;

    // Pass through Grid properties
    Property RowCount : Integer read GetRowCount write SetRowCount;
    Property ColCount : Integer read GetColCount write SetColCount;
    Property DefaultRowHeight : Integer read GetDefaultRowHeight write SetDefaultRowHeight;
    Property DefaultColWidth : Integer read GetDefaultColWidth write SetDefaultColWidth;
    property FixedRows : Integer read GetFixedRows write SetFixedRows;
    property FixedCols : Integer read GetFixedColumns write SetFixedColumns;
    property FixedColor : TColor read GetFixedColor write SetFixedColor;
    property Color : TColor read GetColor write SetColor;
  end;

procedure Register;

implementation

procedure Register;
begin
  RegisterComponents('ACNCComps', [TTouchStringGrid]);
end;

{ TTouchStringGrid }
constructor TTouchStringGrid.Create(AOwner: TComponent);
begin
  inherited;
  Width := 300;
  Height := 200;
  Grid := TStringGrid.Create(Self);
  with Grid do
        begin
        Parent:= Self;
        Top := 1;
        Left := 1;
        DefaultDrawing := True;
        ScrollBars := ssNone;
        OnDblClick := OnGridDoubleClick;
        OnSelectCell := UpdateCurrentCell;
        OnTopLeftChanged := RedrawIfRequired;
        OnSetEditText := OnGridChange;
        Name := 'TouchGrid';
        end;
SetReadOnly(True);

end;

destructor TTouchStringGrid.Destroy;
begin
  inherited;

end;

function TTouchStringGrid.SBVisibilityChanged:Boolean;
begin
//   Result := False;
   // first detrmine if scroll bars need to be visible
   VsbInfo.RequiredVisible :=  not(Grid.VisibleRowCount = Grid.RowCount-Grid.FixedRows);
   HsbInfo.RequiredVisible :=  not(Grid.VisibleColCount = Grid.ColCount-Grid.FixedCols);
   VsbInfo.VisibilityChange := VsbInfo.RequiredVisible <> VSB.Visible;
   HsbInfo.VisibilityChange := HsbInfo.RequiredVisible <> HSB.Visible;
   Result := VsbInfo.VisibilityChange or HsbInfo.VisibilityChange;
end;

procedure TTouchStringGrid.ConditionScrollBars;
var
VisChanged : Boolean;
begin
VisChanged := SBVisibilityChanged;
if VisChanged then
        begin
        SetVerticalScrollerVisible(VsbInfo.RequiredVisible);
        SetHorizontalScrollerVisible(HsbInfo.RequiredVisible);
        end;
if VsbInfo.RequiredVisible then
        begin
        UpdateVSB;
        end;
if HsbInfo.RequiredVisible then
        begin
        UpdateHSB;
        end;

end;
procedure TTouchStringGrid.UpdateVSB;
var
Changed     : Boolean;
begin
Changed := False;
if VsbInfo.Max <> Grid.RowCount-Grid.FixedRows then
        begin
        VsbInfo.Max := Grid.RowCount-Grid.FixedRows;
        Changed := True;
        end;
if VsbInfo.PageSize <> Grid.VisibleRowCount then
        begin
        VsbInfo.PageSize := Grid.VisibleRowCount;
        Changed := True;
        end;
if VsbInfo.Pos <> Grid.TopRow then
        begin
        VsbInfo.Pos := Grid.TopRow;
        Changed := True;
        end;
if Changed then
        begin
        with VsbInfo do
           begin
           Vsb.SetParameters(POs,PageSize,Max,Grid.FixedRows);
           end;
        InternalResize;
        end;
end;

procedure TTouchStringGrid.UpdateHSB;
var
Changed     : Boolean;
begin
Changed := False;
if HsbInfo.Max <> Grid.ColCount -Grid.FixedCols then
        begin
        HsbInfo.Max := Grid.ColCount-Grid.FixedCols;
        Changed := True;
        end;
if HsbInfo.PageSize <> Grid.VisibleColCount then
        begin
        HsbInfo.PageSize := Grid.VisibleColCount;
        Changed := True;
        end;
if HsbInfo.Pos <> Grid.LeftCol then
        begin
        HsbInfo.Pos := Grid.LeftCol;
        Changed := True;
        end;
if Changed then
        begin
        with HsbInfo do
           begin
           Hsb.SetParameters(POs,PageSize,Max,Grid.FixedCols);
           end;
        InternalResize;
        end;
end;


procedure TTouchStringGrid.RedrawIfRequired(Sender : TObject);
begin
ConditionScrollBars;
end;

procedure TTouchStringGrid.Resize;
var
temp : Boolean;
begin
  inherited;
  Grid.TopRow := Grid.FixedRows;
  Grid.LeftCol := Grid.FixedCols;
  Grid.Row := FixedRows;
  Grid.Col := FixedCols;
  ConditionScrollBars;
  InternalResize;
  UpdateCurrentCell(Self,Grid.Col,Grid.Row,temp);
//  inherited;
end;


// Note Internal Resize MUST NOT change the overall size
// otherwise recurrsion will happen as it is called from resize;
procedure TTouchStringGrid.InternalResize;
begin
  if HSB.Visible then
        begin
        Grid.Height := Height-HSB.Height-4;
        end
  else
        begin
        Grid.Height := Height-2;
        end;

  if VSB.Visible then
        begin
        Grid.Width := Width-VSB.Width-4;
        end
  else
        begin
        Grid.Width := Width-2;
        end;
end;

procedure TTouchStringGrid.UpdateCurrentCell(Sender: TObject; ACol, ARow: Longint; var CanSelect: Boolean);
begin
CanSelect := True;
FSelectedRow := ARow;
FSelectedCol := ACol;
FSelectedString := Grid.Cells[ACol,ARow];
ConditionScrollBars;
if assigned(FSelectionChange) then FSelectionChange(Self,ACol,ARow,CanSelect);
Invalidate;
end;


procedure TTouchStringGrid.Loaded;
var
SelRect : TGridRect;
begin
  inherited;
  Grid.TopRow := 1;
  Grid.LeftCol := 1;
  SelRect.Top := Grid.FixedCols;
  SelRect.Bottom := Grid.FixedCols;
  SelRect.Left := Grid.FixedRows;
  SelRect.Right := Grid.FixedRows;
  Grid.Selection :=SelRect;
  ConditionScrollBars;
  Resize;
end;


procedure TTouchStringGrid.HPosModified(var Position: Integer);
begin
  inherited;
  Grid.LeftCol := Position;
end;

procedure TTouchStringGrid.SetHSHeight(const Value: Integer);
begin
  FHSHeight := Value;
  HSB.Height := Value;
  Resize;
  InternalResize;
end;

procedure TTouchStringGrid.SetVSWidth(const Value: Integer);
begin
  FVSWidth := Value;
  VSB.Width := Value;
  Resize;
  InternalResize;
end;

// passthrough Grid property handlers
procedure TTouchStringGrid.SetRowCount(const Value: Integer);
begin
Grid.RowCount := Value;
Resize;
ConditionScrollBars;
end;

function TTouchStringGrid.GetRowCount: Integer;
begin
Result := Grid.RowCount;
end;

function TTouchStringGrid.GetColCount: Integer;
begin
Result := Grid.ColCount;
end;

procedure TTouchStringGrid.SetColCount(const Value: Integer);
begin
Grid.ColCount := Value;
Resize;
ConditionScrollBars;
end;


procedure TTouchStringGrid.VPosModified(var Position: Integer);
begin
  inherited;
  Grid.TopRow := Position;
end;

procedure TTouchStringGrid.SetDefaultRowHeight(const Value: Integer);
begin
  Grid.DefaultRowHeight := Value;
  ConditionScrollBars;

end;

function TTouchStringGrid.GetDefaultRowHeight: Integer;
begin
Result := Grid.DefaultRowHeight;
end;

function TTouchStringGrid.GetDefaultColWidth: Integer;
begin
Result := Grid.DefaultColWidth;
end;

procedure TTouchStringGrid.SetDefaultColWidth(const Value: Integer);
begin
  Grid.DefaultColWidth := Value;
  ConditionScrollBars;

end;

function TTouchStringGrid.GetColWidths(Index: Integer): Integer;
begin
  Result := Grid.ColWidths[Index];
end;

procedure TTouchStringGrid.SetColWidths(Index: Integer;
  const Value: Integer);
begin
  Grid.ColWidths[Index] := Value;
  ConditionScrollBars;
end;

function TTouchStringGrid.GetRowHeights(Index: Integer): Integer;
begin
Result := Grid.RowHeights[Index];
end;

procedure TTouchStringGrid.SetRowHeights(Index: Integer;
  const Value: Integer);
begin
Grid.RowHeights[Index] := Value;
ConditionScrollBars;
end;



function TTouchStringGrid.GetFixedRows: Integer;
begin
if assigned(Grid) then Result := Grid.FixedRows else Result := -1;
end;

procedure TTouchStringGrid.SetFixedRows(const Value: Integer);
begin
if assigned(Grid) then Grid.FixedRows := Value;
end;

function TTouchStringGrid.GetFixedColumns: Integer;
begin
if assigned(Grid) then Result := Grid.FixedCols else Result := -1;
end;

procedure TTouchStringGrid.SetFixedColumns(const Value: Integer);
begin
if assigned(Grid) then Grid.FixedCols := Value;
end;

function TTouchStringGrid.GetFixedColor: TColor;
begin
if assigned(Grid) then Result := Grid.FixedColor else Result := clwhite;
end;

procedure TTouchStringGrid.SetFixedColor(const Value: TColor);
begin
if assigned(Grid) then Grid.FixedColor := Value;
end;

function TTouchStringGrid.GetColor: TColor;
begin
if assigned(Grid) then Result := Grid.Color else Result := clwhite;
end;

procedure TTouchStringGrid.SetColor(const Value: TColor);
begin
if assigned(Grid) then Grid.Color := Value;
end;

function TTouchStringGrid.GetVSColor: Tcolor;
begin
if assigned(VSB) then Result := VSB.ButtonColor else Result := clwhite;
end;

procedure TTouchStringGrid.SetVSColor(const Value: Tcolor);
begin
if assigned(VSB) then VSB.ButtonColor := Value;
end;

function TTouchStringGrid.GetHSColor: Tcolor;
begin
if assigned(HSB) then Result := HSB.ButtonColor else Result := clwhite;
end;

procedure TTouchStringGrid.SetHSColor(const Value: Tcolor);
begin
if assigned(HSB) then HSB.ButtonColor := Value;
end;

function TTouchStringGrid.GetCells(ACol, ARow: Integer): string;
begin
if assigned(Grid) then
        begin
        REsult := Grid.Cells[Acol,ARow];
        end
end;

procedure TTouchStringGrid.SetCells(ACol, ARow: Integer;
  const Value: string);
begin
if assigned(Grid) then
        begin
        Grid.Cells[Acol,ARow] := Value;
        end

end;

function TTouchStringGrid.SearchColumnFor(ColNumber: Integer;
  SearchStr: String): Integer;
begin
Result := -1;
if (ColNumber < 0) or (ColNumber > Grid.ColCount-1) then exit;
{ TODO : this to be usd in incremental search from index tabs }
end;

procedure TTouchStringGrid.SetReadOnly(const Value: Boolean);
var
CurrentOPtion : TGridOptions;
begin
  FReadOnly := Value;
  CurrentOPtion := Grid.OPtions;
  if FReadOnly then
    begin
    exclude(CurrentOption,goEditing);
    Grid.OPtions := CurrentOption;
    end
  else
    begin
    include(CurrentOption,goEditing);
    Grid.OPtions := CurrentOption;
    end
end;

procedure TTouchStringGrid.OnGridChange(Sender: TObject; ACol,
  ARow: Integer; const Value: string);
begin
if assigned(FCellEdited) then FCellEdited(Sender,ACol,ARow,Value);
end;

procedure TTouchStringGrid.OnGridDoubleClick(Sender: TObject);
begin
if assigned(FGridDblClick) then FGridDblClick(Sender);
end;

end.
