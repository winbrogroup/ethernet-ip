unit SizeKB;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, BaseKeyBoards,stdctrls;

type
  TSizeKB = class(TBaseKB)
  private
    { Private declarations }
    FWidth : Integer;
    FHeight: Integer;
  protected
    { Protected declarations }
    procedure SetKBHeight(Value : Integer);
    procedure SetKBWidth(Value : Integer);


  public
    { Public declarations }
  published
    { Published declarations }
    constructor Create(AOwner: TComponent); override;


    property KBWidth : Integer read FWidth write SetKBWidth;
    property KBHeight : Integer read FHeight write SetKBHeight;

  end;

procedure Register;

implementation
constructor TSizeKB.Create(AOwner: TComponent);
begin
inherited;
FWidth := Width;
FHeight := Height;
end;

procedure TSizeKB.SetKBHeight(Value : Integer);
begin
FHeight := Value;
Height := Value;
Resize;
end;

procedure TSizeKB.SetKBWidth(Value : Integer);
begin
FWidth := Value;
Width := Value;
Resize;
end;




procedure Register;
begin
  RegisterComponents('Touch', [TSizeKB]);
end;

end.
