{***************************************************************************************************
  PlusGutter 4.1c, Jan 10, 2000
  Initial release (v4.0) Oct. 10, 1998
  ==============
  � Electro-Concept Mauricie, 1998-1999
  You have the right to use and modify this source code, but you may not sell it...

  TPlusGutter is a panel that can be placed next to a TPlusMemo component and be linked to it.
  It then automatically shows line numbers at the appropriate vertical position.

  It also allows to bookmark lines using Shift+Ctrl+0..9, and to jump from any point to one of
  the bookmarks using Ctrl+0..9. It also shows (optionally) line numbers or paragraph numbers.

  Great thanks to Thorsten Dittmar, who provided most of the bookmarking functionality. }


{ Programming interface:

    property PlusMemo: TPlusMemo;  The instance of TPlusMemo that the TPlusGutter is attached to

    property Alignment: TAlignment;  can be either taLeftJustify, taCenter or taRightJustify.
                                     this property is used to specify the horizontal position
                                     of line numbers and bookmark indicators inside the control's
                                     border

    property Bookmarks: Boolean;    This property controls whether the bookmark feature is enabled.
                                    When True, Ctrl-Shift-0..9 will toggle a bookmark on the current
                                    line.  Shift-0..9 permits reaching a previously defined bookmark.
                                    When False, these keys have no effect

    property LineNumbers: Boolean;  This property controls whether the TPlusGutter displays numbers
                                    (either line numbers or paragraph numbers, depending on property below)

    property ParagraphNumbers: Boolean; To specify if you want line numbers (when set to False) or
                                        paragraph numbers (when set to True).  Property LineNumbers must
                                        be set to True to display any kind of number.

    function BookmarkLine(ALine: Longint; BookmarkIndex: TBookmarkRange): Boolean;
         This function toggles the bookmark at line ALine.  If this line is already under BookmarkIndex,
         it will be unbookmarked and the result returns False.  If not, ALine will be bookmarked and the result
         is True

    procedure ClearBookmarks;  This procedure clears all bookmarks currently defined.

    procedure JumpToBookmark(BookmarkIndex: TBookmarkRange);  self explanatory.

    property BookmarksArray[BookmarkIndex: TBookmarkRange]: Longint;
         A read only property that returns the line number for a given bookmark index.
         If said bookmark is not defined, this property returns -1. }




unit ACNC_PlusGutter;
{$R PLUSGUTTER.RES}

interface

{$IFDEF VER120} {$DEFINE D4NEW} {$ENDIF}
{$IFDEF VER125} {$DEFINE D4NEW} {$ENDIF}
{$IFDEF VER130} {$DEFINE D4NEW} {$ENDIF}

uses
  Messages, Classes, Controls, ACNC_PlusMemo, ImgList;

type
  TBookmarkRange = 0..9;

  TPlusGutter = class(TGraphicControl)
  private
    { fields corresponding to public or published properties }
    FAlignment      : TAlignment;
    FPlusMemo       : TPlusMemo;
    FLineNumbers,
    FBookmarks      : Boolean;
    FParNumbers     : Boolean;

    procedure SetAlignment(al: TAlignment);
    procedure SetLineNumbers(ln: Boolean);
    procedure SetPlusMemo(Value: TPlusMemo);
    procedure setParNumbers(const Value: Boolean);
    function getBookmarks(BookmarkIndex: TBookmarkRange): Longint;

  protected
    { these fields put in protected part to support for descendant classes that
      provide their own painting code or other effects }
    { last saved values from PlusMemo we are attached }
    FTopY           : Longint;
    FLineCount      : Longint;
    FParCount       : Longint;
    FBookmarkValues : Array[TBookmarkRange] of Longint;

    { internal working fields }
    FBookmarkIcons  : TImageList;
    FBookmarkList   : Array[TBookmarkRange] of TPlusNavigator;

    procedure Paint; override;
    procedure Notification(AComponent: TComponent; Operation: TOperation); override;
    procedure MemoWndProc(var Msg: TMessage);
    function  GetBookmarkFromLine(ALine: Longint): SmallInt;
    procedure Scroll(Sender: TObject);

  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    function    BookmarkLine(ALine: Longint; BookmarkIndex: TBookmarkRange): Boolean; virtual;
    procedure   ClearBookmarks;
    procedure   JumpToBookmark(BookmarkIndex: TBookmarkRange);

    property  BookmarksArray[BookmarkIndex: TBookmarkRange]: Longint read getBookmarks;


  published
    property  PlusMemo: TPlusMemo read FPlusMemo write SetPlusMemo;
    property  Alignment: TAlignment read fAlignment write SetAlignment default taCenter;
    property  Align;
    property  Bookmarks: Boolean read FBookmarks write FBookmarks default True;
    property  Color;
    property  DragCursor;
    property  DragMode;
    property  Font;
    property  LineNumbers: Boolean read fLineNumbers write SetLineNumbers default True;
    property  ParagraphNumbers: Boolean read fParNumbers write setParNumbers default False;
    property  ParentColor;
    property  ParentShowHint;
    property  PopupMenu;
    property  ShowHint;
    property  Visible;
    property  OnClick;
    property  OnDblClick;
    property  OnDragDrop;
    property  OnDragOver;
    property  OnEndDrag;
    property  OnMouseDown;
    property  OnMouseMove;
    property  OnMouseUp;
    {$IFDEF D4NEW}  { Delphi 4 new properties }
    property Anchors;
    property BiDiMode;
    property Constraints;
    property DragKind;
    property ParentBiDiMode;
    property OnEndDock;
    property OnStartDock;
    {$ENDIF}
  end;

procedure Register;

implementation

{$IFDEF VER120} {$DEFINE IMGLIST} {$ENDIF}
{$IFDEF VER125} {$DEFINE IMGLIST} {$ENDIF}
{$IFDEF VER130} {$DEFINE IMGLIST}  {$ENDIF}


uses Windows, SysUtils, Graphics, Forms {$IFDEF IMGLIST}, ImgList {$ENDIF};

procedure Register;
  begin
  RegisterComponents('ACNCComps', [TPlusGutter]);
  end;

const
  HotKeys   : set of Char = ['0'..'9'];
  ICNHEIGHT = 10;
  ICNWIDTH  = 10;


constructor TPlusGutter.Create(AOwner: TComponent);
  begin
  inherited Create(AOwner);

  ControlStyle:= ControlStyle + [csOpaque];

  { Initialize inherited properties }
  Width := 30;
  Height:= 50;

  { Initialize new properties }
  FLineNumbers:= True;
  FAlignment  := taCenter;
  FBookmarks  := True;

  { Initialize the list of bitmaps }
  FBookmarkIcons := TImageList.CreateSize(IcnWidth, IcnHeight);
  FBookmarkIcons.DrawingStyle := dsTransparent;

  {$IFDEF VER90}
    FBookmarkIcons.ResourceLoad(rtBitmap,'PG_BOOKMARKICONS1',clWhite);
  {$ELSE}
    FBookmarkIcons.ResInstLoad(HInstance, rtBitmap,'PG_BOOKMARKICONS1',clWhite);
  {$ENDIF}
  end;

destructor TPlusGutter.Destroy;
  var i: Integer;
  begin
  if FPlusMemo <> nil then FPlusMemo.MsgList.Remove(Self);
  FBookmarkIcons.Free;
  for i:= 0 to 9 do FBookmarkList[i].Free;
  inherited Destroy;
  end;


procedure TPlusGutter.setAlignment(al: TAlignment);
  begin
  if al<>fAlignment then
    begin
    fAlignment:= al;
    Invalidate
    end
  end;

procedure TPlusGutter.SetLineNumbers(ln: Boolean);
  begin
  if ln<>fLineNumbers then
    begin
    fLineNumbers:= ln;
    Invalidate
    end
  end;

procedure TPlusGutter.Scroll(Sender: TObject);
  var currenttopy : Longint;
      r           : TRect;
  begin
  if (Parent<>nil) and Parent.HandleAllocated then
    begin
    r:= BoundsRect;
    if (FPlusMemo.LineCount<>FLineCount) or (FPlusMemo.ParagraphCount<>FParCount) then Invalidate
    else
      begin
        currenttopy:= FPlusMemo.TopOrigin;
        ScrollWindow(Parent.Handle, 0, FTopY-currenttopy, @r, @r);
        FTopY  := currenttopy
      end
    end
  end;


procedure TPlusGutter.Paint;
var clRect     : TRect;      { local value of clip rectangle }
    DrawY      : Integer;    { Y value where we are drawing }
    DrawLine   : Longint;    { line index being drawn }
    DrawIndex  : Integer;    { bookmark index of current line being drawn }
    LHeight    : Integer;    { local value of line height }
    i          : Integer;
    DigitWidth : Integer;    { width of a digit, used only if LineNumbers=True }
    CWidth     : Integer;    { local value of ClientWidth, minus border }
    t          : string;     { temporary string used if LineNumbers = True }
    LeftText   : Integer;    { left coordinate of text or bookmark icon }
    po         : TPoint;
    mt         : Integer;
    IconYAdd   : Integer;    { used in centering the bookmark icon vertically }
    tracknav   : TPlusNavigator;  { used for painting paragraph numbers }

begin
CWidth:= ClientWidth - 2;

with Canvas do
  begin
  clRect := ClipRect;

  { Fill with color }
  Brush.Color := Color;
  FillRect(clRect);
  Brush.Style:= bsClear;

  { Draw the right border, which is 2 pixels wide }
  if clRect.Right > CWidth then
    begin
    Pen.Color := clBtnHighlight;
    MoveTo(CWidth, clRect.Top);
    LineTo(CWidth, clRect.Bottom);

    Pen.Color := clBtnShadow;
    MoveTo(CWidth+1, clRect.Top);
    LineTo(CWidth+1, clRect.Bottom);
    end;
  end;

  { Draw the bookmarks and line numbers }
  if (FPlusMemo <> nil) and (FPlusMemo.HandleAllocated) then
    begin
    FTopY   := FPlusMemo.TopOrigin;
    with FPlusMemo do
      begin
      if (Self.FLineCount<>LineCount) or (FParCount<>ParagraphCount) then
        begin
        Self.FLineCount:= LineCount;
        FParCount:= ParagraphCount;
        Self.Invalidate;
        Exit
        end;

      LHeight:= FLineHeight;
      { N.B: at design time, LineHeight may returns 0.  That's why we use fLineHeight }
      end;

    for i:= 0 to High(TBookmarkRange) do
        if FBookmarkList[i]<>nil then FBookmarkValues[i]:= FBookmarkList[i].LineNumber
                                 else FBookmarkValues[i]:= -1;

  po:= ClientOrigin;
  mt:= FPlusMemo.ClientToScreen(Point(0,0)).Y;
  if mt<po.y+clRect.Top then DrawLine:= (po.Y+clRect.Top-mt+FTopY) div lheight
                        else DrawLine:= fPlusMemo.FirstVisibleLine;

  if LineNumbers and ParagraphNumbers then
    begin
    tracknav:= TPlusNavigator.Create(fPlusMemo);
    tracknav.LineNumber:= DrawLine
    end
  else
    tracknav:= nil;

  DigitWidth:= 0;
  if LineNumbers then
    begin
    Canvas.Font:= Self.Font;
    SetTextAlign(Canvas.Handle, ta_baseline);
    if Alignment<>taLeftJustify then digitwidth:= Canvas.TextWidth('0')
                                else digitwidth:= 0;
    end;

  DrawY    := DrawLine*LHeight - FTopY + mt - po.Y;
  LeftText := 0;

  IconYAdd := (LHeight - ICNHeight) div 2;
  if IconYAdd+ICNHeight<fPlusMemo.fLineBase then
                         IconYAdd:= fPlusMemo.fLineBase-ICNHeight;

  while (DrawY<clRect.Bottom) and (DrawLine<FLineCount) do
    begin
    DrawIndex:= GetBookmarkFromLine(DrawLine);
    if DrawIndex>=0 then
        begin
        case Alignment of
          taLeftJustify: lefttext:= 2;
          taRightJustify: lefttext:= CWidth - IcnWidth;
          taCenter      : lefttext:= (CWidth - IcnWidth) div 2
          end;
        FBookmarkIcons.Draw(Canvas, lefttext, DrawY + iconyadd, DrawIndex)
        end

    else
      if LineNumbers then
          begin
          if ParagraphNumbers then
            if tracknav.ParLine=0 then t:= IntToStr(tracknav.ParNumber+1)
                                  else t:= ''
          else
            t:= IntToStr(DrawLine+1);
          case Alignment of
            taLeftJustify:  lefttext:= 0;
            taRightJustify: lefttext:= CWidth - Length(t)*DigitWidth;
            taCenter:       lefttext:= (CWidth - Length(t)*DigitWidth) div 2
            end;
          Canvas.TextOut(lefttext, DrawY + fPlusMemo.fLineBase, t)
          end;

    Inc(DrawLine);
    Inc(DrawY, LHeight);
    if LineNumbers and ParagraphNumbers then tracknav.LineNumber:= DrawLine
    end;  { while DrawLine<FLineCount }

  tracknav.Free
  end;   { FPlusMemo <> nil }
end;  { TPlusGutter.Paint }

procedure TPlusGutter.Notification(AComponent: TComponent; Operation: TOperation);
  var i: Integer;
  begin
  if Integer(Operation)=pm_Msg then MemoWndProc(fPlusMemo.NotifyMsg)
  else
    begin
    inherited Notification(AComponent, Operation);
    if (Operation = opRemove) and (Assigned(FPlusMemo)) and (AComponent = FPlusMemo) then
      begin
      for i:= 0 to 9 do FBookmarkList[i] := nil;
      FPlusMemo := nil;
      end
    end;
  end;

procedure TPlusGutter.MemoWndProc(var Msg: TMessage);

  function SameBookmarks: Boolean;
    var i: Integer;
    begin
    Result:= True;
    for i:= 0 to High(TBookmarkRange) do
      if (FBookmarkList[i]<>nil) and (FBookmarkValues[i]<>FBookmarkList[i].LineNumber) then
        begin
        Result:= False;
        Exit
        end
    end;

  var
    ss: TShiftState;
    ch: Char;
    ln: Integer;
  begin
  case Msg.Msg of
    WM_KEYDOWN: { check for trying to reach or set a bookmark }
      with Msg do
        begin
        ss := KeyDataToShiftState(LParam);
        ch := Chr(WParam);
        ln := WParam - Ord('0');

        if (ch in HotKeys) and (ssShift in ss) and (ssCtrl in ss) and not (ssAlt in ss) then
          begin
          BookmarkLine(FPlusMemo.SelLine, ln);
          Invalidate;
          end
        else
          if (ch in HotKeys) and (ssCtrl in ss) and not (ssShift in ss) and not (ssAlt in ss) and (FBookmarkList[ln] <> nil) then
             begin
             FPlusMemo.SelLine := FBookmarkList[ln].LineNumber;
             FPlusMemo.ScrollInView;
             end;
        end;

     CM_FONTCHANGED: Invalidate;  { to replace lineheight and other internal values }

     CM_TEXTCHANGED: begin     { OnChange event handler: repaint the line numbers if the
                                 number of lines has changed, or some bookmark position has changed }
                     if (fPlusMemo.LineCount<>FLineCount) or (not SameBookmarks) or (fPlusMemo.ParagraphCount<>fParCount) then Invalidate;
                     FLineCount:= fPlusMemo.LineCount;
                     FParCount:= FPlusMemo.ParagraphCount
                     end;

     pm_VScroll: Scroll(Self)

     end;   { case Msg }

  end;   { MemoWndProc }



function TPlusGutter.BookmarkLine(ALine: Longint; BookmarkIndex: TBookmarkRange): Boolean;
  var CurrentBIndex: Integer;
  begin
  { Return False if the line is already bookmarked }
  Result := False;

  CurrentBIndex:= GetBookmarkFromLine(ALine);

  { Remove the bookmark, if the same number should be assigned twice }
  if CurrentBIndex = BookmarkIndex then
    begin
    FBookmarkList[BookmarkIndex].Free;
    FBookmarkList[BookmarkIndex]:= nil;
    Result := True;
    Invalidate;
    Exit;
    end;

  { Add the bookmark, if the line was not previously bookmarked }
  if CurrentBIndex<0 then
    begin
    FBookmarkList[BookmarkIndex] := TPlusNavigator.Create(FPlusMemo);
    FBookmarkList[BookmarkIndex].LineNumber:= ALine;
    Result := True;
    Invalidate;
    end;
  end;


function TPlusGutter.GetBookmarkFromLine(ALine: Longint): SmallInt;
var
  i: Integer;
begin
  { Iterate through bookmarks and return the bookmark number for ALine or -1 }
  Result := -1;
  for i := 0 to 9 do
    if (FBookmarkList[i] <> nil) and (FBookmarkList[i].LineNumber = ALine) then
      begin
      Result := i;
      Break;
      end;
end;


procedure TPlusGutter.SetPlusMemo(Value: TPlusMemo);
  var i: Integer;
  begin
  { Clear the old bookmarks }
  if FPlusMemo <> Value then
    for i := 0 to 9 do
      begin
      FBookmarkList[i].Free;
      FBookmarkList[i]:= nil;
      end;

  { Restore the original window procedure, replace event handlers }
  if FPlusMemo <> nil then FPlusMemo.MsgList.Remove(Self);

  { Set to the new memo, and the new memos window procedure }
  FPlusMemo := Value;
  if FPlusMemo <> nil then fPlusMemo.MsgList.Add(Self);
  Invalidate;
  end;

procedure TPlusGutter.setParNumbers(const Value: Boolean);
  begin
  if Value<>fParNumbers then
    begin
    fParNumbers := Value;
    Invalidate
    end
  end;

function TPlusGutter.getBookmarks(BookmarkIndex: TBookmarkRange): Longint;
  begin
  Result:= -1;
  if (BookmarkIndex<=High(TBookmarkRange)) and Assigned(fBookmarkList[BookmarkIndex]) then
    Result:= fBookmarkList[BookmarkIndex].LineNumber
  end;

procedure TPlusGutter.ClearBookmarks;
  var i: Integer;
  begin
  for i:= 0 to High(TBookmarkRange) do
    if BookmarksArray[i]>=0 then
      BookmarkLine(BookmarksArray[i], i)
  end;

procedure TPlusGutter.JumpToBookmark(BookmarkIndex: TBookmarkRange);
begin
  if Assigned(FPlusMemo) and (BookmarksArray[BookmarkIndex]>=0) then
    begin
    FPlusMemo.SelStart:= fBookmarkList[BookmarkIndex].Pos;
    FPlusMemo.ScrollInView
    end
end;


end.
