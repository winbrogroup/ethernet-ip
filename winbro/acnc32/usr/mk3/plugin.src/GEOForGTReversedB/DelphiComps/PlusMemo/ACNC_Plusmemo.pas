unit ACNC_Plusmemo;
{ PlusMemo version 5.3
{ � Electro-Concept Mauricie, 1997-2001 }

{ This is the main source file for the TPlusMemo component.  Other complement source files are
      plussup.pas  =>  contains support objects and routines
      plusnav.pas  =>  source code for TPlusNavigator object
      pmemoreg.pas =>  used only for design time installation

      Under Delphi1, plussup compiles to a separate unit (plussup.dcu), which includes plusnav;
      Under Delphi2 and up, they all compile to a single unit, plusmemo.dcu }

{ Conditional defines used by these source code files:
  INTERFACE  => used to include only the interface parts of complement files under D2 and up
                defined and undefined by this main file.
  CBuilder   => defined under CBuilder environment
  D4New      => defined under D4, D5, D6, CB4, CB5
  D5New      => defined under D5, D6
  PMDEBUG      => used to include debug code.  It is not recommended to have this defined in your environment }

{$IFDEF VER120} {$DEFINE D4New} {$ENDIF}
{$IFDEF VER125} {$DEFINE D4New} {$ENDIF}
{$IFDEF VER130} {$DEFINE D4New} {$ENDIF}
{$IFDEF VER140} {$DEFINE D4New} {$ENDIF}

{$IFDEF VER130} {$DEFINE D5New} {$ENDIF}
{$IFDEF VER140} {$DEFINE D5New} {$ENDIF}

{$A+}  { this unit requires word alignment of data }
{$B-}  { not complete boolean evaluation }
{$T-}  { not typed address operator }

{$IFDEF WIN32}
  {$H+}  { long strings }
  {$J+}  { writeable typed constants }
{$ENDIF}


{$IFNDEF PMDEBUG}
  {$R-}
  {$Q-}
{$ENDIF}

{$IFNDEF WIN32}
    {$S-}   { Stack checking is a luxury not possible under D1, otherwise "CodeSegment too large" }
{$ENDIF}


{$IFDEF BCB}  {$ObjExportAll On}  {$ENDIF}

interface
uses Graphics, Controls, Classes, WinTypes, Forms, Messages, Menus, StdCtrls, Dialogs
     {$IFNDEF WIN32}, PlusSup {$ENDIF};

  {$IFDEF WIN32}
  {$DEFINE INTERFACE}
  {$I PLUSSUP.INC}
  {$UNDEF INTERFACE}
  {$ENDIF}

const

  pm_UpdateBkg = WM_User+1;
  pm_VScroll   = WM_User+2;
  pm_Context   = WM_User+3;
  pm_SelMove   = WM_User+4;
  pm_RightContext = WM_User+5;
  pm_Msg       = 64;
  {$IFDEF WIN32}
    {$IFDEF VER90} WM_MOUSEWHEEL       = $020A;  { not defined in D2, so define it }
    {$ENDIF}
  {$ENDIF}

type
  pWndMethod = ^TWndMethod;

  TUpdateMode = (umImmediate, umOnNeed, umBackground);

  TPlusMemoOption = (pmoKeepColumnPos, pmoPutExtraSpaces, pmoWrapCaret,
                     pmoInsertKeyActive, pmoWideOverwriteCaret,
                     pmoLargeWordSelect, pmoAutoScrollBars, pmoNoDragnDrop,
                     pmoAutoIndent, pmoBackIndent,
                     pmoWindowsSelColors, pmoFullLineSelect,
                     pmoDiscardTrailingSpaces, pmoNoFineScroll,
                     pmoNoLineSelection,
                     pmoBlockSelection);

  TPlusMemoOptions = set of TPlusMemoOption;

  TPlusLineBreak = (pbCRLF, pbLFCR, pbCR, pbLF);

  TSetOfChar  = set of Char;

  TUndoRecord = record UndoStart, UndoStop: Longint;
                       UndoText           : PChar
                       end;


  TParseEvent = procedure (Sender: TObject; StartOffset, StopOffset: Longint) of object;

  TContextEvent = procedure(Sender: TObject; Context: Integer;
                            StartOffset, StopOffset: Longint) of object;

  TpmBeforeChangeEvent = procedure(Sender: TObject; var Txt: PChar) of object;

  TPlusMemo = class(TCustomControl)
    protected
    { fields corresponding to public or published properties }
    fTextLen, fLineCount       : Longint;
    fParagraphs                : TParagraphsList;
    fPars, fLines              : TStrings;
    fSelLen                    : Longint;
    fcp, fDisplayTop           : TPlusNavigator;
    fLeftMargin, fRightMargin  : Integer;
    fDisplayLeft               : Integer;
    fTabStops                  : Integer;
    fAutoLineHeight            : Boolean;
    fModified, fReadOnly       : Boolean;
    fWantTabs, fWordWrap       : Boolean;
    fHScrollBar, fVScrollBar   : Boolean;
    fHideSelection             : Boolean;
    fShowEndParSelected        : Boolean;
    fEnableHotKeys             : Boolean;
    fPassOver, fDisplayOnly    : Boolean;
    fAlignment                 : TAlignment;
    fJustified                 : Boolean;
    fAltFont                   : TFont;
    fHTColor, fHBColor         : TColor;
    fBorderStyle               : TBorderStyle;
    fCaretWidth, fCompleted    : Integer;
    fKeywords                  : TKeywordList;
    fStartStopKeys             : TStartStopKeyList;
    fApplyKeyWords,
    fApplyStartStopKeys        : Boolean;
    fUpdateMode                : TUpdateMode;
    fStripStrayCtrlCodes       : Boolean;
    fDelimiters                : TSetOfChar;
    fEndOfTextPen              : TPen;
    fSpecUnderlinePen          : TPen;
    fOverWrite                 : Boolean;
    fOptions                   : TPlusMemoOptions;
    fUndoLevel, fUndoMaxLevel  : Integer;
    fUndoList                  : TList;
    fUndoMaxSpace              : Longint;
    fNull                      : Char;
    fHighlighter               : TPlusHighlighter;
    fRTFLeading                : Integer;
    fLineBreak                 : TPlusLineBreak;
    fTopOrigin                 : Longint;
    fStaticFormat              : Boolean;
    fCharSet                   : Integer;     { used only under D1 and D2 }
    fMouseWheelFact            : Integer;
    fBackground                : TPicture;
    fSelBackColor, fSelTextColor: TColor;
    fUpperCaseType             : TpmUpperCase;

    fOnChange, fOnAttrChange,
    fOnMove,   fOnProgress,
    fOnHScroll, fOnVScroll     : TNotifyEvent;
    fOnParse                   : TParseEvent;
    fOnContext                 : TContextEvent;
    fOnBeforeChange            : TpmBeforeChangeEvent;
    fOnAfterMouseDown          : TMouseEvent;

    fOnCtrlF                   : TNotifyEvent;
    fOnF3                      : TNotifyEvent;
    fOnF1                      : TNotifyEvent;
    fOnESCape                  : TNotifyEvent;
    fOnWordwrapChange          : TNotifyEvent;

    { internal working fields, see TPlusMemo Internals for more info }
    fDisplayLines                : Longint;     { number of lines across the height of the client window }
    fWideCaret, fLastPosWidth    : Integer;     { related to wide caret in overwrite mode }
    fNavigators                  : TList;       { list of TPlusNavigator's attached to me }
    fMaxLineWidth, fMaxLineNumber: Longint;     { largest line }
    fSelStart, fSelStop          : TPlusNavigator;   { navigators that delimit the selection range }
    ftmpnav1, ftmpnav2           : TPlusNavigator;   { temporary usage }
    fkeynav1, fkeynav2           : TPlusNavigator;
    fssnav1, fssnav2             : TPlusNavigator;
    fformnav1, fformnav2         : TPlusNavigator;
    fpnav1, fpnav2               : TPlusNavigator;
    fMouseNav                    : TPlusNavigator;   { a navigator that follows the mouse }

    fMouseDownPos                : Longint;      { fields related to mouse tracking }
    fStartLineSelection          : Integer;      { negative value if not in line selection mode }
    fBlockSelection              : Boolean;      { true if a block is selected or is being selected }
    fBlockStartCol,
    fBlockStopCol                : Integer;      { column numbers of selected block }
    fMouseWheelAcc               : Integer;
    fDragging, fDraggingOutside  : Boolean;
    fMouseDown, fDblClick,
    fMouseInContext, fMouseInSel : Boolean;
    fMouseScroll                 : TMouseScrollType;
    fScrollRate                  : Integer;
    fCursor                      : TCursor;

    fsStyle                      : TExtFontStyles;  { saved style at caret pos }
    fsPos                        : Longint;   { saved caret position, updated in UpdateCaret }
    fCaretX, fCaretY             : Integer;   { caret location on the display }
    fNoCheckFormat               : Boolean;   { flag for SetSelTextBuf }
    fNoFormat                    : Boolean;   { flag for CreateHandle }
    fInSetScroll                 : Boolean;   { flag for SetBounds }
    fNoCompleteFormat            : Boolean;
    fSpaceWidth, fMaxOneShotChars: Integer;
    fVScrollfact, fHScrollFact   : Integer;
    fAutoCaretWidth              : Boolean;
    fLineBmp                     : HBitmap;
    fLineBmpWidth                : Integer;
    fReceivedKeyUp, fNoPaint     : Boolean;
    fInStripCodes                : Boolean;
    fCanvas           : TCanvas;        { if Paint and Reformat should use a different DC }
    fW                : Integer;        { Client width of this DC }
    fLastProgress     : Longint;

    fRunningSpaceWidth, fSpaceKern : Integer;
    fExtraCols                     : PlusCardinal;
    fXCaretRunningPos              : Integer;
    fUndoTotalSize                 : Longint;
    fUndoSkip, fNoClearRedo        : Boolean;
    fInUndo, fUndoBreak            : Boolean;
    fSavedDragCursor               : THandle;
    fInternalPopup                 : TPopupMenu;

    { Mod and Update fields }
    fModStartLine,
    fModStartPar,
    fModStopPar,
    fModLinesOffset,
    fLastStartStopParsed,
    fUpdateStartPar,
    fUpdateStopPar       : Longint;
    fUpdating            : Boolean;

    fLockedCount         : Integer;
    fOldFinalDyn         : DynInfoRec;

    fVScrollChange       : Boolean;     { whether the last call to SetVScrollParams did a reformat }

    { Cached Uppercase text }
    fUpText  : PChar;
    fUpParNb : Longint;
    fUpOffset, fUpLength: PlusCardinal;

    { Accessories support }
    fMsgList : TList;

    { message handlers }
    procedure WMCut(var Message: TMessage); message WM_CUT;
    procedure WMCopy(var Message: TMessage); message WM_COPY;
    procedure WMPaste(var Message: TMessage); message WM_PASTE;
    procedure WMUndo(var Message: TMessage); message WM_UNDO;
    procedure WMGetDlgCode(var Message: TWMGetDlgCode); message WM_GETDLGCODE;
    procedure WMSetFocus(var Message: TMessage); message WM_SETFOCUS;
    procedure WMKillFocus(var Message: TMessage); message WM_KILLFOCUS;
    procedure CMFontChanged(var Message: TMessage); message CM_FONTCHANGED;
    procedure CMCtl3Dchanged(var Message: TMessage); message CM_CTL3DCHANGED;
    procedure WMEraseBkgnd(var Message: TMessage); message WM_ERASEBKGND;
    procedure WMPaint(var  Message: TMessage); message WM_PAINT;
    procedure WMVSCROLL(var m: TWMScroll ); message WM_VSCROLL;
    procedure WMHSCROLL(var m: TWMScroll ); message WM_HSCROLL;
    procedure WMSetCursor(var Message: TWMSetCursor); message WM_SETCURSOR;
    {$IFDEF WIN32}
    procedure WMMouseWheel(var m: TMessage); message WM_MOUSEWHEEL;
    {$ENDIF}
    procedure PMUpdateBkg(var m: TMessage); message PM_UpdateBkg;

    { private methods }
    procedure SetupLogFontStyle(var lf: TLogFont; style: TExtFontStyles);
    procedure setHScrollParams;
    procedure setVScrollParams;
    procedure BackgroundChange(Sender: TObject);

    { property access methods }
    function  GetParCount: Longint;
    function  GetPar(Pos: Longint): Longint;
    procedure setSelLength(l: Longint);
    function  getSelText: string;
    procedure setSelText(const s: string);
    procedure SetSelAttrib(a: Char);
    function  getCurrentStyle: TExtFontStyles;
    function  getChar(i: Longint): Char;
    function  getLines(i: Longint): string;
    procedure setLines(Value: TStrings);
    function  getParagraphs(i: Longint): string;
    function  getLinesBuf(i: Longint): PChar;
    function  getParsBuf(i: Longint): PChar;
    procedure setParsBuf(i: Longint; parg: PChar);
    function  getParsOffset(i: Longint): Longint;
    procedure setLeftMargin(lm: Integer);
    procedure setRightMargin(rm: Integer);
    procedure setBorderStyle(b: TBorderStyle);
    procedure setTabStops(t: Integer);
    procedure setWordWrap(ww: Boolean);
    procedure setHTColor(c: TColor);
    procedure setHBColor(c: TColor);
    procedure setHideSelection(h: Boolean);
    procedure setfAltFont(f: TFont);
    function  getCanRedo: Boolean;
    function  getCanUndo: Boolean;
    procedure setSelPar(par: Longint);
    function  getSelPar: Longint;
    procedure setSelStart(ss: Longint);
    procedure setSelLine(line: Longint);
    function  getSelLine: Longint;
    procedure setSelCol(col: PlusCardinal);
    function  getSelCol: PlusCardinal;
    function  getSelStart: Longint;
    function  getUndoCount: Integer;
    procedure setParagraphs(i: Longint; const par: string);
    procedure setScrollBars(s: TScrollStyle);
    function  getVersion: string;
    procedure setVersion(const v: string);
    procedure setCaretWidth(w: Integer);
    procedure setAlignment(al: TAlignment);
    procedure setJustified(j: Boolean);
    function  getTopLine: Longint;
    procedure setTopLine(tl: Longint);
    procedure setDisplayLeft(dl: Integer);
    function  getScrollBars: TScrollStyle;
    procedure setDisplayOnly(d: Boolean);
    procedure setLineHeight(lh: Integer);
    function  getLineHeight: Integer;
    procedure setUpdateMode(um: TUpdateMode);
    procedure setDelimiters(const d: TSetOfChar);
    procedure setApplyKeywords(apply: Boolean);
    procedure setApplyStartStopKeys(apply: Boolean);
    procedure setEndOfTextMark(p: TPen);
    procedure setSpecUnderline(p: TPen);
    function  getSeparators: string;
    procedure setSeparators(const s: string);
    procedure setOptions(opt: TPlusMemoOptions);
    procedure setOverwrite(ovr: Boolean);
    function  getUndoList(i: Integer): TUndoRecord;
    procedure setUndoMaxLevel(uml: Integer);
    procedure setUndoMaxSpace(ums: Longint);
    procedure setHighlighter(aHighlighter: TPlusHighlighter);
    function  getCurrentWord: string;
    function  getParBackgnd(i: Longint): TColor;
    function  getParForegnd(i: Longint): TColor;
    procedure setParBackgnd(i: Longint; c: TColor);
    procedure setParForegnd(i: Longint; c: TColor);
    procedure setTopOrigin(top: Longint);
    procedure setStaticFormat(ssf: Boolean);
    procedure setBackground(pic: TPicture);
    procedure setSelBackColor(bc: TColor);
    procedure setSelTextColor(tc: TColor);
    procedure setUpperCaseType(ut: TpmUpperCase);
    function GetSelContext: Integer;
    function GetColumnBlockXY: TRect;

    { overriden protected methods }
    procedure CreateHandle; override;
    procedure DestroyWindowHandle; override;
    procedure CreateParams(var p: TCreateParams); override;
    procedure KeyDown(var Key: Word; Shift: TShiftState); override;
    procedure KeyPress(var Key: Char); override;
    procedure KeyUp(var Key: Word; Shift: TShiftState); override;
    procedure MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
    procedure MouseMove(Shift: TShiftState; X, Y: Integer); override;
    procedure MouseUp(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
    procedure DblClick; override;
    procedure Loaded; override;
    procedure WndProc(var Message: TMessage); override;

    { internal protected methods }
    procedure Reformat;
    procedure FormatNow(StartPar, StopPar: Longint; Unconditional: Boolean);
    procedure ParseStartStopNow(ToReach: Longint);
    procedure ReformatPar(DC: HDC; InitDC: Boolean; Width: Integer;
                          Par: pParInfo;
                          frmParNb: Longint;
                          var Line, LastLineChanged: PlusCardinal;
                          CompleteReformat: Boolean;  var OldFontH: THandle); virtual;

    function AttrToExtFontStyles(StaticAttrib: TExtFontStyles; DynAttrib: Word): TExtFontStyles;

    procedure Change; virtual;
    procedure ETPenChange(Sender: TObject);
    procedure InvalidateLines(start, stop: Longint; erase: Boolean);
    procedure RemoveUndo(i: Integer);
    procedure UpdateCaret(Recreate: Boolean);
    procedure PlaceCaret;

    {procedure EndModifications;    moved in PlusSup.Inc to make room under Delphi 1}
    procedure RefreshDisplay;
    function  ColPos(Par: pParInfo; ParNb: Longint; LineNb, ColNb: PlusCardinal): Longint; virtual;
    function  ColNb(Par: pParInfo; ParNb: Longint; LineNb: PlusCardinal;
                                     X: Integer; NoTabRounding: Boolean): PlusCardinal; virtual;
    function  LinePos(lineFromTop: Integer): Integer; virtual;
    function  LineNb(Y: Integer): Integer; virtual;
    procedure SetDynStyleP(Start, Stop: TPlusNavigator; dinfo: DynInfoRec;
                           AddDInfo, ExtendModFields: boolean); virtual;
    procedure ApplyKeywordsList(Start, Stop: TPlusNavigator);  virtual;
    procedure ApplyStartStopKeyList(Start, Stop: TPlusNavigator); virtual;
    procedure ExtendMods(Startpar: Longint; Startline: PlusCardinal; Stoppar: Longint);
    procedure CheckIntegrity;
    {function FindStop(Start, Stop: TPlusNavigator): Boolean;   they have been moved in PlusSup.inc
    function FindStart(Start, Stop: TPlusNavigator): Boolean;   to make room under Delphi 1 }
    function GetCaretWidth: Integer;
    function SelectedBlockText: string;
    procedure CreatePopupMenu;
    procedure PopupClickHandler(Sender: TObject);

  public
    { special support for highlighters and other accessories }
    fLineHeight : Integer;
    fLineBase   : Integer;
    NotifyMsg   : TMessage;

    property MsgList : TList read fMsgList;
    function GetUpText(Par: pParInfo; ParNb: Longint; Start, Len: PlusCardinal): PChar;

    { TControl overrides }
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure  SetBounds(ALeft, ATop, AWidth, AHeight: Integer); override;

    { TMemo replacements }
    procedure Clear;
    procedure ClearSelection;
    procedure CopyToClipboard;
    procedure CutToClipboard;
    function  GetTextBuf(Buffer: PChar; BufSize: PlusCardinal): PlusCardinal;
    function  GetTextLen: Longint;
    function  GetSelTextBuf(Buffer: PChar; BufSize: PlusCardinal): PlusCardinal;
    procedure PasteFromClipboard;
    procedure SelectAll;
    procedure SetSelTextBuf(t: PChar); virtual;
    procedure SetTextBuf(t: PChar);

    property Modified: Boolean read fModified write fModified;
    property SelStart : Longint read getSelStart write setSelStart;
    property SelLength: Longint read fSelLen write setSelLength;
    property SelText  : string read getSelText write setSelText;


    { new methods, properties of TPlusMemo }
    procedure ClearStyle(Pos: Longint);
    procedure ClearStyleRange(FromRange, ToRange: Longint);
    procedure ClearUndo;
    procedure DrawLine(Line: Longint; Canvas: TCanvas; Pos: TPoint);
    procedure DrawLines(StartLine, StopLine: Longint; Canvas: TCanvas; Pos: TPoint; VertSpacing: Single);
    function  DynText(Start, Stop: Longint): string;
    {$IFDEF BCB} function  FindTxt
    {$ELSE}           function  FindText
    {$ENDIF}               (const Text: string; GoForward, MatchCase, WholeWordsOnly: Boolean): Boolean;
    procedure FormatText(Canvas: TCanvas; Width: Integer);
    function  GetTextPart(Start, Stop: Longint): string;
    function  GetTextPartBuf(Start, Stop: Longint): PChar;
    procedure InsertBlock(const block: string);
    procedure LoadFromNativeStream(Stream: TStream; Reformat: Boolean); virtual;
    procedure LoadFromStream(Stream: TStream); virtual;
    procedure ReApplyKeywords;
    procedure SaveToNativeStream(Stream: TStream); virtual;
    procedure SaveToStream(Stream: TStream); virtual;
    procedure SaveToRTFStream(Stream: TStream; DynFormat: Boolean);
    procedure ScrollInView;
    procedure SelectBlock(StartCol, StartLine, StopCol, StopLine: Integer);
    procedure SetBold;
    procedure SetItalic;
    procedure SetUnderline;
    procedure SetHighlight;
    procedure SetFindMode;
    procedure SetFindAgainMode;
    procedure DoHelpRequested;
    procedure DoEscapeRequested;
    procedure ToggleWordwrap;
    procedure SetDynStyle(Start, Stop: Longint; Style: TExtFontStyles; ContentDependant: Boolean;
                          Context: Integer; Cursor : TCursor; Backgnd, Foregnd: TColor);
    procedure SetDynText(Start, Stop: Longint; const DText: string);
    procedure SetTopLeft(NewTopOrigin, NewLeftOrigin: Longint);
    procedure Redo;
    procedure Undo;
    procedure ForceUpdate;

    property CanRedo   : Boolean read getCanRedo;
    property CanUndo   : Boolean read getCanUndo;
    property CaretX    : Integer read fCaretX;
    property CaretY    : Integer read fCaretY;
    property CharCount : Longint read fTextLen;
    property Chars[i: Longint]: Char read getChar;
    property CharSet   : Integer read fCharSet write fCharSet;
    property ColumnBlockSelection: Boolean read fBlockSelection;
    property ColumnBlockXY: TRect read getColumnBlockXY;
    property CurrentWord: string read getCurrentWord;
    property Delimiters: TSetOfChar read fDelimiters write setDelimiters;
    property DraggingSelection: Boolean read fDragging;
    property FirstVisibleLine: Longint read getTopLine write setTopLine;
    property FormatCompleted : Integer read fCompleted;
    property LeftOrigin      : Integer read fDisplayLeft write setDisplayLeft;
    property LineCount       : Longint read fLineCount;
    property LinesArray[i: Longint]: string read getLines;
    property LinesBuf[i: Longint]  : PChar read getLinesBuf;
    property MouseNav        : TPlusNavigator read fMouseNav;
    property ParagraphCount  : Longint read getParCount;
    property Paragraphs: TStrings read fPars;
    property PargrphBuf[i: Longint]   : PChar read getParsBuf write setParsBuf;
    property PargrphNumber[Offset: Longint]: Longint read getPar;
    property PargrphOffset[i: Longint]: Longint read getParsOffset;
    property ParsArray[i: Longint]: string read getParagraphs write setParagraphs;
    property ParagraphsBackground[i: Longint]: TColor read getParBackgnd write setParBackgnd;
    property ParagraphsForeground[i: Longint]: TColor read getParForegnd write setParForegnd;

    property RTFLeading : Integer read fRTFLeading write fRTFLeading;
    property SelContext : Integer read GetSelContext;
    property SelCol     : PlusCardinal read getSelCol write setSelCol;
    property SelLine    : Longint read getSelLine write setSelLine;
    property SelPar     : Longint read getSelpar write setSelPar;
    property SelStyle   : TExtFontStyles read getCurrentStyle;
    property SelStartNav: TPlusNavigator read fSelStart;
    property SelStopNav : TPlusNavigator read fSelStop;
    property SelNav     : TPlusNavigator read fcp;
    property StripStrayCtrlCodes: Boolean read fStripStrayCtrlCodes write fStripStrayCtrlCodes default True;
    property TopOrigin: Longint read fTopOrigin write setTopOrigin;
    property UndoCount: Integer read getUndoCount;
    property UndoLevel: Integer read fUndoLevel;
    property UndoList[Index: Integer]: TUndoRecord read getUndoList;

    property Canvas;

  published
    { TMemo replacements }
    property Alignment : TAlignment read fAlignment write setAlignment;
    property BorderStyle: TBorderStyle read fBorderStyle write setBorderStyle default bsSingle;
    property HideSelection   : Boolean read fHideSelection write setHideSelection default True;
    property Lines: TStrings read fLines write setLines;
    property ScrollBars : TScrollStyle read getScrollBars write setScrollBars;
    property WantTabs   : Boolean read fWantTabs write fWantTabs default True;
    property WordWrap   : Boolean read fWordWrap write setWordWrap default True;

    property OnChange   : TNotifyEvent read fOnChange write fOnChange;

    { new properties in TPlusMemo }
    property AltFont         : TFont read fAltFont write setfAltFont;
    property ApplyKeyWords: Boolean read fApplyKeyWords write setApplyKeyWords default True;
    property ApplyStartStopKeys: Boolean read fApplyStartStopKeys write setApplyStartStopKeys default True;
    property BackgroundBmp   : TPicture read fBackground write setBackground;
    property CaretWidth      : Integer read fCaretWidth write setCaretWidth;
    property DisplayOnly     : Boolean read fDisplayOnly write setDisplayOnly;
    property EnableHotKeys   : Boolean read fEnableHotKeys write fEnableHotKeys default True;
    property EndOfTextMark   : TPen read fEndOfTextPen write setEndOfTextMark;
    property HighlightBackgnd: TColor read fHBColor write setHBColor default clWindow;
    property HighlightColor  : TColor read fHTColor write setHTColor default clRed;
    property Highlighter     : TPlusHighlighter read fHighlighter write setHighlighter;
    property Justified  : boolean read fJustified write setJustified;
    property Keywords   : TKeywordList read fKeywords write fKeywords;
    property LeftMargin : Integer read fLeftMargin write setLeftMargin default 8;
    property LineBreak  : TPlusLineBreak read fLineBreak write fLineBreak default pbCRLF;
    property LineHeight : Integer read getLineHeight write setLineHeight;
    property MouseWheelFactor: Integer read fMouseWheelFact write fMouseWheelFact default 3;
    property NullReplacement : Char read fNull write fNull;
    property Options         : TPlusMemoOptions read fOptions write setOptions;
    property Overwrite       : Boolean read fOverwrite write setOverwrite;
    property PassOverCodes: Boolean read fPassOver write fPassOver default True;
    property ReadOnly   : Boolean read fReadOnly write fReadOnly default False;
    property RightMargin: Integer read fRightMargin write setRightMargin default 8;
    property SelBackColor: TColor read fSelBackColor write setSelBackColor default -1;
    property SelTextColor: TColor read fSelTextColor write setSelTextColor default -1;
    property Separators : string read getSeparators write setSeparators;
    property ShowEndParSelected: Boolean read fShowEndParSelected write fShowEndParSelected default True;
    property SpecUnderline: TPen read fSpecUnderlinePen write setSpecUnderline;
    property StartStopKeys: TStartStopKeyList read fStartStopKeys write fStartStopKeys;
    property StaticFormat : Boolean read fStaticFormat write setStaticFormat default True;
    property TabStops   : Integer read fTabStops write setTabStops default 8;
    property UndoMaxSpace : Longint read fUndoMaxSpace write setUndoMaxSpace default 65538;
    property UndoMaxLevel : Integer read fUndoMaxLevel write setUndoMaxLevel default 256;
    property UpperCaseType: TpmUpperCase read fUpperCaseType write setUpperCaseType default pmuAscii;
    property UpdateMode : TUpdateMode read fUpdateMode write setUpdateMode default umBackground;
    property Version    : string read getVersion write setVersion;

    { new events in TPlusMemo }
    property OnStyleChange: TNotifyEvent read fOnAttrChange write fOnAttrChange;
    property OnSelMove    : TNotifyEvent read fOnMove write fOnMove;
    property OnHScroll    : TNotifyEvent read fOnHScroll write fOnHScroll;
    property OnVScroll    : TNotifyEvent read fOnVScroll write fOnVScroll;
    property OnProgress   : TNotifyEvent read fOnProgress write fOnProgress;
    property OnParse      : TParseEvent  read fOnParse write fOnParse;
    property OnContext    : TContextEvent read fOnContext write fOnContext;
    property OnBeforeChange: TpmBeforeChangeEvent read fOnBeforeChange write fOnBeforeChange;
    property OnAfterMouseDown: TMouseEvent read fOnAfterMouseDown write fOnAfterMouseDown;
    property OnFindModeRequest : TNotifyEvent read fOnCtrlF write fOnCtrlF;
    property OnFindAgainRequest : TNotifyEvent read fOnF3 write fOnF3;
    property OnHelpRequest : TNotifyEvent read fOnF1 write fOnF1;
    property OnEscapeRequest : TNotifyEvent read fOnESCape write fOnESCape;
    property OnWordwrapChange : TNotifyEvent read fOnWordwrapChange write fOnWordwrapChange;

    { exposition from TControl and TWinControl }
    property Align;
    property Color default clWindow;
    property Ctl3D;
    property DragCursor;
    property DragMode;
    property Enabled;
    property Font;
    property ParentColor default False;
    property ParentCtl3D;
    property ParentFont;
    property ParentShowHint;
    property PopupMenu;
    property ShowHint;
    property TabOrder;
    property TabStop;
    property Visible;
    property OnClick;
    property OnDblClick;
    property OnDragDrop;
    property OnDragOver;
    property OnEndDrag;
    property OnEnter;
    property OnExit;
    property OnKeyDown;
    property OnKeyPress;
    property OnKeyUp;
    property OnMouseDown;
    property OnMouseMove;
    property OnMouseUp;
    {$IFDEF WIN32}
    property OnStartDrag;
    {$ENDIF}

    {$IFDEF D4New}  { Delphi 4 new properties }
    property Anchors;
    property BiDiMode;
    property Constraints;
    property DragKind;
    property ParentBiDiMode;
    property OnEndDock;
    property OnStartDock;
    {$ENDIF}

    {$IFDEF D5New}   { Delphi 5 new properties }
    property OnContextPopup;
    {$ENDIF}

  end;


const ProgressInterval: Integer = 300;  { how long in ms between each OnProgress event }
      UserUpperCaseProc: TpmUpperCaseProc = nil;

procedure Register;

implementation

{$R PLUSMEMO.RES}   { The drag-copy and line selection cursors, which are not defined in VCL }


uses WinProcs, Clipbrd, SysUtils;

const maxtextlen = High(PlusCardinal)-100;
      IdSn: string[42] = 'PlusMemo v5.3cKRP0A A24EB6Professional ed.';
          { Changed the way TPlusNavigator's are updated following editing operations }

var MemoCount: Integer;  { number of TPlusMemo created and still alive }
    PMRightArrowCur: THandle; { Handle of right arrow cursor from plusmemo.res, loaded when first TPlusMemo created }
    ClipboardBlockFormat: Cardinal;  { Clipboard format for blocks, initialized as needed in CopyToClipboard and PasteFrom... }
    {$IFDEF PMDEBUG} debugwin: THandle; {$ENDIF}
    {$IFNDEF WIN32} Win32Platform: Integer; {$ENDIF}

var tmpPars : TParagraphsList;       { we keep this list alive to save on creating it in SetSelTextBuf for efficiency }
    tmpnav1, tmpnav2, tmpnav3: TPlusNavigator; { temporary TPlusNavigators used in ApplyStartStopkeys }
                                               { they are kept created for efficiency whenever MemoCount>0 }

{ type declarations for structures used in Undo buffers }
type OffsetRangeRecord = record Start, Stop: LongInt; Coupled: Boolean end;
     pOffsetRangeRecord = ^OffsetRangeRecord;

{********* First, a few support routines *************}

procedure RegisterCbBlocks;
  begin
  if ClipboardBlockFormat=0 then
    ClipboardBlockFormat:= RegisterClipboardFormat('Borland IDE Block Type')
  end;

function MaxOf(i1, i2: Longint): Longint;
  begin
  if i1<i2 then Result:= i2 else Result:= i1
  end;

function MinOf(l1, l2: Longint): Longint;
  begin
  if l1<l2 then Result:= l1 else Result:= l2
  end;

function XORStyle(style: TExtFontStyles; element: TExtFontStyle): TExtFontStyles;
  begin
  if element in style then Result:= style - [element]
                      else Result:= style + [element]
  end;

{$IFDEF WIN32}
{$I PLUSSUP.INC}
{$ENDIF}


{ *********** TPlusMemo ************** }

{ public methods }

constructor TPlusMemo.Create(AOwner: TComponent);             { public methods }
  begin
  inherited Create(AOwner);

  fMsgList:= TList.Create;
  fBackground:= TPicture.Create;
  fBackground.OnChange:= BackgroundChange;
  fNavigators:= TList.Create;
  fDisplayTop:= TPlusNavigator.Create(Self);
  fSelStart:= TPlusNavigator.Create(Self);
  fSelStop:= TPlusNavigator.Create(Self);
  fcp:= fSelStart;

  ftmpnav1:= TPlusNavigator.Create(Self);
  ftmpnav2:= TPlusNavigator.Create(Self);
  fkeynav1:= TPlusNavigator.Create(Self);
  fkeynav2:= TPlusNavigator.Create(Self);
  fssnav1 := TPlusNavigator.Create(Self);
  fssnav2 := TPlusNavigator.Create(Self);
  fformnav1:= TPlusNavigator.Create(Self);
  fformnav2:= TPlusNavigator.Create(Self);
  fpnav1   := TPlusNavigator.Create(Self);
  fpnav2   := TPlusNavigator.Create(Self);
  fMouseNav:= TPlusNavigator.Create(Self);

  fUpdateStopPar:= -1;
  fUpParNb:= -1;

  Inc(MemoCount);
  if (MemoCount=1) and (IdSN[3]<>'Z') then
    begin
    tmpPars:= TParagraphsList.Create;
    tmpnav1:= TPlusNavigator.Create(nil);
    tmpnav2:= TPlusNavigator.Create(nil);
    tmpnav3:= TPlusNavigator.Create(nil);
    PmRightArrowCur:= LoadCursor(hInstance, 'PMRIGHTARROW')
    end;

  fKeywords:= TKeywordList.Create;
  fStartStopKeys:= TStartStopKeyList.Create;
  ControlStyle := [csClickEvents, csCaptureMouse, csDoubleClicks];

  {$IFDEF WIN32}
    if not NewStyleControls then
  {$ENDIF} ControlStyle:= ControlStyle + [csFramed];

  fEndOfTextPen:= TPen.Create;
  fEndOfTextPen.Color:= clRed;
  fEndOfTextPen.OnChange:= ETPenChange;
  fSpecUnderlinePen:= TPen.Create;
  fSpecUnderlinePen.Color:= clRed;
  fSpecUnderlinePen.OnChange:= ETPenChange;

  CreateStartingPar(Self);
  fLineCount:= 1;
  fPars:= TPlusParaStrings.Create;
  TPlusParaStrings(fPars).Memo:= Self;
  fLines:= TPlusLinesStrings.Create;
  TPlusLinesStrings(fLines).Memo:= Self;
  fDelimiters:= [#9, ' ', '.', ',', ';', ':', '=', '<', '>', '$', '%', '&', '/'];
  fOptions:= [pmoWideOverwriteCaret, pmoWrapCaret, pmoInsertKeyActive, pmoAutoScrollBars, pmoBlockSelection];
  fSelBackColor:= clHighlight;
  fSelTextColor:= clHighlightText;

  fUpdateMode:= umBackground;
  fApplyKeywords:= True;
  fApplyStartStopKeys:= True;

  fUndoList:= TList.Create;
  fUndoMaxLevel:= 256;
  fUndoMaxSpace:= 65536;
  fStaticFormat:= True;

  Width:= 100; Height:= 60;
  Cursor:= crIBeam;
  ParentColor:= False;
  Color:= clWindow;
  fCaretWidth:= 1;
  fAutoLineHeight:= True;
  fShowEndParSelected:= True;
  fLeftMargin:= 8;
  fRightMargin:= 8;
  fBorderStyle:= bsSingle;
  fEnableHotKeys:= True;
  fCaretWidth:= 1;
  fMouseWheelFact:= 3;
  fWantTabs:= True;
  fWordWrap:= True;
  fHTColor:= clRed;
  fHBColor:= clWindow;
  fHideSelection:= True;
  fAltFont:= TFont.Create;
  fTabStops:= 8;
  fCompleted:= -1;
  fPassOver:= True;
  fStripStrayCtrlCodes:= True;
  fSpaceWidth:= 1;
  fCharSet:= 1;
  fVScrollBar:= True;
  fXCaretRunningPos:= Low(fXCaretRunningPos);   { mark as undetermined }
  fStartLineSelection:= -1;   { not line selecting }
  fHScrollFact:= 1;
  fVScrollFact:= 1;
  fMaxOneShotChars:= 4096;
  TabStop:= true
  end;


destructor TPlusMemo.Destroy;                                 { public methods }
  var i: Integer;
  begin
  CleanUp(Self);
  fParagraphs.Destroy;
  Dec(MemoCount);
  if MemoCount=0 then
    begin
    tmpPars.Destroy;
    tmpnav1.Free;
    tmpnav2.Free;
    tmpnav3.Free
    end;

  fMsgList.Free;
  fMsgList:= nil;   { this one is important because inherited Destroy may result in a call WndProc }
  fSelLen:= 0;      { idem }
  fKeywords.Free;
  fStartStopKeys.Free;
  fBackground.Free;

  for i:= 0 to fNavigators.Count-1 do
    with TPlusNavigator(fNavigators[i]) do
      begin
      fPMemo:= nil;
      Free
      end;

  fNavigators.Free;
  fPars.Free;
  fLines.Free;
  if fLineBmp<>0 then
    begin
    DeleteObject(fLineBmp);
    fLineBmp:= 0;
    fLineBmpWidth:= 0
    end;
  for i:= 0 to fUndoList.Count-1 do StrDispose(fUndoList[i]);
  fUndoList.Destroy;
  fAltFont.Free;
  fEndOfTextPen.Free;
  fSpecUnderlinePen.Free;
  if fHighlighter<>nil then
    begin
    i:= fHighlighter.MemoList.IndexOf(Self);
    if i>=0 then fHighlighter.MemoList.Delete(i)
    end;
  inherited Destroy
  end;

procedure TPlusMemo.SetBounds;                                { public methods }
  begin
  inherited SetBounds(ALeft, ATop, AWidth, AHeight);
  if HandleAllocated and (not fInSetScroll) then
    begin
    setHScrollParams;
    SetVScrollParams;
    if fHScrollBar then SetScrollPos(Handle, SB_HORZ, fDisplayLeft div fHScrollFact, True);
    if fVScrollBar then SetScrollPos(Handle, SB_VERT, fTopOrigin div fVScrollFact, True);
    end
  end;


procedure TPlusMemo.ScrollInView;                             { public methods }
  begin
  ScrollAPMemo(Self);
  (* this method has been moved in Plussup.INC to make room under D1 *)
  end;  { method ScrollInView }

    { public methods }

function TPlusMemo.GetTextBuf(Buffer: PChar; BufSize: PlusCardinal): PlusCardinal;
  begin
  if fTextLen+1<BufSize then Result:= fTextLen else Result:= BufSize-1;
  tmpnav1.fPMemo:= Self;
  with tmpnav1 do
    begin
    fPos:= 0;
    Invalidate
    end;
  tmpnav1.GetTextBuf(Buffer, Result);
  tmpnav1.fPMemo:= nil;
  end;

function TPlusMemo.GetTextLen: Longint;
  begin
  Result:= CharCount
  end;

type TProtMemStream = class(TMemoryStream);   { to have access to SetPointer }

procedure TPlusMemo.SetTextBuf(t: PChar);                    { public methods }
  var tmpstream: TProtMemStream;
  begin
  if (fTextLen<>0) or (t[0]<>#0) then fModified:= True
                                 else Exit;
  tmpstream:= TProtMemStream.Create;
  {$IFDEF WIN32}
    tmpstream.SetPointer(t, StrLen(t));
    LoadFromStream(tmpstream);
    tmpstream.SetPointer(nil, 0);
  {$ELSE}
  { in D1, we have no choice but write to the stream buffer }
    tmpstream.WriteBuffer(t^, StrLen(t));
    tmpstream.Position:= 0;
    LoadFromStream(tmpstream);
  {$ENDIF}
  tmpstream.Free
  end;  { method SetTextBuf }


procedure TPlusMemo.SetSelTextBuf(t: PChar);                  { public methods }
{ this is the heart of TPlusMemo; every text modification occurs through this method,
  except for LoadFromStream }

  procedure SetTextError;
    begin
    raise Exception.Create('Text too long')
    end;

  procedure MakeTextRoom(txt: PChar; startremove, stopremove, replacelen, tlen: PlusCardinal);
    begin
    if stopremove-startremove<>replacelen then
        System.Move(txt[stopremove], txt[startremove+replacelen], tlen-stopremove+1)
    end;

  procedure StripCodes(parnb: Integer; var run: Longint);
    var par: pParInfo; part: PChar; j, k, parlen: PlusCardinal;
    begin
    par:= fParagraphs.Pointers[parnb];
    part:= par.Text;
    parlen:= par.Length;
    j:= 0;
    while j<parlen do
      if (part[j]<#26) and (part[j] in CtrlCodesSet) then
        begin
          k:= j+1;
          while (k<parlen) and (part[k] in CtrlCodesSet) and (part[k]<>part[j]) do Inc(k);
          if (k<parlen) and (part[k]=part[j]) then { flush redondant ctrl codes }
            begin
            fInStripCodes:= True;
            fNoCheckFormat:= True;
            SelStart:= par.StartOffset + j;
            SelLength:= 1;
            SetSelTextBuf(nil);
            SelStart:= par.StartOffset+k-1;
            SelLength:= 1;
            SetSelTextBuf(nil);
            Dec(parlen, 2);
            part:= par.Text;
            if par.StartOffset + j<run then Dec(run);
            if par.StartOffset + j<run then Dec(run);
            fInStripCodes:= False;
            fNoCheckFormat:= False
            end
          else Inc(j)
        end
      else Inc(j);
    end;


  var a0, al, i  : Longint;   { a0: Length of t; al: a0+number of ctrlcodes to add to not affect existing text }
      nlen, nblen,            { new length of paragraph, new buffer length }
      buflen,
      oldlen     : PlusCardinal;

      k, np      : Integer;   { k: general integer index;  np: number of new paragraphs in t }

      startoff, stopoff,      { offset in text buffer of start of selection, of stop of selection }
      j: PlusCardinal;        { general index }

      tt, ntxt : PChar;        { general purpose PChars }

      nextoffset,             { running start offset of paragraphs following modified ones }
      oldparcount,            { paragraph count before making text modifications }
      startp, stopp,          { paragraph number of start of selection, of stop of selection }
      startl, stopl,          { line number of start of selection, of stop of selection }
      startpos, stoppos,      { offset of start of selection, of stop of selection }
      cp,                     { offset of current position, either equal to startpos or stoppos }
      toffset,                { increase in character count brought by text modification}
      poffset,                { increase in paragraph count brought by text modification }
      loffset,                { increase in line count brought by text modification }
      lastnewlen  : Longint;  { new length of last paragraph being modified }

      s: string[4];           { control codes to add following t to not affect existing text static format }
      unbalformat: TExtFontStyles;  { unbalanced static styles in selection and new text combined }

      par, newparp: pParInfo;
      newpar: ParInfo;
      lin: LineInfo;
      linp: pLineInfo;

      attr  : TExtFontStyles;   { running static style }

      nextstartline,           { running StartLine for new paragraphs and subsequent ones }
      linesrem: Longint;       { number of lines removed, multiparagraph modification case }

      sremoveddyn,             { whether some DynInfoRec were included in the selection }
      mustparse: Boolean;      { whether there is some text to parse for keywords, start-stop keys or simply OnParse event }

      startdynattr,            { dyn attributes at start of selection }
      stopdynattr: DynInfoRec; { dyn attributes at stop of selection }
      rundynattr: pDynInfoRec;

      sselstart, sselstop,     { to hold values of fSelStart and fSelStop (access on the stack is more code efficient }
      dynstart,                { navigator used to find the location of dyn record of start of selection }
      dynstop: TPlusNavigator; { navigator used to find location of dyn record following stop of selection }

      foundstop : Boolean;     { used in finding the dyn record ending that of start of selection }

      firstnewparbuffer: PChar;  { temporary text buffer for first paragraph contained in t }

      p1: pParInfo;            { vars used in adjusting subsequent paragraphs information }
      p1add: Integer;
      p1offlim: PlusCardinal;
      imod, imodmax: PlusCardinal;

      abssellen, newundolen: Longint;   { vars used in processing undo }
      newundo: Boolean;
      newundop: PChar;
      oldundop: pOffsetRangeRecord;

      sometext: Boolean;

  begin    { SetSelTextBuf }
  if Assigned(fOnBeforeChange) then fOnBeforeChange(Self, t);
  if (fSelLen=0) and ((t=nil) or (t^=#0)) then Exit;

  if fBlockSelection then
    begin
    fBlockSelection:= False;
    Lines.BeginUpdate;
    for i:= fSelStart.ParNumber to fSelStop.ParNumber do
      begin
      SelPar:= i;
      if fBlockStopCol<fBlockStartCol then
        begin
        nlen:= fBlockStopCol;
        nblen:= fBlockStartCol
        end
      else
        begin
        nblen:= fBlockStopCol;
        nlen:= fBlockStartCol
        end;

      SelCol:= ColToOffset(fParagraphs.Pointers[i], nlen, TabStops, StaticFormat);
      SelLength:= ColToOffset(fParagraphs.Pointers[i], nblen, TabStops, StaticFormat) - SelCol;
      SetSelTextBuf(nil)
      end;
    Lines.EndUpdate;
    if (t=nil) or (t^=#0) then Exit
    end;

  fExtraCols:= 0;
  fXCaretRunningPos:= Low(fXCaretRunningPos);
  ExtendMods(fSelStart.ParNumber, fSelStart.ParLine, fSelStop.ParNumber);
  fModified:= True;


  { test wether new text has paired formatting codes }
  { at the same time, compute length of new text }
  unbalformat:= [];
  if t<>nil then
    if StaticFormat then
      begin
      tt:= t;
      while tt^<>#0 do
        begin
        if tt^<#26 then XorStyleCode(unbalformat, tt^);
        Inc(tt)
        end;
      a0:= tt-t
      end
    else a0:= StrLen(t)
  else a0:= 0;

  sselstart:= fSelStart;
  sselstop:= fSelStop;
  { break down the new text into individual paragraphs }
  tmpPars.Count:= 0;
  newpar:= sselstart.fPar^;
  with newpar do
    begin
    StartOffset:= 0;
    Text:= t;
    DynCount:= 0;
    LinesBuf:= nil;
    {$IFNDEF WIN32} LinesHandle:= 0; {$ENDIF}
    NbLines:= 1;
    DynBuffer:= nil;
    DynBufLen:= 0
    end;

  np:= 0;

  tt:= pmStrScan(t, #13);
  if tt<>nil then
      begin
      newpar.Length:= tt-t;
      Inc(tt);
      if tt^=#10 then Inc(tt)
      end
  else newpar.Length:= a0;
  tmpPars.Add(newpar);

  while tt<>nil do
    begin
    Inc(np);
    newpar.Text:= tt;
    tt:= pmStrScan(tt, #13);
    if tt<>nil then
      begin
      newpar.Length:= tt-newpar.Text;
      Inc(tt);
      if tt^=#10 then Inc(tt);
      end
    else newpar.Length:= @t[a0]-newpar.Text;
    newpar.StartDynAttrib:= nil;
    newpar.Formatted:= False;
    newpar.StartStopDone:= False;
    newpar.KeywordsDone:= False;
    newpar.Foregnd:= -1;
    newpar.Backgnd:= -1;
    tmpPars.Add(newpar);
    end;

  if pmoDiscardTrailingSpaces in Options then
    begin
    { get back with fSelStart }
    if (tmppars.Count>1) or ((tmppars.Count<=1) and (fSelStop.fOffset=fSelStop.Par^.Length)) then
      begin
      sometext:= False;
      j:= 0;
      with tmppars.ParPointers[0]^ do
        while (j<Length) and (not sometext) do
          begin
          sometext:= not (Text[j] in [' ', #9]);
          Inc(j)
          end;
      if not sometext then
        begin
        sometext:= False;
        while (fSelStart.ParOffset>0) and (fSelStart.fPar^.Text[fSelStart.fOffset-1] in [' ', #9]) do
          begin
          fSelStart.Pos:= fSelStart.Pos - 1;
          if fSelLen>0 then Inc(fSelLen) else Dec(fSelLen)
          end
        end
      end;    { getback with fSelStart }

    { trim trailing spaces in new text }
    for i:= 0 to tmppars.Count-1 do
      with tmppars.ParPointers[i]^ do
        begin
        j:= Length;
        while (j>0) and (Text[j-1] in [' ', #9]) do Dec(j);
        if (i<tmppars.Count-1) or (fSelStop.fOffset=fSelStop.fPar^.Length) then
          begin
          Dec(a0, Length-j);
          Length:= j;
          if i=tmppars.Count-1 then newpar.Length:= j;  { we could get by newpar altogether }
          end
        end
    end;

  { set initial values for local stack vars }
  al:= a0;
  cp:= fcp.Pos;
  stoppos:= sselstop.Pos;
  startpos:= sselstart.Pos;
  abssellen:= stoppos-startpos;
  startl:= sselstart.LineNumber;
  stopl:= sselstop.LineNumber;
  startp:= sselstart.fParNb;
  startoff:= sselstart.fOffset;
  stopoff:= sselstop.fOffset;
  startdynattr:= sselstart.DynAttr;
  sselstop.RightOfDyn;
  fOldFinalDyn:= sselstop.DynAttr;
  stopdynattr:= fOldFinalDyn;

  if (fMaxLineNumber>=startl) and (fMaxLineNumber<=stopl) then fMaxLineNumber:= -1;  { flag as unknown }
  fStartLineSelection:= -1; { no more line selecting after text change }
  oldparcount:= fParagraphs.Count;


  { test whether the selection has paired formatting codes
    also record whether we encompass some DynInfoRec }
  par:= sselstart.fPar;
  sremoveddyn:= False;
  stopp:= startp;  { stopp is used as a running paragraph number }
  tt:= par^.Text;
  j:= startoff;
  nlen:= par^.Length;
  if not StaticFormat then j:= nlen;
  while (j<stopoff) or (stopp<sselstop.fParNb) do
    begin
    if j<nlen then
      begin
      if tt[j]<#26 then XORStyleCode(unbalformat, tt[j]);
      Inc(j)
      end
    else begin
         if stopp>startp then
           begin
           if par^.DynCount>0 then sremoveddyn:= True
           end
         else
           if sselstart.DynNb<par^.DynCount then sremoveddyn:= True;
         inc(stopp);
         par:= fParagraphs.Pointers[stopp];
         tt:= par^.Text;
         nlen:= par^.Length;
         if StaticFormat then j:= 0 else j:= nlen
         end;
    end;

  sremoveddyn:= sremoveddyn or ((stopp>startp) and (sselstop.fDynNb>0)) or
                       ((stopp=startp) and (sselstop.fDynNb>sselstart.fDynNb));

  { put in s the necessary formatting codes to not affect existing text }
  s:= '';
  if (not fNoCheckFormat) and (unbalformat<>[]) then
      begin
      if fsUnderline in unbalformat then s:= s + ctrlUnderline;
      if fsItalic in unbalformat then s:= s + ctrlItalic;
      if fsBold in unbalformat then s:= s + ctrlBold;
      if TPlusFontStyle(fsHighlight) in TPlusFontStyles(unbalformat) then s:= s + ctrlHighlight;
      if TPlusFontStyle(fsAltFont) in TPlusFontStyles(unbalformat) then s:= s + ctrlAltFont
      end;
  Inc(al, Length(s));

  { take care of undo }
  if not fUndoSkip then
    begin
    if not fNoClearRedo then for i:= fUndoLevel to fUndoList.Count-1 do RemoveUndo(fUndoLevel);
    oldundop:= nil;
    if (fSelLen<>0) or (fUndoLevel=0) or fNoClearRedo or fUndoBreak then newundo:= True
    else begin
         oldundop:= fUndoList[fUndoLevel-1];
         newundo:= (cp<>oldundop^.Stop) or (cp=oldundop^.Start)
         end;

    fUndoBreak:= False;
    if newundo then
      begin
      newundolen:= abssellen+1+ SizeOf(OffsetRangeRecord);
      if newundolen>maxtextlen then
        begin
        newundolen:=maxtextlen;
        ClearUndo
        end;

      newundop:= StrAlloc(newundolen);
      GetSelTextBuf(newundop+SizeOf(OffsetRangeRecord), newundolen-SizeOf(OffsetRangeRecord));
      if fUndoLevel>=fUndoList.Count then fUndoList.Add(newundop)
                                     else begin
                                          Dec(fUndoTotalSize, StrBufSize(fUndoList[fUndoLevel]));
                                          StrDispose(fUndoList[fUndoLevel]);
                                          fUndoList[fUndoLevel]:= newundop
                                          end;
      Inc(fUndoTotalSize, newundolen);
      Inc(fUndoLevel);
      oldundop:= pOffsetRangeRecord(newundop);
      oldundop^.Coupled:= False;
      oldundop^.Start:= sselstart.Pos;
      oldundop^.Stop:= sselstart.Pos+al;
      while (fUndoTotalSize>fUndoMaxSpace) and (fUndoLevel<fUndoList.Count) do RemoveUndo(fUndoList.Count-1);
      while (fUndoTotalSize>fUndoMaxSpace) and (fUndoLevel>0) do RemoveUndo(0);
      end
    else oldundop^.Stop:= cp+a0
    end;


  { go on with text modification }
  if (stopp=startp) and (np=0) then
    with par^ do
      begin
      loffset:= 0;
      nlen:= Length-Longint((stopoff-startoff))+newpar.Length+System.Length(s);
      if nlen>maxtextlen then SetTextError;
      toffset:= al-(stopoff-startoff);

      { update subsequent DynCodes offset }
      for k:= sselstop.fDynNb to DynCount-1 do Inc(DynBuffer^[k].DynOffset, tOffset);
      if sselstop.fDynNb>sselstart.fDynNb then
        begin
        for k:= sselstop.fDynNb to DynCount-1 do DynBuffer^[sselstart.fDynNb+(k-sselstop.fDynNb)]:= DynBuffer^[k];
        Dec(DynCount, sselstop.fDynNb-sselstart.fDynNb)
        end;

      if Text<>nil then buflen:= StrBufSize(Text)
                   else buflen:= 0;
      if nlen>=buflen then
        begin
        if nlen>0 then
          begin
          nblen:= nlen + nlen div 64 + 1;
          if nblen<nlen then SetTextError;   { an overflow condition }
          ntxt:= StrAlloc(nblen);
          if Text<>nil then
            begin
            System.Move(Text^, ntxt^, Length+1);
            StrDispose(Text)
            end
          else ntxt^:= #0;
          Text:= ntxt;
          MakeTextRoom(ntxt, startoff, stopoff, al, Length)
          end
        end
      else
        if nlen+nlen div 8 <= buflen then
          begin
          if nlen>0 then nblen:= nlen + nlen div 64 + 1
                    else nblen:= 0;
          if nblen<nlen then SetTextError;    { an overflow condition }
          if nblen>0 then ntxt:= StrAlloc(nblen)
                     else ntxt:= nil;
          if ntxt<>nil then
            begin
            MakeTextRoom(Text, startoff, stopoff, al, Length);
            System.Move(Text^, ntxt^, nlen+1)
            end;
          StrDispose(Text);
          Text:= ntxt
          end
        else
          MakeTextRoom(Text, startoff, stopoff, al, Length);

      if nlen>0 then
        begin
        System.Move(t^, Text[startoff], a0);
        System.Move(s[1], Text[startoff+a0], System.Length(s))
        end;
      Length:= nlen;

      { update subsequent lines start and stop info }
      linp:= sselstart.NavLines.LinePointers[startl-StartLine];
      if stopl>startl then linp^.Stop:= startoff+al
                      else Inc(linp^.Stop, tOffset);

      for k:= startl-StartLine+1 to stopl-StartLine-1 do
         with sselstart.fNavLines.LinePointers[k]^ do
           begin
           Stop:= linp^.Stop;
           Start:= linp^.Stop
           end;

      if stopl>startl then
       with sselstart.fNavLines.LinePointers[stopl-StartLine]^ do
        begin
        Start:= linp^.Stop;
        Inc(Stop, tOffset)
        end;

      for k:= stopl-StartLine+1 to NbLines-1 do
        with sselstart.fNavLines.LinePointers[k]^ do
          begin
          Inc(Start, tOffset);
          Inc(Stop, tOffset);
          Inc(JustifyStart, tOffset)
          end;

      if fLockedCount=0 then fNoCompleteFormat:= True;
      if sremoveddyn then
        begin
        loffset:= sselstart.ParLine-NbLines+1;
        sselstart.fNavLines.Count:= sselstart.fParLine+1;
        sselstart.fNavLines.LinePointers[sselstart.fParLine].Stop:= Length;
        fNoCompleteFormat:= False;
        end
      end    { if startp=stopp and np=0, with par^ }

  else       { we have destroyed one or more paragraph, or added one or more }
    begin
    loffset:= np-(stopp-startp);
    { arrange the first modified paragraph }
    newparp:= tmpPars.Pointers[0];
    nlen:= startoff+newparp^.Length;
    if nlen>maxtextlen then SetTextError;
    par:= sselstart.fPar;
    newparp^.DynCount:= sselstart.fDynNb;
    {$IFNDEF WIN32}
      pmReAllocMem(newparp^.DynBuffer, 0,  par^.DynBufLen*DCodeLen);
    {$ELSE}
      ReAllocMem(newparp^.DynBuffer, par^.DynBufLen*DCodeLen);
    {$ENDIF}
    if par^.DynBufLen>0 then System.Move(par^.DynBuffer^, newparp^.DynBuffer^, newparp^.DynCount*DCodeLen);
    nblen:= nlen + nlen div 64;
    if nblen<nlen then SetTextError;  { an overflow condition  }
    if par^.Text<>nil then buflen:= StrBufSize(par^.Text)
                      else buflen:= 0;
    if nblen>0 then
      begin
      newparp^.Text:= StrAlloc(nblen+1);
      if startoff>0 then System.Move(par^.Text^, newparp^.Text^, startoff);
      if nlen>startoff then System.Move(t^, newparp^.Text[startoff], nlen-startoff);
      newparp^.Text[nlen]:= #0
      end
    else newparp^.Text:= nil;

    firstnewparbuffer:= newparp^.Text;

    newparp^.Length:= nlen;
    newparp^.DynBufLen:= par^.DynBufLen;
    tOffset:= newparp^.Length-startoff;

    for i:= 1 to np-1 do
      begin
      newparp:= tmpPars.Pointers[i];
      nlen:= newparp^.Length;
      nblen:= nlen + nlen div 64;
      if (nblen>maxtextlen+1) or (nblen<nlen) then SetTextError;
      if nblen>0 then ntxt:= StrAlloc(nblen+1)
                 else ntxt:= nil;
      if nlen>0 then
        begin
        System.Move(newparp^.Text[0], ntxt[0], nlen);
        ntxt[nlen]:= #0
        end;
      newparp^.Length:= nlen;
      newparp^.Text:= ntxt;
      Inc(tOffset, nlen+2)
      end;

    { set last modified paragraph }
    par:= fParagraphs.Pointers[stopp];
    newparp:= tmpPars.Pointers[tmpPars.Count-1];
    lastnewlen:= newparp^.Length;     { assign in a Longint for following calc. }

    { update subsequent dyn codes offset }
    if np=0 then nextoffset:= startoff-stopoff+toffset
            else if stopp=startp then nextoffset:= lastnewlen - stopoff
                                 else nextoffset:= lastnewlen - (par^.Length - stopoff);
    Inc(newparp^.DynCount, par^.DynCount-sselstop.fDynNb);
    if newparp^.DynCount>newparp^.DynBufLen then
      begin
      {$IFNDEF WIN32}
        pmReAllocMem(newparp^.DynBuffer, newparp^.DynBufLen*DCodeLen, newparp^.DynCount*DCodeLen);

      {$ELSE}
        ReAllocMem(newparp^.DynBuffer, newparp^.DynCount*DCodeLen);
      {$ENDIF}
      newparp^.DynBufLen:= newparp^.DynCount
      end;
    for k:= sselstop.fDynNb to par^.DynCount-1 do
      begin
      Inc(par^.DynBuffer^[k].DynOffset, nextoffset);
      newparp^.DynBuffer^[newparp^.DynCount+k-par^.DynCount]:= par^.DynBuffer^[k]
      end;

    nlen:= par^.Length - stopoff + Longint(Length(s))+lastnewlen;

    if nlen>maxtextlen then SetTextError;
    nblen:= nlen + nlen div 64;
    if nblen>0 then ntxt:= StrAlloc(nblen+1)
               else ntxt:= nil;

    System.Move(newparp^.Text^, ntxt^, lastnewlen);
    System.Move(s[1], ntxt[lastnewlen], Length(s));
    System.Move(par^.Text[stopoff], ntxt[lastnewlen+System.Length(s)], par^.Length-stopoff);
    if nblen>0 then ntxt[nlen]:= #0;
    if np>0 then Inc(tOffset, newparp^.Length+2)
            else if firstnewparbuffer<>nil then StrDispose(firstnewparbuffer);
    Inc(tOffset, Length(s));
    newparp^.Length:= nlen;
    Dec(toffset, absSelLen);
    newparp^.Text:= ntxt
    end;   { startp<>stopp or np<>0 }

  Inc(fTextLen, toffset);

  {  replace paragraph info }
  poffset:= np-(stopp-startp);
  if (poffset=0) and (np=0) then
      begin    { update only StartOffset, StartLine and TPlusNavigator fpos }
      if startp+1<oldparcount then p1:= fParagraphs.Pointers[startp+1]
                              else p1:= nil;
      p1add:= fParagraphs.ElementSpace;
      imodmax:= $08000 div p1add ;
      imod:= (startp+1) mod imodmax;

      for i:= startp+1 to oldparcount-1 do
        begin
        Inc(p1^.StartOffset, toffset);
        Inc(p1^.StartLine, loffset);
        Inc(imod); if imod<imodmax then begin
                                        {$IFDEF WIN32}
                                        Inc(pbyte(p1), p1add);
                                        {$ELSE}
                                        Inc(Longrec(p1).lo, p1add)
                                        {$ENDIF}
                                        end
        else if i<oldparcount-1 then begin p1:= fParagraphs.Pointers[i+1]; imod:= 0 end
        end;
      if fMaxLineNumber>stopl then Inc(fMaxLineNumber, loffset)
      end

  else
    begin     { discard invalid paragraphs, update StartOffset, StartLine }
    if fUpdateStartPar>startp then
      if fUpdateStartPar>stopp then Inc(fUpdateStartPar, poffset)
                               else fUpdateStartPar:= startp;
    if fUpdateStopPar>startp then
      if fUpdateStopPar>stopp then Inc(fUpdateStopPar, poffset)
                              else fUpdateStopPar:= startp+np
    else fUpdateStopPar:= startp+np;

    with fParagraphs.ParPointers[startp]^ do
      begin
      nextoffset:= StartOffset;
      nextstartline:= StartLine
      end;

    for i:= 0 to np do
        with tmpPars.ParPointers[i]^ do
          begin
          StartLine:= nextstartline;
          StartOffset:= nextoffset;
          nextoffset:= nextoffset+Length+2;
          Inc(nextstartline)
          end;

    linesrem:= 0;
    with fMouseNav.NavLines do
      begin
      LLPar:= sselstart.fPar;
      for i:= startp to stopp do
        begin
        if i>startp then
            begin
            LLPar:= fParagraphs.Pointers[i];
            RemoveRef(LLPar^.StartDynAttrib);
            end;
        {$IFNDEF WIN32} pmReAllocMem(LLPar^.DynBuffer, LLPar^.DynBufLen*DCodeLen, 0);
        {$ELSE}         ReAllocMem(LLPar^.DynBuffer, 0); {$ENDIF}
        if LLPar^.Text<>nil then StrDispose(LLPar^.Text);
        Inc(linesrem, LLPar^.NbLines);
        Count:= 0
        end;
      LLPar:= fMouseNav.fPar;
      end;

    loffset:= np+1-linesrem;
    if pOffset<0 then
      begin
      for i:= startp+np+1 to fParagraphs.Count+poffset-1 do
        begin
        par:= fParagraphs.Pointers[i-poffset];
        Inc(par^.StartOffset, tOffset);
        Inc(par^.StartLine, lOffset);
        fParagraphs[i]:= par^
        end;
      fParagraphs.Count:= oldparcount+pOffset
      end
    else begin
         fParagraphs.Count:= oldparcount+poffset;
         for i:= fParagraphs.Count-1 downto stopp+poffset+1 do
           begin
           par:= fParagraphs.Pointers[i-poffset];
           Inc(par^.StartOffset, tOffset);
           Inc(par^.StartLine, lOffset);
           fParagraphs[i]:= par^
           end;
         end;
    for i:= 0 to np do fParagraphs[i+startp]:= tmpPars[i];

    with fParagraphs do
      if Capacity > Count + Count div 16 + 8 then
          Capacity:= Count + Count div 32 +4;
    sselstart.fPar:= fParagraphs.Pointers[startp];  { the list may have been reallocated }
    if fMaxLineNumber>stopl then Inc(fMaxLineNumber, lOffset)
    end;  { replace paragraphs, case poffset<>0 or np<>0 }


  { enter the new lines }
  if (startp<>stopp) or (np<>0) then
    begin
    sselstart.fParLine:= High(PlusCardinal);
    sselstart.fPar:= fParagraphs.Pointers[startp];
    if sselstart.fNavLines<>nil then sselstart.fNavLines.LLPar:= sselstart.fPar;
    if sselstart.fDynNb=0 then rundynattr:= sselstart.fPar^.StartDynAttrib
    else if np>0 then
         begin
         New(rundynattr);
         rundynattr^.DynOffset:= 0     { its reference count }
         end;

    with sselstart.fPar^ do
      begin
      attr:= FirstLine.StartAttrib;
      if StaticFormat and (Length>0) then
          for j:= 0 to Length-1 do
              if Text[j]<#26 then XORStyleCode(attr, Text[j])
      end;


    for i:= 1 to np do
      begin
      with fParagraphs.ParPointers[startp+i]^ do
        begin
        NbLines:= 1;
        with FirstLine do
          begin
          Start:= 0; Stop:= Length;
          StartAttrib:= attr;
          StartDynAttrib:= rundynattr;
          Inc(rundynattr^.DynOffset)    { its reference count }
          end;
        if StaticFormat and (Length>0) then
          for j:= 0 to Length-1 do
              if Text[j]<=#26 then XORStyleCode(attr, Text[j]);
        end
      end
    end;

  { keep cached uppercase text in sync with the changes }
  if fUpParNb>=startp then
    begin
    if fUpParNb>stopp then Inc(fUpParNb, poffset)
    else
      begin
      StrDispose(fUpText);
      fUpParNb:= -1
      end
    end;

  Inc(fLineCount, lOffset);
  Inc(fModLinesOffset, loffset);
  Inc(fModStopPar, poffset);

  for i:= 0 to fNavigators.Count-1 do
      with TPlusNavigator(fNavigators[i]) do
        begin
        if fPos>=startpos then
          if fPos>stoppos then
            begin
            Inc(fPos, toffset);
            if fPar<>nil then
              begin
              if fParNb=stopp then
                begin   { invalidate it }
                fDynNb:= High(PlusCardinal);
                fParLine:= High(PlusCardinal);
                Inc(fOffset, toffset)   { it may be recomputed below if paragraph mods occured }
                end;
              Inc(fParNb, poffset);
              end
            end
          else Assign(sselstart);
        if (poffset<>0) and (fPar<>nil) then  { we may have reallocated the list }
          begin
          fPar:= fParagraphs.Pointers[fParNb];
          if fNavLines<>nil then fNavLines.LLPar:= fPar;
          fOffset:= fPos - fPar^.StartOffset
          end
        end;

  al:= abssellen+toffset;
  cp:= startpos+al;
  if np=0 then
    begin
    sselstop.Assign(sselstart);
    sselstop.fPos:=cp;
    sselstop.fOffset:= sselstart.fOffset+al;
    sselstop.fParLine:= High(PlusCardinal)
    end
  else begin
       sselstop.fOffset:= lastnewlen+Length(s);
       sselstop.fDynNb:= 0;
       sselstop.fParNb:= startp+np;
       sselstop.fpos:= cp;
       sselstop.fPar:= fParagraphs.Pointers[sselstop.fParNb];
       if sselstop.fNavLines<>nil then sselstop.fNavLines.LLPar:= sselstop.fPar;
       sselstop.fParLine:= 0
       end;

  { now take care of dyn codes }
  { selstart and selstop contain the parse range }
  mustparse:= True;
  dynstart:= ftmpnav1;
  dynstop:= ftmpnav2;
  fExtraCols:= sselstop.fOffset;  { used in some highlighters }

  if not sremoveddyn then  { no dyn code has been met in selection }
    begin
    if (startdynattr.DynStyle and $80)<>0 then
      begin
      if (startdynattr.DynStyle and $40)=0 then mustparse:= False
      else  { content dependant: check if we touched the start key, if so extend the parse range
                                                                       to include it.
                                 check if we touched the stop key, if so extend the parse range;
                                 note: parsing must include the dyn to the right of the
                                       parse range! }
        if startdynattr.Level=-1 then
          begin
          sselstart.BackToDyn(0);
          sselstop.ForwardToDyn(fTextLen)
          end
        else
          begin
          dynstart.Assign(sselstart);
          repeat
             with dynstart do
              if BackToDyn(Par^.StartOffset) and (sselstart.Pos-Pos<=startdynattr.StartKLen) then
                 begin
                 sselstart.Assign(dynstart);
                 fModStartLine:= MinOf(fModStartLine, sselstart.LineNumber);
                 startdynattr:= sselstart.DynAttr;
                 foundstop:= getlevel(startdynattr)<0;
                 fNoCompleteFormat:= False
                 end
              else foundstop:= True
          until foundstop;

          dynstop.Assign(sselstop);
          with dynstop do
            if ForwardToDyn(Par^.StartOffset+Par^.Length) and
               (Pos-sselstop.Pos<=stopdynattr.StopKLen) then
              begin
              sselstop.Assign(dynstop);
              fOldFinalDyn:= fPar^.DynBuffer^[fDynNb];
              fNoCompleteFormat:= False
              end
          end  { content dependant }
      end  { selection was inside of dyn }
    end   { no dyn code in selection }

  else begin { there were some dyn codes in the selection: check for start in dyn, stop in dyn }
       fNoCompleteFormat:= False;
       if startdynattr.DynStyle and $80=$80 then
         if startdynattr.DynStyle and $40=$40 then
           begin    { content dependant }
           if startdynattr.Level=-1 then
             begin
             sselstart.BackToDyn(0);
             sselstop.ForwardToDyn(fTextLen)
             end
           else
             begin
             dynstart.Assign(sselstart);
             repeat
                with dynstart do
                 if BackToDyn(Par^.StartOffset) and (sselstart.Pos-Pos<=startdynattr.StartKLen) then
                    begin
                    sselstart.Assign(dynstart);
                    fModStartLine:= MinOf(fModStartLine, sselstart.LineNumber);
                    startdynattr:= sselstart.DynAttr;
                    foundstop:= getlevel(startdynattr)<0
                    end
                 else foundstop:= True
             until foundstop;
             end
           end

         else begin { not content dependant }
              startdynattr.DynStyle:= 0;
              sselstart.AddDyn(startdynattr);
              if sselstop.ParNumber=sselstart.ParNumber then sselstop.fDynNb:= High(PlusCardinal)
              end;

       if stopdynattr.DynStyle and $80=$80 then
         if stopdynattr.DynStyle and $40=$40 then
           begin      { content dependant }
           dynstop.Assign(sselstop);
           with dynstop do
             if ForwardToDyn(Par^.StartOffset+Par^.Length) and
                    (Pos-sselstop.Pos<stopdynattr.StopKLen) then
               begin
               sselstop.Assign(dynstop);
               fOldFinalDyn:= fPar^.DynBuffer^[fDynNb]
               end
           end
         else sselstop.AddDyn(stopdynattr)
       end;


  { we are done with temporary lists }
  tmpPars.Count:= 0;
  tmpPars.Capacity:= 2;
  Inc(fLockedCount);

  if mustparse then
    begin
    if Assigned(fOnParse) then fOnParse(Self, sselstart.Pos, sselstop.Pos);
    if fLockedCount>1 then
      with sselstart.Par^ do
          begin
          StartStopDone:= False;
          KeywordsDone:= False;
          Formatted:= False
          end
    else
      begin
      if sselstart.Par^.StartStopDone then    { don't leave a paragraph semi parsed }
        begin
        if sselstop.ParNumber<>sselstart.fParNb then
          begin
          sselstop.Assign(sselstart);
          sselstop.Pos:= sselstop.fPar^.Length+sselstop.fPar^.StartOffset
          end;
        ApplyStartStopKeyList(sselstart, sselstop);
        end;
      if sselstart.Par^.KeywordsDone then    { don't leave a paragraph semi parsed }
        begin
        if sselstop.ParNumber<>sselstart.fParNb then
          begin
          sselstop.Assign(sselstart);
          sselstop.Pos:= sselstop.fPar^.Length+sselstop.fPar^.StartOffset
          end;
        ApplyKeywordsList(sselstart, sselstop);
        sselstart.Par^.KeywordsDone:= True
        end
      end
    end;

  fExtraCols:= 0;
  fSelLen:= 0;
  sselstart.Invalidate;
  fBlockSelection:= False;

  if (not fInStripCodes) and StaticFormat and StripStrayCtrlCodes then
    for i:= fModStartPar to fModStopPar do
      StripCodes(i, cp);
  sselstart.Pos:= cp;
  sselstop.Assign(sselstart);
  fcp:= sselstart;


  Dec(fLockedCount);
  if fLockedCount=0 then
    begin
    EndModifications(Self);
    ScrollInView;
    end;

  fNoCompleteFormat:= False;
  end;   { method setSelTextBuf }

{ public methods }

function TPlusMemo.GetSelTextBuf(Buffer: PChar; BufSize: PlusCardinal): PlusCardinal;
  begin
  if Abs(fSelLen)+1<=BufSize then Result:= Abs(fSelLen) else Result:= BufSize-1;
  fSelStart.GetTextBuf(Buffer, Result)
  end;


function TPlusMemo.GetTextPart(Start, Stop: Longint): string; { public methods }
  const max = 255;
  var toget: PlusCardinal;
  begin
  if Start<0 then Start:= 0;
  if Stop>fTextLen then Stop:= fTextLen;
  if Stop<=Start then begin Result:= ''; Exit end;
  {$IFNDEF WIN32}
    if Stop-Start>max then toget:= max
    else
  {$ENDIF}
  toget:= Stop-Start;
  ftmpnav2.Pos:= Start;
  SetLength(Result, toget);
  ftmpnav2.GetTextBuf(@Result[1], toget)
  end;  { method GetTexPart }


function TPlusMemo.GetTextPartBuf(Start, Stop: Longint): PChar; { public methods }
  var tomove: PlusCardinal;
  begin
  if Stop>fTextLen then Stop:= fTextLen;
  if Start<0 then Start:= 0;
  if Stop<Start then begin Result:= nil; Exit end;
  if Stop-Start>maxtextlen then tomove:= maxtextlen else tomove:= Stop-Start;
  Result:= StrAlloc(tomove+1);
  ftmpnav2.Pos:= Start;
  ftmpnav2.GetTextBuf(Result, tomove);
  end;  { method GetTexPartBuf }


procedure TPlusMemo.ClearSelection;                           { public methods }
  begin
  SetSelText('');
  end;

procedure TPlusMemo.Clear;                                    { public methods }
  begin
  SetTextBuf('')
  end;


procedure TPlusMemo.SetBold;                                  { public methods }
  begin
  SetSelAttrib(ctrlBold)
  end;

procedure TPlusMemo.SetItalic;                                { public methods }
  begin
  SetSelAttrib(ctrlItalic);
  end;

procedure TPlusMemo.SetUnderline;                             { public methods }
  begin
  SetSelAttrib(ctrlUnderline)
  end;

procedure TPlusMemo.SetHighlight;                             { public methods }
  begin
  SetSelAttrib(ctrlHighlight)
  end;

procedure TPlusMemo.SetFindMode;                               { public methods }
  begin
  if assigned(OnFindModeRequest) then OnFindModeRequest(self);
  end;

procedure TPlusMemo.SetFindAgainMode;                               { public methods }
  begin
  if assigned(OnFindAgainRequest) then OnFindAgainRequest(self);
  end;

procedure TPlusMemo.DoHelpRequested;
  begin
  if assigned(OnHelpRequest) then OnHelpRequest(self);
  end;

procedure TPlusMemo.DoEscapeRequested;
  begin
  if assigned(OnEscapeRequest) then OnEscapeRequest(self);
  end;

procedure TPlusMemo.ToggleWordwrap;
  begin
  WordWrap := not WordWrap;
//  if assigned(fOnWordwrapChange) then fOnWordwrapChange(Self);
  Change;
  end;

procedure TPlusMemo.CopyToClipboard;                          { public methods }
  var s: PChar; bl: PlusCardinal;
      Data: THandle; DataPtr: PChar;
      sblock: string;
  begin
  if SelLength<>0 then
    if fBlockSelection then
      begin
      sblock:= SelectedBlockText;
      Clipboard.Open;
      RegisterCbBlocks;
      Data := GlobalAlloc(GMEM_MOVEABLE+GMEM_DDESHARE, Length(sblock)+1);
      DataPtr := GlobalLock(Data);
      Clipboard.SetTextBuf(PChar(sblock));
      Move(sblock[1], DataPtr^, Length(sblock)+1);
      GlobalUnlock(Data);
      Clipboard.SetAsHandle(ClipboardBlockFormat, Data);
      Clipboard.Close
      end
    else
      begin
      {$IFNDEF WIN32}
      if Abs(SelLength)>32700 then bl:= 32700
                              else bl:= Abs(SelLength);
      {$ELSE}
      bl:= abs(SelLength);
      {$ENDIF}
      s:= StrAlloc(bl+1);
      GetSelTextBuf(s, bl+1);
      Clipboard.SetTextBuf(s);
      StrDispose(s)
      end
  end;

procedure TPlusMemo.CutToClipboard;                           { public methods }
  begin
  CopyToClipboard;
  SetSelText('')
  end;

function TPlusMemo.DynText(Start, Stop: Longint): string;
  var tmp: Longint; dcodes, currpos: Integer;
  begin
  {$IFDEF WIN32}   { available only in D2 and up, because there is no space in D1 }
  if Start=Stop then
    begin
    Result:= '';
    Exit
    end;

  if Start>Stop then
    begin
    tmp:= Stop;
    Stop:= Start;
    Start:= tmp
    end;
  ftmpnav1.Pos:= Start;

  dcodes:= 0;
  while ftmpnav1.ForwardToDyn(Stop) do
    begin
    ftmpnav1.RightOfDyn;
    Inc(dcodes);
    end;

  { make room for dyn info, the position of each, and total number of dyn }
  SetLength(Result, Stop-Start+ (dcodes+1)*(SizeOf(DynInfoRec)+SizeOf(Integer))+SizeOf(Integer));

  { write the number of dyns }
  pInteger(@Result[1])^:= dcodes+1;
  currpos:= SizeOf(Integer)+1;

  { write each dyn and its pos }
  ftmpnav1.Pos:= Start;
  pDynInfoRec(@Result[currpos])^:= ftmpnav1.DynAttr;
  Inc(currpos, SizeOf(DynInfoRec));
  pInteger(@Result[currpos])^:= 0;
  Inc(currpos, SizeOf(Integer));
  while ftmpnav1.ForwardToDyn(Stop) do
    begin
    ftmpnav1.RightOfDyn;
    pDynInfoRec(@Result[currpos])^:= ftmpnav1.DynAttr;
    Inc(currpos, SizeOf(DynInfoRec));
    pInteger(@Result[currpos])^:= ftmpnav1.Pos-Start;
    Inc(currpos, SizeOf(Integer))
    end;

  ftmpnav1.Pos:= Start;
  ftmpnav1.GetTextBuf(PChar(@Result[currpos]), Stop-Start)
  {$ENDIF}
  end;


procedure TPlusMemo.PasteFromClipboard;                       { public methods }
  var sblock: Boolean;
  {$IFNDEF WIN32} var s:  PChar; len: PlusCardinal; {$ENDIF}
  begin
  if pmoBlockSelection in Options then
    begin
    RegisterCbBlocks;
    sblock:= Clipboard.HasFormat(ClipboardBlockFormat)
    end
  else
    sblock:= False;
  if Clipboard.HasFormat(CF_TEXT) then
    begin
    fUndoBreak:= True;
    {$IFNDEF WIN32}
    s:= StrAlloc(32760);
    Clipboard.GetTextBuf(s, 32759);
    SetSelTextBuf(s);
    StrDispose(s);
    {$ELSE}
    if sblock then InsertBlock(Clipboard.AsText)
              else SelText:= Clipboard.AsText;
    {$ENDIF}
    fUndoBreak:= True
    end
  end;

procedure TPlusMemo.SelectAll;                                { public methods }
  begin
  SelStart := fTextLen;
  SelLength:= -fTextLen
  end;

procedure TPlusMemo.LoadFromStream(Stream: TStream);          { public methods }
  begin
  LoadFromStreamLoc(Self, Stream);
  { this method has been moved in Plussup.INC to make room in D1 }
  end;  { method LoadFromStream }


procedure TPlusMemo.SaveToStream(Stream: TStream);            { public methods }
  begin
  SaveToStreamLoc(Self, Stream)  { moved in Plussup.INC to make room in D1 }
  end;

procedure TPlusMemo.SaveToRTFStream(Stream: TStream; DynFormat: Boolean); { public methods }
  begin
  SaveToRTFStreamLoc(Self, Stream, DynFormat);
  { this method has been moved in Plussup.INC to make room in D1 }
  end;   { method SaveToRTFStream }

procedure TPlusMemo.SaveToNativeStream(Stream: TStream);     { public methods }
   begin
   SaveToNativeStreamP(Self, Stream);
   (*  moved in Plussup.INC to make room under D1 *)
   end;

  { public methods }

procedure TPlusMemo.LoadFromNativeStream(Stream: TStream; Reformat: Boolean);
  begin
  LoadFromNativeStreamP(Self, Stream, Reformat);
  (* moved in PlusSup.INC to make room under D1 *)
  end;  { method LoadFromNativeStream }


 { public methods }

function  TPlusMemo.
  {$IFDEF BCB} FindTxt
  {$ELSE}           FindText
  {$ENDIF}          (const Text: string; GoForward, MatchCase: Boolean; WholeWordsOnly: Boolean): Boolean;
  begin
  Result:= FindTextP(Self, Text, GoForward, MatchCase, WholeWordsOnly)
  (* moved in Plussup.INC to make room in D1 *)
  end;

procedure TPlusMemo.ClearUndo;
  var i: Integer;
  begin
  for i:= 0 to fUndoList.Count-1 do StrDispose(fUndoList[i]);
  fUndoLevel:= 0;
  fUndoTotalSize:= 0;
  fUndoList.Clear
  end;

procedure TPlusMemo.FormatText(Canvas: TCanvas; Width: Integer);
    var dummy: TMessage;
    begin
    if Canvas<>nil then
      begin
      fW:= Width;
      fCanvas:= Canvas;
      fCanvas.Font:= Font;
      fSpaceWidth:= fCanvas.TextWidth(' ');
      Reformat;
      fCanvas:= nil
      end
    else CMFontChanged(dummy)
    end;

type TPaintInfo = record
                    StartLine, StopLine: Longint;
                    Position      : TPoint;
                    LineSpacingPix: Single
                    end;
    pPaintInfo = ^TPaintInfo;

procedure TPlusMemo.DrawLine(Line: Longint; Canvas: TCanvas; Pos: TPoint);
  begin
  DrawLines(Line, Line, Canvas, Pos, 0)
  end;

procedure TPlusMemo.DrawLines(StartLine, StopLine: Longint; Canvas: TCanvas; Pos: TPoint; VertSpacing: Single);
  var dummy: TMessage; pi: TPaintInfo;
  begin
  fCanvas:= Canvas;
  pi.Position:= pos;
  pi.StartLine:= StartLine;
  pi.StopLine:= StopLine;
  pi.LineSpacingPix:= VertSpacing;
  dummy.LParam:= Longint(@pi);
  WMPAINT(dummy);
  fCanvas:= nil;
  end;


{  public methods }

procedure TPlusMemo.SetDynText(Start, Stop: Longint; const DText: string);
  var i, dcodes: Integer; startpos, savedlen, currpos: Longint; pdyn: pDynInfoRec;
      nav1, nav2: TPlusNavigator;
  begin
  {$IFDEF WIN32}    { available only in D2 and up, because there is no room in D1 }
  if DText='' then Exit;
  nav1:= TPlusNavigator.Create(Self);
  nav2:= TPlusNavigator.Create(Self);
  nav1.Assign(fSelStart);
  nav2.Assign(fSelStop);
  savedlen:= SelLength;
  Lines.BeginUpdate;
  SelStart:= Start;
  SelLength:= Stop-Start;

  dcodes:= pInteger(@DText[1])^;
  currpos:= SizeOf(Integer)+1;
  startpos:= fSelStart.Pos;
  SetSelTextBuf(PChar(@DText[SizeOf(Integer)+dcodes*(SizeOf(DynInfoRec)+SizeOf(Integer))+1]));

  for i:= 1 to dcodes do
    begin
    pdyn:= @DText[currpos];
    Inc(currpos, SizeOf(DynInfoRec));
    fSelStart.Pos:= startpos + pInteger(@DText[currpos])^;
    Inc(currpos, SizeOf(Integer));

    { adjust its level and set it as not content dependant }
    with pdyn^ do
      if DynStyle and $80<>0 then
        begin
        Level:= -1;
        DynStyle:= DynStyle and $bf
        end;
    SetDynStyleP(fSelStart, fSelStop, pdyn^, pdyn^.DynStyle and $80 <>0, False)
    end;

  fSelStart.Assign(fSelStop);
  if savedlen>=0 then
    begin
    SelStart:= nav1.Pos;
    SelLength:= nav2.Pos-ftmpnav1.Pos
    end
  else
    begin
    SelStart:= nav2.Pos;
    SelLength:= nav1.Pos-nav2.Pos
    end;

  nav1.Free;
  nav2.Free;
  Lines.EndUpdate
  {$ENDIF}
  end;

procedure TPlusMemo.SetDynStyle(Start, Stop: Longint; Style: TExtFontStyles;
                                ContentDependant: Boolean;
                                Context: Integer;
                                Cursor : TCursor;
                                Backgnd, Foregnd: TColor);
  var dstart, dstop: TPlusNavigator;
      dinf   : DynInfoRec;
      lastpar: Longint;
  begin
  dStart:= TPlusNavigator.Create(Self);
  dStop:= TPlusNavigator.Create(Self);
  dstart.Pos:= Start;
  dstop.Pos:= Stop;
  dinf.DynStyle:= Byte(Style);
  dinf.Level:= -1;
  if ContentDependant then dinf.DynStyle:= dinf.DynStyle or $40;
  dinf.Klen:=0;
  dinf.Context:= Context;
  dinf.Cursor:= Cursor;
  dinf.Backgnd:= Backgnd;
  dinf.Foregnd:= Foregnd;
  lastpar:= dstop.ParNumber;
  SetDynStyleP(dstart, dstop, dinf, True, True);
  dstart.Free;
  dstop.Free;

  InvalidateNavs(fNavigators, Start, lastpar);

  if fLockedCount=0 then EndModifications(Self)
  end;



procedure TPlusMemo.ClearStyle(Pos: Longint);                 { public methods }
  var dummy: DynInfoRec;
  begin
  ftmpnav1.Pos:= Pos;
  if ftmpnav1.DynAttr.DynStyle and $80 <> 0 then
    begin
    ftmpnav2.Assign(ftmpnav1);
    ftmpnav1.BackToDyn(0);
    ftmpnav2.ForwardToDyn(fTextLen);
    Inc(ftmpnav2.fDynNb);
    SetDynStyleP(ftmpnav1, ftmpnav2, dummy, False, True);

    InvalidateNavs(fNavigators, ftmpnav1.Pos, ftmpnav2.ParNumber);
    if fLockedCount=0 then EndModifications(Self)
    end
  end;

procedure TPlusMemo.ClearStyleRange(FromRange, ToRange: Longint);
  var dummy: DynInfoRec;
  begin
  ftmpnav1.Pos:= FromRange;
  ftmpnav2.Pos:= ToRange;
  SetDynStyleP(ftmpnav1, ftmpnav2, dummy, False, True);
  InvalidateNavs(fNavigators, ftmpnav1.Pos, ftmpnav2.ParNumber);
  if fLockedCount=0 then EndModifications(Self)
  end;

procedure TPlusMemo.InsertBlock(const block: string);          { public methods }
  var slist: TStringList; i, scol, pcol, sline: Integer; s: string;
  begin
  slist:= TStringList.Create;
  slist.Text:= block;
  Lines.BeginUpdate;
  scol:= (fCaretX + fDisplayLeft - fLeftMargin) div fSpaceWidth;
  sline:= fSelStart.fParNb;
  for i:= 0 to slist.Count-1 do
    begin
    if i+sline>=fParagraphs.Count then Paragraphs.Add('');
    SelPar:= i+sline;
    SelCol:= ColToOffset(fParagraphs.Pointers[i+sline], scol, TabStops, StaticFormat);
    pcol:= ColToExtra(fParagraphs.Pointers[i+sline], scol, TabStops, StaticFormat);
    if pcol>0 then
      begin
      SetLength(s, pcol);
      FillChar(s[1], Length(s), Ord(' '))
      end
    else
      s:= '';
    SelText:= s + slist[i]
    end;
  Lines.EndUpdate;
  if fLockedCount=0 then ScrollInView
  end;

procedure TPlusMemo.SelectBlock(StartCol, StartLine, StopCol, StopLine: Integer);      { public methods }
  procedure Adjust(var Line: Integer);
    begin
    if Line<0 then Line:= 0;
    if Line>=fParagraphs.Count then Line:= fParagraphs.Count-1
    end;
  var snav: TPlusNavigator; scol: Integer;
  begin
  if WordWrap then Exit;
  Adjust(StartLine);
  Adjust(StopLine);
  snav:= TPlusNavigator.Create(Self);
  snav.ParNumber:= StopLine;
  snav.Col:= StopCol;
  scol:= snav.Col;
  SelStart:= snav.Pos;
  snav.ParNumber:= StartLine;
  snav.Col:= StartCol;
  SelLength:= snav.Pos - SelStart;
  fExtraCols:= MaxOf(StopCol-scol, 0);
  fBlockSelection:= True;
  UpdateCaret(False);
  fBlockStartCol:= StartCol;
  fBlockStopCol:= StopCol;
  snav.Free
  end;

procedure TPlusMemo.ReApplyKeywords;                          { public methods }
  var i: Longint; startpdyn: pDynInfoRec;
  begin
  startpdyn:= nil;
  for i:= 0 to fParagraphs.Count-1 do
    with pParInfo(fParagraphs.Pointers[i])^ do
      begin
      Formatted:= False;
      StartStopDone:= False;
      KeywordsDone:= False;
      DynCount:= 0;
      if i=0 then startpdyn:= StartDynAttrib
      else
        if StartDynAttrib<>startpdyn then
          begin
          RemoveRef(StartDynAttrib);
          StartDynAttrib:= startpdyn;
          Inc(startpdyn^.DynOffset)
          end
      end;

  InvalidateNavs(fNavigators, 0, fParagraphs.Count-1);
  fLastStartStopParsed:= -1;
  Reformat;
  Invalidate;
  end;

procedure TPlusMemo.ForceUpdate;
begin
Change;
end;

procedure TPlusMemo.Undo;                                     { public methods }
  var undocopy: PChar; doubleundo: Boolean;
  begin
  if fUndoLevel>0 then
    with UndoList[fUndoLevel-1] do
      begin
      doubleundo:= pOffsetRangeRecord(fUndoList[fUndoLevel-1])^.Coupled;
      SelStart:= UndoStart;
      SelLength:= UndoStop-UndoStart;
      fNoClearRedo:= True;
      fInUndo:= True;
      Dec(fUndoLevel);
      undocopy:= StrNew(UndoText);
      SetSelTextBuf(undocopy);
      if fUndoLevel>0 then Dec(fUndoLevel);
      fNoClearRedo:= False;
      fInUndo:= False;
      StrDispose(undocopy);

      if doubleundo then
        begin
        Undo;
        pOffsetRangeRecord(fUndoList[fUndoLevel+1])^.Coupled:= True
        end
      end
  end;

procedure TPlusMemo.Redo;                                     { public methods }
  var t: PChar; doubleredo: Boolean;
  begin
  if fUndoLevel<fUndoList.Count then
    with UndoList[fUndoLevel] do
      begin
      doubleredo:= (fUndoLevel<fUndoList.Count-1) and (pOffsetRangeRecord(fUndoList[fUndoLevel+1])^.Coupled);
      SelStart:= UndoStart;
      SelLength:= UndoStop-UndoStart;
      fNoClearRedo:= True;
      t:= StrNew(UndoText);
      SetSelTextBuf(t);
      fNoClearRedo:= False;
      StrDispose(t);

      if doubleredo then
        begin
        Redo;
        pOffsetRangeRecord(fUndoList[fUndoLevel-1])^.Coupled:= True
        end
      end
  end;


function TPlusMemo.GetCurrentStyle: TExtFontStyles;  { property access methods }
  begin
  ParseStartStopNow(fcp.ParNumber);
  Result:= AttrToExtFontStyles(fcp.Style, fcp.DynAttr.DynStyle);
  end;

function TPlusMemo.GetParCount: Longint;             { property access methods }
  begin
  Result:= fParagraphs.Count
  end;

function TPlusMemo.GetSelText: string;                { property access methods}
  {$IFNDEF WIN32} var Len: Integer; {$ENDIF}
  begin
  {$IFNDEF WIN32}
    Len := GetSelTextBuf(@Result[1], 255);
    Result[0] := Char(Len)
  {$ELSE}
    if SelLength=0 then Result:= ''
                   else
                     if fBlockSelection then Result:= SelectedBlockText
                     else
                       begin
                       SetLength(Result, Abs(SelLength));
                       GetSelTextBuf(PChar(Result), Abs(SelLength)+1)
                       end;
  {$ENDIF}
  end;


procedure TPlusMemo.SetSelText(const s: string);      { property access methods}
  var s1: string; {$IFNDEF WIN32} t: PChar; {$ENDIF}
  begin
  if (fExtraCols>0) and (pmoPutExtraSpaces in Options) and ((not fBlockSelection) or (fSelLen=0)) then
    begin
    {$IFNDEF WIN32} if fExtraCols>255 then fExtraCols:= 255; {$ENDIF}
    SetLength(s1, fExtraCols);
    FillChar(s1[1], fExtraCols, Ord(' '))
    end
  else s1:= '';
  {$IFDEF WIN32}
    SetSelTextBuf(PChar(s1+s))
  {$ELSE}
    t:= StrAlloc(520);
    StrPCopy(t, s1);
    System.Move(s[1], t[Length(s1)], Length(s));
    t[Length(s)+Length(s1)]:= #0;
    SetSelTextBuf(t);
    StrDispose(t)
  {$ENDIF}
  end;

procedure TPlusMemo.SetSelLength(l: Longint);         { property access methods}
  var  start, stop: Longint; cp: Longint;
  begin
  fBlockSelection:= False;
  if l=fSelLen then exit;
  cp:= fcp.Pos;
  if cp+l<0 then l:= -cp;
  if cp+l>fTextLen then l:= fTextLen-cp;
  if fSelLen>0 then start:= fSelStop.LineNumber
               else start:= fSelStart.LineNumber;

  fSelLen:= l;
  if fSelLen>0 then
    begin
    fSelStart.Pos:= cp;
    fSelStop.Pos:= cp+fSelLen;
    fcp:= fSelStart;
    stop:= fSelStop.LineNumber
    end
  else
    begin
    fSelStart.Pos:= cp+fSelLen;
    fSelStop.Pos:= cp;
    fcp:= fSelStop;
    stop:= fSelStart.LineNumber
    end;

  fUndoBreak:= True;
  if (not fNoPaint) and HandleAllocated then InvalidateLines(start, stop, False);
  end;

procedure TPlusMemo.setBorderStyle(b: TBorderStyle); { property access methods }
  begin
  if b<>fBorderStyle then
    begin
    fBorderStyle:= b;
    RecreateWnd
    end
  end;

procedure TPlusMemo.setEndOfTextMark(p: TPen);        { property access methods}
  begin
  fEndOfTextPen.Assign(p)
  end;

procedure TPlusMemo.setSpecUnderline(p: TPen);        { property access methods}
  begin
  fSpecUnderlinePen.Assign(p)
  end;

procedure TPlusMemo.setLeftMargin(lm: Integer);      { property access methods }
  begin
  if lm<>fLeftMargin then
    begin
    fLeftMargin:= lm;
    RefreshDisplay
    end
  end;

procedure TPlusMemo.setRightMargin(rm: Integer);     { property access methods }
  begin
  if rm<>fRightMargin then
    begin
    fRightMargin:= rm;
    RefreshDisplay
    end
  end;

procedure TPlusMemo.SetTabStops(t: Integer);         { property access methods }
  begin
  if t<>fTabStops then
    begin
    fTabStops:= t;
    RefreshDisplay
    end
  end;

procedure TPlusMemo.SetWordWrap(ww: Boolean);        { property access methods }
  begin
  if ww<>fWordWrap then
    begin
    fWordWrap:= ww;
    RefreshDisplay;
    if HandleAllocated then SetHScrollParams;
    LeftOrigin:= 0;
    ScrollInView;
    if assigned(fOnWordwrapChange) then fOnWordwrapChange(Self);
    end
  end;

procedure TPlusMemo.SetHTColor(c: TColor);           { property access methods }
  begin
  if c<>fHTColor then
    begin
    fHTColor:= c;
    if HandleAllocated then Invalidate
    end
  end;

procedure TPlusMemo.setHBColor(c: TColor);           { property access methods }
  begin
  if c<>fHBColor then
    begin
    fHBColor:= c;
    Invalidate
    end
  end;

procedure TPlusMemo.setHideSelection(h: Boolean);    { property access methods }
  begin
  if h<>fHideSelection then
    begin
    fHideSelection:= h;
    if HandleAllocated and (not Focused) then InvalidateLines(fSelStart.LineNumber, fSelStop.LineNumber, False)
    end
  end;

procedure TPlusMemo.setSelBackColor(bc: TColor);
  begin
  if bc<>fSelBackColor then
    begin
    fSelBackColor:= bc;
    if HandleAllocated then InvalidateLines(fSelStart.LineNumber, fSelStop.LineNumber, False)
    end
  end;

procedure TPlusMemo.setSelTextColor(tc: TColor);
  begin
  if tc<>fSelTextColor then
    begin
    fSelTextColor:= tc;
    if HandleAllocated then InvalidateLines(fSelStart.LineNumber, fSelStop.LineNumber, False)
    end
  end;

function TPlusMemo.getChar(i: Longint): Char;        { property access methods }
  begin
  ftmpnav2.Pos:= i;
  Result:= ftmpnav2.Text
  end;

function TPlusMemo.GetCurrentWord: string;
  var founddel: Boolean; stopword, cp, wlen: PlusCardinal;
  begin
  with fcp.Par^ do
    begin
    cp:= fcp.ParOffset;
    while (cp<Length) and (not (Text[cp] in Delimiters)) do Inc(cp);
    stopword:= cp;
    founddel:= False;
    while (cp>0) and (not founddel) do
      begin
      Dec(cp);
      founddel:= Text[cp] in Delimiters;
      if founddel then Inc(cp)
      end;

    wlen:= stopword-cp;
    {$IFNDEF WIN32}
    if wlen>255 then wlen:= 255;
    {$ENDIF}
    SetLength(Result, wlen);
    Move(Text[cp], Result[1], wlen)
    end
  end;


function  TPlusMemo.getLines(i: Longint): string;    { property access methods }
  var l: PlusCardinal;
  begin
  if i<0 then begin Result:= ''; Exit end;

  repeat
    ftmpnav1.LineNumber:= i;
    FormatNow(fUpdateStartPar, ftmpnav1.ParNumber, False)
  until (ftmpnav1.LineNumber=i) or (fUpdateStartPar>=fParagraphs.Count);

  if i>=fLineCount then Result:= ''
  else
    with ftmpnav1.Par^, ftmpnav1.NavLines.LinePointers[i-StartLine]^ do
         begin
         l:= Stop-Start;
         {$IFNDEF WIN32}
           if l>255 then l:= 255;
         {$ENDIF}
         SetLength(Result, l);
         Move(Text[Start], Result[1], l)
         end
  end;


{ property access methods }

function  TPlusMemo.getParagraphs(i: Longint): string;
  var l: PlusCardinal;
  begin
  with fParagraphs.ParPointers[i]^ do
    begin
    l:= Length;
    {$IFNDEF WIN32}
      if l>255 then l:= 255;
    {$ENDIF}
    SetLength(Result, l);
    Move(Text^, Result[1], l)
    end
  end;

{ property access methods }

procedure TPlusMemo.setParagraphs(i: Longint; const par: string);
  {$IFNDEF WIN32} var parbuf: PChar; {$ENDIF}
  begin
  {$IFDEF WIN32}
  PargrphBuf[i]:= PChar(par)
  {$ELSE}
  parbuf:= StrAlloc(Length(par)+2);
  StrPCopy(parbuf, par);
  PargrphBuf[i]:= parbuf;
  StrDispose(parbuf)
  {$ENDIF}
  end;


procedure TPlusMemo.setLines(Value: TStrings);       { property access methods }
  begin
  fLines.Assign(Value)
  end;

function  TPlusMemo.getLinesBuf(i: Longint): PChar;  { property access methods }
  begin
  repeat
    ftmpnav1.LineNumber:= i;
    FormatNow(fUpdateStartPar, ftmpnav1.ParNumber, False)
  until (ftmpnav1.LineNumber=i) or (fUpdateStartPar>=fParagraphs.Count);

  if (i>=fLineCount) or (i<0) then Result:= nil
  else
    with ftmpnav1.Par^, ftmpnav1.NavLines.LinePointers[i-StartLine]^ do
         begin
         Result:= StrAlloc(Stop-Start+1);
         Move(Text[Start], Result^, Stop-Start);
         Result[Stop-Start]:= #0
         end
  end;

function  TPlusMemo.getParsBuf(i: Longint): PChar;   { property access methods }
  begin
  with fParagraphs.ParPointers[i]^ do
    begin
    Result:= StrAlloc(Length+1);
    if Length>0 then Move(Text^, Result[0], Length+1)
                else Result[0]:= #0
    end
  end;


procedure TPlusMemo.setParsBuf(i: Longint; parg: PChar);{ property access methods }
  var sstart, slen, spar, oldparlen: Longint;
  begin
  spar:= SelPar;
  if SelPar=i then begin SelStart:= PargrphOffset[i]; SelLength:= 0 end;
  sstart:= SelStart;
  slen:= SelLength;
  fNoPaint:= True;
  Inc(fLockedCount);
  while i>=ParagraphCount do
    begin     {  put empty paragraphs }
    SelLength:= 0;
    SelStart:= CharCount;
    SelText:= #13#10
    end;

  with fParagraphs.ParPointers[i]^ do
      begin
      SelStart:= StartOffset;
      oldparlen:= Length;
      SelLength:= Length;
      SetSelTextBuf(parg)
      end;

  fNoPaint:= False;
  Dec(fLockedCount);
  if fLockedCount=0 then EndModifications(Self);
  if spar>i then
        SelStart:= sstart + fParagraphs[i].Length - oldparlen
  else SelStart:= sstart;
  if spar<>i then SelLength:= slen
  end;


procedure TPlusMemo.setStaticFormat(ssf: Boolean);
  var tmpstream: TMemoryStream;
  begin
  if ssf<>fStaticFormat then
    begin
    fStaticFormat:= ssf;
    tmpstream:= TMemoryStream.Create;
    SaveToStream(tmpstream);
    tmpstream.Position:= 0;
    LoadFromStream(tmpstream);
    tmpstream.Free
    end
  end;

procedure TPlusMemo.setBackground(pic: TPicture);
  begin
  fBackground.Assign(pic)
  end;

    { property access methods }

function TPlusMemo.getParsOffset(i: Longint): Longint;
  begin
  Result:= fParagraphs.ParPointers[i]^.StartOffset
  end;

procedure TPlusMemo.setSelPar(par: Longint);         { property access methods }
  begin
  if par>=fParagraphs.Count then par:= fParagraphs.Count-1;
  SelLength:= 0;
  SelStart:= fParagraphs.ParPointers[par]^.StartOffset
  end;

function TPlusMemo.getSelPar: Longint;
  begin
  Result:= fcp.ParNumber
  end;

procedure TPlusMemo.setSelStart(ss: Longint);        { property access methods }
  begin
  SelLength:= 0;
  fExtraCols:= 0;
  fXCaretRunningPos:= Low(fXCaretRunningPos);
  if ss<0 then ss:= 0;
  if ss>fTextLen then ss:= fTextLen;
  fSelStart.Pos:= ss;
  fSelStop.Assign(fSelStart);
  fUndoBreak:= True;
  if HandleAllocated then UpdateCaret(False)
  end;

function TPlusMemo.getSelStart: Longint;
  begin
  Result:= fcp.Pos
  end;

procedure TPlusMemo.setSelLine(line: Longint);       { property access methods }
  begin
  HandleNeeded;
  ftmpnav1.Assign(fcp);
  repeat
    ftmpnav1.LineNumber:= line;
    FormatNow(fUpdateStartPar, ftmpnav1.ParNumber, False)
  until (ftmpnav1.LineNumber=line) or (fUpdateStartPar>=fParagraphs.Count);
  SelStart:= ftmpnav1.Pos
  end;

function TPlusMemo.getSelLine: Longint;
  begin
  Result:= fcp.LineNumber
  end;

procedure TPlusMemo.setSelCol(col: PlusCardinal);    { property access methods }
  var l: Longint;
  begin
  l:= SelLength;
  SelLength:= 0;
  fSelStart.Col:= col;
  fSelStop.Assign(fSelStart);
  SelLength:= l;
  UpdateCaret(False)
  end;

function TPlusMemo.getSelCol: PlusCardinal;
  begin
  Result:= fcp.Col+fExtraCols
  end;

function TPlusMemo.GetSelContext: Integer;           { property access methods }
  begin
  Result:= fcp.Context
  end;

function TPlusMemo.GetColumnBlockXY;
  begin
  if not fBlockSelection then Result:= Rect(0, 0, 0, 0)
  else
    begin
    if fBlockStartCol<fBlockStopCol then
      begin
      Result.Left:= fBlockStartCol;
      Result.Right:= fBlockStopCol
      end
    else
      begin
      Result.Left:= fBlockStopCol;
      Result.Right:= fBlockStartCol
      end;
    Result.Top:= fSelStart.LineNumber;
    Result.Bottom:= fSelStop.LineNumber
    end
  end;



procedure TPlusMemo.setfAltFont(f: TFont);           { property access methods }
  begin
  fAltFont.Assign(f);
  RefreshDisplay
  end;

procedure TPlusMemo.setScrollBars(s: TScrollStyle);  { property access methods }
  begin
  if s<> ScrollBars then
     begin
     fNoFormat:= (s=ssVertical) or (s=ssBoth) = fVScrollBar;
     fVScrollBar:= (s=ssVertical) or (s=ssBoth);
     fHScrollBar:= (s=ssHorizontal) or (s=ssBoth);
     if HandleAllocated then RecreateWnd;
     fNoFormat:= False
     end
  end;

function TPlusMemo.getScrollBars: TScrollStyle;      { property access methods }
  begin
  if fVScrollBar and fHScrollBar then Result:= ssBoth
  else if fVScrollBar then Result:= ssVertical
                      else if fHScrollBar then Result:= ssHorizontal
                                          else Result:= ssNone
  end;

function TPlusMemo.getVersion: string;               { property access methods }
  begin
  Result:= Copy(IdSn, 10, 5) + ' ' + Copy(IdSn, 27, 16)
  end;

procedure TPlusMemo.setVersion(const v: string);     { property access methods }
  begin
  end;

procedure TPlusMemo.setCaretWidth(w: Integer);       { property access methods }
  begin
  if w=0 then
    begin
    fAutoCaretWidth:= True;
    fCaretWidth:= fLineHeight div 14 + 1
    end
  else begin
       fAutoCaretWidth:= False;
       fCaretWidth:= w
       end;
  if Focused then
    begin
    DestroyCaret;
    CreateCaret(Handle, 0, GetCaretWidth, fLineHeight-1);
    PlaceCaret;
    ShowCaret(Handle)
    end
  end;

procedure TPlusMemo.setDelimiters(const d: TSetOfChar);  { property access methods}
  begin
  fDelimiters:= d;
  if fStartStopKeys<>nil then fStartStopKeys.DelChecked:= False;
  if csDesigning in ComponentState then ReApplyKeywords
  end;

procedure TPlusMemo.setApplyKeywords(apply: Boolean);   { property access methods}
  begin
  if apply<>fApplyKeywords then
    begin
    fApplyKeywords:= apply;
    if csDesigning in ComponentState then ReApplyKeywords
    end
  end;

procedure TPlusMemo.setApplyStartStopKeys(apply: Boolean);  { property access methods}
  begin
  if apply<>fApplyStartStopKeys then
    begin
    fApplyStartStopKeys:= apply;
    if csDesigning in ComponentState then ReApplyKeywords
    end
  end;

function  TPlusMemo.getSeparators: string;            { property access methods}
  var i: Char;
  begin
  Result:= '';
  for i:= #0 to #255 do
          if i in Delimiters then Result:= Result + i
  end;

procedure TPlusMemo.setSeparators(const s: string);   { property access methods}
  var i: Integer; del: TSetOfChar;
  begin
  del:= [];
  for i:= 1 to Length(s) do del:= del + [s[i]];
  Delimiters:= del
  end;

procedure TPlusMemo.setOptions(opt: TPlusMemoOptions);{ property access methods}
  var reform, reshowsel: Boolean; smod: Boolean;

  function seloptions(const optset: TPlusMemoOptions): TPlusMemoOptions;
    begin
    Result:= optset * [pmoWindowsSelColors, pmoFullLineSelect]
    end;

  var tmpstream: TMemoryStream;

  begin
  if opt<>fOptions then
    begin
    reform:= (pmoAutoScrollBars in opt) <> (pmoAutoScrollBars in fOptions);
    reshowsel:= seloptions(opt) <> seloptions(fOptions);

    if (pmoDiscardTrailingSpaces in opt) and (not (pmoDiscardTrailingSpaces in fOptions)) then
      begin
      fOptions:= opt;
      { remove existing trailing spaces }
      smod:= Modified;
      tmpstream:= TMemoryStream.Create;
      SaveToStream(tmpstream);
      tmpstream.Position:= 0;
      LoadFromStream(tmpstream);
      tmpstream.Free;
      Modified:= smod
      end
    else
      begin
      fOptions:= opt;
      if HandleAllocated then
        if reform then begin if csDesigning in ComponentState then ReApplyKeywords end
                  else
                    if reshowsel and (fSelLen<>0) then
                        InvalidateLines(fSelStart.LineNumber, fSelStop.LineNumber, False)
      end
    end
  end;

procedure TPlusMemo.setOverwrite(ovr: Boolean);       { property access methods}
  begin
  if ovr<>fOverwrite then
    begin
    fOverwrite:= ovr;
    if Focused and (not DisplayOnly) then UpdateCaret(True)
    end
  end;

procedure TPlusMemo.setHighlighter(aHighlighter: TPlusHighlighter);
  begin
  if fHighlighter<>nil then fHighlighter.MemoList.Remove(Self);
  fHighlighter:= aHighlighter;
  if aHighlighter<>nil then aHighlighter.MemoList.Add(Self);
  if ComponentState*[csDesigning, csLoading]<>[] then ReApplyKeywords
  end;


procedure TPlusMemo.setUndoMaxLevel(uml: Integer);    { property access methods}
  var i: Integer;
  begin
  if uml>MaxListSize then raise EListError.Create('Undo set too high');
  for i:= fUndoList.Count-1 downto MaxOf(fUndoLevel, uml) do RemoveUndo(i);
  while fUndoLevel>uml do RemoveUndo(0);
  fUndoMaxLevel:= uml
  end;

procedure TPlusMemo.setUndoMaxSpace(ums: Longint);    { property access methods}
  begin
  while (fUndoList.Count>fUndoLevel) and (fUndoTotalSize>ums) do RemoveUndo(fUndoList.Count-1);

  while (fUndoTotalSize>ums) and (fUndoList.Count>0) do
    RemoveUndo(0);

  fUndoMaxSpace:= ums
  end;

procedure TPlusMemo.setUpdateMode(um: TUpdateMode);   { property access methods}
  var dummy: TMessage;
  begin
  if um<>fUpdateMode then
    begin
    fUpdateMode:= um;
    if HandleAllocated then
      case um of
        umImmediate: FormatNow(fUpdateStartPar, fUpdateStopPar, False);
        umBackground: pmUpdateBkg(dummy)
        end
    end
  end;

procedure TPlusMemo.setUpperCaseType(ut: TpmUpperCase);  { property access methods }
  begin
  if ut<>fUpperCaseType then
    begin
      fUpperCaseType:= ut;
      if fKeywords<>nil then fKeywords.UpperCaseType:= ut;
      if fStartStopKeys<>nil then fStartStopKeys.UpperCaseType:= ut;
      if csDesigning in ComponentState then ReApplyKeywords
    end
  end;

procedure TPlusMemo.setAlignment(al: TAlignment);    { property access methods }
  begin
  if al<>fAlignment then
    begin
    fDisplayLeft:= 0;
    if (not WordWrap) and ((fAlignment in [taRightJustify, taCenter]) or
                           (al in [taRightJustify, taCenter])) then
      begin
      fAlignment:= al;
      RefreshDisplay
      end
    else
      begin
      fAlignment:= al;
      if HandleAllocated then
        begin
        UpdateCaret(False);
        Invalidate
        end
      end
    end
  end;

procedure TPlusMemo.setJustified(j: Boolean);        { property access methods }
  begin
  if j<>fJustified then
    begin
    fDisplayLeft:= 0;
    fJustified:= j;
    if HandleAllocated then
      begin
      UpdateCaret(False);
      Invalidate
      end
    end
  end;


function TPlusMemo.getCanUndo: Boolean;              { property access methods }
  begin
  if fInUndo then Result:= fUndoLevel>1
             else Result:= fUndoLevel>0
  end;

function TPlusMemo.getCanRedo: Boolean;               { property access methods}
  begin
  if fInUndo then Result:= fUndoLevel<=fUndoList.Count
             else Result:= fUndoLevel<fUndoList.Count
  end;

function TPlusMemo.getUndoCount: Integer;             { property access methods}
  begin
  Result:= fUndoList.Count
  end;

function TPlusMemo.getUndoList(i: Integer): TUndoRecord;   { property access methods}
  var orp: ^OffsetRangeRecord;
  begin
  orp:= fUndoList[i];
  with Result do
    begin
    UndoStart:= orp^.Start;
    UndoStop:= orp^.Stop;
    UndoText:= PChar(orp)+SizeOf(OffsetRangeRecord)
    end
  end;

procedure TPlusMemo.setTopLine(tl: Longint);         { property access methods }
  begin
  TopOrigin:= tl*fLineHeight
  end;

procedure TPlusMemo.SetTopLeft(NewTopOrigin, NewLeftOrigin: Longint);
  var xscroll, yscroll: Longint;
      vis             : Boolean;
  begin
  vis:= HandleAllocated;
  if NewLeftOrigin<>fDisplayLeft then
    begin
    xscroll:= fDisplayLeft-NewLeftOrigin;
    Inc(fCaretX, xscroll);
    if fXCaretRunningPos<>Low(fXCaretRunningPos) then Inc(fXCaretRunningPos, xscroll);
    fDisplayLeft:= NewLeftOrigin;
    if vis and fHScrollBar then SetScrollPos(Handle, SB_HORZ, fDisplayLeft div fHScrollfact, True)
    end
  else
    xscroll:= 0;

  if NewTopOrigin<>fTopOrigin then
    begin
    yscroll:= fTopOrigin-NewTopOrigin;
    fTopOrigin:= NewTopOrigin;
    if vis then fDisplayTop.LineNumber:= fTopOrigin div fLineHeight;
    Inc(fCaretY, yscroll);
    if vis and fVScrollBar then SetScrollPos(Handle, SB_VERT, fTopOrigin div fVScrollFact, True)
    end
  else
    yscroll:= 0;

  if vis and ((xscroll<>0) or (yscroll<>0)) then
      begin
      if (xscroll>32767) or (xscroll<-32768) or (yscroll>32767) or (yscroll<-32768) then
        Invalidate
      else
        ScrollWindow(Handle, xscroll, yscroll, nil, nil);
      if Focused then PlaceCaret;
      if (xscroll<>0) and Assigned(fOnHScroll) then fOnHScroll(Self);
      if (yscroll<>0) then
        begin
        if fMsgList.Count>0 then Perform(pm_VScroll, 0, 0);
        if Assigned(fOnVScroll) then fOnVScroll(Self)
        end
      end;
  end;


procedure TPlusMemo.setTopOrigin(top: Longint);       { property access methods}
  begin
  SetTopLeft(top, fDisplayLeft)
  end;

function TPlusMemo.getTopLine: Longint;               { property access methods}
  begin
  Result:= fDisplayTop.LineNumber
  end;

procedure TPlusMemo.setDisplayLeft(dl: Integer);     { property access methods }
  begin
  SetTopLeft(fTopOrigin, dl)
  end;

procedure TPlusMemo.setDisplayOnly(d: Boolean);      { property access methods }
  begin
  if d<>fDisplayOnly then
    begin
    if (not fDisplayOnly) and Focused then DestroyCaret;
    fDisplayOnly:= d;
    if (not fDisplayOnly) and Focused then
      begin
      CreateCaret(Handle, 0, GetCaretWidth, fLineHeight);
      PlaceCaret;
      ShowCaret(Handle)
      end
    end
  end;

procedure TPlusMemo.setLineHeight(lh: Integer);       { property access methods}
  begin
  if lh>0 then
    begin
    fLineHeight:= lh;
    fAutoLineHeight:= False;
    end
  else fAutoLineHeight:= True;

  fNoFormat:= True;
  Perform(CM_FONTCHANGED, 0, 0);  { update all things which depend on font size and line height }
  fNoFormat:= False
  end;

function TPlusMemo.getLineHeight: Integer;            { property access methods}
  begin
  if csDesigning in ComponentState then
    if fAutoLineHeight then Result:= 0
                       else Result:= fLineHeight
  else Result:= fLineHeight
  end;

function TPlusMemo.getParBackgnd(i: Longint): TColor;
  begin
  Result:= fParagraphs.ParPointers[i]^.Backgnd
  end;

function TPlusMemo.getParForegnd(i: Longint): TColor;
  begin
  Result:= fParagraphs.ParPointers[i]^.Foregnd
  end;

procedure TPlusMemo.setParBackgnd(i: Longint; c: TColor);
  begin
  with fParagraphs.ParPointers[i]^ do
    begin
    Backgnd:= c;
    if HandleAllocated then InvalidateLines(StartLine, StartLine+NbLines-1, False)
    end
  end;

procedure TPlusMemo.setParForegnd(i: Longint; c: TColor);
  begin
  with fParagraphs.ParPointers[i]^ do
    begin
    Foregnd:= c;
    if HandleAllocated then InvalidateLines(StartLine, StartLine+NbLines-1, False)
    end
  end;


 { protected methods }
procedure TPlusMemo.RemoveUndo(i: Integer);
  var up: PChar;
  begin
  up:= fUndoList[i];
  Dec(fUndoTotalSize, StrBufSize(up));
  StrDispose(up);
  fUndoList.Delete(i);
  if i<fUndoLevel then Dec(fUndoLevel)
  end;

procedure TPlusMemo.PlaceCaret;
  var cpos: Integer;
  begin
  if WordWrap and (fCaretX>=fLineBmpWidth) then cpos:= fLineBmpWidth-1
                                           else cpos:= fCaretX;
  SetCaretPos(cpos, fCaretY)
  end;

procedure TPlusMemo.UpdateCaret(Recreate: Boolean);        { protected methods }
  var y: Longint; oldstyle: TExtFontStyles;
  begin
  if fLockedCount>0 then Exit;
  if HandleAllocated then
    begin
    FormatNow(fcp.ParNumber, fcp.ParNumber, False);
    fCaretX:= ColPos(fcp.Par, fcp.fParNb, fcp.ParLine, fcp.Col)+fExtraCols*fSpaceWidth;
    fWideCaret:= fLastPosWidth
    end;

  y:= LinePos(fcp.LineNumber-fDisplayTop.LineNumber);
  fCaretY:= y;

  if Focused then
    begin
    if Recreate or (Overwrite and (pmoWideOverwriteCaret in Options)) then
      begin
      DestroyCaret;
      CreateCaret(Handle, 0, GetCaretWidth, fLineHeight);
      ShowCaret(Handle)
      end;
    PlaceCaret
    end;

  oldstyle:= fsStyle;
  fsStyle:= GetCurrentStyle;
  if fsPos<>fcp.Pos then
    begin
    Perform(pm_SelMove, 0, 0);
    if Assigned(fOnMove) then fOnMove(Self)
    end;
  if (fsStyle<>oldstyle) and Assigned(fOnAttrChange) then fOnAttrChange(Self);
  fsPos:= fcp.Pos;
  end;


procedure TPlusMemo.ExtendMods(startpar: Longint; startline: PlusCardinal; stoppar: Longint);
  var newstartline: Longint;
  begin
  if fModStopPar=-1 then
    begin
    fModStartPar:= fParagraphs.Count;
    fModStartLine:= fLineCount
    end;
  if startpar<=fModStartPar then
    begin
    fModStartPar:= startpar;
    newstartline:= fParagraphs[startpar].StartLine+startline;
    if newstartline<fModStartLine then fModStartLine:= newstartline
    end;
  if stoppar>fModStopPar then fModStopPar:= stoppar;
  if fLastStartStopParsed>=startpar then fLastStartStopParsed:= startpar-1;
  fNoCompleteFormat:= False
  end;

procedure TPlusMemo.Reformat;                              { protected methods }
  { Reformat the lines according to the width of the control }
  var i: Longint;
  begin
  fMaxLineWidth:= 0; fMaxLineNumber:= -1; fStartLineSelection:= -1;
  for i:= 0 to fParagraphs.Count-1 do
            pParInfo(fParagraphs.Pointers[i])^.Formatted:= False;
  fUpdateStartPar:= 0;
  fUpdateStopPar:= fParagraphs.Count-1;
  if (csReading in ComponentState) or (fLockedCount>0) then Exit;
  {$IFDEF PMDEBUG}
      checkintegrity;
  {$ENDIF}

  if (fCanvas<>nil) or ((csDesigning in ComponentState) and HandleAllocated) then
    FormatNow(0, fParagraphs.Count-1, True)
  else
    if HandleAllocated then
      case fUpdateMode of
        umImmediate: begin
                     FormatNow(0, fParagraphs.Count-1, True);
                     fUpdateStartPar:= fParagraphs.Count;
                     fUpdateStopPar:= -1
                     end;
        umBackground: if not fUpdating then
                        begin
                        fUpdating:= True;
                        PostMessage(Handle, PM_UpdateBkg, 1, 0)
                        end;
      end;
  {$IFDEF PMDEBUG}
      checkintegrity
  {$ENDIF}
  end;      { method Reformat }


procedure TPlusMemo.FormatNow(StartPar, StopPar: Longint; Unconditional: Boolean); { protected methods }
  { call only when Handle is allocated, or with fCanvas<>nil }
  { Reformat the lines according to the width of the control if WordWrap is True, for parg StartPar to StopPar }
  var i               : Longint;             { paragraph loop index }
      dummy1, dummy2  : PlusCardinal;        { used in call to ReformatPar }
      oldfont, hand   : THandle;             { previous font handle of DC, handle of DC to use }
      w, soldmaxwidth : Integer;             { formatting width, old value of fMaxLineWidth }
      apar            : pParInfo;            { pointer used in paragraph loop }
      loffset,                               { number of lines added while running the loop }
      nextstartline   : Longint;             { running starting line number for paragraphs }
      oldlinecount    : PlusCardinal;        { previous line count of paragraph (before formatting) }
      initdone        : Boolean;             { whether any paragraph has been formatted }
      lastnotify,                            { last time OnProgress was called }
      savedupdate     : Longint;             { previous value of fUpdateStartPar }
      markUpdateCaret : Boolean;             { whether we need to update the caret position after the loop }
      newtoporigin    : Longint;
  begin
  if StopPar>=fParagraphs.Count then StopPar:= fParagraphs.Count-1;
  if StartPar>StopPar then Exit;

  lastnotify:= GetTickCount;
  fNoPaint:= True;            { avoid paint messages to be effective,
                                so that Application.ProcessMessages can be done inside of OnProgress }
  soldmaxwidth:= fMaxLineWidth;

  if fCanvas=nil then
    begin
    hand:= Canvas.Handle;
    w:= ClientWidth
    end
  else
    begin
    hand:= fCanvas.Handle;
    w:= fw
    end;

  loffset:= 0;
  initdone:= False;
  markUpdateCaret:= False;
  savedupdate:= fUpdateStartPar;
  nextstartline:= 0;   { to avoid a warning }

  for i:= StartPar to StopPar do
    begin
    ParseStartStopNow(i);
    apar:= fParagraphs.Pointers[i];
    if i>StartPar then apar^.StartLine:= nextstartline
                  else nextstartline:= apar^.StartLine;

    if Unconditional or (not apar^.Formatted) then
      begin
      if Assigned(fOnProgress) then
        if Longint(GetTickCount)-lastnotify>ProgressInterval then
          begin
          fCompleted:= (i*100) div fParagraphs.Count;
          if fCompleted=100 then fCompleted:= 99;
          if initdone then oldfont:= SelectObject(hand, oldfont);
          fOnProgress(Self);
          if initdone then oldfont:= SelectObject(hand, oldfont);
          lastnotify:= GetTickCount
          end;
      oldlinecount:= apar^.NbLines;
      dummy1:= 0;
      ReformatPar(hand, not initdone, w, apar, i, dummy1, dummy2, True, oldfont);
      initdone:= True;
      Inc(loffset, apar^.NbLines-oldlinecount);
      if i=fcp.ParNumber then
          begin
          fcp.fParLine:= High(PlusCardinal);
          markUpdateCaret:= True
          end
      end;

    fUpdateStartPar:= i+1;
    Inc(nextstartline, apar^.NbLines)
    end;  { for i:= StartPar to StopPar }

  if initdone then
    begin
    if fStartLineSelection>nextstartline then Inc(fStartLineSelection, loffset)
                                         else fStartLineSelection:= -1;
    InvalidateNavs(fNavigators, fParagraphs.ParPointers[StartPar]^.StartOffset, StopPar);
    DeleteObject(SelectObject(hand, oldfont));
    if loffset<>0 then
      begin
      if StartPar<=fDisplayTop.ParNumber then
        begin
        newtoporigin:= fDisplayTop.LineNumber * fLineHeight + fTopOrigin mod fLineHeight;
        if newtoporigin<>fTopOrigin then
          begin
          fTopOrigin:= newtoporigin;
          if fVScrollBar then SetScrollPos(Handle, SB_VERT, fTopOrigin div fVScrollFact, True)
          end
        end;

      for i:= StopPar+1 to fParagraphs.Count-1 do
        Inc(fParagraphs.ParPointers[i]^.StartLine, loffset);

      if (not WordWrap) and (fMaxLineNumber>StopPar) then Inc(fMaxLineNumber, loffset);
      if fCanvas=nil then
        begin
        SetVScrollParams;
        if fVScrollChange then
          begin
          fNoPaint:= False;
          Exit
          end
        end
      end;

    if fCanvas=nil then
      begin
      if markUpdateCaret then UpdateCaret(False);
      if (fMaxLineWidth<>soldmaxwidth) and (not WordWrap) then
        begin
        SetHScrollParams;
        if fHScrollBar then
             SetScrollPos(Handle, SB_HORZ, fDisplayLeft div fHScrollfact, True);
        end
      end
    end;

  fNoPaint:= False;
  if startpar<>savedUpdate then fUpdateStartPar:= savedUpdate
  {$IFDEF WIN32}
    else
      if Assigned(fOnProgress) and (fUpdateStartPar>=fParagraphs.Count) then
        begin
        fCompleted:= 100;
        fOnProgress(Self)
        end;
  {$ENDIF}
  end;      { method FormatNow }


procedure TPlusMemo.ParseStartStopNow(ToReach: Longint);
  var i: Longint; spar: pParInfo;
  begin
  i:= fLastStartStopParsed+1;
  while i<=ToReach do
    begin
    spar:= fParagraphs.Pointers[i];
    if not spar^.StartStopDone then
      begin
      with fpnav1 do
        begin
        fPar:= spar;
        fParNb:= i;
        fPos:= sPar^.StartOffset;
        fOffset:= 0;
        fDynNb:= 0;
        fParLine:= 0;
        if fNavLines<>nil then fNavLines.LLPar:= par
        end;
      fpnav2.Assign(fpnav1);
      fpnav2.Pos:= fpnav1.Pos+sPar^.Length;
      ApplyStartStopKeyList(fpnav1, fpnav2)
      end;
    Inc(i)
    end;
  fLastStartStopParsed:= i-1;
  fpnav1.Pos:= 0;
  fpnav2.Assign(fpnav1)
  end;


{ protected methods }

procedure TPlusMemo.ReformatPar(DC: HDC; InitDC: Boolean; Width: Integer;
                                Par: pParInfo;
                                frmParNb: Longint;
                                var Line, LastLineChanged: PlusCardinal;
                                CompleteReformat: Boolean;
                                var OldFontH: THandle);
  { reformat a paragraph starting from Line;
    Lines are given start/stop values here, as well as width and start attrib.;
    return in LastLineChanged the number of last line whose start/stop values
    have changed, to help determine which lines need update on the display;
    If CompleteReformat is false, the method returns as soon as start/stop
    values appear in sync with values before beginning formatting.  If True,
    the method completes the paragraph until its end. }

  begin
  ReformatParP(Self, DC, InitDC, Width, Par, frmParNb, fMouseNav.NavLines, Line, LastLineChanged,
                             CompleteReformat, OldFontH);
  fMouseNav.fNavLines.LLPar:= fMouseNav.fPar
  { This method has been moved in Plussup.INC to make room in D1 }
  end;  { method ReformatPar }


procedure TPlusMemo.Change;                                { protected methods }
  begin
  if fMsgList.Count>0 then Perform(CM_TEXTCHANGED, 0, 0);
  if Assigned(fOnChange) then fOnChange(Self)
  end;


procedure TPlusMemo.ApplyKeywordsList(Start, Stop: TPlusNavigator);
  begin
  { moved in Plussup.inc to make room under Delphi 1 }
  ApplyKeywordsListP(Start, Stop)
  end;

procedure TPlusMemo.ApplyStartStopKeyList(Start, Stop: TPlusNavigator);     { protected methods }
  begin
  ApplyStartStopKeyListP(Start, Stop, tmpnav1, tmpnav2, tmpnav3)
  { moved in Plussup.inc to make room under Delphi 1 }
  end;

{ protected methods }

procedure TPlusMemo.SetDynStyleP(Start, Stop: TPlusNavigator; dinfo: DynInfoRec; AddDInfo, ExtendModFields: Boolean);
  var startpar, stoppar: Longint;
      i: Longint;
      d: TPlusNavigator;
      stopdyninfo: DynInfoRec;
      stoppos, lim: Longint;
      currentpar: pParInfo;
      rundyn: pDynInfoRec;
  begin
  if Start.Pos>Stop.Pos then begin d:= Stop; Stop:= Start; Start:= d end;
  startpar:= Start.ParNumber;
  stoppar:= Stop.ParNumber;

  if ExtendModFields then ExtendMods(startpar, Start.ParLine, stoppar);

  stoppos:= Stop.Pos;

  stopdyninfo:= Start.DynAttr;
  if AddDInfo then dinfo.DynStyle:= dinfo.DynStyle or $80
              else dinfo.DynStyle:= 0;
  if AddDInfo or (stopdyninfo.DynStyle and $80<>0) then Start.AddDyn(dinfo);

  Stop.Assign(Start);
  currentpar:= Start.fPar;
  rundyn:= nil;

 for i:= startpar to stoppar do
    begin
    if i>startpar then currentpar:= fParagraphs.Pointers[i];
    with currentpar^ do
      begin
      Formatted:= False;
      if i<>startpar then
        with Stop do
          begin
          Stop.fParNb:= i;
          Stop.fPar:= currentpar;
          Stop.fPos:= StartOffset;
          Stop.fDynNb:= 0;
          Stop.fOffset:= 0;
          Stop.fParLine:= High(PlusCardinal)
          end;

      if i=stoppar then lim:= stoppos else lim:= StartOffset + Length;

      while Stop.ForwardToDyn(lim) do
        begin
        stopdynInfo:= DynBuffer^[Stop.fDynNb];
        Stop.RemoveDyn
        end;

      if i<>startpar then
        begin
        RemoveRef(StartDynAttrib);
        if i=startpar+1 then
          begin
          New(StartDynAttrib);
          StartDynAttrib^:= dinfo;
          StartDynAttrib^.DynOffset:= 1;
          rundyn:= StartDynAttrib
          end
        else
          begin
          StartDynAttrib:= rundyn;
          Inc(rundyn^.DynOffset)
          end
        end  { i<>startpar }
      end   { with currentpar^ }
    end;   { i loop over paragraphs }

  dinfo.DynStyle:= dinfo.DynStyle and $7f;
  stopdyninfo.StartKlen:= MaxOf(0, stopdyninfo.StartKLen-(stoppos-Stop.Pos));
  Stop.Pos:= stoppos;
  if (Stop.Pos<CharCount) and (AddDInfo or (stopdyninfo.DynStyle and $80<>0)) then Stop.AddDyn(stopdyninfo);
  end;      { procedure SetDynStyleP }

{ protected methods }

procedure TPlusMemo.InvalidateLines(start, stop: Longint; erase: Boolean);
  var s1, s2: Longint;  r: TRect;
  begin
  if start>stop then begin s1:= stop; stop:= start; start:= s1 end;
  s2:= FirstVisibleLine;
  if (start-s2<=fDisplayLines+1) and (stop>=s2) then
    begin
    if start>s2 then r.Top:= start*fLineHeight - fTopOrigin else r.Top:= 0;
    if stop<=s2+fDisplayLines+1 then r.Bottom:= (stop+1)*fLineHeight - fTopOrigin else r.Bottom:= ClientHeight;
    r.Left:= 0;
    r.Right:= ClientWidth;
    InvalidateRect(Handle, @r, erase)
    end
  end;


procedure TPlusMemo.ETPenChange(Sender: TObject);
  begin
  Invalidate
  end;

{ protected methods }

function TPlusMemo.ColPos(Par: pParInfo; ParNb: Longint; LineNb, ColNb: PlusCardinal): Longint;
  { return the X position of column ColNb in line LineNb }
  var j, jlim,
      lastcar     : PlusCardinal;
      dc          : hdc;
      lin         : LineInfo;
      t           : PChar;
      c           : Char;
      changed     : Boolean;
      breakadd,
      breakerror,
      runningerror: Integer;
      ss, newss   : TExtFontStyles;
      currentattr : TExtFontStyles;
      currentdyn  : pDynInfoRec;
      oldfont     : THandle;
      logfont     : TLogFont;
      Extra,
      curdynnb    : Integer;
  begin
  dc:= GetDC(Handle);
  if Par=nil then Par:= fParagraphs.Pointers[ParNb];
  fMouseNav.NavLines.LLPar:= Par;

  lin:= fMouseNav.fNavLines.Items[LineNb];
  t:= Par^.Text;
  currentattr:= lin.StartAttrib;
  curdynnb:= 0;
  while (curdynnb<Par^.DynCount) and (Par^.DynBuffer^[curdynnb].DynOffset<=lin.Start) do Inc(curdynnb);
  if curdynnb=0 then currentdyn:= Par^.StartDynAttrib
                else currentdyn:= @Par^.DynBuffer^[curdynnb-1];
  if curdynnb<Par^.DynCount then jlim:= Par^.DynBuffer^[curdynnb].DynOffset
                            else jlim:= High(jlim);

  TPlusFontStyles(ss):= [TPlusFontStyle(fsHighlight)];
  ss:= AttrToExtFontStyles(currentattr, currentdyn^.DynStyle) - ss;


  SetupLogFontStyle(LogFont, ss);
  oldfont:= SelectObject(dc, CreateFontIndirect(LogFont));

  j:= lin.Start;
  lastcar:= j;
  if ColNb <= lin.Stop-lin.Start then lin.Stop:= lin.Start+ColNb;


  if lin.Spaces>0 then
    begin
    runningerror:= ClientWidth-fLeftMargin-fRightMargin-lin.LineWidth;
    breakadd:= runningerror div lin.Spaces;
    breakerror:= runningerror mod lin.Spaces
    end
  else begin
       breakadd:= 0;
       breakerror:= 0
       end;
  runningerror:= breakerror div 2;
  Extra:= 0;
  Result:= 0;
  changed:= False;

  while j<lin.Stop do
    begin
    if (j>=jlim) and (j>lastcar) then
      begin
      Result:= Result + GetTextWidth(dc, t+lastcar, j-lastcar, fMaxOneShotChars);
      lastcar:= j
      end;

    c:= t[j];
    if ((c>#26) and ((c<>' ') or (not fJustified)  or (j<lin.JustifyStart))) or
       ((c<=#26) and (c<>#9) and (not (StaticFormat and (c in ctrlCodesSet)))) then

        begin
        Inc(j);
        end
    else
      begin
        if c=' ' then
          begin
          Inc(Extra, breakadd);
          runningerror:= runningerror + breakerror;
          if runningerror>=lin.Spaces then
            begin
            Inc(Extra);
            runningerror:= runningerror-lin.Spaces
            end;
          Inc(j);
          end
        else
          begin
          if j>lastcar then
              Result:= Result + GetTextWidth(dc, t+lastcar, j-lastcar, fMaxOneShotChars);

          while (j<lin.Stop) and ((t[j]=#9) or  (StaticFormat and (t[j] in CtrlCodesSet))) do
                 begin
                 case t[j] of
                   ctrlItalic, ctrlBold, ctrlAltFont, ctrlUnderline:
                         begin
                         changed:= True;
                         XORStyleCode(currentattr, t[j])
                         end;
                   #9: Result:= (Result div (fTabStops*fSpaceWidth) +1)*
                                            (fTabStops*fSpaceWidth);
                   end;
                 Inc(j);
                 end;  { while t[j]<=#26 }
          lastcar:= j
          end;   { t[j]<>' ' }
        end;  { t[j]=' ' or control code }

   if (changed or (j>jlim)) and ((j<lin.Stop) or (j>lastcar)) then
     begin
     if j>jlim then
       begin
       Inc(curdynnb);
       while (curdynnb<Par^.DynCount) and (Par^.DynBuffer^[curdynnb].DynOffset<j) do Inc(curdynnb);
       currentdyn:= @Par^.DynBuffer^[curdynnb-1];
       if curdynnb<Par^.DynCount then jlim:= Par^.DynBuffer^[curdynnb].DynOffset
                                 else jlim:= High(jlim)
       end;

     TPlusFontStyles(newss):= [TPlusFontStyle(fsHighlight)];
     newss:= AttrToExtFontStyles(currentattr, currentdyn^.DynStyle) - newss;
     if newss<>ss then
       begin
       SetupLogFontStyle(logfont, newss);
       ss:= newss;
       DeleteObject(SelectObject(dc, CreateFontIndirect(LogFont)))
       end;
     end;
   end;  { while j<stop }

  Result:= Result-fDisplayLeft+GetTextWidth(dc, t+lastcar, j-lastcar, fMaxOneShotChars)+Extra;
  if (lin.Stop<Par^.Length) and ((t[lin.Stop]>#26) or (not (t[lin.Stop] in CtrlCodesSet))) then
      fLastPosWidth:= GetTextWidth(dc, t+lin.Stop, 1, 1000);

  DeleteObject(SelectObject(dc, oldfont));
  ReleaseDC(Handle, dc);
  if fJustified and (lin.JustifyStart<Par^.Length) then Result:= Result+fLeftMargin
  else
    case fAlignment of
         taLeftJustify : Result:= Result+fLeftMargin;
         taRightJustify: Result:= Result + ClientWidth - fRightMargin - lin.LineWidth;
         taCenter: Result:= Result + fLeftMargin+(ClientWidth-fLeftMargin-fRightMargin-lin.LineWidth) div 2
         end;

  fMouseNav.fNavLines.LLPar:= fMouseNav.fPar
  end; { method ColPos }

function TPlusMemo.GetCaretWidth: Integer;
  begin
  if (fSelLen<>0) or (not Overwrite) or (not (pmoWideOverwriteCaret in Options)) or
     (fcp.ParOffset>=fcp.Par^.Length) then
         Result:= fCaretWidth
  else Result:= fWideCaret
  end;

function TPlusMemo.LinePos(lineFromTop: Integer): Integer; { protected methods }
  begin
  Result:= lineFromTop*fLineHeight - fTopOrigin mod fLineHeight
  end;


function TPlusMemo.LineNb(Y: Integer): Integer;
  begin
  if fLineHeight>0 then Result:= (Y + fTopOrigin mod fLineHeight) div fLineHeight
                   else Result:= 0
  end;

{ protected methods }

function TPlusMemo.ColNb(Par: pParInfo; ParNb: Longint; LineNb: PlusCardinal;
                                X: Integer; NoTabRounding: Boolean): PlusCardinal;
  { return the Column number at position X in line number LineNb }
  var i, ilim : PlusCardinal;
      lin     : LineInfo;
      xpos    : Longint;
      w       : PlusCardinal;
      dc      : hdc;
      changed : Boolean;
      t       : PChar;
      r       : Integer;
      breakadd,
      breakerror,
      runerror: Integer;
      currentattr: TExtFontStyles;
      currentdyn : pDynInfoRec;
      curdynnb   : Integer;
      ss, newss  : TExtFontStyles;
      oldfont    : THandle;
      logfont    : TLogFont;
      nbchars    : Integer;
      extraspace : Integer;

  begin
  dc:= GetDC(Handle);
  if Par=nil then Par:= fParagraphs.Pointers[ParNb];
  fMouseNav.NavLines.LLPar:= Par;
  with Par^ do
    begin
    lin:= fMouseNav.fNavLines[LineNb];
    t:= Text;
    currentattr:= lin.StartAttrib;
    curdynnb:= 0;
    while (curdynnb<DynCount) and (DynBuffer^[curdynnb].DynOffset<=lin.Start) do Inc(curdynnb);
    if curdynnb=0 then currentdyn:= StartDynAttrib
                  else currentdyn:= @DynBuffer^[curdynnb-1];
    if curdynnb<DynCount then ilim:= DynBuffer^[curdynnb].DynOffset
                         else ilim:= High(ilim);
    TPlusFontStyles(ss):= [TPlusFontStyle(fsHighlight)];
    ss:= AttrToExtFontStyles(currentattr, currentdyn^.DynStyle) - ss;

    SetupLogFontStyle(LogFont, ss);
    oldfont:= SelectObject(dc, CreateFontIndirect(LogFont));
    nbchars:= 1;
    extraspace:= ClientWidth-fRightMargin-lin.LineWidth-fLeftMargin;

    if lin.Spaces>0 then
      begin
      breakadd:= extraspace div lin.Spaces;
      breakerror:= extraspace mod lin.Spaces
      end
    else begin
         breakadd:= 0;
         breakerror:= 0
         end;

    runerror:= breakerror div 2;
    r:= -1;
    w:= 0;
    xpos:= 0;

    if fJustified and (lin.JustifyStart<lin.Stop) then xpos:= fLeftMargin
    else
        begin
        case fAlignment of
             taLeftJustify : xpos:= fLeftMargin;
             taRightJustify: xpos:= extraspace+fLeftMargin;
             taCenter: xpos:= fLeftMargin+ extraspace div 2
           end
        end;

    i:= lin.Start;

    repeat
      xpos:= xpos+w;
      changed:= False;
      while (i<lin.Stop) and ((t[i]<=#26) and StaticFormat and (t[i] in ctrlCodesSet)) do
        begin
        changed:= True;
        XORStyleCode(currentattr, t[i]);
        inc(i);
        Inc(r)
        end;

      if changed or (i>=ilim) then
        begin
        nbchars:= 1;
        if i>=ilim then
          begin
          Inc(curdynnb);
          while (curdynnb<DynCount) and (DynBuffer^[curdynnb].DynOffset<=i) do Inc(curdynnb);
          if curdynnb=0 then currentdyn:= StartDynAttrib
                        else currentdyn:= @DynBuffer^[curdynnb-1];
          if curdynnb<DynCount then ilim:= DynBuffer^[curdynnb].DynOffset
                               else ilim:= High(ilim)
          end;

        TPlusFontStyles(newss):= [TPlusFontStyle(fsHighlight)];
        newss:= AttrToExtFontStyles(currentattr, currentdyn^.DynStyle) - newss;
        if newss<>ss then
          begin
          SetupLogFontStyle(logfont, newss);
          ss:= newss;
          DeleteObject(SelectObject(dc, CreateFontIndirect(LogFont)));
          end;
        end;

      if i<lin.Stop then
        begin
        if t[i]<>#9 then
          begin
          w:= GetTextWidth(dc, t+i-nbchars+1, nbchars, fMaxOneShotChars);
          if nbchars>1 then Dec(w, GetTextWidth(dc, t+i-1, 1, 1000));
          nbchars:= 2
          end
        else
            w:= fLeftMargin+ ((xpos-fLeftMargin) div (fTabStops*fSpaceWidth)+1)*(fTabStops*fSpaceWidth) - xpos;

        if (fJustified) and (t[i]=' ') and (i>=lin.JustifyStart) then
          begin
          w:= w + breakadd;
          runerror:= runerror+breakerror;
          if runerror>lin.Spaces then
               begin
               Inc(w);
               runerror:= runerror-lin.Spaces
               end
          end;
        Inc(i);
        end;

      Inc(r)
    until (i>=lin.Stop) or (xpos+w div 2>X+fDisplayLeft)
    end; { with fParagrahps[currentparagraph] }

  DeleteObject(SelectObject(dc, oldfont));
  ReleaseDC(Handle, dc);
  if (xpos+w div 2 <= X+fDisplayLeft) then Inc(r);
  if NoTabRounding and (r>0) and (xpos>X+fDisplayLeft) and (t[lin.Start+r-1]=#9) then Dec(r);
  Result:= r;
  fMouseNav.fNavLines.LLPar:= fMouseNav.fPar
  end;     { method ColNb }

{ protected methods }

function TPlusMemo.GetPar(pos: Longint): Longint;
  { return the paragraph number that includes pos, where pos is an offset
    starting from beginning of text }
  begin
  ftmpnav1.Pos:= pos;
  Result:= ftmpnav1.ParNumber
  end;


procedure TPlusMemo.SetSelAttrib(a: Char);                 { protected methods }
  var t: PChar; sl: Longint; len: Longint; startnav, stopnav: TPlusNavigator;
  begin
  if not StaticFormat then Exit;
  sl:= SelLength;
  if abs(sl)<maxtextlen-2 then
    begin
    startnav:= TPlusNavigator.Create(Self);
    startnav.Assign(fSelStart);
    stopnav:= TPlusNavigator.Create(Self);
    //stopnav.Assign(fSelStop);
    t:= StrAlloc(abs(sl)+3);
    len:= GetSelTextBuf(@t[1], abs(SelLength)+1);
    t[0]:= a;
    t[len+1]:= a;
    t[len+2]:= #0;
    fNoCheckFormat:= True;
    if sl=0 then fInStripCodes:= True;
    SetSelTextBuf(t);
    fNoCheckFormat:= False;
    fInStripCodes:= False;
    StrDispose(t);
    stopnav.Assign(fSelStart);
    if sl=0 then SelStart:= fcp.Pos-1
            else if sl>0 then
                       begin
                       SelStart:= startnav.Pos;//fcp.Pos-len-2;
                       SelLength:= stopnav.Pos - startnav.Pos
                       end
                 else
                   begin
                   SelStart:= stopnav.Pos;
                   SelLength:= startnav.Pos - stopnav.Pos //SelLength:= -len-2
                   end;
    startnav.Free;
    stopnav.Free
    end
  end;

function TPlusMemo.SelectedBlockText: string;
  var i, indx, bstart, bstop: Integer; p: pParInfo; pstop, pstart: Integer;
  begin
  if (not fBlockSelection) or (fSelLen=0) then Result:= ''
  else
    begin
    if fBlockStartCol>fBlockStopCol then
      begin
      bstop:= fBlockStartCol;
      bstart:= fBlockStopCol
      end
    else
      begin
      bstop:= fBlockStopCol;
      bstart:= fBlockStartCol
      end;
    SetLength(Result, (bstop-bstart+2)*(fSelStop.ParNumber-fSelStart.ParNumber+1));
    indx:= 1;
    for i:= fSelStart.fParNb to fSelStop.fParNb do
      begin
      p:= fParagraphs.Pointers[i];
      pstart:= ColToOffset(p, bstart, TabStops, StaticFormat);
      pstop:= ColToOffset(p, bstop, TabStops, StaticFormat);
      if pstop>pstart then
        begin
        Move(p.Text[pstart], Result[indx], pstop-pstart);
        Inc(indx, pstop-pstart)
        end;
      Result[indx]:= #13;
      Result[indx+1]:= #10;
      Inc(indx, 2)
      end;
    Result[indx]:= #0;
    SetLength(Result, indx-1)
    end
  end;

procedure TPlusMemo.CreatePopupMenu;                       { protected methods }
  function CreateMenuItem(const s: string): TMenuItem;
    begin
    Result:= TMenuItem.Create(fInternalPopup);
    Result.Caption:= s;
    Result.OnClick:= PopupClickHandler
    end;
  begin
  fInternalPopup:= TPopupMenu.Create(Self);
  fInternalPopup.OnPopup:= PopupClickHandler;
  fInternalPopup.Items.Add(CreateMenuItem('&Undo'));
  fInternalPopup.Items.Add(CreateMenuItem('&Redo'));
  fInternalPopup.Items.Add(CreateMenuItem('-'));
  fInternalPopup.Items.Add(CreateMenuItem('Cu&t'));
  fInternalPopup.Items.Add(CreateMenuItem('&Copy'));
  fInternalPopup.Items.Add(CreateMenuItem('&Paste'));
  fInternalPopup.Items.Add(CreateMenuItem('-'));
  fInternalPopup.Items.Add(CreateMenuItem('&Select all'));
  end;

procedure TPlusMemo.PopupClickHandler(Sender: TObject);     { protected methods }
  begin
  if Sender=fInternalPopup then
    begin
    fInternalPopup.Items[0].Enabled:= CanUndo;
    fInternalPopup.Items[1].Enabled:= CanRedo;
    fInternalPopup.Items[3].Enabled:= (SelLength<>0) and (not (ReadOnly or DisplayOnly));
    fInternalPopup.Items[4].Enabled:= SelLength<>0;
    fInternalPopup.Items[5].Enabled:= Clipboard.HasFormat(CF_TEXT) and (not (ReadOnly or DisplayOnly))
    end
  else
    case (Sender as TMenuItem).MenuIndex of
      0: Undo;
      1: Redo;
      3: CutToClipboard;
      4: CopyToClipboard;
      5: PasteFromClipboard;
      7: SelectAll
      end
  end;

procedure TPlusMemo.RefreshDisplay;                        { protected methods }
  begin
  if HandleAllocated then
      begin
      Reformat;
      {$IFDEF PMDEBUG}
      CheckIntegrity;
      {$ENDIF}

      if fDisplayTop.LineNumber>MaxOf(fLineCount-fDisplayLines+1, 0) then
                         FirstVisibleLine:= MaxOf(fLineCount-fDisplayLines+1, 0);
      Invalidate;
      UpdateWindow(Handle);
      UpdateCaret(False);
      end
  end;


procedure TPlusMemo.CheckIntegrity;                        { protected methods }
  { checks wether everything is properly set in internal data structures }
  var i, j, line, offset: Longint; lin: LineInfo;
      loffset: PlusCardinal;
  begin
  {$IFDEF WIN32}
  line:= 0; offset:= 0;
  for i:= 0 to fParagraphs.Count-1 do
     begin
     fMouseNav.NavLines.LLPar:= fParagraphs.Pointers[i];
     with fMouseNav.fNavLines.LLPar^ do
       begin
       if StartLine<>line then
               raise Exception.Create('Error 1'+Version);
       if StartOffset<>offset then
               raise Exception.Create('Error 2 in ' + Version);
       loffset:= 0;
       for j:= 0 to NbLines-1 do
         begin
         lin:= fMouseNav.fNavLines[j];
         if lin.Start<>loffset then
                 raise Exception.Create('Error 3 in ' + Version);
         loffset:= lin.Stop
         end;
       if lin.Stop<>Length then
                 raise Exception.Create('Error 4 in ' + Version);
       line:= line+NbLines;
       offset:= offset+Length+2
       end
     end;
  if offset<>fTextLen+2 then
       raise Exception.Create('Error 5 in ' + Version);
  if line<>fLineCount then
       raise Exception.Create('Error 6 in ' + Version);
  fMouseNav.fNavLines.LLPar:= fMouseNav.fPar
  {$ENDIF}
  end;


  { overriden protected methods }

procedure TPlusMemo.MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
  var cline, sline, scp, cp: Longint;
      ccol: PlusCardinal;
  begin
  inherited MouseDown(Button, Shift, X, Y);
  if Button=mbLeft then
    begin
    fBlockSelection:= False;
    fMouseDownPos:= fMouseNav.Pos;
    fXCaretRunningPos:= Low(fXCaretRunningPos);
    if not fDisplayOnly then
      begin
      fMouseDown:= True;
      if fDblClick then Exit;
      fExtraCols:= 0;
      fDragging:= fMouseInSel and (not ReadOnly);

      if fDragging then
          begin
          fDraggingOutside:= False;
          fcp:= fMouseNav;
          Screen.Cursor:= crDrag;
          ShowCursor(True);
          UpdateCaret(True);
          if Assigned(fOnAfterMouseDown) then fOnAfterMouseDown(Self, Button, Shift, X, Y);
          Exit
          end;

      if fStartLineSelection>=0 then
        begin
          SelStart:= fMouseNav.Pos;
          with fMouseNav.NavLines[fMouseNav.ParLine] do SelLength:= Stop-Start;
          if (fMouseNav.ParNumber<fParagraphs.Count-1) and (fMouseNav.ParLine>=fMouseNav.Par^.NbLines-1) then
            SelLength:= SelLength + 2;
        end

      else
        begin
        if not (ssShift in Shift) then SelLength:= 0;
        fBlockSelection:= (Shift=[ssLeft, ssAlt]) and (pmoBlockSelection in Options) and (not WordWrap);
        scp:= fcp.Pos;
        sline:= fcp.LineNumber;
        ftmpnav1.Assign(fcp);
        if LineNb(Y) >= fLineCount-fDisplayTop.LineNumber then ftmpnav1.Pos:= fTextLen
        else begin
             cline:= LineNb(Y) + fDisplayTop.LineNumber;
             ftmpnav1.LineNumber:= cline;
             ccol:= ColNb(ftmpnav1.Par, ftmpnav1.fParNb, ftmpnav1.ParLine, X, pmoKeepColumnPos in Options);
             ftmpnav1.Col:= ccol;
             if ((pmoKeepColumnPos in Options) or fBlockSelection) and ((ftmpnav1.ParOffset>=ftmpnav1.Par^.Length) or (ftmpnav1.Text=#9)) then
                 fExtraCols:= MaxOf(X-ColPos(ftmpnav1.fPar, ftmpnav1.fParNb, ftmpnav1.ParLine, ccol), 0) div fSpaceWidth
             end;
        cp:= ftmpnav1.Pos;

        if ssShift in Shift then
          begin
          fSelLen:= fSelLen + (scp-cp);
          if fSelLen>=0 then begin
                             fSelStart.Assign(ftmpnav1);
                             fSelStop.Pos:= cp+fSelLen;
                             fcp:= fSelStart
                             end
          else begin
               fSelStart.Pos:= cp+fSelLen;
               fSelStop.Assign(ftmpnav1);
               fcp:= fSelStop
               end;
          InvalidateLines(fcp.LineNumber, sline, {False} True)
          end
        else begin fSelStart.Assign(ftmpnav1); fSelStop.Assign(ftmpnav1); fcp:= fSelStart end
        end;  { not line selecting }

      fUndoBreak:= True;
      UpdateCaret(False);
      if fBlockSelection then
        begin
        fBlockStartCol:= (fCaretX + fDisplayLeft - fLeftMargin) div fSpaceWidth;
        fBlockStopCol:= fBlockStartCol
        end;
      end  { display only }
    end; { left button }

  if not Focused then WinProcs.SetFocus(Handle);
  if Assigned(fOnAfterMouseDown) then OnAfterMouseDown(Self, Button, Shift, X, Y);
  end;

{ overriden protected methods }

var CurrentX, CurrentY: Integer;

procedure TPlusMemo.MouseMove(Shift: TShiftState; X, Y: Integer);
  var scp, cp, cline, sline: Longint;

  procedure Scroll;
    var next: Longint; xfake, yfake, del: Integer;
        scrolltype: TMouseScrollType;
    begin
    scrolltype:= fMouseScroll;
    next:= GetTickCount;
    repeat
      while (Longint(GetTickCount)<next) and (fMouseScroll=scrolltype) do
        Application.ProcessMessages;
      Application.ProcessMessages;
      if fMouseScroll=scrolltype then
        begin
        case fScrollRate of
          1: del:= 400;
          2: del:= 300;
          3: del:= 200;
          4: del:= 130;
          5: del:= 78;
          6: del:= 48;
          7: del:= 30
          else del:= 0
          end;
        next:= Longint(GetTickCount)+del;
        xfake:= CurrentX;
        yfake:= CurrentY;

        case fMouseScroll of
          msUp: begin
                if fDisplayTop.LineNumber>0 then Perform(WM_VSCROLL, SB_LINEUP, 0);
                yfake:=  - fScrollRate*3;
                end;

          msDown: begin
                  if fDisplayTop.LineNumber<=fLineCount-fDisplayLines then
                                          Perform(WM_VSCROLL, SB_LINEDOWN, 0);
                  yfake:= ClientHeight + fScrollRate*3;
                  end;

          msLeft: begin
                  if fDisplayLeft>0 then Perform(WM_HSCROLL, SB_LINEUP, 0);
                  xfake:= -fScrollRate*3;
                  end;

          msRight: begin
                   if fDisplayLeft<fMaxLineWidth-ClientWidth then
                                         Perform(WM_HSCROLL, SB_LINEDOWN, 0);
                   xfake:= ClientWidth+fScrollRate*3;
                   end
          end;

        MouseMove([ssLeft], xfake, yfake)
        end;

    until fMouseScroll<>scrolltype
    end;

  var oldscrolltype: TMouseScrollType;
      lineselect: Boolean;
      sexcols: Integer;
  begin
  CurrentX:= X; CurrentY:= Y;
  if fLockedCount>0 then
    begin
    inherited MouseMove(Shift, X, Y);
    Exit
    end;
  fMouseNav.LineNumber:= MaxOf(LineNb(Y)+fDisplayTop.LineNumber, 0);
  if fMouseNav.LineNumber-fDisplayTop.LineNumber>=fDisplayLines then
    fMouseNav.LineNumber:= MaxOf(fDisplayTop.LineNumber + fDisplayLines -1, 0);
  if not fMouseNav.Par^.Formatted then
    begin
    inherited MouseMove(Shift, X, Y);
    Exit
    end;
  if (fMouseNav.LineNumber<fLineCount-1) or (Y<=LinePos(fLineCount - fDisplayTop.LineNumber)) then
       fMouseNav.Col:= ColNb(fMouseNav.Par, fMouseNav.fParNb, fMouseNav.ParLine, X, pmoKeepColumnPos in Options)
  else fMouseNav.Pos:= fTextLen;

  fMouseNav.RightOfDyn;

  if not fMouseDown then
    begin
      lineselect:= X<fLeftMargin-2-fDisplayLeft;
      if lineselect and (not (pmoNoLineSelection in Options)) then
        begin
          lineselect:= fStartLineSelection>=0;
          fStartLineSelection:= fMouseNav.LineNumber;
          if not lineselect then
            begin
              if not (fMouseInContext or fMouseInSel) then fCursor:= Cursor;
              PostMessage(Handle, WM_SETCURSOR, Handle, HTCLIENT); 
            end;
          fMouseInSel:= False;
          fMouseInContext:= False
        end
      else   { not line selection }
        begin
        if fStartLineSelection>=0 then
          begin       { replace the cursor }
          fStartLineSelection:= -1;
          Cursor:= fCursor
          end;
        if not (pmoNoDragnDrop in Options) then
          if (fMouseNav.Pos>=fSelStart.Pos) and (fMouseNav.Pos<fSelStop.Pos) and (not fBlockSelection) then
            begin
            if (not fMouseInSel) and (not (ReadOnly or DisplayOnly)) then
               begin
               fMouseInSel:= True;
               if not fMouseInContext then fCursor:= Cursor;
               fMouseInContext:= False;
               Cursor:= crArrow
               end
            end
          else
            if fMouseInSel then
                begin
                fMouseInSel:= False;
                Cursor:= fCursor
                end
          end;  { not line selection }

    if not (fMouseInSel or (fStartLineSelection>=0)) then
      with fMouseNav.DynAttr do
        if (fMouseNav.Pos<fTextLen) and (DynStyle and $80 <>0) and (Cursor<>crDefault) then
          begin
          if not fMouseInContext then fCursor:= Self.Cursor;
          Self.Cursor:= Cursor;
          fMouseInContext:= True
          end
        else
           begin
           if fMouseInContext then Self.Cursor:= fCursor;
           fMouseInContext:= False
           end
    end;  { not fMouseDown }

  if fMouseDown and (ssLeft in Shift) then
    begin
    if LineNb(Y) >= fLineCount-fDisplayTop.LineNumber then
                   Y:= LinePos(fLineCount-fDisplayTop.LineNumber)-1;

    oldscrolltype:= fMouseScroll;

    fMouseScroll:= msNoScroll;
    if (X<0) and ((X>-24) or (not fDragging)) then fMouseScroll:= msLeft;
    if (X>ClientWidth) and ((X<ClientWidth+24) or (not fDragging)) then fMouseScroll:= msRight;

    if (Y<-3) and ((Y>-27) or (not fDragging)) then fMouseScroll:= msUp;
    if (Y>ClientHeight) and ((Y<ClientHeight+24) or (not fDragging)) then fMouseScroll:= msDown;

    if fMouseScroll in [msUp, msDown] then
      if fDragging and ((X<-3) or (X>ClientWidth+3)) then fMouseScroll:= msNoScroll;

    case fMouseScroll of
      msLeft : fScrollRate:= (-X) div 3;
      msRight: fScrollRate:= (X-ClientWidth) div 3;
      msUp   : fScrollRate:= (-Y) div 3;
      msDown : fScrollRate:= (Y-ClientHeight) div 3
      end;

    if (fMouseScroll<>msNoScroll) and (oldscrolltype<>fMouseScroll) then Scroll;

    if fMouseDown then
      if fStartLineSelection>=0 then
        if fMouseNav.LineNumber<fStartLineSelection then
          begin
            cp:= fSelStop.Pos;
            SelStart:= fMouseNav.Pos;
            SelLength:= cp - SelStart
          end
        else
          if fMouseNav.LineNumber>fStartLineSelection then
            begin
              cp:= fSelStart.Pos;
              ftmpnav1.Assign(fMouseNav);
              if fMouseNav.LineNumber<fLineCount-1 then ftmpnav1.LineNumber:= fMouseNav.LineNumber+1
                                                   else ftmpnav1.Col:= High(ftmpnav1.Col);
              SelStart:= ftmpnav1.Pos;
              SelLength:= cp - SelStart
            end
          else
            begin
              ftmpnav1.Assign(fMouseNav);
              if fMouseNav.LineNumber<fLineCount-1 then ftmpnav1.LineNumber:= fMouseNav.LineNumber+1
                                                   else ftmpnav1.Col:= High(ftmpnav1.Col);
              SelStart:= fMouseNav.Pos;
              SelLength:= ftmpnav1.Pos-SelStart
            end

      else      { mouse down, but not line selection }
        if fDragging then
          begin
          if (CurrentY>0) and (CurrentY<ClientHeight) and (CurrentX>0) and (CurrentX<ClientWidth) then
            begin
            UpdateCaret(False);
            if fDraggingOutside then
              begin
              fDraggingOutside:= False;
              if ssCtrl in Shift then SetCursor(LoadCursor(HInstance, 'DRAGCOPY'))
                                 else SetCursor(Screen.Cursors[crDrag])
              end
            end

          else if (fMouseScroll=msNoScroll) and (not fDraggingOutside) then
                 begin
                 SetCursor(Screen.Cursors[crNoDrop]);
                 fDraggingOutside:= True
                 end;
          end

        else    { not dragging }
          begin
          sline:= fcp.LineNumber;
          scp:= fcp.Pos;
          sexcols:= fExtraCols;
          if fBlockSelection then
              fExtraCols:= MaxOf(X-ColPos(fMouseNav.Par, fMouseNav.ParNumber, fMouseNav.ParLine, fMouseNav.ParOffset), 0) div fSpaceWidth
          else fExtraCols:= 0;

          cline:= fMouseNav.LineNumber;
          cp:= fMouseNav.Pos;
          if (scp<>cp) or (sexcols<>fExtraCols) then
            begin
            fSelLen:= scp+fSelLen-cp;
            if fBlockSelection then { refresh all lines part of old selection }
              InvalidateLines(fSelStart.LineNumber, fSelStop.LineNumber, False);

            if fSelLen<0 then begin fSelStart.Pos:= cp+fSelLen; fSelStop.Pos:= cp; fcp:= fSelStop end
                         else begin fSelStart.Pos:= cp; fSelStop.Pos:= cp+fSelLen; fcp:= fSelStart end;
            if fBlockSelection then  { refresh all lines part of new selection }
              InvalidateLines(fSelStart.LineNumber, fSelStop.LineNumber, False)
            else   { refresh only lines newly added or removed from selection }
              InvalidateLines(sline, cline, False);
            end;

          if (fsPos<>fcp.Pos) or (sexcols<>fExtraCols) then
            begin
            UpdateCaret(False);
            if fBlockSelection then fBlockStopCol:= (fCaretX + fDisplayLeft - fLeftMargin) div fSpaceWidth
            end;

          if fDblClick then
            begin
            if fMouseDownPos<cp then
              begin
              fSelStart.Pos:= MinOf(fMouseDownPos, fSelStart.Pos);
              fSelStop.Assign(fMouseNav);
              fcp:= fSelStop
              end
            else begin
                 fSelStart.Assign(fMouseNav);
                 fSelStop.Pos:= MaxOf(fMouseDownPos, fSelStop.Pos);
                 fcp:= fSelStart
                 end;
            SelectWords(Self, fMouseNav.Pos>=fSelStop.Pos)
            end
          end   { not dragging }

    end;     { mouse down }
  inherited MouseMove(Shift, X, Y)
  end;  { method MouseMove }

{ overriden protected methods }

type TPHighlighter = {$IFDEF WIN32} TPlusHighlighter; {$ELSE} class(TPlusHighlighter); {$ENDIF}

procedure TPlusMemo.MouseUp(Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
  var cn: DynInfoRec;
      dragbuf: PChar;
      draglen: Longint;
      cnstring: string;
      op: Byte;
      cstart, cstop: Longint;
      ckind: Integer;
      spos: TPoint;
  begin
  if fDblClick then UpdateCaret(False);
  fMouseDown:= False; fDblClick:= False; fMouseScroll:= msNoScroll;
  
  if (Button=mbRight) and fMouseInSel then
    begin
    fMouseInSel:= False;
    Cursor:= fCursor
    end;

  CurrentX:= X; CurrentY:= Y;

  if fDragging then
    begin
    fDragging:= False;
    Screen.Cursor:= crDefault;
    Cursor:= fCursor;
    if fSelLen>0 then fcp:= fSelStart else fcp:= fSelStop;

    if not fDraggingOutside then
      if (fMouseNav.Pos>=fSelStart.Pos) and (fMouseNav.Pos<fSelStop.Pos) then
        SelStart:= fMouseNav.Pos
      else
        begin
        draglen:= Abs(SelLength);
        if draglen>maxtextlen then draglen:= maxtextlen;
        dragbuf:= StrAlloc(draglen+1);
        fSelStart.GetTextBuf(dragbuf, draglen);
        Lines.BeginUpdate;
        if not (ssCtrl in Shift) then SelText:= ''
                                 else SelLength:= 0;
        SelStart:= fMouseNav.Pos;
        SetSelTextBuf(dragbuf);
        SelLength:= fMouseNav.Pos-SelStart;
        pOffsetRangeRecord(fUndoList[fUndoLevel-1])^.Coupled:= not (ssCtrl in Shift);
        StrDispose(dragbuf);
        Lines.EndUpdate
        end;
    UpdateCaret(True);
    ScrollInView;
    fMouseInSel:= False;
    MouseMove(Shift, X, Y);
    inherited MouseUp(Button, Shift, X, Y);
    Exit
    end;

  if (fMouseDownPos=fMouseNav.Pos) or (Button=mbRight) then
    begin
    cn:= fMouseNav.DynAttr;
    if fMouseNav.Pos=fTextLen then cn.DynStyle:= 0;
    if (cn.DynStyle and $80 <>0) and (cn.Context<>0) and
       (Assigned(fOnContext) or (fMsgList.Count>0) or (fHighlighter<>nil)) then
        begin
        ftmpnav1.Assign(fMouseNav);
        ftmpnav1.BackToDyn(0);
        cstart:= ftmpnav1.Pos;
        ftmpnav1.Assign(fMouseNav);
        if not ftmpnav1.ForwardToDyn(fTextLen) then ftmpnav1.Pos:= fTextLen;
        cstop:= ftmpnav1.Pos;
        if (fMsgList.Count>0) or (fHighlighter<>nil) then
          begin
          if Button=mbRight then ckind:= pm_RightContext
                            else ckind:= pm_Context;
          cnstring:= GetTextPart(cstart, cstop);
          Perform(ckind, cn.Context, Longint(@cnstring));
          op:= pm_Msg;
          if fHighlighter<>nil then TPHighlighter(fHighlighter).Notification(Self, TOperation(op))
          end;
        if Assigned(fOnContext) then fOnContext(Self, cn.Context, cstart, cstop);
        inherited MouseUp(Button, Shift, X, Y);
        Exit
        end
    end;

  inherited MouseUp(Button, Shift, X, Y);
  if (Button=mbRight) and (PopupMenu=nil) then
    begin
    if fInternalPopup=nil then CreatePopupMenu;
    spos:= ClientToScreen(Point(X, Y));
    fInternalPopup.Popup(spos.X, spos.Y)
    end;
  if not fDisplayOnly then ScrollInView;
  end;


procedure TPlusMemo.DblClick;                    { overriden protected methods }
  begin
  if not fDisplayOnly then
    begin
    fDblClick:= True;
    SelectWords(Self, True);
    UpdateCaret(False)
    end;
  inherited DblClick;
  end;

procedure TPlusMemo.Loaded;                      { overriden protected methods }
  begin
  inherited Loaded;
  ClearUndo;
  if (ApplyKeywords and (Keywords<>nil) and (Keywords.Count>0)) or
     (ApplyStartStopKeys and (StartStopKeys<>nil) and (StartStopKeys.Count>0)) then
     ReapplyKeywords;
  end;

procedure TPlusMemo.WndProc(var Message: TMessage);
  var i: Integer; aop: Byte; msgmemo: TPlusMemo; somenil: Boolean;
  begin
  NotifyMsg:= Message;
  aop:= pm_Msg;
  somenil:= False;
  if Assigned(fMsgList) then
    for i:= 0 to fMsgList.Count-1 do
      begin
      msgmemo:= TPlusMemo(MsgList[i]);
      if msgmemo<>nil then msgmemo.Notification(Self, TOperation(aop))
                      else somenil:= True
      end;
  if somenil then fMsgList.Pack;
  inherited WndProc(Message)
  end;

{ overriden protected methods }

procedure TPlusMemo.KeyDown(var Key: Word; Shift: TShiftState);
var scp, sline : Longint;
    sk : Word;
    ProcessShift, founddel  : Boolean;
    ilevel, icolpos  : Longint;
    linenumber: Longint;
    backdone  : Boolean;
    firstvline: Longint;
    sextra    : Integer;

  function indentlevel(lnb: Longint): Word;
    var txt: string;
    begin
    txt:= Lines[lnb];
    Result:= 0;
    while (Result<Length(txt)) and (txt[Result+1] in [' ', #9]) do
      Inc(Result);
    end;

  procedure dokeyvmove;
    begin
    fExtraCols:= 0;
    if not (ssShift in Shift) then
      begin
      SelLength:= 0;
      fBlockSelection:= False
      end;
    if (fXCaretRunningPos=Low(fXCaretRunningPos)) or
       (not (WordWrap or (Options*[pmoKeepColumnPos, pmoPutExtraSpaces]<>[pmoPutExtraSpaces]))) then
           fXCaretRunningPos:= fCaretX;

    ftmpnav1.Col:= ColNb(ftmpnav1.fPar, ftmpnav1.fParNb, ftmpnav1.ParLine, fXCaretRunningPos,
                                                                              pmoKeepColumnPos in Options);

    if (fBlockSelection or (pmoKeepColumnPos in Options)) and
       ((ftmpnav1.ParOffset=ftmpnav1.fPar^.Length) or (ftmpnav1.Text=#9)) and
       ((not (ssShift in Shift)) or fBlockSelection) then
         begin
         CurrentX:= ColPos(ftmpnav1.fPar, ftmpnav1.fParNb, ftmpnav1.ParLine, ftmpnav1.Col);
         if fXCaretRunningPos>CurrentX then fExtraCols:= (fXCaretRunningPos-CurrentX) div fSpaceWidth
         end;
    ProcessShift:= True
    end;

begin
inherited KeyDown(Key, Shift);
if fLockedCount>0 then Exit;
if fDragging then
  begin
  if (Key=VK_CONTROL) and (not fDraggingOutside) then SetCursor(LoadCursor(HInstance, 'DRAGCOPY'));
  Exit
  end;

if fDisplayOnly then
  begin   { effect only display move }
  firstvline:= FirstVisibleLine;
  case key of
    VK_HOME: LeftOrigin:= 0;
    VK_END : LeftOrigin:= MaxOf(fMaxLineWidth - ClientWidth + fRightMargin, 0);
    VK_PRIOR: if ssCtrl in Shift then FirstVisibleLine:= 0
              else FirstVisibleLine:= MaxOf(firstvline-1, 0);

    VK_NEXT: if ssCtrl in Shift then FirstVisibleLine:= MaxOf(LineCount-fDisplayLines+1, 0)
             else FirstVisibleLine:= MinOf(firstvline+1,
                                           MaxOf(LineCount-fDisplayLines+1, 0));

    VK_UP  : FirstVisibleLine:= MaxOf(firstvline-1, 0);
    VK_DOWN: FirstVisibleLine:= MinOf(firstvline+1,
                                      MaxOf(LineCount-fDisplayLines+1, 0));
    VK_LEFT: LeftOrigin:= MaxOf(LeftOrigin-fHScrollFact, 0);
    VK_RIGHT: LeftOrigin:= MinOf(LeftOrigin+fHScrollFact,
                                 MaxOf(fMaxLineWidth-ClientWidth+fRightMargin, 0))
    end;
  Exit
  end;

{ not DisplayOnly, so do the necessary processing }
if (ssCtrl in Shift) and (not (ssAlt in Shift)) then
   begin
   ProcessShift:= True;
   fsStyle:= GetCurrentStyle;
   fsPos:= fcp.Pos;
   if (not fReadOnly) and fEnableHotKeys  then
     begin
     ProcessShift:= False;
     case Key of
       $42: SetBold;
       $46: SetFindMode;
       $48: SetHighlight;
       $49: SetItalic;
       $54: SetSelText(#9);  {ctrl T mapped to a TAB}
       $55: SetUnderline;
       $57: ToggleWordwrap; {ctrl W}
       $50: SetSelText(#13#10#16#13#10);
       $5A: if ssShift in Shift then Redo
                                else Undo

       else ProcessShift:= True end
       end;
   if not ProcessShift then begin Key:= 0; exit end
   end;

if Key=VK_F3 then
  begin
  Key := 0;
  SetFindAgainMode;
  exit;
  end;

if Key=VK_F1 then
  begin
  Key := 0;
  DoHelpRequested;
  exit;
  end;

if Key=VK_ESCAPE then
  begin
  Key := 0;
  DoEscapeRequested;
  exit;
  end;

if Key>=$30 then { VK code for '0', lowest value for normal typing }
  Exit;          { avoid case statement for text typing }

sk:= Key;
Key:= 0;
FormatNow(fcp.ParNumber, fcp.ParNumber, False);
scp:= fcp.Pos;
sline:= fcp.LineNumber;
ftmpnav1.Assign(fcp);
ProcessShift:= False;

if (Shift=[ssAlt, ssShift]) and (pmoBlockSelection in Options) and
   (not WordWrap) and (not fBlockSelection) and (sk in [VK_LEFT, VK_RIGHT, VK_UP, VK_DOWN]) then
  begin
  fBlockSelection:= True;
  fBlockStartCol:= fcp.ParOffset + fExtraCols;
  fBlockStopCol:= fBlockStartCol
  end;

case sk of
    VK_TAB: if ssAlt in Shift then
              begin
              Key:= VK_TAB;
              Exit
              end
            else
              begin
              if not ReadOnly then SelText:= #9;
              Key:= 0;
              Exit
              end;

    VK_HOME: if not (ssAlt in Shift) then
               begin
               fBlockSelection:= False;
               fExtraCols:= 0;
               if not (ssShift in Shift) then SelLength:= 0;
               if ssCtrl in Shift then ftmpnav1.Pos:= 0
                                  else ftmpnav1.LineNumber:= fcp.LineNumber;
               fXCaretRunningPos:= Low(fXCaretRunningPos);
               ProcessShift:= True
               end;

    VK_END: if not (ssAlt in Shift) then
              begin
              fBlockSelection:= False;
              fExtraCols:= 0;
              fXCaretRunningPos:= Low(fXCaretRunningPos);
              if not (ssShift in Shift) then SelLength:= 0;
              if ssCtrl in Shift then ftmpnav1.Pos:= fTextLen
                                 else ftmpnav1.Col:= maxtextlen;
              ProcessShift:= True
              end;

    VK_PRIOR: if not (ssAlt in Shift) then
                begin
                FormatNow(MaxOf(fcp.fParNb-fDisplayLines+1, 0), fcp.fParNb, False);
                ftmpnav1.LineNumber:= MaxOf(fcp.LineNumber-fDisplayLines+1, 0);
                dokeyvmove
                end;

    VK_NEXT: if not (ssAlt in Shift) then
               begin
               FormatNow(fcp.fParNb, MinOf(fcp.fParNb+fDisplayLines-1, fParagraphs.Count-1), False);
               ftmpnav1.LineNumber:= MinOf(fcp.LineNumber+fDisplayLines-1, fLineCount-1);
               dokeyvmove
               end;

    VK_UP: if (not (ssAlt in Shift)) or fBlockSelection then
             if fcp.LineNumber=0 then inherited KeyDown(sk, Shift)
             else
               begin
               if fcp.fParLine=0 then FormatNow(MaxOf(fcp.fParNb-1, 0), fcp.fParNb-1, False);
               ftmpnav1.LineNumber:= MaxOf(fcp.LineNumber-1, 0);
               dokeyvmove
               end;

    VK_DOWN: if (not (ssAlt in Shift)) or fBlockSelection then
               if fcp.LineNumber<fLineCount-1 then
                 begin
                 if fcp.fParLine=fcp.fPar^.NbLines-1 then
                   FormatNow(fcp.fParNb+1, MinOf(fcp.fParNb+1, fParagraphs.Count-1), False);
                 ftmpnav1.LineNumber:= fcp.LineNumber+1;
                 dokeyvmove
                 end;

    VK_LEFT: if (not (ssAlt in Shift)) or fBlockSelection then
               begin
               fXCaretRunningPos:= Low(fXCaretRunningPos);
               if not (ssShift in Shift) then
                 begin
                 SelLength:= 0;
                 fBlockSelection:= False
                 end;
               if (ftmpnav1.Pos>0) or (fExtraCols>0) then
                 begin
                 if ssCtrl in Shift then
                   begin
                   fExtraCols:= 0;
                   { back to first non blank char. }
                   founddel:= True;
                   while (ftmpnav1.Pos>0) and founddel do
                     begin
                     ftmpnav1.Pos:= ftmpnav1.Pos-1;
                     founddel:= ftmpnav1.Text in Delimiters;
                     end;
                   if ftmpnav1.Text=#10 then ftmpnav1.Pos:= ftmpnav1.Pos-1
                   else
                     begin
                     founddel:= False;
                     while (ftmpnav1.Pos>0) and (not founddel) do
                       begin
                       ftmpnav1.Pos:= ftmpnav1.Pos-1;
                       founddel:= ftmpnav1.Text in (Delimiters+[#13, #10]);
                       if founddel then ftmpnav1.Pos:= ftmpnav1.Pos+1
                       end
                     end
                   end

                 else
                   if fExtraCols>0 then Dec(fExtraCols)
                   else
                     begin
                     backdone:= False;
                     if ftmpnav1.ParOffset>0 then
                       begin
                       ftmpnav1.Pos:= ftmpnav1.Pos-1;
                       backdone:= True;
                       if fPassOver and StaticFormat then
                           begin
                           while (ftmpnav1.Text in CtrlCodesSet) and (ftmpnav1.ParOffset>0) do ftmpnav1.Pos:= ftmpnav1.Pos-1;
                           backdone:= not (ftmpnav1.Text in CtrlCodesSet)
                           end
                       end;
                     if (not fBlockSelection) and (not backdone) and (ftmpnav1.Pos>0) then
                       begin
                       ftmpnav1.Pos:= ftmpnav1.Pos - 2;
                       if (pmoDiscardTrailingSpaces in Options) and (ftmpnav1.ParOffset>0) then
                         begin
                         ftmpnav1.Pos:= ftmpnav1.Pos-1;
                         while (not backdone) and (ftmpnav1.Text in [' ', #9]) do
                           begin
                           if ftmpnav1.ParOffset>0 then ftmpnav1.Pos:= ftmpnav1.Pos-1
                                                   else backdone:= True
                           end;
                         if not backdone then ftmpnav1.Pos:= ftmpnav1.Pos+1
                         end
                       end
                     end;
                 end  { if cp>0 }
               else inherited KeyDown(sk, Shift);
               ProcessShift:= True
               end;

    VK_RIGHT: if (not (ssAlt in Shift)) or fBlockSelection then
                begin
                fXCaretRunningPos:= Low(fXCaretRunningPos);
                if not (ssShift in Shift) then
                  begin
                  SelLength:=0;
                  fBlockSelection:= False
                  end;
                if ssCtrl in Shift then
                  begin
                  fExtraCols:= 0;
                  if (ftmpnav1.Pos<CharCount) and (ftmpnav1.Text=#13) then ftmpnav1.Pos:= ftmpnav1.Pos+2
                  else
                    begin
                    while (ftmpnav1.Pos<CharCount) and
                          (not (ftmpnav1.Text in (Delimiters+[#13, #10]))) do ftmpnav1.Pos:= ftmpnav1.Pos+1;
                    if (ftmpnav1.Pos<CharCount) and (ftmpnav1.Pos=fcp.Pos) and (ftmpnav1.Text=#13) then
                                                                            ftmpnav1.Pos:= ftmpnav1.Pos+2;
                    while (ftmpnav1.Pos<CharCount) and (ftmpnav1.Text in Delimiters) do ftmpnav1.Pos:= ftmpnav1.Pos+1
                    end
                  end
                else begin
                     if (ftmpnav1.ParOffset=ftmpnav1.Par^.Length) and
                        ((pmoKeepColumnPos in Options) and (not (pmoWrapCaret in Options)) or fBlockSelection)
                          then
                       Inc(fExtraCols)
                     else
                         if ftmpnav1.pos<CharCount then
                           begin
                           fExtraCols:= 0;
                           if StaticFormat and fPassOver then
                             while (ftmpnav1.Pos<CharCount) and (ftmpnav1.Text in CtrlCodesSet) do
                                 ftmpnav1.Pos:= ftmpnav1.Pos+1;
                           if (pmoDiscardTrailingSpaces in Options) and (not fBlockSelection) then
                             begin  { reach the end of paragraph if only spaces remain }
                             while (ftmpnav1.Pos<CharCount) and (ftmpnav1.Text in [' ', #9]) do
                                 ftmpnav1.Pos:= ftmpnav1.Pos+1;
                             if (ftmpnav1.Pos<CharCount) and (ftmpnav1.Text<>#13) then ftmpnav1.Pos:= scp
                             end;
                           if ftmpnav1.Pos<CharCount then
                               if ftmpnav1.Text=#13 then ftmpnav1.Pos:= ftmpnav1.Pos+2
                                                    else ftmpnav1.Pos:= ftmpnav1.Pos+1;
                           end
                     end;
                ProcessShift:= True
                end;

    VK_DELETE: if not fReadOnly then
               begin
               if (SelLength=0) and (pmoKeepColumnPos in Options) then sextra:= fCaretX
                                                                  else sextra:= Low(sextra);
               fNoPaint:= True;
               if ssShift in Shift then
                   begin
                   if fSelLen<>0 then begin
                                      fNoPaint:= False;
                                      CutToClipboard;
                                      Exit
                                      end
                   else if fcp.ParOffset=0 then SelLength:= -2 else SelLength:= -1;
                   end

               else
                   if (ssCtrl in Shift) and (fSelStart.LineNumber=fSelStop.LineNumber) then
                       begin
                       fSelStop.Col:= maxtextlen;
                       fcp:= fSelStart;
                       fSelLen:= fSelStop.Pos-fSelStart.Pos
                       end
                   else
                      if SelLength=0 then
                          if fcp.ParOffset=fcp.Par^.Length then SelLength:= 2
                          else
                            begin
                            while (fcp.Pos+SelLength<fTextLen) and (Chars[fcp.Pos+SelLength] in CtrlCodesSet) do
                               SelLength:= SelLength+1;
                            if fSelStop.ParOffset=pParInfo(fSelStop.Par)^.Length then SelLength:= SelLength+2
                                                                                 else SelLength:= SelLength+1
                            end;
               fNoPaint:= False;
               ClearSelection;
               if pmoKeepColumnPos in Options then
                   if sextra>fCaretX then fExtraCols:= (sextra-fCaretX) div fSpaceWidth
               end;

    VK_BACK: begin
             fNoPaint:= True;
             fXCaretRunningPos:= Low(fXCaretRunningPos);
             if not fReadOnly then
             begin
             if SelLength=0 then
               if ssCtrl in Shift then
                 begin
                 if ftmpnav1.ParOffset>0 then ftmpnav1.Pos:= ftmpnav1.Pos-1;
                 while (ftmpnav1.ParOffset>0) and (ftmpnav1.Text in (Delimiters+[#13,#10])) do
                   ftmpnav1.Pos:= ftmpnav1.Pos-1;
                 founddel:= False;
                 while (ftmpnav1.ParOffset>0) and (not founddel) do
                   begin
                   ftmpnav1.Pos:= ftmpnav1.Pos-1;
                   founddel:= ftmpnav1.Text in (Delimiters+[#13, #10]);
                   if founddel then ftmpnav1.Pos:= ftmpnav1.Pos+1
                   end;
                 SelLength:= ftmpnav1.Pos - fcp.Pos;
                 end

               else
                     if (fcp.ParOffset=0) and ((SelLength<>0) or (fExtraCols=0)) then SelLength:= -2
                     else begin
                          if pmoBackIndent in Options then
                            begin
                            icolpos:= SelCol;
                            linenumber:= SelLine;
                            if (icolpos>0) and (indentlevel(linenumber)=icolpos) then
                              begin
                              ilevel:= icolpos;
                              while (linenumber>=0) and (ilevel>=icolpos) do
                                begin
                                Dec(linenumber);
                                if linenumber>=0 then ilevel:= indentlevel(linenumber)
                                                 else ilevel:= 0
                                end;
                              SelLength:= ilevel-icolpos
                              end
                            end;
                          if SelLength=0 then
                            if fExtraCols>0 then Dec(fExtraCols)
                            else
                               begin
                               SelLength:= -1;
                               while (fcp.Pos+SelLength>0) and (Chars[fcp.Pos+SelLength] in CtrlCodesSet) do
                                   begin
                                   if fSelStart.ParOffset=0 then SelLength:= SelLength-2
                                                            else SelLength:= SelLength-1
                                   end
                               end
                          end;

             fNoPaint:= False;
             ClearSelection
             end
             end;

    VK_INSERT:
        begin
        if not (ssAlt in Shift) then
                 begin
                 if (ssShift in Shift) and (not fReadOnly) then PasteFromClipboard
                 else if ssCtrl in Shift then CopyToClipboard
                      else if pmoInsertKeyActive in Options then
                                   begin
                                   Overwrite:= not Overwrite;
                                   if Assigned(fOnAttrChange) then OnStyleChange(Self)
                                   end;
                 Exit
                 end
        end

  else begin Key:= sk; exit end
  end;

if ProcessShift then
  if ssShift in Shift then
    begin    { extend selection and repaint }
    if fBlockSelection then  { refresh all lines part of old selection }
      InvalidateLines(fSelStart.LineNumber, fSelStop.LineNumber, False);
    fSelLen:= fSelLen + (scp-ftmpnav1.Pos);
    if fSelLen>=0 then begin fSelStart.Assign(ftmpnav1); fSelStop.Pos:= ftmpnav1.Pos+fSelLen; fcp:= fSelStart end
                  else begin fSelStart.Pos:= ftmpnav1.Pos+fSelLen; fSelStop.Assign(ftmpnav1); fcp:= fSelStop end;
    if fBlockSelection then { refresh all lines part of new selection }
      InvalidateLines(fSelStart.LineNumber, fSelStop.LineNumber, False)
    else
      InvalidateLines(fcp.LineNumber, sline, False); { just invalidate added or removed selection lines }
    end
  else begin fSelLen:= 0; fSelStart.Assign(ftmpnav1); fSelStop.Assign(ftmpnav1); fcp:= fSelStart end;

fUndoBreak:= True;
UpdateCaret(False);
if fBlockSelection then fBlockStopCol:= (fCaretX + fDisplayLeft - fLeftMargin) div fSpaceWidth;
ScrollInView;
UpdateWindow(Handle)
end;     { method KeyDown }



procedure TPlusMemo.KeyPress(var Key: Char);     { overriden protected methods }
var linetext: string;
    ilevel, colpos  : Word;
    sline   : Longint;

begin
inherited KeyPress(Key);
if (fLockedCount>0) or (fDragging) then Exit;
if (fReadOnly and (Key<>#3)) or fDisplayOnly then exit;
if (Key>#30) and (Key<>#127) then
  begin
  if Overwrite and (fSelLen=0) and (fcp.ParOffset<fcp.fPar^.Length) then SelLength:= 1;
  if (Key=' ') and (SelLength=0) and (pmoDiscardTrailingSpaces in Options) and
     (pmoKeepColumnPos in Options) and (fcp.ParOffset=fcp.fPar^.Length) then
       begin
       Inc(fExtraCols);
       UpdateCaret(False);
       ScrollInView
       end
  else
    SelText:= Key
  end

else begin
     case Key of #13: if Overwrite and (SelLength=0) then
                        if SelLine<fLineCount-1 then begin SelLine:= SelLine+1; ScrollInView end
                                                else begin SelStart:= fTextLen; SelText:= #13#10 end
                      else
                        if (SelLength<>0) or (not (pmoAutoIndent in Options)) then SelText:= #13#10
                        else begin  { autoindent }
                             sline:= SelLine;
                             while (sline>=0) and (Length(linetext)=0) do
                               begin
                               linetext:= Lines[sline];
                               Dec(sline)
                               end;
                             colpos:= SelCol;
                             ilevel:= 0;
                             while (ilevel<Length(linetext)) and (linetext[ilevel+1] in [' ', #9]) do Inc(ilevel);
                             if ilevel>=colpos then ilevel:= colpos;
                             if (pmoDiscardTrailingSpaces in Options) and (fcp.ParOffset=fcp.fPar^.Length) then
                               begin
                               SelText:= #13#10;
                               fExtraCols:= 0;
                               for colpos:= 1 to ilevel do if linetext[colpos]=#9 then Inc(fExtraCols, TabStops)
                                                                                  else Inc(fExtracols);
                               UpdateCaret(False);
                               ScrollInView
                               end
                             else SelText:= #13#10+ Copy(linetext, 1, ilevel);
                             end;

                  #5: begin Invalidate; CheckIntegrity end; { ctrl-e }
                  #3: CopyToClipboard;
                 #22: PasteFromClipboard;
                 #24: CutToClipboard
                 end
     end;
{ we want to updtate the window if the user keeps a key pressed,
  so that he sees the effect as the char. accumulate.
  But we don't want if he presses different keys, as this will
  permit the control to catch up with typing from time to time,
  something which would not be done if so many paint messages are
  processed }
if not fReceivedKeyUp then UpdateWindow(Handle)
else fReceivedKeyUp:= False
end;

{ overriden protected methods }

procedure TPlusMemo.KeyUp(var Key: Word; Shift: TShiftState);
  begin
  fReceivedKeyUp:= True;
  inherited KeyUp(Key, Shift);
  if fDragging and (Key=VK_CONTROL) and (not fDraggingOutside) then SetCursor(Screen.Cursors[crDrag])
  end;

procedure TPlusMemo.CreateHandle;                { overriden protected methods }
  begin
  inherited CreateHandle;
  fLineBmpWidth:= ClientWidth;
  fLineBmp:= CreateCompatibleBitmap(Canvas.Handle, fLineBmpWidth, fLineHeight);
  fExtraCols:= 0;
  fXCaretRunningPos:= Low(fXCaretRunningPos);
   try Perform(CM_FontChanged, 0, 0);  { update many internal fields, reformat the text }
   except end
  end;

procedure TPlusMemo.DestroyWindowHandle;
  begin
  if fLineBmp<>0 then
    begin
    DeleteObject(fLineBmp);
    fLineBmp:= 0;
    fLineBmpWidth:= 0
    end;
  inherited DestroyWindowHandle
  end;


{ overriden protected methods }

procedure TPlusMemo.CreateParams(var p: TCreateParams);
  begin
  inherited CreateParams(p);
  with p do
    begin
    if fVScrollBar then Style:= Style or WS_VSCROLL;
    if fHScrollBar then Style:= Style or WS_HSCROLL;
    if FBorderStyle = bsSingle then
      {$IFDEF WIN32}
      if NewStyleControls and Ctl3D then
      begin
        Style := Style and not WS_BORDER;
        ExStyle := ExStyle or WS_EX_CLIENTEDGE;
      end
      else
      {$ENDIF}
        Style := Style or WS_BORDER;
    end
  end;


{ ************* message handlers ************************ }

procedure TPlusMemo.PMUpdateBkg(var m: TMessage);
  { reformat a chunck of paragraphs for background updating }
  begin
  UpdateBkg(Self, m);  { moved in Plussup.inc to make room in D1 }
  end;      { message pmUpdateBkgnd }


procedure TPlusMemo.WMSetFocus;                             { message handlers }
  begin
  inherited;
  if not fDisplayOnly then
    begin
    CreateCaret(Handle, 0, GetCaretWidth, fLineHeight);
    PlaceCaret;
    ShowCaret(Handle)
    end;
  if HideSelection and (fSelLen<>0) and (fLockedCount=0) then
    InvalidateLines(fSelStart.LineNumber, fSelStop.LineNumber, False);
  end;

procedure TPlusMemo.WMKillFocus;                            { message handlers }
  begin
  if not fDisplayOnly then DestroyCaret;
  if HideSelection and (fSelLen<>0) then
    InvalidateLines(fSelStart.LineNumber, fSelStop.LineNumber, False);
  inherited
  end;

{ message handlers }

procedure TPlusMemo.WMGetDlgCode(var Message: TWMGetDlgCode);    { message handlers }
  begin
  inherited;
  Message.Result := DLGC_WANTARROWS or DLGC_WANTCHARS or DLGC_WANTALLKEYS;
  if fWantTabs and (not fDisplayOnly) then
                          Message.Result:= Message.Result or DLGC_WANTTAB
  end;

procedure TPlusMemo.WMCut(var Message: TMessage);         { message handlers }
begin
  CutToClipboard;
end;

procedure TPlusMemo.WMCopy(var Message: TMessage);        { message handlers }
begin
  CopyToClipboard;
end;

procedure TPlusMemo.WMPaste(var Message: TMessage);       { message handlers }
begin
  PasteFromClipboard;
end;

procedure TPlusMemo.WMUndo(var Message: TMessage);        { message handlers }
begin
  Undo;
end;


procedure TPlusMemo.WMVScroll(var m: TWMScroll);             { message handlers }
  begin
  m.Result:= 0;
  if fLockedCount>0 then Exit;
  with fDisplayTop do
   case m.ScrollCode of
    SB_LINEDOWN: if FirstVisibleLine<fLineCount-fDisplayLines+2 then FirstVisibleLine:= FirstVisibleLine+1;

    SB_LINEUP  : if fTopOrigin>0 then
                    if fTopOrigin mod fLineHeight<>0 then FirstVisibleLine:= FirstVisibleLine
                                                     else FirstVisibleLine:= FirstVisibleLine-1;
    SB_PAGEUP  : begin
                 ftmpnav1.Assign(fDisplayTop);
                 with ftmpnav1 do LineNumber:= MaxOf(LineNumber-fDisplayLines+2, 0);
                 FormatNow(ftmpnav1.ParNumber, ParNumber-1, False);
                 with ftmpnav1 do LineNumber:= MaxOf(fDisplayTop.LineNumber-fDisplayLines+2, 0);
                 FirstVisibleLine:= ftmpnav1.LineNumber
                 end;

    SB_PAGEDOWN: FirstVisibleLine:= MinOf(FirstVisibleLine+fDisplayLines-1, MaxOf(0, fLineCount-fDisplayLines+2));

    SB_THUMBTRACK:  if pmoNoFineScroll in Options then FirstVisibleLine:= Round(m.Pos*Longint(fVScrollFact)/fLineHeight)
                                                  else TopOrigin:= m.Pos*Longint(fVScrollFact)
    end;

  UpdateWindow(Handle);
  end;  { message WM_VSCROLL }

procedure TPlusMemo.WMHScroll(var m: TWMScroll);             { message handlers }
  begin
  m.Result:= 0;
  if fLockedCount>0 then Exit;
  case m.ScrollCode of
    SB_LINEDOWN: LeftOrigin:= LeftOrigin + ClientWidth div 20 + 1;
    SB_LINEUP  : LeftOrigin:= MaxOf(LeftOrigin - ClientWidth div 20 -1, 0);
    SB_PAGEUP  : LeftOrigin:= MaxOf(LeftOrigin - (ClientWidth - ClientWidth div 20), 0);
    SB_PAGEDOWN : LeftOrigin:= LeftOrigin + ClientWidth - ClientWidth div 20;
    SB_THUMBTRACK:  LeftOrigin:= m.Pos * fHScrollFact;
    end;
  UpdateWindow(Handle)
  end;

procedure TPlusMemo.WMSetCursor(var Message: TWMSetCursor);    { message handlers }
begin
  if (Message.CursorWnd=Handle) and (Message.HitTest=HTCLIENT) and (fStartLineSelection>=0) then
    begin
      WinProcs.SetCursor(PMRightArrowCur);
      Message.Result:= 1
    end
  else
      inherited
end;



{ message handlers }

procedure TPlusMemo.WMEraseBkgnd;
  begin
  Message.Result:= 1  { no need to paint background }
  end;

procedure TPlusMemo.WMPaint;

  var dc: hdc;
      InSel: Boolean;
      t: PChar;
      par: pParInfo;
      sstyle: TExtFontStyles;
      currentattr: TExtFontStyles;
      currentdyn: DynInfoRec;
      curdynnb  : Integer;
      sustart: Longint;
      stemppos: TPoint;
      sdisplaystartoff,
      sdisplaystopoff: Integer;

  procedure setColors;
    var t, b: Longint;
    begin
    t:= Font.Color;
    b:= -1;  { Transparent }
    if par^.Foregnd<>-1 then t:= par^.Foregnd;

    if TPlusFontStyle(fsHighlight) in TPlusFontStyles(sstyle) then
      begin
      t:=fHTColor;
      b:=fHBColor
      end;

    with currentdyn do
      if (DynStyle and $80 <>0) and ((Foregnd<>0) or (Backgnd<>0)) then
      begin
      if (par^.Foregnd=-1) and (Foregnd<>-1) then t:= Foregnd;
      if (par^.Backgnd=-1) and (Backgnd<>-1) then b:= Backgnd
      end;

    if InSel and (Focused or (not fHideSelection)) then
      begin
      t:= fSelTextColor;
      b:= -1
      end;

    SetTextColor(dc, ColorToRGB(t));
    if b<>-1 then
      begin
        SetBkMode(dc, Opaque);
        SetBkColor(dc, ColorToRGB(b))
      end
    else SetBkMode(dc, Transparent);
    end;

  procedure SetDC(var offset: PlusCardinal; maxoffset: PlusCardinal);
    var changed: Boolean; ss: TExtFontStyles; sdnb: Integer; slf: TLogFont;
    begin
    changed:= False;
    if StaticFormat then
      while (offset<maxoffset) and (t[offset]<=#26) and (t[offset] in CtrlCodesSet) do
        begin
        changed:= True;
        XORStyleCode(currentattr, t[offset]);
        Inc(offset)
        end;

    sdnb:= curdynnb;
    while (curdynnb<par^.DynCount) and (par^.DynBuffer^[curdynnb].DynOffset<=offset) do
      Inc(curdynnb);

    if sdnb<>curdynnb then
      begin
      changed:= True;
      currentdyn:= par^.DynBuffer^[curdynnb-1];
      end;

    if changed then
      begin
      ss:= sstyle;
      sstyle:= AttrToExtFontStyles(currentattr, currentdyn.DynStyle);
      if (TPlusFontStyles(ss)*[TPlusFontStyle(fsHighlight)]<>
         TPlusFontStyles(sstyle)*[TPlusFontStyle(fsHighlight)]) or (sdnb<>curdynnb) then SetColors;

      if (TPlusFontStyles(ss)-[TPlusFontStyle(fsHighlight)])<>
         (TPlusFontStyles(sstyle)-[TPlusFontStyle(fsHighlight)]) then
        begin
        SetupLogFontStyle(slf, sstyle);
        DeleteObject(SelectObject(dc, CreateFontIndirect(slf)))
        end
      end;

    if Byte(sstyle) and $80<>0 then
      begin
      if sustart=High(sustart) then sustart:= stemppos.X
      end
    else
      if sustart<>High(sustart) then
        begin
        WavyLine(dc, sustart, stemppos.X, stemppos.Y+1);
        sustart:= High(sustart)
        end;

    if sdisplaystartoff<sdisplaystopoff then
        if InSel then
            begin
            if offset>=sdisplaystopoff then
                begin
                InSel:= False;
                SetColors
                end
            end

        else
          if (offset>=sdisplaystartoff) and (offset<sdisplaystopoff) then
            begin
            InSel:= True;
            SetColors
            end

    end;

  procedure getwords(var offset: PlusCardinal; maxoffset: PlusCardinal);
    var soffset: PlusCardinal; st: PChar;
    begin
    if curdynnb<par^.DynCount then
      begin
      soffset:= par^.DynBuffer^[curdynnb].DynOffset;
      if maxoffset>soffset then maxoffset:= soffset
      end;

    soffset:= offset;
    if sdisplaystartoff<sdisplaystopoff then
      if soffset<sdisplaystartoff then
          begin
          if maxoffset>sdisplaystartoff then maxoffset:= sdisplaystartoff
          end
      else if soffset<sdisplaystopoff then
             if maxoffset>sdisplaystopoff then maxoffset:= sdisplaystopoff;

    st:= t;
    if StaticFormat then
      while (soffset<maxoffset) and
            ((st[soffset]>#26) or (not (st[soffset] in (CtrlCodesSet+[#9])))) do
        Inc(soffset)
    else
      while (soffset<maxoffset) and (st[soffset]<>#9) do
        Inc(soffset);

    offset:= soffset
    end;

var r, rp: TRect;
    currentpar: Longint;
    cw: Integer;
    ps: TPaintStruct;
    tm: TTextMetric;
    hdc1: hdc;
    oldbmp: HBitmap;
    leftstart, saveddcnb: Integer;
    lastpainty  : Integer;
    oldpen      : THandle;
    slinespacing: Single;
    spos        : TPoint;
    enddisp     : PlusCardinal;
    sbmp        : THandle;
    usebackground: Boolean;
    bgwidth, bgheight: Integer;
    bghandle    : THandle;
    fl, ll, i   : Longint;
    j, b        : PlusCardinal;
    k           : Integer;
    joff, koff  : Integer;
    sblockstartcoord, sblockstopcoord: Integer;
    sblockstartoff, sblockstopoff    : Integer;
    selstartoff, selstopoff          : Longint;
    StartSelCoord, StopSelCoord      : Integer;
    UseWindowsColors : Boolean;
    lin              : LineInfo;
    oldfont          : THandle;
    logfont          :  TLogFont;
    {$IFDEF PMDEBUG}
    dpaintmsg: string;
    {$ENDIF}

begin
saveddcnb:= 0; { to avoid warnings }
lastpainty:= 0;
slinespacing:= 0;

if fCanvas=nil then
    begin
    cw:= ClientWidth;
    spos.X:= 0;
    spos.Y:= 0;
    if Message.WParam<>0 then
      begin
      hdc1:= Message.WParam;
      with ps.rcpaint do
        begin
        Top:= 0;
        Left:= 0;
        Right:= cw;
        Bottom:= ClientHeight
        end;
      sBmp:= CreateCompatibleBitmap(hdc1, fLineBmpWidth, fLineHeight)
      end
    else
      begin
      hdc1:= BeginPaint(Handle, ps);
      sbmp:= fLineBmp
      end;

    if fNoPaint or (fLockedCount>0) then
      begin
      if Message.WParam=0 then EndPaint(Handle, ps);
      Exit
      end;

    dc:= CreateCompatibleDC(hdc1);
    Oldbmp:= SelectObject(dc, sbmp);
    currentpar:= fDisplayTop.ParNumber;
    ParseStartStopNow(currentpar);
    if not fDisplayTop.fPar^.Formatted then FormatNow(currentpar, currentpar, False);
    with ps.rcpaint do
      begin
      lastpainty:= Top;
      fl:= LineNb(Top) + fDisplayTop.LineNumber;
      if fl<0 then fl:= 0;
      ll:= LineNb((Bottom-1)) + fDisplayTop.LineNumber;
      if ll<0 then ll:= 0
      end;

    currentpar:= fDisplayTop.fParNb;
    par:= fDisplayTop.fPar;
    while (currentpar<fParagraphs.Count-1) and (par^.StartLine+par^.NbLines-1<fl) do
      begin
      Inc(currentpar);
      par:= fParagraphs.Pointers[currentpar];
      if not par^.Formatted then
        FormatNow(currentpar, currentpar, False);
      end;
    end { fCanvas=nil }

else
  begin
  fCanvas.Font:= Font;
  hdc1:= 0;
  oldbmp:= 0;
  dc:= fCanvas.Handle;  saveddcnb:= SaveDC(dc);
  if GetTextMetrics(dc, tm) then
    begin
    fLineHeight:= tm.tmHeight+1;
    fLineBase:= fLineHeight - tm.tmDescent-1;
    fSpaceWidth:= fCanvas.TextWidth(' ');
    end
  else
    begin
    fLineBase:= 0; fLineHeight:= 10;
    fSpaceWidth:= 10
    end;
  with pPaintInfo(Message.LParam)^ do
    begin
    cw:= fw;
    fl:= StartLine;
    ll:= StopLine;
    slinespacing:= LineSpacingPix;
    spos:= Position
    end;
  ftmpnav1.LineNumber:= fl;
  currentpar:= ftmpnav1.ParNumber;
  par:= ftmpnav1.fPar;
  sbmp:= 0
  end;    { fCanvas<>nil }

SetTextAlign(dc, ta_updatecp or ta_baseline);
oldfont:= 0;   { to avoid a warning }
UseWindowsColors:= pmoWindowsSelColors in Options;

usebackground:= (fBackground.Width>0) and (fBackground.Height>0);
if usebackground then
  begin
  bgwidth:= fBackground.Bitmap.Width;
  bgheight:= fBackground.Bitmap.Height;
  usebackground:= (bgwidth>0) and (bgheight>0)
  end
else
  begin
  bgwidth:= 0;
  bgheight:= 0;
  end;

if fl<fLineCount then
    begin
    {set font attributes and starting value in buffer }
    fMouseNav.NavLines.LLPar:= par;
    lin:= fMouseNav.fNavLines[fl-par^.StartLine];
    t:= par^.Text;
    curdynnb:= 0;
    while (curdynnb<par^.DynCount) and (par^.DynBuffer^[curdynnb].DynOffset<=lin.Start) do Inc(curdynnb);
    if curdynnb=0 then currentdyn:= par^.StartDynAttrib^
                  else currentdyn:= par^.DynBuffer^[curdynnb-1];
    currentattr:= lin.StartAttrib;
    sstyle:= AttrToExtFontStyles(currentattr, currentdyn.DynStyle);

    j:= lin.Start;
    if fSelStart.ParNumber<currentpar then selstartoff:= -1
    else if fSelStart.ParNumber>currentpar then selstartoff:= maxlongint
         else selstartoff:= fSelStart.ParOffset;
    if fSelStop.parNumber<currentpar then selstopoff:= -1
    else if fSelStop.ParNumber>currentpar then selstopoff:= maxlongint
         else selstopoff:= fSelStop.ParOffset;

    InSel:= UseWindowsColors and (j>=SelStartoff) and (j<SelStopoff);

    SetUpLogFontStyle(logfont, sstyle);
    oldfont:= SelectObject(dc, CreateFontIndirect(LogFont));
    oldpen:= SelectObject(dc, fSpecUnderlinePen.Handle);
    SetColors;
    sblockstartcoord:= High(sblockstartcoord);

    for i:= fl to ll do
      if i<fLineCount then
        begin
        if fCanvas=nil then
          begin
          r.Left:= 0;
          r.Top:= 0; r.Bottom:= fLineHeight; r.Right:= cw;
          if (usebackground) and (par^.Backgnd=-1) then
            begin
            bghandle:= fBackground.Bitmap.Canvas.Handle;
            j:= 0;
            joff:= (i*fLineHeight) mod bgheight;
            while j<r.Bottom do
              begin
              k:= 0;
              koff:= fDisplayLeft mod bgwidth;
              while k<r.Right do
                begin
                WinProcs.BitBlt(dc, k, j, bgwidth, fLineHeight, bghandle, koff, joff, SRCCOPY);
                Inc(k, bgwidth-koff);
                koff:= 0
                end;
              Inc(j, bgheight-joff);
              joff:= 0
              end
            end
          else      { not usebackground }
            begin
            if par^.Backgnd<>-1 then Brush.Color:= par^.Backgnd
                                else Brush.Color:= Color;
            WinProcs.FillRect(dc, r, Brush.Handle)
            end;
          r.Left:= fLeftMargin-fDisplayLeft;
          r.Right:= cw-fRightMargin
          end

        else    { fCanvas<>nil }
          begin
          r.Left:= spos.X+fLeftMargin;
          r.Top:= spos.Y + Round((i-fl)*slinespacing);
          r.Right:= cw+r.Left; r.Bottom:= r.Top+fLineHeight
          end;

        if (not fJustified) or (lin.JustifyStart>=lin.Stop) then
          case fAlignment of
            taRightJustify: r.Left:= r.Right - lin.LineWidth;
            taCenter: r.Left:= (r.Left+r.Right-lin.LineWidth) div 2
            end;
        leftstart:= r.Left+fDisplayLeft;
        stemppos:= r.TopLeft; Inc(stemppos.Y, fLineBase);
        j:= lin.Start;
        SetTextJustification(dc, 0, 0);
        if (fJustified) and (lin.JustifyStart=j) and (lin.Spaces>0) then
          SetTextJustification(dc, cw-fLeftMargin-fRightMargin-lin.LineWidth, lin.Spaces);

        { determine where to start and stop drawing the selection }
        startselcoord:= High(Startselcoord);
        stopselcoord:= startselcoord;
        sdisplaystartoff:= High(sdisplaystartoff);
        sdisplaystopoff:= High(sdisplaystopoff);

        if (lin.Start<=selstopoff) and (fSelLen<>0) and ((not fHideSelection) or Focused) then
          if fBlockSelection then
            begin
            if lin.Stop>=selstartoff then
              begin
              if sblockstartcoord=High(sblockstartcoord) then
                begin    { compute block horizontal extents }
                if fBlockStartCol<fBlockStopCol then
                  begin
                  sblockstartoff:= fBlockStartCol;
                  sblockstopoff:= fBlockStopCol
                  end
                else
                  begin
                  sblockstartoff:= fBlockStopCol;
                  sblockstopoff:= fBlockStartCol
                  end;
                sblockstartcoord:= sblockstartoff*fSpaceWidth-fDisplayLeft+fLeftMargin;
                sblockstopcoord:= sblockstopoff*fSpaceWidth-fDisplayLeft+fLeftMargin;
                end;
              startselcoord:= sblockstartcoord;
              stopselcoord:= sblockstopcoord;
              if UseWindowsColors then
                begin
                sdisplaystartoff:= ColToOffset(par, sblockstartoff, fTabStops, StaticFormat);
                sdisplaystopoff:= ColToOffset(par, sblockstopoff, fTabStops, StaticFormat);
                if InSel then
                  begin
                  InSel:= False;
                  SetColors
                  end
                end
              end
            end

          else { not Block Selection }
            begin
            if (lin.Start>=selstartoff) then
                begin
                if lin.Stop>=selstartoff then startselcoord:= ColPos(nil, currentpar, i-par^.StartLine, 0)
                end
            else
                if (lin.Stop>selstartoff) or
                   ((lin.Stop=selstartoff) and
                    (selstartoff=par^.Length) and
                    (selstopoff=High(selstopoff))) then
                            startselcoord:= ColPos(nil, currentpar, fSelStart.ParLine, fSelStart.Col);

            if (lin.Stop>selstopoff) or ((lin.Stop=selstopoff) and (i=par^.StartLine+par^.NbLines-1)) then
                        stopselcoord:= ColPos(nil, currentpar, fSelStop.ParLine, fSelStop.Col)
            else
              if pmoFullLineSelect in Options then stopselcoord:= High(stopselcoord)
                                              else stopselcoord:= ColPos(nil, currentpar, i-par^.StartLine, lin.Stop);
            fMouseNav.fNavLines.LLPar:= par;
            if UseWindowsColors then
              begin
              sdisplaystartoff:= selstartoff;
              sdisplaystopoff:= selstopoff
              end
            end;


        { put the background to clHighlight before drawing text }
        if UseWindowsColors and (startselcoord<>High(startselcoord)) then
          begin
          r.Left:= startselcoord;
          r.Right:= stopselcoord;
          Rect16(r);
          Brush.Color:= fSelBackColor;
          FillRect(dc, r, Brush.Handle);
          end;

        sustart:= High(sustart);

        { draw the text }
        repeat
        setDC(j, lin.Stop);
        if j<lin.Stop then
          begin
          b:= j;
          getWords(j, lin.Stop);

          enddisp:= j;
          if (j=lin.Stop) and (i<par^.StartLine+par^.NbLines-1) then
            begin
            while (enddisp>b) and (t[enddisp-1]=' ') do Dec(enddisp)
            end;

          PutText(dc, stemppos, t+b, enddisp-b, fMaxOneShotChars, cw+spos.X);

          if (j<lin.Stop) and (t[j]=#9) then
              begin
              SetDC(j, lin.Stop);
              r.Left:= stemppos.X;
              if fCanvas=nil then
                r.Right:= Leftstart-fDisplayLeft +
                        ((r.Left-leftstart+fDisplayLeft) div
                        (fTabStops*fSpaceWidth)+1)*(fTabStops*fSpaceWidth)
              else r.Right:= Leftstart-fDisplayLeft +
                        ((r.Left-leftstart+fDisplayLeft) div (fTabStops*fSpaceWidth)+1)
                          * (fTabStops*fSpaceWidth);
              if GetBkMode(dc)=OPAQUE then
                repeat
                  PutText(dc, stemppos, ' ', 1, 10, cw);
                until (stemppos.X>=r.Right) or (stemppos.X>=cw);
              SetDC(j, j);

              stemppos.X:= r.Right;
              Inc(j);
              if (fJustified) and (lin.JustifyStart<=j) and (lin.Spaces>0) then
                SetTextJustification(dc, cw-fLeftMargin-fRightMargin-lin.LineWidth, lin.Spaces);
              end;
          end

        until (j>=lin.Stop);

        if (Byte(sstyle) and $80<>0) and (sustart<>High(sustart)) then
            WavyLine(dc, sustart, stemppos.X, stemppos.Y+1);

        { finish drawing the highlight }
        if StartSelCoord<>High(StartSelCoord) then
          begin
          if not UseWindowsColors then
              begin
              r.Left:= StartSelCoord;
              r.Right:= StopSelCoord;
              Rect16(r);
              InvertRect(dc,  r)
              end
          else
            begin
            InSel:= lin.Stop<=selstopoff;
            if InSel then
                begin
                if lin.Stop=sdisplaystopoff then begin InSel:= False; SetColors end;
                if lin.Stop=sdisplaystartoff then SetColors
                end
            end;


          if fShowEndParSelected and (not ((pmoFullLineSelect in Options) or fBlockSelection)) and
             (SelStartoff<=j) and (j=par^.Length) and (j<SelStopoff) then
            begin
            rp.Top:= fLineHeight div 3; rp.Bottom:= 2*rp.Top;
            rp.Left:= StopSelCoord;
            rp.Right:= rp.Left+fSpaceWidth div 2 + 1;
            Rect16(r);
            if UseWindowsColors then
               begin
               Brush.Color:= fSelBackColor;
               FillRect(dc, rp, Brush.Handle);
               end
            else
               InvertRect(dc, rp)
            end
          end;


        { put it on the display }
        if fCanvas=nil then
          begin
          lastpainty:= LinePos(i-fDisplayTop.LineNumber);
          if not WinProcs.BitBlt(hdc1, 0, lastpainty, cw, fLineHeight, dc, 0, 0, SRCCopy) then
            Inc(lastpainty);
          Inc(lastpainty, fLineHeight)
          end;

        { finished for this line, go on with next one }
        if (i>=par^.StartLine+par^.NbLines-1) and (i<fLineCount-1) and (i<ll) then
            begin
            if curdynnb<par^.DynCount then SetDC(j, lin.Stop);
            Inc(currentpar);
            sustart:= par^.Foregnd;
            par:= fParagraphs.Pointers[currentpar];
            if sustart<>par^.Foregnd then SetColors;
            fMouseNav.fNavLines.LLPar:= par;
            curdynnb:= 0;
            if not par^.Formatted then
              begin
              FormatNow(currentpar, currentpar, False);
              fMouseNav.fNavLines.LLPar:= par
              end;
            t:= par^.Text;
            if currentpar=fSelStart.ParNumber then selstartoff:= fSelStart.ParOffset
                else if currentpar<fSelStart.ParNumber then selstartoff:= maxlongint
                     else selstartoff:= -1;
            if currentpar=fSelStop.ParNumber then selstopoff:= fSelStop.ParOffset
            else if currentpar<fSelStop.ParNumber then selstopoff:= maxlongint
                 else selstopoff:= -1
            end;
        if (i<fLineCount-1) and (i<ll) then
          lin:= fMouseNav.fNavLines[i+1-par^.StartLine]

        end;

    fMouseNav.fNavLines.LLPar:= fMouseNav.fPar
    end;

  if fl<fLineCount then
    begin
    DeleteObject(SelectObject(dc, oldfont));
    SelectObject(dc, oldpen)
    end;

  if fCanvas=nil then
    begin
    r:= ps.rcPaint;
    r.Top:= lastpainty;
    if usebackground then
      begin
      bghandle:= fBackground.Bitmap.Canvas.Handle;
      j:= r.Top;
      joff:= (fTopOrigin+lastpainty) mod bgheight;
      while j<r.Bottom do
        begin
        k:= 0;
        koff:= fDisplayLeft mod bgwidth;
        while k<r.Right do
          begin
          WinProcs.BitBlt(hdc1, k, j, bgwidth, bgheight, bghandle, koff, joff, SRCCOPY);
          Inc(k, bgwidth-koff);
          koff:= 0
          end;
        Inc(j, bgheight-joff);
        joff:= 0
        end
      end
    else
      begin
      Brush.Color:= Color;
      WinProcs.FillRect(hdc1, r, Brush.Handle)
      end;
    if (fEndOfTextPen.Color<>-1) and (ll>=fLineCount) and (fl<=fLineCount) then
      begin
      oldpen:= SelectObject(hdc1, fEndOfTextPen.Handle);
      r.Top:= fLineCount*fLineHeight - fTopOrigin + fEndOfTextPen.Width div 2;
      WinProcs.MoveToEx(hdc1, 0, r.Top, nil);
      WinProcs.LineTo(hdc1, r.Right, r.Top);
      SelectObject(hdc1, oldpen);
      end;
    SelectObject(dc, Oldbmp);
    DeleteDC(dc);
    if Message.WParam=0 then EndPaint(Handle, ps)
                        else DeleteObject(sbmp)
    end
  else RestoreDC(dc, saveddcnb);
  end;      { message WM_PAINT }

{$IFDEF WIN32}
procedure TPlusMemo.WMMouseWheel(var m: TMessage);    { message handlers }
  var scrollDir: Longint;
  begin
    scrollDir := m.wParam shr 16;
    if scrollDir>$7fff then scrollDir:= scrollDir - $10000;
    Inc(fMouseWheelAcc, scrolldir*fMouseWheelFact);
    while fMouseWheelAcc<=-120 do
      begin
      SendMessage(Handle, WM_VSCROLL, SB_LINEDOWN, 0);
      Inc(fMouseWheelAcc, 120)
      end;

    while fMouseWheelAcc>=120 do
      begin
      SendMessage(Handle, WM_VSCROLL, SB_LINEUP, 0);
      Dec(fMouseWheelAcc, 120)
      end;
  end;
{$ENDIF}

procedure TPlusMemo.CMFontChanged(var Message: TMessage);   { message handlers }
  var tm: TTextMetric;
  begin
  inherited;
  if fAutoLineHeight then fLineHeight:= abs(Font.Height)+1;
  if HandleAllocated then
    begin
    Canvas.Font:= Font;
    GetTextMetrics(Canvas.Handle, tm);
    if fAutoLineHeight then fLineHeight:= tm.tmHeight+1;
    fLineBase:= fLineHeight - tm.tmDescent-1;

    with tm do
      if fLineBase<tmHeight-tmInternalLeading-tmDescent then
        begin
        fLineBase:= fLineHeight-tmInternalLeading;
        if fLineBase<tmHeight-tmDescent-tmInternalLeading then
            fLineBase:= fLineHeight
        end;

    fDisplayLines:= ClientHeight div fLineHeight + 1;
    fSpaceWidth:= Canvas.TextWidth(' ');
    fMaxOneShotChars:= $4000 div tm.tmMaxCharWidth;
    DeleteObject(fLineBmp);
    fLineBmpWidth:= ClientWidth;
    fLineBmp:= CreateCompatibleBitmap(Canvas.Handle, fLineBmpWidth, fLineHeight);
    fMaxLineWidth:= High(fMaxLineWidth);
    if not fNoFormat then Reformat;
    fDisplayLeft:= 0;
    fTopOrigin:= 0;
    fDisplayTop.LineNumber:= 0;
    SetVScrollParams;
    SetHScrollParams;
    if fAutoCaretWidth then CaretWidth:= 0
                       else CaretWidth:= CaretWidth;
    if fVScrollBar then SetScrollPos(Handle, SB_VERT, fTopOrigin div fVScrollFact, True);
    if fHScrollBar then SetScrollPos(Handle, SB_HORZ, 0, True);
    if Assigned(fOnVScroll) then fOnVScroll(Self);
    UpdateCaret(False)
    end;
  end;

procedure TPlusMemo.Cmctl3dchanged;
  begin
  inherited;
  RecreateWnd;
  end;

function TPlusMemo.AttrToExtFontStyles(StaticAttrib: TExtFontStyles; DynAttrib: Word): TExtFontStyles;
  var newattr: Byte; fontstyle: TFontStyles;
  begin
  if DynAttrib and $80<>0 then
    if DynAttrib and $18=$18 then Byte(Result):= DynAttrib and $a7
                             else Byte(Result):= DynAttrib and $3f

  else
    begin
    fontstyle:= Font.Style;
    newattr:= Byte(StaticAttrib) xor Byte(fontstyle);
    Result:= TExtFontStyles(newattr)
    end
  end;

procedure TPlusMemo.SetupLogfontStyle(var lf: TLogFont; style: TExtFontStyles);
  var f: TFont;
  begin
  with lf do
    begin
    if TPlusFontStyle(fsAltFont) in TPlusFontStyles(style) then f:= fAltFont
                                                           else f:= Font;
    if (fCanvas<>nil) and (f.Height<0) then
           begin lfHeight:= f.Height*fCanvas.Font.PixelsPerInch div f.PixelsPerInch end
    else lfHeight:= f.Height;
    StrPCopy(lfFaceName, f.Name);

    lfWidth := 0; { have font mapper choose }
    lfEscapement := 0; { only straight fonts }
    lfOrientation := 0; { no rotation }
    if fsBold in Style then lfWeight := FW_BOLD
                       else lfWeight := FW_NORMAL;
    lfItalic := Byte(fsItalic in Style);
    lfUnderline := Byte(fsUnderline in Style);
    lfStrikeOut := Byte(fsStrikeOut in Style);
    lfCharSet := {$IFDEF WIN32} {$IFNDEF VER90} f.Charset; {$ELSE} fCharSet; {$ENDIF} {$ELSE} fCharSet; {$ENDIF}
    lfQuality := DEFAULT_QUALITY;
        { Everything else as default }
    lfOutPrecision := OUT_DEFAULT_PRECIS;
    lfClipPrecision := CLIP_DEFAULT_PRECIS;
    case f.Pitch of
          fpVariable: lfPitchAndFamily := VARIABLE_PITCH;
          fpFixed: lfPitchAndFamily := FIXED_PITCH;
        else
          lfPitchAndFamily := DEFAULT_PITCH;
        end;
    end
  end;

procedure TPlusMemo.setHScrollParams;
  {$IFDEF WIN32} var sc: TScrollInfo;  {$ENDIF}
  begin
  fHScrollfact:= (fMaxLineWidth+fLeftMargin+fRightMargin) div Longint(32760) + 1;
  fInSetScroll:= True;   { avoid SetBounds calling this method }
  if fHScrollBar then
    {$IFDEF WIN32}
    with sc do
      begin
      cbSize:= SizeOf(sc);
      fMask:=  sif_range or sif_page;
      nMin:= 0;
      nPage:= (ClientWidth-fLeftMargin-fRightMargin) div fHScrollfact;
      nMax:= MaxOf((fMaxLineWidth+fLeftmargin+fRightMargin-ClientWidth) div fHScrollfact, 0)+Integer(nPage)-1;
      if WordWrap then nMax:= nPage-1;
      if not (pmoAutoScrollBars in Options) then Inc(nMax);
      if fAlignment in [taRightJustify, taCenter] then nMax:= nPage;
      SetScrollInfo(Handle, sb_horz, sc, False);
      if nMax<Integer(nPage) then LeftOrigin:= 0
      end;
    {$ELSE}

    if fAlignment in [taRightJustify, taCenter] then
         SetScrollRange(Handle, SB_HORZ, 0, 0, False)
    else SetScrollRange(Handle, SB_HORZ, 0,
                   MaxOf((fMaxLineWidth+fLeftmargin+fRightMargin-ClientWidth) div
                          fHScrollfact+1,
                          1),
                   False);
    {$ENDIF}
  if fLineHeight>0 then fDisplayLines:= ClientHeight div fLineHeight + 1
                   else fDisplayLines:= 1;
  fInSetScroll:= False
  end;

procedure TPlusMemo.SetVScrollParams;
  {$IFDEF WIN32} var sc: TScrollInfo;
  {$ELSE}        var min,  max: Integer; {$ENDIF}
  var newmax, newpage: Integer;
  begin
  fVscrollfact:= (fLineCount*fLineHeight) div 32760 + 1;
  fVScrollChange:= False;
  fInSetScroll:= True;
  newmax:= 0; newpage:= 0;
  if fVScrollBar then
    {$IFDEF WIN32}
    with sc do
      begin
      cbSize:= SizeOf(sc);
      fMask:=  sif_range or sif_page;
      nMin:= 0;
      GetScrollInfo(Handle, sb_vert, sc);
      newMax:= MaxOf((fLineCount*fLineHeight) div fVScrollFact, ClientHeight-fLineHeight-1);
      if (not (pmoAutoScrollBars in Options)) and (newMax < ClientHeight) then Inc(newMax);
      newPage:= (ClientHeight - fLineHeight) div fVScrollFact;
      if (newMax<>nMax) or (newPage<>Integer(nPage)) then
        begin
        nPage:= newPage;
        nMax:= newMax;
        SetScrollInfo(Handle, sb_vert, sc, True);
        end
      end;

    {$ELSE}
    begin
    GetScrollRange(Handle, sb_VERT,  min, max);
    newmax:= MaxOf((fLineCount*fLineHeight-ClientHeight+2*fLineHeight) div fVScrollfact, 2*fLineHeight);
    if newmax<>max then SetScrollRange(Handle, sb_VERT, 0, newmax, False)
    end;
    {$ENDIF}

  if ClientWidth<>fLineBmpWidth then
       begin
       { recreate internal drawing bmp with proper width }
       fVScrollChange:= True;
       DeleteObject(fLineBmp);
       fLineBmpWidth:= ClientWidth;
       fLineBmp:= CreateCompatibleBitmap(Canvas.Handle, fLineBmpWidth, fLineHeight);
       if fWordWrap or (fAlignment<>taLeftJustify) then
         begin
         {fUpdating:= True;} Reformat;
         Invalidate;
         end
       else
         SetHScrollParams;
       fInSetScroll:= True;
       if (not fVScrollBar) or (newMax<newPage) then FirstVisibleLine:= 0
       end;
  fInSetScroll:= False
  end;

function TPlusMemo.GetUpText(Par: pParInfo; ParNb: Longint; Start, Len: PlusCardinal): PChar;
  var newlen: PlusCardinal; newupt: PChar; frontadd: Integer;
      upproc: TpmUpperCaseProc;
  begin
  upproc:= @StrUpper;
  case UpperCaseType of
    pmuAnsi: upproc:= pmCharUpper;
    pmuUserDefined: if Assigned(UserUpperCaseProc) then upproc:= UserUpperCaseProc
    end;

  if Par=nil then Par:= fParagraphs.Pointers[ParNb];
  if (fUpParNb<>ParNb) or (fUpOffset>Start+Len) or (fUpOffset+fUpLength<Start) then
    begin   { start with a completely new buffer }
    if fUpText<>nil then StrDispose(fUpText);
    fUpText:= StrAlloc(Len+1);
    Move(Par^.Text[Start], fUpText^, Len);
    fUpText[Len]:= #0;
    upproc(fUpText);
    fUpOffset:= Start;
    fUpLength:= Len
    end

  else
    begin  { just expand existing buffer if needed }
    frontadd:= fUpOffset-Start;
    if frontadd>0 then
      begin
      newlen:= MaxOf(Len, fUpLength+frontadd);
      newupt:= StrAlloc(newlen+1);
      Move(Par^.Text[Start], newupt^, frontadd);
      newupt[frontadd]:= #0;
      upproc(newupt);
      Move(fUpText^, newupt[frontadd], fUpLength+1);
      if newlen>fUpLength+frontadd then
        begin
        Move(Par^.Text[fUpOffset+fUpLength], newupt[fUpLength+frontadd], newlen-(fUpLength+frontadd));
        newupt[newlen]:= #0;
        upproc(newupt + (frontadd+fUpLength))
        end;
      StrDispose(fUpText);
      fUpText:= newupt;
      fUpOffset:= Start;
      fUpLength:= newlen
      end
    else
      if Len>fUpLength+frontadd then
        begin
        newlen:= Len - frontadd;
        newupt:= StrAlloc(newlen+1);
        Move(fUpText^, newupt^, fUpLength);
        Move(Par^.Text[fUpOffset+fUpLength], newupt[fUpLength], newlen-fUpLength);
        newupt[newlen]:= #0;
        upproc(newupt + fUpLength);
        StrDispose(fUpText);
        fUpText:= newupt;
        fUpLength:= newlen
        end
      end;

  Result:= fUpText+Start-fUpOffset
  end;

procedure TPlusMemo.BackgroundChange(Sender: TObject);
  begin
  Invalidate
  end;

procedure Register;
begin
  RegisterComponents('ACNCComps', [TPlusMemo]);
end;


initialization
MemoCount:= 0;
ClipboardBlockFormat:= 0;


{$IFDEF PMDEBUG}
debugwin:= FindWindow('Notepad', nil);
debugwin:= GetWindow(debugwin, GW_Child)
{$ENDIF}



end.
















