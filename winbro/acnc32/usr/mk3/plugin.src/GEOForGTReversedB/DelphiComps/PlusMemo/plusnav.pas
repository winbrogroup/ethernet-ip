

{$IFDEF INTERFACE}

    TPlusNavigator = class    { a class used to navigate in a TPlusMemo }
        private                                     { internal fields are computed only when needed, }
          fOnFree: TNotifyEvent;                    { with special values indicating yet undetermined }
          function  GetOffset: PlusCardinal;        
          procedure setParOffset(off: PlusCardinal);
          function  GetDynNb : PlusCardinal;
          procedure SetPos(p: Longint);
          function  GetParNb: Longint;
          procedure setParNb(pnb: Longint);
          function  GetLineNb: Longint;
          procedure SetLineNb(l: Longint);
          function  GetColNb: PlusCardinal;
          procedure SetColNb(c: PlusCardinal);
          function  GetParLine: PlusCardinal;
          procedure SetParLine(pl: PlusCardinal);
          function  GetPar: pParInfo;
          function  GetText: Char;
          function  GetDynInfo: DynInfoRec;
          function  GetpDynInfo: pDynInfoRec;
          function  GetStyle: TFontStyles;
          function  GetNavLines: TLinesList;
          function  GetContext: Integer;
          function  GetDisplayPos: TPoint;
          procedure SetDisplayPos(DispPos: TPoint);

        protected                 { fields beginning with 'f' are internal fields, yet made public for the sake of efficiency }
                                  { in accessory components.  Playing with them directly implies you know what you do! }
        public
          fPMemo: TComponent;                    { the TPlusMemo this navigator is attached to }
          fPos  : Longint;                       { the position of this nav. (offset in chars from the start of text }
          fPar  : pParInfo;                      { an unknown paragraph is indicated by both fPar=nil }
          fParNb: Longint;                       { and fParNb=High(PlusCardinal)}
          fOffset, fDynNb  : PlusCardinal;       { unknown dynattributes indicated by fDynNb=High(PlusCardinal)}
          fParLine         : PlusCardinal;       { line number in current paragraph corresponding to position fPos }
          fNavLines        : TLinesList;         { a TLinesList is created and used internally whenever necessary }

          constructor Create(APlusMemo: TComponent);
          destructor  Destroy; override;
          procedure AddDyn(const dyn: DynInfoRec);
          procedure Assign(Source: TPlusNavigator);
          function  BackToDyn(Min: Longint): Boolean;
          function  ForwardToDyn(Max: Longint): Boolean;
          procedure GetTextBuf(Buffer: PChar; Len: PlusCardinal); { Buffer must be at least Len+1 long }
          procedure Invalidate;
          procedure RemoveDyn;
          procedure RightOfDyn;
          property DisplayPos: TPoint read getDisplayPos write setDisplayPos;
          property Pos: Longint read fPos write setPos;
          property ParNumber: Longint read getParNb write setParNb;
          property LineNumber: Longint read getLineNb write setLineNb;
          property Col: PlusCardinal read getColNb write setColNb;
          property ParOffset: PlusCardinal read getOffset write setParOffset;
          property ParLine: PlusCardinal read getParLine write setParLine;
          property Text: Char read getText;
          property Style: TFontStyles read GetStyle;
          property DynNb: PlusCardinal read GetDynNb;
          property Par: pParInfo read GetPar;
          property DynAttr: DynInfoRec read GetDynInfo;
          property pDynAttr: pDynInfoRec read GetpDynInfo;
          property Context: Integer read GetContext;
          property NavLines: TLinesList read GetNavLines;
          property OnFree: TNotifyEvent read fOnFree write fOnFree;
        end;

procedure InvalidateNavs(NavList: TList; startpos, lastpar: Longint);

{$ELSE}

type TProtPMemo = {$IFNDEF WIN32} class(TPlusMemo);
                  {$ELSE}         TPlusMemo;
                  {$ENDIF}
    { this class defined in order to have access to protected fields
      of TPlusMemo from within TPlusNavigator under Delphi 1 }

procedure InvalidateNavs(NavList: TList; startpos, lastpar: Longint);
  var i: Integer;
  begin
  for i:= 0 to NavList.Count-1 do
  with TPlusNavigator(NavList[i]) do
    if (fPar<>nil) and (Pos>StartPos) and (fParNb<=lastpar) then
        begin
        fParLine:= High(PlusCardinal);
        fDynNb:= High(PlusCardinal)
        end
  end;


constructor TPlusNavigator.Create(APlusMemo: TComponent);
  begin
  inherited  Create;
  if APlusMemo<>nil then
    begin
    if not (APlusMemo is TPlusMemo) then
      raise Exception.Create('Internal error: component is not a TPlusMemo');
    fPMemo:= APlusMemo;
    TProtPMemo(fPMemo).fNavigators.Add(Self)
    end;
  fParNb:= -1;
  fDynNb:= High(PlusCardinal);   { flag as non valid }
  fParLine:= High(PlusCardinal)
  end;

destructor TPlusNavigator.Destroy;
  begin
  if fPMemo<>nil then TProtPMemo(fPMemo).fNavigators.Remove(Self);
  fNavLines.Free;
  if Assigned(fOnFree) then fOnFree(Self);
  inherited Destroy
  end;

procedure TPlusNavigator.AddDyn(const dyn: DynInfoRec);
 var  needed: PlusCardinal;
 begin
 with Par^ do
   begin
   needed:= DynCount+1;
   if needed>DynBufLen then
     begin
     Inc(needed, needed div 64);
      {$IFNDEF WIN32} pmReAllocMem(DynBuffer,  DynBufLen*DCodeLen, needed*DCodeLen);
      {$ELSE}         ReAllocMem(DynBuffer, needed*DCodeLen); {$ENDIF}
     DynBufLen:= needed;
     end;

   fDynNb:= DynNb;  { just to have it compute internal fDynNb }
   Move(DynBuffer^[fDynNb], DynBuffer^[fDynNb+1], (DynCount-fDynNb)*DCodeLen );
   DynBuffer^[fDynNb]:= dyn;
   DynBuffer^[fDynNb].DynOffset:= fOffset;
   Inc(fDynNb);
   Inc(DynCount)
   end
 end;  { procedure adddyn }

procedure TPlusNavigator.RemoveDyn;
 begin
 with Par^ do
   begin
   fDynNb:= DynNb;   { just to have it compute its internal field }
   if DynBuffer^[fDynNb].DynOffset<>fOffset then raise Exception.Create('Internal PlusMemo error');
   System.Move(DynBuffer^[fDynNb+1], DynBuffer^[fDynNb], (DynCount-fDynNb-1)*DCodeLen);
   Dec(DynCount);
  end;
 fParLine:= High(PlusCardinal)
 end;


function TPlusNavigator.GetParNb: Longint;
  var pcount, factp, fact: Longint;
  begin
  if fPar=nil then
    with TProtPMemo(fPMemo) do
      begin
      fParLine:= High(PlusCardinal);
      fDynNb:= High(PlusCardinal);
      pcount:= fParagraphs.Count-1;
      if fTextLen=0 then fParNb:= 0
      else  if (fPos>=$8000) or (pcount>=$8000) then
                begin
                fact:= fPos div $8000+1;
                factp:= pcount div $8000 +1;
                fParNb:= ((((pcount-1) div factp)*(fPos div fact)) div fTextLen) * fact *factp
                end
            else fParNb:= ((pcount-1)*fPos) div fTextLen;
      if fParNb<0 then fParNb:= 0;
      if fParNb>pcount then fParNb:= pcount;
      fPar:= fParagraphs.Pointers[fParNb];
      if fPos<fPar^.StartOffset then
        begin
        repeat
          Dec(fParNb);
          fPar:= fParagraphs.Pointers[fParNb]
        until fPos>=fPar^.StartOffset
        end
      else
          while fPos>fPar^.StartOffset+fPar^.Length+1 do
            begin
            Inc(fParNb);
            fPar:= fParagraphs.Pointers[fParNb]
            end;
      if fNavLines<>nil then fNavLines.LLPar:= fPar;
      fOffset:= fPos-fPar^.StartOffset
      end;  { with fpMemo } 
  Result:= fParNb
  end;  { method getParNb }

procedure TPlusNavigator.SetParNb(pnb: Longint);
  begin
  Pos:= TProtPMemo(fPMemo).fParagraphs.ParPointers[pnb]^.StartOffset
  end;


function TPlusNavigator.GetNavLines: TLinesList;
  begin
  if fNavLines=nil then
    begin
    fNavLines:= TLinesList.Create;
    fNavLines.LLPar:= Par
    end;
  Result:= fNavLines
  end;

function TPlusNavigator.GetContext: Integer;
  var pdyn: pDynInfoRec;
  begin
  pdyn:= pDynAttr;
  if pdyn^.DynStyle<>0 then Result:= pdyn^.Context
                       else Result:= 0
  end;

function TPlusNavigator.GetPar: pParInfo;
  begin
  if fPar=nil then GetParNb;
  Result:= fPar
  end;

function TPlusNavigator.GetOffset: PlusCardinal;
  begin
  if fPar=nil then GetParNb
  else
    if fOffset=High(PlusCardinal) then fOffset:= fPos - fPar^.StartOffset;
  Result:= fOffset
  end;

procedure TPlusNavigator.setParOffset(off: PlusCardinal);
  begin
  if off>Par^.Length then off:= fPar^.Length;
  Pos:= fPar^.StartOffset + off
  end;

function TPlusNavigator.GetDynNb: PlusCardinal;
  var illoff: PlusCardinal;
  begin
  if fDynNb=High(PlusCardinal) then
    begin
    illoff:= ParOffset;
    fDynNb:= 0;
    while (fDynNb<fPar^.DynCount) and (fPar^.DynBuffer^[fDynNb].DynOffset<illoff) do Inc(fDynNb);
    end;
  Result:= fDynNb
  end;

function TPlusNavigator.GetLineNb: Longint;
  begin
  Result:= ParLine + Par^.StartLine;
  end;

procedure TPlusNavigator.SetLineNb(l: Longint);
  var pcount, lcount, pnb: Longint;
      pmemo       : TProtPMemo;
      parp        : pParInfo;
      factl, factp: Longint;
  begin
  pmemo:= TProtPMemo(fPMemo);
  pcount:= PMemo.ParagraphCount;
  lcount:= PMemo.fLineCount;
  if l>=lcount then l:= lcount-1;
  if (l>=$8000) or (pcount>$8000) then
    begin
    factl:= l div $8000 + 1;
    factp:= pcount div $8000 + 1;
    pnb:= (((l div factl)*(pcount div factp)) div lcount) * factl*factp
    end
  else pnb:= (l*pcount) div lcount;
  if pnb<0 then pnb:= 0;
  if pnb>=pcount then pnb:= pcount-1;
  with PMemo.fParagraphs do
    begin
    parp:= Pointers[pnb];
    while parp^.StartLine>l do begin Dec(pnb); parp:= Pointers[pnb] end;
    while parp^.StartLine+parp^.NbLines<=l do
      begin
      Inc(pnb);
      parp:= Pointers[pnb]
      end;
    fPar:= parp;
    if fNavLines<>nil then fNavLines.LLPar:= fPar;
    fParNb:= pnb;
    fParLine:= l-parp^.StartLine;

    with NavLines.LinePointers[fParLine]^ do
      begin
      fOffset:= Start;
      end;
    fPos:= parp^.StartOffset+fOffset;
    fDynNb:= High(PlusCardinal);
    end   { with pmemo.fParagraphs }
  end;   { method setLineNb }



function TPlusNavigator.GetColNb: PlusCardinal;
  begin
  Result:= ParOffset - NavLines.LinePointers[ParLine]^.Start;
  end;

procedure TPlusNavigator.SetColNb(c: PlusCardinal);
  begin
  with NavLines.LinePointers[ParLine]^ do
    begin
    if c>Stop-Start then c:= Stop-Start;
    fOffset:= Start + c
    end;
  fPos:= fPar^.StartOffset + fOffset;
  fDynNb:= High(PlusCardinal);
  end;

procedure TPlusNavigator.SetPos(p: Longint);
  begin
  if (fParNb<>-1) and (fPar<>nil) then
    with fPar^ do
      if (p<fPos) or (p>StartOffset+Length+1) then
        begin  { change of paragraph or going backward : mark as invalid }
        fDynNb:= High(PlusCardinal);
        fParLine:= High(PlusCardinal);
        if (p<StartOffset) or (p>StartOffset+Length+1) then fPar:= nil
                                                       else fOffset:= p-StartOffset
        end
      else
        begin  { same paragraph and going forward: update internal fields }
        fOffset:= p-StartOffset;

        if fDynNb<>High(PlusCardinal) then
          while (fDynNb<DynCount) and (DynBuffer^[fDynNb].DynOffset<fOffset) do Inc(fDynNb);

        if fParLine<>High(PlusCardinal) then
          while (fParLine<NavLines.Count-1) and (fOffset>=fNavLines.LinePointers[fParLine]^.Stop) do Inc(fParLine)

        end;   { same paragraph }

  fPos:= p
  end;   { TPlusNavigator.SetPos }

function TPlusNavigator.GetParLine:  PlusCardinal;
  var linecount: Integer;
  begin
  if fParLine=High(PlusCardinal) then
    begin
    with Par^ do
      begin
      fParLine:= 0;
      linecount:= NavLines.Count-1;
      while (fParLine<linecount) and (fOffset>=fNavLines.LinePointers[fParLine]^.Stop) do Inc(fParLine);
      end;
    end;
  Result:= fParLine
  end;   { TPlusNavigator.getParLine }

procedure TPlusNavigator.SetParLine(pl: PlusCardinal);
  begin
  if pl>=Par^.NbLines then pl:= fPar^.NbLines-1;
  LineNumber:= fPar^.StartLine + pl
  end;

function TPlusNavigator.GetText: Char;
  begin
  with Par^ do
    if fOffset>=Length then
      if fOffset=Length then Result:= #13
                        else Result:= #10
    else Result:= Text[fOffset]
  end;

function TPlusNavigator.BackToDyn(Min: Longint): Boolean;
  var cdyn: PlusCardinal;
      pn: Longint;
      pp: pParInfo;
      pmemo: TProtPMemo;
  begin
  pmemo:= TProtPMemo(fPMemo);
  pn:= ParNumber;
  pp:= fPar;
  Result:= False;
  cdyn:= DynNb;
  repeat
    if cdyn=0 then
      begin
      if (pn=0) or (pp^.StartOffset<=Min) then Exit;
      Dec(pn);
      pp:= PMemo.fParagraphs.Pointers[pn];
      cdyn:= pp^.DynCount
      end
    else Result:= True
  until Result;

  if pp^.StartOffset+pp^.DynBuffer^[cdyn-1].DynOffset<Min then
    begin
    Result:= False;
    Exit
    end;

  Result:= True;
  fPar:= pp;
  fParNb:= pn;
  if fNavLines<>nil then fNavLines.LLPar:= pp;
  fDynNb:= cdyn-1;
  fOffset:= pp^.DynBuffer^[fDynNb].DynOffset;
  fPos:= pp^.StartOffset+fOffset;
  fParLine:= High(PlusCardinal);
  end;  { method BackToDyn }

function TPlusNavigator.ForwardToDyn(Max: Longint): Boolean;
  var cdyn : PlusCardinal;
      pn   : Longint;
      pp   : pParInfo;
      pmemo: TProtPMemo;
  begin
  pmemo:= TProtPMemo(fPMemo);
  Result:= False;
  pn:= ParNumber;
  pp:= fPar;
  cdyn:= DynNb;
  repeat
    if cdyn<pp^.DynCount then Result:= True
    else
      begin
      if (pn<PMemo.fParagraphs.Count-1) and (pp^.StartOffset+pp^.Length<Max) then
        begin
        Inc(pn);
        pp:= PMemo.fParagraphs.Pointers[pn];
        cdyn:= 0
        end
      else Exit
      end
  until Result;

  if pp^.DynBuffer^[cdyn].DynOffset+pp^.StartOffset<=Max then
    begin
    fPar:= pp;
    if fNavLines<>nil then fNavLines.LLPar:= pp;
    fDynNb:= cdyn;
    fParNb:= pn;
    fOffset:= pp^.DynBuffer^[cdyn].DynOffset;
    fPos:= pp^.StartOffset+fOffset;
    fParLine:= High(PlusCardinal)
    end
  else Result:= False
  end;  { method ForwardToDyn }

procedure TPlusNavigator.RightOfDyn;
  begin
  while (DynNb<fPar^.DynCount) and (fOffset=fPar^.DynBuffer^[fDynNb].DynOffset) do Inc(fDynNb)
  end;

function TPlusNavigator.GetpDynInfo: pDynInfoRec;
  begin
  if DynNb=0 then Result:= fPar^.StartDynAttrib
             else Result:= @fPar^.DynBuffer^[fDynNb-1]
  end;

function TPlusNavigator.GetDynInfo: DynInfoRec;
  begin
  Result:= pDynAttr^
  end;

function TPlusNavigator.GetStyle: TFontStyles;
  var off: PlusCardinal; c: Char;
  begin
  if not TPlusMemo(fPMemo).StaticFormat then
    begin
    Result:= [];
    Exit
    end;
  with NavLines.LinePointers[ParLine]^ do
    begin
    Result:= StartAttrib;
    off:= Start
    end;
  while off<fOffset do
    begin
    c:= fPar^.Text[off];
    if (c<#26) and (c in CtrlCodesSet) then XORStyleCode(TExtFontStyles(Result), c);
    Inc(off)
    end
  end;

procedure TPlusNavigator.Assign(Source: TPlusNavigator);
  begin
  fPos := Source.fPos;
  fPar := Source.fPar;
  if fNavLines<>nil then fNavLines.LLPar:= fPar;
  fParNb:= Source.fParNb;
  fParLine:= Source.fParLine;
  fOffset:= Source.fOffset;
  fDynNb := Source.fDynNb;
  end;

procedure TPlusNavigator.GetTextBuf(Buffer: PChar; Len: PlusCardinal);
  var moved, count,
      startoff     : PlusCardinal;
      parnb        : Longint;
      spar         : pParInfo;
      tb           : PChar;
  begin
  moved:= 0;
  parnb:= ParNumber;
  spar:= fPar;
  startoff:= ParOffset;
  tb:= Buffer;

  if startoff>=spar^.Length then
    begin
    if (startoff=spar^.Length) and (Len>0) then
      begin
      tb^:= #13;
      moved:= 1;
      Inc(tb)
      end;
    if Len>moved then
      begin
      tb^:= #10;
      Inc(tb);
      Inc(moved);
      Inc(parnb);
      spar:= TProtPMemo(fPMemo).fParagraphs.Pointers[parnb];
      startoff:= 0
      end
    end;

  while moved<Len do
    begin
    if Len-moved>spar^.Length-startoff then count:= spar^.Length-startoff
                                       else count:= Len-moved;
    if count>0 then System.Move(spar^.Text[startoff], tb^, count);
    Inc(moved, count);
    Inc(tb, count);

    if moved<Len then begin
                      tb^:= #13;
                      inc(moved);
                      Inc(tb)
                      end;
    if moved<Len then begin
                      tb^:= #10;
                      Inc(moved);
                      Inc(tb);
                      Inc(parnb);
                      spar:= TProtPMemo(fPMemo).fParagraphs.Pointers[parnb];
                      startoff:= 0;
                      end
    end;
  Buffer[moved]:= #0;
  end;  { method GetTextBufP }

procedure TPlusNavigator.Invalidate;
  begin
  fPar:= nil;
  fParNb:= -1;
  fParLine:= High(PlusCardinal);
  fDynNb:= High(PlusCardinal)
  end;

function  TPlusNavigator.GetDisplayPos;
  begin
  Result.X:= TProtPMemo(fPMemo).ColPos(Par, ParNumber, ParLine, Col);
  Result.Y:= LineNumber*TPlusMemo(fPMemo).LineHeight - TPlusMemo(fPMemo).TopOrigin + TProtPMemo(fPMemo).fLineBase
  end;

procedure TPlusNavigator.SetDisplayPos(DispPos: TPoint);
  var sline: Longint;
  begin
  sline:= (DispPos.Y + TPlusMemo(fPMemo).TopOrigin) div TProtPMemo(fPMemo).fLineHeight;
  if sline>=TPlusMemo(fPMemo).LineCount then Pos:= TPlusMemo(fPMemo).CharCount
  else
    begin
      LineNumber:= sline;
      Col:= TProtPMemo(fPMemo).ColNb(Par, ParNumber, ParLine, DispPos.X, False)
    end
  end;

{$ENDIF}
