// ShapedButton
// Version 1.0 21/4/98
// Version 1.1 25/4/98 Space key only activates KeyDown Event.
unit ACNC_TouchShapedButton;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls,ACNC_TouchGlyphSoftkey;

type
  TTouchShapeButton = class(TCustomPanel)
  private
    { Private declarations }
    FSelected              : Boolean;
    FLegend                : String;
    OnButton               : TTouchGlyphSoftkey;
//  OffBitmap              : TBitmap;
    OffBuffer              : TImage;

    procedure SetOnBitmap(Value : TBitmap);
    function  GetOnBitmap:TBitmap;
    procedure SetOffBitmap(Value : TBitmap);
    function  GetOffBitmap:TBitmap;
    procedure SetLegend(Text : String);
    procedure SetSelected(Select : Boolean);
    procedure MyOnClick(Sender : Tobject);

    procedure MyMouseDown(Sender: TObject; Button: TMouseButton;
Shift: TShiftState; X, Y: Integer);
    procedure MyMouseUp(Sender: TObject; Button: TMouseButton;
Shift: TShiftState; X, Y: Integer);
    procedure MyKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState) ;
    procedure MyKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);

    procedure MyOnEnter(Sender : TObject);
    procedure MyOnExit(Sender : TObject);

  protected
    { Protected declarations }
  public
    { Public declarations }
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure Resize; override;

  published
    { Published declarations }
    property SelectedBitmap   : TBitmap     read GetOnBitmap         Write SetOnBitmap;
    property UnSelectedBitmap : TBitmap     read GetOffBitmap        Write SetOffBitmap;
    property Selected         : Boolean     read FSelected           Write SetSelected;
    property Legend           : String      read FLegend             Write SetLegend;
    property BevelInner;
    property BevelOuter;
    property OnClick;
    property OnMouseDown;
    property OnMouseUp;
    property TabOrder;
    property TabStop;

  end;

procedure Register;

implementation

constructor TTouchShapeButton.Create(AOwner: TComponent);
begin
inherited Create(AOwner);
//FOnBitmap := TBitmap.Create;
//FOffBitmap := TBitmap.Create;

OnButton := TTouchGlyphSoftkey.Create(Self);
OnButton.Parent := self;
OnButton.OnMouseDown := MyMouseDown;
OnButton.OnMouseUp := MyMouseUp;
OnButton.OnClick := MyOnClick;
OffBuffer := TImage.Create(Self);
OffBuffer.Parent := Self;
OffBuffer.AutoSize := True;
OffBuffer.top :=0;
OffBuffer.Left :=0;
OffBuffer.OnMouseDown := MyMouseDown;
OffBuffer.OnMouseUp := MyMouseUp;
OffBuffer.OnClick := MyOnClick;
OnEnter := MyOnEnter;
onExit  := MyOnExit;
onKeyDown := MyKeyDown;
onKeyUp   := MyKeyUp;
//OffBitMap := TBitMap.Create;
OnButton.Visible := False;
OffBuffer.Visible := True;
FSelected := False;
Resize;
end;



destructor TTouchShapeButton.Destroy;
begin
OnButton.Free;
OffBuffer.Free;
inherited Destroy;
end;

procedure TTouchShapeButton.SetOnBitMap(Value : TBitmap);
begin
if assigned(Value) then
   begin
   OnButton.Bitmap.Assign(Value);
   end;
Resize;
end;

function TTouchShapeButton.GetOnBitmap : TBitmap;
begin
if assigned(OnButton.Bitmap) then Result := OnButton.Bitmap else Result := nil;
end;

procedure TTouchShapeButton.SetLegend(Text : String);
begin
FLegend := Text;
OnButton.Caption := Text;
end;

procedure TTouchShapeButton.SetOffBitMap(Value : TBitmap);
begin
if assigned(Value) then
   begin
   OffBuffer.Picture.{Bitmap.}Assign(Value);
   OffBuffer.Repaint;
   end;
Resize;
end;

function TTouchShapeButton.GetOffBitmap : TBitmap;
begin
if assigned(OffBuffer.Picture.Bitmap) then Result := OffBuffer.Picture.Bitmap else Result := nil;
end;

procedure TTouchShapeButton.Resize;
begin
if ((not assigned(OffBuffer)) or (not assigned(OnButton))) then exit;
Inherited Resize;
if Fselected then
   begin
   Width := OnButton.Width;
   Height := OnButton.Height;
   end
else
   begin
   Width := OffBuffer.Width;
   Height := OffBuffer.Height;
   end

end;

procedure TTouchShapeButton.MyOnEnter(Sender : Tobject);
begin
SetSelected(True);
end;

procedure TTouchShapeButton.MyOnExit(Sender : Tobject);
begin
SetSelected(False);
end;


procedure TTouchShapeButton.SetSelected(Select : Boolean);
begin
FSelected := Select;
if select then
   begin
   OffBuffer.Visible := False;
   OnButton.Visible := True;
   end
else
   begin
   OnButton.Visible := False;
   OffBuffer.Visible := True;
   OffBuffer.Canvas.Brush.Color := TPanel(Parent).Color;
   OffBuffer.Invalidate;
   end;
ParentColor := True;
Resize;
OffBuffer.Invalidate;
OnButton.InValidate;
end;

procedure TTouchShapeButton.MyMouseDown(Sender: TObject; Button: TMouseButton;
Shift: TShiftState; X, Y: Integer);
begin
if (Sender = OffBuffer) and (FSelected = False) then
   begin
   SetSelected(True);
   end;
MouseDown(Button,Shift,x,y);
ParentColor := True;
SetFocus;
end;

procedure TTouchShapeButton.MyMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
MouseUp(Button,Shift,x,y);
ParentColor := True;
end;

procedure TTouchShapeButton.MyOnClick(Sender : Tobject);
begin
Click;
end;

procedure TTouchShapeButton.MyKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
//MouseDown;
if key = VK_SPACE then
   begin
   MyMouseDown(Self,mbLeft,[],1,1);
   OnButton.MouseDown(mbLeft,[],Width div 2,Height div 2);
   end;
//OnButton.Invalidate;
end;

procedure TTouchShapeButton.MyKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
MyMouseUp(Self,mbLeft,[],1,1);
OnButton.MouseUp(mbLeft,[],Width div 2,Height div 2);
OnButton.Invalidate;
end;

procedure Register;
begin
RegisterComponents('Touch', [TTouchShapeButton]);
end;


end.
