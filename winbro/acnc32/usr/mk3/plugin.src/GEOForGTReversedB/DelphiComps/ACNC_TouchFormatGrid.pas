unit ACNC_TouchFormatGrid;
{ TODO : incremental Search by letter on first row }
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, ACNC_TouchScroller, ACNC_TouchStringGrid,Grids,FileCtrl,
  ACNC_FileList,Inifiles,IStringArray,NamedPlugin;

Const
NDString = 'NotDef';

type
  TColFormat = record
      Formatted         : Boolean;
      FitToTitleWidth   : Boolean;
      FitToMaxString    : Boolean;
      IsSystemDate      : Boolean;
      MaxWidth          : Integer;
      MinWidth          : Integer;
      NumberFormat       : String;
      FontModified      : Boolean;
      FontColour        : TColor;
      FontSize          : Integer;
      FontName          : String;
      FontStyle         : TFontStyles;
      HText1            : String;
      HText2            : String;
      HText3            : String;
      end;

  pColFormat = ^TColFormat;

  TColData = record
    ColNumber : Integer;
    ColTitle : String;
    ColFormat : pColFormat;
    end;


  TTouchFormatGrid = class(TTouchStringGrid)
  private
    { Private declarations }
    LastSelectedsCol    : Integer;
    ColFormatList       : TList;
    FDefNumFormat       : String;
    FRuntimeEditable    : Boolean;
    FDisplayUnformatted : Boolean;
    FOnSelectCell       : TSelectCellEvent;
    FGridDoubleClick    : TNotifyEvent;
    FDefPath            : String;
    FHLCol: Boolean;
    FHLRow: Boolean;
    FSelectedColData: TColData;
    FNewColSelected: TNotifyEvent;
    FViewatRuntime: Boolean;
    FLaunchTag: String;
    FIncludeInDirectory: Boolean;
    FUseLaunchTag: Boolean;
    FHeaderRows: Integer;
    FHeaderFontSize: Integer;
    procedure FormatDrawCell(Sender: TObject; ACol, ARow: Longint; Rect: TRect; State: TGridDrawState);
    procedure DefaultAllFormatting;
    procedure FreeFormatList;
    procedure SetDefNumFormat(const Value: String);
    procedure FormatSetColCount(const Value: Integer);
    function  FormatCellData(var InBuff: String; FormatRec: pColFormat): Boolean;
    procedure SetDisplayUnformatted(const Value: Boolean);
    procedure SetHLCol(const Value: Boolean);
    procedure SetHLRow(const Value: Boolean);
    procedure UpdateColData(Col : Integer);
    procedure RefreshTableData(Table: IACNC32StringArray);
    procedure SetViewAtRuntime(const Value: Boolean);
    procedure SetIncludeInDirectory(const Value: Boolean);
    procedure SetHeaderRows(const Value: Integer);
    procedure SetHeaderFontSize(const Value: Integer);
    function HeaderWidth(FormatData : pColFormat): Integer;
    procedure GridDoubleClick(sender : TObject);
    procedure SeteditableatRuntime(const Value: Boolean);

  protected
    { Protected declarations }
    procedure UpdateCurrentCell(Sender: TObject; ACol, ARow: Integer;var CanSelect: Boolean);override;
    function  LoadDBConfig(DBName,Path : String): Boolean;

  public
    { Public declarations }
    procedure SetColFormat(Column : Integer; CFormat : pColFormat);
    function  GetColFormat(Column : Integer): pColFormat;
    procedure ForceAllFormatted;
    procedure FormatAll;
    procedure FormatColumn(ColNumber: Integer);
    function  SaveDBConfig(DBName,Path : String): Boolean;
    procedure LoadDBTable(Table: IACNC32StringArray);
  published
    { Published declarations }
    constructor Create(AOwner:TComponent); override;
    destructor Destroy; override;
    property DefaultColNumberFormat : String read FDefNumFormat write SetDefNumFormat;
    property ColCount write FormatSetColCount;
    property DisplayUnformatted : Boolean read FDisplayUnformatted write SetDisplayUnformatted;
    property OnSelectCell : TSelectCellEvent read FOnSelectCell write FOnSelectCell;
    property OnGridDoubleClick : TNotifyEvent read FGridDoubleClick write FGridDoubleClick;
    property DefaultDBPath : String read FDefPath write FDefPath;
    property HighLightSelectedCol : Boolean read FHLCol write SetHLCol;
    property HighLightSelectedRow : Boolean read FHLRow write SetHLRow;
    property isReadOnly;
    property SelectedColData : TColData read FSelectedColData;
    property NewColSelected : TNotifyEvent read FNewColSelected write FNewColSelected;
    property ViewAtRuntime : Boolean read FViewAtRuntime write SetViewAtRuntime;
    property EditableAtRuntime : Boolean read FRuntimeEditable write SeteditableatRuntime;
    property IncludeInDirectory : Boolean read FIncludeInDirectory write SetIncludeInDirectory;
    property LaunchTag : String  read FLaunchTag write FLaunchtag;
    property UseLaunchTag : Boolean read FUseLaunchTag write FUseLaunchTag;
    property HeaderRows : Integer read FHeaderRows write SetHeaderRows;
    property HeaderFontSize : Integer read FHeaderFontSize write SetHeaderFontSize;
  end;

procedure Register;

implementation

function StorageDate(DT: TDateTime): WideString;
var ST : TSystemTime;
begin
  DateTimeToSystemTime(DT, ST);
  // YYYY-MM-DD hh:mm:ss  This seems to be an SQL standard as it Ascii / chronologically the same
  Result := Format('%4d-%.2d-%.2d %.2d:%.2d:%.2d',
     [ST.wYear, ST.wMonth, ST.wDay, ST.wHour, ST.wMinute, ST.wSecond ]  );
end;

function IsValidNumber(InString : String;var Num : Double) : Boolean;
begin
Result := True;
try
Num := StrToFloat(InString);
except
Result := False
end;
end;

{procedure CopyColFormat(Source: pColFormat;Dest : TColFormat);
begin
Dest.Formatted          := Source.Formatted;
Dest.FitToTitleWidth    := Source.FitToTitleWidth;
Dest.FitToMaxString     := Source.FitToMaxString;
Dest.MaxWidth           := Source.MaxWidth;
Dest.NumberFormat       := Source.NumberFormat;
Dest.FontModified       := Source.FontModified;
Dest.FontColour         := Source.FontColour;
Dest.FontSize           := Source.FontSize;
Dest.FontStyle          := Source.FontStyle;
end;}


procedure Register;
begin
  RegisterComponents('ACNCComps', [TTouchFormatGrid]);
end;

{ TTouchFormatGrid }

procedure TTouchFormatGrid.DefaultAllFormatting;
var
ColNumber : Integer;
FormatEntry :  pColFormat;

begin
FreeFormatList;
For ColNumber := Grid.FixedCols to Grid.ColCount do
  begin
  New(FormatEntry);
  FormatEntry.Formatted := True;
  FormatEntry.FitToTitleWidth := True;
  FormatEntry.FitToMaxString := True;
  FormatEntry.IsSystemDate := False;
  FormatEntry.FontModified := False;
  FormatEntry.FontColour := Grid.Font.Color;
  FormatEntry.FontSize := Grid.Font.Size;
  FormatEntry.FontName := Grid.Font.Name;
  FormatEntry.HText1 := '';
  FormatEntry.HText2 := '';
  FormatEntry.HText3 := '';
  ColFormatList.Add(FormatEntry)
  end;
end;

constructor TTouchFormatGrid.Create(AOwner: TComponent);
begin
  inherited;

  FDefPath := DLLProfilePath;
  ColFormatList := TList.Create;
  FDisplayUnformatted := False;
  HighLightSelectedRow := True;
  HighLightSelectedCol := True;
  FHLCol := False;
  FHLRow := True;
  DefaultAllFormatting;
  with Grid do
        begin
        DefaultDrawing := False;
        OnDrawCell := FormatDrawCell;
        OnDblClick := GridDoubleClick;
        end;
end;

destructor TTouchFormatGrid.Destroy;
begin
  inherited;
  FreeFormatList;
end;


procedure TTouchFormatGrid.FormatDrawCell(Sender: TObject; ACol,
  ARow: Integer; Rect: TRect; State: TGridDrawState);
var
FormatString    : String;
IsFixed         : Boolean;
Fentry          : pColFormat;
DisplayBuffer   : String;
TextExt         : TSize;
THeight         : Integer;
begin
IsFixed := False;
with Grid.Canvas do
begin
Pen.Color := clBlack;
Pen.Width := 1;
FEntry := ColFormatList[ACol];
if (ACol < Grid.FixedCols) or (ARow < Grid.FixedRows) then
        begin
        Brush.Color := Grid.FixedColor;
        FillRect(Rect);
        with Grid.Canvas do
          begin
          Font.Size := FHeaderFontSize;
          TextExt := TextExtent(FEntry.HText1);
          THeight := TextExt.cy;
          if FEntry.HText1 <> '' then
            begin
            TextOut(Rect.Left+2,Rect.Top+1,FEntry.HText1);
            if HeaderRows > 1 then
              TextOut(Rect.Left+2,Rect.Top+3+THeight,FEntry.HText2);
            if HeaderRows > 2 then
              TextOut(Rect.Left+2,Rect.Top+5+THeight+THeight,FEntry.HText3);
            end
          else
            begin
            TextOut(Rect.Left+2,Rect.Top+2,Grid.Cells[Acol,Arow]);
            end
          end;
        exit;
        end
else
        begin
        if ARow = SelectedRow then
                begin
                if ACol = SelectedCol then
                    begin
                    // selected row and column
                    if goEditing  in Grid.Options then
                        begin
                        Brush.Color := clAqua;
                        Pen.Width := 5;
                        Pen.Color := clred;
                        Rectangle(Rect);
                        end
                    else
                        begin
                        Brush.Color := Clwhite - $00202030;
                        end
                    end
                else
                    begin
                    if FHLRow then
                        // Selected Row but Not selected column
                        Brush.Color := Clwhite - $00101010
                    else
                        Brush.Color := Self.Color;
                    end
                end
        else
                begin
                if ACol = SelectedCol then
                    begin
                    //Selected column but NOt selected Row
                    if FHLCol then
                        begin
                        Brush.Color := Clwhite - $00101010;
                        end
                    else
                        begin
                        Brush.Color := Self.Color;
                        end;
                    end
                else
                    begin
                    //Niether select Row Nor SelectedColumn
                    Brush.Color := Self.Color;
                    end;
                end;


        if Fentry.FontModified then
                begin
                Grid.Canvas.Font.Size := Fentry.FontSize;
                Grid.Canvas.Font.Name := Fentry.FontName;
                Grid.Canvas.Font.Color := Fentry.FontColour;
                Grid.Canvas.Font.Style := FEntry.FontStyle;
                end
        else
                begin
                Grid.Canvas.Font.Assign(Grid.Font);
                end
        end;

FillRect(Rect);

if (IsFixed or FDisplayUnformatted) then
   begin
   Font.assign(Grid.Font);
   TextOut(Rect.Left+2,Rect.Top+2,Grid.Cells[Acol,Arow]);
   end
else
   begin
   if FEntry.IsSystemDate then
      begin
      try
      TextOut(Rect.Left+2,Rect.Top+2,StorageDate(StrToFloat(Grid.Cells[Acol,Arow])));
      except
      TextOut(Rect.Left+2,Rect.Top+2,Grid.Cells[Acol,Arow]);
      end;
      end else
   begin
   if Acol < ColFormatList.Count then
        begin
        if Fentry.Formatted then
                FormatString := FEntry.NumberFormat
        else
                FormatString := DefaultColNumberFormat;
        end;
   if FormatString <> '' then
        begin
        DisplayBuffer := Grid.Cells[Acol,Arow];
        if FormatCellData(DisplayBuffer,Fentry) then
                begin
                TextOut(Rect.Left+2,Rect.Top+2,DisplayBuffer);
                end
        else
                begin
                TextOut(Rect.Left+2,Rect.Top+2,Grid.Cells[Acol,Arow]);
                end;
        end
   else
        begin
        TextOut(Rect.Left+2,Rect.Top+2,Grid.Cells[Acol,Arow]);
        end
   end;
   end
end; // with canvas
end;



procedure TTouchFormatGrid.FreeFormatList;
begin
while ColFormatList.Count > 0 do
   begin
   Dispose(pColFormat(ColFormatList[0]));
   ColFormatList.Delete(0);
   end;
end;

function TTouchFormatGrid.GetColFormat(Column: Integer): pColFormat;
begin
Result := ColFormatList[Column];
end;

procedure TTouchFormatGrid.FormatSetColCount(const Value: Integer);
var
ListEntry : pColFormat;
begin
TTouchStringGrid(SELF).ColCount := Value;
//exit;
if Grid.ColCount > ColFormatList.Count then
        begin
        while ColFormatList.Count < Grid.ColCount do
                begin
                New(ListEntry);
                ListEntry.Formatted := True;
                ListEntry.FitToTitleWidth := True;
                ListEntry.FitToMaxString := True;
                ListEntry.IsSystemDate := False;
                ListEntry.MaxWidth := 100;
                ListEntry.MinWidth := 20;
                ListEntry.FontModified := False;
                ListEntry.FontColour := Grid.Font.Color;
                ListEntry.FontSize := Grid.Font.Size;
                ListEntry.FontName := Grid.Font.Name;;
                ListEntry.FontStyle := Grid.Font.Style;
                ListEntry.HText1 := '';
                ListEntry.HText2 := '';
                ListEntry.HText2 := '';
                ColFormatList.Add(ListEntry);
                end;
        end;

if ColCount < ColFormatList.Count then
        begin
        while ColFormatList.Count > ColCount do
                begin
                Dispose(pColFormat(ColFormatList[ColFormatList.Count-1]));
                ColFormatList.Delete(ColFormatList.Count-1);
                end;
        end;
end;


procedure TTouchFormatGrid.SetColFormat(Column: Integer;
  CFormat: pColFormat);
var
FListEntry : pColFormat;
begin
if Column < ColFormatList.Count then
   begin
   FListEntry := ColFormatList[Column];
   FListEntry.Formatted := CFormat.Formatted;
   FListEntry.HText1 := CFormat.HText1;
   FListEntry.HText2 := CFormat.HText2;
   FListEntry.HText3 := CFormat.HText3;
   if FListEntry.Formatted then
        begin
        FListEntry.FitToTitleWidth := CFormat.FitToTitleWidth;
        FListEntry.FitToMaxString  := CFormat.FitToMaxString;
        FListEntry.MaxWidth        := CFormat.MaxWidth;
        FListEntry.MinWidth        := CFormat.MinWidth;
        FListEntry.NumberFormat    := CFormat.NumberFormat;
        FListEntry.IsSystemDate    := CFormat.IsSystemDate;

        end;
   FListEntry.FontModified := CFormat.FontModified;
   if FListEntry.FontModified then
        begin
        FListEntry.FontColour := CFormat.FontColour;
        FListEntry.FontSize  :=  CFormat.FontSize;
        FListEntry.FontName :=  CFormat.FontName;
        FListEntry.FontStyle :=  CFormat.FontStyle;
        end;
   end;
end;

procedure TTouchFormatGrid.SetDefNumFormat(const Value: String);
begin
  FDefNumFormat := Value;
end;

function TTouchFormatGrid.FormatCellData(var InBuff : String;FormatRec : pColFormat): Boolean;
var
SNumber : Double;
Buffer  : String;
begin
Result := True;
Buffer := InBuff;
if IsValidNumber(Inbuff,SNumber) then
   begin
   if FormatRec.NumberFormat <> '' then
        begin
        try
        Buffer := Format(FormatRec.NumberFormat,[SNumber]);
        except
        Buffer := InBuff;
        Result := False;
        end;
        end
   else
        begin
        if DefaultColNumberFormat <> '' then
           begin
           try
           Buffer := Format(DefaultColNumberFormat,[SNumber]);
           except
           Buffer := InBuff;
           Result := False;
           end;
           end
        end;


   end;
InBuff := Buffer;
end;
function TTouchFormatGrid.HeaderWidth(FormatData : pColFormat): Integer;
var
BufferFS : Integer;
MaxW : Integer;
//Row  : Integer;
BufferExtent    : TSize;
begin
//MaxW := 10;
//Result := 10;
BufferFS := Grid.Canvas.Font.Size;
try
with Grid.Canvas do
  begin
  Font.Size := FHeaderFontSize;
  BufferExtent := TextExtent(FormatData.HText1);
  MaxW := BufferExtent.cx;
  if FHeaderRows > 1 then
    begin
    BufferExtent := TextExtent(FormatData.HText2);
    if BufferExtent.cx > MaxW then MaxW := BufferExtent.cx;
    end;
  if FHeaderRows > 2 then
    begin
    BufferExtent := TextExtent(FormatData.HText3);
    if BufferExtent.cx > MaxW then MaxW := BufferExtent.cx;
    end;
  Result := MaxW;
  end;
finally
Grid.Canvas.Font.Size := BufferFS;
end;
end;

procedure TTouchFormatGrid.FormatColumn(ColNumber : Integer);
var
Buffer     : String;
RowNumber  : Integer;
FormatData : pColFormat;
MaxWidth   : INteger;
//MinWidth   : INteger;
BufferExtent    : TSize;
DoFormatting    : Boolean;
TitleWidth       : Integer;
begin
MaxWidth := 0;
//MinWidth := 0;
if ColNumber < ColFormatList.Count then
    begin
    FormatData := ColFormatList[ColNumber];
    if (FormatData.FontModified) and (not DisplayUnformatted) then
        begin
        Grid.Canvas.Font.Size := FormatData.FontSize;
        Grid.Canvas.Font.Name := FormatData.FontName;
        Grid.Canvas.Font.Style := FormatData.FontStyle;
        end
    else
       begin
       Grid.Canvas.Font.Assign(Grid.Font);
       end
    end
else
    begin
    exit;
    end;
    DoFormatting := (FormatData.Formatted) or (FDefNumFormat <> '') and (not DisplayUnformatted);
    if DoFormatting then
       begin
        For RowNumber := Grid.FixedRows to Grid.RowCount-1 do
            begin
            Buffer := Grid.Cells[ColNumber,RowNumber];
            if Buffer <> '' then
                begin
                if FormatCellData(Buffer,FormatData) then
                    begin
                    BufferExtent := Grid.Canvas.TextExtent(Buffer);
                    if (BufferExtent.cx > MaxWidth) then MaxWidth := BufferExtent.cx;
                    end;
                end;
            end;
      end
    else
       begin
       if DisplayUnformatted then
           begin
           For RowNumber := Grid.FixedRows to Grid.RowCount-1 do
            begin
            Buffer := Grid.Cells[ColNumber,RowNumber];
            if Buffer <> '' then
                begin
                BufferExtent := Grid.Canvas.TextExtent(Buffer);
                if (BufferExtent.cx > MaxWidth) then MaxWidth := BufferExtent.cx;
                end;
            end;
           end;
       end;
// Finally check if column width formatting is in place and adjust if required
if  DoFormatting then
begin
if FormatData.FitToMaxString then
     begin
     if FormatData.FitToTitleWidth then
         begin
//         TitleSize := Grid.Canvas.TextExtent(Grid.Cells[ColNumber,0]);
         TitleWidth := HeaderWidth(FormatData);
         if TitleWidth > Maxwidth then MaxWidth := TitleWidth;
         end;
     end
else
     begin
     if FormatData.FitToTitleWidth then
         begin
         TitleWidth := HeaderWidth(FormatData);
         MaxWidth := TitleWidth;
         end;
     end;
end;

Grid.ColWidths[ColNumber] := MaxWidth+10;
end;
procedure TTouchFormatGrid.ForceAllFormatted;
var
Fentry          : pColFormat;
ColNumber       : Integer;
begin
For ColNumber := Grid.FixedCols to Grid.ColCount-1 do
        begin
        if ColNumber < ColFormatList.Count then
            begin
            Fentry := ColFormatList[ColNumber];
            Fentry.Formatted := True;
            end
        end;
end;

procedure TTouchFormatGrid.FormatAll;
var
ColNumber : Integer;
begin
For ColNumber := Grid.FixedCols to Grid.ColCount-1 do
        begin
        FormatColumn(ColNumber);
        end;
end;

procedure TTouchFormatGrid.SetDisplayUnformatted(const Value: Boolean);
begin
  FDisplayUnformatted := Value;
  Grid.Invalidate;
end;


procedure TTouchFormatGrid.UpdateCurrentCell(Sender: TObject; ACol,
  ARow: Integer; var CanSelect: Boolean);
begin
  inherited;
  Grid.Invalidate;
  UpdateColData(ACol);
  if Assigned(FOnSelectCell) then FOnSelectCell(Self,Acol,Arow,CanSelect);
end;

function TTouchFormatGrid.LoadDBConfig(DBName, Path: String): Boolean;
var
ColNumber : Integer;
//NumberofCOls : Integer;
Config    : pColFormat;
FileText  : TIniFile;
FilePath : String;
ListIndex : Integer;
Buffer :String;
IntBuff : Integer;
KeyName : String;
begin

Result := False;
if ColFormatList.Count < 1 then exit;
If DirectoryExists(FDefPath) then
   begin
   FilePath := SlashSep(FDefPath,'FormatGridConfig');
   if Not DirectoryExists(FilePath) then
        begin
        ForceDirectories(FilePath);
        end;
   FilePath := SlashSep(FilePath,DBName)+'.dbc';
   if FileExists(FilePath) then
       begin
       FileText := TIniFile.Create(FilePath);
       try
       with FileText do
         begin
         Buffer := FileText.ReadString('COMMON','ViewAtRuntime',NDString);
         if Buffer <> NDString then
               begin
               ViewatRuntime := (Buffer[1] = 't') or (Buffer[1] = 'T')
               end
         else
               begin
               ViewatRuntime := False;
               end;

         Buffer := FileText.ReadString('COMMON','IncludeInDirectory',NDString);
         if Buffer <> NDString then
               begin
               FIncludeInDirectory := (Buffer[1] = 't') or (Buffer[1] = 'T')
               end
         else
               begin
               FIncludeInDirectory := False;
               end;

         Buffer := FileText.ReadString('COMMON','UseLaunchTag',NDString);
         if Buffer <> NDString then
               begin
               FUseLaunchTag := (Buffer[1] = 't') or (Buffer[1] = 'T');
               FLaunchTag := FileText.ReadString('COMMON','LaunchTag','');
               end
         else
               begin
               FUseLaunchTag := False;
               FLaunchTag := '';
               end;


         Buffer := FileText.ReadString('COMMON','Editable',NDString);
         if Buffer <> NDString then
               begin
               FRuntimeEditable := (Buffer[1] = 't') or (Buffer[1] = 'T')
               end
         else
               begin
               FRuntimeEditable := False;
               end;


         DefaultColNumberFormat := FileText.ReadString('COMMON','DefaultNumFormat','');
         FixedCols := FileText.ReadInteger('COMMON','FixedCols',0);
         FixedRows := FileText.ReadInteger('COMMON','FixedRows',0);
//         NumberofCOls := FileText.ReadInteger('COMMON','FieldCount',0);
         Grid.Font.Color := StringToColor(FileText.ReadString('COMMON','FontColour','clBlack'));
         Grid.Font.Size := FileText.ReadInteger('COMMON','FontSize',8);
         Grid.Font.Name := FileText.ReadString('COMMON','FontName','MS Sans Serif');
         HeaderRows := FileText.ReadInteger('COMMON','HeaderRows',1);
         HeaderFontSize := FileText.ReadInteger('COMMON','HeaderFontSize',10);
         end;
       ListIndex := 0;
       For ColNumber := FixedCols to ColCount-1 do
          begin
          If ListIndex < ColFormatList.Count then
            begin
            KeyName := 'Col'+InTToStr(ListIndex);
            Config := ColFormatList[ListIndex];
            Buffer := FileText.ReadString(Keyname,'Formatted',NDString);
            if Buffer <> NDString then
               begin
               Config.Formatted := (Buffer[1] = 't') or (Buffer[1] = 'T')
               end;

            Buffer := FileText.ReadString(Keyname,'IsSystemDate',NDString);
            if Buffer <> NDString then
               begin
               Config.IsSystemDate := (Buffer[1] = 't') or (Buffer[1] = 'T')
               end;

            Buffer := FileText.ReadString(Keyname,'FitToTitleWidth',NDString);
            if Buffer <> NDString then
               begin
               Config.FitToTitleWidth := (Buffer[1] = 't') or (Buffer[1] = 'T')
               end;

            Buffer := FileText.ReadString(Keyname,'FitToMaxString',NDString);
            if Buffer <> NDString then
               begin
               Config.FitToMaxString := (Buffer[1] = 't') or (Buffer[1] = 'T')
               end;

            IntBuff := FileText.ReadInteger(Keyname,'MaxWidth',-1);
            if IntBuff >= 0 then
               begin
               Config.MaxWidth := IntBuff;
               end;

            IntBuff := FileText.ReadInteger(Keyname,'MinWidth',-1);
            if IntBuff >= 0 then
               begin
               Config.MinWidth := IntBuff;
               end;

            Buffer := FileText.ReadString(Keyname,'NumberFormat',NDString);
            if Buffer <> NDString then
               begin
               Config.NumberFormat := Buffer
               end;

            Buffer := FileText.ReadString(Keyname,'FontModified',NDString);
            if Buffer <> NDString then
               begin
               Config.FontModified := (Buffer[1] = 't') or (Buffer[1] = 'T')
               end;

            Buffer := FileText.ReadString(Keyname,'FontColour',NDString);
            if Buffer <> NDString then
               begin
               Config.FontColour := StringToColor(Buffer);
               end;

            IntBuff := FileText.ReadInteger(Keyname,'FontSize',-1);
            if IntBuff >= 0 then
               begin
               Config.FontSize := IntBuff;
               end;

            Buffer := FileText.ReadString(Keyname,'FontName',NDString);
            if Buffer <> NDString then
               begin
               Config.FontName := Buffer
               end;
//            end;
            Config.FontStyle := [];
            Buffer := FileText.ReadString(Keyname,'FontBold',NDString);
            if Buffer <> NDString then
               begin
               if (Buffer[1] = 't') or (Buffer[1] = 'T') then
                   begin
                   Include(Config.FontStyle,fsBold);
                   end;
               end;

            Buffer := FileText.ReadString(Keyname,'FontItalic',NDString);
            if Buffer <> NDString then
               begin
               if (Buffer[1] = 't') or (Buffer[1] = 'T') then
                   begin
                   Include(Config.FontStyle,fsItalic);
                   end;
               end;

            Buffer := FileText.ReadString(Keyname,'FontUnder',NDString);
            if Buffer <> NDString then
               begin
               if (Buffer[1] = 't') or (Buffer[1] = 'T') then
                   begin
                   Include(Config.FontStyle,fsUnderline);
                   end;
               end;

            Buffer := FileText.ReadString(Keyname,'FontStrike',NDString);
            if Buffer <> NDString then
               begin
               if (Buffer[1] = 't') or (Buffer[1] = 'T') then
                   begin
                   Include(Config.FontStyle,fsStrikeOut);
                   end;
               end;
            ConFig.HText1 := FileText.ReadString(Keyname,'HTEXT1','');
            ConFig.HText2 := FileText.ReadString(Keyname,'HTEXT2','');
            ConFig.HText3 := FileText.ReadString(Keyname,'HTEXT3','');
            inc(ListIndex);
            end;
          end;
       finally
       FileText.Free;
       end;
       end
   else
        begin
        // Here if no format file exists fo DB
        // Load default Config
        ViewatRuntime := False;
        FIncludeInDirectory := False;
        FUseLaunchTag := False;
        FRuntimeEditable := False;
//        DefaultColNumberFormat '';
        FixedCols := 0;
        FixedRows := 1;
//        NumberofCOls := 0;
        Grid.Font.Color :=clBlack;
        Grid.Font.Size := 8;
        Grid.Font.Name := 'MS Sans Serif';
        HeaderRows := 1;
        HeaderFontSize := 10;
        ListIndex := 0;
        For ColNumber := FixedCols to ColCount-1 do
          begin
          If ListIndex < ColFormatList.Count then
            begin
            Config := ColFormatList[ListIndex];
            ConFig.HText1 := Grid.Cells[ListIndex,0];
            ConFig.HText2 := '';
            ConFig.HText3 := '';
            end;
          inc(ListIndex);
          end;

        end;
   end;

Formatall;

end;

function TTouchFormatGrid.SaveDBConfig(DBName, Path: String): Boolean;
var
ColNumber : Integer;
Config    : pColFormat;
FileText  : TStringList;
FilePath : String;
begin

Result := False;
if ColFormatList.Count < 1 then exit;
If DirectoryExists(FDefPath) then
   begin
   FilePath := SlashSep(FDefPath,'FormatGridConfig');
   if Not DirectoryExists(FilePath) then
        begin
        ForceDirectories(FilePath);
        end;
   FilePath := SlashSep(FilePath,DBName)+'.dbc';
   FileText := TStringList.Create;
   try
   with FileText do
     begin
     Add('[COMMON]');
     if FViewatRuntime then
        Add('ViewatRuntime=True')
     else
        Add('ViewatRuntime=False');

     if FIncludeInDirectory then
        Add('IncludeInDirectory=True')
     else
        Add('IncludeInDirectory=False');


     if FUseLaunchTag then
        Add('UseLaunchTag=True')
     else
        Add('UseLaunchTag=False');

     Add(Format('LaunchTag=%s',[FLaunchTag]));

     if FRuntimeEditable then
        Add('Editable=True')
     else
        Add('Editable=False');


        Add(Format('DefaultNumFormat=%s',[FDefNumFormat]));
        Add(Format('FixedCols=%d',[FixedCols]));
        Add(Format('FixedRows=%d',[FixedRows]));
        Add(Format('FieldCount=%d',[ColFormatList.Count]));
        Add(Format('FontColour=%s',[ColorToString(Grid.Font.Color)]));
        Add(Format('FontSize=%d',[Grid.Font.Size]));
        Add(Format('FontName=%s',[Grid.Font.Name]));
        Add(Format('HeaderRows=%d',[FHeaderRows]));
        Add(Format('HeaderFontSize=%d',[FHeaderFontSize]));


        For ColNumber := 0 to ColFormatList.Count-1 do
         begin
         Add(Format('[Col%d]',[ColNumber]));
         Config := ColFormatList[ColNumber];
         if Config.Formatted then
             Add('Formatted=True')
         else
             Add('Formatted=False');

         if Config.FitToTitleWidth then
             Add('FitToTitleWidth=True')
         else
             Add('FitToTitleWidth=False');

         if Config.IsSystemDate then
             Add('IsSystemDate=True')
         else
             Add('IsSystemDate=False');


         if Config.FitToMaxString then
             Add('FitToMaxString=True')
         else
             Add('FitToMaxString=False');

         Add(Format('MaxWidth=%d',[Config.MaxWidth]));
         Add(Format('MinWidth=%d',[Config.MinWidth]));
         Add(Format('NumberFormat=%s',[Config.NumberFormat]));

         if Config.FontModified then
             Add('FontModified=True')
         else
             Add('FontModified=False');

         Add(Format('FontColour=%s',[ColorToString(Config.FontColour)]));
         Add(Format('FontSize=%d',[Config.FontSize]));
         Add(Format('FontName=%s',[Config.FontName]));
         if fsBold in Config.FontStyle then
             begin
             Add('FontBold=True');
             end
         else
             begin
             Add('FontBold=False');
             end;

         if fsItalic in Config.FontStyle then
             begin
             Add('FontItalic=True');
             end
         else
             begin
             Add('FontItalic=False');
             end;

         if fsUnderline in Config.FontStyle then
             begin
             Add('FontUnder=True');
             end
         else
             begin
             Add('FontUnder=False');
             end;
         if fsStrikeOut in Config.FontStyle then
             begin
             Add('FontStrike=True');
             end
         else
             begin
             Add('FontStrike=False');
             end;
         Add(Format('HTEXT1=%s',[Config.HText1]));
         Add(Format('HTEXT2=%s',[Config.HText2]));
         Add(Format('HTEXT3=%s',[Config.HText3]));
         end;
         SaveToFile(FilePath);
      end;
   finally
   FileText.Free;
   end;
   end;


end;


procedure TTouchFormatGrid.SetHLCol(const Value: Boolean);
begin
  FHLCol := Value;
end;

procedure TTouchFormatGrid.SetHLRow(const Value: Boolean);
begin
  FHLRow := Value;
end;

procedure TTouchFormatGrid.UpdateColData(Col: Integer);
//var
//CFormat : pColFormat;
begin
FSelectedColData.ColNumber := Col;
if Grid.FixedRows > 0 then
    begin
    FSelectedColData.ColTitle := Grid.Cells[Col,0];
    end
else
    begin
    FSelectedColData.ColTitle := '';
    end;
FSelectedColData.ColFormat := GetColFormat(Col);
if Col <> LastSelectedsCol then
    begin
    if assigned(FNewColSelected) then FNewColSelected(Self);
    end;
LastSelectedsCol := Col;

end;



procedure TTouchFormatGrid.LoadDBTable(Table : IACNC32StringArray);
var I : Integer;
    S : WideString;
//    StartRow,StartCol : Integer;
    TName : String;
    FormatPath : String;
begin
//  StartRow := Gridref.SelectedRow;
//  StartCol := Gridref.SelectedCol;
  ColCount := Table.FieldCount;
  if Table.RecordCount = 0 then begin
    RowCount := Table.RecordCount + 2;
    for I := 0 to Table.FieldCount - 1 do
      Cells[I, 1] := '';
  end else
    RowCount := Table.RecordCount + 1;

  FixedCols := 0;
  FixedRows := 1;
  for I := 0 to Table.FieldCount - 1 do begin
    Table.FieldNameAtIndex(I, S);
    Cells[I, 0] := S;
  end;
  // Now load ColumnForm DATA IF IT EXISTS
  TName := Format('%s.dbc',[Table.Alias]);
  FormatPath := SlashSep(FDefPath,TName);
  
  LoadDBConfig(Table.Alias,FDefPath);

  RefreshTableData(Table);

  FormatAll;
{  if (StartRow < Gridref.RowCount) and (StartCol < Gridref.ColCount) then
    begin
    Gridref.Grid.Row := StartRow;
    Gridref.Grid.Col := StartCol;
    end;}


end;

procedure TTouchFormatGrid.RefreshTableData(Table :  IACNC32StringArray);
var I, J : Integer;
    S : WideString;
begin
  for J := 0 to Table.RecordCount - 1 do begin
    for I := 0 to Table.FieldCount - 1 do begin
      Table.GetValue(J, I, S);
      Cells[I, J + 1] := S;
    end;
  end;
end;

procedure TTouchFormatGrid.SetViewAtRuntime(const Value: Boolean);
begin
  FViewAtRuntime := Value;
end;

procedure TTouchFormatGrid.SetIncludeInDirectory(const Value: Boolean);
begin
  FIncludeInDirectory := Value;
end;

procedure TTouchFormatGrid.SetHeaderRows(const Value: Integer);
var
TextH : Integer;
begin
  FHeaderRows := Value;
  Grid.Canvas.Font.Size := FHeaderFontSize;
  TextH := Grid.Canvas.TextHeight('TTT');
  Grid.RowHeights[0] := ((TextH+4)*FHeaderRows)+4;
end;

procedure TTouchFormatGrid.SetHeaderFontSize(const Value: Integer);
var
TextH : Integer;
begin
  FHeaderFontSize := Value;
  Grid.Canvas.Font.Size := FHeaderFontSize;
  TextH := Grid.Canvas.TextHeight('TTT');
  Grid.RowHeights[0] := ((TextH+4)*FHeaderRows)+4;
end;

procedure TTouchFormatGrid.GridDoubleClick(sender: TObject);
begin
if assigned(FGridDoubleClick) then FGridDoubleClick(sender);
end;

procedure TTouchFormatGrid.SeteditableatRuntime(const Value: Boolean);
begin
  FRuntimeEditable := Value;
end;

end.


