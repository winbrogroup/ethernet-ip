library GTGEO;
//
// Version 1.0.1 25_4_09 Fixed Bug in GT.GetNDPWithHoleAligned which did not work for solution 0
// Taged in CVS as GTGEOV1_0_1_25_4_09
// Version 1.0.2 04_06_09 Added Gt.AdjustToolDatum Command
// Taged in CVS as GTGEOV1_0_2_06_6_09



uses
  Forms,
  SysUtils,
  G3DDefs,
  G3Math2d,
  Math,
  CncTypes,
  VectorMaths,
  GTGEOMain in 'GTGEOMain.pas' {GEO},
  PCMPlugin in 'PCMPlugin.pas',
  G3DOTree in 'DelphiComps\3DComponents\G3DOTree.pas';

{$R *.RES}
{
Inch Metric Switching Functions
}
function LocationToInch(Position :TPLocation) : TPLocation;
begin
Result.x := Position.x/25.4;
Result.y := Position.y/25.4;
Result.z := Position.z/25.4;
end;

function XYZToInch(Position :TXYZ): TXYZ;
begin
Result.x := Position.x/25.4;
Result.y := Position.y/25.4;
REsult.z := Position.z/25.4;
end;

function LocationToMetric(Position :TPLocation): TPLocation;
begin
Result.x := POsition.x*25.4;
Result.y := POsition.y*25.4;
Result.z := POsition.z*25.4;
end;

function XYZToMetric(Position :TXYZ): TXYZ;
begin
Result.x := POsition.x*25.4;
Result.y := POsition.y*25.4;
Result.z := POsition.z*25.4;
end;

function InchToMetric(InValue : Double):Double;
begin
Result := InValue*25.4;
end;

function MetricToInch(InValue : Double):Double;
begin
Result := InValue/25.4;
end;

function BadCharReplace(InString : String):String;
var
CharNum : Integer;
CChar : Char;
InLength : Integer;
OutString : String;
begin
Result := Instring;
try
InLength := Length(InString);
if InLength > 0 then
  begin
  OutString := '';
  For CharNum := 1 to InLength do
    begin
    CChar := InString[CharNum];
    if CChar = ' ' then
      begin
      OutString := OutString+'_';
      end else
    if CChar = '\' then
      begin
      OutString := OutString+'-';
      end else
    if CChar = '/' then
      begin
      OutString := OutString+'-';
      end else
    if CChar = ',' then
      begin
      OutString := OutString+'_';
      end else
    if CChar = '-' then
      begin
      OutString := OutString+'_';
      end else
    if CChar = ';' then
      begin
      OutString := OutString+'-';
      end else
    if CChar = '.' then
      begin
      OutString := OutString+'_';
      end
    else
      begin
      OutString := OutString+CChar;
      end
    end;
  end;
Result := OutString;
except
Result := InString;
end;
end;


function InConvertLocation(POsn : TPlocation):TPlocation;
begin
if GEO.InchMode then
    Result := LocationToMetric(Posn)
else
    Result := Posn;
end;

function InConvertPosn(POsn : Double):Double;
begin
if GEO.InchMode then
    Result := InchToMetric(Posn)
else
    Result := Posn;
end;

function OutConvertLocation(POsn : TPlocation):TPlocation;
begin
if GEO.InchMode then
    Result := LocationToInch(Posn)
else
    Result := Posn;
end;

function InConvertXYZ(Posn : TXYZ):TXYZ;
begin
if GEO.InchMode then
    Result := XYZToMetric(Posn)
else
    Result := Posn;
end;

function OutConvertXYZ(POsn : TXYZ):TXYZ;
begin
if GEO.InchMode then
    Result := XYZToInch(Posn)
else
    Result := Posn;
end;

function OutConvertPosn(POsn : Double):Double;
begin
if GEO.InchMode then
    Result := MetricToInch(Posn)
else
    Result := Posn;
end;

// not in the interface?
function DeleteReportPoint(PName : PChar): Boolean;stdcall;
begin
Result := False;
  if Assigned(GEO) then begin
  end;
end;


function ACNC32PluginRevision : Cardinal; stdcall;
begin
  Result := $01000000;
end;

procedure GEOPluginCreate; stdcall;
begin
{  if Assigned(GEO) then
    raise Exception.Create('Only one instance of GT may be created per client');
  GEO := TGEO.Create(Application);
  ReportPointEdit := TReportPointEdit.Create(Application);
  ReportVectorEdit := TReportVectorEdit(Application);}
end;

procedure GEOPluginDestroy; stdcall;
begin
  if Assigned(GEO) then begin
    GEO.Free;
    GEO := nil;
  end;
end;


procedure GEOPluginGetFitQuality(var Nominal, Mount :  array of Double;var Max : Integer);stdcall
begin
  if Assigned(GEO) then
     begin
     GEO.PCM.GetFitError(Nominal, Mount,Max);
     end;
end;


// Clear all offsets and vectors first
// call PCM LoadConfig with file name and then get Nominal stacking axis from PCM
// then set corrected stacking axis to Nominal
procedure GEOPluginLoadConfig(aFile : PChar; Sqr, Retry : Integer; Fit : Double); stdcall;
begin
  if Assigned(GEO) then
     begin
     GEO.TV1.ClearAllOffsetsandVectors;
     GEO.LoadPCMandInitialise(afile,Sqr,Retry,Fit);
     end;
end;


procedure GEOPluginShow;
begin
  if Assigned(GEO) then
     begin
     if not GEO.Visible then
        begin
        GEO.OnMachineShow
        end
     else
         begin
         GEO.Visible := False;
         end
     end;

end;

procedure GEOPluginHide;
begin
  if Assigned(GEO) then
    GEO.Hide;
end;

procedure GEOPluginReset;
begin
  if Assigned(GEO) then
     GEO.TV1.ClearAllOffsetsandVectors;
end;


// acnc gives machine xyz machine axis position
// create a probe point machine relative with given name
// get position of the point at machine datum and then pass this to PCM
// using PCM setprobe point in the PCM object on GEO form
procedure GEOPluginSetProbePoint(aName : PChar; P1 : TPLocation); stdcall;
var
ProbePoint     : TPlocation;
PointLocation  : TPlocation;
StartPositions : TAxisPosn;
PIndex         : Integer;
begin
if Assigned(GEO) then
     begin
     P1 := InConvertLocation(P1);
     GEO.InhibitExternalUpdate := True;
     try
     // first attach a point to the part datum which is the position of the
     // tooling ball for the current axis position
     StartPositions := GEO.TV1.GetAllAxisPositions;

     GEO.ProbePosnToPartRelativePoint('SPtemp',P1.x,
                                               P1.y,
                                               P1.z,
                                               StartPositions[3],
                                               StartPositions[4],
                                               StartPositions[5]);

     PIndex := GEO.Tv1.GetReportPointIndexfor('SPtemp');
     GEO.TV1.UpdateReportData(True);
     PointLocation := GEO.TV1.GetReportPointFromList(Pindex);
     ProbePoint := GEO.TV1.GetPointAtDatum(PointLocation.x,PointLocation.y,PointLocation.z);

//NGT!!!     GEO.PCM.SetProbePoint(aName,-ProbePoint.x,-ProbePoint.y,ProbePoint.z);
     GEO.PCM.SetProbePoint(aName,ProbePoint.x,ProbePoint.y,ProbePoint.z);
     finally
     GEO.TV1.DeleteReportPointCalled('SPtemp');
     GEO.InhibitExternalUpdate := False;
     end;
     end;
end;


// Sets axis position of machine model and updates all report points etc
procedure GEOPluginSetAxisPositions(X,Y,Z,A,B,C : Double);stdcall;
begin
if Assigned(GEO) then
   begin
   if GEO.InhibitExternalUpdate then exit;
     GEO.TV1.UserMode := umRuntime;
     GEO.TV1.AxisMoves[0].Position := InConvertPosn(X);
     GEO.TV1.AxisMoves[1].Position := InConvertPosn(Y);
     GEO.TV1.AxisMoves[2].Position := InConvertPosn(Z);
     GEO.TV1.AxisMoves[3].Position := A;
     GEO.TV1.AxisMoves[4].Position := B;
     GEO.TV1.AxisMoves[5].Position := C;
     GEO.TV1.AxisMoves[0].Move := True;
     GEO.TV1.AxisMoves[1].Move := True;
     GEO.TV1.AxisMoves[2].Move := True;
     GEO.TV1.AxisMoves[3].Move := True;
     GEO.TV1.AxisMoves[4].Move := True;
     GEO.TV1.AxisMoves[5].Move := True;
     GEO.TV1.MoveAllAxes(6,GEO.TV1.AxisMoves);

     if (GEO.FDMode = dmOnMachine) then
        begin
        if GEO.ExtentsMode = emLatched then
           begin
           GEO.TV1.RedrawToExtents(GEO.Paper);
           end;
        GEO.UpdateMachineReportDataAndDisplay;
        GEO.UpdateGEOPositionGrid;
        end;
   end;
end;

// Add a report point at a position P1 relative to the Part datum
function GEOPluginAddPartRelativePoint(PName :PChar;  P1 : TPLocation): Integer; stdcall;
var
PointName : String;
begin
  PointName := BadCharReplace(PName);
  if Assigned(GEO) then
     begin
     P1 := InConvertLocation(P1);
     Result := GEO.TV1.AddPartRelativePoint(P1.X, P1.Y, P1.Z, PointName);
     end
  else
     begin
     Result := -1;
     end
end;

// Add a report point at a position P1 to 0,0,0 of machine and attach it to the part datum
function GEOPluginAddMachineRelativePoint(PName :PChar;  P1 : TPLocation): Integer;stdcall;
var
PointName : String;
begin
  PointName := BadCharReplace(PName);
  if Assigned(GEO) then
     begin
     P1 := InConvertLocation(P1);
      Result := GEO.TV1.AddMachineRelativePoint(P1.X, P1.Y, P1.Z, PointName);
     end
     else
     begin
     Result := -1;
     end
end;


// Returns the Real World position of the point in space
// (relative to Part Datum being 0,0,0) in te var P1
function GEOPluginGetPoint(PName :PChar; var P1 : TPLocation): Boolean;stdcall;
var
PointName : String;
begin
PointName := BadCharReplace(PName);
Result := false;
if Assigned(GEO) then
     begin
     Result := GEO.GetPointData(PointName,P1);
     P1 := OutConvertLocation(P1);
     end;
end;

// Add a report point at a position P1 relative to the Tool datum
function GEOPluginAddToolRelativePoint(PName :PChar; P1 : TPLocation): Integer;stdcall;
begin
  if Assigned(GEO) then
     begin
     P1 := InConvertLocation(P1);
     Result := GEO.TV1.AddToolRelativePoint(P1.X, P1.Y, P1.Z, PName);
     end
     else
     begin
     Result := -1;
     end
end;


{This function affects the position of the 'Electrode' or Probe Datum in the
machine model and moves the displayed and reported ElectrodeDatum Point}
procedure GEOPluginSetToolOffset(P1 : TPLocation)stdcall;
var
P1T : TPLocation;

begin
  if Assigned(GEO) then
     begin
     P1 := InConvertLocation(P1);
     P1T := GTTransform(P1);
     GEO.SetToolOffset(P1T);
     end
end;

// Get named vector from PCM
// pass to ACN the position of vector points when rotary axes are at given
// ABC in the vars P1 and P2
procedure GEOPluginGetProbeVector(aName : PChar;A,B,C :Double; var P1, P2 : TPLocation); stdcall;
var
PCMP1,PCMP2 : TPlocation;
begin
if Assigned(GEO) then
     begin
     // Get Vecto Points from PCM into PCMP1 and PCMP2
     GEO.GetPCMProbeVector(aName,PCMP1,PCMP2);
     // Add the points to the model
     // Note x and Y are negated as they are on the Part . Z is on the tool
//NGT!!!    GEO.TV1.AddVector(-PCMP1.x,-PCMP1.y,PCMP1.z,-PCMP2.x,-PCMP2.y,PCMP2.z, rvPartRelative, aName);
//NGT!!!     GEO.TV1.AddVector(-PCMP1.x,-PCMP1.y,PCMP1.z,-PCMP2.x,-PCMP2.y,PCMP2.z, rvPartRelative, aName);
     GEO.TV1.AddVector(PCMP1.x,PCMP1.y,PCMP1.z,PCMP2.x,PCMP2.y,PCMP2.z, rvPartRelative, aName);
     GEO.TV1.AddVector(PCMP1.x,PCMP1.y,PCMP1.z,PCMP2.x,PCMP2.y,PCMP2.z, rvPartRelative, aName);
     try
     // make temporary points in order to find axis position for point usung current tool offset
//NGT!!!     GEO.TV1.AddReportPointat(-PCMP1.x,-PCMP1.y,PCMP1.z,'temp1',rpPartRelative,True,False);
     GEO.TV1.AddReportPointat(PCMP1.x,PCMP1.y,PCMP1.z,'temp1',rpPartRelative,True,False);
     P1 := GEO.GetAxisPosForPoint('temp1',A,B,C);
//NGT!!!     GEO.TV1.AddReportPointat(-PCMP2.x,-PCMP2.y,PCMP2.z,'temp2',rpPartRelative,True,False);
     GEO.TV1.AddReportPointat(PCMP2.x,PCMP2.y,PCMP2.z,'temp2',rpPartRelative,True,False);
     P2 := GEO.GetAxisPosForPoint('temp2',A,B,C);
     finally
     // Delete the temporary points
     P1 := OutConvertLocation(P1);
     P2 := OutConvertLocation(P2);
     GEO.TV1.DeleteReportPointCalled('temp1');
     GEO.TV1.DeleteReportPointCalled('temp2');
     end;
     if (GEO.FDMode = dmOnMachine) then
        begin
        GEO.UpdateMachineReportDataAndDisplay;
        GEO.UpdateVectorSelectList;
        end;
     end;
end;

// This procedure returns the absolute XYZ axis positions which place the current
// Electrode datum Point co-incident with the named point given the ABC axis positions;
procedure GEOPluginMachinePosForPoint(aName : PChar; A,B,C : Double; var XYZPos :TPLocation); stdcall;
begin

if assigned(GEO) then XYZPos := GEO.GetAxisPosForPoint(aName,A,B,C);
XYZPos := OutConvertLocation(XYZPos);

end;

{ This procedure is used to create a PartRelative Point of the given name
at the Electrode Datum Position for the given axis positions
It assumes the passed in positions have been corrcted for tool ball radius.
This is used to 'attach' points to the Part Dtaum from  probe data after it has
been corrected for tool ball radius.
}
procedure GEOPluginProbePositionToMachineRelativePoint(PName :PChar;X,Y,Z,A,B,C : Double);stdcall;
begin
X := InConvertPosn(X);
Y := InConvertPosn(Y);
Z := InConvertPosn(Z);
if assigned(GEO) then GEO.ProbePosnToPartRelativePoint(PName,X,Y,Z,A,B,C);
end;

// Calculate corrected axis position starting at position defined by XYZABC and returning
// answer in XYZABC. Use of C axis for comp is controled by UseCAxis Parameter.
procedure GEOPluginCorrectForStackingAxis(UseCAxis : Boolean; var X,Y,Z,A,B,C : Double); stdcall;
begin
if Assigned(GEO) then begin
  X := InConvertPosn(X);
  Y := InConvertPosn(Y);
  Z := InConvertPosn(Z);
  GEO.CorrectForStackingAxis(UseCAxis,X,Y,Z,A,B,C);
  X := OutConvertPosn(X);
  Y := OutConvertPosn(Y);
  Z := OutConvertPosn(Z);
  end;
end;

// START OF NOT CURRENTLY IMPLEMENTED
function GEOPluginGetPointAtDatum( P1 : TPLocation):TPLocation; stdcall;
begin
  if Assigned(GEO) then
    begin
    P1 := InConvertLocation(P1);
    Result := GEO.TV1.GetPointAtDatum(P1.x, P1.y, P1.z);
    Result := OutConvertLocation(Result);
    end
   else
     begin
     Result.X := 0;
     Result.Y := 0;
     Result.Z := 0;
     end
end;


// Get Nominal vector points by name from PCm
// Return data after correction for stacking axis in var parameters P1 , P2
procedure GEOPluginGetAdjustProbeVector (aName : PChar; P1, P2 : TXYZ); stdcall;
begin
end;



procedure GEOPluginAddReportPoint(VName : PChar; P1 : TPLocation); stdcall;
begin
  if Assigned(GEO) then
     begin
     end;
end;

procedure GEOPluginAddVector(VName : PChar;  P1, P2 : TPLocation); stdcall;
var
VectorName : String;
begin
  VectorName := BadCharReplace(VName);
  if Assigned(GEO) then
     begin
     P1 := InConvertLocation(P1);
     P2 := InConvertLocation(P2);
     GEO.TV1.AddVector(P1.x, P1.y, P1.z, P2.x, P2.y, P2.z, rvPartRelative, VectorName);
     if (GEO.FDMode = dmOnMachine) then GEO.TV1.Update3DDisplay(GEO.Paper,umCustomOnly);
     P1 := OutConvertLocation(P1);
     P2 := OutConvertLocation(P2);
     end;
end;

procedure GetStackingVectors(var Pa1, Pa2, RotaP,Pn1, Pn2, RotnP : TXYZ)  ;
var
NomVec,ActVec,NomTiming,ActTiming  :TVectorData;
begin
if not assigned(GEO) then exit;
if (GEO.TV1.NominalVectorIndex >= 0) and (GEO.TV1.ActualVectorIndex >= 0) then
   begin
   NomVec := GEO.TV1.FReportVectorList[GEO.TV1.NominalVectorIndex];
   ActVec := GEO.TV1.FReportVectorList[GEO.TV1.ActualVectorIndex];
   end;

if (GEO.TV1.ATVectorIndex >= 0) and (GEO.TV1.NTVectorIndex >= 0) then
   begin
   NomTiming := GEO.TV1.FReportVectorList[GEO.TV1.NTVectorIndex];
   ActTiming := GEO.TV1.FReportVectorList[GEO.TV1.ATVectorIndex];
   end;
Pn1 := OutConvertXYZ(TXYZ(NomVec.Point1Position));
Pn2 := OutConvertXYZ(TXYZ(NomVec.Point2Position));
Pa1 := OutConvertXYZ(TXYZ(ActVec.Point1Position));
Pa2 := OutConvertXYZ(TXYZ(ActVec.Point2Position));
RotaP := OutConvertXYZ(TXYZ(ActTiming.Point2Position));
RotnP := OutConvertXYZ(TXYZ(NomTiming.Point2Position));
end;

// Modeified in Version 1.1.0 (sign of X and Y changed)
procedure   GEOPluginGetStackingAxis(var Pa1, Pa2, RotaP,Pn1, Pn2, RotnP : TXYZ); stdcall;
begin
  if Assigned(GEO) then
     begin
     GEO.PCM.GetAdjustedStackingAxis(Pa1, Pa2, RotaP);
     GEO.PCM.GetStackingAxis(Pn1, Pn2, RotnP);
     GEO.TV1.SetNominalStackingAxis(Pn1.x, Pn1.y, Pn1.z,
                                   Pn2.x, Pn2.y, Pn2.z,
                                   RotnP.x, RotnP.y, RotnP.z);
     GEO.TV1.SetActualStackingAxis (Pa1.x, Pa1.y, Pa1.z,
                                   Pa2.x, Pa2.y, Pa2.z,
                                   RotaP.x, RotaP.y, RotaP.z);
     if (GEO.FDMode = dmOnMachine) then
        begin
        GEO.UpdateMachineReportDataAndDisplay;
        end;
     Pa1 :=OutConvertXYZ(Pa1);
     Pa2 :=OutConvertXYZ(Pa2);
     Pn1 :=OutConvertXYZ(Pn1);
     Pn2 :=OutConvertXYZ(Pn2);
     RotaP :=OutConvertXYZ(RotaP);
     RotnP :=OutConvertXYZ(RotnP);
     end;
end;

{procedure   GEOPluginGetStackingAxis(var Pa1, Pa2, RotaP,Pn1, Pn2, RotnP : TXYZ); stdcall;
begin
  if Assigned(GEO) then
     begin
     GEO.PCM.GetAdjustedStackingAxis(Pa1, Pa2, RotaP);
     GEO.PCM.GetStackingAxis(Pn1, Pn2, RotnP);
     GEO.TV1.SetNominalStackingAxis(-Pn1.x, -Pn1.y, Pn1.z,
                                   -Pn2.x, -Pn2.y, Pn2.z,
                                   -RotnP.x, -RotnP.y, RotnP.z);
     GEO.TV1.SetActualStackingAxis (-Pa1.x, -Pa1.y, Pa1.z,
                                   -Pa2.x, -Pa2.y, Pa2.z,
                                   -RotaP.x, -RotaP.y, RotaP.z);
     if (GEO.FDMode = dmOnMachine) then
        begin
        GEO.UpdateMachineReportDataAndDisplay;
        end;
     Pa1 :=OutConvertXYZ(Pa1);
     Pa2 :=OutConvertXYZ(Pa2);
     Pn1 :=OutConvertXYZ(Pn1);
     Pn2 :=OutConvertXYZ(Pn2);
     RotaP :=OutConvertXYZ(RotaP);
     RotnP :=OutConvertXYZ(RotnP);
     end;
end;}

{procedure   GEOPluginSetStackingAxis(Pa1, Pa2, RotaP,Pn1, Pn2, RotnP : TXYZ); stdcall;
begin
  if Assigned(GEO) then
     begin
     Pa1 := InConvertXYZ(Pa1);
     Pa2 := InConvertXYZ(Pa2);
     Pn1 := InConvertXYZ(Pn1);
     Pn2 := InConvertXYZ(Pn2);
     RotaP := InConvertXYZ(RotaP);
     RotnP := InConvertXYZ(RotnP);
     GEO.TV1.SetNominalStackingAxis(-Pn1.x, -Pn1.y, Pn1.z, -pn2.x, -pn2.y, pn2.z, -RotnP.x,-RotnP.y, RotnP.z);
     GEO.TV1.SetActualStackingAxis(-Pa1.x, -Pa1.y, Pa1.z, -pa2.x, -pa2.y, pa2.z, -RotaP.x,-RotaP.y, RotaP.z);
     if (GEO.FDMode = dmOnMachine) then
        begin
        GEO.UpdateMachineReportDataAndDisplay;
        end;
     end;
end;}

// Modeified in Version 1.1.0 (sign of X and Y changed)
procedure   GEOPluginSetStackingAxis(Pa1, Pa2, RotaP,Pn1, Pn2, RotnP : TXYZ); stdcall;
begin
  if Assigned(GEO) then
     begin
     Pa1 := InConvertXYZ(Pa1);
     Pa2 := InConvertXYZ(Pa2);
     Pn1 := InConvertXYZ(Pn1);
     Pn2 := InConvertXYZ(Pn2);
     RotaP := InConvertXYZ(RotaP);
     RotnP := InConvertXYZ(RotnP);
     GEO.TV1.SetNominalStackingAxis(Pn1.x, Pn1.y, Pn1.z, pn2.x, pn2.y, pn2.z, RotnP.x,RotnP.y, RotnP.z);
     GEO.TV1.SetActualStackingAxis(Pa1.x, Pa1.y, Pa1.z, pa2.x, pa2.y, pa2.z, RotaP.x,RotaP.y, RotaP.z);
     if (GEO.FDMode = dmOnMachine) then
        begin
        GEO.UpdateMachineReportDataAndDisplay;
        end;
     end;
end;

// Add a report point at a position P1 relative to the Tool datum
procedure GEOPluginNDPReset();stdcall;
begin
  if Assigned(GEO) then
     begin
     GEO.TV1.NominalsReset;
     end
end;

procedure GEOPluginSetNDPOffset(Index : Integer; Offset : TXYZ; A,B,C:Double)stdcall;
begin
  if Assigned(GEO) then
     begin
     if not(GEO.GEONavInstalled) then
        begin
        raise EGEOError.Create('GEONav Not Installed');
        end
     else
        begin
        if (Index>=0) and (Index<=4) then
          begin
          Offset := InConvertXYZ(Offset);
          A := InConvertPosn(A);
          B := InConvertPosn(B);
          C := InConvertPosn(C);
          GEO.TV1.SetAndSaveNominalOffset(Index,Offset.X,Offset.Y,Offset.Z,A,B,C);
          end
        end
     end;
end;

{function GEOPluginSetRadialHoleNDPVector(Radius,RadialAngle,Height,BAngle : Double):Integer stdcall;
var
P1,P2 : TXYZ;
P2AtZero : TXYZ;
RadialAngleRadians : Double;
BAngleRadians : Double;
ListPos : INteger;
VName : String;
P1T,P2T : TXYZ;
begin
  if Assigned(GEO) then
    begin
     if not(GEO.GEONavInstalled) then
        begin
        raise EGEOError.Create('GEONav Not Installed');
        end
     else
       begin
       RadialAngleRadians := DegToRad(RadialAngle);
       BAngleRadians      := DegToRad(BAngle);
       P1.X := Radius*Cos(RadialAngleRadians);
       P1.Y := Radius*Sin(RadialAngleRadians);
       P1.Z := Height;
       P2AtZero.X := Radius+(10*Cos(BAngleRadians));
       P2AtZero.Y := 0;
       P2AtZero.Z := Height+(10*Sin(Bangle));
       P2.X := P2AtZero.X*Cos(RadialAngleRadians);
       P2.Y := P2AtZero.X*Sin(RadialAngleRadians);
       P2.Z := P2AtZero.Z;
       ListPos := Length(GEO.TV1.FReportVectorList);
       Vname := NDPName;
       P1T := GTTransform(P1);
       P2T := GTTransform(P2);
       GEO.TV1.AddActualStackingAxisRelativeVector(P1T.x, P1T.y, P1T.z, P2T.x, P2T.y, P2T.z, rvDrillNominal, PAnsiChar(VName));
       Result := 1;
       end
    end
  else
    begin
    Result := 0
    end;
end;}

function GEOPluginSetNDPVector(P1,P2 : TXYZ):Integer stdcall;
var
ListPos : INteger;
VName : String;
P1T,P2T : TXYZ;
begin
  P1T := GTTransform(P1);
  P2T := GTTransform(P2);
  if Assigned(GEO) then
     begin
     if not(GEO.GEONavInstalled) then
        begin
        raise EGEOError.Create('GEONav Not Installed');
        end
     else
       begin
       P1 := InConvertXYZ(P1);
       P2 := InConvertXYZ(P2);
       ListPos := Length(GEO.TV1.FReportVectorList);
       Vname := NDPName;
       GEO.TV1.AddActualStackingAxisRelativeVector(P1T.x, P1T.y, P1T.z, P2T.x, P2T.y, P2T.z, rvDrillNominal, PAnsiChar(VName));
       Result := 1;
       end
     end
  else
     begin
     Result := 0;
     end;
end;



procedure GEOPluginSetToolApproach(I,J,K : Double);stdcall;
begin
  if Assigned(GEO) then
     begin
     if not(GEO.GEONavInstalled) then
        begin
        raise EGEOError.Create('GEONav Not Installed');
        end
     else
        begin
        GEO.TV1.SetToolApproach(I,J,K);
        end
     end;
end;


procedure GEOPluginBeginUpdate();stdcall;
begin
  if Assigned(GEO) then
     begin
     GEO.ExternalBeginUpdate;
     end;

end;
procedure GEOPluginEndUpdate();stdcall;
begin
  if Assigned(GEO) then
     begin
     GEO.ExternalEndUpdate;
     end;
end;


// END OF NOT CURRENTLY IMPLEMENTED

exports
// These are used by acnc32
  GEOPluginCreate,
  GEOPluginDestroy,

  GEOPluginReset,
  GEOPluginLoadConfig,
  GEOPluginAddPartRelativePoint,
  GEOPluginAddMachineRelativePoint,
  GEOPluginAddToolRelativePoint,
  GEOPluginGetPoint,
  GEOPluginShow,
  GEOPluginHide,
  GEOPluginSetToolOffset,
  GEOPluginProbePositionToMachineRelativePoint,
  GEOPluginMachinePosForPoint,
  GEOPluginCorrectForStackingAxis,
  GEOPluginGetStackingAxis,
  GEOPluginSetStackingAxis,
  ACNC32PluginRevision,
  GEOPluginSetProbePoint,
  GEOPluginGetFitQuality,

// New in version 2.0 for Nominal Generation ACNC (ab0)
  GEOPluginNDPReset,
  GEOPluginSetNDPOffset,
  GEOPluginSetNDPVector,
//  GEOPluginGetMachineNDP,
  GEOPluginSetToolApproach,


// New For Version ?????
  GEOPluginBeginUpdate,
  GEOPluginEndUpdate,

// Are these  currently used ?????
  GEOPluginGetPointAtDatum,
  GEOPluginSetAxisPositions,
  GEOPluginGetProbeVector,

//  GEOPluginAddVector,
  GEOPluginAddReportPoint,
  GEOPluginGetAdjustProbeVector;

// New in version 2.1 for use on combusters and Rings
//  GEOPluginSetRadialHoleNDPVector;
end.

