{
SendNativeCommand
GetSpreadsheetValue
LoadJobCalled
GetCurrentJobName
SetLiveVideo
GetSymbolicValue
SetSymbolicInteger
SetSymbolicFloat
SetSymbolicString
RunJob
}
unit VisionLanguageForm;

interface
uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls,Buttons, ExtCtrls, ComCtrls,Menus, INamedInterface,  Grids,
  NamedPlugin, IFormInterface, CNCTypes, AbstractFormPlugin,IniFiles,
  AbstractScript,IScriptInterface,IStringArray;
type
  TNCLanguageFunction = (tccNone,tccSendNativeCommand,tccGetSpreadsheetValue,tccGetSymbolicValue,tccSetSymbolicInteger,
                         tccSetSymbolicFloat,tccSetSymbolicString,tccLoadJobCalled,tccGetCurrentJobName,tccSetLiveVideo,
                         tccResetandRunJob,tccSetJobScaling,tccSetJobOffset,tccCheckJobPass);

  TNCCommands = (tncNone,tncLogin,tncFromForm,tncGetSpreadsheetValue,tncGetCellValue,tncSendNativeCommand,
                 tncGetSymbolicValue,tncSetSymbolicValueAsString,tncSetSymbolicValueAsFloat,tncSetSymbolicValueAsInteger,
                 tncLoadJobCalled,tncGetJobList,tncGetCurrentJobName,tncSetLiveState,tncResetandRunJob);


  TVLangForm = class(TInstallForm)
  private
    { Private declarations }
    InstalledVisionName : String;
//    DefaultLiveJob      : String;
    LiveOnReset         : Boolean;
    procedure TagChanged(Tag : TTagRef);

  public
    { Public declarations }
    NCCommandHasTimedOut : Boolean;
    //**********************
    // TAGS
    //**********************
    VisionResponseTag         : TTagref;
    VisionCommandStatusTag    : TTagref;
    VisionFunctionIndexTag    : TTagref;
    VisionRunFunctionTag      : TTagref;
    VisionCanTalkTag          : TTagref;
    VisionParam1Tag           : TTagref;
    VisionParam2Tag           : TTagref;
    VisionParam3Tag           : TTagref;
    CurrentJobNameTag         : TTagref;
    LiveDefaultJobTag         : TTagref;
    DefaultJobNameTag         : TTagref;
    ShowCameraTag             : TTagref;
    InCycleTag                : TTagref;
    VisionResponseString      : String;

    constructor Create(AOwner : TComponent); override;
    procedure Install;override;
    procedure FinalInstall; override;
    procedure Shutdown; override;
//    procedure DisplaySweep; override;
    function CanTalk : Boolean;


   //***********************************************************************
   // Vision Comands implementataions
   //***********************************************************************
    function NCSendNativeCommand(CommandString : String): Boolean;
    function NCGetSpreadsheetValue(CommandString: String): Boolean;
    function NCGetCellValue(CommandString: String): Boolean;
    function NCLoadJobCalled(JobName: String;ForceLoad : Integer;LatchJob : Integer): Boolean;
    function NCGetCurrentJobName: Boolean;
    function NCGetJobList: Boolean;
    function NCSetLiveVideo(LiveState : Boolean):Boolean;
    function NCGetSymbolicValue(Command : String):Boolean;
    function NCSetSymbolicValueAsString(Command : String):Boolean;
    function NCSetSymbolicValueAsInteger(Command : String):Boolean;
    function NCSetSymbolicValueAsFloat(Command : String):Boolean;
    function NCSetJobScaling(Scaling: Double):Boolean;
    function NCSetJobOffset(Xoffset,YOffset : Double):Boolean;
    function NCResetandRunJob():Boolean;
    function NCCheckJobPass():Boolean;
    end;


//***********************************************************************
// Vision Comand Definitions
//***********************************************************************

  TSetScale = class (TScriptMethod)
  public
    procedure Reset; override; stdcall;
    function Name: WideString; override; stdcall;
    function IsFunction: Boolean; override; stdcall;
    function ParameterPassing(Index: Integer): Integer; override; stdcall;
    function ParameterType(Index: Integer): Integer; override; stdcall;
    function ParameterName(Index: Integer): WideString; override; stdcall;
    function ReturnType: Integer; override; stdcall;
    function ParameterCount: Integer; override; stdcall;
    procedure Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer); override; stdcall;
  end;

  TSetOffset = class (TScriptMethod)
  public
    procedure Reset; override; stdcall;
    function Name: WideString; override; stdcall;
    function IsFunction: Boolean; override; stdcall;
    function ParameterPassing(Index: Integer): Integer; override; stdcall;
    function ParameterType(Index: Integer): Integer; override; stdcall;
    function ParameterName(Index: Integer): WideString; override; stdcall;
    function ReturnType: Integer; override; stdcall;
    function ParameterCount: Integer; override; stdcall;
    procedure Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer); override; stdcall;
  end;

  TSendNativeCommand = class (TScriptMethod)
  public
    procedure Reset; override; stdcall;
    function Name: WideString; override; stdcall;
    function IsFunction: Boolean; override; stdcall;
    function ParameterPassing(Index: Integer): Integer; override; stdcall;
    function ParameterType(Index: Integer): Integer; override; stdcall;
    function ParameterName(Index: Integer): WideString; override; stdcall;
    function ReturnType: Integer; override; stdcall;
    function ParameterCount: Integer; override; stdcall;
    procedure Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer); override; stdcall;
  end;

  TGetSpreadsheetValue = class (TScriptMethod)
  public
    procedure Reset; override; stdcall;
    function Name: WideString; override; stdcall;
    function IsFunction: Boolean; override; stdcall;
    function ParameterPassing(Index: Integer): Integer; override; stdcall;
    function ParameterType(Index: Integer): Integer; override; stdcall;
    function ParameterName(Index: Integer): WideString; override; stdcall;
    function ReturnType: Integer; override; stdcall;
    function ParameterCount: Integer; override; stdcall;
    procedure Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer); override; stdcall;
  end;


  TLoadJobCalled = class (TScriptMethod)
  public
    procedure Reset; override; stdcall;
    function Name: WideString; override; stdcall;
    function IsFunction: Boolean; override; stdcall;
    function ParameterPassing(Index: Integer): Integer; override; stdcall;
    function ParameterType(Index: Integer): Integer; override; stdcall;
    function ParameterName(Index: Integer): WideString; override; stdcall;
    function ReturnType: Integer; override; stdcall;
    function ParameterCount: Integer; override; stdcall;
    procedure Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer); override; stdcall;
  end;

  TGetCurrentJobName = class (TScriptMethod)
  public
    procedure Reset; override; stdcall;
    function Name: WideString; override; stdcall;
    function IsFunction: Boolean; override; stdcall;
    function ParameterPassing(Index: Integer): Integer; override; stdcall;
    function ParameterType(Index: Integer): Integer; override; stdcall;
    function ParameterName(Index: Integer): WideString; override; stdcall;
    function ReturnType: Integer; override; stdcall;
    function ParameterCount: Integer; override; stdcall;
    procedure Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer); override; stdcall;
  end;

  TSetLiveVideo = class (TScriptMethod)
  public
    procedure Reset; override; stdcall;
    function Name: WideString; override; stdcall;
    function IsFunction: Boolean; override; stdcall;
    function ParameterPassing(Index: Integer): Integer; override; stdcall;
    function ParameterType(Index: Integer): Integer; override; stdcall;
    function ParameterName(Index: Integer): WideString; override; stdcall;
    function ReturnType: Integer; override; stdcall;
    function ParameterCount: Integer; override; stdcall;
    procedure Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer); override; stdcall;
  end;

  TGetSymbolicValue = class (TScriptMethod)
  public
    procedure Reset; override; stdcall;
    function Name: WideString; override; stdcall;
    function IsFunction: Boolean; override; stdcall;
    function ParameterPassing(Index: Integer): Integer; override; stdcall;
    function ParameterType(Index: Integer): Integer; override; stdcall;
    function ParameterName(Index: Integer): WideString; override; stdcall;
    function ReturnType: Integer; override; stdcall;
    function ParameterCount: Integer; override; stdcall;
    procedure Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer); override; stdcall;
  end;

  TSetSymbolicInteger = class (TScriptMethod)
  public
    procedure Reset; override; stdcall;
    function Name: WideString; override; stdcall;
    function IsFunction: Boolean; override; stdcall;
    function ParameterPassing(Index: Integer): Integer; override; stdcall;
    function ParameterType(Index: Integer): Integer; override; stdcall;
    function ParameterName(Index: Integer): WideString; override; stdcall;
    function ReturnType: Integer; override; stdcall;
    function ParameterCount: Integer; override; stdcall;
    procedure Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer); override; stdcall;
  end;

  TSetSymbolicFloat = class (TScriptMethod)
  public
    procedure Reset; override; stdcall;
    function Name: WideString; override; stdcall;
    function IsFunction: Boolean; override; stdcall;
    function ParameterPassing(Index: Integer): Integer; override; stdcall;
    function ParameterType(Index: Integer): Integer; override; stdcall;
    function ParameterName(Index: Integer): WideString; override; stdcall;
    function ReturnType: Integer; override; stdcall;
    function ParameterCount: Integer; override; stdcall;
    procedure Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer); override; stdcall;
  end;

  TSetSymbolicString = class (TScriptMethod)
  public
    procedure Reset; override; stdcall;
    function Name: WideString; override; stdcall;
    function IsFunction: Boolean; override; stdcall;
    function ParameterPassing(Index: Integer): Integer; override; stdcall;
    function ParameterType(Index: Integer): Integer; override; stdcall;
    function ParameterName(Index: Integer): WideString; override; stdcall;
    function ReturnType: Integer; override; stdcall;
    function ParameterCount: Integer; override; stdcall;
    procedure Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer); override; stdcall;
  end;

  TTriggerCurrentJob = class (TScriptMethod)
  public
    procedure Reset; override; stdcall;
    function Name: WideString; override; stdcall;
    function IsFunction: Boolean; override; stdcall;
    function ParameterPassing(Index: Integer): Integer; override; stdcall;
    function ParameterType(Index: Integer): Integer; override; stdcall;
    function ParameterName(Index: Integer): WideString; override; stdcall;
    function ReturnType: Integer; override; stdcall;
    function ParameterCount: Integer; override; stdcall;
    procedure Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer); override; stdcall;
  end;

  TCheckJobPassed = class (TScriptMethod)
  public
    procedure Reset; override; stdcall;
    function Name: WideString; override; stdcall;
    function IsFunction: Boolean; override; stdcall;
    function ParameterPassing(Index: Integer): Integer; override; stdcall;
    function ParameterType(Index: Integer): Integer; override; stdcall;
    function ParameterName(Index: Integer): WideString; override; stdcall;
    function ReturnType: Integer; override; stdcall;
    function ParameterCount: Integer; override; stdcall;
    procedure Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer); override; stdcall;
  end;
  var
  VLangForm: TVLangForm;

implementation

{$R *.dfm}

{ TForm3 }


procedure TagChangeHandler(Tag : TTagRef);stdcall;
begin
VLangForm.TagChanged(Tag);
end;

procedure TVLangForm.TagChanged(Tag: TTagRef);
begin
end;


procedure TVLangForm.Install;
var
 ConfigFilename : String;
 CFile : TIniFile;
 Buffer : String;
 FOK : Boolean;
begin
  inherited;
//  JobLatch := False;
  FOK := False;
  ConfigFileName := ExtractFilePath(Application.ExeName);
  ConfigFileName := Format('%sLive\VisionLanguage.cfg',[ConfigFileName]);
  if FileExists(ConfigFileName) then
    begin
    CFile := TIniFile.Create(ConfigFilename);
    if assigned(CFile) then
      begin
      FOK := True;
      InstalledVisionName := CFile.ReadString('GENERAL','InstalledVisionName','Vision');
      Buffer         :=      CFile.ReadString('GENERAL','LiveOnReset','True');
      If (Buffer[1] = 't') or (Buffer[1] = 'T') then
        LiveOnReset := True
      else
        LiveOnReset := False;
    end
  end;
  If not FOK then
    begin
      InstalledVisionName := 'Vision';
      LiveOnReset := True;
    end;
end;

procedure TVLangForm.FinalInstall;
begin
  inherited;
   ShowCameraTag              := Named.AquireTag(PAnsiChar(InstalledVisionName+'ShowCamera'),'TIntegerTag',nil);
   VisionCommandStatusTag     := Named.AquireTag(PAnsiChar(InstalledVisionName+'CommandStatus'),'TIntegerTag',TagChangeHandler);
   VisionFunctionIndexTag     := Named.AquireTag(PAnsiChar(InstalledVisionName+'FunctionIndex'),'TIntegerTag',TagChangeHandler);
   VisionRunFunctionTag       := Named.AquireTag(PAnsiChar(InstalledVisionName+'RunFunction'),'TIntegerTag',TagChangeHandler);
   VisionCanTalkTag           := Named.AquireTag(PAnsiChar(InstalledVisionName+'CanTalk'),'TIntegerTag',TagChangeHandler);
   VisionResponseTag          := Named.AquireTag(PAnsiChar(InstalledVisionName+'Response'),'TStringTag',TagChangeHandler);
   VisionParam1Tag            := Named.AquireTag(PAnsiChar(InstalledVisionName+'Param1'),'TStringTag',TagChangeHandler);
   VisionParam2Tag            := Named.AquireTag(PAnsiChar(InstalledVisionName+'Param2'),'TStringTag',TagChangeHandler);
   VisionParam3Tag            := Named.AquireTag(PAnsiChar(InstalledVisionName+'Param3'),'TStringTag',TagChangeHandler);
   CurrentJobNameTag          := Named.AquireTag(PAnsiChar(InstalledVisionName+'CurrentJob'),'TStringTag',TagChangeHandler);
   DefaultJobNameTag          := Named.AquireTag(PAnsiChar(InstalledVisionName+'DefaultltJobName'),'TStringTag',TagChangeHandler);
end;


{procedure TVLangForm.Activate;
begin
  inherited;
end;

procedure TVLangForm.CloseForm;
begin
  inherited;

end;}


//**************************************************************
// NCCommand FUNCTIONS
//**************************************************************
function TVLangForm.NCSendNativeCommand(CommandString: String): Boolean;
var
FIndex : Integer;
begin
Result := False;
if CanTalk then
  begin
  FIndex := cardinal(tccSendNativeCommand);
  Named.SetAsInteger(VisionFunctionIndexTag,FIndex);
  Named.SetAsString(VisionParam1Tag,PAnsiChar(CommandString));
  Named.SetAsInteger(VisionRunFunctionTag,1);
  Named.SetAsInteger(VisionRunFunctionTag,0);
  Result := True;
  end;
end;


function TVLangForm.NCGetSpreadsheetValue(CommandString : String): Boolean;
var
FIndex : Integer;
begin
Result := False;
if CanTalk then
  begin
  FIndex := cardinal(tccGetSpreadsheetValue);
  Named.SetAsInteger(VisionFunctionIndexTag,FIndex);
  Named.SetAsString(VisionParam1Tag,PAnsiChar(CommandString));
  Named.SetAsInteger(VisionRunFunctionTag,1);
  Named.SetAsInteger(VisionRunFunctionTag,0);
  Result := True;
  end;

end;

function TVLangForm.NCGetCellValue(CommandString: String): Boolean;
var
FIndex : Integer;
begin
Result := False;

Result := False;
{if CanTalk then
  begin
  if CTelnet.IsConnected then
    begin
    NCCommand := True;
    NCCommandTimeout.Enabled := True;
    ResponseCount := 0;
    VisionResponseString := '';
    NCCommandType :=tncGetCellValue;
//    Named.SetAsString(VisionCommandStatusTag,'0');
    CTelnet.SendCommand(CommandString,False,False);
    Result := True;
    end;
  end;}
end;

function TVLangForm.NCSetJobScaling(Scaling : Double): Boolean;
var
FIndex : Integer;
begin
Result := False;
if CanTalk then
    begin
    FIndex := cardinal(tccSetJobScaling);
    Named.SetAsInteger(VisionCommandStatusTag,0);
    Named.SetAsInteger(VisionFunctionIndexTag,FIndex);
    Named.SetAsDouble(VisionParam1Tag,Scaling);
    Named.SetAsInteger(VisionRunFunctionTag,1);
    Named.SetAsInteger(VisionRunFunctionTag,0);
    Result := True;
    end;

end;

function TVLangForm.NCSetJobOffset(Xoffset, YOffset: Double): Boolean;
var
FIndex : Integer;
begin
Result := False;
if CanTalk then
    begin
    FIndex := cardinal(tccSetJobOffset);
    Named.SetAsInteger(VisionCommandStatusTag,0);
    Named.SetAsInteger(VisionFunctionIndexTag,FIndex);
    Named.SetAsDouble(VisionParam1Tag,XOffset);
    Named.SetAsDouble(VisionParam2Tag,YOffset);
    Named.SetAsInteger(VisionRunFunctionTag,1);
    Named.SetAsInteger(VisionRunFunctionTag,0);
    Result := True;
    end;

end;

function TVLangForm.NCLoadJobCalled(JobName: String;ForceLoad : Integer;LatchJob : Integer): Boolean;
var
FIndex : Integer;
begin
Result := False;
if CanTalk then
    begin
    FIndex := cardinal(tccLoadJobCalled);
    Named.SetAsInteger(VisionCommandStatusTag,0);
    Named.SetAsInteger(VisionFunctionIndexTag,FIndex);
    Named.SetAsString(VisionParam1Tag,PAnsiChar(JobName));
    Named.SetAsInteger(VisionParam2Tag,ForceLoad);
    Named.SetAsInteger(VisionParam3Tag,LatchJob);
    Named.SetAsInteger(VisionRunFunctionTag,1);
    Named.SetAsInteger(VisionRunFunctionTag,0);
    Result := True;
    end;
end;

function TVLangForm.NCGetCurrentJobName: Boolean;
var
FIndex : Integer;
begin
Result := False;
if CanTalk then
    begin
    Named.SetAsInteger(VisionCommandStatusTag,0);
    FIndex := cardinal(tccGetCurrentJobName);
    Named.SetAsInteger(VisionFunctionIndexTag,FIndex);
    Named.SetAsInteger(VisionRunFunctionTag,1);
    Named.SetAsInteger(VisionRunFunctionTag,0);
    Result := True;
    end;
end;

function TVLangForm.NCGetJobList: Boolean;
var
FIndex : Integer;
begin
Result := False;
if CanTalk then
    begin
    Named.SetAsInteger(VisionCommandStatusTag,0);
    FIndex := cardinal(tccGetCurrentJobName);
    Named.SetAsInteger(VisionFunctionIndexTag,FIndex);
    Named.SetAsInteger(VisionRunFunctionTag,1);
    Named.SetAsInteger(VisionRunFunctionTag,0);
    Result := True;
    end;
end;

function TVLangForm.NCSetLiveVideo(LiveState: Boolean): Boolean;
var
CommandString : String;
FIndex : Integer;
begin
Named.SetAsInteger(VisionCommandStatusTag,0);
CommandString := 'Put Live ';
if LiveState then
  begin
    Named.SetAsString(VisionParam1Tag,'1');
  end
else
  begin
    Named.SetAsString(VisionParam1Tag,'0');
  end;
Named.SetAsInteger(ShowCameraTag,1);
Named.SetAsInteger(ShowCameraTag,0);
Result := False;
if CanTalk then
    begin
    FIndex := cardinal(tccSetLiveVideo);
    Named.SetAsInteger(VisionFunctionIndexTag,FIndex);
    Named.SetAsInteger(VisionRunFunctionTag,1);
    Named.SetAsInteger(VisionRunFunctionTag,0);
    Result := True;
    end;

end;


function TVLangForm.NCGetSymbolicValue(Command: String): Boolean;
var
FIndex : Integer;
begin
Result := False;
if CanTalk then
    begin
    Named.SetAsInteger(VisionCommandStatusTag,0);
    Named.SetAsString(VisionParam1Tag,PAnsiChar(Command));
    FIndex := cardinal(tccGetSymbolicValue);
    Named.SetAsInteger(VisionFunctionIndexTag,FIndex);
    Named.SetAsInteger(VisionRunFunctionTag,1);
    Named.SetAsInteger(VisionRunFunctionTag,0);
    Result := True;
    end;
end;

function TVLangForm.NCSetSymbolicValueAsString(Command: String): Boolean;
var
FIndex : Integer;
begin
Result := False;
if CanTalk then
    begin
    Named.SetAsInteger(VisionCommandStatusTag,0);
    Named.SetAsString(VisionParam1Tag,PAnsiChar(Command));
    FIndex := cardinal(tccSetSymbolicString);
    Named.SetAsInteger(VisionFunctionIndexTag,FIndex);
    Named.SetAsInteger(VisionRunFunctionTag,1);
    Named.SetAsInteger(VisionRunFunctionTag,0);
    Result := True;
    end;
end;

function TVLangForm.NCSetSymbolicValueAsFloat(Command: String): Boolean;
var
FIndex : Integer;
begin
Result := False;
if CanTalk then
    begin
    Named.SetAsInteger(VisionCommandStatusTag,0);
    Named.SetAsString(VisionParam1Tag,PAnsiChar(Command));
    FIndex := cardinal(tccSetSymbolicFloat);
    Named.SetAsInteger(VisionFunctionIndexTag,FIndex);
    Named.SetAsInteger(VisionRunFunctionTag,1);
    Named.SetAsInteger(VisionRunFunctionTag,0);
    Result := True;
    end;
end;

function TVLangForm.NCSetSymbolicValueAsInteger(Command: String): Boolean;
var
FIndex : Integer;
begin
Result := False;
if CanTalk then
    begin
    Named.SetAsInteger(VisionCommandStatusTag,0);
    Named.SetAsString(VisionParam1Tag,PAnsiChar(Command));
    FIndex := cardinal(tccSetSymbolicInteger);
    Named.SetAsInteger(VisionFunctionIndexTag,FIndex);
    Named.SetAsInteger(VisionRunFunctionTag,1);
    Named.SetAsInteger(VisionRunFunctionTag,0);
    Result := True;
    end;
end;

function TVLangForm.NCResetandRunJob():Boolean;
var
FIndex : Integer;
begin
Result := False;
if CanTalk then
    begin
    Named.SetAsInteger(VisionCommandStatusTag,0);
    FIndex := cardinal(tccResetandRunJob);
    Named.SetAsInteger(VisionFunctionIndexTag,FIndex);
    Named.SetAsInteger(VisionRunFunctionTag,1);
    Named.SetAsInteger(VisionRunFunctionTag,0);
    Result := True;
    end;
end;



function TVLangForm.NCCheckJobPass: Boolean;
var
FIndex : Integer;
begin
Result := False;
if CanTalk then
    begin
    Named.SetAsInteger(VisionCommandStatusTag,0);
    FIndex := cardinal(tccCheckJobPass);
    Named.SetAsInteger(VisionFunctionIndexTag,FIndex);
    Named.SetAsInteger(VisionRunFunctionTag,1);
    Named.SetAsInteger(VisionRunFunctionTag,0);
    Result := True;
    end;
end;


//**************************************************************
// NCCommand scripts
//**************************************************************
{ TSetOffset }

function TSetOffset.Name: WideString;
begin
Result := 'SetOffset';
end;

function TSetOffset.ParameterCount: Integer;
begin
Result := 2;
end;

procedure TSetOffset.Reset;
begin
  inherited;

end;

function TSetOffset.ReturnType: Integer;
begin
  Result:= dtString;
end;

function TSetOffset.ParameterType(Index: Integer): Integer;
begin
  Result:= dtString;
end;

function TSetOffset.IsFunction: Boolean;
begin
Result := False;
end;

function TSetOffset.ParameterName(Index: Integer): WideString;
begin

end;

procedure TSetOffset.Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer);
var
Buffer  : array[0..256] of Char;
P1 ,P2 : Double;
begin
  inherited;
  Named.GetAsString(Params[0], Buffer, 256);
  P1 := StrToFloatDef(Buffer,0.0);
  Named.GetAsString(Params[1], Buffer, 256);
  P2 := StrToFloatDef(Buffer,0);
  VLangForm.NCSetJobOffset(P1,P2);
end;

function TSetOffset.ParameterPassing(Index: Integer): Integer;
begin
Result := 0;
end;

{ TSetScale }

function TSetScale.Name: WideString;
begin
Result := 'SetScale';
end;

function TSetScale.ParameterCount: Integer;
begin
Result := 1;
end;

procedure TSetScale.Reset;
begin
  inherited;

end;

function TSetScale.ReturnType: Integer;
begin
  Result:= dtString;
end;

function TSetScale.ParameterType(Index: Integer): Integer;
begin
  Result:= dtString;
end;

function TSetScale.IsFunction: Boolean;
begin
Result := False;
end;

function TSetScale.ParameterName(Index: Integer): WideString;
begin

end;

procedure TSetScale.Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer);
var
Buffer  : array[0..256] of Char;
P1  : Double;
begin
  inherited;
  Named.GetAsString(Params[0], Buffer, 256);
  P1 := StrToFloatDef(Buffer,0.0);

  VLangForm.NCSetJobScaling(P1);

end;

function TSetScale.ParameterPassing(Index: Integer): Integer;
begin
Result := 0;
end;


{ TSendNativeCommand }

function TSendNativeCommand.Name: WideString;
begin
Result := 'SendNativeCommand'
end;

function TSendNativeCommand.ParameterCount: Integer;
begin
//P1 = command to execute as a string
Result := 1;
end;

procedure TSendNativeCommand.Reset;
begin
  inherited;

end;

function TSendNativeCommand.ReturnType: Integer;
begin
Result := dtString;
end;

function TSendNativeCommand.ParameterType(Index: Integer): Integer;
begin
  Result:= dtString;
end;

function TSendNativeCommand.IsFunction: Boolean;
begin
Result := True;
end;

function TSendNativeCommand.ParameterName(Index: Integer): WideString;
begin

end;

procedure TSendNativeCommand.Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer);
var
Buffer  : array[0..256] of Char;
PStr : String;
Command : String;
begin
  inherited;
  Named.GetAsString(Params[0], Buffer, 256);
  Command := Buffer;
  VLangForm.NCSendNativeCommand(Command);
end;

function TSendNativeCommand.ParameterPassing(Index: Integer): Integer;
begin
Result := 0;
end;





constructor TVLangForm.Create(AOwner: TComponent);
begin
inherited Create(Application);
VLangForm := Self;
end;

{ TGetSpreadsheetValue }

function TGetSpreadsheetValue.Name: WideString;
begin
Result := 'GetSpreadsheetValue';
end;

function TGetSpreadsheetValue.ParameterCount: Integer;
begin
Result := 2;
end;

procedure TGetSpreadsheetValue.Reset;
begin
  inherited;

end;

function TGetSpreadsheetValue.ReturnType: Integer;
begin
  Result:= dtString;
end;

function TGetSpreadsheetValue.ParameterType(Index: Integer): Integer;
begin
  Result:= dtString;
end;

function TGetSpreadsheetValue.IsFunction: Boolean;
begin
Result := True;
end;

function TGetSpreadsheetValue.ParameterName(Index: Integer): WideString;
begin

end;

procedure TGetSpreadsheetValue.Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer);
var
Buffer  : array[0..256] of Char;
P1 : String;
P2 : String;
Command : String;
WaitForResult : Boolean;
begin
  inherited;
  Named.GetAsString(Params[0], Buffer, 256);
  P1 := Buffer;
  Named.GetAsString(Params[1], Buffer, 256);
  P2 := Buffer;
  Command := Format('GV%s%s',[P1,P2]);
  VLangForm.NCCommandHasTimedOut := False;
  VLangForm.NCGetSpreadsheetValue(Command);
end;

function TGetSpreadsheetValue.ParameterPassing(Index: Integer): Integer;
begin
Result := 0;
end;

{ TGetSymbolicValue }

function TGetSymbolicValue.Name: WideString;
begin
Result := 'GetSymbolicValue';
end;

function TGetSymbolicValue.ParameterCount: Integer;
begin
Result := 1;
end;

procedure TGetSymbolicValue.Reset;
begin
  inherited;

end;

function TGetSymbolicValue.ReturnType: Integer;
begin
Result := dtString;
end;

function TGetSymbolicValue.ParameterType(Index: Integer): Integer;
begin
Result := 0;
end;

function TGetSymbolicValue.IsFunction: Boolean;
begin
Result := True;
end;

function TGetSymbolicValue.ParameterName(Index: Integer): WideString;
begin

end;

procedure TGetSymbolicValue.Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer);
var
Buffer  : array[0..256] of Char;
P1 : String;
Command : String;
WaitForResult : Boolean;
begin
  inherited;
  Named.GetAsString(Params[0], Buffer, 256);
  P1 := Buffer;
  Command := Format('GV%s',[Buffer]);
  VLangForm.NCCommandHasTimedOut := False;
  VLangForm.NCGetSymbolicValue(Command); // this waitsFor Response

  Named.GetAsString(VLangForm.VisionResponseTag,Buffer,200);
  Named.SetAsString(Res,Buffer);
end;

function TGetSymbolicValue.ParameterPassing(Index: Integer): Integer;
begin
Result := 0;
end;


{ TSetSymbolicInteger }

function TSetSymbolicInteger.Name: WideString;
begin
Result := 'SetSymbolicInteger';
end;

function TSetSymbolicInteger.ParameterCount: Integer;
begin
Result := 2;
end;

procedure TSetSymbolicInteger.Reset;
begin
  inherited;

end;

function TSetSymbolicInteger.ReturnType: Integer;
begin
Result := dtString;
end;

function TSetSymbolicInteger.ParameterType(Index: Integer): Integer;
begin
Result := dtString;
end;

function TSetSymbolicInteger.IsFunction: Boolean;
begin
Result := False;
end;

function TSetSymbolicInteger.ParameterName(Index: Integer): WideString;
begin

end;

procedure TSetSymbolicInteger.Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer);
var
Buffer  : array[0..256] of Char;
P1 : String;
P2 : Integer;
Command : String;
WaitForResult : Boolean;
begin
  inherited;
  Named.GetAsString(Params[0], Buffer, 256);
  P1 := Buffer;
  Named.GetAsString(Params[1], Buffer, 256);
  P2 := StrToIntDef(Buffer,0);
  Command := Format('SI%s %d',[P1,P2]);
  VLangForm.NCCommandHasTimedOut := False;
//  VLangForm.NCCommand := True;
  VLangForm.NCSetSymbolicValueAsInteger(Command);
end;


function TSetSymbolicInteger.ParameterPassing(Index: Integer): Integer;
begin
Result := 0;
end;

{ TSetSymbolicFloat }

function TSetSymbolicFloat.Name: WideString;
begin
Result := 'SetSymbolicFloat';
end;

function TSetSymbolicFloat.ParameterCount: Integer;
begin
Result := 2;
end;

procedure TSetSymbolicFloat.Reset;
begin
  inherited;

end;

function TSetSymbolicFloat.ReturnType: Integer;
begin
Result := dtInteger;
end;

function TSetSymbolicFloat.ParameterType(Index: Integer): Integer;
begin
Result := dtString;
end;

function TSetSymbolicFloat.IsFunction: Boolean;
begin
Result := False;
end;

function TSetSymbolicFloat.ParameterName(Index: Integer): WideString;
begin

end;

procedure TSetSymbolicFloat.Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer);
var
Buffer  : array[0..256] of Char;
P1 : String;
P2 : Single;
Command : String;
WaitForResult : Boolean;
begin
  inherited;
  Named.GetAsString(Params[0], Buffer, 256);
  P1 := Buffer;
  Named.GetAsString(Params[1], Buffer, 256);
  P2 := StrToFloatDef(Buffer,0.0);
  Command := Format('SF%s %7.4f',[P1,P2]);
  VLangForm.NCCommandHasTimedOut := False;
//  VLangForm.NCCommand := True;
  VLangForm.NCSetSymbolicValueAsFloat(Command);
end;

function TSetSymbolicFloat.ParameterPassing(Index: Integer): Integer;
begin
Result := 0;
end;

{ TLoadJobCalled }

function TLoadJobCalled.Name: WideString;
begin
Result := 'LoadJob';
end;

function TLoadJobCalled.ParameterCount: Integer;
begin
Result := 3;
end;

procedure TLoadJobCalled.Reset;
begin
  inherited;

end;

function TLoadJobCalled.ReturnType: Integer;
begin
Result := dtString;
end;

function TLoadJobCalled.ParameterType(Index: Integer): Integer;
begin
Result := dtString;
end;

function TLoadJobCalled.IsFunction: Boolean;
begin
Result := True;
end;

function TLoadJobCalled.ParameterName(Index: Integer): WideString;
begin

end;

procedure TLoadJobCalled.Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer);
var
Buffer  : array[0..256] of Char;
P1 : String;
P2,P3 : Integer;
WaitForResult : Boolean;
begin
  inherited;
  Named.GetAsString(Params[0], Buffer, 256);
  P1 := Buffer;
  Named.GetAsString(Params[1], Buffer, 256);
  P2 := StrToIntDef(Buffer,0);
  Named.GetAsString(Params[2], Buffer, 256);
  P3 := StrToIntDef(Buffer,0);
  VLangForm.NCCommandHasTimedOut := False;
  VLangForm.NCLoadJobCalled(P1,P2,P3);
end;

function TLoadJobCalled.ParameterPassing(Index: Integer): Integer;
begin
Result := 0;
end;




procedure TVLangForm.Shutdown;
begin
  inherited;

end;

{ TGetCurrentJobName }
function TGetCurrentJobName.Name: WideString;
begin
Result := 'GetCurrentJobName'
end;

function TGetCurrentJobName.ParameterCount: Integer;
begin
Result := 0;
end;

procedure TGetCurrentJobName.Reset;
begin
  inherited;

end;

function TGetCurrentJobName.ReturnType: Integer;
begin
Result := dtString;
end;

function TGetCurrentJobName.ParameterType(Index: Integer): Integer;
begin
Result := dtString;
end;

function TGetCurrentJobName.IsFunction: Boolean;
begin
Result := True;
end;

function TGetCurrentJobName.ParameterName(Index: Integer): WideString;
begin

end;

procedure TGetCurrentJobName.Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer);
begin
  inherited;
  VLangForm.NCCommandHasTimedOut := False;
//  VLangForm.NCCommand := True;
  VLangForm.NCGetCurrentJobName;
end;

function TGetCurrentJobName.ParameterPassing(Index: Integer): Integer;
begin
Result := 0;
end;


{ TSetLiveVideo }

function TSetLiveVideo.Name: WideString;
begin
Result := 'SetLiveVideo'
end;

function TSetLiveVideo.ParameterCount: Integer;
begin
Result := 1;
end;

procedure TSetLiveVideo.Reset;
begin
  inherited;
end;

function TSetLiveVideo.ReturnType: Integer;
begin
Result := dtString;
end;

function TSetLiveVideo.ParameterType(Index: Integer): Integer;
begin
Result := dtString;
end;

function TSetLiveVideo.IsFunction: Boolean;
begin
Result := True;
end;

function TSetLiveVideo.ParameterName(Index: Integer): WideString;
begin

end;

procedure TSetLiveVideo.Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer);
var
Buffer  : array[0..256] of Char;
PStr : String;
Command : String;
WaitForResult : Boolean;
PNum : INteger;
IntResult : INteger;
begin
  inherited;
    VLangForm.NCCommandHasTimedOut := False;
    Named.GetAsString(Params[0], Buffer, 256);
    PStr := Buffer;
    if PStr = '1' then
      begin
      VLangForm.NCSetLiveVideo(True);
      end
    else
      begin
      VLangForm.NCSetLiveVideo(False);
      end
end;

function TSetLiveVideo.ParameterPassing(Index: Integer): Integer;
begin
Result := 0;
end;






{ TSetSymbolicString }

function TSetSymbolicString.Name: WideString;
begin
Result := 'SetSymbolicString';
end;

function TSetSymbolicString.ParameterCount: Integer;
begin
Result := 2;
end;

procedure TSetSymbolicString.Reset;
begin
  inherited;

end;

function TSetSymbolicString.ReturnType: Integer;
begin
Result := dtInteger;
end;

function TSetSymbolicString.ParameterType(Index: Integer): Integer;
begin
Result := dtString;
end;

function TSetSymbolicString.IsFunction: Boolean;
begin
Result := False;
end;

function TSetSymbolicString.ParameterName(Index: Integer): WideString;
begin

end;

procedure TSetSymbolicString.Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer);
var
Buffer  : array[0..256] of Char;
P1 : String;
P2 : String;
Command : String;
WaitForResult : Boolean;
begin
  inherited;
  Named.GetAsString(Params[0], Buffer, 256);
  P1 := Buffer;
  Named.GetAsString(Params[1], Buffer, 256);
  P2 := Buffer;
  Command := Format('SS%s %s',[P1,P2]);
  VLangForm.NCCommandHasTimedOut := False;
  VLangForm.NCSetSymbolicValueAsString(Command);
end;

function TSetSymbolicString.ParameterPassing(Index: Integer): Integer;
begin
Result := 0;
end;



{ TTriggerCurrentJob }

function TTriggerCurrentJob.Name: WideString;
begin
Result := 'RunJob';
end;

function TTriggerCurrentJob.ParameterCount: Integer;
begin
Result := 0;
end;

procedure TTriggerCurrentJob.Reset;
begin
  inherited;

end;

function TTriggerCurrentJob.ReturnType: Integer;
begin
Result := dtInteger;
end;

function TTriggerCurrentJob.ParameterType(Index: Integer): Integer;
begin
Result := dtInteger;
end;

function TTriggerCurrentJob.IsFunction: Boolean;
begin
Result := False;
end;

function TTriggerCurrentJob.ParameterName(Index: Integer): WideString;
begin

end;

procedure TTriggerCurrentJob.Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer);
begin
  inherited;
  VLangForm.NCCommandHasTimedOut := False;
  VLangForm.NCResetandRunJob();
end;

function TTriggerCurrentJob.ParameterPassing(Index: Integer): Integer;
begin
Result := 0;
end;

{ TCheckJobPassed }

function TCheckJobPassed.Name: WideString;
begin
Result := 'CheckJobPassed';
end;

function TCheckJobPassed.ParameterCount: Integer;
begin
Result := 0;
end;

procedure TCheckJobPassed.Reset;
begin
  inherited;

end;

function TCheckJobPassed.ReturnType: Integer;
begin
Result := dtInteger;
end;

function TCheckJobPassed.ParameterType(Index: Integer): Integer;
begin
Result := dtInteger;
end;

function TCheckJobPassed.IsFunction: Boolean;
begin
Result := False;
end;

function TCheckJobPassed.ParameterName(Index: Integer): WideString;
begin
Result := '';
end;

procedure TCheckJobPassed.Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer);
begin
  inherited;
  VLangForm.NCCommandHasTimedOut := False;
  VLangForm.NCCheckJobPass();

end;

function TCheckJobPassed.ParameterPassing(Index: Integer): Integer;
begin
Result := 0;
end;



function TVLangForm.CanTalk: Boolean;
var
IntVal : INteger;
begin
IntVal := Named.GetAsInteger(VisionCanTalkTag);
Result := IntVal>0;
end;




Initialization
  TAbstractFormPlugin.AddPlugin('VisionLanguage',TVLangForm);
  TDefaultScriptLibrary.SetName('Vision');
  TDefaultScriptLibrary.AddMethod(TSendNativeCommand);
  TDefaultScriptLibrary.AddMethod(TGetSpreadsheetValue);
  TDefaultScriptLibrary.AddMethod(TLoadJobCalled);
  TDefaultScriptLibrary.AddMethod(TGetCurrentJobName);
  TDefaultScriptLibrary.AddMethod(TSetLiveVideo);
  TDefaultScriptLibrary.AddMethod(TGetSymbolicValue);
  TDefaultScriptLibrary.AddMethod(TSetSymbolicInteger);
  TDefaultScriptLibrary.AddMethod(TSetSymbolicFloat);
  TDefaultScriptLibrary.AddMethod(TSetSymbolicString);
  TDefaultScriptLibrary.AddMethod(TTriggerCurrentJob);
  TDefaultScriptLibrary.AddMethod(TSetScale);
  TDefaultScriptLibrary.AddMethod(TSetOffset);
  TDefaultScriptLibrary.AddMethod(TSetOffset);
  TDefaultScriptLibrary.AddMethod(TCheckJobPassed);
end.
