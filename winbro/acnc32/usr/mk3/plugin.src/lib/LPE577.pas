unit LPE577;

interface

{ Pulse Output Byte Enumerations Bits }
{  I_CHB         = $01;
  I_CHA         = $02;
  I_CHI         = $03;
  I_TempOK      = $08;
  I_TempNano    = $10;
  I_SplyOK      = $20;
  I_Cutoff      = $40;
  I_Ign         = $80; }

{ Oscillator Status Byte Enumeration Bits }
{  TLPE577OscStatus =
  I_TFail       = $01;
  I_FLimit      = $02;
  I_BLimit      = $03;
  I_LowElec     = $08;
  I_SvPres      = $10;
  I_TFailed     = $20;
  I_FSpk        = $40;
  I_DacStat     = $80; }

type
  // 4.5 must be 0
  // 4.6 must be 1
  // 4.7 Spare for FPGA
  // 5.0 TFail turn power of generator (EDM Gen)
  TLPE577Status = (
    lpe577StatDCSuppOn,       //HV Logic supply healthy
    lpe577StatBit1,
    lpe577StatBit2,
    lpe577StatTempHealthy,    //HV Temperature OK
    lpe577StatBit4,
    lpe577StatNot577,
    lpe577Stat577,
    lpe577StatFPGASignal,
    lpe577StatTFail,
    lpe577StatBit9,
    lpe577StatBit10,
    lpe577StatBit11,
    lpe577StatBit12,
    lpe577StatTFailed,  // These three added 25/08/02
    lpe577StatFSpark,
    lpe577StatDAC
  );

  TLPE577Statii = set of TLPE577Status;

  TLPE577Status1 = (
    lpe577Stat1NotRESREG,
    lpe577Stat1Bit1,
    lpe577Stat1Bit2,
    lpe577Stat1Bit3,
    lpe577Stat1OverPower,
    lpe577Stat1SupplyHealthy,    //HV Output voltage OK
    lpe577Stat1Suppression,
    lpe577Stat1NotIgnition
  );
  TLPE577Statii1 = set of TLPE577Status1;

{ Control Output Byte En`umeration Bits }
  TLPE577Control = (
    Lpe577CntlFSpkReset,
    Lpe577CntlTFReset,
    Lpe577CntlSupp_RE,
    Lpe577CntlPTEna,
    Lpe577CntlPositive,
    Lpe577CntlMode,     // 477 / 577
    Lpe577CntlSuppEna,
    Lpe577CntlTrgEna
  );
  TLPE577Controls = set of TLPE577Control;

  PLPE577Out = ^TLPE577Out;
  TLPE577Out = packed record
    OnTimeH    : Byte;
    OnTimeL    : Byte;
    OffTimeH   : Byte;
    OffTimeL   : Byte;
    Control    : TLPE577Controls;
    Cutoff     : Byte;
    Peak       : Byte;
    Volts      : Byte;
    Plc        : Byte;
    Mask       : Byte;
  end;

  // DCSUPPON HV regs got a problem
  // TEMPHEALTH HV regulator temperature fault
  // 7.4 Overpower limit
  // 7.5 Supply healthy
  //

  PLPE577In = ^TLPE577In;
  TLPE577In  = packed record
    Spare      : array [0..3] of Byte;
    Status     : TLPE577Statii;
    Spare1     : Byte;
    Status1    : TLPE577Statii1;
    Spare2     : array [0..1] of Byte;
  end;

  T577Device = record
    ISize, OSize : Integer;
    Input : PLPE577In;
    Output : PLPE577Out;
  end;


implementation

end.
