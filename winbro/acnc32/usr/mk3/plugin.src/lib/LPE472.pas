unit LPE472;

interface

type
  TLPE472FlowControl = (
    lpe472Convert,
    lpe472ADC3,
    lpe472ADC2,
    lpe472ADC1,
    lpe472Enable,
    lpe472Reset,
    lpe472DumpSol,
    lpe472FlowForward
  );

  TLPE472FlowControls = set of TLPE472FlowControl;

  TByteWord = packed record
    case Byte of
      0 : ( WData : Word );
      1 : ( BData : array [0..1] of Byte );
  end;

  PTLPE472Outputs = ^TLPE472Outputs;
  TLPE472Outputs = packed record
    FlowOutputs: Byte;
    FlowControl: TLPE472FlowControls;
    DAC        : TByteWord;
  end;

  PTLPE472Inputs =  ^TLPE472Inputs;
  TLPE472Inputs = packed record
    ADC    : TByteWord;
    Status : Word;
  end;

  TLPE472Device = class(TObject)
    Input  : PTLPE472Inputs;
    Output : PTLPE472Outputs;
    ISize, OSize : Integer;
    procedure StartADC(Index : Integer);
    function Converted : Boolean;
  end;


implementation

const
  FlowADCSelect : array [0..7] of TLPE472FlowControls = (
    [],
    [lpe472ADC1],
    [lpe472ADC2],
    [lpe472ADC1, lpe472ADC2],
    [lpe472ADC3],
    [lpe472ADC1, lpe472ADC3],
    [lpe472ADC2, lpe472ADC3],
    [lpe472ADC1, lpe472ADC2, lpe472ADC3]
  );


procedure TLPE472Device.StartADC(Index : Integer);
begin
  with Output^ do begin
    if lpe472Convert in FlowControl then
      Exclude(FlowControl, lpe472Convert)
    else
      Include(FlowControl, lpe472Convert);

    FlowControl := FlowControl - FlowADCSelect[7];
    FlowControl := FlowControl + FlowADCSelect[Index mod 8];
  end;
end;

function TLPE472Device.Converted : Boolean;
begin
  Result := lpe472Convert in Output^.FlowControl;
end;


end.
