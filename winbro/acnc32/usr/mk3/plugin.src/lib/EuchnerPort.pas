unit EuchnerPort;

interface

uses
  Classes, SysUtils, Messages, Windows, NamedPlugin, INamedInterface;

const
  DLE = $10; // Data link excape
  STX = $02; // Start of Text
  NAK = $15; // Negative acknowlegment
  ETX = $03; // End of text


type
  TEuchnerPort = class(TThread)
  private
    FPort: Integer;
    FErrorState : Integer;
    FInitialized : Boolean;
    CommHandle: THandle;
    FOnRead : TNotifyEvent;
    FOnWrite : TNotifyEvent;
    FOnBecomeMaster : TNotifyEvent;
    FOnInitialize : TNotifyEvent;
    FOnDataChange : TNotifyEvent;
    FBusy : Boolean;
    ReadEvent : THandle;
    WriteEvent : THandle;
    InitEvent : THandle;
    OutputBytes : array [0..1023] of Byte;
    ReadStart : Integer;
    ReadLength : Integer;
    WriteStart : Integer;
    HeadNumber : Integer;
    FReadDataOK : Boolean;
    FWriteDataOK : Boolean;
    RxBCC, TxBCC : Byte;
    FDataList : TList; //TThreadList;
    FClearHistory : Boolean;

    procedure NotifyRead;
    procedure NotifyWrite;
    procedure NotifyBecomeMaster;
    procedure DoReadData;
    procedure DoWriteData;
    procedure DoInitialize;
    procedure SetTimeOut(Timeout : Integer);
    function ServerHandshake : Boolean;
    function ClientHandshake : Boolean;
    function ReadBytes(Start, Length : Integer) : Boolean;
    function WriteBytes(Start, Length : Integer) : Boolean;
    procedure BecomeMaster;
    function Acknowledge : Boolean;
    function WriteTelegram(Buffer : array of Byte) : Boolean;
    function ReadTelegram(var Buffer : array of Byte) : Boolean;
    function ReadByte(var B : Byte) : Boolean;
    procedure WriteByte(B : Byte);
    procedure ClearDataBuffer;
    function TransferInputData(Buffer : array of Byte) : Boolean;
  protected
    procedure Execute; override;
  public
    InputBytes : array [0..1023] of Byte;
    StatusString : string;
    constructor Create( aPort: Integer);
    procedure ReadData;
    procedure WriteData(const Data : array of Byte);
    procedure Initialize;
    procedure ClearHistory;
    property ErrorState : Integer read FErrorState;
    property OnRead : TNotifyEvent read FOnRead write FOnRead;
    property OnWrite : TNotifyEvent read FOnWrite write FOnWrite;
    property OnBecomeMaster : TNotifyEvent read FOnBecomeMaster write FOnBecomeMaster;
    property OnInitialize : TNotifyEvent read FOnInitialize write FOnInitialize;
    property OnDataChange : TNotifyEvent read FOnDataChange write FOnDataChange;
    property Busy : Boolean read FBusy;
    property ReadDataOK : Boolean read FReadDataOK;
    property WriteDataOK : Boolean read FWriteDataOK;
    property Initialized : Boolean read FInitialized;
    property DataList : TList{TThreadList} read FDataList;
  end;

implementation

const
{DCBFlags}
  fBinary = $0001;                     //binary mode, no EOF check
  fParity = $0002;                     //enable parity checking
  fOutxCtsFlow = $0004;                //CTS output flow control
  fOutxDsrFlow = $0008;                //DSR output flow control
  DTR_CONTROL_DISABLE = $0000;         //two bit fields don't translate
  DTR_CONTROL_ENABLE =  $0010;         //terribly well to pascal
  DTR_CONTROL_HANDSHAKE = $0020;
  fDsrSensitivity= $0040;              //DSR sensitivity
  fTXContinueOnXoff = $0080;           //XOFF continues Tx

  fOutX = $0100;                       //XON/XOFF out flow control
  fInX = $0200;                        //XON/XOFF in flow control
  fErrorChar = $0400;                  //enable error replacement
  fNull = $0800;                       //enable null stripping
  RTS_CONTROL_DISABLE = $0000;
  RTS_CONTROL_ENABLE = $1000;
  RTS_CONTROL_HANDSHAKE = $2000;
  RTS_CONTROL_TOGGLE = $3000;
  fAbortOnError = $4000;               //abort reads/writes on error
{error codes}
  reOverflow = 1;

const
  DCB: TDCB =
    (DCBLength: SizeOf(TDCB);
     BaudRate: CBR_9600;
     Flags: fBinary or
            RTS_CONTROL_ENABLE or
            DTR_CONTROL_ENABLE;
     wReserved: 0;
     XonLim: 0;
     XoffLim: 0;
     ByteSize: 8;
     Parity: EVENPARITY;
     StopBits: ONESTOPBIT;
     XonChar: #0;
     XoffChar: #0;
     ErrorChar: #0;
     EoFChar: #0;
     EvtChar: #0;
     wReserved1: 0);


procedure TEuchnerPort.Execute;
var
  EList : array [0..2] of THandle;
  Ev : Cardinal;
begin
  CommHandle:= CreateFile(PChar(Format('COM%d:', [FPort])),
     GENERIC_READ or GENERIC_WRITE, 0, nil, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, 0);
  if CommHandle = INVALID_HANDLE_VALUE then begin
      FErrorState := Windows.GetLastError;
      Exit;
  end;

  if not SetCommState(CommHandle, DCB) then begin
    FErrorState := Windows.GetLastError;
    Exit;
  end;
  try
    ReadEvent := CreateEvent(nil, True, False, nil);
    WriteEvent := CreateEvent(nil, True, False, nil);
    InitEvent := CreateEvent(nil, True, False, nil);
    EList[0] := ReadEvent;
    EList[1] := WriteEvent;
    EList[2] := InitEvent;

    repeat
      Ev := WaitForMultipleObjects(3, @EList, False, 1000);
      if Ev <> WAIT_TIMEOUT then begin

        FBusy := True;
        case Ev of
          0 : DoReadData;
          1 : DoWriteData;
          2 : DoInitialize;
        end;
        ResetEvent(EList[Ev]);
        FBusy := False;
      end;

      BecomeMaster;
    until Terminated;
  finally
    CloseHandle(CommHandle)
  end;
end;

procedure TEuchnerPort.SetTimeOut(Timeout : Integer);
var
  CommTimeouts: TCommTimeouts;
begin
  with CommTimeouts do begin
    ReadIntervalTimeout:= 100;
    ReadTotalTimeoutMultiplier:= 100;
    ReadTotalTimeoutConstant:= Timeout;
    WriteTotalTimeoutMultiplier:= 10;
    WriteTotalTimeoutConstant:= 100
  end;
  if not SetCommTimeouts(CommHandle, CommTimeouts) then begin
    FErrorState := Windows.GetLastError;
    Terminate;
  end;
end;

procedure TEuchnerPort.ClearHistory;
begin
  FClearHistory := True;
end;

constructor TEuchnerPort.Create( aPort: Integer);
begin
  inherited Create(True);
  FPort:= aPort;
  ReadStart := 0;
  ReadLength := 62;
  WriteStart := 0;
  HeadNumber := 1;
  Self.Resume;
  FDataList := TList.Create; //TThreadList.Create;
end;


procedure TEuchnerPort.ReadData;
begin
  if not Busy then begin
    StatusString := '';
    SetEvent(ReadEvent);
  end;
end;


procedure TEuchnerPort.WriteData(const Data : array of Byte);
var I : Integer;
begin
  if not Busy then begin
    StatusString := '';
    for I := 0 to Length(Data) do begin
      OutputBytes[I] := Data[I];
    end;
    SetEvent(WriteEvent);
  end;
end;

procedure TEuchnerPort.Initialize;
begin
  if not Busy then begin
    FBusy := True;
    SetEvent(InitEvent);
  end;
end;

procedure TEuchnerPort.BecomeMaster;
var Buffer : array [0..255] of Byte;
begin
  if FClearHistory then begin
    FClearHistory := False;
    DataList.Clear;
    OnDataChange(Self);
  end;

  SetTimeout(100);
  if ReadTelegram(Buffer) then
    if Assigned(OnBecomeMaster) then
      NotifyBecomeMaster;
//      Synchronize(NotifyBecomeMaster);
end;

function TEuchnerPort.ServerHandshake : Boolean;
var B : Byte;
begin
  SetTimeout(2000);
  WriteByte(STX);
  ReadByte(B);
  Result := B = DLE;
end;

function TEuchnerPort.ClientHandshake : Boolean;
var  B : Byte;
begin
  Result := False;
  if ReadByte(B) then begin
    if (B <> STX) then
      Exit;

    WriteByte(DLE);
    Result := True;
  end;
end;

function TEuchnerPort.WriteTelegram(Buffer : array of Byte) : Boolean;
var I : Integer;
    B : array [0..12] of Byte;
begin
  for I := 0 to 10 do begin
    B[I] := Buffer[I];
  end;

  Result := False;
  if ServerHandshake then begin
    TxBCC := 0;
    for I := 0 to Buffer[0] - 1 do begin
      B[10] := Buffer[I];
      if Buffer[I] = DLE then begin
        WriteByte(DLE);
      end;
      WriteByte(Buffer[I]);
    end;
    WriteByte(DLE);
    WriteByte(ETX);
    WriteByte(TxBCC);
    if Acknowledge then
      Result := True;
  end;
end;


procedure TEuchnerPort.ClearDataBuffer;
var List : TList;
begin
  List := DataList; //DataList.LockList;
  try
    while List.Count > 1000 do
      List.Delete(0);
  finally
//    DataList.UnlockList;
  end;

  if Assigned(OnDataChange) then
    OnDataChange(Self);
end;

function TEuchnerPort.ReadByte(var B : Byte) : Boolean;
var Written : Cardinal;
begin
  Result := ReadFile(CommHandle, B, 1, Written, nil);
  RxBCC := RxBCC xor B;
  Result := Result and (Written = 1);
  if Result then begin
    DataList.Add(Pointer(B));
    ClearDataBuffer;
  end;
end;

procedure TEuchnerPort.WriteByte(B : Byte);
var Written : Cardinal;
begin
  WriteFile(CommHandle, B, 1, Written, nil);
  TxBCC := TxBCC xor B;
  DataList.Add(Pointer(B or $100));
  ClearDataBuffer;
end;

function TEuchnerPort.ReadTelegram(var Buffer : array of Byte) : Boolean;
var B : Byte;
    I : Cardinal;
begin
  Result := False;
  if ClientHandshake then begin
    RxBCC := 0;
    ReadByte(Buffer[0]);
    I := 1;
    while I < Buffer[0] do begin
      ReadByte(Buffer[I]);
      if Buffer[I] = DLE then begin
        ReadByte(B);
        if B <> DLE then
          Exit;
      end;
      Inc(I);
    end;

    ReadByte(Buffer[I]); // ETX
    ReadByte(Buffer[I + 1]); // DLE
    B := RxBCC;
    ReadByte(Buffer[I + 2]); // BCC
    Result := B = Buffer[I + 2];
    if Result then
      WriteByte(DLE)
    else
      WriteByte(NAK);
  end;
end;

function TEuchnerPort.TransferInputData(Buffer : array of Byte) : Boolean;
var I : Integer;
begin
  Result := False;
  if (Buffer[1] = Byte('R')) and
    ((Buffer[2] = Byte('L')) or (Buffer[2] = Byte('K'))) then begin
    for I := 0 to Buffer[0] - 8 do begin
      InputBytes[Buffer[5] + I] := Buffer[7 + I];
    end;
    Result := True;
  end;
end;

function TEuchnerPort.ReadBytes(Start, Length : Integer) : Boolean;
var Buffer : array [0..32] of Byte;
    Count : Integer;
begin
  Result := False;
  Count := 0;
  Buffer[0] := 7;
  Buffer[1] := Byte('T');
  Buffer[2] := Byte('L');
  Buffer[3] := HeadNumber;
  Buffer[4] := 0;
  Buffer[5] := Start;
  Buffer[6] := Length;

  repeat
    if Count >= 6 then
      Exit;
  until WriteTelegram(Buffer);

  if ReadTelegram(Buffer) then begin
    Result := TransferInputData(Buffer);
  end;
end;

function TEuchnerPort.Acknowledge : Boolean;
var B : Byte;
begin
  Result := False;
  if ReadByte(B) then
    Result := (B = DLE);
end;

function TEuchnerPort.WriteBytes(Start, Length : Integer) : Boolean;
var Buffer : array [0..32] of Byte;
    I : Integer;
    Count : Integer;
begin
  Result := False;
  Count := 0;
  Buffer[0] := 7 + Length;
  Buffer[1] := Byte('T');
  Buffer[2] := Byte('P');
  Buffer[3] := HeadNumber;
  Buffer[4] := 0;
  Buffer[5] := Start;
  Buffer[6] := Length;
  for I := 0 to Length - 1 do begin
    Buffer[7 + I] := OutputBytes[Start + I];
  end;

  repeat
    if Count >= 6 then
      Exit;
  until WriteTelegram(Buffer);

  Result := ReadTelegram(Buffer);
  if Result then begin
    Result := Buffer[6] = 0;
  end;
end;

procedure TEuchnerPort.DoReadData;
begin
  FReadDataOK := False;
  if ReadBytes(0, 16) then
    if ReadBytes(16, 16) then
      if ReadBytes(32, 16) then
        if ReadBytes(48, 14) then
          FReadDataOK := True;
  NotifyRead;
//  Synchronize(NotifyRead);
end;


procedure TEuchnerPort.DoWriteData;
begin
  FWriteDataOK := False;
{  if WriteBytes(0, 16) then
    if WriteBytes(16, 16) then
      if WriteBytes(32, 15) then
        if WriteBytes(48, 15) then }
  if WriteBytes(52, 8) then
          FWriteDataOK := True;
  NotifyWrite;
//  Synchronize(NotifyWrite);
end;

procedure TEuchnerPort.DoInitialize;
var Buffer : array [0..255] of Byte;
begin
  FInitialized := False;
  Buffer[0] := 4;
  Buffer[1] := Byte('T');
  Buffer[2] := Byte('A');
  Buffer[3] := $FF;
  if not WriteTelegram(Buffer) then
    Exit;

  Buffer[0] := 4;
  Buffer[1] := Byte('T');
  Buffer[2] := Byte('A');
  Buffer[3] := $FF;
  if not WriteTelegram(Buffer) then
    Exit;

  Buffer[0] := 4;
  Buffer[1] := Byte('T');
  Buffer[2] := Byte('I');
  Buffer[3] := $0;
  if not WriteTelegram(Buffer) then
    Exit;

  Sleep(2000);
  Buffer[0] := 7;
  Buffer[1] := Byte('T');
  Buffer[2] := Byte('R');
  Buffer[3] := $00;
  Buffer[4] := $00;
  Buffer[5] := $00;
  Buffer[6] := $10;
  if not WriteTelegram(Buffer) then
    Exit;

  if not ReadTelegram(Buffer) then
    Exit;

  Buffer[0] := 7;
  Buffer[1] := Byte('T');
  Buffer[2] := Byte('R');
  Buffer[3] := $01;
  Buffer[4] := $00;
  Buffer[5] := $08;
  Buffer[6] := $01;
  if not WriteTelegram(Buffer) then
    Exit;

  if not ReadTelegram(Buffer) then
    Exit;


  FInitialized := True;
  if Assigned(OnInitialize) then
    OnInitialize(Self);
end;

procedure TEuchnerPort.NotifyRead;
begin
  if Assigned(FOnRead) then
    OnRead(Self);
end;

procedure TEuchnerPort.NotifyWrite;
begin
  if Assigned(FOnWrite) then
    OnWrite(Self);
end;

procedure TEuchnerPort.NotifyBecomeMaster;
begin
  if Assigned(OnBecomeMaster) then
    OnBecomeMaster(Self);
end;

end.
