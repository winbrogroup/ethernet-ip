unit LPE497;

interface

uses
  SysUtils, WinTypes, Classes,
  IniFiles, CNCTypes, Windows;

type

  Tlpe497Status = ( lpe497WaitingStart,
                    lpe497WaitingEdge,
                    lpe497Counting,
                    lpe497PrepareToStop,
                    lpe497Complete,
                    lpe497RefOverflow,
                    lpe497FlowOverFlow );

type

(*typedef struct  _GENPORT_WRITE_INPUT {
    ULONG   PortNumber;     // Port # to write to
    union   {               // Data to be output to port
        ULONG   LongData;
        USHORT  ShortData;
        UCHAR   CharData;
    };
}   GENPORT_WRITE_INPUT;
  *)

  TPortUnion = record
    case Byte of
      0 : ( I : Cardinal );
      1 : ( W : Word );
      2 : ( B : Byte );
  end;

  _LPE497_WRITE_INPUT = record
    PortNumber : Cardinal;
    Data : TPortUnion;
  end;

  TLPE497PortIO = class(TObject)
  private
    hndFile : THANDLE;
    IOCTL_497_WRITE_PORT_UCHAR : Cardinal;
    IOCTL_497_READ_PORT_UCHAR : Cardinal;
    DataBuffer : _LPE497_WRITE_INPUT;
    IoctrlResult : Boolean;
    LastError : Integer;

    function CTL_CODE(DevT, Func, Method, Access : Integer) : Integer;
  public
    constructor Create(Board : Integer);
    destructor Destroy; override;
    procedure Output(Index : Cardinal; Value : Byte);
    function  Input (Index : Cardinal) : Byte;
  end;

  TLPE497 = class(TObject)
  private
    { Private declarations }
    FStatus      : Tlpe497Status;
    FFrequency   : double;
    FBaseAddress : integer;
    FCounter     : integer;
    FTimeOut     : double;
    fDebug       : Boolean;

    FRefFrequency : double;
{    Base8255, Base8254 : integer;
    PortA8255, PortB8255, PortC8255, Control8255 : integer;
    Counter0, Counter1, Counter2, Control8254 : integer; }
    PortIO : TLPE497PortIO;
    function getStatus : Tlpe497Status;
    function getFrequency : double;

    procedure setBaseAddress(b : integer);
    procedure SetCounter(c : integer);
    procedure setRefFrequency(r : double);
    procedure setTimeOut(t : double);
    procedure SelectCounter;
    procedure ToggleLoadBit;
    procedure EnableStart;
    procedure DisableStart;
    function  StartValue : integer;
    function  StopValue  : integer;
    function  DoneValue  : integer;
    procedure setDebug(d : boolean);
    procedure debugOutput(stat : string);
    procedure Output(Index : Cardinal; val : byte);
    function  Input (Index : Cardinal) : byte;
    function  Dou2Str (n : double) : string;
  protected
  public
    stop_count   : Word;
    flow_count   : Word;
    ref_count    : Word;
    statusref, statusflow, statusstop : Word;
    SelectWas, SelectIs, Select : Byte;
    constructor Create(aBase, aCounter : Integer; aRef, aTimeout : double);
    destructor Destroy; override;
    property Status : Tlpe497Status read getStatus;
    property Frequency : double read getFrequency;
    property Debug : boolean read fdebug write setdebug;
    procedure Start;
    procedure Reset;
    procedure HitLoadBit;
    property  Port[Index : Cardinal] : byte read Input write Output;
    property BaseAddress : integer read FBaseAddress write setBaseAddress;
    property Counter : Integer read FCounter write SetCounter;
    property RefFrequency : double read FRefFrequency write setRefFrequency;
    property Timeout : double read FTimeOut write setTimeOut;
    function LastError : Integer;
  end;


implementation

const
  STAT_8254_OUTPUT = $80;
  STAT_8254_NULL   = $40;
  STAT_8254_RW1    = $20;
  STAT_8254_RW0    = $10;
  STAT_8254_M2     = $08;
  STAT_8254_M1     = $04;
  STAT_8254_M0     = $02;
  STAT_8254_BCD    = $01;

  Mode0_8254       = $0;
  WriteC0_8254     = $30;
  WriteC1_8254     = $70;
  WriteC2_8254     = $b0;

  Latch8254        = $c0;
  Count8254        = $10;
  Status8254       = $20;

  ReadCounter0     = $02;
  ReadCounter1     = $04;
  ReadCounter2     = $08;

  NbrOfCounters    = $04;
  MaxRefFrequency  = 1000000.0;
  NbrRefFreqJumpers = 8;

  PortA8255    = 0;
  PortB8255    = 1;
  PortC8255    = 2;
  Control8255  = 3;
  Counter0     = 4;
  Counter1     = 5;
  Counter2     = 6;
  Control8254  = 7;

constructor TLPE497.Create(aBase, aCounter : Integer; aRef, aTimeout : double);
begin
  inherited Create;
  SetBaseAddress(aBase); // $340
  PortIO := TLPE497PortIO.Create(aBase);
  PortIO.Input(0);
  RefFrequency := aRef; //62500.0;
  TimeOut := aTimeOut;
  Counter := aCounter;
  Port[Control8255] := $83;
end;

function TLPE497.LastError : Integer;
begin
  Result := PortIO.LastError;
end;

destructor TLPE497.Destroy;
begin
  PortIO.Free;
  inherited;
end;

function TLPE497.getStatus : Tlpe497Status;
var
  done, cstart : word;
begin
  SelectCounter;

  { Read the current reference count }
  port[Control8254] := Latch8254 + Count8254 +
                       ReadCounter0 + ReadCounter1 + ReadCounter2;
  ref_count := port[Counter1];
  ref_count := ref_count + port[Counter1] * $100;

  stop_count := port[Counter2];
  stop_count := stop_count + port[Counter2] * $100;

  flow_count := port[Counter0];
  flow_count := flow_count + port[Counter0] * $100;

  { Read the status of all counters }
  port[Control8254] := Latch8254 + Status8254 +
                    ReadCounter0 + ReadCounter1 + ReadCounter2;

  statusflow := port[Counter0];
  statusref  := port[Counter1];
  statusstop := port[Counter2];

  done  := doneValue;
  cstart := startValue;

  done := done and port[PortC8255];
  cstart := cstart and port[PortA8255];

  result := lpe497RefOverFlow;  // Default value should be reasigned

  if (done <> 0) and (cstart = 0) and (ref_count = $ffff) then begin
    result := lpe497WaitingStart;
    debugOutput('Waiting Start');
  end;

  if (done <> 0) and (cstart <> 0) and (ref_count = $ffff) then begin
    result := lpe497WaitingEdge;
    debugOutput('Waiting Edge');
  end;

  if (done = 0) and ((statusstop and STAT_8254_OUTPUT) = 0) then begin
    result := lpe497Counting;
  end;

  if (done = 0) and ((statusstop and STAT_8254_OUTPUT) <> 0) then begin
    result := lpe497PrepareToStop;
    debugOutput('Prepare To Stop');
  end;

  if (done <> 0) and (ref_count <> $ffff) then begin
    result := lpe497Complete;
    debugOutput('Complete ');
  end;

  if (statusref and STAT_8254_OUTPUT) <> 0 then begin
    result := lpe497RefOverFlow;
    debugOutput('Ref Over Flow');
  end;

  if (statusflow and STAT_8254_OUTPUT) <> 0 then begin
    result := lpe497FlowOverFlow;
    debugOutput('Flow Over Flow');
  end;

  FStatus := result;
end;

procedure TLPE497.Reset;
var
  tmp : double;
begin
  SelectCounter;
  DisableStart;

  { Set counters for write operation }
  port[Control8254] := WriteC0_8254 + Mode0_8254;
  port[Control8254] := WriteC1_8254 + Mode0_8254;
  port[Control8254] := WriteC2_8254 + Mode0_8254;

  { Write values to counters }
  Port[Counter0] := $ff;
  Port[Counter0] := $ff;
  Port[Counter1] := $ff;
  Port[Counter1] := $ff;

  tmp := TimeOut * RefFrequency;
  Port[Counter2] := Round(tmp) mod $100;
  Port[Counter2] := Round(tmp) div $100;

  Sleep(1);
  ToggleLoadBit;
  Sleep(1);
{  debugOutput('Reset ' + inttostr(Round(timeout)) + ' ' +
                       inttostr(Round(RefFrequency))); }
  ToggleLoadBit;
end;

procedure TLPE497.start;
begin
  EnableStart;
  debugOutput('Start')
end;

procedure TLPE497.hitLoadBit;
begin
  ToggleLoadBit;
end;
{ ***************************************************

  Private Functions for handling the card

  ***************************************************}
procedure TLPE497.SelectCounter;
var
  tmp : byte;
begin
  SelectWas := Port[PortA8255];
  tmp := SelectWas and $f0;
  SelectIs := tmp or Counter;
  Select := Counter;
  Port[PortA8255] := SelectIs;
  Select := Port[PortA8255];
end;

function TLPE497.DoneValue : integer;
begin
  result := 1 shl Counter;
end;

function TLPE497.StartValue : integer;
begin
  result := $10 shl Counter;
end;

function TLPE497.StopValue : integer;
begin
  case Counter of
    0 : result := $ef;
    1 : result := $df;
    2 : result := $bf;
    3 : result := $7f;
  else
    result := 0;
  end;
end;

procedure TLPE497.EnableStart;
var
  tmp : byte;
begin
  tmp := Port[PortA8255];
  tmp := tmp or StartValue;
  Port[PortA8255] := tmp;
end;

procedure TLPE497.DisableStart;
var
  tmp : byte;
begin
  tmp := Port[PortA8255];
  tmp := tmp and StopValue;
  Port[PortA8255] := tmp;
end;

procedure TLPE497.ToggleLoadBit;
var
  tmp : byte;
begin
  tmp := Port[PortC8255];
  tmp := tmp xor ($10 shl Counter);
  Port[PortC8255] := tmp;
end;

{ ************************************************

  Property handlers

  ************************************************}
procedure TLPE497.SetBaseAddress(b : integer);
begin
  FBaseAddress := b;
{  Base8255     := b;
  Base8254     := b + 4;
  PortA8255    := Base8255;
  PortB8255    := Base8255 + 1;
  PortC8255    := Base8255 + 2;
  Control8255  := Base8255 + 3;
  Counter0     := Base8254;
  Counter1     := Base8254 + 1;
  Counter2     := Base8254 + 2;
  Control8254  := Base8254 + 3; }
end;

function TLPE497.GetFrequency : double;
var
  flow, ref : word;
begin
  SelectCounter;
  flow := $ffff;
  flow := flow - port[Counter0];
  flow := flow - (port[Counter0] * $100);

  ref  := $ffff;
  ref  := ref - port[Counter1];
  ref  := ref - (port[Counter1] * $100);

  if ref = 0 then
    result := 0
  else
    result := ((flow) * RefFrequency) / (ref);

  DebugOutput('Freq ' + Dou2Str(result));
  FFrequency := result;
end;

procedure TLPE497.SetCounter(c : integer);
begin
  FCounter := c mod NbrOfCounters;
end;

procedure TLPE497.setRefFrequency(r : double);
var
  freq : double;
  i : integer;
begin
  freq := MaxRefFrequency;

  for i := 0 to NbrRefFreqJumpers do begin
    if r+0.0001 > freq then begin
      FRefFrequency := freq;
      setTimeOut(FTimeOut);
      exit;
    end;
    freq := freq / 2;
  end;

  FRefFrequency := freq;
end;

procedure TLPE497.setTimeOut(t : double);
begin
  if t > $ffff / FRefFrequency then
    t := $ffff / FRefFrequency;

  FTimeOut := t;
end;

{ ************************************************

  Debug handlers

  ************************************************}

procedure TLPE497.debugOutput(stat : string);
begin
end;

function TLPE497.Dou2Str (n : double) : string;
var
  s : string;
begin
  Str(n:9:4, s);
  result := s;
end;

procedure TLPE497.setDebug (d : boolean);
begin
end;

procedure TLPE497.Output(Index : Cardinal; val : byte);
begin
  PortIO.Output(Index, Val);
end;

function  TLPE497.Input (Index : Cardinal) : byte;
begin
  Result := PortIO.Input(index);
end;

// Create file NTDDK and move the following to it.
const METHOD_BUFFERED    = 0;
const METHOD_IN_DIRECT   = 1;
const METHOD_OUT_DIRECT  = 2;
const METHOD_NEITHER     = 3;

const FILE_ANY_ACCESS     =   0;
const FILE_SPECIAL_ACCESS =   FILE_ANY_ACCESS;
const FILE_READ_ACCESS    =   $0001;    // file & pipe
const FILE_WRITE_ACCESS   =   $0002;    // file & pipe

const LPE497_TYPE = 40000;

constructor TLPE497PortIO.Create(Board : Integer);
begin
  inherited Create;
//EXIT;

  hndFile := CreateFile( '\\.\LPE497Dev',
                         GENERIC_READ + GENERIC_WRITE,
                         FILE_SHARE_READ,
                         nil,
                         OPEN_EXISTING,
                         0,
                         0 );
  if hndFile = INVALID_HANDLE_VALUE then
    raise Exception.CreateFmt('Unable to open LPE497 [Error %d]', [GetLastError]);

  IOCTL_497_WRITE_PORT_UCHAR := CTL_CODE(LPE497_TYPE, $910, METHOD_BUFFERED, FILE_WRITE_ACCESS);
  IOCTL_497_READ_PORT_UCHAR := CTL_CODE(LPE497_TYPE, $900, METHOD_BUFFERED, FILE_READ_ACCESS);
end;

destructor TLPE497PortIO.Destroy;
begin
  if hndFile <> INVALID_HANDLE_VALUE then
    CloseHandle(hndFile);
  inherited;
end;

function TLPE497PortIO.CTL_CODE(DevT, Func, Method, Access : Integer) : Integer;
begin
//#define CTL_CODE( DeviceType, Function, Method, Access ) (                 \
//    ((DeviceType) << 16) | ((Access) << 14) | ((Function) << 2) | (Method) \
  Result := (DevT shl 16) or (Access shl 14) or (Func shl 2) or Method;
end;

var T : Double;

procedure Delay(C : Integer);
var I : Integer;
begin
  T := 10.1;

  for I := 0 to C do begin
    T := T + 1;
    T := Sqrt(T);
    T := T * T;
    T := T - 1;
  end;
end;

procedure TLPE497PortIO.Output(Index : Cardinal; Value : byte);
var ReturnedLength : DWord;
begin
//Exit;
    DataBuffer.PortNumber := Index;
    DataBuffer.Data.B := Value;

         IoctrlResult := DeviceIoControl(
                            hndFile,                   // Handle to device
                            IOCTL_497_WRITE_PORT_UCHAR, // IO Control code for Read
                            @DataBuffer,                    // Buffer to driver.
                            Sizeof(DataBuffer.PortNumber) + Sizeof(DataBuffer.Data.B), // Length of buffer in bytes.
                            nil,        // Buffer from driver.
                            0,         // Length of buffer in bytes.
                            ReturnedLength,    // Bytes placed in DataBuffer.
                            nil                // NULL means wait till op. completes.
                            );
    if not IoctrlResult then
      LastError := GetLastError;
//    Sleep(1);
//    Delay(100);
end;

function TLPE497PortIO.Input(Index : Cardinal) : Byte;
var ReturnedLength : DWord;
begin
//Exit;
    IoctrlResult := DeviceIoControl(
                            hndFile,                   // Handle to device
                            IOCTL_497_READ_PORT_UCHAR, // IO Control code for Read
                            @Index,                    // Buffer to driver.
                            sizeof(Index), // Length of buffer in bytes.
                            @DataBuffer.Data,        // Buffer from driver.
                            SizeOf(DataBuffer.Data.I),         // Length of buffer in bytes.
                            ReturnedLength,    // Bytes placed in DataBuffer.
                            nil                // NULL means wait till op. completes.
                            );
    if not IoctrlResult then
      LastError := GetLastError;
    Result := Databuffer.Data.B;

//    Sleep(1);
//    Delay(100);
end;


end.
