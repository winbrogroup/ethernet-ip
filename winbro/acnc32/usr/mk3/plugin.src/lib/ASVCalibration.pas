unit ASVCalibration;

interface

uses SysUtils, Classes, IniFiles, NamedPlugin;

type
  TCalibItem = class
  private
    Programmed: Double;
    Actual: Double;
  public
    constructor Create; overload;
    constructor Create(P, A: Double); overload;
    function Deviation: Double;
    function ToString: string;
  end;

  TASVCalibration = class
  private
    List: TList;
    function GetPoint(Index: Integer): TCalibItem;
    procedure AddPoint(Item: TCalibItem);
    procedure ClearPoints;
  public
    constructor Create;
    destructor Destroy; override;
    procedure Load(AFilename: string);
    function Voltage(Programmed: Double): Double;
  end;

implementation

{ TAFCCalibration }

procedure TASVCalibration.Load(AFilename: string);
var Ini: TIniFile;
    Points: Integer;
    Cal: TCalibItem;
    I: Integer;
begin
  ClearPoints;
  Ini:= TIniFile.Create(AFilename);
  try
    Points:= Ini.ReadInteger('global', 'points', 0);
    for I:= 1 to Points do begin
      Cal:= TCalibItem.Create;
      Cal.Programmed:= Ini.ReadFloat('programmed', IntToStr(I), 0);
      Cal.Actual:= Ini.ReadFloat('actual', IntToStr(I), 0);
      Named.EventLog('Loading point: ' + Cal.ToString);
      AddPoint(Cal);
    end;
  finally
    Ini.Free;
  end;
end;

constructor TASVCalibration.Create;
begin
  List:= TList.Create;
end;

procedure TASVCalibration.AddPoint(Item: TCalibItem);
var I: Integer;
begin
  for I:= 0 to List.Count - 1 do begin
    if GetPoint(I).Programmed > Item.Programmed then begin
      List.Insert(I, Item);
      Exit;
    end;
  end;

  List.Add(Item);
end;

{ Deviation is subtracted as it is the measured deviation }
function TASVCalibration.Voltage(Programmed: Double): Double;
var I: Integer;
    LastDev: Double;
    LastPro: Double;
    Cal: TCalibItem;
    Slope: Double;
begin
  LastDev:= 0;
  LastPro:= 0;

  for I:= 0 to List.Count - 1 do begin
    Cal:= GetPoint(I);
    if Cal.Programmed > Programmed then begin
      // Find the deviation based on Last and this
      Slope:= (Cal.Deviation - LastDev) / (Cal.Programmed - LastPro);
      Result:= (Programmed - LastPro) * Slope + LastDev;
      Result:= Programmed - Result;
      Exit;
    end;
    LastDev:= Cal.Deviation;
    LastPro:= Cal.Programmed;
  end;
  Result:= Programmed - LastDev;
end;

procedure TASVCalibration.ClearPoints;
begin
  while List.Count > 0 do begin
    TObject(List[0]).Free;
    List.Delete(0);
  end;
end;

destructor TASVCalibration.Destroy;
begin
  ClearPoints;
  List.Free;
  inherited;
end;

{ TCalibItem }

constructor TCalibItem.Create(P, A: Double);
begin
  inherited Create;
  Programmed:= P;
  Actual:= A;
end;

constructor TCalibItem.Create;
begin
  inherited;

end;

function TASVCalibration.GetPoint(Index: Integer): TCalibItem;
begin
  Result:= TCalibItem(List[Index]);
end;

function TCalibItem.Deviation: Double;
begin
  Result:= Actual - Programmed;
end;

function TCalibItem.ToString: string;
begin
  Result:= 'Programmed=' + FloatToStr(Programmed) + ' - Actual=' + FloatToStr(Actual);
end;

end.
