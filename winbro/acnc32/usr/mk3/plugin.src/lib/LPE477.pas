unit LPE477;

interface

const
{ Control Output Byte Enumeration Bits }
  O_FSpkReset   = $01;
  O_TFReset     = $02;
  O_LH          = $03;
  O_PTEna       = $08;
  O_Positive    = $10;
  O_Srv         = $20;
  O_CoffEna     = $40;
  O_TrgEna      = $80;

{ Pulse Output Byte Enumerations Bits }
  I_CHB         = $01;
  I_CHA         = $02;
  I_CHI         = $03;
  I_TempOK      = $08;
  I_TempNano    = $10;
  I_SplyOK      = $20;
  I_Cutoff      = $40;
  I_Ign         = $80;

{ Oscillator Status Byte Enumeration Bits }
  I_TFail       = $01;
  I_FLimit      = $02;
  I_BLimit      = $03;
  I_LowElec     = $08;
  I_SvPres      = $10;
  I_TFailed     = $20;
  I_FSpk        = $40;
  I_DacStat     = $80;

  Opt0PositiveBit= $10;
  Opt1PositiveBit= $40;

type
  TLPE477ControlBit = (
    lpe477cntlFSpkReset,
    lpe477cntlTFReset,
    lpe477cntl_LH,
    lpe477cntlPTEna,
    lpe477cntlPositive,
    lpe477cntlSrv,
    lpe477cntlCoffEna,
    lpe477cntlTrgEna
  );
  TLPE477ControlBits = set of TLPE477ControlBit;

  TLPE477PulseBit = (
     lep477PulseNotRESREG,
     lpe477pulseCHB,
     lpe477pulseCHA,
     lpe477pulseCHI,
     lpe477pulseTempOK,
     lpe477pulseSplyOK,
     lpe477pulseCutoff,
     lpe477pulseNotIgn
  );
  TLPE477PulseBits = set of TLPE477PulseBit;

  TLPE477OscStatusBit = (
    lpe477oscTFail,
    lpe477oscFLimit,
    lpe477oscBLimit,
    lpe477oscLowElec,
    lpe477oscSvPres,
    lpe477oscTFailed,
    lpe477oscFSpk,
    lpe477oscDacStat
  );
  TLPE477OscStatusBits = set of TLPE477OscStatusBit;

  PLPE477Out = ^TLPE477Out;
  TLPE477Out = packed record
    OnTimeH    : Byte;
    OnTimeL    : Byte;
    OffTimeH   : Byte;
    OffTimeL   : Byte;
    Control    : TLPE477ControlBits;
    Cutoff     : Byte;
    Peak       : Byte;
    Volts      : Byte;
    Plc        : Byte;
    Mask       : Byte;
  end;


  PLPE477In = ^TLPE477In;
  TLPE477In  = packed record
    spare      : array [0..3] of byte;
    spare2: Byte; // Added 18-04-06
    RecOscStat    : TLPE477OscStatusBits;
    RecEncoder    : Byte;
    RecpulseStat  : TLPE477PulseBits;
    Spare1     : array [0..1] of Byte;
  end;

  T477Device = record
    ISize, OSize : Integer;
    Input : PLPE477In;
    Output : PLPE477Out;
  end;

implementation

end.
