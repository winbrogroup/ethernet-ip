unit PmacServo;

interface

type
  TPmacEDMHeadCommand = (
    pehcServo,
    pehcDCM,
    pehcHome,
    pehcJogPlus,
    pehcJogMinus,
    pehcDrive,
    pehcReset,
    pehcFreeze,
    pehcStoreFirstSpark,
    pehcLastFirstSpark,
    pehcForceFirstSpark,
    pehcFromFirstSpark,
    pehcFromDepth,
    pehcRemoveOffset,
    pehcRapidRecovery,
    pehcReverseDrilling
  );

  TPmacEDMHeadCommands = set of TPmacEDMHeadCommand;

  TPmacEDMHeadStatus = (
    pehsFinalDepth,
    pehsFirstSpark,
    pehsRetracted,
    pehsDCM,
    pehsPositiveLimit,
    pehsNegativeLimit,
    pehsHomed,
    pehsOpenLoop,
    pehsEDMOffRequest,
    pehsOffsetPresent,
    pehsRetractError
  );

  TPmacEDMHeadStatii = set of TPmacEDMHeadStatus;


  PPmacPlcOut = ^TPmacPlcOut;
  TPmacPlcOut = packed record
    FinalDepth : Integer;           // 0 start byte
    Gap : Word;                     // 4
    Servo : Word;                   // 6
    VoltageScaling : Byte;          // 8
    ForwardThreshold : Byte;        // 9
    ReverseThreshhold : Byte;       // 10
    ShortVelocity : Byte;           // 11
    ShortInterval : Byte;           // 12
    Spindle : Byte;                 // 13
    Command : TPmacEDMHeadCommands; // 14
    Standoff : Integer;             // 16
    RRDepth: Word;                  // 20
    RRServo: Byte;                  // 22
    HP: Byte;                       // 23
  end;

  PPmacPlcIn = ^TPmacPlcIn;
  TPmacPlcIn = packed record
    Position : Integer;             // 0
    HIC : Integer;                  // 4
    SpareI1 : Integer;              // 8
    SLIC : Word;                    // 12
    Velocity : Word;                // 14
    GapVolts : Byte;                // 16
    GapCurrent : Byte;              // 17
    Status : TPmacEdmHeadStatii;    // 18
    MaxPosition : Integer;          // 20..23
  end;

  TPmacDevice = record
    ISize, OSize : Integer;
    Input : PPmacPlcIn;
    Output : PPmacPlcOut;
  end;

implementation

end.
