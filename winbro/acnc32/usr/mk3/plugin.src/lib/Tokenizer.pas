unit Tokenizer;

interface

uses
  Windows, Messages, SysUtils, Classes;

type
  EendOfFile = class(exception);
  ETokenizerError = class(Exception);


  TStringTokenizer = class (TStringList)
  private
    Fseperator   : string;
    FFileName    : string;
    Work         : string;
    FLine        : Integer;
    prevline     : Integer;
    prevpos      : Integer;
    tokenLine    : Integer;
    FtokenPos    : Integer;
    FEndOfStream : Boolean;
    function GetCurrentLine : string;
    function Whitespace : boolean;
    function SeperatorChar : boolean;
    function Index : Boolean;
    procedure AddChar;
    procedure SetLine(aLine : Integer);
  protected
    FPosition    : Integer;
    Working      : string;
  public
    function Peek : Char;
    function  FindToken(aToken : string) : Boolean; virtual;
    function Read : string; virtual;
    function ReadChar : Char;
    procedure Push;
    procedure Rewind;
    function  ReadLine : string;  // Reads the rest of the line as is.
    procedure ExpectedToken(aToken : string);
    procedure ExpectedTokens(const Args : array of string);
    function ReadInteger : Integer;
    function ReadBoolean : Boolean;
    function ReadIntegerAssign : Integer;
    function ReadStringAssign : string;
    function ReadBooleanAssign : Boolean;

    function ReadIndex(Max : Integer) : Integer;
    procedure ReplaceToken(newToken : string);
    procedure LoadFromFile(const aFileName : string); override;
    procedure SaveToFile(const aFileName : string); override;
    property Seperator: string read FSeperator write FSeperator;
    property EndOfStream : boolean read FEndOfStream;
    property CurrentLine : string read GetCurrentLine;
    property Line : Integer read FLine write SetLine;
    property TokenPos : Integer read FTokenPos;
    property LastToken : string read Working;
    property Position : Integer read FPosition;
    property FileName : string read FFileName write FFilename;
  end;

  TQuoteTokenizer = class(TStringTokenizer)
  private
    FQuoteChar : Char;
    procedure SetQuoteChar(aChar : Char);
  public
    function Read : string; override;
    property QuoteChar : Char read FQuoteChar write SetQuoteChar;
  end;

implementation

function  TStringTokenizer.FindToken(aToken : string) : Boolean;
begin
  Result := True;
  while not FEndOfStream do
    if AnsiCompareText(Read, aToken) = 0 then Exit;
  Result := False;
end;

procedure TStringTokenizer.SetLine(aLine : Integer);
begin
  Rewind;
  FLine := aLine;
end;


function TStringTokenizer.Read : string;
begin
  Working := '';
  Result  := Working;

  if Count = 0 then
    FEndOfStream := True;

  if FEndOfStream then
    Exit;
  try
    while whitespace do
      if Index then Exit;

// This is the start of a token
    FTokenPos   := Position;
    tokenLine  := FLine;

    prevLine := FLine;
    prevPos  := position;

    if SeperatorChar then begin
      AddChar;
      if Index then Exit;
      Result := Working;
      Exit;
    end;

    while (not whitespace) and (not seperatorChar) do begin
      AddChar;
      if Index then Exit;
    end;

  finally
    Result := Working;
  end;
end;

function TStringTokenizer.ReadChar : Char;
begin
  while Position > Length(CurrentLine) do begin
    FPosition := 1;
    FLine := FLine + 1;
    if FLine > Count then begin
      FEndOfStream := True;
      Result := ' ';
      Exit;
    end;
  end;

  Working := '';
  AddChar;
  Index;
  if Length(Working) > 0 then
    Result := Working[1]
  else
    Result := ' ';
end;

function TStringTokenizer.Peek : Char;
begin
  if (Line >= Count) or (Position > Length(CurrentLine)) then
    Result := #0
  else
    Result := CurrentLine[position];
end;

procedure TStringTokenizer.Push;
begin
  if FendOfStream then exit;
  Fline     := prevline;
  FPosition := prevpos;
end;

procedure TStringTokenizer.LoadFromFile(const aFileName : string);
begin
  inherited LoadFromFile(aFileName);
  FFileName := aFileName;
end;

procedure TStringTokenizer.SaveToFile(const aFileName : string);
begin
  inherited SaveToFile(aFileName);
  FFileName := aFileName;
end;

function TStringTokenizer.GetCurrentLine : string;
begin
  Result := Strings[FLine];
end;

procedure TStringTokenizer.Rewind;
begin
  FEndOfStream := false;
  Fline     := 0;
  FPosition := 1;
  PrevLine  := 0;
  Prevpos   := 1;
  TokenLine := 0;
  FTokenPos := 1;
end;

// Reads the rest of the line as is.
function  TStringTokenizer.Readline : string;
begin
  Result := '';

  if FendOfStream then
    Exit;

  prevline := Fline;
  prevpos  := position;

  Result := Copy(CurrentLine, Position, Length(CurrentLine));

  { Force an automated next line }
  FPosition := Length(CurrentLine) + 2;
{  try
    Index;
  except
    // Silent exception has added end of file
    Exit;
  end;   }
end;

procedure TStringTokenizer.ReplaceToken(newToken : string);
begin
  work := Copy(CurrentLine, 1, TokenPos - 1);
  work := work + newToken;
  FPosition := Length(Work);
  work := work + Copy(CurrentLine, TokenPos + Length(Working), Length(CurrentLine));
  Strings[FLine] := Work;
end;

function TStringTokenizer.Index;
begin
  FPosition := Position + 1;
  if Position > Length(CurrentLine) + 1 then begin
    FPosition := 1;
    FLine := FLine + 1;

    if FLine >= Count then begin
      FEndOfStream := true;
    end;
  end;
  Result := EndOfStream;
end;

function TStringTokenizer.Whitespace : Boolean;
begin
  result := true;
  if position > length(CurrentLine) then begin
    Result := true;
    Exit
  end;

  case CurrentLine[Position] of
    '!'..'~' : Result := False;
  end;
end;

function TStringTokenizer.SeperatorChar : Boolean;
var
  I : Integer;
begin
  for I := 1 to Length(Seperator) do begin
    if Seperator[i] = CurrentLine[position] then begin
      Result := True;
      Exit;
    end;
  end;
  Result := False;
end;

procedure TStringTokenizer.ExpectedToken(aToken : string);
begin
  if Read <> aToken then
    raise Exception.CreateFmt('Unexpected token [%s] should be [%s]', [LastToken, aToken]);
end;

procedure TStringTokenizer.ExpectedTokens(const Args : array of string);
var I : Integer;
begin
  for I := 0 to High(Args) do begin
    ExpectedToken(Args[I]);
  end;
end;

function TStringTokenizer.ReadInteger : Integer;
begin
  Result := StrToInt(Read);
end;

function TStringTokenizer.ReadIntegerAssign : Integer;
begin
  ExpectedToken('=');
  Result := ReadInteger;
end;

function TStringTokenizer.ReadBoolean : Boolean;
begin
  try
    Result := Round(StrToFloat(Self.Read)) <> 0;
  except
    if lowercase(LastToken) = 'true' then
      Result := True
    else
      Result := False;
  end;
end;


function TStringTokenizer.ReadBooleanAssign : Boolean;
begin
  ExpectedToken('=');
  Result := ReadBoolean;
end;

function TStringTokenizer.ReadStringAssign : string;
begin
  ExpectedToken('=');
  Result := Read;
end;

function TStringTokenizer.ReadIndex(Max : Integer) : Integer;
begin
  ExpectedToken('[');
  Result := ReadInteger mod Max;
  ExpectedToken(']');
end;

procedure TStringTokenizer.AddChar;
begin
  Working := Working + CurrentLine[position];
end;




procedure TQuoteTokenizer.SetQuoteChar(aChar : Char);
begin
  if Seperator[Length(Seperator)] = QuoteChar then
    FSeperator[Length(Seperator)] := aChar
  else
    Seperator := Seperator + aChar;

  FQuoteChar := aChar;
end;

function TQuoteTokenizer.Read : string;
begin
  Working := '';
  Result  := Working;

  if Count = 0 then
    FEndOfStream := True;

  if FEndOfStream then Exit;
  try
    while whitespace do
      if Index then Exit;

// This is the start of a token
    FTokenPos   := Position;
    tokenLine  := FLine;

    prevLine := FLine;
    prevPos  := position;

    if SeperatorChar then begin
      if CurrentLine[Position] = QuoteChar then begin
        Index;
        while CurrentLine[Position] <> QuoteChar do begin
          AddChar;
          Index;
        end;
        Index;
        Exit;
      end;

      AddChar;
      if Index then Exit;
      Result := Working;
      Exit;
    end;

    while (not whitespace) and (not seperatorChar) do begin
      AddChar;
      if Index then Exit;
    end;

  finally
    Result := Working;
  end;
end;

end.
