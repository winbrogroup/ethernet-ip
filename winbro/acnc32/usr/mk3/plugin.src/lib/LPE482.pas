unit LPE482;

interface

type
  TLPE482Control = (
    lpe482CtrlSpare8,
    lpe482CtrlSpare9,
    lpe482CtrlSpare10,
    lpe482CtrlSpare11,
    lpe482ctrlCapSelect,
    lpe482ctrlBusVoltage100V,
    lpe482ctrlBusVoltage78V,
    lpe482ctrlResetIDetect,

    lpe482CtrlHeadCap0,
    lpe482CtrlHeadCap1,
    lpe482CtrlHeadCap2,
    lpe482CtrlHeadCap3,
    lpe482CtrlHeadCap4,
    lpe482CtrlSpare5,
    lpe482CtrlSpare6,
    lpe482CtrlSpare7
  );
  TLPE482Controls = set of TLPE482Control;


  TElectrodeMask = record
    case Byte of
      0 : (
        W1, W2, W3 : Word;
      );
      1 : (
        B : array [0..5] of Byte;
      );
  end;

  PLPE482In = ^TLPE482In;
  TLPE482In = packed record
    Sense : TElectrodeMask;
    Spare : array [0..1] of Byte;
  end;

  PLPE482Out = ^TLPE482Out;
  TLPE482Out = packed record
    Control : TLPE482Controls;
    Spare1 : array [0..5] of Byte;
  end;

  T482Device = record
    ISize, OSize : Integer;
    Input : PLPE482In;
    Output : PLPE482Out;
  end;

implementation

end.
