unit Matrix3D;

interface

uses Classes, Math;

type
  TXYZ = record
    X, Y, Z : Double;
  end;
  TPoint = record
    X, Y : Integer;
  end;

  TMatrix3D = class;

  TVertex3D = class(TObject) // Maybe this should be vector3d ?
  private
    procedure FindMatrix(AX, AY : TVertex3D; M : TMatrix3D);
  public
    Index : Integer; // Index into a vertex set
    X, Y, Z : Double;
    constructor Create; overload;
    constructor Create(I, J, K : Double); overload;
    constructor Create(P : TVertex3D); overload;
    procedure Clear;
    procedure Copy(P : TVertex3D);
    procedure CrossProduct(P : TVertex3D);
    function InnerProduct(P : TVertex3D) : Double;
    function InnerProductX(P : TVertex3D) : Double; // 2D
    function InnerProductY(P : TVertex3D) : Double; // 2D
    function InnerProductZ(P : TVertex3D) : Double; // 2D
    function Magnitude : Double;
    function MagnitudeX : Double; // 2D
    function MagnitudeY : Double; // 2D
    function MagnitudeZ : Double; // 2D
    procedure Reverse; // reverses the direction of this vector
    procedure Add(P : TVertex3D);
    procedure Subtract(P : TVertex3D);
    procedure Midpoint(P : TVertex3D);
    procedure Unitary;
    function DistanceTo(P : TVertex3D) : Double;
    function WTFAI(PX, PY : TVertex3D; M : TMatrix3D) : Double;
    function WTFAI_Fix(PX, PY : TVertex3D; M : TMatrix3D) : Boolean;
    procedure Position(aX, aY, aZ : Double);
    procedure SetMagnitude(M : Double);
    procedure CopyTo(var XYZ : TXYZ);
    procedure CopyFrom(const XYZ : TXYZ);
  end;

  // Maintains both a linear relationship between points and a spacial
  // relationship for planar patches of points
  TVertexList = class(TObject)
  private
    List : TList;
    FWidth, FHeight : Integer;
    function GetPoint(Index : Integer) : TVertex3D;
    function GetRPoint(X, Y : Integer) : TVertex3D;
    function GetCount : Integer;
  public
    constructor Create;
    destructor Destroy; override;
    procedure SetDimensions(X, Y : Integer);
    function Add(V : TVertex3D) : Integer;
    function GetIJ(Index : Integer) : TPoint;
    procedure Clear;
    function GetIndex(X, Y : Integer) : Integer;
    function CompleteSquare(A, B, C : Integer) : Integer;
    property Count : Integer read GetCount;
    property Points[Index : Integer] : TVertex3D read GetPoint; default;
    property RPoints[X, Y : Integer] : TVertex3D read GetRPoint;
    property Width : Integer read FWidth;
    property Height : Integer read FHeight;
  end;

  TVertexSet = class(TObject)
  private
    List : TList;
    function GetPoint(Index : Integer) : TVertex3D;
    function GetCount : Integer;
  public
    constructor Create(aSize : Integer);
    destructor Destroy; override;
    procedure SetSize(aSize : Integer);
    procedure Copy(aSource : TVertexSet);
    procedure Clean;
    function Add(X, Y, Z : Double) : Integer; overload;
    function Add(V : TVertex3D) : Integer; overload;
    procedure AddList(VL : TVertexList);
    property Count : Integer read GetCount;
    property Points[Index : Integer] : TVertex3D read GetPoint; default;
  end;

  TMatrix3D = class(TObject)
  private
    xx, xy, xz, xo : Double;
    yx, yy, yz, yo : Double;
    zx, zy, zz, zo : Double;
  public
    A, B, C, SA, SB, SC : Double;  // These do not represent the transformation
    constructor Create;
    procedure Translate(x, y, z : Double); overload;
    procedure Translate(V : TVertex3D); overload;
    procedure Scale(f : Double); overload;
    procedure Scale(xf, yf, zf: Double); overload;
    procedure YRot(theta : Double);
    procedure XRot(theta : Double);
    procedure ZRot(theta : Double);
    procedure Multiply(rhs : TMatrix3D);
    procedure Unitary;
    procedure Transform(v, tv : TVertexSet; Offset : Integer); overload;
    procedure Transform(v, tv : TVertexSet); overload;
    procedure Transform(v, tv : TVertex3D); overload;

    function toString : string;
  end;

  TFace3D = class(TObject)
  private
    List : TList;
    FPeer : TObject;
    FIsSpline : Boolean;
    function GetPoint(Index : Integer) : Integer;
    function GetCount : Integer;
  public
    constructor Create(aPeer : TObject); overload;
    constructor Create(aPeer : TObject; Points : array of Integer); overload;
    destructor Destroy; override;
    procedure Clear;
    property Count : Integer read GetCount;
    procedure Add(P : Integer);
    property Points[Index : Integer] : Integer read GetPoint; default;
    property Peer : TObject read FPeer;
    property IsSpline : Boolean read FIsSpline write FIsSpline;
  end;

  TObject3D = class(TObject)
  private
    List : TList;
    Points : array [0..5] of TVertexSet;
//    FLevels : Integer;
    CLevel : Integer;
    function GetCount: Integer;
    function GetFace(Index: Integer): TFace3D;
    function GetVertex(Level, Index : Integer) : TVertex3D;
    function GetVertexCount : Integer;
  public
    constructor Create(aLevels : Integer);
    destructor Destroy; override;
    procedure Clean;
    property Count : Integer read GetCount;
    function AddFace(F: TFace3D) : Integer;
    function AddVertex(X, Y, Z : Double) : Integer; overload;
    function AddVertex(V : TVertex3D) : Integer; overload;
    procedure AddList(VL : TVertexList);
    procedure Transform(Matrix : TMatrix3D);
    procedure TransformBtoB(Matrix : TMatrix3D);


//    procedure CreateCube(aPeer : TObject; S, D : TVertexSet; x1, y1, z1, x2, y2, z2 : Double);
    procedure CreateCylinder(aPeer : TObject; P1, P2, PP : TVertex3D; R : Double; N : Integer);
    procedure CreateSimpleQuadPatch(VL : TVertexList; PA, PB, PC, PD : TVertex3D; N : Integer);
    procedure CreateMountPoint(aPeer : TObject; PO, PA, PB : TVertex3D);
    procedure CreateCylinderPatch(VL : TVertexList; P1, P2, PP : TVertex3D; R : Double; N : Integer; Spacing : Double);
    property Faces[Index : Integer] : TFace3D read GetFace; default;
    property Vertex[Level, Index : Integer] : TVertex3D read GetVertex;
    property VertexCount : Integer read GetVertexCount;
//    property Levels : Integer read FLevels;

    procedure Fix;
  end;

implementation

//const pi = 3.14159265;


constructor TMatrix3D.Create;
begin
  xx := 1;
  yy := 1;
  zz := 1;
end;

procedure TMatrix3D.Scale(f : Double);
begin
  xx := xx * f;
  xy := xy * f;
  xz := xz * f;
  xo := xo * f;
  yx := yx * f;
  yy := yy * f;
  yz := yz * f;
  yo := yo * f;
  zx := zx * f;
  zy := zy * f;
  zz := zz * f;
  zo := zo * f;
end;

{** Scale along each axis independently *}
procedure TMatrix3D.Scale(xf, yf, zf: Double);
begin
  xx := xx * xf;
  xy := xy * xf;
  xz := xz * xf;
  xo := xo * xf;
  yx := yx * yf;
  yy := yy * yf;
  yz := yz * yf;
  yo := yo * yf;
  zx := zx * zf;
  zy := zy * zf;
  zz := zz * zf;
  zo := zo * zf;
end;

{** Translate the origin **}
procedure TMatrix3D.Translate(x, y, z : Double);
begin
  xo := xo + x;
  yo := yo + y;
  zo := zo + z;
end;

procedure TMatrix3D.Translate(V : TVertex3D);
begin
  Translate(-V.X, -V.Y, -V.Z);
end;


{** rotate theta degrees about the y axis *}
procedure TMatrix3D.YRot(theta : Double);
var ct, st : Double;
    Nxx, Nxy, Nxz, Nxo, Nzx, Nzy, Nzz, Nzo : Double;
begin
  SB := SB + Theta;
  ct := Cos(theta);
  st := Sin(theta);

  Nxx := (xx * ct + zx * st);
  Nxy := (xy * ct + zy * st);
  Nxz := (xz * ct + zz * st);
  Nxo := (xo * ct + zo * st);

  Nzx := (zx * ct - xx * st);
  Nzy := (zy * ct - xy * st);
  Nzz := (zz * ct - xz * st);
  Nzo := (zo * ct - xo * st);

  xo := Nxo;
  xx := Nxx;
  xy := Nxy;
  xz := Nxz;
  zo := Nzo;
  zx := Nzx;
  zy := Nzy;
  zz := Nzz;
end;

{** rotate theta degrees about the x axis *}
procedure TMatrix3D.XRot(theta : Double);
var ct, st : Double;
    Nyx, Nyy, Nyz, Nyo, Nzx, Nzy, Nzz, Nzo : Double;
begin
  SA := SA + Theta;
  ct := Cos(theta);
  st := Sin(theta);

  Nyx := (yx * ct + zx * st);
  Nyy := (yy * ct + zy * st);
  Nyz := (yz * ct + zz * st);
  Nyo := (yo * ct + zo * st);

  Nzx := (zx * ct - yx * st);
  Nzy := (zy * ct - yy * st);
  Nzz := (zz * ct - yz * st);
  Nzo := (zo * ct - yo * st);

  yo := Nyo;
  yx := Nyx;
  yy := Nyy;
  yz := Nyz;
  zo := Nzo;
  zx := Nzx;
  zy := Nzy;
  zz := Nzz;
end;

{** rotate theta degrees about the z axis *}
procedure TMatrix3D.ZRot(theta : Double);
var ct, st : Double;
    Nyx, Nyy, Nyz, Nyo, Nxx, Nxy, Nxz, Nxo : Double;
begin
  SC := SC + Theta;
  ct := Cos(theta);
  st := Sin(theta);
  Nyx := (yx * ct + xx * st);
  Nyy := (yy * ct + xy * st);
  Nyz := (yz * ct + xz * st);
  Nyo := (yo * ct + xo * st);
  Nxx := (xx * ct - yx * st);
  Nxy := (xy * ct - yy * st);
  Nxz := (xz * ct - yz * st);
  Nxo := (xo * ct - yo * st);

  yo := Nyo;
  yx := Nyx;
  yy := Nyy;
  yz := Nyz;
  xo := Nxo;
  xx := Nxx;
  xy := Nxy;
  xz := Nxz;
end;


{** Multiply this matrix by a second: M = M*R *}
procedure TMatrix3D.Multiply(rhs : TMatrix3D);
var lxx, lxy, lxz, lxo, lyx, lyy, lyz, lyo, lzx, lzy, lzz, lzo : Double;
begin
  lxx := xx * rhs.xx + yx * rhs.xy + zx * rhs.xz;
  lxy := xy * rhs.xx + yy * rhs.xy + zy * rhs.xz;
  lxz := xz * rhs.xx + yz * rhs.xy + zz * rhs.xz;
  lxo := xo * rhs.xx + yo * rhs.xy + zo * rhs.xz + rhs.xo;

  lyx := xx * rhs.yx + yx * rhs.yy + zx * rhs.yz;
  lyy := xy * rhs.yx + yy * rhs.yy + zy * rhs.yz;
  lyz := xz * rhs.yx + yz * rhs.yy + zz * rhs.yz;
  lyo := xo * rhs.yx + yo * rhs.yy + zo * rhs.yz + rhs.yo;

  lzx := xx * rhs.zx + yx * rhs.zy + zx * rhs.zz;
  lzy := xy * rhs.zx + yy * rhs.zy + zy * rhs.zz;
  lzz := xz * rhs.zx + yz * rhs.zy + zz * rhs.zz;
  lzo := xo * rhs.zx + yo * rhs.zy + zo * rhs.zz + rhs.zo;

  xx := lxx;
  xy := lxy;
  xz := lxz;
  xo := lxo;

  yx := lyx;
  yy := lyy;
  yz := lyz;
  yo := lyo;

  zx := lzx;
  zy := lzy;
  zz := lzz;
  zo := lzo;
end;

{** Reinitialize to the unit matrix *}
procedure TMatrix3D.Unitary;
begin
  xo := 0;
  xx := 1;
  xy := 0;
  xz := 0;
  yo := 0;
  yx := 0;
  yy := 1;
  yz := 0;
  zo := 0;
  zx := 0;
  zy := 0;
  zz := 1;

  SA := 0;
  SB := 0;
  SC := 0;
end;

{** Transform nvert points from v into tv.  v contains the input
    coordinates in floating point.  Three successive entries in
    the array constitute a point.  tv ends up holding the transformed
    points as integers; three successive entries per point *}


{procedure TMatrix3D.Transform(v, tv : TVertexSet);
var lxx, lxy, lxz, lxo, lyx, lyy, lyz, lyo, lzx, lzy, lzz, lzo : Double;
    T : TVertex3D;
    I : Integer;
    x1, Y1, z1 : Double;
begin
  lxx := xx; lxy := xy; lxz := xz; lxo := xo;
  lyx := yx; lyy := yy; lyz := yz; lyo := yo;
  lzx := zx; lzy := zy; lzz := zz; lzo := zo;

  if v <> tv then begin
    for I := 0 to v.Count - 1 do begin
      with V[I] do begin
        T := tv[I];
        t.x := (x * lxx + y * lxy + z * lxz + lxo);
        t.y := (x * lyx + y * lyy + z * lyz + lyo);
        t.z := (x * lzx + y * lzy + z * lzz + lzo);
      end;
    end;
  end else begin
    for I := 0 to v.Count - 1 do begin
      with V[I] do begin
        x1 := x;
        y1 := y;
        z1 := z;
        T := tv[I];
        t.x := (x1 * lxx + y1 * lxy + z1 * lxz + lxo);
        t.y := (x1 * lyx + y1 * lyy + z1 * lyz + lyo);
        t.z := (x1 * lzx + y1 * lzy + z1 * lzz + lzo);
      end;
    end;
  end;
end; }

procedure TMatrix3D.Transform(v, tv : TVertexSet);
begin
  Transform(v, tv, 0);
end;

procedure TMatrix3D.Transform(v, tv : TVertexSet; Offset : Integer);
var    T : TVertex3D;
    I : Integer;
    x1, Y1, z1 : Double;
begin
  if v <> tv then begin
    for I := 0 to v.Count - 1 do begin
      with V[I] do begin
        T := tv[I + Offset];
        t.x := (x * xx + y * xy + z * xz + xo);
        t.y := (x * yx + y * yy + z * yz + yo);
        t.z := (x * zx + y * zy + z * zz + zo);
      end;
    end;
  end else begin
    for I := 0 to v.Count - 1 do begin
      with V[I + Offset] do begin
        x1 := x;
        y1 := y;
        z1 := z;
        T := tv[I];
        t.x := (x1 * xx + y1 * xy + z1 * xz + xo);
        t.y := (x1 * yx + y1 * yy + z1 * yz + yo);
        t.z := (x1 * zx + y1 * zy + z1 * zz + zo);
      end;
    end;
  end;
end;

procedure TMatrix3D.Transform(v, tv : TVertex3D);
var lxx, lxy, lxz, lxo, lyx, lyy, lyz, lyo, lzx, lzy, lzz, lzo : Double;
    x1, y1, z1 : Double;
begin
  lxx := xx; lxy := xy; lxz := xz; lxo := xo;
  lyx := yx; lyy := yy; lyz := yz; lyo := yo;
  lzx := zx; lzy := zy; lzz := zz; lzo := zo;

  x1 := v.x;
  y1 := v.y;
  z1 := v.z;

  tv.x := (x1 * lxx + y1 * lxy + z1 * lxz + lxo);
  tv.y := (x1 * lyx + y1 * lyy + z1 * lyz + lyo);
  tv.z := (x1 * lzx + y1 * lzy + z1 * lzz + lzo);
end;

function TMatrix3D.toString : string;
begin
{    Result := ("[" + xo + "," + xx + "," + xy + "," + xz + ";"
            + yo + "," + yx + "," + yy + "," + yz + ";"
            + zo + "," + zx + "," + zy + "," + zz + "]"); }
end;

{ TVertexSet }

constructor TVertexSet.Create(aSize : Integer);
begin
  inherited Create;
  List := TList.Create;
  SetSize(aSize);
end;

function TVertexSet.Add(V : TVertex3D) : Integer;
begin
  V.Index := List.Add(V);
  Result := V.Index;
end;

function TVertexSet.Add(X, Y, Z : Double) : Integer;
begin
  Result := Add(TVertex3D.Create(X, Y, Z));
end;

procedure TVertexSet.AddList(VL : TVertexList);
var I : Integer;
begin
  for I := 0 to VL.Count - 1 do begin
    Add(VL[I]);
  end;
end;

procedure TVertexSet.Copy(aSource : TVertexSet);
var I : Integer;
begin
  Clean;

  for I := 0 to aSource.Count - 1 do begin
    Add(aSource[I].X, aSource[I].Y, aSource[I].Z);
  end;
end;

procedure TVertexSet.Clean;
var I : Integer;
begin
  for I := 0 to Count - 1 do begin
    GetPoint(I).Free;
  end;

  List.Clear;
end;

destructor TVertexSet.Destroy;
begin
  Clean;
  List.Free;
  inherited;
end;

function TVertexSet.GetPoint(Index: Integer): TVertex3D;
begin
  Result := TVertex3D(List[Index]);
end;

function TVertexSet.GetCount : Integer;
begin
  Result := List.Count;
end;

procedure TVertexSet.SetSize(aSize: Integer);
var I : Integer;
begin
  if aSize > List.Count then begin
    for I := List.Count to aSize - 1 do
      List.Add(TVertex3D.Create);
  end else begin
    for I := List.Count - 1 downto aSize do begin
      TObject(List[I]).Free;
      List.Delete(I);
    end;
  end;
end;

{ TVertex3D }


constructor TVertex3D.Create;
begin
  Clear;
end;

constructor TVertex3D.Create(I, J, K : Double);
begin
  X := I;
  Y := J;
  Z := K;
end;

constructor TVertex3D.Create(P : TVertex3D);
begin
  Copy(P);
end;

procedure TVertex3D.Copy(P : TVertex3D);
begin
  X := P.X;
  Y := P.Y;
  Z := P.Z;
end;

procedure TVertex3D.Clear;
begin
  X := 0;
  Y := 0;
  Z := 0;
end;

procedure TVertex3D.CrossProduct(P : TVertex3D);
var Ax, Ay, Az : Double;
begin
  Ax := X;
  Ay := Y;
  Az := z;

  X := (Ay * P.Z) - (Az * P.Y);
  Y := (Az * P.X) - (Ax * P.Z);
  Z := (Ax * P.Y) - (Ay * P.X);

{	nx = (y1 * z2) - (z1 * y2);
	ny = (z1 * x2) - (x1 * z2);
	nz = (x1 * y2) - (y1 * x2); }
end;

{ Page 619 Flanders Calculus }
function TVertex3D.InnerProduct(P : TVertex3D) : Double;
begin
  Result := (X * P.X) + (Y * P.Y) + (Z * P.Z);
end;

function TVertex3D.InnerProductX(P : TVertex3D) : Double; // 2D
begin
  Result := (Y * P.Y) + (Z * P.Z);
end;

function TVertex3D.InnerProductY(P : TVertex3D) : Double; // 2D
begin
  Result := (X * P.X) + (Z * P.Z);
end;

function TVertex3D.InnerProductZ(P : TVertex3D) : Double; // 2D
begin
  Result := (X * P.X) + (Y * P.Y);
end;

procedure TVertex3D.Add(P : TVertex3D);
begin
  X := X + P.X;
  Y := Y + P.Y;
  Z := Z + P.Z;
end;

procedure TVertex3D.Subtract(P : TVertex3D);
begin
  X := X - P.X;
  Y := Y - P.Y;
  Z := Z - P.Z;
end;

procedure TVertex3D.Midpoint(P : TVertex3D);
begin
  X := (X + P.X) / 2;
  Y := (Y + P.Y) / 2;
  Z := (Z + P.Z) / 2;
end;

function TVertex3D.Magnitude : Double;
begin
  Result := Sqrt(X*X + Y*Y + Z*Z);
end;

function TVertex3D.MagnitudeX : Double; // 2D
begin
  Result := Sqrt(Y*Y + Z*Z);
end;

function TVertex3D.MagnitudeY : Double; // 2D
begin
  Result := Sqrt(X*X + Z*Z);
end;

function TVertex3D.MagnitudeZ : Double; // 2D
begin
  Result := Sqrt(X*X + Y*Y);
end;

procedure TVertex3D.SetMagnitude(M : Double);
var A : Double;
begin
  A := Magnitude;
  X := X * (M / A);
  Y := Y * (M / A);
  Z := Z * (M / A);
end;


procedure TVertex3D.CopyTo(var XYZ : TXYZ);
begin
  XYZ.X := X;
  XYZ.Y := Y;
  XYZ.Z := Z;
end;

procedure TVertex3D.CopyFrom(const XYZ : TXYZ);
begin
  X := XYZ.X;
  Y := XYZ.Y;
  Z := XYZ.Z;
end;

procedure TVertex3D.Unitary;
//var M : Double;
begin
  SetMagnitude(1);
{  M := Magnitude;
  X := X / M;
  Y := Y / M;
  Z := Z / M; }
end;

procedure TVertex3D.Reverse; // reverses the direction of this vector
begin
  X := -X;
  Y := -Y;
  Z := -Z;
end;


function TVertex3D.DistanceTo(P : TVertex3D) : Double;
begin
  Result := Sqrt(Power(X - P.X, 2) + Power(Y - P.Y, 2) + Power(Z - P.Z, 2));
end;

procedure TVertex3D.Position(aX, aY, aZ : Double);
begin
  X := aX;
  Y := aY;
  Z := aZ;
end;


// Given two points find the matrix that will transpose these points from
// AX.X = 0
// AX.Y = 0
// AY.Z = 0
procedure TVertex3D.FindMatrix(AX, AY : TVertex3D; M : TMatrix3D);
begin
  // Looking from z what is the angle between PX and X0
  if AX.X = 0 then begin
    if AX.Y > 0 then
      M.C := PI/2
    else
      M.C := -PI/2;
  end else begin
    M.C := ArcTan(AX.Y / AX.X);
    if AX.X < 0 then
      M.C := PI + M.C;
  end;

  M.ZRot(-M.C);
  M.Transform(AX, AX);
  M.Transform(AY, AY);

  // Now we have rotated our vectors to remove C we can look from
  // y and correct PX to fall on to X0
  if AX.X = 0 then begin
    if AX.Z > 0 then
      M.B := PI/2
    else
      M.B := -PI/2;
  end else begin
    M.B := ArcTan(AX.Z / AX.X);
    if AX.X < 0 then
      M.B := PI + M.B;
  end;
  M.B := -M.B;

  M.Unitary;
  M.YRot(-M.B);
  M.Transform(AX, AX);
  M.Transform(AY, AY);

  // Now we have rotated our vectors to remove C and B we can look from
  // x and correct PY to fall on Y0
  if AY.Y = 0 then begin
    if AY.Z > 0 then
      M.A := PI/2
    else
      M.A := -PI/2;
  end else begin
    M.A := ArcTan(AY.Z / AY.Y);
    if AY.Y < 0 then
      M.A := PI + M.A;
  end;
  M.A := -M.A;

  M.Unitary;
  M.XRot(-M.A);
  M.Transform(AX, AX);
  M.Transform(AY, AY); 
end;

function TVertex3D.WTFAI(PX, PY : TVertex3D; M : TMatrix3D) : Double;
var AO, AX, AY : TVertex3D;
begin
  AO := TVertex3D.Create(Self);
  AX := TVertex3D.Create(PX);
  AY := TVertex3D.Create(PY);

  // Sequence
  // move Self to origin and
  // From z correct AX to X0
  // From y correct AX to X0
  // From x correct AY to Y0

  try
    M.Unitary;
    AO.Subtract(Self);
    AX.Subtract(Self);
    AY.Subtract(Self);

    FindMatrix(AX, AY, M);

    Result := AY.X;

    M.Unitary;
    M.XRot(M.A);
    M.YRot(M.B);
    M.ZRot(M.C);
    M.Translate(Self.X, Self.Y, Self.Z);
  finally
    AO.Free;
    AX.Free;
    AY.Free;
  end;
end;

function TVertex3D.WTFAI_Fix(PX, PY : TVertex3D; M : TMatrix3D) : Boolean;
var AO, AX, AY : TVertex3D;
begin
  AO := TVertex3D.Create(Self);
  AX := TVertex3D.Create(PX);
  AY := TVertex3D.Create(PY);

  // Sequence
  // move Self to origin and
  // From z correct AX to X0
  // From y correct AX to X0
  // From x correct AY to Y0

  try
    M.Unitary;
    AO.Subtract(Self);
    AX.Subtract(Self);
    AY.Subtract(Self);

    FindMatrix(AX, AY, M);
  finally
    AO.Free;
    AX.Free;
    AY.Free;
  end;
end;
{ TFace3D }

procedure TFace3D.Add(P : Integer);
begin
  List.Add(Pointer(P));
end;

procedure TFace3D.Clear;
begin
  List.Clear;
end;

constructor TFace3D.Create(aPeer : TObject);
begin
  FPeer := aPeer;
  List := TList.Create;
  FIsSpline := False;
end;

destructor TFace3D.Destroy;
begin
  List.Free;
  inherited;
end;

constructor TFace3D.Create(aPeer : TObject; Points : array of Integer);
var I : Integer;
begin
  Create(aPeer);
  for I := Low(Points) to High(Points) do begin
    Add(Points[I]);
  end;
end;

function TFace3D.GetCount: Integer;
begin
  Result := List.Count;
end;

function TFace3D.GetPoint(Index: Integer): Integer;
begin
  Result := Integer(List[Index]);
end;

{ TObject3D }

function TObject3D.AddFace(F: TFace3D) : Integer;
begin
  Result := List.Add(F);
end;

function TObject3D.AddVertex(X, Y, Z : Double) : Integer;
begin
  Result := Points[1].Add(X, Y, Z);
end;

procedure TObject3D.Transform(Matrix : TMatrix3D);
begin
  Matrix.Transform(Points[0], Points[1]);
end;

procedure TObject3D.TransformBtoB(Matrix : TMatrix3D);
begin
  Matrix.Transform(Points[1], Points[1]);
end;

function TObject3D.AddVertex(V : TVertex3D) : Integer;
begin
  Result := Points[1].Add(V);
end;

constructor TObject3D.Create(aLevels : Integer);
begin
  inherited Create;
  List := TList.Create;
  CLevel := 1;
  Points[0] := TVertexSet.Create(0);
  Points[1] := TVertexSet.Create(0);
end;

destructor TObject3D.Destroy;
begin
  Clean;
  Points[0].Free;
  Points[1].Free;
  List.Free;
  inherited;
end;

procedure TObject3D.Clean;
var I : Integer;
begin
  Points[0].Clean;
  Points[1].Clean;
  for I := 0 to List.Count - 1 do begin
    TFace3D(List[I]).Free;
  end;
  List.Clear;
end;

procedure TObject3D.Fix;
begin
  Points[0].Copy(Points[1]);
end;

{procedure TObject3D.CreateCube(aPeer : TObject; S, D : TVertexSet; x1, y1, z1, x2, y2, z2 : Double);
var Index : Integer;
begin
  Index := S.Count;
  S.SetSize(S.Count + 8);

  S[Index].X     := x1;
  S[Index].Y     := y1;
  S[Index].Z     := z1;

  S[Index + 1].X := x1;
  S[Index + 1].Y := y1;
  S[Index + 1].Z := z2;

  S[Index + 2].X := x1;
  S[Index + 2].Y := y2;
  S[Index + 2].Z := z2;

  S[Index + 3].X := x1;
  S[Index + 3].Y := y2;
  S[Index + 3].Z := z1;

  S[Index + 4].X := x2;
  S[Index + 4].Y := y2;
  S[Index + 4].Z := z2;

  S[Index + 5].X := x2;
  S[Index + 5].Y := y2;
  S[Index + 5].Z := z1;

  S[Index + 6].X := x2;
  S[Index + 6].Y := y1;
  S[Index + 6].Z := z1;

  S[Index + 7].X := x2;
  S[Index + 7].Y := y1;
  S[Index + 7].Z := z2;

  D.SetSize(S.Count);
  Add(TFace3D.Create(aPeer, [Index, Index + 1, Index + 2, Index + 3]));
  Add(TFace3D.Create(aPeer, [Index + 4, Index + 5, Index + 6, Index + 7]));

  Add(TFace3D.Create(aPeer, [Index + 0, Index + 6, Index + 7, Index + 1]));
  Add(TFace3D.Create(aPeer, [Index + 3, Index + 5, Index + 4, Index + 2]));
  Add(TFace3D.Create(aPeer, [Index + 3, Index + 5, Index + 6, Index + 0]));
  Add(TFace3D.Create(aPeer, [Index + 4, Index + 2, Index + 1, Index + 7]));
end;
}

// Generate a Pyramid centered upon PO with a base centered on PA, PB is required
// to generate a timing for the four points of the pyramid
procedure TObject3D.CreateMountPoint(aPeer : TObject; PO, PA, PB : TVertex3D);
var PP1, PP2, PPP : TVertex3D;
    Matrix : TMatrix3D;
    I, J : Integer;
begin
  Matrix := TMatrix3D.Create;
  PP1 := TVertex3D.Create(PO);
  PP2 := TVertex3D.Create(PA);
  PPP := TVertex3D.Create(PB);

  PP1.WTFAI(PP2, PPP, Matrix);

  I := Points[CLevel].Count;

  Points[CLevel].Add(0, 0, 0);
  Points[CLevel].Add(1, 1, 1);
  Points[CLevel].Add(1, -1, 1);
  Points[CLevel].Add(1, -1, -1);
  Points[CLevel].Add(1, 1, -1);

  Self.AddFace(TFace3D.Create(aPeer, [I, I+1, I+2]));
  Self.AddFace(TFace3D.Create(aPeer, [I, I+2, I+3]));
  Self.AddFace(TFace3D.Create(aPeer, [I, I+3, I+4]));
  Self.AddFace(TFace3D.Create(aPeer, [I, I+4, I+1]));

  for J := 0 to 4 do
    Matrix.Transform(Points[CLevel][I+J], Points[CLevel][I+J]);

  PP1.Free;
  PP2.Free;
  PPP.Free;
  Matrix.Free;
end;


{ P1 and P2 represent the two end of the cylinder PP represents a point that is
  midway along its length and on the circumference. }
procedure TObject3D.CreateCylinder(aPeer : TObject; P1, P2, PP : TVertex3D; R : Double; N : Integer);
var PPM, PP1, PP2, PPP : TVertex3D;
    Matrix : TMatrix3D;
    L, Theta : Double;
    I, Index : Integer;
    F1 : TFace3D;
begin
  Matrix := TMatrix3D.Create;
  PP1 := TVertex3D.Create(P1);
  PP2 := TVertex3D.Create(P2);
  PPP := TVertex3D.Create(PP);

  PP1.WTFAI(PP2, PPP, Matrix);

  L := P1.DistanceTo(P2);
  PPM := TVertex3D.Create(P1);
  PPM.Midpoint(P2);
//  R := PPM.DistanceTo(PP);

  Index := Points[CLevel].Count;
  Points[CLevel].SetSize(Points[CLevel].Count + (N * 2));
  Theta := 2 * PI / n;

  for I := 0 to N - 1 do begin
    Points[CLevel][Index + I].X := 0;
    Points[CLevel][Index + I].Z := Sin(Theta * I) * R;
    Points[CLevel][Index + I].Y := Cos(Theta * I) * R;

    Points[CLevel][Index + I + N].Copy(Points[CLevel][Index + I]);
    Points[CLevel][Index + I + N].X := L;
    Matrix.Transform(Points[CLevel][Index + I], Points[CLevel][Index + I]);
    Matrix.Transform(Points[CLevel][Index + I + N], Points[CLevel][Index + I + N]);
  end;

  F1 := TFace3D.Create(aPeer, [Index, Index + n, Index + n + n - 1, Index + n - 1]);
  AddFace(F1);

  for I := 0 to n - 2 do begin
    F1 := TFace3D.Create(aPeer, [Index + I, Index + n + I, Index + n + I + 1, Index + I + 1]);
    AddFace(F1);
  end;

  PP1.Free;
  PP2.Free;
  PPP.Free;
  PPM.Free;
  Matrix.Free;
end;

procedure TObject3D.CreateCylinderPatch(VL : TVertexList;
     P1, P2, PP : TVertex3D; R : Double; N : Integer; Spacing : Double);
var PPM, PP1, PP2, PPP : TVertex3D;
    Matrix : TMatrix3D;
    C, T, Theta : Double;
    I, J, Index : Integer;
begin
  Matrix := TMatrix3D.Create;
  PP1 := TVertex3D.Create(P1);
  PP2 := TVertex3D.Create(P2);
  PPP := TVertex3D.Create(PP);

  C := PP1.WTFAI(PP2, PPP, Matrix);

  PPM := TVertex3D.Create(P1);
  PPM.Midpoint(P2);

  Theta := ArcTan(Spacing / R);

  C := C - ((N / 2) * Spacing);
  T := -Theta * (N / 2);
  VL.Clear;
  VL.SetDimensions(N, N);
  for J := 0 to N - 1 do begin
    for I := 0 to N - 1 do begin
      Index := VL.Add(TVertex3D.Create);
      VL[Index].X := C + (Spacing * J);
      VL[Index].Z := Sin(Theta * I + T) * R;
      VL[Index].Y := Cos(Theta * I + T) * R;
      Matrix.Transform(VL[Index], VL[Index]);
    end;
  end;

  PP1.Free;
  PP2.Free;
  PPP.Free;
  PPM.Free;
  Matrix.Free;
end;

{procedure TObject3D.CreateSimpleQuad(aPeer : TObject; PA, PB, PC, PD : TVertex3D);
var F1 : TFace3D;
    I : Integer;
begin
  I := Draw.Count;
  Draw.Add(PA.X, PA.Y, PA.Z);
  Draw.Add(PB.X, PB.Y, PB.Z);
  Draw.Add(PC.X, PC.Y, PC.Z);
  Draw.Add(PD.X, PD.Y, PD.Z);

  F1 := TFace3D.Create(aPeer, [I, I + 1, I + 2, I + 3]);
  Add(F1);
end; }

procedure TObject3D.CreateSimpleQuadPatch(VL : TVertexList; PA, PB, PC, PD : TVertex3D; N : Integer);
var I, J : Integer;
    X1, Y1, Z1, X2, Y2, Z2 : Double;
begin
//
//  PA    PB
//  PD    PC
//
  VL.Clear;
  VL.SetDimensions(N, N);
  for J := 0 to N - 1 do begin
    X1 := PA.X - ((PA.X - PD.X) / N * J);
    Y1 := PA.Y - ((PA.Y - PD.Y) / N * J);
    Z1 := PA.Z - ((PA.Z - PD.Z) / N * J);

    X2 := PB.X - ((PB.X - PC.X) / N * J);
    Y2 := PB.Y - ((PB.Y - PC.Y) / N * J);
    Z2 := PB.Z - ((PB.Z - PC.Z) / N * J);
    for I := 0 to N - 1 do begin
      VL.Add(TVertex3D.Create(X1 - ((X1 - X2) / N * I), Y1 - ((Y1 - Y2) / N * I), Z1 - ((Z1 - Z2) / N * I)));
//      Result.Add(G[T]);
    end;
  end;
end;

function TObject3D.GetCount: Integer;
begin
  Result := List.Count;
end;

function TObject3D.GetFace(Index: Integer): TFace3D;
begin
  Result := TFace3D(List[Index]);
end;

function TObject3D.GetVertex(Level, Index : Integer) : TVertex3D;
begin
  Result := Points[Level][Index];
end;

procedure TObject3D.AddList(VL : TVertexList);
begin
  Points[CLevel].AddList(VL);
end;


function TObject3D.GetVertexCount : Integer;
begin
  Result := Points[CLevel].Count;
end;

{ TVertexList }

function TVertexList.Add(V: TVertex3D): Integer;
begin
  Result := List.Add(V);
end;

constructor TVertexList.Create;
begin
  List := TList.Create;
end;

procedure TVertexList.SetDimensions(X, Y : Integer);
begin
  FWidth := X;
  FHeight := Y;
end;

function TVertexList.GetIJ(Index : Integer) : TPoint;
begin
  Result.X := Index div Width;
  Result.Y := Index mod Width;
end;

function TVertexList.CompleteSquare(A, B, C : Integer) : Integer;
  function Adjacent(const I, J : TPoint) : Boolean;
  begin
    Result := (Abs(I.X - J.X) <= 1) and (Abs(I.Y - J.Y) <= 1)
  end;

var Indic : array [0..3] of TPoint;
begin
  Indic[0] := GetIJ(A);
  Indic[1] := GetIJ(B);
  Indic[2] := GetIJ(C);
  Result := -1;

  if Adjacent(Indic[0], Indic[1]) and
     Adjacent(Indic[0], Indic[2]) and
     Adjacent(Indic[1], Indic[2]) then begin

     if (Indic[0].X <> Indic[1].X) and
        (Indic[0].X <> Indic[2].X) then begin
       Indic[3].X := Indic[0].X
     end else if (Indic[1].X <> Indic[0].X) and
                 (Indic[1].X <> Indic[2].X) then begin
       Indic[3].X := Indic[1].X
     end else if (Indic[2].X <> Indic[0].X) and
                 (Indic[2].X <> Indic[1].X) then begin
       Indic[3].X := Indic[2].X
     end else begin
       Exit;
     end;


     if (Indic[0].Y <> Indic[1].Y) and
        (Indic[0].Y <> Indic[2].Y) then begin
       Indic[3].Y := Indic[0].Y
     end else if (Indic[1].Y <> Indic[0].Y) and
                 (Indic[1].Y <> Indic[2].Y) then begin
       Indic[3].Y := Indic[1].Y
     end else if (Indic[2].Y <> Indic[0].Y) and
                 (Indic[2].Y <> Indic[1].Y) then begin
       Indic[3].Y := Indic[2].Y
     end else begin
       Exit;
     end;
    Result := GetIndex(Indic[3].X, Indic[3].Y)
  end;
end;

destructor TVertexList.Destroy;
begin
  List.Free;
  inherited;
end;

procedure TVertexList.Clear;
begin
  List.Clear;
end;

function TVertexList.GetCount: Integer;
begin
  Result := List.Count;
end;

function TVertexList.GetPoint(Index: Integer): TVertex3D;
begin
  Result := TVertex3D(List[Index]);
end;

function TVertexList.GetRPoint(X, Y : Integer) : TVertex3D;
begin
  Result := GetPoint(X * Width + Y);
end;

function TVertexList.GetIndex(X, Y : Integer) : Integer;
begin
  Result := X * Width + Y;
end;

end.
