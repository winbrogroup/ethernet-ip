unit ConstraintRules;

interface

uses
  SysUtils, Classes, Tokenizer, Matrix3D;

type
  TConstraintRulesFile = class;
  TProbePoint = class;

  TStackingAxis = class(TObject)
  private
    Rules : TConstraintRulesFile;
  public
    Name : string;
    PO, PBC, PA : Integer;
    constructor Create(aRulesFile : TConstraintRulesFile);
    procedure ReadFromStrings(S : TQuoteTokenizer);
    procedure GenerateVisual(Obj : TObject3D);
    procedure Clean;
    destructor Destroy; override;
  end;

  TAxisNames = (
    anX,
    anY,
    anZ,
    anA,
    anB,
    anC,
    anU,
    anV,
    anW
  );

  TMachinePosition = class(TObject)
    Positions : array [TAxisNames] of Double;
    procedure ReadFromStrings(S : TQuoteTokenizer);
  end;


  TConstraintClass = class of TConstraint;
  TConstraint = class(TObject)
  private
    BRot, CRot : Double;
  public
    Name : string;
    PO : Integer;         // Vertex in move array representing normal contact
    PA, PB : Integer;     // Primary definition vertices
    PM : Integer;         // Vertex representing a point inside the mount
    PT : Integer;         // Vertex used for timing the mount point
    PMount : Integer;     // Unmoving point of the mount
    PResult : Integer;    // Unmoving point of result
    PVar : Integer;       // Point created in the mount array that corresponds to
                          // the nearest point interpolated on the patch
    PPath1, PPath2 : Integer;
    Rules : TConstraintRulesFile;
    Patch : TVertexList;
    ProbePoint : TProbePoint; // Rules governing what to do with the result
    PClosest : TVertex3D;
    Tmp : Integer;
    Geometry : TObject3D;
    constructor Create(aRulesFile : TConstraintRulesFile); virtual;
    destructor Destroy; override;
    procedure ReadFromStrings(S : TQuoteTokenizer); virtual; abstract;
    function ReadToken(const aToken : string; S : TQuoteTokenizer) : Boolean;
    procedure GenerateVisual; virtual;
    function MountError : Double; virtual;
    function NominalError : Double; virtual;
    function SetProbeResult(X, Y, Z : Double) : Double;
    procedure CenterOfRotation(var P1 : TXYZ); virtual; abstract;
    function HasAxisOfRotation : Boolean; virtual; abstract;
    procedure BCAxisOfRotation; virtual; abstract;
    procedure CheckParamters; virtual;
  end;

  TCylinderConstraint = class(TConstraint)
    Radius : Double;
    D : Double;           // Half face size of patch
    procedure ReadFromStrings(S : TQuoteTokenizer); override;
    procedure GenerateVisual; override;
    function MountError : Double; override;
    function NominalError : Double; override;
    procedure CenterOfRotation(var P1 : TXYZ); override;
    function HasAxisOfRotation : Boolean; override;
    procedure BCAxisOfRotation; override;
    procedure CheckParamters; override;
  end;

  TSplineConstraint = class(TConstraint)
    procedure ReadFromStrings(S : TQuoteTokenizer); override;
    procedure GenerateVisual; override;
    function MountError : Double; override;
    function NominalError : Double; override;
    procedure CenterOfRotation(var P1 : TXYZ); override;
    function HasAxisOfRotation : Boolean; override;
    procedure BCAxisOfRotation; override;
  end;

  TPlaneConstraint = class(TConstraint)
    PC, PD : Integer;
    constructor Create(aRulesFile : TConstraintRulesFile); override;
    destructor Destroy; override;
    procedure ReadFromStrings(S : TQuoteTokenizer); override;
    procedure GenerateVisual; override;
    function MountError : Double; override;
    function NominalError : Double; override;
    procedure CenterOfRotation(var P1 : TXYZ); override;
    function HasAxisOfRotation : Boolean; override;
    procedure BCAxisOfRotation; override;
  end;

  TAdjustType = (
    atLinear,
    atCircular,
    atCylindrical
  );

  TProbePoint = class(TObject)
  private
    ConstraintList : TList;
    PrimaryConstraint : TConstraint;
    Adjust : TAdjustType;
    RulesFile : TConstraintRulesFile;
    Clearance, Through : Double;
    function GetConstraint(Index : Integer) : TConstraint;
  public
    constructor Create(aRulesFile : TConstraintRulesFile);
    destructor Destroy; override;
    procedure ReadFromStrings(S : TQuoteTokenizer); virtual;
    property Constraints[Index : Integer] : TConstraint read GetConstraint;
  end;



  TConstraintRulesFile = class(TQuoteTokenizer)
  private
    ConstraintList : TStringList;
    ProbePointList : TStringList;
    FLoaded : Boolean;
    FStackingAxis : TStackingAxis;
    Machine : TMachinePosition;
    ToolBallRadius : Double;
    ToolOffset : TXYZ;
    Retry : Integer;
    PatchPoints : Integer;

    procedure ReadConstraints;
    procedure ReadMachine;
    procedure ReadStackingAxis;
    function ReadVertex(O : TObject3D) : Integer;
    procedure ReadDisplay;

    function GetConstraint(Index : Integer) : TConstraint;
    function GetConstraintCount : Integer;
    procedure DoReadFromStrings;

    procedure Clean;
  public
    Mount : TObject3D;
    MountMatrix : TMatrix3D;

    Part : TObject3D;
    PartMatrix : TMatrix3D;

    Normal : array[0..3] of Integer;
    ViewC : Integer;

    UnityMatrix : TMatrix3D;
    Fit : Double;

    constructor Create;
    destructor Destroy; override;
    procedure ReadFromStrings(aSqr, aRetry : Integer; aFit : Double);
    procedure SaveToStrings;
    property StackingAxis : TStackingAxis read FStackingAxis;
    property Constraints[Index : Integer] : TConstraint read GetConstraint;
    property ConstraintCount : Integer read GetConstraintCount;
    property Loaded : Boolean read FLoaded;
    function FindConstraint(aName : string) : TConstraint;
    procedure Reset;
    procedure VisualTransform(aMatrix : TMatrix3D);
    procedure ApplyMatrix(aMatrix : TMatrix3D);
    procedure Constrain(C : TConstraint);
  end;

implementation

var Units : Double;

function ReadDouble(S : TQuoteTokenizer) : Double;
begin
  Result := StrToFloat(S.Read) * Units;
end;

function ReadDoubleAssign(S : TQuoteTokenizer) : Double;
begin
  S.ExpectedToken('=');
  Result := ReadDouble(S);
end;

function ReadDelimited(var S : string; Delimiter : Char) : string;
var P : Integer;
begin
  P := Pos(Delimiter, S);
  if P = 0 then begin
    Result := Trim(S);
    S := '';
  end else begin
    Result := Trim(Copy(S, 1, P - 1));
    System.Delete(S, 1, P);
  end;
end;

{ TConstraintRulesFile }

constructor TConstraintRulesFile.Create;
begin
  inherited Create;
  ConstraintList := TStringList.Create;
  FStackingAxis := TStackingAxis.Create(Self);
  ProbePointList := TStringList.Create;

  Mount := TObject3D.Create(3);
  MountMatrix := TMatrix3D.Create;
  Part := TObject3D.Create(3);
  PartMatrix := TMatrix3D.Create;
  Machine := TMachinePosition.Create;

  UnityMatrix := TMatrix3D.Create;
end;

destructor TConstraintRulesFile.Destroy;
begin
  Clean;
  ConstraintList.Free;
  FStackingAxis.Free;
  ProbePointList.Free;
  Machine.Free;

  MountMatrix.Free;
  Mount.Free;

  Part.Free;
  PartMatrix.Free;

  UnityMatrix.Free;
  inherited;
end;

procedure TConstraintRulesFile.Reset;
begin
  ToolOffset.X := 0;
  ToolOffset.Y := 0;
  ToolOffset.Z := 0;
  MountMatrix.Unitary;
  PartMatrix.Unitary;
  UnityMatrix.Unitary;
end;

procedure TConstraintRulesFile.Clean;
  procedure CleanList(List : TStringList);
  begin
    if Assigned(List) then
      while List.Count > 0 do begin
        List.Objects[0].Free;
        List.Delete(0);
      end;
  end;
begin
  Mount.Clean;
  Part.Clean;
  StackingAxis.Clean;
  CleanList(ConstraintList);
  CleanList(ProbePointList);
end;


procedure TConstraintRulesFile.ReadFromStrings(aSqr, aRetry : Integer; aFit : Double);
var I : Integer;
begin
  FLoaded := False;
  Retry := aRetry;
  Fit := aFit;
  PatchPoints := aSqr;

  if PatchPoints > 100 then
    PatchPoints := 100;

  if PatchPoints < 5 then
    PatchPoints := 5;

  DoReadFromStrings;
  Mount.Fix;
  Part.Fix;
  for I := 0 to ConstraintCount - 1 do begin
    Constraints[I].Geometry.Fix;
  end;

  Reset;
  ApplyMatrix(nil);
  FLoaded := True;
end;

procedure TConstraintRulesFile.DoReadFromStrings;

  procedure ReadUnits;
  begin
    ExpectedToken('=');
    Units := StrToFloat(Read)
  end;

  procedure ReadToolOffset;
  var I : Integer;
  begin
    I :=  ReadVertex(Mount);
    Mount.Vertex[1, I].CopyTo(ToolOffset);
  end;

var Token : string;
begin
  Clean;
  Seperator := '[](){}=,';
  QuoteChar := '''';
  Rewind;
  Units := 1;

  Normal[0] := Mount.AddVertex(0, 0, 0);
  Normal[1] := Mount.AddVertex(0, 0, 1);
  Normal[2] := Mount.AddVertex(0, 1, 0);
  Normal[3] := Mount.AddVertex(1, 0, 0);

  Part.AddVertex(0, 0, 0);
  Part.AddVertex(0, 0, 1);
  Part.AddVertex(0, 1, 0);
  Part.AddVertex(1, 0, 0);

  ViewC := Mount.AddVertex(0, 0, 0);

  try
    ExpectedTokens(['ConstraintRules', '=', '{']);

    Token := LowerCase(Read);
    while not EndOfStream do begin
      if Token = 'constraints' then
        ReadConstraints
      else if Token = 'machine' then
        ReadMachine
      else if Token = 'stackingaxis' then
        ReadStackingAxis
      else if Token = 'display' then
        ReadDisplay
      else if Token = 'toolballradius' then
        ToolBallRadius := ReadDoubleAssign(Self)
      else if Token = 'tooloffset' then
        ReadToolOffset
      else if Token = 'units' then
        ReadUnits
      else if Token = '}' then
        Exit
      else
        raise Exception.CreateFmt('Invalid token "%s"', [Token]);

      Token := LowerCase(Read);
    end;
    raise Exception.Create('Unexpected end of file');
  except
    on E : Exception do
      raise Exception.CreateFmt('%s occured at line %d', [E.message, Line]);
  end;
end;

procedure TConstraintRulesFile.ReadConstraints;
  procedure CreateConstraint(ConstraintClass : TConstraintClass);
  var Constraint : TConstraint;
  begin
    Constraint := ConstraintClass.Create(Self);
    ConstraintList.AddObject(Constraint.Name, Constraint);
    Push;
    Constraint.ReadFromStrings(Self);
    Constraint.GenerateVisual;
    // Now create the fixture to support this point
  end;

var Token : string;
begin
  ExpectedTokens(['=', '{']);
  Token := LowerCase(Read);
  while not EndOfStream do begin
    if Token = 'cylinder' then
      CreateConstraint(TCylinderConstraint)
    else if Token = 'plane' then
      CreateConstraint(TPlaneConstraint)
    else if Token = 'spline' then
      CreateConstraint(TSplineConstraint)
    else if Token = '}' then
      Exit
    else
      raise Exception.CreateFmt('Invalid token "%s"', [Token]);
    Token := LowerCase(Read);
  end;
  raise Exception.Create('Unexpected end of file');
end;

procedure TConstraintRulesFile.ReadMachine;
begin
  Push;
  Machine.ReadFromStrings(Self);
end;

procedure TConstraintRulesFile.ReadDisplay;
var Token : string;
    F : TFace3D;
begin
  ExpectedTokens(['=', '{']);
  Token := LowerCase(Read);
  F := TFace3D.Create(Self);
  Part.AddFace(F);

  while not EndOfStream do begin
    if Token = '}' then begin
      Exit;
    end else if Token = 'p'then begin
      F.Add(ReadVertex(Part));
    end else
      raise Exception.CreateFmt('Invalid token "%s"', [Token]);

    Token := LowerCase(Read);
  end;
  raise Exception.Create('Unexpected end of file');
end;


procedure TConstraintRulesFile.ReadStackingAxis;
begin
  Push;
  StackingAxis.Clean;
  StackingAxis.ReadFromStrings(Self);
  StackingAxis.GenerateVisual(Part);
end;

function TConstraintRulesFile.ReadVertex(O : TObject3D) : Integer;
var V : TVertex3D;
begin
  V := TVertex3D.Create;
  Result := O.AddVertex(V);
//  ExpectedToken('=');
  V.X := ReadDoubleAssign(Self);//StrToFloat(Read);
  ExpectedToken(',');
  V.Y := ReadDouble(Self);//StrToFloat(Read);
  ExpectedToken(',');
  V.Z := ReadDouble(Self);//StrToFloat(Read);

  V.X := V.X + ToolOffset.X;
  V.Y := V.Y + ToolOffset.Y;
  V.Z := V.Z + ToolOffset.Z;
end;

procedure TConstraintRulesFile.SaveToStrings;
begin
end;


{ TMachine }

procedure TMachinePosition.ReadFromStrings(S: TQuoteTokenizer);
var Token : string;
begin
  S.ExpectedTokens(['machine', '=', '{']);
  Token := LowerCase(S.Read);
  FillChar(Positions, SizeOf(Positions), 0);
  while not S.EndOfStream do begin
    if Token = '}' then
      Exit;
    if Length(Token) > 1 then
      raise Exception.CreateFmt('Invalid axis name "%s"', [Token]);

    case Token[1] of
      'x' : Positions[anX] := ReadDoubleAssign(S);
      'y' : Positions[anX] := ReadDoubleAssign(S);
      'z' : Positions[anX] := ReadDoubleAssign(S);
      'a' : Positions[anX] := ReadDoubleAssign(S);
      'b' : Positions[anX] := ReadDoubleAssign(S);
      'c' : Positions[anX] := ReadDoubleAssign(S);
      'u' : Positions[anX] := ReadDoubleAssign(S);
      'v' : Positions[anX] := ReadDoubleAssign(S);
      'w' : Positions[anX] := ReadDoubleAssign(S);
    else
      raise Exception.CreateFmt('Invalid axis name "%s"', [Token]);
    end;
    Token := LowerCase(S.Read);
  end;
  raise Exception.Create('Unexpected end of file');
end;

function TConstraintRulesFile.GetConstraint(Index: Integer): TConstraint;
begin
  Result := TConstraint(ConstraintList.Objects[Index]);
end;

function TConstraintRulesFile.GetConstraintCount: Integer;
begin
  Result := ConstraintList.Count;
end;

function TConstraintRulesFile.FindConstraint(aName: string): TConstraint;
var I : Integer;
begin
  for I := 0 to ConstraintCount - 1 do begin
    Result := Constraints[I];
    if CompareText(Result.Name, aName) = 0 then begin
      Exit;
    end;
  end;

  raise Exception.CreateFmt('Unknown constraint "%s"', [aName]);
end;


procedure TConstraintRulesFile.ApplyMatrix(aMatrix : TMatrix3D);
var I : Integer;
    M : TMatrix3D;
begin
//  Part.Vertex[1, Normal[0]].WTFAI(Part.Vertex[1, Normal[3]], Part.Vertex[1, Normal[2]], PartMatrix);
  M := TMatrix3D.Create;

  for I := 0 to ConstraintCount - 1 do begin
    M.Unitary;
    if Assigned(aMatrix) then
      M.Multiply(aMatrix);

    M.Multiply(PartMatrix);
    Constraints[I].Geometry.Transform(M);
  end;

  M.Unitary;
  M.Multiply(PartMatrix);
  if Assigned(aMatrix) then
    M.Multiply(aMatrix);
  Part.Transform(M);

  M.Unitary;
  M.Multiply(UnityMatrix);
  if Assigned(aMatrix) then
    M.Multiply(aMatrix);
  Mount.Transform(M);

  M.Free;
end;

procedure TConstraintRulesFile.VisualTransform(aMatrix : TMatrix3D);
var I : Integer;
begin
//  ApplyMatrix(nil);
  if Assigned(aMatrix) then begin
    for I := 0 to Constraintcount - 1 do
      Constraints[I].Geometry.TransformBtoB(aMatrix);

    Part.TransformBtoB(aMatrix);
    Mount.TransformBtoB(aMatrix);
  end;
end;

procedure TConstraintRulesFile.Constrain(C : TConstraint);
var M, P, PC : TVertex3D;
    Mat : TMatrix3D;
    Rot : Double;
    I : Integer;
    BRot, CRot : Double;
    RotCenter, Tmp : TXYZ;
    A1, A2 : Double;
begin
  if not Assigned(C.ProbePoint) then
    Exit; // ??Error??

  ApplyMatrix(nil);

  M := Mount.Vertex[1, C.PResult];
  P := C.PClosest;

  case C.ProbePoint.Adjust of
    atLinear : begin
      PartMatrix.Translate(M.X - P.X, M.Y - P.Y, M.Z - P.Z);
    end;

    atCircular : begin
      if not Assigned(C.ProbePoint.PrimaryConstraint) then
        Exit; // ??Error??

      PC := Mount.Vertex[1, C.ProbePoint.PrimaryConstraint.PResult];
      // What we need to do
      // 1. Translate to -PC
      // 2. Run WTFAI to Determine the transformation to come from this
      //                               M   +
      //                               |   Y
      //     PC  --------------------- P   -
      //                  -X+
      // 3. Transform M onto P with a C rotation
      // 4. Then run the matrix generated from WTFAI to put the vertices back
      // 5. Run WTFAI on the Nominal vertices to generate a single transformation
      //    to do what we just did.

      Mat := TMatrix3D.Create;
      try
        PC.WTFAI_Fix(P, M, Mat); // Mat now contains values for A,B,C to unwrap
        Mat.Unitary;
        PC.CopyTo(RotCenter);
        Mat.Translate(-RotCenter.X, -RotCenter.Y, -RotCenter.Z);
//        Mat.Translate(PC);
        Mat.ZRot(-Mat.C);
        Mat.YRot(-Mat.B);
        Mat.XRot(-Mat.A);

        VisualTransform(Mat);
//        ApplyMatrix(Mat);

        // Now looking from Z determine the rotation to move M->P
        if M.X = 0 then begin
          if M.Y > 0 then
            Rot := PI/2
          else
            Rot := -PI/2;
        end else begin
          Rot := ArcTan(M.Y / M.X);
//          if M.X < 0 then
//            Rot := PI + Rot;
        end;

        Mat.Unitary;
        Mat.ZRot(Rot);
        VisualTransform(Mat);

        // Now apply the original transformation to the part
        Mat.Unitary;
        Mat.XRot(Mat.A);
        Mat.YRot(Mat.B);
        Mat.ZRot(Mat.C);
        Mat.Translate(RotCenter.X, RotCenter.Y, RotCenter.Z);
//        Mat.Translate(PC.X, PC.Y, PC.Z);

        VisualTransform(Mat);
//        ApplyMatrix(Mat);

        // Finally determine a single transformation that performs this whole thing.

        Part.Vertex[1, Normal[0]].WTFAI(Part.Vertex[1, Normal[3]], Part.Vertex[1, Normal[2]], PartMatrix);
      finally
        Mat.Free;
      end;
    end;

    atCylindrical : begin
      // Here the problem is very similar to Circular except the axis of rotation is
      // about several patches that each contain a centre of rotation (e.g. cylidrical patches)
      // The first job is determine the common centre of rotation.
      //
      // 1. Check each constraint contains an AxisOfRotation
      // 2. Determine the average BC of Each rotation
      // 3. Determine the averate
      //
      BRot := 0;
      CRot := 0;

      RotCenter.X := 0;
      RotCenter.Y := 0;
      RotCenter.Z := 0;

      for I := 0 to C.ProbePoint.ConstraintList.Count - 1 do begin
        if not C.ProbePoint.Constraints[I].HasAxisOfRotation then
          raise Exception.Create('Invalid use of cylindrical correction, all constraints must be cylindrical');
        C.ProbePoint.Constraints[I].BCAxisOfRotation;
        BRot := BRot + C.ProbePoint.Constraints[I].BRot;
        CRot := CRot + C.ProbePoint.Constraints[I].CRot;

        C.ProbePoint.Constraints[I].CenterOfRotation(Tmp);
        RotCenter.X := RotCenter.X + Tmp.X;
        RotCenter.Y := RotCenter.Y + Tmp.Y;
        RotCenter.Z := RotCenter.Z + Tmp.Z;
      end;

      BRot := BRot / C.ProbePoint.ConstraintList.Count;
      CRot := CRot / C.ProbePoint.ConstraintList.Count;
      RotCenter.X := RotCenter.X / C.ProbePoint.ConstraintList.Count;
      RotCenter.Y := RotCenter.Y / C.ProbePoint.ConstraintList.Count;
      RotCenter.Z := RotCenter.Z / C.ProbePoint.ConstraintList.Count;

      // Now BRot, CRot represent the plane of Rotation and RotCenter represents
      // the average center of rotation.
      // The job now is to find a point on this line that decribes a line perpendicular
      // to BC that intercepts the current mount point.  Its fairly easy to iterate a
      // solution but I think Ill stop here and see if I can find a formula.

      // So if we do a rotation and translation that centers on RotCenter with a BC transformation
      // that lays our normalized axis of rotation on the X axis
      //
      //             |      /
      //             |    /
      //             |  /
      //             |/__________   Axis of Rotation layed on X
      //          RotCenter
      //
      Mat := TMatrix3D.Create;
      try
        Mat.Translate(-RotCenter.X, -RotCenter.Y, -RotCenter.Z);
        VisualTransform(Mat);

        Mat.Unitary;
        Mat.XRot(0);
        Mat.ZRot(-CRot);
        Mat.YRot(-BRot);

        VisualTransform(Mat);


      // Now determine the angle subtended by this constraint and the result. Then
      // Rotate in the A axis to correct. NOTE P is Patch vertex closest, M is the mount point

        Mat.Unitary;
        A1 := P.Z / P.Y;
        A2 := M.Z / M.Y;

        Mat.XRot(ArcTan(A1) - ArcTan(A2));
        VisualTransform(Mat);

      // Now unwind the beginning transformation
        Mat.Unitary;
        Mat.YRot(BRot);
        Mat.ZRot(CRot);
        VisualTransform(Mat);

        Mat.Unitary;
        Mat.Translate(RotCenter.X, RotCenter.Y, RotCenter.Z);
        VisualTransform(Mat);

      // Tell the base matrix what we just did
        Part.Vertex[1, Normal[0]].WTFAI(Part.Vertex[1, Normal[3]], Part.Vertex[1, Normal[2]], PartMatrix);
//        Mount.Vertex[1, Normal[0]].WTFAI(Mount.Vertex[1, Normal[3]], Mount.Vertex[1, Normal[2]], UnityMatrix);
      finally
        Mat.Free;
      end;
    end;
  end;

  ApplyMatrix(nil);

  for I := 0 to C.ProbePoint.ConstraintList.Count - 1 do begin
    C.ProbePoint.Constraints[I].MountError;
  end;
end;


{ TStackingAxis }

constructor TStackingAxis.Create(aRulesFile : TConstraintRulesFile);
begin
  Rules := aRulesFile;
end;

destructor TStackingAxis.Destroy;
begin
  inherited;
end;

procedure TStackingAxis.ReadFromStrings(S: TQuoteTokenizer);
var Token : string;
begin
  S.ExpectedTokens(['stackingaxis', '=', '{']);
  Token := LowerCase(S.Read);
  while not S.EndOfStream do begin
    if Token = 'name' then
      Name := S.ReadStringAssign
    else if Token = 'po' then
      PO := Rules.ReadVertex(Rules.Part)
    else if Token = 'pbc' then
      PBC := Rules.ReadVertex(Rules.Part)
    else if Token = 'pa' then
      PA := Rules.ReadVertex(Rules.Part)
    else if Token = '}' then
      Exit
    else
      raise Exception.CreateFmt('Invalid token "%s"', [Token]);
    Token := LowerCase(S.Read);
  end;
  raise Exception.Create('Unexpected end of file');
end;

procedure TStackingAxis.GenerateVisual(Obj : TObject3D);
var F : TFace3D;
begin
  F := TFace3D.Create(Self, [PA, PO, PBC]);
  F.IsSpline := True;
  Obj.AddFace(F);
end;

procedure TStackingAxis.Clean;
begin
end;

{ TConstraint }

constructor TConstraint.Create(aRulesFile : TConstraintRulesFile);
begin
  Rules := aRulesFile;
  Geometry := TObject3D.Create(4);
  Patch := TVertexList.Create;
  ProbePoint := TProbePoint.Create(aRulesFile);
end;

destructor TConstraint.Destroy;
begin
  Geometry.Free;
  Patch.Free;
  ProbePoint.Free;
  inherited;
end;


function TConstraint.ReadToken(const aToken : string; S : TQuoteTokenizer) : Boolean;
begin
  Result := True;
  if aToken = 'name' then
    Name := S.ReadStringAssign
  else if aToken = 'po' then
    PO := Rules.ReadVertex(Geometry)
  else if aToken = 'pa' then
    PA := Rules.ReadVertex(Geometry)
  else if aToken = 'pb' then
    PB := Rules.ReadVertex(Geometry)
  else if aToken = 'probe' then
    ProbePoint.ReadFromStrings(S)
  else
    Result := False;
end;

procedure TConstraint.GenerateVisual;
begin
end;

procedure TConstraint.CheckParamters;
begin
end;

var TTT1, TTT2, TTT3, TTT4: Double;

function TConstraint.MountError : Double;
const FineTooth = 100;
      LargeNumber = 10000;
var I, J : Integer;
    C, N1, N2, N3 : Double;
    M : TVertex3D;
    P1, P2, P3, P4 : TVertex3D;
    PV1, PV2, PV3, PV4 : TVertex3D;
    I1, I2, I3, I4 : Integer;
    X1, Y1, Z1, X2, Y2, Z2 : Double;
    Tmp : TXYZ;
begin
{  M := Rules.Mount.Vertex[1, PResult];
  Result := 100000;
  for I := 0 to Patch.Count - 1 do begin
    C := M.DistanceTo(Patch[I]);
    if C < Result then begin
      PClosest := Patch[I];
      Result := C;
    end;
  end; }

  // Now looking for two points
  // P1. The closest
  // P2. The next closest

  M := Rules.Mount.Vertex[1, PResult];
  N1 := LargeNumber;
  N2 := LargeNumber;
  N3 := LargeNumber;
  P1 := nil;
  P2 := nil;
  P3 := nil;
  I1 := 0;
  I2 := 0;
  I3 := 0;

  for I := 0 to Patch.Count - 1 do begin
    C := M.DistanceTo(Patch[I]);
    if C < N1 then begin
      P2 := P1;
      I2 := I1;
      N2 := N1;

      P1 := Patch[I];
      I1 := I;
      N1 := C;
    end else if C < N2 then begin
      N3 := N2;
      I3 := I2;
      P3 := P2;

      N2 := C;
      P2 := Patch[I];
      I2 := I;
    end else if C < N3 then begin
      N3 := C;
      P3 := Patch[I];
      I3 := I;
    end;
  end;
  Result := N1;
  PClosest := P1;


  I4 := Patch.CompleteSquare(I1, I2, I3);

  if (I4 <> -1) and (Result > 0.001) then begin
    P4 := Patch[I4];
    TTT1 := P1.DistanceTo(P2);
    TTT2 := P1.DistanceTo(P3);
    TTT3 := P1.DistanceTo(P4);

    // Make P3 the diagonal
    if (TTT1 > TTT2) and (TTT1 > TTT3) then begin
    // P2 is the diagonal
      PV1 := P3;
      P3 := P2;
      P2 := PV1;
    end else if (TTT2 > TTT1) and (TTT2 > TTT3) then begin
    // P3 is the diagonal GOOD
    end else if (TTT3 > TTT1) and (TTT3 > TTT2) then begin
    // P4 is the diagonal
      PV1 := P3;
      P3 := P4;
      P4 := PV1;
    end else begin
      Exit;
    end;

    PV1 := Geometry.Vertex[0, P1.Index];
    PV2 := Geometry.Vertex[0, P2.Index];
    PV3 := Geometry.Vertex[0, P3.Index];
    PV4 := Geometry.Vertex[0, P4.Index];

    TTT1 := PV1.DistanceTo(PV2);
    TTT2 := PV1.DistanceTo(PV3);
    TTT3 := PV2.DistanceTo(PV3);
    TTT4 := PV1.DistanceTo(PV4);

    P1 := Geometry.Vertex[0, PVar]; // def array
    P2 := Geometry.Vertex[1, PVar]; // Trans array
    // Find the minimum Error

    N1 := LargeNumber;
    for J := 0 to FineTooth - 1 do begin
      X1 := PV1.X - ((PV1.X - PV4.X) / FineTooth * J);
      Y1 := PV1.Y - ((PV1.Y - PV4.Y) / FineTooth * J);
      Z1 := PV1.Z - ((PV1.Z - PV4.Z) / FineTooth * J);

      X2 := PV2.X - ((PV2.X - PV3.X) / FineTooth * J);
      Y2 := PV2.Y - ((PV2.Y - PV3.Y) / FineTooth * J);
      Z2 := PV2.Z - ((PV2.Z - PV3.Z) / FineTooth * J);
      for I := 0 to FineTooth - 1 do begin
        P1.X := X1 - ((X1 - X2) / FineTooth * I);
        P1.Y := Y1 - ((Y1 - Y2) / FineTooth * I);
        P1.Z := Z1 - ((Z1 - Z2) / FineTooth * I);

        Rules.PartMatrix.Transform(P1, P2);

        C := P2.DistanceTo(M);
        if C < N1 then begin
          P1.CopyTo(Tmp);
          N1 := C;
        end;
      end;
    end;
    P1.CopyFrom(Tmp);
    Rules.PartMatrix.Transform(P1, P2);
    PClosest := P2;
    Result := P2.DistanceTo(M);
  end;
end;

function TConstraint.NominalError : Double;
begin
//  Result := Geometry.Vertex[1, PO].DistanceTo(Rules.Mount.Vertex[1, PMount]);
  Result := Geometry.Vertex[1, PO].DistanceTo(Rules.Mount.Vertex[1, PResult]);
end;

var TTT : Integer;

function TConstraint.SetProbeResult(X, Y, Z : Double) : Double;
var I, J, Redo : Integer;
    Max, M : Double;
begin
  Rules.Mount.Vertex[0, PResult].X := X;
  Rules.Mount.Vertex[0, PResult].Y := Y;
  Rules.Mount.Vertex[0, PResult].Z := Z;
  Rules.ApplyMatrix(nil);
  MountError;
  Result := NominalError;
//  Result := PClosest.DistanceTo(Rules.Mount.Vertex[0, PResult]);
  Rules.Constrain(Self);

  if ProbePoint.ConstraintList.Count > 0 then begin
    for I := 0 to Rules.Retry do begin
      Max := MountError;
      Redo := -1;
      for J := 0 to ProbePoint.ConstraintList.Count - 1 do begin
        M := ProbePoint.Constraints[J].MountError;
        if M > Max then begin
          Redo := J;
          Max := M;
        end;
      end;
      if Max < Rules.Fit then begin
        TTT := 0;
        Exit;
      end;
{      if (I mod 4) = 0 then
        Rules.Constrain(Self) }
      if Redo = -1 then
        Rules.Constrain(Self)
      else
        Rules.Constrain(ProbePoint.Constraints[Redo]);
    end;
  end;

//  for J := 0 to ProbePoint.ConstraintList.Count - 1 do begin
//    Rules.Constrain(ProbePoint.Constraints[J]);
//  end;
end;

{ TCylinderConstraint }

procedure TCylinderConstraint.GenerateVisual;
var P1, P2, PM1, PTmp : TVertex3D;
    S : Double;
begin
  inherited;

  S := D / Rules.PatchPoints;
  Geometry.CreateCylinder(Self, Geometry.Vertex[1, PA], Geometry.Vertex[1, PB], Geometry.Vertex[1, PO], Radius, 32);
  Patch.Clear;
  Geometry.CreateCylinderPatch(Patch, Geometry.Vertex[1, PA], Geometry.Vertex[1, PB], Geometry.Vertex[1, PO], Radius, Rules.PatchPoints, S);
  Geometry.AddList(Patch);

  PM1 := TVertex3D.Create;
  P1 := TVertex3D.Create;
  P2 := TVertex3D.Create;

  PM1.Copy(Geometry.Vertex[1, PB]);     // End centre of cylinder
  PM1.Midpoint(Geometry.Vertex[1, PA]); // Centre of cylinder
  PM1.Subtract(Geometry.Vertex[1, PO]); // From origin
  P2.Copy(PM1); // Vector for end of probe
  PM1.Reverse;      // Now facing away
  P1.Copy(PM1); // Vector for start of probe
  PM1.Add(Geometry.Vertex[1, PO]);
//  PM.Unitary;

  P1.SetMagnitude(ProbePoint.Clearance);
  P1.Add(Geometry.Vertex[1, PO]);
  P2.SetMagnitude(ProbePoint.Through);
  P2.Add(Geometry.Vertex[1, PO]);

  PPath1 := Geometry.AddVertex(P1);
  PPath2 := Geometry.AddVertex(P2);
  PM := Geometry.AddVertex(PM1);

  Geometry.AddFace(TFace3D.Create(Self, [PPath1, PPath2]));

  PT := PB; // Used for timing
  PTmp := Geometry.Vertex[1, PO];
  PResult := Rules.Mount.AddVertex(PTmp.X, PTmp.Y, PTmp.Z);
  PMount := Rules.Mount.AddVertex(PTmp.X, PTmp.Y, PTmp.Z);
  PVar := Geometry.AddVertex(0, 0, 0);
  Rules.Mount.CreateMountPoint(Self, Geometry.Vertex[1, PO], Geometry.Vertex[1, PM], Geometry.Vertex[1, PT]);
end;

procedure TCylinderConstraint.ReadFromStrings(S: TQuoteTokenizer);
var Token : string;
begin
  S.ExpectedTokens(['cylinder', '=', '{']);
  D := 2;
  Token := LowerCase(S.Read);
  while not S.EndOfStream do begin
    if not ReadToken(Token, S) then begin
      if Token = 'r' then
        Radius := ReadDoubleAssign(S) //StrToFloat(S.ReadStringAssign)
      else if Token = 'd' then
        D := ReadDoubleAssign(S) //StrToFloat(S.Read) This was probably a mistake
      else if Token = '}' then
        Exit
      else
        raise Exception.CreateFmt('Invalid token "%s"', [Token]);
    end;
    Token := LowerCase(S.Read);
  end;
  raise Exception.Create('Unexpected end of file');
end;

function TCylinderConstraint.MountError : Double;
begin
  Result := inherited MountError;
end;

function TCylinderConstraint.NominalError : Double;
begin
  Result := inherited NominalError;
end;


procedure TCylinderConstraint.BCAxisOfRotation;
var Mat : TMatrix3D;
begin
  Mat := TMatrix3D.Create;
  try
    Geometry.Vertex[1, PA].WTFAI(Geometry.Vertex[1, PB], Geometry.Vertex[1, PO], Mat);
    BRot := Mat.B;
    CRot := Mat.C;
  finally
    Mat.Free;
  end;
end;

procedure TCylinderConstraint.CheckParamters;
begin
  if Radius = 0 then
    raise Exception.CreateFmt('Radius parameter not set for constraint %s', [Name]);
end;

procedure TCylinderConstraint.CenterOfRotation(var P1: TXYZ);
var P : TVertex3D;
begin
  P := TVertex3D.Create(Geometry.Vertex[1, PA]);
  P.Midpoint(Geometry.Vertex[1, PB]);
  P.CopyTo(P1);
  P.Free;
end;

function TCylinderConstraint.HasAxisOfRotation: Boolean;
begin
  Result := True;
end;

{ TSplineConstraint }

procedure TSplineConstraint.GenerateVisual;
begin
  inherited;
end;

procedure TSplineConstraint.ReadFromStrings(S: TQuoteTokenizer);
var Token : string;
begin
  S.ExpectedTokens(['spline', '=', '{']);
  Token := LowerCase(S.Read);
  while not S.EndOfStream do begin
    if not ReadToken(Token, S) then begin
      if Token = '}' then
        Exit
      else
        raise Exception.CreateFmt('Invalid token "%s"', [Token]);
    end;
    Token := LowerCase(S.Read);
  end;
  raise Exception.Create('Unexpected end of file');
end;

function TSplineConstraint.MountError : Double;
begin
  Result := inherited MountError;
end;

function TSplineConstraint.NominalError : Double;
begin
  Result := inherited NominalError;
end;

procedure TSplineConstraint.BCAxisOfRotation;
begin
  BRot := 0; CRot := 0;
end;

procedure TSplineConstraint.CenterOfRotation(var P1: TXYZ);
begin
  Rules.Mount.Vertex[1, PO].CopyTo(P1);
end;

function TSplineConstraint.HasAxisOfRotation: Boolean;
begin
  Result := False;
end;

{ TPlaneConstraint }

constructor TPlaneConstraint.Create(aRulesFile : TConstraintRulesFile);
begin
  inherited;
end;

destructor TPlaneConstraint.Destroy;
begin
  inherited;
end;

procedure TPlaneConstraint.GenerateVisual;
var P, P1, P2, PM1, PTmp : TVertex3D;
begin
  inherited;
  Patch.Clear;
  Geometry.AddFace(TFace3D.Create(Self, [PA, PB, PC, PD]));
  Geometry.CreateSimpleQuadPatch(Patch, Geometry.Vertex[1, PA], Geometry.Vertex[1, PB], Geometry.Vertex[1, PC], Geometry.Vertex[1, PD], Rules.PatchPoints);
  Geometry.AddList(Patch);

  P := TVertex3D.Create(Geometry.Vertex[1, PC]);
  PM1 := TVertex3D.Create(Geometry.Vertex[1, PA]);
  P1 := TVertex3D.Create;
  P2 := TVertex3D.Create;

  PM1.Subtract(Geometry.Vertex[1, PB]);     // Origin on second point
  P.Subtract(Geometry.Vertex[1, PB]);      // third point origin on PB
  P1.Copy(PM1);
  PM1.CrossProduct(P);  // Generate a vector perpendicular to the described plane
  PM1.Unitary;          // but only one unit long
  PM1.Add(Geometry.Vertex[1, PO]);          // Now put it back relative

  P1.CrossProduct(P); // Probe start position
  P1.SetMagnitude(1);
  P2.Copy(P1);
  P2.Reverse;
  P1.SetMagnitude(ProbePoint.Clearance);
  P2.SetMagnitude(ProbePoint.Through);

  P1.Add(Geometry.Vertex[1, PO]);
  P2.Add(Geometry.Vertex[1, PO]);

  PPath1 := Geometry.AddVertex(P1);
  PPath2 := Geometry.AddVertex(P2);
  PM := Geometry.AddVertex(PM1);

  Geometry.AddFace(TFace3D.Create(Self, [PPath1, PPath2]));


  PTmp := Geometry.Vertex[1, PO];
  PResult := Rules.Mount.AddVertex(PTmp.X, PTmp.Y, PTmp.Z);
  PMount := Rules.Mount.AddVertex(PTmp.X, PTmp.Y, PTmp.Z);
  PVar := Geometry.AddVertex(0, 0, 0);
  Rules.Mount.CreateMountPoint(Self, Geometry.Vertex[1, PO], Geometry.Vertex[1, PM], Geometry.Vertex[1, PT]);

  P.Free;

  PT := PA; // Just used for timing the mount point
//  Rules.Mount.Vertex[0, PResult].Copy(Geometry.Vertex[1, PO]);
end;

procedure TPlaneConstraint.ReadFromStrings(S: TQuoteTokenizer);
var Token : string;
begin
  S.ExpectedTokens(['plane', '=', '{']);
  Token := LowerCase(S.Read);
  while not S.EndOfStream do begin
    if not ReadToken(Token, S) then begin
      if Token = 'pc' then
        PC := Rules.ReadVertex(Geometry)
      else if Token = 'pd' then
        PD := Rules.ReadVertex(Geometry)
      else if Token = '}' then
        Exit
      else
        raise Exception.CreateFmt('Invalid token "%s"', [Token]);
    end;
    Token := LowerCase(S.Read);
  end;
  raise Exception.Create('Unexpected end of file');
end;

function TPlaneConstraint.MountError : Double;
begin
  Result := inherited MountError;
end;

function TPlaneConstraint.NominalError : Double;
begin
  Result := inherited NominalError;
end;

{ TProbeOperation }

constructor TProbePoint.Create(aRulesFile : TConstraintRulesFile);
begin
  inherited Create;
  ConstraintList := TList.Create;
  RulesFile := aRulesFile;
end;

destructor TProbePoint.Destroy;
begin
  ConstraintList.Free;
  inherited;
end;

function TProbePoint.GetConstraint(Index: Integer): TConstraint;
begin
  Result := TConstraint(ConstraintList[Index]);
end;

procedure TProbePoint.ReadFromStrings(S: TQuoteTokenizer);
var Token : string;

  procedure ReadAdjust;
  begin
    Token := LowerCase(S.ReadStringAssign);
    if Token = 'linear' then
      Adjust := atLinear
    else if Token = 'circular' then
      Adjust := atCircular
    else if Token = 'cylindrical' then
      Adjust := atCylindrical
    else
      raise Exception.CreateFmt('Invalid Adjust "%s"', [Token]);
  end;

  procedure ReadConstraints;
  var Line, Token : string;
  begin
    S.ExpectedToken('=');
    Line := Trim(S.ReadLine);
    Token := ReadDelimited(Line, ',');
    while Token <> '' do begin
      Self.ConstraintList.Add(RulesFile.FindConstraint(Token));
      Token := ReadDelimited(Line, ',');
    end;
  end;

begin
  S.ExpectedTokens(['=', '{']);

  Token := LowerCase(S.Read);

  while not S.EndOfStream do begin
    if Token = 'constraints' then
      ReadConstraints
    else if Token = 'adjust' then
      ReadAdjust
    else if Token = 'primaryconstraint' then
      PrimaryConstraint := RulesFile.FindConstraint(S.ReadStringAssign)
    else if Token = 'clearance' then
      Clearance := ReadDoubleAssign(S)
    else if Token = 'through' then
      Through := ReadDoubleAssign(S)
    else if Token = '}' then
      Exit
    else
      raise Exception.CreateFmt('Invalid token "%s"', [Token]);

    Token := LowerCase(S.Read);
  end;
  raise Exception.Create('Unexpected end of file');
end;


procedure TPlaneConstraint.BCAxisOfRotation;
begin
  BRot := 0; CRot := 0;
end;

procedure TPlaneConstraint.CenterOfRotation(var P1: TXYZ);
begin
  Rules.Mount.Vertex[1, PO].CopyTo(P1);
end;

function TPlaneConstraint.HasAxisOfRotation: Boolean;
begin
  Result := False;
end;

end.

