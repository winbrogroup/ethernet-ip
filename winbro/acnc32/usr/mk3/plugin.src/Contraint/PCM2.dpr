library PCM2;


uses
  Forms,
  SysUtils,
  DefineForm in 'DefineForm.pas' {Define},
  Matrix3D in 'Matrix3D.pas',
  ConstraintRules in 'ConstraintRules.pas';

{$R *.RES}

procedure Reset; stdcall;
begin
  Rules.Reset;
  if Assigned(Define) then
    Define.GeneralUpdate;
end;

procedure Show; stdcall;
begin
  if not Assigned(Define) then
    Define := TDefine.Create(Application);

  Define.Show;
  Define.UpdateConstraintList;
end;

procedure Close; stdcall;
begin
  if Assigned(Define) then begin
    if Define.Visible then
      Define.Close;
    Define.Free;
    Define := nil;
  end;
end;

procedure LoadConfig(aFile : PChar; Sqr, Retry : Integer; Fit : Double); stdcall;
begin
  Rules.LoadFromFile(aFile);
  Rules.ReadFromStrings(Sqr, Retry, Fit);
  if Assigned(Define) then
    Define.UpdateConstraintList;
end;

procedure SetProbePoint(aName : PChar; X, Y, Z : Double); stdcall;
var C : TConstraint;
    A : Double;
begin
  C := Rules.FindConstraint(aName);
  A := C.SetProbeResult(X, Y, Z);
  if Assigned(Define) then begin
    Define.LogMemo.Lines.Add(aName + Format(' Error = %.4g', [A]));
    Define.GeneralUpdate;
    Define.Report1Click(Application);
  end;
end;

procedure GetStackingAxis(var PO, PBC, PA : TXYZ); stdcall;
var S : TStackingAxis;
begin
  S := Rules.StackingAxis;
  Rules.Part.Vertex[0, S.PO].CopyTo(PO);
  Rules.Part.Vertex[0, S.PBC].CopyTo(PBC);
  Rules.Part.Vertex[0, S.PA].CopyTo(PA);
end;

procedure GetAdjustedStackingAxis(var PO, PBC, PA : TXYZ); stdcall;
var S : TStackingAxis;
begin
  S := Rules.StackingAxis;
  Rules.Part.Vertex[1, S.PO].CopyTo(PO);
  Rules.Part.Vertex[1, S.PBC].CopyTo(PBC);
  Rules.Part.Vertex[1, S.PA].CopyTo(PA);
end;

procedure GetProbeVector(aName : PChar; var P1, P2 : TXYZ); stdcall;
var C : TConstraint;
begin
  C := Rules.FindConstraint(aName);
  C.Geometry.Vertex[1, C.PPath1].CopyTo(P1);
  C.Geometry.Vertex[1, C.PPath2].CopyTo(P2);
end;

function GetConstraintNames(aNames : PChar; Maxlen : Integer) : Integer; stdcall;
var I : Integer;
    S : string;
begin
  S := '';
  for I := 0 to Rules.ConstraintCount - 1 do begin
    S := S + Rules.Constraints[I].Name + #$0d + #$0a;
  end;

  StrPLCopy(aNames, S, MaxLen);
  Result := Rules.ConstraintCount;
end;

procedure GetProbePoint(aName : PChar; var P1 : TXYZ); stdcall;
var C : TConstraint;
begin
  C := Rules.FindConstraint(aName);
  C.Geometry.Vertex[0, C.PO].CopyTo(P1);
end;

procedure GetFitError(var Nominal, Mount :  array of Double; var Max : Integer); stdcall;
var I : Integer;
begin
  if Max > Rules.ConstraintCount then
    Max := Rules.ConstraintCount;

  for I := 0 to Max - 1 do begin
    Nominal[I] := Rules.Constraints[I].NominalError;
    Mount[I] := Rules.Constraints[I].MountError;
  end;
end;

exports
  Reset,
  LoadConfig,
  SetProbePoint,
  GetStackingAxis,
  GetAdjustedStackingAxis,
  GetProbeVector,
  GetProbePoint,
  GetConstraintNames,
  GetFitError,
  Show,
  Close;
begin
end.
