unit PCMPlugin;

interface

uses Classes, SysUtils, Windows;

type
  TXYZ = record
    X, Y, Z : Double;
  end;

  TPCMReset = procedure; stdcall;
  TPCMShow = procedure; stdcall;
  TPCMClose = procedure; stdcall;
  TPCMLoadConfig = procedure(aName : PChar; Sqr, Retry : Integer; Fit : Double); stdcall;
//  TPCMLoadConfig = procedure (aFile : PChar); stdcall;
  TPCMSetProbePoint = procedure (C : PChar; X, Y, Z : Double); stdcall;
  TPCMGetStackingAxis = procedure (var P1, P2, P3 : TXYZ); stdcall;
  TPCMGetAdjustedStackingAxis = procedure (var P1, P2, P3 : TXYZ); stdcall;
  TPCMGetProbeVector = procedure (aName : PChar; var P1, P2 : TXYZ); stdcall;
  TPCMGetConstraintNames = function(aNames : PChar; Maxlen : Integer) : Integer; stdcall;
  TPCMGetProbePoint = procedure(aNAme : PChar; var P1 : TXYZ); stdcall;
  TPCMGetFitError = procedure (var Nominal, Mount :  array of Double; var Max : Integer); stdcall;

  TPCMPlugin = class(TObject)
  private
    HLib : HModule;
  public
    Reset : TPCMReset;
    Show : TPCMShow;
    Close : TPCMClose;
    LoadConfig : TPCMLoadConfig;
    SetProbePoint : TPCMSetProbePoint;
    GetStackingAxis : TPCMGetStackingAxis;
    GetAdjustedStackingAxis : TPCMGetAdjustedStackingAxis;
    GetProbeVector : TPCMGetProbeVector;
    GetConstraintNames : TPCMGetConstraintNames;
    GetProbePoint : TPCMGetProbePoint;
    GetFitError : TPCMGetFitError;

    constructor Create(aPathToPCM : string);
    destructor Destroy; override;
  end;

implementation

constructor TPCMPlugin.Create(aPathToPCM : string);
  function GetProcAddressX(aHLib : HModule; aName : PChar) : FARPROC;
  begin
    Result := GetProcAddress(aHLib, aName);
    if Result = nil then
      raise Exception.CreateFmt('PCM does not export method "%s"', [aName]);
  end;

begin
  HLib := LoadLibrary(PChar(aPathToPCM));
  if HLib = 0 then
    raise Exception.CreateFmt('Cannot find ' + aPathToPCM + ' %d', [GetLastError]);


  Reset := GetProcAddressX(HLib, 'Reset');
  Show := GetProcAddressX(HLib, 'Show');
  Close := GetProcAddressX(HLib, 'Close');
  LoadConfig := GetProcAddressX(HLib, 'LoadConfig');
  SetProbePoint := GetProcAddressX(HLib, 'SetProbePoint');
  GetStackingAxis := GetProcAddressX(HLib, 'GetStackingAxis');
  GetAdjustedStackingAxis := GetProcAddressX(HLib, 'GetAdjustedStackingAxis');
  GetProbeVector := GetProcAddressX(HLib, 'GetProbeVector');
  GetProbePoint := GetProcAddressX(HLib, 'GetProbePoint');
  GetConstraintNames := GetProcAddressX(HLib, 'GetConstraintNames');
  GetFitError := GetProcAddressX(HLib, 'GetFitError');
end;

destructor TPCMPlugin.Destroy;
begin
  if HLib <> 0 then
    FreeLibrary(HLib);
  inherited;
end;


end.
