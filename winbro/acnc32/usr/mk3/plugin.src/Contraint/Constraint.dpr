program Constraint;

uses
  Forms,
  Matrix3D in 'Matrix3D.pas',
  DefineForm in 'DefineForm.pas' {Define},
  ConstraintRules in 'ConstraintRules.pas',
  ProbePoint in 'ProbePoint.pas' {AddProbeForm};

{$R *.RES}

begin
  Application.Initialize;
  Application.CreateForm(TDefine, Define);
  Application.CreateForm(TAddProbeForm, AddProbeForm);
  Application.Run;
end.
