unit ProbePoint;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls;

type
  TAddProbeForm = class(TForm)
    ConstraintEdit: TEdit;
    Label1: TLabel;
    Panel1: TPanel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    XEdit: TEdit;
    YEdit: TEdit;
    ZEdit: TEdit;
    procedure BitBtn1Click(Sender: TObject);
  private
  public
    X, Y, Z : Double;
    Constraint : string;
  end;

var
  AddProbeForm: TAddProbeForm;

implementation

{$R *.DFM}

procedure TAddProbeForm.BitBtn1Click(Sender: TObject);
begin
  X := StrToFloat(XEdit.Text);
  Y := StrToFloat(YEdit.Text);
  Z := StrToFloat(ZEdit.Text);
  Constraint := ConstraintEdit.Text;
end;

end.
