object Form2: TForm2
  Left = 401
  Top = 484
  Width = 444
  Height = 235
  Caption = 'Form2'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Button1: TButton
    Left = 24
    Top = 40
    Width = 75
    Height = 25
    Caption = 'Show'
    TabOrder = 0
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 112
    Top = 40
    Width = 75
    Height = 25
    Caption = 'Hide'
    TabOrder = 1
    OnClick = Button2Click
  end
  object BitBtn4: TBitBtn
    Left = 112
    Top = 8
    Width = 75
    Height = 25
    Caption = 'Load'
    TabOrder = 2
    OnClick = BitBtn4Click
  end
  object BitBtn5: TBitBtn
    Left = 24
    Top = 8
    Width = 75
    Height = 25
    Caption = 'Reset'
    TabOrder = 3
    OnClick = BitBtn5Click
  end
  object ComboBox1: TComboBox
    Left = 16
    Top = 104
    Width = 145
    Height = 21
    ItemHeight = 0
    TabOrder = 4
    Text = 'ComboBox1'
  end
  object Memo1: TMemo
    Left = 192
    Top = 8
    Width = 233
    Height = 185
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Courier New'
    Font.Style = []
    ParentFont = False
    TabOrder = 5
  end
  object OpenDialog: TOpenDialog
    Left = 120
    Top = 152
  end
end
