object Define: TDefine
  Left = 419
  Top = 113
  BorderStyle = bsDialog
  Caption = 'Define'
  ClientHeight = 449
  ClientWidth = 783
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -16
  Font.Name = 'Comic Sans MS'
  Font.Style = []
  Menu = MainMenu1
  OldCreateOrder = False
  Position = poMainFormCenter
  OnCreate = FormCreate
  OnPaint = FormPaint
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 23
  object PBZ: TPaintBox
    Left = 56
    Top = 16
    Width = 193
    Height = 193
  end
  object PBX: TPaintBox
    Left = 256
    Top = 24
    Width = 105
    Height = 105
  end
  object PBY: TPaintBox
    Left = 40
    Top = 272
    Width = 105
    Height = 105
  end
  object PBISO: TPaintBox
    Left = 232
    Top = 272
    Width = 105
    Height = 105
  end
  object ToolPanel: TPanel
    Left = 508
    Top = 0
    Width = 275
    Height = 449
    Align = alRight
    TabOrder = 0
    object SpeedButton1: TSpeedButton
      Left = 12
      Top = 168
      Width = 23
      Height = 22
      Caption = 'R'
      OnClick = SpeedButton1Click
    end
    object XLab: TLabel
      Left = 48
      Top = 288
      Width = 10
      Height = 23
      Caption = '0'
    end
    object YLab: TLabel
      Left = 48
      Top = 336
      Width = 10
      Height = 23
      Caption = '0'
    end
    object ZLab: TLabel
      Left = 48
      Top = 384
      Width = 10
      Height = 23
      Caption = '0'
    end
    object Label1: TLabel
      Left = 96
      Top = 368
      Width = 47
      Height = 23
      Caption = 'Label1'
    end
    object SpeedButton2: TSpeedButton
      Left = 224
      Top = 328
      Width = 23
      Height = 22
      OnClick = SpeedButton2Click
    end
    object Label2: TLabel
      Left = 96
      Top = 400
      Width = 50
      Height = 23
      Caption = 'Label2'
    end
    object UDA: TUpDown
      Left = 8
      Top = 280
      Width = 30
      Height = 40
      Min = -10000
      Max = 10000
      TabOrder = 0
      OnClick = UDClick
    end
    object UDB: TUpDown
      Left = 8
      Top = 328
      Width = 30
      Height = 40
      Min = -10000
      Max = 10000
      TabOrder = 1
      OnClick = UDClick
    end
    object UDC: TUpDown
      Left = 8
      Top = 376
      Width = 30
      Height = 40
      Min = -10000
      Max = 10000
      TabOrder = 2
      OnClick = UDClick
    end
    object UDScale: TUpDown
      Left = 8
      Top = 216
      Width = 30
      Height = 40
      Min = 1
      Max = 10000
      Position = 20
      TabOrder = 3
      OnClick = UDClick
    end
    object LogMemo: TMemo
      Left = 1
      Top = 1
      Width = 273
      Height = 152
      Align = alTop
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Comic Sans MS'
      Font.Style = []
      ParentFont = False
      TabOrder = 4
    end
    object ConstraintList: TListBox
      Left = 136
      Top = 168
      Width = 113
      Height = 153
      ItemHeight = 23
      TabOrder = 5
      OnClick = ConstraintListClick
    end
    object Button1: TButton
      Left = 176
      Top = 384
      Width = 75
      Height = 25
      Caption = 'Close'
      TabOrder = 6
      OnClick = Button1Click
    end
  end
  object MainMenu1: TMainMenu
    Left = 88
    Top = 104
    object File1: TMenuItem
      Caption = 'File'
      object Open1: TMenuItem
        Caption = 'Open'
        OnClick = Open1Click
      end
      object Save1: TMenuItem
        Caption = 'Save'
      end
    end
    object Edit1: TMenuItem
      Caption = 'Edit'
      object Addproberesult1: TMenuItem
        Caption = 'Add probe result'
        OnClick = Addproberesult1Click
      end
      object Report1: TMenuItem
        Caption = 'Report'
        OnClick = Report1Click
      end
      object Reset1: TMenuItem
        Caption = 'Reset'
        OnClick = Reset1Click
      end
    end
  end
  object OpenDialog: TOpenDialog
    InitialDir = 'c:\acnc32\profile'
    Left = 288
    Top = 128
  end
end
