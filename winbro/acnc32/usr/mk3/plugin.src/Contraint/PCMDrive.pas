unit PCMDrive;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, PCMPlugin, Buttons;

type
  TForm2 = class(TForm)
    Button1: TButton;
    Button2: TButton;
    BitBtn4: TBitBtn;
    BitBtn5: TBitBtn;
    ComboBox1: TComboBox;
    Memo1: TMemo;
    OpenDialog: TOpenDialog;
    procedure Button1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure BitBtn4Click(Sender: TObject);
    procedure BitBtn5Click(Sender: TObject);
  private
    PCM : TPCMPlugin;
    procedure UpdateMemo;
  public
  end;

var
  Form2: TForm2;

implementation

{$R *.DFM}

procedure TForm2.Button1Click(Sender: TObject);
begin
  PCM.Show
end;

procedure TForm2.FormCreate(Sender: TObject);
begin
  PCM := TPCMPlugin.Create('c:\profiles\hsd6mk3\profile\lib\pcm.dll');
end;

procedure TForm2.Button2Click(Sender: TObject);
begin
  PCM.Close;
end;

procedure TForm2.BitBtn4Click(Sender: TObject);
var Stuff : array [0..1023] of Char;
begin
  if Self.OpenDialog.Execute then begin
    PCM.LoadConfig(PChar(OpenDialog.Filename), 100, 200, 0.002);
    PCM.GetConstraintNames(Stuff, 1024);
    ComboBox1.Items.Text := Stuff;
    ComboBox1.ItemIndex := 0;
end;
end;

procedure TForm2.BitBtn5Click(Sender: TObject);
begin
  PCM.Reset;
end;

procedure TForm2.UpdateMemo;
var Mount, Nominal : array [0..7] of Double;
    I : Integer;
begin
  I := 7;
  PCM.GetFitError(Nominal, Mount, I);
  for I := 0 to I do begin
    Memo1.Lines.Add(Format('%d : M%.4g N%.4g', [I, Mount[I], Nominal[I]]));
  end;
end;

end.
