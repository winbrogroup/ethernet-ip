unit DefineForm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Matrix3D, ExtCtrls, StdCtrls, ComCtrls, Buttons, Menus, ConstraintRules,
  ProbePoint, SyncObjs;

type
  TDefine = class(TForm)
    ToolPanel: TPanel;
    UDA: TUpDown;
    UDB: TUpDown;
    UDC: TUpDown;
    SpeedButton1: TSpeedButton;
    XLab: TLabel;
    YLab: TLabel;
    ZLab: TLabel;
    MainMenu1: TMainMenu;
    File1: TMenuItem;
    Open1: TMenuItem;
    Save1: TMenuItem;
    Edit1: TMenuItem;
    UDScale: TUpDown;
    PBZ: TPaintBox;
    PBX: TPaintBox;
    PBY: TPaintBox;
    PBISO: TPaintBox;
    Report1: TMenuItem;
    LogMemo: TMemo;
    Addproberesult1: TMenuItem;
    Reset1: TMenuItem;
    OpenDialog: TOpenDialog;
    Label1: TLabel;
    ConstraintList: TListBox;
    SpeedButton2: TSpeedButton;
    Label2: TLabel;
    Button1: TButton;
    procedure FormCreate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure UDClick(Sender: TObject; Button: TUDBtnType);
    procedure SpeedButton1Click(Sender: TObject);
    procedure Open1Click(Sender: TObject);
    procedure ConstraintListClick(Sender: TObject);
    procedure FormPaint(Sender: TObject);
    procedure Report1Click(Sender: TObject);
    procedure Addproberesult1Click(Sender: TObject);
    procedure Reset1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    DrawMatrix : TMatrix3D;
    Scale : Double;
    Lock : TCriticalSection;
    procedure UpdateLabels;
    procedure BGMoveTo(V : TVertex3D; ISO : Boolean);
    procedure BGLineTo(V : TVertex3D; ISO : Boolean);
    procedure BGPixel(V : TVertex3D; ISO : Boolean);
  public
    BGZImage, BGXImage, BGYImage, BGISOImage : TBitMap;
    procedure UpdateConstraintList;
    procedure GeneralUpdate;
  end;

var                                                       
  Define: TDefine;
  Rules : TConstraintRulesFile;

implementation

{$R *.DFM}


procedure TDefine.FormCreate(Sender: TObject);
begin
  Lock := TCriticalSection.Create;
  BGZImage := TBitMap.Create;
  BGZImage.Width := PBZ.Width;
  BGZImage.Height := PBZ.Height;
  BGXImage := TBitMap.Create;
  BGXImage.Width := PBZ.Width;
  BGXImage.Height := PBZ.Height;
  BGYImage := TBitMap.Create;
  BGYImage.Width := PBZ.Width;
  BGYImage.Height := PBZ.Height;
  BGISOImage := TBitMap.Create;
  BGISOImage.Width := PBZ.Width;
  BGISOImage.Height := PBZ.Height;
  DrawMatrix := TMatrix3D.Create;

  BGZImage.Canvas.Pen.Width := 1;
  BGXImage.Canvas.Pen.Width := 1;
  BGYImage.Canvas.Pen.Width := 1;
  BGISOImage.Canvas.Pen.Width := 1;

  GeneralUpdate;
end;

procedure TDefine.BGPixel(V : TVertex3D; ISO : Boolean);
var w2, h2 : Integer;
    Y, Z, X : Double;
begin
  w2 := (BGZImage.Width div 2);
  h2 := (BGZImage.Height div 2);

  if not ISO then begin
    X := (V.X - Rules.Mount.Vertex[1, Rules.ViewC].X) * Scale;
    Y := (V.Y - Rules.Mount.Vertex[1, Rules.ViewC].Y) * Scale;
    Z := (V.Z - Rules.Mount.Vertex[1, Rules.ViewC].Z) * Scale;
    with BGZImage.Canvas do
      Pixels[Round(X) + w2, PBZ.Height - Round(Y) - h2] := clBlack;
    with BGXImage.Canvas do
      Pixels[Round(Z) + w2, PBX.Height - Round(Y) - h2] := clBlack;
    with BGYImage.Canvas do
      Pixels[Round(X) + w2, PBY.Height - Round(Z) - h2] := clBlack;
  end else begin
    X := (V.X - Rules.Mount.Vertex[1, Rules.ViewC].X) * Scale;
    Y := (V.Y - Rules.Mount.Vertex[1, Rules.ViewC].Y) * Scale;
    with BGISOImage.Canvas do
      Pixels[Round(X) + w2, PBZ.Height - Round(Y) - h2] := clBlack;
  end;
end;

procedure TDefine.BGLineTo(V : TVertex3D; ISO : Boolean);
var w2, h2 : Integer;
    Y, Z, X : Double;
begin
  w2 := (BGZImage.Width div 2);
  h2 := (BGZImage.Height div 2);
  if not ISO then begin
    X := (V.X - Rules.Mount.Vertex[1, Rules.ViewC].X) * Scale;
    Y := (V.Y - Rules.Mount.Vertex[1, Rules.ViewC].Y) * Scale;
    Z := (V.Z - Rules.Mount.Vertex[1, Rules.ViewC].Z) * Scale;
    with BGZImage.Canvas do
      LineTo(Round(X) + w2, PBZ.Height - Round(Y) - h2);
    with BGXImage.Canvas do
      LineTo(Round(Z) + w2, PBX.Height - Round(Y) - h2);
    with BGYImage.Canvas do
      LineTo(Round(X) + w2, PBY.Height - Round(Z) - h2);
  end else begin
    X := (V.X - Rules.Mount.Vertex[1, Rules.ViewC].X) * Scale;
    Y := (V.Y - Rules.Mount.Vertex[1, Rules.ViewC].Y) * Scale;
    with BGISOImage.Canvas do
      LineTo(Round(X) + w2, PBZ.Height - Round(Y) - h2);
  end;
end;

procedure TDefine.BGMoveTo(V : TVertex3D; ISO : Boolean);
var w2, h2 : Integer;
    Y, Z, X : Double;
begin
  w2 := (BGZImage.Width div 2);
  h2 := (BGZImage.Height div 2);
  if not ISO then begin
    X := (V.X - Rules.Mount.Vertex[1, Rules.ViewC].X) * Scale;
    Y := (V.Y - Rules.Mount.Vertex[1, Rules.ViewC].Y) * Scale;
    Z := (V.Z - Rules.Mount.Vertex[1, Rules.ViewC].Z) * Scale;
    with BGZImage.Canvas do
      MoveTo(Round(X) + w2, PBZ.Height - Round(Y) - h2);
    with BGXImage.Canvas do
      MoveTo(Round(Z) + w2, PBX.Height - Round(Y) - h2);
    with BGYImage.Canvas do
      MoveTo(Round(X) + w2, PBY.Height - Round(Z) - h2);
  end else begin
    X := (V.X - Rules.Mount.Vertex[1, Rules.ViewC].X) * Scale;
    Y := (V.Y - Rules.Mount.Vertex[1, Rules.ViewC].Y) * Scale;
//    Z := (V.Z - Rules.Part.Vertex[2, Rules.ViewC].Z) * Scale;
    with BGISOImage.Canvas do
      MoveTo(Round(X) + w2, PBZ.Height - Round(Y) - h2);
  end;
end;

const Divisor = 40;

procedure TDefine.GeneralUpdate;
  procedure DrawFace(P : TObject3D; F : TFace3D; ISO : Boolean);
  var I : Integer;
  begin
    if not ISO then begin
      BGMoveTo(P.Vertex[1, F[0]], ISO);
      for I := 1 to F.Count - 1 do begin
        BGLineTo(P.Vertex[1, F[I]], ISO);
      end;
      if not F.IsSpline then
        BGLineTo(P.Vertex[1, F[0]], ISO);
    end else begin
      BGMoveTo(P.Vertex[1, F[0]], ISO);
      for I := 1 to F.Count - 1 do begin
        BGLineTo(P.Vertex[1, F[I]], ISO);
      end;
      if not F.IsSpline then
        BGLineTo(P.Vertex[1, F[0]], ISO);
    end;
  end;

  procedure DrawScene(WithISO : Boolean);
  var I, J : Integer;
      SelectedConstraint : TObject;
      C : TConstraint;
  begin
    for J := 0 to Rules.ConstraintCount - 1 do begin
      C := Rules.Constraints[J];
      for I := 0 to C.Geometry.VertexCount - 1 do
        BGPixel(C.Geometry.Vertex[1, I], WithISO);
    end;

    BGZImage.Canvas.Pen.Color := clGreen;
    BGXImage.Canvas.Pen.Color := clGreen;
    BGYImage.Canvas.Pen.Color := clGreen;
    BGISOImage.Canvas.Pen.Color := clGreen;

    if ConstraintList.ItemIndex <> -1 then
      SelectedConstraint := ConstraintList.Items.Objects[ConstraintList.ItemIndex]
    else
      SelectedConstraint := nil;

    BGZImage.Canvas.Pen.Color := clBlack;
    BGXImage.Canvas.Pen.Color := clBlack;
    BGYImage.Canvas.Pen.Color := clBlack;
    BGISOImage.Canvas.Pen.Color := clBlack;

    for I := 0 to Rules.ConstraintCount - 1 do begin
      if Rules.Constraints[I] <> SelectedConstraint then begin
        with Rules.Constraints[I] do
          for J := 0 to Geometry.Count - 1 do begin
            if WithISO then
              DrawFace(Geometry, Geometry[J], True)
            else
              DrawFace(Geometry, Geometry[J], False);
          end;
      end;
    end;


    BGZImage.Canvas.Pen.Color := clRed;
    BGXImage.Canvas.Pen.Color := clRed;
    BGYImage.Canvas.Pen.Color := clRed;
    BGISOImage.Canvas.Pen.Color := clRed;

    for I := 0 to Rules.ConstraintCount - 1 do begin
      if Rules.Constraints[I] = SelectedConstraint then begin
        with Rules.Constraints[I] do
          for J := 0 to Geometry.Count - 1 do begin
            if WithISO then
              DrawFace(Geometry, Geometry[J], True)
            else
              DrawFace(Geometry, Geometry[J], False);
          end;
      end;
    end;


    BGZImage.Canvas.Pen.Color := clBlue;
    BGXImage.Canvas.Pen.Color := clBlue;
    BGYImage.Canvas.Pen.Color := clBlue;
    BGISOImage.Canvas.Pen.Color := clBlue;

    for I := 0 to Rules.Part.Count - 1 do
      DrawFace(Rules.Part, Rules.Part[I], WithISO);

    for I := 0 to Rules.Mount.Count - 1 do
      DrawFace(Rules.Mount, Rules.Mount[I], WithISO)
  end;

  procedure FrameRect(Image : TBitMap);
  begin
    Image.Canvas.PolyLine([ Point(0,0),
                          Point(Image.Width - 1, 0),
                          Point(Image.Width - 1, Image.Height - 1),
                          Point(0, Image.Height - 1)]);
  end;

begin
  UpdateLabels;
  if not Rules.Loaded then
    Exit;

  Scale := UDScale.Position / 2;

  Rules.ApplyMatrix(nil);

  BGZImage.Canvas.Brush.Color := clWhite;
  BGZImage.Canvas.FillRect(PBZ.BoundsRect);
  BGXImage.Canvas.Brush.Color := clWhite;
  BGXImage.Canvas.FillRect(PBZ.BoundsRect);
  BGYImage.Canvas.Brush.Color := clWhite;
  BGYImage.Canvas.FillRect(PBZ.BoundsRect);
  BGISOImage.Canvas.Brush.Color := clWhite;
  BGISOImage.Canvas.FillRect(PBZ.BoundsRect);

  BGZImage.Canvas.Pen.Color := clBlack;
  BGXImage.Canvas.Pen.Color := clBlack;
  BGYImage.Canvas.Pen.Color := clBlack;
  BGISOImage.Canvas.Pen.Color := clBlack;

  { ----------------- Draw Orthoganal }
  DrawScene(False);

  { ----------------- Rotate }
  DrawMatrix.Unitary;
  DrawMatrix.XRot(UDA.Position / Divisor);
  DrawMatrix.YRot(UDB.Position / Divisor);
  DrawMatrix.ZRot(UDC.Position / Divisor);

  Lock.Enter;
  try
    Rules.VisualTransform(DrawMatrix);
  { ----------------- Draw Isometric }
    DrawScene(True);
  finally
    Rules.ApplyMatrix(nil);
    Lock.Leave;
  end;

  FrameRect(BGZImage);
  FrameRect(BGXImage);
  FrameRect(BGYImage);
  FrameRect(BGISOImage);

  PBZ.Canvas.Draw(0, 0, BGZImage);
  PBX.Canvas.Draw(0, 0, BGXImage);
  PBY.Canvas.Draw(0, 0, BGYImage);
  PBISO.Canvas.Draw(0, 0, BGISOImage);
end;

procedure TDefine.UDClick(Sender: TObject; Button: TUDBtnType);
begin
  GeneralUpdate;
end;

procedure TDefine.UpdateLabels;
begin
  ZLab.Caption := Format('A%f', [DrawMatrix.SC]);
  YLab.Caption := Format('B%f', [DrawMatrix.SB]);
  XLab.Caption := Format('C%f', [DrawMatrix.SA]);
  Label1.Caption := IntToStr(AllocMemSize);
  Label2.Caption := FloatToStr(Rules.Fit);
end;


procedure TDefine.FormResize(Sender: TObject);
var Tmp :Integer;
begin
  Tmp := ClientWidth - ToolPanel.Width;
  PBZ.Width := Tmp div 2;
  PBZ.Height := clientHeight div 2;
  PBZ.Top := 0;
  PBZ.Left := 0;

  PBX.Width := PBZ.Width;
  PBX.Height := PBZ.Height;
  PBX.Top := 0;
  PBX.Left := PBZ.Width;

  PBY.Width := PBZ.Width;
  PBY.Height := PBZ.Height;
  PBY.Top := PBZ.Height;
  PBY.Left := 0;

  PBISO.Width := PBZ.Width;
  PBISO.Height := PBZ.Height;
  PBISO.Top := PBZ.Height;
  PBISO.Left := PBZ.Width;

  BGZImage.Width := PBZ.Width;
  BGZImage.Height := PBZ.Height;
  BGXImage.Width := PBZ.Width;
  BGXImage.Height := PBZ.Height;
  BGYImage.Width := PBZ.Width;
  BGYImage.Height := PBZ.Height;
  BGISOImage.Width := PBZ.Width;
  BGISOImage.Height := PBZ.Height;

  Invalidate;
end;


procedure TDefine.SpeedButton1Click(Sender: TObject);
begin
  UDScale.Position := 10;
  UDA.Position := 0;
  UDB.Position := 0;
  UDC.Position := 0;
  GeneralUpdate;
end;

procedure TDefine.Open1Click(Sender: TObject);
begin
  if OpenDialog.Execute then begin
    Rules.LoadFromFile(OpenDialog.FileName);
    Rules.ReadFromStrings(10, 20, 0.012);

    UpdateConstraintList;
    GeneralUpdate;
  end;
end;

procedure TDefine.UpdateConstraintList;
var I : Integer;
begin
  ConstraintList.Clear;
  for I := 0 to Rules.ConstraintCount - 1 do begin
    ConstraintList.Items.AddObject(Rules.Constraints[I].Name, Rules.Constraints[I]);
  end;
end;

procedure TDefine.ConstraintListClick(Sender: TObject);
var I : Integer;
    V : TVertex3D;
begin
  I := ConstraintList.ItemIndex;
  if I <> -1 then begin
    V := Rules.Mount.Vertex[0, Rules.ViewC];
    V.Copy(Rules.Constraints[I].Geometry.Vertex[0, Rules.Constraints[I].PO]);
  end;
  GeneralUpdate;
end;

procedure TDefine.FormPaint(Sender: TObject);
begin
  GeneralUpdate;
end;
var TTT : Double;

procedure TDefine.Report1Click(Sender: TObject);
var I : Integer;
begin
  for I := 0 to Rules.ConstraintCount - 1 do begin
    LogMemo.Lines.Add(Format('P%d NErr=%.4f MErr=%.4f', [I + 1,
          Rules.Constraints[I].NominalError, Rules.Constraints[I].MountError]));

    TTT := Rules.Constraints[I].NominalError;
  end;
  GeneralUpdate;
end;

procedure TDefine.Addproberesult1Click(Sender: TObject);
var Form : TAddProbeForm;
    C : TConstraint;
begin
  Form := ProbePoint.TAddProbeForm.Create(Self);
  if Form.ShowModal = mrOK then begin
    C := Rules.FindConstraint(Form.Constraint);
    C.SetProbeResult(Form.X, Form.Y, Form.Z);
  end;
  Form.Free;
  GeneralUpdate;
end;

procedure TDefine.Reset1Click(Sender: TObject);
begin
  Rules.Reset;
  GeneralUpdate;
end;

procedure TDefine.SpeedButton2Click(Sender: TObject);
var I : Integer;
    C : TConstraint;
begin
  for I := 0 to Rules.ConstraintCount - 1 do begin
    C := Rules.Constraints[I];
    C.MountError;
    Rules.Constrain(C);
  end;
  Report1Click(Self);
  GeneralUpdate;
end;

procedure TDefine.Button1Click(Sender: TObject);
begin
Close;
end;

procedure TDefine.FormShow(Sender: TObject);
begin
Resize;
end;

initialization
  Rules := TConstraintRulesFile.Create;
end.
