unit ConstraintRules;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Tokenizer, Matrix3D, CNC32;

type
  TConstraintRulesFile = class;
  TProbePoint = class;

  TConstraintRuleForm = class(TForm)
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  TStackingAxis = class(TObject)
  private
    PointList : TVertexList;
    RulesFile : TConstraintRulesFile;
  public
    Name : string;
    constructor Create(aRulesFile : TConstraintRulesFile);
    procedure ReadFromStrings(S : TQuoteTokenizer);
    procedure GenerateVisual(Obj : TObject3D);
    destructor Destroy; override;
  end;

  TAxisNames = (
    anX,
    anY,
    anZ,
    anA,
    anB,
    anC,
    anU,
    anV,
    anW
  );

  TMachinePosition = class(TObject)
    Positions : array [TAxisNames] of Double;
    procedure ReadFromStrings(S : TQuoteTokenizer);
  end;


  TConstraintClass = class of TConstraint;
  TConstraint = class(TObject)
  public
    Name : string;
    PO : TVertex3D; // Vertex in move array representing normal contact
    PA, PB : TVertex3D; // Primary definition vertices
    PM : TVertex3D; // Vertex representing a point inside the mount
    PT : TVertex3D; // Vertex used for timing the mount point
    PResult : TVertex3D; // Probe result
    PPath1, PPath2 : TVertex3D;
    RulesFile : TConstraintRulesFile;
    Constraints : TStringList;
    PrimaryConstraint : TConstraint;
    Patch : TVertexList;
    ProbePoint : TProbePoint; // Rules governing what to do with the result
    Mount : TObject3D;
    MountMatrix : TMatrix3D;
    constructor Create(aRulesFile : TConstraintRulesFile); virtual;
    destructor Destroy; override;
    procedure ReadFromStrings(S : TQuoteTokenizer); virtual; abstract;
    function ReadToken(const aToken : string; S : TQuoteTokenizer) : Boolean;
    procedure GenerateVisual(Obj : TObject3D); virtual;
    function MountError : Double; virtual;
    function NominalError : Double; virtual;
    procedure SetProbeResult(X, Y, Z : Double);
  end;

  TCylinderConstraint = class(TConstraint)
    R : Double;
    procedure ReadFromStrings(S : TQuoteTokenizer); override;
    procedure GenerateVisual(Obj : TObject3D); override;
    function MountError : Double; override;
    function NominalError : Double; override;
  end;

  TSplineConstraint = class(TConstraint)
    procedure ReadFromStrings(S : TQuoteTokenizer); override;
    procedure GenerateVisual(Obj : TObject3D); override;
    function MountError : Double; override;
    function NominalError : Double; override;
  end;

  TPlaneConstraint = class(TConstraint)
    PC, PD : TVertex3D;
    constructor Create(aRulesFile : TConstraintRulesFile); override;
    destructor Destroy; override;
    procedure ReadFromStrings(S : TQuoteTokenizer); override;
    procedure GenerateVisual(Obj : TObject3D); override;
    function MountError : Double; override;
    function NominalError : Double; override;
  end;

  TAdjustType = (
    atLinear,
    atCircular,
    atCylindrical
  );

  TProbePoint = class(TObject)
    Constraints : TList;
    MachineCoordinates : Boolean;
    PrimaryConstraint : TConstraint;
    Adjust : TAdjustType;
    StackingAxis : TStackingAxis;
    Name : string;
    RulesFile : TConstraintRulesFile;
    constructor Create(aRulesFile : TConstraintRulesFile);
    procedure ReadFromStrings(S : TQuoteTokenizer); virtual;
  end;



  TConstraintRulesFile = class(TQuoteTokenizer)
  private
    StackingList : TStringList;
    ConstraintList : TStringList;
    ProbePointList : TStringList;
    FLoaded : Boolean;

    procedure ReadConstraints;
    procedure ReadMachine;
    procedure ReadStackingAxis;
    procedure ReadOperations;
    procedure ReadVertex(Vertex : TVertex3D);
    procedure ReadDisplay;

    function GetStackingAxis(Index : Integer) : TStackingAxis;
    function GetConstraint(Index : Integer) : TConstraint;
    function GetProbePoint(Index : Integer) : TProbePoint;
    function GetStackingCount : Integer;
    function GetConstraintCount : Integer;
    function GetProbePointCount: Integer;

    procedure DoReadFromStrings;

    procedure Clean;
  public
//    Fixture : TObject3D;
    Part : TObject3D;
    PartMatrix : TMatrix3D;
    UnityMatrix : TMatrix3D;
    Fixed : TObject3D;

    Normal : array[0..3] of Integer;
    ViewC : Integer;


    constructor Create;
    destructor Destroy; override;
    procedure ReadFromStrings;
    procedure SaveToStrings;
    property StackingAxes[Index : Integer] : TStackingAxis read GetStackingAxis;
    property StackingCount : Integer read GetStackingCount;
    property Constraints[Index : Integer] : TConstraint read GetConstraint;
    property ConstraintCount : Integer read GetConstraintCount;
    property ProbePoints[Index : Integer] : TProbePoint read GetProbePoint;
    property ProbePointCount : Integer read GetProbePointCount;
    property Loaded : Boolean read FLoaded;
    procedure DoProbeAdjust(P : string; X, Y, Z : Double);
    function FindConstraint(aName : string) : TConstraint;
    function FindProbePoint(aName : string) : TProbePoint;
    procedure ApplyMatrix(aMatrix : TMatrix3D);
  end;

var
  ConstraintRuleForm: TConstraintRuleForm;

implementation

{$R *.DFM}


{ TConstraintRulesFile }

constructor TConstraintRulesFile.Create;
begin
  inherited Create;
  ConstraintList := TStringList.Create;
  StackingList := TStringList.Create;
  ProbePointList := TStringList.Create;

  Part := TObject3D.Create;
  Fixed := TObject3D.Create;
  PartMatrix := TMatrix3D.Create;
  UnityMatrix := TMatrix3D.Create;
end;

destructor TConstraintRulesFile.Destroy;
begin
  Clean;
  ConstraintList.Free;
  StackingList.Free;
  ProbePointList.Free;

  PartMatrix.Free;
  UnityMatrix.Free;
  Part.Free;
  Fixed.Free;

  inherited;
end;

procedure TConstraintRulesFile.Clean;
  procedure CleanList(List : TStringList);
  begin
    if Assigned(List) then
      while List.Count > 0 do begin
        List.Objects[0].Free;
        List.Delete(0);
      end;
  end;
begin
  Part.Clean;
  Fixed.Clean;
  CleanList(ConstraintList);
  CleanList(StackingList);
  CleanList(ProbePointList);
end;


procedure TConstraintRulesFile.ReadFromStrings;
var I : Integer;
begin
  FLoaded := False;
  DoReadFromStrings;
  Part.Fix;
  Fixed.Fix;
  for I := 0 to ConstraintCount - 1 do begin
    Constraints[I].Mount.Fix;
  end;

  ApplyMatrix(nil);
  FLoaded := True;
end;

procedure TConstraintRulesFile.DoReadFromStrings;
var Token : string;
begin
  Clean;
  Seperator := '[](){}=,';
  QuoteChar := '''';
  Rewind;

  Normal[0] := Fixed.Source.Add(0, 0, 0);
  Normal[1] := Fixed.Source.Add(0, 0, 1);
  Normal[2] := Fixed.Source.Add(0, 1, 0);
  Normal[3] := Fixed.Source.Add(1, 0, 0);

  ViewC := Fixed.Source.Add(0, 0, 0);

  try
    ExpectedTokens(['ConstraintRules', '=', '{']);

    Token := LowerCase(Read);
    while not EndOfStream do begin
      if Token = 'constraints' then
        ReadConstraints
      else if Token = 'machine' then
        ReadMachine
      else if Token = 'stackingaxis' then
        ReadStackingAxis
      else if Token = 'operations' then
        ReadOperations
      else if Token = 'display' then
        ReadDisplay
      else if Token = '}' then
        Exit
      else
        raise Exception.CreateFmt('Invalid token "%s"', [Token]);

      Token := LowerCase(Read);
    end;
    raise Exception.Create('Unexpected end of file');
  except
    on E : Exception do
      raise Exception.CreateFmt('%s occured at line %d', [E.message, Line]);
  end;
end;

procedure TConstraintRulesFile.ReadConstraints;
  procedure CreateConstraint(ConstraintClass : TConstraintClass);
  var Constraint : TConstraint;
  begin
    Constraint := ConstraintClass.Create(Self);
    ConstraintList.AddObject(Constraint.Name, Constraint);
    Push;
    Constraint.ReadFromStrings(Self);
    Constraint.GenerateVisual(Part);
    // Now create the fixture to support this point

  end;

var Token : string;
begin
  ExpectedTokens(['=', '{']);
  Token := LowerCase(Read);
  while not EndOfStream do begin
    if Token = 'cylinder' then
      CreateConstraint(TCylinderConstraint)
    else if Token = 'plane' then
      CreateConstraint(TPlaneConstraint)
    else if Token = 'spline' then
      CreateConstraint(TSplineConstraint)
    else if Token = '}' then
      Exit
    else
      raise Exception.CreateFmt('Invalid token "%s"', [Token]);
    Token := LowerCase(Read);
  end;
  raise Exception.Create('Unexpected end of file');
end;

procedure TConstraintRulesFile.ReadMachine;
var Machine : TMachinePosition;
begin
  Push;
  Machine := TMachinePosition.Create;
  Machine.ReadFromStrings(Self);
end;

procedure TConstraintRulesFile.ReadDisplay;
var Token : string;
    F : TFace3D;
    I : Integer;
begin
  ExpectedTokens(['=', '{']);
  Token := LowerCase(Read);
  F := TFace3D.Create(Self);
  Part.Add(F);

  while not EndOfStream do begin
    if Token = '}' then begin
      Exit;
    end else if Token = 'p'then begin
      I := Part.Source.Add(0, 0, 0);
      ReadVertex(Part.Source[I]);
      F.Add(I);
    end else
      raise Exception.CreateFmt('Invalid token "%s"', [Token]);

    Token := LowerCase(Read);
  end;
  raise Exception.Create('Unexpected end of file');
end;

procedure TConstraintRulesFile.ReadOperations;
  procedure CreateProbePoint;
  var ProbePoint : TProbePoint;
  begin
    ProbePoint := TProbePoint.Create(Self);
    try
      Push;
      ProbePoint.ReadFromStrings(Self);
      ProbePointList.AddObject(ProbePoint.Name, ProbePoint);
    except
      on E : Exception do begin
        ProbePoint.Free;
        raise;
      end;
    end;
  end;

var Token : string;
begin
  ExpectedTokens(['=', '{']);
  Token := LowerCase(Read);
  while not EndOfStream do begin
    if Token = 'probe' then
      CreateProbePoint
    else if Token = '}' then
      Exit
    else
      raise Exception.CreateFmt('Invalid token "%s"', [Token]);
    Token := LowerCase(Read);
  end;
  raise Exception.Create('Unexpected end of file');
end;

procedure TConstraintRulesFile.ReadStackingAxis;
var StackingAxis : TStackingAxis;
begin
  Push;
  StackingAxis := TStackingAxis.Create(Self);
  try
    StackingAxis.ReadFromStrings(Self);
    StackingList.AddObject(StackingAxis.Name, StackingAxis);
  except
    StackingAxis.Free;
    raise;
  end;
  StackingAxis.GenerateVisual(Part);
end;

procedure TConstraintRulesFile.ReadVertex(Vertex : TVertex3D);
begin
  ExpectedToken('=');
  Vertex.X := StrToFloat(Read);
  ExpectedToken(',');
  Vertex.Y := StrToFloat(Read);
  ExpectedToken(',');
  Vertex.Z := StrToFloat(Read);
end;

procedure TConstraintRulesFile.SaveToStrings;
begin
end;


procedure TConstraintRulesFile.DoProbeAdjust(P : string; X, Y, Z : Double);
begin
  // Sequence
  // Find this constraint
  // Verify the other restraints are where they should be
  // Determine the direction of adjustment
  //
end;

{ TMachine }

function ReadDoubleAssign(S : TQuoteTokenizer) : Double;
begin
  S.ExpectedToken('=');
  Result := StrToFloat(S.Read);
end;

procedure TMachinePosition.ReadFromStrings(S: TQuoteTokenizer);
var Token : string;
begin
  S.ExpectedTokens(['machine', '=', '{']);
  Token := LowerCase(S.Read);
  FillChar(Positions, SizeOf(Positions), 0);
  while not S.EndOfStream do begin
    if Token = '}' then
      Exit;
    if Length(Token) > 1 then
      raise Exception.CreateFmt('Invalid axis name "%s"', [Token]);

    case Token[1] of
      'x' : Positions[anX] := ReadDoubleAssign(S);
      'y' : Positions[anX] := ReadDoubleAssign(S);
      'z' : Positions[anX] := ReadDoubleAssign(S);
      'a' : Positions[anX] := ReadDoubleAssign(S);
      'b' : Positions[anX] := ReadDoubleAssign(S);
      'c' : Positions[anX] := ReadDoubleAssign(S);
      'u' : Positions[anX] := ReadDoubleAssign(S);
      'v' : Positions[anX] := ReadDoubleAssign(S);
      'w' : Positions[anX] := ReadDoubleAssign(S);
    else
      raise Exception.CreateFmt('Invalid axis name "%s"', [Token]);
    end;
    Token := LowerCase(S.Read);
  end;
  raise Exception.Create('Unexpected end of file');
end;

function TConstraintRulesFile.GetConstraint(Index: Integer): TConstraint;
begin
  Result := TConstraint(ConstraintList.Objects[Index]);
end;

function TConstraintRulesFile.GetConstraintCount: Integer;
begin
  Result := ConstraintList.Count;
end;

function TConstraintRulesFile.GetProbePoint(Index: Integer): TProbePoint;
begin
  Result := TProbePoint(ProbePointList.Objects[Index]);
end;

function TConstraintRulesFile.GetProbePointCount: Integer;
begin
  Result := ProbePointList.Count;
end;

function TConstraintRulesFile.GetStackingAxis(
  Index: Integer): TStackingAxis;
begin
  Result := TStackingAxis(StackingList.Objects[Index]);
end;

function TConstraintRulesFile.GetStackingCount: Integer;
begin
  Result := StackingList.Count;
end;


function TConstraintRulesFile.FindConstraint(aName: string): TConstraint;
var I : Integer;
begin
  for I := 0 to ConstraintCount - 1 do begin
    Result := Constraints[I];
    if CompareText(Result.Name, aName) = 0 then begin
      Exit;
    end;
  end;

  raise Exception.CreateFmt('Unknown constraint "%s"', [aName]);
end;

function TConstraintRulesFile.FindProbePoint(aName : string) : TProbePoint;
var I : Integer;
begin
  for I := 0 to ProbePointCount - 1 do begin
    Result := ProbePoints[I];
    if CompareText(Result.Name, aName) = 0 then begin
      Exit;
    end;
  end;

  raise Exception.CreateFmt('Unknown constraint "%s"', [aName]);
end;

procedure TConstraintRulesFile.ApplyMatrix(aMatrix : TMatrix3D);
var I : Integer;
begin
  for I := 0 to ConstraintCount - 1 do
    Constraints[I].MountMatrix.Transform(Constraints[I].Mount.Source, Constraints[I].Mount.Draw);

  PartMatrix.Transform(Part.Source, Part.Draw);
  UnityMatrix.Transform(Fixed.Source, Fixed.Draw);

  if Assigned(aMatrix) then begin
    for I := 0 to ConstraintCount - 1 do
      aMatrix.Transform(Constraints[I].Mount.Draw, Constraints[I].Mount.Draw);

    aMatrix.Transform(Part.Draw, Part.Draw);
    aMatrix.Transform(Fixed.Draw, Fixed.Draw);
  end;
end;

{ TStackingAxis }

constructor TStackingAxis.Create(aRulesFile : TConstraintRulesFile);
begin
  PointList := TVertexList.Create;
  RulesFile := aRulesFile;
end;

destructor TStackingAxis.Destroy;
begin
  // The vertices in the list should be owned by a vertex list
  PointList.Free;
  inherited;
end;

procedure TStackingAxis.ReadFromStrings(S: TQuoteTokenizer);
  procedure ReadVertex;
  var Vertex : TVertex3D;
  begin
    Vertex := TVertex3D.Create;
    PointList.Add(Vertex);
    RulesFile.ReadVertex(Vertex);
  end;

var Token : string;
begin
  S.ExpectedTokens(['stackingaxis', '=', '{']);
  Token := LowerCase(S.Read);
  while not S.EndOfStream do begin
    if Token = 'name' then
      Name := S.ReadStringAssign
    else if Token = 'p' then
      ReadVertex
    else if Token = '}' then
      Exit
    else
      raise Exception.CreateFmt('Invalid token "%s"', [Token]);
    Token := LowerCase(S.Read);
  end;
  raise Exception.Create('Unexpected end of file');
end;

procedure TStackingAxis.GenerateVisual(Obj : TObject3D);
var F : TFace3D;
    I : Integer;
begin
  F := TFace3D.Create(Self);
  F.IsSpline := True;
  for I := 0 to PointList.Count - 1 do begin
    F.Add(Obj.Source.Add(PointList[I]));
  end;
  Obj.Add(F);
end;

{ TConstraint }

constructor TConstraint.Create(aRulesFile : TConstraintRulesFile);
begin
  RulesFile := aRulesFile;
  Constraints := TStringList.Create;
  PO := TVertex3D.Create;
  PA := TVertex3D.Create;
  PB := TVertex3D.Create;
  PM := TVertex3D.Create;
  PT := TVertex3D.Create;
  PResult := TVertex3D.Create;
  PPath1 := TVertex3D.Create;
  PPath2 := TVertex3D.Create;
  Patch := TVertexList.Create;
  Mount := TObject3D.Create;
  MountMatrix := TMatrix3D.Create;
end;

destructor TConstraint.Destroy;
begin
  Constraints.Free;
{  PO.Free;
  PA.Free;
  PB.Free;
  PM.Free;
  PT.Free; }
  PResult.Free;
//  PPath1.Free;
//  PPath2.Free;
  Mount.Free;
  inherited;
end;

function TConstraint.ReadToken(const aToken : string; S : TQuoteTokenizer) : Boolean;
begin
  Result := True;
  if aToken = 'name' then
    Name := S.ReadStringAssign
  else if aToken = 'po' then
    RulesFile.ReadVertex(PO)
  else if aToken = 'pa' then
    RulesFile.ReadVertex(PA)
  else if aToken = 'pb' then
    RulesFile.ReadVertex(PB)
  else
    Result := False;
end;

procedure TConstraint.GenerateVisual(Obj : TObject3D);
begin
  Obj.Source.Add(PO);
  Obj.Source.Add(PA);
  Obj.Source.Add(PB);
  Obj.Source.Add(PM);
  Obj.Source.Add(PT);
  PResult.Copy(PO);
end;

function TConstraint.MountError : Double;
var I : Integer;
    L, L1 : Double;
    V1, V2 : TVertex3D;
begin
  Result := 100000;
  V1 := nil;
  V2 := nil;
  for I := 0 to Patch.Count - 1 do begin
    L := PResult.DistanceTo(Patch[I]);
    if L < Result then begin
      V1 := Patch[I];
      Result := L;
    end;
  end;
end;

function TConstraint.NominalError : Double;
begin
  Result := PO.DistanceTo(PResult);
end;

procedure TConstraint.SetProbeResult(X, Y, Z : Double);
begin
  PResult.X := X;
  PResult.Y := Y;
  PResult.Z := Z;
  MountMatrix.Translate(PO.X - X, PO.Y - Y, PO.Z - Z);
end;

{ TCylinderConstraint }

procedure TCylinderConstraint.GenerateVisual(Obj: TObject3D);
var P1Ndx, P2Ndx : Integer;
begin
  inherited;
  Obj.CreateCylinder(Self, PA, PB, PO, R, 32);
  Patch.Clear;
  Obj.CreateCylinderPatch(Patch, PA, PB, PO, R, 10, 0.025);
  Obj.Source.AddList(Patch);

  PM.Copy(PB);     // End centre of cylinder
  PM.Midpoint(PA); // Centre of cylinder
  PM.Subtract(PO); // From origin
  PPath2.Copy(PM); // Vector for end of probe
  PM.Reverse;      // Now facing away
  PPath1.Copy(PM); // Vector for start of probe
  PM.Add(PO);
//  PM.Unitary;

  PPath1.SetMagnitude(1);
  PPath1.Add(PO);
  PPath2.SetMagnitude(1);
  PPath2.Add(PO);

  P1ndx := Obj.Source.Add(PPath1);
  P2ndx := Obj.Source.Add(PPath2);

  Obj.Add(TFace3D.Create(Self, [P1ndx, P2ndx]));

  PT.Copy(PB); // Used for timing

  Mount.CreateMountPoint(Self, PO, PM, PT);
end;

procedure TCylinderConstraint.ReadFromStrings(S: TQuoteTokenizer);
var Token : string;
begin
  S.ExpectedTokens(['cylinder', '=', '{']);
  Token := LowerCase(S.Read);
  while not S.EndOfStream do begin
    if not ReadToken(Token, S) then begin
      if Token = 'r' then
        R := StrToFloat(S.ReadStringAssign)
      else if Token = '}' then
        Exit
      else
        raise Exception.CreateFmt('Invalid token "%s"', [Token]);
    end;
    Token := LowerCase(S.Read);
  end;
  raise Exception.Create('Unexpected end of file');
end;

function TCylinderConstraint.MountError : Double;
begin
  Result := inherited MountError;
end;

function TCylinderConstraint.NominalError : Double;
begin
  Result := inherited NominalError;
end;


{ TSplineConstraint }

procedure TSplineConstraint.GenerateVisual(Obj: TObject3D);
begin
  inherited;
end;

procedure TSplineConstraint.ReadFromStrings(S: TQuoteTokenizer);
var Token : string;
begin
  S.ExpectedTokens(['spline', '=', '{']);
  Token := LowerCase(S.Read);
  while not S.EndOfStream do begin
    if not ReadToken(Token, S) then begin
      if Token = '}' then
        Exit
      else
        raise Exception.CreateFmt('Invalid token "%s"', [Token]);
    end;
    Token := LowerCase(S.Read);
  end;
  raise Exception.Create('Unexpected end of file');
end;

function TSplineConstraint.MountError : Double;
begin
  Result := inherited MountError;
end;

function TSplineConstraint.NominalError : Double;
begin
  Result := inherited NominalError;
end;

{ TPlaneConstraint }

constructor TPlaneConstraint.Create(aRulesFile : TConstraintRulesFile);
begin
  inherited;
  PC := TVertex3D.Create;
  PD := TVertex3D.Create;
end;

destructor TPlaneConstraint.Destroy;
begin
//  PC.Free;
//  PD.Free;
  inherited;
end;

{    Plane = {
      Name = 'P5'
      PO = 13.671, -7.336, 54.494
      PA = 14.035, -7.679, 53.994
      PB = 14.014, -7.699, 54.994
      PC = 13.304, -6.995, 54.994
      PD = 13.323, -6.976, 53.994
    }


procedure TPlaneConstraint.GenerateVisual(Obj: TObject3D);
var P : TVertex3D;
    P1Ndx, P2Ndx : Integer;
begin
  inherited;
  Obj.Source.Add(PC);
  Obj.Source.Add(PD);
  Patch.Clear;
  Obj.CreateSimpleQuad(Self, PA, PB, PC, PD);
  Obj.CreateSimpleQuadPatch(Patch, PA, PB, PC, PD, 10);
  Obj.Source.AddList(Patch);

  P := TVertex3D.Create(PC);
  PM.Copy(PA);         // First point of triangle
  PM.Subtract(PB);     // Origin on second point
  P.Subtract(PB);      // third point origin on PB
  PPath1.Copy(PM);
  PM.CrossProduct(P);  // Generate a vector perpendicular to the described plane
  PM.Unitary;          // but only one unit long
  PM.Add(PO);          // Now put it back relative

  PPath1.CrossProduct(P); // Probe start position
  PPath1.SetMagnitude(1);
  PPath2.Copy(PPath1);
  PPath2.Reverse;
  PPath1.Add(PO);
  PPath2.Add(PO);

  P1ndx := Obj.Source.Add(PPath1);
  P2ndx := Obj.Source.Add(PPath2);

  Obj.Add(TFace3D.Create(Self, [P1ndx, P2ndx]));
  Mount.CreateMountPoint(Self, PO, PM, PT);

  P.Free;

  PT.Copy(PA); // Just used for timing the mount point
end;

procedure TPlaneConstraint.ReadFromStrings(S: TQuoteTokenizer);
var Token : string;
begin
  S.ExpectedTokens(['plane', '=', '{']);
  Token := LowerCase(S.Read);
  while not S.EndOfStream do begin
    if not ReadToken(Token, S) then begin
      if Token = 'pc' then
        RulesFile.ReadVertex(PC)
      else if Token = 'pd' then
        RulesFile.ReadVertex(PD)
      else if Token = '}' then
        Exit
      else
        raise Exception.CreateFmt('Invalid token "%s"', [Token]);
    end;
    Token := LowerCase(S.Read);
  end;
  raise Exception.Create('Unexpected end of file');
end;

function TPlaneConstraint.MountError : Double;
begin
  Result := inherited MountError;
end;

function TPlaneConstraint.NominalError : Double;
begin
  Result := inherited NominalError;
end;

{ TProbeOperation }

constructor TProbePoint.Create(aRulesFile : TConstraintRulesFile);
begin
  inherited Create;
  Constraints := TList.Create;
  RulesFile := aRulesFile;
end;

procedure TProbePoint.ReadFromStrings(S: TQuoteTokenizer);
var Token : string;

  procedure ReadAdjust;
  begin
    Token := LowerCase(S.ReadStringAssign);
    if Token = 'linear' then
      Adjust := atLinear
    else if Token = 'circular' then
      Adjust := atCircular
    else if Token = 'cylindrical' then
      Adjust := atCylindrical
    else
      raise Exception.CreateFmt('Invalid Adjust "%s"', [Token]);
  end;

  procedure ReadConstraints;
  var Line, Token : string;
  begin
    S.ExpectedToken('=');
    Line := Trim(S.ReadLine);
    Token := CNC32.ReadDelimited(Line, ',');
    while Token <> '' do begin
      Self.Constraints.Add(RulesFile.FindConstraint(Token));
      Token := CNC32.ReadDelimited(Line, ',');
    end;

  end;

  procedure FindStackingAxis(aName : string);
  var I : Integer;
  begin
    for I := 0 to RulesFile.StackingCount - 1 do begin
      if CompareText(RulesFile.StackingAxes[I].Name, aName) = 0 then begin
        StackingAxis := RulesFile.StackingAxes[I];
        Exit;
      end;
    end;
    raise Exception.CreateFmt('Invalid stacking axis "%s"', [aName]);
  end;

begin
  S.ExpectedTokens(['probe', '[']);
  Name := S.Read;
  RulesFile.FindConstraint(Name).ProbePoint := Self;
  S.ExpectedTokens([']', '=', '{']);

  Token := LowerCase(S.Read);

  while not S.EndOfStream do begin
    if Token = 'constraints' then
      ReadConstraints
    else if Token = 'machinecoordinates' then
      MachineCoordinates := S.ReadBooleanAssign
    else if Token = 'adjust' then
      ReadAdjust
    else if Token = 'stackingaxis' then
      FindStackingAxis(S.ReadStringAssign)
    else if Token = 'primaryconstraint' then
      PrimaryConstraint := RulesFile.FindConstraint(S.ReadStringAssign)
    else if Token = '}' then
      Exit
    else
      raise Exception.CreateFmt('Invalid token "%s"', [Token]);

    Token := LowerCase(S.Read);
  end;
  raise Exception.Create('Unexpected end of file');
end;


end.
