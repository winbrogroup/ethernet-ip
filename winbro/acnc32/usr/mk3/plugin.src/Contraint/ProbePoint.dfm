object AddProbeForm: TAddProbeForm
  Left = 466
  Top = 478
  Width = 232
  Height = 259
  Caption = 'Probe Result'
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -16
  Font.Name = 'Comic Sans MS'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 23
  object Label1: TLabel
    Left = 16
    Top = 16
    Width = 40
    Height = 23
    Caption = 'Name'
  end
  object Label2: TLabel
    Left = 32
    Top = 64
    Width = 12
    Height = 23
    Caption = 'X'
  end
  object Label3: TLabel
    Left = 32
    Top = 104
    Width = 11
    Height = 23
    Caption = 'Y'
  end
  object Label4: TLabel
    Left = 32
    Top = 144
    Width = 12
    Height = 23
    Caption = 'Z'
  end
  object ConstraintEdit: TEdit
    Left = 72
    Top = 16
    Width = 121
    Height = 31
    TabOrder = 0
  end
  object Panel1: TPanel
    Left = 0
    Top = 191
    Width = 224
    Height = 41
    Align = alBottom
    TabOrder = 1
    object BitBtn1: TBitBtn
      Left = 8
      Top = 8
      Width = 80
      Height = 25
      TabOrder = 0
      OnClick = BitBtn1Click
      Kind = bkOK
    end
    object BitBtn2: TBitBtn
      Left = 104
      Top = 8
      Width = 80
      Height = 25
      TabOrder = 1
      Kind = bkCancel
    end
  end
  object XEdit: TEdit
    Left = 72
    Top = 56
    Width = 121
    Height = 31
    TabOrder = 2
  end
  object YEdit: TEdit
    Left = 72
    Top = 96
    Width = 121
    Height = 31
    TabOrder = 3
  end
  object ZEdit: TEdit
    Left = 72
    Top = 136
    Width = 121
    Height = 31
    TabOrder = 4
  end
end
