unit MDTExtPlc;

interface

uses SysUtils, Classes, CncTypes, AbstractPlcPlugin, NamedPlugin, IniFiles,
     INamedInterface, IStringArray;

type
  TPlcMDTExt = class(TAbstractPlcPlugin)
  private
    ItemList: TList;
    MDTName: string;
    Table: IACNC32StringArray;
    ThisName: string;
    procedure RebuildTable;
    procedure ReadConfig(Path: string);
  public
    constructor Create( Parameters : string;
                        ResolveDeviceEv : TPlcPluginResolveDeviceEv;
                        ResolveItemEv : TPlcPluginResolveItemEv); override;
    destructor Destroy; override;
    procedure Compile ( var Embedded : TEmbeddedData;
                        aReadEv : TPlcPluginReadEv;
                        ResolveItemEv : TPlcPluginResolveItemEv;
                        Write : Boolean);  override;
    procedure GatePre; override;
    function GatePost : Integer; override;
    function Read(var Embedded : TEmbeddedData) : Boolean; override;
    procedure Write(var Embedded : TEmbeddedData; Line, LastLine : Boolean); override;
    function GateName(var Embedded : TEmbeddedData) : string; override;
    function ItemProperties : string; override;
    procedure ReadRowAtIndex(Index: Integer);
    procedure WriteRowAtIndex(Index: Integer);

    class procedure ReadRow(const Obj, Col, Value: string);
    class procedure WriteRow(const Obj, Col, Value: string);

    class function FindComponent(Name: string): TPlcMDTExt;
  end;

  TTagReference = class
    TagName: string;
    ColName: string;
    Tag: TTagRef;
  end;

implementation

var
  ObjectList: TStringList;

procedure AddComponent(Comp: TPlcMDTExt);
begin
  ObjectList.AddObject(UpperCase(Comp.ThisName), Comp);
end;

procedure RemoveComponent(Comp: TPlcMDTExt);
var I: Integer;
begin
  I:= ObjectList.IndexOf(UpperCase(Comp.ThisName));
  if I <> -1 then begin
    ObjectList.Delete(I);
  end;
end;


{ TPlcMDTExt }

procedure TPlcMDTExt.Write(var Embedded: TEmbeddedData; Line, LastLine: Boolean);
begin
  inherited;

end;

procedure TPlcMDTExt.Compile(var Embedded: TEmbeddedData; aReadEv: TPlcPluginReadEv; ResolveItemEv: TPlcPluginResolveItemEv;
  Write: Boolean);
begin
  raise Exception.Create('MDTExt is not compilable');
end;

function TPlcMDTExt.GateName(var Embedded: TEmbeddedData): string;
begin

end;

constructor TPlcMDTExt.Create(Parameters: string; ResolveDeviceEv: TPlcPluginResolveDeviceEv;
  ResolveItemEv: TPlcPluginResolveItemEv);
begin
  ItemList:= TList.Create;
  ThisName:= ReadDelimited(Parameters, ';');
  ReadConfig(NamedPlugin.DllProfilePath + 'ObjectPlc\' + ThisName + '.cfg');
  AddComponent(Self);
end;

function TPlcMDTExt.ItemProperties: string;
begin
end;

function TPlcMDTExt.GatePost: Integer;
begin
  Result:= 0;
end;

function TPlcMDTExt.Read(var Embedded: TEmbeddedData): Boolean;
begin
  Result:= False;
end;

destructor TPlcMDTExt.Destroy;
var T: TTagReference;
    I: Integer;
begin
  for I:= 0 to ItemList.Count - 1 do begin
    T:= TTagReference(ItemList[I]);
    T.Free;
  end;
  ItemList.Free;
  RemoveComponent(Self);

  if Table <> nil then begin
    StringArraySet.ReleaseArray(nil, Table);
    Table:= nil;
  end;

  inherited;
end;

procedure TPlcMDTExt.GatePre;
begin
end;

procedure TPlcMDTExt.ReadConfig(Path: string);
var Ini: TIniFile;
    I: Integer;
    T: TTagReference;
    S: TStringList;
    FieldName: WideString;
begin
  Ini:= TIniFile.Create(Path);
  S:= TStringList.Create;
  try
    MDTName:= Ini.ReadString('MDTExt', 'MDTName', 'Test');
    Ini.ReadSectionValues('tags', S);
    for I:= 0 to S.Count - 1 do begin
      T:= TTagReference.Create;
      T.TagName:= S.Names[i];
      T.ColName:= S.Values[T.TagName];
      T.Tag:= Named.AquireTag(PChar(T.TagName), 'TStringTag', nil);
      ItemList.Add(T);
    end;

    if StringArraySet.UseArray(MDTName, nil, Table) <> AAR_OK then begin
      if StringArraySet.CreateArray(MDTName, nil, Table) <> AAR_OK then begin
        raise Exception.Create('Failed to create MDT: ' + MDTName);
      end;
    end;

    // Verify MDT is valid
    if Table.FieldCount >= ItemList.Count then begin
      for I:= 0 to ItemList.Count - 1 do begin
        T:= TTagReference(ItemList[I]);
        Table.FieldNameAtIndex(I, FieldName);
        FieldName:= UpperCase(FieldName);
        if UpperCase(T.ColName) <> FieldName then begin
          RebuildTable;
          Exit;
        end;
      end;
    end else
      RebuildTable;

  finally
    Ini.Free;
  end;
end;


procedure TPlcMDTExt.RebuildTable;
var F: WideString;
    I: Integer;
    T: TTagReference;
begin
  while Table.FieldCount > 0 do begin
    Table.FieldNameAtIndex(0, F);
    Table.DeleteField(F);
  end;

  for I:= 0 to ItemList.Count  - 1 do begin
    T:= TTagReference(ItemList[I]);
    Table.AddField(T.ColName);
  end;
end;

procedure TPlcMDTExt.ReadRowAtIndex(Index: Integer);
var
    I: Integer;
    T: TTagReference;
    Test: WideString;
    Tmp: string;
begin
  for I:= 0 to ItemList.Count - 1 do begin
    T:= TTagReference(ItemList[I]);
    Table.GetValue(Index, I, Test);
    Tmp:= Test;
    Named.SetAsString(T.Tag, PChar(Tmp));
  end;
end;

procedure TPlcMDTExt.WriteRowAtIndex(Index: Integer);
var
    I: Integer;
    T: TTagReference;
    Tmp: array [0..255] of Char;
begin
  for I:= 0 to ItemList.Count - 1 do begin
    T:= TTagReference(ItemList[I]);
    Named.GetAsString(T.Tag, Tmp, 255);
    Table.SetValue(Index, I, Tmp);
  end;
end;



class function TPlcMDTExt.FindComponent(Name: string): TPlcMDTExt;
var I: Integer;
begin
  Name:= UpperCase(Name);
  I:= ObjectList.IndexOf(Name);
  if I <> -1 then
    Result:= TPlcMDTExt(ObjectList.Objects[I])
  else
    raise Exception.Create('Unable to find MDTExt of name: ' + Name);
end;

class procedure TPlcMDTExt.ReadRow(const Obj, Col, Value: string);
var MDTExt: TPlcMDTExt;
    I: Integer;
    Test: WideString;
begin
  MDTExt:= FindComponent(Obj);
  MDTExt.Table.BeginUpdate;
  try
    for I:= 0 to MDTExt.ItemList.Count - 1 do begin
      MDTExt.Table.GetFieldValue(I, Col, Test);
      if MDTExt.Table.GetFieldValue(I, Col, Test) <> AAR_OK then begin
        raise Exception.CreateFmt('Column name not found [%s]', [Col]);
      end;
      if UpperCase(Test) = UpperCase(Value) then begin
        MDTExt.ReadRowAtIndex(I);
        Exit;
      end;
    end;
  finally
    MDTExt.Table.EndUpdate;
  end;
  raise Exception.CreateFmt('Value not found in column [%s]', [Value]);
end;

class procedure TPlcMDTExt.WriteRow(const Obj, Col, Value: string);
var MDTExt: TPlcMDTExt;
    I: Integer;
    Test: WideString;
begin
  MDTExt:= FindComponent(Obj);
  MDTExt.Table.BeginUpdate;
  try
    for I:= 0 to MDTExt.ItemList.Count - 1 do begin
      if MDTExt.Table.GetFieldValue(I, Col, Test) <> AAR_OK then begin
        raise Exception.CreateFmt('Column name not found [%s]', [Col]);
      end;
      if UpperCase(Test) = UpperCase(Value) then begin
        MDTExt.WriteRowAtIndex(I);
        Exit;
      end;
    end;
  finally
    MDTExt.Table.EndUpdate;
  end;
  raise Exception.CreateFmt('Value not found in column [%s]', [Value]);
end;

initialization
  ObjectList:= TStringList.Create;
  ObjectList.Sorted:= True;
  TPlcMDTExt.AddPlugin('MDTExt', TPlcMDTExt);
end.
