unit Methods;

interface

uses Classes, SysUtils, AbstractScript, INamedInterface, NamedPlugin,
     IScriptInterface, IStringArray, MDTExtPlc;

type
  TMDTReadRowExtension = class(TScriptMethod)
    function Name: WideString; override; stdcall;
    function ParameterCount: Integer; override; stdcall;
    procedure Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer); override; stdcall;
  end;

  TMDTWriteRowExtension = class(TScriptMethod)
    function Name: WideString; override; stdcall;
    function ParameterCount: Integer; override; stdcall;
    procedure Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer); override; stdcall;
  end;


implementation

{ TMDTReadRowExtension }

function TMDTReadRowExtension.Name: WideString;
begin
  Result:= 'ReadRow';
end;

function TMDTReadRowExtension.ParameterCount: Integer;
begin
  Result:= 3;
end;

procedure TMDTReadRowExtension.Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer);
var Buffer: array [0..255] of Char;
    Table, Column, Value: string;
begin
  Named.GetAsString(Params[0], Buffer, 255);
  Table:= Buffer;
  Named.GetAsString(Params[1], Buffer, 255);
  Column:= Buffer;
  Named.GetAsString(Params[2], Buffer, 255);
  Value:= Buffer;

  MDTExtPlc.TPlcMDTExt.ReadRow(Table, Column, Value);
end;

{ TMDTWriteRowExtension }

function TMDTWriteRowExtension.Name: WideString;
begin
  Result:= 'WriteRow';
end;

function TMDTWriteRowExtension.ParameterCount: Integer;
begin
  Result:= 3;
end;

procedure TMDTWriteRowExtension.Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer);
var Buffer: array [0..255] of Char;
    Table, Column, Value: string;
begin
  Named.GetAsString(Params[0], Buffer, 255);
  Table:= Buffer;
  Named.GetAsString(Params[1], Buffer, 255);
  Column:= Buffer;
  Named.GetAsString(Params[2], Buffer, 255);
  Value:= Buffer;

  MDTExtPlc.TPlcMDTExt.WriteRow(Table, Column, Value);
end;

initialization
  TDefaultScriptLibrary.SetName('MDTExt');
  TDefaultScriptLibrary.AddMethod(TMDTReadRowExtension);
  TDefaultScriptLibrary.AddMethod(TMDTWriteRowExtension);
end.
