unit PlcLogging;

{Change log
16.01.2012 Outstring changed to use function FormatDate rather than DateTimeToStr
16.01.2012 Additional parameter added to config file "AbsolutePath" to allow access to all directories
}

interface

uses SysUtils, Classes, CncTypes, AbstractPlcPlugin, NamedPlugin, IniFiles,
     INamedInterface, Windows;

type
  TPlcLogging = class(TAbstractPlcPlugin)
  private
    StartTime: Double;
    EndTime: Double;
    TagList: TList;
    OutputPath: string;
    FilenameTag: TTagRef;
    FilenameSuffix: string;
    FolderTag: TTagRef;
    ConfigName: string;
    procedure ReadConfig(Path: string);
    procedure WriteLog;
  public
    constructor Create( Parameters : string;
                        ResolveDeviceEv : TPlcPluginResolveDeviceEv;
                        ResolveItemEv : TPlcPluginResolveItemEv); override;
    destructor Destroy; override;
    procedure Compile ( var Embedded : TEmbeddedData;
                        aReadEv : TPlcPluginReadEv;
                        ResolveItemEv : TPlcPluginResolveItemEv;
                        Write : Boolean);  override;
    procedure GatePre; override;
    function GatePost : Integer; override;
    function Read(var Embedded : TEmbeddedData) : Boolean; override;
    procedure Write(var Embedded : TEmbeddedData; Line, LastLine : Boolean); override;
    function GateName(var Embedded : TEmbeddedData) : string; override;
    function ItemProperties : string; override;
  end;

implementation

{ TProductionMonitor }

procedure TPlcLogging.Write(var Embedded: TEmbeddedData; Line,
  LastLine: Boolean);
begin
  if Line and not LastLine then begin
    StartTime:= Now;
  end
  else
  if not Line and LastLine then begin
    EndTime:= Now;
    if(FilenameTag <> nil) then
      WriteLog;
  end;
end;

procedure TPlcLogging.Compile(var Embedded: TEmbeddedData;
  aReadEv: TPlcPluginReadEv; ResolveItemEv: TPlcPluginResolveItemEv;
  Write: Boolean);
begin
end;

function TPlcLogging.GateName(var Embedded: TEmbeddedData): string;
begin
  Result:= '';
end;

constructor TPlcLogging.Create(Parameters: string;
  ResolveDeviceEv: TPlcPluginResolveDeviceEv;
  ResolveItemEv: TPlcPluginResolveItemEv);
begin
  ConfigName:= ReadDelimited(Parameters, ';');
  TagList:= TList.Create;
  ReadConfig(NamedPlugin.DllProfilePath + 'ObjectPlc\' + ConfigName + '.cfg');
end;

destructor TPlcLogging.Destroy;
begin
  TagList.Free;
  inherited;
end;

function TPlcLogging.ItemProperties: string;
begin
  Result:= '';
end;

function TPlcLogging.GatePost: Integer;
begin
  Result:= 0;
end;

function TPlcLogging.Read(var Embedded: TEmbeddedData): Boolean;
begin
  Result:= False;
end;

procedure TPlcLogging.GatePre;
begin
end;

procedure TPlcLogging.ReadConfig(Path: string);
var Ini: TIniFile;
    I, TagCount: Integer;
    Tmp: string;
    Tag: TTagRef;
    AbsolutePath: string;
begin
  Ini:= TIniFile.Create(Path);
  try
    TagCount:= Ini.ReadInteger('logging', 'tagcount', 0);
    for I:= 1 to TagCount do begin
      Tmp:= Ini.ReadString('logging', IntToStr(I), '');
      if Tmp <> '' then begin
        Tag:= Named.AquireTag(PChar(Tmp), 'TStringTag', nil);
        if Tag <> nil then
          TagList.Add(Tag);
      end;
    end;

    Tmp:= Ini.ReadString('logging', 'foldertag', '');
    if(tmp <> '') then
      FolderTag:= Named.AquireTag(PChar(Tmp), 'TStringTag', nil);

    AbsolutePath := Ini.ReadString('logging', 'AbsolutePath', NamedPlugin.DllLoggingPath);

    OutputPath:= Ini.ReadString('logging', 'outputpath', '');
    Tmp:= Ini.ReadString('logging', 'filenametag', '');
    if Tmp <> '' then begin
      FilenameTag:= Named.AquireTag(PChar(Tmp), 'TStringTag', nil);
      FilenameSuffix:= Ini.ReadString('logging', 'filenamesuffix', 'log');
      OutputPath:= ValidFilename(OutputPath);
      Named.EventLog('Test ' + NamedPlugin.DllProfilePath );

      OutputPath:= IncludeTrailingPathDelimiter(AbsolutePath + OutputPath);
      ForceDirectories(OutputPath);
    end;
  finally
    Ini.Free;
  end;
end;

function FormatDate(DT: TDateTime): string;
var ST : TSystemTime;
begin
  DateTimeToSystemTime(DT, ST);
  Result := Format('%0.2d/%0.2d/%.4d %0.2d:%0.2d:%0.2d',
     [ST.wDay, ST.wMonth, ST.wYear, ST.wHour, ST.wMinute, ST.wSecond ]  );
end;


{ Append a csv record }
procedure TPlcLogging.WriteLog;
var OutString: string;
    Stream: TFileStream;
    Filename: string;
    TagValue: array[0..255] of Char;
    Folder: string;
    I: Integer;
begin
  Folder:= OutputPath;

  if FilenameTag <> nil then begin

    if FolderTag <> nil then begin
      Named.GetAsString(FolderTag, TagValue, 255);
      Folder:= Folder + ValidFilename(TagValue) + '\';
      ForceDirectories(Folder);
    end;

    Named.GetAsString(FilenameTag, TagValue, 255);
    Filename:= Folder + ValidFilename(TagValue) + '.' + FilenameSuffix;

    try
      if FileExists(Filename) then begin
        Named.EventLog(ConfigName + ' - Opening existing file: ' + filename);
        Stream:= TFileStream.Create(Filename, fmOpenWrite)
      end else begin
        Named.EventLog(ConfigName + ' - Creating new file: ' + filename);
        Stream:= TFileStream.Create(Filename, fmCreate);
      end;

      try
        Stream.Seek(0, soFromEnd);


        //OutString:= DateTimeToStr(StartTime) + ',' + DateTimeToStr(EndTime);
        OutString:= FormatDate(StartTime) + ',' + FormatDate(EndTime);

        for I:= 0 to TagList.Count - 1 do begin
          Named.GetAsString(TagList[I], TagValue, 255);
          OutString:= OutString + ',' + CncTypes.CSVField(TagValue);
        end;
        OutString:= OutString + CarRet;
        Stream.Write(PChar(OutString)^, Length(OutString));
      finally
        Named.EventLog(ConfigName + ' - Closing file: ' + filename);
        Stream.Free;
      end;
    except
      Named.EventLog(ConfigName + ' - Failed to append field to: ' + Filename);
    end;
  end;
end;

initialization
  TPlcLogging.AddPlugin('Logging', TPlcLogging);
end.
