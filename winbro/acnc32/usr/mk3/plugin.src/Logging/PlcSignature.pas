unit PlcSignature;

interface

uses SysUtils, Classes, CncTypes, AbstractPlcPlugin, NamedPlugin, IniFiles,
     INamedInterface;

type
  TPlcSignatureValue = (
    psvRun,
    psvSample
  );

  TPlcSignatureValues = set of TPlcSignatureValue;

  TPlcSignature = class(TAbstractPlcPlugin)
  private
    StartTime: Double;
    EndTime: Double;
    TagList: TList;
    OutputPath: string;
    FilenameTag: TTagRef;
    FilenameSuffix: string;
    FolderTag: TTagRef;
    OutList: TStringList;
    Values: TPlcSignatureValues;
    MaxLogLength: Integer;
    Header: string;

    procedure ReadConfig(Path: string);
    procedure Append;
    procedure WriteLog;
  public
    constructor Create( Parameters : string;
                        ResolveDeviceEv : TPlcPluginResolveDeviceEv;
                        ResolveItemEv : TPlcPluginResolveItemEv); override;
    destructor Destroy; override;
    procedure Compile ( var Embedded : TEmbeddedData;
                        aReadEv : TPlcPluginReadEv;
                        ResolveItemEv : TPlcPluginResolveItemEv;
                        Write : Boolean);  override;
    procedure GatePre; override;
    function GatePost : Integer; override;
    function Read(var Embedded : TEmbeddedData) : Boolean; override;
    procedure Write(var Embedded : TEmbeddedData; Line, LastLine : Boolean); override;
    function GateName(var Embedded : TEmbeddedData) : string; override;
    function ItemProperties : string; override;
  end;

implementation

const
  PropertyName : array [TPlcSignatureValue] of TPlcProperty = (
    ( Name : 'Run';     ReadOnly : False ),
    ( Name : 'Clock';   ReadOnly : False )
  );

{ TPlcSignature }

procedure TPlcSignature.Write(var Embedded: TEmbeddedData; Line,
  LastLine: Boolean);
begin
  if Line <> LastLine then begin  // Edge detect rising / falling
    case TPlcSignatureValue(Embedded.Command) of
      psvRun: begin
        if Line then begin
          StartTime:= Now;
          OutList.Clear;
          Append;
        end else begin
          EndTime:= Now;
          WriteLog;
        end;
      end;

      psvSample: begin
        if Line and (psvRun in Values) then begin
          Append;
        end;
      end;
    end;
    if Line then
      Include(Values, TPlcSignatureValue(Embedded.Command))
    else
      Exclude(Values, TPlcSignatureValue(Embedded.Command));
  end;
end;

procedure TPlcSignature.Compile(var Embedded: TEmbeddedData;
  aReadEv: TPlcPluginReadEv; ResolveItemEv: TPlcPluginResolveItemEv;
  Write: Boolean);
var Token: string;
    I: TPlcSignatureValue;
begin
  Token := aReadEv(Self);
  if Token <> '.' then
    raise Exception.Create('Period [.] expected');

  Token := aReadEv(Self);
  for I := Low(I) to High(I) do begin
    if CompareText(PropertyName[I].Name, Token) = 0 then begin
      Embedded.Command := Ord(I);
      Exit;
    end;
  end;
  raise Exception.CreateFmt('Unknown property [%s]', [Token]);
end;

function TPlcSignature.GateName(var Embedded: TEmbeddedData): string;
begin
  Result:= '';
end;

constructor TPlcSignature.Create(Parameters: string;
  ResolveDeviceEv: TPlcPluginResolveDeviceEv;
  ResolveItemEv: TPlcPluginResolveItemEv);
var Token: string;
begin
  Token:= ReadDelimited(Parameters, ';');
  TagList:= TList.Create;
  ReadConfig(NamedPlugin.DllProfilePath + 'ObjectPlc\' + Token + '.cfg');
  OutList:= TStringList.Create;
end;

destructor TPlcSignature.Destroy;
begin
  TagList.Free;
  OutList.Free;
  inherited;
end;

function TPlcSignature.ItemProperties: string;
var I: TPlcSignatureValue;
begin
  Result:= '';
  for I := Low(I) to High(I) do
    Result := Result + PropertyName[I].Name + CarRet + BooleanIdent[I in Values] + CarRet;
end;

function TPlcSignature.GatePost: Integer;
begin
  Result:= 0;
end;

function TPlcSignature.Read(var Embedded: TEmbeddedData): Boolean;
begin
  Result := TPlcSignatureValue(Embedded.Command) in Values;
end;

procedure TPlcSignature.GatePre;
begin
end;

procedure TPlcSignature.ReadConfig(Path: string);
var Ini: TIniFile;
    I, TagCount: Integer;
    Tmp: string;
    Tag: TTagRef;
    AbsolutePath: string;
begin
  Ini:= TIniFile.Create(Path);
  try
    MaxLogLength:= Ini.ReadInteger('logging', 'MaxLogLength', 3000);
    TagCount:= Ini.ReadInteger('logging', 'tagcount', 0);
    Header:= 'Running';
    for I:= 1 to TagCount do begin
      Tmp:= Ini.ReadString('logging', IntToStr(I), '');
      if Tmp <> '' then begin
        Tag:= Named.AquireTag(PChar(Tmp), 'TStringTag', nil);
        Header:= Header + ',' + Tmp;
        if Tag <> nil then
          TagList.Add(Tag);
      end;
    end;
    Header:= Header + CarRet;

    Tmp:= Ini.ReadString('logging', 'foldertag', '');
    if(tmp <> '') then
      FolderTag:= Named.AquireTag(PChar(Tmp), 'TStringTag', nil);

    AbsolutePath := Ini.ReadString('logging', 'AbsolutePath', NamedPlugin.DllLoggingPath);

    OutputPath:= Ini.ReadString('logging', 'outputpath', '');
    Tmp:= Ini.ReadString('logging', 'filenametag', '');
    if Tmp <> '' then begin
      FilenameTag:= Named.AquireTag(PChar(Tmp), 'TStringTag', nil);
      FilenameSuffix:= Ini.ReadString('logging', 'filenamesuffix', 'log');
      OutputPath:= ValidFilename(OutputPath);
      Named.EventLog('Test ' + NamedPlugin.DllProfilePath );

      OutputPath:= IncludeTrailingPathDelimiter(AbsolutePath + OutputPath);
      ForceDirectories(OutputPath);
    end;
  finally
    Ini.Free;
  end;
end;

procedure TPlcSignature.WriteLog;
var OutString: string;
    Stream: TFileStream;
    Filename: string;
    Folder: string;
    I: Integer;
    TagValue: array [0..255] of Char;
begin
  Folder:= OutputPath;

  if FilenameTag <> nil then begin
    if FolderTag <> nil then begin
      Named.GetAsString(FolderTag, TagValue, 255);
      Folder:= Folder + ValidFilename(TagValue) + '\';
      ForceDirectories(Folder);
    end;

    Named.GetAsString(FilenameTag, TagValue, 255);
    Filename:= Folder + ValidFilename(TagValue) + '.' + FilenameSuffix;

    try
      if FileExists(Filename) then begin
        Stream:= TFileStream.Create(Filename, fmOpenWrite);
      end else
        Stream:= TFileStream.Create(Filename, fmCreate);

      try
        Stream.Seek(0, soFromEnd);
        OutString:= 'Start=' + DateToStr(StartTime) + CarRet;
        Stream.Write(PChar(OutString)^, Length(OutString));
        Stream.Write(PChar(Header)^, Length(Header));
        for I:= 0 to OutList.Count - 1 do begin
          OutString:= OutList[I] + CarRet;
          Stream.Write(PChar(OutString)^, Length(OutString));
        end;
      finally
        Stream.Free;
      end;
    except
      Named.EventLog('Failed to append field to: ' + Filename);
    end;
  end;
end;

procedure TPlcSignature.Append;
var I: Integer;
    OutString: string;
    TagValue: array[0..255] of Char;
    Tmp: Extended;
begin
  if OutList.Count < MaxLogLength then begin
    Tmp:= (Now - StartTime) * 86400;
    Outstring:= Format('%.4f', [Tmp]);
    for I:= 0 to TagList.Count - 1 do begin
      Named.GetAsString(TagList[I], TagValue, 255);
      OutString:= OutString + ',' + CncTypes.CSVField(TagValue);
    end;

    OutList.Add(OutString);
  end;
end;

initialization
  TPlcSignature.AddPlugin('Signature', TPlcSignature);
end.
