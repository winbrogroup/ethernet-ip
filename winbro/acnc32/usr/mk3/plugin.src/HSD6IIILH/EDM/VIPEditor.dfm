object VIPForm: TVIPForm
  Left = 569
  Top = 665
  Width = 456
  Height = 290
  Caption = 'VIPForm'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'MS Sans Serif'
  Font.Style = [fsBold]
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 16
  object Panel1: TPanel
    Left = 0
    Top = 219
    Width = 448
    Height = 41
    Align = alBottom
    TabOrder = 0
    object BitBtn1: TBitBtn
      Left = 16
      Top = 2
      Width = 85
      Height = 36
      TabOrder = 0
      Kind = bkOK
    end
    object BitBtn2: TBitBtn
      Left = 112
      Top = 2
      Width = 85
      Height = 36
      TabOrder = 1
      Kind = bkCancel
    end
    object ApplyBtn: TBitBtn
      Left = 208
      Top = 2
      Width = 85
      Height = 36
      Caption = '&Apply'
      TabOrder = 2
      OnClick = ApplyBtnClick
      Glyph.Data = {
        DE010000424DDE01000000000000760000002800000024000000120000000100
        0400000000006801000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333444444
        33333333333F8888883F33330000324334222222443333388F3833333388F333
        000032244222222222433338F8833FFFFF338F3300003222222AAAAA22243338
        F333F88888F338F30000322222A33333A2224338F33F8333338F338F00003222
        223333333A224338F33833333338F38F00003222222333333A444338FFFF8F33
        3338888300003AAAAAAA33333333333888888833333333330000333333333333
        333333333333333333FFFFFF000033333333333344444433FFFF333333888888
        00003A444333333A22222438888F333338F3333800003A2243333333A2222438
        F38F333333833338000033A224333334422224338338FFFFF8833338000033A2
        22444442222224338F3388888333FF380000333A2222222222AA243338FF3333
        33FF88F800003333AA222222AA33A3333388FFFFFF8833830000333333AAAAAA
        3333333333338888883333330000333333333333333333333333333333333333
        0000}
      NumGlyphs = 2
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 65
    Height = 219
    Align = alLeft
    BevelOuter = bvNone
    TabOrder = 1
    object HVLabel: TLabel
      Left = 8
      Top = 8
      Width = 22
      Height = 16
      Caption = 'HV'
    end
    object NegativeBtn: TSpeedButton
      Left = 0
      Top = 160
      Width = 65
      Height = 57
      AllowAllUp = True
      GroupIndex = 1
      Caption = 'Neg'
      OnClick = NegativeBtnClick
    end
    object HVUpDown: TUpDown
      Left = 0
      Top = 24
      Width = 65
      Height = 105
      Min = 88
      Max = 300
      Increment = 4
      Position = 88
      TabOrder = 0
      OnClick = HVUpDownClick
    end
  end
  object Panel3: TPanel
    Left = 65
    Top = 0
    Width = 383
    Height = 219
    Align = alClient
    BevelOuter = bvLowered
    TabOrder = 2
    DesignSize = (
      383
      219)
    object BusVoltageGroup: TRadioGroup
      Left = 282
      Top = 1
      Width = 100
      Height = 217
      Align = alRight
      Caption = 'Bus Voltage'
      Items.Strings = (
        '78V'
        '100V')
      TabOrder = 0
      OnClick = BusVoltageGroupClick
    end
    object DrawPanel: TPanel
      Left = 8
      Top = 16
      Width = 259
      Height = 184
      Anchors = [akLeft, akTop, akRight, akBottom]
      BevelOuter = bvNone
      TabOrder = 1
      object HVPanel: TPanel
        Left = 0
        Top = 32
        Width = 10
        Height = 152
        BevelOuter = bvNone
        Color = clWhite
        TabOrder = 0
      end
      object BusPanel: TPanel
        Left = 8
        Top = 120
        Width = 249
        Height = 64
        BevelOuter = bvNone
        Color = clWhite
        TabOrder = 1
        DesignSize = (
          249
          64)
        object SpeedButton2: TSpeedButton
          Left = 128
          Top = 20
          Width = 90
          Height = 40
          Anchors = [akLeft, akTop, akBottom]
          Caption = 'I-'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -22
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TSpeedButton
          Left = 16
          Top = 20
          Width = 90
          Height = 40
          Anchors = [akLeft, akTop, akBottom]
          Caption = 'I+'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -22
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          OnClick = SpeedButton1Click
        end
        object PeakLabel: TLabel
          Left = 64
          Top = 0
          Width = 66
          Height = 16
          Caption = 'PeakLabel'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
      end
    end
  end
end
