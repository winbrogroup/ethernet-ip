unit EDMPlc;

interface

uses
  Classes, AbstractPlcPlugin, EDMReaderWriter, SysUtils, PmacServo, AbstractOPPPlugin,
  LPE577, LPE482, Windows, NamedPlugin, CNCTypes, INamedInterface, IFSDServer,
  DepthMonitor, ISignature, ASVCalibration;

type
  TPlcEDMHeadProperty = (
    pepEnable,            // Disables all communication to devices
    pepEDM,               // Runs EDM program while true
    pepContinue,          // EDM processing uses last first spark position
    pepStoreFirstSpark,   // EDM processing uses last first spark position
    pepRedress,           // Block processing uses rules  for redress
    pepServo,             // Enables EDM servo
    pepDCM,               // Puts the EDM head into depth control mode
    pepHoleProgram,       // Use to assign active hole program number.
    pepStandOff,          // Distance to pullback in microns
    pepFromFirstSpark,    // Standoff applies to first spark position
    pepFromDepth,         // Standoff applied to end of cut
    pepResetIDetect,      // Reset LPE482 IDetect
    pepSuppression,       // Set suppression enable in lpe577
//    pepElectrodeCount,    // Sets the number of electrodes in the cartrige
    pepRemoveOffset,      // Remove head offset
    pepNextBlock,         // Rising edge forces action of next block
    pepRefresh,
    pepForceFirstSpark,
    pepReverseDrilling,
    pepTFReset,
    pepPTEnable,
    pepSynch,

    pepFrozen,            // In servo mode the head locks in its current position
    pepEDMRelay,          // True when EDM relay is required
    pepComplete,          // True when while EDM is demanded and block-processing complete
    pepRetracted,         // True when the Servo has retracted.
    pepFirstSpark,        // True when EDM demanded and First spark found.
    pepAltBit0,           // Bit 0 of OEM bits
    pepAltBit1,           // Bit 1 of OEM bits
    pepAltBit2,           // Bit 2 of OEM bits
    pepAltBit3,           // Bit 3 of OEM bits
    pepInvalidProgram,    // Program number is invalid
    pepFailedFirstSpark,  // Program failed to find first spark
    pepFailedRedress,     // Program failed to redress
    pepCapacitance,       // Capacitance is selected
    pepOffsetPresent,
    pepDCSuppon,
    pepHVHealthy,
    pepFPGA,
    pepTFail,
    pepOverpower,
    pepSupplyHealthy,
    pepElectrodeSense,
    pepRetractError,
    pepInSuppression,
    pepElectrodeOverSense,
    pepExternalControl,
    pepCapacitance1,
    pepCapacitance2,
    pepCapacitance3,
    pepCapacitance4,
    pepCapacitance5,
    pepHighBus,
    pepLowBus
  );

  TPlcEDMHeadProperties = set of TPlcEdmHeadProperty;

  TElectrodeMask = record
    case Byte of
      0 : (
        W1, W2, W3 : Word;
      );
      1 : (
        B : array [0..5] of Byte;
      );
  end;

  TIndexMap = record
    Electrode, Offset, Mask, Bit : Integer;
  end;
  PIndexMap = ^TIndexMap;

  TDataCapture = class(TObject)
  private
    Data : array [0..50] of Integer;
    Ndx : Integer;
    FIntegration : Integer;
    FMin, FMax, FMean : Integer;
    procedure SetIntegration(aInt : Integer);
  public
    constructor Create;
    procedure AddData(aValue : Integer);
    procedure UpdateMinMax;
    procedure UpdateExtended;
    property Min : Integer read FMin;
    property Max : Integer read FMax;
    property Mean : Integer read FMean;
    property Integration : Integer read FIntegration write SetIntegration;
  end;

  //Copied from MK1.5
  TEDMStage = (
    esWaitServoOn,
    esWaitFirstSpark,
    esWaitForFeedBack1,
    esWaitRedress,
    esWaitForFeedBack2,
    esBlockProcess,
    esWaitRetracted,
    esComplete
  );

  THSD6IIEDMHeadPlcItem = class(TAbstractPlcPlugin, ISignatureClient)
  private
    Inputs : TPlcEDMHeadProperties;
    Index : Integer;
    Pmac : TPmacDevice;
    LPE577 : T577Device;
    FLPE482 : T482Device;
    ProgNumEv : TPlcPluginResolveItemValueEv; // Slightly out of spec (multiple compile instances)
    ProgramNumber : Integer;
    FBlockProcessing : Boolean;
    FActiveProgram : TEDMProgram;
    FActiveBlock : Integer;
    ThisUpdate : Integer;
    EDMStage : TEDMStage;
    Debug : Integer;
    FLPE577UpdateStage : Integer;
    ActualElectrodeMap : Int64;
    LastActualElectrodeMap : Int64;

    // All below are used for safe parameter switching
    LastBoards : Integer;
    LastCapacitance : Integer;
    LastBusVoltage : Integer;
    LastCoupling : Integer;
    LastNegative : Boolean;

    Delay : Cardinal; // Used for off delay in safe parameter change
    FSafeChange : Boolean;
    ThisTick : Cardinal;

    StartTime: TDateTime;
    FHadFirstSpark: Boolean;
    FirstSparkPos : Double;

    DM: TDepthMonitor;
    IOSynchState: Boolean;
    FeedbackPos: Integer; // Used to check update has occured
    ASVCalib: TASVCalibration;

    ExternalPTEnable: Boolean;
    InternalPTEnable: Boolean;

    procedure Update577;
    procedure SetActiveProgram(ProgID : Integer);
    procedure SetBlock(aBlock : Integer);
    function GetDepth : Double;
    procedure NewProgram(Sender : TObject);
    procedure UpdateFinalDepth;
  public
    ICapture : TDataCapture;
    VCapture : TDataCapture;
    property Values: TPlcEDMHeadProperties read Inputs;
    constructor Create( Parameters : string;
                        ResolveDeviceEv : TPlcPluginResolveDeviceEv;
                        ResolveItemEv : TPlcPluginResolveItemEv); override;
    procedure Compile ( var Embedded : TEmbeddedData;
                        aReadEv : TPlcPluginReadEv;
                        ResolveItemEv : TPlcPluginResolveItemEv;
                        Write : Boolean);  override;
    destructor Destroy; override;
    procedure GatePre; override;
    function GatePost : Integer; override;
    function Read(var Embedded : TEmbeddedData) : Boolean; override;
    procedure Write(var Embedded : TEmbeddedData; Line, LastLine : Boolean); override;
    function GateName(var Embedded : TEmbeddedData) : string; override;

    function GetNativeDepth: Integer;
    procedure SetFinalDepth(AFinal: Integer);

    function ItemProperties : string; override;
    class function Properties : string; override;
    property ActiveProgram : TEDMProgram read FActiveProgram;
    property ActiveBlock : Integer read FActiveBlock;
    property BlockProcessing : Boolean read FBlockProcessing;
    property Depth : Double read GetDepth;
    property LPE482 : T482Device read FLPE482;
    property SafeChange : Boolean read FSafeChange;

    class procedure FinalInstall; override;
    class procedure Install; override;

    property LPE577UpdateStage: Integer read FLPE577UpdateStage; //FLPE577UpdateStage;
    property FirstSpark: Boolean read FHadFirstSpark;
    procedure GateEnabled(Field: Integer; Domain: ICriterionDomain); stdcall;
    function EDMHeadPosition: Integer;
    procedure SwitchToExternalControl(Prog: TEDMProgram);
    procedure SwitchToInternalControl;
  end;

  THsd6Web = class(TInterfacedObject, IHTTPServerComponent)
  protected
    function HTTPGeneratePage(HFile : THANDLE; var CGIPath : WideString; CGIRequest: ICGIStringList) : Boolean; stdcall;
    function HTTPAddStreaming(Stream : THandle; CGIR : WideString; Method : IHTTPServer; DT : Integer) : Boolean; stdcall;
    procedure HTTPRemoveStreaming(Stream : THandle; CGIR : WideString); stdcall;
  public
    constructor Create;
  end;

  TElectrodeMap = record
    case Byte of
      0 : ( I : Int64 );
      1 : ( W : array [0..3] of Word );
  end;

var
  EDMHead : THSD6IIEDMHeadPlcItem;
  EMapTag : TTagRef;
  ActEMapTag : TTagRef;
  EmapCofNTag : TTagRef;
  VibAmpTag: TTagRef;
  VibFreTag: TTagRef;
  ElectrodeMap : TElectrodeMap;

  EMapTotal : Integer;
  EMapTotalAct : Integer;
  LoggingTag : TTagRef;
  FeatureIDTag: TTagRef;
  CutIDTag: TTagRef;

implementation


const
  PropertyName : array [TPlcEDMHeadProperty] of TPlcProperty = (
    ( Name : 'Enable';          ReadOnly : False ),
    ( Name : 'EDM';             ReadOnly : False ),
    ( Name : 'Continue';        ReadOnly : False ),
    ( Name : 'StoreFirstSpark'; ReadOnly : False ),
    ( Name : 'Redress';         ReadOnly : False ),
    ( Name : 'Servo';           ReadOnly : False ),
    ( Name : 'DCM';             ReadOnly : False ),
    ( Name : 'HoleProgram';     ReadOnly : False ),
    ( Name : 'StandOff';        ReadOnly : False ),
    ( Name : 'FromFirstSpark';  ReadOnly : False ),
    ( Name : 'FromDepth';       ReadOnly : False ),
    ( Name : 'ResetIDetect';    ReadOnly : False ),
    ( Name : 'Suppression';     ReadOnly : False ),
    ( Name : 'RemoveOffset';    ReadOnly : False ),
    ( Name : 'NextBlock';       ReadOnly : False ),
    ( Name : 'Refresh';         ReadOnly : False ),
    ( Name : 'ForceFirstSpark'; ReadOnly : False ),
    ( Name : 'ReverseDrilling'; ReadOnly : False ),
    ( Name : 'TFReset';         ReadOnly : False ),
    ( Name : 'PTEnable';        ReadOnly : False ),
    ( Name : 'IOSynch';         ReadOnly : False ),

    ( Name : 'Frozen';          ReadOnly : True ),
    ( Name : 'EDMRelay';        ReadOnly : True ),
    ( Name : 'Complete';        ReadOnly : True ),
    ( Name : 'Retracted';       ReadOnly : True ),
    ( Name : 'FirstSpark';      ReadOnly : True ),
    ( Name : 'AltBit0';         ReadOnly : True ),
    ( Name : 'AltBit1';         ReadOnly : True ),
    ( Name : 'AltBit2';         ReadOnly : True ),
    ( Name : 'AltBit3';         ReadOnly : True ),
    ( Name : 'InvalidProgram';  ReadOnly : True ),
    ( Name : 'FailedFirstSpark';ReadOnly : True ),
    ( Name : 'FailedRedress';   ReadOnly : True ),
    ( Name : 'Capacitance';     ReadOnly : True ),
    ( Name : 'OffsetPresent';   ReadOnly : True ),
    ( Name : 'DCSuppon';        ReadOnly : True ),
    ( Name : 'HVHealthy';       ReadOnly : True ),
    ( Name : 'FPGA';            ReadOnly : True ),
    ( Name : 'TFail';           ReadOnly : True ),
    ( Name : 'Overpower';       ReadOnly : True ),
    ( Name : 'SupplyHealthy';   ReadOnly : True ),
    ( Name : 'ElectrodeSense';  ReadOnly : True ),
    ( Name : 'RetractError';    ReadOnly : True ),
    ( Name : 'InSuppression';   ReadOnly : True ),
    ( Name : 'ElectrodeOverSense'; ReadOnly : True ),
    ( Name : 'ExternalControl'; ReadOnly : True ),
    ( Name : 'Capacitance1';    ReadOnly : True ),
    ( Name : 'Capacitance2';    ReadOnly : True ),
    ( Name : 'Capacitance3';    ReadOnly : True ),
    ( Name : 'Capacitance4';    ReadOnly : True ),
    ( Name : 'Capacitance5';    ReadOnly : True ),
    ( Name : 'LowBus';    ReadOnly : True ),
    ( Name : 'HighBus';    ReadOnly : True )
  );


function CountBits(I : Int64) : Integer;
begin
  Result := 0;

  while I <> 0 do begin
    if (I and 1) <> 0 then
      Inc(Result);
    I := I shr 1;
  end;
end;

procedure UpdateEmapCofN;
var A, B : Integer;
begin
  A := EmapTotalAct;
  B := EmapTotal;

  if A > B then
    A := B;

  Named.SetAsString(EmapCofNTag, PChar(Format('%d/%d', [A, B])));
end;

procedure EMapChange(aTag : TTagRef); stdcall;
var Buffer : array [0..63] of Char;
begin
  Named.GetAsString(EMapTag, Buffer, 64);
  try
    ElectrodeMap.I := StrToInt64('$' + Buffer);
  except
    ElectrodeMap.I := $100;
  end;
  EMapTotal := CountBits(ElectrodeMap.I);
  UpdateEmapCofN;
end;


constructor THSD6IIEDMHeadPlcItem.Create( Parameters : string;
                    ResolveDeviceEv : TPlcPluginResolveDeviceEv;
                    ResolveItemEv : TPlcPluginResolveItemEv);
var
  Token : string;
begin
  try
    Token := ReadDelimited(Parameters, ';');
    Index := StrToInt(Token);
  except
    raise Exception.CreateFmt('Incorrect index value for EDMHead [%s]', [Token]);
  end;
  if Index <> 1 then
    raise Exception.Create('Currently Index for edm head must be ''1''');

  Token := ReadDelimited(Parameters, ';');
  if not ResolveDeviceEv(Self, PChar(Token), Pmac.ISize, Pmac.OSize, Pointer(Pmac.Input), Pointer(Pmac.Output)) then
    raise Exception.CreateFmt('Cannot find device [%s] in IO', [Token]);

  if (Pmac.ISize < Sizeof(TPmacPlcIn)) or (Pmac.OSize <> Sizeof(TPmacPlcOut)) then
    raise Exception.CreateFmt('Wrong size of device [%s] expected [I%d, O%d]', [Token, Sizeof(TPmacPlcIn), Sizeof(TPmacPlcOut)]);

  Token := ReadDelimited(Parameters, ';');
  if not ResolveDeviceEv(Self, PChar(Token), LPE577.ISize, LPE577.OSize, Pointer(LPE577.Input), Pointer(LPE577.Output)) then
    raise Exception.CreateFmt('Cannot find device [%s] in IO', [Token]);

  if (LPE577.ISize < Sizeof(TLPE577In)) or (LPE577.OSize <> Sizeof(TLPE577Out)) then
    raise Exception.CreateFmt('Wrong size of device [%s] expected [I%d, O%d]', [Token, Sizeof(TLPE577In), Sizeof(TLPE577Out)]);


  Token := ReadDelimited(Parameters, ';');
  if not ResolveDeviceEv(Self, PChar(Token), FLPE482.ISize, FLPE482.OSize, Pointer(FLPE482.Input), Pointer(FLPE482.Output)) then
    raise Exception.CreateFmt('Cannot find device [%s] in IO', [Token]);

  if (LPE482.ISize < Sizeof(TLPE482In)) or (LPE482.OSize <> Sizeof(TLPE482Out)) then
    raise Exception.CreateFmt('Wrong size of device [%s] expected [I%d, O%d]', [Token, Sizeof(TLPE482In), Sizeof(TLPE482Out)]);

  SetActiveProgram(0);
  Pmac.Output.Command := [];
  LPE577.Output.Control := [Lpe577CntlSuppEna];
  Inputs := [pepSuppression];
  Pmac.Output.VoltageScaling := 50;
  Pmac.Output.ReverseThreshhold := 10;
  Pmac.Output.ForwardThreshold := 10;
  Pmac.Output.ShortVelocity := 10;
  Pmac.Output.ShortInterval := 2;
  FSafeChange := False;
  Exclude(Lpe577.Output.Control, Lpe577CntlSupp_RE);

  {
  if (lpe577StatNot577 in LPE577.Input^.Status) or
     not (lpe577Stat577 in LPE577.Input^.Status) then
     raise Exception.Create('Not an LPE577');
   }

  TAbstractOPPPluginConsumer.AddCallback(HSD6IISectionName, NewProgram);
  ICapture := TDataCapture.Create;
  VCapture := TDataCapture.Create;
  EDMHead := Self;
  Named.GetSignatureHost.AddClient(Self);
  DM:= TDepthMonitor.Create;

  ASVCalib:= TASVCalibration.Create;
  ASVCalib.Load(NamedPlugin.DllMachSpecPath + 'live\hsd6edm' + IntToStr(Index) + '.cfg');
end;

destructor THSD6IIEDMHeadPlcItem.Destroy;
begin
  EDMHead := nil;
  Named.GetSignatureHost.RemoveClient(Self);

  if Assigned(ICapture) then
    ICapture.Free;

  if Assigned(VCapture) then
    VCapture.Free;

  if Assigned(ASVCalib) then
    ASVCalib.Free;

  if Assigned(DM) then
    DM.Free;

  TAbstractOPPPluginConsumer.RemoveCallback(NewProgram);

{  if Assigned(Lock) then
    Lock.Free; }

  inherited Destroy;
end;

procedure THSD6IIEDMHeadPlcItem.Compile ( var Embedded : TEmbeddedData;
                    aReadEv : TPlcPluginReadEv;
                    ResolveItemEv : TPlcPluginResolveItemEv;
                    Write : Boolean);
var I : TPlcEDMHeadProperty;
    Token : string;
    IsConstant : Boolean;
begin
  Token := aReadEv(Self);
  if Token <> '.' then
    raise Exception.Create('Period [.] expected');

  Token := aReadEv(Self);
  for I := Low(TPlcEDMHeadProperty) to High(TPlcEDMHeadProperty) do begin
    if CompareText(PropertyName[I].Name, Token) = 0 then begin
      Embedded.Command := Ord(I);
      if (I = pepHoleProgram) or (I = pepStandoff){ or (I = pepElectrodeCount)} then begin
        Token := aReadEv(Self);
        if Token <> '.' then
          raise Exception.Create('Period [.] expected');
        Token := aReadEv(Self);
        Embedded.OData := ResolveItemEv(Self, PChar(Token), IsConstant, ProgNumEv);
        if Embedded.OData = nil then begin
          raise Exception.CreateFmt('Cannot resolve [%s] to a value', [Token]);
        end;
      end;
      Exit;
    end;
  end;
  raise Exception.CreateFmt('Unknown property [%s]', [Token]);
end;

procedure THSD6IIEDMHeadPlcItem.GatePre;
begin
end;

procedure THSD6IIEDMHeadPlcItem.SetBlock(aBlock : Integer);
begin
  if not (pepExternalControl in Inputs) then begin
    FValue := aBlock;
    if Assigned(ActiveProgram) then begin
      FActiveBlock := aBlock;

      if ActiveBlock >= ActiveProgram.BlockCount then begin
        Named.EventLog('EDM Block Count Exceeded, starting retraction');
        FActiveBlock := ActiveProgram.BlockCount - 1;
        EDMStage := esWaitRetracted;
      end;

      ActiveProgram[ActiveBlock].UpdateRequired := True;
    end;
  end;
end;

function THSD6IIEDMHeadPlcItem.GatePost : Integer;
var A, B : Int64;
begin
  EDMReaderWriter.Lock.Enter;
  try
    A := LPE482.Input^.Sense.W1 or $00E0;
    B := LPE482.Input^.Sense.W2;

    ActualElectrodeMap := (A shl 32) +
                          (B shl 16) +
                           LPE482.Input^.Sense.W3;
    ActualElectrodeMap := (not ActualElectrodeMap) and $FFFFFFFFFFFF;

    if LastActualElectrodeMap <> ActualElectrodeMap then begin
      Named.SetAsString(ActEMapTag, PChar('$' + IntToHex(ActualElectrodeMap, 12)));
      LastActualElectrodeMap := ActualElectrodeMap;
      EmapTotalAct := CountBits(ActualElectrodeMap and ElectrodeMap.I);
      UpdateEmapCofN;
    end;

    if ActualElectrodeMap and ElectrodeMap.I = ElectrodeMap.I then
      Include(Inputs, pepElectrodeSense)
    else
      Exclude(Inputs, pepElectrodeSense);

    if (ActualElectrodeMap and not ElectrodeMap.I) > 0 then
      Include(Inputs, pepElectrodeOverSense)
    else
      Exclude(Inputs, pepElectrodeOverSense);

    VCapture.AddData(Pmac.Input^.GapVolts);
    ICapture.AddData(Pmac.Input^.GapCurrent);

    with LPE577.Input^ do begin
      if lpe577StatDCSuppOn in Status then
        Include(Inputs, pepDCSuppOn)
      else
        Exclude(Inputs, pepDCSuppOn);

      if lpe577Stat1SupplyHealthy in Status1 then
        Include(Inputs, pepHVHealthy)
      else
        Exclude(Inputs, pepHVHealthy);

      if lpe577StatFPGASignal in Status then
        Include(Inputs, pepFPGA)
      else
        Exclude(Inputs, pepFPGA);

      if lpe577StatTFail in Status then
        Include(Inputs, pepTFail)
      else
        Exclude(Inputs, pepTFail);

      if lpe577Stat1OverPower in Status1 then
        Include(Inputs, pepOverpower)
      else
        Exclude(Inputs, pepOverpower);

      if lpe577Stat1SupplyHealthy in Status1 then
        Include(Inputs, pepSupplyHealthy)
      else
        Exclude(Inputs, pepSupplyHealthy);

      if lpe577Stat1Suppression in Status1 then
        Include(Inputs, pepInSuppression)
      else
        Exclude(Inputs, pepInSuppression);
    end;

    if pehsRetracted in Pmac.Input.Status then
      Include(Inputs, pepRetracted)
    else
      Exclude(Inputs, pepRetracted);

    if pehsOffsetPresent in Pmac.Input^.Status then
      Include(Inputs, pepOffsetPresent)
    else
      Exclude(Inputs, pepOffsetPresent);

    if pehsRetractError in Pmac.Input.Status then
      Include(Inputs, pepRetractError)
    else
      Exclude(Inputs, pepRetractError);

    if pehsFirstSpark in Pmac.Input^.Status then begin
      Include(Inputs, pepFirstSpark);
      FHadFirstSpark := True;
    end else
      Exclude(Inputs, pepFirstSpark);

    if (pepEnable in Inputs) then begin
      if Assigned(ActiveProgram) then begin
        if BlockProcessing and not (pepExternalControl in Inputs) then begin
          if pehsEDMOffRequest in Pmac.Input.Status then
            EDMStage := esWaitRetracted;

          case EDMStage of
            esWaitServoOn : begin
              if pepServo in Inputs then
                EDMStage := esWaitFirstSpark;
            end;

            esWaitFirstSpark : begin
              if FirstSparkPos < EDMHeadPosition then
                FirstSparkPos := EDMHeadPosition;

              if (pepContinue in Inputs) or (pehsFirstSpark in Pmac.Input.Status) then begin
                SetBlock(1);
                FeedbackPos:= EDMHeadPosition;
                EDMStage:= esWaitForFeedBack1;
              end;

              if ActiveProgram[0].Depth < (EDMHeadPosition div 10) then
                Include(Inputs, pepFailedFirstSpark)
              else
                Exclude(Inputs, pepFailedFirstSpark);
            end;

            esWaitForFeedBack1: begin
              if FeedbackPos <> EDMHeadPosition then begin
                if pepRedress in Inputs then
                  EDMStage := esWaitRedress
                else
                  EDMStage := esBlockProcess;
              end;
            end;

            esWaitRedress : begin
              if (pepElectrodeSense in Inputs) then begin
                Include(Pmac.Output^.Command, pehcForceFirstSpark);
                FeedbackPos:= EDMHeadPosition;
                EDMStage:= esWaitForFeedback2;
                SetBlock(2);
              end;
              if ActiveProgram[1].Depth < (EDMHeadPosition div 10) then
                Include(Inputs, pepFailedRedress);
            end;

            esWaitForFeedback2: begin
              // This waits until the position if forced to zero by the servo
              if EDMHeadPosition = 0 then begin
                EDMStage := esBlockProcess;
              end;
            end;

            esBlockProcess : begin
              Exclude(Pmac.Output^.Command, pehcForceFirstSpark);
              {
              if pehsFinalDepth in Pmac.Input.Status then begin
                EDMStage := esWaitRetracted;
                SetBlock(0);
              end;
              }

              if not (pepDCM in inputs) then
                if ActiveProgram[ActiveBlock].Depth <= (EDMHeadPosition div 10) then begin
                  //Named.EventLog('Block change to block ' + IntToStr(ActiveBlock + 1) + ' @ ' + IntToStr(EDMHeadPosition div 10));
                  SetBlock(ActiveBlock + 1);
                end;
            end;

            esWaitRetracted : begin
              Debug := 10;
              Exclude(Pmac.Output.Command, pehcServo);
              Include(Inputs, pepComplete);
              if pehsRetracted in Pmac.Input.Status then
                EDMStage := esComplete;
            end;

            esComplete : begin
              Debug := 11;
              FBlockProcessing := False;
              EDMStage := Low(EDMStage);
              SetBlock(0);
            end;
          end;
        end;
        Update577;
      end;
    end;
  finally
    EDMReaderWriter.Lock.Leave;
  end;
  Result := FValue;
end;

function THSD6IIEDMHeadPlcItem.GetDepth : Double;
begin
  if BlockProcessing then begin
    Result := EDMHeadPosition / 10000;
    DM.Monitor;
  end else
    Result := 0;
end;

procedure THSD6IIEDMHeadPlcItem.Update577;
  function BitReverse(normal : byte) : byte;
  begin
    asm
      MOV AL,NORMAL
      XOR AH,0
      RCR AL, 1
      RCL AH, 1
      RCR AL, 1
      RCL AH, 1
      RCR AL, 1
      RCL AH, 1
      RCR AL, 1
      RCL AH, 1
      RCR AL, 1
      RCL AH, 1
      RCR AL, 1
      RCL AH, 1
      RCR AL, 1
      RCL AH, 1
      RCR AL, 1
      RCL AH, 1
      MOV result, AH
    end
  end;

  procedure LPE577Voltage(Voltage : Double);
  var tmp : double;
      SV : Byte;
  begin
    tmp    := Abs(voltage) - 88;
    if tmp < 0 then tmp := 0;
    SV := Round(tmp * (255 / (300 - 88)));
    LPE577.Output^.Volts := BitReverse(SV);
  end;

  procedure LPE577Peak(aPeak : Byte; Negative : Boolean);
  begin
    with LPE577.Output^ do begin
      if Negative then Control := Control - [Lpe577CntlPositive]
                  else Control := Control + [Lpe577CntlPositive];

      Peak := BitReverse(aPeak and $0F);
    end;
    LastNegative := Negative;
  end;

  procedure LPE577PulseBoardsAndCoupling(aBoards : Integer; aCoupling : Integer);
  var aPeak, aPlc : Byte;
  begin
    aPeak := 0;
    aPlc := 0;

    if (1 and aCoupling) <> 0 then
      aPlc := aPlc or $80;

    if (2 and aCoupling) <> 0 then
      aPlc := aPlc or $40;

    if (4 and aCoupling) <> 0 then
      aPlc := aPlc or $20;

    if (8 and aCoupling) <> 0 then
      aPlc := aPlc or $10;

    if (16 and aCoupling) <> 0 then
      aPlc := aPlc or $8;

    if (32 and aCoupling) <> 0 then
      aPlc := aPlc or $4;


    if (1 and aBoards) <> 0 then
      aPeak := aPeak or 4;

    if (2 and aBoards) <> 0 then
      aPeak := aPeak or 8;

    if (4 and aBoards) <> 0 then
      aPeak := aPeak or 2;

    if (8 and aBoards) <> 0 then
      aPeak := aPeak or 1;

    if (16 and aBoards) <> 0 then
      aPlc := aPlc or 2;

    if (32 and aBoards) <> 0 then
      aPlc := aPlc or 1;

    with LPE577.Output^ do begin
      aPeak:= aPeak or $f0;
      Peak := (Peak or $0f) and aPeak;
      Plc := aPlc;
    end;

    LastBoards := aBoards;
    LastCoupling := aCoupling;
  end;

  procedure LPE577Cutoff(aCutoff : Double);
  var Tmp : Double;
  begin
    if aCutoff >= 0.1 then begin
      Tmp := aCutoff * (255 / 100); // Note the scaling of ASV is infact 0.0943
      LPE577.Output^.Cutoff := BitReverse(Round(Tmp));
      if pepSuppression in Inputs then
        Include(Lpe577.Output.Control, Lpe577CntlSuppEna);
    end else begin
      LPE577.Output^.Cutoff := 0;
      Exclude(Lpe577.Output.Control, Lpe577CntlSuppEna);
    end;
  end;

  procedure SetCapacitance(Capacitance : Integer);
  begin
    if (1 and Capacitance <> 0) then begin
      Include(LPE482.Output.Control, lpe482CtrlHeadCap0);
      Include(Inputs, pepCapacitance1);
    end else begin
      Exclude(LPE482.Output.Control, lpe482CtrlHeadCap0);
      Exclude(Inputs, pepCapacitance1);
    end;

    if (2 and Capacitance <> 0) then begin
      Include(LPE482.Output.Control, lpe482CtrlHeadCap1);
      Include(Inputs, pepCapacitance2);
    end else begin
      Exclude(LPE482.Output.Control, lpe482CtrlHeadCap1);
      Exclude(Inputs, pepCapacitance2);
    end;

    if (4 and Capacitance <> 0) then begin
      Include(LPE482.Output.Control, lpe482CtrlHeadCap2);
      Include(Inputs, pepCapacitance3);
    end else begin
      Exclude(LPE482.Output.Control, lpe482CtrlHeadCap2);
      Exclude(Inputs, pepCapacitance3);
    end;

    if (8 and Capacitance <> 0) then begin
      Include(LPE482.Output.Control, lpe482CtrlHeadCap3);
      Include(Inputs, pepCapacitance4);
    end else begin
      Exclude(LPE482.Output.Control, lpe482CtrlHeadCap3);
      Exclude(Inputs, pepCapacitance4);
    end;

    if (16 and Capacitance <> 0) then begin
      Include(LPE482.Output.Control, lpe482ctrlCapSelect);
      Include(Inputs, pepCapacitance5)
    end else begin
      Exclude(LPE482.Output.Control, lpe482ctrlCapSelect);
      Exclude(Inputs, pepCapacitance5)
    end;

    if (Capacitance and $F) <> 0 then
      Include(Inputs, pepCapacitance)
    else
      Exclude(Inputs, pepCapacitance);

    LastCapacitance := Capacitance;
  end;

  procedure SetBusVoltage(aBusVoltage : Integer);
  begin
    if aBusVoltage = 78 then begin
      Include(LPE482.Output.Control, lpe482ctrlBusVoltage78V);
      Include(Inputs, pepLowBus);
      Exclude(LPE482.Output.Control, lpe482ctrlBusVoltage100V);
      Exclude(Inputs, pepHighBus);
    end else begin
      Exclude(LPE482.Output.Control, lpe482ctrlBusVoltage78V);
      Exclude(Inputs, pepLowBus);
      Include(LPE482.Output.Control, lpe482ctrlBusVoltage100V);
      Include(Inputs, pepHighBus);
    end;
    LastBusVoltage := aBusVoltage;
  end;

begin
  Exclude(Lpe577.Output.Control, Lpe577CntlTrgEna);
  Include(Lpe577.Output.Control, Lpe577CntlMode);
  if (ThisUpdate <> ActiveBlock) and (LPE577UpdateStage = 0) then
    ThisUpdate := ActiveBlock;

  if ThisUpdate > ActiveProgram.BlockCount then begin
    ThisUpdate := 0;
    ActiveProgram[ThisUpdate].UpdateRequired:= True;
  end;

  with ActiveProgram[ThisUpdate] do begin
    if UpdateRequired then begin
      case LPE577UpdateStage of
        0 : begin
          FSafeChange := False;
          //Include(Lpe577.Output.Control, Lpe577CntlPTEna);
          InternalPTEnable:= True;
          LPE577.Output^.Mask   := $ff;
          LPE577Voltage(HighVoltage);
          LPE577Cutoff(ASVCalib.Voltage(ASV));

          Pmac.Output^.Gap := Round(GapVolts * 10);
          Pmac.Output^.Servo := Round(Servo * 5);
          Pmac.Output^.Spindle := Round(SpindleSpeed);
          Pmac.Output^.RRDepth:= RRDepth * 10;
          Pmac.Output^.RRServo:= Round(RRServo) div 100;
          Pmac.Output^.HP:= Round(HP * 2);
          Named.SetAsInteger(VibAmpTag, Round(VibAmp));
          Named.SetAsInteger(VibFreTag, Round(VibFreq));

          if (1 and AltBits) <> 0 then
            Include(Inputs, pepAltBit0)
          else
            Exclude(Inputs, pepAltBit0);

          if (2 and AltBits) <> 0 then
            Include(Inputs, pepAltBit1)
          else
            Exclude(Inputs, pepAltBit1);

          if (4 and AltBits) <> 0 then
            Include(Inputs, pepAltBit2)
          else
            Exclude(Inputs, pepAltBit2);

          if (8 and AltBits) <> 0 then
            Include(Inputs, pepAltBit3)
          else
            Exclude(Inputs, pepAltBit3);

          IOSynchState:= pepSynch in Inputs;

          LPE577.Output^.OnTimeL := SuppOnTime div 10;
          LPE577.Output^.OnTimeH := SuppOnTime div (10 * $100);

          LPE577.Output^.OffTimeL := SuppOffTime div 10;
          LPE577.Output^.OffTimeH := SuppOffTime div (10 * $100);

          UpdateFinalDepth;

          Inc(FLPE577UpdateStage);
        end;

        1 : begin
          if IOSynchState <> (pepSynch in Inputs) then begin
            Include(Lpe577.Output.Control, Lpe577CntlSupp_RE);
            Inc(FLPE577UpdateStage);
          end;
        end;

        2 : begin
          if IOSynchState = (pepSynch in Inputs) then begin
            LPE577.Output^.OnTimeL := OnTime div 10;
            LPE577.Output^.OnTimeH := OnTime div (10 * $100);

            LPE577.Output^.OffTimeL := OffTime div 10;
            LPE577.Output^.OffTimeH := OffTime div (10 * $100);
            Inc(FLPE577UpdateStage);
          end;
        end;

        3 : begin
          if IOSynchState <> (pepSynch in Inputs) then begin
            Exclude(Lpe577.Output.Control, Lpe577CntlSupp_RE);
            Inc(FLPE577UpdateStage);
          end;
        end;

        4 : begin
          Delay := 0;
          if BlockProcessing then begin
            // Starting at the highest delay and moving to the least
            if Capacitance <> LastCapacitance then
              Delay := 2000
            else if BusVoltage <> LastBusVoltage then
              Delay := 1000
            else if Negative <> LastNegative then
              Delay := 1000
            else if PulseBoards <> LastBoards then
              Delay := 500
            else if Coupling <> LastCoupling then
              Delay := 500;

            if Delay = 0 then begin
              FLPE577UpdateStage := 6;
            end else begin
              ThisTick := Windows.GetTickCount;
              //Exclude(Lpe577.Output.Control, Lpe577CntlPTEna);
              InternalPTEnable:= False;
              FLPE577UpdateStage := 5;
              FSafeChange := True;
              if pehcServo in Pmac.Output.Command then
                Include(Pmac.Output.Command, pehcFreeze);
            end;
          end else begin
            FLPE577UpdateStage := 6;
          end;
        end;

        5 : begin // Wait delay
          if (ThisTick + Delay) < Windows.GetTickCount then
            FLPE577UpdateStage := 6;
        end;

        6 : begin
          LPE577Peak(Peak, Negative);
          LPE577PulseBoardsAndCoupling(PulseBoards, Coupling);
          SetCapacitance(Capacitance);
          SetBusVoltage(BusVoltage);
          FLPE577UpdateStage := 7;
          ThisTick := Windows.GetTickCount;
          Delay := 95;
        end;

        7 : begin
          if (ThisTick + Delay) < Windows.GetTickCount then begin
            FSafeChange := False;
            FLPE577UpdateStage := 8;
            ThisTick := Windows.GetTickCount;
            Delay := 90;
          end;
        end;

        8 : begin
          if (ThisTick + Delay) < Windows.GetTickCount then begin
            FLPE577UpdateStage := 9;
            //Include(Lpe577.Output.Control, Lpe577CntlPTEna);
            InternalPTEnable:= True;
          end;
        end;

        9 : begin
          UpdateRequired := False;
          ThisUpdate := ActiveBlock;
          Exclude(Pmac.Output.Command, pehcFreeze);
          FLPE577UpdateStage := 0;
        end;
      end;
    end else begin
      Exclude(Lpe577.Output.Control, Lpe577CntlSupp_RE);
    end;
  end;
  if pepEDM in Inputs then begin
    if not SafeChange then
      Include(Inputs, pepEDMRelay)
    else
      Exclude(Inputs, pepEDMRelay)
  end else begin
    Exclude(LPE482.Output.Control, lpe482CtrlHeadCap0);
    Exclude(LPE482.Output.Control, lpe482CtrlHeadCap1);
    Exclude(LPE482.Output.Control, lpe482CtrlHeadCap2);
    Exclude(LPE482.Output.Control, lpe482CtrlHeadCap3);
    Exclude(LPE482.Output.Control, lpe482ctrlCapSelect);
    Exclude(Inputs, pepCapacitance);
    Exclude(Inputs, pepEDMRelay)
  end;

  if ExternalPTEnable and InternalPTEnable then
    Include(Lpe577.Output.Control, Lpe577CntlPTEna)
  else
    Exclude(Lpe577.Output.Control, Lpe577CntlPTEna);

end;

function THSD6IIEDMHeadPlcItem.Read(var Embedded : TEmbeddedData) : Boolean;
begin
  Result := TPlcEDMHeadProperty(Embedded.Command) in Inputs;
end;

procedure THSD6IIEDMHeadPlcItem.Write(var Embedded : TEmbeddedData; Line, LastLine : Boolean);
  procedure PmacCommand(pehc : TPmacEDMHeadCommand);
  begin
    if Line then
      Include(Pmac.Output.Command, pehc)
    else
      Exclude(Pmac.Output.Command, pehc);
  end;

  procedure SetElectrodeCount(aCnt : Integer);
  begin
{    ElectrodeCount := aCnt;
    if ElectrodeCount > 45 then
      ElectrodeCount := 45;
    if ElectrodeCount < 0 then
      ElectrodeCount := 0; }
  end;

  procedure EDMStateChange;
  var FeatureIDStr, CutIDStr: array [0..256] of Char;
  begin
    if not (pepInvalidProgram in Inputs) then begin
      if Line then begin
        //Include(Inputs, pepEDM);
        StartTime := Now;
        FBlockProcessing := True;
        EDMStage := Low(TEDMStage);
        Exclude(Inputs, pepComplete);
        FHadFirstSpark := False;
        FirstSparkPos := 0;

        // Cheap and cheerful EDM on sequencing
        FSafeChange := True;
        ActiveProgram[ActiveBlock].UpdateRequired := True;
        FLPE577UpdateStage := 6;
      end else begin
        Debug := 9;
        EDMStage := esWaitRetracted;
        Named.GetAsString(CutIDTag, CutIDStr, 255);
        Named.GetAsString(FeatureIDTag, FeatureIDStr, 255);
        Exclude(LPE482.Output.Control, lpe482CtrlCapSelect);
        //Exclude(Inputs, pepEDM);    { Should this be here ????}
        if FHadFirstSpark then begin
          Named.SetAsString(LoggingTag, PChar(StorageDate(StartTime) + ',' +
                                              StorageDate(Now) + ',' +
                                              IntToStr(ProgramNumber) + ',' +
                                              Format('%.3f', [FirstSparkPos / 10000]) + ',' +
                                              CNCTypes.CSVField(CutIDStr) + ','  +
                                              CNCTypes.CSVField(FeatureIDStr)
                                              ));
          FHadFirstSpark := False;
        end;
      end;
      //Named.EventLog('EDM Terminated by request');

      SetBlock(0);
      Exclude(Inputs, pepFailedFirstSpark);
      Exclude(Inputs, pepFailedRedress);
      Exclude(Pmac.Output^.Command, pehcForceFirstSpark);
      Exclude(Pmac.Output.Command, pehcFreeze);

      FSafeChange := False;
      DM.ClearAll;
      UpdateFinalDepth;
    end;
  end;

begin
  if Line <> LastLine then begin  // Edge detect rising / falling
    if pepEnable in Inputs then begin
      case TPlcEDMHeadProperty(Embedded.Command) of
        pepEDM :         EDMStateChange;
        pepContinue :    PmacCommand(pehcLastFirstSpark);
        pepStoreFirstSpark : PmacCommand(pehcStoreFirstSpark);
        pepServo :       PmacCommand(pehcServo);
        pepDCM :         PmacCommand(pehcDCM);
        pepHoleProgram : if Line then SetActiveProgram(ProgNumEv(Self, Embedded.OData));
        pepStandoff :    if Line then Pmac.Output.Standoff := ProgNumEv(Self, Embedded.OData);
//        pepElectrodeCount : if Line then SetElectrodeCount(ProgNumEv(Self, Embedded.OData));
        pepFromFirstSpark : PmacCommand(pehcFromFirstSpark);
        pepFromDepth :   PmacCommand(pehcFromDepth);
        pepRemoveOffset : PmacCommand(pehcRemoveOffset);
        pepNextBlock : if Line and BlockProcessing then
                         SetBlock(ActiveBlock + 1);
        pepRefresh :
         if Line and Assigned(ActiveProgram) then
                         ActiveProgram[ActiveBlock].UpdateRequired := True;

        pepForceFirstSpark : PmacCommand(pehcForceFirstSpark);
        pepReverseDrilling : PmacCommand(pehcReverseDrilling);
        pepResetIDetect :
          if Line then
            Include(LPE482.Output.Control, lpe482ctrlResetIDetect)
          else
            Exclude(LPE482.Output.Control, lpe482ctrlResetIDetect);

        pepSuppression : if Line then
            Include(LPE577.Output.Control, Lpe577CntlSuppEna)
          else
            Exclude(LPE577.Output.Control, Lpe577CntlSuppEna);

        pepTFReset : if Line then
            Include(LPE577.Output.Control, Lpe577CntlTFReset)
          else
            Exclude(LPE577.Output.Control, Lpe577CntlTFReset);

        pepPTEnable : ExternalPTEnable:= Line

      end;
    end;
    if Line then
      Include(Inputs, TPlcEDMHeadProperty(Embedded.Command))
    else
      Exclude(Inputs, TPlcEDMHeadProperty(Embedded.Command));
  end;
end;

procedure THSD6IIEDMHeadPlcItem.SetActiveProgram(ProgID : Integer);
begin
  Lock.Enter;
  try
    Exclude(Inputs, pepExternalControl);
    if not BlockProcessing then begin
      ProgramNumber := ProgID;
      FActiveProgram := EDMConsumer.EDMSection.FindProgram(ProgramNumber);
      if not Assigned(ActiveProgram) then begin
        Include(Inputs, pepInvalidProgram);
        Pmac.Output.HP:= 68 * 2;
      end else begin
        Exclude(Inputs, pepInvalidProgram);
        SetBlock(0);
      end;
    end;
  finally
    Lock.Leave;
  end;
end;

procedure THSD6IIEDMHeadPlcItem.NewProgram(Sender : TObject);
begin
  SetActiveProgram(ProgramNumber);
end;


function THSD6IIEDMHeadPlcItem.GateName(var Embedded : TEmbeddedData) : string;
begin
  Result := PropertyName[TPlcEDMHeadProperty(Embedded.Command)].Name;
end;

function THSD6IIEDMHeadPlcItem.ItemProperties : string;
var I : TPlcEDMHeadProperty;
begin
  Result := 'Program' + CarRet + IntToStr(ProgramNumber) + CarRet;
  for I := Low(TPlcEDMHeadProperty) to High(TPlcEDMHeadProperty) do
    Result := Result + PropertyName[I].Name + CarRet + BooleanIdent[I in Inputs] + CarRet;

  Result := Result + 'Debug' + CarRet + IntToStr(Debug) + CarRet;
  Result := Result + 'EDMStage' + CarRet + IntToStr(Ord(EDMStage)) + CarRet;
  Result := Result + 'ParamStage' + CarRet + IntToStr(LPE577UpdateStage) +CarRet;
  Result := Result + 'Delay' + CarRet + IntToStr(Delay) + CarRet;
  Result := Result + 'Elect. Map' + CarRet + IntToHex(ElectrodeMap.I, 12) + CarRet;
  Result := Result + 'Act. Elect. Map' + CarRet + IntToHex(ActualElectrodeMap, 12) + CarRet;
  Result := Result + 'VMean' + CarRet + IntToStr(VCapture.Mean) + CarRet;
  Result := Result + 'IMean' + CarRet + IntToStr(ICapture.Mean) + CarRet;
  Result := Result + 'GapV' + CarRet + IntToStr(Pmac.Input^.GapVolts);
end;

class procedure THSD6IIEDMHeadPlcItem.FinalInstall;
begin
  EMapTag := Named.AquireTag('OPP_ElectrodeMap', 'TStringTag', EMapChange);
  FeatureIDTag:= Named.AquireTag('FeatureID', 'TStringTag', nil);
  CutIDTag:= Named.AquireTag('CutID', 'TStringTag', nil);
  VibAmpTag:= Named.AquireTag('VIB_AMP', 'TIntegerTag', nil);
  VibFreTag:= Named.AquireTag('VIB_FREQ', 'TIntegerTag', nil);
  THsd6Web.Create;
end;

class procedure THSD6IIEDMHeadPlcItem.Install;
begin
  ActEMapTag := Named.AddTag('HSD6IIEDMHeadActElectrodeMask', 'TStringTag');
  EmapCofNTag := Named.AddTag('HSD6IISenseCofN', 'TStringTag');

  LoggingTag := Named.AddTag('EDMRecord1', 'TStringTag');
end;

class function THSD6IIEDMHeadPlcItem.Properties : string;
var I : TPlcEDMHeadProperty;
begin
  Result := '';
  for I := Low(TPlcEDMHeadProperty) to High(TPlcEDMHeadProperty) do
    Result := Result + PropertyName[I].Name + CarRet;
end;

{ TDataCapture }

constructor TDataCapture.Create;
begin
  FIntegration := 10;
end;

procedure TDataCapture.AddData(aValue: Integer);
begin
//  Data[0] := aValue;
//  FMean := aValue;
  Data[Ndx] := aValue;
  Ndx := (Ndx + 1) mod Integration;
end;

procedure TDataCapture.UpdateMinMax;
var I : Integer;
begin
  FMin := Data[0];
  FMax := FMin;
  FMean := FMin;
  for I := 1 to Integration - 1 do begin
    if FMin > Data[I] then
      FMin := Data[I];
    if FMax < Data[I] then
      FMax := Data[I];
    FMean := FMean + Data[I];
  end;

  FMean := FMean div Integration;
//  FMean := Data[0];
//  FMean := 40 + (IOSweep^ mod 30);
//  FMax := 60;
//  FMin := 40;
end;

procedure TDataCapture.UpdateExtended;
begin

end;


procedure TDataCapture.SetIntegration(aInt: Integer);
begin
  if aInt > 50 then
    aInt := 50;
  if aInt < 2 then
    aInt := 2;
  FIntegration := aInt;
end;

{ THsd6Web }

constructor THsd6Web.Create;
begin
  HttpServerHost.AddComponentPath(HttpHelpSection, 'hsd6ii', 'Edm control', Self);
end;

function THsd6Web.HTTPAddStreaming(Stream: THandle; CGIR: WideString;
  Method: IHTTPServer; DT: Integer): Boolean;
begin

end;

function THsd6Web.HTTPGeneratePage(HFile: THANDLE; var CGIPath: WideString;
  CGIRequest: ICGIStringList): Boolean;
begin
  WriteACNC32HTTPTitle(hFile, 'HSD6II EDM Controller');
  WriteACNC32HTTPHeading(hFile, 2, 'EDM Control');
end;

procedure THsd6Web.HTTPRemoveStreaming(Stream: THandle; CGIR: WideString);
begin

end;

procedure THSD6IIEDMHeadPlcItem.SetFinalDepth(AFinal: Integer);
begin
  {if Pmac.Output.FinalDepth <> AFinal then
    Named.EventLog('Setting Final depth to: ' + IntToStr(AFinal) + ' @ Depth: ' + IntToStr(EDMHeadPosition)); }
  Pmac.Output.FinalDepth:= AFinal;
end;

function THSD6IIEDMHeadPlcItem.GetNativeDepth: Integer;
begin
  if BlockProcessing then
    Result := EDMHeadPosition
  else
    Result := 0;
end;

procedure THSD6IIEDMHeadPlcItem.GateEnabled(Field: Integer;
  Domain: ICriterionDomain);
begin
  if Field = 0 then begin
    //Named.EventLog('Gate Enabled Called on field 0');
    DM.AddMonitorPoint(Domain.FieldHigh {+ 5000 Required for Snecma review required });
  end;
end;

procedure THSD6IIEDMHeadPlcItem.UpdateFinalDepth;
var I, Depth, Start: Integer;
begin
  //Pmac.Output.FinalDepth := ActiveProgram[0].Depth * 10;
  if pepRedress in Inputs then
    Start := 2
  else
    Start := 1;

  Depth := 0;

  for I := Start to ActiveProgram.BlockCount - 1 do
    if ActiveProgram[I].Depth > Depth then
      Depth:= ActiveProgram[I].Depth;

  //Named.EventLog('Setting final EDM depth');
  DM.AddMonitorPoint((Depth * 10) + 5000);
end;

function THSD6IIEDMHeadPlcItem.EDMHeadPosition: Integer;
begin
  Result:= Pmac.Input.Position;
end;

procedure THSD6IIEDMHeadPlcItem.SwitchToInternalControl;
begin
  SetActiveProgram(ProgramNumber);
end;

procedure THSD6IIEDMHeadPlcItem.SwitchToExternalControl(Prog: TEDMProgram);
begin
  if FActiveProgram <> nil then begin
    Include(Inputs, pepExternalControl);
    FActiveProgram:= Prog;
  end;
end;

initialization
  THSD6IIEDMHeadPlcItem.AddPlugin('HSD6IIEDMHead', THSD6IIEDMHeadPlcItem);
end.


