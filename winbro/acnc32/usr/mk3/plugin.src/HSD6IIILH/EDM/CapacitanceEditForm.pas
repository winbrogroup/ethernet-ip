unit CapacitanceEditForm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, NamedPlugin, EDMOptions;

type
  TCapacitanceApply = procedure(Sender : TObject; var Capacitance : Integer) of Object;
  TCapacitanceEditor = class(TForm)
    HeadButton1: TSpeedButton;
    HeadButton2: TSpeedButton;
    HeadButton3: TSpeedButton;
    HeadButton4: TSpeedButton;
    Panel1: TPanel;
    BitBtnOk: TBitBtn;
    BitBtnApply: TBitBtn;
    BitBtnCancel: TBitBtn;
    BoardButton1: TSpeedButton;
    HeadButton5: TSpeedButton;
    procedure FormCreate(Sender: TObject);
    procedure BitBtnApplyClick(Sender: TObject);
  private
    Capacitance : Integer;
    FApply : TCapacitanceApply;
    procedure ReadVisuals;
    procedure WriteVisuals;
  public
    class procedure Execute(var Cap : Integer; Apply : TCapacitanceApply);
    property Apply : TCapacitanceApply read FApply write FApply;
  end;

var
  CapacitanceEditor: TCapacitanceEditor;

implementation

{$R *.DFM}

class procedure TCapacitanceEditor.Execute(var Cap : Integer; Apply : TCapacitanceApply);
var Form : TCapacitanceEditor;
    Hold : Integer;
begin
  Form := TCapacitanceEditor.Create(Application);
  Form.HeadButton5.Enabled:= TEDMOptions.getInstance.GTCapacitorBox;
  //Form.BoardButton1.Enabled:= TEDMOptions.getInstance.GTCapacitorBox;

  if FontName <> '' then
    Form.Font.Name := FontName;
  Hold := Cap;
  Form.Apply := Apply;
  Form.Capacitance := Cap;
  Form.WriteVisuals;

  if Form.ShowModal = mrOK then begin
    Form.ReadVisuals;
    Cap := Form.Capacitance;
  end else begin
    Cap := Hold;
  end;

  Form.Free;
end;



procedure TCapacitanceEditor.ReadVisuals;
begin
  Capacitance := 0;
  Capacitance := Capacitance or
         Ord(HeadButton1.Down) or
         (Ord(HeadButton2.Down) shl 1) or
         (Ord(HeadButton3.Down) shl 2) or
         (Ord(HeadButton4.Down) shl 3);

  if TEDMOptions.getInstance.GTCapacitorBox then begin
    Capacitance := Capacitance or
         (Ord(HeadButton5.Down) shl 4) or
         (Ord(BoardButton1.Down) shl 5);
  end
  else begin
    Capacitance := Capacitance or
         (Ord(BoardButton1.Down) shl 4);
  end;

end;

procedure TCapacitanceEditor.WriteVisuals;
begin
  HeadButton1.Down := (Capacitance and 1) <> 0;
  HeadButton2.Down := (Capacitance and 2) <> 0;
  HeadButton3.Down := (Capacitance and 4) <> 0;
  HeadButton4.Down := (Capacitance and 8) <> 0;
  if TEDMOptions.getInstance.GTCapacitorBox then begin
    HeadButton5.Down := (Capacitance and 16) <> 0;
    BoardButton1.Down := (Capacitance and 32) <> 0;
  end
  else begin
    BoardButton1.Down := (Capacitance and 16) <> 0;
  end;
end;


procedure TCapacitanceEditor.BitBtnApplyClick(Sender: TObject);
begin
  ReadVisuals;
  if Assigned(Apply) then
    Apply(Self, Capacitance);

  WriteVisuals;
end;

procedure TCapacitanceEditor.FormCreate(Sender: TObject);
var I       : Integer;
    Translator  : TTranslator;
begin
  Translator := TTranslator.Create;
  try
    BitBtnOk.Caption := Translator.GetString('$OK');
    BitBtnApply.Caption := Translator.GetString('$APPLY');
    BitBtnCancel.Caption := Translator.GetString('$CANCEL');
    HeadButton1.Caption := Translator.GetString('$HEADCAP1');
    HeadButton2.Caption := Translator.GetString('$HEADCAP2');
    HeadButton3.Caption := Translator.GetString('$HEADCAP3');
    HeadButton4.Caption := Translator.GetString('$HEADCAP4');
    BoardButton1.Caption := Translator.GetString('$BOARDCAP');
  finally
    Translator.Free;
  end;
end;

end.
