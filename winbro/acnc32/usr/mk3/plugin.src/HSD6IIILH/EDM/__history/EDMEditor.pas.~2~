unit EDMEditor;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Grids, StdCtrls, Buttons, ComCtrls, ExtCtrls, AbstractOPPPlugin, EDMReaderWriter,
  Spin, NamedPlugin, ThreeLineButton, INamedInterface, EDMOptions;

type
  TEDMGrid = class(TStringGrid)
  private
    FDiff1, FDiff2 : TEDMProgram;
  public
    constructor Create(aOwner : TComponent); override;
    procedure DrawCell(ACol, ARow: Longint; ARect: TRect; AState: TGridDrawState); override;
    property Diff1 : TEDMProgram read FDiff1 write FDiff1;
    property Diff2 : TEDMProgram read FDiff2 write FDiff2;
  end;

  TEDMEditorForm = class(TForm)
    Panel1: TPanel;
    StatusBar: TStatusBar;
    Panel2: TPanel;
    BitBtn2: TBitBtn;
    BitBtn1: TBitBtn;
    CommentEdit: TEdit;
    Label2: TLabel;
    IDUpDown: TUpDown;
    BlockUpDown: TUpDown;
    OPPLiveBtn: TSpeedButton;
    EditCellSpeedButton: TSpeedButton;
    EDMTab: TTabControl;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
//    procedure NewProgramBtnClick(Sender: TObject);
    procedure IDUpDownClick(Sender: TObject; Button: TUDBtnType);
    procedure ProgramSelectChange(Sender: TObject);
    procedure CommentEditChange(Sender: TObject);
    procedure BlockUpDownClick(Sender: TObject; Button: TUDBtnType);
//    procedure DeleteProgramBtnClick(Sender: TObject);
    procedure OPPLiveBtnClick(Sender: TObject);
//    procedure CopyBlockBtnClick(Sender: TObject);
//    procedure PasteBlockBtnClick(Sender: TObject);
//   procedure CopyProgramBtnClick(Sender: TObject);
//    procedure PasteProgramBtnClick(Sender: TObject);
    procedure EditCellSpeedButtonClick(Sender: TObject);
//    procedure SpeedButton1Click(Sender: TObject);
//    procedure CopyLiveBtnClick(Sender: TObject);
  private
    ULCButton : TThreeLineButton;
    CopyBlockButton : TThreeLineButton;
    PasteBlockButton : TThreeLineButton;
    CopyProgButton   : TThreeLineButton;
    PasteProgramButton : TThreeLineButton;
    DeleteProgramButton : TThreeLineButton;
    NewProgramButton : TThreeLineButton;
    CopyLiveButton   : TThreeLineButton;
    Grid : TEDMGrid;
    FEditSection : TEDMSection;
    OPPSection : TEDMSection;
    FEditProgram : TEDMProgram;
    procedure UseLiveCellUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure CopyBlockButtonUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure PasteBlockButtonUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure CopyProgButtonUp (Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure PasteProgButtonUp (Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure DeleteProgButtonUp (Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure NewProgButtonUp (Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure CopyLiveButtonUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);

    procedure UpdateScreen;
    procedure SetEditProgram(aProgram : TEDMProgram);
    procedure SetEditSection(aSection : TEDMSection);
    procedure GridSelectCell(Sender: TObject; ACol, ARow: Longint; var CanSelect: Boolean);
    procedure GridSetEditText(Sender: TObject; ACol, ARow : Integer; const Value : string);
    procedure RedrawBlock(I : Integer);
    procedure UpdateTabs;
    procedure ApplyValue(Sender : TObject; var aValue : Double);
    procedure DCApply(Sender : TObject; var OnT, OffT, SOnT, SOffT : Integer);
    procedure PBApply(Sender : TObject; var aBoards, aCoupling : Integer);
    procedure VIPApply(Sender : TObject; var aHV, aBusV, aPeak : Integer; var aNegative : Boolean);
    procedure CapacitanceApply(Sender : TObject; var Cap : Integer);
  public
    property EditSection : TEDMSection read FEditSection write SetEditSection;
    property EditProgram : TEDMProgram read FEditProgram write SetEditProgram;
  end;

  // Creating a Editor requires one method, registration (see initialization)
  // And a suitable editor for the data.
  TEDMSectionEditor = class(TAbstractOPPPluginEditor)
    class function Execute(var Section : string) : Boolean; override;
  end;

const PolarityText : array[Boolean] of PChar = ( '+', '-' );
function BitField(AValue, AWidth : Integer) : string;
procedure SetParameter(EDMProg : TEDMProgram; ACol, ARow : Integer; const Value : string);

var
  EDMEditorForm: TEDMEditorForm;

resourcestring
  BlockLang = 'Block';
  DepthLang = 'Depth';
  PolarityLang = 'Pol';
  GapVoltsLang = 'GV';
  ServoLang = 'SV';
  PeakLang = 'I';
  HighVoltageLang = 'HV';
  OnTimeLang = 'On';
  OffTimeLang = 'Off';
  SuppOnTimeLang = 'S On';
  SuppOffTimeLang = 'S Off';
  CapacitanceLang = 'Cap';
  ASVLang = 'ASV';
  PulseBoardsLang = 'Brds';
  CouplingLang = 'Cpl''g';
  BusVoltsLang = 'Bus';
  AltBitsLang = 'Alt';
  SpindleSpeedLang = 'Spnd';
  RRServoLang = 'RRS';
  RRDepthLang = 'RRD';
  HPLang = 'Bar';

  VibAmpLang = 'VAmp';
  VibFreqLang = 'VFreq';

  OPPSectionDescriptionLang = 'Editing OPP Section';
  LiveSectionDescriptionLang = 'Viewing Live program buffer';
  UnknownSectionDescriptionLang = 'Editing unknown EDM section';

  ProgCountFormatLang = 'Prog. Count = %d';

implementation

{$R *.DFM}

uses Clipbrd, PulseBoardForm, DutyCycleEditor, VIPEditor, GeneralEditor, CapacitanceEditForm;

constructor TEDMGrid.Create(aOwner : TComponent);
begin
  inherited Create(aOwner);
  FixedColor := clWindow;
//  DefaultDrawing := False;
  Options := Options + [goEditing, goColSizing];
end;

procedure TEDMGrid.DrawCell(ACol, ARow: Longint; ARect: TRect; AState: TGridDrawState);
  function Different : Boolean;
  begin
    Result := True;
    if Assigned(Diff1) and
       Assigned(Diff2) and
       (Diff1.BlockCount > ARow - 1) and
       (Diff2.BlockCount > ARow - 1) then
      case ACol of
        1 : Result := Diff1[ARow - 1].Depth <> Diff2[ARow - 1].Depth;
        2 : Result := Diff1[ARow - 1].Negative <> Diff2[ARow - 1].Negative;
        3 : Result := Diff1[ARow - 1].GapVolts <> Diff2[ARow - 1].GapVolts;
        4 : Result := Diff1[ARow - 1].Servo <> Diff2[ARow - 1].Servo;
        5 : Result := Diff1[ARow - 1].HighVoltage <> Diff2[ARow - 1].HighVoltage;
        6 : Result := Diff1[ARow - 1].Peak <> Diff2[ARow - 1].Peak;
        7 : Result := Diff1[ARow - 1].OnTime <> Diff2[ARow - 1].OnTime;
        8 : Result := Diff1[ARow - 1].OffTime <> Diff2[ARow - 1].OffTime;
        9 : Result := Diff1[ARow - 1].SuppOnTime <> Diff2[ARow - 1].SuppOnTime;
        10: Result := Diff1[ARow - 1].SuppOffTime <> Diff2[ARow - 1].SuppOffTime;
        11: Result := Diff1[ARow - 1].Capacitance <> Diff2[ARow - 1].Capacitance;
        12: Result := Diff1[ARow - 1].ASV <> Diff2[ARow - 1].ASV;
        13: Result := Diff1[ARow - 1].PulseBoards <> Diff2[ARow - 1].PulseBoards;
        14: Result := Diff1[ARow - 1].Coupling <> Diff2[ARow - 1].Coupling;
        15: Result := Diff1[ARow - 1].BusVoltage <> Diff2[ARow - 1].BusVoltage;
        16: Result := Diff1[ARow - 1].AltBits <> Diff2[ARow - 1].AltBits;
        17: Result := Diff1[ARow - 1].SpindleSpeed <> Diff2[ARow - 1].SpindleSpeed;
        18: Result := Diff1[ARow - 1].RRServo <> Diff2[ARow - 1].RRServo;
        19: Result := Diff1[ARow - 1].RRDepth <> Diff2[ARow - 1].RRDepth;
        20: Result := Diff1[ARow - 1].HP <> Diff2[ARow - 1].HP;
        21: Result := Diff1[ARow - 1].VibAmp <> Diff2[ARow - 1].VibAmp;
        22: Result := Diff1[ARow - 1].VibFreq <> Diff2[ARow - 1].VibFreq;

      end;
  end;

  procedure DrawTriangle;
  var Triangle : array [0..2] of TPoint;
  begin
    Triangle[0] := Point(ARect.Right - 8, ARect.Top + 1);
    Triangle[1] := Point(ARect.Right - 1, ARect.Top + 1);
    Triangle[2] := Point(ARect.Right - 1, ARect.Top + 5);
    Canvas.Pen.Color := clRed;
    Canvas.Pen.Width := 1;
    Canvas.Brush.Color := clRed;
    Canvas.Polygon(Triangle);
  end;

var TE : TSize;
    L, T : Integer;
begin
  if gdFixed in AState then begin
    Canvas.Font.Style := [fsBold];
    Canvas.Brush.Color := clBtnFace;
    TE := Canvas.TextExtent(Cells[ACol, ARow]);
    L := ARect.Left + ((ARect.Right - ARect.Left) div 2) - (TE.cx div 2);
    T := ARect.Top + ((ARect.Bottom - ARect.Top) div 2) - (TE.cy div 2);
    Canvas.TextRect(aRect, L, T, Cells[ACol, ARow]);
  end else begin
    Canvas.Font.Style := [];
    Canvas.Brush.Color := clWindow;
    Canvas.TextRect(aRect, aRect.Left + 3, aRect.Top + 3, Cells[ACol, ARow]);

    if gdFocused in AState then begin
      Canvas.Pen.Color := $a0a000;
      Canvas.Pen.Width := 3;
      with ARect do begin
        Canvas.MoveTo(Left+2, Top+2);
        Canvas.LineTo(Right-2, Top+2);
        Canvas.LineTo(Right-2, Bottom-2);
        Canvas.LineTo(Left+2, Bottom-2);
        Canvas.LineTo(Left+2, Top+2);
      end;
    end;
    if Different then
      DrawTriangle;
  end;
//  Canvas.Pen.Width := 3;
//  Canvas.Font.Style := [];
end;



class function TEDMSectionEditor.Execute(var Section : string) : Boolean;
var Form : TEDMEditorForm;
begin
  Form := TEDMEditorForm.Create(Application);
  if NamedPlugin.FontName <> '' then
    Form.Font.Name := FontName;
  Form.OPPSection.Text := Section;
  Form.OPPSection.ReadFromStrings;
  Form.EditSection := Form.OPPSection;

  Result := Form.ShowModal = mrOK;
  if Result then begin
    Form.EditSection.WriteToStrings;
    Section := Form.EditSection.Text;
  end;

  Form.Free;
end;

procedure TEDMEditorForm.FormCreate(Sender: TObject);
var
Translator : TTranslator;
begin
  OPPSection := TEDMSection.Create;
  Grid := TEDMGrid.Create(Self);
  Grid.Parent := EDMTab;
  Grid.OnSelectCell := GridSelectCell;
  Grid.OnSetEditText := GridSetEditText;
  Grid.Align := alClient;
  Grid.ColCount := 23;
  Grid.RowCount := 1;
  Grid.Cells[0, 0] := BlockLang;
  Grid.Cells[1, 0] := DepthLang;
  Grid.Cells[2, 0] := PolarityLang;
  Grid.Cells[3, 0] := GapVoltsLang;
  Grid.Cells[4, 0] := ServoLang;
  Grid.Cells[5, 0] := HighVoltageLang;
  Grid.Cells[6, 0] := PeakLang;
  Grid.Cells[7, 0] := OnTimeLang;
  Grid.Cells[8, 0] := OffTimeLang;
  Grid.Cells[9, 0] := SuppOnTimeLang;
  Grid.Cells[10, 0] := SuppOffTimeLang;
  Grid.Cells[11, 0] := CapacitanceLang;
  Grid.Cells[12, 0] := ASVLang;
  Grid.Cells[13, 0] := PulseBoardsLang;
  Grid.Cells[14, 0] := CouplingLang;
  Grid.Cells[15, 0] := BusVoltsLang;
  Grid.Cells[16, 0] := AltBitsLang;
  Grid.Cells[17, 0] := SpindleSpeedLang;
  Grid.Cells[18, 0] := RRServoLang;
  Grid.Cells[19, 0] := RRDepthLang;
  Grid.Cells[20, 0] := HPLang;
  Grid.Cells[21, 0] := VibAmpLang;
  Grid.Cells[22, 0] := VibFreqLang;


  Grid.ColWidths[0] := 45;
  Grid.ColWidths[1] := 50;
  Grid.ColWidths[2] := 30;
  Grid.ColWidths[3] := 40;
  Grid.ColWidths[4] := 40;
  Grid.ColWidths[5] := 40;
  Grid.ColWidths[6] := 30;
  Grid.ColWidths[7] := 40;
  Grid.ColWidths[8] := 40;
  Grid.ColWidths[9] := 40;
  Grid.ColWidths[10] := 40;
  Grid.ColWidths[11] := 35;
  Grid.ColWidths[12] := 35;
  Grid.ColWidths[13] := 50;
  Grid.ColWidths[14] := 50;
  Grid.ColWidths[15] := 30;
  Grid.ColWidths[16] := 45;
  Grid.ColWidths[17] := 45;
  Grid.ColWidths[18] := 45;
  Grid.ColWidths[19] := 50;
  Grid.ColWidths[20] := 35;
  Grid.ColWidths[21] := 50;
  Grid.ColWidths[22] := 50;

  EDMTab.Font.Height := -15;

  Translator := TTranslator.Create;
  try

  ULCButton := TThreeLineButton.Create(Self);
  with ULCButton do
    begin
    Parent := Panel1;
    Width := 84;
    Top := 2;
    ButtonColor := clBtnFace;
    Legend := Translator.GetString('$USELIVECELL');
    LegendFont.Name := 'Arial';
    LegendFont.Color := clBlack;
    OnMouseUp := UseLiveCellUp;
    Left := 78;
    end;

  CopyBlockButton:= TThreeLineButton.Create(Self);
  with CopyBlockButton do
    begin
    Parent := Panel1;
    Width := 84;
    Top := 2;
    LegendFont.Name := 'Arial';
    LegendFont.Color := clBlack;
    ButtonColor := clBtnFace;
    Legend := Translator.GetString('$COPYBLOCK');
    OnMouseUp := CopyBlockButtonUp;
    Left := 166;
    end;

  PasteBlockButton:= TThreeLineButton.Create(Self);
  with PasteBlockButton do
    begin
    Parent := Panel1;
    Width := 84;
    Top := 2;
    LegendFont.Name := 'Arial';
    LegendFont.Color := clBlack;
    ButtonColor := clBtnFace;
    Legend := Translator.GetString('$PASTEBLOCK');
    OnMouseUp := PasteBlockButtonUp;
    Left := 252;
    end;

  CopyProgButton := TThreeLineButton.Create(Self);
  with CopyProgButton do
    begin
    Parent := Panel1;
    Width := 84;
    Top := 2;
    LegendFont.Name := 'Arial';
    LegendFont.Color := clBlack;
    ButtonColor := clBtnFace;
    Legend := Translator.GetString('$COPYPROGRAM');
    OnMouseUp := CopyProgButtonUp;
    Left := 252+86;
    end;

  PasteProgramButton := TThreeLineButton.Create(Self);
  with PasteProgramButton do
    begin
    Parent := Panel1;
    Width := 84;
    Top := 2;
    LegendFont.Name := 'Arial';
    LegendFont.Color := clBlack;
    ButtonColor := clBtnFace;
    Legend := Translator.GetString('$PASTEPROGRAM');
    OnMouseUp := PasteProgButtonUp;
    Left := 252+86+86;
    end;

  DeleteProgramButton:= TThreeLineButton.Create(Self);
  with DeleteProgramButton do
    begin
    Parent := Panel1;
    Width := 84;
    Top := 2;
    LegendFont.Name := 'Arial';
    LegendFont.Color := clBlack;
    ButtonColor := clBtnFace;
    Legend := Translator.GetString('$DELETEPROGRAM');
    OnMouseUp := DeleteProgButtonUp;
    Left := 252+86+86+86;
    end;

  NewProgramButton := TThreeLineButton.Create(Self);
  with NewProgramButton do
    begin
    Parent := Panel1;
    Width := 84;
    Top := 2;
    LegendFont.Name := 'Arial';
    LegendFont.Color := clBlack;
    ButtonColor := clBtnFace;
    Legend := Translator.GetString('$NEWPROGRAM');
    OnMouseUp := NewProgButtonUp;
    Left := 252+86+86+86+86;
    end;

  CopyLiveButton  := TThreeLineButton.Create(Self);
  with CopyLiveButton do
    begin
    Parent := Panel1;
    Width := 84;
    Top := 2;
    LegendFont.Name := 'Arial';
    LegendFont.Color := clBlack;
    ButtonColor := clBtnFace;
    Legend := Translator.GetString('$COPYLIVE');
    OnMouseUp := CopyLiveButtonUp;
    Left := 252+86+86+86+86+86;
    end;

  OPPLiveBtn.Caption := Translator.GetString('$OPPLIVE');
  EditCellSpeedButton.Caption := Translator.GetString('$EDITCELL');
  Label2.Caption   := Translator.GetString('$COMMENT');
  BitBtn2.Caption := Translator.GetString('$CANCEL');
  BitBtn1.Caption := Translator.GetString('$OK');

  finally
  Translator.Free;
  end;
end;

procedure TEDMEditorForm.FormDestroy(Sender: TObject);
begin
  Grid.Free;
  OPPSection.Free;
end;

procedure TEDMEditorForm.UpdateScreen;
var I : Integer;
begin
  if EditProgram <> nil then begin
    Grid.Visible := True;
    CommentEdit.Enabled := True;
    BlockUpDown.Enabled := True;
    IDUpDown.Enabled := True;
    Grid.RowCount := EditProgram.BlockCount + 1;
    Grid.FixedRows := 1;
    for I := 0 to EditProgram.BlockCount - 1 do begin
      Grid.Cells[0, I + 1] := IntToStr(I);
      RedrawBlock(I);
    end;
    CommentEdit.Text := EditProgram.Comment;
    IDUpDown.Position := EditProgram.ID;
    BlockUpDown.Position := EditProgram.BlockCount;

    StatusBar.Panels[0].Text := Format(ProgCountFormatLang, [EditSection.ProgramCount]);
  end else begin
    Grid.Visible := False;
    CommentEdit.Enabled := False;
    BlockUpDown.Enabled := False;
    IDUpDown.Enabled := False;
    EDMTab.Tabs.Clear;
//    ProgramSelect.Text := '';
  end;
end;

procedure TEDMEditorForm.UpdateTabs;
var I : Integer;
begin
  EDMTab.Tabs.Clear;
  for I := 0 to EditSection.ProgramCount - 1 do begin
    EDMTab.Tabs.Add(IntToStr(EditSection.Programs[I].ID));
    if EditSection.Programs[I] = EditProgram then
      EDMTab.TabIndex := I;
  end;
end;

function BitField(AValue, AWidth : Integer) : string;
var I : Integer;
begin
  Result := '';
  for I := 0 to AWidth - 1 do begin
    if (AValue and (1 shl I)) <> 0 then
      Result := Result + '1'
    else
      Result := Result + '0';
  end;
end;

procedure TEDMEditorForm.RedrawBlock(I : Integer);
begin
  Grid.Cells[1, I + 1] := TEDMOptions.GetInstance.DisplayDepth(EditProgram.Blocks[I].Depth);
  Grid.Cells[2, I + 1] := PolarityText[EditProgram.Blocks[I].Negative];
  Grid.Cells[3, I + 1] := Format('%.1f', [EditProgram.Blocks[I].GapVolts]);
  Grid.Cells[4, I + 1] := TEDMOptions.GetInstance.DisplayServo(EditProgram.Blocks[I].Servo);
  Grid.Cells[5, I + 1] := Format('%d', [EditProgram.Blocks[I].HighVoltage]);
  Grid.Cells[6, I + 1] := Format('%d', [EditProgram.Blocks[I].Peak]);
  Grid.Cells[7, I + 1] := TEDMOptions.GetInstance.DisplayTime(EditProgram.Blocks[I].OnTime);
  Grid.Cells[8, I + 1] := TEDMOptions.GetInstance.DisplayTime(EditProgram.Blocks[I].OffTime);
  Grid.Cells[9, I + 1] := TEDMOptions.GetInstance.DisplayTime(EditProgram.Blocks[I].SuppOnTime);
  Grid.Cells[10, I + 1] := TEDMOptions.GetInstance.DisplayTime(EditProgram.Blocks[I].SuppOffTime);
  Grid.Cells[11, I + 1] := Format('%d', [EditProgram.Blocks[I].Capacitance]);
  Grid.Cells[12, I + 1] := Format('%.1f', [EditProgram.Blocks[I].ASV]);
  Grid.Cells[13, I + 1] := BitField(EditProgram.Blocks[I].PulseBoards, 6);
  Grid.Cells[14, I + 1] := BitField(EditProgram.Blocks[I].Coupling, 6);
  Grid.Cells[15, I + 1] := Format('%d', [EditProgram.Blocks[I].BusVoltage]);
  Grid.Cells[16, I + 1] := BitField(EditProgram.Blocks[I].AltBits, 4);
  Grid.Cells[17, I + 1] := Format('%.1f', [EditProgram.Blocks[I].SpindleSpeed]);
  Grid.Cells[18, I + 1] := TEDMOptions.GetInstance.DisplayServo(EditProgram.Blocks[I].RRServo);
  Grid.Cells[19, I + 1] := TEDMOptions.GetInstance.DisplayDepth(EditProgram.Blocks[I].RRDepth);
  Grid.Cells[20, I + 1] := Format('%.1f', [EditProgram.Blocks[I].HP]);
  Grid.Cells[21, I + 1] := Format('%.1f', [EditProgram.Blocks[I].VibAmp]);
  Grid.Cells[22, I + 1] := Format('%.1f', [EditProgram.Blocks[I].VibFreq]);

end;

{procedure TEDMEditorForm.NewProgramBtnClick(Sender: TObject);
var I : Integer;
    NP : TEDMProgram;
begin
  NP := EditSection.NewProgram;
  NP.NewBlock;
  NP.NewBlock;
  for I := 0 to EditSection.ProgramCount - 1 do begin
    if (EditSection[I] <> NP) and (EditSection[I].ID >= NP.ID) then
      NP.ID := EditSection[I].ID + 1;
  end;

  EditProgram := NP;
  UpdateScreen;
  UpdateTabs;
end;}

procedure TEDMEditorForm.IDUpDownClick(Sender: TObject; Button: TUDBtnType);
begin
  if Assigned(EditProgram) then
    EditProgram.ID := IDUpDown.Position;

  EDMTab.Tabs[EDMTab.TabIndex] := IntToStr(EditProgram.ID);
//  UpdateScreen;
end;

procedure TEDMEditorForm.ProgramSelectChange(Sender: TObject);
begin
//  EditProgram := EditSection.Programs[ProgramSelect.ItemIndex];
  EditProgram := EditSection.Programs[EDMTab.TabIndex];
  UpdateScreen;
end;

procedure TEDMEditorForm.CommentEditChange(Sender: TObject);
begin
  if Assigned(EditProgram) then
    EditProgram.Comment := CommentEdit.Text;
end;

procedure TEDMEditorForm.BlockUpDownClick(Sender: TObject; Button: TUDBtnType);
var Block : TEDMBlock;
begin
  if Assigned(EditProgram) then begin
    case Button of
      btNext : begin
        Block := EditProgram[EditProgram.BlockCount - 1];
        EditProgram.NewBlock.Assign(Block);
      end;

      btPrev : begin
        if EditProgram.BlockCount > 1 then
          EditProgram.DeleteBlock(EditProgram.BlockCount - 1);
      end;
    end;
  end;

  UpdateScreen;
end;

{procedure TEDMEditorForm.DeleteProgramBtnClick(Sender: TObject);
begin
  EditSection.DeleteProgram(EditProgram);
  EditProgram := nil;
  UpdateScreen;
  UpdateTabs;
end;}

procedure TEDMEditorForm.GridSelectCell(Sender: TObject; ACol, ARow: Longint; var CanSelect: Boolean);
begin
  CanSelect := True;
end;

procedure TEDMEditorForm.SetEditSection(aSection : TEDMSection);
var ThisProg : Integer;
begin
  ThisProg := 1;
  if Assigned(EditProgram) then begin
    ThisProg := EditProgram.ID;
  end;

  FEditSection := aSection;
  if aSection = OPPSection then
    StatusBar.Panels[2].Text := OPPSectionDescriptionLang
  else if aSection = EDMConsumer.EDMSection then
    StatusBar.Panels[2].Text := LiveSectionDescriptionLang
  else
    StatusBar.Panels[2].Text := UnknownSectionDescriptionLang;

  EditProgram := EditSection.FindProgram(ThisProg);
  UpdateTabs;
end;

procedure TEDMEditorForm.SetEditProgram(aProgram : TEDMProgram);
begin
  FEditProgram := aProgram;
  if EditProgram = nil then begin
    if EditSection.ProgramCount > 0 then begin
      FEditProgram := EditSection.Programs[0];
    end else begin
      FEditProgram := nil;
      Grid.Diff1 := nil;
      Grid.Diff2 := nil;
    end;
  end;

  if Assigned(EditProgram) then begin
    Grid.Diff1 := OPPSection.FindProgram(EditProgram.ID);
    Grid.Diff2 := EDMConsumer.EDMSection.FindPRogram(EditProgram.ID);
  end;
  UpdateScreen;
end;



procedure TEDMEditorForm.GridSetEditText(Sender: TObject; ACol, ARow : Integer; const Value : string);
begin
  if not Grid.EditorMode then begin
    SetParameter(EditProgram, ACol, ARow, Value);
    EditProgram.Verify(ARow - 1);
    RedrawBlock(ARow - 1);
  end;
end;


procedure SetParameter(EDMProg : TEDMProgram; ACol, ARow : Integer; const Value : string);
var Code : Integer;
  function SetDouble(const aValue : string; Default : Double) : Double;
  begin
    Val(Value, Result, Code);
    if Code <> 0 then
      Result := Default;
  end;

  function SetInteger(const aValue : string; Default : Integer) : Integer;
  begin
    Val(Value, Result, Code);
    if Code <> 0 then
      Result := Default;
  end;

  function SetTime(const aValue : string; Default : Integer) : Integer;
  begin
    Result:= TEDMOptions.GetInstance.StorageTime(AValue, Default);
  end;

  function SetServo(const aValue : string; Default : Double) : Double;
  begin
    Result:= TEDMOptions.GetInstance.StorageServo(AValue, Default);
  end;

  function SetDepth(const aValue : string; Default : Integer) : Integer;
  //var Tmp : Double;
  begin
    Result:= TEDMOptions.GetInstance.StorageDepth(AValue, Default);
    {
    Val(Value, Result, Code);

    if Code <> 0 then begin
      Result := Default
    end else begin
      Tmp := Result * MetricToInch;
      Result := Round(Tmp);
    end;
    }
  end;

  function SetNegative(const aValue : string; Default : Boolean) : Boolean;
  begin
    if (aValue = '1') or (aValue = '+') then
      Result := False
    else if (aValue = '0') or (aValue = '-') then
      Result := True
    else
      Result := Default;
  end;

  function SetBitField(const aValue : string; Default : Integer) : Integer;
  var I : Integer;
  begin
    Result := 0;
    for I := 1 to Length(aValue) do begin
      case aValue[I] of
        '1' : Result := Result + (1 shl (I-1));
        '0' : Result := Result;
      else
        Result := Default;
        Exit;
      end;
    end;
  end;

begin
  if EDMProg.BlockCount >= ARow - 1 then begin
    with EDMProg.Blocks[ARow - 1] do begin
      case ACol of
        1 : Depth := SetDepth(Value, Depth);
        2 : Negative := SetNegative(Value, Negative);
        3 : GapVolts := SetDouble(Value, GapVolts);
        4 : Servo := SetServo(Value, Servo);
        5 : HighVoltage := SetInteger(Value, HighVoltage);
        6 : Peak := SetInteger(Value, Peak);
        7 : OnTime := SetTime(Value, OnTime);
        8 : OffTime := SetTime(Value, OffTime);
        9 : SuppOnTime := SetTime(Value, SuppOnTime);
        10: SuppOffTime := SetTime(Value, SuppOffTime);
        11: Capacitance := SetInteger(Value, Capacitance);
        12: ASV := SetDouble(Value, ASV);
        13: PulseBoards := SetBitField(Value, PulseBoards);
        14: Coupling := SetBitField(Value, Coupling);
        15: BusVoltage := SetInteger(Value, BusVoltage);
        16: AltBits := SetBitField(Value, AltBits);
        17: SpindleSpeed := SetDouble(Value, SpindleSpeed);
        18: RRServo := SetServo(Value, RRServo);
        19: RRDepth := SetDepth(Value, RRDepth);
        20: HP := TEDMOptions.GetInstance.RangeCheckHP(SetDouble(Value, HP));
        21: VibAmp := SetDouble(Value, VibAmp);
        22: VibFreq := SetDouble(Value, VibFreq);

      end;
    end;
  end;
end;


procedure TEDMEditorForm.OPPLiveBtnClick(Sender: TObject);
begin
  if OPPLiveBtn.Down then begin
    Grid.Options := Grid.Options - [goEditing];
    EditSection  := EDMConsumer.EDMSection;
    EditCellSpeedButton.Enabled := False;
    NewProgramButton.IsEnabled := False;
    DeleteProgramButton.IsEnabled := False;
    BlockUpDown.Enabled := False;
    IDUpDown.Enabled := False;
    PasteBlockButton.IsEnabled := False;
    PasteProgramButton.Enabled := False;
  end else begin
    EditSection := OPPSection;
    Grid.Options := Grid.Options + [goEditing];
    EditCellSpeedButton.Enabled := True;
    NewProgramButton.IsEnabled := True;
    DeleteProgramButton.IsEnabled := True;
    BlockUpDown.Enabled := True;
    IDUpDown.Enabled := True;
    PasteBlockButton.IsEnabled := True;
    PasteProgramButton.IsEnabled := True;
  end;
{  if EditSection = OPPSection then
    EditSection := EDMConsumer.EDMSection
  else
    EditSection := OPPSection; }
end;

{procedure TEDMEditorForm.CopyBlockBtnClick(Sender: TObject);
var S : TStringList;
begin
  if Assigned(EditProgram) and (Grid.Row > 0) then begin
    S := TStringList.Create;
    try
      EditProgram[Grid.Row - 1].WriteToStrings(S);
      ClipBrd.Clipboard.AsText := S.Text;
    finally
      S.Free;
    end;
  end;
end;}

{procedure TEDMEditorForm.PasteBlockBtnClick(Sender: TObject);
var S : TEDMSection;
begin
  if Assigned(EditProgram) and (Grid.Row > 0) then begin
    S := TEDMSection.Create;
    try
      S.Text := ClipBrd.ClipBoard.AsText;
      EditProgram[Grid.Row - 1].ReadFromStrings(S);
      UpdateScreen;
    finally
      S.Free;
    end;
  end;
end;}

{procedure TEDMEditorForm.CopyProgramBtnClick(Sender: TObject);
var S : TStringList;
begin
  if Assigned(EditProgram) then begin
    S := TStringList.Create;
    try
      EditProgram.WriteToStrings(S);
      ClipBrd.Clipboard.AsText := S.Text;
    finally
      S.Free;
    end;
  end;
end;}

{procedure TEDMEditorForm.PasteProgramBtnClick(Sender: TObject);
var S : TEDMSection;
    Tmp : Integer;
begin
  if Assigned(EditProgram) then begin
    S := TEDMSection.Create;
    Tmp := EditProgram.ID;
    try
      S.Text := ClipBrd.ClipBoard.AsText;
      EditProgram.ReadFromStrings(S);
      EditProgram.ID := Tmp;
      UpdateScreen;
    finally
      S.Free;
    end;
  end;
end;}

procedure TEDMEditorForm.EditCellSpeedButtonClick(Sender: TObject);
var Tmp : Double;
    C : Integer;
begin
  with EditProgram.Blocks[Grid.Row - 1] do begin
    case Grid.Col of
      13, 14 : TPulseBoardEditor.Execute(PulseBoards, Coupling, PBApply);

      7, 8 : TDutyCycleForm.Execute(OnTime, OffTime, SuppOnTime, SuppOffTime, False, DCApply);

      9, 10 : TDutyCycleForm.Execute(OnTime, OffTime, SuppOnTime, SuppOffTime, True, DCApply);

      2, 5, 6, 15 : TVIPForm.Execute(HighVoltage, BusVoltage, Peak, Negative, VIPApply);

      1 : begin
        Tmp := Depth / MetricToInch;
        TNumericEditorForm.Execute(Tmp, 100000, 0, 0, ApplyValue);
        Depth := Round(Tmp * MetricToInch);
      end;

      3 : TNumericEditorForm.Execute(GapVolts, 250, 0, 1, ApplyValue);
      4 : TNumericEditorForm.Execute(Servo, 65000, 0, 1, ApplyValue);

      11 : begin
        C := Capacitance;
//        TNumericEditorForm.Execute(Tmp, 22, 0, 0, ApplyValue);
        TCapacitanceEditor.Execute(C, CapacitanceApply);
        Capacitance := C;
      end;

      12 : TNumericEditorForm.Execute(ASV, 50, 0, 1, ApplyValue);

      16 : begin
        Tmp := AltBits;
        TNumericEditorForm.Execute(Tmp, 15, 0, 0, ApplyValue);
      end;

      17 : TNumericEditorForm.Execute(SpindleSpeed, 3000, 0, 1, ApplyValue);
      18 : TNumericEditorForm.Execute(RRServo, 3000, 0, 1, ApplyValue);
      19 : begin
        Tmp:= RRDepth;
        TNumericEditorForm.Execute(Tmp, 3000, 0, 1, ApplyValue);
        RRDepth:= Round(Tmp);
      end;
      20 : TNumericEditorForm.Execute(HP, 3000, 0, 1, ApplyValue);
      21 : TNumericEditorForm.Execute(VibAmp, 100, 0, 1, ApplyValue);
      22 : TNumericEditorForm.Execute(VibFreq, 180, 1, 1, ApplyValue);

    end;
  end;

  EditProgram.Verify(Grid.Row - 1);
  UpdateScreen;
end;

procedure TEDMEditorForm.ApplyValue(Sender : TObject; var aValue : Double);
begin
  if Assigned(EditProgram) and (EditProgram.BlockCount > Grid.Row) then begin
    with EditProgram.Blocks[Grid.Row - 1] do begin
      case Grid.Col of
        1 : Depth := Round(aValue * MetricToInch);
        3 : GapVolts := aValue;
        4 : Servo := aValue;
        11 : Capacitance := Round(aValue);
        12 : ASV := aValue;
        16 : AltBits := Round(aValue);
        17 : SpindleSpeed := aValue;
        18 : RRServo := aValue;
        19 : RRDepth := Round(aValue);
        20 : HP := aValue;
        21 : VibAmp := aValue;
        22 : VibFreq := aValue;
      end;
      EditProgram.Verify(Grid.Row - 1);
      HP:= EDMOptions.TEDMOptions.GetInstance.RangeCheckHP(HP);
      case Grid.Col of
        1 : aValue := Depth / MetricToInch;
        3 : aValue := GapVolts;
        4 : aValue := Servo;
        11 : aValue := Capacitance;
        12 : aValue := ASV;
        16 : aValue := AltBits;
        17 : aValue := SpindleSpeed;
        18 : aValue := RRServo;
        19 : aValue := RRDepth;
        20 : aValue := HP;
        21 : aValue := VibAmp;
        22 : aValue := VibFreq;
      end;
    end;
    UpdateScreen;
  end;
end;

procedure TEDMEditorForm.DCApply(Sender : TObject; var OnT, OffT, SOnT, SOffT : Integer);
begin
  if Assigned(EditProgram) and (EditProgram.BlockCount > Grid.Row) then begin
    with EditProgram.Blocks[Grid.Row - 1] do begin
      OnTime := OnT;
      OffTime := OffT;
      SuppOnTime := SOnT;
      SuppOffTime := SOffT;

      EditProgram.Verify(Grid.Row - 1);

      OnT := OnTime;
      OffT := OffTime;
      SOnT := SuppOnTime;
      SOffT := SuppOffTime;
    end;
    UpdateScreen;
  end;
end;

procedure TEDMEditorForm.PBApply(Sender : TObject; var aBoards, aCoupling : Integer);
begin
  if Assigned(EditProgram) and (EditProgram.BlockCount > Grid.Row) then begin
    with EditProgram.Blocks[Grid.Row - 1] do begin
      Coupling := aCoupling;
      PulseBoards := aBoards;
      EditProgram.Verify(Grid.Row - 1);
      aCoupling := Coupling;
      aBoards := PulseBoards;
    end;
    UpdateScreen;
  end;
end;

procedure TEDMEditorForm.VIPApply(Sender : TObject; var aHV, aBusV, aPeak : Integer; var aNegative : Boolean);
begin
  if Assigned(EditProgram) and (EditProgram.BlockCount > Grid.Row) then begin
    with EditProgram.Blocks[Grid.Row - 1] do begin
      HighVoltage := aHV;
      BusVoltage := aBusV;
      Peak := aPeak;
      Negative := aNegative;
      EditProgram.Verify(Grid.Row - 1);
      aHV := HighVoltage;
      aBusV := BusVoltage;
      aPeak := Peak;
      aNegative := Negative;
    end;
    UpdateScreen;
  end;
end;

procedure TEDMEditorForm.CapacitanceApply(Sender : TObject; var Cap : Integer);
begin
  if Assigned(EditProgram) and (EditProgram.BlockCount > Grid.Row) then begin
    with EditProgram.Blocks[Grid.Row - 1] do begin
      Capacitance := Cap;
      EditProgram.Verify(Grid.Row - 1);
      Cap := Capacitance;
    end;
    UpdateScreen;
  end;
end;

{procedure TEDMEditorForm.CopyLiveBtnClick(Sender: TObject);
var I, J : Integer;
begin
  if Assigned(EditProgram) then begin
    for I := 0 to EDMConsumer.EDMSection.ProgramCount - 1 do begin
      if I < OPPSection.ProgramCount then begin
        for J := 0 to EDMConsumer.EDMSection.Programs[I].BlockCount - 1 do begin
          if J < OPPSection.Programs[I].BlockCount then
          OPPSection.Programs[I].Blocks[J].Assign(EDMConsumer.EDMSection.Programs[I].Blocks[J]);
        end;
      end;
    end;
    UpdateScreen;
  end;
end;}

procedure TEDMEditorForm.UseLiveCellUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var I : Integer;
begin
  I := Grid.Row - 1;

  if Assigned(Grid.Diff1) and
     Assigned(Grid.Diff2) and
     (Grid.Diff1.BlockCount > I) and
     (Grid.Diff2.BlockCount > I) then
  case Grid.Col of
    1 : Grid.Diff1[I].Depth := Grid.Diff2[I].Depth;
    2 : Grid.Diff1[I].Negative := Grid.Diff2[I].Negative;
    3 : Grid.Diff1[I].GapVolts := Grid.Diff2[I].GapVolts;
    4 : Grid.Diff1[I].Servo := Grid.Diff2[I].Servo;
    5 : Grid.Diff1[I].HighVoltage := Grid.Diff2[I].HighVoltage;
    6 : Grid.Diff1[I].Peak := Grid.Diff2[I].Peak;
    7 : Grid.Diff1[I].OnTime := Grid.Diff2[I].OnTime;
    8 : Grid.Diff1[I].OffTime := Grid.Diff2[I].OffTime;
    9 : Grid.Diff1[I].SuppOnTime := Grid.Diff2[I].SuppOnTime;
    10 : Grid.Diff1[I].SuppOffTime := Grid.Diff2[I].SuppOffTime;
    11 : Grid.Diff1[I].Capacitance := Grid.Diff2[I].Capacitance;
    12 : Grid.Diff1[I].ASV := Grid.Diff2[I].ASV;
    13 : Grid.Diff1[I].PulseBoards := Grid.Diff2[I].PulseBoards;
    14 : Grid.Diff1[I].Coupling := Grid.Diff2[I].Coupling;
    15 : Grid.Diff1[I].BusVoltage := Grid.Diff2[I].BusVoltage;
    16 : Grid.Diff1[I].AltBits := Grid.Diff2[I].AltBits;
    17 : Grid.Diff1[I].SpindleSpeed := Grid.Diff2[I].SpindleSpeed;
    18 : Grid.Diff1[I].RRServo := Grid.Diff2[I].RRServo;
    19 : Grid.Diff1[I].RRDepth := Grid.Diff2[I].RRDepth;
    20 : Grid.Diff1[I].HP := Grid.Diff2[I].HP;
    21 : Grid.Diff1[I].VibAmp := Grid.Diff2[I].VibAmp;
    22 : Grid.Diff1[I].VibFreq := Grid.Diff2[I].VibFreq;

  end;
  EditProgram.Verify(Grid.Row - 1);
  UpdateScreen;

end;

procedure TEDMEditorForm.CopyBlockButtonUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var S : TStringList;
begin
  if Assigned(EditProgram) and (Grid.Row > 0) then begin
    S := TStringList.Create;
    try
      EditProgram[Grid.Row - 1].WriteToStrings(S);
      ClipBrd.Clipboard.AsText := S.Text;
    finally
      S.Free;
    end;
  end;
end;

procedure TEDMEditorForm.PasteBlockButtonUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var S : TEDMSection;
begin
  if Assigned(EditProgram) and (Grid.Row > 0) then begin
    S := TEDMSection.Create;
    try
      S.Text := ClipBrd.ClipBoard.AsText;
      EditProgram[Grid.Row - 1].ReadFromStrings(S);
      UpdateScreen;
    finally
      S.Free;
    end;
  end;

end;

procedure TEDMEditorForm.CopyProgButtonUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var S : TStringList;
begin
  if Assigned(EditProgram) then begin
    S := TStringList.Create;
    try
      EditProgram.WriteToStrings(S);
      ClipBrd.Clipboard.AsText := S.Text;
    finally
      S.Free;
    end;
  end;
end;

procedure TEDMEditorForm.PasteProgButtonUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var S : TEDMSection;
    Tmp : Integer;
begin
  if Assigned(EditProgram) then begin
    S := TEDMSection.Create;
    Tmp := EditProgram.ID;
    try
      S.Text := ClipBrd.ClipBoard.AsText;
      EditProgram.ReadFromStrings(S);
      EditProgram.ID := Tmp;
      UpdateScreen;
    finally
      S.Free;
    end;
  end;
end;

procedure TEDMEditorForm.DeleteProgButtonUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  EditSection.DeleteProgram(EditProgram);
  EditProgram := nil;
  UpdateScreen;
  UpdateTabs;
end;

procedure TEDMEditorForm.NewProgButtonUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var I : Integer;
    NP : TEDMProgram;
begin
  NP := EditSection.NewProgram;
  NP.NewBlock;
  NP.NewBlock;
  for I := 0 to EditSection.ProgramCount - 1 do begin
    if (EditSection[I] <> NP) and (EditSection[I].ID >= NP.ID) then
      NP.ID := EditSection[I].ID + 1;
  end;

  EditProgram := NP;
  UpdateScreen;
  UpdateTabs;
end;

procedure TEDMEditorForm.CopyLiveButtonUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var I, J : Integer;
begin
  if Assigned(EditProgram) then begin
    for I := 0 to EDMConsumer.EDMSection.ProgramCount - 1 do begin
      if I < OPPSection.ProgramCount then begin
        for J := 0 to EDMConsumer.EDMSection.Programs[I].BlockCount - 1 do begin
          if J < OPPSection.Programs[I].BlockCount then
          OPPSection.Programs[I].Blocks[J].Assign(EDMConsumer.EDMSection.Programs[I].Blocks[J]);
        end;
      end;
    end;
    UpdateScreen;
  end;
end;

initialization
  TEDMSectionEditor.AddPlugin(HSD6IISectionName, TEDMSectionEditor);
end.
