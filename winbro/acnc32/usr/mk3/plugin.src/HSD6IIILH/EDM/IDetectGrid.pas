unit IDetectGrid;

interface

uses AbstractGridPlugin, EDMPlc, Windows, Graphics, SysUtils, LPE482, IGridPlugin;

type
  TIDetectGrid = class(TAbstractGridPlugin)
  private
    Font : TFont;
    LastI : array [0..7] of Byte;
  public
    constructor Create(aHost : IFSDGridPluginHost); override;
    destructor Destroy; override;
    procedure Update; override;
    function CanEdit(X, Y : Integer) : Boolean; override;
    procedure Edit(X, Y : Integer; var Req : WideString); override;
    procedure DrawCell(aHDC : HDC; Rect : TRect; X, Y : Integer; State : TGPGridState); override;
    procedure ComboSelect(Index : Integer); override;
  end;

implementation


constructor TIDetectGrid.Create(aHost : IFSDGridPluginHost);
begin
  inherited Create(aHost);
  Font := TFont.Create;
  Font.Color := clBlack;

  Host.UpdateEv(guCount, 8, 6, nil);
  Host.UpdateEv(guFixed, 0, 0, nil);
  Host.UpdateEv(guVisible, 1, 0, nil);
  Host.UpdateEv(guSelfDraw, 1, 0, nil);
  Host.UpdateEv(guEditing, 0, 0, nil);
end;

destructor TIDetectGrid.Destroy;
begin
  Font.Free;
  inherited Destroy;
end;


procedure TIDetectGrid.Update;
var I, X, Y : Integer;
begin
  if Assigned(EDMHead) then begin
    for I := 0 to 47 do begin
      X := $80 shr (I mod 8);
      Y := I div 8;
      if (LastI[Y] and X) <> (EDMHead.LPE482.Input.Sense.B[Y] and X) then begin
        Host.UpdateEv(guCell,
              I mod 8,
              Y, //I div 8,
              PChar(''));
      end;
    end;
    for I := 0 to 5 do begin
      LastI[I] := EDMHead.LPE482.Input.Sense.B[I];
    end
  end;
end;

function TIDetectGrid.CanEdit(X, Y : Integer) : Boolean;
begin
  Result := True;
end;

procedure TIDetectGrid.Edit(X, Y : Integer; var Req : WideString);
begin
{  if Assigned(EDMHead) then begin
    Include(EDMHead.LPE482.Output.Control, lpe482ctrlResetIDetect);
  end; }
end;

procedure TIDetectGrid.DrawCell(aHDC : HDC; Rect : TRect; X, Y : Integer; State : TGPGridState);
var Text : string;
    Options : Integer;
begin
  if Assigned(EDMHead) then begin
    if (EDMHead.LPE482.Input.Sense.B[Y] and ($80 shr X)) <> 0 then begin
      Windows.SetBKColor(aHDC, clBlue);
      Windows.SetTextColor(aHDC, clYellow);
    end else begin
      Windows.SetBKColor(aHDC, clYellow);
      Windows.SetTextColor(aHDC, clBlue);
    end;
    Text := IntToStr(Y*8 + X + 1); // IntToStr(ElectrodeIndex[Tmp].Electrode)
    Options := ETO_CLIPPED + ETO_OPAQUE;
    Windows.ExtTextOut(aHDC, Rect.Left + 3, Rect.Top + 3, Options, @Rect, PChar(Text), Length(Text), nil);
  end;
end;

procedure TIDetectGrid.ComboSelect(Index : Integer);
begin
end;

initialization
  TAbstractGridPlugin.AddPlugin('IDetectGrid', TIDetectGrid);
end.
