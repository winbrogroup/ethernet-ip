object EDMEditorForm: TEDMEditorForm
  Left = 157
  Top = 200
  Width = 1000
  Height = 454
  Caption = 'HSD6 II EDM Parameters'
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'MS Sans Serif'
  Font.Style = [fsBold]
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 992
    Height = 54
    Align = alTop
    BevelOuter = bvLowered
    TabOrder = 0
    object IDUpDown: TUpDown
      Left = 8
      Top = 2
      Width = 50
      Height = 50
      Hint = 'Program Number Change'
      Max = 32000
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = IDUpDownClick
    end
    object BlockUpDown: TUpDown
      Left = 768
      Top = 2
      Width = 50
      Height = 50
      Hint = 'Block count change'
      Max = 10000
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = BlockUpDownClick
    end
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 393
    Width = 992
    Height = 27
    Panels = <
      item
        Width = 150
      end
      item
        Width = 150
      end
      item
        Width = 50
      end>
    ParentFont = True
    UseSystemFont = False
  end
  object Panel2: TPanel
    Left = 0
    Top = 359
    Width = 992
    Height = 34
    Align = alBottom
    BevelOuter = bvLowered
    TabOrder = 2
    object Label2: TLabel
      Left = 224
      Top = 8
      Width = 52
      Height = 13
      Caption = 'Comment'
    end
    object OPPLiveBtn: TSpeedButton
      Left = 712
      Top = 2
      Width = 90
      Height = 30
      AllowAllUp = True
      GroupIndex = 1
      Caption = 'OPP / Live'
      OnClick = OPPLiveBtnClick
    end
    object EditCellSpeedButton: TSpeedButton
      Left = 608
      Top = 2
      Width = 90
      Height = 30
      Caption = 'Edit Cell'
      OnClick = EditCellSpeedButtonClick
    end
    object BitBtn2: TBitBtn
      Left = 104
      Top = 4
      Width = 89
      Height = 25
      TabOrder = 0
      Kind = bkCancel
    end
    object BitBtn1: TBitBtn
      Left = 8
      Top = 4
      Width = 89
      Height = 25
      TabOrder = 1
      Kind = bkOK
    end
    object CommentEdit: TEdit
      Left = 304
      Top = 4
      Width = 281
      Height = 21
      TabOrder = 2
      Text = 'CommentEdit'
      OnChange = CommentEditChange
    end
  end
  object EDMTab: TTabControl
    Left = 0
    Top = 54
    Width = 992
    Height = 305
    Align = alClient
    HotTrack = True
    TabOrder = 3
    Tabs.Strings = (
      '1'
      '2'
      '3')
    TabIndex = 0
    OnChange = ProgramSelectChange
  end
end
