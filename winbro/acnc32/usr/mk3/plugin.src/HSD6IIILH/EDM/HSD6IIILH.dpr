library HSD6IIILH;

uses
  EDMEditor in 'EDMEditor.pas' {EDMEditorForm},
  EDMGrid in 'EDMGrid.pas',
  EDMPlc in 'EDMPlc.pas',
  PulseBoardForm in 'PulseBoardForm.pas' {PulseBoardEditor},
  DutyCycleEditor in 'DutyCycleEditor.pas' {DutyCycleForm},
  VIPEditor in 'VIPEditor.pas' {VIPForm},
  GeneralEditor in 'GeneralEditor.pas' {NumericEditorForm},
  CapacitanceEditForm in 'CapacitanceEditForm.pas' {CapacitanceEditor},
  IncControl in 'IncControl.pas',
  SimpleMeter in 'SimpleMeter.pas',
  IDetectGrid in 'IDetectGrid.pas',
  EDMOptions in 'EDMOptions.pas',
  ASVCalibration in '..\..\lib\ASVCalibration.pas',
  DepthMonitor in 'DepthMonitor.pas',
  Methods in 'Methods.pas',
  EDMReaderWriter in '..\..\lib\EDMReaderWriter.pas';

{$R *.RES}

function ACNC32PluginRevision : Cardinal; stdcall;
begin
  Result := $01000002;
end;

exports
  ACNC32PluginRevision;

begin

end.
