unit PSUCommsRutland;

interface

uses Sysutils, Classes, Windows, Cnctypes, ECMState, PSUCommon, AbstractPSUComms;

type

  TRStatus2Value = (
    // PSU_pod_status2 , 6809 ref FS 2 */
    rstatus2_STACK_TEMP,
    rstatus2_MAIN_TX_TEMP,
    rstatus2_AUX_TX_TEMP,
    rstatus2_POS_DI1_DT,
    rstatus2_NEG_DI1_DT,
    rstatus2_MOISTURE_DETECTED, //       LHR LEFT HAND RAM */,
    rstatus2_OUTLET_FLOW_FAULT,
    rstatus2_SENSE_LEADS_FAULT,

    rstatus2_CONTACT_SENSED,
    rstatus2_LOSS_OF_TIMING,
    rstatus2_DVDT_IRQ,
    rstatus2_COMMS_FAULT,
    rstatus2_POS_DI2_DT,
    rstatus2_NEG_DI2_DT,
    rstatus2_DV2DT_FAULT,
    rstatus2_DV1DT_FAULT
  );
  TRStatus2Values = set of TRStatus2Value;

  R_BUF_TYPE_S =  packed record
  case Byte of
  0: (
    status1: TStatus1Values;
    status2: TRStatus2Values;

    lhr_current: Byte;
    rhr_current: Byte;
    lhr_voltage: Byte;
    rhr_voltage: Byte;
    anode_current: Byte;
    anode_voltage: Byte;

    main_current: Byte;
    main_voltage: Byte;

    front_box_current: Byte;
    back_box_current: Byte;
  );

  1: (
    rawData: array[0..15] of Byte;
  )
  end;

  R_BUF_TYPE_P = packed record
  case Byte of
  0: (
    format: Byte;
    command: TStatus1Values;
    on_voltage: Byte;
    off_voltage: Byte;
    on_current: Byte;
    on_time: Word;      // in units of phase counts */
    off_time: Word;     // in units of phase counts */
    ramp_incs: Byte;    // no of dac incs the o/p to be increased at .5 sec interval */
    left_dvdt_gain: Byte;
    right_dvdt_gain: Byte;
    anode_voltage: Byte;

    plus_di1_dt: Byte;
    plus_di2_dt: Byte;
    minus_di1_dt: Byte;
    minus_di2_dt: Byte;
  );
  1: (
    rawdata: array [0..17] of Byte;
  );
  end;


type
  TPSUComms = class(TAbstractPSUComms)
  private
    CommHandle: THandle;
    CommTimeouts: TCommTimeouts;
    FComms: string;
    ErrorCount: Integer;
    FOutMessage: string;
    FStateChange: Boolean;
    FParameterChange: Boolean;
    FNewData: Boolean;
    SendIndex: Integer;
    suspendCommand: TStatus1Values;
    procedure SendMessage(Text: string);
    function ReceiveMessage: string;
    procedure GetStatus;
    procedure SendStatus(Format: Byte);
    procedure SendCommand(Cmd: string);
    procedure ReestablishComms;
    procedure SendChar(Char: Byte);
    function GetChar: Byte;
    procedure ReceiveChar(Char: Byte);
    procedure DecodeInputStatus(Buffer: string);
    function EncodeOutputStatus: string;
  public
    InputData: R_BUF_TYPE_S;
    OutputData: R_BUF_TYPE_P;
    CommsError: Boolean;
    Dead: Boolean;
    constructor Create(AComms: string);
    destructor Destroy; override;
    procedure Execute; override;
    property OutMessage: string read FOutMessage write FOutMessage;
    procedure ClearHistory; override;
    procedure SetStateChange;
    procedure SetParameterChange;
    property NewData: Boolean read FNewData write FNewData;
  end;

implementation

uses
  Proto;

var
  DCB: TDCB =
    (DCBLength: SizeOf(TDCB);
     BaudRate: CBR_2400;
     Flags: DTR_CONTROL_DISABLE;
     wReserved: 0;
     XonLim: 0;
     XoffLim: 0;
     ByteSize: 8;
     Parity: NOPARITY;
     StopBits: TWOSTOPBITS;
     XonChar: #0;
     XoffChar: #0;
     ErrorChar: #0;
     EoFChar: #0;
     EvtChar: #0;
     wReserved1: 0);

{ TPSUComms }

constructor TPSUComms.Create(AComms: string);
begin
  inherited Create(True);

  // Prefeed initial state
  ECMRequest.DcOnConstantVoltage:= False;
  ECMRequest.DcOnPulsed:= False;
  ECMRequest.DcOnConstantCurrent:= False;
  Self.SetStateChange;

  FDataList:= TList.Create;
  FComms:= AComms;
  try
    CommHandle:= CreateFile(PChar(Format('%s:', [FComms])),
       GENERIC_READ or GENERIC_WRITE, 0, nil, OPEN_EXISTING, 0, 0);
    if CommHandle = INVALID_HANDLE_VALUE then
      RaiseLastOSError;

    CommTimeouts.ReadIntervalTimeout:= 1000;
    CommTimeouts.ReadTotalTimeoutMultiplier:= 100;
    CommTimeouts.ReadTotalTimeoutConstant:= 1000;
     CommTimeouts.WriteTotalTimeoutMultiplier:= 0;
    CommTimeouts.WriteTotalTimeoutConstant:= 0;

    if not SetCommState(CommHandle, DCB) then
      RaiseLastOSError;

    if not SetCommTimeouts(CommHandle, CommTimeouts) then
      RaiseLastOSError;
  except
    Dead:= True;
  end;

end;

destructor TPSUComms.Destroy;
begin
  if CommHandle <> INVALID_HANDLE_VALUE then
    CloseHandle(CommHandle);

  inherited;
end;

procedure TPSUComms.Execute;
begin
  if Dead then
    Exit;

  while not Terminated do begin
    try
      GetStatus;
      NewData:= True;
      CommsError:= False;

      if FStateChange then begin
        FStateChange:= False;
        SendStatus(COMMAND_PARA);
      end;

      if FParameterChange then begin
        FParameterChange:= False;
        SendStatus(PARA_ONLY);
      end;
    except
      ReestablishComms;
      Sleep(500);
    end;
  end;
end;

procedure TPSUComms.SendCommand(Cmd: string);
var B: Byte;
const MAX_RETRY_COUNT = 6;
begin
  ErrorCount:= 0;

  SendChar(ENQ);
  B:= GetChar;
  if B <> ACK then begin
    raise Exception.Create('Unexpected response');
  end;

  repeat
    Inc(ErrorCount);
    if ErrorCount > MAX_RETRY_COUNT then
      raise Exception.Create('Too many retries on comms');
    SendMessage(Cmd);
  until GetChar = ACK;
  SendChar(EOT);
end;


procedure TPSUComms.SendChar(Char: Byte);
var Buffer: array [0..1] of Byte;
    Count: Cardinal;
begin
  Buffer[0]:= Char;
  WriteFile( CommHandle, Buffer, 1, Count, nil);
  FlushFileBuffers( CommHandle);

  if Count <> 1 then
    raise Exception.Create('Failed to write character to comms');

  DataList.Add(Pointer(Ord(Char) or $100));
  if (SendIndex mod 1) = 0 then begin
    Sleep(10);
    SendIndex:= 0;
  end;
  Inc(SendIndex);
  if Assigned(OnDataChange) then
    OnDataChange(Self);
end;

function TPSUComms.GetChar: Byte;
var Buffer: array [0..1] of Byte;
    Count: Cardinal;
begin
  ReadFile( CommHandle, Buffer, 1, Count, nil);
  if Count = 0 then
    raise Exception.Create('Rx timeout waiting for Ack');
  Result:= Buffer[0];
  DataList.Add(Pointer(Result));
  if Assigned(OnDataChange) then
    OnDataChange(Self);
end;

procedure TPSUComms.ReestablishComms;
begin
  try
    CommsError:= True;
    SendChar(STX);
    SendChar(ETX);
    while GetChar <> 0 do;
  except
  end;
end;

procedure TPSUComms.GetStatus;
var Buffer: string;
begin
  SendCommand('CSTA');

  Buffer:= ReceiveMessage;
  DecodeInputStatus(Buffer);
end;

procedure TPSUComms.SendStatus(Format: Byte);
var Period: Double;
begin
  SendCommand('UTAB');
  ReceiveMessage;
  OutputData.format:= format;

  if Format <> PARA_ONLY then begin
    if ECMRequest.DcSuspendStart then begin
      if (status1_DC_ON_VOLTAGE in OutputData.command) or (status1_DC_ON_CURRENT in OutputData.command) then begin
        suspendCommand:= OutputData.command;
        OutputData.command:= [status1_DC_OFF];
        OutputData.format:= COMMAND_ONLY;
      end else begin
        suspendCommand:= [];
        Exit;
      end;
    end else
    if ECMRequest.DcSuspendEnd then begin
      ECMRequest.DcSuspendEnd:= False;
      if (status1_DC_ON_VOLTAGE in suspendCommand) or (status1_DC_ON_CURRENT in suspendCommand) then begin
        OutputData.command:= suspendCommand;
        OutputData.format:= COMMAND_ONLY;
        suspendCommand:= [];
      end else
        Exit;
    end else
    if ECMRequest.DcOnConstantVoltage then begin
      OutputData.command:= [status1_DC_ON_VOLTAGE];
      suspendCommand:= [];
    end else
    if ECMRequest.DcOnPulsed then begin
      OutputData.command:= [status1_DC_ON_VOLTAGE, status1_PULSING_REQUIRED];
      suspendCommand:= [];
    end else
    if ECMRequest.DcOnConstantCurrent then begin
      OutputData.command:= [status1_DC_ON_CURRENT];
      suspendCommand:= [];
    end
    else begin
      OutputData.command:= [status1_DC_OFF];
      suspendCommand:= [];
      OutputData.format:= COMMAND_ONLY;
    end;
    if ECMRequest.Isset( ECMRequest.ProtectiveAnodeVoltage) then
      OutputData.command := OutputData.command + [status1_ANODE_V_REQUIRED];
  end;

  // CPU voltage factor = 255/Transducer volts, programmed = volts
  // DCO programmed in percent
  OutputData.on_voltage:= Round(ECMRequest.DCO * ECMRequest.OnTimeVoltage * 255.0 / (MainVoltageTransducer * 100.0));
  OutputData.off_voltage:= Round(ECMRequest.DCO * ECMRequest.OffTimeVoltage * 255.0 / (MainVoltageTransducer * 100.0));

  // To produce 0.1 second on
  // Calculte max / min
  // on_off_fac = (float) 1000 / (sys_frequency*6); >>> where sys_frequency = mains
  // on_time/on_off_fac

  if ECMRequest.Frequency > 0.1 then begin
    Period:= 1 / ECMRequest.Frequency;
  end else begin
    Period:= ECMRequest.Period
  end;

  ECMRequest.OnTime:= (ECMRequest.Duty / 100) * Period;
  ECMRequest.Offtime:= ((100 - ECMRequest.Duty) / 100) * Period;
  OutputData.on_time:= Round((ECMRequest.OnTime * MainsFrequency * 6.0));
  OutputData.off_time:= Round((ECMRequest.Offtime * MainsFrequency * 6.0)) ;

  //const_current_fac = msd_int[CONST_CURRENT_FAC] / msd_flt[CUR_FAC1] ;
  //inter_buf_p.on_current=(PSU_pid.on_current * const_current_fac)*PSU_pod.dco_factor;
  // msd_int[CONST_CURRENT_FAC] = 768
  // msd_flt[CUR_FAC1] = 20000
  OutputData.on_current:= Round(5000.0 * 768.0 / 20000.0);

  if ECMRequest.OnTimeVoltageRampTime > 0.3 then
    OutputData.ramp_incs:= Round(MaximumRampTime / ECMRequest.OnTimeVoltageRampTime)
  else
    OutputData.ramp_incs:= 0;

  OutputData.left_dvdt_gain:= 255 - Round(ECMRequest.SparkSensitivityLeft * 255.0 / 100.0);
  OutputData.right_dvdt_gain:= 255 - Round(ECMRequest.SparkSensitivityRight * 255.0 / 100.0);;
  OutputData.anode_voltage:= Round(ECMRequest.ProtectiveAnodeVoltage * 255.0 / AnodeVoltageTransducer);
  OutputData.plus_di1_dt:= 255 - Round(ECMRequest.dIdTPositiveChange * 255.0 / 100.0);
  OutputData.plus_di2_dt:= 255 - Round(ECMRequest.dIdTPositiveChange * 255.0 / 100.0);
  OutputData.minus_di1_dt:= 255 - Round(ECMRequest.dIdTNegativeChange * 255.0 / 100.0);
  OutputData.minus_di2_dt:= 255 - Round(ECMRequest.dIdTNegativeChange * 255.0 / 100.0);

  SendCommand('ETAB' + Self.EncodeOutputStatus);
end;



procedure TPSUComms.SendMessage(Text: string);
var I: Integer;
    CSum: Byte;
begin
  SendChar(STX);
  CSum:= 0;
  for I:= 1 to Length(Text) do begin
    SendChar(Ord(Text[I]));
    CSum:= CSum + Ord(Text[I]);
  end;
  CSum:= CSum + ETX;
  SendChar(ETX);
  SendChar(CSum);
end;

function TPSUComms.ReceiveMessage: string;
var Tmp: Byte;
    CSum: Byte;
begin
  ReceiveChar(ENQ);
  SendChar(ACK);
  ReceiveChar(STX);

  Tmp:= GetChar;
  CSum:= 0;
  while Tmp <> ETX do begin
    Result:= Result + Char(Tmp);
    CSum:= CSum + Tmp;

    if Length(Result) > 100 then begin
      raise Exception.Create('Buffer overrun');
    end;
    Tmp:= GetChar;
  end;
  CSum:= CSum + ETX;

  if GetChar <> CSum then begin
    raise Exception.Create('Invalid CSum');
  end;

  SendChar(ACK);
  ReceiveChar(EOT);

  SendChar(ACK);
end;



procedure TPSUComms.ReceiveChar(Char: Byte);
var Tmp: Byte;
begin
  Tmp:= GetChar;
  if Tmp <> Char then
    raise Exception.Create('Unexpected character: ' + IntToHex(Tmp, 2));
end;

procedure TPSUComms.ClearHistory;
begin
  DataList.Clear;
  if Assigned(OnDataChange) then
    OnDataChange(Self);
end;

procedure TPSUComms.DecodeInputStatus(Buffer: string);
var Tmp: string;
    Count: Integer;
begin
  Count:= 0;
  Delete(Buffer, 1, 4);
  while Buffer <> '' do begin
    Tmp:= ReadDelimited(Buffer, ',');
    InputData.rawData[Count]:= StrToInt('$' + Tmp);
    Inc(Count);
  end;
end;

function TPSUComms.EncodeOutputStatus: string;
var I: Integer;
begin
  Result:= '';
  if (OutputData.format = COMMAND_PARA) or (OutputData.format = PARA_ONLY) then begin
    for I:= 0 to Sizeof(OutputData) do begin
      Result:= Result + IntToHex(OutputData.rawdata[I], 2) + ',';
    end;
  end else begin
    for I:= 0 to 2 do begin
      Result:= Result + IntToHex(OutputData.rawdata[I], 2) + ',';
    end;
  end;
end;

procedure TPSUComms.SetStateChange;
begin
  FStateChange:= True;
end;


procedure TPSUComms.SetParameterChange;
begin
  FParameterChange:= True;
end;

end.
