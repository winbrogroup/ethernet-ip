const
(*  PSU_pod_status2 , 6809 ref FS 2 *)

  POS_WP1_DIDT_FAULT         = $0001;
  POS_WP2_DIDT_FAULT         = $0002;
  POS_WP3_DIDT_FAULT         = $0004;
  NEG_WP1_DIDT_FAULT         = $0008;
  NEG_WP2_DIDT_FAULT         = $0010;
  NEG_WP3_DIDT_FAULT         = $0020;
  NOT_USED_1                 = $0040;
  NOT_USED_2                 = $0080;

(*  PSU_pod_status2 , 6809 ref FS 3 *)

  NOT_USED_3                 = $0100;
  NOT_USED_4                 = $0200;
  NOT_USED_5                 = $0400;
  ANODE_STACK_FAULT          = $0800;
  NOT_USED_6                 = $1000;
  MOISTURE_DETECTED          = $2000;
  OUTLET_FLOW_FAULT          = $4000;
  NOT_USED_7                 = $8000;

(*...................................................................*)

(*  PSU_pod_status3 , 6809 ref FS 4 *)

  CONTACT_SENSED             = $0001;
  SENSE_LEADS_FAULT          = $0002;
  STACK_FAULT                = $0004;
  MAIN_TX_TEMP               = $0008;
  AUX_TX_TEMP                = $0010;
  BUSBAR_TEMP                = $0020;
  DV2DT_FAULT                = $0040;
  DV1DT_FAULT                = $0080;

(*  PSU_pod_status3 , 6809 ref FS 5 *)

  DC_INVALID_REQUEST         = $0100;
  LOSS_OF_TIMING             = $0200;
  DVDT_IRQ                   = $0400;
  NOT_USED_8                 = $0800;
  COMMS_FAULT                = $1000;
  FAST_MAIN_OVER_CURRENT     = $2000;
  FAST_ANODE_OVER_CURRENT    = $4000;
  NOT_USED_11                = $8000;

(*...................................................................*)

(*  PSU_pod_status4 , 6809 ref FS 6 *)

  STACK_TEMP_WARNING         = $0001;
  MAIN_TX_TEMP_WARNING       = $0002;
  AUX_TX_TEMP_WARNING        = $0004;
  ANODE_STACK_TEMP_WARNING   = $0008;
  NOT_USED_12                = $0010;
  NOT_USED_13                = $0020;
  NOT_USED_14                = $0040;
  NOT_USED_15                = $0080;


(*  EQUATES TO GENERAL_ERRORS INTEGER  *)

  VOLTAGE_LIMIT          = $0001;
  CURRENT_LIMIT          = $0002;
  OVER_CURRENT           = $0004;
  COMMS_LINK_BROKEN      = $0008;
  ANODE_OVER_CURRENT     = $0010;
  PROGRAMMING_ERROR      = $0020;
  DC_OFF_DURING_CUT      = $0040;
  UNDEF_5                = $0080;



  SOH = 1;
  STX = 2;
  ETX = 3;
  EOT = 4;
  ENQ = 5;
  ACK = 6;
  DLE = $10;
  NAK = $15;



