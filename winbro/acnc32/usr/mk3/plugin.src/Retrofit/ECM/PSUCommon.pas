unit PSUCommon;

interface

const
  SOH = 1;
  STX = 2;
  ETX = 3;
  EOT = 4;
  ENQ = 5;
  ACK = 6;
  DLE = $10;
  NAK = $15;

  COMMAND_ONLY  = 1;
  PARA_ONLY     = 2;
  COMMAND_PARA  = 4;

  DC_OFF            =  $0100;
  DC_ON_VOLTAGE     =  $0200;
  DC_ON_CURRENT     =  $0400;
  UNDEFINED_1       =  $0800;

  UNDEFINED_2       =  $1000;  // contact sense can only be enabled when dc is off */
  ANODE_V_REQUIRED  =  $2000;  // set this flag if anode supply is also required when dc is switched on */
  NOT_ANODE_V_REQD  =  $DFFF;
  PULSING_REQUIRED  =  $4000;  // set this flag if pulsing is also required when dc is switched on */
  UNDEFINED_3       =  $8000;

type
  TStatus1Value = (
    status1_UNDEFINED_4,
    status1_UNDEFINED_5,
    status1_UNDEFINED_6,
    status1_UNDEFINED_7,

    status1_UNDEFINED_8,
    status1_UNDEFINED_9,
    status1_UNDEFINED_10,
    status1_UNDEFINED_11,

    status1_DC_OFF,
    status1_DC_ON_VOLTAGE,
    status1_DC_ON_CURRENT,
    status1_UNDEFINED_1,

    status1_UNDEFINED_2,         // contact sense can only be enabled when dc is off */
    status1_ANODE_V_REQUIRED,    // set this flag if anode supply is also required when dc is switched on */
    //status1_NOT_ANODE_V_REQD,
    status1_PULSING_REQUIRED,    // set this flag if pulsing is also required when dc is switched on */
    status1_UNDEFINED_3
  );
  TStatus1Values = set of TStatus1Value;


var
  MainsFrequency: Double; // 60.0; // ??????
  RamCurrentTransducer: Double; // = 10000.0;
  MainCurrentTransducer: Double; // = 20000.0;
  MainVoltageTransducer: Double; // = 40.0;
  AnodeCurrentTransducer: Double; // = 1000.0;
  AnodeVoltageTransducer: Double; // 

  FlowTransducer: Double; //= 500;
  PressureTransducer: Double; // = 500;
  TemperatureTransducer: Double; // = 180;
  TemperatureIntersect: Double;
  MaximumRampTime: Double;


implementation

end.
