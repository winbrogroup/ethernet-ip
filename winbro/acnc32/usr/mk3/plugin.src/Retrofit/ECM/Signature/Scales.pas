unit Scales;

interface

uses Classes, Windows, Graphics, ExtCtrls, Controls;

type
  TScale = class(TCustomPanel)
  private
    FMaximumValue: Double;
    FMinimumValue: Double;
    BM: TBitMap;
    FTicks: Integer;
    TickPen: TPen;
    BorderPen: TPen;
    FLegend: string;

    procedure SetMaximumValue(const Value: Double);
    procedure SetMinimumValue(const Value: Double);
  protected
    procedure Resize; override;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;

    property MinimumValue: Double read FMinimumValue write SetMinimumValue;
    property MaximumValue: Double read FMaximumValue write SetMaximumValue;
    property Ticks: Integer read FTicks;
    function GetTick(Index: Integer): Integer; virtual; abstract;

    function GetScreenOrdinate(Value: Double): Integer; virtual; abstract;
    property Legend: string read FLegend write FLegend;
  end;

  TXScale = class(TScale)
  protected
    procedure Paint; override;
    procedure UpdateGraphics;
  public
    function GetScreenOrdinate(Value: Double): Integer; override;
    function GetTick(Index: Integer): Integer; override;
  end;

  TYScale = class(TScale)
  protected
    procedure Paint; override;
    procedure UpdateGraphics;
  public
    function GetScreenOrdinate(Value: Double): Integer; override;
    function GetTick(Index: Integer): Integer; override;
  end;


implementation

{ TScale }

procedure TScale.SetMaximumValue(const Value: Double);
begin
  FMaximumValue := Value;
end;

procedure TScale.SetMinimumValue(const Value: Double);
begin
  FMinimumValue := Value;
end;

constructor TScale.Create(AOwner: TComponent);
begin
  inherited;
  BM:= TBitMap.Create;
  BM.Canvas.Brush.Color:= clBlack;
  BM.Canvas.Font.Size:= 12;
  BM.Canvas.Font.Style:= [fsBold];
  Self.BevelInner:= bvNone;
  Self.BevelOuter:= bvNone;
  ControlStyle:= ControlStyle + [csOpaque];
  FTicks:= 9;

  TickPen:= TPen.Create;
  TickPen.Color:= clGray;
  TickPen.Width:= 4;
  TickPen.Style:= psSolid;
  TickPen.Mode:= pmCopy;

  BorderPen:= TPen.Create;
  BorderPen.Color:= clYellow;
  BorderPen.Mode:= pmCopy;
  BorderPen.Style:= psSolid;
end;

procedure TScale.Resize;
begin
  inherited;
  BM.Width := Self.ClientWidth;
  BM.Height := Self.ClientHeight;
end;

destructor TScale.Destroy;
begin
  inherited;
end;

{ TXScale }

function TXScale.GetScreenOrdinate(Value: Double): Integer;
var Tmp: Double;
begin
  Tmp:= (Value - MinimumValue);
  Tmp:= Tmp / MaximumValue;
  Tmp:= Tmp * Width;
  Result := Round(Tmp);
end;

function TXScale.GetTick(Index: Integer): Integer;
begin
  Result:= Round((FMaximumValue / (FTicks + 1)) * (Index + 1));
end;

procedure TXScale.Paint;
begin
  UpdateGraphics;
  Canvas.Draw(0, 0, BM);
end;

procedure TXScale.UpdateGraphics;
var i: Integer;
    Size: TSize;
begin
  BM.Canvas.FillRect(ClientRect);
  BM.Canvas.Pen:= BorderPen;
  BM.Canvas.Rectangle(1, 1, ClientWidth - 2, ClientHeight - 2);

  BM.Canvas.Pen:= TickPen;
  for I:= 0 to Ticks - 1 do begin
    BM.Canvas.MoveTo(GetScreenOrdinate(GetTick(I)), 0);
    BM.Canvas.LineTo(GetScreenOrdinate(GetTick(I)), 10);
  end;

  BM.Canvas.Font.Color:= clWhite;
  Size:= BM.Canvas.TextExtent(Legend);
  BM.Canvas.TextOut((Width - Size.cx) div 2, (ClientHeight - Size.cy) div 2, Legend);
end;

{ TYScale }

function TYScale.GetScreenOrdinate(Value: Double): Integer;
var Tmp: Double;
begin
  Tmp:= (Value - MinimumValue);
  Tmp:= Tmp / MaximumValue;
  Tmp:= Tmp * Height;
  Result := Round(Height - Tmp);
end;

function TYScale.GetTick(Index: Integer): Integer;
begin
  Result:= Round((FMaximumValue / (FTicks + 1)) * (Index + 1));
end;

procedure TYScale.Paint;
begin
  UpdateGraphics;
  Canvas.Draw(0, 0, BM);
end;

procedure TYScale.UpdateGraphics;
var I: Integer;
  LogRec: TLogFont;
  OldFontHandle,
  NewFontHandle: hFont;
  Size: TSize;
begin
  BM.Canvas.FillRect(ClientRect);

  BM.Canvas.Pen:= BorderPen;
  BM.Canvas.Rectangle(1, 1, ClientWidth - 2, ClientHeight - 2);

  BM.Canvas.Pen:= TickPen;
  for I:= 0 to Ticks - 1 do begin
    BM.Canvas.MoveTo(Width, GetScreenOrdinate(GetTick(I)));
    BM.Canvas.LineTo(Width - 10, GetScreenOrdinate(GetTick(I)));
  end;

  BM.Canvas.Font.Color:= clWhite;

  GetObject(BM.Canvas.Font.Handle, SizeOf(LogRec), Addr(LogRec));
  LogRec.lfEscapement := 10 * 90;
  NewFontHandle := CreateFontIndirect(LogRec);
  OldFontHandle := SelectObject(BM.Canvas.Handle, NewFontHandle);

  Size:= BM.Canvas.TextExtent(Legend);
  //BM.Canvas.TextOut(20, 20, Legend);
  BM.Canvas.TextOut((Width - Size.cy) div 2, (ClientHeight + Size.cx) div 2, Legend);

  NewFontHandle := SelectObject(BM.Canvas.Handle, OldFontHandle);
  DeleteObject(NewFontHandle);
end;

end.
