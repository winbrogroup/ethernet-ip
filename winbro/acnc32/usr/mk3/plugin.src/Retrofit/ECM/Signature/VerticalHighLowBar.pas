unit VerticalHighLowBar;

interface

uses Classes, Windows, Graphics, ExtCtrls;

type
  TVerticalHighLowBar = class(TCustomPanel)
  private
    FLabelHeight: Integer; // Region reserved for legend at bottom of graph
    FLegend: string;
    FMinimumValue: Double;
    FMaximumValue: Double;
    FUpperLimit: Double;
    FLowerLimit: Double;
    FCurrentValue: Double;
    FSelected: Boolean;

    BM: TBitMap;
    procedure SetCurrentValue(const Value: Double);
    procedure SetLowerLimit(const Value: Double);
    procedure SetMaximumValue(const Value: Double);
    procedure SetMinimumValue(const Value: Double);
    procedure SetUpperLimit(const Value: Double);

    function TranslateYFromValue(Value: Double): Integer;

    procedure UpdateGraphics;
  protected
    procedure Resize; override;
    procedure Paint; override;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;

    property MinimumValue: Double read FMinimumValue write SetMinimumValue;
    property MaximumValue: Double read FMaximumValue write SetMaximumValue;
    property UpperLimit: Double read FUpperLimit write SetUpperLimit;
    property LowerLimit: Double read FLowerLimit write SetLowerLimit;
    property CurrentValue: Double read FCurrentValue write SetCurrentValue;

    property LabelHeight: Integer read FLabelHeight write FLabelHeight;
    property Legend: string read FLegend write FLegend;
    property Selected: Boolean read FSelected write FSelected;
  end;

implementation

uses
  Controls;

{ TVerticalHighLowBar }

procedure TVerticalHighLowBar.SetUpperLimit(const Value: Double);
begin
  FUpperLimit := Value;
  Invalidate;
end;

procedure TVerticalHighLowBar.SetMaximumValue(const Value: Double);
begin
  FMaximumValue := Value;
  Invalidate;
end;

procedure TVerticalHighLowBar.SetMinimumValue(const Value: Double);
begin
  FMinimumValue := Value;
  Invalidate;
end;

procedure TVerticalHighLowBar.SetLowerLimit(const Value: Double);
begin
  FLowerLimit := Value;
  Invalidate;
end;

procedure TVerticalHighLowBar.SetCurrentValue(const Value: Double);
begin
  FCurrentValue := Value;
  Invalidate;
end;

constructor TVerticalHighLowBar.Create(AOwner: TComponent);
begin
  inherited;
  BM:= TBitMap.Create;
  BM.Canvas.Font.Name:= 'Verdana';
  BM.Canvas.Font.Size:= 10;
  BM.Canvas.Font.Style:= [fsBold];
  BM.Canvas.Font.Color:= clYellow;

  //PaintBox:= TPaintBox.Create(Self);
  //PaintBox.Parent:= Self;
  //PaintBox.Align:= alClient;
  Self.BevelInner:= bvNone;
  Self.BevelOuter:= bvNone;
end;

procedure TVerticalHighLowBar.Paint;
begin
  UpdateGraphics;
  Canvas.Draw(0, 0, BM);
end;

procedure TVerticalHighLowBar.Resize;
begin
  inherited;
  BM.Width := Self.ClientWidth;
  BM.Height := Self.ClientHeight;
end;

destructor TVerticalHighLowBar.Destroy;
begin
  BM.Free;
  inherited;
end;

procedure TVerticalHighLowBar.UpdateGraphics;
var W, H: Integer;
    Up, Lo: Integer;
    Tmp: Double;
    Size: TSize;
    Rect: TRect;
begin
  W:= ClientWidth;
  H:= ClientHeight - LabelHeight;
  BM.Canvas.Brush.Color:= clBlack;
  BM.Canvas.FillRect(ClientRect);

  BM.Canvas.Pen.Color:= clGray;
  BM.Canvas.Pen.Width:= 2;
  BM.Canvas.Rectangle(2, 2, W - 2, H - 2);

  BM.Canvas.Brush.Color:= clMoneyGreen;
  BM.Canvas.Rectangle(Width * 2 div 5, 2, Width * 3 div 5, H - 2);

  BM.Canvas.Brush.Color:= clYellow;
  BM.Canvas.Pen.Width:= 1;

  Up:= TranslateYFromValue(UpperLimit);
  Lo:= TranslateYFromValue(LowerLimit);

  BM.Canvas.Rectangle(Width * 1 div 5, Up, Width * 4 div 5, Lo);

  BM.Canvas.Pen.Width:= 3;
  BM.Canvas.Pen.Color:= clRed;

  H:= TranslateYFromValue(Self.CurrentValue);
  BM.Canvas.MoveTo(0, H);
  BM.Canvas.LineTo(W, H);

  if Selected then begin
    BM.Canvas.Brush.Color:= clYellow;
    BM.Canvas.Font.Color:= clBlack;
  end else begin
    BM.Canvas.Brush.Color:= clBlack;
    BM.Canvas.Font.Color:= clYellow;
  end;
  Rect.Left:= 0;
  Rect.Right:= Width;
  Rect.Bottom:= Height;
  Rect.Top:= ClientHeight - LabelHeight;
  BM.Canvas.FillRect(Rect);
  Size:= BM.Canvas.TextExtent(Legend);
  BM.Canvas.TextOut((W - Size.cx) div 2, ClientHeight - ((LabelHeight + Size.cy) div 2), Legend);
end;


function TVerticalHighLowBar.TranslateYFromValue(Value: Double): Integer;
var H: Integer;
begin
  Result:= 0;
  if  Abs(Self.MaximumValue - Self.MinimumValue) < 0.0001 then
    Exit;
  H:= ClientHeight - LabelHeight - 2;

  Value:= Value - Self.MinimumValue;
  // Translate to vertical ratio
  Value:= Value / (Self.MaximumValue - Self.MinimumValue);

  // Convert to screen position
  Value:= H - (Value * H);
  Result:= Round(Value);
end;

end.
