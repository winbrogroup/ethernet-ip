unit SignatureForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, Cnctypes,
  Dialogs, ExtCtrls, IFormInterface, AbstractFormPlugin, VerticalHighLowBar,
  SignatureData, NamedPlugin, INamedInterface, ECMState, PSUCommon, Scales, XYGraph, ComCtrls;

type
  TECMSignature = class(TInstallForm)
    FormDataPanel: TPanel;
    Panel12: TPanel;
    WP2Base: TPanel;
    OutletTemperatureBase: TPanel;
    OutletPressureBase: TPanel;
    OutletFlowBase: TPanel;
    WP3Base: TPanel;
    Panel2: TPanel;
    ACBase: TPanel;
    InletTemperatureBase: TPanel;
    InletPressureBase: TPanel;
    InletFlowBase: TPanel;
    WP1Base: TPanel;
    GraphBase: TPanel;
    GraphTimeLineBase: TPanel;
    GraphYBase: TPanel;
    GraphPanel: TPanel;
    GraphXBase: TPanel;
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    Graphs: array[TSignatureField] of TVerticalHighLowBar;
    Maxima: array[TSignatureField] of Double;
    TCGraph: TVerticalHighLowBar;
    LRGraph: TVerticalHighLowBar;
    RRGraph: TVerticalHighLowBar;

    InletFlowGraph: TVerticalHighLowBar;
    InletPressureGraph: TVerticalHighLowBar;
    InletTemperatureGraph: TVerticalHighLowBar;

    OutletFlowGraph: TVerticalHighLowBar;
    OutletPressureGraph: TVerticalHighLowBar;
    OutletTemperatureGraph: TVerticalHighLowBar;

    ACGraph: TVerticalHighLowBar;

    XScale: TXScale;
    YScale: TYScale;
    XYGraph: TXYGraph;

    Logging: Boolean;
    Monitoring: Boolean;
    LoggingStart: Double;
    LoggingSamples: Integer;

    Timer: TTimer;
    ActiveFilename: string;

    FormData: TListView;
    ShutdownState: Boolean;

    Sums: TSampleData;
    SumsCount: Integer;

    procedure UpdateGraph(Sender: TObject);
    procedure LoadNewSample(ATag: TTagRef);
    function CreateGraph(Base: TCustomControl; Min, Max, Up, Lo, Act: Double; Name: string): TVerticalHighLowBar;
    procedure CalculateLimits;
    procedure CalculateTimebase;
    procedure UpdateTableData(Index: Integer);
    procedure UpdateLimitsTableData;
  public
    LiveData: TSignatureData;
    SampleData: TSampleSetSignatureData;
    UpperLimit: TSignatureData;
    LowerLimit: TSignatureData;
    Activefield: TSignatureField;
    Average: TSignatureData;
    StartLogging: Boolean;
    LastStartLogging: Boolean;
    StartClearSamples: Boolean;
    StartAddLive: Boolean;
    procedure CloseForm; override;
    procedure Shutdown; override;

    procedure Install; override; stdcall;
    procedure Execute; override; stdcall;

    procedure SelectGraph(Index: TSignatureField);

    procedure SetLogging(State: Boolean);
    procedure SetMonitoring(State: Boolean);

    procedure AddLive;
    procedure ClearSamples;
  end;

var
  ECMSignature: TECMSignature;

implementation

{$R *.dfm}

const
  DisplayFormat: array[TSignatureField] of string = (
    '%.1f',
    '%.1f',
    '%.1f',
    '%.1f',
    '%.0f',
    '%.1f',
    '%.1f',
    '%.1f',
    '%.0f',
    '%.0f'
  );

  YScaleLegend: array[TSignatureField] of string = (
    'Inlet Flow / 0 - %.0fGPM',
    'Inlet Pressure / 0 - %.0fPSI',
    'Inlet Temperature / 0 - %.0fF',
    'Anode Current / 0 - %.0fA',
    'TC / 0 - %.0fA',
    'Outlet Flow / 0 - %.0fGPM',
    'Outlet Pressure / 0 - %.0fPSI',
    'Outlet Temperature / 0 - %.0fF',
    'LR / 0 - %.0fA',
    'RR / 0 - %.0fA'
  );

  XScaleLegend = '%d Samples of %.4f Increment';

  NoValueText= '-/-';

procedure SignatureGraphChange(ATag: TTagRef); stdcall;
var Index: Integer;
begin
  Index:= Named.GetAsInteger(ATag) mod (Ord(High(TSignatureField)) + 1);
  ECMSignature.SelectGraph(TSignatureField(Index));
end;

procedure StaticLoadNewSample(ATag: TTagRef); stdcall;
begin
  Named.EventLog('StaticLoadNewSample LoadNewSample');
  ECMSignature.LoadNewSample(ATag);
  ECMSignature.SelectGraph(ECMSignature.Activefield);
end;

procedure SetFormtop(ATag: TTagRef); stdcall;
begin
  ECMSignature.Top:= Named.GetAsInteger(ATag);
end;

procedure SetFormleft(ATag: TTagRef); stdcall;
begin
  ECMSignature.Left:= Named.GetAsInteger(ATag);
end;


procedure TECMSignature.ClearSamples;
begin
  SampleData.Clear;
  UpperLimit.Reset;
  LowerLimit.Reset;
  SelectGraph(ActiveField);
end;

procedure TECMSignature.AddLive;
begin
  Named.EventLog('Adding live signatutre: ' + ActiveFilename);
  SampleData.AddFieldData(LiveData.Clone);
  if ActiveFilename <> '' then
    SampleData.SaveToFile(ActiveFilename);
  LiveData.Reset;
  SelectGraph(ActiveField);
end;

function TECMSignature.CreateGraph(Base: TCustomControl; Min, Max, Up, Lo, Act: Double; Name: string): TVerticalHighLowBar;
begin
  Result:= TVerticalHighLowBar.Create(Self);
  Result.Parent:= Base;
  Result.Align:= alClient;
  Result.MinimumValue:= Min;
  Result.MaximumValue:= Max;
  Result.UpperLimit:= Up;
  Result.LowerLimit:= Lo;
  Result.CurrentValue:= Act;
  Result.Legend:= Name;
  Result.ControlStyle:= Result.ControlStyle + [csOpaque];
end;

procedure TECMSignature.Execute;
begin
  Self.Show;
  Width:= 1024;
  Height:= 768;
end;

procedure TECMSignature.FormCreate(Sender: TObject);
  procedure CreateSubItems(li: TListItem);
  begin
    li.SubItems.Add(NoValueText);
    li.SubItems.Add(NoValueText);
    li.SubItems.Add(NoValueText);
    li.SubItems.Add(NoValueText);
    li.SubItems.Add(NoValueText);
  end;

var lc : TListColumn;
    li : TListItem;
    ImageList: TImageList;
begin
  TCGraph:= CreateGraph(WP1Base, 0, RamCurrentTransducer * 2, 0, 0, 0, 'TC');
  LRGraph:= CreateGraph(WP2Base, 0, RamCurrentTransducer, 0, 0, 0, 'LR');
  RRGraph:= CreateGraph(WP3Base, 0, RamCurrentTransducer, 0, 0, 0, 'RR');

  InletFlowGraph:= CreateGraph(InletFlowBase, 0, FlowTransducer, 0, 0, 0, 'IF');
  InletPressureGraph:= CreateGraph(InletPressureBase, 0, PressureTransducer, 0, 0, 0, 'IP');
  InletTemperatureGraph:= CreateGraph(InletTemperatureBase, 0, TemperatureTransducer, 0, 0, 0, 'IT');

  OutletFlowGraph:= CreateGraph(OutletFlowBase, 0, FlowTransducer, 0, 0, 0, 'OF');
  OutletPressureGraph:= CreateGraph(OutletPressureBase, 0, PressureTransducer, 0, 0, 0, 'OP');
  OutletTemperatureGraph:= CreateGraph(OutletTemperatureBase, 0, TemperatureTransducer, 0, 0, 0, 'OT');
  ACGraph:= CreateGraph(ACBase, 0, AnodeCurrentTransducer, 0, 0, 0, 'AC');

  Graphs[sfInletFlow]:= InletFlowGraph;
  Graphs[sfInletPressure]:= InletPressureGraph;
  Graphs[sfInletTemperature]:= InletTemperatureGraph;
  Graphs[sfAnodeCurrent]:= ACGraph;
  Graphs[sfTotalCurrent]:= TCGraph;
  Graphs[sfOutletFlow]:= OutletFlowGraph;
  Graphs[sfOutletPressure]:= OutletPressureGraph;
  Graphs[sfOutletTemperature]:= OutletTemperatureGraph;
  Graphs[sfLeftRam]:= LRGraph;
  Graphs[sfRightRam]:= RRGraph;

  LiveData:= TSignatureData.Create;
  SampleData:= TSampleSetSignatureData.Create;
  UpperLimit:= TSignatureData.Create;
  LowerLimit:= TSignatureData.Create;
  Average:= TSignatureData.Create;

  XScale:= TXScale.Create(Self);
  XScale.Parent:= Self.GraphXBase;
  XScale.Align:= alClient;

  YScale:= TYScale.Create(Self);
  YScale.Parent:= GraphYBase;
  YScale.Align:= alClient;

  XScale.MinimumValue:= 0;
  XScale.MaximumValue:= 50;
  YScale.MinimumValue:= 0;

  XYGraph:= TXYGraph.Create(Self);
  XYGraph.Parent:= GraphPanel;
  XYGraph.Align:= alClient;

  XYGraph.XScale:= XScale;
  XYGraph.YScale:= YScale;

  ECMSignature:= Self;

  Timer:= TTimer.Create(Self);
  Timer.Interval:= 100;
  Timer.OnTimer:= UpdateGraph;

  SelectGraph(sfInletFlow);

  FormData:= TListView.Create(Self);
  FormData.Parent:= FormDataPanel;
  FormData.Align := alClient;
  FormData.ViewStyle := vsReport;
  FormData.GridLines := True;
  FormData.Font.Style:= [fsBold];
  FormData.Font.Height:= 14;
  ImageList:= TImageList.Create(Self);
  ImageList.Height:= 17;
  FormData.StateImages:= ImageList;

  lc:= FormData.Columns.Add;
  lc.Caption:= 'Field';
  lc.Width:= 200;

  lc:= FormData.Columns.Add;
  lc.Caption:= 'Value';
  lc.Width:= 150;

  lc:= FormData.Columns.Add;
  lc.Caption:= 'Dynamic Upper';
  lc.Width:= 150;

  lc:= FormData.Columns.Add;
  lc.Caption:= 'Dynamic Lower';
  lc.Width:= 150;

  lc:= FormData.Columns.Add;
  lc.Caption:= 'Maximum';
  lc.Width:= 150;

  lc:= FormData.Columns.Add;
  lc.Caption:= 'Minimum';
  lc.Width:= 150;


  li:= FormData.Items.Add;
  li.Caption:= 'Inlet Flow (IF)';
  CreateSubItems(li);

  li:= FormData.Items.Add;
  li.Caption:= 'Inlet Pressure (IP)';
  CreateSubItems(li);

  li:= FormData.Items.Add;
  li.Caption:= 'Inlet Temperature (IT)';
  CreateSubItems(li);

  li:= FormData.Items.Add;
  li.Caption:= 'Anode Current (AC)';
  CreateSubItems(li);

  li:= FormData.Items.Add;
  li.Caption:= 'Total Current (TC)';
  CreateSubItems(li);

  li:= FormData.Items.Add;
  li.Caption:= 'Outlet Flow (OF)';
  CreateSubItems(li);

  li:= FormData.Items.Add;
  li.Caption:= 'Outlet Pressure (OP)';
  CreateSubItems(li);

  li:= FormData.Items.Add;
  li.Caption:= 'Outlet Temperature (OT)';
  CreateSubItems(li);

  li:= FormData.Items.Add;
  li.Caption:= 'Left Ram Current (LR)';
  CreateSubItems(li);

  li:= FormData.Items.Add;
  li.Caption:= 'Right Ram Current (RR)';
  CreateSubItems(li);
end;

procedure TECMSignature.Install;
begin
  Named.AquireTag('SignatureGraphIndex', 'TIntegerTag', SignatureGraphChange);
  Named.AquireTag('NewActiveFile', 'TStringTag', StaticLoadNewSample);
  Named.AquireTag('SignatureFormLeft', 'TIntegerTag', SetFormLeft);
  Named.AquireTag('SignatureFormTop', 'TIntegerTag', SetFormTop);
end;

procedure TECMSignature.LoadNewSample(ATag: TTagRef);
begin
  ActiveFilename:= NamedGetAsString(ATag);
  Named.EventLog('LoadNewSample: ' + ActiveFilename);
  ActiveFilename:= ChangeFileExt(ActiveFilename, '.txt');
  SampleData.LoadFromFile(ActiveFilename);
  SelectGraph(ActiveField);
end;

procedure TECMSignature.SetLogging(State: Boolean);
begin
  Logging:= State;
  if State then begin
    LoggingStart:= NC.AxisPosition[0][ptMachine];
    LoggingSamples:= 0;
    LiveData.Reset;
    SelectGraph(ActiveField);
  end;
end;

procedure TECMSignature.SetMonitoring(State: Boolean);
begin
  Monitoring:= State;
end;

procedure TECMSignature.UpdateGraph;
var
    F: TSignatureField;
    OutOfTol: Boolean;
begin
  if ShutdownState = True then Exit;

  // The following is an edge detector to prevent corssiing threads from the PLC
  if StartLogging and not LastStartLogging then begin
    SetLogging(True);
    LiveData.Interval:= ECMState.ECMRequest.SampleInterval;
    SelectGraph(ActiveField);

    for F:= Low(F) to High(F) do begin
      Sums[F]:= 0;
    end;
    SumsCount:= 0;
  end;

  if not StartLogging and LastStartLogging then begin
    SetLogging(False);
  end;
  LastStartLogging:= StartLogging;

  if StartClearSamples then
    ClearSamples;

  if StartAddLive then
    AddLive;

  StartClearSamples:= False;
  StartAddLive:= False;
  // end of edge detection

  Sums[sfInletFlow]:= Sums[sfInletFlow] + ECMActual.InletFlow;
  Sums[sfInletPressure]:= Sums[sfInletPressure] + ECMActual.InletPressure;
  Sums[sfInletTemperature]:= Sums[sfInletTemperature] + ECMActual.InletTemperature;
  Sums[sfOutletFlow]:= Sums[sfOutletFlow] + ECMActual.OutletFlow;
  Sums[sfOutletPressure]:= Sums[sfOutletPressure] + ECMActual.OutletPressure;
  Sums[sfOutletTemperature]:= Sums[sfOutletTemperature] + ECMActual.OutletTemperature;
  Sums[sfAnodeCurrent]:= Sums[sfAnodeCurrent] + ECMActual.AnodeCurrent;
  Sums[sfTotalCurrent]:= Sums[sfTotalCurrent] + ECMActual.MainCurrent;
  Sums[sfRightRam]:= Sums[sfRightRam] + ECMActual.RHRCurrent;
  Sums[sfLeftRam]:= Sums[sfLeftRam] + ECMActual.LHRCurrent;
  Inc(SumsCount);


  if Logging then begin
    for F:= Low(F) to High(F) do begin
      if UpperLimit.Count > LoggingSamples then
        Graphs[F].UpperLimit:= UpperLimit.Value[F, LoggingSamples];

      if LowerLimit.Count > LoggingSamples then
        Graphs[F].LowerLimit:= LowerLimit.Value[F, LoggingSamples];
    end;
    UpdateTableData(LoggingSamples);

    if (LoggingStart + ((LoggingSamples + 1) * ECMRequest.SampleInterval)) < NC.AxisPosition[0][ptMachine] then begin
      for F:= Low(F) to High(F) do begin
        Sums[F]:= Sums[F] / SumsCount;
      end;
      SumsCount:= 0;
      Self.LiveData.Sample(Sums);
      for F:= Low(F) to High(F) do begin
        Sums[F] := 0;
      end;
      Named.EventLog(Format('Sample @ %.5f - InletFlow = %.1f', [NC.AxisPosition[0][ptMachine] - LoggingStart, ECMActual.InletFlow]));

      XYGraph.Invalidate;
      OutOfTol:= False;
      if Monitoring then begin
        for F:= Low(F) to High(F) do begin
          if UpperLimit.Count > LoggingSamples then begin
            if UpperLimit.Value[F, LoggingSamples] < LiveData.Value[F, LoggingSamples] then
               OutOfTol:= True;
          end;
          if LowerLimit.Count > LoggingSamples then begin
            if LowerLimit.Value[F, LoggingSamples] > LiveData.Value[F, LoggingSamples] then
               OutOfTol:= True;
          end;
        end;
      end;
      ECMActual.OutOfDynamicTolerance:= OutOfTol;
      Inc(LoggingSamples);
    end;
  end;

  UpdateLimitsTableData;

  TCGraph.CurrentValue:= ECMState.ECMActual.MainCurrent;
  RRGraph.CurrentValue:= ECMState.ECMActual.RHRCurrent;
  LRGraph.CurrentValue:= ECMState.ECMActual.LHRCurrent;

  InletFlowGraph.CurrentValue:= ECMState.ECMActual.InletFlow;
  InletPressureGraph.CurrentValue:= ECMState.ECMActual.InletPressure;
  InletTemperatureGraph.CurrentValue:= ECMState.ECMActual.InletTemperature;

  OutletFlowGraph.CurrentValue:= ECMState.ECMActual.OutletFlow;
  OutletPressureGraph.CurrentValue:= ECMState.ECMActual.OutletPressure;
  OutletTemperatureGraph.CurrentValue:= ECMState.ECMActual.OutletTemperature;

  ACGraph.CurrentValue:= ECMState.ECMActual.AnodeCurrent;

end;

procedure TECMSignature.SelectGraph(Index: TSignatureField);
var Incr: Double;
    F: TSignatureField;
begin
  ActiveField:= Index;

  for F:= Low(F) to High(F) do begin
    if F <> ActiveField then
      Graphs[F].Selected:= False
    else
      Graphs[F].Selected:= True;
  end;

  XScale.MinimumValue:= 0;

  CalculateTimeBase;

  // Calculate average and upper lower limits
  if SampleData.Count > 0 then
    CalculateLimits;

  YScale.MinimumValue:= Graphs[ActiveField].MinimumValue;
  YScale.MaximumValue:= Graphs[ActiveField].MaximumValue;

  Incr:= ECMRequest.SampleInterval;
  if SampleData.Count > 0 then
    Incr:= SampleData.Samples[0].Interval;

  YScale.Legend:= Format(YScaleLegend[ActiveField], [YScale.MaximumValue]);
  XScale.Legend:= Format(XScaleLegend, [Round(XScale.MaximumValue), Incr]);

  XScale.Repaint;
  YScale.Repaint;
  XYGraph.Repaint;
end;

procedure TECMSignature.FormResize(Sender: TObject);
var F: TSignatureField;
begin
  for F:= Low(F) to High(F) do begin
    Graphs[F].LabelHeight:= GraphXBase.Height;
  end;
end;

procedure TECMSignature.CalculateLimits;
var Sample, Point, I: Integer;
    Field: TSignatureField;
    Total, Value: Double;
    Count: Integer;
const GraphMaxima: array[0..10] of Double = ( 10, 20, 50, 100, 200, 500, 1000, 2000, 5000, 10000, 20000 );
begin
  Average.Reset;
  UpperLimit.Reset;
  LowerLimit.Reset;
  for Field:= Low(Field) to High(Field) do begin
    Maxima[Field]:= 0;
  end;

  for Point:= 0 to Round(XScale.MaximumValue) do begin
    for Field:= Low(Field) to High(Field) do begin
      Count:= 0;
      Total:= 0;
      for Sample:= 0 to SampleData.Count - 1 do begin
        if SampleData.Samples[Sample].Count > Point then begin
          Inc(Count);
          Total:= Total + SampleData.Samples[Sample].Value[Field, Point];
        end;
      end;
      if Count > 0 then begin
        Value:= Total / Count;
        Average.Field[Field].Add(Value);
        if Value > Maxima[Field] then
          Maxima[Field]:= Value;
        case Field of
          sfInletFlow: begin
            //if ECMRequest.InletUpperFlowLimit > 0 then begin
              UpperLimit.Field[Field].Add(Value + ECMRequest.InletUpperFlowLimit);
              LowerLimit.Field[Field].Add(Value - ECMRequest.InletLowerFlowLimit);
            //end;
          end;
          sfInletPressure: begin
            //if ECMRequest.InletUpperPressureLimit > 0 then begin
              UpperLimit.Field[Field].Add(Value + ECMRequest.InletUpperPressureLimit);
              LowerLimit.Field[Field].Add(Value - ECMRequest.InletLowerPressureLimit);
            //end;
          end;
          sfInletTemperature: begin
            //if ECMRequest.InletUpperTemeratureLimit > 0 then begin
              UpperLimit.Field[Field].Add(Value + ECMRequest.InletUpperTemeratureLimit);
              LowerLimit.Field[Field].Add(Value - ECMRequest.InletLowerTemeratureLimit);
            //end;
          end;
          sfAnodeCurrent: begin
            //if ECMRequest.BiasAnodeUpperLimit > 0 then begin
              UpperLimit.Field[Field].Add(Value + ECMRequest.BiasAnodeUpperLimit);
              LowerLimit.Field[Field].Add(Value - ECMRequest.BiasAnodeLowerLimit);
            //end;
          end;
          sfTotalCurrent: begin
            //if ECMRequest.WP1UpperCurrentLimit > 0 then begin
              UpperLimit.Field[Field].Add(Value + ECMRequest.WP1UpperCurrentLimit);
              LowerLimit.Field[Field].Add(Value - ECMRequest.WP1LowerCurrentLimit);
            //end;
          end;
          sfOutletFlow: begin
            //if ECMRequest.OutletUpperFlowLimit > 0 then begin
              UpperLimit.Field[Field].Add(Value + ECMRequest.OutletUpperFlowLimit);
              LowerLimit.Field[Field].Add(Value - ECMRequest.OutletLowerFlowLimit);
            //end;
          end;
          sfOutletPressure: begin
            //if ECMRequest.OutletUpperPressureLimit > 0 then begin
              UpperLimit.Field[Field].Add(Value + ECMRequest.OutletUpperPressureLimit);
              LowerLimit.Field[Field].Add(Value - ECMRequest.OutletLowerPressureLimit);
            //end;
          end;
          sfOutletTemperature: begin
            //if ECMRequest.OutletUpperTemeratureLimit > 0 then begin
              UpperLimit.Field[Field].Add(Value + ECMRequest.OutletUpperTemeratureLimit);
              LowerLimit.Field[Field].Add(Value - ECMRequest.OutletLowerTemeratureLimit);
            //end;
          end;
          sfLeftRam: begin
            //if ECMRequest.WP2UpperCurrentLimit > 0 then begin
              UpperLimit.Field[Field].Add(Value + ECMRequest.WP2UpperCurrentLimit);
              LowerLimit.Field[Field].Add(Value - ECMRequest.WP2LowerCurrentLimit);
            //end;
          end;
          sfRightRam: begin
            //if ECMRequest.WP3UpperCurrentLimit > 0 then begin
              UpperLimit.Field[Field].Add(Value + ECMRequest.WP3UpperCurrentLimit);
              LowerLimit.Field[Field].Add(Value - ECMRequest.WP3LowerCurrentLimit);
            //end;
          end;
        end;
      end;
    end;
  end;
  for Field:= Low(Field) to High(Field) do begin
    for I:= 0 to High(GraphMaxima) do begin
      if Maxima[Field] > 0 then begin
        if Maxima[Field] * 1.2 < (GraphMaxima[I]) then begin
          Maxima[Field]:= GraphMaxima[I];
          Break;
        end
      end;
    end;
  end;

  for Field:= Low(Field) to High(Field) do begin
    if Maxima[Field] > 0 then
      Graphs[Field].MaximumValue:= Maxima[Field];
  end;
end;

procedure TECMSignature.CalculateTimebase;
var I: Integer;
    MaxSampleSize: Integer;

begin
  MaxSampleSize:= 0;
  for I:= 0 to SampleData.Count - 1 do begin
    if SampleData.Samples[I].Count > MaxSampleSize then
      MaxSampleSize:= SampleData.Samples[I].Count;
  end;

  if MaxSampleSize = 0 then
    MaxSampleSize:= 50;

  if LiveData.Count > MaxSampleSize then
    MaxSampleSize:= LiveData.Count + 10;

  XScale.MinimumValue:= 0;
  XScale.MaximumValue:= MaxSampleSize;
end;

procedure TECMSignature.UpdateTableData(Index: Integer);
var F: TSignatureField;
begin
  for F:= Low(F) to High(F) do begin
    FormData.Items[Ord(F)].SubItems[0]:= Format(DisplayFormat[F], [Graphs[F].Currentvalue]);

    if (SampleData.Count > 0) and (UpperLimit.Count >= Index) and (UpperLimit.Value[F, Index] > 0) then
      FormData.Items[Ord(F)].SubItems[1]:= Format(DisplayFormat[F], [UpperLimit.Value[F, Index]])
    else
      FormData.Items[Ord(F)].SubItems[1]:= NoValueText;

    if (SampleData.Count > 0) and (LowerLimit.Count >= Index) and (LowerLimit.Value[F, Index] > 0) then
      FormData.Items[Ord(F)].SubItems[2]:= Format(DisplayFormat[F], [LowerLimit.Value[F, Index]])
    else
      FormData.Items[Ord(F)].SubItems[2]:= NoValueText;
  end;
end;

procedure TECMSignature.UpdateLimitsTableData;
begin
  if ECMRequest.InletFlowHighLimit > 0 then
    FormData.Items[Ord(sfInletFlow)].SubItems[3]:= Format(DisplayFormat[sfInletFlow], [ECMRequest.InletFlowHighLimit])
  else
    FormData.Items[Ord(sfInletFlow)].SubItems[3]:= NoValueText;

  if ECMRequest.InletFlowLowLimit > 0 then
    FormData.Items[Ord(sfInletFlow)].SubItems[4]:= Format(DisplayFormat[sfInletFlow], [ECMRequest.InletFlowLowLimit])
  else
    FormData.Items[Ord(sfInletFlow)].SubItems[4]:= NoValueText;


  if ECMRequest.InletPressureHighLimit > 0 then
    FormData.Items[Ord(sfInletPressure)].SubItems[3]:= Format(DisplayFormat[sfInletPressure], [ECMRequest.InletPressureHighLimit])
  else
    FormData.Items[Ord(sfInletPressure)].SubItems[3]:= NoValueText;

  if ECMRequest.InletPressureLowLimit > 0 then
    FormData.Items[Ord(sfInletPressure)].SubItems[4]:= Format(DisplayFormat[sfInletPressure], [ECMRequest.InletPressureLowLimit])
  else
    FormData.Items[Ord(sfInletPressure)].SubItems[4]:= NoValueText;


  if ECMRequest.InletTemperatureHighLimit > 0 then
    FormData.Items[Ord(sfInletTemperature)].SubItems[3]:= Format(DisplayFormat[sfInletTemperature], [ECMRequest.InletTemperatureHighLimit])
  else
    FormData.Items[Ord(sfInletTemperature)].SubItems[3]:= NoValueText;

  if ECMRequest.InletTemperatureLowLimit > 0 then
    FormData.Items[Ord(sfInletTemperature)].SubItems[4]:= Format(DisplayFormat[sfInletTemperature], [ECMRequest.InletTemperatureLowLimit])
  else
    FormData.Items[Ord(sfInletTemperature)].SubItems[4]:= NoValueText;


  if ECMRequest.OutletFlowHighLimit > 0 then
    FormData.Items[Ord(sfOutletFlow)].SubItems[3]:= Format(DisplayFormat[sfOutletFlow], [ECMRequest.OutletFlowHighLimit])
  else
    FormData.Items[Ord(sfOutletFlow)].SubItems[3]:= NoValueText;

  if ECMRequest.OutletFlowLowLimit > 0 then
    FormData.Items[Ord(sfOutletFlow)].SubItems[4]:= Format(DisplayFormat[sfOutletFlow], [ECMRequest.OutletFlowLowLimit])
  else
    FormData.Items[Ord(sfOutletFlow)].SubItems[4]:= NoValueText;


  if ECMRequest.OutletPressureHighLimit > 0 then
    FormData.Items[Ord(sfOutletPressure)].SubItems[3]:= Format(DisplayFormat[sfOutletPressure], [ECMRequest.OutletPressureHighLimit])
  else
    FormData.Items[Ord(sfOutletPressure)].SubItems[3]:= NoValueText;

  if ECMRequest.OutletPressureLowLimit > 0 then
    FormData.Items[Ord(sfOutletPressure)].SubItems[4]:= Format(DisplayFormat[sfOutletPressure], [ECMRequest.OutletPressureLowLimit])
  else
    FormData.Items[Ord(sfOutletPressure)].SubItems[4]:= NoValueText;


  if ECMRequest.OutletTemperatureHighLimit > 0 then
    FormData.Items[Ord(sfOutletTemperature)].SubItems[3]:= Format(DisplayFormat[sfOutletTemperature], [ECMRequest.OutletTemperatureHighLimit])
  else
    FormData.Items[Ord(sfOutletTemperature)].SubItems[3]:= NoValueText;

  if ECMRequest.OutletTemperatureLowLimit > 0 then
    FormData.Items[Ord(sfOutletTemperature)].SubItems[4]:= Format(DisplayFormat[sfOutletTemperature], [ECMRequest.OutletTemperatureLowLimit])
  else
    FormData.Items[Ord(sfOutletTemperature)].SubItems[4]:= NoValueText;

end;

procedure TECMSignature.CloseForm;
begin
  Close;
end;

procedure TECMSignature.Shutdown;
begin
  Timer.Enabled:= False;
  ShutdownState:= True;
end;

initialization
 TAbstractFormPlugin.AddPlugin('ECMSignature', TECMSignature);
end.
