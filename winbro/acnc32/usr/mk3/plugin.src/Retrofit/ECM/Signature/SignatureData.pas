unit SignatureData;

interface

uses ECMState, Classes, Sysutils, CncTypes, NamedPlugin;

type
  TSignatureField = (
    sfInletFlow,
    sfInletPressure,
    sfInletTemperature,
    sfAnodeCurrent,
    sfTotalCurrent,
    sfOutletFlow,
    sfOutletPressure,
    sfOutletTemperature,
    sfLeftRam,
    sfRightRam
  );

  TSampleData = array[TSignatureField] of Double;

  TSignatureFieldData = class
  private
    FValues: array[0..500] of Double;
    FName: string;
    FField: TSignatureField;
    FSamples: Integer;
    function GetValue(Index: Integer): Double;
  public
    procedure Reset;
    constructor Create(AField: TSignatureField; AName: string);
    //procedure Sample;
    procedure Add(Value: Double);
    property Field: TSignatureField read FField;
    property Samples: Integer read FSamples;
    property Value[Index: Integer]: Double read GetValue;
  end;

  TSignatureData = class
  private
    FInterval: Double;
    FFields: array[TSignatureField] of TSignatureFieldData;
    function GetValue(Field: TSignatureField; Index: Integer): Double;
    function GetField(Field: TSignatureField): TSignatureFieldData;
  public
    constructor Create;
    property Value[Field: TSignatureField; Index: Integer]: Double read GetValue;
    function Count: Integer;
    procedure Sample(Data: TSampleData);
    procedure Reset;
    function Clone: TSignatureData;
    property Interval: Double read FInterval write FInterval;
    property Field[Field: TSignatureField]: TSignatureFieldData read GetField;
  end;

  TSampleSetSignatureData = class
  private
    List: TList;
    function GetSammples(Index: Integer): TSignatureData;
    function GetCount: Integer;
  public
    constructor Create;
    procedure LoadFromFile(Filename: string);
    procedure SaveToFile(Filename: string);
    procedure AddFieldData(Data: TSignatureData);
    property Samples[Index: Integer]: TSignatureData read GetSammples;
    property Count: Integer read GetCount;
    procedure Clear;
  end;


implementation

{ TSignatureFieldData }

function TSignatureData.Clone: TSignatureData;
var I: Integer;
    F: TSignatureField;
begin
  Result:= TSignatureData.Create;
  Result.FInterval:= Interval;

  for I:= 0 to FFields[sfInletFlow].Samples - 1 do begin
    for F:= Low(F) to High(F) do begin
      Result.FFields[F].Add(Value[F, I]);
    end;
  end;
end;

function TSignatureData.Count: Integer;
begin
  Result:= FFields[sfInletFlow].Samples;
end;

constructor TSignatureData.Create;
begin
  FFields[sfInletFlow]:= TSignatureFieldData.Create(sfInletFlow, 'IF');
  FFields[sfInletPressure]:= TSignatureFieldData.Create(sfInletPressure, 'IP');
  FFields[sfInletTemperature]:= TSignatureFieldData.Create(sfInletTemperature, 'IT');
  FFields[sfOutletFlow]:= TSignatureFieldData.Create(sfOutletFlow, 'OF');
  FFields[sfOutletPressure]:= TSignatureFieldData.Create(sfOutletPressure, 'OP');
  FFields[sfOutletTemperature]:= TSignatureFieldData.Create(sfOutletTemperature, 'OT');
  FFields[sfTotalCurrent]:= TSignatureFieldData.Create(sfTotalCurrent, 'TC');
  FFields[sfLeftRam]:= TSignatureFieldData.Create(sfLeftRam, 'LR');
  FFields[sfRightRam]:= TSignatureFieldData.Create(sfRightRam, 'RR');
  FFields[sfAnodeCurrent]:= TSignatureFieldData.Create(sfAnodeCurrent, 'AC');
end;

function TSignatureData.GetField(Field: TSignatureField): TSignatureFieldData;
begin
  Result:= FFields[Field];
end;

function TSignatureData.GetValue(Field: TSignatureField; Index: Integer): Double;
begin
  Result:= FFields[Field].FValues[Index];
end;

procedure TSignatureFieldData.Add(Value: Double);
begin
  FValues[Samples]:= Value;
  Inc(FSamples);
end;

constructor TSignatureFieldData.Create(AField: TSignatureField; AName: string);
begin
  FName:= AName;
  FField:= AField;
  FSamples:= 0;
end;

function TSignatureFieldData.GetValue(Index: Integer): Double;
begin
  Result:= FValues[Index];
end;

procedure TSignatureFieldData.Reset;
begin
  FSamples:= 0;
end;

{
procedure TSignatureFieldData.Sample;
var Value: Double;
begin
  case Field of
    sfInletFlow: Value:= ECMActual.InletFlow;
    sfInletPressure: Value:= ECMActual.InletPressure;
    sfInletTemperature: Value:= ECMActual.InletTemperature;
    sfOutletFlow: Value:= ECMActual.OutletFlow;
    sfOutletPressure: Value:= ECMActual.OutletPressure;
    sfOutletTemperature: Value:= ECMActual.OutletTemperature;
    sfWP1: Value:= ECMActual.MainCurrent;
    sfWP2: Value:= ECMActual.RHRCurrent;
    sfWP3: Value:= ECMActual.LHRCurrent;
    sfAnodeCurrent: Value:= ECMActual.AnodeCurrent;
  else
    Value:= 0;
  end;
  Self.FValues[Samples]:= Value;
  Inc(FSamples);
end;
}

{ TSampleSetSignatureFieldData }

constructor TSampleSetSignatureData.Create;
begin
  List:= TList.Create;
end;

procedure TSampleSetSignatureData.AddFieldData(Data: TSignatureData);
begin
  List.Add(Data);
end;

function TSampleSetSignatureData.GetSammples(Index: Integer): TSignatureData;
begin
  Result:= TSignatureData(List[Index]);
end;

procedure TSampleSetSignatureData.SaveToFile(Filename: string);
var List: TStringList;
    I, J: Integer;
    Sigset: TSignatureData;
begin
  List:= TStringList.Create;
  try
    for I:= 0 to Count - 1 do begin
      Sigset:= Self.GetSammples(I);
      List.Add('sample ' + Format('%7.5f', [Sigset.Interval]));
      for J:= 0 to Sigset.Count - 1 do begin
        List.Add(
        Format('%7.5f,', [Sigset.Value[TSignatureField(0), J]]) +
        Format('%7.5f,', [Sigset.Value[TSignatureField(1), J]]) +
        Format('%7.5f,', [Sigset.Value[TSignatureField(2), J]]) +
        Format('%7.5f,', [Sigset.Value[TSignatureField(3), J]]) +
        Format('%7.5f,', [Sigset.Value[TSignatureField(4), J]]) +
        Format('%7.5f,', [Sigset.Value[TSignatureField(5), J]]) +
        Format('%7.5f,', [Sigset.Value[TSignatureField(6), J]]) +
        Format('%7.5f,', [Sigset.Value[TSignatureField(7), J]]) +
        Format('%7.5f,', [Sigset.Value[TSignatureField(8), J]]) +
        Format('%7.5f' , [Sigset.Value[TSignatureField(9), J]])  );
      end;
      List.Add('endsample');
    end;
    List.SaveToFile(Filename);
  except
    on E: Exception do
      Named.EventLog('Signature failed to save: ' + E.Message);
  end;
end;

procedure TSampleSetSignatureData.LoadFromFile(Filename: string);
var Lines: TStringList;
    I: Integer;
    Fields: TSignatureData;
    Interval: Double;
    Line: string;
    Count: Integer;
  function ReadHeader: Double;
  begin
    while(Count < Lines.Count) do begin
      Line:= Trim(Lines[Count]);
      if Line <> '' then begin
        if LowerCase(ReadDelimited(Line, ' ')) <> 'sample' then
          raise Exception.Create('');
        Result:= StrToFloat(Trim(Line));
        Exit;
      end;
      Inc(Count);
    end;
    Line:= Lines[Count];

  end;

var F: TSignatureField;
begin
  Count:= 0;
  Clear;
  Lines:= TStringList.Create;
  try
    try
      Lines.LoadFromFile(Filename);
      while Count <= Lines.Count do begin
        Interval:= ReadHeader;
        Inc(Count);
        Fields:= TSignatureData.Create;
        Fields.Interval:= Interval;
        AddFieldData(Fields);
        while(Trim(Lowercase(Lines[Count])) <> 'endsample') do begin
          Line:= Lines[Count];
          for F:= Low(TSignatureField) to High(TSignatureField) do begin
            Fields.FFields[F].Add( StrToFloat(ReadDelimited(Line, ',')));
          end;
          Inc(Count);
        end;
        Inc(Count);
      end;
    except
    end;
  finally
    Lines.Free;
  end;
end;

function TSampleSetSignatureData.GetCount: Integer;
begin
  Result:= List.Count;
end;

procedure TSignatureData.Reset;
var F: TSignatureField;
begin
  for F:= Low(TSignatureField) to High(TSignatureField) do begin
    FFields[F].Reset;
  end;
end;

procedure TSignatureData.Sample(Data: TSampleData);
var I: TSignatureField;
begin
// FFields: array[0..9] of TSignatureFieldData;
  for I:= Low(TSignatureField) to High(TSignatureField) do begin
    if Assigned(FFields[I]) then
      FFields[I].Add(Data[I]);
  end;
end;

procedure TSampleSetSignatureData.Clear;
begin
  while List.Count > 0 do begin
    Samples[0].Free;
    List.Delete(0);
  end;
end;

end.
