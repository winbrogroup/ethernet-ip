unit XYGraph;

interface

uses Classes, Windows, Graphics, ExtCtrls, Scales, SignatureData;

type
  TXYGraph = class(TCustomPanel)
  private
    FCount: Integer;
    FXScale: TScale;
    FYScale: TScale;

    BM: TBitMap;

    LimitPen: TPen;
    TracePen: TPen;
    SamplePen: TPen;
    ScalePen: TPen;

    procedure UpdateGraphics;
    procedure DrawTrace(Data: TSignatureData; Field: TSignatureField; Pen: TPen);
  protected
    procedure Resize; override;
    procedure Paint; override;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;

    property Count: Integer read FCount;

    property XScale: TScale read FXScale write FXScale;
    property YScale: TScale read FYScale write FYScale;
  end;

implementation

uses
  Controls, SignatureForm;

{ TXYGraph }

constructor TXYGraph.Create(AOwner: TComponent);
begin
  inherited;
  BM:= TBitMap.Create;
  Self.BevelInner:= bvNone;
  Self.BevelOuter:= bvNone;

  LimitPen:= TPen.Create;
  LimitPen.Color:= clRed;
  LimitPen.Style:= psDash;
  LimitPen.Width:= 1;
  LimitPen.Mode:= pmCopy;


  TracePen:= TPen.Create;
  TracePen.Color:= clYellow;
  TracePen.Style:= psSolid;
  TracePen.Width:= 3;
  TracePen.Mode:= pmCopy;

  SamplePen:= TPen.Create;
  SamplePen.Color:= clBlue;
  SamplePen.Style:= psSolid;
  SamplePen.Width:= 2;
  SamplePen.Mode:= pmCopy;

  ScalePen:= TPen.Create;
  ScalePen.Color:= $404040;
  ScalePen.Style:= psSolid;
  ScalePen.Width:= 1;
  ScalePen.Mode:= pmCopy;

  ControlStyle:= ControlStyle + [csOpaque];
end;

procedure TXYGraph.Paint;
begin
  UpdateGraphics;
  Canvas.Draw(0, 0, BM);
end;

procedure TXYGraph.Resize;
begin
  inherited;
  BM.Width := Self.ClientWidth;
  BM.Height := Self.ClientHeight;
end;

destructor TXYGraph.Destroy;
begin
  BM.Free;
  inherited;
end;

procedure TXYGraph.UpdateGraphics;
var F: Integer;
begin
  BM.Canvas.Brush.Color:= clBlack;
  BM.Canvas.FillRect(ClientRect);

  BM.Canvas.Pen:= ScalePen;
  for F:= 0 to XScale.Ticks - 1 do begin
    BM.Canvas.MoveTo(XScale.GetScreenOrdinate(XScale.GetTick(F)), YScale.GetScreenOrdinate(YScale.MinimumValue));
    BM.Canvas.LineTo(XScale.GetScreenOrdinate(XScale.GetTick(F)), YScale.GetScreenOrdinate(YScale.MaximumValue));
  end;

  for F:= 0 to XScale.Ticks - 1 do begin
    BM.Canvas.MoveTo(XScale.GetScreenOrdinate(XScale.MinimumValue), YScale.GetScreenOrdinate(YScale.GetTick(F)));
    BM.Canvas.LineTo(XScale.GetScreenOrdinate(XScale.MaximumValue), YScale.GetScreenOrdinate(YScale.GetTick(F)));
  end;

  for F:= 0 to ECMSignature.SampleData.Count - 1 do begin
    DrawTrace(ECMSignature.SampleData.Samples[F], ECMSignature.Activefield, SamplePen);
  end;

  DrawTrace(ECMSignature.UpperLimit, ECMSignature.Activefield, LimitPen);
  DrawTrace(ECMSignature.LowerLimit, ECMSignature.Activefield, LimitPen);
  DrawTrace(ECMSignature.LiveData, ECMSignature.Activefield, TracePen);
end;

procedure TXYGraph.DrawTrace(Data: TSignatureData; Field: TSignatureField; Pen: TPen);
var I: Integer;
begin
  if Data.Count = 0 then
    Exit;

  BM.Canvas.Pen:= Pen;
  BM.Canvas.MoveTo(XScale.GetScreenOrdinate(0), YScale.GetScreenOrdinate( Data.Value[Field, 0]));
  for I:= 0 to Data.Count - 1 do begin
    BM.Canvas.LineTo(XScale.GetScreenOrdinate(I), YScale.GetScreenOrdinate( Data.Value[Field, I]));
  end;
end;

end.
