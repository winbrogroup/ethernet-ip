object ECMSignature: TECMSignature
  Left = 0
  Top = -100
  BorderStyle = bsNone
  Caption = 'ECMSignature'
  ClientHeight = 615
  ClientWidth = 1024
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesigned
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object FormDataPanel: TPanel
    Left = 0
    Top = 405
    Width = 1024
    Height = 210
    Align = alBottom
    Caption = 'FormDataPanel'
    TabOrder = 0
  end
  object Panel12: TPanel
    Left = 824
    Top = 0
    Width = 200
    Height = 405
    Align = alRight
    BevelOuter = bvNone
    Caption = 'Panel12'
    FullRepaint = False
    TabOrder = 1
    DesignSize = (
      200
      405)
    object WP2Base: TPanel
      Left = 120
      Top = 0
      Width = 50
      Height = 405
      Anchors = [akTop, akRight, akBottom]
      BevelOuter = bvNone
      Caption = 'WP2'
      FullRepaint = False
      TabOrder = 0
    end
    object OutletTemperatureBase: TPanel
      Left = 80
      Top = 0
      Width = 50
      Height = 405
      Anchors = [akTop, akRight, akBottom]
      BevelOuter = bvNone
      Caption = 'Temp'
      FullRepaint = False
      TabOrder = 1
    end
    object OutletPressureBase: TPanel
      Left = 40
      Top = 0
      Width = 50
      Height = 405
      Anchors = [akTop, akRight, akBottom]
      BevelOuter = bvNone
      Caption = 'Press'
      FullRepaint = False
      TabOrder = 2
    end
    object OutletFlowBase: TPanel
      Left = 0
      Top = 0
      Width = 50
      Height = 405
      Anchors = [akTop, akRight, akBottom]
      BevelOuter = bvNone
      Caption = 'Flow'
      FullRepaint = False
      TabOrder = 3
    end
    object WP3Base: TPanel
      Left = 160
      Top = 0
      Width = 50
      Height = 405
      Anchors = [akTop, akRight, akBottom]
      BevelOuter = bvNone
      Caption = 'WP3'
      FullRepaint = False
      TabOrder = 4
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 200
    Height = 405
    Align = alLeft
    BevelOuter = bvNone
    Caption = 'Panel12'
    FullRepaint = False
    TabOrder = 2
    DesignSize = (
      200
      405)
    object ACBase: TPanel
      Left = 120
      Top = 0
      Width = 40
      Height = 405
      Anchors = [akTop, akRight, akBottom]
      BevelOuter = bvNone
      Caption = 'AC'
      FullRepaint = False
      TabOrder = 0
    end
    object InletTemperatureBase: TPanel
      Left = 80
      Top = 0
      Width = 40
      Height = 405
      Anchors = [akTop, akRight, akBottom]
      BevelOuter = bvNone
      Caption = 'Temp'
      FullRepaint = False
      TabOrder = 1
    end
    object InletPressureBase: TPanel
      Left = 40
      Top = 0
      Width = 40
      Height = 405
      Anchors = [akTop, akRight, akBottom]
      BevelOuter = bvNone
      Caption = 'Press'
      FullRepaint = False
      TabOrder = 2
    end
    object InletFlowBase: TPanel
      Left = 0
      Top = 0
      Width = 40
      Height = 405
      Anchors = [akTop, akRight, akBottom]
      BevelOuter = bvNone
      Caption = 'Flow'
      FullRepaint = False
      TabOrder = 3
    end
    object WP1Base: TPanel
      Left = 160
      Top = 0
      Width = 40
      Height = 405
      Anchors = [akTop, akRight, akBottom]
      BevelOuter = bvNone
      Caption = 'WP1'
      FullRepaint = False
      TabOrder = 4
    end
  end
  object GraphBase: TPanel
    Left = 200
    Top = 0
    Width = 624
    Height = 405
    Align = alClient
    BevelOuter = bvNone
    Caption = 'GraphBase'
    FullRepaint = False
    TabOrder = 3
    object GraphTimeLineBase: TPanel
      Left = 0
      Top = 353
      Width = 624
      Height = 52
      Align = alBottom
      BevelOuter = bvNone
      Caption = 'GraphTimeLineBase'
      Color = clBlack
      TabOrder = 0
      object GraphXBase: TPanel
        Left = 56
        Top = 0
        Width = 568
        Height = 52
        Align = alRight
        Anchors = [akLeft, akTop, akRight, akBottom]
        BevelOuter = bvNone
        Caption = 'GraphXBase'
        FullRepaint = False
        TabOrder = 0
      end
    end
    object GraphYBase: TPanel
      Left = 0
      Top = 0
      Width = 57
      Height = 353
      Align = alLeft
      BevelOuter = bvNone
      Caption = 'GraphValuePanel'
      FullRepaint = False
      TabOrder = 1
    end
    object GraphPanel: TPanel
      Left = 57
      Top = 0
      Width = 567
      Height = 353
      Align = alClient
      BevelOuter = bvNone
      Caption = 'GraphPanel'
      FullRepaint = False
      TabOrder = 2
    end
  end
end
