unit ECMState;

interface

type
  TECMRequestState = class
  public
    DcOnConstantVoltage: Boolean;
    DcOnPulsed: Boolean;
    DcOnConstantCurrent: Boolean;

    DcSuspendStart: Boolean; // Command only DC off
    DcSuspendEnd: Boolean;  // Command only DC on

    DCO: Integer; // percent

    // G100 V R H L
    OnTimeVoltage: Double;            //  Comms
    OnTimeVoltageRampTime: Double;
    OnTimeVoltageHighLimit: Double;
    OnTimeVoltageLowLimit: Double;

    //G101 Off time voltage V H L
    OffTimeVoltage: Double;           // Comms
    OffTimeVoltageHighLimit: Double;
    OffTimeVoltageLowLimit: Double;

    //G102 Current settings I R H L
    Current: Double;                  // Comms
    CurrentRampTime: Double;
    CurrentHighLimit: Double;
    CurrentLowLimit: Double;

    //G103 Pulse Settings F P D
    Frequency: Double;                // > On > Off
    Period: Double;
    Duty: Double;

    OnTime: Double;  // Generated internally from the above
    OffTime: Double;

    //G104 Sperk Sensitivity L R
    SparkSensitivityLeft: Double;     // Comms
    SparkSensitivityRight: Double;

    //G105 Protective Anode V
    ProtectiveAnodeVoltage: Double;   // Comms

    //G106 dIdT A B
    dIdTPositiveChange: Double;       // Comms
    dIdTNegativeChange: Double;

    //G110 Electrolyte Inlet Pressure P H L
    InletPressure: Double;            // > IO
    InletPressureHighLimit: Double;   // < IO
    InletPressureLowLimit: Double;

    //G111 Electrolyte Inlet Flow H L
    InletFlowHighLimit: Double;       // < IO
    InletFlowLowLimit: Double;

    //G112 Electrolyte Inlet Temperature H L
    InletTemperatureHighLimit: Double; // < IO
    InletTemperatureLowLimit: Double;

    //G113 OutletPressureLowLimit
    OutletPressure: Double;            // > IO
    OutletPressureHighLimit: Double;   // < IO
    OutletPressureLowLimit: Double;

    //G114 Electrolyte outlet Flow H L
    OutletFlowHighLimit: Double;       // < IO
    OutletFlowLowLimit: Double;

    //G115 Electrolyte Outlet Temperature
    OutletTemperatureHighLimit: Double;  // < IO
    OutletTemperatureLowLimit: Double;

    //G130 Pressure Dynamic Limit A B C D
    InletUpperPressureLimit: Double;
    InletLowerPressureLimit: Double;
    OutletUpperPressureLimit: Double;
    OutletLowerPressureLimit: Double;

    //G131 Temperature Dynamic Limits A B C D
    InletUpperTemeratureLimit: Double;
    InletLowerTemeratureLimit: Double;
    OutletUpperTemeratureLimit: Double;
    OutletLowerTemeratureLimit: Double;

    //G132 Flow Dynamic Limit A B C D
    InletUpperFlowLimit: Double;
    InletLowerFlowLimit: Double;
    OutletUpperFlowLimit: Double;
    OutletLowerFlowLimit: Double;

    //G133 Bias Anode Current Dynamic Limits A B
    BiasAnodeUpperLimit: Double; // < Comms
    BiasAnodeLowerLimit: Double;

    //G134 Workpiece 1 Current Dynamic Limits A B
    WP1UpperCurrentLimit: Double; // < Comms
    WP1LowerCurrentLimit: Double;

    //G135 Sample Interval I
    SampleInterval: Double;

    //G136 Workpiece 1 Current Dynamic Limits A B
    WP2UpperCurrentLimit: Double; // < Comms
    WP2LowerCurrentLimit: Double;

    //G137 Workpiece 1 Current Dynamic Limits A B
    WP3UpperCurrentLimit: Double; // < Comms
    WP3LowerCurrentLimit: Double;


    // The following are transferred from the programmed
    // whem CopyProgrammed is called
    // G100 V R H L
    SetOnTimeVoltage: Double;
    SetOnTimeVoltageHighLimit: Double;
    SetOnTimeVoltageLowLimit: Double;

    //G101 Off time voltage V H L
    SetOffTimeVoltage: Double;
    SetOffTimeVoltageHighLimit: Double;
    SetOffTimeVoltageLowLimit: Double;

    //G102 Current settings I R H L
    SetCurrent: Double;
    SetCurrentHighLimit: Double;
    SetCurrentLowLimit: Double;

    //G110 Electrolyte Inlet Pressure P H L
    SetInletPressure: Double;
    SetInletPressureHighLimit: Double;   // < IO
    SetInletPressureLowLimit: Double;

    //G111 Electrolyte Inlet Flow H L
    SetInletFlowHighLimit: Double;       // < IO
    SetInletFlowLowLimit: Double;

    //G112 Electrolyte Inlet Temperature H L
    SetInletTemperatureHighLimit: Double; // < IO
    SetInletTemperatureLowLimit: Double;

    //G113 OutletPressureLowLimit
    SetOutletPressure: Double;
    SetOutletPressureHighLimit: Double;   // < IO
    SetOutletPressureLowLimit: Double;

    //G114 Electrolyte outlet Flow H L
    SetOutletFlowHighLimit: Double;       // < IO
    SetOutletFlowLowLimit: Double;

    //G115 Electrolyte Outlet Temperature
    SetOutletTemperatureHighLimit: Double;  // < IO
    SetOutletTemperatureLowLimit: Double;

    function Isset(Value: Double): Boolean;

    procedure CopyProgrammed;
    procedure UpdateElectrolyteLimits;
    procedure Reset;
    function InOnVoltageLimit: Boolean;
    function InOffVoltageLimit: Boolean;
    function InCurrentRange: Boolean;
    function InInletPressureRange: Boolean;
    function InInletFlowRange: Boolean;
    function InInletTemperatureRange: Boolean;
    function InOutletPressureRange: Boolean;
    function InOutletFlowRange: Boolean;
    function InOutletTemperatureRange: Boolean;

    procedure ResetRequest;

  end;


  // Used by signature
  TECMActualState = class

    InletPressure: Double;
    InletFlow: Double;
    InletTemperature: Double;

    OutletPressure: Double;
    OutletFlow: Double;
    OutletTemperature: Double;

    LHRCurrent: Double;
    RHRCurrent: Double;

    LHRVoltage: Double;
    RHRVoltage: Double;

    AnodeCurrent: Double;
    AnodeVoltage: Double;

    MainCurrent: Double;
    MainVoltage: Double;

    OutOfDynamicTolerance: Boolean;
  end;

var
  ECMRequest: TECMRequestState;
  ECMActual: TECMActualState;

implementation

{ TECMRequestState }

procedure TECMRequestState.CopyProgrammed;
begin
  SetOnTimeVoltage:= OnTimeVoltage;
  SetOnTimeVoltageHighLimit:= OnTimeVoltageHighLimit;
  SetOnTimeVoltageLowLimit:= OnTimeVoltageLowLimit;
  SetOffTimeVoltage:= OffTimeVoltage;
  SetOffTimeVoltageHighLimit:= OffTimeVoltageHighLimit;
  SetOffTimeVoltageLowLimit:= OffTimeVoltageLowLimit;
  SetCurrent:= Current;
  SetCurrentHighLimit:= CurrentHighLimit;
  SetCurrentLowLimit:= CurrentLowLimit;
end;

procedure TECMRequestState.UpdateElectrolyteLimits;
begin
  SetInletPressure:= InletPressure;
  SetInletPressureHighLimit:= InletPressureHighLimit;
  SetInletPressureLowLimit:= InletPressureLowLimit;
  SetInletFlowHighLimit:= InletFlowHighLimit;
  SetInletFlowLowLimit:= InletFlowLowLimit;
  SetInletTemperatureHighLimit:= InletTemperatureHighLimit;
  SetInletTemperatureLowLimit:= InletTemperatureLowLimit;
  SetOutletPressure:= OutletPressure;
  SetOutletPressureHighLimit:= OutletPressureHighLimit;
  SetOutletPressureLowLimit:= OutletPressureLowLimit;
  SetOutletFlowHighLimit:= OutletFlowHighLimit;
  SetOutletFlowLowLimit:= OutletFlowLowLimit;
  SetOutletTemperatureHighLimit:= OutletTemperatureHighLimit;
  SetOutletTemperatureLowLimit:= OutletTemperatureLowLimit;
end;


function TECMRequestState.InOutletFlowRange: Boolean;
begin
  Result:= True;
  if Isset(SetOutletFlowHighLimit) and Isset(SetOutletFlowLowLimit) then begin
    Result:=(ECMActual.OutletFlow < Self.SetOutletFlowHighLimit) and (ECMActual.OutletFlow > Self.SetOutletFlowLowLimit);
  end;
end;

function TECMRequestState.InOutletPressureRange: Boolean;
begin
  Result:= True;
  if Isset(SetOutletPressureHighLimit) and Isset(SetOutletPressureLowLimit) then begin
    Result:=(ECMActual.OutletPressure < SetOutletPressureHighLimit) and (ECMActual.OutletPressure > SetOutletPressureLowLimit);
  end;
end;

function TECMRequestState.InCurrentRange: Boolean;
begin
  Result:= True;
  if Isset(SetCurrentHighLimit) and Isset(SetCurrentLowLimit) then begin
    Result:=(ECMActual.MainCurrent < Self.SetCurrentHighLimit) and (ECMActual.MainCurrent > Self.SetCurrentLowLimit);
  end;
end;

function TECMRequestState.InInletFlowRange: Boolean;
begin
  Result:= True;
  if Isset(SetInletFlowHighLimit) and Isset(SetInletFlowLowLimit) then begin
    Result:=(ECMActual.InletFlow < SetInletFlowHighLimit) and (ECMActual.InletFlow > SetInletFlowLowLimit);
  end;
end;

function TECMRequestState.InInletPressureRange: Boolean;
begin
  Result:= True;
  if Isset(SetInletPressureHighLimit) and Isset(SetInletPressureLowLimit) then begin
    Result:=(ECMActual.InletPressure < SetInletPressureHighLimit) and (ECMActual.InletPressure > SetInletPressureLowLimit);
  end;
end;

function TECMRequestState.InOutletTemperatureRange: Boolean;
begin
  Result:= True;
  if Isset(SetOutletTemperatureHighLimit) and Isset(SetOutletTemperatureLowLimit) then begin
    Result:=(ECMActual.OutletTemperature < SetOutletTemperatureHighLimit) and (ECMActual.OutletTemperature > SetOutletTemperatureLowLimit);
  end;
end;

function TECMRequestState.InOffVoltageLimit: Boolean;
begin
  Result:= True;
  if DcOnPulsed then begin
    if Isset(SetOffTimeVoltageHighLimit) and Isset(SetOffTimeVoltageLowLimit) then begin
      Result:=(ECMActual.RHRVoltage < SetOffTimeVoltageHighLimit) and (ECMActual.RHRVoltage > SetOffTimeVoltageLowLimit);
    end;
  end;
end;

function TECMRequestState.InInletTemperatureRange: Boolean;
begin
  Result:= True;
  if Isset(SetInletTemperatureHighLimit) and Isset(SetInletTemperatureLowLimit) then begin
    Result:=(ECMActual.InletTemperature < SetInletTemperatureHighLimit) and (ECMActual.InletTemperature > SetInletTemperatureLowLimit);
  end;
end;

function TECMRequestState.InOnVoltageLimit: Boolean;
begin
  Result:= True;
  if Isset(SetOnTimeVoltageHighLimit) and Isset(SetOnTimeVoltageLowLimit) then begin
    Result:=(ECMActual.LHRVoltage < SetOnTimeVoltageHighLimit) and (ECMActual.LHRVoltage > SetOnTimeVoltageLowLimit);
  end;
end;

procedure TECMRequestState.Reset;
begin
  DcOnConstantVoltage:= False;
  DcOnPulsed:= False;
  DcOnConstantCurrent:= False;

  DcSuspendStart:= False;
  DcSuspendEnd:= False;

  OnTime:= 0;
  OffTime:= 0;

  SparkSensitivityLeft:= 0;
  SparkSensitivityRight:= 0;
  ProtectiveAnodeVoltage:= 0;
  dIdTPositiveChange:= 0;
  dIdTNegativeChange:= 0;

  //OnTimeVoltage:= 0;
  OnTimeVoltageHighLimit:= 0;
  OnTimeVoltageLowLimit:= 0;
  OffTimeVoltage:= 0;
  OffTimeVoltageHighLimit:= 0;
  OffTimeVoltageLowLimit:= 0;
  Current:= 0;
  CurrentHighLimit:= 0;
  CurrentLowLimit:= 0;
  Frequency:= 0;
  Period:= 0;
  Duty:= 0;

  InletPressure:= 0;
  InletPressureHighLimit:= 0;
  InletPressureLowLimit:= 0;
  InletFlowHighLimit:= 0;
  InletFlowLowLimit:= 0;
  InletTemperatureHighLimit:= 0;
  InletTemperatureLowLimit:= 0;
  OutletPressure:= 0;
  OutletPressureHighLimit:= 0;
  OutletPressureLowLimit:= 0;
  OutletFlowHighLimit:= 0;
  OutletFlowLowLimit:= 0;
  OutletTemperatureHighLimit:= 0;
  OutletTemperatureLowLimit:= 0;
end;


function TECMRequestState.Isset(Value: Double): Boolean;
begin
  Result:= Abs(Value) > 0.001;
end;

procedure TECMRequestState.ResetRequest;
begin
  DcSuspendStart:= False;
  DcSuspendEnd:= False;
  DcOnConstantVoltage:= False;
  DcOnPulsed:= False;
  DcOnConstantCurrent:= False;
end;

initialization
  ECMRequest:= TECMRequestState.Create;
  ECMActual:= TECMActualState.Create;
end.
