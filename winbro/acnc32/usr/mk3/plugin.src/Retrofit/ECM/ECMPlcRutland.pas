unit ECMPlcRutland;

interface

uses SysUtils, Classes, AbstractPlcPlugin, NamedPlugin, Proto, INamedInterface, PSUCommsRutland, CncTypes, ECMState, PSUCommon;

type
  TRECMProperty = (

    recms_ReqDC_OFF,                    //-----
    recms_ReqDC_ON,                     //
    recms_ReqDC_ON_Pulsed,              // High edge
    recms_ReqDC_ONCurrent,              //-----

    recms_ReqSuspend,                   // Active high

    recms_DCO,                          // High edge + change

    recms_Reset,                        //--------
    recms_UpdateElectrolyteLimits,      // High edge
                                        // -------
    recms_ReqMonitoringOn,              //
    recms_ReqLoggingOn,                 // Active high
                                        // ------
    recms_ReqSignatureUpdate,           // High edge
    recms_ReqSignatureErase,            //
    recms_ReqCapturePosition,           //--------

    // The following declare state (read only)
    recms_OnVoltageInRange,
    recms_OffVoltageInRange,
    recms_CurrentInRange,
    recms_InletPressureInRange,
    recms_InletFlowInRange,
    recms_InletTemperatureInRange,
    recms_OutletPressureInRange,
    recms_OutletFlowInRange,
    recms_OutletTemperatureInRange,

    recms_comms_error,
    recms_failed_port,

    recms_DC_OFF,
    recms_DC_ON_VOLTAGE,
    recms_DC_ON_CURRENT,

    recms_ANODE_V_REQUIRED,
    recms_PULSING_REQUIRED,    // set this flag if pulsing is also required when dc is switched on */

    recms_MonitoringOutOfTolerance,

    recms_STACK_TEMP,
    recms_MAIN_TX_TEMP,
    recms_AUX_TX_TEMP,
    recms_POS_DI1_DT,
    recms_NEG_DI1_DT,
    recms_MOISTURE_DETECTED, //       LHR LEFT HAND RAM */,
    recms_OUTLET_FLOW_FAULT,
    recms_SENSE_LEADS_FAULT,

    recms_CONTACT_SENSED,
    recms_LOSS_OF_TIMING,
    recms_DVDT_IRQ,
    recms_COMMS_FAULT,
    recms_POS_DI2_DT,
    recms_NEG_DI2_DT,
    recms_DV2DT_FAULT,
    recms_DV1DT_FAULT,

    recms_UNDEFINED_1,

    recms_UNDEFINED_2,         // contact sense can only be enabled when dc is off */
    recms_UNDEFINED_3,
    recms_UNDEFINED_4,
    recms_UNDEFINED_5,
    recms_UNDEFINED_6,
    recms_UNDEFINED_7,

    recms_UNDEFINED_8,
    recms_UNDEFINED_9,
    recms_UNDEFINED_10,
    recms_UNDEFINED_11

  );
  TRECMProperties = set of TRECMProperty;


  TECMPlc = class(TAbstractPlcPlugin)
  private
    Comms: TPSUComms;
    Values: TRECMProperties;
    ResolveEv: TPlcPluginResolveItemValueEv;
  public
    constructor Create( Parameters : string;
                        ResolveDeviceEv : TPlcPluginResolveDeviceEv;
                        ResolveItemEv : TPlcPluginResolveItemEv); override;
    procedure Compile ( var Embedded : TEmbeddedData;
                        aReadEv : TPlcPluginReadEv;
                        ResolveItemEv : TPlcPluginResolveItemEv;
                        Write : Boolean);  override;
    destructor Destroy; override;
    procedure GatePre; override;
    function GatePost : Integer; override;
    function Read(var Embedded : TEmbeddedData) : Boolean; override;
    procedure Write(var Embedded : TEmbeddedData; Line, LastLine : Boolean); override;
    function GateName(var Embedded : TEmbeddedData) : string; override;
    function ItemProperties : string; override;
    class function Properties : string; override;
    class procedure FinalInstall; override;
    class procedure Install; override;
  end;

var
  LHRCurrent: TTagRef;
  RHRCurrent: TTagRef;
  LHRVoltage: TTagRef;
  RHRVoltage: TTagRef;
  AnodeCurrent: TTagRef;
  AnodeVoltage: TTagRef;
  MainCurrent: TTagRef;
  MainVoltage: TTagRef;

  CommsLHRCurrent: TTagRef;
  CommsRHRCurrent: TTagRef;
  CommsLHRVoltage: TTagRef;
  CommsRHRVoltage: TTagRef;
  CommsAnodeCurrent: TTagRef;
  CommsAnodeVoltage: TTagRef;
  CommsMainCurrent: TTagRef;
  CommsMainVoltage: TTagRef;

  InletFlow: TTagRef;
  OutletFlow: TTagRef;
  InletPressure: TTagRef;
  OutletPressure: TTagRef;
  InletTemperature: TTagRef;
  OutletTemperature: TTagRef;
  InletPressureDemand: TTagRef;
  OutletPressureDemand: TTagRef;

  OnVoltageDemand: TTagRef;
  OffVoltageDemand: TTagRef;
  OnTimeDemand: TTagRef;
  OffTimeDemand: TTagRef;

  I_Flow_1: TTagRef;
  I_Flow_2: TTagRef;
  I_Pressure_1: TTagRef;
  I_Pressure_2: TTagRef;
  I_Temperature_1: TTagRef;
  I_Temperature_2: TTagRef;

  XAxisPositionTag: TTagRef;
  UAxisPositionTag: TTagRef;


implementation

{ TECMPlc }

uses Forms, IniFiles, SignatureForm;

const
  PropertyName : array [TRECMProperty] of TPlcProperty = (
    ( Name : 'ReqDC_OFF';         ReadOnly: False ),
    ( Name : 'ReqDC_ON';          ReadOnly: False ),
    ( Name : 'ReqDC_ONPulsed';   ReadOnly: False ),
    ( Name : 'ReqDC_ONCurrent';   ReadOnly: False ),
    ( Name : 'ReqDC_Suspend';   ReadOnly: False ),
    ( Name : 'DCO';   ReadOnly: False ),

    ( Name : 'Reset'; ReadOnly: False),
    ( Name : 'UpdateElectrolyteLimits'; ReadOnly: False),

    ( Name : 'ReqMonitoring'; ReadOnly: False ),
    ( Name : 'ReqLogging'; ReadOnly: False ),
    ( Name : 'ReqSignatureUpdate'; ReadOnly: False ),
    ( Name : 'ReqSignatureErase'; ReadOnly: False ),
    ( Name : 'ReqCapturePosition'; ReadOnly: False ),


    ( Name : 'OnVoltageInRange'; ReadOnly: True ),
    ( Name : 'OffVoltageInRange'; ReadOnly: True ),
    ( Name : 'CurrentInRange'; ReadOnly: True ),
    ( Name : 'InletPressureInRange'; ReadOnly: True ),
    ( Name : 'InletFlowInRange'; ReadOnly: True ),
    ( Name : 'InletTemperatureInRange'; ReadOnly: True ),
    ( Name : 'OutletPressureInRange'; ReadOnly: True ),
    ( Name : 'OutletFlowInRange'; ReadOnly: True ),
    ( Name : 'OutletTemperatureInRange'; ReadOnly: True ),

    ( Name : 'CommsError'; ReadOnly: True ),
    ( Name : 'CommsOpenFailure'; ReadOnly: True ),

    ( Name: 'DC_OFF'; ReadOnly: True ),
    ( Name: 'DC_ON_VOLTAGE'; ReadOnly: True ),
    ( Name: 'DC_ON_CURRENT'; ReadOnly: True ),

    ( Name: 'NOT_ANODE_V_REQD'; ReadOnly: True ),
    ( Name: 'PULSING_REQUIRED'; ReadOnly: True ),

    ( Name: 'OutOfDynamicTolerance'; ReadOnly: True ),
    ( Name: 'STACK_TEMP'; ReadOnly: True ),
    ( Name: 'MAIN_TX_TEMP'; ReadOnly: True ),
    ( Name: 'AUX_TX_TEMP'; ReadOnly: True ),
    ( Name: 'POS_DI1_DT'; ReadOnly: True ),
    ( Name: 'NEG_DI1_DT'; ReadOnly: True ),
    ( Name: 'MOISTURE_DETECTED'; ReadOnly: True ),
    ( Name: 'OUTLET_FLOW_FAULT'; ReadOnly: True ),
    ( Name: 'SENSE_LEADS_FAULT'; ReadOnly: True ),
    ( Name: 'CONTACT_SENSED'; ReadOnly: True ),
    ( Name: 'LOSS_OF_TIMING'; ReadOnly: True ),
    ( Name: 'DVDT_IRQ'; ReadOnly: True ),
    ( Name: 'COMMS_FAULT'; ReadOnly: True ),
    ( Name: 'POS_DI2_DT'; ReadOnly: True ),
    ( Name: 'NEG_DI2_DT'; ReadOnly: True ),
    ( Name: 'DV2DT_FAULT'; ReadOnly: True ),
    ( Name: 'DV1DT_FAULT'; ReadOnly: True ),

    ( Name: 'UNDEFINED_1'; ReadOnly: True ),
    ( Name: 'UNDEFINED_2'; ReadOnly: True ),
    ( Name: 'UNDEFINED_3'; ReadOnly: True ),
    ( Name: 'UNDEFINED_4'; ReadOnly: True ),
    ( Name: 'UNDEFINED_5'; ReadOnly: True ),
    ( Name: 'UNDEFINED_6'; ReadOnly: True ),
    ( Name: 'UNDEFINED_7'; ReadOnly: True ),
    ( Name: 'UNDEFINED_8'; ReadOnly: True ),
    ( Name: 'UNDEFINED_9'; ReadOnly: True ),
    ( Name: 'UNDEFINED_10'; ReadOnly: True ),
    ( Name: 'UNDEFINED_11'; ReadOnly: True )
  );


var Plc: TECMPlc;

procedure DisplayProtocol(ATag: TTagRef); stdcall;
begin
  if not Named.GetAsBoolean(aTag)  then begin
    if Assigned(Plc) then begin
      if not Assigned(Protocol) then
        Protocol := TProtocol.Create(Plc.Comms);
      try
        Protocol.ShowModal;
      finally
        Protocol.Free;
        Protocol := nil;
      end;
    end;
  end;
end;


procedure TECMPlc.Write(var Embedded: TEmbeddedData; Line, LastLine: Boolean);
var oldDCO: Integer;
begin
  if Line <> LastLine then begin  // Edge detect rising / falling
    case TRECMProperty(Embedded.Command) of
      recms_ReqDC_OFF: begin
        if Line then begin
          ECMRequest.ResetRequest;
          Comms.SetStateChange;
        end;
      end;
      recms_ReqDC_ON: begin
        if Line then begin
          ECMRequest.CopyProgrammed;
          ECMRequest.ResetRequest;
          ECMRequest.DcOnConstantVoltage:= True;
          Comms.SetStateChange;
        end;
      end;
      recms_ReqDC_ON_Pulsed: begin
        if Line then begin
          ECMRequest.CopyProgrammed;
          ECMRequest.ResetRequest;
          ECMRequest.DcOnPulsed:= True;
          Comms.SetStateChange;
        end;
      end;
      recms_ReqDC_ONCurrent: begin
        if Line then begin
          ECMRequest.CopyProgrammed;
          ECMRequest.ResetRequest;
          ECMRequest.DcOnConstantCurrent:= True;
          Comms.SetStateChange;
        end;
      end;
      recms_ReqSuspend: begin
        if Line then begin
          if ECMRequest.DcOnConstantVoltage or ECMRequest.DcOnPulsed or ECMRequest.DcOnConstantCurrent then begin
            ECMRequest.DcSuspendStart:= True;
            ECMRequest.DcSuspendEnd:= False;
            Comms.SetStateChange;
          end;
        end else begin
          if  ECMRequest.DcSuspendStart then begin
            ECMRequest.DcSuspendStart:= False;
            ECMRequest.DcSuspendEnd:= True;
            Comms.SetStateChange;
          end;
        end;
      end;
      recms_DCO: begin
        oldDCO:= ECMRequest.DCO;
        ECMRequest.DCO:= ResolveEv(Self, Embedded.OData);
        if oldDCO <> ECMRequest.DCO then
          Comms.SetParameterChange;
      end;
      recms_Reset: begin
        if Line then begin
          //ECMRequest.Reset;
          ECMRequest.ResetRequest;
          Comms.SetStateChange;
        end;
      end;
      recms_UpdateElectrolyteLimits: begin
        ECMRequest.UpdateElectrolyteLimits;
      end;

      recms_ReqMonitoringOn: begin
        if Assigned(ECMSignature) then begin
          ECMSignature.SetMonitoring(Line);
        end;
      end;

      recms_ReqLoggingOn: begin
        if Assigned(ECMSignature) then begin
          ECMSignature.StartLogging:= Line;
        end;
      end;

      recms_ReqSignatureUpdate: begin
        if Line then begin
          if Assigned(ECMSignature) then begin
            ECMSignature.StartAddLive:= True;
          end;
        end;
      end;

      recms_ReqSignatureErase: begin
        if Line then begin
          if Assigned(ECMSignature) then begin
            ECMSignature.StartClearSamples:= True;
          end;
        end;
      end;

      recms_ReqCapturePosition: begin
        if Line then begin
          Named.SetAsDouble(XAxisPositionTag, NC.AxisPosition[0][ptDisplay]);
          Named.SetAsDouble(UAxisPositionTag, NC.AxisPosition[1][ptDisplay]);
        end;
      end;

    end;
    if Line then
      Include(Values, TRECMProperty(Embedded.Command))
    else
      Exclude(Values, TRECMProperty(Embedded.Command));
  end;
end;

procedure TECMPlc.Compile(var Embedded: TEmbeddedData; aReadEv: TPlcPluginReadEv; ResolveItemEv: TPlcPluginResolveItemEv;
  Write: Boolean);
var I : TRECMProperty;
    Token : string;
    Readonly: Boolean;
begin
  Token := aReadEv(Self);
  if Token <> '.' then
    raise Exception.Create('Period [.] expected');

  Token := aReadEv(Self);
  for I := Low(I) to High(I) do begin
    if CompareText(PropertyName[I].Name, Token) = 0 then begin
      Embedded.Command := Ord(I);
      if I = recms_DCO then begin
        Token := aReadEv(Self);
        if Token <> '.' then
          raise Exception.Create('Period [.] expected after DCO');
        Token:= aReadEv(Self);

        Embedded.OData:= ResolveItemEv(Self, PChar(Token), ReadOnly, ResolveEv);

      end;
      Exit;
    end;
  end;
  raise Exception.CreateFmt('Unknown property [%s]', [Token]);
end;

function TECMPlc.GateName(var Embedded: TEmbeddedData): string;
begin
  Result := PropertyName[TRECMProperty(Embedded.Command)].Name;
end;

constructor TECMPlc.Create(Parameters: string; ResolveDeviceEv: TPlcPluginResolveDeviceEv; ResolveItemEv: TPlcPluginResolveItemEv);
var Token, Port: string;
begin
  Plc:= Self;
  Token := ReadDelimited(Parameters, ';');
  Port := 'COM1';
  if Token <> '' then begin
    Port:= Token;
  end;
  Comms:= TPSUComms.Create(Port);
  Comms.Resume;
end;

function TECMPlc.ItemProperties: string;
var I : TRECMProperty;
begin
  Result:= '';
  for I := Low(I) to High(I) do
    Result := Result + PropertyName[I].Name + CarRet + BooleanIdent[I in Values] + CarRet;
end;

class procedure TECMPlc.FinalInstall;
begin
  Named.AquireTag('PSUProtocol', 'TMethodTag', DisplayProtocol);

  I_Flow_1:= Named.AquireTag('I_Flow_1', 'TIntegerTag', nil);
  I_Flow_2:= Named.AquireTag('I_Flow_2', 'TIntegerTag', nil);
  I_Pressure_1:= Named.AquireTag('I_Pressure_1', 'TIntegerTag', nil);
  I_Pressure_2:= Named.AquireTag('I_Pressure_2', 'TIntegerTag', nil);
  I_Temperature_1:= Named.AquireTag('I_Temperature_1', 'TIntegerTag', nil);
  I_Temperature_2:= Named.AquireTag('I_Temperature_2', 'TIntegerTag', nil);
end;

class procedure TECMPlc.Install;
var Ini: TIniFile;
begin
  Ini := TIniFile.Create(DllProfilePath + 'ecm.cfg');

  try
    MainsFrequency:= Ini.ReadFloat('ECM', 'MainsFrequency', 60);
    RamCurrentTransducer:= Ini.ReadFloat('ECM', 'RamCurrentTransducer', 10000);
    MainCurrentTransducer:= Ini.ReadFloat('ECM', 'MainCurrentTransducer', 20000);
    MainVoltageTransducer:= Ini.ReadFloat('ECM', 'MainVoltageTransducer', 40);
    AnodeCurrentTransducer:= Ini.ReadFloat('ECM', 'AnodeCurrentTransducer', 1000);
    AnodeVoltageTransducer:= Ini.ReadFloat('ECM', 'AnodeVoltageTransducer', 12);

    FlowTransducer:= Ini.ReadFloat('ECM', 'FlowTransducer', 500);
    PressureTransducer:= Ini.ReadFloat('ECM', 'PressureTransducer', 500);
    TemperatureTransducer:= Ini.ReadFloat('ECM', 'TemperatureTransducer', 300);
    TemperatureIntersect:= Ini.ReadFloat('ECM', 'TemperatureIntersect', 32);

    MaximumRampTime:= Ini.ReadFloat('ECM', 'MaximumRampTime', 75);
  finally
    Ini.Free;
  end;

  LHRCurrent:= Named.AddTag('ECMLHRCurrent', 'TDoubleTag');
  RHRCurrent:= Named.AddTag('ECMRHRCurrent', 'TDoubleTag');
  LHRVoltage:= Named.AddTag('ECMLHRVoltage', 'TDoubleTag');
  RHRVoltage:= Named.AddTag('ECMRHRVoltage', 'TDoubleTag');
  AnodeCurrent:= Named.AddTag('ECMAnodeCurrent', 'TDoubleTag');
  AnodeVoltage:= Named.AddTag('ECMAnodeVoltage', 'TDoubleTag');
  MainCurrent:= Named.AddTag('ECMMainCurrent', 'TDoubleTag');
  MainVoltage:= Named.AddTag('ECMMainVoltage', 'TDoubleTag');

  CommsLHRCurrent:= Named.AddTag('ECMCommsLHRCurrent', 'TIntegerTag');
  CommsRHRCurrent:= Named.AddTag('ECMCommsRHRCurrent', 'TIntegerTag');
  CommsLHRVoltage:= Named.AddTag('ECMCommsLHRVoltage', 'TIntegerTag');
  CommsRHRVoltage:= Named.AddTag('ECMCommsRHRVoltage', 'TIntegerTag');
  CommsAnodeCurrent:= Named.AddTag('ECMCommsAnodeCurrent', 'TIntegerTag');
  CommsAnodeVoltage:= Named.AddTag('ECMCommsAnodeVoltage', 'TIntegerTag');
  CommsMainCurrent:= Named.AddTag('ECMCommsMainCurrent', 'TIntegerTag');
  CommsMainVoltage:= Named.AddTag('ECMCommsMainVoltage', 'TIntegerTag');

  XAxisPositionTag:= Named.AddTag('SparkDetectXAxisPosition', 'TDoubleTag');
  UAxisPositionTag:= Named.AddTag('SparkDetectUAxisPosition', 'TDoubleTag');


  InletFlow:= Named.AddTag('ECMInletFlow', 'TDoubleTag');
  OutletFlow:= Named.AddTag('ECMOutletFlow', 'TDoubleTag');
  InletPressure:= Named.AddTag('ECMInletPressure', 'TDoubleTag');
  OutletPressure:= Named.AddTag('ECMOutletPressure', 'TDoubleTag');
  InletTemperature:= Named.AddTag('ECMInletTemperature', 'TDoubleTag');
  OutletTemperature:= Named.AddTag('ECMOutletTemperature', 'TDoubleTag');
  InletPressureDemand:= Named.AddTag('ECMInletPressureDemand', 'TDoubleTag');
  OutletPressureDemand:= Named.AddTag('ECMOutletPressureDemand', 'TDoubleTag');

  OnVoltageDemand:= Named.AddTag('ECMOnVoltageDemand', 'TDoubleTag');
  OffVoltageDemand:= Named.AddTag('ECMOffVoltageDemand', 'TDoubleTag');
  OnTimeDemand:= Named.AddTag('ECMOnTimeDemand', 'TDoubleTag');
  OffTimeDemand:= Named.AddTag('ECMOffTimeDemand', 'TDoubleTag');
end;

function TECMPlc.GatePost: Integer;
begin
  Result:= 0;
end;

class function TECMPlc.Properties: string;
var I : TRECMProperty;
begin
  Result := '';
  for I := Low(I) to High(I) do
    Result := Result + PropertyName[I].Name + CarRet;
end;

function TECMPlc.Read(var Embedded: TEmbeddedData): Boolean;
begin
  Result := TRECMProperty(Embedded.Command) in Values;
end;

destructor TECMPlc.Destroy;
begin
  if Protocol <> nil then
    Protocol.Free;
  Comms.Free;
  Plc:= nil;
  inherited;
end;

procedure TECMPlc.GatePre;
begin
  if Comms.NewData then begin

    if recms_DC_OFF in Values then begin
      Comms.InputData.lhr_voltage:= 0;
      Comms.InputData.rhr_voltage:= 0;
    end;
    ECMActual.LHRCurrent:= Comms.InputData.lhr_current * (RamCurrentTransducer / 255.0);
    ECMActual.RHRCurrent:= Comms.InputData.rhr_current * (RamCurrentTransducer / 255.0);
    ECMActual.LHRVoltage:= Comms.InputData.lhr_voltage * (MainVoltageTransducer / 255.0);
    ECMActual.RHRVoltage:= Comms.InputData.rhr_voltage * (MainVoltageTransducer / 255.0);
    ECMActual.AnodeCurrent:= Comms.InputData.anode_current * (AnodeCurrentTransducer / 255.0);
    ECMActual.AnodeVoltage:= Comms.InputData.anode_voltage * (AnodeVoltageTransducer / 255.0);
    ECMActual.MainCurrent:= ECMActual.LHRCurrent + ECMActual.RHRCurrent;
    ECMActual.MainVoltage:= Comms.InputData.main_voltage * (MainVoltageTransducer / 255.0);

    Named.SetAsDouble(LHRCurrent, ECMActual.LHRCurrent);
    Named.SetAsDouble(RHRCurrent, ECMActual.RHRCurrent);
    Named.SetAsDouble(LHRVoltage, ECMActual.LHRVoltage);
    Named.SetAsDouble(RHRVoltage,  ECMActual.RHRVoltage);
    Named.SetAsDouble(AnodeCurrent, ECMActual.AnodeCurrent);
    Named.SetAsDouble(AnodeVoltage, ECMActual.AnodeVoltage);
    Named.SetAsDouble(MainCurrent, ECMActual.MainCurrent);
    Named.SetAsDouble(MainVoltage, ECMActual.MainVoltage);

    Named.SetAsInteger(CommsLHRCurrent, Comms.InputData.lhr_current);
    Named.SetAsInteger(CommsRHRCurrent, Comms.InputData.rhr_current);
    Named.SetAsInteger(CommsLHRVoltage, Comms.InputData.lhr_voltage);
    Named.SetAsInteger(CommsRHRVoltage,  Comms.InputData.rhr_voltage);
    Named.SetAsInteger(CommsAnodeCurrent, Comms.InputData.anode_current);
    Named.SetAsInteger(CommsAnodeVoltage, Comms.InputData.anode_voltage);
    Named.SetAsInteger(CommsMainCurrent, Comms.InputData.lhr_current + Comms.InputData.rhr_current);
    Named.SetAsInteger(CommsMainVoltage, Comms.InputData.main_voltage);

    Named.SetAsDouble(InletFlow, ECMActual.InletFlow);
    Named.SetAsDouble(OutletFlow, ECMActual.OutletFlow);
    Named.SetAsDouble(InletPressure, ECMActual.InletPressure);
    Named.SetAsDouble(OutletPressure, ECMActual.OutletPressure);
    Named.SetAsDouble(InletTemperature, ECMActual.InletTemperature);
    Named.SetAsDouble(OutletTemperature, ECMActual.OutletTemperature);
    Named.SetAsDouble(InletPressureDemand, ECMRequest.SetInletPressure);
    Named.SetAsDouble(OutletPressureDemand, ECMRequest.SetOutletPressure);

    Named.SetAsDouble(OnVoltageDemand, ECMRequest.SetOnTimeVoltage);
    Named.SetAsDouble(OffVoltageDemand, ECMRequest.SetOffTimeVoltage);

    //???? Should these be SetOnTime a field that doesn't currently exist!!!!
    Named.SetAsDouble(OnTimeDemand, ECMRequest.OnTime);
    Named.SetAsDouble(OffTimeDemand, ECMRequest.OffTime);

    Comms.NewData:= False;
  end;

  ECMActual.InletPressure:= Named.GetAsDouble(I_PRESSURE_1) * (PressureTransducer / 32768.0);
  ECMActual.InletFlow:= Named.GetAsDouble(I_FLOW_1) * (FlowTransducer / 32768.0);
  ECMActual.InletTemperature:= Named.GetAsDouble(I_TEMPERATURE_1) * (TemperatureTransducer / 32768.0) + TemperatureIntersect;
  ECMActual.OutletPressure:= Named.GetAsDouble(I_PRESSURE_2) * (PressureTransducer / 32768.0);
  ECMActual.OutletFlow:= Named.GetAsDouble(I_FLOW_1) * (FlowTransducer / 32768.0);
  ECMActual.OutletTemperature:= Named.GetAsDouble(I_TEMPERATURE_2) * (TemperatureTransducer / 32768.0) + TemperatureIntersect;

  if ECMState.ECMActual.OutOfDynamicTolerance then
    Include(Values, recms_MonitoringOutOfTolerance)
  else
    Exclude(Values, recms_MonitoringOutOfTolerance);

  if ECMRequest.InOnVoltageLimit then
    Include(Values, recms_OnVoltageInRange)
  else
    Exclude(Values, recms_OnVoltageInRange);

  if ECMRequest.InOffVoltageLimit then
    Include(Values, recms_OffVoltageInRange)
  else
    Exclude(Values, recms_OffVoltageInRange);

  if ECMRequest.InCurrentRange then
    Include(Values, recms_CurrentInRange)
  else
    Exclude(Values, recms_CurrentInRange);

  if ECMRequest.InInletPressureRange then
    Include(Values, recms_InletPressureInRange)
  else
    Exclude(Values, recms_InletPressureInRange);

  if ECMRequest.InInletFlowRange then
    Include(Values, recms_InletFlowInRange)
  else
    Include(Values, recms_InletFlowInRange);


  if ECMRequest.InInletTemperatureRange then
    Include(Values, recms_InletTemperatureInRange)
  else
    Exclude(Values, recms_InletTemperatureInRange);

  if ECMRequest.InOutletPressureRange then
    Include(Values, recms_OutletPressureInRange)
  else
    Exclude(Values, recms_OutletPressureInRange);

  if ECMRequest.InOutletFlowRange then
    Include(Values, recms_OutletFlowInRange)
  else
    Exclude(Values, recms_OutletFlowInRange);

  if ECMRequest.InOutletTemperatureRange then
    Include(Values, recms_OutletTemperatureInRange)
  else
    Exclude(Values, recms_OutletTemperatureInRange);

  if Comms.Dead then
    Include(Values, recms_failed_port);

  if Comms.CommsError then
    Include(Values, recms_comms_error)
  else
    Exclude(Values, recms_comms_error);

  if (  status1_DC_OFF in Comms.InputData.Status1) or not
     ( (status1_DC_ON_VOLTAGE in Comms.InputData.Status1) or
       (status1_DC_ON_CURRENT in Comms.InputData.Status1) ) then begin
    Include(Values, recms_DC_OFF);
    ECMActual.OutOfDynamicTolerance:= False;
  end
  else
    Exclude(Values, recms_DC_OFF);

  if status1_DC_ON_VOLTAGE in Comms.InputData.Status1 then
    Include(Values, recms_DC_ON_VOLTAGE)
  else
    Exclude(Values, recms_DC_ON_VOLTAGE);

  if status1_DC_ON_CURRENT in Comms.InputData.Status1 then
    Include(Values, recms_DC_ON_CURRENT)
  else
    Exclude(Values, recms_DC_ON_CURRENT);

  if status1_ANODE_V_REQUIRED in Comms.InputData.Status1 then
    Include(Values, recms_ANODE_V_REQUIRED)
  else
    Exclude(Values, recms_ANODE_V_REQUIRED);

  if status1_PULSING_REQUIRED in Comms.InputData.Status1 then
    Include(Values, recms_PULSING_REQUIRED)
  else
    Exclude(Values, recms_PULSING_REQUIRED);


  if rstatus2_STACK_TEMP in Comms.InputData.Status2 then
    Include(Values, recms_STACK_TEMP)
  else
    Exclude(Values, recms_STACK_TEMP);

  if rstatus2_MAIN_TX_TEMP in Comms.InputData.Status2 then
    Include(Values, recms_MAIN_TX_TEMP)
  else
    Exclude(Values, recms_MAIN_TX_TEMP);

  if rstatus2_AUX_TX_TEMP in Comms.InputData.Status2 then
    Include(Values, recms_AUX_TX_TEMP)
  else
    Exclude(Values, recms_AUX_TX_TEMP);

  if rstatus2_POS_DI1_DT in Comms.InputData.Status2 then
    Include(Values, recms_POS_DI1_DT)
  else
    Exclude(Values, recms_POS_DI1_DT);

  if rstatus2_NEG_DI1_DT in Comms.InputData.Status2 then
    Include(Values, recms_NEG_DI1_DT)
  else
    Exclude(Values, recms_NEG_DI1_DT);

  if rstatus2_MOISTURE_DETECTED in Comms.InputData.Status2 then
    Include(Values, recms_MOISTURE_DETECTED)
  else
    Exclude(Values, recms_MOISTURE_DETECTED);

  if rstatus2_OUTLET_FLOW_FAULT in Comms.InputData.Status2 then
    Include(Values, recms_OUTLET_FLOW_FAULT)
  else
    Exclude(Values, recms_OUTLET_FLOW_FAULT);

  if rstatus2_SENSE_LEADS_FAULT in Comms.InputData.Status2 then
    Include(Values, recms_SENSE_LEADS_FAULT)
  else
    Exclude(Values, recms_SENSE_LEADS_FAULT);

  if rstatus2_CONTACT_SENSED in Comms.InputData.Status2 then
    Include(Values, recms_CONTACT_SENSED)
  else
    Exclude(Values, recms_CONTACT_SENSED);

  if rstatus2_LOSS_OF_TIMING in Comms.InputData.Status2 then
    Include(Values, recms_LOSS_OF_TIMING)
  else
    Exclude(Values, recms_LOSS_OF_TIMING);

  if rstatus2_DVDT_IRQ in Comms.InputData.Status2 then
    Include(Values, recms_DVDT_IRQ)
  else
    Exclude(Values, recms_DVDT_IRQ);

  if rstatus2_COMMS_FAULT in Comms.InputData.Status2 then
    Include(Values, recms_COMMS_FAULT)
  else
    Exclude(Values, recms_COMMS_FAULT);

  if rstatus2_POS_DI2_DT in Comms.InputData.Status2 then
    Include(Values, recms_POS_DI2_DT)
  else
    Exclude(Values, recms_POS_DI2_DT);

  if rstatus2_NEG_DI2_DT in Comms.InputData.Status2 then
    Include(Values, recms_NEG_DI2_DT)
  else
    Exclude(Values, recms_NEG_DI2_DT);

  if rstatus2_DV2DT_FAULT in Comms.InputData.Status2 then
    Include(Values, recms_DV2DT_FAULT)
  else
    Exclude(Values, recms_DV2DT_FAULT);

  if rstatus2_DV1DT_FAULT in Comms.InputData.Status2 then
    Include(Values, recms_DV1DT_FAULT)
  else
    Exclude(Values, recms_DV1DT_FAULT);


end;

initialization
  TECMPlc.AddPlugin('ECMControlRutland', TECMPlc);
end.
