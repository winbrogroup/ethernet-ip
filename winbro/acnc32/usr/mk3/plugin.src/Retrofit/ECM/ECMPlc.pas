unit ECMPlc;

interface

uses SysUtils, AbstractPlcPlugin, NamedPlugin, Proto, INamedInterface, PSUComms, CncTypes, ECMState, PSUCommon;

type
  TECMProperty = (
    ecms_status1_DC_OFF,
    ecms_status1_DC_ON_VOLTAGE,
    ecms_status1_DC_ON_CURRENT,
    //status1_ANODE_V_REQUIRED,    // set this flag if anode supply is also required when dc is switched on */
    ecms_status1_ANODE_V_REQUIRED,
    ecms_status1_PULSING_REQUIRED,    // set this flag if pulsing is also required when dc is switched on */

    ecms_status2_POS_WP1_DIDT_FAULT,
    ecms_status2_POS_WP2_DIDT_FAULT,
    ecms_status2_POS_WP3_DIDT_FAULT,
    ecms_status2_NEG_WP1_DIDT_FAULT,
    ecms_status2_NEG_WP2_DIDT_FAULT,
    ecms_status2_NEG_WP3_DIDT_FAULT,

    //  PSU_pod_status2 , 6809 ref FS 3 */

    ecms_status2_ANODE_STACK_FAULT,
    ecms_status2_MOISTURE_DETECTED,
    ecms_status2_OUTLET_FLOW_FAULT,

    ecms_status3_CONTACT_SENSED,
    ecms_status3_SENSE_LEADS_FAULT,
    ecms_status3_STACK_FAULT,
    ecms_status3_MAIN_TX_TEMP,
    ecms_status3_AUX_TX_TEMP,
    ecms_status3_BUSBAR_TEMP,
    ecms_status3_DV2DT_FAULT,
    ecms_status3_DV1DT_FAULT,

    ecms_status3_DC_INVALID_REQUEST,
    ecms_status3_LOSS_OF_TIMING,
    ecms_status3_DVDT_IRQ,
    ecms_status3_COMMS_FAULT,
    ecms_status3_FAST_MAIN_OVER_CURRENT,
    ecms_status3_FAST_ANODE_OVER_CURRENT,
    ecms_status4_STACK_TEMP_WARNING,
    ecms_status4_MAIN_TX_TEMP_WARNING,
    ecms_status4_AUX_TX_TEMP_WARNING,
    ecms_status4_ANODE_STACK_TEMP_WARNING,

    ecms_comms_error,

    ecms_ReqDC_OFF,
    ecms_ReqDC_ON,
    ecms_ReqDC_ON_Pulsed,
    ecms_ReqDC_ONCurrent,
    ecms_ReqMonitoringOn,
    ecms_ReqLoggingOn
  );
  TECMProperties = set of TECMProperty;


  TECMPlc = class(TAbstractPlcPlugin)
  private
    Comms: TPSUComms;
    Values: TECMProperties;
  public
    constructor Create( Parameters : string;
                        ResolveDeviceEv : TPlcPluginResolveDeviceEv;
                        ResolveItemEv : TPlcPluginResolveItemEv); override;
    procedure Compile ( var Embedded : TEmbeddedData;
                        aReadEv : TPlcPluginReadEv;
                        ResolveItemEv : TPlcPluginResolveItemEv;
                        Write : Boolean);  override;
    destructor Destroy; override;
    procedure GatePre; override;
    function GatePost : Integer; override;
    function Read(var Embedded : TEmbeddedData) : Boolean; override;
    procedure Write(var Embedded : TEmbeddedData; Line, LastLine : Boolean); override;
    function GateName(var Embedded : TEmbeddedData) : string; override;
    function ItemProperties : string; override;
    class function Properties : string; override;
    class procedure FinalInstall; override;
    class procedure Install; override;
  end;

var
  WP1Current: TTagRef;
  WP2Current: TTagRef;
  WP3Current: TTagRef;
  OnVoltage: TTagRef;
  OffVoltage: TTagRef;
  AnodeCurrent: TTagRef;
  AnodeVoltage: TTagRef;
  MainVoltage: TTagRef;

  I_Flow_1: TTagRef;
  I_Flow_2: TTagRef;
  I_Pressure_1: TTagRef;
  I_Pressure_2: TTagRef;
  I_Temperature_1: TTagRef;
  I_Temperature_2: TTagRef;

implementation

{ TECMPlc }

uses Forms;

const
  PropertyName : array [TECMProperty] of TPlcProperty = (
    ( Name : 'DC_OFF'; ReadOnly: True ),
    ( Name : 'DC_ON_VOLTAGE'; ReadOnly: True ),
    ( Name : 'DC_ON_CURRENT'; ReadOnly: True ),
    ( Name : 'NOT_ANODE_V_REQD'; ReadOnly: True ),
    ( Name : 'PULSING_REQUIRED'; ReadOnly: True ),
    ( Name : 'POS_WP1_DIDT_FAULT'; ReadOnly: True ),
    ( Name : 'POS_WP2_DIDT_FAULT'; ReadOnly: True ),
    ( Name : 'POS_WP3_DIDT_FAULT'; ReadOnly: True ),
    ( Name : 'NEG_WP1_DIDT_FAULT'; ReadOnly: True ),
    ( Name : 'NEG_WP2_DIDT_FAULT'; ReadOnly: True ),
    ( Name : 'NEG_WP3_DIDT_FAULT'; ReadOnly: True ),
    ( Name : 'ANODE_STACK_FAULT'; ReadOnly: True ),
    ( Name : 'MOISTURE_DETECTED'; ReadOnly: True ),
    ( Name : 'OUTLET_FLOW_FAULT'; ReadOnly: True ),
    ( Name : 'CONTACT_SENSED'; ReadOnly: True ),
    ( Name : 'SENSE_LEADS_FAULT'; ReadOnly: True ),
    ( Name : 'STACK_FAULT'; ReadOnly: True ),
    ( Name : 'MAIN_TX_TEMP'; ReadOnly: True ),
    ( Name : 'AUX_TX_TEMP'; ReadOnly: True ),
    ( Name : 'BUSBAR_TEMP'; ReadOnly: True ),
    ( Name : 'DV2DT_FAULT'; ReadOnly: True ),
    ( Name : 'DV1DT_FAULT'; ReadOnly: True ),
    ( Name : 'DC_INVALID_REQUEST'; ReadOnly: True ),
    ( Name : 'LOSS_OF_TIMING'; ReadOnly: True ),
    ( Name : 'DVDT_IRQ'; ReadOnly: True ),
    ( Name : 'COMMS_FAULT'; ReadOnly: True ),
    ( Name : 'FAST_MAIN_OVER_CURRENT'; ReadOnly: True ),
    ( Name : 'FAST_ANODE_OVER_CURRENT'; ReadOnly: True ),
    ( Name : 'STACK_TEMP_WARNING'; ReadOnly: True ),
    ( Name : 'MAIN_TX_TEMP_WARNING'; ReadOnly: True ),
    ( Name : 'AUX_TX_TEMP_WARNING'; ReadOnly: True ),
    ( Name : 'ANODE_STACK_TEMP_WARNIN'; ReadOnly: True ),
    ( Name : 'CommsError'; ReadOnly: True ),

    ( Name : 'ReqDC_OFF';         ReadOnly: False ),
    ( Name : 'ReqDC_ON';          ReadOnly: False ),
    ( Name : 'ReqDC_ON_Pulsed';   ReadOnly: False ),
    ( Name : 'ReqDC_ONCurrent';   ReadOnly: False ),
    ( Name : 'ReqMonitoringOn';   ReadOnly: False ),
    ( Name : 'ReqLoggingOn';      ReadOnly: False )
  );


var Plc: TECMPlc;

procedure DisplayProtocol(ATag: TTagRef); stdcall;
begin
  if not Named.GetAsBoolean(aTag)  then begin
    if Assigned(Plc) then begin
      if not Assigned(Protocol) then
        Protocol := TProtocol.Create(Plc.Comms);
      try
        Protocol.ShowModal;
      finally
        Protocol.Free;
        Protocol := nil;
      end;
    end;
  end;
end;


procedure TECMPlc.Write(var Embedded: TEmbeddedData; Line, LastLine: Boolean);
begin
  if Line <> LastLine then begin  // Edge detect rising / falling
    if Line then begin
      case TECMProperty(Embedded.Command) of
        ecms_ReqDC_OFF: begin
          ECMRequest.DcOnConstantVoltage:= False;
          ECMRequest.DcOnPulsed:= False;
          ECMRequest.DcOnConstantCurrent:= False;
          Comms.SetStateChange;
        end;
        ecms_ReqDC_ON: begin
          ECMRequest.DcOnConstantVoltage:= Line;
          Comms.SetStateChange;
        end;
        ecms_ReqDC_ON_Pulsed: begin
          ECMRequest.DcOnPulsed:= Line;
          Comms.SetStateChange;
        end;
        ecms_ReqDC_ONCurrent: begin
          ECMRequest.DcOnConstantCurrent:= Line;
          Comms.SetStateChange;
        end;
        ecms_ReqMonitoringOn: begin
        end;
        ecms_ReqLoggingOn: begin
        end;
      end;
      Include(Values, TECMProperty(Embedded.Command))
    end else
      Exclude(Values, TECMProperty(Embedded.Command));
  end;
end;

procedure TECMPlc.Compile(var Embedded: TEmbeddedData; aReadEv: TPlcPluginReadEv; ResolveItemEv: TPlcPluginResolveItemEv;
  Write: Boolean);
var I : TECMProperty;
    Token : string;
begin
  Token := aReadEv(Self);
  if Token <> '.' then
    raise Exception.Create('Period [.] expected');

  Token := aReadEv(Self);
  for I := Low(I) to High(I) do begin
    if CompareText(PropertyName[I].Name, Token) = 0 then begin
      Embedded.Command := Ord(I);
      Exit;
    end;
  end;
  raise Exception.CreateFmt('Unknown property [%s]', [Token]);
end;

function TECMPlc.GateName(var Embedded: TEmbeddedData): string;
begin
  Result := PropertyName[TECMProperty(Embedded.Command)].Name;
end;

constructor TECMPlc.Create(Parameters: string; ResolveDeviceEv: TPlcPluginResolveDeviceEv; ResolveItemEv: TPlcPluginResolveItemEv);
var Token, Port: string;
begin
  Plc:= Self;
  Token := ReadDelimited(Parameters, ';');
  Port := 'COM1';
  if Token <> '' then begin
    Port:= Token;
  end;
  Comms:= TPSUComms.Create(Port);
  Comms.Resume;
end;

function TECMPlc.ItemProperties: string;
var I : TECMProperty;
begin
  Result:= '';
  for I := Low(I) to High(I) do
    Result := Result + PropertyName[I].Name + CarRet + BooleanIdent[I in Values] + CarRet;
end;

class procedure TECMPlc.FinalInstall;
begin
  Named.AquireTag('PSUProtocol', 'TMethodTag', DisplayProtocol);

  I_Flow_1:= Named.AquireTag('I_Flow_1', 'TIntegerTag', nil);
  I_Flow_2:= Named.AquireTag('I_Flow_2', 'TIntegerTag', nil);
  I_Pressure_1:= Named.AquireTag('I_Pressure_1', 'TIntegerTag', nil);
  I_Pressure_2:= Named.AquireTag('I_Pressure_2', 'TIntegerTag', nil);
  I_Temperature_1:= Named.AquireTag('I_Temperature_1', 'TIntegerTag', nil);
  I_Temperature_2:= Named.AquireTag('I_Temperature_2', 'TIntegerTag', nil);
end;

class procedure TECMPlc.Install;
begin
  WP1Current:= Named.AddTag('ECMRawWP1Current', 'TDoubleTag');
  WP2Current:= Named.AddTag('ECMRawWP2Current', 'TDoubleTag');
  WP3Current:= Named.AddTag('ECMRawWP3Current', 'TDoubleTag');
  OnVoltage:= Named.AddTag('ECMRawOnVoltate', 'TDoubleTag');
  OffVoltage:= Named.AddTag('ECMRawOffVoltage', 'TDoubleTag');
  AnodeCurrent:= Named.AddTag('ECMRawAnodeCurrent', 'TDoubleTag');
  AnodeVoltage:= Named.AddTag('ECMRawAnodeVoltage', 'TDoubleTag');
  MainVoltage:= Named.AddTag('ECMRawMainVoltage', 'TDoubleTag');
end;

function TECMPlc.GatePost: Integer;
begin
  Result:= 0;
end;

class function TECMPlc.Properties: string;
var I : TECMProperty;
begin
  Result := '';
  for I := Low(I) to High(I) do
    Result := Result + PropertyName[I].Name + CarRet;
end;

function TECMPlc.Read(var Embedded: TEmbeddedData): Boolean;
begin
  Result := TECMProperty(Embedded.Command) in Values;
end;

destructor TECMPlc.Destroy;
begin
  if Protocol <> nil then
    Protocol.Free;
  Comms.Free;
  Plc:= nil;
  inherited;
end;

procedure TECMPlc.GatePre;
begin
  Named.SetAsDouble( WP1Current, Comms.InputData.wp1_current );
  Named.SetAsDouble( WP2Current, Comms.InputData.wp2_current );
  Named.SetAsDouble( WP3Current, Comms.InputData.wp3_current );
  Named.SetAsDouble( OnVoltage, Comms.InputData.on_voltage );
  Named.SetAsDouble( OffVoltage, Comms.InputData.off_voltage );
  Named.SetAsDouble( AnodeCurrent, Comms.InputData.anode_current );
  Named.SetAsDouble( AnodeVoltage, Comms.InputData.anode_voltage );
  Named.SetAsDouble( MainVoltage, Comms.InputData.main_voltage );

  ECMActual.OnTimeVoltage:= Comms.InputData.on_voltage;
  ECMActual.OffTimeVoltage:= Comms.InputData.off_voltage;

  ECMActual.InletPressure:= 0;
  ECMActual.InletFlow:= 0;
  ECMActual.InletTemperature:= 0;
  ECMActual.OutletPressure:= 0;
  ECMActual.OutletFlow:= 0;
  ECMActual.OutletTemperature:= 0;
  ECMActual.WP1Current:= Comms.InputData.wp1_current;
  ECMActual.WP2Current:= Comms.InputData.wp1_current;
  ECMActual.WP3Current:= Comms.InputData.wp1_current;
  ECMActual.AnodeCurrent:= Comms.InputData.anode_current;
  ECMActual.AnodeVoltage:= Comms.InputData.anode_voltage;
  ECMActual.MainVoltage:= Comms.InputData.main_voltage;

  ECMActual.DcOnConstantVoltage:= status1_DC_ON_VOLTAGE in Comms.InputData.Status1;
  ECMActual.DcOnPulsed:= status1_PULSING_REQUIRED in Comms.InputData.Status1;
  ECMActual.DcOnConstantCurrent:= status1_DC_ON_CURRENT in Comms.InputData.Status1;

  if Comms.CommsError then
    Include(Values, ecms_comms_error)
  else
    Exclude(Values, ecms_comms_error);

  if status1_DC_OFF in Comms.InputData.Status1 then
    Include(Values, ecms_status1_DC_OFF)
  else
    Exclude(Values, ecms_status1_DC_OFF);

  if status1_DC_ON_VOLTAGE in Comms.InputData.Status1 then
    Include(Values, ecms_status1_DC_ON_VOLTAGE)
  else
    Exclude(Values, ecms_status1_DC_ON_VOLTAGE);

  if status1_DC_ON_CURRENT in Comms.InputData.Status1 then
    Include(Values, ecms_status1_DC_ON_CURRENT)
  else
    Exclude(Values, ecms_status1_DC_ON_CURRENT);

  if status1_ANODE_V_REQUIRED in Comms.InputData.Status1 then
    Include(Values, ecms_status1_ANODE_V_REQUIRED)
  else
    Exclude(Values, ecms_status1_ANODE_V_REQUIRED);

  if status1_PULSING_REQUIRED in Comms.InputData.Status1 then
    Include(Values, ecms_status1_PULSING_REQUIRED)
  else
    Exclude(Values, ecms_status1_PULSING_REQUIRED);

  if status2_POS_WP1_DIDT_FAULT in Comms.InputData.Status2 then
    Include(Values, ecms_status2_POS_WP1_DIDT_FAULT)
  else
    Exclude(Values, ecms_status2_POS_WP1_DIDT_FAULT);

  if status2_POS_WP2_DIDT_FAULT in Comms.InputData.Status2 then
    Include(Values, ecms_status2_POS_WP2_DIDT_FAULT)
  else
    Exclude(Values, ecms_status2_POS_WP2_DIDT_FAULT);

  if status2_POS_WP3_DIDT_FAULT in Comms.InputData.Status2 then
    Include(Values, ecms_status2_POS_WP3_DIDT_FAULT)
  else
    Exclude(Values, ecms_status2_POS_WP3_DIDT_FAULT);

  if status2_NEG_WP1_DIDT_FAULT in Comms.InputData.Status2 then
    Include(Values, ecms_status2_NEG_WP1_DIDT_FAULT)
  else
    Exclude(Values, ecms_status2_NEG_WP1_DIDT_FAULT);

  if status2_NEG_WP2_DIDT_FAULT in Comms.InputData.Status2 then
    Include(Values, ecms_status2_NEG_WP2_DIDT_FAULT)
  else
    Exclude(Values, ecms_status2_NEG_WP2_DIDT_FAULT);

  if status2_NEG_WP3_DIDT_FAULT in Comms.InputData.Status2 then
    Include(Values, ecms_status2_NEG_WP3_DIDT_FAULT)
  else
    Exclude(Values, ecms_status2_NEG_WP3_DIDT_FAULT);

  if status2_ANODE_STACK_FAULT in Comms.InputData.Status2 then
    Include(Values, ecms_status2_ANODE_STACK_FAULT)
  else
    Exclude(Values, ecms_status2_ANODE_STACK_FAULT);

  if status2_MOISTURE_DETECTED in Comms.InputData.Status2 then
    Include(Values, ecms_status2_MOISTURE_DETECTED)
  else
    Exclude(Values, ecms_status2_MOISTURE_DETECTED);

  if status2_OUTLET_FLOW_FAULT in Comms.InputData.Status2 then
    Include(Values, ecms_status2_OUTLET_FLOW_FAULT)
  else
    Exclude(Values, ecms_status2_OUTLET_FLOW_FAULT);

  if status3_CONTACT_SENSED in Comms.InputData.Status3 then
    Include(Values, ecms_status3_CONTACT_SENSED)
  else
    Exclude(Values, ecms_status3_CONTACT_SENSED);

  if status3_SENSE_LEADS_FAULT in Comms.InputData.Status3 then
    Include(Values, ecms_status3_SENSE_LEADS_FAULT)
  else
    Exclude(Values, ecms_status3_SENSE_LEADS_FAULT);

  if status3_STACK_FAULT in Comms.InputData.Status3 then
    Include(Values, ecms_status3_STACK_FAULT)
  else
    Exclude(Values, ecms_status3_STACK_FAULT);

  if status3_MAIN_TX_TEMP in Comms.InputData.Status3 then
    Include(Values, ecms_status3_MAIN_TX_TEMP)
  else
    Exclude(Values, ecms_status3_MAIN_TX_TEMP);

  if status3_AUX_TX_TEMP in Comms.InputData.Status3 then
    Include(Values, ecms_status3_AUX_TX_TEMP)
  else
    Exclude(Values, ecms_status3_AUX_TX_TEMP);

  if status3_BUSBAR_TEMP in Comms.InputData.Status3 then
    Include(Values, ecms_status3_BUSBAR_TEMP)
  else
    Exclude(Values, ecms_status3_BUSBAR_TEMP);

  if status3_DV2DT_FAULT in Comms.InputData.Status3 then
    Include(Values, ecms_status3_DV2DT_FAULT)
  else
    Exclude(Values, ecms_status3_DV2DT_FAULT);

  if status3_DV1DT_FAULT in Comms.InputData.Status3 then
    Include(Values, ecms_status3_DV1DT_FAULT)
  else
    Exclude(Values, ecms_status3_DV1DT_FAULT);

  if status3_DC_INVALID_REQUEST in Comms.InputData.Status3 then
    Include(Values, ecms_status3_DC_INVALID_REQUEST)
  else
    Exclude(Values, ecms_status3_DC_INVALID_REQUEST);

  if status3_LOSS_OF_TIMING in Comms.InputData.Status3 then
    Include(Values, ecms_status3_LOSS_OF_TIMING)
  else
    Exclude(Values, ecms_status3_LOSS_OF_TIMING);

  if status3_DVDT_IRQ in Comms.InputData.Status3 then
    Include(Values, ecms_status3_DVDT_IRQ)
  else
    Exclude(Values, ecms_status3_DVDT_IRQ);

  if status3_COMMS_FAULT in Comms.InputData.Status3 then
    Include(Values, ecms_status3_COMMS_FAULT)
  else
    Exclude(Values, ecms_status3_COMMS_FAULT);

  if status3_FAST_MAIN_OVER_CURRENT in Comms.InputData.Status3 then
    Include(Values, ecms_status3_FAST_MAIN_OVER_CURRENT)
  else
    Exclude(Values, ecms_status3_FAST_MAIN_OVER_CURRENT);

  if status3_FAST_ANODE_OVER_CURRENT in Comms.InputData.Status3 then
    Include(Values, ecms_status3_FAST_ANODE_OVER_CURRENT)
  else
    Exclude(Values, ecms_status3_FAST_ANODE_OVER_CURRENT);

  if status4_STACK_TEMP_WARNING in Comms.InputData.Status4 then
    Include(Values, ecms_status4_STACK_TEMP_WARNING)
  else
    Exclude(Values, ecms_status4_STACK_TEMP_WARNING);

  if status4_MAIN_TX_TEMP_WARNING in Comms.InputData.Status4 then
    Include(Values, ecms_status4_MAIN_TX_TEMP_WARNING)
  else
    Exclude(Values, ecms_status4_MAIN_TX_TEMP_WARNING);

  if status4_AUX_TX_TEMP_WARNING in Comms.InputData.Status4 then
    Include(Values, ecms_status4_AUX_TX_TEMP_WARNING)
  else
    Exclude(Values, ecms_status4_AUX_TX_TEMP_WARNING);

  if status4_ANODE_STACK_TEMP_WARNING in Comms.InputData.Status4 then
    Include(Values, ecms_status4_ANODE_STACK_TEMP_WARNING)
  else
    Exclude(Values, ecms_status4_ANODE_STACK_TEMP_WARNING);
end;

initialization
  TECMPlc.AddPlugin('ECMControl', TECMPlc);
end.
