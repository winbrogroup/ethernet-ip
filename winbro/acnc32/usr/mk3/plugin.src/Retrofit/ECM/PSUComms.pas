unit PSUComms;

interface

uses Sysutils, Classes, Windows, Cnctypes, ECMState, PSUCommon, AbstractPSUComms;


type
  TStatus2Value = (
    // PSU_pod_status2 , 6809 ref FS 2 */

    status2_POS_WP1_DIDT_FAULT,
    status2_POS_WP2_DIDT_FAULT,
    status2_POS_WP3_DIDT_FAULT,
    status2_NEG_WP1_DIDT_FAULT,
    status2_NEG_WP2_DIDT_FAULT,
    status2_NEG_WP3_DIDT_FAULT,
    status2_NOT_USED_1,
    status2_NOT_USED_2,

    //  PSU_pod_status2 , 6809 ref FS 3 */

    status2_NOT_USED_3,
    status2_NOT_USED_4,
    status2_NOT_USED_5,
    status2_ANODE_STACK_FAULT,
    status2_NOT_USED_6,
    status2_MOISTURE_DETECTED,
    status2_OUTLET_FLOW_FAULT,
    status2_NOT_USED_7
  );

  TStatus3Value = (
    status3_CONTACT_SENSED,
    status3_SENSE_LEADS_FAULT,
    status3_STACK_FAULT,
    status3_MAIN_TX_TEMP,
    status3_AUX_TX_TEMP,
    status3_BUSBAR_TEMP,
    status3_DV2DT_FAULT,
    status3_DV1DT_FAULT,

    //  PSU_pod_status3 , 6809 ref FS 5 */

    status3_DC_INVALID_REQUEST,
    status3_LOSS_OF_TIMING,
    status3_DVDT_IRQ,
    status3_NOT_USED_8,
    status3_COMMS_FAULT,
    status3_FAST_MAIN_OVER_CURRENT,
    status3_FAST_ANODE_OVER_CURRENT,
    status3_NOT_USED_11
  );
  TStatus3Values = set of TStatus3Value;

  TStatus4Value = (
    status4_STACK_TEMP_WARNING,
    status4_MAIN_TX_TEMP_WARNING,
    status4_AUX_TX_TEMP_WARNING,
    status4_ANODE_STACK_TEMP_WARNING,
    status4_NOT_USED_12,
    status4_NOT_USED_13,
    status4_NOT_USED_14,
    status4_NOT_USED_15
  );
  TStatus4Values = set of TStatus4Value;

  TStatus2Values = set of TStatus2Value;

  BUF_TYPE_S =  packed record
  case Byte of
  0: (
    status1: TStatus1Values;
    status2: TStatus2Values;
    status3: TStatus3Values;
    status4: TStatus4Values;

    wp1_current: Byte;
    wp2_current: Byte;
    wp3_current: Byte;

    on_voltage: Byte;
    off_voltage: Byte;

    anode_current: Byte;
    anode_voltage: Byte;

    main_voltage: Byte;
  );

  1: (
    rawData: array[0..14] of Byte;
  )


  end;

  BUF_TYPE_P = packed record
  case Byte of
  0: (
    format: Byte;
    command: Word;
    on_voltage: Byte;
    off_voltage: Byte;
    on_current: Byte;
    on_time: Word;      // in units of phase counts */
    off_time: Word;     // in units of phase counts */
    ramp_incs: Byte;    // no of dac incs the o/p to be increased at .5 sec interval */
    left_dvdt_gain: Byte;
    right_dvdt_gain: Byte;
    anode_voltage: Byte;

    plus_di1_dt: Byte;
    plus_di2_dt: Byte;
    minus_di1_dt: Byte;
    minus_di2_dt: Byte;
  );
  1: (
    rawdata: array [0..17] of Byte;
  );
  end;


type
  TPSUComms = class(TAbstractPSUComms)
  private
    CommHandle: THandle;
    CommTimeouts: TCommTimeouts;
    FComms: string;
    ErrorCount: Integer;
    FOutMessage: string;
    FStateChange: Boolean;
    SendIndex: Integer;
    procedure SendMessage(Text: string);
    function ReceiveMessage: string;
    procedure GetStatus;
    procedure SendStatus;
    procedure SendCommand(Cmd: string);
    procedure ReestablishComms;
    procedure SendChar(Char: Byte);
    function GetChar: Byte;
    procedure ReceiveChar(Char: Byte);
    procedure DecodeInputStatus(Buffer: string);
    function EncodeOutputStatus: string;
  public
    InputData: BUF_TYPE_S;
    OutputData: BUF_TYPE_P;
    CommsError: Boolean;
    constructor Create(AComms: string);
    destructor Destroy; override;
    procedure Execute; override;
    property OutMessage: string read FOutMessage write FOutMessage;
    procedure ClearHistory; override;
    procedure SetStateChange;
  end;

implementation

uses
  Proto;

var
  DCB: TDCB =
    (DCBLength: SizeOf(TDCB);
     BaudRate: CBR_2400;
     Flags: DTR_CONTROL_DISABLE;
     wReserved: 0;
     XonLim: 0;
     XoffLim: 0;
     ByteSize: 8;
     Parity: NOPARITY;
     StopBits: TWOSTOPBITS;
     XonChar: #0;
     XoffChar: #0;
     ErrorChar: #0;
     EoFChar: #0;
     EvtChar: #0;
     wReserved1: 0);

{ TPSUComms }

constructor TPSUComms.Create(AComms: string);
begin
  inherited Create(True);

  // Prefeed initial state
  ECMRequest.DcOnConstantVoltage:= False;
  ECMRequest.DcOnPulsed:= False;
  ECMRequest.DcOnConstantCurrent:= False;
  Self.SetStateChange;

  FDataList:= TList.Create;
  FComms:= AComms;
  CommHandle:= CreateFile(PChar(Format('%s:', [FComms])),
     GENERIC_READ or GENERIC_WRITE, 0, nil, OPEN_EXISTING, 0, 0);
  if CommHandle = INVALID_HANDLE_VALUE then
    RaiseLastOSError;

  CommTimeouts.ReadIntervalTimeout:= 1000;
  CommTimeouts.ReadTotalTimeoutMultiplier:= 100;
  CommTimeouts.ReadTotalTimeoutConstant:= 1000;
   CommTimeouts.WriteTotalTimeoutMultiplier:= 0;
  CommTimeouts.WriteTotalTimeoutConstant:= 0;

  if not SetCommState(CommHandle, DCB) then
    RaiseLastOSError;

  if not SetCommTimeouts(CommHandle, CommTimeouts) then
    RaiseLastOSError;

end;

destructor TPSUComms.Destroy;
begin
  if CommHandle <> INVALID_HANDLE_VALUE then
    CloseHandle(CommHandle);

  inherited;
end;

procedure TPSUComms.Execute;
begin
  while not Terminated do begin
    try
      GetStatus;
      CommsError:= False;

      if FStateChange then begin
        SendStatus;
        FStateChange:= False;
      end;
    except
      ReestablishComms;
      Sleep(500);
    end;
  end;
end;

procedure TPSUComms.SendCommand(Cmd: string);
var B: Byte;
    Buffer: string;
const MAX_RETRY_COUNT = 6;
begin
  // Standard message send CSTA (simplify)
  ErrorCount:= 0;

  SendChar(ENQ);
  B:= GetChar;
  if B <> ACK then begin
    raise Exception.Create('Unexpected response');
  end;

  repeat
    Inc(ErrorCount);
    if ErrorCount > MAX_RETRY_COUNT then
      raise Exception.Create('Too many retries on comms');
    SendMessage(Cmd);
  until GetChar = ACK;
  SendChar(EOT);
end;


procedure TPSUComms.SendChar(Char: Byte);
var Buffer: array [0..1] of Byte;
    Count: Cardinal;
begin
  Buffer[0]:= Char;
  WriteFile( CommHandle, Buffer, 1, Count, nil);
  FlushFileBuffers( CommHandle);

  if Count <> 1 then
    raise Exception.Create('Failed to write character to comms');

  DataList.Add(Pointer(Ord(Char) or $100));
  if (SendIndex mod 1) = 0 then begin
    Sleep(10);
    SendIndex:= 0;
  end;
  Inc(SendIndex);
  if Assigned(OnDataChange) then
    OnDataChange(Self);
end;

function TPSUComms.GetChar: Byte;
var Buffer: array [0..1] of Byte;
    Count: Cardinal;
begin
  ReadFile( CommHandle, Buffer, 1, Count, nil);
  if Count = 0 then
    raise Exception.Create('Rx timeout waiting for Ack');
  Result:= Buffer[0];
  DataList.Add(Pointer(Result));
  if Assigned(OnDataChange) then
    OnDataChange(Self);
end;

procedure TPSUComms.ReestablishComms;
var I: Integer;
begin
  try
    CommsError:= True;
  {
    for I:= 0 to 255 do begin
      SendChar(I);
      try
        GetChar;
      except
      end;
    end;
    }
    SendChar(STX);
    SendChar(ETX);
    while GetChar <> 0 do;
  except
  end;
end;

procedure TPSUComms.GetStatus;
var Buffer: string;
begin
  SendCommand('CSTA');

  Buffer:= ReceiveMessage;
  DecodeInputStatus(Buffer);
end;

procedure TPSUComms.SendStatus;
begin
  SendCommand('UTAB');
  ReceiveMessage;
  OutputData.format:= COMMAND_PARA;

  if ECMRequest.DcOnConstantVoltage then
    OutputData.command:= DC_ON_VOLTAGE
  else if ECMRequest.DcOnPulsed then
    OutputData.command:= DC_ON_VOLTAGE + PULSING_REQUIRED
  else if ECMRequest.DcOnConstantCurrent then
    OutputData.command:= DC_ON_CURRENT
  else
    OutputData.command:= DC_OFF;

//  if ECMRequest. ANODE ADNOE ADNODE ANODE ANODE_V_REQUIRED

  // CPU voltage factor = 40/255 volts, programmed = volts
  OutputData.on_voltage:= Round(10.0 * 255.0 / 40.0);
  OutputData.off_voltage:= Round(0.0 * 255.0 / 40.0);

  // To produce 0.1 second on
  // Calculte max / min
  // on_off_fac = (float) 1000 / (sys_frequency*6); >>> where sys_frequency = mains
  // on_time/on_off_fac

  OutputData.on_time:= Round((0.1 * 1000.0) / (1000.0 / (60.0*6.0))) ;
  OutputData.off_time:= Round((0.1 * 1000.0) / (1000.0 / (60.0*6.0))) ;

  //const_current_fac = msd_int[CONST_CURRENT_FAC] / msd_flt[CUR_FAC1] ;
  //inter_buf_p.on_current=(PSU_pid.on_current * const_current_fac)*PSU_pod.dco_factor;
  // msd_int[CONST_CURRENT_FAC] = 768
  // msd_flt[CUR_FAC1] = 20000
  OutputData.on_current:= Round(5000.0 * 768.0 / 20000.0);


  OutputData.ramp_incs:= 20;
  OutputData.left_dvdt_gain:= Round(ECMRequest.SparkSensitivityLeft * 255.0 / 100.0);
  OutputData.right_dvdt_gain:= Round(ECMRequest.SparkSensitivityRight * 255.0 / 100.0);;
  OutputData.anode_voltage:= Round(ECMRequest.ProtectiveAnodeVoltage * 255.0 / 40.0);
  OutputData.plus_di1_dt:= Round(ECMRequest.dIdTPositiveChange * 255.0 / 100.0);
  OutputData.plus_di2_dt:= Round(ECMRequest.dIdTPositiveChange * 255.0 / 100.0);
  OutputData.minus_di1_dt:= Round(ECMRequest.dIdTNegativeChange * 255.0 / 100.0);
  OutputData.minus_di2_dt:= Round(ECMRequest.dIdTNegativeChange * 255.0 / 100.0);

  SendCommand('ETAB' + Self.EncodeOutputStatus);
end;



procedure TPSUComms.SendMessage(Text: string);
var I: Integer;
    CSum: Byte;
begin
  SendChar(STX);
  CSum:= 0;
  for I:= 1 to Length(Text) do begin
    SendChar(Ord(Text[I]));
    CSum:= CSum + Ord(Text[I]);
  end;
  CSum:= CSum + ETX;
  SendChar(ETX);
  SendChar(CSum);
end;

function TPSUComms.ReceiveMessage: string;
var Tmp: Byte;
    CSum: Byte;
begin
  ReceiveChar(ENQ);
  SendChar(ACK);
  ReceiveChar(STX);

  Tmp:= GetChar;
  CSum:= 0;
  while Tmp <> ETX do begin
    Result:= Result + Char(Tmp);
    CSum:= CSum + Tmp;

    if Length(Result) > 100 then begin
      raise Exception.Create('Buffer overrun');
    end;
    Tmp:= GetChar;
  end;
  CSum:= CSum + ETX;

  if GetChar <> CSum then begin
    raise Exception.Create('Invalid CSum');
  end;

  SendChar(ACK);
  ReceiveChar(EOT);

  SendChar(ACK);
end;



procedure TPSUComms.ReceiveChar(Char: Byte);
var Tmp: Byte;
begin
  Tmp:= GetChar;
  if Tmp <> Char then
    raise Exception.Create('Unexpected character: ' + IntToHex(Tmp, 2));
end;

procedure TPSUComms.ClearHistory;
begin
  DataList.Clear;
  if Assigned(OnDataChange) then
    OnDataChange(Self);
end;

procedure TPSUComms.DecodeInputStatus(Buffer: string);
var Tmp: string;
    Count: Integer;
begin
  Count:= 0;
  Delete(Buffer, 1, 4);
  while Buffer <> '' do begin
    Tmp:= ReadDelimited(Buffer, ',');
    InputData.rawData[Count]:= StrToInt('$' + Tmp);
    Inc(Count);
  end;
end;

function TPSUComms.EncodeOutputStatus: string;
var I: Integer;
begin
  Result:= '';
  for I:= 0 to Sizeof(OutputData) do begin
    Result:= Result + IntToHex(OutputData.rawdata[I], 2) + ',';
  end;
end;

procedure TPSUComms.SetStateChange;
begin
  FStateChange:= True;
end;


end.
