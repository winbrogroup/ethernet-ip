library ECMPSU;

{ Important note about DLL memory management: ShareMem must be the
  first unit in your library's USES clause AND your project's (select
  Project-View Source) USES clause if your DLL exports any procedures or
  functions that pass strings as parameters or function results. This
  applies to all strings passed to and from your DLL--even those that
  are nested in records and classes. ShareMem is the interface unit to
  the BORLNDMM.DLL shared memory manager, which must be deployed along
  with your DLL. To avoid using BORLNDMM.DLL, pass string information
  using PChar or ShortString parameters. }

{%File '..\..\..\..\C510\SOURCE\JETAVIO\PSURUT.C'}
{%File '..\..\..\..\C510\SOURCE\JETAVIO\PSUDUR.C'}
{%File '..\..\..\..\C510\INCLUDE\ECM\PSU.H'}

uses
  SysUtils,
  Classes,
  Proto in 'Proto.pas' {Protocol},
  G100 in 'GCode\G100.pas',
  G101 in 'GCode\G101.pas',
  G102 in 'GCode\G102.pas',
  G103 in 'GCode\G103.pas',
  G104 in 'GCode\G104.pas',
  G105 in 'GCode\G105.pas',
  G106 in 'GCode\G106.pas',
  G110 in 'GCode\G110.pas',
  G111 in 'GCode\G111.pas',
  G112 in 'GCode\G112.pas',
  G113 in 'GCode\G113.pas',
  G114 in 'GCode\G114.pas',
  G115 in 'GCode\G115.pas',
  G130 in 'GCode\G130.pas',
  G131 in 'GCode\G131.pas',
  G132 in 'GCode\G132.pas',
  G133 in 'GCode\G133.pas',
  G137 in 'GCode\G137.pas',
  G135 in 'GCode\G135.pas',
  G136 in 'GCode\G136.pas',
  ECMState in 'ECMState.pas',
  G134 in 'GCode\G134.pas',
  SignatureForm in 'Signature\SignatureForm.pas' {ECMSignature},
  VerticalHighLowBar in 'Signature\VerticalHighLowBar.pas',
  XYGraph in 'Signature\XYGraph.pas',
  Scales in 'Signature\Scales.pas',
  SignatureData in 'Signature\SignatureData.pas',
  PSUCommsRutland in 'PSUCommsRutland.pas',
  PSUCommon in 'PSUCommon.pas',
  ECMPlcRutland in 'ECMPlcRutland.pas',
  AbstractPSUComms in 'AbstractPSUComms.pas';

{$R *.res}

begin
end.
