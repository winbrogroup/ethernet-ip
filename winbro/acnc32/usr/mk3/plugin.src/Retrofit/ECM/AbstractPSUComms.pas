unit AbstractPSUComms;

interface

uses Classes;

type
  TAbstractPSUComms = class(TThread)
  private
    FOnDataChange : TNotifyEvent;
  protected
    FDataList: TList;
  public
    procedure ClearHistory; virtual; abstract;
    property DataList : TList{TThreadList} read FDataList;
    property OnDataChange : TNotifyEvent read FOnDataChange write FOnDataChange;
  end;

implementation


end.
