unit Proto;

interface


uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, AbstractPSUComms;

type
  TProtocol = class(TForm)
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    Timer1: TTimer;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormPaint(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
  private
    IH, IW : Integer;
    FRefreshRequired : Boolean;
    Comms : TAbstractPSUComms;

    procedure RefreshRequired(Sender : TObject);
    procedure DrawCommItem(B : Byte; T, L : Integer);
    procedure UpdateDisplay(Sender : TObject);
  public
    constructor Create(AComms: TAbstractPSUComms);
    destructor Destroy; override;
  end;

var
  Protocol: TProtocol;

implementation

{$I PSUCOMMS.INC}
{$R *.DFM}

const
  BigFont = 14;
  LittleFont = 7;

procedure TProtocol.DrawCommItem(B : Byte; T, L : Integer);
  procedure LittleOut(const Tmp : string);
  begin
    Canvas.Font.Size := LittleFont;
    Canvas.TextOut(L, T, Tmp[1]);
    Canvas.TextOut(L + (IW div 2), T + (IH div 2) - 2, Tmp[2]);
  end;

var Tmp : string;
begin
  case B of
    Byte('A')..Byte('Z'),
    Byte('a')..Byte('z'),
    Byte(' ')..Byte('9') :  begin
      Canvas.Font.Size := BigFont;
      Canvas.TextOut(L, T, Char(B));
    end;

    DLE : begin
      LittleOut('DL');
    end;

    STX : begin
      LittleOut('SX');
    end;

    NAK : begin
      LittleOut('NK');
    end;

    ETX : begin
      LittleOut('EX');
    end;

    EOT : begin
      LittleOut('ET');
    end;

    ACK : begin
      LittleOut('AK');
    end;

    ENQ: begin
      LittleOut('EQ');
    end;

  else
    Tmp := IntToHex(B, 2);
    LittleOut(Tmp);
  end;
end;

procedure TProtocol.RefreshRequired;
begin
  FRefreshRequired := True;
end;

procedure TProtocol.UpdateDisplay(Sender : TObject);
var H, W, I, J, SoFar, P : Integer;
    T : Integer;
    List : TList;
begin
  if Assigned(Comms) then begin
    List := Comms.DataList; 
    try
      Canvas.Font.Size := BigFont;
      IH := Canvas.TextHeight('Q');
      IW := Canvas.TextWidth('Q');
      H := (ClientHeight - 15) div (IH * 2);
      W := ClientWidth div IW;


      if (W * H) <= List.Count then
        SoFar := List.Count - (W * H)
      else
        SoFar := 0;

      for J := 0 to H - 1 do begin
        T := IH * J * 2;
        Canvas.Brush.Color := clBlue;
        Canvas.Font.Color := clWhite;
        Canvas.FillRect(Rect(0, T, ClientWidth, T + IH));
        for I := 0 to W - 1 do begin
          if I + SoFar >= List.Count then
            Break;
          P := Integer(List[SoFar + I]);
          if (P and $100) <> 0 then begin
            DrawCommItem(Byte(P), T, I * IW);
          end;
        end;

        T := IH * J * 2 + IH;
        Canvas.Brush.Color := clSilver;
        Canvas.Font.Color := clBlack;
        Canvas.FillRect(Rect(0, T, ClientWidth, T + IH));
        for I := 0 to W - 1 do begin
          if I + SoFar >= List.Count then
            Break;
          P := Integer(List[SoFar + I]);
          if (P and $100) = 0 then begin
            DrawCommItem(Byte(P), T, I * IW);
          end;
        end;
        SoFar := SoFar + W;
      end;
    finally
    end;
  end;
end;

procedure TProtocol.FormShow(Sender: TObject);
begin
  Comms.OnDataChange := RefreshRequired;
end;

procedure TProtocol.FormResize(Sender: TObject);
begin
  Repaint;
end;

procedure TProtocol.FormPaint(Sender: TObject);
begin
  UpdateDisplay(Self);
end;

procedure TProtocol.BitBtn2Click(Sender: TObject);
begin
  Comms.ClearHistory;
end;

procedure TProtocol.Timer1Timer(Sender: TObject);
begin
  if FRefreshRequired then
    UpdateDisplay(nil);

  FRefreshRequired := False;
end;

constructor TProtocol.Create(AComms: TAbstractPSUComms);
begin
  inherited Create(nil);
  Self.Comms:= AComms;
  Comms.OnDataChange:= RefreshRequired;
end;

destructor TProtocol.Destroy;
begin
  Comms.OnDataChange:= nil;
  inherited;
end;

procedure TProtocol.FormClose(Sender: TObject; var Action: TCloseAction);
begin
//  Protocol:= nil;
//  Self.Free;
end;

end.
