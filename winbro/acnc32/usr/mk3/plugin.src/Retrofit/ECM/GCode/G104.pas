unit G104;

interface

uses Sysutils, AbstractExtGCode, INamedInterface, NamedPlugin, CncTypes, ECMState;

type
  TG104 = class (TAbstractNCExtGCode)
  private
    Left: TTagRef;
    Right: TTagRef;
  public
    procedure Compile(Compiler: INCCompiler; Line: WideString); override; stdcall;
    procedure Execute; override; stdcall;
  end;


implementation


{ TTestGCode }

procedure TG104.Compile(Compiler: INCCompiler; Line: WideString);
var Working, Piece: string;
    Desig: Char;
    Tag: TTagRef;
begin
  Working:= Trim(Line);
  Piece:= CncTypes.ReadDelimitedParenthesis(Working, ' ');
  Trim(Working);
  while Piece <> '' do begin
    if Length(Piece) > 1 then begin
      Desig:= Piece[1];
      Delete(Piece, 1, 1);
      Tag:= Compiler.ResolveNCVariable(Piece);
      case Desig of
        'L': Left:= Tag;
        'R': Right:= Tag;
      else
        raise Exception.CreateFmt('Unexpected parameter %s', [Desig]);
      end;
    end else
      raise Exception.Create('Invalid parameter format');

    Piece:= CncTypes.ReadDelimitedParenthesis(Working, ' ');
    Trim(Working);
  end;
  if (Left = nil) or (Right = nil) then
    raise Exception.Create('G104 requires all parameters L R');

end;

procedure TG104.Execute;
begin
  ECMRequest.SparkSensitivityLeft:= Named.GetAsDouble(Left);
  ECMRequest.SparkSensitivityRight:= Named.GetAsDouble(Right);
end;


initialization
  TAbstractNCExtGCode.AddNCExtGCode('104', TG104);
end.
