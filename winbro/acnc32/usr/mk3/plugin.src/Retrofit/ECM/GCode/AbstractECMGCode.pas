unit AbstractECMGCode;

interface

uses AbstractExtGCode;

type
  TAbstractECMGCode = class abstract(TAbstractNCExtGCode)
  protected
    Programmed: Boolean;
  public
    procedure Reset; virtual;
    procedure Action; virtual; 
  end;

implementation

{ TAbstractECMGCode }

procedure TAbstractECMGCode.Action;
begin
  Programmed:= True;
end;

procedure TAbstractECMGCode.Reset;
begin
  Programmed:= False;
end;

end.
