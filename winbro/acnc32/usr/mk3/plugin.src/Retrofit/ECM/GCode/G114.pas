unit G114;

interface

uses Sysutils, AbstractExtGCode, INamedInterface, NamedPlugin, CncTypes, G111, ECMState;

type
  TG114 = class (TG111)
  public
    procedure Execute; override; stdcall;
  end;


implementation


{ TTestGCode }

procedure TG114.Execute;
begin
  ECMRequest.OutletFlowHighLimit:= Named.GetAsDouble(HighLimit);
  ECMRequest.OutletFlowLowLimit:= Named.GetAsDouble(LowLimit);
end;


initialization
  TAbstractNCExtGCode.AddNCExtGCode('114', TG114);
end.
