unit G137;

interface

uses AbstractExtGCode, G133, ECMState, NamedPlugin;

type
  TG137 = class (TG133)
  public
    procedure Execute; override; stdcall;
  end;

implementation

{ TG134 }

procedure TG137.Execute;
begin
  ECMRequest.WP3UpperCurrentLimit:= Named.GetAsDouble(A);
  ECMRequest.WP3LowerCurrentLimit:= Named.GetAsDouble(B);
end;

initialization
  TAbstractNCExtGCode.AddNCExtGCode('137', TG137);
end.
