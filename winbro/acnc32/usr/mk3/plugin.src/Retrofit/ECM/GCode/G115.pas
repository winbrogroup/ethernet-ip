unit G115;

interface

uses Sysutils, AbstractExtGCode, INamedInterface, NamedPlugin, CncTypes, G111, ECMState;

type
  TG115 = class (TG111)
  public
    procedure Execute; override; stdcall;
  end;


implementation


{ TTestGCode }

procedure TG115.Execute;
begin
  ECMRequest.OutletTemperatureHighLimit:= Named.GetAsDouble(HighLimit);
  ECMRequest.OutletTemperatureLowLimit:= Named.GetAsDouble(LowLimit);
end;


initialization
  TAbstractNCExtGCode.AddNCExtGCode('115', TG115);
end.
