unit G110;

interface

uses Sysutils, AbstractExtGCode, INamedInterface, NamedPlugin, CncTypes, ECMState;

type
  TG110 = class (TAbstractNCExtGCode)
  protected
    Pressure: TTagRef;
    HighLimit: TTagRef;
    LowLimit: TTagRef;
  public
    procedure Compile(Compiler: INCCompiler; Line: WideString); override; stdcall;
    procedure Execute; override; stdcall;
  end;


implementation


{ TTestGCode }

procedure TG110.Compile(Compiler: INCCompiler; Line: WideString);
var Working, Piece: string;
    Desig: Char;
    Tag: TTagRef;
begin
  Working:= Trim(Line);
  Piece:= CncTypes.ReadDelimitedParenthesis(Working, ' ');
  Trim(Working);
  while Piece <> '' do begin
    if Length(Piece) > 1 then begin
      Desig:= Piece[1];
      Delete(Piece, 1, 1);
      Tag:= Compiler.ResolveNCVariable(Piece);
      case Desig of
        'P': Pressure:= Tag;
        'H': HighLimit:= Tag;
        'L': LowLimit:= Tag;
      else
        raise Exception.CreateFmt('Unexpected parameter %s', [Desig]);
      end;
    end else
      raise Exception.Create('Invalid parameter format');

    Piece:= CncTypes.ReadDelimitedParenthesis(Working, ' ');
    Trim(Working);
  end;
  if (Pressure = nil) or (HighLimit = nil) or (LowLimit = nil)then
    raise Exception.Create('G110 and G113 require parameters P H L');
end;

procedure TG110.Execute;
begin
  ECMRequest.InletPressure:= Named.GetAsDouble(Pressure);
  ECMRequest.InletPressureHighLimit:= Named.GetAsDouble(HighLimit);
  ECMRequest.InletPressureLowLimit:= Named.GetAsDouble(LowLimit);
end;

initialization
  TAbstractNCExtGCode.AddNCExtGCode('110', TG110);
end.
