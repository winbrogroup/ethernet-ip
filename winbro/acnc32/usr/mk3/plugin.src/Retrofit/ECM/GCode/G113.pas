unit G113;

interface

uses Sysutils, AbstractExtGCode, INamedInterface, NamedPlugin, CncTypes, G110, ECMState;

type
  TG113 = class (TG110)
  public
    procedure Execute; override; stdcall;
  end;


implementation


procedure TG113.Execute;
begin
  ECMRequest.OutletPressure:=  Named.GetAsDouble(Pressure);
  ECMRequest.OutletPressureHighLimit:= Named.GetAsDouble(HighLimit);
  ECMRequest.OutletPressureLowLimit:= Named.GetAsDouble(LowLimit);
end;


initialization
  TAbstractNCExtGCode.AddNCExtGCode('113', TG113);
end.
