unit G102;

interface

uses Sysutils, AbstractExtGCode, INamedInterface, NamedPlugin, CncTypes, ECMState;

type
  TG102 = class (TAbstractNCExtGCode)
  private
    Current: TTagRef;
    RampTime: TTagRef;
    HighLimit: TTagRef;
    LowLimit: TTagRef;
  public
    procedure Compile(Compiler: INCCompiler; Line: WideString); override; stdcall;
    procedure Execute; override; stdcall;
  end;


implementation


{ TTestGCode }

procedure TG102.Compile(Compiler: INCCompiler; Line: WideString);
var Working, Piece: string;
    Desig: Char;
    Tag: TTagRef;
begin
  Working:= Trim(Line);
  Piece:= CncTypes.ReadDelimitedParenthesis(Working, ' ');
  Trim(Working);
  while Piece <> '' do begin
    if Length(Piece) > 1 then begin
      Desig:= Piece[1];
      Delete(Piece, 1, 1);
      Tag:= Compiler.ResolveNCVariable(Piece);
      case Desig of
        'I': Current:= Tag;
        'R': RampTime:= Tag;
        'H': HighLimit:= Tag;
        'L': LowLimit:= Tag;
      else
        raise Exception.CreateFmt('Unexpected parameter %s', [Desig]);
      end;
    end else
      raise Exception.Create('Invalid parameter format');

    Piece:= CncTypes.ReadDelimitedParenthesis(Working, ' ');
    Trim(Working);
  end;
  if (Current = nil) or (Ramptime = nil) or (Highlimit = nil) or (LowLimit = nil) then
    raise Exception.Create('G102 requires all parameters I R H L');
end;

procedure TG102.Execute;
begin
  ECMRequest.Current:= Named.GetAsDouble(Current);
  ECMRequest.CurrentRampTime:= Named.GetAsDouble(Ramptime);
  ECMRequest.CurrentHighLimit:= Named.GetAsDouble(HighLimit);
  ECMRequest.CurrentLowLimit:= Named.GetAsDouble(LowLimit);
end;

initialization
  TAbstractNCExtGCode.AddNCExtGCode('102', TG102);
end.
