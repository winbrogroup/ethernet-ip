unit G132;

interface

uses Sysutils, AbstractExtGCode, INamedInterface, NamedPlugin, CncTypes, G130, ECMState;

type
  TG132 = class (TG130)
  public
    procedure Execute; override; stdcall;
  end;


implementation


{ TTestGCode }

procedure TG132.Execute;
begin
  ECMRequest.InletUpperFlowLimit:= Named.GetAsDouble(A);
  ECMRequest.InletLowerFlowLimit:= Named.GetAsDouble(B);
  ECMRequest.OutletUpperFlowLimit:= Named.GetAsDouble(C);
  ECMRequest.OutletLowerFlowLimit:= Named.GetAsDouble(D);
end;


initialization
  TAbstractNCExtGCode.AddNCExtGCode('132', TG132);
end.
