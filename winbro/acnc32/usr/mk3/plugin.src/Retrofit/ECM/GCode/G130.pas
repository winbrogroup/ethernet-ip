unit G130;

interface

uses Sysutils, AbstractExtGCode, INamedInterface, NamedPlugin, CncTypes, ECMState;

type
  TG130 = class (TAbstractNCExtGCode)
  protected
    A: TTagRef;
    B: TTagRef;
    C: TTagRef;
    D: TTagRef;
  public
    procedure Compile(Compiler: INCCompiler; Line: WideString); override; stdcall;
    procedure Execute; override; stdcall;
  end;


implementation


{ TTestGCode }

procedure TG130.Compile(Compiler: INCCompiler; Line: WideString);
var Working, Piece: string;
    Desig: Char;
    Tag: TTagRef;
begin
  Working:= Trim(Line);
  Piece:= CncTypes.ReadDelimitedParenthesis(Working, ' ');
  Trim(Working);
  while Piece <> '' do begin
    if Length(Piece) > 1 then begin
      Desig:= Piece[1];
      Delete(Piece, 1, 1);
      Tag:= Compiler.ResolveNCVariable(Piece);
      case Desig of
        'A': A:= Tag;
        'B': B:= Tag;
        'C': C:= Tag;
        'D': D:= Tag;
      else
        raise Exception.CreateFmt('Unexpected parameter %s', [Desig]);
      end;
    end else
      raise Exception.Create('Invalid parameter format');

    Piece:= CncTypes.ReadDelimitedParenthesis(Working, ' ');
    Trim(Working);
  end;
  if (A = nil) or (B = nil) or (C = nil) or (D = nil) then
     raise Exception.Create('G130, G131 and G132 require parameters A, B, C and D');

end;

procedure TG130.Execute;
begin
  ECMRequest.InletUpperPressureLimit:= Named.GetAsDouble(A);
  ECMRequest.InletLowerPressureLimit:= Named.GetAsDouble(B);
  ECMRequest.OutletUpperPressureLimit:= Named.GetAsDouble(C);
  ECMRequest.OutletLowerPressureLimit:= Named.GetAsDouble(D);
end;

initialization
  TAbstractNCExtGCode.AddNCExtGCode('130', TG130);
end.
