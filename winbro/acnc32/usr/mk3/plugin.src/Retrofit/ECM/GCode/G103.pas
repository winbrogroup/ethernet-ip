unit G103;

interface

uses Sysutils, AbstractExtGCode, INamedInterface, NamedPlugin, CncTypes, ECMState;

type
  TG103 = class (TAbstractNCExtGCode)
  private
    Frequency: TTagRef;
    Period: TTagRef;
    Duty: TTagRef;
  public
    procedure Compile(Compiler: INCCompiler; Line: WideString); override; stdcall;
    procedure Execute; override; stdcall;
  end;


implementation


{ TTestGCode }

procedure TG103.Compile(Compiler: INCCompiler; Line: WideString);
var Working, Piece: string;
    Desig: Char;
    Tag: TTagRef;
begin
  Working:= Trim(Line);
  Piece:= CncTypes.ReadDelimitedParenthesis(Working, ' ');
  Trim(Working);
  while Piece <> '' do begin
    if Length(Piece) > 1 then begin
      Desig:= Piece[1];
      Delete(Piece, 1, 1);
      Tag:= Compiler.ResolveNCVariable(Piece);
      case Desig of
        'F': Frequency:= Tag;
        'P': Period:= Tag;
        'D': Duty:= Tag;
      else
        raise Exception.CreateFmt('Unexpected parameter %s', [Desig]);
      end;
    end else
      raise Exception.Create('Invalid parameter format');

    Piece:= CncTypes.ReadDelimitedParenthesis(Working, ' ');
    Trim(Working);
  end;
  if (Frequency = nil) and (Period = nil) or (Duty = nil) then
    raise Exception.Create('G103 requires all parameters D (F or P)');
end;

procedure TG103.Execute;
begin
  if Frequency <> nil then begin
    ECMRequest.Frequency:= Named.GetAsDouble(Frequency);
    ECMRequest.Period:= 0;
  end;

  if Period <> nil then begin
    ECMRequest.Period:= Named.GetAsDouble(Period);
    ECMRequest.Frequency:= 0;
  end;

  ECMRequest.Duty:= Named.GetAsDouble(Duty);
end;

initialization
  TAbstractNCExtGCode.AddNCExtGCode('103', TG103);
end.
