unit G136;

interface

uses AbstractExtGCode, G133, ECMState, NamedPlugin;

type
  TG136 = class (TG133)
  public
    procedure Execute; override; stdcall;
  end;

implementation

{ TG134 }

procedure TG136.Execute;
begin
  ECMRequest.WP2UpperCurrentLimit:= Named.GetAsDouble(A);
  ECMRequest.WP2LowerCurrentLimit:= Named.GetAsDouble(B);
end;

initialization
  TAbstractNCExtGCode.AddNCExtGCode('136', TG136);
end.
