unit G133;

interface

uses Sysutils, AbstractExtGCode, INamedInterface, NamedPlugin, CncTypes, ECMState;

type
  TG133 = class (TAbstractNCExtGCode)
  protected
    A: TTagRef;
    B: TTagRef;
  public
    procedure Compile(Compiler: INCCompiler; Line: WideString); override; stdcall;
    procedure Execute; override; stdcall;
  end;


implementation


{ TTestGCode }

procedure TG133.Compile(Compiler: INCCompiler; Line: WideString);
var Working, Piece: string;
    Desig: Char;
    Tag: TTagRef;
begin
  Working:= Trim(Line);
  Piece:= CncTypes.ReadDelimitedParenthesis(Working, ' ');
  Trim(Working);
  while Piece <> '' do begin
    if Length(Piece) > 1 then begin
      Desig:= Piece[1];
      Delete(Piece, 1, 1);
      Tag:= Compiler.ResolveNCVariable(Piece);
      case Desig of
        'A': A:= Tag;
        'B': B:= Tag;
      else
        raise Exception.CreateFmt('Unexpected parameter %s', [Desig]);
      end;
    end else
      raise Exception.Create('Invalid parameter format');

    Piece:= CncTypes.ReadDelimitedParenthesis(Working, ' ');
    Trim(Working);
  end;
  if (A = nil) or (B = nil) then
     raise Exception.Create('G133, G134, G136 and G135 require parameters A, B');
end;

procedure TG133.Execute;
begin
  ECMRequest.BiasAnodeUpperLimit:= Named.GetAsDouble(A);
  ECMRequest.BiasAnodeLowerLimit:= Named.GetAsDouble(B);
end;

initialization
  TAbstractNCExtGCode.AddNCExtGCode('133', TG133);
end.
