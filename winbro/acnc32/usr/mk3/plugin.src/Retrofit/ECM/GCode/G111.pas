unit G111;

interface

uses Sysutils, AbstractExtGCode, INamedInterface, NamedPlugin, CncTypes, ECMState;

type
  TG111 = class (TAbstractNCExtGCode)
  protected
    HighLimit: TTagRef;
    LowLimit: TTagRef;
  public
    procedure Compile(Compiler: INCCompiler; Line: WideString); override; stdcall;
    procedure Execute; override; stdcall;
  end;


implementation


{ TTestGCode }

procedure TG111.Compile(Compiler: INCCompiler; Line: WideString);
var Working, Piece: string;
    Desig: Char;
    Tag: TTagRef;
begin
  Working:= Trim(Line);
  Piece:= CncTypes.ReadDelimitedParenthesis(Working, ' ');
  Trim(Working);
  while Piece <> '' do begin
    if Length(Piece) > 1 then begin
      Desig:= Piece[1];
      Delete(Piece, 1, 1);
      Tag:= Compiler.ResolveNCVariable(Piece);
      case Desig of
        'H': HighLimit:= Tag;
        'L': LowLimit:= Tag;
      else
        raise Exception.CreateFmt('Unexpected parameter %s', [Desig]);
      end;
    end else
      raise Exception.Create('Invalid parameter format');

    Piece:= CncTypes.ReadDelimitedParenthesis(Working, ' ');
    Trim(Working);
  end;
  if (Self.HighLimit = nil) or (Self.LowLimit = nil) then
    raise Exception.Create('G111, G112, G114 and G115 require parameters H and L');
end;

procedure TG111.Execute;
begin
  ECMRequest.InletFlowHighLimit:= Named.GetAsDouble(HighLimit);
  ECMRequest.InletFlowLowLimit:= Named.GetAsDouble(LowLimit);
end;

initialization
  TAbstractNCExtGCode.AddNCExtGCode('111', TG111);
end.
