unit G106;

interface

uses Sysutils, AbstractExtGCode, INamedInterface, NamedPlugin, CncTypes, ECMState;

type
  TG106 = class (TAbstractNCExtGCode)
  private
    Positive_dIdT: TTagRef;
    Negative_dIdT: TTagRef;
  public
    procedure Compile(Compiler: INCCompiler; Line: WideString); override; stdcall;
    procedure Execute; override; stdcall;
  end;


implementation                                                     


{ TTestGCode }

procedure TG106.Compile(Compiler: INCCompiler; Line: WideString);
var Working, Piece: string;
    Desig: Char;
    Tag: TTagRef;
begin
  Working:= Trim(Line);
  Piece:= CncTypes.ReadDelimitedParenthesis(Working, ' ');
  Trim(Working);
  while Piece <> '' do begin
    if Length(Piece) > 1 then begin
      Desig:= Piece[1];
      Delete(Piece, 1, 1);
      Tag:= Compiler.ResolveNCVariable(Piece);
      case Desig of
        'A': Positive_dIdT:= Tag;
        'B': Negative_dIdT:= Tag;
      else
        raise Exception.CreateFmt('Unexpected parameter %s', [Desig]);
      end;
    end else
      raise Exception.Create('Invalid parameter format');

    Piece:= CncTypes.ReadDelimitedParenthesis(Working, ' ');
    Trim(Working);
  end;

  if (Self.Positive_dIdT = nil) or (Self.Negative_dIdT = nil) then
    raise Exception.Create('G105 requires parameters A and B');

end;

procedure TG106.Execute;
begin
  ECMRequest.dIdTPositiveChange:= Named.GetAsDouble(Self.Positive_dIdT);
  ECMRequest.dIdTNegativeChange:= Named.GetAsDouble(Self.Negative_dIdT);
end;

initialization
  TAbstractNCExtGCode.AddNCExtGCode('106', TG106);
end.
