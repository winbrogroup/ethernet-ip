unit G134;

interface

uses AbstractExtGCode, G133, ECMState, NamedPlugin;

type
  TG134 = class (TG133)
  public
    procedure Execute; override; stdcall;
  end;

implementation

{ TG134 }

procedure TG134.Execute;
begin
  ECMRequest.WP1UpperCurrentLimit:= Named.GetAsDouble(A);
  ECMRequest.WP1LowerCurrentLimit:= Named.GetAsDouble(B);
end;

initialization
  TAbstractNCExtGCode.AddNCExtGCode('134', TG134);
end.
