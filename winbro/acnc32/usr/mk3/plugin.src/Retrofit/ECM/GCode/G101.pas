unit G101;

interface

uses Sysutils, AbstractExtGCode, INamedInterface, NamedPlugin, CncTypes, ECMState;

type
  TG101 = class (TAbstractNCExtGCode)
  private
    Voltage: TTagRef;
    HighLimit: TTagRef;
    LowLimit: TTagRef;
  public
    procedure Compile(Compiler: INCCompiler; Line: WideString); override; stdcall;
    procedure Execute; override; stdcall;
  end;


implementation


{ TTestGCode }

procedure TG101.Compile(Compiler: INCCompiler; Line: WideString);
var Working, Piece: string;
    Desig: Char;
    Tag: TTagRef;
begin
  Working:= Trim(Line);
  Piece:= CncTypes.ReadDelimitedParenthesis(Working, ' ');
  Trim(Working);
  while Piece <> '' do begin
    if Length(Piece) > 1 then begin
      Desig:= Piece[1];
      Delete(Piece, 1, 1);
      Tag:= Compiler.ResolveNCVariable(Piece);
      case Desig of
        'V': Voltage:= Tag;
        'H': HighLimit:= Tag;
        'L': LowLimit:= Tag;
      else
        raise Exception.CreateFmt('Unexpected parameter %s', [Desig]);
      end;
    end else
      raise Exception.Create('Invalid parameter format');

    Piece:= CncTypes.ReadDelimitedParenthesis(Working, ' ');
    Trim(Working);
  end;
  if (Voltage = nil) or (Highlimit = nil) or (LowLimit = nil) then
    raise Exception.Create('G101 requires all parameters V H L');
end;

procedure TG101.Execute;
begin
  ECMRequest.OffTimeVoltage:= Named.GetAsDouble(Voltage);
  ECMRequest.OffTimeVoltageHighLimit:= Named.GetAsDouble(HighLimit);
  ECMRequest.OffTimeVoltageLowLimit:= Named.GetAsDouble(LowLimit);
end;


initialization
  TAbstractNCExtGCode.AddNCExtGCode('101', TG101);
end.
