unit G112;

interface

uses Sysutils, AbstractExtGCode, INamedInterface, NamedPlugin, CncTypes, G111, ECMState;

type
  TG112 = class (TG111)
  public
    procedure Execute; override; stdcall;
  end;


implementation


procedure TG112.Execute;
begin
  ECMRequest.InletTemperatureHighLimit:= Named.GetAsDouble(HighLimit);
  ECMRequest.InletTemperatureLowLimit:= Named.GetAsDouble(LowLimit);
end;


initialization
  TAbstractNCExtGCode.AddNCExtGCode('112', TG112);
end.
