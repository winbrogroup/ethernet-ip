unit G100;

interface

uses Sysutils, AbstractExtGCode, INamedInterface, NamedPlugin, CncTypes, ECMState;

type
  TG100 = class (TAbstractNCExtGCode)
  private
    Voltage: TTagRef;
    RampTime: TTagRef;
    HighLimit: TTagRef;
    LowLimit: TTagRef;
  public
    procedure Compile(Compiler: INCCompiler; Line: WideString); override; stdcall;
    procedure Execute; override; stdcall;
  end;


implementation


{ TTestGCode }

procedure TG100.Compile(Compiler: INCCompiler; Line: WideString);
var Working, Piece: string;
    Desig: Char;
    Tag: TTagRef;
begin
  Working:= Trim(Line);
  Piece:= CncTypes.ReadDelimitedParenthesis(Working, ' ');
  Trim(Working);
  while Piece <> '' do begin
    if Length(Piece) > 1 then begin
      Desig:= Piece[1];
      Delete(Piece, 1, 1);
      Tag:= Compiler.ResolveNCVariable(Piece);
      case Desig of
        'V': Voltage:= Tag;
        'R': RampTime:= Tag;
        'H': HighLimit:= Tag;
        'L': LowLimit:= Tag;
      else
        raise Exception.CreateFmt('Unexpected parameter %s', [Desig]);
      end;
    end else
      raise Exception.Create('Invalid parameter format');

    Piece:= CncTypes.ReadDelimitedParenthesis(Working, ' ');
    Trim(Working);
  end;
  if (Voltage = nil) or (Highlimit = nil) or (LowLimit = nil) then
    raise Exception.Create('G100 requires all parameters V R H L');
end;


procedure TG100.Execute;
begin
  ECMRequest.OnTimeVoltage:= Named.GetAsDouble(Voltage);
  if RampTime <> nil then
    ECMRequest.OnTimeVoltageRampTime:= Named.GetAsDouble(Ramptime)
  else
    ECMRequest.OnTimeVoltageRampTime:= 0;

  ECMRequest.OnTimeVoltageHighLimit:= Named.GetAsDouble(HighLimit);
  ECMRequest.OnTimeVoltageLowLimit:= Named.GetAsDouble(LowLimit);
end;


initialization
  TAbstractNCExtGCode.AddNCExtGCode('100', TG100);
end.
