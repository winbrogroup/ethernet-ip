unit G105;

interface

uses Sysutils, AbstractExtGCode, INamedInterface, NamedPlugin, CncTypes, ECMState;

type
  TG105 = class (TAbstractNCExtGCode)
  private
    Voltage: TTagRef;
  public
    procedure Compile(Compiler: INCCompiler; Line: WideString); override; stdcall;
    procedure Execute; override; stdcall;
  end;


implementation


{ TTestGCode }

procedure TG105.Compile(Compiler: INCCompiler; Line: WideString);
var Working, Piece: string;
    Desig: Char;
    Tag: TTagRef;
begin
  Working:= Trim(Line);
  Piece:= CncTypes.ReadDelimitedParenthesis(Working, ' ');
  Trim(Working);
  while Piece <> '' do begin
    if Length(Piece) > 1 then begin
      Desig:= Piece[1];
      Delete(Piece, 1, 1);
      Tag:= Compiler.ResolveNCVariable(Piece);
      case Desig of
        'V': Voltage:= Tag;
      else
        raise Exception.CreateFmt('Unexpected parameter %s', [Desig]);
      end;
    end else
      raise Exception.Create('Invalid parameter format');

    Piece:= CncTypes.ReadDelimitedParenthesis(Working, ' ');
    Trim(Working);
  end;
  if Voltage = nil then
    raise Exception.Create('G105 requires parameter V');
end;

procedure TG105.Execute;
begin
  ECMState.ECMRequest.ProtectiveAnodeVoltage:= Named.GetAsDouble(Voltage);
end;

initialization
  TAbstractNCExtGCode.AddNCExtGCode('105', TG105);
end.
