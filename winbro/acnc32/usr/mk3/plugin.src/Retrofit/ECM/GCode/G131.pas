unit G131;

interface

uses Sysutils, AbstractExtGCode, INamedInterface, NamedPlugin, CncTypes, G130, ECMState;

type
  TG131 = class (TG130)
  public
    procedure Execute; override; stdcall;
  end;


implementation


{ TTestGCode }

procedure TG131.Execute;
begin
  ECMRequest.InletUpperTemeratureLimit:= Named.GetAsDouble(A);
  ECMRequest.InletLowerTemeratureLimit:= Named.GetAsDouble(B);
  ECMRequest.OutletUpperTemeratureLimit:= Named.GetAsDouble(C);
  ECMRequest.OutletLowerTemeratureLimit:= Named.GetAsDouble(D);
end;


initialization
  TAbstractNCExtGCode.AddNCExtGCode('131', TG131);
end.
