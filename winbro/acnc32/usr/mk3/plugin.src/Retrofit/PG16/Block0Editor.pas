unit Block0Editor;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, Grids, IFormInterface, ComCtrls, AbstractFormPlugin,
  IStringArray, NamedPlugin, IGridPlugin, AbstractGridPlugin, Common;

type
  spValue = (
    spNone,
    spHv,
    spPlusMinus
  );

  PCheckValue = ^CheckValue;
  CheckValue = record
    ColumnName: string;
    Low: Double;
    High: Double;
    Default: Double;
    Dp: Integer;
    Special: spValue;
  end;

  TPG16Block0Grid = class(TAbstractGridPlugin, IACNC32StringArrayNotify)
  private
    Visible: Boolean;
    Table: IACNC32StringArray;
    procedure SetGridValue(AColumn, Value: WideString);
    function RangeCheckValue(C: PCheckValue; Value, Default: Double): string;
    //function RangeCheckValue(Value, Min, Max, Default: Double; Dp: Integer): string;
  public
    function CanEdit(X, Y : Integer) : Boolean; override; stdcall;
    procedure FinalInstall; override; stdcall;
    procedure Edit(X, Y : Integer; var Req : WideString); override; stdcall;

    procedure CellChange(Table : IACNC32StringArray; Row, Col : Integer); stdcall;
    procedure TableChange(Table : IACNC32StringArray); stdcall;
  end;

implementation

const
  FinalDepthColumn = 'final_depth';
  RamTimerColumn = 'ram_timer';
  BlockLatchColumn = 'block_latch';
  PulsedFlushColumn = 'pulsed_flush';
  HVColumn = 'hv';
  NegativeColumn = 'negative';
  RefeedDepthColumn = 'refeed_depth';
  DepthIncColumn = 'depth_inc';
  DepthLimitColumn = 'depth_limit';
  DepthTimeColumn = 'depth_time';
  PDMColumn = 'pdm';
  CouplingColumn = 'coupling';

const
  RangeCheck: array [1..11] of CheckValue = (
    ( ColumnName: FinalDepthColumn; Low: 0; High: 2; Default: 1; Dp: 2 ),
    ( ColumnName: RamTimerColumn; Low: 0; High: 1; Default: 0; Dp: 0 ),
    ( ColumnName: BlockLatchColumn; Low: 0; High: 1; Default: 0; Dp: 0 ),
    ( ColumnName: PulsedFlushColumn; Low: 0; High: 1; Default: 0; Dp: 0 ),
    ( ColumnName: HVColumn; Low: 100; High: 300; Default: 150; Dp: 0; Special: SpHv ),
    ( ColumnName: NegativeColumn; Low: 0; High: 1; Default: 1; Dp: 0; Special: spPlusMinus ),
    ( ColumnName: RefeedDepthColumn; Low: 0; High: 35; Default: 10; Dp: 2 ),
    ( ColumnName: DepthIncColumn; Low: 0; High: 1; Default: 0; Dp: 2 ),
    ( ColumnName: DepthLimitColumn; Low: 0; High: 30; Default: 0; Dp: 2 ),
    ( ColumnName: DepthTimeColumn; Low: 0; High: 10; Default: 0; Dp: 1 ),
    ( ColumnName: PDMColumn; Low: 0; High: 1; Default: 0; Dp: 0 )
  );

{ TPG16Block0Grid }

procedure TPG16Block0Grid.SetGridValue(AColumn, Value: WideString);
var AValue: string;
    I: Integer;
    V: Double;
begin
  AValue:= Value;

  if CompareText(AColumn, 'negative') = 0 then begin
    if(AValue = '0') or (AValue = '+') then begin
      AValue:= '+';
      Host.UpdateEv(guCell, 1, 6, PChar(AValue));
    end else begin
      AValue:= '-';
      Host.UpdateEv(guCell, 1, 6, PChar(AValue));
    end;
    Exit;
  end;

  for I:= Low(RangeCheck) to High(RangeCheck) do begin
    if CompareText(AColumn, RangeCheck[I].ColumnName) = 0 then begin
      V:= StrToFloatDef(Value, RangeCheck[I].Default);
      AValue:= RangeCheckValue(@RangeCheck[I], V, RangeCheck[I].Default);
      Host.UpdateEv(guCell, 1, I, PChar(AValue));
      Exit;
    end;
  end;

  if CompareText(AColumn, CouplingColumn) = 0 then begin
    AValue:= Bit16Field(Value);
    Host.UpdateEv(guCell, 1, 12, PChar(AValue));
  end;
end;

procedure TPG16Block0Grid.FinalInstall;
begin
  Host.UpdateEv(guCount, 2, 13, nil);
  Host.UpdateEv(guFixed, 1, 1, nil);
  Host.UpdateEv(guVisible, 0, 0, nil);

  Host.UpdateEv(guCell, 0, 0, PChar('Parameter'));
  Host.UpdateEv(guCell, 1, 0, PChar('Value'));

  Host.UpdateEv(guCell, 0, 1, PChar('Final Depth (mm)'));
  Host.UpdateEv(guCell, 0, 2, PChar('Ram Timer (s)'));
  Host.UpdateEv(guCell, 0, 3, PChar('Block Latch'));
  Host.UpdateEv(guCell, 0, 4, PChar('Pulsed Flush'));
  Host.UpdateEv(guCell, 0, 5, PChar('High Volts (V)'));
  Host.UpdateEv(guCell, 0, 6, PChar('Polarity'));
  Host.UpdateEv(guCell, 0, 7, PChar('Refeed Depth'));
  Host.UpdateEv(guCell, 0, 8, PChar('Depth Inc (mm)'));
  Host.UpdateEv(guCell, 0, 9, PChar('Depth Limit (mm)'));
  Host.UpdateEv(guCell, 0, 10,PChar('Depth Time (s)'));
  Host.UpdateEv(guCell, 0, 11,PChar('PDM Select'));

  Host.UpdateEv(guCell, 0, 12, PChar('Coupling'));
  if StringArraySet.UseArray('PG16', Self, Table) = AAR_OK then begin
  end;
end;

function TPG16Block0Grid.CanEdit(X, Y: Integer): Boolean;
begin
  Result:= True;
end;

procedure TPG16Block0Grid.CellChange(Table: IACNC32StringArray; Row, Col: Integer);
var AName, AValue: WideString;
begin
  Table.FieldNameAtIndex(Col, AName);
  Table.GetValue(0, Col, AValue);
  SetGridValue(AName, AValue);
end;

procedure TPG16Block0Grid.TableChange(Table: IACNC32StringArray);
var Col: Integer;
    AName, AValue: WideString;
begin
  if Table.RecordCount = 0 then begin
    if Visible then begin
      Host.UpdateEv(guVisible, 0, 0, nil);
      Host.UpdateEv(guTitle, 0, 0, PChar('No program loaded'));
      Host.UpdateEv(guTitle, 0, 0, PChar('EDM Inactive'));
      Visible := False;
    end;
    Exit;
  end;
  Host.UpdateEv(guCount, 2, 13, nil);

  for Col:= 0 to Table.FieldCount - 1 do begin
    Table.FieldNameAtIndex(Col, AName);
    Table.GetValue(0, Col, AValue);
    SetGridValue(AName, AValue);
  end;

  if not Visible then begin
    Host.UpdateEv(guVisible, 1, 0, nil);
    Visible := True;
  end
end;

function TPG16Block0Grid.RangeCheckValue(C: PCheckValue; Value, Default: Double): string;
begin
  if Value < C.Low then
    Value:= Default;
  if Value > C.High then
    Value:= Default;

  if C.Special = spPlusMinus then begin
    if Value = 0 then
      Result:= '+'
    else
      Result:= '-';
  end else
  if C.Special = spHv then begin
    Value:= (Value - 100) / 50;
    Value:= Round(Value);
    if Value > 4 then
      Value:= 4;
    if Value < 0 then
      Value:= 0;

    Value:= (Value * 50) + 100;
    Result:= Format('%.0f', [Value]);
  end
  else begin
    Result:= Format('%.' + IntToStr(C.Dp) + 'f', [Value]);
  end;
end;

procedure TPG16Block0Grid.Edit(X, Y: Integer; var Req: WideString);
var DValue: Double;
begin
  case Y of
    Low(RangeCheck)..High(RangeCheck): begin
      if (Req = '-') then
        Req:= '1';
      if (Req = '+') then
        Req:= '0';

      DValue:= StrToFloatDef(Req, -9999999);
      Req:= RangeCheckValue(@RangeCheck[Y], DValue, RangeCheck[Y].Default);
      Table.SetFieldValue(0, RangeCheck[Y].ColumnName, Req);
    end;

    12: begin // coupling
      Req:= Bit16Field(Req);
      Table.SetFieldValue(0, CouplingColumn, Req);
    end;
  end;
end;

initialization
  TPG16Block0Grid.AddPlugin('PG16Block0', TPG16Block0Grid);
end.
