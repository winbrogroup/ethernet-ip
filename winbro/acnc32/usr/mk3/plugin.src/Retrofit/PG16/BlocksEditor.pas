unit BlocksEditor;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, Grids, IFormInterface, ComCtrls, AbstractFormPlugin,
  IStringArray, NamedPlugin, IGridPlugin, AbstractGridPlugin, Common;

type
  TPG16EDMDisplay = class(TAbstractGridPlugin, IACNC32StringArrayNotify)
  private
    Visible: Boolean;
    Table: IACNC32StringArray;
    procedure SetGridValue(Row: Integer; AColumn, Value: WideString);
    function RangeCheckValue(Value, Min, Max, Default: Double; Dp: Integer): string;
  public
    function CanEdit(X, Y : Integer) : Boolean; override; stdcall;
    procedure FinalInstall; override; stdcall;
    procedure Edit(X, Y : Integer; var Req : WideString); override; stdcall;

    procedure CellChange(Table : IACNC32StringArray; Row, Col : Integer); stdcall;
    procedure TableChange(Table : IACNC32StringArray); stdcall;
  end;


implementation

uses Methods;

const
  FeedColumn = 'feed';
  ResponseColumn = 'response';
  OnColumn = 'on';
  OffColumn = 'off';
  SuppOnColumn = 'son';
  SuppOffColumn = 'soff';
  DepthColumn = 'depth';
  LGVColumn = 'lgv';
  VibAmpColumn = 'vib_amp';
  RetTimeColumn = 'ret_time';
  DownTimeColumn = 'down_time';
  CurrentColumn = 'current';
  ChannelColumn = 'channel';

type
  CheckValue = record
    ColumnName: string;
    Low: Double;
    High: Double;
    Default: Double;
    Dp: Integer;
  end;

const
  RangeCheck: array [1..12] of CheckValue = (
    ( ColumnName: FeedColumn; Low: 0; High: 100; Default: 40; Dp: 1 ),
    ( ColumnName: ResponseColumn; Low: 0; High: 100; Default: 40; Dp: 1 ),
    ( ColumnName: OnColumn; Low: 10; High: 4095; Default: 20; Dp: 0 ),
    ( ColumnName: OffColumn; Low: 20; High: 4095; Default: 100; Dp: 0 ),
    ( ColumnName: SuppOnColumn; Low: 0; High: 4095; Default: 20; Dp: 0 ),
    ( ColumnName: SuppOffColumn; Low: 0; High: 4095; Default: 1200; Dp: 0 ),
    ( ColumnName: DepthColumn; Low: 0; High: 3; Default: 0.18; Dp: 2 ),
    ( ColumnName: LGVColumn; Low: 0; High: 100; Default: 20; Dp: 1 ),
    ( ColumnName: VibAmpColumn; Low: 0; High: 100; Default: 0; Dp: 1 ),
    ( ColumnName: RetTimeColumn; Low: 0; High: 100; Default: 0; Dp: 1 ),
    ( ColumnName: DownTimeColumn; Low: 0; High: 100; Default: 0; Dp: 1 ),
    ( ColumnName: CurrentColumn; Low: 0; High: 255; Default: 1; Dp: 0 )
  );


{ TPG16EDMDisplay }

procedure TPG16EDMDisplay.FinalInstall; stdcall;
begin
  Host.UpdateEv(guCount, 2, 14, nil);
  Host.UpdateEv(guFixed, 1, 1, nil);
  Host.UpdateEv(guVisible, 0, 0, nil);

  Host.UpdateEv(guCell, 0, 0, PChar('Block'));
  Host.UpdateEv(guCell, 0, 1, PChar('Feed'));
  Host.UpdateEv(guCell, 0, 2, PChar('Resp'));
  Host.UpdateEv(guCell, 0, 3, PChar('On Time (normal)'));
  Host.UpdateEv(guCell, 0, 4, PChar('Off Time (normal)'));
  Host.UpdateEv(guCell, 0, 5, PChar('On Time (suppressed)'));
  Host.UpdateEv(guCell, 0, 6, PChar('Off Time (suppressed)'));
  Host.UpdateEv(guCell, 0, 7, PChar('Depth'));
  Host.UpdateEv(guCell, 0, 8, PChar('LGV'));
  Host.UpdateEv(guCell, 0, 9, PChar('V Amp'));
  Host.UpdateEv(guCell, 0, 10, PChar('Ret.'));
  Host.UpdateEv(guCell, 0, 11, PChar('Adv.'));
  Host.UpdateEv(guCell, 0, 12, PChar(' Current'));

  Host.UpdateEv(guCell, 0, 13, PChar(' Channels'));

  if StringArraySet.UseArray('PG16', Self, Table) = AAR_OK then begin

  end;
end;

procedure TPG16EDMDisplay.CellChange(Table: IACNC32StringArray; Row, Col: Integer);
var AName, AValue: WideString;
begin
  Table.FieldNameAtIndex(Col, AName);
  Table.GetValue(Row, Col, AValue);
  SetGridValue(Row, AName, AValue);
end;

procedure TPG16EDMDisplay.TableChange(Table: IACNC32StringArray);
var Col, Row: Integer;
    AName, AValue: WideString;
begin
  if Table.RecordCount = 0 then begin
    if Visible then begin
      Host.UpdateEv(guVisible, 0, 0, nil);
      Host.UpdateEv(guTitle, 0, 0, PChar('No program loaded'));
      Host.UpdateEv(guTitle, 0, 0, PChar('EDM Inactive'));
      Visible := False;
    end;
    Exit;
  end;
  Host.UpdateEv(guCount, Table.RecordCount + 1, 14, nil);
  Host.UpdateEv(guTitle, 0, 0, PChar('Hole Program: ' + IntToStr(Methods.Current)));

  for Col:= 0 to Table.FieldCount - 1 do begin
    for Row:= 0 to Table.RecordCount - 1 do begin
      Table.FieldNameAtIndex(Col, AName);
      Table.GetValue(Row, Col, AValue);
      SetGridValue(Row, AName, AValue);
    end;
  end;

  if not Visible then begin
    Host.UpdateEv(guVisible, 1, 0, nil);
    Visible := True;
  end
end;

//Block 0 "Final_Depth","ram_timer","block_latch","pulsed_flush","hv","negative","refeed_depth","depth_inc","depth_limit","depth_time","pdm","Coupling"
//Block 1 "Feed","Response","On","Off","son","soff","Depth","LGV","Vib_Amp","Ret_time","Down_Time","Current","Channel"
//6,0,0,0,100,1,0,0,0,0,,0,40,40,30,70,20,100,4,10,0,1,1,15,1


procedure TPG16EDMDisplay.SetGridValue(Row: Integer; AColumn, Value: WideString);
var AValue: string;
    I: Integer;
    V: Double;
const One: PChar = '1';
      Zero: PChar = '0';
begin
  AValue:= Value;
  Host.UpdateEv(guCell, Row + 1, 0, PChar(IntToStr(Row + 1)));

  for I:= Low(RangeCheck) to High(RangeCheck) do begin
    if CompareText(AColumn, RangeCheck[I].ColumnName) = 0 then begin
      V:= StrToFloatDef(Value, RangeCheck[I].Default);
      AValue:= RangeCheckValue(V, RangeCheck[I].Low, RangeCheck[I].High, RangeCheck[I].Default, RangeCheck[I].Dp);
      Host.UpdateEv(guCell, Row + 1, I, PChar(AValue));
    end;
  end;

  if CompareText(AColumn, ChannelColumn) = 0 then begin
    AValue:= Bit16Field(Value);
    Host.UpdateEv(guCell, Row + 1, 13, PChar(AValue));
  end;

end;

function TPG16EDMDisplay.CanEdit(X, Y: Integer): Boolean;
begin
  Result:= True;
end;

function TPG16EDMDisplay.RangeCheckValue(Value, Min, Max, Default: Double; Dp: Integer): string;
begin
  if Value < Min then
    Value:= Default;
  if Value > Max then
    Value:= Default;

  Result:= Format('%.' + IntToStr(Dp) + 'f', [Value]);
end;

procedure TPG16EDMDisplay.Edit(X, Y: Integer; var Req: WideString);
var DValue: Double;
begin
  case Y of
    1..12: begin
      DValue:= StrToFloatDef(Req, -9999999);
      Req:= RangeCheckValue(DValue, RangeCheck[Y].Low, RangeCheck[Y].High, RangeCheck[Y].Default, RangeCheck[Y].Dp);
      Table.SetFieldValue(X - 1, RangeCheck[Y].ColumnName, Req);
    end;

    13: begin // coupling
      Req:= Bit16Field(Req);
      Table.SetFieldValue(X - 1, ChannelColumn, Req);
    end;
  end;
end;

initialization
  TPG16EDMDisplay.AddPlugin('PG16Blocks', TPG16EDMDisplay);
end.
