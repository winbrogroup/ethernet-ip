unit Methods;

interface

uses Classes, SysUtils, AbstractScript, INamedInterface, NamedPlugin,
     IScriptInterface, IStringArray, CncTypes, Dialogs;

type
  TPG16Load = class (TScriptMethod)
  public
    function Name: WideString; override; stdcall;
    function ParameterName(Index: Integer): WideString; override; stdcall;
    function ParameterCount: Integer; override; stdcall;
    procedure Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer); override; stdcall;
  end;

  TPG16Save = class (TScriptMethod)
  public
    function Name: WideString; override; stdcall;
    function ParameterName(Index: Integer): WideString; override; stdcall;
    function ParameterCount: Integer; override; stdcall;
    procedure Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer); override; stdcall;
  end;

  TPG16Select = class (TScriptMethod)
  public
    function ParameterPassing(Index: Integer): Integer; override; stdcall;
    function Name: WideString; override; stdcall;
    function ParameterCount: Integer; override; stdcall;
    procedure Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer); override; stdcall;
    function IsFunction: Boolean; override; stdcall;
  end;

  TPG16Current = class (TScriptMethod)
  public
    function Name: WideString; override; stdcall;
    procedure Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer); override; stdcall;
    function IsFunction: Boolean; override; stdcall;
  end;

  TPG16CouplingString = class(TScriptMethod)
  public
    function ParameterCount: Integer; override; stdcall;
    function Name: WideString; override; stdcall;
    procedure Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer); override; stdcall;
    function IsFunction: Boolean; override; stdcall;
  end;

var
  Current: Integer;

implementation

{ TPG16Load }

function TPG16Load.Name: WideString;
begin
  Result:= 'Load';
end;

function TPG16Load.ParameterCount: Integer;
begin
  Result:= 1;
end;

function TPG16Load.ParameterName(Index: Integer): WideString;
begin
  Result:= 'Index';
end;

procedure TPG16Load.Execute(const Res: Pointer; const Params: array of Pointer;
  ParamCount: Integer);
var Table: IACNC32StringArray;
  Prog, TRes: Integer;
begin
  Prog:= Named.GetAsInteger(Params[0]);

  if StringArraySet.UseArray('PG16', nil, Table) = AAR_OK then begin
    Table.BeginUpdate;
    try
      TRes := Table.ImportFromCSV(DllLocalPath + '\parameters\' + IntToStr(Prog) + '.csv', False, nil);
      if TRes <> AAR_OK then begin
        TRes:= Table.ImportFromCSV(DllLocalPath + '\parameters\default.csv', False, nil);
      end;
      if TRes <> AAR_OK then begin
        Named.SetFaultA('', 'Failed to load default parameters', FaultFatal, nil);
        raise Exception.Create('Failed to load default parameters');
      end;
      Current:= Prog;
    finally
      Table.EndUpdate;
    end;

  end else begin
    Named.SetFaultA('', 'Failed to open PG16 table', FaultFatal, nil);
    raise Exception.Create('Unable to open PG16 table');
  end;
end;

{ TPG16Save }

function TPG16Save.Name: WideString;
begin
  Result:= 'Save';
end;

function TPG16Save.ParameterCount: Integer;
begin
  Result:= 1;
end;

function TPG16Save.ParameterName(Index: Integer): WideString;
begin
  Result:= 'Index';
end;

procedure TPG16Save.Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer);
var Table: IACNC32StringArray;
  Prog, TRes: Integer;
begin
  Prog:= Named.GetAsInteger(Params[0]);

  if StringArraySet.UseArray('PG16', nil, Table) = AAR_OK then begin
    TRes := Table.ExportToCSV(DllLocalPath + '\parameters\' + IntToStr(Prog) + '.csv');
    if TRes <> AAR_OK then begin
      Named.SetFaultA('', 'Failed to save parameters', FaultFatal, nil);
      raise Exception.Create('Failed to save parameters');
    end;
    Current:= Prog;

  end else begin
    Named.SetFaultA('', 'Failed to save PG16 table', FaultFatal, nil);
    raise Exception.Create('Unable to save PG16 table');
  end;
end;

{ TPG16Select }

function TPG16Select.Name: WideString;
begin
  Result:= 'SelectValue';
end;

function TPG16Select.ParameterCount: Integer;
begin
  Result:= 1;
end;

procedure TPG16Select.Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer);
var Value: string;
begin
  if InputQuery('Enter Number', 'Program Number', Value) then begin
     Named.SetAsInteger(Res, 1);
     Named.SetAsString(Params[0], PChar(Value));
     Exit;
  end;

  Named.SetAsInteger(Res, 0);
end;

function TPG16Select.IsFunction: Boolean;
begin
  Result:= True;
end;

function TPG16Select.ParameterPassing(Index: Integer): Integer;
begin
  Result:= PassByReference;
end;

{ TPG16Current }

function TPG16Current.Name: WideString;
begin
  Result:= 'Current';
end;

function TPG16Current.IsFunction: Boolean;
begin
  Result:= True;
end;

procedure TPG16Current.Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer);
begin
  Named.SetAsInteger(Res, Current);
end;

{ TPG16CouplingString }

function TPG16CouplingString.Name: WideString;
begin
  Result:= 'CouplingValue';
end;

function TPG16CouplingString.ParameterCount: Integer;
begin
  Result:= 1;
end;

function TPG16CouplingString.IsFunction: Boolean;
begin
  Result:= True;
end;

procedure TPG16CouplingString.Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer);
var I: Integer;
    A: Integer;
    Q: string;
    TChar: array[0..255] of Char;
begin
  A:= 0;
  Named.GetAsString(Params[0], TChar, 255);
  Q:= TChar;
  for I:= 1 to 16 do
    if I <= Length(Q) then
      if Q[I] = '1' then
        A:= A + (1 shl (I - 1));

  Named.SetAsInteger(Res, A);
end;

initialization
  TDefaultScriptLibrary.SetName('PG16');
  TDefaultScriptLibrary.AddMethod(TPG16Load);
  TDefaultScriptLibrary.AddMethod(TPG16Save);
  TDefaultScriptLibrary.AddMethod(TPG16Select);
  TDefaultScriptLibrary.AddMethod(TPG16Current);
  TDefaultScriptLibrary.AddMethod(TPG16CouplingString);
end.
