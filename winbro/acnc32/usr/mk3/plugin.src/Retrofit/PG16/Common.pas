unit Common;

interface

uses SysUtils;

function Bit16Field(Req: string): string;

implementation

function Bit16Field(Req: string): string;
var I, C: Integer;
begin
  Req:= Trim(Req);
  if Req = '+' then
    Req:= '1111111111111111';

  if (Length(Req) > 1) and (Req[1] = '+') then begin
    C:= StrToIntDef(Copy(Req, 2, 100), 16);
    Req:= '';
    for I:= 1 to C do begin
      Req:= Req + '1';
    end;
  end;

  Result:= '';
  for I:= 1 to 16 do begin
    if I <= Length(Req) then begin
      if Req[I] = '1' then
        Result:= Result + '1'
      else
        Result:= Result + '0';
    end else
      Result:= Result + '0';
  end;
end;

end.
