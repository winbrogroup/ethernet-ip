object CapacitanceEditor: TCapacitanceEditor
  Left = 181
  Top = 207
  Width = 670
  Height = 238
  Caption = 'Capacitance Select'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'MS Sans Serif'
  Font.Style = [fsBold]
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 16
  object HeadButton1: TSpeedButton
    Left = 16
    Top = 10
    Width = 120
    Height = 60
    AllowAllUp = True
    GroupIndex = 1
    Caption = 'Head Cap 1'
  end
  object HeadButton2: TSpeedButton
    Left = 144
    Top = 10
    Width = 120
    Height = 60
    AllowAllUp = True
    GroupIndex = 2
    Caption = 'Head Cap 2'
  end
  object HeadButton3: TSpeedButton
    Left = 272
    Top = 10
    Width = 120
    Height = 60
    AllowAllUp = True
    GroupIndex = 3
    Caption = 'Head Cap 3'
  end
  object HeadButton4: TSpeedButton
    Left = 400
    Top = 10
    Width = 120
    Height = 60
    AllowAllUp = True
    GroupIndex = 4
    Caption = 'Head Cap 4'
  end
  object BoardButton1: TSpeedButton
    Left = 224
    Top = 88
    Width = 217
    Height = 60
    AllowAllUp = True
    GroupIndex = 6
    Caption = 'Board Capacitance'
  end
  object HeadButton5: TSpeedButton
    Left = 528
    Top = 10
    Width = 120
    Height = 60
    AllowAllUp = True
    GroupIndex = 5
    Caption = 'Head Cap 5'
  end
  object Panel1: TPanel
    Left = 0
    Top = 170
    Width = 662
    Height = 41
    Align = alBottom
    TabOrder = 0
    object BitBtnOk: TBitBtn
      Left = 24
      Top = 2
      Width = 85
      Height = 36
      TabOrder = 0
      Kind = bkOK
    end
    object BitBtnApply: TBitBtn
      Left = 128
      Top = 2
      Width = 85
      Height = 36
      Caption = '&Apply'
      TabOrder = 1
      OnClick = BitBtnApplyClick
      Glyph.Data = {
        DE010000424DDE01000000000000760000002800000024000000120000000100
        0400000000006801000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333444444
        33333333333F8888883F33330000324334222222443333388F3833333388F333
        000032244222222222433338F8833FFFFF338F3300003222222AAAAA22243338
        F333F88888F338F30000322222A33333A2224338F33F8333338F338F00003222
        223333333A224338F33833333338F38F00003222222333333A444338FFFF8F33
        3338888300003AAAAAAA33333333333888888833333333330000333333333333
        333333333333333333FFFFFF000033333333333344444433FFFF333333888888
        00003A444333333A22222438888F333338F3333800003A2243333333A2222438
        F38F333333833338000033A224333334422224338338FFFFF8833338000033A2
        22444442222224338F3388888333FF380000333A2222222222AA243338FF3333
        33FF88F800003333AA222222AA33A3333388FFFFFF8833830000333333AAAAAA
        3333333333338888883333330000333333333333333333333333333333333333
        0000}
      NumGlyphs = 2
    end
    object BitBtnCancel: TBitBtn
      Left = 232
      Top = 2
      Width = 85
      Height = 36
      TabOrder = 2
      Kind = bkCancel
    end
  end
end
