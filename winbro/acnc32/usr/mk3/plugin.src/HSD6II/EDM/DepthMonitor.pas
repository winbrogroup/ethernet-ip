unit DepthMonitor;

interface

uses Classes, NamedPlugin;

type
  TDepthMonitor = class
  private
    List: TList;
    CurrentDepth: Integer;
  public
    constructor Create;
    destructor Destroy; override;
    procedure AddMonitorPoint(AMonitorPoint: Integer);
    procedure ClearAll;
    procedure Monitor;
  end;

implementation

uses EDMPlc;

{ TDepthMonitor
  Possibility that this is asociated with the signatures because it creaes a list that cna used for
  indicating the depth of the process.}

// Called only during block processing
procedure TDepthMonitor.Monitor;
begin
  if EDMHead.FirstSpark and (List.Count > 0) then begin
    if EDMHead.GetNativeDepth > CurrentDepth then
      List.Delete(0);
    if List.Count > 0 then begin
      CurrentDepth:= Integer(List[0]);
      EDMHead.SetFinalDepth(CurrentDepth + 100);
    end;
  end else begin
    CurrentDepth:= MAXINT;
    EDMHead.SetFinalDepth(CurrentDepth);
  end;
end;

procedure TDepthMonitor.AddMonitorPoint(AMonitorPoint: Integer);
var I: Integer;
begin
  for I:= 0 to List.Count - 1 do begin
    if Integer(List[I]) = AMonitorPoint then
      Exit;

    if Integer(List[I]) > AMonitorPoint then begin
      List.Insert(I, Pointer(AMonitorPoint));
      Exit;
    end;
  end;
  List.Add(Pointer(AMonitorPoint));
end;

procedure TDepthMonitor.ClearAll;
begin
  List.Clear;
end;

constructor TDepthMonitor.Create;
begin
  List:= TList.Create;
end;

destructor TDepthMonitor.Destroy;
begin
  List.Free;
  inherited;
end;

end.
