unit Methods;

interface

uses Classes, SysUtils, AbstractScript, INamedInterface, NamedPlugin,
     IScriptInterface, IStringArray, EDMPlc, EDMReaderWriter, AbstractOPPPlugin;

implementation

type
  TEDMExternalControl = class(TScriptMethod)
    function Name: WideString; override; stdcall;
    procedure Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer); override; stdcall;
    procedure Reset; override; stdcall;
  end;

  TEDMInternalControl = class(TScriptMethod)
    function Name: WideString; override; stdcall;
    procedure Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer); override; stdcall;
  end;

  TEDMOnOff = class(TScriptMethod)
    function Name: WideString; override; stdcall;
    procedure Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer); override; stdcall;
    function ParameterCount: Integer; override; stdcall;
  end;

  TEDMServo = class(TScriptMethod)
    function Name: WideString; override; stdcall;
    procedure Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer); override; stdcall;
    function ParameterCount: Integer; override; stdcall;
  end;

  TEDMGapVolts = class(TScriptMethod)
    function Name: WideString; override; stdcall;
    procedure Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer); override; stdcall;
    function ParameterCount: Integer; override; stdcall;
  end;

  TEDMHighVolts = class(TScriptMethod)
    function Name: WideString; override; stdcall;
    procedure Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer); override; stdcall;
    function ParameterCount: Integer; override; stdcall;
  end;

  TEDMPeak = class(TScriptMethod)
    function Name: WideString; override; stdcall;
    procedure Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer); override; stdcall;
    function ParameterCount: Integer; override; stdcall;
  end;

  TEDMPolarity = class(TScriptMethod)
    function Name: WideString; override; stdcall;
    procedure Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer); override; stdcall;
    function ParameterCount: Integer; override; stdcall;
  end;

  TEDMASV = class(TScriptMethod)
    function Name: WideString; override; stdcall;
    procedure Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer); override; stdcall;
    function ParameterCount: Integer; override; stdcall;
  end;

  TEDMPulseBoards = class(TScriptMethod)
    function Name: WideString; override; stdcall;
    procedure Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer); override; stdcall;
    function ParameterCount: Integer; override; stdcall;
  end;

  TEDMCoupling = class(TScriptMethod)
    function Name: WideString; override; stdcall;
    procedure Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer); override; stdcall;
    function ParameterCount: Integer; override; stdcall;
  end;

  TEDMCapacitance = class(TScriptMethod)
    function Name: WideString; override; stdcall;
    procedure Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer); override; stdcall;
    function ParameterCount: Integer; override; stdcall;
  end;

  TEDMBusVoltage = class(TScriptMethod)
    function Name: WideString; override; stdcall;
    procedure Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer); override; stdcall;
    function ParameterCount: Integer; override; stdcall;
  end;

  TEDMActivate = class(TScriptMethod)
    function Name: WideString; override; stdcall;
    procedure Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer); override; stdcall;
  end;

  TMethodManager = class
  public
    ExternalProgram: TEDMProgram;
    EDMSection: TEDMSection;
    procedure NewProgram(Sender : TObject);
    constructor Create;
  end;

  var
    MethodManager: TMethodManager;

procedure CheckExternalControl;
begin
  if Assigned(EDMHead) then
    if pepExternalControl in EDMHead.Values then
      Exit;

  raise Exception.Create('Unable to change parameters without being in External control');
end;
{***********************************************************************************************************
TEDMExternalControl
}

function TEDMExternalControl.Name: WideString;
begin
  Result:= 'ExternalControl';
end;

procedure TEDMExternalControl.Reset;
begin
  if Assigned(EDMHead) then begin
    EDMHead.SwitchToInternalControl;
  end;
end;

procedure TEDMExternalControl.Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer);
begin
  if Assigned(EDMHead) then begin
    if Assigned(EDMHead.ActiveProgram) then begin
      if not (pepExternalControl in EDMHead.Values) then begin
        //EDMHead.SwitchToExternalControl(ExternalProgram);
        MethodManager.ExternalProgram:= MethodManager.EDMSection.FindProgram(EDMHead.ActiveProgram.ID);
        if not Assigned(MethodManager.ExternalProgram) then begin
          raise Exception.Create('Failed to locate valid program');
        end;
        EDMHead.SwitchToExternalControl(MethodManager.ExternalProgram);
      end;
    end;
  end;
end;

{**********************************************************************************************************
 TEDMInternalControl
}

function TEDMInternalControl.Name: WideString;
begin
  Result:= 'InternalControl';
end;

procedure TEDMInternalControl.Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer);
begin
  if Assigned(EDMHead) then begin
    EDMHead.SwitchToInternalControl;
  end;
end;

{***********************************************************************************************************
TEDMOnOff
}
function TEDMOnOff.Name: WideString;
begin
  Result:= 'OnOff';
end;

function TEDMOnOff.ParameterCount: Integer;
begin
  Result:= 4;
end;

procedure TEDMOnOff.Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer);
var Buffer: array [0..255] of Char;
    Tmp: string;
begin
  Named.GetAsString(Params[0], Buffer, 255);
  Tmp:= Buffer;
  MethodManager.ExternalProgram.Blocks[0].OnTime:= Round(StrToFloatDef(Buffer, 4) * 1000.0);
  Named.GetAsString(Params[1], Buffer, 255);
  MethodManager.ExternalProgram.Blocks[0].OffTime:= Round(StrToFloatDef(Buffer, 4) * 1000.0);
  Named.GetAsString(Params[2], Buffer, 255);
  MethodManager.ExternalProgram.Blocks[0].SuppOnTime:= Round(StrToFloatDef(Buffer, 4) * 1000.0);
  Named.GetAsString(Params[3], Buffer, 255);
  MethodManager.ExternalProgram.Blocks[0].SuppOffTime:= Round(StrToFloatDef(Buffer, 4) * 1000.0);
end;

{************************************************************************************************************
 TEDMServo
}
function TEDMServo.Name: WideString;
begin
  Result:= 'Servo';
end;

function TEDMServo.ParameterCount: Integer;
begin
  Result:= 1;
end;

procedure TEDMServo.Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer);
var Buffer: array [0..255] of Char;
begin
  CheckExternalControl;
  Named.GetAsString(Params[0], Buffer, 255);
  MethodManager.ExternalProgram.Blocks[0].Servo:= StrToFloatDef(Buffer, 1) * 1000;
end;

{ TEDMGapVolts }

function TEDMGapVolts.Name: WideString;
begin
  Result:= 'GapVolts';
end;

function TEDMGapVolts.ParameterCount: Integer;
begin
  Result:= 1;
end;

procedure TEDMGapVolts.Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer);
var Buffer: array [0..255] of Char;
begin
  CheckExternalControl;
  Named.GetAsString(Params[0], Buffer, 255);
  MethodManager.ExternalProgram.Blocks[0].GapVolts:= StrToFloatDef(Buffer, 30);
end;

{ TEDMHighVolts }

function TEDMHighVolts.Name: WideString;
begin
  Result:= 'HighVolts';
end;

function TEDMHighVolts.ParameterCount: Integer;
begin
  Result:= 1;
end;

procedure TEDMHighVolts.Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer);
var Buffer: array [0..255] of Char;
begin
  CheckExternalControl;
  Named.GetAsString(Params[0], Buffer, 255);
  MethodManager.ExternalProgram.Blocks[0].HighVoltage:= StrToIntDef(Buffer, 100);
end;

{ TEDMPeak }

function TEDMPeak.Name: WideString;
begin
  Result:= 'Peak';
end;

function TEDMPeak.ParameterCount: Integer;
begin
  Result:= 1;
end;

procedure TEDMPeak.Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer);
var Buffer: array [0..255] of Char;
begin
  CheckExternalControl;
  Named.GetAsString(Params[0], Buffer, 255);
  MethodManager.ExternalProgram.Blocks[0].Peak:= StrToIntDef(Buffer, 1);
end;

{ TEDMPolarity }

function TEDMPolarity.Name: WideString;
begin
  Result:= 'Polarity';
end;

function TEDMPolarity.ParameterCount: Integer;
begin
  Result:= 1;
end;

procedure TEDMPolarity.Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer);
var Buffer: array [0..255] of Char;
    Tmp: string;
begin
  CheckExternalControl;
  Named.GetAsString(Params[0], Buffer, 255);
  Tmp:= Buffer;
  if Tmp = '+' then
    MethodManager.ExternalProgram.Blocks[0].Negative:= False
  else
    MethodManager.ExternalProgram.Blocks[0].Negative:= True
end;

{ TEDMASV }

function TEDMASV.Name: WideString;
begin
  Result:= 'ASV';
end;

function TEDMASV.ParameterCount: Integer;
begin
  Result:= 1;
end;

procedure TEDMASV.Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer);
var Buffer: array [0..255] of Char;
begin
  CheckExternalControl;
  Named.GetAsString(Params[0], Buffer, 255);
  MethodManager.ExternalProgram.Blocks[0].ASV:= StrToFloatDef(Buffer, 0);
end;

{ TEDMPulseBoards }

function TEDMPulseBoards.Name: WideString;
begin
  Result:= 'PulseBoards';
end;

function TEDMPulseBoards.ParameterCount: Integer;
begin
  Result:= 1;
end;

procedure TEDMPulseBoards.Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer);
var Buffer: array [0..255] of Char;
begin
  CheckExternalControl;
  Named.GetAsString(Params[0], Buffer, 255);
  MethodManager.ExternalProgram.Blocks[0].PulseBoards:= StrToIntDef(Buffer, 1);
end;

{ TEDMCoupling }

function TEDMCoupling.Name: WideString;
begin
  Result:= 'Coupling';
end;

function TEDMCoupling.ParameterCount: Integer;
begin
  Result:= 1;
end;

procedure TEDMCoupling.Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer);
var Buffer: array [0..255] of Char;
begin
  CheckExternalControl;
  Named.GetAsString(Params[0], Buffer, 255);
  MethodManager.ExternalProgram.Blocks[0].Coupling:= StrToIntDef(Buffer, 1);
end;

{ TEDMCapacitance }

function TEDMCapacitance.Name: WideString;
begin
  Result:= 'Capacitance';
end;

function TEDMCapacitance.ParameterCount: Integer;
begin
  Result:= 1;
end;

procedure TEDMCapacitance.Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer);
var Buffer: array [0..255] of Char;
begin
  CheckExternalControl;
  Named.GetAsString(Params[0], Buffer, 255);
  MethodManager.ExternalProgram.Blocks[0].Capacitance:= StrToIntDef(Buffer, 0);
end;

{ TEDMBusVoltage }

function TEDMBusVoltage.Name: WideString;
begin
  Result:= 'BusVoltage';
end;

function TEDMBusVoltage.ParameterCount: Integer;
begin
  Result:= 1;
end;

procedure TEDMBusVoltage.Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer);
begin
  CheckExternalControl;
  MethodManager.ExternalProgram.Blocks[0].BusVoltage:= Named.GetAsInteger(Params[0]);
end;

{ TEDMActivate }

function TEDMActivate.Name: WideString;
begin
  Result:= 'Activate';
end;

procedure TEDMActivate.Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer);
var Count: Integer;
begin
  CheckExternalControl;
  Count:= 0;
  while EDMHead.LPE577UpdateStage <> 0 do begin
    Sleep(10);
    Inc(Count);
    if Count > 500 then begin
      raise Exception.Create('EDM.Execute(): Failed to handshake parameters to hardware');
    end;
  end;
  MethodManager.ExternalProgram.Verify(EDMHead.ActiveBlock);
  MethodManager.ExternalProgram.Blocks[EDMHead.ActiveBlock].FUpdateRequired:= True;
end;

{ TMethodManager
  Creates or  uses the data in a program under the section for the EDM parameters }

constructor TMethodManager.Create;
begin
  EDMSection:= TEDMSection.Create;
  TAbstractOPPPluginConsumer.AddCallback(HSD6IISectionName, NewProgram);
end;

procedure TMethodManager.NewProgram(Sender: TObject);
begin
  EDMSection.Clear;
  EDMSection.Text := EDMConsumer.EDMSection.Text;
  EDMSection.ReadFromStrings;
end;

initialization
  MethodManager:= TMethodManager.Create;

  TDefaultScriptLibrary.SetName('EDM');
  TDefaultScriptLibrary.AddMethod(TEDMExternalControl);
  TDefaultScriptLibrary.AddMethod(TEDMInternalControl);
  TDefaultScriptLibrary.AddMethod(TEDMOnOff);
  TDefaultScriptLibrary.AddMethod(TEDMServo);
  TDefaultScriptLibrary.AddMethod(TEDMGapVolts);
  TDefaultScriptLibrary.AddMethod(TEDMHighVolts);
  TDefaultScriptLibrary.AddMethod(TEDMPeak);
  TDefaultScriptLibrary.AddMethod(TEDMPolarity);
  TDefaultScriptLibrary.AddMethod(TEDMASV);
  TDefaultScriptLibrary.AddMethod(TEDMPulseBoards);
  TDefaultScriptLibrary.AddMethod(TEDMCoupling);
  TDefaultScriptLibrary.AddMethod(TEDMCapacitance);
  TDefaultScriptLibrary.AddMethod(TEDMBusVoltage);
  TDefaultScriptLibrary.AddMethod(TEDMActivate);
end.
