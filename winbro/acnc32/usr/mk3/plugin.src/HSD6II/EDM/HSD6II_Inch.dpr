library HSD6II_Inch;

uses
  EDMEditor in 'EDMEditor.pas' {EDMEditorForm},
  EDMGrid in 'EDMGrid.pas',
  EDMPlc in 'EDMPlc.pas',
  IDetectGrid in 'IDetectGrid.pas',
  PulseBoardForm in 'PulseBoardForm.pas' {PulseBoardEditor},
  DutyCycleEditor in 'DutyCycleEditor.pas' {DutyCycleForm},
  VIPEditor in 'VIPEditor.pas' {VIPForm},
  EDMReaderWriter in '..\lib\EDMReaderWriter.pas',
  LPE482 in '..\lib\LPE482.pas',
  LPE577 in '..\lib\LPE577.pas',
  PmacServo in '..\lib\PmacServo.pas',
  Tokenizer in '..\lib\Tokenizer.pas',
  AbstractGridPlugin in '..\..\sdk1.0\AbstractGridPlugin.pas',
  AbstractOPPPlugin in '..\..\sdk1.0\AbstractOPPPlugin.pas',
  AbstractPlcPlugin in '..\..\sdk1.0\AbstractPlcPlugin.pas',
  NamedPlugin in '..\..\sdk1.0\NamedPlugin.pas',
  PluginInterface in '..\..\sdk1.0\PluginInterface.pas',
  GeneralEditor in 'GeneralEditor.pas' {NumericEditorForm},
  CapacitanceEditForm in 'CapacitanceEditForm.pas' {CapacitanceEditor},
  SimpleMeter in 'SimpleMeter.pas',
  WidgetTypes in '..\..\sdk1.0\WidgetTypes.pas',
  WidgetInterface in '..\..\sdk1.0\WidgetInterface.pas',
  IncControl in 'IncControl.pas',
  AbstractOCTPlugin in '..\..\sdk1.0\AbstractOCTPlugin.pas',
  IFSDServer in '..\..\sdk1.0\IFSDServer.pas',
  INamedInterface in '..\..\sdk1.0\INamedInterface.pas';

{$R *.RES}

function ACNC32PluginRevision : Cardinal; stdcall;
begin
  Result := $01000000;
end;

exports
  ACNC32PluginRevision;

begin

end.
