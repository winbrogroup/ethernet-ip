unit EDMOptions;
{ This unit has a display function and a storage function for the EDM grid data
  The display function displays the data onto the grid and the storage functions
  stores the data to disk.
  It is also used to input any additional limits from the EDMoptions.ini file the options available are
  MAxpressure, Minpressure, number of boards, Number of capaciors, Current and depth.  }
interface

uses SysUtils, NamedPlugin, IniFiles, EDMReaderWriter;

type
  TEDMOptions = class
  private
    FProgrammableHP: Boolean;
    MaxPressure : Double;
    MinPressure: Double;
    constructor Create;
  public
    Boards: Integer;
    Capacitors: String;
    MaxDepth: Integer;
    MaxCurrent: Integer;
    function MaxCapacitorBitField: Integer;

    function GTCapacitorBox: Boolean;

    function DisplayDepth(ADepth: Integer): string;
    function StorageDepth(AValue: string; ADefault: Integer): Integer;

    function DisplayTime(AValue: Integer): string;
    function StorageTime(AValue: string; const ADefault: Integer): Integer;

    function DisplayServo(AValue: Double): string;
    function StorageServo(AValue: string; ADefault: Double): Double;

    function DisplayHP(AValue: Double): string;
    function StorageHP(AValue: string; const ADefault: Double): Double;

    function ProgrammableHP: Boolean;

    function GetMaxPressure: Double;
    function GetMinPressure: Double;
    function RangeCheckHP(Value: Double): Double;


    class function GetInstance: TEDMOptions;
  end;

implementation

var Instance: TEDMOptions;
{ TEDMOptions }

constructor TEDMOptions.Create;
var Ini: TIniFile;
begin
  Ini:= TIniFile.Create(DllProfilePath + 'EDMOptions.ini');
  try
    Boards:= Ini.ReadInteger('global', 'Boards', 6);
    Capacitors:= Ini.ReadString('global', 'CapacitorBox', 'HSD');
    MaxDepth:= Ini.ReadInteger('global', 'MaxDepth', 100000);
    MaxCurrent:= Ini.ReadInteger('global', 'MaxCurrent', 16);
    MinPressure:= Ini.ReadFloat('global', 'MinPressure', 34);
    MaxPressure:= Ini.ReadFloat('global', 'MaxPressure', 68);
    FProgrammableHP:= True;
  finally
    Ini.Free;
  end;
end;

function TEDMOptions.MaxCapacitorBitField: Integer;
begin

end;

function TEDMOptions.StorageDepth(AValue: string; ADefault: Integer): Integer;
var Tmp: Double;
begin
  Tmp := StrToFloatDef(AValue, ADefault / 1000);
  Result:= Round(Tmp * 1000 * MetricToInch);
end;

function TEDMOptions.DisplayDepth(ADepth: Integer): string;
begin
  Result:= Format('%0.3f', [ADepth / (MetricToInch * 1000.0)])
end;


function TEDMOptions.StorageHP(AValue: string; const ADefault: Double): Double;
var Tmp: Double;
begin
  Tmp := StrToFloatDef(AValue, ADefault);
  Result:= RangeCheckHP(Tmp / BarToPsi);
end;

function TEDMOptions.DisplayHP(AValue: Double): string;
begin
  Result:= Format('%0.1f', [AValue * BarToPsi]);
end;


class function TEDMOptions.getInstance: TEDMOptions;
begin
  if Instance = nil then
    Instance:= TEDMOptions.Create;

  Result:= Instance;
end;

function TEDMOptions.GTCapacitorBox: Boolean;
begin
  Result:= Capacitors = 'GT';
end;


function TEDMOptions.StorageTime(AValue: string; const ADefault: Integer): Integer;
var Tmp: Double;
begin
  Tmp := StrToFloatDef(AValue, ADefault / 1000);
  Result:= Round(Tmp * 1000);
end;

function TEDMOptions.DisplayTime(AValue: Integer): string;
begin
  Result:= Format('%.2f', [AValue / 1000]);
end;

function TEDMOptions.StorageServo(AValue: string; ADefault: Double): Double;
var Tmp: Double;
begin
  Tmp := StrToFloatDef(AValue, ADefault / 1000);
  Result:= Round(Tmp * 1000);
end;

function TEDMOptions.DisplayServo(AValue: Double): string;
begin
  Result:= Format('%.3f', [AValue / 1000]);
end;

function TEDMOptions.ProgrammableHP: Boolean;
begin
  Result:= FProgrammableHP;
end;

function TEDMOptions.GetMaxPressure: Double;
begin
  Result:= MaxPressure;
end;

function TEDMOptions.GetMinPressure: Double;
begin
  Result:= MinPressure;
end;

function TEDMOptions.RangeCheckHP(Value: Double): Double;
begin
  if Value > MaxPressure then
    Value:= MaxPressure;
  if Value < MinPressure then
    Value:= MinPressure;

  Result:= Value;
end;

end.
