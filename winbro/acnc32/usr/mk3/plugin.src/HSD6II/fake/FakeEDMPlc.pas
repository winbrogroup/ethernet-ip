unit FakeEDMPlc;

interface

uses
  Classes, AbstractPlcPlugin, SysUtils, PmacServo, LPE577, LPE482,
  EDMReaderWriter, CNCTypes;


type
  TPlcFakeHeadProperty = (
    fepEnable,
    fepShort,
    fepOpenGap
  );

  TPlcFakeHeadProperties = set of TPlcFakeHeadProperty;


  const
    H1_ENC_SCALING = 1;
    SERVO_FREQ = 50;
    RETRACT_SPEED = 500;
    DesiredPosition = 0;
    SLIC_INTEGRATION_COUNT = 20;
    VELOCITY_INTEGRATION = 20;

type
  TFakePmacServoPlcItem = class(TAbstractPlcPlugin)
  private
    Inputs : TPlcFakeHeadProperties;
    Pmac : TPmacDevice;
    LPE577 : T577Device;
    LPE482 : T482Device;
    Debug : string;

    H1_GAP : Double;
    H1_POSITION_M : Integer;
    H1_ENC_OFFSET : Integer;
    H1_VOLTAGE_M : Integer;
    H1_SERVO_ON : Boolean;
    H1_STANDOFF : Double;
    H1_FSPOS_P : Integer;
    H1_ENC_INC_ADJUST : Double;
    H1_SHORT_COUNTER : Integer;
    H1_DCM_POSITION : Integer;
    H1_HIC : Double;
    H1_DUPENC_M : Double;
    H1_POS : Double;
    H1_VOLTS : Double;
    PLC0_SPEED : Double;
    PLC0_PSERVO : Double;
    H1_SPEED_RATIO_P : Double;

    H1_SLIC_PROGRESS_P : Double;
    H1_CUMULATIVE_SPEED_RATIO_P : Double;
    H1_MAX_POSITION_P : Double;
    H1_SLIC_SPEED_P : Double;
    H1_VELOCITY_COUNTER_P : Double;
    H1_VEL_POS_HOLD_P : Double;

  public
    constructor Create( Parameters : string;
                        ResolveDeviceEv : TPlcPluginResolveDeviceEv;
                        ResolveItemEv : TPlcPluginResolveItemEv); override;
    procedure Compile ( var Embedded : TEmbeddedData;
                        aReadEv : TPlcPluginReadEv;
                        ResolveItemEv : TPlcPluginResolveItemEv;
                        Write : Boolean);  override;
    destructor Destroy; override;
    procedure GatePre; override;
    function GatePost : Integer; override;
    function Read(var Embedded : TEmbeddedData) : Boolean; override;
    procedure Write(var Embedded : TEmbeddedData; Line, LastLine : Boolean); override;
    function GateName(var Embedded : TEmbeddedData) : string; override;
    function ItemProperties : string; override;
    class function Properties : string; override;
  end;


implementation

uses Math;

type
  TPlcProperty = record
    Name : string;
    ReadOnly : Boolean;
  end;


const
  PropertyName : array [TPlcFakeHeadProperty] of TPlcProperty = (
    ( Name : 'Enable' ;  ReadOnly : False ),
    ( Name : 'Short' ;   ReadOnly : False ),
    ( Name : 'OpenGap' ; ReadOnly : False )
  );


constructor TFakePmacServoPlcItem.Create( Parameters : string;
                    ResolveDeviceEv : TPlcPluginResolveDeviceEv;
                    ResolveItemEv : TPlcPluginResolveItemEv);
var
  Token : string;
begin
  Token := ReadDelimited(Parameters, ';');
  if not ResolveDeviceEv(Self, PChar(Token), Pmac.ISize, Pmac.OSize, Pointer(Pmac.Input), Pointer(Pmac.Output)) then
    raise Exception.CreateFmt('Cannot find device [%s] in IO', [Token]);

  if (Pmac.ISize < Sizeof(TPmacPlcIn)) or (Pmac.OSize <> Sizeof(TPmacPlcOut)) then
    raise Exception.CreateFmt('Wrong size of device [%s] expected [I%d, O%d]', [Token, Sizeof(TPmacPlcIn), Sizeof(TPmacPlcOut)]);

  Token := ReadDelimited(Parameters, ';');
  if not ResolveDeviceEv(Self, PChar(Token), LPE577.ISize, LPE577.OSize, Pointer(LPE577.Input), Pointer(LPE577.Output)) then
    raise Exception.CreateFmt('Cannot find device [%s] in IO', [Token]);

  if (LPE577.ISize < Sizeof(TLPE577In)) or (LPE577.OSize <> Sizeof(TLPE577Out)) then
    raise Exception.CreateFmt('Wrong size of device [%s] expected [I%d, O%d]', [Token, Sizeof(TLPE577In), Sizeof(TLPE577Out)]);



  Token := ReadDelimited(Parameters, ';');
  if not ResolveDeviceEv(Self, PChar(Token), LPE482.ISize, LPE482.OSize, Pointer(LPE482.Input), Pointer(LPE482.Output)) then
    raise Exception.CreateFmt('Cannot find device [%s] in IO', [Token]);

  //if (LPE482.ISize < Sizeof(TLPE482In)) or (LPE482.OSize <> Sizeof(TLPE482Out)) then
  //  raise Exception.CreateFmt('Wrong size of device [%s] expected [I%d, O%d]', [Token, Sizeof(TLPE482In), Sizeof(TLPE482Out)]);

  Exclude(LPE577.Input^.Status, lpe577StatNot577);
  Include(LPE577.Input^.Status, lpe577Stat577);
end;

destructor TFakePmacServoPlcItem.Destroy;
begin
  inherited Destroy;
end;

procedure TFakePmacServoPlcItem.Compile ( var Embedded : TEmbeddedData;
                    aReadEv : TPlcPluginReadEv;
                    ResolveItemEv : TPlcPluginResolveItemEv;
                    Write : Boolean);
var I : TPlcFakeHeadProperty;
    Token : string;
begin
  Token := aReadEv(Self);
  if Token <> '.' then
    raise Exception.Create('Period [.] expected');

  Token := aReadEv(Self);
  for I := Low(TPlcFakeHeadProperty) to High(TPlcFakeHeadProperty) do begin
    if CompareText(PropertyName[I].Name, Token) = 0 then begin
      Embedded.Command := Ord(I);
      Exit;
    end;
  end;
  raise Exception.CreateFmt('Unknown property [%s]', [Token]);
end;

procedure TFakePmacServoPlcItem.GatePre;
begin
end;


// NOTE
// Demanded gap and servo are both * 10
//
function TFakePmacServoPlcItem.GatePost : Integer;
begin
  H1_GAP := Pmac.Output.Gap / 10;

// Just for simulation -----------------------
  if fepShort in Inputs then
    H1_VOLTS := 0
  else if fepOpenGap in Inputs then
    H1_VOLTS := H1_GAP  + Pmac.Output.ForwardThreshold
  else
    H1_VOLTS := H1_GAP + 1;

  H1_VOLTS := RandG(H1_VOLTS, H1_VOLTS / 8);
  LPE482.Input.Sense.B[0] := LPE482.Input.Sense.B[0] xor Random($100);
  LPE482.Input.Sense.B[1] := LPE482.Input.Sense.B[1] xor LPE482.Input.Sense.B[0];
  LPE482.Input.Sense.B[2] := LPE482.Input.Sense.B[2] xor LPE482.Input.Sense.B[1];
  LPE482.Input.Sense.B[3] := LPE482.Input.Sense.B[3] xor LPE482.Input.Sense.B[2];
  LPE482.Input.Sense.B[4] := LPE482.Input.Sense.B[4] xor LPE482.Input.Sense.B[3];
  LPE482.Input.Sense.B[5] := LPE482.Input.Sense.B[5] xor LPE482.Input.Sense.B[4];

//--------------------- End of Just for simulation

  //H1_VOLTS := ((H1_VOLTAGE_M / Pmac.Output.VoltageScaling) + 3);
  Pmac.Input.GapVolts := Round(H1_VOLTS);

  H1_POS := (H1_POSITION_M - H1_ENC_OFFSET) * H1_ENC_SCALING;

  PLC0_SPEED := Pmac.Output.Servo / SERVO_FREQ;
  PLC0_PSERVO := H1_VOLTS - H1_GAP;

  if not (pehcFreeze in Pmac.Output.Command) then begin
    IF (not(pehcSERVO in Pmac.Output.Command)) then begin
      IF(H1_SERVO_ON) then begin
        H1_SERVO_ON := False;

        H1_STANDOFF := Pmac.Output.Standoff / 1000;

        if(pehcFROMFIRSTSPARK in Pmac.Output.COMMAND) then begin
          if ((H1_FSPOS_P - H1_HIC) > H1_STANDOFF) then begin
            H1_ENC_INC_ADJUST := ((H1_FSPOS_P - H1_STANDOFF) / H1_ENC_SCALING) / (H1_HIC / RETRACT_SPEED)
          end else begin
            H1_ENC_INC_ADJUST := 0;
          end;


        end else begin
          if(pehcFROMDEPTH in Pmac.Output.COMMAND) then begin
            if (H1_STANDOFF > H1_HIC) then begin
              H1_ENC_INC_ADJUST := ((H1_HIC - H1_STANDOFF) / H1_ENC_SCALING) / (H1_HIC / RETRACT_SPEED);
            end else begin
              H1_ENC_INC_ADJUST := 0;
            end;
          end else begin
            H1_ENC_INC_ADJUST := 0;
          end;
        end;

  //      H1_FSPOS_P = 0
        H1_SHORT_COUNTER := 0;
        H1_MAX_POSITION_P := 0 // Zero forward progress used by SLIC
      end;
      IF(H1_HIC < 0) then begin
        Debug := 'Retracting';
        H1_HIC := H1_HIC + RETRACT_SPEED;
        H1_ENC_OFFSET := Round(H1_ENC_OFFSET + H1_ENC_INC_ADJUST);
      end ELSE  begin // (Pmac.Input.HIC >= 0)
        H1_HIC := 0;
        Debug := 'Retracted';
        Include(Pmac.Input.Status, pehsRetracted);
        Exclude(Pmac.Input.Status, pehsFirstSpark);
      END; // (Q_EDM1_HIC < 0)

    end ELSE begin // In EDM Servo Mode

     H1_SERVO_ON := True;
     Exclude(Pmac.Input.Status, pehsRetracted);

  // Is the volatage less than the ReqGap
      if(H1_VOLTS < H1_GAP) then begin
      // If voltage is less than Pmac.Output.RTHRESH
      // then enter increment short counter
        if (H1_VOLTS < Pmac.Output.ReverseTHRESHhold) then begin
          H1_SHORT_COUNTER := H1_SHORT_COUNTER + 1;

          if (H1_SHORT_COUNTER > Pmac.Output.ShortINTerval) then begin
          // Full Servo Reverse
            if(not(pehSFIRSTSPARK in Pmac.Input.STATUS)) then begin
              if(not(pehcLASTFIRSTSPARK in Pmac.Output.COMMAND)) then begin
                H1_FSPOS_P := Round(H1_HIC);
              end;
              Include(Pmac.Input.STATUS, pehsFIRSTSPARK);
            end;
            H1_HIC := H1_HIC + Pmac.Output.ShortVELocity;
            H1_SHORT_COUNTER := 0;
            Debug := 'Short';
          end; // (H1_SHORT_COUNTER > X_EDM1_SINT)
        end else begin
        // This is a frig that
        // keeps the electrode moving
        // regardless of voltage
          H1_HIC := H1_HIC - (PLC0_SPEED / 15);

          // SLIC accounts for frig
          H1_SPEED_RATIO_P := 1/15;
          Debug := '1/15 Creep';
        end; // (H1_VOLTS < X_EDM1_RTHRESH)

      end else begin //(H1_VOLTS > H1_GAP)
        H1_SHORT_COUNTER := 0;
        IF(H1_VOLTS > (H1_GAP + Pmac.Output.ForwardTHRESHold)) then begin
        // Full Servo forwards
          H1_HIC := H1_HIC - PLC0_SPEED;
          H1_SPEED_RATIO_P := 1;
          Debug := 'Full Fwd';
        end else begin
        // Proportional forwards
         H1_SPEED_RATIO_P := (PLC0_PSERVO / Pmac.Output.ForwardTHRESHold);
         H1_HIC := H1_HIC - (PLC0_PSERVO / Pmac.Output.ForwardTHRESHold) * PLC0_SPEED;
         Debug := 'Prop. Fwd';
        end;
      end;


      if (pehcDCM in Pmac.Output.COMMAND) then begin
      // Forward servo required and Feedhold set
        if (H1_HIC < H1_DCM_POSITION) then begin
          // Attempting to move beyond the current feed hold position
          H1_HIC := H1_DCM_POSITION;
        end;
      end else begin
        H1_DCM_POSITION := Round(H1_HIC);
      end;

      // Forward speed detector used by SLIC
      if(H1_HIC < H1_MAX_POSITION_P) then begin
        H1_SLIC_PROGRESS_P := H1_SLIC_PROGRESS_P + 1;
        H1_CUMULATIVE_SPEED_RATIO_P := H1_CUMULATIVE_SPEED_RATIO_P + H1_SPEED_RATIO_P;
        H1_MAX_POSITION_P := H1_HIC;
        Pmac.Input.MAXPOSition := Round(0 - H1_HIC);

        if(H1_SLIC_PROGRESS_P  > SLIC_INTEGRATION_COUNT) then begin
          H1_SLIC_SPEED_P := H1_CUMULATIVE_SPEED_RATIO_P / H1_SLIC_PROGRESS_P;
          Pmac.Input.SLIC := Round(H1_SLIC_SPEED_P * 10000); // units of 1/100th %
          H1_SLIC_PROGRESS_P := 0;
          H1_CUMULATIVE_SPEED_RATIO_P := 0;
        end;
      end;

      // Added MaxPos Filter + Fwd Velocity
      if(H1_VELOCITY_COUNTER_P > VELOCITY_INTEGRATION) then begin
        // NOTE the Hold - Position give a positive value
        Pmac.Input.VELOCITY := Round((H1_VEL_POS_HOLD_P - H1_MAX_POSITION_P) * (SERVO_FREQ / (VELOCITY_INTEGRATION * 10)));
        H1_VEL_POS_HOLD_P := H1_MAX_POSITION_P;
        H1_VELOCITY_COUNTER_P := 0;
      end;
    end;

    Pmac.Input.HIC := Round(H1_HIC);
    H1_DUPENC_M := H1_POS + Pmac.Input.HIC;

    if(pehsFIRSTSPARK in Pmac.Input.STATUS) then begin
      Pmac.Input.POSITION := H1_FSPOS_P - Pmac.Input.HIC;
    end else begin
      Pmac.Input.POSITION := -Pmac.Input.HIC;
    end;
  end;

  H1_VELOCITY_COUNTER_P := H1_VELOCITY_COUNTER_P + 1;

  // Servo responds with a, 100% recovery from error : Not bad eh?
  H1_POSITION_M := DesiredPosition;
  Result := 0;
end;


function TFakePmacServoPlcItem.Read(var Embedded : TEmbeddedData) : Boolean;
begin
  Result := TPlcFakeHeadProperty(Embedded.Command) in Inputs;
end;

procedure TFakePmacServoPlcItem.Write(var Embedded : TEmbeddedData; Line, LastLine : Boolean);
begin
  if Line <> LastLine then begin
    if Line then
      Include(Inputs, TPlcFakeHeadProperty(Embedded.Command))
    else
      Exclude(Inputs, TPlcFakeHeadProperty(Embedded.Command));
  end;
end;

function TFakePmacServoPlcItem.GateName(var Embedded : TEmbeddedData) : string;
begin
  Result := PropertyName[TPlcFakeHeadProperty(Embedded.Command)].Name;
end;

const
  BooleanIdent : array [Boolean] of PChar = ( 'False', 'True' );

function TFakePmacServoPlcItem.ItemProperties : string;
var I : TPlcFakeHeadProperty;
begin
  for I := Low(TPlcFakeHeadProperty) to High(TPlcFakeHeadProperty) do
    Result := Result + PropertyName[I].Name + CarRet + BooleanIdent[I in Inputs] + CarRet;

  Result := Result + 'Debug' + CarRet + Debug + CarRet;
  Result := Result + Format('COMMAND' + CarRet + '%d' + CarRet, [Ord(Word(Pmac.Output.Command))]);
  Result := Result + Format('H1_HIC' + CarRet + '%f' + CarRet, [H1_HIC]);
  Result := Result + Format('H1_GAP' + CarRet + '%f' + CarRet, [H1_GAP]);
  Result := Result + Format('H1_POSITION_M' + CarRet + '%d' + CarRet, [H1_POSITION_M]);
  Result := Result + Format('H1_ENC_OFFSET' + CarRet + '%d' + CarRet, [H1_ENC_OFFSET]);
  Result := Result + Format('H1_VOLTAGE_M' + CarRet + '%d' + CarRet, [H1_VOLTAGE_M]);
  Result := Result + Format('H1_SERVO_ON' + CarRet + '%d' + CarRet, [Ord(H1_SERVO_ON)]);
  Result := Result + Format('H1_STANDOFF' + CarRet + '%f' + CarRet, [H1_STANDOFF]);
  Result := Result + Format('H1_FSPOS_P' + CarRet + '%d' + CarRet, [H1_FSPOS_P]);
  Result := Result + Format('H1_ENC_INC_ADJUST' + CarRet + '%f' + CarRet, [H1_ENC_INC_ADJUST]);
  Result := Result + Format('H1_SHORT_COUNTER' + CarRet + '%d' + CarRet, [H1_SHORT_COUNTER]);
  Result := Result + Format('H1_MAX_POSITION_P' + CarRet + '%f' + CarRet, [H1_MAX_POSITION_P]);
  Result := Result + Format('H1_DCM_POSITION' + CarRet + '%d' + CarRet, [H1_DCM_POSITION]);
  Result := Result + Format('H1_DUPENC_M' + CarRet + '%f' + CarRet, [H1_DUPENC_M]);
  Result := Result + Format('H1_POS' + CarRet + '%f' + CarRet, [H1_POS]);
  Result := Result + Format('H1_VOLTS' + CarRet + '%f' + CarRet, [H1_VOLTS]);
  Result := Result + Format('PLC0_SPEED' + CarRet + '%f' + CarRet, [PLC0_SPEED]);
  Result := Result + Format('PLC0_PSERVO' + CarRet + '%f' + CarRet, [PLC0_PSERVO]);
  Result := Result + Format('H1_SPEED_RATIO_P' + CarRet + '%f' + CarRet, [H1_SPEED_RATIO_P]);
  Result := Result + Format('H1_SLIC_PROGRESS_P' + CarRet + '%f' + CarRet, [H1_SLIC_PROGRESS_P]);
  Result := Result + Format('H1_CUMULATIVE_SPEED_RATIO_P' + CarRet + '%f' + CarRet, [H1_CUMULATIVE_SPEED_RATIO_P]);
  Result := Result + Format('H1_MAX_POSITION_P' + CarRet + '%f' + CarRet, [H1_MAX_POSITION_P]);
  Result := Result + Format('H1_SLIC_SPEED_P' + CarRet + '%f' + CarRet, [H1_SLIC_SPEED_P]);
  Result := Result + Format('H1_VELOCITY_COUNTER_P' + CarRet + '%f' + CarRet, [H1_VELOCITY_COUNTER_P]);
  Result := Result + Format('H1_VEL_POS_HOLD_P' + CarRet + '%f' + CarRet, [H1_VEL_POS_HOLD_P]);
end;


class function TFakePmacServoPlcItem.Properties : string;
var I : TPlcFakeHeadProperty;
begin
  Result := '';
  for I := Low(TPlcFakeHeadProperty) to High(TPlcFakeHeadProperty) do
    Result := Result + PropertyName[I].Name + CarRet;
end;

initialization
  TFakePmacServoPlcItem.AddPlugin('FakeServo', TFakePmacServoPlcItem);
end.
