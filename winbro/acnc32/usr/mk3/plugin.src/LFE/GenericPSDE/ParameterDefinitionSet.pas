unit ParameterDefinitionSet;

interface

uses Classes, TypedParameter;

type
  TParameterDefinitions = class
  private
    List: TStringList;
  public
    constructor Create;
    destructor Destroy; override;
    procedure Clear;
    procedure AddParameter(P: TUntypedParameter);
    function GetParameter(Key: string): TUntypedParameter; overload;
    function GetParameter(Index: Integer): TUntypedParameter; overload;
    function GetParameterCount: Integer;
  end;



implementation

{ TParameterDefinitions }

constructor TParameterDefinitions.Create;
begin
  List:= TStringList.Create;
end;

function TParameterDefinitions.GetParameter(Key: string): TUntypedParameter;
var I: Integer;
begin
  Result:= nil;
  I:= List.IndexOf(Key);
  if I <> -1 then
    Result:= GetParameter(I);
end;

procedure TParameterDefinitions.AddParameter(P: TUntypedParameter);
begin
  List.AddObject(P.Name, P);
end;

function TParameterDefinitions.GetParameter(Index: Integer): TUntypedParameter;
begin
  Result:= TUntypedParameter(List.Objects[Index]);
end;

function TParameterDefinitions.GetParameterCount: Integer;
begin
  Result:= List.Count;
end;


procedure TParameterDefinitions.Clear;
begin
  if not assigned(List) then exit;
  while List.Count > 0 do begin
    if assigned(List.Objects[0]) then List.Objects[0].Free;
    List.Delete(0);
  end;
end;

destructor TParameterDefinitions.Destroy;
begin
  List.Free;
  inherited;
end;

end.
