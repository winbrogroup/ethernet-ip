// V1.0.0 Tageed on CVS as GEN_PSDE_V1_0_0_14_MAY_09
unit GenericEditorForm;


interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Buttons, StdCtrls, Grids, ValEdit, ExtCtrls;

type
  IParameterPool = interface
    procedure GetDescriptionList(S: TStrings);
    procedure GetReferenceList(S: TStrings);
    procedure SetReference(const Reference: WideString); stdcall;
    function GetReference: WideString;
    function ReferenceValid: Boolean;
    procedure SetValue(const Key: string; const Value: string);
    function GetValue(const Key: string): string;
    procedure Delete(const Reference: WideString);
    procedure New(const Reference: WideString);
    procedure Copy(const Reference: WideString; const Name: WideString);
    procedure Rename(const Old: WideString; const Reference: WideString);
    procedure GetPickList(const Key: string; const Values: TStrings);
    function GetHint(const Reference: string): string;
  end;

  TGenericEditor = class(TForm)
    ReferenceList: TListBox;
    Panel1: TPanel;
    Panel2: TPanel;
    ValueListEditor: TValueListEditor;
    TitlePanel: TPanel;
    BtnNew: TButton;
    BtnRename: TButton;
    BtnDelete: TButton;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    BtnCopy: TButton;
    procedure ValueListEditorMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
    procedure ValueListEditorGetPickList(Sender: TObject; const KeyName: string;
      Values: TStrings);
    procedure ReferenceListClick(Sender: TObject);
    procedure ValueListEditorValidate(Sender: TObject; ACol, ARow: Integer;
      const KeyName, KeyValue: string);
    procedure BtnCopyClick(Sender: TObject);
    procedure BtnDeleteClick(Sender: TObject);
    procedure BtnRenameClick(Sender: TObject);
    procedure BtnNewClick(Sender: TObject);
  private
    Model: IParameterPool;
    procedure SetModel(Model: IParameterPool);
    procedure UpdateVisuals;
    procedure DisableEditor;
    procedure UpdateReferenceIndex;
  public
    class function Execute(Model: IParameterPool; Title: string): Boolean;
  end;

var
  GenericEditor: TGenericEditor;

implementation

{$R *.dfm}

class function TGenericEditor.Execute(Model: IParameterPool; Title: string): Boolean;
var
  Editor: TGenericEditor;
begin
  Editor:= TGenericEditor.Create(Application);
  Editor.setModel(Model);
  Editor.Caption:= Title;
  Result:= Editor.ShowModal = mrOK;
  Editor.Free;
end;

procedure TGenericEditor.SetModel(Model: IParameterPool);
var I: Integer;
    S: TStringList;
begin
  S:= TStringList.Create;
  Self.Model:= Model;
  try
    Model.GetDescriptionList(S);
    for I:= 0 to S.Count - 1 do begin
      S[I]:= S[I] + '=';
    end;
    ValueListEditor.Strings:= S;
  finally
    S.Free;
  end;
  UpdateReferenceIndex;
  UpdateVisuals;
end;

procedure TGenericEditor.BtnNewClick(Sender: TObject);
var Value: string;
begin
  Value:= '';
  if InputQuery('New Parameters', 'Enter new reference name', Value) then begin
    Model.New(Value);
    Model.SetReference(Value);
    Model.GetReferenceList(ReferenceList.Items);
    UpdateVisuals;
  end;
end;

procedure TGenericEditor.BtnRenameClick(Sender: TObject);
var Value: string;
begin
  if Model.ReferenceValid then begin
    Value:= Model.GetReference;
    if InputQuery('Rename Parameters', 'Enter new reference name', Value) then begin
      Model.Rename(Model.GetReference, Value);
      Model.SetReference(Value);
      Model.GetReferenceList(ReferenceList.Items);
      UpdateVisuals;
    end;
  end;
end;

procedure TGenericEditor.BtnDeleteClick(Sender: TObject);
begin
  if Model.ReferenceValid then begin
    Model.Delete(Model.GetReference);
    Model.GetReferenceList(ReferenceList.Items);
    UpdateVisuals;
  end;
end;

procedure TGenericEditor.BtnCopyClick(Sender: TObject);
var Value: string;
begin
  if Model.ReferenceValid then begin
    Value:= Model.GetReference;
    if InputQuery('Copy Parameters', 'Enter new reference name', Value) then begin
      Model.Copy(Model.GetReference, Value);
      Model.SetReference(Value);
      Model.GetReferenceList(ReferenceList.Items);
      UpdateVisuals;
    end;
  end;
end;

procedure TGenericEditor.UpdateVisuals;
var I: Integer;
begin
  if Model.ReferenceValid then begin
    ValueListEditor.Enabled:= True;
    ValueListEditor.Font.Color:= clBlack;
    for I:= 1 to ValueListEditor.RowCount -1 do begin
      ValueListEditor.Cells[1, I] := Model.GetValue(ValueListEditor.Cells[0, I]);
    end;
  end
  else
    DisableEditor;

  UpdateReferenceIndex;
end;

procedure TGenericEditor.ValueListEditorValidate(Sender: TObject; ACol,
  ARow: Integer; const KeyName, KeyValue: string);
begin
  Model.SetValue(ValueListEditor.Cells[0, ARow], ValueListEditor.Cells[1, ARow]);
  ValueListEditor.Cells[1, ARow]:= Model.GetValue(ValueListEditor.Cells[0, ARow]);
end;

procedure TGenericEditor.ValueListEditorGetPickList(Sender: TObject;
  const KeyName: string; Values: TStrings);
begin
  Model.GetPickList(KeyName, Values);
end;


procedure TGenericEditor.ReferenceListClick(Sender: TObject);
begin
  if ReferenceList.ItemIndex <> -1 then begin
    Model.SetReference(ReferenceList.Items[ReferenceList.ItemIndex]);
    UpdateVisuals;
  end;
end;

procedure TGenericEditor.DisableEditor;
begin
  ValueListEditor.Enabled:= False;;
  ValueListEditor.Font.Color:= clGray;
end;

procedure TGenericEditor.UpdateReferenceIndex;
var I: Integer;
begin
  Model.GetReferenceList(ReferenceList.Items);
  I:= ReferenceList.Items.IndexOf( Model.GetReference );
  if I <> -1 then begin
    ReferenceList.ItemIndex:= I;
    TitlePanel.Caption:= Model.GetReference;
  end else begin
    ReferenceList.ItemIndex:= -1;
    TitlePanel.Caption:= '-';
  end;
end;

procedure TGenericEditor.ValueListEditorMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
var G: TGridCoord;
begin
  G:= ValueListEditor.MouseCoord(X, Y);
  
  if (G.Y > 0) and (G.Y < ValueListEditor.RowCount) then begin
    ValueListEditor.Hint:= Model.GetHint(ValueListEditor.Cells[0, G.Y])
  end;
end;

end.
