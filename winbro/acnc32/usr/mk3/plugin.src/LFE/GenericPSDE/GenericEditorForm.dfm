object GenericEditor: TGenericEditor
  Left = 0
  Top = 0
  Width = 584
  Height = 502
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -16
  Font.Name = 'Tahoma'
  Font.Style = [fsBold]
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 19
  object ReferenceList: TListBox
    Left = 0
    Top = 82
    Width = 193
    Height = 345
    Align = alLeft
    ItemHeight = 19
    TabOrder = 0
    OnClick = ReferenceListClick
  end
  object Panel1: TPanel
    Left = 0
    Top = 41
    Width = 576
    Height = 41
    Align = alTop
    BevelInner = bvLowered
    TabOrder = 1
    object BtnNew: TButton
      Left = 24
      Top = 8
      Width = 90
      Height = 25
      Caption = 'New'
      TabOrder = 0
      OnClick = BtnNewClick
    end
    object BtnRename: TButton
      Left = 120
      Top = 8
      Width = 90
      Height = 25
      Caption = 'Rename'
      TabOrder = 1
      OnClick = BtnRenameClick
    end
    object BtnDelete: TButton
      Left = 216
      Top = 8
      Width = 90
      Height = 25
      Caption = 'Delete'
      TabOrder = 2
      OnClick = BtnDeleteClick
    end
    object BtnCopy: TButton
      Left = 312
      Top = 8
      Width = 90
      Height = 25
      Caption = 'Copy'
      TabOrder = 3
      OnClick = BtnCopyClick
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 427
    Width = 576
    Height = 41
    Align = alBottom
    BevelInner = bvLowered
    TabOrder = 2
    object BitBtn1: TBitBtn
      Left = 344
      Top = 8
      Width = 90
      Height = 25
      TabOrder = 0
      Kind = bkCancel
    end
    object BitBtn2: TBitBtn
      Left = 440
      Top = 8
      Width = 90
      Height = 25
      TabOrder = 1
      Kind = bkOK
    end
  end
  object ValueListEditor: TValueListEditor
    Left = 193
    Top = 82
    Width = 383
    Height = 345
    Align = alClient
    DefaultColWidth = 250
    DefaultRowHeight = 24
    ParentShowHint = False
    ShowHint = True
    TabOrder = 3
    TitleCaptions.Strings = (
      'Parameter'
      'Value')
    OnGetPickList = ValueListEditorGetPickList
    OnMouseMove = ValueListEditorMouseMove
    OnValidate = ValueListEditorValidate
    ColWidths = (
      250
      127)
  end
  object TitlePanel: TPanel
    Left = 0
    Top = 0
    Width = 576
    Height = 41
    Align = alTop
    BevelInner = bvLowered
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 4
  end
end
