unit TypedParameter;

interface

uses SysUtils, Classes, StreamTokenizer;

type
  TUntypedParameter = class
  private
    FName: string;
    FDescription: string;
    FDefault: string;
    FValue: string;
    FHint: string;
  public
    procedure SetValue(const AValue: string); virtual;
    procedure Read(Token: string; Tokenizer: TStreamTokenizer); virtual;
    procedure Default;
    procedure GetPickList(Values: TStrings); virtual;
    property Name: string read FName;
    property Value: string read FValue write SetValue;
    property Description: string read FDescription write FDescription;
    property Hint: string read FHint write FHint;
  end;


  TIntegerParameter = class(TUntypedParameter)
  protected
    FMaximum: Integer;
    FMinimum: Integer;
  public
    constructor Create;
    procedure SetValue(const AValue: string); override;
    procedure Read(Token: string; Tokenizer: TStreamTokenizer); override;
  end;

  TDoubleParameter = class(TUntypedParameter)
  protected
    FMaximum: Double;
    FMinimum: Double;
    FPrecision: Integer;
  public
    constructor Create;
    procedure SetValue(const AValue: string); override;
    procedure Read(Token: string; Tokenizer: TStreamTokenizer); override;
  end;

  TListParameter = class(TUntypedParameter)
  private
    ParamList: TStringList;
  public
    constructor Create;
    procedure SetValue(const AValue: string); override;
    procedure Read(Token: string; Tokenizer: TStreamTokenizer); override;
    procedure GetPickList(Values: TStrings); override;
  end;

implementation

{ TUntypedParameter }

procedure TUntypedParameter.Default;
begin
  FValue:= FDefault;
end;

procedure TUntypedParameter.GetPickList(Values: TStrings);
begin

end;

procedure TUntypedParameter.Read(Token: string; Tokenizer: TStreamTokenizer);
begin
  if Token = 'name' then
    FName:= Tokenizer.ReadStringAssign
  else if Token = 'default' then
    FDefault:= Tokenizer.ReadStringAssign
  else if Token = 'description' then
    FDescription:= Tokenizer.ReadStringAssign
  else if Token = 'hint' then
    FHint:= Tokenizer.ReadStringAssign
  else
    raise Exception.Create('Unknown token: ' + Token);
end;

procedure TUntypedParameter.SetValue(const AValue: string);
begin
  FValue:= AValue;
end;

{ TIntegerParameter }

constructor TIntegerParameter.Create;
begin
  FMinimum:= -MAXINT;
  FMaximum:= MAXINT;
end;

procedure TIntegerParameter.SetValue(const AValue: string);
var I: Integer;
begin
  try
    I:= StrToInt(AValue);
  except
    raise Exception.Create('Value must be an Integer: ' + AValue);
  end;

  if I > FMaximum then
    raise Exception.Create('Value is too high: max = ' + IntToStr(FMaximum));
  if I < FMinimum then
    raise Exception.Create('Value is too low: min = ' + IntToStr(FMinimum));

  FValue:= AValue;
end;

procedure TIntegerParameter.Read(Token: string;
  Tokenizer: TStreamTokenizer);
begin
  if Token = 'maximum' then
    FMaximum:= StrToIntDef(Tokenizer.ReadStringAssign, 0)
  else if Token = 'minimum' then
    FMinimum:= StrToIntDef(Tokenizer.ReadStringAssign, 0)
  else
    inherited;
end;

{ TDoubleParameter }

constructor TDoubleParameter.Create;
begin
  FMinimum:= -1E100;
  FMinimum:= 1E100;
end;

procedure TDoubleParameter.Read(Token: string; Tokenizer: TStreamTokenizer);
begin
  if Token = 'maximum' then
    FMaximum:= StrToFloatDef(Tokenizer.ReadStringAssign, 0)
  else if Token = 'minimum' then
    FMinimum:= StrToFloatDef(Tokenizer.ReadStringAssign, 0)
  else if Token = 'precision' then
    FPrecision:= StrToIntDef(Tokenizer.ReadStringAssign, 6)
  else
    inherited;
end;

procedure TDoubleParameter.SetValue(const AValue: string);
var I: Double;
begin
  try
    I:= StrToFloat(AValue);
  except
    raise Exception.Create('Value must be a floating point number: ' + AValue);
  end;

  if I > FMaximum then
    raise Exception.Create('Value is too high: max = ' + FloatToStr(FMaximum));
  if I < FMinimum then
    raise Exception.Create('Value is too low: min = ' + FloatToStr(FMinimum));

  FValue:= Format('%.*f', [FPrecision, I]);
end;

{ TListParameter }

constructor TListParameter.Create;
begin
  ParamList:= TStringList.Create;
end;

procedure TListParameter.GetPickList(Values: TStrings);
var I: Integer;
begin
  for I:= 0 to ParamList.Count - 1 do begin
    Values.Add(ParamList[I]);
  end;
end;

procedure TListParameter.SetValue(const AValue: string);
var I: Integer;
begin
  for I:= 0 to ParamList.Count - 1 do begin
    if AValue = ParamList[I] then
      FValue:= AValue;
  end;
end;

procedure TListParameter.Read(Token: string; Tokenizer: TStreamTokenizer);
begin
  if Token = 'item' then
    ParamList.Add(Tokenizer.ReadStringAssign)
  else
    inherited;
end;

end.
