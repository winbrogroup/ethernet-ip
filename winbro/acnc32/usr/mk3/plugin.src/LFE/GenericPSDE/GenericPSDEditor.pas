unit GenericPSDEditor;

interface

uses SysUtils, Classes, ProcessEditor, AbstractPSD, IniFiles, NamedPlugin,
     StreamTokenizer, TypedParameter, XmlIntf, ParameterDefinitionSet, Cnctypes,
     Variants, PSDPlugin, GenericEditorForm;

type
  TGenericEditorBootStrap = class
  public
    constructor Create;
    procedure AssertIni(var IniName: String; DefaultBasePath: String);
  end;

  TGenericPSDEditor = class(TAbstractPSD, IParameterPool)
  private
    Root: IXmlNode;
    ActiveNode: IXmlNode;
    ParameterDefinitions: TParameterDefinitions;
    CurrentPostFeatureIndex : Integer;
    ChangeList: TStringList;

    procedure ReadConfiguration(Stream: TFileStream);
    procedure LoadParametersFromNode(Node: IXmlNode);
    procedure CheckValidName(const Reference: WideString);

    procedure AddRename(Old, Reference: WideString);
    procedure AddDelete(Reference: WideString);
  public
    constructor Create(const UniqueName: string); override;
    destructor Destroy; override;
    function ShowModal(Host: IPSDHost; const Reference: WideString; X, Y: Integer; Title: WideString): WideString; override; stdcall;
    procedure BeginPost(Host: IPSDHost); override; stdcall;
    procedure EndPost; override; stdcall;
    procedure SetReference(const Reference: WideString); override; stdcall;
    function Resolve(const AToken: WideString; out Res: WideString): Boolean; override; stdcall;
    procedure FirstFeature; override; stdcall;
    procedure NextFeature; override; stdcall;

    { IParameterPool }
    procedure GetDescriptionList(S: TStrings);
    procedure GetReferenceList(S: TStrings);
    function GetReference: WideString;
    function ReferenceValid: Boolean;
    procedure SetValue(const Key: string; const Value: string);
    function GetValue(const Key: string): string;
    procedure Delete(const Reference: WideString);
    procedure New(const Reference: WideString);
    procedure Copy(const Reference: WideString; const NewName: WideString);
    procedure Rename(const Old: WideString; const Reference: WideString);
    procedure GetPickList(const Key: string; const Values: TStrings);
    function GetHint(const Key: string): string;
  end;

procedure PSDInitialise; stdcall;

exports
  PSDInitialise name PSD_Initialise_Name;


implementation


var S: TStringList;

const
  REFERENCE_NAME = 'reference';
  PARAMETER_NODE = 'genparms';
  REFERENCE_NODE = 'generic';

{ TGenericPSDEditor }

constructor TGenericPSDEditor.Create(const UniqueName: string);
begin
  inherited;
  SetUniqueName(UniqueName);
  SetDisplayName(S.Values[UniqueName]);
  ParameterDefinitions:= TParameterDefinitions.Create;
  ReadConfiguration(TFileStream.Create(DllProfilePath + '\GenericPSDE\' + UniqueName + '.cfg', fmOpenRead));
  ChangeList:= TStringList.Create;
end;


function TGenericPSDEditor.Resolve(const AToken: WideString;
  out Res: WideString): Boolean;

var
  P: TUntypedParameter;
begin
  Result:= False;
  P:= ParameterDefinitions.GetParameter(AToken);
  if P <> nil then begin
    Res:= P.Value;
    Result:= True;
  end;
end;

procedure TGenericPSDEditor.EndPost;
begin
  Root:= nil;
end;

procedure TGenericPSDEditor.BeginPost(Host: IPSDHost);
begin
  Root:= GetRootNode(Host.GetDocument).CloneNode(True);
end;

procedure TGenericPSDEditor.LoadParametersFromNode(Node: IXmlNode);
var I: Integer;
    Tmp: OleVariant;
    P: TUntypedParameter;
begin
  if Node.NodeName = PARAMETER_NODE then begin
    for I:= 0 to ParameterDefinitions.GetParameterCount - 1 do begin
      P:= ParameterDefinitions.GetParameter(I);
      Tmp:= Node.Attributes[P.Name];
      if VarType(Tmp) = varNull then begin
        P.Default;
        Node.Attributes[P.Name]:= P.Value;
      end else
        P.Value:= Tmp;
    end;
  end;
end;

{ Throws exception "reference not found" }
procedure TGenericPSDEditor.SetReference(const Reference: WideString);

  procedure ClearParameters;
  var I: Integer;
  begin
    for I:= 0 to ParameterDefinitions.GetParameterCount - 1 do
      ParameterDefinitions.GetParameter(I).Default;
  end;

var I: Integer;
    NL: IXmlNodeList;
    Node: IXmlNode;
begin
  ClearParameters;

  NL:= Root.ChildNodes;
  for I:= 0 to NL.Count - 1 do begin
    Node:= NL[I];
    if Node.NodeName = REFERENCE_NODE then begin
      if Node.Attributes[REFERENCE_NAME] = Reference then begin
        ActiveNode:= Node;
        if Node.ChildNodes.Count > 0 then begin
          try
          LoadParametersFromNode(Node.ChildNodes[0]);
          except
          raise;
         // raise Exception.Create(UniqueName + ' Invalid Process Parameter Set');
          end;
        end;
        Exit;
      end;
    end;
  end;
  ActiveNode:= nil;
  raise Exception.Create(UniqueName + ' Reference not found: ' + Reference);
end;

function TGenericPSDEditor.ShowModal(Host: IPSDHost; const Reference: WideString;
  X, Y: Integer; Title: WideString): WideString;
begin
  ChangeList.Clear;
  Result:= Reference;
  ParameterDefinitions.Clear;
  ReadConfiguration(TFileStream.Create(DllProfilePath + '\GenericPSDE\' + UniqueName + '.cfg', fmOpenRead));
  Root:= GetRootNode(Host.GetDocument).CloneNode(True);

  try
    SetReference(Reference);
  except
    Result:= '';
  end;

  if TGenericEditor.Execute(Self, Title) then begin
    SetRootNode(Host.GetDocument, Root);
    if ReferenceValid then
      Result:= '*=' + ActiveNode.Attributes[REFERENCE_NAME]
    else
      Result:= '*= ';
  end;

  ChangeList.Insert(0, Result);
  Result:= ChangeList.Text;

  ActiveNode:= nil;
  Root:= nil;
end;


{ Throws file open exceptions }
procedure TGenericPSDEditor.ReadConfiguration(Stream: TFileStream);
var ST: TStreamQuoteTokenizer;
    Token: string;

  procedure ReadObject(Parm: TUntypedParameter);
  begin
    ST.ExpectedToken('=');
    ST.ExpectedToken('{');

    Token:= LowerCase(ST.Read);
    try
      while not ST.EndOfStream do begin
        if Token = '}' then
          Exit
        else
          Parm.Read(Token, ST);
        Token:= LowerCase(ST.Read);
      end;
    finally
      ParameterDefinitions.AddParameter(Parm);
    end;
  end;

  procedure ReadInclude;
  var F: string;
  begin
    ST.ExpectedToken('=');
    F:= ST.Read;
    ReadConfiguration(TFileStream.Create(DllProfilePath + '\GenericPSDE\' + F + '.cfg', fmOpenRead));
  end;

begin
  FFeatureBased:= True;
  FCapability := 'ANY';
  try
    ST:= TStreamQuoteTokenizer.Create;
    ST.Seperator:= '[]{}=';
    ST.QuoteChar:= '''';

    ST.SetStream( Stream );
    try
      Token:= LowerCase(ST.Read);
      while not ST.EndOfStream do begin
        if Token = 'integer' then
          ReadObject(TIntegerParameter.Create)
        else if Token = 'string' then
          ReadObject(TUntypedParameter.Create)
        else if Token = 'double' then
          ReadObject(TDoubleParameter.Create)
        else if Token = 'list' then
          ReadObject(TListParameter.Create)
        else if Token = 'featurebased' then
          FFeatureBased:= ST.ReadBooleanAssign
        else if Token = 'capability' then
          FCapability:= ST.ReadStringAssign
        else if Token = 'canmultiassign' then
          FCanMultiAssign:= ST.ReadStringAssign
        else if Token = 'include' then
          ReadInclude
        else
          raise Exception.Create('Unexpected end of file');

        Token:= LowerCase(ST.Read);
      end;
    finally
      ST.ReleaseStream;
      ST.Free;
    end;
  except
    on E: Exception do
    //  Named.SetFaultA(UniqueName, 'Failed to configure ' + UniqueName + ' ' + E.Message, FaultWarning, nil);
      raise Exception.Create('Failed to configure ' + UniqueName + ' ' + E.Message);
  end;
end;

constructor TGenericEditorBootStrap.Create;
var Ini: TIniFile;
    I: Integer;
    Tmp: string;
begin
  inherited;
  // Read the generic configuration file
  if FileExists(DllProfilePath+ '\Camapt.cfg') then
    begin
    Ini := TIniFile.Create(DllProfilePath+ '\Camapt.cfg');
    try
    Tmp := ini.ReadString('GENERAL','DefaultPSDEConfigDirectory','');
    if Tmp = '' then
      begin
      Tmp:= DllProfilePath + 'GenericPSDE\GenericPSDEditor.cfg';
      end
    else
      begin
      AssertIni(Tmp,DllProfilePath);
      end;
    finally
    ini.Free;
    end;
    end
  else
    begin
    Tmp:= DllProfilePath + 'GenericPSDE\GenericPSDEditor.cfg';
    end;
  Ini:= TIniFile.Create(Tmp);
  try
    S:= TStringList.Create;
    Ini.ReadSectionValues('Editors', S);

    for I:= 0 to S.Count - 1 do begin
      TGenericPSDEditor.AddEditor(S.Names[I], TGenericPSDEditor);
    end;
  finally
    Ini.Free;
  end;
end;

procedure PSDInitialise; stdcall;
begin
  TGenericEditorBootStrap.Create;
end;

procedure TGenericPSDEditor.Rename(const Old, Reference: WideString);
begin
  CheckValidName(Reference);
  SetReference(Old);
  ActiveNode.Attributes[REFERENCE_NAME] := Reference;
  SetReference(Reference);
  AddRename(Old, Reference);
end;

procedure TGenericPSDEditor.New(const Reference: WideString);
var Node: IXmlNode;
    I: Integer;
    P: TUntypedParameter;
begin
  CheckValidName(Reference);
  ActiveNode:= Root.AddChild(REFERENCE_NODE);
  ActiveNode.Attributes[REFERENCE_NAME]:= Reference;
  Node:= ActiveNode.AddChild(PARAMETER_NODE);
  LoadParametersFromNode(Node);
  for I:= 0 to ParameterDefinitions.GetParameterCount - 1 do begin
    P:= ParameterDefinitions.GetParameter(I);
    Node.Attributes[P.Name]:= P.Value;
  end;
  AddDelete(Reference);
end;

function TGenericPSDEditor.GetValue(const Key: string): string;
var I: Integer;
    P: TUntypedParameter;
begin
  for I:= 0 to ParameterDefinitions.GetParameterCount - 1 do begin
    P:= ParameterDefinitions.GetParameter(I);
    if P.Description = Key then
      Result:= P.Value;
  end;
end;

procedure TGenericPSDEditor.GetPickList(const Key: string;
  const Values: TStrings);
var I: Integer;
    P: TUntypedParameter;
begin
  for I:= 0 to ParameterDefinitions.GetParameterCount - 1 do begin
    P:= ParameterDefinitions.GetParameter(I);
    if P.Description = Key then begin
      P.GetPickList(Values);
    end;
  end;
end;

function TGenericPSDEditor.GetHint(const Key: string): string;
var I: Integer;
    P: TUntypedParameter;
begin
  for I:= 0 to ParameterDefinitions.GetParameterCount - 1 do begin
    P:= ParameterDefinitions.GetParameter(I);
    if P.Description = Key then begin
      if Result <> P.Hint then
        Result:= P.Hint;
      Exit;
    end;
  end;
  Result:= '';
end;


procedure TGenericPSDEditor.GetDescriptionList(S: TStrings);
var I: Integer;
    P: TUntypedParameter;
begin
  S.Clear;
  for I:= 0 to ParameterDefinitions.GetParameterCount - 1 do begin
    P:= ParameterDefinitions.GetParameter(I);
    S.Add(P.Description);
  end;
end;


procedure TGenericPSDEditor.GetReferenceList(S: TStrings);
var I: Integer;
    NL: IXmlNodeList;
    Node: IXmlNode;
begin
  S.Clear;
  NL:= Root.ChildNodes;
  for I:= 0 to NL.Count - 1 do begin
    Node:= NL[I];
    if Node.NodeName = REFERENCE_NODE then begin
      if VarType(Node.Attributes[REFERENCE_NAME]) <> VarNull then
        S.Add(Node.Attributes[REFERENCE_NAME]);
    end;
  end;
end;

function TGenericPSDEditor.GetReference: WideString;
begin
  if ReferenceValid then
    Result:= ActiveNode.Attributes[REFERENCE_NAME]
  else
    Result:= '';
end;

function TGenericPSDEditor.ReferenceValid: Boolean;
begin
  Result:= Assigned(ActiveNode);
end;

procedure TGenericPSDEditor.Copy(const Reference: WideString; const NewName: WideString);
var NewNode: IXmlNode;
begin
  CheckValidName(NewName);
  if ReferenceValid then begin
    NewNode:= ActiveNode.CloneNode(true);
    NewNode.Attributes[REFERENCE_NAME]:= NewName;
    Root.ChildNodes.Add(NewNode);
    SetReference(NewName);
  end;
end;

procedure TGenericPSDEditor.Delete(const Reference: WideString);
begin
  if ReferenceValid then begin
    Root.ChildNodes.Remove(ActiveNode);
    ActiveNode:= nil;
  end;
end;

procedure TGenericPSDEditor.SetValue(const Key, Value: string);
var I: Integer;
    P: TUntypedParameter;
begin
  for I:= 0 to ParameterDefinitions.GetParameterCount - 1 do begin
    P:= ParameterDefinitions.GetParameter(I);
    if P.Description = Key then begin
      P.SetValue(Value);
      ActiveNode.ChildNodes[0].Attributes[P.Name]:= P.Value;
    end;
  end;
end;

procedure TGenericPSDEditor.CheckValidName(const Reference: WideString);
var I: Integer;
    NL: IXmlNodeList;
    Node: IXmlNode;
    Tmp: string;
begin
  Tmp:= Trim(Reference);
  if Length(Tmp) < 2 then
    raise Exception.Create('Reference name for parameters can''t be blank');

  NL:= Root.ChildNodes;
  for I:= 0 to NL.Count - 1 do begin
    Node:= NL[I];
    if Node.NodeName = REFERENCE_NODE then begin
      if Node.Attributes[REFERENCE_NAME] = Tmp then begin
        raise Exception.Create('Reference aleady exists: ' + Tmp);
      end;
    end;
  end;
end;
// the following are not used in the Generic PSDE
// but can be implimented in Custom PSDE
procedure TGenericPSDEditor.NextFeature;
begin
CurrentPostFeatureIndex := CurrentPostFeatureIndex+1;
end;

procedure TGenericPSDEditor.FirstFeature;
begin
CurrentPostFeatureIndex := 0;
end;


procedure TGenericEditorBootStrap.AssertIni(var IniName: String;DefaultBasePath : String);
var
Buffer : String;
function SlashSep(const Path, S: String): String;
begin
  if AnsiLastChar(Path)^ <> '\' then
    Result := Path + '\' + S
  else
    Result := Path + S;
end;

begin
if IniName[1] <> '\' then
  begin
  Buffer := SlashSep(DefaultBasePath,IniName);
  end
else
  begin
  Buffer := Copy(IniName,2,1000);
  end;
IniName := Buffer;
If not DirectoryExists(Buffer) then ForceDirectories(IniName);
IniName := SlashSep(IniName,'GenericPSDEditor.cfg');
end;


destructor TGenericPSDEditor.Destroy;
begin
 ParameterDefinitions.Clear;
 ParameterDefinitions.Free;
 ChangeList.Free;
 inherited;
end;

procedure TGenericPSDEditor.AddRename(Old, Reference: WideString);
begin
  ChangeList.Values[Old]:= Reference;
end;

procedure TGenericPSDEditor.AddDelete(Reference: WideString);
begin
  ChangeList.Values[Reference]:= ' ';
end;

end.
