program TestHPost;

{%File 'test.txt'}
{%File 'test.i'}
{%File 'box.txt'}

uses
  Forms,
  TestForm in 'TestForm.pas' {Form1},
  HeidenhainModel in 'HeidenhainModel.pas',
  MacroParser in 'MacroParser.pas',
  INCParser in 'INCParser.pas',
  CNCParserModel in 'CNCParserModel.pas',
  NCParser in 'NCParser.pas',
  MacroHost in 'MacroHost.pas',
  MacroVariables in 'MacroVariables.pas',
  SimpleIniModel in 'SimpleIniModel.pas',
  SimpleTest in 'SimpleTest.pas' {Form2},
  NullModel in 'NullModel.pas',
  ParsingIniFile in 'ParsingIniFile.pas',
  INamedInterface in '..\..\..\..\sdk1.4\INamedInterface.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TForm1, Form1);
  //Application.CreateForm(TForm2, Form2);
  Application.Run;
end.
