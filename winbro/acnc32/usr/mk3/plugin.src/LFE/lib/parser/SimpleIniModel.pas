unit SimpleIniModel;

interface

uses Classes, SysUtils, INCParser, IniFiles, INamedInterface;

type
  TParserHostAddReport = procedure(const Report: WideString; Oper: Boolean) of object;

  TSimpleIniModel = class(TInterfacedObject, INCParserHost)
  private
    C: TMemIniFile;
    FeatureID: Integer;
    FFeatureCount: Integer;
    FReport: TParserHostAddReport;
    FResolver: INCParserResolver;
  public
    constructor Create(Config: string);
    destructor Destroy; override;

    function Resolve(const Token: WideString; out Value: WideString): Boolean; stdcall;
    procedure FirstFeature; stdcall;
    procedure NextFeature; stdcall;
    function FeatureCount: Integer; stdcall;
    procedure AddReport(const Report: WideString; Oper: Boolean); stdcall;
    procedure SetFeatureIndex(Index: Integer); stdcall;

    property Report: TParserHostAddReport read FReport write FReport;
    property Resolver: INCParserResolver read FResolver write FResolver;
  end;

const
  OddValue = '+!NN-ooops';

implementation

{ TSimpleIniModel }

procedure TSimpleIniModel.NextFeature;
begin
  Inc(FeatureID);
end;

function TSimpleIniModel.FeatureCount: Integer;
begin
  Result:= FFeatureCount;
end;

function TSimpleIniModel.Resolve(const Token: WideString;
  out Value: WideString): Boolean;
begin
  Value:= C.ReadString('Domain', Token, OddValue);
  Result:= Value <> OddValue;

  if not Result then begin
    Value:= C.ReadString('Feature' + IntToStr(FeatureID), Token, OddValue);
    Result:= Value <> OddValue
  end;

  if not Result and Assigned(Resolver) then begin
    Result:= Resolver.Resolve(Token, Value);
  end;

  if not Result then begin
    Result:= True;
    Value:= '$' + Token;
  end;
end;

procedure TSimpleIniModel.SetFeatureIndex(Index: Integer);
begin
  FeatureID:= Index;
end;

procedure TSimpleIniModel.AddReport(const Report: WideString; Oper: Boolean);
begin
  if Assigned(FReport) then
    FReport(Report, Oper);
end;

procedure TSimpleIniModel.FirstFeature;
begin
  FeatureID:= 0;
end;

constructor TSimpleIniModel.Create(Config: string);
begin
  FeatureId:= 0;
  C:= TMemIniFile.Create(Config);
  FFeatureCount:= C.ReadInteger('Main', 'FeatureCount', 1);
end;

destructor TSimpleIniModel.Destroy;
begin
  C.Free;
  inherited;
end;

end.
