unit MacroParser;

interface

uses SysUtils, Classes, CNCParserModel, INCParser;

type
  (**
  Not re-entrant due to instance variables Ins and Outs
  *)
  TMacroParser = class
  private
    FResolver: INCParserHost;
    Model: TCNCParserModel;
    Ins: TStringList;
    Outs: TStream;
    FPreserveSpace: Boolean;
    FPreserveCase: Boolean;
    FActiveDirectory: string;
    GlobalRepeatCount: Integer;
    procedure WriteToStream(Stream: TStream; const Text: string);
    function Resolve(const Name: string; Throw: Boolean = True): WideString;
    function ParseChunk(StartLine: Integer; Conditional: Boolean = False): Integer;
    function TokenReplace(const Name: string): string;
    function Seperator(C: Char): Boolean;
    function ReadDelimited(var S : string; Delimiter : Char = ' ') : string;
  public
    constructor Create(Host: INCParserHost; Model: TCNCParserModel);
    procedure ParseFile(Ins: TStream; Outs: TStream);
    procedure EndParse;
    property Resolver: INCParserHost read FResolver write FResolver;
    property PreserveSpace: Boolean read FPreserveSpace write FPreserveSpace;
    property PreserveCase: Boolean read FPreserveCase write FPreserveCase;
    property ActiveDirectory: string read FActiveDirectory write FActiveDirectory;
  end;

implementation

const Space = ' ';

{ TParser }

{ Simple read characters from string until space ' ' }
function TMacroParser.ReadDelimited(var S : string; Delimiter : Char = ' ') : string;
var P : Integer;
    I: Integer;
    C: Char;
begin
  if PreserveSpace then begin
    for I:= 1 to Length(S) do begin
      if S[I] = ' ' then begin
        C:= ' ';
        Outs.Write(C, 1);
      end
      else
        Break;
    end;
  end;
  S:= Trim(S);
  P := Pos(Delimiter, S);
  if P = 0 then begin
    Result := Trim(S);
    S := '';
  end else begin
    Result := Trim(Copy(S, 1, P - 1));
    System.Delete(S, 1, P);
  end;
end;

procedure TMacroParser.ParseFile(Ins: TStream; Outs: TStream);
begin
  GlobalRepeatCount;
  Self.Outs:= Outs;
  Self.Ins.LoadFromStream(Ins);
  ParseChunk(0);
end;

function TMacroParser.ParseChunk(StartLine: Integer; Conditional: Boolean): Integer;
var Line: Integer;
    Buffer: string;
    Token, Work: string;
begin
  Line:= StartLine;

  try
    while Line < Ins.Count do begin

      Buffer:= Ins[Line]; //Trim(Ins[Line]);
      if PreserveCase then
        Token:= ReadDelimited(Buffer)
      else
        Token:= UpperCase(ReadDelimited(Buffer));

      if Token <> '' then begin
        if Token[1] = '@' then begin
          if Token = '@LABEL' then begin
            Work:= Model.AllocateLabel;
            Model.SetEnviron(ReadDelimited(Buffer), Work);
            Inc(Line);
          end
          else if Token = '@LOCAL' then begin
            Model.AllocateLocalVariable(ReadDelimited(Buffer));
            Inc(Line);
          end
          else if Token = '@REPORT' then begin
            Resolver.AddReport(TokenReplace(Buffer), False);
            Inc(Line);
          end
          else if Token = '@OPERATOR' then begin
            Resolver.AddReport(TokenReplace(Buffer), True);
            Inc(Line);
          end
          else if Token = '@NL' then begin
            Model.WithLineNumbers:= False;
            Inc(Line);
          end
          {
          else if Token = '@SET' then begin
            Work:= ReadDelimited(Buffer);
            Model.SetEnviron(Work, TokenReplace(Buffer));
            Inc(Line);
          end
          else if Token = '@ADD' then begin
            Work:= ReadDelimited(Buffer);
            Model.ModifyEnviron(Work, Buffer);
            Inc(Line);
          end
          }
          else if Token = '@//' then begin
            Inc(Line);
          end
          else if Token = '@ERROR' then begin
            try
              Token:= TokenReplace(Buffer);
            except
              raise Exception.Create('Template aborted parsing, ' + TokenReplace(Buffer));
            end;
            raise Exception.Create('Template aborted parsing, ' + TokenReplace(Token));
          end
          else
            raise Exception.Create('Invalid Preprocessor Directive: ' + Token);

          Continue;
        end
      end;

      Model.StartLine(Outs);
      Model.WithLineNumbers:= True;

      Work:= '';
      while Token <> '' do begin
        if Work <> '' then
          Work:= Work + ' ' + TokenReplace(Token)
        else
          Work:= Work + TokenReplace(Token);

        if PreserveCase then
          Token:= ReadDelimited(Buffer)
        else
          Token:= UpperCase(ReadDelimited(Buffer));
      end;
      WriteToStream(Outs, Work);
      Model.EndLine(Outs);
      Inc(Line);
    end;
  except
    on E: Exception do
      raise Exception.CreateFmt('[%s] @ Line %d', [E.message, Line]);
  end;

  Result:= 0;
end;


constructor TMacroParser.Create(Host: INCParserHost; Model: TCNCParserModel);
begin
  Self.Model:= Model;
  Self.Resolver:= Host;
  Ins:= TStringList.Create;
end;

procedure TMacroParser.WriteToStream(Stream: TStream; const Text: string);
begin
  Stream.Write(Pointer(Text)^, Length(Text));
end;

{ Resolves a $ prefixed variable }
function TMacroParser.Resolve(const Name: string; Throw: Boolean): WideString;
var Tmp: string;
begin
  Result:= '';
  Tmp:= System.Copy( Name, 2, Length(Name));

  if Model.Resolve(Tmp, Result) then
    Exit;

  if Assigned(Resolver) then
    if not Resolver.Resolve(Tmp, Result) then
      if Throw then
        raise Exception.CreateFmt('Unable to resolve [%s] to any value', [Name])
end;

(**
  Replaces any $xxx[seperator] with the result of the resolver
  Catches $$ and removes preceeding $ from output without calling resolver
*)
function TMacroParser.TokenReplace(const Name: string): string;
var I, S: Integer;
    Tmp: string;
begin
  Result:= '';
  I:= 1;
  while I <= Length(Name) do begin
    if Name[I] = '$' then begin
      if I >= Length(Name) then
        raise Exception.Create('Prefix token without content');

      S:= I;
      Inc(I);

      if Name[I] <> '$' then begin
        while (I <= Length(Name)) and not Seperator(Name[I]) do
          Inc(I);

        Tmp:= Trim(Copy(Name, S, I-S));
        Result:= Result + Resolve(Tmp);
      end else begin
        Result:= Result + Name[I];
        Inc(I);
      end;

    end
    else begin
      Result:= Result + Name[I];
      Inc(I);
    end;
  end;
end;

function TMacroParser.Seperator(C: Char): Boolean;
begin
  Result:= True;
  case C of
    'a'..'z',
    'A'..'Z',
    '0'..'9',
    '_' : Result:= False;
  end;
end;

procedure TMacroParser.EndParse;
begin
  Model.LastLine(Outs);
end;


end.
