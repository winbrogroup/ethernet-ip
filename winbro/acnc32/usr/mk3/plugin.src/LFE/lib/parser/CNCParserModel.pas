unit CNCParserModel;

interface

uses SysUtils, Classes;

type
  TResolveMethod = function(const Name: string; Throw: Boolean = True): WideString of object;

  TCNCParserModel = class
  private
  protected
    FFilename: string;
    function GetOutputLine: Integer; virtual; abstract;
    function GetWithLineNumbers: Boolean; virtual; abstract;
    procedure SetWithLineNumbers(const Value: Boolean); virtual; abstract;
  public
    procedure PushState; virtual; abstract;
    procedure PopState; virtual; abstract;

    procedure AllocateGlobalVariable(const Name: string); virtual; abstract;
    procedure AllocateLocalVariable(const Name: string); virtual; abstract;
    function AllocateLabel: string; virtual; abstract;

    procedure StartLine(Outs: TStream); virtual; abstract;
    procedure EndLine(Outs: TStream); virtual; abstract;

    procedure SetEnviron(const AEnv, AValue: string); virtual; abstract;
    procedure ModifyEnviron(const AEnv, AValue: string); virtual; abstract;

    function Resolve(const Name: string; var Res: WideString): Boolean; virtual; abstract;

    procedure LastLine(Outs: TStream); virtual; abstract;

    property OutputLine: Integer read GetOutputLine;
    property Filename: string read FFilename write FFilename;

    property WithLineNumbers: Boolean read GetWithLineNumbers write SetWithLineNumbers;
  end;

  { Stupid class that wraps a string so that it can be treated as an object }
  TStringThing = class
    SoddingDelphi: string;
    constructor Create(AString: string);
  end;

implementation


{ TStringThing }

constructor TStringThing.Create(AString: string);
begin
  Self.SoddingDelphi:= AString;
end;

end.
