unit MacroVariables;

interface

uses Classes, SysUtils;

type
  TMacroVariables = class
    Parameters: TStringList;
  public
    constructor Create;
    destructor Destroy; override;
    procedure AddParameter(Value: string);
    procedure Clear;
    function Resolve(const Token: WideString; out Value: WideString): Boolean;
  end;

implementation

{ TMacroVariables }

constructor TMacroVariables.Create;
begin
  Parameters:= TStringList.Create;
end;

function TMacroVariables.Resolve(const Token: WideString;
  out Value: WideString): Boolean;
var I: Integer;
begin
  Result:= True;
  I:= StrToIntDef(Token, 0) - 1;
  if (I < 0) or (I >= Parameters.Count) then
    Result:= False
  else
    Value:= Parameters[I];
end;

procedure TMacroVariables.Clear;
begin
  Parameters.Clear;
end;

destructor TMacroVariables.Destroy;
begin
  Parameters.Free;
  inherited;
end;

procedure TMacroVariables.AddParameter(Value: string);
begin
  Parameters.Add(Value);
end;

end.
