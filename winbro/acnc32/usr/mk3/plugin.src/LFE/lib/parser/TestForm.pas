unit TestForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, NCParser, INCParser, CNCParserModel, HeidenhainModel, NullModel;

type
  TForm1 = class(TForm, INCParserHost)
    Memo1: TMemo;
    procedure FormCreate(Sender: TObject);
  private
    FeatureIndex: Integer;
    Model: TCNCParserModel;
    procedure FormCreate1(Sender: TObject);
  public
    function Resolve(const Token: WideString; out Value: WideString): Boolean; stdcall;
    procedure FirstFeature; stdcall;
    procedure NextFeature; stdcall;
    function FeatureCount: Integer; stdcall;
    procedure AddReport(const Report: WideString; Oper: Boolean); stdcall;
    procedure SetFeatureIndex(Index: Integer); stdcall;
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.AddReport(const Report: WideString; Oper: Boolean);
begin
  if Oper then
    Memo1.Lines.Add('Operator ' + IntToStr(Model.OutputLine) + ' ' + Report)
  else
    Memo1.Lines.Add('System   ' + IntToStr(Model.OutputLine) + ' ' + Report)
end;

function TForm1.FeatureCount: Integer;
begin
  Result:= 10;
end;

procedure TForm1.FirstFeature;
begin
  FeatureIndex:= 0;
end;

procedure TForm1.FormCreate(Sender: TObject);
var Parser: TNCParser;
    Outs: TFileStream;
    Ins: TFileStream;
begin
  Model:= THeidenhainModel.Create(False);
  Model.Filename:= 'test';
  THeidenhainModel(Model).SetGlobalRange(1, 50);
  THeidenhainModel(Model).SetLocalRange(51,99);
  THeidenhainModel(Model).SetLabelRange(10,100);
  THeidenhainModel(Model).AddStaticVariable('STATIC', 299);

  Outs:= TFileStream.Create('test.i', fmCreate);

  Parser:= TNCParser.Create(Self, Model);
  Parser.IgnoreBlankLines:= True;
  //Parser.Resolver:= Self;

  // parse machine specific header

  // for each Operation
      // Find PSDE for operation

      // PSDE.BeginPost(XmlDocument)
      // try
      // Derive file name from current machine type, cnc etc
      //Ins:= TFileStream.Create('test.txt', fmOpenRead);
      Ins:= TFileStream.Create('PSDE_MLASER_SCAN.txt', fmOpenRead);

      Parser.PreserveSpace:= False;
      Parser.ActiveDirectory:= 'Macro\';
      Parser.ParseFile(Ins, Outs);
      Ins.Seek(0, soFromBeginning);
      //Parser.ParseFile(Ins, Outs);

      // finally
      //   PSDE.EndPost  // Allows the PSDE to nil its reference to the master document
      // end

  // END Foreach operation

  Memo1.Lines.Add('Resolve count   ' + IntToStr(Parser.ResolveCount));
  Parser.EndParse;
  // parse machine specific footer
  Outs.Free;
  Ins.Free;
end;


procedure TForm1.NextFeature;
begin
  Inc(FeatureIndex);
  //1. Keep a record of the feature index
  //2. PSDE.SetReference( get the reference from the model for this hole )
end;

function TForm1.Resolve(const Token: WideString;
  out Value: WideString): Boolean;
begin
  // If you implement a lookup like this, place the most frequently used at the
  // top
  if Token = 'XPOS' then
    Value:= '0.123'
  else if Token = 'YPOS' then
    Value:= '0.124'
  else if Token = 'ZPOS' then
    Value:= '0.125'
  else if Token = 'BPOS' then
    Value:= '0.126'
  else if Token = 'CPOS' then
    Value:= '0.127'
  else if Token = 'FEATURE_ID' then
    Value:= 'Feature ' + InttoStr(FeatureIndex + 1)
  else if Token = 'FILENAME' then
    Value:= 'test'
  else if Token = 'FEATURE_INDEX' then
    Value:= IntToStr(FeatureIndex)
  else if Token = 'GENERATOR' then
    Value:= 'LFE System (c) Winbro Group Technologies 2006-2007'
  else if Token = 'AUTHOR' then
    Value:= 'Mr. Bloggity Blog'
  else if Token = 'DATE' then
    Value:= DateToStr(Now)
  else begin
    // PSDE.Resolve
    // IPSDEHost.GetElementByName ??
    // If no one can resolve then return false
    Value:= '25'
  end;

  Result:= True;
end;

procedure TForm1.SetFeatureIndex(Index: Integer);
begin
  FeatureIndex:= Index;
end;

procedure TForm1.FormCreate1(Sender: TObject);
var Parser: TNCParser;
    Outs: TFileStream;
    Ins: TFileStream;
begin
  Model:= TNullModel.Create;
  Model.Filename:= 'test';

  Outs:= TFileStream.Create('cnc1.ini', fmCreate);

  Parser:= TNCParser.Create(Self, Model);
  Parser.IgnoreBlankLines:= True;

      Ins:= TFileStream.Create('cnc.ini', fmOpenRead);
      Parser.ParseFile(Ins, Outs);
      Ins.Seek(0, soFromBeginning);

  Memo1.Lines.Add('Resolve count   ' + IntToStr(Parser.ResolveCount));
  Parser.EndParse;

  Outs.Free;

end;

end.
