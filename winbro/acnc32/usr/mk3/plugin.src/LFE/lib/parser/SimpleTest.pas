unit SimpleTest;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, SimpleIniModel, NCParser, CNCParserModel, NullModel;

type
  TForm2 = class(TForm)
    procedure FormCreate(Sender: TObject);
  private
    Host: TSimpleIniModel;
    Model: TCNCParserModel;
  public
    { Public declarations }
  end;

var
  Form2: TForm2;

implementation

{$R *.dfm}

procedure TForm2.FormCreate(Sender: TObject);
var Parser: TNCParser;
    Outs: TFileStream;
    Ins: TFileStream;
begin

  Host:= TSimpleIniModel.Create('SimpleConfig.txt');
  Model:= TNullModel.Create;
  Model.Filename:= 'test';

  Outs:= TFileStream.Create('cnc1.ini', fmCreate);

  Parser:= TNCParser.Create(Host, Model);
  Parser.IgnoreBlankLines:= False;
  Parser.PreserveCase:= True;
  Parser.PreserveSpace:= False;
  Parser.PreserveLines:= True;

      Ins:= TFileStream.Create('cnc.ini', fmOpenRead);
      Parser.ParseFile(Ins, Outs);
      Ins.Seek(0, soFromBeginning);

  Parser.EndParse;

  Outs.Free;

end;

end.
