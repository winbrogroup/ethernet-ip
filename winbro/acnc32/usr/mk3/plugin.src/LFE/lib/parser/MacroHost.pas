unit MacroHost;

interface

uses SysUtils, Classes, INCParser;

type
  TMacroHost = class(TInterfacedObject, INCParserHost)
  private
    Parameters: TStringList;
    Parent: INCParserHost;
  public
    constructor Create(ParentResolver: INCParserHost);
    destructor Destroy; override;
    procedure AddParameter(Value: string);
    procedure Clear;

    // INCParser
    function Resolve(const Token: WideString; out Value: WideString): Boolean; stdcall;
    procedure FirstFeature; stdcall;
    procedure NextFeature; stdcall;
    function FeatureCount: Integer; stdcall;
    procedure AddReport(const Report: WideString; Oper: Boolean); stdcall;
    procedure SetFeatureIndex(Index: Integer); stdcall;
  end;

implementation

{ TMacroHost }

constructor TMacroHost.Create(ParentResolver: INCParserHost);
begin
  Parent:= ParentResolver;
  Parameters:= TStringList.Create;
end;

destructor TMacroHost.Destroy;
begin
  Parameters.Free;
  inherited;
end;


procedure TMacroHost.NextFeature;
begin
  // Error
end;

function TMacroHost.FeatureCount: Integer;
begin
  Result := 0; // To be implemented
end;

function TMacroHost.Resolve(const Token: WideString;
  out Value: WideString): Boolean;
var I: Integer;
begin
  Result:= True;
  I:= StrToIntDef(Token, 0) - 1;
  if (I < 0) or (I >= Parameters.Count) then
    Result:= False
  else
    Value:= Parameters[I];
end;

procedure TMacroHost.AddParameter(Value: string);
begin
  Parameters.Add(Value);
end;

procedure TMacroHost.AddReport(const Report: WideString; Oper: Boolean);
begin
  Parent.AddReport(Report, Oper);
end;

procedure TMacroHost.Clear;
begin
  Parameters.Clear;
end;

procedure TMacroHost.FirstFeature;
begin
  // Error
end;


procedure TMacroHost.SetFeatureIndex(Index: Integer);
begin
  // Error
end;

end.
