unit HeidenhainModel;

interface

uses SysUtils, Classes, CNCParserModel;

type
  THeidenhainModel = class(TCNCParserModel)
  private
    LocalVariables: TStringList;
    GlobalVariables: TStringList;
    Environment: TStringList;

    StaticVariables: TStringList;

    LocalStack: TList;

    LocalStart: Integer;
    LocalTotal: Integer;

    GlobalStart: Integer;
    GlobalTotal: Integer;

    LabelCount: Integer;
    LabelStart: Integer;
    LabelTotal: Integer;

    FOutputLine: Integer;
    FISO: Boolean;
    FInch: Boolean;
    FWithLineNumbers: Boolean;
    procedure FirstLine(Outs: TStream);
  protected
    function GetOutputLine: Integer; override;
    function GetWithLineNumbers: Boolean; override;
    procedure SetWithLineNumbers(const Value: Boolean); override;
  public
    constructor Create(ISO: Boolean);
    destructor Destroy; override;
    procedure SetGlobalRange(StartVar, EndVar: Integer);
    procedure SetLocalRange(StartVar, EndVar: Integer);
    procedure SetLabelRange(StartVar, EndVar: Integer);

    procedure AddStaticVariable(AName: string; Index: Integer);

    procedure PushState; override;
    procedure PopState; override;
    procedure AllocateGlobalVariable(const Name: string); override;
    procedure AllocateLocalVariable(const Name: string); override;
    function AllocateLabel: string; override;
    procedure StartLine(Outs: TStream); override;
    procedure EndLine(Outs: TStream); override;
    procedure LastLine(Outs: TStream); override;
    function Resolve(const Name: string; var Res: WideString): Boolean; override;
    procedure SetEnviron(const AEnv, AValue: string); override;
    procedure ModifyEnviron(const AEnv, AValue: string); override;


    property ISO: Boolean read FISO;
    property Inch: Boolean read FInch write FInch;
  end;


  { Stupid class that wraps a string so that it can be treated as an object }
  TStringThing = class
    SoddingDelphi: string;
    constructor Create(AString: string);
  end;

implementation

{ TCNCModel }


{ TCNCModel }

procedure THeidenhainModel.PopState;
var Tmp: Integer;
begin
  if LocalStack.Count = 0 then
    raise Exception.Create('Heidenhain local stack underflow, @POP @PUSH mismatch');

  Tmp:= Integer(LocalStack[LocalStack.Count - 1]);
  LocalStack.Delete(LocalStack.Count - 1);

  while LocalVariables.Count > Tmp do
    LocalVariables.Delete(LocalVariables.Count - 1);

end;


procedure THeidenhainModel.AllocateLocalVariable(const Name: string);
begin
  if LocalVariables.Count > LocalTotal then
    raise Exception.Create('Exhausted Local variable range: ' + IntToStr(LocalStart) + ':' + IntToStr(LocalStart + LocalTotal));

  LocalVariables.Add(Name);
end;

procedure THeidenhainModel.AllocateGlobalVariable(const Name: string);
begin
  if GlobalVariables.Count > GlobalTotal then
    raise Exception.Create('Exhausted Global variable range: ' + IntToStr(GlobalStart) + ':' + IntToStr(GlobalStart + GlobalTotal));

  GlobalVariables.Add(Name);
end;

constructor THeidenhainModel.Create(ISO: Boolean);
begin
  FISO:= ISO;
  LocalVariables:= TStringList.Create;
  GlobalVariables:= TStringList.Create;
  StaticVariables:= TStringList.Create;
  StaticVariables.Sorted:= True;
  StaticVariables.Duplicates:= dupAccept;

  Environment:= TStringList.Create;

  LocalStack:= TList.Create;
  LabelStart:= 1;
  LabelTotal:= 100;
  GlobalStart:= 1;
  GlobalTotal:= 50;
  LocalStart:= 51;
  LocalTotal:= 50;

  FWithLineNumbers:= True;
end;


destructor THeidenhainModel.Destroy;
begin
  while Environment.Count > 0 do begin
    Environment.Objects[0].Free;
    Environment.Delete(0);
  end;

  LocalVariables.Free;
  GlobalVariables.Free;
  Environment.Free;
  inherited;
end;

procedure THeidenhainModel.SetLabelRange(StartVar, EndVar: Integer);
begin
  LabelStart:= StartVar;
  LabelTotal:= EndVar - StartVar;
end;

procedure THeidenhainModel.SetGlobalRange(StartVar, EndVar: Integer);
begin
  GlobalStart:= StartVar;
  GlobalTotal:= EndVar - StartVar;
end;


procedure THeidenhainModel.AddStaticVariable(AName: string; Index: Integer);
begin
  StaticVariables.AddObject(AName, Pointer(Index));
end;

procedure THeidenhainModel.PushState;
begin
  LocalStack.Add(Pointer(LocalVariables.Count));
end;

function THeidenhainModel.AllocateLabel: string;
begin
  if LabelCount >= LabelTotal then
    raise Exception.Create('Exhausted target label range: ' + IntToStr(LabelStart) + ':' + IntToStr(LabelStart + LabelTotal));

  Result:= IntToStr(LabelCount + LabelStart);
  Inc(LabelCount);
end;

procedure THeidenhainModel.SetLocalRange(StartVar, EndVar: Integer);
begin
  LocalStart:= StartVar;
  LocalTotal:= EndVar - StartVar;
end;

procedure THeidenhainModel.StartLine(Outs: TStream);
var Tmp: string;
begin
  if FOutputLine = 0 then begin
    FirstLine(Outs);
    Inc(FOutputLine);
  end;

  if WithLineNumbers then begin
    if ISO then begin
      Tmp:= Format('N%.4d ', [FOutputLine]);
    end else begin
      Tmp:= Format('%d ', [FOutputLine]);
    end;
    Outs.Write(PChar(Tmp)^, Length(Tmp));
    Inc(FOutputLine);
  end;
end;

function THeidenhainModel.Resolve(const Name: string; var Res: WideString): Boolean;
var I: Integer;
    Tmp: string;
begin
  Tmp:= UpperCase(Name);
  Result:= True;
  I:= Environment.IndexOf(Tmp);
  if I <> -1 then begin
    Res:= TStringThing(Environment.Objects[I]).SoddingDelphi;
    Exit;
  end;

  I:= LocalVariables.IndexOf(Name);
  if I <> -1 then begin
    Res:= 'Q' + IntToStr(LocalStart + I);
    Exit;
  end;

  I:= GlobalVariables.IndexOf(Name);
  if I <> -1 then begin
    Res:= 'Q' + IntToStr(GlobalStart + I);
    Exit;
  end;

  I:= StaticVariables.IndexOf(Name);
  if I <> -1 then begin
    Res:= 'Q' + IntToStr(Integer(StaticVariables.Objects[I]));
    Exit;
  end;
  Result:= False;
end;

procedure THeidenhainModel.EndLine(Outs: TStream);
var Tmp: string;
begin
  if ISO then
    Tmp:= Format('*' + #$0d + #$0a, [FOutputLine])
  else
    Tmp:= Format(#$0d + #$0a, [FOutputLine]);

  Outs.Write(PChar(Tmp)^, Length(Tmp));
end;

function THeidenhainModel.GetOutputLine: Integer;
begin
  Result:= FOutputLine;
end;

procedure THeidenhainModel.FirstLine(Outs: TStream);
var Tmp: string;
begin
  if ISO then begin
    Tmp:= '%' + FFilename + ' ';
    if FInch then
      Tmp:= Tmp + 'G70'
    else
      Tmp:= Tmp + 'G71';

     Tmp:= Tmp + ' *' + #$0d + #$0a;
  end else begin
    Tmp:= 'BEGIN PGM ' + FFilename + ' MM' + #$0d + #$0a;
    if WithLineNumbers then
      Tmp:= '0 ' + Tmp;
  end;
  Outs.Write(PChar(Tmp)^, Length(Tmp));
end;

procedure THeidenhainModel.LastLine(Outs: TStream);
var Tmp: string;
begin
  if ISO then begin
    Tmp:= 'N99999999 %' + FFilename + ' ';
    if FInch then
      Tmp:= Tmp + 'G70'
    else
      Tmp:= Tmp + 'G71';

    Tmp:= Tmp + ' *' + #$0d + #$0a;
  end else begin
    Tmp:= 'END PGM ' + FFilename + ' MM' + #$0d + #$0a;
    if WithLineNumbers then
      Tmp:= IntToStr(FOutputLine) + ' ' + Tmp;
  end;
  Outs.Write(PChar(Tmp)^, Length(Tmp));
end;

procedure THeidenhainModel.SetWithLineNumbers(const Value: Boolean);
begin
  FWithLineNumbers:= Value;
end;

function THeidenhainModel.GetWithLineNumbers: Boolean;
begin
  Result:= FWithLineNumbers;
end;

procedure THeidenhainModel.SetEnviron(const AEnv, AValue: string);
var Tmp: string;
    I: Integer;
begin
  Tmp:= UpperCase(AEnv);
  I:= Environment.IndexOf(Tmp);

  if I <> -1 then begin
    Environment.Objects[I].Free;
    Environment.Objects[I]:= TStringThing.Create(AValue);
  end else begin
    Environment.AddObject(Tmp, TStringThing.Create(AValue));
  end;
end;

procedure THeidenhainModel.ModifyEnviron(const AEnv, AValue: string);
var Tmp: string;
    F1: Double;
    I: Integer;
begin
  Tmp:= UpperCase(AEnv);
  I:= Environment.IndexOf(Tmp);
  if I <> -1 then begin
    Tmp:= TStringThing(Environment.Objects[I]).SoddingDelphi;
    F1:= StrToFloatDef(Tmp, 0);
    F1:= F1 + StrToFloatDef(AValue, 0);
    Tmp:= Format('%-7.4f',[F1]);;
    TStringThing(Environment.Objects[I]).SoddingDelphi:= Tmp;
    Exit;
  end;
  raise Exception.Create('Variable ' + Tmp + ' does not exist');
end;


{ TStringThing }

constructor TStringThing.Create(AString: string);
begin
  Self.SoddingDelphi:= AString;
end;
end.
