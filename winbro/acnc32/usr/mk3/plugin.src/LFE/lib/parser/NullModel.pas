unit NullModel;

interface

uses
  Classes, SysUtils, CNCParserModel;

type
  TNullModel = class(TCNCParserModel)
  private
    Environment: TStringList;
    FOutputLine: Integer;
  public
    function GetOutputLine: Integer; override;
    function GetWithLineNumbers: Boolean; override;
    procedure SetWithLineNumbers(const Value: Boolean); override;

    constructor Create;
    procedure PushState; override;
    procedure PopState; override;

    procedure AllocateGlobalVariable(const Name: string); override;
    procedure AllocateLocalVariable(const Name: string); override;
    function AllocateLabel: string; override;

    procedure StartLine(Outs: TStream); override;
    procedure EndLine(Outs: TStream); override;

    procedure SetEnviron(const AEnv, AValue: string); override;
    procedure ModifyEnviron(const AEnv, AValue: string); override;

    function Resolve(const Name: string; var Res: WideString): Boolean; override;

    procedure LastLine(Outs: TStream); override;
  end;

implementation

{ TNullModel }

procedure TNullModel.PopState;
begin
end;

procedure TNullModel.ModifyEnviron(const AEnv, AValue: string);
var Tmp: string;
    F1: Double;
    I: Integer;
begin
  Tmp:= UpperCase(AEnv);
  I:= Environment.IndexOf(Tmp);
  if I <> -1 then begin
    Tmp:= TStringThing(Environment.Objects[I]).SoddingDelphi;
    F1:= StrToFloatDef(Tmp, 0);
    F1:= F1 + StrToFloatDef(AValue, 0);
    Tmp:= FloatToStr(F1);
    TStringThing(Environment.Objects[I]).SoddingDelphi:= Tmp;
    Exit;
  end;
  raise Exception.Create('Variable ' + Tmp + ' does not exist');
end;

procedure TNullModel.AllocateGlobalVariable(const Name: string);
begin
  raise Exception.Create('Global variables are not available: use environment variables');
end;

procedure TNullModel.StartLine(Outs: TStream);
begin
  Inc(FOutputLine);
end;

function TNullModel.Resolve(const Name: string; var Res: WideString): Boolean;
var I: Integer;
    Tmp: string;
begin
  Tmp:= UpperCase(Name);
  Result:= True;
  I:= Environment.IndexOf(Tmp);
  if I <> -1 then begin
    Res:= TStringThing(Environment.Objects[I]).SoddingDelphi;
    Exit;
  end;

  Result:= False;
end;

procedure TNullModel.SetEnviron(const AEnv, AValue: string);
var Tmp: string;
    I: Integer;
begin
  Tmp:= UpperCase(AEnv);
  I:= Environment.IndexOf(Tmp);

  if I <> -1 then begin
    Environment.Objects[I].Free;
    Environment.Objects[I]:= TStringThing.Create(AValue);
  end else begin
    Environment.AddObject(Tmp, TStringThing.Create(AValue));
  end;
end;

procedure TNullModel.EndLine(Outs: TStream);
var Tmp: string;
begin
  Tmp:= #$0d + #$0a;
  Outs.Write(PChar(Tmp)^, Length(Tmp));
end;

procedure TNullModel.AllocateLocalVariable(const Name: string);
begin
  raise Exception.Create('Local variables are not available: use environment variables');
end;

procedure TNullModel.PushState;
begin
end;

procedure TNullModel.LastLine(Outs: TStream);
var Tmp: string;
begin
  Tmp:= #$0d + #$0a;
  Outs.Write(PChar(Tmp)^, Length(Tmp));
end;

function TNullModel.AllocateLabel: string;
begin
  raise Exception.Create('Labels are not available: they have meaning without a concrete NC type');
end;

constructor TNullModel.Create;
begin
  Environment:= TStringList.Create;
end;

procedure TNullModel.SetWithLineNumbers(const Value: Boolean);
begin
end;

function TNullModel.GetOutputLine: Integer;
begin
  Result:= FOutputLine;
end;

function TNullModel.GetWithLineNumbers: Boolean;
begin
  Result:= False;
end;

end.
