unit INCParser;

interface

uses INamedInterface;

type
  INCParserHost = interface(INCParserResolver)
  ['{F47ABE38-95FE-4338-94F7-0DBCBAB994EE}']

    { Asks the system to select the first feature of the currently selected
      operation.  Throw an exception if there are no selected features }
    procedure FirstFeature; stdcall;

    { Move the internal pointer of selected feature to the next feature, throw
      an exception if this is after the last feature }
    procedure NextFeature; stdcall;

    { Returns the number of features in this operation }
    function FeatureCount: Integer; stdcall;

    { While parsing the template the template can request two types of report
     operator and system }
    procedure AddReport(const Report: WideString; Oper: Boolean); stdcall;

    { Sets the current feature index
      The model should throw an error if this is outside the range of features}
    procedure SetFeatureIndex(Index: Integer); stdcall;
  end;

implementation

end.
