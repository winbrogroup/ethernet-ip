unit Parser;

interface

uses SysUtils, Classes, CNCModel;

type
  TVariableResolver = function (const Variable: string): string;

  TParser = class
  private
    FResolver: TVariableResolver;
    Model: TCNCModel;
  public
    constructor Create(Model: TCNCModel);
    procedure ParseFile(Ins: TStringList; Outs: TStream);
    property Resolver: TVariableResolver read FResolver write FResolver;
  end;

implementation

const Space = ' ';

{ TParser }

function ReadDelimited(var S : string; Delimiter : Char = ' ') : string;
var P : Integer;
begin
  P := Pos(Delimiter, S);
  if P = 0 then begin
    Result := Trim(S);
    S := '';
  end else begin
    Result := Trim(Copy(S, 1, P - 1));
    System.Delete(S, 1, P);
  end;
end;

procedure TParser.ParseFile(Ins: TStringList; Outs: TStream);
var Line: Integer;
    Buffer: string;
    Token: string;
begin
  Line:= 0;

  while Line < Ins.Count do begin
    Buffer:= Trim(Ins[Line]);
    Model.StartLine(Outs);
    Token:= UpperCase(ReadDelimited(Buffer));
    while Token <> '' do begin
      if(Token[1] = '@') or (Token[1] = '$') then begin
      end
      else begin
        Token:= Token + ' ';
        Outs.Write(Token, Length(Token));
      end;
      Token:= UpperCase(ReadDelimited(Buffer));
    end;
    Outs.Write(#$0d + #$0a, 2);
    Inc(Line);
  end;
end;

constructor TParser.Create(Model: TCNCModel);
begin
  Self.Model:= Model;
end;

end.
