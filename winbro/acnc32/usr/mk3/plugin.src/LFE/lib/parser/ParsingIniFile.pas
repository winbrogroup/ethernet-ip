unit ParsingIniFile;

interface

uses SysUtils, Classes, SimpleIniModel, IniFiles, CncParserModel, NullModel, NCParser,
     Windows, INCParser, INamedInterface;

type
  TParsingIniFile = class(TInterfacedObject, IConfigurationParser)
  private
    Host: TSimpleIniModel;
  public
    constructor Create(ConfigFile: string);
    function Parse(SourceFile: WideString): WideString; stdcall;
    procedure SetResolver(Resolver: INCParserResolver); stdcall;

    function _AddRef: Integer; stdcall;
    function _Release: Integer; stdcall;
    
    function ParseIni(SourceFile: string): TMemIniFile;
    function ParseString(SourceFile: string): string;
  end;

implementation

{ TParsingIniFile }

function GetTempDir: TFileName;
var
  TmpDir: array [0..MAX_PATH-1] of char;
begin
  SetString(Result, TmpDir, GetTempPath(MAX_PATH, TmpDir));
end;

function CreateTempFile: TFileName;
var
  TempFileName: array [0..MAX_PATH-1] of char;
begin
  if GetTempFileName(PChar(GetTempDir), '~', 0, TempFileName) = 0 then
    raise Exception.Create(SysErrorMessage(GetLastError));
  Result := TempFileName;
end;

function TParsingIniFile.ParseIni(SourceFile: string): TMemIniFile;
var
  OutputFilename: string;
begin
  OutputFilename:= Parse(SourceFile);
  Result:= TMemIniFile.Create(OutputFilename);
  DeleteFile(PChar(OutputFilename));
end;

constructor TParsingIniFile.Create(ConfigFile: string);
begin
  Host:= TSimpleIniModel.Create(ConfigFile);
end;

function TParsingIniFile.ParseString(SourceFile: string): string;
var
  OutputFilename: string;
  S: TStringList;
begin
  S:= TStringList.Create;
  try
    OutputFilename:= Parse(SourceFile);
    S.LoadFromFile(OutputFilename);
    DeleteFile(PChar(OutputFilename));
    Result:= S.Text;
  finally
    S.Free;
  end;
end;

function TParsingIniFile.Parse(SourceFile: WideString): WideString;
var
  Parser: TNCParser;
  Model: TCNCParserModel;
  Outs: TFileStream;
  Ins: TFileStream;
  OutputFilename: string;
begin
  OutputFilename:= CreateTempFile;

  Model:= TNullModel.Create;
  Model.Filename:= SourceFile;

  Outs:= TFileStream.Create(OutputFilename, fmCreate);

  Parser:= TNCParser.Create(Host, Model);
  Parser.IgnoreBlankLines:= False;
  Parser.PreserveCase:= True;
  //Parser.PreserveLines:= True;

  Ins:= TFileStream.Create(SourceFile, fmOpenRead);
  Parser.ParseFile(Ins, Outs);
  Ins.Seek(0, soFromBeginning);

  Parser.EndParse;
  Outs.Free;
  Ins.Free;
  //Parser.Free;
  Result:= OutputFilename;
end;

procedure TParsingIniFile.SetResolver(Resolver: INCParserResolver);
begin
  Host.Resolver:= Resolver;
end;

function TParsingIniFile._AddRef: Integer;
begin
  Result:= 2;
end;

function TParsingIniFile._Release: Integer;
begin
  Result:= 2;
end;

end.
