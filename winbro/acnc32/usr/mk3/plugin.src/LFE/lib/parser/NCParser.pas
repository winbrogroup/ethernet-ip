unit NCParser;

interface

uses SysUtils, Classes, CNCParserModel, INCParser, MacroParser, MacroHost, MacroVariables,
     NamedPlugin, CncTypes;

type
  EParserAbort = class(Exception)
  end;
  EParserException = class(Exception)
  end;

  (**
  Not re-entrant due to instance variables Ins and Outs
  *)
  TNCParser = class
  private
    Model: TCNCParserModel;
    Outs: TStream;

    FResolver: INCParserHost;
    Ins: TStringList;
    FPreserveSpace: Boolean;
    FPreserveCase: Boolean;
    FPreserveLines: Boolean;
    FActiveDirectory: string;
    GlobalRepeatCount: Integer;
    FResolveCount: Integer;
    PopRepeat: Boolean;
    FIgnoreBlankLines: Boolean;
    FMacroVars: TMacroVariables;
    FNoLineNumbers: Boolean;
    function ValidParameter(const Name: string): string;
    function IntegerResolve(const Name: string): Integer;
    function ParseChunk(StartLine: Integer; Conditional: Boolean = False): Integer;
    function TokenReplace(const Name: string): string;
    function Seperator(C: Char): Boolean;
    function Condition(Check: string): boolean;
    function Eatchunk(Line: Integer; All: Boolean; var Cont: Boolean): Integer;
    function EatRepeat(Line: Integer): Integer;
    procedure IncludeFile(Filename: string);
    procedure IncludeMacro(Domain: string);
    function ReadDelimited(var S : string; Delimiter : Char = ' ') : string;
    procedure SetActiveDirectory(const Value: string);
    function RunForeach(Line: Integer; Buffer: string): Integer;
    procedure SetNoLineNumbers(const Value: Boolean);
    function ReadFormat(const Token: string; var Buffer: string): string;
    function RunScript(JC: string; Work: string): string;
  public

    constructor Create(Host: INCParserHost; Model: TCNCParserModel);
    destructor Destroy; override;
    procedure ParseFile(Ins: TStream; Outs: TStream);
    procedure EndParse;
    property Resolver: INCParserHost read FResolver write FResolver;

    { PreserveSpace attempts to maintain the input white space in the output }
    property PreserveSpace: Boolean read FPreserveSpace write FPreserveSpace;

    { PreserveCase Determines if the output should match the case of the input }
    property PreserveCase: Boolean read FPreserveCase write FPreserveCase;
    property ActiveDirectory: string read FActiveDirectory write SetActiveDirectory;
    property ResolveCount: Integer read FResolveCount;

    { This determines if blank lines in the source are translated to the output
      Note that unless set to false "PreserveLines" will not work }
    property IgnoreBlankLines: Boolean read FIgnoreBlankLines write FIgnoreBlankLines;

    property NoLineNumbers : Boolean read FNoLineNumbers write SetNoLineNumbers;
    property MacroVars: TMacroVariables read FMacroVars write FMacroVars;

    { PreserveLines attempts to make the features of the input file match the
      output-file, this is only possible during simple parsing without loops }
    property PreserveLines: Boolean read FPreserveLines write FPreserveLines;

     // should be private
    function Resolve(const Name: string; Throw: Boolean = True): WideString;
    procedure WriteToStream(Stream: TStream; const Text: string);
  end;

implementation

uses JavaExt;

const Space = ' ';

var JavaExt: TJavaExt;


{ TParser }

{ Simple read characters from string until space ' ' }
function TNCParser.ReadDelimited(var S : string; Delimiter : Char = ' ') : string;
var P : Integer;
    I: Integer;
    C: Char;
begin
  if PreserveSpace then begin
    for I:= 1 to Length(S) do begin
      if S[I] = ' ' then begin
        C:= ' ';
        Outs.Write(C, 1);
      end
      else
        Break;
    end;
  end;
  S:= Trim(S);
  P := Pos(Delimiter, S);
  if P = 0 then begin
    Result := Trim(S);
    S := '';
  end else begin
    Result := Trim(Copy(S, 1, P - 1));
    System.Delete(S, 1, P);
  end;
end;

procedure TNCParser.ParseFile(Ins: TStream; Outs: TStream);
begin
  try
    GlobalRepeatCount;
    Self.Outs:= Outs;
    Self.Ins.LoadFromStream(Ins);
    ParseChunk(0);
  except
    on A: EParserAbort do
      Exit;
  end;
end;

function TNCParser.ParseChunk(StartLine: Integer; Conditional: Boolean): Integer;
var Line, NewPos, I: Integer;
    Buffer: string;
    Token, Work: string;
    Count: Integer;
    Cont: Boolean;
begin
  Line:= StartLine;

  try
    while Line < Ins.Count do begin
//      Model.WithLineNumbers := not FNoLineNumbers;
      Buffer:= Ins[Line]; //Trim(Ins[Line]);
      if PreserveCase then
        Token:= ReadDelimited(Buffer)
      else
        Token:= UpperCase(ReadDelimited(Buffer));

      if Token <> '' then begin
        if Token[1] = '@' then begin
          if PreserveLines then
            Self.WriteToStream(Outs, #$0d + #$0a);

          // Sounds illogical but this ensures uc is always
          // called for pre-proc directrives without wasting calls to uc :)
          if PreserveCase then
            Token:= UpperCase(Token);
          if Token = '@IF' then begin
            if Condition(Buffer) then
              Line:= ParseChunk(Line + 1, True)
            else begin
              Line:= EatChunk(Line + 1, False, Cont);
              if Cont then
                Line:= ParseChunk(Line, False);
            end;

            if PopRepeat then begin
              Result:= Line;
              Exit;
            end;
          end
          else if Token = '@ELSEIF' then begin
            if Conditional then begin
              Result:= EatChunk(Line + 1, True, Cont);
              Exit;
            end;


            if Condition(Buffer) then begin
              Conditional:= True;
              Inc(Line);
            end else begin
              Line:= EatChunk(Line + 1, False, Cont);
              if not Cont then begin
                Result:= Line;
                Exit;
              end;
            end;

            if PopRepeat then begin
              Result:= Line;
              Exit;
            end;
          end
          else if Token = '@ELSE' then begin
            if Conditional then begin
              Result:= EatChunk(Line + 1, True, Cont);
              Exit;
            end;
            Conditional:= True;
            Inc(Line);
          end
          else if Token = '@ENDIF' then begin
            Inc(Line);
            if Conditional then begin
              Result:= Line;
              Exit;
            end else
              raise EParserException.Create('Mismatch @IF / @ENDIF');
            if PopRepeat then begin
              Result:= Line;
              Exit;
            end;
          end
          else if Token = '@LABEL' then begin
            Work:= Model.AllocateLabel;
            Model.SetEnviron(ReadDelimited(Buffer), Work);
            Inc(Line);
          end
          else if Token = '@EXIT' then begin
            raise EParserAbort.Create('');
          end
          else if Token = '@LOCAL' then begin
            Model.AllocateLocalVariable(ReadDelimited(Buffer));
            Inc(Line);
          end
          else if Token = '@GLOBAL' then begin
            Model.AllocateGlobalVariable(ReadDelimited(Buffer));
            Inc(Line);
          end
          else if Token = '@REPEAT' then begin
            Count:= IntegerResolve(ReadDelimited(Buffer));
            NewPos:= Line + 1;
            for I:= 1 to Count do begin
              GlobalRepeatCount:= I;
              NewPos:= ParseChunk(Line + 1);
              if PopRepeat then begin
                PopRepeat:= False;
                Break;
              end;
            end;
            Line:= NewPos;
          end
          else if Token = '@BREAK' then begin
            PopRepeat:= True;
            Result:= EatRepeat(Line);
            Exit;
          end
          else if Token = '@END' then begin
            if Conditional then
              raise EParserException.Create('Encountered @END inside conditional compile');
            Result:= Line + 1;
            Exit;
          end
          else if Token = '@FIRST' then begin
            Resolver.FirstFeature;
            Inc(Line);
          end
          else if Token = '@NEXT' then begin
            Resolver.NextFeature;
            Inc(Line);
          end
          else if Token = '@FOREACH' then begin
            Line:= RunForeach(Line, Buffer);
          end
          else if Token = '@REPORT' then begin
            Resolver.AddReport(TokenReplace(Buffer), False);
            Inc(Line);
          end
          else if Token = '@OPERATOR' then begin
            Resolver.AddReport(TokenReplace(Buffer), True);
            Inc(Line);
          end
          else if Token = '@PUSH' then begin
            Model.PushState;
            Inc(Line);
          end
          else if Token = '@POP' then begin
            Model.PopState;
            Inc(Line);
          end
          else if Token = '@INCLUDE' then begin
            IncludeFile(TokenReplace(Buffer));
            Inc(Line);
          end
          else if Token = '@MACRO' then begin
            IncludeMacro(TokenReplace(Buffer));
            Inc(Line);
          end
          else if Token = '@NL' then begin
            Model.WithLineNumbers:= False;
            Inc(Line);
          end
          else if Token = '@SET' then begin
            Work:= ReadDelimited(Buffer);
            Model.SetEnviron(Work, TokenReplace(Buffer));
            Inc(Line);
          end
          else if Token = '@ADD' then begin
            Work:= ReadDelimited(Buffer);
            Model.ModifyEnviron(Work, Buffer);
            Inc(Line);
          end
          else if Token = '@//' then begin
            Inc(Line);
          end
          else if Token = '@JAVA' then begin
            Token:= ReadDelimited(Buffer);
            RunScript(Token, Buffer);
            Inc(Line);
          end
          else if Token = '@ERROR' then begin
            try
              Token:= TokenReplace(Buffer);
            except
              raise EParserException.Create('Template aborted parsing, ' + TokenReplace(Buffer));
            end;
            raise EParserException.Create('Template aborted parsing, ' + TokenReplace(Token));
          end
          else
            raise EParserException.Create('Invalid Preprocessor Directive: ' + Token);

          Continue;
        end
      end else begin
        if IgnoreBlankLines then begin
          if Trim(Buffer) = '' then begin // This seems illogical
            Inc(Line);
            Continue;
          end;
        end;
      end;

      Model.StartLine(Outs);

      Work:= '';
      while Token <> '' do begin
        if(Pos('$FMT("', Token) = 1) then begin
          //Buffer:= Buffer + ' ';
          Work:= Work + ReadFormat(Token, Buffer);
        end else begin
          if Work <> '' then
            Work:= Work + ' ' + TokenReplace(Token)
          else
            Work:= Work + TokenReplace(Token);
        end;

        if PreserveCase then
          Token:= ReadDelimited(Buffer)
        else
          Token:= UpperCase(ReadDelimited(Buffer));
      end;
      WriteToStream(Outs, Work);
      Model.EndLine(Outs);
      Model.WithLineNumbers:= not FNoLineNumbers;
      Inc(Line);
    end;
  except
    on E: EParserException do
      raise EParserException.CreateFmt('[%s] @ Line %d', [E.message, Line]);
  end;

  Result:= 0;
end;


constructor TNCParser.Create(Host: INCParserHost; Model: TCNCParserModel);
begin
  Self.Model:= Model;
  Self.Resolver:= Host;
  Ins:= TStringList.Create;
  FNoLineNumbers := False;
end;

procedure TNCParser.WriteToStream(Stream: TStream; const Text: string);
begin
  Stream.Write(Pointer(Text)^, Length(Text));
end;

function TNCParser.ValidParameter(const Name: string): string;
begin
  Result:= Name;
  if Length(Name) > 1 then
    if Name[1] = '$' then
      Exit;

  raise EParserException.Create('Malformed variable [' + Name + ']');
end;

function TNCParser.IntegerResolve(const Name: string): Integer;
begin
  try
    Result:= Round(StrToFloat(Name));
    Exit;
  except
  end;

  try
    Result:= Round(StrToFloat(Resolve(ValidParameter(Name))));
  except
    raise EParserException.CreateFmt('Cannot resolve [%s] to an integer', [Name]);
  end;
end;

{ Resolves a $ prefixed variable }
function TNCParser.Resolve(const Name: string; Throw: Boolean): WideString;
var Tmp: string;
begin
  Inc(FResolveCount);
  Result:= '';
  Tmp:= System.Copy( Name, 2, Length(Name));

  if Tmp = 'REPEAT_INDEX' then begin
    Result:= IntToStr(GlobalRepeatCount);
    Exit;
  end;

  if Assigned(MacroVars) then begin
    if MacroVars.Resolve(Tmp, Result) then
      Exit;
  end;

  if Model.Resolve(Tmp, Result) then
    Exit;

  if Assigned(Resolver) then
    if not Resolver.Resolve(Tmp, Result) then
      if Throw then
        raise EParserException.CreateFmt('Unable to resolve [%s] to any value', [Name])
end;

(**
  Replaces any $xxx[seperator] with the result of the resolver
  Catches $$ and removes preceeding $ from output without calling resolver
*)
function TNCParser.TokenReplace(const Name: string): string;
var I, S: Integer;
    Tmp: string;
begin
  Result:= '';
  I:= 1;
  while I <= Length(Name) do begin
    if Name[I] = '$' then begin
      if I >= Length(Name) then
        raise EParserException.Create('Prefix token without content');

      S:= I;
      Inc(I);

      if Name[I] <> '$' then begin
        while (I <= Length(Name)) and not Seperator(Name[I]) do
          Inc(I);

        Tmp:= Trim(Copy(Name, S, I-S));
        Result:= Result + Resolve(Tmp);
      end else begin
        Result:= Result + Name[I];
        Inc(I);
      end;


    end
    else begin
      Result:= Result + Name[I];
      Inc(I);
    end;
  end;
end;

function TNCParser.Seperator(C: Char): Boolean;
begin
  Result:= True;
  case C of
    'a'..'z',
    'A'..'Z',
    '0'..'9',
    '_' : Result:= False;
  end;
end;

(**
  A condition is of the form
  VALUE
  VALUE COMPARATOR VALUE

  Where value can be a literal string or token, comparator is of the form
  = < > <>
*)
function TNCParser.Condition(Check: string): boolean;

  function CompareValues(a, b: string): Boolean;
  var f1, f2: Double;
  begin
    Result:= True;
    try
      f1:= StrToFloat(a);
      f2:= StrToFloat(b);
      if Abs(f1 - f2) < 0.001 then
        Exit;
      Result:= False;
      Exit;
    except
    end;

    Result:= UpperCase(a) = UpperCase(b);
  end;

  function CompareValuesGreaterOrEqual(a, b: string): Boolean;
  var f1, f2: Double;
  begin
    Result:= True;
    f1:= StrToFloat(a);
    f2:= StrToFloat(b);
    if Abs(f1 - f2) < 0.001 then
      Exit;
    if f1 > f2 then
      Exit;

    Result:= False;
    Exit;

    Result:= UpperCase(a) = UpperCase(b);
  end;


var Left, Right, Func: string;
    Temp: string;
begin
  Temp:= Check;
  Left:= ReadDelimited(Temp);
  if Trim(Temp) = '' then begin
    try
      Left:= Resolve(Left);
      Result:= (Left <> '') and (Left <> '0');
      Exit;
    except
      Result:= False;
      Exit;
    end;
  end;

  //Temp:= TokenReplace(Check);

  Left:= ReadDelimited(Check);
  Func:= UpperCase(ReadDelimited(Check));
  right:= ReadDelimited(Check);

  Left:= TokenReplace(Left);
  Right:= TokenReplace(Right);

  if Func = '=' then
    Result:= CompareValues(Left, Right)
  else if Func = '<>' then
    Result:= not CompareValues(Left, Right)
  else if Func = '>' then
    Result:= StrToFloat(Left) > StrToFloat(Right)
  else if Func = '<' then
    Result:= StrToFloat(Left) < StrToFloat(Right)
  else if Func = '>=' then
    Result:= CompareValuesGreaterOrEqual(Left, Right)
  else if Func = '<=' then
    Result:= CompareValuesGreaterOrEqual(Right, Left)
  else
    raise EParserException.CreateFmt('Unknown comparator [%s]', [Func]);
end;

function TNCParser.EatRepeat(Line: Integer): Integer;
var Buffer, Token: string;
    I: Integer;
begin
  Result:= Ins.Count;
  for I:= Line to Ins.Count - 1 do begin
    Buffer:= Ins[I];
    if Length(Buffer) > 1 then begin
      Token:= UpperCase(ReadDelimited(Buffer));
      if Token = '@END' then begin
        Result:= I + 1;
        Exit;
      end;
    end;
  end;
end;

function TNCParser.Eatchunk(Line: Integer; All: Boolean; var Cont: Boolean): Integer;
var Buffer, Token: string;
    I: Integer;
    Nested: Integer;
begin
  Nested := 0;
  Cont:= False;

  for I:= Line to Ins.Count - 1 do begin
    Buffer:= Ins[I];
    if Length(Buffer) > 1 then begin
      Token:= UpperCase(ReadDelimited(Buffer));

      if PreserveLines then
        Self.WriteToStream(Outs, #$0d + #$0a);
        
      if Token = '@IF' then
        Inc(Nested);

      if Token = '@ENDIF' then begin
        if Nested > 0 then begin
          Dec(Nested);
        end else begin
          Result:= I + 1;
          Exit;
        end;
      end;

      if Nested = 0 then begin
        if Token = '@ELSEIF' then begin
          if not All then begin
            Cont:= True;
            Result:= I;
            Exit;
          end;
        end

        else if Token = '@ELSE' then begin
          if not All then begin
            Cont:= True;
            Result:= I;
            Exit;
          end;
        end
      end;
    end;
  end;
  raise EParserException.CreateFmt('Unexpected end of file, no matching @ENDIF from @IF @ line %d', [Line]);
end;

procedure TNCParser.EndParse;
begin
  if assigned(Outs) then Model.LastLine(Outs);
end;

procedure TNCParser.IncludeFile(Filename: string);
var InputStream: TStream;
    Buffer: array [0..255] of Byte;
    Count: Integer;
begin
  if ActiveDirectory <> '' then begin
    Filename:= ActiveDirectory + Filename;
  end;
  if FileExists(Filename) then begin
    try
      InputStream:= TFileStream.Create(Filename, fmOpenRead);
      try
        while(InputStream.Position < InputStream.Size) do begin
          Count:= InputStream.Read(Buffer, 255);
          Outs.Write(Buffer, Count);
        end;
        Exit;
      finally
        InputStream.Free;
      end;
    except
      raise EParserException.Create('Can''t open include file: ' + Filename);
    end;
  end;
  raise EParserException.Create('Include file does not exist: ' + Filename);
end;

procedure TNCParser.IncludeMacro(Domain: string);
var InputStream: TStream;
    Macro: TNCParser;
    MacroVariables: TMacroVariables;
    Macroname: string;
    Filename: string;
begin
  Macroname:= ReadDelimited(Domain);
  Filename:=  Macroname + '.txt';

  if ActiveDirectory <> '' then begin
    Filename:= ActiveDirectory + Filename;
  end;
  if FileExists(Filename) then begin
    try
      InputStream:= TFileStream.Create(Filename, fmOpenRead);
    except
      raise EParserException.Create('Can''t open include file: ' + Filename);
    end;

    MacroVariables:= TMacroVariables.Create;
    while(Domain <> '') do
      MacroVariables.AddParameter(ReadDelimited(Domain));

    Macro:= TNCParser.Create(Resolver, Model);
    Macro.MacroVars:= MacroVariables;
    Macro.PreserveSpace:= PreserveSpace;
    Macro.PreserveCase:= PreserveCase;
    Macro.ActiveDirectory:= ActiveDirectory;
    Macro.IgnoreBlankLines:= IgnoreBlankLines; 

    try
      Model.PushState;
      try
        Macro.ParseFile(InputStream, Outs);
      except
        on E: EParserException do
          raise EParserException.CreateFmt('Macro [%s] %s', [Macroname, E.Message] );
      end;
      Exit;
    finally
      Model.PopState;
      InputStream.Free;
      Macro.Free;
      MacroVariables.Free; //
    end;
  end;
  raise EParserException.Create('Macro file does not exist: ' + Filename);
end;

procedure TNCParser.SetActiveDirectory(const Value: string);
begin
  FActiveDirectory := IncludeTrailingPathDelimiter(Value);
end;

function TNCParser.RunForeach(Line: Integer; Buffer: string): Integer;

  function RangeBind(Name: string; Value, Min, Max: Integer): Integer;
  begin
    if Value > Max then begin
      Resolver.AddReport('Parser: Changed ' + Name + ' of @FOREACH from ' + IntToStr(Value) + ' to ' + IntToStr(Max), False);
      Value:= Max;
    end;

    if Value < Min then begin
      Resolver.AddReport('Parser: Changed ' + Name + ' of @FOREACH from ' + IntToStr(Value) + ' to ' + IntToStr(Min), False);
      Value:= Min;
    end;

    Result:= Value;
  end;

var NewPos: Integer;
    Count: Integer;
    I: Integer;
    Tmp: string;
    Direction, StartIndex, EndIndex: Integer;
begin
  // Parameters all optional in the following order Increment | Start | End

  Direction:= 1;

  Tmp:= ReadDelimited(Buffer);
  if Tmp <> '' then
    Direction:= IntegerResolve(Tmp);

  Tmp:= ReadDelimited(Buffer);
  if Tmp <> '' then
    StartIndex:= IntegerResolve(Tmp)
  else begin
    if Direction > 0 then
      StartIndex:= 0
    else
      StartIndex:= Resolver.FeatureCount - 1;
  end;

  Tmp:= ReadDelimited(Buffer);
  if Tmp <> '' then
    EndIndex:= IntegerResolve(Tmp)
  else begin
    if Direction > 0 then
      EndIndex:= Resolver.FeatureCount - 1
    else
      EndIndex:= 0;
  end;

  if Direction = 0 then
    raise EParserException.Create('@FOREACH Index = 0');

  EndIndex:= RangeBind('End Index', EndIndex, 0, Resolver.FeatureCount - 1);
  StartIndex:= RangeBind('Start Index', StartIndex, 0, Resolver.FeatureCount - 1);


  NewPos:= Line + 1;
  Count:= ((EndIndex - StartIndex) div Direction) + 1;
  if Count = 0 then begin
    Result:= EatRepeat(NewPos);
    Exit;
  end;

  for I:= 0 to Count - 1 do begin
    Resolver.SetFeatureIndex(I * Direction + StartIndex);
    GlobalRepeatCount:= I;
    NewPos:= ParseChunk(Line + 1);
    if PopRepeat then begin
      PopRepeat:= False;
      Break;
    end;

    GlobalRepeatCount:= I;
  end;
  Result:= NewPos;
end;

procedure TNCParser.SetNoLineNumbers(const Value: Boolean);
begin
  FNoLineNumbers := Value;
end;

destructor TNCParser.Destroy;
begin
  Model:= nil;
  JavaExt.Free;
  inherited;
end;

var Test: Integer;
Foo: string;
function TNCParser.ReadFormat(const Token: string; var Buffer: string): string;
var Tmp: string;
    F: string;
    I : Integer;
    FormatString: string;
begin
  Tmp := Token + ' ' + Buffer;
  I:= Pos(')', Tmp);
  if I = 0 then
    raise EParserException.Create('$FMT missing final close parenthesis ")"');

  F:= Copy(Tmp, 1, I);
  Delete(Tmp, 1, I);
  Buffer := Tmp;

  // F = function line of form $FMT("xxxxxxxx"xxxxxxxxxxx)
  // delete rhe preceding $FMT("
  Delete(F, 1, 6);
  // delete trailing )
  Delete(F, Length(F), 1);
  I:= Pos('"', F);
  if I = 0 then
    raise EParserException.Create('$FMT missing closing quotation: """');

  FormatString:= Copy(F, 1, I - 1);
  Delete(F, 1, I);
  F:= TokenReplace(F);

  if Pos('f', FormatString) <> 0 then begin
    Result:= Format(FormatString, [StrToFloatDef(F, 0)]);
  end
  else if Pos('d', FormatString) <> 0 then begin
    Result:= Format(FormatString, [StrToIntDef(F, 0)]);
  end
  else if Pos('s', FormatString) <> 0 then begin
    Result:= Format(FormatString, [F]);
  end
  else if Pos('F', FormatString) <> 0 then begin
    Result:= Format(FormatString, [StrToFloatDef(F, 0)]);
  end
  else if Pos('D', FormatString) <> 0 then begin
    Result:= Format(FormatString, [StrToIntDef(F, 0)]);
  end
  else if Pos('S', FormatString) <> 0 then begin
    Result:= Format(FormatString, [F]);
  end else
    raise EParserException.Create('$FMT invalid format string: ' + FormatString);

end;

function TNCParser.RunScript(JC: string; Work: string): string;
begin
  JavaExt:= TJavaExt.Create(Outs, Model, Resolve);
  Result:= JavaExt.RunScript(JC, Work);
  JavaExt.Free;
end;

end.
