unit JavaExt;

interface

uses SysUtils, Classes, CncTypes, NamedPlugin, JNI, JNIUtils, CNCParserModel;

type
  TJavaExt = class
  private
  public
    constructor Create(AOuts: TStream; AModel: TCNCParserModel; AResolveMethod: TResolveMethod);
    destructor Destroy; override;
    function RunScript(JC: string; Work: string): string;
  end;

implementation

var Model: TCNCParserModel;
    Outs: TStream;
    JNIEnv: TJNIEnv;
    ResolveMethod: TResolveMethod;

destructor TJavaExt.Destroy;
begin
  inherited;
end;

constructor TJavaExt.Create(AOuts: TStream; AModel: TCNCParserModel; AResolveMethod: TResolveMethod);
begin
  Outs:= AOuts;
  Model:= AModel;
  ResolveMethod:= AResolveMethod;
end;

function TJavaExt.RunScript(JC: string; Work: string): string;
  function replace(Tmp: string; Old: Char; New: Char): string;
  var I: Integer;
  begin
    for I := 1 to Length(Tmp) do begin
      if Tmp[I] = Old then
        Tmp[I]:= New;
    end;
    Result:= Tmp;
  end;

var
  Cls, Nat: JClass;
  Exc: JThrowable;
  FileName, Tmp: string;
  V: Variant;
begin
  Filename:= GetCurrentDir;
  Result:= Filename;
  JNIEnv:= TJNIEnv.Create( NamedPlugin.JNIEnv );

  Tmp:= replace(JC, '.', '/');
  Cls := JNIEnv.FindClass(PChar(Tmp));
  Nat:= JNIEnv.FindClass(PChar('com/winbro/ncparser/NCParser'));

  V := JNIUtils.CallMethod(JNIEnv, Cls,
    'parse', 'String (String)',
    [Work], True );
  Filename:= V;
  Cls:= nil;
  Nat:= nil;
  FreeAndNil(JNIEnv)
end;

procedure WriteToStream(Stream: TStream; const Text: string);
begin
  Stream.Write(Pointer(Text)^, Length(Text));
end;


(*
 * Class:     com_winbro_ncparser_NCParser
 * Method:    resolve
 * Signature: (Ljava/lang/String;)Ljava/lang/String;
 *)
function Java_com_winbro_ncparser_NCParser_resolve(PEnv: PJNIEnv; Obj: JObject; Token: JString): JString; stdcall;
var Temp: string;
begin
  Temp:= JNIEnv.JStringToString(Token);
  Temp:= ResolveMethod(Temp);
  Result:= JNIEnv.StringToJString(PChar(Temp));
end;

(*
 * Class:     com_winbro_ncparser_NCParser
 * Method:    PushState
 * Signature: ()V
 *)
procedure Java_com_winbro_ncparser_NCParser_PushState(PEnv: PJNIEnv; Obj: JObject); stdcall;
begin
  Model.PushState;
end;

(*
 * Class:     com_winbro_ncparser_NCParser
 * Method:    PopState
 * Signature: ()V
 *)
procedure Java_com_winbro_ncparser_NCParser_PopState(PEnv: PJNIEnv; Obj: JObject); stdcall;
begin
  Model.PopState;
end;

(*
 * Class:     com_winbro_ncparser_NCParser
 * Method:    AllocateGlobalVariable
 * Signature: (Ljava/lang/String;)V
 *)
procedure Java_com_winbro_ncparser_NCParser_AllocateGlobalVariable(PEnv: PJNIEnv; Obj: JObject; Token: JString); stdcall;
var P1: string;
begin
  P1:= JNIEnv.JStringToString(token);
  Model.AllocateGlobalVariable(P1);
end;

(*
 * Class:     com_winbro_ncparser_NCParser
 * Method:    AllocateLocalVariable
 * Signature: (Ljava/lang/String;)V
 *)
procedure Java_com_winbro_ncparser_NCParser_AllocateLocalVariable(PEnv: PJNIEnv; Obj: JObject; Token: JString); stdcall;
var P1: string;
begin
  P1:= JNIEnv.JStringToString(token);
  Model.AllocateLocalVariable(P1);
end;

(*
 * Class:     com_winbro_ncparser_NCParser
 * Method:    AllocateLabel
 * Signature: (Ljava/lang/String;)Ljava/lang/String;
 *)
function Java_com_winbro_ncparser_NCParser_AllocateLabel(PEnv: PJNIEnv; Obj: JObject; Token: JString): JString; stdcall;
var P1, P2: string;
begin
  P1:= JNIEnv.JStringToString(token);
  P2:= Model.AllocateLabel;
  JNIEnv.ReleaseStringUTFChars(token, PChar(P1));

  Result:= JNIEnv.StringToJString(PChar(P2));
end;

(*
 * Class:     com_winbro_ncparser_NCParser
 * Method:    StartLine
 * Signature: ()V
 *)
procedure Java_com_winbro_ncparser_NCParser_StartLine(PEnv: PJNIEnv; Obj: JObject); stdcall;
begin
  Model.StartLine(Outs);
end;

(*
 * Class:     com_winbro_ncparser_NCParser
 * Method:    EndLine
 * Signature: ()V
 *)
procedure Java_com_winbro_ncparser_NCParser_EndLine(PEnv: PJNIEnv; Obj: JObject); stdcall;
begin
  Model.EndLine(Outs);
end;

(*
 * Class:     com_winbro_ncparser_NCParser
 * Method:    SetEnviron
 * Signature: (Ljava/lang/String;Ljava/lang/String;)V
 *)
procedure Java_com_winbro_ncparser_NCParser_SetEnviron(PEnv: PJNIEnv; Obj: JObject; ev: JString; tok: JString); stdcall;
var P1, P2: string;
begin
  P1:= JNIEnv.JStringToString(ev);
  P2:= JNIEnv.JStringToString(tok);
  Model.SetEnviron(P1, P2);
end;

(*
 * Class:     com_winbro_ncparser_NCParser
 * Method:    ModifyEnviron
 * Signature: (Ljava/lang/String;Ljava/lang/String;)V
 *)
procedure Java_com_winbro_ncparser_NCParser_ModifyEnviron(PEnv: PJNIEnv; Obj: JObject; ev: JString; tok: JString); stdcall;
var P1, P2: string;
begin
  P1:= JNIEnv.JStringToString(ev);
  P2:= JNIEnv.JStringToString(tok);
  Model.ModifyEnviron(P1, P2);
end;

(*
 * Class:     com_winbro_ncparser_NCParser
 * Method:    LastLine
 * Signature: ()V
 *)
procedure Java_com_winbro_ncparser_NCParser_LastLine(PEnv: PJNIEnv; Obj: JObject); stdcall;
begin
  Model.LastLine(Outs);
end;

(*
 * Class:     com_winbro_ncparser_NCParser
 * Method:    GetOutputLine
 * Signature: ()I
 *)
function Java_com_winbro_ncparser_NCParser_GetOutputLine(PEnv: PJNIEnv; Obj: JObject): Integer; stdcall;
begin
  Result:= Model.OutputLine;
end;

(*
 * Class:     com_winbro_ncparser_NCParser
 * Method:    GetFilename
 * Signature: ()Ljava/lang/String;
 *)
function Java_com_winbro_ncparser_NCParser_GetFilename(PEnv: PJNIEnv; Obj: JObject): JString; stdcall;
begin
  Result:= JNIEnv.NewStringUTF(PAnsiChar(Model.Filename));
end;

(*
 * Class:     com_winbro_ncparser_NCParser
 * Method:    write
 * Signature: (Ljava/lang/String;)V
 *)
procedure Java_com_winbro_ncparser_NCParser_write(PEnv: PJNIEnv; Obj: TObject; Text: JString); stdcall;
var P1: string;
begin
  P1:= JNIEnv.JStringToString(Text);
  WriteToStream(Outs, P1);
end;


exports
  Java_com_winbro_ncparser_NCParser_resolve,
  Java_com_winbro_ncparser_NCParser_PushState,
  Java_com_winbro_ncparser_NCParser_PopState,
  Java_com_winbro_ncparser_NCParser_AllocateGlobalVariable,
  Java_com_winbro_ncparser_NCParser_AllocateLocalVariable,
  Java_com_winbro_ncparser_NCParser_AllocateLabel,
  Java_com_winbro_ncparser_NCParser_StartLine,
  Java_com_winbro_ncparser_NCParser_EndLine,
  Java_com_winbro_ncparser_NCParser_SetEnviron,
  Java_com_winbro_ncparser_NCParser_ModifyEnviron,
  Java_com_winbro_ncparser_NCParser_LastLine,
  Java_com_winbro_ncparser_NCParser_GetOutputLine,
  Java_com_winbro_ncparser_NCParser_GetFilename,
  Java_com_winbro_ncparser_NCParser_write;

end.
