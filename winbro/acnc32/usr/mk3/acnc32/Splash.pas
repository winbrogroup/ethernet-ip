unit Splash;

{
  Inch metric conversion ->  Requires thorough testing
                             // Dont save on catastrophy (converted data)
                             Power up mode.
                             Rotary Feedrate.

  User level editor ->

  Access ->    Group access levels.

/  Pcomm ->  Pcomm.txt is growing with crap. / Error 7
/  OCT -> loading an OPP / OCT that doesn't exist.
/         Index and total for debug. Use 1 based
/         Check loading OCT from anywhere.
  OPC -> VCL freezes during connection from client.
  Persistent Data -> Give a button for saving.
  EDM -> Exception occurs while editing EDM grid.
  NC -> Inch-metric switching array
/  Script -> String to integer conversion does not work.
  Custom diagmostics -> problem with OPC points / PLc IOR items

}

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, ExtCtrls, StdCtrls, CNC32;

type
  TSplashForm = class(TForm)
    Image: TImage;
    InfoLabel: TLabel;
    ProgressBar: TProgressBar;
    VersionLabel: TLabel;
  private
    Data: PVSFixedFileInfo;
  public
    Version : string;
    function LoadImage: Boolean;
    procedure Progress(Percent: Integer);
    procedure Msg( const Value: string);
  end;

var
  SplashForm: TSplashForm;

implementation

{$R *.DFM}

uses
  CNCTypes;

const
  SplashFileName = 'acnc.bmp';

resourcestring
  VersionFormat = '%d.%d.%d.%d';
  VersionLang = 'Version';

function TSplashForm.LoadImage: Boolean;
var
  Filename: String;
begin
  try
    Filename:= ProfilePath + SplashFileName;
    Result:= FileExists(Filename);
    if Result then begin
      Image.Picture.LoadFromFile(Filename);
      Data := ReadVersionInfoData(Application.ExeName);
      Version := Format(VersionFormat, [
        HiWord(Data^.dwFileVersionMS),
        LoWord(Data^.dwFileVersionMS),
        HiWord(Data^.dwFileVersionLS),
        LoWord(Data^.dwFileVersionLS) ]);
      VersionLabel.Caption := VersionLang + ' ' + Version;

      ClientWidth:= Image.Width;
      ClientHeight:= Image.Height + VersionLabel.Height + InfoLabel.Height + ProgressBar.Height;
      Show;
      Update
    end;
  except
    on E:EInvalidGraphic do
      Result:= false;
  end;
end;

procedure TSplashForm.Progress(Percent: Integer);
begin
  ProgressBar.Position:= Percent;
  ProgressBar.Update
end;

procedure TSplashForm.Msg( const Value: String);
begin
  InfoLabel.Caption:= Value;
  InfoLabel.Update
end;

end.
