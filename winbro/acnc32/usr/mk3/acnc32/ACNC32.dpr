program ACNC32;

{%File 'ModelSupport\Pmac32NC\Pmac32NC.txvpck'}
{%File 'ModelSupport\PlcStack\PlcStack.txvpck'}
{%File 'ModelSupport\ObjectPLC\ObjectPLC.txvpck'}
{%File 'ModelSupport\ErrorDisplay\ErrorDisplay.txvpck'}
{%File 'ModelSupport\LogSyncObjs\LogSyncObjs.txvpck'}
{%File 'ModelSupport\SelectTagForm\SelectTagForm.txvpck'}
{%File 'ModelSupport\BaseKeyBoards\BaseKeyBoards.txvpck'}
{%File 'ModelSupport\Softkey\Softkey.txvpck'}
{%File 'ModelSupport\SoftkeyTreeData\SoftkeyTreeData.txvpck'}
{%File 'ModelSupport\NCZeroOffsetForm\NCZeroOffsetForm.txvpck'}
{%File 'ModelSupport\MathDebug\MathDebug.txvpck'}
{%File 'ModelSupport\CustomFaultInterface\CustomFaultInterface.txvpck'}
{%File 'ModelSupport\FatalError\FatalError.txvpck'}
{%File 'ModelSupport\PositionWindow\PositionWindow.txvpck'}
{%File 'ModelSupport\SimpleTapReader\SimpleTapReader.txvpck'}
{%File 'ModelSupport\Plc32\Plc32.txvpck'}
{%File 'ModelSupport\PlcMono\PlcMono.txvpck'}
{%File 'ModelSupport\CNCUtils\CNCUtils.txvpck'}
{%File 'ModelSupport\CNC32\CNC32.txvpck'}
{%File 'ModelSupport\PlcAlarm\PlcAlarm.txvpck'}
{%File 'ModelSupport\CustomLayoutForm\CustomLayoutForm.txvpck'}
{%File 'ModelSupport\NC32ProbeOffset\NC32ProbeOffset.txvpck'}
{%File 'ModelSupport\PanelButton\PanelButton.txvpck'}
{%File 'ModelSupport\ACNCScript\ACNCScript.txvpck'}
{%File 'ModelSupport\SignatureAuditing\SignatureAuditing.txvpck'}
{%File 'ModelSupport\TurboPmac32NCValues\TurboPmac32NCValues.txvpck'}
{%File 'ModelSupport\OCTPlugin\OCTPlugin.txvpck'}
{%File 'ModelSupport\PmacCommon\PmacCommon.txvpck'}
{%File 'ModelSupport\OleAutomation\OleAutomation.txvpck'}
{%File 'ModelSupport\SignatureEditorFrm\SignatureEditorFrm.txvpck'}
{%File 'ModelSupport\PlcValueItem\PlcValueItem.txvpck'}
{%File 'ModelSupport\NC32\NC32.txvpck'}
{%File 'ModelSupport\Editor\Editor.txvpck'}
{%File 'ModelSupport\AxUtils\AxUtils.txvpck'}
{%File 'ModelSupport\CurveChart\CurveChart.txvpck'}
{%File 'ModelSupport\CNCAbs\CNCAbs.txvpck'}
{%File 'ModelSupport\CustomLayout\CustomLayout.txvpck'}
{%File 'ModelSupport\CriteriaEdit\CriteriaEdit.txvpck'}
{%File 'ModelSupport\FTP\FTP.txvpck'}
{%File 'ModelSupport\AboutForm\AboutForm.txvpck'}
{%File 'ModelSupport\Pmac32NCIntros\Pmac32NCIntros.txvpck'}
{%File 'ModelSupport\PlcGather\PlcGather.txvpck'}
{%File 'ModelSupport\GEOPlugin\GEOPlugin.txvpck'}
{%File 'ModelSupport\Pmac32NCValues\Pmac32NCValues.txvpck'}
{%File 'ModelSupport\OEE\OEE.txvpck'}
{%File 'ModelSupport\CoreCNC\CoreCNC.txvpck'}
{%File 'ModelSupport\SoftkeyTree\SoftkeyTree.txvpck'}
{%File 'ModelSupport\FTPServer\FTPServer.txvpck'}
{%File 'ModelSupport\ToolLifePlugin\ToolLifePlugin.txvpck'}
{%File 'ModelSupport\SocketMemory\SocketMemory.txvpck'}
{%File 'ModelSupport\Named\Named.txvpck'}
{%File 'ModelSupport\PanelPluginPropertyEditor\PanelPluginPropertyEditor.txvpck'}
{%File 'ModelSupport\FaultRegistration\FaultRegistration.txvpck'}
{%File 'ModelSupport\PmacTerminal\PmacTerminal.txvpck'}
{%File 'ModelSupport\ScrollProgramDisplay\ScrollProgramDisplay.txvpck'}
{%File 'ModelSupport\XHost\XHost.txvpck'}
{%File 'ModelSupport\CMMFileV1\CMMFileV1.txvpck'}
{%File 'ModelSupport\PlcStateMachine\PlcStateMachine.txvpck'}
{%File 'ModelSupport\LogicAnalyser\LogicAnalyser.txvpck'}
{%File 'ModelSupport\NudgeGrid\NudgeGrid.txvpck'}
{%File 'ModelSupport\TurboPmac32NC\TurboPmac32NC.txvpck'}
{%File 'ModelSupport\GenericIO\GenericIO.txvpck'}
{%File 'ModelSupport\PanelLabels\PanelLabels.txvpck'}
{%File 'ModelSupport\PlcItems\PlcItems.txvpck'}
{%File 'ModelSupport\FakeNCHal\FakeNCHal.txvpck'}
{%File 'ModelSupport\Installer\Installer.txvpck'}
{%File 'ModelSupport\AccessEditForm\AccessEditForm.txvpck'}
{%File 'ModelSupport\PComm32Edit\PComm32Edit.txvpck'}
{%File 'ModelSupport\ArrayViewForm\ArrayViewForm.txvpck'}
{%File 'ModelSupport\ExAccessEdit\ExAccessEdit.txvpck'}
{%File 'ModelSupport\WebChat\WebChat.txvpck'}
{%File 'ModelSupport\ScreenSaverFrm\ScreenSaverFrm.txvpck'}
{%File 'ModelSupport\Gather\Gather.txvpck'}
{%File 'ModelSupport\KBControl\KBControl.txvpck'}
{%File 'ModelSupport\SelectLayoutForm\SelectLayoutForm.txvpck'}
{%File 'ModelSupport\FileManager\FileManager.txvpck'}
{%File 'ModelSupport\InstallableScript\InstallableScript.txvpck'}
{%File 'ModelSupport\NC32ScriptRTL\NC32ScriptRTL.txvpck'}
{%File 'ModelSupport\Splash\Splash.txvpck'}
{%File 'ModelSupport\ScriptTypeInfo\ScriptTypeInfo.txvpck'}
{%File 'ModelSupport\PlcPlugin\PlcPlugin.txvpck'}
{%File 'ModelSupport\NCProgram\NCProgram.txvpck'}
{%File 'ModelSupport\PSDLoader\PSDLoader.txvpck'}
{%File 'ModelSupport\PlcTextItem\PlcTextItem.txvpck'}
{%File 'ModelSupport\HeidenhainTable\HeidenhainTable.txvpck'}
{%File 'ModelSupport\SignatureFileList\SignatureFileList.txvpck'}
{%File 'ModelSupport\CustomAppDisplay\CustomAppDisplay.txvpck'}
{%File 'ModelSupport\ScriptNC\ScriptNC.txvpck'}
{%File 'ModelSupport\DynamicHelp\DynamicHelp.txvpck'}
{%File 'ModelSupport\Signature\Signature.txvpck'}
{%File 'ModelSupport\FormPlugin\FormPlugin.txvpck'}
{%File 'ModelSupport\Acknowledge\Acknowledge.txvpck'}
{%File 'ModelSupport\DisplayContainer\DisplayContainer.txvpck'}
{%File 'ModelSupport\CNCRegistry\CNCRegistry.txvpck'}
{%File 'ModelSupport\CustomMachinePanel\CustomMachinePanel.txvpck'}
{%File 'ModelSupport\InterlockForm\InterlockForm.txvpck'}
{%File 'ModelSupport\SignatureEditor\SignatureEditor.txvpck'}
{%File 'ModelSupport\PComm32\PComm32.txvpck'}
{%File 'ModelSupport\SelectScript\SelectScript.txvpck'}
{%File 'ModelSupport\NC32Types\NC32Types.txvpck'}
{%File 'ModelSupport\TurboPmac32NCConfigure\TurboPmac32NCConfigure.txvpck'}
{%File 'ModelSupport\NC32ConfigurationForm\NC32ConfigurationForm.txvpck'}
{%File 'ModelSupport\PlcGatherConfig\PlcGatherConfig.txvpck'}
{%File 'ModelSupport\IScriptInterface\IScriptInterface.txvpck'}
{%File 'ModelSupport\InvokableClient\InvokableClient.txvpck'}
{%File 'ModelSupport\ScriptFunctions\ScriptFunctions.txvpck'}
{%File 'ModelSupport\SectionEditorPlugin\SectionEditorPlugin.txvpck'}
{%File 'ModelSupport\TPMSetup\TPMSetup.txvpck'}
{%File 'ModelSupport\NCMath\NCMath.txvpck'}
{%File 'ModelSupport\Pmac32NCConfigure\Pmac32NCConfigure.txvpck'}
{%File 'ModelSupport\PlcGORItem\PlcGORItem.txvpck'}
{%File 'ModelSupport\clExWatcher\clExWatcher.txvpck'}
{%File 'ModelSupport\NC32NWordSearchForm\NC32NWordSearchForm.txvpck'}
{%File 'ModelSupport\ViewPlcItem\ViewPlcItem.txvpck'}
{%File 'ModelSupport\CompoundFiles\CompoundFiles.txvpck'}
{%File 'ModelSupport\EditNamedLayerTagForm\EditNamedLayerTagForm.txvpck'}
{%File 'ModelSupport\Response\Response.txvpck'}
{%File 'ModelSupport\StringArrayViewer\StringArrayViewer.txvpck'}
{%File 'ModelSupport\GridPlugin\GridPlugin.txvpck'}
{%File 'ModelSupport\SelectVisualForm\SelectVisualForm.txvpck'}
{%File 'ModelSupport\zFileList\zFileList.txvpck'}
{%File 'ModelSupport\DT3155\DT3155.txvpck'}
{%File 'ModelSupport\NC32Offsets\NC32Offsets.txvpck'}
{%File 'ModelSupport\PlcLamptest\PlcLamptest.txvpck'}
{%File 'ModelSupport\PlusMemo\PlusMemo.txvpck'}
{%File 'ModelSupport\OOPMetrics\OOPMetrics.txvpck'}
{%File 'ModelSupport\Diagnostic\Diagnostic.txvpck'}
{%File 'ModelSupport\PlcMCodeItem\PlcMCodeItem.txvpck'}
{%File 'ModelSupport\PlcStopWatch\PlcStopWatch.txvpck'}
{%File 'ModelSupport\PlcSRItems\PlcSRItems.txvpck'}
{%File 'ModelSupport\Displays\Displays.txvpck'}
{%File 'ModelSupport\PanelGroup\PanelGroup.txvpck'}
{%File 'ModelSupport\clDbgHelp\clDbgHelp.txvpck'}
{%File 'ModelSupport\ActiveXDisplay\ActiveXDisplay.txvpck'}
{%File 'ModelSupport\Telnet\Telnet.txvpck'}
{%File 'ModelSupport\ScriptFile\ScriptFile.txvpck'}
{%File 'ModelSupport\Access\Access.txvpck'}
{%File 'ModelSupport\PlcText\PlcText.txvpck'}
{%File 'ModelSupport\InterlockEditForm\InterlockEditForm.txvpck'}
{%File 'ModelSupport\PanelImage\PanelImage.txvpck'}
{%File 'ModelSupport\TagFilter\TagFilter.txvpck'}
{%File 'ModelSupport\ScriptEditor\ScriptEditor.txvpck'}
{%File 'ModelSupport\RestraintEditForm\RestraintEditForm.txvpck'}
{%File 'ModelSupport\PmacVariableView\PmacVariableView.txvpck'}
{%File 'ModelSupport\StringArray\StringArray.txvpck'}
{%File 'ModelSupport\ShellExec\ShellExec.txvpck'}
{%File 'ModelSupport\StdEditor\StdEditor.txvpck'}
{%File 'ModelSupport\Plugin\Plugin.txvpck'}
{%File 'ModelSupport\default.txvpck'}

uses
  Windows,
  Forms,
  CoreCNC in 'CoreCNC\CoreCNC.pas',
  FatalError in 'Installable\FatalError.pas' {FatalErrorForm},
  Diagnostic in 'Installable\Diagnostic.pas',
  CustomFaultInterface in 'CoreCNC\CustomFaultInterface.pas',
  Displays in 'Installable\Displays.pas',
  PlcText in 'Installable\PlcText.pas',
  Access in 'Installable\Access.pas',
  PmacTerminal in 'Installable\PmacTerminal.pas',
  ObjectPlc in 'CoreCNC\ObjectPLC\ObjectPlc.pas',
  Response in 'CoreCNC\PmacUtil\Response.pas' {TextDisplay},
  PComm32Edit in 'CoreCNC\PmacUtil\PComm32Edit.pas' {PmacEdit},
  CompoundFiles in 'CoreCNC\CompoundFiles.pas',
  zFileList in 'Installable\zFileList.pas' {FileListForm},
  FileManager in 'Installable\FileManager.pas',
  Acknowledge in 'CoreCNC\Acknowledge.pas' {OKBottomDlg},
  Softkey in 'Installable\Softkey.pas',
  StdEditor in 'Installable\StdEditor.pas' {StdEditorForm},
  Installer in 'Installer.pas' {CNCInstaller},
  Splash in 'Splash.pas' {SplashForm},
  CNCAbs in 'CoreCNC\CNCAbs.pas',
  Plc32 in 'CoreCNC\ObjectPLC\Plc32.pas',
  PlcItems in 'CoreCNC\ObjectPLC\PlcItems.pas',
  PlcStack in 'CoreCNC\ObjectPLC\PlcStack.pas',
  PlcValueItem in 'CoreCNC\ObjectPLC\PlcValueItem.pas',
  CNC32 in 'CoreCNC\CNC32.pas',
  Named in 'CoreCNC\Named.pas',
  NudgeGrid in 'CoreCNC\NudgeGrid.pas',
  Editor in 'CoreCNC\Editor.pas',
  GenericIO in 'CoreCNC\GenericIO.pas',
  ACNCScript in 'CoreCNC\ACNCScript\ACNCScript.pas',
  ErrorDisplay in 'Installable\ErrorDisplay.pas',
  ScriptTypeInfo in 'CoreCNC\ScriptTypeInfo.pas',
  PlcMCodeItem in 'CoreCNC\ObjectPLC\PlcMCodeItem.pas',
  PlcSRItems in 'CoreCNC\ObjectPLC\PlcSRItems.pas',
  SelectScript in 'CoreCNC\ACNCScript\SelectScript.pas' {ScriptSelectForm},
  InstallableScript in 'Installable\InstallableScript.pas',
  CNCUtils in 'CoreCNC\CNCUtils.pas',
  ScriptFunctions in 'CoreCNC\ACNCScript\ScriptFunctions.pas' {ScriptRTL},
  MathDebug in 'CoreCNC\ACNCScript\MathDebug.pas' {MathPartial},
  ScriptNC in 'Installable\ScriptNC.pas',
  ViewPlcItem in 'CoreCNC\ObjectPLC\ViewPlcItem.pas' {PlcViewItemForm},
  PlcTextItem in 'CoreCNC\ObjectPLC\PlcTextItem.pas',
  InterlockEditForm in 'Installable\InterlockEditForm.pas' {InterlockEditFrm},
  InterlockForm in 'Installable\InterlockForm.pas' {InterlockFrm},
  AccessEditForm in 'Installable\AccessEditForm.pas' {AccessEditFrm},
  PlcStateMachine in 'CoreCNC\ObjectPLC\PlcStateMachine.pas',
  CurveChart in 'Installable\SignatureAnalysis\CurveChart.pas' {CurveChartEdit},
  ScriptEditor in 'CoreCNC\ACNCScript\ScriptEditor.pas' {ScriptEditForm},
  DisplayContainer in 'Installable\DisplayContainer.pas' {DisplayContainerForm},
  PlcStopWatch in 'CoreCNC\ObjectPLC\PlcStopWatch.pas',
  TurboPmac32NC in 'Installable\Pmac32NC\TurboPmac32NC.pas',
  TurboPmac32NCConfigure in 'Installable\Pmac32NC\TurboPmac32NCConfigure.pas',
  NCMath in 'CoreCNC\NC\NCMath.pas',
  NC32 in 'CoreCNC\NC\NC32.pas',
  ScriptFile in 'CoreCNC\ACNCScript\ScriptFile.pas',
  NCProgram in 'CoreCNC\NC\NCProgram.pas',
  ArrayViewForm in 'CoreCNC\ACNCScript\ArrayViewForm.pas' {ArrayView},
  FakeNCHal in 'CoreCNC\NC\FakeNCHal.pas',
  NC32Types in 'CoreCNC\NC\NC32Types.pas',
  NC32NWordSearchForm in 'CoreCNC\NC\NC32NWordSearchForm.pas' {NC_NWordSearch},
  PositionWindow in 'Installable\PositionWindow.pas',
  PmacVariableView in 'Installable\Pmac32NC\PmacVariableView.pas',
  CustomLayout in 'Installable\CustomLayout\CustomLayout.pas',
  CustomLayoutForm in 'Installable\CustomLayout\CustomLayoutForm.pas' {LayoutDesignForm},
  SelectVisualForm in 'Installable\CustomLayout\SelectVisualForm.pas' {SelectVisual},
  RestraintEditForm in 'Installable\CustomLayout\RestraintEditForm.pas' {RestraintEdit},
  SelectLayoutForm in 'Installable\CustomLayout\SelectLayoutForm.pas' {SelectLayout},
  SelectTagForm in 'CoreCNC\SelectTagForm.pas' {SelectTag},
  NC32ConfigurationForm in 'CoreCNC\NC\NC32ConfigurationForm.pas' {NC32Configuration},
  NC32Offsets in 'CoreCNC\NC\NC32Offsets.pas',
  TurboPmac32NCValues in 'Installable\Pmac32NC\TurboPmac32NCValues.pas',
  CustomMachinePanel in 'Installable\FrontPanels\CustomMachinePanel.pas',
  PanelButton in 'Installable\FrontPanels\PanelButton.pas',
  EditNamedLayerTagForm in 'CoreCNC\EditNamedLayerTagForm.pas' {EditNamedLayerTag},
  LogicAnalyser in 'CoreCNC\ObjectPLC\LogicAnalyser\LogicAnalyser.pas' {LogicAnalyserForm},
  PlcPlugin in 'CoreCNC\ObjectPLC\PlcPlugin.pas',
  Plugin in 'CoreCNC\Plugin.pas',
  GridPlugin in 'Installable\GridPlugin.pas',
  SectionEditorPlugin in 'CoreCNC\SectionEditorPlugin.pas',
  OOPMetrics in 'Installable\OOPMetrics.pas',
  PanelPluginPropertyEditor in 'Installable\FrontPanels\PanelPluginPropertyEditor.pas' {PanelPluginEditorForm},
  SignatureEditor in 'Installable\SignatureAnalysis\SignatureEditor.pas',
  SignatureEditorFrm in 'Installable\SignatureAnalysis\SignatureEditorFrm.pas' {SignatureEditForm},
  SignatureFileList in 'Installable\SignatureAnalysis\SignatureFileList.pas' {SignatureFileListForm},
  Signature in 'Installable\SignatureAnalysis\Signature.pas',
  CriteriaEdit in 'Installable\SignatureAnalysis\CriteriaEdit.pas' {CriteriaEditDlg},
  Gather in 'CoreCNC\Gather.pas',
  PlcGather in 'CoreCNC\ObjectPLC\PlcGather\PlcGather.pas',
  PlcGatherConfig in 'CoreCNC\ObjectPLC\PlcGather\PlcGatherConfig.pas' {PlcGatherConfigForm},
  SignatureAuditing in 'Installable\SignatureAnalysis\SignatureAuditing.pas',
  ShellExec in 'Installable\ShellExec.pas',
  TPMSetup in 'Installable\TPMSetup.pas',
  WebChat in 'CoreCNC\Chat\WebChat.pas',
  SimpleTapReader in 'Installable\PasswordAccess\SimpleTapReader.pas' {SimpleTapForm},
  NC32ScriptRTL in 'CoreCNC\NC\NC32ScriptRTL.pas',
  OCTPlugin in 'CoreCNC\OCTPlugin.pas',
  GEOPlugin in 'CoreCNC\GEOPlugin.pas',
  FormPlugin in 'CoreCNC\FormPlugin.pas',
  FaultRegistration in 'CoreCNC\FaultRegistration.pas',
  NC32ProbeOffset in 'CoreCNC\NC\NC32ProbeOffset.pas',
  ToolLifePlugin in 'CoreCNC\ToolLifePlugin.pas',
  CNCRegistry in 'CoreCNC\CNCRegistry.pas',
  CustomAppDisplay in 'Installable\CustomAppDisplay.pas',
  ScreenSaverFrm in 'Installable\CustomLayout\ScreenSaverFrm.pas' {SaverScreen},
  DynamicHelp in 'CoreCNC\DynamicHelp.pas',
  OEE in 'CoreCNC\OEE\OEE.pas',
  BaseKeyBoards in 'CoreCNC\Keyboard\BaseKeyBoards.pas',
  StringArray in 'CoreCNC\StringArray.pas',
  KBControl in 'CoreCNC\Keyboard\KBControl.pas',
  ExAccessEdit in 'Installable\PasswordAccess\ExAccessEdit.pas' {AccessesEditFrm},
  LogSyncObjs in 'CoreCNC\LogSyncObjs.pas',
  AboutForm in 'CoreCNC\AboutForm.pas' {About},
  CMMFileV1 in 'CoreCNC\CMMFileV1.pas',
  Plusmemo in 'CoreCNC\PlusMemo\Plusmemo.pas',
  PlcMono in 'CoreCNC\ObjectPLC\PlcMono.pas',
  PlcLamptest in 'CoreCNC\ObjectPLC\PlcLamptest.pas',
  SocketMemory in 'Installable\SocketMemory.pas',
  FTPServer in 'Installable\FTP\FTPServer.pas',
  FTP in 'Installable\FTP\FTP.pas',
  HeidenhainTable in 'CoreCNC\Heidenhain\HeidenhainTable.pas',
  DT3155 in 'Installable\DT3155.pas',
  TagFilter in 'CoreCNC\TagFilter.pas',
  ActiveXDisplay in 'Installable\activex\ActiveXDisplay.pas',
  XHost in 'Installable\activex\XHost.pas',
  AxUtils in 'Installable\activex\AxUtils.pas',
  OleAutomation in 'Installable\activex\OleAutomation.pas',
  IScriptInterface in '..\sdk1.4\IScriptInterface.pas',
  InvokableClient in 'Installable\activex\InvokableClient.pas',
  Telnet in 'Installable\Telnet.pas',
  Telnet64 in 'Installable\Telnet64.pas',
  StringArrayViewer in 'Installable\StringArrayViewer.pas',
  SoftkeyTreeData in 'Installable\SoftkeyTreeData.pas',
  SoftkeyTree in 'Installable\SoftkeyTree.pas',
  PanelLabels in 'Installable\FrontPanels\PanelLabels.pas',
  PanelImage in 'Installable\FrontPanels\PanelImage.pas',
  PanelGroup in 'Installable\FrontPanels\PanelGroup.pas',
  NCZeroOffsetForm in 'CoreCNC\NC\NCZeroOffsetForm.pas' {ZeroOffsetForm},
  Pmac32NC in 'Installable\Pmac32NC\Pmac32NC.pas',
  Pmac32NCConfigure in 'Installable\Pmac32NC\Pmac32NCConfigure.pas',
  Pmac32NCValues in 'Installable\Pmac32NC\Pmac32NCValues.pas',
  Pmac32NCIntros in 'Installable\Pmac32NC\Pmac32NCIntros.pas',
  PmacCommon in 'Installable\Pmac32NC\PmacCommon.pas',
  PSDLoader in 'CoreCNC\PSDE\PSDLoader.pas',
  ScrollProgramDisplay in 'Installable\ScrollProgramDisplay.pas',
  PlcGORItem in 'CoreCNC\ObjectPLC\PlcGORItem.pas',
  clExWatcher in 'CoreCNC\ExWatcher\clExWatcher.pas',
  clDbgHelp in 'CoreCNC\ExWatcher\clDbgHelp.pas',
  PlcAlarm in 'CoreCNC\ObjectPLC\PlcAlarm.pas',
  ISignature in '..\sdk1.4\ISignature.pas',
  Pcomm32 in 'CoreCNC\PmacUtil\Pcomm32.pas',
  RevisedErrorWindow in 'CoreCNC\ErrorDisplays\RevisedErrorWindow.pas',
  IOErrorListBox in 'CoreCNC\ErrorDisplays\IOErrorListBox.pas',
  DispatchErrorDisplay in 'CoreCNC\ErrorDisplays\DispatchErrorDisplay.pas',
  IdFTPServer in 'Installable\FTP\IdFTPServer.pas',
  IdFTPListOutput in 'Installable\FTP\IdFTPListOutput.pas',
  NC32VariableView in 'CoreCNC\NC\NC32VariableView.pas',
  NC32VariableListView in 'CoreCNC\NC\NC32VariableListView.pas',
  ParsingIniFile in '..\plugin.src\LFE\lib\parser\ParsingIniFile.pas',
  INCParser in '..\plugin.src\LFE\lib\parser\INCParser.pas',
  SimpleIniModel in '..\plugin.src\LFE\lib\parser\SimpleIniModel.pas',
  WebBrowser in 'Installable\WebBrowser.pas',
  JVM in 'Installable\JVM\JVM.pas',
  JNI in '..\sdk1.4\JNI.pas',
  JNIUtils in '..\sdk1.4\JNIUtils.pas',
  INCInterface in '..\sdk1.4\INCInterface.pas',
  INamedInterface in '..\sdk1.4\INamedInterface.pas',
  MDIEditor in 'Installable\MDIEditor.pas',
  StringArrayGrid in 'Installable\StringArrayGrid.pas',
  InlineTimer in 'CoreCNC\InlineTimer.pas',
  ICImagingControl3_TLB in 'Installable\ImagingControls\ICImagingControl3_TLB.pas',
  CustomReportScreen in 'Installable\ReportScreen\CustomReportScreen.pas',
  ReportScreenLibrary in 'Installable\ReportScreen\ReportScreenLibrary.pas',
  AbstractScript in 'CoreCNC\AbstractScript.pas',
  ReportScreenMethods in 'Installable\ReportScreen\ReportScreenMethods.pas',
  ReportScreenTable in 'Installable\ReportScreen\ReportScreenTable.pas',
  SimplePositionWindow in 'Installable\SimplePositionWindow.pas',
  Gauge in 'Installable\FrontPanels\Gauge.pas',
  Cognex in 'Installable\Cognex\Cognex.pas',
  VisionErrorStack in 'Installable\Cognex\VisionErrorStack.pas',
  CognexFTP in 'Installable\Cognex\CognexFTP.pas',
  CognexInSight_TLB in 'Installable\Cognex\CognexInSight_TLB.pas',
  CognexInSightDisplay_TLB in 'Installable\Cognex\CognexInSightDisplay_TLB.pas',
  CognexTelnetClient in 'Installable\Cognex\CognexTelnetClient.pas',
  CognexTypes in 'Installable\Cognex\CognexTypes.pas',
  CognexVisionControlPanel in 'Installable\Cognex\CognexVisionControlPanel.pas',
  CvsInSightDisplayOcx_TLB in 'Installable\Cognex\CvsInSightDisplayOcx_TLB.pas',
  FTPPanel in 'Installable\Cognex\FTPPanel.pas',
  FTPTelnetClient in 'Installable\Cognex\FTPTelnetClient.pas',
  GeneralTelnetClient in 'Installable\Cognex\GeneralTelnetClient.pas',
  TelnetDisplay in 'Installable\Cognex\TelnetDisplay.pas',
  TelnetIndicator in 'Installable\Cognex\TelnetIndicator.pas',
  TelnetIndicatorPanel in 'Installable\Cognex\TelnetIndicatorPanel.pas',
  TextInputCombo in 'Installable\Cognex\TextInputCombo.pas',
  VisionCommand in 'Installable\Cognex\VisionCommand.pas',
  CognexActions in 'Installable\Cognex\CognexActions.pas',
  CognexCommunicator in 'Installable\Cognex\CognexCommunicator.pas',
  DummyCognex in 'Installable\Cognex\DummyCognex.pas',
  CalGraphForm in 'Installable\AxisCalibration\CalGraphForm.pas' {CalibrationGraphForm},
  AxisCalibrationForm in 'Installable\AxisCalibration\AxisCalibrationForm.pas',
  CalDataLists in 'Installable\AxisCalibration\CalDataLists.pas',
  ImportHandler in 'Installable\AxisCalibration\ImportHandler.pas',
  CalDataStatusGrid in 'Installable\AxisCalibration\CalDataStatusGrid.pas',
  CalFileGenerator in 'Installable\AxisCalibration\CalFileGenerator.pas',
  CompFileParser in 'Installable\AxisCalibration\CompFileParser.pas',
  CalArchive in 'Installable\AxisCalibration\CalArchive.pas',
  RenishawImport in 'Installable\AxisCalibration\RenishawImport.pas',
  CalMessagePopup in 'Installable\AxisCalibration\CalMessagePopup.pas' {Form1},
  CompMessDialog in 'Installable\AxisCalibration\CompMessDialog.pas' {CalMsg};

{$R *.RES}
{$R FKeys.RES}
{$R IO.RES}
{$R List.RES}
{$R KB.RES}

resourcestring
  AlreadyRunningCaption = 'Error';
  AlreadyRunningMessage = 'Application already running';

begin
  if not Installer.AlreadyRunning then
  begin
    Application.Initialize;
    Application.Title := 'Advanced CNC';
    Application.CreateForm(TSplashForm, SplashForm);
  Application.CreateForm(TCalibrationGraphForm, CalibrationGraphForm);
  Application.CreateForm(TForm1, Form1);
  Application.CreateForm(TCalMsg, CalMsg);
  SplashForm.LoadImage;
    Application.CreateForm(TCNCInstaller, CNCInstaller);
    Application.Run;
  end else
  begin
    Windows.MessageBox(0, PChar(AlreadyRunningMessage), PChar(AlreadyRunningCaption), MB_OK);
  end;
end.

