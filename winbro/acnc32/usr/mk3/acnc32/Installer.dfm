object CNCInstaller: TCNCInstaller
  Left = 334
  Top = 250
  BorderIcons = [biHelp]
  BorderStyle = bsNone
  Caption = 'Installer'
  ClientHeight = 163
  ClientWidth = 191
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Arial'
  Font.Style = [fsBold]
  OldCreateOrder = True
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 16
  object PowerTimer: TTimer
    Enabled = False
    Interval = 100
    OnTimer = PowerTimerTick
    Left = 128
    Top = 8
  end
end
