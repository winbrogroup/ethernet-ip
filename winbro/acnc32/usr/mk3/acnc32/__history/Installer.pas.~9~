unit Installer;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  extctrls, IniFiles, CoreCNC, CNCTypes, CNC32, Named, Tokenizer, SyncObjs,
  FaultRegistration, {OPC,} StringArray, ComObj, clExWatcher, ParsingIniFile;

type
  TCNCInstaller = class(TForm)
    PowerTimer: TTimer;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure PowerTimerTick(Sender: TObject);
  private
    Ini              : TMemIniFile;
    keyPres          : TAbstractTag;
    MainFormHasFocus : TAbstractTag;
    LastActive       : Boolean;
    PowerTimerTicks  : Cardinal;
    LeftTag, TopTag  : TAbstractTag;
    ProperTermination : Boolean;
    clExceptWatcher: TclExceptWatcher;
    procedure clExceptWatcherSystemSafeCallException(Sender, Target: TObject;
      EI: TclExceptionInformation; var NotifyTarget: Boolean;
      StackTracer: TclStackTracer);
    procedure clExceptWatcherSystemException(Sender: TObject;
      EI: TclExceptionInformation; StackTracer: TclStackTracer);
    procedure clExceptWatcherDelphiSafeCallException(Sender, Target: TObject;
      E: Exception; EI: TclExceptionInformation; StackTracer: TclStackTracer);
    procedure clExceptWatcherDelphiException(Sender: TObject; E: Exception;
      EI: TclExceptionInformation; StackTracer: TclStackTracer);
    procedure SnapShot(var Message : TSnapShotMessage); message CNCM_SNAPSHOT;
    function GetCNCComponent(i: Integer): TAbstractCNC;
    function GetCNCComponentCount: Integer;
    function GetComponentFromName( aName: String): TAbstractCNC;
    procedure ReadSystemHeader(Params : TParamStrings);
    procedure FinalInstall;
    procedure AppMessage(var Message: TMsg; var Handled: Boolean);
    procedure ErrorChange(aTag : TAbstractTag);
    procedure AppException(Sender: TObject; E: Exception);
    procedure DoFatalForm(E : Exception; aTitle : string);
    procedure FormChange(aTag : TAbstractTag);
    procedure TerminateACNC32(aTag : TAbstractTag);
    procedure TerminateWindows(aTag : TAbstractTag);
    procedure ShowStack(Frames: TclStackFrames);
  protected
  public
    ShutdownComplete : Boolean;
    property CNCComponents[i: Integer]: TAbstractCNC read GetCNCComponent;
    property CNCComponentCount: Integer read GetCNCComponentCount;
    property ComponentFromName[ aName: String]: TAbstractCNC read GetComponentFromName;
  end;

const
  OPCServerGUID : TGUID = '{C99E5F6E-08F4-41FA-A0A0-0E773477A03E}';
  ServerVersion = 1;
  ServerDesc = 'CNC for Hi-Tech Machining Processes';
  ServerVendor = 'FSD';


var
  CNCInstaller: TCNCInstaller;

function AlreadyRunning: Boolean;

implementation

uses FatalError, Splash;

{$R *.DFM}

const
  ACNCMutexName = 'ACNC32MUTEX';
  DefaultConfigFilename = 'CNC.INI';
  ComponentSection = 'Components';
  PluginSection = 'Plugins';
  SystemSection    = 'System';
  PowerTimerDelay = 1000;

resourcestring
  CreatingComponentsMsg = 'Creating component.. ';
  InstallingComponentsMsg = 'Installing component.. ';
  PowerTimerWaitMsg = 'External hardware initialising...';
  FinalisingComponentsMsg = 'Finalising components.. ';
  CheckingComponentsMsg = 'Checking Components.. ';

function AlreadyRunning: Boolean;
{note:
  Win32 has no hPrevInstance variable. This technique is the
  'approved' way of preventing multiple instances under Win32.
  See  Win32.Hlp -> WinMain
  The handle to the mutex will be closed automatically when
  the process terminates, so there is no need to save it}
begin
  Windows.CreateMutex(nil, false, ACNCMutexName);
  Result:= GetLastError = ERROR_ALREADY_EXISTS
end;

procedure TCNCInstaller.FormCreate(Sender: TObject);
var
  I                : Integer;
  Params           : TParamStrings;
  NewName, NewType : string;
  ThisComp         : TAbstractCNC;
begin

  left             := 0;
  top              := 0;
  ThisComp         := nil;
  clExceptWatcher:= TclExceptWatcher.Create(Self);
  clExceptWatcher.OnDelphiException:= Self.clExceptWatcherDelphiException;
  clExceptWatcher.OnSystemException:= Self.clExceptWatcherSystemException;
  clExceptWatcher.OnDelphiSafeCallException:= Self.clExceptWatcherDelphiSafeCallException;
  clExceptWatcher.OnSystemSafeCallException:= Self.clExceptWatcherSystemSafeCallException;


  ComObj.CoInitializeEx(nil, 2);

  CreateDirectory(PChar(ProfilePath + LivePath), nil);

  SysObj.ConfigParser:= TParsingIniFile.Create(ProfilePath + ParserConfiguration);
  Ini := SysObj.ConfigParser.ParseIni(ProfilePath + DefaultConfigFileName);

  Params  := TParamStrings.Create;
  SysObj.FCNCComponents:= TStringList.Create;
  SysObj.VisualParent := Self; // Application.MainForm does not exist yet

  try
    PowerTimer.Enabled:= False;
    PowerTimerTicks:= 0;
    Ini.ReadSectionValues(SystemSection, SysObj.InstalledList);
    ReadSystemHeader(SysObj.InstalledList);

    SysObj.StringArraySet := TStringArraySet.Create(LocalPath + 'DB\');
    SysObj.Localized := TLocalization.Create;

    NameLang:= SysObj.Localized.GetString('$NAME');
    ValueLang:= SysObj.Localized.GetString('$VALUE');
    AliasLang:= SysObj.Localized.GetString('$ALIAS');
    OwnerLang:= SysObj.Localized.GetString('$OWNER');
    TypeLang:= SysObj.Localized.GetString('$TYPE');
    SizeLang:= SysObj.Localized.GetString('$SIZE');
    CallbacksLang:= SysObj.Localized.GetString('$CALLBACKS');
    TotalLang:= SysObj.Localized.GetString('$TOTAL');
    LimitLang:= SysObj.Localized.GetString('$LIMIT');
    MessageLang:= SysObj.Localized.GetString('$MESSAGE');
    UpLang:= SysObj.Localized.GetString('$UP');
    DownLang:= SysObj.Localized.GetString('$DOWN');
    ResetLang:= SysObj.Localized.GetString('$RESET');
    ProgramLang:= SysObj.Localized.GetString('$PROGRAM');
    SubroutineLang:= SysObj.Localized.GetString('$SUBROUTINE');

    RemainingLang:= SysObj.Localized.GetString('$REMAINING');
    ToolLifeLang:= SysObj.Localized.GetString('$TOOLLIFE');
    ToolTypeLang:= SysObj.Localized.GetString('$TOOLTYPE');


    TrueLang:= SysObj.Localized.GetString('$TRUE');
    FalseLang:= SysObj.Localized.GetString('$FALSE');


    //SysObj.BootLog := TFileStream.Create(LoggingPath + FbootLog, fmCreate);
    if FileExists(ProfilePath + 'Live\Errors.cfg') then
      FaultRegistration.ReadRegisteredFaults(ProfilePath + 'Live\Errors.cfg')
    else
      MessageLog('Unable to open Errors.cfg');

    FaultRegistration.WriteRegisteredFaults(ProfilePath + 'Live\Errors1.cfg');

    Ini.ReadSectionValues('Translate', SysObj.Translate);
    Ini.ReadSectionValues(PluginSection, SysObj.PluginList);
    try
      SysObj.InstalledList.Clear;
      Ini.ReadSectionValues(ComponentSection, SysObj.InstalledList);
      for i:= 0 to (SysObj.InstalledList.Count - 1) do begin
        NewName:= SysObj.InstalledList.Names[i];  {&&& '' is an error}
        NewType:= SysObj.InstalledList.Values[NewName];  {&&& error check}

        if ComponentFromName[NewName] <> nil then  {name already used}
          raise ECNCInstallFault.CreateFmt(NameUsedTwice, [NewName]);

        SplashForm.Msg(CreatingComponentsMsg + NewName);
        ThisComp:= FindCNCClass(NewName, NewType);

        if ThisComp = nil then
          raise ECNCInstallFault.CreateFmt(ClassNotFound, [NewName, NewType]);

        if ThisComp is TAbstractNC then
          if Assigned(SysObj.NC) then
             raise ECNCInstallFault.CreateFmt(CannotHaveTwo, [NewName, SysObj.NC.Name, SysObj.NC.ClassName])
          else
            SysObj.NC:= TAbstractNC(ThisComp);

        if ThisComp is TAbstractCommunications then
          if Assigned(SysObj.Comms) then begin
             raise ECNCInstallFault.CreateFmt(CannotHaveTwo, [NewName, SysObj.Comms.Name, SysObj.Comms.ClassName])
          end else begin
            SysObj.Comms := TAbstractCommunications(ThisComp);
            SysObj.IO := TAbstractCommunications(ThisComp);
          end;

        if ThisComp is TAbstractLayoutManager then
          if Assigned(SysObj.Layout) then
             raise ECNCInstallFault.CreateFmt(CannotHaveTwo, [NewName, SysObj.Layout.Name, SysObj.Layout.ClassName])
          else
             SysObj.Layout := TAbstractLayoutManager(ThisComp);

        if ThisComp is TAbstractSoftkey then
          if Assigned(SysObj.SoftKey) then
             raise ECNCInstallFault.CreateFmt(CannotHaveTwo, [NewName, SysObj.Softkey.Name, SysObj.Softkey.ClassName])
          else
             SysObj.Softkey := TAbstractSoftkey(ThisComp);

        if ThisComp is TAbstractFaultInterface then
          if Assigned(SysObj.FaultInterface) then
             raise ECNCInstallFault.CreateFmt(CannotHaveTwo, [NewName, SysObj.FaultInterface.Name, SysObj.FaultInterface.ClassName])
          else
             SysObj.FaultInterface := TAbstractFaultInterface(ThisComp);

        if ThisComp is TAbstractFileManager then
          if Assigned(SysObj.FileManager) then
             raise ECNCInstallFault.CreateFmt(CannotHaveTwo, [NewName, SysObj.FileManager.Name, SysObj.FileManager.ClassName])
          else
             SysObj.FileManager := TAbstractFileManager(ThisComp);

        SysObj.FCNCComponents.AddObject(ThisComp.name, ThisComp);
        SplashForm.Progress( Round(i*100/(SysObj.InstalledList.Count)))
      end;

      ThisComp := nil;

      if not Assigned(SysObj.NC) then
        raise ECNCInstallFault.CreateFmt(MustHaveDescendent, [TAbstractNC.ClassName]);
      if not Assigned(SysObj.Comms) then
        raise ECNCInstallFault.CreateFmt(MustHaveDescendent, [TAbstractCommunications.ClassName]);
      if not Assigned(SysObj.Layout) then
        raise ECNCInstallFault.CreateFmt(MustHaveDescendent, [TAbstractLayoutManager.ClassName]);
      if not Assigned(SysObj.Softkey) then
        raise ECNCInstallFault.CreateFmt(MustHaveDescendent, [TAbstractSoftkey.ClassName]);
      if not Assigned(SysObj.FaultInterface) then
        raise ECNCInstallFault.CreateFmt(MustHaveDescendent, [TAbstractFaultInterface.ClassName]);

  { All abstract commponents are now known and can be used }
      for i := 0 to SysObj.FCNCComponents.count - 1 do begin
        thiscomp      := TCNC(SysObj.FCNCComponents.Objects[i]);
        thiscomp.name := SysObj.FCNCComponents[i];
        if thiscomp = nil then
          raise ECNCInstallFault.CreateFmt(ImpossibleException, ['Installer.Create : nil Component'] );

        SplashForm.Msg(InstallingComponentsMsg + ThisComp.Name);

        if thiscomp is TCNC then begin
          Params.Clear;
          ini.ReadSectionValues(thisComp.Name, Params);
          messageLog(Format(componentInstalling, [ThisComp.Name]));
          SysObj.InstallComp:= ThisComp.Name;
          TCNC(thiscomp).InstallComponent(Params);
          TCNC(thiscomp).Font:= SysObj.VisualParent.Font;
          if ThisComp.InstallState <> InstallStateActive then
            raise ECNCInstallFault.Create('Prof Error: Component not installed correctly');
          SplashForm.Progress(Round(i*100/SysObj.FCNCComponents.Count));
        end else begin
          raise ECNCInstallFault.CreateFmt(ImpossibleException, ['Installer.Create : Not ' + TCNC.ClassName]);
        end;
      end;
    finally
      INI.Free
    end;
  except
    On E: Exception do begin
      if ThisComp = nil then
        DoFatalForm(E, 'Installation')
      else
        DoFatalForm(E, ThisComp.name);
      Halt;
    end;
  end;
  KeyPres := SysObj.Comms.TagPublish(NameKeyBoard, TIntegerTag);
  MainFormHasFocus := SysObj.Comms.TagPublish(NameMainFormHasFocus, TMethodTag);

  PowerTimer.Enabled:= True;
  SplashForm.Msg(PowerTimerWaitMsg);
  Params.Free;
  visible := false;
  WindowState := wsMinimized;
  SysObj.Comms.TagPublish('ACNC32Version', TStringTag).AsString := SplashForm.Version;
  SysObj.Comms.TagPublish('ACNC32ProfilePath', TStringTag).AsString := ProfilePath;
  SysObj.Comms.TagPublish('ACNC32MachSpecPath', TStringTag).AsString := MachSpecPath;
  SysObj.Comms.TagPublish('ACNC32ContSpecPath', TStringTag).AsString := ContractSpecificPath;
end;

procedure TCNCInstaller.FinalInstall;
var
  i        : integer;
  ThisComp : TCNC;
  AccessTag : TAbstractTag;
begin
  SysObj.Comms.TagEvent(nameIOfaultLevel, edgeChange, TIntegerTag, ErrorChange);
  SysObj.Comms.TagEvent('ACNC32NormalExit', edgeChange, TMethodTag, TerminateACNC32);
  SysObj.Comms.TagEvent('WindowsNormalExit', edgeChange, TMethodTag, TerminateWindows);
  AccessTag := SysObj.Comms.TagEvent('AccessLevel', EdgeChange, TIntegerTag, nil);
  ThisComp := nil;
  try
    try
      for i := 0 to SysObj.FCNCComponents.count - 1 do begin
        ThisComp := TCNC(SysObj.FCNCComponents.Objects[i]);
        if thiscomp <> nil then begin
          if thiscomp is TCNC then
          begin
            SplashForm.Msg(FinalisingComponentsMsg + ThisComp.Name);
            MessageLog(Format(FinalInstalling, [ThisComp.Name]));
            SplashForm.Progress(Round(i*100/SysObj.FCNCComponents.Count));
            SysObj.InstallComp:= ThisComp.Name;
            TCNC(ThisComp).installed;
            if ThisComp.InstallState <> InstallStateDone then
              raise ECNCInstallFault.Create('Prof Error: Component final install fault');
          end;
        end;
      end;
      LeftTag := SysObj.Comms.TagEvent('FormLeft', edgeChange, TIntegerTag, FormChange);
      TopTag := SysObj.Comms.TagEvent('FormTop', edgeChange, TIntegerTag, FormChange);
      for i := 0 to SysObj.FCNCComponents.count - 1 do begin
        ThisComp := TCNC(SysObj.FCNCComponents.Objects[i]);
        if thiscomp <> nil then begin
          if thiscomp is TCNC then
          begin
            SplashForm.Msg(CheckingComponentsMsg + ThisComp.Name);
            MessageLog(Format(CheckInstall, [ThisComp.Name]));
            SplashForm.Progress(Round(i*100/SysObj.FCNCComponents.Count));
            SysObj.InstallComp:= ThisComp.Name;
            TCNC(ThisComp).CheckInstallation;
            if ThisComp.InstallState <> InstallStateDone then
              raise ECNCInstallFault.Create('Prof Error: Component final install fault');
          end;
        end;
      end;
      SysObj.Softkey.Reset; //
      SysObj.Layout.Normal;
      SysObj.Softkey.keyEvent(VK_ESCAPE, edgeFalling); // is this required

      Application.onMessage := AppMessage;
      Application.OnException := AppException;
      SysObj.FaultInterface.DoResetRewind;
      SysObj.Comms.CheckCallbacks;
    except
      On E: Exception do if ThisComp = nil then
                                             DoFatalForm(E, 'Final Install')
                                           else
                                             DoFatalForm(E, ThisComp.name);
    end;

    MessageLog('----------------------------------');
    MessageLog('- List of installable components -');
    MessageLog('----------------------------------');
    for I := 0 to InstallableList.Count - 1 do begin
      MessageLog(InstallableList[I]);
    end;
    MessageLog('----------------------------------');
  finally
    //SysObj.bootLog.Free;
    //SysObj.BootLog:= nil;
    visible := true;
    WindowState := wsNormal;
  end;
  //SysObj.bootLog := TFileStream.Create(LoggingPath + 'Runtime.log', fmCreate);
  if Assigned(SplashForm) then
  begin
    SplashForm.Hide;
  end;
  AccessTag.AsInteger := 0;
end;

procedure TCNCInstaller.FormChange(aTag : TAbstractTag);
begin
  Left := LeftTag.AsInteger;
  Top := TopTag.AsInteger;
end;

procedure TCNCInstaller.AppMessage(var Message: TMsg; var Handled: Boolean);
begin
  if Active <> LastActive then begin
    SysObj.SoftKey.Enabled := Active;
    LastActive := Active;
    MainFormHasFocus.AsBoolean := LastActive;
  end;

  if not Active then
    Exit;

  case Message.message of
    WM_KEYDOWN       : begin
            case Message.wparam of
              VK_F1..VK_F12,
              VK_ESCAPE: begin
                handled := True;
                PostMessage(SysObj.Softkey.handle, CNCM_BUTTON_EVENT, Message.wparam, Ord(edgeRising));
                KeyPres.AsInteger := Message.WParam;
              end;
            end;
    end;


    WM_KEYUP         :
            case Message.wparam of
              VK_F1..VK_F12,
              VK_ESCAPE: begin
                handled := True;
                PostMessage(SysObj.Softkey.handle, CNCM_BUTTON_EVENT, Message.wparam, Ord(edgeFalling));
                KeyPres.AsInteger := 0;
              end;
            end;
  end;
end;

procedure TCNCInstaller.DoFatalForm(E : Exception; aTitle : string);
var
  Buffer   : array [0..2047] of Char;
begin
  PowerTimer.Enabled := False;
  if Assigned(SysObj.Comms) then
    SysObj.Comms.Freeze;

  ExceptionErrorMessage(ExceptObject, ExceptAddr, Buffer, 2047);
  CoreCNC.EventLog(Buffer);

  {
  if Assigned(SysObj.BootLog) then begin
    ExceptionErrorMessage(ExceptObject, ExceptAddr, Buffer, 2047);
    CoreCNC.MessageLog(Buffer);
    CoreCNC.EventLog(Buffer);
    SysObj.BootLog.Free;
    SysObj.BootLog := nil;
  end;
  }
  if Assigned(SysObj.IO) and (SysObj.IO.InstallState = InstallStateDone) then
    SysObj.IO.Terminate;

  {if Assigned(SysObj.BootLog) then
    SysObj.BootLog.Free; }
  FatalErrorForm := TFatalErrorForm.Construct(E, aTitle);
  FatalErrorForm.ShowModal;
  Application.Terminate;
end;

procedure TCNCInstaller.ErrorChange(aTag : TAbstractTag);
begin
  case SysObj.FaultInterface.FaultLevel of
    faultMessageOnly:  color := clGreen;
    faultWarning:      color := clBlue;
    faultStopCycle:    color := clYellow;
    faultRewind:       color := clYellow;
    faultEstop:        color := clRed;
    faultFatal:        color := clRed;
    faultNone:         color := clSilver;
  end;
end;

procedure TCNCInstaller.TerminateACNC32(aTag : TAbstractTag);
begin
  if not ProperTermination then begin
    ProperTermination := True;
    Close;
  end;
end;

procedure TCNCInstaller.TerminateWindows(aTag : TAbstractTag);
var HToken : Cardinal;
    tkp, newtkp : _TOKEN_PRIVILEGES;
    Length : Cardinal;
    F : LongBool;
begin
  if GetVersion < $80000000 then begin

    if not OpenProcessToken(GetCurrentProcess, TOKEN_ADJUST_PRIVILEGES or TOKEN_QUERY, HToken) then begin
      SysObj.FaultInterface.SetFault(SysObj.Comms, CoreFaults[cfCantShutdown], [], nil);
      Exit;
    end;


    {RegLoadKey, RegSaveKey}
//    LookupPrivilegeValue(nil, SE_SHUTDOWN_NAME, tkp.Previleges[0].Luid);
    LookupPrivilegeValue(nil, 'SeShutdownPrivilege', tkp.Privileges[0].Luid);

    tkp.PrivilegeCount := 1;
    tkp.Privileges[0].Attributes := SE_PRIVILEGE_ENABLED;

    F := False;
    AdjustTokenPrivileges( hToken, F, tkp, 0, newtkp, Length);
    if GetLastError <> ERROR_SUCCESS then begin
      SysObj.FaultInterface.SetFault(SysObj.Comms, CoreFaults[cfCantShutdown], [], nil);
      Exit;
    end;

  end;

  if not ExitWindowsEx(EWX_SHUTDOWN or EWX_FORCE, 0) then
    SysObj.FaultInterface.SetFault(SysObj.Comms, CoreFaults[cfCantShutdown], [], nil)
  else
    TerminateACNC32(aTag);
end;

procedure TCNCInstaller.AppException(Sender: TObject; E: Exception);
begin
  DoFatalForm(E, 'ACNC');
end;


procedure TCNCInstaller.FormClose(Sender: TObject;  var Action: TCloseAction);
var
  i        : integer;
  thiscomp : TComponent;
  //Tmp : string;
begin
  if not ProperTermination and (AccessLevelOEM1 in SysObj.Comms.AccessLevels) and (SysObj.FaultInterface.FaultLevel < FaultFatal) then begin
    Action := caNone;
    Exit;
  end;

  {if ACNCOPCServer.ClientCount > 0 then begin
    for I := 0 to ACNCOPCServer.ClientCount - 1 do
      Tmp := Tmp + ACNCOPCServer.ClientInfo(I).ClientName + ' ';
    MessageDlg(Format('Cannot Close: OPC Clients are connected' + CarRet +
                      '%d connection(s)' + CarRet +
                      Tmp,
                      [ACNCOPCServer.ClientCount]),
                      mtInformation, [mbOK], 0);
    Action := caNone;
    Exit;
  end; }
  Splashform.InfoLabel.Caption := 'ACNC is closing Down';
  SplashForm.Show;
  SplashForm.Update;

  for i := 0 to SysObj.FCNCComponents.Count - 1 do begin
    ThisComp := TCNC(SysObj.FCNCComponents.Objects[i]);
    if thiscomp <> nil then begin
      if thiscomp is TCNC then begin
        if not TCNC(thiscomp).IShutdownPermissive then begin
          Action := caNone;
          ProperTermination := False;
          Exit;
        end;
      end;
    end;
  end;

  Application.onMessage := nil;

  for i := 0 to SysObj.FCNCComponents.count - 1 do begin
    ThisComp := TCNC(SysObj.FCNCComponents.Objects[i]);
    if Thiscomp <> nil then begin
      if ThisComp is TCNC then begin
        try
          TCNC(thiscomp).IShutdown;
        except
          on E: Exception do begin
            EventLog('Shutdown failed on: ' + thiscomp.Name + CarRet + E.Message);
          end;
        end;
      end;
    end;
  end;

{  I really want to add this code but the current implementation of GEOCamOpEdit prevents this
   Need to talk to Dave about shutting down when IShutdown is called
  Sysobj.Comms:= nil;
  Sysobj.NC:= nil;
  Sysobj.IO:= nil;
  Sysobj.FaultInterface:= nil;

  for i := SysObj.FCNCComponents.count - 1 downto 0 do begin
    ThisComp := TCNC(SysObj.FCNCComponents.Objects[i]);
    if Thiscomp <> nil then begin
      if ThisComp is TCNC then begin
        try
          TCNC(thiscomp).Parent:= nil;
          TCNC(thiscomp).Free;
        except
          on E: Exception do begin
          end;
        end;
      end;
    end;
  end;  }

  SysObj.ClearPluginLibraries;
  ShutdownComplete := True;
  action := caFree;
  SplashForm.Close
end;

function TCNCInstaller.GetCNCComponent(i: Integer): TAbstractCNC;
begin
  Result:= TAbstractCNC(SysObj.FCNCComponents[i])
end;

function TCNCInstaller.GetCNCComponentCount: Integer;
begin
  Result:= SysObj.FCNCComponents.Count
end;

function TCNCInstaller.GetComponentFromName(aName: String): TAbstractCNC;
var
  i: Integer;
begin
  Result:= nil;
  for i:= 0 to (SysObj.FCNCComponents.Count - 1) do
  if CompareText(SysObj.FCNCComponents[i], aName) = 0 then begin
    Result:= TAbstractCNC(SysObj.FCNCComponents.Objects[i]);
    break
  end
end;

procedure RelativePath(var PV : string; const PF : string);
begin
  if (PV[2] <> ':') and (PV[1] <> '/') then
    PV := PF + PV;
end;

procedure TCNCInstaller.ReadSystemHeader(Params : TParamStrings);
  procedure EnsureDirectoryName(var aName : string);
  begin
    if aName[Length(aName)] <> '\' then
      aName := aName + '\';
  end;

  var F: TFont;

begin
  ShortDateFormat := Params.ReadString('DateTime', 'd/m/yyyy');
  LocalPath := SysUtils.IncludeTrailingBackslash(Params.ReadString('LocalPath', ProfilePath));
  RelativePath(LocalPath, ProfilePath);
  LocalDrive := ExtractFileDrive(LocalPath);

  LoggingPath := Params.ReadString('LoggingPath', ProfilePath);
  RelativePath(LoggingPath, ProfilePath);
  CreateDirectory(PChar(LoggingPath), nil);
  LoggingPath := SysUtils.IncludeTrailingBackslash(LoggingPath) + LoggingDirectory;
  CreateDirectory(PChar(LoggingPath), nil);

  // Add a new directive for temp path
  TemporaryPath := LoggingPath;

  MachSpecPath := Params.ReadString('MachSpecPath', ProfilePath);
  ContractSpecificPath := Params.ReadString('ContractSpecificPath', ProfilePath);
  RelativePath(MachSpecPath, ProfilePath);
  RelativePath(ContractSpecificPath, ProfilePath);

  SysObj.ClientPath := Params.ReadString('ClientPath', LocalPath);
  SysObj.ClientSystem := Params.ReadBool('ClientSystem', False);
  SysObj.ServerSystem := Params.ReadBool('ServerSystem', False);

  SysObj.StrictMDTUsage:= Params.ReadBool('StrictMDTUsage', True);


  EnsureDirectoryName(ProfilePath);
  EnsureDirectoryName(LoggingPath);
  EnsureDirectoryName(MachSpecPath);
  //EnsureDirectoryName(ProfilePath);
  EnsureDirectoryName(ContractSpecificPath);
  EnsureDirectoryName(SysObj.ClientPath);

  CreateDirectory(PChar(LocalPath), nil);
  CreateDirectory(PChar(MachSpecPath + '\Live'), nil);
  CreateDirectory(PChar(ProfilePath + '\Live'), nil);
  if ContractSpecificPath <> ProfilePath then begin
    CreateDirectory(PChar(ContractSpecificPath), nil);
    CreateDirectory(PChar(ContractSpecificPath + '\Live'), nil);
  end;

  F:= TFont.Create;
  F.Name:= Params.ReadString('FontName', 'Arial');
  F.Size:= 10;
  F.Style:= [fsBold];

  SysObj.FontName := F.Name;
  Sysobj.MonoFontName:= Params.Readstring('Monofontname', 'Courier New') ;
  Font:= F;
  SysObj.Heads := Params.ReadInteger('Heads', 1);
  Left := Params.ReadInteger('Left', 0);
  Top := Params.ReadInteger('Top', 0);
end;

procedure TCNCInstaller.PowerTimerTick(Sender: TObject);
var
  et: Cardinal;
begin
  Inc(PowerTimerTicks);
  et:= PowerTimerTicks*PowerTimer.Interval;
  if et >= PowerTimerDelay then
  begin
    PowerTimer.Enabled:= false;
    FinalInstall;
  end else
  begin
    SplashForm.Progress(Round(et*100/PowerTimerDelay))
  end
end;

procedure TCNCInstaller.SnapShot(var Message : TSnapShotMessage);
//var Lock : TCriticalSection;
begin
//  Lock := TCriticalSection.Create;
  Message.Bmp.Canvas.Lock;
  try
    try
      Message.Bmp.Width := Width;
      Message.Bmp.Height := Height;
      CNCInstaller.PaintTo(Message.Bmp.Canvas.Handle, 0, 0);
    except
      Message.Bmp.Canvas.TextOut(20, 20, 'Bugger it');
    end;
  finally
    Message.Bmp.Canvas.UnLock;
//    Lock.Release;
//    Lock.Free;
  end;
end;

procedure TCNCInstaller.clExceptWatcherDelphiException(Sender: TObject;
  E: Exception; EI: TclExceptionInformation; StackTracer: TclStackTracer);
begin
  memStackLog.Clear;
  memStackLog.Add('Exception:');
  ShowStack(StackTracer.GetCallStack(EI));
end;

procedure TCNCInstaller.clExceptWatcherDelphiSafeCallException(Sender,
  Target: TObject; E: Exception; EI: TclExceptionInformation;
  StackTracer: TclStackTracer);
begin
  memStackLog.Clear;
  memStackLog.Add('SafeCall Exception:');
  ShowStack(StackTracer.GetCallStack(EI));
end;

procedure TCNCInstaller.clExceptWatcherSystemException(Sender: TObject;
  EI: TclExceptionInformation; StackTracer: TclStackTracer);
begin
  memStackLog.Clear;
  memStackLog.Add('System Exception:');
  ShowStack(StackTracer.GetCallStack(EI));
end;

procedure TCNCInstaller.clExceptWatcherSystemSafeCallException(Sender,
  Target: TObject; EI: TclExceptionInformation; var NotifyTarget: Boolean;
  StackTracer: TclStackTracer);
begin
  memStackLog.Clear;
  memStackLog.Add('System SafeCall Exception:');
  ShowStack(StackTracer.GetCallStack(EI));
end;

procedure TCNCInstaller.ShowStack(Frames: TclStackFrames);
var
  i: Integer;
begin
  memStackLog.BeginUpdate();
  for i := 0 to Frames.Count - 1 do
  begin
    memStackLog.Add(Format('%x: %s - %s', [Integer(Frames[i].CallAddress),
      ExtractFileName(Frames[i].ModuleName), Frames[i].FunctionName]));
  end;
  memStackLog.EndUpdate();
end;


end.
