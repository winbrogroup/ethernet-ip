object SplashForm: TSplashForm
  Left = 436
  Top = 295
  BorderStyle = bsNone
  ClientHeight = 108
  ClientWidth = 172
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = True
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Image: TImage
    Left = 0
    Top = 0
    Width = 85
    Height = 85
    AutoSize = True
  end
  object InfoLabel: TLabel
    Left = 0
    Top = 79
    Width = 172
    Height = 16
    Align = alBottom
    Caption = 'ACNC application Loading. Please Wait.'
    Color = clBtnFace
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold, fsItalic]
    ParentColor = False
    ParentFont = False
  end
  object VersionLabel: TLabel
    Left = 0
    Top = 63
    Width = 172
    Height = 16
    Align = alBottom
    Caption = 'Version'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold, fsItalic]
    ParentFont = False
  end
  object ProgressBar: TProgressBar
    Left = 0
    Top = 95
    Width = 172
    Height = 13
    Align = alBottom
    TabOrder = 0
  end
end
