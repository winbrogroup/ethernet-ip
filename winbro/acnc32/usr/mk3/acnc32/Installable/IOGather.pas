unit IOGather;

interface
uses
  SysUtils, Classes, Gather, CoreCNC, Windows, PmacTerminal,
  PComm32;

const
  dwDeltaLinearBuffer = $1000 div 4; {rxBuffer allocated in blocks of this size (dwords)}

type
  TGatherMask = set of 0..31;   {force to DWORD}

  PLinearBuffer = ^TLinearBuffer;
  TLinearBuffer = array[0..$FFFFFFF] of Longint;

  TPMacGather = class(TAbstractGather) {zRingBuffer}
  private
    FSamplePeriod: Double;
    IOInterruptFrequency : Double;
    RxBuffer: PLinearBuffer;

    dwMaxLinearBuffer : Integer;

    dwRxAllocated: Integer;       {total Records in RxBuffer}
    dwRxCount: Integer;           {Rx records filled}
    LastReadTickCount: Integer;   {Windows.GetTickCount}
    dwOffset: array[TGatherHead, TGatherField] of Byte; {into record}
    dwRecordSize: Integer;        {Inc all fields for all heads}
    procedure ValidateBufferRead(var i: Integer; Head: TGatherHead; Field: TGatherField);
    procedure NewLinearBuffer;
    procedure FreeLinearBuffer;
  protected
    procedure SetSamplePeriod(Value : Double); override;
    function GetSamplePeriod: Double; override;
    function GetHeadData( i: Integer; Ch: Integer; Head: TGatherHead ): Integer; override;

    procedure HardwareStart;
    procedure HardwareStop;
    procedure DeleteGather; override;
    procedure StopGather(aHead : Integer); override;
    procedure StartGather(aHead : Integer); override;
    procedure DefineGather; override;
    function GetSampleCount(aHead : Integer): Integer; override;
    function GetSamplePeriodIndex(aIndex : Integer) : Double; override;
    function GetIndexOfSamplePeriod(Index : Double) : Integer; override;
  protected
  public
    procedure InstallComponent(Params: TParamStrings); override;
    procedure Shutdown; override;
    destructor Destroy; override;
    function GetAsInteger( i: Integer; Head: TGatherHead; Field: TGatherField): Integer;
    procedure Poll; override;
  end;

implementation

uses
  CNCTypes, EDMHead, CNC32, TypInfo;

resourcestring
  NotThisHead = 'Buffer does not contain data for head %d';
  NotThisField = 'Buffer does not contain data for field %d';
  NotThisRecord = 'Record %d does not exist in this buffer';
  NoHeadsDefined = 'Cannot %s no heads defined for gather';
  NoFieldsDefined = 'Cannot %s no data fields defined for gather';
  BadRingBufferAllocation = 'Bad ring buffer allocation. Code %d';

  CannotWhileStatus = 'Cannot %s while gather status is %s';
  DefineOp = 'Define gather';
  StartOp = 'start gather';
  ReadOp = 'read buffer';
  StopOp = 'stop gather';


function TPmacGather.GetSamplePeriodIndex(aIndex : Integer) : Double;
begin
  if aIndex >= 0 then
    Result := (aIndex + 1) / IOInterruptFrequency
  else
    Result := 1 / IOInterruptFrequency;
end;

function TPmacGather.GetIndexOfSamplePeriod(Index : Double) : Integer;
begin
  Result := Round(Index * IOInterruptFrequency) - 1;
end;


procedure TPMacGather.SetSamplePeriod(Value : Double);
begin
  FSamplePeriod := Value;
end;

function TPMacGather.GetSamplePeriod: Double;
begin
  Result:= FSamplePeriod
end;

function TPMacGather.GetHeadData( i: Integer; Ch: Integer; Head: TGatherHead ): Integer;
begin
  Result:= GetAsInteger(i, Head, Ch)
end;

function TPmacGather.GetAsInteger( i: Integer; Head: TGatherHead; Field: TGatherField): Integer;
begin
  ValidateBufferRead(i, Head, Field);
  Result := RxBuffer^[I * 4 + dwOffset[Head, Field]];
end;


procedure TPMacGather.NewLinearBuffer;
begin
  FreeLinearBuffer;
  GetMem(RxBuffer, dwMaxLinearBuffer * 4); // dwDeltaLinearBuffer * 4);
  dwRxAllocated:= dwMaxLinearBuffer; //dwDeltaLinearBuffer;
  dwRxCount:= 0;
end;

procedure TPmacGather.FreeLinearBuffer;
begin
  if RxBuffer <> nil then begin
    FreeMem(RxBuffer);
    RxBuffer:= nil;
    dwRxCount:= 0;
    dwRxAllocated:= 0
  end
end;

function TPmacGather.GetSampleCount(aHead : Integer): Integer;
begin
  Result:= 0;
  if (dwRecordSize > 0) and (dwRxCount > 0) then
    Result:= dwRxCount div dwRecordSize;
end;


procedure TPMacGather.HardwareStart;
begin
end;

procedure TPMacGather.HardwareStop;
begin
end;



procedure TPMacGather.InstallComponent(Params: TParamStrings);
var I : Integer;
begin
  IOInterruptFrequency := 100;
  for I := 1 to 100 do
    AllowableFrequencies.Add(Format('%.1fHz', [I / 1]));

  inherited InstallComponent(Params);

  dwMaxLinearBuffer := Params.ReadInteger('LinearBufferSize', $80000);
end;

procedure TPmacGather.Shutdown;
var I : Integer;
begin
  if GatherStatus = gsGathering then
    for I := 0 to SysObj.Heads - 1 do
      StopGather(I);
  DeleteGather;
  inherited ShutDown;
end;


destructor TPMacGather.Destroy;
begin
  inherited;
end;

procedure TPmacGather.Poll; {don't throw an exception in here}
begin
  if (GatherStatus = gsGathering) then begin
  end
end;


procedure TPmacGather.DeleteGather;
begin
  if GatherStatus <> gsGathering then begin
    FGatherStatus:= gsUndefined
  end
end;

procedure TPmacGather.DefineGather;
var I : Integer;
begin
  if GatherStatus = gsGathering then
    for I := 0 to SysObj.Heads - 1 do
      StopGather(I);

  DeleteGather;
  FGatherStatus:= gsDefined
end;

procedure TPmacGather.StopGather(aHead : Integer);
begin
  if (GatherStatus = gsGathering) then
    Poll;

  Exclude(FHeadsGathering, aHead);

  if FHeadsGathering = [] then begin
    HardwareStop;
    FGatherStatus:= gsDefined
  end;
end;

procedure TPmacGather.StartGather(aHead : Integer);
begin
  if GatherStatus in [gsUndefined] then
    raise EGatherError.CreateFmt(CannotWhileStatus,
      [StartOp, GetEnumName(TypeInfo(TGatherStatus), Integer(GatherStatus))]);
  if FHeadsGathering = [] then begin
    NewLinearBuffer;
    HardwareStart;
    LastReadTickCount:= Windows.GetTickCount;
    FGatherStatus:= gsGathering
  end;

  Include(FHeadsGathering, aHead);
end;

procedure TPmacGather.ValidateBufferRead(var i: Integer; Head: TGatherHead; Field: TGatherField);
begin
  if not (Head in Heads) then
    raise EGatherError.CreateFmt(NotThisHead, [Head]);
  if (Field >= ChannelCount) then
    raise EGatherError.CreateFmt(NotThisField, [Field]);
  if (i < 0) or (i >= dwRxCount div dwRecordSize) then
    raise EGatherError.CreateFmt(NotThisRecord, [i])
end;


initialization
  RegisterCNCClass(TPMacGather);
end.
