unit FatalError;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, CNC32, CNCTypes, ExtCtrls, CoreCNC, ComCtrls;

type
  TFatalErrorForm = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    OkBtn: TButton;
    Panel3: TPanel;
    Label1: TLabel;
    Panel4: TPanel;
    Panel5: TPanel;
    TitleLab: TLabel;
    LogListMemo: TRichEdit;
    ExceptionMemo: TRichEdit;
    procedure OkBtnClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
  public
    constructor Construct(E : Exception; aTitle : string);
  end;

var
  FatalErrorForm: TFatalErrorForm;
  memStackLog: TStringList;

implementation

{$R *.DFM}

resourcestring
  ExceptionOccuredInLang = 'Exception Occured in ';

procedure TFatalErrorForm.OkBtnClick(Sender: TObject);
begin
  Close;
end;

constructor TFatalErrorForm.Construct(E : Exception; aTitle : string);
var
  Buffer   : array [0..2047] of Char;
begin
  inherited Create(Application);
  TitleLab.Caption := ExceptionOccuredInLang + aTitle;
  ExceptionErrorMessage(ExceptObject, ExceptAddr, Buffer, 2047);
  ExceptionMemo.Lines.Text := E.Message; //Buffer;

  Font.Name := SysObj.FontName;
  LogListMemo.Lines.AddStrings(memStackLog);
end;

procedure TFatalErrorForm.FormCreate(Sender: TObject);
begin
  Left := (Screen.Width - Width) div 2;
  Top  := (Screen.Height - Height) div 2;
  resize;
end;


initialization
  memStackLog:= TStringList.Create;
end.
