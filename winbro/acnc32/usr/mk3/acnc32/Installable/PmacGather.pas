unit PmacGather;

interface
uses
  SysUtils, Classes, Gather, CoreCNC, Windows, PmacTerminal,
  PComm32;

{ This uses the 'Native' Gathering properties of the PMac. }

const
//  dwMaxLinearBuffer = $80000 div 4;  {maximum size of linear buffer (dwords)}
  dwDeltaLinearBuffer = $1000 div 4; {rxBuffer allocated in blocks of this size (dwords)}

type
  TSendPmacLine = function (s : string) : string of Object;

  TSourceAddr =  array[TGatherHead, TGatherField] of Integer;
  TGatherMask = set of 0..31;   {force to DWORD}

  PLinearBuffer = ^TLinearBuffer;
//  TLinearBuffer = array[0..dwMaxLinearBuffer - 1] of Longint;
  TLinearBuffer = array[0..$FFFFFFF] of Longint;

  TPMacGather = class(TAbstractGather) {zRingBuffer}
  private
    FSendLine : TSendPmacLine;
    InterruptsPerSample: Integer;
    FSamplePeriod: Double;
    AddrCache:  TSourceAddr;
    PMacInterruptFrequency : Double;
    PComm : TPComm32;
    RxBuffer: PLinearBuffer;

    dwMaxLinearBuffer : Integer;

    dwRxAllocated: Integer;       {total Records in RxBuffer}
    dwRxCount: Integer;           {Rx records filled}
    LastReadTickCount: Integer;   {Windows.GetTickCount}
    Tail: Integer;                {"}
    dwOffset: array[TGatherHead, TGatherField] of Byte; {into record}
    dwRecordSize: Integer;        {Inc all fields for all heads}
    dwMaxRingBufferSize: Integer;
    dwRingBufferSize: Integer;

    RingBufferLife : Integer;

    GatherInfo : array [TGatherHead] of TGatherInfo;
    function GetPMacDataType( Head: TGatherHead; Field: TGatherField): Integer;
    function PMAC_GetIVar(i: Word): Integer;
    procedure PMAC_SetIVar(i: Word; Value: Integer);
    function SourceIVar( Head: TGatherHead; Field: TGatherField): Word;
    procedure ValidateBufferRead(var i: Integer; Head: TGatherHead; Field: TGatherField);
    procedure NewLinearBuffer;
    procedure FreeLinearBuffer;
  protected
    function ParseAddressString(const Addr: String): Integer; override;
    procedure SetSamplePeriod(Value : Double); override;
    function GetSamplePeriod: Double; override;
    function GetHeadData( i: Integer; Ch: Integer; Head: TGatherHead ): Integer; override;
    function dwGetVariableSize(Head: TGatherHead; Field: TGatherField): Integer;

    procedure SetGatherMask( Value: TGatherMask);
    function GetGatherMask: TGatherMask;
    function GetVariableAddr( Head: TGatherHead; Field: TGatherField): Integer; override;
    procedure SetVariableAddr( Head: TGatherHead; Field: TGatherField; Value: Integer); override;
    procedure AllocateRingBuffer( Size: Integer);
    procedure FreeRingBuffer;
    procedure HardwareStart;
    procedure HardwareStop;
    property IVar[i: Word]: Integer read PMAC_GetIVar write PMAC_SetIVar;
    procedure DeleteGather; override;
    procedure StopGather(aHead : Integer); override;
    procedure StartGather(aHead : Integer); override;
    procedure DefineGather; override;
    function GetSampleCount(aHead : Integer): Integer; override;
    function GetSamplePeriodIndex(aIndex : Integer) : Double; override;
    function GetIndexOfSamplePeriod(Index : Double) : Integer; override;
  protected
  public
    procedure InstallComponent(Params: TParamStrings); override;
    procedure Shutdown; override;
    destructor Destroy; override;
    function GetAsInteger( i: Integer; Head: TGatherHead; Field: TGatherField): Integer;
    function GetAsComp( i: Integer; Head: TGatherHead; Field: TGatherField): Comp;
    function GetAsDouble( i: Integer; Head: TGatherHead; Field: TGatherField): Double;
    procedure Poll; override;
    property SendLine : TSendPmacLine read FSendLine write FSendLine;
    property GatherMask: TGatherMask read GetGatherMask write SetGatherMask;
  end;

{parameters to TInstallablePMacGather:

RingBufferSize = (how many dwords of DPR to use for ring buffer)
PComm = (name of PCommTerminal component typically PMac16Terminal or PMac32Terminal)

(read by TInstallableGather)
SamplePeriod=0.10 (seconds)
FieldNames = SemicolonText eg Position; Velocity; Voltage; Current
;Source Addresses
Head1Source=Addr, Addr, Addr
Head2Source
Head3Source
Head4Source

For this component address format is 'PMacNative' i.e.

  (X|Y|L|F):[$]AAAA

X = X word, Y = Y word L = LongWord
$ indicates hex address, otherwise decimal.

example:

Head1Source = Y:$002A, X:$0033, Y:$DF11, Y:$C016

parsing addresses is implemented by ParseAddressString

Spaces within addresses are NOT ALLOWED}

implementation

uses
  CNCTypes, EDMHead, CNC32, TypInfo;

resourcestring
  NotThisHead = 'Buffer does not contain data for head %d';
  NotThisField = 'Buffer does not contain data for field %d';
  NotThisRecord = 'Record %d does not exist in this buffer';
  NoHeadsDefined = 'Cannot %s no heads defined for gather';
  NoFieldsDefined = 'Cannot %s no data fields defined for gather';
  BadRingBufferAllocation = 'Bad ring buffer allocation. Code %d';

  CannotWhileStatus = 'Cannot %s while gather status is %s';
  DefineOp = 'Define gather';
  StartOp = 'start gather';
  ReadOp = 'read buffer';
  StopOp = 'stop gather';


const
  InstallablePCommName = 'PComm';
  InstallablePCommDefault = '';

  {PMac Data Type IDs}
  pmidInt24Y  = $000000;
  pmidInt24X  = $400000;
  pmidInt48   = $800000;
  pmidFloat48 = $C00000;


resourcestring
  PCommInterfaceNotDefined = 'Pcomm interface not defined in .INI';
  PcommInterfaceNotFound = 'Pcomm interface not installed';


function TPmacGather.GetSamplePeriodIndex(aIndex : Integer) : Double;
begin
  if aIndex >= 0 then
    Result := (aIndex + 1) / PMacInterruptFrequency
  else
    Result := 1 / PMacInterruptFrequency;
end;

function TPmacGather.GetIndexOfSamplePeriod(Index : Double) : Integer;
begin
  Result := Round(Index * PMacInterruptFrequency) - 1;
end;


function TPmacGather.dwGetVariableSize(Head: TGatherHead; Field: TGatherField): Integer;
{this returns the DPR size (Intel) rather than the 'native' size DOUBLEWORDS!}
begin
  case GetPMacDataType(Head, Field) of
    pmidInt24Y, pmidInt24X: Result:= 1;
    pmidInt48, pmidFloat48: Result:= 2;
  else
    Result:= 0 {error}
  end
end;

function TPmacGather.GetPMacDataType(Head: TGatherHead; Field: TGatherField): Integer;
begin
  Result:= AddrCache[Head, Field] and pmidFloat48;
end;

function TPmacGather.PMAC_GetIVar(i: Word): Integer;
var
  Code: Integer;
begin
  if Assigned(SendLine) then begin
    Val(SendLine(Format('I%d', [i])), Result, Code);
    if Code <> 0 then
      Result:= 0
  end else begin
    Result:= 0
  end
end;

procedure TPmacGather.PMAC_SetIVar(i: Word; Value: Integer);
{write I variable to PMAC}
begin
  if Assigned(SendLine) then
    SendLine(Format('I%d=%d', [i, Value]))
end;

function TPMacGather.SourceIVar( Head: TGatherHead; Field: TGatherField): Word;
begin
  Result:= 21 + Integer(Head)*4 + Integer(Field)
end;

procedure TPMacGather.SetSamplePeriod(Value : Double);
var GP : Double;
    IPS, I : Integer;
begin
  IPS := Round(Value*PMacInterruptFrequency);
  GP :=  IPS/PMacInterruptFrequency;
  if Abs(GP - FSamplePeriod) > 0.00001 then begin
    if GatherStatus = gsGathering then
      for I := 0 to SysObj.Heads - 1 do
        StopGather(I);
    FGatherStatus := gsUndefined;
  end;
  InterruptsPerSample := IPS;
  FSamplePeriod := GP;
end;

function TPMacGather.GetSamplePeriod: Double;
begin
  Result:= FSamplePeriod
end;

function TPMacGather.GetHeadData( i: Integer; Ch: Integer; Head: TGatherHead ): Integer;
begin
  Result:= GetAsInteger(i, Head, Ch)
end;

function TPmacGather.GetAsInteger( i: Integer; Head: TGatherHead; Field: TGatherField): Integer;
begin
  ValidateBufferRead(i, Head, Field);
  case GetPMacDataType(Head, Field) of
    pmidInt24Y, pmidInt24X: Result:=
//      Word(RxBuffer^[i*dwRecordSize + dwOffset[Head, Field]]);
      RxBuffer^[i*dwRecordSize + dwOffset[Head, Field]];
    pmidInt48: Result:= Trunc(GetAsComp(i, Head, Field));
    pmidFloat48: Result:= Round(GetAsDouble(i, Head, Field));
  else
    Result:= 0
  end
end;

function TPmacGather.GetAsComp( i: Integer; Head: TGatherHead; Field: TGatherField): Comp;
begin
  ValidateBufferRead(i, Head, Field);
  case GetPMacDataType(Head, Field) of
    pmidInt24Y, pmidInt24X: Result:= GetAsInteger(i, Head, Field);
    pmidInt48:
    with TCompRec(Result) do begin
      L0:=  RxBuffer^[i*dwRecordSize + dwOffset[Head, Field]];
      L1:=  RxBuffer^[i*dwRecordSize + dwOffset[Head, Field] + 1];
      B[7]:= B[6];    {Sign extend}
    end;
    pmidFloat48: Result:= GetAsDouble(i, Head, Field);
  else
    Result:= 0
  end
end;

function TPmacGather.GetAsDouble( i: Integer; Head: TGatherHead; Field: TGatherField): Double;
  {Where is the sign of the resultant number? Should we
   sign extend the mantissa? What about the exponent?
   PMACSOFT.HLP is too vague on the format used. I shall, for now, just
   assume that the format is the same as IEEE Double, with the least
   significant 16 bits of the mantissa thrown away. This is almost
   certainly wrong}
type
  TDoubleRec =
  packed record
  case Byte of
    0:(D: Double);
    1:(W0: Word; L0: Longint);
    2:(R0: array[0..4] of Byte; L1: Longint);
  end;
var
  DR: TDoubleRec; {must do this as TDoubleRec is bigger than double...}
begin
  ValidateBufferRead(i, Head, Field);
  case GetPMacDataType(Head, Field) of
    pmidInt24Y, pmidInt24X: Result:= GetAsInteger(i, Head, Field);
    pmidInt48: Result:= GetAsComp(i, Head, Field);
    pmidFloat48:
    with DR do begin
      W0:= 0;
      L0:= RxBuffer^[i*dwRecordSize + dwOffset[Head, Field]];
      L1:= RxBuffer^[i*dwRecordSize + dwOffset[Head, Field] + 1];
      Result:= D
    end;
  else
    Result:= 0
  end
end;

procedure TPMacGather.SetGatherMask( Value: TGatherMask);
begin
  IVar[20]:= Integer(Value)
end;

function TPMacGather.GetGatherMask: TGatherMask;
begin
  Result:= TGatherMask(IVar[20])
end;

function TPMacGather.GetVariableAddr( Head: TGatherHead; Field: TGatherField): Integer;
begin
  Result:= AddrCache[Head, Field]
end;

procedure TPMacGather.SetVariableAddr( Head: TGatherHead; Field: TGatherField; Value: Integer);
begin
  AddrCache[Head, Field]:= Value;
  {don't actually send to PMAC until define...}
end;

procedure TPMacGather.NewLinearBuffer;
begin
  FreeLinearBuffer;
  GetMem(RxBuffer, dwMaxLinearBuffer * 4); // dwDeltaLinearBuffer * 4);
  dwRxAllocated:= dwMaxLinearBuffer; //dwDeltaLinearBuffer;
  dwRxCount:= 0;
end;

procedure TPmacGather.FreeLinearBuffer;
begin
  if RxBuffer <> nil then begin
    FreeMem(RxBuffer);
    RxBuffer:= nil;
    dwRxCount:= 0;
    dwRxAllocated:= 0
  end
end;

function TPmacGather.GetSampleCount(aHead : Integer): Integer;
begin
  Result:= 0;
  if (dwRecordSize > 0) and (dwRxCount > 0) then begin
    Result:= dwRxCount div dwRecordSize;
    if aHead in FHeadsGathering then  // Still gathering
      Result := Result - GatherInfo[aHead].StartRecord
    else
      Result := GatherInfo[aHead].StopRecord - GatherInfo[aHead].StartRecord;
  end;
end;

{hardware dependent}
procedure TPMacGather.AllocateRingBuffer( Size: Integer);
var
  i, j: Integer;
begin
  if Assigned(SendLine) then begin
    for i:= Low(TGatherHead) to High(TGatherHead) do
      for j:= Low(TGatherField) to High(TGatherField) do
        IVar[SourceIVar(i, j)]:= AddrCache[i, j];
    IVar[19]:= InterruptsPerSample;
    IVar[45]:= 3;
    {OneTime intialisations complete}
    SendLine(Format('DEFINE GATHER %d', [Size]));
    {setup the Ringbuffer pointer}
  end
end;

procedure TPMacGather.FreeRingBuffer;
begin
  HardwareStop;
  if Assigned(SendLine) then
    SendLine('DELETE GATHER')
end;

procedure TPMacGather.HardwareStart;
var Head : Integer;
begin
  for Head := 0 to SysObj.Heads - 1 do begin
    GatherInfo[Head].StartRecord := 0;
    GatherInfo[Head].StopRecord := 0;
  end;

  if Assigned(SendLine) then
    SendLine('GATHER');
  Sleep(50);
end;

procedure TPMacGather.HardwareStop;
begin
  if Assigned(SendLine) then
    SendLine('ENDGATHER');
end;


const
  DefaultAddresses: TSourceAddr =
{Head 0}
  {gfPosition}((($000a + $20) or pmidInt24Y,
  {gfVelocity}  ($0013 + $20) or pmidInt24X,
  {gfAnalog1}    $DF11 or pmidInt24Y,
  {gfAnalog2}    $C016 or pmidInt24Y),

{Head 1}
  {gfPosition} (($000a + $5C) or pmidInt24Y,
  {gfVelocity}  ($0013 + $5C) or pmidInt24X,
  {gfAnalog1}    $C007 or pmidInt24Y,
  {gfAnalog2}    $C017 or pmidInt24Y),

{Head 2}
  {gfPosition} (($000a + $98) or pmidInt24Y,
  {gfVelocity}  ($0013 + $98) or pmidInt24X,
  {gfAnalog1}    $C00e or pmidInt24Y,
  {gfAnalog2}    $C01e or pmidInt24Y),

{Head 3}
  {gfPosition} (($000a + $D4) or pmidInt24Y,
  {gfVelocity}  ($0013 + $D4) or pmidInt24X,
  {gfAnalog1}    $C00f or pmidInt24Y,
  {gfAnalog2}    $C01f or pmidInt24Y));


procedure TPMacGather.InstallComponent(Params: TParamStrings);
var PCommName      : string;
    PCommInterface : TInstallablePComm;
    I : Integer;
begin
  AddrCache:= DefaultAddresses;
  PMacInterruptFrequency := Params.ReadDouble('PmacFrequency', 2250);  {Hz THIS MUST BE CORRECT}
  for I := 0 to 99 do
    AllowableFrequencies.Add(Format('%.1fHz', [1 / SamplePeriodIndex[I]]));

  inherited InstallComponent(Params);

  dwMaxLinearBuffer := Params.ReadInteger('LinearBufferSize', $80000);
  dwMaxRingBufferSize:= Params.ReadInteger('RingBufferSize', $800);

  PCommName := Params.ReadString(InstallablePCommName, InstallablePCommDefault);
  if PCommName = InstallablePCommDefault then
    raise ECNCInstallFault.Create(PCommInterfaceNotDefined);
  PCommInterface := TInstallablePComm(GetComponent(PCommName));
  if PCommInterface = nil then
    raise ECNCInstallFault.Create(PcommInterfaceNotFound);
  SendLine := PCommInterface.SendPMacLine;

  if PCommInterface is TPmac32Terminal then
    PComm := TPmac32Terminal(PCommInterface).PComm32
  else
    raise ECNCInstallFault.Create(Name + ' Requires TPMac32Terminal as ' + PCommName);
end;

procedure TPmacGather.Shutdown;
var I : Integer;
begin
  if GatherStatus = gsGathering then
    for I := 0 to SysObj.Heads - 1 do
      StopGather(I);
  DeleteGather;
  inherited ShutDown;
end;


destructor TPMacGather.Destroy;
begin
  inherited;
end;

function TPMacGather.ParseAddressString(const Addr: String): Integer;
{   for PMACNative
     (X|Y|L|F):[$]AAAA
     X = X word, Y = Y word L = LongWord
     $ indicates hex, otherwise decimal
     no spaces allowed!
     }
var
  VarType: Integer;
  Astr: String;
begin
  Result:= -1;
  if Length(Addr) < 3 then Exit;
  if Addr = '' then Exit;
  case UpCase(Addr[1]) of
    'X': VarType:= pmidInt24X;
    'Y': VarType:= pmidInt24Y;
    'L': VarType:= pmidInt48;
    'F': VarType:= pmidFloat48;
  else
    Exit
  end;
  if Addr[2] <> ':' then Exit;
  AStr:= Trim(Copy(Addr, 3, Length(Addr) - 2));
  if AStr = '' then Exit;
  try
    Result:= StrToInt(AStr);
    {note that StrToInt supports Hex notation}
    Result:= Result or VarType
  except
    on EConvertError do
  end
end;

const
  PMacGatherPointer = $023F * 4;
  PMacGatherAddress = $0240 * 4;

procedure TPmacGather.Poll; {don't throw an exception in here}
var
  PNext: Integer;
  dwReadSize: Integer;
  EstReadInterval: Integer;
  TickCount: Integer;
  LastCount: Integer;
begin
  if (GatherStatus = gsGathering) then begin
    PNext:= PComm.IDPR[PmacGatherPointer] shr 16; {leave 'real' pointer alone now}
    if (PNext <> Tail) then begin
      LastCount:= dwRxCount div dwRecordSize;
      dwReadSize:= (PNext - Tail + dwRingBufferSize) mod dwRingBufferSize;
      EstReadInterval:= Round((dwReadSize * SamplePeriod * 1000) / dwRecordSize);
      TickCount:= Windows.GetTickCount;
      if Abs(EstReadInterval - (TickCount - LastReadTickCount)) > (RingBufferLife div 2) then begin
        FGatherStatus:= gsRingBufferCrash
      end else begin
        if dwRxCount + dwReadSize > dwRxAllocated then begin
//          Inc(dwRxAllocated, dwDeltaLinearBuffer);
//          if dwRxAllocated <= dwMaxLinearBuffer then begin
//            ReallocMem(RxBuffer, dwRxAllocated * 4)
//          end else begin
            FGatherStatus:= gsTooMuchData;
            Exit;
//          end
        end;
        LastReadTickCount:= TickCount;
        if PNext > Tail then begin  {single block}
          PComm.GetDPRMem(PMacGatherAddress + (Tail*4), dwReadSize * 4, @RxBuffer[dwRxCount]);
          Inc(dwRxCount, dwReadSize);
        end else begin
          if Tail < dwRingBufferSize then begin
            PComm.GetDPRMem(PMacGatherAddress + (Tail*4), (dwRingBufferSize - Tail) * 4, @RxBuffer[dwRxCount]);
            Inc(dwRxCount, dwRingBufferSize - Tail)
          end;
          if pNext > 0 then begin
            PComm.GetDPRMem(PMacGatherAddress, PNext*4, @RxBuffer[dwRxCount]);
            Inc(dwRxCount, pNext)
          end;
        end;
        Tail:= PNext;
        NewSamples(LastCount)
      end
    end
  end
end;


procedure TPmacGather.DeleteGather;
begin
  if GatherStatus <> gsGathering then begin
    FreeRingBuffer;
    FGatherStatus:= gsUndefined
  end
end;

procedure TPmacGather.DefineGather;
  procedure DefineErrorFmt(aText : string; const Args : array of const);
  begin
    SysObj.IO.SetFault(Self, Format(aText, Args), FaultInterlock, nil);
  end;

var
  I : TGatherHead;
  J : TGatherField;
  gm: Longint;
begin
  if GatherStatus = gsGathering then
    for I := 0 to SysObj.Heads - 1 do
      StopGather(I);
//    DefineErrorFmt(CannotWhileStatus, [DefineOp, GetEnumName(TypeInfo(TGatherStatus), Integer(GatherStatus))]);
  if Heads = [] then
    DefineErrorFmt(NoHeadsDefined, [StartOp]);
  if ChannelCount = 0 then
    DefineErrorFmt(NoFieldsDefined, [StartOp]);

  DeleteGather;
  dwRecordSize:= 0;
  gm:= 0;
  for i:= Low(TGatherHead) to High(TGatherHead) do
    if i in Heads then begin
      for j:= 0 to ChannelCount - 1 do begin
        dwOffset[i, j]:= dwRecordSize;
        Inc(dwRecordSize, dwGetVariableSize(i, j));
        gm:= gm or (1 shl ((i*4) + Byte(j)))
      end
    end;
  GatherMask:= TGatherMask(gm);
  {potential errors: &&&
    FRecordSize = 0 (should be filtered out by checks above) }
  dwRingBufferSize:= (dwMaxRingBufferSize div dwRecordSize) * dwRecordSize;

  {Work out the 'life' of the buffer in milliseconds. We will use this
  information for buffer overflow checks. See notes above}
  RingBufferLife:= Round((dwRingBufferSize * SamplePeriod * 1000)/dwRecordSize);
  AllocateRingBuffer(dwRingBufferSize);
  if (PComm.IDPR[PMacGatherPointer] and $FFFF) <> dwRingBufferSize then
    DefineErrorFmt(BadRingBufferAllocation, [20]);
  if (PComm.IDPR[PmacGatherPointer] shr 16) <> 0 then
    DefineErrorFmt(BadRingBufferAllocation, [30]);
  Tail := 0;
  FGatherStatus:= gsDefined
end;

procedure TPmacGather.StopGather(aHead : Integer);
begin
  if (GatherStatus = gsGathering) then begin
    Poll;
    GatherInfo[aHead].StopRecord := dwRxCount div dwRecordSize;
  end;

  Exclude(FHeadsGathering, aHead);

  if FHeadsGathering = [] then begin
    HardwareStop;
    FGatherStatus:= gsDefined
  end;
end;

procedure TPmacGather.StartGather(aHead : Integer);
begin
  if GatherStatus in [gsUndefined] then
    raise EGatherError.CreateFmt(CannotWhileStatus,
      [StartOp, GetEnumName(TypeInfo(TGatherStatus), Integer(GatherStatus))]);
  if FHeadsGathering = [] then begin
    NewLinearBuffer;
    HardwareStart;
    Tail := 0;
    LastReadTickCount:= Windows.GetTickCount;
    FGatherStatus:= gsGathering
  end;

  Include(FHeadsGathering, aHead);
  GatherInfo[aHead].StartRecord := dwRxCount div dwRecordSize;
end;

procedure TPmacGather.ValidateBufferRead(var i: Integer; Head: TGatherHead; Field: TGatherField);
begin
  I := I + GatherInfo[Head].StartRecord;

  if not (Head in Heads) then
    raise EGatherError.CreateFmt(NotThisHead, [Head]);
  if (Field >= ChannelCount) then
    raise EGatherError.CreateFmt(NotThisField, [Field]);
  if (i < 0) or (i >= dwRxCount div dwRecordSize) then
    raise EGatherError.CreateFmt(NotThisRecord, [i])
end;


initialization
  RegisterCNCClass(TPMacGather);
end.
