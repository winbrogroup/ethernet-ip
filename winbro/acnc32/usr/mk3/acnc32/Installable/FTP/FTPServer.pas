unit FTPServer;

interface

uses
  Windows, SysUtils, Classes, IdBaseComponent, IdComponent, IdUDPBase,
  IdUDPServer, IdSocketHandle, IdTCPServer, IdFTPServer, IdFTPList, IdContext,
  IdFTPListOutput, IdCmdTCPServer;

type
  TFTPServer = class;

  TLoggingLevel = (
    llMinimum,
    llExceptional,
    llUser,
    llVerbose
  );

  TLoggingLevels = set of TLoggingLevel;

  TFTPLoggingEvent = procedure (Sender: TOBject; const Text: string) of object;
  TFTPDirectory = class(TObject)
  public
    LocalPath: string;
    PublicPath: string;
    ReadOnly: Boolean;
  end;

  TDirectoryList = class(TObject)
  private
    List: TList;
    FTP: TFTPServer;
    function GetItem(Index: Integer): TFTPDirectory;
  public
    constructor Create(AFTP: TFTPServer);
    function GetPrivatePath(ASender: TIdFTPServerContext; const APath: string; var AReadOnly: Boolean): string;
    function CheckPublicPath(const APath, Default: string): string;
    property Items[Index: Integer]: TFTPDirectory read GetItem; default;
    function Count: Integer;
  end;

  TFTPServer = class(TObject)
  private
    IdFTPServer: TIdFTPServer;
    Dummy: Boolean;
    Directory: TDirectoryList;
    Owner: TComponent;
    FReadOnly: Boolean;
    FLoggingLevel: TLoggingLevels;
    FOnLogging: TFTPLoggingEvent;
    procedure EventLog(const Text: string; Level: TLoggingLevel = lluser);

    procedure IdFTPServerUserLogin(ASender: TIdFTPServerContext; const AUsername, APassword: string;
      var AAuthenticated: Boolean);

    procedure IdFTPServerConnect(AContext: TIdContext);
    //procedure IdFTPServerNoCommandHandler(ASender: TIdTCPServer;
    //  const AData: String; AContext:TIdContext);
    procedure IdFTPServerRetrieveFile(ASender: TIdFTPServerContext; const AFileName: string;
    var VStream: TStream);
    procedure IdFTPServerStatus(ASender: TObject;
      const AStatus: TIdStatus; const AStatusText: String);
    procedure IdFTPServerDisconnect(AContext: TIdContext);
    procedure IdFTPServerListDirectory(ASender: TIdFTPServerContext; const APath: string;
    ADirectoryListing: TIdFTPListOutput; const ACmd : String; const ASwitches : String);
    procedure IdFTPServerChangeDirectory(ASender: TIdFTPServerContext; var VDirectory: string);
    procedure IdFTPServerAfterCommandHandler(ASender: TIdCmdTCPServer;
    AContext: TIdContext);
    procedure IdFTPServerAfterUserLogin(ASender: TIdFTPServerContext);
    procedure IdFTPServerBeforeCommandHandler(ASender: TIdCmdTCPServer;
    var AData: string; AContext: TIdContext);
    procedure IdFTPServerDeleteFile(ASender: TIdFTPServerContext; const APathName: string);
    procedure IdFTPServerException(AContext: TIdContext; AException: Exception);
    procedure IdFTPServerExecute(AContext: TIdContext);
    //procedure IdFTPServerGetCustomListFormat(ASender: TIdFTPServer;
    //  AItem: TIdFTPListItem; var VText: String);
    procedure IdFTPServerGetFileSize(ASender: TIdFTPServerContext; const AFilename: string;
    var VFileSize: Int64);
    procedure IdFTPServerListenException(AThread: TIdListenerThread;
      AException: Exception);
    procedure IdFTPServerMakeDirectory(ASender: TIdFTPServerContext; var VDirectory: string);
    procedure IdFTPServerRemoveDirectory(ASender: TIdFTPServerContext; var VDirectory: string);
    procedure IdFTPServerRenameFile(ASender: TIdFTPServerContext; const ARenameFromFile,ARenameToFile: string);
    procedure IdFTPServerStoreFile(ASender: TIdFTPServerContext; const AFileName: string;
    AAppend: Boolean; var VStream: TStream);
    procedure IdFTPChangeDirectory(ASender: TIdFTPServerContext; var VDirectory: string);
    procedure GenerateDirectoryListing(const APath: string; ADirectoryListing: TIdFTPListOutput);
  public
    constructor Create(AOwner: TComponent);
    procedure AddPath(APath: TFTPDirectory);
    property ReadOnly: Boolean read FReadOnly write FReadOnly;
    property LoggingLevels: TLoggingLevels read FLoggingLevel write FLoggingLevel;
    property OnLogging: TFTPLoggingEvent read FOnLogging write FOnLogging;
    procedure Open;
    procedure Close;
  end;

implementation


procedure TranslateChar(var Str: string; FromChar, ToChar: Char);
var
  I: Integer;
begin
  for I := 1 to Length(Str) do
    if Str[I] = FromChar then
      Str[I] := ToChar;
end;


procedure TFTPServer.IdFTPServerUserLogin(ASender: TIdFTPServerContext; const AUsername, APassword: string;
      var AAuthenticated: Boolean);
begin
  AAuthenticated := (AUsername = 'amchem') and (APassword = 'MyDAS');
  if AAuthenticated then
    EventLog('Log-in from ' + ASender.Connection.Socket.Binding.PeerIP)
  else
    EventLog(Format('Failed log-in from %s Username: "%s" Password "%s"', [ASender.Connection.Socket.Binding.PeerIP, AUserName, APassword] ));
  ASender.CurrentDir := '/';
end;

procedure TFTPServer.GenerateDirectoryListing(const APath: string; ADirectoryListing: TIdFTPListOutput);
var SearchRec: TSearchRec;
    Item : TIdFTPListItem;
    I : Integer;
begin
  //ADirectoryListing.ListFormat:= flfDos;
  if (APath = '/') then begin
    for I := 0 to Directory.Count - 1 do begin
      Item := ADirectoryListing.Add;
      //Item..OwnerPermissions:= '-';
      //Item.GroupPermissions:= '-';
      //Item.UserPermissions:= '-';
      //Item.OwnerName:= 'acnc32';
      Item.Size := 32;
      Item.ModifiedDate := Now;
      Item.FileName := Directory[I].PublicPath;
      Item.ItemType := ditDirectory;
      Item.DisplayName := Item.FileName;
    end;
  end else begin
    if FindFirst(APath + '*', faAnyFile, SearchRec) = 0 then begin
      try
        repeat
          Item := ADirectoryListing.Add;
          //Item.OwnerPermissions:= '-';
          //Item.GroupPermissions:= '-';
          //Item.UserPermissions:= '-';
          //Item.OwnerName:= 'acnc32';
          Item.Size := SearchRec.Size;
          Item.ModifiedDate := FileDateToDateTime(SearchRec.Time);
          Item.FileName := SearchRec.Name;
          if SearchRec.Attr and faDirectory <> 0 then
            Item.ItemType := ditDirectory
          else
            Item.ItemType := ditFile;

        until FindNext(SearchRec) <> 0;
      finally
        FindClose(SearchRec);
      end;
    end;
  end;
end;

procedure TFTPServer.IdFTPServerRetrieveFile(ASender: TIdFTPServerContext; const AFileName: string;
    var VStream: TStream);
var ThisPath: string;
begin
//  Memo1.Lines.Add('Retrieve file: ' + AFileName);
  ThisPath := Directory.GetPrivatePath(ASender, AFileName, Dummy);

  try
    if FileExists(ThisPath) then
      VStream:= TFileStream.Create(ThisPath, fmOpenRead)
    else
      raise Exception.Create('550 File does not exist');
  except
    raise Exception.Create('550 Unable to read file');
    //ASender.Connection.IOHandler.WriteLn('550 Unable to read file');
  end;
end;

procedure TFTPServer.IdFTPServerListDirectory(ASender: TIdFTPServerContext; const APath: string;
    ADirectoryListing: TIdFTPListOutput; const ACmd : String; const ASwitches : String);
var ThisPath: string;
begin
  ThisPath := Directory.GetPrivatePath(ASender, APath, Dummy);
//  Memo1.Lines.Add('List directory: ' + APath + 'Actual: ' + ThisPath);
  if ASender.Authenticated then begin
    GenerateDirectoryListing(ThisPath, ADirectoryListing);
  end else begin
    ASender.Connection.IOHandler.WriteLn('530 Log-on required');
  end;
end;

procedure TFTPServer.IdFTPServerChangeDirectory(ASender: TIdFTPServerContext; var VDirectory: string);
begin
  if ASender.CurrentDir = '' then
    ASender.CurrentDir := '/';

  VDirectory:= Directory.CheckPublicPath(VDirectory, ASender.CurrentDir);
//  Memo1.Lines.Add('Change to directory: ' + VDirectory);
end;

procedure TFTPServer.IdFTPServerDeleteFile(ASender: TIdFTPServerContext; const APathName: string);
var ThisPath: string;
    ReadOnly: Boolean;
begin
//  Memo1.lines.add('IdFTPServerDeleteFile: ' + ASender.CurrentDir + ' ' + APathName);
  ThisPath := Directory.GetPrivatePath(ASender, APathName, ReadOnly);
  if not ReadOnly then begin
    if FileExists(ThisPath) then begin
      if not DeleteFile(ThisPath) then
        ASender.Connection.IOHandler.WriteLn('550 File failed to delete');
    end else begin
      ASender.Connection.IOHandler.WriteLn('550 File not found');
    end;
  end else begin
    ASender.Connection.IOHandler.WriteLn('550 File is read only');
  end;

//  Memo1.lines.add('IdFTPServerDeleteFile: ' + APathName);
end;


procedure TFTPServer.IdFTPServerGetFileSize(ASender: TIdFTPServerContext; const AFilename: string;
    var VFileSize: Int64);
var ThisPath: string;
    SearchRec: TSearchRec;
begin
//  Memo1.lines.add('IdFTPServerGetFileSize: ' + AFileNAme);
  ThisPath := Directory.GetPrivatePath(ASender, AFileName, Dummy);
  VFileSize := 0;
  if FindFirst(ThisPath, faAnyFile, SearchRec) = 0 then begin
    VFileSize := SearchRec.Size;
  end;
  FindClose(SearchRec);
  EventLog('IdFTPServerGetFileSize: ' + AFileName + ': ' + IntToStr(VFileSize));
end;

procedure TFTPServer.IdFTPServerMakeDirectory(ASender: TIdFTPServerContext; var VDirectory: string);
var ThisPath: string;
    ReadOnly: Boolean;
begin
//  Memo1.lines.add('IdFTPServerMakeDirectory: ' + VDirectory);
  ThisPath:= Directory.GetPrivatePath(ASender, VDirectory, ReadOnly) ;

  if not ReadOnly then begin
    CreateDirectory(PChar(ThisPath), nil);
  end else begin
    ASender.Connection.IOHandler.WriteLn('550 File is read only');
  end;
end;

procedure TFTPServer.IdFTPServerRemoveDirectory(ASender: TIdFTPServerContext; var VDirectory: string);
var ThisPath: string;
    ReadOnly: Boolean;
begin
//  Memo1.lines.add('IdFTPServerRemoveDirectory: ' + VDirectory);
  ThisPath := Directory.GetPrivatePath(ASender, VDirectory, ReadOnly);
  if not ReadOnly then begin
    if not RemoveDir(PChar(ThisPath)) then
      ASender.Connection.IOHandler.WriteLn('553 Cannot delete, directory not empty') ;
  end else begin
    ASender.Connection.IOHandler.WriteLn('550 File is read only');
  end;
end;

procedure TFTPServer.IdFTPServerRenameFile(ASender: TIdFTPServerContext; const ARenameFromFile,ARenameToFile: string);
var FromPath, ToPath: string;
    ReadOnly: Boolean;
begin
  FromPath:= Directory.GetPrivatePath(ASender, ARenameFromFile, ReadOnly);
  ToPath:= Directory.GetPrivatePath(ASender, ARenameToFile, Dummy);
  if not ReadOnly then begin
    if RenameFile(FromPath, ToPath) then
      ASender.Connection.IOHandler.WriteLn('226 Rename successful')
    else
      ASender.Connection.IOHandler.WriteLn('550 File could not be renamed');

  end else begin
    ASender.Connection.IOHandler.WriteLn('550 File is read only');
  end;
end;

procedure TFTPServer.IdFTPServerStoreFile(ASender: TIdFTPServerContext; const AFileName: string;
    AAppend: Boolean; var VStream: TStream);
var ThisPath: string;
    ReadOnly: Boolean;
begin
  ThisPath:= Directory.GetPrivatePath(ASender, AFileName, ReadOnly);
  if not ReadOnly then begin
    VStream := TFileStream.Create(ThisPath, fmCreate)
  end else begin
    VStream := TMemoryStream.Create;
    ASender.Connection.IOHandler.WriteLn('550 Read only');
  end;
end;

procedure TFTPServer.IdFTPChangeDirectory(ASender: TIdFTPServerContext; var VDirectory: string);
begin
  //ASender.CurrentDir:= VDirectory;
end;

{ TDirectoryList }

function TDirectoryList.GetPrivatePath(ASender: TIdFTPServerContext; const APath: string; var AReadOnly: Boolean): string;
var Tmp: string;
    ftpd: TFTPDirectory;
    I: Integer;
begin

  AReadOnly:= True;

  Tmp := APath;
  if Tmp[1] = '/' then begin
    Delete(Tmp, 1, 1);
  end else begin
    Tmp := ASender.CurrentDir + Tmp;
    Delete(Tmp, 1, 1);
  end;

  for I := 0 to List.Count - 1 do begin
    ftpd:= TFTPDirectory(List[I]);
    if CompareText(Copy(Tmp, 1, Length(ftpd.PublicPath)), ftpd.PublicPath) = 0 then begin
      System.Delete(Tmp, 1, Length(ftpd.PublicPath));
      AReadOnly:= (Tmp = '') or ftpd.ReadOnly or FTP.ReadOnly;
      TranslateChar(Tmp, '/', '\');
      Result := ftpd.LocalPath + Tmp;
      Exit;
    end;
  end;
  Result:= '/';
end;

constructor TDirectoryList.Create(AFTP: TFTPServer);
begin
  FTP:= AFTP;
  List:= TList.Create;
end;

// What is the purpose
// Given a path make sure it exists
function TDirectoryList.CheckPublicPath(const APath, Default: string): string;
var Tmp: string;
    ftpd: TFTPDirectory;
    I: Integer;
begin
  Tmp := LowerCase(APath);
  Result:= Default;
  if (Tmp = '') or (Tmp = '/') then begin
    Result := '/';
    Exit;
  end;


  if Tmp[Length(Tmp)] = '/' then
    Delete(Tmp, Length(Tmp), 1);

  if Tmp[1] = '/' then
    Delete(Tmp, 1, 1);

  for I := 0 to List.Count - 1 do begin
    ftpd:= TFTPDirectory(List[I]);
    if CompareText(Copy(Tmp, 1, Length(ftpd.PublicPath)), ftpd.PublicPath) = 0 then begin
      System.Delete(Tmp, 1, Length(ftpd.PublicPath));
      TranslateChar(Tmp, '/', '\');
      Tmp := ftpd.LocalPath + Tmp;
      if DirectoryExists(Tmp) then
        Result:= APath
      else
        Result:= Default;
      Exit;
    end;
  end;
  Result:= Default;
end;

function TDirectoryList.GetItem(Index: Integer): TFTPDirectory;
begin
  Result := TFTPDirectory(List[Index]);
end;

function TDirectoryList.Count: Integer;
begin
  Result:= List.Count;
end;


procedure TFTPServer.IdFTPServerConnect(AContext: TIdContext);
begin
  EventLog('Connected');
end;

{
procedure TFTPServer.IdFTPServerNoCommandHandler(ASender: TIdTCPServer;
      const AData: String; AContext:TIdContext);
begin
  //EventLog('No command handler: ' + AData, llExceptional);
  //AThread.Connection.WriteLn('229 Network protocol not supported, use (1,2)');
end;
 }
procedure TFTPServer.IdFTPServerStatus(ASender: TObject;
  const AStatus: TIdStatus; const AStatusText: String);
begin
  EventLog('What is this status thing: ' + AStatusText, llExceptional)
end;

procedure TFTPServer.IdFTPServerDisconnect(AContext: TIdContext);
begin
  EventLog('Goodbye')
end;

procedure TFTPServer.IdFTPServerAfterCommandHandler(ASender: TIdCmdTCPServer;
    AContext: TIdContext);
begin
  EventLog('IdFTPServerAfterCommandHandler', llVerbose);
end;

procedure TFTPServer.IdFTPServerAfterUserLogin(ASender: TIdFTPServerContext);
begin
  EventLog('IdFTPServerAfterUserLogin', llVerbose);
end;

procedure TFTPServer.IdFTPServerBeforeCommandHandler(ASender: TIdCmdTCPServer;
    var AData: string; AContext: TIdContext);
begin
  EventLog('IdFTPServerBeforeCommandHandler: ' + AData, llVerbose);
end;

procedure TFTPServer.IdFTPServerException(AContext: TIdContext; AException: Exception);
begin
  EventLog('IdFTPServerException: ' + AException.Message, llMinimum);
end;

procedure TFTPServer.IdFTPServerExecute(AContext: TIdContext);
begin
  EventLog('IdFTPServerExecute', llExceptional);
end;

{
procedure TFTPServer.IdFTPServerGetCustomListFormat(ASender: TIdFTPServer;
  AItem: TIdFTPListItem; var VText: String);
begin
  EventLog('IdFTPServerGetCustomListFormat', llExceptional);
end;
}

procedure TFTPServer.IdFTPServerListenException(AThread: TIdListenerThread;
  AException: Exception);
begin
  EventLog('IdFTPServerListenException: ' + AException.Message);
end;


constructor TFTPServer.Create(AOwner: TComponent);
var Hndl: TIdSocketHandle;
begin
  Directory:= TDirectoryList.Create(Self);
  IdFTPServer := TIdFTPServer.Create(Owner);
  IdFTPServer.AllowAnonymousLogin:= True;
  IdFTPServer.OnStatus:= IdFTPServerStatus;
  IdFTPServer.DefaultPort := 21;


  IdFTPServer.Greeting.NumericCode := 220;
  IdFTPServer.Greeting.Text.Text :=
      '******************************' + #$0a +
      'Acnc32 FTP ready'               + #$0a +
      '                          '     + #$0a +
      'Please take care when'          + #$0a +
      'modifying files on a live'      + #$0a +
      'system'                         + #$0a +
      '******************************';

  IdFTPServer.Greeting.Code := '220';


  IdFTPServer.MaxConnectionReply.NumericCode := 0;
  IdFTPServer.OnAfterCommandHandler := IdFTPServerAfterCommandHandler;
  IdFTPServer.OnBeforeCommandHandler := IdFTPServerBeforeCommandHandler;
  IdFTPServer.OnConnect := IdFTPServerConnect;
  IdFTPServer.OnExecute := IdFTPServerExecute ;
  IdFTPServer.OnDisconnect := IdFTPServerDisconnect;
  IdFTPServer.OnException := IdFTPServerException;
  IdFTPServer.OnListenException := IdFTPServerListenException;
  //IdFTPServer.OnNoCommandHandler := IdFTPServerNoCommandHandler;
  IdFTPServer.OnAfterUserLogin := IdFTPServerAfterUserLogin;
  IdFTPServer.OnChangeDirectory := IdFTPServerChangeDirectory;
  //IdFTPServer.OnGetCustomListFormat := IdFTPServerGetCustomListFormat;
  IdFTPServer.OnGetFileSize := IdFTPServerGetFileSize;
  IdFTPServer.OnUserLogin := IdFTPServerUserLogin;
  IdFTPServer.OnListDirectory := IdFTPServerListDirectory;
  IdFTPServer.OnRenameFile := IdFTPServerRenameFile;
  IdFTPServer.OnDeleteFile := IdFTPServerDeleteFile;
  IdFTPServer.OnRetrieveFile := IdFTPServerRetrieveFile;
  IdFTPServer.OnStoreFile := IdFTPServerStoreFile;
  IdFTPServer.OnMakeDirectory := IdFTPServerMakeDirectory;
  IdFTPServer.OnRemoveDirectory := IdFTPServerRemoveDirectory;
  IdFTPServer.OnChangeDirectory:= IdFTPChangeDirectory;

  Hndl:= IdFTPServer.Bindings.Add;
  Hndl.IP:= '0.0.0.0';
  Hndl.Port:= 21;

  //IdFTPServer.ReplyExceptionCode := 0;
  IdFTPServer.ReplyUnknownCommand.NumericCode := 500;
  IdFTPServer.ReplyUnknownCommand.Text.Text:= 'Syntax error, command unrecognized.';
  IdFTPServer.ReplyUnknownCommand.Code:= '500';
  IdFTPServer.AllowAnonymousLogin:= True;
  //IdFTPServer.EmulateSystem := ftpsUNIX;
  IdFTPServer.SystemType:= 'WIN32';
  //IdFTPServer.HelpReply.Text:= 'Only supports etc.';
  IdFTPServer.AnonymousAccounts.Text:=
      'anonymous' + #$0a + #$0d +
      'ftp' + #$0a + #$0d +
      'guest';
//  IdFTPServer.CommandHandlers = <>
//  IdFTPServer.ReplyTexts = <>
end;

procedure TFTPServer.Open;
begin
  IdFTPServer.Active := True
end;

procedure TFTPServer.Close;
begin
  IdFTPServer.Active := False;
end;

procedure TFTPServer.AddPath(APath: TFTPDirectory);
begin
  Directory.List.Add(APath);
end;

procedure TFTPServer.EventLog(const Text: string; Level: TLoggingLevel = lluser);
begin
  if Assigned(OnLogging) and (Level in LoggingLevels) then
    OnLogging(Self, Text);
end;


end.
