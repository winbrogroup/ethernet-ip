unit FTP;

interface

uses Classes, CNCTypes, CNC32, CoreCNC, FTPServer;

type
  TFTP = class(TAbstractDisplay)
  private
    FTPServer: TFTPServer;
  public
    procedure InstallComponent(Params: TParamStrings); override;
    procedure IShutdown; override;
  end;

implementation


procedure TFTP.InstallComponent(Params: TParamStrings);
var ftpd: TFTPDirectory;
begin
  inherited;
  try
    FTPServer := TFTPServer.Create(Self);
  except
    Exit;
  end;
  //FTPServer.OnLogging:= FTPLog;
  //FTPServer.LoggingLevels := [llExceptional];

  ftpd:= TFTPDirectory.Create;
  ftpd.LocalPath:= LocalPath + '\audit\h1\';
  ftpd.PublicPath:= 'signatures';
  ftpd.ReadOnly:= True;
  FTPServer.AddPath(ftpd);

  ftpd:= TFTPDirectory.Create;
  ftpd.LocalPath:= LocalPath;
  ftpd.PublicPath:= 'programs';
  ftpd.ReadOnly:= True;
  FTPServer.AddPath(ftpd);

  ftpd:= TFTPDirectory.Create;
  ftpd.LocalPath:= MachSpecPath;
  ftpd.PublicPath:= 'machspec';
  ftpd.ReadOnly:= True;
  FTPServer.AddPath(ftpd);

  ftpd:= TFTPDirectory.Create;
  ftpd.LocalPath:= ProfilePath;
  ftpd.PublicPath:= 'profile';
  ftpd.ReadOnly:= True;
  FTPServer.AddPath(ftpd);

  ftpd:= TFTPDirectory.Create;
  ftpd.LocalPath:= ContractSpecificPath;
  ftpd.PublicPath:= 'contspec';
  ftpd.ReadOnly:= True;
  FTPServer.AddPath(ftpd);

  ftpd:= TFTPDirectory.Create;
  ftpd.LocalPath:= 'c:\compdata';
  ftpd.PublicPath:= 'compdata';
  ftpd.ReadOnly:= True;
  FTPServer.AddPath(ftpd);



  FTPServer.Open;
end;

procedure TFTP.IShutdown;
begin
  inherited;
  if Assigned(FTPServer) then begin
    FTPServer.Close;
  end;
end;

initialization
  CoreCNC.RegisterCNCClass(TFTP);
end.
