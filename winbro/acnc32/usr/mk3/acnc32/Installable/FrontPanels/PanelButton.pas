unit PanelButton;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, extctrls, CNC32, SyncObjs;

type
  TAbstractPanelButtonClass = class of TAbstractPanelButton;

  TAbstractPanelButton = class;

  TPanelButtonPressList = class(TObject)
  private
    List : TList;
    Timer : TTimer;
    Lock: TCriticalSection;
    procedure UpdateButtonPress(Sender : TObject);
  public
    constructor Create;
    procedure Add(Button : TAbstractPanelButton);
    procedure Remove(Button : TAbstractPanelButton);
  end;

  TAbstractPanelButton = class(TCustomPanel)
  private
  protected
    TimeOfPress : TDateTime;
    Legend1                : TLabel;
    Legend2                : TLabel;
    FLegend                : String;

    FOnLightColor          : TColor;
    FOffLightColor         : TColor;
    FStatus                : Integer;
    FPermissive            : Boolean;
    FLatched               : Boolean;
    FDown                  : Boolean;
    FOnPress               : TNotifyEvent;
    FOnRelease             : TNotifyEvent;
    FErrorState : Boolean;
    FHTTPHelp : string;
    FKeyword : string;
    FImages: TImageList;
    FUseImages: Boolean;
    procedure SetOnColor(Colour : TColor); virtual;
    procedure SetOffColor(Colour : TColor); virtual;
    procedure SetStatus(Stat : Integer); virtual; abstract;
    procedure SetPermissive(aPermissive : Boolean); virtual; abstract;
    procedure SetLegend(Text : String);
    procedure SetLatched(aLatched : Boolean); virtual;
    procedure MyMouseUp(Sender: TObject; Button: TMouseButton;
              Shift: TShiftState; X, Y: Integer);
    procedure MyMouseDown(Sender: TObject; Button: TMouseButton;
              Shift: TShiftState; X, Y: Integer);
    procedure SetDown(aDown : Boolean); virtual; abstract;
    procedure SetErrorState(aState : Boolean); virtual;
    procedure SetUseImages(const Value: Boolean); virtual;
  public
    LightTag, ButtonTag : TAbstractTag;
    LightMask : Integer;
    ButtonMask : Integer;
    Ordinate : TPoint;
    constructor Create(aOwner : TComponent); override;
    class function UnitWidth : Integer; virtual; abstract;
    class function UnitHeight : Integer; virtual; abstract;
    property Legend        : String    read FLegend         write SetLegend;
    property Permissive    : Boolean   read FPermissive     write SetPermissive;

    property OnLightColor  : TColor    read FOnLightColor   write SetOnColor;
    property OffLightColor : TColor    read FOffLightColor  write SetOffColor;
    property Status        : Integer   read FStatus         write SetStatus;
    property Latched       : Boolean   read FLatched        write SetLatched;
    property Down          : Boolean   read FDown           write SetDown;
    property OnPress  : TNotifyEvent read FOnPress write FOnPress;
    property OnRelease : TNotifyEvent read FonRelease write FOnRelease;
    property OnResize;
    property Font;
    property ParentFont;
    property HTTPHelp : string read FHTTPHelp write FHTTPHelp;
    property ErrorState : Boolean read FErrorState write SetErrorState;
    property Keyword : string read FKeyword write FKeyword;
    property Images: TImageList read FImages;
    property UseImages: Boolean read FUseImages write SetUseImages;
  end;

  TRoundButton = class(TAbstractPanelButton)
  private
    RButton                : TShape;
    Ring                   : TShape;
    Highlight              : TShape;
  protected
    procedure SetStatus(Stat : Integer); override;
    procedure SetPermissive(aPermissive : Boolean); override;
    procedure SetDown(aDown : Boolean); override;
    procedure SetErrorState(aState : Boolean); override;
  public
    constructor Create(AOwner:TComponent); override;
    destructor Destroy; override;
    procedure   Resize; override;
    class function UnitWidth : Integer; override;
    class function UnitHeight : Integer; override;
  end;

  THylandButton = class(TAbstractPanelButton)
  private
    FTopBorderWidth        : Integer;
    FBorderColor           : TColor;
    FSideBorderWidth       : Integer;
    Light                  : TPanel;
    LatchedShape           : TShape;
    procedure SetTopBorder(Size : Integer);
    procedure SetSideBorder(Size : Integer);
  protected
    procedure SetStatus(Stat : Integer); override;
    procedure SetDown(aDown : Boolean); override;
    procedure SetPermissive(aPermissive : Boolean); override;
    procedure SetLatched(aLatched : Boolean); override;
  public
    constructor Create(AOwner:TComponent); override;
    destructor Destroy; override;
    procedure   Resize; override;
    class function UnitWidth : Integer; override;
    class function UnitHeight : Integer; override;
    property TopBorderWidth   : Integer   read FTopBorderWidth    write SetTopBorder;
    property SideBorderWidth   : Integer   read FSideBorderWidth    write SetSideBorder;
  end;

  TSoftButton = class(TAbstractPanelButton)
  private
    Image: TImage;
  protected
    procedure SetStatus(aStat : Integer); override;
    procedure SetPermissive(aPermissive : Boolean); override;
    procedure SetDown(aDown : Boolean); override;

    procedure Resize; override;
    procedure SetOnColor(Colour : TColor); override;
    procedure SetOffColor(Colour : TColor); override;
    procedure SetErrorState(aState : Boolean); override;
    procedure SetUseImages(const Value: Boolean); override;
  public
    constructor Create(Aowner : TComponent); override;
    class function UnitWidth : Integer; override;
    class function UnitHeight : Integer; override;
  published
  end;

implementation

var
  PressList : TPanelButtonPressList;

constructor TAbstractPanelButton.Create(aOwner : TComponent);
begin
  inherited;

  Legend1 := TLabel.Create(Self);
  with Legend1 do begin
    Parent := Self;
    ParentFont := True;
    alignment := taCenter;
  end;

  Legend2 := TLabel.Create(Self);
  with Legend2 do begin
    Parent := Self;
    ParentFont := True;
    alignment := taCenter;
  end;
end;

procedure TAbstractPanelButton.SetLegend(Text : String);
var SidPos   : Integer;
begin
{  if not assigned (Legend1) or not assigned (Legend2) then
    Exit; }
  SidPos := Pos('~',Text);
  if sidPos > 0 then begin
    Legend1.Caption := Copy(Text,1,SidPos-1);
    Legend2.Caption := Copy(Text,SidPos+1,Length(Text)-SidPOs);
    Legend1.Top := Height div 4;
    Legend2.Top := Height div 4 *3;
    Legend2.visible := True;
  end else begin
    Legend1.Caption := Text;
    Legend2.Caption := '';
    Legend2.visible := False;
    Legend2.Top :=-10;
    Legend2.Left :=-10;
    Legend1.Top := Height div 2;
  end;
  FLegend := Text;
  Resize;
end;

procedure TAbstractPanelButton.SetLatched(aLatched : Boolean);
begin
  FLatched := aLatched;
end;

procedure TAbstractPanelButton.SetOnColor(Colour : TColor);
begin
  FOnLightColor  := Colour;
  SetStatus(FStatus);
end;

procedure TAbstractPanelButton.SetOffColor(Colour : TColor);
begin
  FOffLightColor  := Colour;
  SetStatus(FStatus);
end;

procedure TAbstractPanelButton.SetUseImages(const Value: Boolean);
begin
  FUseImages := Value;
  if not Assigned(FImages) and Value then begin
    FImages:= TImageList.Create(Self);
    FImages.Height:= Height;
    FImages.Width:= Width;
  end;
end;

class function TRoundButton.UnitWidth : Integer;
begin
  Result := 70;
end;

class function TRoundButton.UnitHeight : Integer;
begin
  Result := 100;
end;

constructor TRoundButton.Create(AOwner:TComponent);
begin
  inherited Create(AOwner);
  FPermissive := True;
  ParentColor := False;
  Color := clBlack;
  ParentColor := False;
  Caption := '';
  Height := UnitHeight;
  Width := UnitWidth;
  Ring := TShape.Create(Self);
  Ring.Parent := Self;
  Ring.Shape := stCircle;
  Ring.Brush.Color := clGray;

  RButton := TShape.Create(Self);
  RButton.Parent := Self;
  RButton.Shape := stCircle;
  RButton.Brush.Color := FOffLightColor;

  Highlight := TShape.Create(Self);
  Highlight.Parent := Self;
  Highlight.Shape := stCircle;
  Highlight.Brush.Color := clWhite;
  Highlight.Pen.Color := clWhite;


  RButton.OnMouseDown := MyMouseDown;
  RButton.OnMouseUp   := MYMouseUp;

  FOnlightColor := clYellow;
  FOfflightColor := clWhite;
  Font.Color := clwhite;

  FStatus := 0;
  Font.Size := 8;
  Font.Style := [fsbold];
  Font.Name := 'Arial';
  SetLegend('Line1~Line2');

  Legend1.Font.Color := clWhite;
  Legend2.Font.Color := clWhite;

  Resize;
end;

destructor TRoundButton.Destroy;
begin
  inherited Destroy;
end;


procedure  TRoundButton.Resize;
begin
  inherited Resize;
  Ring.Width := Self.Width-2;
  Ring.Height := Self.Width-2;
  Ring.Top := 1;
  Ring.Left := 1;

  RButton.Width := Self.Width-16;
  RButton.Height := Self.Width-16;
  RButton.Top := 8;
  RButton.Left := 8;

  Highlight.Width := RButton.Width div 6;
  Highlight.Height := RButton.Width div 6;
  Highlight.Top := 20;
  Highlight.Left := 20;

  Legend1.Left := 1;
  Legend2.Left := 1;
  Legend1.Width := Width-2;
  Legend2.Width := Width-2;
  if Legend2.Caption = '' then  begin
    Legend1.Top := Width;
    Legend2.Visible := False;
  end else begin
    Legend1.Top := Width;
    Legend2.Top := Width+Legend1.Height;
    Legend2.Visible := True;
  end;
   // ensure colour is painted at power-up/ resize
   Fstatus := not FStatus;
   SetStatus(not Fstatus);
end;


procedure THylandbutton.SetPermissive(aPermissive : Boolean);
begin
  FPermissive := aPermissive;
  if not Permissive then begin
    Legend1.Font.Color := clGray;
    Legend2.Font.Color := clGray;
  end else begin
    Legend1.Font.Color := clBlack;
    Legend2.Font.Color := clBlack;
  end;
end;

procedure THylandbutton.SetLatched(aLatched : Boolean);
begin
  inherited;
  LatchedShape.Visible := Latched;
end;

procedure TRoundButton.SetPermissive(aPermissive : Boolean);
begin
  FPermissive := aPermissive;
  if aPermissive then begin
    if FStatus <> 0 then begin
      RButton.Brush.Color := FOnLightColor;
      Highlight.Visible := True;
      Ring.Brush.Color := clSilver;
    end else begin
      RButton.Brush.Color := FOffLightColor;
      Highlight.Visible := False;
      Ring.Brush.Color := clGray;
    end;
  end else begin
    RButton.Brush.Color := clGray;
  end;
  RButton.Invalidate
end;


procedure TRoundButton.SetStatus(Stat : Integer);
begin
  if FStatus <> Stat then begin
    FStatus := Stat;
    if FStatus <> 0 then begin
      RButton.Brush.Color := FOnLightColor;
      Highlight.Visible := True;
      Ring.Brush.Color := clSilver;
    end else begin
      RButton.Brush.Color := FOffLightColor;
      Highlight.Visible := False;
      Ring.Brush.Color := clGray;
    end;
  end;
end;

class function THylandButton.UnitWidth : Integer;
begin
  Result := 60;
end;

class function THylandButton.UnitHeight : Integer;
begin
  Result := 35;
end;

constructor THylandButton.Create(AOwner:TComponent);
begin
  inherited Create(AOwner);
  ParentColor := False;
  Color := clBlack;
  FBorderColor := clBlack;
  ParentColor := False;
  Height := UnitHeight;
  Width := UnitWidth;
  Light := TPanel.Create(Self);
  Light.Parent := Self;
  Light.Color := clWhite;
  Light.BevelOuter:= bvLowered;
//  Light.OnClick := OnLightClick;
  Light.OnMouseDown := MyMouseDown;
  Light.OnMouseUp   := MYMouseUp;

  LatchedShape := TShape.Create(Self);
  LatchedShape.Parent := Light;
  LatchedShape.Shape := stRoundRect;
  LatchedShape.Brush.Color := clYellow;
  LatchedShape.Visible := False;
  LatchedShape.Left := 0;
  LatchedShape.Top := 0;
  LatchedShape.Pen.Width := 2;
  LatchedShape.Brush.Color := clWhite;

  FTopBorderWidth :=1;
  FSideBorderWidth :=2;
  SetTopBorder(2);
  SetSideBorder(4);
  FOnlightColor := clYellow;
  FOfflightColor := clWhite;
  Legend1.OnMouseDown := MyMouseDown;
  Legend1.OnMouseUp := MyMouseUp;

  Legend1.Parent := Light;
  Legend2.OnMouseDown := MyMouseDown;
  Legend2.OnMouseUp := MyMouseUp;
  Legend2.Parent := Light;
  FStatus := 0;
  Font.Size := 8;
  {
  Font.Style := [fsbold];
  Font.Name := 'Arial';
  SetLegend('Line1~Line2');
  }
  Resize;
end;

destructor THylandButton.Destroy;
begin
inherited Destroy;
end;


procedure  THylandButton.Resize;
var
LHeight : Integer;
TopPos  : Integer;
begin
   inherited Resize;
   if assigned(light) then
      begin
      Light.Width := Self.Width-(2*FSideBorderWidth);
      Light.Height := Self.Height -(2*FTopBorderWidth);
      Light.Top := FTopBorderWidth;
      Light.Left := FSideBorderWidth;
      end;
   if not assigned (Legend1) or not assigned (Legend2) then exit;
   Legend1.Left := (Light.Width - Legend1.Width) div 2;
   Legend2.Left := (Light.Width - Legend2.Width) div 2;
   if Legend2.Caption = '' then
      begin
      TopPos := (Light.Height-Legend1.Height) div 2;
      if TopPos < 1 then TopPos := 1;
      Legend1.Top := TopPos;
      end
   else
      begin
      LHeight := (Legend1.Height *2) +1 ;
      TopPos := (Light.Height - LHeight) div 2;
      if TopPos < 1 then TopPos := 1;
      Legend1.Top := TopPos;
      Legend2.Top := Legend1.Top + Legend1.Height +1;
      end;


  LatchedShape.Width := Light.Width div 3;
  LatchedShape.Height := Light.Height div 5;
end;


procedure THylandButton.SetTopBorder(Size : Integer);
begin
if (Size > (Height div 2))  then exit;
FTopBorderWidth := Size;
Resize;
end;

procedure THylandButton.SetSideBorder(Size : Integer);
begin
  if (Size > (Width div 2))  then
    Exit;
  FSideBorderWidth := Size;
  Resize;
end;

procedure THylandButton.SetStatus(Stat : Integer);
begin
  FStatus := Stat;
  if Stat <> 0 then begin
    light.Color := FOnLightColor;
  end else begin
    light.Color := FOffLightColor;
  end;

  if Latched and Down and (Stat <> 0) then
    LatchedShape.Brush.Color := FOffLightColor
  else if Latched and Down and not (Stat = 0) then
    LatchedShape.Brush.Color := FOnLightColor
  else
    LatchedShape.Brush.Color := Light.Color;
end;



procedure THylandButton.SetDown(aDown : Boolean);
begin
  if aDown then begin
    if not Down then begin
      Light.Left := Light.Left+2;
      Light.Top := Light.Top+2;
      Legend1.Left := Legend1.Left+2;
      Legend2.Top := Legend2.Top+2;
      FDown := True;
{      if Latched then begin
        LatchedShape.Visible := True;
      end; }
    end;
  end else begin
    if Down then begin
      Light.Left := Light.Left-2;
      Light.Top := Light.Top-2;
      Legend1.Left := Legend1.Left-2;
      Legend2.Top := Legend2.Top-2;
      FDown := False;
{      if Latched then begin
        LatchedShape.Visible := False;
      end; }
    end;
  end;
  Status := Status;
end;

procedure TRoundButton.SetDown(aDown : Boolean);
begin
  if aDown and not Down then begin
    RButton.Width := RButton.Width-2;
    RButton.Left := RButton.Left+1;
    FDown := aDown;
  end else if not aDown and Down then begin
    RButton.Width := RButton.Width+2;
    RButton.Left := RButton.Left-1;
    FDown := aDown;
  end
end;

procedure TRoundButton.SetErrorState(aState : Boolean);
begin
  FErrorState := aState;
end;

procedure TAbstractPanelButton.SetErrorState(aState : Boolean);
begin
  FErrorState := aState;
  if ErrorState then begin
    Self.Legend1.Font.Color := clWhite;
    Self.Legend2.Font.Color := clWhite;
  end else begin
    Self.Legend1.Font.Color := clBlack;
    Self.Legend2.Font.Color := clBlack;
  end;
end;

procedure TAbstractPanelButton.MyMouseDown(Sender: TObject; Button: TMouseButton;
Shift: TShiftState; X, Y: Integer);
begin
  if Permissive then begin
    PressList.Add(Self);
    if Latched then begin
      if Down then begin
        SetDown(False);
        if Assigned(FOnRelease) then
          FOnRelease(Self);
      end else begin
        SetDown(True);
        if Assigned(FOnPress) then
          FOnPress(Self);
      end;
    end else begin
      SetDown(True);
      if Assigned(FOnPress) then
        FOnPress(Self);
    end;
  end;
end;

procedure TAbstractPanelButton.MyMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if Permissive then begin
    PressList.Remove(Self);
    if not Latched then begin
      SetDown(False);
      if Assigned(FOnRelease) then
        FOnRelease(Self);
    end;
  end;
end;


constructor TSoftButton.Create(Aowner : TComponent);
begin
  inherited create(Aowner);
  Legend1.OnMouseDown := MyMouseDown;
  Legend1.OnMouseUp := MyMouseUp;
  Legend2.OnMouseDown := MyMouseDown;
  Legend2.OnMouseUp := MyMouseUp;
  OnMouseDown := MyMouseDown;
  OnMouseUp := MyMouseUp;
  Width := UnitWidth;
  Height := UnitHeight;
end;

procedure TSoftButton.SetDown(aDown : Boolean);
begin
  if Down <> aDown then begin
    FDown := aDown;
    if FDown then begin
      BevelOuter := bvLowered;
      Legend1.Left := Legend1.Left + 1;
      Legend2.Left := Legend2.Left + 1;
    end else begin
      BevelOuter := bvRaised;
      Legend1.Left := Legend1.Left - 1;
      Legend2.Left := Legend2.Left - 1;
    end;
    Repaint;
  end;
end;

procedure TSoftButton.SetErrorState(aState : Boolean);
begin
  FErrorState := aState;
  if ErrorState then begin
    Self.Legend1.Font.Color := clWhite;
    Self.Legend2.Font.Color := clWhite;
  end else begin
    if Status <> 0 then begin
      Self.Legend1.Font.Color := OnLightColor;
      Self.Legend2.Font.Color := OnLightColor;
    end else begin
      Self.Legend1.Font.Color := OffLightColor;
      Self.Legend2.Font.Color := OffLightColor;
    end;
  end;
end;


procedure TSoftButton.Resize;
begin
  inherited Resize;

  Legend1.Left := (Width div 2) - (Legend1.width div 2);
  Legend1.Top  :=  (Height div 2) - Legend1.Height;
  Legend1.Font.Height := -Round(Height / 3);

  Legend2.Left := (Width div 2) - (Legend2.width div 2);
  Legend2.Top  :=  (Height div 2);
  Legend2.Font.Height := -Round(Height / 3);
end;

procedure TSoftButton.SetStatus(aStat : Integer);
var Tmp : TColor;
    T: Integer;
begin
  if not UseImages then begin
    if (aStat = FStatus) then
      Exit;

    FStatus := aStat;

    if FStatus <> 0 then  Tmp := OnLightColor
                    else Tmp := OffLightColor;

    Legend1.Font.Color := Tmp;
    Legend2.Font.Color := Tmp;
  end else begin
    //BM:= TBitmap.Create;
    FStatus := aStat;
    T:= FStatus mod FImages.Count;
    FImages.GetBitmap(T, Image.Picture.Bitmap);
    Repaint;
    //Image.Picture.Bitmap:= FImages[I];
  end;
end;

procedure TSoftButton.SetPermissive(aPermissive : Boolean);
begin
  FPermissive := aPermissive;
  if not Permissive then begin
    Legend1.Font.Color := clGray;
    Legend2.Font.Color := clGray;
  end else begin
    FStatus := not FStatus;
    Status := not Status;
  end;
end;

class function TSoftButton.UnitWidth : Integer;
begin
  Result := 70;
end;

class function TSoftButton.UnitHeight : Integer;
begin
  Result := 40;
end;

procedure TSoftButton.SetOnColor(Colour : TColor);
begin
  inherited;
  FStatus := not FStatus;
  Status := not Status;
end;

procedure TSoftButton.SetOffColor(Colour : TColor);
begin
  inherited;
  FStatus := not FStatus;
  Status := not Status;
end;

procedure TSoftButton.SetUseImages(const Value: Boolean);
begin
  inherited;
  FUseImages:= Value;
  if UseImages then begin
    Image:= TImage.Create(Self);
    Image.Align:= alClient;
    Image.Parent:= Self;
    Image.OnMouseDown:= Self.MyMouseDown;
    Image.OnMouseUp:= Self.MyMouseUp;
    Legend1.Visible:= False;
    Legend2.Visible:= False;
  end;
end;



{ TPanelButtonPressList }

procedure TPanelButtonPressList.Add(Button: TAbstractPanelButton);
begin
  Lock.Enter;
  try
    if List.IndexOf(Button) = -1 then begin
      List.Add(Button);
      Button.TimeOfPress := Now;
    end;
  finally
    Lock.Leave;
  end;
end;

constructor TPanelButtonPressList.Create;
begin
  Lock:= TCriticalSection.Create;
  List := TList.Create;
  Timer := TTimer.Create(Application);
  Timer.Interval := 250;
  Timer.OnTimer := UpdateButtonPress;
end;

procedure TPanelButtonPressList.Remove(Button: TAbstractPanelButton);
var I : Integer;
begin
  Lock.Enter;
  try
    I := List.IndexOf(Button);
    Button.ErrorState := False;
    if I <> -1 then
      List.Delete(I);
  finally
    Lock.Leave;
  end;
end;

procedure TPanelButtonPressList.UpdateButtonPress(Sender: TObject);
var I : Integer;
    B : TAbstractPanelButton;
    T : TDateTime;
const TIMEOUT = 1 / (24 * 60 * 60);
begin
  Lock.Enter;
  try
    T := Now;
    for I := List.Count - 1 downto 0 do begin
      B := TAbstractPanelButton(List[I]);
      if T - B.TimeOfPress > TIMEOUT then begin
        if B.Down then begin
          B.SetDown(False);
          if Assigned(B.FOnRelease) then
            B.FOnRelease(B);
        end else begin
          B.ErrorState := not B.ErrorState;
        end;
        if B.ErrorState and (T - B.TimeOfPress > (TIMEOUT * 4)) then begin
          B.ErrorState := False;
          List.Delete(I);
        end;
      end;
    end;
  finally
    Lock.Leave;
  end;
end;


initialization
  PressList := TPanelButtonPressList.Create;
end.
