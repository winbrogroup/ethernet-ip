object PanelPluginEditorForm: TPanelPluginEditorForm
  Left = 772
  Top = 522
  Width = 359
  Height = 301
  Caption = 'Plugin Widget Editor'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object DLLListBox: TListBox
    Left = 0
    Top = 0
    Width = 351
    Height = 97
    Align = alTop
    ItemHeight = 13
    TabOrder = 0
    OnClick = DLLListBoxClick
  end
  object GroupBox1: TGroupBox
    Left = 209
    Top = 97
    Width = 142
    Height = 158
    Align = alClient
    Caption = 'Position'
    TabOrder = 1
    object Label1: TLabel
      Left = 8
      Top = 24
      Width = 18
      Height = 13
      Caption = 'Left'
    end
    object Label2: TLabel
      Left = 8
      Top = 56
      Width = 19
      Height = 13
      Caption = 'Top'
    end
    object Label3: TLabel
      Left = 8
      Top = 88
      Width = 28
      Height = 13
      Caption = 'Width'
    end
    object Label4: TLabel
      Left = 8
      Top = 120
      Width = 31
      Height = 13
      Caption = 'Height'
    end
    object LeftEdit: TEdit
      Left = 56
      Top = 20
      Width = 60
      Height = 21
      TabOrder = 0
      Text = '0'
    end
    object TopEdit: TEdit
      Left = 56
      Top = 52
      Width = 60
      Height = 21
      TabOrder = 1
      Text = '0'
    end
    object WidthEdit: TEdit
      Left = 56
      Top = 84
      Width = 60
      Height = 21
      TabOrder = 2
      Text = '0'
    end
    object HeightEdit: TEdit
      Left = 56
      Top = 116
      Width = 60
      Height = 21
      TabOrder = 3
      Text = '0'
    end
    object UDLeft: TUpDown
      Left = 116
      Top = 20
      Width = 15
      Height = 21
      Associate = LeftEdit
      Min = 0
      Max = 3200
      Position = 0
      TabOrder = 4
      Wrap = False
      OnClick = UDClick
    end
    object UDTop: TUpDown
      Left = 116
      Top = 52
      Width = 15
      Height = 21
      Associate = TopEdit
      Min = 0
      Max = 3200
      Position = 0
      TabOrder = 5
      Wrap = False
      OnClick = UDClick
    end
    object UDWidth: TUpDown
      Left = 116
      Top = 84
      Width = 15
      Height = 21
      Associate = WidthEdit
      Min = 0
      Max = 3200
      Position = 0
      TabOrder = 6
      Wrap = False
      OnClick = UDClick
    end
    object UDHeight: TUpDown
      Left = 116
      Top = 116
      Width = 15
      Height = 21
      Associate = HeightEdit
      Min = 0
      Max = 3200
      Position = 0
      TabOrder = 7
      Wrap = False
      OnClick = UDClick
    end
  end
  object GroupBox2: TGroupBox
    Left = 0
    Top = 97
    Width = 209
    Height = 158
    Align = alLeft
    Caption = 'Property Editor'
    TabOrder = 2
    object Edit2: TEdit
      Left = 16
      Top = 60
      Width = 161
      Height = 21
      TabOrder = 0
    end
    object Button1: TButton
      Left = 24
      Top = 112
      Width = 75
      Height = 25
      Caption = 'Send'
      TabOrder = 1
      OnClick = Button1Click
    end
    object PropertyComboBox: TComboBox
      Left = 16
      Top = 24
      Width = 169
      Height = 21
      ItemHeight = 13
      TabOrder = 2
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 255
    Width = 351
    Height = 19
    Panels = <>
    SimplePanel = False
  end
end
