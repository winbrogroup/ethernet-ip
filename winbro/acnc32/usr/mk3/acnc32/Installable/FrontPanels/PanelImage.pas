unit PanelImage;

interface

uses SysUtils, Classes, CNCTypes, CNC32, CoreCNC, ImgList, ExtCtrls, Controls,
     StreamTokenizer, Graphics;

type
  TPanelImage = class(TCustomPanel)
  private
    ImageList: TImageList;
    IndexTag: TAbstractTag;
    BitMap: TBitmap;
    BG: TBitMap;
    FTagOwner: TCNC;
    Transparent: Boolean;
    procedure UpdateIndex(ATag: TAbstractTag);
  public
    constructor Create(AOwner: TComponent); override;
    procedure Read(S: TStreamTokenizer);
    procedure Paint; override;
    procedure SetImage;
    procedure Resize; override;
    property TagOwner: TCNC read FTagOwner write FTagOwner;
    property Color;
  end;

implementation

{ TPanelImage }

constructor TPanelImage.Create(AOwner: TComponent);
begin
  inherited;
  ImageList:= TImageList.Create(Self);
  Bitmap:= TBitMap.Create;
  BG:= TBitMap.Create;
end;

procedure TPanelImage.Paint;
begin
  //if Transparent then begin
    BG.Canvas.Brush.Color:= Color;
    BG.Canvas.FillRect(Self.GetClientRect);
    BG.Canvas.Draw(0, 0, Bitmap);
    Canvas.Draw(0, 0, BG);
  //end else
    //Canvas.Draw(0, 0, BitMap);
end;

procedure TPanelImage.Read(S: TStreamTokenizer);

  function LoadImage(const fname: string): TBitmap;
  begin
    Result:= TBitmap.Create;
    Result.Transparent:= Transparent;
    Result.TransparentMode:= tmAuto;
    Result.LoadFromFile(ProfilePath + 'live/images/' + fname);
  end;

  procedure LocalHeight(H: Integer);
  begin
    Height:= H;
    ImageList.Height:= H;
  end;

  procedure LocalWidth(W: Integer);
  begin
    Width:= W;
    ImageList.Width:= W;
  end;

  function AddTag(ro : Boolean) : TAbstractTag;
  var Tmp : string;
  begin
    S.ExpectedToken('=');
    Tmp := S.Read;
    Result := TagOwner.TagEvent(Tmp, EdgeChange, TIntegerTag, UpdateIndex)
  end;

var Token: string;
begin
  Token:= LowerCase(S.Read);
  while not S.EndOfStream do begin
    if Token = 'image' then
      ImageList.Add(LoadImage(S.ReadStringAssign), nil)
    else if Token = 'left' then
      Left:= S.ReadIntegerAssign
    else if Token = 'top' then
      Top:= S.ReadIntegerAssign
    else if Token = 'width' then
      LocalWidth(S.ReadIntegerAssign)
    else if Token = 'height' then
      LocalHeight(S.ReadIntegerAssign)
    else if Token = 'light' then
      IndexTag:= AddTag(True)
    else if Token = 'imagewidth' then
      ImageList.Width:= S.ReadIntegerAssign
    else if Token = 'transparent' then
      Transparent:= S.ReadBooleanAssign
    else if Token = 'color' then
      Color:= ReadColor(S)
    else if Token = '}' then
      Exit
    else
      raise ECNCInstallFault.CreateFmt('Unexpected token %s', [Token]);
    Token := LowerCase(S.Read);
  end;
  raise ECNCInstallFault.CreateFmt('Unexpected end of file %s', [Name]);
end;

procedure TPanelImage.Resize;
begin
  inherited;
  if Assigned(BG) then begin
    BG.Width:= Width;
    BG.Height:= Height;
  end;
end;

procedure TPanelImage.SetImage;
begin
  ImageList.GetBitmap(0, Bitmap);
  Bitmap.Transparent:= Transparent;
  BitMap.TransparentColor:= 0;
end;

procedure TPanelImage.UpdateIndex(ATag: TAbstractTag);
var Tmp: Integer;
begin
  if ImageList.Count > 0 then begin
    Tmp:= ATag.AsInteger mod ImageList.Count;
    ImageList.GetBitmap(Tmp, Bitmap);
    Bitmap.Transparent:= False;
    Bitmap.Transparent:= Transparent;
  end;

  Invalidate;
end;

end.
