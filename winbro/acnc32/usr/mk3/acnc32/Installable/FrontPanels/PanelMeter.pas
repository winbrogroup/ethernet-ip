unit PanelMeter;

interface

uses Classes, CoreCNC, CNC32, CNCTypes, ExtCtrls, Windows, Graphics;

type
  TPanelMeter = class(TPaintBox)
  private
    FVertical : Boolean;
    FBars : Integer;
    FValue : Double;
    FMaxValue : Double;
    FMinValue : Double;
    FLowValue : Double;
    FHighValue : Double;
    FLowColor : TColor;
    FHighColor : TColor;
    FNormalColor : TColor;
    FGap : Integer;
  protected
  public
    constructor Create(aOwner : TComponent); override;
    procedure Paint; override;
    property Vertical : Boolean read FVertical write FVertical;
    property Bars : Integer read FBars write FBars;
    property Value : Double read FValue write FValue;
    property MaxValue : Double read FMaxValue write FMaxValue;
    property MinValue : Double read FMinValue write FMinValue;
    property LowValue : Double read FLowValue write FLowValue;
    property HighValue : Double read FHighValue write FHighValue;
    property LowColor : TColor read FLowColor write FLowColor;
    property NormalColor : TColor read FNormalColor write FNormalColor;
    property HighColor : TColor read FHighColor write FHighColor;
    property Gap : Integer read FGap write FGap;
  end;

implementation


constructor TPanelMeter.Create(aOwner : TComponent);
begin
  inherited Create(aOwner);
  FGap := 2;
  FMinValue := 0;
  FMaxValue := 10;
  FLowValue := 2;
  FHighValue := 9;
  FLowColor := clYellow;
  FHighColor := clRed;
  FNormalColor := clGreen;
end;

procedure TPanelMeter.Paint;
var I : Integer;
    UnitHeight, UnitWidth : Integer;
    R : TRect;
begin
  if Vertical then begin
    UnitHeight := Height div Bars;
    UnitWidth := Width - (2*Gap);
    R.Left := Gap;
    R.Right := Width - Gap;
    Canvas.Brush.Color := clRed;
    Canvas.Pen.Color := clBlue;
    for I := 0 to Bars - 1 do begin
      if
      R.Bottom := Height - (Height * I div Bars);
      R.Top := Height - (Height * (I+1) div Bars - Gap);
      Canvas.Rectangle(R);
    end;
  end else begin
    for I := 0 to Bars - 1 do begin
    end;
  end;
end;


end.
