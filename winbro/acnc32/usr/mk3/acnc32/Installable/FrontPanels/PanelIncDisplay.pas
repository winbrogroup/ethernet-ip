unit PanelIncDisplay;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Dialogs,
  StdCtrls,ExtCtrls, comCtrls, Buttons, CNC32;

type
  DirectionTypes = (dtUp,dtDown,dtLeft,dtRight);

  TPanelIncDisplay = class(TPanel)
  private
    FPosition   : Integer;     // index into strings
    FItems      : TStringList; // Display and value pairs
    FValue      : Integer;     // Value of current position
    FOnChange   : TNotifyEvent;
    FArrowColor : TColor;
    FTextColor  : TColor;

    UpButton     : TPaintBox;
    DownButton   : TPaintBox;
    UpState      : Boolean;
    DownState    : Boolean;
    FROLabel     : TLabel;
    DisplayVal   : TLabel;
    FNearestFit  : Boolean;
    FRequestPosition : Integer;

    procedure SetPosition(aPosition : Integer);
    procedure MyMouseDown(Sender: TObject; Button: TMouseButton;
                Shift: TShiftState; X, Y: Integer);

    procedure MyMouseUp(Sender: TObject; Button: TMouseButton;
                Shift: TShiftState; X, Y: Integer);
    procedure MyPaint(Sender : TObject);

    procedure ArrowPaint(Surface: TPaintBox; Direction : DirectionTypes; DownState : Boolean);

    procedure SetLegend(aLegend : string);
    procedure SetArrowColor(aColor : TColor);
    procedure SetTextColor(aColor : TColor);
    function GetLegend : string;
  protected
    procedure DoChange;    dynamic;

  public
    InputTag, OutputTag : TAbstractTag;
    constructor Create(AOwner:TComponent); override;
    destructor Destroy; override;
    procedure Resize; override;
    function ValueAt(Index : Integer) : Integer;
    procedure SetValue(aValue : Integer);
    property Position   : Integer      read FPosition write SetPosition;
    property OnChange   : TNotifyEvent read FOnChange write FOnChange;
    property Items      : TStringList  read FItems    write FItems;
    property Legend     : string       read GetLegend write SetLegend;
    property TextColor  : TColor       read FTextColor write SetTextColor;
    property ArrowColor : TColor read FArrowColor write SetArrowColor;
    property NearestFit : Boolean read FNearestFit write FNearestFit;
    property Color;
    property OnExit;
    property RequestPosition : Integer read FRequestPosition;
    property Value : Integer read FValue;
    end;

implementation

constructor TPanelIncDisplay.Create(AOwner:TComponent);
begin
  inherited Create(AOwner);
  Color := clBlack;
//  Borderstyle := bsNone;
  FroLabel := TLabel.Create(Self);
  Items := TStringList.Create;

  with FroLabel do begin
    Parent := Self;
    Color := clBlack;
    Font.Color := clYellow;
    Caption := 'Not Set';
    alignment := taCenter;
    Font.Style := [fsBold];
  end;

  UpButton := TPaintbox.Create(Self);
  UpButton.Parent := Self;
  with UpButton do begin
    ControlStyle := [csopaque];
    Visible := True;
    BringToFront;
    Top := 0;
    Left := 0;
    OnMouseDown  := MyMouseDown;
    OnMouseUP  := MyMouseUp;
    OnPaint := MyPaint;
  end;

  DownButton := TPaintbox.Create(Self);
  DownButton.Parent := Self;

  with DownButton do begin
    ControlStyle := [csopaque];
    Visible := True;
    BringToFront;
    Top := (Height div 2) +1;
    OnMouseDown  := MyMouseDown;
    OnMouseUP  := MyMouseUp;
    Left := 0;
    OnPaint := MyPaint;
  end;

  DisplayVal := TLabel.Create(Self);
  with DisplayVal do begin
    Parent := Self;
    AutoSize := True;
//    BorderStyle := bsNone;
    Color := clBlack;
    Font.Color := clYellow;
  end;

  FPosition := 0;
  Resize;
end;

destructor TPanelIncDisplay.Destroy;
begin
  inherited Destroy;
end;

procedure TPanelIncDisplay.Resize;
var Retry  : Integer;
begin
  inherited Resize;
  with UpButton do begin
    Width := Self.Height div 2;
    Height :=Self.Height div 2;
    Left := 0;
    Top := 0;
  end;


  with DownButton do begin
    Width := Self.Height div 2;
    Height :=Self.Height div 2;
    Left := 0;
    Top := Self.Height div 2+1;
  end;

  with DisplayVal do begin
    Left :=  (Self.Height div 2)+20;
    Top :=  Self.Height div 2;
  end;

  with FroLabel do begin
    Retry := 2;
    while (FroLabel.Width > (Self.Width - (Self.Height div 2) -4) -4) and (retry < 20) do begin
      Font.Size := Self.Height div Retry;
      inc(Retry);
    end;



    Left :=  (Self.Height div 2)+2;
    Top :=  5;
    Width := Self.Width - (Self.Height div 2) -4
  end;

  Retry := 2;
  DisplayVal.Font.Size := 1;

  while (DisplayVal.Height < (Self.Height div 2) -4) and (retry < 50) do begin
    DisplayVal.Font.Size := retry;
    inc(Retry);
  end;
end;

procedure TPanelIncDisplay.SetPosition(aPosition : Integer);
begin
  if (aPosition < Items.Count) and (aPosition >= 0) then begin
    FPosition := aPosition;
    DisplayVal.Caption := Items[FPosition];
    FValue := Integer(Items.Objects[FPosition]);
//    DoChange;
//    if Assigned(OnChange) then
//      OnChange(Self);
  end;
end;

procedure TPanelIncDisplay.SetValue(aValue : Integer);
var I : Integer;
begin
  for I := 0 to Items.Count - 1 do begin
    if aValue = Integer(Items.Objects[I]) then begin
      Position := I;
      Exit;
    end;
    if NearestFit and (Integer(Items.Objects[I]) > aValue) then begin
      Position := I;
      Exit;
    end;
  end;
end;

function TPanelIncDisplay.ValueAt(Index : Integer) : Integer;
begin
  if Items.Count = 0 then
    Result := 0
  else if Index < 0 then
    Result := Integer(Items.Objects[0])
  else if Index >= Items.Count then
    Result := Integer(Items.Objects[Items.Count - 1])
  else
    Result := Integer(Items.Objects[Index]);
end;

procedure TPanelIncDisplay.SetLegend(aLegend : string);
begin
  FroLabel.Caption := aLegend;
  DoChange;
end;

function TPanelIncDisplay.GetLegend : string;
begin
  Result := FroLabel.Caption;
end;

procedure TPanelIncDisplay.SetArrowColor(aColor : TColor);
begin
  FArrowColor := aColor;
  DoChange;
end;

procedure TPanelIncDisplay.SetTextColor(aColor : TColor);
begin
  FROLabel.Font.Color := aColor;
  DisplayVal.Font.Color := aColor;
  FROLabel.Color := Color;
  DisplayVal.Color := Color;
  FTextColor := aColor;
  DoChange;
end;


procedure TPanelIncDisplay.DoChange;
begin
  if Assigned(FOnChange) then
    FOnChange(Self);
end;

procedure TPanelIncDisplay.MyMouseDown(Sender: TObject; Button: TMouseButton;
                                          Shift: TShiftState; X, Y: Integer);
begin
  inherited MouseDown(Button,Shift,x,y);
  if Sender = UpButton then begin
    ArrowPaint(TPaintBox(Sender), dtup, True);
  end;

  if Sender = DownButton then begin
    ArrowPaint(TPaintBox(Sender), dtdown, True);
  end;

//  TPaintBox(Sender).Invalidate;
end;

procedure TPanelIncDisplay.MyMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  inherited MouseUp(Button,Shift,x,y);
  if Sender = UpButton then begin
    FRequestPosition := Position + 1;
    ArrowPaint(TPaintBox(Sender), dtup, False);
  end;

  if Sender = DownButton then begin
    FRequestPosition := Position - 1;
    ArrowPaint(TPaintBox(Sender), dtdown, False);
  end;

  if RequestPosition < 0 then
    FRequestPosition := 0;

  if RequestPosition >= Items.Count then
    FRequestPosition := Items.Count - 1;

  if Assigned(FOnChange) then
    FOnChange(Self);
end;


procedure TPanelIncDisplay.ArrowPaint(Surface: TPaintBox; Direction : DirectionTypes;DownState : Boolean);
var
Rect  : Trect;
begin

if not assigned(Surface) then exit;
   Rect.Left :=0;
   Rect.Top := 0;
   Rect.Bottom := Surface.Height;
   Rect.Right := Surface.Width;
   try
   case Direction of
   dtUP:
        begin
        with Surface.Canvas do
             begin
             Brush.Color := Color;
             FillRect(Rect);
             Pen.Color := ArrowColor;
             Pen.width := 1;
             MoveTo((Surface.Width div 2),1);
             LineTo(1,Surface.Height-1);
             LineTo(Surface.Width-1,Surface.Height-1);
             LineTo((Surface.Width div 2),1);
             Brush.Color := ArrowColor;
             FloodFill(Surface.Width div 2 , Surface.Height div 2, ArrowColor, fsBorder);
             if DownState then
                begin
                Pen.Color:= clSilver;
                Pen.Width := 3;
                end
             else
                begin
                Pen.Color:= clWhite;
                Pen.Width := 2;
                end;
              MoveTo((Surface.Width div 2),2);
              LineTo(2,Surface.Height-1);
              if DownState then
                begin
                Pen.Color := clWhite;
                Pen.Width := 3;
                end
              else
                begin
                Pen.Color := clSilver;
                Pen.Width := 2;
                end;
               LineTo(Surface.Width-1,Surface.Height-1);
              end;
        end;

   dtDown:
        begin
        with Surface.Canvas do
             begin
             Brush.Color := Color;
             FillRect(Rect);
             Pen.Color := ArrowColor;
             Pen.width := 1;
             MoveTo((Surface.Width div 2),Surface.Height-1);
             LineTo(1,1);
             LineTo(Surface.Width-2,1);
             LineTo((Surface.Width div 2),Surface.Height-1);
             Brush.Color := ArrowColor;
             FloodFill(Surface.Width div 2 , Surface.Height div 2, ArrowColor, fsBorder);
             if DownState then
                begin
                Pen.Color:= clSilver;
                Pen.Width := 3;
                end
             else
                begin
                Pen.Color:= clWhite;
                Pen.Width := 2;
                end;
             MoveTo((Surface.Width-2),2);
             LineTo(2,2);
              if DownState then
                begin
                Pen.Color := clWhite;
                Pen.Width := 3;
                end
              else
                begin
                Pen.Color := clSilver;
                Pen.Width := 2;
                end;
             LineTo((Surface.Width div 2),Surface.Height-1);
             end
         end;
        end; // case
   except
   end;
end;

procedure TPanelIncDisplay.MyPaint(Sender : TObject);
begin
  if Sender = UpButton then
    ArrowPaint(TPaintBox(Sender),dtUp,UpState);
  if Sender = DownButton then
    ArrowPaint(TPaintBox(Sender),dtDown,DownState);
end;


end.
