unit PanelGroup;

interface

uses Classes, StdCtrls, ExtCtrls;

type
  TPanelGroup = class(TPanel)
  public
    constructor Create(AOwner: TComponent); override;
  end;

implementation

{ TPanelGroup }

constructor TPanelGroup.Create(AOwner: TComponent);
begin
  inherited;
  ParentFont:= True;
end;

end.
