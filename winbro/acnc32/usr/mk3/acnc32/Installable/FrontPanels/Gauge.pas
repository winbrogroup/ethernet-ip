unit Gauge;

interface

uses Classes, SysUtils, Controls, Windows, Graphics, ExtCtrls, CoreCnc, Cnc32, CncTypes, StreamTokenizer;
type
  TAbstractGauge = class(TCustomPanel)
  private

  procedure DrawMarker(Canvas: TCanvas);
  procedure DrawTicks(Canvas: TCanvas; Length: Integer; Inc: Integer; HasLabels: Boolean);
  procedure DrawLabels(Canvas: TCanvas);

  protected
    GaugeLabel: string;
    Units: string;
    Inputtag: TAbstractTag;
    Value: Double;
    MajorTick: Integer;
    MinorTick: Integer;
    //Max: Integer;
    //Min: Integer;
    Modulus: Double;
    StartX: Integer;
    StartY: Integer;
    EndX: Integer;
    EndY: Integer;
    WorkingWidth: Integer;
    ClockImg2: TBitMap;
    BackgroundColor: TColor;
    ForegroundColor: TColor;
    TextColor: TColor;
    Scaling: Double;

  public
    InputTagName: string;
    TagOwner: TCNC;
    procedure Read(S: TStreamTokenizer);
    procedure Install(TagOwner: TCNC);
    procedure Callback(Tag: TAbstractTag);
    procedure Paint; override;
    procedure Resize; override;
  end;

implementation

procedure TAbstractGauge.Callback(Tag: TAbstractTag);
begin
  Value := Tag.AsDouble;
  Invalidate;
end;

procedure TAbstractGauge.Install(TagOwner: TCNC);
begin
  TagOwner.TagEvent(InputTagName, edgeChange, TDoubleTag, Callback);
  WorkingWidth := Width-1;
  Self.DoubleBuffered:= True;
end;

procedure TAbstractGauge.Paint;

begin
  Canvas.Draw(0, 0, ClockImg2);
  DrawMarker(Canvas);
end;

procedure TAbstractGauge.Resize;
begin
  inherited;

  ClockImg2:= TBitMap.Create;
  ClockImg2.Width:= Self.Width;
  ClockImg2.Height:= Self.Height;
  ClockImg2.Canvas.Font.Color:= TextColor;
  ClockImg2.Canvas.Brush.Color:= Parent.Brush.Color;
  ClockImg2.Canvas.Pen.Color:= Parent.Brush.Color;
  ClockImg2.Canvas.Rectangle(0, 0, Self.Width, Self.Height);
  ClockImg2.Canvas.Brush.Color:= BackgroundColor;
  ClockImg2.Canvas.Ellipse(0, 0, WorkingWidth, WorkingWidth);
  ClockImg2.Canvas.Pen.Color:= ForegroundColor;
  DrawTicks(ClockImg2.Canvas, 15, MajorTick, True);
  DrawTicks(ClockImg2.Canvas, 7, MinorTick, False);
  DrawLabels(ClockImg2.Canvas);

end;

procedure TAbstractGauge.Read(S: TStreamTokenizer);
  procedure SetFont(fs: TFontStyle);
  var v: Boolean;
      c: TFontStyles;
  begin
    v:= S.ReadBooleanAssign;
    c:= Canvas.Font.Style;
    if v then
      Include(c, fs)
    else
      Exclude(c, fs);
    Canvas.Font.Style:= c;
  end;

var Token: string;
begin
  Self.BackgroundColor:= clWhite;
  Self.ForegroundColor:= clBlue;
  Self.TextColor:= clBlack;
  Scaling:=1;
  Modulus:= 100;
  MajorTick:=10;
  MinorTick:= 1;

  S.ExpectedTokens(['=', '{']);
  Token:= LowerCase(S.Read);
  while not S.EndOfStream do begin
    if Token = 'left' then
      Self.Left := S.ReadIntegerAssign
    else if Token =  'top' then
      Self.Top := S.ReadIntegerAssign
    else if Token = 'size' then
      begin
        Self.Width := S.ReadIntegerAssign;
        Self.Height := Self.Width
      end
    else if Token = 'label' then
      GaugeLabel:= S.ReadStringAssign
    else if Token = 'units' then
      Units:= S.ReadStringAssign
    else if Token = 'inputtag' then
      InputTagName:= S.ReadStringAssign
    else if Token = 'majortick' then begin
      MajorTick:= S.ReadIntegerAssign;
      if MajorTick = 0 then
        MajorTick := 10;
      end
    else if Token = 'minortick' then begin
      MinorTick:= S.ReadIntegerAssign;
      if MinorTick = 0 then
        MinorTick := 1;
      end
    else if Token = 'modulus' then begin
      Modulus:= S.ReadDoubleAssign;
      if Modulus = 0 then
        Modulus := 100
      end
   else if Token = 'background' then
      BackgroundColor:= S.ReadColorAssign
    else if Token = 'foreground' then
      ForegroundColor:= S.ReadColorAssign
    else if Token = 'textcolor' then
      TextColor:= S.ReadColorAssign
    else if Token = 'scaling' then begin
      Scaling:= S.ReadDoubleAssign;
      if Scaling = 0 then
        Scaling:= 1
      end
    else if Token = '}' then
       Exit
    else
      raise ECNCInstallFault.CreateFmt('Unexpected token %s', [Token]);
    Token := LowerCase(S.Read);
  end;
  raise ECNCInstallFault.CreateFmt('Unexpected end of file %s', [Name]);
end;

procedure TAbstractGauge.DrawTicks(Canvas: TCanvas; Length: Integer; Inc: Integer; HasLabels: Boolean);

var Angle: Double;
var IncLabel: Integer;
var IncLabelStr: String;
var TextX: Integer;
var TextY: Integer;
var TextRadius: Integer;
var Count: Integer;

begin
  Angle:= 0;
  Count:= 0;
  IncLabel := 0;

  While Angle < 2*PI do
  begin

    StartX := Round(WorkingWidth /2 + (WorkingWidth/2-Length) * Sin(Angle));
    StartY := Round(WorkingWidth /2 - (WorkingWidth/2-Length) * Cos(Angle));
    EndX := Round(WorkingWidth /2 + WorkingWidth/2 * Sin(Angle));
    EndY := Round(WorkingWidth /2 - WorkingWidth/2 * Cos(Angle));
    Canvas.MoveTo(StartX, StartY);
    Canvas.LineTo(EndX, EndY);

    if HasLabels then
    begin
    IncLabelStr := IntToStr(IncLabel);
    TextRadius := Canvas.TextHeight(IncLabelStr);
    if (Canvas.TextWidth(IncLabelStr) > TextRadius) then
      TextRadius := Canvas.TextWidth(IncLabelStr);
    TextRadius := Round(TextRadius / 2);
    TextX := Round(WorkingWidth /2 + (WorkingWidth/2-Length-TextRadius-5) * Sin(Angle)) - TextRadius;
    TextY := Round(WorkingWidth /2 - (WorkingWidth/2-Length-TextRadius-5) * Cos(Angle)) - TextRadius;
    Canvas.TextOut(TextX,TextY, IncLabelStr);
    IncLabel := Inc * Count;
    end;

    Angle:= Inc*Count/(Modulus)*2*PI;
    Count := Count + 1;
  end;
end;

procedure TAbstractGauge.DrawLabels(Canvas: TCanvas);

var IncLabelStr: String;
var TextX: Integer;
var TextY: Integer;
begin
  IncLabelStr := FloatToStr(Value);
  TextX := Round(WorkingWidth / 2 - Canvas.TextWidth(GaugeLabel)/2);
  TextY := Round(WorkingWidth / 2 - Canvas.TextHeight(GaugeLabel)/2-Canvas.TextHeight(GaugeLabel));
  Canvas.TextOut(TextX,TextY,GaugeLabel);
  TextX := Round(WorkingWidth / 2 - Canvas.TextWidth(Units)/2);
  TextY := Round(WorkingWidth / 2 - Canvas.TextHeight(Units)/2+Canvas.TextHeight(Units));
  Canvas.TextOut(TextX,TextY,Units);
end;

procedure TAbstractGauge.DrawMarker(Canvas: TCanvas);
var Angle: Double;
var IncLabelStr: String;
var TextX: Integer;
var TextY: Integer;

begin
  Canvas.Pen.Color:= TextColor;
  Canvas.Brush.Color := BackgroundColor;
  Angle := Value/Scaling/Modulus*2*PI;
  IncLabelStr := FloatToStr(Value/Scaling);
  TextX := Round(WorkingWidth / 2 - Canvas.TextWidth(IncLabelStr)/2);
  TextY := Round(WorkingWidth / 2 - Canvas.TextHeight(IncLabelStr)/2);
  Canvas.Font.Color := TextColor;
  Canvas.TextOut(TextX,TextY,IncLabelStr);

  Canvas.Pen.Color:= ForegroundColor;
  StartX := Round(WorkingWidth /2 + 30 * Sin(Angle));
  StartY := Round(WorkingWidth /2 - 30  * Cos(Angle));
  EndX := Round(WorkingWidth /2 + (WorkingWidth/2) * Sin(Angle));
  EndY := Round(WorkingWidth /2 - (WorkingWidth/2) * Cos(Angle));
  Canvas.MoveTo(StartX, StartY);
  Canvas.LineTo(EndX, EndY);
end;

end.
