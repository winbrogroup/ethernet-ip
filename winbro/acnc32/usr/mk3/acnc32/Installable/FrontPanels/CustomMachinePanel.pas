unit CustomMachinePanel;

interface

uses Classes, SysUtils, Controls, CoreCNC, CNC32, CNCTypes, GenericIO, Named,
     StreamTokenizer, ExtCtrls, PanelButton, Forms, WinTypes, WidgetTypes, DynamicHelp,
     IFSDServer, Graphics, StdCtrls, IStringArray, PanelLabels, PanelImage, PanelGroup, Gauge;

// Buttons Color 1 and 2, multi line caption, Tag output
// Labels, static / tag, border, active color, inactive color

type
  TCustomMachinePanel = class;

  TCustomMachinePanel = class(TAbstractDisplay, IAcnc32StringArrayNotifyA)
  private
    ButtonGroups : TList;
    DBLabels: TList;
    NamedLabels: TList;
    PanelImages: TList;
    PWList : TList;
    GaugeList: TList;
    Editing : Boolean;
    MappedMDTs: TMappedMDTs;
    DataChange: Boolean;

    CurrentParent: TWinControl;

    procedure UpdateDisplay(aTag : TAbstractTag);
    procedure ReadConfig(S : TStreamTokenizer);
    procedure InnerReadConfig(S: TStreamTokenizer);
    procedure EditConfig(aTag : TAbstractTag);
    procedure PluginConfig(aTag : TAbstractTag);
  protected
  public
    constructor Create(aOwner : TComponent); override;
    destructor Destroy; override;
    procedure InstallComponent(Params : TParamStrings); override;
    procedure Installed; override;
    procedure CheckInstallation; override;
    procedure LoseFocus; override;
    function HTTPGeneratePage(HFile : THANDLE; var CGIPath: WideString; CGIRequest: ICGIStringList) : Boolean; override;
    procedure CursorChange(Table : IACNC32StringArrayA; Cursor: Integer); stdcall;
    procedure CellChange(Table : IACNC32StringArray; Row, Col : Integer); stdcall;
    procedure TableChange(Table : IACNC32StringArray); stdcall;
    function GetName: WideString; stdcall;
  end;


  TButtonGroup = class(TPanel, IMachinePanelWidget)
  private
    List : TList;
    Horizontal : Integer; // Matrix width
    Vertical : Integer; // Matrix Height
    UnitWidth, UnitHeight : Integer;
    Gap : Integer;
    Keyword : string;
    procedure ButtonPress(Sender : TObject);
    procedure ButtonRelease(Sender : TObject);
  protected
    procedure SetEnabled(AValue: Boolean); override;
  public
    ButtonClass : TAbstractPanelButtonClass;
    TagOwner : TCNC;
    constructor Create(aOwner : TComponent); override;
    destructor Destroy; override;
    procedure ReleaseNonLatching;
    procedure ReadFromStrings(S : TStreamTokenizer);
    procedure MatchDimensions;
    procedure SetEnableState(AValue: Boolean);
    procedure UpdateLamps;
    procedure Resize; override;
    procedure SetWidgetBounds(aRect : TRect);
    function LibName : string;
    function DeviceName : string;
    function SetProperty(aName, aValue : string) : Boolean;
    function GetProperties : string;
    function Dims : TRect;
    function _AddRef : Integer; stdcall;
    function _Release : Integer; stdcall;
  end;

  TPluginWidgetOpen = function (aName : PChar) : THANDLE; stdcall;
  TPluginWidgetClose = procedure (Hndl : THANDLE); stdcall;
  TPluginWidgetSetHDC = procedure(Hndl, HDC : THANDLE); stdcall;
  TPluginWidgetSetBounds = procedure(Hndl, HDC : THANDLE; Width, Height : Integer); stdcall;
  TPluginWidgetSetProperty = function(Hndl, HDC : THANDLE; aToken, aValue : PChar) : Boolean; stdcall;
  TPluginWidgetPaint = procedure(aHandle, HDC : THANDLE); stdcall;
  TPluginWidgetSetIntegerInput = function(aHandle : THANDLE; aName : PChar; aValue : Integer) : Boolean; stdcall;
  TPluginWidgetSetStringInput  = function(aHandle : THANDLE; aName, aValue : PChar) : Boolean; stdcall;
  TPluginWidgetMouseDown = procedure(aHandle, HDC : THANDLE; Button: Integer; Shift: Byte; X, Y: Integer); stdcall;
  TPluginWidgetMouseUp = procedure(aHandle, HDC : THANDLE; Button: Integer; Shift: Byte; X, Y: Integer); stdcall;
  TPluginWidgetSetCallback = procedure(aHandle : THANDLE; aName : PChar; Callback : TPWEv); stdcall;
  TPluginWidgetGetProperties = function(aHandle : THANDLE) : PChar; stdcall;

  TPanelWidgetDLL = class(TInterfacedObject, IMachinePanelWidget)
  private
    WidgetOpen : TPluginWidgetOpen;
    WidgetClose : TPluginWidgetClose;
    WidgetSetHDC : TPluginWidgetSetHDC;
    WidgetSetBounds : TPluginWidgetSetBounds;
    WidgetSetProperty : TPluginWidgetSetProperty;
    WidgetPaint : TPluginWidgetPaint;
    WidgetSetIntegerInput : TPluginWidgetSetIntegerInput;
    WidgetSetStringInput : TPluginWidgetSetStringInput;
    WidgetMouseDown : TPluginWidgetMouseDown;
    WidgetMouseUp : TPluginWidgetMouseUp;
    WidgetSetCallback : TPluginWidgetSetCallback;
    WidgetGetProperties : TPluginWidgetGetProperties;
    LibCode : HMODULE;
    PB : TPaintBox;
    Owner : TCNC;
    LibH : THandle;
    Events : TStringList;
    SEvents : TStringList;
    Publish : TStringList;
    FLibName : string;
    FDeviceName : string;
    HTTPHelp : string;
    procedure DoRepaint(Sender : TObject);
    procedure DoMouseDown(Sender : TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure DoMouseUp(Sender : TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure UpdateInteger(aTag : TAbstractTag);
    procedure UpdateString(aTag : TAbstractTag);
  public
    InputTag : TAbstractTag;
    OutputTag : TAbstractTag;
    constructor Create(aOwner : TCNC; aLib, aName : string);
    destructor Destroy; override;
    procedure Read(S: TStreamTokenizer);
    function SetProperty(aName, aValue : string) : Boolean;
    procedure SetWidgetBounds(aRect : TRect);
    property PaintBox : TPaintBox read PB;
    function GetProperties : string;
    function LibName : string;
    function DeviceName : string;
    function Dims : TRect;
    function _AddRef : Integer; stdcall;
    function _Release : Integer; stdcall;
  end;


implementation

uses
  PanelPluginPropertyEditor;

var
  DLLList : TList;

constructor TCustomMachinePanel.Create(aOwner : TComponent);
begin
  inherited Create(aOwner);
  ButtonGroups := TList.Create;
  PWList := TList.Create;
  DBLabels:= TList.Create;
  NamedLabels:= TList.Create;
  GaugeList:= TList.Create;
  PanelImages:= TList.Create;
  MappedMDTs:= TMappedMDTs.Create(Self);

  Self.ManageEdit := True;
  //Font.Name:= SysObj.FontName;
end;

destructor TCustomMachinePanel.Destroy;
  procedure CleanObjectList(aList : TList);
  begin
    while aList.Count > 0 do begin
      TObject(aList[0]).Free;
      aList.Delete(0);
    end;
  end;
begin
  CleanObjectList(ButtonGroups);
  ButtonGroups.Free;
  CleanObjectList(DLLList);
  inherited;
end;

procedure TCustomMachinePanel.InstallComponent(Params : TParamStrings);
begin
  inherited InstallComponent(Params);
  DynamicHelp.DynamicHelpSystem.RegisterComponent(Self);
  ManageEdit := not Params.ReadBool('UseEditLevel', False);
  SysObj.HttpServerHost.AddComponentPath(HttpHelpSection, Name, 'Machine Panel - ' + Name, Self);
end;

procedure TCustomMachinePanel.Installed;
var S : TStreamQuoteTokenizer;
    Stream: TStream;
begin
  inherited Installed;
  TagEvent(NameDisplaySweep, edgeChange, TMethodTag, UpdateDisplay);
  TagEvent(Name, edgeFalling, TMethodTag, Activate);
  TagEvent(Name + 'Config', EdgeFalling, TMethodTag, EditConfig);
  TagEvent(Name + 'PluginConfig', EdgeFalling, TMethodTag, PluginConfig);
  BevelOuter := bvNone;
  Stream := Self.OpenLiveStream(Name);
  S:= TStreamQuoteTokenizer.Create;
  S.SetStream(Stream);
  S.Seperator := '()[],={}';
  S.QuoteChar := '''';
  S.CommentChar:= '#';
  S.ManageStream:= True;
  try
    ReadConfig(S);
  finally
    S.Free;
  end;
end;


procedure TCustomMachinePanel.CheckInstallation;
var I: Integer;
    L: TAcnc32NamedLabel;
    G: TAbstractGauge;
begin
  inherited;
  for I:= 0 to NamedLabels.Count - 1 do begin
    L:= TAcnc32NamedLabel(NamedLabels[I]);
    L.UpdateTagState;
  end;

  for I:= 0 to GaugeList.Count - 1 do begin
    G:= TAbstractGauge(GaugeList[I]);
    G.Install(Self);
  end;

end;


function TCustomMachinePanel.HTTPGeneratePage(HFile : THANDLE;
            var CGIPath: WideString; CGIRequest : ICGIStringList) : Boolean;
var TopLevel : string;
    B, I : Integer;
    BtnG : TButtonGroup;
    Btn : TAbstractPanelButton;
    PW : TPanelWidgetDll;
begin
  Result := True;
  if not inherited HTTPGeneratePage(HFile, CGIPath, CGIRequest) then begin
    TopLevel := ReadDelimitedW(CGIPath, '/');
    WriteACNC32HTTPTitle(HFile, Name);
    WriteACNC32HTTPHeading(hFile, 1, Name);
    WriteACNC32HTTPHeading(hFile, 2, ClassName);
    if TopLevel = '' then begin
      WriteStringToHandle(hFile, '<table border="1" align="center" width="80%">' + CarRet);
      WriteStringToHandle(hFile, '<h2>Click on the image you require help with</h2>' + CarRet);

      WriteStringToHandle(hFile, '<map name="ImageMap">');
      for B := 0 to ButtonGroups.Count - 1 do begin
        BtnG := TButtonGroup(ButtonGroups[B]);
        for I := 0 to BtnG.List.Count - 1 do begin
          Btn := TAbstractPanelButton(BtnG.List[I]);
          WriteStringToHandle(hFile, Format('<AREA SHAPE=RECT COORDS="%d,%d,%d,%d" a href="/cgi-bin/component/%s/button/%d/%d">' + CarRet,
                         [BtnG.Left + Btn.Left, BtnG.Top + Btn.Top, BtnG.Left + Btn.Left + Btn.Width, BtnG.Top + Btn.Top + Btn.Height,
                           Name, B, I]));
        end;
      end;

      for B := 0 to PWList.Count - 1 do begin
        PW := TPanelWidgetDLL(PWList[B]);
        WriteStringToHandle(hFile, Format('<AREA SHAPE=RECT COORDS="%d,%d,%d,%d" a href="/cgi-bin/component/%s/widget/%d">' + CarRet,
                       [PW.PB.Left, PW.PB.Top, PW.PB.Left + PW.PB.Width, PW.PB.Top + PW.PB.Height,
                         Name, B]));
      end;

      WriteStringToHandle(hFile, '</MAP>');

      WriteStringToHandle(hFile, Format('<img src="/cgi-bin/component/%s/screenshot.jpg" USEMAP="#ImageMap">', [Name]));
      WriteStringToHandle(hFile, '</body></html>' + CarRet);
    end else if CompareText(TopLevel, 'button') = 0 then begin
      B := StrToInt(ReadDelimitedW(CGIPath, '/'));
      I := StrToInt(ReadDelimitedW(CGIPath, '/'));
      BtnG := TButtonGroup(ButtonGroups[B mod ButtonGroups.Count]);
      Btn := TAbstractPanelButton(BtnG.List[I mod BtnG.List.Count]);

      WriteStringToHandle(hFile, '<table border="0" align="center" width="80%">' + CarRet);
      WriteStringToHandle(hFile, Format('<h2>%s</h2>' + CarRet, [Btn.Legend]));
      DynamicHelp.DynamicHelpSystem.WriteHTTPFile(hFile, Btn.HTTPHelp);
      WriteStringToHandle(hFile, Format('</table>' + CarRet, [Btn.Legend]));

    end else if CompareText(TopLevel, 'widget') = 0 then begin
      WriteStringToHandle(hFile, '<table border="0" align="center" width="80%">' + CarRet);
      WriteStringToHandle(hFile, '<h2></h2>' + CarRet);
      B := StrToInt(ReadDelimitedW(CGIPath, '/'));
      PW := TPanelWidgetDLL(PWList[B]);
      DynamicHelp.DynamicHelpSystem.WriteHTTPFile(hFile, PW.HTTPHelp);
      WriteStringToHandle(hFile, '</table>' + CarRet);
    end else if CompareText(TopLevel, 'listall') = 0 then begin
      WriteStringToHandle(hFile, '<table border="0" align="center" width="80%">' + CarRet);
      for B := 0 to ButtonGroups.Count - 1 do begin
        BtnG := TButtonGroup(ButtonGroups[B]);
        for I := 0 to BtnG.List.Count - 1 do begin
          Btn := TAbstractPanelButton(BtnG.List[I]);
          WriteStringToHandle(hFile, Format('<h3>%s</h3>' + CarRet, [Btn.Legend]));
          DynamicHelp.DynamicHelpSystem.WriteHTTPFile(hFile, Btn.HTTPHelp);
          WriteStringToHandle(hFile, '<hr>');
        end;
      end;
      for B := 0 to PWList.Count - 1 do begin
        PW := TPanelWidgetDLL(PWList[B]);
        DynamicHelp.DynamicHelpSystem.WriteHTTPFile(hFile, PW.HTTPHelp);
        WriteStringToHandle(hFile, '<hr>');
      end;
      WriteStringToHandle(hFile, '</table>' + CarRet);
    end else
      Result := False;

    WriteStringToHandle(hFile, '<hr>' + CarRet);
    WriteStringToHandle(hFile, Format('<a href="/cgi-bin/component/%s/listall">List all</a>', [Name]));
    WriteStringToHandle(hFile, '</body>' + CarRet);
  end;
end;


procedure TCustomMachinePanel.EditConfig(aTag : TAbstractTag);
var I : Integer;
begin
  if not Editing then begin
    Editing := True;
    for I := 0 to ButtonGroups.Count - 1 do begin
      with TButtonGroup(ButtonGroups[I]) do begin
        BevelOuter := bvRaised;
        BevelWidth := 3;
        Color := clWhite;
      end;
    end;
  end else begin
    Editing := False;
    for I := 0 to ButtonGroups.Count - 1 do begin
      with TButtonGroup(ButtonGroups[I]) do begin
        BevelOuter := bvnone;
        BevelWidth := 3;
        Color := Self.Color;
      end;
    end;
  end;
end;

procedure TCustomMachinePanel.PluginConfig(aTag : TAbstractTag);
var ExecuteArray : array of IMachinePanelWidget;
    I : Integer;
begin
  SetLength(ExecuteArray, DLLList.Count + ButtonGroups.Count);
  for I := 0 to DLLList.Count - 1 do
    ExecuteArray[I] := TPanelWidgetDLL(DLLList[I]);

  for I := 0 to ButtonGroups.Count - 1 do
    ExecuteArray[I + DLLList.Count] := TButtonGroup(ButtonGroups[I]);

  TPanelPluginEditorForm.Execute(ExecuteArray);
end;

procedure TCustomMachinePanel.UpdateDisplay(aTag : TAbstractTag);
var I : Integer;
begin
  for I := 0 to ButtonGroups.Count - 1 do
    TButtonGroup(ButtonGroups[I]).UpdateLamps;

  for I:= 0 to DBLabels.Count - 1 do begin
    TAcnc32DBLabel(DBLabels[I]).UpdateNamedState;
  end;

  if DataChange then begin
    for I := 0 to Self.DBLabels.Count - 1 do begin
      TAcnc32DBLabel(DBLabels[I]).UpdateFromTable;
    end;
    DataChange:= False;
  end;
end;


procedure TCustomMachinePanel.LoseFocus;
var I : Integer;
begin
  for I := 0 to ButtonGroups.Count - 1 do begin
    with TButtonGroup(ButtonGroups[I]) do
      ReleaseNonLatching;
  end;
end;

procedure TCustomMachinePanel.ReadConfig(S : TStreamTokenizer);
begin
  S.ExpectedTokens(['MachinePanel', '=', '{']);
  CurrentParent:= Self;
  InnerReadConfig(S);
end;

procedure TCustomMachinePanel.InnerReadConfig(S: TStreamTokenizer);

  procedure ReadButtonGroup(BPClass : TAbstractPanelButtonClass);
  var Bg : TButtonGroup;
  begin
    Bg := TButtonGroup.Create(Self);
    Bg.Parent := CurrentParent;
    Bg.ParentFont := True;
    Bg.TagOwner := Self;
    Bg.ButtonClass := BPClass;
    Bg.BorderStyle := bsNone;
    Bg.Color := Color;
    Bg.BevelOuter := bvNone;
    Bg.Keyword := '';
    S.ExpectedTokens(['=', '{']);
    Bg.ReadFromStrings(S);
    Bg.MatchDimensions;
    Bg.SetEnableState(True);
    ButtonGroups.Add(Bg);
  end;

  function AddTag(ro : Boolean) : TAbstractTag;
  var Tmp : string;
  begin
    S.ExpectedToken('=');
    Tmp := S.Read;
    Result := TagEvent(Tmp, EdgeChange, TIntegerTag, nil)
  end;

  procedure ImportDLL;
  var aLib, aName : string;
      PW : TPanelWidgetDLL;
  begin
    S.ExpectedToken('[');
    aLib := S.Read;
    S.ExpectedToken(',');
    aName := S.Read;

    S.ExpectedTokens([']', '=', '{']);

    PW := TPanelWidgetDLL.Create(Self, aLib, aName);
    PW.Read(S);
    PWList.Add(PW);
  end;

  procedure ReadGauge;
  var G: TAbstractGauge;
  begin
    G:= TAbstractGauge.Create(Self);
    G.Parent := CurrentParent;
    G.Read(S);
    GaugeList.Add(G);
  end;


  procedure ReadDBLabel;
  var l: TAcnc32DBLabel;
  begin
    l := TAcnc32DBLabel.Create(Self);
    l.TagOwner:= Self;
    l.BorderStyle:= sbsNone;
    l.BorderWidth:= 0;
    l.Parent:= CurrentParent;
    l.AutoSize:= True;

    Self.DBLabels.Add(l);
    S.ExpectedTokens(['=', '{']);
    l.Read(S, MappedMDTs);
  end;

  procedure ReadNamedLabel;
  var l: TAcnc32NamedLabel;
  begin
    l := TAcnc32NamedLabel.Create(Self);
    l.TagOwner:= Self;
    l.BorderStyle:= sbsNone;
    l.BorderWidth:= 0;
    l.Parent:= CurrentParent;
    l.AutoSize:= True;

    Self.NamedLabels.Add(l);
    S.ExpectedTokens(['=', '{']);
    l.Read(S);
  end;

  procedure ReadImage;
  var image: TPanelImage;
  begin
    S.ExpectedTokens(['=', '{']);
    image:= TPanelImage.Create(Self);
    image.Parent:= CurrentParent;
    image.TagOwner:= Self;
    image.Color:= Color;
    PanelImages.Add(image);
    image.Read(S);
    image.SetImage;
  end;

  procedure ReadGroup;
  var Panel: TPanelGroup;
  begin
    S.ExpectedTokens(['=', '{']);
    Panel:= TPanelGroup.Create(Self);
    Panel.Parent:= CurrentParent;
    CurrentParent:= Panel;
    InnerReadConfig(S);
    CurrentParent:= Panel.Parent;
  end;

  procedure SetColor(C: TColor);
  begin
    if CurrentParent = Self then
      Color:= C
    else if CurrentParent is TPanel then
      TPanel(CurrentParent).Color:= C
    else if CurrentParent is TPanelGroup then
      TPanelGroup(CurrentParent).Color:= C;
  end;

  procedure SetText(const Text: string);
  begin
    if CurrentParent is TPanelGroup then
      TPanelGroup(CurrentParent).Caption:= Text;
  end;

  procedure ReadDefine;
  var DefName, DefValue: string;
  begin
    DefName:= S.Read;
    DefValue:= S.Read;
    S.AddDefine(DefName, DefValue);
  end;

var
  Token : string;
begin
  try
    Token := LowerCase(S.Read);
    while not S.EndOfStream do begin
      if Token = 'hylandgroup' then
        ReadButtonGroup(THylandButton)
      else if Token = 'roundgroup' then
        ReadButtonGroup(TRoundButton)
      else if Token = 'softgroup' then
        ReadButtonGroup(TSoftButton)
      else if Token = 'dblabel' then
        ReadDBLabel
      else if Token = 'namedlabel' then
        ReadNamedLabel
      else if Token = 'image' then
        ReadImage
      else if Token = 'color' then
        SetColor(ReadColor(S))
      else if Token = 'dll' then
        ImportDLL
      else if Token = 'group' then
        ReadGroup
      else if Token = 'left' then
        CurrentParent.Left := S.ReadIntegerAssign
      else if Token = 'top' then
        CurrentParent.Top := S.ReadIntegerAssign
      else if Token = 'width' then
        CurrentParent.Width:= S.ReadIntegerAssign
      else if Token = 'height' then
        CurrentParent.Height:= S.ReadIntegerAssign
      else if Token = 'text' then
        SetText(S.ReadStringAssign)
      else if Token = 'gauge' then
        ReadGauge
      else if Token = 'define' then
        ReadDefine
      else if Token = '}' then
        Exit
      else
        raise ECNCInstallFault.CreateFmt('Unexpected token %s', [Token]);
      Token := LowerCase(S.Read);
    end;
    raise ECNCInstallFault.CreateFmt('Unexpected end of file %s', [Name]);
  except
    on E : Exception do begin
      raise ECNCInstallFault.CreateFmt('Configuration file [%s] Failed to load because [%s] at line [%d]',
                           [Name, E.Message, S.Line]);
    end;
  end;
end;

constructor TButtonGroup.Create(aOwner : TComponent);
begin
  inherited Create(aOwner);
  List := TList.Create;
  ParentFont:= True;
end;

procedure TButtonGroup.ButtonPress(Sender : TObject);
begin
  with Sender as TAbstractPanelButton do
    if Assigned(ButtonTag) then
      ButtonTag.AsInteger := ButtonTag.AsInteger or ButtonMask;
end;

procedure TButtonGroup.ButtonRelease(Sender : TObject);
begin
  with Sender as TAbstractPanelButton do
    if Assigned(ButtonTag) then
      ButtonTag.AsInteger := ButtonTag.AsInteger and not ButtonMask;
end;


destructor TButtonGroup.Destroy;
begin
  while List.Count > 0 do begin
    TObject(List[0]).Free;
    List.Delete(0);
  end;
  List.Free;
  inherited Destroy;
end;

procedure TButtonGroup.Resize;
var I : Integer;
    B : TAbstractPanelButton;
begin
  inherited;
  if Align <> alNone then
    for I := 0 to List.Count - 1 do begin
      B := TAbstractPanelButton(List[I]);
      B.Left := (Width * B.Ordinate.X) div Horizontal;
      B.Top := (Height * B.Ordinate.Y) div Vertical;
      B.Height := Height div Vertical - Gap;
      B.Width :=  Width div Horizontal - Gap;
    end;
end;

procedure TButtonGroup.SetWidgetBounds(aRect : TRect);
begin
  SetBounds(aRect.Left, aRect.Top, aRect.Right, aRect.Bottom);
end;

function TButtonGroup.LibName : string;
begin
  Result := 'Standard';
end;

function TButtonGroup.DeviceName : string;
begin
  Result := 'ButtonGroup';
end;

function TButtonGroup.Dims : TRect;
begin
  Result.Left := Left;
  Result.Top := Top;
  Result.Right := Width;
  Result.Bottom := Height;
end;

function TButtonGroup.SetProperty(aName, aValue : string) : Boolean;
begin
  Result := False;
end;

function TButtonGroup.GetProperties : string;
begin
  Result := '';
end;

function TButtonGroup._AddRef : Integer; stdcall;
begin
  Result := -1;
end;

function TButtonGroup._Release : Integer; stdcall;
begin
  Result := -1;
end;

procedure TButtonGroup.ReadFromStrings(S : TStreamTokenizer);
var Token : string;
  function AddTag(ro : Boolean) : TAbstractTag;
  var Tmp : string;
  begin
    S.ExpectedToken('=');
    Tmp := S.Read;
    Result := TagOwner.TagEvent(Tmp, EdgeChange, TIntegerTag, nil)
  end;

  function ReadAssignString : string;
  begin
    S.ExpectedToken('=');
    Result := S.Read;
  end;

  procedure LoadImages(B: TAbstractPanelButton);
  var BM: TBitmap;
  begin
    BM:= TBitmap.Create;
    B.UseImages:= True;
    S.ExpectedToken('=');
    BM.LoadFromFile(ProfilePath + 'live/images/' + S.Read);
    B.Images.Add(BM, nil);
    B.Status:= 1;
    B.Status:= 0;
  end;

  procedure ReadButton;
  var B : TAbstractPanelButton;
  begin
    B := ButtonClass.Create(Self);
    B.Parent := Self;
    //B.ParentFont := True;
    B.OnPress := ButtonPress;
    B.OnRelease := ButtonRelease;
//    B.OnRightClick := ButtonRightClick;
    B.Width := UnitWidth;
    B.Height := UnitHeight;
    B.ButtonMask := 1;
    B.LightMask := $ff;
    B.Latched := False;
    List.Add(B);
    S.ExpectedToken('[');
    B.Ordinate.X := S.ReadInteger;
    S.ExpectedToken(',');
    B.Ordinate.Y := S.ReadInteger;
    B.Visible := (SysObj.Keywords.Enable[Keyword] > 0) or (Keyword = '');
    S.ExpectedTokens([']', '=', '{']);

    B.Left := (B.Width + Gap) * B.Ordinate.X + (Gap div 2);
    B.Top := (B.Height + Gap) * B.Ordinate.Y + (Gap div 2);
    Token := LowerCase(S.Read);
    while not S.EndOfStream do begin
      if Token = 'oncolor' then
        B.OnLightColor := ReadColor(S)
      else if Token = 'offcolor' then
        B.OffLightColor := ReadColor(S)
      else if Token = 'buttontag' then
        B.ButtonTag := AddTag(True)
      else if Token = 'lighttag' then
        B.LightTag := AddTag(False)
      else if Token = 'lightmask' then
        B.LightMask := S.ReadIntegerAssign
      else if Token = 'buttonmask' then
        B.ButtonMask := S.ReadIntegerAssign
      else if Token = 'legend' then
        B.Legend := SysObj.Localized.GetString(ReadAssignString)
      else if Token = 'latched' then
        B.Latched := S.ReadIntegerAssign <> 0
      else if Token = 'help' then
        B.HTTPHelp := S.ReadStringAssign
      else if Token = 'keyword' then
        B.Keyword := S.ReadStringAssign
      else if Token = 'image' then
        LoadImages(B)
      else if Token = 'fontsize' then
        B.Font.Size:= S.ReadIntegerAssign
      else if Token = '}' then
        Exit
      else
        raise ECNCInstallFault.CreateFmt('Unexpected token %s', [Token]);
      Token := LowerCase(S.Read);
    end;
    raise ECNCInstallFault.CreateFmt('Unexpected end of file %s', [Name]);
  end;

  procedure ReadAlign;
  var ALS : TStringList;
      I : Integer;
  begin
    Token := LowerCase(ReadAssignString);
    Token := 'al' + Token;
    ALS := TStringList.Create;
    try
      CNCTypes.GetEnumerationNames(TypeInfo(TAlign), ALS);
      for I := 0 to ALS.Count - 1 do begin
        if CompareText(ALS[I], Token) = 0 then begin
          Align := TAlign(I);
          Exit;
        end;
      end;
      raise Exception.CreateFmt('Unknown alignment directive [%s]', [Token]);
    finally
      ALS.Free;
    end;
  end;
begin
  UnitWidth := ButtonClass.UnitWidth;
  UnitHeight := ButtonClass.UnitHeight;
  Token := LowerCase(S.Read);
  while not S.EndOfStream do begin
    if Token = 'left' then
      Left := S.ReadIntegerAssign
    else if Token =  'top' then
      Top := S.ReadIntegerAssign
    else if Token = 'width' then
      Horizontal := S.ReadIntegerAssign
    else if Token = 'height' then
      Vertical := S.ReadIntegerAssign
    else if Token = 'unitwidth' then
      UnitWidth := S.ReadIntegerAssign
    else if Token = 'unitheight' then
      UnitHeight := S.ReadIntegerAssign
    else if Token = 'gap' then
      Gap := S.ReadIntegerAssign
    else if Token = 'button' then
      ReadButton
    else if Token = 'align' then
      ReadAlign
    else if Token = 'keyword' then
      Keyword := S.ReadStringAssign
    else if Token = '}' then
      Exit
    else
      raise ECNCInstallFault.CreateFmt('Unexpected token %s', [Token]);
    Token := LowerCase(S.Read);
  end;
  raise ECNCInstallFault.CreateFmt('Unexpected end of file %s', [Name]);
end;

procedure TButtonGroup.ReleaseNonLatching;
var I : Integer;
    B : TAbstractPanelButton;
begin
  for I := 0 to List.Count - 1 do begin
    B := TAbstractPanelButton(List[I]);
    if not B.Latched then begin
      B.Down := False;
      ButtonRelease(B);
    end;
  end;
end;

procedure TButtonGroup.SetEnabled(AValue: Boolean);
begin
  SetEnableState(AValue);
end;

procedure TButtonGroup.MatchDimensions;
var B : TAbstractPanelButton;
begin
  if List.Count > 0 then begin
    B := TAbstractPanelButton(List[0]);
    Width := (B.Width + Gap) * Horizontal;
    Height := (B.Height + Gap) * Vertical
  end;
end;

procedure TButtonGroup.SetEnableState(AValue: Boolean);
var B : TAbstractPanelButton;
    I : Integer;
begin
  for I := 0 to List.Count - 1 do begin
    B := TAbstractPanelButton(List[I]);
    if (B.Keyword <> '') then begin
      B.Permissive := (SysObj.Keywords.Enable[B.Keyword] > 0) and AValue;
    end else begin
      B.Permissive := AValue;
    end;
  end;
end;

procedure TButtonGroup.UpdateLamps;
var I, Tmp : Integer;
    B : TAbstractPanelButton;
begin
  for I := 0 to List.Count - 1 do begin
    B:= TAbstractPanelButton(List[I]);
    Tmp := B.LightTag.AsInteger;
    Tmp := Tmp and B.LightMask;
    if Tmp <> B.Status then
      B.Status := Tmp;
  end;
end;

constructor TPanelWidgetDLL.Create(aOwner : TCNC; aLib, aName : string);
  function GetProcAddressX(hModule: HMODULE; lpProcName: LPCSTR): FARPROC;
  begin
    Result := GetProcAddress(hModule, lpProcName);
    if not Assigned(Result) then
      raise Exception.CreateFmt('Library interface missing [%s]', [lpProcName]);
  end;
begin
  inherited Create;
  Owner := aOwner;
  FLibName := aLib;
  FDeviceName := aName;
  Events := TStringList.Create;
  SEvents := TStringList.Create;
  Publish := TStringList.Create;
  LibCode := SysObj.Comms.FindPlugin(LibName); // LoadLibrary(PChar(ProfilePath + 'lib/' + LibName));
  if Libcode = 0 then
    raise ECNCInstallFault.CreateFmt('DLL [%s] is not declared in the plugins section of CNC.ini', [LibName]);

  WidgetOpen := GetProcAddressX(LibCode, 'ACNCWidgetOpen');
  WidgetClose := GetProcAddressX(LibCode, 'ACNCWidgetClose');
  WidgetSetHDC := GetProcAddressX(LibCode, 'ACNCWidgetSetHDC');
  WidgetSetBounds := GetProcAddressX(LibCode, 'ACNCWidgetSetBounds');
  WidgetSetProperty := GetProcAddressX(LibCode, 'ACNCWidgetSetProperty');
  WidgetPaint := GetProcAddressX(LibCode, 'ACNCWidgetPaint');
  WidgetMouseDown := GetProcAddressX(LibCode, 'ACNCWidgetMouseDown');
  WidgetMouseUp := GetProcAddressX(LibCode, 'ACNCWidgetMouseUp');

  WidgetSetIntegerInput := GetProcAddressX(LibCode, 'ACNCWidgetSetIntegerInput');
  WidgetSetStringInput := GetProcAddressX(LibCode, 'ACNCWidgetSetStringInput');
  WidgetSetCallback := GetProcAddressX(LibCode, 'ACNCWidgetSetCallback');
  WidgetGetProperties := GetProcAddressX(LibCode, 'ACNCWidgetGetProperties');

  PB := TPaintBox.Create(aOwner);
  PB.Parent := aOwner;
  PB.OnPaint := DoRepaint;
  PB.OnMouseDown := DoMouseDown;
  PB.OnMouseUp := DoMouseUp;
  PB.Left := 150;
  PB.Top := 150;
  PB.Width := 40;
  PB.Height := 70;
  PB.Visible := True;

  LibH := WidgetOpen(PChar(aName));
  DLLList.Add(Self);
  WidgetSetHDC(LibH, PB.Canvas.Handle);
end;

function TPanelWidgetDLL.SetProperty(aName, aValue : string) : Boolean;
begin
  Result := WidgetSetProperty(LibH, PB.Canvas.Handle, PChar(aName), PChar(aValue));
  DoRepaint(PB);
end;

procedure TPanelWidgetDLL.SetWidgetBounds(aRect : TRect);
begin
  PB.Left := aRect.Left;
  PB.Top := aRect.Top;
  PB.Width := aRect.Right;
  PB.Height := aRect.Bottom;

  WidgetSetBounds(LibH, PB.Canvas.Handle, PB.Width, PB.Height);
end;

function TPanelWidgetDLL.GetProperties : string;
begin
  Result := WidgetGetProperties(LibH);
end;

function TPanelWidgetDLL.LibName : string;
begin
  Result := FLibName;
end;

function TPanelWidgetDLL.DeviceName : string;
begin
  Result := FDeviceName;
end;

function TPanelWidgetDLL.Dims : TRect;
begin
  Result.Left := PaintBox.Left;
  Result.Top := PaintBox.Top;
  Result.Right := PaintBox.Width;
  Result.Bottom := PaintBox.Height;
end;

function TPanelWidgetDLL._AddRef : Integer;
begin
  Result := -1;
end;

function TPanelWidgetDLL._Release : Integer;
begin
  Result := -1;
end;

destructor TPanelWidgetDLL.Destroy;
begin
  if Assigned(WidgetClose) then
    WidgetClose(LibH);
{  if LibCode <> 0 then
    FreeLibrary(LibCode); }
  inherited;
end;

procedure TPanelWidgetDLL.DoRepaint;
begin
//  PB.Canvas.Rectangle(0, 0, 100, 100);
//  WidgetSetHDC(LibH, PB.Canvas.Handle);
  if Assigned(WidgetPaint) then
    WidgetPaint(LibH, PB.Canvas.Handle);
end;

procedure TPanelWidgetDLL.DoMouseDown(Sender : TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  if Assigned(WidgetMouseDown) then
    WidgetMouseDown(LibH, PB.Canvas.Handle, Ord(Button), Byte(Shift), X, Y);
end;

procedure TPanelWidgetDLL.DoMouseUp(Sender : TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  if Assigned(WidgetMouseUp) then
    WidgetMouseUp(LibH, PB.Canvas.Handle, Ord(Button), Byte(Shift), X, Y);
end;

procedure TPanelWidgetDLL.UpdateInteger(aTag : TAbstractTag);
var I : Integer;
begin
  for I := 0 to Events.Count - 1 do begin
    if Events.Objects[I] = aTag then begin
      WidgetSetHDC(LibH, PB.Canvas.Handle);
      WidgetSetIntegerInput(LibH, PChar(Events[I]), aTag.AsInteger);
    end;
  end;
end;

var
  Tmp : string;

procedure TPanelWidgetDLL.UpdateString(aTag : TAbstractTag);
var I : Integer;
begin
  for I := 0 to SEvents.Count - 1 do begin
    if SEvents.Objects[I] = aTag then begin
      WidgetSetHDC(LibH, PB.Canvas.Handle);
      Tmp := aTag.AsString;
      WidgetSetStringInput(LibH, PChar(SEvents[I]), PChar(Tmp));
    end;
  end;
end;

procedure PanelWidgetDLLEv(aHandle : THandle; aName, aValue : PChar); stdcall;
var I, J : Integer;
begin
  for I := 0 to DLLList.Count - 1 do begin
    if aHandle = TPanelWidgetDLL(DLLList[I]).LibH then begin
      with TPanelWidgetDLL(DLLList[I]) do begin
        for J := 0 to Publish.Count - 1 do begin
          if Publish[J] = aName then
            TAbstractTag(Publish.Objects[J]).AsString := aValue;
        end;
      end;
    end;
  end;
end;

procedure TPanelWidgetDLL.Read(S: TStreamTokenizer);
var  Token : string;

  procedure SetInputInteger;
  var aTag : TAbstractTag;
  begin
    S.ExpectedToken('(');
    Token := S.Read;
    S.ExpectedToken(',');
    aTag := Owner.TagEvent(S.Read, EdgeChange, TIntegerTag, UpdateInteger);
    Events.AddObject(Token, aTag);
    WidgetSetHDC(LibH, PB.Canvas.Handle);
    WidgetSetIntegerInput(LibH, PChar(Token), aTag.AsInteger);
    S.ExpectedToken(')');
  end;

  procedure SetInputString;
  var aTag : TAbstractTag;
  begin
    S.ExpectedToken('(');
    Token := S.Read;
    S.ExpectedToken(',');
    aTag := Owner.TagEvent(S.Read, EdgeChange, TStringTag, UpdateString);
    SEvents.AddObject(Token, aTag);
    WidgetSetHDC(LibH, PB.Canvas.Handle);
    WidgetSetStringInput(LibH, PChar(Token), PChar(aTag.AsString));
    S.ExpectedToken(')');
  end;

  procedure SetOutput;
  begin
    S.ExpectedToken('(');
    Token := S.Read;
    S.ExpectedToken(',');
    Publish.AddObject(Token, Owner.TagEvent(S.Read, EdgeChange, TIntegerTag, nil));
    WidgetSetCallback(LibH, PChar(Token), PanelWidgetDLLEv);
    S.ExpectedToken(')');
  end;

  procedure TrySetProperty;
  var Tmp, Send : string;
  begin
    Tmp := S.Read;
    if Tmp = '=' then begin
      if not WidgetSetProperty(LibH, PB.Canvas.Handle, PChar(Token), PChar(S.Read)) then
        raise ECNCInstallFault.CreateFmt('Unexpected token %s', [Token]);
    end else if Tmp = '(' then begin
      Send := '';
      while not S.EndOfStream and (Tmp <> ')') do begin
        Tmp := S.Read;
        if Tmp <> ')' then
          Send := Send + Tmp;
      end;

      if not WidgetSetProperty(LibH, PB.Canvas.Handle, PChar(Token), PChar(Send)) then
        raise ECNCInstallFault.CreateFmt('Unexpected token %s', [Token]);
    end;
  end;

begin
  Token := LowerCase(S.Read);
  try
    while not S.EndOfStream do begin
      if Token = 'width' then
        PB.Width := S.ReadIntegerAssign
      else if Token = 'height' then
        PB.Height := S.ReadIntegerAssign
      else if Token = 'top' then
        PB.Top := S.ReadIntegerAssign
      else if Token = 'left' then
        PB.Left := S.ReadIntegerAssign
      else if Token = 'setinputinteger' then
        SetInputInteger
      else if Token = 'setinputstring' then
        SetInputString
      else if Token = 'setoutput' then
        SetOutput
      else if Token = 'help' then
        HTTPHelp := S.ReadStringAssign
      else if Token = '}' then
        Exit
      else
        TrySetProperty;
      Token := LowerCase(S.Read);
    end;
  finally
    WidgetSetBounds(LibH, PB.Canvas.Handle, PB.Width, PB.Height);
  end;
end;


procedure TCustomMachinePanel.CellChange(Table: IACNC32StringArray; Row,
  Col: Integer);
begin
  DataChange:= True;
end;

procedure TCustomMachinePanel.CursorChange(Table: IACNC32StringArrayA;
  Cursor: Integer);
begin
  DataChange:= True;
end;

procedure TCustomMachinePanel.TableChange(Table: IACNC32StringArray);
begin
  DataChange:= True;
end;

function TCustomMachinePanel.GetName: WideString; stdcall;
begin
  Result:= Name;
end;


initialization
  DLLList := TList.Create;
  RegisterCNCClass(TCustomMachinePanel);
finalization
  DLLList.Free;
end.

