unit PanelLabels;

interface

uses Classes, SysUtils, Controls, CoreCNC, CNC32, CNCTypes, GenericIO, Named,
     StreamTokenizer, ExtCtrls, PanelButton, Forms, WinTypes,
     IFSDServer, Graphics, StdCtrls, IStringArray;

type
  TMappedMDTs = class
  private
    Arrays: array of IAcnc32StringArrayA;
    Client: IAcnc32StringArrayNotifyA;
    function Find(Tablename: string): IAcnc32StringArrayA;
  public
    constructor Create(AClient: IAcnc32StringArrayNotifyA);
    procedure Clean;
    function Add(Tablename: string): IAcnc32StringArrayA;
  end;

  TAcnc32DBLabel = class(TStaticText)
  private
    FTable: IAcnc32StringArrayA;
    FField: string;
    FieldIndex: Integer;
    FTagOwner: TCNC;
    procedure SetColumnIndex;
    procedure SetTable(ATable: IAcnc32StringArrayA);
    function GetTable: IAcnc32StringArrayA;
    procedure SetField(AField: string);
    function GetField: string;
  public
    BGTag: TAbstractTag;
    FGTag: TAbstractTag;
    constructor Create(AOwner: TComponent); override;
    procedure UpdateFromTable;
    procedure UpdateNamedState;

    property Field: string read GetField write SetField;
    property Table: IAcnc32StringArrayA read GetTable write SetTable;
    property BorderWidth;
    procedure Read(S: TStreamTokenizer; MappedMDTs: TMappedMDTs);
    property TagOwner: TCNC read FTagOwner write FTagOwner;
  end;

  TLabelDataType = (
    ldtNone,
    ldtInteger,
    ldtDouble,
    ldtString,
    ldtHMS
  );

  TAcnc32NamedLabel = class(TStaticText)
  private
    FTagOwner: TCNC;
    procedure UpdateTags(ATag: TAbstractTag);
  public
    BGTag: TAbstractTag;
    FGTag: TAbstractTag;
    FieldTag: TAbstractTag;

    LabelDataType: TLabelDataType;
    FormatString: string;

    Invalid: Boolean;
    constructor Create(AOwner: TComponent); override;
    property BorderWidth;
    procedure Read(S: TStreamTokenizer);
    property TagOwner: TCNC read FTagOwner write FTagOwner;
    procedure UpdateTagState;
  end;


implementation

{ TAcnc32DBLabel }

function TAcnc32DBLabel.GetField: string;
begin
  Result:= FField;
end;

function TAcnc32DBLabel.GetTable: IAcnc32StringArrayA;
begin
  Result:= FTable;
end;

procedure TAcnc32DBLabel.SetColumnIndex;
var I: Integer;
    F: WideString;
begin
  if Assigned(Table) then begin
    for I:= 0 to Table.FieldCount - 1 do begin
      Table.FieldNameAtIndex(I, F);
      if CompareText(FField, F) = 0 then begin
        FieldIndex:= I;
        Exit;
      end;
    end;
  end;
  FieldIndex:= -1;
end;

procedure TAcnc32DBLabel.SetField(AField: string);
begin
  FField:= AField;
  SetColumnIndex;
end;

procedure TAcnc32DBLabel.SetTable(ATable: IAcnc32StringArrayA);
begin
  FTable:= ATable;
  SetColumnIndex;
end;

procedure TAcnc32DBLabel.UpdateFromTable;
var Tmp: WideString;
begin
  if Assigned(Table) and (FieldIndex <> -1) then begin
    Table.GetValue(Table.Cursor, FieldIndex, Tmp);
    if Tmp <> Caption then
      Caption:= Tmp;
  end;
end;

procedure TAcnc32DBLabel.UpdateNamedState;
begin
  if Assigned(BGTag) then begin
    Color:= BGTag.AsInteger;
  end;

  if Assigned(FGTag) then begin
    Font.Color:= FGTag.AsInteger;
  end;
end;

procedure TAcnc32DBLabel.Read(S: TStreamTokenizer; MappedMDTs: TMappedMDTs);
var Token: string;
  procedure SetFont(fs: TFontStyle);
  var v: Boolean;
      c: TFontStyles;
  begin
    v:= S.ReadBooleanAssign;
    c:= Font.Style;
    if v then
      Include(c, fs)
    else
      Exclude(c, fs);

    Font.Style:= c;
  end;

  procedure LSetWidth(AWidth: Integer);
  begin
    AutoSize:= False;
    Width:= AWidth;
  end;

  procedure LSetHeight(AHeight: Integer);
  begin
    AutoSize:= False;
    Width:= AHeight;
  end;

  function AddTag(ro : Boolean) : TAbstractTag;
  var Tmp : string;
  begin
    S.ExpectedToken('=');
    Tmp := S.Read;
    Result := TagOwner.TagEvent(Tmp, EdgeChange, TIntegerTag, nil)
  end;

  procedure SetBorder(B: Integer);
  begin
    case B of
      0: BorderStyle:= sbsNone;
      1: BorderStyle:= sbsSingle;
    else
      BorderStyle:= sbsSunken;
    end;
  end;


begin
  Token := LowerCase(S.Read);
  while not S.EndOfStream do begin
    if Token = 'foreground' then
      Font.Color:= ReadColor(S)
    else if Token = 'background' then
      Color:= ReadColor(S)
    else if Token = 'mdt' then
      Table:=  MappedMDTs.Add(S.ReadStringAssign)
    else if Token = 'field' then
      Field:=  S.ReadStringAssign
    else if Token = 'bgtag' then
      BGTag:= AddTag(true)
    else if Token = 'fgtag' then
      FGTag:= AddTag(true)
    else if Token = 'text' then
      Caption:= S.ReadStringAssign
    else if Token = 'top' then
      Top:= S.ReadIntegerAssign
    else if Token = 'left' then
      Left:= S.ReadIntegerAssign
    else if Token = 'border' then
      SetBorder(S.ReadIntegerAssign)
    else if Token = 'borderwidth' then
      BorderWidth:= S.ReadIntegerAssign
    else if Token = 'transparent' then
      Transparent:= S.ReadBooleanAssign
    else if Token = 'font' then
      Font.Name:= S.ReadStringAssign
    else if Token = 'fontsize' then
      Font.Size:= S.ReadIntegerAssign
    else if Token = 'bold' then
      setFont(fsBold)
    else if Token = 'italic' then
      setFont(fsItalic)
    else if Token = 'width' then
      LsetWidth(S.ReadIntegerAssign)
    else if Token = 'height' then
      LsetHeight(S.ReadIntegerAssign)
    else if Token = '}' then
      Exit
    else
      raise ECNCInstallFault.CreateFmt('Unexpected token %s', [Token]);
    Token := LowerCase(S.Read);
  end;
  raise ECNCInstallFault.CreateFmt('Unexpected end of file %s', [Name]);
end;


constructor TAcnc32DBLabel.Create(AOwner: TComponent);
begin
  inherited;
  ParentFont:= True;
end;

{ TMappedMDTs }

function TMappedMDTs.Add(Tablename: string): IAcnc32StringArrayA;
begin
  Result:= Find(Tablename);
  if Assigned(Result) then
    Exit;

  SysObj.StringArraySet.UseArrayA(Tablename, Client, Result);
  if Assigned(Result) then begin
    SetLength(Arrays, Length(Arrays) + 1);
    Arrays[Length(Arrays) - 1] := Result;
  end;
end;

constructor TMappedMDTs.Create(AClient: IAcnc32StringArrayNotifyA);
begin
  Client:= AClient;
  SetLength(Arrays, 0);
end;

procedure TMappedMDTs.Clean;
var I: Integer;
begin
  for I:= 0 to Length(Arrays) - 1 do begin
    SysObj.StringArraySet.ReleaseArray(Client, Arrays[I]);
  end;
end;

function TMappedMDTs.Find(Tablename: string): IAcnc32StringArrayA;
var I: Integer;
begin
  for I:= 0 to Length(Arrays) - 1 do begin
    Result:= Arrays[I];
    if CompareText(Tablename, Result.Alias) = 0 then begin
       Exit;
    end;
  end;
  Result:= nil;
end;


{ TAcnc32NamedLabel }

constructor TAcnc32NamedLabel.Create(AOwner: TComponent);
begin
  inherited;
  ParentFont:= True;
  //Font.Color:= clBlack;
end;

procedure TAcnc32NamedLabel.Read(S: TStreamTokenizer);
var Token: string;
  procedure SetFont(fs: TFontStyle);
  var v: Boolean;
      c: TFontStyles;
  begin
    v:= S.ReadBooleanAssign;
    c:= Font.Style;
    if v then
      Include(c, fs)
    else
      Exclude(c, fs);

    Font.Style:= c;
  end;

  procedure LSetWidth(AWidth: Integer);
  begin
    AutoSize:= False;
    Width:= AWidth;
  end;

  procedure LSetHeight(AHeight: Integer);
  begin
    AutoSize:= False;
    Height:= AHeight;
  end;

  function AddTag(ro : Boolean; TagClass: TTagClass) : TAbstractTag;
  var Tmp : string;
  begin
    S.ExpectedToken('=');
    Tmp := S.Read;
    Result := TagOwner.TagEvent(Tmp, EdgeChange, TagClass, UpdateTags)
  end;

  procedure SetBorder(B: Integer);
  begin
    case B of
      0: BorderStyle:= sbsNone;
      1: BorderStyle:= sbsSingle;
    else
      BorderStyle:= sbsSunken;
    end;
  end;

  function ConvertLabelDataType(T: string): TLabelDataType;
  begin
    T:= LowerCase(T);
    if T = 'integer' then
      Result:= ldtInteger
    else if T = 'double' then
      Result:= ldtDouble
    else if T = 'string' then
      Result:= ldtString
    else if T = 'hms' then
      Result:= ldtHMS
    else
      Result:= ldtNone;
  end;

begin
  Token := LowerCase(S.Read);
  while not S.EndOfStream do begin
    if Token = 'foreground' then
      Font.Color:= ReadColor(S)
    else if Token = 'background' then
      Color:= ReadColor(S)
    else if Token = 'light' then
      FieldTag:= AddTag(true, TStringTag)
    else if Token = 'bgtag' then
      BGTag:= AddTag(true, TIntegerTag)
    else if Token = 'fgtag' then
      FGTag:= AddTag(true, TIntegerTag)
    else if Token = 'text' then
      Caption:= SysObj.Localized.GetString( S.ReadStringAssign )
    else if Token = 'top' then
      Top:= S.ReadIntegerAssign
    else if Token = 'left' then
      Left:= S.ReadIntegerAssign
    else if Token = 'border' then
      SetBorder(S.ReadIntegerAssign)
    else if Token = 'borderwidth' then
      BorderWidth:= S.ReadIntegerAssign
    else if Token = 'transparent' then
      Transparent:= S.ReadBooleanAssign
    else if Token = 'font' then
      Font.Name:= S.ReadStringAssign
    else if Token = 'fontsize' then
      Font.Size:= S.ReadIntegerAssign
    else if Token = 'bold' then
      setFont(fsBold)
    else if Token = 'italic' then
      setFont(fsItalic)
    else if Token = 'width' then
      LsetWidth(S.ReadIntegerAssign)
    else if Token = 'height' then
      LsetHeight(S.ReadIntegerAssign)
    else if Token = 'datatype' then
      LabelDataType := ConvertLabelDataType(S.ReadStringAssign)
    else if Token = 'formatstring' then
      FormatString := S.ReadStringAssign
    else if Token = '}' then
      Exit
    else
      raise ECNCInstallFault.CreateFmt('Unexpected token %s', [Token]);
    Token := LowerCase(S.Read);
  end;
  raise ECNCInstallFault.CreateFmt('Unexpected end of file %s', [Name]);
end;

procedure TAcnc32NamedLabel.UpdateTags(ATag: TAbstractTag);
begin
  if Assigned(BGTag) and (ATag = BGTag) then begin
    Color:= BGTag.AsInteger;
  end;

  if Assigned(FGTag) and (ATag = FGTag) then begin
    Font.Color:= FGTag.AsInteger;
  end;

  if not Invalid then begin
    if Assigned(FieldTag) and (ATag = FieldTag) then begin
      if FormatString <> '' then begin
        try
          case LabelDataType of
            ldtDouble: begin
              Caption:= Format(FormatString, [ATag.AsDouble]);
            end;

            ldtInteger: begin
              Caption:= Format(FormatString, [ATag.AsInteger]);
            end;

            ldtString: begin
              Caption:= Format(FormatString, [ATag.AsString]);
            end;

            ldtHMS: begin
              Caption:= Format(FormatString, [HMSFormat(ATag.AsDouble)]);
            end;

          else
            Caption:= FieldTag.AsString;
          end;
        except
          EventLog('Invalid Tag Format: ' + FormatString + ' for tag: ' + ATag.Name);
          Invalid:= True;
        end;
      end else
        Caption:= FieldTag.AsString;
    end;
  end;
end;

procedure TAcnc32NamedLabel.UpdateTagState;
begin
  UpdateTags(Self.BGTag);
  UpdateTags(Self.FGTag);
  UpdateTags(Self.FieldTag);
end;

end.
