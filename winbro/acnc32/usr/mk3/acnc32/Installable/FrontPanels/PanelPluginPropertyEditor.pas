unit PanelPluginPropertyEditor;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ComCtrls, CNC32, CNCTypes, WidgetTypes;

type
  TPanelPluginEditorForm = class(TForm)
    DLLListBox: TListBox;
    GroupBox1: TGroupBox;
    LeftEdit: TEdit;
    TopEdit: TEdit;
    WidthEdit: TEdit;
    HeightEdit: TEdit;
    UDLeft: TUpDown;
    UDTop: TUpDown;
    UDWidth: TUpDown;
    UDHeight: TUpDown;
    GroupBox2: TGroupBox;
    Edit2: TEdit;
    Button1: TButton;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    PropertyComboBox: TComboBox;
    StatusBar1: TStatusBar;
    procedure Button1Click(Sender: TObject);
    procedure DLLListBoxClick(Sender: TObject);
    procedure UDClick(Sender: TObject; Button: TUDBtnType);
  private
    Widgets : array of IMachinePanelWidget;
  public
    class procedure Execute(aWidgets : array of IMachinePanelWidget);
  end;

var
  PanelPluginEditorForm: TPanelPluginEditorForm;

implementation

{$R *.DFM}

class procedure TPanelPluginEditorForm.Execute(aWidgets : array of IMachinePanelWidget);
var Form : TPanelPluginEditorForm;
    I : Integer;
begin
  Form := TPanelPluginEditorForm.Create(Application);
  SetLength(Form.Widgets, Length(aWidgets));
  try
    for I := 0 to Length(aWidgets) - 1 do begin
      Form.Widgets[I] := aWidgets[I];
      Form.DLLListBox.Items.Add(aWidgets[I].LibName + '.' + aWidgets[I].DeviceName);
    end;
    if Form.DLLListBox.Items.Count > 0 then begin
      Form.DLLListBox.ItemIndex := 0;
      Form.DLLListBoxClick(Form);
    end;

    Form.ShowModal;
  finally
    Form.Free;
  end;
end;

procedure TPanelPluginEditorForm.Button1Click(Sender: TObject);
begin
  with DLLListBox do
    if ItemIndex <> -1 then begin
      Widgets[ItemIndex].SetProperty(PropertyComboBox.Text, Edit2.Text);
    end;
end;

procedure TPanelPluginEditorForm.DLLListBoxClick(Sender: TObject);
var Tmp : string;
begin
  with DLLListBox do
    if ItemIndex <> -1 then begin
      UDWidth.Position := Widgets[ItemIndex].Dims.Right;
      UDLeft.Position := Widgets[ItemIndex].Dims.Left;
      UDTop.Position := Widgets[ItemIndex].Dims.Top;
      UDHeight.Position := Widgets[ItemIndex].Dims.Bottom;

      PropertyComboBox.Items.Clear;
      Tmp := Widgets[ItemIndex].GetProperties;
      while Tmp <> '' do
        PropertyComboBox.Items.Add(ReadDelimited(Tmp, ';'));

      PropertyComboBox.ItemIndex := 0;
    end;
end;

procedure TPanelPluginEditorForm.UDClick(Sender: TObject;
  Button: TUDBtnType);
begin
  with DLLListBox do
    if ItemIndex <> -1 then begin
      Widgets[ItemIndex].SetWidgetBounds(Rect(UDLeft.Position, UDTop.Position, UDWidth.Position, UDHeight.Position));
    end;
end;

end.
