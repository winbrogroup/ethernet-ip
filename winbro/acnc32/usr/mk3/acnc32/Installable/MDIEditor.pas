unit MDIEditor;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, CoreCNC, CNCUtils, StdCtrls, IniFiles, Named, CNCTypes, CNC32,
  Term, CNCAbs, Grids, ComCtrls, InterlockEditForm, Tokenizer,
  ImgList, FaultRegistration, KBControl;

type
  TMDIEditor = class(TExpandDisplay)
  private
    Panel: TPanel;
    Accept: TButton;
    Terminal: TMemo;
    procedure AcceptClick(Sender: TObject);
    procedure TextChange(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure VerifyBuffer;
  public
    constructor Create(AOwner: TComponent); override;
    procedure InstallComponent(Params : TParamStrings); override;
  end;

implementation

{ TMDIEditor }

constructor TMDIEditor.Create(AOwner: TComponent);
begin
  inherited;
  Panel:= TPanel.Create(Self);
  Panel.Parent:= Self;
  Panel.Align:= alBottom;

  Terminal:= TMemo.Create(Self);
  Terminal.Parent:= Self;
  Terminal.Align:= alClient;
  Terminal.OnKeyUp:= TextChange;

  Accept:= TButton.Create(Self);
  Accept.Parent:= Panel;
  Accept.Align:= alRight;
  Accept.Caption:= 'Accept';
  Accept.OnClick:= AcceptClick;
end;

procedure TMDIEditor.InstallComponent(Params: TParamStrings);
begin
  inherited;
  __KBC.CreateButton( [kbAlpha, kbNumeric, kbNavigation], Self, alTop);
  Primary     := Terminal;
end;

procedure TMDIEditor.AcceptClick(Sender: TObject);
begin
  SysObj.NC.SetMDIBuffer(Terminal.Text);
  VerifyBuffer;
end;

procedure TMDIEditor.TextChange(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  VerifyBuffer;
end;

procedure TMDIEditor.VerifyBuffer;
begin
  if SysObj.NC.GetMDIBuffer = Terminal.Text then
    Terminal.Font.Color:= $C00000
  else
    Terminal.Font.Color:= $0000C0;
end;

initialization
  RegisterCNCClass(TMDIEditor, 'MDIEditor');
end.
