unit CustomReportScreen;

interface

uses
  Classes, SysUtils, CncAbs, CoreCnc, Graphics;

type
  TReportScreen = class(TExpandDisplay)
  private
    BitMap : TBitmap;
    function GetCanvas: TCanvas;
  public
    constructor Create(AOwner: TComponent); override;
    procedure InstallComponent(Params: TParamStrings); override;
    property Graphic: TCanvas read GetCanvas;
    procedure Paint; override;
    procedure Resize; override;
  end;

implementation

uses
  ReportScreenLibrary;

{ TReportScreen }

constructor TReportScreen.Create(AOwner: TComponent);
begin
  inherited;
  BitMap:= TBitMap.Create;
end;

function TReportScreen.GetCanvas: TCanvas;
begin
  Result:= BitMap.Canvas;
end;

procedure TReportScreen.InstallComponent(Params: TParamStrings);
begin
  inherited;
  TReportScreenLibrary.Create(Self);
end;

procedure TReportScreen.Paint;
begin
  Canvas.Draw(0, 0, BitMap);
end;

procedure TReportScreen.Resize;
begin
  inherited;
  BitMap.Width:= Width;
  BitMap.Height:= Height;
end;

initialization
  CoreCnc.RegisterCNCClass(TReportScreen, 'ReportScreen');
end.
