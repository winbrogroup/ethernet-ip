unit ReportScreenLibrary;

interface

uses SysUtils, Classes, CoreCnc, Cnc32, CustomReportScreen, AbstractScript, IScriptInterface, INamedInterface, ReportScreenMethods;

type
  TReportScreenLibrary = class(TInterfacedObject, IScriptLibrary)
  protected
    Screen: TReportScreen;
    InstanceMethods: array of IScriptMethod;
    FHost: IScriptHost;
    Methods: TList;
    LibName: WideString;
  public
    constructor Create(Screen: TReportScreen);
    procedure Initialise;
    procedure Reset; stdcall;
    function MethodCount: Integer; stdcall;
    function GetMethodByIndex(Index: Integer): IScriptMethod; stdcall;
    function GetMethodByName(AName: WideString): IScriptMethod; stdcall;
    function GetLibraryName: WideString; stdcall;
    procedure SetScriptHost(Host: IScriptHost); stdcall;
    property Host: IScriptHost read FHost;
    procedure AddMethod(Method: TReportScreenScriptMethodClass);
    procedure SetName(AName: WideString);
  end;

implementation

{ TReportScreenLibrary }

constructor TReportScreenLibrary.Create(Screen: TReportScreen);
begin
  Methods:= TList.Create;
  Self.Screen:= Screen;
  SetName(Screen.Name);
  AddMethod(TReportScreenPen);
  AddMethod(TReportScreenText);
  AddMethod(TReportScreenFont);
  AddMethod(TReportScreenMoveTo);
  AddMethod(TReportScreenLineTo);
  AddMethod(TReportScreenRepaint);
  AddMethod(TReportScreenBrush);
  AddMethod(TReportScreenEllipse);
  AddMethod(TReportScreenRectangle);
  AddMethod(TReportScreenWidth);
  AddMethod(TReportScreenHeight);
  AddMethod(TReportScreenRoundRectangle);
  AddMethod(TReportScreenClear);
  AddMethod(TReportScreenOpen);
  AddMethod(TReportScreenPolygon);
  AddMethod(TReportScreenFill);
  AddMethod(TReportScreenAddTable);
  AddMethod(TReportScreenAddCell);

  Initialise;

  SysObj.AddOCXLibrary(Self);
end;

procedure TReportScreenLibrary.AddMethod(Method: TReportScreenScriptMethodClass);
begin
  Methods.Add(Method);
end;

procedure TReportScreenLibrary.Initialise;
var I: Integer;
    M: TScriptMethod;
begin
  SetLength(InstanceMethods, Methods.Count);
  for I:= 0 to Methods.Count - 1 do begin
    M:= TReportScreenScriptMethodClass(Methods[I]).Create(Screen);
    InstanceMethods[I]:= M;
  end;
end;

function TReportScreenLibrary.GetMethodByIndex(Index: Integer): IScriptMethod;
begin
  if Index < Length(InstanceMethods) then begin
    Result:= InstanceMethods[Index];
  end else
    raise Exception.Create('Method index too large');
end;

function TReportScreenLibrary.GetMethodByName(AName: WideString): IScriptMethod;
var I: Integer;
begin
  for I:= 0 to Length(InstanceMethods) - 1 do begin
    if UpperCase(AName) = UpperCase(InstanceMethods[I].Name) then begin
      Result:= InstanceMethods[I];
      Exit;
    end;
  end;
  Result:= nil;
end;

function TReportScreenLibrary.MethodCount: Integer;
begin
  Result:= Methods.Count;
end;

function TReportScreenLibrary.GetLibraryName: WideString;
begin
  Result:= LibName;
end;

procedure TReportScreenLibrary.Reset;
var I: Integer;
begin
  for I:= 0 to MethodCount - 1 do begin
    InstanceMethods[I].Reset;
  end;
end;

procedure TReportScreenLibrary.SetName(AName: WideString);
begin
  LibName:= UpperCase(AName);
end;

procedure TReportScreenLibrary.SetScriptHost(Host: IScriptHost);
var I: Integer;
begin
  FHost:= Host;
  for I:= 0 to Length(InstanceMethods) - 1 do begin
    InstanceMethods[I].SetScriptHost(Host);
  end;
end;


end.
