unit ReportScreenMethods;

interface

uses
  Classes, SysUtils, Cnc32, CustomReportScreen, AbstractScript, Graphics, Windows, ReportScreenTable;

type
  TReportScreenScriptMethodClass = class of TReportScreenScriptMethod;
  TReportScreenScriptMethod = class(TScriptMethod)
  private
    Screen: TReportScreen;
  public
    constructor Create(Screen: TReportScreen); virtual;
  end;

  TReportScreenPen = class (TReportScreenScriptMethod)
  public
    function Name: WideString; override;
    function ParameterCount: Integer; override;
    procedure Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer); override;
  end;

  TReportScreenFont = class (TReportScreenScriptMethod)
  public
    function Name: WideString; override;
    function ParameterCount: Integer; override;
    procedure Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer); override;
  end;

  TReportScreenText = class (TReportScreenScriptMethod)
  public
    function Name: WideString; override;
    function ParameterCount: Integer; override;
    procedure Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer); override;
  end;

  TReportScreenMoveTo = class (TReportScreenScriptMethod)
  public
    function Name: WideString; override;
    function ParameterCount: Integer; override;
    procedure Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer); override;
  end;

  TReportScreenLineTo = class (TReportScreenScriptMethod)
  public
    function Name: WideString; override;
    function ParameterCount: Integer; override;
    procedure Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer); override;
  end;

  TReportScreenRepaint = class (TReportScreenScriptMethod)
  public
    function Name: WideString; override;
    function ParameterCount: Integer; override;
    procedure Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer); override;
  end;

  TReportScreenBrush = class (TReportScreenScriptMethod)
  public
    function Name: WideString; override;
    function ParameterCount: Integer; override;
    procedure Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer); override;
  end;

  TReportScreenEllipse = class (TReportScreenScriptMethod)
  public
    function Name: WideString; override;
    function ParameterCount: Integer; override;
    procedure Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer); override;
  end;

  TReportScreenRectangle = class (TReportScreenScriptMethod)
  public
    function Name: WideString; override;
    function ParameterCount: Integer; override;
    procedure Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer); override;
  end;

  TReportScreenWidth = class (TReportScreenScriptMethod)
  public
    function Name: WideString; override;
    function ParameterCount: Integer; override;
    function IsFunction: Boolean; override;
    procedure Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer); override;
  end;

  TReportScreenHeight = class (TReportScreenScriptMethod)
  public
    function Name: WideString; override;
    function ParameterCount: Integer; override;
    function IsFunction: Boolean; override;
    procedure Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer); override;
  end;

  TReportScreenRoundRectangle = class (TReportScreenScriptMethod)
  public
    function Name: WideString; override;
    function ParameterCount: Integer; override;
    procedure Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer); override;
  end;

  TReportScreenClear = class (TReportScreenScriptMethod)
  public
    function Name: WideString; override;
    function ParameterCount: Integer; override;
    procedure Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer); override;
  end;

  TReportScreenOpen = class (TReportScreenScriptMethod)
  public
    function Name: WideString; override;
    function ParameterCount: Integer; override;
    procedure Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer); override;
  end;

  TReportScreenPolygon = class (TReportScreenScriptMethod)
  public
    function Name: WideString; override;
    function ParameterCount: Integer; override;
    procedure Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer); override;
  end;

  TReportScreenFill = class (TReportScreenScriptMethod)
   public
    function Name: WideString; override;
    function ParameterCount: Integer; override;
    procedure Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer); override;
  end;

  TReportScreenAddTable = class (TReportScreenScriptMethod)
  private
    Tables: TList;
  public
    constructor Create(Screen: TReportScreen); override;
    function Name: WideString; override;
    function ParameterCount: Integer; override;
    procedure Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer); override;
    function IsFunction: Boolean; override;
  end;

  TReportScreenAddCell = class (TReportScreenScriptMethod)
   public
    function Name: WideString; override;
    function ParameterCount: Integer; override;
    procedure Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer); override;
  end;

var
  TableManager: TReportScreenTableManager;

implementation

function ReadColor(TmpStr: string) : TColor;
begin
  try
    Result := StrToInt(TmpStr);
    Exit;
  except
  end;

  if not identtocolor('cl' + TmpStr, Integer(Result)) then begin
     raise Exception.Create('Can''t interpret color: ' + TmpStr);
  end;
end;

{ TReportScreenScriptMethod }

constructor TReportScreenScriptMethod.Create(Screen: TReportScreen);
begin
  inherited Create;
  Self.Screen:= Screen;
end;

{ TReportScreenPen }

function TReportScreenPen.Name: WideString;
begin
  Result:= 'Pen';
end;

function TReportScreenPen.ParameterCount: Integer;
begin
  Result:= 3;
end;

procedure TReportScreenPen.Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer);
begin
  Screen.Graphic.Pen.Color:= ReadColor(TAbstractTag(Params[0]).AsString);
  Screen.Graphic.Pen.Width:= TAbstractTag(Params[1]).AsInteger;
  Screen.Graphic.Pen.Style:= TPenStyle(TAbstractTag(Params[2]).AsInteger);
end;

{ TReportScreenText }

function TReportScreenText.Name: WideString;
begin
  Result:= 'Text'
end;

function TReportScreenText.ParameterCount: Integer;
begin
  Result:= 3;
end;

procedure TReportScreenText.Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer);
var X, Y: Integer;
    Tmp: string;
begin
  X:= TAbstractTag(Params[0]).AsInteger;
  Y:= TAbstractTag(Params[1]).AsInteger;
  Tmp:= TAbstractTag(Params[2]).AsString;

  Screen.Graphic.TextOut(X, Y, Tmp);
end;

{ TReportScreenFont }

function TReportScreenFont.Name: WideString;
begin
  Result:= 'Font';
end;

function TReportScreenFont.ParameterCount: Integer;
begin
  Result:= 3;
end;

procedure TReportScreenFont.Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer);
var Tmp: string;
begin
  Screen.Graphic.Font.Name:= TAbstractTag(Params[0]).AsString;
  Tmp:= TAbstractTag(Params[1]).AsString;
  Screen.Graphic.Font.Color:= ReadColor(Tmp);
  Screen.Graphic.Font.Height:= TAbstractTag(Params[2]).AsInteger;
end;

{ TReportScreenMoveTo }

function TReportScreenMoveTo.Name: WideString;
begin
  Result:= 'Moveto';
end;

function TReportScreenMoveTo.ParameterCount: Integer;
begin
  Result:= 2;
end;

procedure TReportScreenMoveTo.Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer);
begin
  Screen.Graphic.MoveTo(TAbstractTag(Params[0]).AsInteger, TAbstractTag(Params[1]).AsInteger);
end;

{ TReportScreenLineTo }

function TReportScreenLineTo.Name: WideString;
begin
  Result:= 'Lineto';
end;

function TReportScreenLineTo.ParameterCount: Integer;
begin
  Result:= 2;
end;

procedure TReportScreenLineTo.Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer);
begin
  Screen.Graphic.LineTo(TAbstractTag(Params[0]).AsInteger, TAbstractTag(Params[1]).AsInteger);
end;

{ TReportScreenRepaint }

function TReportScreenRepaint.Name: WideString;
begin
  Result:= 'Repaint';
end;

function TReportScreenRepaint.ParameterCount: Integer;
begin
  Result:= 0
end;

procedure TReportScreenRepaint.Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer);
begin
  Screen.Invalidate;
end;

{ TReportScreenBrush }

function TReportScreenBrush.Name: WideString;
begin
  Result:= 'Brush';
end;

function TReportScreenBrush.ParameterCount: Integer;
begin
  Result:= 2;
end;

procedure TReportScreenBrush.Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer);
begin
  Screen.Graphic.Brush.Color:= ReadColor(TAbstractTag(Params[0]).AsString);
  Screen.Graphic.Brush.Style:= TBrushStyle(TAbstractTag(Params[1]).AsInteger);
end;

{ TReportScreenCircle }

function TReportScreenEllipse.Name: WideString;
begin
  Result:= 'Ellipse';
end;

function TReportScreenEllipse.ParameterCount: Integer;
begin
  Result:= 4;
end;

procedure TReportScreenEllipse.Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer);
begin
  Screen.Graphic.Ellipse(TAbstractTag(Params[0]).AsInteger, TAbstractTag(Params[1]).AsInteger, TAbstractTag(Params[2]).AsInteger, TAbstractTag(Params[3]).AsInteger);
end;

{ TReportScreenRectangle }

function TReportScreenRectangle.Name: WideString;
begin
  Result:= 'Rectangle'
end;

function TReportScreenRectangle.ParameterCount: Integer;
begin
  Result:= 4;
end;

procedure TReportScreenRectangle.Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer);
begin
  Screen.Graphic.Rectangle(TAbstractTag(Params[0]).AsInteger, TAbstractTag(Params[1]).AsInteger, TAbstractTag(Params[2]).AsInteger, TAbstractTag(Params[3]).AsInteger);
end;

{ TReportScreenWidth }

function TReportScreenWidth.Name: WideString;
begin
  Result:= 'Width';
end;

function TReportScreenWidth.ParameterCount: Integer;
begin
  Result:= 0;
end;

procedure TReportScreenWidth.Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer);
begin
  TAbstractTag(Res).AsInteger:= Screen.Width;
end;

function TReportScreenWidth.IsFunction: Boolean;
begin
  Result:= True;
end;

{ TReportScreenHeight }

function TReportScreenHeight.Name: WideString;
begin
  Result:= 'Height'
end;

function TReportScreenHeight.ParameterCount: Integer;
begin
  Result:= 0;
end;

function TReportScreenHeight.IsFunction: Boolean;
begin
  Result:= True;
end;

procedure TReportScreenHeight.Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer);
begin
  TAbstractTag(Res).AsInteger:= Screen.Height;
end;

{ TReportScreenRoundRectangle }

function TReportScreenRoundRectangle.Name: WideString;
begin
  Result:= 'RoundRect';
end;

function TReportScreenRoundRectangle.ParameterCount: Integer;
begin
  Result:= 6;
end;

procedure TReportScreenRoundRectangle.Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer);
begin
  Screen.Graphic.RoundRect(TAbstractTag(Params[0]).AsInteger, TAbstractTag(Params[1]).AsInteger, TAbstractTag(Params[2]).AsInteger, TAbstractTag(Params[3]).AsInteger, TAbstractTag(Params[4]).AsInteger, TAbstractTag(Params[5]).AsInteger);
end;

{ TReportScreenRoundClear }

function TReportScreenClear.Name: WideString;
begin
  Result:= 'Clear';
end;

function TReportScreenClear.ParameterCount: Integer;
begin
  Result:= 0;
end;

procedure TReportScreenClear.Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer);
var Rect: TRect;
begin
  Rect.Left:= 0;
  Rect.Top:= 0;
  Rect.Right:= Screen.Width;
  Rect.Bottom:= Screen.Height;
  Screen.Graphic.FillRect(Rect);

  TableManager.RemoveTables(Screen);
end;

{ TReportScreenOpen }

function TReportScreenOpen.Name: WideString;
begin
  Result:= 'Open';
end;

function TReportScreenOpen.ParameterCount: Integer;
begin
  Result:= 0;
end;

procedure TReportScreenOpen.Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer);
begin
  Screen.Open;
end;

{ TReportScreenPolygon }

function TReportScreenPolygon.Name: WideString;
begin
  Result:= 'Polygon'
end;

function TReportScreenPolygon.ParameterCount: Integer;
begin
  Result:=5;
end;

procedure TReportScreenPolygon.Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer);
var Points: array[0..50] of TPoint;
    X, Y, I, Sides: Integer;
    Angle, Radius, Theta: Double;
begin
  X:= TAbstractTag(Params[0]).AsInteger;
  Y:= TAbstractTag(Params[1]).AsInteger;
  Sides:= TAbstractTag(Params[2]).AsInteger;
  Radius:= TAbstractTag(Params[3]).AsDouble;
  Angle:= TAbstractTag(Params[4]).AsDouble * PI / 180;

  Theta:= 2 * PI / Sides;
  for I:= 0 to Sides - 1 do begin
    Points[I].X:= Round(Cos(Angle) * Radius + X);
    Points[I].Y:= Round(Sin(Angle) * Radius + Y);
    Angle:= Angle + Theta;
  end;

  Screen.Graphic.Polygon(Slice(Points, Sides));
end;

{ TReportScreenFill }

function TReportScreenFill.Name: WideString;
begin
  Result:= 'Fill';
end;

function TReportScreenFill.ParameterCount: Integer;
begin
  Result:= 2
end;

procedure TReportScreenFill.Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer);
Var
    X, Y, C: Integer;

begin
  X:= TAbstractTag(Params[0]).AsInteger;
  Y:= TAbstractTag(Params[1]).AsInteger;
  C:= Screen.Graphic.Pixels[X, Y];

  Screen.Graphic.FloodFill(X,Y,C, fsSurface);

end;

{ TReportScreenTable }

function TReportScreenAddTable.Name: WideString;
begin

  Result:= 'AddTable'

end;

function TReportScreenAddTable.ParameterCount: Integer;
begin

  Result:= 2

end;

procedure TReportScreenAddTable.Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer);
Var X, Y: integer;
    tmp: TReportScreenTable;
begin

  X:= TAbstractTag(Params[0]).AsInteger;
  Y:= TAbstractTag(Params[1]).AsInteger;

  tmp:= TableManager.AddTable(X, Y, Screen);
  TAbstractTag(Res).AsInteger:= tmp.TableHandle;
  Tables.Add(Tmp);
end;

function TReportScreenAddTable.IsFunction: Boolean;
begin
  Result:= True
end;

constructor TReportScreenAddTable.Create(Screen: TReportScreen);
begin
  inherited;
  Tables:= TList.Create;
end;


{ TReportScreenAddCell }

function TReportScreenAddCell.Name: WideString;
begin

  Result:= 'AddCell'

end;

function TReportScreenAddCell.ParameterCount: Integer;
begin

  Result:= 5

end;

procedure TReportScreenAddCell.Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer);
var X, Y, handle: Integer;
    Text, Modifier: string;
    Tmp: TReportScreenTable;
begin

  handle:= TAbstractTag(Params[0]).AsInteger;
  X:= TAbstractTag(Params[1]).AsInteger;
  Y:= TAbstractTag(Params[2]).AsInteger;
  Text:= TAbstractTag(Params[3]).AsString;
  Modifier:= TAbstractTag(Params[4]).AsString;

  // find table, addcell and rerender to screen
  Tmp:= TableManager.FindTable(handle);
  Tmp.AddCell(X, Y, Text, Screen);
end;


initialization
  TableManager:= TReportScreenTableManager.Create;

end.
