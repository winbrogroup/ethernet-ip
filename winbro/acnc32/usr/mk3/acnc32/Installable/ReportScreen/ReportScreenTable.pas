unit ReportScreenTable;

interface

uses Classes, Cnc32, Graphics, Windows, CustomReportScreen, ExtCtrls, Forms;

const
  MAX_COL_COUNT = 100;
  MAX_ROW_COUNT = 100;

type
  TReportScreenTableCell = class;
  TReportScreenTable = class;


  // Table Manager
  TReportScreenTableManager = class
  private
    Tables: TList;
    CurrentHandle: Integer;
  public
    constructor Create;
    function AddTable(X, Y: Integer; ReportScreen: TReportScreen): TReportScreenTable;
    function FindTable(Handle: Integer): TReportScreenTable;
    procedure RemoveTables(Screen: TReportScreen);
  end;

  // Table Class
  TReportScreenTable = class(TCustomPanel)
  private
    FTableHandle: Integer;
    Screen: TReportScreen;
    TopLeft: TPoint;
    Cells: array [0..MAX_COL_COUNT, 0..MAX_ROW_COUNT] of TReportScreenTableCell;
    ColWidth: array[0..MAX_COL_COUNT] of Integer;
    RowHeight: array[0..MAX_ROW_COUNT] of Integer;
    HGap, VGap: Integer;
    PenColor: TColor;
    PenWidth: Integer;
    PenStyle: TPenStyle;
    BrushColor: TColor;
    BrushStyle: TBrushStyle;
    MaxX, MaxY: Integer;
    XInset: Integer;
    YInset: Integer;
  public
    constructor Create(AOwner: TComponent); override;
    procedure SetProperties(X, Y: Integer; ReportScreen: TReportScreen);
    procedure AddCell (X, Y: Integer; Text: WideString; ReportScreen: TReportScreen);
    property TableHandle: Integer read FTableHandle;
    procedure Paint; override;
  end;

  // Cell Class
  TReportScreenTableCell = class
  private
    Text: string;
    FontName: string;
    FontHeight: Integer;
    FontColor: TColor;
    BrushColor: TColor;
    BrushStyle: TBrushStyle;
    XSpan: Integer;
    YSpan: Integer;

    Size: TSize;
  public
    constructor Create(AText: string; ReportScreen: TReportScreen);
  end;


implementation


{ TReportScreenTableManager }

constructor TReportScreenTableManager.Create;
begin
  Tables:= TList.Create;
  Self.CurrentHandle:= 1;
end;

function TReportScreenTableManager.AddTable(X, Y: Integer; ReportScreen: TReportScreen): TReportScreenTable;
begin
  Result:= TReportScreenTable.Create(ReportScreen);
  Result.SetProperties(X, Y, ReportScreen);
  Result.SetParent(ReportScreen);
  Result.FTableHandle:= CurrentHandle;
  Result.Left:= X;
  Result.Top:= Y;


  Tables.Add(Result);
  Inc(CurrentHandle);
end;

function TReportScreenTableManager.FindTable(Handle: Integer): TReportScreenTable;
var I: Integer;
begin
  // place code here to find and return correct table object

  for I:= 0 to Tables.Count - 1 do begin
    Result:= Tables[I];
    if Result.TableHandle = Handle then begin
       Exit;
    end;
  end;
end;

procedure TReportScreenTableManager.RemoveTables(Screen: TReportScreen);
var I: Integer;
begin
  // place code here to find and return correct table object
  if Windows.GetCurrentThreadId = System.MainThreadID then begin
    for I:= 0 to Tables.Count - 1 do begin
      if Screen = TReportScreenTable(Tables[I]).Screen then begin
         TReportScreenTable(Tables[I]).Parent:= nil;
         TReportScreenTable(Tables[I]).Free;
         Tables.Delete(I);
         Exit;
      end;
    end;
  end;
end;


{ TReportScreenTable }

constructor TReportScreenTable.Create(AOwner: TComponent);
begin
  inherited;
  HGap:= 1;
  VGap:= 1;
  XInset:= 8;
  YInset:= 8;
end;


procedure TReportScreenTable.SetProperties(X, Y: Integer; ReportScreen: TReportScreen);
begin
  Screen:= ReportScreen;
  TopLeft.X:= X;
  TopLeft.Y:= Y;
  PenColor:= ReportScreen.Graphic.Pen.Color;
  PenWidth:= ReportSCreen.Graphic.Pen.Width;
  PenStyle:= ReportScreen.Graphic.Pen.Style;
  BrushStyle:= ReportScreen.Graphic.Brush.Style;
  BrushColor:= ReportScreen.Graphic.Brush.Color;
end;

// add cell
procedure TReportScreenTable.AddCell(X, Y: Integer; Text: WideString; ReportScreen: TReportScreen);
var I, J, W, H: Integer;
begin
  Cells [X,Y]:= TReportScreenTableCell.Create(Text,ReportScreen);
  if X > MaxX then MaxX:= X;
  if Y > MaxY then MaxY:= Y;

  // determine row heights
  if Cells[X,Y].Size.Cx > ColWidth[X] then
    ColWidth[X]:= Cells[X,Y].Size.Cx;

  // determine column widths
  if Cells[X,Y].Size.Cy > RowHeight[Y] then
     RowHeight[Y]:= Cells[X,Y].Size.cy;

  W:= HGap;
  for I:= 0 to MaxX do begin
    W:= W + ColWidth[I] + (2 * XInset) + HGap;
  end;

  H:= VGap;
  for J:= 0 to MaxY do begin
    H:= H + RowHeight[J] + (2 * YInset) + VGap;
  end;

  Width:= W;
  Height:= H;
end;


procedure TReportScreenTable.Paint;
var I, J, XSum, YSum, XSize, YSize: Integer;
begin
  // set pen properties
  Screen.Graphic.Pen.Color:= PenColor;
  Screen.Graphic.Pen.Width:= PenWidth;
  Screen.Graphic.Pen.Style:= PenStyle;

  XSum:= HGap;
  for I:= 0 to MaxX do begin
    YSum:= VGap;
    for J:= 0 to MaxY do begin
      if Assigned(Cells[I,J]) then begin
        Canvas.Brush.Color:= Cells[I,J].BrushColor;
        Canvas.Font.Color:= Cells[I,J].FontColor;
        Canvas.Font.Height:= Cells[I,J].FontHeight;
        Canvas.Font.Name:= Cells[I,J].FontName;
      end else begin
        Canvas.Brush.Color:= BrushColor;
        Canvas.Brush.Style:= BrushStyle;
      end;
      Canvas.Rectangle(XSum, YSum, XSum + ColWidth[I] + (2 * XInset), YSum + RowHeight[J] + (2 * YInset));
      if Assigned(Cells[I,J]) then begin
        Canvas.TextOut(XSum + XInset, YSum + YInset, Cells[I, J].Text);
      end;
      YSum:= YSum + RowHeight[J] + (2 * YInset)+ VGap;
     end;
    XSum:= XSum + ColWidth[I] + (2 * XInset) + HGap;
  end;
end;

{ TReportScreenTableCell }

constructor TReportScreenTableCell.Create(AText: string; ReportScreen: TReportScreen);
begin
  Text:= AText;
  FontName:= ReportScreen.Graphic.Font.Name;
  FontHeight:= ReportScreen.Graphic.Font.Height;
  FontColor:= ReportScreen.Graphic.Font.Color;
  BrushColor:= ReportScreen.Graphic.Brush.Color;
  BrushStyle:= ReportScreen.Graphic.Brush.Style;
  XSpan:= 1;
  YSpan:= 1;

  Size:= ReportScreen.Graphic.TextExtent(Text);
end;

end.
