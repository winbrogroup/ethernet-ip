unit StdEditor;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  AbstractEditor, StdCtrls, Buttons, ExtCtrls, CompoundFiles, CoreCNC, Editor, NCNames;

type
  TStdEditorForm = class(TForm)
    Panel1: TPanel;
    OK: TBitBtn;
    Cancel: TBitBtn;
    CursorLabel: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormActivate(Sender: TObject);
  private
    StdEditor : TStdEditor;
    procedure DoEditClose(Sender : TObject);
    procedure DoCursorMove(Sender : TObject);
  public
  end;

  TStandardEditor = class(TObject)
  private
    FSectionName : string;
  public
    function Execute(Manager: TCompoundFile): Boolean;
    property ThisSectionName : string read FSectionName write FSectionName;
  end;

  TNCTextEditor = class(TSectionEditor)
  public
    function Execute(Manager: TCompoundFile): Boolean; override;
    function SectionName: string; override;
  end;

var
  StdEditorForm: TStdEditorForm;

implementation

{$R *.DFM}

function TNCTextEditor.Execute(Manager: TCompoundFile): Boolean;
var  STEF : TStdEditorForm;
begin
  Result:= false;
  STEF := TStdEditorForm.Create(Application);
  try
    try
      Manager.SectionOpen(SectionName, STEF.StdEditor.Text);
      Result:= STEF.ShowModal = mrOK;
      Manager.SectionClose(STEF.StdEditor.Text, Result);
    except
      on E: ECompoundFileError do
        Application.ShowException(E)
    end
  finally
    STEF.Free
  end
end;

function TNCTextEditor.SectionName : string;
begin
  Result := NC32GCodeSection;
end;

function TStandardEditor.Execute(Manager: TCompoundFile): Boolean;
var  STEF : TStdEditorForm;
begin
  Result:= false;
  STEF := TStdEditorForm.Create(Application);
  try
    try
      Manager.SectionOpen(ThisSectionName, STEF.StdEditor.Text);
      Result:= STEF.ShowModal = mrOK;
      Manager.SectionClose(STEF.StdEditor.Text, Result);
    except
      on E: ECompoundFileError do
        Application.ShowException(E)
    end
  finally
    STEF.Free
  end
end;

procedure TStdEditorForm.FormCreate(Sender: TObject);
begin
  StdEditor := TStdEditor.Create(Self);
  StdEditor.Parent := Self;
  StdEditor.Align := alClient;
  StdEditor.Font.Name := 'Courier New';
  StdEditor.Font.Size := 12;
  StdEditor.Font.Style := [fsBold, fsItalic];
  StdEditor.OnCursorMove := DoCursorMove;
  StdEditor.OnEscape := DoEditClose;
end;

procedure TStdEditorForm.DoCursorMove(Sender : TObject);
begin
  CursorLabel.Caption := Format('Col[%d]:Line[%d]', [StdEditor.CursorPos.X, StdEditor.CursorPos.Y]);
end;

procedure TStdEditorForm.DoEditClose(Sender : TObject);
begin
  ModalResult := mrCancel;
end;

procedure TStdEditorForm.FormActivate(Sender: TObject);
begin
  StdEditor.SetFocus;
end;

initialization
  RegisterCNCClass(TNCTextEditor);
end.
