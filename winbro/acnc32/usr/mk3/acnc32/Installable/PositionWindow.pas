unit PositionWindow;

interface

uses CoreCNC, CNCTypes, CNC32, Classes, Windows, Graphics, SysUtils, NCNames,
     CNCAbs, Named, Controls;

type
  TClassicPositionWindow = class(TExpandDisplay)
  private
    PositionList : TList;
    FontTable : array [0..72] of TPoint;
    LargeFontSize, SmallFontSize : Integer;
    TitleColour : TColor;
    FPositionColor : TColor;
    FHighlightColor : TColor;
    LeftPos1 : Integer;
    LeftPos2 : Integer;
    MidHeight : Integer;
    Values : array [0..15, TPositionType] of Double;
    ForceRedraw : Boolean;
    RequestAxis : TAbstractTag;
    Last : TPoint;
    Disabled : Boolean;
    NDC : Boolean;
    GGroups : TArrayTag;
    CharactersDisplayed : Integer;
    CurrentMode : Integer;
    LocalDisplay : string;
    LocalMachine : string;
    LocalTarget : string;
    LocalDistToGo : string;
    LocalLag : string;
    InchModeTag: TAbstractTag;

    procedure DrawDisplayPosition;
    function FindFontSize(aWidth, aHeight : Integer) : Integer;
    function NumFormat(X, Y : Integer; ReqAN : Boolean; Index : Integer; pt : TPositionType) : string;
    procedure UpdateDisplay(aTag : TAbstractTag);
    procedure AxisSelectChange(aTag : TAbstractTag);
    procedure DoMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure DoAxisSelect(Sender: TObject);
    function GetHighLightColor : TColor;
    function GetPositionColor : TColor;
    procedure DisableDisplay(aTag : TAbstractTag);
  protected
    procedure Resize; override;
    procedure Paint; override;
  public
    constructor Create(aOwner : TComponent); override;
    procedure InstallComponent(Params : TParamStrings); override;
    procedure Installed; override;
    procedure Repaint; override;
    property PositionColor : TColor read GetPositionColor write FPositionColor;
    property HighlightColor : TColor read GetHighLightColor write FHighLightColor;
  end;

implementation

const
  LeftFrame = 45;
  RightFrame = 100 - LeftFrame;
  CharactersDisplayedMM = 13;
  CharactersDisplayedIN = 14;

  MainTitleHeight = 30;

constructor TClassicPositionWindow.Create(aOwner : TComponent);
begin
  inherited Create(aOwner);
  PositionList := TList.Create;
  CharactersDisplayed := CharactersDisplayedMM;
end;

procedure TClassicPositionWindow.InstallComponent(Params : TParamStrings);
var I : Integer;
begin
  inherited InstallComponent(Params);
  ParentFont := False;
  Font.Style := [fsBold];
  Font.Name := Sysobj.MonoFontName;
  //Font.Name := 'Courier New';
  //Font.Name := 'Wingdings';
  Canvas.Font.Assign(Font);

//  Canvas.TextFlags := ETO_OPAQUE;
  OnMouseDown := DoMouseDown;
  if not NDC then
    OnDblClick := DoAxisSelect;


  for I := 1 to 72 do begin
    Canvas.Font.Height := I;
    FontTable[I].Y := Canvas.TextHeight('0');
    FontTable[I].X := Canvas.TextWidth('0');
  end;

  TitleColour := Params.ReadColour('TitleColour', clBlack);
  FPositionColor := Params.ReadColour('PositionColour', clBlack);
  FHighLightColor := Params.ReadColour('HighlightColor', clBlue);
  NDC := Params.ReadBool('NDC', False);

   with NCEventF[ncdeReqAxisSelect] do
    RequestAxis := SysObj.Comms.TagPublish(Format(Tag, [1]), Size);
end;

procedure TClassicPositionWindow.DoMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  Last.X := X;
  Last.Y := Y;
end;

procedure TClassicPositionWindow.DoAxisSelect(Sender: TObject);
begin
  if Last.X < Width div 2 then begin
    RequestAxis.AsInteger := ((Last.Y - MainTitleHeight) * SysObj.NC.AxisCount) div (Height - MainTitleHeight);
  end;
end;

function TClassicPositionWindow.GetHighLightColor : TColor;
begin
  if Disabled then
    Result := clGray
  else
    Result := FHighLightColor;
end;

function TClassicPositionWindow.GetPositionColor : TColor;
begin
  if Disabled then
    Result := clGray
  else
    Result := FPositionColor;
end;


procedure TClassicPositionWindow.Installed;
var I : Integer;
begin
  inherited Installed;
  for I := 0 to SysObj.NC.AxisCount - 1 do begin
    PositionList.Add(TagEvent(Format(AxisPositionFormat, [SysObj.NC.AxisName[I]]), EdgeChange, TAxisPositionTag, nil));
  end;
  TagEvent(NameDisplaySweep, EdgeChange, TMethodTag, UpdateDisplay);
  with NCPublishedF[ncpActiveAxis] do
    TagEvent(Format(Tag, [1]), EdgeChange, Size, AxisSelectChange);

  InchModeTag:= TagEvent('NC1InchModeDefault', EdgeChange, TIntegerTag, nil);

  TagEvent(Name + 'Disable', EdgeChange, TMethodTag, DisableDisplay);
  GGroups := TArrayTag(TagEvent(GGroupName, EdgeChange, TArrayTag, nil));
  LocalDisplay := SysObj.Localized.GetString('$DISPLAY');
  LocalMachine := SysObj.Localized.GetString('$MACHINE');
  LocalTarget := SysObj.Localized.GetString('$TARGET');
  LocalDistToGo := SysObj.Localized.GetString('$DISTTOGO');
  LocalLag := SysObj.Localized.GetString('$LAG');
end;

procedure TClassicPositionWindow.DisableDisplay(aTag : TAbstractTag);
begin
  Disabled := aTag.AsBoolean;
  ForceRedraw := True;
  Repaint;
end;

procedure TClassicPositionWindow.AxisSelectChange(aTag : TAbstractTag);
begin
  ForceRedraw := True;
  Repaint;
end;

procedure TClassicPositionWindow.UpdateDisplay(aTag : TAbstractTag);
begin
  if CurrentMode <> GGroups.AsArrayInteger[7] then begin
    CurrentMode := GGroups.AsArrayInteger[7];
    case CurrentMode of
      21 : CharactersDisplayed := CharactersDisplayedMM;
      20 : CharactersDisplayed := CharactersDisplayedIN;
    end;
    Resize;
    ForceRedraw := True;
    Invalidate;
  end else
    DrawDisplayPosition;
end;

function TClassicPositionWindow.FindFontSize(aWidth, aHeight : Integer) : Integer;
begin
  for Result := 72 downto 1 do begin
    if (aWidth >= FontTable[Result].X) and (aHeight >= FontTable[Result].Y) then
      Exit;
  end;
  Result := 1;
end;

procedure TClassicPositionWindow.Resize;
begin
  Canvas.Brush.Color := Color;
  Canvas.Brush.Style := bsSolid;
  Canvas.Pen.Color := Color;
  Canvas.Rectangle(BevelWidth, BevelWidth, Width - BevelWidth * 2, Height - BevelWidth * 2);
  LargeFontSize := FindFontSize(
                (Width * LeftFrame) div (CharactersDisplayed * 100),
                (Height - MainTitleHeight) div SysObj.NC.AxisCount);
  SmallFontSize := FindFontSize(
                (Width * RightFrame) div (2 * CharactersDisplayed * 100),
                (Height - 50) div (SysObj.NC.AxisCount * 2));

  LeftPos1 := (Width * LeftFrame) div 100 + 5;
  LeftPos2 := LeftPos1 + ((Width - LeftPos1) div 2);
  MidHeight := Height div 2;
end;

function TClassicPositionWindow.NumFormat(X, Y : Integer; ReqAN : Boolean; Index : Integer; pt : TPositionType) : string;
var TmpStr : string;
    Value : Double;
    Inch: Boolean;
begin
  if ForceRedraw or (Values[Index, pt] <> TAxisPositionTag(PositionList[Index]).FValue[pt]) then begin
    Values[Index, pt] := TAxisPositionTag(PositionList[Index]).FValue[pt];
    if ReqAN then
      TmpStr := SysObj.NC.AxisName[Index] + ''
    else
      TmpStr := '';

    Inch:= Self.InchModeTag.AsBoolean;

    Value := Values[Index, pt];

    if Value < 0 then begin
      TmpStr := TmpStr + '-'
    end else begin
      TmpStr := TmpStr + '+'
    end;

    Value := Abs(Value);
    case CurrentMode of
      21 : begin
        if SysObj.NC.AxisType[Index] = atLinear then begin
          if Inch then begin
            FmtStr(Result, '%s%8.3fmm', [TmpStr, Value * 25.4])
          end else begin
            FmtStr(Result, '%s%8.3fmm', [TmpStr, Value]);
          end;
        end else begin
          FmtStr(Result, '%s%8.3fdg', [TmpStr, Value]);
        end;
        Canvas.TextOut(X, Y, Result);

      end;

      20 : begin
        if SysObj.NC.AxisType[Index] = atLinear then begin
          if Inch then begin
            FmtStr(Result, '%s%9.4f"', [TmpStr, Value])
          end else begin
            FmtStr(Result, '%s%9.4f"', [TmpStr, Value / 25.4]);
          end;
        end else begin
          FmtStr(Result, '%s%8.3fdg', [TmpStr, Value]);
        end;
        Canvas.TextOut(X, Y, Result);
      end;
    end;
  end;
end;

procedure TClassicPositionWindow.DrawDisplayPosition;
var I, Seperator : Integer;
begin
  with Canvas do begin
    Font.Color := PositionColor;
    Font.Height := LargeFontSize;
    Font.Style := [fsBold];
    Seperator := (Height - MainTitleHeight) div SysObj.NC.AxisCount;
    for I := 0 to SysObj.NC.AxisCount - 1 do begin
      if SysObj.NC.ActiveAxis = I then
        Font.Color := HighlightColor
      else
        Font.Color := PositionColor;
      NumFormat(5, MainTitleHeight + I * Seperator, True, I, ptDisplay);
    end;

    Font.Color := PositionColor;
    Font.Height := SmallFontSize;
    Font.Style := [];
    Seperator := (Height - 60) div (SysObj.NC.AxisCount * 2);
    for I := 0 to SysObj.NC.AxisCount - 1 do begin
      if SysObj.NC.ActiveAxis = I then
        Font.Color := HighlightColor
      else
        Font.Color := PositionColor;
      NumFormat(LeftPos1, 30 + I * Seperator, True, I, ptMachine);
      NumFormat(LeftPos1, 30 + I * Seperator + MidHeight, True, I, ptTarget);
      NumFormat(LeftPos2, 30 + I * Seperator, False, I, ptDistToGo);
      NumFormat(LeftPos2, 30 + I * Seperator + MidHeight, False, I, ptFErr);
    end;
  end;
end;

procedure TClassicPositionWindow.Paint;
begin
  inherited;
  Canvas.Font.Name := Sysobj.MonoFontName;
  Canvas.Brush.Style := bsSolid;
  Canvas.Brush.Color := Color;
  Canvas.Font.Height := SmallFontSize;
  Canvas.Font.Color := TitleColour;
  Canvas.TextOut(5, 3, LocalDisplay);
  Canvas.TextOut(LeftPos1, 3, LocalMachine);
  Canvas.TextOut(LeftPos1, MidHeight, LocalTarget);
  Canvas.TextOut(LeftPos2, 3, LocalDistToGo);
  Canvas.TextOut(LeftPos2, MidHeight, LocalLag);
  ForceRedraw := True;
  if InstallState >= InstallStateDone  then
    DrawDisplayPosition;
  ForceRedraw := False;
end;

procedure TClassicPositionWindow.Repaint;
begin
  inherited;
end;

initialization
  RegisterCNCClass(TClassicPositionWindow);
end.
