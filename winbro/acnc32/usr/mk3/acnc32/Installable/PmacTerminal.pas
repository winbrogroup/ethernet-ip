unit PmacTerminal;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, CoreCNC, Pcomm32, Term, CNCTypes, CNC32, IniFiles, named,
  CncUtils, PComm32Edit, Pmac32NC, NC32, NC32ConfigurationForm, CNCAbs, TurboPmac32NC;

type
  TInstallablePComm = class(TExpandDisplay)
  private
  protected
    FPComm    : TPComm32;
  public
    function  SendPmacLine(s : string) : string; virtual; abstract;
    property  PComm : TPComm32 read FPComm;
  end;


  TPmac32Terminal = class(TInstallablePComm)
  private
    Device      : Integer;
    Terminal    : TTerminal;
    Config      : TNC32Configure;
    procedure NewLine(Sender : TObject; text : string);
    procedure DownLoadFile(aTag : TAbstractTag);
    procedure EditFile(aTag : TAbstractTag);
  protected
    procedure Resize; override;
  public
    constructor Create(aOwner : TComponent); override;
    procedure InstallComponent(Params : TParamStrings); override;
    procedure Installed; override;
    procedure IShutdown; override;
    function  SendPmacLine(s : string) : string; override;
  published
  end;

implementation

procedure TPmac32Terminal.NewLine(Sender : TObject; text : string);
var Tmp, Resp : string;
begin
  if Assigned(Pcomm) then begin
    if Assigned(Config) then begin
      Tmp := Config.TextParser(Text);
    end else
      Tmp := Text;

    Resp := PComm.Converse(Tmp);

    if Tmp <> Text then
      Tmp := Text + ' (' + Tmp + ')';
    Terminal.AddText(Tmp);
    if Resp <> '' then
      Terminal.AddText(Resp);
  end else begin
    Terminal.AddText('Fake System');
  end;
end;

constructor TPmac32Terminal.Create(aOwner : TComponent);
begin
  inherited Create(aOwner);
  Terminal := TTerminal.create(Self);
  Terminal.parent    := Self;
  Terminal.OnNewLine := NewLine;
  Terminal.Align := alClient;
end;

procedure TPmac32Terminal.InstallComponent(Params : TParamStrings);
begin
  inherited installComponent(Params);

  if not CNC32.FakeNCSystem then begin
    if not Params.ReadBool('UseNC', False) then begin
      Device   := Params.ReadInteger('Device', 0);

      FPComm := TPComm32.Create;
      PComm.OpenPmacNumber(Device);
      //if not PComm.DownLoadFileName(LocalPath + Name + '\download.txt', False) then begin
        //Pcomm32.ResponseBox.ShowModal;
        //raise ECNCInstallFault.Create('Download of Pmac Configuration failed' + CarRet + Pcomm32.ResponseBox.Lines.Text);
      //end;
    end else begin
      if (SysObj.NC is TNC32) then begin
        if TNC32(SysObj.NC).NCHAL is TPmacNCHAL then begin
          FPComm := TPmacNCHAL(TNC32(SysObj.NC).NCHAL).Pcomm;
          Config := TNC32(SysObj.NC).Config;
        end
        else if TNC32(SysObj.NC).NCHAL is TTurboPmacNCHAL then begin
          FPComm := TTurboPmacNCHAL(TNC32(SysObj.NC).NCHAL).Pcomm;
          Config := TNC32(SysObj.NC).Config;
        end
        else
          raise ECNCInstallFault.Create('Property UseNC requires that TPmacNCHAL or TTurboPmacNCHAL is configured');
      end
      else
        raise ECNCInstallFault.Create('Property UseNC requires that TNC32 is configured as the Host NC');
    end;
  end else begin
    if not Params.ReadBool('UseNC', True) then begin
      Device   := Params.ReadInteger('Device', 0);

      FPComm := TPComm32.Create;
      PComm.OpenPmacNumber(Device);
      if not PComm.DownLoadFileName(LocalPath + Name + '\download.txt', False) then begin
        Pcomm32.ResponseBox.ShowModal;
        raise ECNCInstallFault.Create('Download of Pmac Configuration failed' + CarRet + Pcomm32.ResponseBox.Lines.Text);
      end;
    end;
  end;
  Primary     := Terminal.ComboBox;
  InstallKeySet(Params.ReadKeys);
end;

procedure TPmac32Terminal.Installed;
begin
  inherited Installed;
  if Assigned(PComm) then begin
    TagEvent(name + 'File', edgeFalling, TMethodTag, DownLoadFile);
    TagEvent(name + 'Edit', edgeFalling, TMethodTag, EditFile);
  end;
end;

procedure TPmac32Terminal.IShutdown;
begin
  inherited IShutDown;
end;

procedure TPmac32Terminal.DownLoadFile(aTag : TAbstractTag);
begin
  if Assigned(PComm) then begin
    PComm.DownLoadFile;
  end;
end;

procedure TPmac32Terminal.EditFile(aTag : TAbstractTag);
var Edit : TPmacEdit;
begin
  if Assigned(PComm) then begin
    Edit := TPmacEdit.Create(Application);
    Edit.Pcomm32 := PComm;
  end;
end;

procedure TPmac32Terminal.Resize;
begin
  inherited resize;
  Terminal.Align := alClient;
end;

function TPmac32Terminal.SendPmacLine(s : string) : string;
begin
  if Assigned(PComm) then begin
    Terminal.AddText(s);
    Result := PComm.Converse(s);
    if Result <> '' then
      Terminal.AddText(result);
  end;
end;




initialization
  RegisterCNCClass(TPmac32Terminal);
end.
