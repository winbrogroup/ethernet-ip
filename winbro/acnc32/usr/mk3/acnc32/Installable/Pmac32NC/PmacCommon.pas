unit PmacCommon;

interface

uses Classes;
type

  TPmacDataType = (   pdtXWord,
                      pdtYWord,
                      pdtDWord,
                      pdtLWord );


  TPmacMemoryItem = class(TObject)
  private
    procedure SetDValue(aValue : Int64);
    function GetDValue : Int64;
  public
    Name    : string;
    Address : Word;
    DataType : TPmacDataType;
    ValueX : Integer;
    ValueY : Integer;
    DoneEv  : TNotifyEvent;
    property DValue : Int64 read GetDvalue write SetDValue;
  end;


implementation

procedure TPmacMemoryItem.SetDValue(aValue : Int64);
begin
  ValueY := aValue and $FFFFFF;
  ValueX := aValue shr 24;
end;

function TPmacMemoryItem.GetDValue : Int64;
begin
  Result := ValueY + (ValueX shl 24);
end;

end.
