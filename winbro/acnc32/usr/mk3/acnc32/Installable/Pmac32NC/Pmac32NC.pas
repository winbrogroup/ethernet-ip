unit Pmac32NC;

interface

uses Classes, SysUtils, CNCTypes, CNC32, CoreCNC, Windows, PComm32, NC32, NCNames,
     Tokenizer, Pmac32NCConfigure, NCProgram, NC32Types, GenericIO,
     NC32ConfigurationForm, Pmac32NCIntros, Pmac32NCValues, CNCUtils, Named, CNCAbs,
     FaultRegistration, SyncObjs;

type
  TNCCommandType = (
    ctJogPlus,
    ctJogMinus,
    ctCycleStart,
    ctFeedHold,
    ctCycleStop,
    ctCycleAbort,
    ctSingleBlock,
    ctJogReturnPlus,
    ctJogReturnMinus
  );

  TNCCommand = class(TObject)
    Command : TNCCommandType;
    Data : Pointer;
    Condition : Boolean;
  public
    constructor Create(aCommand : TNCCommandType; aData : Pointer; aCondition : Boolean);
  end;

  TPmacNCHAL = class;

  TPmacJogThread = class(TThread)
  private
    HAL : TPmacNCHAL;
    CommandList : TList;
    Lock: TCriticalSection;
  public
    constructor Create(aHAL : TPmacNCHAL);
    procedure Execute; override;
    procedure ProcessCommand(aCommand : TNCCommand);
    procedure Add(Command : TNCCommand);
  end;

  TPmacNCHAL = class(TNCHAL)
  private
    FPcomm : TPcomm32;
    Config : TPmacNCConfigure;
    CoordStatus1 : TPmacCoordSystemStatii1;
//    CoordStatus2 : TPmacCoordSystemStatii2;
    FTargetFeedRate : Double;
    NCSweep : Integer;
    RotaryRemaining : Integer;
    CurrentFeedOverride : Double;
    Intros : TPmac32Intros;
    Pmac32NCValues : TPmac32NCValues;
    JogThread : TPmacJogThread;
    DisplayUnits : Double;

    LastGGroup03 : Integer;
    LastGGroup47 : Integer;
    LastGGroup811 : Integer;

    FDeviceIndex : Integer;

    function GetForceBlocking : Boolean;
    procedure SetForceBlocking(aOn : Boolean);
    procedure IssueAsciiPcommCommand(const Cmd : string);

  protected
    procedure SetAxisPositionTags(APList : TList); override; // List of TAxisPositionTag
    procedure SetJogPlus(aAxis : Integer; aOn : Boolean); override;
    procedure SetJogMinus(aAxis : Integer; aOn : Boolean); override;
    procedure SetCycleStart(aOn : Boolean); override;
    procedure SetFeedHold(aOn : Boolean); override;
    procedure SetCycleStop(aOn : Boolean); override;
    procedure SetCycleAbort(aOn: Boolean); override;
    procedure SetSingleBlock(aOn : Boolean); override;
    procedure SetOptionalStop(aOn : Boolean); override;
    procedure SetBlockDelete(aOn : Boolean); override;
    procedure SetFeedOverride(aFeed : Double); override;
    procedure SetFeedOverrideEnable(aOn : Boolean); override;
    procedure SetJogMode(aMode : TJogModes); override;
    procedure SetJogIncrement(aIncrement : Integer); override;

    function GetJogPlus(aAxis : Integer) : Boolean; override;
    function GetJogMinus(aAxis : Integer) : Boolean; override;
    function GetCycleStart : Boolean; override;
    function GetFeedHold : Boolean; override;
    function GetCycleStop : Boolean; override;
    function GetCycleAbort: Boolean; override;
    function GetFeedOverride : Double; override;
    function GetAxisNames : string; override;
    procedure SetRequestCNCMode(aMode : TCNCMode); override;
    function GetTargetFeedrate : Double; override;
    function GetActualFeedrate : Double; override;
    function GetAxisType(Index : Integer) : TAxisType; override;

    procedure UpdateHardware; override;
    procedure ExecutePartProgram; override;
    property ForceBlocking : Boolean read GetForceBlocking write SetForceBlocking;
    procedure UpdatePersistent(aTag : TAbstractTag); override;
    procedure UpdatePersistentEv(Sender : TObject);
    procedure SetActiveAxis(aAxis : Integer); override;
    procedure ExtFunction(aFunc : Integer); override;
    procedure UpdateActiveOffset; override;

  public
    constructor Create(aTagOwner : TNC32; aDeviceIndex : Integer); override;
    function CreateConfig : TNC32Configure; override;
    destructor Destroy; override;
    procedure Configure; override;
    procedure VCLUpdate; override;
    procedure TestPoint; override;
    procedure Clean; override;
    procedure Reset; override;
    procedure ResetRewind; override;
    property Pcomm : TPcomm32 read FPcomm;
  end;

  TPmacJogPanelDisplay = class(TExpandDisplay)
  private
    IOLED : array [0..15] of TIOLED;
    PComm : TPComm32;
  public
    constructor Create(aOWner : TComponent); override;
    procedure Installed; override;
    procedure UpdateDisplay(aTag : TAbstractTag);
  end;

implementation

uses Math;

const
  FeedOverrideRatio = 145.07;

const
  NoLookAhead = 0;

constructor TPmacNCHAL.Create(aTagOwner : TNC32; aDeviceIndex : Integer);
begin
//  TPmacNCHAL.Create(0);
  inherited Create(aTagOwner, aDeviceIndex);
  FDeviceIndex := aDeviceIndex;
  CreateDirectory(PChar(ProfilePath + NCPmacDirectory), nil);
  FPcomm := TPcomm32.Create;
  Pcomm.OpenPmacNumber(FDeviceIndex);
  Pcomm.Converse('del gat');

//  Test := Pcomm.RotBufInit(1);
//  VBGHandle := Pcomm.VGBInit(24, GrabArray);
  // Pcomm.VarBufferCreate
  Intros := TPmac32Intros.Create(Pcomm);
  DisplayUnits := 1;
end;

destructor TPmacNCHAL.Destroy;
begin
  if Assigned(JogThread) then
    JogThread.Free;

  if Assigned(Pcomm) then begin
    if Pcomm.ControlPanel then
      Pcomm.ControlPanel := False;

    if Config.PmacVersion <> 1 then begin
      Pcomm.SetPmacRealTimeBuffer(10, False);
      //Pcomm.VarBufferRemove
      Pcomm.RotBufRemove(0);
      Pcomm.Converse('del gat');
  //    Pcomm.RotBufRemove(1);
      Pcomm.Converse('$$$***');
      PComm.ClosePmacNumber;
      PComm.Free;
    end;
  end;
  if Assigned(Config) then
    Config.Free;

  if Assigned(Intros) then
    Intros.Free;

  if Assigned(Pmac32NCValues) then
    Pmac32NCValues.Free;

  inherited Destroy;
end;

procedure TPmacNCHAL.Clean;
begin
  PPHWLine := 0;
  PPHWUnit := 0;
  FProgramLine := 0;
  Blocking := False;
end;

procedure TPmacNCHAL.ExecutePartProgram;
var Instr : TNCInstructionRecord;
    NextBGLine : Integer;
    Tmp : Single;
    OutLine, StartLine : string;
    IsMotion : Boolean;
    BStop : Boolean;
    McodeProcessing : Boolean;

  function AssignM(M, Value : Integer) : string;
  begin
    Result := Format('M%d==%d', [M, Value]);
  end;

  procedure RunTimeError(aText : string);
  begin
    ErrorString := Format(aText + ' [%s:%d]', [SPP.Name, PPBGLine]);
    ResetRewind;
  end;

  procedure SetOffset;
  var I : Integer;
  begin
    for I := 0 to FAxisCount - 1 do begin
      TotalOffset[I] := LocalOffset[I] + CoordOffset[I] + ZeroOffsetAdjust.AsArrayDouble[I];
    end;
  end;

  procedure SetOffsetTable(Inst : TNCInstruction; aOffset : TUnaryDoubleArrayTag);
  var P : TSFAParameter;
      I : Integer;
      Ptr : Integer;
      PValue : Double;
  begin
    Ptr := 2;
    for P := Low(TSFAParameter) to High(TSFAParameter) do begin
      if P in TSFAParameters(SPP.InstructionList[PPBGInst + 1]) then begin
        I := Config.AxisIndexFromName(0, NCParamNames[P]);
        if I <> -1 then begin
           if not Config.IsRotary(Config.CoordinateSetup[0].AxisByName[P]) then
             PValue := TAbstractTag(SPP.InstructionList[PPBGInst + Ptr]).AsDouble * Units
           else
             PValue := TAbstractTag(SPP.InstructionList[PPBGInst + Ptr]).AsDouble;
          aOffset.Data^.DoubleArray[I] := PValue;
        end;
        Inc(Ptr);
      end;
    end;

    for I := 0 to FAxisCount - 1 do begin
      LocalOffset[I] := G50.Data^.DoubleArray[I] +
                        G51.Data^.DoubleArray[I] +
                        G52.Data^.DoubleArray[I];
    end;

    SetOffset;

    case Inst of
      nfG50 : OutLine := OutLine + 'G50';
      nfG51 : OutLine := OutLine + 'G51';
      nfG52 : OutLine := OutLine + 'G52';
    else
      RunTimeError('PROF ERROR: Invalid Local offset; TPmacNCHAL.SetOffsetTable');
    end;


    for I := 0 to FAxisCount - 1 do begin
      OutLine := OutLine + Format('%s%.6f', [AxisNames[I+1], LocalOffset[I]]);
    end;
    IsMotion := True;
//    ForceBlocking := True;
  end;

  procedure SetShiftOffset;
  var P : TSFAParameter;
      I : Integer;
      V: Double;
      Ptr : Integer;
  begin
    IsMotion := True;
    Ptr := 2;
    OutLine := OutLine + 'G92';
    for P := Low(TSFAParameter) to High(TSFAParameter) do begin
      if P in TSFAParameters(SPP.InstructionList[PPBGInst + 1]) then begin
        I := Config.AxisIndexFromName(0, NCParamNames[P]);
        if I <> -1 then begin
           if not Config.IsRotary(Config.CoordinateSetup[0].AxisByName[P]) then
             V:= TAbstractTag(SPP.InstructionList[PPBGInst + Ptr]).AsDouble * Units
           else
             V:= TAbstractTag(SPP.InstructionList[PPBGInst + Ptr]).AsDouble;

           V:= V + ZeroOffsetAdjust.AsArrayDouble[I];
           OutLine := OutLine + Format('%s%.6f', [AxisNames[I+1], V]);
        end;
        Inc(Ptr);
      end;
    end;
    ForceBlocking := True;
  end;

  procedure SetCoordOffset(aOffset : Integer);
  var I : Integer;
  begin
    for I := 0 to FAxisCount - 1 do begin
      CoordOffset[I] := CoordSys[aOffset].Data^.DoubleArray[I];
    end;

    SetOffset;

    OutLine := OutLine + Format('G%d', [54+aOffset]);

    for I := 0 to FAxisCount - 1 do begin
      OutLine := OutLine + Format('%s%.6f', [AxisNames[I+1], CoordOffset[I]]);;
    end;
  end;

  procedure ClearCoordOffset;
  var I : Integer;
  begin
    FillChar(CoordOffset, Sizeof(CoordOffset), 0);
    for I := 0 to FAxisCount - 1 do begin
      LocalOffset[I] := 0;
    end;

    OutLine := OutLine + 'G53';
    SetOffset;
    IsMotion := True;
  end;

  procedure NewBlock;
  begin
    McodeProcessing := False;
    OutLine := '';
    StartLine := AssignM(LINE_NUMBER_M, PPBGLine); //Format('M22==%d', [PPBGLine]);
    IsMotion := False;
    BStop := False;
  end;


  procedure DoMCode;
  var MExt, P : Integer;
      STmp : string;
  begin
    McodeProcessing := True;
    Tmp := TAbstractTag(SPP.InstructionList[PPBGInst + 1]).AsDouble;
    OutLine := OutLine + Format('M%d', [Round(Tmp)]);


    STmp := Format('%.4f', [Tmp - Round(Tmp)]);
    MExt := 0;
    P := Pos('.', STmp);
    if P > 0 then begin
      Delete(STmp, 1, P);
      MExt := StrToInt(STmp);
    end;

    MExtTag.AsInteger := MExt;
    case Round(Tmp) of
      0 : begin
        ForceBlocking := True;
        OutLine := OutLine + 'DWELL0STOP';
      end;

      1 : begin
        ForceBlocking := True;
//        OutLine := Format('if(M%d&%d=0)', [PC_STATUS_M, pcsOptionalStop]) + OutLine;
      end;

      30 : begin
        ForceBlocking := True;
        OutLine := OutLine + 'DWELL0STOP';
      end;
    end;
    IsMotion := True;
  end;

  procedure DoSCode;
  begin
//    OutLine := OutLine + Format('S%f', [TAbstractTag(SPP.InstructionList[PPBGInst + 1]).AsDouble]);
    OutLine := OutLine + Format('M%d==%f', [NC_SCODE_M, TAbstractTag(SPP.InstructionList[PPBGInst + 1]).AsDouble]);
    IsMotion := True;
  end;

  procedure DoTCode;
  begin
    OutLine := OutLine + Format('T%f', [TAbstractTag(SPP.InstructionList[PPBGInst + 1]).AsDouble]);
    IsMotion := True;
  end;

  procedure DoFCode;
  begin
    FTargetFeedRate := TAbstractTag(SPP.InstructionList[PPBGInst + 1]).AsDouble;
    OutLine := Format('F%.6f', [TargetFeedRate * Units]) + OutLine;
    IsMotion := True;
  end;

  procedure DoGCode;
  begin
    Tmp := TNCInstructionRecord(SPP.InstructionList[PPBGInst + 1]).NI;
    IsMotion := True;
    case Round(Tmp) of
      0 : InterpolationMode := imRapid;
      1 : InterpolationMode := imLinear;
      2 : InterpolationMode := imCW;
      3 : InterpolationMode := imCCW;
      20 : SetUnits(25.4); //&&&Units
      21 : SetUnits(1);
      70 : SetUnits(25.4); //&&&Units
      71 : SetUnits(1);
      61 : CuttingMode := False;
      64 : CuttingMode := True;
      90 : Incremental := False;
      91 : Incremental := True;
      53 : ClearCoordOffset;
      54 : SetCoordOffset(0);
      55 : SetCoordOffset(1);
      56 : SetCoordOffset(2);
      57 : SetCoordOffset(3);
      58 : SetCoordOffset(4);
      59 : SetCoordOffset(5);
    else
    end;
    OutLine := OutLine + Format('G%d', [Round(Tmp)]);
  end;

  const RotationalMotionModes = [imCW, imCCW];
        PseudoPositionAxes = [nfParamI, nfParamJ, nfParamK, nfParamR];

  procedure DoSFA;
  var I, X : Integer;
      P : TSFAParameter;
      PValue, SOffset : Double;
  begin
    I := 2;
    for P := Low(TSFAParameter) to High(TSFAParameter) do begin
      if P in TSFAParameters(SPP.InstructionList[PPBGInst + 1]) then begin
        X := Config.AxisIndexFromName(0, NCParamNames[P]);
        if (P in Config.CoordinateSetup[0].LegalAxisParameters) and
             not Config.IsRotary(Config.CoordinateSetup[0].AxisByName[P]) then begin
          PValue := TAbstractTag(SPP.InstructionList[PPBGInst + I]).AsDouble * Units;
          SOffset := ActiveShiftOffset[X] * Units;
        end else if (P in PseudoPositionAxes) and (InterpolationMode in RotationalMotionModes) then begin
          PValue :=  TAbstractTag(SPP.InstructionList[PPBGInst + I]).AsDouble * Units;
          SOffset := ActiveShiftOffset[X] * Units;
        end else begin
          PValue := TAbstractTag(SPP.InstructionList[PPBGInst + I]).AsDouble;
          SOffset := ActiveShiftOffset[X];
        end;

        if X <> -1 then
          if not Incremental then
            PValue := PValue + TotalOffset[X] + SOffset;
        Inc(I);
        OutLine := OutLine + Format('%s%.6f', [NCParamNames[P], PValue]);

      end;
    end;
    IsMotion := True;
  end;

  procedure DoDwell;
  begin
    OutLine := OutLine + 'G04';
    if TSFAParameters(SPP.InstructionList[PPBGInst + 1]) <> [] then begin
      OutLine := OutLine + Format('P%f', [TAbstractTag(SPP.InstructionList[PPBGInst + 2]).AsDouble]);
    end;
    ForceBlocking := True;
    IsMotion := True;
  end;

  procedure DoLineNumber;
  begin
    PPBGLine := Integer(SPP.InstructionList[PPBGInst + 1]);
    if (RotaryRemaining >= LookAhead) or ForceBlocking then
      Blocking := True;
    RunBlockDelete := False;
    BStop := True;
  end;

  procedure DoReturn;
  begin
    RunReturn(NextBGLine);;
//    OutLine := AssignM(LINE_NUMBER_M, PPBGLine) + AssignM(UNIT_NUMBER_M, PPBGUnit);
    IsMotion := True;
  end;

  procedure DoCall;
  begin
//    OutLine := OutLine + AssignM(LINE_NUMBER_M, PPBGLine) + 'G04';
    OutLine := 'G04';
    IsMotion := True;
    ForceBlocking := True;


    if not (RunBlockDelete and (ncsBlockDelete in NCStates)) then begin
      RunCall(NextBGLine);
//      OutLine := Outline + AssignM(LINE_NUMBER_M, PPBGLine) + AssignM(UNIT_NUMBER_M, PPBGUnit);
    end;
  end;

  procedure DoBlockDelete;
  begin
    OutLine := Format('if(M%d&%d=0)', [PC_SIGNAL_M, IntTwoPower(Ord(PCS_BLOCK_DELETE))]) + OutLine;
  end;

  procedure DoAssign;
  begin
    if not (RunBlockDelete and (ncsBlockDelete in NCStates)) then begin
      RunAssign;
    end;
  end;

  procedure DoHardware;
  begin
    Outline := TAbstractTag(SPP.InstructionList[PPBGInst + 1]).Name;
    IsMotion := True;
  end;

  procedure DoHardwareSync;
  var I : Integer;
  begin
    HWSynch := True;
    for I := 0 to HWCount - 1 do begin
      IHW.AsArrayDouble[I] := PComm.FDPR[PmacIHWDPR + (I*4)];
      PComm.FDPR[PmacOHWDPR + (I*4)] := OHW.AsArrayDouble[I];
    end;
  end;

var
  CountDown  : Integer;
begin
  CountDown := 500;
  try
    if (Blocking or ForceBlocking) and
       not StartPermissiveTag.AsBoolean
       and not (ncsInCycle in FNCStates) then begin
      StartPermissiveTag.AsBoolean := True;
//      EventLog('++ResetRewind Complete');
    end;

    if RotaryRemaining <= LookAHead then
      Blocking := False;

    if Blocking or ForceBlocking then
      Exit;

    NewBlock;


    if ncsSingleBlock in NCStates then
      LookAHead := NoLookAhead
    else
      LookAHead := NormalLookAhead;

    HWSynch := False;

    try
      while not Blocking and not HWSynch and not ResetRewindRequest do begin
        Instr.Ptr := SPP.InstructionList[PPBGInst];
        NextBGLine := PPBGInst + Instr.ParameterCount;
        case Instr.Instruction of
          nfSCode : DoSCode;
          nfTCode : DoTCode;
          nfFCode : DoFCode;
          nfMCode : DoMCode;
          nfGCode : DoGCode;
          nfG50 : SetOffsetTable(nfG50, G50);
          nfG51 : SetOffsetTable(nfG51, G51);
          nfG52 : SetOffsetTable(nfG52, G52);
          nfG92 : SetShiftOffset;
          nfDwell : DoDwell;
          nfSFA : DoSFA;
          nfMacroCall,
          nfCall : DoCall;
          nfAssign : DoAssign;
          nfJump : NextBGLine := Integer(SPP.InstructionList[PPBGInst + 1]);
      //    nfData,           // Just data so skip
          nfBlockDelete : DoBlockDelete;
          nfIf : RunIF(NextBGLine);
          nfHash : RunHash(NextBGLine);
          nfIndexVar : RunIndexVar;
          nfICompare : RunICompare(NextBGLine);
          nfLineNumber : DoLineNumber;
          nfReturn : DoReturn;
          nfScriptCall : if not ResetRewindRequest then
                           Synchronize(RunScriptCall);
          nfHWSynch : DoHardwareSync;
          nfHardware : DoHardware;
        end;
        PPBGInst := NextBGLine;

        // Decide if we want the block in the rotary buffer
        // And then add or delete
        if BStop then begin
          if (ncsSingleBlock in NCStates) or IsMotion then begin
            if not CuttingMode or (ncsSingleBlock in NCStates) or McodeProcessing then
              OutLine := StartLine + AssignM(NEXT_LINE_NUMBER_M, PPBGLine) + AssignM(UNIT_NUMBER_M, PPBGUnit) + 'DWE0' + OutLine
            else
              OutLine := StartLine + AssignM(NEXT_LINE_NUMBER_M, PPBGLine) + AssignM(UNIT_NUMBER_M, PPBGUnit) + OutLine;

            Pcomm.AsciiToRot('BStart' + OutLine + 'BStop', 0);
            RotaryRemaining := Pcomm.RotaryRemaining[0];
          end;
          NewBlock;
        end;
        Dec(CountDown);
        if CountDown < 0 then
          Exit;
      end;
    except
      on E : Exception do begin
        RunTimeError(E.Message);
      end;
    end;
  finally
//    EventLog('XP <<');
  end;
end;

procedure TPmacNCHAL.Reset;
begin
end;

procedure TPmacNCHAL.ResetRewind;
var OutLine : string;
    I : TGCodeGroup;
    C : Integer;
begin
  inherited ResetRewind;

  Incremental := False;
  CuttingMode := False; // &&& These two do not allow easy parametric default gcodes
  Blocking := True;
  ForceBlocking := True;

  //&&&Units
  if Config.CoordinateSetup[0].InchDefault then begin
    SetUnits(25.4);
    DisplayUnits := 25.4;
  end else begin
    SetUnits(1);
    DisplayUnits := 1;
  end;

  Pcomm.Converse('A');
  Pcomm.IDPR[PmacLineNumberDPR] := 0;
  Pcomm.IDPR[PmacNextLineNumberDPR] := 0;
  Pcomm.IDPR[PmacUnitNumberDPR] := 0;

  FillChar(TotalOffset, Sizeof(TotalOffset), 0);
  FillChar(ActiveOffset, Sizeof(ActiveOffset), 0);
  for C := 0 to AxisCount - 1 do begin
    TotalOffset[C] := ZeroOffsetAdjust.AsArrayDouble[C];
    ActiveOffset[C] := TotalOffset[C];
  end;

  FillChar(LocalOffset, Sizeof(LocalOffset), 0);
  FillChar(G50.Data^, G50.DataSize, 0);
  FillChar(G51.Data^, G51.DataSize, 0);
  FillChar(G52.Data^, G52.DataSize, 0);
  FillChar(G92.Data^, FAxisCount * Sizeof(Double), 0);

  FillChar(CoordOffset, SizeOf(CoordOffset), 0);
  FillChar(ActiveLocalOffset, Sizeof(ActiveLocalOffset), 0);
  FillChar(ActiveCoordOffset, Sizeof(ActiveCoordOffset), 0);
  FillChar(ActiveShiftOffset, Sizeof(ActiveShiftOffset), 0);

  Pcomm.IDPR[PmacNCSignalDPR] := Pcomm.IDPR[PmacNCSignalDPR] and  not IntTwoPower(Ord(NCS_PROGRAM_REWIND));
  Pcomm.IDPR[PmacNCSignalDPR] := Pcomm.IDPR[PmacNCSignalDPR] or IntTwoPower(Ord(NCS_RESETREWIND)); //NCSResetRewind;

  Pcomm.Converse('Close');
  Pcomm.Converse('&1delrot');
//  Pcomm.Converse('&2delrot');
  Pcomm.Converse('&1defrot $400');
//  Pcomm.Converse('&2defrot $400');
  Pcomm.Converse('&1a');
  Pcomm.RotBufClr(0);
  Pcomm.RotBufChange($400, 0);
  {B := }Pcomm.RotBuf(True);

  if FRequestCNCMode in ValidCycleStartMode then begin
    Pcomm.Converse('openrot');
    OutLine := 'BSTARTG04';
    for I := Low(TGCodeGroup) to High(TGCodeGroup) do
      if I <> gcgUnique then
        OutLine := OutLine + Format('G%d', [GCodeGroupDefaults[I]]);

    Pcomm.AsciiToRot(OutLine + 'BSTOP', 0);
  end;
  Sleep(100);
  Blocking := False;
  ForceBlocking := True;
end;

function TPmacNCHAL.GetTargetFeedrate : Double;
begin
  Result := FTargetFeedRate;
end;

function TPmacNCHAL.GetActualFeedrate : Double;
begin
  Result := PComm.CoordVelocity(0, FAxisCount);
end;

function TPmacNCHAL.GetAxisType(Index : Integer) : TAxisType;
begin
  if Config.IsRotary(Config.CoordinateSetup[0].AxisByDisplay[Index]) then
    Result := atRotary
  else
    Result := atLinear;
end;

var
  AP : TAxisPosition;

procedure TPmacNCHAL.VCLUpdate;
var I : Integer;
begin
  I := Pcomm.IDPR[PmacGGroupA_DDpr];
  if I <> LastGGroup03 then begin
    ActiveGGroup.AsArrayInteger[0] := I and $FF;
    ActiveGGroup.AsArrayInteger[1] := (I shr 8) and $ff;
    ActiveGGroup.AsArrayInteger[2] := (I shr 16) and $ff;
    ActiveGGroup.AsArrayInteger[3] := (I shr 24) and $ff;
    LastGGroup03 := I;
  end;

  I := Pcomm.IDPR[PmacGGroupF_IDpr];
  if I <> LastGGroup47 then begin
    ActiveGGroup.AsArrayInteger[4] := I and $FF;
    ActiveGGroup.AsArrayInteger[5] := (I shr 8) and $ff;
    ActiveGGroup.AsArrayInteger[6] := (I shr 16) and $ff;
    ActiveGGroup.AsArrayInteger[7] := (I shr 24) and $ff;
    LastGGroup47 := I;
  end;

  I := Pcomm.IDPR[PmacGGroupEtcDpr];

  if I <> LastGGroup811 then begin
    ActiveGGroup.AsArrayInteger[8] := I and $FF;
    ActiveGGroup.AsArrayInteger[9] := (I shr 8) and $ff;
    ActiveGGroup.AsArrayInteger[10] := (I shr 16) and $ff;
    LastGGroup811 := I;
  end;

  if Integer(AxisHomed) = ((1 shl Self.AxisCount) - 1) then begin
    if not (ncsHomed in FNCStates) then begin
      Include(FNCStates, ncsHomed);
      Pmac32NCValues.WriteZeroOffsets(ZeroOffset);
      Pmac32NCValues.WritePlusLimits(SoftLimitPlus, ZeroOffset);
      Pmac32NCValues.WriteMinusLimits(SoftLimitMinus, ZeroOffset);
    end;
  end else
    Exclude(FNCStates, ncsHomed);

  if Integer(AxisInPosition) = ((1 shl Self.AxisCount) - 1) then
    Exclude(FNCStates, ncsInMotion)
  else
    Include(FNCStates, ncsInMotion);

  RotaryCount.AsInteger := RotaryRemaining;
end;


procedure TPmacNCHAL.UpdateActiveOffset;
var I : Integer;
begin
  if (Pcomm.IDPR[PmacGGroupF_IDpr] shr 24) and $ff = 20 then
    DisplayUnits := 25.4
  else
    DisplayUnits := 1;

  for I := 0 to FAxisCount - 1 do begin
    with Config.CoordinateSetup[0] do begin
      if Config.IsRotary(Config.CoordinateSetup[0].AxisByDisplay[I]) then begin
        ActiveShiftOffset[I] := Pcomm.FDPR[PmacShiftOffsetDPR + I * 4];
        ActiveOffset[I] := (Pcomm.FDPR[PmacLocalOffsetDPR + I * 4] +
                           Pcomm.FDPR[PmacCoordOffsetDPR + I * 4]) +
                           ActiveShiftOffset[I] + ZeroOffsetAdjust.AsArrayDouble[I];
      end else begin
        ActiveShiftOffset[I] := Pcomm.FDPR[PmacShiftOffsetDPR + I * 4] / DisplayUnits;
        ActiveOffset[I] := ((Pcomm.FDPR[PmacLocalOffsetDPR + I * 4] +
                           Pcomm.FDPR[PmacCoordOffsetDPR + I * 4]) / DisplayUnits) +
                           ActiveShiftOffset[I] + ZeroOffsetAdjust.AsArrayDouble[I];
      end;
    end;
  end;
end;

procedure TPmacNCHAL.UpdateHardware;
  function RollOverPosition(Pos : Double) : Double;
  var Tmp : Int64;
  begin
    Tmp := Trunc(Pos / 360);
    Result := Pos - (Tmp * 360);
    if(Result < 0)then
      Result := Result + 360;
  end;

var I : Integer;
    Connection : Integer;
    MotorStatii1 : TPmacMotorStatii1;
    MotorStatii2 : TPmacMotorStatii2;
    CUnits : Double;
    CriticalErrors : Integer;
begin
  // Update position information if data is available
  if not Pcomm.HostBusy then
    Pcomm.HostBusy := True;
  if not Pcomm.PmacBusy then begin

    RotaryRemaining := Pcomm.RotaryRemaining[0];
    CoordStatus1 := Pcomm.CoordStatus1[0];
    CriticalErrors := PComm.IDPR[PmacCriticalErrorsDPR] and $FFFF;
//    CoordStatus2 := Pcomm.CoordStatus2[0];


    if pcs1ProgramRunning in CoordStatus1 then
      Include(FNCStates, ncsInCycle)
    else begin
      Exclude(FNCStates, ncsInCycle);
      if (Pcomm.IDPR[PmacNCSignalDPR] and IntTwoPower(Ord(NCS_PROGRAM_REWIND))) <> 0 then begin
        Pcomm.IDPR[PmacNCSignalDPR] := Pcomm.IDPR[PmacNCSignalDPR] and  not IntTwoPower(Ord(NCS_PROGRAM_REWIND)); //ncsProgramRewind;
        NormalTermination.AsBoolean := True;
        ResetRewind;
      end;
    end;

    PPHWUnit := Pcomm.IDPR[PmacUnitNumberDPR];
    if ncsInCycle in FNCStates then begin
      FProgramLine := Pcomm.IDPR[PmacLineNumberDPR];
    end else begin
      FProgramLine := Pcomm.IDPR[PmacNextLineNumberDPR];
//      PPHWUnit := Pcomm.IDPR[PmacNextUnitNumberDPR];
    end;

//    if pcs1SingleStepMode in CoordStatus1 then
//      Include(FNCStates, ncsSingleBlock)
//    else
//      Exclude(FNCStates, ncsSingleBlock);

    NCSweep := NCSweep + 1;

    if MCode.AsInteger <> Pcomm.IDPR[PmacMCodeDPR] then
      MCode.AsInteger := Pcomm.IDPR[PmacMCodeDPR];

    if MCodeAFC.AsBoolean then
      Pcomm.IDPR[PmacPlcSignalDPR] := Pcomm.IDPR[PmacPlcSignalDPR] or IntTwoPower(Ord(PLCS_MCODE_AFC))
    else
      Pcomm.IDPR[PmacPlcSignalDPR] := Pcomm.IDPR[PmacPlcSignalDPR] and not IntTwoPower(Ord(PLCS_MCODE_AFC));

    if (Pcomm.IDPR[PmacNCSignalDPR] and IntTwoPower(Ord(NCS_OFFSET_CHANGE))) <> 0 then begin
      UpdateActiveOffset;
      Pcomm.IDPR[PmacNCSignalDPR] := Pcomm.IDPR[PmacNCSignalDPR] and not IntTwoPower(Ord(NCS_OFFSET_CHANGE)); //NCSOOffsetChange;
    end;


    PComm.ReadFixedBuffers(1);
    for I := 0 to AxisCount - 1 do begin
      with Config.CoordinateSetup[0].AxisByDisplay[I].AxisPositionTag do begin
        Connection := Config.CoordinateSetup[0].AxisByDisplay[I].Connection;
        if Config.IsRotary(Config.CoordinateSetup[0].AxisByDisplay[I]) then
          CUnits := 1
        else
          CUnits := DisplayUnits;
        try
          AP[ptBias] := Pcomm.SPositionBias(I) / CUnits;
          AP[ptMaster] := Pcomm.SMasterPosition(I) / CUnits;
          AP[ptCompensation] := PComm.SCompensation(I) / CUnits;
          AP[ptAbsMachine] := (PComm.SActualPosition(I) / CUnits) + AP[ptBIAS] + AP[ptCompensation];

          if(Config.CoordinateSetup[0].AxisByDisplay[I].ShortestPath) then begin
            AP[ptMachine] := RollOverPosition(AP[ptAbsMachine]);
            AP[ptDisplay] := RollOverPosition(AP[ptMachine] - ActiveOffset[I]);
            AP[ptCommanded] := RollOverPosition((PComm.SCommandedPosition(I) / CUnits) - ActiveOffset[I] + AP[ptBIAS]);
          end else begin
            AP[ptMachine] := AP[ptAbsMachine];
            AP[ptDisplay] := AP[ptMachine] - ActiveOffset[I];
            AP[ptCommanded] := (PComm.SCommandedPosition(I) / CUnits) - ActiveOffset[I] + AP[ptBIAS];
          end;


          AP[ptFerr] := AP[ptCommanded] - AP[ptDisplay] + AP[ptMaster];

          if FCNCModes <= ValidCycleStartMode then begin
            AP[ptTarget] := (Pcomm.STargetPosition(I) / CUnits) - ActiveOffset[I];
            AP[ptDistToGo] := AP[ptTarget] - AP[ptDisplay];
          end else begin
//            AP[ptTarget] := (Pcomm.SMotorTargetPosition(I) / CUnits) - ActiveOffset[I] + AP[ptBIAS];
//            AP[ptDistToGo] := AP[ptTarget] - AP[ptDisplay] + AP[ptMaster];
            AP[ptTarget] := AP[ptMachine];
            AP[ptDistToGo] := 0;
          end;

          AP[ptCPU] := PComm.PulsesPerUnit[I] * CUnits;
          AP[ptGridShift] := PComm.FDPR[PmacGridPositionDPR + I * 4] / CUnits;
          AP[ptTimeRIM] := 0;
          AP[ptAcceleration] := 0;

          FValue := AP;
        except
        end;
        MotorStatii1 := Pcomm.MotorStatus1[Connection];
        MotorStatii2 := Pcomm.MotorStatus2[Connection];

        if pms2InPosition in MotorStatii2 then
          Include(AxisInPosition, I)
        else
          Exclude(AxisInPosition, I);

        if pms1RunningDefinedMove in MotorStatii1 then
          Include(AxisInMotion, I)
        else
          Exclude(AxisInMotion, I);

        if pms2FatalFollowError in MotorStatii2 then
          Include(AxisFatalFollowError, I)
        else
          Exclude(AxisFatalFollowError, I);

        if pms2WarnFollowError in MotorStatii2 then
          Include(AxisWarnFollowError, I)
        else
          Exclude(AxisWarnFollowError, I);

        if pms2HomeComplete in MotorStatii2 then
          Include(AxisHomed, I)
        else
          Exclude(AxisHomed, I);

        if pms1OpenLoop in MotorStatii1 then
          Include(AxisOpenLoop, I)
        else
          Exclude(AxisOpenLoop, I);

        if pms1NegativeLimit in MotorStatii1 then
          Include(AxisNegativeLimit, I)
        else
          Exclude(AxisNegativeLimit, I);

        if pms1PositiveLimit in MotorStatii1 then
          Include(AxisPositiveLimit, I)
        else
          Exclude(AxisPositiveLimit, I);

        if pms2AmplifierFault in MotorStatii2 then
          Include(AxisAmplifierFault, I)
        else
          Exclude(AxisAmplifierFault, I);

        if (CriticalErrors and (1 shl (I mod 4))) <> 0 then begin
          Include(AxisEncoderLoss, I); // No reset condition
        end;
      end;
    end;
    Pcomm.HostBusy := False;
  end;

  Intros.Sweep;
end;

const CSSelect = 0;

procedure TPmacNCHAL.SetJogMode(aMode : TJogModes);
var I : Integer;
    Motor : TMotorSetup;
    ThisUnits : Double;
    U : IUnion;
begin
  FJogMode := aMode;
  for I := 0 to AxisCount - 1 do begin
    Motor := Config.CoordinateSetup[CSSelect].AxisByDisplay[I];
    if Config.IsRotary(Motor) then
      ThisUnits := 1
    else
      ThisUnits := Units;

    Pmac32NCValues.SetJogReturn(I, (Motor.AxisPositionTag.FValue[ptMachine] -
             ZeroOffset.AsArrayDouble[I]) * ThisUnits);
  end;
  U.I := Pcomm.IDPR[PmacCNCStatus1DPR];
  U.B[PmacJogModeOffset] := Byte(aMode);
  Pcomm.IDPR[PmacCNCStatus1DPR] := U.I;
end;

procedure TPmacNCHAL.SetJogIncrement(aIncrement : Integer);
var U : IUnion;
begin //????? Combine the writing of FeedOVR and FeedIncr
  FJogIncrement := aIncrement;
  U.I := Pcomm.IDPR[PmacFeedOvrDPR];
  U.W[1] := aIncrement;
  Pcomm.IDPR[PmacFeedOvrDPR] := U.I;
end;

procedure TPmacNCHAL.SetFeedOverride(aFeed : Double);
var U : IUnion;
begin //????? Combine the writing of FeedOVR and FeedIncr
  CurrentFeedOverride := aFeed;
  Pcomm.FOEnable[CSSelect] := True;
  if ncsFeedOverrideEnable in NCStates then begin
    Pcomm.FeedrateOverride[CSSelect] := Round(aFeed * FeedOverrideRatio);
    //Pcomm.ControlPanelRequest[CSSelect] := True;
  end else begin
    Pcomm.FeedrateOverride[CSSelect] := Round(100 * FeedOverrideRatio);
    //Pcomm.ControlPanelRequest[CSSelect] := True;
  end;

  U.I := Pcomm.IDPR[PmacFeedOvrDPR];
  U.W[0] := Round(aFeed);
  Pcomm.IDPR[PmacFeedOvrDPR] := U.I;
end;

procedure TPmacNCHAL.IssueAsciiPcommCommand(const Cmd : string);
begin
  PComm.Converse('close');
//  try
//    StrToInt(PComm.Converse('I9'));
    Pcomm.Converse(Cmd);
//  except
//    SysObj.FaultInterface.SetFault(SysObj.NC, CoreFaults[cfSysWarning], ['Unable to close rotary buffer'], nil);
//  end;
end;

procedure TPmacNCHAL.SetJogPlus(aAxis : Integer; aOn : Boolean);
var Command : TNCCommand;
begin
  if jmContinuous in JogMode then begin
    Command := TNCCommand.Create(ctJogPlus, Pointer(Config.CoordinateSetup[0].AxisByDisplay[aAxis].Connection), aOn);
    JogThread.Add(Command);
  end

  else if jmIncremental in JogMode then begin
    if ncsHomed in NCStates then begin
      Command := TNCCommand.Create(ctJogReturnPlus, Pointer(Config.CoordinateSetup[0].AxisByDisplay[aAxis].Connection), aOn);
      JogThread.Add(Command);
    end else
      SysObj.FaultInterface.SetFault(SysObj.NC, NCFaults[ncfNoIncJogBeforeHomed], [], nil);
  end

  else if jmMPG in JogMode then begin
  end

  else if jmHome in JogMode then begin
    if not (ncsInCycle in NCStates) then
      IssueAsciiPcommCommand(Format('B%dR', [Config.CoordinateSetup[CSSelect].AxisByDisplay[aAxis].HomeProgram]));
  end;
end;

procedure TPmacNCHAL.SetJogMinus(aAxis : Integer; aOn : Boolean);
var Command : TNCCommand;
begin
  if jmContinuous in JogMode then begin
    Command := TNCCommand.Create(ctJogMinus, Pointer(Config.CoordinateSetup[0].AxisByDisplay[aAxis].Connection), aOn);
    JogThread.Add(Command);
  end

  else if jmIncremental in JogMode then begin
    if ncsHomed in NCStates then begin
      Command := TNCCommand.Create(ctJogReturnMinus, Pointer(Config.CoordinateSetup[0].AxisByDisplay[aAxis].Connection), aOn);
      JogThread.Add(Command);
    end else
      SysObj.FaultInterface.SetFault(SysObj.NC, NCFaults[ncfNoIncJogBeforeHomed], [], nil);
//      SysObj.FaultInterface.SetFault(SysObj.NC, 'Incremental jog is not possible before is NC homed', FaultInterlock, nil);
  end

  else if jmMPG in JogMode then begin
  end

  else if jmHome in JogMode then begin
    if not (ncsInCycle in NCStates) then
      IssueAsciiPcommCommand(Format('B%dR', [Config.CoordinateSetup[CSSelect].AxisByDisplay[aAxis].HomeProgram]));
//      Self.Pcomm.Converse(Format('B%dR', [Config.CoordinateSetup[CSSelect].AxisByDisplay[aAxis].HomeProgram]));
  end;
end;

procedure TPmacNCHAL.SetCycleStart(aOn : Boolean);
var Command : TNCCommand;
begin
  ActiveNWord.AsString := '';

  if ncsSingleBlock in FNCStates then
    Command := TNCCommand.Create(ctSingleBlock, nil, aOn)
  else
    Command := TNCCommand.Create(ctCycleStart, nil, aOn);
  JogThread.Add(Command);

  if aOn then
    NormalTermination.AsBoolean := False;
end;

procedure TPmacNCHAL.SetSingleBlock(aOn : Boolean);
begin
  if aOn then begin
    if (FCNCModes = [CNCModeMDI]) and not SingleBlock then
      Exit;

    inherited SetSingleBlock(aOn);
    if (ncsSingleBlock in FNCStates) and (ncsInCycle in FNCStates) then
      SetCycleStart(True);

    if (ncsSingleBlock in FNCStates) then
      Pcomm.IDPR[PmacPCSignalDPR] := Pcomm.IDPR[PmacPCSignalDPR] or IntTwoPower(Ord(PCS_SINGLE_BLOCK))
    else
      Pcomm.IDPR[PmacPCSignalDPR] := Pcomm.IDPR[PmacPCSignalDPR] and not IntTwoPower(Ord(PCS_SINGLE_BLOCK));
  end else begin
    SetCycleStart(False);
  end;
end;

procedure TPmacNCHAL.SetOptionalStop(aOn : Boolean);
begin
  inherited;
  if (ncsOptionalStop in FNCStates) then
    Pcomm.IDPR[PmacPCSignalDPR] := Pcomm.IDPR[PmacPCSignalDPR] or IntTwoPower(Ord(PCS_OPTIONAL_STOP))
  else
    Pcomm.IDPR[PmacPCSignalDPR] := Pcomm.IDPR[PmacPCSignalDPR] and not IntTwoPower(Ord(PCS_OPTIONAL_STOP));
end;

procedure TPmacNCHAL.SetBlockDelete(aOn : Boolean);
begin
  inherited;
  if (ncsBlockDelete in FNCStates) then
    Pcomm.IDPR[PmacPCSignalDPR] := Pcomm.IDPR[PmacPCSignalDPR] or IntTwoPower(Ord(PCS_BLOCK_DELETE))
  else
    Pcomm.IDPR[PmacPCSignalDPR] := Pcomm.IDPR[PmacPCSignalDPR] and not IntTwoPower(Ord(PCS_BLOCK_DELETE));
end;

procedure TPmacNCHAL.SetFeedHold(aOn : Boolean);
var Command : TNCCommand;
begin
  if aOn {ncsFeedHold in NCStates} then
    Include(FNCStates, ncsFeedHold)
  else
    Exclude(FNCStates, ncsFeedHold);

  Command := TNCCommand.Create(ctFeedHold, nil, aOn);
  JogThread.Add(Command);
end;

procedure TPmacNCHAL.SetCycleStop(aOn : Boolean);
var Command : TNCCommand;
begin
  Command := TNCCommand.Create(ctCycleStop, nil, aOn);
  JogThread.Add(Command);
end;

procedure TPmacNCHAL.SetCycleAbort(aOn: Boolean);
var Command : TNCCommand;
begin
  Command := TNCCommand.Create(ctCycleAbort, nil, aOn);
  JogThread.Add(Command);
end;

function TPmacNCHAL.GetCycleAbort: Boolean;
begin
  Result:= False;
end;


//
// The feedoverrideenable in the pmac locks the feed at what ever was last
// set, so when disabling ensure the feedpot is programmed with 100 first.
// Unfortunately this approach would require waiting for pmac to figure out
// what I'm trying to do. So sod it.
procedure TPmacNCHAL.SetFeedOverrideEnable(aOn : Boolean);
begin
  if aOn then
    Include(FNCStates, ncsFeedOverrideEnable)
  else
    Exclude(FNCStates, ncsFeedOverrideEnable);

{  if not aOn then
    Pcomm.FeedrateOverride[CSSelect] := Round(100 * FeedOverrideRatio);

  Pcomm.FOEnable[CSSelect] := aOn; }
  SetFeedOverride(CurrentFeedOverride);
end;

function TPmacNCHAL.GetJogPlus(aAxis : Integer) : Boolean;
begin
  Result := False;
end;

function TPmacNCHAL.GetJogMinus(aAxis : Integer) : Boolean;
begin
  Result := False;
end;

function TPmacNCHAL.GetCycleStart : Boolean;
begin
  Result := False;
end;

function TPmacNCHAL.GetFeedHold : Boolean;
begin
  Result := False;
end;

function TPmacNCHAL.GetCycleStop : Boolean;
begin
  Result := False;
end;

function TPmacNCHAL.GetFeedOverride : Double;
begin
  Result := Pcomm.FeedrateOverride[CSSelect] / 145.07;
end;

procedure TPmacNCHAL.SetAxisPositionTags(APList : TList);  // List of TAxisPositionTag
var I : Integer;
begin
  with Config.CoordinateSetup[0] do begin
    for I := 0 to AxisCount - 1 do begin
      AxisByDisplay[I].AxisPositionTag := TAxisPositionTag(APList[I]);
    end;
  end;
end;

function TPmacNCHAL.CreateConfig : TNC32Configure;
begin
  Config := TPmacNCConfigure.Create;
  Result := Config;
end;

// Simple configuration.  This is a simple pmac download with a
// header so that PmacNC knows what we just did.
procedure TPmacNCHAL.Configure;
var I : Integer;
    CurrentPath : array [0..1023] of Char;
    Test  : Integer;
begin
  Config.ReadFromStrings;
  FAxisCount := Config.AxisCount[0];
  Pcomm.SetPmacRealTimeMotors(AxisCount);
  Pcomm.SetPmacBackground(True);
  Pcomm.SetPmacRealTimeBuffer(10, True);
  Pcomm.ControlPanel := True;
  FeedOverride := 100;
//  Pcomm.FeedrateOverride[CSSelect]; // why?
  Pcomm.FOEnable[CSSelect] := True;


  for I := 0 to FAxisCount - 1 do begin
    PComm.PulsesPerUnit[I] := Config.MotorSetup[I].CPUnit;
    PComm.VelocityAxes[I] := I + 1;
    Pcomm.JogPositive[I] := False;
    Pcomm.JogNegative[I] := False;
    Pcomm.JogReturn[I] := False;

    case Config.MotorSetup[I].AxisDesignation of
      'A' : PComm.AxesByName[I] := 0;
      'B' : PComm.AxesByName[I] := 1;
      'C' : PComm.AxesByName[I] := 2;
      'U' : PComm.AxesByName[I] := 3;
      'V' : PComm.AxesByName[I] := 4;
      'W' : PComm.AxesByName[I] := 5;
      'X' : PComm.AxesByName[I] := 6;
      'Y' : PComm.AxesByName[I] := 7;
      'Z' : PComm.AxesByName[I] := 8;
    end;
  end;

  PComm.VelocityAxesCount := FAxisCount;

  if GetCurrentDirectory(1023, CurrentPath) = 0 then
    raise ECNCInstallFault.Create('Unable to read current path');

  if not SetCurrentDirectory(PChar(ProfilePath + NCPmacDirectory)) then
    CoreCNC.MessageLog('Cant change directory to NC Profile');

  try
    Config.Output.SaveToFile(ProfilePath + NCPmacDirectory + 'NC.Txt');
    Config.Output.Clear;
    if not Pcomm.DownLoadFileName(ProfilePath + NCPmacDirectory + 'NC.Txt', False) then begin
      Pcomm32.ResponseBox.ShowModal;
      raise ECNCInstallFault.Create('Download of Pmac Configuration failed' + CarRet + Pcomm32.ResponseBox.Lines.Text);
    end else begin
      MessageLog(Pcomm32.ResponseBox.ResponseMemo.Text);
    end;
  finally
    SetCurrentDirectory(CurrentPath);
  end;
  Pmac32NCValues := TPmac32NCValues.Create(FAxisCount, Intros, Self, Config);
  Pmac32NCValues.OnReadDataComplete := UpdatePersistentEv;

  JogThread := TPmacJogThread.Create(Self);

  Test := Pcomm.RotBufInit(0);
  if Test <> 0 then
    raise ECNCInstallFault.Create('Pmac failed to initialize - open PewinPro and type *** in the terminal');
end;

procedure TPmacNCHAL.SetRequestCNCMode(aMode : TCNCMode);
var U : IUnion;
begin
  Lock.Enter('MC');
  try
    if not SysObj.NC.InCycle then begin
      FRequestCNCMode := aMode;
      FCNCModes := [aMode];
      Self.ResetRewind;
      U.I := Pcomm.IDPR[PmacCNCStatus1DPR];
      U.B[PmacCNCModeOffset] := Byte(FCNCModes);
      Pcomm.IDPR[PmacCNCStatus1DPR] := U.I;

      if (FCNCModes = [CNCModeMDI]) and SingleBlock then
        SingleBlock := True;

  //    if FCNCModes <= ValidCycleStartMode then
  //      Pcomm.Converse('openrot');
    end else begin
      SysObj.FaultInterface.SetFault(SysObj.NC, NCFaults[ncfNoModeChangeInCycle], [], nil);
      Exit;
    end;
  finally
    Lock.Leave('MC');
  end;
end;


function TPmacNCHAL.GetAxisNames : string;
begin
  Result := Config.AxisNames[0];
end;

function TPmacNCHAL.GetForceBlocking : Boolean;
begin
  Result := (Pcomm.IDPR[PmacNCSignalDPR] and 1) <> 0;
end;

procedure TPmacNCHAL.SetForceBlocking(aOn : Boolean);
begin
  if aOn then
    Pcomm.IDPR[PmacNCSignalDPR] := Pcomm.IDPR[PmacNCSignalDPR] or 1
  else
    Pcomm.IDPR[PmacNCSignalDPR] := Pcomm.IDPR[PmacNCSignalDPR] and not 1;
end;

type
  TShortInt = record
    case Byte of
      0 : ( I : Integer );
      1 : ( S : Single );
  end;

procedure TPmacNCHAL.UpdatePersistent(aTag : TAbstractTag);
var I : Integer;
begin
  if aTag = ZeroOffset then begin
    Pmac32NCValues.WriteZeroOffsets(ZeroOffset)
  end else if (aTag = StowA) or (aTag = StowB) then begin
    for I := 0 to AxisCount - 1 do begin
      Pcomm.FDPR[PmacStowPosition1DPR + (I*4)] := StowA.AsArrayDouble[I] * Units;
      Pcomm.FDPR[PmacStowPosition2DPR + (I*4)] := StowB.AsArrayDouble[I] * Units;
    end;
  end else begin
    Pmac32NCValues.WritePlusLimits(SoftLimitPlus, ZeroOffset);
    Pmac32NCValues.WriteMinusLimits(SoftLimitMinus, ZeroOffset);
  end;
end;

procedure TPmacNCHAL.UpdatePersistentEv(Sender : TObject);
begin
  UpdatePersistent(ZeroOffset);
  Pmac32NCValues.OnReadDataComplete := nil;
end;

procedure TPmacNCHAL.SetActiveAxis(aAxis : Integer);
var Command : TNCCommand;
    I : Integer;
    U : IUnion;
begin
  if not (ncsInCycle in FNCStates) then begin
    if FActiveAxis <> aAxis then begin
      for I := 0 to AxisCount - 1 do begin
        Command := TNCCommand.Create(ctJogPlus, Pointer(Config.CoordinateSetup[0].AxisByDisplay[I].Connection), False);
        JogThread.Add(Command);
        Command := TNCCommand.Create(ctJogMinus, Pointer(Config.CoordinateSetup[0].AxisByDisplay[I].Connection), False);
        JogThread.Add(Command);
      end;
    end;
    FActiveAxis := aAxis;
    U.I := Pcomm.IDPR[PmacCNCStatus1DPR];
    U.B[PmacActiveAxisOffset] := Byte(FActiveAxis);
    Pcomm.IDPR[PmacCNCStatus1DPR] := U.I;
  end;
end;

procedure TPmacNCHAL.ExtFunction(aFunc : Integer);
begin
  if not (ncsInCycle in FNCStates) and
     (CNCModeManual in FCNCModes) then
    IssueAsciiPcommCommand(Format('B%dR', [201 + aFunc]));
//    Pcomm.Converse(Format('B%dR', [201 + aFunc]));
end;


procedure TPmacNCHAL.TestPoint;
begin
end;

constructor TNCCommand.Create(aCommand : TNCCommandType; aData : Pointer; aCondition : Boolean);
begin
  inherited Create;
  Command := aCommand;
  Data := aData;
  Condition := aCondition;
end;

constructor TPmacJogThread.Create(aHAL : TPmacNCHAL);
begin
  inherited Create(False);
  HAL := aHAL;
  CommandList := TList.Create;
  Lock:= TCriticalSection.Create;
end;

procedure TPmacJogThread.Execute;
begin
  while not Terminated do begin
    try
      Lock.Enter;
      try
        if CommandList.Count <> 0 then begin
          ProcessCommand(TNCCommand(CommandList[0]));
          TNCCommand(CommandList[0]).Free;
          CommandList.Delete(0);
        end;
      finally
        Lock.Leave;
      end;
      Sleep(20);
    except
      on E: Exception do begin
        EventLog('@@ Jog Thread failed with: ' + E.Message);
        Sleep(200);
      end;
    end;
  end;
end;

procedure TPmacJogThread.ProcessCommand(aCommand : TNCCommand);
var Motor : TMotorSetup;
    IncMove : Integer;
    Target, ThisUnits : Double;
begin
  with aCommand do
    case Command of
      ctJogPlus : HAL.Pcomm.JogPositive[Integer(Data)] := Condition;
      ctJogMinus : HAL.Pcomm.JogNegative[Integer(Data)] := Condition;
      ctCycleStart : HAL.Pcomm.Run[CSSelect] := Condition;
      ctCycleStop : HAL.Pcomm.Stop[CSSelect] := Condition;
      ctCycleAbort : HAL.Pcomm.Abort[CSSelect] := Condition;
      ctFeedHold : HAL.Pcomm.Hold[CSSelect] := Condition;
      ctSingleBlock : HAL.Pcomm.Step[CSSelect] := Condition;
      ctJogReturnPlus, ctJogReturnMinus : begin
        if Condition then begin
          Motor := HAL.Config.CoordinateSetup[CSSelect].AxisByConnection[Integer(Data)];
          while not HAL.Pmac32NCValues.JogReturnPermissive do
            Sleep(10);

          if Command = ctJogReturnPlus then
            IncMove := HAL.JogIncrement
          else
            IncMove := -HAL.JogIncrement;

          if HAL.Config.IsRotary(Motor) then
            ThisUnits := 1
          else
            ThisUnits := HAL.Units;

          Target := (Motor.AxisPositionTag.FValue[ptAbsMachine] -
                     HAL.ZeroOffset.AsArrayDouble[Integer(Data)] +
                     (IncMove / 10000)) * ThisUnits;
{          Target := (Motor.AxisPositionTag.FValue[ptDisplay] -
                     HAL.ZeroOffset.AsArrayDouble[Integer(Data)] -
                     HAL.ZeroOffsetAdjust.AsArrayDouble[Integer(Data)] +
                     (IncMove / 10000)) * ThisUnits; }

          HAL.Pmac32NCValues.SetJogReturn(Integer(Data), Target);

          while not HAL.Pmac32NCValues.JogReturnPermissive do
            Sleep(10);
          HAL.Pcomm.JogReturn[Integer(Data)] := Condition;
        end else begin
          HAL.Pcomm.JogReturn[Integer(Data)] := False;
        end;
      end;
    end;
end;

procedure TPmacJogThread.Add(Command : TNCCommand);
begin
  Lock.Enter;
  try
    CommandList.Add(Command);
  finally
    Lock.Leave;
  end;
end;


{ TPmacJogPanelDisplay }
constructor TPmacJogPanelDisplay.Create(aOWner: TComponent);
var I : Integer;
begin
  inherited;
  for I := 0 to 15 do begin
    IOLED[I] := TIOLED.Create(Self);
    IOLED[I].Parent := Self;
    if not Odd(I) then begin
      IOLED[I].Left := 10;
    end else begin
      IOLED[I].Left := 140;
    end;
    IOLED[I].Top := (I div 2) * 25 + 10;
    IOLED[I].Height := 20;
    IOLED[I].Width := 120;
  end;
end;

procedure TPmacJogPanelDisplay.Installed;
begin
  inherited;
  if SysObj.NC is TNC32 then
    with SysObj.NC as TNC32 do
      if NCHAL is TPmacNCHAL then
        with NCHAL as TPMacNCHal do
          Self.PComm := PComm;
  if Assigned(PComm) then begin
    MessageLog('Pcomm registered');
    TagEvent(NameDisplaySweep, EdgeChange, TMethodTag, UpdateDisplay);
  end;
end;

procedure TPmacJogPanelDisplay.UpdateDisplay(aTag: TAbstractTag);
var I : Cardinal;
begin
  for I := 0 to 7 do begin
    IOLED[I * 2].Value := Pcomm.IDPR[$4 + (I * $4)] shr 8;
  end;
end;

initialization
  RegisterNCDeviceClass(TPmacNCHAL);
  RegisterCNCClass(TPmacJogPanelDisplay);
end.
