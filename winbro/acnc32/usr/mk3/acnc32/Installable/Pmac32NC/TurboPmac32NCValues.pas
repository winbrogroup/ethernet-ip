unit TurboPmac32NCValues;

// Maintains list for accessing data relating to the Pmac
// Contains logic for interacting with intros for updating specific values

interface

uses SysUtils, CoreCNC, CNC32, CNCTypes, NC32ConfigurationForm, Classes,
     NC32, TurboPmac32NCConfigure, PmacCommon;

type
  TTurboPmac32NCValues = class(TObject)
  private
    Config : TTurboPmacNCConfigure;
    NCHAL : TNCHAL;
    //ZeroOffsetArray : array [0..19] of TPmacMemoryItem;
    //PlusSoftLimitArray : array [0..19] of TPmacMemoryItem;
    //MinusSoftLimitArray : array [0..19] of TPmacMemoryItem;
  public
    constructor Create(aNCHAL : TNCHAL; aConfig : TTurboPmacNCConfigure);
    procedure WriteZeroOffsets(aArray : TUnaryDoubleArrayTag);
    procedure WritePlusLimits(aArray, zArray : TUnaryDoubleArrayTag);
    procedure WriteMinusLimits(aArray, zArray : TUnaryDoubleArrayTag);
  end;

implementation

uses TurboPmac32NC;

{ TTurboPmac32NCValues }

constructor TTurboPmac32NCValues.Create(aNCHAL: TNCHAL;
  aConfig: TTurboPmacNCConfigure);
begin
  Self.Config:= aConfig;
  Self.NCHAL:= ANCHAL;
end;

procedure TTurboPmac32NCValues.WriteMinusLimits(aArray,
  zArray: TUnaryDoubleArrayTag);
var I: Integer;
    HAL: TTurboPmacNCHAL;
    Motor: TMotorSetup;
    M: string;
    Counts: Double;
begin
  HAL:= TTurboPmacNCHAL(NCHAL);

  for I:= 0 to Config.CoordinateSetup[0].NumberOfAxis - 1 do begin
    Motor:= Config.CoordinateSetup[0].AxisByDisplay[I];
    Counts:= (aArray.AsArrayDouble[I] - zArray.AsArrayDouble[I]) * Motor.CPUnit;
    if Abs(aArray.AsArrayDouble[I]) > 0.0001 then
      if Config.IsRotary(Motor) then
        M:= Format('I%d14=%.5f', [Motor.Connection + 1, Counts])
      else
        M:= Format('I%d14=%.5f', [Motor.Connection + 1, Counts * NCHAL.Units])
    else
      M:= Format('I%d14=0', [Motor.Connection + 1]);
    HAL.Pcomm.Converse(M);
  end;
end;

procedure TTurboPmac32NCValues.WriteZeroOffsets(aArray: TUnaryDoubleArrayTag);
var I: Integer;
    HAL: TTurboPmacNCHAL;
    Motor: TMotorSetup;
begin
  HAL:= TTurboPmacNCHAL(NCHAL);

  EventLog('�� Writing Offsets');
  for I:= 0 to Config.CoordinateSetup[0].NumberOfAxis - 1 do begin
    Motor:= Config.CoordinateSetup[0].AxisByDisplay[I];
    EventLog(Config.CoordinateSetup[0].AxisNames[I+1] + '=' + Format('%.5f', [aArray.AsArrayDouble[I]]));

    if Config.IsRotary(Motor) then begin
      HAL.Pcomm.Converse(Format('M%d = %.5f * %.5f * I%d08 * 32',
        [ AXIS_ZERO_OFFSET_M + I,
          aArray.AsArrayDouble[I],
          Motor.CPUnit,
          I + 1
        ]));
    end else begin
      HAL.Pcomm.Converse(Format('M%d = %.5f * %.5f * I%d08 * 32 * %.5f',
        [ AXIS_ZERO_OFFSET_M + I,
          aArray.AsArrayDouble[I],
          Motor.CPUnit,
          I + 1,
          NCHAL.Units
        ]));
    end;
    //M:= Format('PSET %s%.5f', [Motor.AxisDesignation, aArray.AsArrayDouble[I]]);
    //HAL.Pcomm.Converse(M);
  end;
end;

procedure TTurboPmac32NCValues.WritePlusLimits(aArray,
  zArray: TUnaryDoubleArrayTag);
var I: Integer;
    HAL: TTurboPmacNCHAL;
    Motor: TMotorSetup;
    M: string;
    Counts: Double;
begin
  HAL:= TTurboPmacNCHAL(NCHAL);

  for I:= 0 to Config.CoordinateSetup[0].NumberOfAxis - 1 do begin
    Motor:= Config.CoordinateSetup[0].AxisByDisplay[I];
    Counts:= (aArray.AsArrayDouble[I] - zArray.AsArrayDouble[I]) * Motor.CPUnit;
    if Abs(aArray.AsArrayDouble[I]) > 0.0001 then
      if Config.IsRotary(Motor) then
        M:= Format('I%d13=%.5f', [Motor.Connection + 1, Counts])
      else
        M:= Format('I%d13=%.5f', [Motor.Connection + 1, Counts * NCHAL.Units])
    else
      M:= Format('I%d13=0', [Motor.Connection + 1]);
    HAL.Pcomm.Converse(M);
  end;
end;

end.
