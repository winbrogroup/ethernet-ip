unit Pmac32NCIO;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  CNCTypes, CNC32, PComm32, GenericIO, CoreCNC;

type
  TForm1 = class(TForm)
  private
  public
  end;

  TPmacIOSubSystem = class(TGenericIOSubSystem)
  private
  public
    Pcomm : TPcomm32;
    procedure CheckValidity; override;
    procedure UpdateShadowToReal; override;
    procedure UpdateRealToShadow; override;
  end;

  PmacHostIOSubSystem = class(TPmacIOSubSystem)
  private
    HostName : string;
  public
    constructor Create(aName : string; var Params : string); override;
    procedure CheckValidity; override;
  end;

implementation

{$R *.DFM}

uses Pmac32NC, TurboPmac32NC, NC32, PmacTerminal;

procedure TPmacIOSubSystem.CheckValidity;
begin
  if (SysObj.NC is TNC32) then begin
    if TNC32(SysObj.NC).NCHAL is TPmacNCHAL then
      Pcomm := TPmacNCHAL(TNC32(SysObj.NC).NCHAL).Pcomm
    else if TNC32(SysObj.NC).NCHAL is TTurboPmacNCHAL then
      Pcomm := TTurboPmacNCHAL(TNC32(SysObj.NC).NCHAL).Pcomm
    else
      raise ECNCInstallFault.Create('Cannot create TPmacIOSubSystem without TPmac32NC or TTurboPmac32NC');
  end
  else
    raise ECNCInstallFault.Create('Cannot create TPmacIOSubSystem without TNC32');
end;

procedure TPmacIOSubSystem.UpdateShadowToReal;
begin
  Pcomm.SetDPRMem(FOutputBase, FOutputSize, ShadowOut);
  WriteTags;
end;

procedure TPmacIOSubSystem.UpdateRealToShadow;
begin
  Pcomm.GetDPRMem(FInputBase, FInputSize, ShadowIn);
  ReadTags;
end;


{ PmacHostIOSubSystem }

procedure PmacHostIOSubSystem.CheckValidity;
var Host : TInstallablePcomm;
    Tmp : TAbstractCNC;
begin
  Tmp := TCNC.GetComponent(HostName);
  if not Assigned(Tmp) then
    raise Exception.CreateFmt('Cannot find host device [%s]', [HostName]);

  if not (Tmp is TInstallablePComm) then
    raise Exception.CreateFmt('%s cannot be used as a PmacIOHost', [HostName]);

  Host := TInstallablePComm(Tmp);
  Pcomm := Host.PComm;
end;

constructor PmacHostIOSubSystem.Create(aName : string; var Params : string);
begin
  inherited;
  HostName := ReadDelimited(Params, ';');
end;

{ TPmacIOHost }

{constructor TPmacIOHost.Create(aOwner: TComponent);
begin
  inherited;
  PComm := TPComm32.Create;
end;

destructor TPmacIOHost.Destroy;
begin
  inherited;
  if Assigned(PComm) then
    PComm.Free;
end;

procedure TPmacIOHost.InstallComponent(Params: TParamStrings);
var Device : Integer;
begin
  inherited;
  Device := Params.ReadInteger('Device', 1);
  PComm.OpenPmacNumber(Device);
  if not PComm.DownLoadFileName(LocalPath + Name + '\download.txt', False) then begin
    Pcomm32.ResponseBox.ShowModal;
    raise ECNCInstallFault.Create('Download of Pmac Configuration failed' + CarRet + Pcomm32.ResponseBox.Lines.Text);
  end;
end;

procedure TPmacIOHost.Installed;
begin
  inherited;

end;
 }
initialization
  RegisterIOClass(TPmacIOSubSystem);
  RegisterIOClass(PmacHostIOSubSystem);
//  RegisterCNCClass(TPmacIOHost);
end.
