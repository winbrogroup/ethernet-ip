unit PmacVariableView;

interface

uses CNC32, CNCTypes, CoreCNC, Classes, CNCAbs, Controls, Grids, PmacTerminal,
     Pcomm32, Named, SysUtils, ComCtrls, FaultRegistration;

type
  TPmacVariableView = class(TExpandDisplay)
  private
    Grid : TStringGrid;
    PCommName : string;
    PComm : TPcomm32;
    TabControl : TTabControl;
    procedure UpdateDisplay(aTag : TAbstractTag);
    procedure GridSetEditText(Sender: TObject; ACol, ARow: Longint; const Value: string);
  protected
  public
    constructor Create(aOwner : TComponent); override;
    procedure InstallComponent(Params : TParamStrings); override;
    procedure Installed; override;
  end;

implementation

constructor TPmacVariableView.Create(aOwner : TComponent);
var I : Integer;
begin
  inherited Create(aOwner);
  TabControl := TTabControl.Create(Self);
  TabControl.Parent := Self;
  TabControl.Align := alClient;

  Grid := TStringGrid.Create(Self);
  Grid.Parent := TabControl;
  Grid.Align := alClient;
  Grid.Options := Grid.Options + [goEditing];
  Grid.RowCount := 1024;
  Grid.ColCount := 2;
  Grid.ColWidths[0] := 50;
  Grid.ColWidths[1] := 150;
  for I := 0 to 1023 do begin
    Grid.Cells[0, I + 1] := IntToStr(I);
  end;
end;

procedure TPmacVariableView.InstallComponent(Params : TParamStrings);
var
  aObject : TObject;
begin
  inherited InstallComponent(Params);

  TabControl.Tabs.Add('I');
  TabControl.Tabs.Add('M');
  TabControl.Tabs.Add('P');
  TabControl.Tabs.Add('Q');
  TabControl.Tabs.Add('M Def');

  Grid.Cells[1, 0] := 'Value';

  if not CNC32.FakeNCSystem then begin
    Grid.OnSetEditText := GridSetEditText;
    PCommName := Params.ReadString('Pcomm', 'Pmac32Terminal');
    aObject := GetComponent(PCommName);
    if aObject = nil then
      raise ECNCInstallFault.Create('Pcomm interface not found');

    if aObject is TPmac32Terminal then
      PComm := TPmac32Terminal(aObject).PComm
    else
      raise ECNCInstallFault.Create(Name + ' Requires TPMac32Terminal as ' + PCommName);
  end;
end;

procedure TPmacVariableView.Installed;
begin
  inherited;
  if Assigned(PComm) then
    TagEvent(NameDisplaySweep, EdgeChange, TMethodTag, UpdateDisplay);
end;

procedure TPmacVariableView.GridSetEditText(Sender: TObject; ACol, ARow: Longint; const Value: string);
var Tmp : string;
begin
  if not Grid.EditorMode then begin
    case TabControl.TabIndex of
      0 : Tmp := 'I' + IntToStr(ARow - 1) + '=' + Grid.Cells[aCol, aRow];
      1 : Tmp := 'M' + IntToStr(ARow - 1) + '=' + Grid.Cells[aCol, aRow];
      2 : Tmp := 'P' + IntToStr(ARow - 1) + '=' + Grid.Cells[aCol, aRow];
      3 : Tmp := 'Q' + IntToStr(ARow - 1) + '=' + Grid.Cells[aCol, aRow];
      4 : Tmp := 'M' + IntToStr(ARow - 1) + '->' + Grid.Cells[aCol, aRow];
    end;
    Tmp := Pcomm.Converse(Tmp);
    if Tmp <> '' then
      SysObj.FaultInterface.SetFault(Self, CoreFaults[cfSysInterlock], ['Pmac response: ' + Tmp], nil);
  end;
end;

var ThisStart, ThisEnd : Integer;
    Counter : Integer;
const Chunk = 512;

procedure TPmacVariableView.UpdateDisplay(aTag : TAbstractTag);
var Tmp, Tmp1 : string;
    I : Integer;
begin
  Inc(Counter);
  if Visible and
    ((Counter mod 8) = 0) and
    not Grid.EditorMode then begin

    ThisStart := ThisStart + Chunk;
    if ThisStart > 1000 then
      ThisStart := 0;

    Counter := 0;

    ThisEnd := ThisStart + Chunk - 1;

    case TabControl.TabIndex of
      0 : Tmp := 'I%d..%d';
      1 : Tmp := 'M%d..%d';
      2 : Tmp := 'P%d..%d';
      3 : Tmp := 'Q%d..%d';
      4 : Tmp := 'M%d..%d->';
    end;

    Tmp := Pcomm.ConverseNL(Format(Tmp, [ThisStart, ThisEnd]));
    I := ThisStart;
    while Tmp <> '' do begin
      Tmp1 := CNCTypes.ReadDelimited(Tmp, #13);
      if Tmp1 <> Grid.Cells[1, I + 1] then
        Grid.Cells[1, I + 1] := Tmp1;
      Inc(I);
    end;
  end;
end;

initialization
  RegisterCNCClass(TPmacVariableView);
end.
