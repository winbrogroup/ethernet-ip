unit Pmac32NCValues;

// Maintains list for accessing data relating to the Pmac
// Contains logic for interacting with intros for updating specific values

interface

uses Pmac32NCIntros, CoreCNC, CNC32, CNCTypes, NC32ConfigurationForm, Classes,
     NC32, Pmac32NCConfigure, PmacCommon;

type
  TPmac32NCValues = class(TObject)
  private
    Intros : TPmac32Intros;
    Config : TPmacNCConfigure;
    NumberOfAxis : Integer;
    WaitingJogReturn : Boolean;
    FJogReturn : Int64;
    NCHAL : TNCHAL;
    ZeroOffsetArray : array [0..19] of TPmacMemoryItem;
    PlusSoftLimitArray : array [0..19] of TPmacMemoryItem;
    MinusSoftLimitArray : array [0..19] of TPmacMemoryItem;
    Ix08Array : array [0..19] of TPmacMemoryItem;
    JogReturnArray : array [0..19] of TPmacMemoryItem;
    FOnReadDataComplete : TNotifyEvent;
    procedure ReadAxisSetupData;
    procedure JogReturnDone(Sender : TObject);
    procedure IntrosComplete(Sender : TObject);
  public
    constructor Create(aAxis : Integer; aIntros : TPmac32Intros; aNCHAL : TNCHAL; aConfig : TPmacNCConfigure);
    procedure WriteZeroOffsets(aArray : TUnaryDoubleArrayTag);
    procedure WritePlusLimits(aArray, zArray : TUnaryDoubleArrayTag);
    procedure WriteMinusLimits(aArray, zArray : TUnaryDoubleArrayTag);
    procedure SetJogReturn(aAxis : Integer; aValue : Double);
    procedure GetJogReturn(aAxis : Integer);
    function JogReturnPermissive : Boolean;
    property JogReturn : Int64 read FJogReturn;
    property OnReadDataComplete : TNotifyEvent read FOnReadDataComplete write FOnReadDataComplete;
  end;

implementation

const
  AxisBaseAddress : array [0..7] of Word = (
                  $0800,
                  $08c0,
                  $0980,
                  $0A40,
                  $0B00,
                  $0BC0,
                  $0C80,
                  $0D40
  );

  MotorBaseAddress : array [0..7] of Word = (
                  $20,
                  $5C,
                  $98,
                  $D4,
                  $110,
                  $14c,
                  $188,
                  $1c4
  );


type
  TCompRec = packed record
  case Byte of
    0: (C: Comp);
    1: (ValueY: Longint);
    2: (R0, R1, R2: Byte;
        ValueX: Longint);
    3: (B: array[0..7] of Byte);
    5: (W0: Word; L0: Longint);
    6: (RA: array[0..4] of Byte; L1: Longint);
  end;

constructor TPmac32NCValues.Create(aAxis : Integer; aIntros : TPmac32Intros; aNCHAL : TNCHAL; aConfig : TPmacNCConfigure);
//constructor Create(aAxis : Integer; aNCHAL : TNCHAL; aIntros : TPmac32Intros; aConfig : TNC32Configure);
var I : Integer;
begin
  NumberOfAxis := aAxis;
  Intros := aIntros;
  Config := aConfig;
  NCHAL := aNCHAL;
  for I := 0 to aAxis - 1 do begin
    ZeroOffsetArray[I] := TPmacMemoryItem.Create;
    ZeroOffsetArray[I].Address := AxisBaseAddress[I] + $13;
    ZeroOffsetArray[I].DataType := pdtDWord;

    PlusSoftLimitArray[I] := TPmacMemoryItem.Create;
    PlusSoftLimitArray[I].Address := AxisBaseAddress[I] + $42;
    PlusSoftLimitArray[I].DataType := pdtDWord;

    MinusSoftLimitArray[I] := TPmacMemoryItem.Create;
    MinusSoftLimitArray[I].Address := AxisBaseAddress[I] + $43;
    MinusSoftLimitArray[I].DataType := pdtDWord;

    Ix08Array[I] := TPmacMemoryItem.Create;
    Ix08Array[I].Address := MotorBaseAddress[I] + $9;
    Ix08Array[I].DataType := pdtYWord;

    JogReturnArray[I] := TPmacMemoryItem.Create;
    JogReturnArray[I].Address := AxisBaseAddress[I] + $B;
    JogReturnArray[I].DataType := pdtDWord;
    JogReturnArray[I].DoneEv := JogReturnDone;
  end;

  ReadAxisSetupData;
end;

procedure TPmac32NCValues.ReadAxisSetupData;
var I : Integer;
begin
  for I := 0 to NumberOfAxis - 1 do begin
    Intros.ReadItem(Ix08Array[I]);
  end;
  Ix08Array[NumberOfAxis - 1].DoneEv := IntrosComplete;
end;

procedure TPmac32NCValues.IntrosComplete(Sender : TObject);
begin
  if Assigned(FOnReadDataComplete) then
     FOnReadDataComplete(Self);
end;

procedure TPmac32NCValues.WriteZeroOffsets(aArray : TUnaryDoubleArrayTag);
var I : Integer;
    Scale : Double;
begin
  for I := 0 to NumberOfAxis - 1 do begin
    Scale := NCHAL.Units;
    if Config.IsRotary(Config.CoordinateSetup[0].AxisByDisplay[I]) then
      Scale := 1;

      ZeroOffsetArray[I].DValue := Round(
          Ix08Array[I].ValueY *
          32 * Scale *
          Config.CoordinateSetup[0].AxisByDisplay[I].CPUnit * aArray.AsArrayDouble[I]);
//    MessageLogFmt('%d offset adjust = %f', [I, bArray.AsArrayDouble[I]]);
    Intros.WriteItem(ZeroOffsetArray[I]);
  end;
end;

procedure TPmac32NCValues.WritePlusLimits(aArray, zArray : TUnaryDoubleArrayTag);
var I : Integer;
    Scale : Double;
begin
  for I := 0 to NumberOfAxis - 1 do begin
    Scale := NCHAL.Units;
    if Config.IsRotary(Config.CoordinateSetup[0].AxisByDisplay[I]) then
      Scale := 1;

    if aArray.AsArrayDouble[I] <> 0 then
      PlusSoftLimitArray[I].DValue :=
        Round((aArray.AsArrayDouble[I] - zArray.AsArrayDouble[I]) *
        Config.CoordinateSetup[0].AxisByDisplay[I].CPUnit * Scale)
    else
      PlusSoftLimitArray[I].DValue := 0;

    Intros.WriteItem(PlusSoftLimitArray[I]);
  end;
end;

procedure TPmac32NCValues.WriteMinusLimits(aArray, zArray : TUnaryDoubleArrayTag);
var I : Integer;
    Scale : Double;
begin
  for I := 0 to NumberOfAxis - 1 do begin
    Scale := NCHAL.Units;
    if Config.IsRotary(Config.CoordinateSetup[0].AxisByDisplay[I]) then
      Scale := 1;
    if aArray.AsArrayDouble[I] <> 0 then
      MinusSoftLimitArray[I].DValue :=
        Round((aArray.AsArrayDouble[I] - zArray.AsArrayDouble[I]) *
        Config.CoordinateSetup[0].AxisByDisplay[I].CPUnit * Scale)
    else
      MinusSoftLimitArray[I].DValue := 0;

    Intros.WriteItem(MinusSoftLimitArray[I]);
  end;
end;


procedure TPmac32NCValues.SetJogReturn(aAxis : Integer; aValue : Double);
begin
  JogReturnArray[aAxis].DValue := Round (
          Ix08Array[aAxis].ValueY *
          32 *
          Config.CoordinateSetup[0].AxisByDisplay[aAxis].CPUnit *
          aValue );

  WaitingJogReturn := True;
  Intros.WriteItem(JogReturnArray[aAxis]);
end;

// Use property Jog return for the final value
procedure TPmac32NCValues.GetJogReturn(aAxis : Integer);
begin
  WaitingJogReturn := True;
  Intros.ReadItem(JogReturnArray[aAxis]);
end;

procedure TPmac32NCValues.JogReturnDone(Sender : TObject);
begin
  FJogReturn := TPmacMemoryItem(Sender).DValue;
  WaitingJogReturn := False;
end;

function TPmac32NCValues.JogReturnPermissive : Boolean;
begin
  Result := not WaitingJogReturn;
end;

end.
