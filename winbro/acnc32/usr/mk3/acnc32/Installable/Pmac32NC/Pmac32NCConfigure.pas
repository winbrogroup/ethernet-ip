unit Pmac32NCConfigure;

interface

uses Classes, SysUtils, Tokenizer, CNC32, CNCTypes, NC32Types, CoreCNC, Windows,
     NC32ConfigurationForm;


type
  TPmacIVariableType = (
    pmiUndefined,
    pmiInteger,
    pmiDouble
  );

  TPmacIVariableData = record
    DataType : TPmacIVariableType;
    case Byte of
      0: ( I : Integer);
      1: ( D : Double);
  end;

  TPmacIVariable = class(TObject)
  private
    FValues : array [0..1023] of TPmacIVariableData;
    procedure SetInteger(aIndex, aValue : Integer);
    procedure SetFloat(aIndex : Integer; aValue : Double);
    function GetInteger(aIndex : Integer) : Integer;
    function GetFloat(aIndex : Integer) : Double;
    function GetTextAssign(aIndex : Integer) : string;
    function GetProgrammed(aIndex : Integer) : Boolean;
  public
    procedure Clear;
    property AsInteger[Index : Integer] : Integer read GetInteger write SetInteger;
    property AsFloat[Index : Integer] : Double read GetFloat write SetFloat;
    property AsTextAssign[Index : Integer] : string read GetTextAssign;
    property Programmed[Index : Integer] : Boolean read GetProgrammed;
  end;

  TPmacMVariableData = record
    Alias : string;
    Definition : string;
  end;

  TPmacMVariable = class(TObject)
  private
    FValues : array [0..1023] of TPmacMVariableData;
    procedure SetPmacPtr(Index : Integer; aText : string);
    procedure SetAlias(Index : Integer; aText : string);
    function GetPmacPtr(Index : Integer) : string;
    function GetAlias(Index : Integer) : string;
    function GetDefinition(Index : Integer) : string;
    function GetAssignment(Index : Integer) : string;
  public
    property Alias[Index : Integer] : string read GetAlias write SetAlias;
    property PmacPtr[Index : Integer] : string read GetPmacPtr write SetPmacPtr;
    property Definition[Index : Integer] : string read GetDefinition;
    property Assignment[Index : Integer] : string read GetAssignment;
  end;

  TPmacConstList = class(TStringList)
  private
    Name : string;
  public
    constructor Create(aName : string);
    procedure WriteDefines(S : TStrings);
    function IsOn(aValue : Integer) : string;
    function IsOff(aValue : Integer) : string;
    function SetOff(aValue : Integer) : string;
    function SetOn(aValue : Integer) : string;
    class function Power(aValue : Integer) : Cardinal;
  end;

  // Note the comment must be a single character that is included in
  // in the seperator list
  TPmacNCConfigure = class(TNC32Configure)
  private
    FOutput : TStringList;
    IVar : TPmacIVariable;
    MVar : TPmacMVariable;
    HMFLn : Integer;
    procedure CompileAxisSetup;
    procedure ReadHardwareFlags;
  public
    PmacVersion : Integer;
    EncoderLossDetect : Boolean;
    NCSignalBits : TPmacConstList;
    PlcSignalBits : TPmacConstList;
    PcSignalBits : TPmacConstList;
    General32Bits : TPmacConstList;
    IntrosSignalBits : TPmacConstList;
    EncoderLossBits : TPmacConstList;

    function IsRotary(aMotor : TMotorSetup) : Boolean;
    constructor Create;
    destructor Destroy; override;
    procedure ReadFromStrings;
    property Output : TStringList read FOutput;
    function TextParser(aText : string) : string; override;
  end;

  // NC_SIGNAL_M defines
  TNCSignalBit = (
    NCS_RELEASE_BLOCKING,
    NCS_OFFSET_CHANGE,
    NCS_RESET,
    NCS_RESETREWIND,
    NCS_PROGRAM_REWIND
  );
  TNCSignalBits = set of TNCSignalBit;

  TPlcSignalBit = (
    PLCS_MCODE_AFC,
    PLCS_SCODE_AFC,
    PLCS_TCODE_AFC,
    PLCS_INTROS_READ,
    PLCS_INTROS_WRITE_Y,
    PLCS_INTROS_WRITE_X,
    PLCS_INTROS_WRITE_D
  );

  TPlcSignalBits = set of TPlcSignalBit;

  TPCSignalBit = (
    PCS_SINGLE_BLOCK,
    PCS_OPTIONAL_STOP,
    PCS_BLOCK_DELETE
  );

  TPCSignalBits = set of TPCSignalBit;

  TIntrosSignalBit = (
    IS_READ_STRB,
    IS_WRITE_Y_STRB,
    IS_WRITE_X_STRB,
    IS_WRITE_D_STRB
  );
  TIntrosSignalBits = set of TIntrosSignalBit;

  TEncoderLossBit = (
    EL_ENC_1_5_LOSS,
    EL_ENC_2_6_LOSS,
    EL_ENC_3_7_LOSS,
    EL_ENC_4_8_LOSS
  );
  TEncoderLossBits = set of TEncoderLossBit;

  function IntTwoPower(N : Integer) : Integer;

const
  NCPmacDirectory = 'NCPmacCode\';

  PmacBaseDPR           = $3580;
  PmacUnitNumberDPR     = PmacBaseDPR;       // dp:$DDE0
  PmacNextLineNumberDPR = PmacBaseDPR + 4;   // dp:$DDE1
  PmacLineNumberDPR     = PmacBaseDPR + 8;   // dp:$DDE2
  PmacNCSignalDPR       = PmacBaseDPR + 12;  // dp:$DDE3
  PmacMCodeDPR          = PmacBaseDPR + 16;  // dp:$DDE4
  PmacSCodeDPR          = PmacBaseDPR + 20;  // dp:$DDE5
  PmacTCodeDPR          = PmacBaseDPR + 24;  // dp:$DDE6
  PmacPlcSignalDPR      = PmacBaseDPR + 28;  // dp:$DDE7
  PmacLocalOffsetDPR    = PmacBaseDPR + 32;  // dp:$DDE8
  PmacShiftOffsetDPR    = PmacBaseDPR + 72;  // dp:$DDF2
  PmacCoordOffsetDPR    = PmacBaseDPR + 112; // dp:$DDFC
  PmacGGroupA_DDpr      = PmacBaseDPR + 152;
  PmacGGroupF_IDpr      = PmacBaseDPR + 156;
  PmacGGroupEtcDpr      = PmacBaseDPR + 160;
  PmacPCSignalDPR       = PmacBaseDPR + 164;
  PmacGridPositionDPR   = PmacBaseDPR + 168; // 32 bytes
  PmacStowPosition1DPR  = PmacBaseDPR + 200; // 32 bytes
  PmacStowPosition2DPR  = PmacBaseDPR + 232; // 32 bytes
  PmacIntrosBase        = PmacBaseDPR + 264; // 24 bytes
  PmacIntrosReadY       = PmacIntrosBase;
  PmacIntrosReadX       = PmacIntrosBase + 4;
  PmacIntrosReadAddress = PmacIntrosBase + 8;
  PmacIntrosWriteAddress= PmacIntrosBase + 10;
  PmacIntrosWriteY      = PmacIntrosBase + 12;
  PmacIntrosWriteX      = PmacIntrosBase + 16;
  PmacIntrosSignals     = PmacIntrosBase + 20;

  PmacIHWDPR            = PmacBaseDPR + 288;
  PmacOHWDPR            = PmacBaseDPR + 328;


  PmacCNCStatus1DPR     = PmacBaseDPR + 368; // Integer
  PmacActiveAxisOffset = 0;
  PmacJogModeOffset    = 1;
  PmacCNCModeOffset    = 2;
  PmacNCFlagsOffset    = 3;


  PmacFeedOvrDPR       = PmacBaseDPR + 372;
  PmacJogIncrDPR       = PmacBaseDPR + 374;
  PmacCriticalErrorsDPR= PmacBaseDPR + 376;

  // MAX DPR address = 812

  HWCount = 10;

//  NEXT_UNIT_NUMBER_M = 1;
  PC_SIGNAL_M        = 2;
  UNIT_NUMBER_M      = 3;
  NEXT_LINE_NUMBER_M = 4;
  LINE_NUMBER_M      = 5;
  NC_SIGNAL_M        = 6;
  NC_MCODE_M         = 7;
  NC_SCODE_M         = 8;
  NC_TCODE_M         = 9;
  PLC_SIGNAL_M       = 10;
  GGroupA_M          = 11;
  GGroupB_M          = 12;
  GGroupC_M          = 13;
  GGroupD_M          = 14;
  GGroupF_M          = 15;
  GGroupG_M          = 16;
  GGroupH_M          = 17;
  GGroupI_M          = 18;
  GGroupJ_M          = 19;
  GGroupUnique_M     = 20;
  GGroupCoord_M      = 21;
  ActiveAxis_M       = 22;
  JogMode_M          = 23;
  CNCMode_M          = 24;
  NC_FLAGS_M         = 25;
  FEED_OVR_M         = 27;
  JOG_INCR_M         = 28;
  CRITICAL_ERRORS_M  = 29;
  PmacEncLoss_M      = 30;

  AXIS_ARRAYS        = 60;
  LOCAL_OFFSET_M     = AXIS_ARRAYS;
  SHIFT_OFFSET_M     = AXIS_ARRAYS + 10;
  COORD_OFFSET_M     = AXIS_ARRAYS + 20;
  TARGET_POS_REGISTER_M = AXIS_ARRAYS + 30;
  HOME_GRID_M        = AXIS_ARRAYS + 40;
  ENCODER_M          = AXIS_ARRAYS + 50;
  ENC_STATUS_M       = AXIS_ARRAYS + 60;
  MOTOR_STATUS_M     = AXIS_ARRAYS + 70;
  AXIS_ZERO_OFFSET_M = AXIS_ARRAYS + 80;
  AXIS_STOW_POS1_M   = AXIS_ARRAYS + 90;
  AXIS_STOW_POS2_M   = AXIS_ARRAYS + 100;

  _INTROS_READ_Y       = 180;  // dp o
  _INTROS_READ_X       = 181;  // dp o
  _INTROS_READ_ADDRESS = 185;  // yword o
  _INTROS_WRITE_ADDRESS= 186;  // xword o
  _INTROS_WRITE_Y      = 182;  // dp i
  _INTROS_WRITE_X      = 183;  // dp i
  _INTROS_SIGNALS      = 184;  // dp i
  _INTROS_INDEX        = 187;  // na
  _INTROS_POINTER      = 188;  // na

  IHW_M = 190;
  OHW_M = 200;

  AXIS_CAPTURE_M = 210;

  ENC_POWERON_ZERO_P   = 10;

implementation

uses Math;

const
  PossibleAxisNames = [nfParamX..nfParamW];

resourcestring
  TooManyMotorsForAxisNames = 'Too many motors being added to Coordinate system [%d]';
  InvalidAxisNameFormat = 'Invalid Axis Name [%c]';

function IntTwoPower(N : Integer) : Integer;
begin
  Result := Round(1 shl N);
end;

constructor TPmacNCConfigure.Create;
begin
  inherited Create;
  FOutput := TStringList.Create;
  IVar := TPmacIVariable.Create;
  MVar := TPmacMVariable.Create;
end;

destructor TPmacNCConfigure.Destroy;
begin
  FOutput.Free;
  IVar.Free;
  Mvar.Free;
  inherited Destroy;
end;

function TPmacNCConfigure.IsRotary(aMotor : TMotorSetup) : Boolean;
begin
  case aMotor.AxisDesignation of
    'A',
    'B',
    'C' : Result := True;
  else
    Result := False;
  end;
end;


procedure TPmacNCConfigure.ReadFromStrings;
begin
  inherited ReadFromStrings;

  try
    ReadHardwareFlags;
    CompileAxisSetup;
  except
    on E : Exception do
      raise ECNCInstallFault.Create(E.Message);
  end;
end;

procedure TPmacNCConfigure.ReadHardwareFlags;
var S : TStringList;
    I : Integer;
begin
  S := TStringList.Create;
  PmacVersion := 2;
  try
    S.Text := HardwareFlags;
    for I := 0 to S.Count - 1 do begin
      if S[I] = 'pmaci' then
        PmacVersion := 1
      else if S[I] = 'pmacii' then
        PmacVersion := 2
      else if S[I] = 'encoderlossdetect' then
        EncoderLossDetect := True;
    end;
  finally
    S.Free;
  end;
  if PmacVersion = 1 then
    EventLog('PmacI selected')
  else if PmacVersion = 2 then
    EventLog('PmacII selected');
end;

function TPmacNCConfigure.TextParser(aText : string) : string;
var I : Integer;
    Post : string;
begin
  Result := aText;
  Post := '';
  I := System.Pos('=', aText);
  if I <> 0 then begin
    Post := Copy(Result, I, Length(Result));
    System.Delete(Result, I, Length(Result));
  end;

  for I := 0 to 1023 do begin
    if Result = MVar.Alias[I] then begin
      Result := Format('M%d', [I]) + Post;
      Exit;
    end;
  end;

  Result := aText;
end;

procedure TPmacNCConfigure.CompileAxisSetup;
  function PmacDPByte(Address, Offset : Integer) : string;
  begin
    case Offset of
      0 : Result := Format('Y:$d%x,0,8', [Address div 4]);
      1 : Result := Format('Y:$d%x,8,8', [Address div 4]);
      2 : Result := Format('X:$d%x,0,8', [Address div 4]);
      3 : Result := Format('X:$d%x,8,8', [Address div 4]);
    else
      Result := 'Invalid Offset';
    end;
  end;

  function PmacYWord(Address : Integer) : string;
  begin
    Result := Format('Y:$%x,0,24', [Address]);
  end;

  function PmacDPYWord(Address : Integer) : string;
  begin
    Result := Format('Y:$d%x,0,16', [Address div 4]);
  end;

  function PmacDPXWord(Address : Integer) : string;
  begin
    Result := Format('X:$d%x,0,16', [Address div 4]);
  end;

{  function PmacDPDouble(Address : Integer) : string;
  begin
    Result := Format('D:$d%x', [Address div 4]);
  end; }

  function PmacDP(Address : Integer) : string;
  begin
    Result := Format('DP:$d%x', [Address div 4]);
  end;

  function PmacDPFloat(Address : Integer) : string;
  begin
    Result := Format('F:$d%x', [Address div 4]);
  end;

  function TargetPosRegister(Connection : Integer) : string;
  begin
    Result := Format('D:$%x', [$80B + (Connection * $C0)]);
  end;

  function PmacEncoder(Connection : Integer) : string;
    // X:$c002 add 4 for each further connection
  begin
    if PmacVersion = 1 then
      Result := Format('X:$%x,0,24,s', [$c002 + (Connection * 4)])
    else
      Result := Format('X:$%x,0,24,s', [$c002 + (Connection * 8)])
  end;

  function PmacCapture(Connection : Integer) : string;
  begin
    if PmacVersion = 1 then
      Result :=Format('X:$%x,0,24,s', [$c003 + (Connection * 4)])
    else
      Result := Format('X:$%x,0,24,s', [$c003 + (Connection * 8)]);
  end;

  function EncoderStatus(Connection : Integer) : string;
  begin
    if PmacVersion = 1 then
      Result := Format('X:$%x,0,24', [$c000 + (Connection * 4)])
    else
      Result := Format('X:$%x,0,24', [$c000 + (Connection * 8)]);
  end;

  function MotorStatus(Connection : Integer) : string;
  begin
    Result := Format('Y:$%x,0,24', [$814 + (Connection * $c0)]);
  end;

  function AxisOffset(Connection : Integer) : string;
  begin
    Result := Format('D:$%x', [$813 + (Connection * $c0)]);
  end;

  procedure OAdd(Fmat : string; Args :array of const);
  begin
    FOutput.Add(Format(Fmat, Args));
  end;

  function ICaptureFlag(Connection : Integer) : Integer;
  begin
    case PmacVersion of
      2 : Result := 913 + (Connection * 10);
    else
      Result := 903 + (Connection * 5);
    end;
  end;

  function ICaptureControl(Connection : Integer) : Integer;
  begin
    case PmacVersion of
      2 : Result := 912 + (Connection * 10);
    else
      Result := 902 + (Connection * 5);
    end;
  end;

  var I, Motor, CMCount, IM : Integer;
    SFA : TSFAParameter;
    Tmp : Integer;
    STmp : string;
    PM : TMotorSetup;
begin
  for I := 0 to 7 do begin
    if CoordinateSetup[I].Enabled then begin
      for Motor := 0 to CoordinateSetup[I].NumberOfAxis - 1 do begin
        try
          SFA := GenerateSFAParameter(CoordinateSetup[I].AxisByDisplay[Motor].AxisDesignation);
        except
          raise ECNCInstallFault.CreateFmt(InvalidAxisNameFormat, [CoordinateSetup[I].AxisNames[Motor]]);
        end;
        if not (SFA in PossibleAxisNames) then
          raise ECNCInstallFault.CreateFmt(InvalidAxisNameFormat, [CoordinateSetup[I].AxisNames[Motor]]);

        CoordinateSetup[I].AxisByName[SFA] := CoordinateSetup[I].AxisByDisplay[Motor];
        Include(CoordinateSetup[I].LegalAxisParameters, SFA);

        Tmp := System.Pos(CoordinateSetup[I].AxisByDisplay[Motor].AxisDesignation, CoordinateSetup[I].AxisNames);
        if Tmp = 0 then
          raise ECNCInstallFault.Create('I dont know what this error is');
      end;
    end;
  end;

  FOutput.Clear;

  FOutput.LoadFromFile(ProfilePath + NCPmacDirectory + PreparatoryFile);
  FOutput.Insert(0, 'I5=0');
  FOutput.Insert(0, 'UNDEFINE ALL');
  FOutput.Insert(0, 'P100..1023=0');
  FOutput.Insert(0, 'Q0..1023=0');
  FOutput.Insert(0, 'M0..1023->*');
  FOutput.Insert(0, 'I100..1023=*');


  // The checks for a coordinate system are NONE
  // Add to the output stream the appropriate axis designations
  FOutput.Add('// Attaching motors to coordinate systems');
  for I := 0 to 7 do begin // For Each coordinate system
    if CoordinateSetup[I].Enabled then begin
      FOutput.Add(Format('&%d', [I+1]));
      CMCount := 0;
      for Motor := 0 to 7 do begin
        with MotorSetup[Motor] do begin
          if Enabled and (CoordinateSystem = CoordinateSetup[I]) then begin
            Inc(CMCount);
            if CMCount > Length(CoordinateSetup[I].AxisNames) then
              raise Exception.CreateFmt(TooManyMotorsForAxisNames, [I]);
            FOutput.Add(Format('#%d->%f%s', [ Connection + 1,
                                              CPUnit,
                                              AxisDesignation]));
          end;
        end;
      end;
    end;
  end;

//  if not CoordinateSetup[0].InchDefault then
    FOutput.Add('Frax');

  NCSignalBits := TPmacConstList.Create('NC_SIGNAL_M');
  CNCTypes.GetEnumerationNames(TypeInfo(TNCSignalBit), NCSignalBits);
  PlcSignalBits := TPmacConstList.Create('PLC_SIGNAL_M');
  CNCTypes.GetEnumerationNames(TypeInfo(TPlcSignalBit), PlcSignalBits);
  PcSignalBits := TPmacConstList.Create('PC_SIGNAL_M');
  CNCTypes.GetEnumerationNames(TypeInfo(TPCSignalBit), PCSignalBits);
  General32Bits := TPmacConstList.Create('GENERAL_BITS');
  for I := 0 to 31 do
    General32Bits.Add(Format('BIT%d', [I]));

  IntrosSignalBits := TPmacConstList.Create('_INTROS_SIGNALS');
  CNCTypes.GetEnumerationNames(TypeInfo(TIntrosSignalBit), IntrosSignalBits);
  EncoderLossBits := TPmacConstList.Create('ENCODER_LOSS_M');
  CNCTypes.GetEnumerationNames(TypeInfo(TEncoderLossBit), EncoderLossBits);

  NCSignalBits.WriteDefines(FOutput);
  PlcSignalBits.WriteDefines(FOutput);
  PcSignalBits.WriteDefines(FOutput);
  General32Bits.WriteDefines(FOutput);
  IntrosSignalBits.WriteDefines(FOutput);
  EncoderLossBits.WriteDefines(FOutput);

  FOutput.Add('');
  FOutput.Add('// Motor counts per unit (units are always mm or degree)');
  for Motor := 0 to 7 do begin
    with MotorSetup[Motor] do begin
      if Enabled then begin
        FOutput.Add(Format('#define MOTOR_CPU_%s %f', [MotorSetup[Motor].AxisDesignation, CPUnit]));
      end;
    end;
  end;
  FOutput.Add('');


  MVar.Alias[PC_SIGNAL_M] := PcSignalBits.Name;
//  MVar.Alias[NEXT_UNIT_NUMBER_M] := 'NEXT_UNIT_NUMBER_M';
  MVar.Alias[UNIT_NUMBER_M] := 'UNIT_NUMBER_M';
  MVar.Alias[NEXT_LINE_NUMBER_M] := 'NEXT_LINE_NUMBER_M';
  MVar.Alias[LINE_NUMBER_M] := 'LINE_NUMBER_M';
  MVar.Alias[NC_SIGNAL_M] := NCSignalBits.Name;
  MVar.Alias[NC_MCODE_M] := 'NC_MCODE_M';
  MVar.Alias[NC_SCODE_M] := 'NC_SCODE_M';
  MVar.Alias[NC_TCODE_M] := 'NC_TCODE_M';
  MVar.Alias[PLC_SIGNAL_M] := PlcSignalBits.Name;
  MVar.Alias[GGroupA_M] := 'GGroupA_M';
  MVar.Alias[GGroupB_M] := 'GGroupB_M';
  MVar.Alias[GGroupC_M] := 'GGroupC_M';
  MVar.Alias[GGroupD_M] := 'GGroupD_M';
  MVar.Alias[GGroupF_M] := 'GGroupF_M';
  MVar.Alias[GGroupG_M] := 'GGroupG_M';
  MVar.Alias[GGroupH_M] := 'GGroupH_M';
  MVar.Alias[GGroupI_M] := 'GGroupI_M';
  MVar.Alias[GGroupJ_M] := 'GGroupJ_M';
  MVar.Alias[GGroupUnique_M] := 'GGroupUnique_M';
  MVar.Alias[GGroupCoord_M] := 'GGroupCoord_M';
  MVar.Alias[ActiveAxis_M] := 'ACTIVE_AXIS_M';
  MVar.Alias[JogMode_M] := 'JOG_MODE_M';
  MVar.Alias[CNCMode_M] := 'CNC_MODE_M';
  MVar.Alias[NC_FLAGS_M]:= 'NC_FLAGS_M';
  MVar.Alias[FEED_OVR_M] := 'FEED_OVR_M';
  MVar.Alias[JOG_INCR_M] := 'JOG_INCR_M';
  MVar.Alias[CRITICAL_ERRORS_M] := 'CRITICAL_ERRORS_M';
  MVar.Alias[PmacEncLoss_M] := 'ENCODER_LOSS_M';


  MVar.PmacPtr[PC_SIGNAL_M] := PmacDP(PmacPCSignalDPR);
  MVar.PmacPtr[UNIT_NUMBER_M] := PmacDP(PmacUnitNumberDPR);
  MVar.PmacPtr[NEXT_LINE_NUMBER_M] := PmacDP(PmacNextLineNumberDPR);
  MVar.PmacPtr[LINE_NUMBER_M] := PmacDP(PmacLineNumberDPR);
  MVar.PmacPtr[NC_SIGNAL_M] := PmacDP(PmacNCSignalDPR);
  MVar.PmacPtr[NC_MCODE_M] := PmacDP(PmacMCodeDPR);
  MVar.PmacPtr[NC_SCODE_M] := PmacDP(PmacSCodeDPR);
  MVar.PmacPtr[NC_TCODE_M] := PmacDP(PmacTCodeDPR);
  MVar.PmacPtr[PLC_SIGNAL_M] := PmacDP(PmacPlcSignalDPR);

  MVar.PmacPtr[GGroupA_M] := PmacDPByte(PmacGGroupA_DDpr, 0);
  MVar.PmacPtr[GGroupB_M] := PmacDPByte(PmacGGroupA_DDpr, 1);
  MVar.PmacPtr[GGroupC_M] := PmacDPByte(PmacGGroupA_DDpr, 2);
  MVar.PmacPtr[GGroupD_M] := PmacDPByte(PmacGGroupA_DDpr, 3);
  MVar.PmacPtr[GGroupF_M] := PmacDPByte(PmacGGroupF_IDpr, 0);
  MVar.PmacPtr[GGroupG_M] := PmacDPByte(PmacGGroupF_IDpr, 1);
  MVar.PmacPtr[GGroupH_M] := PmacDPByte(PmacGGroupF_IDpr, 2);
  MVar.PmacPtr[GGroupI_M] := PmacDPByte(PmacGGroupF_IDpr, 3);
  MVar.PmacPtr[GGroupJ_M] := PmacDPByte(PmacGGroupEtcDpr, 0);
  MVar.PmacPtr[GGroupUnique_M] := PmacDPByte(PmacGGroupEtcDpr, 1);
  MVar.PmacPtr[GGroupCoord_M] := PmacDPByte(PmacGGroupEtcDpr, 2);
  MVar.PmacPtr[ActiveAxis_M] := PmacDPByte(PmacCNCStatus1DPR, PmacActiveAxisOffset);
  MVar.PmacPtr[JogMode_M] := PmacDPByte(PmacCNCStatus1DPR, PmacJogModeOffset);
  MVar.PmacPtr[CNCMode_M] := PmacDPByte(PmacCNCStatus1DPR, PmacCNCModeOffset);
  MVar.PmacPtr[NC_FLAGS_M]:= PmacDPByte(PmacCNCStatus1DPR, PmacNCFlagsOffset);
  MVar.PmacPtr[FEED_OVR_M] := PmacDPYWord(PmacFeedOvrDPR);
  MVar.PmacPtr[JOG_INCR_M] := PmacDPXWord(PmacJogIncrDPR);
  MVar.PmacPtr[CRITICAL_ERRORS_M] := PmacDPYWord(PmacCriticalErrorsDPR);
  MVar.PmacPtr[PmacEncLoss_M] := 'Y:$C083,8,4';

  for I := 0 to CoordinateSetup[0].NumberOfAxis - 1 do begin
    IM := CoordinateSetup[0].AxisByDisplay[I].Connection;
    STmp := CoordinateSetup[0].AxisByDisplay[I].AxisDesignation;
    MVar.Alias[LOCAL_OFFSET_M + I]        := Format('LOCAL_OFFSET_%s_M', [STmp]);
    MVar.Alias[SHIFT_OFFSET_M + I]        := Format('SHIFT_OFFSET_%s_M', [STmp]);
    MVar.Alias[COORD_OFFSET_M + I]        := Format('COORD_OFFSET_%s_M', [STmp]);
    MVar.Alias[TARGET_POS_REGISTER_M + I] := Format('TARGET_POS_REGISTER_%s_M', [STmp]);
    MVar.Alias[HOME_GRID_M + I]           := Format('HOME_GRID_%s_M', [STmp]);
    MVar.Alias[ENCODER_M + I]             := Format('ENCODER_%s_M', [STmp]);
    MVar.Alias[ENC_STATUS_M + I]          := Format('ENC_STATUS_%s_M', [STmp]);
    MVar.Alias[MOTOR_STATUS_M + I]        := Format('MOTOR_STATUS_%s_M', [STmp]);
    MVar.Alias[AXIS_ZERO_OFFSET_M + I]    := Format('AXIS_ZERO_OFFSET_%s_M', [STmp]);
    MVar.Alias[AXIS_STOW_POS1_M + I]      := Format('AXIS_STOW_POS1_%s_M', [STmp]);
    MVar.Alias[AXIS_STOW_POS2_M + I]      := Format('AXIS_STOW_POS2_%s_M', [STmp]);
    MVar.Alias[AXIS_CAPTURE_M + I]        := Format('AXIS_CAPTURE_%s_M', [STmp]);

    MVar.PmacPtr[LOCAL_OFFSET_M + I]       := PmacDPFloat(PmacLocalOffsetDPR + (I*4));
    MVar.PmacPtr[SHIFT_OFFSET_M + I]       := PmacDPFloat(PmacShiftOffsetDPR + (I*4));
    MVar.PmacPtr[COORD_OFFSET_M + I]       := PmacDPFloat(PmacCoordOffsetDPR + (I*4));
    MVar.PmacPtr[TARGET_POS_REGISTER_M + I]:= TargetPosRegister(IM);
    MVar.PmacPtr[HOME_GRID_M + I]          := PmacDPFloat(PmacGridPositionDPR+(I*4));
    MVar.PmacPtr[ENCODER_M + I]            := PmacEncoder(IM);
    MVar.PmacPtr[ENC_STATUS_M + I]         := EncoderStatus(IM);
    MVar.PmacPtr[MOTOR_STATUS_M + I]       := MotorStatus(IM);
    MVar.PmacPtr[AXIS_ZERO_OFFSET_M + I]   := AxisOffset(IM);
    MVar.PmacPtr[AXIS_STOW_POS1_M + I]     := PmacDPFloat(PmacStowPosition1DPR + (I*4));
    MVar.PmacPtr[AXIS_STOW_POS2_M + I]     := PmacDPFloat(PmacStowPosition2DPR + (I*4));
    MVar.PmacPtr[AXIS_CAPTURE_M + I]       := PmacCapture(IM);
  end;

  for I := 0 to HWCount - 1 do begin
    MVar.Alias[IHW_M + I]      := Format('IHW_%d_M', [I]);
    MVar.Alias[OHW_M + I]      := Format('OHW_%d_M', [I]);
    MVar.PmacPtr[IHW_M + I]    := PmacDPFloat(PmacIHWDPR + (I*4));
    MVar.PmacPtr[OHW_M + I]    := PmacDPFloat(PmacOHWDPR + (I*4));
  end;

  MVar.Alias[_INTROS_READ_Y] := '_READ_DATA_Y';
  MVar.Alias[_INTROS_READ_X] := '_READ_DATA_X';
  MVar.Alias[_INTROS_READ_ADDRESS] := '_INTROS_READ_ADDRESS';
  MVar.Alias[_INTROS_WRITE_ADDRESS] := '_INTROS_WRITE_ADDRESS';
  MVar.Alias[_INTROS_WRITE_Y] := '_WRITE_DATA_Y';
  MVar.Alias[_INTROS_WRITE_X] := '_WRITE_DATA_X';
  MVar.Alias[_INTROS_SIGNALS] := IntrosSignalBits.Name;

  MVar.Alias[_INTROS_INDEX] := '_INTROS_INDEX';
  MVar.Alias[_INTROS_POINTER] := '_INTROS_POINTER';

  MVar.PmacPtr[_INTROS_READ_Y] := PmacDP(PmacIntrosBase);
  MVar.PmacPtr[_INTROS_READ_X] := PmacDP(PmacIntrosReadX);
  MVar.PmacPtr[_INTROS_READ_ADDRESS] := PmacDPYWord(PmacIntrosReadAddress);
  MVar.PmacPtr[_INTROS_WRITE_ADDRESS] := PmacDPXWord(PmacIntrosWriteAddress);
  MVar.PmacPtr[_INTROS_WRITE_Y] := PmacDP(PmacIntrosWriteY);
  MVar.PmacPtr[_INTROS_WRITE_X] := PmacDP(PmacIntrosWriteX);
  MVar.PmacPtr[_INTROS_SIGNALS] := PmacDP(PmacIntrosSignals);

  MVar.PmacPtr[_INTROS_INDEX] := PmacYWord($BC00 + _INTROS_POINTER);
  MVar.PmacPtr[_INTROS_POINTER] := PmacYWord(0);

  for I := 0 to 1023 do begin
    if MVar.Alias[I] <> '' then
      FOutput.Add(MVar.Definition[I]);
  end;

  for I := 0 to 1023 do begin
    if (MVar.Alias[I] <> '') and (MVar.PmacPtr[I] <> '') then
      FOutput.Add(MVar.Assignment[I]);
  end;

  // OUTPUT the Pmappings
  FOutput.Add('');
  for I := 0 to CoordinateSetup[0].NumberOfAxis - 1 do begin
//    IM := CoordinateSetup[0].AxisByDisplay[I].Connection;
    STmp := CoordinateSetup[0].AxisByDisplay[I].AxisDesignation;
    FOutput.Add(Format('#define ENC_POWERON_ZERO_%s_P P%d', [STmp, ENC_POWERON_ZERO_P + I]));
  end;

  FOutput.Add('&1');
  IVar.SetInteger(50, 1);
  IVar.SetInteger(13, 10);
  IVar.SetInteger(14, 1); // automatic pmatch on program start
  IVar.SetFloat(190, 60 * 1000); // Units per minute
  IVar.SetInteger(191, 0);     // Default 0 for panel cycle start
  for Motor := 0 to 7 do begin // For Each coordinate system
    with MotorSetup[Motor] do begin
      IM := (Motor + 1) * 100;
      if Enabled then begin
        IVar.AsInteger[IM] := 1;
        IVar.AsInteger[IM + 8] := 96;
        IVar.AsInteger[IM + 9] := 96;
        IVar.AsInteger[IM + 11] := Round(FatalFollowError * CPUnit * 16);
        IVar.AsInteger[IM + 12] := Round(WarnFollowError * CPUnit * 16);
        IVar.AsFloat[IM + 16] := (MaximumRapidFeed * CPUnit) / (60 * 1000);
        IVar.AsFloat[IM + 22] := (MaximumJogFeed * CPUnit) / (60 * 1000);
        IVar.AsFloat[IM + 30] := Proportional;
        IVar.AsFloat[IM + 31] := Derivative;
        IVar.AsFloat[IM + 33] := Integrator;

        if IsRotary(MotorSetup[Motor]) and MotorSetup[Motor].ShortestPath then
          IVar.AsInteger[IM + 27] := Round(CPUnit * 360)
        else
          IVar.AsInteger[IM + 27] := 0;

        IVar.AsInteger[IM + 28] := Round(InPositionWindow * CPUnit * 16);
      end;
    end;
  end;

  for I := 0 to 1023 do begin
    if IVar.Programmed[I] then
      FOutput.Add(IVar.AsTextAssign[I]);
  end;

  // Generate Prog 1000 (GCode)
  FOutput.Add('#define MCODE_Q Q10');
  FOutput.Add('#define TMP1_Q Q12');
  FOutput.Add('#define TMP2_Q Q13');
  FOutput.Add('#define TMP3_Q Q14');
  FOutput.Add('#define ParamMap Q100');
  for I := 0 to 25 do begin
    STmp := Char(Ord('A') + I);
    FOutput.Add(Format('#define Param%s Q%d', [STmp, 101 + I]));
    FOutput.Add(Format('#define Param%sSet %d', [Stmp, Round(IntPower(2, I))]));
  end;

  FOutput.Add('open prog 1000 clear');
  FOutput.Add('RAPID');
  FOutput.Add(MVar.Alias[GGroupA_M] + ' = 0');
  FOutput.Add('Return');

  FOutput.Add('N1000');
  FOutput.Add('LINEAR');
  FOutput.Add(MVar.Alias[GGroupA_M] + ' = 1');
  FOutput.Add('Return');

  FOutput.Add('N2000');
  FOutput.Add('CIRCLE1');
  FOutput.Add(MVar.Alias[GGroupA_M] + ' = 2');
  FOutput.Add('Return');

  FOutput.Add('N3000');
  FOutput.Add('CIRCLE2');
  FOutput.Add(MVar.Alias[GGroupA_M] + ' = 3');
  FOutput.Add('Return');

  FOutput.Add('N4000');
  FOutput.Add('Read(P)');
  FOutput.Add('if (ParamMap & ParamPSet != 0)');
  FOutput.Add('ParamP = ParamP * 1000.0');
  FOutput.Add('else');
  FOutput.Add('ParamP = 0');
  FOutput.Add('endif');
  FOutput.Add('Dwell(ParamP)');
  FOutput.Add(NCSignalBits.SetOff(Ord(NCS_RELEASE_BLOCKING)));
  FOutput.Add('Return');

  for I := 13 to 15 do begin
    FOutput.Add('N' + IntToStr(I * 1000));
    FOutput.Add(MVar.Alias[GGroupB_M] + ' = ' + IntToStr(I));
    FOutput.Add('Return');
  end;


  FOutput.Add('N16000');
  FOutput.Add('TMP1_Q=0');
  FOutput.Add('TMP2_Q=0');
  FOutput.Add('TMP3_Q=0');

  FOutput.Add('if (ParamMap & ParamISet != 0)');
  FOutput.Add('  TMP1_Q=ParamI');
  FOutput.Add('endif');
  FOutput.Add('if (ParamMap & ParamJSet != 0)');
  FOutput.Add('  TMP2_Q=ParamJ');
  FOutput.Add('endif');
  FOutput.Add('if (ParamMap & ParamKSet != 0)');
  FOutput.Add('  TMP3_Q=ParamK');
  FOutput.Add('endif');

  FOutput.Add('NORMAL I(TMP1_Q)J(TMP2_Q)K(TMP3_Q)');
  FOutput.Add(MVar.Alias[GGroupC_M] + ' = 16');
  FOutput.Add('Return');

  FOutput.Add('N17000');
  FOutput.Add('NORMAL K-1');
  FOutput.Add(MVar.Alias[GGroupC_M] + ' = 17');
  FOutput.Add('Return');

  FOutput.Add('N18000');
  FOutput.Add('NORMAL J-1');
  FOutput.Add(MVar.Alias[GGroupC_M] + ' = 18');
  FOutput.Add('Return');

  FOutput.Add('N19000');
  FOutput.Add('NORMAL I-1');
  FOutput.Add(MVar.Alias[GGroupC_M] + ' = 19');
  FOutput.Add('Return');

  FOutput.Add('N20000');
  FOutput.Add(MVar.Alias[GGroupI_M] + ' = 20');
  FOutput.Add(NCSignalBits.SetOn(Ord(NCS_OFFSET_CHANGE)));
  FOutput.Add('Return');

  FOutput.Add('N21000');
  FOutput.Add(MVar.Alias[GGroupI_M] + ' = 21');
  FOutput.Add(NCSignalBits.SetOn(Ord(NCS_OFFSET_CHANGE)));
  FOutput.Add('Return');

  for I := 40 to 44 do begin
    FOutput.Add('N' + IntToStr(I * 1000));
    FOutput.Add(MVar.Alias[GGroupD_M] + ' = ' + IntToStr(I));
    FOutput.Add('Return');
  end;

  FOutput.Add('N90000');
  FOutput.Add('ABS');
  FOutput.Add(MVar.Alias[GGroupF_M] + ' = 90');
  FOutput.Add('Return');

  FOutput.Add('N91000');
  FOutput.Add('INC');
  FOutput.Add(MVar.Alias[GGroupF_M] + ' = 91');
  FOutput.Add('Return');

  for I := 93 to 95 do begin
    FOutput.Add('N' + IntToStr(I * 1000));
    FOutput.Add(MVar.Alias[GGroupG_M] + ' = ' + IntToStr(I));
    FOutput.Add('Return');
  end;

  for I := 96 to 97 do begin
    FOutput.Add('N' + IntToStr(I * 1000));
    FOutput.Add(MVar.Alias[GGroupH_M] + ' = ' + IntToStr(I));
    FOutput.Add('Return');
  end;

  FOutput.Add('N50');
  for SFA := nfParamX to nfParamW do begin
    if SFA in CoordinateSetup[0].LegalAxisParameters then begin
      OAdd('if (ParamMap & Param%sSet != 0)', [NCParamNames[SFA]]);
      OAdd('  LOCAL_OFFSET_%s_M = Param%s', [NCParamNames[SFA], NCParamNames[SFA]]);
      Foutput.Add('endif');
    end;
  end;
  FOutput.Add(NCSignalBits.SetOn(Ord(NCS_OFFSET_CHANGE)));
  FOutput.Add('Return');

  FOutput.Add('N54');
  for SFA := nfParamX to nfParamW do begin
    if SFA in CoordinateSetup[0].LegalAxisParameters then begin
      FOutput.Add(Format('if (ParamMap & Param%sSet != 0)', [NCParamNames[SFA]]));
      FOutput.Add(Format('  COORD_OFFSET_%s_M = Param%s', [NCParamNames[SFA], NCParamNames[SFA]]));
      FOutput.Add('endif');
    end;
  end;
  FOutput.Add(NCSignalBits.SetOn(Ord(NCS_OFFSET_CHANGE)));
  FOutput.Add('Return');

  FOutput.Add('N92');
  FOutput.Add('if (ParamMap = 0)');
  for SFA := nfParamX to nfParamW do begin
    if SFA in CoordinateSetup[0].LegalAxisParameters then begin
      FOutput.Add(Format('  SHIFT_OFFSET_%s_M = 0', [NCParamNames[SFA]]));
    end;
  end;
  FOutput.Add('else');
  for SFA := nfParamX to nfParamW do begin
    if SFA in CoordinateSetup[0].LegalAxisParameters then begin
      FOutput.Add(Format('if (ParamMap & Param%sSet != 0)', [NCParamNames[SFA]]));

      FOutput.Add(Format('  SHIFT_OFFSET_%s_M = (TARGET_POS_REGISTER_%s_M / (%f * 96 * 32)) - COORD_OFFSET_%s_M - LOCAL_OFFSET_%s_M - Param%s',
             [NCParamNames[SFA],
              NCParamNames[SFA],
              CoordinateSetup[0].AxisByName[SFA].CPUnit,
              NCParamNames[SFA],
              NCParamNames[SFA],
              NCParamNames[SFA]]));


      FOutput.Add(Format('  SHIFT_OFFSET_%s_M = SHIFT_OFFSET_%s_M + (AXIS_ZERO_OFFSET_%s_M / (%f * 96 * 32))',
             [NCParamNames[SFA],
              NCParamNames[SFA],
              NCParamNames[SFA],
              CoordinateSetup[0].AxisByName[SFA].CPUnit]));
      FOutput.Add('endif');
    end;
  end;
  FOutput.Add('endif');

  FOutput.Add(NCSignalBits.SetOn(Ord(NCS_OFFSET_CHANGE)));
  FOutput.Add(Format('while(%s)', [NCSignalBits.IsOn(Ord(NCS_OFFSET_CHANGE))]));
  FOutput.Add('  Dwell 0');
  FOutput.Add('endwhile');
  FOutput.Add(NCSignalBits.SetOff(Ord(NCS_RELEASE_BLOCKING)));
  FOutput.Add('Return');

  FOutput.Add('N50000');
  FOutput.Add('Read(A,B,C,U,V,W,X,Y,Z)');
  FOutput.Add('Gosub 50');
  FOutput.Add('Return');

  FOutput.Add('N51000');
  FOutput.Add('Read(A,B,C,U,V,W,X,Y,Z)');
  FOutput.Add('Gosub 50');
  FOutput.Add('Return');

  FOutput.Add('N52000');
  FOutput.Add('Read(A,B,C,U,V,W,X,Y,Z)');
  FOutput.Add('Gosub 50');
  FOutput.Add('Return');

  FOutput.Add('N53000');
  for SFA := nfParamX to nfParamW do begin
    if SFA in CoordinateSetup[0].LegalAxisParameters then begin
      OAdd('SHIFT_OFFSET_%s_M = 0', [NCParamNames[SFA]]);
      OAdd('LOCAL_OFFSET_%s_M = 0', [NCParamNames[SFA]]);
      OAdd('COORD_OFFSET_%s_M = 0', [NCParamNames[SFA]]);
    end;
  end;
  FOutput.Add(MVar.Alias[GGroupCoord_M] + ' = 53');
  FOutput.Add(NCSignalBits.SetOn(Ord(NCS_OFFSET_CHANGE)));
  FOutput.Add('Return');

  for I := 54 to 59 do begin
    FOutput.Add(Format('N%d', [I*1000]));
    FOutput.Add('Read(A,B,C,U,V,W,X,Y,Z)');
    FOutput.Add('Gosub 54');
    FOutput.Add(Format(MVar.Alias[GGroupCoord_M ] + '= %d', [I]));
    FOutput.Add('Return');
    FOutput.Add('');
  end;

  FOutput.Add('N61000');
  FOutput.Add(MVar.Alias[GGroupJ_M] + ' = 61');
  FOutput.Add('Return');
  FOutput.Add('');

  FOutput.Add('N64000');
  FOutput.Add(MVar.Alias[GGroupJ_M] + ' = 64');
  FOutput.Add('Return');
  FOutput.Add('');

  FOutput.Add('N92000');
  FOutput.Add('Read(A,B,C,U,V,W,X,Y,Z)');
  FOutput.Add('Gosub 92');
  FOutput.Add('Return');
  FOutput.Add('');

  FOutput.Add('N93000');
  FOutput.Add(MVar.Alias[GGroupF_M] + ' = 93');
  FOutput.Add('Return');
  FOutput.Add('');

  FOutput.Add('N94000');
  FOutput.Add(MVar.Alias[GGroupF_M] + ' = 94');
  FOutput.Add('Return');
  FOutput.Add('');

  FOutput.Add('Close');

  // Generate Prog 1001 (MCode)
  FOutput.Add('OPEN PROG 1001 CLEAR');
  FOutput.Add(NCSignalBits.SetOff(Ord(NCS_RELEASE_BLOCKING)));
  FOutput.Add('Return');
  FOutput.Add('N00005');
  FOutput.Add('  WHILE (' + MVar.Alias[NC_MCODE_M] + '!=0)');
//  FOutput.Add('    DWELL0'); // Test
  FOutput.Add('  ENDWHILE');
  FOutput.Add(Format('  WHILE (%s)', [PlcSignalBits.IsOn(Ord(PLCS_MCODE_AFC))]));
//  FOutput.Add('    DWELL0'); // Test
  FOutput.Add('  ENDWHILE');
  FOutput.Add('  ' + MVar.Alias[NC_MCODE_M] + '=MCODE_Q');
  FOutput.Add('  WHILE (' + MVar.Alias[NC_MCODE_M] + '!=0)');
//  FOutput.Add('    DWELL0'); // Test
  FOutput.Add('  ENDWHILE');
  FOutput.Add(Format('  WHILE (%s)', [PlcSignalBits.IsOn(Ord(PLCS_MCODE_AFC))]));
//  FOutput.Add('    DWELL0'); // Test
  FOutput.Add('  ENDWHILE');
//  FOutput.Add(NCSignalBits.SetOff(Ord(NCS_RELEASE_BLOCKING)));
  FOutput.Add('RETURN');
  FOutput.Add('');
  FOutput.Add('N00010');
  FOutput.Add('  DWELL0');
  FOutput.Add('  GOSUB 5');
  FOutput.Add('RET');
  for I := 1 to 99 do begin
    FOutput.Add(Format('N%d', [I * 1000]));
    FOutput.Add(Format('  MCODE_Q=%d', [I]));
    FOutput.Add('  GOSUB 10');
    case I of
      1 : begin
         FOutput.Add(NCSignalBits.SetOn(Ord(NCS_RELEASE_BLOCKING)));
         FOutput.Add(Format('  IF(M%d&PCS_OPTIONAL_STOP!=0)', [PC_SIGNAL_M]));
         FOutput.Add('    STOP');
         FOutput.Add('  ENDIF');
         FOutput.Add('  DWELL0');
         FOutput.Add(NCSignalBits.SetOff(Ord(NCS_RELEASE_BLOCKING)));
      end;

      30 : begin
         FOutput.Add(NCSignalBits.SetOn(Ord(NCS_PROGRAM_REWIND)));
      end;
    end;
//    FOutput.Add('  DWELL0');
    FOutput.Add('RET');
    FOutput.Add('');
  end;
  FOutput.Add('Close');
  FOutput.Add('');

  // Generate Prog 1001 (MCode)
  FOutput.Add('OPEN PROG 1011 CLEAR');
  FOutput.Add(NCSignalBits.SetOff(Ord(NCS_RELEASE_BLOCKING)));
  FOutput.Add('Return');
  FOutput.Add('N00005');
  FOutput.Add('  WHILE (' + MVar.Alias[NC_MCODE_M] + '!=0)');
  FOutput.Add('  ENDWHILE');
  FOutput.Add(Format('  WHILE (%s)', [PlcSignalBits.IsOn(Ord(PLCS_MCODE_AFC))]));
  FOutput.Add('  ENDWHILE');
  FOutput.Add('  ' + MVar.Alias[NC_MCODE_M] + '=MCODE_Q');
  FOutput.Add('  WHILE (' + MVar.Alias[NC_MCODE_M] + '!=0)');
  FOutput.Add('  ENDWHILE');
  FOutput.Add(Format('  WHILE (%s)', [PlcSignalBits.IsOn(Ord(PLCS_MCODE_AFC))]));
  FOutput.Add('  ENDWHILE');
  FOutput.Add('RETURN');
  FOutput.Add('');
  FOutput.Add('N00010');
  FOutput.Add('  GOSUB 5');
  FOutput.Add('RET');
  for I := 101 to 199 do begin
    FOutput.Add(Format('N%d', [(I - 100) * 1000]));
    FOutput.Add(Format('  MCODE_Q=%d', [I]));
    FOutput.Add('  GOSUB 10');
    FOutput.Add('RET');
    FOutput.Add('');
  end;
  FOutput.Add('Close');

  FOutput.Add(Format('%s=0', [MVar.Alias[CRITICAL_ERRORS_M]]));

  // Reset Plcc1
  FOutput.Add('Open Plcc 1');
  // Every sweep encoder loss detect code

  if EncoderLossDetect then begin
    if Assigned(CoordinateSetup[0].AxisByConnection[0]) or
       Assigned(CoordinateSetup[0].AxisByConnection[4]) then begin
      FOutput.Add(Format('if (%s)', [EncoderLossBits.IsOff(Ord(EL_ENC_1_5_LOSS))]));
      FOutput.Add(Format('  if (%s & 1 = 0)', [MVar.Alias[CRITICAL_ERRORS_M]]));
      FOutput.Add('    CMD "&1A#1K#5K"');
      FOutput.Add('  endif');
      FOutput.Add(Format('  %s=%s | 1', [MVar.Alias[CRITICAL_ERRORS_M], MVar.Alias[CRITICAL_ERRORS_M]]));
      FOutput.Add('endif');
    end;

    if Assigned(CoordinateSetup[0].AxisByConnection[1]) or
       Assigned(CoordinateSetup[0].AxisByConnection[5]) then begin
      FOutput.Add(Format('if (%s)', [EncoderLossBits.IsOff(Ord(EL_ENC_2_6_LOSS))]));
      FOutput.Add(Format('  if (%s & 2 = 0)', [MVar.Alias[CRITICAL_ERRORS_M]]));
      FOutput.Add('    CMD "&1A#2K#6K"');
      FOutput.Add('  endif');
      FOutput.Add(Format('  %s=%s | 2', [MVar.Alias[CRITICAL_ERRORS_M], MVar.Alias[CRITICAL_ERRORS_M]]));
      FOutput.Add('endif');
    end;

    if Assigned(CoordinateSetup[0].AxisByConnection[2]) or
       Assigned(CoordinateSetup[0].AxisByConnection[6]) then begin
      FOutput.Add(Format('if (%s)', [EncoderLossBits.IsOff(Ord(EL_ENC_3_7_LOSS))]));
      FOutput.Add(Format('  if (%s & 4 = 0)', [MVar.Alias[CRITICAL_ERRORS_M]]));
      FOutput.Add('    CMD "&1A#3K#7K"');
      FOutput.Add('  endif');
      FOutput.Add(Format('  %s=%s | 4', [MVar.Alias[CRITICAL_ERRORS_M], MVar.Alias[CRITICAL_ERRORS_M]]));
      FOutput.Add('endif');
    end;

    if Assigned(CoordinateSetup[0].AxisByConnection[3]) or
       Assigned(CoordinateSetup[0].AxisByConnection[7]) then begin
      FOutput.Add(Format('if (%s)', [EncoderLossBits.IsOff(Ord(EL_ENC_4_8_LOSS))]));
      FOutput.Add(Format('  if (%s & 8 = 0)', [MVar.Alias[CRITICAL_ERRORS_M]]));
      FOutput.Add('    CMD "&1A#4K#8K"');
      FOutput.Add('  endif');
      FOutput.Add(Format('  %s=%s | 8', [MVar.Alias[CRITICAL_ERRORS_M], MVar.Alias[CRITICAL_ERRORS_M]]));
      FOutput.Add('endif');
    end;
  end;

  FOutput.Add(Format('if (%s)', [NCSignalBits.IsOn(Ord(NCS_RESETREWIND))]));
//  FOutput.Add('if (@ON(NC_SIGNAL_M, NCS_RESETREWIND))');
  for SFA := nfParamX to nfParamW do begin
    if SFA in CoordinateSetup[0].LegalAxisParameters then begin
      OAdd('COORD_OFFSET_%s_M = 0', [NCParamNames[SFA]]);
      OAdd('SHIFT_OFFSET_%s_M = 0', [NCParamNames[SFA]]);
      OAdd('LOCAL_OFFSET_%s_M = 0', [NCParamNames[SFA]]);
    end;
  end;
  FOutput.Add(MVar.Alias[NC_MCODE_M] + ' = 0');
  FOutput.Add(MVar.Alias[NC_SCODE_M] + ' = 0');
  FOutput.Add(MVar.Alias[NC_TCODE_M] + ' = 0');

  FOutput.Add(MVar.Alias[GGroupA_M] + ' = 1');
  FOutput.Add(MVar.Alias[GGroupB_M] + ' = 13');
  FOutput.Add(MVar.Alias[GGroupC_M] + ' = 17');
  FOutput.Add(MVar.Alias[GGroupD_M] + ' = 40');
  FOutput.Add(MVar.Alias[GGroupF_M] + ' = 90');
  FOutput.Add(MVar.Alias[GGroupG_M] + ' = 94');
  FOutput.Add(MVar.Alias[GGroupH_M] + ' = 96');
  if not CoordinateSetup[0].InchDefault then
    FOutput.Add(MVar.Alias[GGroupI_M] + ' = 21')
  else
    FOutput.Add(MVar.Alias[GGroupI_M] + ' = 20');

  FOutput.Add(MVar.Alias[GGroupJ_M] + ' = 61');
  FOutput.Add(MVar.Alias[GGroupUnique_M] + ' = 0');
  FOutput.Add(MVar.Alias[GGroupCoord_M] + ' = 53');

  if FileExists(ProfilePath + NCPmacDirectory + 'app_reset.txt') then
    FOutput.Add('#include "app_reset.txt"');

  FOutput.Add(NCSignalBits.SetOff(Ord(NCS_RESETREWIND)));
  FOutput.Add('endif');

  FOutput.Add(Format('if (%s)', [NCSignalBits.IsOn(Ord(NCS_RESET))]));
  FOutput.Add(MVar.Alias[NC_MCODE_M] + ' = 0');
  FOutput.Add(MVar.Alias[NC_SCODE_M] + ' = 0');
  FOutput.Add(MVar.Alias[NC_SCODE_M] + ' = 0');
  FOutput.Add(NCSignalBits.SetOff(Ord(NCS_RESET)));
  FOutput.Add('endif');

  FOutput.Add(Format('if (%s)', [PlcSignalBits.IsOn(Ord(PLCS_MCODE_AFC))]));
  FOutput.Add('NC_MCODE_M = 0');
  FOutput.Add('endif');

  FOutput.Add('close');

  for I := 0 to CoordinateSetup[0].NumberOfAxis - 1 do begin
    PM := CoordinateSetup[0].AxisByDisplay[I];
    IM := PM.Connection;
    STmp := PM.AxisDesignation;

    OAdd('open prog %d clear', [CoordinateSetup[0].AxisByDisplay[I].HomeProgram]);
    OAdd('I%d13=0', [IM + 1]);
    OAdd('I%d14=0', [IM + 1]);

//    OAdd('HOME_GRID_%s_M = ENCODER_%s_M / (%f * 2)', [STmp, STmp, PM.CPUnit]);
    OAdd('@SET_OFF(MOTOR_STATUS_%s_M, %d)', [STmp, Round(IntPower(2, 10))]);

    if PmacVersion = 1 then
      HMFLn := 20
    else
      HMFLn := 16;

    if PM.HomeDogSearchSense then
      OAdd('while(@ON(ENC_STATUS_%s_M, %d))', [STmp, Round(IntPower(2, HMFLn))])
    else
      OAdd('while(@OFF(ENC_STATUS_%s_M, %d))', [STmp, Round(IntPower(2, HMFLn))]);


    FOutput.Add('F600 LIN');
    OAdd('INC %s%f', [STmp, PM.HomeDogSearch * 6]);
    FOutput.Add('endw');

    if not PM.HomeDogSearchSense then
      OAdd('while(@ON(ENC_STATUS_%s_M, %d))', [STmp, Round(IntPower(2, HMFLn))])
    else
      OAdd('while(@OFF(ENC_STATUS_%s_M, %d))', [STmp, Round(IntPower(2, HMFLn))]);


    FOutput.Add('F300 LIN');
    OAdd('INC %s%f', [STmp, -PM.HomeDogSearch]);
    FOutput.Add('Dwell0');
    FOutput.Add('endw');

    FOutput.Add(Format('ENC_POWERON_ZERO_%s_P = AXIS_CAPTURE_%s_M', [STmp, STmp]));
    OAdd('I%d23=%f', [IM + 1, PM.HomeFlagSearch]);
    OAdd('I%d26=0', [IM + 1]);
    OAdd('I%d=0', [ICaptureFlag(IM)]);
    OAdd('I%d=1', [ICaptureControl(IM)]);
    FOutput.Add('Dwell0');
    OAdd('HOME_GRID_%s_M = ENCODER_%s_M / (%f * 2)', [STmp, STmp, PM.CPUnit]);
    OAdd('HOME%d', [IM + 1]);
    FOutput.Add('Dwell0');

    FOutput.Add(Format('ENC_POWERON_ZERO_%s_P = AXIS_CAPTURE_%s_M', [STmp, STmp]));

    OAdd('HOME_GRID_%s_M = HOME_GRID_%s_M - (ENCODER_%s_M / (%f * 2))', [STmp, STmp, STmp, PM.CPUnit]);
    FOutput.Add('close');
    FOutput.Add('');
  end;

  FOutput.Add('open plcc 2 clear');
  FOutput.Add(IntrosSignalBits.SetOff(Ord(IS_READ_STRB)));
  FOutput.Add(Format('%s = 0', [PlcSignalBits.Name]));
  FOutput.Add('while(1<2)');
  FOutput.Add(Format('if(%s)', [PlcSignalBits.IsOn(Ord(PLCS_INTROS_READ))]));
  FOutput.Add(Format('%s = $780000 + %s', [MVar.Alias[_INTROS_INDEX], MVar.Alias[_INTROS_READ_ADDRESS]]));
  FOutput.Add(Format('%s = %s', [MVar.Alias[_INTROS_READ_Y], MVar.Alias[_INTROS_POINTER]]));
  FOutput.Add(Format('%s = $E80000 + %s', [MVar.Alias[_INTROS_INDEX], MVar.Alias[_INTROS_READ_ADDRESS]]));
  FOutput.Add(Format('%s = %s', [MVar.Alias[_INTROS_READ_X], MVar.Alias[_INTROS_POINTER]]));
  FOutput.Add(IntrosSignalBits.SetOn(Ord(IS_READ_STRB)));
  FOutput.Add(Format('while(%s)', [PlcSignalBits.IsOn(Ord(PLCS_INTROS_READ))]));
  FOutput.Add('endwhile');
  FOutput.Add(IntrosSignalBits.SetOff(Ord(IS_READ_STRB)));
  FOutput.Add('endif');
  FOutput.Add('endwhile');
  FOutput.Add('close');

  FOutput.Add('');

  FOutput.Add('open plcc 3 clear');
  FOutput.Add(Format('%s = 0', [IntrosSignalBits.Name]));
  FOutput.Add('while(1<2)');
  FOutput.Add(Format('if(%s)', [PlcSignalBits.IsOn(Ord(PLCS_INTROS_WRITE_X))]));
  FOutput.Add(Format('%s = $E80000 + %s', [MVar.Alias[_INTROS_INDEX], MVar.Alias[_INTROS_WRITE_ADDRESS]]));
  FOutput.Add(Format('%s = %s', [MVar.Alias[_INTROS_POINTER], MVar.Alias[_INTROS_WRITE_X]]));

  FOutput.Add(IntrosSignalBits.SetOn(Ord(IS_WRITE_X_STRB)));
  FOutput.Add(Format('while(%s)', [PlcSignalBits.IsOn(Ord(PLCS_INTROS_WRITE_X))]));
  FOutput.Add('endwhile');
  FOutput.Add(IntrosSignalBits.SetOff(Ord(IS_WRITE_X_STRB)));
  FOutput.Add('endif');

  FOutput.Add(Format('if(%s)', [PlcSignalBits.IsOn(Ord(PLCS_INTROS_WRITE_Y))]));
  FOutput.Add(Format('%s = $780000 + %s', [MVar.Alias[_INTROS_INDEX], MVar.Alias[_INTROS_WRITE_ADDRESS]]));
  FOutput.Add(Format('%s = %s', [MVar.Alias[_INTROS_POINTER], MVar.Alias[_INTROS_WRITE_Y]]));

  FOutput.Add(IntrosSignalBits.SetOn(Ord(IS_WRITE_Y_STRB)));
  FOutput.Add(Format('while(%s)', [PlcSignalBits.IsOn(Ord(PLCS_INTROS_WRITE_Y))]));
  FOutput.Add('endwhile');
  FOutput.Add(IntrosSignalBits.SetOff(Ord(IS_WRITE_Y_STRB)));
  FOutput.Add('endif');


  FOutput.Add(Format('if(%s)', [PlcSignalBits.IsOn(Ord(PLCS_INTROS_WRITE_D))]));
  FOutput.Add(Format('%s = $e80000 + %s', [MVar.Alias[_INTROS_INDEX], MVar.Alias[_INTROS_WRITE_ADDRESS]]));
  FOutput.Add(Format('%s = %s', [MVar.Alias[_INTROS_POINTER], MVar.Alias[_INTROS_WRITE_X]]));

  FOutput.Add(Format('%s = $780000 + %s', [MVar.Alias[_INTROS_INDEX], MVar.Alias[_INTROS_WRITE_ADDRESS]]));
  FOutput.Add(Format('%s = %s', [MVar.Alias[_INTROS_POINTER], MVar.Alias[_INTROS_WRITE_Y]]));

  FOutput.Add(IntrosSignalBits.SetOn(Ord(IS_WRITE_D_STRB)));
  FOutput.Add(Format('while(%s)', [PlcSignalBits.IsOn(Ord(PLCS_INTROS_WRITE_D))]));
  FOutput.Add('endwhile');
  FOutput.Add(IntrosSignalBits.SetOff(Ord(IS_WRITE_D_STRB)));
  FOutput.Add('endif');

  FOutput.Add('endwhile');
  FOutput.Add('close');

  FOutput.Add('');
  OAdd('#include "%s"', [ProfilePath + NCPmacDirectory + PostFile]);
end;

procedure TPmacIVariable.Clear;
var I : Integer;
begin
  for I := 0 to 1023 do begin
    FValues[I].D := 0;
    FValues[I].DataType := pmiUndefined;
  end;
end;

procedure TPmacIVariable.SetInteger(aIndex, aValue : Integer);
begin
  FValues[aIndex].DataType := pmiInteger;
  FValues[aIndex].I := aValue;
end;

procedure TPmacIVariable.SetFloat(aIndex : Integer; aValue : Double);
begin
  FValues[aIndex].DataType := pmiDouble;
  FValues[aIndex].D := aValue;
end;

function TPmacIVariable.GetInteger(aIndex : Integer) : Integer;
begin
  case FValues[aIndex].DataType of
    pmiUndefined : raise Exception.CreateFmt('IVar Not Programmed : %d', [aIndex]);
    pmiInteger : Result := FValues[aIndex].I;
    pmiDouble : Result := Round(FValues[aIndex].D);
  else
    raise Exception.Create('RTPE TPmacIVariable.GetInteger');
  end;
end;

function TPmacIVariable.GetFloat(aIndex : Integer) : Double;
begin
  case FValues[aIndex].DataType of
    pmiUndefined : raise Exception.CreateFmt('IVar Not Programmed : %d', [aIndex]);
    pmiInteger : Result := FValues[aIndex].I;
    pmiDouble : Result := FValues[aIndex].D;
  else
    raise Exception.Create('RTPE TPmacIVariable.GetFloat');
  end;
end;

function TPmacIVariable.GetTextAssign(aIndex : Integer) : string;
begin
  case FValues[aIndex].DataType of
    pmiUndefined : raise Exception.CreateFmt('IVar Not Programmed : %d', [aIndex]);
    pmiInteger : Result := Format('I%d=%d', [aIndex, FValues[aIndex].I]);
    pmiDouble : Result := Format('I%d=%f', [aIndex, FValues[aIndex].D]);
  end;
end;

function TPmacIVariable.GetProgrammed(aIndex : Integer) : Boolean;
begin
  Result := FValues[aIndex].DataType <> pmiUndefined;
end;





function TPmacMVariable.GetDefinition(Index : Integer) : string;
begin
  Result := Format('#define %s M%d', [Alias[Index], Index]);
end;

function TPmacMVariable.GetAssignment(Index : Integer) : string;
begin
  Result := Format('%s->%s', [Alias[Index], PmacPtr[Index]]);
end;

procedure TPmacMVariable.SetPmacPtr(Index : Integer; aText : string);
begin
  FValues[Index].Definition := aText;
end;

procedure TPmacMVariable.SetAlias(Index : Integer; aText : string);
begin
  FValues[Index].Alias := aText;
end;

function TPmacMVariable.GetPmacPtr(Index : Integer) : string;
begin
  Result := FValues[Index].Definition;
end;

function TPmacMVariable.GetAlias(Index : Integer) : string;
begin
  Result := FValues[Index].Alias;
end;


constructor TPmacConstList.Create(aName : string);
begin
  inherited Create;
  Name := aName;
end;

procedure TPmacConstList.WriteDefines(S : TStrings);
var I : Integer;
begin
  S.Add('// Defines for ' + Name);
  for I := 0 to Count - 1 do
    S.Add(Format('#define %s %d', [Strings[I], Power(I)]));
  S.Add('');
end;

function TPmacConstList.IsOn(aValue : Integer) : string;
begin
  Result := Format('@ON(%s, %s)', [Name, Strings[aValue]]);
end;

function TPmacConstList.IsOff(aValue : Integer) : string;
begin
  Result := Format('@OFF(%s, %s)', [Name, Strings[aValue]]);
end;

function TPmacConstList.SetOn(aValue : Integer) : string;
begin
  Result := Format('@SET_ON(%s, %s)', [Name, Strings[aValue]]);
end;

function TPmacConstList.SetOff(aValue : Integer) : string;
begin
  Result := Format('@SET_OFF(%s, %s)', [Name, Strings[aValue]]);
end;

class function TPmacConstList.Power(aValue : Integer) : Cardinal;
begin
  if aValue < 31 then
    Result := Round(IntPower(2, aValue))
  else
    Result := $80000000;
end;

end.
