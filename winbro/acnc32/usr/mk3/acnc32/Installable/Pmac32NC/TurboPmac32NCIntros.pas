unit TurboPmac32NCIntros;

interface

uses CNC32, CNCTypes, CoreCNC, Classes, TurboPmac32NCConfigure, PComm32, PmacCommon;

type
  TPmacMemoryValue = (
    pmvTX,
    pmvRX
  );

  TPmacMemoryValues = set of TPmacMemoryValue;

  TPmacDataUnits = (  pduASIs,                // Straight Double to Integer
                      pduScaled32,            // Conversion by
                      pduEncoder,             //
                      pduSoftLimit,           //
                      pduPseudoL              // PC * 1000 > Pmac
  );

  TTurboPmac32Intros = class(TObject)
  private
    ReadList : TList;
    WriteList : TList;
    PComm : TPcomm32;
    ReadStage, WriteStage : Boolean;
    RPMem, WPMem : TPmacMemoryItem;
  public
    constructor Create(aPcomm : TPcomm32);
    procedure ReadItem(aItem : TPmacMemoryItem);
    procedure WriteItem(aItem : TPmacMemoryItem);
    procedure Sweep;
  end;

implementation

constructor TTurboPmac32Intros.Create(aPcomm : TPcomm32);
begin
  inherited Create;

  Pcomm := aPcomm;
  ReadList := TList.Create;
  WriteList := TList.Create;
end;

procedure   TTurboPmac32Intros.Sweep;
var tmp : Integer;
begin
  Exit;
  case ReadStage of
    False : if (ReadList.Count > 0) and
            (Pcomm.IDPR[PmacIntrosSignals] and IntTwoPower(Ord(IS_READ_STRB)) = 0) then begin

       RPmem := TPmacMemoryItem(ReadList[0]);
       Pcomm.IDPR[PmacIntrosReadAddress] := RPmem.Address;
       Pcomm.IDPR[PmacPlcSignalDPR] := Pcomm.IDPR[PmacPlcSignalDPR] or IntTwoPower(Ord(PLCS_INTROS_READ));
       ReadStage := True;
    end;

    True  : if (Pcomm.IDPR[PmacIntrosSignals] and IntTwoPower(Ord(IS_READ_STRB)) <> 0) then begin
       Pcomm.IDPR[PmacPlcSignalDPR] := Pcomm.IDPR[PmacPlcSignalDPR] and not IntTwoPower(Ord(PLCS_INTROS_READ));
       RPmem.ValueX := Pcomm.IDPR[PmacIntrosReadX];
       RPmem.ValueY := Pcomm.IDPR[PmacIntrosReadY];
       ReadList.Delete(0);
       if Assigned(RPMem.DoneEv) then
         RPMem.DoneEv(RPmem);
       ReadStage := False;
    end;
  end;

  case WriteStage of
    False: if(WriteList.Count > 0) and
             (Pcomm.IDPR[PmacIntrosSignals] and (IntTwoPower(Ord(IS_WRITE_Y_STRB)) +
                                                 IntTwoPower(Ord(IS_WRITE_X_STRB)) +
                                                 IntTwoPower(Ord(IS_WRITE_D_STRB))) = 0) then begin
       WPmem := TPmacMemoryItem(WriteList[0]);
       Pcomm.IDPR[PmacIntrosWriteAddress] := WPmem.Address;
       Pcomm.IDPR[PmacIntrosWriteX] := WPmem.ValueX;
       Pcomm.IDPR[PmacIntrosWriteY] := WPmem.ValueY;
       Tmp := Pcomm.IDPR[PmacPlcSignalDPR];
       case WPmem.DataType of
         pdtXWord : begin
           Tmp := Tmp or (IntTwoPower(Ord(PLCS_INTROS_WRITE_X)));
           Tmp := Tmp and not (IntTwoPower(Ord(PLCS_INTROS_WRITE_Y)));
           Tmp := Tmp and not (IntTwoPower(Ord(PLCS_INTROS_WRITE_D)));
           Pcomm.IDPR[PmacPlcSignalDPR] := Tmp;
         end;

         pdtYWord : begin
           Tmp := Tmp and not (IntTwoPower(Ord(PLCS_INTROS_WRITE_X)));
           Tmp := Tmp or (IntTwoPower(Ord(PLCS_INTROS_WRITE_Y)));
           Tmp := Tmp and not (IntTwoPower(Ord(PLCS_INTROS_WRITE_D)));
           Pcomm.IDPR[PmacPlcSignalDPR] := Tmp;
         end;

         pdtLWord,
         pdtDWord : begin
           Tmp := Tmp and not (IntTwoPower(Ord(PLCS_INTROS_WRITE_X)));
           Tmp := Tmp and not (IntTwoPower(Ord(PLCS_INTROS_WRITE_Y)));
           Tmp := Tmp or (IntTwoPower(Ord(PLCS_INTROS_WRITE_D)));
           Pcomm.IDPR[PmacPlcSignalDPR] := Tmp;
         end;
       end;
       WriteStage := True;;
    end;

    True :  begin
       case WPmem.DataType of
         pdtXWord : Tmp := Pcomm.IDPR[PmacIntrosSignals] and IntTwoPower(Ord(IS_WRITE_X_STRB));
         pdtYWord : Tmp := Pcomm.IDPR[PmacIntrosSignals] and IntTwoPower(Ord(IS_WRITE_Y_STRB));
         pdtLWord,
         pdtDWord : Tmp := Pcomm.IDPR[PmacIntrosSignals] and IntTwoPower(Ord(IS_WRITE_D_STRB));
       else
         Tmp := 0;  // Bit of a frig ( Should never occur )
       end;

       if Tmp <> 0 then begin
         Tmp := Pcomm.IDPR[PmacPlcSignalDPR];
         Tmp := Tmp and not (IntTwoPower(Ord(PLCS_INTROS_WRITE_X)));
         Tmp := Tmp and not (IntTwoPower(Ord(PLCS_INTROS_WRITE_Y)));
         Tmp := Tmp and not (IntTwoPower(Ord(PLCS_INTROS_WRITE_D)));
         Pcomm.IDPR[PmacPlcSignalDPR] := Tmp;
         WriteList.Delete(0);
         if Assigned(WPmem.DoneEv) then
           WPmem.DoneEv(WPmem);
         WriteStage := False;
       end;
    end;
  end;

end;


procedure TTurboPmac32Intros.ReadItem(aItem : TPmacMemoryItem);
begin
  ReadList.Add(aItem);
end;

procedure TTurboPmac32Intros.WriteItem(aItem : TPmacMemoryItem);
begin
  WriteList.Add(aItem);
end;

end.
