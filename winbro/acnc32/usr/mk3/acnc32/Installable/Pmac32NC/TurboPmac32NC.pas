unit TurboPmac32NC;

interface

uses Classes, SysUtils, CNCTypes, CNC32, CoreCNC, Windows, PComm32, NC32, NCNames,
     Tokenizer, TurboPmac32NCConfigure, NCProgram, NC32Types,
     NC32ConfigurationForm, TurboPmac32NCValues, CNCUtils, Named, CNCAbs,
     FaultRegistration, SyncObjs, INamedInterface, InlineTimer;

type
  TNCCommandType = (
    ctJogPlus,
    ctJogMinus,
    ctCycleStart,
    ctFeedHold,
    ctCycleStop,
    ctCycleAbort,
    ctSingleBlock,
    ctJogReturnPlus,
    ctJogReturnMinus,
    ctReleaseAxes
  );

  TNCCommand = class(TObject)
    Command : TNCCommandType;
    Data : Integer;
    Condition : Boolean;
  public
    constructor Create(aCommand : TNCCommandType; aData : Integer; aCondition : Boolean);
  end;

  TTurboPmacNCHAL = class;

  TPmacTag = class(TDoubleTag)
  protected
    procedure SetAsDouble(AValue: Double); override;
  public
    constructor Create(aOwner : TTagOwner; aName : string); override;
  end;

  TPmacJogThread = class(TThread)
  private
    //HAL : TTurboPmacNCHAL;
    Pcomm: TPcomm32;
    HAL: TTurboPmacNCHAL;
    CommandList : TList;
    Lock: TCriticalSection;
  public
    constructor Create(ADevice: Integer; AHAL: TTurboPmacNCHAL);
    procedure Execute; override;
    procedure ProcessCommand(aCommand : TNCCommand);
    procedure Add(Command : TNCCommand);
  end;

  TTurboPmacNCHAL = class(TNCHAL)
  private
    FPcomm : TPcomm32;
    Config : TTurboPmacNCConfigure;
    CoordStatus1 : TPmacCoordSystemStatii1;
//    CoordStatus2 : TPmacCoordSystemStatii2;
    FTargetFeedRate : Double;
    NCSweep : Integer;
    RotaryRemaining : Integer;
    ProgrammedFeedOverride: Double;
    CurrentFeedOverride : Double;
    //Intros : TTurboPmac32Intros;
    Pmac32NCValues : TTurboPmac32NCValues;
    JogThread : TPmacJogThread;
    DisplayUnits : Double;

    LastGGroup03 : Integer;
    LastGGroup47 : Integer;
    LastGGroup811 : Integer;

    FDeviceIndex : Integer;
    NCSignals: TNCSignalBits;
    PCSignals: TPCSignalBits;
    PlcSignals: TPlcSignalBits;

    ResetRewindStage: Integer;

    HomeCompleteDelay: Double;
    DisplaySweep: Boolean;
    AxisRelease: Boolean;
    LastWatchdog: Boolean;
    LastWatchdogChange: Int64;

    WriteMetricTimer: TInlineTimer;
    ResetRewindOneShot: Boolean;
    WriteOffsetsFirstTime: Boolean;

    function GetForceBlocking : Boolean;
    procedure SetForceBlocking(aOn : Boolean);
    procedure IssueAsciiPcommCommand(const Cmd : string);
    procedure UpdateAxisPosition(I: Integer; ATag: TAxisPositionTag);
    procedure FullResetRewind;
    procedure DefineLookAhead;
    procedure DownloadCompfile(ATag: TAbstractTag);
    procedure WatchLog(Value: Integer);
  protected
    procedure SetAxisPositionTags(APList : TList); override; // List of TAxisPositionTag
    procedure SetJogPlus(aAxis : Integer; aOn : Boolean); override;
    procedure SetJogMinus(aAxis : Integer; aOn : Boolean); override;
    procedure SetCycleStart(aOn : Boolean); override;
    procedure SetFeedHold(aOn : Boolean); override;
    procedure SetCycleStop(aOn : Boolean); override;
    procedure SetCycleAbort(aOn: Boolean); override;
    procedure SetSingleBlock(aOn : Boolean); override;
    procedure SetOptionalStop(aOn : Boolean); override;
    procedure SetBlockDelete(aOn : Boolean); override;
    procedure SetFeedOverride(aFeed : Double); override;
    procedure SetFeedOverrideEnable(aOn : Boolean); override;
    procedure SetJogMode(aMode : TJogModes); override;
    procedure SetJogIncrement(aIncrement : Integer); override;

    function GetJogPlus(aAxis : Integer) : Boolean; override;
    function GetJogMinus(aAxis : Integer) : Boolean; override;
    function GetCycleStart : Boolean; override;
    function GetFeedHold : Boolean; override;
    function GetCycleStop : Boolean; override;
    function GetCycleAbort: Boolean; override;
    function GetFeedOverride : Double; override;
    function GetAxisNames : string; override;
    procedure SetRequestCNCMode(aMode : TCNCMode); override;
    function GetTargetFeedrate : Double; override;
    function GetActualFeedrate : Double; override;
    function GetAxisType(Index : Integer) : TAxisType; override;

    procedure UpdateHardware; override;
    procedure ExecutePartProgram; override;
    property ForceBlocking : Boolean read GetForceBlocking write SetForceBlocking;
    procedure UpdatePersistent(aTag : TAbstractTag); override;
    procedure SetActiveAxis(aAxis : Integer); override;
    procedure ExtFunction(aFunc : Integer); override;
    procedure UpdateActiveOffset; override;
    function ResetRewindComplete: Boolean; override;
    procedure Execute; override;
    procedure SetReleaseAxes(Release: Boolean); override;
    procedure SetNoCycle(NoC: Boolean); override;
  public
    constructor Create(aTagOwner : TNC32; aDeviceIndex : Integer); override;
    function CreateConfig : TNC32Configure; override;
    destructor Destroy; override;
    procedure Configure; override;
    procedure VCLUpdate; override;
    procedure TestPoint; override;
    procedure Clean; override;
    procedure Reset; override;
    procedure ResetRewind; override;
    property Pcomm : TPcomm32 read FPcomm;
  end;

  TTurboNCIOSystem = class(TAbstractIOAccessor)
  private
    Host: IIOSystemAccessorHost;
    FTextDef: string;
    FInputSize: Integer;
    FOutputSize: Integer;

    // Dummy areas used when no IO system is initialized
    InArea: array [0..DPRIOLength] of Byte;
    OutArea: array [0..DPRIOLength] of Byte;

    FHealthy: Boolean;
    PcommIn: Pointer;
    PcommOut: Pointer;
    TurboHAL: TTurboPmacNCHAL;
  public
    function GetInfoPageContent(aPage : Integer) : WideString; override;
    procedure Initialise(Host: IIOSystemAccessorHost; const Params : WideString); override;
    procedure UpdateShadowToReal; override;
    procedure UpdateRealToShadow; override;
    function Healthy : Boolean; override;
    procedure Reset; override;
    function LastError : WideString; override;
    function GetInputArea: Pointer; override;
    function GetOutputArea: Pointer; override;
    function GetInputSize: Integer; override;
    function GetOutputSize: Integer; override;
    function TextDefinition: WideString; override;
    procedure Shutdown; override;
  end;

implementation

uses Math;

const
  NoLookAhead = 0;

constructor TTurboPmacNCHAL.Create(aTagOwner : TNC32; aDeviceIndex : Integer);
begin
  inherited Create(aTagOwner, aDeviceIndex);
  FDeviceIndex := aDeviceIndex;
  DisplayUnits:= 1;

  CreateDirectory(PChar(ProfilePath + NCPmacDirectory), nil);
  FPcomm := TPcomm32.Create(pvTurbo, True);
  Pcomm.InputStart:= InputPmacBaseDPR;
  Pcomm.InputLength:= InputDPRLength;
  Pcomm.OutputStart:= OutputPmacBaseDPR;
  Pcomm.OutputLength:= OutputDPRLength;

  Pcomm.OpenPmacNumber(FDeviceIndex);

  WriteMetricTimer:= TInlineTimer.Create;
  WriteMetricTimer.Timeout:= 1;
  //Pcomm.Converse('del gat');

  //Intros := TTurboPmac32Intros.Create(Pcomm);
end;

destructor TTurboPmacNCHAL.Destroy;
begin
  if Assigned(JogThread) then
    JogThread.Free;

  if Assigned(Pcomm) then begin
    Pcomm.RotBufRemove(0);
    Pcomm.Converse('$$$');
    PComm.ClosePmacNumber;
    PComm.Free;
  end;
  if Assigned(Config) then
    Config.Free;

  if Assigned(Pmac32NCValues) then
    Pmac32NCValues.Free;
  inherited Destroy;
end;

procedure TTurboPmacNCHAL.Clean;
begin
  PPHWLine := 0;
  PPHWUnit := 0;
  FProgramLine := 0;
  Blocking := False;
end;

procedure TTurboPmacNCHAL.ExecutePartProgram;
var Instr : TNCInstructionRecord;
    NextBGLine : Integer;
    Tmp : Single;
    OutLine, StartLine : string;
    IsMotion : Boolean;
    BStop : Boolean;
    McodeProcessing : Boolean;

  function AssignM(M, Value : Integer) : string;
  begin
    Result := Format('M%d==%d', [M, Value]);
  end;

  procedure RunTimeError(aText : string);
  begin
    ErrorString := Format(aText + ' [%s:%d]', [SPP.Name, PPBGLine]);
    FullResetRewind;
  end;

  procedure SetOffset;
  var I : Integer;
  begin
    for I := 0 to FAxisCount - 1 do begin
      TotalOffset[I] := LocalOffset[I] + CoordOffset[I] + ZeroOffsetAdjust.AsArrayDouble[I] + G92.Data^.DoubleArray[I];
    end;
  end;

  procedure SetOffsetTable(Inst : TNCInstruction; aOffset : TUnaryDoubleArrayTag);
  var P : TSFAParameter;
      I : Integer;
      Ptr : Integer;
      PValue : Double;
  begin
    Ptr := 2;
    for P := Low(TSFAParameter) to High(TSFAParameter) do begin
      if P in TSFAParameters(SPP.InstructionList[PPBGInst + 1]) then begin
        I := Config.AxisIndexFromName(0, NCParamNames[P]);
        if I <> -1 then begin
           if not Config.IsRotary(Config.CoordinateSetup[0].AxisByName[P]) then
             PValue := TAbstractTag(SPP.InstructionList[PPBGInst + Ptr]).AsDouble * Units
           else
             PValue := TAbstractTag(SPP.InstructionList[PPBGInst + Ptr]).AsDouble;
          aOffset.Data^.DoubleArray[I] := PValue;
        end;
        Inc(Ptr);
      end;
    end;

    for I := 0 to FAxisCount - 1 do begin
      LocalOffset[I] := G50.Data^.DoubleArray[I] +
                        G51.Data^.DoubleArray[I] +
                        G52.Data^.DoubleArray[I];
    end;

    SetOffset;

    case Inst of
      nfG50 : OutLine := OutLine + 'G50';
      nfG51 : OutLine := OutLine + 'G51';
      nfG52 : OutLine := OutLine + 'G52';
    else
      RunTimeError('PROF ERROR: Invalid Local offset; TPmacNCHAL.SetOffsetTable');
    end;


    for I := 0 to FAxisCount - 1 do begin
      OutLine := OutLine + Format('%s%.6f', [AxisNames[I+1], LocalOffset[I]]);
    end;
    IsMotion := True;
    ForceBlocking := True;
  end;

  procedure SetShiftOffset;
  var P : TSFAParameter;
      I : Integer;
      Ptr : Integer;
  begin
    IsMotion := True;
    Ptr := 2;
    OutLine := OutLine + 'G92';
    if TSFAParameters(SPP.InstructionList[PPBGInst + 1]) <> [] then begin
      for P := Low(TSFAParameter) to High(TSFAParameter) do begin
        if P in TSFAParameters(SPP.InstructionList[PPBGInst + 1]) then begin
          I := Config.AxisIndexFromName(0, NCParamNames[P]);
          if I <> -1 then begin
             if Config.IsRotary(Config.CoordinateSetup[0].AxisByName[P]) then begin
               OutLine := OutLine + Format('%s%.6f', [AxisNames[I+1], TAbstractTag(SPP.InstructionList[PPBGInst + Ptr]).AsDouble * Units]);
               G92.Data^.DoubleArray[I]:=
                 Config.CoordinateSetup[0].AxisByDisplay[I].AxisPositionTag.FValue[ptTarget] -
                 TAbstractTag(SPP.InstructionList[PPBGInst + Ptr]).AsDouble;
             end
             else begin
               G92.Data^.DoubleArray[I]:=
                 (Config.CoordinateSetup[0].AxisByDisplay[I].AxisPositionTag.FValue[ptTarget] * Units) -
                 TAbstractTag(SPP.InstructionList[PPBGInst + Ptr]).AsDouble * Units;
               OutLine := OutLine + Format('%s%.6f', [AxisNames[I+1], TAbstractTag(SPP.InstructionList[PPBGInst + Ptr]).AsDouble])
             end
          end;
          Inc(Ptr);
        end;
      end;
    end else begin
      for I:= 0 to FAXisCount - 1 do begin
        G92.Data^.DoubleArray[I]:= 0;
      end;
    end;
    SetOffset;
    ForceBlocking := True;
  end;

  procedure SetCoordOffset(aOffset : Integer);
  var I : Integer;
  begin
    for I := 0 to FAxisCount - 1 do begin
      CoordOffset[I] := CoordSys[aOffset].Data^.DoubleArray[I];
    end;

    SetOffset;
    OutLine := OutLine + Format('G%d', [54+aOffset]);

    for I := 0 to FAxisCount - 1 do begin
      OutLine := OutLine + Format('%s%.6f', [AxisNames[I+1], CoordOffset[I]]);;
    end;
  end;

  procedure ClearCoordOffset;
  var I : Integer;
  begin
    FillChar(CoordOffset, Sizeof(CoordOffset), 0);
    for I := 0 to FAxisCount - 1 do begin
      LocalOffset[I] := 0;
    end;

    OutLine := OutLine + 'G53';
    SetOffset;
    IsMotion := True;
  end;

  procedure NewBlock;
  begin
    McodeProcessing := False;
    OutLine := '';
    StartLine := AssignM(LINE_NUMBER_M, PPBGLine); //Format('M22==%d', [PPBGLine]);
    IsMotion := False;
    BStop := False;
  end;


  procedure DoMCode;
  var MExt, P : Integer;
      STmp : string;
  begin
    McodeProcessing := True;
    Tmp := TAbstractTag(SPP.InstructionList[PPBGInst + 1]).AsDouble;
    STmp:= Format('M%d', [Round(Tmp)]);
    OutLine := OutLine + STmp;

    STmp := Format('%.4f', [Tmp - Round(Tmp)]);
    MExt := 0;
    P := Pos('.', STmp);
    if P > 0 then begin
      Delete(STmp, 1, P);
      MExt := StrToInt(STmp);
    end;

    MExtTag.AsInteger := MExt;
    case Round(Tmp) of
      0 : begin
      end;

      1 : begin
//        OutLine := Format('if(M%d&%d=0)', [PC_STATUS_M, pcsOptionalStop]) + OutLine;
      end;

      30 : begin
        OutLine := OutLine + 'DWELL0STOP';
      end;
    end;
    ForceBlocking := True;
    IsMotion := True;
  end;

  procedure DoSCode;
  begin
//    OutLine := OutLine + Format('S%f', [TAbstractTag(SPP.InstructionList[PPBGInst + 1]).AsDouble]);
    OutLine := OutLine + Format('M%d==%f', [NC_SCODE_M, TAbstractTag(SPP.InstructionList[PPBGInst + 1]).AsDouble]);
    IsMotion := True;
  end;

  procedure DoTCode;
  begin
    OutLine := OutLine + Format('T%f', [TAbstractTag(SPP.InstructionList[PPBGInst + 1]).AsDouble]);
    IsMotion := True;
  end;

  procedure DoFCode;
  begin
    FTargetFeedRate := TAbstractTag(SPP.InstructionList[PPBGInst + 1]).AsDouble;
    OutLine := Format('F%.6f', [TargetFeedRate * Units]) + OutLine;
    IsMotion := True;
  end;

  procedure DoGCode;
  begin
    Tmp := TNCInstructionRecord(SPP.InstructionList[PPBGInst + 1]).NI;
    IsMotion := True;
    case Round(Tmp) of
      0 : InterpolationMode := imRapid;
      1 : InterpolationMode := imLinear;
      2 : InterpolationMode := imCW;
      3 : InterpolationMode := imCCW;
      20 : SetUnits(25.4); //&&&Units
      21 : SetUnits(1);
      70 : SetUnits(25.4); //&&&Units
      71 : SetUnits(1);
      61 : CuttingMode := False;
      // TODO
      // G64 has been disabled, the following program was
      // shown to crash every time.
      64 : CuttingMode := Config.CuttingModeEnabled; // TODO fix crash problem True;
      90 : Incremental := False;
      91 : Incremental := True;
      53 : ClearCoordOffset;
      54 : SetCoordOffset(0);
      55 : SetCoordOffset(1);
      56 : SetCoordOffset(2);
      57 : SetCoordOffset(3);
      58 : SetCoordOffset(4);
      59 : SetCoordOffset(5);
    else
    end;
    OutLine := OutLine + Format('G%d', [Round(Tmp)]);
  end;

  const RotationalMotionModes = [imCW, imCCW];
        PseudoPositionAxes = [nfParamI, nfParamJ, nfParamK, nfParamR];

  procedure DoSFA;
  var I, X : Integer;
      P : TSFAParameter;
      PValue, SOffset, Temp : Double;
  begin
    I := 2;
    for P := Low(TSFAParameter) to High(TSFAParameter) do begin
      if P in TSFAParameters(SPP.InstructionList[PPBGInst + 1]) then begin
        X := Config.AxisIndexFromName(0, NCParamNames[P]);
        Temp:= TAbstractTag(SPP.InstructionList[PPBGInst + I]).AsDouble;
        if (P in Config.CoordinateSetup[0].LegalAxisParameters) and
             not Config.IsRotary(Config.CoordinateSetup[0].AxisByName[P]) then begin
          PValue := Temp * Units;
          SOffset := ActiveShiftOffset[X] * Units;
        end else if (P in PseudoPositionAxes) and (InterpolationMode in RotationalMotionModes) then begin
          PValue :=  Temp * Units;
          SOffset := ActiveShiftOffset[X] * Units;
          Outline:= Outline + Format('%s%.6f', [NCParamNames[P], PValue]);
        end else if (P = nfParamP) then begin
          Outline:= 'M' + IntToStr(LOCAL_PROCESS_M) + '==' + Format('%.4f', [Temp]) + OutLine;
          PValue:= 0;
          SOffset:= 0;
        end else begin
          PValue := Temp;
          SOffset := ActiveShiftOffset[X];
        end;

        if X <> -1 then begin
        DisplayUnits;
          if not Incremental then begin
            if Config.IsRotary(Config.CoordinateSetup[0].AxisByName[P]) then
              PValue := PValue + (TotalOffset[X]) + SOffset
            else
              PValue := PValue + (TotalOffset[X] * Units) + SOffset;
          end;

          OutLine := OutLine + Format('%s%.6f', [NCParamNames[P], PValue]);
        end;
        Inc(I);

      end;
    end;
    IsMotion := True;
  end;

  procedure DoDwell;
  var PValue: Double;
  begin
    //McodeProcessing := True; // Added for dwell in rotary buffer
    OutLine := OutLine + 'G04';
    if TSFAParameters(SPP.InstructionList[PPBGInst + 1]) <> [] then begin
      PValue:= TAbstractTag(SPP.InstructionList[PPBGInst + 2]).AsDouble;
      if PValue > 0.9 then
        DisplaySweep:= False;

      OutLine := OutLine + Format('P%f', [PValue]);
    end;
    //PComm.Log('G04');
    ForceBlocking := True;
    IsMotion := True;
    BStop:= True;
  end;

  procedure DoLineNumber;
  begin
    PPBGLine := Integer(SPP.InstructionList[PPBGInst + 1]);
    if (RotaryRemaining >= LookAhead) or ForceBlocking then
      Blocking := True;
    RunBlockDelete := False;
    BStop := True;
  end;

  procedure DoReturn;
  begin
    RunReturn(NextBGLine);;
//    OutLine := AssignM(LINE_NUMBER_M, PPBGLine) + AssignM(UNIT_NUMBER_M, PPBGUnit);
    IsMotion := True;
  end;

  procedure DoCall;
  begin
    if not (RunBlockDelete and (ncsBlockDelete in NCStates)) then begin
      RunCall(NextBGLine);
    end;
  end;

  procedure DoBlockDelete;
  begin
    OutLine := Format('if(M%d=0)', [Config.PCSignalBits.GetMVar(Ord(PCS_BLOCK_DELETE_M))]) + OutLine;
  end;

  procedure DoAssign;
  begin
    if not (RunBlockDelete and (ncsBlockDelete in NCStates)) then begin
      RunAssign;
    end;
  end;

  procedure DoHardware;
  begin
    Outline := TAbstractTag(SPP.InstructionList[PPBGInst + 1]).Name;
    IsMotion := True;
  end;

  procedure DoHardwareSync;
  var I : Integer;
  begin
    HWSynch := True;
    for I := 0 to HWCount - 1 do begin
      IHW.AsArrayDouble[I] := PComm.FDPR[PmacIHWDPR + (I*4)];
      PComm.FDPR[PmacOHWDPR + (I*4)] := OHW.AsArrayDouble[I];
    end;
  end;

  procedure DoPluginGCode;
  var ECG: INCExtGCode;
  begin
    ECG:= INCExtGCode(SPP.InstructionList[PPBGInst + 1]);
    ECG.Execute;
  end;

var
  CountDown: Integer;
  BlockCount: Integer;
begin
  Lock.Enter('PP');
  CountDown := 500;
  try
    if (ForceBlocking or Blocking) and
       not StartPermissiveTag.AsBoolean
       and not (ncsInCycle in FNCStates) then begin
      StartPermissiveTag.AsBoolean := True;
    end;

    RotaryRemaining := Pcomm.RotaryRemaining[0];
    if RotaryRemaining <= LookAHead then
      Blocking := False;

    if Blocking or ForceBlocking then
      Exit;

    BlockCount:= LookAhead - RotaryRemaining;

    NewBlock;


    if ncsSingleBlock in NCStates then
      LookAHead := NoLookAhead
    else
      LookAHead := NormalLookAhead;

    HWSynch := False;

    try
      while not Blocking
        and not HWSynch
        and not ResetRewindRequest
        and not (PCS_RESETREWIND_M in PCSignals)
        and (SysObj.FaultInterface.FaultLevel < faultStopCycle) do begin
        try
          Instr.Ptr := SPP.InstructionList[PPBGInst];
          NextBGLine := PPBGInst + Instr.ParameterCount;
        except
          raise;
        end;
        case Instr.Instruction of
          nfSCode : DoSCode;
          nfTCode : DoTCode;
          nfFCode : DoFCode;
          nfMCode : DoMCode;
          nfGCode : DoGCode;
          nfG50 : SetOffsetTable(nfG50, G50);
          nfG51 : SetOffsetTable(nfG51, G51);
          nfG52 : SetOffsetTable(nfG52, G52);
          nfG92 : SetShiftOffset;
          nfDwell : DoDwell;
          nfSFA : DoSFA;
          nfMacroCall,
          nfCall : DoCall;
          nfAssign : DoAssign;
          nfJump : NextBGLine := Integer(SPP.InstructionList[PPBGInst + 1]);
      //    nfData,           // Just data so skip
          nfBlockDelete : DoBlockDelete;
          nfIf : RunIF(NextBGLine);
          nfHash : RunHash(NextBGLine);
          nfIndexVar : RunIndexVar;
          nfICompare : RunICompare(NextBGLine);
          nfLineNumber : DoLineNumber;
          nfReturn : DoReturn;
          nfScriptCall : if not ResetRewindRequest then
                           Synchronize(RunScriptCall);
          nfHWSynch : DoHardwareSync;
          nfHardware : DoHardware;
          nfPlugin: DoPluginGCode;
        end;
        PPBGInst := NextBGLine;

        // Decide if we want the block in the rotary buffer
        // And then add or delete
        if BStop then begin
          if (ncsSingleBlock in NCStates) or IsMotion then begin
            Dec(BlockCount);
            if not CuttingMode or (ncsSingleBlock in NCStates) or McodeProcessing then
              OutLine := StartLine + AssignM(NEXT_LINE_NUMBER_M, PPBGLine) + AssignM(UNIT_NUMBER_M, PPBGUnit) + 'DWE0' + OutLine
            else
              OutLine := StartLine + AssignM(NEXT_LINE_NUMBER_M, PPBGLine) + AssignM(UNIT_NUMBER_M, PPBGUnit) + OutLine;
            Pcomm.AsciiToRot('BStart' + OutLine + 'BStop', 0);
            //Pcomm.Log('SPP.Name: ' + SPP.Name);
            Pcomm.Log(OutLine);
            Outline:= '';
            if ForceBlocking then begin
              Exit;
            end;

          end;
          NewBlock;
        end;
        Dec(CountDown);
        if ((CountDown < 0) or (BlockCount < 0)) and not IsMotion then
          Exit;
      end;
    except
      on E : Exception do begin
        RunTimeError(E.Message);
      end;
    end;
  finally
    if OutLine <> '' then begin
      Pcomm.Log('Residual: ' + OutLine);
    end;
    Lock.Leave('PP');
  end;
end;

procedure TTurboPmacNCHAL.Reset;
begin
end;

procedure TTurboPmacNCHAL.ResetRewind;
var OutLine : string;
    I : TGCodeGroup;
    C : Integer;
begin
  inherited ResetRewind;

  Incremental := False;
  CuttingMode := False; // &&& These two do not allow easy parametric default gcodes
  Blocking := True;
  ForceBlocking := True;


  Include(PCSignals, PCS_RESETREWIND_M);
  //PCRR:= True;

  //&&&Units
  if Config.CoordinateSetup[0].InchDefault then begin
    SetUnits(25.4);
    DisplayUnits := 25.4;
  end else begin
    SetUnits(1);
    DisplayUnits := 1;
  end;

  if not AxisRelease then
    Pcomm.Converse('A');

  FillChar(TotalOffset, Sizeof(TotalOffset), 0);
  FillChar(ActiveOffset, Sizeof(ActiveOffset), 0);
  for C := 0 to AxisCount - 1 do begin
    TotalOffset[C] := ZeroOffsetAdjust.AsArrayDouble[C];
    ActiveOffset[C] := TotalOffset[C];
  end;

  FillChar(LocalOffset, Sizeof(LocalOffset), 0);
  FillChar(G50.Data^, G50.DataSize, 0);
  FillChar(G51.Data^, G51.DataSize, 0);
  FillChar(G52.Data^, G52.DataSize, 0);
  FillChar(G92.Data^, FAxisCount * Sizeof(Double), 0);

  FillChar(CoordOffset, SizeOf(CoordOffset), 0);
  FillChar(ActiveLocalOffset, Sizeof(ActiveLocalOffset), 0);
  FillChar(ActiveCoordOffset, Sizeof(ActiveCoordOffset), 0);
  FillChar(ActiveShiftOffset, Sizeof(ActiveShiftOffset), 0);

  {
  Pcomm.RotBuf(False);
  Pcomm.Converse('&1del lookahead');
  Pcomm.RotBufRemove(0);
  Pcomm.RotBufInit($400);
  DefineLookAhead;

  Pcomm.RotBufClr(0);
  Pcomm.RotBufChange($400, 0);
  Pcomm.RotBuf(True);

  Pcomm.Converse('&1a');
  }

  //Pcomm.Converse('Close');
  Pcomm.RotBuf(False);
  Pcomm.Converse('&1del lookahead');
  Pcomm.Converse('&1del rot');
  Pcomm.Converse('&1define rotary $400');
  DefineLookAhead;
  if not AxisRelease then
    Pcomm.Converse('&1a');
  Pcomm.RotBufChange($400, 0);
  Pcomm.RotBuf(False);
  Pcomm.RotBufClr(0);
  Pcomm.RotBufClr(0);
  Pcomm.RotBufClr(0);
  Pcomm.RotBufClr(0);
  Pcomm.RotBufClr(0);
  Pcomm.RotBufClr(0);
  Pcomm.RotBufClr(0);
  Pcomm.RotBufClr(0);
  Pcomm.RotBuf(True);

  //if FRequestCNCMode in ValidCycleStartMode then begin
    //Pcomm.Converse('openrot');
    OutLine := 'BSTARTG04';
    for I := Low(TGCodeGroup) to High(TGCodeGroup) do
      if I <> gcgUnique then
        OutLine := OutLine + Format('G%d', [GCodeGroupDefaults[I]]);

    Pcomm.AsciiToRot(OutLine + 'BSTOP', 0);
  //end;
  Sleep(100);
  Blocking := False;
end;

function TTurboPmacNCHAL.GetTargetFeedrate : Double;
begin
  Result := FTargetFeedRate;
end;

function TTurboPmacNCHAL.GetActualFeedrate : Double;
begin
  Result := PComm.SCoordVelocity;
end;

function TTurboPmacNCHAL.GetAxisType(Index : Integer) : TAxisType;
begin
  if Config.IsRotary(Config.CoordinateSetup[0].AxisByDisplay[Index]) then
    Result := atRotary
  else
    Result := atLinear;
end;

procedure TTurboPmacNCHAL.VCLUpdate;
var I : Integer;
begin
  I := Pcomm.IDPR[PmacGGroupA_DDpr];
  if I <> LastGGroup03 then begin
    ActiveGGroup.AsArrayInteger[0] := I and $FF;
    ActiveGGroup.AsArrayInteger[1] := (I shr 8) and $ff;
    ActiveGGroup.AsArrayInteger[2] := (I shr 16) and $ff;
    ActiveGGroup.AsArrayInteger[3] := (I shr 24) and $ff;
    LastGGroup03 := I;
  end;

  I := Pcomm.IDPR[PmacGGroupF_IDpr];
  if I <> LastGGroup47 then begin
    ActiveGGroup.AsArrayInteger[4] := I and $FF;
    ActiveGGroup.AsArrayInteger[5] := (I shr 8) and $ff;
    ActiveGGroup.AsArrayInteger[6] := (I shr 16) and $ff;
    ActiveGGroup.AsArrayInteger[7] := (I shr 24) and $ff;
    LastGGroup47 := I;
  end;

  I := Pcomm.IDPR[PmacGGroupEtcDpr];

  if I <> LastGGroup811 then begin
    ActiveGGroup.AsArrayInteger[8] := I and $FF;
    ActiveGGroup.AsArrayInteger[9] := (I shr 8) and $ff;
    ActiveGGroup.AsArrayInteger[10] := (I shr 16) and $ff;
    LastGGroup811 := I;
  end;

  if Integer(AxisHomed) = ((1 shl Self.AxisCount) - 1) then begin
    if not (ncsHomed in FNCStates) then begin
      if Now > HomeCompleteDelay then begin
        Include(FNCStates, ncsHomed);
        Pmac32NCValues.WriteZeroOffsets(ZeroOffset);
        Pmac32NCValues.WritePlusLimits(SoftLimitPlus, ZeroOffset);
        Pmac32NCValues.WriteMinusLimits(SoftLimitMinus, ZeroOffset);
      end;
    end;
  end else begin
    Exclude(FNCStates, ncsHomed);
    HomeCompleteDelay := Now + (2 / 86400); // Two second delay
  end;

  if not (ncsInCycle in FNCStates) and
     (CNCModeManual in FCNCModes) then
     GetForceBlocking;

  if Integer(AxisInPosition) = ((1 shl Self.AxisCount) - 1) then
    Exclude(FNCStates, ncsInMotion)
  else
    Include(FNCStates, ncsInMotion);

  RotaryCount.AsInteger := RotaryRemaining;
  DisplaySweep:= True;
end;


function TTurboPmacNCHAL.ResetRewindComplete: Boolean;
begin
  Result:= False;
  case ResetRewindStage of

    0: begin
      if PCS_RESETREWIND_M in PCSignals then
        ResetRewindStage:= 1;

      ActiveNWord.AsString := PreNWordLabel;
      PComm.IDPR[PmacPCLineDPR]:= PreNWordLineNumber;
      PPBGInst:= PreNWordIndex;
    end;

    1: begin
      Result:= not (PCS_RESETREWIND_M in PCSignals)
               and not (NCS1_RESETREWIND_M in NCSignals);

      if NCS1_RESETREWIND_M in NCSignals then begin
        Exclude(PcSignals, PCS_RESETREWIND_M);
      end;

      PreNWordLabel:= '';
      PreNWordLineNumber:= 0;
      PreNWordIndex:= 0;


      if Result then begin
        ResetRewindStage:= 0;
        ForceBlocking := True;
        RewindComplete := True;
        if ExecuteMacroOnResetComplete then begin
            MacroExecute:= True;
            if SingleBlock then
              SingleBlock:= True;

            CycleStart:= True;
        end;

        ExecuteMacroOnResetComplete:= False;
        ForceBlocking:= True;
        StartPermissiveTag.AsBoolean := True;
        //PComm.Log('Resetrewind Complete');
      end;
    end;
  end;
end;

procedure TTurboPmacNCHAL.UpdateActiveOffset;
//var I : Integer;
begin
  if (Pcomm.IDPR[PmacGGroupF_IDpr] shr 24) and $ff = 20 then
    DisplayUnits := 25.4
  else
    DisplayUnits := 1;
end;

function RollOverPosition(Pos : Double) : Double;
var Tmp : Int64;
begin
  Tmp := Trunc(Pos / 360);
  Result := Pos - (Tmp * 360);
  if(Result < 0)then
    Result := Result + 360;
end;


procedure TTurboPmacNCHAL.UpdateAxisPosition(I: Integer; ATag: TAxisPositionTag);

var CUnits: Double;
    Bias: Double;
    Master: Double;
    Compensation: Double;
    Actual: Double;
    Commanded: Double;
    Target: Double;
    Follow: Double;
    Velocity: Double;
    AP: ^TAxisPosition;
    Offset: Double;
begin
  AP:= @ATAg.FValue;
  if Config.IsRotary(Config.CoordinateSetup[0].AxisByDisplay[I]) then
    CUnits := 1
  else
    CUnits := DisplayUnits;

  try
    Bias:= Pcomm.SPositionBias(I) / CUnits;
    Master:= Pcomm.SMasterPosition(I) / CUnits;
    Compensation:= PComm.SCompensation(I) / CUnits;
    Actual:= PComm.SActualPosition(I) / CUnits;
    Commanded:= PComm.SCommandedPosition(I) / CUnits;
    //Target:= Pcomm.STargetPosition(I) / CUnits;
    Target:= PComm.FDPR[TargetPositionDPR + (I * 4)] / CUnits;
    Follow:= PComm.SFollowError(I) / CUnits;
    Velocity:= PComm.SAxisVelocity(I);

    Config.MetricStorageBias[I]:= Pcomm.SPositionBias(I);
    Config.MetricStorageCommand[I]:= PComm.SCommandedPosition(I);
    AP[ptBias] := Bias;
    AP[ptMaster] := Master;
    AP[ptCompensation] := Compensation;
    AP[ptAbsMachine] := Actual - Bias;
    AP[ptFerr]:= Follow;
    AP[ptVelocity]:= Velocity;
    AP[ptTimeRIM]:= PComm.SGetMoveTime(I);

    Offset:= TotalOffset[I] / 1; //CUnits;

    if(Config.CoordinateSetup[0].AxisByDisplay[I].ShortestPath) then begin
      AP[ptMachine] := RollOverPosition(Actual);
      AP[ptDisplay] := RollOverPosition(Actual - Offset);
      AP[ptCommanded] := RollOverPosition(Actual - Follow - Offset);

      if (ncsInCycle in FNCStates) then begin
        AP[ptTarget] := RollOverPosition(Target - TotalOffset[I]);
        AP[ptDistToGo] := AP[ptTarget] - AP[ptDisplay];
      end else begin
        AP[ptTarget] := RollOverPosition(Target - TotalOffset[I]);
        AP[ptDistToGo] := 0;
      end;
    end else begin
      AP[ptMachine] := Actual;
      AP[ptDisplay] := Actual - Offset;
      AP[ptCommanded] := Actual - Follow - Offset;

      if (ncsInCycle in FNCStates) then begin
        AP[ptTarget] := Target - TotalOffset[I];
        AP[ptDistToGo] := Target - Actual;
      end else begin
        AP[ptTarget] := Target - TotalOffset[I];
        AP[ptDistToGo] := 0;
      end;
    end;

    AP[ptFerr] := Follow;

    //if FCNCModes <= ValidCycleStartMode then begin

    AP[ptCPU] := PComm.PulsesPerUnit[I] * CUnits;
    AP[ptGridShift] := PComm.FDPR[PmacGridPositionDPR + I * 4] / CUnits;
    AP[ptTimeRIM] := PComm.STimeRemainingInMove(0);
    AP[ptAcceleration] := 0;
  except
  end;
end;



var Tick1: Integer;

procedure TTurboPmacNCHAL.Execute;
var Tick : Integer;
begin
  while not Terminated do begin
    Tick := Windows.GetTickCount;
    Inc(SweepCount);
    if Tick > LastSecondStart + 1000 then begin
      LastSecondStart := Tick;
      SweepsPerSecond := SweepCount;
      Self.SweepsPerSecondTag.AsDouble:= SweepsPerSecond;
      if SweepsPerSecond < MinSweepsPerSecondTag.AsDouble then
        MinSweepsPerSecondTag.AsDouble:= SweepsPerSecond;

      SweepCount := 0;
    end;

    if SweepsPerSecondResetTag.AsBoolean then begin
      MinSweepsPerSecondTag.AsDouble:= 1000;
    end;

  Tick1:= Windows.GetTickCount;
  Pcomm.IDPR[$6a] := $00008000;
  Pcomm.IDPR[$674] := $80000001;
  PComm.ReadFixedBuffers(1);


  NCSignals:= TNCSignalBits(Byte(Pcomm.IDPR[PmacNCSignalDPR]));

  if(NCS1_WATCHDOG_M in NCSignals) <> LastWatchDog then begin
    //WatchLog(Cnc32.PlcTick.I64 - LastWatchdogChange);
    LastWatchdogChange:= Cnc32.PlcTick.I64;
  end;
  LastWatchdog:= (NCS1_WATCHDOG_M in NCSignals);

  if not Config.DisableWatchDog then begin
    if Cnc32.PlcTick.I64 > (LastWatchdogChange + 2000) then begin
      Sysobj.FaultInterface.SetFault(SysObj.NC, NCFaults[ncfWatchDogFailed], [], nil);
    end;
  end;

    Lock.Enter('EX');
    try
      if SysObj.FaultInterface.FaultLevel >= FaultStopCycle then
        if ncsInCycle in NCStates then
          if not (ncsFeedHold in NCStates) then
            FeedHold := True;

      UpdateHardware;

      if ResetRewindRequest then begin
        ResetRewind;
        ResetRewindRequest := False;
      end;

      try
      if PartProgram.Loaded and ((FCNCModes <= ValidCycleStartMode) or MacroExecute) then
        ExecutePartProgram;
      except
        raise;
      end;

      Pcomm.IDPR[PmacPCSignalDPR]:= Byte(PCSignals);
      Pcomm.IDPR[PmacPLCSignalDPR]:= Byte(PLCSignals);

      PComm.WriteFixedBuffers;
      Pcomm.IDPR[$6a] := $00000000;
      Pcomm.IDPR[$674] := $00000001;
      Tick1:= Windows.GetTickCount - Tick;

    finally
      Lock.Leave('EX');
    end;

    Sleep(ActiveConfig.CoordinateSetup[0].ThreadKindness);
  end;
end;

var TimeStart : Int64;

procedure TTurboPmacNCHAL.UpdateHardware;

    function MachinePos(SFA : TSFAParameter) : Double;
    var Tmp : TMotorSetup;
    begin
      Tmp := Config.CoordinateSetup[0].AxisByName[SFA];
      if Assigned(Tmp) then
        Result := Tmp.AxisPositionTag.FValue[ptMachine]
      else
        Result := 0;
    end;

    procedure EventLogAxisPositions;
    var X, Y, Z, A, B, C: Double;
    begin
      X:= MachinePos(nfParamX);
      Y:= MachinePos(nfParamY);
      Z:= MachinePos(nfParamZ);
      A:= MachinePos(nfParamA);
      B:= MachinePos(nfParamB);
      C:= MachinePos(nfParamC);

      EventLogFmt('%.3f,%.3f,%.3f,%.3f,%.3f,%.3f', [X,Y,Z,A,B,C]);
    end;

var I : Integer;
    Connection : Integer;
    MotorStatii1 : TPmacMotorStatii1;
    MotorStatii2 : TPmacMotorStatii2;
    //CUnits : Double;
    CriticalErrors : Integer;
    APTag: TAxisPositionTag;
    LimitActive: Boolean;
    //Stable: Boolean;
    ErrorString: string;
    LC, SC: Double;
begin
  RotaryRemaining := Pcomm.RotaryRemaining[0];
  RotaryRemaining:= 0;

  CoordStatus1 := Pcomm.CoordStatus1[0];
  CriticalErrors := PComm.IDPR[PmacCriticalErrorsDPR] and $FFFF;

  if Sysobj.FaultInterface.FaultLevel >= faultStopCycle then
    Include(PcSignals, PCS_PROGRAM_STOP_M)
  else
    Exclude(PcSignals, PCS_PROGRAM_STOP_M);

  if pcs1ProgramRunning in CoordStatus1 then
    Include(FNCStates, ncsInCycle)
  else begin
    Exclude(FNCStates, ncsInCycle);
    // Handshake RR complete
    if NCS1_PROGRAM_REWIND_M in NCSignals then begin
      if not ResetRewindOneShot then begin
        EventLog('Reset rewind initiated by NC');
        NormalTermination.AsBoolean := True; //not Interrupted;
        FullResetRewind;
        ResetRewindOneShot:= True;
      end;
    end else begin
      ResetRewindOneShot:= False;
      Interrupted:= True;
    end;
  end;

  PPHWUnit := Pcomm.IDPR[PmacUnitNumberDPR];
  if ncsInCycle in FNCStates then begin
    FProgramLine := Pcomm.IDPR[PmacLineNumberDPR];
  end else begin
    FProgramLine := Pcomm.IDPR[PmacNextLineNumberDPR];
  end;

  NCSweep := NCSweep + 1;

  if MCode.AsInteger <> Pcomm.IDPR[PmacMCodeDPR] then
    MCode.AsInteger := Pcomm.IDPR[PmacMCodeDPR];

  if MCodeAFC.AsBoolean then
    Include(PlcSignals, PLCS_MCODE_AFC_M)
  else
    Exclude(PlcSignals, PLCS_MCODE_AFC_M);

  for I := 0 to AxisCount - 1 do begin
      Connection := Config.CoordinateSetup[0].AxisByDisplay[I].Connection;
      UpdateAxisPosition(I, Config.CoordinateSetup[0].AxisByDisplay[I].AxisPositionTag);

      MotorStatii1 := Pcomm.MotorStatus1[Connection];
      MotorStatii2 := Pcomm.MotorStatus2[Connection];

      if pms1RunningDefinedMove in MotorStatii1 then
        Include(AxisInMotion, I)
      else
        Exclude(AxisInMotion, I);

      if pms2FatalFollowError in MotorStatii2 then
        Include(AxisFatalFollowError, I)
      else
        Exclude(AxisFatalFollowError, I);

      if pms2WarnFollowError in MotorStatii2 then
        Include(AxisWarnFollowError, I)
      else
        Exclude(AxisWarnFollowError, I);

      if pms2HomeComplete in MotorStatii2 then
        Include(AxisHomed, I)
      else
        Exclude(AxisHomed, I);

      if pms1OpenLoop in MotorStatii1 then
        Include(AxisOpenLoop, I)
      else
        Exclude(AxisOpenLoop, I);

      APTag:= Config.CoordinateSetup[0].AxisByDisplay[I].AxisPositionTag;

      if APTag.FValue[ptFerr] > 0.002 then
        Exclude(AxisInPosition, I)
      else
        Include(AxisInPosition, I);

      LimitActive:= False;
      if I in AxisHomed then begin
        if Abs(SoftLimitMinus.AsArrayDouble[I]) > 0.001 then
          LimitActive:= (Abs(APTag.FValue[ptMachine] -  SoftLimitMinus.AsArrayDouble[I]) < (0.005 / Units));
      end;
      if (pms1NegativeLimit in MotorStatii1) or LimitActive then
        Include(AxisNegativeLimit, I)
      else
        Exclude(AxisNegativeLimit, I);


      LimitActive:= False;
      if I in AxisHomed then begin
        if Abs(SoftLimitPlus.AsArrayDouble[I]) > 0.001 then
          LimitActive:= (Abs(APTag.FValue[ptMachine] -  SoftLimitPlus.AsArrayDouble[I]) < (0.005 / Units));
      end;
      if (pms1PositiveLimit in MotorStatii1) or LimitActive then
        Include(AxisPositiveLimit, I)
      else
        Exclude(AxisPositiveLimit, I);

      if pms2AmplifierFault in MotorStatii2 then
        Include(AxisAmplifierFault, I)
      else
        Exclude(AxisAmplifierFault, I);

      if (CriticalErrors and (1 shl I)) <> 0 then begin
        Include(AxisEncoderLoss, I); // No reset condition
      end;
  end;

  if TimeStart < Cnc32.PlcTick.I64 then begin

    ErrorString:= '';
    for I := 0 to 4 do begin
      //APTag:= Config.CoordinateSetup[0].AxisByDisplay[I].AxisPositionTag;
      SC:= PComm.SCommandedPosition(I);
      LC:= Config.LastCommandedPosition[I];
      if Config.CoordinateSetup[0].AxisByDisplay[I].ShortestPath then begin
        LC:= RolloverPosition(LC);
        SC:= RolloverPosition(SC);
      end;

      if Abs(SC - LC) > 0.1 then begin
         ErrorString:= ErrorString + Config.CoordinateSetup[0].AxisNames[I+1] + ' ' + FloatToStr(LC) + ' : ';
      end;
    end;

    if Length(ErrorString) > 1 then begin
      ErrorString:= 'Abs Read Error ' + ErrorString;
      SysObj.FaultInterface.SetFault(SysObj.NC, CoreFaults[cfSysFatal], [ErrorString], nil);
    end;

    TimeStart:= $FFFFFFFFFFF;
  end;

  // Half second interval
  if WriteMetricTimer.Expired then begin
    Config.WriteMetricStorage;
    WriteMetricTimer.Start;
  end;

  if NCS1_DWELL_ACTIVE_M in NCSignals then begin
    //EventLogAxisPositions;
  end
end;

const CSSelect = 0;

procedure TTurboPmacNCHAL.SetJogMode(aMode : TJogModes);
var U : IUnion;
begin
  FJogMode := aMode;
  U.I := Pcomm.IDPR[PmacCNCStatus1DPR];
  U.B[PmacJogModeOffset] := Byte(aMode);
  Pcomm.IDPR[PmacCNCStatus1DPR] := U.I;
  FullResetRewind;
end;

procedure TTurboPmacNCHAL.SetJogIncrement(aIncrement : Integer);
begin
  FJogIncrement := aIncrement;
  Pcomm.IDPR[PmacJogIncrDPR]:= FJogIncrement;
end;

procedure TTurboPmacNCHAL.SetFeedOverrideEnable(aOn : Boolean);
begin
  if aOn then
    Include(FNCStates, ncsFeedOverrideEnable)
  else
    Exclude(FNCStates, ncsFeedOverrideEnable);

  SetFeedOverride(ProgrammedFeedOverride);
end;

procedure TTurboPmacNCHAL.SetFeedOverride(aFeed : Double);
begin
  //Pcomm.FOEnable[CSSelect] := True;
  if ncsFeedOverrideEnable in NCStates then begin
    if AFeed <> CurrentFeedOverride then begin
      CurrentFeedOverride := aFeed;
      ProgrammedFeedOverride:= aFeed;
      Pcomm.IDPR[PmacFeedOvrDPR] := Round(AFeed * 100);
    end;
  end
  else begin
    CurrentFeedOverride:= 100;
    Pcomm.IDPR[PmacFeedOvrDPR] := 100 * 100;
  end;
end;

procedure TTurboPmacNCHAL.IssueAsciiPcommCommand(const Cmd : string);
begin
  //PComm.Converse('close'); ???????
//  try
//    StrToInt(PComm.Converse('I9'));
    Pcomm.Converse(Cmd);
//  except
//    SysObj.FaultInterface.SetFault(SysObj.NC, CoreFaults[cfSysWarning], ['Unable to close rotary buffer'], nil);
//  end;
end;

procedure TTurboPmacNCHAL.SetJogPlus(aAxis : Integer; aOn : Boolean);
var Command : TNCCommand;
begin
  if jmContinuous in JogMode then begin
    Command := TNCCommand.Create(ctJogPlus, Config.CoordinateSetup[0].AxisByDisplay[aAxis].Connection, aOn);
    JogThread.Add(Command);
  end

  else if jmIncremental in JogMode then begin
    if ncsHomed in NCStates then begin
      Command := TNCCommand.Create(ctJogReturnPlus, Config.CoordinateSetup[0].AxisByDisplay[aAxis].Connection, aOn);
      JogThread.Add(Command);
    end else
      SysObj.FaultInterface.SetFault(SysObj.NC, NCFaults[ncfNoIncJogBeforeHomed], [], nil);
  end

  else if jmMPG in JogMode then begin
  end

  else if jmHome in JogMode then begin
    if not (ncsInCycle in NCStates) then
      if not (PCS_NO_CYCLE_M in PcSignals) then
        IssueAsciiPcommCommand(Format('B%dR', [Config.CoordinateSetup[CSSelect].AxisByDisplay[aAxis].HomeProgram]))
      else
        Sysobj.FaultInterface.SetFault(Sysobj.NC, NCFaults[ncfCycleNotPermitted], [], nil);

  end;
end;

procedure TTurboPmacNCHAL.SetReleaseAxes(Release: Boolean);
begin
  if Release then
    Include(PcSignals, PCS_OPEN_LOOP_M)
  else
    Exclude(PcSignals, PCS_OPEN_LOOP_M);

  JogThread.Add(TNCCommand.Create(ctReleaseAxes, 0, Release));
end;

procedure TTurboPmacNCHAL.SetNoCycle(NoC: Boolean);
begin
  inherited;
  if NoCycle then
    Include(PcSignals, PCS_NO_CYCLE_M)
  else
    Exclude(PcSignals, PCS_NO_CYCLE_M);
end;



procedure TTurboPmacNCHAL.SetJogMinus(aAxis : Integer; aOn : Boolean);
var Command : TNCCommand;
begin
  if jmContinuous in JogMode then begin
    Command := TNCCommand.Create(ctJogMinus, Config.CoordinateSetup[0].AxisByDisplay[aAxis].Connection, aOn);
    JogThread.Add(Command);
  end

  else if jmIncremental in JogMode then begin
    if ncsHomed in NCStates then begin
      Command := TNCCommand.Create(ctJogReturnMinus, Config.CoordinateSetup[0].AxisByDisplay[aAxis].Connection, aOn);
      JogThread.Add(Command);
    end else
      SysObj.FaultInterface.SetFault(SysObj.NC, NCFaults[ncfNoIncJogBeforeHomed], [], nil);
//      SysObj.FaultInterface.SetFault(SysObj.NC, 'Incremental jog is not possible before is NC homed', FaultInterlock, nil);
  end

  else if jmMPG in JogMode then begin
  end

  else if jmHome in JogMode then begin
    if not (ncsInCycle in NCStates) then
      if not (PCS_NO_CYCLE_M in PcSignals) then
        IssueAsciiPcommCommand(Format('B%dR', [Config.CoordinateSetup[CSSelect].AxisByDisplay[aAxis].HomeProgram]))
      else
        Sysobj.FaultInterface.SetFault(Sysobj.NC, NCFaults[ncfCycleNotPermitted], [], nil);
  end;
end;

procedure TTurboPmacNCHAL.SetCycleStart(aOn : Boolean);
var Command : TNCCommand;
begin
  ActiveNWord.AsString := '';

  if not (PCS_NO_CYCLE_M in PcSignals) and aOn then begin
    if not (ncsInCycle in FNCStates) then begin
      if ncsSingleBlock in FNCStates then
        Command := TNCCommand.Create(ctSingleBlock, CSSelect, aOn)
      else
        Command := TNCCommand.Create(ctCycleStart, CSSelect, aOn);

      JogThread.Add(Command);
    end;

    if aOn then
      NormalTermination.AsBoolean := False;
  end;
end;

procedure TTurboPmacNCHAL.SetSingleBlock(aOn : Boolean);
var Command : TNCCommand;
begin
  if aOn then begin
    if (FCNCModes = [CNCModeMDI]) and not SingleBlock then
      Exit;

    inherited SetSingleBlock(aOn);
    if (ncsSingleBlock in FNCStates) and (ncsInCycle in FNCStates) then begin
      Command := TNCCommand.Create(ctSingleBlock, CSSelect, aOn);
      JogThread.Add(Command);
    end;

    if (ncsSingleBlock in FNCStates) then
      Include(PcSignals, PCS_SINGLE_BLOCK_M)
    else
      Exclude(PcSignals, PCS_SINGLE_BLOCK_M)
  end;
end;

procedure TTurboPmacNCHAL.SetOptionalStop(aOn : Boolean);
begin
  inherited;
  if (ncsOptionalStop in FNCStates) then
    Include(PCSignals, PCS_OPTIONAL_STOP_M)
  else
    Exclude(PCSignals, PCS_OPTIONAL_STOP_M);
end;

procedure TTurboPmacNCHAL.SetBlockDelete(aOn : Boolean);
begin
  inherited;
  if (ncsBlockDelete in FNCStates) then
    Include(PCSignals, PCS_BLOCK_DELETE_M)
  else
    Exclude(PCSignals, PCS_BLOCK_DELETE_M);
end;

procedure TTurboPmacNCHAL.SetFeedHold(aOn : Boolean);
var Command : TNCCommand;
begin
  if aOn {ncsFeedHold in NCStates} then
    Include(FNCStates, ncsFeedHold)
  else
    Exclude(FNCStates, ncsFeedHold);

  if aOn then begin
    Command := TNCCommand.Create(ctFeedHold, CSSelect, aOn);
    JogThread.Add(Command);
  end;
end;

procedure TTurboPmacNCHAL.SetCycleStop(aOn : Boolean);
var Command : TNCCommand;
begin
  if aOn then begin
    Command := TNCCommand.Create(ctCycleStop, CSSelect, aOn);
    JogThread.Add(Command);
  end;
end;

procedure TTurboPmacNCHAL.SetCycleAbort(aOn: Boolean);
var Command : TNCCommand;
begin
  if aOn then begin
    Command := TNCCommand.Create(ctCycleAbort, CSSelect, aOn);
    JogThread.Add(Command);
  end;
end;

function TTurboPmacNCHAL.GetCycleAbort: Boolean;
begin
  Result:= False;
end;

function TTurboPmacNCHAL.GetJogPlus(aAxis : Integer) : Boolean;
begin
  Result := False;
end;

function TTurboPmacNCHAL.GetJogMinus(aAxis : Integer) : Boolean;
begin
  Result := False;
end;

function TTurboPmacNCHAL.GetCycleStart : Boolean;
begin
  Result := False;
end;

function TTurboPmacNCHAL.GetFeedHold : Boolean;
begin
  Result := False;
end;

function TTurboPmacNCHAL.GetCycleStop : Boolean;
begin
  Result := False;
end;

function TTurboPmacNCHAL.GetFeedOverride : Double;
begin
  //Result := Pcomm.FeedrateOverride[CSSelect] / 145.07;
  Result:= CurrentFeedOverride;
end;

procedure TTurboPmacNCHAL.SetAxisPositionTags(APList : TList);  // List of TAxisPositionTag
var I : Integer;
begin
  with Config.CoordinateSetup[0] do begin
    for I := 0 to AxisCount - 1 do begin
      AxisByDisplay[I].AxisPositionTag := TAxisPositionTag(APList[I]);
    end;
  end;
end;

function TTurboPmacNCHAL.CreateConfig : TNC32Configure;
begin
  Config := TTurboPmacNCConfigure.Create;
  Result := Config;
end;

// Simple configuration.  This is a simple pmac download with a
// header so that PmacNC knows what we just did.
procedure TTurboPmacNCHAL.Configure;
var I : Integer;
    CurrentPath : array [0..1023] of Char;
    Test  : Integer;
    PUTest: Double;
    Tmp: string;
begin
  Config.ReadFromStrings;

  WriteOffsetsFirstTime:= Config.CheckAbsoluteRead;

  if Config.PowerUpTest then begin
    Pcomm.Converse('$$$');

    for I := 0 to 15 do begin
      Sleep(1000);
      Tmp:= Trim(Pcomm.Converse('P1'));
      PUTest := StrToFloatDef(Tmp, -1);
      EventLog('Test Loop: ' + IntToStr(I) + ' P1=' + FloatToStr(PUTest));
      if Abs(PUTest - 999) < 0.0001 then begin
        Break;
      end;
    end;

    if Abs(PUTest - 999) > 0.0001 then
      raise ECNCInstallFault.Create('UMAC Power up test unsuccessful: [P1 != 999]');
  end;

  EventLog('P1=' + Pcomm.Converse('P1'));

  TimeStart:= $FFFFFFFFFF;

  FAxisCount := Config.AxisCount[0];
  PComm.ServoRate:= Config.CoordinateSetup[0].Servotime;

  for I := 0 to FAxisCount - 1 do begin
    PComm.PulsesPerUnit[I] := Config.MotorSetup[I].CPUnit;
    PComm.Scaling[I]:= Config.MotorSetup[I].Scaling;
    PComm.VelocityAxes[I] := I + 1;
    Pcomm.JogPositive[I] := False;
    Pcomm.JogNegative[I] := False;
    Pcomm.JogReturn[I] := False;

    case Config.MotorSetup[I].AxisDesignation of
      'A' : PComm.AxesByName[I] := 0;
      'B' : PComm.AxesByName[I] := 1;
      'C' : PComm.AxesByName[I] := 2;
      'U' : PComm.AxesByName[I] := 3;
      'V' : PComm.AxesByName[I] := 4;
      'W' : PComm.AxesByName[I] := 5;
      'X' : PComm.AxesByName[I] := 6;
      'Y' : PComm.AxesByName[I] := 7;
      'Z' : PComm.AxesByName[I] := 8;
    end;
  end;

  PComm.VelocityAxesCount := FAxisCount;

  if GetCurrentDirectory(1023, CurrentPath) = 0 then
    raise ECNCInstallFault.Create('Unable to read current path');

  if not SetCurrentDirectory(PChar(ProfilePath + NCPmacDirectory)) then
    CoreCNC.MessageLog('Cant change directory to NC Profile');

  try
    Config.Output.SaveToFile(ProfilePath + NCPmacDirectory + 'NC.Txt');
    Config.Output.Clear;
    if not Pcomm.DownLoadFileName(ProfilePath + NCPmacDirectory + 'NC.Txt', False) then begin
      Pcomm32.ResponseBox.ShowModal;
      raise ECNCInstallFault.Create('Download of Pmac Configuration failed' + CarRet + Pcomm32.ResponseBox.Lines.Text);
    end else begin
      MessageLog(Pcomm32.ResponseBox.ResponseMemo.Text);
    end;
  finally
    SetCurrentDirectory(CurrentPath);
  end;
  Pmac32NCValues := TTurboPmac32NCValues.Create(Self, Config);
  JogThread := TPmacJogThread.Create(FDeviceIndex, Self);

  Test := Pcomm.RotBufChange(100, 0);
  CoreCNC.EventLog('Pmac init response = ' + IntToStr(Test));
  if Test = 0 then
    raise ECNCInstallFault.Create('Pmac failed to initialize rotary buffer: ' + IntToStr(Test));

  Pcomm.SetPmacRealTimeMotors(AxisCount); //????
  Pcomm.SetPmacBackgroundEx(True, 10); //????
  Pcomm.SetPmacRealTimeBuffer(10, True); //????
  Pcomm.FOEnable[CSSelect] := True;

  Pcomm.IDPR[$70] := Round(IntPower(2, Self.AxisCount)) - 1;
  Pcomm.IDPR[$674] := 1;
  Pcomm.Converse('I50=5');

  TagOwner.TagEvent('PmacDownloadCompfile', EdgeChange, TIntegerTag, DownloadCompfile);

end;

procedure TTurboPmacNCHAL.SetRequestCNCMode(aMode : TCNCMode);
var U : IUnion;
begin
  Lock.Enter('MC');
  try
    if not SysObj.NC.InCycle then begin
      FRequestCNCMode := aMode;
      FCNCModes := [aMode];
      ExecuteMacroOnResetComplete:= False;
      FullResetRewind;
      U.I := Pcomm.IDPR[PmacCNCStatus1DPR];
      U.B[PmacCNCModeOffset] := Byte(FCNCModes);
      Pcomm.IDPR[PmacCNCStatus1DPR] := U.I;

      if (FCNCModes = [CNCModeMDI]) and SingleBlock then
        SingleBlock := True;

  //    if FCNCModes <= ValidCycleStartMode then
  //      Pcomm.Converse('openrot');
    end else begin
      SysObj.FaultInterface.SetFault(SysObj.NC, NCFaults[ncfNoModeChangeInCycle], [], nil);
      Exit;
    end;
  finally
    Lock.Leave('MC');
  end;
end;


function TTurboPmacNCHAL.GetAxisNames : string;
begin
  Result := Config.AxisNames[0];
end;

function TTurboPmacNCHAL.GetForceBlocking : Boolean;
begin
  Result:= (NCS1_RELEASE_BLOCKING_M in NCSignals) or (PCS_BLOCKING_M in PcSignals);
  //Result :=  NCRB or PCB;
  if (NCS1_RELEASE_BLOCKING_M in NCSignals) and DisplaySweep then
    ForceBlocking:= False;
end;

procedure TTurboPmacNCHAL.SetForceBlocking(aOn : Boolean);
begin
  if AOn then
    Include(PcSignals, PCS_BLOCKING_M)
  else
    Exclude(PcSignals, PCS_BLOCKING_M);
end;

type
  TShortInt = record
    case Byte of
      0 : ( I : Integer );
      1 : ( S : Single );
  end;

procedure TTurboPmacNCHAL.UpdatePersistent(aTag : TAbstractTag);
begin
  if aTag = ZeroOffset then begin
    Pmac32NCValues.WriteZeroOffsets(ZeroOffset);
    if WriteOffsetsFirstTime then begin
      TimeStart := Cnc32.PlcTick.I64 + 3000;
      WriteOffsetsFirstTime:= False;
    end;
  end else begin
    if (ncsHomed in FNCStates) or Config.SetLimitsOnPowerUp then begin
      Pmac32NCValues.WritePlusLimits(SoftLimitPlus, ZeroOffset);
      Pmac32NCValues.WriteMinusLimits(SoftLimitMinus, ZeroOffset);
    end;
  end;
end;

procedure TTurboPmacNCHAL.SetActiveAxis(aAxis : Integer);
var Command : TNCCommand;
    I : Integer;
    U : IUnion;
begin
  if not (ncsInCycle in FNCStates) then begin
    if FActiveAxis <> aAxis then begin
      for I := 0 to AxisCount - 1 do begin
        Command := TNCCommand.Create(ctJogPlus, Config.CoordinateSetup[0].AxisByDisplay[I].Connection, False);
        JogThread.Add(Command);
        Command := TNCCommand.Create(ctJogMinus, Config.CoordinateSetup[0].AxisByDisplay[I].Connection, False);
        JogThread.Add(Command);
      end;
    end;
    FActiveAxis := aAxis;
    U.I := Pcomm.IDPR[PmacCNCStatus1DPR];
    U.B[PmacActiveAxisOffset] := Byte(FActiveAxis);
    Pcomm.IDPR[PmacCNCStatus1DPR] := U.I;
  end;
end;

procedure TTurboPmacNCHAL.ExtFunction(aFunc : Integer);
begin
  if not (ncsInCycle in FNCStates) and
     (CNCModeManual in FCNCModes) then
    IssueAsciiPcommCommand(Format('B%dR', [201 + aFunc]));
//    Pcomm.Converse(Format('B%dR', [201 + aFunc]));
end;


procedure TTurboPmacNCHAL.TestPoint;
begin

end;

constructor TNCCommand.Create(aCommand : TNCCommandType; aData : Integer; aCondition : Boolean);
begin
  inherited Create;
  Command := aCommand;
  Data := aData;
  Condition := aCondition;
end;

constructor TPmacJogThread.Create(ADevice: Integer; AHAL: TTurboPmacNCHAL);
begin
  inherited Create(False);
  HAL:= AHAL;
  Pcomm := TPcomm32.Create(pvTurbo);
  Pcomm.OpenPmacNumber(ADevice);
  CommandList := TList.Create;
  Lock:= TCriticalSection.Create;
end;

procedure TPmacJogThread.Execute;
begin
  while not Terminated do begin
    try
      Lock.Enter;
      try
        if CommandList.Count <> 0 then begin
          ProcessCommand(TNCCommand(CommandList[0]));
          TNCCommand(CommandList[0]).Free;
          CommandList.Delete(0);
        end;
      finally
        Lock.Leave;
      end;
      Sleep(20);
    except
      on E: Exception do begin
        EventLog('@@ Jog Thread failed with: ' + E.Message);
        Sleep(200);
      end;
    end;
  end;
end;

procedure TPmacJogThread.ProcessCommand(aCommand : TNCCommand);

  procedure IncrementalJog(Condition: Boolean; Axis: Integer; Distance: Integer);
  var AUnits: Double;
    MotorS: Char;
    CStr: string;
    Target: Double;
  begin
    MotorS := Char(Axis + Ord('1'));
    AUnits := HAL.Config.CoordinateSetup[0].AxisByDisplay[Axis].CPUnit;
    Target:= AUnits * Distance / 10000.0;
    //Target:= Target + (HAL.Config.CoordinateSetup[0].AxisByDisplay[Axis].AxisPositionTag.FValue[ptMachine] * AUnits);
    if Condition then
      CStr:= '#' + MotorS + 'J:' + FloatToStr(Target);
    //else
    //  CStr:= '#' + MotorS + 'J/';
    Pcomm.Converse(CStr);
  end;

var MotorS, CSS: Char;
    CStr: string;
    Motor: TMotorSetup;
    Counts: Double;
begin
  with aCommand do
    case Command of
    // <<<<<<if not HAL.AxisRelease or not HAL.Config.SafeReleaseAxes>>>>>
    // This above logic allows AxisRelease to work when SafeReleaseAxes is true else the old
    // logic will apply where these are always called
      ctJogPlus : if not HAL.AxisRelease or not HAL.Config.SafeReleaseAxes then begin
        Motor:= HAL.Config.CoordinateSetup[0].AxisByDisplay[Data];
        MotorS := Char(Motor.Connection + Ord('1'));
        if Condition then begin
          if (ncsHomed in HAL.NCStates) and (Abs(HAL.SoftLimitPlus.AsArrayDouble[Data]) > 0.001) then begin
            Counts:= (HAL.SoftLimitPlus.AsArrayDouble[Data]) * Motor.CPUnit * HAL.Units;
            CStr:= '#' + MotorS + 'J=' + FloatToStr(Counts);
          end else
            CStr:= '#' + MotorS + 'J+';
        end else
          CStr:= '#' + MotorS + 'J/';
        Pcomm.Converse(CStr);
      end;
      ctJogMinus : if not HAL.AxisRelease or not HAL.Config.SafeReleaseAxes then begin
        Motor:= HAL.Config.CoordinateSetup[0].AxisByDisplay[Data];
        MotorS := Char(Motor.Connection + Ord('1'));
        if Condition then begin
          if (ncsHomed in HAL.NCStates) and (Abs(HAL.SoftLimitMinus.AsArrayDouble[Data]) > 0.001) then begin
            Counts:= (HAL.SoftLimitMinus.AsArrayDouble[Data]) * Motor.CPUnit * HAL.Units;
            CStr:= '#' + MotorS + 'J=' + FloatToStr(Counts);
          end else
            CStr:= '#' + MotorS + 'J-';
        end else
          CStr:= '#' + MotorS + 'J/';
        Pcomm.Converse(CStr);
      end;

      ctCycleStart : if not HAL.AxisRelease or not HAL.Config.SafeReleaseAxes then begin
        CSS := Char(Data + Ord('1'));
        if Condition then begin
          CStr:= '&' + CSS + 'R';
          Pcomm.Converse(CStr);
        end;
      end;

      ctCycleStop : if not HAL.AxisRelease or not HAL.Config.SafeReleaseAxes then begin
        CSS := Char(Data + Ord('1'));
        if Condition then begin
          CStr:= '&' + CSS + 'H';
          Pcomm.Converse(CStr);
        end;
      end;

      ctCycleAbort : if not HAL.AxisRelease or not HAL.Config.SafeReleaseAxes then begin
        CSS := Char(Data + Ord('1'));
        if Condition then begin
          CStr:= '&' + CSS + 'A';
          Pcomm.Converse(CStr);
        end;
      end;

      ctFeedHold : if not HAL.AxisRelease or not HAL.Config.SafeReleaseAxes then begin
        CSS := Char(Data + Ord('1'));
        if Condition then begin
          CStr:= '&' + CSS + 'H';
          Pcomm.Converse(CStr);
        end;
      end;

      ctSingleBlock : if not HAL.AxisRelease or not HAL.Config.SafeReleaseAxes then begin
        CSS := Char(Data + Ord('1'));
        if Condition then begin
          CStr:= '&' + CSS + 'S';
          Pcomm.Converse(CStr);
        end;
      end;

      ctJogReturnPlus : if not HAL.AxisRelease or not HAL.Config.SafeReleaseAxes then begin
        IncrementalJog(Condition, Data, HAL.JogIncrement);
      end;

      ctJogReturnMinus: if not HAL.AxisRelease or not HAL.Config.SafeReleaseAxes then begin
        IncrementalJog(Condition, Data, -HAL.JogIncrement);
      end;

      ctReleaseAxes: begin
        if Condition then begin
          Pcomm.ControlConverse(#$B);
          HAL.AxisRelease:= True;
        end else begin
          Pcomm.Converse('&1a');
          HAL.AxisRelease:= False;
        end;
      end;
    end;
end;

procedure TPmacJogThread.Add(Command : TNCCommand);
begin
  Lock.Enter;
  try
    CommandList.Add(Command);
  finally
    Lock.Leave;
  end;
end;


{ TTurboNCIOSystem }

function TTurboNCIOSystem.GetOutputSize: Integer;
begin
  Result:= FOutputSize;
end;

function TTurboNCIOSystem.GetInfoPageContent(aPage: Integer): WideString;
begin
  Result:= '';
end;

function TTurboNCIOSystem.GetOutputArea: Pointer;
begin
  Result:= @OutArea;
end;

procedure TTurboNCIOSystem.Shutdown;
begin
  FHealthy:= False;
end;

function TTurboNCIOSystem.GetInputSize: Integer;
begin
  Result:= FInputSize;
end;

procedure TTurboNCIOSystem.Reset;
var HAL: TNCHAL;
    Tmp: PAIOMap;
begin
  if Sysobj.NC is TNC32 then begin
    HAL := TNCHAL(TNC32(SysObj.NC).NCHAL);
    if HAL is TTurboPmacNCHAL then begin
      TurboHAL:= TTurboPmacNCHAL(HAL);
      Tmp:= TurboHAL.Pcomm.GetInputBase;
      PcommIn:= @(Tmp[InputsIODPR - InputPmacBaseDPR]);
      Tmp:= TurboHAL.Pcomm.GetOutputBase;
      PcommOut:= @(Tmp[OutputsIODPR - OutputPmacBaseDPR]);
      FHealthy:= True;
    end;
  end;
end;

function TTurboNCIOSystem.GetInputArea: Pointer;
begin
  Result:= @InArea;
end;

function TTurboNCIOSystem.TextDefinition: WideString;
begin
  Result:= Self.FTextDef;
end;

function TTurboNCIOSystem.Healthy: Boolean;
begin
  Result:= FHealthy;
end;

function TTurboNCIOSystem.LastError: WideString;
begin
  Result:= '';
end;

procedure TTurboNCIOSystem.Initialise(Host: IIOSystemAccessorHost;
  const Params: WideString);
var Tmp: string;
begin
  FHealthy:= False;
  Self.Host:= Host;
  FTextDef:= Params;
  Tmp:= Params;

  FInputSize:= StrToInt(CncTypes.ReadDelimited(Tmp, ';'));
  FOutputSize:= StrToInt(CncTypes.ReadDelimited(Tmp, ';'));

  FInputSize:= CncTypes.RangeInt(FInputSize, 1, DPRIOLength);
  FOutputSize:= CncTypes.RangeInt(FOutputSize, 1, DPRIOLength);
end;

procedure TTurboNCIOSystem.UpdateRealToShadow;
begin
  Windows.CopyMemory(@InArea, PcommIn, FInputSize);
end;

procedure TTurboNCIOSystem.UpdateShadowToReal;
begin
  Windows.CopyMemory(PcommOut, @OutArea, FOutputSize);
end;

procedure TTurboPmacNCHAL.FullResetRewind;
begin
  Sysobj.FaultInterface.ResetRewind;
end;

procedure TTurboPmacNCHAL.DefineLookAhead;
begin
  if CNCModeManual in FCNCModes then
    if jmHome in FJogMode then
      Exit;
  if not Config.DisableLookahead then begin
    Pcomm.Converse('&1define lookahead ' + IntToStr(Config.LookaheadSize) + ',' + IntToStr(5 * Config.LookaheadSize) );
  end;
end;

{ TPmacTag }

constructor TPmacTag.Create(aOwner: TTagOwner; aName: string);
begin
  inherited;
  Self.NetWorkAlias:= 'Pmac' + aName;
end;

procedure TPmacTag.SetAsDouble(AValue: Double);
var HAL: TNCHAL;
    TurboHAL: TTurboPmacNCHAL;
begin
  if Sysobj.NC is TNC32 then begin
    HAL := TNCHAL(TNC32(SysObj.NC).NCHAL);
    if HAL is TTurboPmacNCHAL then begin
      TurboHAL:= TTurboPmacNCHAL(HAL);
      TurboHAL.Pcomm.Converse(Name + '=' + FloatToStr(AValue));
    end;
  end;
  inherited;
end;

procedure TTurboPmacNCHAL.DownloadCompfile(ATag: TAbstractTag);
var S: TStringList;
    F: string;
    I: string;
begin
  F:= MachSpecPath + 'compfile.txt';
  I:= 'I6=0';
  if not ATag.AsBoolean then begin
    if CNCModeManual in FCNCModes then begin
      S:= TStringList.Create;
      try
        try
          S.LoadFromFile(F);
          if S[0] <> I then
            S.Insert(0, I);
          S.SaveToFile(F);
        except
          on E: Exception do begin
            SysObj.FaultInterface.SetFault(Self.TagOwner, CoreFaults[cfSysStopCycle], [E.Message], nil);
            Exit;
          end;
        end;
      finally
        S.Free;
      end;
      Pcomm.DownLoadFileName(F, True);
    end;
  end;
end;



procedure TTurboPmacNCHAL.WatchLog(Value: Integer);
var FileName: string;
    Stream : TFileStream;
    MMsg : string;
begin
  FileName := LoggingPath + 'Watchdog.Log';

  try
    if not FileExists(FileName) then
      Stream := TFileStream.Create(FileName, fmCreate)
    else
      Stream := TFileStream.Create(FileName, fmOpenWrite);
    try
      Stream.Seek(0, soFromEnd);
      MMsg := IntToStr(Value);
      Stream.Write(PChar(MMsg + CarRet)^, Length(MMsg + CarRet));
    finally
      Stream.Free;
    end;
  except
  end;
end;

initialization
  RegisterNCDeviceClass(TTurboPmacNCHAL);
  RegisterIOAccessorClass(TTurboNCIOSystem, 'TurboNCIO');
  RegisterTagClass(TPmacTag);
end.
