unit ShellExec;

interface

uses
  Windows, SysUtils, Classes, CNCTypes, CNC32, CoreCNC, Tokenizer, ShellApi,
  FaultRegistration;

type
  TShellExecType = (
        seConsole,
        seMime
  );

  TShellExecProcess = class(TObject)
  private
    ApplicationName : string;
    CommandLine : string;
    TagName : string;
    CommandTagName : string;
    Tag: TAbstractTag;
    CommandTag: TAbstractTag;
    ExecType : TShellExecType;
  public
    procedure Execute;
  end;

  TShellExecFile = class(TQuoteTokenizer)
  private
    ProcessList : TList;
    Name : string;
    function GetProcess(Index : Integer) : TShellExecProcess;
    function GetProcessCount : Integer;
  public
    constructor Create(aName : string);
    procedure ReadFromStrings;
    property Process[Index : Integer] : TShellExecProcess read GetProcess; default;
    property ProcessCount : Integer read GetProcessCount;
  end;

  TShellExec = class(TCNC)
  private
    ShellExecFile : TShellExecFile;
    procedure SpawnProcess(aTag : TAbstractTag);
  public
    constructor Create(aOWner : TComponent); override;
    destructor Destroy; override;
    procedure InstallComponent(Params : TParamStrings); override;
    procedure Installed; override;
  end;


implementation


constructor TShellExec.Create(aOwner : TComponent);
begin
  inherited;
end;

destructor TShellExec.Destroy;
begin
  if Assigned(ShellExecFile) then
    ShellExecFile.Free;
  inherited;
end;

procedure TShellExec.InstallComponent(Params : TParamStrings);
begin
  inherited;
  ShellExecFile := TShellExecFile.Create(Name);
end;

procedure TShellExec.Installed;
var I : Integer;
begin
  inherited;
  ReadLive(ShellExecFile, Name);
  ShellExecFile.ReadFromStrings;

  for I := 0 to ShellExecFile.ProcessCount - 1 do begin
    if (ShellExecFile[I].TagName <> '') and
       (ShellExecFile[I].ApplicationName <> '') then
       ShellExecFile[I].Tag := TagEvent(ShellExecFile[I].TagName, EdgeFalling, TMethodTag, SpawnProcess);
    if (ShellExecFile[I].CommandTagName <> '') and
       (ShellExecFile[I].ApplicationName <> '') then
       ShellExecFile[I].CommandTag := TagEvent(ShellExecFile[I].CommandTagName, EdgeFalling, TStringTag, nil);
  end;
end;

procedure TShellExec.SpawnProcess(aTag : TAbstractTag);
var I : Integer;
begin
  if not ATag.AsBoolean then begin
    for I := 0 to ShellExecFile.ProcessCount - 1 do begin
      if aTag = ShellExecFile[I].Tag then
        ShellExecFile[I].Execute;
    end;
  end;
end;

constructor TShellExecFile.Create(aName : string);
begin
  inherited Create;
  Name := aName;
  ProcessList := TList.Create;
end;

function TShellExecFile.GetProcessCount : Integer;
begin
  Result := ProcessList.Count;
end;

function TShellExecFile.GetProcess(Index : Integer) : TShellExecProcess;
begin
  Result := TShellExecProcess(ProcessList[Index]);
end;

procedure TShellExecFile.ReadFromStrings;
var Token : string;

  procedure ReadProcess;
  var Exec : TShellExecProcess;
    function ReadExecType : TShellExecType;
    begin
      Token := LowerCase(ReadStringAssign);
      if Token = 'mime' then
        Result := seMime
      else
        Result := seConsole;
    end;

  begin
    Exec := TShellExecProcess.Create;
    Exec.ExecType := seConsole;
    ProcessList.Add(Exec);

    ExpectedTokens(['=', '{']);
    Token := LowerCase(Read);
    while not EndOfStream do begin
      if Token = 'application' then
        Exec.ApplicationName := ReadStringAssign
      else if token = 'commandline' then
        Exec.CommandLine := ReadStringAssign
      else if Token = 'tag' then
        Exec.TagName := ReadStringAssign
      else if Token = 'exectype' then
        Exec.ExecType := ReadExecType
      else if Token = 'commandtag' then
        Exec.CommandTagName := ReadStringAssign
      else if token = '}' then
        Exit
      else
        raise Exception.CreateFmt('Unexpected Token [%s]', [Token]);
      Token := LowerCase(Read);
    end;
    raise Exception.Create('Unexpected end of file');
  end;

begin
  Rewind;
  Seperator := '[]{}';
  QuoteChar := '''';

  ExpectedTokens([Name, '=', '{']);

  Token := LowerCase(Read);
  try
    while not EndOfStream do begin
      if Token = 'process' then
        ReadProcess
      else if Token = '}' then
        Exit
      else
        raise Exception.CreateFmt('Unexpected Token [%s]', [Token]);
      Token := LowerCase(Read);
    end;
    if ProcessCount > 0 then
      raise Exception.Create('Unexpected end of file');
  except
    on E : Exception do
      raise ECNCInstallFault.CreateFmt('%s configuration failed with the error [%s]', [Name, E.Message]);
  end;
end;

procedure TShellExecProcess.Execute;
var
  ProcessInformation : TProcessInformation;
  StartupInfo : TStartupInfo;
  Res : Integer;
begin
  try
    if Assigned(CommandTag) then
      CommandLine := CommandTag.AsString;

    case ExecType of
      seConsole : begin
        StartUpInfo.cb := SizeOf(TStartupInfo);
        StartupInfo.lpDesktop := nil;
        StartupInfo.lpTitle := PChar(ExtractFileName(ApplicationName));
        StartUpInfo.dwFlags := 0;

        if not CreateProcess (nil , PChar(ApplicationName + ' ' + CommandLine),
                        nil, nil, False,
                        CREATE_NEW_CONSOLE,
                        nil,
                        nil,
                        StartupInfo,
                        ProcessInformation) then

          EventLogFmt('Unable to initiate process [%s] : #%d ', [ApplicationName, GetLastError]);
      end;

      seMime : begin
        Res := ShellExecute(SysObj.VisualParent.Handle, 'open',
                          PChar(ApplicationName),
                          nil,
                          nil, SW_SHOWNORMAL);
        if Res <= 32 then
          EventLogFmt('Unable to initiate process %s Result code = %d', [ApplicationName, Res]);
      end;
    end;
  except
    on E : Exception do
      SysObj.FaultInterface.SetFault(SysObj.Comms, CoreFaults[cfSysWarning], [E.Message], nil);
  end;
end;

initialization
  RegisterCNCClass(TShellExec);
end.
