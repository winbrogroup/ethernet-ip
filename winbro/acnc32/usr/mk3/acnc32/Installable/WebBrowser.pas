unit WebBrowser;

interface

uses
  SysUtils, Classes, CncTypes, CoreCNC, CncAbs, Cnc32, Controls, Windows, SHDocVw;

type

  TMyWebBrowser = class(TWebBrowser)
  protected
    NeedDestroy: boolean;
    procedure DestroyWindowHandle; override;
    procedure SetParent(AParent: TWinControl); override;
  end;

  THtmlBrowser = class(TAbstractDisplay)
  private
    Page: TMyWebBrowser;
    PathTag: TAbstractTag;
    HomePage: string;
    RequestPage: TAbstractTag;
    procedure PageRefresh(ATag: TAbstractTag);
    procedure PageNext(ATag: TAbstractTag);
    procedure PagePrevious(ATag: TAbstractTag);
    procedure PageNavigateTo(ATag: TAbstractTag);
    procedure PageHome(ATag: TAbstractTag);
    procedure PageRequest(ATag: TAbstractTag);
  public
    constructor Create(Aowner : TComponent); override;
    procedure InstallComponent (Params : TParamStrings); override;
    procedure Installed; override;
  end;

implementation

{ THtmlBrowser }

constructor THtmlBrowser.Create(Aowner: TComponent);
begin
  inherited;
  Page:= TMyWebBrowser.Create(Self);
end;

procedure THtmlBrowser.Installed;
begin
  inherited;

  Page.SetParent(Self);
  Page.Align:= alClient;
  Page.Visible:= True;

  //Page.Navigate('file:///C:/Documents%20and%20Settings/pdavis/Desktop/wiki/index.html');
  Page.Navigate(HomePage);
end;

procedure THtmlBrowser.InstallComponent(Params: TParamStrings);
begin
  inherited;
  HomePAge:= Params.ReadString('Homepage', 'file:///' + ProfilePath + 'Wiki/index.html');
  TagEvent(Name + 'Refresh', EdgeChange, TIntegerTag, PageRefresh);
  TagEvent(Name + 'Next', EdgeChange, TIntegerTag, PageNext);
  TagEvent(Name + 'Previous', EdgeChange, TIntegerTag, PagePrevious);
  TagEvent(Name + 'NavigateTo', EdgeChange, TStringTag, PageNavigateTo);
  TagEvent(Name + 'Home', EdgeChange, TStringTag, PageHome);
  TagEvent(Name + 'NavigateTo', EdgeChange, TStringTag, PageRequest);
  PathTag:= TagPublish(Name + 'URL', TStringTag);
end;


procedure THtmlBrowser.PageRefresh(ATag: TAbstractTag);
begin
  if not ATag.AsBoolean then
    Page.Refresh;

  PathTag.AsString:= Page.LocationURL;
end;

procedure THtmlBrowser.PagePrevious(ATag: TAbstractTag);
begin
  if not ATag.AsBoolean then
    Page.GoBack;

  PathTag.AsString:= Page.LocationURL;
end;

procedure THtmlBrowser.PageNext(ATag: TAbstractTag);
begin
  try
  if not ATag.AsBoolean then
    Page.GoForward;

  PathTag.AsString:= Page.LocationURL;
  except
    on E: Exception do
      EventLog('Browser: ' + E.Message);
  end;
end;

procedure THtmlBrowser.PageNavigateTo(ATag: TAbstractTag);
begin
  Page.Navigate(ATag.AsString);
  PathTag.AsString:= Page.LocationURL;
end;

procedure THtmlBrowser.PageHome(ATag: TAbstractTag);
begin
  Page.Navigate(HomePage);
end;

{ TMyWebBrower }

procedure TMyWebBrowser.SetParent(AParent: TWinControl);
begin
  NeedDestroy := (AParent = nil);
  inherited;
  if AParent <> nil then
    Windows.SetParent(Handle, AParent.Handle);
end;

procedure TMyWebBrowser.DestroyWindowHandle;
begin
  inherited;
  if NeedDestroy then inherited;
  NeedDestroy := False;
end;


procedure THtmlBrowser.PageRequest(ATag: TAbstractTag);
var Path: string;
begin
  Path:= ATag.AsString;
  Path:= 'file:///' + Path;
  Page.Navigate(Path);
  Open;
end;

initialization
  RegisterCNCClass(THtmlBrowser, 'WebBrowser');
end.
