unit ActiveXDisplay;

interface

uses Classes, SysUtils, CoreCNC, CNC32, CNCTypes, CNCAbs, XHost, Controls,
     Graphics, Splash, Named, ActiveX, Variants, AxUtils, OleAutomation,
     IScriptInterface, FaultRegistration;

type
  TOleService = class(TExpandDisplay, IScriptLibrary)
  private
    FScriptHost: IScriptHost;
    procedure Reset; stdcall;
    function MethodCount: Integer; stdcall;
    function GetMethodByIndex(Index: Integer): IScriptMethod; stdcall;
    function GetMethodByName(Name: WideString): IScriptMethod; stdcall;
    function GetLibraryName: WideString; stdcall;
    procedure SetScriptHost(Host: IScriptHost); stdcall;
  protected
    Methods: TStringList;
    Properties: TStringList;
    function GetPropertyByID(ID: Integer): Variant; virtual; abstract;
    procedure SetPropertyByID(ID: Integer; const V: array of const); virtual; abstract;
    function NameToDispID(AName: string): Integer; virtual; abstract;
    procedure ValueChange(ATag: TAbstractTag);
    //procedure UpdateDisplay(ATag: TAbstractTag);
    procedure RegisterReadOnlyProperties;
    procedure RegisterWritableProperties;
    procedure DisplaySweep(ATag: TAbstractTag);
  public
    procedure Installed; override;
    constructor Create(AOwner: TComponent); override;
  end;

  TActiveXHost = class(TOleService)
  private
    AppletHost: TXAppletHost;
  protected
    function GetPropertyByID(ID: Integer): Variant; override;
    procedure SetPropertyByID(ID: Integer; const V: array of const); override;
    function NameToDispID(AName: string): Integer; override;

    procedure TestTestTest(ATag: TAbstractTag);
  public
    constructor Create(AOwner: TComponent); override;
    procedure InstallComponent(Params: TParamStrings); override;
    destructor Destroy; override;
  end;

  TAutomationXHost = class(TOleService)
  private
    Automation: TAutomationHost;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure InstallComponent(Params: TParamStrings); override;
    function GetPropertyByID(ID: Integer): Variant; override;
    procedure SetPropertyByID(ID: Integer; const V: array of const); override;
    function NameToDispID(AName: string): Integer; override;
  end;

implementation

constructor TOleService.Create(AOwner: TComponent);
begin
  inherited;
  Properties:= TStringList.Create;
  Methods:= TStringList.Create;
end;


procedure TOleService.RegisterReadOnlyProperties;
var I: Integer;
    W: TPropertyWrapper;
begin
  for I:= 0 to Properties.Count - 1 do begin
    W:= TPropertyWrapper(Properties.Objects[I]);
    if not W.Writable then
      W.FTag:= Self.TagPublish(Name + '_' + W.FName, Anc32Type2TagClass(W.FReturnType));
  end;
end;

procedure TOleService.RegisterWritableProperties;
var I: Integer;
    W: TPropertyWrapper;
begin
  for I:= 0 to Properties.Count - 1 do begin
    W:= TPropertyWrapper(Properties.Objects[I]);
    if not Assigned(W.FTag) then
      W.FTag:= Self.TagEvent(Name + '_' + W.FName, EdgeChange, Anc32Type2TagClass(W.FReturnType), ValueChange);
  end;
end;

procedure TOleService.DisplaySweep(ATag: TAbstractTag);
var I: Integer;
    W: TPropertyWrapper;
    Tmp: Variant;
begin
  for I:= 0 to Properties.Count - 1 do begin
    W:= TPropertyWrapper(Properties.Objects[I]);
    Tmp:= Self.GetPropertyByID(W.FMemberID);
    if Tmp <> W.FPrevious then begin
      W.FPrevious:= Tmp;
      W.FTag.AsOleVariant:= Tmp;
    end;
    VarClear(Tmp);
  end;
end;


procedure TOleService.Installed;
begin
  inherited;
  TagEvent(NameDisplaySweep, EdgeChange, TIntegerTag, DisplaySweep);
end;

procedure TOleService.ValueChange(ATag: TAbstractTag);
var I: Integer;
    W: TPropertyWrapper;
begin
  for I:= 0 to Properties.Count - 1 do begin
    W := TPropertyWrapper(Properties.Objects[I]);
    if W.Writable then begin
      try
        if ATag = W.FTag then begin
          W.Write;
        end;
      except
        on E: Exception do
          SysObj.FaultInterface.SetFault(Self, CoreFaults[cfSysWarning], [Format('Property set failed [%s]: %s', [W.FName, E.Message])], nil);
      end;
    end;
  end;
end;

{ TActiveXDisplay }

constructor TActiveXHost.Create(AOwner: TComponent);
begin
  inherited;
  AppletHost:= TXAppletHost.Create(Self);
  AppletHost.Parent:= Self;
  AppletHost.Align:= alClient;
  AppletHost.InActiveColor := clBlack;

  SysObj.AddOCXLibrary(Self);
end;


destructor TActiveXHost.Destroy;
begin
  SysObj.RemoveOCXLibrary(Self);
  AxUtils.DestroyInvokeList(Methods);
  AxUtils.DestroyPropertyList(Properties);
  if Assigned(AppletHost) then
    AppletHost.Parent:= nil;
  if Assigned(AppletHost) then
    AppletHost.Free;
  inherited;
end;

function TActiveXHost.GetPropertyByID(ID: Integer): Variant;
begin
  Result:= AppletHost.GetPropertyByID(ID);
end;

procedure TActiveXHost.InstallComponent(Params: TParamStrings);
var Filename: string;
    Path: string;
begin
  inherited;
  Filename := Params.ReadString('Filename', '');
  AppletHost.Active:= False;
  AppletHost.GUID := Params.ReadString('GUID', '');
  AppletHost.AutoExtractGUID:= True;
  Path:= Params.ReadString('path', ProfilePath + 'ocx\');
  AppletHost.FileName := Path + Filename;
  AppletHost.AutoResize := False;
  AppletHost.AutoRegister := Params.ReadBool('Autoregister', True);

  AppletHost.Parent:= Splash.SplashForm;
  AppletHost.Active := True;
  AppletHost.DefaultDispatch;
  AppletHost.EnumProperties(Properties);
  AppletHost.EnumInvokeList(Methods);

  AppletHost.Parent:= Self;
  Self.TagEvent('TestMe', EdgeFalling, TMethodTag, TestTestTest);
  Self.RegisterReadOnlyProperties;
  Self.RegisterWritableProperties;
end;


function TActiveXHost.NameToDispID(AName: string): Integer;
begin
  Result:= AppletHost.NameToDispID(aName);
end;

procedure TActiveXHost.SetPropertyByID(ID: Integer;
  const V: array of const);
begin
  AppletHost.SetPropertyByID(ID, V);
end;

{ TAutomationXHost }

constructor TAutomationXHost.Create(AOwner: TComponent);
begin
  inherited;
  SysObj.AddOCXLibrary(Self);
end;

destructor TAutomationXHost.Destroy;
begin
  if Assigned(Automation) then
    Automation.Free;
  inherited;
end;

function TAutomationXHost.GetPropertyByID(ID: Integer): Variant;
begin
  Result:= Automation.GetPropertyByID(ID);
end;

procedure TAutomationXHost.InstallComponent(Params: TParamStrings);
var Filename: string;
    Path: string;
begin
  inherited;
  Automation:= TAutomationHost.Create;
//  Automation.Test;
  Filename := Params.ReadString('Filename', '');
  Automation.Active:= False;
  Automation.GUID := Params.ReadString('GUID', '');
  Automation.AutoExtractGUID:= True;
  Path:= Params.ReadString('path', ProfilePath + 'ocx\');
  Automation.FileName := Path + Filename;
  Automation.AutoRegister := Params.ReadBool('Autoregister', True);

  Automation.Active := True;
  Automation.EnumProperties(Properties);
  Automation.EnumInvokeList(Methods);

  Self.RegisterReadOnlyProperties;
  Self.RegisterWritableProperties;
end;

function TAutomationXHost.NameToDispID(AName: string): Integer;
begin
  Result:= Automation.NameToDispID(AName);
end;

procedure TAutomationXHost.SetPropertyByID(ID: Integer; const V: array of const);
begin
  Automation.SetPropertyByID(ID, V);
end;

procedure TActiveXHost.TestTestTest(ATag: TAbstractTag);
//var I: Integer;
begin
  //I:= NameToDispID('SetText');
  Self.AppletHost.CallFunction('SetBothFields', ['Test:Test', 'Bling:Bling']);
end;

function TOleService.GetMethodByIndex(Index: Integer): IScriptMethod;
begin
  Result:= TFunctionWrapper(Methods.Objects[Index]);
end;

function TOleService.GetMethodByName(Name: WideString): IScriptMethod;
var I: Integer;
begin
  for I:= 0 to Methods.Count - 1 do begin
    if CompareText(Name, Methods[I]) = 0 then
      Result:= GetMethodByIndex(I);
  end;
end;

function TOleService.MethodCount: Integer;
begin
  Result:= Self.Methods.Count;
end;

function TOleService.GetLibraryName: WideString;
begin
  Result:= UpperCase(Name);
end;

procedure TOleService.Reset;
begin

end;

procedure TOleService.SetScriptHost(Host: IScriptHost);
begin
  FScriptHost:= Host;
end;

initialization
  CoreCNC.RegisterCNCClass(TActiveXHost);
  CoreCNC.RegisterCNCClass(TAutomationXHost);
end.
