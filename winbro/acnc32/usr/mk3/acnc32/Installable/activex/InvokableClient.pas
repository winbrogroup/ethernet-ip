unit InvokableClient;

interface

uses ActiveX;

type
  IInvokableClient = interface
    function GetClientDispatch: IDispatch;
    function InvokeDispatch(ID: TDispID; Flags: Word;
      var Params: TDispParams; var VarResult: Variant): HResult;

    property ClientDispatch: IDispatch read GetClientDispatch;
  end;

implementation

end.
