unit AxUtils;

interface

uses SysUtils, CoreCNC, CNC32, Classes, ActiveX, ComObj, AxCtrls, Windows,
     IScriptInterface, InvokableClient;

type
  TPropertyWrapper = class
  public
    FName: string;
    FTag: TAbstractTag;
    FMemberID: Integer;
    FHost: IInvokableClient;
    FReadable: Boolean;
    FWritable: Boolean;
    FReturnType: Integer;
    FVT_Type: Integer;
    FPrevious: Variant;
    constructor Create(Name: string; Host: IInvokableClient; TypeInfo: ITypeInfo; FuncDesc: PFuncDesc);
    destructor Destroy; override;
    property Readable: Boolean read FReadable write FReadable;
    property Writable: Boolean read FWritable write FWritable;
    procedure Write;
  end;

  TFunctionWrapper = class(TInterfacedObject, IScriptMethod)
  private
    FIsFunction: Boolean;
    FReturnType: Integer;
    FParameterPassing: TList;
    FParameterType: TList;
    FParameterName: TStringList;
    FParameterCount: Integer;
    FName: string;
    FScriptHost: IScriptHost;
    FHost: IInvokableClient;
    function InternalParameterPassing(Index: Integer): Integer;
  public
    MemberID: Integer;
    constructor Create(Name: string; Host: IInvokableClient; TypeInfo: ITypeInfo; FuncDesc: PFuncDesc);
    destructor Destroy; override;
    function Name: WideString; stdcall;
    procedure Reset; stdcall;
    function IsFunction: Boolean; stdcall;
    function ParameterPassing(Index: Integer): Integer; stdcall;
    function ParameterType(Index: Integer): Integer; stdcall;
    function ParameterName(Index: Integer): WideString; stdcall;
    function ReturnType: Integer; stdcall;
    function ParameterCount: Integer; stdcall;
    procedure Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer); stdcall;
    procedure SetScriptHost(Host: IScriptHost); stdcall;
    function GetScriptHost: IScriptHost; stdcall;
    function _AddRef: Integer; stdcall;
    function _Release: Integer; stdcall;
  end;

procedure EnumDispatchProperties(Host: IInvokableClient; PropType: TGUID;
  VTCode: Integer; PropList: TStrings);

function Anc32Type2TagClass(FType: Integer): TTagClass;
procedure EnumInvokeList(Host: IInvokableClient; AList: TStrings);
procedure VariantCast(var Dest:  TVariantArg; const Value: TVarRec);
procedure CheckHR(HR : HResult);
procedure DestroyInvokeList(AList: TStrings);
procedure DestroyPropertyList(AList: TStrings);

const
  //DISPATCH_METHODNOPARAM = DISPATCH_METHOD or DISPATCH_PROPERTYGET;
  //DISPATCH_METHODPARAMS  = DISPATCH_METHOD or DISPATCH_PROPERTYGET;
  Disp: TDispParams = (rgvarg: nil; rgdispidNamedArgs: nil; cArgs: 0;
    cNamedArgs: 0);


implementation


type
  TConstArray = array of TVarRec;

// Copies a TVarRec and its contents. If the content is referenced
// the value will be copied to a new location and the reference
// updated.
function CopyVarRec(const Item: TVarRec): TVarRec;
var
  W: WideString;
begin
  // Copy entire TVarRec first.
  Result := Item;

  // Now handle special cases.
  case Item.VType of
    vtExtended:
      begin
        New(Result.VExtended);
        Result.VExtended^ := Item.VExtended^;
      end;
    vtString:
      begin
        New(Result.VString);
        Result.VString^ := Item.VString^;
      end;
    vtPChar:
      Result.VPChar := StrNew(Item.VPChar);
    // There is no StrNew for PWideChar.
    vtPWideChar:
      begin
        W := Item.VPWideChar;
        GetMem(Result.VPWideChar,
               (Length(W) + 1) * SizeOf(WideChar));
        Move(PWideChar(W)^, Result.VPWideChar^,
             (Length(W) + 1) * SizeOf(WideChar));
      end;
    // A little trickier: casting to string will ensure
    // reference counting is done properly.
    vtAnsiString:
      begin
        // Nil out first, so no attempt to decrement
        // reference count.
        Result.VAnsiString := nil;
        string(Result.VAnsiString) := string(Item.VAnsiString);
      end;
    vtCurrency:
      begin
        New(Result.VCurrency);
        Result.VCurrency^ := Item.VCurrency^;
      end;
    vtVariant:
      begin
        New(Result.VVariant);
        Result.VVariant^ := Item.VVariant^;
      end;
    // Casting ensures proper reference counting.
    vtInterface:
      begin
        Result.VInterface := nil;
        IInterface(Result.VInterface) := IInterface(Item.VInterface);
      end;
    // Casting ensures a proper copy is created.
    vtWideString:
      begin
        Result.VWideString := nil;
        WideString(Result.VWideString) := WideString(Item.VWideString);
      end;
    vtInt64:
      begin
        New(Result.VInt64);
        Result.VInt64^ := Item.VInt64^;
      end;
    // VPointer and VObject don't have proper copy semantics so it
    // is impossible to write generic code that copies the contents.
  end;
end;

procedure Acnc32AssignTag(Tag: TAbstractTag; const Arg: tagVARIANT);
begin
  case Arg.vt of
    VT_UI1:                  //(bVal: Byte);
      Tag.AsInteger:= Arg.bVal;
    VT_I2:                   //(iVal: Smallint);
      Tag.AsInteger:= Arg.iVal;
    VT_I4:                   //(lVal: Longint);
      Tag.AsInteger:= Arg.lVal;
    VT_R4:                   //(fltVal: Single);
      Tag.AsDouble:= Arg.fltVal;
    VT_R8:                   //(dblVal: Double);
      Tag.AsDouble:= Arg.dblVal;
    VT_BOOL:                 //(vbool: TOleBool);
      Tag.AsBoolean:= Arg.vbool;
    VT_ERROR:                //(scode: HResult);
      Tag.AsString:= 'Error: ' + IntToStr(Arg.scode);
    VT_CY:                   //(cyVal: Currency);
      Tag.AsDouble:= Arg.cyVal;
    VT_DATE:                 //(date: TOleDate);
      Tag.AsDouble:= Arg.date;
    VT_BSTR:                 //(bstrVal: PWideChar{WideString});
      Tag.AsString:= Arg.bstrVal;
    VT_UNKNOWN:              //(unkVal: Pointer{IUnknown});
      Tag.AsString:= 'IUnknown';
    VT_DISPATCH:             //(dispVal: Pointer{IDispatch});
      Tag.AsString:= 'IDispatch';
    VT_ARRAY:                //(parray: PSafeArray);
      Tag.AsString:= 'PSafeArray';
    VT_BYREF or VT_UI1:      //(pbVal: ^Byte);
      Tag.AsInteger:= Arg.pbVal^;
    VT_BYREF or VT_I2:       //(piVal: ^Smallint);
      Tag.AsInteger:= Arg.piVal^;
    VT_BYREF or VT_I4:       //(plVal: ^Longint);
      Tag.AsInteger:= Arg.plVal^;
    VT_BYREF or VT_R4:       //(pfltVal: ^Single);
      Tag.AsDouble:= Arg.pfltVal^;
    VT_BYREF or VT_R8:       //(pdblVal: ^Double);
      Tag.AsDouble:= Arg.pdblVal^;
    VT_BYREF or VT_BOOL:     //(pbool: ^TOleBool);
      Tag.AsBoolean:= Arg.pbool^;
    VT_BYREF or VT_ERROR:    //(pscode: ^HResult);
      Tag.AsString:= 'Error: ' + IntToStr(Arg.pscode^);
    VT_BYREF or VT_CY:       //(pcyVal: ^Currency);
      Tag.AsDouble:= Arg.pcyVal^;
    VT_BYREF or VT_DATE:     //(pdate: ^TOleDate);
      Tag.AsDouble:= Arg.pdate^;
    VT_BYREF or VT_BSTR:     //(pbstrVal: ^WideString);
      Tag.AsString:= Arg.pbstrVal^;
    VT_BYREF or VT_UNKNOWN:  //(punkVal: ^IUnknown);
      Tag.AsString:= 'PIUnknown';
    VT_BYREF or VT_DISPATCH: //(pdispVal: ^IDispatch);
      Tag.AsString:= 'PIDispatch';
    VT_BYREF or VT_ARRAY:    //(pparray: ^PSafeArray);
      Tag.AsString:= 'PSafeArray';
    VT_BYREF or VT_VARIANT:  //(pvarVal: PVariant);
      Tag.AsOleVariant:= Arg.pvarVal^;
    VT_BYREF:                //(byRef: Pointer);
      Tag.AsString:= 'Error: ByRef';
    VT_I1:                   //(cVal: Char);
      Tag.AsString:= Arg.cVal;
    VT_UI2:                  //(uiVal: Word);
      Tag.AsInteger:= Arg.uiVal;
    VT_UI4:                  //(ulVal: LongWord);
      Tag.AsInteger:= Arg.ulVal;
    VT_INT:                  //(intVal: Integer);
      Tag.AsInteger:= Arg.intVal;
    VT_UINT:                 //(uintVal: LongWord);
      Tag.AsInteger:= Arg.uintVal;
    VT_BYREF or VT_DECIMAL:  //(pdecVal: PDecimal);
      Tag.AsDouble:= Arg.pdecVal.Lo32; // ??????????? Incomplete
    VT_BYREF or VT_I1:       //(pcVal: PChar);
      Tag.AsString:= Arg.pcVal^;
    VT_BYREF or VT_UI2:      //(puiVal: PWord);
      Tag.AsInteger:= Arg.puiVal^;
    VT_BYREF or VT_UI4:      //(pulVal: PInteger);
      Tag.AsInteger:= Arg.pulVal^;
    VT_BYREF or VT_INT:      //(pintVal: PInteger);
      Tag.AsInteger:= Arg.pintVal^;
    VT_BYREF or VT_UINT:     //(puintVal: PLongWord);
      Tag.AsInteger:= Arg.puintVal^;
  end;
end;

// Creates a TConstArray out of the values given. Uses CopyVarRec
// to make copies of the original elements.
function CreateConstArray(const Elements: array of const): TConstArray;
var
  I: Integer;
begin
  SetLength(Result, Length(Elements));
  for I := Low(Elements) to High(Elements) do
    Result[I] := CopyVarRec(Elements[I]);
end;

// TVarRecs created by CopyVarRec must be finalized with this function.
// You should not use it on other TVarRecs.
procedure FinalizeVarRec(var Item: TVarRec);
begin
  case Item.VType of
    vtExtended: Dispose(Item.VExtended);
    vtString: Dispose(Item.VString);
    vtPChar: StrDispose(Item.VPChar);
    vtPWideChar: FreeMem(Item.VPWideChar);
    vtAnsiString: string(Item.VAnsiString) := '';
    vtCurrency: Dispose(Item.VCurrency);
    vtVariant: Dispose(Item.VVariant);
    vtInterface: IInterface(Item.VInterface) := nil;
    vtWideString: WideString(Item.VWideString) := '';
    vtInt64: Dispose(Item.VInt64);
  end;
  Item.VInteger := 0;
end;

// A TConstArray contains TVarRecs that must be finalized. This function
// does that for all items in the array.
procedure FinalizeVarRecArray(var Arr: TConstArray);
var
  I: Integer;
begin
  for I := Low(Arr) to High(Arr) do
    FinalizeVarRec(Arr[I]);
  Arr := nil;
end;

procedure EnumDispatchProperties(Host: IInvokableClient; PropType: TGUID;
  VTCode: Integer; PropList: TStrings);
var
  I: Integer;
  TypeInfo: ITypeInfo;
  TypeAttr: PTypeAttr;
  FuncDesc: PFuncDesc;

  // Restrict property names entering the Named layer to those
  // that are prefixed with NC_
  procedure SaveName(FuncDesc: PFuncDesc; Read: Boolean);
  var
    Name: WideString;
    Wrapper: TPropertyWrapper;
    Id: Integer;
    I: Integer;
  begin
    Id:= FuncDesc.memid;
    OleCheck(TypeInfo.GetDocumentation(Id, @Name, nil, nil, nil));
    if Pos('NL_', Name) = 1 then begin
      I:= PropList.IndexOf(Name);
      if I = -1 then begin
        Wrapper:= TPropertyWrapper.Create(Name, Host, TypeInfo, FuncDesc);
        PropList.AddObject(Name, Wrapper);
      end
      else begin
        Wrapper:= TPropertyWrapper(PropList.Objects[I]);
        if Wrapper.FMemberID <> Id then
          Exit;
      end;
      if Read then
        Wrapper.Readable:= True
      else
        Wrapper.Writable:= True;
    end;
  end;

begin
  // Obtain the ITypeInfo structure
  OleCheck(Host.ClientDispatch.GetTypeInfo(0,0,TypeInfo));

  if TypeInfo = nil then
    Exit;

  OleCheck(TypeInfo.GetTypeAttr(TypeAttr));
  try
    // For each function
    for I := 0 to TypeAttr.cFuncs - 1 do begin
      OleCheck(TypeInfo.GetFuncDesc(I, FuncDesc));
      try
        if (FuncDesc.invkind and INVOKE_PROPERTYGET <> 0) then
          SaveName(FuncDesc, True);

        if(FuncDesc.invkind and INVOKE_PROPERTYPUT <> 0) then
          SaveName(FuncDesc, False);
      finally
        TypeInfo.ReleaseFuncDesc(FuncDesc);
      end;
    end;

    // For each interface
    for I:= 0 to TypeAttr.cImplTypes do begin
    end;

  finally
    TypeInfo.ReleaseTypeAttr(TypeAttr);
  end;
end;

procedure DestroyInvokeList(AList: TStrings);
begin
  while(AList.Count > 0) do begin
    TFunctionWrapper(AList.Objects[0]).Free;
    AList.Delete(0);
  end;
end;

procedure DestroyPropertyList(AList: TStrings);
begin
  while(AList.Count > 0) do begin
    TPropertyWrapper(AList.Objects[0]).Free;
    AList.Delete(0);
  end;
end;

procedure EnumInvokeList(Host: IInvokableClient; AList: TStrings);
var
  I: Integer;
  TypeInfo: ITypeInfo;
  TypeAttr: PTypeAttr;
  FuncDesc: PFuncDesc;
  Name: WideString;

begin
  OleCheck(Host.ClientDispatch.GetTypeInfo(0, 0, TypeInfo));
  if TypeInfo = nil then
    Exit;

  OleCheck(TypeInfo.GetTypeAttr(TypeAttr));
  try
    for I := 0 to TypeAttr.cFuncs - 1 do begin
      OleCheck(TypeInfo.GetFuncDesc(I, FuncDesc));
      try
        if (FuncDesc.invkind = INVOKE_FUNC) then begin
          OleCheck(TypeInfo.GetDocumentation(funcDesc.memid, @Name, nil, nil, nil));
          if (Name <> 'AddRef') and
             (Name <> 'Invoke') and
             (Name <> 'GetIDsOfNames') and
             (Name <> 'GetTypeInfo') and
             (Name <> 'GetTypeInfoCount') and
             (Name <> 'QueryInterface') and
             (Name <> '_AddRef') and
             (Name <> 'Release') then begin
               AList.AddObject(Name, TFunctionWrapper.Create(Name, Host, TypeInfo, FuncDesc));
               CoreCnc.MessageLog('Method: ' + Name);
          end;
        end;
      finally
        TypeInfo.ReleaseFuncDesc(FuncDesc);
      end;
    end;

  finally
    TypeInfo.ReleaseTypeAttr(TypeAttr);
  end;
end;

procedure VariantCast(var Dest:  TVariantArg; const Value: TVarRec);
begin
    with Value do
      case VType of
        vtInteger:
          begin
            Dest.vt := VT_I4;
            Dest.lVal := VInteger;
          end;
        vtBoolean:
          begin
            Dest.vt := VT_BOOL;
            Dest.vbool := VBoolean;
          end;
        vtChar:
          begin
            Dest.vt := VT_BSTR;
            Dest.bstrVal := StringToOleStr(VChar);
          end;
        vtExtended:
          begin
            Dest.vt := VT_R8;
            Dest.dblVal := VExtended^;
          end;
        vtString:
          begin
            Dest.vt := VT_BSTR;
            Dest.bstrVal := StringToOleStr(VString^);
          end;
        vtPointer:
          if VPointer = nil then begin
            Dest.vt := VT_NULL;
            Dest.byRef := nil;
          end
          else begin
            Dest.vt := VT_BYREF;
            Dest.byRef := VPointer;
          end;
        vtPChar:
          begin
            Dest.vt := VT_BSTR;
            Dest.bstrVal := StringToOleStr(StrPas(VPChar));
          end;
        vtObject:
          begin
            Dest.vt := VT_BYREF;
            Dest.byRef := VObject;
          end;
        vtClass:
          begin
            Dest.vt := VT_BYREF;
            Dest.byRef := VClass;
          end;
        vtWideChar:
          begin
            Dest.vt := VT_BSTR;
            Dest.bstrVal := @VWideChar;
          end;
        vtPWideChar:
          begin
            Dest.vt := VT_BSTR;
            Dest.bstrVal := VPWideChar;
          end;
        vtAnsiString:
          begin
            Dest.vt := VT_BSTR;
            Dest.bstrVal := StringToOleStr(string(VAnsiString));
          end;
        vtCurrency:
          begin
            Dest.vt := VT_CY;
            Dest.cyVal := VCurrency^;
          end;
        vtVariant:
          begin
            Dest.vt := VT_BYREF or VT_VARIANT;
            Dest.pvarVal := VVariant;
          end;
        vtInterface:
          begin
            Dest.vt := VT_UNKNOWN or VT_BYREF;
            Dest.byRef := VInterface;
          end;
        vtInt64:
          begin
            Dest.vt := VT_I8 or VT_BYREF;
            Dest.byRef := VInt64;
          end;
        else raise EOleError.Create('Invalid parameter typecast');
      end;
end;

procedure Acnc32Cast(var Dest:  TVariantArg; Tag: TAbstractTag; dtType: Integer);
begin
  case dtType of
    dtInteger: begin
      Dest.vt := VT_I4;
      Dest.lVal := Tag.AsInteger;
    end;

    dtDouble: begin
      Dest.vt := VT_R8;
      Dest.dblVal := Tag.AsDouble;
    end;

    dtString: begin
      Dest.vt := VT_BSTR;
      Dest.bstrVal := StringToOleStr(Tag.AsString);
    end;

    dtBoolean: begin
      Dest.vt := VT_BOOL;
      Dest.vbool := Tag.AsBoolean;
    end;

    dtArray: begin
    end;

    dtVariant: begin
      Dest.vt:= VT_VARIANT;
      Dest.pvarVal^:= Tag.AsOleVariant
    end;

    dtUnknown: begin
    end;

  else
    Dest.vt := VT_BSTR;
    Dest.bstrVal := StringToOleStr(Tag.AsString);
  end;
end;

function Anc32Type2TagClass(FType: Integer): TTagClass;
begin
  try
    case FType of
      dtInteger: Result:= TIntegerTag;
      dtDouble: Result:= TDoubleTag;
      dtString: Result:= TStringTag;
      dtBoolean: Result:= TIntegerTag;
      dtArray: raise Exception.Create('Array not supported');
      dtVariant: raise Exception.Create('Variant not supperted');
      dtUnknown: raise Exception.Create('Unknown not supported');
    else
      raise Exception.CreateFmt('Unknown data type %d', [FType]);
    end;
  except
    on E: Exception do
      raise Exception.Create('Acnc32Type2TagClass: ' + E.Message);
  end;
end;

procedure CheckHR(HR : HResult);
var st : String;
begin
  case HR of
    S_OK : Exit;
    DISP_E_BADPARAMCOUNT : st := 'The number of elements provided to DISPPARAMS is different from the number of arguments accepted by the method or property';
    DISP_E_BADVARTYPE    : st := 'One of the arguments in rgvarg is not a valid variant type';
    DISP_E_EXCEPTION     : st := 'OLE Error';
    DISP_E_MEMBERNOTFOUND: st := 'The requested member does not exist, or the call to Invoke tried to set the value of a read-only property';
    DISP_E_NONAMEDARGS   : st := 'This implementation of IDispatch does not support named arguments';
    DISP_E_OVERFLOW      : st := 'One of the arguments in rgvarg could not be coerced to the specified type';
    DISP_E_PARAMNOTFOUND : st := 'One of the parameter DISPIDs does not correspond to a parameter on the method. In this case, puArgErr should be set to the first argument that contains the error';
    DISP_E_TYPEMISMATCH  : st := 'One or more of the arguments could not be coerced. The index within rgvarg of the first parameter with the incorrect type is returned in the puArgErr parameter';
    DISP_E_UNKNOWNINTERFACE : st := 'The interface identifier passed in riid is not IID_NULL';
    DISP_E_UNKNOWNLCID      : st := 'The member being invoked interprets string arguments according to the LCID, and the LCID is not recognized. If the LCID is not needed to interpret arguments, this error should not be returned';
    DISP_E_PARAMNOTOPTIONAL : st := 'A required parameter was omitted';
  end; //case
   raise Exception.Create(St);
end;


{ Converts VT_type used by ? to internal types }
function ConvertVtToDataType(Vt: Integer): Integer;
begin
  case Vt of
    VT_I2        ,      { [V][T][P]  2 byte signed int           }
    VT_I4        ,      { [V][T][P]  4 byte signed int           }
    VT_I1        ,      {    [T]     signed char                 }
    VT_UI1       ,      {    [T]     unsigned char               }
    VT_UI2       ,      {    [T]     unsigned short              }
    VT_UI4       ,      {    [T]     unsigned long               }
    VT_I8        ,      {    [T][P]  signed 64-bit int           }
    VT_UI8       ,      {    [T]     unsigned 64-bit int         }
    VT_INT       ,      {    [T]     signed machine int          }
    VT_UINT      :      {    [T]     unsigned machine int        }
      begin
        Result:= dtInteger;
      end;

    VT_BOOL      :      { [V][T][P]  True=-1, False=0            }
      begin
        Result:= dtBoolean;
      end;

    VT_R4        ,      { [V][T][P]  4 byte real                 }
    VT_R8        ,      { [V][T][P]  8 byte real                 }
    VT_DECIMAL   :      { [V][T]   [S]  16 byte fixed point      }
      begin
        Result:= dtDouble;
      end;


    VT_VARIANT   ,      { [V][T][P]  VARIANT FAR*                }
    VT_CY        ,      { [V][T][P]  currency                    }
    VT_DATE      ,      { [V][T][P]  date                        }
    VT_BSTR      ,      { [V][T][P]  binary string               }
    VT_LPSTR     ,      {    [T][P]  null terminated string      }
    VT_LPWSTR    :      {    [T][P]  wide null terminated string }
      begin
        Result:= dtString;
      end;

    (*
    VT_DISPATCH  :      { [V][T]     IDispatch FAR*              }
    VT_ERROR     :      { [V][T]     SCODE                       }
    VT_UNKNOWN   :      { [V][T]     IUnknown FAR*               }
    VT_VOID      :      {    [T]     C style void                }
    VT_HRESULT   :      {    [T]                                 }
    VT_PTR       :      {    [T]     pointer type                }
    VT_SAFEARRAY :      {    [T]     (use VT_ARRAY in VARIANT)   }
    VT_CARRAY    :      {    [T]     C style array               }
    VT_USERDEFINED:     {    [T]     user defined type          }
    *)
  else
    Result:= dtUnknown;
  end;
end;

{ TFunctionWrapper }

constructor TFunctionWrapper.Create(Name: string; Host: IInvokableClient; TypeInfo: ITypeInfo; FuncDesc: PFuncDesc);
var I: Integer;
    cNames: Integer;
    fNames: PBStrList;
    Passing: Integer;
    PType: Integer;
begin
  FHost:= Host;
  FParameterPassing:= TList.Create;
  FParameterName:= TStringList.Create;
  FParameterType:= TList.Create;

  Self.FName:= Name;

  // If the first name in fNames = Name then this is a function
  System.GetMem(fNames, 20 * Sizeof(POleStr));
  try
    cNames:= 20;
    TypeInfo.GetNames(funcDesc.memid, fNames, 20, cNames);

    MemberID:= FuncDesc.memid;
    FParameterCount:= FuncDesc.cParams;
    FReturnType:= dtString; // ????

    FIsFunction:= FuncDesc.elemdescFunc.tdesc.vt <> VT_VOID;

    if(IsFunction) then
      FReturnType:= ConvertVtToDataType(FuncDesc.elemdescFunc.tdesc.vt);

    if FReturnType = dtUnknown then
      raise Exception.Create('Failed to create FunctionWrapper for ' + Name + ' Return Datatype: ' + IntToStr(FuncDesc.elemdescFunc.tdesc.vt));


    for I:= 0 to FuncDesc.cParams - 1 do begin
      Passing:= PassAsConst;
      if (FuncDesc.lprgelemdescParam[i].paramdesc.wParamFlags and  PARAMFLAG_FOUT) <> 0 then
        Passing:= PassByReference;

      PType:= FuncDesc.lprgelemdescParam[i].tdesc.vt;
      if(PType = VT_PTR) then begin
        PType:= Funcdesc.lprgelemdescParam[i].tdesc.ptdesc.vt;
      end;

      FParameterPassing.Add(Pointer(Passing));
      FParameterName.Add(fNames[I+1]);
      FParameterType.Add(Pointer(ConvertVtToDataType(PType)));
    end;

  finally
    System.FreeMem(fNames);
  end;
end;

// Execute is called with a const array of TTag references
destructor TFunctionWrapper.Destroy;
begin
  FParameterPassing.Free;
  FParameterType.Free;
  FParameterName.Free;
  FScriptHost:= nil;
  FHost:= nil;
  inherited;
end;

procedure TFunctionWrapper.Execute(const Res: Pointer;
  const Params: array of Pointer; ParamCount: Integer);

var
  Disp: TDispParams;
  ArgCnt, I: Integer;
  Args: array[0..63] of TVariantArg;
  ResVar: Variant;
  Tag: TAbstractTag;
begin
  ArgCnt := 0;
  try
    for I := 0 to ParamCount - 1 do begin
      Acnc32Cast(Args[ParamCount - 1 - I], TAbstractTag(Params[I]), Self.ParameterType(I));
      Inc(ArgCnt);
      if ArgCnt >= 64 then Break;
    end;
    if ArgCnt = 0 then
      Disp.rgvarg := nil
    else
      Disp.rgvarg := @Args;

    Disp.rgdispidNamedArgs := nil;
    Disp.cArgs := ArgCnt;
    Disp.cNamedArgs := 0;

    OleCheck(FHost.InvokeDispatch(MemberID, DISPATCH_METHOD, Disp, ResVar));


    for I := 0 to ParamCount - 1 do begin
      if InternalParameterPassing(I) = PassByReference then begin
        Tag:= TAbstractTag(Params[I]);
        if not Tag.IsConstant and Tag.IsWritable then
          Acnc32AssignTag(Tag, Disp.rgvarg[ParamCount - 1 - I]);
        if Disp.rgvarg[ParamCount - 1 - I].vt = VT_BSTR then begin
          //  ARE THERE ANY OTHER SPECIAL CASES MEMORY??????
          WideString(Pointer(Disp.rgvarg[ParamCount - 1 - I].bstrVal)) := '';
        end;
      end;
    end;

    if IsFunction and (Res <> nil) then begin
      Tag:= TAbstractTag(Res);
      Tag.AsOleVariant:= ResVar;
    end;

  finally
  end;
end;

function TFunctionWrapper.GetScriptHost: IScriptHost;
begin
  Result:= FScriptHost;
end;

function TFunctionWrapper.InternalParameterPassing(
  Index: Integer): Integer;
begin
 Result:= Integer(Self.FParameterPassing[Index])
end;

function TFunctionWrapper.IsFunction: Boolean;
begin
  Result:= FIsFunction;
end;

function TFunctionWrapper.Name: WideString;
begin
  Result:= FName;
end;

function TFunctionWrapper.ParameterCount: Integer;
begin
  Result:= Self.FParameterCount;
end;

function TFunctionWrapper.ParameterName(Index: Integer): WideString;
begin
  Result:= Self.FParameterName[Index];
end;

function TFunctionWrapper.ParameterPassing(Index: Integer): Integer;
begin
  Result:= PassAsConst;
end;

function TFunctionWrapper.ParameterType(Index: Integer): Integer;
begin
  Result:= Integer(Self.FParameterType[Index]);
end;

procedure TFunctionWrapper.Reset;
begin

end;

function TFunctionWrapper.ReturnType: Integer;
begin
  Result:= Self.FReturnType;
end;

procedure TFunctionWrapper.SetScriptHost(Host: IScriptHost);
begin
  FScriptHost:= Host;
end;

function TFunctionWrapper._AddRef: Integer;
begin
  Result:= 1;
end;

function TFunctionWrapper._Release: Integer;
begin
  Result:= 1;
end;


{ TPropertyWrapper }

constructor TPropertyWrapper.Create(Name: string; Host: IInvokableClient;
  TypeInfo: ITypeInfo; FuncDesc: PFuncDesc);
begin
  Self.FName:= Name;
  Self.FHost:= Host;

  FVT_Type:= -1;

  if(FuncDesc.elemdescFunc.tdesc.vt <> VT_VOID) then begin
    FVT_Type:= FuncDesc.elemdescFunc.tdesc.vt;
    FReturnType:= ConvertVtToDataType(FVT_Type)
  end;

  if FuncDesc.cParams > 0 then begin
    FVT_Type:= FuncDesc.lprgelemdescParam[0].tdesc.vt;

    if(FVT_Type = VT_PTR) then begin
      FVT_Type:= Funcdesc.lprgelemdescParam[0].tdesc.ptdesc.vt;
    end;

    FReturnType:= ConvertVtToDataType(FVT_Type);
  end;

  {
  if FReturnType = dtUnknown then
    raise Exception.Create('Failed to create PropertyWrapper for ' + Name + ' Return Datatype: ' + IntToStr(FuncDesc.elemdescFunc.tdesc.vt));
    }

  if FReturnType = dtUnknown then
    EventLog('Failed to create PropertyWrapper for ' + Name + ' Return Datatype: ' + IntToStr(FVT_Type));
  Self.FMemberID:= FuncDesc.memid;
end;

destructor TPropertyWrapper.Destroy;
begin
  FHost:= nil;
  inherited;
end;

procedure TPropertyWrapper.Write;
const
  NameArg: TDispID = DISPID_PROPERTYPUT;
var
  Disp: TDispParams;
  Args: array[0..1] of TVariantArg;
  ResVar: Variant;
begin
  Acnc32Cast(Args[0], FTag, FReturnType);
  Disp.rgvarg := @Args;

  Disp.rgdispidNamedArgs := @NameArg;
  Disp.cArgs := 1;
  Disp.cNamedArgs := 1;

  OleCheck(FHost.InvokeDispatch(FMemberID, DISPATCH_PROPERTYPUT, Disp, ResVar));

  if Disp.rgvarg[0].vt = VT_BSTR then begin
    WideString(Pointer(Disp.rgvarg[0].bstrVal)) := '';
  end;
end;

end.
