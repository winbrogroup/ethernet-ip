unit XHost;

interface

uses Windows, Messages, ActiveX, SysUtils, Classes, Controls, Forms,
  Menus, Graphics, ComObj, AxCtrls, OleCtrls, ShlObj,  Dialogs, Variants,
  Registry, AxUtils, InvokableClient, IScriptInterface;

const
  ProcName:  PChar = 'DllRegisterServer';

type
  TRegProc = function : HResult; stdcall;
  TXApplet = class;
  TXAppletHost = class;


  TXAppletHost = class(TWinControl, IDispatch, IInvokableClient)
  private
    FAutoRegister  : boolean;
    FAutoResize    : boolean;
    FApplet        : TXApplet;
    FAutoExtractGUID : boolean;
    FBeforeActivate   : TNotifyEvent;
    FAfterActivate    : TNotifyEvent;
    FBeforeDeactivate : TNotifyEvent;
    FAfterDeactivate  : TNotifyEvent;
    FGUID          : TGUID;
    FGUIDStr       : string;
    FInactiveColor : TColor;
    FActive        : boolean;
    FFilename : string;
    procedure SetActive(Value : boolean);
    procedure SetInactiveColor(Value : TColor);
    Procedure SetGUIDStr(Value : string);
    function  GetApplet : TXApplet;
    function  GetDefaultDispatch : IDispatch;
    function  GetClientDispatch : IDispatch;
   protected
    { IDispatch }
    function GetTypeInfoCount(out Count: Integer): HResult; stdcall;
    function GetTypeInfo(Index, LocaleID: Integer; out TypeInfo): HResult; stdcall;
    function GetIDsOfNames(const IID: TGUID; Names: Pointer;
      NameCount, LocaleID: Integer; DispIDs: Pointer): HResult; stdcall;
    function Invoke(DispID: Integer; const IID: TGUID; LocaleID: Integer;
      Flags: Word; var Params; VarResult, ExcepInfo, ArgErr: Pointer): HResult; stdcall;
    procedure Loaded; override;
    {Applet}
    function GetAppletTypeInfoCount : integer;
    function InvokeDispatch(DispID: Integer;
                     Flags : Word;
                     var Params     : TDispParams;
                     var VarResult  : Variant): HResult;

    //function NameToDispID(const AName: string): TDispID;
    function NameToDispIDs(const AName: string;
                           const AParams: array of string;
                           Dest: PDispIDList): PDispIDList;

    function CallMethodNoParams(ID : TDispID) : Variant;
    function CallMethod(ID: TDispID; const Params: array of const): Variant;
    function CallMethodNamedParams(const IDs: TDispIDList;
                                   const Params: array of const; Cnt: Byte): Variant;

  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    procedure   RegisterApplet;
    function    AppletRegistered : boolean;
    function    ExtractGUID : TGUID;
    procedure   BrowseProperties;
    property    Applet : TXApplet read GetApplet;
    property    DefaultDispatch : IDispatch read GetDefaultDispatch;
    function NameToDispID(const AName: string): TDispID;
    function  GetPropertyByID(ID: TDispID): Variant;
    procedure SetPropertyByID(ID: TDispID; const Prop: array of const);
    //properties
    function    GetProperty(const AName: string): Variant;
    procedure   SetProperty(const AName: string; const Prop: array of const);
    function    GetTypeDescription(V : Variant) : string;
    procedure   EnumProperties(AList : TStrings);
    procedure EnumInvokeList(AList: TStrings);
    //functions
    function    CallFunction(const AName: string; const Params: array of const): Variant; overload;
    function    CallFunction(const AName: string; const Params: array of const;
                                        const ParamNames: array of string): Variant; overload;
    function    CallFunction(const AName: string): Variant; overload;

    //procedures
    procedure   CallProcedure(const AName: string; const Params: array of const); overload;
    procedure   CallProcedure(const AName: string; const Params: array of const;
                              const ParamNames: array of string); overload;
    procedure   CallProcedure(const AName: string); overload;
  published
    property    Ctl3D;
    property    Visible;
    property    BevelInner;
    property    BevelOuter;
    property    BevelKind;
    property    BevelWidth;
    property    BorderWidth;
    property    Active        : boolean read FActive write SetActive default false;
    property    InActiveColor : TColor read FInactiveColor write SetInactiveColor;
    property    GUID          : string read FGUIDStr  write SetGUIDStr;
    property    BeforeActivate: TNotifyEvent read FBeforeActivate write FBeforeActivate;
    property    AfterActivate : TNotifyEvent read FAfterActivate write FAfterActivate;
    property    BeforeDeactivate : TNotifyEvent read FBeforeDeactivate write FBeforeDeactivate;
    property    AfterDeactivate  : TNotifyEvent read FAfterDeactivate write FAfterDeactivate;
    property    AutoExtractGUID : boolean read FAutoExtractGUID write FAutoExtractGUID default true;
    property    Filename: string read FFilename write FFilename;
    property    AutoResize    : boolean read FAutoResize write FAutoResize default true;
    property    AutoRegister  : boolean read FAutoRegister write FAutoRegister;
    property ClientDispatch: IDispatch read GetClientDispatch;
  end;


  TXApplet = class(TWinControl, IUnknown, IOleClientSite,
    IOleControlSite, IOleInPlaceSite, IOleInPlaceFrame, IDispatch,
    IPropertyNotifySink, ISimpleFrameSite)
  private
    FHost : TXAppletHost;
    FControlData: PControlData;
    FRefCount: Longint;
    FEventDispatch: TEventDispatch;
    FObjectData: HGlobal;
    FOleObject: IOleObject;
    FPersistStream: IPersistStreamInit;
    FOleControl: IOleControl;
    FControlDispatch: IDispatch;
    FPropBrowsing: IPerPropertyBrowsing;
    FOleInPlaceObject: IOleInPlaceObject;
    FOleInPlaceActiveObject: IOleInPlaceActiveObject;
    FPropConnection: Longint;
    FEventsConnection: Longint;
    FMiscStatus: Longint;
    FFonts: TList;
    FPictures: TList;
    FUpdatingPictures: Boolean;
    FUpdatingColor: Boolean;
    FUpdatingFont: Boolean;
    FUpdatingEnabled: Boolean;
    { TOleControl }
    procedure CreateControl;
    procedure CreateInstance;
    procedure DesignModified;
    procedure DestroyControl;
    procedure DestroyEnumPropDescs;
    function  GetMainMenu: TMainMenu;
    function  GetDefaultDispatch: IDispatch;
    procedure HookControlWndProc;
    procedure SetUIActive(Active: Boolean);
    procedure WMEraseBkgnd(var Message: TWMEraseBkgnd); message WM_ERASEBKGND;
    procedure WMPaint(var Message: TWMPaint); message WM_PAINT;
    procedure CMDocWindowActivate(var Message: TMessage); message CM_DOCWINDOWACTIVATE;
    procedure CMColorChanged(var Message: TMessage); message CM_COLORCHANGED;
    procedure CMEnabledChanged(var Message: TMessage); message CM_ENABLEDCHANGED;
    procedure CMFontChanged(var Message: TMessage); message CM_FONTCHANGED;
    procedure CMDialogKey(var Message: TMessage); message CM_DIALOGKEY;
    procedure CMUIActivate(var Message: TMessage); message CM_UIACTIVATE;
    procedure CMUIDeactivate(var Message: TMessage); message CM_UIDEACTIVATE;
  protected
    FEvents: Integer;
    { IUnknown }
    function QueryInterface(const IID: TGUID; out Obj): HResult; override;
    function _AddRef: Integer; stdcall;
    function _Release: Integer; stdcall;
    { IOleClientSite }
    function SaveObject: HResult; stdcall;
    function GetMoniker(dwAssign: Longint; dwWhichMoniker: Longint;
      out mk: IMoniker): HResult; stdcall;
    function GetContainer(out container: IOleContainer): HResult; stdcall;
    function ShowObject: HResult; stdcall;
    function OnShowWindow(fShow: BOOL): HResult; stdcall;
    function RequestNewObjectLayout: HResult; stdcall;
    { IOleControlSite }
    function OnControlInfoChanged: HResult; stdcall;
    function LockInPlaceActive(fLock: BOOL): HResult; stdcall;
    function GetExtendedControl(out disp: IDispatch): HResult; stdcall;
    function TransformCoords(var ptlHimetric: TPoint; var ptfContainer: TPointF;
      flags: Longint): HResult; stdcall;
    function IOleControlSite.TranslateAccelerator = OleControlSite_TranslateAccelerator;
    function OleControlSite_TranslateAccelerator(msg: PMsg;
      grfModifiers: Longint): HResult; stdcall;
    function OnFocus(fGotFocus: BOOL): HResult; stdcall;
    function ShowPropertyFrame: HResult; stdcall;
    { IOleWindow }
    function ContextSensitiveHelp(fEnterMode: BOOL): HResult; stdcall;
    { IOleInPlaceSite }
    function IOleInPlaceSite.GetWindow = OleInPlaceSite_GetWindow;
    function OleInPlaceSite_GetWindow(out wnd: HWnd): HResult; stdcall;
    function CanInPlaceActivate: HResult; stdcall;
    function OnInPlaceActivate: HResult; stdcall;
    function OnUIActivate: HResult; stdcall;
    function GetWindowContext(out frame: IOleInPlaceFrame;
      out doc: IOleInPlaceUIWindow; out rcPosRect: TRect;
      out rcClipRect: TRect; out frameInfo: TOleInPlaceFrameInfo): HResult;
      stdcall;
    function Scroll(scrollExtent: TPoint): HResult; stdcall;
    function OnUIDeactivate(fUndoable: BOOL): HResult; stdcall;
    function OnInPlaceDeactivate: HResult; stdcall;
    function DiscardUndoState: HResult; stdcall;
    function DeactivateAndUndo: HResult; stdcall;
    function OnPosRectChange(const rcPosRect: TRect): HResult; stdcall;
    { IOleInPlaceUIWindow }
    function GetBorder(out rectBorder: TRect): HResult; stdcall;
    function RequestBorderSpace(const borderwidths: TRect): HResult; stdcall;
    function SetBorderSpace(pborderwidths: PRect): HResult; stdcall;
    function SetActiveObject(const activeObject: IOleInPlaceActiveObject;
      pszObjName: POleStr): HResult; stdcall;
    { IOleInPlaceFrame }
    function IOleInPlaceFrame.GetWindow = OleInPlaceFrame_GetWindow;
    function OleInPlaceFrame_GetWindow(out wnd: HWnd): HResult; stdcall;
    function InsertMenus(hmenuShared: HMenu;
      var menuWidths: TOleMenuGroupWidths): HResult; stdcall;
    function SetMenu(hmenuShared: HMenu; holemenu: HMenu;
      hwndActiveObject: HWnd): HResult; stdcall;
    function RemoveMenus(hmenuShared: HMenu): HResult; stdcall;
    function SetStatusText(pszStatusText: POleStr): HResult; stdcall;
    function EnableModeless(fEnable: BOOL): HResult; stdcall;
    function IOleInPlaceFrame.TranslateAccelerator = OleInPlaceFrame_TranslateAccelerator;
    function OleInPlaceFrame_TranslateAccelerator(var msg: TMsg;
      wID: Word): HResult; stdcall;
    { IDispatch }
    function GetTypeInfoCount(out Count: Integer): HResult; stdcall;
    function GetTypeInfo(Index, LocaleID: Integer; out TypeInfo): HResult; stdcall;
    function GetIDsOfNames(const IID: TGUID; Names: Pointer;
      NameCount, LocaleID: Integer; DispIDs: Pointer): HResult; stdcall;
    function Invoke(DispID: Integer; const IID: TGUID; LocaleID: Integer;
      Flags: Word; var Params; VarResult, ExcepInfo, ArgErr: Pointer): HResult; stdcall;
    { ISimpleFrameSite }
    function PreMessageFilter(wnd: HWnd; msg, wp, lp: Integer;
      out res: Integer; out Cookie: Longint): HResult; stdcall;
    function PostMessageFilter(wnd: HWnd; msg, wp, lp: Integer;
      out res: Integer; Cookie: Longint): HResult; stdcall;
    { TOleControl }
    procedure CreateWnd; override;
    procedure DestroyWindowHandle; override;
    procedure GetProperty(Index: Integer; var Value: TVarData);
    procedure InitControlData; virtual;
    procedure InvokeMethod(const DispInfo; Result: Pointer);
    function  PaletteChanged(Foreground: Boolean): Boolean; override;
    procedure PictureChanged(Sender: TObject);
    procedure SetName(const Value: TComponentName); override;
    procedure SetParent(AParent: TWinControl); override;
    procedure WndProc(var Message: TMessage); override;
    property ControlData: PControlData read FControlData write FControlData;
    { IPropertyNotifySink }
    function OnChanged(dispid: TDispID): HResult; virtual; stdcall;
    function OnRequestEdit(dispid: TDispID): HResult; virtual; stdcall;
    function GetHelpContext(Member: string; var HelpCtx: Integer;
      var HelpFile: string): Boolean;
    property    TabStop default True;
  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    procedure   BrowseProperties;
    function    GetByteProp(Index: Integer): Byte;
    function    GetColorProp(Index: Integer): TColor;
    function    GetTColorProp(Index: Integer): TColor;
    function    GetCompProp(Index: Integer): Comp;
    function    GetCurrencyProp(Index: Integer): Currency;
    function    GetDoubleProp(Index: Integer): Double;
    function    GetIDispatchProp(Index: Integer): IDispatch;
    function    GetIntegerProp(Index: Integer): Integer;
    function    GetIUnknownProp(Index: Integer): IUnknown;
    function    GetWordBoolProp(Index: Integer): WordBool;
    function    GetTDateTimeProp(Index: Integer): TDateTime;
    function    GetTFontProp(Index: Integer): TFont;
    function    GetOleBoolProp(Index: Integer): TOleBool;
    function    GetOleDateProp(Index: Integer): TOleDate;
    function    GetOleEnumProp(Index: Integer): TOleEnum;
    function    GetTOleEnumProp(Index: Integer): TOleEnum;
    function    GetOleVariantProp(Index: Integer): OleVariant;
    function    GetTPictureProp(Index: Integer): TPicture;
    function    GetShortIntProp(Index: Integer): ShortInt;
    function    GetSingleProp(Index: Integer): Single;
    function    GetSmallintProp(Index: Integer): Smallint;
    function    GetStringProp(Index: Integer): string;
    function    GetVariantProp(Index: Integer): Variant;
    function    GetWideStringProp(Index: Integer): WideString;
    function    GetWordProp(Index: Integer): Word;
    procedure   DefaultHandler(var Message); override;
    procedure   DoObjectVerb(Verb: Integer);
    procedure   SetByteProp(Index: Integer; Value: Byte);
    procedure   SetColorProp(Index: Integer; Value: TColor);
    procedure   SetTColorProp(Index: Integer; Value: TColor);
    procedure   SetCompProp(Index: Integer; const Value: Comp);
    procedure   SetCurrencyProp(Index: Integer; const Value: Currency);
    procedure   SetDoubleProp(Index: Integer; const Value: Double);
    procedure   SetIDispatchProp(Index: Integer; const Value: IDispatch);
    procedure   SetIntegerProp(Index: Integer; Value: Integer);
    procedure   SetIUnknownProp(Index: Integer; const Value: IUnknown);
    procedure   SetWordBoolProp(Index: Integer; Value: WordBool);
    procedure   SetTDateTimeProp(Index: Integer; const Value: TDateTime);
    procedure   SetTFontProp(Index: Integer; Value: TFont);
    procedure   SetOleBoolProp(Index: Integer; Value: TOleBool);
    procedure   SetOleDateProp(Index: Integer; const Value: TOleDate);
    procedure   SetOleEnumProp(Index: Integer; Value: TOleEnum);
    procedure   SetTOleEnumProp(Index: Integer; Value: TOleEnum);
    procedure   SetOleVariantProp(Index: Integer; const Value: OleVariant);
    procedure   SetTPictureProp(Index: Integer;  Value: TPicture);
    procedure   SetProperty(Index: Integer; const Value: TVarData);
    procedure   SetShortIntProp(Index: Integer; Value: Shortint);
    procedure   SetSingleProp(Index: Integer; const Value: Single);
    procedure   SetSmallintProp(Index: Integer; Value: Smallint);
    procedure   SetStringProp(Index: Integer; const Value: string);
    procedure   SetVariantProp(Index: Integer; const Value: Variant);
    procedure   SetWideStringProp(Index: Integer; const Value: WideString);
    procedure   SetWordProp(Index: Integer; Value: Word);
    procedure   GetObjectVerbs(List: TStrings);
    function    GetPropDisplayString(DispID: Integer): string;
    procedure   GetPropDisplayStrings(DispID: Integer; List: TStrings);
    function    IsCustomProperty(DispID: Integer): Boolean;
    function    IsPropPageProperty(DispID: Integer): Boolean;
    procedure   SetBounds(ALeft, ATop, AWidth, AHeight: Integer); override;
    procedure   SetPropDisplayString(DispID: Integer; const Value: string);
    property    PerPropBrowsing: IPerPropertyBrowsing read FPropBrowsing;
    property    DefaultDispatch: IDispatch read GetDefaultDispatch;
  end;


function  FontToOleFont(Font: TFont): Variant;
procedure OleFontToFont(const OleFont: Variant; Font: TFont);
procedure Exchange(var A,B); register;
//function TrimPunctuation(const S: string): string;

implementation

uses OleConst;

const
  OCM_BASE = $2000;

{ Control flags }

const
  cfBackColor = $00000001;
  cfForeColor = $00000002;
  cfFont      = $00000004;
  cfEnabled   = $00000008;
  cfCaption   = $00000010;
  cfText      = $00000020;

const
  MaxDispArgs = 32;


{
function TrimPunctuation(const S: string): string;
var
  P: PChar;
begin
  Result := S;
  P := AnsiLastChar(Result);
  while (Length(Result) > 0) and (P^ in [#0..#32, '.']) do
  begin
    SetLength(Result, P - PChar(Result));
    P := AnsiLastChar(Result);
  end;
end;
}

type
  PDispInfo = ^TDispInfo;
  TDispInfo = packed record
    DispID: TDispID;
    ResType: Byte;
    CallDesc: TCallDesc;
  end;

  TArgKind = (akDWord, akSingle, akDouble);

  PEventArg = ^TEventArg;
  TEventArg = record
    Kind: TArgKind;
    Data: array[0..1] of Integer;
  end;

  TEventInfo = record
    Method: TMethod;
    Sender: TObject;
    ArgCount: Integer;
    Args: array[0..MaxDispArgs - 1] of TEventArg;
  end;


function StringToVarOleStr(const S: string): Variant;
begin
  VarClear(Result);
  TVarData(Result).VOleStr := StringToOleStr(S);
  TVarData(Result).VType := varOleStr;
end;

procedure Exchange(var A,B); register;
asm
  MOV   ECX, [EDX]
  XCHG  ECX, [EAX]
  MOV   [EDX], ECX
end;

{ TpsvAppletHost }

const
  cdForceSetClientSite = 1;
  cdDeferSetClientSite = 2;

constructor TXAppletHost.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  ControlStyle := ControlStyle + [csOpaque];
  FActive := False;
  FAutoExtractGUID := true;
  Width   := 100;
  Height  := 100;
  FInactiveColor   := clBlack;
  Color := FInactiveColor;
  FGUIDStr    := EmptyStr;
  FGUID       := GUID_NULL;
  FAutoResize := true;
end;

procedure TXAppletHost.Loaded;
begin
 inherited;
 if FActive then
  begin
    FActive := False;
    SetActive(True);
  end;
end;

function TXAppletHost.ExtractGUID : TGUID;
var TypeLib : ITypeLIB;
    I : Integer;
    TypeInfo : ActiveX.ITypeInfo;
    TypeAttr : PTypeAttr;
    TK : ActiveX.TTypeKIND;
begin
  Result := GUID_NULL;
  if FileName = EmptyStr then
    Exit;
  OleCheck(LoadTypeLib(PWideChar(WideString(FileName)), TypeLib));
  For I := 0 to TypeLib.GetTypeInfoCount -1 do
  begin
    TypeLib.GetTypeInfoType(I,TK);
    if TK = TKIND_COCLASS then
     begin
        TypeLib.GetTypeInfo(I,TypeInfo);
        TypeInfo.GetTypeAttr(TypeAttr);
        Result := TypeAttr.guid;
        TypeInfo.ReleaseTypeAttr(TypeAttr);
        Exit;
     end;
  end;
end;

function  TXAppletHost.GetApplet : TXApplet;
begin
  Result := nil;
  if (csDesigning in ComponentState) then
     Exit;
  if not Active then Exit;
  Result := FApplet;
end;

function TXAppletHost.GetDefaultDispatch : IDispatch;
begin
  Result := nil;
  if not Active then
    Exit;

  Result := FApplet.DefaultDispatch;
end;


function TXAppletHost.GetClientDispatch : IDispatch;
begin
  Result := nil;
  if not Active then
    Exit;

  Result := FApplet.FControlDispatch;
end;


Procedure  TXAppletHost.SetGUIDStr(Value : string);
begin
  If Value <> FGUIDStr then begin
     FGUIDStr := Value;
     FGUID := StringToGUID(FGUIDStr);
   end;
end;

procedure TXAppletHost.BrowseProperties;
begin
 if FActive then
   FApplet.BrowseProperties;
end;


procedure TXAppletHost.SetActive(Value : boolean);
begin

  if (FActive = Value) then
    Exit;


   if not FAutoExtractGUID then
    begin
      if (FGUIDStr = EmptyStr) then
        raise Exception.Create('GUID Unknown');
    end;

    if FileName = EmptyStr then
       raise Exception.Create('File name missing');

  FActive := Value;
  if not FActive then begin
    if Assigned(FBeforeDeactivate) then
       FBeforeDeactivate(Self);
    FApplet.Parent:= nil;
    FApplet.Free;
    FApplet := nil;
    Color := FInactiveColor;
    RecreateWnd;
    if Assigned(FAfterDeactivate) then
      FAfterDeactivate(Self);
  end
  else begin
    if not FileExists(FileName) then begin
      FActive := False;
      raise Exception.CreateFmt('File %s does not exist',[FileName]);
    end;

    if FGUIDStr = EmptyStr then begin
      if FAutoExtractGUID then begin
        FGUID := ExtractGUID;
        FGUIDStr := GUIDToString(FGUID);
      end;
    end;

    if not AppletRegistered then begin
      if FAutoRegister then
        RegisterApplet
      else
      begin
        //FActive := false;
        //raise Exception.Create('Applet not registered');
      end;
    end;

    if Assigned(FBeforeActivate) then
      FBeforeActivate(Self);
    FApplet := TXApplet.Create(Self);
    if FAutoResize then begin
      Width := FApplet.Width;
      Height := FApplet.Height;
    end;
    FApplet.Parent := Self;
    FApplet.Align := alClient;
    if Assigned(FAfterActivate) then
      FAfterActivate(Self);
  end;
end;

destructor TXAppletHost.Destroy;
begin
  if FActive then
    SetActive(False);
  inherited Destroy;
end;

function  TXAppletHost.AppletRegistered : boolean;
var Reg : TRegistry;
begin
  Reg := TRegistry.Create;
  Reg.RootKey := HKEY_CLASSES_ROOT;
  Result := Reg.OpenKey('CLSID\' + GuidToString(FGUID), False);
  Reg.CloseKey;
  Reg.Free;
end;

procedure TXAppletHost.SetInactiveColor(Value : TColor);
begin
  If FInactiveColor <> Value then
    begin
      FInactiveColor := Value;
      if not FActive then
        begin
          Color := FInactiveColor;
          RecreateWnd;
       end;
   end;
end;



procedure TXAppletHost.RegisterApplet;
var
  LibHandle: THandle;
  RegProc: TRegProc;
  Return: Integer;
begin
  IF FileName = EmptyStr then
    raise Exception.Create('Unknown file name');
  LibHandle := LoadLibrary(PChar(FileName));
  if LibHandle = 0 then raise Exception.CreateFmt('Failed to load "%s"', [FileName]);
  try
    @RegProc := GetProcAddress(LibHandle, ProcName);
    if @RegProc = Nil then
      raise Exception.CreateFmt('%s procedure not found in "%s"', [ProcName, FileName]);
    Return:= RegProc;
    if Return <> 0 then
      raise Exception.CreateFmt('Call to %s failed in "%s" returned %d', [ProcName, FileName, Return]);
  finally
    FreeLibrary(LibHandle);
  end;
end;

function TXAppletHost.GetTypeInfoCount(out Count: Integer): HResult;
begin
  Count := 0;
  Result := S_OK;
end;

function TXAppletHost.GetTypeInfo(Index, LocaleID: Integer;
  out TypeInfo): HResult;
begin
  Pointer(TypeInfo) := nil;
  Result := E_NOTIMPL;
end;

function TXAppletHost.GetIDsOfNames(const IID: TGUID; Names: Pointer;
  NameCount, LocaleID: Integer; DispIDs: Pointer): HResult;
begin
  Result := E_NOTIMPL;
end;

function TXAppletHost.Invoke(DispID: Integer; const IID: TGUID;
  LocaleID: Integer; Flags: Word; var Params;
  VarResult, ExcepInfo, ArgErr: Pointer): HResult;
var
  F: TFont;
begin
  if (Flags and DISPATCH_PROPERTYGET <> 0) and (VarResult <> nil) then
  begin
    Result := S_OK;
    case DispID of
      DISPID_AMBIENT_BACKCOLOR:
        PVariant(VarResult)^ := Color;
      DISPID_AMBIENT_DISPLAYNAME:
        PVariant(VarResult)^ := StringToVarOleStr(Name);
      DISPID_AMBIENT_FONT:
      begin
        if (Parent <> nil) and ParentFont then
          F := TXAppletHost(Parent).Font
        else
          F := Font;
        PVariant(VarResult)^ := FontToOleFont(F);
      end;
      DISPID_AMBIENT_FORECOLOR:
        PVariant(VarResult)^ := Font.Color;
      DISPID_AMBIENT_LOCALEID:
        PVariant(VarResult)^ := Integer(GetUserDefaultLCID);
      DISPID_AMBIENT_MESSAGEREFLECT:
        PVariant(VarResult)^ := True;
      DISPID_AMBIENT_USERMODE:
        PVariant(VarResult)^ := not (csDesigning in ComponentState);
      DISPID_AMBIENT_UIDEAD:
        PVariant(VarResult)^ := csDesigning in ComponentState;
      DISPID_AMBIENT_SHOWGRABHANDLES:
        PVariant(VarResult)^ := False;
      DISPID_AMBIENT_SHOWHATCHING:
        PVariant(VarResult)^ := False;
      DISPID_AMBIENT_SUPPORTSMNEMONICS:
        PVariant(VarResult)^ := True;
      DISPID_AMBIENT_AUTOCLIP:
        PVariant(VarResult)^ := True;
    else
      Result := DISP_E_MEMBERNOTFOUND;
    end;
  end else
    Result := DISP_E_MEMBERNOTFOUND;
end;


constructor TXApplet.Create(AOwner: TComponent);
var
  I: Integer;
begin
  inherited Create(AOwner);
  FHost := TXAppletHost(AOwner);
  Include(FComponentStyle, csCheckPropAvail);
  InitControlData;
  Inc(FControlData^.InstanceCount);

  if FControlData^.FontCount > 0 then begin
    FFonts := TList.Create;
    FFonts.Count := FControlData^.FontCount;
    for I := 0 to FFonts.Count-1 do
      FFonts[I] := TFont.Create;
  end;

  if FControlData^.PictureCount > 0 then begin
    FPictures := TList.Create;
    FPictures.Count := FControlData^.PictureCount;
    for I := 0 to FPictures.Count-1 do begin
      FPictures[I] := TPicture.Create;
      TPicture(FPictures[I]).OnChange := PictureChanged;
    end;
  end;

  CreateInstance;
  OleCheck(FOleObject.GetMiscStatus(DVASPECT_CONTENT, FMiscStatus));
  if (FControlData^.Reserved and cdDeferSetClientSite) = 0 then
    if ((FMiscStatus and OLEMISC_SETCLIENTSITEFIRST) <> 0) or
      ((FControlData^.Reserved and cdForceSetClientSite) <> 0) then
      OleCheck(FOleObject.SetClientSite(Self));

  OleCheck(FOleObject.QueryInterface(IPersistStreamInit, FPersistStream));

  if FMiscStatus and OLEMISC_INVISIBLEATRUNTIME <> 0 then
    Visible := False;

  if FMiscStatus and OLEMISC_SIMPLEFRAME <> 0 then
    ControlStyle := [csAcceptsControls, csDoubleClicks, csNoStdEvents]
  else
    ControlStyle := [csDoubleClicks, csNoStdEvents];

  TabStop := FMiscStatus and (OLEMISC_ACTSLIKELABEL or OLEMISC_NOUIACTIVATE) = 0;
  OleCheck(RequestNewObjectLayout);
end;

destructor TXApplet.Destroy;

  procedure FreeList(var L: TList);
  var
    I: Integer;
  begin
    if L <> nil then begin
      for I := 0 to L.Count-1 do
        TObject(L[I]).Free;
      L.Free;
      L := nil;
    end;
  end;

begin
  SetUIActive(False);
  if FOleObject <> nil then
    FOleObject.Close(OLECLOSE_NOSAVE);

  DestroyControl;

  FPersistStream := nil;
  if FOleObject <> nil then
    FOleObject.SetClientSite(nil);

  FOleObject := nil;
  FEventDispatch.Free;
  FreeList(FFonts);
  FreeList(FPictures);
  Dec(FControlData^.InstanceCount);
  if FControlData^.InstanceCount = 0 then
    DestroyEnumPropDescs;

  FreeMem(FControlData);
  inherited Destroy;
end;

procedure TXApplet.BrowseProperties;
begin
  DoObjectVerb(OLEIVERB_PROPERTIES);
end;

procedure TXApplet.CreateControl;
var
  Stream: IStream;
  CS: IOleClientSite;
  X: Integer;
begin
  if FOleControl = nil then
    try
      try  // work around ATL bug
        X := FOleObject.GetClientSite(CS);
      except
        X := -1;
      end;
      if (X <> 0) or (CS = nil) then
        OleCheck(FOleObject.SetClientSite(Self));
      if FObjectData = 0 then OleCheck(FPersistStream.InitNew) else
      begin
        OleCheck(CreateStreamOnHGlobal(FObjectData, False, Stream));
        OleCheck(FPersistStream.Load(Stream));
      end;
      OleCheck(FOleObject.QueryInterface(IOleControl, FOleControl));
      OleCheck(FOleObject.QueryInterface(IDispatch, FControlDispatch));
      FOleObject.QueryInterface(IPerPropertyBrowsing, FPropBrowsing);
      InterfaceConnect(FOleObject, IPropertyNotifySink,
        Self, FPropConnection);
      InterfaceConnect(FOleObject, FControlData^.EventIID,
        FEventDispatch, FEventsConnection);
      if FControlData^.Flags and cfBackColor <> 0 then
        OnChanged(DISPID_BACKCOLOR);
      if FControlData^.Flags and cfEnabled <> 0 then
        OnChanged(DISPID_ENABLED);
      if FControlData^.Flags and cfFont <> 0 then
        OnChanged(DISPID_FONT);
      if FControlData^.Flags and cfForeColor <> 0 then
        OnChanged(DISPID_FORECOLOR);
      FOleControl.OnAmbientPropertyChange(DISPID_UNKNOWN);
      RequestNewObjectLayout;
    except
      DestroyControl;
      raise;
    end;
end;


procedure TXApplet.CreateInstance;
var
  ClassFactory2: IClassFactory2;
  LicKeyStr: WideString;

  procedure LicenseCheck(Status: HResult; const Ident: string);
  begin
    if Status = CLASS_E_NOTLICENSED then
      raise EOleError.CreateFmt(Ident, [ClassName]);
    OleCheck(Status);
  end;

begin
  if not (csDesigning in ComponentState) and
    (FControlData^.LicenseKey <> nil) then
  begin
    OleCheck(CoGetClassObject(FControlData^.ClassID, CLSCTX_INPROC_SERVER or
      CLSCTX_LOCAL_SERVER, nil, IClassFactory2, ClassFactory2));
    LicKeyStr := PWideChar(FControlData^.LicenseKey);
    LicenseCheck(ClassFactory2.CreateInstanceLic(nil, nil, IOleObject,
      LicKeyStr, FOleObject), SInvalidLicense);
  end else
    LicenseCheck(CoCreateInstance(FControlData^.ClassID, nil,
      CLSCTX_INPROC_SERVER or CLSCTX_LOCAL_SERVER, IOleObject,
      FOleObject), SNotLicensed);
end;


procedure TXApplet.CreateWnd;
begin
  CreateControl;
  if FMiscStatus and OLEMISC_INVISIBLEATRUNTIME = 0 then
  begin
    FOleObject.DoVerb(OLEIVERB_INPLACEACTIVATE, nil, Self, 0,
      GetParentHandle, BoundsRect);
    if FOleInPlaceObject = nil then
      raise EOleError.CreateRes(@SCannotActivate);
    HookControlWndProc;
    if not Visible and IsWindowVisible(Handle) then
      ShowWindow(Handle, SW_HIDE);
  end else
    inherited CreateWnd;
end;

procedure TXApplet.DefaultHandler(var Message);
begin
  if HandleAllocated then
    with TMessage(Message) do
    begin
      if (Msg >= CN_BASE) and (Msg < CN_BASE + WM_USER) then
        Msg := Msg - (CN_BASE - OCM_BASE);
      if FMiscStatus and OLEMISC_SIMPLEFRAME = 0 then
      begin
        Result := CallWindowProc(DefWndProc, Handle, Msg, WParam, LParam);
        Exit;
      end;
    end;
  inherited DefaultHandler(Message);
end;


procedure TXApplet.DesignModified;
var
  Form: TCustomForm;
begin
  Form := GetParentForm(Self);
  if (Form <> nil) and (Form.Designer <> nil) then Form.Designer.Modified;
end;

procedure TXApplet.DestroyControl;
begin
  InterfaceDisconnect(FOleObject, FControlData^.EventIID, FEventsConnection);
  InterfaceDisconnect(FOleObject, IPropertyNotifySink, FPropConnection);
  FPropBrowsing := nil;
  FControlDispatch := nil;
  FOleControl := nil;
end;

procedure TXApplet.DestroyEnumPropDescs;
var
  I: Integer;
begin
  with FControlData^ do
    if EnumPropDescs <> nil then
    begin
      for I := 0 to EnumPropDescs.Count - 1 do
        TEnumPropDesc(EnumPropDescs[I]).Free;
      EnumPropDescs.Free;
      EnumPropDescs := nil;
    end;
end;


procedure TXApplet.DestroyWindowHandle;
begin
  if FMiscStatus and OLEMISC_INVISIBLEATRUNTIME = 0 then
  begin
    SetWindowLong(WindowHandle, GWL_WNDPROC, Longint(DefWndProc));
    if FOleObject <> nil then FOleObject.Close(OLECLOSE_NOSAVE);
    WindowHandle := 0;
  end else
    inherited DestroyWindowHandle;
end;

procedure TXApplet.DoObjectVerb(Verb: Integer);
var
  ActiveWindow: HWnd;
  WindowList: Pointer;
begin
  CreateControl;
  ActiveWindow := GetActiveWindow;
  WindowList := DisableTaskWindows(0);
  try
    OleCheck(FOleObject.DoVerb(Verb, nil, Self, 0,
      GetParentHandle, BoundsRect));
  finally
    EnableTaskWindows(WindowList);
    SetActiveWindow(ActiveWindow);
    Windows.SetFocus(ActiveWindow);
  end;
  if FPersistStream.IsDirty <> S_FALSE then DesignModified;
end;

function TXApplet.GetByteProp(Index: Integer): Byte;
begin
  Result := GetIntegerProp(Index);
end;

function TXApplet.GetColorProp(Index: Integer): TColor;
begin
  Result := GetIntegerProp(Index);
end;

function TXApplet.GetTColorProp(Index: Integer): TColor;
begin
  Result := GetIntegerProp(Index);
end;

function TXApplet.GetCompProp(Index: Integer): Comp;
begin
  Result := GetDoubleProp(Index);
end;

function TXApplet.GetCurrencyProp(Index: Integer): Currency;
var
  Temp: TVarData;
begin
  GetProperty(Index, Temp);
  Result := Temp.VCurrency;
end;

function TXApplet.GetDoubleProp(Index: Integer): Double;
var
  Temp: TVarData;
begin
  GetProperty(Index, Temp);
  Result := Temp.VDouble;
end;


{ TpsvApplet.GetHelpContext:  Fetch the help file name and help context
  id of the given member (property, event, or method) of the Ole Control from
  the control's ITypeInfo interfaces.  GetHelpContext returns False if
  the member name is not found in the control's ITypeInfo.
  To obtain a help context for the entire control class, pass an empty
  string as the Member name.  }

function TXApplet.GetHelpContext(Member: string;
  var HelpCtx: Integer; var HelpFile: string): Boolean;
var
  TypeInfo: ITypeInfo;
  HlpFile: TBStr;
  ImplTypes, MemberID: Integer;
  TypeAttr: PTypeAttr;

  function Find(const MemberStr: string; var TypeInfo: ITypeInfo): Boolean;
  var
    Code: HResult;
    I, Flags: Integer;
    RefType: HRefType;
    Name: TBStr;
    Temp: ITypeInfo;
  begin
    Result := False;
    Name := StringToOleStr(Member);
    try
      I := 0;
      while (I < ImplTypes) do
      begin
        OleCheck(TypeInfo.GetImplTypeFlags(I, Flags));
        if Flags and (IMPLTYPEFLAG_FDEFAULT or IMPLTYPEFLAG_FSOURCE) <> 0 then
        begin
          OleCheck(TypeInfo.GetRefTypeOfImplType(I, RefType));
          OleCheck(TypeInfo.GetRefTypeInfo(RefType, Temp));
          Code := Temp.GetIDsOfNames(@Name, 1, @MemberID);
          if Code <> DISP_E_UNKNOWNNAME then
          begin
            OleCheck(Code);
            Exchange(TypeInfo, Temp);
            Result := True;
            Break;
          end;
        end;
        Inc(I);
      end;
    finally
      SysFreeString(Name);
    end;
  end;

begin
  HelpCtx := 0;
  HelpFile := '';
  CreateControl;
  OleCheck((FOleObject as IProvideClassInfo).GetClassInfo(TypeInfo));
  MemberID := MEMBERID_NIL;
  if Length(Member) > 0 then
  begin
    OleCheck(TypeInfo.GetTypeAttr(TypeAttr));
    ImplTypes := TypeAttr.cImplTypes;
    TypeInfo.ReleaseTypeAttr(TypeAttr);
    Result := Find(Member, TypeInfo);
    if (not Result) and (Member[Length(Member)] = '_') then
    begin
      Delete(Member, Length(Member)-1, 1);
      Result := Find(Member, TypeInfo);
    end;
    if (not Result) and (Pos('On', Member) = 1) then
    begin
      Delete(Member, 1, 2);
      Result := Find(Member, TypeInfo);
    end;
    if not Result then Exit;
  end;
  OleCheck(TypeInfo.GetDocumentation(MemberID, nil, nil, @HelpCtx, @HlpFile));
  HelpFile := OleStrToString(HlpFile);
  SysFreeString(HlpFile);
  Result := True;
end;

function TXApplet.GetIDispatchProp(Index: Integer): IDispatch;
var
  Temp: TVarData;
begin
  GetProperty(Index, Temp);
  Result := IDispatch(Temp.VDispatch);
end;

function TXApplet.GetIntegerProp(Index: Integer): Integer;
var
  Temp: TVarData;
begin
  GetProperty(Index, Temp);
  Result := Temp.VInteger;
end;

function TXApplet.GetIUnknownProp(Index: Integer): IUnknown;
var
  Temp: TVarData;
begin
  GetProperty(Index, Temp);
  Result := IUnknown(Temp.VUnknown);
end;

function TXApplet.GetMainMenu: TMainMenu;
var
  Form: TCustomForm;
begin
  Result := nil;
  Form := GetParentForm(Self);
  if Form <> nil then
    if (Form is TForm) and (TForm(Form).FormStyle <> fsMDIChild) then
      Result := Form.Menu
    else
      if Application.MainForm <> nil then
        Result := Application.MainForm.Menu;
end;

procedure TXApplet.GetObjectVerbs(List: TStrings);
var
  EnumOleVerb: IEnumOleVerb;
  OleVerb: TOleVerb;
  Code: HResult;
begin
  CreateControl;
  List.Clear;
  Code := FOleObject.EnumVerbs(EnumOleVerb);
  if Code = OLE_S_USEREG then
    Code := OleRegEnumVerbs(FControlData.ClassID, EnumOleVerb);
  if Code = 0 then
    while (EnumOleVerb.Next(1, OleVerb, nil) = 0) do
      if (OleVerb.grfAttribs and OLEVERBATTRIB_ONCONTAINERMENU <> 0) then
      begin
        List.AddObject(StripHotkey(OleVerb.lpszVerbName), TObject(OleVerb.lVerb));
      end;
end;

function TXApplet.GetWordBoolProp(Index: Integer): WordBool;
var
  Temp: TVarData;
begin
  GetProperty(Index, Temp);
  Result := Temp.VBoolean;
end;

function TXApplet.GetTDateTimeProp(Index: Integer): TDateTime;
var
  Temp: TVarData;
begin
  GetProperty(Index, Temp);
  Result := Temp.VDate;
end;

function TXApplet.GetTFontProp(Index: Integer): TFont;
var
  I: Integer;
begin
  Result := nil;
  for I := 0 to FFonts.Count-1 do
    if FControlData^.FontIDs^[I] = Index then
    begin
      Result := TFont(FFonts[I]);
      if Result.FontAdapter = nil then
        SetOleFont(Result, GetIDispatchProp(Index) as IFontDisp);
    end;
end;

function TXApplet.GetOleBoolProp(Index: Integer): TOleBool;
var
  Temp: TVarData;
begin
  GetProperty(Index, Temp);
  Result := Temp.VBoolean;
end;

function TXApplet.GetOleDateProp(Index: Integer): TOleDate;
var
  Temp: TVarData;
begin
  GetProperty(Index, Temp);
  Result := Temp.VDate;
end;

function TXApplet.GetOleEnumProp(Index: Integer): TOleEnum;
begin
  Result := GetIntegerProp(Index);
end;

function TXApplet.GetTOleEnumProp(Index: Integer): TOleEnum;
begin
  Result := GetIntegerProp(Index);
end;


function TXApplet.GetDefaultDispatch: IDispatch;
begin
  CreateControl;
  Result := FOleObject as IDispatch;
end;

function TXApplet.GetOleVariantProp(Index: Integer): OleVariant;
begin
  VarClear(Result);
  GetProperty(Index, TVarData(Result));
end;

function TXApplet.GetTPictureProp(Index: Integer): TPicture;
var
  I: Integer;
begin
  Result := nil;
  for I := 0 to FPictures.Count-1 do
    if FControlData^.PictureIDs^[I] = Index then
    begin
      Result := TPicture(FPictures[I]);
      if Result.PictureAdapter = nil then
        SetOlePicture(Result, GetIDispatchProp(Index) as IPictureDisp);
    end;
end;


function TXApplet.GetPropDisplayString(DispID: Integer): string;
var
  S: WideString;
begin
  CreateControl;
  if (FPropBrowsing <> nil) and
    (FPropBrowsing.GetDisplayString(DispID, S) = 0) then
    Result := S else
    Result := GetStringProp(DispID);
end;

procedure TXApplet.GetPropDisplayStrings(DispID: Integer; List: TStrings);
var
  Strings: TCAPOleStr;
  Cookies: TCALongint;
  I: Integer;
begin
  CreateControl;
  List.Clear;
  if (FPropBrowsing <> nil) and
    (FPropBrowsing.GetPredefinedStrings(DispID, Strings, Cookies) = 0) then
    try
      for I := 0 to Strings.cElems - 1 do
        List.AddObject(Strings.pElems^[I], TObject(Cookies.pElems^[I]));
    finally
      for I := 0 to Strings.cElems - 1 do
        CoTaskMemFree(Strings.pElems^[I]);
      CoTaskMemFree(Strings.pElems);
      CoTaskMemFree(Cookies.pElems);
    end;
end;

var  // init to zero, never written to
  DispParams: TDispParams = ();

procedure TXApplet.GetProperty(Index: Integer; var Value: TVarData);
var
  Status: HResult;
  ExcepInfo: TExcepInfo;
begin
  CreateControl;
  Value.VType := varEmpty;
  Status := FControlDispatch.Invoke(Index, GUID_NULL, 0,
    DISPATCH_PROPERTYGET, DispParams, @Value, @ExcepInfo, nil);
  if Status <> 0 then DispatchInvokeError(Status, ExcepInfo);
end;

function TXApplet.GetShortIntProp(Index: Integer): ShortInt;
begin
  Result := GetIntegerProp(Index);
end;

function TXApplet.GetSingleProp(Index: Integer): Single;
var
  Temp: TVarData;
begin
  GetProperty(Index, Temp);
  Result := Temp.VSingle;
end;

function TXApplet.GetSmallintProp(Index: Integer): Smallint;
var
  Temp: TVarData;
begin
  GetProperty(Index, Temp);
  Result := Temp.VSmallint;
end;

function TXApplet.GetStringProp(Index: Integer): string;
begin
  Result := GetVariantProp(Index);
end;

function TXApplet.GetVariantProp(Index: Integer): Variant;
begin
  Result := GetOleVariantProp(Index);
end;

function TXApplet.GetWideStringProp(Index: Integer): WideString;
var
  Temp: TVarData;
begin
  Result := '';
  GetProperty(Index, Temp);
  Pointer(Result) := Temp.VOleStr;
end;

function TXApplet.GetWordProp(Index: Integer): Word;
begin
  Result := GetIntegerProp(Index);
end;

procedure TXApplet.HookControlWndProc;
var
  WndHandle: HWnd;
begin
  if (FOleInPlaceObject <> nil) and (WindowHandle = 0) then
  begin
    WndHandle := 0;
    FOleInPlaceObject.GetWindow(WndHandle);
    if WndHandle = 0 then raise EOleError.CreateRes(@SNoWindowHandle);
    WindowHandle := WndHandle;
    DefWndProc := Pointer(GetWindowLong(WindowHandle, GWL_WNDPROC));
    CreationControl := Self;
    SetWindowLong(WindowHandle, GWL_WNDPROC, Longint(@InitWndProc));
    SendMessage(WindowHandle, WM_NULL, 0, 0);
  end;
end;


type
  PVarArg = ^TVarArg;
  TVarArg = array[0..3] of DWORD;



procedure GetStringResult(BStr: TBStr; var Result: string);
begin
  try
    OleStrToStrVar(BStr, Result);
  finally
    SysFreeString(BStr);
  end;
end;


procedure TXApplet.InvokeMethod(const DispInfo; Result: Pointer); assembler;
asm
        PUSH    EBX
        PUSH    ESI
        PUSH    EDI
        MOV     EBX,EAX
        MOV     ESI,EDX
        MOV     EDI,ECX
        CALL    TXApplet.CreateControl
        PUSH    [ESI].TDispInfo.DispID
        MOV     ECX,ESP
        XOR     EAX,EAX
        PUSH    EAX
        PUSH    EAX
        PUSH    EAX
        PUSH    EAX
        MOV     EDX,ESP
        LEA     EAX,[EBP+16]
        CMP     [ESI].TDispInfo.ResType,varOleStr
        JE      @@1
        CMP     [ESI].TDispInfo.ResType,varVariant
        JE      @@1
        LEA     EAX,[EBP+12]
@@1:    PUSH    EAX
        PUSH    EDX
        LEA     EDX,[ESI].TDispInfo.CallDesc
        MOV     EAX,[EBX].TXApplet.FControlDispatch
        CALL    DispatchInvoke
        XOR     EAX,EAX
        MOV     AL,[ESI].TDispInfo.ResType
        JMP     @ResultTable.Pointer[EAX*4]

@ResultTable:
        DD      @ResEmpty
        DD      @ResNull
        DD      @ResSmallint
        DD      @ResInteger
        DD      @ResSingle
        DD      @ResDouble
        DD      @ResCurrency
        DD      @ResDate
        DD      @ResString
        DD      @ResDispatch
        DD      @ResError
        DD      @ResBoolean
        DD      @ResVariant

@ResSmallint:
@ResBoolean:
        MOV     AX,[ESP+8]
        MOV     [EDI],AX
        JMP     @ResDone

@ResString:
        MOV     EAX,[ESP+8]
        MOV     EDX,EDI
        CALL    GetStringResult
        JMP     @ResDone

@ResVariant:
        MOV     EAX,EDI
        CALL    System.@VarClear
        MOV     ESI,ESP
        MOV     ECX,4
        REP     MOVSD
        JMP     @ResDone

@ResDouble:
@ResCurrency:
@ResDate:
        MOV     EAX,[ESP+12]
        MOV     [EDI+4],EAX

@ResInteger:
@ResSingle:
        MOV     EAX,[ESP+8]
        MOV     [EDI],EAX

@ResEmpty:
@ResNull:
@ResDispatch:
@ResError:
@ResDone:
        ADD     ESP,20
        POP     EDI
        POP     ESI
        POP     EBX
end;

function TXApplet.IsCustomProperty(DispID: Integer): Boolean;
var
  W: WideString;
begin
  Result := (FPropBrowsing <> nil) and
    (FPropBrowsing.GetDisplayString(DispID, W) = 0);
end;

function TXApplet.IsPropPageProperty(DispID: Integer): Boolean;
var
  PPID: TCLSID;
begin
  Result := (FPropBrowsing <> nil) and
    (FPropBrowsing.MapPropertyToPage(DispID, PPID) = S_FALSE) and not
    IsEqualCLSID(PPID, GUID_NULL);
end;

function TXApplet.PaletteChanged(Foreground: Boolean): Boolean;
begin
  Result := False;
  if HandleAllocated and Foreground then
    Result := CallWindowProc(DefWndProc, Handle, WM_QUERYNEWPALETTE, 0, 0) <> 0;
  if not Result then
    Result := inherited PaletteChanged(Foreground);
end;

procedure TXApplet.PictureChanged(Sender: TObject);
var
  I: Integer;
begin
  if (FPictures = nil) or not (Sender is TPicture) then Exit;
  for I := 0 to FPictures.Count - 1 do
    if FPictures[I] = Sender then
    begin
      if (TPicture(Sender).PictureAdapter <> nil) then
        SetTPictureProp(FControlData.PictureIDs^[I], TPicture(Sender));
      Exit;
    end;
end;


procedure TXApplet.SetBounds(ALeft, ATop, AWidth, AHeight: Integer);
begin
  if ((AWidth <> Width) and (Width > 0)) or ((AHeight <> Height) and (Height > 0)) then
    if (FMiscStatus and OLEMISC_INVISIBLEATRUNTIME <> 0) or
      ((FOleObject.SetExtent(DVASPECT_CONTENT, Point(
        MulDiv(AWidth, 2540, Screen.PixelsPerInch),
        MulDiv(AHeight, 2540, Screen.PixelsPerInch))) <> S_OK)) then
    begin
      AWidth := Width;
      AHeight := Height;
    end;
  inherited SetBounds(ALeft, ATop, AWidth, AHeight);
end;

procedure TXApplet.SetByteProp(Index: Integer; Value: Byte);
begin
  SetIntegerProp(Index, Value);
end;

procedure TXApplet.SetColorProp(Index: Integer; Value: TColor);
begin
  SetIntegerProp(Index, Value);
end;

procedure TXApplet.SetTColorProp(Index: Integer; Value: TColor);
begin
  SetIntegerProp(Index, Value);
end;

procedure TXApplet.SetCompProp(Index: Integer; const Value: Comp);
var
  Temp: TVarData;
begin
  Temp.VType := VT_I8;
  Temp.VDouble := Value;
  SetProperty(Index, Temp);
end;

procedure TXApplet.SetCurrencyProp(Index: Integer; const Value: Currency);
var
  Temp: TVarData;
begin
  Temp.VType := varCurrency;
  Temp.VCurrency := Value;
  SetProperty(Index, Temp);
end;

procedure TXApplet.SetDoubleProp(Index: Integer; const Value: Double);
var
  Temp: TVarData;
begin
  Temp.VType := varDouble;
  Temp.VDouble := Value;
  SetProperty(Index, Temp);
end;

procedure TXApplet.SetIDispatchProp(Index: Integer; const Value: IDispatch);
var
  Temp: TVarData;
begin
  Temp.VType := varDispatch;
  Temp.VDispatch := Pointer(Value);
  SetProperty(Index, Temp);
end;

procedure TXApplet.SetIntegerProp(Index: Integer; Value: Integer);
var
  Temp: TVarData;
begin
  Temp.VType := varInteger;
  Temp.VInteger := Value;
  SetProperty(Index, Temp);
end;

procedure TXApplet.SetIUnknownProp(Index: Integer; const Value: IUnknown);
var
  Temp: TVarData;
begin
  Temp.VType := VT_UNKNOWN;
  Temp.VUnknown := Pointer(Value);
  SetProperty(Index, Temp);
end;

procedure TXApplet.SetName(const Value: TComponentName);
var
  OldName: string;
  DispID: Integer;
begin
  OldName := Name;
  inherited SetName(Value);
  if FOleControl <> nil then
  begin
    FOleControl.OnAmbientPropertyChange(DISPID_AMBIENT_DISPLAYNAME);
    if FControlData^.Flags and (cfCaption or cfText) <> 0 then
    begin
      if FControlData^.Flags and cfCaption <> 0 then
        DispID := DISPID_CAPTION else
        DispID := DISPID_TEXT;
      if OldName = GetStringProp(DispID) then SetStringProp(DispID, Value);
    end;
  end;
end;

procedure TXApplet.SetWordBoolProp(Index: Integer; Value: WordBool);
var
  Temp: TVarData;
begin
  Temp.VType := varBoolean;
  if Value then
    Temp.VBoolean := WordBool(-1) else
    Temp.VBoolean := WordBool(0);
  SetProperty(Index, Temp);
end;

procedure TXApplet.SetTDateTimeProp(Index: Integer; const Value: TDateTime);
var
  Temp: TVarData;
begin
  Temp.VType := varDate;
  Temp.VDate := Value;
  SetProperty(Index, Temp);
end;

procedure TXApplet.SetTFontProp(Index: Integer; Value: TFont);
var
  I: Integer;
  F: TFont;
  Temp: IFontDisp;
begin
  for I := 0 to FFonts.Count-1 do
    if FControlData^.FontIDs^[I] = Index then
    begin
      F := TFont(FFonts[I]);
      F.Assign(Value);
      if F.FontAdapter = nil then
      begin
        GetOleFont(F, Temp);
        SetIDispatchProp(Index, Temp);
      end;
    end;
end;

procedure TXApplet.SetOleBoolProp(Index: Integer; Value: TOleBool);
var
  Temp: TVarData;
begin
  Temp.VType := varBoolean;
  if Value then
    Temp.VBoolean := WordBool(-1) else
    Temp.VBoolean := WordBool(0);
  SetProperty(Index, Temp);
end;

procedure TXApplet.SetOleDateProp(Index: Integer; const Value: TOleDate);
var
  Temp: TVarData;
begin
  Temp.VType := varDate;
  Temp.VDate := Value;
  SetProperty(Index, Temp);
end;

procedure TXApplet.SetOleEnumProp(Index: Integer; Value: TOleEnum);
begin
  SetIntegerProp(Index, Value);
end;

procedure TXApplet.SetTOleEnumProp(Index: Integer; Value: TOleEnum);
begin
  SetIntegerProp(Index, Value);
end;

procedure TXApplet.SetOleVariantProp(Index: Integer; const Value: OleVariant);
begin
  SetProperty(Index, TVarData(Value));
end;

procedure TXApplet.SetParent(AParent: TWinControl);
var
  CS: IOleClientSite;
  X: Integer;
begin
  inherited SetParent(AParent);
  if (AParent <> nil) then
  begin
    try  // work around ATL bug
      X := FOleObject.GetClientSite(CS);
    except
      X := -1;
    end;
    if (X <> 0) or (CS = nil) then
      OleCheck(FOleObject.SetClientSite(Self));
    if FOleControl <> nil then
      FOleControl.OnAmbientPropertyChange(DISPID_UNKNOWN);
  end;
end;

procedure TXApplet.SetTPictureProp(Index: Integer; Value: TPicture);
var
  I: Integer;
  P: TPicture;
  Temp: IPictureDisp;
begin
  if FUpdatingPictures then Exit;
  FUpdatingPictures := True;
  try
    for I := 0 to FPictures.Count-1 do
      if FControlData^.PictureIDs^[I] = Index then
      begin
        P := TPicture(FPictures[I]);
        P.Assign(Value);
        GetOlePicture(P, Temp);
        SetIDispatchProp(Index, Temp);
      end;
  finally
    FUpdatingPictures := False;
  end;
end;

procedure TXApplet.SetPropDisplayString(DispID: Integer;
  const Value: string);
var
  I: Integer;
  Values: TStringList;
  V: OleVariant;
begin
  Values := TStringList.Create;
  try
    GetPropDisplayStrings(DispID, Values);
    for I := 0 to Values.Count - 1 do
      if AnsiCompareText(Value, Values[I]) = 0 then
      begin
        OleCheck(FPropBrowsing.GetPredefinedValue(DispID,
          Integer(Values.Objects[I]), V));
        SetProperty(DispID, TVarData(V));
        Exit;
      end;
  finally
    Values.Free;
  end;
  SetStringProp(DispID, Value);
end;

procedure TXApplet.SetProperty(Index: Integer; const Value: TVarData);
const
  DispIDArgs: Longint = DISPID_PROPERTYPUT;
var
  Status, InvKind: Integer;
  DispParams: TDispParams;
  ExcepInfo: TExcepInfo;
begin
  CreateControl;
  DispParams.rgvarg := @Value;
  DispParams.rgdispidNamedArgs := @DispIDArgs;
  DispParams.cArgs := 1;
  DispParams.cNamedArgs := 1;
  if Value.VType <> varDispatch then
    InvKind := DISPATCH_PROPERTYPUT else
    InvKind := DISPATCH_PROPERTYPUTREF;
  Status := FControlDispatch.Invoke(Index, GUID_NULL, 0,
    InvKind, DispParams, nil, @ExcepInfo, nil);
  if Status <> 0 then DispatchInvokeError(Status, ExcepInfo);
end;

procedure TXApplet.SetShortintProp(Index: Integer; Value: ShortInt);
begin
  SetIntegerProp(Index, Value);
end;

procedure TXApplet.SetSingleProp(Index: Integer; const Value: Single);
var
  Temp: TVarData;
begin
  Temp.VType := varSingle;
  Temp.VSingle := Value;
  SetProperty(Index, Temp);
end;

procedure TXApplet.SetSmallintProp(Index: Integer; Value: Smallint);
var
  Temp: TVarData;
begin
  Temp.VType := varSmallint;
  Temp.VSmallint := Value;
  SetProperty(Index, Temp);
end;

procedure TXApplet.SetStringProp(Index: Integer; const Value: string);
var
  Temp: TVarData;
begin
  Temp.VType := varOleStr;
  Temp.VOleStr := StringToOleStr(Value);
  try
    SetProperty(Index, Temp);
  finally
    SysFreeString(Temp.VOleStr);
  end;
end;

procedure TXApplet.SetUIActive(Active: Boolean);
var
  Form: TCustomForm;
begin
  Form := GetParentForm(Self);
  if Form <> nil then
    if Active then
    begin
      if (Form.ActiveOleControl <> nil) and
        (Form.ActiveOleControl <> Self) then
        Form.ActiveOleControl.Perform(CM_UIDEACTIVATE, 0, 0);
      Form.ActiveOleControl := Self;
    end else
      if Form.ActiveOleControl = Self then Form.ActiveOleControl := nil;
end;

procedure TXApplet.SetVariantProp(Index: Integer; const Value: Variant);
begin
  SetOleVariantProp(Index, Value);
end;

procedure TXApplet.SetWideStringProp(Index: Integer; const Value: WideString);
var
  Temp: TVarData;
begin
  Temp.VType := varOleStr;
  if Value <> '' then
    Temp.VOleStr := PWideChar(Value)
  else
    Temp.VOleStr := nil;
  SetProperty(Index, Temp);
end;

procedure TXApplet.SetWordProp(Index: Integer; Value: Word);
begin
  SetIntegerProp(Index, Value);
end;


procedure TXApplet.WndProc(var Message: TMessage);
var
  WinMsg: TMsg;
begin
  if (Message.Msg >= CN_BASE + WM_KEYFIRST) and
    (Message.Msg <= CN_BASE + WM_KEYLAST) and
    (FOleInPlaceActiveObject <> nil) then
  begin
    WinMsg.HWnd := Handle;
    WinMsg.Message := Message.Msg - CN_BASE;
    WinMsg.WParam := Message.WParam;
    WinMsg.LParam := Message.LParam;
    WinMsg.Time := GetMessageTime;
    WinMsg.Pt.X := $115DE1F1;
    WinMsg.Pt.Y := $115DE1F1;
    if FOleInPlaceActiveObject.TranslateAccelerator(WinMsg) = S_OK then
    begin
      Message.Result := 1;
      Exit;
    end;
  end;
  case TMessage(Message).Msg of
    CM_PARENTFONTCHANGED:
      if ParentFont and (FOleControl <> nil) then
      begin
        FOleControl.OnAmbientPropertyChange(DISPID_AMBIENT_FONT);
        FOleControl.OnAmbientPropertyChange(DISPID_AMBIENT_FORECOLOR);
      end;
    CM_PARENTCOLORCHANGED:
      if ParentColor and (FOleControl <> nil) then
        FOleControl.OnAmbientPropertyChange(DISPID_AMBIENT_BACKCOLOR);
  end;
  inherited WndProc(Message);
end;


procedure TXApplet.WMEraseBkgnd(var Message: TWMEraseBkgnd);
begin
  if FMiscStatus and OLEMISC_INVISIBLEATRUNTIME = 0 then
    DefaultHandler(Message) else
    inherited;
end;

procedure TXApplet.WMPaint(var Message: TWMPaint);
var
  DC: HDC;
  PS: TPaintStruct;
begin
  if FMiscStatus and OLEMISC_INVISIBLEATRUNTIME <> 0 then
  begin
    DC := Message.DC;
    if DC = 0 then DC := BeginPaint(Handle, PS);
    OleDraw(FOleObject, DVASPECT_CONTENT, DC, ClientRect);
    if Message.DC = 0 then EndPaint(Handle, PS);
  end else
    inherited;
end;

procedure TXApplet.CMDocWindowActivate(var Message: TMessage);
var
  Form: TCustomForm;
  F: TForm;
begin
  Form := GetParentForm(Self);
  F := nil;
  if Form is TForm then F := TForm(Form);
  if (F <> nil) {and (F.FormStyle = fsMDIChild)} then
  begin
    FOleInPlaceActiveObject.OnDocWindowActivate(LongBool(Message.WParam));
    if Message.WParam = 0 then SetMenu(0, 0, 0);
  end;
end;

procedure TXApplet.CMColorChanged(var Message: TMessage);
begin
  inherited;
  if (FControlData^.Flags and cfBackColor <> 0) and not FUpdatingColor and
    HandleAllocated then
  begin
    FUpdatingColor := True;
    try
      SetColorProp(DISPID_BACKCOLOR, Color);
    finally
      FUpdatingColor := False;
    end;
  end;
end;

procedure TXApplet.CMEnabledChanged(var Message: TMessage);
begin
  inherited;
  if (FControlData^.Flags and cfEnabled <> 0) and not FUpdatingEnabled and
    HandleAllocated then
  begin
    FUpdatingEnabled := True;
    try
      SetWordBoolProp(DISPID_ENABLED, Enabled);
    finally
      FUpdatingEnabled := False;
    end;
  end;
end;

procedure TXApplet.CMFontChanged(var Message: TMessage);
begin
  inherited;
  if (FControlData^.Flags and (cfForeColor or cfFont) <> 0) and
    not FUpdatingFont and HandleAllocated then
  begin
    FUpdatingFont := True;
    try
      if FControlData^.Flags and cfForeColor <> 0 then
        SetIntegerProp(DISPID_FORECOLOR, Font.Color);
      if FControlData^.Flags and cfFont <> 0 then
        SetVariantProp(DISPID_FONT, FontToOleFont(Font));
    finally
      FUpdatingFont := False;
    end;
  end;
end;

procedure TXApplet.CMDialogKey(var Message: TMessage);
var
  Info: TControlInfo;
  Msg: TMsg;
  Cmd: Word;
begin
  if CanFocus then
  begin
    Info.cb := SizeOf(Info);
    if (FOleControl.GetControlInfo(Info) = S_OK) and (Info.cAccel <> 0) then
    begin
      FillChar(Msg, SizeOf(Msg), 0);
      Msg.hwnd := Handle;
      Msg.message := WM_KEYDOWN;
      Msg.wParam := Message.WParam;
      Msg.lParam := Message.LParam;
      if IsAccelerator(Info.hAccel, Info.cAccel, @Msg, Cmd) then
      begin
        FOleControl.OnMnemonic(@Msg);
        Message.Result := 1;
        Exit;
      end;
    end;
  end;
  inherited;
end;

procedure TXApplet.CMUIActivate(var Message: TMessage);
var
  F: TCustomForm;
begin
  F := GetParentForm(Self);
  if (F = nil) or (F.ActiveOleControl <> Self) then
    FOleObject.DoVerb(OLEIVERB_UIACTIVATE, nil, Self, 0,
      GetParentHandle, BoundsRect);
end;

procedure TXApplet.CMUIDeactivate(var Message: TMessage);
var
  F: TCustomForm;
begin
  F := GetParentForm(Self);
  if (F = nil) or (F.ActiveOleControl = Self) then
  begin
    if FOleInPlaceObject <> nil then FOleInPlaceObject.UIDeactivate;
    if (F <> nil) and (F.ActiveControl = Self) then OnUIDeactivate(False);
  end;
end;

{ TpsvApplet.IUnknown }

function TXApplet.QueryInterface(const IID: TGUID; out Obj): HResult;
begin
  if GetInterface(IID, Obj) then Result := S_OK else Result := E_NOINTERFACE;
end;

function TXApplet._AddRef: Integer;
begin
  Inc(FRefCount);
  Result := FRefCount;
end;

function TXApplet._Release: Integer;
begin
  Dec(FRefCount);
  Result := FRefCount;
end;

{ TpsvApplet.IOleClientSite }

function TXApplet.SaveObject: HResult;
begin
  Result := S_OK;
end;

function TXApplet.GetMoniker(dwAssign: Longint; dwWhichMoniker: Longint;
  out mk: IMoniker): HResult;
begin
  Result := E_NOTIMPL;
end;

function TXApplet.GetContainer(out container: IOleContainer): HResult;
begin
  Result := E_NOINTERFACE;
end;

function TXApplet.ShowObject: HResult;
begin
  HookControlWndProc;
  Result := S_OK;
end;

function TXApplet.OnShowWindow(fShow: BOOL): HResult;
begin
  Result := S_OK;
end;

function TXApplet.RequestNewObjectLayout: HResult;
var
  Extent: TPoint;
  W, H: Integer;
begin
  Result := FOleObject.GetExtent(DVASPECT_CONTENT, Extent);
  if Result <> S_OK then Exit;
  W := MulDiv(Extent.X, Screen.PixelsPerInch, 2540);
  H := MulDiv(Extent.Y, Screen.PixelsPerInch, 2540);
  if (FMiscStatus and OLEMISC_INVISIBLEATRUNTIME <> 0) and (FOleControl = nil) then
  begin
    if W > 32 then W := 32;
    if H > 32 then H := 32;
  end;
  SetBounds(Left, Top, W, H);
end;

{ TpsvApplet.IOleControlSite }

function TXApplet.OnControlInfoChanged: HResult;
begin
  Result := E_NOTIMPL;
end;

function TXApplet.LockInPlaceActive(fLock: BOOL): HResult;
begin
  Result := E_NOTIMPL;
end;

function TXApplet.GetExtendedControl(out disp: IDispatch): HResult;
begin
  Result := E_NOTIMPL;
end;

function TXApplet.TransformCoords(var ptlHimetric: TPoint;
  var ptfContainer: TPointF; flags: Longint): HResult;
begin
  if flags and XFORMCOORDS_HIMETRICTOCONTAINER <> 0 then
  begin
    ptfContainer.X := MulDiv(ptlHimetric.X, Screen.PixelsPerInch, 2540);
    ptfContainer.Y := MulDiv(ptlHimetric.Y, Screen.PixelsPerInch, 2540);
  end else
  begin
    ptlHimetric.X := Integer(Round(ptfContainer.X * 2540 / Screen.PixelsPerInch));
    ptlHimetric.Y := Integer(Round(ptfContainer.Y * 2540 / Screen.PixelsPerInch));
  end;
  Result := S_OK;
end;

function TXApplet.OleControlSite_TranslateAccelerator(
  msg: PMsg; grfModifiers: Longint): HResult;
begin
  Result := E_NOTIMPL;
end;

function TXApplet.OnFocus(fGotFocus: BOOL): HResult;
begin
  Result := E_NOTIMPL;
end;

function TXApplet.ShowPropertyFrame: HResult;
begin
  Result := E_NOTIMPL;
end;

{ TpsvApplet.IOleWindow }

function TXApplet.ContextSensitiveHelp(fEnterMode: BOOL): HResult;
begin
  Result := S_OK;
end;

{ TpsvApplet.IOleInPlaceSite }

function TXApplet.OleInPlaceSite_GetWindow(out wnd: HWnd): HResult;
begin
  Result := S_OK;
  wnd := GetParentHandle;
  if wnd = 0 then Result := E_FAIL;
end;

function TXApplet.CanInPlaceActivate: HResult;
begin
  Result := S_OK;
end;

function TXApplet.OnInPlaceActivate: HResult;
begin
  FOleObject.QueryInterface(IOleInPlaceObject, FOleInPlaceObject);
  FOleObject.QueryInterface(IOleInPlaceActiveObject, FOleInPlaceActiveObject);
  Result := S_OK;
end;

function TXApplet.OnUIActivate: HResult;
begin
  SetUIActive(True);
  Result := S_OK;
end;

function TXApplet.GetWindowContext(out frame: IOleInPlaceFrame;
  out doc: IOleInPlaceUIWindow; out rcPosRect: TRect;
  out rcClipRect: TRect; out frameInfo: TOleInPlaceFrameInfo): HResult;
begin
  frame := Self;
  doc := nil;
  rcPosRect := BoundsRect;
  SetRect(rcClipRect, 0, 0, 32767, 32767);
  with frameInfo do
  begin
    fMDIApp := False;
    hWndFrame := GetTopParentHandle;
    hAccel := 0;
    cAccelEntries := 0;
  end;
  Result := S_OK;
end;

function TXApplet.Scroll(scrollExtent: TPoint): HResult;
begin
  Result := E_NOTIMPL;
end;

function TXApplet.OnUIDeactivate(fUndoable: BOOL): HResult;
begin
  SetMenu(0, 0, 0);
  SetUIActive(False);
  Result := S_OK;
end;

function TXApplet.OnInPlaceDeactivate: HResult;
begin
  FOleInPlaceActiveObject := nil;
  FOleInPlaceObject := nil;
  Result := S_OK;
end;

function TXApplet.DiscardUndoState: HResult;
begin
  Result := E_NOTIMPL;
end;

function TXApplet.DeactivateAndUndo: HResult;
begin
  FOleInPlaceObject.UIDeactivate;
  Result := S_OK;
end;

function TXApplet.OnPosRectChange(const rcPosRect: TRect): HResult;
begin
  FOleInPlaceObject.SetObjectRects(rcPosRect, Rect(0, 0, 32767, 32767));
  Result := S_OK;
end;

{ TpsvApplet.IOleInPlaceUIWindow }

function TXApplet.GetBorder(out rectBorder: TRect): HResult;
begin
  Result := INPLACE_E_NOTOOLSPACE;
end;

function TXApplet.RequestBorderSpace(const borderwidths: TRect): HResult;
begin
  Result := INPLACE_E_NOTOOLSPACE;
end;

function TXApplet.SetBorderSpace(pborderwidths: PRect): HResult;
begin
  Result := E_NOTIMPL;
end;

function TXApplet.SetActiveObject(const activeObject: IOleInPlaceActiveObject;
  pszObjName: POleStr): HResult;
begin
  Result := S_OK;
end;

{ TpsvApplet.IOleInPlaceFrame }

function TXApplet.OleInPlaceFrame_GetWindow(out wnd: HWnd): HResult;
begin
  wnd := GetTopParentHandle;
  Result := S_OK;
end;

function TXApplet.InsertMenus(hmenuShared: HMenu;
  var menuWidths: TOleMenuGroupWidths): HResult;
var
  Menu: TMainMenu;
begin
  Menu := GetMainMenu;
  if Menu <> nil then
    Menu.PopulateOle2Menu(hmenuShared, [0, 2, 4], menuWidths.width);
  Result := S_OK;
end;

function TXApplet.SetMenu(hmenuShared: HMenu; holemenu: HMenu;
  hwndActiveObject: HWnd): HResult;
var
  Menu: TMainMenu;
begin
  Menu := GetMainMenu;
  Result := S_OK;
  if Menu <> nil then
  begin
    Menu.SetOle2MenuHandle(hmenuShared);
    Result := OleSetMenuDescriptor(holemenu, Menu.WindowHandle,
      hwndActiveObject, nil, nil);
  end;
end;

function TXApplet.RemoveMenus(hmenuShared: HMenu): HResult;
begin
  while GetMenuItemCount(hmenuShared) > 0 do
    RemoveMenu(hmenuShared, 0, MF_BYPOSITION);
  Result := S_OK;
end;

function TXApplet.SetStatusText(pszStatusText: POleStr): HResult;
begin
  Result := S_OK;
end;

function TXApplet.EnableModeless(fEnable: BOOL): HResult;
begin
  Result := S_OK;
end;

function TXApplet.OleInPlaceFrame_TranslateAccelerator(
  var msg: TMsg; wID: Word): HResult;
begin
  Result := S_FALSE;
end;

{ TpsvApplet.IDispatch }

function TXApplet.GetTypeInfoCount(out Count: Integer): HResult;
begin
  Count := 0;
  Result := S_OK;
end;

function TXApplet.GetTypeInfo(Index, LocaleID: Integer;
  out TypeInfo): HResult;
begin
  Pointer(TypeInfo) := nil;
  Result := E_NOTIMPL;
end;

function TXApplet.GetIDsOfNames(const IID: TGUID; Names: Pointer;
  NameCount, LocaleID: Integer; DispIDs: Pointer): HResult;
begin
  Result := E_NOTIMPL;
end;

function TXApplet.Invoke(DispID: Integer; const IID: TGUID;
  LocaleID: Integer; Flags: Word; var Params;
  VarResult, ExcepInfo, ArgErr: Pointer): HResult;
var
  F: TFont;
begin
  if (Flags and DISPATCH_PROPERTYGET <> 0) and (VarResult <> nil) then
  begin
    Result := S_OK;
    case DispID of
      DISPID_AMBIENT_BACKCOLOR:
        PVariant(VarResult)^ := Color;
      DISPID_AMBIENT_DISPLAYNAME:
        PVariant(VarResult)^ := StringToVarOleStr(Name);
      DISPID_AMBIENT_FONT:
      begin
        if (Parent <> nil) and ParentFont then
          F := TXApplet(Parent).Font
        else
          F := Font;
        PVariant(VarResult)^ := FontToOleFont(F);
      end;
      DISPID_AMBIENT_FORECOLOR:
        PVariant(VarResult)^ := Font.Color;
      DISPID_AMBIENT_LOCALEID:
        PVariant(VarResult)^ := Integer(GetUserDefaultLCID);
      DISPID_AMBIENT_MESSAGEREFLECT:
        PVariant(VarResult)^ := True;
      DISPID_AMBIENT_USERMODE:
        PVariant(VarResult)^ := not (csDesigning in ComponentState);
      DISPID_AMBIENT_UIDEAD:
        PVariant(VarResult)^ := csDesigning in ComponentState;
      DISPID_AMBIENT_SHOWGRABHANDLES:
        PVariant(VarResult)^ := False;
      DISPID_AMBIENT_SHOWHATCHING:
        PVariant(VarResult)^ := False;
      DISPID_AMBIENT_SUPPORTSMNEMONICS:
        PVariant(VarResult)^ := True;
      DISPID_AMBIENT_AUTOCLIP:
        PVariant(VarResult)^ := True;
    else
      Result := DISP_E_MEMBERNOTFOUND;
    end;
  end else
    Result := DISP_E_MEMBERNOTFOUND;
end;

{ TpsvApplet.IPropertyNotifySink }

function TXApplet.OnChanged(dispid: TDispID): HResult;
begin
  try
    case dispid of
      DISPID_BACKCOLOR:
        if not FUpdatingColor then
        begin
          FUpdatingColor := True;
          try
            Color := GetIntegerProp(DISPID_BACKCOLOR);
          finally
            FUpdatingColor := False;
          end;
        end;
      DISPID_ENABLED:
        if not FUpdatingEnabled then
        begin
          FUpdatingEnabled := True;
          try
            Enabled := GetWordBoolProp(DISPID_ENABLED);
          finally
            FUpdatingEnabled := False;
          end;
        end;
      DISPID_FONT:
        if not FUpdatingFont then
        begin
          FUpdatingFont := True;
          try
            OleFontToFont(GetVariantProp(DISPID_FONT), Font);
          finally
            FUpdatingFont := False;
          end;
        end;
      DISPID_FORECOLOR:
        if not FUpdatingFont then
        begin
          FUpdatingFont := True;
          try
            Font.Color := GetIntegerProp(DISPID_FORECOLOR);
          finally
            FUpdatingFont := False;
          end;
        end;
    end;
  except  // control sent us a notification for a dispid it doesn't have.
    on EOleError do ;
  end;
  Result := S_OK;
end;

function TXApplet.OnRequestEdit(dispid: TDispID): HResult;
begin
  Result := S_OK;
end;

{ TpsvApplet.ISimpleFrameSite }

function TXApplet.PreMessageFilter(wnd: HWnd; msg, wp, lp: Integer;
  out res: Integer; out Cookie: Longint): HResult;
begin
  Result := S_OK;
end;

function TXApplet.PostMessageFilter(wnd: HWnd; msg, wp, lp: Integer;
  out res: Integer; Cookie: Longint): HResult;
begin
  Result := S_OK;
end;


procedure TXApplet.InitControlData;
const
  CControlData: TControlData2 =(
    LicenseKey: nil) ;
begin
  GetMem(FControlData, Sizeof(TControlData2));
  FillChar( FControlData^, Sizeof(TControlData2), 0);

  //CControlData.ClassID := FHost.FGUID;
  //ControlData := @CControlData;

  ControlData^.LicenseKey:= nil;
  ControlData^.ClassID:= FHost.FGUID;
end;

function FontToOleFont(Font: TFont): Variant;
var
  Temp: IFontDisp;
begin
  GetOleFont(Font, Temp);
  Result := Temp;
end;

procedure OleFontToFont(const OleFont: Variant; Font: TFont);
begin
  SetOleFont(Font, IUnknown(OleFont) as IFontDisp);
end;

function TXAppletHost.GetAppletTypeInfoCount: integer;
begin
  if DefaultDispatch = nil then
   Result := -1 else
     ClientDispatch.GetTypeInfoCount(Result);
end;

function TXAppletHost.InvokeDispatch(DispID: Integer; Flags: Word;
  var Params: TDispParams; var VarResult: Variant): HResult;
var
  ArgErr : integer;
  Excep  : TExcepInfo;
begin
  Result := ClientDispatch.Invoke(DispID, GUID_NULL, GetThreadLocale, Flags,
         Params, PVariant(@VarResult), @Excep, @ArgErr);
end;

function TXAppletHost.NameToDispID(const AName: string): TDispID;
var
  CharBuf: array[0..255] of WideChar;
  P      : array[0..0]   of PWideChar;
begin
  StringToWideChar(AName, @CharBuf, 256);
  P[0] := @CharBuf[0];
  if Failed(ClientDispatch.GetIDsOfNames(GUID_NULL, @P, 1, GetThreadLocale, @Result)) then
    raise EOleError.CreateFmt('Method %s not supported', [AName]);
end;

function TXAppletHost.NameToDispIDs(const AName: string;
  const AParams: array of string; Dest: PDispIDList): PDispIDList;
var
  CharBuf: array[0..64] of PWideChar;
  Size: Integer;
  I: Byte;
begin
  Result := Dest;
  Size := Length(AName) + 1;
  GetMem(CharBuf[0], Size * SizeOf(WideChar));
  StringToWideChar(AName, CharBuf[0], Size);
  for I := 0 to High(AParams) do begin
    Size := Length(AParams[I]) + 1;
    GetMem(CharBuf[I + 1], Size * SizeOf(WideChar));
    StringToWideChar(AParams[I], CharBuf[I + 1], Size);
  end;
  try
    if Failed(ClientDispatch.GetIDsOfNames(GUID_NULL,
        @CharBuf, High(AParams) + 2, GetThreadLocale, @Result^[0]))
    then
      raise EOleError.CreateFmt('Method %s not supporrted', [AName]);
  finally
    for I := 0 to High(AParams) + 1 do FreeMem(CharBuf[I]);
  end;
end;

function TXAppletHost.CallFunction(const AName: string): Variant;
begin
  Result := CallMethodNoParams(NameToDispID(AName));
end;

function TXAppletHost.CallFunction(const AName: string;
  const Params: array of const;
  const ParamNames: array of string): Variant;
var
  DispIDs: array[0..64] of TDispID;
begin
  Result := CallMethodNamedParams(NameToDispIDs(AName, ParamNames,
    @DispIDs)^, Params, High(ParamNames) + 1);
end;

function TXAppletHost.CallFunction(const AName: string;
  const Params: array of const): Variant;
begin
  Result := CallMethod(NameToDispID(AName), Params);
end;

procedure TXAppletHost.CallProcedure(const AName: string;
  const Params: array of const; const ParamNames: array of string);
var
  DispIDs: array[0..64] of TDispID;
begin
  CallMethodNamedParams(NameToDispIDs(AName, ParamNames, @DispIDs)^,
    Params, High(ParamNames) + 1);
end;

procedure TXAppletHost.CallProcedure(const AName: string;
  const Params: array of const);
begin
  CallMethod(NameToDispID(AName), Params);
end;

procedure TXAppletHost.CallProcedure(const AName: string);
begin
  CallMethodNoParams(NameToDispID(AName));
end;

procedure TXAppletHost.EnumProperties(AList: TStrings);
begin
  EnumDispatchProperties(Self, GUID_NULL, VT_EMPTY, AList);
end;

procedure TXAppletHost.EnumInvokeList(AList: TStrings);
begin
  AxUtils.EnumInvokeList(Self, AList);
end;

function TXAppletHost.GetProperty(const AName: string): Variant;
begin
  Result := GetPropertyByID(NameToDispID(AName));
end;

function TXAppletHost.GetTypeDescription(V: Variant): string;
begin
  case VarType(V) of
     VT_EMPTY : Result := 'Empty';
     VT_NULL  : Result := 'SQL style NULL';
     VT_I1    : Result := 'Signed char';
     VT_UI1   : Result := 'Unsigned char';
     VT_UI2   : Result := 'Unsigned short';
     VT_UI4   : Result := 'Unsigned short';
     VT_I2    : Result := '16-bit integer';
     VT_I4    : Result := '32-bit integer';
     VT_I8    : Result := 'signed 64-bit int';
     VT_UI8   : Result := 'unsigned 64-bit int';
     VT_R4    : Result := 'Single';
     VT_R8    : Result := 'Double';
     VT_CY    : Result := 'Currency';
     VT_DATE  : Result := 'Date';
     VT_BSTR  : Result := 'String';
     VT_ERROR : Result := 'Error Code';
     VT_BOOL  : Result := 'Boolean';
     VT_PTR   : Result := 'Pointer';
     VT_INT   : Result := 'signed machine int';
     VT_UINT  : Result := 'unsigned machine int';
     VT_VOID  : Result := 'C style void';
     VT_DISPATCH : Result := 'IDispatch Interface';
     VT_VARIANT  : Result := 'Variant';
     VT_UNKNOWN  : Result := 'IUnknown Interface';
     VT_DECIMAL  : Result := 'Decimal';
     VT_USERDEFINED : Result := 'User Defined';
     VT_LPSTR           : Result := 'null terminated string';
     VT_LPWSTR          : Result := 'wide null terminated string';
     VT_FILETIME        : Result := 'FILETIME';
     VT_BLOB            : Result := 'Length prefixed bytes';
     VT_STREAM          : Result := 'Name of the stream follows';
     VT_STORAGE         : Result := 'Name of the storage follows';
     VT_STREAMED_OBJECT : Result := 'Stream contains an object';
     VT_STORED_OBJECT   : Result := 'Storage contains an object';
     VT_BLOB_OBJECT     : Result := 'Blob contains an object';
     VT_CF              : Result := 'Clipboard format';
     VT_CLSID           : Result := 'A Class ID';
   else
     Result := 'VarType=' + IntToStr(VarType(V));
  end;
end;

procedure TXAppletHost.SetProperty(const AName: string;
  const Prop: array of const);
begin
  SetPropertyByID(NameToDispID(AName), Prop);
end;

function TXAppletHost.CallMethod(ID: TDispID;
  const Params: array of const): Variant;
var
  Disp: TDispParams;
  ArgCnt, I: Integer;
  Args: array[0..63] of TVariantArg;
begin
  ArgCnt := 0;
  try
    for I := 0 to High(Params) do begin
      VariantCast(Args[I],Params[I]);
      Inc(ArgCnt);
      if ArgCnt >= 64 then Break;
    end;
    if ArgCnt = 0 then
      Disp.rgvarg := nil
    else
      Disp.rgvarg := @Args;
    Disp.rgdispidNamedArgs := nil;
    Disp.cArgs := ArgCnt;
    Disp.cNamedArgs := 0;
    InvokeDispatch(ID, DISPATCH_METHOD, Disp, Result);
  finally
  end;
end;

function TXAppletHost.CallMethodNamedParams(const IDs: TDispIDList;
  const Params: array of const; Cnt: Byte): Variant;
var
  Disp: TDispParams;
  ArgCnt, I: Integer;
  Args: array[0..63] of TVariantArg;
begin
  ArgCnt := 0;
  try
    for I := 0 to High(Params) do begin
      VariantCast(Args[I], Params[I]);
      Inc(ArgCnt);
      if ArgCnt >= 64 then Break;
    end;
    with Disp do begin
      if ArgCnt = 0 then rgvarg := nil
      else rgvarg := @Args;
      if Cnt = 0 then rgdispidNamedArgs := nil
      else rgdispidNamedArgs := @IDs[1];
      cArgs := ArgCnt;
      cNamedArgs := Cnt;
    end;
      InvokeDispatch(IDs[0], DISPATCH_METHOD, Disp, Result);
  finally
  end;
end;

function TXAppletHost.CallMethodNoParams(ID: TDispID): Variant;
begin
  InvokeDispatch(ID, DISPATCH_METHOD, Disp, Result);
end;

function TXAppletHost.GetPropertyByID(ID: TDispID): Variant;
begin
  InvokeDispatch(ID, DISPATCH_PROPERTYGET, Disp, Result);
end;

procedure TXAppletHost.SetPropertyByID(ID: TDispID;
  const Prop: array of const);
const
  NameArg: TDispID = DISPID_PROPERTYPUT;
var
  Disp: TDispParams;
  ArgCnt, I: Integer;
  Args: array[0..63] of TVariantArg;
  Temp : Variant;
begin
  ArgCnt := 0;
  try
    for I := 0 to High(Prop) do begin
      VariantCast(Args[I], Prop[I]);
      Inc(ArgCnt);
      if ArgCnt >= 64 then Break;
    end;
    with Disp do begin
      rgvarg := @Args;
      rgdispidNamedArgs := @NameArg;
      cArgs := ArgCnt;
      cNamedArgs := 1;
    end;
    CheckHR(InvokeDispatch(ID, DISPATCH_PROPERTYPUT, Disp, Temp));
  finally
  end;
end;

end.
