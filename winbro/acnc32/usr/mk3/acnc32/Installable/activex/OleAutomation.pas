unit OleAutomation;

interface

uses SysUtils, Classes, OleCtrls, ActiveX, ComObj, Registry, Windows, AxUtils,
     ComServ, InvokableClient;

type
  TAutomationObject = class;
  TRegProc = function : HResult; stdcall;

  TAutomationHost = class (TInterfacedObject, IInvokableClient)
  private
    FGUID: TGUID;
    FGUIDStr: string;
    FActive: Boolean;
    FAutoExtractGUID: Boolean;
    FFilename: string;
    FAutomation: TAutomationObject;
    FAutoRegister: Boolean;
    procedure SetActive(Value: Boolean);
    procedure SetGUIDStr(Value : string);
    function CheckRegistered: Boolean;
    procedure RegisterObject;
    function ExtractGUID : TGUID;
    function GetClientDispatch: IDispatch;
  public
    constructor Create;
    procedure Test;
    function GetPropertyByID(ID: Integer): Variant;
    procedure SetPropertyByID(ID: Integer; const V: array of const);
    function NameToDispID(AName: string): Integer;
    procedure EnumProperties(AList: TStrings);
    procedure EnumInvokeList(AList: TStrings);
    property Active: Boolean read FActive write SetActive;
    property AutoExtractGUID: Boolean read FAutoExtractGUID write FAutoExtractGUID;
    property Filename: string read FFilename write FFilename;
    property AutoRegister: Boolean read FAutoRegister write FAutoRegister;
    property GUID: string read FGUIDStr write SetGUIDStr;

    function CallMethodNoParams(DispID : integer) : Variant;
    function CallMethod(ID: TDispID; const Params: array of const): Variant;
    function CallMethodNamedParams(const IDs: TDispIDList;
                                   const Params: array of const; Cnt: Byte): Variant;


    function InvokeDispatch(ID: TDispID; Flags: Word;
      var Params: TDispParams; var VarResult: Variant): HResult;
  end;

  TAutomationObject = class(TInterfacedObject, IPropertyNotifySink)
  private
    Host: TAutomationHost;
    FDispatch: IDispatch;
    FPropBrowsing: IPerPropertyBrowsing;
    FPropConnection: Longint;
    FControlData: PControlData;
    FEventDispatch: TEventDispatch;
    FEventsConnection: Longint;
    procedure CreateControl;
    procedure DestroyControl;
    procedure InitControlData;
    procedure CreateInstance;
    function OnChanged(ID: TDispID): HResult; stdcall;
    function OnRequestEdit(ID: TDispID): HResult; stdcall;
  public
    constructor Create(AHost: TAutomationHost);
    property ControlData: PControlData read FControlData;
    function Invoke(ID: TDispID; Flags: Word;
      var Params: TDispParams; var VarResult: Variant): HResult;
  end;

implementation

{$WRITEABLECONST ON}

const
  ProcName:  PChar = 'DllRegisterServer';

{ TIntroSpect }

procedure TAutomationHost.SetActive(Value: Boolean);
begin
  if FActive = Value then
    Exit;

  if not FAutoExtractGUID then begin
    if FGUIDStr = EmptyStr then
      raise Exception.Create('GUID Unknown');
  end;

  if FileName = EmptyStr then
     raise Exception.Create('File name missing');

  FActive := Value;

  if not FActive then begin
     FAutomation.Free;
     FAutomation := nil;
  end else begin
    if not FileExists(FileName) then begin
      FActive := False;
      raise Exception.CreateFmt('File %s not exists',[FileName]);
    end;

    if FGUIDStr = EmptyStr then begin
      if FAutoExtractGUID then begin
        FGUID := ExtractGUID;
        FGUIDStr := GUIDToString(FGUID);
      end;
    end;

    if not CheckRegistered then begin
      if FAutoRegister then begin
        RegisterObject;
      end
      else begin
        FActive := false;
        raise Exception.Create('Object not registered');
      end;
    end;

    FAutomation := TAutomationObject.Create(Self);
  end;
end;



procedure TAutomationObject.CreateInstance;
var Tmp: IUnknown;
begin
  Tmp:= CreateComObject(FControlData^.ClassID);
  FDispatch:= Tmp as IDispatch;
end;

procedure TAutomationObject.InitControlData;
const
  CControlData: TControlData2 =(
    LicenseKey: nil) ;
begin
  CControlData.ClassID := Host.FGUID;
  FControlData := @CControlData;
end;


procedure TAutomationHost.SetGUIDStr(Value : string);
begin
  if Value <> FGUIDStr then begin
    FGUIDStr := Value;
    FGUID := StringToGUID(FGUIDStr);
  end;
end;

constructor TAutomationHost.Create;
begin
  FGUIDStr:= EmptyStr;
  FGUID:= GUID_NULL;
end;

function TAutomationHost.CheckRegistered: Boolean;
var Reg : TRegistry;
begin
  Reg := TRegistry.Create;
  Reg.RootKey := HKEY_CLASSES_ROOT;
  Result := Reg.OpenKey('CLSID\' + GuidToString(FGUID), False);
  Reg.CloseKey;
  Reg.Free;
end;

function TAutomationHost.ExtractGUID : TGUID;
var TypeLib : ITypeLIB;
    I : Integer;
    TypeInfo : ActiveX.ITypeInfo;
    TypeAttr : PTypeAttr;
    TK : ActiveX.TTypeKIND;
begin
  Result := GUID_NULL;

  OleCheck(LoadTypeLib(PWideChar(WideString(FileName)), TypeLib));
  for I := 0 to TypeLib.GetTypeInfoCount -1 do begin
    TypeLib.GetTypeInfoType(I,TK);
    if TK = TKIND_COCLASS then begin
      TypeLib.GetTypeInfo(I,TypeInfo);
      TypeInfo.GetTypeAttr(TypeAttr);
      Result := TypeAttr.guid;
      TypeInfo.ReleaseTypeAttr(TypeAttr);
      Exit;
    end;
  end;
end;

{
procedure EnumDispatchProperties(ADispatch: IDispatch; PropType: TGUID;
  VTCode: Integer; PropList: TStrings);
const
  INVOKE_PROPERTYSET = INVOKE_PROPERTYPUT or INVOKE_PROPERTYPUTREF;
var
  I: Integer;
  TypeInfo: ITypeInfo;
  TypeAttr: PTypeAttr;
  FuncDesc: PFuncDesc;
  VarDesc: PVarDesc;

  procedure SaveName(Id: Integer);
  var
    Name: WideString;
  begin
    OleCheck(TypeInfo.GetDocumentation(Id, @Name, nil, nil, nil));
    if PropList.IndexOfObject(TObject(Id)) = -1 then
      PropList.AddObject(Name, TObject(Id));
  end;

  function IsPropType(const TypeInfo: ITypeInfo; TypeDesc: PTypeDesc): Boolean;
  var
    RefInfo: ITypeInfo;
    RefAttr: PTypeAttr;
    IsNullGuid: Boolean;
  begin
    IsNullGuid := IsEqualGuid(PropType, GUID_NULL);

    Result := IsNullGuid and (VTCode = VT_EMPTY);
    if Result then
      Exit;

    case TypeDesc.vt of
      VT_PTR: Result := IsPropType(TypeInfo, TypeDesc.ptdesc);
      VT_USERDEFINED:
        begin
          OleCheck(TypeInfo.GetRefTypeInfo(TypeDesc.hreftype, RefInfo));
          OleCheck(RefInfo.GetTypeAttr(RefAttr));
          try
            Result := IsEqualGUID(RefAttr.guid, PropType);
            if not Result and (RefAttr.typekind = TKIND_ALIAS) then
              Result := IsPropType(RefInfo, @RefAttr.tdescAlias);
          finally
            RefInfo.ReleaseTypeAttr(RefAttr);
          end;
        end;
    else
      Result := IsNullGuid and (TypeDesc.vt = VTCode);
    end;
  end;

  function HasMember(const TypeInfo: ITypeInfo; Cnt, MemID, InvKind: Integer): Boolean;
  var
    I: Integer;
    FuncDesc: PFuncDesc;
  begin
    for I := 0 to Cnt - 1 do
    begin
      OleCheck(TypeInfo.GetFuncDesc(I, FuncDesc));
      try
        if (FuncDesc.memid = MemID) and (FuncDesc.invkind and InvKind <> 0) then
        begin
          Result := True;
          Exit;
        end;
      finally
        TypeInfo.ReleaseFuncDesc(FuncDesc);
      end;
    end;
    Result := False;
  end;

begin
  OleCheck(ADispatch.GetTypeInfo(0,0,TypeInfo));
  if TypeInfo = nil then Exit;
  OleCheck(TypeInfo.GetTypeAttr(TypeAttr));
  try
    for I := 0 to TypeAttr.cVars - 1 do
    begin
      OleCheck(TypeInfo.GetVarDesc(I, VarDesc));
      try
        if ((VarDesc.wVarFlags and VARFLAG_FREADONLY <> 0 ) and
          IsPropType(TypeInfo, @VarDesc.elemdescVar.tdesc)) then
          SaveName(VarDesc.memid);
      finally
        TypeInfo.ReleaseVarDesc(VarDesc);
      end;
    end;
    for I := 0 to TypeAttr.cFuncs - 1 do
    begin
      OleCheck(TypeInfo.GetFuncDesc(I, FuncDesc));
      try
        if ((FuncDesc.invkind = INVOKE_PROPERTYGET) and
          HasMember(TypeInfo, TypeAttr.cFuncs, FuncDesc.memid, INVOKE_PROPERTYSET) and
          IsPropType(TypeInfo, @FuncDesc.elemdescFunc.tdesc)) or
          ((FuncDesc.invkind and INVOKE_PROPERTYSET <> 0) and
          HasMember(TypeInfo, TypeAttr.cFuncs, FuncDesc.memid, INVOKE_PROPERTYGET) and
          IsPropType(TypeInfo,
            @FuncDesc.lprgelemdescParam[FuncDesc.cParams - 1].tdesc)) then
            SaveName(FuncDesc.memid);
      finally
        TypeInfo.ReleaseFuncDesc(FuncDesc);
      end;
    end;
  finally
    TypeInfo.ReleaseTypeAttr(TypeAttr);
  end;
end;
}
constructor TAutomationObject.Create(AHost: TAutomationHost);
begin
  Host := AHost;

  InitControlData;

  CreateInstance;
  //OleCheck(FDispatch.QueryInterface(IPersistStreamInit, FPersistStream));
  CreateControl;

  Inc(FControlData^.InstanceCount);

end;

procedure TAutomationObject.CreateControl;
begin
  try
    FDispatch.QueryInterface(
      IPerPropertyBrowsing,
      FPropBrowsing
    );

    InterfaceConnect(
      FDispatch,
      IPropertyNotifySink,
      Self,
      FPropConnection
    );

    InterfaceConnect(
      FDispatch,
      FControlData^.EventIID,
      FEventDispatch,
      FEventsConnection
    );

  except
    DestroyControl;
    raise;
  end;
end;

procedure TAutomationObject.DestroyControl;
begin
  InterfaceDisconnect(FDispatch, FControlData^.EventIID, FEventsConnection);
  InterfaceDisconnect(FDispatch, IPropertyNotifySink, FPropConnection);
  FPropBrowsing := nil;
end;

procedure TAutomationHost.EnumInvokeList(AList: TStrings);
begin
  AxUtils.EnumInvokeList(Self, AList);
end;

procedure TAutomationHost.EnumProperties(AList: TStrings);
begin
  EnumDispatchProperties(Self, GUID_NULL, VT_EMPTY, AList);
end;


procedure TAutomationHost.RegisterObject;
var
  LibHandle: THandle;
  RegProc: TRegProc;
  Return: Integer;
begin
  if FileName = EmptyStr then
    raise Exception.Create('Unknown file name');
  LibHandle := LoadLibrary(PChar(FileName));
  if LibHandle = 0 then
    raise Exception.CreateFmt('Failed to load "%s"', [FileName]);

  try
    @RegProc := GetProcAddress(LibHandle, ProcName);
    if @RegProc = nil then
      raise Exception.CreateFmt('%s procedure not found in "%s"', [ProcName, FileName]);
    Return:= RegProc;
    if Return <> 0 then
      raise Exception.CreateFmt('Call to %s failed in "%s" returned %d', [ProcName, FileName, Return]);
  finally
    FreeLibrary(LibHandle);
  end;
end;


function TAutomationObject.OnChanged(ID: TDispID): HResult;
begin
  Result:= S_OK;
end;

function TAutomationObject.OnRequestEdit(ID: TDispID): HResult;
begin
  Result:= S_OK;
end;

function TAutomationHost.GetPropertyByID(ID: Integer): Variant;
begin
  FAutomation.Invoke(ID, DISPATCH_PROPERTYGET, Disp, Result);
end;

function TAutomationHost.NameToDispID(AName: string): Integer;
var
  CharBuf: array[0..255] of WideChar;
  P      : array[0..0]   of PWideChar;
begin
  StringToWideChar(AName, @CharBuf, 256);
  P[0] := @CharBuf[0];
  if Failed(FAutomation.FDispatch.GetIDsOfNames(GUID_NULL, @P, 1, GetThreadLocale, @Result)) then
    raise EOleError.CreateFmt('Method %s not supported', [AName]);
end;

procedure TAutomationHost.SetPropertyByID(ID: Integer; const V: array of const);
const
  NameArg: TDispID = DISPID_PROPERTYPUT;
var
  Disp: TDispParams;
  ArgCnt, I: Integer;
  Args: array[0..63] of TVariantArg;
  Temp : Variant;
begin
  ArgCnt := 0;
  try
    for I := 0 to High(V) do begin
      VariantCast(Args[I], V[I]);
      Inc(ArgCnt);
      if ArgCnt >= 64 then Break;
    end;
    with Disp do begin
      rgvarg := @Args;
      rgdispidNamedArgs := @NameArg;
      cArgs := ArgCnt;
      cNamedArgs := 1;
    end;
    CheckHR(FAutomation.Invoke(ID, DISPATCH_PROPERTYPUT, Disp, Temp));
  finally
  end;
end;

function TAutomationObject.Invoke(ID: TDispID; Flags: Word;
  var Params: TDispParams; var VarResult: Variant): HResult;
var
  ArgErr : integer;
  Excep  : TExcepInfo;
begin
  Result := FDispatch.Invoke(ID, GUID_NULL, GetThreadLocale, Flags, Params, PVariant(@VarResult), @Excep, @ArgErr);
end;


var Tmp1: Integer;
    B: Boolean;
procedure TAutomationHost.Test;
var lib: ITypeLib;
    I: Integer;
    info: ITypeInfo;
    ComS: TComServer;
begin
  //LoadRegTypeLib(guid,
  ComS:= TComServer.Create;
  ComS.SetServerName(FFilename);
  Coms.LoadTypeLib;
  //ComS.Initialize;
  lib:= Coms.TypeLib;
  for I:= 0 to lib.GetTypeInfoCount do begin
    lib.GetTypeInfo(I, info);
  end;

  Tmp1:= ComS.ObjectCount;
  B:= ComS.IsInprocServer;
end;

function TAutomationHost.CallMethod(ID: TDispID;
  const Params: array of const): Variant;
var
  Disp: TDispParams;
  ArgCnt, I: Integer;
  Args: array[0..63] of TVariantArg;
begin
  ArgCnt := 0;
  try
    for I := 0 to High(Params) do begin
      VariantCast(Args[I],Params[I]);
      Inc(ArgCnt);
      if ArgCnt >= 64 then Break;
    end;
    with Disp do begin
      if ArgCnt = 0 then rgvarg := nil
      else rgvarg := @Args;
      rgdispidNamedArgs := nil;
      cArgs := ArgCnt;
      cNamedArgs := 0;
    end;
     Result := FAutomation.Invoke(ID, DISPATCH_METHOD, Disp, Result);
  finally
  end;
end;

function TAutomationHost.CallMethodNoParams(DispID: integer): Variant;
begin
  Result := FAutomation.Invoke(DispID, DISPATCH_METHOD, Disp, Result);
end;

function TAutomationHost.CallMethodNamedParams(const IDs: TDispIDList;
  const Params: array of const; Cnt: Byte): Variant;
var
  Disp: TDispParams;
  ArgCnt, I: Integer;
  Args: array[0..63] of TVariantArg;
begin
  ArgCnt := 0;
  try
    for I := 0 to High(Params) do begin
      VariantCast(Args[I], Params[I]);
      Inc(ArgCnt);
      if ArgCnt >= 64 then Break;
    end;
    with Disp do begin
      if ArgCnt = 0 then rgvarg := nil
      else rgvarg := @Args;
      if Cnt = 0 then rgdispidNamedArgs := nil
      else rgdispidNamedArgs := @IDs[1];
      cArgs := ArgCnt;
      cNamedArgs := Cnt;
    end;
      FAutomation.Invoke(IDs[0], DISPATCH_METHOD, Disp, Result);
  finally
  end;
end;

function TAutomationHost.GetClientDispatch: IDispatch;
begin
  Result:= Self.FAutomation.FDispatch;
end;

function TAutomationHost.InvokeDispatch(ID: TDispID; Flags: Word;
  var Params: TDispParams; var VarResult: Variant): HResult;
begin
  Result:= FAutomation.Invoke(ID, Flags, Params, VarResult);
end;

end.
