unit FileManager;
interface
uses
  SysUtils, Classes, Windows, CompoundFiles, StdCtrls,
  CoreCNC, CNCTypes, CNC32, Named, StdEditor, Plugin, Acknowledge, FaultRegistration;

type
  TFileManager = class(TAbstractFileManager)
  private
    FDefaultFilename: String;
    StatusBox: TStaticText;
    ListBox: TListBox; {objects = Editors [AvailableEditors]}
    ActiveFile, BackgroundFile: TCompoundFile;
    ActiveKeyHandle : word;
    BackGroundKeyHandle : word;
    FEditing : Boolean;
    ShowSoftKeys : Boolean;
    FLoadFailed : Boolean;

    procedure ListBoxDblClick(Sender: TObject);
    procedure ListBoxKeyPress(Sender: TObject; var Key: Char);
    procedure OpenSelectedSection;
    procedure UpdateStatusDisplay; virtual;
    function CurrentFile: TCompoundFile;
             {ActiveFile in EditMode, otherwise Background}
    procedure DoSave(aTag : TAbstractTag);
                {save 'current' file}
    procedure DoSaveAs(aTag : TAbstractTag);
                {'current' file}
    procedure DoEdit(aTag : TAbstractTag);
                {'current' file}
    procedure DoBackgroundClose(aTag : TAbstractTag);
                {Background File}
    procedure DoBackgroundOpen(aTag : TAbstractTag);
                {Background File - do nothing in edit mode}
    procedure DoNewFile(aTag : TAbstractTag);
                {Create A NewFile - Do nothing in edit mode }
    procedure DoEditAsText(aTag : TAbstractTag);
             {ActiveFile in EditMode, otherwise Background}
  protected
    function GetActiveFileState : TFileState; override;
    function GetActiveFileName : String; override;
    procedure DoFollowLoadActive(aTag: TAbstractTag); override;
    procedure CNCModeChanged(aTag : TAbstractTag); override;
    procedure DoLoadActive(aTag : TAbstractTag); override;

    function SaveSemantic(var aFileName : string) : Boolean;
    function LoadSemantic(var aFileName : string) : Boolean;
    function TryLoadActiveFile(aFilename: string): Boolean;

                {load active file}
  public
    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;
    procedure Open; override;
    procedure SectionRead( const SectionName: String; Data: TStrings); override;{read only}
    procedure SectionOpen( const SectionName: String; Data: TStrings); override;{(file = open) and (sect = closed)}
    procedure SectionClose( Data: TStrings; SaveData: Boolean); override;

    procedure InstallComponent(Params : TParamStrings); override;
    procedure Installed; override;
    function LoadActiveFile(aFileName: String) : Boolean; override;
    function IsActiveFile( F: TObject): Boolean; override;
    property ActiveFileName : string read GetActiveFileName;
    property ActiveFileState : TFileState read GetActiveFileState;
    procedure AttachEditor(aEditor : TSectionEditor; aDescription : string); override;
  published
  end;


{possible parameters
  FileMask=*.nc
  FilePath=Programs
  ;relative to exe
  c:\programs
  ;absolute

  see CNC32.GetAbsoluteDirectory}

implementation
uses
  TypInfo,
  Dialogs,
  Forms,
  Controls,
  Graphics,
  NCNames,
  zFileList;

resourcestring
  CannotOpenActive = 'Cannot open %s for background editing, as it is the active file';

const
  SectionKey = 'Section.';
  DefaultFilenameKey = 'DefaultFilename';
  DefaultDefaultFilename = 'Newfile.nc';

  FileEditorName = 'Editor';

function TFileManager.GetActiveFileState : TFileState;
begin
  Result:= ActiveFile.FileState
end;

function TFileManager.GetActiveFileName : String;
begin
  Result:= ActiveFile.FileName
end;

procedure TFileManager.SectionRead( const SectionName: String; Data: TStrings);
begin
  ActiveFile.SectionRead(SectionName, Data)
end;

procedure TFileManager.SectionOpen( const SectionName: String; Data: TStrings);
begin
  ActiveFile.SectionOpen(SectionName, Data)
end;

procedure TFileManager.SectionClose( Data: TStrings; SaveData: Boolean);
begin
  ActiveFile.SectionClose(Data, SaveData)
end;

function TFileManager.CurrentFile: TCompoundFile;
begin
  if EditMode then
    Result:= ActiveFile
  else
    Result:= BackgroundFile
end;


function TFileManager.LoadActiveFile( aFilename: String) : Boolean;
begin
  Result:= TryLoadActiveFile(aFileName);
end;

function TFileManager.TryLoadActiveFile(aFilename: string): Boolean;
begin
  Result := False;
  FLoadFailed := False;
  if not SysObj.NC.InCycle then begin
    try
      with ActiveFile do begin
        if FileState <> fsClosed then
          FileClose;
        FileOpen(aFileName);
        Result := True;
      end;
      FileChangedTag.AsString := aFileName;
      RemoteFileTag.AsString := ExtractFileName(aFileName);
      OPPFilePathTag.AsString:= ExtractFileDir(aFileName);
    except
      FLoadFailed := True;
      Exit;
    end;
    UpdateStatusDisplay
  end;
end;

{
function TFileManager.ResetLoadFailedSerious(FID : FHandle) : Boolean;
begin
  Result := not FLoadFailed;
end;
 }

procedure TFileManager.AttachEditor(aEditor : TSectionEditor; aDescription : string);
begin
  ListBox.Items.AddObject(aDescription, aEditor);
  if ListBox.Items.Count > 0 then
    ListBox.ItemIndex:= 0;
end;

procedure TFileManager.InstallComponent(Params : TParamStrings);
var
  Keys: TKeyArray;

begin
  inherited InstallComponent(Params);
  ParentFont:= True;
  with StatusBox do
  begin
    Font.Size:= 12;
    Font.Style:= [fsBold];
  end;
  with ListBox do begin
    ListBox.Font.Size := 18;
  end;

  ShowSoftKeys := True;
  FDefaultFilename:= Params.ReadString(DefaultFilenameKey, DefaultDefaultFilename);

  Primary := ListBox;

  SectionEditingTag:= TagPublish(nameSectionEditingMode, TIntegerTag);
  keys := Params.ReadPrefixKeys('Active');
  ActiveKeyHandle := SysObj.Softkey.registerKeySet(keys);
  keys := Params.ReadPrefixKeys('Back');
  BackGroundKeyHandle := SysObj.Softkey.RegisterKeySet(keys);

  ManageEdit:= true;
  UpdateStatusDisplay
end;

procedure TFileManager.Installed;
begin
  inherited Installed;
  TagEvent(Name, edgeFalling, TMethodTag, Activate);
  TagEvent('DoSave', edgeFalling, TMethodTag, DoSave);
  TagEvent('DoSaveAs', edgeFalling, TMethodTag, DoSaveAs);
  TagEvent('DoEdit', edgeFalling, TMethodTag, DoEdit);
  TagEvent('DoBackGroundOpen', edgeFalling, TMethodTag, DoBackGroundOpen);
  TagEvent('DoBackGroundClose', edgeFalling, TMethodTag, DoBackGroundClose);
  TagEvent('DoNewFile', edgeFalling, TMethodTag, DoNewFile);
  TagEvent('DoEditAsText', edgeFalling, TMethodTag, DoEditAsText);
end;
procedure TFileManager.Open;
begin
  UpdateStatusDisplay;
  if ShowSoftKeys then
    if EditMode then SysObj.Softkey.KeyChange(ActiveKeyHandle)
                else SysObj.Softkey.KeyChange(BackGroundKeyHandle);
  inherited Open;
end;

procedure TFileManager.DoLoadActive(aTag : TAbstractTag);
var
  NewFilename: String;
begin
  if SysObj.ServerSystem then
    Exit;

  if not ATag.AsBoolean then begin
    if SysObj.NC.InCycle or (SysObj.NC.ProgramLine <> 0) then begin
      SysObj.FaultInterface.SetFault(Self, CoreFaults[cfCantLoadNeedRewind], [], nil);
      Exit;
    end;

    with CurrentFile do
    begin
      NewFilename:= CNC32.GetAbsolutePathLocal(FFilepath) +  FFilemask;
      if LoadSemantic(NewFileName) then begin
        if (FileState = fsClosed) or FileClose then begin
          if SysObj.ClientSystem then
            FileForceTag.AsString := ExtractFileName(NewFilename)
          else if not SysObj.ServerSystem then
            LoadActiveFile(NewFilename);
        end;
      end;
    end
  end;
end;

procedure TFileManager.DoFollowLoadActive(aTag: TAbstractTag);
begin
  if aTag.AsString <> '' then begin
    try
      if FileExists(FFilePath + '\' + aTag.AsString) then
        LoadActiveFile(FFilePath + '\' + aTag.AsString)
      else if aTag.AsString[2] = ':' then
        LoadActiveFile(aTag.AsString)
      else if (aTag.AsString[1] = '\') then
        LoadActiveFile(ProfileDrive + aTag.AsString)
      else
        LoadActiveFile(FFilePath + '\' + aTag.AsString);
    except
      on E : Exception do
        SysObj.FaultInterface.SetFault(Self, CoreFaults[cfFailedLoad], [E.message], nil);
    end;
  end;
end;

procedure TFileManager.DoSave(aTag : TAbstractTag);
begin
  if CurrentFile.FileState = fsEdited then
    try
      CurrentFile.FileSave
    except
      on E:ECompoundFileError do
        Application.ShowException(E);
      on E:Exception do
        SysObj.FaultInterface.SetFault(Self, CoreFaults[cfFailedSave], [E.message], nil);
    end
end;

function TFileManager.SaveSemantic(var aFileName : string) : Boolean;
var Resp : array [0..1023] of Char;
begin
  Resp := '';
  if Assigned(SysObj.AlternateLoad) then begin
    Result := SysObj.AlternateLoad(PChar(aFileName), Resp, False);
    aFileName := Resp;
  end else
    Result := zFileList.SelectFile([sfoCanEditFilename], aFileName)
end;

function TFileManager.LoadSemantic(var aFileName : string) : Boolean;
var Resp : array [0..1023] of Char;
begin
  Result := False;
  if not SysObj.Comms.OKToLoadFile then begin
    if not Acknowledge.AreYouSure(SysObj.Localized.GetString('$LOADWHILEDIRTY')) then
      Exit;
  end;

  Resp := '';
  if Assigned(SysObj.AlternateLoad) then begin
    Result := SysObj.AlternateLoad(PChar(aFileName), Resp, True);
    aFileName := Resp;
  end else
    Result := zFileList.SelectFile([], aFileName)
end;

procedure TFileManager.DoSaveAs(aTag : TAbstractTag);
var
  NewFilename: String;
begin
  with CurrentFile do
  try
    NewFilename:= ActiveFileName; //CNC32.GetAbsolutePathLocal(FFilepath) +  FFilemask;
    if (FileState <> fsClosed) and
      SaveSemantic(NewFileName) then begin
      FileSaveAs(NewFilename);
      if EditMode then
        FileChangedTag.AsString := NewFileName;
      UpdateStatusDisplay
    end;
  except
    on E:ECompoundFileError do
      Application.ShowException(E)
  end
end;

procedure TFileManager.DoEdit(aTag : TAbstractTag);
begin
  OpenSelectedSection
end;

procedure TFileManager.DoBackgroundClose(aTag : TAbstractTag);
begin
  try
    if not aTag.AsBoolean then
      BackgroundFile.FileClose;
  except
    on E:ECompoundFileError do
      Application.ShowException(E)
  end;
  UpdateStatusDisplay
end;

procedure TFileManager.DoBackgroundOpen(aTag : TAbstractTag);
var
  NewFilename: String;
begin
  try
    if EditMode then Exit;

    with BackgroundFile do
    begin
      NewFilename:= CNC32.GetAbsolutePathLocal(FFilepath) +  FFilemask;
      if ((FileState = fsClosed) or FileClose) and
        LoadSemantic(NewFileName) then
      begin
        if (ActiveFile.Filestate <> fsClosed) and (NewFileName = ActiveFile.FileName) then
          MessageDlg(Format(CannotOpenActive, [NewFilename]), mtError, [mbOK], 0)
        else
          FileOpen(NewFilename)
      end;
      UpdateStatusDisplay
    end
  except
    on E:ECompoundFileError do
      Application.ShowException(E)
  end
end;

procedure TFileManager.DoNewFile(aTag : TAbstractTag);
var
    NewFilename: String;
begin
  if EditMode then Exit;
  with CurrentFile do
  try
    if FileState = fsEdited then
      FileClose;
    NewFilename:= CNC32.GetAbsolutePathLocal(FFilepath) +  FDefaultFilename;
    FileNew(NewFilename);
    UpdateStatusDisplay;
  except
    on E: ECompoundFileError do
      Application.ShowException(E)
  end
end;

procedure TFileManager.DoEditAsText(aTag : TAbstractTag);
var StdE : TStandardEditor;
begin
  StdE := TStandardEditor.Create;
  try
    try
      if ListBox.ItemIndex <> -1 then begin
        FEditing := True;
        SectionEditingTag.AsBoolean := FEditing;
        with TSectionEditor(ListBox.Items.Objects[ListBox.ItemIndex]) do
          StdE.ThisSectionName := SectionName;
        SysObj.NC.AcquireModeChangeLock;
        try
          if StdE.Execute(CurrentFile) and EditMode then begin
             FileChangedTag.AsString := ActiveFile.FileName;
             try
               CurrentFile.FileSave
             except
               on E:ECompoundFileError do
                 Application.ShowException(E)
             end;
          end;
        finally
          SysObj.NC.ReleaseModeChangeLock;
        end;
      end;
    except
      on E: ECompoundFileError do Application.ShowException(E)
    end;
  finally
    StdE.Free;
    FEditing := False;
    SectionEditingTag.AsBoolean := FEditing;
  end;
end;

constructor TFileManager.Create( aOwner: TComponent);
begin
  inherited Create(aOwner);
  ActiveFile:= TCompoundFile.Create;
  BackgroundFile:= TCompoundFile.Create;
  StatusBox:= TStaticText.Create(Self);
  with StatusBox do
  begin
    Parent:= Self;
    Caption:= '';
    BorderStyle:= sbsSunken;
    Align:= alTop
  end;
  ListBox:= TListBox.Create(Self);
  with ListBox do begin
    Parent:= Self;
    Align:= alClient;
    OnDblClick:= ListBoxDblClick;
    OnKeyPress:= ListBoxKeyPress;
  end;
end;

destructor TFileManager.Destroy;
begin
  ActiveFile.Free;
  BackgroundFile.Free;
  inherited Destroy
end;

procedure TFileManager.UpdateStatusDisplay;

  function ffn( f: TCompoundFile): String;
  begin
    if f.FileState = fsClosed then
      Result:= SysObj.Localized.GetString('$NONE')
    else
      Result:= ExtractFilename(f.FileName)
  end;


begin
  if EditMode then
  begin
    StatusBox.Caption:= Format(SysObj.Localized.GetString('$EDITING_ACTIVE_FILE_S'), [ffn(ActiveFile)]);
  end else
  begin
    StatusBox.Caption:= Format(SysObj.Localized.GetString('$EDITING_BACKGROUND_FILE_S'), [ffn(BackgroundFile)]);
  end;
end;

procedure TFileManager.OpenSelectedSection;
begin
  try
    try
      if ListBox.ItemIndex <> -1 then
      begin
        if EditMode then
        begin
          FEditing := True;
          SectionEditingTag.AsBoolean := FEditing;
          if not SysObj.Comms.OKToEditSection(TSectionEditor(ListBox.Items.Objects[ListBox.ItemIndex]).SectionName) then
            if not Acknowledge.AreYouSure(SysObj.Localized.GetString('$EDITWHILEDIRTY')) then
              Exit;

          SysObj.NC.AcquireModeChangeLock;
          try
            if TSectionEditor(ListBox.Items.Objects[ListBox.ItemIndex]).Execute(ActiveFile) then begin
                 FileChangedTag.AsString := ActiveFile.FileName;
                 try
                   CurrentFile.FileSave
                 except
                   on E:ECompoundFileError do
                     Application.ShowException(E);
                   on E:Exception do
                     SysObj.FaultInterface.SetFault(Self, CoreFaults[cfFailedSave], [E.message], nil);
                 end;
            end;
          finally
            SysObj.NC.ReleaseModeChangeLock;
          end;
        end else begin
          TSectionEditor(ListBox.Items.Objects[ListBox.ItemIndex]).Execute(BackgroundFile)
        end
      end;
    except
      on E: ECompoundFileError do begin
        SysObj.FaultInterface.SetFault(Self, CoreFaults[cfSysInterlock], [E.message], nil);
      end;
    end;
  finally
    FEditing := False;
    SectionEditingTag.AsBoolean := FEditing;
  end
end;

procedure TFileManager.ListBoxDblClick(Sender: TObject);
begin
  OpenSelectedSection
end;

procedure TFileManager.ListBoxKeyPress(Sender: TObject; var Key: Char);
begin
  if Key = #$0D then
    OpenSelectedSection
end;

procedure TFileManager.CNCModeChanged(aTag : TAbstractTag);
begin
  UpdateStatusDisplay
end;

function TFileManager.IsActiveFile( F: TObject): Boolean;
begin
  Result:= F = ActiveFile
end;


initialization
  RegisterCNCClass(TFileManager);
end.
