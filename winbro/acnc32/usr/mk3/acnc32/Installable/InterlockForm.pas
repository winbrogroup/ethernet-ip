unit InterlockForm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, CoreCNC, CncTypes;

type
  TInterlockFrm = class(TForm)
    ErrorPanelA: TPanel;
    Panel2: TPanel;
    ButtonLabel: TLabel;
    ErrorLabel: TLabel;
    procedure FormClick(Sender: TObject);
    procedure ButtonLabelMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FormCreate(Sender: TObject);
  private
  public
    class procedure Execute(const Text : string);
    class procedure CloseInterlock;
  end;

implementation

{$R *.DFM}

var
  InterlockFrm: TInterlockFrm;


class procedure TInterlockFrm.CloseInterlock;
begin
  if Assigned(InterlockFrm) then begin
    InterlockFrm.ModalResult := mrOK;
  end;
end;

function FormatText(const Text: string) : string;
var I: Integer;
begin
  for I:= 1 to Length(Text) do begin
    if Text[I] = '~' then begin
      Result:= Result + CarRet;
    end
    else begin
      Result:= Result + Text[I];
    end;
  end;
end;

class procedure TInterlockFrm.Execute(const Text : string);
begin
  // Create the form if necessary
  if not Assigned(InterlockFrm) then
    InterlockFrm := TInterlockFrm.Create(Application);
  InterLockFrm.Font.Name:= Sysobj.FontName;

  // Change the text if visible
  if InterlockFrm.Visible then begin
    InterlockFrm.ErrorLabel.Caption := FormatText(Text);
    Exit;
  end;

  // Interlock and display while visible
  SysObj.NC.AcquireCycleStartLock;
  try
    InterlockFrm.ErrorLabel.Caption := FormatText(Text);
    InterlockFrm.Width := SysObj.Layout.DisplayWidth;
    InterlockFrm.ShowModal;
  finally
    SysObj.NC.ReleaseCycleStartLock;
  end;
end;


procedure TInterlockFrm.FormClick(Sender: TObject);
begin
  ModalResult := mrOK;
end;

procedure TInterlockFrm.ButtonLabelMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  ModalResult := mrOK;
end;


procedure TInterlockFrm.FormCreate(Sender: TObject);
var F: TFont;
begin
  Font:= Sysobj.VisualParent.Font;
  ButtonLabel.Caption := SysObj.Localized.GetString(ButtonLabel.Caption);

  F:= ButtonLabel.Font;
  F.Name:= Sysobj.FontName;
  ButtonLabel.Font.Assign(F);

  ErrorLabel.Font.Color:= clWhite;
  ErrorLabel.Font.Name:= Sysobj.MonoFontName;
  ErrorLabel.Font.Size:= 14;
end;

end.
