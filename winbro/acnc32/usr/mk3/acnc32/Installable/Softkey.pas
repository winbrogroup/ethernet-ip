unit Softkey;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, CoreCNC, StdCtrls, IniFiles, Named, CNCUtils, NCNames,
  CNCTypes, CNC32, AbstractEditor;

type
  TSoftButton = btnF1..btnF8;

  TBasicSoftkey = class(TAbstractSoftkey)
  private
    Button           : array [TSoftButton] of TCNCButton;
    FCurrentFunction : TSoftButton;
    AltEnabled       : Boolean;
    OpenHandle       : integer;
    Esc, Switch      : TCNCButton;
    AltKeys          : TKeyArray;
    procedure FunctionChange(func : TSoftButton); virtual; abstract;
    procedure KeyPressed(Sender : Tobject; value : boolean); virtual; abstract;
  protected
    procedure Resize; override;
  public
    constructor Create      (Aowner : TComponent); override;
    procedure Installed; override;
    procedure  KeyClose     (KSetH : word); override;
    property CurrentFunction : TSoftButton read FCurrentFunction write FunctionChange;
    procedure SetEnabled(aEnabled : Boolean); override;
    procedure  KeyEvent        (key : Integer; edge : TEdgeType); override;
    procedure  KeyChange    (KSetH : word); override;
    procedure Reset; override;
  end;

{ This is the first actual implemention of TAbstractSoftkey, it provides the basic
  functionality required for a softkey handler with Function, Service, Alternate
  key selection capabilities }
  TButtonDisplay = class(TBasicSoftkey)
  private
    Mainkeys     : TKeyArray;
    MainEnabled  : Boolean;
    keys         : array [TSoftButton] of TKeyArray;
    procedure FunctionChange(func : TSoftButton); override;
    procedure KeyPressed(Sender : Tobject; value : boolean); override;
  protected
  public
    procedure  InstallComponent(Params : TParamStrings); override;
    destructor Destroy; override;
  published
  end;

  TModalSoftkey = class(TButtonDisplay)
  private
    FNCMode       : TCNCMode;
    keys         : array [TCNCMode] of TKeyArray;
    procedure FunctionChange(func : TSoftButton); override;
    procedure ModeChange(aTag : TAbstractTag);
  protected
    procedure KeyPressed(Sender : Tobject; value : boolean); override;
  public
    procedure InstallComponent(Params : TParamStrings); override;
    procedure Installed; override;
    property NCMode : TCNCMode read FNCMode;
    procedure Reset; override;
  end;

  TSoftKeySet = class (TCNC)
  private
    KsetH : word;
    procedure Callback(aTag : TAbstractTag);
  public
    procedure InstallComponent(Params : TParamStrings); override;
    procedure Installed; override;
  end;

implementation

constructor TBasicSoftkey.Create(Aowner : Tcomponent);
var  i : TSoftButton;
     TmpStr : string;
begin
  inherited Create(AOwner);
  ManageEdit := True;
  ParentFont := True;

  Font.Size := 11;

  for i := Low(TSoftButton) to High(TSoftButton) do begin
    Button[i] := TCNCButton.Create(Self);
    Button[i].parent     := Self;
    Button[i].tag        := VK_F1 + Ord(i);
    Button[i].text       := '';
    Button[i].bevelWidth := 2;
    Button[i].tabStop    := False;
    Button[i].onPressed  := KeyPressed;
//    Button[i].parentfont := True;
    TmpStr := 'F' + inttostr(Ord(i) + 1);
    Button[i].BitMapOn.LoadFromResourceName(Hinstance, TmpStr + 'ON');
    Button[i].BitMapOff.LoadFromResourceName(Hinstance, TmpStr + 'OFF');
  end;

  Esc  := TCNCButton.Create(Self);
  Esc.parent        := self;
//  Esc.ParentFont    := True;
  Esc.tag           := VK_ESCAPE;
  Esc.text          := 'R';
  Esc.bevelWidth    := 2;
  Esc.tabStop       := False;
  Esc.value         := True;
  Esc.onPressed     := KeyPressed;

  Switch := TCNCButton.Create(Self);
  Switch.parent     := self;
//  Switch.ParentFont := True;
  Switch.tag        := VK_F9;
  Switch.text       := '*';
  Switch.bevelWidth := 2;
  Switch.tabStop    := False;
  Switch.onPressed  := KeyPressed;
end;

procedure TBasicSoftkey.Installed;
var  i : TSoftButton;
begin
  inherited Installed;
  ParentFont := True;
  Esc.FontName := Font.Name;
  Esc.FontStyle := Font.Style;

  Switch.FontName := Font.Name;
  Switch.FontStyle := Font.Style;

  for i := Low(TSoftButton) to High(TSoftButton) do begin
    Button[I].FontName := Font.Name;
    Button[I].FontStyle := Font.Style;
  end;
end;

procedure TBasicSoftkey.KeyChange(KSetH : word);
begin
   if not Find(KSetH, AltKeys) then Exit;

  OpenHandle := KSetH;
  AltEnabled := True;
  CurrentFunction := btnF1;
end;

{ If the KeySet passed is the active keyset then close to toplevel }
procedure TBasicSoftkey.KeyClose(KSetH : word);
begin
  if KSetH = openHandle then begin
    AltEnabled := False;
//    MainEnabled := True;
    CurrentFunction := btnF1;
  end;
end;

procedure TBasicSoftkey.resize;
var
  I : TSoftButton;
  SubButtonWidth : Integer;
begin
  SubButtonWidth := Width div 17;

  Esc.width := SubButtonWidth;
  Esc.Left  := 0;
  Esc.top   := 0;
  Esc.height:= height;

  Switch.width := SubButtonWidth;
  Switch.left  := Width - SubButtonWidth;
  Switch.top   := 0;
  Switch.height:= height;

  for i := Low(TSoftButton) to High(TSoftButton) do begin
    Button[i].width  := (Width - (SubButtonWidth * 2)) div 8;
    Button[i].left   := ( (  (  Width - (  SubButtonWidth * 2)  ) * Ord(i) )
                          div (Ord(High(TSoftButton)) + 1) ) + SubButtonWidth;
    Button[i].top    := 0;
    Button[i].height := Height;
  end;
end;

procedure TBasicSoftkey.SetEnabled(aEnabled : Boolean);
var
  i : TSoftButton;
begin
  if aEnabled = FEnabled then exit;
  FEnabled := aEnabled;

  for i := Low(TSoftButton) to High(TSoftButton) do
    Button[i].Enabled := FEnabled;

  Esc.Enabled := FEnabled;
  Switch.Enabled := FEnabled;
end;

procedure TBasicSoftkey.keyEvent(key : integer; Edge : TEdgeType);
var
  i : integer;
begin
  i := key - VK_F1;
  case key of
    VK_F1..VK_F8: Button[TSoftButton(i)].value := edge = edgeRising;
    VK_F9       : Switch.value    := edge = edgeRising;
    VK_Escape   : Esc.value       := edge = edgeRising;
  end;
end;



// Button Display
destructor TButtonDisplay.destroy;
//var  i : integer;
begin
{  for i := 0 to 7 do begin
    Button[i].free;
  end; }
  inherited destroy;
end;

procedure TButtonDisplay.InstallComponent(Params : TParamStrings);
var
  i : TSoftButton;
  j : TSoftButton;
  nin  : TInterlock;
  keystr : string;
begin
  inherited installComponent(Params);

  MainKeys := Params.ReadKeys;
  nin.Active := False;

  for i := Low(TSoftButton) to High(TSoftButton) do begin
    keystr := 'F' + inttostr(Ord(i) + 1);
    Keys[i] := Params.ReadPrefixKeys(keystr);
    for j := Low(TSoftButton) to High(TSoftButton) do begin
      with Keys[i][j] do begin
        if func <> '' then begin
          Key := TagPublish(Func, TMethodTag);
        end;
      end;
    end;
  end;
end;

procedure TButtonDisplay.keyPressed(sender : TObject; value : boolean);
var i    : TSoftButton;
begin
  if sender is TCNCButton then begin
    i := TSoftButton(TCNCButton(sender).tag - VK_F1);

    case TCNCButton(sender).tag of
      VK_F1..VK_F8: begin
        if not Button[i].Enabled then Exit;
        if MainEnabled then begin
          if not value then begin
            MainEnabled := False;
            CurrentFunction := i;
          end;
        end else if AltEnabled then begin
          with Altkeys[i] do
            if Key <> nil then begin
              Key.AsBoolean := Value;
            end;
        end else begin
          with keys[CurrentFunction][i] do
            if Key <> nil then begin
              Key.AsBoolean := Value;
            end;
        end;
      end;
      VK_F9:  begin
              Reset;
              end;
      VK_ESCAPE : begin
                if not Value then begin
                  SysObj.Layout.Normal;
                  AltEnabled := False;
                  MainEnabled := True;
                  CurrentFunction := btnF1;
                end;
              end;
    end;
  end;
end;


procedure TButtonDisplay.FunctionChange(func : TSoftButton);
var
  i : TSoftButton;
begin
  FCurrentFunction := func;

  if MainEnabled then begin
    for i := Low(TSoftButton) to High(TSoftButton) do begin
      Button[i].text := MainKeys[i].name;
    end;
  end else if AltEnabled then begin
    for i := Low(TSoftButton) to High(TSoftButton) do begin
      Button[i].text := AltKeys[i].name;
    end;
  end else begin
    for i := Low(TSoftButton) to High(TSoftButton) do begin
      Button[i].text := keys[func][i].name;
    end;
  end;
end;

procedure TModalSoftkey.FunctionChange(func : TSoftButton);
var i : TSoftButton;
begin
  FCurrentFunction := Func;
  if AltEnabled then begin
    for i := Low(TSoftButton) to High(TSoftButton) do
      Button[i].text := AltKeys[i].name
  end else
    for i := Low(TSoftButton) to High(TSoftButton) do
      Button[i].Text := Keys[NCMode][i].Name;
end;


procedure TModalSoftkey.KeyPressed(Sender : Tobject; value : boolean);
  procedure WriteKey(aTag : TAbstractTag; Value : Integer);
  begin
    aTag.AsInteger := Value;
  end;

var i    : TSoftButton;
begin
  if Sender is TCNCButton then begin
    i := TSoftButton(TCNCButton(sender).tag - VK_F1);

    case TCNCButton(sender).tag of
      VK_F1..VK_F8: begin
        if not Button[i].Enabled then Exit;

        if AltEnabled then begin
          with Altkeys[i] do
            if Key <> nil then begin
              WriteKey(Key, Ord(value));
            end;
        end else
          with Keys[NCMode][i] do
            if Key <> nil then begin
              WriteKey(Key, Ord(value));
            end;
      end;
      VK_F9: begin
                Reset;
                if not Value  then
                  SysObj.Layout.Swap(nil); { ????? Enable swap}
             end;

      VK_ESCAPE : begin
                if not Value then begin
                  AltEnabled := False;
                  SysObj.Layout.Normal;
                  CurrentFunction := btnF1;
                end;
              end;
    end;
  end;
end;

procedure TModalSoftkey.InstallComponent(Params : TParamStrings);
var
  i : TCNCMode;
  j : TSoftButton;
  nin  : TInterlock;
const KeyNames : array [TCNCMode] of pChar = (
               'Manual',
               'Auto',
               'MDI',
               'TeachIn',
               'Edit'
      );
begin
  inherited InstallComponent(Params);

  nin.Active := False;

  for I := Low(TCNCMode) to High(TCNCMode) do begin
    Keys[I] := Params.ReadPrefixKeys(KeyNames[i]);
    for J := Low(TSoftButton) to High(TSoftButton) do begin
      with Keys[I][J] do begin
        if func <> '' then begin
          Key := SysObj.Comms.TagPublish(Func, TMethodTag);
        end;
      end;
    end;
  end;
end;

procedure TModalSoftkey.Installed;
begin
  inherited Installed;
  TagEvent(  Format(NCPublishedF[ncpActiveMode].Tag, [1]),
                         EdgeChange,
                         NCPublishedF[ncpActiveMode].Size,
                         ModeChange);
end;

procedure TModalSoftkey.ModeChange(aTag : TAbstractTag);
var I : TSoftButton;
begin
  FNCMode := SysObj.NC.OrdinalNCMode;
  AltEnabled := False;
  for I := Low(TSoftButton) to High(TSoftButton) do 
    Button[I].Down := False;
  CurrentFunction := btnF1;
  SysObj.Layout.Normal;
end;

procedure TSoftKeySet.InstallComponent(Params : TParamStrings);
begin
  inherited InstallComponent(Params);
  KsetH := SysObj.SoftKey.RegisterKeySet(Params.ReadKeys);
end;

procedure TSoftKeySet.Installed;
begin
  inherited Installed;
  TagEvent(Name, edgeFalling, TMethodTag, Callback);
end;

procedure TSoftKeySet.Callback(aTag : TAbstractTag);
begin
  SysObj.SoftKey.KeyChange(KsetH);
end;


procedure TBasicSoftkey.Reset;
begin
  AltEnabled := False;
  CurrentFunction := btnF1;
end;

procedure TModalSoftkey.Reset;
begin
  AltEnabled := False;
  CurrentFunction := btnF1;
  MainEnabled:=True
end;

initialization
  RegisterCNCClass(TButtonDisplay);
  RegisterCNCClass(TModalSoftkey);
  RegisterCNCClass(TSoftKeySet);
end.
