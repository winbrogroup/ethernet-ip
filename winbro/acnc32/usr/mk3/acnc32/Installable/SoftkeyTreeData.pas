unit SoftkeyTreeData;

interface

uses Classes, SysUtils, CoreCNC, CNC32, CNCTypes, Graphics, StreamTokenizer,
     Controls, AcncScript, ScriptFunctions, ScriptTypeInfo, FaultRegistration,
     Math, ImgList;

type
  { OnActiveSetChange is used by any sub entity to indicate a change  }
  T_Softkey = class;
  T_SoftkeySet = class;
  T_SoftkeyTree = class;
  T_SoftkeyDisplay = class;
  T_SoftkeyAction = class;

  TTagWrapper = class
  private
    FTagName: string;
    FTag: TAbstractTag;
    procedure SetTag(const Value: TAbstractTag);
  public
    constructor Create(ATagName: string);
    property TagName: string read FTagName;
    property Tag: TAbstractTag read FTag write SetTag;
  end;

  T_SoftkeyTree = class
  private
    FShift: Boolean;
    FControl: Boolean;
    FCount: Integer;
    FBackground: TColor;
    FForeground: TColor;
    FBright: TColor;
    FDark: TColor;

    FImageWidth: Integer;
    FImageHeight: Integer;
    FSoftkeySets: TStringList;
    FImages: TImageList;
    FScriptLibrary: TACNCScriptProject;
    FTagOwner: TCNC;
    FTags: TStringList;
    FBasekey: Integer;
    FFontsize: Integer;
    FTempTag: TIntegerTag;

    FDefaultImages: TImageList;
    FPressedImages: TImageList;
    FDisabledImages: TImageList;

    FActiveSet: T_SoftkeySet;
    FOnActiveSetChange: TNotifyEvent;
    FUseIndicators: Boolean;
    function GetSoftKeySet(Index: Integer): T_Softkeyset;
    function GetTag(Index: Integer): TTagWrapper;
    procedure SetActiveSet(const Value: T_SoftkeySet);
  public
    constructor Create(ATagOwner: TCNC);
    destructor Destroy; override;
    procedure Read(ST: TStreamTokenizer);
    procedure Compile;
    procedure Clean;
    procedure SetActiveSetByName(const Name: string);

    property Images: TImageList read FImages;
    property SoftkeySets[Index: Integer]: T_SoftkeySet read GetSoftkeySet;
    function SoftkeySetCount: Integer;
    property Tags[Index: Integer]: TTagWrapper read GetTag;
    function TagCount: Integer;
    property ActiveSet: T_SoftkeySet read FActiveSet write SetActiveSet;
    property OnActiveSetChange: TNotifyEvent read FOnActiveSetChange write FOnActiveSetChange;
    procedure Reset;
    property Count: Integer read FCount;
    property Basekey: Integer read FBasekey;
    property Background: TColor read FBackground;
    property Bright: TColor read FBright;
    property Dark: TColor read FDark;
    property Foreground: TColor read FForeground;
    property Fontsize: Integer read FFontsize;
    procedure LinktoChange(const Disp: string);
    procedure LightChange(ATag: TAbstractTag);
    property UseIndicators: Boolean read FUseIndicators;
    property Shift: Boolean read FShift;
    property Control: Boolean read FControl;
    property DefaultImages: TImageList read FDefaultImages;
    property PressedImages: TImageList read FPressedImages;
    property DisabledImages: TImageList read FDisabledImages;
  end;

  T_SoftkeySet = class
  private
    FLinkto: TAbstractCNC;
    FLight: TTagWrapper;
    FDisable: TTagWrapper;
    FSoftkeys: TStringList;
    FSoftkeyTree: T_SoftkeyTree;
    FName: string;
    FTitle: string;
    FDisabled: Boolean;
    FLatched: Boolean;
    FAction: T_SoftkeyAction;
    function GetSoftkey(Index: Integer): T_Softkey;
  public
    constructor Create(ATree: T_SoftkeyTree; AName: string);
    destructor Destroy; override;
    procedure Read(ST: TStreamTokenizer);
    procedure Compile;
    procedure Clean;
    property Softkeys[Index: Integer]: T_Softkey read GetSoftkey;
    function SoftkeyCount: Integer;
    property Disable: TTagWrapper read FDisable;
    property Name: string read FName;
    property Title: string read FTitle;
    property Linkto: TAbstractCNC read FLinkto;
    property SoftkeyTree: T_SoftkeyTree read FSoftkeyTree;
    procedure CheckDisabled;
    property Disabled: Boolean read FDisabled;
    property Light: TTagWrapper read FLight;
    property Latched: Boolean read FLatched;
    property Action: T_SoftkeyAction read FAction;
  end;

  T_Softkey = class
  private
    FKey: Integer;
    FLight: TTagWrapper;
    FDisable: TTagWrapper;
    FDisabled: Boolean;
    FAction: T_SoftkeyAction;
    FDisplayList: TStringList;
    FHelp: string;
    FSoftkeyset: T_SoftkeySet;
    function GetDisplay(Index: Integer): T_SoftkeyDisplay;
    function GetActiveDisplay: T_SoftkeyDisplay;
  public
    constructor Create(ASoftkeySet: T_SoftkeySet; AKey: Integer);
    destructor Destroy; override;
    procedure Read(ST: TStreamTokenizer);
    procedure Compile;
    procedure Clean;
    property Display[Index: Integer]: T_SoftkeyDisplay read GetDisplay;
    property ActiveDisplay: T_SoftkeyDisplay read GetActiveDisplay;
    function DisplayCount: Integer;
    property Action: T_SoftkeyAction read FAction;
    property Disable: TTagWrapper read FDisable;
    property Light: TTagWrapper read FLight;
    property Key: Integer read FKey;
    property SoftkeySet: T_SoftkeySet read FSoftkeySet;
    property Disabled: Boolean read FDisabled;
    procedure CheckDisabled;

    procedure FireEvent(Value: Boolean);
  end;

  TSoftkeyLockState = (
    sklsNone,
    sklsUp,
    sklsDown
  );

  T_SoftkeyDisplay = class
  private
    FIndex: Integer;
    FBackground: TColor;
    FForeground: TColor;
    FDisabledColor: TColor;
    FBright: TColor;
    FDark: TColor;

    FText: string;
    FImageIndex: Integer;
    FStretch: Boolean;
    FLeft: Integer;
    FTop: Integer;
    FSoftkey: T_Softkey;
    FLockState: TSoftkeyLockState;
  public
    constructor Create(ASoftkey: T_Softkey; AIndex: Integer);
    procedure Compile;
    procedure Read(ST: TStreamTokenizer);
    property Background: TColor read FBackground;
    property Foreground: TColor read FForeground;
    property Bright: TColor read FBright;
    property Dark: TColor read FDark;
    property DisabledColor: TColor read FDisabledColor;
    property Text: string read FText;
    property Lockstate: TSoftkeyLockState read FLockState;
    property ImageIndex: Integer read FImageIndex;
  end;

  TActionType = (
    atSelf,
    atNamed,
    atSystem,
    atScript,
    atError
  );

  T_SoftkeyAction = class
  private
    FTag: TAbstractTag;
    FKeySet: T_SoftkeySet;
    FDefinition: string;
    FActionType: TActiontype;
    FSoftkeySet: T_SoftkeySet;
    procedure SetTag(const Value: TAbstractTag);
  public
    constructor Create(ASoftkeySet: T_SoftkeySet; ADefinition: string);
    destructor Destroy; override;
    property Definition: string read FDefinition;
    procedure Clean;
    property Tag: TAbstractTag read FTag write SetTag;
    property ActionType: TActiontype read FActionType write FActionType;
    procedure FireEvent(Value: Boolean);
    property SoftkeySet: T_SoftkeySet read FSoftkeySet;
  end;

function FractionColor(Base: TColor; Target: TColor; Swing: Double): TColor;

implementation


{ T_SoftkeyTree }

procedure T_SoftkeyTree.Clean;
begin
  while FSoftkeySets.Count > 0 do begin
    Self.SoftkeySets[0].Free;
    FSoftkeySets.Delete(0);
  end;

  while FTags.Count > 0 do begin
    GetTag(0).Free;
    FTags.Delete(0);
  end;

  FScriptLibrary.Free;
  FImages.Free;
  FTempTag.Free;
  FDefaultImages.Free;
  FPressedImages.Free;
  FDisabledImages.Free;

  FOnActiveSetChange:= nil;
end;

procedure T_SoftkeyTree.Compile;
var I: Integer;
begin
  for I:= 0 to SoftkeySetCount - 1 do begin
    SoftkeySets[I].Compile;
  end;
  FForeground:= ColorToRGB(FForeground);
  FBackground:= ColorToRGB(FBackground);
  FBright:= FractionColor(FBackground, clWhite, 0.80);
  FDark:= FractionColor(FBackground, clBlack, 0.80);

//  try
    if FScriptLibrary.Units.Count > 0 then begin
      try
        FScriptLibrary.Compile('main');
        FScriptLibrary.TTL:= 10000;
        FScriptLibrary.Run;
      except
        on E : Exception do
          raise ECNCInstallFault.CreateFmt('Failed to compile script library: [%s]', [E.Message]);
      end;
    end;
{  except
    on E: Exception do
      SysObj.FaultInterface.SetFault(FTagOwner, CoreFaults[cfScriptFatal], [E.Message], nil)
  end; }
end;

constructor T_SoftkeyTree.Create(ATagOwner: TCNC);
begin
  FTagOwner:= ATagOwner;
  FSoftkeySets:= TStringList.Create;
  FScriptLibrary:= TACNCScriptProject.Create(FTagOwner);
  FScriptLibrary.RTL := TScriptRTL.Create(FTagOwner);
  FScriptLibrary.OwnerCleans:= True;
  FTempTag:= TIntegerTag.Create(FTagOwner, '__SK__');

  FTags:= TStringList.Create;

  FImageWidth:= 48;
  FImageHeight:= 48;
  FCount:= 8;
  FBackground:= Graphics.clLtGray;
  FForeGround:= Graphics.clBlack;
  FFontsize:= 10;

  FImages:= TImageList.CreateSize(FImageWidth, FImageHeight);
end;

destructor T_SoftkeyTree.Destroy;
begin
  FScriptLibrary.RTL.CleanUp;
  FScriptLibrary.Clean;
  Clean;
  inherited;
end;

function T_SoftkeyTree.GetSoftKeySet(Index: Integer): T_Softkeyset;
begin
  Result:= T_SoftkeySet(Self.FSoftkeySets.Objects[Index]);
end;

function T_SoftkeyTree.GetTag(Index: Integer): TTagWrapper;
begin
  Result:= TTagWrapper(Self.FTags.Objects[Index]);
end;

procedure T_SoftkeyTree.LightChange(ATag: TAbstractTag);
var I: Integer;
    SK: T_SoftkeySet;
begin
  for I:= 0 to SoftkeySetCount - 1 do begin
    SK:= SoftkeySets[I];
    if Assigned(SK.Light) then begin
      if ATag = SK.Light.Tag then begin
        if not SK.Latched and not ATag.AsBoolean then begin
          ActiveSet:= SK;
          Exit;
        end else if SK.Latched and ATag.AsBoolean then begin
          ActiveSet:= SK;
          Exit;
        end;
      end;
    end;
  end;
end;

procedure T_SoftkeyTree.LinktoChange(const Disp: string);
var I: Integer;
begin
  for I:= 0 to SoftkeySetCount - 1 do begin
    if Assigned(SoftkeySets[I].FLinkto) then begin
      if CompareText(Disp, SoftkeySets[I].FLinkto.Name) = 0 then begin
        ActiveSet:= SoftkeySets[I];
        Exit;
      end;
    end;
  end;
end;

procedure T_SoftkeyTree.Read(ST: TStreamTokenizer);
var Token: string;

  function ReadImageStrip: TImageList;
    function LoadImage(const fname: string): TBitmap;
    begin
      Result:= TBitmap.Create;
      Result.Transparent:= True;
      //Result.TransparentColor:= clWhite;
      Result.TransparentMode:= tmAuto;
      Result.LoadFromFile(ProfilePath + 'live/images/' + fname);
    end;

  begin
    ST.ExpectedTokens(['=', '{']);
    Result:= TImageList.Create(FTagOwner);
    Result.DrawingStyle:= dsTransparent;
    Result.Height:= 16;
    Result.Width:= 16;

    Token:= Lowercase(ST.Read);
    while not ST.EndOfStream do begin
      if Token = 'width' then
        Result.Width:= ST.ReadIntegerAssign
      else if Token = 'height' then
        Result.Height:= ST.ReadIntegerAssign
      else if Token = 'filename' then
        Result.Add(LoadImage(ST.ReadStringAssign), nil)
      else if Token = '}' then
        Exit
      else
        raise ECNCInstallFault.CreateFmt('Unexpected token [%s]', [Token]);
      Token:= Lowercase(ST.Read);
    end;
    raise ECNCInstallFault.Create('Unexpected end of stream');
  end;

  procedure ReadTags;
  begin
    ST.ExpectedTokens(['=', '{']);
    Token:= ST.Read;
    while not ST.EndOfStream do begin
      if Token = '}' then
        Exit;
      FTags.AddObject(Token, TTagWrapper.Create(Token));
      Token:= ST.Read;
    end;
  end;

  procedure ReadSoftkeySet;
  var N: string;
      SKS: T_SoftkeySet;
  begin
    ST.ExpectedToken('[');
    N:= ST.Read;
    ST.ExpectedToken(']');
    ST.ExpectedTokens(['=', '{']);

    SKS:= T_SoftkeySet.Create(Self, N);
    SKS.FTitle:= N;
    Self.FSoftkeySets.AddObject(Lowercase(SKS.FName), SKS);
    SKS.Read(ST);
  end;

  procedure ReadScriptLibrary;
  var SS: TStringStream;
      Src: TStream;
      C: Char;
      Script: TAcncScript;
  begin
    ST.ExpectedTokens(['=', '{']);
    Src:= ST.GetStream;
    SS:= TStringStream.Create('');

    while True do begin
      Src.Read(C, 1);
      if C = '}' then
        Break;
      SS.Write(C, 1);
      if Src.Position >= Src.Size then
        raise ECNCInstallFault.Create('Unexpected end of stream');
    end;
    // Eat the stream until closing brace is reached

    Script := TACNCScript.Create(FTagOwner);
    Script.Name := 'SoftkeyTree';
    Script.Text := SS.DataString;
    Script.Project := FScriptLibrary;
    FScriptLibrary.Units.Add(Script);
  end;

  procedure ReadDefine;
  var DefName, DefValue: string;
  begin
    DefName:= ST.Read;
    DefValue:= ST.Read;
    ST.AddDefine(DefName, DefValue);
  end;

begin
  ST.ExpectedTokens(['softkeytree', '=', '{']);
  Token:= Lowercase(ST.Read);
  while not ST.EndOfStream do begin
    if token = 'shift' then
      FShift:= ST.ReadBooleanAssign
    else if token = 'control' then
      FControl:= ST.ReadBooleanAssign
    else if token = 'count' then
      FCount:= ST.ReadIntegerAssign
    else if token = 'basekey' then
      FBasekey:= ST.ReadIntegerAssign
    else if token = 'background' then
      FBackground:= ST.ReadColorAssign
    else if token = 'foreground' then
      FForeground:= ST.ReadColorAssign
    else if token = 'fontsize' then
      FFontSize:= ST.ReadIntegerAssign
    else if token = 'tags' then
      ReadTags
    else if token = 'softkeyset' then
      ReadSoftkeySet
    else if token = 'scriptlibrary'then
      ReadScriptLibrary
    else if token = 'useindicators' then
      FUseIndicators:= ST.ReadBooleanAssign
    else if Token = 'defaultimages' then
      FDefaultImages:= ReadImageStrip
    else if Token = 'pressedimages' then
      FPressedImages:= ReadImageStrip
    else if Token = 'disabledimages' then
      FDisabledImages:= ReadImageStrip
    else if Token = 'define' then
      ReadDefine
    else if Token = '}' then
      Exit
    else
      raise ECNCInstallFault.CreateFmt('Unexpected token [%s]', [Token]);
    Token:= Lowercase(ST.Read);
  end;
  raise ECNCInstallFault.Create('Unexpected end of stream');
end;

procedure T_SoftkeyTree.Reset;
var I: Integer;
    SK: T_SoftkeySet;
begin
  for I:= 0 to SoftkeySetCount - 1 do begin
    SK:= SoftkeySets[I];
    if Assigned(SK.Light) then begin
      if SK.Latched and SK.Light.Tag.AsBoolean then begin
        ActiveSet:= SK;
        Exit;
      end;
    end;
  end;
  ActiveSet:= SoftkeySets[0];
end;

procedure T_SoftkeyTree.SetActiveSet(const Value: T_SoftkeySet);
begin
  if FActiveSet <> Value then begin

    if Assigned(ActiveSet) and Assigned(FActiveSet.Action) then
      FActiveSet.Action.FireEvent(False);

    FActiveSet := Value;

    if Assigned(Self.FOnActiveSetChange) then
      FOnActiveSetChange(Self);

    if Assigned(ActiveSet) and Assigned(FActiveSet.Action) then
      FActiveSet.Action.FireEvent(True);
  end;
end;

procedure T_SoftkeyTree.SetActiveSetByName(const Name: string);
var I: Integer;
    T: T_SoftkeySet;
begin
  for I:= 0 to SoftkeySetCount - 1 do begin
    T:= SoftKeySets[I];
    if Uppercase(T.FName) = Uppercase(Name) then begin
      SetActiveSet(T);
      CoreCNC.EventLog('Firing softkeyset: ' + Name);
      Exit;
    end;
  end;
  CoreCNC.EventLog('Unknown softkeyset: ' + Name);
end;

function T_SoftkeyTree.SoftkeySetCount: Integer;
begin
  Result:= FSoftkeySets.Count;
end;

function T_SoftkeyTree.TagCount: Integer;
begin
  Result:= FTags.Count;
end;

{ T_SoftkeySet }

procedure T_SoftkeySet.CheckDisabled;
var Tmp: Boolean;
begin
  Tmp:= FDisabled;
  if Assigned(Self.Disable) then begin
    FDisabled:= Disable.Tag.AsBoolean;
  end;
  if FDisabled <> Tmp then
    if Assigned(SoftkeyTree.OnActiveSetChange) then
      SoftkeyTree.OnActiveSetChange(Self);
end;

procedure T_SoftkeySet.Clean;
begin
  FLinkto:= nil;
  FLight.Free;
  FDisable.Free;
  FSoftkeyTree:= nil;
  
  while Self.SoftkeyCount > 0 do begin
    Self.Softkeys[0].Free;
    FSoftkeys.Delete(0);
  end;
end;

procedure T_SoftkeySet.Compile;
var I: Integer;
begin
  for I:= 0 to SoftkeyCount - 1 do begin
    Softkeys[I].Compile;
  end;
end;

constructor T_SoftkeySet.Create(ATree: T_SoftkeyTree; AName: string);
begin
  FSoftkeys:= TStringList.Create;
  FSoftkeyTree:= ATree;
  FName:= AName;
end;

destructor T_SoftkeySet.Destroy;
begin
  Clean;
  inherited;
end;

function T_SoftkeySet.GetSoftkey(Index: Integer): T_Softkey;
begin
  Result:= T_Softkey(Self.FSoftkeys.Objects[Index]);
end;

procedure T_SoftkeySet.Read(ST: TStreamTokenizer);
var Token: string;

  procedure ReadSoftkey;
  var SK: T_Softkey;
      Ind: Integer;
  begin
    ST.ExpectedToken('[');
    Ind:= ST.ReadInteger;
    ST.ExpectedToken(']');
    ST.ExpectedTokens(['=', '{']);

    SK:= T_Softkey.Create(Self, Ind);
    Self.FSoftkeys.AddObject('', SK);
    SK.Read(ST);
  end;

begin
  Token:= Lowercase(ST.Read);
  while not ST.EndOfStream do begin
    if Token = 'linkto' then
      FLinkto:= CoreCNC.SysObj.Comms.GetComponent(ST.ReadStringAssign)
    else if Token = 'light' then
      FLight:= TTagWrapper.Create(ST.ReadStringAssign)
    else if Token = 'title' then
      FTitle:= ST.ReadStringAssign
    else if Token = 'disable' then
      FDisable:= TTagWrapper.Create(ST.ReadStringAssign)
    else if Token = 'softkey' then
      ReadSoftkey
    else if Token = 'latched' then
      FLatched:= ST.ReadBooleanAssign
    else if Token = 'action' then
      FAction:= T_SoftkeyAction.Create(Self, ST.ReadStringAssign)
    else if Token = '}' then
      Exit
    else
      raise ECNCInstallFault.CreateFmt('Unexpected token [%s]', [Token]);
    Token:= Lowercase(ST.Read);
  end;
  raise ECNCInstallFault.Create('Unexpected end of stream');
end;

function T_SoftkeySet.SoftkeyCount: Integer;
begin
  Result:= FSoftkeys.Count;
end;

{ T_Softkey }

procedure T_Softkey.CheckDisabled;
var Tmp: Boolean;
    D: Boolean;
begin
  Tmp:= FDisabled;
  D:= False;
  if Assigned(Disable) then begin
    D:= Disable.Tag.AsBoolean;
  end;

  FDisabled:= D or SoftkeySet.Disabled;

  if Disabled <> Tmp then
    if Assigned(SoftkeySet.SoftkeyTree.OnActiveSetChange) then
      SoftkeySet.SoftkeyTree.OnActiveSetChange(Self);
end;

procedure T_Softkey.Clean;
begin
  FLight.Free;
  FDisable.Free;
  FAction.Free;
  while Self.DisplayCount > 0 do begin
    Self.Display[0].Free;
    FDisplayList.Delete(0);
  end;
end;

procedure T_Softkey.Compile;
var I: Integer;
begin
  if Assigned(Action) then begin
    case Action.ActionType of
      atSelf: begin
        I:= FSoftkeyset.FSoftkeyTree.FSoftkeySets.IndexOf(Action.FDefinition);
        if I <> -1 then begin
          Action.FKeySet:= FSoftkeyset.FSoftkeyTree.SoftkeySets[I];
        end
        else
          Action.ActionType:= atError;
      end;
    end;
  end;

  for I:= 0 to Self.DisplayCount - 1 do begin
    Display[I].Compile;
  end;
end;

constructor T_Softkey.Create(ASoftkeySet: T_SoftkeySet; AKey: Integer);
begin
  FSoftkeySet:= ASoftkeySet;
  FKey:= AKey;
  FLight:= nil;
  FAction:= nil;
  FDisplayList:= TStringList.Create;
  FHelp:= '';
end;

destructor T_Softkey.Destroy;
begin
  Clean;
  inherited;
end;

function T_Softkey.DisplayCount: Integer;
begin
  Result:= FDisplayList.Count;
end;

procedure T_Softkey.FireEvent(Value: Boolean);
begin
  if Assigned(FAction) then
    Action.FireEvent(Value);
end;

function T_Softkey.GetActiveDisplay: T_SoftkeyDisplay;
var I: Integer;
    Tmp: Integer;
begin
  if Assigned(Light) and Assigned(Light.Tag) then begin
    Tmp:= Light.Tag.AsInteger;

    for I:= 0 to DisplayCount - 1 do begin
      Result:= Display[I];
      if Result.FIndex = Tmp then
        Exit;
    end;
  end;
  Result:= Display[0];
end;

function T_Softkey.GetDisplay(Index: Integer): T_SoftkeyDisplay;
begin
  Result:= nil;
  if FDisplayList.Count = 0 then
    Exit;

  if Index >= FDisplayList.Count then
    Index:= 0;

  Result:= T_SoftkeyDisplay(FDisplayList.Objects[Index]);
end;

procedure T_Softkey.Read(ST: TStreamTokenizer);
var Token: string;

  procedure ReadDisplay;
  var Ind: Integer;
      Disp: T_SoftkeyDisplay;
  begin
    ST.ExpectedToken('[');
    Ind:= ST.ReadInteger;
    ST.ExpectedToken(']');
    ST.ExpectedTokens(['=', '{']);
    Disp:= T_SoftkeyDisplay.Create(Self, Ind);
    FDisplayList.AddObject('', Disp);
    Disp.Read(ST);
  end;

begin
  Token:= Lowercase(ST.Read);
  while not ST.EndOfStream do begin
    if Token = 'light' then
      FLight:= TTagWrapper.Create(ST.ReadStringAssign)
    else if Token = 'action' then
      FAction:= T_SoftkeyAction.Create(SoftkeySet, ST.ReadStringAssign)
    else if Token = 'disable' then
      FDisable:= TTagWrapper.Create(ST.ReadStringAssign)
    else if Token = 'help' then
      FHelp:= ST.ReadStringAssign
    else if Token = 'display' then
      ReadDisplay
    else if Token = '}' then
      Exit
    else
      raise ECNCInstallFault.CreateFmt('Unexpected token [%s]', [Token]);
    Token:= Lowercase(ST.Read);
  end;
  raise ECNCInstallFault.Create('Unexpected end of stream');
end;

{ T_SoftkeyDisplay }

{ Compiles the extra colours used for display }
procedure T_SoftkeyDisplay.Compile;
begin
  FForeground:= ColorToRGB(FForeground);
  FBackground:= ColorToRGB(FBackground);
  FDisabledColor:= FractionColor(FForeground, FBackground, 0.75);
  FBright:= FractionColor(FBackground, clWhite, 0.80);
  FDark:= FractionColor(FBackground, clBlack, 0.80);
end;

constructor T_SoftkeyDisplay.Create(ASoftkey: T_Softkey; AIndex: Integer);
begin
  FSoftkey:= ASoftkey;
  FIndex:= AIndex;
  FBackground:= FSoftkey.FSoftkeyset.FSoftkeyTree.FBackground;
  FForeground:= FSoftkey.FSoftkeyset.FSoftkeyTree.FForeground;
  FText:= '';
  FImageIndex:= -1;
  FLeft:= -1;
  FTop:= -1;
end;

procedure T_SoftkeyDisplay.Read(ST: TStreamTokenizer);
var Token: string;

  procedure ReadImage;
  var BM: TBitMap;
  begin
    BM:= TBitmap.Create;
    BM.LoadFromFile(ProfilePath + 'live/images/' + ST.ReadStringAssign);
    FImageIndex:= FSoftkey.FSoftkeyset.FSoftkeyTree.FImages.Add(BM, nil);
  end;
begin
  FBackground:= FSoftkey.SoftkeySet.SoftkeyTree.Background;
  FForeground:= FSoftkey.SoftkeySet.SoftkeyTree.Foreground;
  Token:= Lowercase(ST.Read);
  while not ST.EndOfStream do begin
    if Token = 'background' then
      FBackground:= ST.ReadColorAssign
    else if Token = 'foreground' then
      FForeground:= ST.ReadColorAssign
    else if Token = 'text' then
      FText:= CoreCNC.SysObj.Localized.GetString( ST.ReadStringAssign )
    else if Token = 'image' then
      ReadImage
    else if Token = 'stretch' then
      FStretch:= ST.ReadBooleanAssign
    else if Token = 'left' then
      FLeft:= ST.ReadIntegerAssign
    else if Token = 'top' then
      FTop:= ST.ReadIntegerAssign
    else if Token = 'lockstate' then
      FLockState:= TSoftkeyLockState(ST.ReadIntegerAssign)
    else if Token = '}' then
      Exit
    else
      raise ECNCInstallFault.CreateFmt('Unexpected token [%s]', [Token]);
    Token:= Lowercase(ST.Read);
  end;
  raise ECNCInstallFault.Create('Unexpected end of stream');
end;

{ TTagWrapper }

constructor TTagWrapper.Create(ATagName: string);
begin
  FTagName:= ATagName;
end;


procedure TTagWrapper.SetTag(const Value: TAbstractTag);
begin
  FTag := Value;
end;

{ T_SoftkeyAction }

procedure T_SoftkeyAction.Clean;
begin

end;

constructor T_SoftkeyAction.Create(ASoftkeySet: T_SoftkeySet; ADefinition: string);
var P: Integer;
    Tmp: string;
begin
  FDefinition:= ADefinition;
  FSoftkeySet:= ASoftkeySet;
  P:= System.Pos(':', FDefinition);
  if(P = 0) then
    raise ECNCInstallFault.CreateFmt('Action Definition is malformed: [%s]', [FDefinition]);
  Tmp:= System.Copy(FDefinition, 1, P - 1);
  System.Delete(FDefinition, 1, P);
  Tmp:= Lowercase(Tmp);
  if Tmp = 'self' then
    Self.FActionType:= atSelf
  else if Tmp = 'named' then
    Self.FActionType:= atNamed
  else if Tmp = 'system' then
    Self.FActionType:= atSystem
  else if Tmp = 'script' then
    Self.FActionType:= atScript
  else
    raise ECNCInstallFault.CreateFmt('Action Definition has malformed type [%s]', [Tmp]);
end;

destructor T_SoftkeyAction.Destroy;
begin
  Clean;
  inherited;
end;
                                              
procedure T_SoftkeyAction.FireEvent(Value: Boolean);
  procedure Fault(const Text: string);
  begin
    SysObj.FaultInterface.SetFault(SoftkeySet.SoftkeyTree.FTagOwner, CoreFaults[cfSysInterlock], [Text], nil);
  end;

var aTag: TAbstractTag;
    Method: TScriptMethodTag;
begin
  case ActionType of
    atSelf: begin
      if not Value then
        FSoftkeyset.FSoftkeyTree.SetActiveSet(FKeySet);
    end;

    atNamed: begin
      Tag.AsBoolean:= Value;
    end;

    atSystem: begin
      if not Value then begin
        if CompareText(FDefinition, 'top') = 0 then begin
          SysObj.Layout.Normal;
          SysObj.Softkey.Reset;
        end

        else if CompareText(FDefinition, 'keytop') = 0 then begin
          SysObj.Softkey.Reset;
        end

        else if CompareText(FDefinition, 'reset') = 0 then begin
          SysObj.Softkey.Reset;
        end

        else if CompareText(FDefinition, 'swap') = 0 then begin
          SysObj.Layout.Swap(nil);
        end
        else
          EventLog('Invalid Key definition: ' + FDefinition);
      end;
    end;

    atScript: begin
      FSoftkeyset.FSoftkeyTree.FTempTag.AsBoolean:= Value;
      aTag:= FSoftkeyset.FSoftkeyTree.FScriptLibrary.FindTypeList(FDefinition, '');
      if Assigned(ATag) then begin
        if aTag is TScriptMethodTag then begin
          Method:= TScriptMethodTag(aTag);
          if Method.Prototype.Count = 1 then begin
            //EventLog('We have found a method');
            try
              FSoftkeyset.FSoftkeyTree.FScriptLibrary.TTL:= 100000;
              //FSoftkeyset.FSoftkeyTree.FScriptLibrary.Rewind;
              FSoftkeyset.FSoftkeyTree.FScriptLibrary.SetEntryMethod(Method);
              FSoftkeyset.FSoftkeyTree.FScriptLibrary.Run(FSoftkeyset.FSoftkeyTree.FTempTag);
              //FSoftkeyset.FSoftkeyTree.FScriptLibrary.RTL.CleanUp;
            except
              on E: Exception do
                SysObj.FaultInterface.SetFault(SoftkeySet.SoftkeyTree.FTagOwner, CoreFaults[cfSysWarning], [E.Message + ' In script method ' + Method.Name], nil);
            end;
          end else
            Fault('Script has wrong signature: ' + FDefinition);
        end else
          Fault('Script does not resolve to a method: ' + FDefinition);
      end else
        Fault('Script does not exist: ' + FDefinition);
    end;
  end;
end;

procedure T_SoftkeyAction.SetTag(const Value: TAbstractTag);
begin
  FTag := Value;
end;


function FractionColor(Base: TColor; Target: TColor; Swing: Double): TColor;
var RB, GB, BB: Double;
    RT, GT, BT: Double;
begin
  RB:= (Base shr 16) and $ff;
  GB:= (Base shr 8) and $ff;
  BB:= (Base shr 0) and $ff;
  RT:= (Target shr 16) and $ff;
  GT:= (Target shr 8) and $ff;
  BT:= (Target shr 0) and $ff;

  RB:= RB + ((RT - RB) * (Swing));
  GB:= GB + ((GT - GB) * (Swing));
  BB:= BB + ((BT - BB) * (Swing));

  Result:= ((Floor(RB) shl 16) + (Floor(GB) shl 8) + Floor(BB));
end;

end.


