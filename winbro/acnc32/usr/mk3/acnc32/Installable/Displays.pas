unit Displays;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, CoreCNC, StdCtrls, IniFiles, Named, CNCUtils, NCNames,
  CNCTypes, CNC32, AbstractEditor, CNCAbs, Tokenizer;

const
  DisplayNotDefined = 'Display %d Not Defined In LayoutManager';

const
  CSys = 0;

type
  TBasicStatus = class(TAbstractDisplay)
  private
    FCurrentMode   : TPanel;
    FCurrentUser   : TPanel;
    FCurrentFile   : TPanel;
    mode_H, user_H : TAbstractTag;
    file_H         : TAbstractTag;
    configure_DH   : array [0..7] of TAbstractTag;
    LED            : TIOLED;
    named          : array [0..7] of string;
  protected
//    procedure CNCReset(aTag : TAbstractTag); override;
  public
    constructor Create(Aowner : TComponent); override;
    procedure InstallComponent (Params : TParamStrings); override;
    procedure Installed; override;
    procedure DataChange(aTag : TAbstractTag);
    procedure Resize; override;
  published
  end;

  TBasicStatusA = class(TBasicStatus)
  public
    procedure InstallComponent(Params : TParamStrings); override;
    procedure Resize; override;
  end;

  TBasicStatusB = class(TBasicStatusA)
  public
    procedure Resize; override;
  end;

  TErrorWindow = class(TAbstractDisplay)
  private
    ErrorList : TMemo;
  protected
  public
    procedure InstallComponent (Params : TParamStrings); override;
    procedure Installed; override;
    procedure dataChange(aTag : TAbstractTag);
    procedure displaySweep(aTag : TAbstractTag);
  published
  end;

  TMPGHandler = class(TCNC)
  private
    mpgInput_IH : TAbstractTag;
    mpgOVR_IH   : TAbstractTag;
    LastMPG   : byte;
    Traverse  : boolean;
    CountsPerClick : Integer;
    procedure   MpgUpdate(aTag : TAbstractTag);
  public
    procedure InstallComponent(Params : TParamStrings); override;
    procedure Installed; override;
  end;

  { A canvas based program display class. Sets callbacks for NCPartProgram and
    and NCProgramLine. Its text is read directly from NC.lines, a local copy
    of the displayed text is maintained for screen update only }
  TFirstProgDisplay = class(TExpandDisplay)
  private
    count        : integer;
    yspacing     : integer;
    FactiveText  : TColor;
    FpassiveText : TColor;
    BM           : TBitMap;
    Rect         : TRect;
    LastLines    : TStrings;
    procedure   UpdateDisplay;
  protected
    procedure   Resize; override;
    procedure   Paint; override;
  public
    constructor Create(Aowner : Tcomponent); override;
    destructor  Destroy; override;
    procedure   InstallComponent(Params : TParamStrings); override;
    procedure   Installed; override;
    procedure   IShutdown; override;
    procedure   lineChange(aTag : TAbstractTag);
  published
  end;

  TNullDisplay = class(TAbstractDisplay)
  end;

implementation

constructor TFirstProgDisplay.create(Aowner : TComponent);
begin
  inherited create(Aowner);
  ParentFont  := False;
  Font.Size   := 12;
  Font.Name   := 'Courier New';
  Font.Style  := [fsBold];
  Canvas.Font := Font;
  BM := TBitMap.Create;
end;

destructor TFirstProgDisplay.destroy;
begin
  BM.Free;
  inherited Destroy;
end;

procedure TFirstProgDisplay.InstallComponent(Params : TParamStrings);
begin
  inherited InstallComponent(Params);

  FactiveText  := Params.ReadColour('activeText',  clYellow);
  FpassiveText := Params.ReadColour('passiveText', clSilver);
  Resize;
end;

procedure TFirstProgDisplay.Installed;
begin
  inherited Installed;
  with NCPublishedF[ncpProgramLine] do
    TagEvent(Format(Tag, [1]), edgeChange, Size, lineChange);
  with NCPublishedF[ncpPartProgram] do
    TagEvent(Format(Tag, [1]), edgeChange, Size, lineChange);
end;

procedure TFirstProgDisplay.LineChange(aTag : TAbstractTag);
begin
  UpdateDisplay;
end;

procedure TFirstProgDisplay.IShutdown;
begin
  inherited IShutdown;
end;

procedure TFirstProgDisplay.Resize;
begin
  inherited resize;
  if installState = installStateIdle then exit;

  BM.width := width;
  BM.height := height;
  BM.canvas.font := font;
  yspacing := BM.canvas.TextHeight('Q');
  count := (clientheight - (bevelWidth * 2)) div yspacing;
  rect.Left := 0;
  rect.Top  := 0;
  rect.Bottom := height;
  rect.Right := width;
end;

procedure TFirstProgDisplay.Paint;
begin
  inherited Paint;
  UpdateDisplay;
end;

procedure TFirstProgdisplay.UpdateDisplay;
var i : integer;
    topline : integer;
    NCLines : TStringList;
    NCProgramLine : Integer;
begin
  NCLines := SysObj.NC.SubRoutineLines[SysObj.NC.ActiveSubRoutine];
  NCProgramLine := SysObj.NC.ProgramLine;

  if Assigned(NCLines) then begin
    if NCLines.Count = 0 then begin
      with BM do begin
        Canvas.Brush.color := color;
        Canvas.FillRect(rect);
      end;
    end else begin
      if NCLines <> LastLines then begin
        LastLines := NCLines;
        with BM do begin
          Canvas.Brush.color := color;
          Canvas.FillRect(rect);
        end;
      end;

      TopLine := 0;
      // attempt centre based
      if NCProgramLine >= (count div 2) then
        Topline := NCProgramLine - (count div 2);
      // Detect if end based
      if NCProgramLine >= (NCLines.count - (count div 2)) then
         TopLine := NCLines.count - count;
      if TopLine < 0 then
        TopLine := 0;

      with BM do begin
        canvas.brush.color := color;
        canvas.FillRect(rect);
        for I := 0 to Count - 1 do begin
          // determine if line to be drawn
          if I + TopLine < NCLines.count then begin
            if (i + TopLine) = NCProgramLine then
              canvas.font.color := FActiveText
            else
              canvas.font.color := FPassiveText;
            Canvas.TextOut(10, bevelWidth + (i * ySpacing), NCLines[i + Topline]);
          end;
        end;
      end;
    end;
    Canvas.Draw(0, 0, BM);
  end;
end;


constructor TBasicStatus.Create(Aowner : TComponent);
begin
  inherited create(Aowner);
  FCurrentMode := TPanel.create(Self);
  FCurrentMode.ParentFont := True;
end;

procedure TBasicStatus.InstallComponent (Params : TParamStrings);
var
  I : Integer;
begin
  font.Size := 12;
  FCurrentMode := TPanel.create(self);
  FCurrentMode.BevelOuter := bvLowered;
  FCurrentMode.Parent     := Self;
  FCurrentMode.Caption    := '';
  
  FCurrentUser   := TPanel.create(self);
  FCurrentUser.BevelOuter := bvLowered;
  FCurrentUser.parent     := Self;
  FCurrentUser.caption    := '';

  FCurrentFile   := TPanel.create(self);
  FCurrentFile.BevelOuter := bvLowered;
  FCurrentFile.parent     := Self;
  FCurrentFile.caption    := '';
  FCurrentFile.ParentFont := True;

  LED := TIOLED.create(Self);
  LED.parent := Self;
  LED.value  := 255;
  LED.value  := 0;
  LED.ID     := '';
  inherited installComponent(Params);
  ManageEdit := True;

  LED.ID := Params.ReadString('LEDtags', '');
  LED.resize;

  for I := 0 to 7 do
    named[I] := Params.ReadString('TagNames' + inttostr(i), '');
end;

procedure TBasicStatus.Installed;
var
  i       : integer;
begin
  inherited Installed;
  User_H := TagEvent(NameAccessUserName, edgeChange, TIntegerTag, dataChange);
  with NCPublishedF[ncpActiveMode] do
    Mode_H := TagEvent(Format(Tag, [1]), edgeChange, Size, DataChange);

  File_H := TagEvent(nameNewActiveFile,  edgeChange, TStringTag, DataChange);

  for I := 0 to 7 do begin
    if named[I] <> '' then begin
      Configure_DH[i] := TagEvent(named[I], edgeChange, TMethodTag, dataChange);
    end;
  end;

  DataChange(User_H);
  DataChange(Mode_H);
  DataChange(File_H);
end;

procedure TBasicStatus.DataChange(aTag : TAbstractTag);
var
  CNCMode    : TCNCMode;
//  UserAccess : TAccessLevel;
  I          : Integer;
begin
  if aTag = nil then Exit;

  if aTag = mode_H then begin
    CNCMode := SysObj.NC.OrdinalNCMode;
    case CNCMode of
      CNCModeManual  : FCurrentMode.caption := SysObj.Localized.GetString('$MANUAL');
      CNCModeAuto    : FCurrentMode.caption := SysObj.Localized.GetString('$AUTO');
      CNCModeMDI     : FCurrentMode.caption := SysObj.Localized.GetString('$MDI');
      CNCModeTeachIn : FCurrentMode.caption := SysObj.Localized.GetString('$TEACHIN');
      CNCModeEdit    : FCurrentMode.caption := SysObj.Localized.GetString('$EDIT');
    else
    end;
    exit;
  end else if aTag = user_H then begin
//   UserAccess := TAccessLevel(aTag.AsInteger);
   FCurrentUser.Caption := aTag.AsString;
{   case UserAccess of
     AccessLevelNone          : FCurrentUser.Caption := NoneAccessText;
     AccessLevelOperator      : FCurrentUser.Caption := OperatorAccessText;
     AccessLevelSupervisor    : FCurrentUser.Caption := SupervisorAccessText;
     AccessLevelProgrammer    : FCurrentUser.Caption := ProgrammerAccessText;
     AccessLevelMaintenance   : FCurrentUser.Caption := MaintenanceAccessText;
     AccessLevelAdministrator : FCurrentUser.Caption := AdministratorAccessText;
     AccessLevelOEM1          : FCurrentUser.Caption := OEM1AccessText;
     AccessLevelOEM2          : FCurrentUser.Caption := OEM2AccessText;
    end;   }
    Exit;
  end else if aTag = file_h then begin
    FCurrentFile.caption := StripPath(aTag.AsString);
    Exit;
  end;

  for i := 0 to 7 do begin
    if IsDualTag(aTag, configure_DH[i]) then begin
      if aTag.AsBoolean then
           LED.value := LED.value or (1 shl i)
      else LED.value := LED.value and (not (1 shl i));
    exit;
    end;
  end;
end;

procedure TBasicStatus.Resize;
begin
  if installState = installStateIdle then exit;
  FCurrentUser.width  := width div 5;
  FCurrentUser.height := height - 4;
  FCurrentUser.left   := 2;
  FCurrentUser.top    := 2;

  FCurrentMode.width  := width div 5;
  FCurrentMode.height := height - 4;
  FCurrentMode.left   := (2 + 2) + FCurrentUser.width;
  FCurrentMode.top    := 2;

  LED.width  := (width div 10) * 3;
  LED.height := height - 4;
  LED.left   := (2 + 2 + 2) + FcurrentUser.width + FCurrentMode.width;
  LED.top    := 2;
  LED.resize;

  FCurrentFile.width  := (width div 10) * 3;
  FCurrentFile.height := height - 4;
  FCurrentFile.left   := (2 + 2 + 2 + 2) +
                      FCurrentUser.width +
                      FcurrentMode.width +
                      LED.width;
  FCurrentFile.top    := 2;
end;

procedure TBasicStatusA.InstallComponent(Params : TParamStrings); 
begin
  inherited;
  FCurrentFile.Alignment := taLeftJustify;
end;

procedure TBasicStatusA.Resize;
begin
  if InstallState = installStateIdle then
    Exit;

  FCurrentUser.Width  := width div 8;
  FCurrentUser.Height := height - 4;
  FCurrentUser.left   := 2;
  FCurrentUser.top    := 2;

  FCurrentMode.width  := width div 8;
  FCurrentMode.height := height - 4;
  FCurrentMode.left   := (2 + 2) + FCurrentUser.width;
  FCurrentMode.top    := 2;

  LED.width  := (width div 8) * 2;
  LED.height := height - 4;
  LED.left   := (2 + 2 + 2) + FcurrentUser.width + FCurrentMode.width;
  LED.top    := 2;
  LED.resize;

  FCurrentFile.width  := (width div 8) * 4;
  FCurrentFile.height := height - 4;
  FCurrentFile.left   := (2 + 2 + 2 + 2) +
                      FCurrentUser.width +
                      FcurrentMode.width +
                      LED.width;
  FCurrentFile.top    := 2;
end;

procedure TBasicStatusB.Resize;
begin
  if InstallState = installStateIdle then
    Exit;

  FCurrentUser.Width  := width div 8;
  FCurrentUser.Height := height - 4;
  FCurrentUser.left   := 2;
  FCurrentUser.top    := 2;

  FCurrentMode.width  := width div 8;
  FCurrentMode.height := height - 4;
  FCurrentMode.left   := (2 + 2) + FCurrentUser.width;
  FCurrentMode.top    := 2;

  {
  LED.width  := (width div 8) * 2;
  LED.height := height - 4;
  LED.left   := (2 + 2 + 2) + FcurrentUser.width + FCurrentMode.width;
  LED.top    := 2;
  LED.resize;
  }
  LED.Visible:= False;

  FCurrentFile.width  := (width div 8) * 6;
  FCurrentFile.height := height - 4;
  FCurrentFile.left   := (2 + 2 + 2 ) +
                      FCurrentUser.width +
                      FcurrentMode.width;
  FCurrentFile.top    := 2;
end;

procedure TErrorWindow.InstallComponent (Params : TParamStrings);
begin
  inherited InstallComponent(Params);
  ErrorList := TMemo.create(Self);
  ErrorList.parent      := Self;
  ErrorList.align       := alClient;
  ErrorList.Font.name   := 'Courier New';
  ErrorList.Font.Style  := [fsBold];
  ErrorList.Font.Size   := 12;
end;

procedure TErrorWindow.Installed;
begin
  inherited Installed;
  TagEvent(NameIOFaultChange, edgeChange, TIntegerTag, dataChange);
  TagEvent(NameDisplaySweep, edgeChange, TIntegerTag, displaySweep);
  TagEvent(name, edgeRising, TMethodTag, activate);
end;

procedure TErrorWindow.DataChange(aTag : TAbstractTag);
var I : Integer;
    TmpList : TStringList;
    Text : string;
    Level : TFaultLevel;
begin
  ErrorList.Lines.Clear;
  TmpList := TStringList.Create;
  try
    with SysObj.FaultInterface do begin
      SysObj.FaultInterface.EnterFaultLock;
      try
        for I := 0 to FaultCount - 1 do begin
          GetFault(I, Text, Level);
          TmpList.AddObject(Text, Pointer(Ord(Level)));
        end;
      finally
        ExitFaultLock;
      end;
    end;

    ErrorList.Lines.AddStrings(TmpList);
  finally
    TmpList.Free;
  end;
end;

procedure TErrorWindow.DisplaySweep(aTag : TAbstractTag);
begin
  if ErrorList.Lines.count <> SysObj.FaultInterface.FaultCount then
    DataChange(aTag);
end;


procedure TMPGHandler.InstallComponent(Params : TParamStrings);
begin
  inherited InstallComponent(Params);
  CountsPerClick := Params.ReadInteger('CountsPerClick', 4);
  if CountsPerClick = 0 then
    MessageLog('CountsPerClick = 0 : Resetting to 4');
end;

procedure TMPGHandler.Installed;
begin
  inherited Installed;
  MPGInput_IH := TagEvent(nameInputMPG, edgeChange, TIntegerTag, MPGUpdate);
  MPGOVR_IH   := TagEvent(nameMPGOVR,   edgeChange, TIntegerTag, MPGUpdate);

  LastMPG := MPGInput_IH.AsInteger;
end;

procedure TMPGHandler.MPGUpdate(aTag : TAbstractTag);
var Change : shortInt;
    upd    : TEditChange;
begin
  if aTag = mpgOVR_IH then begin
    traverse := aTag.AsBoolean;
    if traverse then PostMessage(TForm(GetParentForm(Self)).handle, WM_NEXTDLGCTL, 0, 0);
  end;

  if aTag = mpgInput_IH then begin
    Change := aTag.AsInteger - LastMPG;
    if Abs(Change) >= Abs(CountsPerClick) then begin
      LastMPG := aTag.AsInteger;
      case Change div CountsPerClick of
        -127..-6 : upd := EditChangeDown3;
        -5..-2   : upd := EditChangeDown2;
        -1       : upd := EditChangeDown1;
        1        : upd := EditChangeUp1;
        2..5     : upd := EditChangeUp2;
        6..127   : upd := EditChangeUp3
      else
        Exit;
      end;
      if Traverse then begin
        if Change > 0 then
          PostMessage(TForm(owner).handle, WM_NEXTDLGCTL, 0, 0)
        else if change < 0 then
          PostMessage(TForm(owner).handle, WM_NEXTDLGCTL, 1, 0);
      end else begin
        SendMessage(Screen.ActiveControl.Handle, CNCM_NUDGE, Ord(upd), 0);
//        PostMessage(Application.Handle, CNCM_NUDGE, Ord(Upd), 0);
      end;
    end;
  end;
end;


{ TBasicStatusB }


initialization
  RegisterCNCClass(TErrorWindow);
  RegisterCNCClass(TBasicStatus);
  RegisterCNCClass(TBasicStatusA);
  RegisterCNCClass(TBasicStatusB);
  RegisterCNCClass(TFirstProgDisplay);
  RegisterCNCClass(TMPGHandler);
  RegisterCNCClass(TNullDisplay);
end.
