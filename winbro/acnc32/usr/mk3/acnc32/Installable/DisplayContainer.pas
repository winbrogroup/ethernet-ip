unit DisplayContainer;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Menus, StdCtrls, CNC32, CNCTypes, CoreCNC;

type
  TDisplayContainerForm = class(TForm)
    MainMenu1: TMainMenu;
    Display1: TMenuItem;
    Close1: TMenuItem;
    ListBox: TListBox;
    procedure ListBoxClick(Sender: TObject);
    procedure Close1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    OldParent : TWinControl;
    ThisDisplay : TAbstractDisplay;
  public
  end;

  TDisplayContainer = class(TCNC)
  protected
    Container : TDisplayContainerForm;
  public
    procedure InstallComponent(Params : TParamStrings); override;
    procedure Installed; override;
    procedure Callback(aTag : TAbstractTag);
  end;
var
  DisplayContainerForm: TDisplayContainerForm;

implementation

{$R *.DFM}

procedure TDisplayContainer.InstallComponent(Params : TParamStrings);
begin
  inherited InstallComponent(Params);
end;

procedure TDisplayContainer.Installed;
begin
  inherited Installed;
  TagEvent(Name, EdgeFalling, TMethodTag, Callback);
end;

procedure TDisplayContainer.Callback(aTag : TAbstractTag);
var I : Integer;
begin
  if not Assigned(Container) then
    Container := TDisplayContainerForm.Create(Application);

  Container.ListBox.Clear;
  Container.ListBox.Visible := True;
  Container.Font.Name := SysObj.FontName;
  for I := 0 to SysObj.Layout.DisplayCount - 1 do begin
    Container.ListBox.Items.AddObject(SysObj.Layout.Displays[I].Name, SysObj.Layout.Displays[I]);
  end;
  Container.Show;
end;

procedure TDisplayContainerForm.ListBoxClick(Sender: TObject);
begin
  if ListBox.ItemIndex <> -1 then begin
    ThisDisplay := SysObj.Layout.Displays[ListBox.ItemIndex];
    OldParent := ThisDisplay.Parent;
    ThisDisplay.Parent := Self;
    ThisDisplay.Align := alClient;
    ThisDisplay.Open;
    ThisDisplay.ExternalLayout := True;

    ListBox.Visible := False;
  end;
end;

procedure TDisplayContainerForm.Close1Click(Sender: TObject);
begin
  Close;
end;

procedure TDisplayContainerForm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  if Assigned(ThisDisplay) then begin
    ThisDisplay.Align := alNone;
    ThisDisplay.Parent := OldParent;

    ThisDisplay.ExternalLayout := False;
    ThisDisplay.Open;
    ThisDisplay.Close;

    ListBox.Visible := True;
  end;
end;

initialization
  RegisterCNCClass(TDisplayContainer);
end.
