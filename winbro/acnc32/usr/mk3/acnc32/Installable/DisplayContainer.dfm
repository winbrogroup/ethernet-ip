object DisplayContainerForm: TDisplayContainerForm
  Left = 212
  Top = 127
  Width = 572
  Height = 455
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  Menu = MainMenu1
  OldCreateOrder = True
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object ListBox: TListBox
    Left = 0
    Top = 0
    Width = 564
    Height = 409
    Align = alClient
    ItemHeight = 13
    TabOrder = 0
    OnClick = ListBoxClick
  end
  object MainMenu1: TMainMenu
    Left = 24
    Top = 60
    object Display1: TMenuItem
      Caption = 'Display'
      object Close1: TMenuItem
        Caption = 'Close Form'
        OnClick = Close1Click
      end
    end
  end
end
