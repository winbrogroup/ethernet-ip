unit Access;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  CoreCNC, Named, CNCTypes, CNC32, NCNames, AccessEditForm, SimpleTapReader,
  ISecurity, ExAccessEdit, UseKeyBoardA;

{ An simple implementation of user mode change. This takes a request and
  and makes it happen, override the modeChange logic to impliment a more
  useful version }

type
  TAbstractAccessLevel = class(TCNC)
  private
  protected
    ReqNoAccessTag : TAbstractTag;
    ReqOperatorAccessTag : TAbstractTag;
    ReqProgrammerAccessTag : TAbstractTag;
    ReqSupervisorAccessTag : TAbstractTag;
    ReqMaintenanceAccessTag : TAbstractTag;
    ReqAdministratorAccessTag : TAbstractTag;
    ReqOEM1AccessTag : TAbstractTag;
    ReqOEM2AccessTag : TAbstractTag;
    UserAccessTag : TAbstractTag;
    AccessLevelsTag : TAbstractTag;
    UserNameTag : TAbstractTag;
    procedure AccessChange(aTag : TAbstractTag); virtual;
    procedure AccessLogOff(aTag : TAbstractTag); virtual;
  public
    procedure InstallComponent(Params : TParamStrings); override;
    procedure Installed; override;
  published
  end;

  TPasswordAccess = class(TAbstractAccessLevel)
  private
    SecurityFile : TSecurityFile;
    DlgActive : Boolean;
    AccessRecord : TAccessRecord;
  protected
    procedure AccessUserAdmin(aTag : TAbstractTag);
    procedure AccessChange(aTag : TAbstractTag); override;
    procedure AccessLevelFault(aTag : TAbstractTag);
    function SecurityCheck(aText : string) : Boolean;

    procedure ForceSecurity(Access : TAccessLevels); stdcall;
    procedure Admin; stdcall;
    procedure LogOff; stdcall;
    function CurrentUser : WideString; stdcall;
    function CurrentAccess : TAccessLevels; stdcall;
    procedure UpdateTags(const AL : TAccessRecord);
  public
    procedure InstallComponent(Params : TParamStrings); override;
    procedure Installed; override;
  end;


  TAccessPlugin = class(TAbstractAccessLevel, IFSDSecurityHost)
  private
    Access : IFSDSecurity;
    InAdmin : Boolean;
    function SingleLevel(A : TAccessLevels) : TAccessLevel;
    procedure UpdateUserDetails;
    procedure AccessPoll(aTag : TAbstractTag);
  protected
    procedure AccessUserAdmin(aTag : TAbstractTag);
    procedure AccessLogOff(aTag : TAbstractTag); override;
    procedure AccessLevelFault(aTag : TAbstractTag);
    procedure AccessChange(aTag : TAbstractTag); override;
  public
    procedure InstallComponent(Params : TParamStrings); override;
    procedure Installed; override;
    procedure IShutdown; override;
  end;


   {This class creates another access level password form for using with ACNC that allows the selection
    of the security code via the keyboard and not pressing the code from the screen. It is required for making
    it more difficult for observers to see the password used.}
    
   TPasswordAccessKB = class(TAbstractAccessLevel)
  private
    SecurityFile : TSecurityFile;
    DlgActive : Boolean;
    AccessRecord : TAccessRecord;
  protected
    procedure AccessUserAdmin(aTag : TAbstractTag);
    procedure AccessChange(aTag : TAbstractTag); override;
    procedure AccessLevelFault(aTag : TAbstractTag);
    function SecurityCheck(aText : string) : Boolean;

    procedure ForceSecurity(Access : TAccessLevels); stdcall;
    procedure Admin; stdcall;
    procedure LogOff; stdcall;
    function CurrentUser : WideString; stdcall;
    function CurrentAccess : TAccessLevels; stdcall;
    procedure UpdateTags(const AL : TAccessRecord);
  public
    procedure InstallComponent(Params : TParamStrings); override;
    procedure Installed; override;
  end;



implementation

const
  ExSecurityDB = 'securityex.dat';


procedure TAbstractAccessLevel.InstallComponent(Params : TParamStrings);
begin
  inherited installComponent(Params);
  UserAccessTag := TagPublish(nameAccessLevel, TIntegerTag);
  AccessLevelsTag := TagPublish(NameAccessLevels, TIntegerTag);
  UserNameTag := TagPublish(NameAccessUserName, TStringTag);
end;

procedure TAbstractAccessLevel.AccessLogOff(aTag : TAbstractTag);
begin
  EventLog('Logged off');
  UserAccessTag.AsInteger := 0;
  AccessLevelsTag.AsInteger := 1;
  UserNameTag.AsString := SysObj.Localized.GetString('$NONE');
end;

procedure TAbstractAccessLevel.Installed;
begin
  inherited Installed;
  UserAccessTag.AsInteger := Ord(AccessLevelNone);
  UserNameTag.AsString := SysObj.Localized.GetString('$NONE');
  ReqNoAccessTag            := TagEvent(NameReqNoAccess            , EdgeFalling, TMethodTag, AccessChange);
  ReqOperatorAccessTag      := TagEvent(NameReqOperatorAccess      , EdgeFalling, TMethodTag, AccessChange);
  ReqProgrammerAccessTag    := TagEvent(NameReqProgrammerAccess    , EdgeFalling, TMethodTag, AccessChange);
  ReqSupervisorAccessTag    := TagEvent(NameReqSupervisorAccess    , EdgeFalling, TMethodTag, AccessChange);
  ReqMaintenanceAccessTag   := TagEvent(NameReqMaintenanceAccess   , EdgeFalling, TMethodTag, AccessChange);
  ReqAdministratorAccessTag := TagEvent(NameReqAdministratorAccess , EdgeFalling, TMethodTag, AccessChange);
  ReqOEM1AccessTag          := TagEvent(NameReqOEM1Access          , EdgeFalling, TMethodTag, AccessChange);
  ReqOEM2AccessTag          := TagEvent(NameReqOEM2Access          , EdgeFalling, TMethodTag, AccessChange);
  TagEvent(Name + 'LogOff', edgeFalling, TMethodTag, AccessLogOff);
end;

procedure TAbstractAccessLevel.AccessChange(aTag : TAbstractTag);
var AL : TAccessLevel;
begin
  AL := AccessLevelNone;
//  if aTag.AsInteger = 0 then begin
    if aTag = ReqNoAccessTag           then AL := AccessLevelNone;
    if aTag = ReqOperatorAccessTag     then AL := AccessLevelOperator;
    if aTag = ReqProgrammerAccessTag   then AL := AccessLevelProgrammer;
    if aTag = ReqSupervisorAccessTag   then AL := AccessLevelSupervisor;
    if aTag = ReqMaintenanceAccessTag  then AL := AccessLevelMaintenance;
    if aTag =ReqAdministratorAccessTag then AL := AccessLevelAdministrator;
    if aTag = ReqOEM1AccessTag         then AL := AccessLevelOEM1;
    if aTag = ReqOEM2AccessTag         then AL := AccessLevelOEM2;

    UserAccessTag.AsInteger := Ord(AL);
    AccessLevelsTag.AsInteger := Byte(TAccessLevels([AL]));
//  end;
end;


{ **** TPasswordAccess Class ****
 This will display an access security form that allows keyboard entry  or screen keypad entry }

procedure TPasswordAccess.InstallComponent(Params : TParamStrings);
begin
  inherited;
end;

procedure TPasswordAccess.Installed;
begin
  inherited;
  TagEvent(Name + 'AccessAdmin', EdgeFalling, TMethodTag, AccessUserAdmin);
  TagEvent(NameAccessInterlock, edgeChange, TMethodTag, AccessLevelFault);
end;

procedure TPasswordAccess.UpdateTags(const Al : TAccessRecord);
  function MaxLevel(A : TAccessLevels) : TAccessLEvel;
  begin
    for Result := High(TAccessLevel) downto Low(TAccessLevel) do
      if Result in A then
        Exit;
    Result := AccessLevelNone;
  end;

begin
  UserAccessTag.AsInteger := Ord(MaxLevel(AL.AccessLevels));
  AccessLevelsTag.AsInteger := Byte(AL.AccessLevels);
  UserNameTag.AsString := Trim(AL.UserName);
end;


procedure TPasswordAccess.AccessLevelFault(aTag : TAbstractTag);
var DBPath: string;
begin
  DBPath:= ContractSpecificPath + ExSecurityDB;

  if not FileExists(DBPath) then
    DBPath:= ProfilePath + ExSecurityDB;

  if not DlgActive then begin
    SecurityFile := TSecurityFile.Create(True);
    try
      SecurityFile.LoadFromFile(DBPath);

      DlgActive := True;
      try
        if TSimpleTapForm.Execute(SysObj.Localized.GetString('$ENTER_SECURITY_PASSWORD'), SecurityCheck) then begin
          UpdateTags(AccessRecord);
          EventLog('Logged on');
        end else begin
          EventLog('Failed logon');
          UpdateTags(AccessRecord);
          EventLog('Logged off');
        end;
      finally
        DlgActive := False;
      end;
    finally
      SecurityFile.Free;
    end;
  end;
end;

procedure TPasswordAccess.AccessChange(aTag : TAbstractTag);
begin
  AccessLevelFault(aTag);
end;


function TPasswordAccess.SecurityCheck(aText : string) : Boolean;
begin
  AccessRecord := SecurityFile.Check(aText)^;
  Result := AccessRecord.AccessLevel > AccessLevelNone;
end;

procedure TPasswordAccess.AccessUserAdmin(aTag : TAbstractTag);
begin
  Admin;
end;


procedure TPasswordAccess.Admin;
var Sec, Path: string;
begin
  Sec:= ExSecurityDB;
  Path:= ContractSpecificPath + Sec;

  if not FileExists(Path) then
    Path:= ProfilePath + ExSecurityDB;

  TAccessesEditFrm.Execute(Path);
end;

function TPasswordAccess.CurrentAccess: TAccessLevels;
begin
  Result := TAccessLevels(Byte(AccessLevelsTag.AsInteger));
end;

function TPasswordAccess.CurrentUser: WideString;
begin
  Result := UserNameTag.AsString;
end;

procedure TPasswordAccess.ForceSecurity(Access: TAccessLevels);
begin
//  FAccessLevel := Access;
end;

procedure TPasswordAccess.LogOff;
begin
  Self.AccessLogOff(nil);
end;



{ **** TAccessPlugin class **** }

procedure TAccessPlugin.UpdateUserDetails;
begin
  UserAccessTag.AsInteger := Ord(SingleLevel(Access.CurrentAccess));
  AccessLevelsTag.AsInteger := Byte(Access.CurrentAccess);
  UserNameTag.AsString := Access.CurrentUser;
end;


procedure TAccessPlugin.AccessChange(aTag: TAbstractTag);
var TmpAccess : TAccessLevel;
begin
  inherited;
  TmpAccess := AccessLevelNone;
  if aTag.AsInteger = 0 then begin
    if aTag = ReqNoAccessTag           then TmpAccess := AccessLevelNone;
    if aTag = ReqOperatorAccessTag     then TmpAccess := AccessLevelOperator;
    if aTag = ReqProgrammerAccessTag   then TmpAccess := AccessLevelProgrammer;
    if aTag = ReqSupervisorAccessTag   then TmpAccess := AccessLevelSupervisor;
    if aTag = ReqMaintenanceAccessTag  then TmpAccess := AccessLevelMaintenance;
    if aTag = ReqAdministratorAccessTag then TmpAccess := AccessLevelAdministrator;
    if aTag = ReqOEM1AccessTag         then TmpAccess := AccessLevelOEM1;
    if aTag = ReqOEM2AccessTag         then TmpAccess := AccessLevelOEM2;

    Access.ForceSecurity([TmpAccess]);
  end;
  UpdateUserDetails;
end;

function TAccessPlugin.SingleLevel(A : TAccessLevels) : TAccessLevel;
begin
  for Result := High(Result) downto Low(Result) do begin
    if Result in A then
      Exit;
  end;
  Result := AccessLevelNone;
end;

procedure TAccessPlugin.AccessLevelFault(aTag: TAbstractTag);
var TmpUser : WideString;
begin
  Access.LogOn(TmpUser);
  UpdateUserDetails;
end;

procedure TAccessPlugin.AccessLogOff(aTag: TAbstractTag);
begin
  Access.LogOff;
  UpdateUserDetails;
end;

procedure TAccessPlugin.AccessUserAdmin(aTag: TAbstractTag);
begin
  if not InAdmin then begin
    InAdmin := True;
    try
      SysObj.NC.AcquireModeChangeLock;
      try
        Access.Admin;
      finally
        SysObj.NC.ReleaseModeChangeLock;
      end;
    finally
      InAdmin := False;
    end;
  end;
  UpdateUserDetails;
end;

procedure TAccessPlugin.InstallComponent(Params: TParamStrings);
var PluginName : WideString;
    Parms : string;
    ObjectName : string;
    HMod : HModule;
    GetAccess : TGetSecurityInterface;
begin
  inherited;
  ObjectName := Params.ReadString('Object', '*');
  if ObjectName = '*' then
    raise ECNCInstallFault.Create('Parameter "Object" not programmed.');

  PluginName := Params.ReadString('Plugin', '*');
  if PluginName = '*' then
    raise ECNCInstallFault.Create('Parameter "Plugin" not programmed.');


  HMod := SysObj.Comms.FindPlugin(PluginName);
  if HMod = 0 then
    raise ECNCInstallFault.CreateFmt('Cannot find library "%s" in plugins section', [PluginName]);

  Parms := Params.ReadString('HW', 'COM3');
  GetAccess := GetProcAddress(HMod, GetFSDSecurityInterfaceName);
  if Assigned(GetAccess) then
    Access := GetAccess(Self, WideString(ObjectName), WideString(Parms))
  else
    raise ECNCInstallFault.CreateFmt('Plugin does not export "%s"', [GetFSDSecurityInterfaceName]);

  if not Assigned(Access) then
    raise ECNCInstallFault.CreateFmt('Failed to create Access from object "%s" in "%s"',
        [ObjectName, PluginName]);

  Access.InstallName := Name;
  Access.Install;
end;

procedure TAccessPlugin.Installed;
begin
  inherited;
  TagEvent(NameAccessInterlock, edgeChange, TMethodTag, AccessLevelFault);
  TagEvent(Name + 'UserAdmin', edgeFalling, TMethodTag, AccessUserAdmin);
  TagEvent(Name + 'LogOn', edgeFalling, TMethodTag, AccessLevelFault);
  TagEvent(NameDisplaySweep, EdgeChange, TMethodTag, AccessPoll);
  Access.FinalInstall;
end;

procedure TAccessPlugin.AccessPoll;
begin
  if Access.Poll then begin
    UpdateUserDetails;
  end;
end;

procedure TAccessPlugin.IShutdown;
begin
  inherited;
  if Assigned(Access) then
    Access.ShutDown;
  Access := nil;
end;



{ **** TPasswordAccessKB class ****
  This will display an access security form that only allows keyboard entry }

procedure TPasswordAccessKB.InstallComponent(Params : TParamStrings);
begin
  inherited;
end;

procedure TPasswordAccessKB.Installed;
begin
  inherited;
  TagEvent(Name + 'AccessAdmin', EdgeFalling, TMethodTag, AccessUserAdmin);
  TagEvent(NameAccessInterlock, edgeChange, TMethodTag, AccessLevelFault);
end;

procedure TPasswordAccessKB.UpdateTags(const Al : TAccessRecord);
  function MaxLevel(A : TAccessLevels) : TAccessLEvel;
  begin
    for Result := High(TAccessLevel) downto Low(TAccessLevel) do
      if Result in A then
        Exit;
    Result := AccessLevelNone;
  end;

begin
  UserAccessTag.AsInteger := Ord(MaxLevel(AL.AccessLevels));
  AccessLevelsTag.AsInteger := Byte(AL.AccessLevels);
  UserNameTag.AsString := Trim(AL.UserName);
end;


procedure TPasswordAccessKB.AccessLevelFault(aTag : TAbstractTag);
var DBPath: string;
begin
  DBPath:= ContractSpecificPath + ExSecurityDB;

  if not FileExists(DBPath) then
    DBPath:= ProfilePath + ExSecurityDB;

  if not DlgActive then begin
    SecurityFile := TSecurityFile.Create(True);
    try
      SecurityFile.LoadFromFile(DBPath);

      DlgActive := True;
      try
        if TAccessKeyboardA.Execute(SysObj.Localized.GetString('$ENTER_SECURITY_PASSWORD'), SecurityCheck) then begin
          UpdateTags(AccessRecord);
          EventLog('Logged on');
        end else begin
          EventLog('Failed logon');
          UpdateTags(AccessRecord);
          EventLog('Logged off');
        end;
      finally
        DlgActive := False;
      end;
    finally
      SecurityFile.Free;
    end;
  end;
end;

procedure TPasswordAccessKB.AccessChange(aTag : TAbstractTag);
begin
  AccessLevelFault(aTag);
end;


function TPasswordAccessKB.SecurityCheck(aText : string) : Boolean;
begin
  AccessRecord := SecurityFile.Check(aText)^;
  Result := AccessRecord.AccessLevel > AccessLevelNone;
end;

procedure TPasswordAccessKB.AccessUserAdmin(aTag : TAbstractTag);
begin
  Admin;
end;


procedure TPasswordAccessKB.Admin;
var Sec, Path: string;
begin
  Sec:= ExSecurityDB;
  Path:= ContractSpecificPath + Sec;

  if not FileExists(Path) then
    Path:= ProfilePath + ExSecurityDB;

  TAccessesEditFrm.Execute(Path);
end;

function TPasswordAccessKB.CurrentAccess: TAccessLevels;
begin
  Result := TAccessLevels(Byte(AccessLevelsTag.AsInteger));
end;

function TPasswordAccessKB.CurrentUser: WideString;
begin
  Result := UserNameTag.AsString;
end;

procedure TPasswordAccessKB.ForceSecurity(Access: TAccessLevels);
begin
//  FAccessLevel := Access;
end;

procedure TPasswordAccessKB.LogOff;
begin
  Self.AccessLogOff(nil);
end;


// **** Registration of the classes to ACNC ****
begin
 RegisterCNCClass(TAbstractAccessLevel);
  RegisterCNCClass(TPasswordAccess);
  RegisterCNCClass(TPasswordAccessKB, 'KeyboardAccess');
  RegisterCNCClass(TAccessPlugin);
end.
