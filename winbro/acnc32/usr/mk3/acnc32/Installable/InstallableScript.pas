unit InstallableScript;

interface

uses
  CoreCNC, CNC32, CNCTypes, Named, ACNCScript, ScriptTypeInfo, ScriptEditor,
  Classes, CNCABs, Controls, Forms, ScriptFunctions, SysUtils, Windows,
  ExtCtrls, ScriptFile, Acknowledge;

type
  TScriptPanel = class(TCustomPanel)
  public
    RTL : TScriptRTL;
    procedure Resize; override;
    procedure Paint; override;
    procedure Setup;
  end;

  TBackgroundScript = class(TExpandDisplay)
  private
//    ErrorTag : TStringTag;
    LineNoTag : TAbstractTag;
    InCycleTag : TAbstractTag;
  private
    Panel : TScriptPanel;
    SF : TScriptEditForm;
    RTL : TScriptRTL;
    ScriptFile : TACNCScriptFile;
    FileName : string;
    procedure UpdateEv(aTag : TAbstractTag);
    procedure SetRun(aRun : Boolean);
    procedure SetRunState(aTag : TAbstractTag);
    procedure ChangeTTL(aTag : TAbstractTag);
    procedure ShowForm(aTag : TAbstractTag);
    procedure Rewind(aTag : TAbstractTag);
    procedure SaveProject(Sender : TObject);
  public
    Project : TACNCScriptProject;
    constructor Create(aOwner : TComponent); override;
    procedure InstallComponent(Params : TParamStrings); override;
    procedure Installed; override;
    function IShutdownPermissive : Boolean; override;
    procedure CheckInstallation; override;
    destructor Destroy; override;
  end;

  TRemoteScript = class(TCNC)
  private
    ErrorTag : TAbstractTag;
    LineNoTag : TAbstractTag;
    InCycleTag : TAbstractTag;
    FRun : Boolean;
    procedure SetRun(aRun : Boolean);

    procedure UpdateUnitTag(aTag : TAbstractTag);
    procedure SetRunState(aTag : TAbstractTag);
    procedure ScriptUpdate(aTag : TAbstractTag);
    procedure ChangeTTL(aTag : TAbstractTag);
  public
    Project : TACNCScriptProject;
    procedure InstallComponent(Params : TParamStrings); override;
    procedure Installed; override;
    property Run : Boolean read FRun write SetRun;
  end;

implementation


procedure TRemoteScript.InstallComponent(Params : TParamStrings);
begin
  inherited;
  Project := TACNCScriptProject.Create(Self);
  Project.TTL := 1;
  Project.SingleBlock := True;
  Project.RTL := TScriptRTL.Create(Self);
  ErrorTag := TagPublish(NameRemoteScriptError, TStringTag);

  LineNoTag := TagPublish(NameRemoteScriptLineNo, TIntegerTag);
  InCycleTag := TagPublish(NameRemoteScriptInCycle, TMethodTag);
end;


procedure TRemoteScript.Installed;
begin
  inherited;
  TagEvent(NameRemoteScript, edgeChange, TStringTag, UpdateUnitTag);
  TagEvent(NameRemoteScriptRun, edgeChange, TMethodTag, SetRunState);
  TagEvent(NameDisplaySweep, edgeChange, TMethodTag, ScriptUpdate);
  TagEvent(NameRemoteScriptTTL, EdgeChange, TIntegerTag, ChangeTTL);
end;

procedure TRemoteScript.SetRunState(aTag : TAbstractTag);
begin
  Run := aTag.AsBoolean;
end;

procedure TRemoteScript.SetRun(aRun : Boolean);
begin
  FRun := aRun;
  InCycleTag.AsBoolean := Run;
end;

procedure TRemoteScript.ChangeTTL(aTag : TAbstractTag);
begin
  Project.TTL := aTag.AsInteger;
end;

procedure TRemoteScript.ScriptUpdate(aTag : TAbstractTag);
begin
  if Run then begin
    Project.Run;
    Run := Project.InProcess;
    LineNoTag.AsInteger := Project.LineNumber;
  end;
end;

procedure TRemoteScript.UpdateUnitTag(aTag : TAbstractTag);
var Script : TACNCScript;
begin
  if aTag.AsString = '' then
    Exit;

  Project.Clean;
  while Project.Units.Count > 0  do begin
    TObject(Project.Units[0]).Free;
    Project.Units.Delete(0);
  end;

  Script := TACNCScript.Create(Self);
  Script.Name := 'Remote';
  Script.Text := aTag.AsString;
  Script.Project := Project;
  Project.Units.Add(Script);
  try
    Project.Compile('Main');
    ErrorTag.AsString := '';
  except
    on E : Exception do
      ErrorTag.AsString := E.Message;
  end;
end;

const
  ACNCScriptExtension = 'ACNCScript';
  ACNCScriptFilter = 'Acnc Scripts|' + '*.' + ACNCScriptExtension;
  ACNCScriptDirectory = 'ACNCScripts\';


procedure TScriptPanel.Setup;
begin
  RTL.ClientCanvas := Canvas;
//  Form1.RemoteScript.Project.RTL.ClientCanvas := Canvas;
end;

procedure TScriptPanel.Resize;
begin
  if Assigned(RTL) then
    RTL.Resize;
end;

procedure TScriptPanel.Paint;
begin
  inherited;
  RTL.DoPaint;
end;

{ TBackgroundScript }

procedure TBackgroundScript.ChangeTTL(aTag: TAbstractTag);
begin
  SF.SetTTL(aTag.AsInteger);
//  Project.TTL := aTag.AsInteger;
end;

procedure TBackgroundScript.ShowForm(aTag : TAbstractTag);
begin
  SF.Show;
  
{  if SF.Visible then
    Exit;
  if SF.ShowModal = mrOK then begin
  end; }
end;

constructor TBackgroundScript.Create(aOwner: TComponent);
begin
  inherited;
end;

destructor TBackgroundScript.Destroy;
begin
  EventLog('Destroying Script');
  if Assigned(SF) then begin
    EventLog('Assigned SF Script');

    SF.RunActive := False;
    if SF.Visible then begin
      EventLog('Hiding Script');
      SF.ModalResult := mrCancel;
    end;
    EventLog('Goodbye Script');
//    SF.Free;
//    SF := nil;
  end;
  inherited;
  EventLog('Destroyed');
end;

procedure TBackgroundScript.InstallComponent(Params: TParamStrings);
begin
  inherited;
  Project := TACNCScriptProject.Create(Self);
  Project.TTL := 1;
  Project.SingleBlock := True;
  Project.RTL := TScriptRTL.Create(Self);

  FileName := ProfilePath + ACNCScriptDirectory + Params.ReadString('Project', 'default.prj');
  Project.TTL := Params.ReadInteger('TTL', 10);

  LineNoTag := TagPublish(Name + 'ScriptLineNo', TIntegerTag);
  InCycleTag := TagPublish(Name + 'ScriptInCycle', TMethodTag);
end;

procedure TBackgroundScript.SaveProject(Sender : TObject);
begin
  ScriptFile.WriteToStrings;
  ScriptFile.SaveToFile(FileName);
  SF.Modified:= False;
end;

function TBackgroundScript.IShutdownPermissive : Boolean;
begin
  Result := True;

  if SF.Modified then
    case Acknowledge.YesNoCancel(Name + ' Script has been modified do you wish to save?') of
      mrOK : SaveProject(Self);
      mrNo : Exit;
      mrCancel : Result := False;
    end;
end;

procedure TBackgroundScript.Installed;
begin
  inherited;
  RTL := TScriptRTL.Create(Self);
  SF := TScriptEditForm.Create(Application);
  SF.OnSave := SaveProject;

  Project := TACNCScriptProject.Create(Self);
  Project.RTL := RTL;

  ScriptFile := TACNCScriptFile.Create(Self);
  ScriptFile.Project := Project;
  try
    ScriptFile.LoadFromFile(FileName);
    ScriptFile.ReadFromStrings;
  except
  end;
//  Project.Units.Add(Script);
  SF.ScriptProject := Project;

  Panel := TScriptPanel.Create(Self);
  Panel.Parent := Self;
  Panel.RTL := RTL;
  Panel.Setup;
  Panel.Align := alClient;

  TagEvent(NameDisplaySweep, EdgeChange, TMethodTag, UpdateEv);
  TagEvent(Name + 'EditorShow', EdgeFalling, TMethodTag, ShowForm);
  TagEvent(Name + 'ReqScriptTTL', EdgeChange, TIntegerTag, ChangeTTL);
  TagEvent(Name + 'ScriptRun', EdgeChange, TMethodTag, SetRunState);
  TagEvent(Name + 'ScriptRewind', EdgeFalling, TMethodTag, Rewind);
end;

procedure TBackgroundScript.SetRun(aRun: Boolean);
begin
  SF.RunActive := aRun;
end;

procedure TBackgroundScript.SetRunState(aTag: TAbstractTag);
begin
  SetRun(aTag.AsBoolean);
end;

procedure TBackgroundScript.UpdateEv(aTag: TAbstractTag);
begin
  if Assigned(SF) then begin
    SF.DoUpdate;
    if SF.RunActive <> InCycleTag.AsBoolean then
      InCycleTag.AsBoolean := SF.RunActive;
      if LineNoTag.AsInteger <> Project.LineNumber then
        LineNoTag.AsInteger := Project.LineNumber;
  end;
end;


procedure TBackgroundScript.Rewind(aTag: TAbstractTag);
begin
  SF.RewindBtnClick(SF);
end;

procedure TBackgroundScript.CheckInstallation;
begin
  inherited;
  try
    SF.CompileBtnClick(SF);
  except
    on E : Exception do
      MessageLogFmt('Component %s Unable to Compile %s', [Name, FileName]);
  end;
end;

initialization
  RegisterCNCClass(TBackGroundScript);
  RegisterCNCClass(TRemoteScript);
end.
