unit zFileList;

interface

uses Windows, SysUtils, Classes, Graphics, Forms, Controls, StdCtrls,
  Buttons, ExtCtrls, FileCtrl, CNC32, CoreCNC;

type
  {like navigating directories, creating files etc...}
  TSelectFileOption = (sfoCanEditFilename, sfoFileMustExist, sfoEtcEtc);
  TSelectFileOptions = set of TSelectFileOption;

  TFileListForm = class(TForm)
    GroupBox1: TGroupBox;
    FileList: TFileListBox;
    Panel1: TPanel;
    EditLabel: TLabel;
    FilenameEdit: TEdit;
    OKBtn: TButton;
    CancelBtn: TButton;
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FileListClick(Sender: TObject);
  private
    { Private declarations }
    FOptions: TSelectFileOptions;
    DefExt, FilenameResult: String;

  public
    { Public declarations }
    function Execute( Options: TSelectFileOptions;
                  var Filename: String): Boolean;
                  {filename should be a fully qualified mask}
  end;

function SelectFile( Options: TSelectFileOptions; var Filename: String): Boolean;
{Options = sfoCanEditFilename to allow user to type in filename
 Filename = fully qualified mask eg C:\acnc\programs\*.nc
 returns:
   true if user clicks OK
   Fully qualified filename in Filename parameter eg
     C:\acnc\programs\afile.nc
}


implementation
uses
  Dialogs;

resourcestring
  NoFilesMsg = 'There are no files available';

{$R *.DFM}
function TFileListForm.Execute(Options: TSelectFileOptions;
                         var Filename: String): Boolean;
begin
  FOptions:= Options;
  Font.Name := SysObj.FontName;
  FileList.Directory:= ExtractFileDir(Filename);
  FilenameEdit.Enabled:= sfoCanEditFilename in FOptions;
  FileList.Mask:= ExtractFileName(Filename);
  defext:= ExtractFileExt(Filename);
  if not FilenameEdit.Enabled then
  begin
    if Filelist.Items.Count = 0 then
    begin
      MessageDlg(NoFilesMsg, mtError, [mbOk], 0);
      Result:= false;
      exit
    end;
    FilenameEdit.Text:= FileList.Items[0]
  end;
  Result:= ShowModal = mrOK;
  if Result then
    Filename:= CNC32.GetAbsolutePathLocal(FileList.Directory) + FilenameResult;
end;

function SelectFile( Options: TSelectFileOptions; var Filename: String): Boolean;
var
  FLF: TFileListForm;
begin
 // Application.CreateForm(TFilelistForm,FLF);
  FLF:= TFileListForm.Create(Application.MainForm);
  try
    Result:= FLF.Execute(Options, Filename);
  finally
    FLF.Free
  end
end;


procedure TFileListForm.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
  CanClose:= true;
  if ModalResult = mrOK then
  begin
    FilenameResult:= FilenameEdit.Text;
    if ExtractFileExt(FileNameResult) = '' then
      FileNameResult:= FileNameResult + defext;
    if sfoFileMustExist in FOptions then
      CanClose:= FileExists(FilenameResult)
  end
end;


procedure TFileListForm.FileListClick(Sender: TObject);
begin
  if FileList.ItemIndex <> -1 then
    FileNameEdit.Text := FileList.Items[FileList.ItemIndex];
end;

end.
