unit ScreenSaverFrm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, CNCTypes;

type
  TSaverScreen = class(TForm)
    Timer: TTimer;
    procedure FormMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure TimerTimer(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    Control : TWinControl;
    VectorX, VectorY : Integer;
    OldParent : TWinControl;
    procedure CNCMScreenSaverClose(var Message : TMessage); message CNCM_CANCEL_SAVER;
  public
    class procedure Execute(Control : TWinControl);
    class procedure CancelScreenSaver;
  end;

var
  SaverScreen: TSaverScreen;
  Point : TPoint;

implementation

var Form : TSaverScreen;

{$R *.DFM}

class procedure TSaverScreen.Execute(Control : TWinControl);
begin
  Form := TSaverScreen.Create(nil);
  try
    ShowCursor(False);
    try
      Form.Control := Control;

      Point.X := Mouse.CursorPos.X;
      Point.Y := Mouse.CursorPos.Y;
      Form.VectorX := 2;
      Form.VectorY := 2;
      if Assigned(Form.Control) then begin
        Form.Timer.Enabled := True;
        Form.Control.Visible := True;
        Form.OldParent := Control.Parent;
        Control.Parent := Form;
      end;

      Form.ShowModal;
    finally
      ShowCursor(True);
    end;
  finally
    Form.Control.Parent := Form.OldParent;
    Form.Free;
    Form := nil;
  end;
end;

class procedure TSaverScreen.CancelScreenSaver;
begin
  if Assigned(Form) then
    PostMessage(Form.Handle, CNCM_CANCEL_SAVER, 0, 0);
end;

procedure TSaverScreen.FormMouseMove(Sender: TObject; Shift: TShiftState;
  X, Y: Integer);
begin
  if (Mouse.CursorPos.X > Point.X + 2) or
     (Mouse.CursorPos.X < Point.X - 2) or
     (Mouse.CursorPos.Y > Point.Y + 2) or
     (Mouse.CursorPos.Y < Point.Y - 2) then
  ModalResult := mrOK;
end;

procedure TSaverScreen.TimerTimer(Sender: TObject);
begin
  if(Control.Left < 0) then
    VectorX := Round(Random(3) + 1);
  if Control.Left + Control.Width > Width then
    VectorX := -Round(Random(3) + 1);
  if(Control.Top < 0) then
    VectorY := Round(Random(3) + 1);
  if Control.Top + Control.Height > Height then
    VectorY := -Round(Random(3) + 1);

  Control.Left := Control.Left + VectorX;
  Control.Top := Control.Top + VectorY;
end;

procedure TSaverScreen.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  ModalResult := mrOK;
end;

procedure TSaverScreen.CNCMScreenSaverClose(var Message: TMessage);
begin
  ModalResult := mrOK;
end;

end.
