unit RestraintEditForm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, CustomLayout, CoreCNC;

type
  TRestraintEdit = class(TForm)
    LeftEdit: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    TopEdit: TEdit;
    WidthEdit: TEdit;
    HeightEdit: TEdit;
    Panel1: TPanel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    VisualsCombo: TComboBox;
    Label5: TLabel;
    ProtectedCheckBox: TCheckBox;
    DontOpenCheck: TCheckBox;
    Label6: TLabel;
    NameEdit: TEdit;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
    function ReadEdit(E : TEdit) : Double;
  public
    class function Execute(DR : TDisplayRestraint; Visuals : TList) : Boolean;
  end;

implementation

{$R *.DFM}

var
  RestraintEdit: TRestraintEdit;


class function TRestraintEdit.Execute(DR : TDisplayRestraint; Visuals : TList) : Boolean;
var I : Integer;
begin
  Result := False;
  RestraintEdit := TRestraintEdit.Create(Application);
  try
    with RestraintEdit do begin

      for I := 0 to Visuals.Count - 1 do
        VisualsCombo.Items.AddObject(TAbstractCNC(Visuals[I]).Name, Visuals[I]);
      VisualsCombo.Items.AddObject('NONE', nil);

      VisualsCombo.ItemIndex := Visuals.IndexOf(DR.DefaultDisplay);
      if VisualsCombo.ItemIndex = -1 then
        VisualsCombo.ItemIndex := 0;

      LeftEdit.Text := Format('%.3f', [DR.Left]);
      TopEdit.Text := Format('%.3f', [DR.Top]);
      WidthEdit.Text := Format('%.3f', [DR.Width]);
      HeightEdit.Text := Format('%.3f', [DR.Height]);
      ProtectedCheckBox.Checked := DR.ProtectedDisplay;
      DontOpenCheck.Checked := DR.DontOpen;
      NameEdit.Text := DR.Name;

      if RestraintEdit.ShowModal = mrOk then begin
        try
          DR.Left := ReadEdit(LeftEdit);
          DR.Top := ReadEdit(TopEdit);
          DR.Width := ReadEdit(WidthEdit);
          DR.Height := ReadEdit(HeightEdit);
          DR.ProtectedDisplay := ProtectedCheckBox.Checked;
          DR.DontOpen := DontOpenCheck.Checked;
          DR.DefaultDisplay := TAbstractDisplay(VisualsCombo.Items.Objects[VisualsCombo.ItemIndex]);
          DR.Name := NameEdit.Text;
          Result := True;
        except
          on E : Exception do
            Application.ShowException(E);
        end;
      end;
    end;
  finally
    RestraintEdit.Free;
    RestraintEdit := nil;
  end;
end;

function TRestraintEdit.ReadEdit(E : TEdit) : Double;
var Code : Integer;
begin
  Val(E.Text, Result, Code);
  if Code <> 0 then
    raise Exception.Create('Invalid data entry');
end;




procedure TRestraintEdit.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  if ModalResult = mrOK then begin
    try
      ReadEdit(LeftEdit);
      ReadEdit(TopEdit);
      ReadEdit(WidthEdit);
      ReadEdit(HeightEdit);
    except
      on E : Exception do begin
        Application.ShowException(E);
        Action := caNone;
      end;
    end;
  end;
end;

end.
