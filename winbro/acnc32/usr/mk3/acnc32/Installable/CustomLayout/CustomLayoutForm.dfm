object LayoutDesignForm: TLayoutDesignForm
  Left = 200
  Top = 108
  Width = 483
  Height = 377
  Caption = 'LayoutDesignForm'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = True
  PopupMenu = PopupMenu1
  OnCreate = FormCreate
  OnMouseDown = FormMouseDown
  OnMouseMove = FormMouseMove
  OnMouseUp = FormMouseUp
  OnPaint = FormPaint
  PixelsPerInch = 96
  TextHeight = 13
  object PopupMenu1: TPopupMenu
    Left = 24
    Top = 8
    object SelectDisplay1: TMenuItem
      Caption = 'Select Layout'
      OnClick = SelectDisplay1Click
    end
    object ChangeDefault1: TMenuItem
      Caption = 'Change Default Display'
      OnClick = ChangeDefault1Click
    end
    object SetDisplayasprimary1: TMenuItem
      Caption = 'Set Display as primary'
      OnClick = SetDisplayasprimary1Click
    end
    object NameRestraing1: TMenuItem
      Caption = 'Name Restraint'
      OnClick = NameRestraing1Click
    end
    object ModeMenu1: TMenuItem
      Caption = 'Mode Menu'
    end
    object SplitV: TMenuItem
      Caption = 'Split Vertical'
      OnClick = SplitVClick
    end
    object SplitHorizontal1: TMenuItem
      Caption = 'Split Horizontal'
      OnClick = SplitHorizontal1Click
    end
    object EditAsText1: TMenuItem
      Caption = 'Edit As Text'
      OnClick = EditAsText1Click
    end
    object Delete1: TMenuItem
      Caption = 'Delete'
      OnClick = Delete1Click
    end
    object SaveandExit1: TMenuItem
      Caption = 'Save and Exit'
      OnClick = SaveandExit1Click
    end
    object Exitwithoutsaving1: TMenuItem
      Caption = 'Exit without saving'
      OnClick = Exitwithoutsaving1Click
    end
    object Saveanduse: TMenuItem
      Caption = 'Save and Use this display'
      OnClick = SaveanduseClick
    end
  end
end
