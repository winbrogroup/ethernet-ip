unit SelectVisualForm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  CoreCNC, StdCtrls, Buttons, ExtCtrls;

type
  TSelectVisual = class(TForm)
    VisualListBox: TListBox;
    Panel1: TPanel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    procedure VisualListBoxDblClick(Sender: TObject);
  private
    { Private declarations }
  public
    class function Execute(Visuals : TList) : TAbstractDisplay;
  end;

implementation

{$R *.DFM}

var
  SelectVisual: TSelectVisual;

class function TSelectVisual.Execute(Visuals : TList) : TAbstractDisplay;
var I : Integer;
begin
  Result := nil;
  try
    if not Assigned(SelectVisual) then begin
      SelectVisual := TSelectVisual.Create(Application);
      SelectVisual.VisualListBox.Items.AddObject('NONE', nil);
      for I := 0 to Visuals.Count -1 do begin
        SelectVisual.VisualListBox.Items.AddObject(TCNC(Visuals[I]).Name, Visuals[I]);
      end;
      if (SelectVisual.ShowModal = mrOk) and (SelectVisual.VisualListBox.ItemIndex <> -1) then
        Result := TAbstractDisplay(SelectVisual.VisualListBox.Items.Objects[SelectVisual.VisualListBox.ItemIndex])
      else
        Result := nil;
    end;
  finally
    SelectVisual := nil;
  end;
end;

procedure TSelectVisual.VisualListBoxDblClick(Sender: TObject);
begin
  ModalResult := mrOK;
end;

end.
