object SelectLayout: TSelectLayout
  Left = 504
  Top = 292
  Width = 409
  Height = 413
  Caption = 'SelectLayout'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = True
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 345
    Width = 401
    Height = 41
    Align = alBottom
    TabOrder = 0
    object BitBtn1: TBitBtn
      Left = 24
      Top = 8
      Width = 75
      Height = 25
      TabOrder = 0
      Kind = bkOK
    end
    object BitBtn2: TBitBtn
      Left = 112
      Top = 8
      Width = 75
      Height = 25
      TabOrder = 1
      Kind = bkCancel
    end
  end
  object LayoutPanel: TPanel
    Left = 0
    Top = 0
    Width = 401
    Height = 233
    Align = alTop
    TabOrder = 1
    object GroupBox1: TGroupBox
      Left = 1
      Top = 1
      Width = 232
      Height = 231
      Align = alLeft
      Caption = 'Layouts'
      TabOrder = 0
      object LayoutListBox: TListBox
        Left = 2
        Top = 15
        Width = 228
        Height = 214
        Align = alClient
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Courier New'
        Font.Style = []
        ItemHeight = 18
        ParentFont = False
        TabOrder = 0
        OnClick = LayoutListBoxClick
      end
    end
    object GroupBox2: TGroupBox
      Left = 233
      Top = 1
      Width = 167
      Height = 231
      Align = alClient
      Caption = 'Layout Properties'
      TabOrder = 1
      object Label1: TLabel
        Left = 8
        Top = 16
        Width = 29
        Height = 13
        Caption = 'Signal'
      end
      object SpeedButton1: TSpeedButton
        Left = 128
        Top = 32
        Width = 15
        Height = 21
        Caption = '...'
        OnClick = SpeedButton1Click
      end
      object ManualCheck: TCheckBox
        Left = 16
        Top = 72
        Width = 97
        Height = 17
        Caption = 'Manual'
        TabOrder = 0
        OnClick = ModeCheckClick
      end
      object Autocheck: TCheckBox
        Left = 16
        Top = 96
        Width = 97
        Height = 17
        Caption = 'Auto'
        TabOrder = 1
        OnClick = ModeCheckClick
      end
      object MDICheck: TCheckBox
        Left = 16
        Top = 120
        Width = 97
        Height = 17
        Caption = 'MDI'
        TabOrder = 2
        OnClick = ModeCheckClick
      end
      object TeachInCheck: TCheckBox
        Left = 16
        Top = 144
        Width = 97
        Height = 17
        Caption = 'Teach-In'
        TabOrder = 3
        OnClick = ModeCheckClick
      end
      object Editcheck: TCheckBox
        Left = 16
        Top = 168
        Width = 97
        Height = 17
        Caption = 'Edit'
        TabOrder = 4
        OnClick = ModeCheckClick
      end
      object SignalText: TEdit
        Left = 8
        Top = 32
        Width = 121
        Height = 21
        TabOrder = 5
        OnExit = SignalTextExit
      end
      object SingleOpenCheckBox: TCheckBox
        Left = 16
        Top = 208
        Width = 97
        Height = 17
        Caption = 'Single Open'
        TabOrder = 6
        OnClick = SingleOpenCheckBoxClick
      end
      object Panel2: TPanel
        Left = 16
        Top = 194
        Width = 129
        Height = 4
        TabOrder = 7
      end
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 233
    Width = 401
    Height = 112
    Align = alClient
    TabOrder = 2
    object Label2: TLabel
      Left = 184
      Top = 8
      Width = 203
      Height = 80
      Caption = 
        'Changes made in this dialog cannot be undone with out selecting ' +
        'reload from disk in a higher menu'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      WordWrap = True
    end
    object ButtonNewLayout: TButton
      Left = 16
      Top = 8
      Width = 75
      Height = 25
      Caption = 'New Layout'
      TabOrder = 0
      OnClick = ButtonNewLayoutClick
    end
    object ButtonCopyLayout: TButton
      Left = 16
      Top = 40
      Width = 75
      Height = 25
      Caption = 'Copy Layout'
      TabOrder = 1
      OnClick = ButtonCopyLayoutClick
    end
    object ButtonDeleteLayout: TButton
      Left = 96
      Top = 8
      Width = 75
      Height = 25
      Caption = 'Delete'
      TabOrder = 2
      OnClick = ButtonDeleteLayoutClick
    end
    object ButtonRenameLayout: TButton
      Left = 96
      Top = 40
      Width = 75
      Height = 25
      Caption = 'Rename'
      TabOrder = 3
      OnClick = ButtonRenameLayoutClick
    end
  end
end
