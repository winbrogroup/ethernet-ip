unit CustomLayout;

interface

uses Classes, CoreCNC, CNC32, CNCTypes, Tokenizer, SysUtils, Windows, Forms,
     Graphics, NCNames, ExtCtrls, Controls, Named, SyncObjs, Messages, BaseKeyboards,
     IFSDServer;

type
  TBoundarySide = (
    bsLeft,
    bsRight,
    bsTop,
    bsBottom
  );

  TCustomLayoutTokenizer = class(TQuoteTokenizer)
  public
    function Read : string; override;
    function ReadAssignmentDouble : Double;
  end;

  TDisplayRestraint = class(TObject)
  public
    Top, Left, Width, Height : Double;
    DefaultDisplay : TAbstractDisplay;
    CurrentDisplay : TAbstractDisplay;
    Index : Integer;
    ProtectedDisplay : Boolean;
    DontOpen : Boolean;
    Name : string;
    procedure Read(St : TCustomLayoutTokenizer);
    procedure Write(St : TStrings);
    function ContainedIn(Display : TDisplayRestraint) : Boolean;
    procedure AdjustTo(Was, Now : TDisplayRestraint; Bs : TBoundarySide);
    procedure Duplicate(aRestraint : TDisplayRestraint);
  end;

  TRestraintList = class(TObject)
  private
    List : TList;
    FName : string;
    function GetRestraint(aIndex : Integer) : TDisplayRestraint;
  public
    DefaultMode : TCNCModes;
    SignalName : string;
    Signal : TAbstractTag;
    SingleOpen : Boolean; // This layout contains only one openable window.
    constructor Create(aName : string);
    procedure AddRestraint(aRestraint : TDisplayRestraint);
    procedure Delete(aIndex : Integer);
    procedure DeleteRestraint(aRestraint : TDisplayRestraint);
    function Count : Integer;
    property Restraint[Index : Integer] : TDisplayRestraint read GetRestraint; default;
    procedure Read(St : TCustomLayoutTokenizer);
    procedure Write(St : TStrings);
    property Name : string read FName write FName;
    procedure MakePrimary(aRestraint : TDisplayRestraint);
  end;

  TDisplayLayoutList = class(TObject)
  private
    Width, Height : Integer;
    List : TList;
    FThisLayout : Integer;
    function GetLayout(aIndex : Integer) : TRestraintList;
    function GetDefaultLayout : TRestraintList;
    procedure SetThisLayout(aIndex : Integer);
  public
    constructor Create;
    destructor Destroy; override;
    procedure AddLayout(aDisplay : TRestraintList);
    function Count : Integer;
    procedure DeleteLayout(aIndex : Integer);
    procedure Clean;
    property Layout[Index : Integer] : TRestraintList read GetLayout; default;
    property DefaultLayout : TRestraintList read GetDefaultLayout ;
    procedure Read(St : TCustomLayoutTokenizer);
    procedure Write(St : TStrings);
    property ThisLayout : Integer read FThisLayout write SetThisLayout;
    function YDims(aVal : Double) : Integer;
    function XDims(aVal : Double) : Integer;
    function XPCDims(aVal : Integer) : Double;
    function YPCDims(aVal : Integer) : Double;
  end;


  TMaxDisplay = 0..20;

  TCustomLayoutManager = class(TAbstractLayoutManager)
  private
    LayoutList : TDisplayLayoutList;
    Signals : TStringList;
    LayoutEditing : Boolean;
    ScreenSaverComponent : TAbstractDisplay;
    Event : TEvent;
    Keyboard : TBaseKB;
    NumericPad : TBaseFPad;
    PrimaryDisplay: TAbstractTag;

    ModeChangeReset: Boolean;
    SingleOpen: Boolean;

    procedure ModeChange(aTag : TAbstractTag);
    procedure Clean;
    procedure ReadDisplayFile;
    procedure ReadDisplayConfig(St : TCustomLayoutTokenizer);
    procedure OpenIn(aDisplay : TAbstractDisplay; Res : TDisplayRestraint; WithTitle: Boolean= False);
    procedure EditConfig(aTag : TAbstractTag);
    procedure DisplayEvent(aTag : TAbstractTag);
    procedure NormalTo;
    procedure CancelScreenSaver(aTag : TAbstractTag);
    procedure CNCM_SCREEN_SAVER_PROC(var Message : TMessage); message CNCM_SCREEN_SAVER;
    procedure PositionNumeric(Display : TABstractDisplay);
    procedure PositionKeyboard(Display : TABstractDisplay);
  protected
    procedure SetScreenSaver(aSaver : Boolean); override;
    procedure SetDimensions(aDisplay : TAbstractDisplay; Res : TDisplayRestraint; WithTitle: Boolean=False);
  public
    procedure Add   (aDisplay : TAbstractDisplay); override;
    procedure Open  (aDisplay : TAbstractDisplay); override;
    procedure Close (aDisplay : TAbstractDisplay); override;
    procedure Normal; override;
    procedure Swap  (aDisplay : TAbstractDisplay); override;
    procedure Expand (aDisplay : TAbstractDisplay; n : integer); override;
    procedure HTTPCreateImageMap(hFile : THandle); override;
    function HTTPGeneratePage(HFile : THANDLE; var CGIPath: WideString;
             CGIRequest : ICGIStringList) : Boolean; override;

    constructor Create(aOwner : TComponent); override;
    destructor Destroy; override;
    procedure InstallComponent (Params : TParamStrings); override;
    procedure Installed; override;
    function  ValidDisplay     (D : Integer) : Integer; override;
    procedure ShowKeyboard(Display : TAbstractDisplay); override;
    procedure ShowNumericPad(Display : TAbstractDisplay); override;
    procedure ShowNavigation(Display : TAbstractDisplay); override;
    procedure HideKeyboard; override;
    procedure HideNumericPad; override;
    procedure HideNavigation; override;
  end;

  TSaverThread = class(TThread)
  private
    Layout : TCustomLayoutManager;
  public
    constructor Create(aLayout : TCustomLayoutManager);
    procedure Execute; override;
  end;

implementation

uses CustomLayoutForm, ScreenSaverFrm;

var ModeStrings : TStringList;

const TitleHeight = 25;

procedure TDisplayRestraint.Read(St : TCustomLayoutTokenizer);
var Tmp : string;
    Comp : TAbstractCNC;
    CompName : string;
begin
  Top := 0;
  Left := 0;
  Width := 50;
  Height := 50;
  DefaultDisplay := nil;
  ProtectedDisplay := False;
  DontOpen := False;

  St.ExpectedToken('display');
  Index := St.ReadIndex(100);
  St.ExpectedToken('=');
  St.ExpectedToken('{');

  Tmp := LowerCase(St.Read);
  while not St.EndOfStream do begin
    if Tmp = 'top' then
      Top := St.ReadAssignmentDouble
    else if Tmp = 'left' then
      Left := St.ReadAssignmentDouble
    else if Tmp = 'width' then
      Width := St.ReadAssignmentDouble
    else if Tmp = 'height' then
      Height := St.ReadAssignmentDouble
    else if Tmp = 'protected' then
      ProtectedDisplay := St.ReadBooleanAssign
    else if Tmp = 'dontopen' then
      DontOpen := St.ReadBooleanAssign
    else if Tmp = 'name' then
      Name := St.ReadStringAssign
    else if Tmp = '}' then
      Exit
    else if Tmp = 'defaultdisplay' then begin
      St.ExpectedToken('=');
      CompName := St.Read;
      Comp := TCNC.GetComponent(CompName);
      if Comp is TAbstractDisplay then
        DefaultDisplay := TAbstractDisplay(Comp);
    end else
      raise Exception.CreateFmt('Unknown restraint token [%s]', [Tmp]);
    Tmp := LowerCase(St.Read);
  end;
end;

procedure TDisplayRestraint.Write(St : TStrings);
begin
  St.Add(Format('  display[%d] = {', [Index]));
  St.Add(Format('  left = %f', [Left]));
  St.Add(Format('  top = %f', [Top]));
  St.Add(Format('  width = %f', [Width]));
  St.Add(Format('  height = %f', [Height]));
  St.Add(Format('  name = ''%s''', [Name]));
  if ProtectedDisplay then
    St.Add('  protected = True');
  if DontOpen then
    St.Add('  DontOpen = True');
  if Assigned(DefaultDisplay) then
    St.Add(Format('  defaultdisplay = %s', [DefaultDisplay.Name]));
  St.Add('  }');
end;

function TDisplayRestraint.ContainedIn(Display : TDisplayRestraint) : Boolean;
begin
  Result := (Left > Display.Left) and
            (Left + Width < Display.Left + Width) and
            (Top > Display.Top) and
            (Top + Height < Display.Top + Display.Height);
end;

procedure TDisplayRestraint.AdjustTo(Was, Now : TDisplayRestraint; Bs : TBoundarySide);
  function ReasonableRange(A, B : Double) : Boolean;
  begin
    Result := Abs(A-B) < 1;
  end;

begin
  // 1. Was the boundary shared
  case Bs of
    bsLeft : if ReasonableRange(Left + Width, Was.Left) then
               Width := Now.Left - Left;

    bsRight : if ReasonableRange(Left, Was.Left + Was.Width) then begin
               Width := Width + Was.Width - Now.Width;
               Left := Now.Left + Now.Width;
    end;

    bsTop : if ReasonableRange(Top + Height, Was.Top) then
              Height := Now.Top - Top;

    bsBottom : if ReasonableRange(Top, Was.Top + Was.Height) then begin
               Height := Height + Was.Height - Now.Height;
               Top := Now.top + Now.Height;
    end;
  end;
end;

procedure TDisplayRestraint.Duplicate(aRestraint : TDisplayRestraint);
begin
  Self.Top := aRestraint.Top;
  Self.Left := aRestraint.Left;
  Self.Width := aRestraint.Width;
  Self.Height := ARestraint.Height;
  Self.DefaultDisplay := aRestraint.DefaultDisplay;
end;

constructor TRestraintList.Create(aName : string);
begin
  inherited Create;
  Name := aName;
  List := TList.Create;
end;

procedure TRestraintList.Read(St : TCustomLayoutTokenizer);
var Tmp : string;
    I : Integer;
    DisplayRestraint : TDisplayRestraint;
begin
  DefaultMode := [];
  St.ExpectedToken('=');
  St.ExpectedToken('{');
  Tmp := LowerCase(St.Read);
  while not St.EndOfStream do begin
    if Tmp = 'defaultmode' then begin
      St.ExpectedToken('=');
      I := StrToInt(St.Read);
      DefaultMode := TCNCModes(Byte(I));
    end else if Tmp = 'signalname' then begin
      SignalName := St.ReadStringAssign;
    end else if Tmp = 'singleopen' then begin
      SingleOpen := St.ReadBooleanAssign;
    end else if Tmp = 'display' then begin
      DisplayRestraint := TDisplayRestraint.Create;
      try
        St.Push;
        DisplayRestraint.Read(St);
        AddRestraint(DisplayRestraint);
      except
        DisplayRestraint.Free;
        raise;
      end;
    end else if Tmp = '}' then begin
      Exit;
    end else
      raise Exception.CreateFmt('Unknown restraint token [%s]', [Tmp]);
    Tmp := LowerCase(St.Read);
  end;
end;

procedure TRestraintList.Write(St : TStrings);
var I : Integer;
begin
  St.Add(Format('%s = {', [Name]));
  St.Add(Format('defaultmode = %d', [Byte(DefaultMode)]));
  if SignalName <> '' then
    St.Add(Format('signalname = %s', [SignalName]));
  St.Add(Format('SingleOpen = %d', [Ord(SingleOpen)]));
  for I := 0 to Count - 1 do begin
    Restraint[I].Write(St);
  end;
  St.Add('}');
end;

function TRestraintList.GetRestraint(aIndex : Integer) : TDisplayRestraint;
begin
  Result := TDisplayRestraint(List[aIndex]);
end;

procedure TRestraintList.AddRestraint(aRestraint : TDisplayRestraint);
begin
  List.Add(aRestraint);
end;

procedure TRestraintList.MakePrimary(aRestraint : TDisplayRestraint);
var I : Integer;
begin
  I := List.IndexOf(aRestraint);
  if I <> -1 then begin
    List.Delete(I);
    List.Insert(0, aRestraint);
  end;
end;

procedure TRestraintList.Delete(aIndex : Integer);
begin
  TObject(List[aIndex]).Free;
  List.Delete(aIndex);
end;

procedure TRestraintList.DeleteRestraint(aRestraint : TDisplayRestraint);
var I : Integer;
begin
  I := List.IndexOf(aRestraint);
  if I <> -1 then begin
    TObject(List[I]).Free;
    List.Delete(I);
  end
end;

function TRestraintList.Count : Integer;
begin
  Result := List.Count;
end;



constructor TDisplayLayoutList.Create;
begin
  inherited Create;
  List := TList.Create;
end;

destructor TDisplayLayoutList.Destroy;
begin
  Clean;
  inherited Destroy;
end;

procedure TDisplayLayoutList.Read(St : TCustomLayoutTokenizer);
var Tmp : string;
    RS : TRestraintList;
begin
  Tmp := LowerCase(St.Read);
  while not ST.EndOfStream do begin
    if Tmp = 'width' then
      Width := Round(St.ReadAssignmentDouble)
    else if Tmp = 'height' then
      Height := Round(St.ReadAssignmentDouble)
    else if Tmp = '}' then
      Exit
    else begin
      RS := TRestraintList.Create(Tmp);
      RS.Read(St);
      Self.AddLayout(RS);
    end;
    Tmp := LowerCase(St.Read);
  end;
end;

procedure TDisplayLayoutList.Write(St : TStrings);
var I : Integer;
begin
  St.Add('Layout = {');
  St.Add(Format('width = %d', [Width]));
  St.Add(Format('height = %d', [Height]));
  for I := 0 to Count - 1 do begin
    Self.Layout[I].Write(St);
  end;
  St.Add('}');
end;

function TDisplayLayoutList.GetLayout(aIndex : Integer) : TRestraintList;
begin
  Result := TRestraintList(List[aIndex]);
end;

procedure TDisplayLayoutList.SetThisLayout(aIndex : Integer);
begin
  if aIndex <> FThisLayout then begin
    FThisLayout := aIndex;
    for aIndex := 0 to DefaultLayout.Count - 1 do begin
      DefaultLayout[aIndex].CurrentDisplay := nil;
    end;
  end;
end;

function TDisplayLayoutList.GetDefaultLayout : TRestraintList;
begin
  if ThisLayout >= Count then
    ThisLayout := 0;

  Result := Layout[ThisLayout];
end;


procedure TDisplayLayoutList.AddLayout(aDisplay : TRestraintList);
begin
  List.Add(aDisplay);
end;

function TDisplayLayoutList.Count : Integer;
begin
  Result := List.Count;
end;

procedure TDisplayLayoutList.DeleteLayout(aIndex : Integer);
begin
  TObject(List[aIndex]).Free;
  List.Delete(aIndex);
end;

procedure TDisplayLayoutList.Clean;
begin
  while Count > 0 do
    DeleteLayout(0)
end;

function TDisplayLayoutList.XDims(aVal : Double) : Integer;
begin
  Result := Round((aVal * Width) / 100);
end;

function TDisplayLayoutList.YDims(aVal : Double) : Integer;
begin
  Result := Round((aVal * Height) / 100);
end;

function TDisplayLayoutList.XPCDims(aVal : Integer) : Double;
begin
  Result := (aVal / Width) * 100;
end;

function TDisplayLayoutList.YPCDims(aVal : Integer) : Double;
begin
  Result := (aVal / Height) * 100;
end;




const DisplayFileNotFound = 'Display configuration file [%s] not found';

constructor TCustomLayoutManager.Create(aOwner : TComponent);
begin
  inherited;
  //DisplayList := TList.Create;
  LayoutList := TDisplayLayoutList.Create;
  LayoutList.ThisLayout := 0;
  Signals := TStringList.Create;
  {
  TitlePanel1:= TPanel.Create(Self);
  TitlePanel1.Parent:= SysObj.VisualParent;
  TitlePanel1.Visible:= False;
  TitlePanel2:= TPanel.Create(Self);
  TitlePanel2.Parent:= SysObj.VisualParent;
  TitlePanel2.Visible:= False;
  }
end;

destructor TCustomLayoutManager.Destroy;
begin
  DisplayList.Free;
  LayoutList.Free;
  Signals.Free;
  inherited Destroy;
end;

procedure TCustomLayoutManager.Installed;
var Saver : TSaverThread;
    I: Integer;
begin
  inherited;
  ReadDisplayFile;
  FDisplayWidth := LayoutList.Width;
  FDisplayHeight := LayoutList.Height;
  TForm(owner).Width := LayoutList.Width;
  TForm(owner).Height := LayoutList.Height;
  LayoutList.Width := TForm(owner).clientWidth;
  LayoutList.Height := TForm(owner).clientHeight;

  TagEvent(Name + 'Config', EdgeFalling, TMethodTag, EditConfig);
  TagEvent(Name + 'CancelScreenSaver', EdgeChange, TMethodTag, CancelScreenSaver);

  with NCPublishedF[ncpActiveMode] do
    TagEvent(Format(Tag, [1]), edgeChange, Size, ModeChange);

  if not Assigned(ScreenSaverComponent) then
    ScreenSaverComponent := Self.Displays[0];

  Event := TEvent.Create(nil, True, False, 'ACNC32ScreenSaver');
  Saver := TSaverThread.Create(Self);
  Saver.Resume;

  Keyboard := TBaseKB.Create(Self);
  Keyboard.Parent := SysObj.VisualParent;
  Keyboard.Width := 650;
  Keyboard.Height := 250;
  Keyboard.Visible := False;

  Numericpad := TBaseFPad.Create(Self);
  NumericPad.Parent := SysObj.VisualParent;
  NumericPad.Visible := False;

  for I:= 0 to DisplayList.Count - 1 do begin
    GetDisplay(I).VisibleTag:= Self.TagPublish('Layout' + GetDisplay(I).Name, TIntegerTag)
  end;
end;

procedure TCustomLayoutManager.CNCM_SCREEN_SAVER_PROC(var Message : TMessage);
begin
  ScreenSaver := True;
end;

procedure TCustomLayoutManager.CancelScreenSaver(aTag : TAbstractTag);
begin
  ScreenSaver := False;
end;

procedure TCustomLayoutManager.SetScreenSaver(aSaver : Boolean);
begin
  if FScreenSaver and aSaver then
    Exit;

  FScreenSaver := aSaver;

  if FScreenSaver then begin
    try
      TSaverScreen.Execute(ScreenSaverComponent);
    finally
      ScreenSaver := False;
      NormalTo;
    end;
  end else begin
    TSaverScreen.CancelScreenSaver;
  end;
end;

procedure TCustomLayoutManager.ReadDisplayFile;
var St : TCustomLayoutTokenizer;
begin
  LayoutList.Clean;
  St := TCustomLayoutTokenizer.Create;
  try
    try
      ReadLive(St, 'Layout');
    except
      raise ECNCInstallFault.CreateFmt(DisplayFileNotFound, ['Layout']);
    end;

    try
      ReadDisplayConfig(St);
    except
      on E : Exception do begin
        CoreCNC.MessageLog(Format('Failed CustomLayout [%s]', [E.Message]));
        raise;
      end;
    end;
  finally
    St.Free;
  end;
end;

procedure TCustomLayoutManager.DisplayEvent(aTag : TAbstractTag);
var I : Integer;
begin
  for I := 0 to LayoutList.Count - 1 do begin
    if LayoutList.Layout[I].Signal = aTag then begin
      LayoutList.ThisLayout := I;
      NormalTo;
      Exit;
    end;
  end;
end;

procedure TCustomLayoutManager.EditConfig(aTag : TAbstractTag);
var S : TStringList;
    This : Integer;
begin
  This := LayoutList.ThisLayout;
  S := TStringList.Create;
  LayoutEditing := True;
  try
    S.Clear;
    case TLayoutDesignForm.Execute(TForm(Owner), LayoutList, DisplayList) of
      lfaSaveDisplay : begin
        Self.LayoutList.Write(S);
        WriteLive(S, 'Layout');
        Self.ReadDisplayFile;
        LayoutList.ThisLayout := This;
        Normal;
      end;

      lfaReloadDisplay : begin
        ReadDisplayFile;
        Normal;
      end;

      lfaSaveAndUseDisplay : begin
        Self.LayoutList.Write(S);
        WriteLive(S, 'Layout');
//        S.SaveToFile(DisplayFile);
        Self.ReadDisplayFile;
        NormalTo;
      end;
    end;
  finally
    S.Free;
    LayoutEditing := False;
  end;
end;


procedure TCustomLayoutManager.InstallComponent (Params : TParamStrings);
var ScreenSaverComponentName : string;
    Tmp : TAbstractCNC;
begin
  inherited InstallComponent(Params);
  ScreenSaverComponentName := Params.ReadString('ScreenSaverComponent', 'nil');

  ModeChangeReset := Params.ReadBool('ModeChangeReset', True);
  SingleOpen := Params.ReadBool('SingleOpen', False);
  Tmp := SysObj.Comms.GetComponent(ScreenSaverComponentName);
  if Tmp is TAbstractDisplay then
    ScreenSaverComponent := TAbstractDisplay(Tmp);

  SysObj.HttpServerHost.AddComponentPath(HttpHelpSection, Name, 'Context Help', Self);
  PrimaryDisplay := TagPublish(NamePrimaryDisplay, TStringTag);
end;

procedure TCustomLayoutManager.Clean;
begin
  while LayoutList.Count > 0 do begin
    TObject(LayoutList[0]).Free;
    LayoutList.DeleteLayout(0);
  end;
end;



procedure TCustomLayoutManager.ReadDisplayConfig(St : TCustomLayoutTokenizer);
var I, Tmp : Integer;
    RL : TRestraintList;
begin
  Clean;
  St.Rewind;
  St.Seperator := '[]=#';
  St.QuoteChar := '''';

  St.ExpectedToken('layout');
  St.ExpectedToken('=');
  St.ExpectedToken('{');

  LayoutList.Read(St);

  for I := 0 to LayoutList.Count - 1 do begin
    RL := LayoutList.Layout[I];
    if RL.SignalName <> '' then begin
      Tmp := Signals.IndexOf(UpperCase(RL.SignalName));
      if Tmp = -1 then begin
        RL.Signal := TagEvent(RL.SignalName, edgeFalling, TMethodTag, DisplayEvent);
        Signals.AddObject(UpperCase(RL.SignalName), RL.Signal);
      end else begin
        RL.Signal := TAbstractTag(Signals.Objects[Tmp]);
      end;
    end;
  end;
end;

const
  CNCBorder = 2;

procedure TCustomLayoutManager.SetDimensions(aDisplay : TAbstractDisplay; Res : TDisplayRestraint; WithTitle: Boolean=False);
var th: Integer;
begin
  if WithTitle then
    th:= TitleHeight
  else
    th:= 0;

  aDisplay.SetBounds( LayoutList.XDims(Res.Left) + CNCBorder,
                      LayoutList.YDims(Res.Top) + CNCBorder + th,
                      LayoutList.XDims(Res.Width) - (CNCBorder * 2),
                      LayoutList.YDims(Res.Height) - (CNCBorder * 2) - th);
end;

procedure TCustomLayoutManager.Add(aDisplay : TAbstractDisplay);
begin
  DisplayList.Add(aDisplay);
end;

//
// Note this must only be called by the client
//
procedure TCustomLayoutManager.Open(aDisplay : TAbstractDisplay);
var I, J : Integer;
    Vis: Boolean;
begin
  HideKeyboard;
  // This display is already opened
  try
    for I := 0 to LayoutList.DefaultLayout.Count - 1 do begin
      if LayoutList.DefaultLayout[I].CurrentDisplay = aDisplay then begin
        SetDimensions(aDisplay, LayoutList.DefaultLayout[I], ADisplay.UseTitle);
        Exit;
      end;
    end;
  finally
    aDisplay.LayoutDemanded := True;
  end;

  // The display has a default slot in the current display
  for I := 0 to LayoutList.DefaultLayout.Count - 1 do begin
    if LayoutList.DefaultLayout[I].DefaultDisplay = aDisplay then begin
      OpenIn(aDisplay, LayoutList.DefaultLayout[I], ADisplay.UseTitle);
      Exit;
    end;
  end;

  if aDisplay.ActiveDisplay <> '' then begin
    for I := 0 to LayoutList.DefaultLayout.Count - 1 do begin
      if CompareText(aDisplay.ActiveDisplay, LayoutList.DefaultLayout[I].Name) = 0 then begin
        OpenIn(aDisplay, LayoutList.DefaultLayout[I], ADisplay.UseTitle);
        Exit;
      end;
    end;
  end;
  if (LayoutList.DefaultLayout[0].CurrentDisplay =
     LayoutList.DefaultLayout[0].DefaultDisplay) or
     not Assigned(LayoutList.DefaultLayout[0].CurrentDisplay) or
     LayoutList.DefaultLayout.SingleOpen then
     OpenIn(aDisplay, LayoutList.DefaultLayout[0], ADisplay.UseTitle)
  else
    if SingleOpen then
       OpenIn(aDisplay, LayoutList.DefaultLayout[0], ADisplay.UseTitle)
    else
       OpenIn(aDisplay, LayoutList.DefaultLayout[1], ADisplay.UseTitle);

  for I:= 0 to DisplayList.Count - 1 do begin
    Vis:= False;
    for J:= 0 to LayoutList.DefaultLayout.Count - 1 do begin
      if DisplayList[I] = LayoutList.DefaultLayout[J].CurrentDisplay then
        Vis:= True;
    end;
    GetDisplay(I).VisibleTag.AsBoolean:= Vis;
  end;
end;

// Current policy is that closing a display does not invoke the default
// display this is a general policy that avoids code race.
procedure TCustomLayoutManager.Close(aDisplay : TAbstractDisplay);
var I : Integer;
begin
  aDisplay.Closing;
  aDisplay.Visible := False;
  aDisplay.LayoutDemanded := False;

  for I := 0 to LayoutList.DefaultLayout.Count - 1 do begin
    if LayoutList.DefaultLayout[I].CurrentDisplay = aDisplay then begin
      LayoutList.DefaultLayout[I].CurrentDisplay := nil;
      //LayoutList.DefaultLayout[I].DefaultDisplay.Open;
    end;
  end;
end;

procedure TCustomLayoutManager.OpenIn(aDisplay : TAbstractDisplay; Res : TDisplayRestraint; WithTitle: Boolean= False);
begin
  if Assigned(aDisplay) then begin
    SetDimensions(aDisplay, Res, WithTitle);
    if Assigned(Res.CurrentDisplay) and not Res.CurrentDisplay.ExternalLayout then
      Res.CurrentDisplay.Close;
    Res.CurrentDisplay := aDisplay;
    aDisplay.Visible := True;
    aDisplay.BringToFront;
    aDisplay.LayoutDemanded := True;

    if WithTitle then
      aDisplay.ShowTitlePanel(TitleHeight)
    else
      aDisplay.HideTitlePanel;

    if Res = LayoutList.DefaultLayout[0] then
      PrimaryDisplay.AsString:= aDisplay.Name;
  end;
end;

// Normalizes display to Layout.ThisLayout
procedure TCustomLayoutManager.NormalTo;
var RL : TRestraintList;
    I : Integer;
    ThisDisplay : TAbstractDisplay;
    Disp : TAbstractDisplay;
begin
  if LayoutList.Count = 0 then
    Exit;

  HideKeyboard;
  HideNumericPad;

  if LayoutList.ThisLayout > LayoutList.Count then
    LayoutList.ThisLayout := 0;

  RL := LayoutList.DefaultLayout;

  for I := 0 to DisplayList.Count - 1 do begin
    Disp := TAbstractDisplay(DisplayList[I]);
    Disp.LayoutDemanded := False;
  end;

  for I := 0 to RL.Count - 1 do begin
    ThisDisplay := nil;
    if Assigned(RL[I].DefaultDisplay) then begin
      ThisDisplay := RL[I].DefaultDisplay;
    end;
    if Assigned(ThisDisplay) and not RL[I].DontOpen then
      ThisDisplay.Open;
  end;

//  CheckForOrphanedDisplays
  for I := 0 to DisplayList.Count - 1 do begin
    Disp := TAbstractDisplay(DisplayList[I]);
    if not Disp.LayoutDemanded and Disp.Visible and not Disp.ExternalLayout then begin
      //Disp.Close;
      Disp.Visible:= False;
      Disp.HideTitlePanel;
    end;
  end;
end;

procedure TCustomLayoutManager.Normal;
begin
  if not LayoutEditing then begin
    ModeChange(nil);
    if not ModeChangeReset then
      NormalTo;
  end
  else
    NormalTo;
end;

procedure TCustomLayoutManager.ModeChange(aTag : TAbstractTag);
var I : Integer;
begin
  for I := 0 to LayoutList.Count - 1 do begin
    if CoreCNC.SysObj.NC.CNCMode <= LayoutList.Layout[I].DefaultMode then begin
      LayoutList.ThisLayout := I;
      if ModeChangeReset then
        NormalTo;
      Exit;
    end;
  end;
  LayoutList.ThisLayout := 0;
  NormalTo;
end;


procedure TCustomLayoutManager.Swap(aDisplay : TAbstractDisplay);
var A, B: TAbstractDisplay;
begin
  A:= LayoutList.DefaultLayout[0].CurrentDisplay;
  B:= LayoutList.DefaultLayout[1].CurrentDisplay;
  if A = LayoutList.DefaultLayout[0].DefaultDisplay then
    Exit;

  if B = LayoutList.DefaultLayout[1].DefaultDisplay then
    Exit;

  Close(A);
  Close(B);

  Self.OpenIn(B, LayoutList.DefaultLayout[0], True);
  Self.OpenIn(A, LayoutList.DefaultLayout[1], True);
end;

function TrueDisplay(aDisp : Integer) : Integer;
begin
  Result := aDisp mod 10;
end;

procedure TCustomLayoutManager.Expand (aDisplay : TAbstractDisplay; n : integer);
var l1, t1, w1, h1 : Double;
    P1, P2 : TDisplayRestraint;
    I : Integer;
begin
  if not aDisplay.LayoutDemanded then begin
    aDisplay.Open;
    Exit;
  end;

  if LayoutList.DefaultLayout.Count < 2 then
    Exit;

  P1 := LayoutList.DefaultLayout[0];
  P2 := LayoutList.DefaultLayout[1];

  for I := 2 to LayoutList.DefaultLayout.Count - 1 do
    if LayoutList.DefaultLayout[I].CurrentDisplay = aDisplay then
      P2 := LayoutList.DefaultLayout[I];

  if Assigned(P1) and Assigned(P2) then begin

    { Determine cross section l,t : r,b }
    { Which is the left most component }
    if P1.Left < P2.Left then
      l1 := P1.Left
    else
      l1 := P2.Left;

    { Which is the top most component }
    if P1.Top < P2.Top then
      t1 := P1.Top
    else
      t1 := P2.Top;

    { Determine the right extreme of combined }
    if (P1.Left + P1.Width) >
       (P2.Left + P2.Width) then
          w1 := P1.Left + P1.Width - l1
    else
          w1 := P2.Left + P2.Width - l1;

    { Determine the bottom extreme of combined }
    if (P1.Top + P1.Height) >
       (P2.Top + P2.Height) then
          h1 := P1.Top + P1.Height - t1
    else
          h1 := P2.Top + P2.Height - t1;

    aDisplay.SetBounds( LayoutList.XDims(l1) + 1,
                        LayoutList.YDims(t1) + 1,
                        LayoutList.XDims(w1) - 2,
                        LayoutList.YDims(h1) - 2);
    aDisplay.BringToFront;
//    aDisplay.Show;
  end;
end;

function  TCustomLayoutManager.ValidDisplay(D : Integer) : Integer;
begin
  Result := D;
end;


function TCustomLayoutTokenizer.ReadAssignmentDouble : Double;
var Tmp : string;
    Code : Integer;
begin
  ExpectedToken('=');
  Tmp := Self.Read;

  Val(Tmp, Result, Code);
  if Code <> 0 then
    raise Exception.Create('Value is not numeric [' + Tmp + ']');
end;


function TCustomLayoutTokenizer.Read : string;
begin
  if EndOfStream then
    Exit;

  Result := inherited Read;
  if Result = '#' then begin
    Self.Line := Self.Line + 1;
    Result := Self.Read;
  end;
end;


{ TSaverThread }

constructor TSaverThread.Create(aLayout: TCustomLayoutManager);
begin
  inherited Create(True);
  Layout := aLayout;
end;

procedure TSaverThread.Execute;
begin
  while not Terminated do begin
    case Layout.Event.WaitFor(1000) of
      wrSignaled : begin
        PostMessage(Layout.Handle, CNCM_SCREEN_SAVER, 0, 0);
        Layout.Event.ResetEvent;
      end;
    end;
  end;
end;

procedure TCustomLayoutManager.ShowKeyboard(Display: TAbstractDisplay);
begin
  HideKeyboard;
  PositionKeyboard(Display);

  Keyboard.Visible := True;
  NumericPad.Visible := True;
  Keyboard.BringToFront;
  NumericPad.BringToFront;
  FKeyboardVisible := True;
end;

procedure TCustomLayoutManager.PositionNumeric(Display : TABstractDisplay);
begin
  if Display.Left + Display.Width < (LayoutList.Width - NumericPad.Width) then begin
    NumericPad.Left := Display.Left + Display.Width;
    NumericPad.Top := Display.Top;
  end else if Display.Left > NumericPad.Width then begin
    NumericPad.Left := Display.Left - NumericPad.Width;
    NumericPad.Top := Display.Top;
  end else begin
    NumericPad.Top := LayoutList.Height - NumericPad.Height;
    NumericPad.Left := (LayoutList.Width - NumericPad.Width) div 2;
    NumericPad.NavigationKeys := False;
  end;
end;

procedure TCustomLayoutManager.PositionKeyboard(Display : TABstractDisplay);
var W, H : Integer;
begin
  H := Keyboard.Height;
  W := Keyboard.Width + NumericPad.Width;
  if Display.Left + Display.Width < (LayoutList.Width - W) then begin
    Keyboard.Left := Display.Left + Display.Width;
    Keyboard.Top := Display.Top;
  end else if Display.Left > W then begin
    Keyboard.Left := Display.Left - W;
    Keyboard.Top := Display.Top;
  end else if Display.Top + Display.Height < LayoutList.Height - H then begin
    Keyboard.Top := Display.Top + Display.Height;
    Keyboard.Left := Display.Left;
    if W + Display.Left > LayoutList.Width then
      Keyboard.Left := LayoutList.Width - W;
  end else if Display.Top > H then begin
    Keyboard.Top := 0;
    Keyboard.Left := Display.Left;
    if W + Display.Left > LayoutList.Width then
      Keyboard.Left := LayoutList.Width - W;
  end else begin
    Keyboard.Top := LayoutList.Height - Keyboard.Height;
    Keyboard.Left := 0;
  end;
  NumericPad.Top := Keyboard.Top;
  NumericPad.Left := Keyboard.Left + Keyboard.Width;
end;

procedure TCustomLayoutManager.ShowNumericPad(Display: TAbstractDisplay);
begin
  HideKeyboard;
  PositionNumeric(Display);

  NumericPad.NavigationKeys := False;

  NumericPad.Visible := True;
  NumericPad.BringToFront;
  FNumericPadVisible := True;
end;

procedure TCustomLayoutManager.ShowNavigation(Display: TAbstractDisplay);
begin
  HideKeyboard;
  PositionNumeric(Display);

  NumericPad.NavigationKeys := True;

  NumericPad.Visible := True;
  NumericPad.TabStop := False;
  NumericPad.BringToFront;
  FNavigationVisible := True;
end;

procedure TCustomLayoutManager.HideKeyboard;
begin
  Keyboard.Visible := False;
  NumericPad.Visible := False;
  FKeyboardVisible := False;
  FNumericPadVisible := False;
  FNavigationVisible := False;
end;

procedure TCustomLayoutManager.HideNumericPad;
begin
  HideKeyboard;
end;

procedure TCustomLayoutManager.HideNavigation;
begin
  HideKeyboard;
end;

function TCustomLayoutManager.HTTPGeneratePage(HFile : THANDLE;
            var CGIPath: WideString; CGIRequest : ICGIStringList) : Boolean;
begin
  WriteACNC32HTTPTitle(hFile, 'Layout Help');
  WriteStringToHandle(hFile, '<h2>Select component you require help with</h2>');
  SysObj.Layout.HTTPCreateImageMap(hFile);
  Result:= True;
end;

procedure TCustomLayoutManager.HTTPCreateImageMap(hFile: THandle);
var I : Integer;
    DR : TDisplayRestraint;
begin
  WriteStringToHandle(hFile, '<MAP Name="ImageMap">' + CarRet);
  for I := 0 to Self.LayoutList.Layout[LayoutList.ThisLayout].Count - 1 do begin
    DR := Self.LayoutList.Layout[LayoutList.ThisLayout].Restraint[I];
    WriteStringToHandle(hFile, Format('<AREA SHAPE=RECT COORDS="%d,%d,%d,%d" a href="/cgi-bin/component/%s">' + CarRet,
                   [LayoutList.XDims(DR.Left), LayoutList.YDims(DR.Top), LayoutList.XDims(DR.Left + DR.Width), LayoutList.YDims(DR.Top + DR.Height),
                    DR.CurrentDisplay.Name]));
  end;
  WriteStringToHandle(hFile, '</MAP><img src="/cgi-bin/screenshot.jpg" USEMAP="#ImageMap">');
end;

initialization
  RegisterCNCClass(TCustomLayoutManager);

  ModeStrings := TStringList.Create;
  CNCTypes.GetEnumerationNames(TypeInfo(TCNCMode), ModeStrings);
end.
