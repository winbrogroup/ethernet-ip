unit CustomLayoutForm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  CoreCNC, CustomLayout, Menus, StdCtrls;

type
  TLayoutFormAction = (
    lfaSaveDisplay,
    lfaReloadDisplay,
    lfaSaveAndUseDisplay,
    lfaNone
  );

  TLayoutDesignForm = class(TForm)
    PopupMenu1: TPopupMenu;
    SelectDisplay1: TMenuItem;
    ChangeDefault1: TMenuItem;
    ModeMenu1: TMenuItem;
    SplitV: TMenuItem;
    SplitHorizontal1: TMenuItem;
    EditAsText1: TMenuItem;
    Delete1: TMenuItem;
    SaveandExit1: TMenuItem;
    Exitwithoutsaving1: TMenuItem;
    Saveanduse: TMenuItem;
    SetDisplayasprimary1: TMenuItem;
    NameRestraing1: TMenuItem;
    procedure FormPaint(Sender: TObject);
    procedure FormMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure ChangeDefault1Click(Sender: TObject);
    procedure FormMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FormMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure SplitVClick(Sender: TObject);
    procedure SplitHorizontal1Click(Sender: TObject);
    procedure Delete1Click(Sender: TObject);
    procedure EditAsText1Click(Sender: TObject);
    procedure SelectDisplay1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SaveandExit1Click(Sender: TObject);
    procedure Exitwithoutsaving1Click(Sender: TObject);
    procedure SaveanduseClick(Sender: TObject);
    procedure SetDisplayasprimary1Click(Sender: TObject);
    procedure NameRestraing1Click(Sender: TObject);
  private
    CopyFrom : TForm;
    Layout : TDisplayLayoutList;
    HighLight : TDisplayRestraint;
    Visuals : TList;
    BoundaryItem : TDisplayRestraint;
    BoundarySide : TBoundarySide;
    Dragging : Boolean;
    LastPos : TPoint;
    Action : TLayoutFormAction;

    procedure DrawCursorLine(X, Y : Integer);
    procedure PaintGrid;
    procedure PaintRestraint(R : TDisplayRestraint; C : TColor);
  public
    class function Execute(aForm : TForm; aLayout : TDisplayLayoutList; aVisuals : TList) : TLayoutFormAction;
  end;

implementation

{$R *.DFM}

uses
  SelectVisualForm, RestraintEditForm, SelectLayoutForm;
var
  LayoutDesignForm: TLayoutDesignForm;


class function TLayoutDesignForm.Execute(aForm : TForm;
                aLayout : TDisplayLayoutList;
                aVisuals : TList) : TLayoutFormAction;
var CopyR : TRect;
begin
  Result := lfaNone;
  if not Assigned(LayoutDesignForm) then begin
    CopyR := Rect(0, 0, aForm.Width, aForm.Height);
    SysObj.NC.AcquireModeChangeLock;
    try
      LayoutDesignForm := TLayoutDesignForm.Create(Application);
      with LayoutDesignForm do begin
        BorderStyle := aForm.BorderStyle;
        Left := aForm.Left;
        Top := aForm.Top;
        Width := CopyR.Right;
        Height := CopyR.Bottom;
        Color := clBlack;
        CopyFrom := aForm;
        Layout := aLayout;
        Visuals := aVisuals;
        ShowModal;
        Result := Action;
      end;

    finally
      SysObj.NC.ReleaseModeChangeLock;
      LayoutDesignForm.Free;
      LayoutDesignForm := nil;
      aForm.Show;
    end;
  end;
end;

procedure TLayoutDesignForm.FormPaint(Sender: TObject);
var BM : TBitMap;
    CopyR : TRect;
begin
  CopyR := Rect(0, 0, CopyFrom.Width, CopyFrom.Height);
  BM := CopyFrom.GetFormImage;
  Canvas.CopyMode := cmMergePaint;
  Canvas.CopyRect(CopyR, BM.Canvas, CopyR);
  BM.Free;
  PaintGrid;
end;

procedure TLayoutDesignForm.PaintGrid;
var I : Integer;
begin
  for I := 0 to Layout.DefaultLayout.Count - 1 do begin
    PaintRestraint(Layout.DefaultLayout[I], clBlack);
  end;
end;

procedure TLayoutDesignForm.PaintRestraint(R : TDisplayRestraint; C : TColor);
begin
  Canvas.Pen.Color := C;
  Canvas.Pen.Width := 3;
  Canvas.Pen.Mode := pmCopy;
  Canvas.Brush.Style := bsClear;
  Canvas.Rectangle(  Layout.XDims(R.Left),
                     Layout.YDims(R.Top),
                     Layout.XDims(R.Left + R.Width),
                     Layout.YDims(R.Top + R.Height));
end;

const EdgeSize = 4;

procedure TLayoutDesignForm.FormMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);

  function LeftBoundary(DR : TDisplayRestraint) : Boolean;
  begin
    Result := False;
    if (X < Layout.XDims(DR.Left) + EdgeSize) and
       (X > Layout.XDims(DR.Left) - EdgeSize) and
       (Y > Layout.YDims(DR.Top)) and
       (Y < Layout.YDims(DR.Top) + Layout.YDims(DR.Height)) then
       Result := True;
  end;

  function RightBoundary(DR : TDisplayRestraint) : Boolean;
  begin
    Result := False;
    if (X < Layout.XDims(DR.Left + DR.Width) + EdgeSize) and
       (X > Layout.XDims(DR.Left + DR.Width) - EdgeSize) and
       (Y > Layout.YDims(DR.Top)) and
       (Y < Layout.YDims(DR.Top) + Layout.YDims(DR.Height)) then
       Result := True;
  end;

  function TopBoundary(DR : TDisplayRestraint) : Boolean;
  begin
    Result := False;
    if (X > Layout.XDims(DR.Left)) and
       (X < Layout.XDims(DR.Left + DR.Width)) and
       (Y > Layout.YDims(DR.Top) - EdgeSize) and
       (Y < Layout.YDims(DR.Top) + EdgeSize) then
       Result := True;
  end;

  function BottomBoundary(DR : TDisplayRestraint) : Boolean;
  begin
    Result := False;
    if (X > Layout.XDims(DR.Left)) and
       (X < Layout.XDims(DR.Left + DR.Width)) and
       (Y > Layout.YDims(DR.Top + DR.Height) - EdgeSize) and
       (Y < Layout.YDims(DR.Top + DR.Height) + EdgeSize) then
       Result := True;
  end;

  procedure BestFitBoundary(Restraint : TDisplayRestraint; Bs : TBoundarySide);
  begin
    if Assigned(BoundaryItem) then begin
      case Bs of
        bsLeft, bsRight : if BoundaryItem.Height < Restraint.Height then begin
          BoundaryItem := Restraint;
          BoundarySide := Bs;

        end;
        bsTop, bsBottom : if BoundaryItem.Width < Restraint.Width then begin
          BoundaryItem := Restraint;
          BoundarySide := Bs;
        end;
      end;

    end else begin
      BoundaryItem := Restraint;
      BoundarySide := Bs;
      if (Bs = bsTop) or (Bs = bsBottom) then
        Cursor := crVSplit
      else
        Cursor := crHSplit
    end;
  end;

var
    DL : TRestraintList;
    I : Integer;
begin
  // Detect border and set cursor appropriately

  if Dragging then begin
    DrawCursorLine(LastPos.X, LastPos.Y);
    DrawCursorLine(X, Y);
    LastPos.X := X;
    LastPos.Y := Y;
    Exit;
  end;

  DL := Layout.DefaultLayout;
  for I := 0 to DL.Count - 1 do begin
    if (X > Layout.XDims(DL[I].Left) + EdgeSize) and
       (X < Layout.XDims(DL[I].Left) + Layout.XDims(DL[I].Width) - EdgeSize) and
       (Y > Layout.YDims(DL[I].Top) + EdgeSize) and
       (Y < Layout.YDims(DL[I].Top) + Layout.YDims(DL[I].Height) - EdgeSize) then begin

       if Assigned(HighLight) and (HighLight <> DL[I]) then
         PaintRestraint(HighLight, clBlack);

       HighLight := DL[I];
       PaintRestraint(HighLight, clRed);
       Cursor := crDefault;
       BoundaryItem := nil;
       Exit;
    end;
  end;
  if Assigned(HighLight) then
    PaintRestraint(HighLight, clBlack);
  HighLight := nil;

  BoundaryItem := nil;
  Cursor := crDefault;

  for I := 0 to DL.Count - 1 do begin
    if LeftBoundary(DL[I]) then begin
       BestFitBoundary(DL[I], bsLeft);
    end;

    if RightBoundary(DL[I]) then begin
       BestFitBoundary(DL[I], bsRight);
    end;

    if TopBoundary(DL[I]) then begin
       BestFitBoundary(DL[I], bsTop);
    end;

    if BottomBoundary(DL[I]) then begin
       BestFitBoundary(DL[I], bsBottom);
    end;
  end;
end;

procedure TLayoutDesignForm.ChangeDefault1Click(Sender: TObject);
var I : Integer;
    DL : TRestraintList;
    Disp : TAbstractDisplay;
begin
  if Assigned(HighLight) then begin
    DL := Layout.DefaultLayout;
    for I := 0 to DL.Count - 1 do begin
      if DL[I] = HighLight then begin
        Disp := TSelectVisual.Execute(Visuals);
        if Disp <> nil then begin
          DL[I].DefaultDisplay := Disp;
          CoreCNC.SysObj.Layout.Normal;
          Repaint;
          Exit;
        end;
      end;
    end;
  end;
end;

procedure TLayoutDesignForm.DrawCursorLine(X, Y : Integer);
begin
  Canvas.Pen.Mode := pmXor;
  Canvas.Pen.Color := clWhite;
  Canvas.Pen.Width := 2;
  Canvas.Pen.Style := psSolid;
  case BoundarySide of
    bsLeft, bsRight : begin
      Canvas.MoveTo(X, 0);
      Canvas.LineTo(X, Height);
    end;

    bsTop, bsBottom : begin
      Canvas.MoveTo(0, Y);
      Canvas.LineTo(Width, Y);
    end;
  end;

  LastPos.X := X;
  LastPos.Y := Y;
end;

procedure TLayoutDesignForm.FormMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  if BoundaryItem <> nil then begin
    DrawCursorLine(X, Y);
    Dragging := True;
  end;
end;

procedure TLayoutDesignForm.FormMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  I : Integer;
  DL : TRestraintList;
  PropB : TDisplayRestraint;
  XPC, YPC : Double;
begin
  if X >= Width then
    X := Width - 1;

  if Y >= Height then
    Y := Height - 1;

  if Y < 0 then
    Y := 0;

  if X < 0 then
    X := 0;
    
  DL := Layout.DefaultLayout;
  if Dragging then begin
    Dragging := False;
    DrawCursorLine(LastPos.X, LastPos.Y);

    try
      XPC := Layout.XPCDims(X);
      YPC := Layout.YPCDims(Y);
      // Check window does not wrap backwards
      case BoundarySide of
        bsLeft : if XPC > BoundaryItem.Left + BoundaryItem.Width then
                   Exit;
        bsRight : if XPC < BoundaryItem.Left then
                    Exit;
        bsTop : if YPC > BoundaryItem.Top + BoundaryItem.Height then
                  Exit;
        bsBottom : if YPC < BoundaryItem.Top then
                     Exit;
      end;

      PropB := TDisplayRestraint.Create;
      try
        PropB.Duplicate(BoundaryItem);
        // Now deterine the proposed new extents for the window
        case BoundarySide of
          bsLeft : begin
            PropB.Width := PropB.Width + (PropB.Left - XPC);
            PropB.Left := XPC;
          end;
          bsRight : PropB.Width := XPC - BoundaryItem.Left;
          bsTop : begin
            PropB.Height := PropB.Height + (PropB.Top - YPC);
            PropB.Top := YPC;
          end;
          bsBottom : PropB.Height := YPC - PropB.Top;
        end;

        // Check for any contained windows
        for I := 0 to DL.Count - 1 do begin
          if DL[I] <> BoundaryItem then begin
            if DL[I].ContainedIn(PropB) then
              Exit;
          end;
        end;

        // Now determine all affected windows and adjust accordingly
        for I := 0 to DL.Count - 1 do begin
          if DL[I] <> BoundaryItem then begin
            DL[I].AdjustTo(BoundaryItem, PropB, BoundarySide);
          end;
        end;
        BoundaryItem.Duplicate(PropB);
      finally
        PropB.Free;
        CoreCNC.SysObj.Layout.Normal;
        Repaint;
      end;
    finally
      BoundaryItem := nil;
    end;
  end;
end;

procedure TLayoutDesignForm.SplitVClick(Sender: TObject);
var DR : TDisplayRestraint;
begin
  if Assigned(HighLight) then begin
    DR := TDisplayRestraint.Create;

    HighLight.Width := HighLight.Width / 2;
    DR.Duplicate(HighLight);
    DR.Left := HighLight.Left + HighLight.Width;
    Layout.DefaultLayout.AddRestraint(DR);
    CoreCNC.SysObj.Layout.Normal;
    Repaint;
  end;
end;

procedure TLayoutDesignForm.SetDisplayasprimary1Click(Sender: TObject);
begin
  if Assigned(HighLight) then begin
    Layout.DefaultLayout.MakePrimary(HighLight);
    CoreCNC.SysObj.Layout.Normal;
    Repaint;
  end;
end;


procedure TLayoutDesignForm.SplitHorizontal1Click(Sender: TObject);
var DR : TDisplayRestraint;
begin
  if Assigned(HighLight) then begin
    DR := TDisplayRestraint.Create;

    HighLight.Height := HighLight.Height / 2;
    DR.Duplicate(HighLight);
    DR.Top := HighLight.Top + HighLight.Height;
    Layout.DefaultLayout.AddRestraint(DR);
    CoreCNC.SysObj.Layout.Normal;
    Repaint;
  end;
end;

procedure TLayoutDesignForm.Delete1Click(Sender: TObject);
begin
  if Assigned(HighLight) and (Layout.DefaultLayout.Count > 1) then begin
    Layout.DefaultLayout.DeleteRestraint(HighLight);
    HighLight := nil;
    CoreCNC.SysObj.Layout.Normal;
    Repaint;
  end;
end;

procedure TLayoutDesignForm.EditAsText1Click(Sender: TObject);
begin
  if Assigned(HighLight) then
    TRestraintEdit.Execute(HighLight, Visuals);
end;

procedure TLayoutDesignForm.SelectDisplay1Click(Sender: TObject);
begin
  TSelectLayout.Execute(Layout);
  CoreCNC.SysObj.Layout.Normal;
  HighLight := nil;
  Repaint;
end;

procedure TLayoutDesignForm.FormCreate(Sender: TObject);
begin
  Action := lfaReloadDisplay;
end;

procedure TLayoutDesignForm.SaveandExit1Click(Sender: TObject);
begin
  Action := lfaSaveDisplay;
  ModalResult := mrOK;
end;

procedure TLayoutDesignForm.Exitwithoutsaving1Click(Sender: TObject);
begin
  ModalResult := mrOK;
end;

procedure TLayoutDesignForm.SaveanduseClick(Sender: TObject);
begin
  Action := lfaSaveAndUseDisplay;
  ModalResult := mrOK;
end;

resourcestring
  ACaption = 'Name restraint';
  APrompt = 'Enter name for restraint';

procedure TLayoutDesignForm.NameRestraing1Click(Sender: TObject);
begin
  if Assigned(HighLight) then
    InputQuery(ACaption, APrompt, Highlight.Name);
end;

end.
