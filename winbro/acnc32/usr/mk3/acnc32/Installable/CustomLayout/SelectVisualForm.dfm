object SelectVisual: TSelectVisual
  Left = 335
  Top = 195
  Width = 310
  Height = 360
  Caption = 'Select Display'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = True
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object VisualListBox: TListBox
    Left = 0
    Top = 0
    Width = 302
    Height = 292
    Align = alClient
    ItemHeight = 13
    TabOrder = 0
    OnDblClick = VisualListBoxDblClick
  end
  object Panel1: TPanel
    Left = 0
    Top = 292
    Width = 302
    Height = 41
    Align = alBottom
    TabOrder = 1
    object BitBtn1: TBitBtn
      Left = 24
      Top = 8
      Width = 75
      Height = 25
      TabOrder = 0
      Kind = bkOK
    end
    object BitBtn2: TBitBtn
      Left = 112
      Top = 8
      Width = 75
      Height = 25
      TabOrder = 1
      Kind = bkCancel
    end
  end
end
