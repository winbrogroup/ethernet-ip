object RestraintEdit: TRestraintEdit
  Left = 402
  Top = 261
  Width = 258
  Height = 309
  Caption = 'Restraint Edit'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = True
  Position = poScreenCenter
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 8
    Width = 18
    Height = 13
    Caption = 'Left'
  end
  object Label2: TLabel
    Left = 8
    Top = 32
    Width = 19
    Height = 13
    Caption = 'Top'
  end
  object Label3: TLabel
    Left = 8
    Top = 56
    Width = 28
    Height = 13
    Caption = 'Width'
  end
  object Label4: TLabel
    Left = 8
    Top = 80
    Width = 31
    Height = 13
    Caption = 'Height'
  end
  object Label5: TLabel
    Left = 8
    Top = 104
    Width = 71
    Height = 13
    Caption = 'Default Display'
  end
  object Label6: TLabel
    Left = 8
    Top = 136
    Width = 28
    Height = 13
    Caption = 'Name'
  end
  object LeftEdit: TEdit
    Left = 100
    Top = 8
    Width = 121
    Height = 21
    TabOrder = 0
  end
  object TopEdit: TEdit
    Left = 100
    Top = 32
    Width = 121
    Height = 21
    TabOrder = 1
  end
  object WidthEdit: TEdit
    Left = 100
    Top = 56
    Width = 121
    Height = 21
    TabOrder = 2
  end
  object HeightEdit: TEdit
    Left = 100
    Top = 80
    Width = 121
    Height = 21
    TabOrder = 3
  end
  object Panel1: TPanel
    Left = 0
    Top = 241
    Width = 250
    Height = 41
    Align = alBottom
    TabOrder = 4
    object BitBtn1: TBitBtn
      Left = 32
      Top = 8
      Width = 75
      Height = 25
      TabOrder = 0
      Kind = bkOK
    end
    object BitBtn2: TBitBtn
      Left = 120
      Top = 8
      Width = 75
      Height = 25
      TabOrder = 1
      Kind = bkCancel
    end
  end
  object VisualsCombo: TComboBox
    Left = 100
    Top = 104
    Width = 145
    Height = 21
    ItemHeight = 13
    TabOrder = 5
    Text = 'VisualsCombo'
  end
  object ProtectedCheckBox: TCheckBox
    Left = 32
    Top = 200
    Width = 97
    Height = 17
    Caption = 'Protected'
    TabOrder = 6
  end
  object DontOpenCheck: TCheckBox
    Left = 32
    Top = 176
    Width = 97
    Height = 17
    Caption = 'Dont Open'
    TabOrder = 7
  end
  object NameEdit: TEdit
    Left = 100
    Top = 128
    Width = 121
    Height = 21
    TabOrder = 8
  end
end
