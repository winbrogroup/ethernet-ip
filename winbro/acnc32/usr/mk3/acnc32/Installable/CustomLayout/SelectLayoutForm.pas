unit SelectLayoutForm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, Buttons, CustomLayout, CoreCNC, CNC32, CNCTypes, SelectTagForm;

type
  TSelectLayout = class(TForm)
    Panel1: TPanel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    LayoutPanel: TPanel;
    GroupBox1: TGroupBox;
    LayoutListBox: TListBox;
    GroupBox2: TGroupBox;
    Label1: TLabel;
    Panel3: TPanel;
    ButtonNewLayout: TButton;
    ButtonCopyLayout: TButton;
    Label2: TLabel;
    ManualCheck: TCheckBox;
    Autocheck: TCheckBox;
    MDICheck: TCheckBox;
    TeachInCheck: TCheckBox;
    Editcheck: TCheckBox;
    SignalText: TEdit;
    SpeedButton1: TSpeedButton;
    ButtonDeleteLayout: TButton;
    ButtonRenameLayout: TButton;
    SingleOpenCheckBox: TCheckBox;
    Panel2: TPanel;
    procedure ButtonNewLayoutClick(Sender: TObject);
    procedure ModeCheckClick(Sender: TObject);
    procedure LayoutListBoxClick(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure SignalTextExit(Sender: TObject);
    procedure ButtonCopyLayoutClick(Sender: TObject);
    procedure ButtonDeleteLayoutClick(Sender: TObject);
    procedure ButtonRenameLayoutClick(Sender: TObject);
    procedure SingleOpenCheckBoxClick(Sender: TObject);
  private
    LayoutList : TDisplayLayoutList;
    UpdateControlsActive : Boolean;
    procedure UpdateControls;
    procedure UpdateLayoutList;
  public
    class procedure Execute(aLayout : TDisplayLayoutList);
  end;

implementation

{$R *.DFM}

var
  SelectLayout: TSelectLayout;

class procedure TSelectLayout.Execute(aLayout : TDisplayLayoutList);
begin
  if Assigned(SelectLayout) then
    Exit;

  SysObj.NC.AcquireModeChangeLock;
  try
    SelectLayout := TSelectLayout.Create(Application);
    SelectLayout.LayoutList := aLayout;

    SelectLayout.LayoutListBox.ItemIndex := 0;
    SelectLayout.UpdateLayoutList;
    SelectLayout.UpdateControls;

    try
      SelectLayout.ShowModal;
    finally
      SelectLayout := nil;
    end;
  finally
    SysObj.NC.ReleaseModeChangeLock;
  end;
end;


procedure TSelectLayout.UpdateLayoutList;
var I, Tmp : Integer;
begin
  Tmp := LayoutListBox.ItemIndex;

  LayoutListBox.Items.Clear;
  for I := 0 to LayoutList.Count - 1 do
    LayoutListBox.Items.AddObject(LayoutList[I].Name, LayoutList[I]);

  if Tmp < LayoutListBox.ItemIndex then
    LayoutListBox.ItemIndex := Tmp;
end;

procedure TSelectLayout.UpdateControls;
begin
  UpdateControlsActive := True;
  try
    SelectLayout.SignalText.Text := LayoutList.DefaultLayout.SignalName;
    Manualcheck.Checked := cncModeManual in LayoutList.DefaultLayout.DefaultMode;
    Autocheck.Checked := cncModeAuto in LayoutList.DefaultLayout.DefaultMode;
    MDIcheck.Checked := cncModeMDI in LayoutList.DefaultLayout.DefaultMode;
    TeachIncheck.Checked := cncModeTeachIn in LayoutList.DefaultLayout.DefaultMode;
    EditCheck.Checked := cncModeEdit in LayoutList.DefaultLayout.DefaultMode;
    SingleOpenCheckBox.Checked := LayoutList.DefaultLayout.SingleOpen;
  finally
    UpdateControlsActive := False;
  end;
end;

procedure TSelectLayout.ButtonNewLayoutClick(Sender: TObject);
var RL : TRestraintList;
    DR : TDisplayRestraint;
begin
  RL := TRestraintList.Create('New');
  DR := TDisplayRestraint.Create;
  RL.AddRestraint(DR);
  DR.Left := 0;
  DR.Width := 100;
  DR.Top := 0;
  DR.Height := 100;

  LayoutList.AddLayout(RL);

  UpdateLayoutList;
  UpdateControls;
end;

resourcestring
  NewNameLang = 'New Name for Layout';

procedure TSelectLayout.ButtonRenameLayoutClick(Sender: TObject);
var Tmp : string;
begin
  Tmp := LayoutList.DefaultLayout.Name;
  if InputQuery(NewNameLang, NewNameLang, Tmp) then begin
    CNC32.TokenizeString(Tmp);
    LayoutList.DefaultLayout.Name := Tmp;
    UpdateLayoutList;
  end;
end;

procedure TSelectLayout.ModeCheckClick(Sender: TObject);
begin
  SignalText.Text := LayoutList.DefaultLayout.SignalName;

  if not UpdateControlsActive then begin
    if Manualcheck.Checked then
      Include(LayoutList.DefaultLayout.DefaultMode, cncModeManual)
    else
      Exclude(LayoutList.DefaultLayout.DefaultMode, cncModeManual);

    if Autocheck.Checked then
      Include(LayoutList.DefaultLayout.DefaultMode, cncModeAuto)
    else
      Exclude(LayoutList.DefaultLayout.DefaultMode, cncModeAuto);

    if MDIcheck.Checked then
      Include(LayoutList.DefaultLayout.DefaultMode, cncModeMDI)
    else
      Exclude(LayoutList.DefaultLayout.DefaultMode, cncModeMDI);

    if TeachIncheck.Checked then
      Include(LayoutList.DefaultLayout.DefaultMode, cncModeTeachIn)
    else
      Exclude(LayoutList.DefaultLayout.DefaultMode, cncModeTeachIn);

    if EditCheck.Checked then
      Include(LayoutList.DefaultLayout.DefaultMode, cncModeEdit)
    else
      Exclude(LayoutList.DefaultLayout.DefaultMode, cncModeEdit);
  end;
end;

procedure TSelectLayout.LayoutListBoxClick(Sender: TObject);
begin
  LayoutList.ThisLayout := LayoutListBox.ItemIndex;
  UpdateControls;
end;

procedure TSelectLayout.SpeedButton1Click(Sender: TObject);
var aTag : TAbstractTag;
begin
  aTag := TSelectTag.Execute([TGenericTag]);
  if aTag <> nil then begin
    SignalText.Text := aTag.Name;
    LayoutList.DefaultLayout.SignalName := aTag.Name;
  end;
end;

procedure TSelectLayout.SignalTextExit(Sender: TObject);
begin
  LayoutList.DefaultLayout.SignalName := SignalText.Text;
end;

procedure TSelectLayout.ButtonCopyLayoutClick(Sender: TObject);
var RL : TRestraintList;
    DR : TDisplayRestraint;
    I : Integer;
begin
  RL := TRestraintList.Create('Copy_of_' + LayoutList.DefaultLayout.Name);
  for I := 0 to LayoutList.DefaultLayout.Count - 1 do begin
    DR := TDisplayRestraint.Create;
    DR.Duplicate(LayoutList.DefaultLayout[I]);
    RL.AddRestraint(DR);
  end;
  LayoutList.AddLayout(RL);
  UpdateLayoutList;
end;

procedure TSelectLayout.ButtonDeleteLayoutClick(Sender: TObject);
begin
  if LayoutList.Count > 1 then begin
    LayoutList.DeleteLayout(LayoutList.ThisLayout);
    LayoutList.ThisLayout := 0;
    UpdateLayoutList;
  end;
end;


procedure TSelectLayout.SingleOpenCheckBoxClick(Sender: TObject);
begin
  LayoutList.DefaultLayout.SingleOpen := SingleOpenCheckBox.Checked;
end;

end.
