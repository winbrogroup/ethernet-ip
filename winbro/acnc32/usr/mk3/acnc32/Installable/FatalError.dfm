object FatalErrorForm: TFatalErrorForm
  Left = 283
  Top = 222
  Width = 491
  Height = 397
  Caption = 'Fatal Error'
  Color = clRed
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = True
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 16
  object Panel1: TPanel
    Left = 0
    Top = 336
    Width = 483
    Height = 34
    Align = alBottom
    Color = clRed
    TabOrder = 0
    object OkBtn: TButton
      Left = 158
      Top = 7
      Width = 60
      Height = 20
      Caption = 'OK'
      Default = True
      TabOrder = 0
      OnClick = OkBtnClick
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 170
    Width = 483
    Height = 166
    Align = alClient
    TabOrder = 1
    object LogListMemo: TRichEdit
      Left = 1
      Top = 1
      Width = 481
      Height = 164
      Align = alClient
      TabOrder = 0
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 138
    Width = 483
    Height = 32
    Align = alTop
    Color = clRed
    TabOrder = 2
    object Label1: TLabel
      Left = 13
      Top = 11
      Width = 79
      Height = 16
      Caption = 'Stack trace'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
  end
  object Panel4: TPanel
    Left = 0
    Top = 33
    Width = 483
    Height = 105
    Align = alTop
    Color = clRed
    TabOrder = 3
    object ExceptionMemo: TRichEdit
      Left = 1
      Top = 1
      Width = 481
      Height = 103
      Align = alClient
      TabOrder = 0
    end
  end
  object Panel5: TPanel
    Left = 0
    Top = 0
    Width = 483
    Height = 33
    Align = alTop
    Color = clRed
    TabOrder = 4
    object TitleLab: TLabel
      Left = 13
      Top = 14
      Width = 69
      Height = 16
      Caption = 'Exception'
      Color = clRed
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
    end
  end
end
