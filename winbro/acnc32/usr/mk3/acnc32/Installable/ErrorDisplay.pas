unit ErrorDisplay;

interface

uses CNC32, CNCTypes, CoreCNC, ExtCtrls, Classes, Graphics, Named, Controls,
     IOErrorListBox;

type
  TRotateErrorDisplay = class(TAbstractDisplay)
  private
    ErrorPanel : TPanel;
    ModCount   : word;
    DisplayCount : word;
    Rotate : TAbstractTag;
    procedure displayUpdate(aTag : TAbstractTag);
    procedure errorChange(aTag : TAbstractTag);
  protected
  public
    constructor Create(Aowner : TComponent);override;
    procedure InstallComponent(Params : TParamStrings); override;
    procedure Installed; override;
    procedure Resize; override;
  published
  end;

implementation

constructor TRotateErrorDisplay.create(Aowner : TComponent);
begin
  inherited create(Aowner);
end;

procedure TRotateErrorDisplay.InstallComponent(Params : TParamStrings);
begin
  inherited installComponent(Params);
  ParentFont:= True;
  ErrorPanel := TPanel.create(Self);
  ErrorPanel.parent     := Self;
  ErrorPanel.color      := clWhite;
  ErrorPanel.font.color := clRed;
  //ErrorPanel.font.name  := 'Courier New';
  ErrorPanel.font.style := [fsBold];
  ErrorPanel.tabStop    := false;
  ErrorPanel.font.size  := 12;
  ErrorPanel.bevelOuter := bvLowered;

  Rotate := TagPublish(RotateMessageName, TStringTag);
end;

procedure TRotateErrorDisplay.Installed;
begin
  inherited Installed;
  TagEvent(nameIOFaultChange, edgeChange, TIntegerTag, errorChange);
  TagEvent(nameDisplaySweep,  edgeChange, TIntegerTag, displayUpdate);
end;

procedure TRotateErrorDisplay.ErrorChange(aTag : TAbstractTag);
//var Tmp : string;
//    Level : TFaultLevel;
begin
{  SysObj.FaultInterface.EnterFaultLock;
  try
    if SysObj.FaultInterface.FaultCount > 0 then
      SysObj.FaultInterface.GetFault(SysObj.FaultInterface.FaultCount - 1, Tmp, Level)
    else
      Tmp := '';
  finally
    SysObj.FaultInterface.ExitFaultLock;
  end;

  ErrorPanel.caption := Tmp; }
end;

procedure TRotateErrorDisplay.DisplayUpdate(aTag : TAbstractTag);
var  RotCnt : Integer;
     I : Integer;
     Level : TFaultLevel;
     Text : string;
begin
  Inc(ModCount);
  Text := '';

  if (modCount mod 10) = 0 then begin

    SysObj.FaultInterface.EnterFaultLock;
    try
      if SysObj.FaultInterface.FaultCount = 0 then
        Exit;

      RotCnt := 0;    // Initialize total counter
      with SysObj.FaultInterface do begin
        for i := 0 to FaultCount - 1 do begin
          GetFault(I, Text, Level);
          if Level > FaultNone then begin
            Inc(RotCnt);
          end;
        end;
      end;

      // RotCnt contains the number of errors that are displayable

      // No Errors then clear and exit
      if RotCnt = 0 then begin
        Text := '';
        Level:= faultNone;
        Exit;
      end;

      Inc(DisplayCount);
      with SysObj.FaultInterface do begin
        for I := DisplayCount to DisplayCount + FaultCount - 1 do begin
          GetFault(I mod FaultCount, Text, Level);
          if(Level > FaultNone) then begin
            DisplayCount := I;
            Exit;
          end;
        end;
      end;
    finally
      SysObj.FaultInterface.ExitFaultLock;
      ErrorPanel.Caption := Text;
      ErrorPanel.Font.Color:= TIOErrorListBox.LevelToColour(Level);
      Rotate.AsString := Text;
    end;
  end;
end;



procedure TRotateErrorDisplay.resize;
begin
  if installState = installStateIdle then exit;
  ErrorPanel.left   := CNCBorder;
  ErrorPanel.width  := width - (CNCBorder * 2);
  ErrorPanel.height := height - (CNCBorder * 2);
  ErrorPanel.top    := CNCBorder;
end;

initialization
  RegisterCNCClass(TRotateErrorDisplay);
end.
