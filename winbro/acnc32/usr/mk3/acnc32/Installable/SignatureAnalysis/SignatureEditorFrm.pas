unit SignatureEditorFrm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Gather, StdCtrls, ExtCtrls, CNCTypes, Math, Menus, ComCtrls,NamedPlugin,
  CompoundFiles, CNC32, CoreCNC, Signature, Named, Checklst, Buttons,
  Curves, CurveChart, CNCAbs,ThreeLineButton;

const
  SingleHead = 0;
type
  PCriterionInfo = ^TCriterionInfo;
  TCriterionInfo =
  record
    Criterion: TSignatureCriterion;
    BlobMin, BlobMax, Frame: TRect;
  end;

  TSignatureEditForm = class(TForm)
    SidePanel: TPanel;
    CriteriaGroup: TGroupBox;
    SamplesGroup: TGroupBox;
    SampleList: TCheckListBox;
    SigInfoBox: TGroupBox;
    SampleCountLabel: TLabel;
    CriteriaCountLabel: TLabel;
    ViewGroup: TGroupBox;
    FieldList: TCheckListBox;
    ChartPopup: TPopupMenu;
    ChartPopuFitSamples: TMenuItem;
    ChartPopupNew: TMenuItem;
    ChartPopupHorizontal: TMenuItem;
    ChartPopupVertical: TMenuItem;
    ChartPopupZoomDefault: TMenuItem;
    ChartPopupSetDefaults: TMenuItem;
    ChartPopupFitCriteria: TMenuItem;
    ChartPopupPaste: TMenuItem;
    LHFrame: TPanel;
    ChartPanel: TPanel;
    HoleProgPanel: TPanel;
    HPEdit: TEdit;
    HPLabel: TLabel;
    ProgNoUD: TUpDown;
    ChartPopupEditLimits: TMenuItem;
    HeadGroup: TGroupBox;
    OpenDialog1: TOpenDialog;
    OpenDialog2: TOpenDialog;
    SamplePeriodCombo: TComboBox;
    CriteriaList: TListBox;
    procedure FormResize(Sender: TObject);
    procedure FieldListClickCheck(Sender: TObject);
    procedure FieldListClick(Sender: TObject);
    procedure CriteriaListClick(Sender: TObject);
    procedure SampleListClick(Sender: TObject);
    procedure SampleListClickCheck(Sender: TObject);
    procedure SampleListDrawItem(Control: TWinControl; Index: Integer;
      Rect: TRect; State: TOwnerDrawState);
    procedure ChartPaint(Sender: TObject);
    procedure CriterionDeleteButtonClick(Sender: TObject);
    procedure CriterionEditButtonClick(Sender: TObject);
    procedure ChartMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure ChartMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure ChartMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure ChartPopupFitAllClick(Sender: TObject);
    procedure ChartDblClick(Sender: TObject);
    procedure CriterionNewButtonClick(Sender: TObject);
    procedure ChartPopupNewClick(Sender: TObject);
    procedure CriteriaListDrawItem(Control: TWinControl; Index: Integer;
      Rect: TRect; State: TOwnerDrawState);
    procedure FitButtonClick(Sender: TObject);
    procedure DefaultsButtonClick(Sender: TObject);
    procedure SetDefaultsButtonClick(Sender: TObject);
    procedure ChartPopupZoomDefaultClick(Sender: TObject);
    procedure ChartPopupSetDefaultsClick(Sender: TObject);
    procedure SampleDeleteButtonClick(Sender: TObject);
    procedure AverageBoxClick(Sender: TObject);
    procedure FitCriteriaButtonClick(Sender: TObject);
    procedure ChartPopupFitCriteriaClick(Sender: TObject);
//    procedure SpeedButton1Click(Sender: TObject);
    procedure SampleAddButtonClick(Sender: TObject);
    procedure DelSigButtonClick(Sender: TObject);
    procedure ProgNoUDClick(Sender: TObject; Button: TUDBtnType);
    procedure ChartPopupEditLimitsClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SamplePeriodComboChange(Sender: TObject);
  private
    CriteriaCountFormat     : String;
    SampleCountFormat       : String;
    SampleListFormat        : String;
    CriteriaListFormat      : String;
    ConfirmDeleteMsg        : String;
    SigDeleteAreYouSureLang : String;
    SampleMsg               : String;
    CriterionMsg            : String;
    CopyName                : String;
    SignatureFileDiscardedLang : String;

    ButtonDelSig : TThreelineButton;
    ButtonOK     : TThreelineButton;
    ButtonCancel : TThreelineButton;
    ButtonCriteriaNew : TThreelineButton;
    ButtonCriteriaEdit : TThreelineButton;
    ButtonCriteriaDelete : TThreelineButton;
    ButtonSampleAdd      : TThreelineButton;
    ButtonSampleDelete   : TThreelineButton;
    ButtonViewSave       : TThreelineButton;
    ButtonViewShow       : TThreelineButton;
    ButtonFitSample      : TThreelineButton;
    ButtonFitCriteria    : TThreelineButton;



    FOLD: Boolean; {on-line-display. Never changes during lifetime of form}

    FGather: TAbstractGather; {nil in BG mode}
    WasFieldIndex: Integer;
    WasCriteriaIndex: Integer;
    WasSampleIndex: Integer;

    ClickedPart: Integer; {cpNone, cpFrame, cpMinBlob, cpMaxBlob}
    ClickedButton: TMouseButton;
    ClickedInfo: PCriterionInfo;
    ClickedChart: TCurveChart;  {most recently}
    PopupChart: TCurveChart;

    veState: Integer; {chNone, chClicked, chMove, chStretchMin, chStretchMax,
                       chPan, chZoom}
    veRect: TRect;    {feedback during edit}
    veBase: TPoint;  {point initially clicked}

    HoldCheckRefresh: Boolean;
    HeadCheck : array [0..3] of TCheckBox;

    procedure DelSigMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure OKSigMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure CancelSigMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure CriteriaNewMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure CriteriaEditMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure CriteriaDeleteMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure SamplesAddMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure SamplesDeleteMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure ViewSaveMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure ViewShowMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure FitCriteriaMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure FitSampleMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);



    procedure HeadSelectClick(Sender : TObject);
    function GetSelectedHP: Integer;
    procedure SetSelectedHP( Value: Integer);

    procedure FieldSelectRefresh; {SelectField called by FieldList.OnClick}
    procedure FieldCheckRefresh;  {fields checked/unchecked}
    procedure FieldListRefresh;   {use CurrentSig if cs is nil}
                                  {rebuild field list for current
                                   hole program. All checked}
    procedure CriteriaSelectRefresh(InvalidateChannel: Integer); {when criteria selected}
    procedure CriteriaClear(ch: Integer);         {from ChartList}
    procedure CriteriaListRefresh( InvalidateChannel: Integer);
                                   {rebuild list on full rebuild or
                                    whenever selected field is changed...
                                    Invalidate Chart only or ChartPanel if nil}
    procedure SampleSelectRefresh;
    procedure SampleCheckRefresh;
    procedure SampleListRefresh;
              {at SigParamsRefresh and any criteria edit...}

    procedure DeleteCS;

    procedure CriteriaDraw( Chart: TCurveChart; List: TList);
    function GetSelectedCriterion: TSignatureCriterion;
    procedure SetSelectedCriterion( Value: TSignatureCriterion);
    procedure CriteriaCountRefresh(cs: TSignature);

    procedure veProgress;
    procedure veCancel;
    procedure FitCriteria(Channel: Integer);
//    procedure PasteCriterion;
//    procedure CopyCriterion( All: Boolean; ChartList: TList);
    procedure SetOLD( aOLD: Boolean); {On-Line Display}
    procedure SetGather( Value: TAbstractGather);
    procedure SynchronizeCharts(aChart : TCurveChart);
  protected
    procedure SampleDelete;
    procedure SampleAdd;
    procedure ShowRTV( FieldListOK: Boolean);
    procedure SaveRTV;
  public
    SigFile: TSignatureFile;
    mhp : array [0..3] of Integer;
    IsActiveFile: Boolean;
    SelectedHeads : TGatherHeads;
    SelectedHead : TGatherHead;

    Charts: array[TGatherField] of TCurveChart;
    Criteria: array[TGatherField] of TList;

    SignatureExists: Boolean;     {updated by SigParams refresh}
    SignatureCompatible: Boolean; {selected sig is compatible with LD}

    procedure SigParamsRefresh;   {Period, FieldList, CriteriaList}
    procedure Load(Manager: TCompoundFile; WithSamples: Boolean);
    procedure Save(Manager: TCompoundFile; SaveData: Boolean);
    constructor Create(Aowner : TComponent); override;
    destructor  Destroy; override;
    function CurrentSig( var cs: TSignature): Boolean;
    procedure DoSignatureFileException(E : ESignatureFile);
    procedure HeadSelectRefresh;
    procedure ScreenUpdate;
    procedure ResizeCharts;

    procedure CriterionDelete;  {current record}
    procedure CriterionEdit;    {ditto}
    procedure CriterionNew;
    property SelectedHoleProgram: Integer read GetSelectedHP write SetSelectedHP;
    property OLD: Boolean read FOLD write SetOLD;
    property Gather: TAbstractGather read FGather write SetGather;
  published
  end;



implementation
uses
  Clipbrd,
//  EDMHead,
  CriteriaEdit,
  SignatureEditor;
{$R *.DFM}

{

Notes from form:
  CriteriaGroup
  -------------
  Criteria List changes according to the currently selected field.
  Visible: BG + Edit. Checked = Visible, selected = highlighted.
  Must maintain state for each field

  SampleGroup
  -----------
  Sample list shows all the samples present in the current signature.
  The 'Add' button is visible only in 'Active' edit mode.
  Visible: BG + Edit. List changes to match selected hole program

  FieldsGroup
  -----------
  Visible in All modes if  the signature or gather has more than one field.
  Selected field acts as filter on criterion list and highlights the field

  HoleGroup
  ---------
  Select Hole program. Visible in all modes.
  BG: selects appropriate signature; Edit: Selects signature - disable live
  features if not current program. disabled but visible in OLD mode

Live Data
---------
The form is Live if it is accepting data from the EDM head. The signatures
do not contain any live curves: this data is retrieved from the 'Gather' field.
The form is always Live in OLD mode, sometimes live in Active Edit mode and
never live in BG edit mode.


1. Online Display. (OLD)
   In this mode the Form is not visible, but the ChartPanel component is
   transported to TSigDisplay. This is a live mode. Note that the rest
   of the form is instantiated, but not used or displayed.


2. ActiveEdit
   In this mode, the active file is being edited. The Form is shown in full
   Live if Gather is assigned and CurrentSig.ID = Gather.HoleProgram[0]

   the Add Button is enabled if HoleGroup.ItemIndex = Gather.HoleProgram[0].

   The AddButton will call CurrentSig.AddSample(Gather)

   (Multihead &&&) (use Gather.Curve[Head])

   When the form is live and TGather.Status = gsDefined
   (more specific?) then the 'Sample Add' button is enabled.

3. In BG edit mode all facilities for editing a signature file are present.
   There is no live data (Gather = nil)
      HoleGroup.Items.Objects = TSignature

  Note that when the SignatureEditor is open (as opposed to the OnlineDisplay)
  the Live data will be available for adding to the sample set only if
  the hole program selected in the editor matches Gather.HoleProgram.
  It is assumed that the Gather will not be started while the editor is
  open (this is a moderately safe assumption, although not enforced by any
  code in this unit or zSigEditor) When a hole program is selected, the
  'Add' Button is enabled according to whether or not the
}

{
FORM HOT KEY LIST
ABCDEFGHIJKLMNOPQRSTUVWXYZ
X XXXXXXX  XXXXX XXX XX X
}


const  {crit hit areas}
  BlobSize = 8;
  MinZoom = 5;

  NewCriterionSize = 10;  {i.e One tenth of chart height or width}

  chNone = 0;
  chClicked = 1;
  chMove = 2;
  chStretchMin = 3;
  chStretchMax = 4;
  chPan = 5;
  chZoom = 6;
  chDouble = 7;
    {delphi generates a MouseDown after a Double. This
     is not like windows which replaces a MOUSEDOWN with
     a double in the case of a double. This state is
     necessary to counteract this behaviour}

  cpNone = 0;
  cpFrame = 1;
  cpBlobMin = 2;
  cpBlobMax = 3;

  LiveColor = clRed;
  ColorTableSize = 11;
  ColorTable: array[0..ColorTableSize - 1] of TColor =
    (clOlive,
     clBlue,
     clFuchsia,
     clGreen,
     clLime,
     clMaroon,
     clAqua,
     clNavy,
     clTeal,
     clPurple,
     clYellow);


function TSignatureEditForm.GetSelectedHP: Integer;
begin
  Result:= ProgNoUD.Position
end;

procedure TSignatureEditForm.CriteriaClear(ch: Integer);

procedure DoClear( i: Integer);
var
  j: Integer;
begin
  for j:= 0 to Criteria[i].Count - 1 do
    Dispose(PCriterionInfo(Criteria[i][j]));
  Criteria[i].Clear
end;

var
  i: Integer;
begin
  if ch in [0..3] then
    DoClear(ch)
  else
  for i:= Low(TGatherField) to High(TGatherField) do
    DoClear(i)
end;

procedure TSignatureEditForm.SetGather( Value: TAbstractGather);
var
  I, H : Integer;
begin
  FGather:= Value;
  for H := 0 to CoreCNC.SysObj.Heads - 1 do begin
    if Assigned(FGather) and Assigned(FGather[H]) then
      FGather[H].Color:= LiveColor;
    if OLD then
      for i:= Low(TGatherField) to High(TGatherField) do
        Charts[i].LiveCurve:= Value;
  end;
end;

procedure TSignatureEditForm.SetSelectedHP( Value: Integer);
begin
  if Value <> GetSelectedHP then
  begin
    ProgNoUD.Position:= Value;
    SigParamsRefresh
  end
end;

procedure TSignatureEditForm.DoSignatureFileException(E : ESignatureFile);
begin
  E.Message := E.Message + CarRet + SignatureFileDiscardedLang;
  Application.ShowException(E);
end;

procedure TSignatureEditForm.Load(Manager: TCompoundFile; WithSamples: Boolean);
var
  I : Integer;
begin
  try
    Manager.SectionOpen(SectionSignatureData, SigFile);
    SigFile.Load(WithSamples);
    for I := 0 to SysObj.Heads - 1 do
      if mhp[I] > 0 then begin
        if (SigFile.SignatureProg[mhp[I]] = nil) and (Assigned(Gather)) and
           (Gather.HoleProgram[I] = mhp[I]) then
          SigFile.SignatureAdd(TSignature.CreateToMatchCurve(mhp[I], Gather[I], SigFile.SamplePoints));
    end;
    ProgNoUD.Position:= mhp[0];
    SigParamsRefresh
  except
    on E: ESignatureFile do
      DoSignatureFileException(E);
  end;
end;

procedure TSignatureEditForm.Save(Manager: TCompoundFile; SaveData: Boolean);
begin
  try
    if SaveData then begin
      SigFile.Save;
    end;
    Manager.SectionClose(SigFile, SaveData)
  except
    on E: ESignatureFile do
      Application.ShowException(E)
  end
end;

const CaptionControlClass : array [0..1] of TClass = (
  TButton,
  TLabel
);

constructor TSignatureEditForm.Create(Aowner : TComponent);
var
  i: Integer;
begin
  inherited Create(aOwner);
  SigFile:= TSignatureFile.Create;
  SigFile.OnGetSignalTag := InstalledSigEditor.GetSignalTag;
  SigFile.OnGetEnableTag := InstalledSigEditor.GetEnableTag;


  CoreCNC.LocalizeForm(Self);

  for i:= Low(TGatherField) to High(TGatherField) do begin
    Criteria[I]:= TList.Create;
    Charts[I] := TCurveChart.Create(ChartPanel);
    Charts[I].Parent := ChartPanel;
    Charts[I].Channel := I;
    Charts[I].OnDblClick  := ChartDblClick;
    Charts[I].OnMouseDown := ChartMouseDown;
    Charts[I].OnMouseUp   := ChartMouseUp;
    Charts[I].OnMouseMove := ChartMouseMove;
    Charts[I].OnPaint     := ChartPaint;
    Charts[I].Align := alTop;
    Charts[I].Height := 80;
    Charts[I].Heads := [0..3];
  end;
end;

destructor TSignatureEditForm.Destroy;
var
  i: Integer;
begin
  for i:= Low(TGatherField) to High(TGatherField) do
    Criteria[i].Free;
  SigFile.Free;
  inherited Destroy
end;

function TSignatureEditForm.CurrentSig(var cs: TSignature): Boolean;
var
  ii: Integer;
begin
  ii:= ProgNoUD.Position;
  if ii >= 0 then
    cs := SigFile.SignatureProg[ii]
  else
    cs := nil;
  Result:= cs <> nil;
end;

procedure TSignatureEditForm.CriteriaCountRefresh(cs: TSignature);
begin
  if Assigned(cs) then
    CriteriaCountLabel.Caption:= Format(CriteriaCountFormat, [cs.CriterionCount])
  else
    CriteriaCountLabel.Caption:= Format(CriteriaCountFormat, [0]);
end;


procedure TSignatureEditForm.FieldSelectRefresh; {fields checked/unchecked}
begin
  {note: This should be only if the item index has changed...}
  {do not call a criteria list refresh in here - recursive}
  WasFieldIndex:= FieldList.ItemIndex
end;

procedure TSignatureEditForm.FieldCheckRefresh; {fields checked/unchecked}
var
  i: Integer;
begin
  for i:= Low(TGatherField) to High(TGatherField) do
    Charts[i].Visible:= (i < FieldList.Items.Count) and (FieldList.Checked[i]);
  CriteriaListRefresh(-1);
  FormResize(Self);
end;

procedure TSignatureEditForm.FieldListRefresh; {rebuild field list for current
                                      hole program. Checked according to RTV}
var
  i: Integer;
  cs: TSignature;
begin
  FieldList.Clear;
  if CurrentSig(cs) then
    for i:= 0 to cs.FieldCount - 1 do
      FieldList.Checked[FieldList.Items.Add(cs.FieldName[i])]:= i in cs.RTV.Fields;
  FieldCheckRefresh;
  if FieldList.Items.Count > 0 then
    FieldList.ItemIndex:= 0
  else
    FieldList.ItemIndex:= -1;  {This doesn't generate onclick}

  FieldSelectRefresh;
end;

procedure TSignatureEditForm.CriteriaSelectRefresh(InvalidateChannel: Integer);
begin
  if InvalidateChannel in [0..3] then
    Charts[InvalidateChannel].Invalidate
  else
    ChartPanel.Invalidate;
 {note that CriteriaList.Items.Objects[] = PCriterionInfo}
  WasCriteriaIndex:= CriteriaList.ItemIndex    {&&&MS}
end;

function StdCriterionName(C: TSignatureCriterion; i: Integer;FormatString : String): String;
begin
  Result:= C.Name;
  if Result = '' then
    FmtStr(Result, FormatString, [i]);
  FmtStr(Result, '%s (%s)', [Result, C.FieldName])
end;

procedure TSignatureEditForm.CriteriaListRefresh(InvalidateChannel: Integer);
var
  i, j: Integer;
  cs: TSignature;
  p: PCriterionInfo;
  OldIndex: Integer;
begin
  OldIndex:= CriteriaList.ItemIndex;  {&&&MS}
  CriteriaList.Clear;
  CriteriaClear(InvalidateChannel);
  if CurrentSig(cs) then
  with cs do begin
    for i:= 0 to CriterionCount - 1 do
      for j:= 0 to FieldList.Items.Count - 1 do
        if FieldList.Checked[j] and
           (Criterion[i].ValidChannel = j) then begin
        New(p);
        p^.Criterion:= TSignatureCriterion(Criterion[i]);
        if (p^.Criterion.ValidChannel <> -1) then
          Criteria[p^.Criterion.ValidChannel].Add(p);
        CriteriaList.Items.AddObject(StdCriterionName(Criterion[i], i,CriteriaListFormat), Criterion[i])
    end
  end;
  if (OldIndex < CriteriaList.Items.Count) and
     (CriteriaList.Items.Count > 0) then
  begin
    CriteriaList.ItemIndex:= OldIndex  {&&&MS}
  end else
  begin
    if CriteriaList.Items.Count > 0 then  {force CriteriaList.OnClick}
      CriteriaList.ItemIndex:= 0
    else
      CriteriaList.ItemIndex:= -1
  end;
  CriteriaSelectRefresh(InvalidateChannel);
end;

procedure TSignatureEditForm.SampleSelectRefresh;
var
  i: Integer;
  sc: TCurve;
begin
  WasSampleIndex:= SampleList.ItemIndex;
  if WasSampleIndex >= 0 then
    sc:= TCurve(SampleList.Items.Objects[WasSampleIndex])
  else
    sc:= nil;
  for i:= Low(TGatherField) to High(TGatherField) do
    Charts[i].SelectedCurve:= sc
end;

procedure TSignatureEditForm.SampleCheckRefresh;
var
  i, j: Integer;
begin
  for j:= Low(TGatherField) to High(TGatherField) do
    with Charts[j] do begin
      BeginUpdate;
      ClearCurves
    end;
  if not OLD then
    for i:= 0 to SampleList.Items.Count - 1 do
      if SampleList.Checked[i] then
        for j:= Low(TGatherField) to High(TGatherField) do
          Charts[j].AddCurve(TCurve(SampleList.Items.Objects[i]));

  for j:= Low(TGatherField) to High(TGatherField) do
    Charts[J].EndUpdate;
end;

procedure TSignatureEditForm.SampleListRefresh;
var
  i: Integer;
  cs: TSignature;
  tc: TCurve;
begin
  SampleList.Clear;
  if CurrentSig(cs) then begin
    for i:= 0 to cs.SampleCount - 1 do begin
      tc:= cs.SampleCurve[i];
      tc.Color:= ColorTable[i mod ColorTableSize];
      SampleList.Items.AddObject(Format(SampleListFormat, [i]), tc);
      SampleList.Checked[I] := cs.SampleCurve[I].Show;
    end
  end;
  SampleCheckRefresh;
  if not OLD then
  begin
    if SampleList.Items.Count > 0 then
      SampleList.ItemIndex:= 0
    else
      SampleList.ItemIndex:= -1;
    SampleSelectRefresh;
    SampleCountLabel.Caption:= Format(SampleCountFormat, [SampleList.Items.Count])
  end
end;

procedure TSignatureEditForm.SigParamsRefresh;
{called when signature changes}
var
  cs: TSignature;

  procedure SetSampleAdd(aStatus : Boolean);
  begin
    ButtonSampleAdd.IsEnabled:= Assigned(FGather) and
                              (FGather.GatherStatus <> gsGathering) and
                              aStatus;
  end;

  procedure SetLiveD(aStatus : Boolean);
  var
    J : Integer;
  begin
    for J := Low(TGatherField) to High(TGatherField) do
      Charts[J].LiveCurve:= FGather;

    SetSampleAdd(aStatus);
  end;

begin
  SignatureExists:= CurrentSig(cs);
  if not OLD then begin
    HeadSelectRefresh;
    if SignatureExists then begin
      // May need to check for signature
      SamplePeriodCombo.ItemIndex := Gather.IndexOfSamplePeriod[SigFile.Period];
      SetLiveD(True);
    end else begin {no current signature}
      if Assigned(Gather) then begin
        // If not BG editing then assign the SigFile.Period with a permissive
        // gather period
        Gather.SamplePeriod := SigFile.Period;
        SigFile.Period := Gather.SamplePeriod;
        SamplePeriodCombo.ItemIndex := Gather.IndexOfSamplePeriod[SigFile.Period];
      end;
      SetLiveD(False)
    end;
    CriteriaCountRefresh(cs);
  end else begin  {for on line display}
    {important note: If cs Does not exist, then the signature is compatible,
    but no live data can be displayed. The opp has no signature for this hole,
    therefore it is ok to proceed }
    if SignatureExists and Assigned(FGather) then begin
      SignatureCompatible:= cs.CompatibleWith(FGather[SingleHead]);
      if SignatureCompatible then
        SetLiveD(True)
      else
        SetLiveD(False)
    end else begin
      SetLiveD(False);
      SignatureCompatible:= true
    end
  end;
  SampleListRefresh;
  CriteriaListRefresh(-1);
  FieldListRefresh;
  ShowRTV(true)
end;

var
  msg: String;

procedure TSignatureEditForm.ScreenUpdate;
var
  I, H: Integer;
begin
  try
    if OLD and
       Assigned(Gather) and
       (Gather.GatherStatus = gsGathering) then
    if Assigned(ChartPanel) then begin
      if ChartPanel.Visible then
        for i:= Low(TGatherField) to High(TGatherField) do
          with Charts[i] do
            if Visible then
              for H := Low(TGatherHead) to High(TGatherHead) do
                LiveRefresh(H);
    end else
      msg := 'Oh Fuck';

  except
    on E:Exception do
      msg:= e.message
  end;
end;

procedure TSignatureEditForm.FormResize(Sender: TObject);
begin
  ResizeCharts;
end;

procedure TSignatureEditForm.ResizeCharts;
var
  i: Integer;
  j: Integer;
  ch: Integer;
begin
  j:= 0;
  for i:= Low(TGatherField) to High(TGatherField) do
    if Charts[i].Visible then
      inc(j);
  if j > 0 then
  begin
    ch:= (ChartPanel.Height - 2*ChartPanel.BorderWidth)  div j;
    for i:= Low(TGatherField) to High(TGatherField) do
      if Charts[i].Visible then
        Charts[i].Height:= ch
  end
end;

procedure TSignatureEditForm.FieldListClickCheck(Sender: TObject);
begin
  if not HoldCheckRefresh then
    FieldCheckRefresh
end;

procedure TSignatureEditForm.FieldListClick(Sender: TObject);
begin
  if WasFieldIndex <> FieldList.ItemIndex then
    FieldSelectRefresh
end;

procedure TSignatureEditForm.CriteriaListClick(Sender: TObject);

function GetChart( i: Integer): Integer;
var
  GSC: TSignatureCriterion;
begin
  Result:= -1;
  if i > -1 then
  begin
    GSC:= TSignatureCriterion(CriteriaList.Items.Objects[i]);
    if Assigned(GSC) then
      Result:= GSC.ValidChannel
  end
end;

var
  this, was: Integer;

begin
  if WasCriteriaIndex <> CriteriaList.ItemIndex then
  begin
    was:= GetChart(WasCriteriaIndex);
    this:= GetChart(CriteriaList.ItemIndex);
    if was = this then
      CriteriaSelectRefresh(this)
    else
      CriteriaSelectRefresh(-1)
  end
end;

procedure TSignatureEditForm.SampleListClick(Sender: TObject);
var tc : TCurve;
begin
  if SampleList.ItemIndex <> -1 then begin
    tc := TCurve(SampleList.Items.Objects[SampleList.ItemIndex]);
    tc.Show := SampleList.Checked[SampleList.ItemIndex];
  end;

  if WasSampleIndex <> SampleList.ItemIndex then
    SampleSelectRefresh
end;

procedure TSignatureEditForm.SampleListClickCheck(Sender: TObject);
begin
  SampleCheckRefresh
end;

procedure TSignatureEditForm.SampleListDrawItem(Control: TWinControl;
  Index: Integer; Rect: TRect; State: TOwnerDrawState);
var
  S: String;
  Col: TColor;
  x, y: Integer;
begin
  S:= SampleList.Items[Index];
  Col:= TCurve(SampleList.Items.Objects[Index]).Color;
  with SampleList.Canvas do begin
    TextRect(Rect, Rect.Left, Rect.Top, S);
    x:= TextWidth(S) + 5 + Rect.Left;
    y:= (Rect.Top + Rect.Bottom) div 2;
    Pen.Color:= Charts[0].Brush.Color;
    Pen.Width:= 5;
    MoveTo(x, y);
    LineTo(Rect.Right, y);
    Pen.Color:= Col;
    Pen.Width:= 1;
    LineTo(x, y)
  end
end;

function TSignatureEditForm.GetSelectedCriterion: TSignatureCriterion;
begin
  if OLD then begin
    Result:= nil
  end else with CriteriaList do begin
    if ItemIndex > -1 then begin
      Result:= TSignatureCriterion(Items.Objects[ItemIndex])
    end else begin
      Result:= nil
    end
  end
end;

procedure TSignatureEditForm.SetSelectedCriterion(Value: TSignatureCriterion);
var
  i: Integer;
begin
  for i:= 0 to CriteriaList.Items.Count - 1 do
  begin
    if Pointer(Value) = CriteriaList.Items.Objects[i] then
    begin
      if i <> CriteriaList.ItemIndex then
      begin
        CriteriaList.ItemIndex:= i;
        CriteriaListClick(CriteriaList)
      end;
      break
    end
  end
end;

procedure TSignatureEditForm.CriteriaDraw( Chart: TCurveChart; List: TList);
const
  B2 = BlobSize div 2;

var
  ci: PCriterionInfo;

  procedure BlobLo;
  begin
    with Chart.Canvas do begin
      ci^.BlobMin:= Classes.Rect(PenPos.X - B2, PenPos.Y - B2,
                   PenPos.X + B2, PenPos.Y + B2);
      with ci^.BlobMin do
        Ellipse(Left, Top, Right, Bottom)
    end
  end;

  procedure BlobHi;
  begin
    with Chart.Canvas do begin
      ci^.BlobMax:= Classes.Rect(PenPos.X - B2, PenPos.Y - B2,
                  PenPos.X + B2, PenPos.Y + B2);
      with ci^.BlobMax do
        Ellipse(Left, Top, Right, Bottom)
    end;
    with ci^ do
      UnionRect(Frame, BlobMin, BlobMax)
  end;

var
  i: Integer;
  t1, t2: Double;
  f1, f2: Integer;
  Sel: Boolean;
  SelRect: TRect;
begin
  with Chart.Canvas do begin
    Pen.Width:= 1;
    Brush.Style:= bsSolid;
    Sel:= false;

    for i:= 0 to (List.Count - 1) do begin
      ci:= List[i];
      ci^.Criterion.GetVisualBoundingPoints(t1, t2, f1, f2, OLD);
//      Pen.Color:= ci^.Criterion.HitSignalTag.Signal[SelectedHead].Colour;
//      Brush.Color:= ci^.Criterion.HitSignalTag.Signal[SelectedHead].Colour;
      Brush.Style := bsSolid;
      MoveTo(Chart.ScreenT(t1), Chart.ScreenY(f1));
      BlobLo;
      LineTo(Chart.ScreenT(t2), Chart.ScreenY(f2));
      BlobHi;
      Brush.Style := bsBDiagonal; //bsClear;
      Pen.Color := clBlack;
      Pen.Style := psDash;
      Rectangle(Chart.ScreenT(T1), Chart.ScreenY(F1), Chart.ScreenT(T2), Chart.ScreenY(F2));


      if not OLD and (GetSelectedCriterion = ci^.Criterion) then begin
        Sel:= True;
        SelRect:= ci^.Frame;
      end;

      if Sel then begin
        InflateRect(SelRect, 2, 2);
        Pen.Color:= clBlack;
        Brush.Style:= bsClear;
        with SelRect do
          Rectangle(Left, Top, Right, Bottom);
      end;
    end;
  end;
end;

procedure TSignatureEditForm.ChartPaint(Sender: TObject);
begin
  CriteriaDraw( TCurveChart(Sender),
                Criteria[TCurveChart(Sender).Channel])
end;

procedure TSignatureEditForm.CriterionDeleteButtonClick(Sender: TObject);
begin
  CriterionDelete
end;

procedure TSignatureEditForm.CriterionDelete;
var
  sc: TSignatureCriterion;
  cs: TSignature;
  ch: Integer;
begin
  sc:= GetSelectedCriterion;
  if CurrentSig(cs) and Assigned(sc) and
     (MessageDlg(
       Format(ConfirmDeleteMsg, [CriterionMsg]), mtConfirmation, [mbOk, mbCancel], 0) = mrOK) then
  begin
    ch:= sc.ValidChannel;
    cs.DeleteCriterion(sc);
    CriteriaCountRefresh(cs);
    CriteriaListRefresh(ch)
  end
end;

procedure TSignatureEditForm.CriterionEdit;
var
  ii: Integer;
  cs: TSignature;
  Dlg: TCriteriaEditDlg;
  crit: TSignatureCriterion;
begin
  {disabled if more than one selection}
  ii:= CriteriaList.ItemIndex;   {&&& MS}
  if CurrentSig(cs) and (ii >= 0) then
  begin
    Dlg:= TCriteriaEditDlg.Create(Application);
    try
      crit:= TSignatureCriterion(CriteriaList.Items.Objects[ii]);
      if Dlg.Execute(crit, FieldList.Items,
                           InstalledSigEditor.SignalTagList,
                           InstalledSigEditor.EnableTagList) then begin
        CriteriaListRefresh(crit.ValidChannel);
      end;
    finally
      Dlg.Free
    end
  end
end;

procedure TSignatureEditForm.CriterionNew; {general}
var
  NewCrit: TSignatureCriterion;
  ch: Integer;
  fl: Integer;
  tl, th, dt: Double;
  Dlg: TCriteriaEditDlg;
  cs: TSignature;
begin
  if CurrentSig(cs) and (FieldList.Items.Count > 0) then {require fields}
  begin
    ch:= FieldList.ItemIndex;
    if ch < 0 then
     ch:= 0;
    with Charts[ch] do begin
      fl:= Round(YMax + YMin) div 2;
      dt:= (TMax - TMin) / NewCriterionSize;
      tl:= (TMax + TMin - dt) / 2;
      th:= tl + dt;
    end;
    NewCrit:= TSignatureCriterion.Create(cs.FieldName[ch], '', cs,
                              InstalledSigEditor.DefaultSignalTag,
                              InstalledSigEditor.DefaultSignalTag,
                              InstalledSigEditor.DefaultEnableTag,
                              ctHorizontal, fl, fl, tl, th);
    NewCrit.ValidChannel:= ch;
    Dlg:= TCriteriaEditDlg.Create(Application);
    try
      if Dlg.Execute(NewCrit, FieldList.Items,
                              InstalledSigEditor.SignalTagList,
                              InstalledSigEditor.EnableTagList) then begin
        cs.AddCriterion(NewCrit);
        CriteriaCountRefresh(cs);
        CriteriaListRefresh(ch);
        SetSelectedCriterion(NewCrit)
      end else
      begin
        NewCrit.Free
      end
    finally
      Dlg.Free
    end
  end
end;


procedure TSignatureEditForm.CriterionEditButtonClick(Sender: TObject);
begin
  CriterionEdit
end;

procedure TSignatureEditForm.ChartMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  Ch, i: Integer;
  ci: PCriterionInfo;
begin
  if (veState = chDouble) then  {ignore Delphi's extra call}
  begin
    veState:= chNone
  end else
  if Assigned(Sender) and (Sender is TCurveChart) then
  begin
    if (veState <> chNone) then
    begin
      veCancel    {interclick will cancel any operation...}
    end else
    begin
      ClickedChart:= TCurveChart(Sender);
      ch:= ClickedChart.Channel;
      ci:= nil;
      ClickedPart:= cpNone;
      ClickedButton:= Button;
      ClickedInfo:= nil;
      veBase.X:= X; veBase.Y:= Y;
      for i:= 0 to Criteria[Ch].Count - 1 do
      begin
        ci:= PCriterionInfo(Criteria[Ch][i]);
        if PtInRect(ci^.Frame, veBase) then
        begin
          if PtInRect(ci^.BlobMin, veBase) then
            ClickedPart:= cpBlobMin
          else
          if PtInRect(ci^.BlobMax, veBase) then
            ClickedPart:= cpBlobMax
          else
            ClickedPart:= cpFrame;
          ClickedInfo:= ci;
          break
        end
      end;
      {select criterion regardless of button}
      if Assigned(ClickedInfo) then
      begin
        if ClickedInfo^.Criterion <> GetSelectedCriterion then {begin drag}
          SetSelectedCriterion(ci^.Criterion)
      end;
      veState:= chClicked
    end
  end
end;


procedure TSignatureEditForm.ChartMouseMove(Sender: TObject; Shift: TShiftState;
  X, Y: Integer);

  function GetOffsetRect( const Base: TRect): TRect;
  begin
    Result:= Base;
    OffsetRect(Result, X-veBase.X, Y-veBase.Y)
  end;

  procedure ContinueStretch( const Fixed, Moving: TRect; Sign: Integer);
                    {sign = 1 for MaxMoving, -1 for MinMoving}
    function GetDir( const R1, R2: TPoint): Boolean; {true = horiz}
    begin
      Result:= Abs(R1.X - R2.X) >= Abs(R1.Y - R2.Y)
    end;
  var
    tempRect: TRect;
  begin
    veProgress;
    tempRect:= GetOffsetRect(Moving);
    if GetDir(tempRect.TopLeft, Fixed.TopLeft) then
    begin  {horizontal}
      if (tempRect.Left - Fixed.Left)*Sign >= 0 then
      begin
        tempRect.Top:= Fixed.Top;
        tempRect.Bottom:= Fixed.Bottom;
        UnionRect(veRect, tempRect, Fixed)
      end
    end else
    begin  {vertical}
      if (tempRect.Top - Fixed.Top)*Sign <= 0 then
      begin
        tempRect.Left:= Fixed.Left;
        tempRect.Right:= Fixed.Right;
        UnionRect(veRect, tempRect, Fixed)
      end
    end;
    veProgress
  end;

  procedure ContinueZoom; {Zoom, Pan}
  begin
    veProgress;
    veRect.Right:= X;
    veRect.Bottom:= Y;
    veProgress
  end;

  procedure StartZoom( aState: Integer);
  begin
    veRect.TopLeft:= veBase;
    veRect.Right:= X;
    veRect.Bottom:= Y;
    veState:= aState;
    veProgress
  end;

  procedure StartMove(aState: Integer); {or stretch..}
  begin
    veState:= aState;
    veRect:= ClickedInfo^.Frame;
    veProgress
  end;

  procedure ContinueMove;
  begin
    veProgress;
    veRect:= GetOffsetRect(ClickedInfo^.Frame);
    veProgress
  end;

  function MoveDetected: Boolean;
  begin
    Result:= (abs(X - veBase.X) + abs(Y - veBase.Y)) > MinZoom
  end;

begin
  if (Sender = ClickedChart) then
  case veState of
    chClicked:
    begin
      if MoveDetected then
      begin
        case ClickedButton of
          mbLeft:
          case ClickedPart of
            cpNone:  StartZoom(chZoom);
            cpFrame: StartMove(chMove);
            cpBlobMin: StartMove(chStretchMin);
            cpBlobMax: StartMove(chStretchMax);
          end;
          mbRight: StartZoom(chPan);
        end
      end
    end;
    chMove: ContinueMove;
    chStretchMin: ContinueStretch(ClickedInfo^.BlobMax, ClickedInfo^.BlobMin, -1);
    chStretchMax: ContinueStretch(ClickedInfo^.BlobMin, ClickedInfo^.BlobMax, 1);
    chPan, chZoom: ContinueZoom;
  end
end;


procedure TSignatureEditForm.ChartMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);

  procedure DoPopup( Menu: TPopupMenu);
  var
    p: TPoint;
  begin
    p:= TControl(Sender).ClientToScreen(veBase);
    PopupChart:= ClickedChart;
    Menu.Popup(p.X, p.Y);
  end;

  procedure EndEdit;
  var
    FL, FH: Integer;
    TL, TH: Double;
    cs: TSignature;
    NewCrit: TSignatureCriterion;
    NewF: Boolean;
  const
    B2 = BlobSize div 2;

    function GetCopyName: String;
    begin
      FmtStr(Result, CopyName, [ClickedInfo^.Criterion.Name])
    end;

  begin
    veProgress;
    NewF:= (ssCtrl in Shift) and (veState = chMove);
    InflateRect(veRect, -B2, -B2);
    with veRect do begin
      FH:= ClickedChart.RealY(Top);
      FL:= ClickedChart.RealY(Bottom);
      TL:= ClickedChart.RealT(Left);
      TH:= ClickedChart.RealT(Right);
    end;
    if NewF then
      NewCrit:= TSignatureCriterion(ClickedInfo^.Criterion.Clone(GetCopyName))
    else
      NewCrit:= ClickedInfo^.Criterion;

{    if FL = FH then
      NewCrit.CriterionType:= ctHorizontal
    else
      NewCrit.CriterionType:= ctVertical; }

    NewCrit.DisplayMoveTo(FL, FH, TL, TH);

    if NewF then begin
      if CurrentSig(cs) then begin
        cs.AddCriterion(NewCrit);
        CriteriaCountRefresh(cs);
        CriteriaListRefresh(NewCrit.ValidChannel);
        SetSelectedCriterion(NewCrit)
      end else begin
        NewCrit.Free
      end
    end;
    CriteriaListRefresh(ClickedChart.Channel);
  end;

  procedure EndZoom;
  var
    dT, _dt, z0, T0, T1: Double;
    dY, _dy, Y0, Y1: Double;  {waste stack for the sake of clear identifiers&&&}
  begin
    veProgress;
    if (abs(X - veBase.X) > MinZoom) and
       (abs(Y - veBase.Y) > MinZoom) then
    with ClickedChart do
    begin
      if X > veBase.X then  {Zoom in}
      begin
        BeginUpdate;
        SetTLimits(RealT(veBase.X), RealT(X));
        if veBase.Y > Y then
          SetYLimits(RealY(veBase.Y), RealY(Y))
        else
          SetYLimits(RealY(Y), RealY(veBase.Y));
        SynchronizeCharts(ClickedChart);
        EndUpdate;
      end else
      begin                      {Zoom out}
        BeginUpdate;
        try
          try
            if X <= veBase.X then
              z0:= RealT(X)
            else
              z0:= RealT(veBase.X);

            _dt:= TMax - TMin;

            dT:= Sqr(_dt) / abs(RealT(X) - RealT(veBase.X));
            T0:= TMin - (z0 - TMin)*dT / _dt;
            T1:= T0 + dT;
            SetTLimits(T0, T1);
            if Y >= veBase.Y then
              z0:= RealY(Y)
            else
              z0:= RealY(veBase.Y);
            _dy:= YMax - YMin;

            dY:= Sqr(_dy) / abs(RealY(Y) - RealY(veBase.Y));
            Y0:= YMin - (z0 - YMin)*dY / _dy;
            Y1:= Y0 + dY;
            SetYLimits(Round(Y0), Round(Y1));
            SynchronizeCharts(ClickedChart);
          except
            on E : Exception do begin
              { A bit harsh but what the hell }
              Exit;
            end;
          end;
        finally
          EndUpdate
        end;
      end
    end;
  end;

  procedure EndPan;
  var
    dt: Double;
    dy: Integer;
  begin
    veProgress;
    with ClickedChart do
    begin
      dt:= RealT(X) - RealT(veBase.X);
      dy:= RealY(Y) - RealY(veBase.Y);
      BeginUpdate;
      SetTLimits(TMin - dt, TMax - dt);
      SetYLimits(Round(YMin - dy), Round(YMax - dy));
      SynchronizeCharts(ClickedChart);
      EndUpdate
    end
  end;

begin
  if (Sender = ClickedChart) and (Button = ClickedButton) then
  begin  {ignore chNone}
    case veState of
      chClicked       : if ClickedButton = mbRight then begin
        if ClickedPart = cpNone then
          DoPopup(ChartPopup);
      end;

      chMove,
      chStretchMin,
      chStretchMax    : EndEdit;
      chPan           : EndPan;
      chZoom          : EndZoom;
    end;
    veState:= chNone;
    ClickedChart:= nil;
    ClickedInfo:= nil
  end
end;

procedure TSignatureEditForm.SynchronizeCharts(aChart : TCurveChart);
var I : TGatherField;
begin
  for I := Low(TGatherField) to High(TGatherField) do begin
    Charts[I].BeginUpdate;
    Charts[I].TMin := aChart.TMin;
    Charts[I].TMax := aChart.TMax;
    Charts[I].EndUpdate;
  end;
end;

procedure TSignatureEditForm.veCancel;
begin
  if veState in [chMove, chStretchMin, chStretchMax, chPan, chZoom] then
    veProgress;
  veState:= chNone;
  ClickedChart:= nil;
  ClickedPart:= cpNone;
  ClickedInfo:= nil
end;

procedure TSignatureEditForm.veProgress;
var
  Pts: array[0..4] of TPoint;
begin
  if Assigned(ClickedChart) then
    with ClickedChart.Canvas do begin
      Pen.Width:= 0;
      Pen.Color:= clWhite;
      Pen.Mode:= pmXor;
      Pen.Style:= psSolid;
      if veState = chPan then begin
        MoveTo(veRect.Left, veRect.Top);
        LineTo(veRect.Right, veRect.Bottom)
      end else begin
        if veState = chZoom then begin
          if veRect.Right > veRect.Left then
            Pen.Style:= psDot
          else
            Pen.Style:= psDash
        end else begin
          Pen.Style:= psSolid
        end;
        with veRect do begin
          Pts[0]:= TopLeft;
          Pts[2]:= BottomRight;
          Pts[1].X:= Right; Pts[1].Y:= Top;
          Pts[3].X:= Left; Pts[3].Y:= Bottom;
          Pts[4]:= Pts[0]
        end;
        Polyline(Pts)
    end
  end
end;

procedure TSignatureEditForm.ChartPopupFitAllClick(Sender: TObject);
begin
  if Assigned(PopupChart) then
    with PopupChart do begin
      BeginUpdate;
      AutoscaleT;
      AutoscaleY;
      EndUpdate
    end;
  SynchronizeCharts(PopupChart);
end;

procedure TSignatureEditForm.ChartDblClick(Sender: TObject);
begin
  if (ClickedPart <> cpNone) and (veState = chNone) then begin
    CriterionEdit;
    veState:= chDouble
  end
end;

procedure TSignatureEditForm.CriterionNewButtonClick(Sender: TObject);
begin
  CriterionNew
end;

procedure TSignatureEditForm.ChartPopupNewClick(Sender: TObject);
var
  NewCrit: TSignatureCriterion;
  fl, fh, df: Integer;
  tl, th, dt: Double;
  Dlg: TCriteriaEditDlg;
  CritType: TCriterionType;
  cs: TSignature;
begin
  if Assigned(PopupChart) then
  begin
    if CurrentSig(cs) and (FieldList.Items.Count > 0) then {require fields}
    begin
      with PopupChart do
      begin
        fl:= RealY(veBase.Y);
        tl:= RealT(veBase.X);
        dt:= (TMax - TMin) / NewCriterionSize;
        df:= Round(YMax - YMin) div NewCriterionSize;
        if TMenuItem(Sender).Default then {horizontal}
        begin
          CritType:= ctHorizontal;
          th:= tl + dt;
          fh:= fl
        end else
        begin
          CritType:= ctVertical;
          th:= tl;
          fh:= fl + df
        end;
      end;
      NewCrit:= TSignatureCriterion.Create( cs.FieldName[PopupChart.Channel], '', cs,
              InstalledSigEditor.DefaultSignalTag,
              InstalledSigEditor.DefaultSignalTag,
              InstalledSigEditor.DefaultEnableTag,
              CritType, fl, fh, tl, th);
      NewCrit.ValidChannel:= PopupChart.Channel;
      Dlg:= TCriteriaEditDlg.Create(Application);
      try
        if Dlg.Execute(NewCrit, FieldList.Items,
                                InstalledSigEditor.SignalTagList,
                                InstalledSigEditor.EnableTagList) then begin
          cs.AddCriterion(NewCrit);
          CriteriaCountRefresh(cs);
          CriteriaListRefresh(NewCrit.ValidChannel);
          SetSelectedCriterion(NewCrit)
        end else begin
          NewCrit.Free
        end
      finally
        Dlg.Free
      end
    end
  end
end;

procedure TSignatureEditForm.CriteriaListDrawItem(Control: TWinControl;
  Index: Integer; Rect: TRect; State: TOwnerDrawState);
begin
  with TListBox(Control).Canvas do
  begin
    FillRect(Rect);
    TextRect(Rect, Rect.Left, Rect.Top, CriteriaList.Items[Index])
  end
end;

procedure TSignatureEditForm.FitButtonClick(Sender: TObject);
var
  i: Integer;
begin
  for i:= Low(TGatherField) to High(TGatherField) do
  with Charts[i] do
  begin
    BeginUpdate;
    AutoscaleT;
    AutoscaleY;
    EndUpdate
  end
end;

procedure TSignatureEditForm.DefaultsButtonClick(Sender: TObject);
begin
  ShowRTV(false)
end;

procedure TSignatureEditForm.ShowRTV( FieldListOK: Boolean);
var
  i: Integer;
  cs: TSignature;
begin
  if CurrentSig(cs) then
  begin
    for i:= Low(TGatherField) to High(TGatherField) do
    with Charts[i] do
    begin
      BeginUpdate;
      SetTLimits(cs.RTV.Limits[i].TMin, cs.RTV.Limits[i].TMax);
      SetYLimits(cs.RTV.Limits[i].YMin, cs.RTV.Limits[i].YMax);
      EndUpdate
    end;
    HoldCheckRefresh:= True; {stop FieldList.SetChecked from generating events}
    if not FieldListOK then
    for i:= 0 to FieldList.Items.Count - 1 do
      FieldList.Checked[i]:= TGatherField(i) in cs.RTV.Fields;
    HoldCheckRefresh:= false;
    if not FieldListOK then
      FieldCheckRefresh;
  end
end;

procedure TSignatureEditForm.SaveRTV;
var
  i: Integer;
  cs: TSignature;
begin
  if CurrentSig(cs) then
  begin
    for i:= Low(TGatherField) to High(TGatherField) do
    with Charts[i] do
    begin
      cs.RTV.Limits[i].TMin:= TMin;
      cs.RTV.Limits[i].TMax:= TMax;
      cs.RTV.Limits[i].YMin:= YMin;
      cs.RTV.Limits[i].YMax:= YMax
    end;
    cs.RTV.Fields:= [];
    for i:= 0 to FieldList.Items.Count - 1 do
    begin
      if FieldList.Checked[i] then
        Include(cs.RTV.Fields, TGatherField(i))
    end;
  end
end;

procedure TSignatureEditForm.SetDefaultsButtonClick(Sender: TObject);
begin
  SaveRTV
end;

procedure TSignatureEditForm.ChartPopupZoomDefaultClick(Sender: TObject);
var
  i: Integer;
  cs: TSignature;
begin
  if CurrentSig(cs) and Assigned(PopupChart) then
  with PopupChart do
  begin
    BeginUpdate;
    i:= Channel;
    SetTLimits(cs.RTV.Limits[i].TMin, cs.RTV.Limits[i].TMax);
    SetYLimits(cs.RTV.Limits[i].YMin, cs.RTV.Limits[i].YMax);
    SynchronizeCharts(PopupChart);
    EndUpdate
  end
end;

procedure TSignatureEditForm.ChartPopupSetDefaultsClick(Sender: TObject);
var
  i: Integer;
  cs: TSignature;
begin
  if CurrentSig(cs) and Assigned(PopupChart) then
  with PopupChart do
  begin
    i:= Channel;
    cs.RTV.Limits[i].TMin:= TMin;
    cs.RTV.Limits[i].TMax:= TMax;
    cs.RTV.Limits[i].YMin:= YMin;
    cs.RTV.Limits[i].YMax:= YMax
  end
end;


procedure TSignatureEditForm.SampleAdd;
var
  cs: TSignature;
  H, I : Integer;
begin
  if CurrentSig(cs) and Assigned(Gather) and (Gather.GatherStatus <> gsGathering) then begin
    try
      for H := 0 to CoreCNC.SysObj.Heads - 1 do begin
        if H in SelectedHeads then begin
          I := cs.AddSample(Gather[H]);
          cs.SampleCurve[I].Show := True;
          HeadCheck[H].Checked := False;
        end;
      end;
      SampleListRefresh;
    except
      on E: ESignatureFile do
        Application.ShowException(E)
    end;
  end;
end;

procedure TSignatureEditForm.SampleDelete;
var
  cs: TSignature;
  ii: Integer;
  samp: TCurve;
begin
  if CurrentSig(cs) then
  begin
    ii:= SampleList.ItemIndex;
    if (ii >= 0) and
     (MessageDlg(Format(ConfirmDeleteMsg, [SampleMsg]),
         mtConfirmation, [mbOk, mbCancel], 0) = mrOK) then
    begin
      Samp:= TCurve(SampleList.Items.Objects[ii]);
      cs.DeleteSample(samp);
      SampleListRefresh
    end
  end
end;

procedure TSignatureEditForm.SampleDeleteButtonClick(Sender: TObject);
begin
  SampleDelete
end;

procedure TSignatureEditForm.AverageBoxClick(Sender: TObject);
begin
  SampleCheckRefresh
end;

procedure TSignatureEditForm.FitCriteria( Channel: Integer);
const
  Margin = 5; {pixels}
var
  i: Integer;
  fl, fh: Integer;
  tl, th: Double;
  _Ymin, _YMax: Integer;
  _TMin, _TMax: Double;
  ChangedMin, ChangedMax: Boolean;
  TCorrect: Double;
  YCorrect: Integer;

{correcting for the 1/2 a blob outside the axes is a difficult thing -
changing the scale of an axis changes the 'real' size of the blob. The
method employed here is approximate. The rescale to make the criterion visible
is applied, then the size of the blob is calcuated. The limits are then
changed again - this will make the previous blob size calc invalid, but
if the blob size is a small proportion of the view size the error should be
negligible}

begin
  if Criteria[Channel].Count > 0 then
  begin
    PCriterionInfo(Criteria[Channel].Items[0]).Criterion.GetVisualBoundingPoints(_TMin, _TMax, _YMin, _YMax, False);
    for i:= 1 to Criteria[Channel].Count - 1 do
    begin
      PCriterionInfo(Criteria[Channel].Items[i]).Criterion.GetVisualBoundingPoints(tl, th, fl, fh, False);
      if tl < _TMin then _TMin:= tl;
      if th > _TMax then _TMax:= th;
      if fl < _YMin then _YMin:= fl;
      if fh > _YMax then _YMax:= fh
    end;
    with Charts[Channel] do
    begin
      ChangedMin:= False;
      ChangedMax:= False;
      BeginUpdate;
      if _TMin < TMin then
      begin
        TMin:= _TMin;
        ChangedMin:= true
      end;
      if _TMax > TMax then
      begin
        TMax:= _TMax;
        ChangedMax:= true
      end;
      if ChangedMin or ChangedMax then
      begin
        TCorrect:= BlobSize * TScale;
        if ChangedMin then
          TMin:= TMin - TCorrect;
        if ChangedMax then
          TMax:= TMax + TCorrect;
      end;
      ChangedMin:= False;
      ChangedMax:= False;
      if _YMin < YMin then
      begin
        YMin:= _YMin;
        ChangedMin:= true
      end;
      if _YMax > YMax then
      begin
        YMax:= _YMax;
        ChangedMax:= true
      end;
      if ChangedMin or ChangedMax then
      begin
        YCorrect:= Round(BlobSize * YScale);
        if ChangedMin then
          YMin:= YMin - YCorrect;
        if ChangedMax then
          YMax:= YMax + YCorrect;
      end;
      EndUpdate
    end
  end
end;

procedure TSignatureEditForm.SetOLD( aOLD: Boolean);
var
  i: Integer;
{tried Control.Enabled and Control.ControlStyle [csNoStdEvents].
 Neither worked!}
begin
  for i:= Low(TGatherField) to High(TGatherField) do
  begin
    if aOLD then
    begin
      Charts[i].OnMouseDown:= ChartMouseDown;
      Charts[i].OnMouseUp:= ChartMouseUp;
      Charts[i].OnDblClick:= ChartDblClick;
      Charts[i].OnMouseMove:= ChartMouseMove;
    end else
    begin
      Charts[i].OnMouseDown:= nil;
      Charts[i].OnMouseUp:= nil;
      Charts[i].OnDblClick:= nil;
      Charts[i].OnMouseMove:= nil;
    end
  end;

  ChartPopupNew.Enabled := False;
  ChartPopupPaste.Enabled := False;
  ChartPopupSetDefaults.Enabled := False;

  ProgNoUD.Enabled:= not aOLD;
  FOLD:= aOLD
end;


procedure TSignatureEditForm.FitCriteriaButtonClick(Sender: TObject);
var
  i: Integer;
begin
  for i:= Low(TGatherField) to High(TGatherField) do
    FitCriteria(i)
end;

procedure TSignatureEditForm.ChartPopupFitCriteriaClick(Sender: TObject);
begin
  if Assigned(PopupChart) then
    FitCriteria(PopupChart.Channel)
end;

{procedure TSignatureEditForm.PasteCriterion;
begin
end;

procedure TSignatureEditForm.CopyCriterion(All: Boolean; ChartList: TList);
begin
end; }

{temp to test SelectedHoleProgram property}
{procedure TSignatureEditForm.SpeedButton1Click(Sender: TObject);
var
  s: string;
begin
  s:= '0';
  if InputQuery('Input', 'ProgNo', s) then
    SelectedHoleProgram:= StrToInt(s)
end; }

procedure TSignatureEditForm.SampleAddButtonClick(Sender: TObject);
begin
  SampleAdd
end;
procedure TSignatureEditForm.DelSigMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var
  cs: TSignature;
begin
  if CurrentSig(cs) and
     (MessageDlg(SigDeleteAreYouSureLang,
                     mtConfirmation, [mbOK, mbCancel], 0) = mrOK) then
    DeleteCS
end;


procedure TSignatureEditForm.DelSigButtonClick(Sender: TObject);
var
  cs: TSignature;
begin
  if CurrentSig(cs) and
     (MessageDlg(SigDeleteAreYouSureLang,
                     mtConfirmation, [mbOK, mbCancel], 0) = mrOK) then
    DeleteCS
end;

procedure TSignatureEditForm.OKSigMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
 Self.ModalResult := mrOK;
end;

procedure TSignatureEditForm.CancelSigMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
 Self.ModalResult := mrCancel;
end;


procedure TSignatureEditForm.CriteriaNewMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  CriterionNew
end;

procedure TSignatureEditForm.CriteriaDeleteMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  CriterionDelete;
end;

procedure TSignatureEditForm.CriteriaEditMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  CriterionEdit;
end;

procedure TSignatureEditForm.SamplesDeleteMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  SampleDelete
end;

procedure TSignatureEditForm.SamplesAddMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  SampleAdd
end;

procedure TSignatureEditForm.ViewShowMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  ShowRTV(false);
end;

procedure TSignatureEditForm.ViewSaveMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  SaveRTV;
end;

procedure TSignatureEditForm.FitSampleMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  i: Integer;
begin
  for i:= Low(TGatherField) to High(TGatherField) do
  with Charts[i] do
  begin
    BeginUpdate;
    AutoscaleT;
    AutoscaleY;
    EndUpdate
  end
end;

procedure TSignatureEditForm.FitCriteriaMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  i: Integer;
begin
  for i:= Low(TGatherField) to High(TGatherField) do
    FitCriteria(i)
end;

procedure TSignatureEditForm.DeleteCS;
var
  cs: TSignature;
begin
  if CurrentSig(cs) then begin
    SigFile.SignatureDelete(cs);
//    cs.Free;
//    SigFile[ProgNoUD.Position]:= nil;
    SigParamsRefresh
  end
end;

procedure TSignatureEditForm.ProgNoUDClick(Sender: TObject; Button: TUDBtnType);
begin
  SigParamsRefresh;
end;

procedure TSignatureEditForm.HeadSelectClick(Sender : TObject);
begin
  SigParamsRefresh;
end;


procedure TSignatureEditForm.HeadSelectRefresh;
var I : Integer;
begin
  // If not an On-Line-Display generate the selected heads from the
  // gui HeadCheck
  // if this is the OLD then SelectedHeads is already valid so just update the
  // Charts.
  if not OLD then begin
    if Assigned(FGather) then begin
      for I := 0 to CoreCNC.SysObj.Heads - 1 do begin
        if FGather.HoleProgram[I] = ProgNoUD.Position then
          HeadCheck[I].Enabled := True
        else
          HeadCheck[I].Enabled := False;
      end;
    end;

    SelectedHeads := [];
    for I := 0 to CoreCNC.SysObj.Heads - 1 do
      if HeadCheck[I].Enabled and HeadCheck[I].Checked then
        Include(SelectedHeads, I);
  end;

  for I := Low(TGatherField) to High(TGatherField) do
    Charts[I].Heads := SelectedHeads;
end;


procedure TSignatureEditForm.FormCreate(Sender: TObject);
var I : Integer;
begin
  CriteriaCountFormat := SysObj.Localized.GetString('$FORMAT_CRITERIA');
  SampleCountFormat   := SysObj.Localized.GetString('$FORMAT_SAMPLES');
  SampleListFormat    := SysObj.Localized.GetString('$FORMAT_SAMPLES_LIST');
  CriteriaListFormat    := SysObj.Localized.GetString('$FORMAT_CRITERIA_LIST');
  ConfirmDeleteMsg      := SysObj.Localized.GetString('$SIG_CONFIRM_DELETE');
  SigDeleteAreYouSureLang := SysObj.Localized.GetString('$SIG_CONFIRM_CURRENT_DELETE');
  SampleMsg := SysObj.Localized.GetString('$SAMPLE_LOWERCASE');
  CriterionMsg := SysObj.Localized.GetString('$CRITERION_LOWERCASE');
  CopyName := SysObj.Localized.GetString('$FORMAT_COPY_NAME');
  SignatureFileDiscardedLang := SysObj.Localized.GetString('$SIG_FILE_DISCARDED');


  ButtonDelSig := TThreeLineButton.create(Self);
  with ButtonDelSig do
    begin
    Width := 80;
    Height := 45;
    ButtonColor := clBtnFace;
    LegendFont.Name := 'Verdana';
    MaxFontSize := 12;
    LegendFont.Color := clBlack;
    Legend := '$DELETE_SIGNATURE';
    Parent := HoleProgPanel;
    Left := 304;
    Top := 5;
    OnMouseUp := DelSigMouseUp;
    end;

  ButtonOK := TThreeLineButton.create(Self);
  with ButtonOK do
    begin
    Width := 80;
    Height := 45;
    LegendFont.Name := 'Verdana';
    MaxFontSize := 12;
    LegendFont.Color := clBlack;
    ButtonColor := clBtnFace;
    Legend := '$OK';
    Parent := HoleProgPanel;
    Left := 391;
    Top := 5;
    OnMouseUp := OKSigMouseUp;
    end;

  ButtonCancel := TThreeLineButton.create(Self);
  with ButtonCancel do
    begin
    Width := 80;
    Height := 45;
    MaxFontSize := 12;
    LegendFont.Name := 'Verdana';
    LegendFont.Color := clBlack;
    ButtonColor := clBtnFace;
    Legend := '$CANCEL';
    Parent := HoleProgPanel;
    Left := 479;
    Top := 5;
    OnMouseUp := CancelSigMouseUp;
    end;

  ButtonCriteriaNew:= TThreeLineButton.create(Self);
  with ButtonCriteriaNew do
    begin
    Width := 80;
    Height := 35;
    LegendFont.Name := 'Verdana';
    MaxFontSize := 12;
    LegendFont.Color := clBlack;
    ButtonColor := clBtnFace;
    Legend := '$NEW';
    Parent := CriteriaGroup;
    Left := 133;
    Top := 12;
    OnMouseUp := CriteriaNewMouseUp;
    end;

  ButtonCriteriaEdit:= TThreeLineButton.create(Self);
  with ButtonCriteriaEdit do
    begin
    Width := 80;
    Height := 35;
    LegendFont.Name := 'Verdana';
    MaxFontSize := 12;
    LegendFont.Color := clBlack;
    ButtonColor := clBtnFace;
    Legend := '$EDIT';
    Parent := CriteriaGroup;
    Left := 133;
    Top := 50;
    OnMouseUp := CriteriaEditMouseUp;
    end;

  ButtonCriteriaDelete:= TThreeLineButton.create(Self);
  with ButtonCriteriaDelete do
    begin
    Width := 80;
    Height := 35;
    LegendFont.Name := 'Verdana';
    MaxFontSize := 12;
    LegendFont.Color := clBlack;
    ButtonColor := clBtnFace;
    Legend := '$DELETE';
    Parent := CriteriaGroup;
    Left := 133;
    Top := 87;
    OnMouseUp := CriteriaDeleteMouseUp;
    end;


  ButtonSampleAdd   := TThreelineButton.Create(Self);
  with ButtonSampleAdd do
    begin
    Width := 80;
    Height := 44;
    LegendFont.Name := 'Verdana';
    MaxFontSize := 12;
    LegendFont.Color := clBlack;
    ButtonColor := clBtnFace;
    Legend := '$ADD';
    Parent := SamplesGroup;
    Left := 133;
    Top := 30;
    OnMouseUp := SamplesAddMouseUp;
    end;

  ButtonSampleDelete   := TThreelineButton.Create(Self);
  with ButtonSampleDelete do
    begin
    Width := 80;
    Height := 44;
    LegendFont.Name := 'Verdana';
    MaxFontSize := 12;
    LegendFont.Color := clBlack;
    ButtonColor := clBtnFace;
    Legend := '$DELETE_SAMPLE';
    Parent := SamplesGroup;
    Left := 133;
    Top := 77;
    OnMouseUp := SamplesDeleteMouseUp;
    end;

  ButtonViewSave   := TThreelineButton.Create(Self);
  with ButtonViewSave do
    begin
    Width := 80;
    Height := 44;
    LegendFont.Name := 'Verdana';
    MaxFontSize := 12;
    LegendFont.Color := clBlack;
    ButtonColor := clBtnFace;
    Legend := 'Save';
    Parent := ViewGroup;
    Left := 133;
    Top := 20;
    OnMouseUp := ViewSaveMouseUp;
    end;

  ButtonViewShow := TThreelineButton.Create(Self);
  with ButtonViewShow do
    begin
    Width := 80;
    Height := 44;
    MaxFontSize := 12;
    LegendFont.Name := 'Verdana';
    LegendFont.Color := clBlack;
    ButtonColor := clBtnFace;
    Legend := '$SHOW';
    Parent := ViewGroup;
    Left := 133;
    Top := 67;
    OnMouseUp := ViewShowMouseUp;
    end;

  ButtonFitSample:= TThreelineButton.Create(Self);
  with ButtonFitSample do
    begin
    Width := 80;
    Height := 42;
    LegendFont.Name := 'Verdana';
    MaxFontSize := 12;
    LegendFont.Color := clBlack;
    ButtonColor := clBtnFace;
    Legend := '$FIT_SAMPLES';
    Parent := HeadGroup;
    Left := 133;
    Top := 10;
    OnMouseUp := FitSampleMouseUp;
    end;

  ButtonFitCriteria:= TThreelineButton.Create(Self);
  with ButtonFitCriteria do
    begin
    Width := 80;
    Height := 42;
    LegendFont.Name := 'Verdana';
    MaxFontSize := 12;
    LegendFont.Color := clBlack;
    ButtonColor := clBtnFace;
    Legend := '$FIT_CRITERIA';
    Parent := HeadGroup;
    Left := 133;
    Top := 53;
    OnMouseUp := FitCriteriaMouseUp;
    end;

  for I := 0 to CoreCNC.SysObj.Heads - 1 do begin
    HeadCheck[I] := TCheckBox.Create(Self);
    HeadCheck[I].Parent := HeadGroup;
    HeadCheck[I].Left := 10;
    HeadCheck[I].Top := (I * 20) + 10;
    HeadCheck[I].Caption := Format(SysObj.Localized.GetString('$HEAD_D'), [I+1]);
    HeadCheck[I].Checked := False;;
    HeadCheck[I].OnClick := HeadSelectClick;
  end;
end;

procedure TSignatureEditForm.ChartPopupEditLimitsClick(Sender: TObject);
var I : TGatherField;
begin
  if Assigned(PopupChart) then begin
    PopupChart.EditLimits;

    for I := Low(TGatherField) to High(TGatherField) do begin
       Charts[I].TMin := Charts[PopupChart.Channel].TMin;
       Charts[I].TMax := Charts[PopupChart.Channel].TMax;
    end;
  end;
end;

procedure TSignatureEditForm.SamplePeriodComboChange(Sender: TObject);
begin
  if SamplePeriodCombo.ItemIndex <> -1 then begin
    if SigFile.SignatureCount > 0 then begin
      if Gather.IndexOfSamplePeriod[Sigfile.Period] <> SamplePeriodCombo.ItemIndex then begin
  {      if(MessageDlg(ConfirmSampleRateLang, mtConfirmation, [mbOk, mbCancel], 0) = mrOK) then begin
          DeleteCS;
          SigFile.Clean;
          SigFile.Period := Gather.SamplePeriodIndex[SamplePeriodCombo.ItemIndex];
          SigParamsRefresh;
        end else begin
          SamplePeriodCombo.ItemIndex := Gather.IndexOfSamplePeriod[SigFile.Period];
        end; }
        SigFile.Period := Gather.SamplePeriodIndex[SamplePeriodCombo.ItemIndex];
      end;
    end;
  end;
end;






initialization
  CoreCNC.RegisterCNCClass(TSignatureEditor);
  CoreCNC.RegisterCNCClass(TSignatureOnline);
end.

