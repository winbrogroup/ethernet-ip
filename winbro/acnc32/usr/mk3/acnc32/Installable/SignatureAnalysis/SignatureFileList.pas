unit SignatureFileList;

interface

uses Windows, SysUtils, Classes, Graphics, Forms, Controls, StdCtrls,
  Buttons, ExtCtrls, FileCtrl, CNC32, CNCTypes, CoreCNC, Named;

type
  TSignatureFileListForm = class(TForm)
    GroupBox1: TGroupBox;
    Panel1: TPanel;
    EditLabel: TLabel;
    FilenameEdit: TEdit;
    OKBtn: TButton;
    CancelBtn: TButton;
    Label1: TLabel;
    EditFilter: TEdit;
    UpdateBtn: TButton;
    FileList: TListBox;
    procedure UpdateBtnClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FileListClick(Sender: TObject);
  private
    { Private declarations }
    //FOptions: TSelectFileOptions;
    //DefExt,
    FilenameResult: String;
    procedure GenerateFileList;
  public
    { Public declarations }
    constructor Create(Aowner : TComponent); override;

    function Execute( {Options: TSelectFileOptions; }var FileName : string): Boolean;
  end;

function SelectFile(var Filename: String): Boolean;

function SignatureAuditPath(aHead : Integer) : string;

const
  SignatureAuditMask = '*.Sig';

implementation

uses
  Dialogs;

resourcestring
  NoFilesMsg = 'There are no files available';

var
  FLF: TSignatureFileListForm;

function SignatureAuditPath(aHead : Integer) : string;
begin
  Result := LocalPath + 'Audit\' + 'H' + IntToStr(aHead) + '\';
end;

{$R *.DFM}
function TSignatureFileListForm.Execute(var FileName : string): Boolean;
begin
  FilenameEdit.Text:= Filename;
  GenerateFileList;

  if not FilenameEdit.Enabled then begin
    if Filelist.Items.Count = 0 then begin
      MessageDlg(NoFilesMsg, mtError, [mbOk], 0);
      Result:= false;
      Exit
    end;
    FilenameEdit.Text:= FileList.Items[0]
  end;
  Result:= ShowModal = mrOK;
  if Result then
    Filename:= SignatureAuditPath(1) + FilenameEdit.Text;

  if not FileExists(FileName) then
    Result := False;
end;

function SelectFile(var FileName : string): Boolean;
begin
  if FLF = nil then begin
    FLF:= TSignatureFileListForm.Create(Application.MainForm);
    try
      Result:= FLF.Execute(Filename);
    finally
      FLF.Free;
      FLF:= nil;
    end
  end else
   Result:= False;
end;


procedure TSignatureFileListForm.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
  CanClose:= true;
  if ModalResult = mrOK then
  begin
    FilenameResult:= FilenameEdit.Text;
    CanClose:= FileExists(SignatureAuditPath(1) + FilenameResult)
  end
end;


procedure TSignatureFileListForm.FileListClick(Sender: TObject);
begin
  if FileList.ItemIndex <> -1 then
    FileNameEdit.Text := FileList.Items[FileList.ItemIndex];
end;

procedure TSignatureFileListForm.UpdateBtnClick(Sender: TObject);
begin
  if EditFilter.Text = '' then
    EditFilter.Text:= '*.sig';

  GenerateFileList;
end;

constructor TSignatureFileListForm.Create(Aowner: TComponent);
begin
  inherited;
  CoreCNC.LocalizeForm(Self);
end;

procedure TSignatureFileListForm.GenerateFileList;
var F: TSearchRec;
    I: Integer;
begin
  FileList.Clear;
  I:= 0;

  try
    if FindFirst(SignatureAuditPath(1) + EditFilter.Text, faAnyFile, F) = 0 then begin
      repeat
        Inc(I);
        if I > 500 then
          Exit;
        FileList.Items.Add(F.Name);
      until FindNext(F) <> 0;
    end;
  finally
    FindClose(F);
  end;
end;

end.
