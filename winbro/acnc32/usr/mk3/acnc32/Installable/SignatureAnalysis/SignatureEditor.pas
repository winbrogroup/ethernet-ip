unit SignatureEditor;

interface

uses Classes, SysUtils, Graphics, Forms, Controls, ExtCtrls,NamedPlugin,
     CNCTypes, CNC32, CoreCNC, SignatureEditorFrm, CompoundFiles,
     Gather, Signature, CNCAbs, {EDMHead,} Named, Curves, FaultRegistration,
     ISignature;

const
  PeriodFormat = 'Period %.9f';

type
  TSignatureEditor = class(TSectionEditor)
  private
    CompRequiresGather : String;
    IncompatibleHoleProgram : String;
    Gather: TAbstractGather; {not strictly needed - could be local to execute}
    Prog_IH: array[0..3] of TAbstractTag; {for initialising the dialog...}
    TrueTag : TAbstractTag;
    FalseTag : TAbstractTag;
    SamplePoints : Integer;
  protected
  public
    SignalTagList : TStringList;
    EnableTagList : TStringList;
    constructor Create(aOwner : TComponent); override;
    function Execute(Manager: TCompoundFile): Boolean; override;
             {instantiate Form (including Signature) as local for duration.
             Pull in Gather only if Live Edit}
    function SectionName: String; override;
    procedure InstallComponent(Params : TParamStrings); override;
    procedure Installed; override;
    destructor Destroy; override;
    function DefaultSignalTag: TCriterionTag;
    function DefaultEnableTag: TCriterionTag;
    function GetSignalTag(Sender : TObject; aName : string) : TCriterionTag;
    function GetEnableTag(Sender : TObject; aName : string) : TCriterionTag;

  end;

  {Single head only! Head1 &&&}
  TSignatureOnline = class(TExpandDisplay, ISignatureHost)
  private
    CompRequiresGather : String;
    IncompatibleHoleProgram : String;
    SignatureFile : TSignatureFile;
    Sel : array [0..3] of TAbstractTag;
    EventTagList : TList;
    OPPName      : TAbstractTag;
    Start : array [0..3] of TAbstractTag;
    TitlePanel : TPanel;
    SignatureClients: array of ISignatureClient;
    function IncompatibleReset(FID : Integer): Boolean;
    procedure TestSignature;
    procedure SignatureScan(aTag : TAbstractTag);
    procedure HeadSelect(aTag : TAbstractTag);
    procedure AuditEvents(aTag : TAbstractTag);
    procedure CheckSignature;
    procedure HeadSelectChange(aHead : Integer);
  protected
    Prog_IH: array [0..3] of TAbstractTag;
    Audit : array [0..3] of TStringList;
    LoggingStartTime : array [0..3] of TDateTime;
    Gather : TAbstractGather;
    procedure CNCResetRW(aTag : TAbstractTag); override;
    procedure GatherStartStop(aTag : TAbstractTag);
    procedure ResetSignatureCallback(aTag : TAbstractTag);
    procedure ResetSignature(aHead : Integer);
    procedure WriteAudit(aHead : Integer); virtual;
    procedure IOSweep(aTag : TAbstractTag);
    procedure Resize; override;
  public
    Form: TSignatureEditForm;  {this contains the 'real' signature data}
    constructor Create(aOwner: TComponent); override;
    procedure InstallComponent(Params : TParamStrings); override;
    procedure Installed; override;
    procedure NewActiveFile(aTag : TAbstractTag);
    procedure ProgramChange(aTag : TAbstractTag);

    procedure FireGateEnable(Field: Integer; Domain: TCriterionDomain);
    procedure AddClient(Client: ISignatureClient); stdcall;
    procedure RemoveClient(Client: ISignatureClient); stdcall;
  end;

var
  InstalledSigEditor : TSignatureEditor;

const
  SectionSignatureData = 'SIGNATUREDATA';

implementation

destructor TSignatureEditor.Destroy;
begin
  if Assigned(SignalTagList) then
    SignalTagList.Free;
  if Assigned(EnableTagList) then
    EnableTagList.Free;
  inherited;
end;

constructor TSignatureEditor.Create(aOwner : TComponent);
begin
  inherited Create(aOwner);
  if Assigned(InstalledSigEditor) then
    raise ECNCInstallFault.Create('Only one instance of ' + ClassName + ' Is Permitted');
  InstalledSigEditor := Self;
  SignalTagList := TStringList.Create;
  EnableTagList := TStringList.Create;
end;

const
  TrueName = 'True';
  FalseName = 'False';

  GatherName = 'Gather';
  GatherDefault = GatherName;


  OutputPrefix = 'Out.';
  InputPrefix = 'In.';


  SigAlwaysTrueLang = 'SigAlways';
  SigAlwaysFalseLang = 'SigNever';
  LocalCriteriaTagLang = 'CriterionSignal';


//resourcestring

//  CompRequiresGather = '%s: %s component requires %s: TAbstractGather';
//  IncompatibleHoleProgram = 'hole %d signature incompatible with gather settings';


procedure TSignatureEditor.InstallComponent(Params : TParamStrings);
var
  GatName: String;
  I, Head : Integer;
  Tmp : string;
  Tag : TAbstractTag;
  CTag : TCriterionTag;
begin
  inherited InstallComponent(Params);

  SamplePoints := Params.ReadInteger('SamplePoints', 150);

  GatName:= Params.ReadString(GatherName, GatherDefault);
  Gather:= TAbstractGather(GetComponent(GatName));
  if Gather = nil then
    raise ECNCInstallFault.CreateFmt(CompRequiresGather, [Name, ClassName, GatName]);

//  TrueTag := TTrueIOTag.Create(Self, SigAlwaysTrueLang);
  TrueTag := TagPublish(SigAlwaysTrueLang, TTrueIOTag);
  FalseTag := TagPublish(SigAlwaysFalseLang, TFalseIOTag);

  CTag := TCriterionTag.Create(Self);
  CTag.Name := TrueName;
  for Head := 0 to SysObj.Heads - 1 do
    CTag.Signal[Head] := TrueTag;
  SignalTagList.AddObject(CTag.Name, CTag);


  TagPublish(SigAlwaysFalseLang, TFalseIOTag);
  CTag := TCriterionTag.Create(Self);
  CTag.Name := FalseName;
  for Head := 0 to SysObj.Heads - 1 do
    CTag.Signal[Head] := FalseTag;
  SignalTagList.AddObject(CTag.Name, CTag);

  for I := 1 to 5 do begin
    Tmp := Format('%s%d', [LocalCriteriaTagLang, I]);
    CTag := TCriterionTag.Create(Self);
    CTag.Name := Tmp;
    for Head := 0 to SysObj.Heads - 1 do begin
      Tag := TagPublish('H' + IntToStr(Head + 1) + Tmp, TMethodTag);
//      Tag.Colour := clBlack;
      CTag.Signal[Head] := Tag;
    end;
    SignalTagList.AddObject(CTag.Name, CTag);
  end;

  for I := 0 to Params.Count - 1 do begin
    if CompareText(Copy(Params.Names[I], 1, Length(OutputPrefix)), OutputPrefix) = 0 then begin
      Tmp := Params.Names[I];
      System.Delete(Tmp, 1, Length(OutputPrefix));
      CTag := TCriterionTag.Create(Self);
      CTag.Name := Tmp;
      for Head := 0 to SysObj.Heads - 1 do begin
        Tag := TagPublish('H' + IntToStr(Head + 1) + Tmp, TMethodTag);
//        Tag.Colour := Params.ReadColour(Params.Names[I], clRed);
        CTag.Signal[Head] := Tag;
      end;
      SignalTagList.AddObject(CTag.Name, CTag);
    end else if CompareText(Copy(Params.Names[I], 1, Length(InputPrefix)), InputPrefix) = 0 then begin
      Tmp := Params.Names[I];
      System.Delete(Tmp, 1, Length(InputPrefix));
      CTag := TCriterionTag.Create(Self);
      CTag.Name := Tmp;

      EnableTagList.AddObject(CTag.Name, CTag);
    end;
  end;

  EnableTagList.AddStrings(SignalTagList);
end;

procedure TSignatureEditor.Installed;
var I : Integer;
begin
  inherited Installed;
  for I := 0 to SysObj.Heads - 1 do
    Prog_IH[I] := TagEvent(EDMHeadPrefix + IntToStr(I+1) + 'Program', edgeChange, TMethodTag, nil);

  IncompatibleHoleProgram  := SysObj.Localized.GetString('$HOLE_SIG_INCOMPATIBLE');
  CompRequiresGather := SysObj.Localized.GetString('$COMP_REQUIRES_GATHER');

end;

function TSignatureEditor.GetSignalTag(Sender : TObject; aName : string) : TCriterionTag;
var I : Integer;
begin
  I := SignalTagList.IndexOf(aName);
  if I <> -1 then
    Result := TCriterionTag(SignalTagList.Objects[I])
  else
    Result := DefaultSignalTag;
end;

function TSignatureEditor.GetEnableTag(Sender : TObject; aName : string) : TCriterionTag;
var I : Integer;
begin
  I := EnableTagList.IndexOf(aName);
  if I <> -1 then
    Result := TCriterionTag(EnableTagList.Objects[I])
  else
    Result := DefaultEnableTag;
end;

function TSignatureEditor.Execute(Manager: TCompoundFile): Boolean;
var
  Form: TSignatureEditForm;
  I : Integer;
begin
  Form:= TSignatureEditForm.Create(Application);
  Form.SigFile.SamplePoints := SamplePoints;
  Result:= false;
  try
    {initialise Form.Gather only if live...}
    try
      Form.IsActiveFile:= CoreCNC.SysObj.FileManager.IsActiveFile(Manager);
      {note that Form.OLD is, of course, false by default..}
      if Form.IsActiveFile then begin
        for I := 0 to 3 do begin
          if Assigned(Prog_IH[I]) then
            Form.mhp[I] := Prog_IH[I].AsInteger
          else
            Form.mhp[I] := 0;

//          if (Form.mhp[I] < Low(TValidMHP)) or (Form.mhp[I] > High(TValidMHP)) then
//            Form.mhp[I]:= 0;
        end;

        Form.Gather:= Gather;
        Form.SamplePeriodCombo.Items.AddStrings(Gather.AllowableFrequencies);
      end else begin
        for I := 0 to 3 do
          Form.mhp[I]:= 0;
        Form.Gather:= nil
      end;
      Form.Load(Manager, True);
      SysObj.FaultInterface.ResetInterlock;
      Result:= Form.ShowModal = mrOK;
      Form.Save(Manager, Result)

    except
      on E: ESignatureFile do
        Form.DoSignatureFileException(E);
    end
  finally
    Form.Free
  end
end;

function TSignatureEditor.SectionName: String;
begin
  Result:= SectionSignatureData
end;

function TSignatureEditor.DefaultSignalTag: TCriterionTag;
begin
  Result := TCriterionTag(InstalledSigEditor.SignalTagList.Objects[0]);
end;

function TSignatureEditor.DefaultEnableTag: TCriterionTag;
begin
  Result := TCriterionTag(InstalledSigEditor.EnableTagList.Objects[0]);
end;


constructor TSignatureOnline.Create(aOwner: TComponent);
var
  I : Integer;
begin
  inherited Create(aOwner);
  TitlePanel := TPanel.Create(Self);
  TitlePanel.Parent := Self;
  TitlePanel.Align := alTop;
  TitlePanel.Height := 23;

  Form:= TSignatureEditForm.Create(Self);
  Form.OLD:= true;
  Form.ChartPanel.Parent:= Self;
  Form.ChartPanel.Align := alClient;
  SignatureFile := Form.SigFile;
  for I := 0 to CoreCNC.SysObj.Heads - 1 do
    Audit[I] := TStringList.Create;
  EventTagList := TList.Create;

  SetLength(SignatureClients, 20);
  SysObj.SignatureHost:= Self;
end;

procedure TSignatureOnline.InstallComponent(Params: TParamStrings);
var
  GatName: String;
begin
  inherited InstallComponent(Params);
  InstallKeySet(Params.ReadKeys);

  GatName:= Params.ReadString(GatherName, GatherDefault);
  Gather:= TAbstractGather(GetComponent(GatName));
  if Gather = nil then
    raise ECNCInstallFault.CreateFmt(CompRequiresGather, [Name, ClassName, GatName]);

  Form.Gather := Gather;

  SysObj.Comms.AddToIOSweep(IOSweep);
end;

procedure TSignatureOnline.IOSweep(aTag : TAbstractTag);
begin
  Gather.Poll;
  CheckSignature;
end;

procedure TSignatureOnline.Resize;
begin
  Form.ResizeCharts;
end;

procedure TSignatureOnline.Installed;
var I, Head : Integer;
  HParm: String;
  CTag, ITag : TCriterionTag;
begin
  inherited Installed;
  OPPName := TagEvent(nameNewActiveFile, edgeChange, TStringTag, NewActiveFile);
  TagEvent(nameDisplaySweep, edgeChange, TIntegerTag, SignatureScan);

  for I := 0 to CoreCNC.SysObj.Heads - 1 do begin
    HParm:= EDMHeadPrefix + IntToStr(I+1);
    Prog_IH[I]:= TagEvent(HParm + 'Program', edgeChange, TIntegerTag, ProgramChange);
    if Prog_IH[I] = nil then
      raise ECNCInstallFault.CreateFmt(IOTagRequiredText, [HParm + 'Program']);
  end;

  for I := 0 to CoreCNC.SysObj.Heads - 1 do begin
    Sel[I] := TagEvent(Name + 'Head' + IntToStr(I + 1), edgeFalling, TMethodTag, HeadSelect);
    Start[I]:= TagEvent(Format(GatherStartFormat, [I + 1]), edgeChange, TMethodTag, GatherStartStop);
  end;

  for I := 0 to InstalledSigEditor.EnableTagList.Count - 1 do begin
    ITag := TCriterionTag(InstalledSigEditor.EnableTagList.Objects[I]);
    if (ITag.Name <> FalseName) and (ITag.Name <> TrueName) then begin
      CTag := TCriterionTag.Create(Self);
      CTag.Name := ITag.Name;
      EventTagList.Add(CTag);
      for Head := 0 to CoreCNC.SysObj.Heads - 1 do
        CTag.Signal[Head] := TagEvent('H' + IntToStr(Head + 1) + CTag.Name, edgeChange, TMethodTag, AuditEvents);
    end;
  end;

  HeadSelectChange(0);

  TagEvent(Name + 'Reset', EdgeFalling, TMethodTag, ResetSignatureCallback);
  CompRequiresGather := SysObj.Localized.GetString('$COMP_REQUIRES_GATHER');
  IncompatibleHoleProgram  := SysObj.Localized.GetString('$HOLE_SIG_INCOMPATIBLE');

end;

procedure TSignatureOnline.ResetSignatureCallback(aTag : TAbstractTag);
var I : Integer;
begin
  for I := 0 to CoreCNC.SysObj.Heads - 1 do
    ResetSignature(I);

  for I := Low(TGatherHead) to High(TGatherHead) do
    Form.Charts[I].Invalidate;
end;

procedure TSignatureOnline.HeadSelectChange(aHead : Integer);
var I, J : Integer;
    Signature : TSignature;
begin
  Form.SelectedHeads := [aHead];
  Form.SelectedHoleProgram := Prog_IH[Form.SelectedHead].AsInteger;

  TitlePanel.Caption := Format(SysObj.Localized.GetString('$PROCESSPROG_D_HEAD_D'),
                     [Form.SelectedHoleProgram, Form.SelectedHead + 1]);
  for J:= Low(TGatherField) to High(TGatherField) do
    with Form.Charts[J] do begin
      BeginUpdate;
      ClearCurves
    end;


  Signature := Form.SigFile.SignatureProg[Form.SelectedHoleProgram];
  if Assigned(Signature) and (Form.SelectedHoleProgram <> 0) then begin
    for I := 0 to Signature.SampleCount - 1 do begin
      for J := Low(TGatherField) to High(TGatherField) do
        Form.Charts[J].AddCurve(Signature.SampleCurve[I]);
    end;
  end;

  for J:= Low(TGatherField) to High(TGatherField) do
    Form.Charts[J].EndUpdate;

  Form.HeadSelectRefresh;
end;

procedure TSignatureOnline.AuditEvents(aTag : TAbstractTag);
var CTag : TCriterionTag;
    I, Head : Integer;
begin
  if Form.Gather.GatherStatus = gsGathering then
    for I := 0 to EventTagList.Count - 1 do begin
      CTag := TCriterionTag(EventTagList[I]);
      for Head := 0 to CoreCNC.SysObj.Heads - 1 do begin
        if aTag = CTag.Signal[Head] then begin
          Audit[Head].Add( Format('%s = %d @ %.2gS',
               [aTag.Name, aTag.AsInteger, Form.Gather[Head].Length]));
          Exit;
        end;
      end;
    end;
end;

{ Ensures all criteria are prepared }
procedure TSignatureOnline.ResetSignature(aHead : Integer);
var F : TGatherField;
    I : Integer;
    ci : PCriterionInfo;
    List : TList;
begin
  for F := Low(TGatherField) to High(TGatherField) do begin
    List := Form.Criteria[F];
    for I := 0 to List.Count - 1 do begin
      ci := List[I];
      ci^.Criterion.Reset(aHead);
    end;
  end;
  Audit[aHead].Clear;
end;


const
  StartTime = 'Signature-Start ';

procedure TSignatureOnline.GatherStartStop(aTag : TAbstractTag);
var
  Head, I : Integer;
  AuditFile : TSignatureFile;
  Hp : Integer;
begin
  Head := 0;

  for I := 0 to CoreCNC.SysObj.Heads - 1 do begin
    if aTag = Start[I] then
      Head := I;
  end;

  if aTag.AsBoolean then begin
    ResetSignature(Head);
    Audit[Head].Add(StartTime + DateToStr(Now) + ' ' + TimeToStr(Now));
    LoggingStartTime[Head] := Now;
  end else begin
    AuditFile := TSignatureFile.Create;

    try
      try
        Hp := Form.Gather.HoleProgram[Head];
        if Assigned(Form.SigFile.SignatureProg[Hp]) then begin
          AuditFile.Clear;
          AuditFile.Add(Format(PeriodFormat, [Form.Gather.SamplePeriod]));
          Form.Gather[Head].Show := True;
          Form.SigFile.SignatureProg[Hp].Save(AuditFile, Hp, Form.Gather[Head]);
          AuditFile.AddStrings(Audit[Head]);
          Audit[Head].Clear;
          Audit[Head].AddStrings(AuditFile);
          WriteAudit(Head);
        end;
      finally
        AuditFile.Free;
      end;
    except
      On E: ESignatureFile do
        SysObj.FaultInterface.SetFault(Self, CoreFaults[cfSysWarning], [E.Message], nil);
    end;
  end;
end;


procedure TSignatureOnline.WriteAudit(aHead : Integer);
begin
end;

procedure TSignatureOnline.CNCResetRW(aTag : TAbstractTag);
var I : Integer;
begin
  inherited;
  if aTag.AsBoolean then
    for I := 0 to CoreCNC.SysObj.Heads - 1 do
      ResetSignature(I);
end;

procedure TSignatureOnline.SignatureScan(aTag : TAbstractTag);
begin
  if Assigned(Form) then
    with Form do
      if (Gather.GatherStatus = gsGathering) and
        SignatureCompatible and
        SignatureExists then begin

        ScreenUpdate;
//        CheckSignature;
      end;
end;

{ This is now called from the gather thread at linear buffer update }
procedure TSignatureOnline.CheckSignature;
var
  Curve: TCurve;
  Cs: TSignature;
  I, hp, Ch: Integer;
  Redraw : Integer;
  Head : TGatherHead;

begin
  Redraw := 0;
  for Head := 0 to CoreCNC.SysObj.Heads - 1 do begin
    if Head in Gather.HeadsGathering then begin
      Curve := Gather[Head];
      if Assigned(Curve) and Assigned(SignatureFile) then begin
        hp := Gather.HoleProgram[Head];
        if hp > 0 then begin
          cs := SignatureFile.SignatureProg[hp];
          if Assigned(cs) then begin
            for I := 0 to Cs.CriterionCount - 1 do begin
              Ch := Cs.Criterion[I].Applies(Curve);
              if (Ch <> -1) then begin
                with Cs.Criterion[I] do begin
                  if EnableTag.Signal[Head].AsBoolean and
                     not (HitSignalTag.Signal[Head].AsBoolean or
                          MissSignalTag.Signal[Head].AsBoolean) then begin
                    Meets(Curve, Head, Ch);

                    if not Cs.Criterion[I].Enabled then begin
                      Inc(Redraw);
                      Cs.Criterion[I].Enabled:= True;
                      FireGateEnable(Ch, Cs.Criterion[I].RunDomain)
                    end;
                  end;
                end;
              end;
            end;
          end;
        end;
      end;
    end;
  end;
  if Redraw > 0 then
    Form.ChartPanel.Invalidate;
end;

procedure TSignatureOnline.ProgramChange(aTag : TAbstractTag);
begin
  Form.SelectedHead := 0;
  HeadSelectChange(Form.SelectedHead);
  Form.SelectedHoleProgram:= Prog_IH[Form.SelectedHead].AsInteger;
  TestSignature
end;

procedure TSignatureOnline.HeadSelect(aTag : TAbstractTag);
var I, OldSel : Integer;
begin
  OldSel := Form.SelectedHead;
  for I := 0 to CoreCNC.SysObj.Heads - 1 do begin
    if Sel[I] = aTag then
      Form.SelectedHead := I;
  end;

  HeadSelectChange(Form.SelectedHead);

  if not Visible or (OldSel = Form.SelectedHead) then
    Activate(aTag);
end;


procedure TSignatureOnline.TestSignature;
begin
  if (Form.SelectedHoleProgram <> 0) and not Form.SignatureCompatible then
    SysObj.FaultInterface.SetFault(Self, CoreFaults[cfSysStopCycle],
      [Format(IncompatibleHoleProgram, [Form.SelectedHoleProgram])],
        IncompatibleReset)
end;

function TSignatureOnline.IncompatibleReset(FID: Integer): Boolean;
begin
  Result:= Form.SignatureCompatible
end;

procedure TSignatureOnline.NewActiveFile(aTag : TAbstractTag);
var ProgNo, I : Integer;
begin
//  EventLog('TSignatureOnline.NewActiveFile');
  with Form do
  try
    try
      CoreCNC.SysObj.FileManager.SectionRead(SectionSignatureData, SigFile);
    except
      On E : ECompoundFileError do Exit;
    end;
    SigFile.Load(True);
    Gather.SamplePeriod := SigFile.Period;
    ProgNo:= Prog_IH[SelectedHead].AsInteger;
    ProgNoUD.Position:= ProgNo;
    SigParamsRefresh;
    TestSignature;
    HeadSelectChange(0);
    for I := 0 to CoreCNC.SysObj.Heads - 1 do
      ResetSignature(I);
  except
    on E: ESignatureFile do
      Form.DoSignatureFileException(E);
  end;
end;

procedure TSignatureOnline.AddClient(Client: ISignatureClient);
var I: Integer;
begin
  for I:= 0 to Length(SignatureClients) - 1 do begin
    if not Assigned(SignatureClients[I]) then begin
      SignatureClients[I]:= Client;
      Exit;
    end;
  end;
end;

procedure TSignatureOnline.FireGateEnable(Field: Integer;
  Domain: TCriterionDomain);
var I: Integer;
begin
  for I:= 0 to Length(SignatureClients) - 1 do begin
    if Assigned(SignatureClients[I]) then begin
      SignatureClients[I].GateEnabled(Field, Domain);
    end;
  end;
end;

procedure TSignatureOnline.RemoveClient(Client: ISignatureClient);
var I: Integer;
begin
  for I:= 0 to Length(SignatureClients) - 1 do begin
    if SignatureClients[I] = Client then begin
      SignatureClients[I]:= nil;
      Exit;
    end;
  end;
end;

end.

