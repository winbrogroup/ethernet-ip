object CriteriaEditDlg: TCriteriaEditDlg
  Left = 440
  Top = 266
  BorderStyle = bsDialog
  Caption = '$CriterionEditor'
  ClientHeight = 401
  ClientWidth = 434
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = True
  Position = poScreenCenter
  OnCloseQuery = FormCloseQuery
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 360
    Width = 434
    Height = 41
    Align = alBottom
    TabOrder = 0
    object BitBtn1: TBitBtn
      Left = 88
      Top = 8
      Width = 75
      Height = 25
      Caption = '$OK'
      TabOrder = 0
      Kind = bkOK
    end
    object CancelBtn: TBitBtn
      Left = 184
      Top = 8
      Width = 75
      Height = 25
      Caption = '$Cancel'
      TabOrder = 1
      Kind = bkCancel
    end
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 179
    Width = 434
    Height = 181
    Align = alBottom
    Caption = '$&Positioning'
    TabOrder = 1
    object FieldLowLabel: TLabel
      Left = 42
      Top = 52
      Width = 48
      Height = 13
      Alignment = taRightJustify
      Caption = '$Field&Low'
      FocusControl = FieldLowEdit
    end
    object FieldHighLabel: TLabel
      Left = 40
      Top = 20
      Width = 50
      Height = 13
      Alignment = taRightJustify
      Caption = '$FieldHi&gh'
      FocusControl = FieldHighEdit
    end
    object TimeLowLabel: TLabel
      Left = 267
      Top = 52
      Width = 57
      Height = 13
      Alignment = taRightJustify
      Caption = '$TIMELOW'
      FocusControl = TimeLowEdit
    end
    object TimeHighLabel: TLabel
      Left = 265
      Top = 20
      Width = 59
      Height = 13
      Alignment = taRightJustify
      Caption = '$TIMEHIGH'
      FocusControl = TimeHighEdit
    end
    object FieldHighEdit: TEdit
      Left = 96
      Top = 20
      Width = 85
      Height = 21
      TabOrder = 0
      Text = '0'
      OnExit = IntFieldExit
    end
    object FieldLowEdit: TEdit
      Left = 96
      Top = 52
      Width = 85
      Height = 21
      TabOrder = 1
      Text = '0'
      OnExit = IntFieldExit
    end
    object TimeHighEdit: TEdit
      Tag = 1
      Left = 330
      Top = 20
      Width = 85
      Height = 21
      TabOrder = 2
      Text = '0'
      OnExit = RealFieldExit
    end
    object TimeLowEdit: TEdit
      Left = 330
      Top = 52
      Width = 85
      Height = 21
      TabOrder = 3
      Text = '0'
      OnExit = RealFieldExit
    end
    object TypeGroup: TRadioGroup
      Left = 16
      Top = 76
      Width = 153
      Height = 60
      Caption = '$&Orientation'
      Items.Strings = (
        '$Horizontal'
        '$Vertical')
      TabOrder = 4
      OnClick = TypeGroupClick
    end
    object RelativeGroup: TRadioGroup
      Left = 208
      Top = 76
      Width = 177
      Height = 101
      Caption = '$Relative'
      Items.Strings = (
        '$NON_RELATIVE'
        '$TIME'
        '$VALUE'
        '$BOTH')
      TabOrder = 5
    end
  end
  object EnableSignalPanel: TPanel
    Left = 0
    Top = 49
    Width = 434
    Height = 130
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 2
    object GroupBox2: TGroupBox
      Left = 0
      Top = 0
      Width = 201
      Height = 130
      Align = alLeft
      Caption = '$&Enable'
      TabOrder = 0
      object EnableComboBox: TComboBox
        Left = 48
        Top = 12
        Width = 145
        Height = 21
        ItemHeight = 0
        TabOrder = 0
        Text = 'EnableComboBox'
      end
      object EdgeGroup: TRadioGroup
        Left = 16
        Top = 40
        Width = 177
        Height = 80
        Caption = '$Edge'
        Items.Strings = (
          '$Transition'
          '$Rising'
          '$Falling')
        TabOrder = 1
      end
    end
    object GroupBox3: TGroupBox
      Left = 201
      Top = 0
      Width = 233
      Height = 130
      Align = alClient
      Caption = '$&Signal'
      TabOrder = 1
      object Label1: TLabel
        Left = 24
        Top = 16
        Width = 46
        Height = 13
        Caption = '$Through'
        FocusControl = HitSignalComboBox
      end
      object Label2: TLabel
        Left = 24
        Top = 46
        Width = 40
        Height = 13
        Caption = '$Around'
        FocusControl = MissSignalComboBox
      end
      object BiasLabel: TLabel
        Left = 24
        Top = 76
        Width = 26
        Height = 13
        Caption = '$&Bias'
        FocusControl = BiasPeriodEdit
      end
      object DelayLabel: TLabel
        Left = 24
        Top = 106
        Width = 33
        Height = 13
        Caption = '$&Delay'
        FocusControl = DelaySignalEdit
      end
      object HitSignalComboBox: TComboBox
        Left = 79
        Top = 12
        Width = 145
        Height = 21
        ItemHeight = 0
        TabOrder = 0
        Text = 'HitSignalComboBox'
      end
      object MissSignalComboBox: TComboBox
        Left = 79
        Top = 42
        Width = 145
        Height = 21
        ItemHeight = 0
        TabOrder = 1
        Text = 'MissSignalComboBox'
      end
      object BiasPeriodEdit: TEdit
        Left = 80
        Top = 72
        Width = 121
        Height = 21
        TabOrder = 2
        Text = 'BiasPeriodEdit'
      end
      object DelaySignalEdit: TEdit
        Left = 80
        Top = 102
        Width = 121
        Height = 21
        TabOrder = 3
        Text = 'DelaySignalEdit'
      end
    end
  end
  object GroupBox4: TGroupBox
    Left = 0
    Top = 0
    Width = 434
    Height = 49
    Align = alTop
    Caption = '$&Definition'
    TabOrder = 3
    object NameLabel: TLabel
      Left = 24
      Top = 21
      Width = 34
      Height = 13
      Alignment = taRightJustify
      Caption = '$Name'
      FocusControl = NameEdit
    end
    object FieldLabel: TLabel
      Left = 175
      Top = 21
      Width = 28
      Height = 13
      Alignment = taRightJustify
      Caption = '$Field'
      FocusControl = FieldList
    end
    object NameEdit: TEdit
      Left = 66
      Top = 16
      Width = 98
      Height = 21
      TabOrder = 0
      Text = 'NameEdit'
    end
    object FieldList: TComboBox
      Left = 212
      Top = 16
      Width = 118
      Height = 21
      Style = csDropDownList
      ItemHeight = 0
      TabOrder = 1
    end
  end
end
