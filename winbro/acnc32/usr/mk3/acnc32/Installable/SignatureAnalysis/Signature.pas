unit Signature;
{see SignatureEditor.pas for Synopsis of Signature Analysis}
interface
uses
  SysUtils, Windows, CNCTypes, CoreCNC, Curves, Gather, CompoundFiles,
  Classes, Tokenizer, Graphics, SyncObjs, CNC32, ISignature;

type
  PSampleData = ^TSampleData;
  TSampleData = array[TGatherField] of Integer; {data on one cut}

  TChartLimits = record
    TMin, TMax: Double;
    YMin, YMax: Integer;
  end;

  {RTV means 'RunTimeView' this is edited as part of the signature}
  TRTVLimits = array[TGatherField] of TChartLimits;

  TRTVSettings = record
    Limits: TRTVLimits;
    Fields: TGatherFields;
  end;

  TSignature = class;
  TSignatureFile = class;

  {note that TSample always stores data on four channels even if
   TSignature defines a channel count less than this. Excess
   data is ignored (usually null)}
  {Note that TSample may construct itself from TStrings. Abstracts cannot
  do this as they cannot store data}
  TSample = class(TCurve)
  private
    Owner: TSignature;
    Buffer: TList;  {of PSampleData}
    StoragePeriod : Double;
    FSamplePoints : Integer;
  protected
    function GetChannelCount: Integer; override; {get from owner}
    procedure SetChannelCount( Val: Integer); override; {do nothing}
    function GetChannelName( Ch: Integer): String; override; {get from owner}
    procedure SetChannelName( Ch: Integer; const Val: String); override; {do nothing}
    function GetSamplePeriod: Double; override; {get from owner}
    procedure SetSamplePeriod( Value: Double); override; {do nothing}
    function GetSampleData(i: Integer; Ch: Integer): Integer; override; {get from buffer}
    function GetSampleCount: Integer; override; {buffer.count}
  public
    constructor Create(aOwner: TSignature; aSamplePoints : Integer);
    destructor Destroy; override;
    procedure AddSamplePoint( const Data: TSampleData);
    procedure ClearSample;
    procedure Copy(Curve : TCurve);
    procedure Save(S: TSignatureFile; Index : Integer); virtual;
    procedure Load(S : TSignatureFile; Index : Integer); virtual;
    property SamplePoints : Integer read FSamplePoints write FSamplePoints;
  end;

  TCriterionType = (
       ctHorizontal,
       ctVertical
  );

  TCriterionEdge = (
       ceDataTransition,
       ceDataRising,
       ceDataFalling
  );

  TCriterionRelative = (
    crNonRelative,
    crTime,
    crValue,
    crBoth
  );

  TCriterionState = (
    csNotEnabled,
    csFirstMoment,
    csInProgress,
    csSecondMoment,
    csPassed
  );

  TCriterionDomain = class(TObject, ICriterionDomain)
  private
    RefCount: Integer;
  protected
    function QueryInterface(const IID: TGUID; out Obj): HResult; stdcall;
    function _AddRef : Integer; stdcall;
    function _Release: Integer; stdcall;
  public
    FTimeLow, FTimeHigh  : Double;
    FFieldLow, FFieldHigh : Integer;
    constructor Create;
    procedure Copy(Src: TCriterionDomain);
    function GetFieldLow: Integer; stdcall;
    procedure SetFieldLow(AFieldLow: Integer);  stdcall;
    function GetFieldHigh: Integer;  stdcall;
    procedure SetFieldHigh(AFieldHigh: Integer);  stdcall;
    function GetTimeLow: Double;  stdcall;
    procedure SetTimeLow(ATimeLow: Double);  stdcall;
    function GetTimeHigh: Double;  stdcall;
    procedure SetTimeHigh(ATimeHigh: Double);  stdcall;

    property FieldLow: Integer read GetFieldLow write SetFieldLow;
    property FieldHigh: Integer read GetFieldHigh write SetFieldHigh;
    property TimeLow: Double read GetTimeLow write SetTimeLow;
    property TimeHigh: Double read GetTimeHigh write SetTimeHigh;
  end;

  TCriterionTag = class(TObject)
    FOwner : TCNC;
  public
    Name : string;
    Signal : array [0..3] of TAbstractTag;
    constructor Create(aOwner : TCNC);
    property Owner : TCNC read FOwner;
  end;

  TSignatureCriterion = class(TInterfacedObject)
  public
    FieldName: String;
    HitSignalTag : TCriterionTag;
    MissSignalTag : TCriterionTag;
    EnableTag : TCriterionTag;
    Name: String;
    ValidChannel: Integer;
    EditDomain, DisplayDomain, RunDomain : TCriterionDomain;
    CriterionType: TCriterionType;
    Edge  : TCriterionEdge;
    Relative : TCriterionRelative;
    BiasPeriod : Double;
    DelaySignal : Double;
    Signature : TSignature;
    State : TCriterionState;
    Enabled: Boolean; // Used for rising edge detect of enable signal


    CheckedSamples : Integer;
    Sign : Boolean;

    constructor Create( const aFieldName, aName: String;
                        aSignature : TSignature;
                        aHSigTag: TCriterionTag;
                        aMSigTag: TCriterionTag;
                        aEnaTag: TCriterionTag;
                        aCriterionType: TCriterionType;
                        aFieldLow, aFieldHigh: Integer;
                        aTimeLow, aTimeHigh: Double);
    destructor Destroy; override;
    function Applies( Curve: TCurve): Integer; virtual;
             {Return channel index to which criterion applies.
              return -1 if doesn't apply}
    function Meets(Curve: TCurve; Head : TGatherHead; Channel: Integer): Boolean; virtual;
             {the Curve applies, and meets the criterion}
    constructor Load(S: TSignatureFile; aSignature : TSignature); virtual;
    procedure Save(S: TSignatureFile; RunTime : Boolean); virtual;
    procedure GetVisualBoundingPoints( var T1, T2: Double; var F1, F2: Integer; RunTime : Boolean); virtual;
    function Clone(const aName: String): TSignatureCriterion; virtual;
    procedure UpdateDisplayDomain;
    procedure DisplayMoveTo(FL, FH: Integer; TL, TH: Double);
    procedure Reset(aHead : Integer);
//    function InRange( Curve: TCurve): Boolean; virtual;
             {the criterion is placed within the bounds of curve}
  end;

  {this is the object against which plots must be tested - loaded from disk}
  TSignature = class(TObject)
  private
  protected
    SampleList: TList; {of TSample}
    CriteriaList: TList;
    Version: String;
    FFieldName: array[TGatherField] of String;
    FFieldCount: Integer;
    FileID: Integer;
    FSamplePeriod: Double;
    FSamplePoints : Integer;
    function GetSampleCount: Integer;
    function GetSampleCurve(i: Integer): TCurve;
    function GetCriterionCount: Integer;
    function GetCriterion(i: Integer): TSignatureCriterion;
    function GetFieldName( i: Integer): String;
  public
    RTV: TRTVSettings;
    constructor Create;
    constructor CreateToMatchCurve(ID: Integer; Curve: TCurve; aSamplePoints : Integer);
    destructor Destroy; override;
    procedure BindCriteria;  {&&& use sparingly to aid  efficiency
                              Generally TCriteria.Applies is safer}

    //These functions are only used by the editor
    function SignalUsage(cTag : TCriterionTag) : Integer;
    function SignalUsedRelative(cTag : TCriterionTag) : Boolean;
    function FindEnableCriterion(cTag : TCriterionTag) : TSignatureCriterion;
//    function IsCircularEnable(aEnable : TIOTag; aCriterion : TSignatureCriterion) : Boolean;
    function IsCircularSignal(aSignal : TCriterionTag; aCriterion : TSignatureCriterion) : Boolean;
    //End of functions used by editor

    procedure ClearSamples;
    procedure DeleteSample(Curve: TCurve);
    function AddSample(Curve: TCurve): Integer;

    procedure ClearCriteria;
    procedure DeleteCriterion(aCriterion: TSignatureCriterion);
    function  AddCriterion(aCriterion: TSignatureCriterion): Integer;
    function CompatibleWith(Curve: TCurve): Boolean;
    procedure Save(S : TSignatureFile; aProgNo: Integer; ActiveCurve : TCurve); {adds to strings}
    function Load(S : TSignatureFile; WithSamples: Boolean): Boolean; {returns prog no}

    property FieldCount: Integer read FFieldCount;
    property FieldName[i: Integer]: String read GetFieldName;

    {pass in samples from gather}
{   property ID: Integer read FID write FID;}
    property SampleCount: Integer read GetSampleCount;
    property SampleCurve[i: Integer]: TCurve read GetSampleCurve;

    property CriterionCount: Integer read GetCriterionCount;
    property Criterion[i: Integer]: TSignatureCriterion read GetCriterion;
    property ID: Integer read FileID;   {Microhole program number or -1 if no data}
    property SamplePeriod: Double read FSamplePeriod write FSamplePeriod;
    property SamplePoints : Integer read FSamplePoints write FSamplePoints;
  end;

  TGetTagByName = function (Sender : TObject; aName : string) : TCriterionTag of Object;

  TSignatureFile = class(TStringTokenizer)
  private
    FPeriod : Double;
    SignatureList : TList;
    FSamplePoints : Integer; // Number of points used for storing samples
    FOnGetSignalTag : TGetTagByName;
    FOnGetEnableTag : TGetTagByName;
    function GetSignatureI(Index: Integer): TSignature;
    function GetSignatureProg(HoleProg: Integer): TSignature;
//    procedure SetSignature(HoleProg: Integer; Value: TSignature);
    function GetSignatureCount : Integer;
  public
    constructor Create;
    destructor Destroy; override;
    function Read : string; override;
    procedure Load(WithSamples: Boolean);
    procedure Save;
    procedure SignatureAdd(aSignature : TSignature);
    procedure SignatureDelete(aSignature : TSignature);
    property SignatureI[Index: Integer]: TSignature read GetSignatureI;
    property SignatureProg[HoleProg : Integer] : TSignature read GetSignatureProg; // default;
//      write SetSignature; default;
    property SignatureCount : Integer read GetSignatureCount;
    procedure Clean;
    function ReadInteger : Integer;
    function ReadDouble : Double;
    function ReadIndex : Integer;
    procedure ReadToken(aToken : string);

    property SamplePoints : Integer read FSamplePoints write FSamplePoints;
    property Period : Double read FPeriod write FPeriod;
    property OnGetSignalTag : TGetTagByName read FOnGetSignalTag write FOnGetSignalTag;
    property OnGetEnableTag : TGetTagByName read FOnGetEnableTag write FOnGetEnableTag;
  end;

  TFalseIOTag = class(TIntegerTag)
  protected
    procedure SetAsBoolean(aValue : Boolean); override;
    procedure SetAsInteger(aValue : Integer); override;
  public
    procedure ReadFromBuf(var Buf; aLength : Integer); override;
  end;

  TTrueIOTag = class(TFalseIOTag)
  public
    constructor Create(aOwner : TTagOwner; aName : string); override;
  end;

  ESignatureFile = class(Exception);

implementation

{Signature data format:
(format is tabular to enable exchange with spreadsheet)

Typical format. ;Comments do not generally appear in file -
they are there to explain the syntax

Signature  1
;the number must match microhole prog no

Version=1.00
FixedWidth=0
;if 0 then data is delimited. Applies to SampleProfile data only)

Delimiter=,
;must be present, but ignored if FixedWidth <> 0)

FieldNames=Position,Voltage,Velocity
;comma delimited, regardless of setting above. Not case sensitive

Period=0.020
;cannot add samples to this signature unless Gather.SamplePeriod = Period

SampleProfile 0
;number is arbitrary. Only provided to assist in navigation of text file

;Position, Voltage, Velocity
;TSignature.Save adds this line but it is ignored on load, as it is a comment

97,    32,   44
100,   30, 41
101,    30, 43
102,    30, 42
106,    33,   41
EndSampleProfile



SampleProfile 1
97,    32,   44
100, 30, 41
101,  30, 43
102,    30, 42
106,    33,   41
EndSampleProfile

;and so on

;similarly, criterion numbers are arbitrary

Criterion 0
Class=TVerticalWindow
Field=Position
FieldLow=98
FieldHigh=100
TimeVal=10
EndCriterion

Criterion 1
Class=TVerticalWindow
Field=Position
FieldLow=100
FieldHigh=101
TimeVal=15
EndCriterion


RELATIVE CRITERION

When enabled the

T
0

   *-------*  Edit Time of Relative

     *-----------* Edit time of enabling gate

        *------------------* Display Time of Relative Gate



   *   EditTime of Relative Gate
   |
   |
   |
   |
   *


     *-----------* Edit time of enabling gate


        *-----------*
        |           | Display Time of Relative Gate
        |           |
        |           |
        |           |
        *-----------*


   of course Relative by value when crossing between fields cannot be displayed
   but the display value could be distended to include the whole RTV.
}

resourcestring
    SigInvalidIntegerValue = 'SigFile Error: Invalid Integer [%s] on line [%s]';
    SigInvalidDataValue = 'SigFile Error: Invalid Data [%s] in line [%s]';
    SignatureFileNotFound = 'SigFile Error: Sig data not found';
    SigExpectedToken = 'SigFile Error : Expected [%s] in Line [%s]';
    SigErrorUnexpectedToken = 'SigFile Error : Unexpected Token %s on Line %d';
    SigErrorTooManyGatherFields = 'SigFile Error : Too Many Gather Fields [%s]';
    InvalidFileID = 'SigFile Error: Invalid file ID %d';
    IncompatibleGather = 'SigFile %d Error: Attempt to add incompatible data';
    SigErrorInvalidVersion = 'SigFile Error: Invalid Version [%s]';
    SigErrorProf = 'SigFile Error: Professional [%s]';
    SigErrorSignalNotInstalled = 'SigFile Error : Signal not installed [%s]';


const
  SigHeader = 'Signature';
  SampleTrailer = 'EndSampleProfile';

  CriterionHeader = 'Criterion';

  SignatureSeperator = '[]=.;#';
  SignatureNumericSeperator = '[]=;#';

  SignatureFileVersion = 1.00;
  PeriodName = 'Period';
  PeriodDefault = 0.020;

  {in practice, read in From INI file}
  DefaultRTV: TRTVSettings =
    (Limits:((TMin: 0; TMax: 100; YMin: 0; YMax: 10000),
            (TMin: 0; TMax: 100; YMin: 0; YMax: 10000),
            (TMin: 0; TMax: 100; YMin: 0; YMax: 10000),
            (TMin: 0; TMax: 100; YMin: 0; YMax: 10000));
    );


type
  TSignatureToken = (
    stSample,
    stCriterion,
    stRTV,
    stFieldNames,
    stVersion,
    stEndSignature
  );

  TRTVType = (
    rtvYMin,
    rtvYMax,
    rtvTMin,
    rtvTMax,
    rtvField,
    rtvGrade
  );


const
  RTVDataNames : array [TRTVType] of PChar = (
    'YMin',
    'YMax',
    'TMin',
    'TMax',
    'Fields',
    'Grades'
  );

  RTVIndexed = [rtvYMin..rtvTMax];

  SignatureNames : array [TSignatureToken] of PChar = (
    'SampleProfile',
    CriterionHeader,
    'RTV',
    'FieldNames',
    'Version',
    'EndSignature'
  );

function TSample.GetSamplePeriod: Double;
begin
  Result:= Owner.SamplePeriod
end;

procedure TSample.SetSamplePeriod( Value: Double);
begin  {not appropriate}
end;

constructor TSample.Create(aOwner: TSignature; aSamplePoints : Integer);
begin
  inherited Create;
  Owner:= aOwner;
  Buffer:= TList.Create;
  FSamplePoints := aSamplePoints;
end;

destructor TSample.Destroy;
begin
  ClearSample;
  Buffer.Free;
  inherited Destroy
end;

function TSample.GetSampleData(i: Integer; Ch: Integer): Integer;
var Tmp : Integer;
begin
  if ValidChannel(Ch) and (i >= 0) and (i < SampleCount) then begin
      Tmp := Round(I * (Owner.SamplePeriod / StoragePeriod));
      if Tmp < Buffer.Count then
        Result:= PSampleData(Buffer[Tmp])[Ch]
      else
        Result := 0;
  end else begin
    Result:= 0
  end
end;

procedure TSample.AddSamplePoint(const Data: TSampleData);
var
  P: PSampleData;
begin
  New(P);
  p^:= Data;
  Buffer.Add(P)
end;


procedure TSample.ClearSample;
var
  i: Integer;
begin
  for i:= 0 to Buffer.Count - 1 do
    Dispose(PSampleData(Buffer[i]));
  Buffer.Clear;
end;

procedure TSample.Save(S: TSignatureFile; Index : Integer);
var Sam, Channel : Integer;
    T : string;
begin
  S.Add(Format('%s %d %.6f %d', [SignatureNames[stSample], Index, StoragePeriod, Ord(Show)]));
  for Sam := 0 to Buffer.Count - 1 do begin
    T := IntToStr(PSampleData(Buffer[Sam])[0]);
    for Channel := 1 to Owner.FFieldCount - 1 do begin
      T := T + ';' + IntToStr(PSampleData(Buffer[Sam])[Channel]);
    end;
    S.Add(T);
  end;
  S.Add(SampleTrailer);
end;

procedure TSample.Load(S : TSignatureFile; Index : Integer);
var I : Integer;
    Data : TSampleData;
begin
  S.ReadToken(SignatureNames[stSample]);
  S.ReadToken(IntToStr(Index));
  StoragePeriod := S.ReadDouble;
  Show := S.ReadInteger <> 0;
  while S.Read <> SampleTrailer do begin
    S.Push;
    Data[0] := S.ReadInteger;
    for I := 1 to Owner.FieldCount - 1 do begin
      S.ReadToken(';');
      Data[I] := S.ReadInteger;
    end;
    AddSamplePoint(Data);
  end;
end;

procedure TSample.Copy(Curve : TCurve);
var I, J, Index : Integer;
    Data : TSampleData;
    CSampleP : Double;
begin
  { This algorithm will not work for slower samples than storage so
    reduce the storage to sample period }
  StoragePeriod := Curve.Length / SamplePoints;
  if Curve.SamplePeriod > StoragePeriod then
    StoragePeriod := SamplePeriod;

  Index := 0;

  CSampleP := Curve.SamplePeriod;

  for I:= 0 to Curve.SampleCount - 1 do begin
    if (CSampleP * I) >= (StoragePeriod * Index) then begin
      Inc(Index);
      for j:= 0 to Owner.FieldCount - 1 do begin
        Data[J]:= Curve.SampleData[i, j];
      end;
      AddSamplePoint(Data)
    end;
  end;
end;

function TSample.GetSampleCount: Integer;
begin
  Result:= Round((Buffer.Count - 1) * (StoragePeriod / Owner.SamplePeriod));
end;

function TSample.GetChannelCount: Integer;
begin
  Result:= Owner.FFieldCount
end;

procedure TSample.SetChannelCount( Val: Integer);
begin {not appropriate}
end;

function TSample.GetChannelName( Ch: Integer): String;
begin
  if ValidChannel(Ch) then
    Result:= Owner.FFieldName[Ch]
  else
    Result:= ''
end;

procedure TSample.SetChannelName( Ch: Integer; const Val: String);
begin  {not appropriate}
end;

function TSignature.GetFieldName( i: Integer): String;
begin
  if (i >= Low(TGatherField)) and (i <= High(TGatherField)) then
    Result:= FFieldName[i]
  else
    Result:= ''
end;


constructor TSignature.Create;
begin
  inherited Create;
  CriteriaList:= TList.Create;
  SampleList:= TList.Create;
  RTV:= DefaultRTV
end;

destructor TSignature.Destroy;
begin
  ClearSamples;
  SampleList.Free;
  ClearCriteria;
  CriteriaList.Free;
  inherited Destroy
end;

function TSignature.SignalUsage(cTag : TCriterionTag) : Integer;
var I : Integer;
    SC : TSignatureCriterion;
begin
  Result := 0;

  for I := 0 to CriteriaList.Count - 1 do begin
    SC := TSignatureCriterion(CriteriaList[I]);
    if (SC.HitSignalTag = cTag) or (SC.MissSignalTag = cTag) then
      Inc(Result);
  end;
end;

function TSignature.SignalUsedRelative(cTag : TCriterionTag) : Boolean;
var I : Integer;
    SC : TSignatureCriterion;
begin
  Result := False;
  for I := 0 to CriteriaList.Count - 1 do begin
    SC := TSignatureCriterion(CriteriaList[I]);
    if (SC.EnableTag = cTag) and (SC.Relative <> crNonrelative) then
      Result := True;
  end;
end;

function TSignature.FindEnableCriterion(CTag : TCriterionTag) : TSignatureCriterion;
var I : Integer;
begin
  Result := nil;
  if (CTag.Signal[0] is TFalseIOTag) or (CTag.Signal[0] is TTrueIOTag) then
    Exit;

  for I := 0 to CriteriaList.Count - 1 do begin
    Result := TSignatureCriterion(CriteriaList[I]);
    if (Result.HitSignalTag = CTag) or (Result.MissSignalTag = CTag) then
      Exit;
  end;

  Result := nil;
end;

(*function TSignature.IsCircularEnable(aEnable : TIOTag; aCriterion : TSignatureCriterion) : Boolean;
var SC : TSignatureCriterion;
begin
  { Check if this enable would cause a circular reference within this criterions' peers}
  Result := True;

  SC := aCriterion;

  while SC <> nil do begin
    if (SC.MissSignalTag = aEnable) or
       (SC.HitSignalTag = aEnable) then
         Exit;
    SC := FindEnableCriterion(aEnable);
  end;

  Result := False;
end; *)

function TSignature.IsCircularSignal(aSignal : TCriterionTag; aCriterion : TSignatureCriterion) : Boolean;
var SC : TSignatureCriterion;
begin
  Result := True;

  SC := aCriterion;

  while SC <> nil do begin
    if (aSignal = SC.EnableTag) then
      Exit;

    SC := FindEnableCriterion(SC.EnableTag);
  end;

  Result := False;
end;


procedure TSignature.BindCriteria;
var
  i, j: Integer;
begin
  for i:= 0 to CriterionCount - 1 do
  with Criterion[i] do
  begin
    ValidChannel:= -1;  {invalidate}
    for j:= 0 to FFieldCount - 1 do
    begin
      if CompareText(FieldName, FFieldName[j]) = 0 then
      begin  {bound}
        ValidChannel:= j;
        break
      end
    end
  end
end;

function TSignature.Load(S : TSignatureFile;  WithSamples: Boolean): Boolean;
  function ReadSignatureToken : TSignatureToken;
  var Token : string;
  begin
   Token := S.Read;
    for Result := Low(TSignatureToken) to High(TSignatureToken) do begin
      if CompareText(Token, SignatureNames[Result]) = 0 then
        Exit;
    end;
    raise ESignatureFile.CreateFmt(SigErrorUnexpectedToken, [Token, S.Line]);
  end;

  procedure ReadSample;
  var NewSample, I : Integer;
  begin
    if WithSamples then begin
      NewSample:= SampleList.Add(TSample.Create(Self, 1));
      S.Push;
      TSample(SampleList[NewSample]).Load(S, NewSample);
    end else begin
      for I := S.Line to S.Count - 1 do begin
        if S.Strings[I] = SampleTrailer then begin
          S.Line := I + 1;
          Exit;
        end;
      end;
    end;
  end;

  procedure ReadRTV;
    function RTVDataType : TRTVType;
    var Token : string;
    begin
      Token := S.Read;
      for Result := Low(TRTVType) to High(TRTVType) do begin
        if CompareText(RTVDataNames[Result], Token) = 0 then
          Exit;
      end;

      raise ESignatureFile.CreateFmt(SigErrorUnexpectedToken, [Token, S.Line]);
    end;
  var rtvt : TRTVType;
      I : Integer;
  begin
    S.ReadToken('.');
    rtvt := RTVDataType;
    I := 0;
    if rtvt in RTVIndexed then
      I := S.ReadIndex mod (High(TRTVLimits) + 1);

    S.ReadToken('=');
    case rtvt of
      rtvYMin : RTV.Limits[I].YMin := S.ReadInteger;
      rtvYMax : RTV.Limits[I].YMax := S.ReadInteger;
      rtvTMin : RTV.Limits[I].TMin := S.ReadDouble;
      rtvTMax : RTV.Limits[I].TMax := S.ReadDouble;
      rtvField: RTV.Fields := TGatherFields(Byte(S.ReadInteger));
      rtvGrade: S.ReadInteger;
    end;
  end;

  procedure ReadCriterion;
  begin
    S.Push;
    CriteriaList.Add(TSignatureCriterion.Load(S, Self))
  end;

  procedure ReadFieldNames;
  begin
    S.ReadToken('=');
    FFieldName[0] := S.Read;
    FFieldcount := 1;
    while S.Read = ';' do begin
      if FFieldCount > High(TGatherField) then
        raise ESignatureFile.CreateFmt(SigErrorTooManyGatherFields, [S.CurrentLine]);
      FFieldName[FFieldCount] := S.Read;
      Inc(FFieldCount);
    end;
    S.Push;
  end;

  procedure ReadVersion;
  begin
    S.ReadToken('=');
    if S.ReadDouble <> SignatureFileVersion then
      raise ESignatureFile.CreateFmt(SigErrorInvalidVersion, [S.CurrentLine]);
  end;

begin
  Result := False;
  if CompareText(S.Read, SigHeader) <> 0 then
    Exit;
  Self.FileID := S.ReadInteger;
  Result := True;
//  SignalList.Clear;
  while True do begin
    case ReadSignatureToken of
      stSample    : ReadSample;
      stRTV       : ReadRTV;
      stCriterion : ReadCriterion;
      stFieldNames: ReadFieldNames;
      stVersion   : ReadVersion;
      stEndSignature : Exit;
    end;
  end;
end;

procedure TSignature.Save(S : TSignatureFile; aProgNo: Integer; ActiveCurve : TCurve);
  procedure WriteVersion;
  begin
    S.Add(Format('%s = %4.2f' + CarRet, [SignatureNames[stVersion], 1.0]));
  end;

  procedure WriteFieldNames;
  var F : Integer;
      T : string;
  begin
    T := SignatureNames[stFieldNames] + '=';
    T := T + FFieldName[0];
    for F := 1 to FFieldCount - 1 do begin
       T := T + ';' + FFieldName[F];
    end;
    S.Add(T);
  end;

  procedure WriteRTV;
  var G : Integer;
  begin
    S.Add(SignatureNames[stRTV] + '.' + RTVDataNames[rtvField] + '=' + IntToStr(Byte(RTV.Fields)));
    for G := Low(TGatherField) to High(TGatherField) do begin
      S.Add(Format('%s.%s[%d]=%d', [
                  SignatureNames[stRTV],
                  RTVDataNames[rtvYMin],
                  G,
                  RTV.Limits[G].YMin ]));
      S.Add(Format('%s.%s[%d]=%d', [
                  SignatureNames[stRTV],
                  RTVDataNames[rtvYMax],
                  G,
                  RTV.Limits[G].YMax ]));
      S.Add(Format('%s.%s[%d]=%f', [
                  SignatureNames[stRTV],
                  RTVDataNames[rtvTMin],
                  G,
                  RTV.Limits[G].TMin ]));
      S.Add(Format('%s.%s[%d]=%f', [
                  SignatureNames[stRTV],
                  RTVDataNames[rtvTMax],
                  G,
                  RTV.Limits[G].TMax ]));
    end;
  end;

  procedure WriteSample(I : Integer);
  begin
    TSample(SampleList[I]).Save(S, I);
    S.Add('');
  end;

var I : Integer;
    Sample : TSample;
    Crit : TCriticalSection;
begin
  S.Add(SigHeader + ' ' + IntToStr(FileID) + CarRet);

  WriteVersion;
  WriteFieldNames;
  WriteRTV;
  S.Add('');

  if not Assigned(ActiveCurve) then begin
    for I := 0 to SampleList.Count - 1 do
      WriteSample(I)
  end else begin
    if ActiveCurve.SampleCount > 0 then begin
      Sample := TSample.Create(Self, ActiveCurve.SampleCount);
      Crit := TCriticalSection.Create;
      try
        Sample.StoragePeriod := ActiveCurve.SamplePeriod;
        try
          Crit.Enter;
          Sample.Copy(ActiveCurve);
        finally
          Crit.Leave;
        end;
        Sample.Show := True;
        Sample.Save(S, 0);
      finally
        Sample.Free;
        Crit.Free;
      end;
    end;
  end;

  for I := 0 to CriteriaList.Count - 1 do begin
    TSignatureCriterion(CriteriaList[i]).Save(S, Assigned(ActiveCurve));
    S.Add('');
  end;

  S.Add(SignatureNames[stEndSignature]);
  S.Add('');
end;


procedure TSignature.ClearSamples;
var
  i: Integer;
begin
  for i:= 0 to SampleList.Count - 1 do
    TObject(SampleList[i]).Free;
  SampleList.Clear
end;

function TSignature.CompatibleWith(Curve: TCurve): Boolean;
const
  PeriodMargin = 1e-6;
var
  i: Integer;
begin
  if SampleList.Count = 0 then begin
    Result:= true
  end else  begin
    Result:= {(abs(SamplePeriod - Curve.SamplePeriod) < PeriodMargin) and }
             (FieldCount = Curve.ChannelCount);
    if Result then
    for i:= 0 to FieldCount - 1 do
    if CompareText(FieldName[i], Curve.ChannelName[i]) <> 0 then
    begin
      Result:= false;
      break
    end
  end
end;

function TSignature.AddSample( Curve: TCurve): Integer;
var
  NewSample: TSample;
  Data: TSampleData;
  I : Integer;
begin
  {if there are no existing samples then setup
  Curve info (period, channelcount and channelname)
  to match the curve being added. Otherwise test
  to make sure that the new curve is compatible with
  existing curves}
  if Curve.ChannelCount > MaxGatherFields then
    raise ESignatureFile.CreateFmt(IncompatibleGather, [FileID]);

  if (SampleList.Count = 0) then begin
    FSamplePeriod:= Curve.SamplePeriod;
    FFieldCount:= Curve.ChannelCount;
    for i:= 0 to Curve.ChannelCount - 1 do
      FFieldName[i]:= Curve.ChannelName[i]
  end;
  NewSample:= TSample.Create(Self, SamplePoints);
  FillChar(Data, SizeOf(Data), 0);
  if not Curve.Compatible(NewSample) then begin
    NewSample.Free;
    raise ESignatureFile.CreateFmt(IncompatibleGather, [FileID])
  end;

  NewSample.Copy(Curve);
  Result := SampleList.Add(NewSample)
end;


procedure TSignature.ClearCriteria;
begin
  while CriteriaList.Count > 0 do
  begin
    TObject(CriteriaList[0]).Free;
    CriteriaList.Delete(0)
  end
end;

procedure TSignature.DeleteCriterion(aCriterion: TSignatureCriterion);
var
  i: Integer;
begin
  i:= CriteriaList.IndexOf(aCriterion);
  if i <> -1 then
    CriteriaList.Delete(i);
  aCriterion.Free
end;


procedure TSignature.DeleteSample(Curve: TCurve);
var
  i: Integer;
begin
  i:= SampleList.IndexOf(Curve);
  if i <> -1 then
    SampleList.Delete(i);
  Curve.Free
end;


function TSignature.AddCriterion(aCriterion: TSignatureCriterion): Integer;
begin
  Result:= CriteriaList.Add(aCriterion)
end;

function TSignature.GetSampleCount: Integer;
begin
  Result:= SampleList.Count
end;

function TSignature.GetSampleCurve(i: Integer): TCurve;
begin
  {raise EListError if i is out of range...&&&}
  Result:= TCurve(SampleList[i])
end;

function TSignature.GetCriterionCount: Integer;
begin
  Result:= CriteriaList.Count
end;

function TSignature.GetCriterion(i: Integer): TSignatureCriterion;
begin
  {raise EListError if i is out of range...&&&}
  Result:= TSignatureCriterion(CriteriaList[i])
end;

constructor TSignature.CreateToMatchCurve(ID: Integer;  Curve: TCurve; aSamplePoints : Integer);
var
  i: Integer;
begin
  Create;
  FileID:= ID;
  FFieldCount:= Curve.ChannelCount;
  FSamplePeriod:= Curve.SamplePeriod;
  SamplePoints := aSamplePoints;
  with RTV do begin
    for i:= 0 to Curve.ChannelCount - 1 do begin
      Limits[i].TMin:= 0;
      Limits[i].TMax:= Curve.Length;
      Limits[i].YMin:= Curve.MinData[i];
      Limits[i].YMax:= Curve.MaxData[i];
      FFieldName[i]:= Curve.ChannelName[i]
    end;
  end;
end;


function TSignatureCriterion.Applies(Curve: TCurve): Integer;
var
  i: Integer;
begin
  Result:= -1;
  for i:= 0 to Curve.ChannelCount - 1 do
    if CompareText(FieldName, Curve.ChannelName[i]) = 0 then begin
      Result:= i;
      break
    end
end;

{function TSignatureCriterion.InRange(Curve: TCurve): Boolean;
var
  t1, t2: Double;
  f1, f2: Integer;
begin
  GetBoundingPoints(t1, t2, f1, f2);
  Result:=
    (t1 >= 0) and (t1 <= Curve.Length) and
    (t2 >= 0) and (t2 <= Curve.Length)
end; }

constructor TSignatureCriterion.Create( const aFieldName, aName: String;
                        aSignature : TSignature;
                        aHSigTag: TCriterionTag;
                        aMSigTag: TCriterionTag;
                        aEnaTag: TCriterionTag;
                        aCriterionType: TCriterionType;
                        aFieldLow, aFieldHigh: Integer;
                        aTimeLow, aTimeHigh: Double);
begin
//  inherited Create(aFieldName, aName, aGrade);
  inherited Create;
  FieldName:= aFieldName;
  Name:= aName;
  Signature := aSignature;
  HitSignalTag := aHSigTag;
  MissSignalTag := aMSigTag;
  EnableTag := aEnaTag;
  ValidChannel:= -1;
  CriterionType:= aCriterionType;

  EditDomain := TCriterionDomain.Create;

  EditDomain.FieldLow:= aFieldLow;
  EditDomain.FieldHigh:= aFieldHigh;
  EditDomain.TimeLow:= aTimeLow;
  EditDomain.TimeHigh:= aTimeHigh;

  RunDomain := TCriterionDomain.Create;
  RunDomain.Copy(EditDomain);

  DisplayDomain:= TCriterionDomain.Create;
end;

destructor TSignatureCriterion.Destroy;
begin
  EditDomain.Free;
  DisplayDomain.Free;
  RunDomain.Free;
end;


  {  HighF -|          X
            |         /
            |        /
  FieldLow -|     |-/--|
            |      /
    LowF   -|     x
            |___________________
                  |    |
             TimeLow  TimeHigh
  }
  {FieldHigh -|       -  x
              |       | /
              |       |/
              |       /
              |      /|
   FieldLow  -|     x -
              |___________________
                    | |  |
                 LowT |  HighT
                      |
                     TimeVal}

{ NOTE : USAGE
  Meets should only be called when enabled and neither output has fired

  BUGS
  1. This attempt is non retrospective.
  It does not attempt to timestamp when an event occurs, rather it just assumes
  it occurs meets is called.

  2. Could be written to be retrospective and the signature
  file is completely accurate, but events to and from the outside world would
  still have the actual delays imposed

   }
function TSignatureCriterion.Meets(Curve: TCurve; Head : TGatherHead; Channel: Integer) : Boolean;
  procedure HorizontalMeets;
  var
    ThisSample : Integer;
    I : Integer;
    EntryIndex : Integer;
  begin
    Result := False;
    with RunDomain do begin
      if State = csFirstMoment then begin
        CheckedSamples := Curve.IndexAtTime[TimeLow];
        EntryIndex := CheckedSamples;
        case Edge of
          ceDataRising : begin
            if Curve.SampleData[EntryIndex, Channel] > FieldLow then begin
              MissSignalTag.Signal[Head].AsInteger := 1;
              Result:= True;
            end;
          end;
          ceDataFalling: begin
            if Curve.SampleData[EntryIndex, Channel] < FieldLow then begin
              MissSignalTag.Signal[Head].AsInteger := 1;
              Result:= True;
            end;
          end;
        end;
        Sign := Curve.SampleData[EntryIndex - 1, Channel] > FieldLow;
        State := csInProgress;
      end;

      if State = csInProgress then begin
        if Curve.Length < TimeHigh then begin
          ThisSample := Curve.IndexAtTime[Curve.Length]
        end else begin
          ThisSample := Curve.IndexAtTime[TimeHigh];
          State := csSecondMoment
        end;

        for I := CheckedSamples to ThisSample - 1 do begin
          if (Curve.SampleData[I, Channel] > FieldLow) <> Sign then begin
            HitSignalTag.Signal[Head].AsInteger := 1;
            Result:= True;
            Break;
          end;
        end;
        CheckedSamples := ThisSample;
      end;

      if State = csSecondMoment then begin
        if not HitSignalTag.Signal[Head].AsBoolean then
          MissSignalTag.Signal[Head].AsInteger := 1;
      end;
    end;
  end;

  procedure VerticalMeets;
  var
    FieldVal: Integer;
  begin
    with RunDomain do
      if TimeLow < Curve.Length then begin
        FieldVal:= Curve[TimeLow, Channel];
        if (FieldVal >= FieldLow) and (FieldVal <= FieldHigh) then begin
          HitSignalTag.Signal[Head].AsInteger := 1;
          Result:= True;
        end else begin
          MissSignalTag.Signal[Head].AsInteger := 1;
          Result:= True;
        end;
      end;
  end;

  procedure SetTimeDomain;
  begin
    RunDomain.TimeLow := Curve.Length + EditDomain.TimeLow;
    RunDomain.TimeHigh := Curve.Length + EditDomain.TimeHigh;
  end;

  procedure SetValueDomain;
  begin
    RunDomain.FieldLow := Curve[Curve.Length, Channel] + EditDomain.FieldLow;
    RunDomain.FieldHigh := Curve[Curve.Length, Channel] + EditDomain.FieldHigh;
  end;

begin
  Result := False;

  if State = csNotEnabled then begin
    State := csFirstMoment;
    RunDomain.Copy(EditDomain);
    case Relative of
      crTime        : SetTimeDomain;
      crValue       : SetValueDomain;
      crBoth        : begin
                      SetTimeDomain;
                      SetValueDomain;
      end;
    end;
  end;

  if Curve.Length <= RunDomain.TimeLow then
    Exit;

  case CriterionType of
    ctHorizontal: HorizontalMeets;
    ctVertical: VerticalMeets;
  end;
end;

type

  TSignatureCriterionToken = (
    ctFieldName,
    ctName,
    ctHitSignalTag,
    ctMissSignalTag,
    ctEnableTag,
    ctTimeLow,
    ctTimeHigh,
    ctFieldLow,
    ctFieldHigh,
    ctCriterionType,
    ctEdge,
    ctRelative,
    ctBiasPeriod,
    ctDelaySignal,
    ctEndCriterion
  );

const
  SignatureCriterionNames : array[TSignatureCriterionToken] of PChar = (
    'Field',
    'Name',
    'HitSignalTag',
    'MissSignalTag',
    'EnableTag',
    'TimeLow',
    'TimeHigh',
    'FieldLow',
    'FieldHigh',
    'VerticalCriterion',
    'Edge',
    'Relative',
    'BiasPeriod',
    'DelayTime',
    'EndCriterion'
  );
constructor TSignatureCriterion.Load(S: TSignatureFile; aSignature : TSignature);
  function GetTag(aName : string; TagMethod : TGetTagByName) : TCriterionTag;
  begin
    if Assigned(TagMethod) then
      Result := TagMethod(Self, aName)
    else
      raise ESignatureFile.CreateFmt(SigErrorProf, ['TSignatureCriterion.Load']);
  end;

var Tmp : string;
    CT : TSignatureCriterionToken;
  function CriterionToken : TSignatureCriterionToken;
  begin
    Tmp := S.Read;
    for Result := Low(TSignatureCriterionToken) to High(TSignatureCriterionToken) do
      if CompareText(SignatureCriterionNames[Result], Tmp) = 0 then
        Exit;
    raise ESignatureFile.CreateFmt(SigErrorUnexpectedToken, [Tmp, S.Line]);
  end;
begin
  RunDomain:= TCriterionDomain.Create;
  DisplayDomain:= TCriterionDomain.Create;
  EditDomain:= TCriterionDomain.Create;

  S.ReadToken(CriterionHeader);
  ValidChannel := S.ReadInteger;
  Signature := aSignature;
  try
    while True do begin
      CT := CriterionToken;
      if CT <> ctEndCriterion then
        S.ReadToken('=');
      case CT of
        ctFieldName    : FieldName := Trim(S.ReadLine);
        ctName         : Name := Trim(S.ReadLine);
        ctHitSignalTag : HitSignalTag := GetTag(S.Read, S.OnGetSignalTag);
        ctMissSignalTag: MissSignalTag := GetTag(S.Read, S.OnGetSignalTag);
        ctEnableTag    : EnableTag := GetTag(S.Read, S.OnGetEnableTag);
        ctTimeLow      : EditDomain.TimeLow := S.ReadDouble;
        ctTimeHigh     : EditDomain.TimeHigh:= S.ReadDouble;
        ctFieldLow     : EditDomain.FieldLow:= S.ReadInteger;
        ctFieldHigh    : EditDomain.FieldHigh:= S.ReadInteger;
        ctCriterionType: CriterionType:= TCriterionType(S.ReadInteger);
        ctEdge         : Edge := TCriterionEdge(S.ReadInteger);
        ctRelative     : Relative := TCriterionRelative(S.ReadInteger);
        ctBiasPeriod   : BiasPeriod := S.ReadDouble;
        ctDelaySignal  : DelaySignal := S.ReadDouble;
        ctEndCriterion : Exit;
      end;
    end;
  finally
    // For the purpose of Signature viewing the RunDomain is preloaded with the
    // edit domain.  This allows the viewer to act as an OLD displaying data with
    // the rundomain info
    RunDomain.Copy(EditDomain);
    if HitSignalTag = nil then
      HitSignalTag := GetTag('', S.OnGetSignalTag);
    if MissSignalTag = nil then
      MissSignalTag := GetTag('', S.OnGetSignalTag);
    if EnableTag = nil then
      EnableTag := GetTag('', S.OnGetEnableTag);
  end;
  RunDomain.Copy(EditDomain);
end;

procedure TSignatureCriterion.Save(S: TSignatureFile; RunTime : Boolean);
var
  Domain : TCriterionDomain;
begin
  S.Add(Format('%s %d', [SignatureNames[stCriterion], ValidChannel]));
  S.Add(SignatureCriterionNames[ctName] + ' = ' + Name);
  S.Add(SignatureCriterionNames[ctFieldName] + ' = ' + FieldName);
  S.Add(SignatureCriterionNames[ctHitSignalTag] + ' = ' + HitSignalTag.Name);
  S.Add(SignatureCriterionNames[ctMissSignalTag] + ' = ' + MissSignalTag.Name);
  S.Add(SignatureCriterionNames[ctEnableTag] + ' = ' + EnableTag.Name);
  S.Add(SignatureCriterionNames[ctCriterionType] + ' = ' + IntToStr(Integer(CriterionType)));

  if RunTime then
    Domain := RunDomain
  else
    Domain := EditDomain;

  S.Add(SignatureCriterionNames[ctTimeLow] + ' = ' + FloatToStr(Domain.TimeLow));
  S.Add(SignatureCriterionNames[ctTimeHigh] + ' = ' + FloatToStr(Domain.TimeHigh));
  S.Add(SignatureCriterionNames[ctFieldLow] + ' = ' + IntToStr(Domain.FieldLow));
  S.Add(SignatureCriterionNames[ctFieldHigh] + ' = ' + IntToStr(Domain.FieldHigh));

  S.Add(SignatureCriterionNames[ctEdge] + ' = ' + IntToStr(Ord(Edge)));
  S.Add(SignatureCriterionNames[ctRelative] + ' = ' + IntToStr(Ord(Relative)));
  S.Add(SignatureCriterionNames[ctBiasPeriod] + ' = ' + FloatToStr(BiasPeriod));
  S.Add(SignatureCriterionNames[ctDelaySignal] + ' = ' + FloatToStr(DelaySignal));
  S.Add(SignatureCriterionNames[ctEndCriterion]);
end;

procedure TSignatureCriterion.GetVisualBoundingPoints( var T1, T2: Double; var F1, F2: Integer; RunTime : Boolean);
var D : TCriterionDomain;
begin
  UpdateDisplayDomain;
  if RunTime then
    D := RunDomain
  else
    D := DisplayDomain;

  case CriterionType of
    ctHorizontal: begin
      T1:= D.TimeLow; F1:= D.FieldLow;
      T2:= D.TimeHigh; F2:= D.FieldHigh
    end;
    ctVertical: begin
      T1:= D.TimeLow; F1:= D.FieldLow;
      T2:= D.TimeHigh; F2:= D.FieldHigh
    end;
  end;
end;

procedure TSignatureCriterion.DisplayMoveTo(FL, FH: Integer; TL, TH: Double);
begin
  with DisplayDomain do begin
    FL := FL - FieldLow;
    FH := FH - FieldHigh;
    TL := TL - TimeLow;
    TH := TH - TimeHigh;
  end;

  with EditDomain do begin
    case CriterionType of
      ctHorizontal : begin
        FieldLow := FieldLow + FL;
        FieldHigh := FieldLow;
        TimeLow := TimeLow + TL;
        TimeHigh := TimeHigh + TH;
      end;
      ctVertical : begin
        FieldLow := FieldLow + FL;
        FieldHigh := FieldHigh + FH;
        TimeLow := TimeLow + TL;
        TimeHigh := TimeLow;
      end;
    end;
  end;

  UpdateDisplayDomain;
end;

procedure TSignatureCriterion.UpdateDisplayDomain;
var
  EC : TSignatureCriterion;

  procedure SetTimeDisplay;
  begin
    DisplayDomain.TimeLow := DisplayDomain.TimeLow + EC.DisplayDomain.TimeLow;
    DisplayDomain.TimeHigh := DisplayDomain.TimeHigh + EC.DisplayDomain.TimeHigh;
  end;

  procedure SetValueDisplay;
  begin
    if EC.ValidChannel = ValidChannel then begin
      DisplayDomain.FieldLow := DisplayDomain.FieldLow + EC.DisplayDomain.FieldLow;
      DisplayDomain.FieldHigh := DisplayDomain.FieldHigh + EC.DisplayDomain.FieldHigh;
    end else begin
      DisplayDomain.FieldLow := Signature.RTV.Limits[ValidChannel].YMin;
      DisplayDomain.FieldHigh := Signature.RTV.Limits[ValidChannel].YMax;
    end;
  end;

  procedure SetBothDisplay;
  begin
    SetTimeDisplay;
    SetValueDisplay;
  end;

begin
  EC := Signature.FindEnableCriterion(EnableTag);
  DisplayDomain.Copy(EditDomain);
  if EC <> nil then begin
    EC.UpdateDisplayDomain;
    case Relative of
      crTime        : SettimeDisplay;
      crValue       : SetValueDisplay;
      crBoth        : SetBothDisplay;
    end;
  end;
end;

function TSignatureCriterion.Clone(const aName: String): TSignatureCriterion;
begin
  with EditDomain do
    Result:= TSignatureCriterion.Create(
                   FieldName, aName,
                   Signature,
                   HitSignalTag, MissSignalTag, EnableTag,
                   CriterionType, FieldLow, FieldHigh, TimeLow, TimeHigh);
  Result.ValidChannel:= ValidChannel
end;

procedure TSignatureCriterion.Reset(aHead : Integer);
begin
  State := csNotEnabled;
  with HitSignalTag.Signal[aHead] do
    if AsBoolean then
      AsBoolean := False;
  with MissSignalTag.Signal[aHead] do
    if AsBoolean then
      AsBoolean := False;

  RunDomain.TimeLow := 0;
  RunDomain.TimeHigh := 0;
  RunDomain.FieldLow := 0;
  RunDomain.FieldHigh := 0;
  Enabled:= False;
end;

constructor TSignatureFile.Create;
begin
  inherited Create;
  FSamplePoints := 150;
  SignatureList := TList.Create;
end;

destructor TSignatureFile.Destroy;
begin
  Clean;
  SignatureList.Free;
  inherited Destroy;
end;

procedure TSignatureFile.Clean;
begin
  while SignatureList.Count > 0 do begin
    TObject(SignatureList[0]).Free;
    SignatureList.Delete(0);
  end
end;

function TSignatureFile.Read : string;
begin
  Result := inherited Read;
  if Result = '#' then begin
    Self.Line := Self.Line + 1;
    Result := Read;
  end;
end;

function TSignatureFile.ReadInteger : Integer;
begin
  try
    Result := StrToInt(Self.Read);
  except
    On E : EConvertError do
      raise ESignatureFile.CreateFmt(SigInvalidIntegerValue, [Self.LastToken, Self.CurrentLine]);
  end;
end;

function TSignatureFile.ReadDouble : Double;
var aToken : string;
    Code : Integer;
begin
  Self.Seperator := SignatureNumericSeperator;
  aToken := Self.Read;
  Val(aToken, Result, Code);
  if Code <> 0 then
    raise ESignatureFile.CreateFmt(SigInvalidDataValue, [aToken, CurrentLine]);
  Self.Seperator := SignatureSeperator;
end;

function TSignatureFile.ReadIndex : Integer;
begin
  ReadToken('[');
  Result := ReadInteger;
  ReadToken(']');
end;

procedure TSignatureFile.ReadToken(aToken : string);
begin
  if CompareText(Self.Read, aToken) <> 0 then
    raise ESignatureFile.CreateFmt(SigExpectedToken, [aToken, CurrentLine]);
end;

procedure TSignatureFile.Load(WithSamples: Boolean);
var
  Sig: TSignature;
begin
  Clean;
  Rewind;
  Seperator := SignatureSeperator;
  Period := 0.5;
  if (Self.Read <> PeriodName) and Self.EndOfStream then
    Exit;
  Self.ReadToken('=');
  Period := Self.ReadDouble;
  Sig:= TSignature.Create;
  Sig.FSamplePeriod := Period;
  Sig.SamplePoints := SamplePoints;
  try
    while Sig.Load(Self, WithSamples) do begin
      Sig.FSamplePeriod := Period;
      Sig.SamplePoints := SamplePoints;
      SignatureList.Add(Sig);
      Sig:= TSignature.Create
    end;
  finally
    Sig.Free
  end
end;

procedure TSignatureFile.Save;
var I : Integer;
begin
  Self.Text := '';
  Self.Add(Format(PeriodName + ' = %.6g', [Period]));
  for i := 0 to SignatureList.Count - 1 do
    SignatureI[I].Save(Self, i, nil)
end;

function TSignatureFile.GetSignatureCount : Integer;
begin
  Result := SignatureList.Count;
end;

function TSignatureFile.GetSignatureI(Index : Integer): TSignature;
begin
  Result:= TSignature(SignatureList[Index]);
end;

procedure TSignatureFile.SignatureAdd(aSignature : TSignature);
begin
  SignatureList.Add(aSignature);
end;

procedure TSignatureFile.SignatureDelete(aSignature : TSignature);
var I : Integer;
begin
  for i := 0 to SignatureList.Count - 1 do begin
    if SignatureI[I] = aSignature then begin
      TObject(SignatureList[I]).Free;
      SignatureList.Delete(I);
      Exit;
    end;
  end;
end;


function TSignatureFile.GetSignatureProg(HoleProg: Integer): TSignature;
var I : Integer;
begin
  for I := 0 to SignatureCount - 1 do begin
    Result := SignatureI[I];
    if SignatureI[I].FileID = HoleProg then
      Exit;
  end;
  Result := nil;
end;

{procedure TSignatureFile.SetSignature(HoleProg: TValidMHP; Value: TSignature);
begin
  Signatures[HoleProg]:= Value
end; }

constructor TTrueIOTag.Create(aOwner : TTagOwner; aName : string);
begin
  inherited Create(aOwner, aName);
  Value := 1;
end;

procedure TFalseIOTag.ReadFromBuf(var Buf; aLength : Integer);
begin
end;

procedure TFalseIOTag.SetAsBoolean;
begin
end;

procedure TFalseIOTag.SetAsInteger(aValue : Integer);
begin
end;

constructor TCriterionTag.Create(aOwner : TCNC);
begin
  inherited Create;
  FOwner := aOwner;
end;

{ TCriterionDomain }

procedure TCriterionDomain.Copy(Src: TCriterionDomain);
begin
  TimeLow:= Src.TimeLow;
  TimeHigh:= Src.TimeHigh;
  FieldLow:= Src.FieldLow;
  FieldHigh:= Src.FieldHigh;
end;

constructor TCriterionDomain.Create;
begin

end;

procedure TCriterionDomain.SetFieldHigh(AFieldHigh: Integer);
begin
  FFieldHigh:= AFieldHigh;
end;

procedure TCriterionDomain.SetTimeHigh(ATimeHigh: Double);
begin
  FTimeHigh:= ATimeHigh;
end;

function TCriterionDomain.GetFieldLow: Integer;
begin
  Result:= FFieldLow
end;

function TCriterionDomain.GetTimeLow: Double;
begin
  Result:= FTimeLow;
end;

function TCriterionDomain.GetFieldHigh: Integer;
begin
  Result:= FFieldHigh
end;

procedure TCriterionDomain.SetFieldLow(AFieldLow: Integer);
begin
  FFieldLow:= AFieldLow;
end;

function TCriterionDomain.GetTimeHigh: Double;
begin
  Result:= FTimeHigh;
end;

procedure TCriterionDomain.SetTimeLow(ATimeLow: Double);
begin
  FTimeLow:= ATimeLow;
end;

function TCriterionDomain.QueryInterface(const IID: TGUID; out Obj): HResult;
const
  E_NOINTERFACE = HResult($80004002);
begin
  if GetInterface(IID, Obj) then Result := 0 else Result := E_NOINTERFACE;
end;

function TCriterionDomain._AddRef: Integer;
begin
  Inc(RefCount);
  Result := RefCount;
end;

function TCriterionDomain._Release: Integer;
begin
  Dec(RefCount);
  Result := RefCount;
end;

initialization
  RegisterTagClass(TTrueIOTag);
  RegisterTagClass(TFalseIOTag);
end.
