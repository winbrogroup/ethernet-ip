unit CurveChart;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Curves, StdCtrls, Buttons, Gather, ExtCtrls;

type
  TCurveCanvas = class(TControlCanvas)
  private
    FontAngle: Integer;
    HAngleFont: HFont;
    CanvasFontChanged: TNotifyEvent;
    procedure CreateAngleFont;
    procedure FontChanged( Sender: TObject);
  public
    constructor Create;
    destructor Destroy; override;
    procedure RotatedTextOut(X, Y: Integer; Angle: Integer; const Text: String);
                        {Angle in degrees}
  end;

  {initially, single curve plot}
  {live data cannot be added without static curves.
   live data must be compatible with static curves}
  TCurveChart = class(TControl)
  private
    FCanvas: TCurveCanvas;
    FPen: TPen;
    FBrush: TBrush;
    FCurves: TList;
    FLive: TAbstractGather;
    FSelectedCurve: TCurve;
    LastLivePoint: array [TGatherHead] of TPoint;
    LastLiveT: array [TGatherHead] of Integer;
    FChannel: Integer;
    FYScale: Double;
    FYMin, FYMax: Integer;
    FTScale: Double;
    FTMin, FTMax: Double;
    UpdateCount: Integer;
    PlotOrigin: TPoint;
    PlotOriginInvalid: Boolean;
    FOnPaint: TNotifyEvent;
    FHeads : TGatherHeads;
    procedure PlotOriginRecalc;
    procedure ScaleRecalc;  {calc axes size}
                            {font, tick size, CurveChannelUpdate}
//    procedure SetBrush( Value: TBrush);
    procedure StyleChanged( Sender: TObject);
    procedure SetChannel( Value: Integer);
    procedure WMPaint(var Msg: TWMPaint); message WM_PAINT;

    procedure CMFontChanged( var Msg: TMessage); message CM_FONTCHANGED;
    function CurveMaxY: Integer;
    function CurveMinY: Integer;
    function CurveMaxLength: Double;
    function GetFieldName: String;
    procedure SetYMax( Value: Integer);
    procedure SetYMin( Value: Integer);
    procedure SetTMax( Value: Double);
    procedure SetTMin( Value: Double);
    procedure SetSelectedCurve( Value: TCurve);
    function GetPlotRect: TRect;
    procedure SetLiveCurve( aGather: TAbstractGather);
    procedure DrawAxes;
  protected
    procedure Paint; virtual;  {probably not necessary}
  public
    function RealY( ScrY: Integer): Integer;
    function RealT( ScrT: Integer): Double;
    function ScreenY( Y: Integer): Integer;
    function ScreenT( T: Double): Integer;
    procedure DrawCurve(Curve: TCurve; aHead : TGatherHead);
    procedure SetBounds(ALeft, ATop, AWidth, AHeight: Integer); override;
    constructor Create( aOwner: TComponent); override;
    destructor Destroy; override;
    procedure SetLimits( aTMin, aTMax: Double; aYMin, aYMax: Integer);
    procedure SetYLimits( aMin, aMax: Integer);
    procedure SetTLimits( aMin, aMax: Double);
    function EditLimits: Boolean;
    procedure Invalidate; override;
    procedure BeginUpdate;
    procedure EndUpdate;
    procedure AddCurve(Value: TCurve);
    procedure LiveRefresh(aHead : TGatherHead);
    procedure AutoscaleY;
    procedure AutoscaleT;
    procedure ClearCurves;

    property Canvas: TCurveCanvas read FCanvas;
    property SelectedCurve: TCurve read FSelectedCurve write SetSelectedCurve;
    property PlotRect: TRect read GetPlotRect;
    property TScale: Double read FTScale;
    property YScale: Double read FYScale;
    property LiveCurve: TAbstractGather read FLive write SetLiveCurve;
  published
    property Align;
    property Brush: TBrush read FBrush write FBrush;
    property Font;
    property Visible;
    property OnMouseDown;
    property OnMouseMove;
    property OnMouseUp;
    property OnDblClick;
    property Channel: Integer read FChannel write SetChannel;
    property YMax: Integer read FYMax write SetYMax;
    property YMin: Integer read FYMin write SetYMin;
    property TMax: Double read FTMax write SetTMax;
    property TMin: Double read FTMin write SetTMin;
    property OnPaint: TNotifyEvent read FOnPaint write FOnPaint;
    property Heads : TGatherHeads read FHeads write FHeads;
  end;

  TCurveChartEdit = class(TForm)
    Label1: TLabel;
    YMinEdit: TEdit;
    Label2: TLabel;
    YMaxEdit: TEdit;
    Label3: TLabel;
    TMinEdit: TEdit;
    Label4: TLabel;
    TMaxEdit: TEdit;
    Panel1: TPanel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    constructor Create(Aowner : TComponent); override;
  private
    YMax, YMin: Integer;
    TMax, TMin: Double;
    CC: TCurveChart;
  public
    function Execute(aCC: TCurveChart): Boolean;
  end;

{Comments:
  TCurve is used extensively in Signature Analysis, and this visual component
  may be used to represent a curve on the screen.

  Note that a single curve may have more than one Channel (Fields) each of
  which share a time-frame. TCurveChart, as it stands only displays one field,
  but I know that Pete is keen to display all the fields on one chart. There
  should be no reason why this cannot be done (it would be very appropriate for
  live display in multi-head applications). Keep in mind that for editing it
  is much more likely that you will only want to display one field on each
  chart. Ideally both modes should be possible (maybe if Channel = -1 then show
  all)

  Remember that under the current editor CurveChart is used to show more
  than one Curve - an arbitrary number of samples are displayed along with
  the LiveData. The live data is actually Gather[Head] (i.e
  TAbstractGather.Curve[Head]) whereas the samples are


  TCurveChart was only ever meant to be a 'bare bones' implementation of
  a Curve display component. I am reasonably sure that the principle of
  TCurve is correct for signature analysis.
}


implementation

{$R *.dfm}

uses
  Math, CoreCNC;

resourcestring
  UnknownFieldName = 'unknown';

const
  fpPrec = 4;
  fpDigits = 2;
  eps30 = 1e-30;
  DefaultMaxY = 100;
  DefaultMinY = 0;
  DefaultMaxLength = 300.0;  {seconds}

procedure TCurveCanvas.CreateAngleFont;
var
  logf: TLogFont;
begin
  if HAngleFont <> 0 then
    Windows.DeleteObject(HAngleFont);
  Windows.GetObject(Font.Handle, SizeOf(TLogFont), @logf);
  logf.lfEscapement:= FontAngle*10;
  logf.lfOrientation:= FontAngle*10;
  logf.lfOutPrecision := OUT_TT_ONLY_PRECIS;
  HAngleFont:= Windows.CreateFontIndirect(logf)
end;

procedure TCurveCanvas.FontChanged( Sender: TObject);
begin
  if Assigned(CanvasFontChanged) then
    CanvasFontChanged(Sender);
  CreateAngleFont
end;

constructor TCurveCanvas.Create;
begin
  inherited Create;
  FontAngle:= 90;
  CreateAngleFont;
  CanvasFontChanged:= Font.OnChange;
  Font.OnChange:= FontChanged
end;

destructor TCurveCanvas.Destroy;
begin
  Windows.DeleteObject(HAngleFont);
  inherited Destroy
end;

procedure TCurveCanvas.RotatedTextOut(X, Y: Integer; Angle: Integer; const Text: String);
var
  OldObj: THandle;
begin
  if Angle <> FontAngle then begin
    FontAngle:= Angle;
    CreateAngleFont
  end;
  OldObj:= Windows.SelectObject(Handle, HAngleFont);
  Windows.TextOut(Handle, X, Y, PChar(Text), Length(Text));
  Windows.SelectObject(Handle, OldObj);
  Changed
end;

procedure TCurveChart.ClearCurves;
begin
  FCurves.Clear;
  Invalidate
end;

procedure TCurveChart.SetLimits( aTMin, aTMax : Double; aYMin, aYMax: Integer);
begin
  FTMin:= aTMin;
  FTMax:= aTMax;
  FYMin:= aYMin;
  FYMax:= aYMax;
  ScaleRecalc
end;

procedure TCurveChart.SetYMax( Value: Integer);
begin
  FYMax:= Value;
  ScaleRecalc
end;

procedure TCurveChart.SetYMin( Value: Integer);
begin
  FYMin:= Value;
  ScaleRecalc
end;

procedure TCurveChart.SetTMax( Value: Double);
begin
  FTMax:= Value;
  ScaleRecalc
end;

procedure TCurveChart.SetTMin( Value: Double);
begin
  FTMin:= Value;
  ScaleRecalc
end;

procedure TCurveChart.SetSelectedCurve( Value: TCurve);
begin
  FSelectedCurve:= Value;
  Invalidate
end;


constructor TCurveChart.Create( aOwner: TComponent);
begin
  inherited Create(aOwner);
  FCanvas:= TCurveCanvas.Create;
  FCurves:= TList.Create;
  FCanvas.Control:= Self;
  FPen:= TPen.Create;
//  FPen.OnChange:= StyleChanged;
  FBrush:= TBrush.Create;
  FBrush.OnChange:= StyleChanged;
  FBrush.Color := clWindow;
  
//  Font.Name:= 'Arial';
  Font.Style:= [fsBold];
  PlotOrigin.X:= 20;
  PlotOrigin.Y:= 20;
  PlotOriginInvalid:= true;
  FTMax:= DefaultMaxLength;
  FYMax:= DefaultMaxY;
  FYMin:= DefaultMinY;
  Width:= 100;
  Height:= 60
end;

destructor TCurveChart.Destroy;
begin
  FPen.Free;
  FBrush.Free;
  FCanvas.Free;
  FCurves.Free;
  inherited Destroy
end;

function TCurveChart.GetPlotRect: TRect;
begin
  Result.Left:= PlotOrigin.X + 1;
  Result.Top:=  1;
  Result.Right:= Width - 1;
  Result.Bottom:= Height - PlotOrigin.Y
end;

function TCurveChart.GetFieldName: String;
begin
  if (FCurves.Count > 0) and
     (Channel < TCurve(FCurves[0]).ChannelCount) then
    Result:= TCurve(FCurves[0]).ChannelName[Channel]
  else
    Result:= UnknownFieldName
end;

procedure TCurveChart.Invalidate;
begin
  if UpdateCount = 0 then
    inherited Invalidate;
end;

function TCurveChart.CurveMaxY: Integer;
var
  i, md: Integer;
begin
  if FCurves.Count = 0 then
  begin
    Result:= DefaultMaxY
  end else
  begin
    Result:= TCurve(FCurves[0]).MaxData[Channel];
    for i:= 1 to FCurves.Count - 1 do
    begin
      md:= TCurve(FCurves[i]).MaxData[Channel];
      if md > Result then
        Result:= md
    end
  end
end;

procedure TCurveChart.DrawCurve( Curve: TCurve; aHead : TGatherHead);
var
  i: Integer;
  LP, NP: TPoint;
  NextT: Double;
  FirstPoint, LastPoint: Integer;
begin
  if not ( (Assigned(FLive) and
           (Curve = FLive[aHead])) and not
           (aHead in Heads)  ) then begin
    if (Curve.SampleCount > 0) and (Curve.SamplePeriod > 0) and
       (Channel < Curve.SampleCount) then begin
      with Canvas do begin
        if Curve.Length > FTMin then begin
          Pen.Color:= Curve.Color;
try
          if Curve = FSelectedCurve then
            Pen.Width:= 2
          else
            Pen.Width:= 1;
          FirstPoint:= Floor(FTMin/Curve.SamplePeriod);
          if FirstPoint < 0 then
            FirstPoint:= 0;
          LastPoint:= Ceil(FTMax/Curve.SamplePeriod);
          if LastPoint >= Curve.SampleCount then
            LastPoint:= Curve.SampleCount - 1;
          {Curve Drawing area is clipped, so we can go 'over the lines'}
          MoveTo(ScreenT(FirstPoint*Curve.SamplePeriod),
                 ScreenY(Curve.SampleData[FirstPoint, Channel]));
          LP:= PenPos;
except
  on E : Exception do Exit;
end;
          for i:= FirstPoint + 1 to LastPoint do begin
            NextT:= Curve.SamplePeriod * i;
            NP.X:= ScreenT(NextT);
            NP.Y:= ScreenY(Curve.SampleData[i, Channel]);
            if (abs(NP.X - LP.X) + abs(NP.Y - LP.Y)) > 2 then begin
              LineTo(NP.X, NP.Y);
              LP:= PenPos
            end
          end;
          if Assigned(FLive) and (Curve = FLive[aHead]) then begin
            LastLivePoint[aHead]:= LP;
            LastLiveT[aHead]:= LastPoint
          end
        end;
      end
    end;
  end;
end;

procedure TCurveChart.LiveRefresh(aHead : TGatherHead);
var
  i: Integer;
  NP: TPoint;
  NextT: Double;
  FirstPoint, LastPoint: Integer;
  LCurve : TCurve;
begin
  if Assigned(FLive) then begin
    LCurve := FLive[aHead];
    if Assigned(LCurve) and
       (aHead in Heads) and
       (LastLiveT[aHead] <> LCurve.SampleCount) then begin
      if LastLiveT[aHead] > LCurve.SampleCount then begin  {erase old live data}
        Invalidate;
        LastLiveT[aHead] := 0;
      end else begin
        with Canvas do begin
          Canvas.Pen.Style := psSolid;
          if LCurve.Color <> 0 then
            Pen.Color:= LCurve.Color;
          if LastLiveT[aHead] = 0 then
            FirstPoint:= Ceil(FTMin/FLive.SamplePeriod)
          else
            FirstPoint:= LastLiveT[aHead] + 1;
          if FirstPoint < 0 then
            FirstPoint:= 0;
          LastPoint:= Floor(FTMax/LCurve.SamplePeriod);
          if LastPoint >= LCurve.SampleCount then
            LastPoint:= LCurve.SampleCount - 1;
          {FTMin may not be on a sample point... allow TCurve to interpolate}
          if LastLiveT[aHead] = 0 then begin
            LastLivePoint[aHead].X:= ScreenT(FTMin);
            LastLivePoint[aHead].Y:= ScreenY(LCurve[FTMin, Channel])
          end;
          MoveTo(LastLivePoint[aHead].X, LastLivePoint[aHead].Y);
          for i:= FirstPoint to LastPoint do begin
            NextT:= LCurve.SamplePeriod * i;
            NP.X:= ScreenT(NextT);
            NP.Y:= ScreenY(LCurve.SampleData[i, Channel]);
            if (abs(NP.X - LastLivePoint[aHead].X) +
                  abs(NP.Y - LastLivePoint[aHead].Y)) > 2 then begin
              LineTo(NP.X, NP.Y);
            end;
            LastLivePoint[aHead]:= PenPos;
          end;
        end;
        LastLiveT[aHead]:= LastPoint;
      end;
    end;
  end;
end;

procedure TCurveChart.BeginUpdate;
begin
  Inc(UpdateCount)
end;

procedure TCurveChart.EndUpdate;
begin
  Dec(UpdateCount);
  if UpdateCount = 0 then
    Invalidate
end;

procedure TCurveChart.DrawAxes;
var Str : string;
  te: TSize;
  W, H: Integer;
  pp: TPoint;
begin
  with Canvas do begin
    H := Height;
    W := Width;

    Str:= FloatToStrF(FTMin, ffGeneral, fpPrec, fpDigits);
    te := TextExtent(Str);
    TextOut(PlotOrigin.X + Pen.Width + 1, H - Pen.Width - 1 - te.cy, Str);
    Str:= FloatToStrF(FTMax, ffGeneral, fpPrec, fpDigits);
    te:= TextExtent(Str);
    TextOut(W - Pen.Width - 1 - te.cx, PenPos.Y, Str);
    MoveTo(0, H - PlotOrigin.y);
    LineTo(W, PenPos.y);
    Str:= FloatToStrF(FYMax, ffGeneral, fpPrec, fpDigits);
//    Str:= IntToStr(FYMax);
    te:= TextExtent(Str);
    pp.x:= PlotOrigin.x - Pen.Width - te.cy;
    RotatedTextOut( pp.x, te.cx + Pen.Width + 1, 90, Str);
    Str:= FloatToStrF(FYMin, ffGeneral, fpPrec, fpDigits);
//    Str:= IntToStr(FYMin);
    RotatedTextOut( pp.x, H - PlotOrigin.y - Pen.Width - 1, 90, Str);
    MoveTo(PlotOrigin.x, H);
    LineTo(PenPos.X, 0);
    Str:= GetFieldName;
    te:= TextExtent(Str);
    pp.x:= pp.x - te.cy;
    RotatedTextOut( pp.x, (H - PlotOrigin.y + te.cx) div 2, 90, Str);

    MoveTo(PlotOrigin.x, PlotOrigin.Y);
    LineTo(W, PlotOrigin.y);
    LineTo(W, H);
    LineTo(PlotOrigin.X, H);
    LineTo(PlotOrigin.x, PlotOrigin.y);
  end;
end;

procedure TCurveChart.Paint;
var
  W, H: Integer;
  i: Integer;
  hClipRegion: HRgn;
  DelayCurve: TCurve;
begin
//  Canvas.Font:= Font;  {must be before refresh}
  if PlotOriginInvalid then
    PlotOriginRecalc;

  with Canvas do begin
    Pen := FPen;
    Pen.Color := clBlack; // FPen;
    Pen.Width := 2;
    Pen.Style := psSolid;
    Brush:= FBrush;
    H:= Height;
    W:= Width;
    FillRect(Rect(0, 0, W, H));

    DrawAxes;

    {note that the TGraphicControl does not have its own device context...}
    hClipRegion:=
      Windows.CreateRectRgn(
         PlotOrigin.X + 1 + Left,
         1 + Top,
         W-1 + Left,
         H - PlotOrigin.Y + Top);


    {Draw Static Curves}
    Windows.SelectClipRgn(Handle, hClipRegion);
    Windows.DeleteObject(hClipRegion);
    DelayCurve:= nil;
    Pen.Width := 1;
    for i:= 0 to FCurves.Count - 1 do begin
      if FCurves[i] = FSelectedCurve then
        DelayCurve:= TCurve(FCurves[i])
      else
        DrawCurve(TCurve(FCurves[i]),0);
    end;

    {Draw delayed curve}
    if Assigned(DelayCurve) then
      DrawCurve(DelayCurve, 0);

    {Draw Live Curve}
    if Assigned(FLive) then
      for I := Low(TGatherHead) to High(TGatherHead) do begin
        if Assigned(FLive[I]) then
          DrawCurve(FLive[I], I);
    end;

    if Assigned(FOnPaint) then
      FOnPaint(Self);
    Windows.SelectClipRgn(Handle, 0);
  end
end;

{procedure TCurveChart.SetBrush(Value: TBrush);
begin
  FBrush.Assign(Value)
end; }

procedure TCurveChart.StyleChanged(Sender: TObject);
begin
  Invalidate
end;

procedure TCurveChart.SetChannel( Value: Integer);
begin
  if Value <> FChannel then
  begin
    FChannel:= Value;
    Invalidate
  end
end;

procedure TCurveChart.SetLiveCurve( aGather: TAbstractGather);
begin
  if Assigned(aGather) then begin
    if (aGather.ChannelCount > 0) and
       (Channel >= 0) and
       (Channel < aGather.ChannelCount) then
      FLive:= aGather
    else
      FLive:= nil;
  end else begin
    FLive:= aGather;
  end;
  Invalidate
end;

procedure TCurveChart.AddCurve( Value: TCurve);
begin
  FCurves.Add(Value);
  Invalidate
end;

procedure TCurveChart.AutoscaleY;
begin
  SetYLimits(CurveMinY, CurveMaxY)
end;

procedure TCurveChart.AutoscaleT;
begin
  SetTLimits(0, CurveMaxLength)
end;



procedure TCurveChart.SetYLimits( aMin, aMax: Integer);
begin
  FYMin:= aMin;
  FYMax:= aMax;
  ScaleRecalc
end;

procedure TCurveChart.SetTLimits( aMin, aMax: Double);
begin
  FTMin:= aMin;
  FTMax:= aMax;
  ScaleRecalc
end;

function TCurveChart.EditLimits: Boolean;
var
  F: TCurveChartEdit;
begin
  F:= TCurveChartEdit.Create(Application);
  try
    Result:= F.Execute(Self)
  finally
    F.Free
  end
end;


procedure TCurveChart.WMPaint(var Msg: TWMPaint);
begin
  if Msg.DC <> 0 then
  begin
    Canvas.Lock;
    try
      Canvas.Handle := Msg.DC;
      try
        Paint
      finally
        Canvas.Handle := 0
      end
    finally
      Canvas.Unlock
    end
  end
end;

procedure TCurveChart.SetBounds(ALeft, ATop, AWidth, AHeight: Integer);
begin
  BeginUpdate;
  inherited SetBounds(aLeft, ATop, AWidth, AHeight);
  ScaleRecalc;
  EndUpdate
end;

function TCurveChart.ScreenY( Y: Integer): Integer;
begin
  if abs(FYScale) > eps30 then
    Result:= Height - PlotOrigin.y - Round((Y - FYMin)/FYScale)
  else
    Result:= 0
end;

function TCurveChart.ScreenT( T: Double): Integer;
begin
  if abs(FTScale) > eps30 then
    Result:= PlotOrigin.x + Round((T - FTMin)/FTScale)
  else
    Result:= 0
end;

function TCurveChart.RealY( ScrY: Integer): Integer;
begin
  Result:= Round((Height - PlotOrigin.Y - ScrY)*FYScale + FYMin)
end;

function TCurveChart.RealT( ScrT: Integer): Double;
begin
  Result:= (ScrT - PlotOrigin.x)*FTScale + FTMin
end;

procedure TCurveChart.PlotOriginRecalc;
var
  th: Integer;
begin
  th:= Canvas.TextHeight('W');
  PlotOrigin.y:= th + 2*Canvas.Pen.Width;
  PlotOrigin.x:= th*2 + 2*Canvas.Pen.Width;
  ScaleRecalc;
  PlotOriginInvalid:= False;
end;

procedure TCurveChart.CMFontChanged( var Msg: TMessage);
begin
  PlotOriginInvalid:= True;
  Invalidate
end;

procedure TCurveChart.ScaleRecalc;
begin
  FYScale:= (FYMax - FYMin)/(Height - PlotOrigin.y);
  FTScale:= (FTMax - FTMin)/(Width - PlotOrigin.x);
  Invalidate
end;

// SCALING FUNCTIONS

function TCurveChart.CurveMinY: Integer;
var
  i, md: Integer;
begin
  if FCurves.Count = 0 then begin
    Result:= DefaultMinY
  end else begin
    Result:= TCurve(FCurves[0]).MinData[Channel];
    for i:= 1 to FCurves.Count - 1 do begin
      md:= TCurve(FCurves[i]).MinData[Channel];
      if md < Result then
        Result:= md
    end
  end
end;

function TCurveChart.CurveMaxLength: Double;
var
  i: Integer;
  l: Double;
begin
  if FCurves.Count = 0 then begin
    Result:= DefaultMaxLength
  end else begin
    Result:= 0;
    for i:= 0 to FCurves.Count - 1 do begin
      l:= TCurve(FCurves[i]).Length;
      if l > Result then
        Result:= l
    end
  end
end;



// CURVE CHART EDITOR

constructor TCurveChartEdit.Create(Aowner: TComponent);
begin
  inherited;
  CoreCNC.LocalizeForm(Self);

end;

function TCurveChartEdit.Execute(aCC: TCurveChart): Boolean;
begin
  CC:= aCC;
  YMinEdit.Text:= Format('%d', [CC.YMin]);
  YMaxEdit.Text:= Format('%d', [CC.YMax]);
  TMinEdit.Text:= Format('%.4f', [CC.TMin]);
  TMaxEdit.Text:= Format('%.4f', [CC.TMax]);
  Result:= ShowModal = mrOK;
  if Result then
    CC.SetLimits(TMin, TMax, YMin, YMax)
end;

procedure TCurveChartEdit.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
  CanClose:= true;
  if ModalResult = mrOK then
    try
      YMin:= StrToInt(YMinEdit.Text);
      YMax:= StrToInt(YMaxEdit.Text);
      TMin:= StrToFloat(TMinEdit.Text);
      TMax:= StrToFloat(TMaxEdit.Text);
    except
      on E: EConvertError do begin
        Application.ShowException(E);
        CanClose:= false
      end
    end
end;




end.


