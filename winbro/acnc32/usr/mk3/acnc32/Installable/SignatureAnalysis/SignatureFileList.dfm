object SignatureFileListForm: TSignatureFileListForm
  Left = 83
  Top = 205
  BorderStyle = bsDialog
  Caption = 'Dialog'
  ClientHeight = 389
  ClientWidth = 792
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -16
  Font.Name = 'Verdana'
  Font.Style = [fsBold]
  OldCreateOrder = True
  Position = poScreenCenter
  OnCloseQuery = FormCloseQuery
  PixelsPerInch = 96
  TextHeight = 18
  object GroupBox1: TGroupBox
    Left = 0
    Top = 0
    Width = 792
    Height = 304
    Align = alClient
    Caption = '$FileList'
    TabOrder = 0
    object FileList: TListBox
      Left = 2
      Top = 20
      Width = 788
      Height = 282
      Align = alClient
      ItemHeight = 18
      TabOrder = 0
      OnClick = FileListClick
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 304
    Width = 792
    Height = 85
    Align = alBottom
    BevelOuter = bvLowered
    TabOrder = 1
    object EditLabel: TLabel
      Left = 25
      Top = 14
      Width = 89
      Height = 18
      Caption = '$&Filename'
      FocusControl = FilenameEdit
    end
    object Label1: TLabel
      Left = 464
      Top = 54
      Width = 44
      Height = 18
      Caption = 'Filter'
    end
    object FilenameEdit: TEdit
      Left = 128
      Top = 10
      Width = 625
      Height = 26
      ReadOnly = True
      TabOrder = 0
    end
    object OKBtn: TButton
      Left = 77
      Top = 47
      Width = 110
      Height = 34
      Caption = '$OK'
      Default = True
      ModalResult = 1
      TabOrder = 1
    end
    object CancelBtn: TButton
      Left = 299
      Top = 47
      Width = 110
      Height = 34
      Cancel = True
      Caption = '$Cancel'
      ModalResult = 2
      TabOrder = 2
    end
    object EditFilter: TEdit
      Left = 536
      Top = 50
      Width = 121
      Height = 26
      TabOrder = 3
      Text = '*.sig'
    end
    object UpdateBtn: TButton
      Left = 680
      Top = 50
      Width = 81
      Height = 25
      Caption = 'Update'
      TabOrder = 4
      OnClick = UpdateBtnClick
    end
  end
end
