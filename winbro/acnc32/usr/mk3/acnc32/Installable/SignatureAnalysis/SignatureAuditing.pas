unit SignatureAuditing;

interface

uses Classes, SysUtils, CNCTypes, CNC32, CoreCNC, SignatureEditorFrm, Signature,
     Windows, CNCAbs, Named, SignatureEditor, Gather, FaultRegistration, FileCtrl;

type
  TSignatureAuditOnLine = class(TSignatureOnline)
  private
    AuditEnable : TAbstractTag;
    TagNames : array [0..19] of string;
    Tags : array [0..19] of TAbstractTag;
    TagCount : Integer;
    AuditFileName : TAbstractTag;
    LoggingTagList: TStringList;
    LoggingTagCount: Integer;
    LoggingTag: array [0..3] of TAbstractTag;
  protected
    procedure WriteAudit(aHead : Integer); override;
    procedure CreateSignatureRecord(aHead: Integer);
  public
    constructor Create(aOwner: TComponent); override;
    procedure InstallComponent(Params : TParamStrings); override;
    procedure Installed; override;
  end;


  TSignatureAuditViewer = class(TExpandDisplay)
  private
    Form: TSignatureEditForm;
    Signature : TSignature;
    FileName : string;
    procedure LoadSample(aTag : TAbstractTag);
//    procedure NextHole(aTag : TAbstractTag);
    procedure LoadSampleFromFile(aName : string);
  protected
    procedure Resize; override;
  public
    constructor Create(aOwner : TComponent); override;
    procedure InstallComponent(Params : TParamStrings); override;
    procedure Installed; override;
  end;


implementation

uses SignatureFileList;

constructor TSignatureAuditOnLine.Create(aOwner: TComponent);
begin
  inherited;
  LoggingTagList := TStringList.Create;
end;


procedure TSignatureAuditOnLine.InstallComponent(Params : TParamStrings);
var I : Integer;
begin
  inherited;
  CreateDirectory(PChar(LocalPath + 'Audit'), nil);
  for I := 1 to CoreCNC.SysObj.Heads do
    CreateDirectory(PChar(SignatureAuditPath(I)), nil);

  TagCount := Params.ReadInteger('TagCount', 0);
  TagCount := TagCount mod 20;

  for I := 0 to TagCount - 1 do
    TagNames[I] := Params.ReadString('Tag' + IntToStr(I), 'NotSet');

  LoggingTagCount := Params.ReadInteger('LoggingTagCount', 0);
  if LoggingTagCount > 0 then begin
    for I := 0 to LoggingTagCount - 1 do begin
      LoggingTagList.Add(Params.ReadString(Format('LoggingTag%d', [I]), NotConfiguredTagName))
    end;
  end;

  for I := 0 to CoreCNC.SysObj.Heads - 1 do
    LoggingTag[I] := Self.TagPublish(Format(Name + 'Report%d', [I+1]), TStringTag);
end;

procedure CleanupFilename(var Filename : string);
var I : Integer;
begin
  for I := 1 to Length(Filename) do begin
    case FileName[I] of
      '/' : Filename[I] := '-';
      '\' : Filename[I] := '-';
      ':' : Filename[I] := '-';
      '*' : Filename[I] := '_';
      '?' : Filename[I] := '_';
      '"' : Filename[I] := '_';
      '<' : Filename[I] := '_';
      '>' : Filename[I] := '_';
      '|' : Filename[I] := '_';
    end;
  end;
end;

function RemoveDelimiters(const AToken: string): string;
var I: Integer;
    Token: string;
begin
  Token:= AToken;
  for I := 1 to Length(Token) do begin
    case Token[I] of
      ',' : Token[I] := '_';
    end;
  end;
  Result := Token;
end;

procedure TSignatureAuditOnLine.Installed;
var I : Integer;
begin
  inherited;
  AuditEnable := TagEvent(Name + 'EnableAudit', edgeChange, TMethodTag, nil);
  AuditFileName := TagEvent(Name + 'FileName', edgeChange, TStringTag, nil);

  for I := 0 to TagCount - 1 do
    Tags[I] := TagEvent(TagNames[I], EdgeChange, TStringTag, nil);

  for I := 0 to LoggingTagList.Count - 1 do begin
    if LoggingTagList[I] <> '' then
      LoggingTagList.Objects[I] := TagEvent(LoggingTagList[I], EdgeChange, TStringTag, nil)
    else
      LoggingTagList.Objects[I] := TagEvent(NotConfiguredTagName, EdgeChange, TStringTag, nil);
  end;
end;


procedure TSignatureAuditOnLine.CreateSignatureRecord(aHead: Integer);
var I: Integer;
    OutText: string;
begin
  OutText := StorageDate(LoggingStartTime[aHead]) + ',' +
             StorageDate(Now) + ',' +
             IntToStr(Self.Gather.HoleProgram[aHead]) + ',' +
             AuditFileName.AsString;
  try
    if LoggingTagCount > 0 then begin
      for I := 0 to LoggingTagCount - 1 do begin
        OutText := OutText + ',' + RemoveDelimiters(TAbstractTag(LoggingTagList.Objects[I]).AsString);
      end;
    end;
  except
    on E: Exception do
      EventLogFmt('Signature report error [%s]', [E.Message]);
  end;
  LoggingTag[aHead].AsString := OutText;
end;


procedure TSignatureAuditOnLine.WriteAudit(aHead : Integer);
var FileName : string;
    I : Integer;
begin

  try
    if AuditEnable.AsBoolean then begin
      if TagCount = 0 then begin
        FileName := SignatureAuditPath(aHead + 1) + AuditFileName.AsString;
        try
          ForceDirectories(ExtractFilePath(FileName));
          Audit[aHead].SaveToFile(FileName);
        except
          on E : Exception do
          SysObj.FaultInterface.SetFault(Self, CoreFaults[cfSysWarning],
            [Format('Signature file failed to save "%s"', [E.Message])], nil);
        end;
      end else begin
        FileName := '';
        for I := 0 to TagCount - 1 do
          FileName := FileName + RemoveDelimiters(Tags[I].AsString) + '.';

        CleanupFilename(Filename);

        AuditFileName.AsString := FileName + 'sig';
        CreateSignatureRecord(aHead);
        FileName := SignatureAuditPath(aHead + 1) + FileName + 'sig';
        try
          ForceDirectories(ExtractFilePath(FileName));
          Audit[aHead].SaveToFile(FileName);
        except
          on E : Exception do begin
            SysObj.FaultInterface.SetFault(Self, CoreFaults[cfSysWarning], [Format('Unable to write File %s', [FileName])], nil);
          end;
        end;
      end;
    end;
  finally
  end;
end;

procedure TSignatureAuditViewer.Resize;
begin
  Form.ResizeCharts;
end;

constructor TSignatureAuditViewer.Create(aOwner : TComponent);
begin
  inherited Create(aOwner);
  Form:= TSignatureEditForm.Create(Self);
  Form.OLD:= True;
  Form.ChartPanel.Parent:= Self;
end;

procedure TSignatureAuditViewer.InstallComponent(Params : TParamStrings);
begin
  inherited InstallComponent(Params);
end;

procedure TSignatureAuditViewer.Installed;
begin
  inherited Installed;
  TagEvent(Name + 'Load', EdgeFalling, TMethodTag, LoadSample);
end;

procedure TSignatureAuditViewer.LoadSampleFromFile(aName : string);
var J : TGatherField;
    I : Integer;
begin
{
  if Signature <> nil then
    Signature.Free;
 }
  Signature := TSignature.Create;

  try
    Form.SigFile.Clean;
    Form.SigFile.LoadFromFile(aName);
    Form.SigFile.Rewind;
    Form.SigFile.Read; // Read period designation (no trap for error)
    Form.SigFile.Period := Form.SigFile.ReadDouble;

    Signature.Load(Form.SigFile, True);
    Form.SigFile.SignatureAdd(Signature);
    Signature.SamplePeriod := Form.SigFile.Period;
    Form.SelectedHoleProgram := Signature.ID;
    Form.SelectedHeads := [0];
    Form.SelectedHead := 0;
    Form.SigParamsRefresh;

    for J:= Low(TGatherField) to High(TGatherField) do
      with Form.Charts[J] do begin
        BeginUpdate;
        ClearCurves
      end;

    if Assigned(Signature) and (Form.SelectedHoleProgram <> 0) then begin
      for I := 0 to Signature.SampleCount - 1 do begin
        for J := Low(TGatherField) to High(TGatherField) do
          Form.Charts[J].AddCurve(Signature.SampleCurve[I]);
      end;
    end;

    for J:= Low(TGatherField) to High(TGatherField) do
      Form.Charts[J].EndUpdate;

    FileName := aName;
  except
    On E : Exception do begin
      Form.SigFile.Clean;
      SysObj.FaultInterface.SetFault(Self, CoreFaults[cfSysWarning], [E.Message], nil);
    end;
  end;
end;

procedure TSignatureAuditViewer.LoadSample(aTag : TAbstractTag);
var NewFileName : string;
begin
  if not ATag.AsBoolean then begin
    Newfilename:= '*.*';
    if SignatureFileList.SelectFile(NewFileName) then begin
      LoadSampleFromFile(NewFileName);
    end;
  end;
end;


initialization
  RegisterCNCClass(TSignatureAuditViewer);
  RegisterCNCClass(TSignatureAuditOnLine);
end.
