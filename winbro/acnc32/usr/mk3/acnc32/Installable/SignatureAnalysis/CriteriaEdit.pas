unit CriteriaEdit;

interface

uses Windows, SysUtils, Classes, Graphics, Forms, Controls, StdCtrls,
  Buttons, ExtCtrls, Signature, ComCtrls, CoreCNC;

type
  TCriteriaEditDlg = class(TForm)
    Panel1: TPanel;
    BitBtn1: TBitBtn;
    CancelBtn: TBitBtn;
    GroupBox1: TGroupBox;
    FieldLowLabel: TLabel;
    FieldHighLabel: TLabel;
    FieldHighEdit: TEdit;
    FieldLowEdit: TEdit;
    TimeLowLabel: TLabel;
    TimeHighLabel: TLabel;
    TimeHighEdit: TEdit;
    TimeLowEdit: TEdit;
    EnableSignalPanel: TPanel;
    GroupBox2: TGroupBox;
    GroupBox3: TGroupBox;
    GroupBox4: TGroupBox;
    NameLabel: TLabel;
    NameEdit: TEdit;
    FieldLabel: TLabel;
    FieldList: TComboBox;
    EnableComboBox: TComboBox;
    TypeGroup: TRadioGroup;
    HitSignalComboBox: TComboBox;
    MissSignalComboBox: TComboBox;
    Label1: TLabel;
    Label2: TLabel;
    BiasPeriodEdit: TEdit;
    BiasLabel: TLabel;
    DelaySignalEdit: TEdit;
    DelayLabel: TLabel;
    EdgeGroup: TRadioGroup;
    RelativeGroup: TRadioGroup;
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure IntFieldExit(Sender: TObject);
    procedure RealFieldExit(Sender: TObject);
    procedure TypeGroupClick(Sender: TObject);
  private
    Crit: TSignatureCriterion;
    function SignalsOK : Boolean;
  public
    constructor Create(Aowner : TComponent); override;
    function Execute( SC: TSignatureCriterion;
                      Fields: TStrings;
                      Signals : TStrings;
                      Enables : TStrings): Boolean;
    procedure OrientationRefresh;
  end;

  { NOTE :
    Signals Miss / Hit are integrity checked for uniquness but the logic
    used will not correct for already corrupted integrity.  This must be
    performed when the criterion is loaded from disk }
implementation
uses
  Dialogs;
{$R *.DFM}
resourcestring
  InvalidData = 'Invalid input';
  InvalidOutputSignalLang = 'Output Signals Conflict with another Criteria';
  InputSignalUniqueLang = 'The Enable Signal must be Unique for a Relative Gate';
  SignalIsReadOnlyLang = 'An Output Signal Is Declared Read Only';
  OutputSignalMustBeUniqueLang = 'The Output Signal Cannot be Shared as it is Used by a Relative Gate';
  EnableCausesCircularLang = 'Enable Signal Causes a Circular Reference';
  OutputCausesCircularLang = 'Output Signal Causes a Circular Reference';
  OutputSameAsInput = 'The Enable Signal is the Same as an Output Signal';
  DefaultName = 'New Criterion';

function EditGetReal(Edit: TEdit; var Val: Double): Boolean;
var
  Code: Integer;
begin
  System.Val(Edit.Text, Val, Code);
  Result:= Code = 0
end;

procedure EditSetReal(Edit: TEdit; const Val: Double);
begin
  Edit.Text:= FloatToStr(Val)
end;

function EditGetInt(Edit: TEdit; var Val: Integer): Boolean;
var
  Code: Integer;
begin
  System.Val(Edit.Text, Val, Code);
  Result:= Code = 0
end;

procedure EditSetInt(Edit: TEdit; const Val: Integer);
begin
  Edit.Text:= IntToStr(Val)
end;

function TCriteriaEditDlg.Execute( SC: TSignatureCriterion;
                                   Fields: TStrings;
                                   Signals : TStrings;
                                   Enables : TStrings): Boolean;
begin
  NameEdit.Text:= SC.Name;
  FieldList.Items.Clear;
  FieldList.Items.AddStrings(Fields);
  FieldList.ItemIndex:= SC.ValidChannel;
  TypeGroup.ItemIndex:= Integer(SC.CriterionType);
  with HitSignalComboBox do begin
    Items := Signals;
    ItemIndex:= Items.IndexOf(SC.HitSignalTag.Name);
    if ItemIndex = -1 then
      ItemIndex := 0;
  end;

  with MissSignalComboBox do begin
    Items := Signals;
    ItemIndex:= Items.IndexOf(SC.MissSignalTag.Name);
    if ItemIndex = -1 then
      ItemIndex := 0;
  end;

  with EnableComboBox do begin
    Items := Enables;
    ItemIndex := Items.IndexOf(SC.EnableTag.Name);
    if ItemIndex = -1 then
      ItemIndex := 0;
  end;

  EdgeGroup.ItemIndex := Ord(SC.Edge);
  RelativeGroup.ItemIndex := Ord(SC.Relative);

  with SC.EditDomain do begin
    EditSetReal(TimeHighEdit, TimeHigh);
    EditSetReal(TimeLowEdit, TimeLow);
    EditSetInt(FieldLowEdit, FieldLow);
    EditSetInt(FieldHighEdit, FieldHigh);
  end;
  EditSetReal(BiasPeriodEdit, SC.BiasPeriod);
  EditSetReal(DelaySignalEdit, SC.DelaySignal);

  Crit:= SC;
  OrientationRefresh;
  Result:= ShowModal = mrOK;
  if Result then begin
    SC.FieldName:= FieldList.Text;
    SC.Name:= NameEdit.Text;
    SC.CriterionType:= TCriterionType(TypeGroup.ItemIndex);

    // Assign the new signals
    with HitSignalComboBox do
      SC.HitSignalTag:= TCriterionTag(Items.Objects[ItemIndex]);
    with MissSignalComboBox do
      SC.MissSignalTag:= TCriterionTag(Items.Objects[ItemIndex]);
    with EnableComboBox do
      SC.EnableTag:= TCriterionTag(Items.Objects[ItemIndex]);

    SC.ValidChannel:= FieldList.ItemIndex;
    SC.Edge := TCriterionEdge(EdgeGroup.ItemIndex);
    SC.Relative := TCriterionRelative(RelativeGroup.ItemIndex);

    case SC.CriterionType of
      ctHorizontal : SC.EditDomain.FieldHigh := SC.EditDomain.FieldLow;
      ctVertical   : SC.EditDomain.TimeHigh := SC.EditDomain.TimeLow;
    end;

  end;
end;

function TCriteriaEditDlg.SignalsOK : Boolean;
var cTag : TCriterionTag;
begin
  Result := False;

  { First check that the selected Enable has either unique or no current setting }
  with EnableComboBox do begin
    CTag:= TCriterionTag(Items.Objects[ItemIndex]);
    if Items.Objects[ItemIndex] <> Crit.EnableTag then begin
      if TCriterionRelative(RelativeGroup.ItemIndex) <> crNonRelative then begin
        if Crit.Signature.SignalUsage(CTag) > 1 then begin
          MessageDlg(InputSignalUniqueLang, mtError, [mbOK], 0);
          Result := False;
          Exit;
        end;
      end;
    end;
  end;

  { Now check that the output does not match the input ; CTag is the enable signal
    set above }
  with HitSignalComboBox do
    if Items.Objects[ItemIndex] = CTag then begin
      MessageDlg(OutputSameAsInput, mtError, [mbOk], 0);
      Exit;
    end;

  with MissSignalComboBox do
    if Items.Objects[ItemIndex] = CTag then begin
      MessageDlg(OutputSameAsInput, mtError, [mbOk], 0);
      Exit;
    end;

  with HitSignalComboBox do
    { If the Signal has changed }
    if Items.Objects[ItemIndex] <> Crit.HitSignalTag then begin
     { Check that the selected HitSignal is not Readonly }
      cTag := TCriterionTag(Items.Objects[ItemIndex]);
      if not cTag.Signal[0].IsWritable then begin
        MessageDlg(SignalIsReadOnlyLang, mtError, [mbOK], 0);
        Exit;
      end;

      { Check that the Selected HitSignal if used relative does not
        Create a double reference }
      if Crit.Signature.SignalUsedRelative(cTag) and
         (Crit.Signature.SignalUsage(cTag) <> 0) then begin
        MessageDlg(OutputSignalMustBeUniqueLang, mtError, [mbOK], 0);
        Exit;
      end;

      { Check that this output will not create a circular reference }
      if Crit.Signature.IsCircularSignal(cTag, Crit) then begin
        MessageDlg(OutputCausesCircularLang, mtError, [mbOK], 0);
        Exit;
      end;
    end;

  with MissSignalComboBox do
    { If the Signal has changed }
    if Items.Objects[ItemIndex] <> Crit.MissSignalTag then begin
     { Check that the selected MissSignal is not Readonly }
      cTag := TCriterionTag(Items.Objects[ItemIndex]);
      if not cTag.Signal[0].IsWritable then begin
        MessageDlg(SignalIsReadOnlyLang, mtError, [mbOK], 0);
        Exit;
      end;

      { Check that the Selected MissSignal if used relative does not
        Create a double reference }
      if Crit.Signature.SignalUsedRelative(cTag) and
         (Crit.Signature.SignalUsage(cTag) <> 0) then begin
        MessageDlg(OutputSignalMustBeUniqueLang, mtError, [mbOK], 0);
        Exit;
      end;

      { Check that this output will not create a circular reference }
      if Crit.Signature.IsCircularSignal(cTag, Crit) then begin
        MessageDlg(OutputCausesCircularLang, mtError, [mbOK], 0);
        Exit;
      end;
    end;


  Result := True;
end;

procedure TCriteriaEditDlg.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
  if ModalResult <> mrCancel then begin
    with Crit.EditDomain do
      CanClose:= ( EditGetReal(TimeLowEdit, FTimeLow) and
                   EditGetReal(TimeHighEdit, FTimeHigh) and
                   EditGetInt(FieldLowEdit, FFieldLow) and
                   EditGetInt(FieldHighEdit, FFieldHigh)and
                   EditGetReal(BiasPeriodEdit, Crit.BiasPeriod)and
                   EditGetReal(DelaySignalEdit, Crit.DelaySignal) );
    if not CanClose then
      MessageDlg(InvalidData, mtError, [mbOK], 0);

    if CanClose then
      CanClose := SignalsOK;
  end;
end;

procedure TCriteriaEditDlg.IntFieldExit(Sender: TObject);
begin
  if ActiveControl <> CancelBtn then
  begin
    try
      StrToInt(TEdit(Sender).Text)
    except
      on E: EConvertError do
      begin
        ActiveControl:= TEdit(Sender);
        Application.ShowException(E)
      end
    end
  end
end;

procedure TCriteriaEditDlg.OrientationRefresh;
begin
  TimeHighEdit.Enabled:=  (TypeGroup.ItemIndex = 0);
  TimeHighLabel.Enabled:= TimeHighEdit.Enabled;
  FieldHighEdit.Enabled:= (TypeGroup.ItemIndex = 1);
  FieldHighLabel.Enabled:= FieldHighEdit.Enabled;

  EdgeGroup.Enabled := TCriterionType(TypeGroup.ItemIndex) = ctHorizontal;
end;

procedure TCriteriaEditDlg.RealFieldExit(Sender: TObject);
begin
  if ActiveControl <> CancelBtn then
  begin
    try
    StrToFloat(TEdit(Sender).Text)
    except
      on E: EConvertError do
      begin
        ActiveControl:= TEdit(Sender);
        Application.ShowException(E)
      end
    end
  end
end;

procedure TCriteriaEditDlg.TypeGroupClick(Sender: TObject);
begin
  OrientationRefresh
end;

constructor TCriteriaEditDlg.Create(Aowner: TComponent);
begin
  inherited;
  CoreCNC.LocalizeForm(Self);
end;

end.
