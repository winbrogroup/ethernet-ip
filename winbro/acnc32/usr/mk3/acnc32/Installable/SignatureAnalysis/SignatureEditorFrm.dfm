object SignatureEditForm: TSignatureEditForm
  Left = 480
  Top = 332
  Width = 800
  Height = 600
  Caption = '$SIGNATUREEDITORMINFORM'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = True
  Position = poScreenCenter
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 16
  object SidePanel: TPanel
    Left = 575
    Top = 0
    Width = 217
    Height = 570
    Align = alRight
    BevelOuter = bvNone
    Caption = 'SidePanel'
    TabOrder = 0
    object CriteriaGroup: TGroupBox
      Left = 0
      Top = 81
      Width = 217
      Height = 128
      Align = alTop
      Caption = '$CRITERIA_HOTKEY'
      TabOrder = 1
      object CriteriaList: TListBox
        Left = 2
        Top = 18
        Width = 127
        Height = 100
        Style = lbOwnerDrawFixed
        Align = alLeft
        IntegralHeight = True
        ItemHeight = 16
        TabOrder = 0
        OnClick = CriteriaListClick
        OnDrawItem = CriteriaListDrawItem
      end
    end
    object SamplesGroup: TGroupBox
      Left = 0
      Top = 209
      Width = 217
      Height = 144
      Align = alTop
      Caption = '$SAMPLES'
      TabOrder = 2
      object SampleList: TCheckListBox
        Left = 2
        Top = 18
        Width = 127
        Height = 116
        OnClickCheck = SampleListClickCheck
        Align = alLeft
        IntegralHeight = True
        ItemHeight = 16
        Style = lbOwnerDrawFixed
        TabOrder = 0
        OnClick = SampleListClick
        OnDrawItem = SampleListDrawItem
      end
    end
    object SigInfoBox: TGroupBox
      Left = 0
      Top = 0
      Width = 217
      Height = 81
      Align = alTop
      Caption = '$SIGNATURE_INFO'
      TabOrder = 0
      object SampleCountLabel: TLabel
        Left = 7
        Top = 21
        Width = 115
        Height = 16
        Caption = 'SampleCountLabel'
      end
      object CriteriaCountLabel: TLabel
        Left = 107
        Top = 21
        Width = 110
        Height = 16
        Caption = 'CriteriaCountLabel'
      end
      object SamplePeriodCombo: TComboBox
        Left = 16
        Top = 44
        Width = 145
        Height = 28
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ItemHeight = 20
        ParentFont = False
        TabOrder = 0
        Text = 'SamplePeriodCombo'
        OnChange = SamplePeriodComboChange
      end
    end
    object ViewGroup: TGroupBox
      Left = 0
      Top = 353
      Width = 217
      Height = 120
      Align = alTop
      Caption = '$VIEW'
      TabOrder = 3
      object FieldList: TCheckListBox
        Left = 2
        Top = 18
        Width = 127
        Height = 100
        OnClickCheck = FieldListClickCheck
        Align = alLeft
        IntegralHeight = True
        ItemHeight = 16
        TabOrder = 0
        OnClick = FieldListClick
      end
    end
    object HeadGroup: TGroupBox
      Left = 0
      Top = 473
      Width = 217
      Height = 97
      Align = alClient
      TabOrder = 4
    end
  end
  object LHFrame: TPanel
    Left = 0
    Top = 0
    Width = 575
    Height = 570
    Align = alClient
    BevelInner = bvLowered
    BevelOuter = bvNone
    TabOrder = 1
    object ChartPanel: TPanel
      Left = 1
      Top = 1
      Width = 573
      Height = 516
      Align = alClient
      BevelInner = bvLowered
      BevelOuter = bvNone
      BorderWidth = 4
      Caption = '$NO_SIGNATURE'
      TabOrder = 0
    end
    object HoleProgPanel: TPanel
      Left = 1
      Top = 517
      Width = 573
      Height = 52
      Align = alBottom
      TabOrder = 1
      object HPLabel: TLabel
        Left = 13
        Top = 13
        Width = 148
        Height = 16
        Caption = '$PROCESS_PROGRAM'
      end
      object HPEdit: TEdit
        Left = 147
        Top = 10
        Width = 110
        Height = 32
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -19
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        Text = '0'
      end
      object ProgNoUD: TUpDown
        Left = 257
        Top = 10
        Width = 32
        Height = 32
        Associate = HPEdit
        Max = 32000
        TabOrder = 1
        OnClick = ProgNoUDClick
      end
    end
  end
  object ChartPopup: TPopupMenu
    Left = 280
    Top = 432
    object ChartPopupNew: TMenuItem
      Caption = '$New'
      object ChartPopupHorizontal: TMenuItem
        Caption = '$Horizontal'
        Default = True
        OnClick = ChartPopupNewClick
      end
      object ChartPopupVertical: TMenuItem
        Tag = 1
        Caption = '$Vertical'
        OnClick = ChartPopupNewClick
      end
    end
    object ChartPopuFitSamples: TMenuItem
      Caption = '$Fittosamples'
      OnClick = ChartPopupFitAllClick
    end
    object ChartPopupFitCriteria: TMenuItem
      Caption = '$Fittocriteria'
      OnClick = ChartPopupFitCriteriaClick
    end
    object ChartPopupPaste: TMenuItem
      Caption = '$Paste'
    end
    object ChartPopupZoomDefault: TMenuItem
      Caption = '$ShowfieldRTV'
      OnClick = ChartPopupZoomDefaultClick
    end
    object ChartPopupSetDefaults: TMenuItem
      Caption = '$SavefieldRTV'
      OnClick = ChartPopupSetDefaultsClick
    end
    object ChartPopupEditLimits: TMenuItem
      Caption = '$EditLimits'
      OnClick = ChartPopupEditLimitsClick
    end
  end
  object OpenDialog1: TOpenDialog
    Left = 360
    Top = 250
  end
  object OpenDialog2: TOpenDialog
    Left = 370
    Top = 260
  end
end
