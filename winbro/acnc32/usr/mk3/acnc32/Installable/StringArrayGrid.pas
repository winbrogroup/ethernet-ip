unit StringArrayGrid;

interface

uses SysUtils, Classes, CoreCNC, CNCAbs, IStringArray, StdCtrls, ExtCtrls,
     Controls, Windows, Graphics, CommCtrl, Named, CNCTypes, CNC32, Grids;

type
  TStringArrayGrid = class(TExpandDisplay, IACNC32StringArrayNotify, IACNC32StringArrayNotifyA)
  private
    ComboBox: TComboBox;
    Panel: TPanel;
    Grid: TStringGrid;
    Table : IACNC32StringArrayA;
    ExtentColumn: array [0..100] of Integer;
    DefaultTable: string;
    FixedCols: Integer;

    procedure DisplaySweep(ATag: TAbstractTag);
    procedure RedrawTable(WithExtent: Boolean = False);
    procedure SelectCell(Sender: TObject; ACol, ARow: Longint; var CanSelect: Boolean);
    procedure InitializeTable;
    procedure ComboBoxChange(Sender: TObject);
    procedure UpdateComboBox;
    procedure ConnectToTable(TableName: string);
  public
    constructor Create(AOwner: TComponent); override;
    procedure InstallComponent(Params: TParamStrings); override;
    procedure Installed; override;

    // IACNC32StingArrayNotify
    procedure CellChange(Table : IACNC32StringArray; Row, Col : Integer); stdcall;
    procedure TableChange(Table : IACNC32StringArray); stdcall;

    // IACNC32StringArrayNotifyA
    procedure CursorChange(Table : IACNC32StringArrayA; Cursor: Integer); stdcall;
    function GetName: WideString; stdcall;
  end;

implementation

{ TStringArrayViewer }

procedure TStringArrayGrid.CellChange(Table: IACNC32StringArray; Row,
  Col: Integer);
var V : WideString;
begin
  Table.GetValue(Row, Col, V);
  Grid.Cells[Col, Row + 1]:= V;
end;

procedure TStringArrayGrid.ComboBoxChange(Sender: TObject);
var ArrayName: string;
begin
  if ComboBox.ItemIndex <> -1 then begin
    ArrayName:= ComboBox.Items[ComboBox.ItemIndex];
    ConnectToTable(ArrayName);
  end;
end;

procedure TStringArrayGrid.ConnectToTable(TableName: string);
begin
  if Table <> nil then begin
    SysObj.StringArraySet.ReleaseArray(Self, Table);
    Table:= nil;
  end;
  SysObj.StringArraySet.UseArrayA(TableName, Self, Table);
  InitializeTable;
end;

constructor TStringArrayGrid.Create(AOwner: TComponent);
begin
  inherited;
  ComboBox:= TComboBox.Create(Self);
  Panel:= TPanel.Create(Self);
  Panel.Parent:= Self;
  ComboBox.Parent:= Panel;
  Panel.Align:= alTop;
  Panel.Height:= 22;
  ComboBox.OnChange:= ComboBoxChange;

  Grid:= TStringGrid.Create(Self);
  Grid.Parent:= Self;
  Grid.FixedCols:= 0;
  Grid.Align:= alClient;
  Grid.DefaultRowHeight:= 15;
  //Grid.Font.Name:= SysObj.FontName;
  Grid.Font.Height:= 12;
  Grid.OnSelectCell:= SelectCell;
end;

procedure TStringArrayGrid.CursorChange(Table: IACNC32StringArrayA;
  Cursor: Integer);
begin

end;

procedure TStringArrayGrid.DisplaySweep(ATag: TAbstractTag);
var CPos: Integer;
begin
  if Table <> nil then begin
    CPos:= Table.Cursor + 1;
    if CPos <> Grid.Row  then
      if Grid.RowCount > CPos then begin
         Grid.Row:= CPos;
      end;
  end;
end;

function TStringArrayGrid.GetName: WideString;
begin
  Result:= Name;
end;

procedure TStringArrayGrid.InitializeTable;
var I : Integer;
    S : WideString;
begin
    if Table <> nil then begin
      Grid.ColCount:= Table.FieldCount;
      Grid.RowCount:= Table.RecordCount + 1;

      for I:= 0 to Table.FieldCount - 1 do begin
        Table.FieldNameAtIndex(I, S);
        Grid.Cells[I, 0]:= S;
      end;
    end;

    RedrawTable(True);
end;

procedure TStringArrayGrid.InstallComponent(Params: TParamStrings);
var I: Integer;
begin
  inherited;
  Panel.Visible:= Params.ReadBool('CanSelectTable', True);
  DefaultTable:= Params.ReadString('DefaultTable', '');

  UpdateComboBox;

  Grid.Options:= Grid.Options + [goRowSelect, goColSizing] - [{goVertLine, }goRangeSelect];
  for I:= 0 to ComboBox.Items.Count - 1 do begin
    if CompareText(ComboBox.Items[I], DefaultTable) = 0 then begin
      ComboBox.ItemIndex:= I;
      Break;
    end;
  end;

  FixedCols:= Params.ReadInteger('FixedCols', 1);
end;

procedure TStringArrayGrid.Installed;
begin
  inherited;
  if DefaultTable <> '' then
    ConnectToTable(DefaultTable);

  TagEvent(NameDisplaySweep, EdgeChange, TMethodTag, DisplaySweep);
end;

procedure TStringArrayGrid.RedrawTable(WithExtent: Boolean);
var I, K : Integer;
    S : WideString;
    Tmp: Integer;
begin
  if WithExtent then begin
    for K:= 0 to Table.FieldCount - 1 do
      ExtentColumn[K]:= Grid.Canvas.TextWidth(Grid.Cells[0, K]) + 20;
  end;

  for I:= 0 to Table.RecordCount - 1 do begin
    for K:= 0 to Table.FieldCount - 1 do begin
      Table.GetValue(I, K, S);
      Grid.Cells[K, I + 1]:= S;

      if WithExtent then begin
        Tmp:= Grid.Canvas.TextWidth(S) + 20;
        if Tmp > ExtentColumn[K] then
          ExtentColumn[K]:= Tmp;
      end;
    end;
  end;
  for I:= 0 to Grid.ColCount - 1 do begin
    Grid.ColWidths[I]:= ExtentColumn[I];
  end;
  if Grid.ColCount > FixedCols then
    Grid.FixedCols:= FixedCols;
end;

procedure TStringArrayGrid.SelectCell(Sender: TObject; ACol, ARow: Integer;
  var CanSelect: Boolean);
begin
  if Table <> nil then
    if Grid.Row > 0 then
      Table.SetCursor(ARow - 1);
end;

procedure TStringArrayGrid.TableChange(Table: IACNC32StringArray);
begin
  InitializeTable;
end;

procedure TStringArrayGrid.UpdateComboBox;
var S : WideString;
    I : Integer;
begin
  ComboBox.Items.Clear;
  for I := 0 to SysObj.StringArraySet.ArrayCount - 1 do begin
    SysObj.StringArraySet.AliasAtIndex(I, S);
    ComboBox.Items.Add(S);
  end;
end;

initialization
  CoreCNC.RegisterCNCClass(TStringArrayGrid, 'MDTGrid');
end.
