unit PlcText;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  CoreCNC, CNC32, CNCTypes, StdCtrls, ExtCtrls, IniFiles, ObjectPlc, CNCUtils,
  named, PLC32, PlcItems, Editor, Acknowledge, CNCAbs, ViewPlcItem, FaultRegistration,
  ComCtrls, PlusMemo;

type
  TObjectPlcText = class(TExpandDisplay)
  private
    Text         : TPlusMemo;
    Title        : TPanel;
    Fault        : TPanel;
    Posi         : TPanel;
    download     : array [0..MAX_NBR_OBJECT_PLC] of boolean;
    next         : TAbstractTag;
    prev         : TAbstractTag;
    down         : TAbstractTag;
    upda         : TAbstractTag;
    PlcN         : TAbstractTag;
    PlcL         : TAbstractTag;
    PlcCompile   : TAbstractTag;
    AllocMemCount: TAbstractTag;
    AllocMemSize : TAbstractTag;
    TagCount     : TAbstractTag;
    PlcItemCount : TAbstractTag;
    PLC          : TObjectPlc;
    SummaryTag : TAbstractTag;
    SummaryVisible: TAbstractTag;
    Access : TAccessLevels;
    ListView : TListView;
    SearchBox : TEdit;
    procedure SearchChange(Sender : TObject);
    procedure ApplyKeyWords;
    procedure TextKeyDown(Sender : TObject;  var Key: Word; Shift: TShiftState);
    procedure SearchKeyDown(Sender : TObject;  var Key: Word; Shift: TShiftState);
    procedure SearchKeyPress(Sender: TObject; var Key: Char);
    procedure SearchExit(Sender : TObject);
  protected
    procedure SavePlc;
    procedure DisplayTextPlc(aPLC : Integer);
    function  PlcFileName(aPlc : Integer) : string;
    function  PlcShortFileName(aPlc : Integer) : string;
    procedure CreateTemplate(aPlc : Integer);
    procedure EnableDisableEvent(aTag : TAbstractTag);
    procedure UserAccessChange(aTag : TAbstractTag); override;
    procedure RungUp(aTag : TAbstractTag);
    procedure RungDown(aTag : TAbstractTag);
    procedure DoSummary(aTag : TAbstractTag);
    procedure DoSummaryClick(Sender : TObject);
    procedure DoSummaryIntl(Show: Boolean);
  public
    constructor Create(AOwner : TComponent); override;
    destructor Destroy; override;
    procedure InstallComponent(Params : TParamStrings); override;
    procedure CallBack (aTag : TAbstractTag);
    procedure Installed; override;
    procedure Resize; override;
    procedure Closing; override;
    procedure Open; override;
  published
  end;

  TObjectPlcGraphics = class(TExpandDisplay)
  private
    PlcN      : TAbstractTag;
    PlcL      : TAbstractTag;
    PlcC      : TAbstractTag;
    PLC       : TObjectPlc;
    PlcTextName : string;
    LastPlcN, LastPlcL, LastPlcRung : Integer;
    ViewItemForm : TPlcViewItemForm;
    Access : TAccessLevels;
    ScrollBox : TScrollBox;
    PB : TPaintBox;
    Printing : Boolean;
    ActiveColor, InactiveColor, TextColor : TColor;
    ITEM_DISPLAY_WIDTH : Integer;
    ITEM_DISPLAY_HEIGHT : Integer;
    procedure PBPaint(Sender : TObject);
  protected
    procedure ItemPaint(Sender : TObject);
    procedure Resize; override;
    procedure Callback (aTag : TAbstractTag);
    procedure PrintCallback(aTag : TAbstractTag);
    procedure PrintPlc;
    procedure Paint; override;
    procedure DisplayViewItem;
    procedure UserAccessChange(aTag : TAbstractTag); override;
    procedure DoMouseDown(Sender: TObject; Button: TMouseButton;
          Shift: TShiftState; X, Y: Integer);
  public
    constructor Create(aOwner : TComponent); override;
    procedure InstallComponent(Params : TParamStrings); override;
    procedure Installed; override;
  end;

implementation

uses Printers;

ResourceString
  ObjectPlcNotInstalled = 'Object PLC NOT INSTALLED';
  AbandonChangesToPLC   = 'Do You Wish To Save Changes To PLC %s';
  ForcesActiveContinueLang = 'Forces Are Active; Do you want to lose them';

const
  TitleHeight = 24;
  FaultHeight = 24;
  PosiWidth   = 100;
  PlcName     = 'Unit';
  PlcExt      = '.Plc';


constructor TObjectPlcText.Create(AOwner : TComponent);
var LC : TListColumn;
begin
  inherited Create(Aowner);

  CreateDirectory(PChar(ProfilePath + PlcPath), nil);
  Text := TPlusMemo.Create(self);
  Text.Parent   := Self;
  Text.Font.Name := 'Courier New';
  Text.Font.Style := [];
  Text.Font.Size := 12;
  Text.OnKeyDown := TextKeyDown;
  Text.WordWrap := False;
  Text.HideSelection := False;
  ApplyKeyWords;


  Title := TPanel.create(Self);
  Title.parent := Self;
  Title.bevelOuter := bvLowered;

  Fault := TPanel.create(Self);
  Fault.parent := Self;
  Fault.bevelOuter := bvLowered;
  Fault.font.color := clRed;

  SearchBox := TEdit.Create(Self);
  SearchBox.Parent := Fault;
  SearchBox.Visible := False;
  SearchBox.Align := alRight;
  SearchBox.Font.Name := Text.Font.Name;
  SearchBox.OnChange := SearchChange;

  SearchBox.OnKeyDown := SearchKeyDown;
  SearchBox.OnKeyPress := SearchKeyPress;
  SearchBox.OnExit := SearchExit;

  Posi := TPanel.create(Self);
  Posi.parent := Self;
  Posi.bevelOuter := bvLowered;
  Posi.font.color := clRed;

  ListView := TListView.Create(Self);
  ListView.Parent := Self;
  ListView.Visible := False;
  ListView.Align := alClient;
  ListView.ViewStyle := vsReport;
  ListView.GridLines := True;

  LC := ListView.Columns.Add;
  LC.Caption := 'PLC';
  LC.Width := 220;
  LC := ListView.Columns.Add;
  LC.Caption := 'Loaded';
  LC.Width := 60;
  LC := ListView.Columns.Add;
  LC.Caption := 'Running';
  LC.Width := 60;
  LC := ListView.Columns.Add;
  LC.Caption := 'Data Count';
  LC.Width := 60;
  LC := ListView.Columns.Add;
  LC.Caption := 'Force Count';
  LC.Width := 60;
  LC := ListView.Columns.Add;
  LC.Caption := 'Lines';
  LC.Width := 60;
  LC := ListView.Columns.Add;
  LC.Caption := 'Rungs';
  LC.Width := 60;
end;

destructor TObjectPlcText.Destroy;
begin
  ListView.Free;
  Posi.Free;
  Searchbox.Free;
  Title.Free;
  Fault.Free;
  Text.Free;
  inherited;
end;

procedure TObjectPlcText.ApplyKeyWords;
var KWL : TKeyWordList;
begin
  Text.Keywords.BeginUpdate;
  KWL := Text.Keywords;
  KWL.AddKeyWord( 'if', [woWholeWordsOnly], [fsBold], 1, crIBeam, clWhite, clBlack);
  KWL.AddKeyWord( 'ld', [woWholeWordsOnly], [fsBold], 1, crIBeam, clWhite, clBlack);
  KWL.AddKeyWord( 'and', [woWholeWordsOnly], [fsBold], 1, crIBeam, clWhite, clBlack);
  KWL.AddKeyWord( 'or', [woWholeWordsOnly], [fsBold], 1, crIBeam, clWhite, clBlack);
  KWL.AddKeyWord( 'out', [woWholeWordsOnly], [fsBold], 1, crIBeam, clWhite, clBlack);
  KWL.AddKeyWord( 'nif', [woWholeWordsOnly], [fsBold], 1, crIBeam, clWhite, clBlack);
  KWL.AddKeyWord( 'nld', [woWholeWordsOnly], [fsBold], 1, crIBeam, clWhite, clBlack);
  KWL.AddKeyWord( 'nand', [woWholeWordsOnly], [fsBold], 1, crIBeam, clWhite, clBlack);
  KWL.AddKeyWord( 'nor', [woWholeWordsOnly], [fsBold], 1, crIBeam, clWhite, clBlack);
  KWL.AddKeyWord( 'nout', [woWholeWordsOnly], [fsBold], 1, crIBeam, clWhite, clBlack);

  KWL.AddKeyWord( 'local', [woWholeWordsOnly], [fsBold], 1, crIBeam, clWhite, clBlue);
  KWL.AddKeyWord( 'implementation', [woWholeWordsOnly], [fsBold], 1, crIBeam, clWhite, clBlue);
  KWL.AddKeyWord( 'unit', [woWholeWordsOnly], [fsBold], 1, crIBeam, clWhite, clBlue);
  KWL.AddKeyWord( 'end', [woWholeWordsOnly], [fsBold], 1, crIBeam, clWhite, clBlue);
  Text.Keywords := KWL;
  Text.ReApplyKeywords;
  Text.ApplyKeyWords := True;

  Text.StartStopKeys.AddStartStopKey('@',' ',[],[fsBold],1,crIBeam,clWhite,clMoneyGreen,True);
  Text.StartStopKeys.AddStartStopKey('$',' ',[],[fsBold],1,crIBeam,clWhite,clBlue,True);
  Text.StartStopKeys.AddStartStopKey('{', '}', [], [fsBold], 1, crDefault, clWhite, clGreen, False);
  Text.StartStopKeys.AddStartStopKey('"', '"', [], [fsBold], 1, crDefault, clWhite, $404090, False);
  Text.StartStopKeys.AddStartStopKey('//', '', [], [fsBold], 1, crDefault, clWhite, clGreen, True);
  Text.ApplyStartStopKeys := True;

  Text.Keywords.EndUpdate;
end;

procedure TObjectPlcText.TextKeyDown(Sender : TObject;  var Key: Word; Shift: TShiftState);
begin
  if (Key = VK_F) and (Shift = [ssCtrl]) then begin
//    SearchBox.Text := '';
    SearchBox.Visible := True;
    SearchBox.SetFocus;
    Key := 0;
  end;
end;

procedure TObjectPlcText.SearchKeyDown(Sender : TObject;  var Key: Word; Shift: TShiftState);
begin
  case Key of
    VK_UP, VK_LEFT, VK_RIGHT, VK_DOWN : begin
      SearchBox.Visible := False;
      Text.SetFocus;
    end;

    VK_F : if Shift = [ssCtrl] then begin
      Text.FindText(SearchBox.Text, True, False, False);
    end;
  end;
  Text.ScrollInView;
end;

procedure TObjectPlcText.SearchKeyPress(Sender: TObject; var Key: Char);
begin
  if Key = #6 then
    Key := #0;
end;

procedure TObjectPlcText.SearchExit(Sender : TObject);
begin
  SearchBox.Visible := False;
end;

procedure TObjectPlcText.SearchChange;
begin
  Text.SelCol := 0;
  Text.SelLine := 0;
  Text.FindText(SearchBox.Text, True, False, False);
  Text.ScrollInView;
end;

procedure TObjectPlcText.Open;
begin
{  if not Visible then
    DisplayTextPlc(PlcN.AsInteger); }

  inherited;
end;

procedure TObjectPlcText.DoSummaryClick(Sender : TObject);
begin
  if ListView.ItemIndex <> -1 then begin
    if Text.Enabled then begin
      DisplayTextPlc(ListView.ItemIndex);
      Text.Visible := True;
      Text.SetFocus;
      DoSummaryIntl(False);
    end;
  end;
end;

procedure TObjectPlcText.DoSummary(aTag : TAbstractTag);
begin
  if ATag.AsBoolean then
    Exit;
  if Visible then begin
    if not ListView.Visible then begin
      DoSummaryIntl(True);
    end else begin
      DoSummaryIntl(False);
    end;
  end;
end;

procedure TObjectPlcText.DoSummaryIntl(Show: Boolean);
var LI : TListItem;
    I : Integer;
begin
  if Show then begin
    ListView.Items.Clear;
    ListView.OnDblClick := DoSummaryClick;

    for I := 0 to MAX_NBR_OBJECT_PLC - 1 do begin
      LI := ListView.Items.Add;
      LI.Caption := Format(PLC.Names[I], [I]);
      LI.SubItems.Add(BooleanIdent[PLC.Resident[I]]);
      LI.SubItems.Add(BooleanIdent[PLC.Active[I]]);
      LI.SubItems.Add(IntToStr(PLC.DataCount[I]));
      LI.SubItems.Add(IntToStr(PLC.ForceCount[I]));
      //LI.SubItems.Add(IntToStr(PLC.LineCount[I])); ????? FixMe
      LI.SubItems.Add(IntToStr(PLC.RungCount[I]));
    end;
    Text.Visible := False;
    ListView.Visible := True;
  end else begin
    ListView.Visible := False;
    Text.Visible := True;
    Text.SetFocus;
  end;
  SummaryVisible.AsBoolean:= ListView.Visible;
end;

procedure TObjectPlcText.InstallComponent(Params : TParamStrings);
var i    : integer;
begin
  inherited installComponent(Params);
  ManageEdit := True;

  Text.Enabled := False;
  Text.Visible := False;

  PlcN := TagPublish(Name + 'PLC', TIntegerTag);
  PlcL := TagPublish(Name + 'Line', TIntegerTag);

  PlcCompile := TagPublish(Name + 'Compile', TIntegerTag);

  AllocMemCount := TagPublish('AllocMemCount', TIntegerTag);
  AllocMemSize := TagPublish('AllocMemSize', TIntegerTag);
  TagCount     := TagPublish('AllocTagCount', TIntegerTag);
  PlcItemCount := TagPublish('AllocPlcItemCount', TIntegerTag);
  SummaryVisible:= TagPublish(Name + 'InSummary', TIntegerTag);

  Primary := Text;
  InstallKeySet(Params.ReadKeys);

  for i := 0 to  MAX_NBR_OBJECT_PLC - 1 do begin
    if Params.ReadInteger('PLCload' + IntToStr(i), 1) <> 0 then
         Download[i] := True
    else Download[i] := False;
  end;
end;

procedure TObjectPlcText.UserAccessChange(aTag : TAbstractTag);
var Permissive : Boolean;
    I : Integer;
    Total : Integer;
begin
  inherited;
  Permissive := AccessLevelMaintenance in Access;

  Access := TAccessLevels(Byte(aTag.AsInteger));
  Text.Enabled :=  AccessLevelMaintenance in Access;
  Text.Visible:= Text.Enabled;

  if not(AccessLevelMaintenance in Access) and Permissive then begin
    Total := 0;
    for I := 0 to 31 do
      Total := Total + Plc.ForceCount[I];

    if Total > 0 then begin
      if Acknowledge.AreYouSure(ForcesActiveContinueLang) then
        Plc.DeleteAllForces
      else
        SysObj.FaultInterface.SetFault(Self, PlcFaults[plcfForceJoking], [], nil);
    end;
  end;
end;


procedure TObjectPlcText.RungUp(aTag : TAbstractTag);
var
  Local : TPoint;
begin
  if ATag.AsBoolean then
    Exit;
  Local.X := Text.SelCol;
  Local.Y := Text.SelLine;
  Local.Y := Plc.GetRungFromLine(PlcN.AsInteger, Local.Y) - 1;
  Local.Y := Plc.GetLineFromRung(PlcN.AsInteger, Local.Y);

  Text.SelCol := Local.X;
  Text.SelLine := Local.Y;
  Text.ScrollInView;

  PlcL.AsInteger := Local.Y;
end;

procedure TObjectPlcText.RungDown(aTag : TAbstractTag);
var
  Local : TPoint;
begin
  if ATag.AsBoolean then
    Exit;
  Local.X := Text.SelCol;
  Local.Y := Text.SelLine;
  Local.Y := Plc.GetRungFromLine(PlcN.AsInteger, Local.Y) + 1;
  Local.Y := Plc.GetLineFromRung(PlcN.AsInteger, Local.Y);

  Text.SelCol := Local.X;
  Text.SelLine := Local.Y;
  Text.ScrollInView;

  PlcL.AsInteger := Local.Y;
end;


procedure TObjectPlcText.EnableDisableEvent(aTag : TAbstractTag);
var I : Integer;
    First : Boolean;
begin
  if aTag.AsBoolean then begin
    if AccessLevelOEM1 in SysObj.Comms.AccessLevels then
      Plc.Clean;
  end else begin
    First := True;
    for i := 0 to MAX_NBR_OBJECT_PLC - 1 do begin
      if Download[i] then begin
        if not Plc.LoadFromFile(I, PlcFileName(I), First) then
          SysObj.FaultInterface.SetFault(Self, CoreFaults[cfSysStopCycle], [Plc.FaultString], nil);
        First := False;
      end;
    end;
    Plc.Initialize;
  end;
end;

procedure TObjectPlcText.Installed;
var i : integer;
    First : Boolean;
begin
  inherited installed;
  next := TagEvent(name + 'Next', edgeFalling, TMethodTag, callback);
  prev := TagEvent(name + 'Prev', edgeFalling, TMethodTag, callback);
  down := TagEvent(name + 'Down', edgeFalling, TMethodTag, callback);
  TagEvent(Name + 'RungUp', edgeFalling, TMethodTag, RungUp);
  TagEvent(Name + 'RungDown', edgeFalling, TMethodTag, RungDown);
  SummaryTag := TagEvent(Name + 'Summary', edgeFalling, TMethodTag, DoSummary);

  TagEvent(Name + 'Disable', edgeChange, TMethodTag, EnableDisableEvent);
  upda := TagEvent(nameFlashTimer, edgeChange, TIntegerTag, callback);

  if SysObj.IO.PLC = nil then begin
    Fault.caption := ObjectPlcNotInstalled;
    Plc := nil;
    exit;
  end;

  if SysObj.IO.PLC is TObjectPlc then
    PLC := SysObj.IO.PLC as TObjectPlc;

  First := True;
  for i := 0 to MAX_NBR_OBJECT_PLC - 1 do begin
    if Download[i] then begin
      if not Plc.LoadFromFile(I, PlcFileName(I), First) then
        SysObj.FaultInterface.SetFault(Self, CoreFaults[cfSysStopCycle], [Plc.FaultString], nil);
      First := False;
    end;
  end;

  Plc.Initialize;
  DisplayTextPlc(0);
end;

function CheckAccessLevel(Sender : TComponent; Access : TAccessLevels) : Boolean;
begin
  Result := False;
  if AccessLevelMaintenance in Access then
    Result := True
  else
    SysObj.FaultInterface.SetFault(InstalledPlc, PlcFaults[plcfEditingRequiresMaint], [], nil);
end;

procedure TObjectPlcText.Callback(aTag : TAbstractTag);
begin
  if aTag = upda then begin
    if AllocMemCount.AsInteger <> System.AllocMemCount then
      AllocMemCount.AsInteger := System.AllocMemCount;
    if AllocMemSize.AsInteger <> System.AllocMemSize then
      AllocMemSize.AsInteger := System.AllocMemSize;
    if TagCount.AsInteger <> TAbstractTag.TagCount then
      TagCount.AsInteger := TAbstractTag.TagCount;
    if PlcItemCount.AsInteger <> TPlcItem.PlcItemCount then
      PlcItemCount.AsInteger := TPlcItem.PlcItemCount;

    if not Assigned(PLC) or not Visible then
      Exit;
  end
  else if not ATag.AsBoolean then begin
    if ListView.Visible then
      DoSummaryIntl(False);

    if aTag = next then begin
      SavePlc;
      DisplayTextPlc((PlcN.AsInteger + 1) mod MAX_NBR_OBJECT_PLC);
    end else if aTag = prev then begin
      SavePlc;
      DisplayTextPlc((PlcN.AsInteger + MAX_NBR_OBJECT_PLC - 1) mod MAX_NBR_OBJECT_PLC);
    end else if aTag = down then begin
      if not CheckAccessLevel(Self, Access) then
        Exit;

      if PLC = nil then
        Exit;
      SavePlc;
      PlcCompile.AsBoolean := True;
      if not PLC.LoadFromFile(PlcN.AsInteger, PlcFileName(PlcN.AsInteger), True) then begin
        Fault.Caption := PLC.faultString;
        Text.SelCol := Plc.FaultPos.X;
        Text.SelLine := Plc.FaultPos.Y;
      end else
        Fault.caption := '';
      PlcCompile.AsBoolean := False;
      Plc.GenerateCrossReference(ProfilePath + PLCPath + 'XRef.Txt');
    end;
  end;

  Posi.Caption := Inttostr(Text.SelCol) + ',' +
                  Inttostr(Text.SelLine) + ':' +
                  Inttostr(PLC.DataCount[PlcN.AsInteger]);
  if Text.SelLine <> PlcL.AsInteger then
    PlcL.AsInteger := Text.SelLine;
end;

procedure TObjectPlcText.Closing;
var aText : string;
begin
  FmtStr(aText, AbandonChangesToPLC, [PlcShortFileName(PlcN.AsInteger)]);
  if Text.Modified and Acknowledge.AreYouSure(aText) then
    SavePlc;
end;

procedure TObjectPlcText.Resize;
begin
  inherited resize;
  if installstate = installStateIdle then exit;

  Text.Align := alClient;
  Text.top    := CNCBorder + TitleHeight;
  Text.left   := CNCBorder;

  Title.height := TitleHeight;
  Title.Align := alTop;
  Title.top    := CNCBorder;
  Title.left   := CNCBorder;

  Fault.height := FaultHeight;
  Fault.Align := alBottom;

  Posi.height  := FaultHeight;
  Posi.top     := Fault.top;
  Posi.left    := CNCBorder;
  Posi.width   := PosiWidth;
end;

procedure TObjectPlcText.SavePlc;
var Stream : TFileStream;
begin
  try
    Stream := TFileStream.Create(PlcFileName(PlcN.AsInteger), fmCreate);
  except
    Exit;
  end;

  try
    Text.SaveToStream(Stream); //.saveToFile(PlcFileName(PlcN.AsInteger));
    Text.Modified := False;
  finally
    Stream.Free;
  end;
end;

procedure TObjectPlcText.DisplayTextPlc(aPLC : Integer);
var Stream : TFileStream;
begin
  PlcN.AsInteger := aPLC;

  Title.caption := PlcShortFileName(aPlc);

  try
    Stream := TFileStream.Create(PlcFileName(aPlc), fmOpenRead);
  except
    CreateTemplate(aPlc);
    Exit;
  end;

  try
    Text.LoadFromStream(Stream);
    Text.Modified := False;
  finally
    Stream.Free;
  end;
end;

procedure TObjectPlcText.CreateTemplate(aPlc : Integer);
begin
  if not assigned(Plc) then
    Exit;
  Text.Lines.Clear;
  Text.Lines.addStrings(Plc.template(aPlc));
end;

function  TObjectPlcText.PlcFileName(aPlc : Integer) : string;
begin
  result := ProfilePath + PlcPath + PlcName + inttostr(aPlc) + PlcExt;
end;

function TObjectPlcText.PlcShortFileName(aPlc : Integer) : string;
begin
  result := PlcPath + PlcName + inttostr(aPlc) + PlcExt;
end;

procedure TObjectPlcGraphics.Callback (aTag : TAbstractTag);
var I : Integer;
begin
  if PLC = nil then
    Exit;

  if Visible and not Printing then begin
    ViewItemForm.PlcN := PlcN.AsInteger;
    if PlcL.AsInteger <> LastPlcL then begin
      LastPlcL := PlcL.AsInteger;
      I := PLC.GetRungFromLine(PlcN.AsInteger, PlcL.AsInteger);
      if I <> LastPlcRung then begin
        LastPlcRung := I;
        PB.Invalidate;
      end;
    end else if PlcN.AsInteger <> LastPlcN then begin
      Resize;
      PB.Invalidate;
      LastPlcN := PlcN.AsInteger;
    end else if aTag = PlcC then begin
      ViewItemForm.ItemViewPanel.Visible := False;
      ViewITemForm.Element := nil;
      PB.Invalidate;
    end else begin
      I := PLC.GetRungFromLine(PlcN.AsInteger, PlcL.AsInteger);
      if Assigned(ViewItemForm.Element) then
        try
          ViewItemForm.UpdateItems
        except
          on E : Exception do begin
            SysObj.FaultInterface.SetFault(Self, CoreFaults[cfSysWarning], [E.Message], nil);
          end;
        end
      else
        PLC.ClientPaint(PlcN.AsInteger, I, PB.Canvas, ITEM_DISPLAY_WIDTH, ITEM_DISPLAY_HEIGHT);
    end;
  end;
end;

procedure TObjectPlcGraphics.UserAccessChange(aTag : TAbstractTag);
begin
  inherited;
  Access := TAccessLevels(Byte(aTag.AsInteger));
  if not (AccessLevelMaintenance in Access) then begin
    ViewItemForm.ItemViewPanel.Visible := False;
    ViewItemForm.Element := nil;
    Repaint;
  end;
end;


procedure TObjectPlcGraphics.Paint;
begin
  inherited Paint;
end;

procedure TObjectPlcGraphics.PBPaint(Sender : TObject);
var Rung : Integer;
begin
  PB.Canvas.Brush.Color := Color;
  PB.Canvas.Pen.Color := Color;
  PB.Canvas.Rectangle(0, 0, PB.Width, PB.Height);
  Rung := PLC.GetRungFromLine(PlcN.AsInteger, PlcL.AsInteger);
  PLC.ClientPaint(PlcN.AsInteger, Rung, PB.Canvas, ITEM_DISPLAY_WIDTH, ITEM_DISPLAY_HEIGHT);
end;

procedure TObjectPlcGraphics.Resize;
begin
  inherited Resize;
  PB.Height := ScrollBox.ClientHeight;
  if Assigned(PLC) then
    PLC.SetRightMargin(ScrollBox.ClientWidth, ITEM_DISPLAY_WIDTH);
end;

constructor TObjectPlcGraphics.Create(aOwner : TComponent);
begin
  inherited Create(aOwner);

  Color := clBlack;
  ScrollBox := TScrollBox.Create(Self);
  ScrollBox.Parent := Self;
  ScrollBox.Align := alClient;
  PB := TPaintBox.Create(Self);
  PB.Parent := ScrollBox;
  PB.Width := 1600;
  PB.Left := 0;
  PB.Top := 0;
  PB.OnPaint := PBPaint;

  PB.OnMouseDown := DoMouseDown;
  ITEM_DISPLAY_WIDTH := 90;
  ITEM_DISPLAY_HEIGHT := 50;
end;

procedure TObjectPlcGraphics.DoMouseDown(Sender: TObject; Button: TMouseButton;
          Shift: TShiftState; X, Y: Integer);
var
  Rung : Integer;
begin
  case Button of
    mbRight{, mbLeft} : begin
      Rung := PLC.GetRungFromLine(PlcN.AsInteger, PlcL.AsInteger);
      ViewItemForm.Element := PLC.FindItem(PlcN.AsInteger, X, Y, Rung, ITEM_DISPLAY_WIDTH, ITEM_DISPLAY_HEIGHT);
    end;
  end;
  DisplayViewItem;
end;

procedure TObjectPlcGraphics.InstallComponent(Params : TParamStrings);
begin
  inherited InstallComponent(Params);

  ViewItemForm := TPlcViewItemForm.Create(Self);
  ViewItemForm.ItemViewPanel.Parent := ScrollBox;
  ViewItemForm.ItemViewPanel.Visible := False;
  ViewItemForm.ItemViewPanel.Align := alLeft;
  ViewItemForm.ItemViewPanel.Width := 600;

  ManageEdit := True;
  PlcTextName := Params.ReadString('ObjectPlcText', 'ObjectPlcText');
  ParentFont := false;
  ActiveColor := Params.ReadColour('ActiveColor', clYellow);
  InactiveColor := Params.ReadColour('InactiveColor', clBlue);
  TextColor := Params.ReadColour('TextColor', clYellow);
  PB.Color := Color;
  ITEM_DISPLAY_WIDTH := Params.ReadInteger('ItemWidth', 90);
  ITEM_DISPLAY_HEIGHT := Params.ReadInteger('ItemHeight', 50);
end;

procedure TObjectPlcGraphics.Installed;
begin
  inherited installed;

  PlcN := TagEvent(PlcTextName + 'Plc', edgeChange, TIntegerTag, CallBack);
  PlcL := TagEvent(PlcTextName + 'Line', edgeChange, TIntegerTag, CallBack);
  PlcC := TagEvent(PlcTextName + 'Compile', edgeRising, TIntegerTag, CallBack);
  TagEvent(NameDisplaySweep, edgeChange, TIntegerTag, callback);
  TagEvent(Name + 'Print', EdgeFalling, TMethodTag, PrintCallback);

  if SysObj.IO.Plc is TObjectPlc then
    Plc := SysObj.IO.plc as TObjectPlc;

  ViewItemForm.PLC := Plc;

  if Assigned(Plc) then begin
    PLC.TextColor := TextColor;
    PLC.ActiveColor := ActiveColor;
    PLC.InactiveColor := InactiveColor;
    PLC.X_MARGIN := -2;
    PLC.Y_MARGIN := 10;
  end;
end;

procedure TObjectPlcGraphics.ItemPaint(Sender : TObject);
begin
  ViewItemForm.UpdateItems;
end;

procedure TObjectPlcGraphics.DisplayViewItem;
begin
  if CheckAccessLevel(Self, Access) then begin
    if ViewItemForm.Element <> nil then begin
      ViewItemForm.ItemViewPanel.Visible := True;
    end;
  end else
    ViewItemForm.Element := nil;
end;

procedure TObjectPlcGraphics.PrintPlc;
var IWidth, IHeight : Integer;
    Rung, LastRung : Integer;
begin
  Printer.Title := Format('ObjectPlc Unit %s (%d)', [Plc.Names[PlcN.AsInteger], PlcN.AsInteger]);
  Printer.BeginDoc;
  try
    IWidth := Printer.PageWidth div 14;
    IHeight := IWidth * 5 div 7;
    PLC.SetRightMargin(Printer.PageWidth - (IWidth * 2), IWidth);
    PLC.TextColor := clBlack;
    PLC.ActiveColor := clBlack;
    PLC.InactiveColor := clBlack;
    PLC.X_MARGIN := IWidth;
    PLC.Y_MARGIN := Printer.PageHeight div 10;
    Rung := 1;
    LastRung := 1;
    while Rung <> -1 do begin
      Printer.Canvas.Font.Color := clBlack;
      Printer.Canvas.Font.Size := 12;
      Printer.Canvas.Brush.Color := clWhite;
      Printer.Canvas.TextOut(PLC.X_MARGIN, PLC.Y_MARGIN div 2, Printer.Title + '    Page ' + IntToStr(Printer.PageNumber));
      PLC.ClientPaint(PlcN.AsInteger, Rung, Printer.Canvas, IWidth, IHeight);
      Printer.NewPage;
      if Rung = LastRung then
        Exit;
      LastRung := Rung;
    end;
  finally
    PLC.TextColor := TextColor;
    PLC.ActiveColor := ActiveColor;
    PLC.InactiveColor := InactiveColor;
    PLC.X_MARGIN := -2;
    PLC.Y_MARGIN := 10;
    Resize;
    Printer.EndDoc;
  end;
end;

procedure TObjectPlcGraphics.PrintCallback(aTag : TAbstractTag);
var PrintDlg : TPrintDialog;
begin
  if Printers.Printer.Printers.Count > 0 then begin
    PrintDlg := TPrintDialog.Create(Application);
    PrintDlg.Options := [poWarning];
    try
      with PrintDlg do begin
        if Execute then begin
          Printing := True;
          try
            PrintPlc;
          finally
            Printing := False;
          end;
        end;
      end;
    finally
      PrintDlg.Free;
    end;
  end;
end;

begin
  RegisterCNCClass(TObjectPlcText);
  RegisterCNCClass(TObjectPlcGraphics);
end.
