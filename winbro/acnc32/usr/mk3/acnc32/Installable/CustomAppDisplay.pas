unit CustomAppDisplay;

interface

uses Classes, SysUtils, Windows, CoreCNC, CNCTypes, CNC32, StreamTokenizer, CNCUtils,
     Graphics, Named;

type
  TCustomApplicationDisplay = class(TAbstractDisplay)
  private
    StatusList : TList;
    ColCount, RowCount : Integer;
    procedure DataPoll(aTag : TAbstractTag);
    procedure ReadConfig(S : TStreamQuoteTokenizer);
  protected
    procedure Resize; override;
  public
    constructor Create(aOwner : TComponent); override;
    procedure InstallComponent(Params : TParamStrings); override;
    procedure Installed; override;
  end;

implementation

const
  DataTypeFloat = 0;
  DataTypeString = 1;

type
  TCellData = class
    ActiveColor : TColor;
    InactiveColor : TColor;
    AlarmColor : TColor;
    ActiveTagName : string;
    TagName : string;
    ActiveTag : TAbstractTag;
    Tag : TAbstractTag;
    TagType : TStatusModifier;
    ActiveText : string;
    InactiveText : string;
    Status  : TStatusControl;
    LowerLimit : Double;
    UpperLimit : Double;
    LimitsActive : Boolean;
    P : TPoint;
    Divisor : Double;
    FormatString: string;
  end;

{ TCustomApplicationDisplay }

constructor TCustomApplicationDisplay.Create(aOwner: TComponent);
begin
  inherited;
  StatusList := TList.Create;
end;


procedure TCustomApplicationDisplay.InstallComponent(Params: TParamStrings);
var S : TStringList;
    QT : TStreamQuoteTokenizer;
begin
  inherited;
  S := TStringList.Create;
  QT := TStreamQuoteTokenizer.Create;
  try
    ReadLive(S, Name);
    QT.QuoteChar := '''';
    QT.Seperator := '[],{}';
    QT.SetStream(TStringStream.Create(S.Text));
    ReadConfig(QT);
  finally
    QT.Free;
    S.Free;
  end;
end;

procedure TCustomApplicationDisplay.ReadConfig(S : TStreamQuoteTokenizer);
var Token : string;
    ActiveColor, InactiveColor, AlarmColor : TColor;

  procedure ReadCell;
  var Cell : TCellData;
  begin
    Cell := TCellData.Create;
    Cell.ActiveColor := ActiveColor;
    Cell.InactiveColor := InactiveColor;
    Cell.AlarmColor := AlarmColor;
    Cell.LimitsActive := False;
    Cell.Status := TStatusControl.Create(Self);
    Cell.Divisor := 1;
    Cell.Status.Parent := Self;
    StatusList.Add(Cell);

    S.ExpectedToken('[');
    Cell.P.X := S.ReadInteger;
    S.ExpectedToken(',');
    Cell.P.Y := S.ReadInteger;
    S.ExpectedToken(']');
    S.ExpectedTokens(['=', '{']);
    Token := LowerCase(S.Read);
    while not S.EndOfStream do begin
      if Token = '}' then
        Exit
      else if Token = 'activecolor' then
        Cell.ActiveColor := S.ReadColorAssign
      else if Token = 'inactivecolor' then
        Cell.InactiveColor := S.ReadColorAssign
      else if Token = 'alarmcolor' then
        Cell.AlarmColor := S.ReadColorAssign
      else if Token = 'activetag' then
        Cell.ActiveTagName := S.ReadStringAssign
      else if Token = 'tag' then
        Cell.TagName := S.ReadStringAssign
      else if Token = 'tagtype' then
        Cell.TagType := TStatusModifier(S.ReadIntegerAssign)
      else if Token = 'activetext' then
        Cell.ActiveText := S.ReadStringAssign
      else if Token = 'inactivetext' then
        Cell.InactiveText := S.ReadStringAssign
      else if Token = 'upperlimit' then
        Cell.UpperLimit := S.ReadDoubleAssign
      else if Token = 'lowerlimit' then
        Cell.LowerLimit := S.ReadDoubleAssign
      else if Token = 'limitsactive' then
        Cell.LimitsActive := S.ReadBooleanAssign
      else if Token = 'divisor' then
        Cell.Divisor := S.ReadDoubleAssign
      else if Token = 'formatstring' then
        Cell.FormatString:= S.ReadStringAssign
      else
        raise Exception.CreateFmt('Unexpected token %s', [Token]);
      Token := LowerCase(S.Read);
    end;
  end;


begin
  S.ExpectedTokens([Name, '=', '{']);
  Token := LowerCase(S.Read);
  ActiveColor := clBlue;
  InactiveColor := clGreen;
  AlarmColor := clRed;
  RowCount := 3;
  ColCount := 4;

  while not S.EndOfStream do begin
    if Token = '}' then
      Exit
    else if Token = 'colcount' then
      ColCount := S.ReadIntegerAssign
    else if Token = 'rowcount' then
      RowCount := S.ReadIntegerAssign
    else if Token = 'activecolor' then
      ActiveColor := S.ReadColorAssign
    else if Token = 'inactivecolor' then
      InactiveColor := S.ReadColorAssign
    else if Token = 'alarmcolor' then
      AlarmColor := S.ReadColorAssign
    else if Token = 'cell' then
      ReadCell
    else
      raise Exception.CreateFmt('Unexpected token %s', [Token]);
    Token := LowerCase(S.Read);
  end;
end;

procedure TCustomApplicationDisplay.Installed;
var I : Integer;
    Cell : TCellData;
begin
  inherited;
  TagEvent(Name, edgeFalling, TMethodtag, Activate);
  TagEvent(NameDisplaySweep, EdgeChange, TMethodTag, DataPoll);

  for I := 0 to StatusList.Count - 1 do begin
    Cell := TCellData(StatusList[I]);
    if Cell.Divisor = 0 then
      Cell.Divisor := 1;


    Cell.Status.Modifier := Cell.TagType;
    Cell.Status.ColourActive := Cell.ActiveColor;
    Cell.Status.ColourInActive := Cell.InactiveColor;
    Cell.Status.ColourAlarm := Cell.AlarmColor;
    Cell.Status.TextActive := Cell.ActiveText;
    Cell.Status.TextInactive := Cell.InactiveText;
    Cell.Status.Divisor := Cell.Divisor;
    if Cell.LimitsActive then begin
      Cell.Status.LimitUpper := Cell.UpperLimit;
      Cell.Status.LimitLower := Cell.LowerLimit;
    end else begin
      Cell.Status.LimitUpper := 100000000;
      Cell.Status.LimitLower := -100000000;
    end;
    if Cell.TagName <> '' then begin
      if Cell.TagType = smString then
        Cell.Tag := TagEvent(Cell.TagName, EdgeChange, TStringTag, nil)
      else if (Cell.TagType = smInteger) or (Cell.TagType = smMSToHMS) then
        Cell.Tag := TagEvent(Cell.TagName, EdgeChange, TIntegerTag, nil)
      else
        Cell.Tag := TagEvent(Cell.TagName, EdgeChange, TDoubleTag, nil)
    end;
    if Cell.ActiveTagName <> '' then begin
      Cell.ActiveTag := TagEvent(Cell.ActiveTagName, EdgeChange, TMethodTag, nil);
    end;
  end;
end;

procedure TCustomApplicationDisplay.Resize;
var Cell : TCellData;
    I : Integer;
begin
  inherited;
  for I := 0 to StatusList.Count - 1 do begin
    Cell := TCellData(StatusList[I]);
    Cell.Status.Left := (Width * Cell.P.X) div ColCount;
    Cell.Status.Top := (Height * Cell.P.Y) div RowCount;
    Cell.Status.Width := Width div ColCount;
    Cell.Status.Height := Height div RowCount;
  end;
end;

procedure TCustomApplicationDisplay.DataPoll(aTag: TAbstractTag);
var Cell : TCellData;
    I : Integer;
begin
  for I := 0 to StatusList.Count - 1 do begin
    Cell := TCellData(STatusList[I]);
    case Cell.TagType of

      smString : begin
        if Assigned(Cell.Tag) then
          Cell.Status.TextActive := Cell.Tag.AsString;
      end;
    else
      if Assigned(Cell.Tag) then
        Cell.Status.Value := Cell.Tag.AsDouble;
    end;

    if Assigned(Cell.ActiveTag) then begin
      if Cell.Status.Active <> Cell.ActiveTag.AsBoolean then begin
        Cell.Status.Active := Cell.ActiveTag.AsBoolean;
      end;
    end;
  end;
end;


initialization
  RegisterCNCClass(TCustomApplicationDisplay);
end.
