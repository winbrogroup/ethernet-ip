unit SoftkeyTree;

interface

uses SysUtils, Classes, CoreCNC, CNC32, CNCtypes, SoftkeyTreeData, StreamTokenizer,
     ExtCtrls, Controls, Windows, Named, Graphics, FaultRegistration;

type
  TSoftkey = class;
  TSoftkeyTreeStub = class(TCustomPanel)
  private
    Config: T_SoftkeyTree;
    Softkeys: TStringList;
    PressedKey: TSoftkey; // used by softkey to ensure consistency
    AcncClient: TAbstractDisplay;
    CurrentFault: FHandle;
    function ResetFault(Fid: FHandle): Boolean;
    procedure PrimaryDisplayChange(ATag: TAbstractTag);
    procedure EnableDisableKeyset(ATag: TAbstractTag);
    procedure EnableDisableKey(ATag: TAbstractTag);
    procedure KeyLight(ATag: TAbstractTag);
    procedure ResetKeys(ATag: TAbstractTag);
    procedure ChangeSoftkeySet(Sender: TObject);
    procedure ReloadConfiguration(Stream: TStream);
  protected
    procedure Resize; override;
    procedure Clean;
  public
    procedure InstallComponent(Stream: TStream);
    procedure Installed;
    procedure CheckInstallation;
    procedure KeyEvent(Shift, Control: Boolean; Key: Integer; Edge: TEdgeType);
  end;

  TSoftkey = class(TCustomPanel)
  private
    FText: string;
    Strings: TStringList;
    FLogic: T_Softkey;
    FActiveDisplay: T_SoftkeyDisplay;
    Down: Boolean;
    Config: T_SoftkeyTree;
    FSoftkeyTree: TSoftkeyTreeStub;
    FIndex: Integer;
    procedure SetText(const Value: string);
    procedure SetLogic(const Value: T_Softkey);
    procedure SetActiveDisplay(const Value: T_SoftkeyDisplay);
    procedure PaintBackground(Color, Hi, Lo: TColor);
  protected
    BitMapOn: TBitMap;
    BitMapOff: TBitMap;
    BitMapDisabled: TBitMap;
    procedure Paint; override;
    procedure MouseDown(Button: TMouseButton; Shift: TShiftState;
      X, Y: Integer); override;
    procedure MouseUp(Button: TMouseButton; Shift: TShiftState;
      X, Y: Integer); override;
    procedure SetEnabled(Avalue: Boolean); override;
    procedure Press;
    procedure Release;
  public
    constructor Create(Aowner: TComponent); override;
    property SoftkeyTree: TSoftkeyTreeStub read FSoftkeyTree write FSoftkeyTree;
    property Text: string read FText write SetText;
    property Logic: T_Softkey read FLogic write SetLogic;
    property ActiveDisplay: T_SoftkeyDisplay read FActiveDisplay write SetActiveDisplay;
    property Index: Integer read FIndex;
  end;

  TSoftkeyTree = class(TAbstractDisplay)
  private
    Stub: TSoftkeyTreeStub;
    procedure ReloadConfiguration(ATag: TAbstractTag);
  public
    procedure InstallComponent(Params: TParamStrings); override;
    procedure Installed; override;
    procedure CheckInstallation; override;
    procedure SetEnabled(AValue: Boolean); override;
    procedure OpenkeySet(Name: string);
  end;

  TSoftkeyTreeCore = class(TAbstractSoftkey)
  private
    Stub: TSoftkeyTreeStub;
    procedure ReloadConfiguration(ATag: TAbstractTag);
  public
    procedure InstallComponent(Params: TParamStrings); override;
    procedure Installed; override;
    procedure CheckInstallation; override;
    procedure KeyEvent (key : integer; edge : TedgeType); override;
    procedure KeyChange (handle : word); override;
    procedure KeyClose (handle : word); override;
    procedure Reset; override; // invoke top layer keys
    procedure SetEnabled(AValue: Boolean); override;
    procedure OpenkeySet(Name: string);
  end;


implementation

var InstalledKeysets: TStringList;

{ TSoftkeyTree }

procedure TSoftkeyTreeStub.ChangeSoftkeySet(Sender: TObject);
var I: Integer;
    KS: T_SoftkeySet;
    K: T_Softkey;
    SK: TSoftkey;
    C: Integer;
begin
  KS:= Config.ActiveSet;

  AcncClient.Title:= KS.Title;
  if PressedKey <> nil then
    PressedKey.Release;

  for I:= 0 to Config.Count - 1 do begin
    SK:= TSoftkey(Softkeys.Objects[I]);
    SK.Logic:= nil;
    //SK.ActiveDisplay:= nil;
  end;

  for I:= 0 to KS.SoftkeyCount - 1 do begin
    K:= KS.Softkeys[I];
    C:= K.Key - Config.Basekey;
    if (C >= 0) and (C < Config.Count) then begin
      SK:= TSoftkey(Softkeys.Objects[C]);
      SK.Logic:= K;
      //SK.ActiveDisplay:= SK.ActiveDisplay;
    end;
  end;

  for I:= 0 to Config.Count - 1 do begin
    SK:= TSoftkey(Softkeys.Objects[I]);
    SK.Invalidate;
  end;
end;

procedure TSoftkeyTreeStub.CheckInstallation;
begin
  try
    Config.Compile;
  except
    on E: ECNCInstallFault do
      CurrentFault:= SysObj.FaultInterface.SetFault(AcncClient, CoreFaults[cfSysStopCycle], [Format(Name + 'Script Compile Failed [%s]', [E.message])], ResetFault);
      //raise ECNCInstallFault.CreateFmt(Name + 'Failed [%s] at line %d', [E.message, ST.Line]);
  end;
  Config.OnActiveSetChange:= ChangeSoftkeySet;
  Config.Reset;
  InstalledKeySets.AddObject(AcncClient.Name, Self);
end;

{ Cleans out the entire config, this must reverse all code in InstallComponent,
  Installed and CheckInstallation }
procedure TSoftkeyTreeStub.Clean;
var I, J: Integer;
    KS: T_SoftkeySet;
    K: T_Softkey;
begin
  for I:= 0 to Config.SoftkeySetCount - 1 do begin
    KS:= Config.SoftkeySets[I];
    if Assigned(KS.Disable) then
      KS.Disable.Tag.RemoveCallback(EnableDisableKeyset);

    if Assigned(KS.Light) then
      KS.Light.Tag.RemoveCallback(KeyLight);

    for J:= 0 to KS.SoftkeyCount - 1 do begin
      K:= KS.Softkeys[J];
      if Assigned(K.Disable) then begin
        K.Disable.Tag.RemoveCallback(EnableDisableKey);
      end;
      if Assigned(K.Light) then begin
        K.Light.Tag.RemoveCallback(KeyLight);
      end;
    end;
  end;

  while Softkeys.Count > 0 do begin
    TSoftkey(Softkeys.Objects[0]).Parent:= nil;
    TSoftkey(Softkeys.Objects[0]).Free;
    Softkeys.Delete(0);
  end;

  SoftKeys.Free;
  Config.Free;
end;

procedure TSoftkeyTreeStub.EnableDisableKey(ATag: TAbstractTag);
var I, J: Integer;
    K: T_Softkey;
    S: T_SoftkeySet;
begin
  for I:= 0 to Self.Config.SoftkeySetCount - 1 do begin
    S:= Config.SoftkeySets[I];
    S.CheckDisabled;
    for J:= 0 to S.SoftkeyCount - 1 do begin
      K:= S.Softkeys[J];
      K.CheckDisabled;
    end;
  end;
end;

procedure TSoftkeyTreeStub.EnableDisableKeyset(ATag: TAbstractTag);
begin
  EnableDisableKey(ATag);
end;

procedure TSoftkeyTreeStub.InstallComponent(Stream: TStream);
var ST: TStreamQuoteTokenizer;
    I: Integer;
    SK: TSoftkey;
    TmpStr: string;
begin
  SysObj.FaultInterface.FaultResetID(CurrentFault);
  CurrentFault:= 0;

  Softkeys:= TStringList.Create;
  Config:= T_SoftkeyTree.Create(AcncClient);
  ST:= TStreamQuoteTokenizer.Create;
  ST.SetStream(Stream);
  ST.QuoteChar:= '''';
  ST.Seperator:= '[],{};';
  ST.CommentChar:= '#';
  ST.ManageStream:= True;
  try
    Config.Read(ST);
  except
    on E: ECNCInstallFault do
      CurrentFault:= SysObj.FaultInterface.SetFault(AcncClient, CoreFaults[cfSysStopCycle], [Format(Name + 'Failed [%s] at line %d', [E.message, ST.Line])], ResetFault);
      //raise ECNCInstallFault.CreateFmt(Name + 'Failed [%s] at line %d', [E.message, ST.Line]);
  end;
  ST.ReleaseStream;
  ST.Free;

  ParentFont:= True;
  Font.Name:= Sysobj.FontName;
  Font.Size:= Config.Fontsize;

  for I:= 0 to Config.Count - 1 do begin
    SK:= TSoftkey.Create(Self);
    SK.Config:= Config;
    SK.SoftkeyTree:= Self;
    SK.Parent:= Self;
    SK.ParentFont:= False;
    SK.Font.Name:= SysObj.FontName;
    //SK.Canvas.Font.Name:= SysObj.FontName;
    SK.FIndex:= I + Config.Basekey;
    if Config.UseIndicators then begin
      TmpStr := 'F' + IntToStr(I);
      try
        SK.BitMapOn.LoadFromResourceName(Hinstance, TmpStr + 'ON');
        SK.BitMapOff.LoadFromResourceName(Hinstance, TmpStr + 'OFF');
      except
      end;
    end;

    if Assigned(Config.DefaultImages) then
      Config.DefaultImages.GetBitmap(SK.FIndex, SK.BitMapOff);
    if Assigned(Config.PressedImages) then
      Config.PressedImages.GetBitmap(SK.FIndex, SK.BitMapOn);
    if Assigned(Config.DisabledImages) then
      Config.DisabledImages.GetBitmap(SK.FIndex, SK.BitMapDisabled);

    Softkeys.AddObject('', SK);
  end;
end;

procedure TSoftkeyTreeStub.Installed;
var I, J: Integer;
    KS: T_SoftkeySet;
    K: T_Softkey;
    TW: TTagWrapper;
begin
  AcncClient.TagEvent('SoftkeyReset', EdgeChange, TIntegerTag, ResetKeys);

  for I:= 0 to Config.TagCount - 1 do begin
    TW:= Config.Tags[I];
    TW.Tag:= AcncClient.TagEvent( TW.TagName, EdgeChange, TIntegerTag, nil);
  end;

  for I:= 0 to Config.SoftkeySetCount - 1 do begin
    KS:= Config.SoftkeySets[I];
    if Assigned(KS.Disable) then
      KS.Disable.Tag:= AcncClient.TagEvent( KS.Disable.TagName, EdgeChange, TIntegerTag, EnableDisableKeyset);

    if Assigned(KS.Light) then
      KS.Light.Tag:= AcncClient.TagEvent( KS.Light.TagName, EdgeChange, TIntegerTag, KeyLight);

    if Assigned(KS.Action) then
      if KS.Action.ActionType = atNamed then
        KS.Action.Tag:= AcncClient.TagEvent( KS.Action.Definition, EdgeChange, TIntegerTag, nil);


    for J:= 0 to KS.SoftkeyCount - 1 do begin
      K:= KS.Softkeys[J];
      if Assigned(K.Disable) then begin
        K.Disable.Tag:= AcncClient.TagEvent( K.Disable.TagName, EdgeChange, TIntegerTag, EnableDisableKey);
      end;
      if Assigned(K.Light) then begin
        K.Light.Tag:= AcncClient.TagEvent( K.Light.TagName, EdgeChange, TIntegerTag, KeyLight);
      end;
      if Assigned(K.Action) then begin
        if K.Action.ActionType = atNamed then begin
          K.Action.Tag:= AcncClient.TagEvent( K.Action.Definition, EdgeChange, TIntegerTag, nil);
        end;
      end;
    end;
  end;
  AcncClient.TagEvent(NamePrimaryDisplay, EdgeChange, TStringTag, PrimaryDisplayChange);
end;

procedure TSoftkeyTreeStub.KeyEvent(Shift, Control: Boolean; Key: Integer; Edge: TEdgeType);
var Tmp: Integer;
    SK: TSoftkey;
begin
  if (Shift = Config.Shift) and (Control = Config.Control) then begin
    Tmp:= Key - Config.Basekey;
    if (Tmp >= 0) and (Tmp < Config.Count) then begin
      SK:= TSoftKey(Softkeys.Objects[Tmp]);
      if Assigned(SK.Logic) and not SK.Logic.Disabled then begin
        if Edge = EdgeRising then
           SK.Press
        else
           SK.Release;
      end;
    end;
  end;
end;

procedure TSoftkeyTreeStub.KeyLight(ATag: TAbstractTag);
var I: Integer;
    SK: TSoftkey;
begin
  Config.LightChange(ATag);

  for I:= 0 to Config.Count - 1 do begin
    SK:= TSoftkey(Softkeys.Objects[I]);
    if Assigned(SK.Logic) and Assigned(SK.Logic.Light) then begin
      if SK.Logic.Light.Tag = ATag then begin
        SK.ActiveDisplay:= SK.Logic.ActiveDisplay;
        SK.Invalidate;
      end;
    end;
  end;
end;

var Debug: Integer;

procedure TSoftkeyTreeStub.PrimaryDisplayChange(ATag: TAbstractTag);
begin
  Config.LinkToChange(ATag.AsString);
end;

procedure TSoftkeyTreeStub.ReloadConfiguration(Stream: TStream);
begin
  Self.PressedKey:= nil;
  Self.Clean;
  //Config:= T_SoftkeyTree.Create(AcncClient);
  InstallComponent(Stream);
  Installed;
  CheckInstallation;
  Resize;
end;

function TSoftkeyTreeStub.ResetFault(Fid: FHandle): Boolean;
begin
  Result:= CurrentFault = 0;
end;

procedure TSoftkeyTreeStub.ResetKeys(ATag: TAbstractTag);
begin
  Config.Reset;
end;

procedure TSoftkeyTreeStub.Resize;
var I: Integer;
    SK: TSoftkey;
    R, Tmp: Double;
const
    Border = 1;
begin
  inherited;
  if Assigned(Config) then begin
    if Width > Height then begin
      R:= 0;
      for I:= 0 to Config.Count - 1 do begin
        SK:= TSoftkey(Softkeys.Objects[I]);
        SK.Top:= Border;
        SK.Height:= Height - (Border * 2);
        SK.Left:= Trunc(R) + Border;
        Tmp:= Width;
        Tmp:= Tmp / Config.Count;
        Tmp:= Tmp * (I + 1);
        SK.Width:= Trunc(Tmp - R) - Border;
        Debug:= SK.Width;
        R:= Tmp;
      end;
    end
    else begin
      R:= 0;
      for I:= 0 to Config.Count - 1 do begin
        SK:= TSoftkey(Softkeys.Objects[I]);
        SK.Left:= Border;
        SK.Width:= Width - (Border * 2);
        SK.Top:= Round(R) + Border;
        Tmp:= Height;
        Tmp:= Tmp / Config.Count;
        Tmp:= Tmp * (I + 1);
        SK.Height:= Round(Tmp - R) - Border;
        R:= Tmp;
      end;
    end;
  end;
end;

{ TSoftkey }

constructor TSoftkey.Create(Aowner: TComponent);
begin
  inherited;
  Strings:= TStringList.Create;
  BitMapOn:= TBitMap.Create;
  BitMapOff:= TBitMap.Create;
  BitMapDisabled:= TBitMap.Create;

  BitMapOn.Transparent:= True;
  BitMapOff.Transparent:= True;
  BitMapDisabled.Transparent:= True;
end;

procedure TSoftkey.MouseDown(Button: TMouseButton; Shift: TShiftState; X,
  Y: Integer);
begin
  if Assigned(Logic) and not FLogic.Disabled then
    Press;
end;

procedure TSoftkey.MouseUp(Button: TMouseButton; Shift: TShiftState; X,
  Y: Integer);
begin
  if Assigned(Logic) and not FLogic.Disabled then
    Release;
end;

//var Blog1, Blog2: Integer;

procedure TSoftkey.Paint;
var I, T, H, W, O: Integer;
    Tmp: string;
begin
  if Down then
    O:= 1
  else
    O:= 0;

  if Assigned(ActiveDisplay) then begin
    Canvas.Font:= Font;
    Canvas.Font.Name:= SysObj.FontName;

    PaintBackground(ActiveDisplay.Background, ActiveDisplay.Bright, ActiveDisplay.Dark);

    if ActiveDisplay.ImageIndex >= 0 then begin
      Config.Images.Draw(Canvas, 5+O, 5+O, ActiveDisplay.ImageIndex, True);
    end;

    if Self.Strings.Count > 0 then begin
      H:= Canvas.TextHeight('gG') + 2;
      T:= H * Strings.Count;
      T:= (Height - T) div 2;
      if Logic.Disabled then begin
        Canvas.Pen.Color:= ActiveDisplay.DisabledColor;
        Canvas.Font.Color:= ActiveDisplay.DisabledColor;
      end
      else begin
        Canvas.Pen.Color:= ActiveDisplay.Foreground;
        Canvas.Font.Color:= ActiveDisplay.Foreground;
      end;

      Canvas.Brush.Color:= ActiveDisplay.Background;
//Blog1:= SelectObject(Canvas.Handle, Font.Handle);
//Windows.SetTextColor(Canvas.Handle, clLime);
      for I:= 0 to Strings.Count - 1 do begin
        Tmp:= Strings[I];
        W:= Canvas.TextWidth(Tmp);
//BLog2:= SelectObject(Canvas.Handle, Sysobj.VisualParent.Font.Handle); //Font.Handle);
//Windows.ExtTextOut(Canvas.Handle, (Width - W) div 2 + O, T + (I*H) + O, Canvas.TextFlags, nil, PChar(Tmp), Length(Tmp), nil);
        Canvas.TextOut((Width - W) div 2 + O, T + (I*H) + O, Tmp);
      end;
    end;

    if Logic.Disabled then
      Canvas.draw(Width - BitMapDisabled.Width - 3 + O, 3 + O, BitMapDisabled)
    else if Down then
      Canvas.draw(Width - BitMapOn.Width - 3 + O, 3 + O, BitMapOn)
    else
      Canvas.draw(Width - BitMapOff.Width - 3 + O, 3 + O, BitMapOff);
  end
  else begin
    PaintBackground(Config.Background, Config.Bright, Config.Dark);
    if Down then
      Canvas.draw(Width - BitMapOn.Width - 3 + O, 3 + O, BitMapOn)
    else
      Canvas.draw(Width - BitMapOff.Width - 3 + O, 3 + O, BitMapOff);
  end;
end;


procedure TSoftkey.PaintBackground(Color, Hi, Lo: TColor);
var R: TRect;
begin
  R.Left:= 0;
  R.Top:= 0;
  R.Bottom:= Height;
  R.Right:= Width;

  Canvas.Brush.Color:= Color;
  Canvas.FillRect(R);

  if Down then
    Canvas.Pen.Color:= Lo
  else
    Canvas.Pen.Color:= Hi;

  Canvas.MoveTo(0, 0);
  Canvas.LineTo(Width, 0);
  Canvas.LineTo(Width - 1, 1);
  Canvas.LineTo(1, 1);
  Canvas.LineTo(1, Height - 1);
  Canvas.LineTo(0, Height);
  Canvas.LineTo(0, 0);

  if not Down then
    Canvas.Pen.Color:= Lo
  else
    Canvas.Pen.Color:= Hi;

  Canvas.MoveTo(Width, Height);
  Canvas.LineTo(Width, 0);
  Canvas.LineTo(Width - 1, 1);
  Canvas.LineTo(Width - 1, Height - 1);
  Canvas.LineTo(1, Height - 1);
  Canvas.LineTo(0, Height);
  Canvas.LineTo(Width, Height);
end;

procedure TSoftkey.Press;
begin
  if Assigned(SoftkeyTree.PressedKey) and (SoftkeyTree.PressedKey <> Self) then begin
    SoftkeyTree.PressedKey.Release;
  end;

  if Assigned(Logic) and Assigned(Logic.Disable)then begin
    if Logic.Disable.Tag.AsBoolean then
      Exit;
  end;

  SoftkeyTree.PressedKey:= Self;
  if Assigned(ActiveDisplay) then begin
    if ActiveDisplay.Lockstate = sklsNone then
      Down:= True;
    Logic.FireEvent(True);
  end
  else
    Down:= True;
  Invalidate;
end;

procedure TSoftkey.Release;
begin
  if SoftkeyTree.PressedKey <> Self then
    EventLog('SoftkeyTree Releasing a key other than self?');

  SoftkeyTree.PressedKey:= nil;

  if Assigned(ActiveDisplay) then begin
    if ActiveDisplay.Lockstate = sklsNone then
      Down:= False;

    Logic.FireEvent(False);
  end
  else
    Down:= False;
  Invalidate;
end;

procedure TSoftkey.SetActiveDisplay(const Value: T_SoftkeyDisplay);
begin
  FActiveDisplay := Value;
  if Assigned(FActiveDisplay) then begin
    Text:= ActiveDisplay.Text;
    if ActiveDisplay.Lockstate = sklsDown then
      Down:= True

    else if ActiveDisplay.Lockstate = sklsUp then
      Down:= False
    else if SoftkeyTree.PressedKey <> Self then
      Down:= False;

  end else
    Down:= False;
end;

procedure TSoftkey.SetEnabled(Avalue: Boolean);
begin
  inherited;
end;

procedure TSoftkey.SetLogic(const Value: T_Softkey);
begin
  FLogic := Value;
  if Assigned(Logic) then
    ActiveDisplay:= Logic.ActiveDisplay
  else
    ActiveDisplay:= nil;
end;

procedure TSoftkey.SetText(const Value: string);
var P: Integer;
    Tmp: string;
begin
  FText := Value;
  Tmp:= FText;
  Strings.Clear;
  P:= System.Pos('~', Tmp);
  
  Caption:= Value;

  while P <> 0 do begin
    Strings.Add(System.Copy(Tmp, 1, P - 1));
    System.Delete(Tmp, 1, P);
    P:= System.Pos('~', Tmp);
  end;
  Strings.Add(Tmp);
end;

{ TSoftkeyTree }

procedure TSoftkeyTree.CheckInstallation;
begin
  inherited;
  Stub.CheckInstallation;
end;

procedure TSoftkeyTree.InstallComponent(Params: TParamStrings);
begin
  inherited;
  Stub:= TSoftkeyTreeStub.Create(Self);
  Stub.Parent:= Self;
  Stub.Align:= alClient;
  Stub.AcncClient:= Self;
  Stub.InstallComponent( OpenLiveStream(Name) );
  ManageEdit := True;
end;

procedure TSoftkeyTree.Installed;
begin
  inherited;
  Stub.Installed;
  TagEvent(Name + 'Reload', EdgeFalling, TMethodTag, ReloadConfiguration);
end;


procedure TSoftkeyTree.ReloadConfiguration(ATag: TAbstractTag);
begin
  if not ATag.AsBoolean then
    Stub.ReloadConfiguration( OpenLiveStream(Name) );
end;

procedure TSoftkeyTree.SetEnabled(AValue: Boolean);
begin
  inherited;
  if Stub.PressedKey <> nil then
    Stub.PressedKey.Release;
end;

procedure TSoftkeyTree.OpenkeySet(Name: string);
begin
  Stub.Config.SetActiveSetByName(Name);
end;


{ TSoftkeyTreeCore }

procedure TSoftkeyTreeCore.CheckInstallation;
begin
  inherited;
  Stub.CheckInstallation;
end;

procedure TSoftkeyTreeCore.InstallComponent(Params: TParamStrings);
begin
  inherited;
  Stub:= TSoftkeyTreeStub.Create(Self);
  Stub.Parent:= Self;
  Stub.Align:= alClient;
  Stub.AcncClient:= Self;
  Stub.InstallComponent( OpenLiveStream(Name) );
  ManageEdit := True;
end;

procedure TSoftkeyTreeCore.Installed;
begin
  inherited;
  Stub.Installed;
  TagEvent(Name + 'Reload', EdgeFalling, TMethodTag, ReloadConfiguration);
end;

procedure TSoftkeyTreeCore.KeyChange(handle: word);
begin
end;

procedure TSoftkeyTreeCore.KeyClose(handle: word);
begin
end;

procedure TSoftkeyTreeCore.KeyEvent(key: integer; Edge: TedgeType);
var Shift, Control: Boolean;
    I: Integer;
begin
  case Key of
    VK_ESCAPE: begin
      Key:= 0;
    end;

    VK_F1..VK_F12: begin
      Key:= Key - VK_F1 + 1
    end;

  else
    Key:= -1;
  end;

  Shift := (Windows.GetKeyState(VK_SHIFT) < 0);
  Control := (Windows.GetKeyState(VK_CONTROL) < 0);
  //Key := Windows.GetKeyState(VK_MENU);

  for I:= 0 to InstalledKeySets.Count - 1 do begin
    TSoftkeyTreeStub(InstalledKeySets.Objects[I]).KeyEvent(Shift, Control, Key, Edge);
  end;
end;

procedure TSoftkeyTreeCore.OpenkeySet(Name: string);
begin
  Stub.Config.SetActiveSetByName(Name);
end;

procedure TSoftkeyTreeCore.ReloadConfiguration(ATag: TAbstractTag);
begin
  if not ATag.AsBoolean then
    Stub.ReloadConfiguration( OpenLiveStream(Name) );
end;

procedure TSoftkeyTreeCore.Reset;
var I: Integer;
begin
  inherited;
  Stub.Config.Reset;

  for I:= 0 to InstalledKeySets.Count - 1 do begin
    TSoftkeyTreeStub(InstalledKeySets.Objects[I]).Config.Reset;
  end;

end;

procedure TSoftkeyTreeCore.SetEnabled(AValue: Boolean);
begin
  inherited;
  if Stub.PressedKey <> nil then
    Stub.PressedKey.Release;
end;


initialization
  InstalledKeysets := TStringList.Create;
  CoreCNC.RegisterCNCClass(TSoftkeyTree, 'SoftkeyTree');
  CoreCNC.RegisterCNCClass(TSoftkeyTreeCore, 'SoftkeyTreeCore');
end.
