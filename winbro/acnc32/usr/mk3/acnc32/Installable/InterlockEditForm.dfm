object InterlockEditFrm: TInterlockEditFrm
  Left = 691
  Top = 403
  BorderStyle = bsToolWindow
  Caption = 'Interlock Administration'
  ClientHeight = 393
  ClientWidth = 319
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = True
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object SpeedButton13: TSpeedButton
    Left = 16
    Top = 112
    Width = 25
    Height = 17
  end
  object Panel1: TPanel
    Left = 0
    Top = 352
    Width = 319
    Height = 41
    Align = alBottom
    BevelOuter = bvLowered
    TabOrder = 0
    object OkBtn: TBitBtn
      Left = 24
      Top = 8
      Width = 75
      Height = 25
      Enabled = False
      TabOrder = 0
      Kind = bkOK
    end
    object BitBtn2: TBitBtn
      Left = 112
      Top = 8
      Width = 75
      Height = 25
      TabOrder = 1
      Kind = bkCancel
    end
    object RemoveBtn: TBitBtn
      Left = 208
      Top = 8
      Width = 75
      Height = 25
      Caption = '&Remove'
      Enabled = False
      TabOrder = 2
      Kind = bkNo
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 319
    Height = 193
    Align = alTop
    BevelOuter = bvLowered
    TabOrder = 1
    object AccessGroup: TGroupBox
      Left = 161
      Top = 1
      Width = 157
      Height = 191
      Align = alRight
      Caption = 'Permissive User Access Levels'
      Enabled = False
      TabOrder = 0
      object OperatorSpeedBtn: TSpeedButton
        Tag = 11
        Left = 15
        Top = 40
        Width = 25
        Height = 17
        OnClick = ManualSpeedBtnClick
      end
      object SupervisorSpeedBtn: TSpeedButton
        Tag = 12
        Left = 15
        Top = 64
        Width = 25
        Height = 17
        OnClick = ManualSpeedBtnClick
      end
      object ProgrammerSpeedBtn: TSpeedButton
        Tag = 13
        Left = 15
        Top = 88
        Width = 25
        Height = 17
        OnClick = ManualSpeedBtnClick
      end
      object MaintenanceSpeedBtn: TSpeedButton
        Tag = 14
        Left = 15
        Top = 112
        Width = 25
        Height = 17
        OnClick = ManualSpeedBtnClick
      end
      object AdministratorSpeedBtn: TSpeedButton
        Tag = 15
        Left = 15
        Top = 136
        Width = 25
        Height = 17
        OnClick = ManualSpeedBtnClick
      end
      object OEM1SpeedBtn: TSpeedButton
        Tag = 16
        Left = 15
        Top = 160
        Width = 25
        Height = 17
        OnClick = ManualSpeedBtnClick
      end
      object NoneSpeedBtn: TSpeedButton
        Tag = 10
        Left = 15
        Top = 16
        Width = 25
        Height = 17
        OnClick = ManualSpeedBtnClick
      end
      object OperatorLabel: TStaticText
        Left = 42
        Top = 40
        Width = 65
        Height = 17
        AutoSize = False
        BorderStyle = sbsSunken
        Caption = 'Operator'
        Color = clLime
        ParentColor = False
        TabOrder = 0
      end
      object SupervisorLabel: TStaticText
        Left = 42
        Top = 64
        Width = 65
        Height = 17
        AutoSize = False
        BorderStyle = sbsSunken
        Caption = 'Supervisor'
        Color = clLime
        ParentColor = False
        TabOrder = 1
      end
      object ProgrammerLabel: TStaticText
        Left = 42
        Top = 88
        Width = 65
        Height = 17
        AutoSize = False
        BorderStyle = sbsSunken
        Caption = 'Programmer'
        Color = clLime
        ParentColor = False
        TabOrder = 2
      end
      object MaintenanceLabel: TStaticText
        Left = 42
        Top = 112
        Width = 65
        Height = 17
        AutoSize = False
        BorderStyle = sbsSunken
        Caption = 'Maintenance'
        Color = clLime
        ParentColor = False
        TabOrder = 3
      end
      object AdministratorLabel: TStaticText
        Left = 42
        Top = 136
        Width = 65
        Height = 17
        AutoSize = False
        BorderStyle = sbsSunken
        Caption = 'Administrator'
        Color = clLime
        ParentColor = False
        TabOrder = 4
      end
      object OEM1Label: TStaticText
        Left = 42
        Top = 160
        Width = 65
        Height = 17
        AutoSize = False
        BorderStyle = sbsSunken
        Caption = 'OEM1'
        Color = clLime
        ParentColor = False
        TabOrder = 5
      end
      object NoneLabel: TStaticText
        Left = 42
        Top = 16
        Width = 65
        Height = 17
        AutoSize = False
        BorderStyle = sbsSunken
        Caption = 'None'
        Color = clLime
        ParentColor = False
        TabOrder = 6
      end
    end
    object Panel3: TPanel
      Left = 1
      Top = 1
      Width = 160
      Height = 191
      Align = alClient
      TabOrder = 1
      object ModesGroup: TGroupBox
        Left = 1
        Top = 1
        Width = 158
        Height = 151
        Align = alClient
        Caption = 'Permissive Modes'
        Enabled = False
        TabOrder = 0
        object ManualSpeedBtn: TSpeedButton
          Left = 16
          Top = 16
          Width = 25
          Height = 17
          OnClick = ManualSpeedBtnClick
        end
        object AutoSpeedBtn: TSpeedButton
          Tag = 1
          Left = 16
          Top = 40
          Width = 25
          Height = 17
          OnClick = ManualSpeedBtnClick
        end
        object MDISpeedBtn: TSpeedButton
          Tag = 2
          Left = 16
          Top = 64
          Width = 25
          Height = 17
          OnClick = ManualSpeedBtnClick
        end
        object TeachInSpeedBtn: TSpeedButton
          Tag = 3
          Left = 16
          Top = 88
          Width = 25
          Height = 17
          OnClick = ManualSpeedBtnClick
        end
        object EditSpeedBtn: TSpeedButton
          Tag = 4
          Left = 16
          Top = 112
          Width = 25
          Height = 17
          OnClick = ManualSpeedBtnClick
        end
        object ManualLabel: TStaticText
          Left = 42
          Top = 16
          Width = 65
          Height = 17
          AutoSize = False
          BorderStyle = sbsSunken
          Caption = 'Manual'
          Color = clLime
          ParentColor = False
          TabOrder = 0
        end
        object AutoLabel: TStaticText
          Tag = 1
          Left = 42
          Top = 40
          Width = 65
          Height = 17
          AutoSize = False
          BorderStyle = sbsSunken
          Caption = 'Auto'
          Color = clLime
          ParentColor = False
          TabOrder = 1
        end
        object MDILabel: TStaticText
          Tag = 2
          Left = 42
          Top = 64
          Width = 65
          Height = 17
          AutoSize = False
          BorderStyle = sbsSunken
          Caption = 'MDI'
          Color = clLime
          ParentColor = False
          TabOrder = 2
        end
        object TeachinLabel: TStaticText
          Tag = 3
          Left = 42
          Top = 88
          Width = 65
          Height = 17
          AutoSize = False
          BorderStyle = sbsSunken
          Caption = 'Teach-In'
          Color = clLime
          ParentColor = False
          TabOrder = 3
        end
        object EditLabel: TStaticText
          Tag = 4
          Left = 42
          Top = 112
          Width = 65
          Height = 17
          AutoSize = False
          BorderStyle = sbsSunken
          Caption = 'Edit'
          Color = clLime
          ParentColor = False
          TabOrder = 4
        end
      end
      object Panel4: TPanel
        Left = 1
        Top = 152
        Width = 158
        Height = 38
        Align = alBottom
        TabOrder = 1
        object ActiveSpeedBtn: TSpeedButton
          Left = 16
          Top = 8
          Width = 25
          Height = 20
          AllowAllUp = True
          GroupIndex = 1
          Enabled = False
          OnClick = ActiveSpeedBtnClick
        end
        object ActiveLabel: TStaticText
          Left = 42
          Top = 8
          Width = 82
          Height = 20
          Alignment = taCenter
          AutoSize = False
          BorderStyle = sbsSunken
          Caption = 'Active'
          Color = clLime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
          TabOrder = 0
        end
      end
    end
  end
  object StatusPanel: TPanel
    Left = 0
    Top = 193
    Width = 319
    Height = 159
    Align = alClient
    BevelOuter = bvLowered
    TabOrder = 2
    object StatusGroup: TGroupBox
      Left = 1
      Top = 1
      Width = 317
      Height = 157
      Align = alClient
      Caption = 'Status'
      Enabled = False
      TabOrder = 0
      object IncInMotionSpeedBtn: TSpeedButton
        Tag = 20
        Left = 72
        Top = 48
        Width = 25
        Height = 17
        OnClick = ManualSpeedBtnClick
      end
      object IncInCycleSpeedBtn: TSpeedButton
        Tag = 21
        Left = 72
        Top = 72
        Width = 25
        Height = 17
        OnClick = ManualSpeedBtnClick
      end
      object IncHomeSpeedBtn: TSpeedButton
        Tag = 22
        Left = 72
        Top = 96
        Width = 25
        Height = 17
        OnClick = ManualSpeedBtnClick
      end
      object IncEditingSpeedBtn: TSpeedButton
        Tag = 23
        Left = 72
        Top = 120
        Width = 25
        Height = 17
        OnClick = ManualSpeedBtnClick
      end
      object ExcInMotionSpeedBtn: TSpeedButton
        Tag = 30
        Left = 200
        Top = 48
        Width = 25
        Height = 17
        OnClick = ManualSpeedBtnClick
      end
      object ExcInCycleSpeedBtn: TSpeedButton
        Tag = 31
        Left = 200
        Top = 72
        Width = 25
        Height = 17
        OnClick = ManualSpeedBtnClick
      end
      object ExcHomeSpeedBtn: TSpeedButton
        Tag = 32
        Left = 200
        Top = 96
        Width = 25
        Height = 17
        OnClick = ManualSpeedBtnClick
      end
      object ExcEditingSpeedBtn: TSpeedButton
        Tag = 33
        Left = 200
        Top = 120
        Width = 25
        Height = 17
        OnClick = ManualSpeedBtnClick
      end
      object StaticText12: TStaticText
        Left = 24
        Top = 24
        Width = 105
        Height = 17
        Alignment = taCenter
        AutoSize = False
        BorderStyle = sbsSunken
        Caption = 'Inclusive'
        Color = clLime
        ParentColor = False
        TabOrder = 0
      end
      object StaticText13: TStaticText
        Left = 168
        Top = 24
        Width = 105
        Height = 17
        Alignment = taCenter
        AutoSize = False
        BorderStyle = sbsSunken
        Caption = 'Exclusive'
        Color = clRed
        ParentColor = False
        TabOrder = 1
      end
      object InMotionLabel: TStaticText
        Left = 96
        Top = 48
        Width = 105
        Height = 17
        Alignment = taCenter
        AutoSize = False
        BorderStyle = sbsSunken
        Caption = 'In Motion'
        TabOrder = 2
      end
      object InCycleLabel: TStaticText
        Left = 96
        Top = 72
        Width = 105
        Height = 17
        Alignment = taCenter
        AutoSize = False
        BorderStyle = sbsSunken
        Caption = 'In Cycle'
        TabOrder = 3
      end
      object HomedLabel: TStaticText
        Left = 96
        Top = 96
        Width = 105
        Height = 17
        Alignment = taCenter
        AutoSize = False
        BorderStyle = sbsSunken
        Caption = 'Homed'
        TabOrder = 4
      end
      object EditingLabel: TStaticText
        Left = 96
        Top = 120
        Width = 105
        Height = 17
        Alignment = taCenter
        AutoSize = False
        BorderStyle = sbsSunken
        Caption = 'Editing'
        TabOrder = 5
      end
    end
  end
end
