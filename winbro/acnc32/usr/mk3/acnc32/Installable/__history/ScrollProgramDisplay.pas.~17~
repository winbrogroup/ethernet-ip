unit ScrollProgramDisplay;

interface

uses SysUtils, Classes, CNCTypes, CNC32, CoreCNC, CNCAbs, Forms, ExtCtrls,
     Graphics, Controls, NCNames, Named, Types;

type
  TScrollProgDisplay = class(TExpandDisplay)
  private
    BM: TBitMap;
    ScrollBox: TScrollBox;
    PaintBox: TPaintBox;
    FActiveText: TColor;
    FPassiveText: TColor;
    //NCLines: TStrings;
    NCProgramLine: Integer;
    NCLineCount: Integer;

    CharY: Integer;
    CharX: Integer;

    procedure UpdateDisplay;
    procedure LineChange(aTag: TAbstractTag);
    procedure DisplaySweep(ATag: TAbstractTag);
    procedure PaintBoxPaint(Server: TObject);
  protected
    procedure Resize; override;
    procedure Paint; override;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure InstallComponent(Params: TParamStrings); override;
    procedure Installed; override;
  end;

implementation

{ TScrollProgDisplay }

constructor TScrollProgDisplay.Create(AOwner: TComponent);
begin
  inherited;
  Font.Size   := 12;
  Font.Name   := 'Courier New';
  Font.Style  := [fsBold];

  ScrollBox:= TScrollBox.Create(Self);
  ScrollBox.Parent:= Self;
  ScrollBox.Align:= alClient;
  ScrollBox.BevelInner:= bvNone;
  ScrollBox.BevelOuter:= bvNone;
  ScrollBox.DoubleBuffered:= True;
  ScrollBox.VertScrollBar.Smooth:= True;

  PaintBox:= TPaintBox.Create(Self);
  PaintBox.Parent:= ScrollBox;
  PaintBox.Left:= 0;
  PaintBox.Top:= 0;
  PaintBox.OnPaint:= PaintBoxPaint;

  BM:= TBitMap.Create;
  BM.canvas.font := font;
end;

destructor TScrollProgDisplay.Destroy;
begin
  BM.Destroy;
  inherited;
end;

procedure TScrollProgDisplay.DisplaySweep(ATag: TAbstractTag);
begin
end;

procedure TScrollProgDisplay.InstallComponent(Params: TParamStrings);
begin
  inherited;
  FActiveText  := Params.ReadColour('ActiveText',  clYellow);
  FPassiveText := Params.ReadColour('PassiveText', clSilver);
  Resize;
end;

procedure TScrollProgDisplay.Installed;
begin
  inherited;
  with NCPublishedF[ncpProgramLine] do
    TagEvent(Format(Tag, [1]), edgeChange, Size, lineChange);
  with NCPublishedF[ncpPartProgram] do
    TagEvent(Format(Tag, [1]), edgeChange, Size, lineChange);

  TagEvent(NameDisplaySweep, edgeChange, TIntegerTag, DisplaySweep);
end;

procedure TScrollProgDisplay.LineChange(aTag: TAbstractTag);
var I: Integer;
    H: Integer;
    W: Integer;
    N: TStrings;
    Change: Boolean;
begin

    N:= SysObj.NC.SubRoutineLines[SysObj.NC.ActiveSubRoutine];
    Change:= False;
    if N = nil then begin
      Change:= True;
    end else
    if N.Count <> NCLineCount then begin
      Change:= True;
    end;

    //NCLines:= N;

try
    if Change then begin
      if N <> nil then begin
        NCLineCount:= N.Count;
        ScrollBox.VertScrollBar.Position:= 0;
        PaintBox.Height:= N.Count * CharY;

        EventLogFmt('Paintbox Height: %d', [PaintBox.Height]);
        W:= 0;
        for I:= 0 to N.Count - 1 do begin
          if Length(N[I]) > W then begin
            W:= Length(N[I]);
          end;
        end;
        PaintBox.Width:= (W * CharX) + (CharX div 2);
      end else begin
        PaintBox.Width:= ScrollBox.ClientRect.Right;
        PaintBox.Height:= ScrollBox.ClientRect.Bottom;
        EventLogFmt('NL Paintbox Height: %d', [PaintBox.Height]);
        //ScrollBox.HorzScrollBar.Range:= 1;
        //ScrollBox.VertScrollBar.Range:= 1;
      end;
    end;
except on E: Exception do
  EventLog('Error: ' + E.Message);
end;


//    if NCLines <> SysObj.NC.SubRoutineLines[SysObj.NC.ActiveSubRoutine] then begin
//      NCLines:= SysObj.NC.SubRoutineLines[SysObj.NC.ActiveSubRoutine];
//    end;

    if NCProgramLine <> SysObj.NC.ProgramLine then begin
      NCProgramLine := SysObj.NC.ProgramLine;

      if ScrollBox.HorzScrollBar.IsScrollBarVisible then
        ScrollBox.HorzScrollBar.Position := 0;

      if ScrollBox.VertScrollBar.IsScrollBarVisible then begin
        H:= NCProgramLine * CharY;
        H:= H - (ScrollBox.ClientHeight div 2);
        if H > 0 then
          ScrollBox.VertScrollBar.Position:= H
        else
          ScrollBox.VertScrollBar.Position:= 0;
      end;
    end;
  PaintBox.Invalidate;
end;

procedure TScrollProgDisplay.Paint;
begin
  inherited;
end;

procedure TScrollProgDisplay.PaintBoxPaint(Server: TObject);
begin
  UpdateDisplay;
  PaintBox.Canvas.Draw(ScrollBox.HorzScrollBar.Position, ScrollBox.VertScrollBar.Position, BM);
end;

procedure TScrollProgDisplay.Resize;
begin
  inherited;
  BorderWidth:= 1;
  BevelInner:= bvNone;
  BevelOuter:= bvRaised;

  if (BM.Width <> ScrollBox.ClientWidth) or
     (BM.Height <> ScrollBox.ClientHeight) then begin
    BM.Width := ScrollBox.ClientRect.Right;
    BM.Height := ScrollBox.ClientRect.Bottom;
    EventLogFmt('BM Height: %d', [BM.Height]);
  end;
  BM.Canvas.Font := Font;

  CharY := BM.canvas.TextHeight('Q');
  CharX := BM.canvas.TextWidth('Q');
end;

procedure TScrollProgDisplay.UpdateDisplay;
var OX, OY, I, TX, TY: Integer;
    Start, C: Integer;
    TmpLines: TStrings;
begin
  BM.Canvas.Brush.color := color;
  BM.Canvas.FillRect(ScrollBox.ClientRect);
  TmpLines:= SysObj.NC.SubRoutineLines[SysObj.NC.ActiveSubRoutine];

  if Assigned(TmpLines) then begin
    OY:= ScrollBox.VertScrollBar.Position;
    OX:= ScrollBox.HorzScrollBar.Position;

    Start:= OY div CharY;
    C:= (ScrollBox.ClientHeight div CharY) + 1;
    if C + Start >= TmpLines.Count then
      C:= TmpLines.Count - Start - 1;

    for I:= 0 to C do begin
      if (Start + I) = NCProgramLine then
        BM.Canvas.Font.Color := FActiveText
      else
        BM.Canvas.Font.Color := FPassiveText;
      TX:= -OX + (CharX div 2);
      TY:= ((Start + I) * CharY) - OY;
      BM.Canvas.TextOut(TX, TY, TmpLines[Start + I]);
    end;
  end;
end;

initialization
  RegisterCNCClass(TScrollProgDisplay, 'ScrollProgramDisplay');
end.
