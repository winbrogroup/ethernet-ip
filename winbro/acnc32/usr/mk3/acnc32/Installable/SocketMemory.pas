unit SocketMemory;

interface

uses Classes, SysUtils, CoreCNC, CNCTypes, CNC32, ScktComp, WinSock, GenericIO,
     SyncObjs, Windows, Messages;

{  Client command            Server response
   manifest                  manifest "NTags" ["TagName" "TagValue" "TagType"]
   fullmanifest              fullmanifest "NTags" ["TagName" "TagValue" "TagType"]
   get "TagName"             get "TagName" "TagValue" "TagType" "OK" / "NOK"
   set "TagName" "TagValue"  set "TagName" "TagValue" "TagType" "OK" / "NOK"
   auto "TagName"            auto "TagName" "OK" / "NOK"
   rate "RateMS"             rate "RateMS" "OK" / "NOK"
   manual "TagName"          manual "TagName" "OK" / "NOK"

   ServerUnsolicited
   data "TagName" "TagValue" "TagType"


   Transport
   All values are transported as strings, a string packetized as follows
   4byte length field
   null terminated string.

   All transports are sent as a single string. with each data item inside
   quotes "".
   If any string contains a quote or non printable character then this is
   translated to %nn, where nn is the hex code of the character.
   Example
     "Hello" -> "%22Hello%22"
     "Gosh" 20% is a lot -> "%22Gosh%22 20%25 is a lot"

   Responses are sent as single strings except Manifest which sends
   "TagName" "TagValue" "TagType" as discreet strings NTags times.
}


type
  TServerSocketOPC = class;
  TDispatchThread = class;

  TCommItem = class(TObject)
    OPCItem : TOPCTagRecord;
    Value : string;
    LastValue : string;
    LastUpdate : TDateTime;
    Monitoring : Boolean;
    UpdateRequired : Boolean;
  end;


  TServSockWinSocket = class(TServerClientWinSocket)
  private
    DispatchList : TStringList;
    EventList : TStringList;
    ClientTags : TList;
    Memory: TServerSocketOPC;
    Manifest : TStringList;
    tv_usec : Integer;
    tv_sec : Integer;
    ReadWriteLock : TCriticalSection;
  protected
    procedure ProcessPacket(Packet : string);
    procedure AddCallback(aTag : TAbstractTag);
    procedure RemoveCallback(aTag : TAbstractTag);
    procedure LocalEvent(aTag : TAbstractTag);
    procedure WriteString(aText : string);
    procedure Clean;
    procedure DoRead;
    procedure WriteStandard(const Comm : string;
          Tag : TCommItem; OK : Boolean; TagName : string = '');
    function FindCommItem(const aName : string) : TCommItem;
    procedure AddEvent(aEvent : string);
    procedure DeleteEvent;
    procedure AddDispatch(aDispatch : string);
    procedure DeleteDispatch;
  public
    constructor Create(aMemory : TServerSocketOPC; aSocket: TSocket;
                               aServerWinSocket: TServerWinSocket);
    destructor Destroy; override;
    procedure SendCommItem(CommItem : TCommItem);
  end;

  TServerSocketOPC = class(TCNC)
  private
    Server : TServerSocket;
    Lock : TCriticalSection;
    ConnectionCountTag : TAbstractTag;
    procedure ThreadEvent(var Message : TMessage); message CNCM_LOCAL_EVENT;
    procedure DoGetSocket(Sender : TObject; Socket : TSocket;
      var ClientSocket : TServerClientWinSocket);
    procedure DoAccept(Sender : TObject; Socket: TCustomWinSocket);
    procedure DoClientError(Sender: TObject; Socket: TCustomWinSocket; ErrorEvent: TErrorEvent; var ErrorCode: Integer);
    procedure DoGetThread(Sender: TObject; ClientSocket: TServerClientWinSocket;
   var SocketThread: TServerClientThread);
    procedure DoError(Sender: TObject; Socket: TCustomWinSocket; ErrorEvent: TErrorEvent;
  var ErrorCode: Integer);
  public
    constructor Create(aOwner : TComponent); override;
    destructor Destroy; override;
    procedure InstallComponent(Params : TParamStrings); override;
    procedure CheckInstallation; override;
    procedure IShutdown; override;
  end;

  TDispatchThread = class(TServerClientThread)
  private
    Server : TServSockWinSocket;
    RxBuff : PChar;
    RxBuffSize : Integer;
    FEvent : TEvent;
    function ReadAsStream(var Buffer; Count: Longint): Longint;
  protected
    procedure ClientExecute; override;
    procedure WriteSocket;
    procedure WriteString(aText: string);
    procedure ReadString;
  public
    constructor Create(Suspended : Boolean; aServer : TServSockWinSocket);
    destructor Destroy; override;
  end;

implementation

const
  CNCSocketMemoryPort = 12122;

constructor TServerSocketOPC.Create;
begin
  inherited;
  Lock := TCriticalSection.Create;
  Server := TServerSocket.Create(Self);
end;

destructor TServerSocketOPC.Destroy;
begin
  Server.Free;
  inherited;
end;

procedure TServerSocketOPC.InstallComponent(Params : TParamStrings);
begin
  inherited;
  ConnectionCountTag := TagPublish(Name + 'ConnectionCount', TIntegerTag);
end;

procedure TServerSocketOPC.CheckInstallation;
begin
  inherited Installed;
  Server := TServerSocket.Create(Self);
  Server.Port := CNCSocketMemoryPort;
  Server.ServerType := stThreadBlocking;
  Server.OnAccept := DoAccept;
  Server.OnGetSocket := DoGetSocket;
  Server.OnGetThread := DoGetThread;
  Server.OnClientError := DoClientError;
//  Server.OnClientConnect := DoClientConnect;
//  Server.OnClientDisconnect := DoClientDisconnect;
  Server.ThreadCacheSize := 2;
  Server.Open;
end;

procedure TServerSocketOPC.IShutdown;
begin
  Server.Close;
  inherited;
end;

procedure TServerSocketOPC.ThreadEvent(var Message : TMessage);
begin
  if Message.Msg = CNCM_LOCAL_EVENT then begin
    ConnectionCountTag.AsInteger := ConnectionCountTag.AsInteger + Message.WParam;
  end;
end;

procedure TServerSocketOPC.DoGetSocket(Sender : TObject; Socket : TSocket;
      var ClientSocket : TServerClientWinSocket);
var ServSockWinSocket : TServSockWinSocket;
begin
  ServSockWinSocket := TServSockWinSocket.Create(Self, Socket, Server.Socket);
  ServSockWinSocket.OnErrorEvent := DoError;
//  ServSockWinSocket.OnSocketEvent := DoSocketEvent;
  ClientSocket := TServerClientWinSocket(ServSockWinSocket);
end;

procedure TServerSocketOPC.DoGetThread(Sender: TObject; ClientSocket: TServerClientWinSocket;
   var SocketThread: TServerClientThread);
begin
  SocketThread := TDispatchThread.Create(False, TServSockWinSocket(ClientSocket));
  SocketThread.Priority := tpLowest;
end;

procedure TServerSocketOPC.DoAccept(Sender: TObject;
  Socket: TCustomWinSocket);
begin

end;

procedure TServerSocketOPC.DoClientError(Sender: TObject; Socket: TCustomWinSocket; ErrorEvent: TErrorEvent; var ErrorCode: Integer);
begin
  ErrorCode := 0;
end;

procedure TServerSocketOPC.DoError(Sender: TObject;
  Socket: TCustomWinSocket; ErrorEvent: TErrorEvent;
  var ErrorCode: Integer);
begin
  if Socket.Connected then
    Socket.Disconnect(Socket.Handle);
  ErrorCode := 0;
end;

{ TServSockWinSocket }

constructor TServSockWinSocket.Create(aMemory: TServerSocketOPC;
  aSocket: TSocket; aServerWinSocket: TServerWinSocket);
begin
  inherited Create(aSocket, aServerWinSocket);
  ReadWriteLock := TCriticalSection.Create;
  DispatchList := TStringList.Create;
  EventList := TStringList.Create;
  tv_sec := 2;
  tv_usec := 500000;

  ClientTags := TList.Create;
  Memory := aMemory;
  Manifest := TStringList.Create;
  PostMessage(Memory.Handle, CNCM_LOCAL_EVENT, 1, 0);
end;

destructor TServSockWinSocket.Destroy;
begin
  PostMessage(Memory.Handle, CNCM_LOCAL_EVENT, -1, 0);
  Clean;
  ReadWriteLock.Free;
  ClientTags.Free;
  Manifest.Free;
  DispatchList.Free;
  inherited;
end;

procedure TServSockWinSocket.Clean;
var aTag : TAbstractTag;
    CommItem : TCommItem;
begin
  while ClientTags.Count > 0  do begin
    aTag := TAbstractTag(ClientTags[0]);
    RemoveCallback(aTag);
  end;
  while Manifest.Count > 0 do begin
    CommItem := TCommItem(Manifest.Objects[0]);
    CommItem.Free;
    Manifest.Delete(0);
  end;
end;

procedure TServSockWinSocket.DoRead;
begin
  while EventList.Count > 0 do begin
//    EventLog('ProcessPacket: ' + EventList[0]);
    ProcessPacket(EventList[0]);
//    EventLog('ProcessPacket Exit');
    Self.DeleteEvent;
  end;
end;

procedure TServSockWinSocket.WriteStandard(const Comm : string;
          Tag : TCommItem; OK : Boolean; TagName : string = '');
begin
  if Assigned(Tag) then begin
    if OK then
      WriteString( Comm + ' ' +
                   QuotedString(Tag.OPCItem.NetAlias) + ' ' +
                   QuotedString(Tag.OPCItem.Tag.AsString) + ' ' +
                   QuotedString(Tag.OPCItem.Tag.ClassName) + ' "OK"')
    else
      WriteString( Comm + ' ' +
                   QuotedString(Tag.OPCItem.NetAlias) + ' ' +
                   QuotedString(Tag.OPCItem.Tag.AsString) + ' ' +
                   QuotedString(Tag.OPCItem.Tag.ClassName) + ' "NOK"');
  end else begin
      WriteString( Comm + ' ' + QuotedString(TagName) +
                   ' "NOT-FOUND"' + '"TStringTag" "NOK"');
  end;
end;

function TServSockWinSocket.FindCommItem(const aName : string) : TCommItem;
var I : Integer;
begin
  Result := nil;
  I := Manifest.IndexOf(LowerCase(aName));
  if I <> -1 then
    Result := TCommItem(Manifest.Objects[I]);
end;

procedure TServSockWinSocket.ProcessPacket(Packet : string);
var TagName : string;
    Tmp : string;

  procedure DoManifest;
  var I : Integer;
      P : TOPCTagRecord;
      CommItem : TCommItem;
  begin
    WriteString('manifest ' + QuotedString(IntToStr(SysObj.Comms.FOPCList.Count)));
    for I := 0 to SysObj.Comms.FOPCList.Count - 1 do begin

      P := TOPCTagRecord(SysObj.Comms.FOPCList.Objects[I]);

      CommItem := TCommItem.Create;
      CommItem.OPCItem := P;
      CommItem.LastUpdate := Now;
      CommItem.LastValue := P.Tag.AsString;

      WriteString( QuotedString(P.NetAlias) + ' ' +
                   QuotedString(P.Tag.AsString) + ' ' +
                   QuotedString(P.Tag.ClassName));

      Manifest.AddObject(LowerCase(P.NetAlias), CommItem);
    end;
  end;

  procedure DoFullManifest;
  var I : Integer;
      aTag : TAbstractTag;
  begin
    WriteString('manifest ' + QuotedString(IntToStr(SysObj.Comms.NamedSize)));
    for I := 0 to SysObj.Comms.NamedSize - 1 do begin

      aTag := SysObj.Comms.NamedLayer.Tag[I];
      WriteString( QuotedString(aTag.Name) + ' ' +
                   QuotedString(aTag.AsString) + ' ' +
                   QuotedString(aTag.ClassName));
    end;
  end;

  procedure DoSet;
  var CommItem : TCommItem;
  begin
    TagName := ReadQuotedString(Packet);
    CommItem := FindCommItem(TagName);
    if Assigned(CommItem) then begin
      if not CommItem.OPCItem.Tag.IsOwned then begin
        CommItem.LastValue :=ReadQuotedSTring(Packet);
        CommItem.OPCItem.Tag.AsString := CommItem.LastValue;
        WriteStandard('set', CommItem, True);
      end else begin
        WriteStandard('set', CommItem, False);
      end;
    end else begin
      WriteStandard('set', nil, False, TagName);
    end;
  end;

  procedure DoGet;
  var CommItem : TCommItem;
  begin
    TagName := ReadQuotedString(Packet);
    CommItem := FindCommItem(LowerCase(TagName));
    if Assigned(CommItem) then begin
      WriteStandard('get', CommItem, True);
    end else begin
      WriteStandard('get', nil, False, TagName);
    end;
  end;

  procedure DoAuto;
  var CommItem : TCommItem;
  begin
    TagName := ReadQuotedString(Packet);
    CommItem := FindCommItem(TagName);
    if Assigned(CommItem) then begin
      AddCallback(CommItem.OPCItem.Tag);
      WriteStandard('auto', CommItem , True);
    end else begin
      WriteStandard('auto ', nil, False, TagName);
    end;
  end;

  procedure DoRate;
  var Rate : Integer;
  begin
    Tmp := ReadQuotedString(Packet);
    try
      Rate := StrToInt(Tmp);
      WriteString('rate ' + QuotedString(Tmp) + ' "OK"');
    except
      Rate := 800;
      WriteString('rate "800" "NOK"');
    end;
    Rate := Rate * 1000;
    tv_usec := Rate mod 1000000;
    tv_sec := Rate div 1000000;
  end;

  procedure DoManual;
  var CommItem : TCommItem;
  begin
    TagName := ReadQuotedString(Packet);
    CommItem := FindCommItem(TagName);
    if Assigned(CommItem) then begin
      RemoveCallback(CommItem.OPCItem.Tag);
      WriteStandard('manual', CommItem, True);
    end else begin
      WriteStandard('manual ', nil, False, TagName);
    end;
  end;

begin
  Tmp := LowerCase(ReadQuotedString(Packet));
  if Tmp = 'manifest' then
    DoManifest
  else if Tmp = 'fullmanifest' then
    DoFullManifest
  else if Tmp = 'get' then
    DoGet
  else if Tmp = 'set' then
    DoSet
  else if Tmp = 'rate' then
    DoRate
  else if Tmp = 'auto' then
    DoAuto
  else if Tmp = 'manual' then
    DoManual
end;

procedure TServSockWinSocket.AddCallback(aTag : TAbstractTag);
begin
  if ClientTags.IndexOf(aTag) = -1 then begin
    ClientTags.Add(aTag);
    aTag.AddCallback(LocalEvent, edgeChange, 0);
  end;
end;

procedure TServSockWinSocket.RemoveCallback(aTag : TAbstractTag);
var I : Integer;
begin
  I := ClientTags.IndexOf(aTag);
  if I <> -1 then
    ClientTags.Delete(I);
  aTag.RemoveCallback(LocalEvent);
end;

procedure TServSockWinSocket.WriteString(aText : string);
begin
  AddDispatch(aText)
end;

procedure TServSockWinSocket.LocalEvent(aTag : TAbstractTag);
var I : Integer;
    CommItem : TCommItem;
begin
  if not Connected then
    Exit;

  if aTag.NetWorkAlias <> '' then
    I := Manifest.IndexOf(LowerCase(aTag.NetWorkAlias))
  else
    I := Manifest.IndexOf(LowerCase(aTag.Name));
  if I <> -1 then begin
    CommItem := TCommItem(Manifest.Objects[I]);
    CommItem.UpdateRequired := CommItem.LastValue <> aTag.AsString;
  end;
end;


procedure TServSockWinSocket.SendCommItem(CommItem: TCommItem);
begin
  WriteString('data ' + QuotedString(CommItem.OPCItem.Tag.NetWorkAlias) + ' ' +
                        QuotedString(CommItem.LastValue) + ' ' +
                        QuotedString(CommItem.OPCItem.Tag.ClassName));
end;

procedure TServSockWinSocket.AddDispatch(aDispatch: string);
begin
  ReadWriteLock.Enter;
  try
    DispatchList.Add(aDispatch)
  finally
    ReadWriteLock.Release;
  end;
end;

procedure TServSockWinSocket.AddEvent(aEvent: string);
begin
  ReadWriteLock.Enter;
  try
    EventList.Add(aEvent)
  finally
    ReadWriteLock.Release;
  end;
end;

procedure TServSockWinSocket.DeleteDispatch;
begin
  ReadWriteLock.Enter;
  try
    if DispatchList.Count > 0 then
      DispatchList.Delete(0);
  finally
    ReadWriteLock.Release;
  end;
end;

procedure TServSockWinSocket.DeleteEvent;
begin
  ReadWriteLock.Enter;
  try
    if EventList.Count > 0 then
      EventList.Delete(0);
  finally
    ReadWriteLock.Release;
  end;
end;

{ TDispatchThread }

constructor TDispatchThread.Create(Suspended : Boolean; aServer: TServSockWinSocket);
begin
  Server := aServer;
  inherited Create(Suspended, aServer);
  FEvent := TSimpleEvent.Create;
end;

destructor TDispatchThread.Destroy;
begin
   FEvent.Free;
  inherited;
end;

function TDispatchThread.ReadAsStream(var Buffer; Count: Longint): Longint;
var
  Overlapped: TOverlapped;
  Timeout : Integer;
begin
  TimeOut := (Server.tv_sec * 1000) + (Server.tv_usec div 1000);

  FillChar(OVerlapped, SizeOf(Overlapped), 0);
  Overlapped.hEvent := FEvent.Handle;

  if not ReadFile(ClientSocket.SocketHandle, Buffer, Count, DWORD(Result),
    @Overlapped) and (GetLastError <> ERROR_IO_PENDING) then begin

    Terminate;
  end;
  if FEvent.WaitFor(TimeOut) <> wrSignaled then
    Result := 0
  else begin
    GetOverlappedResult(ClientSocket.SocketHandle, Overlapped, DWORD(Result), False);
    FEvent.ResetEvent;
  end;
end;

procedure TDispatchThread.ReadString;
var LField : TStreamingUnion;
begin
  ReadAsStream(LField.B[0], 4);

  if LField.I >= RxBuffSize then begin
    RxBuffSize := LField.I + 10;
    ReallocMem(RxBuff, RxBuffSize);
  end;

  ReadAsStream(RxBuff[0], LField.I);

  RxBuff[LField.I] := #0;
  Server.AddEvent(RxBuff);

  Synchronize(Server.DoRead);
end;

procedure TDispatchThread.WriteString(aText: string);
var I : Integer;
begin
  try
    I := Length(aText);

    if Server.SendBuf(I, SizeOf(I)) = 0 then begin
      Server.Disconnect(Server.SocketHandle);
      EventLog('Disconnect on failed write 1: ');
      Exit;
    end;
    if Server.SendText(aText) = 0 then begin
      Server.Disconnect(Server.SocketHandle);
      EventLog('Disconnect on failed write 2: ');
      Exit;
    end;
  except
    on E : Exception do begin
      EventLog('Exception on write: ' + E.Message);
      try
        Server.Disconnect(Server.SocketHandle);
      except
        EventLog('Failed disconnect');
      end;
    end;
  end;
end;

procedure TDispatchThread.WriteSocket;
var I : Integer;
    CommItem : TCommItem;
begin
  while Server.DispatchList.Count > 0 do begin
    WriteString(Server.DispatchList[0]);
    Server.DeleteDispatch;
  end;

  for I := 0 to Server.Manifest.Count - 1 do begin
    if Terminated then
      Break;
    CommItem := TCommItem(Server.Manifest.Objects[I]);
    if CommItem.UpdateRequired then begin
      CommItem.LastValue := CommItem.OPCItem.Tag.AsString;
      CommItem.UpdateRequired := False;
      Server.SendCommItem(CommItem);
    end;
  end;
end;


procedure TDispatchThread.ClientExecute;
var
  FDSet: TFDSet;
  TimeVal: TTimeVal;
begin
  Server := TServSockWinSocket(ClientSocket);
  while not Terminated and ClientSocket.Connected do begin
    FD_ZERO(FDSet);
    FD_SET(ClientSocket.SocketHandle, FDSet);
    TimeVal.tv_sec := Server.tv_sec;
    TimeVal.tv_usec := Server.tv_usec;
    if (select(0, @FDSet, nil, nil, @TimeVal) > 0) and not Terminated then
      if ClientSocket.ReceiveBuf(FDSet, -1) = 0 then
        Break
      else
        ReadString;

//    if (select(0, nil, @FDSet, nil, @TimeVal) > 0) and not Terminated then
    if not Terminated and ClientSocket.Connected then
      WriteSocket;
  end;
end;

initialization
  RegisterCNCClass(TServerSocketOPC);
end.
