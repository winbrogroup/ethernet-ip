unit FTPTelnetClient;

interface
uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,SyncObjs,GeneralTelnetClient,
  Dialogs, StdCtrls, IdBaseComponent, IdComponent, IdFTP,  ComCtrls,ExtCtrls,CognexTypes;


type
//TRequestCompleteEvent = procedure(Success : Boolean) of object;
TConnectionChangeEvent  = procedure(IsConnected : Boolean) of object;
TFTPActionCompleteEvent = procedure(ActionType : TFTPActionTypes;Success : Boolean) of object;

TTelnetFTPCommand = Class(TObject)
  CommandStr :  String;
  Response   : String;
  CommandType : TFTPCommandTYpes;
  ServiceInterval : Integer;
  procedure AssignTo(Dest : TTelnetFTPCommand);
  procedure AssignFrom(Source: TTelnetFTPCommand);
  constructor Create(SInterval : Integer);
  end;

TFTPTelnetClient = class(TGeneralTelnetClient)
 private
    FOnTelnetConnected: TConnectionChangeEvent;
    FOnFTPConnected: TConnectionChangeEvent;
    CurrentlyConnected : Boolean;
    ServiceTimer : TTimer;
    ResponseTimeout : TTimer;
    ServiceMode : TFTPServiceModes;
    Restart : Boolean;
    FOnActionComplete: TFTPActionCompleteEvent;
    CurrentCommand : TTelnetFTPCommand;
    ResponseCount : Integer;
    FIsIdle: Boolean;
    ExpectedFiles : Integer;
    procedure FTPTimeout(Sender: Tobject);
    procedure GotData(Sender: Tobject; Response: String);
    procedure ConnectToFtp;
    procedure ServiceCommands(SEnder : TObject);
    procedure SendFTPCommand(CommandString: String; CType: TFTPCommandTYpes);
    procedure TerminateCurrentCommand(CType: TFTPCommandTYpes;Success : Boolean);

 public
    ExternalAllFileList : TStringList; // Externally Set
    aExternalJobList : TStringList;     // Externally Set
    procedure RequestFileList;
    procedure RequestJobList;
    procedure RequestInitialJobList;
    function RequestConnect : Boolean;
    procedure TelnetHasConnected(Sender : TObject);
    constructor Create(AOwner: TComponent); override;
    property IsIdle : Boolean read FIsIdle;
    property OnTelnetConnectionChange : TConnectionChangeEvent read FOnTelnetConnected write FOnTelnetConnected;
    property OnFTPConnectionChange    : TConnectionChangeEvent read FOnFTPConnected write FOnFTPConnected;
    property OnFTPActionComplete : TFTPActionCompleteEvent read FOnActionComplete write FOnActionComplete;
//    property OnRequestComplete : TRequestCompleteEvent read FOnRequestComplete write FOnRequestComplete;
 end;

 implementation

{ TFTPTelnetClient }

procedure TFTPTelnetClient.ConnectToFtp;
begin

end;

constructor TFTPTelnetClient.Create(AOwner: TComponent);
begin
  inherited;
  FIsIdle := True;
  HostIPAddress := '10.0.4.25';
  PortNumber := 23;
  OnConnected := TelnetHasConnected;
  OnDataInBuffer := GotData;

  ServiceTimer := TTimer.Create(nil);
  with ServiceTimer do
    begin
    Enabled := False;
    Interval := 30;
    OnTimer := ServiceCommands;
    end;

  ResponseTimeout := TTimer.Create(nil);
  with ResponseTimeout do
    begin
    Enabled := False;
    Interval := 5000;
    OnTimer := FTPTimeout;
    end;
  CurrentCommand := TTelnetFTPCommand.Create(40);
end;

procedure TFTPTelnetClient.SendFTPCommand(CommandString : String;CType :TFTPCommandTYpes);
begin
if assigned(CurrentCommand) then
  begin
  CurrentCommand.CommandStr := CommandString;
  CurrentCommand.CommandType := CType;
  ServiceMode := fsmSend;
  FIsIdle := False;
  ServiceTimer.Enabled := True;
  ResponseCount := 0;
  end;
end;
//TFTPActionTypes = (taIdle,taWaiting,taTelnetConnect,taConnectandLogin,taGetJobList,taGetFileList,taImportFile,TaExportFile,taDeleteFiles);
procedure TFTPTelnetClient.TerminateCurrentCommand(CType :TFTPCommandTYpes;Success : Boolean);
begin
Case CTYpe of
  ftpGetFileList:
    begin
    if assigned(FOnActionComplete) then FOnActionComplete(taGetFileList,Success);
    ServiceMode := fsmIdle;
    FIsIdle := True;
    end;

  ftpGetJobList:
    begin
    if assigned(FOnActionComplete) then FOnActionComplete(taGetJobList,Success);
    ServiceMode := fsmIdle;
    FIsIdle := True;
    end;

  ftpGetInitialJobList:
    begin
    if assigned(FOnActionComplete) then FOnActionComplete(taGetInitialJobList,Success);
    ServiceMode := fsmIdle;
    FIsIdle := True;
    end;

{  ftpGetInitialJobList:
    if assigned(FOnActionComplete) then FOnActionComplete(taGetInitialJobList,Success);
    ServiceMode := fsmIdle;
    FIsIdle := True;
    end;}

  FtpImport:
    begin
    end;
 end; // case
end;

procedure TFTPTelnetClient.GotData(Sender: Tobject; Response: String);
begin

Case CurrentCommand.CommandType of
  ftpGetFileList:
    begin
    end;

  ftpGetJobList,ftpGetInitialJobList:
    begin

    if ResponseCount = 0 then
      begin
      aExternalJobList.Clear;
      ExpectedFiles := StrToIntDef(Response,0);
      if ExpectedFiles > 0 then
        begin
        Inc(ResponseCount);
        exit;
        end
      else
        begin
        ResponseTimeout.Enabled := False;
        If ExpectedFiles = 0 then
            TerminateCurrentCommand(CurrentCommand.CommandType,True)
        else
            TerminateCurrentCommand(CurrentCommand.CommandType,False)
        end
      end;
    if ResponseCount > 0 then
      begin
      inc(ResponseCount);
      aExternalJobList.Add(Response);
      Dec(ExpectedFiles);
      If ExpectedFiles = 0 then
        begin
        TerminateCurrentCommand(CurrentCommand.CommandType,True);
        end;
      end;

    end;

  FtpImport:
    begin
    end;

  ftpExport:
    begin
    end;
  end; // case
end;


procedure TFTPTelnetClient.RequestFileList;
begin
if CurrentlyConnected then
  begin

  end;
end;


procedure TFTPTelnetClient.RequestJobList;
begin
if CurrentlyConnected and Isidle then
  begin
  SendFTPCommand('GJ',ftpGetJobList);
  end;
end;

procedure TFTPTelnetClient.RequestInitialJobList;
begin
if CurrentlyConnected and Isidle then
  begin
  SendFTPCommand('GJ',ftpGetInitialJobList);
  end;
end;

Function TFTPTelnetClient.RequestConnect:Boolean;
begin
 Result := True;
 if not CurrentlyConnected then
   begin
   if ConnectToServer(True,'CognexFTP') then
    begin
     if assigned(FOnActionComplete) then FOnActionComplete(taFTPTelnetConnect,True)
    end
   else
    begin
     if assigned(FOnActionComplete) then FOnActionComplete(taFTPTelnetConnect,False)
    end
   end
 else
   Begin
   end;
end;

procedure TFTPTelnetClient.ServiceCommands(SEnder: TObject);
begin
ServiceTimer.Enabled := False;
Restart := False;
 case ServiceMode of
  fsmIdle:
    begin
    end;

  fsmSend:
    begin
    ResponseTimeout.Enabled := True;
    SendCommand(CurrentCommand.CommandStr,False,False);
    ServiceMode := FSMWait;
    end;

  fsmwait:
    begin
    end;

 end;
 if restart then
   ServiceTimer.Enabled := True;

end;

procedure TFTPTelnetClient.TelnetHasConnected(Sender: TObject);
begin
CurrentlyConnected := True;
if assigned(FOnTelnetConnected) Then FOnTelnetConnected(True);
end;


procedure TFTPTelnetClient.FTPTimeout(Sender: Tobject);
begin
ResponseTimeout.Enabled := False;
{ TODO : Event here }
end;

{ TTelnetFTPCommand }

procedure TTelnetFTPCommand.AssignTo(Dest: TTelnetFTPCommand);
begin
  Dest.CommandStr := CommandStr;
  Dest.Response   := Response;
  Dest.CommandType := CommandType;
  Dest.ServiceInterval := ServiceInterval;

end;

procedure TTelnetFTPCommand.AssignFrom(Source: TTelnetFTPCommand);
begin
  CommandStr := Source.CommandStr;
  Response := Source.Response;
  CommandType := Source.CommandType;
  ServiceInterval := Source.ServiceInterval;


end;

constructor TTelnetFTPCommand.Create(SInterval: Integer);
begin
ServiceInterval := SInterval;
Response := '';
CommandStr := '';
end;

end.
