unit CognexTypes;

interface

Type
//                {$IFDEF FPC}
//;  {$MODE DELPHI}
//;  {$IFDEF WIN32}
{$DEFINE LOWDEBUG}
TFTPServiceModes = (fsmIdle,fsmSend,fsmwait);

TNCCommands = (  tncNone,tncService,tncLoginResponse,tncLoadDefaultOnReset,tncGoLiveOnReset,
                 tncLoadDefaultOnXHairBuffer,
                 tncFromForm,
                 tncTestTalkingLogin,tncTestTalkingReconect,tncTestTalkingPowerOnReset,tncTestTalkingReset,tncTestGetFilename,
                 tncTestTalkingRestore,tncGetSpreadsheetValue,tncGetCellValue,tncSendNativeCommand,
                 tncGetSymbolicValue,tncSetSymbolicValueAsString,tncSetSymbolicValueAsFloat,tncSetSymbolicValueAsInteger,
                 tncOffLineForJobLoad,tncJobLoadStart,tncLoadJobCalled,tncGetJobList,
                 tncGetCurrentJobName,tncResetExerciseS02,tncDoneInitialOnLineConditioning,tncSetOfflineOnReset,
                 tncSetOfflineOnDisconnect,tncCheckOnline,
                 tncSetOfflineForLiveVideo,tncSetLiveState,tncSetExposure,
                 tncSaveSnapshot,tncSaveTelnet,
                 tncInvalidFunction,tncFollowOn,
                 tncRunJobOnLine1,tncRunJobOnLine2,tncFollowOnLast,
                 tncSetJobScaling,tncSetJobXOffset,tncSetJobYOffset,
                 tncSettingOnLine,tncSettingOffLine,
                 tncResetandRunJob,tncResetandRunJobFastFirst,
                 tncRunJobGetStartCount,
                 tncResetandRunJobFastInMode,
                 tncResetJobDownAfterLoad,tncLiveAfterLoad,tncResetJobDownBeforeRun,
                 tncRunJob,tncRunJobFastFirst,tncRunJobFastInMode,
                 tncOffLineAfterJobRun,tncLiveAfterJobRun,tncCheckJobPass,tncCheckPassAfterFastRun,
                 tncStartGeneralRestore,tncRestoreCamera,tncLastFitCommand,
                 tncStartRestoreCameraForBuffer,tncRestoreCameraForBufferEnd,
                 tncSetJobExposure,tncSetCameraMode,tncSetScalingAfterJobLoad,tncGetErrorAtIndex,
                 tncOnLineForImport,tncLoadFTPFile,tncOffLineForImport,tncGetUserExposure
                 );

TVisValTypes = (tvvInteger,tvvFloat,tvvString);

TVisionDisplayModes = (vdmUnknown,vdmPOwerUp,vdmCamera,vdmTelnet,vdmTelnetAndCamera,

{$IFDEF GRID_MODE_VALID}
vdmGrid,
{$ENDIF}
vdmSystem,vdmDisconnecting,vdmDisconnected,vdmReconnecting,vdmRecconnected,vdmError);

TFTPDisplayModes = (ftpDisplayStd,ftpDisplayDebug,ftpDual);
TFTPActionTypes = (taIdle,taWaiting,taFTPTelnetConnect,taConnectandLogin,taGetJobListAtPowerUp,taGetInitialJobList,taGetJobList,taGetFileList,taImportFile,TaExportFile,taDeleteFiles);
TFTPCommandTYpes = (ftpGetFileList,ftpGetInitialJobList,ftpGetJobList,FtpImport,ftpExport);


TVJogModes = (jmNone,jmHairUp,jmHairDown,jmHairLeft,jmHairRight,jmCircleUp,jmCircleDown,jmScaleUp,jmScaleDown,jmBigger,lmSmaller);

TTalkingModes = (tmAtLogin,tmAtReconnect,tmPowerOnReset,tmGeneralReset,tmRestore);

TNCLanguageFunctions = ( tccNone,tccSendNativeCommand,tccGetSpreadsheetValue,tccGetSymbolicValue,tccSetSymbolicInteger,
                         tccSetSymbolicFloat,tccSetSymbolicString,tccLoadJobCalled,tccGetCurrentJobName,tccSetLiveVideo,
                         tccResetandRunJob,tccSetJobScaling,tccSetJobOffset,tccCheckJobPass,tccSetExposure,
                         tccSetCameraView,tccBufferToXHair,tccResetErrorStack,tccGetErrorAtIndex,
                         tccLoadFileFromDisk,tccSaveFileToDisk,
                         tccSaveImageToFileWithGraphics,tccSaveImageToFileNoGraphics,tccPartLifeSnapshotWithGraphics,
                         tccPartLifeSnapshotNoGraphics,tccSaveTelnetLog,tccGetUserExposure);

TCameraActionStages =(  tcaIdlle,tcaWait,
                        tcaInitialLoginStart,tcaInitialLoginDone,
                        tcaOffLineForJobLoad,tcaLoadJobStart,tcaLoadJobDone,tcaLoadJobFailed,
                        tcaRestoreCheckStart,tcaRestoreStart,tcaRestoreDone,
                        tcaStartFit,tcaFitDone,
                        tcaStartFullReset,tcaResetSetOffline,tcaResetExerciseS02,tcaReset0,tcaReset1,tcaReset2,tcaReset3,tcaResetEnd,
                        tcaStartDiscOnRecon,
                        tcaStartXhairEstablish,tcaXhairEstablishEnd,
//                        tcaStartBufferXHair,tcaBufferXHairRestore,tcaBufferXHairEnd,
                        tcaDisconnectStart,tcaTelnetDisconnect,tcaCameraDisconect,tcaDisconnectWait,tcaDisconnectDone,
                        tcaReconnectStart,tcaReconnectGetCurrentJob,
                        tcaValidateJobOnReconnect,tcaReconnectDone,tcaReconnectError,
                        tcaStartRunJob,tcaLiveAfterRun,tcaCheckPassAfterFastRun,tcaRunJobDone,
                        tcaStartFastRunJob,tcaStartFastRunJobFirst,tcaStartFastRunJobFInMode,
                        tcaStartCheckJobPass,tcaJobPassGoodResult,tcaJobPassBadResult,
                        tcaFTPLoginStart,tcaFTPLoginGetJobList,tcaFTPLoginDone,
                        tcaImportFileStart,tcaOnLineForImport,tcaFileImported,TcaOfflineForImport,
                        tcaSnapShotStart,tcaSnapShotCheckFile,tcaSnapshotDone,tcaSnapshotFail,
                        tcaGetValueStart,tcaGetValueDone,tcaSetValueStart,tcaSetValueDone,
                        tcaTelnetSaveStart,tcaTelnetSaveCheckFile,tcaTelnetSaveDone,tcaTelnetSaveFail);



TFTPActionStages = (ftpsIdle,ftpsConnectandLoginStart,ftpsConnectandLoginDone,ftpsRefreshStart,ftpsRefreshDone);

TVModes = (tvmOff,tvActionStageOnly,tvmOutOnly,tvmDuplex,tvmDebug,tvmSuperDebug);

implementation

end.
