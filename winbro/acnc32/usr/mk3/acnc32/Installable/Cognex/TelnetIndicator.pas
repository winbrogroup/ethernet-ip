unit TelnetIndicator;

interface
uses CoreCNC, CNCTypes, CNC32, Classes, Windows, Controls, Graphics,ExtCtrls, StdCtrls;

type
TTelnetIndicator = class(TObject)
 private
    Indicator : TShape;
    Highlight : TShape;
    FOnColour: TColor;
    FOffColour: TColor;
    FOnStatus : Boolean;
    FHighlightOnColour: TColor;
    FHighLightOffColour: TColor;
    FDiameter: Integer;
    FLeft: Integer;
    FTop: Integer;
    procedure ReDraw;

    procedure SetOnColour(const Value: TColor);
    procedure SetHighLightonColour(const Value: TColor);
    procedure SetOffColour(const Value: TColor);
    procedure SetOnStatus(const Value: Boolean);
    procedure SetHighLightoffColour(const Value: TColor);
    procedure SetDiameter(const Value: Integer);
    procedure SetLeft(const Value: Integer);
    procedure SetTop(const Value: Integer);
 public
    constructor CreateOnParent(AParent: TWinControl);
    destructor Destroy; override;

    property OnColour : TColor read FOnColour write SetOnColour;
    property OnStatus : Boolean read FOnStatus write SetOnStatus;
    property OffColour : TColor read FOffColour write SetOffColour;
    property Diameter : Integer read FDiameter write SetDiameter;
    property HighLightOnColour : TColor read FHighlightOnColour write SetHighLightonColour;
    property HighLightOffColour : TColor read FHighLightOffColour write SetHighLightoffColour;
    property Left : Integer read FLeft write SetLeft;
    property Top  : Integer read FTop write SetTop;
 end;

 TNamedIndicator = Class(TObject)
   private
    NamePanel : TPanel;
    IndicatorLight : TTelnetIndicator;
    ValueLabel  : TLabel;
    LegendLabel : TLabel;
    FOffColour: TColor;
    FHighLightOffColour: TColor;
    FOnColour: TColor;
    FHighlightOnColour: TColor;
    FOnStatus: Boolean;
    MyParent : TWinControl;
    FPanelHeight: Integer;
    FPanelWidth: Integer;
    FPanelLeft: Integer;
    FPanelTop: Integer;
    FLegend: String;
    FHasValue : Boolean;
    FStrValue: String;
    FValueWidth: Integer;
    procedure RedrawOnResize(Sender : TObject);
    procedure SetHighLightoffColour(const Value: TColor);
    procedure SetHighLightonColour(const Value: TColor);
    procedure SetOffColour(const Value: TColor);
    procedure SetOnColour(const Value: TColor);
    procedure SetOnStatus(const Value: Boolean);
    procedure SetPanelHeight(const Value: Integer);
    procedure SetPanelWidth(const Value: Integer);
    procedure SetPanelLeft(const Value: Integer);
    procedure SetPanelTop(const Value: Integer);
    procedure SetLegend(const Value: String);
    procedure SetStrValue(const Value: String);
   public
    constructor CreateOnParent(AParent: TWinControl;HasValue : Boolean);
    destructor Destroy; override;
    procedure Redraw;
    property PanelWidth : Integer read FPanelWidth write SetPanelWidth;
    property PanelHeight : Integer read FPanelHeight write SetPanelHeight;
    property PanelLeft : Integer   read FPanelLeft write SetPanelLeft;
    property PanelTop : Integer   read FPanelTop write SetPanelTop;
    property OnColour : TColor read FOnColour write SetOnColour;
    property OnStatus : Boolean read FOnStatus write SetOnStatus;
    property OffColour : TColor read FOffColour write SetOffColour;
    property HighLightOnColour : TColor read FHighlightOnColour write SetHighLightonColour;
    property HighLightOffColour : TColor read FHighLightOffColour write SetHighLightoffColour;
    property Legend : String read FLegend write SetLegend;
    property ValueAsString : String Read FStrValue write SetStrValue;
    property ValueWidth : Integer read FValueWidth write FValueWidth;
   end;


implementation

{ TTelnetIndicator }


destructor TTelnetIndicator.Destroy;
begin
  Indicator.Free;
  Highlight.Free;
  inherited;
end;

procedure TTelnetIndicator.SetOffColour(const Value: TColor);
begin
  FOffColour := Value;
end;

procedure TTelnetIndicator.SetHighLightonColour(const Value: TColor);
begin
  FHighlightOnColour := Value;
end;

procedure TTelnetIndicator.SetOnColour(const Value: TColor);
begin
  FOnColour := Value;
end;

procedure TTelnetIndicator.ReDraw;
var
StartX,StartY : Integer;
begin
If FOnStatus then
  begin
  Indicator.Brush.Color := FOnColour;
  Highlight.Brush.Color := FHighlightOnColour;
  Highlight.Pen.Color   := FHighlightOnColour;
  end
else
  begin
  Indicator.Brush.Color := FOffColour;
  Highlight.Brush.Color := FHighlightOffColour;
  Highlight.Pen.Color   := FHighlightOffColour;
  end;
 If Indicator.HasParent then
  begin
  Indicator.Height := FDiameter;
  Indicator.Width  := FDiameter;
  Indicator.Left := FLeft;
  Indicator.Top := FTop;
  Highlight.Width := Indicator.Width div 3;
  Highlight.Height := Indicator.Height div 3;
  StartX := (Indicator.Width div 5);
  StartY := Indicator.Height div 4;
  Highlight.Top :=  Indicator.Top+ StartY;
  Highlight.Left := Indicator.Left+StartX;
  Highlight.BringToFront;
  end;

end;

procedure TTelnetIndicator.SetOnStatus(const Value: Boolean);
begin
  FOnStatus := Value;
  Redraw;
end;

procedure TTelnetIndicator.SetHighLightoffColour(const Value: TColor);
begin
  FHighLightOffColour := Value;
end;

constructor TTelnetIndicator.CreateOnParent(AParent: TWinControl);
begin
  inherited Create;
  FDiameter := 20;
  Indicator := TShape.Create(nil);
  Indicator.Parent := AParent;
  with Indicator do
    begin
    Shape := StCircle;
    Pen.Color := clBlack;
    end;

  Highlight := TShape.Create(Nil);
  Highlight.Parent := AParent;
  with Highlight do
    begin
    Shape := StCircle;
    end;


end;

procedure TTelnetIndicator.SetDiameter(const Value: Integer);
begin
  FDiameter := Value;
  Redraw;
//  Indicator.Height := FRadius div 2;
//  Indicator.Width  := FRadius div 2;
//  redraw;
end;

procedure TTelnetIndicator.SetLeft(const Value: Integer);
begin
  FLeft := Value;
  Redraw;
//  if Indicator.HasParent then
//    begin
//    Indicator.Left := Value;
//    Redraw;
//    end;
end;

procedure TTelnetIndicator.SetTop(const Value: Integer);
begin
  FTop := Value;
  Redraw;
{  if Indicator.HasParent then
    begin
    Indicator.Top := Value;
    Redraw;
    end;}
end;

{ TNamedIndicator }


procedure TNamedIndicator.SetOffColour(const Value: TColor);
begin
  FOffColour := Value;
  if assigned(IndicatorLight) then IndicatorLight.OffColour := FOffColour;
end;

procedure TNamedIndicator.SetHighLightoffColour(const Value: TColor);
begin
  FHighLightOffColour := Value;
  if assigned(IndicatorLight) then IndicatorLight.HighLightOffColour := FHighLightOffColour;
end;

procedure TNamedIndicator.SetOnColour(const Value: TColor);
begin
  FOnColour := Value;
  if assigned(IndicatorLight) then IndicatorLight.FOnColour := Value;
end;

procedure TNamedIndicator.SetHighLightonColour(const Value: TColor);
begin
  FHighlightOnColour := Value;
  if assigned(IndicatorLight) then IndicatorLight.HighLightOnColour := FHighlightOnColour;
end;

procedure TNamedIndicator.SetOnStatus(const Value: Boolean);
begin
  FOnStatus := Value;
  Redraw;
end;


constructor TNamedIndicator.CreateOnParent(AParent: TWinControl;HasValue : Boolean);
begin
  inherited Create;
  MyParent := AParent;
  FHasValue := HasValue;
  FValueWidth := 30;
  NamePanel := TPanel.Create(nil);
  NamePanel.Parent := MyParent;
  with NamePanel do
    begin
    Width := 150;
    Height := 30;
    OnResize := RedrawOnResize;
    end;
  LegendLabel := TLabel.Create(nil);
  LegendLabel.Parent := NamePanel;
  with LegendLabel do
    begin
    AutoSize := False;
    Caption := 'Legend';
    end;
  if FHasValue then
    begin
    ValueLabel :=  TLabel.Create(nil);
    ValueLabel.Parent := NamePanel;
    with ValueLabel do
      begin
      AutoSize := False;
      Caption := '';
      end;
    end
  else
    begin
    IndicatorLight := TTelnetIndicator.CreateOnParent(NamePanel);
    IndicatorLight.Left := 4;
    IndicatorLight.Top := 4;
    IndicatorLight.OnColour := clGreen;
    IndicatorLight.OffColour := clred;
    IndicatorLight.HighLightOnColour := clYellow;
    IndicatorLight.HighLightOffColour := clSilver;
    IndicatorLight.Diameter := 16;
    end;

end;

procedure TNamedIndicator.Redraw;
begin
  if assigned(MyParent) then
  try
   NamePanel.Width := FPanelWidth;
   NamePanel.Height := FPanelHeight;
   NamePanel.Left := FPanelLeft;
   NamePanel.Top :=  FPanelTop;
   LegendLabel.Align := alRight;
   LegendLabel.Caption := FLegend;
   if FHasValue Then
      begin
      ValueLabel.Caption := FStrValue;
      ValueLabel.Width := FValueWidth;
      ValueLabel.align := AlLeft;
      ValueLabel.align := AlNone;
      ValueLabel.Left := 4;
      LegendLabel.Align := alRight;
      LegendLabel.Width := NamePanel.ClientWidth-ValueLabel.Width-6;
      end
   else
      begin
      LegendLabel.Align := alRight;
      IndicatorLight.Diameter := (NamePanel.ClientHeight-8-NamePanel.BevelWidth);
      LegendLabel.Width := NamePanel.ClientWidth-IndicatorLight.Diameter-15;
      IndicatorLight.OnStatus := FOnStatus;
      end;
  except
  end;
end;

procedure TNamedIndicator.SetPanelHeight(const Value: Integer);
begin
  FPanelHeight := Value;
  NamePanel.Height := FPanelHeight;
end;

procedure TNamedIndicator.SetPanelWidth(const Value: Integer);
begin
  FPanelWidth := Value;
  NamePanel.Width := FPanelWidth;
end;

procedure TNamedIndicator.SetPanelLeft(const Value: Integer);
begin
  FPanelLeft := Value;
end;


procedure TNamedIndicator.SetPanelTop(const Value: Integer);
begin
  FPanelTop := Value;
end;

procedure TNamedIndicator.SetLegend(const Value: String);
begin
  FLegend := Value;
  
end;

destructor TNamedIndicator.Destroy;
begin
  NamePanel.Free;
  if assigned(IndicatorLight) then IndicatorLight.Free;
  if assigned(ValueLabel) then ValueLabel.Free;
  LegendLabel.Free;
  inherited;
end;

procedure TNamedIndicator.RedrawOnResize(Sender: TObject);
begin
Redraw;
end;


procedure TNamedIndicator.SetStrValue(const Value: String);
begin
  FStrValue := Value;
  Redraw;
end;

end.
