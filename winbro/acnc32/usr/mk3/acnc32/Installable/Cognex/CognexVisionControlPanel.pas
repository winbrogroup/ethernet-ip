unit CognexVisionControlPanel;

interface
uses CoreCNC, CNCTypes, CNC32, Classes, Windows, Graphics, SysUtils, NCNames,
  CNCAbs, Named, Controls,ExtCtrls,ActiveX,CognexTelnetClient,StdCtrls,Grids,
  AbstractScript,IScriptInterface,INamedInterface,IniFiles,Forms,Buttons;

const
ButtonSpacing=1;
ZoomLabelHeight = 24;
CircleLabelHeight= 24;
XHairLabelHeight= 24;
FocusPanelHeight = 40;
FormBevelWidth = 3;
BigFontSize = 16;
type
  TMoveModes = (mmIncremental,mmContinuous);

  TCognexControlPanel = class(TExpandDisplay)
  private
    MainButtonPanel : TPanel;
    ZoomLabel : TPanel;
    CircleLabel: TPanel;
    XHairLabel : TPanel;
    FocusPanel : TPanel;
    UpButton : TBitBtn;
    DownButton : TBitBtn;
    LeftButton : TBitBtn;
    RightButton : TBitBtn;
    ResetButton : TBitBtn;
    CircleOnButton : TSpeedButton;
    XHairOnButton : TSpeedButton;
    CircleColourButton : TBitBtn;
    XHairColourButton : TBitBtn;
    ContinuousButton : TSpeedButton;
    IncrementalButton : TSpeedButton;
    ZoomUpButton  : TBitBtn;
    ZoomDownButton  : TBitBtn;
    FitButton       : TBitBtn;
    FullScaleButton : TBitBtn;
    BiggerButton    : TBitBtn;
    SmallerButton   : TBitBtn;

    MoveMode : TMoveModes;
    InstalledVisionName : String;
    SceneScalePercent : Double;



    XHairResetTag : TAbstractTag;
    XHairUpTag : TAbstractTag;
    XHairDownTag : TAbstractTag;
    XHairLeftTag : TAbstractTag;
    XHairRightTag : TAbstractTag;
    ReqXHairOnTag : TAbstractTag;
    XHairJogDownTag : TAbstractTag;
    XHairJogUpTag : TAbstractTag;
    XHairJogLeftTag : TAbstractTag;
    XHairJogRightTag : TAbstractTag;
    IncrementModeTag : TAbstractTag;
    FitToPanelTag     : TAbstractTag;
    ImageScaleTag     : TAbstractTag;
    CentreOnXHairTag  : TAbstractTag;

    CircleUpTag  : TAbstractTag;
    ReqCircleOnTag  : TAbstractTag;
    CircleIsOnTag   : TAbstractTag;
    XHairIsOnTag   : TAbstractTag;

    CircleDownTag : TAbstractTag;
    CircleJogUpTag  : TAbstractTag;
    CircleJogDownTag : TAbstractTag;

    ScaleUpTag : TAbstractTag;
    ScaleDownTag: TAbstractTag;
    ScaleJogUpTag : TAbstractTag;
    ScaleJogDownTag: TAbstractTag;
    CircleColorNumberTag: TAbstractTag;
    XHairColorNumberTag: TAbstractTag;

    FirstShow : Boolean;


    procedure FormatButtons;
    procedure RefreshStatus;
    procedure DoSceneScaleChanged(ATag: TAbstractTag);
    procedure DoCircleIsOnChange(ATag: TAbstractTag);
    procedure DoXHairIsOnChange(ATag: TAbstractTag);
    procedure DoControlsValidChange(ATag: TAbstractTag);
    procedure XHairUpMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure XHairUpMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure XHairDownMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure XHairDownMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure XHairLeftMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure XHairLeftMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure XHairRightMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure XHairRightMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure BiggerMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure BiggerMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure SmallerMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure SmallerMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure ScaleUpMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure ScaleUpMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure ScaleDownMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure ScaleDownMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure CircleOnMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure CircleOnMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure XHairOnMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure XHairOnMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure CircleColourMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure CircleColourMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure XHairColourMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure XHairColourMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);

    procedure MoveModeButtonClick(Sender: TObject);
    procedure FitButtonClick(Sender: TObject);
    procedure FullButtonClick(Sender: TObject);
    procedure ConScreenButtonClick(Sender: TObject);
    procedure ConXButtonClick(Sender: TObject);
    procedure ResetXHairClicked(Sender: TObject);

  public
    CoarseButton : TSpeedButton;
    MediumButton : TSpeedButton;
    FineButton : TSpeedButton;
    COnscreenButton : TSpeedButton;
    COnXHairButton : TSpeedButton;

    constructor Create(aOwner : TComponent); override;
    procedure InstallComponent(Params : TParamStrings); override;
    procedure Installed; override;
    procedure Repaint; override;
    procedure IShutdown; override;
    procedure OPen; override;
  end;

implementation

{ TCognexDisplay }

constructor TCognexControlPanel.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
  Self.BevelWidth := 1;
  Self.BevelInner := bvNone;
  Self.BevelOuter := bvNone;

  MainButtonPanel := TPanel.create(Self);
  MainButtonPanel.Parent:= Self;
  with MainButtonPanel do
    begin
    Align := alClient;
    Color := clMoneyGreen;
    BevelInner := bvRaised;
    BevelWidth := 1;
    Caption := '';
    Visible := True;
    end;

  ZoomLabel := TPanel.Create(Self);
  ZoomLabel.Parent:= MainButtonPanel;
  with ZoomLabel do
    begin
    Alignment := taCenter;
    Caption := 'ZOOM';
    Font.Size := 8;
    Height := ZoomLabelHeight;
    BevelInner := bvLowered;
    BevelWidth := 1;
    Color := clInfoBk;
    end;

  CircleLabel := TPanel.Create(Self);
  CircleLabel.Parent:= MainButtonPanel;

  with CircleLabel do
    begin
    Alignment := taCenter;
    Caption := 'Circle';
    Font.Size := 8;
    Height := CircleLabelHeight;
    BevelInner := bvLowered;
    BevelWidth := 1;
    Color := clInfoBk;
    end;

  XHairLabel := TPanel.Create(Self);
  XHairLabel.Parent:= MainButtonPanel;
  with XHairLabel do
    begin
    Alignment := taCenter;
    Caption := 'Cross Hair';
    Font.Size := 8;
    Height := CircleLabelHeight;
    BevelInner := bvLowered;
    BevelWidth := 1;
    Color := clInfoBk;
    end;


  FocusPanel := TPanel.Create(Self);
  FocusPanel.Parent:= MainButtonPanel;
  with FocusPanel do
    begin
    Alignment := taLeftJustify;
    Caption := '  Centre on  ';
    Font.Size := 8;
    Height := FocusPanelHeight;
    BevelInner := bvLowered;
    BevelWidth := 1;
    Color := clInfoBk;
    end;

  UpButton := TBitBtn.Create(self);
  UpButton.Parent := MainButtonPanel;
  with UpButton do
    begin
    Caption := 'UP';
    OnMouseDown :=XHairUpMouseDown;
    OnMouseUp :=XHairUpMouseUp;
    end;

  DownButton := TBitBtn.Create(self);
  DownButton.Parent := MainButtonPanel;
  with DownButton do
    begin
    Caption := 'Down';
    OnMouseDown :=XHairDownMouseDown;
    OnMouseUp :=XHairDownMouseUp;
    end;

  LeftButton := TBitBtn.Create(self);
  LeftButton.Parent := MainButtonPanel;
  with LeftButton do
    begin
    Caption := 'Left';
    OnMouseDown :=XHairLeftMouseDown;
    OnMouseUp :=XHairLeftMouseUp;
    end;

  RightButton := TBitBtn.Create(self);
  RightButton.Parent := MainButtonPanel;
  with RightButton do
    begin
    Caption := 'Right';
    OnMouseDown :=XHairRightMouseDown;
    OnMouseUp :=XHairRightMouseUp;
    end;

  CircleOnButton := TSpeedButton.Create(self);
  CircleOnButton.Parent := MainButtonPanel;
  with CircleOnButton do
    begin
    Caption := 'On/Off';
    AllowAllUp := True;
    GroupIndex := 10;
    OnMouseDown :=CircleOnMouseDown;
    OnMouseUp :=CircleOnMouseUp;
    end;

  XHairOnButton := TSpeedButton.Create(self);
  XHairOnButton.Parent := MainButtonPanel;
  with XHairOnButton do
    begin
    AllowAllUp := True;
    GroupIndex := 20;
    Caption := 'On/Off';
    OnMouseDown :=XHairOnMouseDown;
    OnMouseUp :=XHairOnMouseUp;
    end;

  CircleColourButton := TBitBtn.Create(self);
  CircleColourButton.Parent := MainButtonPanel;
  with CircleColourButton do
    begin
    Caption := 'Colour';
    OnMouseDown :=CircleColourMouseDown;
    OnMouseUp :=CircleColourMouseUp;
    end;

  XHairColourButton := TBitBtn.Create(self);
  XHairColourButton.Parent := MainButtonPanel;
  with XHairColourButton do
    begin
    Caption := 'Colour';
    OnMouseDown :=XHairColourMouseDown;
    OnMouseUp :=XHairColourMouseUp;
    end;

  ResetButton  := TBitBtn.Create(self);
  ResetButton.Parent := MainButtonPanel;
  with ResetButton do
    begin
    Caption := 'Reset';
    OnClick := ResetXHairClicked;
   end;

  BiggerButton  := TBitBtn.Create(self);
  BiggerButton.Parent := MainButtonPanel;
  with BiggerButton do
    begin
    Caption := 'Bigger';
    OnMouseDown :=BiggerMouseDown;
    OnMouseUp :=BiggerMouseUp;
    end;


  SmallerButton  := TBitBtn.Create(self);
  SmallerButton.Parent := MainButtonPanel;
  with SmallerButton do
    begin
    Caption := 'Smaller';
    OnMouseDown :=SmallerMouseDown;
    OnMouseUp :=SmallerMouseUp;
    end;




  ZoomUpButton  := TBitBtn.Create(self);
  ZoomUpButton.Parent := MainButtonPanel;
  with ZoomUpButton do
    begin
    Caption := '+';
    Font.Size := BigFontSize;
    OnMouseDown := ScaleUpMouseDown;
    OnMouseUp   := ScaleUpMouseUp;
    end;

  ZoomDownButton  := TBitBtn.Create(self);
  ZoomDownButton.Parent := MainButtonPanel;
  with ZoomDownButton do
    begin
    Caption := '-';
    Font.Size := BigFontSize;
    OnMouseDown := ScaleDownMouseDown;
    OnMouseUp   := ScaleDownMouseUp;
    end;

  FitButton  := TBitBtn.Create(self);
  FitButton.Parent := MainButtonPanel;
  with FitButton do
    begin
    Caption := 'Fit';
    OnClick := FitButtonClick;
    end;

  FullScaleButton:= TBitBtn.Create(self);
  FullScaleButton.Parent := MainButtonPanel;
  with FullScaleButton do
    begin
    Caption := 'Full';
    OnClick := FullButtonClick;
    end;



  ContinuousButton := TSpeedButton.Create(Self);
  ContinuousButton.Parent := MainButtonPanel;
  with ContinuousButton do
    begin
    AllowAllUp := False;
    GroupIndex := 3;
    Caption := 'Cont.';
    OnClick := MoveModeButtonClick;
    end;

  IncrementalButton := TSpeedButton.Create(Self);
  IncrementalButton.Parent := MainButtonPanel;
  with IncrementalButton do
    begin
    AllowAllUp := False;
    GroupIndex := 3;
    Caption := 'Incr.';
    OnClick := MoveModeButtonClick;
    end;


  CoarseButton := TSpeedButton.Create(Self);
  CoarseButton.Parent := MainButtonPanel;
  with CoarseButton do
    begin
    AllowAllUp := False;
    GroupIndex := 2;
    Caption := 'Coarse';
    OnClick := MoveModeButtonClick;
    end;

  MediumButton := TSpeedButton.Create(Self);
  MediumButton.Parent := MainButtonPanel;
  with MediumButton do
    begin
    AllowAllUp := False;
    GroupIndex := 2;
    Caption := 'Medium';
    OnClick := MoveModeButtonClick;
    end;

  FineButton := TSpeedButton.Create(Self);
  FineButton.Parent := MainButtonPanel;
  with FineButton do
    begin
    AllowAllUp := False;
    GroupIndex := 2;
    Caption := 'Fine';
    OnClick := MoveModeButtonClick;
    end;

  COnscreenButton := TSpeedButton.Create(Self);
  COnscreenButton.Parent := FocusPanel;
  with COnscreenButton do
    begin
    AllowAllUp := False;
    GroupIndex := 1;
    Caption := 'F.O.V.';
    OnClick := ConScreenButtonClick;
    end;

  COnXHairButton := TSpeedButton.Create(Self);
  COnXHairButton.Parent := FocusPanel;
  with COnXHairButton do
    begin
    AllowAllUp := False;
    GroupIndex := 1;
    Caption := 'X Hair';
    OnClick := ConXButtonClick;
    end;
IncrementalButton.Down := True;
end;

procedure TCognexControlPanel.IShutdown;
begin
  inherited;

end;

procedure TCognexControlPanel.Installed;
begin
  inherited;
  FirstShow := True;

end;

procedure TCognexControlPanel.Repaint;
begin
  inherited;

end;

procedure TCognexControlPanel.InstallComponent(Params: TParamStrings);
begin
  inherited;
  InstalledVisionName := Params.ReadString('InstalledVisionName','Vision');

{  XHairResetTag     := TagPublish(InstalledVisionName + 'XHairReset',TIntegerTag);
  XHairUpTag        := TagPublish(InstalledVisionName + 'XHairUp',TIntegerTag);
  XHairDownTag      := TagPublish(InstalledVisionName + 'XHairDown',TIntegerTag);
  XHairLeftTag      := TagPublish(InstalledVisionName + 'XHairLeft',TIntegerTag);
  XHairRightTag     := TagPublish(InstalledVisionName + 'XHairRight',TIntegerTag);
  XHairJogDownTag   := TagPublish(InstalledVisionName + 'XHairJogDown',TIntegerTag);
  XHairJogUpTag     := TagPublish(InstalledVisionName + 'XHairJogUp',TIntegerTag);
  XHairJogLeftTag   := TagPublish(InstalledVisionName + 'XHairJogLeft',TIntegerTag);
  XHairJogRightTag  := TagPublish(InstalledVisionName + 'XHairJogRight',TIntegerTag);
  ReqXHairOnTag        := TagPublish(InstalledVisionName + 'ReqXHairOn',TIntegerTag);
//  FitToPanelTag     := TagPublish(InstalledVisionName + 'FitToPanel',TIntegerTag);
  ImageScaleTag     := TagPublish(InstalledVisionName + 'RequestScaleChange',TDoubleTag);
  CentreOnXHairTag  := TagPublish(InstalledVisionName + 'CentreOnXHair',TIntegerTag);
  CircleUpTag       := TagPublish(InstalledVisionName + 'CircleUp',TIntegerTag);
  ReqCircleOnTag       := TagPublish(InstalledVisionName + 'ReqCircleOn',TIntegerTag);
  CircleDownTag     := TagPublish(InstalledVisionName + 'CircleDown',TIntegerTag);
  CircleJogUpTag    := TagPublish(InstalledVisionName + 'CircleJogUp',TIntegerTag);
  CircleJogDownTag  := TagPublish(InstalledVisionName + 'CircleJogDown',TIntegerTag);
  ScaleUpTag        := TagPublish(InstalledVisionName + 'ScaleUp',TDoubleTag);
  ScaleDownTag      := TagPublish(InstalledVisionName + 'ScaleDown',TDoubleTag);
  IncrementModeTag  := TagPublish(InstalledVisionName + 'IncrementMode',TIntegerTag);
  ScaleJogUpTag     := TagPublish(InstalledVisionName + 'ScaleJogUp',TIntegerTag);
  ScaleJogDownTag   := TagPublish(InstalledVisionName + 'ScaleJogDown',TIntegerTag);
  CircleColorNumberTag :=TagPublish (InstalledVisionName + 'CircleColorNumber',TIntegerTag);
  XHairColorNumberTag :=TagPublish (InstalledVisionName + 'XHairColorNumber',TIntegerTag);

  TagEvent(InstalledVisionName +'SceneScale',EdgeChange, TDoubleTag,DoSceneScaleChanged );
  TagEvent(InstalledVisionName +'IsCentredOnXHair',EdgeChange, TDoubleTag,DoSceneScaleChanged );
  TagEvent(InstalledVisionName +'IsCentredOnFOV',EdgeChange, TDoubleTag,DoSceneScaleChanged );
  CircleIsOnTag := TagEvent(InstalledVisionName +'CircleIsOn',EdgeChange, TIntegerTag,DoCircleIsOnChange );
  XHairIsOnTag  := TagEvent(InstalledVisionName +'XHairIsOn',EdgeChange, TIntegerTag,DoXHairIsOnChange );
  TagEvent(InstalledVisionName +'XHairControlsInValid',EdgeChange, TDoubleTag,DoControlsValidChange)}

end;


procedure TCognexControlPanel.Open;
begin
  inherited;
  RefreshStatus;
  FormatButtons;
end;

procedure TCognexControlPanel.RefreshStatus();
begin

end;


procedure TCognexControlPanel.FormatButtons();
var
PHeight : Integer;
PWidth : Integer;
ButtonWidth : INteger;
ButtonHeight : Integer;
Row1 : Integer;
Row2 :Integer;
Row3 : Integer;
Row4 :Integer;
Row5 :Integer;
Row6 :Integer;
Col1 : Integer;
Col2 : Integer;
Col3 : Integer;
Col4 : Integer;
begin
  inherited;
  PHeight := MainButtonPanel.ClientHeight;
  PWidth  := MainButtonPanel.ClientWidth;
  ButtonWidth := (PWidth-(ButtonSpacing*4)-(FormBevelWidth*2)) div 4;
  ButtonHeight := (PHeight-(ButtonSpacing*6)-ZoomLabelHeight-CircleLabelHeight-(FormBevelWidth*2)-FocusPanelHeight) div 5;
  Row1 := BevelWidth+FocusPanelHeight+ButtonSpacing;
  Row2 := Row1+ButtonSpacing+ButtonHeight;
  Row3 := Row2+ButtonHeight+ButtonSpacing;
  Row4 := Row3+ButtonHeight+ButtonSpacing+ZoomLabelHeight+ButtonSpacing+ButtonSpacing;
  Row5 := Row4+ButtonHeight+ButtonSpacing+CircleLabelHeight+ButtonSpacing+ButtonSpacing;
  Row6 := Row5+ButtonHeight+ButtonSpacing;

  Col1 := BevelWidth+ButtonSpacing;
  Col2 := Col1+ButtonWidth+ButtonSpacing;
  Col3 := Col2+ButtonWidth+ButtonSpacing;
  Col4 := Col3+ButtonWidth+ButtonSpacing;

  with FocusPanel do
    begin
    Top := FormBevelWidth;
    Left := FormBevelWidth;
    Width := PWidth-FormBevelWidth;
    end;

  with UpButton do
    begin
    Width := ButtonWidth;
    Height:= ButtonHeight;
    Left :=  Col2;
    Top := Row1;
    end;

  with DownButton do
    begin
    Width := ButtonWidth;
    Height:= ButtonHeight;
    Left :=  Col2;
    Top := Row3;
    end;

  with LeftButton do
    begin
    Width := ButtonWidth;
    Height:= ButtonHeight;
    Left :=  Col1;
    Top := Row2;
    end;

  with RightButton do
    begin
    Width := ButtonWidth;
    Height:= ButtonHeight;
    Left :=  Col3;
    Top := Row2
    end;

  with ResetButton do
    begin
    Width := ButtonWidth;
    Height:= ButtonHeight;
    Left :=  Col2;
    Top := Row2;
    end;

  with BiggerButton do
    begin
    Width := ButtonWidth;
    Height:= ButtonHeight;
    Left :=  Col1;
    Top := Row1
    end;


  with SmallerButton do
    begin
    Width := ButtonWidth;
    Height:= ButtonHeight;
    Left :=  Col3;
    Top := Row1
    end;

  with IncrementalButton do
    begin
    Width := ButtonWidth;
    Height:= ButtonHeight;
    Top := Row3;
    Left := Col1;
    end;

  with ContinuousButton do
    begin
    Width := ButtonWidth;
    Height:= ButtonHeight;
    Top := Row3;
    Left := Col3;
    end;

  with CoarseButton do
    begin
    Width := ButtonWidth;
    Height:= ButtonHeight;
    Top := Row1;
    Left := Col4;
    end;

  with MediumButton do
    begin
    Width := ButtonWidth;
    Height:= ButtonHeight;
    Top := Row2;
    Left := Col4;
    end;

  with FineButton do
    begin
    Width := ButtonWidth;
    Height:= ButtonHeight;
    Top := Row3;
    Left := Col4;
    end;


  with ZoomLabel do
    begin
    Top := Row3+ButtonHeight+ButtonSpacing;
    Left := 2;
    Width := PWidth-4;
    end;


  with XHairLabel do
    begin
    Top := Row4+ButtonHeight+ButtonSpacing;
    Left := 2;
    Width := (PWidth-8) div 2;
    end;

  with CircleLabel do
    begin
    Width := XHairLabel.Width;
    Top := Row4+ButtonHeight+ButtonSpacing;
    Left := XHairLabel.Left+XHairLabel.Width+2;
    end;

  with ZoomUpButton do
    begin
    Width := ButtonWidth;
    Height:= ButtonHeight;
    Top := Row4;
    Left := Col1;
    end;

  with ZoomDownButton do
    begin
    Width := ButtonWidth;
    Height:= ButtonHeight;
    Top := Row4;
    Left := Col2;
    end;

  with FitButton do
    begin
    Width := ButtonWidth;
    Height:= ButtonHeight;
    Top := Row4;
    Left := Col3;
    end;

  with FullScaleButton do
    begin
    Width := ButtonWidth;
    Height:= ButtonHeight;
    Top := Row4;
    Left := Col4;
    end;

  with COnscreenButton do
    begin
    Width := FocusPanel.ClientWidth div 3;
    Top := 2;
    Height := FocusPanel.ClientHeight-4;
    Left := FocusPanel.ClientWidth-Width-2;
    end;

  with COnXHairButton do
    begin
    Width := FocusPanel.ClientWidth div 3;
    Top := 2;
    Height := FocusPanel.ClientHeight-4;
    Left := FocusPanel.ClientWidth-Width-2-Width;
    end;

  with XHairOnButton do
    begin
    Width := ButtonWidth;
    Top := Row5;
    Height:= ButtonHeight;
    Left := Col1;
    end;

  with XHairColourButton do
    begin
    Width := ButtonWidth;
    Top := Row5;
    Height:= ButtonHeight;
    Left := Col2
    end;

  with CircleOnButton do
    begin
    Width := ButtonWidth;
    Top := Row5;
    Height:= ButtonHeight;
    Left := Col3;
    end;

  with CircleColourButton do
    begin
    Width := ButtonWidth;
    Top := Row5;
    Height:= ButtonHeight;
    Left := Col4;
    end;

  if FirstShow then
    begin
    FirstShow := False;
    MediumButton.Down := True;
    COnscreenButton.Down := True;
    end;
end;




procedure TCognexControlPanel.XHairUpMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  case MoveMode of
  mmIncremental:
    begin
    XHairUpTag.AsInteger := 1;
    end;
  mmContinuous:
    begin
    XHairJogUpTag.AsInteger := 1;
    end;
  end; // case
end;

procedure TCognexControlPanel.XHairUpMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
    XHairUpTag.AsInteger := 0;
    XHairJogUpTag.AsInteger := 0;
end;

procedure TCognexControlPanel.XHairDownMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  case MoveMode of
  mmIncremental:
    begin
    XHairDownTag.AsInteger := 1;
    end;
  mmContinuous:
    begin
    XHairJogDownTag.AsInteger := 1;
    end;
  end; // case
end;

procedure TCognexControlPanel.XHairDownMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
    XHairDownTag.AsInteger := 0;
    XHairJogDownTag.AsInteger := 0;
end;

procedure TCognexControlPanel.XHairLeftMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  case MoveMode of
  mmIncremental:
    begin
    XHairLeftTag.AsInteger := 1;
    end;
  mmContinuous:
    begin
    XHairJogLeftTag.AsInteger := 1;
    end;
  end; // case
end;

procedure TCognexControlPanel.XHairLeftMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
    XHairLeftTag.AsInteger := 0;
    XHairJogLeftTag.AsInteger := 0;
end;

procedure TCognexControlPanel.XHairRightMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  case MoveMode of
  mmIncremental:
    begin
    XHairRightTag.AsInteger := 1;
    end;
  mmContinuous:
    begin
    XHairJogRightTag.AsInteger := 1;
    end;
  end; // case
end;

procedure TCognexControlPanel.XHairRightMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
   XHairRightTag.AsInteger := 0;
   XHairJogRightTag.AsInteger := 0;
end;


procedure TCognexControlPanel.BiggerMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  case MoveMode of
  mmIncremental:
    begin
    CircleUpTag.AsInteger := 1;
    end;
  mmContinuous:
    begin
    CircleJogUpTag.AsInteger := 1;
    end;
  end; // case
end;

procedure TCognexControlPanel.BiggerMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  CircleUpTag.AsInteger := 0;
  CircleJogUpTag.AsInteger := 0;
end;

procedure TCognexControlPanel.SmallerMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  case MoveMode of
  mmIncremental:
    begin
    CircleDownTag.AsInteger := 1;
    end;
  mmContinuous:
    begin
    CircleJogDownTag.AsInteger := 1;
    end;
  end; // case
end;

procedure TCognexControlPanel.SmallerMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  CircleDownTag.AsInteger := 0;
  CircleJogDownTag.AsInteger := 0;
end;

procedure TCognexControlPanel.ScaleDownMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  case MoveMode of
  mmIncremental:
    begin
    ScaleDownTag.AsInteger := 1;
    end;
  mmContinuous:
    begin
    ScaleJogDownTag.AsInteger := 1;
    end;
   end;
end;

procedure TCognexControlPanel.ScaleDownMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  ScaleDownTag.AsInteger := 0;
  ScaleJogDownTag.AsInteger := 0;
end;

procedure TCognexControlPanel.ScaleUpMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  case MoveMode of
  mmIncremental:
    begin
    ScaleUpTag.AsInteger := 1;
    end;
  mmContinuous:
    begin
    ScaleJogUpTag.AsInteger := 1;
    end;
  end;

end;

procedure TCognexControlPanel.ScaleUpMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  ScaleUpTag.AsInteger := 0;
  ScaleJogUpTag.AsInteger := 0;
end;


procedure TCognexControlPanel.XHairOnMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
TagVal : Integer;
begin
TagVal := XHairIsOnTag.AsInteger;
if TagVal = 0 then
  begin
  ReqXHairOnTag.AsInteger := 1;
  end
else
  begin
  ReqXHairOnTag.AsInteger := 0;
  end
end;

procedure TCognexControlPanel.CircleOnMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
end;

procedure TCognexControlPanel.XHairColourMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
TagVal : Integer;
begin
TagVal := XHairColorNumberTag.AsInteger;
if TagVal < 10 then
  begin
  XHairColorNumberTag.AsInteger := TagVal+1;
  end
else
  begin
  XHairColorNumberTag.AsInteger := 1;
  end
end;

procedure TCognexControlPanel.CircleColourMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin

end;

procedure TCognexControlPanel.CircleOnMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
TagVal : Integer;
begin
TagVal := CircleIsOnTag.AsInteger;
if TagVal = 0 then
  begin
  ReqCircleOnTag.AsInteger := 1;
  end
else
  begin
  ReqCircleOnTag.AsInteger := 0;
  end
end;

procedure TCognexControlPanel.CircleColourMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
TagVal : Integer;
begin
TagVal := CircleColorNumberTag.AsInteger;
if TagVal < 10 then
  begin
  CircleColorNumberTag.AsInteger := TagVal+1;
  end
else
  begin
  CircleColorNumberTag.AsInteger := 1;
  end
end;

procedure TCognexControlPanel.XHairOnMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin

end;

procedure TCognexControlPanel.XHairColourMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
end;



procedure TCognexControlPanel.MoveModeButtonClick(Sender: TObject);
begin
if Sender = ContinuousButton then
  begin
  MoveMode := mmContinuous;
  end else if Sender = IncrementalButton then
  begin
  MoveMode := mmIncremental;
  end else if Sender = CoarseButton then
  begin
    IncrementModeTag.AsInteger := 0;
    IncrementModeTag.AsInteger := 3;
  end else if Sender = MediumButton then
  begin
    IncrementModeTag.AsInteger := 0;
    IncrementModeTag.AsInteger := 2;
  end else if Sender = FineButton then
  begin
    IncrementModeTag.AsInteger := 0;
    IncrementModeTag.AsInteger := 1;
  end;
end;


procedure TCognexControlPanel.FitButtonClick(Sender: TObject);
begin
  CentreOnXHairTag.AsInteger := 0;
  COnscreenButton.Down := True;
  FitToPanelTag.AsInteger := 1;
  FitToPanelTag.AsInteger := 0;
end;

procedure TCognexControlPanel.FullButtonClick(Sender: TObject);
begin
  ImageScaleTag.AsDouble :=0.99;
  ImageScaleTag.AsDouble :=1.0;
end;

procedure TCognexControlPanel.ConScreenButtonClick(Sender: TObject);
begin
CentreOnXHairTag.AsInteger := 0;
end;

procedure TCognexControlPanel.ConXButtonClick(Sender: TObject);
begin
CentreOnXHairTag.AsInteger := 1;
end;

procedure TCognexControlPanel.ResetXHairClicked(Sender: TObject);
begin
XHairResetTag.AsInteger := 1;
XHairResetTag.AsInteger := 0;
end;



procedure TCognexControlPanel.DoSceneScaleChanged(ATag: TAbstractTag);
var
TagVal : Double;
begin
TagVal := ATag.AsDouble;
SceneScalePercent := TagVal*100.0;
ZoomLabel.Caption := Format('Zoom = %3.1f',[SceneScalePercent]);
end;




procedure TCognexControlPanel.DoXHairIsOnChange(ATag: TAbstractTag);
var
TagVal : Integer;
begin
TagVal := ATag.AsInteger;
if TagVal <> 0 then
  XHairOnButton.Down := True
else
  XHairOnButton.Down := False;


end;

procedure TCognexControlPanel.DoCircleIsOnChange(ATag: TAbstractTag);
var
TagVal : Integer;
begin
TagVal := ATag.AsInteger;
if TagVal <> 0 then
  CircleOnButton.Down := True
else
  CircleOnButton.Down := False;
end;

procedure TCognexControlPanel.DoControlsValidChange(ATag: TAbstractTag);
var
TagVal : Integer;
IsEnabled : Boolean;
begin
TagVal := ATag.AsInteger;
isEnabled :=  TagVal = 0 ;
BiggerButton.Enabled := IsEnabled;
SmallerButton.Enabled := IsEnabled;
UpButton.Enabled := IsEnabled;
DownButton.Enabled := IsEnabled;
LeftButton.Enabled := IsEnabled;
RightButton.Enabled := IsEnabled;
ResetButton.Enabled := IsEnabled;
CircleOnButton.Enabled := IsEnabled;
XHairOnButton.Enabled := IsEnabled;
CircleColourButton.Enabled := IsEnabled;
XHairColourButton.Enabled := IsEnabled;

end;

initialization
//  RegisterCNCClass(TCognexControlPanel);

end.
