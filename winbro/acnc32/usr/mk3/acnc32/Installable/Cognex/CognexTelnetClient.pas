unit CognexTelnetClient;

interface
uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls,Buttons, ExtCtrls, ComCtrls,Menus, Grids,
  IdBaseComponent, IdComponent, IdTCPServer, IdTelnetServer, IdTCPConnection, IdTCPClient, IdTelnet;

const
  CRLF = #$d + #$a;
  LF =  #$a;
Type
TTelnetResponseEvent = procedure(Sender : Tobject; Response:String) of object;
TTelnetStatusChangeEvent = procedure(Sender : Tobject; Status : Boolean) of object;


TCognexTelnetClient = class(TComponent)
  private
  FIPAddress : String;
  FUsername : String;
  FPassword : String;
  FPort : Integer;
  FLoggedIn : Boolean;
  Telnet : TIdTelnet;
  ReadTimer : TTimer;
  ResponseTimeout : TTimer;
  IsFirstConnect : Boolean;
  IsClosing : Boolean;

  FOnDataInBuffer: TTelnetResponseEvent;
  FonConnected   : TNotifyEvent;
  FOnLoggedIn    : TNotifyEvent;
  FonReConnected   : TNotifyEvent;
  NoResponse : Boolean;
  FDataRecieved: Boolean;
  FTelnetHealthy : Boolean;
  FOnTelnetException: TNotifyEvent;
  FHasDisconnected: TNotifyEvent;
  FonHealthyChange: TTelnetStatusChangeEvent;
  FReadTimerPeriod: Integer;
  FResponseTimeoutInSeconds: Single;
  DisableTimeout : Boolean;

  procedure CheckForInput(Sender : Tobject);
  procedure ResetOnTimeout(Sender : Tobject);
    function GetIsConnected: Boolean;
    function ExtractFirstLine(Var DAvailable : Boolean;var MoreData : Boolean):String;
    function GetSignedIn: Boolean;
    procedure SetDataRecieved(const Value: Boolean);
    procedure DoTelnetDisconnected(Sender : TObject);
    procedure SetTelnetHealthy(const Value: Boolean);
    procedure SetReadTimerPeriod(const Value: Integer);
    procedure SetResponseTimeout(const Value: Single);

  public
    LastCommand : String;
    InBuffer : String;
    CommandResponse : String;
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    function SendCommand(CText: String;AddLineFeed : Boolean;WaitForResponse : Boolean):String;
//    procedure SendInternalCommand(CText: String;AllowedResponse : String);
    function ConnectToCamera(FirstConnect : Boolean):Boolean;
    procedure SendUsernameandPassword;
    procedure ReconnectTelnet;
    procedure LoginSuccesful;
    procedure PrepareToClose;
    procedure TemporaryDisconnect;
    property IPAddress : String read FIPAddress Write FIPAddress;
    property PortNumber : INteger read FPort Write FPort;
    property Username : String read FUsername write FUsername;
    property Password : String read FPassword write FPassword;
    property OnDataInBuffer : TTelnetResponseEvent read FOnDataInBuffer write FOnDataInBuffer;
    property aOnConnected : TNotifyEvent read FonConnected write FonConnected;
    property OnReConnected : TNotifyEvent read FonReConnected write FonReConnected;
//    property OnLoggedIn : TNotifyEvent read FOnLoggedIn write FOnLoggedIn;
    property OnTelnetException : TNotifyEvent read FOnTelnetException write FOnTelnetException;
    property IsConnected : Boolean read GetIsConnected;
    property SignedIn : Boolean read GetSignedIn;
    property OnDisconnected : TNotifyEvent read FHasDisconnected write FHasDisconnected;
    property OnHealthyChange : TTelnetStatusChangeEvent read FonHealthyChange write FonHealthyChange;
    property TelnetHealthy : Boolean read FTelnetHealthy write SetTelnetHealthy;
    property ReadTimerPeriod : Integer read FReadTimerPeriod write SetReadTimerPeriod;
    property ResponseTimeoutInSeconds : Single read FResponseTimeoutInSeconds write SetResponseTimeout;
  end;


implementation

uses IdIOHandler;

{ TCognexTelnetClient }
constructor TCognexTelnetClient.Create(AOwner: TComponent);
begin
  inherited;
  IsClosing := False;
  FLoggedIn := False;
  FIPAddress := '172,0,0,0';
  FUsername := 'admin';
  FPort := 23;
//  Telnet := TIdTelnet.Create(nil);
//  Telnet.OnDisconnect := DoTelnetDisconnected;

  ReadTimer := TTimer.Create(nil);
  with ReadTimer do
    begin
    Enabled := False;
    Interval := 20;
    onTimer := CheckForInput;
    end;

  ResponseTimeout := TTimer.Create(nil);
  with ResponseTimeout do
    begin
    Enabled := False;
    Interval := 2000;
    onTimer := ResetOnTimeout;
    end;


end;

destructor TCognexTelnetClient.Destroy;
begin
if assigned(ReadTimer) then
  begin
  ReadTimer.Enabled := False;
  ReadTimer.Free;
  end;
if assigned(ResponseTimeout) then
  begin
  ResponseTimeout.Enabled := False;
  ResponseTimeout.Free;
  end;

if assigned(Telnet) then
  begin
  Telnet.Disconnect(True);
  Telnet.Free;
  end;
  inherited;
end;

procedure TCognexTelnetClient.CheckForInput(Sender: Tobject);
var
DataAvailable,MoreData : Boolean;
begin
  try
  if assigned(Telnet.IOHandler) then
     begin
     Telnet.IOHandler.CheckForDataOnSource(10);
     if  not Telnet.IOHandler.InputBufferIsEmpty then
       begin
       InBuffer := Telnet.IOHandler.ReadLn(LF,10,-1);
       ResponseTimeout.Enabled := False;
       try
       if InBuffer = '-2' then
          begin
          if assigned(FOnDataInBuffer) then FOnDataInBuffer(Self,InBuffer);
          end
       else
          begin
          if assigned(FOnDataInBuffer) then FOnDataInBuffer(Self,InBuffer);
          end
       except
       InBuffer := 'exception';
       end
       end;
     end;
  except
  ReadTimer.Enabled := False;
  If assigned(FOnTelnetException) then FOnTelnetException(Self);
  TelnetHealthy := False;
  end
end;

function TCognexTelnetClient.ConnectToCamera(FirstConnect : Boolean):Boolean;
var
WPOs : Integer;
begin
Result := False;
IsFirstConnect := FirstConnect;
If assigned(Telnet) then
  begin
  Telnet.Disconnect(True);
  Telnet.Free;
  end;
Telnet := TIdTelnet.Create(nil);
Telnet.OnDisconnect := DoTelnetDisconnected;
Telnet.Host := FIPAddress;
Telnet.Port := FPort;

ReadTimer.Enabled := False;
TelnetHealthy := False;
 try
 Telnet.Connect;
 except
 ReadTimer.Enabled := False;
 If assigned(FOnTelnetException) then FOnTelnetException(Self);
 exit ;
 end;

if assigned(Telnet.IOHandler) then
  begin
  InBuffer := Telnet.IOHandler.ReadLn;
  WPOs := Pos('Welcome',InBuffer);
  if WPOs > 0 then
     begin
     TelnetHealthy := True;
     Result := True;
     If FirstConnect then
        begin
        if assigned(FonConnected) then  FonConnected(Self);
        end
     else
        begin
        if assigned(FonReConnected) then  FonReConnected(Self);
        end
     end;
  end;
end;

procedure TCognexTelnetClient.ReconnectTelnet;
var
S : String;
begin
FLoggedIn := False;
Telnet.Disconnect(True);
ConnectToCamera(False)
{Telnet.Host := FIPAddress;
Telnet.Port := FPort;
Telnet.Connect;
if assigned(Telnet.IOHandler) then
  begin
  S := Telnet.IOHandler.ReadLn;
  ReadTimer.Enabled := true;
  end;
SendCommand('admin',True,True);}
end;


procedure TCognexTelnetClient.ResetOnTimeout(Sender: Tobject);
begin
//ReadTimer.Enabled := False;
ResponseTimeout.Enabled := False;
NoResponse := True;
TelnetHealthy := False;
//ReconnectTelnet;
end;



function TCognexTelnetClient.SendCommand(CText : String;AddLineFeed : Boolean;WaitForResponse : Boolean): String;
var
ComStr : String;
begin
ComStr := CText;
Result := '';
try
LastCommand := Ctext;
if LastCommand = '12345' then
  begin
  Exit;
  end;
if AddLineFeed then ComStr := CText+CRLF;
if assigned(Telnet.IOHandler) then
     begin
     Telnet.IOHandler.WriteLn(ComStr);
     end;
ReadTimer.Enabled := True;
except
 ReadTimer.Enabled := False;
 if assigned(OnTelnetException) then OnTelnetException(Self);
end;
end;



procedure TCognexTelnetClient.SendUsernameandPassword;
var
S : String;
begin
S := FUsername+FPassword;
SendCommand(S,True,False);
end;

function TCognexTelnetClient.GetIsConnected: Boolean;
begin
try
if assigned(Telnet) then
  begin
  Result := Telnet.Connected and TelnetHealthy;
  end
else
  begin
  Result := False;
  end
 except
 Result := False;
 end;
end;

function TCognexTelnetClient.GetSignedIn: Boolean;
begin
Result := FLoggedIn;
end;

function TCognexTelnetClient.ExtractFirstLine(var DAvailable, MoreData: Boolean): String;
begin
DAvailable := False;
MoreData := False;
 if assigned(Telnet.IOHandler) then
     begin
     Telnet.IOHandler.CheckForDataOnSource(10);
     if  not Telnet.IOHandler.InputBufferIsEmpty then
       begin
       Result := Telnet.IOHandler.ReadLn(LF,10,-1);
       end
     end;
end;

procedure TCognexTelnetClient.LoginSuccesful;
begin
FLoggedIn := True;
//if assigned(FOnLoggedIn) then FOnLoggedIn(Self);
end;

procedure TCognexTelnetClient.PrepareToClose;
begin
IsClosing := True;
ReadTimer.Enabled := False;
ReadTimer.Free;
ReadTimer := nil;
ResponseTimeout.Enabled := False;
ResponseTimeout.Free;
ResponseTimeout := nil;
if assigned(Telnet) then Telnet.Disconnect(True);
end;

procedure TCognexTelnetClient.SetDataRecieved(const Value: Boolean);
begin
  FDataRecieved := Value;
end;

procedure TCognexTelnetClient.TemporaryDisconnect;
begin
ReadTimer.Enabled := False;
ResponseTimeout.Enabled := False;
FLoggedIn := False;
Telnet.Disconnect(True);
TelnetHealthy := False;
Telnet.Free;
Telnet := Nil;
end;

procedure TCognexTelnetClient.DoTelnetDisconnected(Sender: TObject);
begin
  if isClosing then Exit;
  if assigned(FHasDisconnected) then FHasDisconnected(Self);
end;

procedure TCognexTelnetClient.SetTelnetHealthy(const Value: Boolean);
begin
  FTelnetHealthy := Value;
  if assigned(FonHealthyChange) then FonHealthyChange(Self,FTelnetHealthy)
end;

procedure TCognexTelnetClient.SetReadTimerPeriod(const Value: Integer);
begin
FReadTimerPeriod := Value;
if assigned(ReadTimer) then ReadTimer.Interval := Value;
end;


procedure TCognexTelnetClient.SetResponseTimeout(const Value: Single);
begin
  FResponseTimeoutInSeconds := Value;
  if Value = 0.0 then
    begin
    DisableTimeout := True;
    end
  else
    begin
    DisableTimeout := False;
    If assigned(ResponseTimeout) then
      begin
      ResponseTimeout.Interval := round(FResponseTimeoutInSeconds*1000)
      end
    end;

end;

end.
