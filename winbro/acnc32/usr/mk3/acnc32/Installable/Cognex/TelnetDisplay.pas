unit TelnetDisplay;

interface
uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls,Grids,PlusMemo;
Type
 TTelnetRollingBuffer= class;

 TTelnetDisplay = class(TPlusMemo)
  private
    FOutColour    : TColor;
    FReplyColour  : TColor;
    FDebugColour  : TColor;
    Initialised   : Boolean;
    PadString : String;
    IdleString: String;
    FSystemIdleState: Boolean;
    FCommandStatusNumber: Integer;


    procedure SetOutColour(const Value: TColor);
    procedure SetReplyColour(const Value: TColor);
    procedure SetBufferLineSize(const Value: Integer);
    procedure SetSystemIdleState(const Value: Boolean);
    procedure SetCommandStatusNumber(const Value: Integer);
    procedure TrimToMax;

  public
  FBufferLineSize: Integer;
  FMaxLineCount : Integer;
  RollingBuffer : TTelnetRollingBuffer;
  constructor Create(AOwner: TComponent); override;
  destructor Destroy; override;
  procedure RefreshFromRollingBuffer;

  procedure AddSuccessMessage(OutMess: String); dynamic;
  procedure AddOutMessage(OutMess : String);   dynamic;
  procedure AddReplyMessage(OutMess : String); dynamic;
  procedure AddDebugMessage(OutMess : String); dynamic;
  procedure AddErrorMessage(OutMess : String); dynamic;
  procedure AddStatusMessage(OutMess : String); dynamic;
  procedure AddStageMessage(OutMess : String); dynamic;
  Procedure Initialise(OutColour,OutColourBack,ReplyColour,ReplyColourBack,DebugColour,DebugColourBack : TColor);
  property OutColour : TColor read FOutColour write SetOutColour;
  property ReplyColour : TColor read FReplyColour write SetReplyColour;
  property BufferLineSize : Integer read FBufferLineSize write SetBufferLineSize;
  property SystemIdleState : Boolean read FSystemIdleState write SetSystemIdleState;
  property CommandStatusNumber   : Integer read FCommandStatusNumber write SetCommandStatusNumber;
  property MaxLineCount : Integer read FMaxLineCount write FMaxLineCount;
  end;

 TTelnetRollingBuffer= class(TTelnetDisplay)
   private
    FBufferSize : Integer;
    procedure TrimToSize;
    procedure SetBufferSize(const Value: Integer);

   public
    constructor Create(AOwner: TComponent); override;
    procedure AddSuccessMessage(OutMess: String); override;
    procedure AddOutMessage(OutMess : String); override;
    procedure AddReplyMessage(OutMess : String); override;
    procedure AddDebugMessage(OutMess : String); override;
    procedure AddErrorMessage(OutMess : String); override;
    procedure AddStageMessage(OutMess : String); override;
    property BufferSize : Integer read FBufferSize write SetBufferSize;
   end;



implementation

{ TTelnetDisplay }

procedure TTelnetDisplay.SetOutColour(const Value: TColor);
begin
  FOutColour := Value;
end;

procedure TTelnetDisplay.AddReplyMessage(OutMess: String);
begin
TrimToMax;
Lines.Add(Format('%2d %s <-    %s%s',[CommandStatusNumber,IdleString,OutMess,PadString]));
end;

procedure TTelnetDisplay.AddOutMessage(OutMess: String);
begin
TrimToMax;
Lines.Add(Format('%2d %s ->    %s%s',[CommandStatusNumber,IdleString,OutMess,PadString]));
end;

procedure TTelnetDisplay.AddErrorMessage(OutMess : String);
begin
TrimToMax;
Lines.Add(Format('%2d %s <_    %s%s',[CommandStatusNumber,IdleString,OutMess,PadString]));
end;

procedure TTelnetDisplay.AddDebugMessage(OutMess: String);
begin
TrimToMax;
Lines.Add(Format('%2d %s --    %s%s',[CommandStatusNumber,IdleString,OutMess,PadString]));
end;

procedure TTelnetDisplay.AddSuccessMessage(OutMess: String);
begin
TrimToMax;
Lines.Add(Format('%2d %s <+    %s%s',[CommandStatusNumber,IdleString,OutMess,PadString]));
end;

procedure TTelnetDisplay.AddStatusMessage(OutMess: String);
begin
TrimToMax;
Lines.Add(Format('%2d %s ^^    %s%s',[CommandStatusNumber,IdleString,OutMess,PadString]));
end;

procedure TTelnetDisplay.AddStageMessage(OutMess: String);
begin
TrimToMax;
Lines.Add(Format('%2d %s //    %s%s',[CommandStatusNumber,IdleString,OutMess,PadString]));
end;

procedure TTelnetDisplay.SetReplyColour(const Value: TColor);
begin
  FReplyColour := Value;
end;

procedure TTelnetDisplay.Initialise(OutColour,OutColourBack,ReplyColour,ReplyColourBack,DebugColour,DebugColourBack : TColor);
var
Temp : TFontStyle;
begin
 FOutColour := OutColour;
 FReplyColour := ReplyColour;
 FDebugColour := DebugColour;
 Initialised := True;
 Font.Name := 'Verdana';
 Font.Size := 10;
 StartStopKeys.AddStartStopKey('->','',[],[],1,crDefault,OutColourBack,OutColour,True);
 StartStopKeys.AddStartStopKey('<-','',[],[fsbold],1,crDefault,ReplyColourBack,ReplyColour,True);
 StartStopKeys.AddStartStopKey('--','',[],[],1,crDefault,DebugColourBack,DebugColour,True);
 StartStopKeys.AddStartStopKey('<_','',[],[fsbold],1,crDefault,DebugColourBack,clred,True);
 StartStopKeys.AddStartStopKey('<+','',[],[fsbold],1,crDefault,DebugColourBack,clGreen,True);
 StartStopKeys.AddStartStopKey('^^','',[],[fsbold],1,crDefault,ClMoneyGreen,clMaroon,True);
 StartStopKeys.AddStartStopKey('//','',[],[fsbold],1,crDefault,Clwhite,clWebDarkOrange,True);
 ApplyStartStopKeys := True;
 WordWrap := False;
 Lines.Add('// System Start')

end;

constructor TTelnetDisplay.Create(AOwner: TComponent);
begin
  inherited;
  Initialised   := False;
  FMaxLineCount := 500;
  PadString := '                                                                                                                                                                            ';
  IdleString := 'I';
  if not (AOwner is TTelnetDisplay) then
    begin
    RollingBuffer := TTelnetRollingBuffer.Create(Self);
    RollingBuffer.Parent := TWinControl(AOwner);
    RollingBuffer.Initialise(clBlack,clLtGray+$111111,clNavy,clLtGray+$222222,ClBlue,clLtGray+$333333);
    RollingBuffer.Visible := False;
    RollingBuffer.BufferSize := 400;
    end
  else
    begin
    RollingBuffer := nil;
    end;


end;


destructor TTelnetDisplay.Destroy;
begin
  if assigned(RollingBuffer) then
    try
    RollingBuffer.Free;
    except
    end;
  inherited;
end;

procedure TTelnetDisplay.RefreshFromRollingBuffer;
var
LCount :Integer;
begin
Lines.Clear;
Lines.BeginUpdate;
  try
  For Lcount := 0 to RollingBuffer.Lines.Count-1 do
    begin
    Lines.Add(RollingBuffer.Lines[LCount])
    end
  finally
  Lines.EndUpdate;
  try
  ScrollInView;
  except
  end;
  end;
end;

procedure TTelnetDisplay.SetBufferLineSize(const Value: Integer);
begin
  FBufferLineSize := Value;
  if assigned(RollingBuffer) Then RollingBuffer.BufferSize := FBufferLineSize;
end;


procedure TTelnetDisplay.SetSystemIdleState(const Value: Boolean);
begin
  FSystemIdleState := Value;
  if Value then IdleString := 'I' else IdleString := 'B';
  if assigned(RollingBuffer) then
    begin
    RollingBuffer.SystemIdleState := Value;
    end;
end;

procedure TTelnetDisplay.SetCommandStatusNumber(const Value: Integer);
begin
  FCommandStatusNumber := Value;
  if assigned(RollingBuffer) then
    begin
    RollingBuffer.CommandStatusNumber := Value;
    end;

end;

procedure TTelnetDisplay.TrimToMax;
begin
while Lines.Count > MaxLineCount do
  begin
  Lines[0] := '';
  Lines.Delete(0);
  end;
end;

{ TTelnetRollingBuffer }


constructor TTelnetRollingBuffer.Create(AOwner: TComponent);
begin
  inherited;
  If assigned(RollingBuffer) then RollingBuffer.Free;
  RollingBuffer := nil;
  FBufferSize := 200;
  RollingBuffer := nil;
end;

procedure TTelnetRollingBuffer.AddReplyMessage(OutMess: String);
begin
  inherited;
  TrimToSize;
end;

procedure TTelnetRollingBuffer.AddErrorMessage(OutMess: String);
begin
  inherited;
  TrimToSize;
end;

procedure TTelnetRollingBuffer.AddOutMessage(OutMess: String);
begin
  inherited;
  TrimToSize;
end;

procedure TTelnetRollingBuffer.AddSuccessMessage(OutMess: String);
begin
  inherited;
  TrimToSize;
end;

procedure TTelnetRollingBuffer.AddDebugMessage(OutMess: String);
begin
  inherited;
  TrimToSize;
end;
procedure TTelnetRollingBuffer.AddStageMessage(OutMess: String);
begin
  inherited;
  TrimToSize;
end;


procedure TTelnetRollingBuffer.TrimToSize;
begin
While Lines.Count > FBufferSize do
  begin
  Lines[0] := '';
  Lines.Delete(0);
  end;
end;

procedure TTelnetRollingBuffer.SetBufferSize(const Value: Integer);
begin
  FBufferSize := Value;
end;


end.
