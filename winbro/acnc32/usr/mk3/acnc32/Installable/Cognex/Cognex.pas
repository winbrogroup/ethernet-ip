unit Cognex;

{$DEFINE HIGHDEBUG}
{$DEFINE GRID_MODE_InVALID}

interface
uses CoreCNC, CNCTypes, CNC32, Classes, Windows, Graphics, SysUtils, NCNames,TelnetIndicator,TelnetIndicatorPanel,
  CNCAbs, Named, Controls,ExtCtrls,ActiveX,CognexTelnetClient,StdCtrls,Grids,Buttons,TelnetDisplay,CognexActions,VisionErrorStack,
  AbstractScript,IScriptInterface,INamedInterface,IniFiles,Forms,CogNexCommunicator,TextInputCombo,CognexTYpes,VisionCommand,
  FTPPanel,FaultRegistration,
  CvsInSightDisplayOcx_TLB,CognexInSight_TLB,CognexInSightDisplay_TLB;

type


  TCameraDisplaySize = (tcdUnknown,tcdSmall,tcdWide,tcdFull);
 // tccmConnected Implies connected via Active X but not neccesarily via Telnet
  TCameraConnectModes =(tccmNone,tccmInitialConnect,tccmRetryConnect,tccmReconnectConnect,tccmUnsolicitedReconect,tccmOPeratorDisconnect,tccmUnsolicitedDisconect,tccmFailed,tccmConnected);
 // tccmConnected Implies connected via Active X but not neccesarily via Telnet
 // CVS test Insert

 TTMTypes = (ttmtNone,ttmtSessionConnect,ttmtSessionEmulated,ttmtTelnetConnect,ttmtLogin);

  TCameraState = record
     PixelWidth               : Integer;
     PixelHeight              : Integer;
     Scale                    : Double;
     FitMode                  : Integer;
     CentreModeInteger        : Integer;
     CentreMode               : TCentreModes;
     CircleRadius             : Double;
     aaXHairOn                : Integer;
     CircleOn                 : Integer;
     XHairColorNumber         : Integer;
     CircleColorNumber        : Integer;
     Exposure                 : Double;
     JobScaling               : Double;
     XOffset                  : Double;
     YOffset                  : Double;
     OffsetIsManual           : Integer;
     end;

  TCognexDisplay = class(TExpandDisplay)
  private
    CommandIsBuffered : Boolean;
    BufferredCommandType : TNCCommands;
    BufferedComandString : String;
    BufferedCommandErrorString : String;
    PerformingBufferedCommand : Boolean;
//       CognexComms.SendNCCommand(Command,tncGetSymbolicValue,'get symbolic value',True);

    LastProgramName : String;
    UseFTPServer : Boolean;
    GoVisibleOnFit : Boolean;
    BackImageFileName : String;
    ImageOrientation : Integer;
    FirstShow : Boolean;
    EnableCameraUpdate : Boolean;
    DisplayModeEstablished : Boolean;
    fil : Integer;
    FirstPaint : Boolean;
    IsClosing : Boolean;
    ConnectString : String;
    FCameraConnected : Boolean;
    SessionConnectRetry : Integer;
    FTPConnectRetry     : Integer;
    TelnetConnectRetry  : Integer;
    LoginRetry : Integer;
    OfflineRetry : Integer;
    LatchTelnet : Boolean;
    LatchJob : Boolean;
    TelnetMemoLength : Integer;
    ManualOffsetMode : Boolean;
    InCycle : Boolean;
    SessionConnectRetries : INteger;

    TelnetIndicatorPanel : TTelnetIndicatorPanel;
    TelnetCameraVisible : Boolean;
    FTPControlPanel : TFTPPanel;
    InitialSessionConnectDelayTimer     : TTimer;
//  InitialCameraTelnetConnectDelay   : TTimer;
    CameraSessionConnectTimeout    : TTimer;
    CameraTelnetConnectTimeout     : TTimer;
    TelnetConnectDelayTimer        : TTimer;
    MessageTimer                   : TTimer;
    MessOffTimer                   : TTimer;
    JogTimer                       : TTimer;

    CurrentScale : Single;
    InstallDone : Boolean;
    DisplayPanel  : TPanel;
    FTPPanel      : TPanel;
    TelnetPanel   : TPanel;
    TelnetMemo : TTelnetDisplay;
    MessPanel : TPanel;
    StatusPanel : TPanel;
    TelnetLatch : TCheckBox;
    Image : TImage;
    ErrorDisplay   : TErrorDisplayPanel;
    LeftDisplayPanel : TPanel;
    CameraParentPanel : TPanel;

    TelnetLowerPanel : TPanel;
    TelnetClearButton: TButton;
    ReconnectButton : TButton;
    WriteToFileButton : TButton;
    TelnetSendEdit    : TTextInputCombo;
    TelnetDebugButton : TSpeedButton;
    TelnetDuplexButton : TSpeedButton;
    TelnetCameraButton: TSpeedButton;
    TelnetStageButton: TSpeedButton;
    TelnetNoneButton : TSpeedButton;
    DefaultJobName : String;
    InitialRetryCount : Integer;
    SnapShotDirectory : String;
    ErrorStackClearMode : Integer;
    ShowCameraOnReset : Boolean;
    ControlAllowedAllJobs : Integer;
    ServiceTimerPeriod : INteger;
    ReadTimerPeriod : INteger;
    TelnetAtPowerUp : Boolean;
    TelnetDebugBufferLength : Integer;
    ResponseTimeoutInSecs : Single;
    JobLoadTimeoutInseconds : Single;

    FCameraIP : String;
    FEmulatorIP : String;
    FCameraName : String;
    FEmulatorCameraName : String;
    FCameraPortNumber : Integer;
    FCameraFTPPortNumber : Integer;

    EmulatorMode : Boolean;
    NoCognex : Boolean;
    FNoCognexMode: Boolean;

    RestoreStage : INteger;
    RestoreRetry : INteger;
    ResetStage : Integer;
    ResetFailCount : Integer;
    ResetInProgress : Boolean;
    ConfigFilename : String;
    PartLifeBasePath : String;
    VisionJogMode : TVJogModes;
    TestForm : TForm;

    VisionLatchJobTag         : TAbstractTag;
    VisionResponseTag         : TAbstractTag;
    ExternalResponseTag       : TAbstractTag;
    VisionCommandStatusTag    : TAbstractTag;
    VisionErrorMessTag        : TAbstractTag;
    VisionFunctionIndexTag    : TAbstractTag;
    VisionCanTalkTag          : TAbstractTag;
    VisionParam1Tag           : TAbstractTag;
    VisionParam2Tag           : TAbstractTag;
    VisionParam3Tag           : TAbstractTag;
    VisionRunFunctionTag      : TAbstractTag;
    CurrentJobNameTag         : TAbstractTag;
    SceneScaleTag             : TAbstractTag;
    JogIncFineTag             : TAbstractTag;
    JogIncMediumTag           : TAbstractTag;
    JogIncCoarseTag           : TAbstractTag;
    COXTag                    : TAbstractTag;
    COFOVTag                  : TAbstractTag;
    IsFullFOVTag              : TAbstractTag;
    CircleIsOnTag             : TAbstractTag;
    XHairIsOnTag              : TAbstractTag;
    VisionIdleTag             : TAbstractTag;
    XHairControlsInValidTag   : TAbstractTag;
    VisionReqDisconnectTag    : TAbstractTag;
    VisionDisconnectState     : TAbstractTag;
    VisionReqReconnectTag     : TAbstractTag;
    ResetExposureTag          : TAbstractTag;
    CurrentExposureTag        : TAbstractTag;

    LaunchTag                 : TAbstractTag;
    OffsetIsManualTag         : TAbstractTag;
    OnlineTag                 : TAbstractTag;
    XoffsetTag                : TAbstractTag;
    YoffsetTag                : TAbstractTag;
    ScalingTag                : TAbstractTag;
    JobPassedTag              : TAbstractTag;
    JobInlineErrorTag         : TAbstractTag;
    JobDataTag                : TAbstractTag;
    ErrorStackCountTag        : TAbstractTag;
    SystemResetTag            : TAbstractTag;
    EstablishXHairTag         : TAbstractTag;
    IsReadyTag                : TAbstractTag;
    CameraConnectedTag        : TAbstractTag;
    SnapShotWGTag             : TAbstractTag;
    SnapShotNGTag             : TAbstractTag;
    XHairBufferedTag          : TAbstractTag;
    DisableXhairButtonTag     : TAbstractTag;


    Disconnecting : Boolean;
    Disconnected  : Boolean;
    ReConnecting  : Boolean;
    Reconnected   : Boolean;
    DisconnectOnIdle : Boolean;
    ConRequestCount : Integer;
    DisConRequestCount : Integer;
    ResetActionEnable : Boolean;

    CurrentFunctionIndex : TNCLanguageFunctions;
    Param1 : String;
    Param2 : String;
    Param3 : String;
    BufferProgram : String;

    NCCommand : Boolean;
    NCCommandGoodComplete : Boolean;
    NativeCommandComplete : Boolean;
    DefaultXOffset : Single;
    DefaultYOffset : Single;
    DefaultCircleRadius : Single;
    DefaultCircleColorNumber : Integer;
    DefaultXHairColorNumber  : Integer;
    InitialSessionConnectDelayTime   : Integer; /// Delay before sending IPand Username to Camera
    InitialTelnetConnectDelayTime   : Integer; /// Delay before trying telnet log on after session connect
    SessionConnectTimeOutTime : Integer; // Timeout for response to Initial Connect
    TelnetConnectSecondsDelay : Integer; // Delay after getting Connected before trying to Log in to Telnet

    DelayLeft : Integer;

    XHairIncrement : Single;
    CircleIncrement: Single;
    ScaleIncrement : Single;
    XHairIncrementCoarse : Single;
    XHairIncrementMedium : Single;
    XHairIncrementFine : Single;
    CircleIncrementCoarse : Single;
    CircleIncrementMedium : Single;
    CircleIncrementFine : Single;
    ScaleIncrementCoarse : Single;
    ScaleIncrementMedium : Single;
    ScaleIncrementFine : Single;
    FirstConnectSweep  : Boolean;
    FirstReset : Boolean;
    InternalExposure : Boolean;
    TimedMessageType : TTMTypes;

    CameraActionHandler : TCameraActionRequestor;
    InLineErrorOccuredDuringJob : Boolean;
//    IsXhairButton : Boolean;

//    CurrentCameraState : TCameraState;
//    BufferCameraState : TCameraState;

    procedure TelnetCommandEntered(Sender : Tobject ; TelnetText : String);
    procedure LocateMessagePanel(FSize : Integer;FWidth : Integer);overload;
    procedure LocateMessagePanel(FWidth : Integer);overload;

    procedure AddDebug(DString : String;LowestMode :TVModes);
    procedure NewJobLoaded(Sender: Tobject;JobName : String);
    procedure CognexLockout(Sender: Tobject;ErrorMess : String);
    procedure CognexLoggedIn(Sender: Tobject);
    procedure CognexLoggedFail(Sender: Tobject);


    procedure NCCommandComplete(Success: Boolean;
                                Command : TCocgnexInputMessage;
                                CommandType : TNCCommands;
                                InternalResponse : String;
                                ExternalResponse : String;
                                ResponseState : Integer;
                                ErrMess : String);

    procedure CheckTalkingResponse(Sender: Tobject; Success: Boolean; JobName : String;TalkMode : TTalkingModes;FIsonline :Boolean);
    procedure CheckForIdleActions(Sender : Tobject;IsIdle : Boolean;var InLineErrStat: String;var InLineErrMess : String);
    procedure TelnetHealthyChange(Sender : Tobject;IsHealthy : Boolean);
    procedure OnLineChanged(Sender : Tobject;IsOnline : Boolean);
    procedure HandleTelnetBroken(Sender : Tobject);
    procedure TelnetConnectionChanged(Sender : Tobject;Connected : Boolean);
    procedure NCReady(Sender: TObject);
    procedure CameraGFLocked(Sender: TObject);
    procedure ImageScaleChanged(Sender : TObject);

    procedure ConnectionStateChanged(Sender : TObject);
    procedure SessionConnectionCompleted(Sender : TObject;ErrorNumber: Integer; const ErrorMessage: WideString);
    procedure CDisplaySatusChanged(Sender : TObject);

    procedure CdisplayStatusChange(Sender : TObject);
    procedure JobNameChanged(Sender : Tobject;JobName : String);
    procedure DoJog(Sender: TObject);
    procedure StartConnectingFTP(Sender: TObject);
    procedure NoCameraSessionConnect(Sender: TObject);
    procedure RetryTelnetConnect(Sender: TObject);
    procedure NoCameraFaultOnReconnect;
    procedure UnsolicitedReconnect;
    procedure UnsolicitedDisconnectReConnectOnReset;
    procedure NoCameraFaultUnsolicited;
    procedure DoCameraSessionConnect(Sender : Tobject);
    procedure DoCameraRetryConnect;
    procedure DoTelnetConnect(Sender : TObject);

    procedure DoMessageUpdate(Sender : TObject);
    procedure DoMessageOff(Sender : TObject);
    procedure ShowTelnet(ATag: TAbstractTag); // DisplayMode 2
    procedure ShowCamera (ATag: TAbstractTag);// DisplayMode 1
    {$IFDEF GRID_MODE_VALID}
    procedure ShowGrid (ATag: TAbstractTag);// DisplayMode 3
    {$ENDIF}

    procedure ShowSystem(ATag: TAbstractTag);
    procedure ClearButtonClick(Sender: TObject);
    procedure TelnetLatchClick(Sender: TObject);
    procedure RetryConnection(Sender: TObject);
    procedure SaveToFile(Sender: TObject);
    procedure TelnetButtonClick(Sender : TObject);

    procedure GetFunctionData;
    procedure RunFunction(ATag: TAbstractTag);
    procedure ResetButtonAction(ATag: TAbstractTag);
    procedure EstablishXHairAction(ATag: TAbstractTag);
    procedure InCycleChange(ATag: TAbstractTag);
    function IntegerAsFunction(FIndex: Integer): TNCLanguageFunctions;
    procedure DoXHairLeft(ATag: TAbstractTag);
    procedure DoXHairRight(ATag: TAbstractTag);

    procedure DoXHairUp(ATag: TAbstractTag);
    procedure DoXHairJogUp(ATag: TAbstractTag);

    procedure DoXHairDown(ATag: TAbstractTag);
    procedure DoXHairJogDown(ATag: TAbstractTag);

    procedure DoXHairOnOff(ATag: TAbstractTag);

    procedure DoCircleUp(ATag: TAbstractTag);
    procedure DoCircleDown(ATag: TAbstractTag);
    procedure DoCircleColor(ATag: TAbstractTag);
    procedure DoXHairColor(ATag: TAbstractTag);
    procedure DoCircleOnOff(ATag: TAbstractTag);
    procedure DoFitChanged(ATag: TAbstractTag);
    procedure DoFullScale(ATag: TAbstractTag);

    procedure RegisterScreenMode(ATag: TAbstractTag);
    procedure DoExposureChanged(ATag: TAbstractTag);
    procedure DoLatchJob(ATag: TAbstractTag);
    procedure DoCentreOnXHair(ATag: TAbstractTag);
    procedure DoScaleUp(ATag: TAbstractTag);
    procedure DoScaleDown(ATag: TAbstractTag);
    procedure DoScaleSet(ATag: TAbstractTag);
    procedure DisconnectRequested(ATag: TAbstractTag);
    procedure ReconnectRequested(ATag: TAbstractTag);
    procedure ProgramChanged(ATag: TAbstractTag);
    procedure TakeSnapshotNG(ATag: TAbstractTag);
    procedure TakeSnapshotWG(ATag: TAbstractTag);

    procedure TimedMessage(MessText: String; Interval: Integer);
    procedure DoBufferedCommand(Sender : TObject);
    procedure DoGotJobPassResult(Sender : TObject;Pass: Boolean;InLineError : Boolean;InFastRun : Boolean;ExecutionTime : Double);
    procedure OnLineFailed(Sender: TObject);





    function NamesMatch(Name1,Name2: String;Interlocked : Boolean):Boolean;
    procedure DoXReset(ATag: TAbstractTag);
    procedure DoXHairJogLeft(ATag: TAbstractTag);
    procedure DoXHairJogRight(ATag: TAbstractTag);
    procedure DoCircleJogUp(ATag: TAbstractTag);
    procedure DoCircleJogDown(ATag: TAbstractTag);
    procedure DoIncrementModeChange(ATag: TAbstractTag);
    procedure DoScaleJogDown(ATag: TAbstractTag);
    procedure DoScaleJogUp(ATag: TAbstractTag);
    procedure DoShowErrors(ATag: TAbstractTag);

    procedure SaveCameraState;
    function LoadImage: Boolean;
    procedure SetCurrentJobName(JobName: String);
    procedure CognexJobLoaded(Sender: Tobject; Success: Boolean);
    procedure MakeComponents;
    procedure ShowBannerMessage(Sender: TOBject; MessText: String);

   //Telnet Response Callbacks
   procedure ResetRestoreDone(Sender : Tobject;Success : Boolean);
   procedure ResetFitComplete(Sender: Tobject; Success: Boolean);
   procedure CameraResetComplete(Sender: TOBject);
   procedure CameraResetFail(Sender: TOBject);
   procedure ReconnectStarted(Sender: TOBject);
   procedure CameraReconnectComplete(Sender: TOBject);

   procedure DisconnectDone(Sender: TOBject);
   procedure CheckFitActions(Sender: TOBject);
   procedure JobRunComplete(Sender: Tobject; Success: Boolean);
//   procedure LiveAfterRunComplete(Sender: Tobject; Success: Boolean);
   procedure JobPassChecked(Success : Boolean;Passed : Boolean);
   procedure InitilaResetComplete(Sender: TOBject);
//   procedure BufferRestoreDone(Sender : Tobject;Success : Boolean);
   procedure DeafaultProgLoaded(Sender : Tobject; Success : Boolean);
   procedure GetValueDone(Sender : TObject; Success : Boolean;Value : String);
   procedure SetValueDone(Sender : TObject; Success : Boolean;Value : String);
//   procedure DefaultProgLoadedForBuffer(Sender : Tobject; Success : Boolean);

   procedure FTPIsLoggedIn(Sender : Tobject;LoginState : Boolean);
   procedure FTPLoggedInAndGotJobList(Sender : Tobject;LoginState : Boolean);
   procedure FTPJobListChanged(Sender : Tobject;AtPOwerup : Boolean);
   procedure SnapshotDone(Sender : Tobject;Success : Boolean);
   procedure TelnetSaveDone(Sender : Tobject;Success : Boolean);
   procedure TestedPowerUpDone (Sender : Tobject);


   procedure FTPFileImported(Filename : String; Success: Boolean);
   procedure GoneOnLineForImport(Sender : Tobject;Success : Boolean);
   procedure PostCommandOverrunDetected(Sender : Tobject);
   procedure FastJobRunDone(Sender: Tobject; Success: Boolean);
   procedure UserExposureChanged(Sender: Tobject; Success: Boolean;ExposureVal : Double);
   procedure TakeSnapShotTo(Path: String; WithGraphics: Boolean);
   function MakeDateStamp: String;
   function MakeNumberedDateStamp: String;
   function MakeTimeStamp: String;
   function MakeCommaSepTimeStamp: String;
   procedure SetCommandStataus(CSValue: INteger);
   procedure CameraSessionConnected(CState : Boolean);
   function CameraIsConnected: Boolean;
    procedure GotSymbolicValue(Sender: Tobject; Success: Boolean;Value: String);
//   function BufferCommand(Command: String; CommandType: TNCCommands; EMess: String): Boolean;



  protected
    procedure Resize; override;
    procedure Paint; override;
    procedure MakeCDisplay;
  public
    CameraConnectMode : TCameraConnectModes;
//    PowerUpConnectFailed : Boolean;
    CDisplay : TCvsInSightDisplay;
    CognexComms : TCognexComunicator;
    StartConnectTime : TDateTime;

    ConnectTime      : TDateTime;

    CurrentDisplayMode : TVisionDisplayModes;
    CurrentJobName : String;
//    CurrentJobIsBuffered : Boolean;

    constructor Create(aOwner : TComponent); override;
    procedure CDisplayBeginUpdate;
    procedure CDisplayEndUpdate;
    procedure CDisplayEndUpdateOnException;
    procedure StartConnectTimeout;


    procedure InstallComponent(Params : TParamStrings); override;
    procedure Installed; override;
    procedure Repaint; override;
    procedure IShutdown; override;

    procedure SetDisplayMode(DMode :TVisionDisplayModes); overload;
    procedure SetDisplayMode(DMode :TVisionDisplayModes;WithFullReset : Boolean;AtPOwerUp : Boolean); overload;
    procedure InternalSetExposure;
    procedure SetNoCognexMode(State : Boolean);
    function CanTalk:Boolean;
    function MakeTimeStampedSnapName: String;

   //***********************************************************************
   // Vision Comands implementataions
   //***********************************************************************
    function NCSendNativeCommand(CommandString : String;WaitForResponse : Boolean): Boolean;
    function NCGetSpreadsheetValue(CommandString: String): Boolean;
    function NCGetCellValue(CommandString: String): Boolean;
    function NCLoadJobCalled(JobName: String;ForceLoad : Boolean; LatchJob : Boolean): Boolean;
    function NCGetCurrentJobName: Boolean;
    function NCGetJobList: Boolean;
    function NCSetLiveVideo(LiveState : Boolean):Boolean;
    function NCGetSymbolicValue(Command : String):Boolean;
    function NCGetUserExposure:Boolean;
    function NCSetSymbolicValueAsString(Command : String):Boolean;
    function NCSetSymbolicValueAsInteger(Command : String):Boolean;
    function NCSetSymbolicValueAsFloat(Command : String):Boolean;
    function NCResetandRunJob(FastMode : Boolean):Boolean;
    function NCCheckJobPass(): Boolean;
    function NCSetScaling(): Boolean;
    function NCSetJobOffset(): Boolean;
    function NCSetExposure(ExposureVal : Double):Boolean;
    function NCSetCameraView(Scale : Double;CentreOnXHair : Integer):Boolean;
    function NCBufferToXHair():Boolean;
    function NCLoadFileFromDisk(FPath : String):Boolean;

 end;

var
GlobalCognexDisplay : TCvsInSightDisplay;
GlobalCognexForm : TCognexDisplay;

implementation



{ TCognexDisplay }

constructor TCognexDisplay.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
  ConRequestCount := 0;
  DisConRequestCount := 0;
  PerformingBufferedCommand := False;
  CommandIsBuffered := False;
  CameraConnectMode := tccmNone;
  FirstReset := True;
  InternalExposure := False;
//  CurrentJobIsBuffered := False;
  Name := 'CognexDisplay';
  IsClosing := False;
  LatchTelnet := False;
  LatchJob := False;
  InCycle := False;
  GlobalCognexForm := Self;
  MakeComponents;
  Disconnecting := False;
  Reconnected := False;
  ReConnecting := False;
  Disconnected := False;
  ResetActionEnable := False;
  
end;


Procedure TCognexDisplay.MakeComponents;
begin
  DisplayPanel := TPanel.create(Self);
  DisplayPanel.Parent:= Self;
  DisplayPanel.Align := alClient;
  Image := TImage.Create(Self);
  CameraSessionConnected(False);

  CognexComms := TCognexComunicator.Create;
  with CognexComms do
    begin
    OnLockOut := CognexLockout;
    OnJobNameChange := NewJobLoaded;
    OnLoginSuccess := CognexLoggedIn;
    OnLoginFail  := CognexLoggedFail;
    OnDefaultOnResetLoaded := DeafaultProgLoaded ;
    OnGotValue := GetValueDone;
    OnFailedToGoOffLine := OnLineFailed;
//    OnDefaultOnBufferLoaded:= DefaultProgLoadedForBuffer;
    OnJobLoaded := CognexJobLoaded;
    OnCheckTalking := CheckTalkingResponse;
    OnNCAvailable := NCReady;
    OnIdleBusyChange := CheckForIdleActions;
    OnGFLock := CameraGFLocked;
    OnNCCommandComplete := NCCommandComplete;
    OnCommandException := HandleTelnetBroken;
    OnTelnetBroken := HandleTelnetBroken;
    OnTelnetConnectChange := TelnetConnectionChanged;
    OnHealthyStateChanged := TelnetHealthyChange;
    OnOnlineChanged := OnLineChanged;
    // Telnet Response Callbacks
    OnCameraRestoreDone := ResetRestoreDone;
    OnFitComplete := ResetFitComplete;
    OnRunJobComplete := JobRunComplete;
    OnFastJobRunDone := FastJobRunDone;
//    OnLiveAfterJobRun:= LiveAfterRunComplete;
    OnCheckJobPassDone := JobPassChecked;
    OnUserExposureChanged := UserExposureChanged;
//!!!!!!!!    OnCameraRestoreOnBufferDone := BufferRestoreDone;
//    OnOnlineForImport := GoneOnLineForImport;
    end;


  CameraActionHandler := TCameraActionRequestor.Create;

  with CameraActionHandler do
    begin
//    CameraActionHandler.ScaleAndFitAreTemporary := False;

    CogNexCommunicator := CognexComms;
//    OnOffsetIsManualChange := OffsetOwnerChangeEvent;
    OnResetComplete := CameraResetComplete;
    OnResetFail := CameraResetFail;
    OnReconnectStart := ReconnectStarted;
    OnDisconectComplete := DisconnectDone;
    OnReconnectComplete := CameraReconnectComplete;
    OnInitialResetComplete := InitilaResetComplete;
    OnBannerMessage := ShowBannerMessage;
    OnFitComplete := CheckFitActions;
    OnFTPLoggedIn := FTPIsLoggedIn;
    OnJobListChanged := FTPJobListChanged;
    OnFTPLoginComplete := FTPLoggedInAndGotJobList;
    OnPostCommandOverrun := PostCommandOverrunDetected;
    OnSnapDone := SnapshotDone;
    ONGotValue := GotSymbolicValue;
    OnTestedPOwerUp := TestedPowerUpDone;
    OnFreeToDoImediateCommand := DoBufferedCommand;
    OnPassJobResult := DoGotJobPassResult;
//    aaOnFileImported := FTPFileImported;
    end;


  with DisplayPanel do
    begin
    Color := clYellow;
    BevelInner := bvLowered;
    Caption := '';
    Visible := True;
    Color := clWebMintcream;
    Align := alClient;
    DoubleBuffered := True;
    end;

  LeftDisplayPanel := TPanel.create(Self);
  LeftDisplayPanel.Parent:= DisplayPanel;
  with LeftDisplayPanel do
    begin
    Align := alLeft;
    Width :=1;
    Color := clWebLavender;
    BevelInner := bvNone;
    Caption := 'LeftDisplay Panel';
    Visible := True;
    Name := 'LeftDisplayPanel';
    end;


  CameraParentPanel := TPanel.Create(Self);
  CameraParentPanel.Parent := LeftDisplayPanel;
  with CameraParentPanel do
    begin
    Name := 'DPP';
    Align := alClient;
    Color := clWebPurple;
    BevelInner := bvNone;
    Caption := '';
    Visible := True;
    end;

  Image.Parent := CameraParentPanel;
  Image.Visible := True;

  ErrorDisplay := TErrorDisplayPanel.Create(Self);
  ErrorDisplay.Parent := Self;
  with  ErrorDisplay do
    begin
    Align := AlClient;
    Visible := False;
    end;

  FTPPanel :=  TPanel.create(Self);
  FTPPanel.Parent:= Self;
  with FTPPanel do
    begin
    Align := alNone;
    Height := 50;
    Width := 50;
    Color := clWebTan;
    BevelInner := bvLowered;
    Caption := 'FTP Panel';
    Visible := False;
    end;

  TelnetLowerPanel := TPanel.Create(Self);
//  TelnetLowerPanel.Parent := TelnetPanel;
  TelnetLowerPanel.Parent := LeftDisplayPanel;
  with TelnetLowerPanel do
    begin
    align := alBottom;
    Height := 28;
    BevelInner := bvLowered;
    Visible := False;
    end;

  StatusPanel := TPanel.Create(Self);
  StatusPanel.Parent := DisplayPanel;
  with StatusPanel do
    begin
    align := alRight;
    Width := 150;
    Font.Size := 8;
    BevelInner := bvLowered;
    Caption := '';
    BevelWidth := 1;
    Visible := False;
    end;


  TelnetMemo := TTelnetDisplay.Create(StatusPanel);
  TelnetMemo.Parent := LeftDisplayPanel;
  with TelnetMemo do
    begin
    Visible := False;
    align := alNone;
    Font.Size := 8;
    ReadOnly := True;
    TabStop := false;
    BufferLineSize := 400;
    Initialise(clBlack,clLtGray+$111111,clNavy,clLtGray+$222222,ClBlue,clLtGray+$333333);
    end;
  CameraActionHandler.TelnetMemo := TelnetMemo;


  TelnetIndicatorPanel := TTelnetIndicatorPanel.Create(Self);
  TelnetIndicatorPanel.Parent := StatusPanel;
  with TelnetIndicatorPanel do
    begin
    Height := 155;
    Align := alTop;
    end;

  TelnetSendEdit    := TTextInputCombo.Create(Self);
  TelnetSendEdit.Parent := TelnetLowerPanel;
  with TelnetSendEdit do
    begin
    Width := 400;
    Height := 22;
    Top :=2;
    Left := 2;
    OnTextEntered := TelnetCommandEntered;
    end;

   TelnetCameraButton := TSpeedButton.Create(Self);
   TelnetCameraButton.Parent := StatusPanel;
   with TelnetCameraButton do
      begin
      OnClick := TelnetButtonClick;
      Top := TelnetIndicatorPanel.Height+TelnetIndicatorPanel.Top+10;
      Left := 4;
      Height := 40;
      Width := StatusPanel.Width-8;
      Caption := 'Show Camera';
      end;

   TelnetDebugButton := TSpeedButton.Create(Self);
   TelnetDebugButton.Parent := StatusPanel;
   with TelnetDebugButton do
      begin
      GroupIndex := 301;
      OnClick := TelnetButtonClick;
      Top := TelnetCameraButton.Top+TelnetCameraButton.Height+2;
//      Top := TelnetIndicatorPanel.Height+TelnetIndicatorPanel.Top+20;
      Left := 4;
      Height := 40;
      Width := StatusPanel.Width-8;
      Caption := 'Debug';
      end;

    TelnetDuplexButton := TSpeedButton.Create(Self);
    TelnetDuplexButton.Parent := StatusPanel;
    with TelnetDuplexButton do
      begin
      GroupIndex := 301;
      Top := TelnetDebugButton.Top+TelnetDebugButton.Height+2;
      Left := 4;
      Height := 40;
      Width := StatusPanel.Width-8;
      Caption := 'Duplex';
      OnClick := TelnetButtonClick;
      end;



    TelnetStageButton := TSpeedButton.Create(Self);
    TelnetStageButton.Parent := StatusPanel;
    with TelnetStageButton do
      begin
      GroupIndex := 301;
      Top := TelnetDuplexButton.Top+TelnetDuplexButton.Height+2;
      Left := 4;
      Height := 40;
      Width := (StatusPanel.Width-10) div 2;
      Caption := 'Stage';
      OnClick := TelnetButtonClick;
      end;

    TelnetNoneButton := TSpeedButton.Create(Self);
    TelnetNoneButton.Parent := StatusPanel;
    with TelnetNoneButton do
      begin
      GroupIndex := 301;
      Top := TelnetStageButton.Top;
      Left := TelnetNoneButton.Left+TelnetStageButton.Width+6;
      Height := 40;
      Width := TelnetStageButton.Width;
      Caption := 'Off';
      OnClick := TelnetButtonClick;
      end;


  TelnetClearButton := TButton.Create(Self);
  TelnetClearButton.Parent := StatusPanel;
  with TelnetClearButton do
    begin
    Height := 22;
    Top := TelnetNoneButton.Top+TelnetNoneButton.Height+2;
    Left := 4;
    Width := StatusPanel.Width-8;
    Height := 40;
    OnClick := ClearButtonClick;
    Caption := 'Clear Memo';
    end;

  WriteToFileButton := TButton.Create(Self);
  WriteToFileButton.Parent := StatusPanel;
  with WriteToFileButton do
    begin
    Height := 40;
    Top := TelnetClearButton.Top+TelnetClearButton.Height+2;
    Left := 4;
    Width := StatusPanel.Width-8;
    Caption := 'Save';
    OnClick := SaveToFile;
    Visible := True;
    end;

  TelnetLatch := TCheckBox.Create(Self);
  TelnetLatch.Parent := StatusPanel;
  with TelnetLatch do
    begin
    Caption := 'Latch Telnet';
    Width := 140;
    Left := 6;
    Top := WriteToFileButton.Top+WriteToFileButton.Height+15;
    Checked := False;
    OnClick := TelnetLatchClick;
    end;

  ReconnectButton:= TButton.Create(Self);
  ReconnectButton.Parent := Self;
  with ReconnectButton do
    begin
    Width := 150;
    Height := 60;
    Caption := 'Re-Connect';
    OnClick := RetryConnection;
    Top := 300;
    Left := 100;
    Visible := False;
    end;

  FTPControlPanel := TFTPPanel.CreateOnParent(FTPPanel);
  FTPControlPanel.Parent := FTPPanel;
  with FTPControlPanel do
    begin
    Align := alClient;
    end;

end;

procedure TCognexDisplay.Paint;
begin
  inherited;
  if FirstPaint then
    begin
    FirstPaint := False;
//    TestIndicatorPanel.Redraw;
    end;
end;


function TCognexDisplay.LoadImage: Boolean;
var
  Filename: String;
begin
  try
    Filename:= ProfilePath + BackImageFileName;
    Result:= FileExists(Filename);
    if Result then begin
      Image.Picture.LoadFromFile(Filename);
      Image.Align := alClient;
      Image.Stretch := True;
      Show;
      Update
    end;
  except
    on E:EInvalidGraphic do
      Result:= false;
  end;
end;


procedure TCognexDisplay.Installed;
begin
  inherited Installed;
  LoadImage;
  DisplayModeEstablished := False;
  CameraActionHandler.EnableCameraUpdate := False;
  CameraParentPanel.Visible := True;
  AddDebug('Installed',tvmDebug);
  ResetInProgress := False;
  ConnectString := Format('%s,%s',[FCameraIP,FCameraName]);
  CameraActionHandler.ConnectString := ConnectString;
  CogNexComms.ExternalMemo := TelnetMemo;
  CurrentFunctionIndex := tccNone;
  TagEvent(Name + 'ShowTelnet', EdgeFalling, TMethodTag, ShowTelnet);
  TagEvent(Name + 'ShowCamera', EdgeFalling, TMethodTag, ShowCamera);
  TagEvent(Name + 'ShowErrors' , EdgeFalling, TMethodTag, DoShowErrors);
  {$IFDEF GRID_MODE_VALID}
  TagEvent(Name + 'ShowGrid', EdgeFalling, TMethodTag, ShowGrid);
  {$ENDIF}
  TagEvent(Name + 'ShowSystem', EdgeFalling, TMethodTag, ShowSystem);
  TagEvent(Name + 'RunFunction', EdgeRising, TMethodTag, RunFunction);
  SystemResetTag := TagEvent('ResetComplete', EdgeFalling, TMethodTag, ResetButtonAction);
  EstablishXHairTag := TagEvent(Name + 'EstablishCrossHair', EdgeFalling, TMethodTag, EstablishXHairAction);

  TagEvent('NC1InCycle'  , EdgeChange, TMethodTag, InCycleChange);
  TagEvent(Name + 'XHairReset'    , EdgeFalling, TMethodTag, DoXReset);
  TagEvent(Name + 'XHairUp'    , EdgeFalling, TMethodTag, DoXHairUp);
  TagEvent(Name + 'XHairDown'  , EdgeFalling, TMethodTag, DoXHairDown);
  TagEvent(Name + 'XHairLeft'  , EdgeFalling, TMethodTag, DoXHairLeft);
  TagEvent(Name + 'XHairRight' , EdgeFalling, TMethodTag, DoXHairRight);
  TagEvent(Name + 'ReqXHairOn' , EdgeChange, TMethodTag, DoXHairOnOff);
  TagEvent(Name + 'XHairJogDown'  , EdgeChange, TMethodTag, DoXHairJogDown);
  TagEvent(Name + 'XHairJogUp'  , EdgeChange, TMethodTag, DoXHairJogUp);
  TagEvent(Name + 'XHairJogLeft'  , EdgeChange, TMethodTag, DoXHairJogLeft);
  TagEvent(Name + 'XHairJogRight'  , EdgeChange, TMethodTag, DoXHairJogRight);


  TagEvent(Name + 'CircleUp'  , EdgeFalling, TMethodTag, DoCircleUp);
  TagEvent(Name + 'CircleDown'  , EdgeFalling, TMethodTag, DoCircleDown);
  TagEvent(Name + 'CircleJogUp'  , EdgeFalling, TMethodTag, DoCircleJogUp);
  TagEvent(Name + 'CircleJogDown'  , EdgeFalling, TMethodTag, DoCircleJogDown);
  TagEvent(Name + 'ReqCircleOn'  , EdgeChange, TMethodTag, DoCircleOnOff);
  TagEvent(Name + 'CircleColorNumber'  , EdgeChange, TIntegerTag, DoCircleColor);
  TagEvent(Name + 'XHairColorNumber'  , EdgeChange, TIntegerTag, DoXHairColor);
  TagEvent(Name + 'ScaleUp'  , EdgeFalling, TMethodTag, DoScaleUp);
  TagEvent(Name + 'ScaleDown'  , EdgeFalling, TMethodTag, DoScaleDown);
  TagEvent(Name + 'ScaleJogUp'  , EdgeChange, TMethodTag, DoScaleJogUp);
  TagEvent(Name + 'ScaleJogDown'  , EdgeChange, TMethodTag, DoScaleJogDown);

  TagEvent(Name + 'RequestScaleChange'  , EdgeChange, TDoubleTag, DoScaleSet);
  TagEvent(Name + 'CentreOnXHair'  ,EdgeChange, TDoubleTag, DoCentreOnXHair);

  TagEvent(Name + 'IncrementMode',EdgeChange,TIntegerTag,DoIncrementModeChange);

  TagEvent(Name + 'FitToPanel'  , EdgeFalling, TMethodTag, DoFitChanged);
  TagEvent(Name + 'FullScale'   , EdgeFalling, TMethodTag, DoFullScale);
  TagEvent(Name + 'RegisterScreenMode'  , EdgeFalling, TMethodTag, RegisterScreenMode);

  ResetExposureTag := TagEvent(Name + 'Exposure'  , EdgeFalling, TDoubleTag, DoExposureChanged);
  VisionLatchJobTag   := TagEvent(Name + 'LatchJob'  , EdgeChange, TIntegerTag, DoLatchJob);
  CameraActionHandler.LatchJobTag := VisionLatchJobTag;

  SetDisplayMode(vdmPOwerup,False,True);

  CognexComms.ResponseTimeoutInSeconds := ResponseTimeoutInSecs;
  if ResponseTimeoutInSecs <= 0 then
    begin
    CameraActionHandler.TimeoutEnabled := False;
    end
  else
    begin
    CameraActionHandler.TimeoutEnabled := True;
    end;

  VisionReqDisconnectTag   := TagEvent(Name +  'RequestDisconnect',EdgeFalling, TMethodTag,DisconnectRequested);
  VisionReqReconnectTag    := TagEvent(Name + 'RequestReconnect', EdgeFalling, TMethodTag, ReconnectRequested);
  SnapShotNGTag            := TagEvent(Name + 'SnapShotNoGraphics',EdgeFalling, TMethodTag, TakeSnapshotNG);
  SnapShotWGTag            := TagEvent(Name + 'SnapShotWithGraphics',EdgeFalling, TMethodTag, TakeSnapshotWG);


  ErrorDisplay.ErrorStack.YearTag   := TagEvent('CronYear'   , EdgeFalling, TIntegerTag, Nil);
  ErrorDisplay.ErrorStack.MonthTag  := TagEvent('CronMonth'  , EdgeFalling, TIntegerTag, Nil);
  ErrorDisplay.ErrorStack.DayTag    := TagEvent('CronDay'    , EdgeFalling, TIntegerTag, Nil);
  ErrorDisplay.ErrorStack.HourTag   := TagEvent('CronHour'   , EdgeFalling, TIntegerTag, Nil);
  ErrorDisplay.ErrorStack.MinuteTag := TagEvent('CronMinute' , EdgeFalling, TIntegerTag, Nil);
  ErrorDisplay.ErrorStack.SecondTag := TagEvent('CronSecond' , EdgeFalling, TIntegerTag, Nil);
  ErrorDisplay.ErrorStack.ErrorCountTag := ErrorStackCountTag;
  TagEvent('RemoteFileLoad' , EdgeChange, TStringTag, ProgramChanged);

  COFOVTag.AsInteger := 1;
  COXTag.AsInteger := 0;
  IsFullFOVTag.AsInteger := 0;
  OnLineTag.AsInteger := 0;
  ScalingTag.AsDouble := BufferCameraState.JobScaling;
  XoffsetTag.AsDouble := BufferCameraState.XOffset;
  YOffsetTag.AsDouble := BufferCameraState.YOffset;
  CameraActionHandler.ScalingTag := ScalingTag;
  CameraActionHandler.XoffsetTag := XOffsetTag;
  CameraActionHandler.YOffsetTag := YOffsetTag;


   if EmulatorMode then
      begin
      DelayLeft := InitialSessionConnectDelayTime div 1000;
      TimedMessageType := ttmtSessionEmulated;
      end
    else
      begin
      DelayLeft := (InitialSessionConnectDelayTime div 1000)-1;
      AddDebug(Format('PU Delay - %d',[InitialSessionConnectDelayTime]),tvmDebug);
      TimedMessageType := ttmtSessionConnect;
      end;
    MessageTimer.Enabled := True;
    CameraActionHandler.FTPPortNumber := FCameraFTPPortNumber;
    CameraActionHandler.FTPIPAddress := FCameraIP;
    CameraActionHandler.FTPUsername := FCameraName;
    CameraActionHandler.FTPPassword := '';
    FirstConnectSweep := True;
    InitialSessionConnectDelayTimer.Enabled := True;
end;

procedure TCognexDisplay.Repaint;
begin
  inherited;
end;

procedure TCognexDisplay.InstallComponent(Params: TParamStrings);
var
 CFile : TIniFile;
 Buffer : String;
begin
  LoginRetry := 0;
  Self.Color := clWebPink;
  CDisplay := nil;
  inherited InstallComponent(Params);
  DisplayModeEstablished := False;
  CameraActionHandler.EnableCameraUpdate := False;
  EventLog('Making Cognex Display');

  MakeCDisplay;

//  UpdateCalibration  := False;

  FirstShow := True;
  FirstPaint:= True;

  VisionJogMode := jmNone;
  CurrentCameraState.PixelHeight := 1200;
  CurrentCameraState.PixelWidth := 1600;
  CurrentCameraState.CentreMode := tcmScreen;
 // InstallDone := False;
  ConfigFileName := ExtractFilePath(Application.ExeName);

  PartLifeBasePath := Format('%s..\data\PartLife\',[ConfigFileName]);
  ConfigFileName := Format('%sLive\Vision.cfg',[ConfigFileName]);
  if FileExists(ConfigFileName) then
    begin
    CameraActionHandler.ConfigFileName := ConfigFilename;
    CFile := TIniFile.Create(ConfigFilename);
    if assigned(CFile) then
      try
      ImageOrientation  :=         CFile.ReadInteger('GENERAL','ImageOrientation',0);
      if (ImageOrientation <> 0) and (ImageOrientation <> 90) and (ImageOrientation <> 180) and (ImageOrientation <> 270) then
        begin
        ImageOrientation := 0;
        end;
      Buffer := CFile.ReadString('GENERAL','UseFTPServer','False');
      if (Buffer[1] = 'f') or (Buffer[1] = 'F')  then
        begin
        UseFTPServer := False
        end
      else
        begin
        UseFTPServer := False;//
        end;

      BackImageFileName :=         CFile.ReadString('GENERAL','BackgroundBitmapName','');
      InitialTelnetConnectDelayTime   := CFile.ReadInteger('GENERAL','InitialConnectDelayInSeconds',4);
      InitialTelnetConnectDelayTime   := InitialTelnetConnectDelayTime*1000;
      InitialSessionConnectDelayTime   := CFile.ReadInteger('GENERAL','InitialSessionConnectDelayInSeconds',12);
      SessionConnectRetries            := CFile.ReadInteger('GENERAL','SessionConnectReties',10);
      InitialSessionConnectDelayTime   := InitialSessionConnectDelayTime*1000;
      SessionConnectTimeOutTime := CFile.ReadInteger('GENERAL','SessionConnectTimeOutInSeconds',20);
      SessionConnectTimeOutTime := SessionConnectTimeOutTime*1000;
      DefaultJobName :=      CFile.ReadString('GENERAL','DefaultJobName','Crosshair.job');
      InitialRetryCount  :=  CFile.ReadInteger('GENERAL','InitialRetryCount',10);
      CameraActionHandler.DefaultJobName := DefaultJobName;
      ControlAllowedAllJobs  := CFile.ReadInteger('GENERAL','XHairControlAllowedAllJobs',0);
      TelnetConnectSecondsDelay := CFile.ReadInteger('GENERAL','TelnetConnectDelayInSeconds',15);
      TelnetConnectSecondsDelay := TelnetConnectSecondsDelay*1000;
      CurrentCameraState.PixelHeight := CFile.ReadInteger('General','CameraPixelHeight',1200);
      CurrentCameraState.PixelWidth := CFile.ReadInteger('General','CameraPixelWidth',1600);
      ServiceTimerPeriod := CFile.ReadInteger('General','ServiceTimerPeriod',10);
      ReadTimerPeriod := CFile.ReadInteger('General','ReadTimerPeriod',10);
      TelnetDebugBufferLength := CFile.ReadInteger('General','TelnetDebugBufferLength',300);
      TelnetMemo.FBufferLineSize :=  TelnetDebugBufferLength;
      CognexComms.BusyTimeout  := CFile.ReadInteger('General','LockBusyTimeoutInSeconds',10)*1000;
      SnapShotDirectory := CFile.ReadString('General','SnapshotPath','');
      ErrorStackClearMode := CFile.ReadInteger('General','ErrorStackClearMode',1);
      TelnetMemoLength    := CFile.ReadInteger('General','TelnetMemoLength',500);
      if assigned(TelnetMemo) then
        TelnetMemo.MaxLineCount := TelnetMemoLength;

      if SnapShotDirectory <> '' then
        begin
        if not DirectoryExists(SnapShotDirectory) then
          begin
          ForceDirectories(SnapShotDirectory);
          end
        end
      else
        begin
        SnapShotDirectory := ExtractFilePath(Application.ExeName);
        SnapShotDirectory := Format('%s..\data\ScriptOut\Snapshots\',[SnapShotDirectory]);
        end;
      ResponseTimeoutInSecs := CFile.ReadFloat('General','ResponseTimeoutInSeconds',20);
      JobLoadTimeoutInseconds := CFile.ReadFloat('General','JobLoadResponseTimeoutInSeconds',20);
      if not DirectoryExists(SnapShotDirectory) then ForceDirectories(SnapShotDirectory);

      Buffer := CFile.ReadString('General','ShowCameraOnReset','False');
      if (Buffer[1] = 'f') or (Buffer[1] = 'F')  then
        ShowCameraOnReset := False
      else
        ShowCameraOnReset := True;

      Buffer := CFile.ReadString('General','TelnetVerboseMode','Debug');
      if (Buffer[1] = 'f') or (Buffer[1] = 'F')  then
        begin
        CognexComms.VerboseMode := tvmDuplex;
        TelnetDuplexButton.Down := True;
        end
      else if (Buffer[1] = 'D') or (Buffer[1]='d') then
        begin
        CognexComms.VerboseMode := tvmDebug;
        TelnetDebugButton.Down := True;
        end
      else
        begin
        CognexComms.VerboseMode := tvmOff;
        TelnetNoneButton.Down := True;
        end;

      Buffer := CFile.ReadString('General','TelnetAtPOwerUp','False');
      if (Buffer[1] = 'f') or (Buffer[1] = 'F')  then
        begin
        TelnetAtPowerUp := False
        end
      else
        begin
        TelnetAtPowerUp := True;
        CognexComms.VerboseMode := tvmDebug;
        TelnetLatch.checked := True;
        LatchTelnet := True;
        end;

      with CognexComms do
        begin
        BufferCameraState.PixelWidth := 1600;
        BufferCameraState.PixelHeight := 1200;

        BufferCameraState.CircleRadius    := Cfile.ReadFloat('DYNAMIC','CircleRadius',50.00);
        CurrentCameraState.CircleRadius    := BufferCameraState.CircleRadius;
        BufferCameraState.aaXHairOn       := Cfile.ReadInteger('DYNAMIC','XHairOn',1);
        CurrentCameraState.aaXHairOn    := BufferCameraState.aaXHairOn;
        BufferCameraState.CircleOn      := Cfile.ReadInteger('DYNAMIC','CircleOn',1);
        CurrentCameraState.CircleOn    := BufferCameraState.CircleOn;
        BufferCameraState.XHairColorNumber := Cfile.ReadInteger('DYNAMIC','CircleColorNumber',11);
        CurrentCameraState.XHairColorNumber    := BufferCameraState.XHairColorNumber;
        BufferCameraState.CircleColorNumber := Cfile.ReadInteger('DYNAMIC','XHairColorNumber',11);
        CurrentCameraState.CircleColorNumber    := BufferCameraState.CircleColorNumber;
        BufferCameraState.Scale         := Cfile.ReadFloat('DYNAMIC','Scale',0.4);
        CurrentCameraState.Scale    := BufferCameraState.Scale;
        CameraActionHandler.ResetImageScale := BufferCameraState.Scale;

        BufferCameraState.CentreModeInteger    := Cfile.ReadInteger('DYNAMIC','XHCentreMode',1);
        CurrentCameraState.CentreModeInteger    := BufferCameraState.CentreModeInteger;
        CameraActionHandler.ResetXHMode         := BufferCameraState.CentreModeInteger;

        if BufferCameraState.CentreModeInteger = 0 then
          begin
          BufferCameraState.CentreMode := tcmScreen;
          end
        else
          begin
          BufferCameraState.CentreMode := tcmXHair;
          end;
//        BufferCameraState.FitMode       := Cfile.ReadInteger('DYNAMIC','FitMode',0);
        CurrentCameraState.FitMode      := Cfile.ReadInteger('DYNAMIC','FitMode',0);
        CameraActionHandler.ResetFitMode := CurrentCameraState.FitMode;

        BufferCameraState.Exposure      :=   Cfile.ReadFloat('DYNAMIC','Exposure',24.0);

        CurrentCameraState.Exposure     :=   BufferCameraState.Exposure;
        BufferCameraState.JobScaling    :=   Cfile.ReadFloat('DYNAMIC','JobScaling',239.123);

        CurrentCameraState.JobScaling     :=   BufferCameraState.JobScaling;
        BufferCameraState.XOffset :=   Cfile.ReadFloat('DYNAMIC','JobXoffset',0.123);
        CurrentCameraState.XOffset :=   BufferCameraState.XOffset;
        BufferCameraState.YOffset :=   Cfile.ReadFloat('DYNAMIC','JobYOffset',2.234);
        CurrentCameraState.YOffset :=   BufferCameraState.YOffset;
        BufferCameraState.OffsetIsManual := Cfile.ReadInteger('DYNAMIC','OffsetIsManual',1);
        CurrentCameraState.OffsetIsManual := BufferCameraState.OffsetIsManual;
        end;


      XHairIncrementCoarse := Cfile.ReadFloat('GENERAL','XHairIncrementCourse',10);
      XHairIncrementMedium := Cfile.ReadFloat('GENERAL','XHairIncrementMedium',5);
      XHairIncrementFine := Cfile.ReadFloat('GENERAL','XHairIncrementFine',1);
      CircleIncrementCoarse := Cfile.ReadFloat('GENERAL','CircleIncrementCourse',10);
      CircleIncrementMedium := Cfile.ReadFloat('GENERAL','CircleIncrementMedium',5);
      CircleIncrementFine := Cfile.ReadFloat('GENERAL','CircleIncrementFine',1);
      ScaleIncrementCoarse := Cfile.ReadFloat('GENERAL','ScaleIncrementCourse',0.1);
      ScaleIncrementMedium := Cfile.ReadFloat('GENERAL','ScaleIncrementMedium',0.05);
      ScaleIncrementFine := Cfile.ReadFloat('GENERAL','ScaleIncrementFine',0.001);

      XHairIncrement := XHairIncrementCoarse;
      CircleIncrement :=CircleIncrementCoarse;
      ScaleIncrement  :=ScaleIncrementCoarse;

      DefaultXOffset := Cfile.ReadFloat('GENERAL','DefaultXOffset',800);
      DefaultYOffset := Cfile.ReadFloat('GENERAL','DefaultYOffset',600);
      DefaultCircleRadius := Cfile.ReadFloat('GENERAL','DefaultCircleRadius',50);
      DefaultCircleColorNumber := Cfile.ReadInteger('GENERAL','DefaultCircleColorNumber',11);
      DefaultXHairColorNumber  := Cfile.ReadInteger('GENERAL','DefaultXHairColorNumber',11);

      finally
      CFile.Free;
      end;
    end;

  Buffer := Params.ReadString('EmulatorMode','False');
  if (Buffer[1]='t') or (Buffer[1] = 'T') then
     EmulatorMode := True
  else
     EmulatorMode := False;

  CognexComms.EmulatorMode := EmulatorMode;
  if EmulatorMode then
    begin
    CognexComms.VerboseMode := tvmDebug;
    FCameraName:= Params.ReadString('EmulatorCameraName','admin2');
    FCameraIP:= Params.ReadString('EmulatorIP','192.168.1.82');
    FCameraPortNumber := Params.ReadInteger('EmulatorPortNumber',23);
    end
  else
    begin
    FCameraName:= Params.ReadString('CameraName','admin');
    FCameraIP:= Params.ReadString('CameraIP','10.0.3.17');
    FCameraPortNumber := Params.ReadInteger('CameraPortNumber',23);
    end;

  FCameraFTPPortNumber     := Params.ReadInteger('CameraFTPPortNumber',23);



  VisionCommandStatusTag   := TagPublish(Name + 'CommandStatus',TIntegerTag);
  VisionErrorMessTag       := TagPublish(Name + 'ErrorMessage',TStringTag);
  VisionCanTalkTag         := TagPublish(Name + 'CanTalk',TIntegerTag);
  VisionResponseTag        := TagPublish(Name + 'InternalResponse',TStringTag);
  ExternalResponseTag      := TagPublish(Name + 'Response',TStringTag);
  VisionFunctionIndexTag   := TagPublish(Name + 'FunctionIndex',TIntegerTag);
  VisionParam1Tag          := TagPublish(Name + 'Param1',TStringTag);
  VisionParam2Tag          := TagPublish(Name + 'Param2',TStringTag);
  VisionParam3Tag          := TagPublish(Name + 'Param3',TStringTag);
  CurrentJobNameTag        := TagPublish(Name + 'CurrentJob',TStringTag);
  SceneScaleTag            := TagPublish(Name + 'SceneScale',TDoubleTag);
  XHairControlsInValidTag  := TagPublish(Name + 'XHairControlsInValid',TIntegerTag);
  JogIncFineTag            := TagPublish(Name + 'JogIncFine',TIntegerTag);
  JogIncMediumTag          := TagPublish(Name + 'JogIncMedium',TIntegerTag);
  JogIncCoarseTag          := TagPublish(Name + 'JogIncCoarse',TIntegerTag);
  COXTag                   := TagPublish(Name + 'IsCentredOnXHair',TIntegerTag);
  COFOVTag                 := TagPublish(Name + 'IsCentredOnFOV',TIntegerTag);
  IsFullFOVTag             := TagPublish(Name + 'IsFullFOV',TIntegerTag);
  CircleIsOnTag            := TagPublish(Name + 'CircleIsOn',TIntegerTag);
  XHairIsOnTag             := TagPublish(Name + 'XHairIsOn',TIntegerTag);
  VisionIdleTag            := TagPublish(Name + 'IsIdle',TIntegerTag);
  VisionDisconnectState    := TagPublish(Name + 'DisconnectState',TIntegerTag);
  OffsetIsManualTag        := TagPublish(Name + 'OffsetIsManual',TIntegerTag);
  OnlineTag                := TagPublish(Name + 'OnLine',TIntegerTag);
  XoffsetTag               := TagPublish(Name + 'Xoffset',TDoubleTag);
  YoffsetTag               := TagPublish(Name + 'Yoffset',TDoubleTag);
  ScalingTag               := TagPublish(Name + 'Scaling',TDoubleTag);
  JobPassedTag             := TagPublish(Name + 'JobPassed',TIntegerTag);
  JobInlineErrorTag        := TagPublish(Name + 'InLineErrorDuringJob',TIntegerTag);
  JobDataTag               := TagPublish(Name + 'LastJobData',TStringTag);
  ErrorStackCountTag       := TagPublish(Name + 'ErrorStackCount',TIntegerTag);
  LaunchTag                := TagPublish('LaunchCognexFTPServer',TIntegerTag);
  IsReadyTag               := TagPublish(Name + 'IsReady',TIntegerTag);
  CameraConnectedTag       := TagPublish(Name + 'CameraConnected',TIntegerTag);
  XHairBufferedTag         := TagPublish(Name + 'XHBuffered',TIntegerTag);
  DisableXhairButtonTag    := TagPublish(Name + 'DisableXHairButton',TIntegerTag);
  CurrentExposureTag       := TagPublish(Name + 'CurrentExposure'  , TDoubleTag);

//NewMode tbi
{ TODO : Newmode }
//  MaxCommandsInBufferTag   := TagPublish(Name + 'MaxCommands',TIntegerTag);
//  MaxCommandsInBufferTag.AsInteger := 0;

  CameraActionHandler.COXTag := COXTag;
  CameraActionHandler.COFOVTag := COFOVTag;
  CameraActionHandler.IsFullFOVTag := IsFullFOVTag;
  CameraActionHandler.XHairIsOnTag := XHairIsOnTag;
  CameraActionHandler.CircleIsOnTag := SceneScaleTag;
  CameraActionHandler.SceneScaleTag := SceneScaleTag;
  CameraActionHandler.OnLineTag := OnLineTag;
  CameraActionHandler.IsReadyTag := IsReadyTag;
  CameraActionHandler.XHairBufferedTag := XHairBufferedTag;
  CameraActionHandler.DisableXhairButtonTag := DisableXhairButtonTag;
  XHairBufferedTag.AsInteger := 0;
  OffsetIsManualTag.AsInteger := CurrentCameraState.OffsetIsManual;
  IsReadyTag.AsInteger := 0;
  DisableXhairButtonTag.ASInteger := 1;

{  try
  CDisplay := TCvsInSightDisplay.Create(nil);
  CDisplay.Parent := DisplayPanel;
  with CDisplay do
    begin
    Align := alClient;
    end;
  except
  TelnetMemo.Color := clRed;
  end;}

  MessPanel := TPanel.Create(nil);
  MessPanel.Parent := Self;
  with MessPanel do
    begin
    Font.Size := 12;
    Font.Color := clBlack;
    BorderStyle := bsNone;
    Left := 30;
    Top := 30;
    Width := 350;
    Height := 45;
    Visible := False;
    end;

{  MessageText := TLabel.Create(nil);
  MessageText.Parent := MessPanel;
  with MessageText do
    begin
    Top := 10;
    Left := 10;
    Font.Color := clRed;
    end;}


  JogTimer := TTimer.Create(nil);
  With JogTimer do
    begin
    Enabled := False;
    Interval := 100;
    OnTimer := DoJog;
    end;

{  ResetStuckTimer  := TTimer.Create(nil);
  With ResetStuckTimer do
    begin
    Enabled := False;
    Interval := 2000;
    OnTimer := ClearResetting;
    end;}
  InitialSessionConnectDelayTimer  := TTimer.create(nil);
  with InitialSessionConnectDelayTimer do
    begin
    Enabled := False;
    Interval := InitialSessionConnectDelayTime;
    OnTimer := DoCameraSessionConnect;
    end;


{ InitialCameraTelnetConnectDelay := TTimer.create(nil);
  with InitialCameraTelnetConnectDelay do
    begin
    Enabled := False;
    Interval := InitialTelnetConnectDelayTime;
    OnTimer := DoTelnetConnect;
    end;}


  CameraSessionConnectTimeout := TTimer.create(nil);
  with CameraSessionConnectTimeout do
    begin
    Enabled := False;
    Interval := SessionConnectTimeOutTime;
    OnTimer := NoCameraSessionConnect;
    end;

  CameraTelnetConnectTimeout  := TTimer.create(nil);
  with CameraTelnetConnectTimeout do
    begin
    Enabled := False;
    Interval := 10000;
    OnTimer := RetryTelnetConnect;
    end;

 TelnetConnectDelayTimer := TTimer.create(nil);
  With TelnetConnectDelayTimer do
    begin
    Enabled := False;
    Interval := TelnetConnectSecondsDelay;
    OnTimer := DoTelnetConnect;
    end;

  MessageTimer := TTimer.create(nil);
  With MessageTimer do
    begin
    Enabled := False;
    Interval := 1000;
    OnTimer := DoMessageUpdate;
    end;

  MessOffTimer := TTimer.create(Nil);
  With MessOffTimer do
    begin
    Enabled := False;
    Interval := 3000;
    OnTimer := DoMessageOff;
    end;


end;

procedure TCognexDisplay.Resize;
begin
  inherited;
  if not DisplayModeEstablished then exit;
  CameraActionHandler.RequestRestore(False);
end;


procedure TCognexDisplay.SetDisplayMode(DMode :TVisionDisplayModes);
begin
SetDisplayMode(DMode,False,False);
end;

procedure TCognexDisplay.SetNoCognexMode(State: Boolean);
begin
if State then
  AddDebug('No CognexMode set',tvmDebug)
else
  AddDebug('No CognexMode Cleared',tvmDebug);

FNoCognexMode := State;
if FNoCognexMode then CameraActionHandler.RestoreValid := False;
end;


procedure TCognexDisplay.InternalSetExposure;
begin
InternalExposure := True;
ResetExposureTag.AsDouble := BufferCameraState.Exposure;
CurrentCameraState.Exposure := BufferCameraState.Exposure;
end;

procedure TCognexDisplay.SetDisplayMode(DMode :TVisionDisplayModes;WithFullReset : Boolean;AtPOwerUp : Boolean);
begin
//TelnetLowerPanel.Visible := False;
StatusPanel.Visible := False;
ErrorDisplay.Visible := False;

//Bad if LatchTelnet and (DMode <> vdmTelnet) and (DMode <> vdmTelnetAndCamera) then
//Bad       DMode := vdmTelnet;

if not AtPOwerUp then
  begin
  DisplayModeEstablished := True;
  // Good
  if LatchTelnet and (DMode <> vdmTelnet) and (DMode <> vdmTelnetAndCamera) then DMode := vdmTelnet;
  if (CurrentDisplayMode = vdmCamera) and (DMode <> vdmError) and (NoCognex = False) then
    begin
    CameraActionHandler.SaveCameraState;
    end;
  end;

CurrentDisplayMode := DMode;

case DMode of
  {$IFDEF GRID_MODE_VALID}


  vdmUnknown,vdmCamera,VdmGrid:
  {$ELSE}
  vdmUnknown,vdmCamera:
  {$ENDIF}
    begin
    FTPPanel.Align := alNone;
    FTPPanel.Visible := False;
    FTPPanel.Height := 20;
    FTPPanel.Width := 20;
    DisplayPanel.Visible := True;
    DisplayPanel.Align := alClient;
  {$IFDEF GRID_MODE_VALID}
    if DMode = vdmGrid then
      begin
      CDisplayBeginUpdate;
      try
      try
      CDisplay.GridOpacity := 1;
      CDisplay.ShowGrid := True;
      CDisplay.Align := alClient;
      finally
        CDisplayEndUpdate;
      end;
      except
        CDisplayEndUpdateOnException;
      end;
      CurrentCameraState.FitMode := 1;
      CameraActionHandler.RequestFit(False);
      end
    else
  {$ENDIF}
      begin
      {$IFDEF GRID_MODE_VALID}
      CDisplay.ShowGrid := False;
     {$ENDIF}
      CDisplayBeginUpdate;
      try
      try
      CDisplay.Align := alClient;
      finally
        CDisplayEndUpdate;
      end;
      except
        CDisplayEndUpdateOnException;
      end;
      LeftDisplayPanel.Visible := True;
      LeftDisplayPanel.Align := alClient;
      CameraParentPanel.Visible  := True;
      CameraParentPanel.Align := alClient;
      if (NamesMatch(CurrentJobName,DefaultJobName,False)) and (CanTalk) and (not inCycle) then
        begin
        if WithFullReset then
          begin
          CDisplayBeginUpdate;
          try
          try
          CDisplay.Visible := False;
          finally
            CDisplayEndUpdate;
          end;
          except
            CDisplayEndUpdateOnException;
          end;
          Image.Visible := True;
          GoVisibleOnFit := True;
          CameraActionHandler.RequestReset(False,True);
          end
        else
          begin
          CDisplayBeginUpdate;
          try
          try
          CDisplay.Visible := False;
          finally
            CDisplayEndUpdate;
          end;
          except
          CDisplayEndUpdateOnException;
          end;

          Image.Visible := True;
          GoVisibleOnFit := True;
          CameraActionHandler.RequestRestore(False);
          end;
        end
      else
        begin
        // Program Is latched so no Crosshair functions... Just show the camera Display
          CDisplayBeginUpdate;
          try
          try
          CDisplay.Visible := False;
          finally
           CDisplayEndUpdate;
          end;
          except
            CDisplayEndUpdateOnException;
          end;
        Image.Visible := True;
        GoVisibleOnFit := True;
        CameraActionHandler.RequestRestore(False);
        end;
      end;
    end;

  vdmTelnetAndCamera:
    begin
    CameraParentPanel.Align := alBottom;
    CameraParentPanel.Height := (Self.Height div 4)*3;
    CameraParentPanel.Visible  := True;
    FTPPanel.Align := alNone;
    FTPPanel.Visible := False;
    FTPPanel.Height := 20;
    FTPPanel.Width := 20;
    StatusPanel.Visible := True;
    StatusPanel.Align := alRight;
    TelnetLowerPanel.Visible := True;
    TelnetLowerPanel.Align := alBottom;
    TelnetMemo.Visible := True;
    TelnetMemo.Align := AlClient;
    TelnetCameraVisible := True;
    CDisplayBeginUpdate;
    try
    try
    CDisplay.Align := alClient;
    if (NamesMatch(CurrentJobName,DefaultJobName,False)) and (CanTalk) and (not inCycle) then
      begin
      Image.Visible := True;
      CDisplay.Visible := False;
      GoVisibleOnFit := True;
      CameraActionHandler.RequestRestore(False);
      end
    else
      begin
      CDisplay.Visible := True;
      end;
    finally
    CDisplayEndUpdate;
    end;
    except
      CDisplayEndUpdateOnException;
    end;
    end;


  vdmTelnet:
    begin
    CameraParentPanel.Align := alNone;
    CameraParentPanel.Visible  := False;
    StatusPanel.Visible := True;
    StatusPanel.Align := alRight;
    TelnetLowerPanel.Visible := True;
    TelnetLowerPanel.Align := alBottom;
    FTPPanel.Align := alNone;
    FTPPanel.Visible := False;
    FTPPanel.Height := 20;
    FTPPanel.Width := 20;

    CDisplayBeginUpdate;
    try
    try
    CDisplay.Align := alNone;
    CDisplay.Visible := False;
    finally
    CDisplayEndUpdate;
    end;
    except
    CDisplayEndUpdateOnException;
    end;
    LeftDisplayPanel.Visible := True;
    LeftDisplayPanel.Align := alClient;
    TelnetMemo.Visible := True;
    TelnetMemo.Align := AlClient;
    TelnetCameraVisible := False;
    end;

  vdmSystem:
    begin
    DisplayPanel.Align := alNone;
    DisplayPanel.Visible := False;
    DisplayPanel.Height := 20;
    DisplayPanel.Width := 20;
    FTPPanel.Visible := True;
    FTPPanel.Align := alClient;
    end;

  vdmPOwerUp:
    begin
    DisplayPanel.Visible := True;
    DisplayPanel.Align := alClient;
    LeftDisplayPanel.Visible := True;
    LeftDisplayPanel.Align := alClient;
    CameraParentPanel.Visible := True;
    CameraParentPanel.Align := alClient;

    CDisplayBeginUpdate;
    try
    try
    if assigned(CDisplay) then
      begin
      CDisplay.Visible := False;
      end;
    finally
    CDisplayEndUpdate;
    end;
    except
    CDisplayEndUpdateOnException;
    end;
    DisplayModeEstablished := False;
    Image.Visible := True;
    Image.Align := alClient;
    Image.BringToFront;
    FTPPanel.Visible := False;
    end;

  vdmDisconnecting:
    begin
    DisplayModeEstablished := False;
    CDisplayBeginUpdate;
    try
    try
    CDisplay.Visible := False;
    finally
    CDisplayEndUpdate;
    end;
    except
    CDisplayEndUpdateOnException;
    end;
    CameraParentPanel.Visible  := True;
    CameraParentPanel.Align := alClient;
//    DisplayPanel.Align := alClient;
    DisplayPanel.Visible := True;
    LeftDisplayPanel.Visible := True;
    LeftDisplayPanel.Align := alClient;
    FTPPanel.Visible := False;
    Image.Visible := True;
    Image.BringToFront;
    FTPPanel.Align := alNone;
    end;

  vdmReconnecting:
    begin
    Image.Visible := True;
    DisplayModeEstablished := False;
    DisplayPanel.Align := alClient;
    LeftDisplayPanel.Visible := True;
    LeftDisplayPanel.Align := alClient;
    FTPPanel.Visible := False;
    FTPPanel.Align := alNone;
//  LocateMessagePanel(10,350);
    LocateMessagePanel(350);
    CDisplayBeginUpdate;
    try
    try
    CDisplay.Visible := False;
    finally
    CDisplayEndUpdate;
    end;
    except
    CDisplayEndUpdateOnException;
    end;
    GoVisibleOnFit := True;
    MessPanel.Caption := 'Reconnecting.. Please Wait';
    end;


  vdmError:
    begin
    DisplayPanel.Visible := False;
    FTPPanel.Visible := False;
    with ErrorDisplay do
      begin
      Visible := True;
      Align := alclient;
      InitialiseForDisplay;
      end;
    ReconnectButton.Visible := False;
    end;
  end; //case
end;


procedure TCognexDisplay.ShowTelnet(ATag: TAbstractTag);
begin
SetDisplayMode(vdmTelnet);
end;

procedure TCognexDisplay.ShowCamera(ATag: TAbstractTag);
var
TagVal : Integer;
begin
If Incycle then exit;
TagVal := ATag.AsInteger;
if TagVal = 0 then
  begin
  if Not LatchTelnet then SetDisplayMode(vdmCamera,True,False)
  end;
end;

procedure TCognexDisplay.ShowSystem(ATag: TAbstractTag);
begin
SetDisplayMode(vdmSystem);
end;

{$IFDEF GRID_MODE_VALID}
procedure TCognexDisplay.ShowGrid(ATag: TAbstractTag);
begin
SetDisplayMode(vdmGrid);
end;
{$ENDIF}

procedure TCognexDisplay.DoIncrementModeChange(ATag: TAbstractTag);
var
TagVal : Integer;
begin
if InstallDone then
  begin
  if NamesMatch(CurrentJobName,DefaultJobName,True) then
    begin
    TagVal := ATag.AsInteger;
    if TagVal = 1 then
      begin
      ScaleIncrement := ScaleIncrementFine;
      XHairIncrement := XHairIncrementFine;
      CircleIncrement:= CircleIncrementFine;
      JogIncFineTag.AsInteger := 1;
      JogIncMediumTag.AsInteger := 0;
      JogIncCoarseTag.AsInteger := 0;
      end else
    if TagVal = 2 then
      begin
      ScaleIncrement := ScaleIncrementMedium;
      XHairIncrement := XHairIncrementMedium;
      CircleIncrement:= CircleIncrementMedium;
      JogIncFineTag.AsInteger := 0;
      JogIncMediumTag.AsInteger := 1;
      JogIncCoarseTag.AsInteger := 0;
      end else
    if TagVal = 3 then
      begin
      ScaleIncrement := ScaleIncrementCoarse;
      XHairIncrement := XHairIncrementCoarse;
      CircleIncrement:= CircleIncrementCoarse;
      JogIncFineTag.AsInteger := 0;
      JogIncMediumTag.AsInteger := 0;
      JogIncCoarseTag.AsInteger := 1;
      end;
    end;
  end;
end;



procedure TCognexDisplay.DoLatchJob(ATag: TAbstractTag);
var
TagVal : Double;
begin
if Not InstallDone Then Exit;
if Assigned(ATag) then
  begin
  TagVal := ATag.AsInteger;
  if TagVal <> 0 then
     LatchJob := True
  else
     LatchJob := False;
  end;
end;


function TCognexDisplay.IntegerAsFunction(FIndex : Integer):TNCLanguageFunctions;
begin
Result := tccNone;
if (Findex > 0) and (Findex < Cardinal(tncInvalidFunction)) then

  begin
  case Findex of
    1: Result := tccSendNativeCommand;
    2: Result := tccGetSpreadsheetValue;
    3: Result := tccGetSymbolicValue;
    4: Result := tccSetSymbolicInteger;
    5: Result := tccSetSymbolicFloat;
    6: Result := tccSetSymbolicString;
    7: Result := tccLoadJobCalled;
    8: Result := tccGetCurrentJobName;
    9: Result := tccSetLiveVideo;
    10: Result := tccResetandRunJob;
    11: Result := tccSetJobScaling;
    12: Result := tccSetJobOffset;
    13: Result := tccCheckJobPass;
    14: Result := tccSetExposure;
    15: Result := tccSetCameraView;
    16: Result := tccBufferToXHair;
    17: Result := tccResetErrorStack;
    18: Result := tccGetErrorAtIndex;
    19: Result := tccLoadFileFromDisk;
    20: Result := tccSaveFileToDisk;
    21: Result := tccSaveImageToFileWithGraphics;
    22: Result := tccSaveImageToFileNoGraphics;
    23: Result := tccPartLifeSnapshotWithGraphics;
    24: Result := tccPartLifeSnapshotNoGraphics;
    25: Result := tccSaveTelnetLog;
    26: Result := tccGetUserExposure
    end;
 end;
end;

procedure TCognexDisplay.InCycleChange(ATag: TAbstractTag);
var
TagValue : Integer;
begin
TagValue := ATag.AsInteger;
if TagValue = 1 then
  begin
  InCycle := True;
  TelnetSendEdit.Enabled := False;
  end
else
  begin
  InCycle := False;
  TelnetSendEdit.Enabled := True;
  end;
end;

procedure TCognexDisplay.EstablishXHairAction(ATag: TAbstractTag);
var
TagValue : Integer;
begin
ResetButtonAction(ATag);
end;

procedure TCognexDisplay.ResetButtonAction(ATag: TAbstractTag);
var
TagValue : Integer;
begin
if (Not ResetActionEnable) or FNoCognexMode then
  begin
  if FNoCognexMode then AddDebug('NocognexMode is true',tvmDebug);
  if Not ResetActionEnable then AddDebug('ResetActionEnable is False',tvmDebug);
  if FNoCognexMode then
    begin
    EventLog('Ignoring Reset after No Cognex or cognex disconnect');
    end;
  AddDebug('Ignoring Reset at Power Up',tvmDebug);
  exit
  end
else
  begin
  AddDebug('Actioning Reset Button',tvmDebug);
  end;

TagValue := ATag.AsInteger;
if TagValue = 0 then
  begin
//  CameraActionHandler.ImmediateCommandPending := False;
  if (CameraConnectMode = tccmUnsolicitedDisconect) or (CameraConnectMode = tccmFailed)then
    begin
    CognexComms.GraceFullDisconnect;
      AddDebug('what to do here!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!',tvmDebug);
//    CameraConnectMode :=  tccmUnsolicitedReconect;
//    UnsolicitedDisconnectReConnectOnReset;
    exit;
    end;
  if CameraIsConnected and CognexComms.TelnetOk then
    begin
//    if CognexComms.IsIdle then
      begin
      if ATag = EstablishXHairTag then
        begin
        CameraActionHandler.RequestXHairEstablish(False);
        end
      else
        begin
        if VisionLatchJobTag.AsInteger = 1 then DisableXhairButtonTag.AsInteger := 0;
        CameraActionHandler.RequestReset(False,False);
        end
      end
    end
  else
   begin
     {$IFDEF HIGHDEBUG}
      AddDebug('Waiting For Camera To Connect',tvmDebug);
      if CameraisConnected then AddDebug('CameraConnected',tvmDebug) else AddDebug('Camera not Connected',tvmDebug);
      if CognexComms.TelnetOk then AddDebug('CognexComms TelnetOk',tvmDebug) else AddDebug('CognexComms NOT TelnetOk',tvmDebug);
    {$ENDIF}
   end;
  end;
end;

function StripFileExtension(InPath : String): String;
var
FileExt : String;
OrgLen : INteger;
begin
Result := InPath;
FileExt := ExtractFileExt(InPath);
if FileExt <> '' then
  begin
  OrgLen := Length(InPath);
  Result := Copy(Result,1,OrgLen-Length(FileExt));
  end;

end;

procedure TCognexDisplay.TakeSnapShotTo(Path : String;WithGraphics : Boolean);
var
BufferScale : Double;
RetryCount : INteger;
Done : Boolean;
NewPath : String;
TestPath: String;
begin
Path := StripFileExtension(Path);
if not assigned(CDisplay) then exit;
NewPath := Format('%s.bmp',[Path]);
   if FileExists(NewPath) then
    begin
    Done := False;
    RetryCount := 1;
    while (RetryCount < 11) and (not Done) do
      begin
      try
      NewPath := Format('%s_%d',[Path,RetryCount]);
      TestPath := Format('%s.bmp',[NewPath]);
      Done := Not FileExists(TestPath);
      finally
      inc(RetryCount);
      end;
      end;
    If Done then Path := NewPath else exit;
    end;
   Path := Format('%s.bmp',[Path]);

   CDisplayBeginUpdate;
   try
   BufferScale := CDisplay.ImageScale;
   CDisplay.ImageScale := 1.0;
   try
   if WithGraphics then
    begin
    CDisplay.ShowGraphics := True;
    CDisplay.SaveBitmap(Path);
    end
   else
    begin
    CDisplay.ShowGraphics := False;
    CDisplay.SaveBitmap(Path);
    CDisplay.ImageScale := BufferScale;
    CDisplay.ShowGraphics := True;
    end;
   CDisplay.ImageScale := BufferScale;
   finally
   CDisplayEndUpdate
   end;
   AddDebug(Format('Snapshot Saved To %s',[Param1]),tvmDebug);
   except
   CDisplay.ImageScale := BufferScale;
   CDisplay.ShowGraphics := True;
   AddDebug('Exception trying to save snapshot to following Path',tvmDebug);
   AddDebug(Format('%s',[Path]),tvmDebug);
   SysObj.FaultInterface.SetFault(Self, CoreFaults[cfSysWarning], [Format('Exception trying to save snapshot to following Path: %s',[Path])], nil);
   CDisplayEndUpdateOnException;
   end;
end;
procedure TCognexDisplay.SetCommandStataus(CSValue : INteger);
begin
  VisionCommandStatusTag.AsInteger := CSValue;
  TelnetIndicatorPanel.CommandReponseValueAsInteger := CSValue;
  TelnetMemo.CommandStatusNumber := CSValue;
//  if CSValue <> 0 then CameraActionHandler.ImmediateCommandPending := False;
end;

procedure TCognexDisplay.RunFunction(ATag: TAbstractTag);
var
PInt : Integer;
Buffer : String;
SnapPath : String;
begin
CurrentFunctionIndex := tccNone;
try
If CanTalk then
  begin
  VisionCanTalkTag.AsInteger := 1;
  SetCommandStataus(0);
  CognexComms.SetInLineError('','');
  GetFunctionData;
  case CurrentFunctionIndex of
    tccNone:
      begin

      end;

    tccSendNativeCommand:
      begin
      NCCommand := True;
      NCSendNativeCommand(Param1,False);
      end;

    tccGetSpreadsheetValue:
      begin
      NCCommand := True;
      NCGetSpreadsheetValue(Param1);
      end;


    tccGetSymbolicValue:
      begin
      NCCommand := True;
      NCGetSymbolicValue(Param1)
      end;

    tccSetSymbolicInteger:
      begin
      NCCommand := True;
      NCSetSymbolicValueAsInteger(Param1);
      end;

    tccSetSymbolicFloat:
      begin
      NCCommand := True;
      NCSetSymbolicValueAsFloat(Param1);
      end;

    tccSetSymbolicString:
      begin
      NCCommand := True;
      NCSetSymbolicValueAsString(Param1);
      end;

    tccLoadJobCalled:
      begin
      NCCommand := True;
      if Param2 <> '0' then
        begin
        if Param3 <> '0' then
          begin
          NCLoadJobCalled(Param1,True,True);
          end
        else
          begin
          NCLoadJobCalled(Param1,True,False);
          end
        end
      else
        begin
        if Param3 <> '0' then
          begin
          NCLoadJobCalled(Param1,False,True)
          end
        else
          begin
          NCLoadJobCalled(Param1,False,False)
          end
        end
      end;

    tccGetCurrentJobName:
      begin
      NCCommand := True;
      NCGetCurrentJobName;
      end;

    tccSetLiveVideo:
      begin
      NCCommand := True;
      if Param1 = '1' then
        NCSetLiveVideo(True)
      else
        NCSetLiveVideo(False);
      end;


    tccResetandRunJob:
      begin
      NCCommand := True;
      if ErrorStackClearMode = 1 then
        begin
        if assigned(ErrorDisplay) then
          begin
          ErrorDisplay.ClearErrors;
          end;
        end;
      PInt := StrToIntDef(Param1,0);
      if PInt = 1 then
        NCResetandRunJob(True)
      else
        NCResetandRunJob(False);
      end;

    tccSetJobScaling:
      begin
      NCCommand := True;
      CurrentCameraState.JobScaling := StrToFloatDef(Param1,-100.0);
      BufferCameraState.JobScaling := CurrentCameraState.JobScaling;
      AddDebug(Format('SettingScalingData %5.3f',[CurrentCameraState.JobScaling]),tvmDebug);
      CognexComms.JobScaling := CurrentCameraState.JobScaling;
      NCSetScaling();
      end;

    tccSetJobOffset:
      begin
      CurrentCameraState.XOffset := StrToFloatDef(Param1,-10.0);
      BufferCameraState.XOffset := CurrentCameraState.XOffset;
      CurrentCameraState.YOffset := StrToFloatDef(Param2,-10.0);
      BufferCameraState.YOffset := CurrentCameraState.YOffset;
      CurrentCameraState.OffsetIsManual := 0;
      CognexComms.JobXOffset := CurrentCameraState.XOffset;
      CognexComms.JobYOffset := CurrentCameraState.YOffset;
      AddDebug(Format('SettingOffsetData %5.3f %5.3f',[CurrentCameraState.XOffset,CurrentCameraState.YOffset]),tvmDebug);
      NCSetJobOffset();
//      XoffsetTag.AsDouble := CurrentCameraState.XOffset;
//      YoffsetTag.AsDouble := CurrentCameraState.YOffset;
//      NCCommandComplete(self,tncSetJobOffset,'1',1,'');
//      UpdateCalibration := True;
      CameraActionHandler.UpdateCalibration := True;
      CameraActionHandler.KickLazyUpdate;
      end;


    tccCheckJobPass:
      begin
      NCCommand := True;
      NCCheckJobPass;
      end;

    tccSetExposure:
      begin
      NCSetExposure(StrToFloatDef(Param1,20.0));
      end;

    tccSetCameraView:
      begin
      try
      NCSetCameraView(StrToFloatDef(Param1,1.0),StrToIntDef(Param2,1));
      except
      AddDebug(Format('Exception In SetCameraView %d',[Param2]),tvmDebug);
      end
      end;


    tccBufferToXHair:
      begin
      NCBufferToXHair();
      end;

    tccResetErrorStack:
      begin
      if assigned(ErrorDisplay) then
        begin
        ErrorDisplay.ClearErrors;
        end;
      end;

    tccGetErrorAtIndex:
      begin
      if assigned(ErrorDisplay) then
        begin
        PInt := StrToIntDef(Param1,-1);
        If PInt >= 0 then
          begin
          Buffer :=  ErrorDisplay.GetErrorTextAtIndex(PInt);
          NCCommandComplete(True,nil,tncGetErrorAtIndex,'','',1,Buffer);
          end
        else
          begin
          NCCommandComplete(True,nil,tncGetErrorAtIndex,'','',-1,'Invalid Error Index');
          end
        end
      else
        begin
        VisionCanTalkTag.AsInteger := 1;
        end;
      end;


    tccLoadFileFromDisk:
      begin
      NCLoadFileFromDisk(Param1);
      end;

    tccSaveImageToFileWithGraphics:
      begin
      try
      if not DirectoryExists(ExtractFileDir(Param1)) then
        begin
        ForceDirectories(ExtractFileDir(Param1));
        end;
      CDisplayBeginUpdate;
      try
      try
      CameraActionHandler.RequestSaveSnapshot(Param1,True,CDisplay.ImageScale);
      finally
      CDisplayEndUpdate;
      end;
      except
      CDisplayEndUpdateOnException;
      end;


      CDisplayEndUpdate

//      TakeSnapShotTo(Param1,True);
//      NCCommandComplete(True,nil,tncSaveSnapshot,'',1,Buffer);
      except
      AddDebug('Could not make directory For Snapshot',tvmDebug);
      NCCommandComplete(False,nil,tncSaveSnapshot,'','',-1,Buffer);
//    SysObj.FaultInterface.SetFault(Self, CoreFaults[cfSysWarning], [Format('Unable to Make Directory for Snapshot: %s',[Param1])], nil);
      end
      end;

    tccSaveImageToFileNoGraphics:
      begin
      try
      if not DirectoryExists(ExtractFileDir(Param1)) then
        begin
        ForceDirectories(ExtractFileDir(Param1));
        end;
      CDisplayBeginUpdate;
      try
      try
      CameraActionHandler.RequestSaveSnapshot(Param1,False,CDisplay.ImageScale);
      finally
      CDisplayEndUpdate;
      end;
      except
      CDisplayEndUpdateOnException;
      end;
//      TakeSnapShotTo(Param1,False);
      except
      AddDebug('Could not make directory For Snapshot',tvmDebug);
      SysObj.FaultInterface.SetFault(Self, CoreFaults[cfSysWarning], [Format('Unable to Make Directory for Snapshot: %s',[Param1])], nil);
      CDisplayEndUpdate;
      end
      end;

    tccPartLifeSnapshotWithGraphics:
     begin
     SnapPath := Format('%s\%s\%s',[PartLifeBasePath,Param1,Param2]);
     If Not DirectoryExists(SnapPath) then ForceDirectories(SnapPath);
     SnapPath := Format('%s\%s',[SnapPath,Param3]);
//     TakeSnapShotTo(SnapPath,True);
     CDisplayBeginUpdate;
     try
     try
     CameraActionHandler.RequestSaveSnapshot(SnapPath,True,CDisplay.ImageScale);
     finally
     CDisplayEndUpdate;
     end;
     except
     CDisplayEndUpdateOnException;
     end;

     AddDebug(Format('Snapshot Saved To %s',[SnapPath]),tvmDebug);
     end;

    tccPartLifeSnapshotNoGraphics:
     begin
     SnapPath := Format('%s\%s\%s',[PartLifeBasePath,Param1,Param2]);
     If Not DirectoryExists(SnapPath) then ForceDirectories(SnapPath);
     SnapPath := Format('%s\%s',[SnapPath,Param3]);
     CDisplayBeginUpdate;
     try
     try
     CameraActionHandler.RequestSaveSnapshot(SnapPath,False,CDisplay.ImageScale);
     finally
     CDisplayEndUpdate;
     end;
     except
     CDisplayEndUpdateOnException;
     end;
//     TakeSnapShotTo(SnapPath,False);
     AddDebug(Format('Snapshot Saved To %s',[SnapPath]),tvmDebug);
     end;

     tccSaveTelnetLog:
       begin
       CDisplayBeginUpdate;
       try
       try
       CameraActionHandler.RequestSaveTelnetLog();
       finally
        CDisplayEndUpdate;
       end;
       except
        CDisplayEndUpdateOnException;
       end;

//     TakeSnapShotTo(SnapPath,False);
       AddDebug(Format('Telnet Log Saved To %s',[SnapPath]),tvmDebug);

       end;

    tccGetUserExposure:
      begin
      NCCommand := True;
      NCGetUserExposure;
      end;

 end; //case

  end
else
  begin
  VisionResponseTag.AsInteger := -200;
  GetFunctionData;
  if NOT CameraActionHandler.aaIsIdle then
    begin
    if(CurrentFunctionIndex <> tccSetCameraView) then
      begin
      AddDebug(Format('Failed to perform part program function number %d',[cardinal(CurrentFunctionIndex)]),tvmDebug);
      end
    else
      begin
      AddDebug('Ignored Zoom Button',tvmDebug);
      end;

    end;
  end
except
  AddDebug(Format('Exception In Run Function %d',[cardinal(CurrentFunctionIndex)]),tvmDebug);
end;
end;






procedure TCognexDisplay.IShutDown;
begin
  try
  IsClosing := True;
//  InitialCameraTelnetConnectDelay.Enabled := false;
//  InitialCameraTelnetConnectDelay.Free;

  CameraSessionConnectTimeout.Enabled := false;
  CameraSessionConnectTimeout.Free;

  JogTimer.Enabled := False;
  JogTimer.Free;

  CognexComms.Free;
  CDisplay.Disconnect;
  Cdisplay.Free;
  CDisplay := nil;


  MessageTimer.Enabled := False;
  MessageTimer.Free;
  MessPanel.Visible := False;
  MessPanel.Free;
//  UpdateCalibration := True;

  TelnetConnectDelayTimer.Enabled := False;
  TelnetConnectDelayTimer.Free;

  CameraActionHandler.ShutDownRequest;
  CameraActionHandler.Free;

  except
  Color := clred;
  end;

  inherited IShutDown;
end;

procedure TCognexDisplay.ClearButtonClick(Sender: TObject);
begin
TelnetMemo.Clear;
end;

procedure TCognexDisplay.TelnetLatchClick(Sender: TObject);
begin
  LatchTelnet := TelnetLatch.checked
end;

function TCognexDisplay.CanTalk: Boolean;
begin
  if Disconnecting or Disconnected then
    begin
    Result := False;
    end
  else
    Result := (CognexComms.CanTalk);
//    Result := (CognexComms.CanTalk and CameraActionHandler.aaIsIdle);
end;




{function TCognexDisplay.BufferCommand(Command : String;CommandType :TNCCommands;EMess : String): Boolean;
begin
if not CommandIsBuffered then
  begin
  AddDebug(Format('%s :Buffering Command',[Command]),tvmDebug);
  CommandIsBuffered := True;
  BufferredCommandType := CommandType;
  BufferedComandString := Command;
  BufferedCommandErrorString := EMess;
  CameraActionHandler.ImmediateCommandPending := True;
  end
else
  begin
   AddDebug(Format('%s :Imediate Command overrun',[Command]),tvmDebug);
  end;
end;}

//**************************************************************
// NCCommand FUNCTIONS
//**************************************************************
function TCognexDisplay.NCSendNativeCommand(CommandString: String; WaitForResponse: Boolean): Boolean;
begin
Result := False;
if CanTalk then
  begin
  if CognexComms.IsConnected then
    begin
    NCCommand := True;
    CognexComms.SendNCCommand(CommandString,tncSendNativeCommand,'Native command',True);
    Result := True;
    end;
  end
{ else
   begin
   If CognexComms.CanTalk then
      begin
      BufferCommand(CommandString,tncSendNativeCommand,'Native command');
      end;
   end}
end;


function TCognexDisplay.NCGetSpreadsheetValue(CommandString : String): Boolean;
begin
Result := False;
if CanTalk then
  begin
  if CognexComms.IsConnected then
    begin
    NCCommand := True;
    CognexComms.SendNCCommand(CommandString,tncGetSpreadsheetValue,'getting spread sheet value',True);
    Result := True;
    end;
  end;
end;

function TCognexDisplay.NCGetCellValue(CommandString: String): Boolean;
begin
Result := False;
if CanTalk then
  begin
  if CognexComms.IsConnected then
    begin
    NCCommand := True;
    CognexComms.SendNCCommand(CommandString,tncGetCellValue,'getting cell value',True);
    Result := True;
    end;
  end;
end;


function TCognexDisplay.NCLoadJobCalled(JobName: String;ForceLoad : Boolean; LatchJob : Boolean): Boolean;
var
CommandString : String;
begin
Result := False;
if CanTalk then
    begin
    XHairControlsInValidTag.AsInteger := 1;
    CommandString := Uppercase(JobName);
    if LatchJob then
      begin
      VisionLatchJobTag.AsInteger := 1;
      CameraActionHandler.LatchJob := True;
      end
    else
      begin
      VisionLatchJobTag.AsInteger := 0;
      DisableXhairButtonTag.AsInteger := 1;
      CameraActionHandler.LatchJob := False;
      end;
    if not ForceLoad then
      begin
      If NamesMatch(JobName,CurrentJobName,False) then
        begin
        NCCommandComplete(True,nil,tncLoadJobCalled,'1','',1,'');
        Result := True;
        exit;
        end
      end;
    if POS('.JOB',CommandString) = 0  then
      begin
      CommandString := Format('%s.JOB',[CommandString]);
      end;
    if LatchJob then
      begin
      MessPanel.Caption := Format('Loading %s (Latched)',[Jobname]);
//      LocateMessagePanel(12,450);
      LocateMessagePanel(450);
      end
    else
      begin
//      MessPanel.Caption := Format('Loading %s (Unlatched)',[Jobname]);
      MessPanel.Caption := Format('Loading %s (Unlatched)',[Jobname]);
//      LocateMessagePanel(12,490);
      LocateMessagePanel(490);
      end;
    CameraActionHandler.RequestLoadJob(CommandString,LatchJob,False);
    Result := True;
    end;
end;


function TCognexDisplay.NCGetCurrentJobName: Boolean;
begin
Result := False;
if CanTalk then
    begin
    NCCommand := True;
    CognexComms.SendNCCommand('GF',tncGetCurrentJobName,'getting current job name',True);
    Result := True;
    end;
end;

function TCognexDisplay.NCGetJobList: Boolean;
begin
Result := False;
if CanTalk then
    begin
    NCCommand := True;
    CognexComms.SendNCCommand('Get FileList',tncGetJobList,'getting job list',True);
    Result := True;
    end;
end;

function TCognexDisplay.NCSetLiveVideo(LiveState: Boolean): Boolean;
var
CommandString : String;
begin
If LatchJob or EmulatorMode  then
  begin
  Result := True;
  CommandString := 'GF';
  AddDebug('inhibited set live as Job is latched',tvmDebug);
  if CanTalk then
      begin
      NCCommand := True;
      CognexComms.SendNCCommand(CommandString,tncGetCurrentJobName,'set live video',True);
      Result := True;
      end;
  end
else
  begin
  CommandString := 'Put Live ';
  if LiveState then
    begin
    CommandString := CommandString+'1';
    end
  else
    begin
    CommandString := CommandString+'0';
    end;
  Result := False;
  if CanTalk then
      begin
      NCCommand := True;
      CognexComms.SendNCCommand('SO0',tncSetOfflineOnReset,'set offline on reset',False);
      CognexComms.SendNCCommand(CommandString,tncSetLiveState,'change live state',False);
      Result := True;
      end;
   end
end;

function TCognexDisplay.NCGetUserExposure: Boolean;
begin
if CanTalk then
    begin
    NCCommand := True;
    CognexComms.SendNCCommand('GVAcquisition.Exposure_Time',tncGetUserExposure,'getting User Exposure',True);
    Result := True;
    end
  else
    begin
    Result := False;
    AddDebug('Failed Getting User Exposure',tvmDebug);
    end

end;


function TCognexDisplay.NCGetSymbolicValue(Command: String): Boolean;
begin
//Result := False;
if CanTalk then
    begin
    NCCommand := True;
    CameraActionHandler.RequestGetValue(Command);
//    CognexComms.SendNCCommand(Command,tncGetSymbolicValue,'get symbolic value',True);
    Result := True;
    end
  else
    begin
    Result := False;
    AddDebug('Failed Gtting Symbolic Value',tvmDebug);
    end
end;

function TCognexDisplay.NCSetSymbolicValueAsString(Command: String): Boolean;
begin
if CanTalk then
    begin
    NCCommand := True;
//    CognexComms.SendNCCommand(Command,tNCSetSymbolicValueAsString,'set symbolic value as String',True);
    Result := True;
    end
  else
    begin
    Result := False;
    AddDebug('Failed Setting Symbolic String',tvmDebug);
    end
end;

function TCognexDisplay.NCSetSymbolicValueAsFloat(Command: String): Boolean;
begin
if CanTalk then
    begin
    NCCommand := True;
    CognexComms.SendNCCommand(Command,tNCSetSymbolicValueAsFloat,'set symbolic value as Float',True);
    Result := True;
    end
  else
    begin
    Result := False;
    AddDebug('Failed Setting Symbolic Float',tvmDebug);
    end
end;

function TCognexDisplay.NCSetSymbolicValueAsInteger(Command: String): Boolean;
begin
if CanTalk then
    begin
    NCCommand := True;
    CognexComms.SendNCCommand(Command,tncSetSymbolicValueAsInteger,'set symbolic value as integer',True);
    Result := True;
    end
  else
    begin
    Result := False;
    AddDebug('Failed Setting Symbolic Integer',tvmDebug);
    end
end;

function TCognexDisplay.NCResetandRunJob(FastMode : Boolean):Boolean;
begin
Result := False;
if CanTalk then
    begin
    InLineErrorOccuredDuringJob := False;
    JobInlineErrorTag.AsInteger := 0;
    CameraActionHandler.RequestRunJob(FastMode);
    Result := True;
    end;
end;


function TCognexDisplay.NCSetJobOffset: Boolean;
begin
Result := False;
if CanTalk then
    begin
    NCCommand := True;
    CognexComms.SendNCCommand(Format('SFCalData.XOffset %5.3f',[BufferCameraState.XOffset]),tncSetJobXOffset,'set job offset',True);
    Result := True;
    end;
end;

function TCognexDisplay.NCSetScaling: Boolean;
begin
if CanTalk then
    begin
    NCCommand := True;
    CognexComms.SendNCCommand(Format('SFCalData.Scaling %5.3f',[BufferCameraState.JobScaling]),tncSetJobScaling,'set job scaling',True);
    Result := True;
    end;
end;

function TCognexDisplay.NCCheckJobPass: Boolean;
begin
Result := False;
if CanTalk then
    begin
    JobPassedTag.AsInteger := -1;
    CameraActionHandler.RequestCheckJobPass(InLineErrorOccuredDuringJob);
    Result := True;
    end;
end;

function TCognexDisplay.NCSetExposure(ExposureVal: Double): Boolean;
begin
Result := False;
if CanTalk then
    begin
    CognexComms.InsipientExposure := ExposureVal;
    CognexComms.SendNCCommand(Format('SFAcquisition.Exposure_Time %5.3f',[ExposureVal]),tncSetJobExposure,'set exposure',True);
    Result := True;
    end;
end;

function TCognexDisplay.NCSetCameraView(Scale: Double; CentreOnXHair: Integer): Boolean;
begin
Result := False;
if CanTalk then
    begin
    if Scale = 0 then
      begin
      CameraActionHandler.RequestFit(False);
      end
    else
      begin
      CameraActionHandler.ChangeLoadedJobImageScale(Scale,CentreOnXHair);
      CameraActionHandler.KickLazyUpdate;
      NCCommandComplete(True,nil,tncSetCameraMode,'1','',1,'');
      end;
    Result := True;
    end;
end;

function TCognexDisplay.NCBufferToXHair(): Boolean;
begin
Result := False;
if CanTalk then
    begin
    CameraActionHandler.RequestBufferToXHair(False);
    Result := True;
    end;
end;

function TCognexDisplay.NCLoadFileFromDisk(FPath: String): Boolean;
begin
Result := True;
CameraActionHandler.RequestImportFile(FPath);
end;



procedure TCognexDisplay.GetFunctionData;
var
FIndex : Integer;
begin
Findex := VisionFunctionIndexTag.AsInteger;
CurrentFunctionIndex := IntegerAsFunction(Findex);
Param1 := VisionParam1Tag.AsString;
Param2 := VisionParam2Tag.AsString;
Param3 := VisionParam3Tag.AsString;
end;







procedure TCognexDisplay.ConnectionStateChanged(Sender: TObject);
var
IsConnected : Boolean;
begin
 if CDisplay.Connected then
   begin
   ConRequestCount := ConRequestCount-1;
   AddDebug(Format('Camera Connection Change to CONNECTED %d',[ConRequestCount]),tvmDebug);

   end
 else
   begin
   DisConRequestCount := DisConRequestCount-1;
   AddDebug(Format('Camera Connection Change to DISCONNECTED %d',[DisConRequestCount]),tvmDebug);
   end ;

 if IsClosing then  exit;

 if assigned(CDisplay) then
  begin
    DisplayPanel.Visible := True;
    CDisplayBeginUpdate;
    try
    try
    IsConnected := CDisplay.Connected;
    finally
    CDisplayEndUpdate;
    end;
    except
     CDisplayEndUpdateOnException;
    end;

    if IsConnected  then
       begin
       ResetActionEnable := True;
       CameraActionHandler.RestoreValid := True;
       end
     else
       begin
       if CameraActionHandler.InDisconnect then
          begin
          AddDebug('Ending wait for disconnect',tvmDebug);
          CameraActionHandler.EndWaitForCameraDisconnect(True);
          end
       else
         begin
         AddDebug('NOT IN DISCONECT',tvmDebug);
         MessPanel.Caption := 'Camera Disconnected';
//         LocateMessagePanel(10,350);
         LocateMessagePanel(350);
         TelnetConnectDelayTimer.Enabled := False;
         MessageTimer.Enabled := False;
         NoCameraFaultUnsolicited;
         end;
       end
  end;
end;


procedure TCognexDisplay.CDisplaySatusChanged(Sender: TObject);
begin
if CameraActionHandler.InDisConRecon then
  begin
  AddDebug(Format('%d:%s',[CDisplay.StatusInformationState,CDisplay.StatusInformation]),tvmDebug);
  end;
end;


procedure TCognexDisplay.SessionConnectionCompleted(Sender: TObject; ErrorNumber: Integer; const ErrorMessage: WideString);
var
IsConnected : Boolean;
begin
 CameraSessionConnectTimeout.Enabled := False;
 AddDebug('CameraConnectTimeout.Enabled := False',tvmDebug);
 AddDebug('Connection Completed with Following State',tvmDebug);
 AddDebug(Format('%d : %s',[ErrorNumber,ErrorMessage]),tvmDebug);
 if IsClosing then  exit;
 CDisplayBeginUpdate;
 try
 try
 IsConnected := CDisplay.Connected;
 finally
 CDisplayEndUpdate
 end;
 except
  CDisplayEndUpdateOnException;
 end;
 if IsConnected  then
     begin
     {$IFDEF HIGHDEBUG}
     AddDebug('CameraConnectionComplete',tvmDebug);
     ConnectTime := Now-StartConnectTime;
     ConnectTime := ConnectTime*24*60*60*1000;
     AddDebug(Format('Got Connected %f ms.',[ConnectTime]),tvmDebug);
     {$ENDIF}
     try
     DisplayPanel.Color := clWebOrange;
     MessPanel.Font.Color := clBlack;
     MessPanel.Font.Size := 12;
     ReconnectButton.Visible := False;
     CameraSessionConnected(True);
     CameraConnectMode := tccmConnected;
     if CameraActionHandler.InDisConRecon then
      begin
      MessPanel.Caption := 'Testing Camera 1';
//      LocateMessagePanel(10,350);
      LocateMessagePanel(350);
      end
     else
      begin
      MessPanel.Caption := 'Camera is Live';
//      LocateMessagePanel(12,350);
      LocateMessagePanel(350);
      end;
     except
     {$IFDEF HIGHDEBUG}
     AddDebug('Exception After Camera Connect',tvmOff);
     {$ENDIF}
     end;
     DelayLeft := (TelnetConnectSecondsDelay div 1000)-1;
     TimedMessageType := ttmtTelnetConnect;
     MessageTimer.Enabled := True;
     TelnetConnectDelayTimer.Enabled := True;
     CDisplayBeginUpdate;
     try
     try
     CDisplay.Visible := True;
     finally
     CDisplayEndUpdate;
     end;
     except
      CDisplayEndUpdateOnException;
     end;
     end
  else
    begin
    AddDebug('CameraConnectionFailed ',tvmDebug);
    CameraSessionConnectTimeout.Enabled := False;
    NoCameraSessionConnect(nil);
    end;

end;




procedure TCognexDisplay.ImageScaleChanged(Sender : TObject);
var
PWidth : Integer;
PHeight: Integer;
ImageWidth: Integer;
ImageHeight: Integer;
MidPos : INteger;
Xoff,Yoff : Integer;
begin
PWidth := Width;
PHeight := Height;
CDisplayBeginUpdate;
try
try
CDisplay.Align := alClient;
SceneScaleTag.AsDouble := CDisplay.ImageScale;
if CDisplay.ImageScale > 0.1 then
  begin
  ImageWidth := Round(CurrentCameraState.PixelWidth * CDisplay.ImageScale);
  ImageHeight:= Round(CurrentCameraState.PixelHeight * CDisplay.ImageScale);
  if CurrentCameraState.CentreMode = tcmScreen then
    begin
    if (ImageWidth > PWidth) or (ImageHeight > PHeight) then
      begin
      CDisplay.Align := alNone;
      if (ImageWidth > PWidth) then
        begin
        CDisplay.Width := ImageWidth+1;
        CDisplay.Left := -(ImageWidth - PWidth) div 2
        end;
      if (ImageHeight > PHeight) then
        begin
        CDisplay.Height := ImageHeight+1;
        CDisplay.Top := -(ImageHeight - PHeight) div 2
        end;
      end
    else
      CDisplay.Align := alClient;
    end
  else
    begin
//    if (ImageWidth > PWidth) or (ImageHeight > PHeight) then
      begin
      CDisplay.Align := alNone;
//      if (ImageWidth > PWidth) then
        begin
        CDisplay.Width := ImageWidth+1;
        XOff := Round((Round(CurrentCameraState.XOffset)-(CurrentCameraState.PixelWidth div 2))* CDisplay.ImageScale);
        CDisplay.Left := (-(ImageWidth - PWidth) div 2)-Xoff;
        end;
//      if (ImageHeight > PHeight) then
        begin
        CDisplay.Height := ImageHeight+1;
        YOff := Round((Round(CurrentCameraState.YOffset)-(CurrentCameraState.PixelHeight div 2))*CDisplay.ImageScale);
        CDisplay.Top := (-(ImageHeight - PHeight) div 2)-Yoff;
        end;
      end
//    else
//      CDisplay.Align := alClient;
     end;
  end;
  finally
  CDisplayEndUpdate;
  end;
  except
   CDisplayEndUpdateOnException;
  end;
end;

function TCognexDisplay.NamesMatch(Name1, Name2: String;Interlocked : Boolean): Boolean;
begin
if Interlocked and (ControlAllowedAllJobs = 1) then
  begin
  Result := True;
  end
else
  Result := UpperCase(Name1) = UpperCase(Name2);
end;


procedure TCognexDisplay.JobNameChanged(Sender: Tobject; JobName: String);
begin
  SetCurrentJobName(Jobname);
end;

procedure TCognexDisplay.NewJobLoaded(Sender: Tobject; JobName: String);
begin
  SetCurrentJobName(Jobname);
end;



procedure TCognexDisplay.CognexLoggedFail(Sender: Tobject);
begin
  inc(LoginRetry);
  if LoginRetry < InitialRetryCount then
    begin
     if CameraActionHandler.InDisConRecon then
      begin
      MessPanel.Caption := 'Testing Telnet Retry';
      end
     else
      begin
      MessPanel.Caption := 'Failed To Log In to Telnet -- Retrying';
      end;

    AddDebug('Login Failed- Retrying',tvmDebug);
//    LocateMessagePanel(10,350);
    LocateMessagePanel(350);
    CognexComms.GraceFullDisconnect;
    CognexComms.ClearDown;
    CameraTelnetConnectTimeout.Enabled := False;
    TelnetConnectDelayTimer.Enabled := True;
    end
  else
    begin
    MessPanel.Caption := 'Can Not Login To Camera';
//    LocateMessagePanel(10,350);
    LocateMessagePanel(350);
    end
end;



procedure TCognexDisplay.CognexLoggedIn(Sender: Tobject);
begin
{$IFDEF HIGHDEBUG}
AddDebug('Got Logged In Response',tvmDebug);
{$ENDIF}
TelnetIndicatorPanel.LoggedInStatus := True;
if CameraActionHandler.InReconnect then
  begin
  Disconnecting := False;
  Disconnected := False;
  CameraActionHandler.EndWaitForReconnectLogin(True);
  end
else
  begin
  CameraActionHandler.EndWaitForInitialLogin(True);
  end;
end;



procedure TCognexDisplay.DoTelnetConnect(Sender: TObject);
begin
TelnetConnectRetry := 0;
TelnetConnectDelayTimer.Enabled := False;
try
if not CognexComms.ConnectToTelnet(FCameraIP,FCameraName,FCameraPortNumber,'',ReadTimerPeriod,ServiceTimerPeriod,1500,EmulatorMode) then
  begin
  if CameraActionHandler.InDisConRecon then
    begin
    MessPanel.Caption := 'Telnet Not Talking In Recon';
    end
  else
    begin
    MessPanel.Caption := 'Telnet Not Talking';
    end;
//LocateMessagePanel(10,350);
  LocateMessagePanel(350);
  end
else
  begin
  if CameraActionHandler.InDisConRecon then
    begin
    MessPanel.Caption := 'Testing Telnet';
//  LocateMessagePanel(10,350);
    LocateMessagePanel(350);
    end
  else
    begin
    MessPanel.Caption := 'Telnet Responding';
//  LocateMessagePanel(12,350);
    LocateMessagePanel(350);
    end;
  end;
except
end;
  TelnetMemo.Color := clwhite;
end;

procedure TCognexDisplay.RetryTelnetConnect(Sender: TObject);
begin
CameraTelnetConnectTimeout.Enabled := False;
inc(TelnetConnectRetry);
if  TelnetConnectRetry < 5 then
  begin
  if CameraActionHandler.InDisConRecon then
    begin
    if CognexComms.ConnectToTelnet(FCameraIP,FCameraName,FCameraPortNumber,'',ReadTimerPeriod,ServiceTimerPeriod,1500,EmulatorMode) then
      begin
      MessPanel.Caption := 'Telnet OK';
//      LocateMessagePanel(10,350);
      LocateMessagePanel(350);
      end
    else
      begin
      CameraTelnetConnectTimeout.Enabled := True;
      end;
    end
  else
    Begin
    MessPanel.Caption := Format('Retrying Camera Telnet Connection %d',[TelnetConnectRetry]);
    if CognexComms.ConnectToTelnet(FCameraIP,FCameraName,FCameraPortNumber,'',ReadTimerPeriod,ServiceTimerPeriod,1500,EmulatorMode) then
      begin
      //LocateMessagePanel(12,350);
      MessPanel.Caption := 'Telnet Responding';
  //    LocateMessagePanel(12,350);
      LocateMessagePanel(350);
      end
    else
      begin
      CameraTelnetConnectTimeout.Enabled := True;
      end;
    end
  end
 else
    begin
    CameraTelnetConnectTimeout.Enabled := False;
    MessPanel.Caption := 'Can not connect to Telnet';
    end
end;

procedure TCognexDisplay.DoMessageOff(Sender: TObject);
begin
MessPanel.Visible := False;
MessOffTimer.Enabled := False;
end;

procedure TCognexDisplay.DoMessageUpdate(Sender: TObject);
begin
//LocateMessagePanel(10,350);
LocateMessagePanel(350);
DelayLeft := DelayLeft-1;
if DelayLeft > 0 then
  begin
  case TimedMessageType of
    ttmtNone:
     begin
     end;
    ttmtSessionConnect:
     begin
     MessPanel.Caption := Format('Starting Camera Session in %d Seconds',[DelayLeft]);
     end;
    ttmtSessionEmulated:
     begin
     end;
    ttmtTelnetConnect:
     begin
     if CameraActionHandler.InDisConRecon then
       begin
       MessPanel.Caption := 'Testing Login';
       end
     else
       begin
       MessPanel.Caption := Format('Connecting to Camera Telnet in %d Seconds',[DelayLeft]);
       end;
     end;
    ttmtLogin:
     begin
     if CameraActionHandler.InDisConRecon then
       begin
       MessPanel.Caption := 'Testing Login';
       end
     else
       begin
       MessPanel.Caption := Format('Loging In To Telnet in %d Seconds',[DelayLeft]);
       end;
     end;
   end; // case
  end
else
  begin
  MessageTimer.Enabled := False;
  case TimedMessageType of
    ttmtNone:
     begin
     end;
    ttmtSessionConnect:
     begin
     MessPanel.Caption := 'Connecting To Camera Session';
     end;
    ttmtSessionEmulated:
     begin
     end;
    ttmtTelnetConnect:
     begin
     if CameraActionHandler.InDisConRecon then
      begin
      MessPanel.Caption := 'Testing Telnet';
      end
     else
      begin
      MessPanel.Caption := 'Connecting Telnet';
      end
     end;
    ttmtLogin:
     begin
     if CameraActionHandler.InDisConRecon then
      begin
      MessPanel.Caption := 'Testing Telnet';
      end
     else
      begin
      MessPanel.Caption := 'Logging In to Telnet';
      end
     end;
   end; // case
   end;
//locateMessagePanel(10,350);
LocateMessagePanel(350);
end;


procedure TCognexDisplay.DeafaultProgLoaded(Sender: Tobject; Success: Boolean);
begin
if Success then
  begin
  SetCurrentJobName(DefaultJobName);
{$IFDEF HIGHDEBUG}
  AddDebug('Loaded Default Job On Reset',tvmDebug);
{$ENDIF}
  end
else
  begin
{$IFDEF HIGHDEBUG}
  AddDebug('Could Not Load Default Job On Reset',tvmDebug);
{$ENDIF}
  SetCurrentJobName('');
  end;
 CameraActionHandler.EndWaitForDefaultLoadOnReset(Success);
end;


procedure TCognexDisplay.GetValueDone(Sender : TObject; Success : Boolean;Value : String);
begin
{if Success then
  begin
  ExternalResponseTag.AsString := Value;
  end
else
  begin
  ExternalResponseTag.AsString := '';
  end;}
 CameraActionHandler.EndWaitForGetValue(Success,Value);
end;

procedure TCognexDisplay.SetValueDone(Sender: TObject; Success: Boolean; Value: String);
begin
CameraActionHandler.EndWaitForSetValue(Success);
end;

(*procedure TCognexDisplay.DefaultProgLoadedForBuffer(Sender: Tobject; Success: Boolean);
begin
CurrentJobIsBuffered := True;
if Success then
  begin
  SetCurrentJobName(DefaultJobName);
{$IFDEF HIGHDEBUG}
  AddDebug('Loaded Default Job For Buffer',tvmDebug);
{$ENDIF}
  end
else
  begin
{$IFDEF HIGHDEBUG}
  AddDebug('Could Not Load Default Job For Buffer',tvmDebug);
{$ENDIF}
  end;
CameraActionHandler.EndWaitForXHairLoadonBuffer(Success);
end;*)

procedure TCognexDisplay.SetCurrentJobName(JobName : String);
begin
CurrentJobName := JobName;
CurrentJobNameTag.AsString := CurrentJobName;
if NamesMatch(CurrentJobName,DefaultJobName,False) then
  begin
  XHairControlsInValidTag.AsInteger := 0;
  CameraActionHandler.DefaultJobActive := True;
  end
else
  begin
  XHairControlsInValidTag.AsInteger := 1;
  CameraActionHandler.DefaultJobActive := False;
  end;
end;

{ TODO : Put disconnection and reconection into cameraactionHandler }
procedure TCognexDisplay.CheckTalkingResponse(Sender: Tobject; Success: Boolean; JobName : String;TalkMode : TTalkingModes;FIsonline :Boolean);
begin
    CameraActionHandler.EndWaitForTalking(Success,TalkMode,FIsoNline);
    if TalkMode = tmAtReconnect then
      begin
//      CurrentDisplayMode := vdmReconnecting;
      LatchTelnet := false;
      SetDisplayMode(vdmReconnecting,False,False);
      end;
end;


(*procedure TCognexDisplay.BufferRestoreDone(Sender: Tobject;Success : Boolean);
begin
//CameraActionHandler.EndWaitForResetRestoreOnBuffer(Success);
end;*)



procedure TCognexDisplay.SaveCameraState;
begin
  BufferCameraState.Scale := CurrentCameraState.Scale;
  BufferCameraState.FitMode := CurrentCameraState.FitMode;
  BufferCameraState.CentreMode := CurrentCameraState.CentreMode;
  BufferCameraState.CentreModeInteger := CurrentCameraState.CentreModeInteger;
  BufferCameraState.CircleRadius := CurrentCameraState.CircleRadius;
  BufferCameraState.XOffset  := CurrentCameraState.XOffset;
  BufferCameraState.YOffset  := CurrentCameraState.YOffset;
end;


procedure TCognexDisplay.NCReady(Sender: TObject);
begin
VisionCanTalkTag.AsInteger := 1;
end;

procedure TCognexDisplay.NCCommandComplete(Success: Boolean;
                                Command : TCocgnexInputMessage;
                                CommandType : TNCCommands;
                                InternalResponse : String;
                                ExternalResponse : String;
                                ResponseState : Integer;
                                ErrMess : String);


begin
VisionResponseTag.AsString := InternalResponse;
if ExternalResponse <> '' then ExternalResponseTag.AsString := ExternalResponse;
{$IFDEF HIGHDEBUG}
AddDebug(Format('CommandStatus %s %s',[InternalResponse,CognexComms.LastCommandStr]),tvmDebug);
{$ENDIF}
if ResponseState = 0 then
  begin
  AddDebug('Invalid Response State',tvmdebug);
  end;
SetCommandStataus(ResponseState);
VisionErrorMessTag.AsString := ErrMess;

If assigned(Command) then
  begin
  if (not Success) then
    begin
  {$IFDEF HIGHDEBUG}
    AddDebug(Format('Adding error message %s,%s,%s',[Command.CommandStr,InternalResponse,Command.ErrorString]),tvmDebug);
    CognexComms.SetInLineError(InternalResponse,Command.ErrorString);
  {$ENDIF}
    ErrorDisplay.ErrorStack.aAddEntry(Command,InternalResponse);
    InLineErrorOccuredDuringJob := True;
    end;
  end
else
  begin
  if (not Success) then
    begin
   {$IFDEF HIGHDEBUG}
    AddDebug(Format('Adding error message %s,%s,%s',['ftpCommand',InternalResponse,Command.ErrorString]),tvmDebug);
    CognexComms.SetInLineError(InternalResponse,Command.ErrorString);
    ErrorDisplay.ErrorStack.aAddEntry(Command,InternalResponse);
    InLineErrorOccuredDuringJob := True;
   {$ENDIF}
    end;
  end;

case CommandType of

tncOffLineForImport:
  begin
    CameraActionHandler.EndWaitForOffLineForFileImport(Success);
  end;

tncSetOfflineOnReset,tncDoneInitialOnLineConditioning:
  begin
  if CognexComms.IsOnLine then
    begin
    CameraActionHandler.EndWaitForOfflineOnReset(False);
    end
  else
    begin
    CameraActionHandler.EndWaitForOfflineOnReset(True);
    end;
  end;

  tncGetSymbolicValue:
    begin
    //CameraActionHandler.EndWaitForGetValue(True)
    AddDebug('Got Symbolic Value',tvmDebug);
    end;

  tncSetJobScaling:
  begin
  AddDebug('Updating Scaling in File',tvmDebug);
  CameraActionHandler.UpdateCalibration := True;
  CameraActionHandler.KickLazyUpdate;

  //LazyUpdateTimer.Enabled := True;
  end;

  tncSetJobYOffset:
  begin
  AddDebug('Updating Offset in File',tvmDebug);
  CameraActionHandler.UpdateCalibration := True;
  CameraActionHandler.KickLazyUpdate;
  //LazyUpdateTimer.Enabled := True;
  end;

  tncGoLiveOnReset:
  begin
  if InternalResponse = '1' then
    begin
    CameraActionHandler.EndWaitForGoLiveOnreset(True);
    end
  else
    begin
    CameraActionHandler.EndWaitForGoLiveOnreset(False);
    end;
  if ShowCameraOnReset then SetDisplayMode(vdmCamera,True,False);
  end
end ; //case

If PerformingBufferedCommand then
  begin
  PerformingBufferedCommand := False;
  CommandIsBuffered := False;
//  CameraActionHandler.ImmediateCommandPending := False;
  AddDebug('Completed Buffered Command',tvmDebug);
  end;
end;


procedure TCognexDisplay.AddDebug(DString: String;LowestMode :TVModes);
begin
TelnetMemo.RollingBuffer.AddDebugMessage(DString);
if cardinal(CognexComms.VerboseMode) >= cardinal(LowestMode) then
  begin
  TelnetMemo.AddDebugMessage(DString);
  end
end;

{procedure TCognexDisplay.ClearResetting(Sender: TObject);
begin
 ResetStuckTimer.Enabled := False;
 AddDebug('ClearingResetInProgress',tvmDebug);
 CognexComms.ClearDown;
 ResetInProgress := False;
 ResetStage := 0;
end;}

//TIdleStatusChangeEvent = procedure(Sender : Tobject;Status : Boolean;var InLineErrorStatus : String;var InLineErrorMessage : String) of object;
procedure TCognexDisplay.CheckForIdleActions(Sender: Tobject;IsIdle : Boolean;var InLineErrStat: String;var InLineErrMess : String);
begin
//TestIndicator.OnStatus := True;
TelnetIndicatorPanel.BusyStatus := not IsIdle;
if not IsIdle then
  begin
  VisionIdleTag.AsInteger := 0;
  TelnetMemo.SystemIdleState := False;
  AddDebug('^^ Vision Not Idle',tvmDebug)
  end
else
  begin
  if InLineErrStat <> '' then
    begin
    InLineErrStat := '';
    SetCommandStataus(-3);
    VisionErrorMessTag.AsString := InLineErrMess;
    InLineErrMess :='';
    end;
  VisionIdleTag.AsInteger := 1;
  TelnetMemo.SystemIdleState := True;
  AddDebug('^^ Vision Is Idle',tvmDebug)
  end;
end;

procedure TCognexDisplay.TelnetHealthyChange(Sender : Tobject;IsHealthy : Boolean);
begin
TelnetIndicatorPanel.HealthyStatus := IsHealthy;
end;


procedure TCognexDisplay.OnLineChanged(Sender: Tobject; IsOnline: Boolean);
begin
TelnetIndicatorPanel.OnLineStatus := IsOnline;
if IsOnline then
   OnLineTag.AsInteger := 1
else
  begin
   OnLineTag.AsInteger := 0;
   if CameraActionHandler.InDisconnect then
     begin
     CameraActionHandler.EndWaitForOfflineOnDisconnect;
     end
  end
end;


procedure TCognexDisplay.TelnetButtonClick(Sender: TObject);
begin
if Sender = TelnetCameraButton then
  begin
  if not TelnetCameraVisible then
    begin
    SetDisplayMode(vdmTelnetAndCamera,False,False);
    TelnetCameraButton.Caption := 'Hide Camera';
    end
  else
    begin
    SetDisplayMode(vdmTelnet,False,False);
    TelnetCameraButton.Caption := 'Show Camera';
    end;
  end else
if Sender = TelnetDebugButton then
  begin
  if CognexComms.VerboseMode = tvmDebug then
    begin
    TelnetDebugButton.Font.Color := clRed;
    CognexComms.VerboseMode := tvmSuperDebug
    end
  else
    begin
    TelnetDebugButton.Font.Color := clBlack;
    CognexComms.VerboseMode := tvmDebug;
    end;
  TelnetMemo.RefreshFromRollingBuffer;
  end else

if Sender = TelnetDuplexButton then
  begin
    TelnetDebugButton.Font.Color := clBlack;
    CognexComms.VerboseMode := tvmDuplex;
  end else
if Sender = TelnetStageButton then
  begin
   TelnetDebugButton.Font.Color := clBlack;
   CognexComms.VerboseMode := tvActionStageOnly;
  end;
if Sender = TelnetNoneButton then
  begin
   TelnetDebugButton.Font.Color := clBlack;
   CognexComms.VerboseMode := tvmOff;
  end;
end;

procedure TCognexDisplay.LocateMessagePanel(FWidth: Integer);
begin
//cateMessagePanel(12,FWidth);
LocateMessagePanel(10,FWidth);
end;

procedure TCognexDisplay.LocateMessagePanel(FSize : Integer;FWidth : Integer);
var
PWidth : Integer;
NoFit : Boolean;
Retry    : Integer;
TextW : Integer;
TextWFloat : Single;
TempBox : TPaintBox;
TestString : String;
begin
if FWidth = 0 then PWidth := 350 else PWidth := FWidth;
TestString := MessPanel.Caption;

if FirstShow then
  begin
  FirstShow := False ;
  MessPanel.Left := 143;
  MessPanel.Top  := 108;
  end
else
  begin
  MessPanel.Left := (ClientWidth-PWidth) div 2;
  MessPanel.Top  := (ClientHeight- MessPanel.Height) div 4;
  end;
MessPanel.Font.Size := FSize;
MessPanel.Font.Color := clBlack;
MessPanel.Color := clSilver;
NoFit := True;
Retry := 0;
  begin
    TempBox := TPaintBox.Create(nil);
    try
    MessPanel.Visible := False;
    TempBox.Parent := MessPanel;
    TempBox.Align := alClient;
    TempBox.Canvas.Font.Size := MessPanel.Font.Size;
    TempBox.Canvas.Font.Name := MessPanel.Font.Name;
    while NoFit = True do
    begin
    try
    try  // +2 for except
    TextWFloat := TempBox.Canvas.TextWidth(TestString)*1.1;
    TextW := Round(TextWFloat);
    If TextW > (Pwidth-10) then
      begin
      if TempBox.Canvas.Font.Size >= 7 then
        begin
        TempBox.Canvas.Font.Size := TempBox.Canvas.Font.Size-1;
        end
      else
        begin
        NoFit := False;
        MessPanel.Font.Size := 6;
        end
      end
    else
      begin
      NoFit := False;
      MessPanel.Font.Size := TempBox.Canvas.Font.Size;
      end;
    except //*2
    inc(retry);
    NoFit := False;
    end;
    finally
    inc(Retry);
    end;
    if Retry > 10 then
      begin
      NoFit := False
      end;
    end;
    finally
    TempBox.Free;
    MessPanel.Width := PWidth;
    end;
  end;

MessPanel.Visible := True;
MessPanel.BringToFront;
MessPanel.Invalidate;
end;

// Event of InitialCameraFTPConnectDelay Timer out
procedure TCognexDisplay.StartConnectingFTP(Sender: TObject);
var
AlreadyExists : Boolean;
begin
  FTPConnectRetry := 0;
//  InitialCameraTelnetConnectDelay.Enabled := False;
//  TelnetConnectionChanged.Enabled := False;
  AlreadyExists := False;
  if FirstConnectSweep and UseFTPServer then
    begin
    Windows.CreateMutex(nil, false, 'CognexFTPServerMutex');
    if GetLastError = ERROR_ALREADY_EXISTS then
      begin
      AlreadyExists := True;
      end;
    end;

//!!!!!!!!!!!!!!  CameraConnectMode := tccmInitialConnect;
//!!!!!!!!!!!!!!  CameraConnectTimeout.Enabled := True;

  if not(AlreadyExists) and(FirstConnectSweep) and UseFTPServer then
    begin
    // here To launch FTPServer App and allow 2 seconds before connecting
    FirstConnectSweep := False;
    LaunchTag.AsInteger := 1;
    LaunchTag.AsInteger := 0;
//    InitialCameraTelnetConnectDelay.Interval := 2000;
//    InitialCameraTelnetConnectDelay.Enabled := True;
    end
  else
    begin
    // Here if either not using FTPServer or Using it and already exists
    if NoCognex = False then
      begin
      if USeFTPServer then
        begin
        CameraActionHandler.RequestFTPInitialConnectandLogin;
        end
      else
        begin
     //   CameraActionHandler.EndWaitForInitialJobList(True);
        end;
      InstallDone := True;
      CDisplayBeginUpdate;
      try
      try
      CDisplay.Align := alClient;
      finally
      CDisplayEndUpdate;
      end;
      except
       CDisplayEndUpdateOnException;
      end;
      MessPanel.Visible := True;
      MessPanel.BringToFront;
      if EmulatorMode then
        begin
        MessPanel.Caption := 'Initialising Video (Emulator Mode)';
//      LocateMessagePanel(10,350);
        LocateMessagePanel(350);
        end
      else
        begin
        MessPanel.Caption := 'Initialising Video';
        LocateMessagePanel(12,350);
        end;
      AddDebug(Format('Retry = %d',[FTPConnectRetry]),tvmDebug);

      TelnetSendEdit.ClearDownTo('GF',10);
      TelnetSendEdit.RegisterText('Get FileList');
      end;
   end;
end;



procedure TCognexDisplay.NoCameraSessionConnect(Sender: TObject);
begin
AddDebug('Timed Out On Connection',tvmDebug);
CameraSessionConnectTimeout.Enabled := False;
Case CameraConnectMode of
  tccmNone:
  begin
  end;

  tccmRetryConnect:
  begin
  DoCameraRetryConnect;
  end;

  tccmReconnectConnect:
  begin
  NoCameraFaultOnReconnect
  end;

  tccmUnsolicitedReconect:
  begin
  UnsolicitedReconnect;
  end;

  tccmOPeratorDisconnect:
  begin
  UnsolicitedReconnect;
  end;

end; // case
end;


procedure TCognexDisplay.NoCameraFaultUnsolicited;
begin
//CameraActionHandler.endwaitfor
end;

procedure TCognexDisplay.DoCameraSessionConnect(Sender : Tobject);
var
LC : Integer;
begin
// Here for Initial Session Connect after start delay
  InitialSessionConnectDelayTimer.Enabled := False;
  CameraConnectMode := tccmRetryConnect;
  begin
  AddDebug(Format('Initial Camera Session Connect ',[SessionConnectRetry]),tvmDebug);
  if NoCognex or EmulatorMode then  exit;
  CDisplayBeginUpdate;
  try
  try
//  CDisplay.Disconnect;
  StartConnectTime := Now;
  AddDebug(Format('Sending Connect Message %s',[ConnectString]),tvmDebug);
  CameraActionHandler.ForceDisconRecon := True;
  CDisplay.AutoConnectString := ConnectString;
  finally
  CDisplayEndUpdate;
  end;
  except
   CDisplayEndUpdateOnException;
  end;

  MessPanel.Caption := 'Starting Camera Session';
//LocateMessagePanel(10,350);
  LocateMessagePanel(350);
  MessPanel.Font.Color := clRed;
  CameraSessionConnectTimeout.Enabled := True;
  end
end;


procedure TCognexDisplay.DoCameraRetryConnect;
begin
// Here for failure to responde to IP and Username message
inc(SessionConnectRetry);
if SessionConnectRetry < SessionConnectRetries then
  begin
//  MakeCDisplay(False); // Destroy and remake Cdisplay
  AddDebug(Format('Retrying Camera Connect %d',[SessionConnectRetry]),tvmDebug);
  AddDebug(Format('Sending Connect Message %s',[ConnectString]),tvmDebug);
  CDisplayBeginUpdate;
  try
  try
  CDisplay.Disconnect;
  StartConnectTime := Now;
  CDisplay.Refresh;
  CDisplay.AutoConnectString := ConnectString;
  finally
  CDisplayEndUpdate;
  end;
  except
   CDisplayEndUpdateOnException;
  end;
  MessPanel.Caption := Format('No Camera Detected. Retrying Connection: %d',[SessionConnectRetry]);
  CameraActionHandler.ForceDisconRecon := True;
//LocateMessagePanel(10,350);
  LocateMessagePanel(350);
  MessPanel.Font.Color := clRed;
  CameraSessionConnectTimeout.Enabled := True;
  end
else
  begin
  try
  try
  CDisplay.Disconnect;
  finally
  CDisplayEndUpdate;
  end;
  except
   CDisplayEndUpdateOnException;
  end;
  CameraSessionConnected(False);
  AddDebug(Format('No Camera Detected after %d attempts',[SessionConnectRetry]),tvmDebug);
  MessPanel.Caption := Format('No Camera Detected after %d attempts',[SessionConnectRetry]);
  SetNoCognexMode(True);
//LocateMessagePanel(10,350);
  LocateMessagePanel(350);
  MessPanel.Font.Color := clRed;
  CameraConnectMode := tccmFailed;
  end
end;

procedure TCognexDisplay.UnsolicitedDisconnectReConnectOnReset;
begin
  if IsClosing Then Exit;
  SessionConnectRetry := 0;
  if NoCognex or EmulatorMode then exit;
  AddDebug('*********************',tvmDebug);
  AddDebug('*********************',tvmDebug);
  AddDebug('*********************',tvmDebug);
  AddDebug('Reconnecting On Reset',tvmDebug);
  AddDebug('Reconnecting On Reset',tvmDebug);
  AddDebug('*********************',tvmDebug);
  AddDebug('*********************',tvmDebug);
  AddDebug('*********************',tvmDebug);
 {$IFDEF HIGHDEBUG}
  TelnetMemo.AddDebugMessage(Format('Connection Retry to IP %s',[FCameraIP]));
 {$ENDIF}
  CDisplayBeginUpdate;
  try
  try
  CDisplay.Disconnect;
  StartConnectTime := Now;
  Cdisplay.AutoConnectString := ConnectString;
  finally
  CDisplayEndUpdate;
  end;
  except
   CDisplayEndUpdateOnException;
  end;
  MessPanel.Caption := 'Retrying Re-Connect On Reset';
//  LocateMessagePanel(10,350);
  LocateMessagePanel(350);
  CameraSessionConnectTimeout.Enabled := True;
end;

procedure TCognexDisplay.UnsolicitedReconnect;
begin
if IsClosing Then Exit;
AddDebug('Retrying Reset Reconnect',tvmDebug);
AddDebug(Format('Retry = %d',[SessionConnectRetry]),tvmDebug);

if NoCognex or EmulatorMode then
   exit;
if SessionConnectRetry < 5 then
  begin
  CDisplayBeginUpdate;
  try
  try
  CDisplay.Disconnect;
  StartConnectTime := Now;
  AddDebug('Unsolicited Reconnect',tvmDebug);
  AddDebug(Format('Sending Connect Message %s Retry %d',[ConnectString,SessionConnectRetry]),tvmDebug);
  Cdisplay.AutoConnectString := ConnectString;
  finally
  CDisplayEndUpdate;
  end;
  except
   CDisplayEndUpdateOnException;
  end;
  MessPanel.Caption := Format('Retrying Re-Connect: %d',[SessionConnectRetry]);
  AddDebug('No Camera Fault On Reset',tvmDebug);
//  LocateMessagePanel(10,350);
  LocateMessagePanel(350);
  CameraSessionConnectTimeout.Enabled := True;
  end
else
  begin
  CameraConnectMode := tccmUnsolicitedDisconect;
  end
end;


procedure TCognexDisplay.NoCameraFaultOnReconnect;
begin
if IsClosing Then Exit;
AddDebug('No Camera Fault InitialConnectTimeout.Enabled := False',tvmDebug);
AddDebug(Format('Retry = %d',[SessionConnectRetry]),tvmDebug);
if NoCognex or EmulatorMode then
   begin
   AddDebug('No Cognex.... exiting NoCameraFaultOnReconnect',tvmDebug);
   exit;
   end;
if SessionConnectRetry < 5 then
  begin
  inc(SessionConnectRetry);
 {$IFDEF HIGHDEBUG}
  TelnetMemo.AddDebugMessage(Format('Connection Retry to IP %s',[FCameraIP]));
 {$ENDIF}
  CDisplayBeginUpdate;
  try
  try
  CDisplay.Disconnect;
  StartConnectTime := Now;
  AddDebug(Format('Sending Connect Message %s Retry %d',[ConnectString,SessionConnectRetry]),tvmDebug);
  Cdisplay.AutoConnectString := ConnectString;
  finally
  CDisplayEndUpdate;
  end;
  except
   CDisplayEndUpdateOnException;
  end;
  MessPanel.Caption := Format('No Camera Detected. Retrying Re-Connect: %d',[SessionConnectRetry]);
  AddDebug('No Camera Fault On Reconect',tvmDebug);
  LocateMessagePanel(350);
  CameraSessionConnectTimeout.Enabled := True;
  end
else
  begin
  SessionConnectRetry := 0;
  CameraSessionConnected(False);
  //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!123!!!!
    AddDebug('Would have gone to error Display Here',tvmDebug);
//  SetDisplayMode(vdmError,False,False);
//  CDisplayBeginUpdate;
//  CameraConnectMode := tccmFailed;
//  try
//  try
//  CDisplay.Visible := False;
//  finally
//  CDisplayEndUpdate;
//  end;
//  except
//   CDisplayEndUpdateOnException;
//  end;
  MessPanel.Caption := 'No Camera Detected';
  MessPanel.Font.Size := 16;
  LocateMessagePanel(350);
  MessPanel.Font.Color := clRed;
  end;
end;

procedure TCognexDisplay.SaveToFile(Sender: TObject);
var
FPath : String;
FName : String;
begin
 FPath := LocalPath+'ScriptOut\';
 try
 FName := Format('%STelnetLog_%d.txt',[FPath,DateTimeToFileDate(Now)]);
 TelnetMemo.Lines.SaveToFile(FName);
 except
 Color := clred;
 end;
end;


procedure TCognexDisplay.RetryConnection(Sender: TObject);
var
Buffer : String;
begin
  if NoCognex then exit;
  ResetInProgress := False;
  ReconnectButton.Enabled := False;
  Buffer := Format('%s,%s',[FCameraIP,FCameraName]);
  try
  MessPanel.Caption := 'Trying to Re-connect';
//MessPanel.Font.Color := clBlack;
//  LocateMessagePanel(10,350);
  LocateMessagePanel(350);
  CDisplayBeginUpdate;
  try
  try
  CDisplay.Disconnect;
  StartConnectTime := Now;
  CDisplay.AutoConnectString := Buffer;
  finally
  CDisplayEndUpdate;
  end;
  except
   CDisplayEndUpdateOnException;
  end;

  MessPanel.Caption := 'Trying to Re-connect';
//MessPanel.Font.Color := clBlack;
//  LocateMessagePanel(12,350);
  LocateMessagePanel(350);
  except
  ReconnectButton.Enabled := False;
  MessPanel.Caption := 'Reconnect Failed';
//  LocateMessagePanel(10,350);
  LocateMessagePanel(350);
  MessPanel.Font.Color := clRed;
  Self.Color := clRed;
  end;

  CDisplayBeginUpdate;
  try
  try
  CDisplay.ImageScale := 0.37;
  finally
  CDisplayEndUpdate;
  end;
  except
   CDisplayEndUpdateOnException;
  end;
  AddDebug('Retry Connection No Camera Fault ConnectTimeout.Enabled := True',tvmDebug);
  CameraSessionConnectTimeout.Enabled := True;

end;

procedure TCognexDisplay.HandleTelnetBroken(Sender: Tobject);
begin
if not FNoCognexMode then
    begin
    MessPanel.Caption := 'Unexpected Camera Disconected.';
//    LocateMessagePanel(10,500);
    LocateMessagePanel(350);
    CameraConnectMode := tccmOPeratorDisconnect;
    CameraSessionConnected(False); //!!!!!!!!!
    AddDebug('Cleaning Down on Disconnect',tvmDebug);
    CameraActionHandler.CogNexCommunicator.ClearDown;
     AddDebug('NoCognexMode *********************',tvmDebug);
     AddDebug('NoCognexMode *********************',tvmDebug);
     AddDebug('NoCognexMode *********************',tvmDebug);
     AddDebug('NoCognexMode *********************',tvmDebug);
     AddDebug('NoCognexMode *********************',tvmDebug);
     AddDebug('NoCognexMode *********************',tvmDebug);
     SetNoCognexMode(True);
     end;
end;

procedure TCognexDisplay.ReconnectRequested(ATag: TAbstractTag);
var
TagVal : INteger;
begin
  TagVal := ATag.AsInteger;
  if (TagVal = 0)  then
    begin
    if FNoCognexMode then
      begin
      CameraActionHandler.RequestReconnect(True,True);
      AddDebug('Reconnect in No Cognex Mode',tvmDebug);
      end
    else
      begin
      CameraActionHandler.RequestReconnect(False,True);
      AddDebug('Reconnect in Std Mode',tvmDebug);
      end ;

    FirstReset := True;
    end
end;

procedure TCognexDisplay.ProgramChanged(ATag: TAbstractTag);
var
NewProg : String;
begin
NewProg := ATag.AsString;
if NewProg <> LastProgramName then
  begin
  LastProgramName := NewProg;
  if assigned(ErrorDisplay) then
     begin
     ErrorDisplay.ClearErrors;
     end;

  end;
end;


procedure TCognexDisplay.TakeSnapshotWG(ATag: TAbstractTag);
var
TagVal : INteger;
Path : String;
begin
  TagVal := ATag.AsInteger;
  if (TagVal = 0)  then
    begin
    if DirectoryExists(SnapShotDirectory) then
      begin
      Path := Format('%s%s',[SnapShotDirectory,MakeTimeStampedSnapName]);
      TakeSnapShotTo(Path,True);
      end
    else
      begin
      end;
    end;
end;

procedure TCognexDisplay.TakeSnapshotNG(ATag: TAbstractTag);
var
TagVal : INteger;
Path : String;
begin
  TagVal := ATag.AsInteger;
  if (TagVal = 0)  then
    begin
    if DirectoryExists(SnapShotDirectory) then
      begin
      Path := Format('%s%s',[SnapShotDirectory,MakeTimeStampedSnapName]);
      TakeSnapShotTo(Path,False);
      end
    else
      begin
      end;
    end;
end;


procedure TCognexDisplay.DisconnectRequested(ATag: TAbstractTag);
var
TagVal : INteger;
begin
  begin
  TagVal := ATag.AsInteger;
  if (TagVal = 0)  then
    begin
    if InCycle then
       begin
       TimedMessage('Can Not Disconnect in cycle',2000);
       end
    else
       begin
       SetDisplayMode(vdmDisconnecting,False,False);
       CameraActionHandler.RequestDisconnect(False);
       end;
    end;
  end;
end;



procedure TCognexDisplay.TelnetConnectionChanged(Sender: Tobject;Connected : Boolean);
begin
 //TelnetIndicatorPanel.ConnectedStatus := Connected;
 if not Connected Then
   begin
   TelnetIndicatorPanel.LoggedInStatus := False;
   TelnetIndicatorPanel.HealthyStatus := False;
   TelnetIndicatorPanel.BusyStatus := False;
   if CameraActionHandler.InDisconnect then
      begin
      CameraActionHandler.EndWaitForTelnetDisconnect(True);
     end;
//   ChangeConnectionStage := dsCameraDisconect;
//   DisconnectTimer.Enabled := True;
   end
 else
  begin
  CameraTelnetConnectTimeout.Enabled := False;
  AddDebug('Connected ReConnectTimeout.Enabled := False',tvmDebug);
  end;

end;


procedure TCognexDisplay.TelnetCommandEntered(Sender: Tobject; TelnetText: String);
begin
if InCycle then exit;
If TelnetText <> '' then
  begin
  if CognexComms.IsConnected or EmulatorMode then
    begin
    CognexComms.SendNCCommand(TelnetSendEdit.Text,tncFromForm,'run manually entered native command',True);
    end
  else
    begin
    AddDebug('Not Connected To Telnet',tvmDebug);
    end;
  end;
end;


procedure TCognexDisplay.CdisplayStatusChange(Sender: TObject);
var
Status : WIdestring;
begin
  CDisplayBeginUpdate;
  try
  try
  Status := CDisplay.StatusInformation;
  finally
  CDisplayEndUpdate;
  end;
  except
   CDisplayEndUpdateOnException;
  end;
MessPanel.Caption := Status;
//LocateMessagePanel(10,350);
LocateMessagePanel(350);
end;



 //Telnet Response Callbacks

procedure TCognexDisplay.ResetRestoreDone(Sender: Tobject; Success: Boolean);
begin
CameraActionHandler.EndWaitForResetRestore(Success);
end;

procedure TCognexDisplay.ResetFitComplete(Sender: Tobject;Success : Boolean);
begin
AddDebug('ResetFitComplete Callback',tvmDebug);
CameraActionHandler.EndWaitForResetFit(Success);
end;

procedure TCognexDisplay.CheckFitActions(Sender: TOBject);
begin
If GoVisibleOnFit then
  begin
  LeftDisplayPanel.Visible := True;
  TelnetLowerPanel.Visible := False;
  TelnetMemo.Visible := False;
  CDisplayBeginUpdate;
  try
  try
  CDisplay.Visible := True;
  finally
  CDisplayEndUpdate;
  end;
  except
   CDisplayEndUpdateOnException;
  end;
  Image.Visible := False;
  GoVisibleOnFit := False;
  end;
end;

procedure TCognexDisplay.DisconnectDone(Sender: TOBject);
begin
CameraSessionConnected(False);
CameraConnectMode := tccmOPeratorDisconnect;
SetNoCognexMode(True);
end;


procedure TCognexDisplay.ReconnectStarted(Sender: TOBject);
begin
  CameraActionHandler.RestoreValid := False;
  ResetActionEnable := False;
  MessPanel.Visible := False;
  MessPanel.Caption := 'Camera Reconnection Started';
//  LocateMessagePanel(10,350);
  LocateMessagePanel(350);
end;

procedure TCognexDisplay.CameraReconnectComplete(Sender: TOBject);
begin
  AddDebug('NoCognexMode := False',tvmDebug);
  CameraActionHandler.RestoreValid := True;
  ResetActionEnable := True;
  SetNoCognexMode(False);
  MessPanel.Visible := False;
  TimedMessage('Camera Reconnection Complete',2000)
//  MessPanel.Caption := 'Camera Reconnection Complete';
//  LocateMessagePanel(350);
end;

procedure TCognexDisplay.CameraResetFail(Sender: TOBject);
begin
AddDebug('**************************',tvmDebug);
AddDebug('Responding to reset Fail ',tvmDebug);
AddDebug('**************************',tvmDebug);
CameraGFLocked(nil);
end;


procedure TCognexDisplay.CameraResetComplete(Sender: TOBject);
begin
  MessPanel.Visible := False;
  VisionCanTalkTag.AsInteger := 1;
  CognexComms.PostCommandsPending := False;
  CognexComms.ExternalLockBusy := False;
  CognexComms.FastRunMode := False;
  ResetActionEnable := True;
  CameraActionHandler.RestoreValid := True;
  SetNoCognexMode(False);
If GoVisibleOnFit then
  begin
  TelnetLowerPanel.Visible := False;
  TelnetMemo.Visible := False;
  CDisplayBeginUpdate;
  try
  try
  CDisplay.Visible := True;
  finally
  CDisplayEndUpdate;
  end;
  except
   CDisplayEndUpdateOnException;
  end;
  GoVisibleOnFit := False;
  end;
If FirstReset then
  begin
  FirstReset := False;
  InternalSetExposure;
  end;
end;

procedure TCognexDisplay.JobRunComplete(Sender: Tobject; Success: Boolean);
begin
CameraActionHandler.EndWaitForJobRunComplete(Success);
end;

procedure TCognexDisplay.FastJobRunDone(Sender: Tobject; Success: Boolean);
begin
CameraActionHandler.EndWaitForFastJobRunComplete(Success);
end;

procedure TCognexDisplay.UserExposureChanged(Sender: Tobject; Success: Boolean; ExposureVal: Double);
begin
if Success then
  begin
  CurrentExposureTag.AsDouble := ExposureVal;
  end
else
  begin
  CurrentExposureTag.AsDouble := 0;
  end;
end;

procedure TCognexDisplay.JobPassChecked(Success, Passed: Boolean);
begin
if Passed then
  begin
  if InLineErrorOccuredDuringJob then
    begin
    AddDebug('Setting Pass Tag False Due to Inline error',tvmDebug);
    JobInlineErrorTag.AsInteger := 1;
    JobPassedTag.AsInteger := 0;
    end
  else
    begin
    JobInlineErrorTag.AsInteger := 0;
    AddDebug('Setting Pass Tag',tvmDebug);
    JobPassedTag.AsInteger := 1;
    end;
  end
else
  begin
  AddDebug('Clearing Pass Tag',tvmDebug);
  JobPassedTag.AsInteger := 0;
  if InLineErrorOccuredDuringJob then
    begin
    JobInlineErrorTag.AsInteger := 1;
    end
  else
    JobInlineErrorTag.AsInteger := 0
  end;

CameraActionHandler.EndWaitForJobPassCheck(Success,Passed);
end;


procedure TCognexDisplay.CognexJobLoaded(Sender: Tobject; Success: Boolean);
begin
  CameraActionHandler.EndwaitForJobLoad(Success);
  MessPanel.Visible := False;
end;

procedure TCognexDisplay.InitilaResetComplete(Sender: TOBject);
begin
  MessPanel.Visible := False;
  VisionCanTalkTag.AsInteger := 1;
  CognexComms.ExternalLockBusy := False;
  SetDisplayMode(vdmCamera,False,False);
end;

procedure TCognexDisplay.ShowBannerMessage(Sender: TOBject; MessText: String);
begin
if MessText <> '' then
  begin
  MessPanel.Visible := True;
  MessPanel.BringToFront;
  MessPanel.Caption := MessText;
//  LocateMessagePanel(10,350);
  LocateMessagePanel(350);
  end
else
  begin
  MessPanel.Visible := False;
  MessPanel.Caption := '';
  end;
end;

procedure TCognexDisplay.TimedMessage(MessText: String; Interval: Integer);
begin
MessOffTimer.Interval := Interval;
MessPanel.Caption := MessText;
MessPanel.Visible := True;
MessPanel.BringToFront;
MessOffTimer.enabled := True;
end;


//*************************************************
// Crosshair Control
//*************************************************

procedure TCognexDisplay.DoXReset(ATag: TAbstractTag);
var
TagVal : Integer;
begin
TagVal := ATag.AsInteger;
if TagVal = 0 then
  begin
  if CanTalk then
    begin
    if NamesMatch(CurrentJobName,DefaultJobName,True) then
      begin
      CurrentCameraState.XOffset := CurrentCameraState.PixelWidth div 2;
      CurrentCameraState.YOffset := CurrentCameraState.PixelHeight div 2;
      CognexComms.SendServiceCommand(Format('SFCalData.XOffset %7.3f',[CurrentCameraState.XOffset]));
      CognexComms.SendServiceCommand(Format('SFCalData.YOffset %7.3f',[CurrentCameraState.YOffset]));
      CurrentCameraState.OffsetIsManual := 1;
      CameraActionHandler.DoCalibrationUpdate;
      end;
    end;
    if CurrentCameraState.CentreMode = tcmXHair then
      begin
      CDisplayBeginUpdate;
      try
      try
      SceneScaleTag.AsDouble := CDisplay.ImageScale;
      finally
      CDisplayEndUpdate;
      end;
      except
       CDisplayEndUpdateOnException;
      end;
      CameraActionHandler.DefaultJobImageScaleChanged(nil);
      end;
  end;
end;

procedure TCognexDisplay.DoJog(Sender: TObject);
begin
case VisionJogMode of
  jmNone:
    begin
    JogTimer.Enabled := False;
    CameraActionHandler.KickLazyUpdate;
    end;

  jmHairLeft:
    begin
    CurrentCameraState.XOffset := CurrentCameraState.XOffset-XHairIncrement;
    CurrentCameraState.OffsetIsManual := 1;
    CognexComms.SendServiceCommand(Format('SFCalData.XOffset %7.3f',[CurrentCameraState.XOffset]));
    XoffsetTag.AsDouble := CurrentCameraState.XOffset;
    CameraActionHandler.DoCalibrationUpdate;
    end;

  jmHairRight:
    begin
    CurrentCameraState.XOffset := CurrentCameraState.XOffset+XHairIncrement;
    CurrentCameraState.OffsetIsManual := 1;
    CognexComms.SendServiceCommand(Format('SFCalData.XOffset %7.3f',[CurrentCameraState.XOffset]));
    XoffsetTag.AsDouble := CurrentCameraState.XOffset;
    CameraActionHandler.DoCalibrationUpdate;
    end;

  jmHairUp:
    begin
    CurrentCameraState.YOffset := CurrentCameraState.YOffset-XHairIncrement;
    CurrentCameraState.OffsetIsManual := 1;
    CognexComms.SendServiceCommand(Format('SFCalData.YOffset %7.3f',[CurrentCameraState.YOffset]));
    YoffsetTag.AsDouble := CurrentCameraState.YOffset;
    CameraActionHandler.DoCalibrationUpdate;
    end;

  jmHairDown:
    begin
    CurrentCameraState.YOffset := CurrentCameraState.YOffset+XHairIncrement;
    CurrentCameraState.OffsetIsManual := 1;
    CognexComms.SendServiceCommand(Format('SFCalData.YOffset %7.3f',[CurrentCameraState.YOffset]));
    YoffsetTag.AsDouble := CurrentCameraState.YOffset;
    CameraActionHandler.DoCalibrationUpdate;
    end;

  jmCircleUp:
    begin
    CurrentCameraState.CircleRadius := CurrentCameraState.CircleRadius+CircleIncrement;
    if CurrentCameraState.CircleRadius >400 then CurrentCameraState.CircleRadius := 400;
    CognexComms.SendServiceCommand(Format('SFCircleRadius.Value %7.3f',[CurrentCameraState.CircleRadius]));
    end;

  jmCircleDown:
    begin
    CurrentCameraState.CircleRadius := CurrentCameraState.CircleRadius-CircleIncrement;
    if CurrentCameraState.CircleRadius < 0 then CurrentCameraState.CircleRadius := 0;
    CognexComms.SendServiceCommand(Format('SFCircleRadius.Value %7.3f',[CurrentCameraState.CircleRadius]));
    end;

  jmScaleUp:
    begin
    CDisplayBeginUpdate;
    try
    try
    CameraActionHandler.CurrentScale := CDisplay.ImageScale+ScaleIncrement;
    if CameraActionHandler.CurrentScale > 4.0  then CameraActionHandler.CurrentScale := 4.00;
    CDisplay.ImageScale := CameraActionHandler.CurrentScale;
    finally
    CDisplayEndUpdate;
    end;
    except
     CDisplayEndUpdateOnException;
    end;
    end;

  jmScaleDown:
    begin
    CDisplayBeginUpdate;
    try
    try
    CameraActionHandler.CurrentScale := CDisplay.ImageScale-ScaleIncrement;
    if CameraActionHandler.CurrentScale <0.2  then CameraActionHandler.CurrentScale := 0.2;
    CDisplay.ImageScale := CameraActionHandler.CurrentScale;
    finally
    CDisplayEndUpdate;
    end;
    except
     CDisplayEndUpdateOnException;
    end;
    end;
end; //case
  if CurrentCameraState.CentreMode = tcmXHair then
      begin
      SceneScaleTag.AsDouble := CDisplay.ImageScale;
      CameraActionHandler.DefaultJobImageScaleChanged(nil);
      end;
    CameraActionHandler.KickLazyUpdate;

end;


procedure TCognexDisplay.DoXHairLeft(ATag: TAbstractTag);
var
TagVal : Integer;
begin
TagVal := ATag.AsInteger;
if TagVal = 0 then
  begin
  if CanTalk then
    begin
    if NamesMatch(CurrentJobName,DefaultJobName,True) then
      begin
      CurrentCameraState.XOffset := CurrentCameraState.XOffset-XHairIncrement;
      CurrentCameraState.OffsetIsManual := 1;
      CognexComms.SendServiceCommand(Format('SFCalData.XOffset %7.3f',[CurrentCameraState.XOffset]));
      XoffsetTag.AsDouble := CurrentCameraState.XOffset;
      CameraActionHandler.DoCalibrationUpdate;
      end;
    end;
    if CurrentCameraState.CentreMode = tcmXHair then
      begin
      CDisplayBeginUpdate;
      try
      try
      SceneScaleTag.AsDouble := CDisplay.ImageScale;
      finally
      CDisplayEndUpdate;
      end;
      except
       CDisplayEndUpdateOnException;
      end;
      CameraActionHandler.DefaultJobImageScaleChanged(nil);
      end;
  end;
end;

procedure TCognexDisplay.DoXHairRight(ATag: TAbstractTag);
var
TagVal : Integer;
begin
TagVal := ATag.AsInteger;
if TagVal = 0 then
  begin
  if CanTalk then
    begin
    if NamesMatch(CurrentJobName,DefaultJobName,True) then
      begin
      CurrentCameraState.XOffset := CurrentCameraState.XOffset+XHairIncrement;
      CurrentCameraState.OffsetIsManual := 1;
      CognexComms.SendServiceCommand(Format('SFCalData.XOffset %7.3f',[CurrentCameraState.XOffset]));
      XoffsetTag.AsDouble := CurrentCameraState.XOffset;
      CameraActionHandler.DoCalibrationUpdate;
      end;
    end;
    if CurrentCameraState.CentreMode = tcmXHair then
      begin
      CDisplayBeginUpdate;
      try
      try
      SceneScaleTag.AsDouble := CDisplay.ImageScale;
      finally
      CDisplayEndUpdate;
      end;
      except
       CDisplayEndUpdateOnException;
      end;
      CameraActionHandler.DefaultJobImageScaleChanged(nil);
      end;
  end;
end;

procedure TCognexDisplay.DoXHairOnOff(ATag: TAbstractTag);
var
TagVal : Integer;
begin
if CanTalk then
  begin
  if NamesMatch(CurrentJobName,DefaultJobName,True) then
    begin
    TagVal := ATag.AsInteger;
    if TagVal <> 0 then
      begin
      CurrentCameraState.aaXHairOn := 1;
      CognexComms.SendServiceCommand('SIVerticalLine.TOOL_ENABLED 1');
      CognexComms.SendServiceCommand('SIHorizontalLine.TOOL_ENABLED 1');
      XHairIsOnTag.AsInteger := 0;
      XHairIsOnTag.AsInteger := 1;
      end
    else
      begin
      CurrentCameraState.aaXHairOn := 0;
      CognexComms.SendServiceCommand('SIVerticalLine.TOOL_ENABLED 0');
      CognexComms.SendServiceCommand('SIHorizontalLine.TOOL_ENABLED 0');
      XHairIsOnTag.AsInteger := 1;
      XHairIsOnTag.AsInteger := 0;
      end
    end;
   CameraActionHandler.KickLazyUpdate;
  end;
end;


procedure TCognexDisplay.DoScaleUp(ATag: TAbstractTag);
var
TagVal : Integer;
begin
TagVal := ATag.AsInteger;
if TagVal = 0 then
  begin
  if CanTalk then
    begin
    if NamesMatch(CurrentJobName,DefaultJobName,True) then
      begin
      CDisplayBeginUpdate;
      try
      try
      CameraActionHandler.CurrentScale := CDisplay.ImageScale+ScaleIncrement;
      if CameraActionHandler.CurrentScale > 4.0  then CameraActionHandler.CurrentScale := 4.00;
      CDisplay.ImageScale := CameraActionHandler.CurrentScale;
      finally
      CDisplayEndUpdate;
      end;
      except
       CDisplayEndUpdateOnException;
      end;
      CurrentCameraState.FitMode := 0;
      CameraActionHandler.KickLazyUpdate;
      end;
    end;
  end;
end;

procedure TCognexDisplay.DoScaleDown(ATag: TAbstractTag);
var
TagVal : Integer;
begin
TagVal := ATag.AsInteger;
if TagVal = 0 then
  begin
  if CanTalk then
    begin
    if NamesMatch(CurrentJobName,DefaultJobName,True) then
      begin
      CDisplayBeginUpdate;
      try
      try
      CameraActionHandler.CurrentScale := CDisplay.ImageScale-ScaleIncrement;
      CurrentCameraState.Scale := CameraActionHandler.CurrentScale;
      CurrentCameraState.FitMode := 0;
      if CameraActionHandler.CurrentScale <0.2  then CameraActionHandler.CurrentScale := 0.2;
      CDisplay.ImageScale := CameraActionHandler.CurrentScale;
      finally
      CDisplayEndUpdate;
      end;
      except
       CDisplayEndUpdateOnException;
      end;
      CameraActionHandler.KickLazyUpdate;
      end;
    end;
  end;

end;

procedure TCognexDisplay.DoCircleUp(ATag: TAbstractTag);
Var
Rad : Double;
TagVal : Integer;
begin
TagVal := ATag.AsInteger;
if TagVal = 0 then
  begin
  if CanTalk then
    begin
    if NamesMatch(CurrentJobName,DefaultJobName,True) then
      begin
      Rad := CurrentCameraState.CircleRadius+CircleIncrement;
      if Rad >400 then Rad := 400;
      CurrentCameraState.CircleRadius := Rad;
      CognexComms.SendServiceCommand(Format('SFCircleRadius.Value %7.3f',[Rad]));
      CameraActionHandler.KickLazyUpdate;
      end;
    end;
  end;
end;

procedure TCognexDisplay.DoCircleDown(ATag: TAbstractTag);
Var
Rad : Double;
TagVal : Integer;
begin
TagVal := ATag.AsInteger;
if TagVal = 0 then
  begin
  if CanTalk then
    begin
    if NamesMatch(CurrentJobName,DefaultJobName,True) then
      begin
      Rad := CurrentCameraState.CircleRadius-CircleIncrement;
      if Rad < 0 then Rad := 0;
      CurrentCameraState.CircleRadius := Rad;
      CognexComms.SendServiceCommand(Format('SFCircleRadius.Value %7.3f',[Rad]));
      CameraActionHandler.KickLazyUpdate;
      end;
    end;
  end;
end;
procedure TCognexDisplay.DoXHairColor(ATag: TAbstractTag);
var
TagVal : Integer;
begin
if CanTalk then
  begin
  if NamesMatch(CurrentJobName,DefaultJobName,True) then
    begin
    TagVal := ATag.AsInteger;
    if TagVal > 0 then
      begin
      CurrentCameraState.XHairColorNumber := TagVal;
      CognexComms.SendServiceCommand(Format('SIVerticalLine.COLOR %d',[TagVal]));
      CognexComms.SendServiceCommand(Format('SIHorizontalLine.COLOR %d',[TagVal]));
      CameraActionHandler.KickLazyUpdate;
      end
    end;
  end;
end;

procedure TCognexDisplay.DoCircleColor(ATag: TAbstractTag);
var
TagVal : Integer;
begin
if CanTalk then
  begin
  if NamesMatch(CurrentJobName,DefaultJobName,True) then
    begin
    TagVal := ATag.AsInteger;
    if TagVal > 0 then
      begin
      CurrentCameraState.CircleColorNumber := TagVal;
      CognexComms.SendServiceCommand(Format('SICIRCLE_1.COLOR %d',[TagVal]));
      CameraActionHandler.KickLazyUpdate;
      end
    end;
  end;
end;

procedure TCognexDisplay.DoCircleOnOff(ATag: TAbstractTag);
var
TagVal : Integer;
begin
if CanTalk then
  begin
  if NamesMatch(CurrentJobName,DefaultJobName,True) then
    begin
    TagVal := ATag.AsInteger;
    if TagVal <> 0 then
      begin
      CurrentCameraState.CircleOn := 1;
      CognexComms.SendServiceCommand('SICIRCLE_1.TOOL_ENABLED 1');
      CircleIsOnTag.AsInteger := 0;
      CircleIsOnTag.AsInteger := 1;
      end
    else
      begin
      CurrentCameraState.CircleOn := 0;
      CognexComms.SendServiceCommand('SICIRCLE_1.TOOL_ENABLED 0');
      CircleIsOnTag.AsInteger := 1;
      CircleIsOnTag.AsInteger := 0;
      end;
    CameraActionHandler.KickLazyUpdate;
    end;
  end;
end;

procedure TCognexDisplay.DoXHairUp(ATag: TAbstractTag);
var
TagVal : Integer;
begin
TagVal := ATag.AsInteger;
if TagVal = 0 then
  begin
  if CanTalk then
    begin
    if NamesMatch(CurrentJobName,DefaultJobName,True) then
      begin
      CurrentCameraState.YOffset := CurrentCameraState.YOffset-XHairIncrement;
      CognexComms.SendServiceCommand(Format('SFCalData.YOffset %7.3f',[CurrentCameraState.YOffset]));
      YoffsetTag.AsDouble := CurrentCameraState.YOffset;
      CameraActionHandler.DoCalibrationUpdate;
      end;
    end;
    if CurrentCameraState.CentreMode = tcmXHair then
      begin
      CDisplayBeginUpdate;
      try
      try
      SceneScaleTag.AsDouble := CDisplay.ImageScale;
      finally
      CDisplayEndUpdate;
      end;
      except
       CDisplayEndUpdateOnException;
      end;
      CameraActionHandler.DefaultJobImageScaleChanged(nil);
      end
  end;
end;

procedure TCognexDisplay.DoXHairJogUp(ATag: TAbstractTag);
var
TagVal : Integer;
begin
if CanTalk then
  begin
  if NamesMatch(CurrentJobName,DefaultJobName,True) then
    begin
    TagVal := ATag.AsInteger;
    If TagVal <> 0 then
      begin
      VisionJogMode := jmHairUp;
      JogTimer.Enabled := True;
      end
    else
      begin
      VisionJogMode := jmNone;
      end;
    end;
  end;
end;

procedure TCognexDisplay.DoXHairDown(ATag: TAbstractTag);
var
TagVal : Integer;
begin
TagVal := ATag.AsInteger;
if TagVal = 0 then
  begin
  if CanTalk then
    begin
    if NamesMatch(CurrentJobName,DefaultJobName,True) then
      begin
      CurrentCameraState.YOffset := CurrentCameraState.YOffset+XHairIncrement;
      CognexComms.SendServiceCommand(Format('SFCalData.YOffset %7.3f',[CurrentCameraState.YOffset]));
      YoffsetTag.AsDouble := CurrentCameraState.YOffset;
      CameraActionHandler.DoCalibrationUpdate;
      end;
    end;
    if CurrentCameraState.CentreMode = tcmXHair then
      begin
      CDisplayBeginUpdate;
      try
      try
      SceneScaleTag.AsDouble := CDisplay.ImageScale;
      finally
      CDisplayEndUpdate;
      end;
      except
       CDisplayEndUpdateOnException;
      end;
      CameraActionHandler.DefaultJobImageScaleChanged(nil);
      end
  end;
end;

procedure TCognexDisplay.DoXHairJogDown(ATag: TAbstractTag);
var
TagVal : Integer;
begin
if CanTalk then
  begin
  if NamesMatch(CurrentJobName,DefaultJobName,True) then
    begin
    TagVal := ATag.AsInteger;
    If TagVal <> 0 then
      begin
      VisionJogMode := jmHairDown;
      JogTimer.Enabled := True;
      end
    else
      begin
      VisionJogMode := jmNone;
      end;
    end;
  end;
end;

procedure TCognexDisplay.DoFitChanged(ATag: TAbstractTag);
begin
  CameraActionHandler.RequestFit(False);
end;

procedure TCognexDisplay.DoFullScale(ATag: TAbstractTag);
begin
  CameraActionHandler.RequestFullScale(InCycle);
end;

procedure TCognexDisplay.RegisterScreenMode(ATag: TAbstractTag);
begin
  if ((not InCycle) and CameraActionHandler.DefaultJobActive) then
    CameraActionHandler.RequestScreenUpdate;
end;


procedure TCognexDisplay.DoShowErrors(ATag: TAbstractTag);
var
TagVal : Integer;
begin
if Not InstallDone Then Exit;
if Assigned(ATag) then
  begin
  TagVal := ATag.AsInteger;
  if TagVal = 0 then SetDisplayMode(vdmError,False,False);
  end;

end;


procedure TCognexDisplay.DoExposureChanged(ATag: TAbstractTag);
var
TagVal : Double;
begin
if Not InstallDone Then Exit;
if Assigned(ATag) then
  begin
  TagVal := ATag.AsDouble;
  if CanTalk then
    begin
    if NamesMatch(CurrentJobName,DefaultJobName,True) then
      begin
      CurrentCameraState.Exposure := TagVal;
      CognexComms.SendServiceCommand(Format('SFAcquisition.Exposure_Time %5.3f',[CurrentCameraState.Exposure]));
      if InternalExposure then
        begin
        InternalExposure := False;
        end
      else
        begin
        CameraActionHandler.KickLazyUpdate;
        end;
      end;
    end;
 end;
end;

procedure TCognexDisplay.DoCentreOnXHair(ATag: TAbstractTag);
var
TagVal : Integer;
begin
if InstallDone then
  begin
  if NamesMatch(CurrentJobName,DefaultJobName,True) then
    begin
    TagVal := ATag.AsInteger;
    if TagVal <> 0 then
      begin
      CurrentCameraState.CentreMode := tcmXHair;
      CurrentCameraState.CentreModeInteger := 1;
      COFOVTag.AsInteger := 0;
      COXTag.AsInteger := 1;
      CurrentCameraState.FitMode := 0;
      end
    else
      begin
      CurrentCameraState.CentreMode := tcmScreen;
      CurrentCameraState.CentreModeInteger := 0;
      COFOVTag.AsInteger := 1;
      COXTag.AsInteger := 0;
      end
    end;
  end;
 CDisplayBeginUpdate;
 try
 try
 SceneScaleTag.AsDouble := CDisplay.ImageScale;
 finally
 CDisplayEndUpdate;
 end;
 except
  CDisplayEndUpdateOnException;
 end;

 CameraActionHandler.DefaultJobImageScaleChanged(nil);
 CameraActionHandler.XHCentreMode := 1;
 CameraActionHandler.KickLazyUpdate;
end;

procedure TCognexDisplay.DoScaleSet(ATag: TAbstractTag);
var
NewScale : Double;
begin
if InstallDone then
  begin
  NewScale := ATag.AsDouble;
 {$IFDEF HIGHDEBUG}
 AddDebug(Format('Setting SceneScale to %5.3f',[NewScale]),tvmDebug);
 {$ENDIF}
//  if NewScale <=0.1 then ProcessFitChanged;
  CameraActionHandler.CurrentScale := NewScale;
  CDisplayBeginUpdate;
  try
  try
  CDisplay.ImageScale := NewScale;
  finally
  CDisplayEndUpdate;
  end;
  except
   CDisplayEndUpdateOnException;
  end;
  CurrentCameraState.Scale := CameraActionHandler.CurrentScale;
  CurrentCameraState.FitMode := 0;
  CameraActionHandler.DoCalibrationUpdate;
  end;
end;

procedure TCognexDisplay.DoXHairJogLeft(ATag: TAbstractTag);
var
TagVal : Integer;
begin
if CanTalk then
  begin
  if NamesMatch(CurrentJobName,DefaultJobName,True) then
    begin
    TagVal := ATag.AsInteger;
    If TagVal <> 0 then
      begin
      VisionJogMode := jmHairLeft;
      JogTimer.Enabled := True;
      end
    else
      begin
      VisionJogMode := jmNone;
      end;
    end;
  end;
end;

procedure TCognexDisplay.DoXHairJogRight(ATag: TAbstractTag);
var
TagVal : Integer;
begin
if CanTalk then
  begin
  if NamesMatch(CurrentJobName,DefaultJobName,True) then
    begin
    TagVal := ATag.AsInteger;
    If TagVal <> 0 then
      begin
      VisionJogMode := jmHairRight;
      JogTimer.Enabled := True;
      end
    else
      begin
      VisionJogMode := jmNone;
      end;
    end;
  end;
end;

procedure TCognexDisplay.DoCircleJogUp(ATag: TAbstractTag);
var
TagVal : Integer;
begin
if CanTalk then
  begin
  if NamesMatch(CurrentJobName,DefaultJobName,True) then
    begin
    TagVal := ATag.AsInteger;
    If TagVal <> 0 then
      begin
      VisionJogMode := jmCircleUp;
      JogTimer.Enabled := True;
      end
    else
      begin
      VisionJogMode := jmNone;
      end;
    end;
  end;
end;
procedure TCognexDisplay.DoCircleJogDown(ATag: TAbstractTag);
var
TagVal : Integer;
begin
if CanTalk then
  begin
  if NamesMatch(CurrentJobName,DefaultJobName,True) then
    begin
    TagVal := ATag.AsInteger;
    If TagVal <> 0 then
      begin
      VisionJogMode := jmCircleDown;
      JogTimer.Enabled := True;
      end
    else
      begin
      VisionJogMode := jmNone;
      end;
    end;
  end;
end;

procedure TCognexDisplay.DoScaleJogUp(ATag: TAbstractTag);
var
TagVal : Integer;
begin
if CanTalk then
  begin
  if NamesMatch(CurrentJobName,DefaultJobName,True) then
    begin
    TagVal := ATag.AsInteger;
    If TagVal <> 0 then
      begin
      VisionJogMode := jmScaleUp;
      JogTimer.Enabled := True;
      end
    else
      begin
      VisionJogMode := jmNone;
      end;
    end;
  end;
end;

procedure TCognexDisplay.DoScaleJogDown(ATag: TAbstractTag);
var
TagVal : Integer;
begin
if CanTalk then
  begin
  if NamesMatch(CurrentJobName,DefaultJobName,True) then
    begin
    TagVal := ATag.AsInteger;
    If TagVal <> 0 then
      begin
      VisionJogMode := jmScaleDown;
      JogTimer.Enabled := True;
      end
    else
      begin
      VisionJogMode := jmNone;
      end;
    end;
  end;
end;

procedure TCognexDisplay.FTPIsLoggedIn(Sender: Tobject; LoginState: Boolean);
begin
if assigned(FTPControlPanel) then
  begin
  FTPControlPanel.LoggedIn := LoginState;
  CameraActionHandler.EndWaitForFTPLogin(LoginState);
  end;
end;

procedure TCognexDisplay.FTPJobListChanged(Sender: Tobject;AtPOwerup : Boolean);
begin
if assigned(FTPControlPanel) then
  begin
  FTPControlPanel.FTPJobList := CameraActionHandler.JobList;
  if AtPOwerup then CameraActionHandler.EndWaitForInitialJobList(True);
  end;
end;


procedure TCognexDisplay.GotSymbolicValue(Sender: Tobject; Success: Boolean;Value: String);
begin
 {if Success then
   begin
   NCCommandComplete(True,nil,tncGetSymbolicValue,Value,Value,1,'');
   end
 else
   begin
   NCCommandComplete(False,nil,tncGetSymbolicValue,'','',1,'Error Getting Value');
   end}
end;

procedure TCognexDisplay.SnapshotDone(Sender: Tobject; Success: Boolean);
begin
 if Success then
   begin
   NCCommandComplete(True,nil,tncSaveSnapshot,'','',1,'');
   end
 else
   begin
   NCCommandComplete(False,nil,tncSaveSnapshot,'','',1,'Error Taking Snapshot');
   end
end;

procedure TCognexDisplay.TelnetSaveDone(Sender: Tobject; Success: Boolean);
begin
 if Success then
   begin
   NCCommandComplete(True,nil,tncSaveTelnet,'','',1,'');
   end
 else
   begin
   NCCommandComplete(False,nil,tncSaveTelnet,'','',1,'Error Taking Snapshot');
   end
end;


procedure TCognexDisplay.TestedPowerUpDone(Sender: Tobject);
begin
DelayLeft := 2;
TimedMessage('Camera Connection Successful',1500)
end;


procedure TCognexDisplay.FTPFileImported(Filename: String; Success: Boolean);
begin
  CameraActionHandler.EndWaitForFileImport(Success);
end;

procedure TCognexDisplay.GoneOnLineForImport(Sender: Tobject; Success: Boolean);
begin
  CameraActionHandler.EndWaitForOnLineForFileImport(Success);
end;


procedure TCognexDisplay.FTPLoggedInAndGotJobList(Sender: Tobject; LoginState: Boolean);
begin
CameraActionHandler.EndWaitInitialJobList(True);
end;

procedure TCognexDisplay.PostCommandOverrunDetected(Sender: Tobject);
var
OutMess : String;
begin
    OutMess := 'Detected Overrun in Part Program';
    SysObj.FaultInterface.SetFault(Self, CoreFaults[cfSysWarning], [OutMess], nil);
end;


procedure TCognexDisplay.CognexLockout(Sender: Tobject; ErrorMess: String);
begin
SetCommandStataus(-1);
VisionErrorMessTag.AsString := Format('Vision Lockout performing command %s',[ErrorMess]);
end;



function TCognexDisplay.MakeTimeStampedSnapName: String;
var
TimeStamp : String;
DateStamp :String;
begin
TimeStamp := MakeTimeStamp;
DateStamp := MakeDateStamp;
Result := Format('SnapShot_%s_%s',[TimeStamp,DateStamp]);

end;

function TCognexDisplay.MakeNumberedDateStamp: String;
begin
Result:= Format('%d,%d,%d',[ErrorDisplay.ErrorStack.DayTag.AsInteger,ErrorDisplay.ErrorStack.MonthTag.AsInteger,ErrorDisplay.ErrorStack.YearTag.AsInteger]);
end;

function TCognexDisplay.MakeDateStamp: String;
var
Month : Integer;
MStr : String;
begin
Month := ErrorDisplay.ErrorStack.MonthTag.AsInteger;
MStr := 'Jan';
case Month of
  1: MStr := 'Jan';
  2: MStr := 'Feb';
  3: MStr := 'Mar';
  4: MStr := 'Apr';
  5: MStr := 'May';
  6: MStr := 'Jun';
  7: MStr := 'Jul';
  8: MStr := 'Aug';
  9: MStr := 'Sep';
  10: MStr := 'Oct';
  11: MStr := 'Nov';
  12: MStr := 'Dec';
  end;
Result:= Format('%d-%s-%d',[ErrorDisplay.ErrorStack.DayTag.AsInteger,MStr,ErrorDisplay.ErrorStack.YearTag.AsInteger]);
end;

function TCognexDisplay.MakeTimeStamp: String;
begin
Result:= Format('%d_%d_%d',[ErrorDisplay.ErrorStack.HourTag.AsInteger,
                            ErrorDisplay.ErrorStack.MinuteTag.AsInteger,
                            ErrorDisplay.ErrorStack.SecondTag.AsInteger]);
end;


function TCognexDisplay.MakeCommaSepTimeStamp: String;
begin
Result:= Format('%d,%d,%d',[ErrorDisplay.ErrorStack.HourTag.AsInteger,
                            ErrorDisplay.ErrorStack.MinuteTag.AsInteger,
                            ErrorDisplay.ErrorStack.SecondTag.AsInteger]);
end;

procedure TCognexDisplay.CDisplayEndUpdate;
begin
  Set8087CW($1332);
end;

procedure TCognexDisplay.CDisplayEndUpdateOnException;
begin
  Set8087CW($1332);
  EventLog('Trapped potential .net exception in Cognex');

end;

procedure TCognexDisplay.CDisplayBeginUpdate;
begin
  Set8087CW($133F);
end;



procedure TCognexDisplay.StartConnectTimeout;
begin
CameraSessionConnectTimeout.Enabled := True;
end;



procedure TCognexDisplay.CameraSessionConnected(CState: Boolean);
begin
FCameraConnected := CState;
if assigned(CameraConnectedTag) then
  begin
  If FCameraConnected then
    begin
    CameraConnectedTag.AsInteger := 1;
    if assigned(TelnetIndicatorPanel) then TelnetIndicatorPanel.ConnectedStatus := True;
    end
  else
    begin
    CameraConnectedTag.AsInteger := 0;
    if assigned(TelnetIndicatorPanel) then
      begin
      TelnetIndicatorPanel.ConnectedStatus := False;
      TelnetIndicatorPanel.LoggedInStatus := False;
      end;
    end;
  end;
end;

function TCognexDisplay.CameraIsConnected: Boolean;
begin
Result := FCameraConnected;
end;


procedure TCognexDisplay.MakeCDisplay;
begin
if assigned(CDisplay) then
  begin
  CDisplay.Parent.RemoveControl(CDisplay);
  CDisplay.Disconnect;
  CDisplay.Free;
  InstallDone := False;
  end;

 CDisplay := TCvsInSightDisplay.Create(nil);
 EventLog('Cdisplay Created');
 CDisplay.Parent := CameraParentPanel;
 GlobalCognexDisplay := CDisplay;
 CDisplay.Visible := True;
 try
  NoCognex:= False;
  SetNoCognexMode(False);
  with CDisplay do
    begin
    Align := alClient;
    CDisplayBeginUpdate;
     try
     try
     CDisplay.ImageOrientation := ImageOrientation;
     finally
     CDisplayEndUpdate;
     end;
     except
      CDisplayEndUpdateOnException;
     end;
    end;
  except
  TelnetMemo.Color := clRed;
  NoCognex := True;
  end;

  if NoCognex then
    begin
    MessPanel.Visible := True;
    MessPanel.BringToFront;
    MessPanel.Caption := 'No Cognex Software Detected';
//    LocateMessagePanel(12,350);
    LocateMessagePanel(350);
    end
  else
    begin
    CDisplayBeginUpdate;
    try
    try
    CDisplay.OnImageScaleChanged := ImageScaleChanged;
    CDisplay.OnConnectedChanged  := ConnectionStateChanged;
    CDisplay.OnConnectCompleted := SessionConnectionCompleted;
    CDisplay.Refresh;
    CDisplay.Visible := False;
    CDisplay.Align := alClient;
    CDIsplay.Disconnect;
    finally
      CDisplayEndUpdate;
      CameraActionHandler.CognexDisplay := CDisplay;
      CameraActionHandler.CognexParent := CameraParentPanel;
      InstallDone := True;
      EventLog('Cdisplay Install Done');
    end;
    except
      CDisplayEndUpdateOnException;
      DisplayPanel.Color := clRed;
      InstallDone := True;
    end;
    end;
end;




procedure TCognexDisplay.DoBufferedCommand(Sender : TObject);
begin
    if CommandIsBuffered then
      begin
      PerformingBufferedCommand := True;
      end;
end;





procedure TCognexDisplay.DoGotJobPassResult(Sender: TObject; Pass: Boolean; InLineError : Boolean;InFastRun : Boolean;ExecutionTime: Double);
var
Date,Time : String;
IsFastStr : String;
ETime : Integer;
begin
 Date := MakeNumberedDateStamp;
 Time := MakeCommaSepTimeStamp;
 ETime := round(ExecutionTime);
 if InFastRun then IsFastStr := '1' else IsFastStr := '0';
 if Pass then
  begin
  JobDataTag.AsString := Format('%s,%s,%s,1,%s,%d',[Date,Time,CurrentJobNameTag.AsString,IsFastStr,ETime]);
  end
 else
  begin
  JobDataTag.AsString := Format('%s,%s,%s,0,%s,%d',[Date,Time,CurrentJobNameTag.AsString,IsFastStr,ETime]);
  end;
end;



procedure TCognexDisplay.OnLineFailed(Sender: TObject);
var
Date,Time : String;
IsFastStr : String;
ETime : Integer;
begin
 Date := MakeNumberedDateStamp;
 Time := MakeCommaSepTimeStamp;
 ETime := round(CameraActionHandler.JobRunTime);
 JobDataTag.AsString := Format('%s,%s,%s,-1,-1,%d',[Date,Time,CurrentJobNameTag.AsString,ETime]);
end;


procedure TCognexDisplay.CameraGFLocked(Sender: TObject);
begin
AddDebug('**************************',tvmDebug);
AddDebug('GFLock Set',tvmDebug);
AddDebug('**************************',tvmDebug);
if FNoCognexMode = False then
  begin
  CognexComms.GraceFullDisconnect;

  SetNoCognexMode(True);
  CameraActionHandler.CogNexCommunicator.ClearDown;
  end;
end;

initialization
  RegisterCNCClass(TCognexDisplay);
end.
