unit VisionErrorStack;

interface
uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,VisionCommand,
  StdCtrls,Buttons, ExtCtrls, ComCtrls,Menus,   Grids,CognexTelnetClient,TelnetDisplay,CognexTYpes,CoreCNC,CNC32,
  IdBaseComponent, IdComponent, IdTCPServer, IdTelnetServer, IdTCPConnection, IdTCPClient, IdTelnet,PlusMemo;
const

  CRLF = #$d + #$a;
  MessHeight = 22;
  LabelWidth = 80;
  ButtonPanelWidth = 60;

type

TErrorSelectionEvent = procedure(EIndex : Integer) of object;
TServiceIntervalChangeEvent = procedure(SI : Integer) of object;

{TCocgnexInputMessage = class(TObject)
  public
  ServiceInterval : Integer;
  CommandStr : String;
  CommandType : TNCCommands;
  IsNCCommand : Boolean;
  HasResponse : Boolean;
  CommandsFollowing : Integer;
  NCResponse  : String;
  ErrorString : String;
  Procedure AssignTo(var Dest : TCocgnexInputMessage);
  constructor Create(SInterval : Integer);
  end;}


TErrorStackEntry = class(TObject)
 TimeStamp : String;
 DateStamp : String;
 OriginalCommand : TCocgnexInputMessage;
 ResponseString  : String;
 Destructor Destroy; override;
 end;



TVisionErrorStack = class (TObject)
  private
    ErrorList : TList;
    function GetErrorsExist: Boolean;
  public
    YearTag       : TAbstractTag; //Externally Set
    MonthTag      : TAbstractTag;//Externally Set
    DayTag        : TAbstractTag;  //Externally Set
    HourTag       : TAbstractTag; //Externally Set
    MinuteTag     : TAbstractTag;//Externally Set
    SecondTag     : TAbstractTag;//Externally Set
    ErrorCountTag : TAbstractTag;//Externally Set
    constructor Create;
    destructor Destroy;override;
    procedure CleanDown;
    function MakeTimeStamp: String;
    function MakeDateStamp: String;
    function ErrorStringForIndex(EIndex : Integer): String;
    function ErrorDataAtIndex(EIndex : Integer): TErrorStackEntry;
    procedure aAddEntry(Command : TCocgnexInputMessage; Response : String);
    property ErrorsExist : Boolean  read GetErrorsExist;
  end;

TErrorGrid = Class(TStringGrid)
  private
    FonErrorSelected : TErrorSelectionEvent;
    procedure Initialise;
    procedure AddError(AError : TErrorStackEntry;Enum : Integer);
    procedure ErrorSelected(Sender: TObject; ACol, ARow: Longint; var CanSelect: Boolean);

  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure CleanDown;
    procedure RefitToParent;
    procedure LoadFromErrorStack(EStack : TVisionErrorStack);
    property OnErrorSelected : TErrorSelectionEvent read FonErrorSelected write FonErrorSelected;
  end;


TErrorDisplayPanel = class(TPanel)
  private
    LowerPanel : TPanel;
    ErrorGrid : TErrorGrid;
    DateStampDisplay : TEdit;
    DateStampLabel : TLabel;
    TimeStampDisplay : TEdit;
    TimeStampLabel : TLabel;
    EMDisplay : TEdit;
    EMLabel : TLabel;
    ResponseDisplay: TEdit;
    ResponseLabel : TLabel;
    OutMessDisplay: TEdit;
    OutMessLabel : TLabel;
    ClearButton : TButton;
    procedure RedrawOnResize(Sender: TObject);
    procedure ClearButtonClick(Sender: TObject);
    Procedure ShowErrorAtIndex(EIndex : Integer);

  public
    ErrorStack : TVisionErrorStack;
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure InitialiseForDisplay;
    procedure ClearErrors;
    procedure CleanDisplays;
    function GetErrorTextAtIndex(EIndex : Integer): String;
    end;


implementation
(*{ TCocgnexInputMessage }
procedure TCocgnexInputMessage.AssignTo(var Dest: TCocgnexInputMessage);
begin
if not assigned(Dest) then Dest := TCocgnexInputMessage.Create(ServiceInterval);
  Dest.CommandStr := CommandStr;
  Dest.CommandType := CommandType;
  Dest.IsNCCommand := IsNCCommand;
  Dest.HasResponse := HasResponse;
  Dest.CommandsFollowing := CommandsFollowing;
  Dest.NCResponse  := NCResponse;
  Dest.ErrorString := ErrorString;
end;

constructor TCocgnexInputMessage.Create(SInterval : Integer);
begin
  inherited Create;
  CommandsFollowing := 0;
  ServiceInterval := SInterval;
  ErrorString := '';
end;*)

{ TErrorStack }
procedure TVisionErrorStack.aAddEntry(Command: TCocgnexInputMessage; Response: String);
var
NewEntry : TErrorStackEntry;
NewCommand : TCocgnexInputMessage;
begin
NewCommand := TCocgnexInputMessage.Create(10);
Command.AssignTo(NewCommand);
NewEntry := TErrorStackEntry.Create;
NewEntry.TimeStamp := MakeTimestamp;
NewEntry.DateStamp := MakeDateStamp;
NewEntry.OriginalCommand := NewCommand;
NewEntry.ResponseString := Response;
ErrorList.Add(NewEntry);
if assigned(ErrorCountTag) then ErrorCountTag.AsInteger := ErrorList.Count;
end;

procedure TVisionErrorStack.CleanDown;
var
DEntry : TErrorStackEntry;
begin
 try
 While ErrorList.Count > 0 do
  begin
  DEntry := TErrorStackEntry(ErrorList[0]);
  DEntry.Free;
  ErrorList.Delete(0);
  end;
 except
 ErrorList.Clear;
 end;
 if assigned(ErrorCountTag) then ErrorCountTag.AsInteger := ErrorList.Count;
end;



constructor TVisionErrorStack.Create;
begin
ErrorList := TList.Create;
end;

destructor TVisionErrorStack.Destroy;
begin
  CleanDown;
  ErrorList.Free;
  ErrorList := nil;
  inherited;
end;

function TVisionErrorStack.ErrorDataAtIndex(EIndex: Integer): TErrorStackEntry;
begin

Result := nil;
if EIndex < ErrorList.count then
  begin
  Result := TErrorStackEntry(ErrorList[EIndex]);
  end
end;

function TVisionErrorStack.ErrorStringForIndex(EIndex: Integer): String;
var
EEntry : TErrorStackEntry;
begin
If (EIndex < ErrorList.Count) then
  begin
  EEntry := TErrorStackEntry(ErrorList[EIndex]);
  Result := EEntry.OriginalCommand.ErrorString + ' sending command--'+ EEntry.OriginalCommand.CommandStr +Format('(%s)',[EEntry.ResponseString]);
  end
else
 Result := 'Invalid Error Index';
end;

function TVisionErrorStack.GetErrorsExist: Boolean;
begin
Result := ErrorList.Count > 0;
end;

function TVisionErrorStack.MakeDateStamp: String;
var
Month : Integer;
MStr : String;
begin
Month := MonthTag.AsInteger;
MStr := 'Jan';
case Month of
  1: MStr := 'Jan';
  2: MStr := 'Feb';
  3: MStr := 'Mar';
  4: MStr := 'Apr';
  5: MStr := 'May';
  6: MStr := 'Jun';
  7: MStr := 'Jul';
  8: MStr := 'Aug';
  9: MStr := 'Sep';
  10: MStr := 'Oct';
  11: MStr := 'Nov';
  12: MStr := 'Dec';
  end;
Result:= Format('%d-%s-%d',[DayTag.AsInteger,MStr,YearTag.AsInteger]);
end;

function TVisionErrorStack.MakeTimestamp: String;
begin
Result:= Format('%d:%d:%d',[HourTag.AsInteger,MinuteTag.AsInteger,SecondTag.AsInteger]);
end;

{ TErrorGrid }
constructor TErrorGrid.Create(AOwner: TComponent);
begin
  inherited;
  OPtions := [goFixedVertLine,goFixedHorzLine,goVertLine,goHorzLine,goRowSelect,goDrawFocusSelected];
  Initialise;
end;

procedure TErrorGrid.AddError(AError: TErrorStackEntry;Enum : Integer);
begin
if Enum > 0 then RowCount := RowCount+1;
Cells[0,RowCount-1] := AError.TimeStamp;
Cells[1,RowCount-1] := AError.OriginalCommand.ErrorString;
Cells[2,RowCount-1] := AError.ResponseString;
end;


procedure TErrorGrid.CleanDown;
begin
//ColCount := 0;
//RowCount := 0;
end;

procedure TErrorGrid.Initialise;
begin
  ColCount := 3;
  RowCount := 2;
  FixedCols := 0;
  FixedRows := 1;
  ColWidths[0] := 160;
  ColWidths[1] := 260;
  ColWidths[1] := 30;
  Cells[0,0] := 'Time';
  Cells[1,0] := 'Error';
  Cells[2,0] := 'Resp.';
  OnSelectCell := ErrorSelected;
end;

destructor TErrorGrid.Destroy;
begin
  CleanDown;
  inherited;
end;

procedure TErrorGrid.RefitToParent;
begin
  ColWidths[0] := 160;
  ColWidths[1] := ClientWidth-214;
  ColWidths[2] := 50;
end;

procedure TErrorGrid.LoadFromErrorStack(EStack: TVisionErrorStack);
var
ENum : Integer;
EData : TErrorStackEntry;
begin
 RowCount := 2;
 FixedRows := 1;
 if EStack.ErrorList.Count > 0 then
  begin
  For Enum := 0 to EStack.ErrorList.Count-1 do
    begin
    EData := Estack.ErrorDataAtIndex(Enum);
    AddError(Edata,Enum);
   end;
  end;
  Invalidate;
end;

procedure TErrorGrid.ErrorSelected(Sender: TObject; ACol, ARow: Integer; var CanSelect: Boolean);
begin
If assigned(FonErrorSelected) then FonErrorSelected(Arow-1);
end;

{ TErrorDisplayPanel }

constructor TErrorDisplayPanel.Create(AOwner: TComponent);
begin
  inherited;
  ErrorStack := TVisionErrorStack.Create;
  LowerPanel := TPanel.create(nil);
  LowerPanel.Parent := Self;
  with LowerPanel do
    begin
    Align:= alBottom;
    Height := 110;
    end;

  ErrorGrid := TErrorGrid.Create(nil);
  ErrorGrid.Parent := Self;
  with ErrorGrid do
    begin
    Align := alClient;
    OnErrorSelected := ShowErrorAtIndex;
    end;

  TimeStampDisplay  := TEdit.create(nil);
  TimeStampDisplay.Parent := LowerPanel;
  with TimeStampDisplay do
    begin
    Text := 'Time';
    ReadOnly := True;
    Left := LabelWidth+10+200;
    Top := 5;
    Height := MessHeight;
    end;

  DateStampDisplay   := TEdit.create(nil);
  DateStampDisplay.Parent := LowerPanel;
  with DateStampDisplay do
    begin
    Text := 'Date';
    ReadOnly := True;
    Left := LabelWidth+10;
    Top := 5;
    Height := MessHeight;
    end;

  EMDisplay         := TEdit.create(nil);
  EMDisplay.Parent := LowerPanel;
  with EMDisplay do
    begin
    Text := 'EM';
    ReadOnly := True;
    Left := LabelWidth+10;
    Top := TimeStampDisplay.Top+MessHeight+2;
    Height := MessHeight;
    end;

  OutMessDisplay    := TEdit.create(nil);
  OutMessDisplay.Parent := LowerPanel;
  with OutMessDisplay do
    begin
    Top := EMDisplay.Top+MessHeight+2;
    Left := LabelWidth+10;
    Text := 'Out Message';
    ReadOnly := True;
    end;

  ResponseDisplay   := TEdit.create(nil);
  ResponseDisplay.Parent := LowerPanel;
  with ResponseDisplay do
    begin
    Top := OutMessDisplay.Top+MessHeight+2;
    Left := LabelWidth+10;
    Text := 'Response';
    ReadOnly := True;
    end;

  TimeStampLabel    := TLabel.create(nil);
  TimeStampLabel.Parent := LowerPanel;
  with TimeStampLabel do
    begin
    Caption := 'Time';
    Left := 235;
    Top := TimeStampDisplay.Top;
    Height := MessHeight;
    Width := LabelWidth;
    end;

  DateStampLabel    := TLabel.create(nil);
  DateStampLabel.Parent := LowerPanel;
  with DateStampLabel do
    begin
    Caption := 'Date';
    Left := 5;
    Top := TimeStampDisplay.Top;
    Height := MessHeight;
    Width := LabelWidth;
    end;



  EMLabel           := TLabel.create(nil);
  EMLabel.Parent := LowerPanel;
  with EMLabel do
    begin
    Caption := 'Error';
    Left := 5;
    Top := EMDisplay.Top;
    Height := MessHeight;
    Width := LabelWidth;
    end;

  OutMessLabel      := TLabel.create(nil);
  OutMessLabel.Parent := LowerPanel;
  with OutMessLabel do
    begin
    Caption := 'Command';
    Left := 5;
    Top := OutMessDisplay.Top;
    Height := MessHeight;
    Width := LabelWidth;
    end;

  ResponseLabel     := TLabel.create(nil);
  ResponseLabel.Parent := LowerPanel;
  with ResponseLabel do
    begin
    Caption := 'Resp.';
    Left := 5;
    Top := ResponseDisplay.Top;
    Height := MessHeight;
    Width := LabelWidth;
    end;

  ClearButton := TButton.Create(self);
  ClearButton.Parent := LowerPanel;
  with ClearButton do
    begin
    Caption := 'Clear';
    Top := 5;
    Width := 50;
    OnClick := ClearButtonClick;
    end;
  OnResize := RedrawOnResize;
end;

procedure TErrorDisplayPanel.RedrawOnResize(Sender: TObject);
var
CWidth : Integer;
begin
ErrorGrid.RefitToParent;
CWidth := ClientWidth-10;
EMDisplay.Width := CWidth-EMDisplay.Left-ButtonPanelWidth;
OutMessDisplay.Width := Cwidth-OutMessDisplay.Left-ButtonPanelWidth;
ClearButton.Left := CWidth- ClearButton.Width-5;
end;

destructor TErrorDisplayPanel.Destroy;
begin

  inherited;
end;


procedure TErrorDisplayPanel.CleanDisplays;
begin
  TimeStampDisplay.Text := '';
  DateStampDisplay.Text := '';
  EMDisplay.Text := '';
  ResponseDisplay.Text := '';
  OutMessDisplay.Text := '';
end;

procedure TErrorDisplayPanel.ShowErrorAtIndex(EIndex : INteger);
var
ErrorData : TErrorStackEntry;
begin
ErrorData := ErrorStack.ErrorDataAtIndex(EIndex);
if assigned(ErrorData) then
  begin
  TimeStampDisplay.Text := ErrorData.TimeStamp;
  DateStampDisplay.Text := ErrorData.DateStamp;
  EMDisplay.Text := ErrorData.OriginalCommand.ErrorString;
  ResponseDisplay.Text := ErrorData.ResponseString;
  OutMessDisplay.Text := ErrorData.OriginalCommand.CommandStr;
  end;

end;

procedure TErrorDisplayPanel.InitialiseForDisplay;
begin
 ErrorGrid.CleanDown;
 If ErrorStack.ErrorsExist then
  begin
  ErrorGrid.LoadFromErrorStack(ErrorStack);
  end;
end;

procedure TErrorDisplayPanel.ClearErrors;
begin
 ClearButtonClick(Self);
end;

procedure TErrorDisplayPanel.ClearButtonClick(Sender: TObject);
begin
ErrorStack.CleanDown;
with ErrorGrid do
  begin
  RowCount := 2;
  Cells[0,RowCount-1] := '';
  Cells[1,RowCount-1] := '';
  Cells[2,RowCount-1] := '';
  end;
 CleanDisplays;
end;


function TErrorDisplayPanel.GetErrorTextAtIndex(EIndex: Integer): String;
begin
If assigned(ErrorStack) then
  begin
  Result := ErrorStack.ErrorStringForIndex(EIndex)
  end
else
  begin
  Result := Format('Can not get Error at Index %d',[EIndex]);
  end

end;

{ TErrorStackEntry }

destructor TErrorStackEntry.Destroy;
begin
if assigned(OriginalCommand) then
  begin
  OriginalCommand.Free;
  end;
inherited;
end;

end.
