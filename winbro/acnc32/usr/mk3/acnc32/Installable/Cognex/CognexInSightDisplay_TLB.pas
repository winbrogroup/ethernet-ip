unit CognexInSightDisplay_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// PASTLWTR : 1.2
// File generated on 29/09/2016 10:08:00 from Type Library described below.

// ************************************************************************  //
// Type Lib: C:\PROGRA~1\COMMON~1\Cognex\In-Sight\52133~1.2\COGNEX~1.TLB (1)
// LIBID: {EFB08520-866E-4B00-8D24-A0EBCAB31799}
// LCID: 0
// Helpfile: 
// HelpString: Cognex  In-Sight Display Type Library
// DepndLst: 
//   (1) v2.0 stdole, (C:\WINDOWS\system32\stdole2.tlb)
//   (2) v5.2 CognexInSight, (C:\PROGRA~1\COMMON~1\Cognex\In-Sight\52133~1.2\COGNEX~2.TLB)
//   (3) v2.0 System_Drawing, (c:\WINDOWS\Microsoft.NET\Framework\v2.0.50727\System.Drawing.tlb)
//   (4) v2.0 System, (c:\WINDOWS\Microsoft.NET\Framework\v2.0.50727\System.tlb)
//   (5) v2.4 mscorlib, (C:\WINDOWS\Microsoft.NET\Framework\v4.0.30319\mscorlib.tlb)
//   (6) v2.0 System_Windows_Forms, (c:\WINDOWS\Microsoft.NET\Framework\v2.0.50727\System.Windows.Forms.tlb)
// Parent TypeLibrary:
//   (0) v1.2 CvsInSightDisplayOcx, (C:\Program Files\Common Files\Cognex\In-Sight\5.2.133.2\CvsInSightDisplay.ocx)
// Errors:
//   Hint: Member 'GoTo' of '_CvsInSightDisplayEdit' changed to 'GoTo_'
//   Hint: Member 'Record' of '_CvsInSightDisplayEdit' changed to 'Record_'
// ************************************************************************ //
// *************************************************************************//
// NOTE:                                                                      
// Items guarded by $IFDEF_LIVE_SERVER_AT_DESIGN_TIME are used by properties  
// which return objects that may need to be explicitly created via a function 
// call prior to any access via the property. These items have been disabled  
// in order to prevent accidental use from within the object inspector. You   
// may enable them by defining LIVE_SERVER_AT_DESIGN_TIME or by selectively   
// removing them from the $IFDEF blocks. However, such items must still be    
// programmatically created via a method of the appropriate CoClass before    
// they can be used.                                                          
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
{$WARN SYMBOL_PLATFORM OFF}
{$WRITEABLECONST ON}
{$VARPROPSETTER ON}
interface

uses Windows, ActiveX, Classes, CognexInSight_TLB, Graphics, mscorlib_TLB, OleServer, StdVCL, 
System_Drawing_TLB, System_TLB, System_Windows_Forms_TLB, Variants;
  

// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  CognexInSightDisplayMajorVersion = 5;
  CognexInSightDisplayMinorVersion = 2;

  LIBID_CognexInSightDisplay: TGUID = '{EFB08520-866E-4B00-8D24-A0EBCAB31799}';

  IID_ICvsInSightDisplayInternal: TGUID = '{5B11005B-6F26-4213-8196-9BAAA93B5D44}';
  IID_ICvsFilmstripInternal: TGUID = '{4B5440C3-37F9-488E-9A2A-BC682CB9F131}';
  IID_ICvsFilmQueue: TGUID = '{FFBF7CA5-FC20-4501-B67A-281D0F3858D0}';
  DIID_ICvsInSightDisplayEvents: TGUID = '{5B11335B-6F26-4213-8196-9BAAA9371744}';
  IID__DynamicValue: TGUID = '{77B7165D-C7E5-35E1-83D3-D2741EA109CC}';
  IID__DynamicValueComputed: TGUID = '{3631182E-0485-3D67-B6E7-CA2E2712D879}';
  IID_ICvsPlaybackFilmstripDisplayInternal: TGUID = '{93882184-435F-4F65-95E9-5C81DC5DDFAD}';
  DIID_ICvsFilmstripEvents: TGUID = '{E4C82C6E-587E-4B91-A0FF-C3BA3348D9C8}';
  IID_ICvsSensorFilmstripDisplayInternal: TGUID = '{82CA4E4B-DE49-4169-BF2F-A5378B6AE8D3}';
  CLASS_CvsSensorFilmstripDisplay: TGUID = '{11771845-694F-4C60-A2EC-66D4665016C3}';
  IID_ICvsFilmstripImage: TGUID = '{C1A669E9-DD1C-4EF2-9D9A-96356C352E58}';
  IID_ICvsFilmstripResultStatus: TGUID = '{FFED5642-E098-4D66-85BB-3665AD737651}';
  CLASS_CvsFilmstrip: TGUID = '{019D7437-EF26-4BF8-BEDF-EB5A667B75F7}';
  IID__CvsFilmstripPlayback: TGUID = '{9B9FDBA0-AE22-3ECD-8785-51751871C1E1}';
  IID__CvsFilmstripResultsQueue: TGUID = '{524D6F61-7394-3733-AD7A-48067469D6D2}';
  IID__CvsFilmstripResultsQueueOptions: TGUID = '{635261FD-A8DA-317D-B750-4352160602FC}';
  IID__CvsFilmstripResultsQueueSettings: TGUID = '{79F11714-EF4F-3DC1-B1C1-74930739B2F7}';
  CLASS_CvsPlaybackFilmstripDisplay: TGUID = '{839730D3-6FA2-4EC3-AD06-17BBCAF76F21}';
  IID__CvsInSightDisplayEdit: TGUID = '{D70FD312-7035-3834-9997-F0C3743DE8C3}';
  IID__CvsInSightDisplayRecorder: TGUID = '{7B3B0ABE-C699-3843-B98F-618C7E3166A5}';
  CLASS_CvsInSightDisplay: TGUID = '{5AFEB75B-6126-4213-8196-1333325B5D90}';
  CLASS_DynamicValue: TGUID = '{0E256753-0507-4B1E-94EC-6C4FADE45549}';
  CLASS_DynamicValueComputed: TGUID = '{9C576897-CABC-47AA-898A-E80D0BC13330}';
  CLASS_CvsFilmstripPlayback: TGUID = '{FB4BA2C3-0E08-46BD-AD54-68B56D86ACD8}';
  CLASS_CvsFilmstripResultsQueue: TGUID = '{C668348E-83CB-4D94-8E79-EA1180E713BF}';
  CLASS_CvsFilmstripResultsQueueOptions: TGUID = '{63238FAA-800B-43CC-8F66-C7D35B8AD820}';
  CLASS_CvsFilmstripResultsQueueSettings: TGUID = '{FE10703A-3DCB-4598-9A53-279DED74A227}';
  CLASS_CvsInSightDisplayEdit: TGUID = '{AA96A7D0-DC4F-47D3-BDDA-01ADB0EE7C3D}';
  CLASS_CvsInSightDisplayRecorder: TGUID = '{9F691A4C-797B-480D-9EEA-63FC29D83346}';

// *********************************************************************//
// Declaration of Enumerations defined in Type Library                    
// *********************************************************************//
// Constants for enum CvsFilmstripScale
type
  CvsFilmstripScale = TOleEnum;
const
  CvsFilmstripScale_Quarter = $00000019;
  CvsFilmstripScale_Half = $00000032;
  CvsFilmstripScale_One = $00000064;
  CvsFilmstripScale_Two = $000000C8;
  CvsFilmstripScale_Four = $00000190;

// Constants for enum CvsStatusLevelStyle
type
  CvsStatusLevelStyle = TOleEnum;
const
  CvsStatusLevelStyle_Geometric = $00000000;
  CvsStatusLevelStyle_OKorNG = $00000001;
  CvsStatusLevelStyle_Check = $00000002;

// Constants for enum CvsDisplayScrollMode
type
  CvsDisplayScrollMode = TOleEnum;
const
  CvsDisplayScrollMode_None = $FFFFFFFF;
  CvsDisplayScrollMode_Image = $00000000;
  CvsDisplayScrollMode_Grid = $00000001;

// Constants for enum CvsDisplayState
type
  CvsDisplayState = TOleEnum;
const
  CvsDisplayState_Normal = $00000000;
  CvsDisplayState_Connecting = $00000001;
  CvsDisplayState_EditingCellExpression = $00000002;
  CvsDisplayState_EditingCellValue = $00000003;
  CvsDisplayState_EditingGraphics = $00000004;
  CvsDisplayState_EditingReferenceRanges = $00000005;
  CvsDisplayState_Waiting = $00000006;
  CvsDisplayState_Dialog = $00000007;
  CvsDisplayState_DialogEditingReferenceRanges = $00000008;
  CvsDisplayState_DialogOpenedByPeer = $00000009;
  CvsDisplayState_Wizard = $0000000A;

// Constants for enum CvsDisplayZoom
type
  CvsDisplayZoom = TOleEnum;
const
  CvsDisplayZoom_None = $00000000;
  CvsDisplayZoom_Fit = $00000001;
  CvsDisplayZoom_Fill = $00000002;

// Constants for enum CvsStatusInformationState
type
  CvsStatusInformationState = TOleEnum;
const
  CvsStatusInformationState_None = $00000000;
  CvsStatusInformationState_Info = $00000001;
  CvsStatusInformationState_Warning = $00000002;
  CvsStatusInformationState_Error = $00000003;

// Constants for enum CvsRecordMode
type
  CvsRecordMode = TOleEnum;
const
  CvsRecordMode_All = $00000000;
  CvsRecordMode_Good = $00000001;
  CvsRecordMode_Bad = $00000002;
  CvsRecordMode_Sort = $00000003;

// Constants for enum ICvsInSightDisplayEventsIDs
type
  ICvsInSightDisplayEventsIDs = TOleEnum;
const
  ICvsInSightDisplayEventsIDs_CurrentCellChanged = $00000064;
  ICvsInSightDisplayEventsIDs_FontChanged = $00000065;
  ICvsInSightDisplayEventsIDs_GridOffsetChanged = $00000066;
  ICvsInSightDisplayEventsIDs_GridOpacityChanged = $00000067;
  ICvsInSightDisplayEventsIDs_GridScaleChanged = $00000068;
  ICvsInSightDisplayEventsIDs_ImageOffsetChanged = $00000069;
  ICvsInSightDisplayEventsIDs_ImageScaleChanged = $0000006A;
  ICvsInSightDisplayEventsIDs_InSightChanged = $0000006B;
  ICvsInSightDisplayEventsIDs_Invalidated = $0000006C;
  ICvsInSightDisplayEventsIDs_ResultsChanged = $0000006D;
  ICvsInSightDisplayEventsIDs_ScrollModeChanged = $0000006E;
  ICvsInSightDisplayEventsIDs_SelectedRangeChanged = $0000006F;
  ICvsInSightDisplayEventsIDs_StateChanged = $00000070;
  ICvsInSightDisplayEventsIDs_StatusInformationChanged = $00000071;
  ICvsInSightDisplayEventsIDs_ConnectedChanged = $00000072;
  ICvsInSightDisplayEventsIDs_CurrentCellExpressionChanged = $00000073;
  ICvsInSightDisplayEventsIDs_ConnectCompleted = $00000074;
  ICvsInSightDisplayEventsIDs_ImageOrientationChanged = $00000075;

type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary                    
// *********************************************************************//
  ICvsInSightDisplayInternal = interface;
  ICvsFilmstripInternal = interface;
  ICvsFilmQueue = interface;
  ICvsInSightDisplayEvents = dispinterface;
  _DynamicValue = interface;
  _DynamicValueDisp = dispinterface;
  _DynamicValueComputed = interface;
  _DynamicValueComputedDisp = dispinterface;
  ICvsPlaybackFilmstripDisplayInternal = interface;
  ICvsFilmstripEvents = dispinterface;
  ICvsSensorFilmstripDisplayInternal = interface;
  ICvsFilmstripImage = interface;
  ICvsFilmstripImageDisp = dispinterface;
  ICvsFilmstripResultStatus = interface;
  ICvsFilmstripResultStatusDisp = dispinterface;
  _CvsFilmstripPlayback = interface;
  _CvsFilmstripPlaybackDisp = dispinterface;
  _CvsFilmstripResultsQueue = interface;
  _CvsFilmstripResultsQueueDisp = dispinterface;
  _CvsFilmstripResultsQueueOptions = interface;
  _CvsFilmstripResultsQueueOptionsDisp = dispinterface;
  _CvsFilmstripResultsQueueSettings = interface;
  _CvsFilmstripResultsQueueSettingsDisp = dispinterface;
  _CvsInSightDisplayEdit = interface;
  _CvsInSightDisplayEditDisp = dispinterface;
  _CvsInSightDisplayRecorder = interface;
  _CvsInSightDisplayRecorderDisp = dispinterface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                       
// (NOTE: Here we map each CoClass to its Default Interface)              
// *********************************************************************//
  CvsSensorFilmstripDisplay = ICvsSensorFilmstripDisplayInternal;
  CvsFilmstrip = ICvsFilmstripInternal;
  CvsPlaybackFilmstripDisplay = ICvsPlaybackFilmstripDisplayInternal;
  CvsInSightDisplay = ICvsInSightDisplayInternal;
  DynamicValue = _DynamicValue;
  DynamicValueComputed = _DynamicValueComputed;
  CvsFilmstripPlayback = _CvsFilmstripPlayback;
  CvsFilmstripResultsQueue = _CvsFilmstripResultsQueue;
  CvsFilmstripResultsQueueOptions = _CvsFilmstripResultsQueueOptions;
  CvsFilmstripResultsQueueSettings = _CvsFilmstripResultsQueueSettings;
  CvsInSightDisplayEdit = _CvsInSightDisplayEdit;
  CvsInSightDisplayRecorder = _CvsInSightDisplayRecorder;


// *********************************************************************//
// Interface: ICvsInSightDisplayInternal
// Flags:     (384) NonExtensible OleAutomation
// GUID:      {5B11005B-6F26-4213-8196-9BAAA93B5D44}
// *********************************************************************//
  ICvsInSightDisplayInternal = interface(IUnknown)
    ['{5B11005B-6F26-4213-8196-9BAAA93B5D44}']
    function AttachToWindow(parentHandle: Integer; out pRetVal: Integer): HResult; stdcall;
    function Get_InSight(out pRetVal: _CvsInSight): HResult; stdcall;
    function _Set_InSight(const pRetVal: _CvsInSight): HResult; stdcall;
    function Dispose: HResult; stdcall;
    function PreProcessMessage(msg: Integer; out pRetVal: WordBool): HResult; stdcall;
    function Get_BackColor(out pRetVal: OLE_COLOR): HResult; stdcall;
    function Set_BackColor(pRetVal: OLE_COLOR): HResult; stdcall;
    function Get_Enabled(out pRetVal: WordBool): HResult; stdcall;
    function Set_Enabled(pRetVal: WordBool): HResult; stdcall;
    function GetFont(out pRetVal: IUnknown): HResult; stdcall;
    function SetFont(const font: IUnknown): HResult; stdcall;
    function Get_Size(out pRetVal: Size): HResult; stdcall;
    function Set_Size(pRetVal: Size): HResult; stdcall;
    function Get_Action(out pRetVal: WideString): HResult; stdcall;
    function Set_Action(const pRetVal: WideString): HResult; stdcall;
    function Get_AutoConnectString(out pRetVal: WideString): HResult; stdcall;
    function Set_AutoConnectString(const pRetVal: WideString): HResult; stdcall;
    function Get_AutoReconnect(out pRetVal: WordBool): HResult; stdcall;
    function Set_AutoReconnect(pRetVal: WordBool): HResult; stdcall;
    function Get_ConfirmOnlineTransitions(out pRetVal: WordBool): HResult; stdcall;
    function Set_ConfirmOnlineTransitions(pRetVal: WordBool): HResult; stdcall;
    function Connect(const hostName: WideString; const userName: WideString; 
                     const password: WideString; forceConnect: WordBool): HResult; stdcall;
    function Connect_2(const hostName: WideString; const userName: WideString; 
                       const password: WideString; const sensor: IUnknown; forceConnect: WordBool): HResult; stdcall;
    function Get_Connected(out pRetVal: WordBool): HResult; stdcall;
    function Disconnect: HResult; stdcall;
    function Get_CurrentCell(out pRetVal: CvsCellLocation): HResult; stdcall;
    function Set_CurrentCell(pRetVal: CvsCellLocation): HResult; stdcall;
    function GhostMethod_ICvsInSightDisplayInternal_120_1: HResult; stdcall;
    function Get_CurrentCellExpression(out pRetVal: WideString): HResult; stdcall;
    function Set_CurrentCellExpression(const pRetVal: WideString): HResult; stdcall;
    function SetCurrentCell(row: Integer; column: Integer): HResult; stdcall;
    function Get_Edit(out pRetVal: _CvsInSightDisplayEdit): HResult; stdcall;
    function Get_FtpPort(out pRetVal: Integer): HResult; stdcall;
    function Set_FtpPort(pRetVal: Integer): HResult; stdcall;
    function Get_GridOffset(out pRetVal: CvsCellLocation): HResult; stdcall;
    function Set_GridOffset(pRetVal: CvsCellLocation): HResult; stdcall;
    function SetGridOffset(row: Integer; column: Integer): HResult; stdcall;
    function Get_GridOpacity(out pRetVal: Double): HResult; stdcall;
    function Set_GridOpacity(pRetVal: Double): HResult; stdcall;
    function Get_GridScale(out pRetVal: Double): HResult; stdcall;
    function Set_GridScale(pRetVal: Double): HResult; stdcall;
    function Get_ImageOffset(out pRetVal: PointF): HResult; stdcall;
    function Set_ImageOffset(pRetVal: PointF): HResult; stdcall;
    function SetImageScaleAndOffset(scale: Double; x: Single; y: Single): HResult; stdcall;
    function Get_ImageScale(out pRetVal: Double): HResult; stdcall;
    function Set_ImageScale(pRetVal: Double): HResult; stdcall;
    function Get_ImageZoomMode(out pRetVal: CvsDisplayZoom): HResult; stdcall;
    function Set_ImageZoomMode(pRetVal: CvsDisplayZoom): HResult; stdcall;
    function Get_ImageOrientation(out pRetVal: CvsImageOrientation): HResult; stdcall;
    function Set_ImageOrientation(pRetVal: CvsImageOrientation): HResult; stdcall;
    function EditCellGraphic(row: Integer; column: Integer): HResult; stdcall;
    function EnsureCellIsVisible(row: Integer; column: Integer): HResult; stdcall;
    function Get_Recorder(out pRetVal: _CvsInSightDisplayRecorder): HResult; stdcall;
    function _Set_Recorder(const pRetVal: _CvsInSightDisplayRecorder): HResult; stdcall;
    function Refresh: HResult; stdcall;
    function Get_Results(out pRetVal: _CvsResultSet): HResult; stdcall;
    function _Set_Results(const pRetVal: _CvsResultSet): HResult; stdcall;
    function Get_ScrollMode(out pRetVal: CvsDisplayScrollMode): HResult; stdcall;
    function Set_ScrollMode(pRetVal: CvsDisplayScrollMode): HResult; stdcall;
    function Get_SelectedRange(out pRetVal: CvsCellRange): HResult; stdcall;
    function Set_SelectedRange(pRetVal: CvsCellRange): HResult; stdcall;
    function SetSelectedRange(row: Integer; column: Integer; rows: Integer; columns: Integer): HResult; stdcall;
    function Get_ShiftPartialImage(out pRetVal: WordBool): HResult; stdcall;
    function Set_ShiftPartialImage(pRetVal: WordBool): HResult; stdcall;
    function Get_ShowCustomView(out pRetVal: WordBool): HResult; stdcall;
    function Set_ShowCustomView(pRetVal: WordBool): HResult; stdcall;
    function Get_ShowDependencyErrorsOnly(out pRetVal: WordBool): HResult; stdcall;
    function Set_ShowDependencyErrorsOnly(pRetVal: WordBool): HResult; stdcall;
    function Get_ShowGraphics(out pRetVal: WordBool): HResult; stdcall;
    function Set_ShowGraphics(pRetVal: WordBool): HResult; stdcall;
    function Get_ShowGrid(out pRetVal: WordBool): HResult; stdcall;
    function Set_ShowGrid(pRetVal: WordBool): HResult; stdcall;
    function Get_ShowImage(out pRetVal: WordBool): HResult; stdcall;
    function Set_ShowImage(pRetVal: WordBool): HResult; stdcall;
    function Get_ShowImageSaturation(out pRetVal: WordBool): HResult; stdcall;
    function Set_ShowImageSaturation(pRetVal: WordBool): HResult; stdcall;
    function Get_ShowScrollBars(out pRetVal: WordBool): HResult; stdcall;
    function Set_ShowScrollBars(pRetVal: WordBool): HResult; stdcall;
    function Get_SnippetPath(out pRetVal: WideString): HResult; stdcall;
    function Set_SnippetPath(const pRetVal: WideString): HResult; stdcall;
    function Get_SoftOnline(out pRetVal: WordBool): HResult; stdcall;
    function Set_SoftOnline(pRetVal: WordBool): HResult; stdcall;
    function Get_State(out pRetVal: CvsDisplayState): HResult; stdcall;
    function Get_StatusInformation(out pRetVal: WideString): HResult; stdcall;
    function Get_StatusInformationState(out pRetVal: CvsStatusInformationState): HResult; stdcall;
    function Get_MaxUserAccess(out pRetVal: CvsInSightSecurityAccess): HResult; stdcall;
    function Set_MaxUserAccess(pRetVal: CvsInSightSecurityAccess): HResult; stdcall;
    function GetCellBounds(row: Integer; column: Integer; rows: Integer; columns: Integer; 
                           out pRetVal: Rectangle): HResult; stdcall;
    function GetPicture(out pRetVal: IDispatch): HResult; stdcall;
    function SaveBitmap(const fileName: WideString): HResult; stdcall;
    function DrawTo(hdc: Integer): HResult; stdcall;
    function OpenPropertySheet(row: Integer; column: Integer; const expression: WideString): HResult; stdcall;
    function ClickSpreadsheetControl(row: Integer; column: Integer): HResult; stdcall;
  end;

// *********************************************************************//
// Interface: ICvsFilmstripInternal
// Flags:     (384) NonExtensible OleAutomation
// GUID:      {4B5440C3-37F9-488E-9A2A-BC682CB9F131}
// *********************************************************************//
  ICvsFilmstripInternal = interface(IUnknown)
    ['{4B5440C3-37F9-488E-9A2A-BC682CB9F131}']
    function AttachToWindow(parentHandle: Integer; out pRetVal: Integer): HResult; stdcall;
    function Dispose: HResult; stdcall;
    function PreProcessMessage(msg: Integer; out pRetVal: WordBool): HResult; stdcall;
    function Get_BackColor(out pRetVal: OLE_COLOR): HResult; stdcall;
    function Set_BackColor(pRetVal: OLE_COLOR): HResult; stdcall;
    function Get_Enabled(out pRetVal: WordBool): HResult; stdcall;
    function Set_Enabled(pRetVal: WordBool): HResult; stdcall;
    function Get_Size(out pRetVal: Size): HResult; stdcall;
    function Set_Size(pRetVal: Size): HResult; stdcall;
    function Get_FilmQueue(out pRetVal: ICvsFilmQueue): HResult; stdcall;
    function _Set_FilmQueue(const pRetVal: ICvsFilmQueue): HResult; stdcall;
    function Get_StatusLevelStyle(out pRetVal: CvsStatusLevelStyle): HResult; stdcall;
    function Set_StatusLevelStyle(pRetVal: CvsStatusLevelStyle): HResult; stdcall;
    function Get_ShowThumbnailImage(out pRetVal: WordBool): HResult; stdcall;
    function Set_ShowThumbnailImage(pRetVal: WordBool): HResult; stdcall;
    function Get_HeightScale(out pRetVal: CvsFilmstripScale): HResult; stdcall;
    function Set_HeightScale(pRetVal: CvsFilmstripScale): HResult; stdcall;
    function Get_MaxThumbnailsDisplayed(out pRetVal: Integer): HResult; stdcall;
    function Get_ThumbnailsDisplayed(out pRetVal: Integer): HResult; stdcall;
    function Get_ShowSummary(out pRetVal: WordBool): HResult; stdcall;
    function Set_ShowSummary(pRetVal: WordBool): HResult; stdcall;
  end;

// *********************************************************************//
// Interface: ICvsFilmQueue
// Flags:     (384) NonExtensible OleAutomation
// GUID:      {FFBF7CA5-FC20-4501-B67A-281D0F3858D0}
// *********************************************************************//
  ICvsFilmQueue = interface(IUnknown)
    ['{FFBF7CA5-FC20-4501-B67A-281D0F3858D0}']
    function Get_QueueSize(out pRetVal: Integer): HResult; stdcall;
    function Get_SelectedIndex(out pRetVal: Integer): HResult; stdcall;
    function Set_SelectedIndex(pRetVal: Integer): HResult; stdcall;
    function Get_SupportsEdit(out pRetVal: WordBool): HResult; stdcall;
    function Get_ThumbnailImages(out pRetVal: PSafeArray): HResult; stdcall;
    function Get_ResultsStatus(out pRetVal: PSafeArray): HResult; stdcall;
    function Get_Filmstrip(out pRetVal: ICvsFilmstripInternal): HResult; stdcall;
    function _Set_Filmstrip(const pRetVal: ICvsFilmstripInternal): HResult; stdcall;
    function Get_SelectFirst(out pRetVal: ICvsAction): HResult; stdcall;
    function Get_SelectLast(out pRetVal: ICvsAction): HResult; stdcall;
    function Get_SelectNext(out pRetVal: ICvsAction): HResult; stdcall;
    function Get_SelectPrevious(out pRetVal: ICvsAction): HResult; stdcall;
    function Get_SelectNextPage(out pRetVal: ICvsAction): HResult; stdcall;
    function Get_SelectPrevPage(out pRetVal: ICvsAction): HResult; stdcall;
    function Get_ClearQueue(out pRetVal: ICvsAction): HResult; stdcall;
    function Get_SaveQueue(out pRetVal: ICvsAction): HResult; stdcall;
    function Get_DeleteSelectedItem(out pRetVal: ICvsAction): HResult; stdcall;
    function Get_Enabled(out pRetVal: WordBool): HResult; stdcall;
    function Set_Enabled(pRetVal: WordBool): HResult; stdcall;
    function RequestThumbnailImagesUpdate(index: Integer; count: Integer; imageIndex: Integer): HResult; stdcall;
  end;

// *********************************************************************//
// DispIntf:  ICvsInSightDisplayEvents
// Flags:     (4752) Hidden NonExtensible Restricted Dispatchable
// GUID:      {5B11335B-6F26-4213-8196-9BAAA9371744}
// *********************************************************************//
  ICvsInSightDisplayEvents = dispinterface
    ['{5B11335B-6F26-4213-8196-9BAAA9371744}']
    procedure ConnectedChanged(sender: OleVariant; const e: _EventArgs); dispid 114;
    procedure ConnectCompleted(sender: OleVariant; const e: _CvsConnectCompletedEventArgs); dispid 116;
    procedure CurrentCellChanged(sender: OleVariant; const e: _EventArgs); dispid 100;
    procedure CurrentCellExpressionChanged(sender: OleVariant; const e: _EventArgs); dispid 115;
    procedure FontChanged(sender: OleVariant; const e: _EventArgs); dispid 101;
    procedure GridOffsetChanged(sender: OleVariant; const e: _EventArgs); dispid 102;
    procedure GridOpacityChanged(sender: OleVariant; const e: _EventArgs); dispid 103;
    procedure GridScaleChanged(sender: OleVariant; const e: _EventArgs); dispid 104;
    procedure ImageOffsetChanged(sender: OleVariant; const e: _EventArgs); dispid 105;
    procedure ImageScaleChanged(sender: OleVariant; const e: _EventArgs); dispid 106;
    procedure ImageOrientationChanged(sender: OleVariant; const e: _EventArgs); dispid 117;
    procedure InSightChanged(sender: OleVariant; const e: _EventArgs); dispid 107;
    procedure Invalidated(sender: OleVariant; const e: IUnknown); dispid 108;
    procedure ResultsChanged(sender: OleVariant; const e: _EventArgs); dispid 109;
    procedure ScrollModeChanged(sender: OleVariant; const e: _EventArgs); dispid 110;
    procedure SelectedRangeChanged(sender: OleVariant; const e: _EventArgs); dispid 111;
    procedure StateChanged(sender: OleVariant; const e: _EventArgs); dispid 112;
    procedure StatusInformationChanged(sender: OleVariant; const e: _EventArgs); dispid 113;
  end;

// *********************************************************************//
// Interface: _DynamicValue
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {77B7165D-C7E5-35E1-83D3-D2741EA109CC}
// *********************************************************************//
  _DynamicValue = interface(IDispatch)
    ['{77B7165D-C7E5-35E1-83D3-D2741EA109CC}']
  end;

// *********************************************************************//
// DispIntf:  _DynamicValueDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {77B7165D-C7E5-35E1-83D3-D2741EA109CC}
// *********************************************************************//
  _DynamicValueDisp = dispinterface
    ['{77B7165D-C7E5-35E1-83D3-D2741EA109CC}']
  end;

// *********************************************************************//
// Interface: _DynamicValueComputed
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {3631182E-0485-3D67-B6E7-CA2E2712D879}
// *********************************************************************//
  _DynamicValueComputed = interface(IDispatch)
    ['{3631182E-0485-3D67-B6E7-CA2E2712D879}']
  end;

// *********************************************************************//
// DispIntf:  _DynamicValueComputedDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {3631182E-0485-3D67-B6E7-CA2E2712D879}
// *********************************************************************//
  _DynamicValueComputedDisp = dispinterface
    ['{3631182E-0485-3D67-B6E7-CA2E2712D879}']
  end;

// *********************************************************************//
// Interface: ICvsPlaybackFilmstripDisplayInternal
// Flags:     (384) NonExtensible OleAutomation
// GUID:      {93882184-435F-4F65-95E9-5C81DC5DDFAD}
// *********************************************************************//
  ICvsPlaybackFilmstripDisplayInternal = interface(IUnknown)
    ['{93882184-435F-4F65-95E9-5C81DC5DDFAD}']
    function Get_BackColor(out pRetVal: OLE_COLOR): HResult; stdcall;
    function Set_BackColor(pRetVal: OLE_COLOR): HResult; stdcall;
    function Get_Enabled(out pRetVal: WordBool): HResult; stdcall;
    function Set_Enabled(pRetVal: WordBool): HResult; stdcall;
    function GetFont(out pRetVal: IUnknown): HResult; stdcall;
    function SetFont(const font: IUnknown): HResult; stdcall;
    function Get_Size(out pRetVal: Size): HResult; stdcall;
    function Set_Size(pRetVal: Size): HResult; stdcall;
    function AttachToWindow(parentHandle: Integer; out pRetVal: Integer): HResult; stdcall;
    function Dispose: HResult; stdcall;
    function PreProcessMessage(msg: Integer; out pRetVal: WordBool): HResult; stdcall;
    function Get_InSight(out pRetVal: _CvsInSight): HResult; stdcall;
    function _Set_InSight(const pRetVal: _CvsInSight): HResult; stdcall;
    function Get_Action(out pRetVal: WideString): HResult; stdcall;
    function Set_Action(const pRetVal: WideString): HResult; stdcall;
    function Get_AutoConnectString(out pRetVal: WideString): HResult; stdcall;
    function Set_AutoConnectString(const pRetVal: WideString): HResult; stdcall;
    function Get_AutoReconnect(out pRetVal: WordBool): HResult; stdcall;
    function Set_AutoReconnect(pRetVal: WordBool): HResult; stdcall;
    function Get_ConfirmOnlineTransitions(out pRetVal: WordBool): HResult; stdcall;
    function Set_ConfirmOnlineTransitions(pRetVal: WordBool): HResult; stdcall;
    function Connect(const hostName: WideString; const userName: WideString; 
                     const password: WideString; forceConnect: WordBool): HResult; stdcall;
    function Connect_2(const hostName: WideString; const userName: WideString; 
                       const password: WideString; const sensor: IUnknown; forceConnect: WordBool): HResult; stdcall;
    function Get_Connected(out pRetVal: WordBool): HResult; stdcall;
    function Disconnect: HResult; stdcall;
    function Get_CurrentCell(out pRetVal: CvsCellLocation): HResult; stdcall;
    function Set_CurrentCell(pRetVal: CvsCellLocation): HResult; stdcall;
    function Get_CurrentCellExpression(out pRetVal: WideString): HResult; stdcall;
    function Set_CurrentCellExpression(const pRetVal: WideString): HResult; stdcall;
    function SetCurrentCell(row: Integer; column: Integer): HResult; stdcall;
    function Get_Edit(out pRetVal: _CvsInSightDisplayEdit): HResult; stdcall;
    function Get_FtpPort(out pRetVal: Integer): HResult; stdcall;
    function Set_FtpPort(pRetVal: Integer): HResult; stdcall;
    function Get_GridOffset(out pRetVal: CvsCellLocation): HResult; stdcall;
    function Set_GridOffset(pRetVal: CvsCellLocation): HResult; stdcall;
    function SetGridOffset(row: Integer; column: Integer): HResult; stdcall;
    function Get_GridOpacity(out pRetVal: Double): HResult; stdcall;
    function Set_GridOpacity(pRetVal: Double): HResult; stdcall;
    function Get_GridScale(out pRetVal: Double): HResult; stdcall;
    function Set_GridScale(pRetVal: Double): HResult; stdcall;
    function Get_ImageOffset(out pRetVal: PointF): HResult; stdcall;
    function Set_ImageOffset(pRetVal: PointF): HResult; stdcall;
    function SetImageScaleAndOffset(scale: Double; x: Single; y: Single): HResult; stdcall;
    function Get_ImageScale(out pRetVal: Double): HResult; stdcall;
    function Set_ImageScale(pRetVal: Double): HResult; stdcall;
    function Get_ImageZoomMode(out pRetVal: CvsDisplayZoom): HResult; stdcall;
    function Set_ImageZoomMode(pRetVal: CvsDisplayZoom): HResult; stdcall;
    function Get_ImageOrientation(out pRetVal: CvsImageOrientation): HResult; stdcall;
    function Set_ImageOrientation(pRetVal: CvsImageOrientation): HResult; stdcall;
    function EditCellGraphic(row: Integer; column: Integer): HResult; stdcall;
    function EnsureCellIsVisible(row: Integer; column: Integer): HResult; stdcall;
    function Get_Recorder(out pRetVal: _CvsInSightDisplayRecorder): HResult; stdcall;
    function _Set_Recorder(const pRetVal: _CvsInSightDisplayRecorder): HResult; stdcall;
    function Refresh: HResult; stdcall;
    function Get_Results(out pRetVal: _CvsResultSet): HResult; stdcall;
    function _Set_Results(const pRetVal: _CvsResultSet): HResult; stdcall;
    function Get_ScrollMode(out pRetVal: CvsDisplayScrollMode): HResult; stdcall;
    function Set_ScrollMode(pRetVal: CvsDisplayScrollMode): HResult; stdcall;
    function Get_SelectedRange(out pRetVal: CvsCellRange): HResult; stdcall;
    function Set_SelectedRange(pRetVal: CvsCellRange): HResult; stdcall;
    function SetSelectedRange(row: Integer; column: Integer; rows: Integer; columns: Integer): HResult; stdcall;
    function Get_ShiftPartialImage(out pRetVal: WordBool): HResult; stdcall;
    function Set_ShiftPartialImage(pRetVal: WordBool): HResult; stdcall;
    function Get_ShowCustomView(out pRetVal: WordBool): HResult; stdcall;
    function Set_ShowCustomView(pRetVal: WordBool): HResult; stdcall;
    function Get_ShowDependencyErrorsOnly(out pRetVal: WordBool): HResult; stdcall;
    function Set_ShowDependencyErrorsOnly(pRetVal: WordBool): HResult; stdcall;
    function Get_ShowGraphics(out pRetVal: WordBool): HResult; stdcall;
    function Set_ShowGraphics(pRetVal: WordBool): HResult; stdcall;
    function Get_ShowGrid(out pRetVal: WordBool): HResult; stdcall;
    function Set_ShowGrid(pRetVal: WordBool): HResult; stdcall;
    function Get_ShowImage(out pRetVal: WordBool): HResult; stdcall;
    function Set_ShowImage(pRetVal: WordBool): HResult; stdcall;
    function Get_ShowImageSaturation(out pRetVal: WordBool): HResult; stdcall;
    function Set_ShowImageSaturation(pRetVal: WordBool): HResult; stdcall;
    function Get_ShowScrollBars(out pRetVal: WordBool): HResult; stdcall;
    function Set_ShowScrollBars(pRetVal: WordBool): HResult; stdcall;
    function Get_SnippetPath(out pRetVal: WideString): HResult; stdcall;
    function Set_SnippetPath(const pRetVal: WideString): HResult; stdcall;
    function Get_SoftOnline(out pRetVal: WordBool): HResult; stdcall;
    function Set_SoftOnline(pRetVal: WordBool): HResult; stdcall;
    function Get_State(out pRetVal: CvsDisplayState): HResult; stdcall;
    function Get_StatusInformation(out pRetVal: WideString): HResult; stdcall;
    function Get_StatusInformationState(out pRetVal: CvsStatusInformationState): HResult; stdcall;
    function Get_MaxUserAccess(out pRetVal: CvsInSightSecurityAccess): HResult; stdcall;
    function Set_MaxUserAccess(pRetVal: CvsInSightSecurityAccess): HResult; stdcall;
    function GetCellBounds(row: Integer; column: Integer; rows: Integer; columns: Integer; 
                           out pRetVal: Rectangle): HResult; stdcall;
    function GetPicture(out pRetVal: IDispatch): HResult; stdcall;
    function SaveBitmap(const fileName: WideString): HResult; stdcall;
    function DrawTo(hdc: Integer): HResult; stdcall;
    function OpenPropertySheet(row: Integer; column: Integer; const expression: WideString): HResult; stdcall;
    function ClickSpreadsheetControl(row: Integer; column: Integer): HResult; stdcall;
    function Get_FilmQueue(out pRetVal: ICvsFilmQueue): HResult; stdcall;
    function _Set_FilmQueue(const pRetVal: ICvsFilmQueue): HResult; stdcall;
    function Get_StatusLevelStyle(out pRetVal: CvsStatusLevelStyle): HResult; stdcall;
    function Set_StatusLevelStyle(pRetVal: CvsStatusLevelStyle): HResult; stdcall;
    function Get_ShowThumbnailImage(out pRetVal: WordBool): HResult; stdcall;
    function Set_ShowThumbnailImage(pRetVal: WordBool): HResult; stdcall;
    function Get_HeightScale(out pRetVal: CvsFilmstripScale): HResult; stdcall;
    function Set_HeightScale(pRetVal: CvsFilmstripScale): HResult; stdcall;
    function Get_MaxThumbnailsDisplayed(out pRetVal: Integer): HResult; stdcall;
    function Get_ThumbnailsDisplayed(out pRetVal: Integer): HResult; stdcall;
    function Get_ShowSummary(out pRetVal: WordBool): HResult; stdcall;
    function Set_ShowSummary(pRetVal: WordBool): HResult; stdcall;
    function Get_SplitterHeight(out pRetVal: Integer): HResult; stdcall;
    function Set_SplitterHeight(pRetVal: Integer): HResult; stdcall;
    function Get_FilmstripHeight(out pRetVal: Integer): HResult; stdcall;
    function Set_FilmstripHeight(pRetVal: Integer): HResult; stdcall;
    function Get_IsSplitterFixed(out pRetVal: WordBool): HResult; stdcall;
    function Set_IsSplitterFixed(pRetVal: WordBool): HResult; stdcall;
    function Get_RecordActive(out pRetVal: WordBool): HResult; stdcall;
    function Set_RecordActive(pRetVal: WordBool): HResult; stdcall;
    function Get_RecordFilename(out pRetVal: WideString): HResult; stdcall;
    function Get_RecordFilenameFormat(out pRetVal: WideString): HResult; stdcall;
    function Set_RecordFilenameFormat(const pRetVal: WideString): HResult; stdcall;
    function Get_RecordFolder(out pRetVal: WideString): HResult; stdcall;
    function Set_RecordFolder(const pRetVal: WideString): HResult; stdcall;
    function Get_RecordImageCount(out pRetVal: Integer): HResult; stdcall;
    function Get_RecordIndex(out pRetVal: Integer): HResult; stdcall;
    function Set_RecordIndex(pRetVal: Integer): HResult; stdcall;
    function Get_RecordMax(out pRetVal: Integer): HResult; stdcall;
    function Set_RecordMax(pRetVal: Integer): HResult; stdcall;
    function Get_RecordMode(out pRetVal: CvsRecordMode): HResult; stdcall;
    function Set_RecordMode(pRetVal: CvsRecordMode): HResult; stdcall;
    function Get_RecordResolution(out pRetVal: CvsImageResolution): HResult; stdcall;
    function Set_RecordResolution(pRetVal: CvsImageResolution): HResult; stdcall;
    function Get_RecordBitDepth(out pRetVal: CvsColorBitDepth): HResult; stdcall;
    function Set_RecordBitDepth(pRetVal: CvsColorBitDepth): HResult; stdcall;
    function Get_PlaybackAllowed(out pRetVal: WordBool): HResult; stdcall;
    function Get_PlayActive(out pRetVal: WordBool): HResult; stdcall;
    function Set_PlayActive(pRetVal: WordBool): HResult; stdcall;
    function Get_PlaybackContinuous(out pRetVal: WordBool): HResult; stdcall;
    function Set_PlaybackContinuous(pRetVal: WordBool): HResult; stdcall;
    function Get_PlaybackDelay(out pRetVal: Double): HResult; stdcall;
    function Set_PlaybackDelay(pRetVal: Double): HResult; stdcall;
    function Get_PlaybackFilename(out pRetVal: WideString): HResult; stdcall;
    function Set_PlaybackFilename(const pRetVal: WideString): HResult; stdcall;
    function Get_PlaybackFolder(out pRetVal: WideString): HResult; stdcall;
    function Set_PlaybackFolder(const pRetVal: WideString): HResult; stdcall;
    function Get_PlaybackImageCount(out pRetVal: Integer): HResult; stdcall;
    function Get_PlaybackIndex(out pRetVal: Integer): HResult; stdcall;
    function Set_PlaybackIndex(pRetVal: Integer): HResult; stdcall;
    function GetPlaybackFiles(out pRetVal: PSafeArray): HResult; stdcall;
    function PlayFirst: HResult; stdcall;
    function PlayPrevious: HResult; stdcall;
    function PlayNext: HResult; stdcall;
    function PlayLast: HResult; stdcall;
  end;

// *********************************************************************//
// DispIntf:  ICvsFilmstripEvents
// Flags:     (4752) Hidden NonExtensible Restricted Dispatchable
// GUID:      {E4C82C6E-587E-4B91-A0FF-C3BA3348D9C8}
// *********************************************************************//
  ICvsFilmstripEvents = dispinterface
    ['{E4C82C6E-587E-4B91-A0FF-C3BA3348D9C8}']
  end;

// *********************************************************************//
// Interface: ICvsSensorFilmstripDisplayInternal
// Flags:     (384) NonExtensible OleAutomation
// GUID:      {82CA4E4B-DE49-4169-BF2F-A5378B6AE8D3}
// *********************************************************************//
  ICvsSensorFilmstripDisplayInternal = interface(IUnknown)
    ['{82CA4E4B-DE49-4169-BF2F-A5378B6AE8D3}']
    function Get_BackColor(out pRetVal: OLE_COLOR): HResult; stdcall;
    function Set_BackColor(pRetVal: OLE_COLOR): HResult; stdcall;
    function Get_Enabled(out pRetVal: WordBool): HResult; stdcall;
    function Set_Enabled(pRetVal: WordBool): HResult; stdcall;
    function GetFont(out pRetVal: IUnknown): HResult; stdcall;
    function SetFont(const font: IUnknown): HResult; stdcall;
    function Get_Size(out pRetVal: Size): HResult; stdcall;
    function Set_Size(pRetVal: Size): HResult; stdcall;
    function AttachToWindow(parentHandle: Integer; out pRetVal: Integer): HResult; stdcall;
    function Dispose: HResult; stdcall;
    function PreProcessMessage(msg: Integer; out pRetVal: WordBool): HResult; stdcall;
    function Get_InSight(out pRetVal: _CvsInSight): HResult; stdcall;
    function _Set_InSight(const pRetVal: _CvsInSight): HResult; stdcall;
    function Get_Action(out pRetVal: WideString): HResult; stdcall;
    function Set_Action(const pRetVal: WideString): HResult; stdcall;
    function Get_AutoConnectString(out pRetVal: WideString): HResult; stdcall;
    function Set_AutoConnectString(const pRetVal: WideString): HResult; stdcall;
    function Get_AutoReconnect(out pRetVal: WordBool): HResult; stdcall;
    function Set_AutoReconnect(pRetVal: WordBool): HResult; stdcall;
    function Get_ConfirmOnlineTransitions(out pRetVal: WordBool): HResult; stdcall;
    function Set_ConfirmOnlineTransitions(pRetVal: WordBool): HResult; stdcall;
    function Connect(const hostName: WideString; const userName: WideString; 
                     const password: WideString; forceConnect: WordBool): HResult; stdcall;
    function Connect_2(const hostName: WideString; const userName: WideString; 
                       const password: WideString; const sensor: IUnknown; forceConnect: WordBool): HResult; stdcall;
    function Get_Connected(out pRetVal: WordBool): HResult; stdcall;
    function Disconnect: HResult; stdcall;
    function Get_CurrentCell(out pRetVal: CvsCellLocation): HResult; stdcall;
    function Set_CurrentCell(pRetVal: CvsCellLocation): HResult; stdcall;
    function Get_CurrentCellExpression(out pRetVal: WideString): HResult; stdcall;
    function Set_CurrentCellExpression(const pRetVal: WideString): HResult; stdcall;
    function SetCurrentCell(row: Integer; column: Integer): HResult; stdcall;
    function Get_Edit(out pRetVal: _CvsInSightDisplayEdit): HResult; stdcall;
    function Get_FtpPort(out pRetVal: Integer): HResult; stdcall;
    function Set_FtpPort(pRetVal: Integer): HResult; stdcall;
    function Get_GridOffset(out pRetVal: CvsCellLocation): HResult; stdcall;
    function Set_GridOffset(pRetVal: CvsCellLocation): HResult; stdcall;
    function SetGridOffset(row: Integer; column: Integer): HResult; stdcall;
    function Get_GridOpacity(out pRetVal: Double): HResult; stdcall;
    function Set_GridOpacity(pRetVal: Double): HResult; stdcall;
    function Get_GridScale(out pRetVal: Double): HResult; stdcall;
    function Set_GridScale(pRetVal: Double): HResult; stdcall;
    function Get_ImageOffset(out pRetVal: PointF): HResult; stdcall;
    function Set_ImageOffset(pRetVal: PointF): HResult; stdcall;
    function SetImageScaleAndOffset(scale: Double; x: Single; y: Single): HResult; stdcall;
    function Get_ImageScale(out pRetVal: Double): HResult; stdcall;
    function Set_ImageScale(pRetVal: Double): HResult; stdcall;
    function Get_ImageZoomMode(out pRetVal: CvsDisplayZoom): HResult; stdcall;
    function Set_ImageZoomMode(pRetVal: CvsDisplayZoom): HResult; stdcall;
    function Get_ImageOrientation(out pRetVal: CvsImageOrientation): HResult; stdcall;
    function Set_ImageOrientation(pRetVal: CvsImageOrientation): HResult; stdcall;
    function EditCellGraphic(row: Integer; column: Integer): HResult; stdcall;
    function EnsureCellIsVisible(row: Integer; column: Integer): HResult; stdcall;
    function Get_Recorder(out pRetVal: _CvsInSightDisplayRecorder): HResult; stdcall;
    function _Set_Recorder(const pRetVal: _CvsInSightDisplayRecorder): HResult; stdcall;
    function Refresh: HResult; stdcall;
    function Get_Results(out pRetVal: _CvsResultSet): HResult; stdcall;
    function _Set_Results(const pRetVal: _CvsResultSet): HResult; stdcall;
    function Get_ScrollMode(out pRetVal: CvsDisplayScrollMode): HResult; stdcall;
    function Set_ScrollMode(pRetVal: CvsDisplayScrollMode): HResult; stdcall;
    function Get_SelectedRange(out pRetVal: CvsCellRange): HResult; stdcall;
    function Set_SelectedRange(pRetVal: CvsCellRange): HResult; stdcall;
    function SetSelectedRange(row: Integer; column: Integer; rows: Integer; columns: Integer): HResult; stdcall;
    function Get_ShiftPartialImage(out pRetVal: WordBool): HResult; stdcall;
    function Set_ShiftPartialImage(pRetVal: WordBool): HResult; stdcall;
    function Get_ShowCustomView(out pRetVal: WordBool): HResult; stdcall;
    function Set_ShowCustomView(pRetVal: WordBool): HResult; stdcall;
    function Get_ShowDependencyErrorsOnly(out pRetVal: WordBool): HResult; stdcall;
    function Set_ShowDependencyErrorsOnly(pRetVal: WordBool): HResult; stdcall;
    function Get_ShowGraphics(out pRetVal: WordBool): HResult; stdcall;
    function Set_ShowGraphics(pRetVal: WordBool): HResult; stdcall;
    function Get_ShowGrid(out pRetVal: WordBool): HResult; stdcall;
    function Set_ShowGrid(pRetVal: WordBool): HResult; stdcall;
    function Get_ShowImage(out pRetVal: WordBool): HResult; stdcall;
    function Set_ShowImage(pRetVal: WordBool): HResult; stdcall;
    function Get_ShowImageSaturation(out pRetVal: WordBool): HResult; stdcall;
    function Set_ShowImageSaturation(pRetVal: WordBool): HResult; stdcall;
    function Get_ShowScrollBars(out pRetVal: WordBool): HResult; stdcall;
    function Set_ShowScrollBars(pRetVal: WordBool): HResult; stdcall;
    function Get_SnippetPath(out pRetVal: WideString): HResult; stdcall;
    function Set_SnippetPath(const pRetVal: WideString): HResult; stdcall;
    function Get_SoftOnline(out pRetVal: WordBool): HResult; stdcall;
    function Set_SoftOnline(pRetVal: WordBool): HResult; stdcall;
    function Get_State(out pRetVal: CvsDisplayState): HResult; stdcall;
    function Get_StatusInformation(out pRetVal: WideString): HResult; stdcall;
    function Get_StatusInformationState(out pRetVal: CvsStatusInformationState): HResult; stdcall;
    function Get_MaxUserAccess(out pRetVal: CvsInSightSecurityAccess): HResult; stdcall;
    function Set_MaxUserAccess(pRetVal: CvsInSightSecurityAccess): HResult; stdcall;
    function GetCellBounds(row: Integer; column: Integer; rows: Integer; columns: Integer; 
                           out pRetVal: Rectangle): HResult; stdcall;
    function GetPicture(out pRetVal: IDispatch): HResult; stdcall;
    function SaveBitmap(const fileName: WideString): HResult; stdcall;
    function DrawTo(hdc: Integer): HResult; stdcall;
    function OpenPropertySheet(row: Integer; column: Integer; const expression: WideString): HResult; stdcall;
    function ClickSpreadsheetControl(row: Integer; column: Integer): HResult; stdcall;
    function Get_FilmQueue(out pRetVal: ICvsFilmQueue): HResult; stdcall;
    function _Set_FilmQueue(const pRetVal: ICvsFilmQueue): HResult; stdcall;
    function Get_StatusLevelStyle(out pRetVal: CvsStatusLevelStyle): HResult; stdcall;
    function Set_StatusLevelStyle(pRetVal: CvsStatusLevelStyle): HResult; stdcall;
    function Get_ShowThumbnailImage(out pRetVal: WordBool): HResult; stdcall;
    function Set_ShowThumbnailImage(pRetVal: WordBool): HResult; stdcall;
    function Get_HeightScale(out pRetVal: CvsFilmstripScale): HResult; stdcall;
    function Set_HeightScale(pRetVal: CvsFilmstripScale): HResult; stdcall;
    function Get_MaxThumbnailsDisplayed(out pRetVal: Integer): HResult; stdcall;
    function Get_ThumbnailsDisplayed(out pRetVal: Integer): HResult; stdcall;
    function Get_ShowSummary(out pRetVal: WordBool): HResult; stdcall;
    function Set_ShowSummary(pRetVal: WordBool): HResult; stdcall;
    function Get_SplitterHeight(out pRetVal: Integer): HResult; stdcall;
    function Set_SplitterHeight(pRetVal: Integer): HResult; stdcall;
    function Get_FilmstripHeight(out pRetVal: Integer): HResult; stdcall;
    function Set_FilmstripHeight(pRetVal: Integer): HResult; stdcall;
    function Get_IsSplitterFixed(out pRetVal: WordBool): HResult; stdcall;
    function Set_IsSplitterFixed(pRetVal: WordBool): HResult; stdcall;
    function Get_Options(out pRetVal: _CvsFilmstripResultsQueueOptions): HResult; stdcall;
    function _Set_Options(const pRetVal: _CvsFilmstripResultsQueueOptions): HResult; stdcall;
    function Get_FreezeQueue(out pRetVal: ICvsActionToggle): HResult; stdcall;
    function Get_Settings(out pRetVal: _CvsFilmstripResultsQueueSettings): HResult; stdcall;
  end;

// *********************************************************************//
// Interface: ICvsFilmstripImage
// Flags:     (4288) Dual NonExtensible Dispatchable
// GUID:      {C1A669E9-DD1C-4EF2-9D9A-96356C352E58}
// *********************************************************************//
  ICvsFilmstripImage = interface(IDispatch)
    ['{C1A669E9-DD1C-4EF2-9D9A-96356C352E58}']
    function Get_Size: Size; safecall;
    procedure GhostMethod_ICvsFilmstripImage_32_1; safecall;
    function Get_Width: Integer; safecall;
    function Get_Height: Integer; safecall;
    procedure GhostMethod_ICvsFilmstripImage_44_2; safecall;
    function ToBitmap: _Bitmap; safecall;
    property Size: Size read Get_Size;
    property Width: Integer read Get_Width;
    property Height: Integer read Get_Height;
  end;

// *********************************************************************//
// DispIntf:  ICvsFilmstripImageDisp
// Flags:     (4288) Dual NonExtensible Dispatchable
// GUID:      {C1A669E9-DD1C-4EF2-9D9A-96356C352E58}
// *********************************************************************//
  ICvsFilmstripImageDisp = dispinterface
    ['{C1A669E9-DD1C-4EF2-9D9A-96356C352E58}']
    property Size: {??Size}OleVariant readonly dispid 1610743808;
    procedure GhostMethod_ICvsFilmstripImage_32_1; dispid 1610743809;
    property Width: Integer readonly dispid 1610743810;
    property Height: Integer readonly dispid 1610743811;
    procedure GhostMethod_ICvsFilmstripImage_44_2; dispid 1610743812;
    function ToBitmap: _Bitmap; dispid 1610743813;
  end;

// *********************************************************************//
// Interface: ICvsFilmstripResultStatus
// Flags:     (4288) Dual NonExtensible Dispatchable
// GUID:      {FFED5642-E098-4D66-85BB-3665AD737651}
// *********************************************************************//
  ICvsFilmstripResultStatus = interface(IDispatch)
    ['{FFED5642-E098-4D66-85BB-3665AD737651}']
    function Get_StatusLevel: CvsStatusLevel; safecall;
    function Get_StatusMessage: WideString; safecall;
    function Get_SequenceNumber: Int64; safecall;
    function Get_Timestamp: TDateTime; safecall;
    property StatusLevel: CvsStatusLevel read Get_StatusLevel;
    property StatusMessage: WideString read Get_StatusMessage;
    property SequenceNumber: Int64 read Get_SequenceNumber;
    property Timestamp: TDateTime read Get_Timestamp;
  end;

// *********************************************************************//
// DispIntf:  ICvsFilmstripResultStatusDisp
// Flags:     (4288) Dual NonExtensible Dispatchable
// GUID:      {FFED5642-E098-4D66-85BB-3665AD737651}
// *********************************************************************//
  ICvsFilmstripResultStatusDisp = dispinterface
    ['{FFED5642-E098-4D66-85BB-3665AD737651}']
    property StatusLevel: CvsStatusLevel readonly dispid 1610743808;
    property StatusMessage: WideString readonly dispid 1610743809;
    property SequenceNumber: {??Int64}OleVariant readonly dispid 1610743810;
    property Timestamp: TDateTime readonly dispid 1610743811;
  end;

// *********************************************************************//
// Interface: _CvsFilmstripPlayback
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {9B9FDBA0-AE22-3ECD-8785-51751871C1E1}
// *********************************************************************//
  _CvsFilmstripPlayback = interface(IDispatch)
    ['{9B9FDBA0-AE22-3ECD-8785-51751871C1E1}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Get_QueueSize: Integer; safecall;
    function Get_SelectedIndex: Integer; safecall;
    procedure Set_SelectedIndex(pRetVal: Integer); safecall;
    function Get_SupportsEdit: WordBool; safecall;
    function Get_ThumbnailImages: PSafeArray; safecall;
    function Get_ResultsStatus: PSafeArray; safecall;
    function Get_Filmstrip: ICvsFilmstripInternal; safecall;
    procedure _Set_Filmstrip(const pRetVal: ICvsFilmstripInternal); safecall;
    function Get_SelectFirst: ICvsAction; safecall;
    function Get_SelectLast: ICvsAction; safecall;
    function Get_SelectNext: ICvsAction; safecall;
    function Get_SelectPrevious: ICvsAction; safecall;
    function Get_SelectNextPage: ICvsAction; safecall;
    function Get_SelectPrevPage: ICvsAction; safecall;
    function Get_ClearQueue: ICvsAction; safecall;
    function Get_SaveQueue: ICvsAction; safecall;
    function Get_DeleteSelectedItem: ICvsAction; safecall;
    function Get_Enabled: WordBool; safecall;
    procedure Set_Enabled(pRetVal: WordBool); safecall;
    procedure RequestThumbnailImagesUpdate(index: Integer; count: Integer; imageIndex: Integer); safecall;
    procedure Dispose; safecall;
    function Get_Recorder: _CvsInSightDisplayRecorder; safecall;
    procedure _Set_Recorder(const pRetVal: _CvsInSightDisplayRecorder); safecall;
    procedure Connect; safecall;
    procedure Disconnect; safecall;
    function Get_Play: ICvsActionToggle; safecall;
    procedure ProcessShortcutKeys(const e: _KeyEventArgs); safecall;
    property ToString: WideString read Get_ToString;
    property QueueSize: Integer read Get_QueueSize;
    property SelectedIndex: Integer read Get_SelectedIndex write Set_SelectedIndex;
    property SupportsEdit: WordBool read Get_SupportsEdit;
    property ThumbnailImages: PSafeArray read Get_ThumbnailImages;
    property ResultsStatus: PSafeArray read Get_ResultsStatus;
    property Filmstrip: ICvsFilmstripInternal read Get_Filmstrip write _Set_Filmstrip;
    property SelectFirst: ICvsAction read Get_SelectFirst;
    property SelectLast: ICvsAction read Get_SelectLast;
    property SelectNext: ICvsAction read Get_SelectNext;
    property SelectPrevious: ICvsAction read Get_SelectPrevious;
    property SelectNextPage: ICvsAction read Get_SelectNextPage;
    property SelectPrevPage: ICvsAction read Get_SelectPrevPage;
    property ClearQueue: ICvsAction read Get_ClearQueue;
    property SaveQueue: ICvsAction read Get_SaveQueue;
    property DeleteSelectedItem: ICvsAction read Get_DeleteSelectedItem;
    property Enabled: WordBool read Get_Enabled write Set_Enabled;
    property Recorder: _CvsInSightDisplayRecorder read Get_Recorder write _Set_Recorder;
    property Play: ICvsActionToggle read Get_Play;
  end;

// *********************************************************************//
// DispIntf:  _CvsFilmstripPlaybackDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {9B9FDBA0-AE22-3ECD-8785-51751871C1E1}
// *********************************************************************//
  _CvsFilmstripPlaybackDisp = dispinterface
    ['{9B9FDBA0-AE22-3ECD-8785-51751871C1E1}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    property QueueSize: Integer readonly dispid 1610743812;
    property SelectedIndex: Integer dispid 1610743813;
    property SupportsEdit: WordBool readonly dispid 1610743815;
    property ThumbnailImages: {??PSafeArray}OleVariant readonly dispid 1610743816;
    property ResultsStatus: {??PSafeArray}OleVariant readonly dispid 1610743817;
    property Filmstrip: ICvsFilmstripInternal dispid 1610743818;
    property SelectFirst: ICvsAction readonly dispid 1610743820;
    property SelectLast: ICvsAction readonly dispid 1610743821;
    property SelectNext: ICvsAction readonly dispid 1610743822;
    property SelectPrevious: ICvsAction readonly dispid 1610743823;
    property SelectNextPage: ICvsAction readonly dispid 1610743824;
    property SelectPrevPage: ICvsAction readonly dispid 1610743825;
    property ClearQueue: ICvsAction readonly dispid 1610743826;
    property SaveQueue: ICvsAction readonly dispid 1610743827;
    property DeleteSelectedItem: ICvsAction readonly dispid 1610743828;
    property Enabled: WordBool dispid 1610743829;
    procedure RequestThumbnailImagesUpdate(index: Integer; count: Integer; imageIndex: Integer); dispid 1610743831;
    procedure Dispose; dispid 1610743832;
    property Recorder: _CvsInSightDisplayRecorder dispid 1610743833;
    procedure Connect; dispid 1610743835;
    procedure Disconnect; dispid 1610743836;
    property Play: ICvsActionToggle readonly dispid 1610743837;
    procedure ProcessShortcutKeys(const e: _KeyEventArgs); dispid 1610743838;
  end;

// *********************************************************************//
// Interface: _CvsFilmstripResultsQueue
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {524D6F61-7394-3733-AD7A-48067469D6D2}
// *********************************************************************//
  _CvsFilmstripResultsQueue = interface(IDispatch)
    ['{524D6F61-7394-3733-AD7A-48067469D6D2}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Get_QueueSize: Integer; safecall;
    function Get_SelectedIndex: Integer; safecall;
    procedure Set_SelectedIndex(pRetVal: Integer); safecall;
    function Get_SupportsEdit: WordBool; safecall;
    function Get_ThumbnailImages: PSafeArray; safecall;
    function Get_ResultsStatus: PSafeArray; safecall;
    function Get_Filmstrip: ICvsFilmstripInternal; safecall;
    procedure _Set_Filmstrip(const pRetVal: ICvsFilmstripInternal); safecall;
    function Get_SelectFirst: ICvsAction; safecall;
    function Get_SelectLast: ICvsAction; safecall;
    function Get_SelectNext: ICvsAction; safecall;
    function Get_SelectPrevious: ICvsAction; safecall;
    function Get_SelectNextPage: ICvsAction; safecall;
    function Get_SelectPrevPage: ICvsAction; safecall;
    function Get_ClearQueue: ICvsAction; safecall;
    function Get_SaveQueue: ICvsAction; safecall;
    function Get_DeleteSelectedItem: ICvsAction; safecall;
    function Get_Enabled: WordBool; safecall;
    procedure Set_Enabled(pRetVal: WordBool); safecall;
    procedure RequestThumbnailImagesUpdate(index: Integer; count: Integer; imageIndex: Integer); safecall;
    procedure Dispose; safecall;
    function Get_InSight: _CvsInSight; safecall;
    procedure _Set_InSight(const pRetVal: _CvsInSight); safecall;
    function Get_IsFrozen: WordBool; safecall;
    function Get_Options: _CvsFilmstripResultsQueueOptions; safecall;
    procedure _Set_Options(const pRetVal: _CvsFilmstripResultsQueueOptions); safecall;
    procedure Connect; safecall;
    procedure Disconnect; safecall;
    function Get_FreezeQueue: ICvsActionToggle; safecall;
    procedure ProcessShortcutKeys(const e: _KeyEventArgs); safecall;
    function Get_Settings: _CvsFilmstripResultsQueueSettings; safecall;
    property ToString: WideString read Get_ToString;
    property QueueSize: Integer read Get_QueueSize;
    property SelectedIndex: Integer read Get_SelectedIndex write Set_SelectedIndex;
    property SupportsEdit: WordBool read Get_SupportsEdit;
    property ThumbnailImages: PSafeArray read Get_ThumbnailImages;
    property ResultsStatus: PSafeArray read Get_ResultsStatus;
    property Filmstrip: ICvsFilmstripInternal read Get_Filmstrip write _Set_Filmstrip;
    property SelectFirst: ICvsAction read Get_SelectFirst;
    property SelectLast: ICvsAction read Get_SelectLast;
    property SelectNext: ICvsAction read Get_SelectNext;
    property SelectPrevious: ICvsAction read Get_SelectPrevious;
    property SelectNextPage: ICvsAction read Get_SelectNextPage;
    property SelectPrevPage: ICvsAction read Get_SelectPrevPage;
    property ClearQueue: ICvsAction read Get_ClearQueue;
    property SaveQueue: ICvsAction read Get_SaveQueue;
    property DeleteSelectedItem: ICvsAction read Get_DeleteSelectedItem;
    property Enabled: WordBool read Get_Enabled write Set_Enabled;
    property InSight: _CvsInSight read Get_InSight write _Set_InSight;
    property IsFrozen: WordBool read Get_IsFrozen;
    property Options: _CvsFilmstripResultsQueueOptions read Get_Options write _Set_Options;
    property FreezeQueue: ICvsActionToggle read Get_FreezeQueue;
    property Settings: _CvsFilmstripResultsQueueSettings read Get_Settings;
  end;

// *********************************************************************//
// DispIntf:  _CvsFilmstripResultsQueueDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {524D6F61-7394-3733-AD7A-48067469D6D2}
// *********************************************************************//
  _CvsFilmstripResultsQueueDisp = dispinterface
    ['{524D6F61-7394-3733-AD7A-48067469D6D2}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    property QueueSize: Integer readonly dispid 1610743812;
    property SelectedIndex: Integer dispid 1610743813;
    property SupportsEdit: WordBool readonly dispid 1610743815;
    property ThumbnailImages: {??PSafeArray}OleVariant readonly dispid 1610743816;
    property ResultsStatus: {??PSafeArray}OleVariant readonly dispid 1610743817;
    property Filmstrip: ICvsFilmstripInternal dispid 1610743818;
    property SelectFirst: ICvsAction readonly dispid 1610743820;
    property SelectLast: ICvsAction readonly dispid 1610743821;
    property SelectNext: ICvsAction readonly dispid 1610743822;
    property SelectPrevious: ICvsAction readonly dispid 1610743823;
    property SelectNextPage: ICvsAction readonly dispid 1610743824;
    property SelectPrevPage: ICvsAction readonly dispid 1610743825;
    property ClearQueue: ICvsAction readonly dispid 1610743826;
    property SaveQueue: ICvsAction readonly dispid 1610743827;
    property DeleteSelectedItem: ICvsAction readonly dispid 1610743828;
    property Enabled: WordBool dispid 1610743829;
    procedure RequestThumbnailImagesUpdate(index: Integer; count: Integer; imageIndex: Integer); dispid 1610743831;
    procedure Dispose; dispid 1610743832;
    property InSight: _CvsInSight dispid 1610743833;
    property IsFrozen: WordBool readonly dispid 1610743835;
    property Options: _CvsFilmstripResultsQueueOptions dispid 1610743836;
    procedure Connect; dispid 1610743838;
    procedure Disconnect; dispid 1610743839;
    property FreezeQueue: ICvsActionToggle readonly dispid 1610743840;
    procedure ProcessShortcutKeys(const e: _KeyEventArgs); dispid 1610743841;
    property Settings: _CvsFilmstripResultsQueueSettings readonly dispid 1610743842;
  end;

// *********************************************************************//
// Interface: _CvsFilmstripResultsQueueOptions
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {635261FD-A8DA-317D-B750-4352160602FC}
// *********************************************************************//
  _CvsFilmstripResultsQueueOptions = interface(IDispatch)
    ['{635261FD-A8DA-317D-B750-4352160602FC}']
  end;

// *********************************************************************//
// DispIntf:  _CvsFilmstripResultsQueueOptionsDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {635261FD-A8DA-317D-B750-4352160602FC}
// *********************************************************************//
  _CvsFilmstripResultsQueueOptionsDisp = dispinterface
    ['{635261FD-A8DA-317D-B750-4352160602FC}']
  end;

// *********************************************************************//
// Interface: _CvsFilmstripResultsQueueSettings
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {79F11714-EF4F-3DC1-B1C1-74930739B2F7}
// *********************************************************************//
  _CvsFilmstripResultsQueueSettings = interface(IDispatch)
    ['{79F11714-EF4F-3DC1-B1C1-74930739B2F7}']
  end;

// *********************************************************************//
// DispIntf:  _CvsFilmstripResultsQueueSettingsDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {79F11714-EF4F-3DC1-B1C1-74930739B2F7}
// *********************************************************************//
  _CvsFilmstripResultsQueueSettingsDisp = dispinterface
    ['{79F11714-EF4F-3DC1-B1C1-74930739B2F7}']
  end;

// *********************************************************************//
// Interface: _CvsInSightDisplayEdit
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {D70FD312-7035-3834-9997-F0C3743DE8C3}
// *********************************************************************//
  _CvsInSightDisplayEdit = interface(IDispatch)
    ['{D70FD312-7035-3834-9997-F0C3743DE8C3}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Get_AcceptChanges: ICvsAction; safecall;
    function Get_AutoFitColumns: ICvsAction; safecall;
    function Get_AutoFitRows: ICvsAction; safecall;
    function Get_CancelChanges: ICvsAction; safecall;
    function Get_ClearAll: ICvsAction; safecall;
    function Get_ClearComments: ICvsAction; safecall;
    function Get_ClearContents: ICvsAction; safecall;
    function Get_ClearEasyTags: ICvsAction; safecall;
    function Get_ClearFormats: ICvsAction; safecall;
    function Get_ClearOpcTags: ICvsAction; safecall;
    function Get_ClearPermissions: ICvsAction; safecall;
    function Get_ConvertToEasyTags: ICvsAction; safecall;
    function Get_Copy: ICvsAction; safecall;
    function Get_CopyBitmap: ICvsAction; safecall;
    function Get_CreateNewCustomPrivilege: ICvsAction; safecall;
    function Get_Cut: ICvsAction; safecall;
    function Get_CycleGraphicColor: ICvsAction; safecall;
    function Get_DecreaseGridOpacity: ICvsAction; safecall;
    function Get_DeleteCellsOrRowsOrColumns: ICvsAction; safecall;
    function Get_DialogUndoOnCancel: ICvsActionToggle; safecall;
    function Get_EasyBuilderPlaybackOptions: ICvsAction; safecall;
    function Get_EasyBuilderRecordOptions: ICvsAction; safecall;
    function Get_EditActiveCellsLimit: ICvsAction; safecall;
    function Get_EditAuditMessageSettings: ICvsAction; safecall;
    function Get_EditCcLinkSettings: ICvsAction; safecall;
    function Get_EditCellExpression: ICvsAction; safecall;
    function Get_EditCellExpressionInPlace: ICvsAction; safecall;
    function Get_EditCellGraphic: ICvsAction; safecall;
    function Get_EditCellState: ICvsAction; safecall;
    function Get_EditCustomPrivileges: ICvsAction; safecall;
    function Get_EditCustomViewSettings: ICvsAction; safecall;
    function Get_EditDateTimeSettings: ICvsAction; safecall;
    function Get_EditDiscreteInputSettings: ICvsAction; safecall;
    function Get_EditDiscreteOutputSettings: ICvsAction; safecall;
    function Get_EditEasyViewSettings: ICvsAction; safecall;
    function Get_EditExternalLightSettings: ICvsAction; safecall;
    function Get_EditFtpSettings: ICvsAction; safecall;
    function Get_EditHostTableSettings: ICvsAction; safecall;
    function Get_EditImageBuffers: ICvsAction; safecall;
    function Get_EditImageSettings: ICvsAction; safecall;
    function Get_EditLicensing: ICvsAction; safecall;
    function Get_EditLineScanSettings: ICvsAction; safecall;
    function Get_EditNetworkSettings: ICvsAction; safecall;
    function Get_EditSerialPort0Settings: ICvsAction; safecall;
    function Get_EditSerialPort1Settings: ICvsAction; safecall;
    function Get_EditStartupSettings: ICvsAction; safecall;
    function Get_EditUserAccessSettings: ICvsAction; safecall;
    function Get_EditVisionViewViewSelection: ICvsAction; safecall;
    function Get_ExportCells: ICvsAction; safecall;
    function Get_ExportSnippet: ICvsAction; safecall;
    function Get_Find: ICvsAction; safecall;
    function Get_FormatBold: ICvsActionToggle; safecall;
    function Get_FormatCenter: ICvsActionToggle; safecall;
    function Get_FormatDecreaseDecimal: ICvsAction; safecall;
    function Get_FormatDecreaseFont: ICvsAction; safecall;
    function Get_FormatIncreaseDecimal: ICvsAction; safecall;
    function Get_FormatIncreaseFont: ICvsAction; safecall;
    function Get_FormatItalic: ICvsActionToggle; safecall;
    function Get_FormatLeft: ICvsActionToggle; safecall;
    function Get_FormatRight: ICvsActionToggle; safecall;
    function Get_GoTo_: ICvsAction; safecall;
    function Get_GoToFirstError: ICvsAction; safecall;
    function Get_HideColumns: ICvsAction; safecall;
    function Get_HideRows: ICvsAction; safecall;
    function Get_HideRowsOrColumns: ICvsAction; safecall;
    function Get_ImportCells: ICvsAction; safecall;
    function Get_ImportSnippet: ICvsAction; safecall;
    function Get_IncreaseGridOpacity: ICvsAction; safecall;
    function Get_InsertAbsoluteReference: ICvsAction; safecall;
    function Get_InsertCells: ICvsAction; safecall;
    function Get_InsertCellsOrRowsOrColumns: ICvsAction; safecall;
    function Get_InsertColumns: ICvsAction; safecall;
    function Get_InsertComment: ICvsAction; safecall;
    function Get_InsertEasyTag: ICvsAction; safecall;
    function Get_InsertFunction: ICvsAction; safecall;
    function Get_InsertOpcTag: ICvsAction; safecall;
    function Get_InsertRelativeReference: ICvsAction; safecall;
    function Get_InsertRows: ICvsAction; safecall;
    function Get_LiveAcquire: ICvsActionToggle; safecall;
    function Get_ManualAcquire: ICvsAction; safecall;
    function Get_MaximizeCellRegion: ICvsAction; safecall;
    function Get_MoveDown: ICvsAction; safecall;
    function Get_MoveLeft: ICvsAction; safecall;
    function Get_MovePageDown: ICvsAction; safecall;
    function Get_MovePageUp: ICvsAction; safecall;
    function Get_MoveRight: ICvsAction; safecall;
    function Get_MoveToFirstCell: ICvsAction; safecall;
    function Get_MoveToLastCell: ICvsAction; safecall;
    function Get_MoveToRowEnd: ICvsAction; safecall;
    function Get_MoveToRowStart: ICvsAction; safecall;
    function Get_MoveUp: ICvsAction; safecall;
    function Get_NewJob: ICvsAction; safecall;
    function Get_NextImageOrientation: ICvsAction; safecall;
    function Get_OpenCellFormatDialog: ICvsAction; safecall;
    function Get_OpenImage: ICvsAction; safecall;
    function Get_OpenJob: ICvsAction; safecall;
    function Get_OpenOverallPassFailDialog: ICvsAction; safecall;
    function Get_OpenOverallPassFailDialogWithCurrentCell: ICvsAction; safecall;
    function Get_OpenPageSetupDialog: ICvsAction; safecall;
    function Get_OpenPrintDialog: ICvsAction; safecall;
    function Get_Paste: ICvsAction; safecall;
    function Get_Play: ICvsActionToggle; safecall;
    function Get_PlayFirst: ICvsAction; safecall;
    function Get_PlayLast: ICvsAction; safecall;
    function Get_PlayNext: ICvsAction; safecall;
    function Get_PlayPrevious: ICvsAction; safecall;
    function Get_PrintNow: ICvsAction; safecall;
    function Get_ProfileJob: ICvsAction; safecall;
    function Get_ProtectJob: ICvsAction; safecall;
    function Get_Record_: ICvsActionToggle; safecall;
    function Get_RecordPlaybackOptions: ICvsAction; safecall;
    function Get_Redo: ICvsAction; safecall;
    function Get_RepeatingTrigger: ICvsActionToggle; safecall;
    function Get_ResetSensor: ICvsAction; safecall;
    function Get_SaveImageAs: ICvsAction; safecall;
    function Get_SaveJob: ICvsAction; safecall;
    function Get_SaveJobAs: ICvsAction; safecall;
    function Get_SelectColumn: ICvsAction; safecall;
    function Get_SelectDown: ICvsAction; safecall;
    function Get_SelectLeft: ICvsAction; safecall;
    function Get_SelectPageDown: ICvsAction; safecall;
    function Get_SelectPageUp: ICvsAction; safecall;
    function Get_SelectRight: ICvsAction; safecall;
    function Get_SelectRow: ICvsAction; safecall;
    function Get_SelectToRowEnd: ICvsAction; safecall;
    function Get_SelectToRowStart: ICvsAction; safecall;
    function Get_SelectUp: ICvsAction; safecall;
    function Get_SetColumnWidth: ICvsAction; safecall;
    function Get_SetGridOpacity: ICvsAction; safecall;
    function Get_SetGridScale: ICvsAction; safecall;
    function Get_SetRamDiskSize: ICvsAction; safecall;
    function Get_SetRowHeight: ICvsAction; safecall;
    function Get_SetTextScaleMode: ICvsAction; safecall;
    function Get_ShowCustomView: ICvsActionToggle; safecall;
    function Get_ShowDependencyErrorsOnly: ICvsActionToggle; safecall;
    function Get_ShowDependencyLevelsDecrease: ICvsAction; safecall;
    function Get_ShowDependencyLevelsIncrease: ICvsAction; safecall;
    function Get_ShowDependencyLevelsReset: ICvsAction; safecall;
    function Get_ShowGraphics: ICvsActionToggle; safecall;
    function Get_ShowGrid: ICvsActionToggle; safecall;
    function Get_ShowImageSaturation: ICvsActionToggle; safecall;
    function Get_SoftOnline: ICvsActionToggle; safecall;
    function Get_SynchronizedInspectionParameters: ICvsAction; safecall;
    function Get_Undo: ICvsAction; safecall;
    function Get_UnhideColumns: ICvsAction; safecall;
    function Get_UnhideRows: ICvsAction; safecall;
    function Get_UnhideRowsOrColumns: ICvsAction; safecall;
    function Get_ZoomGrid1To1: ICvsAction; safecall;
    function Get_ZoomGridIn: ICvsAction; safecall;
    function Get_ZoomGridOut: ICvsAction; safecall;
    function Get_ZoomImage1To1: ICvsAction; safecall;
    function Get_ZoomImageIn: ICvsAction; safecall;
    function Get_ZoomImageOut: ICvsAction; safecall;
    function Get_ZoomImageToFill: ICvsActionToggle; safecall;
    function Get_ZoomImageToFit: ICvsActionToggle; safecall;
    function Get_ZoomImageToMax: ICvsAction; safecall;
    procedure MnuSensor_BeforePopup; safecall;
    procedure SetForeColor(color: OLE_COLOR); safecall;
    procedure SetBackColor(color: OLE_COLOR); safecall;
    function Get_SelectedFontSize: Single; safecall;
    procedure Set_SelectedFontSize(pRetVal: Single); safecall;
    function Get_SelectedFontName: WideString; safecall;
    procedure Set_SelectedFontName(const pRetVal: WideString); safecall;
    procedure add_SelectedFontChanged(const value: _EventHandler); safecall;
    procedure remove_SelectedFontChanged(const value: _EventHandler); safecall;
    procedure add_EnablesUpdated(const value: _EventHandler); safecall;
    procedure remove_EnablesUpdated(const value: _EventHandler); safecall;
    function ProcessShortcutKey(keyData: Keys): WordBool; safecall;
    procedure add_NetworkTreeRefreshNeeded(const value: _EventHandler); safecall;
    procedure remove_NetworkTreeRefreshNeeded(const value: _EventHandler); safecall;
    function Get_EditMaxGuiConnections: ICvsAction; safecall;
    function Get_EditJobServerSettings: ICvsAction; safecall;
    procedure add_RepeatingTriggerChanged(const value: _EventHandler); safecall;
    procedure remove_RepeatingTriggerChanged(const value: _EventHandler); safecall;
    procedure add_JobSaving(const value: _EventHandler); safecall;
    procedure remove_JobSaving(const value: _EventHandler); safecall;
    procedure PromptForInSightRestart; safecall;
    function Get_ClassicUserRefreshNeeded: _EventHandler; safecall;
    procedure _Set_ClassicUserRefreshNeeded(const pRetVal: _EventHandler); safecall;
    property ToString: WideString read Get_ToString;
    property AcceptChanges: ICvsAction read Get_AcceptChanges;
    property AutoFitColumns: ICvsAction read Get_AutoFitColumns;
    property AutoFitRows: ICvsAction read Get_AutoFitRows;
    property CancelChanges: ICvsAction read Get_CancelChanges;
    property ClearAll: ICvsAction read Get_ClearAll;
    property ClearComments: ICvsAction read Get_ClearComments;
    property ClearContents: ICvsAction read Get_ClearContents;
    property ClearEasyTags: ICvsAction read Get_ClearEasyTags;
    property ClearFormats: ICvsAction read Get_ClearFormats;
    property ClearOpcTags: ICvsAction read Get_ClearOpcTags;
    property ClearPermissions: ICvsAction read Get_ClearPermissions;
    property ConvertToEasyTags: ICvsAction read Get_ConvertToEasyTags;
    property Copy: ICvsAction read Get_Copy;
    property CopyBitmap: ICvsAction read Get_CopyBitmap;
    property CreateNewCustomPrivilege: ICvsAction read Get_CreateNewCustomPrivilege;
    property Cut: ICvsAction read Get_Cut;
    property CycleGraphicColor: ICvsAction read Get_CycleGraphicColor;
    property DecreaseGridOpacity: ICvsAction read Get_DecreaseGridOpacity;
    property DeleteCellsOrRowsOrColumns: ICvsAction read Get_DeleteCellsOrRowsOrColumns;
    property DialogUndoOnCancel: ICvsActionToggle read Get_DialogUndoOnCancel;
    property EasyBuilderPlaybackOptions: ICvsAction read Get_EasyBuilderPlaybackOptions;
    property EasyBuilderRecordOptions: ICvsAction read Get_EasyBuilderRecordOptions;
    property EditActiveCellsLimit: ICvsAction read Get_EditActiveCellsLimit;
    property EditAuditMessageSettings: ICvsAction read Get_EditAuditMessageSettings;
    property EditCcLinkSettings: ICvsAction read Get_EditCcLinkSettings;
    property EditCellExpression: ICvsAction read Get_EditCellExpression;
    property EditCellExpressionInPlace: ICvsAction read Get_EditCellExpressionInPlace;
    property EditCellGraphic: ICvsAction read Get_EditCellGraphic;
    property EditCellState: ICvsAction read Get_EditCellState;
    property EditCustomPrivileges: ICvsAction read Get_EditCustomPrivileges;
    property EditCustomViewSettings: ICvsAction read Get_EditCustomViewSettings;
    property EditDateTimeSettings: ICvsAction read Get_EditDateTimeSettings;
    property EditDiscreteInputSettings: ICvsAction read Get_EditDiscreteInputSettings;
    property EditDiscreteOutputSettings: ICvsAction read Get_EditDiscreteOutputSettings;
    property EditEasyViewSettings: ICvsAction read Get_EditEasyViewSettings;
    property EditExternalLightSettings: ICvsAction read Get_EditExternalLightSettings;
    property EditFtpSettings: ICvsAction read Get_EditFtpSettings;
    property EditHostTableSettings: ICvsAction read Get_EditHostTableSettings;
    property EditImageBuffers: ICvsAction read Get_EditImageBuffers;
    property EditImageSettings: ICvsAction read Get_EditImageSettings;
    property EditLicensing: ICvsAction read Get_EditLicensing;
    property EditLineScanSettings: ICvsAction read Get_EditLineScanSettings;
    property EditNetworkSettings: ICvsAction read Get_EditNetworkSettings;
    property EditSerialPort0Settings: ICvsAction read Get_EditSerialPort0Settings;
    property EditSerialPort1Settings: ICvsAction read Get_EditSerialPort1Settings;
    property EditStartupSettings: ICvsAction read Get_EditStartupSettings;
    property EditUserAccessSettings: ICvsAction read Get_EditUserAccessSettings;
    property EditVisionViewViewSelection: ICvsAction read Get_EditVisionViewViewSelection;
    property ExportCells: ICvsAction read Get_ExportCells;
    property ExportSnippet: ICvsAction read Get_ExportSnippet;
    property Find: ICvsAction read Get_Find;
    property FormatBold: ICvsActionToggle read Get_FormatBold;
    property FormatCenter: ICvsActionToggle read Get_FormatCenter;
    property FormatDecreaseDecimal: ICvsAction read Get_FormatDecreaseDecimal;
    property FormatDecreaseFont: ICvsAction read Get_FormatDecreaseFont;
    property FormatIncreaseDecimal: ICvsAction read Get_FormatIncreaseDecimal;
    property FormatIncreaseFont: ICvsAction read Get_FormatIncreaseFont;
    property FormatItalic: ICvsActionToggle read Get_FormatItalic;
    property FormatLeft: ICvsActionToggle read Get_FormatLeft;
    property FormatRight: ICvsActionToggle read Get_FormatRight;
    property GoTo_: ICvsAction read Get_GoTo_;
    property GoToFirstError: ICvsAction read Get_GoToFirstError;
    property HideColumns: ICvsAction read Get_HideColumns;
    property HideRows: ICvsAction read Get_HideRows;
    property HideRowsOrColumns: ICvsAction read Get_HideRowsOrColumns;
    property ImportCells: ICvsAction read Get_ImportCells;
    property ImportSnippet: ICvsAction read Get_ImportSnippet;
    property IncreaseGridOpacity: ICvsAction read Get_IncreaseGridOpacity;
    property InsertAbsoluteReference: ICvsAction read Get_InsertAbsoluteReference;
    property InsertCells: ICvsAction read Get_InsertCells;
    property InsertCellsOrRowsOrColumns: ICvsAction read Get_InsertCellsOrRowsOrColumns;
    property InsertColumns: ICvsAction read Get_InsertColumns;
    property InsertComment: ICvsAction read Get_InsertComment;
    property InsertEasyTag: ICvsAction read Get_InsertEasyTag;
    property InsertFunction: ICvsAction read Get_InsertFunction;
    property InsertOpcTag: ICvsAction read Get_InsertOpcTag;
    property InsertRelativeReference: ICvsAction read Get_InsertRelativeReference;
    property InsertRows: ICvsAction read Get_InsertRows;
    property LiveAcquire: ICvsActionToggle read Get_LiveAcquire;
    property ManualAcquire: ICvsAction read Get_ManualAcquire;
    property MaximizeCellRegion: ICvsAction read Get_MaximizeCellRegion;
    property MoveDown: ICvsAction read Get_MoveDown;
    property MoveLeft: ICvsAction read Get_MoveLeft;
    property MovePageDown: ICvsAction read Get_MovePageDown;
    property MovePageUp: ICvsAction read Get_MovePageUp;
    property MoveRight: ICvsAction read Get_MoveRight;
    property MoveToFirstCell: ICvsAction read Get_MoveToFirstCell;
    property MoveToLastCell: ICvsAction read Get_MoveToLastCell;
    property MoveToRowEnd: ICvsAction read Get_MoveToRowEnd;
    property MoveToRowStart: ICvsAction read Get_MoveToRowStart;
    property MoveUp: ICvsAction read Get_MoveUp;
    property NewJob: ICvsAction read Get_NewJob;
    property NextImageOrientation: ICvsAction read Get_NextImageOrientation;
    property OpenCellFormatDialog: ICvsAction read Get_OpenCellFormatDialog;
    property OpenImage: ICvsAction read Get_OpenImage;
    property OpenJob: ICvsAction read Get_OpenJob;
    property OpenOverallPassFailDialog: ICvsAction read Get_OpenOverallPassFailDialog;
    property OpenOverallPassFailDialogWithCurrentCell: ICvsAction read Get_OpenOverallPassFailDialogWithCurrentCell;
    property OpenPageSetupDialog: ICvsAction read Get_OpenPageSetupDialog;
    property OpenPrintDialog: ICvsAction read Get_OpenPrintDialog;
    property Paste: ICvsAction read Get_Paste;
    property Play: ICvsActionToggle read Get_Play;
    property PlayFirst: ICvsAction read Get_PlayFirst;
    property PlayLast: ICvsAction read Get_PlayLast;
    property PlayNext: ICvsAction read Get_PlayNext;
    property PlayPrevious: ICvsAction read Get_PlayPrevious;
    property PrintNow: ICvsAction read Get_PrintNow;
    property ProfileJob: ICvsAction read Get_ProfileJob;
    property ProtectJob: ICvsAction read Get_ProtectJob;
    property Record_: ICvsActionToggle read Get_Record_;
    property RecordPlaybackOptions: ICvsAction read Get_RecordPlaybackOptions;
    property Redo: ICvsAction read Get_Redo;
    property RepeatingTrigger: ICvsActionToggle read Get_RepeatingTrigger;
    property ResetSensor: ICvsAction read Get_ResetSensor;
    property SaveImageAs: ICvsAction read Get_SaveImageAs;
    property SaveJob: ICvsAction read Get_SaveJob;
    property SaveJobAs: ICvsAction read Get_SaveJobAs;
    property SelectColumn: ICvsAction read Get_SelectColumn;
    property SelectDown: ICvsAction read Get_SelectDown;
    property SelectLeft: ICvsAction read Get_SelectLeft;
    property SelectPageDown: ICvsAction read Get_SelectPageDown;
    property SelectPageUp: ICvsAction read Get_SelectPageUp;
    property SelectRight: ICvsAction read Get_SelectRight;
    property SelectRow: ICvsAction read Get_SelectRow;
    property SelectToRowEnd: ICvsAction read Get_SelectToRowEnd;
    property SelectToRowStart: ICvsAction read Get_SelectToRowStart;
    property SelectUp: ICvsAction read Get_SelectUp;
    property SetColumnWidth: ICvsAction read Get_SetColumnWidth;
    property SetGridOpacity: ICvsAction read Get_SetGridOpacity;
    property SetGridScale: ICvsAction read Get_SetGridScale;
    property SetRamDiskSize: ICvsAction read Get_SetRamDiskSize;
    property SetRowHeight: ICvsAction read Get_SetRowHeight;
    property SetTextScaleMode: ICvsAction read Get_SetTextScaleMode;
    property ShowCustomView: ICvsActionToggle read Get_ShowCustomView;
    property ShowDependencyErrorsOnly: ICvsActionToggle read Get_ShowDependencyErrorsOnly;
    property ShowDependencyLevelsDecrease: ICvsAction read Get_ShowDependencyLevelsDecrease;
    property ShowDependencyLevelsIncrease: ICvsAction read Get_ShowDependencyLevelsIncrease;
    property ShowDependencyLevelsReset: ICvsAction read Get_ShowDependencyLevelsReset;
    property ShowGraphics: ICvsActionToggle read Get_ShowGraphics;
    property ShowGrid: ICvsActionToggle read Get_ShowGrid;
    property ShowImageSaturation: ICvsActionToggle read Get_ShowImageSaturation;
    property SoftOnline: ICvsActionToggle read Get_SoftOnline;
    property SynchronizedInspectionParameters: ICvsAction read Get_SynchronizedInspectionParameters;
    property Undo: ICvsAction read Get_Undo;
    property UnhideColumns: ICvsAction read Get_UnhideColumns;
    property UnhideRows: ICvsAction read Get_UnhideRows;
    property UnhideRowsOrColumns: ICvsAction read Get_UnhideRowsOrColumns;
    property ZoomGrid1To1: ICvsAction read Get_ZoomGrid1To1;
    property ZoomGridIn: ICvsAction read Get_ZoomGridIn;
    property ZoomGridOut: ICvsAction read Get_ZoomGridOut;
    property ZoomImage1To1: ICvsAction read Get_ZoomImage1To1;
    property ZoomImageIn: ICvsAction read Get_ZoomImageIn;
    property ZoomImageOut: ICvsAction read Get_ZoomImageOut;
    property ZoomImageToFill: ICvsActionToggle read Get_ZoomImageToFill;
    property ZoomImageToFit: ICvsActionToggle read Get_ZoomImageToFit;
    property ZoomImageToMax: ICvsAction read Get_ZoomImageToMax;
    property SelectedFontSize: Single read Get_SelectedFontSize write Set_SelectedFontSize;
    property SelectedFontName: WideString read Get_SelectedFontName write Set_SelectedFontName;
    property EditMaxGuiConnections: ICvsAction read Get_EditMaxGuiConnections;
    property EditJobServerSettings: ICvsAction read Get_EditJobServerSettings;
    property ClassicUserRefreshNeeded: _EventHandler read Get_ClassicUserRefreshNeeded write _Set_ClassicUserRefreshNeeded;
  end;

// *********************************************************************//
// DispIntf:  _CvsInSightDisplayEditDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {D70FD312-7035-3834-9997-F0C3743DE8C3}
// *********************************************************************//
  _CvsInSightDisplayEditDisp = dispinterface
    ['{D70FD312-7035-3834-9997-F0C3743DE8C3}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    property AcceptChanges: ICvsAction readonly dispid 1610743812;
    property AutoFitColumns: ICvsAction readonly dispid 1610743813;
    property AutoFitRows: ICvsAction readonly dispid 1610743814;
    property CancelChanges: ICvsAction readonly dispid 1610743815;
    property ClearAll: ICvsAction readonly dispid 1610743816;
    property ClearComments: ICvsAction readonly dispid 1610743817;
    property ClearContents: ICvsAction readonly dispid 1610743818;
    property ClearEasyTags: ICvsAction readonly dispid 1610743819;
    property ClearFormats: ICvsAction readonly dispid 1610743820;
    property ClearOpcTags: ICvsAction readonly dispid 1610743821;
    property ClearPermissions: ICvsAction readonly dispid 1610743822;
    property ConvertToEasyTags: ICvsAction readonly dispid 1610743823;
    property Copy: ICvsAction readonly dispid 1610743824;
    property CopyBitmap: ICvsAction readonly dispid 1610743825;
    property CreateNewCustomPrivilege: ICvsAction readonly dispid 1610743826;
    property Cut: ICvsAction readonly dispid 1610743827;
    property CycleGraphicColor: ICvsAction readonly dispid 1610743828;
    property DecreaseGridOpacity: ICvsAction readonly dispid 1610743829;
    property DeleteCellsOrRowsOrColumns: ICvsAction readonly dispid 1610743830;
    property DialogUndoOnCancel: ICvsActionToggle readonly dispid 1610743831;
    property EasyBuilderPlaybackOptions: ICvsAction readonly dispid 1610743832;
    property EasyBuilderRecordOptions: ICvsAction readonly dispid 1610743833;
    property EditActiveCellsLimit: ICvsAction readonly dispid 1610743834;
    property EditAuditMessageSettings: ICvsAction readonly dispid 1610743835;
    property EditCcLinkSettings: ICvsAction readonly dispid 1610743836;
    property EditCellExpression: ICvsAction readonly dispid 1610743837;
    property EditCellExpressionInPlace: ICvsAction readonly dispid 1610743838;
    property EditCellGraphic: ICvsAction readonly dispid 1610743839;
    property EditCellState: ICvsAction readonly dispid 1610743840;
    property EditCustomPrivileges: ICvsAction readonly dispid 1610743841;
    property EditCustomViewSettings: ICvsAction readonly dispid 1610743842;
    property EditDateTimeSettings: ICvsAction readonly dispid 1610743843;
    property EditDiscreteInputSettings: ICvsAction readonly dispid 1610743844;
    property EditDiscreteOutputSettings: ICvsAction readonly dispid 1610743845;
    property EditEasyViewSettings: ICvsAction readonly dispid 1610743846;
    property EditExternalLightSettings: ICvsAction readonly dispid 1610743847;
    property EditFtpSettings: ICvsAction readonly dispid 1610743848;
    property EditHostTableSettings: ICvsAction readonly dispid 1610743849;
    property EditImageBuffers: ICvsAction readonly dispid 1610743850;
    property EditImageSettings: ICvsAction readonly dispid 1610743851;
    property EditLicensing: ICvsAction readonly dispid 1610743852;
    property EditLineScanSettings: ICvsAction readonly dispid 1610743853;
    property EditNetworkSettings: ICvsAction readonly dispid 1610743854;
    property EditSerialPort0Settings: ICvsAction readonly dispid 1610743855;
    property EditSerialPort1Settings: ICvsAction readonly dispid 1610743856;
    property EditStartupSettings: ICvsAction readonly dispid 1610743857;
    property EditUserAccessSettings: ICvsAction readonly dispid 1610743858;
    property EditVisionViewViewSelection: ICvsAction readonly dispid 1610743859;
    property ExportCells: ICvsAction readonly dispid 1610743860;
    property ExportSnippet: ICvsAction readonly dispid 1610743861;
    property Find: ICvsAction readonly dispid 1610743862;
    property FormatBold: ICvsActionToggle readonly dispid 1610743863;
    property FormatCenter: ICvsActionToggle readonly dispid 1610743864;
    property FormatDecreaseDecimal: ICvsAction readonly dispid 1610743865;
    property FormatDecreaseFont: ICvsAction readonly dispid 1610743866;
    property FormatIncreaseDecimal: ICvsAction readonly dispid 1610743867;
    property FormatIncreaseFont: ICvsAction readonly dispid 1610743868;
    property FormatItalic: ICvsActionToggle readonly dispid 1610743869;
    property FormatLeft: ICvsActionToggle readonly dispid 1610743870;
    property FormatRight: ICvsActionToggle readonly dispid 1610743871;
    property GoTo_: ICvsAction readonly dispid 1610743872;
    property GoToFirstError: ICvsAction readonly dispid 1610743873;
    property HideColumns: ICvsAction readonly dispid 1610743874;
    property HideRows: ICvsAction readonly dispid 1610743875;
    property HideRowsOrColumns: ICvsAction readonly dispid 1610743876;
    property ImportCells: ICvsAction readonly dispid 1610743877;
    property ImportSnippet: ICvsAction readonly dispid 1610743878;
    property IncreaseGridOpacity: ICvsAction readonly dispid 1610743879;
    property InsertAbsoluteReference: ICvsAction readonly dispid 1610743880;
    property InsertCells: ICvsAction readonly dispid 1610743881;
    property InsertCellsOrRowsOrColumns: ICvsAction readonly dispid 1610743882;
    property InsertColumns: ICvsAction readonly dispid 1610743883;
    property InsertComment: ICvsAction readonly dispid 1610743884;
    property InsertEasyTag: ICvsAction readonly dispid 1610743885;
    property InsertFunction: ICvsAction readonly dispid 1610743886;
    property InsertOpcTag: ICvsAction readonly dispid 1610743887;
    property InsertRelativeReference: ICvsAction readonly dispid 1610743888;
    property InsertRows: ICvsAction readonly dispid 1610743889;
    property LiveAcquire: ICvsActionToggle readonly dispid 1610743890;
    property ManualAcquire: ICvsAction readonly dispid 1610743891;
    property MaximizeCellRegion: ICvsAction readonly dispid 1610743892;
    property MoveDown: ICvsAction readonly dispid 1610743893;
    property MoveLeft: ICvsAction readonly dispid 1610743894;
    property MovePageDown: ICvsAction readonly dispid 1610743895;
    property MovePageUp: ICvsAction readonly dispid 1610743896;
    property MoveRight: ICvsAction readonly dispid 1610743897;
    property MoveToFirstCell: ICvsAction readonly dispid 1610743898;
    property MoveToLastCell: ICvsAction readonly dispid 1610743899;
    property MoveToRowEnd: ICvsAction readonly dispid 1610743900;
    property MoveToRowStart: ICvsAction readonly dispid 1610743901;
    property MoveUp: ICvsAction readonly dispid 1610743902;
    property NewJob: ICvsAction readonly dispid 1610743903;
    property NextImageOrientation: ICvsAction readonly dispid 1610743904;
    property OpenCellFormatDialog: ICvsAction readonly dispid 1610743905;
    property OpenImage: ICvsAction readonly dispid 1610743906;
    property OpenJob: ICvsAction readonly dispid 1610743907;
    property OpenOverallPassFailDialog: ICvsAction readonly dispid 1610743908;
    property OpenOverallPassFailDialogWithCurrentCell: ICvsAction readonly dispid 1610743909;
    property OpenPageSetupDialog: ICvsAction readonly dispid 1610743910;
    property OpenPrintDialog: ICvsAction readonly dispid 1610743911;
    property Paste: ICvsAction readonly dispid 1610743912;
    property Play: ICvsActionToggle readonly dispid 1610743913;
    property PlayFirst: ICvsAction readonly dispid 1610743914;
    property PlayLast: ICvsAction readonly dispid 1610743915;
    property PlayNext: ICvsAction readonly dispid 1610743916;
    property PlayPrevious: ICvsAction readonly dispid 1610743917;
    property PrintNow: ICvsAction readonly dispid 1610743918;
    property ProfileJob: ICvsAction readonly dispid 1610743919;
    property ProtectJob: ICvsAction readonly dispid 1610743920;
    property Record_: ICvsActionToggle readonly dispid 1610743921;
    property RecordPlaybackOptions: ICvsAction readonly dispid 1610743922;
    property Redo: ICvsAction readonly dispid 1610743923;
    property RepeatingTrigger: ICvsActionToggle readonly dispid 1610743924;
    property ResetSensor: ICvsAction readonly dispid 1610743925;
    property SaveImageAs: ICvsAction readonly dispid 1610743926;
    property SaveJob: ICvsAction readonly dispid 1610743927;
    property SaveJobAs: ICvsAction readonly dispid 1610743928;
    property SelectColumn: ICvsAction readonly dispid 1610743929;
    property SelectDown: ICvsAction readonly dispid 1610743930;
    property SelectLeft: ICvsAction readonly dispid 1610743931;
    property SelectPageDown: ICvsAction readonly dispid 1610743932;
    property SelectPageUp: ICvsAction readonly dispid 1610743933;
    property SelectRight: ICvsAction readonly dispid 1610743934;
    property SelectRow: ICvsAction readonly dispid 1610743935;
    property SelectToRowEnd: ICvsAction readonly dispid 1610743936;
    property SelectToRowStart: ICvsAction readonly dispid 1610743937;
    property SelectUp: ICvsAction readonly dispid 1610743938;
    property SetColumnWidth: ICvsAction readonly dispid 1610743939;
    property SetGridOpacity: ICvsAction readonly dispid 1610743940;
    property SetGridScale: ICvsAction readonly dispid 1610743941;
    property SetRamDiskSize: ICvsAction readonly dispid 1610743942;
    property SetRowHeight: ICvsAction readonly dispid 1610743943;
    property SetTextScaleMode: ICvsAction readonly dispid 1610743944;
    property ShowCustomView: ICvsActionToggle readonly dispid 1610743945;
    property ShowDependencyErrorsOnly: ICvsActionToggle readonly dispid 1610743946;
    property ShowDependencyLevelsDecrease: ICvsAction readonly dispid 1610743947;
    property ShowDependencyLevelsIncrease: ICvsAction readonly dispid 1610743948;
    property ShowDependencyLevelsReset: ICvsAction readonly dispid 1610743949;
    property ShowGraphics: ICvsActionToggle readonly dispid 1610743950;
    property ShowGrid: ICvsActionToggle readonly dispid 1610743951;
    property ShowImageSaturation: ICvsActionToggle readonly dispid 1610743952;
    property SoftOnline: ICvsActionToggle readonly dispid 1610743953;
    property SynchronizedInspectionParameters: ICvsAction readonly dispid 1610743954;
    property Undo: ICvsAction readonly dispid 1610743955;
    property UnhideColumns: ICvsAction readonly dispid 1610743956;
    property UnhideRows: ICvsAction readonly dispid 1610743957;
    property UnhideRowsOrColumns: ICvsAction readonly dispid 1610743958;
    property ZoomGrid1To1: ICvsAction readonly dispid 1610743959;
    property ZoomGridIn: ICvsAction readonly dispid 1610743960;
    property ZoomGridOut: ICvsAction readonly dispid 1610743961;
    property ZoomImage1To1: ICvsAction readonly dispid 1610743962;
    property ZoomImageIn: ICvsAction readonly dispid 1610743963;
    property ZoomImageOut: ICvsAction readonly dispid 1610743964;
    property ZoomImageToFill: ICvsActionToggle readonly dispid 1610743965;
    property ZoomImageToFit: ICvsActionToggle readonly dispid 1610743966;
    property ZoomImageToMax: ICvsAction readonly dispid 1610743967;
    procedure MnuSensor_BeforePopup; dispid 1610743968;
    procedure SetForeColor(color: OLE_COLOR); dispid 1610743969;
    procedure SetBackColor(color: OLE_COLOR); dispid 1610743970;
    property SelectedFontSize: Single dispid 1610743971;
    property SelectedFontName: WideString dispid 1610743973;
    procedure add_SelectedFontChanged(const value: _EventHandler); dispid 1610743975;
    procedure remove_SelectedFontChanged(const value: _EventHandler); dispid 1610743976;
    procedure add_EnablesUpdated(const value: _EventHandler); dispid 1610743977;
    procedure remove_EnablesUpdated(const value: _EventHandler); dispid 1610743978;
    function ProcessShortcutKey(keyData: Keys): WordBool; dispid 1610743979;
    procedure add_NetworkTreeRefreshNeeded(const value: _EventHandler); dispid 1610743980;
    procedure remove_NetworkTreeRefreshNeeded(const value: _EventHandler); dispid 1610743981;
    property EditMaxGuiConnections: ICvsAction readonly dispid 1610743982;
    property EditJobServerSettings: ICvsAction readonly dispid 1610743983;
    procedure add_RepeatingTriggerChanged(const value: _EventHandler); dispid 1610743984;
    procedure remove_RepeatingTriggerChanged(const value: _EventHandler); dispid 1610743985;
    procedure add_JobSaving(const value: _EventHandler); dispid 1610743986;
    procedure remove_JobSaving(const value: _EventHandler); dispid 1610743987;
    procedure PromptForInSightRestart; dispid 1610743988;
    property ClassicUserRefreshNeeded: _EventHandler dispid 1610743989;
  end;

// *********************************************************************//
// Interface: _CvsInSightDisplayRecorder
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {7B3B0ABE-C699-3843-B98F-618C7E3166A5}
// *********************************************************************//
  _CvsInSightDisplayRecorder = interface(IDispatch)
    ['{7B3B0ABE-C699-3843-B98F-618C7E3166A5}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    procedure Dispose; safecall;
    procedure add_PlaybackFolderChanged(const value: _EventHandler); safecall;
    procedure remove_PlaybackFolderChanged(const value: _EventHandler); safecall;
    procedure add_PlaybackIndexChanged(const value: _EventHandler); safecall;
    procedure remove_PlaybackIndexChanged(const value: _EventHandler); safecall;
    procedure add_PlaybackFolderSizeChanged(const value: _EventHandler); safecall;
    procedure remove_PlaybackFolderSizeChanged(const value: _EventHandler); safecall;
    procedure add_RecordFolderChanged(const value: _EventHandler); safecall;
    procedure remove_RecordFolderChanged(const value: _EventHandler); safecall;
    function Get_Options: IUnknown; safecall;
    procedure _Set_Options(const pRetVal: IUnknown); safecall;
    function Get_AlwaysShowOptions: WordBool; safecall;
    procedure Set_AlwaysShowOptions(pRetVal: WordBool); safecall;
    function Get_RecordActive: WordBool; safecall;
    procedure Set_RecordActive(pRetVal: WordBool); safecall;
    function Get_RecordFilename: WideString; safecall;
    function Get_RecordFilenameFormat: WideString; safecall;
    procedure Set_RecordFilenameFormat(const pRetVal: WideString); safecall;
    function Get_RecordFolder: WideString; safecall;
    procedure Set_RecordFolder(const pRetVal: WideString); safecall;
    function Get_RecordImageCount: Integer; safecall;
    function Get_RecordIndex: Integer; safecall;
    procedure Set_RecordIndex(pRetVal: Integer); safecall;
    function Get_RecordMax: Integer; safecall;
    procedure Set_RecordMax(pRetVal: Integer); safecall;
    function Get_RecordMaxLimit: Integer; safecall;
    function Get_RecordMode: CvsRecordMode; safecall;
    procedure Set_RecordMode(pRetVal: CvsRecordMode); safecall;
    function Get_RecordResolution: CvsImageResolution; safecall;
    procedure Set_RecordResolution(pRetVal: CvsImageResolution); safecall;
    function Get_RecordBitDepth: CvsColorBitDepth; safecall;
    procedure Set_RecordBitDepth(pRetVal: CvsColorBitDepth); safecall;
    procedure Set_RecordWatchCell(const pRetVal: WideString); safecall;
    function Get_RecordWatchCell: WideString; safecall;
    procedure GhostMethod__CvsInSightDisplayRecorder_180_1; safecall;
    procedure GhostMethod__CvsInSightDisplayRecorder_184_2; safecall;
    function Get_PlaybackAllowed: WordBool; safecall;
    function Get_PlayActive: WordBool; safecall;
    procedure Set_PlayActive(pRetVal: WordBool); safecall;
    function Get_PlaybackContinuous: WordBool; safecall;
    procedure Set_PlaybackContinuous(pRetVal: WordBool); safecall;
    function Get_PlaybackDelay: Double; safecall;
    procedure Set_PlaybackDelay(pRetVal: Double); safecall;
    function Get_PlaybackFilename: WideString; safecall;
    procedure Set_PlaybackFilename(const pRetVal: WideString); safecall;
    function Get_PlaybackFolder: WideString; safecall;
    procedure Set_PlaybackFolder(const pRetVal: WideString); safecall;
    function Get_RecentPlaybackFolders: PSafeArray; safecall;
    procedure AddRecentPlaybackFolder(const folder: WideString); safecall;
    function Get_PlaybackImageCount: Integer; safecall;
    function Get_PlaybackIndex: Integer; safecall;
    procedure Set_PlaybackIndex(pRetVal: Integer); safecall;
    function Get_DeferImageLoading: IDisposable; safecall;
    procedure Refresh; safecall;
    function GetPlaybackFiles: PSafeArray; safecall;
    procedure PlayFirst; safecall;
    procedure PlayPrevious; safecall;
    procedure PlayNext; safecall;
    procedure PlayLast; safecall;
    property ToString: WideString read Get_ToString;
    property Options: IUnknown read Get_Options write _Set_Options;
    property AlwaysShowOptions: WordBool read Get_AlwaysShowOptions write Set_AlwaysShowOptions;
    property RecordActive: WordBool read Get_RecordActive write Set_RecordActive;
    property RecordFilename: WideString read Get_RecordFilename;
    property RecordFilenameFormat: WideString read Get_RecordFilenameFormat write Set_RecordFilenameFormat;
    property RecordFolder: WideString read Get_RecordFolder write Set_RecordFolder;
    property RecordImageCount: Integer read Get_RecordImageCount;
    property RecordIndex: Integer read Get_RecordIndex write Set_RecordIndex;
    property RecordMax: Integer read Get_RecordMax write Set_RecordMax;
    property RecordMaxLimit: Integer read Get_RecordMaxLimit;
    property RecordMode: CvsRecordMode read Get_RecordMode write Set_RecordMode;
    property RecordResolution: CvsImageResolution read Get_RecordResolution write Set_RecordResolution;
    property RecordBitDepth: CvsColorBitDepth read Get_RecordBitDepth write Set_RecordBitDepth;
    property RecordWatchCell: WideString read Get_RecordWatchCell write Set_RecordWatchCell;
    property PlaybackAllowed: WordBool read Get_PlaybackAllowed;
    property PlayActive: WordBool read Get_PlayActive write Set_PlayActive;
    property PlaybackContinuous: WordBool read Get_PlaybackContinuous write Set_PlaybackContinuous;
    property PlaybackDelay: Double read Get_PlaybackDelay write Set_PlaybackDelay;
    property PlaybackFilename: WideString read Get_PlaybackFilename write Set_PlaybackFilename;
    property PlaybackFolder: WideString read Get_PlaybackFolder write Set_PlaybackFolder;
    property RecentPlaybackFolders: PSafeArray read Get_RecentPlaybackFolders;
    property PlaybackImageCount: Integer read Get_PlaybackImageCount;
    property PlaybackIndex: Integer read Get_PlaybackIndex write Set_PlaybackIndex;
    property DeferImageLoading: IDisposable read Get_DeferImageLoading;
  end;

// *********************************************************************//
// DispIntf:  _CvsInSightDisplayRecorderDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {7B3B0ABE-C699-3843-B98F-618C7E3166A5}
// *********************************************************************//
  _CvsInSightDisplayRecorderDisp = dispinterface
    ['{7B3B0ABE-C699-3843-B98F-618C7E3166A5}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    procedure Dispose; dispid 1610743812;
    procedure add_PlaybackFolderChanged(const value: _EventHandler); dispid 1610743813;
    procedure remove_PlaybackFolderChanged(const value: _EventHandler); dispid 1610743814;
    procedure add_PlaybackIndexChanged(const value: _EventHandler); dispid 1610743815;
    procedure remove_PlaybackIndexChanged(const value: _EventHandler); dispid 1610743816;
    procedure add_PlaybackFolderSizeChanged(const value: _EventHandler); dispid 1610743817;
    procedure remove_PlaybackFolderSizeChanged(const value: _EventHandler); dispid 1610743818;
    procedure add_RecordFolderChanged(const value: _EventHandler); dispid 1610743819;
    procedure remove_RecordFolderChanged(const value: _EventHandler); dispid 1610743820;
    property Options: IUnknown dispid 1610743821;
    property AlwaysShowOptions: WordBool dispid 1610743823;
    property RecordActive: WordBool dispid 1610743825;
    property RecordFilename: WideString readonly dispid 1610743827;
    property RecordFilenameFormat: WideString dispid 1610743828;
    property RecordFolder: WideString dispid 1610743830;
    property RecordImageCount: Integer readonly dispid 1610743832;
    property RecordIndex: Integer dispid 1610743833;
    property RecordMax: Integer dispid 1610743835;
    property RecordMaxLimit: Integer readonly dispid 1610743837;
    property RecordMode: CvsRecordMode dispid 1610743838;
    property RecordResolution: CvsImageResolution dispid 1610743840;
    property RecordBitDepth: CvsColorBitDepth dispid 1610743842;
    property RecordWatchCell: WideString dispid 1610743844;
    procedure GhostMethod__CvsInSightDisplayRecorder_180_1; dispid 1610743846;
    procedure GhostMethod__CvsInSightDisplayRecorder_184_2; dispid 1610743847;
    property PlaybackAllowed: WordBool readonly dispid 1610743848;
    property PlayActive: WordBool dispid 1610743849;
    property PlaybackContinuous: WordBool dispid 1610743851;
    property PlaybackDelay: Double dispid 1610743853;
    property PlaybackFilename: WideString dispid 1610743855;
    property PlaybackFolder: WideString dispid 1610743857;
    property RecentPlaybackFolders: {??PSafeArray}OleVariant readonly dispid 1610743859;
    procedure AddRecentPlaybackFolder(const folder: WideString); dispid 1610743860;
    property PlaybackImageCount: Integer readonly dispid 1610743861;
    property PlaybackIndex: Integer dispid 1610743862;
    property DeferImageLoading: IDisposable readonly dispid 1610743864;
    procedure Refresh; dispid 1610743865;
    function GetPlaybackFiles: {??PSafeArray}OleVariant; dispid 1610743866;
    procedure PlayFirst; dispid 1610743867;
    procedure PlayPrevious; dispid 1610743868;
    procedure PlayNext; dispid 1610743869;
    procedure PlayLast; dispid 1610743870;
  end;

// *********************************************************************//
// The Class CoCvsSensorFilmstripDisplay provides a Create and CreateRemote method to          
// create instances of the default interface ICvsSensorFilmstripDisplayInternal exposed by              
// the CoClass CvsSensorFilmstripDisplay. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsSensorFilmstripDisplay = class
    class function Create: ICvsSensorFilmstripDisplayInternal;
    class function CreateRemote(const MachineName: string): ICvsSensorFilmstripDisplayInternal;
  end;

// *********************************************************************//
// The Class CoCvsFilmstrip provides a Create and CreateRemote method to          
// create instances of the default interface ICvsFilmstripInternal exposed by              
// the CoClass CvsFilmstrip. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsFilmstrip = class
    class function Create: ICvsFilmstripInternal;
    class function CreateRemote(const MachineName: string): ICvsFilmstripInternal;
  end;

// *********************************************************************//
// The Class CoCvsPlaybackFilmstripDisplay provides a Create and CreateRemote method to          
// create instances of the default interface ICvsPlaybackFilmstripDisplayInternal exposed by              
// the CoClass CvsPlaybackFilmstripDisplay. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsPlaybackFilmstripDisplay = class
    class function Create: ICvsPlaybackFilmstripDisplayInternal;
    class function CreateRemote(const MachineName: string): ICvsPlaybackFilmstripDisplayInternal;
  end;

// *********************************************************************//
// The Class CoCvsInSightDisplay provides a Create and CreateRemote method to          
// create instances of the default interface ICvsInSightDisplayInternal exposed by              
// the CoClass CvsInSightDisplay. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsInSightDisplay = class
    class function Create: ICvsInSightDisplayInternal;
    class function CreateRemote(const MachineName: string): ICvsInSightDisplayInternal;
  end;

// *********************************************************************//
// The Class CoDynamicValue provides a Create and CreateRemote method to          
// create instances of the default interface _DynamicValue exposed by              
// the CoClass DynamicValue. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoDynamicValue = class
    class function Create: _DynamicValue;
    class function CreateRemote(const MachineName: string): _DynamicValue;
  end;

// *********************************************************************//
// The Class CoDynamicValueComputed provides a Create and CreateRemote method to          
// create instances of the default interface _DynamicValueComputed exposed by              
// the CoClass DynamicValueComputed. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoDynamicValueComputed = class
    class function Create: _DynamicValueComputed;
    class function CreateRemote(const MachineName: string): _DynamicValueComputed;
  end;

// *********************************************************************//
// The Class CoCvsFilmstripPlayback provides a Create and CreateRemote method to          
// create instances of the default interface _CvsFilmstripPlayback exposed by              
// the CoClass CvsFilmstripPlayback. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsFilmstripPlayback = class
    class function Create: _CvsFilmstripPlayback;
    class function CreateRemote(const MachineName: string): _CvsFilmstripPlayback;
  end;

// *********************************************************************//
// The Class CoCvsFilmstripResultsQueue provides a Create and CreateRemote method to          
// create instances of the default interface _CvsFilmstripResultsQueue exposed by              
// the CoClass CvsFilmstripResultsQueue. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsFilmstripResultsQueue = class
    class function Create: _CvsFilmstripResultsQueue;
    class function CreateRemote(const MachineName: string): _CvsFilmstripResultsQueue;
  end;

// *********************************************************************//
// The Class CoCvsFilmstripResultsQueueOptions provides a Create and CreateRemote method to          
// create instances of the default interface _CvsFilmstripResultsQueueOptions exposed by              
// the CoClass CvsFilmstripResultsQueueOptions. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsFilmstripResultsQueueOptions = class
    class function Create: _CvsFilmstripResultsQueueOptions;
    class function CreateRemote(const MachineName: string): _CvsFilmstripResultsQueueOptions;
  end;

// *********************************************************************//
// The Class CoCvsFilmstripResultsQueueSettings provides a Create and CreateRemote method to          
// create instances of the default interface _CvsFilmstripResultsQueueSettings exposed by              
// the CoClass CvsFilmstripResultsQueueSettings. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsFilmstripResultsQueueSettings = class
    class function Create: _CvsFilmstripResultsQueueSettings;
    class function CreateRemote(const MachineName: string): _CvsFilmstripResultsQueueSettings;
  end;

// *********************************************************************//
// The Class CoCvsInSightDisplayEdit provides a Create and CreateRemote method to          
// create instances of the default interface _CvsInSightDisplayEdit exposed by              
// the CoClass CvsInSightDisplayEdit. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsInSightDisplayEdit = class
    class function Create: _CvsInSightDisplayEdit;
    class function CreateRemote(const MachineName: string): _CvsInSightDisplayEdit;
  end;

// *********************************************************************//
// The Class CoCvsInSightDisplayRecorder provides a Create and CreateRemote method to          
// create instances of the default interface _CvsInSightDisplayRecorder exposed by              
// the CoClass CvsInSightDisplayRecorder. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsInSightDisplayRecorder = class
    class function Create: _CvsInSightDisplayRecorder;
    class function CreateRemote(const MachineName: string): _CvsInSightDisplayRecorder;
  end;

implementation

uses ComObj;

class function CoCvsSensorFilmstripDisplay.Create: ICvsSensorFilmstripDisplayInternal;
begin
  Result := CreateComObject(CLASS_CvsSensorFilmstripDisplay) as ICvsSensorFilmstripDisplayInternal;
end;

class function CoCvsSensorFilmstripDisplay.CreateRemote(const MachineName: string): ICvsSensorFilmstripDisplayInternal;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsSensorFilmstripDisplay) as ICvsSensorFilmstripDisplayInternal;
end;

class function CoCvsFilmstrip.Create: ICvsFilmstripInternal;
begin
  Result := CreateComObject(CLASS_CvsFilmstrip) as ICvsFilmstripInternal;
end;

class function CoCvsFilmstrip.CreateRemote(const MachineName: string): ICvsFilmstripInternal;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsFilmstrip) as ICvsFilmstripInternal;
end;

class function CoCvsPlaybackFilmstripDisplay.Create: ICvsPlaybackFilmstripDisplayInternal;
begin
  Result := CreateComObject(CLASS_CvsPlaybackFilmstripDisplay) as ICvsPlaybackFilmstripDisplayInternal;
end;

class function CoCvsPlaybackFilmstripDisplay.CreateRemote(const MachineName: string): ICvsPlaybackFilmstripDisplayInternal;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsPlaybackFilmstripDisplay) as ICvsPlaybackFilmstripDisplayInternal;
end;

class function CoCvsInSightDisplay.Create: ICvsInSightDisplayInternal;
begin
  Result := CreateComObject(CLASS_CvsInSightDisplay) as ICvsInSightDisplayInternal;
end;

class function CoCvsInSightDisplay.CreateRemote(const MachineName: string): ICvsInSightDisplayInternal;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsInSightDisplay) as ICvsInSightDisplayInternal;
end;

class function CoDynamicValue.Create: _DynamicValue;
begin
  Result := CreateComObject(CLASS_DynamicValue) as _DynamicValue;
end;

class function CoDynamicValue.CreateRemote(const MachineName: string): _DynamicValue;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_DynamicValue) as _DynamicValue;
end;

class function CoDynamicValueComputed.Create: _DynamicValueComputed;
begin
  Result := CreateComObject(CLASS_DynamicValueComputed) as _DynamicValueComputed;
end;

class function CoDynamicValueComputed.CreateRemote(const MachineName: string): _DynamicValueComputed;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_DynamicValueComputed) as _DynamicValueComputed;
end;

class function CoCvsFilmstripPlayback.Create: _CvsFilmstripPlayback;
begin
  Result := CreateComObject(CLASS_CvsFilmstripPlayback) as _CvsFilmstripPlayback;
end;

class function CoCvsFilmstripPlayback.CreateRemote(const MachineName: string): _CvsFilmstripPlayback;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsFilmstripPlayback) as _CvsFilmstripPlayback;
end;

class function CoCvsFilmstripResultsQueue.Create: _CvsFilmstripResultsQueue;
begin
  Result := CreateComObject(CLASS_CvsFilmstripResultsQueue) as _CvsFilmstripResultsQueue;
end;

class function CoCvsFilmstripResultsQueue.CreateRemote(const MachineName: string): _CvsFilmstripResultsQueue;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsFilmstripResultsQueue) as _CvsFilmstripResultsQueue;
end;

class function CoCvsFilmstripResultsQueueOptions.Create: _CvsFilmstripResultsQueueOptions;
begin
  Result := CreateComObject(CLASS_CvsFilmstripResultsQueueOptions) as _CvsFilmstripResultsQueueOptions;
end;

class function CoCvsFilmstripResultsQueueOptions.CreateRemote(const MachineName: string): _CvsFilmstripResultsQueueOptions;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsFilmstripResultsQueueOptions) as _CvsFilmstripResultsQueueOptions;
end;

class function CoCvsFilmstripResultsQueueSettings.Create: _CvsFilmstripResultsQueueSettings;
begin
  Result := CreateComObject(CLASS_CvsFilmstripResultsQueueSettings) as _CvsFilmstripResultsQueueSettings;
end;

class function CoCvsFilmstripResultsQueueSettings.CreateRemote(const MachineName: string): _CvsFilmstripResultsQueueSettings;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsFilmstripResultsQueueSettings) as _CvsFilmstripResultsQueueSettings;
end;

class function CoCvsInSightDisplayEdit.Create: _CvsInSightDisplayEdit;
begin
  Result := CreateComObject(CLASS_CvsInSightDisplayEdit) as _CvsInSightDisplayEdit;
end;

class function CoCvsInSightDisplayEdit.CreateRemote(const MachineName: string): _CvsInSightDisplayEdit;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsInSightDisplayEdit) as _CvsInSightDisplayEdit;
end;

class function CoCvsInSightDisplayRecorder.Create: _CvsInSightDisplayRecorder;
begin
  Result := CreateComObject(CLASS_CvsInSightDisplayRecorder) as _CvsInSightDisplayRecorder;
end;

class function CoCvsInSightDisplayRecorder.CreateRemote(const MachineName: string): _CvsInSightDisplayRecorder;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsInSightDisplayRecorder) as _CvsInSightDisplayRecorder;
end;

end.
