unit CognexFTP;
interface
uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,SyncObjs,TelnetDisplay,
  Dialogs, StdCtrls, IdBaseComponent, IdComponent, IdFTP,  ComCtrls,ExtCtrls,CognexTypes;

type

  TFTPLoginEvent = procedure(Sender : TObject;LoginState : Boolean) of object;
  TFTPTransferEvent = procedure(Sender : TObject; VStream: TStream) of object;
  TFileListChangedEvent= procedure(Sender : TObject) of object;
  TJobListChangedEvent= procedure(Sender : TObject) of object;
  TFTPFileImportedEvent = procedure(FPath : String;Success : Boolean) of object;
  TFTPActoinCompleteEvent = procedure(ActionType : TFTPActionTypes;Success : Boolean) of object;

  TExtendedFTP = Class(TIdFTP)
  private
    FLocalOnAfterPut: TNotifyEvent;
    procedure SetOnAfterPut(const Value: TNotifyEvent);
   public
   property OnAfterPut : TNotifyEvent read FLocalOnAfterPut write SetOnAfterPut;
   end;

  TCognexFTPHandler = class(TObject)
    private
    FExternalAllFileList : TStringList;
    FExternalJobList : TStringList;
    FIsloggedIn: Boolean;
    FTP : TExtendedFTP;
//    FOnFileExported: TFTPTransferEvent;
//    FOnFileImported: TFTPTransferEvent;
//    FOnJobListChanged: TFileListChangedEvent;
//    FOnFileListChanged: TFileListChangedEvent;
    FOnActionComplete: TFTPActoinCompleteEvent;
    procedure SentFile(ASender: TObject; VStream: TStream);
    procedure GotFile(ASender: TObject);
    procedure DeleteAllJobs;
    procedure SetAllFileList(const Value: TStringList);
    procedure SetJobList(const Value: TStringList);
    public
    DebugMemo : TTelnetDisplay; // Externally Set
    constructor Create;
    destructor Destroy; override;

    function ConnectandLogin(IPAdddress: String;Port : Integer;username : String;Password : String): Boolean;
    function Login: String;
    function UpdateJobList(GetDetails : Boolean): Boolean;
    procedure UpdateFileList(GetDetails : Boolean);
    procedure BackupAllFiles;
    procedure DeleteAllFilesWithExtension(FileExt : String);
    procedure SendJob;
    procedure ExportFileCalled(FName :String;DestDir : String);
    function ImportFileCalled(FPath :String;CanOverwrite : Boolean):Boolean;
    procedure GetFilesToListBox( FStrings : TListBox;GetDetails : Boolean);
    procedure GetJobListToListBox( var FStrings: TListBox; GetDetails: Boolean);
    procedure GetFilesToStringList( var FStrings : TStringList;GetDetails : Boolean);
    function GetJobListToStringList( var FStrings: TStringList; GetDetails: Boolean): Boolean;
    property IsLoggedIn : Boolean read FIsloggedIn;
    property OnFTPActionComplete : TFTPActoinCompleteEvent read FOnActionComplete write FOnActionComplete;
//    property OnFileExported : TFTPTransferEvent read FOnFileExported write FOnFileExported;
//    property OnFileImported : TFTPTransferEvent read FOnFileImported write FOnFileImported;
    property ExternalAllFileList : TStringList read FExternalAllFileList write SetAllFileList;
    property ExternalJobList : TStringList read FExternalJobList write SetJobList;
    end;

implementation

{ TCognexFTPHandler }

procedure TCognexFTPHandler.BackupAllFiles;
begin

end;

function TCognexFTPHandler.ConnectandLogin(IPAdddress: String; Port: Integer; username, Password: String): Boolean;
begin
      try
      FTP.Port := Port;
      FTP.Host := IPAdddress;
      FTP.Username := Username;
      FTP.Password := Password;
      FTP.Connect;
      Result := True;
      Login;
      except
      Result := False;
      end;


end;

constructor TCognexFTPHandler.Create;
begin
  inherited;
  FIsloggedIn := False;
  FTP := TExtendedFTP.Create();
  FTP.OnAfterGet := SentFile;
  FTP.OnAfterPut := GotFile;
end;

destructor TCognexFTPHandler.Destroy;
begin
  FTP.Free;
  inherited;
end;


procedure TCognexFTPHandler.DeleteAllJobs;
begin

end;

procedure TCognexFTPHandler.ExportFileCalled(FName, DestDir: String);
begin
FTP.Get(FName,DestDir,True,False);
end;

Function TCognexFTPHandler.ImportFileCalled(FPath: String; CanOverwrite: Boolean):Boolean;
var
LocalFilename : String;
begin
Result := True;
LocalFilename := ExtractFileName(FPath);
try
FTP.Put(FPath,LocalFilename,False);
except
if assigned(FOnActionComplete) then FOnActionComplete(taImportFile,False);
Result := False;
end;
end;

procedure TCognexFTPHandler.GetFilesToListBox(FStrings : TListBox;GetDetails : Boolean);
begin
if  assigned(FStrings) then
  begin
  FTP.List(FStrings.Items, '', false);
  end;
end;




procedure TCognexFTPHandler.GetFilesToStringList(var FStrings: TStringList; GetDetails: Boolean);
begin
if  assigned(FStrings) then
  begin
  FTP.List(FStrings, '', GetDetails);
  end;
end;

procedure TCognexFTPHandler.GetJobListToListBox(var FStrings : TListBox;GetDetails : Boolean);
begin
if  assigned(FStrings) then
  begin
  FTP.List(FStrings.Items, '*.job', false);
  end;
end;


function TCognexFTPHandler.GetJobListToStringList(var FStrings: TStringList; GetDetails: Boolean): Boolean;
begin
Result := False;
if  assigned(FStrings) then
  begin
  try
  Result := True;
  FTP.List(FStrings, '*.job', false);
  except
  Result := False;
  end;
  end;
end;

procedure TCognexFTPHandler.SentFile(ASender: TObject; VStream: TStream);
begin
  If assigned(FOnActionComplete) then FOnActionComplete(TaExportFile,True);
end;


function TCognexFTPHandler.Login: String;
var
PosText : Integer;
begin
FTP.Login;
if assigned(FTP.LoginMsg) then
  begin
  Result := FTP.LoginMsg.Text[0];
  PosText := Pos('logged in',Result);
  if PosText > 0 then
    begin
    FIsloggedIn := True;
    If assigned(FOnActionComplete) then FOnActionComplete(taConnectandLogin,True);
//    If assigned(FOnLogin) then FOnLogin(Self,True);
    end
  else
    begin
    FIsloggedIn := False;
    If assigned(FOnActionComplete) then FOnActionComplete(taConnectandLogin,False);
//    If assigned(FOnLogin) then FOnLogin(Self,False);
    end;
  end;
end;

procedure TCognexFTPHandler.SendJob;
begin
end;

procedure TCognexFTPHandler.GotFile(ASender: TObject);
begin
if assigned(FOnActionComplete) then FOnActionComplete(taImportFile,True);
end;

function TCognexFTPHandler.UpdateJobList(GetDetails : Boolean): Boolean;
begin
if GetJobListToStringList(FExternalJobList,GetDetails) then
  begin
  if assigned(FOnActionComplete) then  FOnActionComplete(taGetJobList,True)
  end
else
  begin
  if assigned(FOnActionComplete) then  FOnActionComplete(taGetJobList,False)
  end
end;


procedure TCognexFTPHandler.UpdateFileList(GetDetails: Boolean);
begin
GetFilesToStringList(FExternalAllFileList,GetDetails);
end;

procedure TCognexFTPHandler.DeleteAllFilesWithExtension(FileExt: String);
var
DelList : TStringList;
FNum : Integer;
FName : String;
TestExt : String;
FExt : String;
begin
TestExt := Uppercase(FileExt);
UpdateFileList(False);
if ExternalAllFileList.Count > 0 then
  begin
  DelList := TStringList.Create;
  try
  if FileExt[1] = '.' then FileExt := Copy(FileExt,2,100);
  for FNum := 0 To ExternalAllFileList.Count-1 do
    begin
    FName := Uppercase(ExternalAllFileList[FNum]);
    FExt := UpperCase(ExtractFileExt(FName));
    if Fext = TestExt then DelList.add(FName);
    end;
  if DelList.Count > 0 then
    begin
    for FNum := 0 To DelList.Count-1 do
      begin
      try
      FTP.Delete(DelList[Fnum]);
      except
      end;
      end;
    end;
  finally
  DelList.Free;
  end;
  end;

end;


procedure TCognexFTPHandler.SetAllFileList(const Value: TStringList);
begin
  FExternalAllFileList := Value;

end;

procedure TCognexFTPHandler.SetJobList(const Value: TStringList);
begin
  FExternalJobList := Value;
end;

{ TExtendedFTP }

procedure TExtendedFTP.SetOnAfterPut(const Value: TNotifyEvent);
begin
  FonAfterPut := Value;
end;




end.
