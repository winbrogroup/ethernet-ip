unit DummyCognex;

{$DEFINE HIGHDEBUG}
{$DEFINE GRID_MODE_InVALID}

interface
uses CoreCNC, CNCTypes, CNC32, Classes, Windows, Graphics, SysUtils, NCNames,
  CNCAbs, Named, Controls,ExtCtrls,ActiveX,StdCtrls,Grids,Buttons,
  AbstractScript,IScriptInterface,INamedInterface,IniFiles,Forms;


type
  TCameraDisplaySize = (tcdUnknown,tcdSmall,tcdWide,tcdFull);


  TCameraState = record
     PixelWidth : Integer;
     PixelHeight : Integer;
     Scale : Double;
     FitMode : Integer;
     CentreModeInteger : Integer;
     CircleRadius : Double;
     aaXHairOn : Integer;
     CircleOn: Integer;
     XHairColorNumber : Integer;
     CircleColorNumber : Integer;
     Exposure          : Double;
     JobScaling          : Double;
     XOffset        : Double;
     YOffset        : Double;
     OffsetIsManual    : Integer;
     end;

  TDummyCognexDisplay = class(TExpandDisplay)
  private
    NoCognex : Boolean;
    LastProgramName : String;
    UseFTPServer : Boolean;
    GoVisibleOnFit : Boolean;
    BackImageFileName : String;
    ImageOrientation : Integer;
    FirstShow : Boolean;
    EnableCameraUpdate : Boolean;
    UpdateCalibration : Boolean;
    DisplayModeEstablished : Boolean;

    FirstPaint : Boolean;
    IsClosing : Boolean;
    ConnectString : String;
    CameraConnected : Boolean;
    ConnectRetry : Integer;
    OfflineRetry : Integer;
    LatchTelnet : Boolean;
    LatchJob : Boolean;
    ManualOffsetMode : Boolean;
    InCycle : Boolean;
    DisplayPanel  : TPanel;
    FTPPanel      : TPanel;
    TelnetPanel   : TPanel;
    MessPanel : TPanel;
    StatusPanel : TPanel;
    TelnetLatch : TCheckBox;
    Image : TImage;
    LeftDisplayPanel : TPanel;
    CameraParentPanel : TPanel;

    VisionLatchJobTag         : TAbstractTag;
    VisionResponseTag         : TAbstractTag;
    VisionCommandStatusTag    : TAbstractTag;
    VisionErrorMessTag        : TAbstractTag;
    VisionFunctionIndexTag    : TAbstractTag;
    VisionCanTalkTag          : TAbstractTag;
    VisionParam1Tag           : TAbstractTag;
    VisionParam2Tag           : TAbstractTag;
    VisionParam3Tag           : TAbstractTag;
    VisionRunFunctionTag      : TAbstractTag;
    CurrentJobNameTag         : TAbstractTag;
    SceneScaleTag             : TAbstractTag;
    JogIncFineTag             : TAbstractTag;
    JogIncMediumTag           : TAbstractTag;
    JogIncCoarseTag           : TAbstractTag;
    COXTag                    : TAbstractTag;
    COFOVTag                  : TAbstractTag;
    CircleIsOnTag             : TAbstractTag;
    XHairIsOnTag              : TAbstractTag;
    VisionIdleTag             : TAbstractTag;
    XHairControlsInValidTag   : TAbstractTag;
    VisionReqDisconnectTag    : TAbstractTag;
    VisionDisconnectState     : TAbstractTag;
    VisionReqReconnectTag     : TAbstractTag;
    ResetExposureTag               : TAbstractTag;
    LaunchTag                 : TAbstractTag;
    OffsetIsManualTag         : TAbstractTag;
    OnlineTag                 : TAbstractTag;
    XoffsetTag                : TAbstractTag;
    YoffsetTag                : TAbstractTag;
    ScalingTag                : TAbstractTag;
    JobPassedTag              : TAbstractTag;
    ErrorStackCountTag        : TAbstractTag;
    SystemResetTag            : TAbstractTag;
    EstablishXHairTag         : TAbstractTag;
    RegisterScreenMode        : TAbstractTag;
    CameraConnectedTag        : TAbstractTag;
    IsReadyTag                : TAbstractTag;
    DisableXhairButtonTag     : TAbstractTag;
    SnapShotWGTag             : TAbstractTag;
    SnapShotNGTag             : TAbstractTag;
    JobDataTag                : TAbstractTag;


//  TagEvent(Name + 'RegisterScreenMode'  , EdgeFalling, TMethodTag, RegisterScreenMode);

    procedure DummyTagEvent(ATag: TAbstractTag);

    {$IFDEF GRID_MODE_VALID}
    procedure ShowGrid (ATag: TAbstractTag);// DisplayMode 3
    {$ENDIF}

  protected
    procedure Resize; override;
    procedure Paint; override;
  public
    constructor Create(aOwner : TComponent); override;
    procedure InstallComponent(Params : TParamStrings); override;
    procedure Installed; override;
    procedure Repaint; override;
    procedure IShutdown; override;
 end;

implementation


{ TCognexDisplay }

constructor TDummyCognexDisplay.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
  Name := 'DummyCognexDisplay';
  NoCognex := True;

end;



procedure TDummyCognexDisplay.Paint;
begin
  inherited;
  if FirstPaint then
    begin
    FirstPaint := False;
//    TestIndicatorPanel.Redraw;
    end;
end;



procedure TDummyCognexDisplay.Installed;
begin
  inherited Installed;
  TagEvent(Name + 'ShowTelnet', EdgeFalling, TMethodTag, DummyTagEvent);
  TagEvent(Name + 'ShowCamera', EdgeFalling, TMethodTag, DummyTagEvent);
  TagEvent(Name + 'ShowErrors' , EdgeFalling, TMethodTag, DummyTagEvent);
  {$IFDEF GRID_MODE_VALID}
  TagEvent(Name + 'ShowGrid', EdgeFalling, TMethodTag, ShowGrid);
  {$ENDIF}
  TagEvent(Name + 'ShowSystem', EdgeFalling, TMethodTag, DummyTagEvent);
  TagEvent(Name + 'RunFunction', EdgeRising, TMethodTag, DummyTagEvent);
  SystemResetTag := TagEvent('ResetComplete', EdgeFalling, TMethodTag, DummyTagEvent);
  EstablishXHairTag := TagEvent(Name + 'EstablishCrossHair', EdgeFalling, TMethodTag, DummyTagEvent);

  TagEvent('NC1InCycle'  , EdgeChange, TMethodTag, DummyTagEvent);
  TagEvent(Name + 'XHairReset'    , EdgeFalling, TMethodTag, DummyTagEvent);
  TagEvent(Name + 'XHairUp'    , EdgeFalling, TMethodTag, DummyTagEvent);
  TagEvent(Name + 'XHairDown'  , EdgeFalling, TMethodTag, DummyTagEvent);
  TagEvent(Name + 'XHairLeft'  , EdgeFalling, TMethodTag, DummyTagEvent);
  TagEvent(Name + 'XHairRight' , EdgeFalling, TMethodTag, DummyTagEvent);
  TagEvent(Name + 'ReqXHairOn' , EdgeChange, TMethodTag, DummyTagEvent);
  TagEvent(Name + 'XHairJogDown'  , EdgeChange, TMethodTag, DummyTagEvent);
  TagEvent(Name + 'XHairJogUp'  , EdgeChange, TMethodTag, DummyTagEvent);
  TagEvent(Name + 'XHairJogLeft'  , EdgeChange, TMethodTag, DummyTagEvent);
  TagEvent(Name + 'XHairJogRight'  , EdgeChange, TMethodTag, DummyTagEvent);


  TagEvent(Name + 'CircleUp'  , EdgeFalling, TMethodTag, DummyTagEvent);
  TagEvent(Name + 'CircleDown'  , EdgeFalling, TMethodTag, DummyTagEvent);
  TagEvent(Name + 'CircleJogUp'  , EdgeFalling, TMethodTag, DummyTagEvent);
  TagEvent(Name + 'CircleJogDown'  , EdgeFalling, TMethodTag, DummyTagEvent);
  TagEvent(Name + 'ReqCircleOn'  , EdgeChange, TMethodTag, DummyTagEvent);
  TagEvent(Name + 'CircleColorNumber'  , EdgeChange, TIntegerTag, DummyTagEvent);
  TagEvent(Name + 'XHairColorNumber'  , EdgeChange, TIntegerTag, DummyTagEvent);
  TagEvent(Name + 'ScaleUp'  , EdgeFalling, TMethodTag, DummyTagEvent);
  TagEvent(Name + 'ScaleDown'  , EdgeFalling, TMethodTag, DummyTagEvent);
  TagEvent(Name + 'ScaleJogUp'  , EdgeChange, TMethodTag, DummyTagEvent);
  TagEvent(Name + 'ScaleJogDown'  , EdgeChange, TMethodTag, DummyTagEvent);

  TagEvent(Name + 'RequestScaleChange'  , EdgeChange, TDoubleTag, DummyTagEvent);
  TagEvent(Name + 'CentreOnXHair'  ,EdgeChange, TDoubleTag, DummyTagEvent);

  TagEvent(Name + 'IncrementMode',EdgeChange,TIntegerTag,DummyTagEvent);

  TagEvent(Name + 'FitToPanel'  , EdgeFalling, TMethodTag, DummyTagEvent);
  TagEvent(Name + 'FullScale'   , EdgeFalling, TMethodTag, DummyTagEvent);
  ResetExposureTag := TagEvent(Name + 'Exposure'  , EdgeFalling, TDoubleTag, DummyTagEvent);
  VisionLatchJobTag   := TagEvent(Name + 'LatchJob'  , EdgeChange, TIntegerTag, DummyTagEvent);


  VisionReqDisconnectTag   := TagEvent(Name +  'RequestDisconnect',EdgeFalling, TMethodTag,DummyTagEvent);
  VisionReqReconnectTag    := TagEvent(Name + 'RequestReconnect', EdgeFalling, TMethodTag, DummyTagEvent);


  TagEvent('RemoteFileLoad' , EdgeChange, TStringTag, DummyTagEvent);

  COFOVTag.AsInteger := 1;
  COXTag.AsInteger := 0;
  OnLineTag.AsInteger := 0;

  if NoCognex then
    begin
    CameraConnectedTag.AsInteger := 0;
    MessPanel.Visible := True;
    MessPanel.BringToFront;
    MessPanel.Caption := 'Running Dummy Cognex Mode';
    end
end;

procedure TDummyCognexDisplay.Repaint;
begin
  inherited;
end;

procedure TDummyCognexDisplay.InstallComponent(Params: TParamStrings);
var
 Buffer : String;
begin
  Self.Color := clWebPink;
  inherited InstallComponent(Params);
  FirstShow := True;
  FirstPaint:= True;


  VisionCommandStatusTag   := TagPublish(Name + 'CommandStatus',TIntegerTag);
  VisionErrorMessTag       := TagPublish(Name + 'ErrorMessage',TStringTag);
  VisionCanTalkTag         := TagPublish(Name + 'CanTalk',TIntegerTag);
  VisionResponseTag        := TagPublish(Name + 'Response',TStringTag);
  VisionFunctionIndexTag   := TagPublish(Name + 'FunctionIndex',TIntegerTag);
  VisionParam1Tag          := TagPublish(Name + 'Param1',TStringTag);
  VisionParam2Tag          := TagPublish(Name + 'Param2',TStringTag);
  VisionParam3Tag          := TagPublish(Name + 'Param3',TStringTag);
  CurrentJobNameTag        := TagPublish(Name + 'CurrentJob',TStringTag);
  SceneScaleTag            := TagPublish(Name + 'SceneScale',TDoubleTag);
  XHairControlsInValidTag  := TagPublish(Name + 'XHairControlsInValid',TIntegerTag);
  JogIncFineTag            := TagPublish(Name + 'JogIncFine',TIntegerTag);
  JogIncMediumTag          := TagPublish(Name + 'JogIncMedium',TIntegerTag);
  JogIncCoarseTag          := TagPublish(Name + 'JogIncCoarse',TIntegerTag);
  COXTag                   := TagPublish(Name + 'IsCentredOnXHair',TIntegerTag);
  COFOVTag                 := TagPublish(Name + 'IsCentredOnFOV',TIntegerTag);
  CircleIsOnTag            := TagPublish(Name + 'CircleIsOn',TIntegerTag);
  XHairIsOnTag             := TagPublish(Name + 'XHairIsOn',TIntegerTag);
  VisionIdleTag            := TagPublish(Name + 'IsIdle',TIntegerTag);
  VisionDisconnectState    := TagPublish(Name + 'DisconnectState',TIntegerTag);
  OffsetIsManualTag        := TagPublish(Name + 'OffsetIsManual',TIntegerTag);
  OnlineTag                := TagPublish(Name + 'OnLine',TIntegerTag);
  XoffsetTag               := TagPublish(Name + 'Xoffset',TDoubleTag);
  YoffsetTag               := TagPublish(Name + 'Yoffset',TDoubleTag);
  ScalingTag               := TagPublish(Name + 'Scaling',TDoubleTag);
  JobPassedTag             := TagPublish(Name + 'JobPassed',TIntegerTag);
  ErrorStackCountTag       := TagPublish(Name + 'ErrorStackCount',TIntegerTag);
  LaunchTag                := TagPublish('LaunchCognexFTPServer',TIntegerTag);
  RegisterScreenMode       := TagPublish(Name + 'RegisterScreenMode',TIntegerTag);
  CameraConnectedTag       := TagPublish(Name + 'CameraConnected',TIntegerTag);
  IsReadyTag               := TagPublish(Name + 'IsReady',TIntegerTag);
  JobDataTag               := TagPublish(Name + 'LastJobData',TStringTag);

  SnapShotNGTag            := TagPublish(Name + 'SnapShotNoGraphics',TIntegerTag);
  SnapShotWGTag            := TagPublish(Name + 'SnapShotWithGraphics',TIntegerTag);
  DisableXhairButtonTag    := TagPublish(Name + 'DisableXHairButton',TIntegerTag);



  MessPanel := TPanel.Create(nil);
  MessPanel.Parent := Self;
  with MessPanel do
    begin
    Font.Size := 12;
    Font.Color := clBlack;
    BorderStyle := bsNone;
    Left := 30;
    Top := 30;
    Width := 350;
    Height := 45;
    Visible := False;
    end;

  DisableXhairButtonTag.AsInteger := 1;

end;

procedure TDummyCognexDisplay.Resize;
begin
  inherited;
end;

{ TODO : New mode to be done }
{procedure TCognexDisplay.Resize;
begin
  inherited;
  if not DisplayModeEstablished then exit;
  CameraActionHandler.RequestRestore(False);
end;}











procedure TDummyCognexDisplay.DummyTagEvent(ATag: TAbstractTag);
var
TagVal : Double;
begin
if Assigned(ATag) then
  begin
  TagVal := ATag.AsInteger;
  if TagVal <> 0 then
  else
  end;
end;



procedure TDummyCognexDisplay.IShutdown;
begin
  inherited;

end;



initialization
  RegisterCNCClass(TDummyCognexDisplay);
end.
