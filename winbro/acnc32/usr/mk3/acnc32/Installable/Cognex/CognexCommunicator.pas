unit CognexCommunicator;

{$DEFINE HIGHDEBUG}

interface
uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,CoreCnC,CNC32,CNCTypes,
  StdCtrls,Buttons, ExtCtrls, ComCtrls,Menus,   Grids,CognexTelnetClient,TelnetDisplay,CognexTYpes,VisionCommand,
  IdBaseComponent, IdComponent, IdTCPServer, IdTelnetServer, IdTCPConnection, IdTCPClient, IdTelnet;
const
  CRLF = #$d + #$a;
Type
TServiceModes = (smIdle,smSending,smWaitingResponse,smGetNext,smException);
TCentreModes = (tcmScreen,tcmXHair);


TNCCompleteEvent              = procedure(Success: Boolean;
                                        Command : TCocgnexInputMessage;
                                        CommandType : TNCCommands;
                                        InternalResponse : String;
                                        ExternalResponse : String;
                                        ResponseState : Integer;
                                        ErrMess : String) of object;

TNCCommandFailEvent           = procedure(Sender : Tobject;ResponseState : Integer) of object;
TJobnameChangeEvent           = procedure(Sender : Tobject;JobName : String) of object;
TJobListLoadedEvent           = procedure(Sender : Tobject;JobCount : Integer) of object;
TStatusChangeEvent            = procedure(Sender : Tobject;Status : Boolean) of object;
TIdleStatusChangeEvent        = procedure(Sender : Tobject;Status : Boolean;var InLineErrorStatus : String;var InLineErrorMessage : String) of object;
TOnlineSateChangedEvent       = procedure(Sender : Tobject;OnlineStatus : Boolean) of object;
TCameraIsTalkingEvent         = procedure(Sender : TObject;ResponseRecieved : Boolean;CurrentJobname : String;TalkMode : TTalkingModes;Online : Boolean) of object;
TDefaultOnResetLoadedEvent    = procedure(Sender : TObject;Passed : Boolean) of object;
TXHairRestoreEvent            = procedure(Sender : TObject;Success : Boolean) of object;
TFitCompleteEvent             = procedure(Sender : TObject;Success : Boolean) of object;
TRunJobDoneEvent              = procedure(Sender : TObject;Success : Boolean) of object;
TJobCheckEvent                = procedure(Success: Boolean;Passed : Boolean) of object;
TLockOutEvent                 = procedure(Sender : TObject;ErrMess : String) of object;
TGetValueEvent                = procedure(Sender : TObject;Success  : Boolean;Value : String) of object;
TExposureChangedEvent         = procedure(Sender : TObject;Success  : Boolean;ExposureValue : Double) of object;
TFailedToGoOffLineEvent        = procedure(Sender : TObject) of object;

TCognexComunicator  = class(TObject)
  private
    CurrentServiceInterval : Integer;
    DefaultServiceInterval : Integer;
    LastServiceInterval    : Integer;

    LockedCount            : Integer;
    LockedMax              : Integer;
    ReportedMax            : Integer;
    GFLockCount            : Integer;

    InsipientJob : String;
    ServiceTimer    : TTimer;
    ResponseTimeout : TTimer;
    TelnetLoginTimer: TTimer;
    LoginTimeout    : TTimer;
    LockTimout      : TTimer;
    TelnetBroken : Boolean;
    FollowOnMode : Boolean;

    InRestore : Boolean;
    ResponseCount : Integer;
    CameraTelnetConnected : Boolean;
    ResetFailCount : Integer;
    FNCResponseString : String;
    FReturnCode : String;
    CommandList : TCognexCommandList;
    ServiceMode : TServiceModes;
    CurrentNCCommand : TCocgnexInputMessage;
    CurrentServiceCommand : TCocgnexInputMessage;
    FVerboseMode : TVModes;
    TelnetC :  TCognexTelnetClient;
    TelnetMemo : TTelnetDisplay;

    FCurrentJobName : String;
    FileCount : Integer;


    NCCommandInProgress : Boolean;
    NCCommandGoodComplete : Boolean;
    FOnNCCommandComplete : TNCCompleteEvent;
//    FOnServiceCommandComplete : TNCCompleteEvent;
//    FonIdle : TNotifyEvent;
//    FonBusy : TNotifyEvent;
    FonException : TNotifyEvent;
    FOnFailedIsTalking: TNotifyEvent;
    FOnJobNameChange: TJobnameChangeEvent;
    FOnLoginSuccess: TNotifyEvent;
    FOnLoginTimeout: TNotifyEvent;
    FONNCCommandFail: TNCCommandFailEvent;
    FOnJobListLoaded: TJobListLoadedEvent;
    FOnJobLoaded    : TStatusChangeEvent;
    FPassword: String;
    FUsername: String;
    FOnIsTalkingResponse: TCameraIsTalkingEvent;
    FOnDefaultOnResetLoaded: TDefaultOnResetLoadedEvent;
    FOnDefaultOnBufferLoaded: TDefaultOnResetLoadedEvent;

    FOnNCAvailable: TNotifyEvent;
    FOnTelnetBroken: TNotifyEvent;
    FOnGFLock      : TNotifyEvent;
    FOnIdleBusy: TIdleStatusChangeEvent;
    FOnConnectTelnetChange: TStatusChangeEvent;
    FOnLoggedIn: TNotifyEvent;
    FOnHealthyStateChanged: TStatusChangeEvent;
    FEmulatorMode: Boolean;
    FJobScaling: Double;
    FJobXOffset: Double;
    FJobYOffset: Double;
    FIsOnline : Boolean;
    LastOnlineState : Boolean;
    FOnLineChanged: TOnlineSateChangedEvent;
    FJobLoadCommand: String;
    FExternalLockBusy: Boolean;
    FIsIdle: Boolean;

    //NewMode After Here
    FResponseTimeoutInSeconds: Single;
    TimeoutEnabled : Boolean;

    //Telnet response callbacks
    FOnCameraRestoreDone          : TXHairRestoreEvent;
    FonCameraRestoreOnBufferDone  : TXHairRestoreEvent;
    FOnFitComplete                : TFitCompleteEvent;
    FonRunJobDone                 : TRunJobDoneEvent;
    FonFastJobRunDone             : TRunJobDoneEvent;
//    FonLiveAfterRunJob            : TRunJobDoneEvent;
    FonJobPassChecked             : TJobCheckEvent;
    FOnOnlineForImport: TOnlineSateChangedEvent;
    FJobloadTimeoutInSeconds: Single;
    FPostCommandsPending: Boolean;
    FFastRun: Boolean;
    FFastRunFirstSweep : Boolean;
    FBusyTimeOut: Integer;
    FonLockout: TLockOutEvent;
    FOnGotValue : TGetValueEvent;
    FInLineErrorMessage : String;
    FInLineErrorResponse : String;
    FLastPassCount: Integer;
    FOnUserExposureChanged: TExposureChangedEvent;
    FInsipientExposure: Double;
    FOnFailedToGoOffLine: TFailedToGoOffLineEvent;

    procedure TrimToSize;
    procedure AddDebug(DString : String;LowestMode : TVModes);
    procedure AddOutMess(DString : String;LowestMode : TVModes);
    procedure AddReplyMessage(DString : String;LowestMode : TVModes);
    procedure AddErrorMessage(DString : String;LowestMode : TVModes);
    procedure AddStatusMessage(DString : String;LowestMode : TVModes);
    procedure AddSuccessMessage(DString: String; LowestMode: TVModes);
    procedure ServiceCommands(Sender: Tobject);
    procedure ResponseTimedOut(Sender: Tobject);
    function GetIntegerReturnCode: Integer;
    function NCCommandOK: Boolean;
    function GetPendingServiceCommandCount: Integer;
    function GetNCCommandDone: Boolean;
    procedure GetTelnetResponse(Sender: Tobject; Response: String);
    function GetIsconnected: Boolean;
    procedure TelnetConnected(Sender: Tobject);
    procedure TelnetLoginSpeakingButNoWelcome(Sender: Tobject);
    procedure TelnetDisconnected(Sender: Tobject);
    procedure TelnetHealthyChanged(Sender : TObject; IsHealthy: Boolean);
    procedure TelnetLoggedin(Sender: Tobject);
    procedure DoDelayedLogin(Sender: TObject);
    procedure DoLoginTimedout(Sender: TObject);
    procedure HandleBusyLocked(Sender: TObject);
    procedure HandleTelnetException(Sender: TObject);
    procedure TerminateCurrentNCCommand(Success : Boolean;ResponseState : Integer;InternalResponse : String;ExternalResponse : String;ErrMessage :String); overload;
    procedure TerminateCurrentNCCommand(Success : Boolean;ResponseState : Integer;InternalResponse : String;ExternalResponse : String;ErrMessage :String;CommandsToFollow : Boolean); overload;

    procedure RunFollowOnCommands(Success : Boolean;ResponseState : Integer;Response : String;ErrMessage :String);

    procedure AddFollowCommand(CTYpe: TNCCommands;
                               Index: Integer;
                               CommandStr: String;
                               ErrorMessage : String;
                               ServiceInterval : Integer);


    procedure TerminateServiceCommand(Response: String);
    function GetNCFunctionPending: Boolean;
    function GetTelnetOK: Boolean;
    procedure SetExternalLockBusy(const Value: Boolean);
    procedure SetResponseTimeoutInSeconds(const Value: Single);
    procedure SetPostCommanddsPending(const Value: Boolean);
    procedure SetFastRunMode(const Value: Boolean);
    procedure SetBusyTimeout(const Value: Integer);
    procedure SetIsIdle(const Value: Boolean);

  public
    DebugTemp :           INteger;
    LastCommandStr         : String;
    PostCommandError    : Boolean;
    JobList : TStringList;
    Constructor Create;
    destructor Destroy; override;
//  procedure SendNCCommand(Command : String;CommandType :TNCCommands;WaitForResponse: Boolean); overload;
    procedure SendNCCommand(Command : String;CommandType :TNCCommands;Emess : String;IsNewNCCommand : Boolean);
    procedure SendServiceCommand(Command : String);

    function ConnectToTelnet(IPAddress,Username : String;
                             PortNumber : Integer;
                             Password : String;
                             ReadPeriod : Integer;
                             ServicePeriod : Integer;
                             LoginDelay    : Integer;
                             EmulatorMode : Boolean) : Boolean;

    procedure GraceFullDisconnect;
//    procedure CheckIsTalking; overload;
    procedure CheckIsTalking(Mode : TTalkingModes); overload;
    procedure SetInLineError(ResponseNumber : String;ResponseMessage : String);

    procedure ClearDown;
    procedure ResetTimeoutToDefault;

    property NCResponseString : String read FNCResponseString;
    property ReturnCodesString : String read FReturnCode;
    property ReturnCodesAsInteger :Integer read GetIntegerReturnCode;
    property CanSendNCCommand : Boolean read NCCommandOK;
    property PendingServiceCommandCount : Integer read GetPendingServiceCommandCount;
    property VerboseMode : TVModes read FVerboseMode write FVerboseMode;
    property ExternalMemo : TTelnetDisplay read TelnetMemo write TelnetMemo;
    property IsConnected : Boolean read GetIsconnected;
    property CanTalk     : Boolean read GetIsconnected;
    property IsIdle : Boolean read FIsIdle write SetIsIdle;
    property IsOnLine : Boolean Read FIsOnline write FIsOnline;

    property CurrentJobName : String read FCurrentJobName;
    property Username : String read FUsername;
    property Password : String read FPassword;
    property TelnetOk : Boolean read GetTelnetOK;
    property EmulatorMode : Boolean read FEmulatorMode write FEmulatorMode;
    property NCFunctionPending : Boolean read GetNCFunctionPending;
    property JobScaling : Double read FJobScaling write FJobScaling;
    property JobXOffset : Double read FJobXOffset write FJobXOffset;
    property JobYOffset : Double read FJobYOffset write FJobYOffset;
    property JobNameToBeLoaded : String Read FJobLoadCommand write FJobLoadCommand;
    property ExternalLockBusy  : Boolean read FExternalLockBusy write SetExternalLockBusy;


    // Events
    property OnNCCommandComplete : TNCCompleteEvent read FOnNCCommandComplete write FOnNCCommandComplete;
    property ONNCCommandFail : TNCCommandFailEvent read  FONNCCommandFail write FONNCCommandFail;
    property OnServiceCommandComplete : TNCCompleteEvent read FOnNCCommandComplete write FOnNCCommandComplete;
    property OnIdleBusyChange : TIdleStatusChangeEvent read FOnIdleBusy write FonIdleBusy;
    property OnGFLock : TNotifyEvent read FonGFLock write FonGFLock;
    property OnFailedIsTalking : TNotifyEvent read FOnFailedIsTalking write FOnFailedIsTalking;
    property OnDefaultOnResetLoaded : TDefaultOnResetLoadedEvent read FOnDefaultOnResetLoaded write FOnDefaultOnResetLoaded;
    property OnJobNameChange : TJobnameChangeEvent read FOnJobNameChange write FOnJobNameChange;
    property OnLoginSuccess : TNotifyEvent read FOnLoginSuccess write FOnLoginSuccess;
    property OnLoginFail : TNotifyEvent read FOnLoginTimeout write FOnLoginTimeout;
    property OnJobListLoaded : TJobListLoadedEvent read FOnJobListLoaded write FOnJobListLoaded;
    property OnJobLoaded : TStatusChangeEvent read FOnJobLoaded write FOnJobLoaded;
    property OnCheckTalking : TCameraIsTalkingEvent read FOnIsTalkingResponse write FOnIsTalkingResponse;
    property OnNCAvailable : TNotifyEvent read FOnNCAvailable write FOnNCAvailable;
    property OnCommandException : TNotifyEvent read FonException write FonException;
    property OnTelnetBroken: TNotifyEvent read FonTelnetBroken write FonTelnetBroken;
    property OnTelnetConnectChange : TStatusChangeEvent read FOnConnectTelnetChange write FOnConnectTelnetChange;
    property OnHealthyStateChanged  : TStatusChangeEvent read FOnHealthyStateChanged write FOnHealthyStateChanged;
    property OnOnlineChanged : TOnlineSateChangedEvent read FOnLineChanged write FOnLineChanged;
    property OnOnlineForImport :TOnlineSateChangedEvent read FOnOnlineForImport write FOnOnlineForImport;
    property PostCommandsPending : Boolean read FPostCommandsPending write SetPostCommanddsPending; // externally set
    property OnLockout :TLockOutEvent read FonLockout write FonLockout;
    property OnGotValue  :TGetValueEvent read FOnGotValue write FOnGotValue;
    property OnFailedToGoOffLine : TFailedToGoOffLineEvent read FOnFailedToGoOffLine write FOnFailedToGoOffLine;
    property OnUserExposureChanged : TExposureChangedEvent read FOnUserExposureChanged write FOnUserExposureChanged;

    // NewMode After here
    property ResponseTimeoutInSeconds : Single read FResponseTimeoutInSeconds write SetResponseTimeoutInSeconds;
    property JobLoadTimeoutInSeconds  : Single read FJobloadTimeoutInSeconds write FJobloadTimeoutInSeconds;

    // Telnet Response Callbacks
    property OnCameraRestoreDone  : TXHairRestoreEvent  read FOnCameraRestoreDone write FOnCameraRestoreDone;
    property OnFitComplete        : TFitCompleteEvent   read FOnFitComplete       write FOnFitComplete;
    property OnRunJobComplete     : TRunJobDoneEvent    read FonRunJobDone        write FonRunJobDone;
    property OnFastJobRunDone     : TRunJobDoneEvent    read FonFastJobRunDone    write FonFastJobRunDone;

//    property OnLiveAfterJobRun    : TRunJobDoneEvent    read FonLiveAfterRunJob   write FonLiveAfterRunJob;
    property OnCheckJobPassDone   : TJobCheckEvent      read FonJobPassChecked    write FonJobPassChecked;
    property OnCameraRestoreOnBufferDone : TXHairRestoreEvent read FonCameraRestoreOnBufferDone write FonCameraRestoreOnBufferDone;
    property OnDefaultOnBufferLoaded : TDefaultOnResetLoadedEvent read FOnDefaultOnBufferLoaded write FOnDefaultOnBufferLoaded;

    property FastRunMode : Boolean read FFastRun write SetFastRunMode;
    property FastRunModeFirstSweep : Boolean read FFastRunFirstSweep;
    property BusyTimeout : Integer read FBusyTimeOut write SetBusyTimeout;
    property LastPassCount : Integer read FLastPassCount write FLastPassCount;
    property InsipientExposure : Double  read FInsipientExposure write FInsipientExposure;
end;


implementation
uses CognexActions;

{ TCognexComunicator }
function TCognexComunicator.ConnectToTelnet(IPAddress, Username: String;
                                            PortNumber: Integer;
                                            Password : String;
                                            ReadPeriod : Integer;
                                            ServicePeriod : Integer;
                                            LoginDelay    : Integer;
                                            EmulatorMode : Boolean) : Boolean;
begin
  Result := True;
  try
  FUsername := Username;
  FPassword := Password;
  TelnetC.IPAddress := IPAddress;
  TelNetC.PortNumber := PortNumber;
  TelnetC.Username := Username;
  TelnetC.ReadTimerPeriod := ReadPeriod;
  TelnetLoginTimer.Interval := LoginDelay;
  ServiceTimer.Interval := ServicePeriod;
  CurrentServiceInterval := ServicePeriod;
  DefaultServiceInterval := ServicePeriod;
  LastServiceInterval := ServicePeriod;
  AddDebug('Connecting to Camera Telnet',tvmDebug);
  try
  if not TelnetC.ConnectToCamera(True) then
    begin
    if assigned(TelnetMemo) then TelnetMemo.Color := clAqua;
    Result := False;
    end
  except
  if assigned(TelnetMemo) then TelnetMemo.Color := clRed;
  Result := False;
  end;
  except
  if EmulatorMode then
  end

end;

constructor TCognexComunicator.Create;
begin
  inherited;
  GFLockCount := 0;
  LockedMax := 0;
  ReportedMax := 0;
  LockedCount := 0;
  PostCommandError := False;
  TimeoutEnabled := True;
  FPostCommandsPending := False;
  IsIdle := True;
  FIsOnLine := False;
  FExternalLockBusy := False;
  LastOnlineState := False;
  FollowOnMode := False;
  FEmulatorMode := False;
  TelnetBroken := False;
  CurrentNCCommand := TCocgnexInputMessage.Create;
  CameraTelnetConnected := False;
  JobList := TStringList.Create;


  TelnetMemo := nil;
  ServiceTimer := TTimer.Create(nil);
  with ServiceTimer do
    begin
    Enabled := False;
    Interval := 5;
    OnTimer := ServiceCommands;
    end;

  LockTimout  := TTimer.Create(nil);
  With LockTimout do
    begin
    Enabled := False;
    Interval := 5000;
    OnTimer := HandleBusyLocked;
    end;

  ResponseTimeout := TTimer.Create(nil);
  with ResponseTimeout do
    begin
    Enabled := False;
    Interval := 20000;
    OnTimer := ResponseTimedOut;
    end;

  TelnetLoginTimer := TTimer.Create(nil);
  With TelnetLoginTimer do
    begin
    Enabled := False;
    Interval := 3000;
    OnTimer := DoDelayedLogin;
    end;

  LoginTimeOut := TTimer.Create(nil);
  With LoginTimeOut do
    begin
    Enabled := False;
    Interval := 5000;
    OnTimer := DoLoginTimedout;
    end;


  ServiceMode := smIdle;
  CommandList := TCognexCommandList.Create;
  NCCommandInProgress := False;


  TelnetC := TCognexTelnetClient.Create(nil);
  With TelnetC do
      begin
      OnDataInBuffer := GetTelnetResponse;
      aOnConnected := TelnetConnected;
      OnReConnected := TelnetLoginSpeakingButNoWelcome;
      OnDisconnected := TelnetDisconnected;
      OnTelnetException := HandleTelnetException;
      OnHealthyChange := TelnetHealthyChanged;
      end;
end;


destructor TCognexComunicator.Destroy;
begin
  if assigned(TelnetC) then TelnetC.PrepareToClose;
  ServiceTimer.Enabled := False;
  ResponseTimeout.Enabled := False;
  TelnetLoginTimer.Enabled := False;
  ServiceTimer.Free;
  ResponseTimeout.Free;
  TelnetLoginTimer.Free;
  CommandList.Clean;
  CommandList.Free;
  if assigned(TelnetC) then
    begin
    TelnetC.Free;
    TelnetC := nil;
    end;
  inherited;
end;

function TCognexComunicator.GetIntegerReturnCode: Integer;
begin

end;

function TCognexComunicator.GetNCCommandDone: Boolean;
begin

end;

function TCognexComunicator.GetPendingServiceCommandCount: Integer;
begin

end;

function TCognexComunicator.NCCommandOK: Boolean;
begin
Result := True;
end;



procedure TCognexComunicator.AddFollowCommand(CTYpe :TNCCommands;Index : Integer;
                                              CommandStr : String;
                                              ErrorMessage : String;
                                              ServiceInterval : Integer);
var
aaNewCommand  : TCocgnexInputMessage;
begin
  aaNewCommand := TCocgnexInputMessage.Create(ServiceInterval);
  aaNewCommand.CommandType := CType;
  aaNewCommand.IsNCCommand := True;
  aaNewCommand.CommandStr := CommandStr;
  aaNewCommand.CommandsFollowing := Index;
  aaNewCommand.ErrorString := 'Failed to '+ErrorMessage;
  CommandList.AddCommand(aaNewCommand);
end;



procedure TCognexComunicator.SendNCCommand(Command : String;CommandType :TNCCommands;EMess : String;IsNewNCCommand : Boolean);
var
UCCommand : String;
NewCommand  : TCocgnexInputMessage;
NextCommand : TCocgnexInputMessage;
IsBufferRestore : Boolean;
begin
  CurrentServiceInterval := DefaultServiceInterval;
  ServiceTimer.Enabled := False;
  try
  if PostCommandsPending and IsNewNCCommand then
    begin
    PostCommandError := True;
    end;

  if Command = 'GF' then
    begin
    addDebug('Got GF ******',tvmDebug);
    AddDebug(Format('CC = %d %d',[CommandList.count,Cardinal(ServiceMode)]),tvmDebug);
    end;
  NewCommand := TCocgnexInputMessage.Create;
  NewCommand.CommandType := CommandType;
  NewCommand.IsNCCommand := True;
  NewCommand.CommandStr := Command;
  NewCommand.ErrorString := 'Failed to '+EMess;
  CommandList.Add(NewCommand);

  case NewCommand.CommandType of
    tncTestTalkingLogin,tncTestTalkingReconect,tncTestTalkingPowerOnReset,tncTestTalkingReset,tncTestTalkingRestore:
    begin
    {$IFDEF HIGHDEBUG}
    addDebug('Got GF *******',tvmDebug);
    AddDebug(Format('CC = %d %d',[CommandList.count,Cardinal(ServiceMode)]),tvmDebug);
    {$ENDIF}

    AddDebug(Format('Making  GO Commans with SI of %d',[CurrentServiceInterval]),tvmDebug);
    NextCommand := TCocgnexInputMessage.Create(CurrentServiceInterval);
    NextCommand.CommandType := NewCommand.CommandType;
    NextCommand.CommandsFollowing := 1;
    NextCommand.CommandStr := 'GO' ;
    NextCommand.IsNCCommand := True;
    NewCommand.CommandType := tncTestGetFilename;
    CommandList.AddCommand(NextCommand);
    end;

   tncOnLineForImport:
      begin
      NewCommand.CommandsFollowing := 5;
      NewCommand.CommandStr := Command;
      NewCommand.CommandType := tncFollowOn;
      AddFollowCommand(tncFollowOn,4,'SO2','go Online 1 second time for Run Job',80);
      AddFollowCommand(tncOnLineForImport,3,'SO1','go Online 1 second time for Run Job',80);
      AddFollowCommand(tncFollowOn,2,'SO2','go Online 1 second time for Run Job',80);
      AddFollowCommand(tncOnLineForImport,1,'SO1','go Online 1 second time for Run Job',CurrentServiceInterval);
      end;

   tncOffLineForJobLoad:
     begin
     if TimeoutEnabled then ResponseTimeout.Interval := Round(FJobloadTimeoutInSeconds*1000);
     AddDebug('Extending Timeout For JobLoad',tvmDebug);
     InsipientJob := FJobLoadCommand;
     NewCommand.CommandsFollowing := 6;
     NewCommand.CommandType := tncFollowOn;
     NewCommand.CommandStr := Command;
     AddFollowCommand(tncFollowOn,5,Format('LF%s',[JobNameToBeLoaded]),'Load Job After setting offline',CurrentServiceInterval);
     AddFollowCommand(tncFollowOn,4,Format('SFCalData.Scaling %7.5f',[JobScaling]),'set Scale For Job Load',CurrentServiceInterval);
     AddFollowCommand(tncFollowOn,3,Format('SFCalData.XOffset %7.5f',[JobXOffset]),'set XOffset For Job Load',CurrentServiceInterval);
     AddFollowCommand(tncFollowOn,2,Format('SFCalData.YOffset %7.5f',[JobYOffset]),'set YOffset For Job Load',CurrentServiceInterval);
     AddFollowCommand(tncLiveAfterLoad,1,'Put Live 1','go live For Job Load',CurrentServiceInterval);
    end;


   tncJobLoadStart:
    begin
    if TimeoutEnabled then ResponseTimeout.Interval := Round(FJobloadTimeoutInSeconds*1000);
    AddDebug('Extending Timeout For JobLoad',tvmDebug);
    InsipientJob := FJobLoadCommand;
    NewCommand.CommandsFollowing := 5;
    NewCommand.CommandType := tncFollowOn;
    NewCommand.CommandStr := Command;
    AddFollowCommand(tncFollowOn,4,Format('SFCalData.Scaling %7.5f',[JobScaling]),'set Scale For Job Load',CurrentServiceInterval);
    AddFollowCommand(tncFollowOn,3,Format('SFCalData.XOffset %7.5f',[JobXOffset]),'set XOffset For Job Load',CurrentServiceInterval);
    AddFollowCommand(tncFollowOn,2,Format('SFCalData.YOffset %7.5f',[JobYOffset]),'set YOffset For Job Load',CurrentServiceInterval);
    AddFollowCommand(tncLiveAfterLoad,1,'Put Live 1','go live For Job Load',CurrentServiceInterval);
    end;

   tncResetExerciseS02:
      begin
      NewCommand.CommandsFollowing := 3;
      NewCommand.CommandType := tncFollowOn;
      NewCommand.CommandStr := Command;
      AddFollowCommand(tncFollowOn,2,'SO1','Exercise Initial On Line conditioning',CurrentServiceInterval);
      AddFollowCommand(tncDoneInitialOnLineConditioning,2,'SO0','Exercise Initial On Line conditioning',CurrentServiceInterval);
      end;

   tncStartRestoreCameraForBuffer,tncStartGeneralRestore:
    begin
    IsBufferRestore := False;
    if NewCommand.CommandType = tncStartRestoreCameraForBuffer then
      begin
      IsBufferRestore := True;
      end ;
    NewCommand.CommandsFollowing := 8;
    NewCommand.CommandType := tncFollowOn;
    AddFollowCommand(tncFollowOn,7,Format('SFCalData.XOffset %7.3f',[BufferCameraState.XOffset]),'set XOffset in Buffer Mode',CurrentServiceInterval);
    AddFollowCommand(tncFollowOn,6,Format('SFCalData.YOffset %7.3f',[BufferCameraState.YOffset]),'set YOffsetin Buffer Mode',CurrentServiceInterval);
    AddFollowCommand(tncFollowOn,5,Format('SIVerticalLine.COLOR %d',[BufferCameraState.XHairColorNumber]),'set Line Colour in Buffer Mode',CurrentServiceInterval);
    AddFollowCommand(tncFollowOn,4,Format('SIHorizontalLine.COLOR %d',[BufferCameraState.XHairColorNumber]),'set Line Colour in Buffer Mode',CurrentServiceInterval);
    AddFollowCommand(tncFollowOn,3,Format('SICIRCLE_1.COLOR %d',[BufferCameraState.CircleColorNumber]),'set Circle Colour in Buffer Mode',CurrentServiceInterval);
    AddFollowCommand(tncFollowOn,2,Format('SFCircleRadius.Value %7.3f',[CurrentCameraState.CircleRadius]),'set Circle Radius in Buffer Mode',CurrentServiceInterval);

    if IsBufferRestore then
      begin
      AddFollowCommand(tncRestoreCameraForBufferEnd,1,'Put live 1','go live In Buffer Mode',CurrentServiceInterval);
      end
    else
      begin
      AddFollowCommand(tncFollowOn,2,Format('SFAcquisition.Exposure_Time %5.3f',[BufferCameraState.Exposure]),'set Exposure In Restore',CurrentServiceInterval);
      AddFollowCommand(tncRestoreCamera,1,'Put live 1','go Live In Restore',CurrentServiceInterval);
      if assigned(FOnUserExposureChanged) then FOnUserExposureChanged(Self,True,BufferCameraState.Exposure);
      end
    end;

{  tncOffLineAfterJobRun:
    begin
    NewCommand.CommandsFollowing := 2;
    NewCommand.CommandStr := Command;
    NewCommand.CommandType := tncOffLineAfterJobRun;
    AddFollowCommand(tncLiveAfterJobRun,1,'Put Live 1','go Live after Run Job',CurrentServiceInterval);
    end;}

  tncOffLineAfterJobRun:
    begin
    NewCommand.CommandsFollowing := 3;
    NewCommand.CommandStr := Command;
    NewCommand.CommandType := tncOffLineAfterJobRun;
    AddFollowCommand(tncFollowOn,2,'Put Live 1','go Live after Run Job',CurrentServiceInterval);
    AddFollowCommand(tncCheckJobPass,1,'GVJob.Pass_Count','getting Job Pass Count',80);
    end;

  tncSetJobXOffset:
    begin
    NewCommand.CommandsFollowing := 2;
    NewCommand.CommandStr := Command;

    NewCommand := TCocgnexInputMessage.Create;
    NewCommand.CommandType := tncSetJobYOffset;
    NewCommand.CommandsFollowing := 1;
    NewCommand.CommandStr := Format('SFCalData.YOffset %5.3f',[JobYOffset]);
    NewCommand.IsNCCommand := True;
    CommandList.Add(NewCommand);
    end;

  tncResetandRunJob:   // Instigated by SO1 command
    begin
    NewCommand.CommandsFollowing := 6;
    NewCommand.CommandStr := Command;
    NewCommand.CommandType := tncRunJobOnLine1;
    FLastPassCount := 0;
    AddFollowCommand(tncRunJobOnLine1,5,'SO2','go Online 1 second time for Run Job',200);
    AddFollowCommand(tncResetandRunJob,4,'SIJob.External_Reset_Counters 1','set counters reset high for Run Job',80);
    AddFollowCommand(tncResetJobDownBeforeRun,3,'SIJob.External_Reset_Counters 0','set reset counters low for Run Job',80);
    AddFollowCommand(tncRunJobGetStartCount,2,'GVJob.Pass_Count','Getting Start Pass Count',250);
    AddFollowCommand(tncRunJob,1,'SW8','run job (SW8) for Run Job',250);
    end;


  tncResetandRunJobFastFirst:
    begin
    NewCommand.CommandsFollowing := 6;
    NewCommand.CommandStr := Command;
    NewCommand.CommandType := tncRunJobOnLine1;
    AddFollowCommand(tncRunJobOnLine1,5,'SO2','go Online 1 second time for Run Job',200);
    AddFollowCommand(tncResetandRunJob,4,'SIJob.External_Reset_Counters 1','set counters reset high for Run Job',80);
    AddFollowCommand(tncResetJobDownBeforeRun,3,'SIJob.External_Reset_Counters 0','set reset counters low for Run Job',80);
    AddFollowCommand(tncRunJobGetStartCount,2,'GVJob.Pass_Count','Getting Start Pass Count',250);
    AddFollowCommand(tncRunJobFastFirst,1,'SW8','run job (SW8) for Run Job',250);
    end;

   tncFromForm:
    begin
    UCCommand := UpperCase(Command);
    if Length(UCCommand) > 2 then
    if (UCCommand[1] = 'S') and (UCCommand[2] = 'O') then
      begin
      if UCCommand[3] = '0' then
        begin
        NewCommand.CommandType := tncSettingOffLine;
        end
      else
        begin
        NewCommand.CommandType := tncSettingOnLine;
        end;
      end
    end;
   end; // case

  if NCCommandInProgress then
    begin
    AddDebug('Overrrun ???',tvmDebug);
    end;
  finally
  ServiceTimer.Enabled := True;
  end;
end;


procedure TCognexComunicator.SendServiceCommand(Command : String);
var
NewCommand  : TCocgnexInputMessage;
begin
 NewCommand := TCocgnexInputMessage.Create;
 NewCommand.CommandStr := Command;
 NewCommand.IsNCCommand := False;
 NewCommand.CommandType := tncService;
 CommandList.Add(NewCommand);
 ServiceTimer.Enabled := True;

end;




procedure TCognexComunicator.ServiceCommands(Sender: Tobject);
var
Command : TCocgnexInputMessage;
GotCommand : Boolean;
begin
case ServiceMode of
  smIdle:
    begin
    GotCommand := False;
    if CommandList.Count > 0 then
      begin
      Command := CommandList[0];
      ResponseCount := 0;
      GotCommand := True;
      CurrentNCCommand.AssignFrom(Command);
      LastCommandStr := CurrentNCCommand.CommandStr;
//      if Command.ServiceInterval <> LastServiceInterval then
      if Command.ServiceInterval <> ServiceTimer.Interval then
        begin
        LastServiceInterval := Command.ServiceInterval;
       {$IFDEF HIGHDEBUG}
        AddDebug(Format('Changing Service Interval From %d  to %d',[ServiceTimer.Interval,Command.ServiceInterval]),tvmDebug);
       {$ENDIF}
        ServiceTimer.Enabled := False;
        ServiceTimer.Interval := Command.ServiceInterval;
        CurrentServiceInterval := Command.ServiceInterval;
        ServiceTimer.Enabled := True;
        end;

      if FIsIdle then
        begin
        IsIdle := False;
        If Assigned(FOnIdleBusy) then
          begin
          SetInLineError('','');
          FOnIdleBusy(Self,FIsIdle,FInLineErrorResponse,FInLineErrorMessage);
          AddStatusMessage(Format('Comms Busy %d',[CurrentServiceInterval]),tvmDuplex);
          end
        end;

      FNCResponseString := '';
      if TimeoutEnabled then ResponseTimeout.Enabled := True;
      CurrentNCCommand.IsNCCommand := Command.IsNCCommand;
      NCCommandInProgress          := Command.IsNCCommand;
      ServiceMode := smSending;
      end;

    If Not GotCommand then
      begin
      // Nothing In Command list
      If not FIsIdle then
        begin
        if (not ExternalLockBusy) then // this is designed to keep comms showing busy for sequenced comands
          begin
          IsIdle := True;
          LockedCount := 0;
          AddDebug('Locked Count Reset',tvmDebug);
          If Assigned(FOnIdleBusy) then
            begin
            FOnIdleBusy(Self,FIsIdle,FInLineErrorResponse,FInLineErrorMessage);
            AddStatusMessage('Comms Idle In smIdle(No external Lock)',tvmDuplex);
 //           ServiceTimer.Enabled := False;
            end
          end
        else
          begin
          {$IFDEF HIGHDEBUG}
          if LastCommandStr = 'GF' then
            begin
            inc(GFLockCount);
            if GFLockCount > 10 then
                begin
                if assigned(FOnGFLock) then  FOnGFLock(Self);
                end;
            end;
          AddDebug(Format('Locked Busy: %s',[LastCommandStr]),tvmDebug);
          inc(LockedCount);
          if (LockedCount > LockedMax)  then
            begin
            LockedMax :=LockedCount;
            if (LockedCount < 11) then
                begin
                AddDebug(Format('Locked Max %d with Last Command %s',[LockedMax,LastCommandStr]),tvmDebug);
                ReportedMax := LockedMax;
                end
            else
               begin
               if (LockedMax - ReportedMax) > 100 then
                 begin
                 AddDebug(Format('Locked Max %d with Last Command %s',[LockedMax,LastCommandStr]),tvmDebug);
                 ReportedMax := LockedMax;
                 end
               end
     //       EventLog(Format('Locked Max %d with Last Command %s',[LockedMax,LastCommandStr]));
            end;
         {$ENDIF}
          end
        end;
      end;
    end;

  smSending:
    begin
     try
     AddOutMess(CurrentNCCommand.CommandStr,tvmDuplex);
     TelnetC.SendCommand(CurrentNCCommand.CommandStr,False,CurrentNCCommand.HasResponse);
     ServiceMode := smWaitingResponse;
     except
     LastCommandStr := CurrentNCCommand.CommandStr;
     ServiceMode := smException;
     end;
    end;

  smGetNext:
    begin
    if CommandList.Count > 0 then
      begin
      ResponseCount := 0;
      Command := CommandList[0];
{      if Command.IsStartCommand then
        begin
        if CameraActionsBusy then
          begin
          AddDebug('waiting for cameraActionHandler  in smGet Next',tvmDebug);
          Exit;
          end;
        end;}
//      if Command.ServiceInterval <> CurrentServiceInterval then
      if Command.ServiceInterval <> ServiceTimer.Interval then
        begin
        {$IFDEF HIGHDEBUG}
        AddDebug(Format('Changing Service Interval from %d to %d',[ServiceTimer.Interval,Command.ServiceInterval]),tvmDebug);
       {$ENDIF}
        ServiceTimer.Enabled := False;
        ServiceTimer.Interval := Command.ServiceInterval;
        CurrentServiceInterval := Command.ServiceInterval;
        ServiceTimer.Enabled := True;
        end;

      NCCommandInProgress := Command.IsNCCommand;
      CurrentNCCommand.IsNCCommand := Command.IsNCCommand;
      CurrentNCCommand.CommandStr := Command.CommandStr;
      {$IFDEF HIGHDEBUG}
      AddDebug(Format('Getting Next command %s',[Command.CommandStr]),tvmDebug);
      {$ENDIF}
      CurrentNCCommand.HasResponse := Command.HasResponse;
      CurrentNCCommand.CommandType := Command.CommandType;
      CurrentNCCommand.ErrorString := Command.ErrorString;
      if TimeoutEnabled then ResponseTimeout.Enabled := True;
      ServiceMode := smSending;
      end
    else
      begin
      ServiceMode := smIdle;
      {$IFDEF HIGHDEBUG}
      AddDebug('Going Idle From Get Next',tvMDebug);
      {$ENDIF}
      end;
    end;

  smException:
    begin
    if assigned(TelnetMemo) then TelnetMemo.Color := clGreen;
    CommandList.Clean;
    FExternalLockBusy := False;
    FollowOnMode := False;
    ServiceMode := smIdle;
    if assigned(FonException) then FOnException(Self);
    end;

  end; // case
end;

procedure TCognexComunicator.TerminateServiceCommand(Response : String);
var
DelCommand : TCocgnexInputMessage;
begin
  {$IFDEF HIGHDEBUG}
  AddDebug(Format('Service Terminating %s',[CurrentNCCommand.CommandStr]),tvmDebug);
  {$ENDIF}
  ResponseTimeout.Enabled := False;
  NCCommandInProgress := False;
  {$IFDEF HIGHDEBUG}
  AddDebug('Removing Service Command from Command List',tvmDebug);
  {$ENDIF}
  CommandList.RemoveCommand;
  if CommandList.Count > 0 then
    begin
    ServiceMode :=smGetNext;
    end
  else
    begin
    ServiceMode :=smIdle;
    {$IFDEF HIGHDEBUG}
    AddDebug('Going Idle From Service Command',tvMDebug);
    {$ENDIF}
//    if assigned(FonIdleBusy) then
//        begin
//        FonIdleBusy(Self,False);
//        end;
    end;
  if not CommandList.NCFunctionPending then
    begin
    if assigned(FOnNCAvailable) then FOnNCAvailable(Self);
    end;
end;

procedure TCognexComunicator.RunFollowOnCommands(Success : Boolean;ResponseState : Integer;Response : String;ErrMessage :String);
var
DelCommand : TCocgnexInputMessage;
NextCommand : TCocgnexInputMessage;
begin
  ResponseTimeout.Enabled := False;
  If not Success then
    begin
    SetInLineError(Response,ErrMessage);
    AddDebug(Format('ILE %s %s',[FInLineErrorResponse,FInLineErrorMessage]),tvmDebug);
    end;
  if CommandList.Count > 0 then
    begin
    DelCommand := CommandList[0];
    DelCommand.CommandStr := '';
    DelCommand.Free;
    DelCommand := nil;
    CommandList.Delete(0);
    end;

    if CommandList.Count > 0 then
      begin
      NextCommand := CommandList[0];
      if NextCommand.CommandsFollowing > 0 then
        begin
        FollowOnMode := True;
        ServiceMode :=smGetNext;
        end
      else
        begin
        // Check Here for errors in any of the follow-on commands
{        If InLineErrorResponse <> '' then
          begin
          TerminateCurrentNCCommand(False,-3,InLineErrorResponse,InLineErrorMessage)
          end
        else}
          begin
          TerminateCurrentNCCommand(True,1,Response,'','');
          end;
        end;
      end
  else
    begin
    FollowOnMode := False;
    end;
end;

procedure TCognexComunicator.TerminateCurrentNCCommand(Success : Boolean;ResponseState : Integer;InternalResponse : String;ExternalResponse : String;ErrMessage :String);
begin
    TerminateCurrentNCCommand(Success,ResponseState,InternalResponse,ExternalResponse,ErrMessage,False);
end;

procedure TCognexComunicator.TerminateCurrentNCCommand(Success : Boolean;ResponseState : Integer;InternalResponse : String;ExternalResponse : String;ErrMessage :String;CommandsToFollow : Boolean);
var
DelCommand : TCocgnexInputMessage;
CType : TNCCommands;
begin

  FollowOnMode := False;
  ResponseTimeout.Enabled := False;
  CType := CurrentNCCommand.CommandType;
  if Success then
    begin
    {$IFDEF HIGHDEBUG}
    AddDebug(Format('NC Success Terminating %s,%s,%s',[CurrentNCCommand.CommandStr,InternalResponse,ErrMessage]),tvmDebug);
    {$ENDIF}
    end
  else
    begin
    {$IFDEF HIGHDEBUG}
    AddDebug(Format('NC Fail Terminating %s,%s,%s',[CurrentNCCommand.CommandStr,InternalResponse,ErrMessage]),tvmDebug);
    {$ENDIF}
    end;
  if Not CommandsToFollow then
    begin
    if assigned(FOnNCCommandComplete) then FOnNCCommandComplete(Success,CurrentNCCommand,CurrentNCCommand.CommandType,InternalResponse,ExternalResponse,ResponseState,ErrMessage);
    NCCommandInProgress := False;
    end;
  CommandList.RemoveCommand;

  if CommandList.Count > 0 then
    begin
    ServiceMode :=smGetNext;
    end
  else
    begin
    ServiceMode :=smIdle;
    if (Not CommandsToFollow) and (Not FExternalLockBusy) then
        begin
        AddDebug('Going Idle From NC Command',tvMDebug);
        if assigned(FonIdleBusy) then FonIdleBusy(Self,True,FInLineErrorResponse,FInLineErrorMessage);
        if not CommandList.NCFunctionPending then
            begin
            if assigned(FOnNCAvailable) then FOnNCAvailable(Self);   //!!!!!!!!!!!
            end;
        end
    else
        begin
        case CType of
          tncOnLineForImport:
            begin
            if assigned(FOnOnlineForImport) then FOnOnlineForImport(Self,Success);
            end;
        end; //case
        end;
    end;
end;

procedure TCognexComunicator.GetTelnetResponse(Sender: Tobject;Response : String);
var
FNum : Integer;
ResultNum : Single;
ResultInt : Integer;
begin
if (ResponseCount = 0) then
  if Response[1] = '-' then
     AddErrorMessage(TelnetC.InBuffer,tvmDuplex)
  else
     AddSuccessMessage(TelnetC.InBuffer,tvmDuplex)
else
  begin
  if Response = '#ERR' then
    begin
    AddErrorMessage(TelnetC.InBuffer,tvmDuplex)
    end
  else
    begin
    AddReplyMessage(TelnetC.InBuffer,tvmDuplex);
    end;
  end;

 if not CurrentNCCommand.IsNCCommand then
  begin
  TerminateServiceCommand(Response);
  end
else
  begin
  //**************************************
  case CurrentNCCommand.CommandType of
  //**************************************
   tncOnLineForImport:
    begin
    if Response  = '1' then
        begin
        TerminateCurrentNCCommand(True,1,'1','','',True); // uses overload version
        end
    else
        begin
        TerminateCurrentNCCommand(False,1,'1','','',True);// uses overload version
        end
    end;

   tncOffLineForImport:
    begin
    if Response  = '1' then
        begin
        TerminateCurrentNCCommand(True,1,'1','','');
        end
    else
        begin
        TerminateCurrentNCCommand(False,1,'1','','');
        end
    end;

   tncLoadDefaultOnXHairBuffer:
     begin
     if Response  = '1' then
        begin
        TerminateCurrentNCCommand(True,1,'1','','');
        AddDebug('Default Job Loaded',tvmDebug);
        if assigned(FOnDefaultOnBufferLoaded) then
          begin
          FOnDefaultOnBufferLoaded(Self,True);
          end;
        end
      else
        begin
        TerminateCurrentNCCommand(False,-6,'','','Could not load default job on Reset');
        if assigned( FOnDefaultOnBufferLoaded) then
          begin
          FOnDefaultOnBufferLoaded(Self,False);
          end;
        end;
     end;

   tncStartRestoreCameraForBuffer,tncStartGeneralRestore:
     if Response  = '1' then
        begin
        TerminateCurrentNCCommand(True,1,'1','','');
        AddDebug('Starting Buffer Croshair Restore',tvmDebug);
        end
      else
        begin
        TerminateCurrentNCCommand(False,1,'1','','');
        AddErrorMessage('Starting Buffer Croshair Restore Fail',tvmDebug);
        end;

   tncNone,tncService:
    begin
    ResponseTimeout.Enabled := False;
    TerminateCurrentNCCommand(True,1,'','','');
    end;

   tncFromForm:
   begin
    if ResponseCount = 0 then
      begin
      ResponseTimeout.Enabled := False;
      TerminateCurrentNCCommand(True,1,'','','');
      end
    else
      begin
      AddDebug('Ignoring follow on responses from manually entered command',tvmDebug);
      end;
      inc(ResponseCount);
    end;

   tncSettingOffLine:
     begin
     if Response  = '1' then
       begin
       AddDebug('Discovered OffLine',tvmDebug);
       FIsOnlIne := False;
       If assigned(FOnLineChanged) then FOnLineChanged(Self,FIsOnlIne);
       end;
     ResponseTimeout.Enabled := False;
     TerminateCurrentNCCommand(True,1,'','','');
     end;

   tncSettingOnLine:
     begin
     if Response  = '1' then
       begin
       AddDebug('Discovered OnLine',tvmDebug);
       FIsOnlIne := True;
       If assigned(FOnLineChanged) then FOnLineChanged(Self,FIsOnlIne);
       end ;
     ResponseTimeout.Enabled := False;
     TerminateCurrentNCCommand(True,1,'','','');
     end;

    tncOffLineAfterJobRun:
      begin
      ResponseTimeout.Enabled := False;
      if Response  = '1' then
        begin
        FIsOnline := False;
        if assigned(FOnLineChanged) then FOnLineChanged(Self,False);
        RunFollowOnCommands(True,1,Response,'');
        end
      else
        begin
        if assigned(FOnFailedToGoOffLine) then FOnFailedToGoOffLine(Self);
        RunFollowOnCommands(False,1,Response,'Go Offline after Job Run');
        end;
      end;


    tncRestoreCamera:
     begin
     {$IFDEF HIGHDEBUG}
      AddDebug('Finished Camera restore commands',tvMDebug);
     {$ENDIF}
      if Response  = '1' then
        begin
        TerminateCurrentNCCommand(True,1,Response,'','');
        if assigned(FOnCameraRestoreDone) then FOnCameraRestoreDone(Self,True);
        end
      else
        begin
        TerminateCurrentNCCommand(False,-2,'','','Failed On Camera Restore');
        if assigned(FOnCameraRestoreDone) then FOnCameraRestoreDone(Self,False);
        end ;
     end;

    tncRestoreCameraForBufferEnd:
     begin
     {$IFDEF HIGHDEBUG}
      AddDebug('Finished Camre restore for Buffer',tvMDebug);
     {$ENDIF}
      if Response  = '1' then
        begin
        TerminateCurrentNCCommand(True,1,Response,'','');
        if assigned(FonCameraRestoreOnBufferDone) then FonCameraRestoreOnBufferDone(Self,True);
        end
      else
        begin
        TerminateCurrentNCCommand(False,-2,'','','Failed On Camera Restore in Buffer');
        if assigned(FonCameraRestoreOnBufferDone) then FonCameraRestoreOnBufferDone(Self,False);
        end ;
     end;

   tncLastFitCommand:
    begin
    ResponseTimeout.Enabled := False;
    {$IFDEF HIGHDEBUG}
    AddDebug('LastFit command Done',tvmDebug);
    {$ENDIF}
    if Response  = '1' then
      begin
      If assigned(FOnFitComplete) then FOnFitComplete(Self,True);
      end
    else
      begin
      If assigned(FOnFitComplete) then FOnFitComplete(Self,False);
      end;
    TerminateCurrentNCCommand(True,1,'','','');
    end;



   tncTestTalkingReset,tncTestTalkingLogin,tncTestTalkingReconect,tncTestTalkingPowerOnReset,tncTestTalkingRestore:
    begin
      if Response = '1' then
        begin
        FIsOnline := True;
        If assigned(FOnLineChanged) then FOnLineChanged(Self,True);
        end
      else if Response = '0' then
        begin
        FIsOnline := False;
        If assigned(FOnLineChanged) then FOnLineChanged(Self,False);
        end;

    case CurrentNCCommand.CommandType of
        tncTestTalkingRestore       : FOnIsTalkingResponse(Self,True,FCurrentJobName,tmRestore,FIsonline);
        tncTestTalkingReset         : FOnIsTalkingResponse(Self,True,FCurrentJobName,tmGeneralReset,FIsonline);
        tncTestTalkingLogin         : FOnIsTalkingResponse(Self,True,FCurrentJobName,tmAtLogin,FIsonline);
        tncTestTalkingReconect      : FOnIsTalkingResponse(Self,True,FCurrentJobName,tmAtReconnect,FIsonline);
        tncTestTalkingPowerOnReset  : FOnIsTalkingResponse(Self,True,FCurrentJobName,tmPowerOnReset,FIsonline);
        end  ;
    TerminateCurrentNCCommand(True,1,'1','','');
    end;

   tncTestGetFilename:
    begin
    AddDebug(Format('Got response for GF at Reset %d %S',[ResponseCount,Response]),tvmDebug);
    case ResponseCount of
      0:
      begin
      if Response  = '1' then
        begin
        inc(ResponseCount);
        end
      else if Response  = '-6' then // No job loaded
        begin
        ResponseTimeout.Enabled := False;
        FCurrentJobName := '';
        if assigned(FOnJobNameChange) then FOnJobNameChange(Self,FCurrentJobName);
        RunFollowOnCommands(False,1,Response,'No Job Name');
        end
      else
        begin
        ResponseTimeout.Enabled := False;
        FCurrentJobName := '';
        if assigned(FOnJobNameChange) then FOnJobNameChange(Self,FCurrentJobName);
        RunFollowOnCommands(False,1,Response,'No Job Name');
        end;
      end;

      1:
      begin
      ResponseTimeout.Enabled := False;
      FCurrentJobName := Response;
      if assigned(FOnJobNameChange) then FOnJobNameChange(Self,FCurrentJobName);
      RunFollowOnCommands(True,1,Response,'');
      end;
    end;  // inner case

    end;



   tncLoadDefaultOnReset:
     begin
     if Response  = '1' then
        begin
        TerminateCurrentNCCommand(True,1,'1','','');
        AddDebug('Default Job Loaded',tvmDebug);
        if assigned( FOnDefaultOnResetLoaded) then
          begin
          FOnDefaultOnResetLoaded(Self,True);
          end;
        end
      else
        begin
        TerminateCurrentNCCommand(False,-6,'','','Could not load default job on Reset');
        if assigned( FOnDefaultOnResetLoaded) then
          begin
          FOnDefaultOnResetLoaded(Self,False);
          end;
        end;
     end;

   tncGoLiveOnReset:
     begin
     if Response  = '1' then
        begin
        TerminateCurrentNCCommand(True,1,'1','','');
        end
      else
        begin
        TerminateCurrentNCCommand(False,-2,'','','Failed to go live on Reset')
      end;
     end;

   tncLoginResponse:
    begin
    AddDebug('Got response from Login' + Response,tvmDebug);
    ResponseTimeout.Enabled := False;
    LoginTimeout.Enabled := False;

    if Pos('Logged In',Response) > 0 then
      begin
      TelnetC.LoginSuccesful;
      ExternalMemo.AddReplyMessage(Response);
      TerminateCurrentNCCommand(True,1,Response,'','');
      if assigned(FOnLoginSuccess) Then FOnLoginSuccess(Self);
      end
    else
      begin
      TerminateCurrentNCCommand(False,-6,Response,'','Failed Log in');
      end;
    end;

    tncLiveAfterLoad:
      begin
      ResponseTimeout.Enabled := False;
{      if FInLineErrorResponse <> '' then
        begin
        FollowOnMode := False;
        TerminateCurrentNCCommand(False,-2,Response,FInLineErrorMessage);
        SetInLineError('','');
        if assigned(FonJobLoaded) then FOnJobLoaded(Self,False)
        end
      else
        begin  }
     if Response  = '1' then
          begin
          TerminateCurrentNCCommand(True,1,Response,'','');
          if assigned(FonJobLoaded) then FOnJobLoaded(Self,True);
          if assigned(FOnJobNameChange) then FOnJobNameChange(Self,InsipientJob);
          end
      else
          begin
          FollowOnMode := False;
          TerminateCurrentNCCommand(False,-2,Response,'','Failed Load Job');
          if assigned(FonJobLoaded) then FOnJobLoaded(Self,False)
          end;
//      end;

      end;


   tncSetJobScaling:
     begin
     if Response  = '1' then
        begin
        TerminateCurrentNCCommand(True,1,'1','','');
        end
      else
        begin
        TerminateCurrentNCCommand(False,-2,'','','Failed SetJob Scaling To Current Job')
      end;
     end;

   tncSetJobXOffset:
     begin
      if Response  = '1' then
          begin
          RunFollowOnCommands(True,1,Response,'');
          end
      else
          begin
          TerminateCurrentNCCommand(False,-2,Response,'','Failed to Set XandY offset to File');
          end;
     end;

   tncSetJobYOffset:
     begin
     if Response  = '1' then
        begin
        TerminateCurrentNCCommand(True,1,'1','','');
        end
      else
        begin
        TerminateCurrentNCCommand(False,-2,'','','Failed SetJob Y Offset To Current Job')
      end;
     end;

     tncSetJobExposure:
     begin
      if Response  = '1' then
          begin
          TerminateCurrentNCCommand(True,1,'1','','');
          if assigned(FOnUserExposureChanged) then FOnUserExposureChanged(Self,True,FInsipientExposure);
          end
      else
          begin
          TerminateCurrentNCCommand(False,-2,Response,'','Failed to Set Job Exposure');
          if assigned(FOnUserExposureChanged) then FOnUserExposureChanged(Self,False,0);
          end;
     end;


   tncGetJobList:
    begin
    case ResponseCount of
      0:
        begin
        ResponseTimeout.Enabled := False;
        inc(ResponseCount);
        if Response <> '1' then
          begin
          if assigned(FONNCCommandFail) then FONNCCommandFail(Self,-2);
          end
        else
          begin
          JobList.clear;
          end;
        end;

      1:
        begin
        FileCount := StrToIntDef(Response,-1);
        inc(ResponseCount);
        end;

      2:
        begin
        if FileCount > 0 then
          begin
          if Pos('.job',Response) > 0 then
            begin
            JobList.Add(Response);
            end
          end;
        if FileCount = 1 then
          begin
          If assigned(FOnJobListLoaded) Then FOnJobListLoaded(Self,JobList.Count);
{          JobListGrid.RowCount := JobList.Count+1;
          For FNum := 0 to JobList.Count-1 do
            begin
            JobListGrid.Cells[0,Fnum+1] := JobList[Fnum];
            end;
          NCCommand := False;
          NCCommandGoodComplete := True;
          VisionCommandStatusTag.AsInteger := 1;}
          end;
        dec(FileCount);
        end;
    end; // inner case
    end;

   tncSetSymbolicValueAsString,
   tncSetSymbolicValueAsInteger,
   tncSetSymbolicValueAsFloat:
     begin
     ResponseTimeout.Enabled := False;
      if Response  = '1' then
        begin
        NCCommandGoodComplete := True;
        TerminateCurrentNCCommand(True,1,Response,'','');
        end
      else
        begin
        case CurrentNCCommand.CommandType of
           tncSetSymbolicValueAsString  :TerminateCurrentNCCommand(False,-2,Response,'Error','Failed To Set Symbolic String Value');
           tncSetSymbolicValueAsInteger :TerminateCurrentNCCommand(False,-2,Response,'Error','Failed To Set Symbolic Integer Value');
           tncSetSymbolicValueAsFloat   :TerminateCurrentNCCommand(False,-2,Response,'Error','Failed To Set Symbolic Float Value');
           end; // case
        end;
     end;

   tncGetSymbolicValue:
    begin
    case ResponseCount of
      0:
      begin
      ResponseTimeout.Enabled := False;
      if Response  = '1' then
        begin
        end
      else
        begin
        TerminateCurrentNCCommand(False,-2,Response,'Error','Failed To Get Symbolic Value');
        If assigned(FOnGotValue) Then FonGotValue(Self,False,'');
      end;
      inc(ResponseCount);
      end;

      1:
      begin
      TerminateCurrentNCCommand(True,1,Response,Response,'');
      If assigned(FOnGotValue) Then FonGotValue(Self,True,Response);
      end;
    end;  // inner case
    end;

   tncSendNativeCommand:
     begin
     // Number of responses not know so have to set Datarecieved on First return
      ResponseTimeout.Enabled := False;
      if Response  = '1' then
        begin
        TerminateCurrentNCCommand(True,1,Response,'','');
        end
      else
        begin
        TerminateCurrentNCCommand(False,-2,Response,'','Failed Sending Native Command');
        end;
     end;

   tncGetSpreadsheetValue,tncGetCurrentJobName:
    begin
    case ResponseCount of
      0:
      begin
      if Response  = '1' then
        begin
        end
      else
        begin
          case CurrentNCCommand.CommandType of
          tncGetSpreadsheetValue : TerminateCurrentNCCommand(False,-2,'','','Failed to Get Spreadsheet Value');
          tncGetCurrentJobName   : TerminateCurrentNCCommand(False,-2,'','','Failed to Get Current Job Name');
          end ; //case
        end;
      inc(ResponseCount);
      end;
      1:
      begin
      ResponseTimeout.Enabled := False;
      TerminateCurrentNCCommand(True,1,Response,Response,'');
      end;
    end;  // inner case
    end; //tncGetSpreadsheetValue


   tncGetUserExposure:
    begin
    case ResponseCount of
      0:
      begin
      if Response  = '1' then
        begin
        end
      else
        begin
        TerminateCurrentNCCommand(False,-2,'','','Failed to Get User Exposure');
        end;
      inc(ResponseCount);
      end;
      1:
      begin
      ResponseTimeout.Enabled := False;
      TerminateCurrentNCCommand(True,1,Response,Response,'');
      if assigned(FOnUserExposureChanged) then FOnUserExposureChanged(Self,True,StrToFloatDef(Response,0));
      end;
    end;  // inner case
    end; //tncGetSpreadsheetValue


   tncSetLiveState:
    begin
    ResponseTimeout.Enabled := False;
    if Response  = '1' then
        begin
        TerminateCurrentNCCommand(True,1,Response,'','');
        end
    else
        begin
        TerminateCurrentNCCommand(False,-2,Response,'','Failed to set Live State');
        end;
    end;

   tncCheckOnline:
     begin
      ResponseTimeout.Enabled := False;
      if Response = '1' then
        begin
        TerminateCurrentNCCommand(True,1,Response,'','');
        FIsOnline := True;
        end
      else
        begin
        TerminateCurrentNCCommand(False,StrToIntDef(Response,-20),'','','Failed to Go Online');
        FIsOnline := False;
        end;

      If LastOnlineState <> FIsOnline then
        begin
        IF assigned(FOnLineChanged) then FOnLineChanged(Self,FIsOnline);
        end
    end;


   tncSetOfflineOnDisconnect,
   tncSetOfflineOnReset,tncDoneInitialOnLineConditioning:
    begin
    ResponseTimeout.Enabled := False;
    if Response  = '1' then
        begin
        FIsOnline := False;
        If assigned(FOnLineChanged) then FOnLineChanged(Self,FIsOnlIne);
        TerminateCurrentNCCommand(True,1,Response,'','');
        end
    else
        begin
        TerminateCurrentNCCommand(False,-2,Response,'','Failed to Go Off Line');
        end;
    end;

    tncRunJobGetStartCount:
      begin
      AddDebug(Format('Getting Start Pass Count %d %s',[ResponseCount,Response]),tvmDebug);
      case ResponseCount of
        0:
        begin
        if Response  = '1' then
          begin
          AddDebug('Good response to GVJob.Pass_Count',tvmDebug);
          end
        else
          begin
          TerminateCurrentNCCommand(False,-2,'','','Failed To Get Start Pass Count');
          if assigned(FonJobPassChecked) then FonJobPassChecked(False,False)
          end ;
        inc(ResponseCount);
        end;

        1:
        begin
        ResultNum := StrToFloatDef(Response,0);
        FLastPassCount := Round(ResultNum);
        AddDebug(Format('Start Pass_Count = %d',[FLastPassCount]),tvmDebug);
        RunFollowOnCommands(True,1,'1','');
        end;  // inner case
        end;
      end; //tncCheckJobPass

    tncResetandRunJob,tncResetJobDownBeforeRun:
      begin
      // carry on regarfless of result
      ResponseTimeout.Enabled := False;
      RunFollowOnCommands(True,1,'1','');
      end;

    tncRunJobOnLine1:
      Begin
      if Response  = '1' then
          begin
          FIsOnline := True;
          If assigned(FOnLineChanged) then FOnLineChanged(Self,FIsOnlIne);
          end;
      // Ignore failure as there is a second attenpt
      RunFollowOnCommands(True,1,Response,'');
      end;

    tncRunJobOnLine2:
      Begin
      if Response  = '1' then
          begin
          FIsOnline := True;
          If assigned(FOnLineChanged) then FOnLineChanged(Self,FIsOnlIne);
          RunFollowOnCommands(True,1,Response,'');
          end
      else
          begin
          RunFollowOnCommands(False,1,Response,'Go On Line Prior To Run Job');
//          TerminateCurrentNCCommand(Fals  e,-2,Response,'Failed to Go On Line Prior To Run Job');
          end;
      end;

    tncLoadJobCalled:
    begin
    ResponseTimeout.Enabled := False;
    if Response  = '1' then
        begin
        RunFollowOnCommands(True,1,Response,'');
        end
    else
        begin
        TerminateCurrentNCCommand(False,-2,Response,'','Failed to Load Job');
        if assigned(FonJobLoaded) then FOnJobLoaded(Self,False)
        end;
    end;

    tncFollowOn:
    begin
    ResponseTimeout.Enabled := False;
    if Response  = '1' then
        begin
        RunFollowOnCommands(True,1,Response,'');
        end
    else
        begin
        FollowOnMode := False;
        TerminateCurrentNCCommand(False,-2,Response,'','Failed to Load Job');
        if assigned(FonJobLoaded) then FOnJobLoaded(Self,False)
        end;
    end;

    tncSetScalingAfterJobLoad:
      begin
     {$IFDEF HIGHDEBUG}
      AddDebug('tncSetScalingAfterJobLoad',tvmOutOnly);
     {$ENDIF}
      ResponseTimeout.Enabled := False;
      if Response  = '1' then
        begin
        FIsOnLine := True;
        TerminateCurrentNCCommand(True,1,Response,'','');
//        if assigned(FOnLineChanged) then  FOnLineChanged(Self,FIsOnLine);
        if assigned(FonJobLoaded) then FOnJobLoaded(Self,True);
        if assigned(FOnJobNameChange) then FOnJobNameChange(Self,InsipientJob);
        end
    else
        begin
        FollowOnMode := False;
        TerminateCurrentNCCommand(False,-2,Response,'','Failed Load Job');
        if assigned(FonJobLoaded) then FOnJobLoaded(Self,False)
        end;
      end;

    tncRunJob:
      begin
      ResponseTimeout.Enabled := False;
      if Response = '1' then
        begin
        TerminateCurrentNCCommand(True,1,Response,'','');
        if assigned(FonRunJobDone) then FonRunJobDone(Self,True);
        end
      else
        begin
        TerminateCurrentNCCommand(False,-2,Response,'',Format('Run Job with Resonse %s',[Response]));
        if assigned(FonRunJobDone) then FonRunJobDone(Self,False);
        end;

      end;

    tncRunJobFastFirst,tncRunJobFastInMode: // response to sw8
      begin
      FFastRunFirstSweep := False;
      ResponseTimeout.Enabled := False;
      if Response  = '1' then
        begin
        TerminateCurrentNCCommand(True,1,Response,'','');
        if assigned(FonFastJobRunDone) then FonFastJobRunDone(Self,True);
        end
      else
        begin
        TerminateCurrentNCCommand(False,-2,Response,'','Run Job In Fast Mode');
        if assigned(FonFastJobRunDone) then FonFastJobRunDone(Self,False);
        end;
      end;



    tncLiveAfterJobRun:
      begin
      ResponseTimeout.Enabled := False;
      if Response  = '1' then
        begin
        TerminateCurrentNCCommand(True,1,Response,'','');
//        if assigned(FonLiveAfterRunJob) then FonLiveAfterRunJob(Self,True);
        end
    else
        begin
        TerminateCurrentNCCommand(False,-2,Response,'','Go Live After Run Job');
//        if assigned(FonLiveAfterRunJob) then FonLiveAfterRunJob(Self,False);
        end;
      end;

    tncCheckJobPass,tncCheckPassAfterFastRun:
    begin
    AddDebug(Format('Checking Pass %d %s',[ResponseCount,Response]),tvmDebug);
    case ResponseCount of
      0:
      begin
      if Response  = '1' then
        begin
        AddDebug('Good response to GVJob.Pass_Count',tvmDebug);
        end
      else
        begin
        TerminateCurrentNCCommand(False,-2,'','','Failed To Get Pass Status');
        if assigned(FonJobPassChecked) then FonJobPassChecked(False,False)
        end ;
      inc(ResponseCount);
      end;

      1:
      begin
      ResultNum := StrToFloatDef(Response,0);
      ResultInt := Round(ResultNum);
      If ResultInt > FLastPassCount  then
        begin
        AddDebug(Format('Got Job pASSED: PC : %d, LPC %d',[ResultInt,FLastPassCount]),tvmDebug);
        if assigned(FonJobPassChecked) then FonJobPassChecked(True,True);
        FLastPassCount := ResultInt
        end
      else
        begin
        AddDebug(Format('Got Job Failed: PC : %d, LPC %d',[ResultInt,FLastPassCount]),tvmDebug);
        if assigned(FonJobPassChecked) then FonJobPassChecked(True,False);
        FLastPassCount := ResultInt
        end;
      TerminateCurrentNCCommand(True,1,Response,'','');
      end;
      end;  // inner case
      end;
    end; //tncCheckJobPass


  end; // outer case

end;


procedure TCognexComunicator.ResponseTimedOut(Sender: Tobject);
begin
// clear down here and set event
ResponseTimeout.Enabled := False;
//ServiceTimer.Enabled := False;
//if assigned(FonIdleBusy) then FonIdleBusy(Self,False);
ServiceMode := smIdle;
AddStatusMessage('Response Timeout',tvmDebug);
TerminateCurrentNCCommand(False,-5,'TimeOut','','Timeout On command');
ServiceTimer.Enabled := False;
if CurrentNCCommand.CommandStr = 'GF' then
  begin
  AddDebug('Timeout on check Talking',tvmDebug);
  ClearDown;
  if assigned(FOnGFLock) then FOnGFLock(Nil);
  end;
end;

function TCognexComunicator.GetIsconnected: Boolean;
begin
Result := TelnetC.IsConnected;
end;


procedure TCognexComunicator.CheckIsTalking(Mode : TTalkingModes);
begin
  GFLockCount := 0;
  ResponseTimeout.Interval :=  2000;
  if IsConnected then
    begin
   {$IFDEF HIGHDEBUG}
    AddDebug('Sending Reset GF',tvmDebug);
   {$ENDIF}
    case Mode of
      tmRestore       :
        begin
        SendNCCommand('GF',tncTestTalkingRestore,'Get Filename on check talking',False);
       {$IFDEF HIGHDEBUG}
        AddDebug('Sending Reset GF for Restore',tvmDebug)
       {$ENDIF}
        end;
      tmAtLogin       :
        begin
        SendNCCommand('GF',tncTestTalkingLogin,'Get Filename on check talking',False);
       {$IFDEF HIGHDEBUG}
        AddDebug('Sending Reset GF for Login',tvmDebug)
       {$ENDIF}
        end;
      tmAtReconnect   :
        begin
       {$IFDEF HIGHDEBUG}
        SendNCCommand('GF',tncTestTalkingReconect,'Get Filename on reconnect',False);
       {$ENDIF}
        end;
      tmPowerOnReset  :
        begin
        SendNCCommand('GF',tncTestTalkingPowerOnReset,'Get Filename on Power on Resey',False);
       {$IFDEF HIGHDEBUG}
        AddDebug('Sending Reset GF for Power On Reset',tvmDebug)
       {$ENDIF}
        end;
      tmGeneralReset  :
        begin
        SendNCCommand('GF',tncTestTalkingReset,'Get Filename on General Reset',False);
       {$IFDEF HIGHDEBUG}
        AddDebug('Sending Reset GF for General Reset',tvmDebug)
       {$ENDIF}
        end;
      end;
    end
  else
    begin
    {$IFDEF HIGHDEBUG}
    AddDebug('Refused Reset',tvmDebug);
    {$ENDIF}
//    ResetInProgress := False;
    //!!!! Reconnect here
    end;

end;

{procedure TCognexComunicator.CheckIsTalking;
begin
  if IsConnected then
    begin
    AddErrorMessage('Old Style check is talking called',tvmDebug);
    AddDebug('Sending Reset GF',tvmDebug);
    SendNCCommand('GF',tncTestTalking,False);
    end
  else
    begin
    AddDebug('Refused Reset',tvmDebug);
//    ResetInProgress := False;
    //!!!! Reconnect here
    end;

end;}


procedure TCognexComunicator.TelnetDisconnected(Sender: Tobject);
begin
if assigned(FOnConnectTelnetChange) then FOnConnectTelnetChange(Self,False);
end;

procedure TCognexComunicator.TelnetLoggedIn(Sender: Tobject);
begin
if assigned(FOnLoginSuccess) then FOnLoginSuccess(Self);
end;



//Got welcome so complete Login
procedure TCognexComunicator.TelnetConnected(Sender: Tobject);
begin
if assigned(FOnConnectTelnetChange) then FOnConnectTelnetChange(Self,True);
if assigned(TelnetMemo) then
  begin
  TelnetMemo.AddDebugMessage(TelnetC.InBuffer);
  end;
CameraTelnetConnected := True;
TelnetLoginTimer.Enabled := True;
end;

procedure TCognexComunicator.TelnetLoginSpeakingButNoWelcome(Sender: Tobject);
var
CommandString : String;
begin
if assigned(TelnetMemo) then
  begin
  TelnetMemo.AddDebugMessage('--');
  TelnetMemo.AddDebugMessage(TelnetC.InBuffer);
  TelnetMemo.AddDebugMessage('--');
  TelnetMemo.AddDebugMessage('No Welcome recieved');
  TelnetMemo.AddDebugMessage('Forcing Verbose Mode');
  VerboseMode := tvmDebug;
  AddDebug('Starting Login No welcome',tvmDuplex);
  CommandString := FUsername;
  CommandString :=CommandString + CRLF;
  SendNCCommand(CommandString,tncLoginResponse,'',False);
  end;
end;

procedure TCognexComunicator.DoDelayedLogin(Sender: TObject);
var
CommandString : String;
begin
  TelnetLoginTimer.Enabled := False;
  LoginTimeout.Enabled := True;
  AddDebug('Starting Login-- Got Welcome',tvmDuplex);
  CommandString := FUsername;
  CommandString :=CommandString + CRLF;
  SendNCCommand(CommandString,tncLoginResponse,'at delayed login',False);
end;

procedure TCognexComunicator.DoLoginTimedout(Sender: TObject);
begin
TelnetLoginTimer.Enabled := False;
if assigned(FOnLoginTimeout) then FOnLoginTimeout(Self);
end;




function TCognexComunicator.GetNCFunctionPending: Boolean;

begin
Result := False;
if CommandList.Count > 0 then
  begin
  Result := CommandList.NCFunctionPending;
  end;
end;
procedure TCognexComunicator.AddOutMess(DString : String;LowestMode : TVModes);
begin
ExternalMemo.RollingBuffer.AddOutMessage(DString);
if cardinal(VerboseMode) >= cardinal(LowestMode) then
  begin
  ExternalMemo.AddOutMessage(DString);
  end;
end;

procedure TCognexComunicator.AddReplyMessage(DString : String;LowestMode : TVModes);
begin
ExternalMemo.RollingBuffer.AddReplyMessage(DString);
if cardinal(VerboseMode) >= cardinal(LowestMode) then
  begin
  ExternalMemo.AddReplyMessage(DString);
  end;
end;

procedure TCognexComunicator.AddErrorMessage(DString : String;LowestMode : TVModes);
begin
ExternalMemo.RollingBuffer.AddErrorMessage(DString);
if cardinal(VerboseMode) >= cardinal(LowestMode) then
  begin
  ExternalMemo.AddErrorMessage(DString);
  end;
end;

procedure TCognexComunicator.AddStatusMessage(DString: String; LowestMode: TVModes);
begin
ExternalMemo.RollingBuffer.AddStatusMessage(DString);
if cardinal(VerboseMode) >= cardinal(LowestMode) then
  begin
  ExternalMemo.AddStatusMessage(DString);
  end;

end;


procedure TCognexComunicator.AddSuccessMessage(DString : String;LowestMode : TVModes);
begin
ExternalMemo.RollingBuffer.AddSuccessMessage(DString);
if cardinal(VerboseMode) >= cardinal(LowestMode) then
  begin
  ExternalMemo.AddSuccessMessage(DString);
  end;
end;


procedure TCognexComunicator.TrimToSize;
begin

end;

procedure TCognexComunicator.AddDebug(DString: String;LowestMode : TVModes);
begin

ExternalMemo.RollingBuffer.AddDebugMessage(DString);
if cardinal(VerboseMode) >= cardinal(LowestMode) then
  begin
  ExternalMemo.AddDebugMessage(DString);
  end;
end;


procedure TCognexComunicator.HandleTelnetException(Sender: TObject);
begin
TelnetBroken := True;
if assigned(FonTelnetBroken) then FonTelnetBroken(Self);
end;

function TCognexComunicator.GetTelnetOK: Boolean;
begin
Result := TelNetC.IsConnected;
end;

procedure TCognexComunicator.GraceFullDisconnect;
begin
If assigned(TelnetC) then
  begin
  try
  ServiceTimer.Enabled := False;
  ResponseTimeout.Enabled := False;
  CommandList.Clean;
  FollowOnMode := False;
  ServiceMode :=  smIdle;
  TelnetC.TemporaryDisconnect;
  except

  end;
  end;
end;



procedure TCognexComunicator.TelnetHealthyChanged(Sender: TObject; IsHealthy: Boolean);
begin
If assigned(FOnHealthyStateChanged) then FOnHealthyStateChanged(Self,IsHealthy);
end;

procedure TCognexComunicator.SetExternalLockBusy(const Value: Boolean);
begin
  FExternalLockBusy := Value;
  if not FExternalLockBusy then
    begin
    LockTimout.Enabled := False;
    AddStatusMessage('Comms External Lock False',tvmDuplex);
    /// THis Put Back Start
{    if (ServiceMode = smIdle) and (CommandList.Count = 0) then
      begin
      if not FIsIdle then
        begin
        FIsIdle := True;
        If Assigned(FOnIdleBusy) then
          begin
          FOnIdleBusy(Self,FIsIdle,FInLineErrorResponse,FInLineErrorMessage);
          AddStatusMessage('Comms Idle (External Set)',tvmDuplex);
          end
        end;
      end
    else
      begin
      end
    /// THis Put Back End}
    end
  else
    begin
    If FIsidle then
      begin
      AddStatusMessage('Comms External Lock True Request)',tvmDuplex);
      if not LockTimout.Enabled then LockTimout.Enabled := True;
      Isidle := False;
      If Assigned(FOnIdleBusy) then
        begin
        SetInLineError('','');
        FOnIdleBusy(Self,FIsIdle,FInLineErrorResponse,FInLineErrorMessage);
        AddStatusMessage('Comms Busy (External Set)',tvmDuplex);
        ServiceTimer.Enabled := True;
        end
      end
    end;

  if FExternalLockBusy then
    AddOutMess('ExternalLockBusy = True',tvmDebug)
  else
    AddOutMess('ExternalLockBusy = False',tvmDebug)
end;

procedure TCognexComunicator.ClearDown;
begin
ServiceTimer.Enabled := False;
CommandList.Clean;
ServiceMode := smIdle;
NCCommandInProgress := False;
FExternalLockBusy := False;
if not FIsIdle then
  begin
  IsIdle := True;
  If Assigned(FOnIdleBusy) then
    begin
    SetInLineError('','');
    FOnIdleBusy(Self,FIsIdle,FInLineErrorResponse,FInLineErrorMessage);
    AddStatusMessage('Comms Idle on Clear Down',tvmDuplex);
    end
  end;
end;

procedure TCognexComunicator.SetResponseTimeoutInSeconds(const Value: Single);
begin
  FResponseTimeoutInSeconds := Value;
  If Value <=0 then
    begin
    TimeoutEnabled := False;
    ResponseTimeout.Enabled := False;
    end
  else
    begin
    TimeoutEnabled := True;
    ResponseTimeout.Interval :=  round(FResponseTimeoutInSeconds*1000);
    end
end;



procedure TCognexComunicator.ResetTimeoutToDefault;
begin
if TimeoutEnabled then ResponseTimeout.Interval := Round(ResponseTimeoutInSeconds*1000);
//AddDebug('*********************',tvmDebug);
//AddDebug('Timeout value reset to default',tvmDebug);
//AddDebug('*********************',tvmDebug);
end;

procedure TCognexComunicator.SetPostCommanddsPending(const Value: Boolean);
begin
  FPostCommandsPending := Value;
end;

procedure TCognexComunicator.SetFastRunMode(const Value: Boolean);
begin
  If (Not FFastRun) and Value then FFastRunFirstSweep := true else FFastRunFirstSweep := False;
  FFastRun := Value;
  If not FFastRun then FFastRunFirstSweep := False;
end;


procedure TCognexComunicator.HandleBusyLocked(Sender: TObject);
begin
 LockTimout.Enabled := False;
 ClearDown;
 if assigned(FOnLockout) then FOnLockOut(Self,LastCommandStr)
end;


procedure TCognexComunicator.SetBusyTimeout(const Value: Integer);
begin
  FBusyTimeOut := Value;
  LockTimout.Interval := FBusyTimeOut;
end;

procedure TCognexComunicator.SetInLineError(ResponseNumber, ResponseMessage: String);
begin
//AddDebug(Format('InLine = %s-%s',[ResponseNumber,ResponseMessage]),tvmDebug);
FInLineErrorResponse := ResponseNumber;
FInLineErrorMessage := ResponseMessage;
end;

procedure TCognexComunicator.SetIsIdle(const Value: Boolean);
begin
  if (Not Value) and FiSIdle then
    begin
    AddDebug('GoingBusy',tvmDebug);
    end;
  FIsIdle := Value;
end;


end.
