unit FTPPanel;

interface
uses CoreCNC, CNCTypes, CNC32, Classes, Windows, Controls, Graphics,StdCtrls,ExtCtrls,
TelnetIndicator,SysUtils,CognexFTP,CognexTypes,Grids;

const
  ButtonHeight = 28;

type


TFileListGrid = Class(TStringGrid)
  private
    FTitleText: String;
    FFileList: TStringList;
   procedure ResizeGrid(Sender : TObject);
    procedure SetTitleText(const Value: String);
    procedure SetFileList(const Value: TStringList);
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure UpdateFromFileList;
    property TitleText : String read FTitleText write SetTitleText;
    property FileList : TStringList read FFileList write SetFileList;

  end;

TFTPPanel = class(TPanel)
 private
    DisplayMode : TFTPDisplayModes;
    ButtonPanel : TPanel;
    TopPanel    : TPanel;
    CameraPanel : TPanel;
    RemotePanel : TPanel;
    LeftPanel   : TPanel;
    LoginIndicator : TNamedIndicator;
    DebugMemo : TMemo;
    CameraFileListGrid :TFileListGrid;
    RemoteFileListGrid :TFileListGrid;
    FLoggedIn : Boolean;
    FJobList: TStringList;
    FAllFileList: TStringList;

//   procedure UpdateFileList(GetDetail : Boolean);
   procedure LoginButtonClick(Sender : Tobject);
   procedure RedrawOnResize(Sender: TObject);
   procedure SetDisplayMode(DMode : TFTPDisplayModes);
    procedure SetLoggedIn(const Value: Boolean);
    procedure aaSetAllFileList(const Value: TStringList);
    procedure aaSetJobListList(const Value: TStringList);
 public
   constructor CreateonParent(aParent: TWinControl);
   destructor Destroy; override;
   Property LoggedIn : Boolean read FLoggedIn write SetLoggedIn;
   property AllFileList : TStringList read FAllFileList write aaSetAllFileList;
   property FTPJobList : TStringList read FJobList write aaSetJobListList;
 end;

implementation

{ TFTPPanel }


constructor TFTPPanel.CreateOnParent(aParent : TWinControl);
begin
  inherited Create(nil);
  FLoggedIn := False;
  Parent := AParent;
  BevelOuter := bvNone;
  BevelInner := bvNone;

  ButtonPanel := TPanel.Create(nil);
  ButtonPanel.Parent := Self;
  with ButtonPanel do
    begin
    Align := alRight;
    Width := 120;
    BevelInner := bvLowered;
    Color := clWebBeige;
    end;


  LeftPanel   := TPanel.Create(nil);
  LeftPanel.Parent := Self;
  with LeftPanel do
    begin
    Align := alClient;
    BevelInner := bvLowered;
    Color := clWebBeige;
    end;

  TopPanel := TPanel.Create(nil);
  TopPanel.Parent := LeftPanel;
  with TopPanel do
    begin
    Align := alTop;
    Height := 26;
    BevelInner := bvLowered;
    Color := clWebBeige;
    end;

  LoginIndicator := TNamedIndicator.CreateOnParent(TopPanel,False);
  with LoginIndicator do
    begin
    OnColour := clGreen;
    OffColour := clGray;
    Legend := 'Logged in';
    Width := 120;
    Height := 24;
    end;

  CameraPanel := TPanel.Create(Nil);
  CameraPanel.Parent := LeftPanel;
  with CameraPanel do
    begin
    Align := alLeft;
    Width := 100;
    end;

  RemotePanel := TPanel.Create(Nil);
  RemotePanel.Parent := LeftPanel;
  with CameraPanel do
    begin
    Align := alRight;
    Width := 100;
    end;


  DebugMemo := TMemo.Create(nil);
  DebugMemo.Parent := Self;
  DebugMemo.Align := alTop;
  DebugMemo.Visible := False;

//  FTPServer.DebugMemo := DebugMemo;

  CameraFileListGrid := TFileListGrid.Create(nil);
  CameraFileListGrid.Parent := CameraPanel;
  with CameraFileListGrid do
    begin
    align := alClient;
    TitleText := 'Camera Files';
    end;

  RemoteFileListGrid := TFileListGrid.Create(nil);
  RemoteFileListGrid.Parent := RemotePanel;
  with RemoteFileListGrid do
    begin
    align := alClient;
    TitleText := 'Remote Files';
    end;
  SetDisplayMode(ftpDual);
  OnResize := RedrawOnResize;
end;


procedure TFTPPanel.RedrawOnResize(Sender: TObject);

begin
  try
  with LoginIndicator do
    begin
    PanelTop := 2;
    PanelLeft := TopPanel.ClientWidth-117;
    PanelWidth := 115;
    PanelHeight := 22;
    Redraw;
    end;


  case DisplayMode of
    ftpDisplayStd:
      begin
      end;

    ftpDisplayDebug:
     begin
     end;

    ftpDual:
      begin
      CameraPanel.Width := LeftPanel.Width div 2;
      RemotePanel.Width := LeftPanel.Width div 2;
      end
    end; //case


  if DebugMemo.visible then DebugMemo.Height := ClientHeight div 4;


  except
  end;
end;


procedure TFTPPanel.LoginButtonClick(Sender: Tobject);
begin
{if assigned(FTPServer) then
  begin
  FTPServer.Login;
  end;}
end;

{procedure TFTPPanel.Login;
begin
FTPServer.Login;

end;}

{procedure TFTPPanel.UpdateFileList(GetDetail : Boolean);
begin
If LoggedIn then
  begin
  FTPServer.GetFilesToStringList(CameraFileListGrid.FileList,GetDetail);
  CameraFileListGrid.UpdateFromFileList;
  end
end;}

procedure TFTPPanel.SetDisplayMode(DMode: TFTPDisplayModes);
begin
DisplayMode := DMode;

case DMode of
 ftpDisplayStd:
  begin
  DebugMemo.Visible := False;
  RemoteFileListGrid.Visible := False;
  CameraFileListGrid.Align := alClient;
  end;

 ftpDisplayDebug:
  Begin
  DebugMemo.Align := alNone;
  CameraPanel.Align := alNone;
  DebugMemo.Visible := True;
  DebugMemo.Align := alTop;
  CameraPanel.Align := alClient;
  end;

 ftpDual:
  begin
  DebugMemo.Visible := False;
  DebugMemo.Align := alNone;
  CameraPanel.Align := alLeft;
  RemotePanel.Align := alRight;
  end
 end; // case
 RedrawOnResize(nil);
end;

procedure TFTPPanel.aaSetJobListList(const Value: TStringList);
begin
  FJobList := Value;
  CameraFileListGrid.FileList := FJobList;
end;

procedure TFTPPanel.aaSetAllFileList(const Value: TStringList);
begin
  FAllFileList := Value;

end;

destructor TFTPPanel.Destroy;
begin
  inherited;
end;

{ TFileListGrid }

constructor TFileListGrid.Create(AOwner: TComponent);
begin
  inherited;
  RowCount := 2;
  FixedRows := 1;
  FixedCols := 0;
  ColCount := 1;
  OnResize := ResizeGrid;
  FileList := TStringList.Create;
end;

destructor TFileListGrid.Destroy;
begin
 FileList.Clear;
 FileList.Free;
 inherited;
end;

procedure TFileListGrid.ResizeGrid(Sender: TObject);
begin
 ColWidths[0] := ClientWidth-5;
end;

procedure TFileListGrid.SetFileList(const Value: TStringList);
begin
  FFileList := Value;
  UpdateFromFileList;
end;

procedure TFileListGrid.SetTitleText(const Value: String);
begin
  FTitleText := Value;
  Cells[0,0] := FTitleText;
end;

procedure TFileListGrid.UpdateFromFileList;
Var
RowNum : Integer;
begin
  RowCount := 2;
  if assigned(FileList) then
    begin
    If FileList.Count > 0 then
      begin
      RowCount := FileList.Count+1;
      For RowNum := 1 to FileList.Count do
        begin
        Cells[0,RowNum] := FileList[RowNum-1];
        end;
      end;
    end;
end;

procedure TFTPPanel.SetLoggedIn(const Value: Boolean);
begin
  FLoggedIn := Value;
  LoginIndicator.OnStatus := FLoggedIn;
end;

end.
