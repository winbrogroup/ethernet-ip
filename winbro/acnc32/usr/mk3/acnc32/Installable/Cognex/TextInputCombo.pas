unit TextInputCombo;

interface
uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;


Type

TSendTextEvent = procedure(Sender : Tobject;OutText : String) of object;
TTextInputCombo = class(TComboBox)
  private
    FListSize : Integer;
    FOnTextSend : TSendTextEvent;
    FOnTextEntered: TSendTextEvent;
    procedure RegisterInternalText;
    function TextInList(TestText: String): Boolean;
  public
    procedure KeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    Procedure ClearDownTo(InitialText : String;ListSize : Integer);
    procedure RegisterText(InText : String);
    property OnTextEntered : TSendTextEvent read FOnTextEntered write FOnTextEntered;

  end;




implementation

{ TTextInputCombo }

procedure TTextInputCombo.ClearDownTo(InitialText : String;ListSize : Integer);
begin
Items.Clear;
Items.Add(InitialText);
Text := InitialText;
FListSize := ListSize;
end;

constructor TTextInputCombo.Create(AOwner: TComponent);
begin
  inherited;
  OnKeyDown := KeyDown;
end;


destructor TTextInputCombo.Destroy;
begin

  inherited;
end;


function TTextInputCombo.TextInList(TestText : String): Boolean;
var
TIndex : Integer;
begin
TIndex := Items.IndexOf(TestText);
Result := Tindex >= 0;
end;

procedure TTextInputCombo.KeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  if Key=13 then
    begin
    if Text <> '' then
      begin
      If assigned(FOnTextEntered) then FOnTextEntered(Self,Text);
      If (Items.Count > 0) then
        begin
        If not TextInList(Text) then
          begin
          RegisterInternalText;
          end;
        end
      else
        begin
        Items.Add(Text);
        end
      end;
    end;
end;

procedure TTextInputCombo.RegisterInternalText;
begin
  Items.Insert(0,Text);
   While Items.Count > FListSize do
     begin
     Items.Delete(Items.Count-1);
     end;
end;



procedure TTextInputCombo.RegisterText(InText: String);
begin
  Text := InText;
  RegisterInternalText;
end;

end.
