unit VisionCommand;

interface
uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls,Buttons, ExtCtrls, ComCtrls,Menus,   Grids,CognexTelnetClient,TelnetDisplay,CognexTYpes,CoreCNC,CNC32,
  IdBaseComponent, IdComponent, IdTCPServer, IdTelnetServer, IdTCPConnection, IdTCPClient, IdTelnet,PlusMemo;
type

TCommandMismatchEvent       = procedure(Sender : TObject;Added : Integer;Current : INteger; CommandDtr : String) of object;
TCommandMaxExent            = procedure(Sender : TObject;MaxCount : Integer) of object;
TMaxEvent                   = procedure(Sender : TObject; Max : Integer)of object;

TCocgnexInputMessage = class(TObject)
  public
  ServiceInterval : Integer;
  CommandStr  : String;
  CommandType : TNCCommands;
  IsNCCommand : Boolean;
  HasResponse : Boolean;
  CommandsFollowing : Integer;
  NCResponse  : String;
  ErrorString : String;
  Procedure AssignTo(var Dest : TCocgnexInputMessage);
  Procedure AssignFrom(Source : TCocgnexInputMessage);
  constructor Create(SInterval : Integer); overload;
  constructor Create(); overload;
  end;


TCognexCommandList = class(TList)
  private
  FAddedCommands : Integer;
  FonMismatch : TCommandMismatchEvent;
  FOnUnderRun : TNotifyEvent;
  NewMax      : TMaxEvent;
  public
    MaxCommands : Integer;
    procedure Clean;
    function NCFunctionPending():Boolean;
    procedure AddCommand(Command : TCocgnexInputMessage);
    Procedure RemoveCommand;
    property OnUnderrun : TNotifyEvent read FOnUnderRun write FOnUnderRun;
    property OnMismatch : TCommandMismatchEvent read FOnMismatch write FOnMismatch;
  end;



implementation
{ TCocgnexInputMessage }
procedure TCocgnexInputMessage.AssignTo(var Dest: TCocgnexInputMessage);
begin
if not assigned(Dest) then Dest := TCocgnexInputMessage.Create(ServiceInterval);
  Dest.CommandStr := CommandStr;
  Dest.CommandType := CommandType;
  Dest.IsNCCommand := IsNCCommand;
  Dest.HasResponse := HasResponse;
  Dest.CommandsFollowing := CommandsFollowing;
  Dest.NCResponse  := NCResponse;
  Dest.ErrorString := ErrorString;
  Dest.ServiceInterval :=ServiceInterval;

end;

procedure TCocgnexInputMessage.AssignFrom(Source: TCocgnexInputMessage);
begin
  CommandStr := Source.CommandStr;
  CommandType := Source.CommandType;
  IsNCCommand := Source.IsNCCommand;
  HasResponse := Source.HasResponse;
  CommandsFollowing := Source.CommandsFollowing;
  NCResponse  := Source.NCResponse;
  ErrorString := Source.ErrorString;
  ServiceInterval := Source.ServiceInterval;
end;

constructor TCocgnexInputMessage.Create(SInterval : Integer);
begin
  inherited Create;
  CommandsFollowing := 0;
  ServiceInterval := SInterval;
  ErrorString := '';
end;

constructor TCocgnexInputMessage.Create;
begin
  inherited Create;
  CommandsFollowing := 0;
  ServiceInterval := 20;
  ErrorString := '';
end;



{ TCognexCommandList }
procedure TCognexCommandList.AddCommand(Command: TCocgnexInputMessage);
begin
Add(Command);
If Capacity-Count < 10 then Capacity := Capacity+50;
FAddedCommands:= FAddedCommands+1;
if FAddedCommands > MaxCommands then
  begin
  MaxCommands := FAddedCommands;
  if assigned(NewMax) then NewMax(Self, MaxCommands);
  end;
if FaddedCommands <> Count then
  begin
  If Assigned(FonMismatch) then FonMismatch(Self,FAddedCommands,Count,Command.CommandStr);
  end;
end;

procedure TCognexCommandList.RemoveCommand();
var
DelCommand : TCocgnexInputMessage;
begin
if Capacity-Count > 100 then Capacity := Capacity -50;
if Count > 0 then
  begin
  try
  DelCommand := Items[0];
  DelCommand.CommandStr := '';
  DelCommand.Free;
  DelCommand := nil;
  Delete(0);
  FAddedCommands := FAddedCommands-1;
  except
  if assigned(FOnUnderRun) then FOnUnderRun(Self)
  end;
  end
else
  begin
  if assigned(FOnUnderRun) then FOnUnderRun(Self)
  end;
end;

procedure TCognexCommandList.Clean;
var
Command : TCocgnexInputMessage;
begin
while Count > 0 do
  begin
  Command := Items[0];
  Command.CommandStr := '';
  Command.Free;
  Command := nil;
  Delete(0);
  end;
FAddedCommands := 0;
MaxCommands := 0;
end;

function TCognexCommandList.NCFunctionPending: Boolean;
var
TestCommand : TCocgnexInputMessage;
TIndex : INteger;
begin
Result := False;
TIndex := 0;
if count > 0 then
  begin
  while (Result =False) and (TIndex < Count) do
    begin
    try
    TestCommand := Items[TIndex];
    Result := TestCommand.IsNCCommand;
    finally
    inc(Tindex);
    end;
    end;
  end;
end;

end.
