unit CognexActions;

interface
uses CoreCNC, CNCTypes, CNC32, Classes, Windows, Graphics, SysUtils, NCNames,TelnetIndicator,TelnetIndicatorPanel,
  CNCAbs, Named, Controls,ExtCtrls,ActiveX,CognexTelnetClient,StdCtrls,Grids,Buttons,TelnetDisplay,CognexTYpes,
  AbstractScript,IScriptInterface,INamedInterface,IniFiles,Forms,CogNexCommunicator,TextInputCombo,FTPTelnetClient,
  CvsInSightDisplayOcx_TLB;//,CognexInSight_TLB,CognexInSightDisplay_TLB;
  {$DEFINE HIGHDEBUG}

type
  TCameraState = record
     PixelWidth        : Integer;
     PixelHeight       : Integer;
     Scale             : Double;
     FitMode           : Integer;
     CentreModeInteger : Integer;
     CentreMode        : TCentreModes;
     CircleRadius      : Double;
     aaXHairOn         : Integer;
     CircleOn          : Integer;
     XHairColorNumber  : Integer;
     CircleColorNumber : Integer;
     Exposure          : Double;
     JobScaling        : Double;
     XOffset           : Double;
     YOffset           : Double;
     OffsetIsManual    : Integer;
     end;

  TImageScaleChangedEvent = procedure(Sender : Tobject; Scale : Double) of object;
  TCalDataChangedEvent    = procedure(Sender : Tobject; Scaling : Double;Xoffset : Double;Yoffset : DOuble) of object;
  TBannerMessageEvent     = procedure(Sender : Tobject; MessText : String) of object;
  TOffsetOwnerChangeEvent = procedure(Sender : Tobject; IsManual : Integer) of object;
  TOverrunEvent           = procedure(Sender : Tobject; ErrMessText : String) of object;
  TFTPLoginEvent          = procedure(Sender : Tobject; Success : Boolean) of object;
  TFileListChangedEvent   = procedure(Sender : Tobject; AtPOwerUp : Boolean) of object;
  TOnSnapDoneEvent        = procedure(Sender : Tobject; Passed : Boolean) of object;
  TResetTimeEvent         = procedure(Sender : Tobject; ResetTime : Double) of object;
  TJobResultEvent         = procedure(Sender : Tobject; Passed : Boolean;InLineError : Boolean;InFastRun : Boolean;ExecutionTime : Double) of object;
  TGotValEvent            = procedure(Sender : Tobject; Success : Boolean;Value: String) of object;

  TCameraActionRequestor = class(TObject)
  private
    ImportFilePath        :String;
    SnapShotFilePath      :String;
    TelnetFilePath        : String;

    SnapCheckCount        : Integer;
    TelnetCheckCount      : Integer;
    SnapFileSize          : LongInt;
    SnapHandle            : Integer;
    SnapFile              : File;

    SnapWithGraphics      : Boolean;
    SnapScale             : Double;
    FTPLoggedIn           : Boolean;
    ActionTimer           : TTimer;
    WaitTimeout           : TTimer;
    LazyUpdateTimer       : TTimer;
    LazyRestoreTimer      : TTimer;
    ResetTimeout          : TTimer;
    OfflineWaitTimer      : TTimer;
//    CAInitialConnectTimeout : TTimer;

    DisconnectPending   : Boolean;
    ReconnectPending    : Boolean;
    FullResetPending    : Boolean;
    RestorePending      : Boolean;
    JobLoadPending      : Boolean;
    BufferToXhairPending: Boolean;
    FitPending          : Boolean;
    XHairEstablishPending : Boolean;
    RunJobPending         : Boolean;
    CheckJobPassPending   : Boolean;
    FTPLoginPending       : Boolean;
    FTPImportPending      : Boolean;
    SnapshotPending       : Boolean;
    TelnetSavePending     : Boolean;
    ScreenUpdatePending   : Boolean;
    GetValuePending       : Boolean;
    SetValuePending       : Boolean;

    LoginInProgress             : Boolean;
    FTPLoginInProgress          : Boolean;
    SnapShotInProgress          : Boolean;
    TelnetSaveProgress          : Boolean;
    FTPImportInProgress         : Boolean;
    FullResetInProgress         : Boolean;
    XHairEstablishInProgress    : Boolean;
    BufferToXhairInProgress     : Boolean;
//    BufferToStandard            : Boolean;
    FitInProgress               : Boolean;
    RestoreInProgress           : Boolean;
    JobLoadInProgress           : Boolean;
    DisconnectInProgress        : Boolean;
    DisconectedFlag             : Boolean;
    ReconnectInProgress         : Boolean;
    RunJobInProgress            : Boolean;
    CheckJobPassInProgress      : Boolean;
    GetValueInProgress          : Boolean;
    SetValueInProgress          : Boolean;

    InsipientGetValueName         : String;
    InsipientSetValueName         : String;
    InsipientSetValueType         : TVisValTypes;

    BufferNewCentreOnXHair : Integer;
    BufferNewExposure      : Double;
//    BufferNewScale : Double;
    InShutDown : Boolean;
    IsFastRun : Boolean;
    Restart : Boolean;
    ActionStage : TCameraActionStages;
    FIsIdle : Boolean;
    FLatchJob: Boolean;
    OffLineRetry : Integer;
    ForceFullReset : Boolean;
    IsResetFit     : Boolean;
    IsResetRestore : Boolean;
    IsReconnectReset : Boolean;
    ReconnectDone    : Boolean;
    ForceTestOnRecon : Boolean;

    IsFitRestore   : Boolean;
    FDefaultJobActive: Boolean;
    FDefaultJobName: String;
    FOnCalDataChanged: TCalDataChangedEvent;
    FOnSnapDone      : TOnSnapDoneEvent;
    FOnGotValue      : TGotValEvent;
    FConfigFileName: String;
    FEnableCameraUpdate: Boolean;
    FUpdateCalibration: Boolean;
    FOnOffsetIsManualChange: TOffsetOwnerChangeEvent;
    FCurrentScale: Double;
    FInstallDone: Boolean;
    FXHCentreMode: Integer;
    FOnResetComplete: TNotifyEvent;
    FOnResetFail    : TNotifyEvent;

    FonInitialResetComplete : TNotifyEvent;
    LoadJobName : String;
    LatchLoadJob : Boolean;
    FConnectString: String;
    FCurrentJobIsBuffered : Boolean;
    FInDisconnect: Boolean;
    FInReconnect : Boolean;
    FOnReconnectComplete: TNotifyEvent;
    FOnOverrunError : TOverrunEvent;
    FOnBannerMessage: TBannerMessageEvent;
    FOnFitComplete: TNotifyEvent;
    FFTPPort: Integer;
    FFTPIPAddress: String;
    FFTPUserName: String;
    FFTPPassword: String;
    FOnFTPLoggedIn: TFTPLoginEvent;
    FonJobListChanged : TFileListChangedEvent;


    ResetStartTime : TDateTime;
    ResetTime : Double;
    JobStartTime : TDateTime;
    FJobRunTime: Double;
    FCheckResetTime : Boolean;
    FMaxResetTime : Double;
    PostFirstLoginReset : Boolean;
    FForceDisconRecon : Boolean;
    FInDisConRecon : Boolean;
    IsDisconReconReset  : Boolean;
    WasInLineError : Boolean;

    FJobList: TStringList;      // THis one
    FAllFileList: TStringList;
    FOnFTPLoginComplete: TFTPLoginEvent;
    FonPostCommandOverrun: TNotifyEvent;
    FOnReconnectStart: TNotifyEvent;
    FOnDisconectComplete: TNotifyEvent;
    FOnResetTimeAvailable: TResetTimeEvent;
    FonTestedPOwerUp: TNotifyEvent;
    LastPassCount : Integer;
    FOnPassJobResult: TJobResultEvent;
    FRestoreValid : Boolean;
    GotValue : String;

    //    FImmediateCommandPending: Boolean;
    ImmediateCommandLock : Boolean;
    FOnFreeToDoImediateCommand: TNotifyEvent;
//    FOnSnapshotTaken: TSnapShotEvent;
    procedure ClearDown;
    procedure AwaitAction;
    procedure EndAction;
    procedure AddDebug(DString: String; LowestMode: TVModes);
    procedure SetDefaultActive(const Value: Boolean);
    procedure SetXHairOnOff(OnState: Boolean);
    procedure SetCircleOnOff(OnState: Boolean);

    function CameraCanTalk : Boolean;
    procedure InternalCentreOnFOV;
    procedure InternalCentreOnXHair;
    procedure ProcessFitChanged;
//    procedure SetFitMode(AsPartofReset: Boolean);
    procedure SetActionTimer(State : Boolean);
    procedure SetCurrentScale(const Value: Double);
    procedure OfflineFailed(Sender: TObject);
    procedure SetIdle(const Value: Boolean);
    procedure FTPActionComplete(ActionType: TFTPActionTypes;Success : Boolean);
    procedure FTPIsLoggedIn (Sender : TObject;LoginState : Boolean);
    procedure JobListHasChanged(Sender: TObject;AtPOwerUp : Boolean);
    procedure SetTelnetMemo(const Value: TTelnetDisplay);
    procedure SetLatchJob(const Value: Boolean);
    function GetScreenIsInResetMode: Boolean;
    procedure TidyDownResetCheck;
//    procedure SetImmediateCommandPending(const Value: Boolean);
    function CanStart:Boolean;




  public
    ResetFitMode : Integer;
    ResetXHMode : INteger;
    ResetImageScale : Double;
    COXTag            : TAbstractTag;            // Externally Set
    COFOVTag          : TAbstractTag;            // Externally Set
    IsFullFOVTag      : TAbstractTag;            // Externally Set
    XHairIsOnTag      : TAbstractTag;            // Externally Set
    CircleIsOnTag     : TAbstractTag;            // Externally Set
    SceneScaleTag     : TAbstractTag;            // Externally Set
    LatchJobTag       : TAbstractTag;            // Externally Set
    ScalingTag        : TAbstractTag;            // Externally Set              x
    XoffsetTag        : TAbstractTag;            // Externally Set
    YoffsetTag        : TAbstractTag;            // Externally Set
    OnLineTag         : TAbstractTag;            // Externally Set
    IsReadyTag        : TAbstractTag;            // Externally Set
    XHairBufferedTag  : TAbstractTag;            // Externally Set
    DisableXhairButtonTag : TAbstractTag;            // Externally Set

    TimeoutEnabled : Boolean;
    ForceReset     : Boolean;
    UseSO2         : Boolean;
    FTPServer      : TFTPTelnetClient;

    CogNexCommunicator : TCognexComunicator; // Externally Set
    FTelnetMemo : TTelnetDisplay;             // Externally Set
    EmulatorMode : Boolean;                  // Externally Set
    CognexDisplay : TCvsInSightDisplay;      // Externally Set
    CognexParent : TPanel;                    // Externally Set
    DisconnectCount : Integer;
    ControlledDisconnect : Boolean;


    constructor Create;
    destructor Destroy; override;

    // Vision/Telnet Actions
    function RequestReset(CurrentJobBuffered : Boolean;InternalForce : Boolean) : Boolean;
    function RequestFit(InternalForce : Boolean) : Boolean;
    function RequestFullScale(IsInCycle : Boolean) : Boolean;
    function RequestXHairEstablish(InternalForce : Boolean) : Boolean;
    function RequestBufferToXHair(InternalForce : Boolean): Boolean;
    function RequestLoadJob(JobName : String;LatchJob : Boolean;InternalForce : Boolean) : Boolean;
    function RequestRestore(InternalForce : Boolean)     : Boolean;
    function RequestDisconnect(InternalForce : Boolean)  : Boolean;
    function RequestReconnect(InternalForce : Boolean;RetestOnDone : Boolean): Boolean;
    function RequestRunJob(FastMode : Boolean): Boolean;
    function RequestCheckJobPass(InLineError : Boolean): Boolean;
    procedure RequestScreenUpdate;
    function RequestSaveSnapshot(FPath : String;WithGraphics : Boolean;CurrentSceneScale : Double):Boolean;
    function RequestSaveTelnetLog: Boolean;
    function RequestGetValue(ValueName : String): Boolean;
    function RequestSetValue(ValueName : String;ValueType : TVisValTypes): Boolean;

    // FTPActions
    function RequestFTPInitialConnectandLogin(): Boolean;
    function RequestCameraConnectandLogin(): Boolean;
    function RequestImportFile(FPath : String):Boolean;
    procedure EndWaitInitialJobList(Success : Boolean);
    procedure EndWaitForInitialLogin(Success : Boolean);
    procedure EndWaitForTalking(Success : Boolean;TalkMode : TTalkingModes;FIsonline :Boolean);
    procedure EndWaitForOfflineOnReset(Success : Boolean);
    procedure EndWaitForOfflineRetry(Success : Boolean);
    procedure EndWaitForDefaultLoadOnReset(Success : Boolean);
    procedure EndWaitForGoLiveOnreset(Success : Boolean);
    procedure EndWaitForJobLoad(Success : Boolean);
//    procedure EndWaitForXHairLoadonBuffer(Success : Boolean);
    procedure EndWaitForResetRestore(Success : Boolean);
//    procedure EndWaitForResetRestoreOnBuffer(Success : Boolean);
    procedure EndWaitForOfflineOnDisconnect;
    procedure EndWaitForTelnetDisconnect(Success : Boolean);
    procedure EndWaitForCameraDisconnect(Success : Boolean);
    procedure EndWaitForReconnectLogin(Success : Boolean);
    procedure EndWaitForResetFit(Success : Boolean);
    procedure EndWaitForJobRunComplete(Success : Boolean);
    procedure EndWaitForFastJobRunComplete(Success: Boolean);

    procedure EndWaitForLiveAfterJobRun(Success : Boolean);
    procedure EndWaitForJobPassCheck(Success: Boolean;Passed: Boolean);
    procedure EndWaitForOnLineForFileImport(Success : Boolean);
    procedure EndWaitForOffLineForFileImport(Success : Boolean);

    procedure EndWaitForFTPLogin(Success : Boolean);
    procedure EndWaitForInitialJobList(Success : Boolean);
    procedure EndwaitForFileImport(Success : Boolean);

    procedure EndWaitForGetValue(Success : Boolean;Value: String);
    procedure EndWaitForSetValue(Success : Boolean);

    procedure ShutDownRequest;

    procedure DefaultJobImageScaleChanged(Sender: TObject);
    procedure ChangeLoadedJobImageScale(NewScale : Double;CentreOnXHair : Integer);

    procedure SaveCameraState;
    procedure KickLazyUpdate;
    procedure KickLazyRestore;
    procedure DoLazyUpdate(Sender: TObject);
    procedure DoScreenUpdateRequest(Sender: TObject);

    procedure DoScreenUpdate;
    procedure DoLazyRestore(Sender: TObject);
    procedure ResetStuck(Sender: TObject);
//    procedure ConnectTimedOut(Sender: TObject);

    procedure DoCalibrationUpdate;

    procedure aaCameraAction(Sender : TObject);
    procedure DoNothing(Sender : TObject);
    procedure WaitTimedOut(Sender : TObject);

    property FTPPortNumber : Integer read FFTPPort write FFTPPort;
    property FTPIPAddress  : String read FFTPIPAddress write FFTPIPAddress;
    property FTPUsername   : String read FFTPUserName write FFTPUserName;
    property FTPPassword   : String read FFTPPassword write FFTPPassword;


    property CurrentScale : Double read FCurrentScale write SetCurrentScale;
    property XHCentreMode : Integer read FXHCentreMode write FXHCentreMode;
    property InstallDone : Boolean read FInstallDone write FInstallDone ;
    property LatchJob : Boolean read FLatchJob write SetLatchJob;
    property DefaultJobActive : Boolean read FDefaultJobActive write SetDefaultActive;
    property DefaultJobName : String Read FDefaultJobName write FDefaultJobName;
    property ScreenInResetMode : Boolean read GetScreenIsInResetMode;

    property ConfigFileName : String read FConfigFileName write FConfigFileName;
    property EnableCameraUpdate : Boolean read FEnableCameraUpdate  write FEnableCameraUpdate;
    property ConnectString : String read FConnectString write FConnectString;
    property InDisconnect : Boolean read FInDisconnect;
    property InReconnect : Boolean read FInReconnect;
    property aaIsIdle : Boolean Read FIsIdle write SetIdle;
    property TelNetMemo : TTelnetDisplay read FTelnetMemo write SetTelnetMemo;
    property UpdateCalibration : Boolean read FUpdateCalibration write FUpdateCalibration;
    property CheckResetTime : Boolean read FCheckResetTime write FCheckResetTime;
    property ForceDisconRecon : Boolean read FForceDisconRecon write FForceDisconRecon;
    property InDisConRecon : Boolean read FInDisConRecon write FInDisConRecon;
    property JobRunTime: Double read FJobRunTime;
//    property ImmediateCommandPending : Boolean read FImmediateCommandPending write SetImmediateCommandPending;

    //Events
    property OnBannerMessage : TBannerMessageEvent read FOnBannerMessage write FOnBannerMessage;
    property OnCalDataChanged : TCalDataChangedEvent read FOnCalDataChanged write FOnCalDataChanged;
    property OnOffsetIsManualChange : TOffsetOwnerChangeEvent read FOnOffsetIsManualChange write FOnOffsetIsManualChange;
    property OnResetComplete : TNotifyEvent read FOnResetComplete write FOnResetComplete;
    property OnResetFail : TNotifyEvent read FOnResetFail write FOnResetFail;
    property OnInitialResetComplete: TNotifyEvent read  FonInitialResetComplete write FonInitialResetComplete;
    property OnActionOverrun  :TOverrunEvent read FOnOverrunError write FOnOverrunError;
    property OnFitComplete : TNotifyEvent read FOnFitComplete write FOnFitComplete;
    property OnPostCommandOverrun : TNotifyEvent read FonPostCommandOverrun write FonPostCommandOverrun;
    property OnSnapDone    : TOnSnapDoneEvent read FOnSnapDone  write FOnSnapDone;
    property OnReconnectStart : TNotifyEvent read FOnReconnectStart write FOnReconnectStart;
    property OnReconnectComplete : TNotifyEvent read FOnReconnectComplete write FOnReconnectComplete;
    property OnDisconectComplete : TNotifyEvent read FOnDisconectComplete write FOnDisconectComplete;
    property OnResetTimeAvailable : TResetTimeEvent read FOnResetTimeAvailable write FOnResetTimeAvailable;
    property OnTestedPOwerUp : TNotifyEvent read  FonTestedPOwerUp write FonTestedPOwerUp;
    property OnPassJobResult : TJobResultEvent read  FOnPassJobResult write FOnPassJobResult;
    property OnFreeToDoImediateCommand : TNotifyEvent  read FOnFreeToDoImediateCommand write FOnFreeToDoImediateCommand;
    property ONGotValue : TGotValEvent read FONGotValue write  FONGotValue;
    property RestoreValid : Boolean read FRestoreValid write FRestoreValid;






    // FTPProperties

    property AllFileList : TStringList read FAllFileList ;
    property JobList : TStringList read FJobList ;

    // FTPEvents

    property OnFTPLoginComplete : TFTPLoginEvent read FOnFTPLoginComplete write FOnFTPLoginComplete;
    property OnFTPLoggedIn : TFTPLoginEvent read FOnFTPLoggedIn write FOnFTPLoggedIn;
    property OnJobListChanged : TFileListChangedEvent read FonJobListChanged write FonJobListChanged;
//    property aaOnFileImported : TFTPFileImportedEvent read FonFileImported write FonFileImported;
    end;

    var
    CurrentCameraState : TCameraState;
    BufferCameraState : TCameraState;

implementation

uses GeneralTelnetClient,Cognex;

{ TCameraTimerRequestor }


constructor TCameraActionRequestor.Create;
begin
//FImmediateCommandPending := False;
ReconnectDone := False;
DisconectedFlag := True;
FRestoreValid := False;
ImmediateCommandLock := False;
FMaxResetTime := 2000;
InDisConRecon := False;
DisconnectCount := 0;
FAllFileList := TStringList.Create;
FJobList := TStringList.Create;
//FTPIdleCount :=0;
FTPLoggedIn := False;
InShutDown := False;
ForceReset := False;
TimeoutEnabled := True;
FDefaultJobActive := False;
FUpdateCalibration := False;
FDefaultJobName := '';
IsResetRestore := False;
IsFitRestore   := False;
ForceFullReset := False;
aaIsIdle := True;

FTPServer := TFTPTelnetClient.Create(nil);
with FTPServer do
  begin
//  Initialise;
  OnFTPActionComplete := FTPActionComplete;
  OnJobListChanged := JobListHasChanged;
  HostIPAddress := '10.0.1.42';
  PortNumber := 25;
  ExternalAllFileList := FallFileList;
  aExTernalJobList := FJobList;
  end;

ActionTimer := TTimer.Create(nil);
with ActionTimer do
  begin
  Enabled := False;
  Interval := 20;
  OnTimer := aaCameraAction;
  end;

LazyUpdateTimer := TTimer.Create(nil);
with LazyUpdateTimer do
  begin
  Enabled := False;
  Interval := 200;
  OnTimer := DoLazyUpdate;
  end;



LazyRestoreTimer := TTimer.Create(nil);
with LazyRestoreTimer do
  begin
  Enabled := False;
  Interval := 200;
  OnTimer := DoLazyRestore;
  end;

ResetTimeout := TTimer.Create(nil);
with ResetTimeout do
  begin
  Enabled := False;
  Interval := 15000;
  OnTimer := ResetStuck;
  end;

OfflineWaitTimer := TTimer.Create(nil);
with OfflineWaitTimer do
  begin
  Enabled := False;
  Interval := 2000;
  OnTimer := OffLineFailed;
  end;

{CAInitialConnectTimeout := TTimer.Create(nil);
with CAInitialConnectTimeout do
  begin
  Enabled := False;
  Interval := 5000;
  OnTimer := ConnectTimedOut;
  end;}

WaitTimeout := TTimer.Create(nil);
with WaitTimeout do
  begin
  Enabled := False;
  Interval := 10000;
  OnTimer := WaitTimedOut;
  end;
 TelnetMemo := Nil;
 ClearDown;

end;

procedure TCameraActionRequestor.AddDebug(DString: String;LowestMode :TVModes);
begin
if assigned (TelnetMemo) then
  begin
  if cardinal(CogNexCommunicator.VerboseMode) >= cardinal(LowestMode) then
    begin
    if  LowestMode = tvActionStageOnly then
      begin
      TelnetMemo.AddStageMessage(DString);
      end
    else
      begin
      if CogNexCommunicator.VerboseMode = tvmSuperDebug then
        begin
        if (LowestMode = tvmSuperDebug) then TelnetMemo.AddDebugMessage(DString);
        end
      else
        TelnetMemo.AddDebugMessage(DString);
      end;
    end
  end
else
  begin

  end;
end;

function TCameraActionRequestor.CanStart:Boolean;
begin
Result := FIsIdle and not ImmediateCommandLock;
if Result = False then
  begin
  if FIsIdle = False then AddDebug('FIsIdle = False',tvmDebug);
  if ImmediateCommandLock then AddDebug('ImmediateCommandLock',tvmDebug);
  end;
end;

function TCameraActionRequestor.RequestLoadJob(JobName : String;LatchJob : Boolean;InternalForce : Boolean): Boolean;
begin
Result := True;
if CanStart then
  begin
  LoadJobName := JobName;
  LatchLoadJob := LatchJob;
  JobLoadInProgress := True;
  if CogNexCommunicator.FastRunMode then
    begin
    CogNexCommunicator.FastRunMode := False;
    ActionStage := tcaOffLineForJobLoad;
    end
  else
    begin
    ActionStage := tcaLoadJobStart;
    end;
  SetActionTimer(True);
  end
else
  begin
  JobLoadPending := True;
  end
end;

function TCameraActionRequestor.RequestBufferToXHair(InternalForce : Boolean): Boolean;
begin
Result := True;
//BufferToStandard := CentreOnXHair < 0;
FLatchJob := False;
LatchJobTag.AsInteger := 0;
if CanStart then
  begin
  ForceFullReset := True;
  FullResetInProgress := True;
  ActionStage := tcaStartFullReset;
  SetActionTimer(True);
  end
else
  begin
  if Not BufferToXhairPending then
    begin
    if not(FullResetPending or FullResetInProgress) then
      begin
      FullResetPending := True;
      end
    end;
  end
end;



function TCameraActionRequestor.RequestXHairEstablish(InternalForce : Boolean): Boolean;
begin
Result := True;
if CanStart then
  begin
  XHairEstablishInProgress := True;
  ActionStage := tcaStartXhairEstablish;
  SetActionTimer(True);
  end
else
  begin
  if Not XHairEstablishPending then
    begin
    if not(FullResetPending or FullResetInProgress) then
      begin
      XHairEstablishPending := True;
      end
    end;
  end
end;




function TCameraActionRequestor.RequestReset(CurrentJobBuffered : Boolean;InternalForce : Boolean): Boolean;
begin
Result := True;
if InShutDown then exit;
ResetTimeout.Enabled := True;
if ForceReset or InternalForce or (XHairBufferedTag.AsInteger = 1) then
  begin
  AddDebug ('Forced reset',tvmOutOnly);
  ForceReset := False;
  XHairBufferedTag.AsInteger := 0;
  CogNexCommunicator.ClearDown;
  ClearDown;
  FCurrentJobIsBuffered := False;
  ForceFullReset := True;
  FullResetInProgress := True;
  ActionStage := tcaStartFullReset;
  SetActionTimer(True);
  end
else
  begin
  if CanStart then
    begin
    FCurrentJobIsBuffered := CurrentJobBuffered;
    FullResetInProgress := True;
    ActionStage := tcaStartFullReset;
    SetActionTimer(True);
    end
  else
    begin
    {$IFDEF HIGHDEBUG}
    if JobLoadInProgress then
      begin
      AddDebug('Reset will occur After Job Load',tvmDebug);
      end;
    {$ENDIF}

    FullResetPending := True;
    end
  end;
end;


function TCameraActionRequestor.RequestDisconnect(InternalForce : Boolean): Boolean;
begin
Result := True;
if InShutDown then exit;
InDisConRecon := False;
if CanStart or InternalForce then
  begin
  DisconnectPending := False;
  DisconnectInProgress := True;
  if Not CognexDIsplay.Connected then  AddDebug('Disconnect request already Disconnected',tvmDebug);
  ActionStage := tcaDisconnectStart;
  CogNexCommunicator.ExternalLockBusy := True;
  FInDisconnect := True;
  SetActionTimer(True);
  end
else
  begin
  DisconnectPending := True;
  end;
end;

function TCameraActionRequestor.RequestReconnect(InternalForce : Boolean;RetestOnDone : Boolean): Boolean;
begin
Result := True;
if InShutDown then exit;
if CanStart or InternalForce then
  begin
  ReconnectPending := False;
  ReconnectInProgress := True;
  ForceTestOnRecon := RetestOnDone;
  GlobalCognexForm.StartConnectTimeout;
  ActionStage := tcaReconnectStart;
  FInDisconnect := False;
  FInReconnect := True;
  SetActionTimer(True);
  end
else
  begin
  AddDebug('Can Start Is False',tvmDebug);
  if Not ReconnectInProgress then ReconnectPending := True;
  end;
end;

function TCameraActionRequestor.RequestRestore(InternalForce : Boolean): Boolean;
begin
Result := True;
if (FRestoreValid = False)  then
  begin
  AddDebug('Restore Blocked.. Camera Disconnected',tvmDebug);
  exit;
  end;
if InShutDown then exit;
//if not  then exit;
if CanStart or InternalForce then
  begin
  aaIsIdle := False;
  RestoreInProgress := True;
  ActionStage := tcaRestoreCheckStart;
  SetActionTimer(True);
  end
else
  begin
  if Not FullResetPending then
    begin
    if not(FullResetInProgress) then
      begin
      RestorePending := True;
      end
    end;
  end
end;


function TCameraActionRequestor.RequestFullScale(IsInCycle : Boolean): Boolean;
begin
if InShutDown then exit;
If (FitInProgress) or FullResetPending or BufferToXhairInProgress or XHairEstablishInProgress or GetValuePending or GetValueInProgress then
  begin
  exit;
  end;
//if aaIsIdle then
  begin
  ChangeLoadedJobImageScale(1,1);
  CurrentCameraState.CentreModeInteger := 1;
  CurrentCameraState.CentreMode := tcmXHair;

  if (not IsInCycle) and DefaultJobActive then
      begin
      KickLazyUpdate;
      end;
  end;
end;


function TCameraActionRequestor.RequestFit(InternalForce : Boolean): Boolean;
begin
Result := True;
if InShutDown then exit;
If (FitInProgress) or FullResetPending or BufferToXhairInProgress or XHairEstablishInProgress or GetValuePending or GetValueInProgress then
  begin
  exit;
  end;
if (CanStart or InternalForce) and not CogNexCommunicator.ExternalLockBusy  then
  begin
  FitInProgress := True;
  FitPending := False;
  CurrentCameraState.FitMode := 1;
  CurrentCameraState.CentreModeInteger := 1;
  CurrentCameraState.CentreMode := tcmScreen;
  ActionStage := tcaStartFit;
  IsResetRestore := False;
  SetActionTimer(True);
  end
else
  begin
    FitPending :=True
  end
end;

function TCameraActionRequestor.RequestRunJob(FastMode : Boolean): Boolean;
begin
Result := True;
if InShutDown then exit;
If FullResetInProgress then
  begin
  exit;
  end;
//If (RunJobPending) or RunJobInProgress then
If  RunJobInProgress then
  begin
  If assigned(FOnOverrunError) then FOnOverrunError(Self,'Overrun on running job');
  exit;
  end;
if CanStart  then
  begin
  RunJobInProgress := True;
  RunJobPending := False;
  if FastMode then
    begin
    ActionStage := tcaStartFastRunJob
    end
  else
    begin
    ActionStage := tcaStartRunJob;
    end;
  CogNexCommunicator.ExternalLockBusy := True;
  SetActionTimer(True);
  end
else
  begin
    RunJobPending :=True
  end

end;


procedure TCameraActionRequestor.RequestScreenUpdate;
begin
ScreenUpdatePending := True;
end;


function TCameraActionRequestor.RequestCheckJobPass(InLineError : Boolean): Boolean;
begin
Result := True;
WasInLineError := InLineError;
if InShutDown then exit;
If FullResetInProgress then
  begin
  exit;
  end;
If (RunJobPending) or RunJobInProgress then
  begin
  CheckJobPassPending := True;
  exit;
  end;
if CanStart  then
  begin
  CheckJobPassInProgress := True;
  CheckJobPassPending := False;
  ActionStage := tcaStartCheckJobPass;
  CogNexCommunicator.ExternalLockBusy := True;
  SetActionTimer(True);
  end
else
  begin
   CheckJobPassPending := True;
  end
end;

function TCameraActionRequestor.RequestFTPInitialConnectandLogin: Boolean;
begin
Result := True;
if InShutDown then exit;
If FTPLoggedIn then exit;
if CanStart  then
  begin
  FIsIdle := False;
  FTPLoginInProgress := True;
  ActionStage := tcaFTPLoginStart;
  SetActionTimer(True);
  end
else
  begin
  FTPLoginPending := True;
  end;

end;

function TCameraActionRequestor.RequestCameraConnectandLogin: Boolean;
begin
Result := True;
if InShutDown then exit;
If FTPLoggedIn then exit;
if CanStart  then
  begin
  FIsIdle := False;
  FTPLoginInProgress := True;
//  ActionStage := tcaFTPLoginStart;
  ActionStage := tcaInitialLoginStart;
  SetActionTimer(True);
  end
else
  begin
  FTPLoginPending := True;
  end;

end;

function TCameraActionRequestor.RequestSaveSnapshot(FPath: String;WithGraphics : Boolean;CurrentSceneScale : Double):Boolean;
begin
Result := True;
if InShutDown then exit;
SnapShotFilePath := FPath;
SnapWithGraphics := WithGraphics;
SnapScale := CurrentSceneScale;
if CanStart  then
  begin
  FIsIdle := False;
  SnapShotInProgress := True;
  ActionStage := tcaSnapShotStart;
  SetActionTimer(True);
  end
else
  begin
  SnapshotPending := True;
  end;
end;


function TCameraActionRequestor.RequestGetValue(ValueName : String): Boolean;
begin
Result := True;
InsipientGetValueName := ValueName;
if CanStart  then
  begin
  FIsIdle := False;
  GetValueInProgress := True;
  ActionStage := tcaGetValueStart;
  SetActionTimer(True);
  end
else
  begin
  GetValuePending := True;

  end;

end;

function TCameraActionRequestor.RequestSetValue(ValueName : String;ValueType : TVisValTypes): Boolean;
begin
InsipientSetValueName := ValueName;
InsipientSetValueType := ValueType;
if CanStart  then
  begin
  FIsIdle := False;
  SetValueInProgress := True;
  ActionStage := tcaSetValueStart;
  SetActionTimer(True);
  end
else
  begin
  SetValuePending := True;
  end;

end;


function TCameraActionRequestor.RequestSaveTelnetLog: Boolean;
begin
Result := True;
if CanStart  then
  begin
  FIsIdle := False;
  TelnetSaveProgress := True;
  ActionStage := tcaTelnetSaveStart;
  SetActionTimer(True);
  end
else
  begin
  TelnetSavePending := True;
  end;
end;

function TCameraActionRequestor.RequestImportFile(FPath: String): Boolean;
begin
Result := True;
if InShutDown then exit;
If FTPLoggedIn then exit;
ImportFilePath := FPath;
if CanStart  then
  begin
  FIsIdle := False;
  FTPImportInProgress := True;
  ActionStage := tcaImportFileStart;
  SetActionTimer(True);
  end
else
  begin
  FTPImportPending := True;
  end;
end;


destructor TCameraActionRequestor.Destroy;
begin
  inherited;
end;

procedure TCameraActionRequestor.ClearDown;
begin
  ActionTimer.Enabled := False;
  ActionStage := tcaIdlle;
  FullResetPending := False;
  FullResetInProgress := False;
  FullResetInProgress := False;
  XHairEstablishPending := False;
  XHairEstablishInProgress := False;
  BufferToXhairPending := False;
  BufferToXhairInProgress := False;
  FitPending := False;
  ScreenUpdatePending := False;
  FitInProgress := False;
  RestorePending := False;
  RestoreInProgress := False;
  JobLoadPending := False;
  JobLoadInProgress := False;
  DisconnectInProgress := False;
  DisconnectPending := False;
  ReconnectPending  := False;
  ReconnectInProgress := False;
  IsReconnectReset := False;
  RunJobPending := False;
  RunJobInProgress := False;
  CheckJobPassPending := False;
  CheckJobPassInProgress := False;
  SnapShotInProgress  := False;
  TelnetSaveProgress := False;
  FTPLoginInProgress := False;
  FTPLoginPending    := False;
  aaIsIdle := True;
end;

procedure TCameraActionRequestor.DoNothing(Sender: TObject);
begin
ActionTimer.Enabled := False;
{$IFDEF HIGHDEBUG}
AddDebug('Do Nothing!!!!',tvmDebug);
{$ENDIF}

Restart := False;
end;


procedure TCameraActionRequestor.aaCameraAction(Sender: TObject);
var
CommandString : String;
Done : Boolean;
RetryCount : Integer;
NewPath,TestPath : String;
TelnetPath : String;

function StripFileExtension(InPath : String): String;
var
FileExt : String;
OrgLen : INteger;
begin
Result := InPath;
FileExt := ExtractFileExt(InPath);
if FileExt <> '' then
  begin
  OrgLen := Length(InPath);
  Result := Copy(Result,1,OrgLen-Length(FileExt));
  end;
end;
begin
ActionTimer.Enabled := False;
Restart := False;
try
  case ActionStage of
    tcaIdlle:
     begin
     {$IFDEF HIGHDEBUG}
     AddDebug('AS Idle',tvActionStageOnly);
     {$ENDIF}
     end;

    tcaWait:
     begin
     AddDebug('tcaWait',tvActionStageOnly);
     {$IFDEF HIGHDEBUG}
     AddDebug('tcaWait',tvActionStageOnly);
     {$ENDIF}
     end;

    tcaInitialLoginStart:
     begin
     {$IFDEF HIGHDEBUG}
     AddDebug('tcaInitialLoginStart',tvActionStageOnly);
     {$ENDIF}
     try
     AddDebug(Format('Sending Connect Message %s',[ConnectString]),tvmDebug);
     GlobalCognexForm.StartConnectTime := Now;
     CognexDisplay.AutoConnectString := ConnectString;
     ActionStage := tcaIdlle;
     except
     ActionStage := tcaIdlle;
     end;

     end;

    tcaInitialLoginDone:
     begin
     {$IFDEF HIGHDEBUG}
     AddDebug('tcaInitialLoginDone',tvActionStageOnly);
     {$ENDIF}
     FCheckResetTime := True;
     if DisconectedFlag then
      begin
      PostFirstLoginReset := True;
      DisconectedFlag := False;
      end
     else
      begin
      PostFirstLoginReset := False;
      end;
     CogNexCommunicator.CheckIsTalking(tmAtLogin);
     AwaitAction;
     end;

    tcaOffLineForJobLoad:
      begin
     {$IFDEF HIGHDEBUG}
      AddDebug('tcaOffLineForJobLoad',tvActionStageOnly);
      {$ENDIF}
      AwaitAction;  // must be before Update
      JobLoadPending := False;
      JobLoadInProgress := True;
      CogNexCommunicator.ExternalLockBusy := True;
      CommandString := Uppercase(LoadJobName);
      if LatchLoadJob then
        begin
        LatchJobTag.AsInteger := 1
        end
      else
        begin
        LatchJobTag.AsInteger := 0
        end;
      CogNexCommunicator.JobNameToBeLoaded := CommandString;
      CogNexCommunicator.SendNCCommand('SO0',tncOffLineForJobLoad,'go Offline after Run Job',False);
      CogNexCommunicator.PostCommandsPending := True;
      end;

    tcaLoadJobStart:
      begin
     {$IFDEF HIGHDEBUG}
     AddDebug('tcaLoadJobStart',tvActionStageOnly);
     {$ENDIF}
     JobLoadPending := False;
     JobLoadInProgress := True;
     CogNexCommunicator.ExternalLockBusy := True;
     CommandString := Uppercase(LoadJobName);
      if LatchLoadJob then
        begin
        LatchJobTag.AsInteger := 1
        end
      else
        begin
        LatchJobTag.AsInteger := 0
        end;
      CogNexCommunicator.JobNameToBeLoaded := CommandString;
      CognexCommunicator.SendNCCommand(Format('LF%S' ,[CommandString]),tncJobLoadStart,'Load job',True);
      CogNexCommunicator.PostCommandsPending := True;
      AwaitAction;
      end;


    tcaLoadJobDone:
      begin
     {$IFDEF HIGHDEBUG}
      AddDebug('tcaLoadJobDone',tvActionStageOnly);
     {$ENDIF}
      JobLoadPending := False;
      JobLoadInProgress := False;
      CogNexCommunicator.ExternalLockBusy := False;
      CogNexCommunicator.PostCommandsPending := False;
      EndAction;
      end;

    tcaLoadJobFailed:
      begin
     {$IFDEF HIGHDEBUG}
     AddDebug('tcaLoadJobFailed',tvActionStageOnly);
     {$ENDIF}
      JobLoadPending := False;
      JobLoadInProgress := False;
      CogNexCommunicator.ExternalLockBusy := False;
      CogNexCommunicator.PostCommandsPending := False;
      EndAction;
      end;


(*     tcaStartBufferXHair  :
      begin
      AddDebug('Buffer XHair Start',tvmDebug);
      {$IFDEF HIGHDEBUG}
      AddDebug('tcaStartBufferXHair',tvActionStageOnly);
      AddDebug('Buffer XHair Start',tvmDebug);
      {$ENDIF}
      CogNexCommunicator.ExternalLockBusy := True;
      If DefaultJobActive then
         begin
        {$IFDEF HIGHDEBUG}
        AddDebug('Croshair Already loaded go straight to restore',tvmDebug);
        {$ENDIF}
         EndWaitForXHairLoadonBuffer(True);
         end
       else
         begin
         CommandString := Uppercase(DefaultJobName);
         if POS('.JOB',CommandString) = 0  then
            begin
            CommandString := Format('LF%s.JOB',[CommandString]);
            end
          else
            begin
            CommandString := Format('LF%s',[CommandString]);
            end;
          CogNexCommunicator.SendNCCommand(CommandString,tncLoadDefaultOnXHairBuffer,'load default job on buffer show X hair',False);
         {$IFDEF HIGHDEBUG}
          AddDebug('Reloading default job on Buffer',tvmDebug);
         {$ENDIF}
          AwaitAction;
          end;
        end;

   tcaBufferXHairRestore:
       begin
       AwaitAction;
       {$IFDEF HIGHDEBUG}
       AddDebug('tcaBufferXHairRestore',tvActionStageOnly);
       {$ENDIF}
       CogNexCommunicator.SendNCCommand(Format('SFAcquisition.Exposure_Time %5.3f',[BufferNewExposure]),tncStartRestoreCameraForBuffer,'restore exposure on buffer X hair',False);
       AddDebug('tcaBufferXHairRestore Stage in Progress',tvmDebug);
       end;

   tcaBufferXHairEnd:
     begin
     {$IFDEF HIGHDEBUG}
     AddDebug('tcaBufferXHairEnd',tvActionStageOnly);
     AddDebug(Format ('BufferFit %5.3f %5.3f %d',[BufferNewScale,BufferNewExposure,BufferNewCentreOnXHair]),tvmDebug);
     {$ENDIF}
     ChangeLoadedJobImageScale(BufferNewScale,BufferNewCentreOnXHair);
     BufferToXhairInProgress := False;
     BufferToXhairPending := False;
     XHairBufferedTag.AsInteger := 1;
     EndAction;
     end;*)

    tcaStartFullReset:
      begin
     {$IFDEF HIGHDEBUG}
      AddDebug('tcaStartFullReset',tvActionStageOnly);
      AddDebug('Starting Full Reset',tvmDebug);
     {$ENDIF}
      ResetStartTime := Now;
      CogNexCommunicator.ExternalLockBusy := True;
      CogNexCommunicator.CheckIsTalking(tmGeneralReset);
      IsResetFit := True;
      FullResetInProgress := True;
      FullResetPending := False;
      AwaitAction;
      end;

    tcaResetExerciseS02:
      begin
     {$IFDEF HIGHDEBUG}
      AddDebug('tcaResetSetOffline',tvActionStageOnly);
      // here when established Talking, Check Which Job Loaded
     {$ENDIF}
      ResetTimeout.Enabled := True;
      CogNexCommunicator.SendNCCommand('SO2',tncResetExerciseS02,'go offline on reset',False);
      AddDebug('Setting Offline In Reset',tvmDebug);
      AwaitAction;
      end;


    tcaResetSetOffline:
      begin
     {$IFDEF HIGHDEBUG}
      AddDebug('tcaResetSetOffline',tvActionStageOnly);
      // here when established Talking, Check Which Job Loaded
     {$ENDIF}
      ResetTimeout.Enabled := True;
      OffLineRetry := 0;
      CogNexCommunicator.SendNCCommand('SO0',tncSetOfflineOnReset,'go offline on reset',False);
      AddDebug('Setting Offline In Reset',tvmDebug);
      AwaitAction;
      end;

     tcaReset0:
       begin
     {$IFDEF HIGHDEBUG}
      AddDebug('tcaReset0',tvActionStageOnly);
      // here when established Talking, Check Which Job Loaded
     {$ENDIF}
      ResetStartTime := Now;
      ResetTimeout.Enabled := True;
      If LatchJob then
        begin
       {$IFDEF HIGHDEBUG}
        AddDebug('Straight to stage 4',tvmDebug);
       {$ENDIF}
        ActionStage := tcaReset3;
        Restart := True;
        end
      else
        begin
        ActionStage := tcaReset1;
        {$IFDEF HIGHDEBUG}
        AddDebug('Already Offline Job Not Latched Straight to stage 1',tvmDebug);
        {$ENDIF}
        Restart := True;
        end;
       end;

     tcaReset1:
       begin
       {$IFDEF HIGHDEBUG}
        AddDebug('tcaReset1',tvActionStageOnly);
       {$ENDIF}
       if LatchJob = False  then
          begin
          if IsReconnectReset then
             begin
              CognexDisplay.Align := alClient;
              ActionStage := tcaReset2;
              Restart := True;
              exit;
             end;
          If DefaultJobActive and (Not FCurrentJobIsBuffered) then
            begin
            if ForceFullReset or (Not ScreenInResetMode) or (XHairBufferedTag.AsInteger = 1) or (DisableXhairButtonTag.AsInteger = 0) then
              begin
              if DisableXhairButtonTag.AsInteger = 0 then ForceFullReset := True;
              IsResetFit  := True;
              XHairBufferedTag.AsInteger := 0;
              CognexDisplay.Align := alClient;
              ActionStage := tcaReset2;
              Restart := True;
              end
            else
              begin
              ActionStage := tcaReset3;
              Restart := True;
              end;
            end
          else
            begin
            FCurrentJobIsBuffered := False;
            CommandString := Uppercase(DefaultJobName);
              if POS('.JOB',CommandString) = 0  then
                begin
                CommandString := Format('LF%s.JOB',[CommandString]);
                end
              else
                begin
                CommandString := Format('LF%s',[CommandString]);
                end;
             CogNexCommunicator.SendNCCommand(CommandString,tncLoadDefaultOnReset,'load default program on reset',False);
             {$IFDEF HIGHDEBUG}
             AddDebug('Reloading default job',tvmDebug);
             {$ENDIF}
             AwaitAction;
            end;
          end
        else
          begin
          IsResetRestore := True;
          ActionStage := tcaRestoreStart;
          Restart := True;
          end;
       end;

     tcaReset2:
       begin
       {$IFDEF HIGHDEBUG}
       AddDebug('tcaReset2',tvActionStageOnly);
       {$ENDIF}
       IsResetRestore := True;
       ActionStage := tcaRestoreStart;
       Restart := True;
       end;

     tcaReset3:
       begin
       {$IFDEF HIGHDEBUG}
        AddDebug('tcaReset3',tvActionStageOnly);
       {$ENDIF}
        if (LatchJob = False) then
          begin
          CogNexCommunicator.SendNCCommand('Put Live 1',tncGoLiveOnReset,'go live on reset',False);
          AwaitAction;
          end
        else
          begin
         {$IFDEF HIGHDEBUG}
          AddDebug('Job Latched Reset Done',tvmDebug);
         {$ENDIF}
          FullResetInProgress :=  False;
          FullResetPending := False;
          ResetTimeout.Enabled := False;
          if assigned(FOnResetComplete) Then FOnResetComplete(Self);
          IsReconnectReset := False;
          IsDisconReconReset := False;
          EndAction;
        end;
       end;

     tcaResetEnd:
       begin
       {$IFDEF HIGHDEBUG}
       AddDebug('tcaResetEnd',tvActionStageOnly);
       {$ENDIF}
//       ScaleAndFitAreTemporary := False;
       DisableXhairButtonTag.AsInteger := 1;
       ResetTimeout.Enabled := False;
       ResetTime := Now-ResetStartTime;
       ResetTime := ResetTime*24*60*60*1000;
       if XHairEstablishInProgress then
         begin
         ActionStage := tcaXhairEstablishEnd;
         Restart := True;
         end
       else if IsReconnectReset or IsDisconReconReset then
         begin
         IsReconnectReset := False;
         {$IFDEF HIGHDEBUG}                                                //
           AddDebug('Reconnect Reset Done',tvmDebug);
         {$ENDIF}
         AddDebug(Format(' Recon Reset took %5.3f ms',[ResetTime]),tvmDebug);
         //Here for PU retries of disconrecon
         if FCheckResetTime  then
            begin
            if PostFirstLoginReset then AddDebug('PostLoginReset TRUE for Powwer Up1',tvmDebug);
            If (ResetTime > FMaxResetTime) or PostFirstLoginReset then
              begin
              FCheckResetTime := False;
              PostFirstLoginReset := False;
              ActionStage := tcaStartDiscOnRecon;
              Restart := True;
              exit;
              end;
            FCheckResetTime := False;
            end
         else
            begin
            if IsDisconReconReset then
              begin
              AddDebug('-----------------------------------',tvmDebug);
              AddDebug('Cleared Down DisCon Rcon Mode',tvmDebug);
              InDisConRecon := False;
              AddDebug('IsDisconReconReset set False 33333',tvmDebug);
              IsDisconReconReset := False;
              ForceDisconRecon := False;
              if assigned(FonTestedPOwerUp) then FonTestedPOwerUp(Self)
              end;
            end;
         AddDebug('-----------------------------------',tvmDebug);
         AddDebug('Cleared Prior to tcaReconnectDone',tvmDebug);
         FCurrentJobIsBuffered := False;
         FullResetInProgress :=  False;
         FullResetPending := False;
         FitPending := False;
         ForceDisconRecon := False;
         InDisConRecon := False;
         RestorePending := False;
         RunJobPending := False;
         RunJobInProgress := False;
         if ReconnectDone then
            begin
            AddDebug('IsSecondReset',tvmDebug);
            ReconnectDone := False;
            IsReadyTag.AsInteger := 1;
            EndAction;
            end
         else
            begin
            AddDebug('Is NOT SecondReset',tvmDebug);
            ActionStage := tcaReconnectDone;
            Restart := True;
            end;
         end
       else
         begin
       {$IFDEF HIGHDEBUG}
         AddDebug('Reset Finished',tvmDebug);
       {$ENDIF}
         AddDebug(Format(' Reset took %5.3f ms',[ResetTime]),tvmDebug);

         if assigned(OnResetTimeAvailable) then
           begin
           FOnResetTimeAvailable(Self,ResetTime);
           end;
         FCurrentJobIsBuffered := False;
         FullResetInProgress :=  False;
         FullResetPending := False;
         FitPending := False;
         RestorePending := False;
         RunJobPending := False;
         RunJobInProgress := False;
         if assigned(FOnResetComplete) Then FOnResetComplete(Self);
         if FForceDisconRecon then
            begin
            FForceDisconRecon := False;
            FCheckResetTime := False;
            ActionStage := tcaStartDiscOnRecon;
            Restart := True;
            end
         else If CheckResetTime and not(IsDisconReconReset) then
            begin
            FCheckResetTime := False;
            If ResetTime > FMaxResetTime then
              begin
              ActionStage := tcaStartDiscOnRecon;
              Restart := True;
              end
            end
         else
            begin
            IsReadyTag.AsInteger := 1;
            EndAction;
            end;
         end
       end;

     tcaStartDiscOnRecon:
      begin
      {$IFDEF HIGHDEBUG}
      AddDebug('tcaStartDiscOnRecon',tvActionStageOnly);
      {$ENDIF}
      InDisConRecon := True;
//      AddDebug('**************************************',tvmDebug);
//      AddDebug('InDisConRecon Set True 1111',tvmDebug);
//      AddDebug('**************************************',tvmDebug);
      IsDisconReconReset := True;
//      AddDebug('IsDisconReconReset Set True 1',tvmDebug);
      ActionStage := tcaDisconnectStart;
      CogNexCommunicator.ExternalLockBusy := True;
      FInDisconnect := True;
      SetActionTimer(True);
      end;

     tcaRestoreCheckStart:
       begin
       {$IFDEF HIGHDEBUG}
        AddDebug('tcaRestoreCheckStart',tvActionStageOnly);
       {$ENDIF}
       CogNexCommunicator.CheckIsTalking(tmRestore);
       AwaitAction;

       end;

     tcaRestoreStart:
       begin
       {$IFDEF HIGHDEBUG}
       AddDebug('tcaRestoreStart',tvActionStageOnly);
       {$ENDIF}
       if IsResetRestore then
       {$IFDEF HIGHDEBUG}
         AddDebug('In Reset Restore Start',tvmDebug)
       {$ENDIF}
       else
         begin
         RestoreInProgress := True;
         end;
       if ForceFullReset then
          begin
         {$IFDEF HIGHDEBUG}
          AddDebug('Doing forced Full reset',tvmDebug);
         {$ENDIF}
          ForceFullReset := False;
          if assigned(FonInitialResetComplete) then FonInitialResetComplete(Self);
          end;
       RestorePending := False;
//       CognexDisplay.ImageScale := BufferCameraState.Scale;
//       CurrentScale := CognexDisplay.ImageScale;
//       SceneScaleTag.AsDouble := CognexDisplay.ImageScale;
       CogNexCommunicator.ExternalLockBusy := True;
       If DefaultJobActive then
         begin
        {$IFDEF HIGHDEBUG}
         AddDebug('Restore--NamesMatch--Setting circle radius',tvmDebug);
        {$ENDIF}
         CogNexCommunicator.JobScaling := BufferCameraState.JobScaling;
         CogNexCommunicator.JobXOffset := BufferCameraState.Xoffset;
         CogNexCommunicator.JobYOffset := BufferCameraState.Yoffset;
         ScalingTag.AsDouble := BufferCameraState.JobScaling;
         XoffsetTag.AsDouble := BufferCameraState.XOffset;
         YoffsetTag.AsDouble := BufferCameraState.YOffset;
         CogNexCommunicator.SendNCCommand(Format('SFCircleRadius.Value %7.3f',[BufferCameraState.CircleRadius]),tncStartGeneralRestore,'set Cirle Radius in Buffer Mode',False);
        {$IFDEF HIGHDEBUG}
         AddDebug('In General Restore Start',tvmDebug);
         AddDebug('Restoring Camera State',tvmDebug);
         AddDebug(Format('Restoring Scale To %5.3f',[BufferCameraState.Scale]),tvmDebug);
        {$ENDIF}
         AwaitAction;
         end
       else
          begin
          ActionStage := tcaRestoreDone;
          Restart := True;
          end
         end;

       tcaRestoreDone:
         begin
        {$IFDEF HIGHDEBUG}
         AddDebug('tcaRestoreDone',tvActionStageOnly);
        {$ENDIF}
         RestoreInProgress := False;
//         ScaleAndFitAreTemporary := False;
         RestorePending := False;
         ActionStage := tcaStartFit;
         Restart := True;
       end;

    tcaStartFit:
      begin
       {$IFDEF HIGHDEBUG}
       AddDebug('tcaStartFit',tvActionStageOnly);
       {$ENDIF}
       if IsResetRestore then
         begin
        {$IFDEF HIGHDEBUG}
         AddDebug('In Reset Restore Start',tvmDebug);
        {$ENDIF}
         end
       else
         begin
        {$IFDEF HIGHDEBUG}
         AddDebug('In General Restore Start',tvmDebug);
        {$ENDIF}
         end;
      FitPending := False;
      if IsResetFit then
        begin
        IsResetFit := False;
        CurrentCameraState.FitMode := ResetFitMode;
        CurrentCameraState.CentreModeInteger := ResetXHMode;
        CurrentCameraState.Scale := ResetImageScale;
        BufferCameraState.Scale := ResetImageScale;
        BufferCameraState.FitMode := ResetFitMode;
        BufferCameraState.CentreModeInteger := ResetXHMode;
        If ResetXHMode = 1 then
          begin
          CurrentCameraState.CentreMode := tcmXHair;
          BufferCameraState.CentreMode := tcmXHair;
          end
        else
          begin
          CurrentCameraState.CentreMode := tcmScreen;
          BufferCameraState.CentreMode := tcmScreen;
          end
        end;

      if (CurrentCameraState.FitMode = 1) then
         begin
        {$IFDEF HIGHDEBUG}
         AddDebug('Restore--Fit Mode =1',tvmDebug);
        {$ENDIF}
         CurrentCameraState.CentreMode := tcmScreen;
         CurrentCameraState.CentreModeInteger := 0;
         ProcessFitChanged;
         end
       else
         begin
        {$IFDEF HIGHDEBUG}
         AddDebug('Restore--Fit Mode = 0',tvmDebug);
        {$ENDIF}
         IsFullFOVTag.AsInteger := 0;
         CognexDisplay.ImageScale := CurrentCameraState.Scale;
         if BufferCameraState.CentreModeInteger = 0 then
           begin
           AddDebug('Restore--CentreMode = 0',tvmDebug);
           CurrentCameraState.CentreMode := tcmScreen;
           CurrentCameraState.CentreModeInteger := 0;
           end
         else
           begin
          {$IFDEF HIGHDEBUG}
           AddDebug('Restore--CentreMode = 1',tvmDebug);
          {$ENDIF}
           CurrentCameraState.CentreMode := tcmXHair;
           CurrentCameraState.CentreModeInteger := 1;
           end;
         SceneScaleTag.AsDouble := CognexDisplay.ImageScale;
         DefaultJobImageScaleChanged(nil);
         end;

       If FDefaultJobActive then
         begin
         If BufferCameraState.CircleOn <> 0 then
           begin
           CogNexCommunicator.SendNCCommand('SICIRCLE_1.TOOL_ENABLED 1',tncLastFitCommand,'enable circle in fit mode',False);
           end
         else
           begin
           CogNexCommunicator.SendNCCommand('SICIRCLE_1.TOOL_ENABLED 0',tncLastFitCommand,'disable circle in fit mode',False);
           end;
          AwaitAction;
          end
         else
           begin
           ActionStage := tcaFitDone;
           Restart := True;
           end

      end;

    tcaFitDone:
      begin
      If assigned(FOnFitComplete) then FOnFitComplete(Self);
      {$IFDEF HIGHDEBUG}
      AddDebug('In Fit Done',tvmDebug);
      {$ENDIF}
      if not EnableCameraUpdate then
        begin
        EnableCameraUpdate := True;
        DoLazyUpdate(Nil);
        end;
      if FitInProgress then
        begin
        FitInProgress := False;
        FitPending := False;
        KickLazyUpdate;
        EndAction;
        {$IFDEF HIGHDEBUG}
        AddDebug('tcaFitDone',tvActionStageOnly);
        CognexDisplay.Visible := True;
       {$ENDIF}
        end
      else
        begin
        {$IFDEF HIGHDEBUG}
        AddDebug('Reset Fit Done, finishing Reset',tvActionStageOnly);
        {$ENDIF}
        ActionStage := tcaResetEnd;
        Restart := True;
        end

      end;

    tcaStartXhairEstablish:
      begin
      LatchJob := False;
      LatchJobTag.AsInteger := 0;
      ActionStage := tcaReset0;
      IsFitRestore := False;
      Restart := True;
      {$IFDEF HIGHDEBUG}
      AddDebug('tcaStartXhairEstablish',tvActionStageOnly);
      {$ENDIF}
      end;

    tcaXhairEstablishEnd:
      begin
      XHairEstablishInProgress := False;
      XHairEstablishPending := False;
      EndAction;
      {$IFDEF HIGHDEBUG}
      AddDebug('tcaXhairEstablishEnd',tvActionStageOnly);
      {$ENDIF}
      end;


    tcaDisconnectStart:
    begin
    if not CognexDisplay.Connected then
     begin
     ActionStage := tcaDisconnectDone;
     Restart := True;
     end
    else
      begin
      if OnlineTag.AsInteger = 1 then
        begin
        AwaitAction;
        if InDisConRecon then
          begin
          if Assigned(FOnBannerMessage) then FOnBannerMessage(Self,'Testing Connection');
          end
        else
          begin
          if Assigned(FOnBannerMessage) then FOnBannerMessage(Self,'Starting Disconnect');
          end;
        OfflineWaitTimer.Enabled := True;
        LatchJobTag.AsInteger := 0;
        CogNexCommunicator.SendNCCommand('SO0',tncSetOfflineOnDisconnect,'go offline on camera disconnect',False);
        LatchJobTag.AsInteger := 0;
        end
      else
        begin
        ActionStage := tcaTelnetDisconnect;
        Restart := True;
        end;
      end;
  {$IFDEF HIGHDEBUG}
  AddDebug('tcaDisconnectStart',tvActionStageOnly);
  {$ENDIF}
  end;


  tcaTelnetDisconnect:
  begin
    if CogNexCommunicator.IsConnected then
      begin
      AWaitAction;
      if InDisConRecon then
        begin
        if Assigned(FOnBannerMessage) then FOnBannerMessage(Self,'Testing Connection');
        end
      else
        begin
        if Assigned(FOnBannerMessage) then FOnBannerMessage(Self,'Disconnecting Telnet');
        end;
      CogNexCommunicator.GracefullDisconnect;
      end
    else
      begin
      ActionStage :=  tcaCameraDisconect;
      Restart := True;
      end;
  {$IFDEF HIGHDEBUG}
   AddDebug('tcaTelnetDisconnect',tvActionStageOnly);
  {$ENDIF}
  end;


  tcaCameraDisconect:
  begin
  {$IFDEF HIGHDEBUG}
  AddDebug('tcaCameraDisconect',tvActionStageOnly);
  {$ENDIF}

  if CognexDisplay.Connected then
    begin
    DisconnectCount := 0;
    AddDebug('Await on Disconnect',tvmDebug);
    AwaitAction;
    ActionStage :=  tcaDisconnectWait;
    if InDisconRecon then
      begin
      if Assigned(FOnBannerMessage) then FOnBannerMessage(Self,'Testing Camera');
      AddDebug('Requesting Disconnect At POwerup Test',tvmDebug);
      end
    else
      begin
      if Assigned(FOnBannerMessage) then FOnBannerMessage(Self,'Disconnecting Camera');
      AddDebug('Requesting Disconnect',tvmDebug);
      end;
    ControlledDisconnect := False;
    CognexDisplay.Disconnect;  // this causes immediate disconnect
    Restart := True;
    end
  else
    begin
      ActionStage :=  tcaDisconnectDone;
      Restart := True;
    end;
  end;

  tcaDisconnectWait:
    begin
    CognexDisplay.Disconnect;  // this causes immediate disconnect
    DisconnectCount := DisconnectCount+1;
    AddDebug(Format('tcaDisconnectWait Count %d',[DisconnectCount]),tvActionStageOnly);
    if ControlledDisconnect then ActionStage := tcaDisconnectDone;
    if (not CognexDisplay.Connected) and (DisconnectCount > 30) then
      begin
      DisconnectCount := 0;
      ActionStage :=  tcaDisconnectDone;
      Restart := True;
      end;
    Restart := True;
    end;

  tcaDisconnectDone:
  begin
    FLatchJob := False;
    ReconnectDone := False;
    if InDisConRecon then
      begin
      FInDisconnect := False;
      AddDebug('Reconnecting at Powerup',tvActionStageOnly);
      ActionStage := tcaReconnectStart;
      Restart:= True;
      end
    else
      begin
      EndAction;
      DisconnectCount := DisconnectCount+1;
      AddDebug(Format('tcaDisconnectDone Count %d',[DisconnectCount]),tvActionStageOnly);
      DisconnectInProgress := False;
      DisconnectPending := False;
      DisconectedFlag := True;

      if Assigned(FOnDisconectComplete) then FOnDisconectComplete(Self);
      if Assigned(FOnBannerMessage) then FOnBannerMessage(Self,'Camera Fully Disconnected');
      end;
   {$IFDEF HIGHDEBUG}
    AddDebug('tcaDisconnectDone',tvActionStageOnly);
   {$ENDIF}
  end;

  tcaReconnectStart:
  begin
  if not CognexDisplay.Connected then
    begin
    //!!!!!!!!!!!!!!""""""!!!!!!!!!!!!!
//    InDisConRecon := True;
    //!!!!!!!!!!!!!!""""""!!!!!!!!!!!!!
    if assigned(FOnReconnectStart) then FOnReconnectStart(Self);
    DefaultJobActive := False;
   {$IFDEF HIGHDEBUG}
    AddDebug('tcaReconnectStart',tvActionStageOnly);
   {$ENDIF}
    GlobalCognexForm.CameraConnectMode := tccmReconnectConnect;
    GlobalCognexForm.StartConnectTime := Now;
    CognexDisplay.AutoConnectString := ConnectString;
    AwaitAction;
    end
  else
    begin
    ActionStage := tcaReconnectDone;
    Restart := True;
    end;
  end;



  tcaReconnectGetCurrentJob:
  begin
   {$IFDEF HIGHDEBUG}
  AddDebug('tcaReconnectGetCurrentJob',tvActionStageOnly);
   {$ENDIF}
  CogNexCommunicator.CheckIsTalking(tmAtReconnect);
  AwaitAction;
  end;

  tcaValidateJobOnReconnect:
   begin
   {$IFDEF HIGHDEBUG}
     AddDebug('tcaValidateJobOnReconnect',tvActionStageOnly);
   {$ENDIF}
   end;

  tcaReconnectDone:
  begin
  {$IFDEF HIGHDEBUG}
  ReconnectDone := True;
  AddDebug('ReconnectDone',tvActionStageOnly);
  AddDebug('ReconnectDone',tvmDebug);
  {$ENDIF}
  ReconnectInProgress := False;
  ReconnectPending := False;
  if InDisConRecon then
    begin
    InDisConRecon := False;
    ActionStage := tcaReset0;
    FCheckResetTime := True;
    IsDisconReconReset := True;
    end;
  AddDebug('Calling On Reconect Complete',tvmDebug);
  if assigned(FOnReconnectComplete) then FOnReconnectComplete(Self);
  IsReadyTag.AsInteger := 1;
  EndAction;
  end;

  tcaReconnectError:
  begin
  {$IFDEF HIGHDEBUG}
  TidyDownResetCheck;
  AddDebug('tcaReconnectError',tvActionStageOnly);
  {$ENDIF}
  end;

  tcaStartFastRunJob:
  begin
  AwaitAction;
  CogNexCommunicator.FastRunMode := True;
  JobStartTime := Now;
  if CogNexCommunicator.FastRunModeFirstSweep then
    begin
    {$IFDEF HIGHDEBUG}
    AddDebug('tcaStartFastRunJobFirst',tvActionStageOnly);
    {$ENDIF}
    CogNexCommunicator.SendNCCommand('SO1',tncResetandRunJobFastFirst,'set first Online for Fast Run job',True);
    CogNexCommunicator.PostCommandsPending := True;
    end
  else
    begin
    {$IFDEF HIGHDEBUG}
    AddDebug('tcaStartFastRunJobFInMode',tvActionStageOnly);
    {$ENDIF}
    CogNexCommunicator.SendNCCommand('SW8',tncRunJobFastInMode,'set first Online for Fast Run job',True);
    CogNexCommunicator.PostCommandsPending := False;
    end
  end;

  tcaStartRunJob:
  begin
  {$IFDEF HIGHDEBUG}
  AddDebug('tcaStartRunJob',tvActionStageOnly);
  {$ENDIF}
  JobStartTime := Now;
  AwaitAction;
  CogNexCommunicator.FastRunMode := False;
  CogNexCommunicator.SendNCCommand('SO1',tncResetandRunJob,'set first Online for Run job',True);
  CogNexCommunicator.PostCommandsPending := True;
  end;

  tcaLiveAfterRun:
  begin
  {$IFDEF HIGHDEBUG}
  AddDebug('tcaLiveAfterRun',tvActionStageOnly);
  {$ENDIF}
  AwaitAction;
  IsFastRun := False;
  CogNexCommunicator.SendNCCommand('So0',tncOffLineAfterJobRun,'go Offline after Run Job',False);
  end;


  tcaCheckPassAfterFastRun:
  begin
  {$IFDEF HIGHDEBUG}
  AddDebug('tcaCheckPassAfterFastRun',tvActionStageOnly);
  {$ENDIF}
  AwaitAction;
  IsFastRun := True;
  CogNexCommunicator.SendNCCommand('GVJob.Pass_Count',tncCheckPassAfterFastRun,'getting Job Pass Count after Fast run',False);
  end;


  tcaStartCheckJobPass:
  begin
  {$IFDEF HIGHDEBUG}
  AddDebug('tcaStartCheckJobPass',tvActionStageOnly);
  {$ENDIF}
  AwaitAction;
  CogNexCommunicator.SendNCCommand('GVJob.Pass_Count',tncCheckJobPass,'getting job Pass count Normal Run Mode',True);
  end;


  tcaJobPassGoodResult:
  begin
  {$IFDEF HIGHDEBUG}
  AddDebug('tcaJobPassGoodResult',tvActionStageOnly);
  {$ENDIF}
  CheckJobPassPending := False;
  CheckJobPassInProgress := False;
  RunJobInProgress := False;
  RunJobPending := False;
  CogNexCommunicator.ExternalLockBusy := False;
  CogNexCommunicator.PostCommandsPending := False;
  if assigned(FonPassJobResult) then FonPassJobResult(Self,True,WasInLineError,IsFastRun,JobRunTime);
  EndAction;
  end;

  tcaJobPassBadResult:
  begin
  {$IFDEF HIGHDEBUG}
  AddDebug('tcaJobPassBadResult',tvActionStageOnly);
  {$ENDIF}
  CheckJobPassPending := False;
  CheckJobPassInProgress := False;
  RunJobInProgress := False;
  RunJobPending := False;
  CogNexCommunicator.ExternalLockBusy := False;
  CogNexCommunicator.PostCommandsPending := False;
  if assigned(FonPassJobResult) then FonPassJobResult(Self,False,WasInLineError,IsFastRun,JobRunTime);
  EndAction;
  end;


  tcaFTPLoginStart:
   begin
  {$IFDEF HIGHDEBUG}
   AddDebug('tcaFTPLoginStart',tvActionStageOnly);
  {$ENDIF}
    AwaitAction; // must be before Login
    if not FTPServer.RequestConnect then
        begin
        FTPLoginPending := False;
        FTPLoginInProgress := False;
        EndAction;
        end;
   end;

  tcaFTPLoginGetJobList:
   begin
  {$IFDEF HIGHDEBUG}
   AddDebug('tcaFTPLoginGetJobList',tvActionStageOnly);
  {$ENDIF}
    AwaitAction;  // must be before Update
    FTPServer.RequestInitialJobList;
   end;

  tcaFTPLoginDone:
    begin
   {$IFDEF HIGHDEBUG}
    AddDebug('tcaFTPLoginDone',tvActionStageOnly);
   {$ENDIF}
    FTPLoginPending := False;
    FTPLoginInProgress := False;
    EndAction;
    if assigned(FOnFTPLoginComplete) then FOnFTPLoginComplete(Self,True);
    end;



  tcaSnapShotStart:
    begin
   {$IFDEF HIGHDEBUG}
    AddDebug('tcaSnapShotStart',tvActionStageOnly);
   {$ENDIF}
    AwaitAction;
    SnapCheckCount := 10;
    SnapShotFilePath := StripFileExtension(SnapShotFilePath);
    NewPath := Format('%s.bmp',[SnapShotFilePath]);
    SnapShotInProgress := True;
    SnapshotPending := False;
    GlobalCognexForm.CDisplayBeginUpdate;
    try
    try
    GlobalCognexDisplay.ImageScale := 1.0;
    finally
    GlobalCognexForm.CDisplayEndUpdate;
    end;
    except
    GlobalCognexForm.CDisplayEndUpdateOnException;
    end;
    if FileExists(NewPath) then
       begin
       Done := False;
       RetryCount := 1;
       while (RetryCount < 11) and (not Done) do
          begin
          try
          NewPath := Format('%s_%d',[SnapShotFilePath,RetryCount]);
          TestPath := Format('%s.bmp',[NewPath]);
          Done := Not FileExists(TestPath);
          finally
          inc(RetryCount);
          end;
          end;
       If Done then
        begin
        SnapShotFilePath := TestPath;
        end
       else
        begin
        // HERE for error
        end
        end
    else
        begin
        SnapShotFilePath := NewPath;
        end;

    try
    GlobalCognexForm.CDisplayBeginUpdate;
    try
    try
    if SnapWithGraphics then
      begin
      GlobalCognexDisplay.ShowGraphics := True;
      GlobalCognexDisplay.SaveBitmap(SnapShotFilePath);
      end
    else
      begin
      GlobalCognexDisplay.ShowGraphics := False;
      GlobalCognexDisplay.SaveBitmap(SnapShotFilePath);
      GlobalCognexDisplay.ImageScale := SnapScale;
      GlobalCognexDisplay.ShowGraphics := True;
      end;
//  AddDebug(Format('Snapshot Saved To %s',[SnapShotFilePath]),tvmDebug);
    GlobalCognexDisplay.ImageScale := SnapScale;
    finally
    GlobalCognexForm.CDisplayEndUpdate;
    end;
    except
    GlobalCognexForm.CDisplayEndUpdateOnException;
    end;

    ActionStage := tcaSnapShotCheckFile;
    SetActionTimer(True);
    except
    GlobalCognexDisplay.ImageScale := SnapScale;
    GlobalCognexDisplay.ShowGraphics := True;
    GlobalCognexForm.CDisplayEndUpdateOnException;
    ActionStage := tcaSnapShotFail;
    SetActionTimer(True);
//  AddDebug('Exception trying to save snapshot to following Path',tvmDebug);
//  AddDebug(Format('%s',[SnapShotFilePath]),tvmDebug);
//  SysObj.FaultInterface.SetFault(Self, CoreFaults[cfSysWarning], [Format('Exception trying to save snapshot to following Path: %s',[Path])], nil);
    end;
    end;

  tcaSnapShotCheckFile:
    begin
   {$IFDEF HIGHDEBUG}
    AddDebug('tcaSnapShotCheckFile',tvActionStageOnly);
   {$ENDIF}
   ActionTimer.Enabled := False;
    if SnapCheckCount > 0 then
      begin
      Dec(SnapCheckCount);
      if FileExists(SnapShotFilePath) then
        begin
        try
        try
        SnapHandle := FileOPen(SnapShotFilePath,2);
        SnapFileSize := GetFileSize(SnapHandle,0)
        finally
        FileClose(SnapHandle);
        end;
        If SnapFileSize > 10000 then
          begin
          AddDebug(Format('File Exists with retry Count of %d',[SnapCheckCount]),tvmDebug);
          ActionStage := tcaSnapshotDone;
          SetActionTimer(True);
          end
        except
        AddDebug('Exception getting File Size',tvmDebug);
        SetActionTimer(True);
        end;
        end
      end
    else
      begin
      ActionStage := tcaSnapShotFail;
      SetActionTimer(True);
      end;
    end;

  tcaSnapshotDone:
    begin
   {$IFDEF HIGHDEBUG}
    AddDebug('tcaSnapshotDone',tvActionStageOnly);
   {$ENDIF}
    EndAction;
    SnapShotInProgress := False;
    SnapshotPending := False;
    if assigned(FOnSnapDone) then FOnSnapDone(Self,True);
    end;

  tcaSnapshotFail:
    begin
   {$IFDEF HIGHDEBUG}
    AddDebug('tcaSnapshotFail',tvActionStageOnly);
   {$ENDIF}
    EndAction;
    SnapShotInProgress := False;
    SnapshotPending := False;
    if assigned(FOnSnapDone) then FOnSnapDone(Self,False);
    end;

  tcaTelnetSaveStart:
    begin
   {$IFDEF HIGHDEBUG}
//     AwaitAction;
     TelnetSavePending := False;
     TelnetSaveProgress := True;
     AddDebug('tcaTelnetSaveStart',tvActionStageOnly);
     TelnetPath := LocalPath+'ScriptOut\';
     TelnetCheckCount := 10;
     try
     TelnetFilePath := Format('%STelnetLog_%d.txt',[TelnetPath,DateTimeToFileDate(Now)]);
     TelnetMemo.RollingBuffer.Lines.SaveToFile(TelnetFilePath);
     ActionStage := tcaTelnetSaveCheckFile;
     except
     ActionStage := tcaTelnetSaveFail;
     end;
     SetActionTimer(True);
   {$ENDIF}
    end;

  tcaTelnetSaveCheckFile:
    begin
   {$IFDEF HIGHDEBUG}
    AddDebug('tcaTelnetSaveCheckFile',tvActionStageOnly);
   {$ENDIF}
    ActionTimer.Enabled := False;
    if TelnetCheckCount > 0 then
      begin
      Dec(TelnetCheckCount);
      if FileExists(TelnetFilePath) then
        begin
        ActionStage := tcaTelnetSaveDone;
        end
      end
    else
      begin
      ActionStage := tcaTelnetSaveFail;
      end;
    SetActionTimer(True);
    end;

  tcaTelnetSaveDone:
    begin
   {$IFDEF HIGHDEBUG}
    AddDebug('tcaTelnetSaveCheckFile',tvActionStageOnly);
   {$ENDIF}
    EndAction;
    TelnetSaveProgress := False;
    TelnetSavePending := False;
    if assigned(FOnSnapDone) then FOnSnapDone(Self,True);
    end;

  tcaTelnetSaveFail:
    begin
   {$IFDEF HIGHDEBUG}
    AddDebug('tcaTelnetSaveCheckFile',tvActionStageOnly);
    EndAction;
    TelnetSaveProgress := False;
    TelnetSavePending := False;
    if assigned(FOnSnapDone) then FOnSnapDone(Self,False);
   {$ENDIF}
    end;


  tcaImportFileStart:
    begin
   {$IFDEF HIGHDEBUG}
    AddDebug('tcaImportFile',tvActionStageOnly);
   {$ENDIF}
    If FileExists(ImportFilePath) then
      begin
      AwaitAction;  // must be before Update
      FTPImportInProgress := True;
      FTPImportInProgress := False;
      CogNexCommunicator.SendNCCommand('SO1',tncOnLineForImport,'go Offline after Run Job',True);
      end
    else
      begin
      { TODO : handle this }
      end
    end;


   tcaonlineForImport:
     begin
   {$IFDEF HIGHDEBUG}
    AddDebug('tcaonlineForImport',tvActionStageOnly);
   {$ENDIF}
     If FileExists(ImportFilePath) then
       begin
       try
       AwaitAction;  // must be before Update
//        if FTPServer.ImportFileCalled(ImportFilePath,True) then
       except
        FTPImportInProgress := False;
        FTPImportPending := False;
        EndAction;
       end
       end

    else
       begin
       { TODO : handle this }
//       CogNexCommunicator.SendNCCommand('SO0',tncOffLineForImport,'go Offline after Run Job');
       end;
    end;

    tcaOfflineForImport:
      begin
     {$IFDEF HIGHDEBUG}
      AddDebug('tcaOfflineForImport',tvActionStageOnly);
      {$ENDIF}
      AwaitAction;  // must be before Update
      CogNexCommunicator.SendNCCommand('SO0',tncOffLineForImport,'go Offline after Run Job',False);
      end;

    tcaFileImported:
     begin
     FTPImportInProgress := False;
     FTPImportPending := False;
     CogNexCommunicator.ExternalLockBusy := False;
     EndAction;
     end;

    tcaGetValueStart:
     begin
   {$IFDEF HIGHDEBUG}
    AddDebug('tcaGetValueStart',tvActionStageOnly);
   {$ENDIF}
     GetValueInProgress := True;
     GetValuePending := False;
     CogNexCommunicator.ExternalLockBusy := True;
     CogNexCommunicator.SendNCCommand(InsipientGetValueName,tncGetSymbolicValue,'get symbolic value',True);
     end;

    tcaGetValueDone:
     begin
     {$IFDEF HIGHDEBUG}
     AddDebug('tcaGetValueDone',tvActionStageOnly);
     {$ENDIF}
     GetValueInProgress := False;
     CogNexCommunicator.ExternalLockBusy := False;
     if assigned(FOnGotValue) then FOnGotValue(Self,True,GotValue);
      AddDebug('tcaGetValueStartEndAction',tvmDebug);

     EndAction;
     end;

    tcaSetValueStart:
     begin
   {$IFDEF HIGHDEBUG}
    AddDebug('tcaSetValueStart',tvActionStageOnly);
   {$ENDIF}
     SetValueInProgress := True;
     SetValuePending := False;
     CogNexCommunicator.ExternalLockBusy := True;
     case InsipientSetValueType of
       tvvString: CogNexCommunicator.SendNCCommand(InsipientSetValueName,tNCSetSymbolicValueAsString,'set symbolic value as String',True);
       tvvFloat : CogNexCommunicator.SendNCCommand(InsipientSetValueName,tncSetSymbolicValueAsFloat,'set symbolic value as String',True);
       tvvInteger : CogNexCommunicator.SendNCCommand(InsipientSetValueName,tNCSetSymbolicValueAsInteger,'set symbolic value as String',True);
     end;
     end;

    tcaSetValueDone:
     begin
    {$IFDEF HIGHDEBUG}
     AddDebug('tcaSetValueDone',tvActionStageOnly);
    {$ENDIF}
     SetValueInProgress := False;
     CogNexCommunicator.ExternalLockBusy := False;
     EndAction;
     end;

  end; // case

finally
if Restart then
  begin
  Restart := False;
  SetActionTimer(True);
  end;
end;
end;


procedure TCameraActionRequestor.AwaitAction;
begin
SetActionTimer(False);
ActionTimer.OnTimer := DoNothing;
ActionStage := tcaWait;
WaitTimeout.Enabled := TimeoutEnabled;
aaIsIdle := False;
Restart := False;
end;

procedure TCameraActionRequestor.EndAction;
//var
//TempPos : INteger;
begin
SetActionTimer(False);
CogNexCommunicator.ResetTimeoutToDefault;
Restart := False;
aaIsIdle := True;
if CogNexCommunicator.PostCommandError then
  begin
  if assigned(FonPostCommandOverrun) then FonPostCommandOverrun(Self);
  CogNexCommunicator.PostCommandError := False;
  end;
if DisconnectPending then
  begin
  RequestDisconnect(True);
  end
else if ReconnectPending then
  begin
  RequestReconnect(True,False);
  end
else if FullResetPending then
    begin
    ActionStage := tcaStartFullReset;
    FullResetPending := False;
    FitPending :=False;
    RestorePending :=False;
    AddDebug('Performing Pending Reset',tvmDebug);
    SetActionTimer(True);
    end
else if BufferToXhairPending then
    begin
    RequestBufferToXHair(True);
    RestorePending := False;
    FitPending := False;
    end
else if RestorePending then
    begin
    RequestRestore(True);
    FitPending := False;
    end
else if FitPending then
    begin
    RequestFit(True);
    end
else if CheckJobPassPending then
    begin
    RequestCheckJobPass(WasInLineError);
    end
else if FTPLoginPending then
    begin
    RequestFTPInitialConnectandLogin;
    end
else if SnapshotPending then
    begin
    RequestSaveSnapshot(SnapShotFilePath,SnapWithGraphics,SnapScale);
    end
else if TelnetSavePending then
    begin
    RequestSaveTelnetLog();
    end
else if GetValuePending then
    begin
    GetValuePending := False;
    RequestGetValue(InsipientGetValueName);
    end
else if SetValuePending then
    begin
    SetValuePending := False;
    RequestSetValue(InsipientSetValueName,InsipientSetValueType);
    end
else
    begin
    AddDebug('Idle on End Action',tvmDebug );
    CogNexCommunicator.ExternalLockBusy := False;
//    if assigned(CogNexCommunicator.OnIdleBusyChange) then CogNexCommunicator.OnIdleBusyChange(Self,True,CogNexCommunicator.InLineErrorResponse,CogNexCommunicator.InLineErrorMessage);
    end;
end;

procedure TCameraActionRequestor.WaitTimedOut(Sender: TObject);
begin
WaitTimeout.Enabled := False;
end;

procedure TCameraActionRequestor.EndWaitInitialJobList(Success: Boolean);
begin
WaitTimeout.Enabled := False;
ActionStage := tcaInitialLoginStart;
SetActionTimer(True);
end;


procedure TCameraActionRequestor.EndWaitForInitialLogin(Success: Boolean);
begin
WaitTimeout.Enabled := False;
ActionStage := tcaInitialLoginDone;
SetActionTimer(True);
end;

procedure TCameraActionRequestor.EndWaitForReconnectLogin(Success: Boolean);
begin
WaitTimeout.Enabled := False;
ActionStage := tcaInitialLoginDone;
SetActionTimer(True);
end;

procedure TCameraActionRequestor.EndWaitForResetFit(Success: Boolean);
begin
AddDebug('EndWaitForResetFit',tvmDebug);
WaitTimeout.Enabled := False;
ActionStage := tcaFitDone;
SetActionTimer(True);
end;



procedure TCameraActionRequestor.EndWaitForJobRunComplete(Success: Boolean);
begin
WaitTimeout.Enabled := False;
FJobRunTime := Now-JobStartTime;
FJobRunTime := JobRunTime*24*60*60*1000;
AddDebug(Format('Time For Std Job Run = %5.4f',[JobRunTime]),tvmDebug);
ActionStage := tcaLiveAfterRun;
SetActionTimer(True);
end;

procedure TCameraActionRequestor.EndWaitForFastJobRunComplete(Success: Boolean);
begin
WaitTimeout.Enabled := False;
FJobRunTime := Now-JobStartTime;
FJobRunTime := JobRunTime*24*60*60*1000;
AddDebug(Format('Time For Fast Job Run = %5.4f',[JobRunTime]),tvmDebug);
ActionStage := tcaCheckPassAfterFastRun;
SetActionTimer(True);
end;


procedure TCameraActionRequestor.EndWaitForLiveAfterJobRun(Success: Boolean);
begin
WaitTimeout.Enabled := False;
ActionStage := tcaRunJobDone;
SetActionTimer(True);
end;

procedure TCameraActionRequestor.EndWaitForJobPassCheck(Success, Passed: Boolean);
begin
WaitTimeout.Enabled := False;
if Passed then
  ActionStage := tcaJobPassGoodResult
else
  ActionStage := tcaJobPassBadResult;
SetActionTimer(True);
end;

procedure TCameraActionRequestor.EndWaitForFTPLogin(Success: Boolean);
begin
WaitTimeout.Enabled := False;
if Success then
  ActionStage := tcaFTPLoginGetJobList
else
  ActionStage := tcaFTPLoginGetJobList;
SetActionTimer(True);
end;

procedure TCameraActionRequestor.EndWaitForInitialJobList(Success: Boolean);
begin
WaitTimeout.Enabled := False;
if Success then
  ActionStage := tcaFTPLoginDone
else
  ActionStage := tcaFTPLoginDone;
  SetActionTimer(True);
end;

procedure TCameraActionRequestor.EndwaitForFileImport(Success: Boolean);
begin
WaitTimeout.Enabled := False;
if Success then
  ActionStage := tcaOfflineForImport
else
{ TODO : handle this }
  ActionStage := TcaOfflineForImport;
  SetActionTimer(True);
end;

procedure TCameraActionRequestor.EndWaitForGetValue(Success : Boolean;Value: String);
begin
WaitTimeout.Enabled := False;
GotValue := Value;
if Success then
  ActionStage := tcaGetValueDone
else
  ActionStage := tcaGetValueDone;
  SetActionTimer(True);
end;

procedure TCameraActionRequestor.EndWaitForSetValue(Success : Boolean);
begin
WaitTimeout.Enabled := False;
if Success then
  ActionStage := tcaSetValueDone
else
  ActionStage := tcaSetValueDone;
  SetActionTimer(True);
end;

procedure TCameraActionRequestor.EndWaitForOnLineForFileImport(Success: Boolean);
begin
WaitTimeout.Enabled := False;
if Success then
  ActionStage := tcaOnLineForImport
else
{ TODO : handle this }
  ActionStage := tcaOnLineForImport;
  SetActionTimer(True);
end;

procedure TCameraActionRequestor.EndWaitForOffLineForFileImport(Success: Boolean);
begin
WaitTimeout.Enabled := False;
if Success then
  ActionStage := tcaFileImported
else
{ TODO : handle this }
  ActionStage := tcaFileImported;

  SetActionTimer(True);
end;

procedure TCameraActionRequestor.EndWaitForTalking(Success : Boolean;TalkMode : TTalkingModes;FIsonline :Boolean);
begin
WaitTimeout.Enabled := False;
UseSO2 := False;
if Success then
  begin
//  ForceFullReset := False;
  case TalkMode of
  tmRestore:
   begin
   AddDebug('Talking For Restore',tvmDebug);
   ActionStage := tcaRestoreStart;
   SetActionTimer(True);
   exit;
   end;

  tmAtLogin:
   begin
   AddDebug('Talking after Login',tvmDebug);
   ForceFullReset := True;
   UseSO2 := True;
   IsReconnectReset := True;
   end;

  tmAtReconnect:
   begin
   AddDebug('Talking after Reconnect',tvmDebug);
   LatchJob := False;
   ForceFullReset := True;
   UseSO2 := True;
   LatchJobTag.AsInteger := 0;
   IsReconnectReset := True;
   end;
  tmPowerOnReset : AddDebug('Talking after Login',tvmDebug);
  tmGeneralReset : AddDebug('Talking General Reset',tvmDebug);
  end;

 if UseSO2 then
  begin
  ActionStage := tcaResetExerciseS02;
  SetActionTimer(True);
  end
else
  begin
  if FIsonline then
    begin
    ActionStage := tcaResetSetOffline;
    end
  else
    begin
    ActionStage := tcaReset0;
    end;
  SetActionTimer(True);
  end;
  end
else
  begin
  // Here if Success = 0
{ TODO : put code here }
  end;
end;

procedure TCameraActionRequestor.EndWaitForOfflineOnReset(Success: Boolean);
begin
WaitTimeout.Enabled := False;
if Success then
  begin
  ActionStage := tcaReset0;
  SetActionTimer(True);
  end
else
  begin
{ TODO : put code here }
  end;
end;

procedure TCameraActionRequestor.EndWaitForOfflineRetry(Success: Boolean);
begin
WaitTimeout.Enabled := False;

end;


procedure TCameraActionRequestor.EndWaitForGoLiveOnreset(Success: Boolean);
begin
WaitTimeout.Enabled := False;
AddDebug('Done Go Live',tvmDebug);
AddDebug('RESettingResetInProgress',tvmDebug);
//CogNexCommunicator.ExternalLockBusy := False;
ActionStage := tcaResetEnd;
SetActionTimer(True);
end;

procedure TCameraActionRequestor.EndWaitForResetRestore(Success: Boolean);
begin
WaitTimeout.Enabled := False;
if Success then
  begin
  AddDebug('Reset XHair Restore Done',tvmDebug);
//  CogNexCommunicator.ExternalLockBusy := False;
  ActionStage := tcaRestoreDone;
  SetActionTimer(True);
  end
else
  begin
  AddDebug('Reset XHair Restore Failed',tvmDebug);
  CogNexCommunicator.ExternalLockBusy := False;
//  ActionStage := tcaRestoreDone;
  SetActionTimer(True);
  end
end;


(*procedure TCameraActionRequestor.EndWaitForResetRestoreOnBuffer(Success: Boolean);
begin
WaitTimeout.Enabled := False;
if Success then
  begin
  AddDebug('Reset XHair Restore Done For Buffer',tvmDebug);
//  CogNexCommunicator.ExternalLockBusy := False;
  ActionStage := tcaBufferXHairEnd;
  SetActionTimer(True);
  end
else
  begin
  AddDebug('Reset XHair Restore Failed For Buffer',tvmDebug);
//  CogNexCommunicator.ExternalLockBusy := False;
  ActionStage := tcaBufferXHairEnd;
  SetActionTimer(True);
  end

end;*)

procedure TCameraActionRequestor.EndWaitForOfflineOnDisconnect;
begin
WaitTimeout.Enabled := False;
AddDebug('Gone Offline For Disconnect',tvmDebug);
ActionStage := tcaTelnetDisconnect;
SetActionTimer(True);
end;

procedure TCameraActionRequestor.EndWaitForTelnetDisconnect(Success: Boolean);
begin
WaitTimeout.Enabled := False;
AddDebug('Telnet Disconnected',tvmDebug);
ActionStage := tcaCameraDisconect;
SetActionTimer(True);
end;

procedure TCameraActionRequestor.EndWaitForCameraDisconnect(Success: Boolean);
begin
WaitTimeout.Enabled := False;
AddDebug('Camera  Disconnected',tvmDebug);
ControlledDisconnect := True;
//ActionStage := tcaDisconnectDone;
SetActionTimer(True);
end;

(*procedure TCameraActionRequestor.EndWaitForXHairLoadonBuffer(Success: Boolean);
begin
WaitTimeout.Enabled := False;
if Success then
  begin
  AddDebug('Reestablished Xhair for Buffer',tvmDebug);
//  CogNexCommunicator.ExternalLockBusy := False;
  ActionStage := tcaBufferXHairRestore;
  SetActionTimer(True);
  end
else
  begin
  AddDebug('Reestablished Xhair for Buffer',tvmDebug);
//  CogNexCommunicator.ExternalLockBusy := False;
  ActionStage := tcaBufferXHairEnd;
  SetActionTimer(True);
  end
end;*)

procedure TCameraActionRequestor.EndWaitForJobLoad(Success: Boolean);
begin
WaitTimeout.Enabled := False;
//CogNexCommunicator.ExternalLockBusy := False;
if success then
  begin
  AddDebug('Job Loaded and reset',tvmDebug);
  ActionStage := tcaLoadJobDone;
  SetActionTimer(True);
  end
else
  begin
  AddDebug('Job Failed to Loaded and reset',tvmDebug);
  ActionStage := tcaLoadJobFailed;
  SetActionTimer(True);
  end

end;

procedure TCameraActionRequestor.EndWaitForDefaultLoadOnReset(Success: Boolean);
begin
WaitTimeout.Enabled := False;
if Success then
  begin
  IsResetRestore := True;
  ActionStage := tcaRestoreStart;
  SetActionTimer(True);
  end
else
  begin
  AddDebug('Default Load Fail',tvmDebug);
  IsResetRestore := True;
  ActionStage := tcaRestoreStart;
  SetActionTimer(True);
  end;
end;

procedure TCameraActionRequestor.SetDefaultActive(const Value: Boolean);
begin
  FDefaultJobActive := Value;
end;




procedure TCameraActionRequestor.ProcessFitChanged;
var
PWidth : Integer;
PHeight: Integer;
WidthScale: Double;
HeightScale: Double;
begin
if CurrentCameraState.FitMode = 1 then
  begin
  IsFullFOVTag.AsInteger := 1;
  CognexParent.Align := alClient;
  PWidth := CognexParent.ClientWidth-10;
  PHeight := CognexParent.ClientHeight-10;
  WidthScale := PWidth/CurrentCameraState.PixelWidth;
  HeightScale := PHeight/CurrentCameraState.PixelHeight;
  COFOVTag.AsInteger := 1;
  COXTag.AsInteger := 0;
  If (WidthScale < HeightScale) then
    begin
    CognexDisplay.ImageScale := WidthScale;
    CurrentScale := WidthScale;
    end
  else
    begin
    CognexDisplay.ImageScale := HeightScale;
    CurrentScale := HeightScale;
    end;
  end;
  CurrentCameraState.Scale := CurrentScale;
  KickLazyUpdate;
if CurrentCameraState.CentreMode = tcmXHair then InternalCentreOnXHair else InternalCentreOnFOV;
end;

procedure TCameraActionRequestor.InternalCentreOnXHair;
begin
if InstallDone then
  begin
  if FDefaultJobActive then
    begin
      CurrentCameraState.CentreMode := tcmXHair;
      CurrentCameraState.CentreModeInteger := 1;
      COFOVTag.AsInteger := 0;
      COXTag.AsInteger := 1;
   end;
   SceneScaleTag.AsDouble := CognexDisplay.ImageScale;
   DefaultJobImageScaleChanged(nil);
   XHCentreMode := 1;
   KickLazyUpdate;
 end;
end;

procedure TCameraActionRequestor.InternalCentreOnFOV;
begin
if InstallDone then
  begin
  if FDefaultJobActive  then
    begin
      CurrentCameraState.CentreMode := tcmScreen;
      CurrentCameraState.CentreModeInteger := 0;
      COFOVTag.AsInteger := 1;
      COXTag.AsInteger := 0;
   end;
 SceneScaleTag.AsDouble := CognexDisplay.ImageScale;
 DefaultJobImageScaleChanged(nil);
 XHCentreMode := 0;
 KickLazyUpdate;
 end;

end;


procedure TCameraActionRequestor.SetCircleOnOff(OnState : Boolean);
begin
if CameraCanTalk then
  begin
  if FDefaultJobActive then
    begin
    if OnState then
      begin
      CurrentCameraState.CircleOn := 1;
      CogNexCommunicator.SendServiceCommand('SICIRCLE_1.TOOL_ENABLED 1');
      CircleIsOnTag.AsInteger := 0;
      CircleIsOnTag.AsInteger := 1;
      end
    else
      begin
      CurrentCameraState.CircleOn := 0;
      CogNexCommunicator.SendServiceCommand('SICIRCLE_1.TOOL_ENABLED 0');
      CircleIsOnTag.AsInteger := 1;
      CircleIsOnTag.AsInteger := 0;
      end;
    KickLazyUpdate;
    end;
  end;
end;


procedure TCameraActionRequestor.SetXHairOnOff(OnState : Boolean);
begin
if CameraCanTalk then
  begin
  if FDefaultJobActive then
    begin
    if OnState then
      begin
      CurrentCameraState.aaXHairOn := 1;
      CogNexCommunicator.SendServiceCommand('SIVerticalLine.TOOL_ENABLED 1');
      CogNexCommunicator.SendServiceCommand('SIHorizontalLine.TOOL_ENABLED 1');
      XHairIsOnTag.AsInteger := 0;
      XHairIsOnTag.AsInteger := 1;
      end
    else
      begin
      CurrentCameraState.aaXHairOn := 0;
      CogNexCommunicator.SendServiceCommand('SIVerticalLine.TOOL_ENABLED 0');
      CogNexCommunicator.SendServiceCommand('SIHorizontalLine.TOOL_ENABLED 0');
      XHairIsOnTag.AsInteger := 1;
      XHairIsOnTag.AsInteger := 0;
      end
    end;
    KickLazyUpdate;
  end;
end;

procedure TCameraActionRequestor.DoLazyRestore(Sender: TObject);
begin
LazyRestoreTimer.Enabled := False;
RequestRestore(False);
end;

procedure TCameraActionRequestor.ResetStuck(Sender: TObject);
begin
ResetTimeout.Enabled := False;
AddDebug('Reset Stuck Going to no Cognex Mode',tvmDebug);
if assigned(FOnResetFail) then FOnResetFail(Self)
//AddDebug('Resetting Stuck Reset Temporary Do Nothing',tvmDebug);
//CogNexCommunicator.ClearDown;
//ClearDown;
//ForceReset := True;
//RequestReset(False,False);
{ TODO : sort out what I do here...Is force reset required? }
//ResetTimeout.Enabled := False;
//CogNexCommunicator.ClearDown;
//ClearDown;
end;

{procedure TCameraActionRequestor.ConnectTimedOut(Sender: TObject);
begin
//CAInitialConnectTimeout.Enabled := False;
AddDebug('Initial Connect Timed Out --- Retryinging',tvmDebug);
//if

end;}

procedure TCameraActionRequestor.DoScreenUpdate;
var
CFile : TIniFile;
begin
  ResetFitMode := CurrentCameraState.FitMode;
  ResetXHMode := CurrentCameraState.CentreModeInteger;
  ResetImageScale := CurrentCameraState.Scale;
  if FileExists(FConfigFileName) then
    begin
    CFile := TIniFile.Create(ConfigFilename);
    if assigned(CFile) then
    try
    CFile.WriteInteger('DYNAMIC','XHCentreMode',ResetXHMode);
    Cfile.WriteInteger('DYNAMIC','FitMode',ResetFitMode);
    CFile.WriteFloat('DYNAMIC','Scale',ResetImageScale);
    finally
    CFile.Free;
    end;
    end;
end;



procedure TCameraActionRequestor.DoScreenUpdateRequest(Sender: TObject);
begin
RequestScreenUpdate;
end;

procedure TCameraActionRequestor.DoLazyUpdate(Sender: TObject);
var
CFile : TIniFile;
begin
  LazyUpdateTimer.Enabled := False;
  If EnableCameraUpdate = False Then exit;
  BufferCameraState := CurrentCameraState;
  if FileExists(FConfigFileName) then
    begin
    CFile := TIniFile.Create(ConfigFilename);
    if assigned(CFile) then
    try
    Cfile.WriteFloat('DYNAMIC','CircleRadius',CurrentCameraState.CircleRadius);
    Cfile.WriteInteger('DYNAMIC','CircleColorNumber',CurrentCameraState.CircleColorNumber);
    Cfile.WriteInteger('DYNAMIC','XHairColorNumber',CurrentCameraState.XHairColorNumber);
    Cfile.WriteInteger('DYNAMIC','XHairOn',CurrentCameraState.aaXHairOn);
    Cfile.WriteInteger('DYNAMIC','CircleOn',CurrentCameraState.CircleOn);
    CFile.WriteFloat('DYNAMIC','Exposure',CurrentCameraState.Exposure);
    if(FUpdateCalibration) then
      begin
      FUpdateCalibration := False;
      CFile.WriteFloat('DYNAMIC','JobScaling',CurrentCameraState.JobScaling);
      ScalingTag.AsDouble := CurrentCameraState.JobScaling;
      CFile.WriteFloat('DYNAMIC','JobXOffset',CurrentCameraState.XOffset);
      XoffsetTag.AsDouble := CurrentCameraState.Xoffset;
      CFile.WriteFloat('DYNAMIC','JobYOffset',CurrentCameraState.YOffset);
      YoffsetTag.AsDouble := CurrentCameraState.Yoffset;
      CFile.WriteInteger('DYNAMIC','OffsetIsManual',CurrentCameraState.OffsetIsManual);
      if assigned(FOnCalDataChanged) then  FOnCalDataChanged(Self,CurrentCameraState.JobScaling,CurrentCameraState.XOffset,CurrentCameraState.YOffset);
      end;

    if assigned(FOnOffsetIsManualChange) then   FOnOffsetIsManualChange(Self,CurrentCameraState.OffsetIsManual);
    finally
    CFile.Free;
    if ScreenUpdatePending then
      begin
      ScreenUpdatePending := False;
      DoScreenUpdate;
      end;
    end;
    end;

end;

procedure TCameraActionRequestor.ChangeLoadedJobImageScale(NewScale : Double;CentreOnXHair : Integer);
var
PWidth : Integer;
PHeight: Integer;
ImageWidth: Integer;
ImageHeight: Integer;
Xoff,Yoff : Integer;
begin
GlobalCognexForm.CDisplayBeginUpdate;
try
try
CognexDisplay.ImageScale := NewScale;
CurrentCameraState.Scale := NewScale;
if assigned(CognexDisplay.Parent) then
  begin
  PWidth := CognexDisplay.Parent.ClientWidth;
  PHeight := CognexDisplay.Parent.ClientHeight;
  end
else
  begin
  exit;
  end;
SceneScaleTag.AsDouble := CognexDisplay.ImageScale;
if CognexDisplay.ImageScale > 0.1 then
  begin
  ImageWidth := Round(CurrentCameraState.PixelWidth * CognexDisplay.ImageScale);
  ImageHeight:= Round(CurrentCameraState.PixelHeight * CognexDisplay.ImageScale);
  end
else
  begin
  exit;
  end;
  CurrentCameraState.FitMode := 0;
  IsFullFOVTag.AsInteger := 0;
  if CentreOnXHair = 0 then
    begin
    if (ImageWidth > PWidth) or (ImageHeight > PHeight) then
      begin
      CognexDisplay.Align := alNone;
      if (ImageWidth > PWidth) then
        begin
        CognexDisplay.Width := ImageWidth+1;
        CognexDisplay.Left := -(ImageWidth - PWidth) div 2
        end;
      if (ImageHeight > PHeight) then
        begin
        CognexDisplay.Height := ImageHeight+1;
        CognexDisplay.Top := -(ImageHeight - PHeight) div 2
        end;
      end
    else
      CognexDisplay.Align := alClient;
    end
  else
    begin
      begin
      CognexDisplay.Align := alNone;
      CognexDisplay.Width := ImageWidth+1;
      XOff := Round((Round(CurrentCameraState.XOffset)-(CurrentCameraState.PixelWidth div 2))* CognexDisplay.ImageScale);
      CognexDisplay.Left := (-(ImageWidth - PWidth) div 2)-Xoff;
      CognexDisplay.Height := ImageHeight+1;
      YOff := Round((Round(CurrentCameraState.YOffset)-(CurrentCameraState.PixelHeight div 2))*CognexDisplay.ImageScale);
      CognexDisplay.Top := (-(ImageHeight - PHeight) div 2)-Yoff;
      end
     end;
  finally
  GlobalCognexForm.CDisplayEndUpdate;
  end
  except
  GlobalCognexForm.CDisplayEndUpdateOnException;
  end

end;

procedure TCameraActionRequestor.DefaultJobImageScaleChanged(Sender : TObject);
var
PWidth : Integer;
PHeight: Integer;
ImageWidth: Integer;
ImageHeight: Integer;
Xoff,Yoff : Integer;
begin
GlobalCognexForm.CDisplayBeginUpdate;
try
try
if assigned(CognexDisplay.Parent) then
  begin
  PWidth := CognexDisplay.Parent.ClientWidth;
  PHeight := CognexDisplay.Parent.ClientHeight;
  end
else
  begin
  exit;
  end;
SceneScaleTag.AsDouble := CognexDisplay.ImageScale;
if CognexDisplay.ImageScale > 0.1 then
  begin
  ImageWidth := Round(CurrentCameraState.PixelWidth * CognexDisplay.ImageScale);
  ImageHeight:= Round(CurrentCameraState.PixelHeight * CognexDisplay.ImageScale);
  if CurrentCameraState.CentreMode = tcmScreen then
    begin
    if (ImageWidth > PWidth) or (ImageHeight > PHeight) then
      begin
      CognexDisplay.Align := alNone;
      if (ImageWidth > PWidth) then
        begin
        CognexDisplay.Width := ImageWidth+1;
        CognexDisplay.Left := -(ImageWidth - PWidth) div 2
        end;
      if (ImageHeight > PHeight) then
        begin
        CognexDisplay.Height := ImageHeight+1;
        CognexDisplay.Top := -(ImageHeight - PHeight) div 2
        end;
      end
    else
      CognexDisplay.Align := alClient;
    end
  else
    begin
    // To Put the Display on Centre
//    if (ImageWidth > PWidth) or (ImageHeight > PHeight) then
      begin
      CognexDisplay.Align := alNone;
//      if (ImageWidth > PWidth) then
        begin
        CognexDisplay.Width := ImageWidth+1;
        XOff := Round((Round(CurrentCameraState.XOffset)-(CurrentCameraState.PixelWidth div 2))* CognexDisplay.ImageScale);
        CognexDisplay.Left := (-(ImageWidth - PWidth) div 2)-Xoff;
        end;
//      if (ImageHeight > PHeight) then
        begin
        CognexDisplay.Height := ImageHeight+1;
        YOff := Round((Round(CurrentCameraState.YOffset)-(CurrentCameraState.PixelHeight div 2))*CognexDisplay.ImageScale);
        CognexDisplay.Top := (-(ImageHeight - PHeight) div 2)-Yoff;
        end;
      end
//    else
//      CDisplay.Align := alClient;
     end;
  end;
  finally
  GlobalCognexForm.CDisplayEndUpdate;
  end
  except
  GlobalCognexForm.CDisplayEndUpdateOnException;
  end
end;


procedure TCameraActionRequestor.SaveCameraState;
begin
  BufferCameraState.Scale := CurrentCameraState.Scale;
  BufferCameraState.FitMode := CurrentCameraState.FitMode;
  BufferCameraState.CentreMode := CurrentCameraState.CentreMode;
  BufferCameraState.CentreModeInteger := CurrentCameraState.CentreModeInteger;
  BufferCameraState.CircleRadius := CurrentCameraState.CircleRadius;
  BufferCameraState.XOffset  := CurrentCameraState.XOffset;
  BufferCameraState.YOffset  := CurrentCameraState.YOffset;
end;


procedure TCameraActionRequestor.KickLazyUpdate;
begin
LazyUpdateTimer.Enabled := True;
end;

procedure TCameraActionRequestor.KickLazyRestore;
begin
LazyRestoreTimer.Enabled := True;
end;


procedure TCameraActionRequestor.DoCalibrationUpdate;
begin
FUpdateCalibration := True;
KickLazyUpdate;
end;

function TCameraActionRequestor.CameraCanTalk: Boolean;
begin
{!!!!  if Disconnecting or Disconnected then
    begin
    Result := False;
    end
  else }
    Result := CogNexCommunicator.CanTalk;

end;




procedure TCameraActionRequestor.ShutDownRequest;
begin
    InShutDown := True;
    ActionTimer.enabled := false;
    WaitTimeout.enabled := false;
    LazyUpdateTimer.enabled := false;
    LazyRestoreTimer.enabled := false;
    ResetTimeout.enabled := false;
    OfflineWaitTimer.enabled := false;
//    CAInitialConnectTimeout.enabled := false;



    ActionTimer.Free;
    WaitTimeout.Free;
    LazyUpdateTimer.Free;
    LazyRestoreTimer.Free;
    ResetTimeout.Free;
    OfflineWaitTimer.Free;
//    CAInitialConnectTimeout.Free;
end;




procedure TCameraActionRequestor.SetActionTimer(State: Boolean);
begin
if State = True then
  begin
  aaIsIdle := False;
  ActionTimer.OnTimer := aaCameraAction;
  ActionTimer.Enabled := True;
  end
else
  begin
  ActionTimer.Enabled := False;
  end
end;


procedure TCameraActionRequestor.SetCurrentScale(const Value: Double);
begin
  FCurrentScale := Value;
end;


procedure TCameraActionRequestor.OfflineFailed(Sender: TObject);
begin
OfflineWaitTimer.Enabled := False;
AddDebug('Failed To Go OffLine',tvmDebug);
ActionStage := tcaTelnetDisconnect;
ActionTimer.Enabled := True;
end;

procedure TCameraActionRequestor.SetIdle(const Value: Boolean);
begin
  FIsIdle := Value;
  if CanStart then
    begin
    AddDebug('CAH Idle = 1',tvmDebug);
    if FullResetInProgress then AddDebug('##ERRRRRRRR',tvmOutOnly);
    end
  else
    begin
    AddDebug('CAH Idle = 0',tvmDebug);
    end;
end;

procedure TCameraActionRequestor.FTPIsLoggedIn(Sender: TObject; LoginState: Boolean);
begin
if Assigned(FOnFTPLoggedIn) then FOnFTPLoggedIn(Sender,LoginState);
end;

procedure TCameraActionRequestor.JobListHasChanged(Sender: TObject;AtPOwerUp : Boolean);
begin
if Assigned(FonJobListChanged) then FonJobListChanged(Sender,AtPOwerUp);
end;

procedure TCameraActionRequestor.FTPActionComplete(ActionType: TFTPActionTypes;Success : Boolean);
begin
case ActionType of

 taFTPTelnetConnect:
   begin
   if assigned(FOnFTPLoggedIn) then FOnFTPLoggedIn(SElf,Success);
   end;

 taGetJobList:
   begin
   if assigned(FonJobListChanged) then FonJobListChanged(SElf,False);
   end;

 taGetInitialJobList:
   begin
   if assigned(FonJobListChanged) then FonJobListChanged(SElf,True);
   end;

 taImportFile:
   begin
//   if assigned(FonFileImported) then FonFileImported(ImportFilePath,Success);
   end;
 end;



end;


{procedure TCameraActionRequestor.FTPWatchDog(Action: TFTPThreadActionTypes);
begin
case Action of
  taIdle:
    begin
    inc(FTPIdleCount)
    end;
end; //case
end;}


procedure TCameraActionRequestor.SetTelnetMemo(const Value: TTelnetDisplay);
begin
  FTelnetMemo := Value;
//  if assigned(FTPServer) then FTPServer.DebugMemo := FTelnetMemo;
end;




procedure TCameraActionRequestor.SetLatchJob(const Value: Boolean);
begin
  FLatchJob := Value;
end;

function TCameraActionRequestor.GetScreenIsInResetMode: Boolean;
begin
Result := False;
if FDefaultJobActive then
  begin
  if (CurrentCameraState.FitMode = ResetFitMode)
     and (CurrentCameraState.CentreModeInteger = ResetXHMode)
     and (CurrentCameraState.Scale = ResetImageScale) then Result := True;
  end;
end;


procedure  TCameraActionRequestor.TidyDownResetCheck;
begin
{ TODO : implement this }
end;



{procedure TCameraActionRequestor.SetImmediateCommandPending(const Value: Boolean);
begin
//  FImmediateCommandPending := Value;
  if not Value then ImmediateCommandLock := False;
end;}


end.



