unit CognexInSight_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// PASTLWTR : 1.2
// File generated on 29/09/2016 10:07:59 from Type Library described below.

// ************************************************************************  //
// Type Lib: C:\PROGRA~1\COMMON~1\Cognex\In-Sight\52133~1.2\COGNEX~2.TLB (2)
// LIBID: {86972E3A-D963-407B-83F3-4D155D7B276C}
// LCID: 0
// Helpfile: 
// HelpString: Cognex  In-Sight Type Library
// DepndLst: 
//   (1) v2.0 stdole, (C:\WINDOWS\system32\stdole2.tlb)
//   (2) v2.4 mscorlib, (C:\WINDOWS\Microsoft.NET\Framework\v4.0.30319\mscorlib.tlb)
//   (3) v2.0 System_Drawing, (c:\WINDOWS\Microsoft.NET\Framework\v2.0.50727\System.Drawing.tlb)
// Parent TypeLibrary:
//   (0) v1.2 CvsInSightDisplayOcx, (C:\Program Files\Common Files\Cognex\In-Sight\5.2.133.2\CvsInSightDisplay.ocx)
// Errors:
//   Hint: Member 'LiveAcquisitionResolution' of '_CvsSettings' changed to 'LiveAcquisitionResolution1'
//   Hint: Member 'LiveAcquisitionMaxImageRate' of '_CvsSettings' changed to 'LiveAcquisitionMaxImageRate1'
//   Hint: Member 'OnlineImageResolution' of '_CvsSettings' changed to 'OnlineImageResolution1'
//   Hint: Member 'OnlineAcquisitionMaxImageRate' of '_CvsSettings' changed to 'OnlineAcquisitionMaxImageRate1'
//   Hint: Member 'OnlineImagesBitDepth' of '_CvsSettings' changed to 'OnlineImagesBitDepth1'
//   Hint: Member 'LiveImagesBitDepth' of '_CvsSettings' changed to 'LiveImagesBitDepth1'
//   Hint: Member 'CdsSupported' of '_CvsSettings' changed to 'CdsSupported1'
//   Hint: Member 'EditFlags' of '_CvsGraphicPolygon' changed to 'EditFlags1'
//   Hint: Member 'EditBounds' of '_CvsGraphicPolygon' changed to 'EditBounds1'
//   Hint: Member 'Enabled' of '_CvsNetworkMonitor' changed to 'Enabled1'
//   Hint: Member 'PingInterval' of '_CvsNetworkMonitor' changed to 'PingInterval1'
//   Hint: Parameter 'object' of _ArgUpdateHandler.BeginInvoke changed to 'object_'
//   Hint: Parameter 'array' of _CvsRemoteSubnetCollection.CopyTo changed to 'array_'
//   Hint: Member 'Do' of '_UndoCellTagsAction' changed to 'Do_'
//   Hint: Member 'Do' of '_UndoClearAllAction' changed to 'Do_'
//   Hint: Member 'Do' of '_UndoCutAndPasteAction' changed to 'Do_'
//   Hint: Member 'Do' of '_UndoExpressionAction' changed to 'Do_'
//   Hint: Member 'Do' of '_UndoOpcTagEditorAction' changed to 'Do_'
//   Hint: Member 'Do' of '_UndoPropertySheetAction' changed to 'Do_'
//   Hint: Member 'Do' of '_UndoReferenceRedirectAction' changed to 'Do_'
//   Hint: Member 'Do' of '_UndoScriptChangeAction' changed to 'Do_'
//   Hint: Parameter 'object' of _CdsConnectionChange.BeginInvoke changed to 'object_'
//   Hint: Member 'Label' of '_CvsGraphic' changed to 'Label_'
//   Hint: Member 'Label' of '_CvsGraphicArrows' changed to 'Label_'
//   Hint: Symbol 'Type' renamed to 'type_'
//   Hint: Symbol 'Type' renamed to 'type_'
//   Hint: Member 'Label' of '_CvsGraphicCompositeBase' changed to 'Label_'
//   Hint: Parameter 'Label' of _CvsGraphicCompositeBase.Initialize changed to 'Label_'
//   Hint: Member 'Label' of '_CvsGraphicComposite' changed to 'Label_'
//   Hint: Parameter 'Label' of _CvsGraphicComposite.Initialize changed to 'Label_'
//   Hint: Member 'Label' of '_CvsGraphicPolylinePath' changed to 'Label_'
//   Hint: Parameter 'Label' of _CvsGraphicPolylinePath.Initialize changed to 'Label_'
//   Hint: Member 'Label' of '_CvsGraphicMaskedRegion' changed to 'Label_'
//   Hint: Parameter 'Label' of _CvsGraphicMaskedRegion.Initialize changed to 'Label_'
//   Hint: Member 'Label' of '_CvsGraphicDefectPoints' changed to 'Label_'
//   Hint: Member 'Label' of '_CvsGraphicEdgePoints' changed to 'Label_'
//   Hint: Member 'Label' of '_CvsGraphicExtremePoints' changed to 'Label_'
//   Hint: Member 'Label' of '_CvsGraphicFilledBox' changed to 'Label_'
//   Hint: Member 'Label' of '_CvsGraphicParallelogram' changed to 'Label_'
//   Hint: Member 'Label' of '_CvsGraphicPolygon' changed to 'Label_'
//   Hint: Parameter 'Label' of _CvsGraphicPolygon.Initialize changed to 'Label_'
//   Hint: Parameter 'Type' of _CvsDiscreteIOLines.IsValidInputType changed to 'Type_'
//   Hint: Parameter 'Type' of _CvsUniversalIOSettings.IsValidInputType changed to 'Type_'
//   Hint: Symbol 'Type' renamed to 'type_'
//   Hint: Symbol 'Type' renamed to 'type_'
//   Hint: Parameter 'record' of _CvsOcrMaxAutoTune.UpdateRecord changed to 'record_'
//   Hint: Parameter 'record' of _CvsOcrMaxAutoTune.DeleteRecord changed to 'record_'
//   Hint: Parameter 'record' of _CvsOcrMaxAutoTune.GetRecordImageAndMarkings changed to 'record_'
//   Hint: Parameter 'object' of _ArgUpdateHandler_2.BeginInvoke changed to 'object_'
//   Hint: Parameter 'object' of _AutoTuneSessionClosedHandler.BeginInvoke changed to 'object_'
//   Hint: Symbol 'Type' renamed to 'type_'
//   Hint: Symbol 'Type' renamed to 'type_'
//   Hint: Symbol 'Type' renamed to 'type_'
//   Hint: Parameter 'Type' of _CvsDiscreteOutputLine.IsValidOutputType changed to 'Type_'
//   Hint: Parameter 'Type' of _CvsExpansionIOSettings.IsValidInputType changed to 'Type_'
//   Hint: Member 'Label' of '_CvsGraphic4Side' changed to 'Label_'
//   Hint: Member 'Label' of '_CvsGraphicArc' changed to 'Label_'
//   Hint: Parameter 'Label' of _CvsGraphicArc.Initialize changed to 'Label_'
//   Hint: Member 'Label' of '_CvsGraphicBlob' changed to 'Label_'
//   Hint: Member 'Label' of '_CvsGraphicCircle' changed to 'Label_'
//   Hint: Parameter 'Label' of _CvsGraphicCircle.Initialize changed to 'Label_'
//   Hint: Member 'Label' of '_CvsGraphicPatternMatch' changed to 'Label_'
//   Hint: Member 'Label' of '_CvsGraphicCross' changed to 'Label_'
//   Hint: Member 'Label' of '_CvsGraphicEdgeProfile' changed to 'Label_'
//   Hint: Member 'Label' of '_CvsGraphicFixture' changed to 'Label_'
//   Hint: Parameter 'Label' of _CvsGraphicFixture.Initialize changed to 'Label_'
//   Hint: Member 'Label' of '_CvsGraphicHistogram' changed to 'Label_'
//   Hint: Member 'Label' of '_CvsGraphicImage' changed to 'Label_'
//   Hint: Member 'Label' of '_CvsGraphicBlobProperties' changed to 'Label_'
//   Hint: Member 'Label' of '_CvsGraphicLine' changed to 'Label_'
//   Hint: Parameter 'Label' of _CvsGraphicLine.Initialize changed to 'Label_'
//   Hint: Parameter 'Label' of _CvsGraphicLine.Initialize_2 changed to 'Label_'
//   Hint: Member 'Label' of '_CvsGraphicOffset' changed to 'Label_'
//   Hint: Parameter 'Label' of _CvsGraphicOffset.Initialize changed to 'Label_'
//   Hint: Member 'Label' of '_CvsGraphicPoint' changed to 'Label_'
//   Hint: Parameter 'Label' of _CvsGraphicPoint.Initialize changed to 'Label_'
//   Hint: Member 'Label' of '_CvsGraphicPolyline' changed to 'Label_'
//   Hint: Member 'Label' of '_CvsGraphicRegion' changed to 'Label_'
//   Hint: Member 'Label' of '_CvsGraphicText' changed to 'Label_'
//   Hint: Parameter 'Label' of _CvsGraphicText.Initialize changed to 'Label_'
//   Hint: Member 'Label' of '_CvsGraphicAnnulus' changed to 'Label_'
//   Hint: Parameter 'Label' of _CvsGraphicAnnulus.Initialize changed to 'Label_'
//   Hint: Symbol 'Type' renamed to 'type_'
//   Hint: Member 'File' of '_CvsInSight' changed to 'File_'
//   Hint: Parameter 'Type' of _CvsLocalIOSettings.IsValidInputType changed to 'Type_'
//   Hint: Parameter 'Type' of _CvsUtility.CvsDiscreteInputLineSupportsSignalSetting changed to 'Type_'
//   Hint: Parameter 'Type' of _CvsUtility.CvsDiscreteOutputLineSupportsPulseLengthSetting changed to 'Type_'
//   Hint: Parameter 'Type' of _CvsUtility.CvsDiscreteOutputLineSupportsStrobeTriggerSelection changed to 'Type_'
//   Hint: Parameter 'Type' of _CvsUtility.CvsDiscreteOutputLineSupportsPulseAndAcquisitionDelay changed to 'Type_'
//   Hint: Parameter 'object' of _CvsAcceptMessageHandler.BeginInvoke changed to 'object_'
// ************************************************************************ //
// *************************************************************************//
// NOTE:                                                                      
// Items guarded by $IFDEF_LIVE_SERVER_AT_DESIGN_TIME are used by properties  
// which return objects that may need to be explicitly created via a function 
// call prior to any access via the property. These items have been disabled  
// in order to prevent accidental use from within the object inspector. You   
// may enable them by defining LIVE_SERVER_AT_DESIGN_TIME or by selectively   
// removing them from the $IFDEF blocks. However, such items must still be    
// programmatically created via a method of the appropriate CoClass before    
// they can be used.                                                          
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
{$WARN SYMBOL_PLATFORM OFF}
{$WRITEABLECONST ON}
{$VARPROPSETTER ON}
interface

uses Windows, ActiveX, Classes, Graphics, mscorlib_TLB, OleServer, StdVCL, System_Drawing_TLB, 
Variants;
  

// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  CognexInSightMajorVersion = 5;
  CognexInSightMinorVersion = 2;

  LIBID_CognexInSight: TGUID = '{86972E3A-D963-407B-83F3-4D155D7B276C}';

  IID__CvsCell: TGUID = '{099BD627-3E57-39C4-AA0A-126821639B22}';
  IID__CvsCellOcrMaxAutoTune: TGUID = '{0C8CCB26-D3FA-3704-8D48-2BC551E1FAEC}';
  IID_ICvsAction: TGUID = '{477FA70B-A051-47B1-8A03-E119EC212284}';
  DIID_ICvsActionEvents: TGUID = '{477FA70B-A051-47B1-8A03-E889EC211E84}';
  IID_ICvsActionObject: TGUID = '{74D721EB-659B-4607-B9C5-C0FC17A44344}';
  IID_ICvsActionToggle: TGUID = '{477FA70B-A051-47B1-8A03-E223EC211EE4}';
  CLASS_CvsActionToggle: TGUID = '{F1668C1A-E283-44EA-AA68-511E664DEB76}';
  IID__CvsCrcResponse: TGUID = '{5971FCFF-A542-3854-BB5E-985965238834}';
  IID__CvsCellString: TGUID = '{9AA03B85-804A-347B-9318-3054189C0C59}';
  IID__CvsCellAuthorize: TGUID = '{6D51A27E-BAA4-3CFF-BA51-2162A61018F2}';
  IID__CvsCellEditGraphic: TGUID = '{7F66847B-AB9B-3E85-9803-5C6F5239628D}';
  IID__CvsCellEditMultiGraphics: TGUID = '{15E722BC-6E78-3CD0-85A5-26C16F29AF14}';
  IID__CvsCellReference: TGUID = '{8272F831-480F-3FDA-AC8D-A6E434285C42}';
  IID__CvsCellReferenceInfo: TGUID = '{6F20E7AD-065E-35B7-ABDC-C9D833F71F8B}';
  IID__CvsCellReferenceTable: TGUID = '{0F9FDEA5-5FCF-38EA-966B-209108242E8A}';
  IID__CvsDefaultRegion: TGUID = '{4CD3CAE1-5503-385F-8406-70C00852C0A5}';
  IID__CvsEasyView: TGUID = '{B8BBEA0D-1383-3C0E-BB05-68F9D4326791}';
  IID__CvsEasyViewItem: TGUID = '{7100135C-A3BA-3B2A-BEB7-836E2EC90BF0}';
  IID__CvsFeatureLicense: TGUID = '{9CD6199B-6343-3BD3-8038-4F8DA367D226}';
  IID__CvsIspMessageStore: TGUID = '{09DCA83D-F627-3EC4-A7D6-818AE78DF571}';
  IID__CvsJobServerSettings: TGUID = '{E4AD036A-F152-386C-9A15-283DF1C0A7F8}';
  IID__CvsOcrMaxDiagnosticData: TGUID = '{AF2197DF-BF7F-3559-AABF-F879E704E398}';
  IID__FragmentStatistics: TGUID = '{EE8CD83B-EEF9-3735-8B52-00CDB503FBE1}';
  IID__CharacterStatistics: TGUID = '{399FF024-59EC-3C17-B517-3546A3CEB45B}';
  IID__CvsOcrMaxDiagnostics: TGUID = '{65B3AA14-99C5-33F2-BC86-F41D16A38013}';
  IID_ICvsOcrMaxExpressionEditor: TGUID = '{F404247C-4F50-4D8C-9750-05D545995D21}';
  IID__CvsOcrMaxExpressionEditorBase: TGUID = '{33C9EAA7-B5CB-3B6F-98A0-4083C985A1FF}';
  IID__CvsOcrMaxSpreadsheetExpressionEditor: TGUID = '{6554EBF0-A814-3D3C-903D-BC0BB6500537}';
  IID__ArgUpdateHandler: TGUID = '{8B49AD1C-3A22-34A7-AF5D-3BF7D1E2AFD2}';
  IID__CvsPowerlinkSettings: TGUID = '{61ADD365-19DA-36EB-B784-CBFBF745A63D}';
  IID__CvsRemoteSubnetCollection: TGUID = '{0A67ED61-DB3A-37AE-87E8-D76919CB769F}';
  IID__CvsRemoteSubnetEntry: TGUID = '{01CF13EA-E55E-31F1-AF6B-87B8CC131012}';
  IID__CvsSensor: TGUID = '{768D28B1-8C55-383E-B409-DCFA8954A4AE}';
  IID__CvsUser: TGUID = '{EE3D71F3-7D97-3E28-A715-75B214A2FDEB}';
  IID__CvsInSightExtensions: TGUID = '{E7A9EF7A-D411-3265-B924-402262C986F8}';
  IID__CvsSettings: TGUID = '{42C74EC2-8B77-3621-8C5F-361776D7902E}';
  IID__UndoAction: TGUID = '{B4DABB56-FED0-324E-BACA-8731148D1999}';
  IID__UndoCellTagsAction: TGUID = '{1F925BF3-94A8-3451-AF7F-B3D1584E70D7}';
  IID__UndoClearAllAction: TGUID = '{F889BDF4-0EEB-3D75-9A34-4A67A31E182F}';
  IID__UndoCutAndPasteAction: TGUID = '{3549ABDB-38DA-36B4-AEBE-0FA1BEEB3CF5}';
  IID__UndoEventArgs: TGUID = '{A5B5906D-27A8-392B-A11C-ECFA005C35C9}';
  IID__UndoExpressionAction: TGUID = '{FCB28B69-8A40-3CE8-86C8-9FEC7EE91476}';
  IID__UndoOpcTagEditorAction: TGUID = '{73358417-1DAD-3136-9085-5C4E9400191A}';
  IID__UndoPropertySheetAction: TGUID = '{851C990B-2069-3B2B-B5AF-8D090F0A616E}';
  IID__UndoReferenceRedirectAction: TGUID = '{8B599734-9247-3F81-A95B-B72E5893033A}';
  IID__UndoScriptChangeAction: TGUID = '{E390666D-1791-371A-9D23-3017B3A15BD6}';
  IID__CdsConnectionChange: TGUID = '{30EFDA1B-F328-36F4-8674-42CD2CF6B4C5}';
  IID__CvsGraphic: TGUID = '{0CCF4169-CBA6-3343-9564-71FCE1254EF9}';
  IID__CvsGraphicArrows: TGUID = '{4393C594-D9B6-32C9-8EFB-089E1BD9AC99}';
  IID__CvsCellEditMaskedRegion: TGUID = '{58ADA604-FF86-3DFA-9D16-1ABFF6AA9AEE}';
  IID__CvsCellEditPolylinePath: TGUID = '{5CEEAA4B-FBEC-3E06-878D-21A23AF79C93}';
  IID__CvsAuditMessageSettings: TGUID = '{8C2ACA9A-F0DC-37BC-BBA3-FCBB1DEBC631}';
  IID_ICaliperEditor: TGUID = '{D3E948D6-F022-42F7-9999-566ACF0CCF5B}';
  IID__CvsCaliperScoringMethod: TGUID = '{A4FE0B1D-E603-349C-8BB7-76EDC46C1BC2}';
  IID__CvsCellEditComposite: TGUID = '{C9C050F1-E8A3-3A62-B5AC-BE4628ADBD44}';
  IID__CvsCellEditPolygon: TGUID = '{0E832240-E333-3F3D-9FC9-B050B4EA7CE9}';
  IID__CvsColorMatchEditor: TGUID = '{CF595F98-ECD7-3F67-8A9E-474EFBFE7404}';
  IID__CvsColorModel: TGUID = '{30B1E578-8789-3E41-B9DF-D8F74D3313B6}';
  IID__CvsEasyViewHelper: TGUID = '{60F573FB-8BC9-38A8-83C3-5980D1C03B5A}';
  IID__CvsCompositeSubregion: TGUID = '{7A49158D-4C5E-3426-B6CE-F1B2A87F46FA}';
  IID_ICvsEditableGraphic: TGUID = '{300FD59B-6D5B-4DF0-ACFA-148DFE4BDCA8}';
  IID__CvsGraphicCompositeBase: TGUID = '{11B694E0-3C62-3F08-943D-40DC345A998E}';
  IID__CvsGraphicComposite: TGUID = '{BEA06CB0-516B-31BA-BE67-2D6BCD22B778}';
  IID__CvsGraphicPolylinePath: TGUID = '{F1FB4FFA-F36D-3018-94DF-80C519B9DA18}';
  IID__CvsGraphicMaskedRegion: TGUID = '{8E08C0A5-7B2A-3CE5-9B5A-5A9B4547618D}';
  IID__CvsGraphicDefectPoints: TGUID = '{B13E5E00-6116-3A1A-9E37-446E4C2F9FCD}';
  IID__CvsGraphicEdgePoints: TGUID = '{18C86A45-77C4-32A8-816B-929C8B750A01}';
  IID__CvsGraphicExtremePoints: TGUID = '{4DA5E115-886F-3531-BDD5-B1AC4EF11AAF}';
  IID__CvsGraphicFilledBox: TGUID = '{67527C7E-F54B-3CCD-9B86-D29DCA88D7A6}';
  IID__CvsGraphicParallelogram: TGUID = '{B9BC59F2-E341-3FE9-AEC2-C8299E29FFEE}';
  IID__CvsGraphicPolygon: TGUID = '{A92D0551-AA65-34DA-9C31-6C246E1D9A49}';
  IID__CvsJobAnalyzer: TGUID = '{48D9D7C9-0686-3892-8C91-FEDDEF77701D}';
  IID__CvsModbusSettings: TGUID = '{2274E1C7-5D1C-3912-AA83-815EBC41ACBE}';
  IID_IPolygonEditor: TGUID = '{A21A33BD-7F07-4751-9B79-978986610AEA}';
  IID__CvsString: TGUID = '{57AC87B7-8CB9-31D7-9ED1-F0E965D3A083}';
  IID__StringProcessing: TGUID = '{C3A4654A-70DB-3A89-96DF-2B952DFCBE73}';
  IID__StringBuilderProcessing: TGUID = '{B4A2A753-3DB2-3F82-8534-E591010C3FBF}';
  IID__CvsSymbolicTag: TGUID = '{6499C465-7395-38EE-BEB8-2CF77021621C}';
  IID__CvsSymbolicTagCollection: TGUID = '{BF5B2074-DEB8-3A63-85D5-C0A0C1F24D78}';
  IID__CvsDiscreteIOLines: TGUID = '{FC6F4204-4B5E-3D6D-A5C9-9C0086DB2A6E}';
  IID__CvsUniversalIOSettings: TGUID = '{787A43A5-D389-3360-8041-E3ECA5BF78FE}';
  IID__CvsPassFailSettings: TGUID = '{6D16034F-7757-3D30-B236-1FCD37A25E33}';
  IID__CvsHostCollection: TGUID = '{71715B58-C516-3A42-B9BA-AA1D5975FDD6}';
  IID__CvsHost: TGUID = '{ED6DB3C3-77D4-372B-B6F1-53B506A61A9C}';
  IID__CvsHostSensor: TGUID = '{13E6663F-68D4-38A5-9F86-C086032B49B3}';
  IID__CvsNetworkListener: TGUID = '{5BDEBB01-09ED-3A81-9099-CA4EFBE85481}';
  IID__CvsNetworkMonitor: TGUID = '{3473F45C-91E2-33BE-83C1-FF2AFEA34248}';
  IID__CvsNetworkMonitorSubnet: TGUID = '{12CA0420-1A75-3BFB-BDB0-A51DBA47EF98}';
  DIID_ICvsNetworkMonitorEvents: TGUID = '{AE9E35F0-292A-4030-9C5E-8AE5B5C145DB}';
  IID__CvsOcrMaxAutoTune: TGUID = '{E282C93F-E9A1-3167-82E5-89F35C13E4A9}';
  IID__AutoTuneSettings: TGUID = '{85A67E4F-F2F2-3794-8977-C537621C0D83}';
  IID__SpaceSettings: TGUID = '{0D712F72-6CAB-3A3C-9321-B6174BE5FB1D}';
  IID__AutoSegmentResult: TGUID = '{97D777CB-4617-3A1C-A0A6-6CAEF31887A3}';
  IID__AutoTuneRecord: TGUID = '{8CEFEDD1-0325-32E6-8CAA-C44547907B05}';
  IID__AutoTuneException: TGUID = '{84E8E4D9-2902-36D9-A9FC-6E984867DAC7}';
  IID__AutoTuneSessionClosedEventArgs: TGUID = '{12C8989E-3B38-3E66-B5DA-74933E79899B}';
  IID__AutoTuneSessionClosedHandler: TGUID = '{F3ADDE24-E39C-35F6-B5BE-7C8F5134AF22}';
  CLASS_CvsAction: TGUID = '{D19193E6-8BF6-4AE9-94AE-4884525A3F84}';
  IID__CvsCellButton: TGUID = '{2BF38D3C-906F-396B-8C35-E4C86EBD4A72}';
  IID__CvsCellChart: TGUID = '{DD5130E6-8827-3309-8778-2190BC212DCD}';
  IID__CvsCellCheckBox: TGUID = '{D2BA701D-CFEF-3DA4-826C-361DB52EFC97}';
  IID__CvsCellCollection: TGUID = '{FA7CA735-559A-3658-AF28-97D7E502C6FD}';
  IID__CvsCellColorLabel: TGUID = '{267B9840-E087-3A43-9D74-6BEC618BE35E}';
  IID__CvsCellDialog: TGUID = '{35A88803-F5E3-3EA4-95B5-D5BE73DE8898}';
  IID__CvsCellEditCircle: TGUID = '{3B8B571F-FE63-352B-8BD5-FC203F3D659B}';
  IID__CvsCellEditFloat: TGUID = '{F703BE3C-1696-32AB-B178-AF83E783700D}';
  IID__CvsCellEditInt: TGUID = '{5386AD1A-3C03-3D8A-84AA-F7C27001A17C}';
  IID__CvsCellEditLine: TGUID = '{EB32850E-A72E-38FB-A995-34249BC76810}';
  IID__CvsCellEditPoint: TGUID = '{FA263340-7149-3E9F-9E20-570BD35B2D17}';
  IID__CvsCellEditRegion: TGUID = '{10D989A4-5AB9-3DE5-A159-624ED7C7CEB4}';
  IID__CvsCellEditString: TGUID = '{BC72DB91-DD4E-3F98-BCCD-4B1BBBA1B6C0}';
  IID__CvsCellEditAnnulus: TGUID = '{1CC7D80D-9925-35D8-AE4C-16A04AD9E463}';
  IID__CvsCellEmpty: TGUID = '{02001A00-97A2-34BE-8EA5-501C950D2A77}';
  IID__CvsCellError: TGUID = '{A91D5C86-FEFD-311E-A79C-FA761DDCFE1B}';
  IID__CvsCellFloat: TGUID = '{EC343975-D309-3E1C-BE3F-5D536FB83C6B}';
  IID__CvsCellInt: TGUID = '{10C119AA-C63E-36FF-91AA-A77C4BA91FAB}';
  IID__CvsCellLink: TGUID = '{5D9BE2C7-B8C9-3914-B7CB-C65806E8F8A1}';
  IID__CvsCellListBox: TGUID = '{86BA4F3E-9268-3A86-8D44-68418EE7237D}';
  IID__CvsCellMultiStatus: TGUID = '{A7CB1DF4-2B18-3F16-841F-681B78C4C879}';
  IID__CvsCellSelect: TGUID = '{6EC66596-4D7E-3EDA-8487-79ED481896E0}';
  IID__CvsCellStatus: TGUID = '{6D105E29-1EB0-3375-BC50-CB32CE69C366}';
  IID__CvsCellStatusLight: TGUID = '{411D685A-67AD-3624-A94A-D37D92DEFE64}';
  IID__CvsCellWizard: TGUID = '{C8B3640F-7372-3A1C-AA59-6DD29CC6FA27}';
  IID__CvsDiscreteInputLine: TGUID = '{37846D60-72EE-3CFB-AF2B-361662DE3410}';
  IID__CvsInputCollection: TGUID = '{49A657D3-D3D2-353F-A819-9B31EE6E952E}';
  IID__CvsOutputCollection: TGUID = '{740B7E9C-0FBF-3FD0-8898-8F17B3AF992E}';
  IID__CvsDiscreteOutputLine: TGUID = '{E698B9AA-B001-320D-BC83-254CB7587327}';
  IID__CvsExpansionIOSettings: TGUID = '{55789679-C862-3A94-8645-6A348005D300}';
  IID__CvsFile: TGUID = '{13CB3169-81EC-3B04-BE4D-89465F002446}';
  IID__CvsFileCollection: TGUID = '{59975A48-BFBF-3159-BED2-AA16C6FE7A18}';
  IID__CvsFileManager: TGUID = '{B6C002CA-DC0A-3F79-ACBA-B86CB68DFED6}';
  IID__CvsFtpClient: TGUID = '{25DF9E32-6A27-30D7-BC1D-D897B126E115}';
  IID__CvsFtpSettings: TGUID = '{9A4B5A65-EFAF-3E0C-B24D-7FCFCF1D6BD4}';
  IID__CvsGraphic4Side: TGUID = '{DC188909-6498-3594-BE3D-FE499F6BF004}';
  IID__CvsGraphicArc: TGUID = '{8CCC33A2-8B40-3707-9369-86D4BAFBEF3A}';
  IID__CvsGraphicBlob: TGUID = '{B6F1C8B5-662D-30F2-80A8-6F2451374558}';
  IID__CvsGraphicCircle: TGUID = '{06B7B5E9-63F1-375B-8F6D-3A0B3D8CBFBB}';
  IID__CvsGraphicCollection: TGUID = '{EB6FE800-AF5F-38FD-B84C-5E9606D18A0C}';
  IID__CvsGraphicPatternMatch: TGUID = '{08922AB7-BA6C-3C41-B0BD-A5C56E349249}';
  IID__CvsPatternMatchSegment: TGUID = '{01B6BE8E-25E3-3420-A3A3-EEEB6A7A2961}';
  IID__CvsGraphicCross: TGUID = '{C723F6AC-9290-3C76-A898-0DAD31B86DFD}';
  IID__CvsGraphicEdgeProfile: TGUID = '{B3BC696B-9B60-3175-89B0-A7CA174F43B3}';
  IID__CvsGraphicFixture: TGUID = '{57E2E98E-DF90-3491-AB46-0CD215D0B0A3}';
  IID__CvsGraphicHistogram: TGUID = '{69A60F75-610B-3D38-996F-4ED080D0063B}';
  IID__CvsGraphicImage: TGUID = '{A29F245B-AB2A-386B-8E7F-57BCA5BEB97E}';
  IID__CvsGraphicBlobProperties: TGUID = '{D4FD6383-1901-3F66-A9E8-AB13CFE9084D}';
  IID__CvsGraphicLine: TGUID = '{81F6EF11-1064-3CC3-8B2E-88C3C4D1A4CD}';
  IID__CvsGraphicOffset: TGUID = '{21B97C6C-8737-3008-A1D3-DD2DE976FEBE}';
  IID__CvsGraphicPoint: TGUID = '{67EC08CE-1642-3932-996B-9AE54B5D6D49}';
  IID__CvsGraphicPolyline: TGUID = '{D32A3B3A-1DE5-3094-8D04-9901B2422E07}';
  IID__CvsGraphicRegion: TGUID = '{24393BC4-5E11-35E2-8822-1F3B0096E4E6}';
  IID__CvsGraphicText: TGUID = '{C5E456AF-7D37-3A44-AD6A-CE25A92DB9A7}';
  IID__CvsGraphicAnnulus: TGUID = '{41CB8318-8E53-3810-8207-FF772A15ACF5}';
  IID__CvsHostTableCollection: TGUID = '{786467CE-A694-367D-8348-A1113C179602}';
  IID__CvsHostTableEntry: TGUID = '{A672A610-C97A-380C-AD06-CCDC99192EF1}';
  IID__CvsHostVersion: TGUID = '{C9BEE73D-CDA9-3E2A-9D5D-542D85818564}';
  IID__CvsImage: TGUID = '{102F35DA-843F-3773-A830-E2F4298B0AA6}';
  IID__CvsInSight: TGUID = '{24AC313D-8C47-3209-BE20-D3F1B4F6BAA6}';
  IID__CvsJobInfo: TGUID = '{C913BD9F-A2EA-3CF8-84F7-0698A5FE4F46}';
  IID__CvsKernel: TGUID = '{37E445AA-CC97-3AAC-88C0-382A0F564AE7}';
  IID__CvsLicense: TGUID = '{1365B4D2-D8A8-3353-9039-7B0A458B4082}';
  IID__CvsLocalIOSettings: TGUID = '{3226A657-CC6D-34D7-8F7F-E5EB388400DD}';
  IID__CvsNativeModeClient: TGUID = '{79EF7AC7-ED5E-33FF-9F95-72C360E16037}';
  IID__CvsNetworkSettings: TGUID = '{50B9AB3B-2568-3418-9D64-C8821BE4252C}';
  IID__CvsPicture: TGUID = '{A9FCAC72-2F17-38FB-8743-41B8A1D8564A}';
  IID__CvsProcessorConstants: TGUID = '{95B1AA64-472D-3310-82CD-9AAACC607722}';
  IID__CvsResultSet: TGUID = '{9CEF22B9-4FE4-3053-99FA-EFFB0C73F94F}';
  IID_ICvsResultsQueue: TGUID = '{F2D13F4E-DD3C-4451-A529-789730C020F5}';
  IID__CvsSerialSettings: TGUID = '{1E563D32-BD3F-3BFA-A484-034FFF7E0416}';
  IID__CvsSntpCurrentData: TGUID = '{25164D5D-C0F6-3635-95D6-B48A3D3C7E0E}';
  IID__CvsSntpSettings: TGUID = '{DEA76515-4B6D-344B-AE68-59D0A36C7552}';
  IID__CvsTimeDateData: TGUID = '{977B56FC-8513-3602-893F-1C7BE18A5295}';
  IID__CvsTimeZones: TGUID = '{C29E5451-FE22-378F-B7C5-096E27211F51}';
  IID__CvsUserCollection: TGUID = '{C24C0D24-A3CC-3A66-B2E9-2D22FAD711A0}';
  IID__CvsUtility: TGUID = '{70E955A1-469A-3E28-9122-53741341E025}';
  IID__CvsView: TGUID = '{F8F91C76-5F55-33D0-BAB6-82347F515402}';
  IID__CvsStateChangedEventArgs: TGUID = '{CCFFFE15-804E-3FC2-BE38-8ABFD9874F1A}';
  IID__CvsConnectCompletedEventArgs: TGUID = '{E9D641E0-5A5B-3485-AB2E-C3CDCDAE3EE9}';
  IID__CvsInSightMessageEventArgs: TGUID = '{DF03592F-4988-3B71-91DB-8A57DBB86000}';
  IID__CvsLoadCompletedEventArgs: TGUID = '{DC302895-CD8E-3EF6-A3E1-FD6CDFCB4847}';
  IID__CvsSaveCompletedEventArgs: TGUID = '{750D5AF2-DE37-3F44-9BDC-BCF2BB1387BF}';
  DIID_ICvsInSightEvents: TGUID = '{463FA70B-A051-47B1-8A03-E889EC28CE8F}';
  IID__CvsAcceptMessageHandler: TGUID = '{05C68D21-17FE-3028-9255-CFF83EE8A11D}';
  IID__BadSnippetException: TGUID = '{9CF08882-4D11-3683-BC48-BAD12DAA1EC3}';
  IID__Snippet: TGUID = '{480E2AFC-DC3E-3692-A444-1EBD575A26CB}';
  IID__Syslog: TGUID = '{F77E96BA-4F7D-3336-93E3-066F35F4B401}';
  IID__SyslogTraceListener: TGUID = '{2FF29391-A107-3F22-9500-285CF5FB43F9}';
  IID__ArgUpdateHandler_2: TGUID = '{4CEC5DEB-4FBC-376D-B9A9-3B4233F13FCB}';
  CLASS_CvsCell: TGUID = '{1B176340-214B-4094-9F3D-8CE00079E46F}';
  CLASS_CvsCellOcrMaxAutoTune: TGUID = '{D2C920E7-72FF-491A-97CA-2F4201F72F2C}';
  CLASS_CvsCrcResponse: TGUID = '{D59F686D-D978-4A4E-8FA4-7B3C14618987}';
  CLASS_CvsCellString: TGUID = '{2214D5D2-0A3D-4800-9475-5F5D0AF91F9A}';
  CLASS_CvsCellAuthorize: TGUID = '{DE22BD88-C4DE-4700-959D-E2E6861EDF3A}';
  CLASS_CvsCellEditGraphic: TGUID = '{C7A2E86A-53A6-417F-A358-4DC8FAEFC60F}';
  CLASS_CvsCellEditMultiGraphics: TGUID = '{34BA2B02-CE54-439F-A87E-4061975344FE}';
  CLASS_CvsCellReference: TGUID = '{B48BD431-0D0E-4560-BBC8-0E97FB25A285}';
  CLASS_CvsCellReferenceInfo: TGUID = '{DCA3154D-3EE5-4C26-B168-08C25A68A18F}';
  CLASS_CvsCellReferenceTable: TGUID = '{2E79096E-0375-4AE5-92A2-D896A42BB306}';
  CLASS_CvsDefaultRegion: TGUID = '{19A60BB3-6032-4905-8795-03CD492991DC}';
  CLASS_CvsEasyView: TGUID = '{495CB1A3-3E0D-4061-B36B-64DAC5C5E0D6}';
  CLASS_CvsEasyViewItem: TGUID = '{4353ED7F-86F0-4E4E-A940-93981E8EEF52}';
  CLASS_CvsFeatureLicense: TGUID = '{BDDF669E-5AC1-448A-9B98-150E4261403F}';
  CLASS_CvsIspMessageStore: TGUID = '{63B60D0E-5915-4027-AE7B-E43C5D19567F}';
  CLASS_CvsJobServerSettings: TGUID = '{26A5E421-3E9C-4FC1-A403-544EF349BEEB}';
  CLASS_CvsOcrMaxDiagnosticData: TGUID = '{730BF1C3-0711-423A-B2F1-1B6AA49636D2}';
  CLASS_FragmentStatistics: TGUID = '{15ADF929-DC06-4544-B12A-DB398F6546C1}';
  CLASS_CharacterStatistics: TGUID = '{05878B7B-89B9-4E7B-8062-C9D426DF95AA}';
  CLASS_CvsOcrMaxDiagnostics: TGUID = '{CA4EDF1E-D92B-492E-8715-CCFB51BB1367}';
  CLASS_CvsOcrMaxExpressionEditorBase: TGUID = '{0EADA81B-328D-430A-94F7-354B87D9A34C}';
  CLASS_CvsOcrMaxSpreadsheetExpressionEditor: TGUID = '{F6704779-E922-4E3A-BC92-787A3CC50852}';
  CLASS_ArgUpdateHandler: TGUID = '{2A5DE1D1-3100-3283-93F4-AD0130A7B1D5}';
  CLASS_CvsPowerlinkSettings: TGUID = '{232501AD-734B-4D24-A11C-CD96FEF61CA8}';
  CLASS_CvsRemoteSubnetCollection: TGUID = '{DB8CBCD9-DCF1-456E-AC29-1EFC604C22C4}';
  CLASS_CvsRemoteSubnetEntry: TGUID = '{36CE68E7-E048-4E49-9AB9-9734242A1713}';
  CLASS_CvsSensor: TGUID = '{CF6C74BF-F418-4989-9E6D-E35019A12989}';
  CLASS_CvsUser: TGUID = '{11C3C515-4D12-48E5-8508-3C31CA7A9EF4}';
  CLASS_CvsInSightExtensions: TGUID = '{FB850AB2-DE36-48D9-B3CA-F406D488D980}';
  CLASS_CvsSettings: TGUID = '{BAABBBB6-5478-46B0-A695-2F790D081F4F}';
  CLASS_UndoAction: TGUID = '{97CA89AC-DF78-4AF6-8C53-AA65AF020925}';
  CLASS_UndoCellTagsAction: TGUID = '{FC9CDDED-7863-4803-B169-5B560FE6D1D1}';
  CLASS_UndoClearAllAction: TGUID = '{14396B93-80C0-4D55-8731-3553CB03AD80}';
  CLASS_UndoCutAndPasteAction: TGUID = '{8C785FBB-6F57-4FDE-8BCC-3EAF0944ABFB}';
  CLASS_UndoEventArgs: TGUID = '{82648753-BB8A-4490-A8A9-488207BA0922}';
  CLASS_UndoExpressionAction: TGUID = '{FFF233A4-F437-428F-9EBE-05C0760335C8}';
  CLASS_UndoOpcTagEditorAction: TGUID = '{581B6060-0C78-42A5-8C1B-F56EC8DDB591}';
  CLASS_UndoPropertySheetAction: TGUID = '{FE7F0D90-DD5E-48DF-AE52-62C9563BBC0C}';
  CLASS_UndoReferenceRedirectAction: TGUID = '{AA3E8349-892D-4821-9807-10D3FC22DAC8}';
  CLASS_UndoScriptChangeAction: TGUID = '{95711172-4ACC-40A1-9EFE-AE67ABCEF007}';
  CLASS_CdsConnectionChange: TGUID = '{073AAE31-83F0-3454-8236-BED18D1FDB1A}';
  CLASS_CvsGraphic: TGUID = '{128A02A9-8326-4E1F-9F49-18A185A344EA}';
  CLASS_CvsGraphicArrows: TGUID = '{1F438B80-16DD-4155-9761-83AEC8FDD7B9}';
  CLASS_CvsCellEditMaskedRegion: TGUID = '{1305F631-6785-429D-9C2D-CEAF9C0BB777}';
  CLASS_CvsCellEditPolylinePath: TGUID = '{801F3466-FDFB-4D85-82E7-47C87D435162}';
  CLASS_CvsAuditMessageSettings: TGUID = '{DAD1E0D3-7025-4418-9698-8761F9CDFC93}';
  CLASS_CvsCaliperScoringMethod: TGUID = '{994AF413-5671-4565-BC2B-010EADC41A96}';
  CLASS_CvsCellEditComposite: TGUID = '{97D1DA04-FE50-471F-8EE8-5A91AB62A448}';
  CLASS_CvsCellEditPolygon: TGUID = '{54C9D42F-711C-484F-B1F0-CDEF9791D9EE}';
  CLASS_CvsColorMatchEditor: TGUID = '{74B39007-6E88-46C9-9ED0-B0029B3B6C3A}';
  CLASS_CvsColorModel: TGUID = '{CC44A4F3-07CC-42D4-9AAE-621A2A1A7F4F}';
  CLASS_CvsEasyViewHelper: TGUID = '{40C35E33-C736-415C-9676-63E7D11BC6FE}';
  CLASS_CvsCompositeSubregion: TGUID = '{778A5433-3FC4-47A6-B4C6-45F29BB0FCE1}';
  CLASS_CvsGraphicCompositeBase: TGUID = '{93CEB443-71E1-4C21-A3B1-5DEE34402EFB}';
  CLASS_CvsGraphicComposite: TGUID = '{BE38FF12-D7D6-494B-8E14-26236219F195}';
  CLASS_CvsGraphicPolylinePath: TGUID = '{02369A0F-E8B4-4979-B919-CB1C9A4C7B28}';
  CLASS_CvsGraphicMaskedRegion: TGUID = '{DB157F28-8133-4EE1-8249-1A3C08BAFCA6}';
  CLASS_CvsGraphicDefectPoints: TGUID = '{DAF1A1BF-A75E-4829-9A96-952CEAB119CB}';
  CLASS_CvsGraphicEdgePoints: TGUID = '{7787367C-B39C-4D91-8EAD-7ABDFD5F7BDA}';
  CLASS_CvsGraphicExtremePoints: TGUID = '{3E6A4E3B-C833-44DB-94CA-B69B8200DD6D}';
  CLASS_CvsGraphicFilledBox: TGUID = '{3D3D9A24-09D7-4B69-8CDB-645389F7CAF6}';
  CLASS_CvsGraphicParallelogram: TGUID = '{F83418CC-6632-4A98-B77A-310D63A37DEB}';
  CLASS_CvsGraphicPolygon: TGUID = '{3FAA0BB3-EECE-4793-839F-FF5CA390D260}';
  CLASS_CvsJobAnalyzer: TGUID = '{827EC4B1-AFFC-4AC3-8DEA-D4A6E0C0A2A9}';
  CLASS_CvsModbusSettings: TGUID = '{93452607-1CDA-469A-89AC-3687603D3500}';
  CLASS_CvsString: TGUID = '{D2BB59C1-00FB-4948-B727-2CA679BC7865}';
  CLASS_StringProcessing: TGUID = '{16CDAA49-DCA3-4975-B7F0-B9D132263A9F}';
  CLASS_StringBuilderProcessing: TGUID = '{72749894-A087-4B84-8484-DFC5638807D1}';
  CLASS_CvsSymbolicTag: TGUID = '{D6314073-2EE1-408E-8A04-0CDFAD65C6AD}';
  CLASS_CvsSymbolicTagCollection: TGUID = '{7CC7C958-89B4-4375-A862-C10418297999}';
  CLASS_CvsDiscreteIOLines: TGUID = '{1D4F7BA6-E972-4B53-A734-83E9AD9DFD9A}';
  CLASS_CvsUniversalIOSettings: TGUID = '{24443BD7-74EB-4478-98C8-6271A92776B9}';
  CLASS_CvsPassFailSettings: TGUID = '{10026291-B261-4A98-903B-1BA986375076}';
  CLASS_CvsHostCollection: TGUID = '{C09BDAAF-DAC2-44D6-B6E3-86D6D265DE7B}';
  CLASS_CvsHost: TGUID = '{9E184B26-E329-432B-AB18-BB2BC0DBB3FB}';
  CLASS_CvsHostSensor: TGUID = '{81D9712B-4247-4998-81AF-70EB4A05A07B}';
  CLASS_CvsNetworkListener: TGUID = '{6A177146-8B82-4369-B469-D6F028FC5439}';
  CLASS_CvsNetworkMonitor: TGUID = '{6A157A96-8B82-4369-B469-D6F028FC5439}';
  CLASS_CvsNetworkMonitorSubnet: TGUID = '{70A3304E-7D40-4271-9B6E-70CDE2C3791F}';
  CLASS_CvsOcrMaxAutoTune: TGUID = '{2D9012D9-A1A4-401E-8080-A860026A5479}';
  CLASS_AutoTuneSettings: TGUID = '{38CEC04A-B629-4356-9C5F-2DCCA0CB951C}';
  CLASS_SpaceSettings: TGUID = '{1D7548E3-5905-4A25-B576-E26F32195EBF}';
  CLASS_AutoSegmentResult: TGUID = '{31A31D8D-E514-4ED6-B463-549E30F10652}';
  CLASS_AutoTuneRecord: TGUID = '{3EF41308-56C1-4453-8CAD-CBACACD911D4}';
  CLASS_AutoTuneException: TGUID = '{4A263071-CCE3-4531-A1C5-5B0E051C63FC}';
  CLASS_ArgUpdateHandler_2: TGUID = '{7D1E8B0E-6F39-31A3-88A7-46D2E793E6CB}';
  CLASS_AutoTuneSessionClosedEventArgs: TGUID = '{1ADE4E8B-5E1A-4718-990A-5C7D326CA926}';
  CLASS_AutoTuneSessionClosedHandler: TGUID = '{4A172375-D9BA-3489-BD51-31732B533700}';
  CLASS_CvsCellButton: TGUID = '{F2C645A2-F7EF-4F0F-BFAB-0DF2D4D5E206}';
  CLASS_CvsCellChart: TGUID = '{14485781-A8F3-49A9-8566-5CCD4A55329D}';
  CLASS_CvsCellCheckBox: TGUID = '{92C81AAB-7ECE-45F4-9D44-A05624F38371}';
  CLASS_CvsCellCollection: TGUID = '{3F770D1A-3664-41A8-9E4F-157E16BD447C}';
  CLASS_CvsCellColorLabel: TGUID = '{CE5BA849-67DA-4127-81AB-25D49C5A70ED}';
  CLASS_CvsCellDialog: TGUID = '{42ED89DA-2DF7-4D1B-9D1C-10CB9E6E2DC1}';
  CLASS_CvsCellEditCircle: TGUID = '{DE63E3CC-55BA-4A67-954B-54D39B976BBD}';
  CLASS_CvsCellEditFloat: TGUID = '{E9C7961C-4DF1-4376-BB19-91F1B513A443}';
  CLASS_CvsCellEditInt: TGUID = '{045238BA-7D52-4B0C-8263-8D1936B475AD}';
  CLASS_CvsCellEditLine: TGUID = '{907DA2B2-DCD9-42BA-9434-1FE17BD21CEC}';
  CLASS_CvsCellEditPoint: TGUID = '{F05EF55D-DF5B-4837-99C9-A6925297CDD5}';
  CLASS_CvsCellEditRegion: TGUID = '{DDF7D0F0-5F2F-41E1-9110-7BB8E684C019}';
  CLASS_CvsCellEditString: TGUID = '{8B5C6C1E-58D7-40E6-9ADE-4791DE6E922B}';
  CLASS_CvsCellEditAnnulus: TGUID = '{D144B929-319A-45F6-B201-701544B86423}';
  CLASS_CvsCellEmpty: TGUID = '{3E8816C8-C82D-441E-8482-48FEC6F269A1}';
  CLASS_CvsCellError: TGUID = '{15ABD702-4A0E-4972-B68B-95D91AF377F2}';
  CLASS_CvsCellFloat: TGUID = '{16E95F70-38D7-4935-9EBD-76F050E2E6F3}';
  CLASS_CvsCellInt: TGUID = '{0E66844A-8268-42F3-B8D3-48EC428CFACA}';
  CLASS_CvsCellLink: TGUID = '{345A7524-FADE-4B94-A700-51D08A7237C1}';
  CLASS_CvsCellListBox: TGUID = '{464C5B84-6B74-4697-B56C-5E6B6448DF08}';
  CLASS_CvsCellMultiStatus: TGUID = '{1710EA30-6DA2-46CB-B2BA-0852598487EB}';
  CLASS_CvsCellSelect: TGUID = '{3395D44C-901C-41AB-AA07-F3B8BC2AEC24}';
  CLASS_CvsCellStatus: TGUID = '{7EF17819-517E-450B-A4BE-BCA3C39C90B8}';
  CLASS_CvsCellStatusLight: TGUID = '{3B80E64F-A576-4EFA-AD55-60B4583CFB40}';
  CLASS_CvsCellWizard: TGUID = '{CC674FE6-1AA3-40A1-B0C1-ADB3D5CBB52D}';
  CLASS_CvsDiscreteInputLine: TGUID = '{CEDA5B93-7170-495C-864C-6C830B406237}';
  CLASS_CvsInputCollection: TGUID = '{EEBF77A8-995E-4A24-B662-B873997F7B60}';
  CLASS_CvsOutputCollection: TGUID = '{E716FF67-0722-42BC-AB5D-B837FCBEB06E}';
  CLASS_CvsDiscreteOutputLine: TGUID = '{63621090-B568-43CF-8B22-649FB61D5287}';
  CLASS_CvsExpansionIOSettings: TGUID = '{1C74C8E4-7E75-483A-B4CD-43BF351A2B89}';
  CLASS_CvsFile: TGUID = '{5AE8947D-1E72-46E0-825B-B455E0B21AEB}';
  CLASS_CvsFileCollection: TGUID = '{64FB3FA4-83BF-4580-95E9-C6CBBB241908}';
  CLASS_CvsFileManager: TGUID = '{8AD87E17-ADF5-4FAB-A1EB-CA8E8367B290}';
  CLASS_CvsFtpClient: TGUID = '{16B49641-306D-453F-8B33-A4A35D6705FE}';
  CLASS_CvsFtpSettings: TGUID = '{7E812758-6F8E-426B-819F-FB519F05D2DD}';
  CLASS_CvsGraphic4Side: TGUID = '{DF7DD420-0BC5-43D7-A0BB-05C00F1F4557}';
  CLASS_CvsGraphicArc: TGUID = '{2AE9CEA9-E04B-43C5-8C46-CFD1BC5EB43F}';
  CLASS_CvsGraphicBlob: TGUID = '{BFBE0883-1851-465E-A171-124811112D25}';
  CLASS_CvsGraphicCircle: TGUID = '{B5DB91A0-DCC8-4843-992A-68863A564D08}';
  CLASS_CvsGraphicCollection: TGUID = '{4EE82051-2F1C-4DB2-8CEF-97B2B6ADAFA5}';
  CLASS_CvsGraphicPatternMatch: TGUID = '{00CE5845-04DD-43C8-B057-D57E82649436}';
  CLASS_CvsPatternMatchSegment: TGUID = '{4DFF8AEA-EEC5-403B-BF14-39F2D64C84E5}';
  CLASS_CvsGraphicCross: TGUID = '{692FC651-CA36-4012-A395-74D3392D01CB}';
  CLASS_CvsGraphicEdgeProfile: TGUID = '{197479CC-F9A0-4336-BF5F-84A50E4B9FE1}';
  CLASS_CvsGraphicFixture: TGUID = '{F277A0F0-4D66-4187-B441-A88CFA9BF933}';
  CLASS_CvsGraphicHistogram: TGUID = '{94AD5351-556A-47C5-A307-BDF4AE9A761B}';
  CLASS_CvsGraphicImage: TGUID = '{20620046-135A-469A-83A0-C0BB36D5E4C5}';
  CLASS_CvsGraphicBlobProperties: TGUID = '{B88A8AB5-7533-4338-B1F9-39DA97524F9A}';
  CLASS_CvsGraphicLine: TGUID = '{4E8829F5-FC99-405D-9DD8-9BC273322BF7}';
  CLASS_CvsGraphicOffset: TGUID = '{4FC85944-FDC0-475D-9CD9-7FE9B00074DA}';
  CLASS_CvsGraphicPoint: TGUID = '{AA6B1820-2EF1-4671-AA2B-A445D592B7A6}';
  CLASS_CvsGraphicPolyline: TGUID = '{D746A694-2558-47EF-B476-30F420E8101F}';
  CLASS_CvsGraphicRegion: TGUID = '{CE5435A7-D4BE-4DE9-9BFC-D831F612F98F}';
  CLASS_CvsGraphicText: TGUID = '{C39EFCA9-1352-4103-9BF0-393D1B13EBF2}';
  CLASS_CvsGraphicAnnulus: TGUID = '{C95BAB3F-96EE-4A5D-87A5-47D6CA95371E}';
  CLASS_CvsHostTableCollection: TGUID = '{514A7EEA-09A1-430F-B0E6-7A8E34791E71}';
  CLASS_CvsHostTableEntry: TGUID = '{2274D7A5-81A6-447A-99AC-2523447EBF8D}';
  CLASS_CvsHostVersion: TGUID = '{5EEB35FC-CF3A-4528-AF7E-B605E7ED36DE}';
  CLASS_CvsImage: TGUID = '{20945F6C-7D5D-4A11-9929-6E342640F686}';
  CLASS_CvsInSight: TGUID = '{63644DED-0C6F-4702-B495-E47B2BA633E7}';
  CLASS_CvsJobInfo: TGUID = '{4543CF46-1C5A-42AC-BCB0-423A43AC0D37}';
  CLASS_CvsKernel: TGUID = '{4036C970-3402-4584-83A2-1A8B8ED26CF1}';
  CLASS_CvsLicense: TGUID = '{5B80E76B-F138-4167-AEF7-6A21D1ED0874}';
  CLASS_CvsLocalIOSettings: TGUID = '{E0835932-EC25-4F40-8877-1EAE2BA4404C}';
  CLASS_CvsNativeModeClient: TGUID = '{A0402813-B3CD-4E18-919D-9403B4847E41}';
  CLASS_CvsNetworkSettings: TGUID = '{E0C0A7E6-1F9F-498B-965F-2F33620C83C9}';
  CLASS_CvsPicture: TGUID = '{D0B38958-B093-46AF-8DCA-35F7B3EF855A}';
  CLASS_CvsProcessorConstants: TGUID = '{B099078F-5D49-4C3B-B89D-B3691B6CE332}';
  CLASS_CvsResultSet: TGUID = '{96E81209-595D-4CF3-8E7D-93DEC72F5D88}';
  CLASS_CvsSerialSettings: TGUID = '{8B95DA60-CB02-4840-A0AB-46DCDF0FD7AB}';
  CLASS_CvsSntpCurrentData: TGUID = '{777C314B-88CF-48A6-BD2E-9EC97D4E7AA8}';
  CLASS_CvsSntpSettings: TGUID = '{7914B130-0428-4478-A9A9-D7417ED9010E}';
  CLASS_CvsTimeDateData: TGUID = '{999ADA2A-A00F-4950-AA04-CEB88F41CB12}';
  CLASS_CvsTimeZones: TGUID = '{CB143AC0-FEB6-42FD-AAF6-E4EAA7B4EE66}';
  CLASS_CvsUserCollection: TGUID = '{532D39C2-4EF8-4549-B356-0F42A9A68927}';
  CLASS_CvsUtility: TGUID = '{724929C2-1C45-47DD-B7C9-A09425E88098}';
  CLASS_CvsView: TGUID = '{6173688F-71E8-4A4F-B683-411F107FAEEB}';
  CLASS_CvsStateChangedEventArgs: TGUID = '{3AA628EF-0058-41FD-B65F-8572581C50C2}';
  CLASS_CvsConnectCompletedEventArgs: TGUID = '{37E628EF-0058-41FD-B65F-0072581810C2}';
  CLASS_CvsInSightMessageEventArgs: TGUID = '{F63DD11C-BD8A-40E5-8CE3-D91CC3F28174}';
  CLASS_CvsLoadCompletedEventArgs: TGUID = '{D2FE47AC-9E9B-45AE-A7B0-4FF492394487}';
  CLASS_CvsSaveCompletedEventArgs: TGUID = '{1CCFAFDB-0356-46F4-BD4F-605F01169199}';
  CLASS_CvsAcceptMessageHandler: TGUID = '{01425FA6-D711-397B-9F67-ACB2FB41EBEA}';
  CLASS_BadSnippetException: TGUID = '{99073A90-5DAA-4A42-AF0B-4125E50FE2B4}';
  CLASS_Snippet: TGUID = '{5ECB243B-36A9-4D99-815F-B2D389E163B7}';
  CLASS_Syslog: TGUID = '{A6CD7CF5-470C-4B30-ACF6-9039AD05DFD9}';
  CLASS_SyslogTraceListener: TGUID = '{4AB47A28-C6C5-4890-99DE-6D12902BA938}';

// *********************************************************************//
// Declaration of Enumerations defined in Type Library                    
// *********************************************************************//
// Constants for enum InSightCrc
type
  InSightCrc = TOleEnum;
const
  InSightCrc_None = $FFFFFFFF;
  InSightCrc_EasyBuilderOnly = $00000000;
  InSightCrc_LrOnly = $00000001;
  InSightCrc_OcrOnly = $00000002;
  InSightCrc_Limited1 = $00000004;
  InSightCrc_Limited2 = $00000005;
  InSightCrc_Limited3 = $00000006;

// Constants for enum CvsEasyViewItemType
type
  CvsEasyViewItemType = TOleEnum;
const
  CvsEasyViewItemType_Standard = $00000000;
  CvsEasyViewItemType_Label = $00000001;

// Constants for enum CvsHostSubtype
type
  CvsHostSubtype = TOleEnum;
const
  CvsHostSubtype_Unknown = $00000000;
  CvsHostSubtype_InSightGeneric = $00000100;
  CvsHostSubtype_InSight5000Series = $00000101;
  CvsHostSubtype_InSight3400 = $00000102;
  CvsHostSubtype_InSight1700SeriesWaferReader = $00000103;
  CvsHostSubtype_InSightMicroSeries = $00000104;
  CvsHostSubtype_InSightEZSeries = $00000105;
  CvsHostSubtype_InSightAdvantageEngineSeries = $00000106;
  CvsHostSubtype_InSightMicro1000LR = $00000107;
  CvsHostSubtype_InSight500Series = $00000108;
  CvsHostSubtype_InSightHummingBird = $00000108;
  CvsHostSubtype_InSight7000Series = $00000109;
  CvsHostSubtype_InSightReserved1 = $0000010A;
  CvsHostSubtype_InSightReserved2 = $0000010B;
  CvsHostSubtype_InSightABBSeries = $0000010C;
  CvsHostSubtype_InSightEZ700Series = $0000010D;
  CvsHostSubtype_InSightVCSeries = $0000010E;
  CvsHostSubtype_InSightMotoSightSeries = $0000010F;
  CvsHostSubtype_InSight8200Series = $00000112;
  CvsHostSubtype_InSight8400Series = $00000112;
  CvsHostSubtype_InSight8405 = $00000112;
  CvsHostSubtype_InSight2000Series = $00000113;
  CvsHostSubtype_InSightEmulatorGeneric = $00000200;
  CvsHostSubtype_InSightEmulatorPcHost = $00000201;
  CvsHostSubtype_VisionViewGeneric = $00000300;
  CvsHostSubtype_VisionView700 = $00000301;
  CvsHostSubtype_VisionViewVGA = $00000302;
  CvsHostSubtype_VisionViewPC = $00000303;
  CvsHostSubtype_VisionView700A = $00000304;
  CvsHostSubtype_VisionViewCE = $00000310;

// Constants for enum CvsAuthenticationAlgorithms
type
  CvsAuthenticationAlgorithms = TOleEnum;
const
  CvsAuthenticationAlgorithms_None = $00000000;
  CvsAuthenticationAlgorithms_MD5 = $00000001;
  CvsAuthenticationAlgorithms_SHA1 = $00000002;

// Constants for enum CvsEncryptionAlgorithms
type
  CvsEncryptionAlgorithms = TOleEnum;
const
  CvsEncryptionAlgorithms_None = $00000000;
  CvsEncryptionAlgorithms_DES = $00000001;
  CvsEncryptionAlgorithms_TripleDES = $00000002;
  CvsEncryptionAlgorithms_AES = $00000003;
  CvsEncryptionAlgorithms_Blowfish = $00000004;

// Constants for enum CvsProtocols
type
  CvsProtocols = TOleEnum;
const
  CvsProtocols_Any = $00000000;
  CvsProtocols_TCP = $00000001;
  CvsProtocols_UDP = $00000002;
  CvsProtocols_None = $FFFFFFFF;

// Constants for enum UndoEventType
type
  UndoEventType = TOleEnum;
const
  UndoEventType_Reset = $00000000;
  UndoEventType_PushStack = $00000001;
  UndoEventType_PopStack = $00000002;
  UndoEventType_Add = $00000003;
  UndoEventType_Redo = $00000004;
  UndoEventType_Undo = $00000005;

// Constants for enum CaliperScoringMethodType
type
  CaliperScoringMethodType = TOleEnum;
const
  CaliperScoringMethodType_None = $00000000;
  CaliperScoringMethodType_EdgePairSizeDifference = $00000001;
  CaliperScoringMethodType_EdgePairSize = $00000002;
  CaliperScoringMethodType_PositionNormalized = $00000003;
  CaliperScoringMethodType_Position = $00000004;
  CaliperScoringMethodType_Straddle = $00000005;
  CaliperScoringMethodType_Contrast = $00000006;
  CaliperScoringMethodType_FullRegionPositionNormalized = $00000007;
  CaliperScoringMethodType_FullRegionPosition = $00000008;
  CaliperScoringMethodType_EdgePairSizeDifferenceAsymmetric = $00000009;

// Constants for enum ScoringMethodDirection
type
  ScoringMethodDirection = TOleEnum;
const
  ScoringMethodDirection_LowMiddleCutOff = $00000000;
  ScoringMethodDirection_HighOuterCutOff = $00000001;

// Constants for enum EdgeMode
type
  EdgeMode = TOleEnum;
const
  EdgeMode_SingleEdge = $00000001;
  EdgeMode_EdgePair = $00000002;

// Constants for enum ScoringMethodParameter
type
  ScoringMethodParameter = TOleEnum;
const
  ScoringMethodParameter_Xc = $00000000;
  ScoringMethodParameter_X1 = $00000001;
  ScoringMethodParameter_X0 = $00000002;
  ScoringMethodParameter_Xch = $00000003;
  ScoringMethodParameter_X1h = $00000004;
  ScoringMethodParameter_X0h = $00000005;
  ScoringMethodParameter_Y0 = $00000006;
  ScoringMethodParameter_Y1 = $00000007;
  ScoringMethodParameter_Y0h = $00000008;
  ScoringMethodParameter_Y1h = $00000009;

// Constants for enum CvsCompositeOperation
type
  CvsCompositeOperation = TOleEnum;
const
  CvsCompositeOperation_Invalid = $FFFFFFFF;
  CvsCompositeOperation_Subtract = $00000000;
  CvsCompositeOperation_Add = $00000001;

// Constants for enum CvsCompositeShapeType
type
  CvsCompositeShapeType = TOleEnum;
const
  CvsCompositeShapeType_None = $00000000;
  CvsCompositeShapeType_RegionType = $00000001;
  CvsCompositeShapeType_PolygonType = $00000002;
  CvsCompositeShapeType_CircleType = $00000003;
  CvsCompositeShapeType_AnnulusType = $00000004;

// Constants for enum CvsSupportedFeatures
type
  CvsSupportedFeatures = TOleEnum;
const
  CvsSupportedFeatures_None = $00000000;
  CvsSupportedFeatures_OPC = $00000001;
  CvsSupportedFeatures_AutoExposure = $00000002;
  CvsSupportedFeatures_MasterSlave = $00000004;
  CvsSupportedFeatures_Terminal = $00000008;

// Constants for enum CvsNetworkMonitorScope
type
  CvsNetworkMonitorScope = TOleEnum;
const
  CvsNetworkMonitorScope_None = $00000000;
  CvsNetworkMonitorScope_Version3 = $00000001;
  CvsNetworkMonitorScope_Version4 = $00000002;
  CvsNetworkMonitorScope_VersionMask = $00000003;
  CvsNetworkMonitorScope_InSubnet = $00000100;
  CvsNetworkMonitorScope_OutOfSubnet = $00000200;
  CvsNetworkMonitorScope_RemoteSubnets = $00000400;
  CvsNetworkMonitorScope_LocationMask = $00000700;
  CvsNetworkMonitorScope_UnsupportedDevices = $00010000;

// Constants for enum CvsNetworkMonitorCommand
type
  CvsNetworkMonitorCommand = TOleEnum;
const
  CvsNetworkMonitorCommand_IPRequest = $00000001;
  CvsNetworkMonitorCommand_SetNetwork = $00000002;
  CvsNetworkMonitorCommand_FactoryReset = $00000003;
  CvsNetworkMonitorCommand_Flash = $00000004;
  CvsNetworkMonitorCommand_Hello = $00000005;

// Constants for enum CogNamerErrorCode
type
  CogNamerErrorCode = TOleEnum;
const
  CogNamerErrorCode_Success = $00000000;
  CogNamerErrorCode_Failed = $00000001;
  CogNamerErrorCode_ErrorUnsupportedCommand = $00000002;
  CogNamerErrorCode_ErrorInvalidUsername = $00000003;
  CogNamerErrorCode_ErrorInvalidPassword = $00000004;
  CogNamerErrorCode_ErrorInsufficientPrivilege = $00000005;
  CogNamerErrorCode_ErrorMissingRequiredRecord = $00000006;
  CogNamerErrorCode_ErrorInvalidNetworkSettings = $00000007;
  CogNamerErrorCode_ErrorOnlySupportedDuringIpRequest = $00000008;
  CogNamerErrorCode_ErrorNonExistantRecord = $00000009;
  CogNamerErrorCode_ErrorQueryCacheRecordTruncated = $00000040;

// Constants for enum LockableParameter
type
  LockableParameter = TOleEnum;
const
  LockableParameter_EnablePolarity = $00000000;
  LockableParameter_EnableAngleHalfRange = $00000001;
  LockableParameter_EnableSkewHalfRange = $00000002;
  LockableParameter_EnableNormalizationMode = $00000003;
  LockableParameter_EnableUseStrokeWidthFilter = $00000004;
  LockableParameter_EnableForegroundThresholdFrac = $00000005;
  LockableParameter_EnableIgnoreBorderFragments = $00000006;
  LockableParameter_EnableCharacterFragmentMinNumPels = $00000007;
  LockableParameter_EnableCharacterFragmentMinXOverlap = $00000008;
  LockableParameter_EnableCharacterFragmentContrastThreshold = $00000009;
  LockableParameter_EnableCharacterMinNumPels = $0000000A;
  LockableParameter_EnableCharacterMinWidth = $0000000B;
  LockableParameter_EnableCharacterMinHeight = $0000000C;
  LockableParameter_EnableCharacterFragmentMaxDistanceToMainLine = $0000000D;
  LockableParameter_EnableCharacterFragmentMergeMode = $0000000E;
  LockableParameter_EnableMaxIntracharacterGap = $0000000F;
  LockableParameter_EnableMinIntercharacterGap = $00000010;
  LockableParameter_EnableCharacterUseMaxHeight = $00000011;
  LockableParameter_EnableCharacterMaxHeight = $00000012;
  LockableParameter_EnableCharacterUseMaxWidth = $00000013;
  LockableParameter_EnableCharacterMaxWidth = $00000014;
  LockableParameter_EnableCharacterUseMinAspect = $00000015;
  LockableParameter_EnableCharacterMinAspect = $00000016;
  LockableParameter_EnablePitchMetric = $00000017;
  LockableParameter_EnablePitchType = $00000018;
  LockableParameter_EnableWidthType = $00000019;
  LockableParameter_EnableMinPitch = $0000001A;
  LockableParameter_EnableAnalysisMode = $0000001B;
  LockableParameter_EnableSpaceInsertMode = $0000001C;
  LockableParameter_EnableSpaceMinWidth = $0000001D;
  LockableParameter_EnableSpaceMaxWidth = $0000001E;
  LockableParameter_PARAMETER_COUNT = $0000001F;

// Constants for enum ErrorCodes
type
  ErrorCodes = TOleEnum;
const
  ErrorCodes_ManualTuneWarning = $0112A88B;
  ErrorCodes_DuplicateRecordWarning = $0112A88F;
  ErrorCodes_OperationCancelled = $0112A88D;

// Constants for enum CvsCellIPProtection
type
  CvsCellIPProtection = TOleEnum;
const
  CvsCellIPProtection_None = $00000000;
  CvsCellIPProtection_Licensed = $00000001;
  CvsCellIPProtection_TemporaryEdit = $00000002;
  CvsCellIPProtection_Unlicensed = $00000003;

// Constants for enum CvsCellDataType
type
  CvsCellDataType = TOleEnum;
const
  CvsCellDataType_Unknown = $FFFFFFFF;
  CvsCellDataType_Integer = $00000000;
  CvsCellDataType_FloatingPoint = $00000001;
  CvsCellDataType_String = $00000002;
  CvsCellDataType_Action = $00000003;
  CvsCellDataType_Chart = $00000004;

// Constants for enum CvsCellProperty
type
  CvsCellProperty = TOleEnum;
const
  CvsCellProperty_None = $00000000;
  CvsCellProperty_EditPassword = $00000001;
  CvsCellProperty_AllPropertiesMask = $00000001;

// Constants for enum CvsEditFlags
type
  CvsEditFlags = TOleEnum;
const
  CvsEditFlags_None = $00000000;
  CvsEditFlags_Location = $00000001;
  CvsEditFlags_Size = $00000002;
  CvsEditFlags_Point0 = $00000001;
  CvsEditFlags_Point1 = $00000002;
  CvsEditFlags_Rotation = $00000004;
  CvsEditFlags_Bend = $00000008;
  CvsEditFlags_AddItem = $00000040;
  CvsEditFlags_DeleteItem = $00000020;
  CvsEditFlags_EditItem = $00000100;
  CvsEditFlags_Corner0 = $00000008;
  CvsEditFlags_Corner1 = $00000010;
  CvsEditFlags_Corner2 = $00000020;
  CvsEditFlags_Corner3 = $00000040;
  CvsEditFlags_Skew = $00000008;
  CvsEditFlags_All = $000001FF;

// Constants for enum CvsInputType
type
  CvsInputType = TOleEnum;
const
  CvsInputType_UserData = $00000000;
  CvsInputType_EventTrigger = $00000001;
  CvsInputType_JobIdNumber = $00000002;
  CvsInputType_OnlineOffline = $00000003;
  CvsInputType_AcquisitionTrigger = $00000004;
  CvsInputType_JobLoadSwitch = $00000005;
  CvsInputType_TrackingPulse = $00000006;
  CvsInputType_Encoder = $00000007;
  CvsInputType_SerialRX = $00000008;

// Constants for enum CvsInputSignal
type
  CvsInputSignal = TOleEnum;
const
  CvsInputSignal_RisingEdge = $00000000;
  CvsInputSignal_FallingEdge = $00000001;
  CvsInputSignal_BothEdges = $00000002;

// Constants for enum CvsOutputType
type
  CvsOutputType = TOleEnum;
const
  CvsOutputType_Programmed = $00000000;
  CvsOutputType_High = $00000001;
  CvsOutputType_Low = $00000002;
  CvsOutputType_AcquisitionStart = $00000003;
  CvsOutputType_AcquisitionEnd = $00000004;
  CvsOutputType_JobCompleted = $00000005;
  CvsOutputType_SystemBusy = $00000006;
  CvsOutputType_JobLoadOK = $00000007;
  CvsOutputType_JobLoadFail = $00000008;
  CvsOutputType_MissedAcquisition = $00000009;
  CvsOutputType_TrackingOverrun = $0000000A;
  CvsOutputType_TrackingQueueFull = $0000000B;
  CvsOutputType_OnlineOffline = $0000000C;
  CvsOutputType_Strobe = $0000000D;
  CvsOutputType_IOModuleStandby = $4000000D;
  CvsOutputType_LightControl = $0000000E;
  CvsOutputType_Pass = $0000000F;
  CvsOutputType_Fail = $00000010;
  CvsOutputType_Lifeline = $00000011;
  CvsOutputType_Waveform = $00000012;
  CvsOutputType_Encoder = $00000013;
  CvsOutputType_SerialTransmit = $00000014;
  CvsOutputType_SynchronizerComplete = $00000015;
  CvsOutputType_SynchronizerFail = $00000016;

// Constants for enum CvsOutputListType
type
  CvsOutputListType = TOleEnum;
const
  CvsOutputListType_Unknown = $FFFFFFFE;
  CvsOutputListType_Unsupported = $FFFFFFFF;
  CvsOutputListType_GroupA = $00000000;
  CvsOutputListType_GroupBIncludingLightControl = $00000001;
  CvsOutputListType_GroupBExceptStrobe = $00000003;
  CvsOutputListType_GroupB = $00000004;
  CvsOutputListType_GroupBIncludingLifeline = $00000005;
  CvsOutputListType_EncoderSupport = $00000006;
  CvsOutputListType_SerialSupport = $00000007;
  CvsOutputListType_SynchronizerSupport = $00000008;
  CvsOutputListType_List1A = $00000000;
  CvsOutputListType_List1B = $00000001;
  CvsOutputListType_List1 = $00000003;
  CvsOutputListType_List1BNoLightControl = $00000004;

// Constants for enum CvsStrobeStartPositionType
type
  CvsStrobeStartPositionType = TOleEnum;
const
  CvsStrobeStartPositionType_AcquisitionStart = $00000000;
  CvsStrobeStartPositionType_CameraTrigger = $00000001;
  CvsStrobeStartPositionType_AllRowsExposed = $00000002;

// Constants for enum CvsOutputDelayTypeFlags
type
  CvsOutputDelayTypeFlags = TOleEnum;
const
  CvsOutputDelayTypeFlags_AcquisitionCount = $00000000;
  CvsOutputDelayTypeFlags_TimeAfterTrigger = $10000000;
  CvsOutputDelayTypeFlags_EncoderCount = $20000000;

// Constants for enum CvsInSightJobEncryption
type
  CvsInSightJobEncryption = TOleEnum;
const
  CvsInSightJobEncryption_Legacy = $00000000;
  CvsInSightJobEncryption_Fast = $00000001;

// Constants for enum CvsDisplayable4SideStyle
type
  CvsDisplayable4SideStyle = TOleEnum;
const
  CvsDisplayable4SideStyle_DataMatrix = $00000000;
  CvsDisplayable4SideStyle_SolidLines = $00000001;

// Constants for enum CvsGraphicBlobDirection
type
  CvsGraphicBlobDirection = TOleEnum;
const
  CvsGraphicBlobDirection_ChainNW = $00000000;
  CvsGraphicBlobDirection_ChainN = $00000001;
  CvsGraphicBlobDirection_ChainNE = $00000002;
  CvsGraphicBlobDirection_ChainE = $00000003;
  CvsGraphicBlobDirection_ChainSE = $00000004;
  CvsGraphicBlobDirection_ChainS = $00000005;
  CvsGraphicBlobDirection_ChainSW = $00000006;
  CvsGraphicBlobDirection_ChainW = $00000007;

// Constants for enum CvsGraphicLineAdornment
type
  CvsGraphicLineAdornment = TOleEnum;
const
  CvsGraphicLineAdornment_None = $00000000;
  CvsGraphicLineAdornment_Arrowhead = $00000001;

// Constants for enum CvsHostType
type
  CvsHostType = TOleEnum;
const
  CvsHostType_Server = $00000000;
  CvsHostType_Unknown = $00000001;
  CvsHostType_HostTableEntry = $00000002;
  CvsHostType_InSightEmulator = $00000003;
  CvsHostType_InSightSensor = $00000004;
  CvsHostType_Browser = $00000005;
  CvsHostType_PC = $00000003;

// Constants for enum CvsImageOrientation
type
  CvsImageOrientation = TOleEnum;
const
  CvsImageOrientation_Normal = $00000000;
  CvsImageOrientation_RotateCounterclockwise90 = $0000005A;
  CvsImageOrientation_Rotate180 = $000000B4;
  CvsImageOrientation_RotateClockwise90 = $0000010E;

// Constants for enum CvsInSightError
type
  CvsInSightError = TOleEnum;
const
  CvsInSightError_None = $00000000;
  CvsInSightError_NetworkException = $80040000;
  CvsInSightError_InvalidLogonException = $80040001;
  CvsInSightError_SensorAlreadyConnectedException = $80040002;
  CvsInSightError_InSightLockedException = $80040003;
  CvsInSightError_NativeModeResponseException = $80040004;
  CvsInSightError_NativeModeTimeoutException = $80040005;
  CvsInSightError_FileNotFoundException = $80040006;
  CvsInSightError_FileOperationFailedException = $80040007;
  CvsInSightError_NotConnectedException = $80040008;
  CvsInSightError_NotOfflineException = $80040009;
  CvsInSightError_LiveAcquisitionException = $8004000A;
  CvsInSightError_ExpressionException = $8004000B;
  CvsInSightError_CellProtectionException = $8004000C;
  CvsInSightError_CdsLogonException = $8004000D;
  CvsInSightError_InvalidTimeException = $8004000E;
  CvsInSightError_CdsCertificateException = $8004000F;
  CvsInSightError_CdsServerDownException = $80040010;
  CvsInSightError_CdsNotAuthorizedException = $80040011;
  CvsInSightError_CdsFaultToleranceCacheException = $80040012;
  CvsInSightError_CdsCustomPermissionException = $80040013;

// Constants for enum CvsInSightState
type
  CvsInSightState = TOleEnum;
const
  CvsInSightState_NotConnected = $00000000;
  CvsInSightState_Offline = $00000001;
  CvsInSightState_Online = $00000002;

// Constants for enum CvsInSightStateChangedReason
type
  CvsInSightStateChangedReason = TOleEnum;
const
  CvsInSightStateChangedReason_Normal = $00000000;
  CvsInSightStateChangedReason_Timeout = $00000001;
  CvsInSightStateChangedReason_Restart = $00000002;

// Constants for enum CvsInSightLanguage
type
  CvsInSightLanguage = TOleEnum;
const
  CvsInSightLanguage_English = $00000000;
  CvsInSightLanguage_French = $00000001;
  CvsInSightLanguage_Japanese = $00000002;
  CvsInSightLanguage_German = $00000003;
  CvsInSightLanguage_Spanish = $00000004;
  CvsInSightLanguage_Italian = $00000005;
  CvsInSightLanguage_JapaneseSJIS2 = $00000006;
  CvsInSightLanguage_SimplifiedChinese = $00000007;
  CvsInSightLanguage_TraditionalChinese = $00000008;
  CvsInSightLanguage_Korean = $00000009;

// Constants for enum CvsInSightSecurityAccess
type
  CvsInSightSecurityAccess = TOleEnum;
const
  CvsInSightSecurityAccess_None = $FFFFFFFF;
  CvsInSightSecurityAccess_Full = $00000000;
  CvsInSightSecurityAccess_Protected = $00000001;
  CvsInSightSecurityAccess_Locked = $00000002;

// Constants for enum CvsEipWatchdogTimeoutActions
type
  CvsEipWatchdogTimeoutActions = TOleEnum;
const
  CvsEipWatchdogTimeoutActions_TransitionToTimedOut = $00000000;
  CvsEipWatchdogTimeoutActions_AutoDelete = $00000001;

// Constants for enum CvsProtocolServices
type
  CvsProtocolServices = TOleEnum;
const
  CvsProtocolServices_None = $00000000;
  CvsProtocolServices_EipService = $00000002;
  CvsProtocolServices_ProfiNetService = $00000004;
  CvsProtocolServices_Modbus = $00000001;
  CvsProtocolServices_PTP = $00000008;
  CvsProtocolServices_MCProtocolScanner = $00000010;
  CvsProtocolServices_Powerlink = $00000020;
  CvsProtocolServices_IQSS = $00000040;
  CvsProtocolServices_All = $0000007F;

// Constants for enum CvsNetworkLinkModes
type
  CvsNetworkLinkModes = TOleEnum;
const
  CvsNetworkLinkModes_AutoNegotiation = $00000000;
  CvsNetworkLinkModes_Force10Half = $00000064;
  CvsNetworkLinkModes_Force10Full = $00000065;
  CvsNetworkLinkModes_Force100Half = $00000066;
  CvsNetworkLinkModes_Force100Full = $00000067;
  CvsNetworkLinkModes_Force1000Full = $00000068;
  CvsNetworkLinkModes_Unknown = $FFFFFFFF;

// Constants for enum CvsStatusLevel
type
  CvsStatusLevel = TOleEnum;
const
  CvsStatusLevel_None = $00000000;
  CvsStatusLevel_Pass = $00000001;
  CvsStatusLevel_Fail = $00000002;
  CvsStatusLevel_Warn = $00000003;

// Constants for enum CvsResultsQueueEventType
type
  CvsResultsQueueEventType = TOleEnum;
const
  CvsResultsQueueEventType_StatusChanged = $00000000;
  CvsResultsQueueEventType_QueuedResultsChanged = $00000001;
  CvsResultsQueueEventType_SlotRequestComplete = $00000002;
  CvsResultsQueueEventType_SlotRequestFailed = $00000003;
  CvsResultsQueueEventType_SettingsChanged = $00000004;
  CvsResultsQueueEventType_DeleteResultComplete = $00000005;
  CvsResultsQueueEventType_ClearQueueComplete = $00000006;

// Constants for enum CvsBaudRate
type
  CvsBaudRate = TOleEnum;
const
  CvsBaudRate_Baud1200 = $000004B0;
  CvsBaudRate_Baud2400 = $00000960;
  CvsBaudRate_Baud4800 = $000012C0;
  CvsBaudRate_Baud9600 = $00002580;
  CvsBaudRate_Baud19200 = $00004B00;
  CvsBaudRate_Baud38400 = $00009600;
  CvsBaudRate_Baud57600 = $0000E100;
  CvsBaudRate_Baud115200 = $0001C200;

// Constants for enum CvsParity
type
  CvsParity = TOleEnum;
const
  CvsParity_None = $00000100;
  CvsParity_Odd = $00000200;
  CvsParity_Even = $00000400;

// Constants for enum CvsDataBits
type
  CvsDataBits = TOleEnum;
const
  CvsDataBits_Bits7 = $00000004;
  CvsDataBits_Bits8 = $00000008;

// Constants for enum CvsStopBits
type
  CvsStopBits = TOleEnum;
const
  CvsStopBits_Bits1 = $00000001;
  CvsStopBits_Bits2 = $00000004;

// Constants for enum CvsHandshake
type
  CvsHandshake = TOleEnum;
const
  CvsHandshake_None = $00000000;
  CvsHandshake_XonXoff = $00000002;
  CvsHandshake_Hardware = $00000001;

// Constants for enum CvsMode
type
  CvsMode = TOleEnum;
const
  CvsMode_Unused = $FFFFFFFF;
  CvsMode_None = $00000000;
  CvsMode_Text = $00000006;
  CvsMode_Native = $00000007;
  CvsMode_DeviceNet = $00000008;
  CvsMode_Motoman = $00000009;
  CvsMode_Kuka = $0000000A;

// Constants for enum CvsSupportedTools
type
  CvsSupportedTools = TOleEnum;
const
  CvsSupportedTools_Blob = $00000001;
  CvsSupportedTools_Edge = $00000002;
  CvsSupportedTools_Histogram = $00000003;
  CvsSupportedTools_Symbol = $00000004;
  CvsSupportedTools_Wafer = $00000005;
  CvsSupportedTools_Barcode = $00000006;
  CvsSupportedTools_CompareImage = $00000007;
  CvsSupportedTools_Image = $00000008;
  CvsSupportedTools_PatFind = $00000009;
  CvsSupportedTools_Transforms = $0000000A;
  CvsSupportedTools_Geometry = $0000000B;
  CvsSupportedTools_OCV = $0000000C;
  CvsSupportedTools_Color = $0000000D;
  CvsSupportedTools_CvtIdCode = $0000000E;
  CvsSupportedTools_DefectInspect = $00000011;
  CvsSupportedTools_Caliper = $00000012;
  CvsSupportedTools_CalibrateGrid = $00000013;
  CvsSupportedTools_CvtPatmax = $00000021;
  CvsSupportedTools_CvtIdVerify = $00000022;
  CvsSupportedTools_WaferOCR = $00000024;
  CvsSupportedTools_WaferBarcode = $00000025;
  CvsSupportedTools_Wafer2D = $00000026;
  CvsSupportedTools_Reserved1 = $00000031;
  CvsSupportedTools_NotchMax = $00000032;
  CvsSupportedTools_EasyBuilderOnly = $00000033;
  CvsSupportedTools_SlaveOnly = $00000034;
  CvsSupportedTools_FeatureFamily0 = $0000003E;
  CvsSupportedTools_FeatureFamily1 = $0000003F;
  CvsSupportedTools_FeatureFamily2 = $00000040;
  CvsSupportedTools_FeatureFamily3 = $00000041;
  CvsSupportedTools_FeatureFamily4 = $00000042;
  CvsSupportedTools_FFPPassFailOnly = $00000044;
  CvsSupportedTools_LiteUI = $00000045;

// Constants for enum CvsInSightFeatureFamily
type
  CvsInSightFeatureFamily = TOleEnum;
const
  CvsInSightFeatureFamily_None = $00000000;
  CvsInSightFeatureFamily_LR = $00000001;
  CvsInSightFeatureFamily_OcrOnly = $00000002;
  CvsInSightFeatureFamily_Limited1 = $00000004;
  CvsInSightFeatureFamily_Limited2 = $00000005;
  CvsInSightFeatureFamily_Limited3 = $00000006;

// Constants for enum CvsImageResolution
type
  CvsImageResolution = TOleEnum;
const
  CvsImageResolution_Default = $00000000;
  CvsImageResolution_Full = $00000001;
  CvsImageResolution_Half = $00000002;
  CvsImageResolution_Quarter = $00000004;

// Constants for enum CvsIOModule
type
  CvsIOModule = TOleEnum;
const
  CvsIOModule_CIO_Expansion = $FFFFFFFE;
  CvsIOModule_CIO_None = $FFFFFFFF;
  CvsIOModule_CIO_Standard = $00000000;
  CvsIOModule_CIO_1450 = $00000001;
  CvsIOModule_CIO_1460 = $00000002;
  CvsIOModule_CIO_1400 = $00000003;
  CvsIOModule_WAGO_750_341 = $00000004;
  CvsIOModule_CIO_Micro = $00000005;
  CvsIOModule_CIO_Micro_CC = $00000006;

// Constants for enum CvsColorBitDepth
type
  CvsColorBitDepth = TOleEnum;
const
  CvsColorBitDepth_D16 = $00000000;
  CvsColorBitDepth_D24 = $00000001;

// Constants for enum CvsInSightMessageId
type
  CvsInSightMessageId = TOleEnum;
const
  CvsInSightMessageId_None = $00000000;
  CvsInSightMessageId_TooManyDialogsError = $000007AB;
  CvsInSightMessageId_LockedJobError = $000007AE;
  CvsInSightMessageId_FileLoadError = $000007AF;
  CvsInSightMessageId_ExpressionSyntaxError = $000007B1;
  CvsInSightMessageId_ExpressionSizeError = $000007B2;
  CvsInSightMessageId_ExpressionComplexityError = $000007B3;
  CvsInSightMessageId_MemoryAllocationError = $000007B4;
  CvsInSightMessageId_NotAllowedInDialogError = $000007B6;
  CvsInSightMessageId_MaxCellsError = $000007B7;
  CvsInSightMessageId_MaxReferenceDepthError = $000007B8;
  CvsInSightMessageId_CircularReferenceError = $000007B9;
  CvsInSightMessageId_FileDeleteError = $000007BA;
  CvsInSightMessageId_FileSaveError = $000007BB;
  CvsInSightMessageId_SaveMemoryError = $000007BC;
  CvsInSightMessageId_VersionLoadError = $000007BD;
  CvsInSightMessageId_JobLanguageMismatch = $000007BE;
  CvsInSightMessageId_JobContentsError = $000007BF;
  CvsInSightMessageId_JobLoadError = $000007C0;
  CvsInSightMessageId_LockedJobLoadError = $000007C1;
  CvsInSightMessageId_ReadPermissionsError = $000007C2;
  CvsInSightMessageId_LockedJobSaveWarning = $000007C3;
  CvsInSightMessageId_LockedJobSaveWarning2 = $000007C4;
  CvsInSightMessageId_PermissionDeniedError = $000007C5;
  CvsInSightMessageId_MiscError = $000007C9;
  CvsInSightMessageId_LowMemoryError = $000007CA;
  CvsInSightMessageId_BlankRowsError = $000007CB;

// Constants for enum OutputNameListMode
type
  OutputNameListMode = TOleEnum;
const
  OutputNameListMode_Default = $00000000;
  OutputNameListMode_OpcNames = $00000001;
  OutputNameListMode_NonOpcNames = $00000002;

type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary                    
// *********************************************************************//
  _CvsCell = interface;
  _CvsCellDisp = dispinterface;
  _CvsCellOcrMaxAutoTune = interface;
  _CvsCellOcrMaxAutoTuneDisp = dispinterface;
  ICvsAction = interface;
  ICvsActionEvents = dispinterface;
  ICvsActionObject = interface;
  ICvsActionObjectDisp = dispinterface;
  ICvsActionToggle = interface;
  _CvsCrcResponse = interface;
  _CvsCrcResponseDisp = dispinterface;
  _CvsCellString = interface;
  _CvsCellStringDisp = dispinterface;
  _CvsCellAuthorize = interface;
  _CvsCellAuthorizeDisp = dispinterface;
  _CvsCellEditGraphic = interface;
  _CvsCellEditGraphicDisp = dispinterface;
  _CvsCellEditMultiGraphics = interface;
  _CvsCellEditMultiGraphicsDisp = dispinterface;
  _CvsCellReference = interface;
  _CvsCellReferenceDisp = dispinterface;
  _CvsCellReferenceInfo = interface;
  _CvsCellReferenceInfoDisp = dispinterface;
  _CvsCellReferenceTable = interface;
  _CvsCellReferenceTableDisp = dispinterface;
  _CvsDefaultRegion = interface;
  _CvsDefaultRegionDisp = dispinterface;
  _CvsEasyView = interface;
  _CvsEasyViewDisp = dispinterface;
  _CvsEasyViewItem = interface;
  _CvsEasyViewItemDisp = dispinterface;
  _CvsFeatureLicense = interface;
  _CvsFeatureLicenseDisp = dispinterface;
  _CvsIspMessageStore = interface;
  _CvsIspMessageStoreDisp = dispinterface;
  _CvsJobServerSettings = interface;
  _CvsJobServerSettingsDisp = dispinterface;
  _CvsOcrMaxDiagnosticData = interface;
  _CvsOcrMaxDiagnosticDataDisp = dispinterface;
  _FragmentStatistics = interface;
  _FragmentStatisticsDisp = dispinterface;
  _CharacterStatistics = interface;
  _CharacterStatisticsDisp = dispinterface;
  _CvsOcrMaxDiagnostics = interface;
  _CvsOcrMaxDiagnosticsDisp = dispinterface;
  ICvsOcrMaxExpressionEditor = interface;
  ICvsOcrMaxExpressionEditorDisp = dispinterface;
  _CvsOcrMaxExpressionEditorBase = interface;
  _CvsOcrMaxExpressionEditorBaseDisp = dispinterface;
  _CvsOcrMaxSpreadsheetExpressionEditor = interface;
  _CvsOcrMaxSpreadsheetExpressionEditorDisp = dispinterface;
  _ArgUpdateHandler = interface;
  _ArgUpdateHandlerDisp = dispinterface;
  _CvsPowerlinkSettings = interface;
  _CvsPowerlinkSettingsDisp = dispinterface;
  _CvsRemoteSubnetCollection = interface;
  _CvsRemoteSubnetCollectionDisp = dispinterface;
  _CvsRemoteSubnetEntry = interface;
  _CvsRemoteSubnetEntryDisp = dispinterface;
  _CvsSensor = interface;
  _CvsSensorDisp = dispinterface;
  _CvsUser = interface;
  _CvsUserDisp = dispinterface;
  _CvsInSightExtensions = interface;
  _CvsInSightExtensionsDisp = dispinterface;
  _CvsSettings = interface;
  _CvsSettingsDisp = dispinterface;
  _UndoAction = interface;
  _UndoActionDisp = dispinterface;
  _UndoCellTagsAction = interface;
  _UndoCellTagsActionDisp = dispinterface;
  _UndoClearAllAction = interface;
  _UndoClearAllActionDisp = dispinterface;
  _UndoCutAndPasteAction = interface;
  _UndoCutAndPasteActionDisp = dispinterface;
  _UndoEventArgs = interface;
  _UndoEventArgsDisp = dispinterface;
  _UndoExpressionAction = interface;
  _UndoExpressionActionDisp = dispinterface;
  _UndoOpcTagEditorAction = interface;
  _UndoOpcTagEditorActionDisp = dispinterface;
  _UndoPropertySheetAction = interface;
  _UndoPropertySheetActionDisp = dispinterface;
  _UndoReferenceRedirectAction = interface;
  _UndoReferenceRedirectActionDisp = dispinterface;
  _UndoScriptChangeAction = interface;
  _UndoScriptChangeActionDisp = dispinterface;
  _CdsConnectionChange = interface;
  _CdsConnectionChangeDisp = dispinterface;
  _CvsGraphic = interface;
  _CvsGraphicDisp = dispinterface;
  _CvsGraphicArrows = interface;
  _CvsGraphicArrowsDisp = dispinterface;
  _CvsCellEditMaskedRegion = interface;
  _CvsCellEditMaskedRegionDisp = dispinterface;
  _CvsCellEditPolylinePath = interface;
  _CvsCellEditPolylinePathDisp = dispinterface;
  _CvsAuditMessageSettings = interface;
  _CvsAuditMessageSettingsDisp = dispinterface;
  ICaliperEditor = interface;
  ICaliperEditorDisp = dispinterface;
  _CvsCaliperScoringMethod = interface;
  _CvsCaliperScoringMethodDisp = dispinterface;
  _CvsCellEditComposite = interface;
  _CvsCellEditCompositeDisp = dispinterface;
  _CvsCellEditPolygon = interface;
  _CvsCellEditPolygonDisp = dispinterface;
  _CvsColorMatchEditor = interface;
  _CvsColorMatchEditorDisp = dispinterface;
  _CvsColorModel = interface;
  _CvsColorModelDisp = dispinterface;
  _CvsEasyViewHelper = interface;
  _CvsEasyViewHelperDisp = dispinterface;
  _CvsCompositeSubregion = interface;
  _CvsCompositeSubregionDisp = dispinterface;
  ICvsEditableGraphic = interface;
  ICvsEditableGraphicDisp = dispinterface;
  _CvsGraphicCompositeBase = interface;
  _CvsGraphicCompositeBaseDisp = dispinterface;
  _CvsGraphicComposite = interface;
  _CvsGraphicCompositeDisp = dispinterface;
  _CvsGraphicPolylinePath = interface;
  _CvsGraphicPolylinePathDisp = dispinterface;
  _CvsGraphicMaskedRegion = interface;
  _CvsGraphicMaskedRegionDisp = dispinterface;
  _CvsGraphicDefectPoints = interface;
  _CvsGraphicDefectPointsDisp = dispinterface;
  _CvsGraphicEdgePoints = interface;
  _CvsGraphicEdgePointsDisp = dispinterface;
  _CvsGraphicExtremePoints = interface;
  _CvsGraphicExtremePointsDisp = dispinterface;
  _CvsGraphicFilledBox = interface;
  _CvsGraphicFilledBoxDisp = dispinterface;
  _CvsGraphicParallelogram = interface;
  _CvsGraphicParallelogramDisp = dispinterface;
  _CvsGraphicPolygon = interface;
  _CvsGraphicPolygonDisp = dispinterface;
  _CvsJobAnalyzer = interface;
  _CvsJobAnalyzerDisp = dispinterface;
  _CvsModbusSettings = interface;
  _CvsModbusSettingsDisp = dispinterface;
  IPolygonEditor = interface;
  IPolygonEditorDisp = dispinterface;
  _CvsString = interface;
  _CvsStringDisp = dispinterface;
  _StringProcessing = interface;
  _StringProcessingDisp = dispinterface;
  _StringBuilderProcessing = interface;
  _StringBuilderProcessingDisp = dispinterface;
  _CvsSymbolicTag = interface;
  _CvsSymbolicTagDisp = dispinterface;
  _CvsSymbolicTagCollection = interface;
  _CvsSymbolicTagCollectionDisp = dispinterface;
  _CvsDiscreteIOLines = interface;
  _CvsDiscreteIOLinesDisp = dispinterface;
  _CvsUniversalIOSettings = interface;
  _CvsUniversalIOSettingsDisp = dispinterface;
  _CvsPassFailSettings = interface;
  _CvsPassFailSettingsDisp = dispinterface;
  _CvsHostCollection = interface;
  _CvsHostCollectionDisp = dispinterface;
  _CvsHost = interface;
  _CvsHostDisp = dispinterface;
  _CvsHostSensor = interface;
  _CvsHostSensorDisp = dispinterface;
  _CvsNetworkListener = interface;
  _CvsNetworkListenerDisp = dispinterface;
  _CvsNetworkMonitor = interface;
  _CvsNetworkMonitorDisp = dispinterface;
  _CvsNetworkMonitorSubnet = interface;
  _CvsNetworkMonitorSubnetDisp = dispinterface;
  ICvsNetworkMonitorEvents = dispinterface;
  _CvsOcrMaxAutoTune = interface;
  _CvsOcrMaxAutoTuneDisp = dispinterface;
  _AutoTuneSettings = interface;
  _AutoTuneSettingsDisp = dispinterface;
  _SpaceSettings = interface;
  _SpaceSettingsDisp = dispinterface;
  _AutoSegmentResult = interface;
  _AutoSegmentResultDisp = dispinterface;
  _AutoTuneRecord = interface;
  _AutoTuneRecordDisp = dispinterface;
  _AutoTuneException = interface;
  _AutoTuneExceptionDisp = dispinterface;
  _AutoTuneSessionClosedEventArgs = interface;
  _AutoTuneSessionClosedEventArgsDisp = dispinterface;
  _AutoTuneSessionClosedHandler = interface;
  _AutoTuneSessionClosedHandlerDisp = dispinterface;
  _CvsCellButton = interface;
  _CvsCellButtonDisp = dispinterface;
  _CvsCellChart = interface;
  _CvsCellChartDisp = dispinterface;
  _CvsCellCheckBox = interface;
  _CvsCellCheckBoxDisp = dispinterface;
  _CvsCellCollection = interface;
  _CvsCellCollectionDisp = dispinterface;
  _CvsCellColorLabel = interface;
  _CvsCellColorLabelDisp = dispinterface;
  _CvsCellDialog = interface;
  _CvsCellDialogDisp = dispinterface;
  _CvsCellEditCircle = interface;
  _CvsCellEditCircleDisp = dispinterface;
  _CvsCellEditFloat = interface;
  _CvsCellEditFloatDisp = dispinterface;
  _CvsCellEditInt = interface;
  _CvsCellEditIntDisp = dispinterface;
  _CvsCellEditLine = interface;
  _CvsCellEditLineDisp = dispinterface;
  _CvsCellEditPoint = interface;
  _CvsCellEditPointDisp = dispinterface;
  _CvsCellEditRegion = interface;
  _CvsCellEditRegionDisp = dispinterface;
  _CvsCellEditString = interface;
  _CvsCellEditStringDisp = dispinterface;
  _CvsCellEditAnnulus = interface;
  _CvsCellEditAnnulusDisp = dispinterface;
  _CvsCellEmpty = interface;
  _CvsCellEmptyDisp = dispinterface;
  _CvsCellError = interface;
  _CvsCellErrorDisp = dispinterface;
  _CvsCellFloat = interface;
  _CvsCellFloatDisp = dispinterface;
  _CvsCellInt = interface;
  _CvsCellIntDisp = dispinterface;
  _CvsCellLink = interface;
  _CvsCellLinkDisp = dispinterface;
  _CvsCellListBox = interface;
  _CvsCellListBoxDisp = dispinterface;
  _CvsCellMultiStatus = interface;
  _CvsCellMultiStatusDisp = dispinterface;
  _CvsCellSelect = interface;
  _CvsCellSelectDisp = dispinterface;
  _CvsCellStatus = interface;
  _CvsCellStatusDisp = dispinterface;
  _CvsCellStatusLight = interface;
  _CvsCellStatusLightDisp = dispinterface;
  _CvsCellWizard = interface;
  _CvsCellWizardDisp = dispinterface;
  _CvsDiscreteInputLine = interface;
  _CvsDiscreteInputLineDisp = dispinterface;
  _CvsInputCollection = interface;
  _CvsInputCollectionDisp = dispinterface;
  _CvsOutputCollection = interface;
  _CvsOutputCollectionDisp = dispinterface;
  _CvsDiscreteOutputLine = interface;
  _CvsDiscreteOutputLineDisp = dispinterface;
  _CvsExpansionIOSettings = interface;
  _CvsExpansionIOSettingsDisp = dispinterface;
  _CvsFile = interface;
  _CvsFileDisp = dispinterface;
  _CvsFileCollection = interface;
  _CvsFileCollectionDisp = dispinterface;
  _CvsFileManager = interface;
  _CvsFileManagerDisp = dispinterface;
  _CvsFtpClient = interface;
  _CvsFtpClientDisp = dispinterface;
  _CvsFtpSettings = interface;
  _CvsFtpSettingsDisp = dispinterface;
  _CvsGraphic4Side = interface;
  _CvsGraphic4SideDisp = dispinterface;
  _CvsGraphicArc = interface;
  _CvsGraphicArcDisp = dispinterface;
  _CvsGraphicBlob = interface;
  _CvsGraphicBlobDisp = dispinterface;
  _CvsGraphicCircle = interface;
  _CvsGraphicCircleDisp = dispinterface;
  _CvsGraphicCollection = interface;
  _CvsGraphicCollectionDisp = dispinterface;
  _CvsGraphicPatternMatch = interface;
  _CvsGraphicPatternMatchDisp = dispinterface;
  _CvsPatternMatchSegment = interface;
  _CvsPatternMatchSegmentDisp = dispinterface;
  _CvsGraphicCross = interface;
  _CvsGraphicCrossDisp = dispinterface;
  _CvsGraphicEdgeProfile = interface;
  _CvsGraphicEdgeProfileDisp = dispinterface;
  _CvsGraphicFixture = interface;
  _CvsGraphicFixtureDisp = dispinterface;
  _CvsGraphicHistogram = interface;
  _CvsGraphicHistogramDisp = dispinterface;
  _CvsGraphicImage = interface;
  _CvsGraphicImageDisp = dispinterface;
  _CvsGraphicBlobProperties = interface;
  _CvsGraphicBlobPropertiesDisp = dispinterface;
  _CvsGraphicLine = interface;
  _CvsGraphicLineDisp = dispinterface;
  _CvsGraphicOffset = interface;
  _CvsGraphicOffsetDisp = dispinterface;
  _CvsGraphicPoint = interface;
  _CvsGraphicPointDisp = dispinterface;
  _CvsGraphicPolyline = interface;
  _CvsGraphicPolylineDisp = dispinterface;
  _CvsGraphicRegion = interface;
  _CvsGraphicRegionDisp = dispinterface;
  _CvsGraphicText = interface;
  _CvsGraphicTextDisp = dispinterface;
  _CvsGraphicAnnulus = interface;
  _CvsGraphicAnnulusDisp = dispinterface;
  _CvsHostTableCollection = interface;
  _CvsHostTableCollectionDisp = dispinterface;
  _CvsHostTableEntry = interface;
  _CvsHostTableEntryDisp = dispinterface;
  _CvsHostVersion = interface;
  _CvsHostVersionDisp = dispinterface;
  _CvsImage = interface;
  _CvsImageDisp = dispinterface;
  _CvsInSight = interface;
  _CvsInSightDisp = dispinterface;
  _CvsJobInfo = interface;
  _CvsJobInfoDisp = dispinterface;
  _CvsKernel = interface;
  _CvsKernelDisp = dispinterface;
  _CvsLicense = interface;
  _CvsLicenseDisp = dispinterface;
  _CvsLocalIOSettings = interface;
  _CvsLocalIOSettingsDisp = dispinterface;
  _CvsNativeModeClient = interface;
  _CvsNativeModeClientDisp = dispinterface;
  _CvsNetworkSettings = interface;
  _CvsNetworkSettingsDisp = dispinterface;
  _CvsPicture = interface;
  _CvsPictureDisp = dispinterface;
  _CvsProcessorConstants = interface;
  _CvsProcessorConstantsDisp = dispinterface;
  _CvsResultSet = interface;
  _CvsResultSetDisp = dispinterface;
  ICvsResultsQueue = interface;
  ICvsResultsQueueDisp = dispinterface;
  _CvsSerialSettings = interface;
  _CvsSerialSettingsDisp = dispinterface;
  _CvsSntpCurrentData = interface;
  _CvsSntpCurrentDataDisp = dispinterface;
  _CvsSntpSettings = interface;
  _CvsSntpSettingsDisp = dispinterface;
  _CvsTimeDateData = interface;
  _CvsTimeDateDataDisp = dispinterface;
  _CvsTimeZones = interface;
  _CvsTimeZonesDisp = dispinterface;
  _CvsUserCollection = interface;
  _CvsUserCollectionDisp = dispinterface;
  _CvsUtility = interface;
  _CvsUtilityDisp = dispinterface;
  _CvsView = interface;
  _CvsViewDisp = dispinterface;
  _CvsStateChangedEventArgs = interface;
  _CvsStateChangedEventArgsDisp = dispinterface;
  _CvsConnectCompletedEventArgs = interface;
  _CvsConnectCompletedEventArgsDisp = dispinterface;
  _CvsInSightMessageEventArgs = interface;
  _CvsInSightMessageEventArgsDisp = dispinterface;
  _CvsLoadCompletedEventArgs = interface;
  _CvsLoadCompletedEventArgsDisp = dispinterface;
  _CvsSaveCompletedEventArgs = interface;
  _CvsSaveCompletedEventArgsDisp = dispinterface;
  ICvsInSightEvents = dispinterface;
  _CvsAcceptMessageHandler = interface;
  _CvsAcceptMessageHandlerDisp = dispinterface;
  _BadSnippetException = interface;
  _BadSnippetExceptionDisp = dispinterface;
  _Snippet = interface;
  _SnippetDisp = dispinterface;
  _Syslog = interface;
  _SyslogDisp = dispinterface;
  _SyslogTraceListener = interface;
  _SyslogTraceListenerDisp = dispinterface;
  _ArgUpdateHandler_2 = interface;
  _ArgUpdateHandler_2Disp = dispinterface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                       
// (NOTE: Here we map each CoClass to its Default Interface)              
// *********************************************************************//
  CvsActionToggle = ICvsActionToggle;
  CvsAction = ICvsAction;
  CvsCell = _CvsCell;
  CvsCellOcrMaxAutoTune = _CvsCellOcrMaxAutoTune;
  CvsCrcResponse = _CvsCrcResponse;
  CvsCellString = _CvsCellString;
  CvsCellAuthorize = _CvsCellAuthorize;
  CvsCellEditGraphic = _CvsCellEditGraphic;
  CvsCellEditMultiGraphics = _CvsCellEditMultiGraphics;
  CvsCellReference = _CvsCellReference;
  CvsCellReferenceInfo = _CvsCellReferenceInfo;
  CvsCellReferenceTable = _CvsCellReferenceTable;
  CvsDefaultRegion = _CvsDefaultRegion;
  CvsEasyView = _CvsEasyView;
  CvsEasyViewItem = _CvsEasyViewItem;
  CvsFeatureLicense = _CvsFeatureLicense;
  CvsIspMessageStore = _CvsIspMessageStore;
  CvsJobServerSettings = _CvsJobServerSettings;
  CvsOcrMaxDiagnosticData = _CvsOcrMaxDiagnosticData;
  FragmentStatistics = _FragmentStatistics;
  CharacterStatistics = _CharacterStatistics;
  CvsOcrMaxDiagnostics = _CvsOcrMaxDiagnostics;
  CvsOcrMaxExpressionEditorBase = _CvsOcrMaxExpressionEditorBase;
  CvsOcrMaxSpreadsheetExpressionEditor = _CvsOcrMaxSpreadsheetExpressionEditor;
  ArgUpdateHandler = _ArgUpdateHandler;
  CvsPowerlinkSettings = _CvsPowerlinkSettings;
  CvsRemoteSubnetCollection = _CvsRemoteSubnetCollection;
  CvsRemoteSubnetEntry = _CvsRemoteSubnetEntry;
  CvsSensor = _CvsSensor;
  CvsUser = _CvsUser;
  CvsInSightExtensions = _CvsInSightExtensions;
  CvsSettings = _CvsSettings;
  UndoAction = _UndoAction;
  UndoCellTagsAction = _UndoCellTagsAction;
  UndoClearAllAction = _UndoClearAllAction;
  UndoCutAndPasteAction = _UndoCutAndPasteAction;
  UndoEventArgs = _UndoEventArgs;
  UndoExpressionAction = _UndoExpressionAction;
  UndoOpcTagEditorAction = _UndoOpcTagEditorAction;
  UndoPropertySheetAction = _UndoPropertySheetAction;
  UndoReferenceRedirectAction = _UndoReferenceRedirectAction;
  UndoScriptChangeAction = _UndoScriptChangeAction;
  CdsConnectionChange = _CdsConnectionChange;
  CvsGraphic = _CvsGraphic;
  CvsGraphicArrows = _CvsGraphicArrows;
  CvsCellEditMaskedRegion = _CvsCellEditMaskedRegion;
  CvsCellEditPolylinePath = _CvsCellEditPolylinePath;
  CvsAuditMessageSettings = _CvsAuditMessageSettings;
  CvsCaliperScoringMethod = _CvsCaliperScoringMethod;
  CvsCellEditComposite = _CvsCellEditComposite;
  CvsCellEditPolygon = _CvsCellEditPolygon;
  CvsColorMatchEditor = _CvsColorMatchEditor;
  CvsColorModel = _CvsColorModel;
  CvsEasyViewHelper = _CvsEasyViewHelper;
  CvsCompositeSubregion = _CvsCompositeSubregion;
  CvsGraphicCompositeBase = _CvsGraphicCompositeBase;
  CvsGraphicComposite = _CvsGraphicComposite;
  CvsGraphicPolylinePath = _CvsGraphicPolylinePath;
  CvsGraphicMaskedRegion = _CvsGraphicMaskedRegion;
  CvsGraphicDefectPoints = _CvsGraphicDefectPoints;
  CvsGraphicEdgePoints = _CvsGraphicEdgePoints;
  CvsGraphicExtremePoints = _CvsGraphicExtremePoints;
  CvsGraphicFilledBox = _CvsGraphicFilledBox;
  CvsGraphicParallelogram = _CvsGraphicParallelogram;
  CvsGraphicPolygon = _CvsGraphicPolygon;
  CvsJobAnalyzer = _CvsJobAnalyzer;
  CvsModbusSettings = _CvsModbusSettings;
  CvsString = _CvsString;
  StringProcessing = _StringProcessing;
  StringBuilderProcessing = _StringBuilderProcessing;
  CvsSymbolicTag = _CvsSymbolicTag;
  CvsSymbolicTagCollection = _CvsSymbolicTagCollection;
  CvsDiscreteIOLines = _CvsDiscreteIOLines;
  CvsUniversalIOSettings = _CvsUniversalIOSettings;
  CvsPassFailSettings = _CvsPassFailSettings;
  CvsHostCollection = _CvsHostCollection;
  CvsHost = _CvsHost;
  CvsHostSensor = _CvsHostSensor;
  CvsNetworkListener = _CvsNetworkListener;
  CvsNetworkMonitor = _CvsNetworkMonitor;
  CvsNetworkMonitorSubnet = _CvsNetworkMonitorSubnet;
  CvsOcrMaxAutoTune = _CvsOcrMaxAutoTune;
  AutoTuneSettings = _AutoTuneSettings;
  SpaceSettings = _SpaceSettings;
  AutoSegmentResult = _AutoSegmentResult;
  AutoTuneRecord = _AutoTuneRecord;
  AutoTuneException = _AutoTuneException;
  ArgUpdateHandler_2 = _ArgUpdateHandler_2;
  AutoTuneSessionClosedEventArgs = _AutoTuneSessionClosedEventArgs;
  AutoTuneSessionClosedHandler = _AutoTuneSessionClosedHandler;
  CvsCellButton = _CvsCellButton;
  CvsCellChart = _CvsCellChart;
  CvsCellCheckBox = _CvsCellCheckBox;
  CvsCellCollection = _CvsCellCollection;
  CvsCellColorLabel = _CvsCellColorLabel;
  CvsCellDialog = _CvsCellDialog;
  CvsCellEditCircle = _CvsCellEditCircle;
  CvsCellEditFloat = _CvsCellEditFloat;
  CvsCellEditInt = _CvsCellEditInt;
  CvsCellEditLine = _CvsCellEditLine;
  CvsCellEditPoint = _CvsCellEditPoint;
  CvsCellEditRegion = _CvsCellEditRegion;
  CvsCellEditString = _CvsCellEditString;
  CvsCellEditAnnulus = _CvsCellEditAnnulus;
  CvsCellEmpty = _CvsCellEmpty;
  CvsCellError = _CvsCellError;
  CvsCellFloat = _CvsCellFloat;
  CvsCellInt = _CvsCellInt;
  CvsCellLink = _CvsCellLink;
  CvsCellListBox = _CvsCellListBox;
  CvsCellMultiStatus = _CvsCellMultiStatus;
  CvsCellSelect = _CvsCellSelect;
  CvsCellStatus = _CvsCellStatus;
  CvsCellStatusLight = _CvsCellStatusLight;
  CvsCellWizard = _CvsCellWizard;
  CvsDiscreteInputLine = _CvsDiscreteInputLine;
  CvsInputCollection = _CvsInputCollection;
  CvsOutputCollection = _CvsOutputCollection;
  CvsDiscreteOutputLine = _CvsDiscreteOutputLine;
  CvsExpansionIOSettings = _CvsExpansionIOSettings;
  CvsFile = _CvsFile;
  CvsFileCollection = _CvsFileCollection;
  CvsFileManager = _CvsFileManager;
  CvsFtpClient = _CvsFtpClient;
  CvsFtpSettings = _CvsFtpSettings;
  CvsGraphic4Side = _CvsGraphic4Side;
  CvsGraphicArc = _CvsGraphicArc;
  CvsGraphicBlob = _CvsGraphicBlob;
  CvsGraphicCircle = _CvsGraphicCircle;
  CvsGraphicCollection = _CvsGraphicCollection;
  CvsGraphicPatternMatch = _CvsGraphicPatternMatch;
  CvsPatternMatchSegment = _CvsPatternMatchSegment;
  CvsGraphicCross = _CvsGraphicCross;
  CvsGraphicEdgeProfile = _CvsGraphicEdgeProfile;
  CvsGraphicFixture = _CvsGraphicFixture;
  CvsGraphicHistogram = _CvsGraphicHistogram;
  CvsGraphicImage = _CvsGraphicImage;
  CvsGraphicBlobProperties = _CvsGraphicBlobProperties;
  CvsGraphicLine = _CvsGraphicLine;
  CvsGraphicOffset = _CvsGraphicOffset;
  CvsGraphicPoint = _CvsGraphicPoint;
  CvsGraphicPolyline = _CvsGraphicPolyline;
  CvsGraphicRegion = _CvsGraphicRegion;
  CvsGraphicText = _CvsGraphicText;
  CvsGraphicAnnulus = _CvsGraphicAnnulus;
  CvsHostTableCollection = _CvsHostTableCollection;
  CvsHostTableEntry = _CvsHostTableEntry;
  CvsHostVersion = _CvsHostVersion;
  CvsImage = _CvsImage;
  CvsInSight = _CvsInSight;
  CvsJobInfo = _CvsJobInfo;
  CvsKernel = _CvsKernel;
  CvsLicense = _CvsLicense;
  CvsLocalIOSettings = _CvsLocalIOSettings;
  CvsNativeModeClient = _CvsNativeModeClient;
  CvsNetworkSettings = _CvsNetworkSettings;
  CvsPicture = _CvsPicture;
  CvsProcessorConstants = _CvsProcessorConstants;
  CvsResultSet = _CvsResultSet;
  CvsSerialSettings = _CvsSerialSettings;
  CvsSntpCurrentData = _CvsSntpCurrentData;
  CvsSntpSettings = _CvsSntpSettings;
  CvsTimeDateData = _CvsTimeDateData;
  CvsTimeZones = _CvsTimeZones;
  CvsUserCollection = _CvsUserCollection;
  CvsUtility = _CvsUtility;
  CvsView = _CvsView;
  CvsStateChangedEventArgs = _CvsStateChangedEventArgs;
  CvsConnectCompletedEventArgs = _CvsConnectCompletedEventArgs;
  CvsInSightMessageEventArgs = _CvsInSightMessageEventArgs;
  CvsLoadCompletedEventArgs = _CvsLoadCompletedEventArgs;
  CvsSaveCompletedEventArgs = _CvsSaveCompletedEventArgs;
  CvsAcceptMessageHandler = _CvsAcceptMessageHandler;
  BadSnippetException = _BadSnippetException;
  Snippet = _Snippet;
  Syslog = _Syslog;
  SyslogTraceListener = _SyslogTraceListener;


// *********************************************************************//
// Declaration of structures, unions and aliases.                         
// *********************************************************************//
  CvsCellComment = packed record
    mText: PChar;
    mFlagColor: OLE_COLOR;
  end;

  CvsCellLocation = packed record
    row: Integer;
    column: Integer;
  end;

  CvsCellRange = packed record
    Location: CvsCellLocation;
    rows: Integer;
    columns: Integer;
  end;

  CvsImageLocation = packed record
    row: Single;
    column: Single;
  end;

  Arrow = packed record
    Location: CvsImageLocation;
    angle: Single;
  end;

  CvsLineScanStatistics = packed record
    AcqLineCount: Integer;
    LineCount: Integer;
    AcqEncoderSteps: Single;
    EncoderSteps: Single;
    BufferCount: Integer;
    BufferOverrunCount: Integer;
    LineOverrunCount: Integer;
    AcqTime: Single;
    ExposureConflict: Integer;
    EncoderAcqTimeoutCount: Integer;
    TimestampUsecUpper: Integer;
    TimestampUsecLower: Integer;
  end;

  CvsGraphicBlobPoint = packed record
    direction: CvsGraphicBlobDirection;
    distance: Integer;
  end;


// *********************************************************************//
// Interface: _CvsCell
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {099BD627-3E57-39C4-AA0A-126821639B22}
// *********************************************************************//
  _CvsCell = interface(IDispatch)
    ['{099BD627-3E57-39C4-AA0A-126821639B22}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Get_Text: WideString; safecall;
    function Get_IsEditCell: WordBool; safecall;
    function Get_SupportsAuthorization: WordBool; safecall;
    function Get_DataType: CvsCellDataType; safecall;
    function Get_ErrorString: WideString; safecall;
    function Get_Error: WordBool; safecall;
    function Get_Control: WordBool; safecall;
    function Get_HasChecksum: WordBool; safecall;
    function Get_Hidden: WordBool; safecall;
    function Get_ProtectionState: CvsCellIPProtection; safecall;
    function Get_Enabled: WordBool; safecall;
    function Get_Location: CvsCellLocation; safecall;
    function Get_Expression: WideString; safecall;
    function Get_SymbolicTag: WideString; safecall;
    function Get_OpcTag: WideString; safecall;
    function Get_CellStateReference: WideString; safecall;
    function Get_Properties: CvsCellProperty; safecall;
    function Get_DataTypeString: WideString; safecall;
    property ToString: WideString read Get_ToString;
    property Text: WideString read Get_Text;
    property IsEditCell: WordBool read Get_IsEditCell;
    property SupportsAuthorization: WordBool read Get_SupportsAuthorization;
    property DataType: CvsCellDataType read Get_DataType;
    property ErrorString: WideString read Get_ErrorString;
    property Error: WordBool read Get_Error;
    property Control: WordBool read Get_Control;
    property HasChecksum: WordBool read Get_HasChecksum;
    property Hidden: WordBool read Get_Hidden;
    property ProtectionState: CvsCellIPProtection read Get_ProtectionState;
    property Enabled: WordBool read Get_Enabled;
    property Location: CvsCellLocation read Get_Location;
    property Expression: WideString read Get_Expression;
    property SymbolicTag: WideString read Get_SymbolicTag;
    property OpcTag: WideString read Get_OpcTag;
    property CellStateReference: WideString read Get_CellStateReference;
    property Properties: CvsCellProperty read Get_Properties;
    property DataTypeString: WideString read Get_DataTypeString;
  end;

// *********************************************************************//
// DispIntf:  _CvsCellDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {099BD627-3E57-39C4-AA0A-126821639B22}
// *********************************************************************//
  _CvsCellDisp = dispinterface
    ['{099BD627-3E57-39C4-AA0A-126821639B22}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    property Text: WideString readonly dispid 1610743812;
    property IsEditCell: WordBool readonly dispid 1610743813;
    property SupportsAuthorization: WordBool readonly dispid 1610743814;
    property DataType: CvsCellDataType readonly dispid 1610743815;
    property ErrorString: WideString readonly dispid 1610743816;
    property Error: WordBool readonly dispid 1610743817;
    property Control: WordBool readonly dispid 1610743818;
    property HasChecksum: WordBool readonly dispid 1610743819;
    property Hidden: WordBool readonly dispid 1610743820;
    property ProtectionState: CvsCellIPProtection readonly dispid 1610743821;
    property Enabled: WordBool readonly dispid 1610743822;
    property Location: {??CvsCellLocation}OleVariant readonly dispid 1610743823;
    property Expression: WideString readonly dispid 1610743824;
    property SymbolicTag: WideString readonly dispid 1610743825;
    property OpcTag: WideString readonly dispid 1610743826;
    property CellStateReference: WideString readonly dispid 1610743827;
    property Properties: CvsCellProperty readonly dispid 1610743828;
    property DataTypeString: WideString readonly dispid 1610743829;
  end;

// *********************************************************************//
// Interface: _CvsCellOcrMaxAutoTune
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {0C8CCB26-D3FA-3704-8D48-2BC551E1FAEC}
// *********************************************************************//
  _CvsCellOcrMaxAutoTune = interface(IDispatch)
    ['{0C8CCB26-D3FA-3704-8D48-2BC551E1FAEC}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Get_Text: WideString; safecall;
    function Get_IsEditCell: WordBool; safecall;
    function Get_SupportsAuthorization: WordBool; safecall;
    function Get_DataType: CvsCellDataType; safecall;
    function Get_ErrorString: WideString; safecall;
    function Get_Error: WordBool; safecall;
    function Get_Control: WordBool; safecall;
    function Get_HasChecksum: WordBool; safecall;
    function Get_Hidden: WordBool; safecall;
    function Get_ProtectionState: CvsCellIPProtection; safecall;
    function Get_Enabled: WordBool; safecall;
    function Get_Location: CvsCellLocation; safecall;
    function Get_Expression: WideString; safecall;
    function Get_SymbolicTag: WideString; safecall;
    function Get_OpcTag: WideString; safecall;
    function Get_CellStateReference: WideString; safecall;
    function Get_Properties: CvsCellProperty; safecall;
    function Get_DataTypeString: WideString; safecall;
    property ToString: WideString read Get_ToString;
    property Text: WideString read Get_Text;
    property IsEditCell: WordBool read Get_IsEditCell;
    property SupportsAuthorization: WordBool read Get_SupportsAuthorization;
    property DataType: CvsCellDataType read Get_DataType;
    property ErrorString: WideString read Get_ErrorString;
    property Error: WordBool read Get_Error;
    property Control: WordBool read Get_Control;
    property HasChecksum: WordBool read Get_HasChecksum;
    property Hidden: WordBool read Get_Hidden;
    property ProtectionState: CvsCellIPProtection read Get_ProtectionState;
    property Enabled: WordBool read Get_Enabled;
    property Location: CvsCellLocation read Get_Location;
    property Expression: WideString read Get_Expression;
    property SymbolicTag: WideString read Get_SymbolicTag;
    property OpcTag: WideString read Get_OpcTag;
    property CellStateReference: WideString read Get_CellStateReference;
    property Properties: CvsCellProperty read Get_Properties;
    property DataTypeString: WideString read Get_DataTypeString;
  end;

// *********************************************************************//
// DispIntf:  _CvsCellOcrMaxAutoTuneDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {0C8CCB26-D3FA-3704-8D48-2BC551E1FAEC}
// *********************************************************************//
  _CvsCellOcrMaxAutoTuneDisp = dispinterface
    ['{0C8CCB26-D3FA-3704-8D48-2BC551E1FAEC}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    property Text: WideString readonly dispid 1610743812;
    property IsEditCell: WordBool readonly dispid 1610743813;
    property SupportsAuthorization: WordBool readonly dispid 1610743814;
    property DataType: CvsCellDataType readonly dispid 1610743815;
    property ErrorString: WideString readonly dispid 1610743816;
    property Error: WordBool readonly dispid 1610743817;
    property Control: WordBool readonly dispid 1610743818;
    property HasChecksum: WordBool readonly dispid 1610743819;
    property Hidden: WordBool readonly dispid 1610743820;
    property ProtectionState: CvsCellIPProtection readonly dispid 1610743821;
    property Enabled: WordBool readonly dispid 1610743822;
    property Location: {??CvsCellLocation}OleVariant readonly dispid 1610743823;
    property Expression: WideString readonly dispid 1610743824;
    property SymbolicTag: WideString readonly dispid 1610743825;
    property OpcTag: WideString readonly dispid 1610743826;
    property CellStateReference: WideString readonly dispid 1610743827;
    property Properties: CvsCellProperty readonly dispid 1610743828;
    property DataTypeString: WideString readonly dispid 1610743829;
  end;

// *********************************************************************//
// Interface: ICvsAction
// Flags:     (384) NonExtensible OleAutomation
// GUID:      {477FA70B-A051-47B1-8A03-E119EC212284}
// *********************************************************************//
  ICvsAction = interface(IUnknown)
    ['{477FA70B-A051-47B1-8A03-E119EC212284}']
    function Get_Enabled(out pRetVal: WordBool): HResult; stdcall;
    function Get_Suppressed(out pRetVal: WordBool): HResult; stdcall;
    function Set_Suppressed(pRetVal: WordBool): HResult; stdcall;
    function Execute: HResult; stdcall;
  end;

// *********************************************************************//
// DispIntf:  ICvsActionEvents
// Flags:     (4224) NonExtensible Dispatchable
// GUID:      {477FA70B-A051-47B1-8A03-E889EC211E84}
// *********************************************************************//
  ICvsActionEvents = dispinterface
    ['{477FA70B-A051-47B1-8A03-E889EC211E84}']
    procedure EnabledChanged(sender: OleVariant; const e: _EventArgs); dispid 1;
  end;

// *********************************************************************//
// Interface: ICvsActionObject
// Flags:     (4288) Dual NonExtensible Dispatchable
// GUID:      {74D721EB-659B-4607-B9C5-C0FC17A44344}
// *********************************************************************//
  ICvsActionObject = interface(IDispatch)
    ['{74D721EB-659B-4607-B9C5-C0FC17A44344}']
    function Get_MenuText: WideString; safecall;
    procedure Set_MenuText(const pRetVal: WideString); safecall;
    function Get_ContextMenuText: WideString; safecall;
    procedure Set_ContextMenuText(const pRetVal: WideString); safecall;
    function Get_ToolTipText: WideString; safecall;
    procedure Set_ToolTipText(const pRetVal: WideString); safecall;
    function Get_NewCheckedStatus: WordBool; safecall;
    function Get_CheckedStatus: WordBool; safecall;
    procedure Set_CheckedStatus(pRetVal: WordBool); safecall;
    function Get_ImageIndex: Integer; safecall;
    procedure Set_ImageIndex(pRetVal: Integer); safecall;
    function Get_Image: _Image; safecall;
    procedure _Set_Image(const pRetVal: _Image); safecall;
    function Get_IconSize: Size; safecall;
    function Get_Visible: WordBool; safecall;
    procedure Set_Visible(pRetVal: WordBool); safecall;
    procedure add_Activate(const value: _EventHandler); safecall;
    procedure remove_Activate(const value: _EventHandler); safecall;
    function Get_Enabled: WordBool; safecall;
    procedure Set_Enabled(pRetVal: WordBool); safecall;
    function Get_BindToEnabled: WordBool; safecall;
    function Get_BindToEvent: WordBool; safecall;
    function Get_Tag: OleVariant; safecall;
    procedure _Set_Tag(pRetVal: OleVariant); safecall;
    property MenuText: WideString read Get_MenuText write Set_MenuText;
    property ContextMenuText: WideString read Get_ContextMenuText write Set_ContextMenuText;
    property ToolTipText: WideString read Get_ToolTipText write Set_ToolTipText;
    property NewCheckedStatus: WordBool read Get_NewCheckedStatus;
    property CheckedStatus: WordBool read Get_CheckedStatus write Set_CheckedStatus;
    property ImageIndex: Integer read Get_ImageIndex write Set_ImageIndex;
    property Image: _Image read Get_Image write _Set_Image;
    property IconSize: Size read Get_IconSize;
    property Visible: WordBool read Get_Visible write Set_Visible;
    property Enabled: WordBool read Get_Enabled write Set_Enabled;
    property BindToEnabled: WordBool read Get_BindToEnabled;
    property BindToEvent: WordBool read Get_BindToEvent;
    property Tag: OleVariant read Get_Tag write _Set_Tag;
  end;

// *********************************************************************//
// DispIntf:  ICvsActionObjectDisp
// Flags:     (4288) Dual NonExtensible Dispatchable
// GUID:      {74D721EB-659B-4607-B9C5-C0FC17A44344}
// *********************************************************************//
  ICvsActionObjectDisp = dispinterface
    ['{74D721EB-659B-4607-B9C5-C0FC17A44344}']
    property MenuText: WideString dispid 1610743808;
    property ContextMenuText: WideString dispid 1610743810;
    property ToolTipText: WideString dispid 1610743812;
    property NewCheckedStatus: WordBool readonly dispid 1610743814;
    property CheckedStatus: WordBool dispid 1610743815;
    property ImageIndex: Integer dispid 1610743817;
    property Image: _Image dispid 1610743819;
    property IconSize: {??Size}OleVariant readonly dispid 1610743821;
    property Visible: WordBool dispid 1610743822;
    procedure add_Activate(const value: _EventHandler); dispid 1610743824;
    procedure remove_Activate(const value: _EventHandler); dispid 1610743825;
    property Enabled: WordBool dispid 1610743826;
    property BindToEnabled: WordBool readonly dispid 1610743828;
    property BindToEvent: WordBool readonly dispid 1610743829;
    property Tag: OleVariant dispid 1610743830;
  end;

// *********************************************************************//
// Interface: ICvsActionToggle
// Flags:     (384) NonExtensible OleAutomation
// GUID:      {477FA70B-A051-47B1-8A03-E223EC211EE4}
// *********************************************************************//
  ICvsActionToggle = interface(IUnknown)
    ['{477FA70B-A051-47B1-8A03-E223EC211EE4}']
    function Get_Enabled(out pRetVal: WordBool): HResult; stdcall;
    function Get_Suppressed(out pRetVal: WordBool): HResult; stdcall;
    function Set_Suppressed(pRetVal: WordBool): HResult; stdcall;
    function Execute: HResult; stdcall;
    function Get_Activated(out pRetVal: WordBool): HResult; stdcall;
    function Set_Activated(pRetVal: WordBool): HResult; stdcall;
  end;

// *********************************************************************//
// Interface: _CvsCrcResponse
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {5971FCFF-A542-3854-BB5E-985965238834}
// *********************************************************************//
  _CvsCrcResponse = interface(IDispatch)
    ['{5971FCFF-A542-3854-BB5E-985965238834}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Get_IsDirty: WordBool; safecall;
    function Get_Crc: Integer; safecall;
    property ToString: WideString read Get_ToString;
    property IsDirty: WordBool read Get_IsDirty;
    property Crc: Integer read Get_Crc;
  end;

// *********************************************************************//
// DispIntf:  _CvsCrcResponseDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {5971FCFF-A542-3854-BB5E-985965238834}
// *********************************************************************//
  _CvsCrcResponseDisp = dispinterface
    ['{5971FCFF-A542-3854-BB5E-985965238834}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    property IsDirty: WordBool readonly dispid 1610743812;
    property Crc: Integer readonly dispid 1610743813;
  end;

// *********************************************************************//
// Interface: _CvsCellString
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {9AA03B85-804A-347B-9318-3054189C0C59}
// *********************************************************************//
  _CvsCellString = interface(IDispatch)
    ['{9AA03B85-804A-347B-9318-3054189C0C59}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Get_Text: WideString; safecall;
    function Get_IsEditCell: WordBool; safecall;
    function Get_SupportsAuthorization: WordBool; safecall;
    function Get_DataType: CvsCellDataType; safecall;
    function Get_ErrorString: WideString; safecall;
    function Get_Error: WordBool; safecall;
    function Get_Control: WordBool; safecall;
    function Get_HasChecksum: WordBool; safecall;
    function Get_Hidden: WordBool; safecall;
    function Get_ProtectionState: CvsCellIPProtection; safecall;
    function Get_Enabled: WordBool; safecall;
    function Get_Location: CvsCellLocation; safecall;
    function Get_Expression: WideString; safecall;
    function Get_SymbolicTag: WideString; safecall;
    function Get_OpcTag: WideString; safecall;
    function Get_CellStateReference: WideString; safecall;
    function Get_Properties: CvsCellProperty; safecall;
    function Get_DataTypeString: WideString; safecall;
    function Get_AsciiText: WideString; safecall;
    function GetRawBytes: PSafeArray; safecall;
    function Get_ControlFunction: WideString; safecall;
    function Get_Checksum: Integer; safecall;
    property ToString: WideString read Get_ToString;
    property Text: WideString read Get_Text;
    property IsEditCell: WordBool read Get_IsEditCell;
    property SupportsAuthorization: WordBool read Get_SupportsAuthorization;
    property DataType: CvsCellDataType read Get_DataType;
    property ErrorString: WideString read Get_ErrorString;
    property Error: WordBool read Get_Error;
    property Control: WordBool read Get_Control;
    property HasChecksum: WordBool read Get_HasChecksum;
    property Hidden: WordBool read Get_Hidden;
    property ProtectionState: CvsCellIPProtection read Get_ProtectionState;
    property Enabled: WordBool read Get_Enabled;
    property Location: CvsCellLocation read Get_Location;
    property Expression: WideString read Get_Expression;
    property SymbolicTag: WideString read Get_SymbolicTag;
    property OpcTag: WideString read Get_OpcTag;
    property CellStateReference: WideString read Get_CellStateReference;
    property Properties: CvsCellProperty read Get_Properties;
    property DataTypeString: WideString read Get_DataTypeString;
    property AsciiText: WideString read Get_AsciiText;
    property ControlFunction: WideString read Get_ControlFunction;
    property Checksum: Integer read Get_Checksum;
  end;

// *********************************************************************//
// DispIntf:  _CvsCellStringDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {9AA03B85-804A-347B-9318-3054189C0C59}
// *********************************************************************//
  _CvsCellStringDisp = dispinterface
    ['{9AA03B85-804A-347B-9318-3054189C0C59}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    property Text: WideString readonly dispid 1610743812;
    property IsEditCell: WordBool readonly dispid 1610743813;
    property SupportsAuthorization: WordBool readonly dispid 1610743814;
    property DataType: CvsCellDataType readonly dispid 1610743815;
    property ErrorString: WideString readonly dispid 1610743816;
    property Error: WordBool readonly dispid 1610743817;
    property Control: WordBool readonly dispid 1610743818;
    property HasChecksum: WordBool readonly dispid 1610743819;
    property Hidden: WordBool readonly dispid 1610743820;
    property ProtectionState: CvsCellIPProtection readonly dispid 1610743821;
    property Enabled: WordBool readonly dispid 1610743822;
    property Location: {??CvsCellLocation}OleVariant readonly dispid 1610743823;
    property Expression: WideString readonly dispid 1610743824;
    property SymbolicTag: WideString readonly dispid 1610743825;
    property OpcTag: WideString readonly dispid 1610743826;
    property CellStateReference: WideString readonly dispid 1610743827;
    property Properties: CvsCellProperty readonly dispid 1610743828;
    property DataTypeString: WideString readonly dispid 1610743829;
    property AsciiText: WideString readonly dispid 1610743830;
    function GetRawBytes: {??PSafeArray}OleVariant; dispid 1610743831;
    property ControlFunction: WideString readonly dispid 1610743832;
    property Checksum: Integer readonly dispid 1610743833;
  end;

// *********************************************************************//
// Interface: _CvsCellAuthorize
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {6D51A27E-BAA4-3CFF-BA51-2162A61018F2}
// *********************************************************************//
  _CvsCellAuthorize = interface(IDispatch)
    ['{6D51A27E-BAA4-3CFF-BA51-2162A61018F2}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Get_Text: WideString; safecall;
    function Get_IsEditCell: WordBool; safecall;
    function Get_SupportsAuthorization: WordBool; safecall;
    function Get_DataType: CvsCellDataType; safecall;
    function Get_ErrorString: WideString; safecall;
    function Get_Error: WordBool; safecall;
    function Get_Control: WordBool; safecall;
    function Get_HasChecksum: WordBool; safecall;
    function Get_Hidden: WordBool; safecall;
    function Get_ProtectionState: CvsCellIPProtection; safecall;
    function Get_Enabled: WordBool; safecall;
    function Get_Location: CvsCellLocation; safecall;
    function Get_Expression: WideString; safecall;
    function Get_SymbolicTag: WideString; safecall;
    function Get_OpcTag: WideString; safecall;
    function Get_CellStateReference: WideString; safecall;
    function Get_Properties: CvsCellProperty; safecall;
    function Get_DataTypeString: WideString; safecall;
    function Get_AsciiText: WideString; safecall;
    function GetRawBytes: PSafeArray; safecall;
    function Get_ControlFunction: WideString; safecall;
    function Get_Checksum: Integer; safecall;
    property ToString: WideString read Get_ToString;
    property Text: WideString read Get_Text;
    property IsEditCell: WordBool read Get_IsEditCell;
    property SupportsAuthorization: WordBool read Get_SupportsAuthorization;
    property DataType: CvsCellDataType read Get_DataType;
    property ErrorString: WideString read Get_ErrorString;
    property Error: WordBool read Get_Error;
    property Control: WordBool read Get_Control;
    property HasChecksum: WordBool read Get_HasChecksum;
    property Hidden: WordBool read Get_Hidden;
    property ProtectionState: CvsCellIPProtection read Get_ProtectionState;
    property Enabled: WordBool read Get_Enabled;
    property Location: CvsCellLocation read Get_Location;
    property Expression: WideString read Get_Expression;
    property SymbolicTag: WideString read Get_SymbolicTag;
    property OpcTag: WideString read Get_OpcTag;
    property CellStateReference: WideString read Get_CellStateReference;
    property Properties: CvsCellProperty read Get_Properties;
    property DataTypeString: WideString read Get_DataTypeString;
    property AsciiText: WideString read Get_AsciiText;
    property ControlFunction: WideString read Get_ControlFunction;
    property Checksum: Integer read Get_Checksum;
  end;

// *********************************************************************//
// DispIntf:  _CvsCellAuthorizeDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {6D51A27E-BAA4-3CFF-BA51-2162A61018F2}
// *********************************************************************//
  _CvsCellAuthorizeDisp = dispinterface
    ['{6D51A27E-BAA4-3CFF-BA51-2162A61018F2}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    property Text: WideString readonly dispid 1610743812;
    property IsEditCell: WordBool readonly dispid 1610743813;
    property SupportsAuthorization: WordBool readonly dispid 1610743814;
    property DataType: CvsCellDataType readonly dispid 1610743815;
    property ErrorString: WideString readonly dispid 1610743816;
    property Error: WordBool readonly dispid 1610743817;
    property Control: WordBool readonly dispid 1610743818;
    property HasChecksum: WordBool readonly dispid 1610743819;
    property Hidden: WordBool readonly dispid 1610743820;
    property ProtectionState: CvsCellIPProtection readonly dispid 1610743821;
    property Enabled: WordBool readonly dispid 1610743822;
    property Location: {??CvsCellLocation}OleVariant readonly dispid 1610743823;
    property Expression: WideString readonly dispid 1610743824;
    property SymbolicTag: WideString readonly dispid 1610743825;
    property OpcTag: WideString readonly dispid 1610743826;
    property CellStateReference: WideString readonly dispid 1610743827;
    property Properties: CvsCellProperty readonly dispid 1610743828;
    property DataTypeString: WideString readonly dispid 1610743829;
    property AsciiText: WideString readonly dispid 1610743830;
    function GetRawBytes: {??PSafeArray}OleVariant; dispid 1610743831;
    property ControlFunction: WideString readonly dispid 1610743832;
    property Checksum: Integer readonly dispid 1610743833;
  end;

// *********************************************************************//
// Interface: _CvsCellEditGraphic
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {7F66847B-AB9B-3E85-9803-5C6F5239628D}
// *********************************************************************//
  _CvsCellEditGraphic = interface(IDispatch)
    ['{7F66847B-AB9B-3E85-9803-5C6F5239628D}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Get_Text: WideString; safecall;
    function Get_IsEditCell: WordBool; safecall;
    function Get_SupportsAuthorization: WordBool; safecall;
    function Get_DataType: CvsCellDataType; safecall;
    function Get_ErrorString: WideString; safecall;
    function Get_Error: WordBool; safecall;
    function Get_Control: WordBool; safecall;
    function Get_HasChecksum: WordBool; safecall;
    function Get_Hidden: WordBool; safecall;
    function Get_ProtectionState: CvsCellIPProtection; safecall;
    function Get_Enabled: WordBool; safecall;
    function Get_Location: CvsCellLocation; safecall;
    function Get_Expression: WideString; safecall;
    function Get_SymbolicTag: WideString; safecall;
    function Get_OpcTag: WideString; safecall;
    function Get_CellStateReference: WideString; safecall;
    function Get_Properties: CvsCellProperty; safecall;
    function Get_DataTypeString: WideString; safecall;
    function Get_EditFlags: CvsEditFlags; safecall;
    property ToString: WideString read Get_ToString;
    property Text: WideString read Get_Text;
    property IsEditCell: WordBool read Get_IsEditCell;
    property SupportsAuthorization: WordBool read Get_SupportsAuthorization;
    property DataType: CvsCellDataType read Get_DataType;
    property ErrorString: WideString read Get_ErrorString;
    property Error: WordBool read Get_Error;
    property Control: WordBool read Get_Control;
    property HasChecksum: WordBool read Get_HasChecksum;
    property Hidden: WordBool read Get_Hidden;
    property ProtectionState: CvsCellIPProtection read Get_ProtectionState;
    property Enabled: WordBool read Get_Enabled;
    property Location: CvsCellLocation read Get_Location;
    property Expression: WideString read Get_Expression;
    property SymbolicTag: WideString read Get_SymbolicTag;
    property OpcTag: WideString read Get_OpcTag;
    property CellStateReference: WideString read Get_CellStateReference;
    property Properties: CvsCellProperty read Get_Properties;
    property DataTypeString: WideString read Get_DataTypeString;
    property EditFlags: CvsEditFlags read Get_EditFlags;
  end;

// *********************************************************************//
// DispIntf:  _CvsCellEditGraphicDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {7F66847B-AB9B-3E85-9803-5C6F5239628D}
// *********************************************************************//
  _CvsCellEditGraphicDisp = dispinterface
    ['{7F66847B-AB9B-3E85-9803-5C6F5239628D}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    property Text: WideString readonly dispid 1610743812;
    property IsEditCell: WordBool readonly dispid 1610743813;
    property SupportsAuthorization: WordBool readonly dispid 1610743814;
    property DataType: CvsCellDataType readonly dispid 1610743815;
    property ErrorString: WideString readonly dispid 1610743816;
    property Error: WordBool readonly dispid 1610743817;
    property Control: WordBool readonly dispid 1610743818;
    property HasChecksum: WordBool readonly dispid 1610743819;
    property Hidden: WordBool readonly dispid 1610743820;
    property ProtectionState: CvsCellIPProtection readonly dispid 1610743821;
    property Enabled: WordBool readonly dispid 1610743822;
    property Location: {??CvsCellLocation}OleVariant readonly dispid 1610743823;
    property Expression: WideString readonly dispid 1610743824;
    property SymbolicTag: WideString readonly dispid 1610743825;
    property OpcTag: WideString readonly dispid 1610743826;
    property CellStateReference: WideString readonly dispid 1610743827;
    property Properties: CvsCellProperty readonly dispid 1610743828;
    property DataTypeString: WideString readonly dispid 1610743829;
    property EditFlags: CvsEditFlags readonly dispid 1610743830;
  end;

// *********************************************************************//
// Interface: _CvsCellEditMultiGraphics
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {15E722BC-6E78-3CD0-85A5-26C16F29AF14}
// *********************************************************************//
  _CvsCellEditMultiGraphics = interface(IDispatch)
    ['{15E722BC-6E78-3CD0-85A5-26C16F29AF14}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Get_Text: WideString; safecall;
    function Get_IsEditCell: WordBool; safecall;
    function Get_SupportsAuthorization: WordBool; safecall;
    function Get_DataType: CvsCellDataType; safecall;
    function Get_ErrorString: WideString; safecall;
    function Get_Error: WordBool; safecall;
    function Get_Control: WordBool; safecall;
    function Get_HasChecksum: WordBool; safecall;
    function Get_Hidden: WordBool; safecall;
    function Get_ProtectionState: CvsCellIPProtection; safecall;
    function Get_Enabled: WordBool; safecall;
    function Get_Location: CvsCellLocation; safecall;
    function Get_Expression: WideString; safecall;
    function Get_SymbolicTag: WideString; safecall;
    function Get_OpcTag: WideString; safecall;
    function Get_CellStateReference: WideString; safecall;
    function Get_Properties: CvsCellProperty; safecall;
    function Get_DataTypeString: WideString; safecall;
    function Get_EditFlags: CvsEditFlags; safecall;
    procedure GhostMethod__CvsCellEditMultiGraphics_120_1; safecall;
    function Get_ShowLabels: WordBool; safecall;
    property ToString: WideString read Get_ToString;
    property Text: WideString read Get_Text;
    property IsEditCell: WordBool read Get_IsEditCell;
    property SupportsAuthorization: WordBool read Get_SupportsAuthorization;
    property DataType: CvsCellDataType read Get_DataType;
    property ErrorString: WideString read Get_ErrorString;
    property Error: WordBool read Get_Error;
    property Control: WordBool read Get_Control;
    property HasChecksum: WordBool read Get_HasChecksum;
    property Hidden: WordBool read Get_Hidden;
    property ProtectionState: CvsCellIPProtection read Get_ProtectionState;
    property Enabled: WordBool read Get_Enabled;
    property Location: CvsCellLocation read Get_Location;
    property Expression: WideString read Get_Expression;
    property SymbolicTag: WideString read Get_SymbolicTag;
    property OpcTag: WideString read Get_OpcTag;
    property CellStateReference: WideString read Get_CellStateReference;
    property Properties: CvsCellProperty read Get_Properties;
    property DataTypeString: WideString read Get_DataTypeString;
    property EditFlags: CvsEditFlags read Get_EditFlags;
    property ShowLabels: WordBool read Get_ShowLabels;
  end;

// *********************************************************************//
// DispIntf:  _CvsCellEditMultiGraphicsDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {15E722BC-6E78-3CD0-85A5-26C16F29AF14}
// *********************************************************************//
  _CvsCellEditMultiGraphicsDisp = dispinterface
    ['{15E722BC-6E78-3CD0-85A5-26C16F29AF14}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    property Text: WideString readonly dispid 1610743812;
    property IsEditCell: WordBool readonly dispid 1610743813;
    property SupportsAuthorization: WordBool readonly dispid 1610743814;
    property DataType: CvsCellDataType readonly dispid 1610743815;
    property ErrorString: WideString readonly dispid 1610743816;
    property Error: WordBool readonly dispid 1610743817;
    property Control: WordBool readonly dispid 1610743818;
    property HasChecksum: WordBool readonly dispid 1610743819;
    property Hidden: WordBool readonly dispid 1610743820;
    property ProtectionState: CvsCellIPProtection readonly dispid 1610743821;
    property Enabled: WordBool readonly dispid 1610743822;
    property Location: {??CvsCellLocation}OleVariant readonly dispid 1610743823;
    property Expression: WideString readonly dispid 1610743824;
    property SymbolicTag: WideString readonly dispid 1610743825;
    property OpcTag: WideString readonly dispid 1610743826;
    property CellStateReference: WideString readonly dispid 1610743827;
    property Properties: CvsCellProperty readonly dispid 1610743828;
    property DataTypeString: WideString readonly dispid 1610743829;
    property EditFlags: CvsEditFlags readonly dispid 1610743830;
    procedure GhostMethod__CvsCellEditMultiGraphics_120_1; dispid 1610743831;
    property ShowLabels: WordBool readonly dispid 1610743832;
  end;

// *********************************************************************//
// Interface: _CvsCellReference
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {8272F831-480F-3FDA-AC8D-A6E434285C42}
// *********************************************************************//
  _CvsCellReference = interface(IDispatch)
    ['{8272F831-480F-3FDA-AC8D-A6E434285C42}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Get_CellLocation: CvsCellLocation; safecall;
    function Get_ReferencedRange: CvsCellRange; safecall;
    function Get_IsCellStateReference: WordBool; safecall;
    function Get_ExpressionCellReference: IUnknown; safecall;
    property ToString: WideString read Get_ToString;
    property CellLocation: CvsCellLocation read Get_CellLocation;
    property ReferencedRange: CvsCellRange read Get_ReferencedRange;
    property IsCellStateReference: WordBool read Get_IsCellStateReference;
    property ExpressionCellReference: IUnknown read Get_ExpressionCellReference;
  end;

// *********************************************************************//
// DispIntf:  _CvsCellReferenceDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {8272F831-480F-3FDA-AC8D-A6E434285C42}
// *********************************************************************//
  _CvsCellReferenceDisp = dispinterface
    ['{8272F831-480F-3FDA-AC8D-A6E434285C42}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    property CellLocation: {??CvsCellLocation}OleVariant readonly dispid 1610743812;
    property ReferencedRange: {??CvsCellRange}OleVariant readonly dispid 1610743813;
    property IsCellStateReference: WordBool readonly dispid 1610743814;
    property ExpressionCellReference: IUnknown readonly dispid 1610743815;
  end;

// *********************************************************************//
// Interface: _CvsCellReferenceInfo
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {6F20E7AD-065E-35B7-ABDC-C9D833F71F8B}
// *********************************************************************//
  _CvsCellReferenceInfo = interface(IDispatch)
    ['{6F20E7AD-065E-35B7-ABDC-C9D833F71F8B}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Get_Cell: _CvsCell; safecall;
    function Get_CellTag: IUnknown; safecall;
    function Get_Precedents: PSafeArray; safecall;
    function Get_Dependents: PSafeArray; safecall;
    function Get_SymbolicExpression: WideString; safecall;
    property ToString: WideString read Get_ToString;
    property Cell: _CvsCell read Get_Cell;
    property CellTag: IUnknown read Get_CellTag;
    property Precedents: PSafeArray read Get_Precedents;
    property Dependents: PSafeArray read Get_Dependents;
    property SymbolicExpression: WideString read Get_SymbolicExpression;
  end;

// *********************************************************************//
// DispIntf:  _CvsCellReferenceInfoDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {6F20E7AD-065E-35B7-ABDC-C9D833F71F8B}
// *********************************************************************//
  _CvsCellReferenceInfoDisp = dispinterface
    ['{6F20E7AD-065E-35B7-ABDC-C9D833F71F8B}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    property Cell: _CvsCell readonly dispid 1610743812;
    property CellTag: IUnknown readonly dispid 1610743813;
    property Precedents: {??PSafeArray}OleVariant readonly dispid 1610743814;
    property Dependents: {??PSafeArray}OleVariant readonly dispid 1610743815;
    property SymbolicExpression: WideString readonly dispid 1610743816;
  end;

// *********************************************************************//
// Interface: _CvsCellReferenceTable
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {0F9FDEA5-5FCF-38EA-966B-209108242E8A}
// *********************************************************************//
  _CvsCellReferenceTable = interface(IDispatch)
    ['{0F9FDEA5-5FCF-38EA-966B-209108242E8A}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Get_Item(Location: CvsCellLocation): _CvsCellReferenceInfo; safecall;
    function Get_DialogCells: IEnumerable; safecall;
    procedure GhostMethod__CvsCellReferenceTable_52_1; safecall;
    function Get_ResultSet: _CvsResultSet; safecall;
    function GetCellSymbolicName(Location: CvsCellLocation): WideString; safecall;
    function Update(const ResultSet: _CvsResultSet; const inSight: _CvsInSight): WordBool; safecall;
    function Update_2(const ResultSet: _CvsResultSet; const functionTable: IUnknown): WordBool; safecall;
    function Update_3(const ResultSet: _CvsResultSet; const functionTable: IUnknown; sheet: Integer): WordBool; safecall;
    function GetFirstErrorPrecedent(Location: CvsCellLocation): CvsCellLocation; safecall;
    procedure GetText(const tw: _TextWriter; Precedents: WordBool; cellRange: CvsCellRange); safecall;
    property ToString: WideString read Get_ToString;
    property Item[Location: CvsCellLocation]: _CvsCellReferenceInfo read Get_Item; default;
    property DialogCells: IEnumerable read Get_DialogCells;
    property ResultSet: _CvsResultSet read Get_ResultSet;
  end;

// *********************************************************************//
// DispIntf:  _CvsCellReferenceTableDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {0F9FDEA5-5FCF-38EA-966B-209108242E8A}
// *********************************************************************//
  _CvsCellReferenceTableDisp = dispinterface
    ['{0F9FDEA5-5FCF-38EA-966B-209108242E8A}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 1610743810;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    property Item[Location: {??CvsCellLocation}OleVariant]: _CvsCellReferenceInfo readonly dispid 0; default;
    property DialogCells: IEnumerable readonly dispid 1610743813;
    procedure GhostMethod__CvsCellReferenceTable_52_1; dispid 1610743814;
    property ResultSet: _CvsResultSet readonly dispid 1610743815;
    function GetCellSymbolicName(Location: {??CvsCellLocation}OleVariant): WideString; dispid 1610743816;
    function Update(const ResultSet: _CvsResultSet; const inSight: _CvsInSight): WordBool; dispid 1610743817;
    function Update_2(const ResultSet: _CvsResultSet; const functionTable: IUnknown): WordBool; dispid 1610743818;
    function Update_3(const ResultSet: _CvsResultSet; const functionTable: IUnknown; sheet: Integer): WordBool; dispid 1610743819;
    function GetFirstErrorPrecedent(Location: {??CvsCellLocation}OleVariant): {??CvsCellLocation}OleVariant; dispid 1610743820;
    procedure GetText(const tw: _TextWriter; Precedents: WordBool; 
                      cellRange: {??CvsCellRange}OleVariant); dispid 1610743821;
  end;

// *********************************************************************//
// Interface: _CvsDefaultRegion
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {4CD3CAE1-5503-385F-8406-70C00852C0A5}
// *********************************************************************//
  _CvsDefaultRegion = interface(IDispatch)
    ['{4CD3CAE1-5503-385F-8406-70C00852C0A5}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Get_X: Integer; safecall;
    function Get_Y: Integer; safecall;
    function Get_Width: Integer; safecall;
    function Get_Height: Integer; safecall;
    property ToString: WideString read Get_ToString;
    property X: Integer read Get_X;
    property Y: Integer read Get_Y;
    property Width: Integer read Get_Width;
    property Height: Integer read Get_Height;
  end;

// *********************************************************************//
// DispIntf:  _CvsDefaultRegionDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {4CD3CAE1-5503-385F-8406-70C00852C0A5}
// *********************************************************************//
  _CvsDefaultRegionDisp = dispinterface
    ['{4CD3CAE1-5503-385F-8406-70C00852C0A5}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    property X: Integer readonly dispid 1610743812;
    property Y: Integer readonly dispid 1610743813;
    property Width: Integer readonly dispid 1610743814;
    property Height: Integer readonly dispid 1610743815;
  end;

// *********************************************************************//
// Interface: _CvsEasyView
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {B8BBEA0D-1383-3C0E-BB05-68F9D4326791}
// *********************************************************************//
  _CvsEasyView = interface(IDispatch)
    ['{B8BBEA0D-1383-3C0E-BB05-68F9D4326791}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    procedure GhostMethod__CvsEasyView_44_1; safecall;
    function Get_Count: Integer; safecall;
    function Get_Item(index: Integer): _CvsEasyViewItem; safecall;
    procedure _Set_Item(index: Integer; const pRetVal: _CvsEasyViewItem); safecall;
    procedure Clear; safecall;
    function IsTagAllowedInEasyView(const Tag: _CvsSymbolicTag): WordBool; safecall;
    procedure Add(const Tag: _CvsSymbolicTag); safecall;
    procedure Add_2(const Tag: _CvsSymbolicTag; const caption: WideString); safecall;
    procedure AddLabel; safecall;
    procedure AddLabel_2(const caption: WideString); safecall;
    procedure AddRange(const tags: _CvsSymbolicTagCollection); safecall;
    procedure Insert(const Tag: _CvsSymbolicTag; index: Integer); safecall;
    procedure Insert_2(const Tag: _CvsSymbolicTag; const caption: WideString; index: Integer); safecall;
    procedure InsertLabel(index: Integer); safecall;
    procedure InsertLabel_2(const caption: WideString; index: Integer); safecall;
    procedure InsertRange(index: Integer; const tags: _CvsSymbolicTagCollection); safecall;
    function IndexOf(const Item: _CvsEasyViewItem): Integer; safecall;
    function IndexOf_2(const Item: _CvsEasyViewItem; index: Integer): Integer; safecall;
    function IndexOf_3(const Item: _CvsEasyViewItem; index: Integer; Count: Integer): Integer; safecall;
    function IndexOf_4(const Tag: _CvsSymbolicTag): Integer; safecall;
    function IndexOf_5(const Tag: _CvsSymbolicTag; index: Integer): Integer; safecall;
    function IndexOf_6(const Tag: _CvsSymbolicTag; index: Integer; Count: Integer): Integer; safecall;
    function Contains(const Item: _CvsEasyViewItem): WordBool; safecall;
    function Contains_2(const Tag: _CvsSymbolicTag): WordBool; safecall;
    procedure RemoveAt(index: Integer); safecall;
    function Remove(const Item: _CvsEasyViewItem): WordBool; safecall;
    function Remove_2(const Tag: _CvsSymbolicTag): WordBool; safecall;
    property ToString: WideString read Get_ToString;
    property Count: Integer read Get_Count;
    property Item[index: Integer]: _CvsEasyViewItem read Get_Item write _Set_Item; default;
  end;

// *********************************************************************//
// DispIntf:  _CvsEasyViewDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {B8BBEA0D-1383-3C0E-BB05-68F9D4326791}
// *********************************************************************//
  _CvsEasyViewDisp = dispinterface
    ['{B8BBEA0D-1383-3C0E-BB05-68F9D4326791}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 1610743810;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    procedure GhostMethod__CvsEasyView_44_1; dispid 1610743812;
    property Count: Integer readonly dispid 1610743813;
    property Item[index: Integer]: _CvsEasyViewItem dispid 0; default;
    procedure Clear; dispid 1610743816;
    function IsTagAllowedInEasyView(const Tag: _CvsSymbolicTag): WordBool; dispid 1610743817;
    procedure Add(const Tag: _CvsSymbolicTag); dispid 1610743818;
    procedure Add_2(const Tag: _CvsSymbolicTag; const caption: WideString); dispid 1610743819;
    procedure AddLabel; dispid 1610743820;
    procedure AddLabel_2(const caption: WideString); dispid 1610743821;
    procedure AddRange(const tags: _CvsSymbolicTagCollection); dispid 1610743822;
    procedure Insert(const Tag: _CvsSymbolicTag; index: Integer); dispid 1610743823;
    procedure Insert_2(const Tag: _CvsSymbolicTag; const caption: WideString; index: Integer); dispid 1610743824;
    procedure InsertLabel(index: Integer); dispid 1610743825;
    procedure InsertLabel_2(const caption: WideString; index: Integer); dispid 1610743826;
    procedure InsertRange(index: Integer; const tags: _CvsSymbolicTagCollection); dispid 1610743827;
    function IndexOf(const Item: _CvsEasyViewItem): Integer; dispid 1610743828;
    function IndexOf_2(const Item: _CvsEasyViewItem; index: Integer): Integer; dispid 1610743829;
    function IndexOf_3(const Item: _CvsEasyViewItem; index: Integer; Count: Integer): Integer; dispid 1610743830;
    function IndexOf_4(const Tag: _CvsSymbolicTag): Integer; dispid 1610743831;
    function IndexOf_5(const Tag: _CvsSymbolicTag; index: Integer): Integer; dispid 1610743832;
    function IndexOf_6(const Tag: _CvsSymbolicTag; index: Integer; Count: Integer): Integer; dispid 1610743833;
    function Contains(const Item: _CvsEasyViewItem): WordBool; dispid 1610743834;
    function Contains_2(const Tag: _CvsSymbolicTag): WordBool; dispid 1610743835;
    procedure RemoveAt(index: Integer); dispid 1610743836;
    function Remove(const Item: _CvsEasyViewItem): WordBool; dispid 1610743837;
    function Remove_2(const Tag: _CvsSymbolicTag): WordBool; dispid 1610743838;
  end;

// *********************************************************************//
// Interface: _CvsEasyViewItem
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {7100135C-A3BA-3B2A-BEB7-836E2EC90BF0}
// *********************************************************************//
  _CvsEasyViewItem = interface(IDispatch)
    ['{7100135C-A3BA-3B2A-BEB7-836E2EC90BF0}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Get_Name: WideString; safecall;
    function Get_ReadOnly: WordBool; safecall;
    procedure Set_ReadOnly(pRetVal: WordBool); safecall;
    function Get_DataType: WideString; safecall;
    function Get_Editable: WordBool; safecall;
    procedure Set_Editable(pRetVal: WordBool); safecall;
    function Get_Location: CvsCellLocation; safecall;
    function Get_caption: WideString; safecall;
    procedure Set_caption(const pRetVal: WideString); safecall;
    function Get_ItemType: CvsEasyViewItemType; safecall;
    procedure Set_ItemType(pRetVal: CvsEasyViewItemType); safecall;
    function Get_IsValid: WordBool; safecall;
    property ToString: WideString read Get_ToString;
    property Name: WideString read Get_Name;
    property ReadOnly: WordBool read Get_ReadOnly write Set_ReadOnly;
    property DataType: WideString read Get_DataType;
    property Editable: WordBool read Get_Editable write Set_Editable;
    property Location: CvsCellLocation read Get_Location;
    property caption: WideString read Get_caption write Set_caption;
    property ItemType: CvsEasyViewItemType read Get_ItemType write Set_ItemType;
    property IsValid: WordBool read Get_IsValid;
  end;

// *********************************************************************//
// DispIntf:  _CvsEasyViewItemDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {7100135C-A3BA-3B2A-BEB7-836E2EC90BF0}
// *********************************************************************//
  _CvsEasyViewItemDisp = dispinterface
    ['{7100135C-A3BA-3B2A-BEB7-836E2EC90BF0}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    property Name: WideString readonly dispid 1610743812;
    property ReadOnly: WordBool dispid 1610743813;
    property DataType: WideString readonly dispid 1610743815;
    property Editable: WordBool dispid 1610743816;
    property Location: {??CvsCellLocation}OleVariant readonly dispid 1610743818;
    property caption: WideString dispid 1610743819;
    property ItemType: CvsEasyViewItemType dispid 1610743821;
    property IsValid: WordBool readonly dispid 1610743823;
  end;

// *********************************************************************//
// Interface: _CvsFeatureLicense
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {9CD6199B-6343-3BD3-8038-4F8DA367D226}
// *********************************************************************//
  _CvsFeatureLicense = interface(IDispatch)
    ['{9CD6199B-6343-3BD3-8038-4F8DA367D226}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    property ToString: WideString read Get_ToString;
  end;

// *********************************************************************//
// DispIntf:  _CvsFeatureLicenseDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {9CD6199B-6343-3BD3-8038-4F8DA367D226}
// *********************************************************************//
  _CvsFeatureLicenseDisp = dispinterface
    ['{9CD6199B-6343-3BD3-8038-4F8DA367D226}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
  end;

// *********************************************************************//
// Interface: _CvsIspMessageStore
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {09DCA83D-F627-3EC4-A7D6-818AE78DF571}
// *********************************************************************//
  _CvsIspMessageStore = interface(IDispatch)
    ['{09DCA83D-F627-3EC4-A7D6-818AE78DF571}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    procedure Dispose; safecall;
    property ToString: WideString read Get_ToString;
  end;

// *********************************************************************//
// DispIntf:  _CvsIspMessageStoreDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {09DCA83D-F627-3EC4-A7D6-818AE78DF571}
// *********************************************************************//
  _CvsIspMessageStoreDisp = dispinterface
    ['{09DCA83D-F627-3EC4-A7D6-818AE78DF571}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    procedure Dispose; dispid 1610743812;
  end;

// *********************************************************************//
// Interface: _CvsJobServerSettings
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {E4AD036A-F152-386C-9A15-283DF1C0A7F8}
// *********************************************************************//
  _CvsJobServerSettings = interface(IDispatch)
    ['{E4AD036A-F152-386C-9A15-283DF1C0A7F8}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Clone: OleVariant; safecall;
    function Get_Enabled: WordBool; safecall;
    procedure Set_Enabled(pRetVal: WordBool); safecall;
    function Get_HostName: WideString; safecall;
    procedure Set_HostName(const pRetVal: WideString); safecall;
    function Get_Port: Integer; safecall;
    procedure Set_Port(pRetVal: Integer); safecall;
    function Get_Username: WideString; safecall;
    procedure Set_Username(const pRetVal: WideString); safecall;
    function Get_Password: WideString; safecall;
    procedure Set_Password(const pRetVal: WideString); safecall;
    function Get_Directory: WideString; safecall;
    procedure Set_Directory(const pRetVal: WideString); safecall;
    property ToString: WideString read Get_ToString;
    property Enabled: WordBool read Get_Enabled write Set_Enabled;
    property HostName: WideString read Get_HostName write Set_HostName;
    property Port: Integer read Get_Port write Set_Port;
    property Username: WideString read Get_Username write Set_Username;
    property Password: WideString read Get_Password write Set_Password;
    property Directory: WideString read Get_Directory write Set_Directory;
  end;

// *********************************************************************//
// DispIntf:  _CvsJobServerSettingsDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {E4AD036A-F152-386C-9A15-283DF1C0A7F8}
// *********************************************************************//
  _CvsJobServerSettingsDisp = dispinterface
    ['{E4AD036A-F152-386C-9A15-283DF1C0A7F8}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    function Clone: OleVariant; dispid 1610743812;
    property Enabled: WordBool dispid 1610743813;
    property HostName: WideString dispid 1610743815;
    property Port: Integer dispid 1610743817;
    property Username: WideString dispid 1610743819;
    property Password: WideString dispid 1610743821;
    property Directory: WideString dispid 1610743823;
  end;

// *********************************************************************//
// Interface: _CvsOcrMaxDiagnosticData
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {AF2197DF-BF7F-3559-AABF-F879E704E398}
// *********************************************************************//
  _CvsOcrMaxDiagnosticData = interface(IDispatch)
    ['{AF2197DF-BF7F-3559-AABF-F879E704E398}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Get_MainLineAngle: Single; safecall;
    function Get_MainLineSkew: Single; safecall;
    function Get_MinimumIntercharacterGap: Integer; safecall;
    function Get_MaximumIntercharacterGap: Integer; safecall;
    function Get_MinimumIntracharacterGap: Integer; safecall;
    function Get_MaximumIntracharacterGap: Integer; safecall;
    function Get_MinimumCharacterPitch: Integer; safecall;
    function Get_MaximumCharacterPitch: Integer; safecall;
    function Get_AcceptedFragments: _FragmentStatistics; safecall;
    function Get_RejectedFragments: _FragmentStatistics; safecall;
    function Get_AcceptedCharacters: _CharacterStatistics; safecall;
    function Get_RejectedCharacters: _CharacterStatistics; safecall;
    property ToString: WideString read Get_ToString;
    property MainLineAngle: Single read Get_MainLineAngle;
    property MainLineSkew: Single read Get_MainLineSkew;
    property MinimumIntercharacterGap: Integer read Get_MinimumIntercharacterGap;
    property MaximumIntercharacterGap: Integer read Get_MaximumIntercharacterGap;
    property MinimumIntracharacterGap: Integer read Get_MinimumIntracharacterGap;
    property MaximumIntracharacterGap: Integer read Get_MaximumIntracharacterGap;
    property MinimumCharacterPitch: Integer read Get_MinimumCharacterPitch;
    property MaximumCharacterPitch: Integer read Get_MaximumCharacterPitch;
    property AcceptedFragments: _FragmentStatistics read Get_AcceptedFragments;
    property RejectedFragments: _FragmentStatistics read Get_RejectedFragments;
    property AcceptedCharacters: _CharacterStatistics read Get_AcceptedCharacters;
    property RejectedCharacters: _CharacterStatistics read Get_RejectedCharacters;
  end;

// *********************************************************************//
// DispIntf:  _CvsOcrMaxDiagnosticDataDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {AF2197DF-BF7F-3559-AABF-F879E704E398}
// *********************************************************************//
  _CvsOcrMaxDiagnosticDataDisp = dispinterface
    ['{AF2197DF-BF7F-3559-AABF-F879E704E398}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    property MainLineAngle: Single readonly dispid 1610743812;
    property MainLineSkew: Single readonly dispid 1610743813;
    property MinimumIntercharacterGap: Integer readonly dispid 1610743814;
    property MaximumIntercharacterGap: Integer readonly dispid 1610743815;
    property MinimumIntracharacterGap: Integer readonly dispid 1610743816;
    property MaximumIntracharacterGap: Integer readonly dispid 1610743817;
    property MinimumCharacterPitch: Integer readonly dispid 1610743818;
    property MaximumCharacterPitch: Integer readonly dispid 1610743819;
    property AcceptedFragments: _FragmentStatistics readonly dispid 1610743820;
    property RejectedFragments: _FragmentStatistics readonly dispid 1610743821;
    property AcceptedCharacters: _CharacterStatistics readonly dispid 1610743822;
    property RejectedCharacters: _CharacterStatistics readonly dispid 1610743823;
  end;

// *********************************************************************//
// Interface: _FragmentStatistics
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {EE8CD83B-EEF9-3735-8B52-00CDB503FBE1}
// *********************************************************************//
  _FragmentStatistics = interface(IDispatch)
    ['{EE8CD83B-EEF9-3735-8B52-00CDB503FBE1}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Get_MinimumSize: Integer; safecall;
    function Get_MaximumSize: Integer; safecall;
    function Get_MinimumContrast: Single; safecall;
    function Get_MaximumContrast: Single; safecall;
    function Get_MaximumDistanceToMainline: Single; safecall;
    property ToString: WideString read Get_ToString;
    property MinimumSize: Integer read Get_MinimumSize;
    property MaximumSize: Integer read Get_MaximumSize;
    property MinimumContrast: Single read Get_MinimumContrast;
    property MaximumContrast: Single read Get_MaximumContrast;
    property MaximumDistanceToMainline: Single read Get_MaximumDistanceToMainline;
  end;

// *********************************************************************//
// DispIntf:  _FragmentStatisticsDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {EE8CD83B-EEF9-3735-8B52-00CDB503FBE1}
// *********************************************************************//
  _FragmentStatisticsDisp = dispinterface
    ['{EE8CD83B-EEF9-3735-8B52-00CDB503FBE1}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    property MinimumSize: Integer readonly dispid 1610743812;
    property MaximumSize: Integer readonly dispid 1610743813;
    property MinimumContrast: Single readonly dispid 1610743814;
    property MaximumContrast: Single readonly dispid 1610743815;
    property MaximumDistanceToMainline: Single readonly dispid 1610743816;
  end;

// *********************************************************************//
// Interface: _CharacterStatistics
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {399FF024-59EC-3C17-B517-3546A3CEB45B}
// *********************************************************************//
  _CharacterStatistics = interface(IDispatch)
    ['{399FF024-59EC-3C17-B517-3546A3CEB45B}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Get_MinimumWidth: Integer; safecall;
    function Get_MaximumWidth: Integer; safecall;
    function Get_MinimumHeight: Integer; safecall;
    function Get_MaximumHeight: Integer; safecall;
    function Get_MinimumSize: Integer; safecall;
    function Get_MaximumSize: Integer; safecall;
    property ToString: WideString read Get_ToString;
    property MinimumWidth: Integer read Get_MinimumWidth;
    property MaximumWidth: Integer read Get_MaximumWidth;
    property MinimumHeight: Integer read Get_MinimumHeight;
    property MaximumHeight: Integer read Get_MaximumHeight;
    property MinimumSize: Integer read Get_MinimumSize;
    property MaximumSize: Integer read Get_MaximumSize;
  end;

// *********************************************************************//
// DispIntf:  _CharacterStatisticsDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {399FF024-59EC-3C17-B517-3546A3CEB45B}
// *********************************************************************//
  _CharacterStatisticsDisp = dispinterface
    ['{399FF024-59EC-3C17-B517-3546A3CEB45B}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    property MinimumWidth: Integer readonly dispid 1610743812;
    property MaximumWidth: Integer readonly dispid 1610743813;
    property MinimumHeight: Integer readonly dispid 1610743814;
    property MaximumHeight: Integer readonly dispid 1610743815;
    property MinimumSize: Integer readonly dispid 1610743816;
    property MaximumSize: Integer readonly dispid 1610743817;
  end;

// *********************************************************************//
// Interface: _CvsOcrMaxDiagnostics
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {65B3AA14-99C5-33F2-BC86-F41D16A38013}
// *********************************************************************//
  _CvsOcrMaxDiagnostics = interface(IDispatch)
    ['{65B3AA14-99C5-33F2-BC86-F41D16A38013}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Get_inSight: _CvsInSight; safecall;
    procedure _Set_inSight(const pRetVal: _CvsInSight); safecall;
    function Get_CellLocation: CvsCellLocation; safecall;
    procedure Set_CellLocation(pRetVal: CvsCellLocation); safecall;
    function Get_DiagnosticData: _CvsOcrMaxDiagnosticData; safecall;
    procedure add_DiagnosticDataChanged(const value: _EventHandler); safecall;
    procedure remove_DiagnosticDataChanged(const value: _EventHandler); safecall;
    procedure QueryDiagnostics; safecall;
    property ToString: WideString read Get_ToString;
    property inSight: _CvsInSight read Get_inSight write _Set_inSight;
    property CellLocation: CvsCellLocation read Get_CellLocation write Set_CellLocation;
    property DiagnosticData: _CvsOcrMaxDiagnosticData read Get_DiagnosticData;
  end;

// *********************************************************************//
// DispIntf:  _CvsOcrMaxDiagnosticsDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {65B3AA14-99C5-33F2-BC86-F41D16A38013}
// *********************************************************************//
  _CvsOcrMaxDiagnosticsDisp = dispinterface
    ['{65B3AA14-99C5-33F2-BC86-F41D16A38013}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    property inSight: _CvsInSight dispid 1610743812;
    property CellLocation: {??CvsCellLocation}OleVariant dispid 1610743814;
    property DiagnosticData: _CvsOcrMaxDiagnosticData readonly dispid 1610743816;
    procedure add_DiagnosticDataChanged(const value: _EventHandler); dispid 1610743817;
    procedure remove_DiagnosticDataChanged(const value: _EventHandler); dispid 1610743818;
    procedure QueryDiagnostics; dispid 1610743819;
  end;

// *********************************************************************//
// Interface: ICvsOcrMaxExpressionEditor
// Flags:     (4288) Dual NonExtensible Dispatchable
// GUID:      {F404247C-4F50-4D8C-9750-05D545995D21}
// *********************************************************************//
  ICvsOcrMaxExpressionEditor = interface(IDispatch)
    ['{F404247C-4F50-4D8C-9750-05D545995D21}']
    function Get_ExternalFontLocation: CvsCellLocation; safecall;
    function Get_OcrMaxSettingsLocation: CvsCellLocation; safecall;
    function IsParameterReadOnly(LockableParameter: LockableParameter): WordBool; safecall;
    function Get_ReadOnlyParameters: PSafeArray; safecall;
    function GetLocalizedNameAndHelpText(lockableParam: LockableParameter; out Name: WideString; 
                                         out help: WideString): WordBool; safecall;
    procedure UpdateSettings(const ocrMaxSettings: WideString; 
                             const spacingArguments: _SpaceSettings); safecall;
    property ExternalFontLocation: CvsCellLocation read Get_ExternalFontLocation;
    property OcrMaxSettingsLocation: CvsCellLocation read Get_OcrMaxSettingsLocation;
    property ReadOnlyParameters: PSafeArray read Get_ReadOnlyParameters;
  end;

// *********************************************************************//
// DispIntf:  ICvsOcrMaxExpressionEditorDisp
// Flags:     (4288) Dual NonExtensible Dispatchable
// GUID:      {F404247C-4F50-4D8C-9750-05D545995D21}
// *********************************************************************//
  ICvsOcrMaxExpressionEditorDisp = dispinterface
    ['{F404247C-4F50-4D8C-9750-05D545995D21}']
    property ExternalFontLocation: {??CvsCellLocation}OleVariant readonly dispid 1610743808;
    property OcrMaxSettingsLocation: {??CvsCellLocation}OleVariant readonly dispid 1610743809;
    function IsParameterReadOnly(LockableParameter: LockableParameter): WordBool; dispid 1610743810;
    property ReadOnlyParameters: {??PSafeArray}OleVariant readonly dispid 1610743811;
    function GetLocalizedNameAndHelpText(lockableParam: LockableParameter; out Name: WideString; 
                                         out help: WideString): WordBool; dispid 1610743812;
    procedure UpdateSettings(const ocrMaxSettings: WideString; 
                             const spacingArguments: _SpaceSettings); dispid 1610743813;
  end;

// *********************************************************************//
// Interface: _CvsOcrMaxExpressionEditorBase
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {33C9EAA7-B5CB-3B6F-98A0-4083C985A1FF}
// *********************************************************************//
  _CvsOcrMaxExpressionEditorBase = interface(IDispatch)
    ['{33C9EAA7-B5CB-3B6F-98A0-4083C985A1FF}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Get_ExternalFontLocation: CvsCellLocation; safecall;
    function Get_OcrMaxSettingsLocation: CvsCellLocation; safecall;
    function Get_inSight: _CvsInSight; safecall;
    function Get_CellLocation: CvsCellLocation; safecall;
    function Get_OcrMaxExpression: WideString; safecall;
    property ToString: WideString read Get_ToString;
    property ExternalFontLocation: CvsCellLocation read Get_ExternalFontLocation;
    property OcrMaxSettingsLocation: CvsCellLocation read Get_OcrMaxSettingsLocation;
    property inSight: _CvsInSight read Get_inSight;
    property CellLocation: CvsCellLocation read Get_CellLocation;
    property OcrMaxExpression: WideString read Get_OcrMaxExpression;
  end;

// *********************************************************************//
// DispIntf:  _CvsOcrMaxExpressionEditorBaseDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {33C9EAA7-B5CB-3B6F-98A0-4083C985A1FF}
// *********************************************************************//
  _CvsOcrMaxExpressionEditorBaseDisp = dispinterface
    ['{33C9EAA7-B5CB-3B6F-98A0-4083C985A1FF}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    property ExternalFontLocation: {??CvsCellLocation}OleVariant readonly dispid 1610743812;
    property OcrMaxSettingsLocation: {??CvsCellLocation}OleVariant readonly dispid 1610743813;
    property inSight: _CvsInSight readonly dispid 1610743814;
    property CellLocation: {??CvsCellLocation}OleVariant readonly dispid 1610743815;
    property OcrMaxExpression: WideString readonly dispid 1610743816;
  end;

// *********************************************************************//
// Interface: _CvsOcrMaxSpreadsheetExpressionEditor
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {6554EBF0-A814-3D3C-903D-BC0BB6500537}
// *********************************************************************//
  _CvsOcrMaxSpreadsheetExpressionEditor = interface(IDispatch)
    ['{6554EBF0-A814-3D3C-903D-BC0BB6500537}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Get_ExternalFontLocation: CvsCellLocation; safecall;
    function Get_OcrMaxSettingsLocation: CvsCellLocation; safecall;
    function Get_inSight: _CvsInSight; safecall;
    function Get_CellLocation: CvsCellLocation; safecall;
    function Get_OcrMaxExpression: WideString; safecall;
    function IsParameterReadOnly(LockableParameter: LockableParameter): WordBool; safecall;
    function Get_ReadOnlyParameters: PSafeArray; safecall;
    procedure UpdateSettings(const ocrMaxSettings: WideString; 
                             const spacingArguments: _SpaceSettings); safecall;
    function Get_ArgUpdateDelegate: _ArgUpdateHandler; safecall;
    procedure _Set_ArgUpdateDelegate(const pRetVal: _ArgUpdateHandler); safecall;
    property ToString: WideString read Get_ToString;
    property ExternalFontLocation: CvsCellLocation read Get_ExternalFontLocation;
    property OcrMaxSettingsLocation: CvsCellLocation read Get_OcrMaxSettingsLocation;
    property inSight: _CvsInSight read Get_inSight;
    property CellLocation: CvsCellLocation read Get_CellLocation;
    property OcrMaxExpression: WideString read Get_OcrMaxExpression;
    property ReadOnlyParameters: PSafeArray read Get_ReadOnlyParameters;
    property ArgUpdateDelegate: _ArgUpdateHandler read Get_ArgUpdateDelegate write _Set_ArgUpdateDelegate;
  end;

// *********************************************************************//
// DispIntf:  _CvsOcrMaxSpreadsheetExpressionEditorDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {6554EBF0-A814-3D3C-903D-BC0BB6500537}
// *********************************************************************//
  _CvsOcrMaxSpreadsheetExpressionEditorDisp = dispinterface
    ['{6554EBF0-A814-3D3C-903D-BC0BB6500537}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    property ExternalFontLocation: {??CvsCellLocation}OleVariant readonly dispid 1610743812;
    property OcrMaxSettingsLocation: {??CvsCellLocation}OleVariant readonly dispid 1610743813;
    property inSight: _CvsInSight readonly dispid 1610743814;
    property CellLocation: {??CvsCellLocation}OleVariant readonly dispid 1610743815;
    property OcrMaxExpression: WideString readonly dispid 1610743816;
    function IsParameterReadOnly(LockableParameter: LockableParameter): WordBool; dispid 1610743817;
    property ReadOnlyParameters: {??PSafeArray}OleVariant readonly dispid 1610743818;
    procedure UpdateSettings(const ocrMaxSettings: WideString; 
                             const spacingArguments: _SpaceSettings); dispid 1610743819;
    property ArgUpdateDelegate: _ArgUpdateHandler dispid 1610743820;
  end;

// *********************************************************************//
// Interface: _ArgUpdateHandler
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {8B49AD1C-3A22-34A7-AF5D-3BF7D1E2AFD2}
// *********************************************************************//
  _ArgUpdateHandler = interface(IDispatch)
    ['{8B49AD1C-3A22-34A7-AF5D-3BF7D1E2AFD2}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function GetInvocationList: PSafeArray; safecall;
    function Clone: OleVariant; safecall;
    procedure GetObjectData(const info: _SerializationInfo; context: StreamingContext); safecall;
    function DynamicInvoke(args: PSafeArray): OleVariant; safecall;
    function Get_Method: _MethodInfo; safecall;
    function Get_Target: OleVariant; safecall;
    procedure Invoke_2; safecall;
    function BeginInvoke(const callback: _AsyncCallback; object_: OleVariant): IAsyncResult; safecall;
    procedure EndInvoke(const result: IAsyncResult); safecall;
    property ToString: WideString read Get_ToString;
    property Method: _MethodInfo read Get_Method;
    property Target: OleVariant read Get_Target;
  end;

// *********************************************************************//
// DispIntf:  _ArgUpdateHandlerDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {8B49AD1C-3A22-34A7-AF5D-3BF7D1E2AFD2}
// *********************************************************************//
  _ArgUpdateHandlerDisp = dispinterface
    ['{8B49AD1C-3A22-34A7-AF5D-3BF7D1E2AFD2}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    function GetInvocationList: {??PSafeArray}OleVariant; dispid 1610743812;
    function Clone: OleVariant; dispid 1610743813;
    procedure GetObjectData(const info: _SerializationInfo; context: {??StreamingContext}OleVariant); dispid 1610743814;
    function DynamicInvoke(args: {??PSafeArray}OleVariant): OleVariant; dispid 1610743815;
    property Method: _MethodInfo readonly dispid 1610743816;
    property Target: OleVariant readonly dispid 1610743817;
    procedure Invoke_2; dispid 1610743818;
    function BeginInvoke(const callback: _AsyncCallback; object_: OleVariant): IAsyncResult; dispid 1610743819;
    procedure EndInvoke(const result: IAsyncResult); dispid 1610743820;
  end;

// *********************************************************************//
// Interface: _CvsPowerlinkSettings
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {61ADD365-19DA-36EB-B784-CBFBF745A63D}
// *********************************************************************//
  _CvsPowerlinkSettings = interface(IDispatch)
    ['{61ADD365-19DA-36EB-B784-CBFBF745A63D}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Clone: OleVariant; safecall;
    function Get_NodeID: Integer; safecall;
    procedure Set_NodeID(pRetVal: Integer); safecall;
    property ToString: WideString read Get_ToString;
    property NodeID: Integer read Get_NodeID write Set_NodeID;
  end;

// *********************************************************************//
// DispIntf:  _CvsPowerlinkSettingsDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {61ADD365-19DA-36EB-B784-CBFBF745A63D}
// *********************************************************************//
  _CvsPowerlinkSettingsDisp = dispinterface
    ['{61ADD365-19DA-36EB-B784-CBFBF745A63D}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    function Clone: OleVariant; dispid 1610743812;
    property NodeID: Integer dispid 1610743813;
  end;

// *********************************************************************//
// Interface: _CvsRemoteSubnetCollection
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {0A67ED61-DB3A-37AE-87E8-D76919CB769F}
// *********************************************************************//
  _CvsRemoteSubnetCollection = interface(IDispatch)
    ['{0A67ED61-DB3A-37AE-87E8-D76919CB769F}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Get_Count: Integer; safecall;
    procedure Add(const Item: _CvsRemoteSubnetEntry); safecall;
    procedure Clear; safecall;
    function Contains(const Item: _CvsRemoteSubnetEntry): WordBool; safecall;
    procedure CopyTo(array_: PSafeArray; arrayIndex: Integer); safecall;
    function Remove(const Item: _CvsRemoteSubnetEntry): WordBool; safecall;
    procedure GhostMethod__CvsRemoteSubnetCollection_68_1; safecall;
    function Clone: OleVariant; safecall;
    function Get_Item(index: Integer): _CvsRemoteSubnetEntry; safecall;
    property ToString: WideString read Get_ToString;
    property Count: Integer read Get_Count;
    property Item[index: Integer]: _CvsRemoteSubnetEntry read Get_Item; default;
  end;

// *********************************************************************//
// DispIntf:  _CvsRemoteSubnetCollectionDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {0A67ED61-DB3A-37AE-87E8-D76919CB769F}
// *********************************************************************//
  _CvsRemoteSubnetCollectionDisp = dispinterface
    ['{0A67ED61-DB3A-37AE-87E8-D76919CB769F}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 1610743810;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    property Count: Integer readonly dispid 1610743812;
    procedure Add(const Item: _CvsRemoteSubnetEntry); dispid 1610743813;
    procedure Clear; dispid 1610743814;
    function Contains(const Item: _CvsRemoteSubnetEntry): WordBool; dispid 1610743815;
    procedure CopyTo(array_: {??PSafeArray}OleVariant; arrayIndex: Integer); dispid 1610743816;
    function Remove(const Item: _CvsRemoteSubnetEntry): WordBool; dispid 1610743817;
    procedure GhostMethod__CvsRemoteSubnetCollection_68_1; dispid 1610743818;
    function Clone: OleVariant; dispid 1610743819;
    property Item[index: Integer]: _CvsRemoteSubnetEntry readonly dispid 0; default;
  end;

// *********************************************************************//
// Interface: _CvsRemoteSubnetEntry
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {01CF13EA-E55E-31F1-AF6B-87B8CC131012}
// *********************************************************************//
  _CvsRemoteSubnetEntry = interface(IDispatch)
    ['{01CF13EA-E55E-31F1-AF6B-87B8CC131012}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Clone: OleVariant; safecall;
    function Get_Name: WideString; safecall;
    procedure Set_Name(const pRetVal: WideString); safecall;
    function Get_CustomGatewayEnabled: WordBool; safecall;
    procedure Set_CustomGatewayEnabled(pRetVal: WordBool); safecall;
    function Matches(const other: _CvsRemoteSubnetEntry): WordBool; safecall;
    property ToString: WideString read Get_ToString;
    property Name: WideString read Get_Name write Set_Name;
    property CustomGatewayEnabled: WordBool read Get_CustomGatewayEnabled write Set_CustomGatewayEnabled;
  end;

// *********************************************************************//
// DispIntf:  _CvsRemoteSubnetEntryDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {01CF13EA-E55E-31F1-AF6B-87B8CC131012}
// *********************************************************************//
  _CvsRemoteSubnetEntryDisp = dispinterface
    ['{01CF13EA-E55E-31F1-AF6B-87B8CC131012}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    function Clone: OleVariant; dispid 1610743812;
    property Name: WideString dispid 1610743813;
    property CustomGatewayEnabled: WordBool dispid 1610743815;
    function Matches(const other: _CvsRemoteSubnetEntry): WordBool; dispid 1610743817;
  end;

// *********************************************************************//
// Interface: _CvsSensor
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {768D28B1-8C55-383E-B409-DCFA8954A4AE}
// *********************************************************************//
  _CvsSensor = interface(IDispatch)
    ['{768D28B1-8C55-383E-B409-DCFA8954A4AE}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function GetLifetimeService: OleVariant; safecall;
    function InitializeLifetimeService: OleVariant; safecall;
    function CreateObjRef(const requestedType: _Type): _ObjRef; safecall;
    function Get_DiscreteIOSupported: WordBool; safecall;
    function Get_ExternalLightSettingsSupported: WordBool; safecall;
    function Get_ImportExportSupported: WordBool; safecall;
    function Get_LicensingSupported: WordBool; safecall;
    function Get_MultiSheetSupported: WordBool; safecall;
    function Get_PowerlinkSettingsSupported: WordBool; safecall;
    function Get_RamDiskSupported: WordBool; safecall;
    function Get_RecordModeOnSensorSupported: WordBool; safecall;
    function SerialPortSupported(Port: Integer): WordBool; safecall;
    function Get_SntpSupported: WordBool; safecall;
    function Get_SpreadsheetSupported: WordBool; safecall;
    function SupportsTool(tool: CvsSupportedTools): WordBool; safecall;
    function Get_ScriptingSupported: WordBool; safecall;
    function Get_IsYaskawa: WordBool; safecall;
    function Get_Is2000: WordBool; safecall;
    function Get_Version: _CvsHostVersion; safecall;
    function Get_ModelNumber: WideString; safecall;
    function Get_SystemType: CvsHostType; safecall;
    function Get_SystemSubtype: CvsHostSubtype; safecall;
    function Get_MacAddress: WideString; safecall;
    function Get_SerialNumber: WideString; safecall;
    function Get_FirmwareUpgradeFilename: WideString; safecall;
    function Get_FirmwareUpgradeReconnectDelay: Integer; safecall;
    function Get_IsColor: WordBool; safecall;
    function Get_AuditMessagesSupported: WordBool; safecall;
    function Get_EncoderOutputSupported: WordBool; safecall;
    function Get_BufferSize: Integer; safecall;
    function Get_HostName: WideString; safecall;
    function Get_RemoteJobServerSupported: WordBool; safecall;
    function GetSettings: _CvsSettings; safecall;
    function SetSettings(const settings: _CvsSettings): WordBool; safecall;
    procedure SetTimeDateData(const data: _CvsTimeDateData); safecall;
    function GetSntpCurrentData: _CvsSntpCurrentData; safecall;
    function GetTimeZones: _CvsTimeZones; safecall;
    function GetDefaultRegion: _CvsDefaultRegion; safecall;
    function Get_MaxCellNameLength: Integer; safecall;
    procedure ValidateCellName(const Name: WideString); safecall;
    function Get_ProcessorConstants: _CvsProcessorConstants; safecall;
    function Get_MaxUserListLength: Integer; safecall;
    function Get_FirmwareDrivenTestRunSupported: WordBool; safecall;
    function Get_SupportedProtocols: CvsProtocolServices; safecall;
    property ToString: WideString read Get_ToString;
    property DiscreteIOSupported: WordBool read Get_DiscreteIOSupported;
    property ExternalLightSettingsSupported: WordBool read Get_ExternalLightSettingsSupported;
    property ImportExportSupported: WordBool read Get_ImportExportSupported;
    property LicensingSupported: WordBool read Get_LicensingSupported;
    property MultiSheetSupported: WordBool read Get_MultiSheetSupported;
    property PowerlinkSettingsSupported: WordBool read Get_PowerlinkSettingsSupported;
    property RamDiskSupported: WordBool read Get_RamDiskSupported;
    property RecordModeOnSensorSupported: WordBool read Get_RecordModeOnSensorSupported;
    property SntpSupported: WordBool read Get_SntpSupported;
    property SpreadsheetSupported: WordBool read Get_SpreadsheetSupported;
    property ScriptingSupported: WordBool read Get_ScriptingSupported;
    property IsYaskawa: WordBool read Get_IsYaskawa;
    property Is2000: WordBool read Get_Is2000;
    property Version: _CvsHostVersion read Get_Version;
    property ModelNumber: WideString read Get_ModelNumber;
    property SystemType: CvsHostType read Get_SystemType;
    property SystemSubtype: CvsHostSubtype read Get_SystemSubtype;
    property MacAddress: WideString read Get_MacAddress;
    property SerialNumber: WideString read Get_SerialNumber;
    property FirmwareUpgradeFilename: WideString read Get_FirmwareUpgradeFilename;
    property FirmwareUpgradeReconnectDelay: Integer read Get_FirmwareUpgradeReconnectDelay;
    property IsColor: WordBool read Get_IsColor;
    property AuditMessagesSupported: WordBool read Get_AuditMessagesSupported;
    property EncoderOutputSupported: WordBool read Get_EncoderOutputSupported;
    property BufferSize: Integer read Get_BufferSize;
    property HostName: WideString read Get_HostName;
    property RemoteJobServerSupported: WordBool read Get_RemoteJobServerSupported;
    property MaxCellNameLength: Integer read Get_MaxCellNameLength;
    property ProcessorConstants: _CvsProcessorConstants read Get_ProcessorConstants;
    property MaxUserListLength: Integer read Get_MaxUserListLength;
    property FirmwareDrivenTestRunSupported: WordBool read Get_FirmwareDrivenTestRunSupported;
    property SupportedProtocols: CvsProtocolServices read Get_SupportedProtocols;
  end;

// *********************************************************************//
// DispIntf:  _CvsSensorDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {768D28B1-8C55-383E-B409-DCFA8954A4AE}
// *********************************************************************//
  _CvsSensorDisp = dispinterface
    ['{768D28B1-8C55-383E-B409-DCFA8954A4AE}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    function GetLifetimeService: OleVariant; dispid 1610743812;
    function InitializeLifetimeService: OleVariant; dispid 1610743813;
    function CreateObjRef(const requestedType: _Type): _ObjRef; dispid 1610743814;
    property DiscreteIOSupported: WordBool readonly dispid 1610743815;
    property ExternalLightSettingsSupported: WordBool readonly dispid 1610743816;
    property ImportExportSupported: WordBool readonly dispid 1610743817;
    property LicensingSupported: WordBool readonly dispid 1610743818;
    property MultiSheetSupported: WordBool readonly dispid 1610743819;
    property PowerlinkSettingsSupported: WordBool readonly dispid 1610743820;
    property RamDiskSupported: WordBool readonly dispid 1610743821;
    property RecordModeOnSensorSupported: WordBool readonly dispid 1610743822;
    function SerialPortSupported(Port: Integer): WordBool; dispid 1610743823;
    property SntpSupported: WordBool readonly dispid 1610743824;
    property SpreadsheetSupported: WordBool readonly dispid 1610743825;
    function SupportsTool(tool: CvsSupportedTools): WordBool; dispid 1610743826;
    property ScriptingSupported: WordBool readonly dispid 1610743827;
    property IsYaskawa: WordBool readonly dispid 1610743828;
    property Is2000: WordBool readonly dispid 1610743829;
    property Version: _CvsHostVersion readonly dispid 1610743830;
    property ModelNumber: WideString readonly dispid 1610743831;
    property SystemType: CvsHostType readonly dispid 1610743832;
    property SystemSubtype: CvsHostSubtype readonly dispid 1610743833;
    property MacAddress: WideString readonly dispid 1610743834;
    property SerialNumber: WideString readonly dispid 1610743835;
    property FirmwareUpgradeFilename: WideString readonly dispid 1610743836;
    property FirmwareUpgradeReconnectDelay: Integer readonly dispid 1610743837;
    property IsColor: WordBool readonly dispid 1610743838;
    property AuditMessagesSupported: WordBool readonly dispid 1610743839;
    property EncoderOutputSupported: WordBool readonly dispid 1610743840;
    property BufferSize: Integer readonly dispid 1610743841;
    property HostName: WideString readonly dispid 1610743842;
    property RemoteJobServerSupported: WordBool readonly dispid 1610743843;
    function GetSettings: _CvsSettings; dispid 1610743844;
    function SetSettings(const settings: _CvsSettings): WordBool; dispid 1610743845;
    procedure SetTimeDateData(const data: _CvsTimeDateData); dispid 1610743846;
    function GetSntpCurrentData: _CvsSntpCurrentData; dispid 1610743847;
    function GetTimeZones: _CvsTimeZones; dispid 1610743848;
    function GetDefaultRegion: _CvsDefaultRegion; dispid 1610743849;
    property MaxCellNameLength: Integer readonly dispid 1610743850;
    procedure ValidateCellName(const Name: WideString); dispid 1610743851;
    property ProcessorConstants: _CvsProcessorConstants readonly dispid 1610743852;
    property MaxUserListLength: Integer readonly dispid 1610743853;
    property FirmwareDrivenTestRunSupported: WordBool readonly dispid 1610743854;
    property SupportedProtocols: CvsProtocolServices readonly dispid 1610743855;
  end;

// *********************************************************************//
// Interface: _CvsUser
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {EE3D71F3-7D97-3E28-A715-75B214A2FDEB}
// *********************************************************************//
  _CvsUser = interface(IDispatch)
    ['{EE3D71F3-7D97-3E28-A715-75B214A2FDEB}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Clone: OleVariant; safecall;
    procedure Initialize(const Name: WideString; const Password: WideString; 
                         access: CvsInSightSecurityAccess; ftpReadEnabled: WordBool; 
                         ftpWriteEnabled: WordBool; customViewDefaultEnabled: WordBool); safecall;
    function Get_Name: WideString; safecall;
    procedure Set_Name(const pRetVal: WideString); safecall;
    function Get_Password: WideString; safecall;
    procedure Set_Password(const pRetVal: WideString); safecall;
    function Get_access: CvsInSightSecurityAccess; safecall;
    procedure Set_access(pRetVal: CvsInSightSecurityAccess); safecall;
    function Get_ftpReadEnabled: WordBool; safecall;
    procedure Set_ftpReadEnabled(pRetVal: WordBool); safecall;
    function Get_ftpWriteEnabled: WordBool; safecall;
    procedure Set_ftpWriteEnabled(pRetVal: WordBool); safecall;
    function Get_customViewDefaultEnabled: WordBool; safecall;
    procedure Set_customViewDefaultEnabled(pRetVal: WordBool); safecall;
    function Get_OnlineOfflineEnabled: WordBool; safecall;
    procedure Set_OnlineOfflineEnabled(pRetVal: WordBool); safecall;
    function Get_OnlineJobSaveEnabled: WordBool; safecall;
    procedure Set_OnlineJobSaveEnabled(pRetVal: WordBool); safecall;
    property ToString: WideString read Get_ToString;
    property Name: WideString read Get_Name write Set_Name;
    property Password: WideString read Get_Password write Set_Password;
    property access: CvsInSightSecurityAccess read Get_access write Set_access;
    property ftpReadEnabled: WordBool read Get_ftpReadEnabled write Set_ftpReadEnabled;
    property ftpWriteEnabled: WordBool read Get_ftpWriteEnabled write Set_ftpWriteEnabled;
    property customViewDefaultEnabled: WordBool read Get_customViewDefaultEnabled write Set_customViewDefaultEnabled;
    property OnlineOfflineEnabled: WordBool read Get_OnlineOfflineEnabled write Set_OnlineOfflineEnabled;
    property OnlineJobSaveEnabled: WordBool read Get_OnlineJobSaveEnabled write Set_OnlineJobSaveEnabled;
  end;

// *********************************************************************//
// DispIntf:  _CvsUserDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {EE3D71F3-7D97-3E28-A715-75B214A2FDEB}
// *********************************************************************//
  _CvsUserDisp = dispinterface
    ['{EE3D71F3-7D97-3E28-A715-75B214A2FDEB}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    function Clone: OleVariant; dispid 1610743812;
    procedure Initialize(const Name: WideString; const Password: WideString; 
                         access: CvsInSightSecurityAccess; ftpReadEnabled: WordBool; 
                         ftpWriteEnabled: WordBool; customViewDefaultEnabled: WordBool); dispid 1610743813;
    property Name: WideString dispid 1610743814;
    property Password: WideString dispid 1610743816;
    property access: CvsInSightSecurityAccess dispid 1610743818;
    property ftpReadEnabled: WordBool dispid 1610743820;
    property ftpWriteEnabled: WordBool dispid 1610743822;
    property customViewDefaultEnabled: WordBool dispid 1610743824;
    property OnlineOfflineEnabled: WordBool dispid 1610743826;
    property OnlineJobSaveEnabled: WordBool dispid 1610743828;
  end;

// *********************************************************************//
// Interface: _CvsInSightExtensions
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {E7A9EF7A-D411-3265-B924-402262C986F8}
// *********************************************************************//
  _CvsInSightExtensions = interface(IDispatch)
    ['{E7A9EF7A-D411-3265-B924-402262C986F8}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    property ToString: WideString read Get_ToString;
  end;

// *********************************************************************//
// DispIntf:  _CvsInSightExtensionsDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {E7A9EF7A-D411-3265-B924-402262C986F8}
// *********************************************************************//
  _CvsInSightExtensionsDisp = dispinterface
    ['{E7A9EF7A-D411-3265-B924-402262C986F8}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
  end;

// *********************************************************************//
// Interface: _CvsSettings
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {42C74EC2-8B77-3621-8C5F-361776D7902E}
// *********************************************************************//
  _CvsSettings = interface(IDispatch)
    ['{42C74EC2-8B77-3621-8C5F-361776D7902E}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Get_DiscreteIOSupported: WordBool; safecall;
    function Get_EipWatchdogTimeoutActionSupported: WordBool; safecall;
    function Get_ImportExportSupported: WordBool; safecall;
    function Get_LicensingSupported: WordBool; safecall;
    function Get_ModbusSettingsSupported: WordBool; safecall;
    function Get_OnlineImageResolutionSupported: WordBool; safecall;
    function Get_PowerlinkSettingsSupported: WordBool; safecall;
    function Get_ProtocolServiceSupported: WordBool; safecall;
    function Get_RamDiskSupported: WordBool; safecall;
    function Get_RecordModeOnSensorSupported: WordBool; safecall;
    function SerialPortSupported(Port: Integer): WordBool; safecall;
    function Get_SntpSupported: WordBool; safecall;
    function Get_TelnetPortSupported: WordBool; safecall;
    function Get_ScriptingSupported: WordBool; safecall;
    function Clone: OleVariant; safecall;
    function Get_ActiveIOModule: CvsIOModule; safecall;
    procedure Set_ActiveIOModule(pRetVal: CvsIOModule); safecall;
    function Get_ActiveIOModuleLines: _CvsDiscreteIOLines; safecall;
    function Get_ConnectedIOModule: CvsIOModule; safecall;
    function Get_RamDiskSize: Integer; safecall;
    procedure Set_RamDiskSize(pRetVal: Integer); safecall;
    function Get_StartupJob: WideString; safecall;
    procedure Set_StartupJob(const pRetVal: WideString); safecall;
    function Get_StartupOnline: WordBool; safecall;
    procedure Set_StartupOnline(pRetVal: WordBool); safecall;
    function Get_SerialPort0Settings: _CvsSerialSettings; safecall;
    function Get_SerialPort1Settings: _CvsSerialSettings; safecall;
    function Get_UniversalIOSupported: WordBool; safecall;
    function Get_DeEnergizeOutputsWhileOffline: WordBool; safecall;
    procedure Set_DeEnergizeOutputsWhileOffline(pRetVal: WordBool); safecall;
    function Get_DeEnergizeOutputsWhileOfflineSupported: WordBool; safecall;
    function Get_EthernetIOAddress: WideString; safecall;
    procedure Set_EthernetIOAddress(const pRetVal: WideString); safecall;
    function Get_EthernetIOPollRate: Integer; safecall;
    procedure Set_EthernetIOPollRate(pRetVal: Integer); safecall;
    function Get_MaxEthernetIOUpdateRate: Integer; safecall;
    function Get_EthernetIOInactivePollRate: Integer; safecall;
    procedure Set_EthernetIOInactivePollRate(pRetVal: Integer); safecall;
    function Get_ProtectedUserOnlineOfflineSupported: WordBool; safecall;
    function Get_LocalIOSettings: _CvsLocalIOSettings; safecall;
    function Get_ExpansionIOSettings: _CvsExpansionIOSettings; safecall;
    function Get_UniversalIOSettings: _CvsUniversalIOSettings; safecall;
    function Get_EthernetIOSettings: _CvsExpansionIOSettings; safecall;
    function Get_NetworkSettings: _CvsNetworkSettings; safecall;
    procedure GhostMethod__CvsSettings_220_1; safecall;
    function GetNetworkSettings(const Name: WideString): _CvsNetworkSettings; safecall;
    function Get_AuditMessageSettings: _CvsAuditMessageSettings; safecall;
    function Get_CdsSettings: IUnknown; safecall;
    function Get_JobServerSettings: _CvsJobServerSettings; safecall;
    function Get_SntpSettings: _CvsSntpSettings; safecall;
    function Get_ExpansionIOMessagingSupported: WordBool; safecall;
    function Get_EthernetIOSupported: WordBool; safecall;
    function Get_SerialSupportOnIO_1: WordBool; safecall;
    function Get_SerialSupportOnIO: WordBool; safecall;
    function Get_EncoderSupportOnIO_2and3: WordBool; safecall;
    function Get_EncoderOutputSupport: WordBool; safecall;
    function Get_EncoderOutputConfigured: WordBool; safecall;
    function GetSupportedIOModules: PSafeArray; safecall;
    function Get_ExpansionIOSupported: WordBool; safecall;
    function Get_HardwareStrobeSupported: WordBool; safecall;
    procedure GetSlowBaudRateSupportedFlags(expansionIOAttached: WordBool; out allow1200: WordBool; 
                                            out allow2400: WordBool); safecall;
    function Get_EthernetIOAddressSupported: WordBool; safecall;
    function Get_AuditMessagesSupported: WordBool; safecall;
    function Get_DiscreteTriggerDelayTimeSupported: WordBool; safecall;
    function Get_PTPTimeSynchSupported: WordBool; safecall;
    function Get_MCProtocolScannerSupported: WordBool; safecall;
    function Get_ProfinetSettingsSupported: WordBool; safecall;
    function Get_OnlineJobSaveSupported: WordBool; safecall;
    function Get_ForceNetworkLinkModeSupported: WordBool; safecall;
    function Get_ResourceMessagesSupported: WordBool; safecall;
    function Get_IOCameraMaskSupported: WordBool; safecall;
    function Get_LiveAcquisitionResolution: CvsImageResolution; safecall;
    procedure Set_LiveAcquisitionResolution1(pRetVal: CvsImageResolution); safecall;
    function Get_LiveAcquisitionMaxImageRate: Single; safecall;
    procedure Set_LiveAcquisitionMaxImageRate1(pRetVal: Single); safecall;
    function Get_OnlineImageResolution: CvsImageResolution; safecall;
    procedure Set_OnlineImageResolution1(pRetVal: CvsImageResolution); safecall;
    function Get_OnlineAcquisitionMaxImageRate: Single; safecall;
    procedure Set_OnlineAcquisitionMaxImageRate1(pRetVal: Single); safecall;
    function Get_OnlineAcquisitionMaxImageRateSupported: WordBool; safecall;
    function Get_OnlineImagesBitDepth: CvsColorBitDepth; safecall;
    procedure Set_OnlineImagesBitDepth1(pRetVal: CvsColorBitDepth); safecall;
    function Get_LiveImagesBitDepth: CvsColorBitDepth; safecall;
    procedure Set_LiveImagesBitDepth1(pRetVal: CvsColorBitDepth); safecall;
    function Get_CdsSupported: WordBool; safecall;
    procedure Set_CdsSupported1(pRetVal: WordBool); safecall;
    function Get_AdminPasswordResetSupported: WordBool; safecall;
    function Get_IsImprovedEncodingSupported: WordBool; safecall;
    function Get_RemoteJobServerSupported: WordBool; safecall;
    function Get_RemoteJobServerDirectorySupported: WordBool; safecall;
    procedure SetIOSettings(const local: _CvsLocalIOSettings; 
                            const expansion: _CvsExpansionIOSettings; 
                            const universal: _CvsUniversalIOSettings; 
                            const ethernet: _CvsExpansionIOSettings; module: CvsIOModule; 
                            deenergize: WordBool); safecall;
    procedure SetSerialPort1Settings(const SerialPort1Settings: _CvsSerialSettings); safecall;
    function GetFeatureFamily: CvsInSightFeatureFamily; safecall;
    property ToString: WideString read Get_ToString;
    property DiscreteIOSupported: WordBool read Get_DiscreteIOSupported;
    property EipWatchdogTimeoutActionSupported: WordBool read Get_EipWatchdogTimeoutActionSupported;
    property ImportExportSupported: WordBool read Get_ImportExportSupported;
    property LicensingSupported: WordBool read Get_LicensingSupported;
    property ModbusSettingsSupported: WordBool read Get_ModbusSettingsSupported;
    property OnlineImageResolutionSupported: WordBool read Get_OnlineImageResolutionSupported;
    property PowerlinkSettingsSupported: WordBool read Get_PowerlinkSettingsSupported;
    property ProtocolServiceSupported: WordBool read Get_ProtocolServiceSupported;
    property RamDiskSupported: WordBool read Get_RamDiskSupported;
    property RecordModeOnSensorSupported: WordBool read Get_RecordModeOnSensorSupported;
    property SntpSupported: WordBool read Get_SntpSupported;
    property TelnetPortSupported: WordBool read Get_TelnetPortSupported;
    property ScriptingSupported: WordBool read Get_ScriptingSupported;
    property ActiveIOModule: CvsIOModule read Get_ActiveIOModule write Set_ActiveIOModule;
    property ActiveIOModuleLines: _CvsDiscreteIOLines read Get_ActiveIOModuleLines;
    property ConnectedIOModule: CvsIOModule read Get_ConnectedIOModule;
    property RamDiskSize: Integer read Get_RamDiskSize write Set_RamDiskSize;
    property StartupJob: WideString read Get_StartupJob write Set_StartupJob;
    property StartupOnline: WordBool read Get_StartupOnline write Set_StartupOnline;
    property SerialPort0Settings: _CvsSerialSettings read Get_SerialPort0Settings;
    property SerialPort1Settings: _CvsSerialSettings read Get_SerialPort1Settings;
    property UniversalIOSupported: WordBool read Get_UniversalIOSupported;
    property DeEnergizeOutputsWhileOffline: WordBool read Get_DeEnergizeOutputsWhileOffline write Set_DeEnergizeOutputsWhileOffline;
    property DeEnergizeOutputsWhileOfflineSupported: WordBool read Get_DeEnergizeOutputsWhileOfflineSupported;
    property EthernetIOAddress: WideString read Get_EthernetIOAddress write Set_EthernetIOAddress;
    property EthernetIOPollRate: Integer read Get_EthernetIOPollRate write Set_EthernetIOPollRate;
    property MaxEthernetIOUpdateRate: Integer read Get_MaxEthernetIOUpdateRate;
    property EthernetIOInactivePollRate: Integer read Get_EthernetIOInactivePollRate write Set_EthernetIOInactivePollRate;
    property ProtectedUserOnlineOfflineSupported: WordBool read Get_ProtectedUserOnlineOfflineSupported;
    property LocalIOSettings: _CvsLocalIOSettings read Get_LocalIOSettings;
    property ExpansionIOSettings: _CvsExpansionIOSettings read Get_ExpansionIOSettings;
    property UniversalIOSettings: _CvsUniversalIOSettings read Get_UniversalIOSettings;
    property EthernetIOSettings: _CvsExpansionIOSettings read Get_EthernetIOSettings;
    property NetworkSettings: _CvsNetworkSettings read Get_NetworkSettings;
    property AuditMessageSettings: _CvsAuditMessageSettings read Get_AuditMessageSettings;
    property CdsSettings: IUnknown read Get_CdsSettings;
    property JobServerSettings: _CvsJobServerSettings read Get_JobServerSettings;
    property SntpSettings: _CvsSntpSettings read Get_SntpSettings;
    property ExpansionIOMessagingSupported: WordBool read Get_ExpansionIOMessagingSupported;
    property EthernetIOSupported: WordBool read Get_EthernetIOSupported;
    property SerialSupportOnIO_1: WordBool read Get_SerialSupportOnIO_1;
    property SerialSupportOnIO: WordBool read Get_SerialSupportOnIO;
    property EncoderSupportOnIO_2and3: WordBool read Get_EncoderSupportOnIO_2and3;
    property EncoderOutputSupport: WordBool read Get_EncoderOutputSupport;
    property EncoderOutputConfigured: WordBool read Get_EncoderOutputConfigured;
    property ExpansionIOSupported: WordBool read Get_ExpansionIOSupported;
    property HardwareStrobeSupported: WordBool read Get_HardwareStrobeSupported;
    property EthernetIOAddressSupported: WordBool read Get_EthernetIOAddressSupported;
    property AuditMessagesSupported: WordBool read Get_AuditMessagesSupported;
    property DiscreteTriggerDelayTimeSupported: WordBool read Get_DiscreteTriggerDelayTimeSupported;
    property PTPTimeSynchSupported: WordBool read Get_PTPTimeSynchSupported;
    property MCProtocolScannerSupported: WordBool read Get_MCProtocolScannerSupported;
    property ProfinetSettingsSupported: WordBool read Get_ProfinetSettingsSupported;
    property OnlineJobSaveSupported: WordBool read Get_OnlineJobSaveSupported;
    property ForceNetworkLinkModeSupported: WordBool read Get_ForceNetworkLinkModeSupported;
    property ResourceMessagesSupported: WordBool read Get_ResourceMessagesSupported;
    property IOCameraMaskSupported: WordBool read Get_IOCameraMaskSupported;
    property LiveAcquisitionResolution: CvsImageResolution read Get_LiveAcquisitionResolution;
    property LiveAcquisitionResolution1: CvsImageResolution write Set_LiveAcquisitionResolution1;
    property LiveAcquisitionMaxImageRate: Single read Get_LiveAcquisitionMaxImageRate;
    property LiveAcquisitionMaxImageRate1: Single write Set_LiveAcquisitionMaxImageRate1;
    property OnlineImageResolution: CvsImageResolution read Get_OnlineImageResolution;
    property OnlineImageResolution1: CvsImageResolution write Set_OnlineImageResolution1;
    property OnlineAcquisitionMaxImageRate: Single read Get_OnlineAcquisitionMaxImageRate;
    property OnlineAcquisitionMaxImageRate1: Single write Set_OnlineAcquisitionMaxImageRate1;
    property OnlineAcquisitionMaxImageRateSupported: WordBool read Get_OnlineAcquisitionMaxImageRateSupported;
    property OnlineImagesBitDepth: CvsColorBitDepth read Get_OnlineImagesBitDepth;
    property OnlineImagesBitDepth1: CvsColorBitDepth write Set_OnlineImagesBitDepth1;
    property LiveImagesBitDepth: CvsColorBitDepth read Get_LiveImagesBitDepth;
    property LiveImagesBitDepth1: CvsColorBitDepth write Set_LiveImagesBitDepth1;
    property CdsSupported: WordBool read Get_CdsSupported;
    property CdsSupported1: WordBool write Set_CdsSupported1;
    property AdminPasswordResetSupported: WordBool read Get_AdminPasswordResetSupported;
    property IsImprovedEncodingSupported: WordBool read Get_IsImprovedEncodingSupported;
    property RemoteJobServerSupported: WordBool read Get_RemoteJobServerSupported;
    property RemoteJobServerDirectorySupported: WordBool read Get_RemoteJobServerDirectorySupported;
  end;

// *********************************************************************//
// DispIntf:  _CvsSettingsDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {42C74EC2-8B77-3621-8C5F-361776D7902E}
// *********************************************************************//
  _CvsSettingsDisp = dispinterface
    ['{42C74EC2-8B77-3621-8C5F-361776D7902E}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    property DiscreteIOSupported: WordBool readonly dispid 1610743812;
    property EipWatchdogTimeoutActionSupported: WordBool readonly dispid 1610743813;
    property ImportExportSupported: WordBool readonly dispid 1610743814;
    property LicensingSupported: WordBool readonly dispid 1610743815;
    property ModbusSettingsSupported: WordBool readonly dispid 1610743816;
    property OnlineImageResolutionSupported: WordBool readonly dispid 1610743817;
    property PowerlinkSettingsSupported: WordBool readonly dispid 1610743818;
    property ProtocolServiceSupported: WordBool readonly dispid 1610743819;
    property RamDiskSupported: WordBool readonly dispid 1610743820;
    property RecordModeOnSensorSupported: WordBool readonly dispid 1610743821;
    function SerialPortSupported(Port: Integer): WordBool; dispid 1610743822;
    property SntpSupported: WordBool readonly dispid 1610743823;
    property TelnetPortSupported: WordBool readonly dispid 1610743824;
    property ScriptingSupported: WordBool readonly dispid 1610743825;
    function Clone: OleVariant; dispid 1610743826;
    property ActiveIOModule: CvsIOModule dispid 1610743827;
    property ActiveIOModuleLines: _CvsDiscreteIOLines readonly dispid 1610743829;
    property ConnectedIOModule: CvsIOModule readonly dispid 1610743830;
    property RamDiskSize: Integer dispid 1610743831;
    property StartupJob: WideString dispid 1610743833;
    property StartupOnline: WordBool dispid 1610743835;
    property SerialPort0Settings: _CvsSerialSettings readonly dispid 1610743837;
    property SerialPort1Settings: _CvsSerialSettings readonly dispid 1610743838;
    property UniversalIOSupported: WordBool readonly dispid 1610743839;
    property DeEnergizeOutputsWhileOffline: WordBool dispid 1610743840;
    property DeEnergizeOutputsWhileOfflineSupported: WordBool readonly dispid 1610743842;
    property EthernetIOAddress: WideString dispid 1610743843;
    property EthernetIOPollRate: Integer dispid 1610743845;
    property MaxEthernetIOUpdateRate: Integer readonly dispid 1610743847;
    property EthernetIOInactivePollRate: Integer dispid 1610743848;
    property ProtectedUserOnlineOfflineSupported: WordBool readonly dispid 1610743850;
    property LocalIOSettings: _CvsLocalIOSettings readonly dispid 1610743851;
    property ExpansionIOSettings: _CvsExpansionIOSettings readonly dispid 1610743852;
    property UniversalIOSettings: _CvsUniversalIOSettings readonly dispid 1610743853;
    property EthernetIOSettings: _CvsExpansionIOSettings readonly dispid 1610743854;
    property NetworkSettings: _CvsNetworkSettings readonly dispid 1610743855;
    procedure GhostMethod__CvsSettings_220_1; dispid 1610743856;
    function GetNetworkSettings(const Name: WideString): _CvsNetworkSettings; dispid 1610743857;
    property AuditMessageSettings: _CvsAuditMessageSettings readonly dispid 1610743858;
    property CdsSettings: IUnknown readonly dispid 1610743859;
    property JobServerSettings: _CvsJobServerSettings readonly dispid 1610743860;
    property SntpSettings: _CvsSntpSettings readonly dispid 1610743861;
    property ExpansionIOMessagingSupported: WordBool readonly dispid 1610743862;
    property EthernetIOSupported: WordBool readonly dispid 1610743863;
    property SerialSupportOnIO_1: WordBool readonly dispid 1610743864;
    property SerialSupportOnIO: WordBool readonly dispid 1610743865;
    property EncoderSupportOnIO_2and3: WordBool readonly dispid 1610743866;
    property EncoderOutputSupport: WordBool readonly dispid 1610743867;
    property EncoderOutputConfigured: WordBool readonly dispid 1610743868;
    function GetSupportedIOModules: {??PSafeArray}OleVariant; dispid 1610743869;
    property ExpansionIOSupported: WordBool readonly dispid 1610743870;
    property HardwareStrobeSupported: WordBool readonly dispid 1610743871;
    procedure GetSlowBaudRateSupportedFlags(expansionIOAttached: WordBool; out allow1200: WordBool; 
                                            out allow2400: WordBool); dispid 1610743872;
    property EthernetIOAddressSupported: WordBool readonly dispid 1610743873;
    property AuditMessagesSupported: WordBool readonly dispid 1610743874;
    property DiscreteTriggerDelayTimeSupported: WordBool readonly dispid 1610743875;
    property PTPTimeSynchSupported: WordBool readonly dispid 1610743876;
    property MCProtocolScannerSupported: WordBool readonly dispid 1610743877;
    property ProfinetSettingsSupported: WordBool readonly dispid 1610743878;
    property OnlineJobSaveSupported: WordBool readonly dispid 1610743879;
    property ForceNetworkLinkModeSupported: WordBool readonly dispid 1610743880;
    property ResourceMessagesSupported: WordBool readonly dispid 1610743881;
    property IOCameraMaskSupported: WordBool readonly dispid 1610743882;
    property LiveAcquisitionResolution: CvsImageResolution readonly dispid 1610743883;
    property LiveAcquisitionResolution1: CvsImageResolution writeonly dispid 1610743884;
    property LiveAcquisitionMaxImageRate: Single readonly dispid 1610743885;
    property LiveAcquisitionMaxImageRate1: Single writeonly dispid 1610743886;
    property OnlineImageResolution: CvsImageResolution readonly dispid 1610743887;
    property OnlineImageResolution1: CvsImageResolution writeonly dispid 1610743888;
    property OnlineAcquisitionMaxImageRate: Single readonly dispid 1610743889;
    property OnlineAcquisitionMaxImageRate1: Single writeonly dispid 1610743890;
    property OnlineAcquisitionMaxImageRateSupported: WordBool readonly dispid 1610743891;
    property OnlineImagesBitDepth: CvsColorBitDepth readonly dispid 1610743892;
    property OnlineImagesBitDepth1: CvsColorBitDepth writeonly dispid 1610743893;
    property LiveImagesBitDepth: CvsColorBitDepth readonly dispid 1610743894;
    property LiveImagesBitDepth1: CvsColorBitDepth writeonly dispid 1610743895;
    property CdsSupported: WordBool readonly dispid 1610743896;
    property CdsSupported1: WordBool writeonly dispid 1610743897;
    property AdminPasswordResetSupported: WordBool readonly dispid 1610743898;
    property IsImprovedEncodingSupported: WordBool readonly dispid 1610743899;
    property RemoteJobServerSupported: WordBool readonly dispid 1610743900;
    property RemoteJobServerDirectorySupported: WordBool readonly dispid 1610743901;
    procedure SetIOSettings(const local: _CvsLocalIOSettings; 
                            const expansion: _CvsExpansionIOSettings; 
                            const universal: _CvsUniversalIOSettings; 
                            const ethernet: _CvsExpansionIOSettings; module: CvsIOModule; 
                            deenergize: WordBool); dispid 1610743902;
    procedure SetSerialPort1Settings(const SerialPort1Settings: _CvsSerialSettings); dispid 1610743903;
    function GetFeatureFamily: CvsInSightFeatureFamily; dispid 1610743904;
  end;

// *********************************************************************//
// Interface: _UndoAction
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {B4DABB56-FED0-324E-BACA-8731148D1999}
// *********************************************************************//
  _UndoAction = interface(IDispatch)
    ['{B4DABB56-FED0-324E-BACA-8731148D1999}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    property ToString: WideString read Get_ToString;
  end;

// *********************************************************************//
// DispIntf:  _UndoActionDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {B4DABB56-FED0-324E-BACA-8731148D1999}
// *********************************************************************//
  _UndoActionDisp = dispinterface
    ['{B4DABB56-FED0-324E-BACA-8731148D1999}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
  end;

// *********************************************************************//
// Interface: _UndoCellTagsAction
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {1F925BF3-94A8-3451-AF7F-B3D1584E70D7}
// *********************************************************************//
  _UndoCellTagsAction = interface(IDispatch)
    ['{1F925BF3-94A8-3451-AF7F-B3D1584E70D7}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Get_Cost: Integer; safecall;
    function Get_Text: WideString; safecall;
    procedure SaveState; safecall;
    procedure Do_; safecall;
    procedure Redo; safecall;
    procedure Undo; safecall;
    property ToString: WideString read Get_ToString;
    property Cost: Integer read Get_Cost;
    property Text: WideString read Get_Text;
  end;

// *********************************************************************//
// DispIntf:  _UndoCellTagsActionDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {1F925BF3-94A8-3451-AF7F-B3D1584E70D7}
// *********************************************************************//
  _UndoCellTagsActionDisp = dispinterface
    ['{1F925BF3-94A8-3451-AF7F-B3D1584E70D7}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    property Cost: Integer readonly dispid 1610743812;
    property Text: WideString readonly dispid 1610743813;
    procedure SaveState; dispid 1610743814;
    procedure Do_; dispid 1610743815;
    procedure Redo; dispid 1610743816;
    procedure Undo; dispid 1610743817;
  end;

// *********************************************************************//
// Interface: _UndoClearAllAction
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {F889BDF4-0EEB-3D75-9A34-4A67A31E182F}
// *********************************************************************//
  _UndoClearAllAction = interface(IDispatch)
    ['{F889BDF4-0EEB-3D75-9A34-4A67A31E182F}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Get_Cost: Integer; safecall;
    function Get_Text: WideString; safecall;
    procedure SaveState; safecall;
    procedure Do_; safecall;
    procedure Redo; safecall;
    procedure Undo; safecall;
    property ToString: WideString read Get_ToString;
    property Cost: Integer read Get_Cost;
    property Text: WideString read Get_Text;
  end;

// *********************************************************************//
// DispIntf:  _UndoClearAllActionDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {F889BDF4-0EEB-3D75-9A34-4A67A31E182F}
// *********************************************************************//
  _UndoClearAllActionDisp = dispinterface
    ['{F889BDF4-0EEB-3D75-9A34-4A67A31E182F}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    property Cost: Integer readonly dispid 1610743812;
    property Text: WideString readonly dispid 1610743813;
    procedure SaveState; dispid 1610743814;
    procedure Do_; dispid 1610743815;
    procedure Redo; dispid 1610743816;
    procedure Undo; dispid 1610743817;
  end;

// *********************************************************************//
// Interface: _UndoCutAndPasteAction
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {3549ABDB-38DA-36B4-AEBE-0FA1BEEB3CF5}
// *********************************************************************//
  _UndoCutAndPasteAction = interface(IDispatch)
    ['{3549ABDB-38DA-36B4-AEBE-0FA1BEEB3CF5}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Get_Cost: Integer; safecall;
    function Get_Text: WideString; safecall;
    procedure SaveState; safecall;
    procedure Do_; safecall;
    procedure Redo; safecall;
    procedure Undo; safecall;
    property ToString: WideString read Get_ToString;
    property Cost: Integer read Get_Cost;
    property Text: WideString read Get_Text;
  end;

// *********************************************************************//
// DispIntf:  _UndoCutAndPasteActionDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {3549ABDB-38DA-36B4-AEBE-0FA1BEEB3CF5}
// *********************************************************************//
  _UndoCutAndPasteActionDisp = dispinterface
    ['{3549ABDB-38DA-36B4-AEBE-0FA1BEEB3CF5}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    property Cost: Integer readonly dispid 1610743812;
    property Text: WideString readonly dispid 1610743813;
    procedure SaveState; dispid 1610743814;
    procedure Do_; dispid 1610743815;
    procedure Redo; dispid 1610743816;
    procedure Undo; dispid 1610743817;
  end;

// *********************************************************************//
// Interface: _UndoEventArgs
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {A5B5906D-27A8-392B-A11C-ECFA005C35C9}
// *********************************************************************//
  _UndoEventArgs = interface(IDispatch)
    ['{A5B5906D-27A8-392B-A11C-ECFA005C35C9}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Get_EventType: UndoEventType; safecall;
    property ToString: WideString read Get_ToString;
    property EventType: UndoEventType read Get_EventType;
  end;

// *********************************************************************//
// DispIntf:  _UndoEventArgsDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {A5B5906D-27A8-392B-A11C-ECFA005C35C9}
// *********************************************************************//
  _UndoEventArgsDisp = dispinterface
    ['{A5B5906D-27A8-392B-A11C-ECFA005C35C9}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    property EventType: UndoEventType readonly dispid 1610743812;
  end;

// *********************************************************************//
// Interface: _UndoExpressionAction
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {FCB28B69-8A40-3CE8-86C8-9FEC7EE91476}
// *********************************************************************//
  _UndoExpressionAction = interface(IDispatch)
    ['{FCB28B69-8A40-3CE8-86C8-9FEC7EE91476}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Get_Cost: Integer; safecall;
    function Get_Text: WideString; safecall;
    procedure SaveState; safecall;
    procedure Do_; safecall;
    procedure Redo; safecall;
    procedure Undo; safecall;
    property ToString: WideString read Get_ToString;
    property Cost: Integer read Get_Cost;
    property Text: WideString read Get_Text;
  end;

// *********************************************************************//
// DispIntf:  _UndoExpressionActionDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {FCB28B69-8A40-3CE8-86C8-9FEC7EE91476}
// *********************************************************************//
  _UndoExpressionActionDisp = dispinterface
    ['{FCB28B69-8A40-3CE8-86C8-9FEC7EE91476}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    property Cost: Integer readonly dispid 1610743812;
    property Text: WideString readonly dispid 1610743813;
    procedure SaveState; dispid 1610743814;
    procedure Do_; dispid 1610743815;
    procedure Redo; dispid 1610743816;
    procedure Undo; dispid 1610743817;
  end;

// *********************************************************************//
// Interface: _UndoOpcTagEditorAction
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {73358417-1DAD-3136-9085-5C4E9400191A}
// *********************************************************************//
  _UndoOpcTagEditorAction = interface(IDispatch)
    ['{73358417-1DAD-3136-9085-5C4E9400191A}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Get_Cost: Integer; safecall;
    function Get_Text: WideString; safecall;
    procedure SaveState; safecall;
    procedure Do_; safecall;
    procedure Redo; safecall;
    procedure Undo; safecall;
    property ToString: WideString read Get_ToString;
    property Cost: Integer read Get_Cost;
    property Text: WideString read Get_Text;
  end;

// *********************************************************************//
// DispIntf:  _UndoOpcTagEditorActionDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {73358417-1DAD-3136-9085-5C4E9400191A}
// *********************************************************************//
  _UndoOpcTagEditorActionDisp = dispinterface
    ['{73358417-1DAD-3136-9085-5C4E9400191A}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    property Cost: Integer readonly dispid 1610743812;
    property Text: WideString readonly dispid 1610743813;
    procedure SaveState; dispid 1610743814;
    procedure Do_; dispid 1610743815;
    procedure Redo; dispid 1610743816;
    procedure Undo; dispid 1610743817;
  end;

// *********************************************************************//
// Interface: _UndoPropertySheetAction
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {851C990B-2069-3B2B-B5AF-8D090F0A616E}
// *********************************************************************//
  _UndoPropertySheetAction = interface(IDispatch)
    ['{851C990B-2069-3B2B-B5AF-8D090F0A616E}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Get_Cost: Integer; safecall;
    function Get_Text: WideString; safecall;
    procedure SaveState; safecall;
    procedure Do_; safecall;
    procedure Redo; safecall;
    procedure Undo; safecall;
    property ToString: WideString read Get_ToString;
    property Cost: Integer read Get_Cost;
    property Text: WideString read Get_Text;
  end;

// *********************************************************************//
// DispIntf:  _UndoPropertySheetActionDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {851C990B-2069-3B2B-B5AF-8D090F0A616E}
// *********************************************************************//
  _UndoPropertySheetActionDisp = dispinterface
    ['{851C990B-2069-3B2B-B5AF-8D090F0A616E}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    property Cost: Integer readonly dispid 1610743812;
    property Text: WideString readonly dispid 1610743813;
    procedure SaveState; dispid 1610743814;
    procedure Do_; dispid 1610743815;
    procedure Redo; dispid 1610743816;
    procedure Undo; dispid 1610743817;
  end;

// *********************************************************************//
// Interface: _UndoReferenceRedirectAction
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {8B599734-9247-3F81-A95B-B72E5893033A}
// *********************************************************************//
  _UndoReferenceRedirectAction = interface(IDispatch)
    ['{8B599734-9247-3F81-A95B-B72E5893033A}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Get_Cost: Integer; safecall;
    function Get_Text: WideString; safecall;
    procedure SaveState; safecall;
    procedure Do_; safecall;
    procedure Redo; safecall;
    procedure Undo; safecall;
    property ToString: WideString read Get_ToString;
    property Cost: Integer read Get_Cost;
    property Text: WideString read Get_Text;
  end;

// *********************************************************************//
// DispIntf:  _UndoReferenceRedirectActionDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {8B599734-9247-3F81-A95B-B72E5893033A}
// *********************************************************************//
  _UndoReferenceRedirectActionDisp = dispinterface
    ['{8B599734-9247-3F81-A95B-B72E5893033A}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    property Cost: Integer readonly dispid 1610743812;
    property Text: WideString readonly dispid 1610743813;
    procedure SaveState; dispid 1610743814;
    procedure Do_; dispid 1610743815;
    procedure Redo; dispid 1610743816;
    procedure Undo; dispid 1610743817;
  end;

// *********************************************************************//
// Interface: _UndoScriptChangeAction
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {E390666D-1791-371A-9D23-3017B3A15BD6}
// *********************************************************************//
  _UndoScriptChangeAction = interface(IDispatch)
    ['{E390666D-1791-371A-9D23-3017B3A15BD6}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Get_Cost: Integer; safecall;
    function Get_Text: WideString; safecall;
    procedure SaveState; safecall;
    procedure Do_; safecall;
    procedure Redo; safecall;
    procedure Undo; safecall;
    property ToString: WideString read Get_ToString;
    property Cost: Integer read Get_Cost;
    property Text: WideString read Get_Text;
  end;

// *********************************************************************//
// DispIntf:  _UndoScriptChangeActionDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {E390666D-1791-371A-9D23-3017B3A15BD6}
// *********************************************************************//
  _UndoScriptChangeActionDisp = dispinterface
    ['{E390666D-1791-371A-9D23-3017B3A15BD6}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    property Cost: Integer readonly dispid 1610743812;
    property Text: WideString readonly dispid 1610743813;
    procedure SaveState; dispid 1610743814;
    procedure Do_; dispid 1610743815;
    procedure Redo; dispid 1610743816;
    procedure Undo; dispid 1610743817;
  end;

// *********************************************************************//
// Interface: _CdsConnectionChange
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {30EFDA1B-F328-36F4-8674-42CD2CF6B4C5}
// *********************************************************************//
  _CdsConnectionChange = interface(IDispatch)
    ['{30EFDA1B-F328-36F4-8674-42CD2CF6B4C5}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function GetInvocationList: PSafeArray; safecall;
    function Clone: OleVariant; safecall;
    procedure GetObjectData(const info: _SerializationInfo; context: StreamingContext); safecall;
    function DynamicInvoke(args: PSafeArray): OleVariant; safecall;
    function Get_Method: _MethodInfo; safecall;
    function Get_Target: OleVariant; safecall;
    procedure Invoke_2(const serverInfo: IUnknown); safecall;
    function BeginInvoke(const serverInfo: IUnknown; const callback: _AsyncCallback; 
                         object_: OleVariant): IAsyncResult; safecall;
    procedure EndInvoke(const result: IAsyncResult); safecall;
    property ToString: WideString read Get_ToString;
    property Method: _MethodInfo read Get_Method;
    property Target: OleVariant read Get_Target;
  end;

// *********************************************************************//
// DispIntf:  _CdsConnectionChangeDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {30EFDA1B-F328-36F4-8674-42CD2CF6B4C5}
// *********************************************************************//
  _CdsConnectionChangeDisp = dispinterface
    ['{30EFDA1B-F328-36F4-8674-42CD2CF6B4C5}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    function GetInvocationList: {??PSafeArray}OleVariant; dispid 1610743812;
    function Clone: OleVariant; dispid 1610743813;
    procedure GetObjectData(const info: _SerializationInfo; context: {??StreamingContext}OleVariant); dispid 1610743814;
    function DynamicInvoke(args: {??PSafeArray}OleVariant): OleVariant; dispid 1610743815;
    property Method: _MethodInfo readonly dispid 1610743816;
    property Target: OleVariant readonly dispid 1610743817;
    procedure Invoke_2(const serverInfo: IUnknown); dispid 1610743818;
    function BeginInvoke(const serverInfo: IUnknown; const callback: _AsyncCallback; 
                         object_: OleVariant): IAsyncResult; dispid 1610743819;
    procedure EndInvoke(const result: IAsyncResult); dispid 1610743820;
  end;

// *********************************************************************//
// Interface: _CvsGraphic
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {0CCF4169-CBA6-3343-9564-71FCE1254EF9}
// *********************************************************************//
  _CvsGraphic = interface(IDispatch)
    ['{0CCF4169-CBA6-3343-9564-71FCE1254EF9}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Clone: _CvsGraphic; safecall;
    function Get_CellLocation: CvsCellLocation; safecall;
    function Get_HighlightColorAsRgb: Integer; safecall;
    function Get_Label_: WideString; safecall;
    procedure Set_Label_(const pRetVal: WideString); safecall;
    function Get_IsAlwaysShown: WordBool; safecall;
    function Get_IsDefaultEdit: WordBool; safecall;
    function Get_IsLocked: WordBool; safecall;
    function GetDrawOrder: Integer; safecall;
    procedure add_PropertyChanged(const value: IUnknown); safecall;
    procedure remove_PropertyChanged(const value: IUnknown); safecall;
    property ToString: WideString read Get_ToString;
    property CellLocation: CvsCellLocation read Get_CellLocation;
    property HighlightColorAsRgb: Integer read Get_HighlightColorAsRgb;
    property Label_: WideString read Get_Label_ write Set_Label_;
    property IsAlwaysShown: WordBool read Get_IsAlwaysShown;
    property IsDefaultEdit: WordBool read Get_IsDefaultEdit;
    property IsLocked: WordBool read Get_IsLocked;
  end;

// *********************************************************************//
// DispIntf:  _CvsGraphicDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {0CCF4169-CBA6-3343-9564-71FCE1254EF9}
// *********************************************************************//
  _CvsGraphicDisp = dispinterface
    ['{0CCF4169-CBA6-3343-9564-71FCE1254EF9}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    function Clone: _CvsGraphic; dispid 1610743812;
    property CellLocation: {??CvsCellLocation}OleVariant readonly dispid 1610743813;
    property HighlightColorAsRgb: Integer readonly dispid 1610743814;
    property Label_: WideString dispid 1610743815;
    property IsAlwaysShown: WordBool readonly dispid 1610743817;
    property IsDefaultEdit: WordBool readonly dispid 1610743818;
    property IsLocked: WordBool readonly dispid 1610743819;
    function GetDrawOrder: Integer; dispid 1610743820;
    procedure add_PropertyChanged(const value: IUnknown); dispid 1610743821;
    procedure remove_PropertyChanged(const value: IUnknown); dispid 1610743822;
  end;

// *********************************************************************//
// Interface: _CvsGraphicArrows
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {4393C594-D9B6-32C9-8EFB-089E1BD9AC99}
// *********************************************************************//
  _CvsGraphicArrows = interface(IDispatch)
    ['{4393C594-D9B6-32C9-8EFB-089E1BD9AC99}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Clone: _CvsGraphic; safecall;
    function Get_CellLocation: CvsCellLocation; safecall;
    function Get_HighlightColorAsRgb: Integer; safecall;
    function Get_Label_: WideString; safecall;
    procedure Set_Label_(const pRetVal: WideString); safecall;
    function Get_IsAlwaysShown: WordBool; safecall;
    function Get_IsDefaultEdit: WordBool; safecall;
    function Get_IsLocked: WordBool; safecall;
    function GetDrawOrder: Integer; safecall;
    procedure add_PropertyChanged(const value: IUnknown); safecall;
    procedure remove_PropertyChanged(const value: IUnknown); safecall;
    procedure Initialize(cellRow: Integer; cellCol: Integer; start: CvsImageLocation; angle: Single); safecall;
    property ToString: WideString read Get_ToString;
    property CellLocation: CvsCellLocation read Get_CellLocation;
    property HighlightColorAsRgb: Integer read Get_HighlightColorAsRgb;
    property Label_: WideString read Get_Label_ write Set_Label_;
    property IsAlwaysShown: WordBool read Get_IsAlwaysShown;
    property IsDefaultEdit: WordBool read Get_IsDefaultEdit;
    property IsLocked: WordBool read Get_IsLocked;
  end;

// *********************************************************************//
// DispIntf:  _CvsGraphicArrowsDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {4393C594-D9B6-32C9-8EFB-089E1BD9AC99}
// *********************************************************************//
  _CvsGraphicArrowsDisp = dispinterface
    ['{4393C594-D9B6-32C9-8EFB-089E1BD9AC99}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    function Clone: _CvsGraphic; dispid 1610743812;
    property CellLocation: {??CvsCellLocation}OleVariant readonly dispid 1610743813;
    property HighlightColorAsRgb: Integer readonly dispid 1610743814;
    property Label_: WideString dispid 1610743815;
    property IsAlwaysShown: WordBool readonly dispid 1610743817;
    property IsDefaultEdit: WordBool readonly dispid 1610743818;
    property IsLocked: WordBool readonly dispid 1610743819;
    function GetDrawOrder: Integer; dispid 1610743820;
    procedure add_PropertyChanged(const value: IUnknown); dispid 1610743821;
    procedure remove_PropertyChanged(const value: IUnknown); dispid 1610743822;
    procedure Initialize(cellRow: Integer; cellCol: Integer; start: {??CvsImageLocation}OleVariant; 
                         angle: Single); dispid 1610743823;
  end;

// *********************************************************************//
// Interface: _CvsCellEditMaskedRegion
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {58ADA604-FF86-3DFA-9D16-1ABFF6AA9AEE}
// *********************************************************************//
  _CvsCellEditMaskedRegion = interface(IDispatch)
    ['{58ADA604-FF86-3DFA-9D16-1ABFF6AA9AEE}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Get_Text: WideString; safecall;
    function Get_IsEditCell: WordBool; safecall;
    function Get_SupportsAuthorization: WordBool; safecall;
    function Get_DataType: CvsCellDataType; safecall;
    function Get_ErrorString: WideString; safecall;
    function Get_Error: WordBool; safecall;
    function Get_Control: WordBool; safecall;
    function Get_HasChecksum: WordBool; safecall;
    function Get_Hidden: WordBool; safecall;
    function Get_ProtectionState: CvsCellIPProtection; safecall;
    function Get_Enabled: WordBool; safecall;
    function Get_Location: CvsCellLocation; safecall;
    function Get_Expression: WideString; safecall;
    function Get_SymbolicTag: WideString; safecall;
    function Get_OpcTag: WideString; safecall;
    function Get_CellStateReference: WideString; safecall;
    function Get_Properties: CvsCellProperty; safecall;
    function Get_DataTypeString: WideString; safecall;
    function Get_EditFlags: CvsEditFlags; safecall;
    function Get_Subregions: PSafeArray; safecall;
    property ToString: WideString read Get_ToString;
    property Text: WideString read Get_Text;
    property IsEditCell: WordBool read Get_IsEditCell;
    property SupportsAuthorization: WordBool read Get_SupportsAuthorization;
    property DataType: CvsCellDataType read Get_DataType;
    property ErrorString: WideString read Get_ErrorString;
    property Error: WordBool read Get_Error;
    property Control: WordBool read Get_Control;
    property HasChecksum: WordBool read Get_HasChecksum;
    property Hidden: WordBool read Get_Hidden;
    property ProtectionState: CvsCellIPProtection read Get_ProtectionState;
    property Enabled: WordBool read Get_Enabled;
    property Location: CvsCellLocation read Get_Location;
    property Expression: WideString read Get_Expression;
    property SymbolicTag: WideString read Get_SymbolicTag;
    property OpcTag: WideString read Get_OpcTag;
    property CellStateReference: WideString read Get_CellStateReference;
    property Properties: CvsCellProperty read Get_Properties;
    property DataTypeString: WideString read Get_DataTypeString;
    property EditFlags: CvsEditFlags read Get_EditFlags;
    property Subregions: PSafeArray read Get_Subregions;
  end;

// *********************************************************************//
// DispIntf:  _CvsCellEditMaskedRegionDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {58ADA604-FF86-3DFA-9D16-1ABFF6AA9AEE}
// *********************************************************************//
  _CvsCellEditMaskedRegionDisp = dispinterface
    ['{58ADA604-FF86-3DFA-9D16-1ABFF6AA9AEE}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    property Text: WideString readonly dispid 1610743812;
    property IsEditCell: WordBool readonly dispid 1610743813;
    property SupportsAuthorization: WordBool readonly dispid 1610743814;
    property DataType: CvsCellDataType readonly dispid 1610743815;
    property ErrorString: WideString readonly dispid 1610743816;
    property Error: WordBool readonly dispid 1610743817;
    property Control: WordBool readonly dispid 1610743818;
    property HasChecksum: WordBool readonly dispid 1610743819;
    property Hidden: WordBool readonly dispid 1610743820;
    property ProtectionState: CvsCellIPProtection readonly dispid 1610743821;
    property Enabled: WordBool readonly dispid 1610743822;
    property Location: {??CvsCellLocation}OleVariant readonly dispid 1610743823;
    property Expression: WideString readonly dispid 1610743824;
    property SymbolicTag: WideString readonly dispid 1610743825;
    property OpcTag: WideString readonly dispid 1610743826;
    property CellStateReference: WideString readonly dispid 1610743827;
    property Properties: CvsCellProperty readonly dispid 1610743828;
    property DataTypeString: WideString readonly dispid 1610743829;
    property EditFlags: CvsEditFlags readonly dispid 1610743830;
    property Subregions: {??PSafeArray}OleVariant readonly dispid 1610743831;
  end;

// *********************************************************************//
// Interface: _CvsCellEditPolylinePath
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {5CEEAA4B-FBEC-3E06-878D-21A23AF79C93}
// *********************************************************************//
  _CvsCellEditPolylinePath = interface(IDispatch)
    ['{5CEEAA4B-FBEC-3E06-878D-21A23AF79C93}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Get_Text: WideString; safecall;
    function Get_IsEditCell: WordBool; safecall;
    function Get_SupportsAuthorization: WordBool; safecall;
    function Get_DataType: CvsCellDataType; safecall;
    function Get_ErrorString: WideString; safecall;
    function Get_Error: WordBool; safecall;
    function Get_Control: WordBool; safecall;
    function Get_HasChecksum: WordBool; safecall;
    function Get_Hidden: WordBool; safecall;
    function Get_ProtectionState: CvsCellIPProtection; safecall;
    function Get_Enabled: WordBool; safecall;
    function Get_Location: CvsCellLocation; safecall;
    function Get_Expression: WideString; safecall;
    function Get_SymbolicTag: WideString; safecall;
    function Get_OpcTag: WideString; safecall;
    function Get_CellStateReference: WideString; safecall;
    function Get_Properties: CvsCellProperty; safecall;
    function Get_DataTypeString: WideString; safecall;
    function Get_EditFlags: CvsEditFlags; safecall;
    function Get_Subregions: PSafeArray; safecall;
    property ToString: WideString read Get_ToString;
    property Text: WideString read Get_Text;
    property IsEditCell: WordBool read Get_IsEditCell;
    property SupportsAuthorization: WordBool read Get_SupportsAuthorization;
    property DataType: CvsCellDataType read Get_DataType;
    property ErrorString: WideString read Get_ErrorString;
    property Error: WordBool read Get_Error;
    property Control: WordBool read Get_Control;
    property HasChecksum: WordBool read Get_HasChecksum;
    property Hidden: WordBool read Get_Hidden;
    property ProtectionState: CvsCellIPProtection read Get_ProtectionState;
    property Enabled: WordBool read Get_Enabled;
    property Location: CvsCellLocation read Get_Location;
    property Expression: WideString read Get_Expression;
    property SymbolicTag: WideString read Get_SymbolicTag;
    property OpcTag: WideString read Get_OpcTag;
    property CellStateReference: WideString read Get_CellStateReference;
    property Properties: CvsCellProperty read Get_Properties;
    property DataTypeString: WideString read Get_DataTypeString;
    property EditFlags: CvsEditFlags read Get_EditFlags;
    property Subregions: PSafeArray read Get_Subregions;
  end;

// *********************************************************************//
// DispIntf:  _CvsCellEditPolylinePathDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {5CEEAA4B-FBEC-3E06-878D-21A23AF79C93}
// *********************************************************************//
  _CvsCellEditPolylinePathDisp = dispinterface
    ['{5CEEAA4B-FBEC-3E06-878D-21A23AF79C93}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    property Text: WideString readonly dispid 1610743812;
    property IsEditCell: WordBool readonly dispid 1610743813;
    property SupportsAuthorization: WordBool readonly dispid 1610743814;
    property DataType: CvsCellDataType readonly dispid 1610743815;
    property ErrorString: WideString readonly dispid 1610743816;
    property Error: WordBool readonly dispid 1610743817;
    property Control: WordBool readonly dispid 1610743818;
    property HasChecksum: WordBool readonly dispid 1610743819;
    property Hidden: WordBool readonly dispid 1610743820;
    property ProtectionState: CvsCellIPProtection readonly dispid 1610743821;
    property Enabled: WordBool readonly dispid 1610743822;
    property Location: {??CvsCellLocation}OleVariant readonly dispid 1610743823;
    property Expression: WideString readonly dispid 1610743824;
    property SymbolicTag: WideString readonly dispid 1610743825;
    property OpcTag: WideString readonly dispid 1610743826;
    property CellStateReference: WideString readonly dispid 1610743827;
    property Properties: CvsCellProperty readonly dispid 1610743828;
    property DataTypeString: WideString readonly dispid 1610743829;
    property EditFlags: CvsEditFlags readonly dispid 1610743830;
    property Subregions: {??PSafeArray}OleVariant readonly dispid 1610743831;
  end;

// *********************************************************************//
// Interface: _CvsAuditMessageSettings
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {8C2ACA9A-F0DC-37BC-BBA3-FCBB1DEBC631}
// *********************************************************************//
  _CvsAuditMessageSettings = interface(IDispatch)
    ['{8C2ACA9A-F0DC-37BC-BBA3-FCBB1DEBC631}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Clone: OleVariant; safecall;
    function Get_IPAddress: WideString; safecall;
    procedure Set_IPAddress(const pRetVal: WideString); safecall;
    function Get_Port: Integer; safecall;
    procedure Set_Port(pRetVal: Integer); safecall;
    function Get_Enabled: WordBool; safecall;
    procedure Set_Enabled(pRetVal: WordBool); safecall;
    function Get_LogEventsEnabled: WordBool; safecall;
    procedure Set_LogEventsEnabled(pRetVal: WordBool); safecall;
    function Get_LogTagsEnabled: WordBool; safecall;
    procedure Set_LogTagsEnabled(pRetVal: WordBool); safecall;
    function Get_LogSystemChangesEnabled: WordBool; safecall;
    procedure Set_LogSystemChangesEnabled(pRetVal: WordBool); safecall;
    property ToString: WideString read Get_ToString;
    property IPAddress: WideString read Get_IPAddress write Set_IPAddress;
    property Port: Integer read Get_Port write Set_Port;
    property Enabled: WordBool read Get_Enabled write Set_Enabled;
    property LogEventsEnabled: WordBool read Get_LogEventsEnabled write Set_LogEventsEnabled;
    property LogTagsEnabled: WordBool read Get_LogTagsEnabled write Set_LogTagsEnabled;
    property LogSystemChangesEnabled: WordBool read Get_LogSystemChangesEnabled write Set_LogSystemChangesEnabled;
  end;

// *********************************************************************//
// DispIntf:  _CvsAuditMessageSettingsDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {8C2ACA9A-F0DC-37BC-BBA3-FCBB1DEBC631}
// *********************************************************************//
  _CvsAuditMessageSettingsDisp = dispinterface
    ['{8C2ACA9A-F0DC-37BC-BBA3-FCBB1DEBC631}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    function Clone: OleVariant; dispid 1610743812;
    property IPAddress: WideString dispid 1610743813;
    property Port: Integer dispid 1610743815;
    property Enabled: WordBool dispid 1610743817;
    property LogEventsEnabled: WordBool dispid 1610743819;
    property LogTagsEnabled: WordBool dispid 1610743821;
    property LogSystemChangesEnabled: WordBool dispid 1610743823;
  end;

// *********************************************************************//
// Interface: ICaliperEditor
// Flags:     (4288) Dual NonExtensible Dispatchable
// GUID:      {D3E948D6-F022-42F7-9999-566ACF0CCF5B}
// *********************************************************************//
  ICaliperEditor = interface(IDispatch)
    ['{D3E948D6-F022-42F7-9999-566ACF0CCF5B}']
    procedure GhostMethod_ICaliperEditor_28_1; safecall;
    procedure GhostMethod_ICaliperEditor_32_2; safecall;
    procedure SetScoringMethod(const Method: _CvsCaliperScoringMethod); safecall;
    procedure ClearScoringMethods; safecall;
  end;

// *********************************************************************//
// DispIntf:  ICaliperEditorDisp
// Flags:     (4288) Dual NonExtensible Dispatchable
// GUID:      {D3E948D6-F022-42F7-9999-566ACF0CCF5B}
// *********************************************************************//
  ICaliperEditorDisp = dispinterface
    ['{D3E948D6-F022-42F7-9999-566ACF0CCF5B}']
    procedure GhostMethod_ICaliperEditor_28_1; dispid 1610743808;
    procedure GhostMethod_ICaliperEditor_32_2; dispid 1610743809;
    procedure SetScoringMethod(const Method: _CvsCaliperScoringMethod); dispid 1610743810;
    procedure ClearScoringMethods; dispid 1610743811;
  end;

// *********************************************************************//
// Interface: _CvsCaliperScoringMethod
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {A4FE0B1D-E603-349C-8BB7-76EDC46C1BC2}
// *********************************************************************//
  _CvsCaliperScoringMethod = interface(IDispatch)
    ['{A4FE0B1D-E603-349C-8BB7-76EDC46C1BC2}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Clone: OleVariant; safecall;
    function Get_Id: Integer; safecall;
    procedure Set_Id(pRetVal: Integer); safecall;
    function Get_RegionSize: Integer; safecall;
    function Get_EdgePairSize: Integer; safecall;
    function Get_EdgeMode: EdgeMode; safecall;
    function Get_type_: CaliperScoringMethodType; safecall;
    procedure Set_type_(pRetVal: CaliperScoringMethodType); safecall;
    procedure GhostMethod__CvsCaliperScoringMethod_76_1; safecall;
    procedure GhostMethod__CvsCaliperScoringMethod_80_2; safecall;
    function Get_UnPlottable: WordBool; safecall;
    procedure Set_UnPlottable(pRetVal: WordBool); safecall;
    function Get_MaxRegion: Single; safecall;
    function Get_MinRegion: Single; safecall;
    function Get_MidPoint: Single; safecall;
    function GetName: WideString; safecall;
    function GetDirection: ScoringMethodDirection; safecall;
    function SetDirection(direction: ScoringMethodDirection): WordBool; safecall;
    function SetValue(valType: ScoringMethodParameter; value: Single): WordBool; safecall;
    function GetEquationValue(valType: ScoringMethodParameter): Single; safecall;
    function IsWithinRanges: WordBool; safecall;
    function IsValidEdgeMode: WordBool; safecall;
    function Serialize(const caliperIdx: WideString): WideString; safecall;
    function Get_X0Low: Single; safecall;
    procedure Set_X0Low(pRetVal: Single); safecall;
    function Get_X1Low: Single; safecall;
    procedure Set_X1Low(pRetVal: Single); safecall;
    function Get_XCLow: Single; safecall;
    procedure Set_XCLow(pRetVal: Single); safecall;
    function Get_X0High: Single; safecall;
    procedure Set_X0High(pRetVal: Single); safecall;
    function Get_X1High: Single; safecall;
    procedure Set_X1High(pRetVal: Single); safecall;
    function Get_XCHigh: Single; safecall;
    procedure Set_XCHigh(pRetVal: Single); safecall;
    function Get_Y0Low: Single; safecall;
    procedure Set_Y0Low(pRetVal: Single); safecall;
    function Get_Y1Low: Single; safecall;
    procedure Set_Y1Low(pRetVal: Single); safecall;
    function Get_Y0High: Single; safecall;
    procedure Set_Y0High(pRetVal: Single); safecall;
    function Get_Y1High: Single; safecall;
    procedure Set_Y1High(pRetVal: Single); safecall;
    function Get_YMax: Single; safecall;
    procedure Set_YMax(pRetVal: Single); safecall;
    function Get_YMin: Single; safecall;
    procedure Set_YMin(pRetVal: Single); safecall;
    property ToString: WideString read Get_ToString;
    property Id: Integer read Get_Id write Set_Id;
    property RegionSize: Integer read Get_RegionSize;
    property EdgePairSize: Integer read Get_EdgePairSize;
    property EdgeMode: EdgeMode read Get_EdgeMode;
    property type_: CaliperScoringMethodType read Get_type_ write Set_type_;
    property UnPlottable: WordBool read Get_UnPlottable write Set_UnPlottable;
    property MaxRegion: Single read Get_MaxRegion;
    property MinRegion: Single read Get_MinRegion;
    property MidPoint: Single read Get_MidPoint;
    property X0Low: Single read Get_X0Low write Set_X0Low;
    property X1Low: Single read Get_X1Low write Set_X1Low;
    property XCLow: Single read Get_XCLow write Set_XCLow;
    property X0High: Single read Get_X0High write Set_X0High;
    property X1High: Single read Get_X1High write Set_X1High;
    property XCHigh: Single read Get_XCHigh write Set_XCHigh;
    property Y0Low: Single read Get_Y0Low write Set_Y0Low;
    property Y1Low: Single read Get_Y1Low write Set_Y1Low;
    property Y0High: Single read Get_Y0High write Set_Y0High;
    property Y1High: Single read Get_Y1High write Set_Y1High;
    property YMax: Single read Get_YMax write Set_YMax;
    property YMin: Single read Get_YMin write Set_YMin;
  end;

// *********************************************************************//
// DispIntf:  _CvsCaliperScoringMethodDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {A4FE0B1D-E603-349C-8BB7-76EDC46C1BC2}
// *********************************************************************//
  _CvsCaliperScoringMethodDisp = dispinterface
    ['{A4FE0B1D-E603-349C-8BB7-76EDC46C1BC2}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    function Clone: OleVariant; dispid 1610743812;
    property Id: Integer dispid 1610743813;
    property RegionSize: Integer readonly dispid 1610743815;
    property EdgePairSize: Integer readonly dispid 1610743816;
    property EdgeMode: EdgeMode readonly dispid 1610743817;
    property type_: CaliperScoringMethodType dispid 1610743818;
    procedure GhostMethod__CvsCaliperScoringMethod_76_1; dispid 1610743820;
    procedure GhostMethod__CvsCaliperScoringMethod_80_2; dispid 1610743821;
    property UnPlottable: WordBool dispid 1610743822;
    property MaxRegion: Single readonly dispid 1610743824;
    property MinRegion: Single readonly dispid 1610743825;
    property MidPoint: Single readonly dispid 1610743826;
    function GetName: WideString; dispid 1610743827;
    function GetDirection: ScoringMethodDirection; dispid 1610743828;
    function SetDirection(direction: ScoringMethodDirection): WordBool; dispid 1610743829;
    function SetValue(valType: ScoringMethodParameter; value: Single): WordBool; dispid 1610743830;
    function GetEquationValue(valType: ScoringMethodParameter): Single; dispid 1610743831;
    function IsWithinRanges: WordBool; dispid 1610743832;
    function IsValidEdgeMode: WordBool; dispid 1610743833;
    function Serialize(const caliperIdx: WideString): WideString; dispid 1610743834;
    property X0Low: Single dispid 1610743835;
    property X1Low: Single dispid 1610743837;
    property XCLow: Single dispid 1610743839;
    property X0High: Single dispid 1610743841;
    property X1High: Single dispid 1610743843;
    property XCHigh: Single dispid 1610743845;
    property Y0Low: Single dispid 1610743847;
    property Y1Low: Single dispid 1610743849;
    property Y0High: Single dispid 1610743851;
    property Y1High: Single dispid 1610743853;
    property YMax: Single dispid 1610743855;
    property YMin: Single dispid 1610743857;
  end;

// *********************************************************************//
// Interface: _CvsCellEditComposite
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {C9C050F1-E8A3-3A62-B5AC-BE4628ADBD44}
// *********************************************************************//
  _CvsCellEditComposite = interface(IDispatch)
    ['{C9C050F1-E8A3-3A62-B5AC-BE4628ADBD44}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Get_Text: WideString; safecall;
    function Get_IsEditCell: WordBool; safecall;
    function Get_SupportsAuthorization: WordBool; safecall;
    function Get_DataType: CvsCellDataType; safecall;
    function Get_ErrorString: WideString; safecall;
    function Get_Error: WordBool; safecall;
    function Get_Control: WordBool; safecall;
    function Get_HasChecksum: WordBool; safecall;
    function Get_Hidden: WordBool; safecall;
    function Get_ProtectionState: CvsCellIPProtection; safecall;
    function Get_Enabled: WordBool; safecall;
    function Get_Location: CvsCellLocation; safecall;
    function Get_Expression: WideString; safecall;
    function Get_SymbolicTag: WideString; safecall;
    function Get_OpcTag: WideString; safecall;
    function Get_CellStateReference: WideString; safecall;
    function Get_Properties: CvsCellProperty; safecall;
    function Get_DataTypeString: WideString; safecall;
    function Get_EditFlags: CvsEditFlags; safecall;
    function Get_Subregions: PSafeArray; safecall;
    property ToString: WideString read Get_ToString;
    property Text: WideString read Get_Text;
    property IsEditCell: WordBool read Get_IsEditCell;
    property SupportsAuthorization: WordBool read Get_SupportsAuthorization;
    property DataType: CvsCellDataType read Get_DataType;
    property ErrorString: WideString read Get_ErrorString;
    property Error: WordBool read Get_Error;
    property Control: WordBool read Get_Control;
    property HasChecksum: WordBool read Get_HasChecksum;
    property Hidden: WordBool read Get_Hidden;
    property ProtectionState: CvsCellIPProtection read Get_ProtectionState;
    property Enabled: WordBool read Get_Enabled;
    property Location: CvsCellLocation read Get_Location;
    property Expression: WideString read Get_Expression;
    property SymbolicTag: WideString read Get_SymbolicTag;
    property OpcTag: WideString read Get_OpcTag;
    property CellStateReference: WideString read Get_CellStateReference;
    property Properties: CvsCellProperty read Get_Properties;
    property DataTypeString: WideString read Get_DataTypeString;
    property EditFlags: CvsEditFlags read Get_EditFlags;
    property Subregions: PSafeArray read Get_Subregions;
  end;

// *********************************************************************//
// DispIntf:  _CvsCellEditCompositeDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {C9C050F1-E8A3-3A62-B5AC-BE4628ADBD44}
// *********************************************************************//
  _CvsCellEditCompositeDisp = dispinterface
    ['{C9C050F1-E8A3-3A62-B5AC-BE4628ADBD44}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    property Text: WideString readonly dispid 1610743812;
    property IsEditCell: WordBool readonly dispid 1610743813;
    property SupportsAuthorization: WordBool readonly dispid 1610743814;
    property DataType: CvsCellDataType readonly dispid 1610743815;
    property ErrorString: WideString readonly dispid 1610743816;
    property Error: WordBool readonly dispid 1610743817;
    property Control: WordBool readonly dispid 1610743818;
    property HasChecksum: WordBool readonly dispid 1610743819;
    property Hidden: WordBool readonly dispid 1610743820;
    property ProtectionState: CvsCellIPProtection readonly dispid 1610743821;
    property Enabled: WordBool readonly dispid 1610743822;
    property Location: {??CvsCellLocation}OleVariant readonly dispid 1610743823;
    property Expression: WideString readonly dispid 1610743824;
    property SymbolicTag: WideString readonly dispid 1610743825;
    property OpcTag: WideString readonly dispid 1610743826;
    property CellStateReference: WideString readonly dispid 1610743827;
    property Properties: CvsCellProperty readonly dispid 1610743828;
    property DataTypeString: WideString readonly dispid 1610743829;
    property EditFlags: CvsEditFlags readonly dispid 1610743830;
    property Subregions: {??PSafeArray}OleVariant readonly dispid 1610743831;
  end;

// *********************************************************************//
// Interface: _CvsCellEditPolygon
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {0E832240-E333-3F3D-9FC9-B050B4EA7CE9}
// *********************************************************************//
  _CvsCellEditPolygon = interface(IDispatch)
    ['{0E832240-E333-3F3D-9FC9-B050B4EA7CE9}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Get_Text: WideString; safecall;
    function Get_IsEditCell: WordBool; safecall;
    function Get_SupportsAuthorization: WordBool; safecall;
    function Get_DataType: CvsCellDataType; safecall;
    function Get_ErrorString: WideString; safecall;
    function Get_Error: WordBool; safecall;
    function Get_Control: WordBool; safecall;
    function Get_HasChecksum: WordBool; safecall;
    function Get_Hidden: WordBool; safecall;
    function Get_ProtectionState: CvsCellIPProtection; safecall;
    function Get_Enabled: WordBool; safecall;
    function Get_Location: CvsCellLocation; safecall;
    function Get_Expression: WideString; safecall;
    function Get_SymbolicTag: WideString; safecall;
    function Get_OpcTag: WideString; safecall;
    function Get_CellStateReference: WideString; safecall;
    function Get_Properties: CvsCellProperty; safecall;
    function Get_DataTypeString: WideString; safecall;
    function Get_EditFlags: CvsEditFlags; safecall;
    function Get_Points: PSafeArray; safecall;
    property ToString: WideString read Get_ToString;
    property Text: WideString read Get_Text;
    property IsEditCell: WordBool read Get_IsEditCell;
    property SupportsAuthorization: WordBool read Get_SupportsAuthorization;
    property DataType: CvsCellDataType read Get_DataType;
    property ErrorString: WideString read Get_ErrorString;
    property Error: WordBool read Get_Error;
    property Control: WordBool read Get_Control;
    property HasChecksum: WordBool read Get_HasChecksum;
    property Hidden: WordBool read Get_Hidden;
    property ProtectionState: CvsCellIPProtection read Get_ProtectionState;
    property Enabled: WordBool read Get_Enabled;
    property Location: CvsCellLocation read Get_Location;
    property Expression: WideString read Get_Expression;
    property SymbolicTag: WideString read Get_SymbolicTag;
    property OpcTag: WideString read Get_OpcTag;
    property CellStateReference: WideString read Get_CellStateReference;
    property Properties: CvsCellProperty read Get_Properties;
    property DataTypeString: WideString read Get_DataTypeString;
    property EditFlags: CvsEditFlags read Get_EditFlags;
    property Points: PSafeArray read Get_Points;
  end;

// *********************************************************************//
// DispIntf:  _CvsCellEditPolygonDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {0E832240-E333-3F3D-9FC9-B050B4EA7CE9}
// *********************************************************************//
  _CvsCellEditPolygonDisp = dispinterface
    ['{0E832240-E333-3F3D-9FC9-B050B4EA7CE9}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    property Text: WideString readonly dispid 1610743812;
    property IsEditCell: WordBool readonly dispid 1610743813;
    property SupportsAuthorization: WordBool readonly dispid 1610743814;
    property DataType: CvsCellDataType readonly dispid 1610743815;
    property ErrorString: WideString readonly dispid 1610743816;
    property Error: WordBool readonly dispid 1610743817;
    property Control: WordBool readonly dispid 1610743818;
    property HasChecksum: WordBool readonly dispid 1610743819;
    property Hidden: WordBool readonly dispid 1610743820;
    property ProtectionState: CvsCellIPProtection readonly dispid 1610743821;
    property Enabled: WordBool readonly dispid 1610743822;
    property Location: {??CvsCellLocation}OleVariant readonly dispid 1610743823;
    property Expression: WideString readonly dispid 1610743824;
    property SymbolicTag: WideString readonly dispid 1610743825;
    property OpcTag: WideString readonly dispid 1610743826;
    property CellStateReference: WideString readonly dispid 1610743827;
    property Properties: CvsCellProperty readonly dispid 1610743828;
    property DataTypeString: WideString readonly dispid 1610743829;
    property EditFlags: CvsEditFlags readonly dispid 1610743830;
    property Points: {??PSafeArray}OleVariant readonly dispid 1610743831;
  end;

// *********************************************************************//
// Interface: _CvsColorMatchEditor
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {CF595F98-ECD7-3F67-8A9E-474EFBFE7404}
// *********************************************************************//
  _CvsColorMatchEditor = interface(IDispatch)
    ['{CF595F98-ECD7-3F67-8A9E-474EFBFE7404}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    procedure InvalidateData; safecall;
    procedure Init(const inSight: _CvsInSight; CellLocation: CvsCellLocation; inDialog: WordBool); safecall;
    procedure Detach; safecall;
    function Get_inSight: _CvsInSight; safecall;
    procedure _Set_inSight(const pRetVal: _CvsInSight); safecall;
    function Get_CellLocation: CvsCellLocation; safecall;
    function Get_ColorModels: PSafeArray; safecall;
    property ToString: WideString read Get_ToString;
    property inSight: _CvsInSight read Get_inSight write _Set_inSight;
    property CellLocation: CvsCellLocation read Get_CellLocation;
    property ColorModels: PSafeArray read Get_ColorModels;
  end;

// *********************************************************************//
// DispIntf:  _CvsColorMatchEditorDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {CF595F98-ECD7-3F67-8A9E-474EFBFE7404}
// *********************************************************************//
  _CvsColorMatchEditorDisp = dispinterface
    ['{CF595F98-ECD7-3F67-8A9E-474EFBFE7404}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    procedure InvalidateData; dispid 1610743812;
    procedure Init(const inSight: _CvsInSight; CellLocation: {??CvsCellLocation}OleVariant; 
                   inDialog: WordBool); dispid 1610743813;
    procedure Detach; dispid 1610743814;
    property inSight: _CvsInSight dispid 1610743815;
    property CellLocation: {??CvsCellLocation}OleVariant readonly dispid 1610743817;
    property ColorModels: {??PSafeArray}OleVariant readonly dispid 1610743818;
  end;

// *********************************************************************//
// Interface: _CvsColorModel
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {30B1E578-8789-3E41-B9DF-D8F74D3313B6}
// *********************************************************************//
  _CvsColorModel = interface(IDispatch)
    ['{30B1E578-8789-3E41-B9DF-D8F74D3313B6}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Get_Name: WideString; safecall;
    function Get_Id: Integer; safecall;
    function Get_ModelColor: OLE_COLOR; safecall;
    function Get_Enabled: WordBool; safecall;
    procedure Set_Enabled(pRetVal: WordBool); safecall;
    property ToString: WideString read Get_ToString;
    property Name: WideString read Get_Name;
    property Id: Integer read Get_Id;
    property ModelColor: OLE_COLOR read Get_ModelColor;
    property Enabled: WordBool read Get_Enabled write Set_Enabled;
  end;

// *********************************************************************//
// DispIntf:  _CvsColorModelDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {30B1E578-8789-3E41-B9DF-D8F74D3313B6}
// *********************************************************************//
  _CvsColorModelDisp = dispinterface
    ['{30B1E578-8789-3E41-B9DF-D8F74D3313B6}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    property Name: WideString readonly dispid 1610743812;
    property Id: Integer readonly dispid 1610743813;
    property ModelColor: OLE_COLOR readonly dispid 1610743814;
    property Enabled: WordBool dispid 1610743815;
  end;

// *********************************************************************//
// Interface: _CvsEasyViewHelper
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {60F573FB-8BC9-38A8-83C3-5980D1C03B5A}
// *********************************************************************//
  _CvsEasyViewHelper = interface(IDispatch)
    ['{60F573FB-8BC9-38A8-83C3-5980D1C03B5A}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    property ToString: WideString read Get_ToString;
  end;

// *********************************************************************//
// DispIntf:  _CvsEasyViewHelperDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {60F573FB-8BC9-38A8-83C3-5980D1C03B5A}
// *********************************************************************//
  _CvsEasyViewHelperDisp = dispinterface
    ['{60F573FB-8BC9-38A8-83C3-5980D1C03B5A}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
  end;

// *********************************************************************//
// Interface: _CvsCompositeSubregion
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {7A49158D-4C5E-3426-B6CE-F1B2A87F46FA}
// *********************************************************************//
  _CvsCompositeSubregion = interface(IDispatch)
    ['{7A49158D-4C5E-3426-B6CE-F1B2A87F46FA}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Get_EditableGraphic: ICvsEditableGraphic; safecall;
    function Get_InSightGraphic: _CvsGraphic; safecall;
    function Get_ShapeType: CvsCompositeShapeType; safecall;
    function Get_Operation: CvsCompositeOperation; safecall;
    procedure Set_Operation(pRetVal: CvsCompositeOperation); safecall;
    property ToString: WideString read Get_ToString;
    property EditableGraphic: ICvsEditableGraphic read Get_EditableGraphic;
    property InSightGraphic: _CvsGraphic read Get_InSightGraphic;
    property ShapeType: CvsCompositeShapeType read Get_ShapeType;
    property Operation: CvsCompositeOperation read Get_Operation write Set_Operation;
  end;

// *********************************************************************//
// DispIntf:  _CvsCompositeSubregionDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {7A49158D-4C5E-3426-B6CE-F1B2A87F46FA}
// *********************************************************************//
  _CvsCompositeSubregionDisp = dispinterface
    ['{7A49158D-4C5E-3426-B6CE-F1B2A87F46FA}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    property EditableGraphic: ICvsEditableGraphic readonly dispid 1610743812;
    property InSightGraphic: _CvsGraphic readonly dispid 1610743813;
    property ShapeType: CvsCompositeShapeType readonly dispid 1610743814;
    property Operation: CvsCompositeOperation dispid 1610743815;
  end;

// *********************************************************************//
// Interface: ICvsEditableGraphic
// Flags:     (4288) Dual NonExtensible Dispatchable
// GUID:      {300FD59B-6D5B-4DF0-ACFA-148DFE4BDCA8}
// *********************************************************************//
  ICvsEditableGraphic = interface(IDispatch)
    ['{300FD59B-6D5B-4DF0-ACFA-148DFE4BDCA8}']
    function Get_EditFlags: CvsEditFlags; safecall;
    function Get_EditBounds: Rectangle; safecall;
    property EditFlags: CvsEditFlags read Get_EditFlags;
    property EditBounds: Rectangle read Get_EditBounds;
  end;

// *********************************************************************//
// DispIntf:  ICvsEditableGraphicDisp
// Flags:     (4288) Dual NonExtensible Dispatchable
// GUID:      {300FD59B-6D5B-4DF0-ACFA-148DFE4BDCA8}
// *********************************************************************//
  ICvsEditableGraphicDisp = dispinterface
    ['{300FD59B-6D5B-4DF0-ACFA-148DFE4BDCA8}']
    property EditFlags: CvsEditFlags readonly dispid 1610743808;
    property EditBounds: {??Rectangle}OleVariant readonly dispid 1610743809;
  end;

// *********************************************************************//
// Interface: _CvsGraphicCompositeBase
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {11B694E0-3C62-3F08-943D-40DC345A998E}
// *********************************************************************//
  _CvsGraphicCompositeBase = interface(IDispatch)
    ['{11B694E0-3C62-3F08-943D-40DC345A998E}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Clone: _CvsGraphic; safecall;
    function Get_CellLocation: CvsCellLocation; safecall;
    function Get_HighlightColorAsRgb: Integer; safecall;
    function Get_Label_: WideString; safecall;
    procedure Set_Label_(const pRetVal: WideString); safecall;
    function Get_IsAlwaysShown: WordBool; safecall;
    function Get_IsDefaultEdit: WordBool; safecall;
    function Get_IsLocked: WordBool; safecall;
    function GetDrawOrder: Integer; safecall;
    procedure add_PropertyChanged(const value: IUnknown); safecall;
    procedure remove_PropertyChanged(const value: IUnknown); safecall;
    procedure Initialize(cellRow: Integer; cellCol: Integer; Subregions: PSafeArray; 
                         const Label_: WideString); safecall;
    function Get_Subregions: PSafeArray; safecall;
    procedure Set_Subregions(pRetVal: PSafeArray); safecall;
    function InsertRegion(index: Integer; const region: _CvsGraphicRegion; 
                          Operation: CvsCompositeOperation): _CvsCompositeSubregion; safecall;
    function InsertCircle(index: Integer; const circle: _CvsGraphicCircle; 
                          Operation: CvsCompositeOperation): _CvsCompositeSubregion; safecall;
    function InsertAnnulus(index: Integer; const annulus: _CvsGraphicAnnulus; 
                           Operation: CvsCompositeOperation): _CvsCompositeSubregion; safecall;
    function InsertPolygon(index: Integer; const polygon: _CvsGraphicPolygon; 
                           Operation: CvsCompositeOperation): _CvsCompositeSubregion; safecall;
    procedure ClearSubregions; safecall;
    procedure DeleteSubregionByIndex(index: Integer); safecall;
    procedure DeleteSubregion(const subRegion: _CvsCompositeSubregion); safecall;
    procedure SetSubregionOperation(index: Integer; Operation: CvsCompositeOperation); safecall;
    function GetSubregionOperation(index: Integer): CvsCompositeOperation; safecall;
    procedure SetSubregionRegion(index: Integer; const region: _CvsGraphicRegion); safecall;
    procedure SetSubregionCircle(index: Integer; const circle: _CvsGraphicCircle); safecall;
    procedure SetSubregionAnnulus(index: Integer; const annulus: _CvsGraphicAnnulus); safecall;
    procedure SetSubregionPolygon(index: Integer; const polygon: _CvsGraphicPolygon); safecall;
    function Get_EditFlags: CvsEditFlags; safecall;
    procedure Set_EditFlags(pRetVal: CvsEditFlags); safecall;
    property ToString: WideString read Get_ToString;
    property CellLocation: CvsCellLocation read Get_CellLocation;
    property HighlightColorAsRgb: Integer read Get_HighlightColorAsRgb;
    property Label_: WideString read Get_Label_ write Set_Label_;
    property IsAlwaysShown: WordBool read Get_IsAlwaysShown;
    property IsDefaultEdit: WordBool read Get_IsDefaultEdit;
    property IsLocked: WordBool read Get_IsLocked;
    property Subregions: PSafeArray read Get_Subregions write Set_Subregions;
    property EditFlags: CvsEditFlags read Get_EditFlags write Set_EditFlags;
  end;

// *********************************************************************//
// DispIntf:  _CvsGraphicCompositeBaseDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {11B694E0-3C62-3F08-943D-40DC345A998E}
// *********************************************************************//
  _CvsGraphicCompositeBaseDisp = dispinterface
    ['{11B694E0-3C62-3F08-943D-40DC345A998E}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    function Clone: _CvsGraphic; dispid 1610743812;
    property CellLocation: {??CvsCellLocation}OleVariant readonly dispid 1610743813;
    property HighlightColorAsRgb: Integer readonly dispid 1610743814;
    property Label_: WideString dispid 1610743815;
    property IsAlwaysShown: WordBool readonly dispid 1610743817;
    property IsDefaultEdit: WordBool readonly dispid 1610743818;
    property IsLocked: WordBool readonly dispid 1610743819;
    function GetDrawOrder: Integer; dispid 1610743820;
    procedure add_PropertyChanged(const value: IUnknown); dispid 1610743821;
    procedure remove_PropertyChanged(const value: IUnknown); dispid 1610743822;
    procedure Initialize(cellRow: Integer; cellCol: Integer; Subregions: {??PSafeArray}OleVariant; 
                         const Label_: WideString); dispid 1610743823;
    property Subregions: {??PSafeArray}OleVariant dispid 1610743824;
    function InsertRegion(index: Integer; const region: _CvsGraphicRegion; 
                          Operation: CvsCompositeOperation): _CvsCompositeSubregion; dispid 1610743826;
    function InsertCircle(index: Integer; const circle: _CvsGraphicCircle; 
                          Operation: CvsCompositeOperation): _CvsCompositeSubregion; dispid 1610743827;
    function InsertAnnulus(index: Integer; const annulus: _CvsGraphicAnnulus; 
                           Operation: CvsCompositeOperation): _CvsCompositeSubregion; dispid 1610743828;
    function InsertPolygon(index: Integer; const polygon: _CvsGraphicPolygon; 
                           Operation: CvsCompositeOperation): _CvsCompositeSubregion; dispid 1610743829;
    procedure ClearSubregions; dispid 1610743830;
    procedure DeleteSubregionByIndex(index: Integer); dispid 1610743831;
    procedure DeleteSubregion(const subRegion: _CvsCompositeSubregion); dispid 1610743832;
    procedure SetSubregionOperation(index: Integer; Operation: CvsCompositeOperation); dispid 1610743833;
    function GetSubregionOperation(index: Integer): CvsCompositeOperation; dispid 1610743834;
    procedure SetSubregionRegion(index: Integer; const region: _CvsGraphicRegion); dispid 1610743835;
    procedure SetSubregionCircle(index: Integer; const circle: _CvsGraphicCircle); dispid 1610743836;
    procedure SetSubregionAnnulus(index: Integer; const annulus: _CvsGraphicAnnulus); dispid 1610743837;
    procedure SetSubregionPolygon(index: Integer; const polygon: _CvsGraphicPolygon); dispid 1610743838;
    property EditFlags: CvsEditFlags dispid 1610743839;
  end;

// *********************************************************************//
// Interface: _CvsGraphicComposite
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {BEA06CB0-516B-31BA-BE67-2D6BCD22B778}
// *********************************************************************//
  _CvsGraphicComposite = interface(IDispatch)
    ['{BEA06CB0-516B-31BA-BE67-2D6BCD22B778}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Clone: _CvsGraphic; safecall;
    function Get_CellLocation: CvsCellLocation; safecall;
    function Get_HighlightColorAsRgb: Integer; safecall;
    function Get_Label_: WideString; safecall;
    procedure Set_Label_(const pRetVal: WideString); safecall;
    function Get_IsAlwaysShown: WordBool; safecall;
    function Get_IsDefaultEdit: WordBool; safecall;
    function Get_IsLocked: WordBool; safecall;
    function GetDrawOrder: Integer; safecall;
    procedure add_PropertyChanged(const value: IUnknown); safecall;
    procedure remove_PropertyChanged(const value: IUnknown); safecall;
    procedure Initialize(cellRow: Integer; cellCol: Integer; Subregions: PSafeArray; 
                         const Label_: WideString); safecall;
    function Get_Subregions: PSafeArray; safecall;
    procedure Set_Subregions(pRetVal: PSafeArray); safecall;
    function InsertRegion(index: Integer; const region: _CvsGraphicRegion; 
                          Operation: CvsCompositeOperation): _CvsCompositeSubregion; safecall;
    function InsertCircle(index: Integer; const circle: _CvsGraphicCircle; 
                          Operation: CvsCompositeOperation): _CvsCompositeSubregion; safecall;
    function InsertAnnulus(index: Integer; const annulus: _CvsGraphicAnnulus; 
                           Operation: CvsCompositeOperation): _CvsCompositeSubregion; safecall;
    function InsertPolygon(index: Integer; const polygon: _CvsGraphicPolygon; 
                           Operation: CvsCompositeOperation): _CvsCompositeSubregion; safecall;
    procedure ClearSubregions; safecall;
    procedure DeleteSubregionByIndex(index: Integer); safecall;
    procedure DeleteSubregion(const subRegion: _CvsCompositeSubregion); safecall;
    procedure SetSubregionOperation(index: Integer; Operation: CvsCompositeOperation); safecall;
    function GetSubregionOperation(index: Integer): CvsCompositeOperation; safecall;
    procedure SetSubregionRegion(index: Integer; const region: _CvsGraphicRegion); safecall;
    procedure SetSubregionCircle(index: Integer; const circle: _CvsGraphicCircle); safecall;
    procedure SetSubregionAnnulus(index: Integer; const annulus: _CvsGraphicAnnulus); safecall;
    procedure SetSubregionPolygon(index: Integer; const polygon: _CvsGraphicPolygon); safecall;
    function Get_EditFlags: CvsEditFlags; safecall;
    procedure Set_EditFlags(pRetVal: CvsEditFlags); safecall;
    property ToString: WideString read Get_ToString;
    property CellLocation: CvsCellLocation read Get_CellLocation;
    property HighlightColorAsRgb: Integer read Get_HighlightColorAsRgb;
    property Label_: WideString read Get_Label_ write Set_Label_;
    property IsAlwaysShown: WordBool read Get_IsAlwaysShown;
    property IsDefaultEdit: WordBool read Get_IsDefaultEdit;
    property IsLocked: WordBool read Get_IsLocked;
    property Subregions: PSafeArray read Get_Subregions write Set_Subregions;
    property EditFlags: CvsEditFlags read Get_EditFlags write Set_EditFlags;
  end;

// *********************************************************************//
// DispIntf:  _CvsGraphicCompositeDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {BEA06CB0-516B-31BA-BE67-2D6BCD22B778}
// *********************************************************************//
  _CvsGraphicCompositeDisp = dispinterface
    ['{BEA06CB0-516B-31BA-BE67-2D6BCD22B778}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    function Clone: _CvsGraphic; dispid 1610743812;
    property CellLocation: {??CvsCellLocation}OleVariant readonly dispid 1610743813;
    property HighlightColorAsRgb: Integer readonly dispid 1610743814;
    property Label_: WideString dispid 1610743815;
    property IsAlwaysShown: WordBool readonly dispid 1610743817;
    property IsDefaultEdit: WordBool readonly dispid 1610743818;
    property IsLocked: WordBool readonly dispid 1610743819;
    function GetDrawOrder: Integer; dispid 1610743820;
    procedure add_PropertyChanged(const value: IUnknown); dispid 1610743821;
    procedure remove_PropertyChanged(const value: IUnknown); dispid 1610743822;
    procedure Initialize(cellRow: Integer; cellCol: Integer; Subregions: {??PSafeArray}OleVariant; 
                         const Label_: WideString); dispid 1610743823;
    property Subregions: {??PSafeArray}OleVariant dispid 1610743824;
    function InsertRegion(index: Integer; const region: _CvsGraphicRegion; 
                          Operation: CvsCompositeOperation): _CvsCompositeSubregion; dispid 1610743826;
    function InsertCircle(index: Integer; const circle: _CvsGraphicCircle; 
                          Operation: CvsCompositeOperation): _CvsCompositeSubregion; dispid 1610743827;
    function InsertAnnulus(index: Integer; const annulus: _CvsGraphicAnnulus; 
                           Operation: CvsCompositeOperation): _CvsCompositeSubregion; dispid 1610743828;
    function InsertPolygon(index: Integer; const polygon: _CvsGraphicPolygon; 
                           Operation: CvsCompositeOperation): _CvsCompositeSubregion; dispid 1610743829;
    procedure ClearSubregions; dispid 1610743830;
    procedure DeleteSubregionByIndex(index: Integer); dispid 1610743831;
    procedure DeleteSubregion(const subRegion: _CvsCompositeSubregion); dispid 1610743832;
    procedure SetSubregionOperation(index: Integer; Operation: CvsCompositeOperation); dispid 1610743833;
    function GetSubregionOperation(index: Integer): CvsCompositeOperation; dispid 1610743834;
    procedure SetSubregionRegion(index: Integer; const region: _CvsGraphicRegion); dispid 1610743835;
    procedure SetSubregionCircle(index: Integer; const circle: _CvsGraphicCircle); dispid 1610743836;
    procedure SetSubregionAnnulus(index: Integer; const annulus: _CvsGraphicAnnulus); dispid 1610743837;
    procedure SetSubregionPolygon(index: Integer; const polygon: _CvsGraphicPolygon); dispid 1610743838;
    property EditFlags: CvsEditFlags dispid 1610743839;
  end;

// *********************************************************************//
// Interface: _CvsGraphicPolylinePath
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {F1FB4FFA-F36D-3018-94DF-80C519B9DA18}
// *********************************************************************//
  _CvsGraphicPolylinePath = interface(IDispatch)
    ['{F1FB4FFA-F36D-3018-94DF-80C519B9DA18}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Clone: _CvsGraphic; safecall;
    function Get_CellLocation: CvsCellLocation; safecall;
    function Get_HighlightColorAsRgb: Integer; safecall;
    function Get_Label_: WideString; safecall;
    procedure Set_Label_(const pRetVal: WideString); safecall;
    function Get_IsAlwaysShown: WordBool; safecall;
    function Get_IsDefaultEdit: WordBool; safecall;
    function Get_IsLocked: WordBool; safecall;
    function GetDrawOrder: Integer; safecall;
    procedure add_PropertyChanged(const value: IUnknown); safecall;
    procedure remove_PropertyChanged(const value: IUnknown); safecall;
    procedure Initialize(cellRow: Integer; cellCol: Integer; Subregions: PSafeArray; 
                         const Label_: WideString); safecall;
    function Get_Subregions: PSafeArray; safecall;
    procedure Set_Subregions(pRetVal: PSafeArray); safecall;
    function InsertRegion(index: Integer; const region: _CvsGraphicRegion; 
                          Operation: CvsCompositeOperation): _CvsCompositeSubregion; safecall;
    function InsertCircle(index: Integer; const circle: _CvsGraphicCircle; 
                          Operation: CvsCompositeOperation): _CvsCompositeSubregion; safecall;
    function InsertAnnulus(index: Integer; const annulus: _CvsGraphicAnnulus; 
                           Operation: CvsCompositeOperation): _CvsCompositeSubregion; safecall;
    function InsertPolygon(index: Integer; const polygon: _CvsGraphicPolygon; 
                           Operation: CvsCompositeOperation): _CvsCompositeSubregion; safecall;
    procedure ClearSubregions; safecall;
    procedure DeleteSubregionByIndex(index: Integer); safecall;
    procedure DeleteSubregion(const subRegion: _CvsCompositeSubregion); safecall;
    procedure SetSubregionOperation(index: Integer; Operation: CvsCompositeOperation); safecall;
    function GetSubregionOperation(index: Integer): CvsCompositeOperation; safecall;
    procedure SetSubregionRegion(index: Integer; const region: _CvsGraphicRegion); safecall;
    procedure SetSubregionCircle(index: Integer; const circle: _CvsGraphicCircle); safecall;
    procedure SetSubregionAnnulus(index: Integer; const annulus: _CvsGraphicAnnulus); safecall;
    procedure SetSubregionPolygon(index: Integer; const polygon: _CvsGraphicPolygon); safecall;
    function Get_EditFlags: CvsEditFlags; safecall;
    procedure Set_EditFlags(pRetVal: CvsEditFlags); safecall;
    property ToString: WideString read Get_ToString;
    property CellLocation: CvsCellLocation read Get_CellLocation;
    property HighlightColorAsRgb: Integer read Get_HighlightColorAsRgb;
    property Label_: WideString read Get_Label_ write Set_Label_;
    property IsAlwaysShown: WordBool read Get_IsAlwaysShown;
    property IsDefaultEdit: WordBool read Get_IsDefaultEdit;
    property IsLocked: WordBool read Get_IsLocked;
    property Subregions: PSafeArray read Get_Subregions write Set_Subregions;
    property EditFlags: CvsEditFlags read Get_EditFlags write Set_EditFlags;
  end;

// *********************************************************************//
// DispIntf:  _CvsGraphicPolylinePathDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {F1FB4FFA-F36D-3018-94DF-80C519B9DA18}
// *********************************************************************//
  _CvsGraphicPolylinePathDisp = dispinterface
    ['{F1FB4FFA-F36D-3018-94DF-80C519B9DA18}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    function Clone: _CvsGraphic; dispid 1610743812;
    property CellLocation: {??CvsCellLocation}OleVariant readonly dispid 1610743813;
    property HighlightColorAsRgb: Integer readonly dispid 1610743814;
    property Label_: WideString dispid 1610743815;
    property IsAlwaysShown: WordBool readonly dispid 1610743817;
    property IsDefaultEdit: WordBool readonly dispid 1610743818;
    property IsLocked: WordBool readonly dispid 1610743819;
    function GetDrawOrder: Integer; dispid 1610743820;
    procedure add_PropertyChanged(const value: IUnknown); dispid 1610743821;
    procedure remove_PropertyChanged(const value: IUnknown); dispid 1610743822;
    procedure Initialize(cellRow: Integer; cellCol: Integer; Subregions: {??PSafeArray}OleVariant; 
                         const Label_: WideString); dispid 1610743823;
    property Subregions: {??PSafeArray}OleVariant dispid 1610743824;
    function InsertRegion(index: Integer; const region: _CvsGraphicRegion; 
                          Operation: CvsCompositeOperation): _CvsCompositeSubregion; dispid 1610743826;
    function InsertCircle(index: Integer; const circle: _CvsGraphicCircle; 
                          Operation: CvsCompositeOperation): _CvsCompositeSubregion; dispid 1610743827;
    function InsertAnnulus(index: Integer; const annulus: _CvsGraphicAnnulus; 
                           Operation: CvsCompositeOperation): _CvsCompositeSubregion; dispid 1610743828;
    function InsertPolygon(index: Integer; const polygon: _CvsGraphicPolygon; 
                           Operation: CvsCompositeOperation): _CvsCompositeSubregion; dispid 1610743829;
    procedure ClearSubregions; dispid 1610743830;
    procedure DeleteSubregionByIndex(index: Integer); dispid 1610743831;
    procedure DeleteSubregion(const subRegion: _CvsCompositeSubregion); dispid 1610743832;
    procedure SetSubregionOperation(index: Integer; Operation: CvsCompositeOperation); dispid 1610743833;
    function GetSubregionOperation(index: Integer): CvsCompositeOperation; dispid 1610743834;
    procedure SetSubregionRegion(index: Integer; const region: _CvsGraphicRegion); dispid 1610743835;
    procedure SetSubregionCircle(index: Integer; const circle: _CvsGraphicCircle); dispid 1610743836;
    procedure SetSubregionAnnulus(index: Integer; const annulus: _CvsGraphicAnnulus); dispid 1610743837;
    procedure SetSubregionPolygon(index: Integer; const polygon: _CvsGraphicPolygon); dispid 1610743838;
    property EditFlags: CvsEditFlags dispid 1610743839;
  end;

// *********************************************************************//
// Interface: _CvsGraphicMaskedRegion
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {8E08C0A5-7B2A-3CE5-9B5A-5A9B4547618D}
// *********************************************************************//
  _CvsGraphicMaskedRegion = interface(IDispatch)
    ['{8E08C0A5-7B2A-3CE5-9B5A-5A9B4547618D}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Clone: _CvsGraphic; safecall;
    function Get_CellLocation: CvsCellLocation; safecall;
    function Get_HighlightColorAsRgb: Integer; safecall;
    function Get_Label_: WideString; safecall;
    procedure Set_Label_(const pRetVal: WideString); safecall;
    function Get_IsAlwaysShown: WordBool; safecall;
    function Get_IsDefaultEdit: WordBool; safecall;
    function Get_IsLocked: WordBool; safecall;
    function GetDrawOrder: Integer; safecall;
    procedure add_PropertyChanged(const value: IUnknown); safecall;
    procedure remove_PropertyChanged(const value: IUnknown); safecall;
    procedure Initialize(cellRow: Integer; cellCol: Integer; Subregions: PSafeArray; 
                         const Label_: WideString); safecall;
    function Get_Subregions: PSafeArray; safecall;
    procedure Set_Subregions(pRetVal: PSafeArray); safecall;
    function InsertRegion(index: Integer; const region: _CvsGraphicRegion; 
                          Operation: CvsCompositeOperation): _CvsCompositeSubregion; safecall;
    function InsertCircle(index: Integer; const circle: _CvsGraphicCircle; 
                          Operation: CvsCompositeOperation): _CvsCompositeSubregion; safecall;
    function InsertAnnulus(index: Integer; const annulus: _CvsGraphicAnnulus; 
                           Operation: CvsCompositeOperation): _CvsCompositeSubregion; safecall;
    function InsertPolygon(index: Integer; const polygon: _CvsGraphicPolygon; 
                           Operation: CvsCompositeOperation): _CvsCompositeSubregion; safecall;
    procedure ClearSubregions; safecall;
    procedure DeleteSubregionByIndex(index: Integer); safecall;
    procedure DeleteSubregion(const subRegion: _CvsCompositeSubregion); safecall;
    procedure SetSubregionOperation(index: Integer; Operation: CvsCompositeOperation); safecall;
    function GetSubregionOperation(index: Integer): CvsCompositeOperation; safecall;
    procedure SetSubregionRegion(index: Integer; const region: _CvsGraphicRegion); safecall;
    procedure SetSubregionCircle(index: Integer; const circle: _CvsGraphicCircle); safecall;
    procedure SetSubregionAnnulus(index: Integer; const annulus: _CvsGraphicAnnulus); safecall;
    procedure SetSubregionPolygon(index: Integer; const polygon: _CvsGraphicPolygon); safecall;
    function Get_EditFlags: CvsEditFlags; safecall;
    procedure Set_EditFlags(pRetVal: CvsEditFlags); safecall;
    property ToString: WideString read Get_ToString;
    property CellLocation: CvsCellLocation read Get_CellLocation;
    property HighlightColorAsRgb: Integer read Get_HighlightColorAsRgb;
    property Label_: WideString read Get_Label_ write Set_Label_;
    property IsAlwaysShown: WordBool read Get_IsAlwaysShown;
    property IsDefaultEdit: WordBool read Get_IsDefaultEdit;
    property IsLocked: WordBool read Get_IsLocked;
    property Subregions: PSafeArray read Get_Subregions write Set_Subregions;
    property EditFlags: CvsEditFlags read Get_EditFlags write Set_EditFlags;
  end;

// *********************************************************************//
// DispIntf:  _CvsGraphicMaskedRegionDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {8E08C0A5-7B2A-3CE5-9B5A-5A9B4547618D}
// *********************************************************************//
  _CvsGraphicMaskedRegionDisp = dispinterface
    ['{8E08C0A5-7B2A-3CE5-9B5A-5A9B4547618D}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    function Clone: _CvsGraphic; dispid 1610743812;
    property CellLocation: {??CvsCellLocation}OleVariant readonly dispid 1610743813;
    property HighlightColorAsRgb: Integer readonly dispid 1610743814;
    property Label_: WideString dispid 1610743815;
    property IsAlwaysShown: WordBool readonly dispid 1610743817;
    property IsDefaultEdit: WordBool readonly dispid 1610743818;
    property IsLocked: WordBool readonly dispid 1610743819;
    function GetDrawOrder: Integer; dispid 1610743820;
    procedure add_PropertyChanged(const value: IUnknown); dispid 1610743821;
    procedure remove_PropertyChanged(const value: IUnknown); dispid 1610743822;
    procedure Initialize(cellRow: Integer; cellCol: Integer; Subregions: {??PSafeArray}OleVariant; 
                         const Label_: WideString); dispid 1610743823;
    property Subregions: {??PSafeArray}OleVariant dispid 1610743824;
    function InsertRegion(index: Integer; const region: _CvsGraphicRegion; 
                          Operation: CvsCompositeOperation): _CvsCompositeSubregion; dispid 1610743826;
    function InsertCircle(index: Integer; const circle: _CvsGraphicCircle; 
                          Operation: CvsCompositeOperation): _CvsCompositeSubregion; dispid 1610743827;
    function InsertAnnulus(index: Integer; const annulus: _CvsGraphicAnnulus; 
                           Operation: CvsCompositeOperation): _CvsCompositeSubregion; dispid 1610743828;
    function InsertPolygon(index: Integer; const polygon: _CvsGraphicPolygon; 
                           Operation: CvsCompositeOperation): _CvsCompositeSubregion; dispid 1610743829;
    procedure ClearSubregions; dispid 1610743830;
    procedure DeleteSubregionByIndex(index: Integer); dispid 1610743831;
    procedure DeleteSubregion(const subRegion: _CvsCompositeSubregion); dispid 1610743832;
    procedure SetSubregionOperation(index: Integer; Operation: CvsCompositeOperation); dispid 1610743833;
    function GetSubregionOperation(index: Integer): CvsCompositeOperation; dispid 1610743834;
    procedure SetSubregionRegion(index: Integer; const region: _CvsGraphicRegion); dispid 1610743835;
    procedure SetSubregionCircle(index: Integer; const circle: _CvsGraphicCircle); dispid 1610743836;
    procedure SetSubregionAnnulus(index: Integer; const annulus: _CvsGraphicAnnulus); dispid 1610743837;
    procedure SetSubregionPolygon(index: Integer; const polygon: _CvsGraphicPolygon); dispid 1610743838;
    property EditFlags: CvsEditFlags dispid 1610743839;
  end;

// *********************************************************************//
// Interface: _CvsGraphicDefectPoints
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {B13E5E00-6116-3A1A-9E37-446E4C2F9FCD}
// *********************************************************************//
  _CvsGraphicDefectPoints = interface(IDispatch)
    ['{B13E5E00-6116-3A1A-9E37-446E4C2F9FCD}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Clone: _CvsGraphic; safecall;
    function Get_CellLocation: CvsCellLocation; safecall;
    function Get_HighlightColorAsRgb: Integer; safecall;
    function Get_Label_: WideString; safecall;
    procedure Set_Label_(const pRetVal: WideString); safecall;
    function Get_IsAlwaysShown: WordBool; safecall;
    function Get_IsDefaultEdit: WordBool; safecall;
    function Get_IsLocked: WordBool; safecall;
    function GetDrawOrder: Integer; safecall;
    procedure add_PropertyChanged(const value: IUnknown); safecall;
    procedure remove_PropertyChanged(const value: IUnknown); safecall;
    function Get_ZOrder: Byte; safecall;
    procedure Initialize(cellRow: Integer; cellCol: Integer; Points: PSafeArray); safecall;
    function Get_NumPoints: Integer; safecall;
    function GetPoints: PSafeArray; safecall;
    property ToString: WideString read Get_ToString;
    property CellLocation: CvsCellLocation read Get_CellLocation;
    property HighlightColorAsRgb: Integer read Get_HighlightColorAsRgb;
    property Label_: WideString read Get_Label_ write Set_Label_;
    property IsAlwaysShown: WordBool read Get_IsAlwaysShown;
    property IsDefaultEdit: WordBool read Get_IsDefaultEdit;
    property IsLocked: WordBool read Get_IsLocked;
    property ZOrder: Byte read Get_ZOrder;
    property NumPoints: Integer read Get_NumPoints;
  end;

// *********************************************************************//
// DispIntf:  _CvsGraphicDefectPointsDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {B13E5E00-6116-3A1A-9E37-446E4C2F9FCD}
// *********************************************************************//
  _CvsGraphicDefectPointsDisp = dispinterface
    ['{B13E5E00-6116-3A1A-9E37-446E4C2F9FCD}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    function Clone: _CvsGraphic; dispid 1610743812;
    property CellLocation: {??CvsCellLocation}OleVariant readonly dispid 1610743813;
    property HighlightColorAsRgb: Integer readonly dispid 1610743814;
    property Label_: WideString dispid 1610743815;
    property IsAlwaysShown: WordBool readonly dispid 1610743817;
    property IsDefaultEdit: WordBool readonly dispid 1610743818;
    property IsLocked: WordBool readonly dispid 1610743819;
    function GetDrawOrder: Integer; dispid 1610743820;
    procedure add_PropertyChanged(const value: IUnknown); dispid 1610743821;
    procedure remove_PropertyChanged(const value: IUnknown); dispid 1610743822;
    property ZOrder: Byte readonly dispid 1610743823;
    procedure Initialize(cellRow: Integer; cellCol: Integer; Points: {??PSafeArray}OleVariant); dispid 1610743824;
    property NumPoints: Integer readonly dispid 1610743825;
    function GetPoints: {??PSafeArray}OleVariant; dispid 1610743826;
  end;

// *********************************************************************//
// Interface: _CvsGraphicEdgePoints
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {18C86A45-77C4-32A8-816B-929C8B750A01}
// *********************************************************************//
  _CvsGraphicEdgePoints = interface(IDispatch)
    ['{18C86A45-77C4-32A8-816B-929C8B750A01}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Clone: _CvsGraphic; safecall;
    function Get_CellLocation: CvsCellLocation; safecall;
    function Get_HighlightColorAsRgb: Integer; safecall;
    function Get_Label_: WideString; safecall;
    procedure Set_Label_(const pRetVal: WideString); safecall;
    function Get_IsAlwaysShown: WordBool; safecall;
    function Get_IsDefaultEdit: WordBool; safecall;
    function Get_IsLocked: WordBool; safecall;
    function GetDrawOrder: Integer; safecall;
    procedure add_PropertyChanged(const value: IUnknown); safecall;
    procedure remove_PropertyChanged(const value: IUnknown); safecall;
    procedure Initialize(cellRow: Integer; cellCol: Integer; Points: PSafeArray); safecall;
    function Get_NumPoints: Integer; safecall;
    function GetPoints: PSafeArray; safecall;
    property ToString: WideString read Get_ToString;
    property CellLocation: CvsCellLocation read Get_CellLocation;
    property HighlightColorAsRgb: Integer read Get_HighlightColorAsRgb;
    property Label_: WideString read Get_Label_ write Set_Label_;
    property IsAlwaysShown: WordBool read Get_IsAlwaysShown;
    property IsDefaultEdit: WordBool read Get_IsDefaultEdit;
    property IsLocked: WordBool read Get_IsLocked;
    property NumPoints: Integer read Get_NumPoints;
  end;

// *********************************************************************//
// DispIntf:  _CvsGraphicEdgePointsDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {18C86A45-77C4-32A8-816B-929C8B750A01}
// *********************************************************************//
  _CvsGraphicEdgePointsDisp = dispinterface
    ['{18C86A45-77C4-32A8-816B-929C8B750A01}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    function Clone: _CvsGraphic; dispid 1610743812;
    property CellLocation: {??CvsCellLocation}OleVariant readonly dispid 1610743813;
    property HighlightColorAsRgb: Integer readonly dispid 1610743814;
    property Label_: WideString dispid 1610743815;
    property IsAlwaysShown: WordBool readonly dispid 1610743817;
    property IsDefaultEdit: WordBool readonly dispid 1610743818;
    property IsLocked: WordBool readonly dispid 1610743819;
    function GetDrawOrder: Integer; dispid 1610743820;
    procedure add_PropertyChanged(const value: IUnknown); dispid 1610743821;
    procedure remove_PropertyChanged(const value: IUnknown); dispid 1610743822;
    procedure Initialize(cellRow: Integer; cellCol: Integer; Points: {??PSafeArray}OleVariant); dispid 1610743823;
    property NumPoints: Integer readonly dispid 1610743824;
    function GetPoints: {??PSafeArray}OleVariant; dispid 1610743825;
  end;

// *********************************************************************//
// Interface: _CvsGraphicExtremePoints
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {4DA5E115-886F-3531-BDD5-B1AC4EF11AAF}
// *********************************************************************//
  _CvsGraphicExtremePoints = interface(IDispatch)
    ['{4DA5E115-886F-3531-BDD5-B1AC4EF11AAF}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Clone: _CvsGraphic; safecall;
    function Get_CellLocation: CvsCellLocation; safecall;
    function Get_HighlightColorAsRgb: Integer; safecall;
    function Get_Label_: WideString; safecall;
    procedure Set_Label_(const pRetVal: WideString); safecall;
    function Get_IsAlwaysShown: WordBool; safecall;
    function Get_IsDefaultEdit: WordBool; safecall;
    function Get_IsLocked: WordBool; safecall;
    function GetDrawOrder: Integer; safecall;
    procedure add_PropertyChanged(const value: IUnknown); safecall;
    procedure remove_PropertyChanged(const value: IUnknown); safecall;
    procedure Initialize(cellRow: Integer; cellCol: Integer; Points: PSafeArray); safecall;
    function Get_NumPoints: Integer; safecall;
    function GetPoints: PSafeArray; safecall;
    property ToString: WideString read Get_ToString;
    property CellLocation: CvsCellLocation read Get_CellLocation;
    property HighlightColorAsRgb: Integer read Get_HighlightColorAsRgb;
    property Label_: WideString read Get_Label_ write Set_Label_;
    property IsAlwaysShown: WordBool read Get_IsAlwaysShown;
    property IsDefaultEdit: WordBool read Get_IsDefaultEdit;
    property IsLocked: WordBool read Get_IsLocked;
    property NumPoints: Integer read Get_NumPoints;
  end;

// *********************************************************************//
// DispIntf:  _CvsGraphicExtremePointsDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {4DA5E115-886F-3531-BDD5-B1AC4EF11AAF}
// *********************************************************************//
  _CvsGraphicExtremePointsDisp = dispinterface
    ['{4DA5E115-886F-3531-BDD5-B1AC4EF11AAF}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    function Clone: _CvsGraphic; dispid 1610743812;
    property CellLocation: {??CvsCellLocation}OleVariant readonly dispid 1610743813;
    property HighlightColorAsRgb: Integer readonly dispid 1610743814;
    property Label_: WideString dispid 1610743815;
    property IsAlwaysShown: WordBool readonly dispid 1610743817;
    property IsDefaultEdit: WordBool readonly dispid 1610743818;
    property IsLocked: WordBool readonly dispid 1610743819;
    function GetDrawOrder: Integer; dispid 1610743820;
    procedure add_PropertyChanged(const value: IUnknown); dispid 1610743821;
    procedure remove_PropertyChanged(const value: IUnknown); dispid 1610743822;
    procedure Initialize(cellRow: Integer; cellCol: Integer; Points: {??PSafeArray}OleVariant); dispid 1610743823;
    property NumPoints: Integer readonly dispid 1610743824;
    function GetPoints: {??PSafeArray}OleVariant; dispid 1610743825;
  end;

// *********************************************************************//
// Interface: _CvsGraphicFilledBox
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {67527C7E-F54B-3CCD-9B86-D29DCA88D7A6}
// *********************************************************************//
  _CvsGraphicFilledBox = interface(IDispatch)
    ['{67527C7E-F54B-3CCD-9B86-D29DCA88D7A6}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Clone: _CvsGraphic; safecall;
    function Get_CellLocation: CvsCellLocation; safecall;
    function Get_HighlightColorAsRgb: Integer; safecall;
    function Get_Label_: WideString; safecall;
    procedure Set_Label_(const pRetVal: WideString); safecall;
    function Get_IsAlwaysShown: WordBool; safecall;
    function Get_IsDefaultEdit: WordBool; safecall;
    function Get_IsLocked: WordBool; safecall;
    function GetDrawOrder: Integer; safecall;
    procedure add_PropertyChanged(const value: IUnknown); safecall;
    procedure remove_PropertyChanged(const value: IUnknown); safecall;
    procedure Initialize(cellRow: Integer; cellCol: Integer; regionRow: Single; regionCol: Single; 
                         regionHeight: Single; regionWidth: Single; regionAngle: Single; 
                         regionCurve: Single; const regionLabel: WideString); safecall;
    function Get_Location: CvsImageLocation; safecall;
    procedure Set_Location(pRetVal: CvsImageLocation); safecall;
    function Get_Height: Single; safecall;
    procedure Set_Height(pRetVal: Single); safecall;
    function Get_Width: Single; safecall;
    procedure Set_Width(pRetVal: Single); safecall;
    function Get_angle: Single; safecall;
    procedure Set_angle(pRetVal: Single); safecall;
    function Get_Curve: Single; safecall;
    procedure Set_Curve(pRetVal: Single); safecall;
    function Get_LabelForeColorAsRgb: Integer; safecall;
    procedure Set_LabelForeColorAsRgb(pRetVal: Integer); safecall;
    function Get_LabelBackColorAsRgb: Integer; safecall;
    procedure Set_LabelBackColorAsRgb(pRetVal: Integer); safecall;
    property ToString: WideString read Get_ToString;
    property CellLocation: CvsCellLocation read Get_CellLocation;
    property HighlightColorAsRgb: Integer read Get_HighlightColorAsRgb;
    property Label_: WideString read Get_Label_ write Set_Label_;
    property IsAlwaysShown: WordBool read Get_IsAlwaysShown;
    property IsDefaultEdit: WordBool read Get_IsDefaultEdit;
    property IsLocked: WordBool read Get_IsLocked;
    property Location: CvsImageLocation read Get_Location write Set_Location;
    property Height: Single read Get_Height write Set_Height;
    property Width: Single read Get_Width write Set_Width;
    property angle: Single read Get_angle write Set_angle;
    property Curve: Single read Get_Curve write Set_Curve;
    property LabelForeColorAsRgb: Integer read Get_LabelForeColorAsRgb write Set_LabelForeColorAsRgb;
    property LabelBackColorAsRgb: Integer read Get_LabelBackColorAsRgb write Set_LabelBackColorAsRgb;
  end;

// *********************************************************************//
// DispIntf:  _CvsGraphicFilledBoxDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {67527C7E-F54B-3CCD-9B86-D29DCA88D7A6}
// *********************************************************************//
  _CvsGraphicFilledBoxDisp = dispinterface
    ['{67527C7E-F54B-3CCD-9B86-D29DCA88D7A6}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    function Clone: _CvsGraphic; dispid 1610743812;
    property CellLocation: {??CvsCellLocation}OleVariant readonly dispid 1610743813;
    property HighlightColorAsRgb: Integer readonly dispid 1610743814;
    property Label_: WideString dispid 1610743815;
    property IsAlwaysShown: WordBool readonly dispid 1610743817;
    property IsDefaultEdit: WordBool readonly dispid 1610743818;
    property IsLocked: WordBool readonly dispid 1610743819;
    function GetDrawOrder: Integer; dispid 1610743820;
    procedure add_PropertyChanged(const value: IUnknown); dispid 1610743821;
    procedure remove_PropertyChanged(const value: IUnknown); dispid 1610743822;
    procedure Initialize(cellRow: Integer; cellCol: Integer; regionRow: Single; regionCol: Single; 
                         regionHeight: Single; regionWidth: Single; regionAngle: Single; 
                         regionCurve: Single; const regionLabel: WideString); dispid 1610743823;
    property Location: {??CvsImageLocation}OleVariant dispid 1610743824;
    property Height: Single dispid 1610743826;
    property Width: Single dispid 1610743828;
    property angle: Single dispid 1610743830;
    property Curve: Single dispid 1610743832;
    property LabelForeColorAsRgb: Integer dispid 1610743834;
    property LabelBackColorAsRgb: Integer dispid 1610743836;
  end;

// *********************************************************************//
// Interface: _CvsGraphicParallelogram
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {B9BC59F2-E341-3FE9-AEC2-C8299E29FFEE}
// *********************************************************************//
  _CvsGraphicParallelogram = interface(IDispatch)
    ['{B9BC59F2-E341-3FE9-AEC2-C8299E29FFEE}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Clone: _CvsGraphic; safecall;
    function Get_CellLocation: CvsCellLocation; safecall;
    function Get_HighlightColorAsRgb: Integer; safecall;
    function Get_Label_: WideString; safecall;
    procedure Set_Label_(const pRetVal: WideString); safecall;
    function Get_IsAlwaysShown: WordBool; safecall;
    function Get_IsDefaultEdit: WordBool; safecall;
    function Get_IsLocked: WordBool; safecall;
    function GetDrawOrder: Integer; safecall;
    procedure add_PropertyChanged(const value: IUnknown); safecall;
    procedure remove_PropertyChanged(const value: IUnknown); safecall;
    procedure Initialize(cellRow: Integer; cellCol: Integer; regionRow: Single; regionCol: Single; 
                         regionHeight: Single; regionWidth: Single; regionRotation: Single; 
                         regionInteriorAngle: Single; const regionLabel: WideString); safecall;
    function Get_Location: CvsImageLocation; safecall;
    procedure Set_Location(pRetVal: CvsImageLocation); safecall;
    function Get_Height: Single; safecall;
    procedure Set_Height(pRetVal: Single); safecall;
    function Get_Width: Single; safecall;
    procedure Set_Width(pRetVal: Single); safecall;
    function Get_Rotation: Single; safecall;
    procedure Set_Rotation(pRetVal: Single); safecall;
    function Get_InteriorAngle: Single; safecall;
    procedure Set_InteriorAngle(pRetVal: Single); safecall;
    function Get_Area: Single; safecall;
    function Get_LabelForeColorAsRgb: Integer; safecall;
    procedure Set_LabelForeColorAsRgb(pRetVal: Integer); safecall;
    function Get_LabelBackColorAsRgb: Integer; safecall;
    procedure Set_LabelBackColorAsRgb(pRetVal: Integer); safecall;
    function Get_EditFlags: CvsEditFlags; safecall;
    procedure Set_EditFlags(pRetVal: CvsEditFlags); safecall;
    function Get_EditBounds: Rectangle; safecall;
    procedure Set_EditBounds(pRetVal: Rectangle); safecall;
    property ToString: WideString read Get_ToString;
    property CellLocation: CvsCellLocation read Get_CellLocation;
    property HighlightColorAsRgb: Integer read Get_HighlightColorAsRgb;
    property Label_: WideString read Get_Label_ write Set_Label_;
    property IsAlwaysShown: WordBool read Get_IsAlwaysShown;
    property IsDefaultEdit: WordBool read Get_IsDefaultEdit;
    property IsLocked: WordBool read Get_IsLocked;
    property Location: CvsImageLocation read Get_Location write Set_Location;
    property Height: Single read Get_Height write Set_Height;
    property Width: Single read Get_Width write Set_Width;
    property Rotation: Single read Get_Rotation write Set_Rotation;
    property InteriorAngle: Single read Get_InteriorAngle write Set_InteriorAngle;
    property Area: Single read Get_Area;
    property LabelForeColorAsRgb: Integer read Get_LabelForeColorAsRgb write Set_LabelForeColorAsRgb;
    property LabelBackColorAsRgb: Integer read Get_LabelBackColorAsRgb write Set_LabelBackColorAsRgb;
    property EditFlags: CvsEditFlags read Get_EditFlags write Set_EditFlags;
    property EditBounds: Rectangle read Get_EditBounds write Set_EditBounds;
  end;

// *********************************************************************//
// DispIntf:  _CvsGraphicParallelogramDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {B9BC59F2-E341-3FE9-AEC2-C8299E29FFEE}
// *********************************************************************//
  _CvsGraphicParallelogramDisp = dispinterface
    ['{B9BC59F2-E341-3FE9-AEC2-C8299E29FFEE}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    function Clone: _CvsGraphic; dispid 1610743812;
    property CellLocation: {??CvsCellLocation}OleVariant readonly dispid 1610743813;
    property HighlightColorAsRgb: Integer readonly dispid 1610743814;
    property Label_: WideString dispid 1610743815;
    property IsAlwaysShown: WordBool readonly dispid 1610743817;
    property IsDefaultEdit: WordBool readonly dispid 1610743818;
    property IsLocked: WordBool readonly dispid 1610743819;
    function GetDrawOrder: Integer; dispid 1610743820;
    procedure add_PropertyChanged(const value: IUnknown); dispid 1610743821;
    procedure remove_PropertyChanged(const value: IUnknown); dispid 1610743822;
    procedure Initialize(cellRow: Integer; cellCol: Integer; regionRow: Single; regionCol: Single; 
                         regionHeight: Single; regionWidth: Single; regionRotation: Single; 
                         regionInteriorAngle: Single; const regionLabel: WideString); dispid 1610743823;
    property Location: {??CvsImageLocation}OleVariant dispid 1610743824;
    property Height: Single dispid 1610743826;
    property Width: Single dispid 1610743828;
    property Rotation: Single dispid 1610743830;
    property InteriorAngle: Single dispid 1610743832;
    property Area: Single readonly dispid 1610743834;
    property LabelForeColorAsRgb: Integer dispid 1610743835;
    property LabelBackColorAsRgb: Integer dispid 1610743837;
    property EditFlags: CvsEditFlags dispid 1610743839;
    property EditBounds: {??Rectangle}OleVariant dispid 1610743841;
  end;

// *********************************************************************//
// Interface: _CvsGraphicPolygon
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {A92D0551-AA65-34DA-9C31-6C246E1D9A49}
// *********************************************************************//
  _CvsGraphicPolygon = interface(IDispatch)
    ['{A92D0551-AA65-34DA-9C31-6C246E1D9A49}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Clone: _CvsGraphic; safecall;
    function Get_CellLocation: CvsCellLocation; safecall;
    function Get_HighlightColorAsRgb: Integer; safecall;
    function Get_Label_: WideString; safecall;
    procedure Set_Label_(const pRetVal: WideString); safecall;
    function Get_IsAlwaysShown: WordBool; safecall;
    function Get_IsDefaultEdit: WordBool; safecall;
    function Get_IsLocked: WordBool; safecall;
    function GetDrawOrder: Integer; safecall;
    procedure add_PropertyChanged(const value: IUnknown); safecall;
    procedure remove_PropertyChanged(const value: IUnknown); safecall;
    procedure Initialize(cellRow: Integer; cellCol: Integer; Points: PSafeArray; 
                         const Label_: WideString); safecall;
    function Get_Points: PSafeArray; safecall;
    procedure Set_Points(pRetVal: PSafeArray); safecall;
    function Get_PointCount: Integer; safecall;
    function GetPoint(index: Integer): CvsImageLocation; safecall;
    procedure SetPoint(index: Integer; Location: CvsImageLocation); safecall;
    procedure InsertPoint(index: Integer; Location: CvsImageLocation); safecall;
    procedure DeletePoint(index: Integer); safecall;
    procedure GhostMethod__CvsGraphicPolygon_120_1; safecall;
    function Get_Area: Double; safecall;
    function Get_Center: CvsImageLocation; safecall;
    function Get_EditFlags: CvsEditFlags; safecall;
    procedure Set_EditFlags1(pRetVal: CvsEditFlags); safecall;
    function Get_EditBounds: Rectangle; safecall;
    procedure Set_EditBounds1(pRetVal: Rectangle); safecall;
    property ToString: WideString read Get_ToString;
    property CellLocation: CvsCellLocation read Get_CellLocation;
    property HighlightColorAsRgb: Integer read Get_HighlightColorAsRgb;
    property Label_: WideString read Get_Label_ write Set_Label_;
    property IsAlwaysShown: WordBool read Get_IsAlwaysShown;
    property IsDefaultEdit: WordBool read Get_IsDefaultEdit;
    property IsLocked: WordBool read Get_IsLocked;
    property Points: PSafeArray read Get_Points write Set_Points;
    property PointCount: Integer read Get_PointCount;
    property Area: Double read Get_Area;
    property Center: CvsImageLocation read Get_Center;
    property EditFlags: CvsEditFlags read Get_EditFlags;
    property EditFlags1: CvsEditFlags write Set_EditFlags1;
    property EditBounds: Rectangle read Get_EditBounds;
    property EditBounds1: Rectangle write Set_EditBounds1;
  end;

// *********************************************************************//
// DispIntf:  _CvsGraphicPolygonDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {A92D0551-AA65-34DA-9C31-6C246E1D9A49}
// *********************************************************************//
  _CvsGraphicPolygonDisp = dispinterface
    ['{A92D0551-AA65-34DA-9C31-6C246E1D9A49}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    function Clone: _CvsGraphic; dispid 1610743812;
    property CellLocation: {??CvsCellLocation}OleVariant readonly dispid 1610743813;
    property HighlightColorAsRgb: Integer readonly dispid 1610743814;
    property Label_: WideString dispid 1610743815;
    property IsAlwaysShown: WordBool readonly dispid 1610743817;
    property IsDefaultEdit: WordBool readonly dispid 1610743818;
    property IsLocked: WordBool readonly dispid 1610743819;
    function GetDrawOrder: Integer; dispid 1610743820;
    procedure add_PropertyChanged(const value: IUnknown); dispid 1610743821;
    procedure remove_PropertyChanged(const value: IUnknown); dispid 1610743822;
    procedure Initialize(cellRow: Integer; cellCol: Integer; Points: {??PSafeArray}OleVariant; 
                         const Label_: WideString); dispid 1610743823;
    property Points: {??PSafeArray}OleVariant dispid 1610743824;
    property PointCount: Integer readonly dispid 1610743826;
    function GetPoint(index: Integer): {??CvsImageLocation}OleVariant; dispid 1610743827;
    procedure SetPoint(index: Integer; Location: {??CvsImageLocation}OleVariant); dispid 1610743828;
    procedure InsertPoint(index: Integer; Location: {??CvsImageLocation}OleVariant); dispid 1610743829;
    procedure DeletePoint(index: Integer); dispid 1610743830;
    procedure GhostMethod__CvsGraphicPolygon_120_1; dispid 1610743831;
    property Area: Double readonly dispid 1610743832;
    property Center: {??CvsImageLocation}OleVariant readonly dispid 1610743833;
    property EditFlags: CvsEditFlags readonly dispid 1610743834;
    property EditFlags1: CvsEditFlags writeonly dispid 1610743835;
    property EditBounds: {??Rectangle}OleVariant readonly dispid 1610743836;
    property EditBounds1: {??Rectangle}OleVariant writeonly dispid 1610743837;
  end;

// *********************************************************************//
// Interface: _CvsJobAnalyzer
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {48D9D7C9-0686-3892-8C91-FEDDEF77701D}
// *********************************************************************//
  _CvsJobAnalyzer = interface(IDispatch)
    ['{48D9D7C9-0686-3892-8C91-FEDDEF77701D}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    procedure add_Invalidated(const value: _EventHandler); safecall;
    procedure remove_Invalidated(const value: _EventHandler); safecall;
    function Get_inSight: _CvsInSight; safecall;
    procedure _Set_inSight(const pRetVal: _CvsInSight); safecall;
    function Get_Results: _CvsResultSet; safecall;
    function GetCellName(Location: CvsCellLocation): WideString; safecall;
    function GetCellReferenceInfo(Location: CvsCellLocation): _CvsCellReferenceInfo; safecall;
    function ReferenceConnected(destination: CvsCellLocation; source: CvsCellLocation): WordBool; safecall;
    function Get_NameList: _CvsSymbolicTagCollection; safecall;
    procedure Validate; safecall;
    function Get_HasChanged: WordBool; safecall;
    function Get_HasNameListChanged: WordBool; safecall;
    procedure Reset; safecall;
    procedure Dispose; safecall;
    property ToString: WideString read Get_ToString;
    property inSight: _CvsInSight read Get_inSight write _Set_inSight;
    property Results: _CvsResultSet read Get_Results;
    property NameList: _CvsSymbolicTagCollection read Get_NameList;
    property HasChanged: WordBool read Get_HasChanged;
    property HasNameListChanged: WordBool read Get_HasNameListChanged;
  end;

// *********************************************************************//
// DispIntf:  _CvsJobAnalyzerDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {48D9D7C9-0686-3892-8C91-FEDDEF77701D}
// *********************************************************************//
  _CvsJobAnalyzerDisp = dispinterface
    ['{48D9D7C9-0686-3892-8C91-FEDDEF77701D}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    procedure add_Invalidated(const value: _EventHandler); dispid 1610743812;
    procedure remove_Invalidated(const value: _EventHandler); dispid 1610743813;
    property inSight: _CvsInSight dispid 1610743814;
    property Results: _CvsResultSet readonly dispid 1610743816;
    function GetCellName(Location: {??CvsCellLocation}OleVariant): WideString; dispid 1610743817;
    function GetCellReferenceInfo(Location: {??CvsCellLocation}OleVariant): _CvsCellReferenceInfo; dispid 1610743818;
    function ReferenceConnected(destination: {??CvsCellLocation}OleVariant; 
                                source: {??CvsCellLocation}OleVariant): WordBool; dispid 1610743819;
    property NameList: _CvsSymbolicTagCollection readonly dispid 1610743820;
    procedure Validate; dispid 1610743821;
    property HasChanged: WordBool readonly dispid 1610743822;
    property HasNameListChanged: WordBool readonly dispid 1610743823;
    procedure Reset; dispid 1610743824;
    procedure Dispose; dispid 1610743825;
  end;

// *********************************************************************//
// Interface: _CvsModbusSettings
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {2274E1C7-5D1C-3912-AA83-815EBC41ACBE}
// *********************************************************************//
  _CvsModbusSettings = interface(IDispatch)
    ['{2274E1C7-5D1C-3912-AA83-815EBC41ACBE}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Clone: OleVariant; safecall;
    function Get_IdleTimeout: Integer; safecall;
    procedure Set_IdleTimeout(pRetVal: Integer); safecall;
    function Get_IdleTimeoutMin: Integer; safecall;
    function Get_IdleTimeoutMax: Integer; safecall;
    function Get_MaxConnections: Integer; safecall;
    procedure Set_MaxConnections(pRetVal: Integer); safecall;
    function Get_MaxConnectionsMin: Integer; safecall;
    function Get_MaxConnectionsMax: Integer; safecall;
    property ToString: WideString read Get_ToString;
    property IdleTimeout: Integer read Get_IdleTimeout write Set_IdleTimeout;
    property IdleTimeoutMin: Integer read Get_IdleTimeoutMin;
    property IdleTimeoutMax: Integer read Get_IdleTimeoutMax;
    property MaxConnections: Integer read Get_MaxConnections write Set_MaxConnections;
    property MaxConnectionsMin: Integer read Get_MaxConnectionsMin;
    property MaxConnectionsMax: Integer read Get_MaxConnectionsMax;
  end;

// *********************************************************************//
// DispIntf:  _CvsModbusSettingsDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {2274E1C7-5D1C-3912-AA83-815EBC41ACBE}
// *********************************************************************//
  _CvsModbusSettingsDisp = dispinterface
    ['{2274E1C7-5D1C-3912-AA83-815EBC41ACBE}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    function Clone: OleVariant; dispid 1610743812;
    property IdleTimeout: Integer dispid 1610743813;
    property IdleTimeoutMin: Integer readonly dispid 1610743815;
    property IdleTimeoutMax: Integer readonly dispid 1610743816;
    property MaxConnections: Integer dispid 1610743817;
    property MaxConnectionsMin: Integer readonly dispid 1610743819;
    property MaxConnectionsMax: Integer readonly dispid 1610743820;
  end;

// *********************************************************************//
// Interface: IPolygonEditor
// Flags:     (4288) Dual NonExtensible Dispatchable
// GUID:      {A21A33BD-7F07-4751-9B79-978986610AEA}
// *********************************************************************//
  IPolygonEditor = interface(IDispatch)
    ['{A21A33BD-7F07-4751-9B79-978986610AEA}']
    function Get_Points: PSafeArray; safecall;
    procedure UpdatePoints; safecall;
    procedure SetPoints(Points: PSafeArray); safecall;
    procedure SetPoint(index: Integer; point: CvsImageLocation); safecall;
    procedure InsertPoint(index: Integer; point: CvsImageLocation); safecall;
    procedure RemovePoint(index: Integer); safecall;
    property Points: PSafeArray read Get_Points;
  end;

// *********************************************************************//
// DispIntf:  IPolygonEditorDisp
// Flags:     (4288) Dual NonExtensible Dispatchable
// GUID:      {A21A33BD-7F07-4751-9B79-978986610AEA}
// *********************************************************************//
  IPolygonEditorDisp = dispinterface
    ['{A21A33BD-7F07-4751-9B79-978986610AEA}']
    property Points: {??PSafeArray}OleVariant readonly dispid 1610743808;
    procedure UpdatePoints; dispid 1610743809;
    procedure SetPoints(Points: {??PSafeArray}OleVariant); dispid 1610743810;
    procedure SetPoint(index: Integer; point: {??CvsImageLocation}OleVariant); dispid 1610743811;
    procedure InsertPoint(index: Integer; point: {??CvsImageLocation}OleVariant); dispid 1610743812;
    procedure RemovePoint(index: Integer); dispid 1610743813;
  end;

// *********************************************************************//
// Interface: _CvsString
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {57AC87B7-8CB9-31D7-9ED1-F0E965D3A083}
// *********************************************************************//
  _CvsString = interface(IDispatch)
    ['{57AC87B7-8CB9-31D7-9ED1-F0E965D3A083}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    property ToString: WideString read Get_ToString;
  end;

// *********************************************************************//
// DispIntf:  _CvsStringDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {57AC87B7-8CB9-31D7-9ED1-F0E965D3A083}
// *********************************************************************//
  _CvsStringDisp = dispinterface
    ['{57AC87B7-8CB9-31D7-9ED1-F0E965D3A083}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
  end;

// *********************************************************************//
// Interface: _StringProcessing
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {C3A4654A-70DB-3A89-96DF-2B952DFCBE73}
// *********************************************************************//
  _StringProcessing = interface(IDispatch)
    ['{C3A4654A-70DB-3A89-96DF-2B952DFCBE73}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    property ToString: WideString read Get_ToString;
  end;

// *********************************************************************//
// DispIntf:  _StringProcessingDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {C3A4654A-70DB-3A89-96DF-2B952DFCBE73}
// *********************************************************************//
  _StringProcessingDisp = dispinterface
    ['{C3A4654A-70DB-3A89-96DF-2B952DFCBE73}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
  end;

// *********************************************************************//
// Interface: _StringBuilderProcessing
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {B4A2A753-3DB2-3F82-8534-E591010C3FBF}
// *********************************************************************//
  _StringBuilderProcessing = interface(IDispatch)
    ['{B4A2A753-3DB2-3F82-8534-E591010C3FBF}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    property ToString: WideString read Get_ToString;
  end;

// *********************************************************************//
// DispIntf:  _StringBuilderProcessingDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {B4A2A753-3DB2-3F82-8534-E591010C3FBF}
// *********************************************************************//
  _StringBuilderProcessingDisp = dispinterface
    ['{B4A2A753-3DB2-3F82-8534-E591010C3FBF}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
  end;

// *********************************************************************//
// Interface: _CvsSymbolicTag
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {6499C465-7395-38EE-BEB8-2CF77021621C}
// *********************************************************************//
  _CvsSymbolicTag = interface(IDispatch)
    ['{6499C465-7395-38EE-BEB8-2CF77021621C}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Get_Location: CvsCellLocation; safecall;
    function Get_Name: WideString; safecall;
    function Get_ExposeToOpc: WordBool; safecall;
    function Get_AuditMessage: WordBool; safecall;
    function Get_InEasyView: WordBool; safecall;
    property ToString: WideString read Get_ToString;
    property Location: CvsCellLocation read Get_Location;
    property Name: WideString read Get_Name;
    property ExposeToOpc: WordBool read Get_ExposeToOpc;
    property AuditMessage: WordBool read Get_AuditMessage;
    property InEasyView: WordBool read Get_InEasyView;
  end;

// *********************************************************************//
// DispIntf:  _CvsSymbolicTagDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {6499C465-7395-38EE-BEB8-2CF77021621C}
// *********************************************************************//
  _CvsSymbolicTagDisp = dispinterface
    ['{6499C465-7395-38EE-BEB8-2CF77021621C}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    property Location: {??CvsCellLocation}OleVariant readonly dispid 1610743812;
    property Name: WideString readonly dispid 1610743813;
    property ExposeToOpc: WordBool readonly dispid 1610743814;
    property AuditMessage: WordBool readonly dispid 1610743815;
    property InEasyView: WordBool readonly dispid 1610743816;
  end;

// *********************************************************************//
// Interface: _CvsSymbolicTagCollection
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {BF5B2074-DEB8-3A63-85D5-C0A0C1F24D78}
// *********************************************************************//
  _CvsSymbolicTagCollection = interface(IDispatch)
    ['{BF5B2074-DEB8-3A63-85D5-C0A0C1F24D78}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Get_Count: Integer; safecall;
    procedure Clear; safecall;
    function IndexOf(const Item: _CvsSymbolicTag): Integer; safecall;
    procedure Add(const Item: _CvsSymbolicTag); safecall;
    procedure Remove(Location: CvsCellLocation); safecall;
    procedure RemoveAt(index: Integer); safecall;
    function Get_Item(Location: CvsCellLocation): _CvsSymbolicTag; safecall;
    function Get_Item_2(index: Integer): _CvsSymbolicTag; safecall;
    function Get_Item_3(const Name: WideString): _CvsSymbolicTag; safecall;
    procedure SetName(Location: CvsCellLocation; const Name: WideString); safecall;
    procedure SetName_2(Location: CvsCellLocation; const Name: WideString; ExposeToOpc: WordBool); safecall;
    procedure SetName_3(Location: CvsCellLocation; const Name: WideString; ExposeToOpc: WordBool; 
                        AuditMessage: WordBool); safecall;
    function UpdateTagLocation(var Tag: _CvsSymbolicTag; Location: CvsCellLocation): WordBool; safecall;
    function UpdateTagName(var Tag: _CvsSymbolicTag; const Name: WideString): WordBool; safecall;
    function UpdateTag(var oldTag: _CvsSymbolicTag; Location: CvsCellLocation; 
                       const Name: WideString; ExposeToOpc: WordBool; AuditMessage: WordBool; 
                       InEasyView: WordBool): WordBool; safecall;
    property ToString: WideString read Get_ToString;
    property Count: Integer read Get_Count;
    property Item[Location: CvsCellLocation]: _CvsSymbolicTag read Get_Item; default;
    property Item_2[index: Integer]: _CvsSymbolicTag read Get_Item_2;
    property Item_3[const Name: WideString]: _CvsSymbolicTag read Get_Item_3;
  end;

// *********************************************************************//
// DispIntf:  _CvsSymbolicTagCollectionDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {BF5B2074-DEB8-3A63-85D5-C0A0C1F24D78}
// *********************************************************************//
  _CvsSymbolicTagCollectionDisp = dispinterface
    ['{BF5B2074-DEB8-3A63-85D5-C0A0C1F24D78}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 1610743810;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    property Count: Integer readonly dispid 1610743812;
    procedure Clear; dispid 1610743813;
    function IndexOf(const Item: _CvsSymbolicTag): Integer; dispid 1610743814;
    procedure Add(const Item: _CvsSymbolicTag); dispid 1610743815;
    procedure Remove(Location: {??CvsCellLocation}OleVariant); dispid 1610743816;
    procedure RemoveAt(index: Integer); dispid 1610743817;
    property Item[Location: {??CvsCellLocation}OleVariant]: _CvsSymbolicTag readonly dispid 0; default;
    property Item_2[index: Integer]: _CvsSymbolicTag readonly dispid 1610743819;
    property Item_3[const Name: WideString]: _CvsSymbolicTag readonly dispid 1610743820;
    procedure SetName(Location: {??CvsCellLocation}OleVariant; const Name: WideString); dispid 1610743821;
    procedure SetName_2(Location: {??CvsCellLocation}OleVariant; const Name: WideString; 
                        ExposeToOpc: WordBool); dispid 1610743822;
    procedure SetName_3(Location: {??CvsCellLocation}OleVariant; const Name: WideString; 
                        ExposeToOpc: WordBool; AuditMessage: WordBool); dispid 1610743823;
    function UpdateTagLocation(var Tag: _CvsSymbolicTag; Location: {??CvsCellLocation}OleVariant): WordBool; dispid 1610743824;
    function UpdateTagName(var Tag: _CvsSymbolicTag; const Name: WideString): WordBool; dispid 1610743825;
    function UpdateTag(var oldTag: _CvsSymbolicTag; Location: {??CvsCellLocation}OleVariant; 
                       const Name: WideString; ExposeToOpc: WordBool; AuditMessage: WordBool; 
                       InEasyView: WordBool): WordBool; dispid 1610743826;
  end;

// *********************************************************************//
// Interface: _CvsDiscreteIOLines
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {FC6F4204-4B5E-3D6D-A5C9-9C0086DB2A6E}
// *********************************************************************//
  _CvsDiscreteIOLines = interface(IDispatch)
    ['{FC6F4204-4B5E-3D6D-A5C9-9C0086DB2A6E}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Clone: OleVariant; safecall;
    function Get_Inputs: _CvsInputCollection; safecall;
    function Get_Outputs: _CvsOutputCollection; safecall;
    function IsValidInputType(line: Integer; Type_: CvsInputType): WordBool; safecall;
    procedure SetInputType(line: Integer; value: CvsInputType); safecall;
    procedure SetOutputType(line: Integer; value: CvsOutputType); safecall;
    function IsEqual(const ioLines: _CvsDiscreteIOLines): WordBool; safecall;
    property ToString: WideString read Get_ToString;
    property Inputs: _CvsInputCollection read Get_Inputs;
    property Outputs: _CvsOutputCollection read Get_Outputs;
  end;

// *********************************************************************//
// DispIntf:  _CvsDiscreteIOLinesDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {FC6F4204-4B5E-3D6D-A5C9-9C0086DB2A6E}
// *********************************************************************//
  _CvsDiscreteIOLinesDisp = dispinterface
    ['{FC6F4204-4B5E-3D6D-A5C9-9C0086DB2A6E}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    function Clone: OleVariant; dispid 1610743812;
    property Inputs: _CvsInputCollection readonly dispid 1610743813;
    property Outputs: _CvsOutputCollection readonly dispid 1610743814;
    function IsValidInputType(line: Integer; Type_: CvsInputType): WordBool; dispid 1610743815;
    procedure SetInputType(line: Integer; value: CvsInputType); dispid 1610743816;
    procedure SetOutputType(line: Integer; value: CvsOutputType); dispid 1610743817;
    function IsEqual(const ioLines: _CvsDiscreteIOLines): WordBool; dispid 1610743818;
  end;

// *********************************************************************//
// Interface: _CvsUniversalIOSettings
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {787A43A5-D389-3360-8041-E3ECA5BF78FE}
// *********************************************************************//
  _CvsUniversalIOSettings = interface(IDispatch)
    ['{787A43A5-D389-3360-8041-E3ECA5BF78FE}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Clone: OleVariant; safecall;
    function Get_Inputs: _CvsInputCollection; safecall;
    function Get_Outputs: _CvsOutputCollection; safecall;
    function IsValidInputType(line: Integer; Type_: CvsInputType): WordBool; safecall;
    procedure SetInputType(line: Integer; value: CvsInputType); safecall;
    procedure SetOutputType(line: Integer; value: CvsOutputType); safecall;
    function IsEqual(const ioLines: _CvsDiscreteIOLines): WordBool; safecall;
    property ToString: WideString read Get_ToString;
    property Inputs: _CvsInputCollection read Get_Inputs;
    property Outputs: _CvsOutputCollection read Get_Outputs;
  end;

// *********************************************************************//
// DispIntf:  _CvsUniversalIOSettingsDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {787A43A5-D389-3360-8041-E3ECA5BF78FE}
// *********************************************************************//
  _CvsUniversalIOSettingsDisp = dispinterface
    ['{787A43A5-D389-3360-8041-E3ECA5BF78FE}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    function Clone: OleVariant; dispid 1610743812;
    property Inputs: _CvsInputCollection readonly dispid 1610743813;
    property Outputs: _CvsOutputCollection readonly dispid 1610743814;
    function IsValidInputType(line: Integer; Type_: CvsInputType): WordBool; dispid 1610743815;
    procedure SetInputType(line: Integer; value: CvsInputType); dispid 1610743816;
    procedure SetOutputType(line: Integer; value: CvsOutputType); dispid 1610743817;
    function IsEqual(const ioLines: _CvsDiscreteIOLines): WordBool; dispid 1610743818;
  end;

// *********************************************************************//
// Interface: _CvsPassFailSettings
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {6D16034F-7757-3D30-B236-1FCD37A25E33}
// *********************************************************************//
  _CvsPassFailSettings = interface(IDispatch)
    ['{6D16034F-7757-3D30-B236-1FCD37A25E33}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Get_Location: CvsCellLocation; safecall;
    procedure Set_Location(pRetVal: CvsCellLocation); safecall;
    function Get_StatusNeg: CvsStatusLevel; safecall;
    procedure Set_StatusNeg(pRetVal: CvsStatusLevel); safecall;
    function Get_StatusZero: CvsStatusLevel; safecall;
    procedure Set_StatusZero(pRetVal: CvsStatusLevel); safecall;
    function Get_StatusPos: CvsStatusLevel; safecall;
    procedure Set_StatusPos(pRetVal: CvsStatusLevel); safecall;
    function Get_StatusErr: CvsStatusLevel; safecall;
    procedure Set_StatusErr(pRetVal: CvsStatusLevel); safecall;
    function Get_StatusString: CvsStatusLevel; safecall;
    procedure Set_StatusString(pRetVal: CvsStatusLevel); safecall;
    function Get_StatusStruct: CvsStatusLevel; safecall;
    procedure Set_StatusStruct(pRetVal: CvsStatusLevel); safecall;
    property ToString: WideString read Get_ToString;
    property Location: CvsCellLocation read Get_Location write Set_Location;
    property StatusNeg: CvsStatusLevel read Get_StatusNeg write Set_StatusNeg;
    property StatusZero: CvsStatusLevel read Get_StatusZero write Set_StatusZero;
    property StatusPos: CvsStatusLevel read Get_StatusPos write Set_StatusPos;
    property StatusErr: CvsStatusLevel read Get_StatusErr write Set_StatusErr;
    property StatusString: CvsStatusLevel read Get_StatusString write Set_StatusString;
    property StatusStruct: CvsStatusLevel read Get_StatusStruct write Set_StatusStruct;
  end;

// *********************************************************************//
// DispIntf:  _CvsPassFailSettingsDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {6D16034F-7757-3D30-B236-1FCD37A25E33}
// *********************************************************************//
  _CvsPassFailSettingsDisp = dispinterface
    ['{6D16034F-7757-3D30-B236-1FCD37A25E33}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    property Location: {??CvsCellLocation}OleVariant dispid 1610743812;
    property StatusNeg: CvsStatusLevel dispid 1610743814;
    property StatusZero: CvsStatusLevel dispid 1610743816;
    property StatusPos: CvsStatusLevel dispid 1610743818;
    property StatusErr: CvsStatusLevel dispid 1610743820;
    property StatusString: CvsStatusLevel dispid 1610743822;
    property StatusStruct: CvsStatusLevel dispid 1610743824;
  end;

// *********************************************************************//
// Interface: _CvsHostCollection
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {71715B58-C516-3A42-B9BA-AA1D5975FDD6}
// *********************************************************************//
  _CvsHostCollection = interface(IDispatch)
    ['{71715B58-C516-3A42-B9BA-AA1D5975FDD6}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function GetEnumerator: IEnumVARIANT; safecall;
    function Get_Count: Integer; safecall;
    function Get_Item(index: Integer): _CvsHostSensor; safecall;
    function Get_Item_2(const Name: WideString): _CvsHostSensor; safecall;
    function Get_Items: PSafeArray; safecall;
    property ToString: WideString read Get_ToString;
    property Count: Integer read Get_Count;
    property Item[index: Integer]: _CvsHostSensor read Get_Item; default;
    property Item_2[const Name: WideString]: _CvsHostSensor read Get_Item_2;
    property Items: PSafeArray read Get_Items;
  end;

// *********************************************************************//
// DispIntf:  _CvsHostCollectionDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {71715B58-C516-3A42-B9BA-AA1D5975FDD6}
// *********************************************************************//
  _CvsHostCollectionDisp = dispinterface
    ['{71715B58-C516-3A42-B9BA-AA1D5975FDD6}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 1610743810;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    function GetEnumerator: IEnumVARIANT; dispid -4;
    property Count: Integer readonly dispid 1610743813;
    property Item[index: Integer]: _CvsHostSensor readonly dispid 0; default;
    property Item_2[const Name: WideString]: _CvsHostSensor readonly dispid 1610743815;
    property Items: {??PSafeArray}OleVariant readonly dispid 1610743816;
  end;

// *********************************************************************//
// Interface: _CvsHost
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {ED6DB3C3-77D4-372B-B6F1-53B506A61A9C}
// *********************************************************************//
  _CvsHost = interface(IDispatch)
    ['{ED6DB3C3-77D4-372B-B6F1-53B506A61A9C}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Matches(const host: _CvsHost): WordBool; safecall;
    function Get_Name: WideString; safecall;
    function Get_IPAddressString: WideString; safecall;
    function Get_type_: CvsHostType; safecall;
    function Get_Subtype: CvsHostSubtype; safecall;
    property ToString: WideString read Get_ToString;
    property Name: WideString read Get_Name;
    property IPAddressString: WideString read Get_IPAddressString;
    property type_: CvsHostType read Get_type_;
    property Subtype: CvsHostSubtype read Get_Subtype;
  end;

// *********************************************************************//
// DispIntf:  _CvsHostDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {ED6DB3C3-77D4-372B-B6F1-53B506A61A9C}
// *********************************************************************//
  _CvsHostDisp = dispinterface
    ['{ED6DB3C3-77D4-372B-B6F1-53B506A61A9C}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    function Matches(const host: _CvsHost): WordBool; dispid 1610743812;
    property Name: WideString readonly dispid 1610743813;
    property IPAddressString: WideString readonly dispid 1610743814;
    property type_: CvsHostType readonly dispid 1610743815;
    property Subtype: CvsHostSubtype readonly dispid 1610743816;
  end;

// *********************************************************************//
// Interface: _CvsHostSensor
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {13E6663F-68D4-38A5-9F86-C086032B49B3}
// *********************************************************************//
  _CvsHostSensor = interface(IDispatch)
    ['{13E6663F-68D4-38A5-9F86-C086032B49B3}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Matches(const hostBase: _CvsHost): WordBool; safecall;
    function Get_Name: WideString; safecall;
    function Get_IPAddressString: WideString; safecall;
    function Get_type_: CvsHostType; safecall;
    function Get_Subtype: CvsHostSubtype; safecall;
    function Get_MacAddress: WideString; safecall;
    function Get_LocalInterfaceString: WideString; safecall;
    function Get_Scope: CvsNetworkMonitorScope; safecall;
    function Get_VersionString: WideString; safecall;
    function Get_Version: _Version; safecall;
    function Get_SerialNumber: WideString; safecall;
    function Get_ModelNumber: WideString; safecall;
    function Get_ProductPartNumber: WideString; safecall;
    function Get_HardwarePartNumber: WideString; safecall;
    function SupportsTool(tool: CvsSupportedTools): WordBool; safecall;
    function SupportsFeature(feature: CvsSupportedFeatures): WordBool; safecall;
    function Get_IsDhcpEnabled: WordBool; safecall;
    function Get_SubnetMask: IUnknown; safecall;
    function Get_DefaultGateway: IUnknown; safecall;
    function Get_DnsServer: IUnknown; safecall;
    function Get_DomainName: WideString; safecall;
    property ToString: WideString read Get_ToString;
    property Name: WideString read Get_Name;
    property IPAddressString: WideString read Get_IPAddressString;
    property type_: CvsHostType read Get_type_;
    property Subtype: CvsHostSubtype read Get_Subtype;
    property MacAddress: WideString read Get_MacAddress;
    property LocalInterfaceString: WideString read Get_LocalInterfaceString;
    property Scope: CvsNetworkMonitorScope read Get_Scope;
    property VersionString: WideString read Get_VersionString;
    property Version: _Version read Get_Version;
    property SerialNumber: WideString read Get_SerialNumber;
    property ModelNumber: WideString read Get_ModelNumber;
    property ProductPartNumber: WideString read Get_ProductPartNumber;
    property HardwarePartNumber: WideString read Get_HardwarePartNumber;
    property IsDhcpEnabled: WordBool read Get_IsDhcpEnabled;
    property SubnetMask: IUnknown read Get_SubnetMask;
    property DefaultGateway: IUnknown read Get_DefaultGateway;
    property DnsServer: IUnknown read Get_DnsServer;
    property DomainName: WideString read Get_DomainName;
  end;

// *********************************************************************//
// DispIntf:  _CvsHostSensorDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {13E6663F-68D4-38A5-9F86-C086032B49B3}
// *********************************************************************//
  _CvsHostSensorDisp = dispinterface
    ['{13E6663F-68D4-38A5-9F86-C086032B49B3}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    function Matches(const hostBase: _CvsHost): WordBool; dispid 1610743812;
    property Name: WideString readonly dispid 1610743813;
    property IPAddressString: WideString readonly dispid 1610743814;
    property type_: CvsHostType readonly dispid 1610743815;
    property Subtype: CvsHostSubtype readonly dispid 1610743816;
    property MacAddress: WideString readonly dispid 1610743817;
    property LocalInterfaceString: WideString readonly dispid 1610743818;
    property Scope: CvsNetworkMonitorScope readonly dispid 1610743819;
    property VersionString: WideString readonly dispid 1610743820;
    property Version: _Version readonly dispid 1610743821;
    property SerialNumber: WideString readonly dispid 1610743822;
    property ModelNumber: WideString readonly dispid 1610743823;
    property ProductPartNumber: WideString readonly dispid 1610743824;
    property HardwarePartNumber: WideString readonly dispid 1610743825;
    function SupportsTool(tool: CvsSupportedTools): WordBool; dispid 1610743826;
    function SupportsFeature(feature: CvsSupportedFeatures): WordBool; dispid 1610743827;
    property IsDhcpEnabled: WordBool readonly dispid 1610743828;
    property SubnetMask: IUnknown readonly dispid 1610743829;
    property DefaultGateway: IUnknown readonly dispid 1610743830;
    property DnsServer: IUnknown readonly dispid 1610743831;
    property DomainName: WideString readonly dispid 1610743832;
  end;

// *********************************************************************//
// Interface: _CvsNetworkListener
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {5BDEBB01-09ED-3A81-9099-CA4EFBE85481}
// *********************************************************************//
  _CvsNetworkListener = interface(IDispatch)
    ['{5BDEBB01-09ED-3A81-9099-CA4EFBE85481}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    procedure Refresh; safecall;
    procedure Dispose; safecall;
    function Get_Hosts: _CvsHostCollection; safecall;
    function Get_Scope: CvsNetworkMonitorScope; safecall;
    procedure Set_Scope(pRetVal: CvsNetworkMonitorScope); safecall;
    procedure GhostMethod__CvsNetworkListener_64_1; safecall;
    function Resolve(const Name: WideString): _CvsHostSensor; safecall;
    function Resolve_2(const ip: IUnknown): _CvsHostSensor; safecall;
    function SendFlash(const sensor: _CvsHostSensor): WordBool; safecall;
    function SendSetNetwork(const sensor: _CvsHostSensor; const Username: WideString; 
                            const Password: WideString; const HostName: WideString; 
                            useDhcp: WordBool; const ip: IUnknown; const subnet: IUnknown; 
                            const gateway: IUnknown; const dns: IUnknown; 
                            const DomainName: WideString; Reset: WordBool): WordBool; safecall;
    function SupportsCommand(const sensor: _CvsHostSensor; cmd: CvsNetworkMonitorCommand): WordBool; safecall;
    property ToString: WideString read Get_ToString;
    property Hosts: _CvsHostCollection read Get_Hosts;
    property Scope: CvsNetworkMonitorScope read Get_Scope write Set_Scope;
  end;

// *********************************************************************//
// DispIntf:  _CvsNetworkListenerDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {5BDEBB01-09ED-3A81-9099-CA4EFBE85481}
// *********************************************************************//
  _CvsNetworkListenerDisp = dispinterface
    ['{5BDEBB01-09ED-3A81-9099-CA4EFBE85481}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    procedure Refresh; dispid 1610743812;
    procedure Dispose; dispid 1610743813;
    property Hosts: _CvsHostCollection readonly dispid 1610743814;
    property Scope: CvsNetworkMonitorScope dispid 1610743815;
    procedure GhostMethod__CvsNetworkListener_64_1; dispid 1610743817;
    function Resolve(const Name: WideString): _CvsHostSensor; dispid 1610743818;
    function Resolve_2(const ip: IUnknown): _CvsHostSensor; dispid 1610743819;
    function SendFlash(const sensor: _CvsHostSensor): WordBool; dispid 1610743820;
    function SendSetNetwork(const sensor: _CvsHostSensor; const Username: WideString; 
                            const Password: WideString; const HostName: WideString; 
                            useDhcp: WordBool; const ip: IUnknown; const subnet: IUnknown; 
                            const gateway: IUnknown; const dns: IUnknown; 
                            const DomainName: WideString; Reset: WordBool): WordBool; dispid 1610743821;
    function SupportsCommand(const sensor: _CvsHostSensor; cmd: CvsNetworkMonitorCommand): WordBool; dispid 1610743822;
  end;

// *********************************************************************//
// Interface: _CvsNetworkMonitor
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {3473F45C-91E2-33BE-83C1-FF2AFEA34248}
// *********************************************************************//
  _CvsNetworkMonitor = interface(IDispatch)
    ['{3473F45C-91E2-33BE-83C1-FF2AFEA34248}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    procedure Refresh; safecall;
    procedure Dispose; safecall;
    function Get_Hosts: _CvsHostCollection; safecall;
    function Get_Scope: CvsNetworkMonitorScope; safecall;
    procedure Set_Scope(pRetVal: CvsNetworkMonitorScope); safecall;
    procedure GhostMethod__CvsNetworkMonitor_64_1; safecall;
    function Resolve(const Name: WideString): _CvsHostSensor; safecall;
    function Resolve_2(const ip: IUnknown): _CvsHostSensor; safecall;
    function SendFlash(const sensor: _CvsHostSensor): WordBool; safecall;
    function SendSetNetwork(const sensor: _CvsHostSensor; const Username: WideString; 
                            const Password: WideString; const HostName: WideString; 
                            useDhcp: WordBool; const ip: IUnknown; const subnet: IUnknown; 
                            const gateway: IUnknown; const dns: IUnknown; 
                            const DomainName: WideString; Reset: WordBool): WordBool; safecall;
    function SupportsCommand(const sensor: _CvsHostSensor; cmd: CvsNetworkMonitorCommand): WordBool; safecall;
    procedure add_HostsChanged(const value: _EventHandler); safecall;
    procedure remove_HostsChanged(const value: _EventHandler); safecall;
    function Get_Enabled: WordBool; safecall;
    procedure Set_Enabled1(pRetVal: WordBool); safecall;
    function Get_PingInterval: Integer; safecall;
    procedure Set_PingInterval1(pRetVal: Integer); safecall;
    property ToString: WideString read Get_ToString;
    property Hosts: _CvsHostCollection read Get_Hosts;
    property Scope: CvsNetworkMonitorScope read Get_Scope write Set_Scope;
    property Enabled: WordBool read Get_Enabled;
    property Enabled1: WordBool write Set_Enabled1;
    property PingInterval: Integer read Get_PingInterval;
    property PingInterval1: Integer write Set_PingInterval1;
  end;

// *********************************************************************//
// DispIntf:  _CvsNetworkMonitorDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {3473F45C-91E2-33BE-83C1-FF2AFEA34248}
// *********************************************************************//
  _CvsNetworkMonitorDisp = dispinterface
    ['{3473F45C-91E2-33BE-83C1-FF2AFEA34248}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    procedure Refresh; dispid 1610743812;
    procedure Dispose; dispid 1610743813;
    property Hosts: _CvsHostCollection readonly dispid 1610743814;
    property Scope: CvsNetworkMonitorScope dispid 1610743815;
    procedure GhostMethod__CvsNetworkMonitor_64_1; dispid 1610743817;
    function Resolve(const Name: WideString): _CvsHostSensor; dispid 1610743818;
    function Resolve_2(const ip: IUnknown): _CvsHostSensor; dispid 1610743819;
    function SendFlash(const sensor: _CvsHostSensor): WordBool; dispid 1610743820;
    function SendSetNetwork(const sensor: _CvsHostSensor; const Username: WideString; 
                            const Password: WideString; const HostName: WideString; 
                            useDhcp: WordBool; const ip: IUnknown; const subnet: IUnknown; 
                            const gateway: IUnknown; const dns: IUnknown; 
                            const DomainName: WideString; Reset: WordBool): WordBool; dispid 1610743821;
    function SupportsCommand(const sensor: _CvsHostSensor; cmd: CvsNetworkMonitorCommand): WordBool; dispid 1610743822;
    procedure add_HostsChanged(const value: _EventHandler); dispid 1610743823;
    procedure remove_HostsChanged(const value: _EventHandler); dispid 1610743824;
    property Enabled: WordBool readonly dispid 1610743825;
    property Enabled1: WordBool writeonly dispid 1610743826;
    property PingInterval: Integer readonly dispid 1610743827;
    property PingInterval1: Integer writeonly dispid 1610743828;
  end;

// *********************************************************************//
// Interface: _CvsNetworkMonitorSubnet
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {12CA0420-1A75-3BFB-BDB0-A51DBA47EF98}
// *********************************************************************//
  _CvsNetworkMonitorSubnet = interface(IDispatch)
    ['{12CA0420-1A75-3BFB-BDB0-A51DBA47EF98}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Get_IPAddress: IUnknown; safecall;
    function Get_SubnetMask: IUnknown; safecall;
    function Get_BroadcastAddress: IUnknown; safecall;
    property ToString: WideString read Get_ToString;
    property IPAddress: IUnknown read Get_IPAddress;
    property SubnetMask: IUnknown read Get_SubnetMask;
    property BroadcastAddress: IUnknown read Get_BroadcastAddress;
  end;

// *********************************************************************//
// DispIntf:  _CvsNetworkMonitorSubnetDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {12CA0420-1A75-3BFB-BDB0-A51DBA47EF98}
// *********************************************************************//
  _CvsNetworkMonitorSubnetDisp = dispinterface
    ['{12CA0420-1A75-3BFB-BDB0-A51DBA47EF98}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    property IPAddress: IUnknown readonly dispid 1610743812;
    property SubnetMask: IUnknown readonly dispid 1610743813;
    property BroadcastAddress: IUnknown readonly dispid 1610743814;
  end;

// *********************************************************************//
// DispIntf:  ICvsNetworkMonitorEvents
// Flags:     (4224) NonExtensible Dispatchable
// GUID:      {AE9E35F0-292A-4030-9C5E-8AE5B5C145DB}
// *********************************************************************//
  ICvsNetworkMonitorEvents = dispinterface
    ['{AE9E35F0-292A-4030-9C5E-8AE5B5C145DB}']
    procedure HostsChanged(sender: OleVariant; const e: _EventArgs); dispid 1;
  end;

// *********************************************************************//
// Interface: _CvsOcrMaxAutoTune
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {E282C93F-E9A1-3167-82E5-89F35C13E4A9}
// *********************************************************************//
  _CvsOcrMaxAutoTune = interface(IDispatch)
    ['{E282C93F-E9A1-3167-82E5-89F35C13E4A9}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Get_inSight: _CvsInSight; safecall;
    procedure _Set_inSight(const pRetVal: _CvsInSight); safecall;
    function Get_CellLocation: CvsCellLocation; safecall;
    procedure Set_CellLocation(pRetVal: CvsCellLocation); safecall;
    function Get_ExpressionEditor: ICvsOcrMaxExpressionEditor; safecall;
    procedure _Set_ExpressionEditor(const pRetVal: ICvsOcrMaxExpressionEditor); safecall;
    function Get_settings: _AutoTuneSettings; safecall;
    procedure add_SettingsChanged(const value: _EventHandler); safecall;
    procedure remove_SettingsChanged(const value: _EventHandler); safecall;
    procedure LockReadOnlyParameters; safecall;
    procedure QueryAutoTuneSettings; safecall;
    procedure SendAutoTuneSettings; safecall;
    function Get_SessionActive: WordBool; safecall;
    procedure StartSession; safecall;
    procedure EndSession(commitSegmentationParameters: WordBool; commitFontCharacters: WordBool); safecall;
    procedure add_AutoTuneSessionClosed(const value: _AutoTuneSessionClosedHandler); safecall;
    procedure remove_AutoTuneSessionClosed(const value: _AutoTuneSessionClosedHandler); safecall;
    function Get_LastSessionFileName: WideString; safecall;
    procedure Set_LastSessionFileName(const pRetVal: WideString); safecall;
    procedure Import(data: PSafeArray); safecall;
    function Export: PSafeArray; safecall;
    function AutoSegment(const trainText: WideString): PSafeArray; safecall;
    procedure ShowAutoSegmentResult(const result: _AutoSegmentResult); safecall;
    function AutoTune(const result: _AutoSegmentResult): PSafeArray; safecall;
    function AutoTune_2(const trainText: WideString; segments: PSafeArray): PSafeArray; safecall;
    function TrainCharacters(const trainText: WideString): PSafeArray; safecall;
    function Get_Records: PSafeArray; safecall;
    procedure add_RecordsChanged(const value: _EventHandler); safecall;
    procedure remove_RecordsChanged(const value: _EventHandler); safecall;
    function UpdateRecord(const record_: _AutoTuneRecord): PSafeArray; safecall;
    function DeleteRecord(const record_: _AutoTuneRecord): PSafeArray; safecall;
    procedure GetRecordImageAndMarkings(const record_: _AutoTuneRecord); safecall;
    property ToString: WideString read Get_ToString;
    property inSight: _CvsInSight read Get_inSight write _Set_inSight;
    property CellLocation: CvsCellLocation read Get_CellLocation write Set_CellLocation;
    property ExpressionEditor: ICvsOcrMaxExpressionEditor read Get_ExpressionEditor write _Set_ExpressionEditor;
    property settings: _AutoTuneSettings read Get_settings;
    property SessionActive: WordBool read Get_SessionActive;
    property LastSessionFileName: WideString read Get_LastSessionFileName write Set_LastSessionFileName;
    property Records: PSafeArray read Get_Records;
  end;

// *********************************************************************//
// DispIntf:  _CvsOcrMaxAutoTuneDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {E282C93F-E9A1-3167-82E5-89F35C13E4A9}
// *********************************************************************//
  _CvsOcrMaxAutoTuneDisp = dispinterface
    ['{E282C93F-E9A1-3167-82E5-89F35C13E4A9}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    property inSight: _CvsInSight dispid 1610743812;
    property CellLocation: {??CvsCellLocation}OleVariant dispid 1610743814;
    property ExpressionEditor: ICvsOcrMaxExpressionEditor dispid 1610743816;
    property settings: _AutoTuneSettings readonly dispid 1610743818;
    procedure add_SettingsChanged(const value: _EventHandler); dispid 1610743819;
    procedure remove_SettingsChanged(const value: _EventHandler); dispid 1610743820;
    procedure LockReadOnlyParameters; dispid 1610743821;
    procedure QueryAutoTuneSettings; dispid 1610743822;
    procedure SendAutoTuneSettings; dispid 1610743823;
    property SessionActive: WordBool readonly dispid 1610743824;
    procedure StartSession; dispid 1610743825;
    procedure EndSession(commitSegmentationParameters: WordBool; commitFontCharacters: WordBool); dispid 1610743826;
    procedure add_AutoTuneSessionClosed(const value: _AutoTuneSessionClosedHandler); dispid 1610743827;
    procedure remove_AutoTuneSessionClosed(const value: _AutoTuneSessionClosedHandler); dispid 1610743828;
    property LastSessionFileName: WideString dispid 1610743829;
    procedure Import(data: {??PSafeArray}OleVariant); dispid 1610743831;
    function Export: {??PSafeArray}OleVariant; dispid 1610743832;
    function AutoSegment(const trainText: WideString): {??PSafeArray}OleVariant; dispid 1610743833;
    procedure ShowAutoSegmentResult(const result: _AutoSegmentResult); dispid 1610743834;
    function AutoTune(const result: _AutoSegmentResult): {??PSafeArray}OleVariant; dispid 1610743835;
    function AutoTune_2(const trainText: WideString; segments: {??PSafeArray}OleVariant): {??PSafeArray}OleVariant; dispid 1610743836;
    function TrainCharacters(const trainText: WideString): {??PSafeArray}OleVariant; dispid 1610743837;
    property Records: {??PSafeArray}OleVariant readonly dispid 1610743838;
    procedure add_RecordsChanged(const value: _EventHandler); dispid 1610743839;
    procedure remove_RecordsChanged(const value: _EventHandler); dispid 1610743840;
    function UpdateRecord(const record_: _AutoTuneRecord): {??PSafeArray}OleVariant; dispid 1610743841;
    function DeleteRecord(const record_: _AutoTuneRecord): {??PSafeArray}OleVariant; dispid 1610743842;
    procedure GetRecordImageAndMarkings(const record_: _AutoTuneRecord); dispid 1610743843;
  end;

// *********************************************************************//
// Interface: _AutoTuneSettings
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {85A67E4F-F2F2-3794-8977-C537621C0D83}
// *********************************************************************//
  _AutoTuneSettings = interface(IDispatch)
    ['{85A67E4F-F2F2-3794-8977-C537621C0D83}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Get_MaxCandidateCount: Integer; safecall;
    procedure Set_MaxCandidateCount(pRetVal: Integer); safecall;
    function Get_ParameterSteps: Integer; safecall;
    procedure Set_ParameterSteps(pRetVal: Integer); safecall;
    procedure LockParameter(parameter: LockableParameter; locked: WordBool); safecall;
    function IsParameterLocked(parameter: LockableParameter): WordBool; safecall;
    property ToString: WideString read Get_ToString;
    property MaxCandidateCount: Integer read Get_MaxCandidateCount write Set_MaxCandidateCount;
    property ParameterSteps: Integer read Get_ParameterSteps write Set_ParameterSteps;
  end;

// *********************************************************************//
// DispIntf:  _AutoTuneSettingsDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {85A67E4F-F2F2-3794-8977-C537621C0D83}
// *********************************************************************//
  _AutoTuneSettingsDisp = dispinterface
    ['{85A67E4F-F2F2-3794-8977-C537621C0D83}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    property MaxCandidateCount: Integer dispid 1610743812;
    property ParameterSteps: Integer dispid 1610743814;
    procedure LockParameter(parameter: LockableParameter; locked: WordBool); dispid 1610743816;
    function IsParameterLocked(parameter: LockableParameter): WordBool; dispid 1610743817;
  end;

// *********************************************************************//
// Interface: _SpaceSettings
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {0D712F72-6CAB-3A3C-9321-B6174BE5FB1D}
// *********************************************************************//
  _SpaceSettings = interface(IDispatch)
    ['{0D712F72-6CAB-3A3C-9321-B6174BE5FB1D}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Get_InsertMode: Integer; safecall;
    function Get_ScoreMode: Integer; safecall;
    function Get_SpaceMinWidth: Integer; safecall;
    function Get_SpaceMaxWidth: Integer; safecall;
    property ToString: WideString read Get_ToString;
    property InsertMode: Integer read Get_InsertMode;
    property ScoreMode: Integer read Get_ScoreMode;
    property SpaceMinWidth: Integer read Get_SpaceMinWidth;
    property SpaceMaxWidth: Integer read Get_SpaceMaxWidth;
  end;

// *********************************************************************//
// DispIntf:  _SpaceSettingsDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {0D712F72-6CAB-3A3C-9321-B6174BE5FB1D}
// *********************************************************************//
  _SpaceSettingsDisp = dispinterface
    ['{0D712F72-6CAB-3A3C-9321-B6174BE5FB1D}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    property InsertMode: Integer readonly dispid 1610743812;
    property ScoreMode: Integer readonly dispid 1610743813;
    property SpaceMinWidth: Integer readonly dispid 1610743814;
    property SpaceMaxWidth: Integer readonly dispid 1610743815;
  end;

// *********************************************************************//
// Interface: _AutoSegmentResult
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {97D777CB-4617-3A1C-A0A6-6CAEF31887A3}
// *********************************************************************//
  _AutoSegmentResult = interface(IDispatch)
    ['{97D777CB-4617-3A1C-A0A6-6CAEF31887A3}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Get_index: Integer; safecall;
    function Get_TrainString: WideString; safecall;
    function Get_NumberOfPassingRecords: Integer; safecall;
    function Get_NumberOfFailingRecords: Integer; safecall;
    property ToString: WideString read Get_ToString;
    property index: Integer read Get_index;
    property TrainString: WideString read Get_TrainString;
    property NumberOfPassingRecords: Integer read Get_NumberOfPassingRecords;
    property NumberOfFailingRecords: Integer read Get_NumberOfFailingRecords;
  end;

// *********************************************************************//
// DispIntf:  _AutoSegmentResultDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {97D777CB-4617-3A1C-A0A6-6CAEF31887A3}
// *********************************************************************//
  _AutoSegmentResultDisp = dispinterface
    ['{97D777CB-4617-3A1C-A0A6-6CAEF31887A3}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    property index: Integer readonly dispid 1610743812;
    property TrainString: WideString readonly dispid 1610743813;
    property NumberOfPassingRecords: Integer readonly dispid 1610743814;
    property NumberOfFailingRecords: Integer readonly dispid 1610743815;
  end;

// *********************************************************************//
// Interface: _AutoTuneRecord
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {8CEFEDD1-0325-32E6-8CAA-C44547907B05}
// *********************************************************************//
  _AutoTuneRecord = interface(IDispatch)
    ['{8CEFEDD1-0325-32E6-8CAA-C44547907B05}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Get_Id: Integer; safecall;
    function Get_RecordVerified: WordBool; safecall;
    function Get_CharactersVerified: WordBool; safecall;
    function Get_TrainString: WideString; safecall;
    function Get_TrainedImage: IUnknown; safecall;
    function Get_TrainedCharacterRegions: PSafeArray; safecall;
    function Get_CurrentCharacterRegions: PSafeArray; safecall;
    property ToString: WideString read Get_ToString;
    property Id: Integer read Get_Id;
    property RecordVerified: WordBool read Get_RecordVerified;
    property CharactersVerified: WordBool read Get_CharactersVerified;
    property TrainString: WideString read Get_TrainString;
    property TrainedImage: IUnknown read Get_TrainedImage;
    property TrainedCharacterRegions: PSafeArray read Get_TrainedCharacterRegions;
    property CurrentCharacterRegions: PSafeArray read Get_CurrentCharacterRegions;
  end;

// *********************************************************************//
// DispIntf:  _AutoTuneRecordDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {8CEFEDD1-0325-32E6-8CAA-C44547907B05}
// *********************************************************************//
  _AutoTuneRecordDisp = dispinterface
    ['{8CEFEDD1-0325-32E6-8CAA-C44547907B05}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    property Id: Integer readonly dispid 1610743812;
    property RecordVerified: WordBool readonly dispid 1610743813;
    property CharactersVerified: WordBool readonly dispid 1610743814;
    property TrainString: WideString readonly dispid 1610743815;
    property TrainedImage: IUnknown readonly dispid 1610743816;
    property TrainedCharacterRegions: {??PSafeArray}OleVariant readonly dispid 1610743817;
    property CurrentCharacterRegions: {??PSafeArray}OleVariant readonly dispid 1610743818;
  end;

// *********************************************************************//
// Interface: _AutoTuneException
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {84E8E4D9-2902-36D9-A9FC-6E984867DAC7}
// *********************************************************************//
  _AutoTuneException = interface(IDispatch)
    ['{84E8E4D9-2902-36D9-A9FC-6E984867DAC7}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Get_data: IDictionary; safecall;
    procedure GetObjectData(const info: _SerializationInfo; context: StreamingContext); safecall;
    function GetType_2: _Type; safecall;
    function Get_Message: WideString; safecall;
    function GetBaseException: _Exception; safecall;
    function Get_StackTrace: WideString; safecall;
    function Get_HelpLink: WideString; safecall;
    procedure Set_HelpLink(const pRetVal: WideString); safecall;
    function Get_source: WideString; safecall;
    procedure Set_source(const pRetVal: WideString); safecall;
    function Get_InnerException: _Exception; safecall;
    function Get_TargetSite: _MethodBase; safecall;
    function Get_HResult: Integer; safecall;
    function Get_ErrorNumber: CvsInSightError; safecall;
    function Get_Code: Integer; safecall;
    property ToString: WideString read Get_ToString;
    property data: IDictionary read Get_data;
    property Message: WideString read Get_Message;
    property StackTrace: WideString read Get_StackTrace;
    property HelpLink: WideString read Get_HelpLink write Set_HelpLink;
    property source: WideString read Get_source write Set_source;
    property InnerException: _Exception read Get_InnerException;
    property TargetSite: _MethodBase read Get_TargetSite;
    property HResult: Integer read Get_HResult;
    property ErrorNumber: CvsInSightError read Get_ErrorNumber;
    property Code: Integer read Get_Code;
  end;

// *********************************************************************//
// DispIntf:  _AutoTuneExceptionDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {84E8E4D9-2902-36D9-A9FC-6E984867DAC7}
// *********************************************************************//
  _AutoTuneExceptionDisp = dispinterface
    ['{84E8E4D9-2902-36D9-A9FC-6E984867DAC7}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    property data: IDictionary readonly dispid 1610743812;
    procedure GetObjectData(const info: _SerializationInfo; context: {??StreamingContext}OleVariant); dispid 1610743813;
    function GetType_2: _Type; dispid 1610743814;
    property Message: WideString readonly dispid 1610743815;
    function GetBaseException: _Exception; dispid 1610743816;
    property StackTrace: WideString readonly dispid 1610743817;
    property HelpLink: WideString dispid 1610743818;
    property source: WideString dispid 1610743820;
    property InnerException: _Exception readonly dispid 1610743822;
    property TargetSite: _MethodBase readonly dispid 1610743823;
    property HResult: Integer readonly dispid 1610743824;
    property ErrorNumber: CvsInSightError readonly dispid 1610743825;
    property Code: Integer readonly dispid 1610743826;
  end;

// *********************************************************************//
// Interface: _AutoTuneSessionClosedEventArgs
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {12C8989E-3B38-3E66-B5DA-74933E79899B}
// *********************************************************************//
  _AutoTuneSessionClosedEventArgs = interface(IDispatch)
    ['{12C8989E-3B38-3E66-B5DA-74933E79899B}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Get_SegmentationChangesCommitted: WordBool; safecall;
    function Get_FontChangesCommitted: WordBool; safecall;
    property ToString: WideString read Get_ToString;
    property SegmentationChangesCommitted: WordBool read Get_SegmentationChangesCommitted;
    property FontChangesCommitted: WordBool read Get_FontChangesCommitted;
  end;

// *********************************************************************//
// DispIntf:  _AutoTuneSessionClosedEventArgsDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {12C8989E-3B38-3E66-B5DA-74933E79899B}
// *********************************************************************//
  _AutoTuneSessionClosedEventArgsDisp = dispinterface
    ['{12C8989E-3B38-3E66-B5DA-74933E79899B}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    property SegmentationChangesCommitted: WordBool readonly dispid 1610743812;
    property FontChangesCommitted: WordBool readonly dispid 1610743813;
  end;

// *********************************************************************//
// Interface: _AutoTuneSessionClosedHandler
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {F3ADDE24-E39C-35F6-B5BE-7C8F5134AF22}
// *********************************************************************//
  _AutoTuneSessionClosedHandler = interface(IDispatch)
    ['{F3ADDE24-E39C-35F6-B5BE-7C8F5134AF22}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function GetInvocationList: PSafeArray; safecall;
    function Clone: OleVariant; safecall;
    procedure GetObjectData(const info: _SerializationInfo; context: StreamingContext); safecall;
    function DynamicInvoke(args: PSafeArray): OleVariant; safecall;
    function Get_Method: _MethodInfo; safecall;
    function Get_Target: OleVariant; safecall;
    procedure Invoke_2(sender: OleVariant; const e: _AutoTuneSessionClosedEventArgs); safecall;
    function BeginInvoke(sender: OleVariant; const e: _AutoTuneSessionClosedEventArgs; 
                         const callback: _AsyncCallback; object_: OleVariant): IAsyncResult; safecall;
    procedure EndInvoke(const result: IAsyncResult); safecall;
    property ToString: WideString read Get_ToString;
    property Method: _MethodInfo read Get_Method;
    property Target: OleVariant read Get_Target;
  end;

// *********************************************************************//
// DispIntf:  _AutoTuneSessionClosedHandlerDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {F3ADDE24-E39C-35F6-B5BE-7C8F5134AF22}
// *********************************************************************//
  _AutoTuneSessionClosedHandlerDisp = dispinterface
    ['{F3ADDE24-E39C-35F6-B5BE-7C8F5134AF22}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    function GetInvocationList: {??PSafeArray}OleVariant; dispid 1610743812;
    function Clone: OleVariant; dispid 1610743813;
    procedure GetObjectData(const info: _SerializationInfo; context: {??StreamingContext}OleVariant); dispid 1610743814;
    function DynamicInvoke(args: {??PSafeArray}OleVariant): OleVariant; dispid 1610743815;
    property Method: _MethodInfo readonly dispid 1610743816;
    property Target: OleVariant readonly dispid 1610743817;
    procedure Invoke_2(sender: OleVariant; const e: _AutoTuneSessionClosedEventArgs); dispid 1610743818;
    function BeginInvoke(sender: OleVariant; const e: _AutoTuneSessionClosedEventArgs; 
                         const callback: _AsyncCallback; object_: OleVariant): IAsyncResult; dispid 1610743819;
    procedure EndInvoke(const result: IAsyncResult); dispid 1610743820;
  end;

// *********************************************************************//
// Interface: _CvsCellButton
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {2BF38D3C-906F-396B-8C35-E4C86EBD4A72}
// *********************************************************************//
  _CvsCellButton = interface(IDispatch)
    ['{2BF38D3C-906F-396B-8C35-E4C86EBD4A72}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Get_Text: WideString; safecall;
    function Get_IsEditCell: WordBool; safecall;
    function Get_SupportsAuthorization: WordBool; safecall;
    function Get_DataType: CvsCellDataType; safecall;
    function Get_ErrorString: WideString; safecall;
    function Get_Error: WordBool; safecall;
    function Get_Control: WordBool; safecall;
    function Get_HasChecksum: WordBool; safecall;
    function Get_Hidden: WordBool; safecall;
    function Get_ProtectionState: CvsCellIPProtection; safecall;
    function Get_Enabled: WordBool; safecall;
    function Get_Location: CvsCellLocation; safecall;
    function Get_Expression: WideString; safecall;
    function Get_SymbolicTag: WideString; safecall;
    function Get_OpcTag: WideString; safecall;
    function Get_CellStateReference: WideString; safecall;
    function Get_Properties: CvsCellProperty; safecall;
    function Get_DataTypeString: WideString; safecall;
    property ToString: WideString read Get_ToString;
    property Text: WideString read Get_Text;
    property IsEditCell: WordBool read Get_IsEditCell;
    property SupportsAuthorization: WordBool read Get_SupportsAuthorization;
    property DataType: CvsCellDataType read Get_DataType;
    property ErrorString: WideString read Get_ErrorString;
    property Error: WordBool read Get_Error;
    property Control: WordBool read Get_Control;
    property HasChecksum: WordBool read Get_HasChecksum;
    property Hidden: WordBool read Get_Hidden;
    property ProtectionState: CvsCellIPProtection read Get_ProtectionState;
    property Enabled: WordBool read Get_Enabled;
    property Location: CvsCellLocation read Get_Location;
    property Expression: WideString read Get_Expression;
    property SymbolicTag: WideString read Get_SymbolicTag;
    property OpcTag: WideString read Get_OpcTag;
    property CellStateReference: WideString read Get_CellStateReference;
    property Properties: CvsCellProperty read Get_Properties;
    property DataTypeString: WideString read Get_DataTypeString;
  end;

// *********************************************************************//
// DispIntf:  _CvsCellButtonDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {2BF38D3C-906F-396B-8C35-E4C86EBD4A72}
// *********************************************************************//
  _CvsCellButtonDisp = dispinterface
    ['{2BF38D3C-906F-396B-8C35-E4C86EBD4A72}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    property Text: WideString readonly dispid 1610743812;
    property IsEditCell: WordBool readonly dispid 1610743813;
    property SupportsAuthorization: WordBool readonly dispid 1610743814;
    property DataType: CvsCellDataType readonly dispid 1610743815;
    property ErrorString: WideString readonly dispid 1610743816;
    property Error: WordBool readonly dispid 1610743817;
    property Control: WordBool readonly dispid 1610743818;
    property HasChecksum: WordBool readonly dispid 1610743819;
    property Hidden: WordBool readonly dispid 1610743820;
    property ProtectionState: CvsCellIPProtection readonly dispid 1610743821;
    property Enabled: WordBool readonly dispid 1610743822;
    property Location: {??CvsCellLocation}OleVariant readonly dispid 1610743823;
    property Expression: WideString readonly dispid 1610743824;
    property SymbolicTag: WideString readonly dispid 1610743825;
    property OpcTag: WideString readonly dispid 1610743826;
    property CellStateReference: WideString readonly dispid 1610743827;
    property Properties: CvsCellProperty readonly dispid 1610743828;
    property DataTypeString: WideString readonly dispid 1610743829;
  end;

// *********************************************************************//
// Interface: _CvsCellChart
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {DD5130E6-8827-3309-8778-2190BC212DCD}
// *********************************************************************//
  _CvsCellChart = interface(IDispatch)
    ['{DD5130E6-8827-3309-8778-2190BC212DCD}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Get_Text: WideString; safecall;
    function Get_IsEditCell: WordBool; safecall;
    function Get_SupportsAuthorization: WordBool; safecall;
    function Get_DataType: CvsCellDataType; safecall;
    function Get_ErrorString: WideString; safecall;
    function Get_Error: WordBool; safecall;
    function Get_Control: WordBool; safecall;
    function Get_HasChecksum: WordBool; safecall;
    function Get_Hidden: WordBool; safecall;
    function Get_ProtectionState: CvsCellIPProtection; safecall;
    function Get_Enabled: WordBool; safecall;
    function Get_Location: CvsCellLocation; safecall;
    function Get_Expression: WideString; safecall;
    function Get_SymbolicTag: WideString; safecall;
    function Get_OpcTag: WideString; safecall;
    function Get_CellStateReference: WideString; safecall;
    function Get_Properties: CvsCellProperty; safecall;
    function Get_DataTypeString: WideString; safecall;
    function Get_NumPoints: Integer; safecall;
    function Get_MaxPoints: Integer; safecall;
    function Get_MinValue: Double; safecall;
    function Get_MaxValue: Double; safecall;
    function GetData: PSafeArray; safecall;
    property ToString: WideString read Get_ToString;
    property Text: WideString read Get_Text;
    property IsEditCell: WordBool read Get_IsEditCell;
    property SupportsAuthorization: WordBool read Get_SupportsAuthorization;
    property DataType: CvsCellDataType read Get_DataType;
    property ErrorString: WideString read Get_ErrorString;
    property Error: WordBool read Get_Error;
    property Control: WordBool read Get_Control;
    property HasChecksum: WordBool read Get_HasChecksum;
    property Hidden: WordBool read Get_Hidden;
    property ProtectionState: CvsCellIPProtection read Get_ProtectionState;
    property Enabled: WordBool read Get_Enabled;
    property Location: CvsCellLocation read Get_Location;
    property Expression: WideString read Get_Expression;
    property SymbolicTag: WideString read Get_SymbolicTag;
    property OpcTag: WideString read Get_OpcTag;
    property CellStateReference: WideString read Get_CellStateReference;
    property Properties: CvsCellProperty read Get_Properties;
    property DataTypeString: WideString read Get_DataTypeString;
    property NumPoints: Integer read Get_NumPoints;
    property MaxPoints: Integer read Get_MaxPoints;
    property MinValue: Double read Get_MinValue;
    property MaxValue: Double read Get_MaxValue;
  end;

// *********************************************************************//
// DispIntf:  _CvsCellChartDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {DD5130E6-8827-3309-8778-2190BC212DCD}
// *********************************************************************//
  _CvsCellChartDisp = dispinterface
    ['{DD5130E6-8827-3309-8778-2190BC212DCD}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    property Text: WideString readonly dispid 1610743812;
    property IsEditCell: WordBool readonly dispid 1610743813;
    property SupportsAuthorization: WordBool readonly dispid 1610743814;
    property DataType: CvsCellDataType readonly dispid 1610743815;
    property ErrorString: WideString readonly dispid 1610743816;
    property Error: WordBool readonly dispid 1610743817;
    property Control: WordBool readonly dispid 1610743818;
    property HasChecksum: WordBool readonly dispid 1610743819;
    property Hidden: WordBool readonly dispid 1610743820;
    property ProtectionState: CvsCellIPProtection readonly dispid 1610743821;
    property Enabled: WordBool readonly dispid 1610743822;
    property Location: {??CvsCellLocation}OleVariant readonly dispid 1610743823;
    property Expression: WideString readonly dispid 1610743824;
    property SymbolicTag: WideString readonly dispid 1610743825;
    property OpcTag: WideString readonly dispid 1610743826;
    property CellStateReference: WideString readonly dispid 1610743827;
    property Properties: CvsCellProperty readonly dispid 1610743828;
    property DataTypeString: WideString readonly dispid 1610743829;
    property NumPoints: Integer readonly dispid 1610743830;
    property MaxPoints: Integer readonly dispid 1610743831;
    property MinValue: Double readonly dispid 1610743832;
    property MaxValue: Double readonly dispid 1610743833;
    function GetData: {??PSafeArray}OleVariant; dispid 1610743834;
  end;

// *********************************************************************//
// Interface: _CvsCellCheckBox
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {D2BA701D-CFEF-3DA4-826C-361DB52EFC97}
// *********************************************************************//
  _CvsCellCheckBox = interface(IDispatch)
    ['{D2BA701D-CFEF-3DA4-826C-361DB52EFC97}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Get_Text: WideString; safecall;
    function Get_IsEditCell: WordBool; safecall;
    function Get_SupportsAuthorization: WordBool; safecall;
    function Get_DataType: CvsCellDataType; safecall;
    function Get_ErrorString: WideString; safecall;
    function Get_Error: WordBool; safecall;
    function Get_Control: WordBool; safecall;
    function Get_HasChecksum: WordBool; safecall;
    function Get_Hidden: WordBool; safecall;
    function Get_ProtectionState: CvsCellIPProtection; safecall;
    function Get_Enabled: WordBool; safecall;
    function Get_Location: CvsCellLocation; safecall;
    function Get_Expression: WideString; safecall;
    function Get_SymbolicTag: WideString; safecall;
    function Get_OpcTag: WideString; safecall;
    function Get_CellStateReference: WideString; safecall;
    function Get_Properties: CvsCellProperty; safecall;
    function Get_DataTypeString: WideString; safecall;
    function Get_Checked: WordBool; safecall;
    property ToString: WideString read Get_ToString;
    property Text: WideString read Get_Text;
    property IsEditCell: WordBool read Get_IsEditCell;
    property SupportsAuthorization: WordBool read Get_SupportsAuthorization;
    property DataType: CvsCellDataType read Get_DataType;
    property ErrorString: WideString read Get_ErrorString;
    property Error: WordBool read Get_Error;
    property Control: WordBool read Get_Control;
    property HasChecksum: WordBool read Get_HasChecksum;
    property Hidden: WordBool read Get_Hidden;
    property ProtectionState: CvsCellIPProtection read Get_ProtectionState;
    property Enabled: WordBool read Get_Enabled;
    property Location: CvsCellLocation read Get_Location;
    property Expression: WideString read Get_Expression;
    property SymbolicTag: WideString read Get_SymbolicTag;
    property OpcTag: WideString read Get_OpcTag;
    property CellStateReference: WideString read Get_CellStateReference;
    property Properties: CvsCellProperty read Get_Properties;
    property DataTypeString: WideString read Get_DataTypeString;
    property Checked: WordBool read Get_Checked;
  end;

// *********************************************************************//
// DispIntf:  _CvsCellCheckBoxDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {D2BA701D-CFEF-3DA4-826C-361DB52EFC97}
// *********************************************************************//
  _CvsCellCheckBoxDisp = dispinterface
    ['{D2BA701D-CFEF-3DA4-826C-361DB52EFC97}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    property Text: WideString readonly dispid 1610743812;
    property IsEditCell: WordBool readonly dispid 1610743813;
    property SupportsAuthorization: WordBool readonly dispid 1610743814;
    property DataType: CvsCellDataType readonly dispid 1610743815;
    property ErrorString: WideString readonly dispid 1610743816;
    property Error: WordBool readonly dispid 1610743817;
    property Control: WordBool readonly dispid 1610743818;
    property HasChecksum: WordBool readonly dispid 1610743819;
    property Hidden: WordBool readonly dispid 1610743820;
    property ProtectionState: CvsCellIPProtection readonly dispid 1610743821;
    property Enabled: WordBool readonly dispid 1610743822;
    property Location: {??CvsCellLocation}OleVariant readonly dispid 1610743823;
    property Expression: WideString readonly dispid 1610743824;
    property SymbolicTag: WideString readonly dispid 1610743825;
    property OpcTag: WideString readonly dispid 1610743826;
    property CellStateReference: WideString readonly dispid 1610743827;
    property Properties: CvsCellProperty readonly dispid 1610743828;
    property DataTypeString: WideString readonly dispid 1610743829;
    property Checked: WordBool readonly dispid 1610743830;
  end;

// *********************************************************************//
// Interface: _CvsCellCollection
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {FA7CA735-559A-3658-AF28-97D7E502C6FD}
// *********************************************************************//
  _CvsCellCollection = interface(IDispatch)
    ['{FA7CA735-559A-3658-AF28-97D7E502C6FD}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Get_Count: Integer; safecall;
    function IndexOf(Location: CvsCellLocation): Integer; safecall;
    function Get_Item(const Cell: WideString): _CvsCell; safecall;
    function Get_Item_2(index: Integer): _CvsCell; safecall;
    function GetCell(row: Integer; column: Integer): _CvsCell; safecall;
    function GetCell_2(sheetIndex: Integer; row: Integer; column: Integer): _CvsCell; safecall;
    function Get_Items: PSafeArray; safecall;
    function GetCells(row: Integer; column: Integer; rows: Integer; columns: Integer): _CvsCellCollection; safecall;
    property ToString: WideString read Get_ToString;
    property Count: Integer read Get_Count;
    property Item[const Cell: WideString]: _CvsCell read Get_Item; default;
    property Item_2[index: Integer]: _CvsCell read Get_Item_2;
    property Items: PSafeArray read Get_Items;
  end;

// *********************************************************************//
// DispIntf:  _CvsCellCollectionDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {FA7CA735-559A-3658-AF28-97D7E502C6FD}
// *********************************************************************//
  _CvsCellCollectionDisp = dispinterface
    ['{FA7CA735-559A-3658-AF28-97D7E502C6FD}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 1610743810;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    property Count: Integer readonly dispid 1610743812;
    function IndexOf(Location: {??CvsCellLocation}OleVariant): Integer; dispid 1610743813;
    property Item[const Cell: WideString]: _CvsCell readonly dispid 0; default;
    property Item_2[index: Integer]: _CvsCell readonly dispid 1610743815;
    function GetCell(row: Integer; column: Integer): _CvsCell; dispid 1610743816;
    function GetCell_2(sheetIndex: Integer; row: Integer; column: Integer): _CvsCell; dispid 1610743817;
    property Items: {??PSafeArray}OleVariant readonly dispid 1610743818;
    function GetCells(row: Integer; column: Integer; rows: Integer; columns: Integer): _CvsCellCollection; dispid 1610743819;
  end;

// *********************************************************************//
// Interface: _CvsCellColorLabel
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {267B9840-E087-3A43-9D74-6BEC618BE35E}
// *********************************************************************//
  _CvsCellColorLabel = interface(IDispatch)
    ['{267B9840-E087-3A43-9D74-6BEC618BE35E}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Get_Text: WideString; safecall;
    function Get_IsEditCell: WordBool; safecall;
    function Get_SupportsAuthorization: WordBool; safecall;
    function Get_DataType: CvsCellDataType; safecall;
    function Get_ErrorString: WideString; safecall;
    function Get_Error: WordBool; safecall;
    function Get_Control: WordBool; safecall;
    function Get_HasChecksum: WordBool; safecall;
    function Get_Hidden: WordBool; safecall;
    function Get_ProtectionState: CvsCellIPProtection; safecall;
    function Get_Enabled: WordBool; safecall;
    function Get_Location: CvsCellLocation; safecall;
    function Get_Expression: WideString; safecall;
    function Get_SymbolicTag: WideString; safecall;
    function Get_OpcTag: WideString; safecall;
    function Get_CellStateReference: WideString; safecall;
    function Get_Properties: CvsCellProperty; safecall;
    function Get_DataTypeString: WideString; safecall;
    function Get_ForeColorAsRgb: Integer; safecall;
    function Get_BackColorAsRgb: Integer; safecall;
    property ToString: WideString read Get_ToString;
    property Text: WideString read Get_Text;
    property IsEditCell: WordBool read Get_IsEditCell;
    property SupportsAuthorization: WordBool read Get_SupportsAuthorization;
    property DataType: CvsCellDataType read Get_DataType;
    property ErrorString: WideString read Get_ErrorString;
    property Error: WordBool read Get_Error;
    property Control: WordBool read Get_Control;
    property HasChecksum: WordBool read Get_HasChecksum;
    property Hidden: WordBool read Get_Hidden;
    property ProtectionState: CvsCellIPProtection read Get_ProtectionState;
    property Enabled: WordBool read Get_Enabled;
    property Location: CvsCellLocation read Get_Location;
    property Expression: WideString read Get_Expression;
    property SymbolicTag: WideString read Get_SymbolicTag;
    property OpcTag: WideString read Get_OpcTag;
    property CellStateReference: WideString read Get_CellStateReference;
    property Properties: CvsCellProperty read Get_Properties;
    property DataTypeString: WideString read Get_DataTypeString;
    property ForeColorAsRgb: Integer read Get_ForeColorAsRgb;
    property BackColorAsRgb: Integer read Get_BackColorAsRgb;
  end;

// *********************************************************************//
// DispIntf:  _CvsCellColorLabelDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {267B9840-E087-3A43-9D74-6BEC618BE35E}
// *********************************************************************//
  _CvsCellColorLabelDisp = dispinterface
    ['{267B9840-E087-3A43-9D74-6BEC618BE35E}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    property Text: WideString readonly dispid 1610743812;
    property IsEditCell: WordBool readonly dispid 1610743813;
    property SupportsAuthorization: WordBool readonly dispid 1610743814;
    property DataType: CvsCellDataType readonly dispid 1610743815;
    property ErrorString: WideString readonly dispid 1610743816;
    property Error: WordBool readonly dispid 1610743817;
    property Control: WordBool readonly dispid 1610743818;
    property HasChecksum: WordBool readonly dispid 1610743819;
    property Hidden: WordBool readonly dispid 1610743820;
    property ProtectionState: CvsCellIPProtection readonly dispid 1610743821;
    property Enabled: WordBool readonly dispid 1610743822;
    property Location: {??CvsCellLocation}OleVariant readonly dispid 1610743823;
    property Expression: WideString readonly dispid 1610743824;
    property SymbolicTag: WideString readonly dispid 1610743825;
    property OpcTag: WideString readonly dispid 1610743826;
    property CellStateReference: WideString readonly dispid 1610743827;
    property Properties: CvsCellProperty readonly dispid 1610743828;
    property DataTypeString: WideString readonly dispid 1610743829;
    property ForeColorAsRgb: Integer readonly dispid 1610743830;
    property BackColorAsRgb: Integer readonly dispid 1610743831;
  end;

// *********************************************************************//
// Interface: _CvsCellDialog
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {35A88803-F5E3-3EA4-95B5-D5BE73DE8898}
// *********************************************************************//
  _CvsCellDialog = interface(IDispatch)
    ['{35A88803-F5E3-3EA4-95B5-D5BE73DE8898}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Get_Text: WideString; safecall;
    function Get_IsEditCell: WordBool; safecall;
    function Get_SupportsAuthorization: WordBool; safecall;
    function Get_DataType: CvsCellDataType; safecall;
    function Get_ErrorString: WideString; safecall;
    function Get_Error: WordBool; safecall;
    function Get_Control: WordBool; safecall;
    function Get_HasChecksum: WordBool; safecall;
    function Get_Hidden: WordBool; safecall;
    function Get_ProtectionState: CvsCellIPProtection; safecall;
    function Get_Enabled: WordBool; safecall;
    function Get_Location: CvsCellLocation; safecall;
    function Get_Expression: WideString; safecall;
    function Get_SymbolicTag: WideString; safecall;
    function Get_OpcTag: WideString; safecall;
    function Get_CellStateReference: WideString; safecall;
    function Get_Properties: CvsCellProperty; safecall;
    function Get_DataTypeString: WideString; safecall;
    function Get_DialogRange: CvsCellRange; safecall;
    property ToString: WideString read Get_ToString;
    property Text: WideString read Get_Text;
    property IsEditCell: WordBool read Get_IsEditCell;
    property SupportsAuthorization: WordBool read Get_SupportsAuthorization;
    property DataType: CvsCellDataType read Get_DataType;
    property ErrorString: WideString read Get_ErrorString;
    property Error: WordBool read Get_Error;
    property Control: WordBool read Get_Control;
    property HasChecksum: WordBool read Get_HasChecksum;
    property Hidden: WordBool read Get_Hidden;
    property ProtectionState: CvsCellIPProtection read Get_ProtectionState;
    property Enabled: WordBool read Get_Enabled;
    property Location: CvsCellLocation read Get_Location;
    property Expression: WideString read Get_Expression;
    property SymbolicTag: WideString read Get_SymbolicTag;
    property OpcTag: WideString read Get_OpcTag;
    property CellStateReference: WideString read Get_CellStateReference;
    property Properties: CvsCellProperty read Get_Properties;
    property DataTypeString: WideString read Get_DataTypeString;
    property DialogRange: CvsCellRange read Get_DialogRange;
  end;

// *********************************************************************//
// DispIntf:  _CvsCellDialogDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {35A88803-F5E3-3EA4-95B5-D5BE73DE8898}
// *********************************************************************//
  _CvsCellDialogDisp = dispinterface
    ['{35A88803-F5E3-3EA4-95B5-D5BE73DE8898}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    property Text: WideString readonly dispid 1610743812;
    property IsEditCell: WordBool readonly dispid 1610743813;
    property SupportsAuthorization: WordBool readonly dispid 1610743814;
    property DataType: CvsCellDataType readonly dispid 1610743815;
    property ErrorString: WideString readonly dispid 1610743816;
    property Error: WordBool readonly dispid 1610743817;
    property Control: WordBool readonly dispid 1610743818;
    property HasChecksum: WordBool readonly dispid 1610743819;
    property Hidden: WordBool readonly dispid 1610743820;
    property ProtectionState: CvsCellIPProtection readonly dispid 1610743821;
    property Enabled: WordBool readonly dispid 1610743822;
    property Location: {??CvsCellLocation}OleVariant readonly dispid 1610743823;
    property Expression: WideString readonly dispid 1610743824;
    property SymbolicTag: WideString readonly dispid 1610743825;
    property OpcTag: WideString readonly dispid 1610743826;
    property CellStateReference: WideString readonly dispid 1610743827;
    property Properties: CvsCellProperty readonly dispid 1610743828;
    property DataTypeString: WideString readonly dispid 1610743829;
    property DialogRange: {??CvsCellRange}OleVariant readonly dispid 1610743830;
  end;

// *********************************************************************//
// Interface: _CvsCellEditCircle
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {3B8B571F-FE63-352B-8BD5-FC203F3D659B}
// *********************************************************************//
  _CvsCellEditCircle = interface(IDispatch)
    ['{3B8B571F-FE63-352B-8BD5-FC203F3D659B}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Get_Text: WideString; safecall;
    function Get_IsEditCell: WordBool; safecall;
    function Get_SupportsAuthorization: WordBool; safecall;
    function Get_DataType: CvsCellDataType; safecall;
    function Get_ErrorString: WideString; safecall;
    function Get_Error: WordBool; safecall;
    function Get_Control: WordBool; safecall;
    function Get_HasChecksum: WordBool; safecall;
    function Get_Hidden: WordBool; safecall;
    function Get_ProtectionState: CvsCellIPProtection; safecall;
    function Get_Enabled: WordBool; safecall;
    function Get_Location: CvsCellLocation; safecall;
    function Get_Expression: WideString; safecall;
    function Get_SymbolicTag: WideString; safecall;
    function Get_OpcTag: WideString; safecall;
    function Get_CellStateReference: WideString; safecall;
    function Get_Properties: CvsCellProperty; safecall;
    function Get_DataTypeString: WideString; safecall;
    function Get_EditFlags: CvsEditFlags; safecall;
    function Get_Center: CvsImageLocation; safecall;
    function Get_Radius: Single; safecall;
    property ToString: WideString read Get_ToString;
    property Text: WideString read Get_Text;
    property IsEditCell: WordBool read Get_IsEditCell;
    property SupportsAuthorization: WordBool read Get_SupportsAuthorization;
    property DataType: CvsCellDataType read Get_DataType;
    property ErrorString: WideString read Get_ErrorString;
    property Error: WordBool read Get_Error;
    property Control: WordBool read Get_Control;
    property HasChecksum: WordBool read Get_HasChecksum;
    property Hidden: WordBool read Get_Hidden;
    property ProtectionState: CvsCellIPProtection read Get_ProtectionState;
    property Enabled: WordBool read Get_Enabled;
    property Location: CvsCellLocation read Get_Location;
    property Expression: WideString read Get_Expression;
    property SymbolicTag: WideString read Get_SymbolicTag;
    property OpcTag: WideString read Get_OpcTag;
    property CellStateReference: WideString read Get_CellStateReference;
    property Properties: CvsCellProperty read Get_Properties;
    property DataTypeString: WideString read Get_DataTypeString;
    property EditFlags: CvsEditFlags read Get_EditFlags;
    property Center: CvsImageLocation read Get_Center;
    property Radius: Single read Get_Radius;
  end;

// *********************************************************************//
// DispIntf:  _CvsCellEditCircleDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {3B8B571F-FE63-352B-8BD5-FC203F3D659B}
// *********************************************************************//
  _CvsCellEditCircleDisp = dispinterface
    ['{3B8B571F-FE63-352B-8BD5-FC203F3D659B}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    property Text: WideString readonly dispid 1610743812;
    property IsEditCell: WordBool readonly dispid 1610743813;
    property SupportsAuthorization: WordBool readonly dispid 1610743814;
    property DataType: CvsCellDataType readonly dispid 1610743815;
    property ErrorString: WideString readonly dispid 1610743816;
    property Error: WordBool readonly dispid 1610743817;
    property Control: WordBool readonly dispid 1610743818;
    property HasChecksum: WordBool readonly dispid 1610743819;
    property Hidden: WordBool readonly dispid 1610743820;
    property ProtectionState: CvsCellIPProtection readonly dispid 1610743821;
    property Enabled: WordBool readonly dispid 1610743822;
    property Location: {??CvsCellLocation}OleVariant readonly dispid 1610743823;
    property Expression: WideString readonly dispid 1610743824;
    property SymbolicTag: WideString readonly dispid 1610743825;
    property OpcTag: WideString readonly dispid 1610743826;
    property CellStateReference: WideString readonly dispid 1610743827;
    property Properties: CvsCellProperty readonly dispid 1610743828;
    property DataTypeString: WideString readonly dispid 1610743829;
    property EditFlags: CvsEditFlags readonly dispid 1610743830;
    property Center: {??CvsImageLocation}OleVariant readonly dispid 1610743831;
    property Radius: Single readonly dispid 1610743832;
  end;

// *********************************************************************//
// Interface: _CvsCellEditFloat
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {F703BE3C-1696-32AB-B178-AF83E783700D}
// *********************************************************************//
  _CvsCellEditFloat = interface(IDispatch)
    ['{F703BE3C-1696-32AB-B178-AF83E783700D}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Get_Text: WideString; safecall;
    function Get_IsEditCell: WordBool; safecall;
    function Get_SupportsAuthorization: WordBool; safecall;
    function Get_DataType: CvsCellDataType; safecall;
    function Get_ErrorString: WideString; safecall;
    function Get_Error: WordBool; safecall;
    function Get_Control: WordBool; safecall;
    function Get_HasChecksum: WordBool; safecall;
    function Get_Hidden: WordBool; safecall;
    function Get_ProtectionState: CvsCellIPProtection; safecall;
    function Get_Enabled: WordBool; safecall;
    function Get_Location: CvsCellLocation; safecall;
    function Get_Expression: WideString; safecall;
    function Get_SymbolicTag: WideString; safecall;
    function Get_OpcTag: WideString; safecall;
    function Get_CellStateReference: WideString; safecall;
    function Get_Properties: CvsCellProperty; safecall;
    function Get_DataTypeString: WideString; safecall;
    function Get_value: Single; safecall;
    function Get_MinValue: Single; safecall;
    function Get_MaxValue: Single; safecall;
    property ToString: WideString read Get_ToString;
    property Text: WideString read Get_Text;
    property IsEditCell: WordBool read Get_IsEditCell;
    property SupportsAuthorization: WordBool read Get_SupportsAuthorization;
    property DataType: CvsCellDataType read Get_DataType;
    property ErrorString: WideString read Get_ErrorString;
    property Error: WordBool read Get_Error;
    property Control: WordBool read Get_Control;
    property HasChecksum: WordBool read Get_HasChecksum;
    property Hidden: WordBool read Get_Hidden;
    property ProtectionState: CvsCellIPProtection read Get_ProtectionState;
    property Enabled: WordBool read Get_Enabled;
    property Location: CvsCellLocation read Get_Location;
    property Expression: WideString read Get_Expression;
    property SymbolicTag: WideString read Get_SymbolicTag;
    property OpcTag: WideString read Get_OpcTag;
    property CellStateReference: WideString read Get_CellStateReference;
    property Properties: CvsCellProperty read Get_Properties;
    property DataTypeString: WideString read Get_DataTypeString;
    property value: Single read Get_value;
    property MinValue: Single read Get_MinValue;
    property MaxValue: Single read Get_MaxValue;
  end;

// *********************************************************************//
// DispIntf:  _CvsCellEditFloatDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {F703BE3C-1696-32AB-B178-AF83E783700D}
// *********************************************************************//
  _CvsCellEditFloatDisp = dispinterface
    ['{F703BE3C-1696-32AB-B178-AF83E783700D}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 1610743810;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    property Text: WideString readonly dispid 1610743812;
    property IsEditCell: WordBool readonly dispid 1610743813;
    property SupportsAuthorization: WordBool readonly dispid 1610743814;
    property DataType: CvsCellDataType readonly dispid 1610743815;
    property ErrorString: WideString readonly dispid 1610743816;
    property Error: WordBool readonly dispid 1610743817;
    property Control: WordBool readonly dispid 1610743818;
    property HasChecksum: WordBool readonly dispid 1610743819;
    property Hidden: WordBool readonly dispid 1610743820;
    property ProtectionState: CvsCellIPProtection readonly dispid 1610743821;
    property Enabled: WordBool readonly dispid 1610743822;
    property Location: {??CvsCellLocation}OleVariant readonly dispid 1610743823;
    property Expression: WideString readonly dispid 1610743824;
    property SymbolicTag: WideString readonly dispid 1610743825;
    property OpcTag: WideString readonly dispid 1610743826;
    property CellStateReference: WideString readonly dispid 1610743827;
    property Properties: CvsCellProperty readonly dispid 1610743828;
    property DataTypeString: WideString readonly dispid 1610743829;
    property value: Single readonly dispid 0;
    property MinValue: Single readonly dispid 1610743831;
    property MaxValue: Single readonly dispid 1610743832;
  end;

// *********************************************************************//
// Interface: _CvsCellEditInt
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {5386AD1A-3C03-3D8A-84AA-F7C27001A17C}
// *********************************************************************//
  _CvsCellEditInt = interface(IDispatch)
    ['{5386AD1A-3C03-3D8A-84AA-F7C27001A17C}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Get_Text: WideString; safecall;
    function Get_IsEditCell: WordBool; safecall;
    function Get_SupportsAuthorization: WordBool; safecall;
    function Get_DataType: CvsCellDataType; safecall;
    function Get_ErrorString: WideString; safecall;
    function Get_Error: WordBool; safecall;
    function Get_Control: WordBool; safecall;
    function Get_HasChecksum: WordBool; safecall;
    function Get_Hidden: WordBool; safecall;
    function Get_ProtectionState: CvsCellIPProtection; safecall;
    function Get_Enabled: WordBool; safecall;
    function Get_Location: CvsCellLocation; safecall;
    function Get_Expression: WideString; safecall;
    function Get_SymbolicTag: WideString; safecall;
    function Get_OpcTag: WideString; safecall;
    function Get_CellStateReference: WideString; safecall;
    function Get_Properties: CvsCellProperty; safecall;
    function Get_DataTypeString: WideString; safecall;
    function Get_value: Integer; safecall;
    function Get_MinValue: Integer; safecall;
    function Get_MaxValue: Integer; safecall;
    property ToString: WideString read Get_ToString;
    property Text: WideString read Get_Text;
    property IsEditCell: WordBool read Get_IsEditCell;
    property SupportsAuthorization: WordBool read Get_SupportsAuthorization;
    property DataType: CvsCellDataType read Get_DataType;
    property ErrorString: WideString read Get_ErrorString;
    property Error: WordBool read Get_Error;
    property Control: WordBool read Get_Control;
    property HasChecksum: WordBool read Get_HasChecksum;
    property Hidden: WordBool read Get_Hidden;
    property ProtectionState: CvsCellIPProtection read Get_ProtectionState;
    property Enabled: WordBool read Get_Enabled;
    property Location: CvsCellLocation read Get_Location;
    property Expression: WideString read Get_Expression;
    property SymbolicTag: WideString read Get_SymbolicTag;
    property OpcTag: WideString read Get_OpcTag;
    property CellStateReference: WideString read Get_CellStateReference;
    property Properties: CvsCellProperty read Get_Properties;
    property DataTypeString: WideString read Get_DataTypeString;
    property value: Integer read Get_value;
    property MinValue: Integer read Get_MinValue;
    property MaxValue: Integer read Get_MaxValue;
  end;

// *********************************************************************//
// DispIntf:  _CvsCellEditIntDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {5386AD1A-3C03-3D8A-84AA-F7C27001A17C}
// *********************************************************************//
  _CvsCellEditIntDisp = dispinterface
    ['{5386AD1A-3C03-3D8A-84AA-F7C27001A17C}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 1610743810;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    property Text: WideString readonly dispid 1610743812;
    property IsEditCell: WordBool readonly dispid 1610743813;
    property SupportsAuthorization: WordBool readonly dispid 1610743814;
    property DataType: CvsCellDataType readonly dispid 1610743815;
    property ErrorString: WideString readonly dispid 1610743816;
    property Error: WordBool readonly dispid 1610743817;
    property Control: WordBool readonly dispid 1610743818;
    property HasChecksum: WordBool readonly dispid 1610743819;
    property Hidden: WordBool readonly dispid 1610743820;
    property ProtectionState: CvsCellIPProtection readonly dispid 1610743821;
    property Enabled: WordBool readonly dispid 1610743822;
    property Location: {??CvsCellLocation}OleVariant readonly dispid 1610743823;
    property Expression: WideString readonly dispid 1610743824;
    property SymbolicTag: WideString readonly dispid 1610743825;
    property OpcTag: WideString readonly dispid 1610743826;
    property CellStateReference: WideString readonly dispid 1610743827;
    property Properties: CvsCellProperty readonly dispid 1610743828;
    property DataTypeString: WideString readonly dispid 1610743829;
    property value: Integer readonly dispid 0;
    property MinValue: Integer readonly dispid 1610743831;
    property MaxValue: Integer readonly dispid 1610743832;
  end;

// *********************************************************************//
// Interface: _CvsCellEditLine
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {EB32850E-A72E-38FB-A995-34249BC76810}
// *********************************************************************//
  _CvsCellEditLine = interface(IDispatch)
    ['{EB32850E-A72E-38FB-A995-34249BC76810}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Get_Text: WideString; safecall;
    function Get_IsEditCell: WordBool; safecall;
    function Get_SupportsAuthorization: WordBool; safecall;
    function Get_DataType: CvsCellDataType; safecall;
    function Get_ErrorString: WideString; safecall;
    function Get_Error: WordBool; safecall;
    function Get_Control: WordBool; safecall;
    function Get_HasChecksum: WordBool; safecall;
    function Get_Hidden: WordBool; safecall;
    function Get_ProtectionState: CvsCellIPProtection; safecall;
    function Get_Enabled: WordBool; safecall;
    function Get_Location: CvsCellLocation; safecall;
    function Get_Expression: WideString; safecall;
    function Get_SymbolicTag: WideString; safecall;
    function Get_OpcTag: WideString; safecall;
    function Get_CellStateReference: WideString; safecall;
    function Get_Properties: CvsCellProperty; safecall;
    function Get_DataTypeString: WideString; safecall;
    function Get_EditFlags: CvsEditFlags; safecall;
    function Get_StartPoint: CvsImageLocation; safecall;
    function Get_EndPoint: CvsImageLocation; safecall;
    property ToString: WideString read Get_ToString;
    property Text: WideString read Get_Text;
    property IsEditCell: WordBool read Get_IsEditCell;
    property SupportsAuthorization: WordBool read Get_SupportsAuthorization;
    property DataType: CvsCellDataType read Get_DataType;
    property ErrorString: WideString read Get_ErrorString;
    property Error: WordBool read Get_Error;
    property Control: WordBool read Get_Control;
    property HasChecksum: WordBool read Get_HasChecksum;
    property Hidden: WordBool read Get_Hidden;
    property ProtectionState: CvsCellIPProtection read Get_ProtectionState;
    property Enabled: WordBool read Get_Enabled;
    property Location: CvsCellLocation read Get_Location;
    property Expression: WideString read Get_Expression;
    property SymbolicTag: WideString read Get_SymbolicTag;
    property OpcTag: WideString read Get_OpcTag;
    property CellStateReference: WideString read Get_CellStateReference;
    property Properties: CvsCellProperty read Get_Properties;
    property DataTypeString: WideString read Get_DataTypeString;
    property EditFlags: CvsEditFlags read Get_EditFlags;
    property StartPoint: CvsImageLocation read Get_StartPoint;
    property EndPoint: CvsImageLocation read Get_EndPoint;
  end;

// *********************************************************************//
// DispIntf:  _CvsCellEditLineDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {EB32850E-A72E-38FB-A995-34249BC76810}
// *********************************************************************//
  _CvsCellEditLineDisp = dispinterface
    ['{EB32850E-A72E-38FB-A995-34249BC76810}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    property Text: WideString readonly dispid 1610743812;
    property IsEditCell: WordBool readonly dispid 1610743813;
    property SupportsAuthorization: WordBool readonly dispid 1610743814;
    property DataType: CvsCellDataType readonly dispid 1610743815;
    property ErrorString: WideString readonly dispid 1610743816;
    property Error: WordBool readonly dispid 1610743817;
    property Control: WordBool readonly dispid 1610743818;
    property HasChecksum: WordBool readonly dispid 1610743819;
    property Hidden: WordBool readonly dispid 1610743820;
    property ProtectionState: CvsCellIPProtection readonly dispid 1610743821;
    property Enabled: WordBool readonly dispid 1610743822;
    property Location: {??CvsCellLocation}OleVariant readonly dispid 1610743823;
    property Expression: WideString readonly dispid 1610743824;
    property SymbolicTag: WideString readonly dispid 1610743825;
    property OpcTag: WideString readonly dispid 1610743826;
    property CellStateReference: WideString readonly dispid 1610743827;
    property Properties: CvsCellProperty readonly dispid 1610743828;
    property DataTypeString: WideString readonly dispid 1610743829;
    property EditFlags: CvsEditFlags readonly dispid 1610743830;
    property StartPoint: {??CvsImageLocation}OleVariant readonly dispid 1610743831;
    property EndPoint: {??CvsImageLocation}OleVariant readonly dispid 1610743832;
  end;

// *********************************************************************//
// Interface: _CvsCellEditPoint
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {FA263340-7149-3E9F-9E20-570BD35B2D17}
// *********************************************************************//
  _CvsCellEditPoint = interface(IDispatch)
    ['{FA263340-7149-3E9F-9E20-570BD35B2D17}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Get_Text: WideString; safecall;
    function Get_IsEditCell: WordBool; safecall;
    function Get_SupportsAuthorization: WordBool; safecall;
    function Get_DataType: CvsCellDataType; safecall;
    function Get_ErrorString: WideString; safecall;
    function Get_Error: WordBool; safecall;
    function Get_Control: WordBool; safecall;
    function Get_HasChecksum: WordBool; safecall;
    function Get_Hidden: WordBool; safecall;
    function Get_ProtectionState: CvsCellIPProtection; safecall;
    function Get_Enabled: WordBool; safecall;
    function Get_Location: CvsCellLocation; safecall;
    function Get_Expression: WideString; safecall;
    function Get_SymbolicTag: WideString; safecall;
    function Get_OpcTag: WideString; safecall;
    function Get_CellStateReference: WideString; safecall;
    function Get_Properties: CvsCellProperty; safecall;
    function Get_DataTypeString: WideString; safecall;
    function Get_EditFlags: CvsEditFlags; safecall;
    function Get_GraphicLocation: CvsImageLocation; safecall;
    property ToString: WideString read Get_ToString;
    property Text: WideString read Get_Text;
    property IsEditCell: WordBool read Get_IsEditCell;
    property SupportsAuthorization: WordBool read Get_SupportsAuthorization;
    property DataType: CvsCellDataType read Get_DataType;
    property ErrorString: WideString read Get_ErrorString;
    property Error: WordBool read Get_Error;
    property Control: WordBool read Get_Control;
    property HasChecksum: WordBool read Get_HasChecksum;
    property Hidden: WordBool read Get_Hidden;
    property ProtectionState: CvsCellIPProtection read Get_ProtectionState;
    property Enabled: WordBool read Get_Enabled;
    property Location: CvsCellLocation read Get_Location;
    property Expression: WideString read Get_Expression;
    property SymbolicTag: WideString read Get_SymbolicTag;
    property OpcTag: WideString read Get_OpcTag;
    property CellStateReference: WideString read Get_CellStateReference;
    property Properties: CvsCellProperty read Get_Properties;
    property DataTypeString: WideString read Get_DataTypeString;
    property EditFlags: CvsEditFlags read Get_EditFlags;
    property GraphicLocation: CvsImageLocation read Get_GraphicLocation;
  end;

// *********************************************************************//
// DispIntf:  _CvsCellEditPointDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {FA263340-7149-3E9F-9E20-570BD35B2D17}
// *********************************************************************//
  _CvsCellEditPointDisp = dispinterface
    ['{FA263340-7149-3E9F-9E20-570BD35B2D17}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    property Text: WideString readonly dispid 1610743812;
    property IsEditCell: WordBool readonly dispid 1610743813;
    property SupportsAuthorization: WordBool readonly dispid 1610743814;
    property DataType: CvsCellDataType readonly dispid 1610743815;
    property ErrorString: WideString readonly dispid 1610743816;
    property Error: WordBool readonly dispid 1610743817;
    property Control: WordBool readonly dispid 1610743818;
    property HasChecksum: WordBool readonly dispid 1610743819;
    property Hidden: WordBool readonly dispid 1610743820;
    property ProtectionState: CvsCellIPProtection readonly dispid 1610743821;
    property Enabled: WordBool readonly dispid 1610743822;
    property Location: {??CvsCellLocation}OleVariant readonly dispid 1610743823;
    property Expression: WideString readonly dispid 1610743824;
    property SymbolicTag: WideString readonly dispid 1610743825;
    property OpcTag: WideString readonly dispid 1610743826;
    property CellStateReference: WideString readonly dispid 1610743827;
    property Properties: CvsCellProperty readonly dispid 1610743828;
    property DataTypeString: WideString readonly dispid 1610743829;
    property EditFlags: CvsEditFlags readonly dispid 1610743830;
    property GraphicLocation: {??CvsImageLocation}OleVariant readonly dispid 1610743831;
  end;

// *********************************************************************//
// Interface: _CvsCellEditRegion
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {10D989A4-5AB9-3DE5-A159-624ED7C7CEB4}
// *********************************************************************//
  _CvsCellEditRegion = interface(IDispatch)
    ['{10D989A4-5AB9-3DE5-A159-624ED7C7CEB4}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Get_Text: WideString; safecall;
    function Get_IsEditCell: WordBool; safecall;
    function Get_SupportsAuthorization: WordBool; safecall;
    function Get_DataType: CvsCellDataType; safecall;
    function Get_ErrorString: WideString; safecall;
    function Get_Error: WordBool; safecall;
    function Get_Control: WordBool; safecall;
    function Get_HasChecksum: WordBool; safecall;
    function Get_Hidden: WordBool; safecall;
    function Get_ProtectionState: CvsCellIPProtection; safecall;
    function Get_Enabled: WordBool; safecall;
    function Get_Location: CvsCellLocation; safecall;
    function Get_Expression: WideString; safecall;
    function Get_SymbolicTag: WideString; safecall;
    function Get_OpcTag: WideString; safecall;
    function Get_CellStateReference: WideString; safecall;
    function Get_Properties: CvsCellProperty; safecall;
    function Get_DataTypeString: WideString; safecall;
    function Get_EditFlags: CvsEditFlags; safecall;
    function Get_GraphicLocation: CvsImageLocation; safecall;
    function Get_Height: Single; safecall;
    function Get_Width: Single; safecall;
    function Get_angle: Single; safecall;
    function Get_Curve: Single; safecall;
    property ToString: WideString read Get_ToString;
    property Text: WideString read Get_Text;
    property IsEditCell: WordBool read Get_IsEditCell;
    property SupportsAuthorization: WordBool read Get_SupportsAuthorization;
    property DataType: CvsCellDataType read Get_DataType;
    property ErrorString: WideString read Get_ErrorString;
    property Error: WordBool read Get_Error;
    property Control: WordBool read Get_Control;
    property HasChecksum: WordBool read Get_HasChecksum;
    property Hidden: WordBool read Get_Hidden;
    property ProtectionState: CvsCellIPProtection read Get_ProtectionState;
    property Enabled: WordBool read Get_Enabled;
    property Location: CvsCellLocation read Get_Location;
    property Expression: WideString read Get_Expression;
    property SymbolicTag: WideString read Get_SymbolicTag;
    property OpcTag: WideString read Get_OpcTag;
    property CellStateReference: WideString read Get_CellStateReference;
    property Properties: CvsCellProperty read Get_Properties;
    property DataTypeString: WideString read Get_DataTypeString;
    property EditFlags: CvsEditFlags read Get_EditFlags;
    property GraphicLocation: CvsImageLocation read Get_GraphicLocation;
    property Height: Single read Get_Height;
    property Width: Single read Get_Width;
    property angle: Single read Get_angle;
    property Curve: Single read Get_Curve;
  end;

// *********************************************************************//
// DispIntf:  _CvsCellEditRegionDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {10D989A4-5AB9-3DE5-A159-624ED7C7CEB4}
// *********************************************************************//
  _CvsCellEditRegionDisp = dispinterface
    ['{10D989A4-5AB9-3DE5-A159-624ED7C7CEB4}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    property Text: WideString readonly dispid 1610743812;
    property IsEditCell: WordBool readonly dispid 1610743813;
    property SupportsAuthorization: WordBool readonly dispid 1610743814;
    property DataType: CvsCellDataType readonly dispid 1610743815;
    property ErrorString: WideString readonly dispid 1610743816;
    property Error: WordBool readonly dispid 1610743817;
    property Control: WordBool readonly dispid 1610743818;
    property HasChecksum: WordBool readonly dispid 1610743819;
    property Hidden: WordBool readonly dispid 1610743820;
    property ProtectionState: CvsCellIPProtection readonly dispid 1610743821;
    property Enabled: WordBool readonly dispid 1610743822;
    property Location: {??CvsCellLocation}OleVariant readonly dispid 1610743823;
    property Expression: WideString readonly dispid 1610743824;
    property SymbolicTag: WideString readonly dispid 1610743825;
    property OpcTag: WideString readonly dispid 1610743826;
    property CellStateReference: WideString readonly dispid 1610743827;
    property Properties: CvsCellProperty readonly dispid 1610743828;
    property DataTypeString: WideString readonly dispid 1610743829;
    property EditFlags: CvsEditFlags readonly dispid 1610743830;
    property GraphicLocation: {??CvsImageLocation}OleVariant readonly dispid 1610743831;
    property Height: Single readonly dispid 1610743832;
    property Width: Single readonly dispid 1610743833;
    property angle: Single readonly dispid 1610743834;
    property Curve: Single readonly dispid 1610743835;
  end;

// *********************************************************************//
// Interface: _CvsCellEditString
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {BC72DB91-DD4E-3F98-BCCD-4B1BBBA1B6C0}
// *********************************************************************//
  _CvsCellEditString = interface(IDispatch)
    ['{BC72DB91-DD4E-3F98-BCCD-4B1BBBA1B6C0}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Get_Text: WideString; safecall;
    function Get_IsEditCell: WordBool; safecall;
    function Get_SupportsAuthorization: WordBool; safecall;
    function Get_DataType: CvsCellDataType; safecall;
    function Get_ErrorString: WideString; safecall;
    function Get_Error: WordBool; safecall;
    function Get_Control: WordBool; safecall;
    function Get_HasChecksum: WordBool; safecall;
    function Get_Hidden: WordBool; safecall;
    function Get_ProtectionState: CvsCellIPProtection; safecall;
    function Get_Enabled: WordBool; safecall;
    function Get_Location: CvsCellLocation; safecall;
    function Get_Expression: WideString; safecall;
    function Get_SymbolicTag: WideString; safecall;
    function Get_OpcTag: WideString; safecall;
    function Get_CellStateReference: WideString; safecall;
    function Get_Properties: CvsCellProperty; safecall;
    function Get_DataTypeString: WideString; safecall;
    function Get_MaxChars: Integer; safecall;
    function Get_IsPassword: WordBool; safecall;
    property ToString: WideString read Get_ToString;
    property Text: WideString read Get_Text;
    property IsEditCell: WordBool read Get_IsEditCell;
    property SupportsAuthorization: WordBool read Get_SupportsAuthorization;
    property DataType: CvsCellDataType read Get_DataType;
    property ErrorString: WideString read Get_ErrorString;
    property Error: WordBool read Get_Error;
    property Control: WordBool read Get_Control;
    property HasChecksum: WordBool read Get_HasChecksum;
    property Hidden: WordBool read Get_Hidden;
    property ProtectionState: CvsCellIPProtection read Get_ProtectionState;
    property Enabled: WordBool read Get_Enabled;
    property Location: CvsCellLocation read Get_Location;
    property Expression: WideString read Get_Expression;
    property SymbolicTag: WideString read Get_SymbolicTag;
    property OpcTag: WideString read Get_OpcTag;
    property CellStateReference: WideString read Get_CellStateReference;
    property Properties: CvsCellProperty read Get_Properties;
    property DataTypeString: WideString read Get_DataTypeString;
    property MaxChars: Integer read Get_MaxChars;
    property IsPassword: WordBool read Get_IsPassword;
  end;

// *********************************************************************//
// DispIntf:  _CvsCellEditStringDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {BC72DB91-DD4E-3F98-BCCD-4B1BBBA1B6C0}
// *********************************************************************//
  _CvsCellEditStringDisp = dispinterface
    ['{BC72DB91-DD4E-3F98-BCCD-4B1BBBA1B6C0}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    property Text: WideString readonly dispid 1610743812;
    property IsEditCell: WordBool readonly dispid 1610743813;
    property SupportsAuthorization: WordBool readonly dispid 1610743814;
    property DataType: CvsCellDataType readonly dispid 1610743815;
    property ErrorString: WideString readonly dispid 1610743816;
    property Error: WordBool readonly dispid 1610743817;
    property Control: WordBool readonly dispid 1610743818;
    property HasChecksum: WordBool readonly dispid 1610743819;
    property Hidden: WordBool readonly dispid 1610743820;
    property ProtectionState: CvsCellIPProtection readonly dispid 1610743821;
    property Enabled: WordBool readonly dispid 1610743822;
    property Location: {??CvsCellLocation}OleVariant readonly dispid 1610743823;
    property Expression: WideString readonly dispid 1610743824;
    property SymbolicTag: WideString readonly dispid 1610743825;
    property OpcTag: WideString readonly dispid 1610743826;
    property CellStateReference: WideString readonly dispid 1610743827;
    property Properties: CvsCellProperty readonly dispid 1610743828;
    property DataTypeString: WideString readonly dispid 1610743829;
    property MaxChars: Integer readonly dispid 1610743830;
    property IsPassword: WordBool readonly dispid 1610743831;
  end;

// *********************************************************************//
// Interface: _CvsCellEditAnnulus
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {1CC7D80D-9925-35D8-AE4C-16A04AD9E463}
// *********************************************************************//
  _CvsCellEditAnnulus = interface(IDispatch)
    ['{1CC7D80D-9925-35D8-AE4C-16A04AD9E463}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Get_Text: WideString; safecall;
    function Get_IsEditCell: WordBool; safecall;
    function Get_SupportsAuthorization: WordBool; safecall;
    function Get_DataType: CvsCellDataType; safecall;
    function Get_ErrorString: WideString; safecall;
    function Get_Error: WordBool; safecall;
    function Get_Control: WordBool; safecall;
    function Get_HasChecksum: WordBool; safecall;
    function Get_Hidden: WordBool; safecall;
    function Get_ProtectionState: CvsCellIPProtection; safecall;
    function Get_Enabled: WordBool; safecall;
    function Get_Location: CvsCellLocation; safecall;
    function Get_Expression: WideString; safecall;
    function Get_SymbolicTag: WideString; safecall;
    function Get_OpcTag: WideString; safecall;
    function Get_CellStateReference: WideString; safecall;
    function Get_Properties: CvsCellProperty; safecall;
    function Get_DataTypeString: WideString; safecall;
    function Get_EditFlags: CvsEditFlags; safecall;
    function Get_Center: CvsImageLocation; safecall;
    function Get_InnerRadius: Single; safecall;
    function Get_OuterRadius: Single; safecall;
    property ToString: WideString read Get_ToString;
    property Text: WideString read Get_Text;
    property IsEditCell: WordBool read Get_IsEditCell;
    property SupportsAuthorization: WordBool read Get_SupportsAuthorization;
    property DataType: CvsCellDataType read Get_DataType;
    property ErrorString: WideString read Get_ErrorString;
    property Error: WordBool read Get_Error;
    property Control: WordBool read Get_Control;
    property HasChecksum: WordBool read Get_HasChecksum;
    property Hidden: WordBool read Get_Hidden;
    property ProtectionState: CvsCellIPProtection read Get_ProtectionState;
    property Enabled: WordBool read Get_Enabled;
    property Location: CvsCellLocation read Get_Location;
    property Expression: WideString read Get_Expression;
    property SymbolicTag: WideString read Get_SymbolicTag;
    property OpcTag: WideString read Get_OpcTag;
    property CellStateReference: WideString read Get_CellStateReference;
    property Properties: CvsCellProperty read Get_Properties;
    property DataTypeString: WideString read Get_DataTypeString;
    property EditFlags: CvsEditFlags read Get_EditFlags;
    property Center: CvsImageLocation read Get_Center;
    property InnerRadius: Single read Get_InnerRadius;
    property OuterRadius: Single read Get_OuterRadius;
  end;

// *********************************************************************//
// DispIntf:  _CvsCellEditAnnulusDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {1CC7D80D-9925-35D8-AE4C-16A04AD9E463}
// *********************************************************************//
  _CvsCellEditAnnulusDisp = dispinterface
    ['{1CC7D80D-9925-35D8-AE4C-16A04AD9E463}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    property Text: WideString readonly dispid 1610743812;
    property IsEditCell: WordBool readonly dispid 1610743813;
    property SupportsAuthorization: WordBool readonly dispid 1610743814;
    property DataType: CvsCellDataType readonly dispid 1610743815;
    property ErrorString: WideString readonly dispid 1610743816;
    property Error: WordBool readonly dispid 1610743817;
    property Control: WordBool readonly dispid 1610743818;
    property HasChecksum: WordBool readonly dispid 1610743819;
    property Hidden: WordBool readonly dispid 1610743820;
    property ProtectionState: CvsCellIPProtection readonly dispid 1610743821;
    property Enabled: WordBool readonly dispid 1610743822;
    property Location: {??CvsCellLocation}OleVariant readonly dispid 1610743823;
    property Expression: WideString readonly dispid 1610743824;
    property SymbolicTag: WideString readonly dispid 1610743825;
    property OpcTag: WideString readonly dispid 1610743826;
    property CellStateReference: WideString readonly dispid 1610743827;
    property Properties: CvsCellProperty readonly dispid 1610743828;
    property DataTypeString: WideString readonly dispid 1610743829;
    property EditFlags: CvsEditFlags readonly dispid 1610743830;
    property Center: {??CvsImageLocation}OleVariant readonly dispid 1610743831;
    property InnerRadius: Single readonly dispid 1610743832;
    property OuterRadius: Single readonly dispid 1610743833;
  end;

// *********************************************************************//
// Interface: _CvsCellEmpty
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {02001A00-97A2-34BE-8EA5-501C950D2A77}
// *********************************************************************//
  _CvsCellEmpty = interface(IDispatch)
    ['{02001A00-97A2-34BE-8EA5-501C950D2A77}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Get_Text: WideString; safecall;
    function Get_IsEditCell: WordBool; safecall;
    function Get_SupportsAuthorization: WordBool; safecall;
    function Get_DataType: CvsCellDataType; safecall;
    function Get_ErrorString: WideString; safecall;
    function Get_Error: WordBool; safecall;
    function Get_Control: WordBool; safecall;
    function Get_HasChecksum: WordBool; safecall;
    function Get_Hidden: WordBool; safecall;
    function Get_ProtectionState: CvsCellIPProtection; safecall;
    function Get_Enabled: WordBool; safecall;
    function Get_Location: CvsCellLocation; safecall;
    function Get_Expression: WideString; safecall;
    function Get_SymbolicTag: WideString; safecall;
    function Get_OpcTag: WideString; safecall;
    function Get_CellStateReference: WideString; safecall;
    function Get_Properties: CvsCellProperty; safecall;
    function Get_DataTypeString: WideString; safecall;
    property ToString: WideString read Get_ToString;
    property Text: WideString read Get_Text;
    property IsEditCell: WordBool read Get_IsEditCell;
    property SupportsAuthorization: WordBool read Get_SupportsAuthorization;
    property DataType: CvsCellDataType read Get_DataType;
    property ErrorString: WideString read Get_ErrorString;
    property Error: WordBool read Get_Error;
    property Control: WordBool read Get_Control;
    property HasChecksum: WordBool read Get_HasChecksum;
    property Hidden: WordBool read Get_Hidden;
    property ProtectionState: CvsCellIPProtection read Get_ProtectionState;
    property Enabled: WordBool read Get_Enabled;
    property Location: CvsCellLocation read Get_Location;
    property Expression: WideString read Get_Expression;
    property SymbolicTag: WideString read Get_SymbolicTag;
    property OpcTag: WideString read Get_OpcTag;
    property CellStateReference: WideString read Get_CellStateReference;
    property Properties: CvsCellProperty read Get_Properties;
    property DataTypeString: WideString read Get_DataTypeString;
  end;

// *********************************************************************//
// DispIntf:  _CvsCellEmptyDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {02001A00-97A2-34BE-8EA5-501C950D2A77}
// *********************************************************************//
  _CvsCellEmptyDisp = dispinterface
    ['{02001A00-97A2-34BE-8EA5-501C950D2A77}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    property Text: WideString readonly dispid 1610743812;
    property IsEditCell: WordBool readonly dispid 1610743813;
    property SupportsAuthorization: WordBool readonly dispid 1610743814;
    property DataType: CvsCellDataType readonly dispid 1610743815;
    property ErrorString: WideString readonly dispid 1610743816;
    property Error: WordBool readonly dispid 1610743817;
    property Control: WordBool readonly dispid 1610743818;
    property HasChecksum: WordBool readonly dispid 1610743819;
    property Hidden: WordBool readonly dispid 1610743820;
    property ProtectionState: CvsCellIPProtection readonly dispid 1610743821;
    property Enabled: WordBool readonly dispid 1610743822;
    property Location: {??CvsCellLocation}OleVariant readonly dispid 1610743823;
    property Expression: WideString readonly dispid 1610743824;
    property SymbolicTag: WideString readonly dispid 1610743825;
    property OpcTag: WideString readonly dispid 1610743826;
    property CellStateReference: WideString readonly dispid 1610743827;
    property Properties: CvsCellProperty readonly dispid 1610743828;
    property DataTypeString: WideString readonly dispid 1610743829;
  end;

// *********************************************************************//
// Interface: _CvsCellError
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {A91D5C86-FEFD-311E-A79C-FA761DDCFE1B}
// *********************************************************************//
  _CvsCellError = interface(IDispatch)
    ['{A91D5C86-FEFD-311E-A79C-FA761DDCFE1B}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Get_Location: CvsCellLocation; safecall;
    function Get_ErrorString: WideString; safecall;
    property ToString: WideString read Get_ToString;
    property Location: CvsCellLocation read Get_Location;
    property ErrorString: WideString read Get_ErrorString;
  end;

// *********************************************************************//
// DispIntf:  _CvsCellErrorDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {A91D5C86-FEFD-311E-A79C-FA761DDCFE1B}
// *********************************************************************//
  _CvsCellErrorDisp = dispinterface
    ['{A91D5C86-FEFD-311E-A79C-FA761DDCFE1B}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    property Location: {??CvsCellLocation}OleVariant readonly dispid 1610743812;
    property ErrorString: WideString readonly dispid 1610743813;
  end;

// *********************************************************************//
// Interface: _CvsCellFloat
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {EC343975-D309-3E1C-BE3F-5D536FB83C6B}
// *********************************************************************//
  _CvsCellFloat = interface(IDispatch)
    ['{EC343975-D309-3E1C-BE3F-5D536FB83C6B}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Get_Text: WideString; safecall;
    function Get_IsEditCell: WordBool; safecall;
    function Get_SupportsAuthorization: WordBool; safecall;
    function Get_DataType: CvsCellDataType; safecall;
    function Get_ErrorString: WideString; safecall;
    function Get_Error: WordBool; safecall;
    function Get_Control: WordBool; safecall;
    function Get_HasChecksum: WordBool; safecall;
    function Get_Hidden: WordBool; safecall;
    function Get_ProtectionState: CvsCellIPProtection; safecall;
    function Get_Enabled: WordBool; safecall;
    function Get_Location: CvsCellLocation; safecall;
    function Get_Expression: WideString; safecall;
    function Get_SymbolicTag: WideString; safecall;
    function Get_OpcTag: WideString; safecall;
    function Get_CellStateReference: WideString; safecall;
    function Get_Properties: CvsCellProperty; safecall;
    function Get_DataTypeString: WideString; safecall;
    function Get_value: Single; safecall;
    property ToString: WideString read Get_ToString;
    property Text: WideString read Get_Text;
    property IsEditCell: WordBool read Get_IsEditCell;
    property SupportsAuthorization: WordBool read Get_SupportsAuthorization;
    property DataType: CvsCellDataType read Get_DataType;
    property ErrorString: WideString read Get_ErrorString;
    property Error: WordBool read Get_Error;
    property Control: WordBool read Get_Control;
    property HasChecksum: WordBool read Get_HasChecksum;
    property Hidden: WordBool read Get_Hidden;
    property ProtectionState: CvsCellIPProtection read Get_ProtectionState;
    property Enabled: WordBool read Get_Enabled;
    property Location: CvsCellLocation read Get_Location;
    property Expression: WideString read Get_Expression;
    property SymbolicTag: WideString read Get_SymbolicTag;
    property OpcTag: WideString read Get_OpcTag;
    property CellStateReference: WideString read Get_CellStateReference;
    property Properties: CvsCellProperty read Get_Properties;
    property DataTypeString: WideString read Get_DataTypeString;
    property value: Single read Get_value;
  end;

// *********************************************************************//
// DispIntf:  _CvsCellFloatDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {EC343975-D309-3E1C-BE3F-5D536FB83C6B}
// *********************************************************************//
  _CvsCellFloatDisp = dispinterface
    ['{EC343975-D309-3E1C-BE3F-5D536FB83C6B}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 1610743810;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    property Text: WideString readonly dispid 1610743812;
    property IsEditCell: WordBool readonly dispid 1610743813;
    property SupportsAuthorization: WordBool readonly dispid 1610743814;
    property DataType: CvsCellDataType readonly dispid 1610743815;
    property ErrorString: WideString readonly dispid 1610743816;
    property Error: WordBool readonly dispid 1610743817;
    property Control: WordBool readonly dispid 1610743818;
    property HasChecksum: WordBool readonly dispid 1610743819;
    property Hidden: WordBool readonly dispid 1610743820;
    property ProtectionState: CvsCellIPProtection readonly dispid 1610743821;
    property Enabled: WordBool readonly dispid 1610743822;
    property Location: {??CvsCellLocation}OleVariant readonly dispid 1610743823;
    property Expression: WideString readonly dispid 1610743824;
    property SymbolicTag: WideString readonly dispid 1610743825;
    property OpcTag: WideString readonly dispid 1610743826;
    property CellStateReference: WideString readonly dispid 1610743827;
    property Properties: CvsCellProperty readonly dispid 1610743828;
    property DataTypeString: WideString readonly dispid 1610743829;
    property value: Single readonly dispid 0;
  end;

// *********************************************************************//
// Interface: _CvsCellInt
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {10C119AA-C63E-36FF-91AA-A77C4BA91FAB}
// *********************************************************************//
  _CvsCellInt = interface(IDispatch)
    ['{10C119AA-C63E-36FF-91AA-A77C4BA91FAB}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Get_Text: WideString; safecall;
    function Get_IsEditCell: WordBool; safecall;
    function Get_SupportsAuthorization: WordBool; safecall;
    function Get_DataType: CvsCellDataType; safecall;
    function Get_ErrorString: WideString; safecall;
    function Get_Error: WordBool; safecall;
    function Get_Control: WordBool; safecall;
    function Get_HasChecksum: WordBool; safecall;
    function Get_Hidden: WordBool; safecall;
    function Get_ProtectionState: CvsCellIPProtection; safecall;
    function Get_Enabled: WordBool; safecall;
    function Get_Location: CvsCellLocation; safecall;
    function Get_Expression: WideString; safecall;
    function Get_SymbolicTag: WideString; safecall;
    function Get_OpcTag: WideString; safecall;
    function Get_CellStateReference: WideString; safecall;
    function Get_Properties: CvsCellProperty; safecall;
    function Get_DataTypeString: WideString; safecall;
    function Get_value: Integer; safecall;
    property ToString: WideString read Get_ToString;
    property Text: WideString read Get_Text;
    property IsEditCell: WordBool read Get_IsEditCell;
    property SupportsAuthorization: WordBool read Get_SupportsAuthorization;
    property DataType: CvsCellDataType read Get_DataType;
    property ErrorString: WideString read Get_ErrorString;
    property Error: WordBool read Get_Error;
    property Control: WordBool read Get_Control;
    property HasChecksum: WordBool read Get_HasChecksum;
    property Hidden: WordBool read Get_Hidden;
    property ProtectionState: CvsCellIPProtection read Get_ProtectionState;
    property Enabled: WordBool read Get_Enabled;
    property Location: CvsCellLocation read Get_Location;
    property Expression: WideString read Get_Expression;
    property SymbolicTag: WideString read Get_SymbolicTag;
    property OpcTag: WideString read Get_OpcTag;
    property CellStateReference: WideString read Get_CellStateReference;
    property Properties: CvsCellProperty read Get_Properties;
    property DataTypeString: WideString read Get_DataTypeString;
    property value: Integer read Get_value;
  end;

// *********************************************************************//
// DispIntf:  _CvsCellIntDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {10C119AA-C63E-36FF-91AA-A77C4BA91FAB}
// *********************************************************************//
  _CvsCellIntDisp = dispinterface
    ['{10C119AA-C63E-36FF-91AA-A77C4BA91FAB}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 1610743810;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    property Text: WideString readonly dispid 1610743812;
    property IsEditCell: WordBool readonly dispid 1610743813;
    property SupportsAuthorization: WordBool readonly dispid 1610743814;
    property DataType: CvsCellDataType readonly dispid 1610743815;
    property ErrorString: WideString readonly dispid 1610743816;
    property Error: WordBool readonly dispid 1610743817;
    property Control: WordBool readonly dispid 1610743818;
    property HasChecksum: WordBool readonly dispid 1610743819;
    property Hidden: WordBool readonly dispid 1610743820;
    property ProtectionState: CvsCellIPProtection readonly dispid 1610743821;
    property Enabled: WordBool readonly dispid 1610743822;
    property Location: {??CvsCellLocation}OleVariant readonly dispid 1610743823;
    property Expression: WideString readonly dispid 1610743824;
    property SymbolicTag: WideString readonly dispid 1610743825;
    property OpcTag: WideString readonly dispid 1610743826;
    property CellStateReference: WideString readonly dispid 1610743827;
    property Properties: CvsCellProperty readonly dispid 1610743828;
    property DataTypeString: WideString readonly dispid 1610743829;
    property value: Integer readonly dispid 0;
  end;

// *********************************************************************//
// Interface: _CvsCellLink
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {5D9BE2C7-B8C9-3914-B7CB-C65806E8F8A1}
// *********************************************************************//
  _CvsCellLink = interface(IDispatch)
    ['{5D9BE2C7-B8C9-3914-B7CB-C65806E8F8A1}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Get_Text: WideString; safecall;
    function Get_IsEditCell: WordBool; safecall;
    function Get_SupportsAuthorization: WordBool; safecall;
    function Get_DataType: CvsCellDataType; safecall;
    function Get_ErrorString: WideString; safecall;
    function Get_Error: WordBool; safecall;
    function Get_Control: WordBool; safecall;
    function Get_HasChecksum: WordBool; safecall;
    function Get_Hidden: WordBool; safecall;
    function Get_ProtectionState: CvsCellIPProtection; safecall;
    function Get_Enabled: WordBool; safecall;
    function Get_Location: CvsCellLocation; safecall;
    function Get_Expression: WideString; safecall;
    function Get_SymbolicTag: WideString; safecall;
    function Get_OpcTag: WideString; safecall;
    function Get_CellStateReference: WideString; safecall;
    function Get_Properties: CvsCellProperty; safecall;
    function Get_DataTypeString: WideString; safecall;
    function Get_HostName: WideString; safecall;
    function Get_DialogLabel: WideString; safecall;
    function Get_CursorPosition: WideString; safecall;
    property ToString: WideString read Get_ToString;
    property Text: WideString read Get_Text;
    property IsEditCell: WordBool read Get_IsEditCell;
    property SupportsAuthorization: WordBool read Get_SupportsAuthorization;
    property DataType: CvsCellDataType read Get_DataType;
    property ErrorString: WideString read Get_ErrorString;
    property Error: WordBool read Get_Error;
    property Control: WordBool read Get_Control;
    property HasChecksum: WordBool read Get_HasChecksum;
    property Hidden: WordBool read Get_Hidden;
    property ProtectionState: CvsCellIPProtection read Get_ProtectionState;
    property Enabled: WordBool read Get_Enabled;
    property Location: CvsCellLocation read Get_Location;
    property Expression: WideString read Get_Expression;
    property SymbolicTag: WideString read Get_SymbolicTag;
    property OpcTag: WideString read Get_OpcTag;
    property CellStateReference: WideString read Get_CellStateReference;
    property Properties: CvsCellProperty read Get_Properties;
    property DataTypeString: WideString read Get_DataTypeString;
    property HostName: WideString read Get_HostName;
    property DialogLabel: WideString read Get_DialogLabel;
    property CursorPosition: WideString read Get_CursorPosition;
  end;

// *********************************************************************//
// DispIntf:  _CvsCellLinkDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {5D9BE2C7-B8C9-3914-B7CB-C65806E8F8A1}
// *********************************************************************//
  _CvsCellLinkDisp = dispinterface
    ['{5D9BE2C7-B8C9-3914-B7CB-C65806E8F8A1}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    property Text: WideString readonly dispid 1610743812;
    property IsEditCell: WordBool readonly dispid 1610743813;
    property SupportsAuthorization: WordBool readonly dispid 1610743814;
    property DataType: CvsCellDataType readonly dispid 1610743815;
    property ErrorString: WideString readonly dispid 1610743816;
    property Error: WordBool readonly dispid 1610743817;
    property Control: WordBool readonly dispid 1610743818;
    property HasChecksum: WordBool readonly dispid 1610743819;
    property Hidden: WordBool readonly dispid 1610743820;
    property ProtectionState: CvsCellIPProtection readonly dispid 1610743821;
    property Enabled: WordBool readonly dispid 1610743822;
    property Location: {??CvsCellLocation}OleVariant readonly dispid 1610743823;
    property Expression: WideString readonly dispid 1610743824;
    property SymbolicTag: WideString readonly dispid 1610743825;
    property OpcTag: WideString readonly dispid 1610743826;
    property CellStateReference: WideString readonly dispid 1610743827;
    property Properties: CvsCellProperty readonly dispid 1610743828;
    property DataTypeString: WideString readonly dispid 1610743829;
    property HostName: WideString readonly dispid 1610743830;
    property DialogLabel: WideString readonly dispid 1610743831;
    property CursorPosition: WideString readonly dispid 1610743832;
  end;

// *********************************************************************//
// Interface: _CvsCellListBox
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {86BA4F3E-9268-3A86-8D44-68418EE7237D}
// *********************************************************************//
  _CvsCellListBox = interface(IDispatch)
    ['{86BA4F3E-9268-3A86-8D44-68418EE7237D}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Get_Text: WideString; safecall;
    function Get_IsEditCell: WordBool; safecall;
    function Get_SupportsAuthorization: WordBool; safecall;
    function Get_DataType: CvsCellDataType; safecall;
    function Get_ErrorString: WideString; safecall;
    function Get_Error: WordBool; safecall;
    function Get_Control: WordBool; safecall;
    function Get_HasChecksum: WordBool; safecall;
    function Get_Hidden: WordBool; safecall;
    function Get_ProtectionState: CvsCellIPProtection; safecall;
    function Get_Enabled: WordBool; safecall;
    function Get_Location: CvsCellLocation; safecall;
    function Get_Expression: WideString; safecall;
    function Get_SymbolicTag: WideString; safecall;
    function Get_OpcTag: WideString; safecall;
    function Get_CellStateReference: WideString; safecall;
    function Get_Properties: CvsCellProperty; safecall;
    function Get_DataTypeString: WideString; safecall;
    function Get_value: Integer; safecall;
    function Get_NumSelections: Integer; safecall;
    function GetSelection(index: Integer): WideString; safecall;
    function Get_Items: PSafeArray; safecall;
    property ToString: WideString read Get_ToString;
    property Text: WideString read Get_Text;
    property IsEditCell: WordBool read Get_IsEditCell;
    property SupportsAuthorization: WordBool read Get_SupportsAuthorization;
    property DataType: CvsCellDataType read Get_DataType;
    property ErrorString: WideString read Get_ErrorString;
    property Error: WordBool read Get_Error;
    property Control: WordBool read Get_Control;
    property HasChecksum: WordBool read Get_HasChecksum;
    property Hidden: WordBool read Get_Hidden;
    property ProtectionState: CvsCellIPProtection read Get_ProtectionState;
    property Enabled: WordBool read Get_Enabled;
    property Location: CvsCellLocation read Get_Location;
    property Expression: WideString read Get_Expression;
    property SymbolicTag: WideString read Get_SymbolicTag;
    property OpcTag: WideString read Get_OpcTag;
    property CellStateReference: WideString read Get_CellStateReference;
    property Properties: CvsCellProperty read Get_Properties;
    property DataTypeString: WideString read Get_DataTypeString;
    property value: Integer read Get_value;
    property NumSelections: Integer read Get_NumSelections;
    property Items: PSafeArray read Get_Items;
  end;

// *********************************************************************//
// DispIntf:  _CvsCellListBoxDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {86BA4F3E-9268-3A86-8D44-68418EE7237D}
// *********************************************************************//
  _CvsCellListBoxDisp = dispinterface
    ['{86BA4F3E-9268-3A86-8D44-68418EE7237D}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 1610743810;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    property Text: WideString readonly dispid 1610743812;
    property IsEditCell: WordBool readonly dispid 1610743813;
    property SupportsAuthorization: WordBool readonly dispid 1610743814;
    property DataType: CvsCellDataType readonly dispid 1610743815;
    property ErrorString: WideString readonly dispid 1610743816;
    property Error: WordBool readonly dispid 1610743817;
    property Control: WordBool readonly dispid 1610743818;
    property HasChecksum: WordBool readonly dispid 1610743819;
    property Hidden: WordBool readonly dispid 1610743820;
    property ProtectionState: CvsCellIPProtection readonly dispid 1610743821;
    property Enabled: WordBool readonly dispid 1610743822;
    property Location: {??CvsCellLocation}OleVariant readonly dispid 1610743823;
    property Expression: WideString readonly dispid 1610743824;
    property SymbolicTag: WideString readonly dispid 1610743825;
    property OpcTag: WideString readonly dispid 1610743826;
    property CellStateReference: WideString readonly dispid 1610743827;
    property Properties: CvsCellProperty readonly dispid 1610743828;
    property DataTypeString: WideString readonly dispid 1610743829;
    property value: Integer readonly dispid 0;
    property NumSelections: Integer readonly dispid 1610743831;
    function GetSelection(index: Integer): WideString; dispid 1610743832;
    property Items: {??PSafeArray}OleVariant readonly dispid 1610743833;
  end;

// *********************************************************************//
// Interface: _CvsCellMultiStatus
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {A7CB1DF4-2B18-3F16-841F-681B78C4C879}
// *********************************************************************//
  _CvsCellMultiStatus = interface(IDispatch)
    ['{A7CB1DF4-2B18-3F16-841F-681B78C4C879}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Get_Text: WideString; safecall;
    function Get_IsEditCell: WordBool; safecall;
    function Get_SupportsAuthorization: WordBool; safecall;
    function Get_DataType: CvsCellDataType; safecall;
    function Get_ErrorString: WideString; safecall;
    function Get_Error: WordBool; safecall;
    function Get_Control: WordBool; safecall;
    function Get_HasChecksum: WordBool; safecall;
    function Get_Hidden: WordBool; safecall;
    function Get_ProtectionState: CvsCellIPProtection; safecall;
    function Get_Enabled: WordBool; safecall;
    function Get_Location: CvsCellLocation; safecall;
    function Get_Expression: WideString; safecall;
    function Get_SymbolicTag: WideString; safecall;
    function Get_OpcTag: WideString; safecall;
    function Get_CellStateReference: WideString; safecall;
    function Get_Properties: CvsCellProperty; safecall;
    function Get_DataTypeString: WideString; safecall;
    function Get_value: Integer; safecall;
    function Get_StartBit: Integer; safecall;
    function Get_NumBits: Integer; safecall;
    function Get_ReverseOrder: WordBool; safecall;
    function Get_Color0AsRgb: Integer; safecall;
    function Get_Color1AsRgb: Integer; safecall;
    property ToString: WideString read Get_ToString;
    property Text: WideString read Get_Text;
    property IsEditCell: WordBool read Get_IsEditCell;
    property SupportsAuthorization: WordBool read Get_SupportsAuthorization;
    property DataType: CvsCellDataType read Get_DataType;
    property ErrorString: WideString read Get_ErrorString;
    property Error: WordBool read Get_Error;
    property Control: WordBool read Get_Control;
    property HasChecksum: WordBool read Get_HasChecksum;
    property Hidden: WordBool read Get_Hidden;
    property ProtectionState: CvsCellIPProtection read Get_ProtectionState;
    property Enabled: WordBool read Get_Enabled;
    property Location: CvsCellLocation read Get_Location;
    property Expression: WideString read Get_Expression;
    property SymbolicTag: WideString read Get_SymbolicTag;
    property OpcTag: WideString read Get_OpcTag;
    property CellStateReference: WideString read Get_CellStateReference;
    property Properties: CvsCellProperty read Get_Properties;
    property DataTypeString: WideString read Get_DataTypeString;
    property value: Integer read Get_value;
    property StartBit: Integer read Get_StartBit;
    property NumBits: Integer read Get_NumBits;
    property ReverseOrder: WordBool read Get_ReverseOrder;
    property Color0AsRgb: Integer read Get_Color0AsRgb;
    property Color1AsRgb: Integer read Get_Color1AsRgb;
  end;

// *********************************************************************//
// DispIntf:  _CvsCellMultiStatusDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {A7CB1DF4-2B18-3F16-841F-681B78C4C879}
// *********************************************************************//
  _CvsCellMultiStatusDisp = dispinterface
    ['{A7CB1DF4-2B18-3F16-841F-681B78C4C879}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 1610743810;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    property Text: WideString readonly dispid 1610743812;
    property IsEditCell: WordBool readonly dispid 1610743813;
    property SupportsAuthorization: WordBool readonly dispid 1610743814;
    property DataType: CvsCellDataType readonly dispid 1610743815;
    property ErrorString: WideString readonly dispid 1610743816;
    property Error: WordBool readonly dispid 1610743817;
    property Control: WordBool readonly dispid 1610743818;
    property HasChecksum: WordBool readonly dispid 1610743819;
    property Hidden: WordBool readonly dispid 1610743820;
    property ProtectionState: CvsCellIPProtection readonly dispid 1610743821;
    property Enabled: WordBool readonly dispid 1610743822;
    property Location: {??CvsCellLocation}OleVariant readonly dispid 1610743823;
    property Expression: WideString readonly dispid 1610743824;
    property SymbolicTag: WideString readonly dispid 1610743825;
    property OpcTag: WideString readonly dispid 1610743826;
    property CellStateReference: WideString readonly dispid 1610743827;
    property Properties: CvsCellProperty readonly dispid 1610743828;
    property DataTypeString: WideString readonly dispid 1610743829;
    property value: Integer readonly dispid 0;
    property StartBit: Integer readonly dispid 1610743831;
    property NumBits: Integer readonly dispid 1610743832;
    property ReverseOrder: WordBool readonly dispid 1610743833;
    property Color0AsRgb: Integer readonly dispid 1610743834;
    property Color1AsRgb: Integer readonly dispid 1610743835;
  end;

// *********************************************************************//
// Interface: _CvsCellSelect
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {6EC66596-4D7E-3EDA-8487-79ED481896E0}
// *********************************************************************//
  _CvsCellSelect = interface(IDispatch)
    ['{6EC66596-4D7E-3EDA-8487-79ED481896E0}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Get_Text: WideString; safecall;
    function Get_IsEditCell: WordBool; safecall;
    function Get_SupportsAuthorization: WordBool; safecall;
    function Get_DataType: CvsCellDataType; safecall;
    function Get_ErrorString: WideString; safecall;
    function Get_Error: WordBool; safecall;
    function Get_Control: WordBool; safecall;
    function Get_HasChecksum: WordBool; safecall;
    function Get_Hidden: WordBool; safecall;
    function Get_ProtectionState: CvsCellIPProtection; safecall;
    function Get_Enabled: WordBool; safecall;
    function Get_Location: CvsCellLocation; safecall;
    function Get_Expression: WideString; safecall;
    function Get_SymbolicTag: WideString; safecall;
    function Get_OpcTag: WideString; safecall;
    function Get_CellStateReference: WideString; safecall;
    function Get_Properties: CvsCellProperty; safecall;
    function Get_DataTypeString: WideString; safecall;
    function Get_index: Integer; safecall;
    property ToString: WideString read Get_ToString;
    property Text: WideString read Get_Text;
    property IsEditCell: WordBool read Get_IsEditCell;
    property SupportsAuthorization: WordBool read Get_SupportsAuthorization;
    property DataType: CvsCellDataType read Get_DataType;
    property ErrorString: WideString read Get_ErrorString;
    property Error: WordBool read Get_Error;
    property Control: WordBool read Get_Control;
    property HasChecksum: WordBool read Get_HasChecksum;
    property Hidden: WordBool read Get_Hidden;
    property ProtectionState: CvsCellIPProtection read Get_ProtectionState;
    property Enabled: WordBool read Get_Enabled;
    property Location: CvsCellLocation read Get_Location;
    property Expression: WideString read Get_Expression;
    property SymbolicTag: WideString read Get_SymbolicTag;
    property OpcTag: WideString read Get_OpcTag;
    property CellStateReference: WideString read Get_CellStateReference;
    property Properties: CvsCellProperty read Get_Properties;
    property DataTypeString: WideString read Get_DataTypeString;
    property index: Integer read Get_index;
  end;

// *********************************************************************//
// DispIntf:  _CvsCellSelectDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {6EC66596-4D7E-3EDA-8487-79ED481896E0}
// *********************************************************************//
  _CvsCellSelectDisp = dispinterface
    ['{6EC66596-4D7E-3EDA-8487-79ED481896E0}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    property Text: WideString readonly dispid 1610743812;
    property IsEditCell: WordBool readonly dispid 1610743813;
    property SupportsAuthorization: WordBool readonly dispid 1610743814;
    property DataType: CvsCellDataType readonly dispid 1610743815;
    property ErrorString: WideString readonly dispid 1610743816;
    property Error: WordBool readonly dispid 1610743817;
    property Control: WordBool readonly dispid 1610743818;
    property HasChecksum: WordBool readonly dispid 1610743819;
    property Hidden: WordBool readonly dispid 1610743820;
    property ProtectionState: CvsCellIPProtection readonly dispid 1610743821;
    property Enabled: WordBool readonly dispid 1610743822;
    property Location: {??CvsCellLocation}OleVariant readonly dispid 1610743823;
    property Expression: WideString readonly dispid 1610743824;
    property SymbolicTag: WideString readonly dispid 1610743825;
    property OpcTag: WideString readonly dispid 1610743826;
    property CellStateReference: WideString readonly dispid 1610743827;
    property Properties: CvsCellProperty readonly dispid 1610743828;
    property DataTypeString: WideString readonly dispid 1610743829;
    property index: Integer readonly dispid 1610743830;
  end;

// *********************************************************************//
// Interface: _CvsCellStatus
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {6D105E29-1EB0-3375-BC50-CB32CE69C366}
// *********************************************************************//
  _CvsCellStatus = interface(IDispatch)
    ['{6D105E29-1EB0-3375-BC50-CB32CE69C366}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Get_Text: WideString; safecall;
    function Get_IsEditCell: WordBool; safecall;
    function Get_SupportsAuthorization: WordBool; safecall;
    function Get_DataType: CvsCellDataType; safecall;
    function Get_ErrorString: WideString; safecall;
    function Get_Error: WordBool; safecall;
    function Get_Control: WordBool; safecall;
    function Get_HasChecksum: WordBool; safecall;
    function Get_Hidden: WordBool; safecall;
    function Get_ProtectionState: CvsCellIPProtection; safecall;
    function Get_Enabled: WordBool; safecall;
    function Get_Location: CvsCellLocation; safecall;
    function Get_Expression: WideString; safecall;
    function Get_SymbolicTag: WideString; safecall;
    function Get_OpcTag: WideString; safecall;
    function Get_CellStateReference: WideString; safecall;
    function Get_Properties: CvsCellProperty; safecall;
    function Get_DataTypeString: WideString; safecall;
    function Get_value: Integer; safecall;
    property ToString: WideString read Get_ToString;
    property Text: WideString read Get_Text;
    property IsEditCell: WordBool read Get_IsEditCell;
    property SupportsAuthorization: WordBool read Get_SupportsAuthorization;
    property DataType: CvsCellDataType read Get_DataType;
    property ErrorString: WideString read Get_ErrorString;
    property Error: WordBool read Get_Error;
    property Control: WordBool read Get_Control;
    property HasChecksum: WordBool read Get_HasChecksum;
    property Hidden: WordBool read Get_Hidden;
    property ProtectionState: CvsCellIPProtection read Get_ProtectionState;
    property Enabled: WordBool read Get_Enabled;
    property Location: CvsCellLocation read Get_Location;
    property Expression: WideString read Get_Expression;
    property SymbolicTag: WideString read Get_SymbolicTag;
    property OpcTag: WideString read Get_OpcTag;
    property CellStateReference: WideString read Get_CellStateReference;
    property Properties: CvsCellProperty read Get_Properties;
    property DataTypeString: WideString read Get_DataTypeString;
    property value: Integer read Get_value;
  end;

// *********************************************************************//
// DispIntf:  _CvsCellStatusDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {6D105E29-1EB0-3375-BC50-CB32CE69C366}
// *********************************************************************//
  _CvsCellStatusDisp = dispinterface
    ['{6D105E29-1EB0-3375-BC50-CB32CE69C366}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 1610743810;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    property Text: WideString readonly dispid 1610743812;
    property IsEditCell: WordBool readonly dispid 1610743813;
    property SupportsAuthorization: WordBool readonly dispid 1610743814;
    property DataType: CvsCellDataType readonly dispid 1610743815;
    property ErrorString: WideString readonly dispid 1610743816;
    property Error: WordBool readonly dispid 1610743817;
    property Control: WordBool readonly dispid 1610743818;
    property HasChecksum: WordBool readonly dispid 1610743819;
    property Hidden: WordBool readonly dispid 1610743820;
    property ProtectionState: CvsCellIPProtection readonly dispid 1610743821;
    property Enabled: WordBool readonly dispid 1610743822;
    property Location: {??CvsCellLocation}OleVariant readonly dispid 1610743823;
    property Expression: WideString readonly dispid 1610743824;
    property SymbolicTag: WideString readonly dispid 1610743825;
    property OpcTag: WideString readonly dispid 1610743826;
    property CellStateReference: WideString readonly dispid 1610743827;
    property Properties: CvsCellProperty readonly dispid 1610743828;
    property DataTypeString: WideString readonly dispid 1610743829;
    property value: Integer readonly dispid 0;
  end;

// *********************************************************************//
// Interface: _CvsCellStatusLight
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {411D685A-67AD-3624-A94A-D37D92DEFE64}
// *********************************************************************//
  _CvsCellStatusLight = interface(IDispatch)
    ['{411D685A-67AD-3624-A94A-D37D92DEFE64}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Get_Text: WideString; safecall;
    function Get_IsEditCell: WordBool; safecall;
    function Get_SupportsAuthorization: WordBool; safecall;
    function Get_DataType: CvsCellDataType; safecall;
    function Get_ErrorString: WideString; safecall;
    function Get_Error: WordBool; safecall;
    function Get_Control: WordBool; safecall;
    function Get_HasChecksum: WordBool; safecall;
    function Get_Hidden: WordBool; safecall;
    function Get_ProtectionState: CvsCellIPProtection; safecall;
    function Get_Enabled: WordBool; safecall;
    function Get_Location: CvsCellLocation; safecall;
    function Get_Expression: WideString; safecall;
    function Get_SymbolicTag: WideString; safecall;
    function Get_OpcTag: WideString; safecall;
    function Get_CellStateReference: WideString; safecall;
    function Get_Properties: CvsCellProperty; safecall;
    function Get_DataTypeString: WideString; safecall;
    function Get_value: Integer; safecall;
    function Get_ColorAsRgb: Integer; safecall;
    property ToString: WideString read Get_ToString;
    property Text: WideString read Get_Text;
    property IsEditCell: WordBool read Get_IsEditCell;
    property SupportsAuthorization: WordBool read Get_SupportsAuthorization;
    property DataType: CvsCellDataType read Get_DataType;
    property ErrorString: WideString read Get_ErrorString;
    property Error: WordBool read Get_Error;
    property Control: WordBool read Get_Control;
    property HasChecksum: WordBool read Get_HasChecksum;
    property Hidden: WordBool read Get_Hidden;
    property ProtectionState: CvsCellIPProtection read Get_ProtectionState;
    property Enabled: WordBool read Get_Enabled;
    property Location: CvsCellLocation read Get_Location;
    property Expression: WideString read Get_Expression;
    property SymbolicTag: WideString read Get_SymbolicTag;
    property OpcTag: WideString read Get_OpcTag;
    property CellStateReference: WideString read Get_CellStateReference;
    property Properties: CvsCellProperty read Get_Properties;
    property DataTypeString: WideString read Get_DataTypeString;
    property value: Integer read Get_value;
    property ColorAsRgb: Integer read Get_ColorAsRgb;
  end;

// *********************************************************************//
// DispIntf:  _CvsCellStatusLightDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {411D685A-67AD-3624-A94A-D37D92DEFE64}
// *********************************************************************//
  _CvsCellStatusLightDisp = dispinterface
    ['{411D685A-67AD-3624-A94A-D37D92DEFE64}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 1610743810;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    property Text: WideString readonly dispid 1610743812;
    property IsEditCell: WordBool readonly dispid 1610743813;
    property SupportsAuthorization: WordBool readonly dispid 1610743814;
    property DataType: CvsCellDataType readonly dispid 1610743815;
    property ErrorString: WideString readonly dispid 1610743816;
    property Error: WordBool readonly dispid 1610743817;
    property Control: WordBool readonly dispid 1610743818;
    property HasChecksum: WordBool readonly dispid 1610743819;
    property Hidden: WordBool readonly dispid 1610743820;
    property ProtectionState: CvsCellIPProtection readonly dispid 1610743821;
    property Enabled: WordBool readonly dispid 1610743822;
    property Location: {??CvsCellLocation}OleVariant readonly dispid 1610743823;
    property Expression: WideString readonly dispid 1610743824;
    property SymbolicTag: WideString readonly dispid 1610743825;
    property OpcTag: WideString readonly dispid 1610743826;
    property CellStateReference: WideString readonly dispid 1610743827;
    property Properties: CvsCellProperty readonly dispid 1610743828;
    property DataTypeString: WideString readonly dispid 1610743829;
    property value: Integer readonly dispid 0;
    property ColorAsRgb: Integer readonly dispid 1610743831;
  end;

// *********************************************************************//
// Interface: _CvsCellWizard
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {C8B3640F-7372-3A1C-AA59-6DD29CC6FA27}
// *********************************************************************//
  _CvsCellWizard = interface(IDispatch)
    ['{C8B3640F-7372-3A1C-AA59-6DD29CC6FA27}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Get_Text: WideString; safecall;
    function Get_IsEditCell: WordBool; safecall;
    function Get_SupportsAuthorization: WordBool; safecall;
    function Get_DataType: CvsCellDataType; safecall;
    function Get_ErrorString: WideString; safecall;
    function Get_Error: WordBool; safecall;
    function Get_Control: WordBool; safecall;
    function Get_HasChecksum: WordBool; safecall;
    function Get_Hidden: WordBool; safecall;
    function Get_ProtectionState: CvsCellIPProtection; safecall;
    function Get_Enabled: WordBool; safecall;
    function Get_Location: CvsCellLocation; safecall;
    function Get_Expression: WideString; safecall;
    function Get_SymbolicTag: WideString; safecall;
    function Get_OpcTag: WideString; safecall;
    function Get_CellStateReference: WideString; safecall;
    function Get_Properties: CvsCellProperty; safecall;
    function Get_DataTypeString: WideString; safecall;
    function Get_NumDialogs: Integer; safecall;
    property ToString: WideString read Get_ToString;
    property Text: WideString read Get_Text;
    property IsEditCell: WordBool read Get_IsEditCell;
    property SupportsAuthorization: WordBool read Get_SupportsAuthorization;
    property DataType: CvsCellDataType read Get_DataType;
    property ErrorString: WideString read Get_ErrorString;
    property Error: WordBool read Get_Error;
    property Control: WordBool read Get_Control;
    property HasChecksum: WordBool read Get_HasChecksum;
    property Hidden: WordBool read Get_Hidden;
    property ProtectionState: CvsCellIPProtection read Get_ProtectionState;
    property Enabled: WordBool read Get_Enabled;
    property Location: CvsCellLocation read Get_Location;
    property Expression: WideString read Get_Expression;
    property SymbolicTag: WideString read Get_SymbolicTag;
    property OpcTag: WideString read Get_OpcTag;
    property CellStateReference: WideString read Get_CellStateReference;
    property Properties: CvsCellProperty read Get_Properties;
    property DataTypeString: WideString read Get_DataTypeString;
    property NumDialogs: Integer read Get_NumDialogs;
  end;

// *********************************************************************//
// DispIntf:  _CvsCellWizardDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {C8B3640F-7372-3A1C-AA59-6DD29CC6FA27}
// *********************************************************************//
  _CvsCellWizardDisp = dispinterface
    ['{C8B3640F-7372-3A1C-AA59-6DD29CC6FA27}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    property Text: WideString readonly dispid 1610743812;
    property IsEditCell: WordBool readonly dispid 1610743813;
    property SupportsAuthorization: WordBool readonly dispid 1610743814;
    property DataType: CvsCellDataType readonly dispid 1610743815;
    property ErrorString: WideString readonly dispid 1610743816;
    property Error: WordBool readonly dispid 1610743817;
    property Control: WordBool readonly dispid 1610743818;
    property HasChecksum: WordBool readonly dispid 1610743819;
    property Hidden: WordBool readonly dispid 1610743820;
    property ProtectionState: CvsCellIPProtection readonly dispid 1610743821;
    property Enabled: WordBool readonly dispid 1610743822;
    property Location: {??CvsCellLocation}OleVariant readonly dispid 1610743823;
    property Expression: WideString readonly dispid 1610743824;
    property SymbolicTag: WideString readonly dispid 1610743825;
    property OpcTag: WideString readonly dispid 1610743826;
    property CellStateReference: WideString readonly dispid 1610743827;
    property Properties: CvsCellProperty readonly dispid 1610743828;
    property DataTypeString: WideString readonly dispid 1610743829;
    property NumDialogs: Integer readonly dispid 1610743830;
  end;

// *********************************************************************//
// Interface: _CvsDiscreteInputLine
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {37846D60-72EE-3CFB-AF2B-361662DE3410}
// *********************************************************************//
  _CvsDiscreteInputLine = interface(IDispatch)
    ['{37846D60-72EE-3CFB-AF2B-361662DE3410}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Get_line: Integer; safecall;
    function Get_Name: WideString; safecall;
    procedure Set_Name(const pRetVal: WideString); safecall;
    function Get_type_: CvsInputType; safecall;
    function Get_Signal: CvsInputSignal; safecall;
    procedure Set_Signal(pRetVal: CvsInputSignal); safecall;
    property ToString: WideString read Get_ToString;
    property line: Integer read Get_line;
    property Name: WideString read Get_Name write Set_Name;
    property type_: CvsInputType read Get_type_;
    property Signal: CvsInputSignal read Get_Signal write Set_Signal;
  end;

// *********************************************************************//
// DispIntf:  _CvsDiscreteInputLineDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {37846D60-72EE-3CFB-AF2B-361662DE3410}
// *********************************************************************//
  _CvsDiscreteInputLineDisp = dispinterface
    ['{37846D60-72EE-3CFB-AF2B-361662DE3410}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    property line: Integer readonly dispid 1610743812;
    property Name: WideString dispid 1610743813;
    property type_: CvsInputType readonly dispid 1610743815;
    property Signal: CvsInputSignal dispid 1610743816;
  end;

// *********************************************************************//
// Interface: _CvsInputCollection
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {49A657D3-D3D2-353F-A819-9B31EE6E952E}
// *********************************************************************//
  _CvsInputCollection = interface(IDispatch)
    ['{49A657D3-D3D2-353F-A819-9B31EE6E952E}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function GetEnumerator: IEnumVARIANT; safecall;
    function Get_Count: Integer; safecall;
    function Get_Item(index: Integer): _CvsDiscreteInputLine; safecall;
    function GetInputSettings(indexInputLine: Integer): _CvsDiscreteInputLine; safecall;
    function Get_Items: PSafeArray; safecall;
    property ToString: WideString read Get_ToString;
    property Count: Integer read Get_Count;
    property Item[index: Integer]: _CvsDiscreteInputLine read Get_Item; default;
    property Items: PSafeArray read Get_Items;
  end;

// *********************************************************************//
// DispIntf:  _CvsInputCollectionDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {49A657D3-D3D2-353F-A819-9B31EE6E952E}
// *********************************************************************//
  _CvsInputCollectionDisp = dispinterface
    ['{49A657D3-D3D2-353F-A819-9B31EE6E952E}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 1610743810;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    function GetEnumerator: IEnumVARIANT; dispid -4;
    property Count: Integer readonly dispid 1610743813;
    property Item[index: Integer]: _CvsDiscreteInputLine readonly dispid 0; default;
    function GetInputSettings(indexInputLine: Integer): _CvsDiscreteInputLine; dispid 1610743815;
    property Items: {??PSafeArray}OleVariant readonly dispid 1610743816;
  end;

// *********************************************************************//
// Interface: _CvsOutputCollection
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {740B7E9C-0FBF-3FD0-8898-8F17B3AF992E}
// *********************************************************************//
  _CvsOutputCollection = interface(IDispatch)
    ['{740B7E9C-0FBF-3FD0-8898-8F17B3AF992E}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function GetEnumerator: IEnumVARIANT; safecall;
    function Get_Count: Integer; safecall;
    function Get_Item(index: Integer): _CvsDiscreteOutputLine; safecall;
    function GetOutputSettings(indexOutputLine: Integer): _CvsDiscreteOutputLine; safecall;
    function Get_Items: PSafeArray; safecall;
    property ToString: WideString read Get_ToString;
    property Count: Integer read Get_Count;
    property Item[index: Integer]: _CvsDiscreteOutputLine read Get_Item; default;
    property Items: PSafeArray read Get_Items;
  end;

// *********************************************************************//
// DispIntf:  _CvsOutputCollectionDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {740B7E9C-0FBF-3FD0-8898-8F17B3AF992E}
// *********************************************************************//
  _CvsOutputCollectionDisp = dispinterface
    ['{740B7E9C-0FBF-3FD0-8898-8F17B3AF992E}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 1610743810;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    function GetEnumerator: IEnumVARIANT; dispid -4;
    property Count: Integer readonly dispid 1610743813;
    property Item[index: Integer]: _CvsDiscreteOutputLine readonly dispid 0; default;
    function GetOutputSettings(indexOutputLine: Integer): _CvsDiscreteOutputLine; dispid 1610743815;
    property Items: {??PSafeArray}OleVariant readonly dispid 1610743816;
  end;

// *********************************************************************//
// Interface: _CvsDiscreteOutputLine
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {E698B9AA-B001-320D-BC83-254CB7587327}
// *********************************************************************//
  _CvsDiscreteOutputLine = interface(IDispatch)
    ['{E698B9AA-B001-320D-BC83-254CB7587327}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Clone: OleVariant; safecall;
    function Get_line: Integer; safecall;
    function Get_Name: WideString; safecall;
    procedure Set_Name(const pRetVal: WideString); safecall;
    function Get_type_: CvsOutputType; safecall;
    procedure Set_type_(pRetVal: CvsOutputType); safecall;
    function Get_OutputListType: CvsOutputListType; safecall;
    function Get_DelayNumber: Integer; safecall;
    procedure Set_DelayNumber(pRetVal: Integer); safecall;
    function Get_EncoderDelayNumber: Integer; safecall;
    procedure Set_EncoderDelayNumber(pRetVal: Integer); safecall;
    function Get_DelayNumberIsTimeAfterTrigger: CvsOutputDelayTypeFlags; safecall;
    procedure Set_DelayNumberIsTimeAfterTrigger(pRetVal: CvsOutputDelayTypeFlags); safecall;
    function Get_PulseEnabledOrStrobedEdge: WordBool; safecall;
    procedure Set_PulseEnabledOrStrobedEdge(pRetVal: WordBool); safecall;
    function Get_PulseLength: Integer; safecall;
    procedure Set_PulseLength(pRetVal: Integer); safecall;
    function Get_IsHighSpeed: WordBool; safecall;
    function IsValidOutputType(Type_: CvsOutputType): WordBool; safecall;
    property ToString: WideString read Get_ToString;
    property line: Integer read Get_line;
    property Name: WideString read Get_Name write Set_Name;
    property type_: CvsOutputType read Get_type_ write Set_type_;
    property OutputListType: CvsOutputListType read Get_OutputListType;
    property DelayNumber: Integer read Get_DelayNumber write Set_DelayNumber;
    property EncoderDelayNumber: Integer read Get_EncoderDelayNumber write Set_EncoderDelayNumber;
    property DelayNumberIsTimeAfterTrigger: CvsOutputDelayTypeFlags read Get_DelayNumberIsTimeAfterTrigger write Set_DelayNumberIsTimeAfterTrigger;
    property PulseEnabledOrStrobedEdge: WordBool read Get_PulseEnabledOrStrobedEdge write Set_PulseEnabledOrStrobedEdge;
    property PulseLength: Integer read Get_PulseLength write Set_PulseLength;
    property IsHighSpeed: WordBool read Get_IsHighSpeed;
  end;

// *********************************************************************//
// DispIntf:  _CvsDiscreteOutputLineDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {E698B9AA-B001-320D-BC83-254CB7587327}
// *********************************************************************//
  _CvsDiscreteOutputLineDisp = dispinterface
    ['{E698B9AA-B001-320D-BC83-254CB7587327}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    function Clone: OleVariant; dispid 1610743812;
    property line: Integer readonly dispid 1610743813;
    property Name: WideString dispid 1610743814;
    property type_: CvsOutputType dispid 1610743816;
    property OutputListType: CvsOutputListType readonly dispid 1610743818;
    property DelayNumber: Integer dispid 1610743819;
    property EncoderDelayNumber: Integer dispid 1610743821;
    property DelayNumberIsTimeAfterTrigger: CvsOutputDelayTypeFlags dispid 1610743823;
    property PulseEnabledOrStrobedEdge: WordBool dispid 1610743825;
    property PulseLength: Integer dispid 1610743827;
    property IsHighSpeed: WordBool readonly dispid 1610743829;
    function IsValidOutputType(Type_: CvsOutputType): WordBool; dispid 1610743830;
  end;

// *********************************************************************//
// Interface: _CvsExpansionIOSettings
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {55789679-C862-3A94-8645-6A348005D300}
// *********************************************************************//
  _CvsExpansionIOSettings = interface(IDispatch)
    ['{55789679-C862-3A94-8645-6A348005D300}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Clone: OleVariant; safecall;
    function Get_Inputs: _CvsInputCollection; safecall;
    function Get_Outputs: _CvsOutputCollection; safecall;
    function IsValidInputType(line: Integer; Type_: CvsInputType): WordBool; safecall;
    procedure SetInputType(line: Integer; value: CvsInputType); safecall;
    procedure SetOutputType(line: Integer; value: CvsOutputType); safecall;
    function IsEqual(const ioLines: _CvsDiscreteIOLines): WordBool; safecall;
    property ToString: WideString read Get_ToString;
    property Inputs: _CvsInputCollection read Get_Inputs;
    property Outputs: _CvsOutputCollection read Get_Outputs;
  end;

// *********************************************************************//
// DispIntf:  _CvsExpansionIOSettingsDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {55789679-C862-3A94-8645-6A348005D300}
// *********************************************************************//
  _CvsExpansionIOSettingsDisp = dispinterface
    ['{55789679-C862-3A94-8645-6A348005D300}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    function Clone: OleVariant; dispid 1610743812;
    property Inputs: _CvsInputCollection readonly dispid 1610743813;
    property Outputs: _CvsOutputCollection readonly dispid 1610743814;
    function IsValidInputType(line: Integer; Type_: CvsInputType): WordBool; dispid 1610743815;
    procedure SetInputType(line: Integer; value: CvsInputType); dispid 1610743816;
    procedure SetOutputType(line: Integer; value: CvsOutputType); dispid 1610743817;
    function IsEqual(const ioLines: _CvsDiscreteIOLines): WordBool; dispid 1610743818;
  end;

// *********************************************************************//
// Interface: _CvsFile
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {13CB3169-81EC-3B04-BE4D-89465F002446}
// *********************************************************************//
  _CvsFile = interface(IDispatch)
    ['{13CB3169-81EC-3B04-BE4D-89465F002446}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function CompareTo(obj: OleVariant): Integer; safecall;
    function Get_Name: WideString; safecall;
    function Get_Size: Integer; safecall;
    function Get_IsDirectory: WordBool; safecall;
    property ToString: WideString read Get_ToString;
    property Name: WideString read Get_Name;
    property Size: Integer read Get_Size;
    property IsDirectory: WordBool read Get_IsDirectory;
  end;

// *********************************************************************//
// DispIntf:  _CvsFileDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {13CB3169-81EC-3B04-BE4D-89465F002446}
// *********************************************************************//
  _CvsFileDisp = dispinterface
    ['{13CB3169-81EC-3B04-BE4D-89465F002446}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    function CompareTo(obj: OleVariant): Integer; dispid 1610743812;
    property Name: WideString readonly dispid 1610743813;
    property Size: Integer readonly dispid 1610743814;
    property IsDirectory: WordBool readonly dispid 1610743815;
  end;

// *********************************************************************//
// Interface: _CvsFileCollection
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {59975A48-BFBF-3159-BED2-AA16C6FE7A18}
// *********************************************************************//
  _CvsFileCollection = interface(IDispatch)
    ['{59975A48-BFBF-3159-BED2-AA16C6FE7A18}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function GetEnumerator: IEnumVARIANT; safecall;
    function Get_Count: Integer; safecall;
    function Get_Item(index: Integer): _CvsFile; safecall;
    function GetFilteredCollection(const extension: WideString): _CvsFileCollection; safecall;
    property ToString: WideString read Get_ToString;
    property Count: Integer read Get_Count;
    property Item[index: Integer]: _CvsFile read Get_Item; default;
  end;

// *********************************************************************//
// DispIntf:  _CvsFileCollectionDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {59975A48-BFBF-3159-BED2-AA16C6FE7A18}
// *********************************************************************//
  _CvsFileCollectionDisp = dispinterface
    ['{59975A48-BFBF-3159-BED2-AA16C6FE7A18}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 1610743810;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    function GetEnumerator: IEnumVARIANT; dispid -4;
    property Count: Integer readonly dispid 1610743813;
    property Item[index: Integer]: _CvsFile readonly dispid 0; default;
    function GetFilteredCollection(const extension: WideString): _CvsFileCollection; dispid 1610743815;
  end;

// *********************************************************************//
// Interface: _CvsFileManager
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {B6C002CA-DC0A-3F79-ACBA-B86CB68DFED6}
// *********************************************************************//
  _CvsFileManager = interface(IDispatch)
    ['{B6C002CA-DC0A-3F79-ACBA-B86CB68DFED6}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function GetLifetimeService: OleVariant; safecall;
    function InitializeLifetimeService: OleVariant; safecall;
    function CreateObjRef(const requestedType: _Type): _ObjRef; safecall;
    procedure SendFileToInSight(const localFilename: WideString; const remoteFilename: WideString); safecall;
    procedure SendImageToInSight(const localFilename: WideString); safecall;
    procedure SendImageToInSight_2(const localFilename: WideString; Target: Integer); safecall;
    procedure GetFileFromInSight(const localFilename: WideString; const remoteFilename: WideString); safecall;
    procedure RemoveFile(const remoteFilename: WideString); safecall;
    function GetFileList: PSafeArray; safecall;
    procedure LoadJobFile(const remoteFilename: WideString); safecall;
    procedure LoadJobFile_2(const remoteFilename: WideString; ftpServer: WordBool); safecall;
    procedure LoadJobFile_3(const remoteFilename: WideString; const Username: WideString; 
                            const Password: WideString); safecall;
    procedure LoadJobFile_4(const remoteFilename: WideString; const Username: WideString; 
                            const Password: WideString; ftpServer: WordBool); safecall;
    procedure CreateNewJob; safecall;
    procedure SaveJobFile(const remoteFilename: WideString); safecall;
    procedure SaveJobFile_2(const remoteFilename: WideString; ftpServer: WordBool); safecall;
    procedure SaveJobFileLocally(const localFilename: WideString); safecall;
    procedure LoadJobFileLocally(const localFilename: WideString); safecall;
    procedure RenameFile(const remoteFilename: WideString; const newRemoteFilename: WideString); safecall;
    property ToString: WideString read Get_ToString;
  end;

// *********************************************************************//
// DispIntf:  _CvsFileManagerDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {B6C002CA-DC0A-3F79-ACBA-B86CB68DFED6}
// *********************************************************************//
  _CvsFileManagerDisp = dispinterface
    ['{B6C002CA-DC0A-3F79-ACBA-B86CB68DFED6}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    function GetLifetimeService: OleVariant; dispid 1610743812;
    function InitializeLifetimeService: OleVariant; dispid 1610743813;
    function CreateObjRef(const requestedType: _Type): _ObjRef; dispid 1610743814;
    procedure SendFileToInSight(const localFilename: WideString; const remoteFilename: WideString); dispid 1610743815;
    procedure SendImageToInSight(const localFilename: WideString); dispid 1610743816;
    procedure SendImageToInSight_2(const localFilename: WideString; Target: Integer); dispid 1610743817;
    procedure GetFileFromInSight(const localFilename: WideString; const remoteFilename: WideString); dispid 1610743818;
    procedure RemoveFile(const remoteFilename: WideString); dispid 1610743819;
    function GetFileList: {??PSafeArray}OleVariant; dispid 1610743820;
    procedure LoadJobFile(const remoteFilename: WideString); dispid 1610743821;
    procedure LoadJobFile_2(const remoteFilename: WideString; ftpServer: WordBool); dispid 1610743822;
    procedure LoadJobFile_3(const remoteFilename: WideString; const Username: WideString; 
                            const Password: WideString); dispid 1610743823;
    procedure LoadJobFile_4(const remoteFilename: WideString; const Username: WideString; 
                            const Password: WideString; ftpServer: WordBool); dispid 1610743824;
    procedure CreateNewJob; dispid 1610743825;
    procedure SaveJobFile(const remoteFilename: WideString); dispid 1610743826;
    procedure SaveJobFile_2(const remoteFilename: WideString; ftpServer: WordBool); dispid 1610743827;
    procedure SaveJobFileLocally(const localFilename: WideString); dispid 1610743828;
    procedure LoadJobFileLocally(const localFilename: WideString); dispid 1610743829;
    procedure RenameFile(const remoteFilename: WideString; const newRemoteFilename: WideString); dispid 1610743830;
  end;

// *********************************************************************//
// Interface: _CvsFtpClient
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {25DF9E32-6A27-30D7-BC1D-D897B126E115}
// *********************************************************************//
  _CvsFtpClient = interface(IDispatch)
    ['{25DF9E32-6A27-30D7-BC1D-D897B126E115}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Get_Connected: WordBool; safecall;
    function Get_Username: WideString; safecall;
    function Get_Password: WideString; safecall;
    function Get_RemoteIPAddressString: WideString; safecall;
    function Get_Port: Integer; safecall;
    function Get_WelcomeBanner: WideString; safecall;
    function Get_LastResponse: WideString; safecall;
    function Get_Timeout: Integer; safecall;
    procedure Set_Timeout(pRetVal: Integer); safecall;
    function Get_UsePasswordEncryption: WordBool; safecall;
    procedure Set_UsePasswordEncryption(pRetVal: WordBool); safecall;
    procedure Connect(const HostName: WideString; const Username: WideString; 
                      const Password: WideString; Port: Integer); safecall;
    function GetReadPermissionEnabled: WordBool; safecall;
    function GetWritePermissionEnabled: WordBool; safecall;
    procedure GetFileFromInSight(const localFilename: WideString; const remoteFilename: WideString); safecall;
    procedure SendFileToInSight(const localFilename: WideString; const remoteFilename: WideString); safecall;
    procedure Disconnect; safecall;
    procedure RemoveFile(const remoteFilename: WideString); safecall;
    function GetFileList: PSafeArray; safecall;
    function GetFileCollection(const Directory: WideString): _CvsFileCollection; safecall;
    procedure RenameFile(const remoteFilename: WideString; const newRemoteFilename: WideString); safecall;
    function IsValidFilename(const remoteFilename: WideString): WordBool; safecall;
    property ToString: WideString read Get_ToString;
    property Connected: WordBool read Get_Connected;
    property Username: WideString read Get_Username;
    property Password: WideString read Get_Password;
    property RemoteIPAddressString: WideString read Get_RemoteIPAddressString;
    property Port: Integer read Get_Port;
    property WelcomeBanner: WideString read Get_WelcomeBanner;
    property LastResponse: WideString read Get_LastResponse;
    property Timeout: Integer read Get_Timeout write Set_Timeout;
    property UsePasswordEncryption: WordBool read Get_UsePasswordEncryption write Set_UsePasswordEncryption;
  end;

// *********************************************************************//
// DispIntf:  _CvsFtpClientDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {25DF9E32-6A27-30D7-BC1D-D897B126E115}
// *********************************************************************//
  _CvsFtpClientDisp = dispinterface
    ['{25DF9E32-6A27-30D7-BC1D-D897B126E115}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    property Connected: WordBool readonly dispid 1610743812;
    property Username: WideString readonly dispid 1610743813;
    property Password: WideString readonly dispid 1610743814;
    property RemoteIPAddressString: WideString readonly dispid 1610743815;
    property Port: Integer readonly dispid 1610743816;
    property WelcomeBanner: WideString readonly dispid 1610743817;
    property LastResponse: WideString readonly dispid 1610743818;
    property Timeout: Integer dispid 1610743819;
    property UsePasswordEncryption: WordBool dispid 1610743821;
    procedure Connect(const HostName: WideString; const Username: WideString; 
                      const Password: WideString; Port: Integer); dispid 1610743823;
    function GetReadPermissionEnabled: WordBool; dispid 1610743824;
    function GetWritePermissionEnabled: WordBool; dispid 1610743825;
    procedure GetFileFromInSight(const localFilename: WideString; const remoteFilename: WideString); dispid 1610743826;
    procedure SendFileToInSight(const localFilename: WideString; const remoteFilename: WideString); dispid 1610743827;
    procedure Disconnect; dispid 1610743828;
    procedure RemoveFile(const remoteFilename: WideString); dispid 1610743829;
    function GetFileList: {??PSafeArray}OleVariant; dispid 1610743830;
    function GetFileCollection(const Directory: WideString): _CvsFileCollection; dispid 1610743831;
    procedure RenameFile(const remoteFilename: WideString; const newRemoteFilename: WideString); dispid 1610743832;
    function IsValidFilename(const remoteFilename: WideString): WordBool; dispid 1610743833;
  end;

// *********************************************************************//
// Interface: _CvsFtpSettings
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {9A4B5A65-EFAF-3E0C-B24D-7FCFCF1D6BD4}
// *********************************************************************//
  _CvsFtpSettings = interface(IDispatch)
    ['{9A4B5A65-EFAF-3E0C-B24D-7FCFCF1D6BD4}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Clone: OleVariant; safecall;
    function Get_PassiveTransfersEnabled: WordBool; safecall;
    procedure Set_PassiveTransfersEnabled(pRetVal: WordBool); safecall;
    function Get_IdleTimeout: Integer; safecall;
    procedure Set_IdleTimeout(pRetVal: Integer); safecall;
    function Get_ReadTimeout: Integer; safecall;
    procedure Set_ReadTimeout(pRetVal: Integer); safecall;
    function Get_ConnectionRetries: Integer; safecall;
    procedure Set_ConnectionRetries(pRetVal: Integer); safecall;
    function Get_RemotePort: Integer; safecall;
    procedure Set_RemotePort(pRetVal: Integer); safecall;
    function Get_LocalPort: Integer; safecall;
    procedure Set_LocalPort(pRetVal: Integer); safecall;
    property ToString: WideString read Get_ToString;
    property PassiveTransfersEnabled: WordBool read Get_PassiveTransfersEnabled write Set_PassiveTransfersEnabled;
    property IdleTimeout: Integer read Get_IdleTimeout write Set_IdleTimeout;
    property ReadTimeout: Integer read Get_ReadTimeout write Set_ReadTimeout;
    property ConnectionRetries: Integer read Get_ConnectionRetries write Set_ConnectionRetries;
    property RemotePort: Integer read Get_RemotePort write Set_RemotePort;
    property LocalPort: Integer read Get_LocalPort write Set_LocalPort;
  end;

// *********************************************************************//
// DispIntf:  _CvsFtpSettingsDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {9A4B5A65-EFAF-3E0C-B24D-7FCFCF1D6BD4}
// *********************************************************************//
  _CvsFtpSettingsDisp = dispinterface
    ['{9A4B5A65-EFAF-3E0C-B24D-7FCFCF1D6BD4}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    function Clone: OleVariant; dispid 1610743812;
    property PassiveTransfersEnabled: WordBool dispid 1610743813;
    property IdleTimeout: Integer dispid 1610743815;
    property ReadTimeout: Integer dispid 1610743817;
    property ConnectionRetries: Integer dispid 1610743819;
    property RemotePort: Integer dispid 1610743821;
    property LocalPort: Integer dispid 1610743823;
  end;

// *********************************************************************//
// Interface: _CvsGraphic4Side
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {DC188909-6498-3594-BE3D-FE499F6BF004}
// *********************************************************************//
  _CvsGraphic4Side = interface(IDispatch)
    ['{DC188909-6498-3594-BE3D-FE499F6BF004}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Clone: _CvsGraphic; safecall;
    function Get_CellLocation: CvsCellLocation; safecall;
    function Get_HighlightColorAsRgb: Integer; safecall;
    function Get_Label_: WideString; safecall;
    procedure Set_Label_(const pRetVal: WideString); safecall;
    function Get_IsAlwaysShown: WordBool; safecall;
    function Get_IsDefaultEdit: WordBool; safecall;
    function Get_IsLocked: WordBool; safecall;
    function GetDrawOrder: Integer; safecall;
    procedure add_PropertyChanged(const value: IUnknown); safecall;
    procedure remove_PropertyChanged(const value: IUnknown); safecall;
    procedure Initialize(cellRow: Integer; cellCol: Integer; pt0Row: Single; pt0Col: Single; 
                         pt1Row: Single; pt1Col: Single; pt2Row: Single; pt2Col: Single; 
                         pt3Row: Single; pt3Col: Single; const graphicLabel: WideString); safecall;
    function Get_Point0: CvsImageLocation; safecall;
    procedure Set_Point0(pRetVal: CvsImageLocation); safecall;
    function Get_Point1: CvsImageLocation; safecall;
    procedure Set_Point1(pRetVal: CvsImageLocation); safecall;
    function Get_Point2: CvsImageLocation; safecall;
    procedure Set_Point2(pRetVal: CvsImageLocation); safecall;
    function Get_Point3: CvsImageLocation; safecall;
    procedure Set_Point3(pRetVal: CvsImageLocation); safecall;
    function Get_Style: CvsDisplayable4SideStyle; safecall;
    procedure Set_Style(pRetVal: CvsDisplayable4SideStyle); safecall;
    function Get_EditBounds: Rectangle; safecall;
    procedure Set_EditBounds(pRetVal: Rectangle); safecall;
    property ToString: WideString read Get_ToString;
    property CellLocation: CvsCellLocation read Get_CellLocation;
    property HighlightColorAsRgb: Integer read Get_HighlightColorAsRgb;
    property Label_: WideString read Get_Label_ write Set_Label_;
    property IsAlwaysShown: WordBool read Get_IsAlwaysShown;
    property IsDefaultEdit: WordBool read Get_IsDefaultEdit;
    property IsLocked: WordBool read Get_IsLocked;
    property Point0: CvsImageLocation read Get_Point0 write Set_Point0;
    property Point1: CvsImageLocation read Get_Point1 write Set_Point1;
    property Point2: CvsImageLocation read Get_Point2 write Set_Point2;
    property Point3: CvsImageLocation read Get_Point3 write Set_Point3;
    property Style: CvsDisplayable4SideStyle read Get_Style write Set_Style;
    property EditBounds: Rectangle read Get_EditBounds write Set_EditBounds;
  end;

// *********************************************************************//
// DispIntf:  _CvsGraphic4SideDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {DC188909-6498-3594-BE3D-FE499F6BF004}
// *********************************************************************//
  _CvsGraphic4SideDisp = dispinterface
    ['{DC188909-6498-3594-BE3D-FE499F6BF004}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    function Clone: _CvsGraphic; dispid 1610743812;
    property CellLocation: {??CvsCellLocation}OleVariant readonly dispid 1610743813;
    property HighlightColorAsRgb: Integer readonly dispid 1610743814;
    property Label_: WideString dispid 1610743815;
    property IsAlwaysShown: WordBool readonly dispid 1610743817;
    property IsDefaultEdit: WordBool readonly dispid 1610743818;
    property IsLocked: WordBool readonly dispid 1610743819;
    function GetDrawOrder: Integer; dispid 1610743820;
    procedure add_PropertyChanged(const value: IUnknown); dispid 1610743821;
    procedure remove_PropertyChanged(const value: IUnknown); dispid 1610743822;
    procedure Initialize(cellRow: Integer; cellCol: Integer; pt0Row: Single; pt0Col: Single; 
                         pt1Row: Single; pt1Col: Single; pt2Row: Single; pt2Col: Single; 
                         pt3Row: Single; pt3Col: Single; const graphicLabel: WideString); dispid 1610743823;
    property Point0: {??CvsImageLocation}OleVariant dispid 1610743824;
    property Point1: {??CvsImageLocation}OleVariant dispid 1610743826;
    property Point2: {??CvsImageLocation}OleVariant dispid 1610743828;
    property Point3: {??CvsImageLocation}OleVariant dispid 1610743830;
    property Style: CvsDisplayable4SideStyle dispid 1610743832;
    property EditBounds: {??Rectangle}OleVariant dispid 1610743834;
  end;

// *********************************************************************//
// Interface: _CvsGraphicArc
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {8CCC33A2-8B40-3707-9369-86D4BAFBEF3A}
// *********************************************************************//
  _CvsGraphicArc = interface(IDispatch)
    ['{8CCC33A2-8B40-3707-9369-86D4BAFBEF3A}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Clone: _CvsGraphic; safecall;
    function Get_CellLocation: CvsCellLocation; safecall;
    function Get_HighlightColorAsRgb: Integer; safecall;
    function Get_Label_: WideString; safecall;
    procedure Set_Label_(const pRetVal: WideString); safecall;
    function Get_IsAlwaysShown: WordBool; safecall;
    function Get_IsDefaultEdit: WordBool; safecall;
    function Get_IsLocked: WordBool; safecall;
    function GetDrawOrder: Integer; safecall;
    procedure add_PropertyChanged(const value: IUnknown); safecall;
    procedure remove_PropertyChanged(const value: IUnknown); safecall;
    procedure Initialize(cellRow: Integer; cellCol: Integer; centerRow: Single; centerCol: Single; 
                         startAngle: Single; endAngle: Single; Radius: Single; 
                         const Label_: WideString); safecall;
    function Get_Center: CvsImageLocation; safecall;
    procedure Set_Center(pRetVal: CvsImageLocation); safecall;
    function Get_startAngle: Single; safecall;
    procedure Set_startAngle(pRetVal: Single); safecall;
    function Get_endAngle: Single; safecall;
    procedure Set_endAngle(pRetVal: Single); safecall;
    function Get_Radius: Single; safecall;
    procedure Set_Radius(pRetVal: Single); safecall;
    function Get_CounterClockwise: WordBool; safecall;
    property ToString: WideString read Get_ToString;
    property CellLocation: CvsCellLocation read Get_CellLocation;
    property HighlightColorAsRgb: Integer read Get_HighlightColorAsRgb;
    property Label_: WideString read Get_Label_ write Set_Label_;
    property IsAlwaysShown: WordBool read Get_IsAlwaysShown;
    property IsDefaultEdit: WordBool read Get_IsDefaultEdit;
    property IsLocked: WordBool read Get_IsLocked;
    property Center: CvsImageLocation read Get_Center write Set_Center;
    property startAngle: Single read Get_startAngle write Set_startAngle;
    property endAngle: Single read Get_endAngle write Set_endAngle;
    property Radius: Single read Get_Radius write Set_Radius;
    property CounterClockwise: WordBool read Get_CounterClockwise;
  end;

// *********************************************************************//
// DispIntf:  _CvsGraphicArcDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {8CCC33A2-8B40-3707-9369-86D4BAFBEF3A}
// *********************************************************************//
  _CvsGraphicArcDisp = dispinterface
    ['{8CCC33A2-8B40-3707-9369-86D4BAFBEF3A}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    function Clone: _CvsGraphic; dispid 1610743812;
    property CellLocation: {??CvsCellLocation}OleVariant readonly dispid 1610743813;
    property HighlightColorAsRgb: Integer readonly dispid 1610743814;
    property Label_: WideString dispid 1610743815;
    property IsAlwaysShown: WordBool readonly dispid 1610743817;
    property IsDefaultEdit: WordBool readonly dispid 1610743818;
    property IsLocked: WordBool readonly dispid 1610743819;
    function GetDrawOrder: Integer; dispid 1610743820;
    procedure add_PropertyChanged(const value: IUnknown); dispid 1610743821;
    procedure remove_PropertyChanged(const value: IUnknown); dispid 1610743822;
    procedure Initialize(cellRow: Integer; cellCol: Integer; centerRow: Single; centerCol: Single; 
                         startAngle: Single; endAngle: Single; Radius: Single; 
                         const Label_: WideString); dispid 1610743823;
    property Center: {??CvsImageLocation}OleVariant dispid 1610743824;
    property startAngle: Single dispid 1610743826;
    property endAngle: Single dispid 1610743828;
    property Radius: Single dispid 1610743830;
    property CounterClockwise: WordBool readonly dispid 1610743832;
  end;

// *********************************************************************//
// Interface: _CvsGraphicBlob
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {B6F1C8B5-662D-30F2-80A8-6F2451374558}
// *********************************************************************//
  _CvsGraphicBlob = interface(IDispatch)
    ['{B6F1C8B5-662D-30F2-80A8-6F2451374558}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Clone: _CvsGraphic; safecall;
    function Get_CellLocation: CvsCellLocation; safecall;
    function Get_HighlightColorAsRgb: Integer; safecall;
    function Get_Label_: WideString; safecall;
    procedure Set_Label_(const pRetVal: WideString); safecall;
    function Get_IsAlwaysShown: WordBool; safecall;
    function Get_IsDefaultEdit: WordBool; safecall;
    function Get_IsLocked: WordBool; safecall;
    function GetDrawOrder: Integer; safecall;
    procedure add_PropertyChanged(const value: IUnknown); safecall;
    procedure remove_PropertyChanged(const value: IUnknown); safecall;
    function Get_Location: CvsImageLocation; safecall;
    function Get_BlobNumber: Integer; safecall;
    function Get_NumPoints: Integer; safecall;
    function GetPoints: PSafeArray; safecall;
    property ToString: WideString read Get_ToString;
    property CellLocation: CvsCellLocation read Get_CellLocation;
    property HighlightColorAsRgb: Integer read Get_HighlightColorAsRgb;
    property Label_: WideString read Get_Label_ write Set_Label_;
    property IsAlwaysShown: WordBool read Get_IsAlwaysShown;
    property IsDefaultEdit: WordBool read Get_IsDefaultEdit;
    property IsLocked: WordBool read Get_IsLocked;
    property Location: CvsImageLocation read Get_Location;
    property BlobNumber: Integer read Get_BlobNumber;
    property NumPoints: Integer read Get_NumPoints;
  end;

// *********************************************************************//
// DispIntf:  _CvsGraphicBlobDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {B6F1C8B5-662D-30F2-80A8-6F2451374558}
// *********************************************************************//
  _CvsGraphicBlobDisp = dispinterface
    ['{B6F1C8B5-662D-30F2-80A8-6F2451374558}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    function Clone: _CvsGraphic; dispid 1610743812;
    property CellLocation: {??CvsCellLocation}OleVariant readonly dispid 1610743813;
    property HighlightColorAsRgb: Integer readonly dispid 1610743814;
    property Label_: WideString dispid 1610743815;
    property IsAlwaysShown: WordBool readonly dispid 1610743817;
    property IsDefaultEdit: WordBool readonly dispid 1610743818;
    property IsLocked: WordBool readonly dispid 1610743819;
    function GetDrawOrder: Integer; dispid 1610743820;
    procedure add_PropertyChanged(const value: IUnknown); dispid 1610743821;
    procedure remove_PropertyChanged(const value: IUnknown); dispid 1610743822;
    property Location: {??CvsImageLocation}OleVariant readonly dispid 1610743823;
    property BlobNumber: Integer readonly dispid 1610743824;
    property NumPoints: Integer readonly dispid 1610743825;
    function GetPoints: {??PSafeArray}OleVariant; dispid 1610743826;
  end;

// *********************************************************************//
// Interface: _CvsGraphicCircle
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {06B7B5E9-63F1-375B-8F6D-3A0B3D8CBFBB}
// *********************************************************************//
  _CvsGraphicCircle = interface(IDispatch)
    ['{06B7B5E9-63F1-375B-8F6D-3A0B3D8CBFBB}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Clone: _CvsGraphic; safecall;
    function Get_CellLocation: CvsCellLocation; safecall;
    function Get_HighlightColorAsRgb: Integer; safecall;
    function Get_Label_: WideString; safecall;
    procedure Set_Label_(const pRetVal: WideString); safecall;
    function Get_IsAlwaysShown: WordBool; safecall;
    function Get_IsDefaultEdit: WordBool; safecall;
    function Get_IsLocked: WordBool; safecall;
    function GetDrawOrder: Integer; safecall;
    procedure add_PropertyChanged(const value: IUnknown); safecall;
    procedure remove_PropertyChanged(const value: IUnknown); safecall;
    procedure Initialize(cellRow: Integer; cellCol: Integer; centerRow: Single; centerCol: Single; 
                         circleRadius: Single; const Label_: WideString); safecall;
    function Get_Center: CvsImageLocation; safecall;
    procedure Set_Center(pRetVal: CvsImageLocation); safecall;
    function Get_Radius: Single; safecall;
    procedure Set_Radius(pRetVal: Single); safecall;
    function Get_EditFlags: CvsEditFlags; safecall;
    procedure Set_EditFlags(pRetVal: CvsEditFlags); safecall;
    function Get_EditBounds: Rectangle; safecall;
    procedure Set_EditBounds(pRetVal: Rectangle); safecall;
    property ToString: WideString read Get_ToString;
    property CellLocation: CvsCellLocation read Get_CellLocation;
    property HighlightColorAsRgb: Integer read Get_HighlightColorAsRgb;
    property Label_: WideString read Get_Label_ write Set_Label_;
    property IsAlwaysShown: WordBool read Get_IsAlwaysShown;
    property IsDefaultEdit: WordBool read Get_IsDefaultEdit;
    property IsLocked: WordBool read Get_IsLocked;
    property Center: CvsImageLocation read Get_Center write Set_Center;
    property Radius: Single read Get_Radius write Set_Radius;
    property EditFlags: CvsEditFlags read Get_EditFlags write Set_EditFlags;
    property EditBounds: Rectangle read Get_EditBounds write Set_EditBounds;
  end;

// *********************************************************************//
// DispIntf:  _CvsGraphicCircleDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {06B7B5E9-63F1-375B-8F6D-3A0B3D8CBFBB}
// *********************************************************************//
  _CvsGraphicCircleDisp = dispinterface
    ['{06B7B5E9-63F1-375B-8F6D-3A0B3D8CBFBB}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    function Clone: _CvsGraphic; dispid 1610743812;
    property CellLocation: {??CvsCellLocation}OleVariant readonly dispid 1610743813;
    property HighlightColorAsRgb: Integer readonly dispid 1610743814;
    property Label_: WideString dispid 1610743815;
    property IsAlwaysShown: WordBool readonly dispid 1610743817;
    property IsDefaultEdit: WordBool readonly dispid 1610743818;
    property IsLocked: WordBool readonly dispid 1610743819;
    function GetDrawOrder: Integer; dispid 1610743820;
    procedure add_PropertyChanged(const value: IUnknown); dispid 1610743821;
    procedure remove_PropertyChanged(const value: IUnknown); dispid 1610743822;
    procedure Initialize(cellRow: Integer; cellCol: Integer; centerRow: Single; centerCol: Single; 
                         circleRadius: Single; const Label_: WideString); dispid 1610743823;
    property Center: {??CvsImageLocation}OleVariant dispid 1610743824;
    property Radius: Single dispid 1610743826;
    property EditFlags: CvsEditFlags dispid 1610743828;
    property EditBounds: {??Rectangle}OleVariant dispid 1610743830;
  end;

// *********************************************************************//
// Interface: _CvsGraphicCollection
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {EB6FE800-AF5F-38FD-B84C-5E9606D18A0C}
// *********************************************************************//
  _CvsGraphicCollection = interface(IDispatch)
    ['{EB6FE800-AF5F-38FD-B84C-5E9606D18A0C}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Get_Count: Integer; safecall;
    function Get_Item(index: Integer): _CvsGraphic; safecall;
    function Get_Items: PSafeArray; safecall;
    function GetCellGraphics(const Location: WideString): _CvsGraphicCollection; safecall;
    function GetDefaultGraphic(const Location: WideString): _CvsGraphic; safecall;
    property ToString: WideString read Get_ToString;
    property Count: Integer read Get_Count;
    property Item[index: Integer]: _CvsGraphic read Get_Item; default;
    property Items: PSafeArray read Get_Items;
  end;

// *********************************************************************//
// DispIntf:  _CvsGraphicCollectionDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {EB6FE800-AF5F-38FD-B84C-5E9606D18A0C}
// *********************************************************************//
  _CvsGraphicCollectionDisp = dispinterface
    ['{EB6FE800-AF5F-38FD-B84C-5E9606D18A0C}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 1610743810;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    property Count: Integer readonly dispid 1610743812;
    property Item[index: Integer]: _CvsGraphic readonly dispid 0; default;
    property Items: {??PSafeArray}OleVariant readonly dispid 1610743814;
    function GetCellGraphics(const Location: WideString): _CvsGraphicCollection; dispid 1610743815;
    function GetDefaultGraphic(const Location: WideString): _CvsGraphic; dispid 1610743816;
  end;

// *********************************************************************//
// Interface: _CvsGraphicPatternMatch
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {08922AB7-BA6C-3C41-B0BD-A5C56E349249}
// *********************************************************************//
  _CvsGraphicPatternMatch = interface(IDispatch)
    ['{08922AB7-BA6C-3C41-B0BD-A5C56E349249}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Clone: _CvsGraphic; safecall;
    function Get_CellLocation: CvsCellLocation; safecall;
    function Get_HighlightColorAsRgb: Integer; safecall;
    function Get_Label_: WideString; safecall;
    procedure Set_Label_(const pRetVal: WideString); safecall;
    function Get_IsAlwaysShown: WordBool; safecall;
    function Get_IsDefaultEdit: WordBool; safecall;
    function Get_IsLocked: WordBool; safecall;
    function GetDrawOrder: Integer; safecall;
    procedure add_PropertyChanged(const value: IUnknown); safecall;
    procedure remove_PropertyChanged(const value: IUnknown); safecall;
    function Get_NumSegments: Integer; safecall;
    function GetSegments: PSafeArray; safecall;
    property ToString: WideString read Get_ToString;
    property CellLocation: CvsCellLocation read Get_CellLocation;
    property HighlightColorAsRgb: Integer read Get_HighlightColorAsRgb;
    property Label_: WideString read Get_Label_ write Set_Label_;
    property IsAlwaysShown: WordBool read Get_IsAlwaysShown;
    property IsDefaultEdit: WordBool read Get_IsDefaultEdit;
    property IsLocked: WordBool read Get_IsLocked;
    property NumSegments: Integer read Get_NumSegments;
  end;

// *********************************************************************//
// DispIntf:  _CvsGraphicPatternMatchDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {08922AB7-BA6C-3C41-B0BD-A5C56E349249}
// *********************************************************************//
  _CvsGraphicPatternMatchDisp = dispinterface
    ['{08922AB7-BA6C-3C41-B0BD-A5C56E349249}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    function Clone: _CvsGraphic; dispid 1610743812;
    property CellLocation: {??CvsCellLocation}OleVariant readonly dispid 1610743813;
    property HighlightColorAsRgb: Integer readonly dispid 1610743814;
    property Label_: WideString dispid 1610743815;
    property IsAlwaysShown: WordBool readonly dispid 1610743817;
    property IsDefaultEdit: WordBool readonly dispid 1610743818;
    property IsLocked: WordBool readonly dispid 1610743819;
    function GetDrawOrder: Integer; dispid 1610743820;
    procedure add_PropertyChanged(const value: IUnknown); dispid 1610743821;
    procedure remove_PropertyChanged(const value: IUnknown); dispid 1610743822;
    property NumSegments: Integer readonly dispid 1610743823;
    function GetSegments: {??PSafeArray}OleVariant; dispid 1610743824;
  end;

// *********************************************************************//
// Interface: _CvsPatternMatchSegment
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {01B6BE8E-25E3-3420-A3A3-EEEB6A7A2961}
// *********************************************************************//
  _CvsPatternMatchSegment = interface(IDispatch)
    ['{01B6BE8E-25E3-3420-A3A3-EEEB6A7A2961}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Get_StartLocation: CvsImageLocation; safecall;
    function Get_EndLocation: CvsImageLocation; safecall;
    function Get_ColorAsRgb: Integer; safecall;
    function Get_HighlightColorAsRgb: Integer; safecall;
    property ToString: WideString read Get_ToString;
    property StartLocation: CvsImageLocation read Get_StartLocation;
    property EndLocation: CvsImageLocation read Get_EndLocation;
    property ColorAsRgb: Integer read Get_ColorAsRgb;
    property HighlightColorAsRgb: Integer read Get_HighlightColorAsRgb;
  end;

// *********************************************************************//
// DispIntf:  _CvsPatternMatchSegmentDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {01B6BE8E-25E3-3420-A3A3-EEEB6A7A2961}
// *********************************************************************//
  _CvsPatternMatchSegmentDisp = dispinterface
    ['{01B6BE8E-25E3-3420-A3A3-EEEB6A7A2961}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    property StartLocation: {??CvsImageLocation}OleVariant readonly dispid 1610743812;
    property EndLocation: {??CvsImageLocation}OleVariant readonly dispid 1610743813;
    property ColorAsRgb: Integer readonly dispid 1610743814;
    property HighlightColorAsRgb: Integer readonly dispid 1610743815;
  end;

// *********************************************************************//
// Interface: _CvsGraphicCross
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {C723F6AC-9290-3C76-A898-0DAD31B86DFD}
// *********************************************************************//
  _CvsGraphicCross = interface(IDispatch)
    ['{C723F6AC-9290-3C76-A898-0DAD31B86DFD}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Clone: _CvsGraphic; safecall;
    function Get_CellLocation: CvsCellLocation; safecall;
    function Get_HighlightColorAsRgb: Integer; safecall;
    function Get_Label_: WideString; safecall;
    procedure Set_Label_(const pRetVal: WideString); safecall;
    function Get_IsAlwaysShown: WordBool; safecall;
    function Get_IsDefaultEdit: WordBool; safecall;
    function Get_IsLocked: WordBool; safecall;
    function GetDrawOrder: Integer; safecall;
    procedure add_PropertyChanged(const value: IUnknown); safecall;
    procedure remove_PropertyChanged(const value: IUnknown); safecall;
    procedure Initialize(cellRow: Integer; cellCol: Integer; centerRow: Single; centerCol: Single; 
                         crossAngle: Single; crossHeight: Single; crossWidth: Single; 
                         const crossLabel: WideString); safecall;
    function Get_Center: CvsImageLocation; safecall;
    procedure Set_Center(pRetVal: CvsImageLocation); safecall;
    function Get_Height: Single; safecall;
    procedure Set_Height(pRetVal: Single); safecall;
    function Get_Width: Single; safecall;
    procedure Set_Width(pRetVal: Single); safecall;
    function Get_angle: Single; safecall;
    procedure Set_angle(pRetVal: Single); safecall;
    function Get_DrawArrows: WordBool; safecall;
    procedure Set_DrawArrows(pRetVal: WordBool); safecall;
    function Get_EditFlags: CvsEditFlags; safecall;
    procedure Set_EditFlags(pRetVal: CvsEditFlags); safecall;
    function Get_EditBounds: Rectangle; safecall;
    procedure Set_EditBounds(pRetVal: Rectangle); safecall;
    property ToString: WideString read Get_ToString;
    property CellLocation: CvsCellLocation read Get_CellLocation;
    property HighlightColorAsRgb: Integer read Get_HighlightColorAsRgb;
    property Label_: WideString read Get_Label_ write Set_Label_;
    property IsAlwaysShown: WordBool read Get_IsAlwaysShown;
    property IsDefaultEdit: WordBool read Get_IsDefaultEdit;
    property IsLocked: WordBool read Get_IsLocked;
    property Center: CvsImageLocation read Get_Center write Set_Center;
    property Height: Single read Get_Height write Set_Height;
    property Width: Single read Get_Width write Set_Width;
    property angle: Single read Get_angle write Set_angle;
    property DrawArrows: WordBool read Get_DrawArrows write Set_DrawArrows;
    property EditFlags: CvsEditFlags read Get_EditFlags write Set_EditFlags;
    property EditBounds: Rectangle read Get_EditBounds write Set_EditBounds;
  end;

// *********************************************************************//
// DispIntf:  _CvsGraphicCrossDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {C723F6AC-9290-3C76-A898-0DAD31B86DFD}
// *********************************************************************//
  _CvsGraphicCrossDisp = dispinterface
    ['{C723F6AC-9290-3C76-A898-0DAD31B86DFD}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    function Clone: _CvsGraphic; dispid 1610743812;
    property CellLocation: {??CvsCellLocation}OleVariant readonly dispid 1610743813;
    property HighlightColorAsRgb: Integer readonly dispid 1610743814;
    property Label_: WideString dispid 1610743815;
    property IsAlwaysShown: WordBool readonly dispid 1610743817;
    property IsDefaultEdit: WordBool readonly dispid 1610743818;
    property IsLocked: WordBool readonly dispid 1610743819;
    function GetDrawOrder: Integer; dispid 1610743820;
    procedure add_PropertyChanged(const value: IUnknown); dispid 1610743821;
    procedure remove_PropertyChanged(const value: IUnknown); dispid 1610743822;
    procedure Initialize(cellRow: Integer; cellCol: Integer; centerRow: Single; centerCol: Single; 
                         crossAngle: Single; crossHeight: Single; crossWidth: Single; 
                         const crossLabel: WideString); dispid 1610743823;
    property Center: {??CvsImageLocation}OleVariant dispid 1610743824;
    property Height: Single dispid 1610743826;
    property Width: Single dispid 1610743828;
    property angle: Single dispid 1610743830;
    property DrawArrows: WordBool dispid 1610743832;
    property EditFlags: CvsEditFlags dispid 1610743834;
    property EditBounds: {??Rectangle}OleVariant dispid 1610743836;
  end;

// *********************************************************************//
// Interface: _CvsGraphicEdgeProfile
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {B3BC696B-9B60-3175-89B0-A7CA174F43B3}
// *********************************************************************//
  _CvsGraphicEdgeProfile = interface(IDispatch)
    ['{B3BC696B-9B60-3175-89B0-A7CA174F43B3}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Clone: _CvsGraphic; safecall;
    function Get_CellLocation: CvsCellLocation; safecall;
    function Get_HighlightColorAsRgb: Integer; safecall;
    function Get_Label_: WideString; safecall;
    procedure Set_Label_(const pRetVal: WideString); safecall;
    function Get_IsAlwaysShown: WordBool; safecall;
    function Get_IsDefaultEdit: WordBool; safecall;
    function Get_IsLocked: WordBool; safecall;
    function GetDrawOrder: Integer; safecall;
    procedure add_PropertyChanged(const value: IUnknown); safecall;
    procedure remove_PropertyChanged(const value: IUnknown); safecall;
    function Get_PositiveThreshold: Integer; safecall;
    function Get_NegativeThreshold: Integer; safecall;
    function Get_NumEdgeValues: Integer; safecall;
    function GetEdgeValues: PSafeArray; safecall;
    property ToString: WideString read Get_ToString;
    property CellLocation: CvsCellLocation read Get_CellLocation;
    property HighlightColorAsRgb: Integer read Get_HighlightColorAsRgb;
    property Label_: WideString read Get_Label_ write Set_Label_;
    property IsAlwaysShown: WordBool read Get_IsAlwaysShown;
    property IsDefaultEdit: WordBool read Get_IsDefaultEdit;
    property IsLocked: WordBool read Get_IsLocked;
    property PositiveThreshold: Integer read Get_PositiveThreshold;
    property NegativeThreshold: Integer read Get_NegativeThreshold;
    property NumEdgeValues: Integer read Get_NumEdgeValues;
  end;

// *********************************************************************//
// DispIntf:  _CvsGraphicEdgeProfileDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {B3BC696B-9B60-3175-89B0-A7CA174F43B3}
// *********************************************************************//
  _CvsGraphicEdgeProfileDisp = dispinterface
    ['{B3BC696B-9B60-3175-89B0-A7CA174F43B3}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    function Clone: _CvsGraphic; dispid 1610743812;
    property CellLocation: {??CvsCellLocation}OleVariant readonly dispid 1610743813;
    property HighlightColorAsRgb: Integer readonly dispid 1610743814;
    property Label_: WideString dispid 1610743815;
    property IsAlwaysShown: WordBool readonly dispid 1610743817;
    property IsDefaultEdit: WordBool readonly dispid 1610743818;
    property IsLocked: WordBool readonly dispid 1610743819;
    function GetDrawOrder: Integer; dispid 1610743820;
    procedure add_PropertyChanged(const value: IUnknown); dispid 1610743821;
    procedure remove_PropertyChanged(const value: IUnknown); dispid 1610743822;
    property PositiveThreshold: Integer readonly dispid 1610743823;
    property NegativeThreshold: Integer readonly dispid 1610743824;
    property NumEdgeValues: Integer readonly dispid 1610743825;
    function GetEdgeValues: {??PSafeArray}OleVariant; dispid 1610743826;
  end;

// *********************************************************************//
// Interface: _CvsGraphicFixture
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {57E2E98E-DF90-3491-AB46-0CD215D0B0A3}
// *********************************************************************//
  _CvsGraphicFixture = interface(IDispatch)
    ['{57E2E98E-DF90-3491-AB46-0CD215D0B0A3}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Clone: _CvsGraphic; safecall;
    function Get_CellLocation: CvsCellLocation; safecall;
    function Get_HighlightColorAsRgb: Integer; safecall;
    function Get_Label_: WideString; safecall;
    procedure Set_Label_(const pRetVal: WideString); safecall;
    function Get_IsAlwaysShown: WordBool; safecall;
    function Get_IsDefaultEdit: WordBool; safecall;
    function Get_IsLocked: WordBool; safecall;
    function GetDrawOrder: Integer; safecall;
    procedure add_PropertyChanged(const value: IUnknown); safecall;
    procedure remove_PropertyChanged(const value: IUnknown); safecall;
    procedure Initialize(cellRow: Integer; cellCol: Integer; pointRow: Single; pointCol: Single; 
                         angle: Single; const Label_: WideString); safecall;
    function Get_Center: CvsImageLocation; safecall;
    procedure Set_Center(pRetVal: CvsImageLocation); safecall;
    function Get_Theta: Single; safecall;
    procedure Set_Theta(pRetVal: Single); safecall;
    function Get_EditFlags: CvsEditFlags; safecall;
    procedure Set_EditFlags(pRetVal: CvsEditFlags); safecall;
    function Get_EditBounds: Rectangle; safecall;
    procedure Set_EditBounds(pRetVal: Rectangle); safecall;
    property ToString: WideString read Get_ToString;
    property CellLocation: CvsCellLocation read Get_CellLocation;
    property HighlightColorAsRgb: Integer read Get_HighlightColorAsRgb;
    property Label_: WideString read Get_Label_ write Set_Label_;
    property IsAlwaysShown: WordBool read Get_IsAlwaysShown;
    property IsDefaultEdit: WordBool read Get_IsDefaultEdit;
    property IsLocked: WordBool read Get_IsLocked;
    property Center: CvsImageLocation read Get_Center write Set_Center;
    property Theta: Single read Get_Theta write Set_Theta;
    property EditFlags: CvsEditFlags read Get_EditFlags write Set_EditFlags;
    property EditBounds: Rectangle read Get_EditBounds write Set_EditBounds;
  end;

// *********************************************************************//
// DispIntf:  _CvsGraphicFixtureDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {57E2E98E-DF90-3491-AB46-0CD215D0B0A3}
// *********************************************************************//
  _CvsGraphicFixtureDisp = dispinterface
    ['{57E2E98E-DF90-3491-AB46-0CD215D0B0A3}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    function Clone: _CvsGraphic; dispid 1610743812;
    property CellLocation: {??CvsCellLocation}OleVariant readonly dispid 1610743813;
    property HighlightColorAsRgb: Integer readonly dispid 1610743814;
    property Label_: WideString dispid 1610743815;
    property IsAlwaysShown: WordBool readonly dispid 1610743817;
    property IsDefaultEdit: WordBool readonly dispid 1610743818;
    property IsLocked: WordBool readonly dispid 1610743819;
    function GetDrawOrder: Integer; dispid 1610743820;
    procedure add_PropertyChanged(const value: IUnknown); dispid 1610743821;
    procedure remove_PropertyChanged(const value: IUnknown); dispid 1610743822;
    procedure Initialize(cellRow: Integer; cellCol: Integer; pointRow: Single; pointCol: Single; 
                         angle: Single; const Label_: WideString); dispid 1610743823;
    property Center: {??CvsImageLocation}OleVariant dispid 1610743824;
    property Theta: Single dispid 1610743826;
    property EditFlags: CvsEditFlags dispid 1610743828;
    property EditBounds: {??Rectangle}OleVariant dispid 1610743830;
  end;

// *********************************************************************//
// Interface: _CvsGraphicHistogram
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {69A60F75-610B-3D38-996F-4ED080D0063B}
// *********************************************************************//
  _CvsGraphicHistogram = interface(IDispatch)
    ['{69A60F75-610B-3D38-996F-4ED080D0063B}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Clone: _CvsGraphic; safecall;
    function Get_CellLocation: CvsCellLocation; safecall;
    function Get_HighlightColorAsRgb: Integer; safecall;
    function Get_Label_: WideString; safecall;
    procedure Set_Label_(const pRetVal: WideString); safecall;
    function Get_IsAlwaysShown: WordBool; safecall;
    function Get_IsDefaultEdit: WordBool; safecall;
    function Get_IsLocked: WordBool; safecall;
    function GetDrawOrder: Integer; safecall;
    procedure add_PropertyChanged(const value: IUnknown); safecall;
    procedure remove_PropertyChanged(const value: IUnknown); safecall;
    function Get_Threshold: Integer; safecall;
    function GetHistogram: PSafeArray; safecall;
    property ToString: WideString read Get_ToString;
    property CellLocation: CvsCellLocation read Get_CellLocation;
    property HighlightColorAsRgb: Integer read Get_HighlightColorAsRgb;
    property Label_: WideString read Get_Label_ write Set_Label_;
    property IsAlwaysShown: WordBool read Get_IsAlwaysShown;
    property IsDefaultEdit: WordBool read Get_IsDefaultEdit;
    property IsLocked: WordBool read Get_IsLocked;
    property Threshold: Integer read Get_Threshold;
  end;

// *********************************************************************//
// DispIntf:  _CvsGraphicHistogramDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {69A60F75-610B-3D38-996F-4ED080D0063B}
// *********************************************************************//
  _CvsGraphicHistogramDisp = dispinterface
    ['{69A60F75-610B-3D38-996F-4ED080D0063B}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    function Clone: _CvsGraphic; dispid 1610743812;
    property CellLocation: {??CvsCellLocation}OleVariant readonly dispid 1610743813;
    property HighlightColorAsRgb: Integer readonly dispid 1610743814;
    property Label_: WideString dispid 1610743815;
    property IsAlwaysShown: WordBool readonly dispid 1610743817;
    property IsDefaultEdit: WordBool readonly dispid 1610743818;
    property IsLocked: WordBool readonly dispid 1610743819;
    function GetDrawOrder: Integer; dispid 1610743820;
    procedure add_PropertyChanged(const value: IUnknown); dispid 1610743821;
    procedure remove_PropertyChanged(const value: IUnknown); dispid 1610743822;
    property Threshold: Integer readonly dispid 1610743823;
    function GetHistogram: {??PSafeArray}OleVariant; dispid 1610743824;
  end;

// *********************************************************************//
// Interface: _CvsGraphicImage
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {A29F245B-AB2A-386B-8E7F-57BCA5BEB97E}
// *********************************************************************//
  _CvsGraphicImage = interface(IDispatch)
    ['{A29F245B-AB2A-386B-8E7F-57BCA5BEB97E}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Clone: _CvsGraphic; safecall;
    function Get_CellLocation: CvsCellLocation; safecall;
    function Get_HighlightColorAsRgb: Integer; safecall;
    function Get_Label_: WideString; safecall;
    procedure Set_Label_(const pRetVal: WideString); safecall;
    function Get_IsAlwaysShown: WordBool; safecall;
    function Get_IsDefaultEdit: WordBool; safecall;
    function Get_IsLocked: WordBool; safecall;
    function GetDrawOrder: Integer; safecall;
    procedure add_PropertyChanged(const value: IUnknown); safecall;
    procedure remove_PropertyChanged(const value: IUnknown); safecall;
    procedure Dispose; safecall;
    function Get_Image: _CvsImage; safecall;
    function Get_IsResultImage: WordBool; safecall;
    function Get_IsSourceImage: WordBool; safecall;
    property ToString: WideString read Get_ToString;
    property CellLocation: CvsCellLocation read Get_CellLocation;
    property HighlightColorAsRgb: Integer read Get_HighlightColorAsRgb;
    property Label_: WideString read Get_Label_ write Set_Label_;
    property IsAlwaysShown: WordBool read Get_IsAlwaysShown;
    property IsDefaultEdit: WordBool read Get_IsDefaultEdit;
    property IsLocked: WordBool read Get_IsLocked;
    property Image: _CvsImage read Get_Image;
    property IsResultImage: WordBool read Get_IsResultImage;
    property IsSourceImage: WordBool read Get_IsSourceImage;
  end;

// *********************************************************************//
// DispIntf:  _CvsGraphicImageDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {A29F245B-AB2A-386B-8E7F-57BCA5BEB97E}
// *********************************************************************//
  _CvsGraphicImageDisp = dispinterface
    ['{A29F245B-AB2A-386B-8E7F-57BCA5BEB97E}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    function Clone: _CvsGraphic; dispid 1610743812;
    property CellLocation: {??CvsCellLocation}OleVariant readonly dispid 1610743813;
    property HighlightColorAsRgb: Integer readonly dispid 1610743814;
    property Label_: WideString dispid 1610743815;
    property IsAlwaysShown: WordBool readonly dispid 1610743817;
    property IsDefaultEdit: WordBool readonly dispid 1610743818;
    property IsLocked: WordBool readonly dispid 1610743819;
    function GetDrawOrder: Integer; dispid 1610743820;
    procedure add_PropertyChanged(const value: IUnknown); dispid 1610743821;
    procedure remove_PropertyChanged(const value: IUnknown); dispid 1610743822;
    procedure Dispose; dispid 1610743823;
    property Image: _CvsImage readonly dispid 1610743824;
    property IsResultImage: WordBool readonly dispid 1610743825;
    property IsSourceImage: WordBool readonly dispid 1610743826;
  end;

// *********************************************************************//
// Interface: _CvsGraphicBlobProperties
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {D4FD6383-1901-3F66-A9E8-AB13CFE9084D}
// *********************************************************************//
  _CvsGraphicBlobProperties = interface(IDispatch)
    ['{D4FD6383-1901-3F66-A9E8-AB13CFE9084D}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Clone: _CvsGraphic; safecall;
    function Get_CellLocation: CvsCellLocation; safecall;
    function Get_HighlightColorAsRgb: Integer; safecall;
    function Get_Label_: WideString; safecall;
    procedure Set_Label_(const pRetVal: WideString); safecall;
    function Get_IsAlwaysShown: WordBool; safecall;
    function Get_IsDefaultEdit: WordBool; safecall;
    function Get_IsLocked: WordBool; safecall;
    function GetDrawOrder: Integer; safecall;
    procedure add_PropertyChanged(const value: IUnknown); safecall;
    procedure remove_PropertyChanged(const value: IUnknown); safecall;
    function Get_Location: CvsImageLocation; safecall;
    function Get_BlobNumber: Integer; safecall;
    function Get_NumPoints: Integer; safecall;
    function GetPoints: PSafeArray; safecall;
    function Get_Centroid: CvsImageLocation; safecall;
    function Get_angle: Single; safecall;
    function Get_Area: Integer; safecall;
    function Get_Elongation: Single; safecall;
    function Get_Hole: Integer; safecall;
    function Get_Perimeter: Integer; safecall;
    function Get_Spread: Single; safecall;
    property ToString: WideString read Get_ToString;
    property CellLocation: CvsCellLocation read Get_CellLocation;
    property HighlightColorAsRgb: Integer read Get_HighlightColorAsRgb;
    property Label_: WideString read Get_Label_ write Set_Label_;
    property IsAlwaysShown: WordBool read Get_IsAlwaysShown;
    property IsDefaultEdit: WordBool read Get_IsDefaultEdit;
    property IsLocked: WordBool read Get_IsLocked;
    property Location: CvsImageLocation read Get_Location;
    property BlobNumber: Integer read Get_BlobNumber;
    property NumPoints: Integer read Get_NumPoints;
    property Centroid: CvsImageLocation read Get_Centroid;
    property angle: Single read Get_angle;
    property Area: Integer read Get_Area;
    property Elongation: Single read Get_Elongation;
    property Hole: Integer read Get_Hole;
    property Perimeter: Integer read Get_Perimeter;
    property Spread: Single read Get_Spread;
  end;

// *********************************************************************//
// DispIntf:  _CvsGraphicBlobPropertiesDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {D4FD6383-1901-3F66-A9E8-AB13CFE9084D}
// *********************************************************************//
  _CvsGraphicBlobPropertiesDisp = dispinterface
    ['{D4FD6383-1901-3F66-A9E8-AB13CFE9084D}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    function Clone: _CvsGraphic; dispid 1610743812;
    property CellLocation: {??CvsCellLocation}OleVariant readonly dispid 1610743813;
    property HighlightColorAsRgb: Integer readonly dispid 1610743814;
    property Label_: WideString dispid 1610743815;
    property IsAlwaysShown: WordBool readonly dispid 1610743817;
    property IsDefaultEdit: WordBool readonly dispid 1610743818;
    property IsLocked: WordBool readonly dispid 1610743819;
    function GetDrawOrder: Integer; dispid 1610743820;
    procedure add_PropertyChanged(const value: IUnknown); dispid 1610743821;
    procedure remove_PropertyChanged(const value: IUnknown); dispid 1610743822;
    property Location: {??CvsImageLocation}OleVariant readonly dispid 1610743823;
    property BlobNumber: Integer readonly dispid 1610743824;
    property NumPoints: Integer readonly dispid 1610743825;
    function GetPoints: {??PSafeArray}OleVariant; dispid 1610743826;
    property Centroid: {??CvsImageLocation}OleVariant readonly dispid 1610743827;
    property angle: Single readonly dispid 1610743828;
    property Area: Integer readonly dispid 1610743829;
    property Elongation: Single readonly dispid 1610743830;
    property Hole: Integer readonly dispid 1610743831;
    property Perimeter: Integer readonly dispid 1610743832;
    property Spread: Single readonly dispid 1610743833;
  end;

// *********************************************************************//
// Interface: _CvsGraphicLine
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {81F6EF11-1064-3CC3-8B2E-88C3C4D1A4CD}
// *********************************************************************//
  _CvsGraphicLine = interface(IDispatch)
    ['{81F6EF11-1064-3CC3-8B2E-88C3C4D1A4CD}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Clone: _CvsGraphic; safecall;
    function Get_CellLocation: CvsCellLocation; safecall;
    function Get_HighlightColorAsRgb: Integer; safecall;
    function Get_Label_: WideString; safecall;
    procedure Set_Label_(const pRetVal: WideString); safecall;
    function Get_IsAlwaysShown: WordBool; safecall;
    function Get_IsDefaultEdit: WordBool; safecall;
    function Get_IsLocked: WordBool; safecall;
    function GetDrawOrder: Integer; safecall;
    procedure add_PropertyChanged(const value: IUnknown); safecall;
    procedure remove_PropertyChanged(const value: IUnknown); safecall;
    procedure Initialize(cellRow: Integer; cellCol: Integer; startRow: Single; startCol: Single; 
                         endRow: Single; endCol: Single; const Label_: WideString); safecall;
    procedure Initialize_2(cellRow: Integer; cellCol: Integer; startRow: Single; startCol: Single; 
                           endRow: Single; endCol: Single; const Label_: WideString; 
                           startPointAdornment: CvsGraphicLineAdornment; 
                           endPointAdornment: CvsGraphicLineAdornment); safecall;
    function Get_StartPoint: CvsImageLocation; safecall;
    procedure Set_StartPoint(pRetVal: CvsImageLocation); safecall;
    function Get_EndPoint: CvsImageLocation; safecall;
    procedure Set_EndPoint(pRetVal: CvsImageLocation); safecall;
    function Get_startPointAdornment: CvsGraphicLineAdornment; safecall;
    function Get_endPointAdornment: CvsGraphicLineAdornment; safecall;
    function Get_EditFlags: CvsEditFlags; safecall;
    procedure Set_EditFlags(pRetVal: CvsEditFlags); safecall;
    function Get_EditBounds: Rectangle; safecall;
    procedure Set_EditBounds(pRetVal: Rectangle); safecall;
    property ToString: WideString read Get_ToString;
    property CellLocation: CvsCellLocation read Get_CellLocation;
    property HighlightColorAsRgb: Integer read Get_HighlightColorAsRgb;
    property Label_: WideString read Get_Label_ write Set_Label_;
    property IsAlwaysShown: WordBool read Get_IsAlwaysShown;
    property IsDefaultEdit: WordBool read Get_IsDefaultEdit;
    property IsLocked: WordBool read Get_IsLocked;
    property StartPoint: CvsImageLocation read Get_StartPoint write Set_StartPoint;
    property EndPoint: CvsImageLocation read Get_EndPoint write Set_EndPoint;
    property startPointAdornment: CvsGraphicLineAdornment read Get_startPointAdornment;
    property endPointAdornment: CvsGraphicLineAdornment read Get_endPointAdornment;
    property EditFlags: CvsEditFlags read Get_EditFlags write Set_EditFlags;
    property EditBounds: Rectangle read Get_EditBounds write Set_EditBounds;
  end;

// *********************************************************************//
// DispIntf:  _CvsGraphicLineDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {81F6EF11-1064-3CC3-8B2E-88C3C4D1A4CD}
// *********************************************************************//
  _CvsGraphicLineDisp = dispinterface
    ['{81F6EF11-1064-3CC3-8B2E-88C3C4D1A4CD}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    function Clone: _CvsGraphic; dispid 1610743812;
    property CellLocation: {??CvsCellLocation}OleVariant readonly dispid 1610743813;
    property HighlightColorAsRgb: Integer readonly dispid 1610743814;
    property Label_: WideString dispid 1610743815;
    property IsAlwaysShown: WordBool readonly dispid 1610743817;
    property IsDefaultEdit: WordBool readonly dispid 1610743818;
    property IsLocked: WordBool readonly dispid 1610743819;
    function GetDrawOrder: Integer; dispid 1610743820;
    procedure add_PropertyChanged(const value: IUnknown); dispid 1610743821;
    procedure remove_PropertyChanged(const value: IUnknown); dispid 1610743822;
    procedure Initialize(cellRow: Integer; cellCol: Integer; startRow: Single; startCol: Single; 
                         endRow: Single; endCol: Single; const Label_: WideString); dispid 1610743823;
    procedure Initialize_2(cellRow: Integer; cellCol: Integer; startRow: Single; startCol: Single; 
                           endRow: Single; endCol: Single; const Label_: WideString; 
                           startPointAdornment: CvsGraphicLineAdornment; 
                           endPointAdornment: CvsGraphicLineAdornment); dispid 1610743824;
    property StartPoint: {??CvsImageLocation}OleVariant dispid 1610743825;
    property EndPoint: {??CvsImageLocation}OleVariant dispid 1610743827;
    property startPointAdornment: CvsGraphicLineAdornment readonly dispid 1610743829;
    property endPointAdornment: CvsGraphicLineAdornment readonly dispid 1610743830;
    property EditFlags: CvsEditFlags dispid 1610743831;
    property EditBounds: {??Rectangle}OleVariant dispid 1610743833;
  end;

// *********************************************************************//
// Interface: _CvsGraphicOffset
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {21B97C6C-8737-3008-A1D3-DD2DE976FEBE}
// *********************************************************************//
  _CvsGraphicOffset = interface(IDispatch)
    ['{21B97C6C-8737-3008-A1D3-DD2DE976FEBE}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Clone: _CvsGraphic; safecall;
    function Get_CellLocation: CvsCellLocation; safecall;
    function Get_HighlightColorAsRgb: Integer; safecall;
    function Get_Label_: WideString; safecall;
    procedure Set_Label_(const pRetVal: WideString); safecall;
    function Get_IsAlwaysShown: WordBool; safecall;
    function Get_IsDefaultEdit: WordBool; safecall;
    function Get_IsLocked: WordBool; safecall;
    function GetDrawOrder: Integer; safecall;
    procedure add_PropertyChanged(const value: IUnknown); safecall;
    procedure remove_PropertyChanged(const value: IUnknown); safecall;
    procedure Initialize(cellRow: Integer; cellCol: Integer; offsetRow: Single; offsetCol: Single; 
                         originRow: Single; originCol: Single; angle: Single; 
                         const Label_: WideString); safecall;
    function Get_Offset: CvsImageLocation; safecall;
    procedure Set_Offset(pRetVal: CvsImageLocation); safecall;
    function Get_Origin: CvsImageLocation; safecall;
    procedure Set_Origin(pRetVal: CvsImageLocation); safecall;
    function Get_angle: Single; safecall;
    procedure Set_angle(pRetVal: Single); safecall;
    function Get_EditFlags: CvsEditFlags; safecall;
    procedure Set_EditFlags(pRetVal: CvsEditFlags); safecall;
    function Get_EditBounds: Rectangle; safecall;
    procedure Set_EditBounds(pRetVal: Rectangle); safecall;
    property ToString: WideString read Get_ToString;
    property CellLocation: CvsCellLocation read Get_CellLocation;
    property HighlightColorAsRgb: Integer read Get_HighlightColorAsRgb;
    property Label_: WideString read Get_Label_ write Set_Label_;
    property IsAlwaysShown: WordBool read Get_IsAlwaysShown;
    property IsDefaultEdit: WordBool read Get_IsDefaultEdit;
    property IsLocked: WordBool read Get_IsLocked;
    property Offset: CvsImageLocation read Get_Offset write Set_Offset;
    property Origin: CvsImageLocation read Get_Origin write Set_Origin;
    property angle: Single read Get_angle write Set_angle;
    property EditFlags: CvsEditFlags read Get_EditFlags write Set_EditFlags;
    property EditBounds: Rectangle read Get_EditBounds write Set_EditBounds;
  end;

// *********************************************************************//
// DispIntf:  _CvsGraphicOffsetDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {21B97C6C-8737-3008-A1D3-DD2DE976FEBE}
// *********************************************************************//
  _CvsGraphicOffsetDisp = dispinterface
    ['{21B97C6C-8737-3008-A1D3-DD2DE976FEBE}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    function Clone: _CvsGraphic; dispid 1610743812;
    property CellLocation: {??CvsCellLocation}OleVariant readonly dispid 1610743813;
    property HighlightColorAsRgb: Integer readonly dispid 1610743814;
    property Label_: WideString dispid 1610743815;
    property IsAlwaysShown: WordBool readonly dispid 1610743817;
    property IsDefaultEdit: WordBool readonly dispid 1610743818;
    property IsLocked: WordBool readonly dispid 1610743819;
    function GetDrawOrder: Integer; dispid 1610743820;
    procedure add_PropertyChanged(const value: IUnknown); dispid 1610743821;
    procedure remove_PropertyChanged(const value: IUnknown); dispid 1610743822;
    procedure Initialize(cellRow: Integer; cellCol: Integer; offsetRow: Single; offsetCol: Single; 
                         originRow: Single; originCol: Single; angle: Single; 
                         const Label_: WideString); dispid 1610743823;
    property Offset: {??CvsImageLocation}OleVariant dispid 1610743824;
    property Origin: {??CvsImageLocation}OleVariant dispid 1610743826;
    property angle: Single dispid 1610743828;
    property EditFlags: CvsEditFlags dispid 1610743830;
    property EditBounds: {??Rectangle}OleVariant dispid 1610743832;
  end;

// *********************************************************************//
// Interface: _CvsGraphicPoint
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {67EC08CE-1642-3932-996B-9AE54B5D6D49}
// *********************************************************************//
  _CvsGraphicPoint = interface(IDispatch)
    ['{67EC08CE-1642-3932-996B-9AE54B5D6D49}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Clone: _CvsGraphic; safecall;
    function Get_CellLocation: CvsCellLocation; safecall;
    function Get_HighlightColorAsRgb: Integer; safecall;
    function Get_Label_: WideString; safecall;
    procedure Set_Label_(const pRetVal: WideString); safecall;
    function Get_IsAlwaysShown: WordBool; safecall;
    function Get_IsDefaultEdit: WordBool; safecall;
    function Get_IsLocked: WordBool; safecall;
    function GetDrawOrder: Integer; safecall;
    procedure add_PropertyChanged(const value: IUnknown); safecall;
    procedure remove_PropertyChanged(const value: IUnknown); safecall;
    procedure Initialize(cellRow: Integer; cellCol: Integer; pointRow: Single; pointCol: Single; 
                         const Label_: WideString); safecall;
    function Get_Location: CvsImageLocation; safecall;
    procedure Set_Location(pRetVal: CvsImageLocation); safecall;
    function Get_EditFlags: CvsEditFlags; safecall;
    procedure Set_EditFlags(pRetVal: CvsEditFlags); safecall;
    function Get_EditBounds: Rectangle; safecall;
    procedure Set_EditBounds(pRetVal: Rectangle); safecall;
    property ToString: WideString read Get_ToString;
    property CellLocation: CvsCellLocation read Get_CellLocation;
    property HighlightColorAsRgb: Integer read Get_HighlightColorAsRgb;
    property Label_: WideString read Get_Label_ write Set_Label_;
    property IsAlwaysShown: WordBool read Get_IsAlwaysShown;
    property IsDefaultEdit: WordBool read Get_IsDefaultEdit;
    property IsLocked: WordBool read Get_IsLocked;
    property Location: CvsImageLocation read Get_Location write Set_Location;
    property EditFlags: CvsEditFlags read Get_EditFlags write Set_EditFlags;
    property EditBounds: Rectangle read Get_EditBounds write Set_EditBounds;
  end;

// *********************************************************************//
// DispIntf:  _CvsGraphicPointDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {67EC08CE-1642-3932-996B-9AE54B5D6D49}
// *********************************************************************//
  _CvsGraphicPointDisp = dispinterface
    ['{67EC08CE-1642-3932-996B-9AE54B5D6D49}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    function Clone: _CvsGraphic; dispid 1610743812;
    property CellLocation: {??CvsCellLocation}OleVariant readonly dispid 1610743813;
    property HighlightColorAsRgb: Integer readonly dispid 1610743814;
    property Label_: WideString dispid 1610743815;
    property IsAlwaysShown: WordBool readonly dispid 1610743817;
    property IsDefaultEdit: WordBool readonly dispid 1610743818;
    property IsLocked: WordBool readonly dispid 1610743819;
    function GetDrawOrder: Integer; dispid 1610743820;
    procedure add_PropertyChanged(const value: IUnknown); dispid 1610743821;
    procedure remove_PropertyChanged(const value: IUnknown); dispid 1610743822;
    procedure Initialize(cellRow: Integer; cellCol: Integer; pointRow: Single; pointCol: Single; 
                         const Label_: WideString); dispid 1610743823;
    property Location: {??CvsImageLocation}OleVariant dispid 1610743824;
    property EditFlags: CvsEditFlags dispid 1610743826;
    property EditBounds: {??Rectangle}OleVariant dispid 1610743828;
  end;

// *********************************************************************//
// Interface: _CvsGraphicPolyline
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {D32A3B3A-1DE5-3094-8D04-9901B2422E07}
// *********************************************************************//
  _CvsGraphicPolyline = interface(IDispatch)
    ['{D32A3B3A-1DE5-3094-8D04-9901B2422E07}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Clone: _CvsGraphic; safecall;
    function Get_CellLocation: CvsCellLocation; safecall;
    function Get_HighlightColorAsRgb: Integer; safecall;
    function Get_Label_: WideString; safecall;
    procedure Set_Label_(const pRetVal: WideString); safecall;
    function Get_IsAlwaysShown: WordBool; safecall;
    function Get_IsDefaultEdit: WordBool; safecall;
    function Get_IsLocked: WordBool; safecall;
    function GetDrawOrder: Integer; safecall;
    procedure add_PropertyChanged(const value: IUnknown); safecall;
    procedure remove_PropertyChanged(const value: IUnknown); safecall;
    procedure Initialize(cellRow: Integer; cellCol: Integer; Points: PSafeArray); safecall;
    function Get_NumPoints: Integer; safecall;
    function GetPoints: PSafeArray; safecall;
    property ToString: WideString read Get_ToString;
    property CellLocation: CvsCellLocation read Get_CellLocation;
    property HighlightColorAsRgb: Integer read Get_HighlightColorAsRgb;
    property Label_: WideString read Get_Label_ write Set_Label_;
    property IsAlwaysShown: WordBool read Get_IsAlwaysShown;
    property IsDefaultEdit: WordBool read Get_IsDefaultEdit;
    property IsLocked: WordBool read Get_IsLocked;
    property NumPoints: Integer read Get_NumPoints;
  end;

// *********************************************************************//
// DispIntf:  _CvsGraphicPolylineDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {D32A3B3A-1DE5-3094-8D04-9901B2422E07}
// *********************************************************************//
  _CvsGraphicPolylineDisp = dispinterface
    ['{D32A3B3A-1DE5-3094-8D04-9901B2422E07}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    function Clone: _CvsGraphic; dispid 1610743812;
    property CellLocation: {??CvsCellLocation}OleVariant readonly dispid 1610743813;
    property HighlightColorAsRgb: Integer readonly dispid 1610743814;
    property Label_: WideString dispid 1610743815;
    property IsAlwaysShown: WordBool readonly dispid 1610743817;
    property IsDefaultEdit: WordBool readonly dispid 1610743818;
    property IsLocked: WordBool readonly dispid 1610743819;
    function GetDrawOrder: Integer; dispid 1610743820;
    procedure add_PropertyChanged(const value: IUnknown); dispid 1610743821;
    procedure remove_PropertyChanged(const value: IUnknown); dispid 1610743822;
    procedure Initialize(cellRow: Integer; cellCol: Integer; Points: {??PSafeArray}OleVariant); dispid 1610743823;
    property NumPoints: Integer readonly dispid 1610743824;
    function GetPoints: {??PSafeArray}OleVariant; dispid 1610743825;
  end;

// *********************************************************************//
// Interface: _CvsGraphicRegion
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {24393BC4-5E11-35E2-8822-1F3B0096E4E6}
// *********************************************************************//
  _CvsGraphicRegion = interface(IDispatch)
    ['{24393BC4-5E11-35E2-8822-1F3B0096E4E6}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Clone: _CvsGraphic; safecall;
    function Get_CellLocation: CvsCellLocation; safecall;
    function Get_HighlightColorAsRgb: Integer; safecall;
    function Get_Label_: WideString; safecall;
    procedure Set_Label_(const pRetVal: WideString); safecall;
    function Get_IsAlwaysShown: WordBool; safecall;
    function Get_IsDefaultEdit: WordBool; safecall;
    function Get_IsLocked: WordBool; safecall;
    function GetDrawOrder: Integer; safecall;
    procedure add_PropertyChanged(const value: IUnknown); safecall;
    procedure remove_PropertyChanged(const value: IUnknown); safecall;
    procedure Initialize(cellRow: Integer; cellCol: Integer; regionRow: Single; regionCol: Single; 
                         regionHeight: Single; regionWidth: Single; regionAngle: Single; 
                         regionCurve: Single; const regionLabel: WideString); safecall;
    function Get_Location: CvsImageLocation; safecall;
    procedure Set_Location(pRetVal: CvsImageLocation); safecall;
    function Get_Height: Single; safecall;
    procedure Set_Height(pRetVal: Single); safecall;
    function Get_Width: Single; safecall;
    procedure Set_Width(pRetVal: Single); safecall;
    function Get_angle: Single; safecall;
    procedure Set_angle(pRetVal: Single); safecall;
    function Get_Curve: Single; safecall;
    procedure Set_Curve(pRetVal: Single); safecall;
    function Get_LabelForeColorAsRgb: Integer; safecall;
    procedure Set_LabelForeColorAsRgb(pRetVal: Integer); safecall;
    function Get_LabelBackColorAsRgb: Integer; safecall;
    procedure Set_LabelBackColorAsRgb(pRetVal: Integer); safecall;
    function Get_DisplayAxisLabels: WordBool; safecall;
    procedure Set_DisplayAxisLabels(pRetVal: WordBool); safecall;
    function Get_EditFlags: CvsEditFlags; safecall;
    procedure Set_EditFlags(pRetVal: CvsEditFlags); safecall;
    function Get_EditBounds: Rectangle; safecall;
    procedure Set_EditBounds(pRetVal: Rectangle); safecall;
    property ToString: WideString read Get_ToString;
    property CellLocation: CvsCellLocation read Get_CellLocation;
    property HighlightColorAsRgb: Integer read Get_HighlightColorAsRgb;
    property Label_: WideString read Get_Label_ write Set_Label_;
    property IsAlwaysShown: WordBool read Get_IsAlwaysShown;
    property IsDefaultEdit: WordBool read Get_IsDefaultEdit;
    property IsLocked: WordBool read Get_IsLocked;
    property Location: CvsImageLocation read Get_Location write Set_Location;
    property Height: Single read Get_Height write Set_Height;
    property Width: Single read Get_Width write Set_Width;
    property angle: Single read Get_angle write Set_angle;
    property Curve: Single read Get_Curve write Set_Curve;
    property LabelForeColorAsRgb: Integer read Get_LabelForeColorAsRgb write Set_LabelForeColorAsRgb;
    property LabelBackColorAsRgb: Integer read Get_LabelBackColorAsRgb write Set_LabelBackColorAsRgb;
    property DisplayAxisLabels: WordBool read Get_DisplayAxisLabels write Set_DisplayAxisLabels;
    property EditFlags: CvsEditFlags read Get_EditFlags write Set_EditFlags;
    property EditBounds: Rectangle read Get_EditBounds write Set_EditBounds;
  end;

// *********************************************************************//
// DispIntf:  _CvsGraphicRegionDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {24393BC4-5E11-35E2-8822-1F3B0096E4E6}
// *********************************************************************//
  _CvsGraphicRegionDisp = dispinterface
    ['{24393BC4-5E11-35E2-8822-1F3B0096E4E6}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    function Clone: _CvsGraphic; dispid 1610743812;
    property CellLocation: {??CvsCellLocation}OleVariant readonly dispid 1610743813;
    property HighlightColorAsRgb: Integer readonly dispid 1610743814;
    property Label_: WideString dispid 1610743815;
    property IsAlwaysShown: WordBool readonly dispid 1610743817;
    property IsDefaultEdit: WordBool readonly dispid 1610743818;
    property IsLocked: WordBool readonly dispid 1610743819;
    function GetDrawOrder: Integer; dispid 1610743820;
    procedure add_PropertyChanged(const value: IUnknown); dispid 1610743821;
    procedure remove_PropertyChanged(const value: IUnknown); dispid 1610743822;
    procedure Initialize(cellRow: Integer; cellCol: Integer; regionRow: Single; regionCol: Single; 
                         regionHeight: Single; regionWidth: Single; regionAngle: Single; 
                         regionCurve: Single; const regionLabel: WideString); dispid 1610743823;
    property Location: {??CvsImageLocation}OleVariant dispid 1610743824;
    property Height: Single dispid 1610743826;
    property Width: Single dispid 1610743828;
    property angle: Single dispid 1610743830;
    property Curve: Single dispid 1610743832;
    property LabelForeColorAsRgb: Integer dispid 1610743834;
    property LabelBackColorAsRgb: Integer dispid 1610743836;
    property DisplayAxisLabels: WordBool dispid 1610743838;
    property EditFlags: CvsEditFlags dispid 1610743840;
    property EditBounds: {??Rectangle}OleVariant dispid 1610743842;
  end;

// *********************************************************************//
// Interface: _CvsGraphicText
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {C5E456AF-7D37-3A44-AD6A-CE25A92DB9A7}
// *********************************************************************//
  _CvsGraphicText = interface(IDispatch)
    ['{C5E456AF-7D37-3A44-AD6A-CE25A92DB9A7}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Clone: _CvsGraphic; safecall;
    function Get_CellLocation: CvsCellLocation; safecall;
    function Get_HighlightColorAsRgb: Integer; safecall;
    function Get_Label_: WideString; safecall;
    procedure Set_Label_(const pRetVal: WideString); safecall;
    function Get_IsAlwaysShown: WordBool; safecall;
    function Get_IsDefaultEdit: WordBool; safecall;
    function Get_IsLocked: WordBool; safecall;
    function GetDrawOrder: Integer; safecall;
    procedure add_PropertyChanged(const value: IUnknown); safecall;
    procedure remove_PropertyChanged(const value: IUnknown); safecall;
    procedure Initialize(cellRow: Integer; cellCol: Integer; textRow: Single; textCol: Single; 
                         const Label_: WideString); safecall;
    function Get_Location: CvsImageLocation; safecall;
    procedure Set_Location(pRetVal: CvsImageLocation); safecall;
    function Get_BackColorAsRgb: Integer; safecall;
    procedure Set_BackColorAsRgb(pRetVal: Integer); safecall;
    function Get_IsPlotString: WordBool; safecall;
    property ToString: WideString read Get_ToString;
    property CellLocation: CvsCellLocation read Get_CellLocation;
    property HighlightColorAsRgb: Integer read Get_HighlightColorAsRgb;
    property Label_: WideString read Get_Label_ write Set_Label_;
    property IsAlwaysShown: WordBool read Get_IsAlwaysShown;
    property IsDefaultEdit: WordBool read Get_IsDefaultEdit;
    property IsLocked: WordBool read Get_IsLocked;
    property Location: CvsImageLocation read Get_Location write Set_Location;
    property BackColorAsRgb: Integer read Get_BackColorAsRgb write Set_BackColorAsRgb;
    property IsPlotString: WordBool read Get_IsPlotString;
  end;

// *********************************************************************//
// DispIntf:  _CvsGraphicTextDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {C5E456AF-7D37-3A44-AD6A-CE25A92DB9A7}
// *********************************************************************//
  _CvsGraphicTextDisp = dispinterface
    ['{C5E456AF-7D37-3A44-AD6A-CE25A92DB9A7}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    function Clone: _CvsGraphic; dispid 1610743812;
    property CellLocation: {??CvsCellLocation}OleVariant readonly dispid 1610743813;
    property HighlightColorAsRgb: Integer readonly dispid 1610743814;
    property Label_: WideString dispid 1610743815;
    property IsAlwaysShown: WordBool readonly dispid 1610743817;
    property IsDefaultEdit: WordBool readonly dispid 1610743818;
    property IsLocked: WordBool readonly dispid 1610743819;
    function GetDrawOrder: Integer; dispid 1610743820;
    procedure add_PropertyChanged(const value: IUnknown); dispid 1610743821;
    procedure remove_PropertyChanged(const value: IUnknown); dispid 1610743822;
    procedure Initialize(cellRow: Integer; cellCol: Integer; textRow: Single; textCol: Single; 
                         const Label_: WideString); dispid 1610743823;
    property Location: {??CvsImageLocation}OleVariant dispid 1610743824;
    property BackColorAsRgb: Integer dispid 1610743826;
    property IsPlotString: WordBool readonly dispid 1610743828;
  end;

// *********************************************************************//
// Interface: _CvsGraphicAnnulus
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {41CB8318-8E53-3810-8207-FF772A15ACF5}
// *********************************************************************//
  _CvsGraphicAnnulus = interface(IDispatch)
    ['{41CB8318-8E53-3810-8207-FF772A15ACF5}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Clone: _CvsGraphic; safecall;
    function Get_CellLocation: CvsCellLocation; safecall;
    function Get_HighlightColorAsRgb: Integer; safecall;
    function Get_Label_: WideString; safecall;
    procedure Set_Label_(const pRetVal: WideString); safecall;
    function Get_IsAlwaysShown: WordBool; safecall;
    function Get_IsDefaultEdit: WordBool; safecall;
    function Get_IsLocked: WordBool; safecall;
    function GetDrawOrder: Integer; safecall;
    procedure add_PropertyChanged(const value: IUnknown); safecall;
    procedure remove_PropertyChanged(const value: IUnknown); safecall;
    procedure Initialize(cellRow: Integer; cellCol: Integer; centerRow: Single; centerCol: Single; 
                         InnerRadius: Single; OuterRadius: Single; const Label_: WideString); safecall;
    function Get_Center: CvsImageLocation; safecall;
    procedure Set_Center(pRetVal: CvsImageLocation); safecall;
    function Get_InnerRadius: Single; safecall;
    procedure Set_InnerRadius(pRetVal: Single); safecall;
    function Get_OuterRadius: Single; safecall;
    procedure Set_OuterRadius(pRetVal: Single); safecall;
    function Get_EditFlags: CvsEditFlags; safecall;
    procedure Set_EditFlags(pRetVal: CvsEditFlags); safecall;
    function Get_EditBounds: Rectangle; safecall;
    procedure Set_EditBounds(pRetVal: Rectangle); safecall;
    property ToString: WideString read Get_ToString;
    property CellLocation: CvsCellLocation read Get_CellLocation;
    property HighlightColorAsRgb: Integer read Get_HighlightColorAsRgb;
    property Label_: WideString read Get_Label_ write Set_Label_;
    property IsAlwaysShown: WordBool read Get_IsAlwaysShown;
    property IsDefaultEdit: WordBool read Get_IsDefaultEdit;
    property IsLocked: WordBool read Get_IsLocked;
    property Center: CvsImageLocation read Get_Center write Set_Center;
    property InnerRadius: Single read Get_InnerRadius write Set_InnerRadius;
    property OuterRadius: Single read Get_OuterRadius write Set_OuterRadius;
    property EditFlags: CvsEditFlags read Get_EditFlags write Set_EditFlags;
    property EditBounds: Rectangle read Get_EditBounds write Set_EditBounds;
  end;

// *********************************************************************//
// DispIntf:  _CvsGraphicAnnulusDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {41CB8318-8E53-3810-8207-FF772A15ACF5}
// *********************************************************************//
  _CvsGraphicAnnulusDisp = dispinterface
    ['{41CB8318-8E53-3810-8207-FF772A15ACF5}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    function Clone: _CvsGraphic; dispid 1610743812;
    property CellLocation: {??CvsCellLocation}OleVariant readonly dispid 1610743813;
    property HighlightColorAsRgb: Integer readonly dispid 1610743814;
    property Label_: WideString dispid 1610743815;
    property IsAlwaysShown: WordBool readonly dispid 1610743817;
    property IsDefaultEdit: WordBool readonly dispid 1610743818;
    property IsLocked: WordBool readonly dispid 1610743819;
    function GetDrawOrder: Integer; dispid 1610743820;
    procedure add_PropertyChanged(const value: IUnknown); dispid 1610743821;
    procedure remove_PropertyChanged(const value: IUnknown); dispid 1610743822;
    procedure Initialize(cellRow: Integer; cellCol: Integer; centerRow: Single; centerCol: Single; 
                         InnerRadius: Single; OuterRadius: Single; const Label_: WideString); dispid 1610743823;
    property Center: {??CvsImageLocation}OleVariant dispid 1610743824;
    property InnerRadius: Single dispid 1610743826;
    property OuterRadius: Single dispid 1610743828;
    property EditFlags: CvsEditFlags dispid 1610743830;
    property EditBounds: {??Rectangle}OleVariant dispid 1610743832;
  end;

// *********************************************************************//
// Interface: _CvsHostTableCollection
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {786467CE-A694-367D-8348-A1113C179602}
// *********************************************************************//
  _CvsHostTableCollection = interface(IDispatch)
    ['{786467CE-A694-367D-8348-A1113C179602}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function GetEnumerator: IEnumVARIANT; safecall;
    function Clone: OleVariant; safecall;
    function Get_Count: Integer; safecall;
    function Get_Item(index: Integer): _CvsHost; safecall;
    function Get_Item_2(const Name: WideString): _CvsHostTableEntry; safecall;
    procedure Add(const host: _CvsHost); safecall;
    procedure Remove(const host: _CvsHost); safecall;
    procedure Clear; safecall;
    function Get_Items: PSafeArray; safecall;
    property ToString: WideString read Get_ToString;
    property Count: Integer read Get_Count;
    property Item[index: Integer]: _CvsHost read Get_Item; default;
    property Item_2[const Name: WideString]: _CvsHostTableEntry read Get_Item_2;
    property Items: PSafeArray read Get_Items;
  end;

// *********************************************************************//
// DispIntf:  _CvsHostTableCollectionDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {786467CE-A694-367D-8348-A1113C179602}
// *********************************************************************//
  _CvsHostTableCollectionDisp = dispinterface
    ['{786467CE-A694-367D-8348-A1113C179602}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 1610743810;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    function GetEnumerator: IEnumVARIANT; dispid -4;
    function Clone: OleVariant; dispid 1610743813;
    property Count: Integer readonly dispid 1610743814;
    property Item[index: Integer]: _CvsHost readonly dispid 0; default;
    property Item_2[const Name: WideString]: _CvsHostTableEntry readonly dispid 1610743816;
    procedure Add(const host: _CvsHost); dispid 1610743817;
    procedure Remove(const host: _CvsHost); dispid 1610743818;
    procedure Clear; dispid 1610743819;
    property Items: {??PSafeArray}OleVariant readonly dispid 1610743820;
  end;

// *********************************************************************//
// Interface: _CvsHostTableEntry
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {A672A610-C97A-380C-AD06-CCDC99192EF1}
// *********************************************************************//
  _CvsHostTableEntry = interface(IDispatch)
    ['{A672A610-C97A-380C-AD06-CCDC99192EF1}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Matches(const host: _CvsHost): WordBool; safecall;
    function Get_Name: WideString; safecall;
    function Get_IPAddressString: WideString; safecall;
    function Get_type_: CvsHostType; safecall;
    function Get_Subtype: CvsHostSubtype; safecall;
    procedure SetName(const Name: WideString); safecall;
    procedure SetIPAddressString(const IPAddress: WideString); safecall;
    property ToString: WideString read Get_ToString;
    property Name: WideString read Get_Name;
    property IPAddressString: WideString read Get_IPAddressString;
    property type_: CvsHostType read Get_type_;
    property Subtype: CvsHostSubtype read Get_Subtype;
  end;

// *********************************************************************//
// DispIntf:  _CvsHostTableEntryDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {A672A610-C97A-380C-AD06-CCDC99192EF1}
// *********************************************************************//
  _CvsHostTableEntryDisp = dispinterface
    ['{A672A610-C97A-380C-AD06-CCDC99192EF1}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    function Matches(const host: _CvsHost): WordBool; dispid 1610743812;
    property Name: WideString readonly dispid 1610743813;
    property IPAddressString: WideString readonly dispid 1610743814;
    property type_: CvsHostType readonly dispid 1610743815;
    property Subtype: CvsHostSubtype readonly dispid 1610743816;
    procedure SetName(const Name: WideString); dispid 1610743817;
    procedure SetIPAddressString(const IPAddress: WideString); dispid 1610743818;
  end;

// *********************************************************************//
// Interface: _CvsHostVersion
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {C9BEE73D-CDA9-3E2A-9D5D-542D85818564}
// *********************************************************************//
  _CvsHostVersion = interface(IDispatch)
    ['{C9BEE73D-CDA9-3E2A-9D5D-542D85818564}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function CompareTo(obj: OleVariant): Integer; safecall;
    function Get_Major: Integer; safecall;
    function Get_Minor: Integer; safecall;
    function Get_Revision: Integer; safecall;
    function Get_ReleaseType: WideString; safecall;
    function Get_BuildNumber: Integer; safecall;
    function CompareTo_2(Major: Integer; Minor: Integer; Revision: Integer): Integer; safecall;
    function CompareTo_3(Major: Integer; Minor: Integer; Revision: Integer; ReleaseType: Integer; 
                         BuildNumber: Integer): Integer; safecall;
    property ToString: WideString read Get_ToString;
    property Major: Integer read Get_Major;
    property Minor: Integer read Get_Minor;
    property Revision: Integer read Get_Revision;
    property ReleaseType: WideString read Get_ReleaseType;
    property BuildNumber: Integer read Get_BuildNumber;
  end;

// *********************************************************************//
// DispIntf:  _CvsHostVersionDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {C9BEE73D-CDA9-3E2A-9D5D-542D85818564}
// *********************************************************************//
  _CvsHostVersionDisp = dispinterface
    ['{C9BEE73D-CDA9-3E2A-9D5D-542D85818564}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    function CompareTo(obj: OleVariant): Integer; dispid 1610743812;
    property Major: Integer readonly dispid 1610743813;
    property Minor: Integer readonly dispid 1610743814;
    property Revision: Integer readonly dispid 1610743815;
    property ReleaseType: WideString readonly dispid 1610743816;
    property BuildNumber: Integer readonly dispid 1610743817;
    function CompareTo_2(Major: Integer; Minor: Integer; Revision: Integer): Integer; dispid 1610743818;
    function CompareTo_3(Major: Integer; Minor: Integer; Revision: Integer; ReleaseType: Integer; 
                         BuildNumber: Integer): Integer; dispid 1610743819;
  end;

// *********************************************************************//
// Interface: _CvsImage
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {102F35DA-843F-3773-A830-E2F4298B0AA6}
// *********************************************************************//
  _CvsImage = interface(IDispatch)
    ['{102F35DA-843F-3773-A830-E2F4298B0AA6}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    procedure Dispose; safecall;
    procedure Save(const fileName: WideString); safecall;
    function ToPicture: IDispatch; safecall;
    function GetPixelGrayscale(row: Integer; column: Integer): Byte; safecall;
    procedure GhostMethod__CvsImage_60_1; safecall;
    function Get_Height: Integer; safecall;
    function Get_Width: Integer; safecall;
    function Get_Location: CvsCellLocation; safecall;
    function Get_startRow: Integer; safecall;
    function Get_StartColumn: Integer; safecall;
    function Get_Resolution: CvsImageResolution; safecall;
    function Get_ResolutionScale: Integer; safecall;
    property ToString: WideString read Get_ToString;
    property Height: Integer read Get_Height;
    property Width: Integer read Get_Width;
    property Location: CvsCellLocation read Get_Location;
    property startRow: Integer read Get_startRow;
    property StartColumn: Integer read Get_StartColumn;
    property Resolution: CvsImageResolution read Get_Resolution;
    property ResolutionScale: Integer read Get_ResolutionScale;
  end;

// *********************************************************************//
// DispIntf:  _CvsImageDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {102F35DA-843F-3773-A830-E2F4298B0AA6}
// *********************************************************************//
  _CvsImageDisp = dispinterface
    ['{102F35DA-843F-3773-A830-E2F4298B0AA6}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    procedure Dispose; dispid 1610743812;
    procedure Save(const fileName: WideString); dispid 1610743813;
    function ToPicture: IDispatch; dispid 1610743814;
    function GetPixelGrayscale(row: Integer; column: Integer): Byte; dispid 1610743815;
    procedure GhostMethod__CvsImage_60_1; dispid 1610743816;
    property Height: Integer readonly dispid 1610743817;
    property Width: Integer readonly dispid 1610743818;
    property Location: {??CvsCellLocation}OleVariant readonly dispid 1610743819;
    property startRow: Integer readonly dispid 1610743820;
    property StartColumn: Integer readonly dispid 1610743821;
    property Resolution: CvsImageResolution readonly dispid 1610743822;
    property ResolutionScale: Integer readonly dispid 1610743823;
  end;

// *********************************************************************//
// Interface: _CvsInSight
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {24AC313D-8C47-3209-BE20-D3F1B4F6BAA6}
// *********************************************************************//
  _CvsInSight = interface(IDispatch)
    ['{24AC313D-8C47-3209-BE20-D3F1B4F6BAA6}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function GetLifetimeService: OleVariant; safecall;
    function InitializeLifetimeService: OleVariant; safecall;
    function CreateObjRef(const requestedType: _Type): _ObjRef; safecall;
    procedure Dispose; safecall;
    procedure add_StateChanged(const value: IUnknown); safecall;
    procedure remove_StateChanged(const value: IUnknown); safecall;
    procedure add_LiveAcquisitionChanged(const value: _EventHandler); safecall;
    procedure remove_LiveAcquisitionChanged(const value: _EventHandler); safecall;
    procedure add_ResultsChanged(const value: _EventHandler); safecall;
    procedure remove_ResultsChanged(const value: _EventHandler); safecall;
    procedure add_InSightMessage(const value: IUnknown); safecall;
    procedure remove_InSightMessage(const value: IUnknown); safecall;
    procedure add_SensorChanged(const value: _EventHandler); safecall;
    procedure remove_SensorChanged(const value: _EventHandler); safecall;
    procedure add_ConnectCompleted(const value: IUnknown); safecall;
    procedure remove_ConnectCompleted(const value: IUnknown); safecall;
    procedure add_LoadCompleted(const value: IUnknown); safecall;
    procedure remove_LoadCompleted(const value: IUnknown); safecall;
    procedure add_SaveCompleted(const value: IUnknown); safecall;
    procedure remove_SaveCompleted(const value: IUnknown); safecall;
    function Get_State: CvsInSightState; safecall;
    function Get_RemoteIPAddressString: WideString; safecall;
    function Get_Username: WideString; safecall;
    function Get_Password: WideString; safecall;
    function Get_Language: CvsInSightLanguage; safecall;
    function Get_SoftOnline: WordBool; safecall;
    procedure Set_SoftOnline(pRetVal: WordBool); safecall;
    function Get_DiscreteOnline: WordBool; safecall;
    function Get_NativeOnline: WordBool; safecall;
    function Get_LiveAcquisition: WordBool; safecall;
    procedure Set_LiveAcquisition(pRetVal: WordBool); safecall;
    function Get_Results: _CvsResultSet; safecall;
    function Get_access: CvsInSightSecurityAccess; safecall;
    function Get_AccessDowngraded: WordBool; safecall;
    function Get_sensor: _CvsSensor; safecall;
    function Get_JobInfo: _CvsJobInfo; safecall;
    function Get_IsJobProtected: WordBool; safecall;
    function Get_File_: _CvsFileManager; safecall;
    function Get_ExclusiveConnection: WordBool; safecall;
    procedure Set_ExclusiveConnection(pRetVal: WordBool); safecall;
    procedure Connect(const HostName: WideString; const Username: WideString; 
                      const Password: WideString; forceConnect: WordBool; asynchronous: WordBool); safecall;
    procedure Disconnect; safecall;
    procedure AcceptUpdate; safecall;
    procedure ManualAcquire; safecall;
    procedure ManualAcquire_2(wait: WordBool); safecall;
    procedure ResetSystem(waitForReconnect: WordBool); safecall;
    procedure SetInteger(row: Integer; col: Integer; cellValue: Integer); safecall;
    procedure SetListBoxIndex(row: Integer; col: Integer; index: Integer); safecall;
    procedure SetCheckBox(row: Integer; col: Integer; cellValue: WordBool); safecall;
    procedure SetFloat(row: Integer; col: Integer; cellValue: Single); safecall;
    procedure SetString(row: Integer; col: Integer; const cellValue: WideString); safecall;
    procedure ClickButton(row: Integer; col: Integer); safecall;
    procedure ClickSelect(row: Integer; col: Integer); safecall;
    procedure SetEditGraphic(const graphic: _CvsGraphic); safecall;
    procedure SetExpression(row: Integer; col: Integer; const Expression: WideString; wait: WordBool); safecall;
    procedure SetCellState(row: Integer; col: Integer; Enabled: WordBool); safecall;
    procedure SetCellState_2(row: Integer; col: Integer; referenceRow: Integer; 
                             absoluteRow: WordBool; referenceColumn: Integer; 
                             absoluteColumn: WordBool); safecall;
    function GetSymbolicTagCollection: _CvsSymbolicTagCollection; safecall;
    procedure SetSymbolicTagCollection(const symbolicTags: _CvsSymbolicTagCollection); safecall;
    procedure SetSymbolicTag(Location: CvsCellLocation; const Name: WideString); safecall;
    function GetEasyView: _CvsEasyView; safecall;
    procedure SetEasyView(const easyView: _CvsEasyView); safecall;
    property ToString: WideString read Get_ToString;
    property State: CvsInSightState read Get_State;
    property RemoteIPAddressString: WideString read Get_RemoteIPAddressString;
    property Username: WideString read Get_Username;
    property Password: WideString read Get_Password;
    property Language: CvsInSightLanguage read Get_Language;
    property SoftOnline: WordBool read Get_SoftOnline write Set_SoftOnline;
    property DiscreteOnline: WordBool read Get_DiscreteOnline;
    property NativeOnline: WordBool read Get_NativeOnline;
    property LiveAcquisition: WordBool read Get_LiveAcquisition write Set_LiveAcquisition;
    property Results: _CvsResultSet read Get_Results;
    property access: CvsInSightSecurityAccess read Get_access;
    property AccessDowngraded: WordBool read Get_AccessDowngraded;
    property sensor: _CvsSensor read Get_sensor;
    property JobInfo: _CvsJobInfo read Get_JobInfo;
    property IsJobProtected: WordBool read Get_IsJobProtected;
    property File_: _CvsFileManager read Get_File_;
    property ExclusiveConnection: WordBool read Get_ExclusiveConnection write Set_ExclusiveConnection;
  end;

// *********************************************************************//
// DispIntf:  _CvsInSightDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {24AC313D-8C47-3209-BE20-D3F1B4F6BAA6}
// *********************************************************************//
  _CvsInSightDisp = dispinterface
    ['{24AC313D-8C47-3209-BE20-D3F1B4F6BAA6}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    function GetLifetimeService: OleVariant; dispid 1610743812;
    function InitializeLifetimeService: OleVariant; dispid 1610743813;
    function CreateObjRef(const requestedType: _Type): _ObjRef; dispid 1610743814;
    procedure Dispose; dispid 1610743815;
    procedure add_StateChanged(const value: IUnknown); dispid 1610743816;
    procedure remove_StateChanged(const value: IUnknown); dispid 1610743817;
    procedure add_LiveAcquisitionChanged(const value: _EventHandler); dispid 1610743818;
    procedure remove_LiveAcquisitionChanged(const value: _EventHandler); dispid 1610743819;
    procedure add_ResultsChanged(const value: _EventHandler); dispid 1610743820;
    procedure remove_ResultsChanged(const value: _EventHandler); dispid 1610743821;
    procedure add_InSightMessage(const value: IUnknown); dispid 1610743822;
    procedure remove_InSightMessage(const value: IUnknown); dispid 1610743823;
    procedure add_SensorChanged(const value: _EventHandler); dispid 1610743824;
    procedure remove_SensorChanged(const value: _EventHandler); dispid 1610743825;
    procedure add_ConnectCompleted(const value: IUnknown); dispid 1610743826;
    procedure remove_ConnectCompleted(const value: IUnknown); dispid 1610743827;
    procedure add_LoadCompleted(const value: IUnknown); dispid 1610743828;
    procedure remove_LoadCompleted(const value: IUnknown); dispid 1610743829;
    procedure add_SaveCompleted(const value: IUnknown); dispid 1610743830;
    procedure remove_SaveCompleted(const value: IUnknown); dispid 1610743831;
    property State: CvsInSightState readonly dispid 1610743832;
    property RemoteIPAddressString: WideString readonly dispid 1610743833;
    property Username: WideString readonly dispid 1610743834;
    property Password: WideString readonly dispid 1610743835;
    property Language: CvsInSightLanguage readonly dispid 1610743836;
    property SoftOnline: WordBool dispid 1610743837;
    property DiscreteOnline: WordBool readonly dispid 1610743839;
    property NativeOnline: WordBool readonly dispid 1610743840;
    property LiveAcquisition: WordBool dispid 1610743841;
    property Results: _CvsResultSet readonly dispid 1610743843;
    property access: CvsInSightSecurityAccess readonly dispid 1610743844;
    property AccessDowngraded: WordBool readonly dispid 1610743845;
    property sensor: _CvsSensor readonly dispid 1610743846;
    property JobInfo: _CvsJobInfo readonly dispid 1610743847;
    property IsJobProtected: WordBool readonly dispid 1610743848;
    property File_: _CvsFileManager readonly dispid 1610743849;
    property ExclusiveConnection: WordBool dispid 1610743850;
    procedure Connect(const HostName: WideString; const Username: WideString; 
                      const Password: WideString; forceConnect: WordBool; asynchronous: WordBool); dispid 1610743852;
    procedure Disconnect; dispid 1610743853;
    procedure AcceptUpdate; dispid 1610743854;
    procedure ManualAcquire; dispid 1610743855;
    procedure ManualAcquire_2(wait: WordBool); dispid 1610743856;
    procedure ResetSystem(waitForReconnect: WordBool); dispid 1610743857;
    procedure SetInteger(row: Integer; col: Integer; cellValue: Integer); dispid 1610743858;
    procedure SetListBoxIndex(row: Integer; col: Integer; index: Integer); dispid 1610743859;
    procedure SetCheckBox(row: Integer; col: Integer; cellValue: WordBool); dispid 1610743860;
    procedure SetFloat(row: Integer; col: Integer; cellValue: Single); dispid 1610743861;
    procedure SetString(row: Integer; col: Integer; const cellValue: WideString); dispid 1610743862;
    procedure ClickButton(row: Integer; col: Integer); dispid 1610743863;
    procedure ClickSelect(row: Integer; col: Integer); dispid 1610743864;
    procedure SetEditGraphic(const graphic: _CvsGraphic); dispid 1610743865;
    procedure SetExpression(row: Integer; col: Integer; const Expression: WideString; wait: WordBool); dispid 1610743866;
    procedure SetCellState(row: Integer; col: Integer; Enabled: WordBool); dispid 1610743867;
    procedure SetCellState_2(row: Integer; col: Integer; referenceRow: Integer; 
                             absoluteRow: WordBool; referenceColumn: Integer; 
                             absoluteColumn: WordBool); dispid 1610743868;
    function GetSymbolicTagCollection: _CvsSymbolicTagCollection; dispid 1610743869;
    procedure SetSymbolicTagCollection(const symbolicTags: _CvsSymbolicTagCollection); dispid 1610743870;
    procedure SetSymbolicTag(Location: {??CvsCellLocation}OleVariant; const Name: WideString); dispid 1610743871;
    function GetEasyView: _CvsEasyView; dispid 1610743872;
    procedure SetEasyView(const easyView: _CvsEasyView); dispid 1610743873;
  end;

// *********************************************************************//
// Interface: _CvsJobInfo
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {C913BD9F-A2EA-3CF8-84F7-0698A5FE4F46}
// *********************************************************************//
  _CvsJobInfo = interface(IDispatch)
    ['{C913BD9F-A2EA-3CF8-84F7-0698A5FE4F46}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function GetLifetimeService: OleVariant; safecall;
    function InitializeLifetimeService: OleVariant; safecall;
    function CreateObjRef(const requestedType: _Type): _ObjRef; safecall;
    function Get_HasCustomView: WordBool; safecall;
    function Get_ActiveJobFile: WideString; safecall;
    function Get_CustomViewConditionalRefresh: WordBool; safecall;
    procedure Set_CustomViewConditionalRefresh(pRetVal: WordBool); safecall;
    function Get_CustomViewConditionalRefreshLocation: CvsCellLocation; safecall;
    procedure Set_CustomViewConditionalRefreshLocation(pRetVal: CvsCellLocation); safecall;
    procedure SetCustomViewConditionalRefreshLocation(row: Integer; col: Integer); safecall;
    function Get_NumRows: Integer; safecall;
    function Get_NumColumns: Integer; safecall;
    function Get_AnyCustomViews: WordBool; safecall;
    function IsInCustomView(loc: CvsCellLocation): WordBool; safecall;
    procedure SwitchAllCurrentViews(customView: WordBool); safecall;
    function Get_SupportsSheets: WordBool; safecall;
    function Get_NumberOfSheets: Integer; safecall;
    property ToString: WideString read Get_ToString;
    property HasCustomView: WordBool read Get_HasCustomView;
    property ActiveJobFile: WideString read Get_ActiveJobFile;
    property CustomViewConditionalRefresh: WordBool read Get_CustomViewConditionalRefresh write Set_CustomViewConditionalRefresh;
    property CustomViewConditionalRefreshLocation: CvsCellLocation read Get_CustomViewConditionalRefreshLocation write Set_CustomViewConditionalRefreshLocation;
    property NumRows: Integer read Get_NumRows;
    property NumColumns: Integer read Get_NumColumns;
    property AnyCustomViews: WordBool read Get_AnyCustomViews;
    property SupportsSheets: WordBool read Get_SupportsSheets;
    property NumberOfSheets: Integer read Get_NumberOfSheets;
  end;

// *********************************************************************//
// DispIntf:  _CvsJobInfoDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {C913BD9F-A2EA-3CF8-84F7-0698A5FE4F46}
// *********************************************************************//
  _CvsJobInfoDisp = dispinterface
    ['{C913BD9F-A2EA-3CF8-84F7-0698A5FE4F46}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    function GetLifetimeService: OleVariant; dispid 1610743812;
    function InitializeLifetimeService: OleVariant; dispid 1610743813;
    function CreateObjRef(const requestedType: _Type): _ObjRef; dispid 1610743814;
    property HasCustomView: WordBool readonly dispid 1610743815;
    property ActiveJobFile: WideString readonly dispid 1610743816;
    property CustomViewConditionalRefresh: WordBool dispid 1610743817;
    property CustomViewConditionalRefreshLocation: {??CvsCellLocation}OleVariant dispid 1610743819;
    procedure SetCustomViewConditionalRefreshLocation(row: Integer; col: Integer); dispid 1610743821;
    property NumRows: Integer readonly dispid 1610743822;
    property NumColumns: Integer readonly dispid 1610743823;
    property AnyCustomViews: WordBool readonly dispid 1610743824;
    function IsInCustomView(loc: {??CvsCellLocation}OleVariant): WordBool; dispid 1610743825;
    procedure SwitchAllCurrentViews(customView: WordBool); dispid 1610743826;
    property SupportsSheets: WordBool readonly dispid 1610743827;
    property NumberOfSheets: Integer readonly dispid 1610743828;
  end;

// *********************************************************************//
// Interface: _CvsKernel
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {37E445AA-CC97-3AAC-88C0-382A0F564AE7}
// *********************************************************************//
  _CvsKernel = interface(IDispatch)
    ['{37E445AA-CC97-3AAC-88C0-382A0F564AE7}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Get_NumberOfRows: Byte; safecall;
    procedure Set_NumberOfRows(pRetVal: Byte); safecall;
    function Get_NumberOfColumns: Byte; safecall;
    procedure Set_NumberOfColumns(pRetVal: Byte); safecall;
    function Get_Coefficients: PSafeArray; safecall;
    procedure Update; safecall;
    property ToString: WideString read Get_ToString;
    property NumberOfRows: Byte read Get_NumberOfRows write Set_NumberOfRows;
    property NumberOfColumns: Byte read Get_NumberOfColumns write Set_NumberOfColumns;
    property Coefficients: PSafeArray read Get_Coefficients;
  end;

// *********************************************************************//
// DispIntf:  _CvsKernelDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {37E445AA-CC97-3AAC-88C0-382A0F564AE7}
// *********************************************************************//
  _CvsKernelDisp = dispinterface
    ['{37E445AA-CC97-3AAC-88C0-382A0F564AE7}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    property NumberOfRows: Byte dispid 1610743812;
    property NumberOfColumns: Byte dispid 1610743814;
    property Coefficients: {??PSafeArray}OleVariant readonly dispid 1610743816;
    procedure Update; dispid 1610743817;
  end;

// *********************************************************************//
// Interface: _CvsLicense
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {1365B4D2-D8A8-3353-9039-7B0A458B4082}
// *********************************************************************//
  _CvsLicense = interface(IDispatch)
    ['{1365B4D2-D8A8-3353-9039-7B0A458B4082}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    property ToString: WideString read Get_ToString;
  end;

// *********************************************************************//
// DispIntf:  _CvsLicenseDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {1365B4D2-D8A8-3353-9039-7B0A458B4082}
// *********************************************************************//
  _CvsLicenseDisp = dispinterface
    ['{1365B4D2-D8A8-3353-9039-7B0A458B4082}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
  end;

// *********************************************************************//
// Interface: _CvsLocalIOSettings
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {3226A657-CC6D-34D7-8F7F-E5EB388400DD}
// *********************************************************************//
  _CvsLocalIOSettings = interface(IDispatch)
    ['{3226A657-CC6D-34D7-8F7F-E5EB388400DD}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Clone: OleVariant; safecall;
    function Get_Inputs: _CvsInputCollection; safecall;
    function Get_Outputs: _CvsOutputCollection; safecall;
    function IsValidInputType(line: Integer; Type_: CvsInputType): WordBool; safecall;
    procedure SetInputType(line: Integer; value: CvsInputType); safecall;
    procedure SetOutputType(line: Integer; value: CvsOutputType); safecall;
    function IsEqual(const ioLines: _CvsDiscreteIOLines): WordBool; safecall;
    property ToString: WideString read Get_ToString;
    property Inputs: _CvsInputCollection read Get_Inputs;
    property Outputs: _CvsOutputCollection read Get_Outputs;
  end;

// *********************************************************************//
// DispIntf:  _CvsLocalIOSettingsDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {3226A657-CC6D-34D7-8F7F-E5EB388400DD}
// *********************************************************************//
  _CvsLocalIOSettingsDisp = dispinterface
    ['{3226A657-CC6D-34D7-8F7F-E5EB388400DD}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    function Clone: OleVariant; dispid 1610743812;
    property Inputs: _CvsInputCollection readonly dispid 1610743813;
    property Outputs: _CvsOutputCollection readonly dispid 1610743814;
    function IsValidInputType(line: Integer; Type_: CvsInputType): WordBool; dispid 1610743815;
    procedure SetInputType(line: Integer; value: CvsInputType); dispid 1610743816;
    procedure SetOutputType(line: Integer; value: CvsOutputType); dispid 1610743817;
    function IsEqual(const ioLines: _CvsDiscreteIOLines): WordBool; dispid 1610743818;
  end;

// *********************************************************************//
// Interface: _CvsNativeModeClient
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {79EF7AC7-ED5E-33FF-9F95-72C360E16037}
// *********************************************************************//
  _CvsNativeModeClient = interface(IDispatch)
    ['{79EF7AC7-ED5E-33FF-9F95-72C360E16037}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    procedure add_ConnectCompleted(const value: IUnknown); safecall;
    procedure remove_ConnectCompleted(const value: IUnknown); safecall;
    function Get_Connected: WordBool; safecall;
    function Get_WelcomeBanner: WideString; safecall;
    function Get_Username: WideString; safecall;
    function Get_Password: WideString; safecall;
    function Get_RemoteIPAddressString: WideString; safecall;
    function Get_LastResponseString: WideString; safecall;
    function Get_Timeout: Integer; safecall;
    procedure Set_Timeout(pRetVal: Integer); safecall;
    function Get_UsePasswordEncryption: WordBool; safecall;
    procedure Set_UsePasswordEncryption(pRetVal: WordBool); safecall;
    procedure Connect(const HostName: WideString; const Username: WideString; 
                      const Password: WideString); safecall;
    procedure ConnectAsynchronous(const HostName: WideString; const Username: WideString; 
                                  const Password: WideString); safecall;
    procedure Disconnect; safecall;
    function SendCommand(const command: WideString): WideString; safecall;
    function ReadStatusValue: Smallint; safecall;
    function Get_NativeOnline: WordBool; safecall;
    procedure Set_NativeOnline(pRetVal: WordBool); safecall;
    procedure ResetSystem; safecall;
    property ToString: WideString read Get_ToString;
    property Connected: WordBool read Get_Connected;
    property WelcomeBanner: WideString read Get_WelcomeBanner;
    property Username: WideString read Get_Username;
    property Password: WideString read Get_Password;
    property RemoteIPAddressString: WideString read Get_RemoteIPAddressString;
    property LastResponseString: WideString read Get_LastResponseString;
    property Timeout: Integer read Get_Timeout write Set_Timeout;
    property UsePasswordEncryption: WordBool read Get_UsePasswordEncryption write Set_UsePasswordEncryption;
    property NativeOnline: WordBool read Get_NativeOnline write Set_NativeOnline;
  end;

// *********************************************************************//
// DispIntf:  _CvsNativeModeClientDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {79EF7AC7-ED5E-33FF-9F95-72C360E16037}
// *********************************************************************//
  _CvsNativeModeClientDisp = dispinterface
    ['{79EF7AC7-ED5E-33FF-9F95-72C360E16037}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    procedure add_ConnectCompleted(const value: IUnknown); dispid 1610743812;
    procedure remove_ConnectCompleted(const value: IUnknown); dispid 1610743813;
    property Connected: WordBool readonly dispid 1610743814;
    property WelcomeBanner: WideString readonly dispid 1610743815;
    property Username: WideString readonly dispid 1610743816;
    property Password: WideString readonly dispid 1610743817;
    property RemoteIPAddressString: WideString readonly dispid 1610743818;
    property LastResponseString: WideString readonly dispid 1610743819;
    property Timeout: Integer dispid 1610743820;
    property UsePasswordEncryption: WordBool dispid 1610743822;
    procedure Connect(const HostName: WideString; const Username: WideString; 
                      const Password: WideString); dispid 1610743824;
    procedure ConnectAsynchronous(const HostName: WideString; const Username: WideString; 
                                  const Password: WideString); dispid 1610743825;
    procedure Disconnect; dispid 1610743826;
    function SendCommand(const command: WideString): WideString; dispid 1610743827;
    function ReadStatusValue: Smallint; dispid 1610743828;
    property NativeOnline: WordBool dispid 1610743829;
    procedure ResetSystem; dispid 1610743831;
  end;

// *********************************************************************//
// Interface: _CvsNetworkSettings
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {50B9AB3B-2568-3418-9D64-C8821BE4252C}
// *********************************************************************//
  _CvsNetworkSettings = interface(IDispatch)
    ['{50B9AB3B-2568-3418-9D64-C8821BE4252C}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Clone: OleVariant; safecall;
    function Get_DhcpEnabled: WordBool; safecall;
    procedure Set_DhcpEnabled(pRetVal: WordBool); safecall;
    function Get_DhcpTimeout: Integer; safecall;
    procedure Set_DhcpTimeout(pRetVal: Integer); safecall;
    function Get_TelnetPort: Integer; safecall;
    procedure Set_TelnetPort(pRetVal: Integer); safecall;
    function Get_EipWatchdogTimeoutAction: CvsEipWatchdogTimeoutActions; safecall;
    procedure Set_EipWatchdogTimeoutAction(pRetVal: CvsEipWatchdogTimeoutActions); safecall;
    function Get_StaticIPAddressString: WideString; safecall;
    procedure Set_StaticIPAddressString(const pRetVal: WideString); safecall;
    function Get_StaticSubnetMaskString: WideString; safecall;
    procedure Set_StaticSubnetMaskString(const pRetVal: WideString); safecall;
    function Get_StaticDefaultGatewayString: WideString; safecall;
    procedure Set_StaticDefaultGatewayString(const pRetVal: WideString); safecall;
    function Get_StaticDnsServerString: WideString; safecall;
    procedure Set_StaticDnsServerString(const pRetVal: WideString); safecall;
    function Get_StaticDomainName: WideString; safecall;
    procedure Set_StaticDomainName(const pRetVal: WideString); safecall;
    function Get_DhcpIPAddressString: WideString; safecall;
    function Get_DhcpSubnetMaskString: WideString; safecall;
    function Get_DhcpDefaultGatewayString: WideString; safecall;
    function Get_DhcpDnsServerString: WideString; safecall;
    function Get_DhcpDomainName: WideString; safecall;
    function Get_HostName: WideString; safecall;
    procedure Set_HostName(const pRetVal: WideString); safecall;
    function Get_FtpSettings: _CvsFtpSettings; safecall;
    function Get_HostTable: _CvsHostTableCollection; safecall;
    function Get_UserList: _CvsUserCollection; safecall;
    function Get_HostTableFavored: WordBool; safecall;
    procedure Set_HostTableFavored(pRetVal: WordBool); safecall;
    function Get_ProtocolService: CvsProtocolServices; safecall;
    procedure Set_ProtocolService(pRetVal: CvsProtocolServices); safecall;
    function Get_ModbusSettings: _CvsModbusSettings; safecall;
    function Get_PowerlinkSettings: _CvsPowerlinkSettings; safecall;
    function Get_LinkMode: CvsNetworkLinkModes; safecall;
    procedure Set_LinkMode(pRetVal: CvsNetworkLinkModes); safecall;
    function Get_CurrentLinkMode: CvsNetworkLinkModes; safecall;
    function Get_SupportedLinkModes: PSafeArray; safecall;
    property ToString: WideString read Get_ToString;
    property DhcpEnabled: WordBool read Get_DhcpEnabled write Set_DhcpEnabled;
    property DhcpTimeout: Integer read Get_DhcpTimeout write Set_DhcpTimeout;
    property TelnetPort: Integer read Get_TelnetPort write Set_TelnetPort;
    property EipWatchdogTimeoutAction: CvsEipWatchdogTimeoutActions read Get_EipWatchdogTimeoutAction write Set_EipWatchdogTimeoutAction;
    property StaticIPAddressString: WideString read Get_StaticIPAddressString write Set_StaticIPAddressString;
    property StaticSubnetMaskString: WideString read Get_StaticSubnetMaskString write Set_StaticSubnetMaskString;
    property StaticDefaultGatewayString: WideString read Get_StaticDefaultGatewayString write Set_StaticDefaultGatewayString;
    property StaticDnsServerString: WideString read Get_StaticDnsServerString write Set_StaticDnsServerString;
    property StaticDomainName: WideString read Get_StaticDomainName write Set_StaticDomainName;
    property DhcpIPAddressString: WideString read Get_DhcpIPAddressString;
    property DhcpSubnetMaskString: WideString read Get_DhcpSubnetMaskString;
    property DhcpDefaultGatewayString: WideString read Get_DhcpDefaultGatewayString;
    property DhcpDnsServerString: WideString read Get_DhcpDnsServerString;
    property DhcpDomainName: WideString read Get_DhcpDomainName;
    property HostName: WideString read Get_HostName write Set_HostName;
    property FtpSettings: _CvsFtpSettings read Get_FtpSettings;
    property HostTable: _CvsHostTableCollection read Get_HostTable;
    property UserList: _CvsUserCollection read Get_UserList;
    property HostTableFavored: WordBool read Get_HostTableFavored write Set_HostTableFavored;
    property ProtocolService: CvsProtocolServices read Get_ProtocolService write Set_ProtocolService;
    property ModbusSettings: _CvsModbusSettings read Get_ModbusSettings;
    property PowerlinkSettings: _CvsPowerlinkSettings read Get_PowerlinkSettings;
    property LinkMode: CvsNetworkLinkModes read Get_LinkMode write Set_LinkMode;
    property CurrentLinkMode: CvsNetworkLinkModes read Get_CurrentLinkMode;
    property SupportedLinkModes: PSafeArray read Get_SupportedLinkModes;
  end;

// *********************************************************************//
// DispIntf:  _CvsNetworkSettingsDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {50B9AB3B-2568-3418-9D64-C8821BE4252C}
// *********************************************************************//
  _CvsNetworkSettingsDisp = dispinterface
    ['{50B9AB3B-2568-3418-9D64-C8821BE4252C}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    function Clone: OleVariant; dispid 1610743812;
    property DhcpEnabled: WordBool dispid 1610743813;
    property DhcpTimeout: Integer dispid 1610743815;
    property TelnetPort: Integer dispid 1610743817;
    property EipWatchdogTimeoutAction: CvsEipWatchdogTimeoutActions dispid 1610743819;
    property StaticIPAddressString: WideString dispid 1610743821;
    property StaticSubnetMaskString: WideString dispid 1610743823;
    property StaticDefaultGatewayString: WideString dispid 1610743825;
    property StaticDnsServerString: WideString dispid 1610743827;
    property StaticDomainName: WideString dispid 1610743829;
    property DhcpIPAddressString: WideString readonly dispid 1610743831;
    property DhcpSubnetMaskString: WideString readonly dispid 1610743832;
    property DhcpDefaultGatewayString: WideString readonly dispid 1610743833;
    property DhcpDnsServerString: WideString readonly dispid 1610743834;
    property DhcpDomainName: WideString readonly dispid 1610743835;
    property HostName: WideString dispid 1610743836;
    property FtpSettings: _CvsFtpSettings readonly dispid 1610743838;
    property HostTable: _CvsHostTableCollection readonly dispid 1610743839;
    property UserList: _CvsUserCollection readonly dispid 1610743840;
    property HostTableFavored: WordBool dispid 1610743841;
    property ProtocolService: CvsProtocolServices dispid 1610743843;
    property ModbusSettings: _CvsModbusSettings readonly dispid 1610743845;
    property PowerlinkSettings: _CvsPowerlinkSettings readonly dispid 1610743846;
    property LinkMode: CvsNetworkLinkModes dispid 1610743847;
    property CurrentLinkMode: CvsNetworkLinkModes readonly dispid 1610743849;
    property SupportedLinkModes: {??PSafeArray}OleVariant readonly dispid 1610743850;
  end;

// *********************************************************************//
// Interface: _CvsPicture
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {A9FCAC72-2F17-38FB-8743-41B8A1D8564A}
// *********************************************************************//
  _CvsPicture = interface(IDispatch)
    ['{A9FCAC72-2F17-38FB-8743-41B8A1D8564A}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Get_Uri: WideString; safecall;
    function Get_data: PSafeArray; safecall;
    function Get_Picture: OleVariant; safecall;
    function Draw(const gr: IUnknown; bounds: Rectangle; scale: Single): Rectangle; safecall;
    function GetDrawnBounds(bounds: Rectangle; scale: Single): Rectangle; safecall;
    function Get_Size: Size; safecall;
    function GetPictureSize(scale: Single): Size; safecall;
    property ToString: WideString read Get_ToString;
    property Uri: WideString read Get_Uri;
    property data: PSafeArray read Get_data;
    property Picture: OleVariant read Get_Picture;
    property Size: Size read Get_Size;
  end;

// *********************************************************************//
// DispIntf:  _CvsPictureDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {A9FCAC72-2F17-38FB-8743-41B8A1D8564A}
// *********************************************************************//
  _CvsPictureDisp = dispinterface
    ['{A9FCAC72-2F17-38FB-8743-41B8A1D8564A}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    property Uri: WideString readonly dispid 1610743812;
    property data: {??PSafeArray}OleVariant readonly dispid 1610743813;
    property Picture: OleVariant readonly dispid 1610743814;
    function Draw(const gr: IUnknown; bounds: {??Rectangle}OleVariant; scale: Single): {??Rectangle}OleVariant; dispid 1610743815;
    function GetDrawnBounds(bounds: {??Rectangle}OleVariant; scale: Single): {??Rectangle}OleVariant; dispid 1610743816;
    property Size: {??Size}OleVariant readonly dispid 1610743817;
    function GetPictureSize(scale: Single): {??Size}OleVariant; dispid 1610743818;
  end;

// *********************************************************************//
// Interface: _CvsProcessorConstants
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {95B1AA64-472D-3310-82CD-9AAACC607722}
// *********************************************************************//
  _CvsProcessorConstants = interface(IDispatch)
    ['{95B1AA64-472D-3310-82CD-9AAACC607722}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Get_IsColor: WordBool; safecall;
    function Get_Is1024x768: WordBool; safecall;
    function Get_IsOpcSupported: WordBool; safecall;
    function Get_IsJobTagSupported: WordBool; safecall;
    function Get_IsUndoSupported: WordBool; safecall;
    function Get_IsLineScan: WordBool; safecall;
    function Get_IsEncryptJobLoadSaveSupported: WordBool; safecall;
    function Get_ImageHeight: Integer; safecall;
    function Get_ImageWidth: Integer; safecall;
    function Get_SpreadsheetColumns: Integer; safecall;
    function Get_SpreadsheetRows: Integer; safecall;
    function Get_SpreadsheetRange: CvsCellRange; safecall;
    function Get_MaxExpressionStack: Integer; safecall;
    function Get_MaxSpreadsheetCells: Integer; safecall;
    function Get_MaxExpressionLength: Integer; safecall;
    function Get_SpreadsheetTotalRows: Integer; safecall;
    function Get_MaxFileNameLength: Integer; safecall;
    function Get_FirmwareDrivenTestRunSupported: WordBool; safecall;
    property ToString: WideString read Get_ToString;
    property IsColor: WordBool read Get_IsColor;
    property Is1024x768: WordBool read Get_Is1024x768;
    property IsOpcSupported: WordBool read Get_IsOpcSupported;
    property IsJobTagSupported: WordBool read Get_IsJobTagSupported;
    property IsUndoSupported: WordBool read Get_IsUndoSupported;
    property IsLineScan: WordBool read Get_IsLineScan;
    property IsEncryptJobLoadSaveSupported: WordBool read Get_IsEncryptJobLoadSaveSupported;
    property ImageHeight: Integer read Get_ImageHeight;
    property ImageWidth: Integer read Get_ImageWidth;
    property SpreadsheetColumns: Integer read Get_SpreadsheetColumns;
    property SpreadsheetRows: Integer read Get_SpreadsheetRows;
    property SpreadsheetRange: CvsCellRange read Get_SpreadsheetRange;
    property MaxExpressionStack: Integer read Get_MaxExpressionStack;
    property MaxSpreadsheetCells: Integer read Get_MaxSpreadsheetCells;
    property MaxExpressionLength: Integer read Get_MaxExpressionLength;
    property SpreadsheetTotalRows: Integer read Get_SpreadsheetTotalRows;
    property MaxFileNameLength: Integer read Get_MaxFileNameLength;
    property FirmwareDrivenTestRunSupported: WordBool read Get_FirmwareDrivenTestRunSupported;
  end;

// *********************************************************************//
// DispIntf:  _CvsProcessorConstantsDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {95B1AA64-472D-3310-82CD-9AAACC607722}
// *********************************************************************//
  _CvsProcessorConstantsDisp = dispinterface
    ['{95B1AA64-472D-3310-82CD-9AAACC607722}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    property IsColor: WordBool readonly dispid 1610743812;
    property Is1024x768: WordBool readonly dispid 1610743813;
    property IsOpcSupported: WordBool readonly dispid 1610743814;
    property IsJobTagSupported: WordBool readonly dispid 1610743815;
    property IsUndoSupported: WordBool readonly dispid 1610743816;
    property IsLineScan: WordBool readonly dispid 1610743817;
    property IsEncryptJobLoadSaveSupported: WordBool readonly dispid 1610743818;
    property ImageHeight: Integer readonly dispid 1610743819;
    property ImageWidth: Integer readonly dispid 1610743820;
    property SpreadsheetColumns: Integer readonly dispid 1610743821;
    property SpreadsheetRows: Integer readonly dispid 1610743822;
    property SpreadsheetRange: {??CvsCellRange}OleVariant readonly dispid 1610743823;
    property MaxExpressionStack: Integer readonly dispid 1610743824;
    property MaxSpreadsheetCells: Integer readonly dispid 1610743825;
    property MaxExpressionLength: Integer readonly dispid 1610743826;
    property SpreadsheetTotalRows: Integer readonly dispid 1610743827;
    property MaxFileNameLength: Integer readonly dispid 1610743828;
    property FirmwareDrivenTestRunSupported: WordBool readonly dispid 1610743829;
  end;

// *********************************************************************//
// Interface: _CvsResultSet
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {9CEF22B9-4FE4-3053-99FA-EFFB0C73F94F}
// *********************************************************************//
  _CvsResultSet = interface(IDispatch)
    ['{9CEF22B9-4FE4-3053-99FA-EFFB0C73F94F}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    procedure Dispose; safecall;
    function Get_Id: Integer; safecall;
    function Get_Cells: _CvsCellCollection; safecall;
    function Get_Image: _CvsImage; safecall;
    function Get_Images: PSafeArray; safecall;
    function GetImage(sheetIndex: Integer): _CvsImage; safecall;
    function Get_Graphics: _CvsGraphicCollection; safecall;
    function Get_ProcessorConstants: _CvsProcessorConstants; safecall;
    function Get_JobInfo: _CvsJobInfo; safecall;
    function Get_StatusLevel: CvsStatusLevel; safecall;
    property ToString: WideString read Get_ToString;
    property Id: Integer read Get_Id;
    property Cells: _CvsCellCollection read Get_Cells;
    property Image: _CvsImage read Get_Image;
    property Images: PSafeArray read Get_Images;
    property Graphics: _CvsGraphicCollection read Get_Graphics;
    property ProcessorConstants: _CvsProcessorConstants read Get_ProcessorConstants;
    property JobInfo: _CvsJobInfo read Get_JobInfo;
    property StatusLevel: CvsStatusLevel read Get_StatusLevel;
  end;

// *********************************************************************//
// DispIntf:  _CvsResultSetDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {9CEF22B9-4FE4-3053-99FA-EFFB0C73F94F}
// *********************************************************************//
  _CvsResultSetDisp = dispinterface
    ['{9CEF22B9-4FE4-3053-99FA-EFFB0C73F94F}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    procedure Dispose; dispid 1610743812;
    property Id: Integer readonly dispid 1610743813;
    property Cells: _CvsCellCollection readonly dispid 1610743814;
    property Image: _CvsImage readonly dispid 1610743815;
    property Images: {??PSafeArray}OleVariant readonly dispid 1610743816;
    function GetImage(sheetIndex: Integer): _CvsImage; dispid 1610743817;
    property Graphics: _CvsGraphicCollection readonly dispid 1610743818;
    property ProcessorConstants: _CvsProcessorConstants readonly dispid 1610743819;
    property JobInfo: _CvsJobInfo readonly dispid 1610743820;
    property StatusLevel: CvsStatusLevel readonly dispid 1610743821;
  end;

// *********************************************************************//
// Interface: ICvsResultsQueue
// Flags:     (4288) Dual NonExtensible Dispatchable
// GUID:      {F2D13F4E-DD3C-4451-A529-789730C020F5}
// *********************************************************************//
  ICvsResultsQueue = interface(IDispatch)
    ['{F2D13F4E-DD3C-4451-A529-789730C020F5}']
    procedure add_ResultsQueueChanged(const value: IUnknown); safecall;
    procedure remove_ResultsQueueChanged(const value: IUnknown); safecall;
    function Get_IsAvailable: WordBool; safecall;
    function Get_EnableQueuing: WordBool; safecall;
    procedure Set_EnableQueuing(pRetVal: WordBool); safecall;
    function Get_EnableMessages: WordBool; safecall;
    procedure Set_EnableMessages(pRetVal: WordBool); safecall;
    function Get_IsFrozen: WordBool; safecall;
    function Get_QueueResults: _ArrayList; safecall;
    function Get_SlotResult: _CvsResultSet; safecall;
    function GetShortStatus: IUnknown; safecall;
    function GetStatus: IUnknown; safecall;
    procedure SetSettings(const settings: IUnknown; writeToProcSet: WordBool); safecall;
    procedure GhostMethod_ICvsResultsQueue_80_1; safecall;
    procedure ClearQueue; safecall;
    procedure DeleteResult(index: Integer); safecall;
    procedure RequestShortStatus; safecall;
    procedure RequestStatus; safecall;
    procedure RequestFirstSlot; safecall;
    procedure RequestNextSlot; safecall;
    procedure RequestSlot(slotNum: Integer); safecall;
    procedure RequestSlotUnscaled(slotNum: Integer); safecall;
    procedure RequestSlotRescaled(slotNum: Integer); safecall;
    function GetThumbnailImages(startIndex: Integer; maxImages: Integer; ImageIndex: Integer; 
                                desiredSize: Size): PSafeArray; safecall;
    property IsAvailable: WordBool read Get_IsAvailable;
    property EnableQueuing: WordBool read Get_EnableQueuing write Set_EnableQueuing;
    property EnableMessages: WordBool read Get_EnableMessages write Set_EnableMessages;
    property IsFrozen: WordBool read Get_IsFrozen;
    property QueueResults: _ArrayList read Get_QueueResults;
    property SlotResult: _CvsResultSet read Get_SlotResult;
  end;

// *********************************************************************//
// DispIntf:  ICvsResultsQueueDisp
// Flags:     (4288) Dual NonExtensible Dispatchable
// GUID:      {F2D13F4E-DD3C-4451-A529-789730C020F5}
// *********************************************************************//
  ICvsResultsQueueDisp = dispinterface
    ['{F2D13F4E-DD3C-4451-A529-789730C020F5}']
    procedure add_ResultsQueueChanged(const value: IUnknown); dispid 1610743808;
    procedure remove_ResultsQueueChanged(const value: IUnknown); dispid 1610743809;
    property IsAvailable: WordBool readonly dispid 1610743810;
    property EnableQueuing: WordBool dispid 1610743811;
    property EnableMessages: WordBool dispid 1610743813;
    property IsFrozen: WordBool readonly dispid 1610743815;
    property QueueResults: _ArrayList readonly dispid 1610743816;
    property SlotResult: _CvsResultSet readonly dispid 1610743817;
    function GetShortStatus: IUnknown; dispid 1610743818;
    function GetStatus: IUnknown; dispid 1610743819;
    procedure SetSettings(const settings: IUnknown; writeToProcSet: WordBool); dispid 1610743820;
    procedure GhostMethod_ICvsResultsQueue_80_1; dispid 1610743821;
    procedure ClearQueue; dispid 1610743822;
    procedure DeleteResult(index: Integer); dispid 1610743823;
    procedure RequestShortStatus; dispid 1610743824;
    procedure RequestStatus; dispid 1610743825;
    procedure RequestFirstSlot; dispid 1610743826;
    procedure RequestNextSlot; dispid 1610743827;
    procedure RequestSlot(slotNum: Integer); dispid 1610743828;
    procedure RequestSlotUnscaled(slotNum: Integer); dispid 1610743829;
    procedure RequestSlotRescaled(slotNum: Integer); dispid 1610743830;
    function GetThumbnailImages(startIndex: Integer; maxImages: Integer; ImageIndex: Integer; 
                                desiredSize: {??Size}OleVariant): {??PSafeArray}OleVariant; dispid 1610743831;
  end;

// *********************************************************************//
// Interface: _CvsSerialSettings
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {1E563D32-BD3F-3BFA-A484-034FFF7E0416}
// *********************************************************************//
  _CvsSerialSettings = interface(IDispatch)
    ['{1E563D32-BD3F-3BFA-A484-034FFF7E0416}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Clone: OleVariant; safecall;
    function Get_BaudRate: CvsBaudRate; safecall;
    procedure Set_BaudRate(pRetVal: CvsBaudRate); safecall;
    function Get_Parity: CvsParity; safecall;
    procedure Set_Parity(pRetVal: CvsParity); safecall;
    function Get_DataBits: CvsDataBits; safecall;
    procedure Set_DataBits(pRetVal: CvsDataBits); safecall;
    function Get_StopBits: CvsStopBits; safecall;
    procedure Set_StopBits(pRetVal: CvsStopBits); safecall;
    function Get_Handshake: CvsHandshake; safecall;
    procedure Set_Handshake(pRetVal: CvsHandshake); safecall;
    function Get_Mode: CvsMode; safecall;
    procedure Set_Mode(pRetVal: CvsMode); safecall;
    function Get_FixedInputLengthEnabled: WordBool; safecall;
    procedure Set_FixedInputLengthEnabled(pRetVal: WordBool); safecall;
    function Get_FixedInputLength: Integer; safecall;
    procedure Set_FixedInputLength(pRetVal: Integer); safecall;
    function Get_InputTerminator: Integer; safecall;
    procedure Set_InputTerminator(pRetVal: Integer); safecall;
    function Get_OutputTerminator: Integer; safecall;
    procedure Set_OutputTerminator(pRetVal: Integer); safecall;
    function Get_TriggerOnFirstByteEnabled: WordBool; safecall;
    procedure Set_TriggerOnFirstByteEnabled(pRetVal: WordBool); safecall;
    function Get_DeviceNetSendLength: Integer; safecall;
    procedure Set_DeviceNetSendLength(pRetVal: Integer); safecall;
    function Get_DeviceNetReceiveLength: Integer; safecall;
    procedure Set_DeviceNetReceiveLength(pRetVal: Integer); safecall;
    function SupportsSlowBaudRates: WordBool; safecall;
    function Get_Port: Integer; safecall;
    function IsEqual(const serialSettings: _CvsSerialSettings): WordBool; safecall;
    property ToString: WideString read Get_ToString;
    property BaudRate: CvsBaudRate read Get_BaudRate write Set_BaudRate;
    property Parity: CvsParity read Get_Parity write Set_Parity;
    property DataBits: CvsDataBits read Get_DataBits write Set_DataBits;
    property StopBits: CvsStopBits read Get_StopBits write Set_StopBits;
    property Handshake: CvsHandshake read Get_Handshake write Set_Handshake;
    property Mode: CvsMode read Get_Mode write Set_Mode;
    property FixedInputLengthEnabled: WordBool read Get_FixedInputLengthEnabled write Set_FixedInputLengthEnabled;
    property FixedInputLength: Integer read Get_FixedInputLength write Set_FixedInputLength;
    property InputTerminator: Integer read Get_InputTerminator write Set_InputTerminator;
    property OutputTerminator: Integer read Get_OutputTerminator write Set_OutputTerminator;
    property TriggerOnFirstByteEnabled: WordBool read Get_TriggerOnFirstByteEnabled write Set_TriggerOnFirstByteEnabled;
    property DeviceNetSendLength: Integer read Get_DeviceNetSendLength write Set_DeviceNetSendLength;
    property DeviceNetReceiveLength: Integer read Get_DeviceNetReceiveLength write Set_DeviceNetReceiveLength;
    property Port: Integer read Get_Port;
  end;

// *********************************************************************//
// DispIntf:  _CvsSerialSettingsDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {1E563D32-BD3F-3BFA-A484-034FFF7E0416}
// *********************************************************************//
  _CvsSerialSettingsDisp = dispinterface
    ['{1E563D32-BD3F-3BFA-A484-034FFF7E0416}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    function Clone: OleVariant; dispid 1610743812;
    property BaudRate: CvsBaudRate dispid 1610743813;
    property Parity: CvsParity dispid 1610743815;
    property DataBits: CvsDataBits dispid 1610743817;
    property StopBits: CvsStopBits dispid 1610743819;
    property Handshake: CvsHandshake dispid 1610743821;
    property Mode: CvsMode dispid 1610743823;
    property FixedInputLengthEnabled: WordBool dispid 1610743825;
    property FixedInputLength: Integer dispid 1610743827;
    property InputTerminator: Integer dispid 1610743829;
    property OutputTerminator: Integer dispid 1610743831;
    property TriggerOnFirstByteEnabled: WordBool dispid 1610743833;
    property DeviceNetSendLength: Integer dispid 1610743835;
    property DeviceNetReceiveLength: Integer dispid 1610743837;
    function SupportsSlowBaudRates: WordBool; dispid 1610743839;
    property Port: Integer readonly dispid 1610743840;
    function IsEqual(const serialSettings: _CvsSerialSettings): WordBool; dispid 1610743841;
  end;

// *********************************************************************//
// Interface: _CvsSntpCurrentData
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {25164D5D-C0F6-3635-95D6-B48A3D3C7E0E}
// *********************************************************************//
  _CvsSntpCurrentData = interface(IDispatch)
    ['{25164D5D-C0F6-3635-95D6-B48A3D3C7E0E}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Clone: OleVariant; safecall;
    function Get_TimeDateData: _CvsTimeDateData; safecall;
    function Get_SntpServer: WideString; safecall;
    property ToString: WideString read Get_ToString;
    property TimeDateData: _CvsTimeDateData read Get_TimeDateData;
    property SntpServer: WideString read Get_SntpServer;
  end;

// *********************************************************************//
// DispIntf:  _CvsSntpCurrentDataDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {25164D5D-C0F6-3635-95D6-B48A3D3C7E0E}
// *********************************************************************//
  _CvsSntpCurrentDataDisp = dispinterface
    ['{25164D5D-C0F6-3635-95D6-B48A3D3C7E0E}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    function Clone: OleVariant; dispid 1610743812;
    property TimeDateData: _CvsTimeDateData readonly dispid 1610743813;
    property SntpServer: WideString readonly dispid 1610743814;
  end;

// *********************************************************************//
// Interface: _CvsSntpSettings
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {DEA76515-4B6D-344B-AE68-59D0A36C7552}
// *********************************************************************//
  _CvsSntpSettings = interface(IDispatch)
    ['{DEA76515-4B6D-344B-AE68-59D0A36C7552}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Clone: OleVariant; safecall;
    function Get_UpdateInterval: Integer; safecall;
    procedure Set_UpdateInterval(pRetVal: Integer); safecall;
    function Get_PreferredServer: WideString; safecall;
    procedure Set_PreferredServer(const pRetVal: WideString); safecall;
    function Get_AlternateServer: WideString; safecall;
    procedure Set_AlternateServer(const pRetVal: WideString); safecall;
    function Get_TimeZoneId: Integer; safecall;
    procedure SetTimeZoneId(const timeZoneObject: _CvsTimeZones; TimeZoneId: Integer); safecall;
    function Get_AdjustForDaylightSavingsTimeEnabled: WordBool; safecall;
    procedure Set_AdjustForDaylightSavingsTimeEnabled(pRetVal: WordBool); safecall;
    procedure SetSntpAndDhcpUsage(enableSntp: WordBool; useDhcp: WordBool); safecall;
    function Get_SntpEnabled: WordBool; safecall;
    function Get_LocateUsingDhcpEnabled: WordBool; safecall;
    property ToString: WideString read Get_ToString;
    property UpdateInterval: Integer read Get_UpdateInterval write Set_UpdateInterval;
    property PreferredServer: WideString read Get_PreferredServer write Set_PreferredServer;
    property AlternateServer: WideString read Get_AlternateServer write Set_AlternateServer;
    property TimeZoneId: Integer read Get_TimeZoneId;
    property AdjustForDaylightSavingsTimeEnabled: WordBool read Get_AdjustForDaylightSavingsTimeEnabled write Set_AdjustForDaylightSavingsTimeEnabled;
    property SntpEnabled: WordBool read Get_SntpEnabled;
    property LocateUsingDhcpEnabled: WordBool read Get_LocateUsingDhcpEnabled;
  end;

// *********************************************************************//
// DispIntf:  _CvsSntpSettingsDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {DEA76515-4B6D-344B-AE68-59D0A36C7552}
// *********************************************************************//
  _CvsSntpSettingsDisp = dispinterface
    ['{DEA76515-4B6D-344B-AE68-59D0A36C7552}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    function Clone: OleVariant; dispid 1610743812;
    property UpdateInterval: Integer dispid 1610743813;
    property PreferredServer: WideString dispid 1610743815;
    property AlternateServer: WideString dispid 1610743817;
    property TimeZoneId: Integer readonly dispid 1610743819;
    procedure SetTimeZoneId(const timeZoneObject: _CvsTimeZones; TimeZoneId: Integer); dispid 1610743820;
    property AdjustForDaylightSavingsTimeEnabled: WordBool dispid 1610743821;
    procedure SetSntpAndDhcpUsage(enableSntp: WordBool; useDhcp: WordBool); dispid 1610743823;
    property SntpEnabled: WordBool readonly dispid 1610743824;
    property LocateUsingDhcpEnabled: WordBool readonly dispid 1610743825;
  end;

// *********************************************************************//
// Interface: _CvsTimeDateData
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {977B56FC-8513-3602-893F-1C7BE18A5295}
// *********************************************************************//
  _CvsTimeDateData = interface(IDispatch)
    ['{977B56FC-8513-3602-893F-1C7BE18A5295}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Clone: OleVariant; safecall;
    function Get_Seconds: Integer; safecall;
    procedure Set_Seconds(pRetVal: Integer); safecall;
    function Get_Minutes: Integer; safecall;
    procedure Set_Minutes(pRetVal: Integer); safecall;
    function Get_Hours: Integer; safecall;
    procedure Set_Hours(pRetVal: Integer); safecall;
    function Get_Day: Integer; safecall;
    procedure Set_Day(pRetVal: Integer); safecall;
    function Get_Month: Integer; safecall;
    procedure Set_Month(pRetVal: Integer); safecall;
    function Get_Year: Integer; safecall;
    procedure Set_Year(pRetVal: Integer); safecall;
    property ToString: WideString read Get_ToString;
    property Seconds: Integer read Get_Seconds write Set_Seconds;
    property Minutes: Integer read Get_Minutes write Set_Minutes;
    property Hours: Integer read Get_Hours write Set_Hours;
    property Day: Integer read Get_Day write Set_Day;
    property Month: Integer read Get_Month write Set_Month;
    property Year: Integer read Get_Year write Set_Year;
  end;

// *********************************************************************//
// DispIntf:  _CvsTimeDateDataDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {977B56FC-8513-3602-893F-1C7BE18A5295}
// *********************************************************************//
  _CvsTimeDateDataDisp = dispinterface
    ['{977B56FC-8513-3602-893F-1C7BE18A5295}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    function Clone: OleVariant; dispid 1610743812;
    property Seconds: Integer dispid 1610743813;
    property Minutes: Integer dispid 1610743815;
    property Hours: Integer dispid 1610743817;
    property Day: Integer dispid 1610743819;
    property Month: Integer dispid 1610743821;
    property Year: Integer dispid 1610743823;
  end;

// *********************************************************************//
// Interface: _CvsTimeZones
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {C29E5451-FE22-378F-B7C5-096E27211F51}
// *********************************************************************//
  _CvsTimeZones = interface(IDispatch)
    ['{C29E5451-FE22-378F-B7C5-096E27211F51}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function GetTimeZones: PSafeArray; safecall;
    function GetTimeZoneId(index: Integer): Integer; safecall;
    function GetTimeZoneIndex(TimeZoneId: Integer): Integer; safecall;
    function IsAdjustedForDST(TimeZoneId: Integer): WordBool; safecall;
    property ToString: WideString read Get_ToString;
  end;

// *********************************************************************//
// DispIntf:  _CvsTimeZonesDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {C29E5451-FE22-378F-B7C5-096E27211F51}
// *********************************************************************//
  _CvsTimeZonesDisp = dispinterface
    ['{C29E5451-FE22-378F-B7C5-096E27211F51}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    function GetTimeZones: {??PSafeArray}OleVariant; dispid 1610743812;
    function GetTimeZoneId(index: Integer): Integer; dispid 1610743813;
    function GetTimeZoneIndex(TimeZoneId: Integer): Integer; dispid 1610743814;
    function IsAdjustedForDST(TimeZoneId: Integer): WordBool; dispid 1610743815;
  end;

// *********************************************************************//
// Interface: _CvsUserCollection
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {C24C0D24-A3CC-3A66-B2E9-2D22FAD711A0}
// *********************************************************************//
  _CvsUserCollection = interface(IDispatch)
    ['{C24C0D24-A3CC-3A66-B2E9-2D22FAD711A0}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function GetEnumerator: IEnumVARIANT; safecall;
    function Get_Item(index: Integer): _CvsUser; safecall;
    function Get_Item_2(const Name: WideString): _CvsUser; safecall;
    procedure Add(const user: _CvsUser); safecall;
    procedure RemoveAt(index: Integer); safecall;
    procedure Clear; safecall;
    function Get_Count: Integer; safecall;
    function IndexOf(const user: _CvsUser): Integer; safecall;
    function IndexOf_2(const Name: WideString): Integer; safecall;
    function Get_Items: PSafeArray; safecall;
    property ToString: WideString read Get_ToString;
    property Item[index: Integer]: _CvsUser read Get_Item; default;
    property Item_2[const Name: WideString]: _CvsUser read Get_Item_2;
    property Count: Integer read Get_Count;
    property Items: PSafeArray read Get_Items;
  end;

// *********************************************************************//
// DispIntf:  _CvsUserCollectionDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {C24C0D24-A3CC-3A66-B2E9-2D22FAD711A0}
// *********************************************************************//
  _CvsUserCollectionDisp = dispinterface
    ['{C24C0D24-A3CC-3A66-B2E9-2D22FAD711A0}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 1610743810;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    function GetEnumerator: IEnumVARIANT; dispid -4;
    property Item[index: Integer]: _CvsUser readonly dispid 0; default;
    property Item_2[const Name: WideString]: _CvsUser readonly dispid 1610743814;
    procedure Add(const user: _CvsUser); dispid 1610743815;
    procedure RemoveAt(index: Integer); dispid 1610743816;
    procedure Clear; dispid 1610743817;
    property Count: Integer readonly dispid 1610743818;
    function IndexOf(const user: _CvsUser): Integer; dispid 1610743819;
    function IndexOf_2(const Name: WideString): Integer; dispid 1610743820;
    property Items: {??PSafeArray}OleVariant readonly dispid 1610743821;
  end;

// *********************************************************************//
// Interface: _CvsUtility
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {70E955A1-469A-3E28-9122-53741341E025}
// *********************************************************************//
  _CvsUtility = interface(IDispatch)
    ['{70E955A1-469A-3E28-9122-53741341E025}']
    function GetType: _Type; stdcall;
    procedure GhostMethod__CvsUtility_32_1; safecall;
    procedure GhostMethod__CvsUtility_36_2; safecall;
    procedure GhostMethod__CvsUtility_40_3; safecall;
    function CvsCellLocationFromString(const Location: WideString): CvsCellLocation; safecall;
    function CvsCellLocationFromLocation(row: Integer; column: Integer): CvsCellLocation; safecall;
    function CvsCellLocationToString(row: Integer; column: Integer; absoluteRow: WordBool; 
                                     absoluteColumn: WordBool): WideString; safecall;
    function CvsImageLocationFromLocation(row: Single; column: Single): CvsImageLocation; safecall;
    function CvsImageLocationToString(row: Single; column: Single): WideString; safecall;
    procedure CvsInSightSoftwareDevelopmentKitInitialize; safecall;
    function CvsCellRangeFromString(const range: WideString): CvsCellRange; safecall;
    function CvsCellRangeFromRange(row: Integer; column: Integer; rows: Integer; columns: Integer): CvsCellRange; safecall;
    function CvsCellRangeToString(row: Integer; column: Integer; rows: Integer; columns: Integer; 
                                  absoluteRow: WordBool; absoluteColumn: WordBool; 
                                  absoluteLastRow: WordBool; absoluteLastColumn: WordBool): WideString; safecall;
    function PingHost(const HostName: WideString; timeToWait: Integer): WordBool; safecall;
    procedure OpenDialog(const inSight: _CvsInSight; const dlgLocation: WideString; 
                         const dlgText: WideString); safecall;
    function IsValidHostNameChar(const s: WideString): WordBool; safecall;
    function IsValidSensorNameChar(const s: WideString): WordBool; safecall;
    function IsValidIPAddressChar(const s: WideString): WordBool; safecall;
    function IsValidHostName(const s: WideString; allowZeroLength: WordBool): WordBool; safecall;
    function IsValidSensorName(const s: WideString; allowZeroLength: WordBool): WordBool; safecall;
    function IsValidHostIPAddress(const address: WideString): WordBool; safecall;
    function IsValidSubnetIPAddress(const address: WideString): WordBool; safecall;
    function IsValidUserNameOrPasswordChar(const s: WideString): WordBool; safecall;
    function IsValidUserName(const s: WideString): WordBool; safecall;
    function IsValidPassword(const s: WideString): WordBool; safecall;
    function CvsDiscreteInputLineSupportsSignalSetting(Type_: CvsInputType): WordBool; safecall;
    function CvsDiscreteOutputLineSupportsPulseLengthSetting(Type_: CvsOutputType): WordBool; safecall;
    function CvsDiscreteOutputLineSupportsStrobeTriggerSelection(Type_: CvsOutputType): WordBool; safecall;
    function CvsDiscreteOutputLineSupportsPulseAndAcquisitionDelay(Type_: CvsOutputType): WordBool; safecall;
  end;

// *********************************************************************//
// DispIntf:  _CvsUtilityDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {70E955A1-469A-3E28-9122-53741341E025}
// *********************************************************************//
  _CvsUtilityDisp = dispinterface
    ['{70E955A1-469A-3E28-9122-53741341E025}']
    function GetType: _Type; dispid 1610743811;
    procedure GhostMethod__CvsUtility_32_1; dispid 1610743809;
    procedure GhostMethod__CvsUtility_36_2; dispid 1610743810;
    function CvsCellLocationFromString(const Location: WideString): {??CvsCellLocation}OleVariant; dispid 1610743812;
    function CvsCellLocationFromLocation(row: Integer; column: Integer): {??CvsCellLocation}OleVariant; dispid 1610743813;
    function CvsCellLocationToString(row: Integer; column: Integer; absoluteRow: WordBool; 
                                     absoluteColumn: WordBool): WideString; dispid 1610743814;
    function CvsImageLocationFromLocation(row: Single; column: Single): {??CvsImageLocation}OleVariant; dispid 1610743815;
    function CvsImageLocationToString(row: Single; column: Single): WideString; dispid 1610743816;
    procedure CvsInSightSoftwareDevelopmentKitInitialize; dispid 1610743817;
    function CvsCellRangeFromString(const range: WideString): {??CvsCellRange}OleVariant; dispid 1610743818;
    function CvsCellRangeFromRange(row: Integer; column: Integer; rows: Integer; columns: Integer): {??CvsCellRange}OleVariant; dispid 1610743819;
    function CvsCellRangeToString(row: Integer; column: Integer; rows: Integer; columns: Integer; 
                                  absoluteRow: WordBool; absoluteColumn: WordBool; 
                                  absoluteLastRow: WordBool; absoluteLastColumn: WordBool): WideString; dispid 1610743820;
    function PingHost(const HostName: WideString; timeToWait: Integer): WordBool; dispid 1610743821;
    procedure OpenDialog(const inSight: _CvsInSight; const dlgLocation: WideString; 
                         const dlgText: WideString); dispid 1610743822;
    function IsValidHostNameChar(const s: WideString): WordBool; dispid 1610743823;
    function IsValidSensorNameChar(const s: WideString): WordBool; dispid 1610743824;
    function IsValidIPAddressChar(const s: WideString): WordBool; dispid 1610743825;
    function IsValidHostName(const s: WideString; allowZeroLength: WordBool): WordBool; dispid 1610743826;
    function IsValidSensorName(const s: WideString; allowZeroLength: WordBool): WordBool; dispid 1610743827;
    function IsValidHostIPAddress(const address: WideString): WordBool; dispid 1610743828;
    function IsValidSubnetIPAddress(const address: WideString): WordBool; dispid 1610743829;
    function IsValidUserNameOrPasswordChar(const s: WideString): WordBool; dispid 1610743830;
    function IsValidUserName(const s: WideString): WordBool; dispid 1610743831;
    function IsValidPassword(const s: WideString): WordBool; dispid 1610743832;
    function CvsDiscreteInputLineSupportsSignalSetting(Type_: CvsInputType): WordBool; dispid 1610743833;
    function CvsDiscreteOutputLineSupportsPulseLengthSetting(Type_: CvsOutputType): WordBool; dispid 1610743834;
    function CvsDiscreteOutputLineSupportsStrobeTriggerSelection(Type_: CvsOutputType): WordBool; dispid 1610743835;
    function CvsDiscreteOutputLineSupportsPulseAndAcquisitionDelay(Type_: CvsOutputType): WordBool; dispid 1610743836;
  end;

// *********************************************************************//
// Interface: _CvsView
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {F8F91C76-5F55-33D0-BAB6-82347F515402}
// *********************************************************************//
  _CvsView = interface(IDispatch)
    ['{F8F91C76-5F55-33D0-BAB6-82347F515402}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function GetLifetimeService: OleVariant; safecall;
    function InitializeLifetimeService: OleVariant; safecall;
    function CreateObjRef(const requestedType: _Type): _ObjRef; safecall;
    function Get_IsCustomView: WordBool; safecall;
    function Get_CurrentLocation: CvsCellLocation; safecall;
    procedure Set_CurrentLocation(pRetVal: CvsCellLocation); safecall;
    function Get_VisibleCells: CvsCellRange; safecall;
    function Get_ActiveRange: CvsCellRange; safecall;
    procedure Set_ActiveRange(pRetVal: CvsCellRange); safecall;
    function Get_IsDefined: WordBool; safecall;
    function Shift(range: CvsCellRange; Insert: WordBool): WordBool; safecall;
    function Get_bounds: Rectangle; safecall;
    procedure Set_bounds(pRetVal: Rectangle); safecall;
    procedure SetCurrentAndVisibleCells(row: Integer; col: Integer; visibleRow: Integer; 
                                        visibleColumn: Integer; visibleRows: Integer; 
                                        visibleColumns: Integer); safecall;
    property ToString: WideString read Get_ToString;
    property IsCustomView: WordBool read Get_IsCustomView;
    property CurrentLocation: CvsCellLocation read Get_CurrentLocation write Set_CurrentLocation;
    property VisibleCells: CvsCellRange read Get_VisibleCells;
    property ActiveRange: CvsCellRange read Get_ActiveRange write Set_ActiveRange;
    property IsDefined: WordBool read Get_IsDefined;
    property bounds: Rectangle read Get_bounds write Set_bounds;
  end;

// *********************************************************************//
// DispIntf:  _CvsViewDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {F8F91C76-5F55-33D0-BAB6-82347F515402}
// *********************************************************************//
  _CvsViewDisp = dispinterface
    ['{F8F91C76-5F55-33D0-BAB6-82347F515402}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    function GetLifetimeService: OleVariant; dispid 1610743812;
    function InitializeLifetimeService: OleVariant; dispid 1610743813;
    function CreateObjRef(const requestedType: _Type): _ObjRef; dispid 1610743814;
    property IsCustomView: WordBool readonly dispid 1610743815;
    property CurrentLocation: {??CvsCellLocation}OleVariant dispid 1610743816;
    property VisibleCells: {??CvsCellRange}OleVariant readonly dispid 1610743818;
    property ActiveRange: {??CvsCellRange}OleVariant dispid 1610743819;
    property IsDefined: WordBool readonly dispid 1610743821;
    function Shift(range: {??CvsCellRange}OleVariant; Insert: WordBool): WordBool; dispid 1610743822;
    property bounds: {??Rectangle}OleVariant dispid 1610743823;
    procedure SetCurrentAndVisibleCells(row: Integer; col: Integer; visibleRow: Integer; 
                                        visibleColumn: Integer; visibleRows: Integer; 
                                        visibleColumns: Integer); dispid 1610743825;
  end;

// *********************************************************************//
// Interface: _CvsStateChangedEventArgs
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {CCFFFE15-804E-3FC2-BE38-8ABFD9874F1A}
// *********************************************************************//
  _CvsStateChangedEventArgs = interface(IDispatch)
    ['{CCFFFE15-804E-3FC2-BE38-8ABFD9874F1A}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Get_State: CvsInSightState; safecall;
    function Get_Reason: CvsInSightStateChangedReason; safecall;
    property ToString: WideString read Get_ToString;
    property State: CvsInSightState read Get_State;
    property Reason: CvsInSightStateChangedReason read Get_Reason;
  end;

// *********************************************************************//
// DispIntf:  _CvsStateChangedEventArgsDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {CCFFFE15-804E-3FC2-BE38-8ABFD9874F1A}
// *********************************************************************//
  _CvsStateChangedEventArgsDisp = dispinterface
    ['{CCFFFE15-804E-3FC2-BE38-8ABFD9874F1A}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    property State: CvsInSightState readonly dispid 1610743812;
    property Reason: CvsInSightStateChangedReason readonly dispid 1610743813;
  end;

// *********************************************************************//
// Interface: _CvsConnectCompletedEventArgs
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {E9D641E0-5A5B-3485-AB2E-C3CDCDAE3EE9}
// *********************************************************************//
  _CvsConnectCompletedEventArgs = interface(IDispatch)
    ['{E9D641E0-5A5B-3485-AB2E-C3CDCDAE3EE9}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Get_ErrorNumber: Integer; safecall;
    function Get_ErrorMessage: WideString; safecall;
    property ToString: WideString read Get_ToString;
    property ErrorNumber: Integer read Get_ErrorNumber;
    property ErrorMessage: WideString read Get_ErrorMessage;
  end;

// *********************************************************************//
// DispIntf:  _CvsConnectCompletedEventArgsDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {E9D641E0-5A5B-3485-AB2E-C3CDCDAE3EE9}
// *********************************************************************//
  _CvsConnectCompletedEventArgsDisp = dispinterface
    ['{E9D641E0-5A5B-3485-AB2E-C3CDCDAE3EE9}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    property ErrorNumber: Integer readonly dispid 1610743812;
    property ErrorMessage: WideString readonly dispid 1610743813;
  end;

// *********************************************************************//
// Interface: _CvsInSightMessageEventArgs
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {DF03592F-4988-3B71-91DB-8A57DBB86000}
// *********************************************************************//
  _CvsInSightMessageEventArgs = interface(IDispatch)
    ['{DF03592F-4988-3B71-91DB-8A57DBB86000}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Get_MessageId: CvsInSightMessageId; safecall;
    function Get_Message: WideString; safecall;
    property ToString: WideString read Get_ToString;
    property MessageId: CvsInSightMessageId read Get_MessageId;
    property Message: WideString read Get_Message;
  end;

// *********************************************************************//
// DispIntf:  _CvsInSightMessageEventArgsDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {DF03592F-4988-3B71-91DB-8A57DBB86000}
// *********************************************************************//
  _CvsInSightMessageEventArgsDisp = dispinterface
    ['{DF03592F-4988-3B71-91DB-8A57DBB86000}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    property MessageId: CvsInSightMessageId readonly dispid 1610743812;
    property Message: WideString readonly dispid 1610743813;
  end;

// *********************************************************************//
// Interface: _CvsLoadCompletedEventArgs
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {DC302895-CD8E-3EF6-A3E1-FD6CDFCB4847}
// *********************************************************************//
  _CvsLoadCompletedEventArgs = interface(IDispatch)
    ['{DC302895-CD8E-3EF6-A3E1-FD6CDFCB4847}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Get_Succeeded: WordBool; safecall;
    function Get_JobLoaded: WordBool; safecall;
    function Get_JobCreated: WordBool; safecall;
    property ToString: WideString read Get_ToString;
    property Succeeded: WordBool read Get_Succeeded;
    property JobLoaded: WordBool read Get_JobLoaded;
    property JobCreated: WordBool read Get_JobCreated;
  end;

// *********************************************************************//
// DispIntf:  _CvsLoadCompletedEventArgsDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {DC302895-CD8E-3EF6-A3E1-FD6CDFCB4847}
// *********************************************************************//
  _CvsLoadCompletedEventArgsDisp = dispinterface
    ['{DC302895-CD8E-3EF6-A3E1-FD6CDFCB4847}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    property Succeeded: WordBool readonly dispid 1610743812;
    property JobLoaded: WordBool readonly dispid 1610743813;
    property JobCreated: WordBool readonly dispid 1610743814;
  end;

// *********************************************************************//
// Interface: _CvsSaveCompletedEventArgs
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {750D5AF2-DE37-3F44-9BDC-BCF2BB1387BF}
// *********************************************************************//
  _CvsSaveCompletedEventArgs = interface(IDispatch)
    ['{750D5AF2-DE37-3F44-9BDC-BCF2BB1387BF}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Get_Succeeded: WordBool; safecall;
    property ToString: WideString read Get_ToString;
    property Succeeded: WordBool read Get_Succeeded;
  end;

// *********************************************************************//
// DispIntf:  _CvsSaveCompletedEventArgsDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {750D5AF2-DE37-3F44-9BDC-BCF2BB1387BF}
// *********************************************************************//
  _CvsSaveCompletedEventArgsDisp = dispinterface
    ['{750D5AF2-DE37-3F44-9BDC-BCF2BB1387BF}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    property Succeeded: WordBool readonly dispid 1610743812;
  end;

// *********************************************************************//
// DispIntf:  ICvsInSightEvents
// Flags:     (4224) NonExtensible Dispatchable
// GUID:      {463FA70B-A051-47B1-8A03-E889EC28CE8F}
// *********************************************************************//
  ICvsInSightEvents = dispinterface
    ['{463FA70B-A051-47B1-8A03-E889EC28CE8F}']
    procedure StateChanged(sender: OleVariant; const e: _CvsStateChangedEventArgs); dispid 1;
    procedure ResultsChanged(sender: OleVariant; const e: _EventArgs); dispid 2;
    procedure LiveAcquisitionChanged(sender: OleVariant; const e: _EventArgs); dispid 3;
    procedure InSightMessage(sender: OleVariant; const e: _CvsInSightMessageEventArgs); dispid 4;
    procedure SensorChanged(sender: OleVariant; const e: _EventArgs); dispid 5;
    procedure ConnectCompleted(sender: OleVariant; const e: _CvsConnectCompletedEventArgs); dispid 6;
    procedure LoadCompleted(sender: OleVariant; const e: _CvsLoadCompletedEventArgs); dispid 7;
    procedure SaveCompleted(sender: OleVariant; const e: _CvsSaveCompletedEventArgs); dispid 8;
  end;

// *********************************************************************//
// Interface: _CvsAcceptMessageHandler
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {05C68D21-17FE-3028-9255-CFF83EE8A11D}
// *********************************************************************//
  _CvsAcceptMessageHandler = interface(IDispatch)
    ['{05C68D21-17FE-3028-9255-CFF83EE8A11D}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function GetInvocationList: PSafeArray; safecall;
    function Clone: OleVariant; safecall;
    procedure GetObjectData(const info: _SerializationInfo; context: StreamingContext); safecall;
    function DynamicInvoke(args: PSafeArray): OleVariant; safecall;
    function Get_Method: _MethodInfo; safecall;
    function Get_Target: OleVariant; safecall;
    function Invoke_2: WordBool; safecall;
    function BeginInvoke(const callback: _AsyncCallback; object_: OleVariant): IAsyncResult; safecall;
    function EndInvoke(const result: IAsyncResult): WordBool; safecall;
    property ToString: WideString read Get_ToString;
    property Method: _MethodInfo read Get_Method;
    property Target: OleVariant read Get_Target;
  end;

// *********************************************************************//
// DispIntf:  _CvsAcceptMessageHandlerDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {05C68D21-17FE-3028-9255-CFF83EE8A11D}
// *********************************************************************//
  _CvsAcceptMessageHandlerDisp = dispinterface
    ['{05C68D21-17FE-3028-9255-CFF83EE8A11D}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    function GetInvocationList: {??PSafeArray}OleVariant; dispid 1610743812;
    function Clone: OleVariant; dispid 1610743813;
    procedure GetObjectData(const info: _SerializationInfo; context: {??StreamingContext}OleVariant); dispid 1610743814;
    function DynamicInvoke(args: {??PSafeArray}OleVariant): OleVariant; dispid 1610743815;
    property Method: _MethodInfo readonly dispid 1610743816;
    property Target: OleVariant readonly dispid 1610743817;
    function Invoke_2: WordBool; dispid 1610743818;
    function BeginInvoke(const callback: _AsyncCallback; object_: OleVariant): IAsyncResult; dispid 1610743819;
    function EndInvoke(const result: IAsyncResult): WordBool; dispid 1610743820;
  end;

// *********************************************************************//
// Interface: _BadSnippetException
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {9CF08882-4D11-3683-BC48-BAD12DAA1EC3}
// *********************************************************************//
  _BadSnippetException = interface(IDispatch)
    ['{9CF08882-4D11-3683-BC48-BAD12DAA1EC3}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function Get_data: IDictionary; safecall;
    procedure GetObjectData(const info: _SerializationInfo; context: StreamingContext); safecall;
    function GetType_2: _Type; safecall;
    function Get_Message: WideString; safecall;
    function GetBaseException: _Exception; safecall;
    function Get_StackTrace: WideString; safecall;
    function Get_HelpLink: WideString; safecall;
    procedure Set_HelpLink(const pRetVal: WideString); safecall;
    function Get_source: WideString; safecall;
    procedure Set_source(const pRetVal: WideString); safecall;
    function Get_InnerException: _Exception; safecall;
    function Get_TargetSite: _MethodBase; safecall;
    function Get_HResult: Integer; safecall;
    property ToString: WideString read Get_ToString;
    property data: IDictionary read Get_data;
    property Message: WideString read Get_Message;
    property StackTrace: WideString read Get_StackTrace;
    property HelpLink: WideString read Get_HelpLink write Set_HelpLink;
    property source: WideString read Get_source write Set_source;
    property InnerException: _Exception read Get_InnerException;
    property TargetSite: _MethodBase read Get_TargetSite;
    property HResult: Integer read Get_HResult;
  end;

// *********************************************************************//
// DispIntf:  _BadSnippetExceptionDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {9CF08882-4D11-3683-BC48-BAD12DAA1EC3}
// *********************************************************************//
  _BadSnippetExceptionDisp = dispinterface
    ['{9CF08882-4D11-3683-BC48-BAD12DAA1EC3}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    property data: IDictionary readonly dispid 1610743812;
    procedure GetObjectData(const info: _SerializationInfo; context: {??StreamingContext}OleVariant); dispid 1610743813;
    function GetType_2: _Type; dispid 1610743814;
    property Message: WideString readonly dispid 1610743815;
    function GetBaseException: _Exception; dispid 1610743816;
    property StackTrace: WideString readonly dispid 1610743817;
    property HelpLink: WideString dispid 1610743818;
    property source: WideString dispid 1610743820;
    property InnerException: _Exception readonly dispid 1610743822;
    property TargetSite: _MethodBase readonly dispid 1610743823;
    property HResult: Integer readonly dispid 1610743824;
  end;

// *********************************************************************//
// Interface: _Snippet
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {480E2AFC-DC3E-3692-A444-1EBD575A26CB}
// *********************************************************************//
  _Snippet = interface(IDispatch)
    ['{480E2AFC-DC3E-3692-A444-1EBD575A26CB}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    property ToString: WideString read Get_ToString;
  end;

// *********************************************************************//
// DispIntf:  _SnippetDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {480E2AFC-DC3E-3692-A444-1EBD575A26CB}
// *********************************************************************//
  _SnippetDisp = dispinterface
    ['{480E2AFC-DC3E-3692-A444-1EBD575A26CB}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
  end;

// *********************************************************************//
// Interface: _Syslog
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {F77E96BA-4F7D-3336-93E3-066F35F4B401}
// *********************************************************************//
  _Syslog = interface(IDispatch)
    ['{F77E96BA-4F7D-3336-93E3-066F35F4B401}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    property ToString: WideString read Get_ToString;
  end;

// *********************************************************************//
// DispIntf:  _SyslogDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {F77E96BA-4F7D-3336-93E3-066F35F4B401}
// *********************************************************************//
  _SyslogDisp = dispinterface
    ['{F77E96BA-4F7D-3336-93E3-066F35F4B401}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
  end;

// *********************************************************************//
// Interface: _SyslogTraceListener
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {2FF29391-A107-3F22-9500-285CF5FB43F9}
// *********************************************************************//
  _SyslogTraceListener = interface(IDispatch)
    ['{2FF29391-A107-3F22-9500-285CF5FB43F9}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function GetLifetimeService: OleVariant; safecall;
    function InitializeLifetimeService: OleVariant; safecall;
    function CreateObjRef(const requestedType: _Type): _ObjRef; safecall;
    function Get_Name: WideString; safecall;
    procedure Set_Name(const pRetVal: WideString); safecall;
    function Get_IsThreadSafe: WordBool; safecall;
    procedure Close; safecall;
    procedure Flush; safecall;
    procedure Fail(const Message: WideString); safecall;
    procedure Fail_2(const Message: WideString; const detailMessage: WideString); safecall;
    procedure Write(const Message: WideString); safecall;
    procedure Write_2(o: OleVariant); safecall;
    procedure Write_3(const Message: WideString; const category: WideString); safecall;
    procedure Write_4(o: OleVariant; const category: WideString); safecall;
    procedure WriteLine(const Message: WideString); safecall;
    procedure WriteLine_2(o: OleVariant); safecall;
    procedure WriteLine_3(const Message: WideString; const category: WideString); safecall;
    procedure WriteLine_4(o: OleVariant; const category: WideString); safecall;
    procedure Dispose; safecall;
    function Get_Attributes: IUnknown; safecall;
    function Get_IndentLevel: Integer; safecall;
    procedure Set_IndentLevel(pRetVal: Integer); safecall;
    function Get_IndentSize: Integer; safecall;
    procedure Set_IndentSize(pRetVal: Integer); safecall;
    property ToString: WideString read Get_ToString;
    property Name: WideString read Get_Name write Set_Name;
    property IsThreadSafe: WordBool read Get_IsThreadSafe;
    property Attributes: IUnknown read Get_Attributes;
    property IndentLevel: Integer read Get_IndentLevel write Set_IndentLevel;
    property IndentSize: Integer read Get_IndentSize write Set_IndentSize;
  end;

// *********************************************************************//
// DispIntf:  _SyslogTraceListenerDisp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {2FF29391-A107-3F22-9500-285CF5FB43F9}
// *********************************************************************//
  _SyslogTraceListenerDisp = dispinterface
    ['{2FF29391-A107-3F22-9500-285CF5FB43F9}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    function GetLifetimeService: OleVariant; dispid 1610743812;
    function InitializeLifetimeService: OleVariant; dispid 1610743813;
    function CreateObjRef(const requestedType: _Type): _ObjRef; dispid 1610743814;
    property Name: WideString dispid 1610743815;
    property IsThreadSafe: WordBool readonly dispid 1610743817;
    procedure Close; dispid 1610743818;
    procedure Flush; dispid 1610743819;
    procedure Fail(const Message: WideString); dispid 1610743820;
    procedure Fail_2(const Message: WideString; const detailMessage: WideString); dispid 1610743821;
    procedure Write(const Message: WideString); dispid 1610743822;
    procedure Write_2(o: OleVariant); dispid 1610743823;
    procedure Write_3(const Message: WideString; const category: WideString); dispid 1610743824;
    procedure Write_4(o: OleVariant; const category: WideString); dispid 1610743825;
    procedure WriteLine(const Message: WideString); dispid 1610743826;
    procedure WriteLine_2(o: OleVariant); dispid 1610743827;
    procedure WriteLine_3(const Message: WideString; const category: WideString); dispid 1610743828;
    procedure WriteLine_4(o: OleVariant; const category: WideString); dispid 1610743829;
    procedure Dispose; dispid 1610743830;
    property Attributes: IUnknown readonly dispid 1610743831;
    property IndentLevel: Integer dispid 1610743832;
    property IndentSize: Integer dispid 1610743834;
  end;

// *********************************************************************//
// Interface: _ArgUpdateHandler_2
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {4CEC5DEB-4FBC-376D-B9A9-3B4233F13FCB}
// *********************************************************************//
  _ArgUpdateHandler_2 = interface(IDispatch)
    ['{4CEC5DEB-4FBC-376D-B9A9-3B4233F13FCB}']
    function GetType: _Type; stdcall;
    function GetHashCode: Integer; stdcall;
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function GetInvocationList: PSafeArray; safecall;
    function Clone: OleVariant; safecall;
    procedure GetObjectData(const info: _SerializationInfo; context: StreamingContext); safecall;
    function DynamicInvoke(args: PSafeArray): OleVariant; safecall;
    function Get_Method: _MethodInfo; safecall;
    function Get_Target: OleVariant; safecall;
    procedure Invoke_2; safecall;
    function BeginInvoke(const callback: _AsyncCallback; object_: OleVariant): IAsyncResult; safecall;
    procedure EndInvoke(const result: IAsyncResult); safecall;
    property ToString: WideString read Get_ToString;
    property Method: _MethodInfo read Get_Method;
    property Target: OleVariant read Get_Target;
  end;

// *********************************************************************//
// DispIntf:  _ArgUpdateHandler_2Disp
// Flags:     (4304) Hidden Dual NonExtensible Dispatchable
// GUID:      {4CEC5DEB-4FBC-376D-B9A9-3B4233F13FCB}
// *********************************************************************//
  _ArgUpdateHandler_2Disp = dispinterface
    ['{4CEC5DEB-4FBC-376D-B9A9-3B4233F13FCB}']
    function GetType: _Type; dispid 1610743808;
    function GetHashCode: Integer; dispid 1610743809;
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743811;
    function GetInvocationList: {??PSafeArray}OleVariant; dispid 1610743812;
    function Clone: OleVariant; dispid 1610743813;
    procedure GetObjectData(const info: _SerializationInfo; context: {??StreamingContext}OleVariant); dispid 1610743814;
    function DynamicInvoke(args: {??PSafeArray}OleVariant): OleVariant; dispid 1610743815;
    property Method: _MethodInfo readonly dispid 1610743816;
    property Target: OleVariant readonly dispid 1610743817;
    procedure Invoke_2; dispid 1610743818;
    function BeginInvoke(const callback: _AsyncCallback; object_: OleVariant): IAsyncResult; dispid 1610743819;
    procedure EndInvoke(const result: IAsyncResult); dispid 1610743820;
  end;

// *********************************************************************//
// The Class CoCvsActionToggle provides a Create and CreateRemote method to          
// create instances of the default interface ICvsActionToggle exposed by              
// the CoClass CvsActionToggle. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsActionToggle = class
    class function Create: ICvsActionToggle;
    class function CreateRemote(const MachineName: string): ICvsActionToggle;
  end;

// *********************************************************************//
// The Class CoCvsAction provides a Create and CreateRemote method to          
// create instances of the default interface ICvsAction exposed by              
// the CoClass CvsAction. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsAction = class
    class function Create: ICvsAction;
    class function CreateRemote(const MachineName: string): ICvsAction;
  end;

// *********************************************************************//
// The Class CoCvsCell provides a Create and CreateRemote method to          
// create instances of the default interface _CvsCell exposed by              
// the CoClass CvsCell. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsCell = class
    class function Create: _CvsCell;
    class function CreateRemote(const MachineName: string): _CvsCell;
  end;

// *********************************************************************//
// The Class CoCvsCellOcrMaxAutoTune provides a Create and CreateRemote method to          
// create instances of the default interface _CvsCellOcrMaxAutoTune exposed by              
// the CoClass CvsCellOcrMaxAutoTune. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsCellOcrMaxAutoTune = class
    class function Create: _CvsCellOcrMaxAutoTune;
    class function CreateRemote(const MachineName: string): _CvsCellOcrMaxAutoTune;
  end;

// *********************************************************************//
// The Class CoCvsCrcResponse provides a Create and CreateRemote method to          
// create instances of the default interface _CvsCrcResponse exposed by              
// the CoClass CvsCrcResponse. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsCrcResponse = class
    class function Create: _CvsCrcResponse;
    class function CreateRemote(const MachineName: string): _CvsCrcResponse;
  end;

// *********************************************************************//
// The Class CoCvsCellString provides a Create and CreateRemote method to          
// create instances of the default interface _CvsCellString exposed by              
// the CoClass CvsCellString. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsCellString = class
    class function Create: _CvsCellString;
    class function CreateRemote(const MachineName: string): _CvsCellString;
  end;

// *********************************************************************//
// The Class CoCvsCellAuthorize provides a Create and CreateRemote method to          
// create instances of the default interface _CvsCellAuthorize exposed by              
// the CoClass CvsCellAuthorize. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsCellAuthorize = class
    class function Create: _CvsCellAuthorize;
    class function CreateRemote(const MachineName: string): _CvsCellAuthorize;
  end;

// *********************************************************************//
// The Class CoCvsCellEditGraphic provides a Create and CreateRemote method to          
// create instances of the default interface _CvsCellEditGraphic exposed by              
// the CoClass CvsCellEditGraphic. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsCellEditGraphic = class
    class function Create: _CvsCellEditGraphic;
    class function CreateRemote(const MachineName: string): _CvsCellEditGraphic;
  end;

// *********************************************************************//
// The Class CoCvsCellEditMultiGraphics provides a Create and CreateRemote method to          
// create instances of the default interface _CvsCellEditMultiGraphics exposed by              
// the CoClass CvsCellEditMultiGraphics. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsCellEditMultiGraphics = class
    class function Create: _CvsCellEditMultiGraphics;
    class function CreateRemote(const MachineName: string): _CvsCellEditMultiGraphics;
  end;

// *********************************************************************//
// The Class CoCvsCellReference provides a Create and CreateRemote method to          
// create instances of the default interface _CvsCellReference exposed by              
// the CoClass CvsCellReference. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsCellReference = class
    class function Create: _CvsCellReference;
    class function CreateRemote(const MachineName: string): _CvsCellReference;
  end;

// *********************************************************************//
// The Class CoCvsCellReferenceInfo provides a Create and CreateRemote method to          
// create instances of the default interface _CvsCellReferenceInfo exposed by              
// the CoClass CvsCellReferenceInfo. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsCellReferenceInfo = class
    class function Create: _CvsCellReferenceInfo;
    class function CreateRemote(const MachineName: string): _CvsCellReferenceInfo;
  end;

// *********************************************************************//
// The Class CoCvsCellReferenceTable provides a Create and CreateRemote method to          
// create instances of the default interface _CvsCellReferenceTable exposed by              
// the CoClass CvsCellReferenceTable. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsCellReferenceTable = class
    class function Create: _CvsCellReferenceTable;
    class function CreateRemote(const MachineName: string): _CvsCellReferenceTable;
  end;

// *********************************************************************//
// The Class CoCvsDefaultRegion provides a Create and CreateRemote method to          
// create instances of the default interface _CvsDefaultRegion exposed by              
// the CoClass CvsDefaultRegion. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsDefaultRegion = class
    class function Create: _CvsDefaultRegion;
    class function CreateRemote(const MachineName: string): _CvsDefaultRegion;
  end;

// *********************************************************************//
// The Class CoCvsEasyView provides a Create and CreateRemote method to          
// create instances of the default interface _CvsEasyView exposed by              
// the CoClass CvsEasyView. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsEasyView = class
    class function Create: _CvsEasyView;
    class function CreateRemote(const MachineName: string): _CvsEasyView;
  end;

// *********************************************************************//
// The Class CoCvsEasyViewItem provides a Create and CreateRemote method to          
// create instances of the default interface _CvsEasyViewItem exposed by              
// the CoClass CvsEasyViewItem. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsEasyViewItem = class
    class function Create: _CvsEasyViewItem;
    class function CreateRemote(const MachineName: string): _CvsEasyViewItem;
  end;

// *********************************************************************//
// The Class CoCvsFeatureLicense provides a Create and CreateRemote method to          
// create instances of the default interface _CvsFeatureLicense exposed by              
// the CoClass CvsFeatureLicense. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsFeatureLicense = class
    class function Create: _CvsFeatureLicense;
    class function CreateRemote(const MachineName: string): _CvsFeatureLicense;
  end;

// *********************************************************************//
// The Class CoCvsIspMessageStore provides a Create and CreateRemote method to          
// create instances of the default interface _CvsIspMessageStore exposed by              
// the CoClass CvsIspMessageStore. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsIspMessageStore = class
    class function Create: _CvsIspMessageStore;
    class function CreateRemote(const MachineName: string): _CvsIspMessageStore;
  end;

// *********************************************************************//
// The Class CoCvsJobServerSettings provides a Create and CreateRemote method to          
// create instances of the default interface _CvsJobServerSettings exposed by              
// the CoClass CvsJobServerSettings. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsJobServerSettings = class
    class function Create: _CvsJobServerSettings;
    class function CreateRemote(const MachineName: string): _CvsJobServerSettings;
  end;

// *********************************************************************//
// The Class CoCvsOcrMaxDiagnosticData provides a Create and CreateRemote method to          
// create instances of the default interface _CvsOcrMaxDiagnosticData exposed by              
// the CoClass CvsOcrMaxDiagnosticData. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsOcrMaxDiagnosticData = class
    class function Create: _CvsOcrMaxDiagnosticData;
    class function CreateRemote(const MachineName: string): _CvsOcrMaxDiagnosticData;
  end;

// *********************************************************************//
// The Class CoFragmentStatistics provides a Create and CreateRemote method to          
// create instances of the default interface _FragmentStatistics exposed by              
// the CoClass FragmentStatistics. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoFragmentStatistics = class
    class function Create: _FragmentStatistics;
    class function CreateRemote(const MachineName: string): _FragmentStatistics;
  end;

// *********************************************************************//
// The Class CoCharacterStatistics provides a Create and CreateRemote method to          
// create instances of the default interface _CharacterStatistics exposed by              
// the CoClass CharacterStatistics. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCharacterStatistics = class
    class function Create: _CharacterStatistics;
    class function CreateRemote(const MachineName: string): _CharacterStatistics;
  end;

// *********************************************************************//
// The Class CoCvsOcrMaxDiagnostics provides a Create and CreateRemote method to          
// create instances of the default interface _CvsOcrMaxDiagnostics exposed by              
// the CoClass CvsOcrMaxDiagnostics. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsOcrMaxDiagnostics = class
    class function Create: _CvsOcrMaxDiagnostics;
    class function CreateRemote(const MachineName: string): _CvsOcrMaxDiagnostics;
  end;

// *********************************************************************//
// The Class CoCvsOcrMaxExpressionEditorBase provides a Create and CreateRemote method to          
// create instances of the default interface _CvsOcrMaxExpressionEditorBase exposed by              
// the CoClass CvsOcrMaxExpressionEditorBase. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsOcrMaxExpressionEditorBase = class
    class function Create: _CvsOcrMaxExpressionEditorBase;
    class function CreateRemote(const MachineName: string): _CvsOcrMaxExpressionEditorBase;
  end;

// *********************************************************************//
// The Class CoCvsOcrMaxSpreadsheetExpressionEditor provides a Create and CreateRemote method to          
// create instances of the default interface _CvsOcrMaxSpreadsheetExpressionEditor exposed by              
// the CoClass CvsOcrMaxSpreadsheetExpressionEditor. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsOcrMaxSpreadsheetExpressionEditor = class
    class function Create: _CvsOcrMaxSpreadsheetExpressionEditor;
    class function CreateRemote(const MachineName: string): _CvsOcrMaxSpreadsheetExpressionEditor;
  end;

// *********************************************************************//
// The Class CoArgUpdateHandler provides a Create and CreateRemote method to          
// create instances of the default interface _ArgUpdateHandler exposed by              
// the CoClass ArgUpdateHandler. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoArgUpdateHandler = class
    class function Create: _ArgUpdateHandler;
    class function CreateRemote(const MachineName: string): _ArgUpdateHandler;
  end;

// *********************************************************************//
// The Class CoCvsPowerlinkSettings provides a Create and CreateRemote method to          
// create instances of the default interface _CvsPowerlinkSettings exposed by              
// the CoClass CvsPowerlinkSettings. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsPowerlinkSettings = class
    class function Create: _CvsPowerlinkSettings;
    class function CreateRemote(const MachineName: string): _CvsPowerlinkSettings;
  end;

// *********************************************************************//
// The Class CoCvsRemoteSubnetCollection provides a Create and CreateRemote method to          
// create instances of the default interface _CvsRemoteSubnetCollection exposed by              
// the CoClass CvsRemoteSubnetCollection. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsRemoteSubnetCollection = class
    class function Create: _CvsRemoteSubnetCollection;
    class function CreateRemote(const MachineName: string): _CvsRemoteSubnetCollection;
  end;

// *********************************************************************//
// The Class CoCvsRemoteSubnetEntry provides a Create and CreateRemote method to          
// create instances of the default interface _CvsRemoteSubnetEntry exposed by              
// the CoClass CvsRemoteSubnetEntry. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsRemoteSubnetEntry = class
    class function Create: _CvsRemoteSubnetEntry;
    class function CreateRemote(const MachineName: string): _CvsRemoteSubnetEntry;
  end;

// *********************************************************************//
// The Class CoCvsSensor provides a Create and CreateRemote method to          
// create instances of the default interface _CvsSensor exposed by              
// the CoClass CvsSensor. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsSensor = class
    class function Create: _CvsSensor;
    class function CreateRemote(const MachineName: string): _CvsSensor;
  end;

// *********************************************************************//
// The Class CoCvsUser provides a Create and CreateRemote method to          
// create instances of the default interface _CvsUser exposed by              
// the CoClass CvsUser. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsUser = class
    class function Create: _CvsUser;
    class function CreateRemote(const MachineName: string): _CvsUser;
  end;

// *********************************************************************//
// The Class CoCvsInSightExtensions provides a Create and CreateRemote method to          
// create instances of the default interface _CvsInSightExtensions exposed by              
// the CoClass CvsInSightExtensions. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsInSightExtensions = class
    class function Create: _CvsInSightExtensions;
    class function CreateRemote(const MachineName: string): _CvsInSightExtensions;
  end;

// *********************************************************************//
// The Class CoCvsSettings provides a Create and CreateRemote method to          
// create instances of the default interface _CvsSettings exposed by              
// the CoClass CvsSettings. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsSettings = class
    class function Create: _CvsSettings;
    class function CreateRemote(const MachineName: string): _CvsSettings;
  end;

// *********************************************************************//
// The Class CoUndoAction provides a Create and CreateRemote method to          
// create instances of the default interface _UndoAction exposed by              
// the CoClass UndoAction. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoUndoAction = class
    class function Create: _UndoAction;
    class function CreateRemote(const MachineName: string): _UndoAction;
  end;

// *********************************************************************//
// The Class CoUndoCellTagsAction provides a Create and CreateRemote method to          
// create instances of the default interface _UndoCellTagsAction exposed by              
// the CoClass UndoCellTagsAction. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoUndoCellTagsAction = class
    class function Create: _UndoCellTagsAction;
    class function CreateRemote(const MachineName: string): _UndoCellTagsAction;
  end;

// *********************************************************************//
// The Class CoUndoClearAllAction provides a Create and CreateRemote method to          
// create instances of the default interface _UndoClearAllAction exposed by              
// the CoClass UndoClearAllAction. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoUndoClearAllAction = class
    class function Create: _UndoClearAllAction;
    class function CreateRemote(const MachineName: string): _UndoClearAllAction;
  end;

// *********************************************************************//
// The Class CoUndoCutAndPasteAction provides a Create and CreateRemote method to          
// create instances of the default interface _UndoCutAndPasteAction exposed by              
// the CoClass UndoCutAndPasteAction. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoUndoCutAndPasteAction = class
    class function Create: _UndoCutAndPasteAction;
    class function CreateRemote(const MachineName: string): _UndoCutAndPasteAction;
  end;

// *********************************************************************//
// The Class CoUndoEventArgs provides a Create and CreateRemote method to          
// create instances of the default interface _UndoEventArgs exposed by              
// the CoClass UndoEventArgs. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoUndoEventArgs = class
    class function Create: _UndoEventArgs;
    class function CreateRemote(const MachineName: string): _UndoEventArgs;
  end;

// *********************************************************************//
// The Class CoUndoExpressionAction provides a Create and CreateRemote method to          
// create instances of the default interface _UndoExpressionAction exposed by              
// the CoClass UndoExpressionAction. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoUndoExpressionAction = class
    class function Create: _UndoExpressionAction;
    class function CreateRemote(const MachineName: string): _UndoExpressionAction;
  end;

// *********************************************************************//
// The Class CoUndoOpcTagEditorAction provides a Create and CreateRemote method to          
// create instances of the default interface _UndoOpcTagEditorAction exposed by              
// the CoClass UndoOpcTagEditorAction. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoUndoOpcTagEditorAction = class
    class function Create: _UndoOpcTagEditorAction;
    class function CreateRemote(const MachineName: string): _UndoOpcTagEditorAction;
  end;

// *********************************************************************//
// The Class CoUndoPropertySheetAction provides a Create and CreateRemote method to          
// create instances of the default interface _UndoPropertySheetAction exposed by              
// the CoClass UndoPropertySheetAction. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoUndoPropertySheetAction = class
    class function Create: _UndoPropertySheetAction;
    class function CreateRemote(const MachineName: string): _UndoPropertySheetAction;
  end;

// *********************************************************************//
// The Class CoUndoReferenceRedirectAction provides a Create and CreateRemote method to          
// create instances of the default interface _UndoReferenceRedirectAction exposed by              
// the CoClass UndoReferenceRedirectAction. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoUndoReferenceRedirectAction = class
    class function Create: _UndoReferenceRedirectAction;
    class function CreateRemote(const MachineName: string): _UndoReferenceRedirectAction;
  end;

// *********************************************************************//
// The Class CoUndoScriptChangeAction provides a Create and CreateRemote method to          
// create instances of the default interface _UndoScriptChangeAction exposed by              
// the CoClass UndoScriptChangeAction. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoUndoScriptChangeAction = class
    class function Create: _UndoScriptChangeAction;
    class function CreateRemote(const MachineName: string): _UndoScriptChangeAction;
  end;

// *********************************************************************//
// The Class CoCdsConnectionChange provides a Create and CreateRemote method to          
// create instances of the default interface _CdsConnectionChange exposed by              
// the CoClass CdsConnectionChange. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCdsConnectionChange = class
    class function Create: _CdsConnectionChange;
    class function CreateRemote(const MachineName: string): _CdsConnectionChange;
  end;

// *********************************************************************//
// The Class CoCvsGraphic provides a Create and CreateRemote method to          
// create instances of the default interface _CvsGraphic exposed by              
// the CoClass CvsGraphic. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsGraphic = class
    class function Create: _CvsGraphic;
    class function CreateRemote(const MachineName: string): _CvsGraphic;
  end;

// *********************************************************************//
// The Class CoCvsGraphicArrows provides a Create and CreateRemote method to          
// create instances of the default interface _CvsGraphicArrows exposed by              
// the CoClass CvsGraphicArrows. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsGraphicArrows = class
    class function Create: _CvsGraphicArrows;
    class function CreateRemote(const MachineName: string): _CvsGraphicArrows;
  end;

// *********************************************************************//
// The Class CoCvsCellEditMaskedRegion provides a Create and CreateRemote method to          
// create instances of the default interface _CvsCellEditMaskedRegion exposed by              
// the CoClass CvsCellEditMaskedRegion. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsCellEditMaskedRegion = class
    class function Create: _CvsCellEditMaskedRegion;
    class function CreateRemote(const MachineName: string): _CvsCellEditMaskedRegion;
  end;

// *********************************************************************//
// The Class CoCvsCellEditPolylinePath provides a Create and CreateRemote method to          
// create instances of the default interface _CvsCellEditPolylinePath exposed by              
// the CoClass CvsCellEditPolylinePath. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsCellEditPolylinePath = class
    class function Create: _CvsCellEditPolylinePath;
    class function CreateRemote(const MachineName: string): _CvsCellEditPolylinePath;
  end;

// *********************************************************************//
// The Class CoCvsAuditMessageSettings provides a Create and CreateRemote method to          
// create instances of the default interface _CvsAuditMessageSettings exposed by              
// the CoClass CvsAuditMessageSettings. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsAuditMessageSettings = class
    class function Create: _CvsAuditMessageSettings;
    class function CreateRemote(const MachineName: string): _CvsAuditMessageSettings;
  end;

// *********************************************************************//
// The Class CoCvsCaliperScoringMethod provides a Create and CreateRemote method to          
// create instances of the default interface _CvsCaliperScoringMethod exposed by              
// the CoClass CvsCaliperScoringMethod. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsCaliperScoringMethod = class
    class function Create: _CvsCaliperScoringMethod;
    class function CreateRemote(const MachineName: string): _CvsCaliperScoringMethod;
  end;

// *********************************************************************//
// The Class CoCvsCellEditComposite provides a Create and CreateRemote method to          
// create instances of the default interface _CvsCellEditComposite exposed by              
// the CoClass CvsCellEditComposite. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsCellEditComposite = class
    class function Create: _CvsCellEditComposite;
    class function CreateRemote(const MachineName: string): _CvsCellEditComposite;
  end;

// *********************************************************************//
// The Class CoCvsCellEditPolygon provides a Create and CreateRemote method to          
// create instances of the default interface _CvsCellEditPolygon exposed by              
// the CoClass CvsCellEditPolygon. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsCellEditPolygon = class
    class function Create: _CvsCellEditPolygon;
    class function CreateRemote(const MachineName: string): _CvsCellEditPolygon;
  end;

// *********************************************************************//
// The Class CoCvsColorMatchEditor provides a Create and CreateRemote method to          
// create instances of the default interface _CvsColorMatchEditor exposed by              
// the CoClass CvsColorMatchEditor. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsColorMatchEditor = class
    class function Create: _CvsColorMatchEditor;
    class function CreateRemote(const MachineName: string): _CvsColorMatchEditor;
  end;

// *********************************************************************//
// The Class CoCvsColorModel provides a Create and CreateRemote method to          
// create instances of the default interface _CvsColorModel exposed by              
// the CoClass CvsColorModel. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsColorModel = class
    class function Create: _CvsColorModel;
    class function CreateRemote(const MachineName: string): _CvsColorModel;
  end;

// *********************************************************************//
// The Class CoCvsEasyViewHelper provides a Create and CreateRemote method to          
// create instances of the default interface _CvsEasyViewHelper exposed by              
// the CoClass CvsEasyViewHelper. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsEasyViewHelper = class
    class function Create: _CvsEasyViewHelper;
    class function CreateRemote(const MachineName: string): _CvsEasyViewHelper;
  end;

// *********************************************************************//
// The Class CoCvsCompositeSubregion provides a Create and CreateRemote method to          
// create instances of the default interface _CvsCompositeSubregion exposed by              
// the CoClass CvsCompositeSubregion. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsCompositeSubregion = class
    class function Create: _CvsCompositeSubregion;
    class function CreateRemote(const MachineName: string): _CvsCompositeSubregion;
  end;

// *********************************************************************//
// The Class CoCvsGraphicCompositeBase provides a Create and CreateRemote method to          
// create instances of the default interface _CvsGraphicCompositeBase exposed by              
// the CoClass CvsGraphicCompositeBase. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsGraphicCompositeBase = class
    class function Create: _CvsGraphicCompositeBase;
    class function CreateRemote(const MachineName: string): _CvsGraphicCompositeBase;
  end;

// *********************************************************************//
// The Class CoCvsGraphicComposite provides a Create and CreateRemote method to          
// create instances of the default interface _CvsGraphicComposite exposed by              
// the CoClass CvsGraphicComposite. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsGraphicComposite = class
    class function Create: _CvsGraphicComposite;
    class function CreateRemote(const MachineName: string): _CvsGraphicComposite;
  end;

// *********************************************************************//
// The Class CoCvsGraphicPolylinePath provides a Create and CreateRemote method to          
// create instances of the default interface _CvsGraphicPolylinePath exposed by              
// the CoClass CvsGraphicPolylinePath. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsGraphicPolylinePath = class
    class function Create: _CvsGraphicPolylinePath;
    class function CreateRemote(const MachineName: string): _CvsGraphicPolylinePath;
  end;

// *********************************************************************//
// The Class CoCvsGraphicMaskedRegion provides a Create and CreateRemote method to          
// create instances of the default interface _CvsGraphicMaskedRegion exposed by              
// the CoClass CvsGraphicMaskedRegion. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsGraphicMaskedRegion = class
    class function Create: _CvsGraphicMaskedRegion;
    class function CreateRemote(const MachineName: string): _CvsGraphicMaskedRegion;
  end;

// *********************************************************************//
// The Class CoCvsGraphicDefectPoints provides a Create and CreateRemote method to          
// create instances of the default interface _CvsGraphicDefectPoints exposed by              
// the CoClass CvsGraphicDefectPoints. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsGraphicDefectPoints = class
    class function Create: _CvsGraphicDefectPoints;
    class function CreateRemote(const MachineName: string): _CvsGraphicDefectPoints;
  end;

// *********************************************************************//
// The Class CoCvsGraphicEdgePoints provides a Create and CreateRemote method to          
// create instances of the default interface _CvsGraphicEdgePoints exposed by              
// the CoClass CvsGraphicEdgePoints. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsGraphicEdgePoints = class
    class function Create: _CvsGraphicEdgePoints;
    class function CreateRemote(const MachineName: string): _CvsGraphicEdgePoints;
  end;

// *********************************************************************//
// The Class CoCvsGraphicExtremePoints provides a Create and CreateRemote method to          
// create instances of the default interface _CvsGraphicExtremePoints exposed by              
// the CoClass CvsGraphicExtremePoints. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsGraphicExtremePoints = class
    class function Create: _CvsGraphicExtremePoints;
    class function CreateRemote(const MachineName: string): _CvsGraphicExtremePoints;
  end;

// *********************************************************************//
// The Class CoCvsGraphicFilledBox provides a Create and CreateRemote method to          
// create instances of the default interface _CvsGraphicFilledBox exposed by              
// the CoClass CvsGraphicFilledBox. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsGraphicFilledBox = class
    class function Create: _CvsGraphicFilledBox;
    class function CreateRemote(const MachineName: string): _CvsGraphicFilledBox;
  end;

// *********************************************************************//
// The Class CoCvsGraphicParallelogram provides a Create and CreateRemote method to          
// create instances of the default interface _CvsGraphicParallelogram exposed by              
// the CoClass CvsGraphicParallelogram. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsGraphicParallelogram = class
    class function Create: _CvsGraphicParallelogram;
    class function CreateRemote(const MachineName: string): _CvsGraphicParallelogram;
  end;

// *********************************************************************//
// The Class CoCvsGraphicPolygon provides a Create and CreateRemote method to          
// create instances of the default interface _CvsGraphicPolygon exposed by              
// the CoClass CvsGraphicPolygon. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsGraphicPolygon = class
    class function Create: _CvsGraphicPolygon;
    class function CreateRemote(const MachineName: string): _CvsGraphicPolygon;
  end;

// *********************************************************************//
// The Class CoCvsJobAnalyzer provides a Create and CreateRemote method to          
// create instances of the default interface _CvsJobAnalyzer exposed by              
// the CoClass CvsJobAnalyzer. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsJobAnalyzer = class
    class function Create: _CvsJobAnalyzer;
    class function CreateRemote(const MachineName: string): _CvsJobAnalyzer;
  end;

// *********************************************************************//
// The Class CoCvsModbusSettings provides a Create and CreateRemote method to          
// create instances of the default interface _CvsModbusSettings exposed by              
// the CoClass CvsModbusSettings. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsModbusSettings = class
    class function Create: _CvsModbusSettings;
    class function CreateRemote(const MachineName: string): _CvsModbusSettings;
  end;

// *********************************************************************//
// The Class CoCvsString provides a Create and CreateRemote method to          
// create instances of the default interface _CvsString exposed by              
// the CoClass CvsString. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsString = class
    class function Create: _CvsString;
    class function CreateRemote(const MachineName: string): _CvsString;
  end;

// *********************************************************************//
// The Class CoStringProcessing provides a Create and CreateRemote method to          
// create instances of the default interface _StringProcessing exposed by              
// the CoClass StringProcessing. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoStringProcessing = class
    class function Create: _StringProcessing;
    class function CreateRemote(const MachineName: string): _StringProcessing;
  end;

// *********************************************************************//
// The Class CoStringBuilderProcessing provides a Create and CreateRemote method to          
// create instances of the default interface _StringBuilderProcessing exposed by              
// the CoClass StringBuilderProcessing. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoStringBuilderProcessing = class
    class function Create: _StringBuilderProcessing;
    class function CreateRemote(const MachineName: string): _StringBuilderProcessing;
  end;

// *********************************************************************//
// The Class CoCvsSymbolicTag provides a Create and CreateRemote method to          
// create instances of the default interface _CvsSymbolicTag exposed by              
// the CoClass CvsSymbolicTag. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsSymbolicTag = class
    class function Create: _CvsSymbolicTag;
    class function CreateRemote(const MachineName: string): _CvsSymbolicTag;
  end;

// *********************************************************************//
// The Class CoCvsSymbolicTagCollection provides a Create and CreateRemote method to          
// create instances of the default interface _CvsSymbolicTagCollection exposed by              
// the CoClass CvsSymbolicTagCollection. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsSymbolicTagCollection = class
    class function Create: _CvsSymbolicTagCollection;
    class function CreateRemote(const MachineName: string): _CvsSymbolicTagCollection;
  end;

// *********************************************************************//
// The Class CoCvsDiscreteIOLines provides a Create and CreateRemote method to          
// create instances of the default interface _CvsDiscreteIOLines exposed by              
// the CoClass CvsDiscreteIOLines. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsDiscreteIOLines = class
    class function Create: _CvsDiscreteIOLines;
    class function CreateRemote(const MachineName: string): _CvsDiscreteIOLines;
  end;

// *********************************************************************//
// The Class CoCvsUniversalIOSettings provides a Create and CreateRemote method to          
// create instances of the default interface _CvsUniversalIOSettings exposed by              
// the CoClass CvsUniversalIOSettings. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsUniversalIOSettings = class
    class function Create: _CvsUniversalIOSettings;
    class function CreateRemote(const MachineName: string): _CvsUniversalIOSettings;
  end;

// *********************************************************************//
// The Class CoCvsPassFailSettings provides a Create and CreateRemote method to          
// create instances of the default interface _CvsPassFailSettings exposed by              
// the CoClass CvsPassFailSettings. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsPassFailSettings = class
    class function Create: _CvsPassFailSettings;
    class function CreateRemote(const MachineName: string): _CvsPassFailSettings;
  end;

// *********************************************************************//
// The Class CoCvsHostCollection provides a Create and CreateRemote method to          
// create instances of the default interface _CvsHostCollection exposed by              
// the CoClass CvsHostCollection. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsHostCollection = class
    class function Create: _CvsHostCollection;
    class function CreateRemote(const MachineName: string): _CvsHostCollection;
  end;

// *********************************************************************//
// The Class CoCvsHost provides a Create and CreateRemote method to          
// create instances of the default interface _CvsHost exposed by              
// the CoClass CvsHost. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsHost = class
    class function Create: _CvsHost;
    class function CreateRemote(const MachineName: string): _CvsHost;
  end;

// *********************************************************************//
// The Class CoCvsHostSensor provides a Create and CreateRemote method to          
// create instances of the default interface _CvsHostSensor exposed by              
// the CoClass CvsHostSensor. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsHostSensor = class
    class function Create: _CvsHostSensor;
    class function CreateRemote(const MachineName: string): _CvsHostSensor;
  end;

// *********************************************************************//
// The Class CoCvsNetworkListener provides a Create and CreateRemote method to          
// create instances of the default interface _CvsNetworkListener exposed by              
// the CoClass CvsNetworkListener. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsNetworkListener = class
    class function Create: _CvsNetworkListener;
    class function CreateRemote(const MachineName: string): _CvsNetworkListener;
  end;

// *********************************************************************//
// The Class CoCvsNetworkMonitor provides a Create and CreateRemote method to          
// create instances of the default interface _CvsNetworkMonitor exposed by              
// the CoClass CvsNetworkMonitor. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsNetworkMonitor = class
    class function Create: _CvsNetworkMonitor;
    class function CreateRemote(const MachineName: string): _CvsNetworkMonitor;
  end;

// *********************************************************************//
// The Class CoCvsNetworkMonitorSubnet provides a Create and CreateRemote method to          
// create instances of the default interface _CvsNetworkMonitorSubnet exposed by              
// the CoClass CvsNetworkMonitorSubnet. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsNetworkMonitorSubnet = class
    class function Create: _CvsNetworkMonitorSubnet;
    class function CreateRemote(const MachineName: string): _CvsNetworkMonitorSubnet;
  end;

// *********************************************************************//
// The Class CoCvsOcrMaxAutoTune provides a Create and CreateRemote method to          
// create instances of the default interface _CvsOcrMaxAutoTune exposed by              
// the CoClass CvsOcrMaxAutoTune. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsOcrMaxAutoTune = class
    class function Create: _CvsOcrMaxAutoTune;
    class function CreateRemote(const MachineName: string): _CvsOcrMaxAutoTune;
  end;

// *********************************************************************//
// The Class CoAutoTuneSettings provides a Create and CreateRemote method to          
// create instances of the default interface _AutoTuneSettings exposed by              
// the CoClass AutoTuneSettings. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoAutoTuneSettings = class
    class function Create: _AutoTuneSettings;
    class function CreateRemote(const MachineName: string): _AutoTuneSettings;
  end;

// *********************************************************************//
// The Class CoSpaceSettings provides a Create and CreateRemote method to          
// create instances of the default interface _SpaceSettings exposed by              
// the CoClass SpaceSettings. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoSpaceSettings = class
    class function Create: _SpaceSettings;
    class function CreateRemote(const MachineName: string): _SpaceSettings;
  end;

// *********************************************************************//
// The Class CoAutoSegmentResult provides a Create and CreateRemote method to          
// create instances of the default interface _AutoSegmentResult exposed by              
// the CoClass AutoSegmentResult. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoAutoSegmentResult = class
    class function Create: _AutoSegmentResult;
    class function CreateRemote(const MachineName: string): _AutoSegmentResult;
  end;

// *********************************************************************//
// The Class CoAutoTuneRecord provides a Create and CreateRemote method to          
// create instances of the default interface _AutoTuneRecord exposed by              
// the CoClass AutoTuneRecord. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoAutoTuneRecord = class
    class function Create: _AutoTuneRecord;
    class function CreateRemote(const MachineName: string): _AutoTuneRecord;
  end;

// *********************************************************************//
// The Class CoAutoTuneException provides a Create and CreateRemote method to          
// create instances of the default interface _AutoTuneException exposed by              
// the CoClass AutoTuneException. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoAutoTuneException = class
    class function Create: _AutoTuneException;
    class function CreateRemote(const MachineName: string): _AutoTuneException;
  end;

// *********************************************************************//
// The Class CoArgUpdateHandler_2 provides a Create and CreateRemote method to          
// create instances of the default interface _ArgUpdateHandler_2 exposed by              
// the CoClass ArgUpdateHandler_2. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoArgUpdateHandler_2 = class
    class function Create: _ArgUpdateHandler_2;
    class function CreateRemote(const MachineName: string): _ArgUpdateHandler_2;
  end;

// *********************************************************************//
// The Class CoAutoTuneSessionClosedEventArgs provides a Create and CreateRemote method to          
// create instances of the default interface _AutoTuneSessionClosedEventArgs exposed by              
// the CoClass AutoTuneSessionClosedEventArgs. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoAutoTuneSessionClosedEventArgs = class
    class function Create: _AutoTuneSessionClosedEventArgs;
    class function CreateRemote(const MachineName: string): _AutoTuneSessionClosedEventArgs;
  end;

// *********************************************************************//
// The Class CoAutoTuneSessionClosedHandler provides a Create and CreateRemote method to          
// create instances of the default interface _AutoTuneSessionClosedHandler exposed by              
// the CoClass AutoTuneSessionClosedHandler. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoAutoTuneSessionClosedHandler = class
    class function Create: _AutoTuneSessionClosedHandler;
    class function CreateRemote(const MachineName: string): _AutoTuneSessionClosedHandler;
  end;

// *********************************************************************//
// The Class CoCvsCellButton provides a Create and CreateRemote method to          
// create instances of the default interface _CvsCellButton exposed by              
// the CoClass CvsCellButton. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsCellButton = class
    class function Create: _CvsCellButton;
    class function CreateRemote(const MachineName: string): _CvsCellButton;
  end;

// *********************************************************************//
// The Class CoCvsCellChart provides a Create and CreateRemote method to          
// create instances of the default interface _CvsCellChart exposed by              
// the CoClass CvsCellChart. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsCellChart = class
    class function Create: _CvsCellChart;
    class function CreateRemote(const MachineName: string): _CvsCellChart;
  end;

// *********************************************************************//
// The Class CoCvsCellCheckBox provides a Create and CreateRemote method to          
// create instances of the default interface _CvsCellCheckBox exposed by              
// the CoClass CvsCellCheckBox. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsCellCheckBox = class
    class function Create: _CvsCellCheckBox;
    class function CreateRemote(const MachineName: string): _CvsCellCheckBox;
  end;

// *********************************************************************//
// The Class CoCvsCellCollection provides a Create and CreateRemote method to          
// create instances of the default interface _CvsCellCollection exposed by              
// the CoClass CvsCellCollection. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsCellCollection = class
    class function Create: _CvsCellCollection;
    class function CreateRemote(const MachineName: string): _CvsCellCollection;
  end;

// *********************************************************************//
// The Class CoCvsCellColorLabel provides a Create and CreateRemote method to          
// create instances of the default interface _CvsCellColorLabel exposed by              
// the CoClass CvsCellColorLabel. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsCellColorLabel = class
    class function Create: _CvsCellColorLabel;
    class function CreateRemote(const MachineName: string): _CvsCellColorLabel;
  end;

// *********************************************************************//
// The Class CoCvsCellDialog provides a Create and CreateRemote method to          
// create instances of the default interface _CvsCellDialog exposed by              
// the CoClass CvsCellDialog. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsCellDialog = class
    class function Create: _CvsCellDialog;
    class function CreateRemote(const MachineName: string): _CvsCellDialog;
  end;

// *********************************************************************//
// The Class CoCvsCellEditCircle provides a Create and CreateRemote method to          
// create instances of the default interface _CvsCellEditCircle exposed by              
// the CoClass CvsCellEditCircle. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsCellEditCircle = class
    class function Create: _CvsCellEditCircle;
    class function CreateRemote(const MachineName: string): _CvsCellEditCircle;
  end;

// *********************************************************************//
// The Class CoCvsCellEditFloat provides a Create and CreateRemote method to          
// create instances of the default interface _CvsCellEditFloat exposed by              
// the CoClass CvsCellEditFloat. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsCellEditFloat = class
    class function Create: _CvsCellEditFloat;
    class function CreateRemote(const MachineName: string): _CvsCellEditFloat;
  end;

// *********************************************************************//
// The Class CoCvsCellEditInt provides a Create and CreateRemote method to          
// create instances of the default interface _CvsCellEditInt exposed by              
// the CoClass CvsCellEditInt. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsCellEditInt = class
    class function Create: _CvsCellEditInt;
    class function CreateRemote(const MachineName: string): _CvsCellEditInt;
  end;

// *********************************************************************//
// The Class CoCvsCellEditLine provides a Create and CreateRemote method to          
// create instances of the default interface _CvsCellEditLine exposed by              
// the CoClass CvsCellEditLine. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsCellEditLine = class
    class function Create: _CvsCellEditLine;
    class function CreateRemote(const MachineName: string): _CvsCellEditLine;
  end;

// *********************************************************************//
// The Class CoCvsCellEditPoint provides a Create and CreateRemote method to          
// create instances of the default interface _CvsCellEditPoint exposed by              
// the CoClass CvsCellEditPoint. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsCellEditPoint = class
    class function Create: _CvsCellEditPoint;
    class function CreateRemote(const MachineName: string): _CvsCellEditPoint;
  end;

// *********************************************************************//
// The Class CoCvsCellEditRegion provides a Create and CreateRemote method to          
// create instances of the default interface _CvsCellEditRegion exposed by              
// the CoClass CvsCellEditRegion. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsCellEditRegion = class
    class function Create: _CvsCellEditRegion;
    class function CreateRemote(const MachineName: string): _CvsCellEditRegion;
  end;

// *********************************************************************//
// The Class CoCvsCellEditString provides a Create and CreateRemote method to          
// create instances of the default interface _CvsCellEditString exposed by              
// the CoClass CvsCellEditString. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsCellEditString = class
    class function Create: _CvsCellEditString;
    class function CreateRemote(const MachineName: string): _CvsCellEditString;
  end;

// *********************************************************************//
// The Class CoCvsCellEditAnnulus provides a Create and CreateRemote method to          
// create instances of the default interface _CvsCellEditAnnulus exposed by              
// the CoClass CvsCellEditAnnulus. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsCellEditAnnulus = class
    class function Create: _CvsCellEditAnnulus;
    class function CreateRemote(const MachineName: string): _CvsCellEditAnnulus;
  end;

// *********************************************************************//
// The Class CoCvsCellEmpty provides a Create and CreateRemote method to          
// create instances of the default interface _CvsCellEmpty exposed by              
// the CoClass CvsCellEmpty. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsCellEmpty = class
    class function Create: _CvsCellEmpty;
    class function CreateRemote(const MachineName: string): _CvsCellEmpty;
  end;

// *********************************************************************//
// The Class CoCvsCellError provides a Create and CreateRemote method to          
// create instances of the default interface _CvsCellError exposed by              
// the CoClass CvsCellError. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsCellError = class
    class function Create: _CvsCellError;
    class function CreateRemote(const MachineName: string): _CvsCellError;
  end;

// *********************************************************************//
// The Class CoCvsCellFloat provides a Create and CreateRemote method to          
// create instances of the default interface _CvsCellFloat exposed by              
// the CoClass CvsCellFloat. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsCellFloat = class
    class function Create: _CvsCellFloat;
    class function CreateRemote(const MachineName: string): _CvsCellFloat;
  end;

// *********************************************************************//
// The Class CoCvsCellInt provides a Create and CreateRemote method to          
// create instances of the default interface _CvsCellInt exposed by              
// the CoClass CvsCellInt. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsCellInt = class
    class function Create: _CvsCellInt;
    class function CreateRemote(const MachineName: string): _CvsCellInt;
  end;

// *********************************************************************//
// The Class CoCvsCellLink provides a Create and CreateRemote method to          
// create instances of the default interface _CvsCellLink exposed by              
// the CoClass CvsCellLink. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsCellLink = class
    class function Create: _CvsCellLink;
    class function CreateRemote(const MachineName: string): _CvsCellLink;
  end;

// *********************************************************************//
// The Class CoCvsCellListBox provides a Create and CreateRemote method to          
// create instances of the default interface _CvsCellListBox exposed by              
// the CoClass CvsCellListBox. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsCellListBox = class
    class function Create: _CvsCellListBox;
    class function CreateRemote(const MachineName: string): _CvsCellListBox;
  end;

// *********************************************************************//
// The Class CoCvsCellMultiStatus provides a Create and CreateRemote method to          
// create instances of the default interface _CvsCellMultiStatus exposed by              
// the CoClass CvsCellMultiStatus. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsCellMultiStatus = class
    class function Create: _CvsCellMultiStatus;
    class function CreateRemote(const MachineName: string): _CvsCellMultiStatus;
  end;

// *********************************************************************//
// The Class CoCvsCellSelect provides a Create and CreateRemote method to          
// create instances of the default interface _CvsCellSelect exposed by              
// the CoClass CvsCellSelect. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsCellSelect = class
    class function Create: _CvsCellSelect;
    class function CreateRemote(const MachineName: string): _CvsCellSelect;
  end;

// *********************************************************************//
// The Class CoCvsCellStatus provides a Create and CreateRemote method to          
// create instances of the default interface _CvsCellStatus exposed by              
// the CoClass CvsCellStatus. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsCellStatus = class
    class function Create: _CvsCellStatus;
    class function CreateRemote(const MachineName: string): _CvsCellStatus;
  end;

// *********************************************************************//
// The Class CoCvsCellStatusLight provides a Create and CreateRemote method to          
// create instances of the default interface _CvsCellStatusLight exposed by              
// the CoClass CvsCellStatusLight. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsCellStatusLight = class
    class function Create: _CvsCellStatusLight;
    class function CreateRemote(const MachineName: string): _CvsCellStatusLight;
  end;

// *********************************************************************//
// The Class CoCvsCellWizard provides a Create and CreateRemote method to          
// create instances of the default interface _CvsCellWizard exposed by              
// the CoClass CvsCellWizard. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsCellWizard = class
    class function Create: _CvsCellWizard;
    class function CreateRemote(const MachineName: string): _CvsCellWizard;
  end;

// *********************************************************************//
// The Class CoCvsDiscreteInputLine provides a Create and CreateRemote method to          
// create instances of the default interface _CvsDiscreteInputLine exposed by              
// the CoClass CvsDiscreteInputLine. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsDiscreteInputLine = class
    class function Create: _CvsDiscreteInputLine;
    class function CreateRemote(const MachineName: string): _CvsDiscreteInputLine;
  end;

// *********************************************************************//
// The Class CoCvsInputCollection provides a Create and CreateRemote method to          
// create instances of the default interface _CvsInputCollection exposed by              
// the CoClass CvsInputCollection. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsInputCollection = class
    class function Create: _CvsInputCollection;
    class function CreateRemote(const MachineName: string): _CvsInputCollection;
  end;

// *********************************************************************//
// The Class CoCvsOutputCollection provides a Create and CreateRemote method to          
// create instances of the default interface _CvsOutputCollection exposed by              
// the CoClass CvsOutputCollection. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsOutputCollection = class
    class function Create: _CvsOutputCollection;
    class function CreateRemote(const MachineName: string): _CvsOutputCollection;
  end;

// *********************************************************************//
// The Class CoCvsDiscreteOutputLine provides a Create and CreateRemote method to          
// create instances of the default interface _CvsDiscreteOutputLine exposed by              
// the CoClass CvsDiscreteOutputLine. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsDiscreteOutputLine = class
    class function Create: _CvsDiscreteOutputLine;
    class function CreateRemote(const MachineName: string): _CvsDiscreteOutputLine;
  end;

// *********************************************************************//
// The Class CoCvsExpansionIOSettings provides a Create and CreateRemote method to          
// create instances of the default interface _CvsExpansionIOSettings exposed by              
// the CoClass CvsExpansionIOSettings. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsExpansionIOSettings = class
    class function Create: _CvsExpansionIOSettings;
    class function CreateRemote(const MachineName: string): _CvsExpansionIOSettings;
  end;

// *********************************************************************//
// The Class CoCvsFile provides a Create and CreateRemote method to          
// create instances of the default interface _CvsFile exposed by              
// the CoClass CvsFile. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsFile = class
    class function Create: _CvsFile;
    class function CreateRemote(const MachineName: string): _CvsFile;
  end;

// *********************************************************************//
// The Class CoCvsFileCollection provides a Create and CreateRemote method to          
// create instances of the default interface _CvsFileCollection exposed by              
// the CoClass CvsFileCollection. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsFileCollection = class
    class function Create: _CvsFileCollection;
    class function CreateRemote(const MachineName: string): _CvsFileCollection;
  end;

// *********************************************************************//
// The Class CoCvsFileManager provides a Create and CreateRemote method to          
// create instances of the default interface _CvsFileManager exposed by              
// the CoClass CvsFileManager. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsFileManager = class
    class function Create: _CvsFileManager;
    class function CreateRemote(const MachineName: string): _CvsFileManager;
  end;

// *********************************************************************//
// The Class CoCvsFtpClient provides a Create and CreateRemote method to          
// create instances of the default interface _CvsFtpClient exposed by              
// the CoClass CvsFtpClient. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsFtpClient = class
    class function Create: _CvsFtpClient;
    class function CreateRemote(const MachineName: string): _CvsFtpClient;
  end;

// *********************************************************************//
// The Class CoCvsFtpSettings provides a Create and CreateRemote method to          
// create instances of the default interface _CvsFtpSettings exposed by              
// the CoClass CvsFtpSettings. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsFtpSettings = class
    class function Create: _CvsFtpSettings;
    class function CreateRemote(const MachineName: string): _CvsFtpSettings;
  end;

// *********************************************************************//
// The Class CoCvsGraphic4Side provides a Create and CreateRemote method to          
// create instances of the default interface _CvsGraphic4Side exposed by              
// the CoClass CvsGraphic4Side. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsGraphic4Side = class
    class function Create: _CvsGraphic4Side;
    class function CreateRemote(const MachineName: string): _CvsGraphic4Side;
  end;

// *********************************************************************//
// The Class CoCvsGraphicArc provides a Create and CreateRemote method to          
// create instances of the default interface _CvsGraphicArc exposed by              
// the CoClass CvsGraphicArc. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsGraphicArc = class
    class function Create: _CvsGraphicArc;
    class function CreateRemote(const MachineName: string): _CvsGraphicArc;
  end;

// *********************************************************************//
// The Class CoCvsGraphicBlob provides a Create and CreateRemote method to          
// create instances of the default interface _CvsGraphicBlob exposed by              
// the CoClass CvsGraphicBlob. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsGraphicBlob = class
    class function Create: _CvsGraphicBlob;
    class function CreateRemote(const MachineName: string): _CvsGraphicBlob;
  end;

// *********************************************************************//
// The Class CoCvsGraphicCircle provides a Create and CreateRemote method to          
// create instances of the default interface _CvsGraphicCircle exposed by              
// the CoClass CvsGraphicCircle. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsGraphicCircle = class
    class function Create: _CvsGraphicCircle;
    class function CreateRemote(const MachineName: string): _CvsGraphicCircle;
  end;

// *********************************************************************//
// The Class CoCvsGraphicCollection provides a Create and CreateRemote method to          
// create instances of the default interface _CvsGraphicCollection exposed by              
// the CoClass CvsGraphicCollection. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsGraphicCollection = class
    class function Create: _CvsGraphicCollection;
    class function CreateRemote(const MachineName: string): _CvsGraphicCollection;
  end;

// *********************************************************************//
// The Class CoCvsGraphicPatternMatch provides a Create and CreateRemote method to          
// create instances of the default interface _CvsGraphicPatternMatch exposed by              
// the CoClass CvsGraphicPatternMatch. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsGraphicPatternMatch = class
    class function Create: _CvsGraphicPatternMatch;
    class function CreateRemote(const MachineName: string): _CvsGraphicPatternMatch;
  end;

// *********************************************************************//
// The Class CoCvsPatternMatchSegment provides a Create and CreateRemote method to          
// create instances of the default interface _CvsPatternMatchSegment exposed by              
// the CoClass CvsPatternMatchSegment. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsPatternMatchSegment = class
    class function Create: _CvsPatternMatchSegment;
    class function CreateRemote(const MachineName: string): _CvsPatternMatchSegment;
  end;

// *********************************************************************//
// The Class CoCvsGraphicCross provides a Create and CreateRemote method to          
// create instances of the default interface _CvsGraphicCross exposed by              
// the CoClass CvsGraphicCross. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsGraphicCross = class
    class function Create: _CvsGraphicCross;
    class function CreateRemote(const MachineName: string): _CvsGraphicCross;
  end;

// *********************************************************************//
// The Class CoCvsGraphicEdgeProfile provides a Create and CreateRemote method to          
// create instances of the default interface _CvsGraphicEdgeProfile exposed by              
// the CoClass CvsGraphicEdgeProfile. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsGraphicEdgeProfile = class
    class function Create: _CvsGraphicEdgeProfile;
    class function CreateRemote(const MachineName: string): _CvsGraphicEdgeProfile;
  end;

// *********************************************************************//
// The Class CoCvsGraphicFixture provides a Create and CreateRemote method to          
// create instances of the default interface _CvsGraphicFixture exposed by              
// the CoClass CvsGraphicFixture. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsGraphicFixture = class
    class function Create: _CvsGraphicFixture;
    class function CreateRemote(const MachineName: string): _CvsGraphicFixture;
  end;

// *********************************************************************//
// The Class CoCvsGraphicHistogram provides a Create and CreateRemote method to          
// create instances of the default interface _CvsGraphicHistogram exposed by              
// the CoClass CvsGraphicHistogram. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsGraphicHistogram = class
    class function Create: _CvsGraphicHistogram;
    class function CreateRemote(const MachineName: string): _CvsGraphicHistogram;
  end;

// *********************************************************************//
// The Class CoCvsGraphicImage provides a Create and CreateRemote method to          
// create instances of the default interface _CvsGraphicImage exposed by              
// the CoClass CvsGraphicImage. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsGraphicImage = class
    class function Create: _CvsGraphicImage;
    class function CreateRemote(const MachineName: string): _CvsGraphicImage;
  end;

// *********************************************************************//
// The Class CoCvsGraphicBlobProperties provides a Create and CreateRemote method to          
// create instances of the default interface _CvsGraphicBlobProperties exposed by              
// the CoClass CvsGraphicBlobProperties. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsGraphicBlobProperties = class
    class function Create: _CvsGraphicBlobProperties;
    class function CreateRemote(const MachineName: string): _CvsGraphicBlobProperties;
  end;

// *********************************************************************//
// The Class CoCvsGraphicLine provides a Create and CreateRemote method to          
// create instances of the default interface _CvsGraphicLine exposed by              
// the CoClass CvsGraphicLine. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsGraphicLine = class
    class function Create: _CvsGraphicLine;
    class function CreateRemote(const MachineName: string): _CvsGraphicLine;
  end;

// *********************************************************************//
// The Class CoCvsGraphicOffset provides a Create and CreateRemote method to          
// create instances of the default interface _CvsGraphicOffset exposed by              
// the CoClass CvsGraphicOffset. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsGraphicOffset = class
    class function Create: _CvsGraphicOffset;
    class function CreateRemote(const MachineName: string): _CvsGraphicOffset;
  end;

// *********************************************************************//
// The Class CoCvsGraphicPoint provides a Create and CreateRemote method to          
// create instances of the default interface _CvsGraphicPoint exposed by              
// the CoClass CvsGraphicPoint. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsGraphicPoint = class
    class function Create: _CvsGraphicPoint;
    class function CreateRemote(const MachineName: string): _CvsGraphicPoint;
  end;

// *********************************************************************//
// The Class CoCvsGraphicPolyline provides a Create and CreateRemote method to          
// create instances of the default interface _CvsGraphicPolyline exposed by              
// the CoClass CvsGraphicPolyline. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsGraphicPolyline = class
    class function Create: _CvsGraphicPolyline;
    class function CreateRemote(const MachineName: string): _CvsGraphicPolyline;
  end;

// *********************************************************************//
// The Class CoCvsGraphicRegion provides a Create and CreateRemote method to          
// create instances of the default interface _CvsGraphicRegion exposed by              
// the CoClass CvsGraphicRegion. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsGraphicRegion = class
    class function Create: _CvsGraphicRegion;
    class function CreateRemote(const MachineName: string): _CvsGraphicRegion;
  end;

// *********************************************************************//
// The Class CoCvsGraphicText provides a Create and CreateRemote method to          
// create instances of the default interface _CvsGraphicText exposed by              
// the CoClass CvsGraphicText. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsGraphicText = class
    class function Create: _CvsGraphicText;
    class function CreateRemote(const MachineName: string): _CvsGraphicText;
  end;

// *********************************************************************//
// The Class CoCvsGraphicAnnulus provides a Create and CreateRemote method to          
// create instances of the default interface _CvsGraphicAnnulus exposed by              
// the CoClass CvsGraphicAnnulus. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsGraphicAnnulus = class
    class function Create: _CvsGraphicAnnulus;
    class function CreateRemote(const MachineName: string): _CvsGraphicAnnulus;
  end;

// *********************************************************************//
// The Class CoCvsHostTableCollection provides a Create and CreateRemote method to          
// create instances of the default interface _CvsHostTableCollection exposed by              
// the CoClass CvsHostTableCollection. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsHostTableCollection = class
    class function Create: _CvsHostTableCollection;
    class function CreateRemote(const MachineName: string): _CvsHostTableCollection;
  end;

// *********************************************************************//
// The Class CoCvsHostTableEntry provides a Create and CreateRemote method to          
// create instances of the default interface _CvsHostTableEntry exposed by              
// the CoClass CvsHostTableEntry. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsHostTableEntry = class
    class function Create: _CvsHostTableEntry;
    class function CreateRemote(const MachineName: string): _CvsHostTableEntry;
  end;

// *********************************************************************//
// The Class CoCvsHostVersion provides a Create and CreateRemote method to          
// create instances of the default interface _CvsHostVersion exposed by              
// the CoClass CvsHostVersion. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsHostVersion = class
    class function Create: _CvsHostVersion;
    class function CreateRemote(const MachineName: string): _CvsHostVersion;
  end;

// *********************************************************************//
// The Class CoCvsImage provides a Create and CreateRemote method to          
// create instances of the default interface _CvsImage exposed by              
// the CoClass CvsImage. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsImage = class
    class function Create: _CvsImage;
    class function CreateRemote(const MachineName: string): _CvsImage;
  end;

// *********************************************************************//
// The Class CoCvsInSight provides a Create and CreateRemote method to          
// create instances of the default interface _CvsInSight exposed by              
// the CoClass CvsInSight. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsInSight = class
    class function Create: _CvsInSight;
    class function CreateRemote(const MachineName: string): _CvsInSight;
  end;

// *********************************************************************//
// The Class CoCvsJobInfo provides a Create and CreateRemote method to          
// create instances of the default interface _CvsJobInfo exposed by              
// the CoClass CvsJobInfo. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsJobInfo = class
    class function Create: _CvsJobInfo;
    class function CreateRemote(const MachineName: string): _CvsJobInfo;
  end;

// *********************************************************************//
// The Class CoCvsKernel provides a Create and CreateRemote method to          
// create instances of the default interface _CvsKernel exposed by              
// the CoClass CvsKernel. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsKernel = class
    class function Create: _CvsKernel;
    class function CreateRemote(const MachineName: string): _CvsKernel;
  end;

// *********************************************************************//
// The Class CoCvsLicense provides a Create and CreateRemote method to          
// create instances of the default interface _CvsLicense exposed by              
// the CoClass CvsLicense. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsLicense = class
    class function Create: _CvsLicense;
    class function CreateRemote(const MachineName: string): _CvsLicense;
  end;

// *********************************************************************//
// The Class CoCvsLocalIOSettings provides a Create and CreateRemote method to          
// create instances of the default interface _CvsLocalIOSettings exposed by              
// the CoClass CvsLocalIOSettings. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsLocalIOSettings = class
    class function Create: _CvsLocalIOSettings;
    class function CreateRemote(const MachineName: string): _CvsLocalIOSettings;
  end;

// *********************************************************************//
// The Class CoCvsNativeModeClient provides a Create and CreateRemote method to          
// create instances of the default interface _CvsNativeModeClient exposed by              
// the CoClass CvsNativeModeClient. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsNativeModeClient = class
    class function Create: _CvsNativeModeClient;
    class function CreateRemote(const MachineName: string): _CvsNativeModeClient;
  end;

// *********************************************************************//
// The Class CoCvsNetworkSettings provides a Create and CreateRemote method to          
// create instances of the default interface _CvsNetworkSettings exposed by              
// the CoClass CvsNetworkSettings. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsNetworkSettings = class
    class function Create: _CvsNetworkSettings;
    class function CreateRemote(const MachineName: string): _CvsNetworkSettings;
  end;

// *********************************************************************//
// The Class CoCvsPicture provides a Create and CreateRemote method to          
// create instances of the default interface _CvsPicture exposed by              
// the CoClass CvsPicture. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsPicture = class
    class function Create: _CvsPicture;
    class function CreateRemote(const MachineName: string): _CvsPicture;
  end;

// *********************************************************************//
// The Class CoCvsProcessorConstants provides a Create and CreateRemote method to          
// create instances of the default interface _CvsProcessorConstants exposed by              
// the CoClass CvsProcessorConstants. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsProcessorConstants = class
    class function Create: _CvsProcessorConstants;
    class function CreateRemote(const MachineName: string): _CvsProcessorConstants;
  end;

// *********************************************************************//
// The Class CoCvsResultSet provides a Create and CreateRemote method to          
// create instances of the default interface _CvsResultSet exposed by              
// the CoClass CvsResultSet. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsResultSet = class
    class function Create: _CvsResultSet;
    class function CreateRemote(const MachineName: string): _CvsResultSet;
  end;

// *********************************************************************//
// The Class CoCvsSerialSettings provides a Create and CreateRemote method to          
// create instances of the default interface _CvsSerialSettings exposed by              
// the CoClass CvsSerialSettings. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsSerialSettings = class
    class function Create: _CvsSerialSettings;
    class function CreateRemote(const MachineName: string): _CvsSerialSettings;
  end;

// *********************************************************************//
// The Class CoCvsSntpCurrentData provides a Create and CreateRemote method to          
// create instances of the default interface _CvsSntpCurrentData exposed by              
// the CoClass CvsSntpCurrentData. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsSntpCurrentData = class
    class function Create: _CvsSntpCurrentData;
    class function CreateRemote(const MachineName: string): _CvsSntpCurrentData;
  end;

// *********************************************************************//
// The Class CoCvsSntpSettings provides a Create and CreateRemote method to          
// create instances of the default interface _CvsSntpSettings exposed by              
// the CoClass CvsSntpSettings. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsSntpSettings = class
    class function Create: _CvsSntpSettings;
    class function CreateRemote(const MachineName: string): _CvsSntpSettings;
  end;

// *********************************************************************//
// The Class CoCvsTimeDateData provides a Create and CreateRemote method to          
// create instances of the default interface _CvsTimeDateData exposed by              
// the CoClass CvsTimeDateData. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsTimeDateData = class
    class function Create: _CvsTimeDateData;
    class function CreateRemote(const MachineName: string): _CvsTimeDateData;
  end;

// *********************************************************************//
// The Class CoCvsTimeZones provides a Create and CreateRemote method to          
// create instances of the default interface _CvsTimeZones exposed by              
// the CoClass CvsTimeZones. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsTimeZones = class
    class function Create: _CvsTimeZones;
    class function CreateRemote(const MachineName: string): _CvsTimeZones;
  end;

// *********************************************************************//
// The Class CoCvsUserCollection provides a Create and CreateRemote method to          
// create instances of the default interface _CvsUserCollection exposed by              
// the CoClass CvsUserCollection. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsUserCollection = class
    class function Create: _CvsUserCollection;
    class function CreateRemote(const MachineName: string): _CvsUserCollection;
  end;

// *********************************************************************//
// The Class CoCvsUtility provides a Create and CreateRemote method to          
// create instances of the default interface _CvsUtility exposed by              
// the CoClass CvsUtility. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsUtility = class
    class function Create: _CvsUtility;
    class function CreateRemote(const MachineName: string): _CvsUtility;
  end;

// *********************************************************************//
// The Class CoCvsView provides a Create and CreateRemote method to          
// create instances of the default interface _CvsView exposed by              
// the CoClass CvsView. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsView = class
    class function Create: _CvsView;
    class function CreateRemote(const MachineName: string): _CvsView;
  end;

// *********************************************************************//
// The Class CoCvsStateChangedEventArgs provides a Create and CreateRemote method to          
// create instances of the default interface _CvsStateChangedEventArgs exposed by              
// the CoClass CvsStateChangedEventArgs. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsStateChangedEventArgs = class
    class function Create: _CvsStateChangedEventArgs;
    class function CreateRemote(const MachineName: string): _CvsStateChangedEventArgs;
  end;

// *********************************************************************//
// The Class CoCvsConnectCompletedEventArgs provides a Create and CreateRemote method to          
// create instances of the default interface _CvsConnectCompletedEventArgs exposed by              
// the CoClass CvsConnectCompletedEventArgs. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsConnectCompletedEventArgs = class
    class function Create: _CvsConnectCompletedEventArgs;
    class function CreateRemote(const MachineName: string): _CvsConnectCompletedEventArgs;
  end;

// *********************************************************************//
// The Class CoCvsInSightMessageEventArgs provides a Create and CreateRemote method to          
// create instances of the default interface _CvsInSightMessageEventArgs exposed by              
// the CoClass CvsInSightMessageEventArgs. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsInSightMessageEventArgs = class
    class function Create: _CvsInSightMessageEventArgs;
    class function CreateRemote(const MachineName: string): _CvsInSightMessageEventArgs;
  end;

// *********************************************************************//
// The Class CoCvsLoadCompletedEventArgs provides a Create and CreateRemote method to          
// create instances of the default interface _CvsLoadCompletedEventArgs exposed by              
// the CoClass CvsLoadCompletedEventArgs. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsLoadCompletedEventArgs = class
    class function Create: _CvsLoadCompletedEventArgs;
    class function CreateRemote(const MachineName: string): _CvsLoadCompletedEventArgs;
  end;

// *********************************************************************//
// The Class CoCvsSaveCompletedEventArgs provides a Create and CreateRemote method to          
// create instances of the default interface _CvsSaveCompletedEventArgs exposed by              
// the CoClass CvsSaveCompletedEventArgs. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsSaveCompletedEventArgs = class
    class function Create: _CvsSaveCompletedEventArgs;
    class function CreateRemote(const MachineName: string): _CvsSaveCompletedEventArgs;
  end;

// *********************************************************************//
// The Class CoCvsAcceptMessageHandler provides a Create and CreateRemote method to          
// create instances of the default interface _CvsAcceptMessageHandler exposed by              
// the CoClass CvsAcceptMessageHandler. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCvsAcceptMessageHandler = class
    class function Create: _CvsAcceptMessageHandler;
    class function CreateRemote(const MachineName: string): _CvsAcceptMessageHandler;
  end;

// *********************************************************************//
// The Class CoBadSnippetException provides a Create and CreateRemote method to          
// create instances of the default interface _BadSnippetException exposed by              
// the CoClass BadSnippetException. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoBadSnippetException = class
    class function Create: _BadSnippetException;
    class function CreateRemote(const MachineName: string): _BadSnippetException;
  end;

// *********************************************************************//
// The Class CoSnippet provides a Create and CreateRemote method to          
// create instances of the default interface _Snippet exposed by              
// the CoClass Snippet. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoSnippet = class
    class function Create: _Snippet;
    class function CreateRemote(const MachineName: string): _Snippet;
  end;

// *********************************************************************//
// The Class CoSyslog provides a Create and CreateRemote method to          
// create instances of the default interface _Syslog exposed by              
// the CoClass Syslog. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoSyslog = class
    class function Create: _Syslog;
    class function CreateRemote(const MachineName: string): _Syslog;
  end;

// *********************************************************************//
// The Class CoSyslogTraceListener provides a Create and CreateRemote method to          
// create instances of the default interface _SyslogTraceListener exposed by              
// the CoClass SyslogTraceListener. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoSyslogTraceListener = class
    class function Create: _SyslogTraceListener;
    class function CreateRemote(const MachineName: string): _SyslogTraceListener;
  end;

implementation

uses ComObj;

class function CoCvsActionToggle.Create: ICvsActionToggle;
begin
  Result := CreateComObject(CLASS_CvsActionToggle) as ICvsActionToggle;
end;

class function CoCvsActionToggle.CreateRemote(const MachineName: string): ICvsActionToggle;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsActionToggle) as ICvsActionToggle;
end;

class function CoCvsAction.Create: ICvsAction;
begin
  Result := CreateComObject(CLASS_CvsAction) as ICvsAction;
end;

class function CoCvsAction.CreateRemote(const MachineName: string): ICvsAction;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsAction) as ICvsAction;
end;

class function CoCvsCell.Create: _CvsCell;
begin
  Result := CreateComObject(CLASS_CvsCell) as _CvsCell;
end;

class function CoCvsCell.CreateRemote(const MachineName: string): _CvsCell;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsCell) as _CvsCell;
end;

class function CoCvsCellOcrMaxAutoTune.Create: _CvsCellOcrMaxAutoTune;
begin
  Result := CreateComObject(CLASS_CvsCellOcrMaxAutoTune) as _CvsCellOcrMaxAutoTune;
end;

class function CoCvsCellOcrMaxAutoTune.CreateRemote(const MachineName: string): _CvsCellOcrMaxAutoTune;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsCellOcrMaxAutoTune) as _CvsCellOcrMaxAutoTune;
end;

class function CoCvsCrcResponse.Create: _CvsCrcResponse;
begin
  Result := CreateComObject(CLASS_CvsCrcResponse) as _CvsCrcResponse;
end;

class function CoCvsCrcResponse.CreateRemote(const MachineName: string): _CvsCrcResponse;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsCrcResponse) as _CvsCrcResponse;
end;

class function CoCvsCellString.Create: _CvsCellString;
begin
  Result := CreateComObject(CLASS_CvsCellString) as _CvsCellString;
end;

class function CoCvsCellString.CreateRemote(const MachineName: string): _CvsCellString;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsCellString) as _CvsCellString;
end;

class function CoCvsCellAuthorize.Create: _CvsCellAuthorize;
begin
  Result := CreateComObject(CLASS_CvsCellAuthorize) as _CvsCellAuthorize;
end;

class function CoCvsCellAuthorize.CreateRemote(const MachineName: string): _CvsCellAuthorize;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsCellAuthorize) as _CvsCellAuthorize;
end;

class function CoCvsCellEditGraphic.Create: _CvsCellEditGraphic;
begin
  Result := CreateComObject(CLASS_CvsCellEditGraphic) as _CvsCellEditGraphic;
end;

class function CoCvsCellEditGraphic.CreateRemote(const MachineName: string): _CvsCellEditGraphic;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsCellEditGraphic) as _CvsCellEditGraphic;
end;

class function CoCvsCellEditMultiGraphics.Create: _CvsCellEditMultiGraphics;
begin
  Result := CreateComObject(CLASS_CvsCellEditMultiGraphics) as _CvsCellEditMultiGraphics;
end;

class function CoCvsCellEditMultiGraphics.CreateRemote(const MachineName: string): _CvsCellEditMultiGraphics;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsCellEditMultiGraphics) as _CvsCellEditMultiGraphics;
end;

class function CoCvsCellReference.Create: _CvsCellReference;
begin
  Result := CreateComObject(CLASS_CvsCellReference) as _CvsCellReference;
end;

class function CoCvsCellReference.CreateRemote(const MachineName: string): _CvsCellReference;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsCellReference) as _CvsCellReference;
end;

class function CoCvsCellReferenceInfo.Create: _CvsCellReferenceInfo;
begin
  Result := CreateComObject(CLASS_CvsCellReferenceInfo) as _CvsCellReferenceInfo;
end;

class function CoCvsCellReferenceInfo.CreateRemote(const MachineName: string): _CvsCellReferenceInfo;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsCellReferenceInfo) as _CvsCellReferenceInfo;
end;

class function CoCvsCellReferenceTable.Create: _CvsCellReferenceTable;
begin
  Result := CreateComObject(CLASS_CvsCellReferenceTable) as _CvsCellReferenceTable;
end;

class function CoCvsCellReferenceTable.CreateRemote(const MachineName: string): _CvsCellReferenceTable;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsCellReferenceTable) as _CvsCellReferenceTable;
end;

class function CoCvsDefaultRegion.Create: _CvsDefaultRegion;
begin
  Result := CreateComObject(CLASS_CvsDefaultRegion) as _CvsDefaultRegion;
end;

class function CoCvsDefaultRegion.CreateRemote(const MachineName: string): _CvsDefaultRegion;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsDefaultRegion) as _CvsDefaultRegion;
end;

class function CoCvsEasyView.Create: _CvsEasyView;
begin
  Result := CreateComObject(CLASS_CvsEasyView) as _CvsEasyView;
end;

class function CoCvsEasyView.CreateRemote(const MachineName: string): _CvsEasyView;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsEasyView) as _CvsEasyView;
end;

class function CoCvsEasyViewItem.Create: _CvsEasyViewItem;
begin
  Result := CreateComObject(CLASS_CvsEasyViewItem) as _CvsEasyViewItem;
end;

class function CoCvsEasyViewItem.CreateRemote(const MachineName: string): _CvsEasyViewItem;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsEasyViewItem) as _CvsEasyViewItem;
end;

class function CoCvsFeatureLicense.Create: _CvsFeatureLicense;
begin
  Result := CreateComObject(CLASS_CvsFeatureLicense) as _CvsFeatureLicense;
end;

class function CoCvsFeatureLicense.CreateRemote(const MachineName: string): _CvsFeatureLicense;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsFeatureLicense) as _CvsFeatureLicense;
end;

class function CoCvsIspMessageStore.Create: _CvsIspMessageStore;
begin
  Result := CreateComObject(CLASS_CvsIspMessageStore) as _CvsIspMessageStore;
end;

class function CoCvsIspMessageStore.CreateRemote(const MachineName: string): _CvsIspMessageStore;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsIspMessageStore) as _CvsIspMessageStore;
end;

class function CoCvsJobServerSettings.Create: _CvsJobServerSettings;
begin
  Result := CreateComObject(CLASS_CvsJobServerSettings) as _CvsJobServerSettings;
end;

class function CoCvsJobServerSettings.CreateRemote(const MachineName: string): _CvsJobServerSettings;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsJobServerSettings) as _CvsJobServerSettings;
end;

class function CoCvsOcrMaxDiagnosticData.Create: _CvsOcrMaxDiagnosticData;
begin
  Result := CreateComObject(CLASS_CvsOcrMaxDiagnosticData) as _CvsOcrMaxDiagnosticData;
end;

class function CoCvsOcrMaxDiagnosticData.CreateRemote(const MachineName: string): _CvsOcrMaxDiagnosticData;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsOcrMaxDiagnosticData) as _CvsOcrMaxDiagnosticData;
end;

class function CoFragmentStatistics.Create: _FragmentStatistics;
begin
  Result := CreateComObject(CLASS_FragmentStatistics) as _FragmentStatistics;
end;

class function CoFragmentStatistics.CreateRemote(const MachineName: string): _FragmentStatistics;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_FragmentStatistics) as _FragmentStatistics;
end;

class function CoCharacterStatistics.Create: _CharacterStatistics;
begin
  Result := CreateComObject(CLASS_CharacterStatistics) as _CharacterStatistics;
end;

class function CoCharacterStatistics.CreateRemote(const MachineName: string): _CharacterStatistics;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CharacterStatistics) as _CharacterStatistics;
end;

class function CoCvsOcrMaxDiagnostics.Create: _CvsOcrMaxDiagnostics;
begin
  Result := CreateComObject(CLASS_CvsOcrMaxDiagnostics) as _CvsOcrMaxDiagnostics;
end;

class function CoCvsOcrMaxDiagnostics.CreateRemote(const MachineName: string): _CvsOcrMaxDiagnostics;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsOcrMaxDiagnostics) as _CvsOcrMaxDiagnostics;
end;

class function CoCvsOcrMaxExpressionEditorBase.Create: _CvsOcrMaxExpressionEditorBase;
begin
  Result := CreateComObject(CLASS_CvsOcrMaxExpressionEditorBase) as _CvsOcrMaxExpressionEditorBase;
end;

class function CoCvsOcrMaxExpressionEditorBase.CreateRemote(const MachineName: string): _CvsOcrMaxExpressionEditorBase;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsOcrMaxExpressionEditorBase) as _CvsOcrMaxExpressionEditorBase;
end;

class function CoCvsOcrMaxSpreadsheetExpressionEditor.Create: _CvsOcrMaxSpreadsheetExpressionEditor;
begin
  Result := CreateComObject(CLASS_CvsOcrMaxSpreadsheetExpressionEditor) as _CvsOcrMaxSpreadsheetExpressionEditor;
end;

class function CoCvsOcrMaxSpreadsheetExpressionEditor.CreateRemote(const MachineName: string): _CvsOcrMaxSpreadsheetExpressionEditor;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsOcrMaxSpreadsheetExpressionEditor) as _CvsOcrMaxSpreadsheetExpressionEditor;
end;

class function CoArgUpdateHandler.Create: _ArgUpdateHandler;
begin
  Result := CreateComObject(CLASS_ArgUpdateHandler) as _ArgUpdateHandler;
end;

class function CoArgUpdateHandler.CreateRemote(const MachineName: string): _ArgUpdateHandler;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_ArgUpdateHandler) as _ArgUpdateHandler;
end;

class function CoCvsPowerlinkSettings.Create: _CvsPowerlinkSettings;
begin
  Result := CreateComObject(CLASS_CvsPowerlinkSettings) as _CvsPowerlinkSettings;
end;

class function CoCvsPowerlinkSettings.CreateRemote(const MachineName: string): _CvsPowerlinkSettings;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsPowerlinkSettings) as _CvsPowerlinkSettings;
end;

class function CoCvsRemoteSubnetCollection.Create: _CvsRemoteSubnetCollection;
begin
  Result := CreateComObject(CLASS_CvsRemoteSubnetCollection) as _CvsRemoteSubnetCollection;
end;

class function CoCvsRemoteSubnetCollection.CreateRemote(const MachineName: string): _CvsRemoteSubnetCollection;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsRemoteSubnetCollection) as _CvsRemoteSubnetCollection;
end;

class function CoCvsRemoteSubnetEntry.Create: _CvsRemoteSubnetEntry;
begin
  Result := CreateComObject(CLASS_CvsRemoteSubnetEntry) as _CvsRemoteSubnetEntry;
end;

class function CoCvsRemoteSubnetEntry.CreateRemote(const MachineName: string): _CvsRemoteSubnetEntry;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsRemoteSubnetEntry) as _CvsRemoteSubnetEntry;
end;

class function CoCvsSensor.Create: _CvsSensor;
begin
  Result := CreateComObject(CLASS_CvsSensor) as _CvsSensor;
end;

class function CoCvsSensor.CreateRemote(const MachineName: string): _CvsSensor;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsSensor) as _CvsSensor;
end;

class function CoCvsUser.Create: _CvsUser;
begin
  Result := CreateComObject(CLASS_CvsUser) as _CvsUser;
end;

class function CoCvsUser.CreateRemote(const MachineName: string): _CvsUser;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsUser) as _CvsUser;
end;

class function CoCvsInSightExtensions.Create: _CvsInSightExtensions;
begin
  Result := CreateComObject(CLASS_CvsInSightExtensions) as _CvsInSightExtensions;
end;

class function CoCvsInSightExtensions.CreateRemote(const MachineName: string): _CvsInSightExtensions;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsInSightExtensions) as _CvsInSightExtensions;
end;

class function CoCvsSettings.Create: _CvsSettings;
begin
  Result := CreateComObject(CLASS_CvsSettings) as _CvsSettings;
end;

class function CoCvsSettings.CreateRemote(const MachineName: string): _CvsSettings;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsSettings) as _CvsSettings;
end;

class function CoUndoAction.Create: _UndoAction;
begin
  Result := CreateComObject(CLASS_UndoAction) as _UndoAction;
end;

class function CoUndoAction.CreateRemote(const MachineName: string): _UndoAction;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_UndoAction) as _UndoAction;
end;

class function CoUndoCellTagsAction.Create: _UndoCellTagsAction;
begin
  Result := CreateComObject(CLASS_UndoCellTagsAction) as _UndoCellTagsAction;
end;

class function CoUndoCellTagsAction.CreateRemote(const MachineName: string): _UndoCellTagsAction;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_UndoCellTagsAction) as _UndoCellTagsAction;
end;

class function CoUndoClearAllAction.Create: _UndoClearAllAction;
begin
  Result := CreateComObject(CLASS_UndoClearAllAction) as _UndoClearAllAction;
end;

class function CoUndoClearAllAction.CreateRemote(const MachineName: string): _UndoClearAllAction;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_UndoClearAllAction) as _UndoClearAllAction;
end;

class function CoUndoCutAndPasteAction.Create: _UndoCutAndPasteAction;
begin
  Result := CreateComObject(CLASS_UndoCutAndPasteAction) as _UndoCutAndPasteAction;
end;

class function CoUndoCutAndPasteAction.CreateRemote(const MachineName: string): _UndoCutAndPasteAction;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_UndoCutAndPasteAction) as _UndoCutAndPasteAction;
end;

class function CoUndoEventArgs.Create: _UndoEventArgs;
begin
  Result := CreateComObject(CLASS_UndoEventArgs) as _UndoEventArgs;
end;

class function CoUndoEventArgs.CreateRemote(const MachineName: string): _UndoEventArgs;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_UndoEventArgs) as _UndoEventArgs;
end;

class function CoUndoExpressionAction.Create: _UndoExpressionAction;
begin
  Result := CreateComObject(CLASS_UndoExpressionAction) as _UndoExpressionAction;
end;

class function CoUndoExpressionAction.CreateRemote(const MachineName: string): _UndoExpressionAction;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_UndoExpressionAction) as _UndoExpressionAction;
end;

class function CoUndoOpcTagEditorAction.Create: _UndoOpcTagEditorAction;
begin
  Result := CreateComObject(CLASS_UndoOpcTagEditorAction) as _UndoOpcTagEditorAction;
end;

class function CoUndoOpcTagEditorAction.CreateRemote(const MachineName: string): _UndoOpcTagEditorAction;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_UndoOpcTagEditorAction) as _UndoOpcTagEditorAction;
end;

class function CoUndoPropertySheetAction.Create: _UndoPropertySheetAction;
begin
  Result := CreateComObject(CLASS_UndoPropertySheetAction) as _UndoPropertySheetAction;
end;

class function CoUndoPropertySheetAction.CreateRemote(const MachineName: string): _UndoPropertySheetAction;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_UndoPropertySheetAction) as _UndoPropertySheetAction;
end;

class function CoUndoReferenceRedirectAction.Create: _UndoReferenceRedirectAction;
begin
  Result := CreateComObject(CLASS_UndoReferenceRedirectAction) as _UndoReferenceRedirectAction;
end;

class function CoUndoReferenceRedirectAction.CreateRemote(const MachineName: string): _UndoReferenceRedirectAction;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_UndoReferenceRedirectAction) as _UndoReferenceRedirectAction;
end;

class function CoUndoScriptChangeAction.Create: _UndoScriptChangeAction;
begin
  Result := CreateComObject(CLASS_UndoScriptChangeAction) as _UndoScriptChangeAction;
end;

class function CoUndoScriptChangeAction.CreateRemote(const MachineName: string): _UndoScriptChangeAction;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_UndoScriptChangeAction) as _UndoScriptChangeAction;
end;

class function CoCdsConnectionChange.Create: _CdsConnectionChange;
begin
  Result := CreateComObject(CLASS_CdsConnectionChange) as _CdsConnectionChange;
end;

class function CoCdsConnectionChange.CreateRemote(const MachineName: string): _CdsConnectionChange;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CdsConnectionChange) as _CdsConnectionChange;
end;

class function CoCvsGraphic.Create: _CvsGraphic;
begin
  Result := CreateComObject(CLASS_CvsGraphic) as _CvsGraphic;
end;

class function CoCvsGraphic.CreateRemote(const MachineName: string): _CvsGraphic;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsGraphic) as _CvsGraphic;
end;

class function CoCvsGraphicArrows.Create: _CvsGraphicArrows;
begin
  Result := CreateComObject(CLASS_CvsGraphicArrows) as _CvsGraphicArrows;
end;

class function CoCvsGraphicArrows.CreateRemote(const MachineName: string): _CvsGraphicArrows;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsGraphicArrows) as _CvsGraphicArrows;
end;

class function CoCvsCellEditMaskedRegion.Create: _CvsCellEditMaskedRegion;
begin
  Result := CreateComObject(CLASS_CvsCellEditMaskedRegion) as _CvsCellEditMaskedRegion;
end;

class function CoCvsCellEditMaskedRegion.CreateRemote(const MachineName: string): _CvsCellEditMaskedRegion;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsCellEditMaskedRegion) as _CvsCellEditMaskedRegion;
end;

class function CoCvsCellEditPolylinePath.Create: _CvsCellEditPolylinePath;
begin
  Result := CreateComObject(CLASS_CvsCellEditPolylinePath) as _CvsCellEditPolylinePath;
end;

class function CoCvsCellEditPolylinePath.CreateRemote(const MachineName: string): _CvsCellEditPolylinePath;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsCellEditPolylinePath) as _CvsCellEditPolylinePath;
end;

class function CoCvsAuditMessageSettings.Create: _CvsAuditMessageSettings;
begin
  Result := CreateComObject(CLASS_CvsAuditMessageSettings) as _CvsAuditMessageSettings;
end;

class function CoCvsAuditMessageSettings.CreateRemote(const MachineName: string): _CvsAuditMessageSettings;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsAuditMessageSettings) as _CvsAuditMessageSettings;
end;

class function CoCvsCaliperScoringMethod.Create: _CvsCaliperScoringMethod;
begin
  Result := CreateComObject(CLASS_CvsCaliperScoringMethod) as _CvsCaliperScoringMethod;
end;

class function CoCvsCaliperScoringMethod.CreateRemote(const MachineName: string): _CvsCaliperScoringMethod;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsCaliperScoringMethod) as _CvsCaliperScoringMethod;
end;

class function CoCvsCellEditComposite.Create: _CvsCellEditComposite;
begin
  Result := CreateComObject(CLASS_CvsCellEditComposite) as _CvsCellEditComposite;
end;

class function CoCvsCellEditComposite.CreateRemote(const MachineName: string): _CvsCellEditComposite;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsCellEditComposite) as _CvsCellEditComposite;
end;

class function CoCvsCellEditPolygon.Create: _CvsCellEditPolygon;
begin
  Result := CreateComObject(CLASS_CvsCellEditPolygon) as _CvsCellEditPolygon;
end;

class function CoCvsCellEditPolygon.CreateRemote(const MachineName: string): _CvsCellEditPolygon;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsCellEditPolygon) as _CvsCellEditPolygon;
end;

class function CoCvsColorMatchEditor.Create: _CvsColorMatchEditor;
begin
  Result := CreateComObject(CLASS_CvsColorMatchEditor) as _CvsColorMatchEditor;
end;

class function CoCvsColorMatchEditor.CreateRemote(const MachineName: string): _CvsColorMatchEditor;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsColorMatchEditor) as _CvsColorMatchEditor;
end;

class function CoCvsColorModel.Create: _CvsColorModel;
begin
  Result := CreateComObject(CLASS_CvsColorModel) as _CvsColorModel;
end;

class function CoCvsColorModel.CreateRemote(const MachineName: string): _CvsColorModel;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsColorModel) as _CvsColorModel;
end;

class function CoCvsEasyViewHelper.Create: _CvsEasyViewHelper;
begin
  Result := CreateComObject(CLASS_CvsEasyViewHelper) as _CvsEasyViewHelper;
end;

class function CoCvsEasyViewHelper.CreateRemote(const MachineName: string): _CvsEasyViewHelper;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsEasyViewHelper) as _CvsEasyViewHelper;
end;

class function CoCvsCompositeSubregion.Create: _CvsCompositeSubregion;
begin
  Result := CreateComObject(CLASS_CvsCompositeSubregion) as _CvsCompositeSubregion;
end;

class function CoCvsCompositeSubregion.CreateRemote(const MachineName: string): _CvsCompositeSubregion;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsCompositeSubregion) as _CvsCompositeSubregion;
end;

class function CoCvsGraphicCompositeBase.Create: _CvsGraphicCompositeBase;
begin
  Result := CreateComObject(CLASS_CvsGraphicCompositeBase) as _CvsGraphicCompositeBase;
end;

class function CoCvsGraphicCompositeBase.CreateRemote(const MachineName: string): _CvsGraphicCompositeBase;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsGraphicCompositeBase) as _CvsGraphicCompositeBase;
end;

class function CoCvsGraphicComposite.Create: _CvsGraphicComposite;
begin
  Result := CreateComObject(CLASS_CvsGraphicComposite) as _CvsGraphicComposite;
end;

class function CoCvsGraphicComposite.CreateRemote(const MachineName: string): _CvsGraphicComposite;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsGraphicComposite) as _CvsGraphicComposite;
end;

class function CoCvsGraphicPolylinePath.Create: _CvsGraphicPolylinePath;
begin
  Result := CreateComObject(CLASS_CvsGraphicPolylinePath) as _CvsGraphicPolylinePath;
end;

class function CoCvsGraphicPolylinePath.CreateRemote(const MachineName: string): _CvsGraphicPolylinePath;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsGraphicPolylinePath) as _CvsGraphicPolylinePath;
end;

class function CoCvsGraphicMaskedRegion.Create: _CvsGraphicMaskedRegion;
begin
  Result := CreateComObject(CLASS_CvsGraphicMaskedRegion) as _CvsGraphicMaskedRegion;
end;

class function CoCvsGraphicMaskedRegion.CreateRemote(const MachineName: string): _CvsGraphicMaskedRegion;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsGraphicMaskedRegion) as _CvsGraphicMaskedRegion;
end;

class function CoCvsGraphicDefectPoints.Create: _CvsGraphicDefectPoints;
begin
  Result := CreateComObject(CLASS_CvsGraphicDefectPoints) as _CvsGraphicDefectPoints;
end;

class function CoCvsGraphicDefectPoints.CreateRemote(const MachineName: string): _CvsGraphicDefectPoints;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsGraphicDefectPoints) as _CvsGraphicDefectPoints;
end;

class function CoCvsGraphicEdgePoints.Create: _CvsGraphicEdgePoints;
begin
  Result := CreateComObject(CLASS_CvsGraphicEdgePoints) as _CvsGraphicEdgePoints;
end;

class function CoCvsGraphicEdgePoints.CreateRemote(const MachineName: string): _CvsGraphicEdgePoints;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsGraphicEdgePoints) as _CvsGraphicEdgePoints;
end;

class function CoCvsGraphicExtremePoints.Create: _CvsGraphicExtremePoints;
begin
  Result := CreateComObject(CLASS_CvsGraphicExtremePoints) as _CvsGraphicExtremePoints;
end;

class function CoCvsGraphicExtremePoints.CreateRemote(const MachineName: string): _CvsGraphicExtremePoints;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsGraphicExtremePoints) as _CvsGraphicExtremePoints;
end;

class function CoCvsGraphicFilledBox.Create: _CvsGraphicFilledBox;
begin
  Result := CreateComObject(CLASS_CvsGraphicFilledBox) as _CvsGraphicFilledBox;
end;

class function CoCvsGraphicFilledBox.CreateRemote(const MachineName: string): _CvsGraphicFilledBox;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsGraphicFilledBox) as _CvsGraphicFilledBox;
end;

class function CoCvsGraphicParallelogram.Create: _CvsGraphicParallelogram;
begin
  Result := CreateComObject(CLASS_CvsGraphicParallelogram) as _CvsGraphicParallelogram;
end;

class function CoCvsGraphicParallelogram.CreateRemote(const MachineName: string): _CvsGraphicParallelogram;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsGraphicParallelogram) as _CvsGraphicParallelogram;
end;

class function CoCvsGraphicPolygon.Create: _CvsGraphicPolygon;
begin
  Result := CreateComObject(CLASS_CvsGraphicPolygon) as _CvsGraphicPolygon;
end;

class function CoCvsGraphicPolygon.CreateRemote(const MachineName: string): _CvsGraphicPolygon;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsGraphicPolygon) as _CvsGraphicPolygon;
end;

class function CoCvsJobAnalyzer.Create: _CvsJobAnalyzer;
begin
  Result := CreateComObject(CLASS_CvsJobAnalyzer) as _CvsJobAnalyzer;
end;

class function CoCvsJobAnalyzer.CreateRemote(const MachineName: string): _CvsJobAnalyzer;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsJobAnalyzer) as _CvsJobAnalyzer;
end;

class function CoCvsModbusSettings.Create: _CvsModbusSettings;
begin
  Result := CreateComObject(CLASS_CvsModbusSettings) as _CvsModbusSettings;
end;

class function CoCvsModbusSettings.CreateRemote(const MachineName: string): _CvsModbusSettings;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsModbusSettings) as _CvsModbusSettings;
end;

class function CoCvsString.Create: _CvsString;
begin
  Result := CreateComObject(CLASS_CvsString) as _CvsString;
end;

class function CoCvsString.CreateRemote(const MachineName: string): _CvsString;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsString) as _CvsString;
end;

class function CoStringProcessing.Create: _StringProcessing;
begin
  Result := CreateComObject(CLASS_StringProcessing) as _StringProcessing;
end;

class function CoStringProcessing.CreateRemote(const MachineName: string): _StringProcessing;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_StringProcessing) as _StringProcessing;
end;

class function CoStringBuilderProcessing.Create: _StringBuilderProcessing;
begin
  Result := CreateComObject(CLASS_StringBuilderProcessing) as _StringBuilderProcessing;
end;

class function CoStringBuilderProcessing.CreateRemote(const MachineName: string): _StringBuilderProcessing;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_StringBuilderProcessing) as _StringBuilderProcessing;
end;

class function CoCvsSymbolicTag.Create: _CvsSymbolicTag;
begin
  Result := CreateComObject(CLASS_CvsSymbolicTag) as _CvsSymbolicTag;
end;

class function CoCvsSymbolicTag.CreateRemote(const MachineName: string): _CvsSymbolicTag;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsSymbolicTag) as _CvsSymbolicTag;
end;

class function CoCvsSymbolicTagCollection.Create: _CvsSymbolicTagCollection;
begin
  Result := CreateComObject(CLASS_CvsSymbolicTagCollection) as _CvsSymbolicTagCollection;
end;

class function CoCvsSymbolicTagCollection.CreateRemote(const MachineName: string): _CvsSymbolicTagCollection;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsSymbolicTagCollection) as _CvsSymbolicTagCollection;
end;

class function CoCvsDiscreteIOLines.Create: _CvsDiscreteIOLines;
begin
  Result := CreateComObject(CLASS_CvsDiscreteIOLines) as _CvsDiscreteIOLines;
end;

class function CoCvsDiscreteIOLines.CreateRemote(const MachineName: string): _CvsDiscreteIOLines;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsDiscreteIOLines) as _CvsDiscreteIOLines;
end;

class function CoCvsUniversalIOSettings.Create: _CvsUniversalIOSettings;
begin
  Result := CreateComObject(CLASS_CvsUniversalIOSettings) as _CvsUniversalIOSettings;
end;

class function CoCvsUniversalIOSettings.CreateRemote(const MachineName: string): _CvsUniversalIOSettings;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsUniversalIOSettings) as _CvsUniversalIOSettings;
end;

class function CoCvsPassFailSettings.Create: _CvsPassFailSettings;
begin
  Result := CreateComObject(CLASS_CvsPassFailSettings) as _CvsPassFailSettings;
end;

class function CoCvsPassFailSettings.CreateRemote(const MachineName: string): _CvsPassFailSettings;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsPassFailSettings) as _CvsPassFailSettings;
end;

class function CoCvsHostCollection.Create: _CvsHostCollection;
begin
  Result := CreateComObject(CLASS_CvsHostCollection) as _CvsHostCollection;
end;

class function CoCvsHostCollection.CreateRemote(const MachineName: string): _CvsHostCollection;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsHostCollection) as _CvsHostCollection;
end;

class function CoCvsHost.Create: _CvsHost;
begin
  Result := CreateComObject(CLASS_CvsHost) as _CvsHost;
end;

class function CoCvsHost.CreateRemote(const MachineName: string): _CvsHost;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsHost) as _CvsHost;
end;

class function CoCvsHostSensor.Create: _CvsHostSensor;
begin
  Result := CreateComObject(CLASS_CvsHostSensor) as _CvsHostSensor;
end;

class function CoCvsHostSensor.CreateRemote(const MachineName: string): _CvsHostSensor;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsHostSensor) as _CvsHostSensor;
end;

class function CoCvsNetworkListener.Create: _CvsNetworkListener;
begin
  Result := CreateComObject(CLASS_CvsNetworkListener) as _CvsNetworkListener;
end;

class function CoCvsNetworkListener.CreateRemote(const MachineName: string): _CvsNetworkListener;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsNetworkListener) as _CvsNetworkListener;
end;

class function CoCvsNetworkMonitor.Create: _CvsNetworkMonitor;
begin
  Result := CreateComObject(CLASS_CvsNetworkMonitor) as _CvsNetworkMonitor;
end;

class function CoCvsNetworkMonitor.CreateRemote(const MachineName: string): _CvsNetworkMonitor;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsNetworkMonitor) as _CvsNetworkMonitor;
end;

class function CoCvsNetworkMonitorSubnet.Create: _CvsNetworkMonitorSubnet;
begin
  Result := CreateComObject(CLASS_CvsNetworkMonitorSubnet) as _CvsNetworkMonitorSubnet;
end;

class function CoCvsNetworkMonitorSubnet.CreateRemote(const MachineName: string): _CvsNetworkMonitorSubnet;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsNetworkMonitorSubnet) as _CvsNetworkMonitorSubnet;
end;

class function CoCvsOcrMaxAutoTune.Create: _CvsOcrMaxAutoTune;
begin
  Result := CreateComObject(CLASS_CvsOcrMaxAutoTune) as _CvsOcrMaxAutoTune;
end;

class function CoCvsOcrMaxAutoTune.CreateRemote(const MachineName: string): _CvsOcrMaxAutoTune;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsOcrMaxAutoTune) as _CvsOcrMaxAutoTune;
end;

class function CoAutoTuneSettings.Create: _AutoTuneSettings;
begin
  Result := CreateComObject(CLASS_AutoTuneSettings) as _AutoTuneSettings;
end;

class function CoAutoTuneSettings.CreateRemote(const MachineName: string): _AutoTuneSettings;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_AutoTuneSettings) as _AutoTuneSettings;
end;

class function CoSpaceSettings.Create: _SpaceSettings;
begin
  Result := CreateComObject(CLASS_SpaceSettings) as _SpaceSettings;
end;

class function CoSpaceSettings.CreateRemote(const MachineName: string): _SpaceSettings;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_SpaceSettings) as _SpaceSettings;
end;

class function CoAutoSegmentResult.Create: _AutoSegmentResult;
begin
  Result := CreateComObject(CLASS_AutoSegmentResult) as _AutoSegmentResult;
end;

class function CoAutoSegmentResult.CreateRemote(const MachineName: string): _AutoSegmentResult;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_AutoSegmentResult) as _AutoSegmentResult;
end;

class function CoAutoTuneRecord.Create: _AutoTuneRecord;
begin
  Result := CreateComObject(CLASS_AutoTuneRecord) as _AutoTuneRecord;
end;

class function CoAutoTuneRecord.CreateRemote(const MachineName: string): _AutoTuneRecord;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_AutoTuneRecord) as _AutoTuneRecord;
end;

class function CoAutoTuneException.Create: _AutoTuneException;
begin
  Result := CreateComObject(CLASS_AutoTuneException) as _AutoTuneException;
end;

class function CoAutoTuneException.CreateRemote(const MachineName: string): _AutoTuneException;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_AutoTuneException) as _AutoTuneException;
end;

class function CoArgUpdateHandler_2.Create: _ArgUpdateHandler_2;
begin
  Result := CreateComObject(CLASS_ArgUpdateHandler_2) as _ArgUpdateHandler_2;
end;

class function CoArgUpdateHandler_2.CreateRemote(const MachineName: string): _ArgUpdateHandler_2;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_ArgUpdateHandler_2) as _ArgUpdateHandler_2;
end;

class function CoAutoTuneSessionClosedEventArgs.Create: _AutoTuneSessionClosedEventArgs;
begin
  Result := CreateComObject(CLASS_AutoTuneSessionClosedEventArgs) as _AutoTuneSessionClosedEventArgs;
end;

class function CoAutoTuneSessionClosedEventArgs.CreateRemote(const MachineName: string): _AutoTuneSessionClosedEventArgs;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_AutoTuneSessionClosedEventArgs) as _AutoTuneSessionClosedEventArgs;
end;

class function CoAutoTuneSessionClosedHandler.Create: _AutoTuneSessionClosedHandler;
begin
  Result := CreateComObject(CLASS_AutoTuneSessionClosedHandler) as _AutoTuneSessionClosedHandler;
end;

class function CoAutoTuneSessionClosedHandler.CreateRemote(const MachineName: string): _AutoTuneSessionClosedHandler;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_AutoTuneSessionClosedHandler) as _AutoTuneSessionClosedHandler;
end;

class function CoCvsCellButton.Create: _CvsCellButton;
begin
  Result := CreateComObject(CLASS_CvsCellButton) as _CvsCellButton;
end;

class function CoCvsCellButton.CreateRemote(const MachineName: string): _CvsCellButton;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsCellButton) as _CvsCellButton;
end;

class function CoCvsCellChart.Create: _CvsCellChart;
begin
  Result := CreateComObject(CLASS_CvsCellChart) as _CvsCellChart;
end;

class function CoCvsCellChart.CreateRemote(const MachineName: string): _CvsCellChart;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsCellChart) as _CvsCellChart;
end;

class function CoCvsCellCheckBox.Create: _CvsCellCheckBox;
begin
  Result := CreateComObject(CLASS_CvsCellCheckBox) as _CvsCellCheckBox;
end;

class function CoCvsCellCheckBox.CreateRemote(const MachineName: string): _CvsCellCheckBox;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsCellCheckBox) as _CvsCellCheckBox;
end;

class function CoCvsCellCollection.Create: _CvsCellCollection;
begin
  Result := CreateComObject(CLASS_CvsCellCollection) as _CvsCellCollection;
end;

class function CoCvsCellCollection.CreateRemote(const MachineName: string): _CvsCellCollection;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsCellCollection) as _CvsCellCollection;
end;

class function CoCvsCellColorLabel.Create: _CvsCellColorLabel;
begin
  Result := CreateComObject(CLASS_CvsCellColorLabel) as _CvsCellColorLabel;
end;

class function CoCvsCellColorLabel.CreateRemote(const MachineName: string): _CvsCellColorLabel;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsCellColorLabel) as _CvsCellColorLabel;
end;

class function CoCvsCellDialog.Create: _CvsCellDialog;
begin
  Result := CreateComObject(CLASS_CvsCellDialog) as _CvsCellDialog;
end;

class function CoCvsCellDialog.CreateRemote(const MachineName: string): _CvsCellDialog;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsCellDialog) as _CvsCellDialog;
end;

class function CoCvsCellEditCircle.Create: _CvsCellEditCircle;
begin
  Result := CreateComObject(CLASS_CvsCellEditCircle) as _CvsCellEditCircle;
end;

class function CoCvsCellEditCircle.CreateRemote(const MachineName: string): _CvsCellEditCircle;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsCellEditCircle) as _CvsCellEditCircle;
end;

class function CoCvsCellEditFloat.Create: _CvsCellEditFloat;
begin
  Result := CreateComObject(CLASS_CvsCellEditFloat) as _CvsCellEditFloat;
end;

class function CoCvsCellEditFloat.CreateRemote(const MachineName: string): _CvsCellEditFloat;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsCellEditFloat) as _CvsCellEditFloat;
end;

class function CoCvsCellEditInt.Create: _CvsCellEditInt;
begin
  Result := CreateComObject(CLASS_CvsCellEditInt) as _CvsCellEditInt;
end;

class function CoCvsCellEditInt.CreateRemote(const MachineName: string): _CvsCellEditInt;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsCellEditInt) as _CvsCellEditInt;
end;

class function CoCvsCellEditLine.Create: _CvsCellEditLine;
begin
  Result := CreateComObject(CLASS_CvsCellEditLine) as _CvsCellEditLine;
end;

class function CoCvsCellEditLine.CreateRemote(const MachineName: string): _CvsCellEditLine;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsCellEditLine) as _CvsCellEditLine;
end;

class function CoCvsCellEditPoint.Create: _CvsCellEditPoint;
begin
  Result := CreateComObject(CLASS_CvsCellEditPoint) as _CvsCellEditPoint;
end;

class function CoCvsCellEditPoint.CreateRemote(const MachineName: string): _CvsCellEditPoint;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsCellEditPoint) as _CvsCellEditPoint;
end;

class function CoCvsCellEditRegion.Create: _CvsCellEditRegion;
begin
  Result := CreateComObject(CLASS_CvsCellEditRegion) as _CvsCellEditRegion;
end;

class function CoCvsCellEditRegion.CreateRemote(const MachineName: string): _CvsCellEditRegion;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsCellEditRegion) as _CvsCellEditRegion;
end;

class function CoCvsCellEditString.Create: _CvsCellEditString;
begin
  Result := CreateComObject(CLASS_CvsCellEditString) as _CvsCellEditString;
end;

class function CoCvsCellEditString.CreateRemote(const MachineName: string): _CvsCellEditString;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsCellEditString) as _CvsCellEditString;
end;

class function CoCvsCellEditAnnulus.Create: _CvsCellEditAnnulus;
begin
  Result := CreateComObject(CLASS_CvsCellEditAnnulus) as _CvsCellEditAnnulus;
end;

class function CoCvsCellEditAnnulus.CreateRemote(const MachineName: string): _CvsCellEditAnnulus;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsCellEditAnnulus) as _CvsCellEditAnnulus;
end;

class function CoCvsCellEmpty.Create: _CvsCellEmpty;
begin
  Result := CreateComObject(CLASS_CvsCellEmpty) as _CvsCellEmpty;
end;

class function CoCvsCellEmpty.CreateRemote(const MachineName: string): _CvsCellEmpty;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsCellEmpty) as _CvsCellEmpty;
end;

class function CoCvsCellError.Create: _CvsCellError;
begin
  Result := CreateComObject(CLASS_CvsCellError) as _CvsCellError;
end;

class function CoCvsCellError.CreateRemote(const MachineName: string): _CvsCellError;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsCellError) as _CvsCellError;
end;

class function CoCvsCellFloat.Create: _CvsCellFloat;
begin
  Result := CreateComObject(CLASS_CvsCellFloat) as _CvsCellFloat;
end;

class function CoCvsCellFloat.CreateRemote(const MachineName: string): _CvsCellFloat;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsCellFloat) as _CvsCellFloat;
end;

class function CoCvsCellInt.Create: _CvsCellInt;
begin
  Result := CreateComObject(CLASS_CvsCellInt) as _CvsCellInt;
end;

class function CoCvsCellInt.CreateRemote(const MachineName: string): _CvsCellInt;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsCellInt) as _CvsCellInt;
end;

class function CoCvsCellLink.Create: _CvsCellLink;
begin
  Result := CreateComObject(CLASS_CvsCellLink) as _CvsCellLink;
end;

class function CoCvsCellLink.CreateRemote(const MachineName: string): _CvsCellLink;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsCellLink) as _CvsCellLink;
end;

class function CoCvsCellListBox.Create: _CvsCellListBox;
begin
  Result := CreateComObject(CLASS_CvsCellListBox) as _CvsCellListBox;
end;

class function CoCvsCellListBox.CreateRemote(const MachineName: string): _CvsCellListBox;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsCellListBox) as _CvsCellListBox;
end;

class function CoCvsCellMultiStatus.Create: _CvsCellMultiStatus;
begin
  Result := CreateComObject(CLASS_CvsCellMultiStatus) as _CvsCellMultiStatus;
end;

class function CoCvsCellMultiStatus.CreateRemote(const MachineName: string): _CvsCellMultiStatus;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsCellMultiStatus) as _CvsCellMultiStatus;
end;

class function CoCvsCellSelect.Create: _CvsCellSelect;
begin
  Result := CreateComObject(CLASS_CvsCellSelect) as _CvsCellSelect;
end;

class function CoCvsCellSelect.CreateRemote(const MachineName: string): _CvsCellSelect;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsCellSelect) as _CvsCellSelect;
end;

class function CoCvsCellStatus.Create: _CvsCellStatus;
begin
  Result := CreateComObject(CLASS_CvsCellStatus) as _CvsCellStatus;
end;

class function CoCvsCellStatus.CreateRemote(const MachineName: string): _CvsCellStatus;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsCellStatus) as _CvsCellStatus;
end;

class function CoCvsCellStatusLight.Create: _CvsCellStatusLight;
begin
  Result := CreateComObject(CLASS_CvsCellStatusLight) as _CvsCellStatusLight;
end;

class function CoCvsCellStatusLight.CreateRemote(const MachineName: string): _CvsCellStatusLight;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsCellStatusLight) as _CvsCellStatusLight;
end;

class function CoCvsCellWizard.Create: _CvsCellWizard;
begin
  Result := CreateComObject(CLASS_CvsCellWizard) as _CvsCellWizard;
end;

class function CoCvsCellWizard.CreateRemote(const MachineName: string): _CvsCellWizard;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsCellWizard) as _CvsCellWizard;
end;

class function CoCvsDiscreteInputLine.Create: _CvsDiscreteInputLine;
begin
  Result := CreateComObject(CLASS_CvsDiscreteInputLine) as _CvsDiscreteInputLine;
end;

class function CoCvsDiscreteInputLine.CreateRemote(const MachineName: string): _CvsDiscreteInputLine;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsDiscreteInputLine) as _CvsDiscreteInputLine;
end;

class function CoCvsInputCollection.Create: _CvsInputCollection;
begin
  Result := CreateComObject(CLASS_CvsInputCollection) as _CvsInputCollection;
end;

class function CoCvsInputCollection.CreateRemote(const MachineName: string): _CvsInputCollection;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsInputCollection) as _CvsInputCollection;
end;

class function CoCvsOutputCollection.Create: _CvsOutputCollection;
begin
  Result := CreateComObject(CLASS_CvsOutputCollection) as _CvsOutputCollection;
end;

class function CoCvsOutputCollection.CreateRemote(const MachineName: string): _CvsOutputCollection;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsOutputCollection) as _CvsOutputCollection;
end;

class function CoCvsDiscreteOutputLine.Create: _CvsDiscreteOutputLine;
begin
  Result := CreateComObject(CLASS_CvsDiscreteOutputLine) as _CvsDiscreteOutputLine;
end;

class function CoCvsDiscreteOutputLine.CreateRemote(const MachineName: string): _CvsDiscreteOutputLine;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsDiscreteOutputLine) as _CvsDiscreteOutputLine;
end;

class function CoCvsExpansionIOSettings.Create: _CvsExpansionIOSettings;
begin
  Result := CreateComObject(CLASS_CvsExpansionIOSettings) as _CvsExpansionIOSettings;
end;

class function CoCvsExpansionIOSettings.CreateRemote(const MachineName: string): _CvsExpansionIOSettings;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsExpansionIOSettings) as _CvsExpansionIOSettings;
end;

class function CoCvsFile.Create: _CvsFile;
begin
  Result := CreateComObject(CLASS_CvsFile) as _CvsFile;
end;

class function CoCvsFile.CreateRemote(const MachineName: string): _CvsFile;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsFile) as _CvsFile;
end;

class function CoCvsFileCollection.Create: _CvsFileCollection;
begin
  Result := CreateComObject(CLASS_CvsFileCollection) as _CvsFileCollection;
end;

class function CoCvsFileCollection.CreateRemote(const MachineName: string): _CvsFileCollection;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsFileCollection) as _CvsFileCollection;
end;

class function CoCvsFileManager.Create: _CvsFileManager;
begin
  Result := CreateComObject(CLASS_CvsFileManager) as _CvsFileManager;
end;

class function CoCvsFileManager.CreateRemote(const MachineName: string): _CvsFileManager;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsFileManager) as _CvsFileManager;
end;

class function CoCvsFtpClient.Create: _CvsFtpClient;
begin
  Result := CreateComObject(CLASS_CvsFtpClient) as _CvsFtpClient;
end;

class function CoCvsFtpClient.CreateRemote(const MachineName: string): _CvsFtpClient;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsFtpClient) as _CvsFtpClient;
end;

class function CoCvsFtpSettings.Create: _CvsFtpSettings;
begin
  Result := CreateComObject(CLASS_CvsFtpSettings) as _CvsFtpSettings;
end;

class function CoCvsFtpSettings.CreateRemote(const MachineName: string): _CvsFtpSettings;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsFtpSettings) as _CvsFtpSettings;
end;

class function CoCvsGraphic4Side.Create: _CvsGraphic4Side;
begin
  Result := CreateComObject(CLASS_CvsGraphic4Side) as _CvsGraphic4Side;
end;

class function CoCvsGraphic4Side.CreateRemote(const MachineName: string): _CvsGraphic4Side;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsGraphic4Side) as _CvsGraphic4Side;
end;

class function CoCvsGraphicArc.Create: _CvsGraphicArc;
begin
  Result := CreateComObject(CLASS_CvsGraphicArc) as _CvsGraphicArc;
end;

class function CoCvsGraphicArc.CreateRemote(const MachineName: string): _CvsGraphicArc;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsGraphicArc) as _CvsGraphicArc;
end;

class function CoCvsGraphicBlob.Create: _CvsGraphicBlob;
begin
  Result := CreateComObject(CLASS_CvsGraphicBlob) as _CvsGraphicBlob;
end;

class function CoCvsGraphicBlob.CreateRemote(const MachineName: string): _CvsGraphicBlob;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsGraphicBlob) as _CvsGraphicBlob;
end;

class function CoCvsGraphicCircle.Create: _CvsGraphicCircle;
begin
  Result := CreateComObject(CLASS_CvsGraphicCircle) as _CvsGraphicCircle;
end;

class function CoCvsGraphicCircle.CreateRemote(const MachineName: string): _CvsGraphicCircle;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsGraphicCircle) as _CvsGraphicCircle;
end;

class function CoCvsGraphicCollection.Create: _CvsGraphicCollection;
begin
  Result := CreateComObject(CLASS_CvsGraphicCollection) as _CvsGraphicCollection;
end;

class function CoCvsGraphicCollection.CreateRemote(const MachineName: string): _CvsGraphicCollection;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsGraphicCollection) as _CvsGraphicCollection;
end;

class function CoCvsGraphicPatternMatch.Create: _CvsGraphicPatternMatch;
begin
  Result := CreateComObject(CLASS_CvsGraphicPatternMatch) as _CvsGraphicPatternMatch;
end;

class function CoCvsGraphicPatternMatch.CreateRemote(const MachineName: string): _CvsGraphicPatternMatch;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsGraphicPatternMatch) as _CvsGraphicPatternMatch;
end;

class function CoCvsPatternMatchSegment.Create: _CvsPatternMatchSegment;
begin
  Result := CreateComObject(CLASS_CvsPatternMatchSegment) as _CvsPatternMatchSegment;
end;

class function CoCvsPatternMatchSegment.CreateRemote(const MachineName: string): _CvsPatternMatchSegment;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsPatternMatchSegment) as _CvsPatternMatchSegment;
end;

class function CoCvsGraphicCross.Create: _CvsGraphicCross;
begin
  Result := CreateComObject(CLASS_CvsGraphicCross) as _CvsGraphicCross;
end;

class function CoCvsGraphicCross.CreateRemote(const MachineName: string): _CvsGraphicCross;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsGraphicCross) as _CvsGraphicCross;
end;

class function CoCvsGraphicEdgeProfile.Create: _CvsGraphicEdgeProfile;
begin
  Result := CreateComObject(CLASS_CvsGraphicEdgeProfile) as _CvsGraphicEdgeProfile;
end;

class function CoCvsGraphicEdgeProfile.CreateRemote(const MachineName: string): _CvsGraphicEdgeProfile;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsGraphicEdgeProfile) as _CvsGraphicEdgeProfile;
end;

class function CoCvsGraphicFixture.Create: _CvsGraphicFixture;
begin
  Result := CreateComObject(CLASS_CvsGraphicFixture) as _CvsGraphicFixture;
end;

class function CoCvsGraphicFixture.CreateRemote(const MachineName: string): _CvsGraphicFixture;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsGraphicFixture) as _CvsGraphicFixture;
end;

class function CoCvsGraphicHistogram.Create: _CvsGraphicHistogram;
begin
  Result := CreateComObject(CLASS_CvsGraphicHistogram) as _CvsGraphicHistogram;
end;

class function CoCvsGraphicHistogram.CreateRemote(const MachineName: string): _CvsGraphicHistogram;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsGraphicHistogram) as _CvsGraphicHistogram;
end;

class function CoCvsGraphicImage.Create: _CvsGraphicImage;
begin
  Result := CreateComObject(CLASS_CvsGraphicImage) as _CvsGraphicImage;
end;

class function CoCvsGraphicImage.CreateRemote(const MachineName: string): _CvsGraphicImage;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsGraphicImage) as _CvsGraphicImage;
end;

class function CoCvsGraphicBlobProperties.Create: _CvsGraphicBlobProperties;
begin
  Result := CreateComObject(CLASS_CvsGraphicBlobProperties) as _CvsGraphicBlobProperties;
end;

class function CoCvsGraphicBlobProperties.CreateRemote(const MachineName: string): _CvsGraphicBlobProperties;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsGraphicBlobProperties) as _CvsGraphicBlobProperties;
end;

class function CoCvsGraphicLine.Create: _CvsGraphicLine;
begin
  Result := CreateComObject(CLASS_CvsGraphicLine) as _CvsGraphicLine;
end;

class function CoCvsGraphicLine.CreateRemote(const MachineName: string): _CvsGraphicLine;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsGraphicLine) as _CvsGraphicLine;
end;

class function CoCvsGraphicOffset.Create: _CvsGraphicOffset;
begin
  Result := CreateComObject(CLASS_CvsGraphicOffset) as _CvsGraphicOffset;
end;

class function CoCvsGraphicOffset.CreateRemote(const MachineName: string): _CvsGraphicOffset;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsGraphicOffset) as _CvsGraphicOffset;
end;

class function CoCvsGraphicPoint.Create: _CvsGraphicPoint;
begin
  Result := CreateComObject(CLASS_CvsGraphicPoint) as _CvsGraphicPoint;
end;

class function CoCvsGraphicPoint.CreateRemote(const MachineName: string): _CvsGraphicPoint;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsGraphicPoint) as _CvsGraphicPoint;
end;

class function CoCvsGraphicPolyline.Create: _CvsGraphicPolyline;
begin
  Result := CreateComObject(CLASS_CvsGraphicPolyline) as _CvsGraphicPolyline;
end;

class function CoCvsGraphicPolyline.CreateRemote(const MachineName: string): _CvsGraphicPolyline;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsGraphicPolyline) as _CvsGraphicPolyline;
end;

class function CoCvsGraphicRegion.Create: _CvsGraphicRegion;
begin
  Result := CreateComObject(CLASS_CvsGraphicRegion) as _CvsGraphicRegion;
end;

class function CoCvsGraphicRegion.CreateRemote(const MachineName: string): _CvsGraphicRegion;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsGraphicRegion) as _CvsGraphicRegion;
end;

class function CoCvsGraphicText.Create: _CvsGraphicText;
begin
  Result := CreateComObject(CLASS_CvsGraphicText) as _CvsGraphicText;
end;

class function CoCvsGraphicText.CreateRemote(const MachineName: string): _CvsGraphicText;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsGraphicText) as _CvsGraphicText;
end;

class function CoCvsGraphicAnnulus.Create: _CvsGraphicAnnulus;
begin
  Result := CreateComObject(CLASS_CvsGraphicAnnulus) as _CvsGraphicAnnulus;
end;

class function CoCvsGraphicAnnulus.CreateRemote(const MachineName: string): _CvsGraphicAnnulus;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsGraphicAnnulus) as _CvsGraphicAnnulus;
end;

class function CoCvsHostTableCollection.Create: _CvsHostTableCollection;
begin
  Result := CreateComObject(CLASS_CvsHostTableCollection) as _CvsHostTableCollection;
end;

class function CoCvsHostTableCollection.CreateRemote(const MachineName: string): _CvsHostTableCollection;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsHostTableCollection) as _CvsHostTableCollection;
end;

class function CoCvsHostTableEntry.Create: _CvsHostTableEntry;
begin
  Result := CreateComObject(CLASS_CvsHostTableEntry) as _CvsHostTableEntry;
end;

class function CoCvsHostTableEntry.CreateRemote(const MachineName: string): _CvsHostTableEntry;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsHostTableEntry) as _CvsHostTableEntry;
end;

class function CoCvsHostVersion.Create: _CvsHostVersion;
begin
  Result := CreateComObject(CLASS_CvsHostVersion) as _CvsHostVersion;
end;

class function CoCvsHostVersion.CreateRemote(const MachineName: string): _CvsHostVersion;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsHostVersion) as _CvsHostVersion;
end;

class function CoCvsImage.Create: _CvsImage;
begin
  Result := CreateComObject(CLASS_CvsImage) as _CvsImage;
end;

class function CoCvsImage.CreateRemote(const MachineName: string): _CvsImage;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsImage) as _CvsImage;
end;

class function CoCvsInSight.Create: _CvsInSight;
begin
  Result := CreateComObject(CLASS_CvsInSight) as _CvsInSight;
end;

class function CoCvsInSight.CreateRemote(const MachineName: string): _CvsInSight;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsInSight) as _CvsInSight;
end;

class function CoCvsJobInfo.Create: _CvsJobInfo;
begin
  Result := CreateComObject(CLASS_CvsJobInfo) as _CvsJobInfo;
end;

class function CoCvsJobInfo.CreateRemote(const MachineName: string): _CvsJobInfo;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsJobInfo) as _CvsJobInfo;
end;

class function CoCvsKernel.Create: _CvsKernel;
begin
  Result := CreateComObject(CLASS_CvsKernel) as _CvsKernel;
end;

class function CoCvsKernel.CreateRemote(const MachineName: string): _CvsKernel;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsKernel) as _CvsKernel;
end;

class function CoCvsLicense.Create: _CvsLicense;
begin
  Result := CreateComObject(CLASS_CvsLicense) as _CvsLicense;
end;

class function CoCvsLicense.CreateRemote(const MachineName: string): _CvsLicense;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsLicense) as _CvsLicense;
end;

class function CoCvsLocalIOSettings.Create: _CvsLocalIOSettings;
begin
  Result := CreateComObject(CLASS_CvsLocalIOSettings) as _CvsLocalIOSettings;
end;

class function CoCvsLocalIOSettings.CreateRemote(const MachineName: string): _CvsLocalIOSettings;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsLocalIOSettings) as _CvsLocalIOSettings;
end;

class function CoCvsNativeModeClient.Create: _CvsNativeModeClient;
begin
  Result := CreateComObject(CLASS_CvsNativeModeClient) as _CvsNativeModeClient;
end;

class function CoCvsNativeModeClient.CreateRemote(const MachineName: string): _CvsNativeModeClient;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsNativeModeClient) as _CvsNativeModeClient;
end;

class function CoCvsNetworkSettings.Create: _CvsNetworkSettings;
begin
  Result := CreateComObject(CLASS_CvsNetworkSettings) as _CvsNetworkSettings;
end;

class function CoCvsNetworkSettings.CreateRemote(const MachineName: string): _CvsNetworkSettings;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsNetworkSettings) as _CvsNetworkSettings;
end;

class function CoCvsPicture.Create: _CvsPicture;
begin
  Result := CreateComObject(CLASS_CvsPicture) as _CvsPicture;
end;

class function CoCvsPicture.CreateRemote(const MachineName: string): _CvsPicture;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsPicture) as _CvsPicture;
end;

class function CoCvsProcessorConstants.Create: _CvsProcessorConstants;
begin
  Result := CreateComObject(CLASS_CvsProcessorConstants) as _CvsProcessorConstants;
end;

class function CoCvsProcessorConstants.CreateRemote(const MachineName: string): _CvsProcessorConstants;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsProcessorConstants) as _CvsProcessorConstants;
end;

class function CoCvsResultSet.Create: _CvsResultSet;
begin
  Result := CreateComObject(CLASS_CvsResultSet) as _CvsResultSet;
end;

class function CoCvsResultSet.CreateRemote(const MachineName: string): _CvsResultSet;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsResultSet) as _CvsResultSet;
end;

class function CoCvsSerialSettings.Create: _CvsSerialSettings;
begin
  Result := CreateComObject(CLASS_CvsSerialSettings) as _CvsSerialSettings;
end;

class function CoCvsSerialSettings.CreateRemote(const MachineName: string): _CvsSerialSettings;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsSerialSettings) as _CvsSerialSettings;
end;

class function CoCvsSntpCurrentData.Create: _CvsSntpCurrentData;
begin
  Result := CreateComObject(CLASS_CvsSntpCurrentData) as _CvsSntpCurrentData;
end;

class function CoCvsSntpCurrentData.CreateRemote(const MachineName: string): _CvsSntpCurrentData;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsSntpCurrentData) as _CvsSntpCurrentData;
end;

class function CoCvsSntpSettings.Create: _CvsSntpSettings;
begin
  Result := CreateComObject(CLASS_CvsSntpSettings) as _CvsSntpSettings;
end;

class function CoCvsSntpSettings.CreateRemote(const MachineName: string): _CvsSntpSettings;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsSntpSettings) as _CvsSntpSettings;
end;

class function CoCvsTimeDateData.Create: _CvsTimeDateData;
begin
  Result := CreateComObject(CLASS_CvsTimeDateData) as _CvsTimeDateData;
end;

class function CoCvsTimeDateData.CreateRemote(const MachineName: string): _CvsTimeDateData;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsTimeDateData) as _CvsTimeDateData;
end;

class function CoCvsTimeZones.Create: _CvsTimeZones;
begin
  Result := CreateComObject(CLASS_CvsTimeZones) as _CvsTimeZones;
end;

class function CoCvsTimeZones.CreateRemote(const MachineName: string): _CvsTimeZones;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsTimeZones) as _CvsTimeZones;
end;

class function CoCvsUserCollection.Create: _CvsUserCollection;
begin
  Result := CreateComObject(CLASS_CvsUserCollection) as _CvsUserCollection;
end;

class function CoCvsUserCollection.CreateRemote(const MachineName: string): _CvsUserCollection;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsUserCollection) as _CvsUserCollection;
end;

class function CoCvsUtility.Create: _CvsUtility;
begin
  Result := CreateComObject(CLASS_CvsUtility) as _CvsUtility;
end;

class function CoCvsUtility.CreateRemote(const MachineName: string): _CvsUtility;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsUtility) as _CvsUtility;
end;

class function CoCvsView.Create: _CvsView;
begin
  Result := CreateComObject(CLASS_CvsView) as _CvsView;
end;

class function CoCvsView.CreateRemote(const MachineName: string): _CvsView;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsView) as _CvsView;
end;

class function CoCvsStateChangedEventArgs.Create: _CvsStateChangedEventArgs;
begin
  Result := CreateComObject(CLASS_CvsStateChangedEventArgs) as _CvsStateChangedEventArgs;
end;

class function CoCvsStateChangedEventArgs.CreateRemote(const MachineName: string): _CvsStateChangedEventArgs;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsStateChangedEventArgs) as _CvsStateChangedEventArgs;
end;

class function CoCvsConnectCompletedEventArgs.Create: _CvsConnectCompletedEventArgs;
begin
  Result := CreateComObject(CLASS_CvsConnectCompletedEventArgs) as _CvsConnectCompletedEventArgs;
end;

class function CoCvsConnectCompletedEventArgs.CreateRemote(const MachineName: string): _CvsConnectCompletedEventArgs;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsConnectCompletedEventArgs) as _CvsConnectCompletedEventArgs;
end;

class function CoCvsInSightMessageEventArgs.Create: _CvsInSightMessageEventArgs;
begin
  Result := CreateComObject(CLASS_CvsInSightMessageEventArgs) as _CvsInSightMessageEventArgs;
end;

class function CoCvsInSightMessageEventArgs.CreateRemote(const MachineName: string): _CvsInSightMessageEventArgs;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsInSightMessageEventArgs) as _CvsInSightMessageEventArgs;
end;

class function CoCvsLoadCompletedEventArgs.Create: _CvsLoadCompletedEventArgs;
begin
  Result := CreateComObject(CLASS_CvsLoadCompletedEventArgs) as _CvsLoadCompletedEventArgs;
end;

class function CoCvsLoadCompletedEventArgs.CreateRemote(const MachineName: string): _CvsLoadCompletedEventArgs;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsLoadCompletedEventArgs) as _CvsLoadCompletedEventArgs;
end;

class function CoCvsSaveCompletedEventArgs.Create: _CvsSaveCompletedEventArgs;
begin
  Result := CreateComObject(CLASS_CvsSaveCompletedEventArgs) as _CvsSaveCompletedEventArgs;
end;

class function CoCvsSaveCompletedEventArgs.CreateRemote(const MachineName: string): _CvsSaveCompletedEventArgs;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsSaveCompletedEventArgs) as _CvsSaveCompletedEventArgs;
end;

class function CoCvsAcceptMessageHandler.Create: _CvsAcceptMessageHandler;
begin
  Result := CreateComObject(CLASS_CvsAcceptMessageHandler) as _CvsAcceptMessageHandler;
end;

class function CoCvsAcceptMessageHandler.CreateRemote(const MachineName: string): _CvsAcceptMessageHandler;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CvsAcceptMessageHandler) as _CvsAcceptMessageHandler;
end;

class function CoBadSnippetException.Create: _BadSnippetException;
begin
  Result := CreateComObject(CLASS_BadSnippetException) as _BadSnippetException;
end;

class function CoBadSnippetException.CreateRemote(const MachineName: string): _BadSnippetException;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_BadSnippetException) as _BadSnippetException;
end;

class function CoSnippet.Create: _Snippet;
begin
  Result := CreateComObject(CLASS_Snippet) as _Snippet;
end;

class function CoSnippet.CreateRemote(const MachineName: string): _Snippet;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_Snippet) as _Snippet;
end;

class function CoSyslog.Create: _Syslog;
begin
  Result := CreateComObject(CLASS_Syslog) as _Syslog;
end;

class function CoSyslog.CreateRemote(const MachineName: string): _Syslog;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_Syslog) as _Syslog;
end;

class function CoSyslogTraceListener.Create: _SyslogTraceListener;
begin
  Result := CreateComObject(CLASS_SyslogTraceListener) as _SyslogTraceListener;
end;

class function CoSyslogTraceListener.CreateRemote(const MachineName: string): _SyslogTraceListener;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_SyslogTraceListener) as _SyslogTraceListener;
end;

end.
