unit CvsInSightDisplayOcx_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// PASTLWTR : 1.2
// File generated on 29/09/2016 10:08:00 from Type Library described below.

// ************************************************************************  //
// Type Lib: C:\Program Files\Common Files\Cognex\In-Sight\5.2.133.2\CvsInSightDisplay.ocx (1)
// LIBID: {5026778F-5E71-466C-B034-EF038AED4258}
// LCID: 0
// Helpfile: 
// HelpString: Cognex  In-Sight Controls
// DepndLst: 
//   (1) v2.0 stdole, (C:\WINDOWS\system32\stdole2.tlb)
//   (2) v5.2 CognexInSightDisplay, (C:\PROGRA~1\COMMON~1\Cognex\In-Sight\52133~1.2\COGNEX~1.TLB)
//   (3) v2.0 System_Drawing, (c:\WINDOWS\Microsoft.NET\Framework\v2.0.50727\System.Drawing.tlb)
//   (4) v5.2 CognexInSight, (C:\PROGRA~1\COMMON~1\Cognex\In-Sight\52133~1.2\COGNEX~2.TLB)
// Errors:
//   Error creating palette bitmap of (TCvsInSightDisplay) : Could not load C:\WINDOWS\system32\CvsInSightDisplay.ocx. The type library may possibly require support libraries that are not on the current search path or are not present on your system
//   Error creating palette bitmap of (TCvsFilmstrip) : Could not load C:\WINDOWS\system32\CvsInSightDisplay.ocx. The type library may possibly require support libraries that are not on the current search path or are not present on your system
//   Error creating palette bitmap of (TCvsSensorFilmstripDisplay) : Could not load C:\WINDOWS\system32\CvsInSightDisplay.ocx. The type library may possibly require support libraries that are not on the current search path or are not present on your system
//   Error creating palette bitmap of (TCvsPlaybackFilmstripDisplay) : Could not load C:\WINDOWS\system32\CvsInSightDisplay.ocx. The type library may possibly require support libraries that are not on the current search path or are not present on your system
// ************************************************************************ //
// *************************************************************************//
// NOTE:                                                                      
// Items guarded by $IFDEF_LIVE_SERVER_AT_DESIGN_TIME are used by properties  
// which return objects that may need to be explicitly created via a function 
// call prior to any access via the property. These items have been disabled  
// in order to prevent accidental use from within the object inspector. You   
// may enable them by defining LIVE_SERVER_AT_DESIGN_TIME or by selectively   
// removing them from the $IFDEF blocks. However, such items must still be    
// programmatically created via a method of the appropriate CoClass before    
// they can be used.                                                          
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
{$WARN SYMBOL_PLATFORM OFF}
{$WRITEABLECONST ON}
{$VARPROPSETTER ON}
interface

uses Windows, ActiveX, Classes, CognexInSight_TLB, CognexInSightDisplay_TLB, Graphics, OleCtrls, OleServer, 
StdVCL, System_Drawing_TLB, Variants;
  

// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  CvsInSightDisplayOcxMajorVersion = 1;
  CvsInSightDisplayOcxMinorVersion = 2;

  LIBID_CvsInSightDisplayOcx: TGUID = '{5026778F-5E71-466C-B034-EF038AED4258}';

  IID_ICvsInSightDisplay: TGUID = '{D93F0D0A-5C98-465C-A300-4BEE37B0E172}';
  DIID__ICvsInSightDisplayEvents: TGUID = '{AAB65625-0DF2-425F-89AE-8AE07431BC50}';
  CLASS_CvsInSightDisplay: TGUID = '{86012278-F05D-4C41-A313-51CE65032312}';
  IID_ICvsFilmstrip: TGUID = '{F2405FF2-CB2B-4221-960A-DA3CC1392081}';
  DIID__ICvsFilmstripEvents: TGUID = '{72AA851A-6FEC-4277-B3D4-574CC3EEE246}';
  CLASS_CvsFilmstrip: TGUID = '{50FDEAE0-F676-489E-BBEB-16AC1628A3CC}';
  IID_ICvsSensorFilmstripDisplay: TGUID = '{3DE3D08D-65B4-4978-8C9B-3E50E217BF98}';
  DIID__ICvsSensorFilmstripDisplayEvents: TGUID = '{A351A047-DDA2-4B3D-85A5-9774564801AC}';
  CLASS_CvsSensorFilmstripDisplay: TGUID = '{42E70435-2112-48FC-B63B-D7CA77AB7C6C}';
  IID_ICvsPlaybackFilmstripDisplay: TGUID = '{F0C1E0CB-52DB-47EA-AC3C-27169D899B16}';
  DIID__ICvsPlaybackFilmstripDisplayEvents: TGUID = '{0A9AF383-19B1-489A-985B-4642B0E7DB89}';
  CLASS_CvsPlaybackFilmstripDisplay: TGUID = '{BDE52065-AE85-4278-8666-124D863847EF}';

// *********************************************************************//
// Declaration of Enumerations defined in Type Library                    
// *********************************************************************//
// Constants for enum CvsDisplayZoom
type
  CvsDisplayZoom = TOleEnum;
const
  CvsDisplayZoom_None = $00000000;
  CvsDisplayZoom_Fit = $00000001;
  CvsDisplayZoom_Fill = $00000002;

// Constants for enum CvsDisplayScrollMode
type
  CvsDisplayScrollMode = TOleEnum;
const
  CvsDisplayScrollMode_None = $FFFFFFFF;
  CvsDisplayScrollMode_Image = $00000000;
  CvsDisplayScrollMode_Grid = $00000001;

// Constants for enum CvsDisplayState
type
  CvsDisplayState = TOleEnum;
const
  CvsDisplayState_Normal = $00000000;
  CvsDisplayState_EditingCellExpression = $00000002;
  CvsDisplayState_EditingCellValue = $00000003;
  CvsDisplayState_EditingReferenceRanges = $00000005;
  CvsDisplayState_EditingGraphics = $00000004;
  CvsDisplayState_Connecting = $00000001;
  CvsDisplayState_Dialog = $00000007;
  CvsDisplayState_DialogEditingReferenceRanges = $00000008;
  CvsDisplayState_Waiting = $00000006;
  CvsDisplayState_Wizard = $0000000A;

// Constants for enum CvsStatusInformationState
type
  CvsStatusInformationState = TOleEnum;
const
  CvsStatusInformationState_None = $00000000;
  CvsStatusInformationState_Info = $00000001;
  CvsStatusInformationState_Warning = $00000002;
  CvsStatusInformationState_Error = $00000003;

// Constants for enum CvsStatusLevelStyle
type
  CvsStatusLevelStyle = TOleEnum;
const
  CvsStatusLevelStyle_Geometric = $00000000;
  CvsStatusLevelStyle_OKorNG = $00000001;
  CvsStatusLevelStyle_Check = $00000002;

// Constants for enum CvsFilmstripScale
type
  CvsFilmstripScale = TOleEnum;
const
  CvsFilmstripScale_Quarter = $00000019;
  CvsFilmstripScale_Half = $00000032;
  CvsFilmstripScale_One = $00000064;
  CvsFilmstripScale_Two = $000000C8;
  CvsFilmstripScale_Four = $00000190;

// Constants for enum CvsImageResolution
type
  CvsImageResolution = TOleEnum;
const
  Default = $00000000;
  Full = $00000001;
  Half = $00000002;
  Quarter = $00000004;

type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary                    
// *********************************************************************//
  ICvsInSightDisplay = interface;
  ICvsInSightDisplayDisp = dispinterface;
  _ICvsInSightDisplayEvents = dispinterface;
  ICvsFilmstrip = interface;
  ICvsFilmstripDisp = dispinterface;
  _ICvsFilmstripEvents = dispinterface;
  ICvsSensorFilmstripDisplay = interface;
  ICvsSensorFilmstripDisplayDisp = dispinterface;
  _ICvsSensorFilmstripDisplayEvents = dispinterface;
  ICvsPlaybackFilmstripDisplay = interface;
  ICvsPlaybackFilmstripDisplayDisp = dispinterface;
  _ICvsPlaybackFilmstripDisplayEvents = dispinterface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                       
// (NOTE: Here we map each CoClass to its Default Interface)              
// *********************************************************************//
  CvsInSightDisplay = ICvsInSightDisplay;
  CvsFilmstrip = ICvsFilmstrip;
  CvsSensorFilmstripDisplay = ICvsSensorFilmstripDisplay;
  CvsPlaybackFilmstripDisplay = ICvsPlaybackFilmstripDisplay;


// *********************************************************************//
// Declaration of structures, unions and aliases.                         
// *********************************************************************//
  PWideString1 = ^WideString; {*}
  PUserType1 = ^tagSAFEARRAY; {*}

  CvsCellLocation = packed record
    row: Integer;
    column: Integer;
  end;

  CvsCellRange = packed record
    Location: CvsCellLocation;
    rows: Integer;
    columns: Integer;
  end;

  Rectangle = packed record
    x: Integer;
    y: Integer;
    width: Integer;
    height: Integer;
  end;

  tagSAFEARRAYBOUND = packed record
    cElements: LongWord;
    lLbound: Integer;
  end;

  tagSAFEARRAY = packed record
    cDims: Word;
    fFeatures: Word;
    cbElements: LongWord;
    cLocks: LongWord;
    pvData: Pointer;
    rgsabound: ^tagSAFEARRAYBOUND;
  end;


// *********************************************************************//
// Interface: ICvsInSightDisplay
// Flags:     (4544) Dual NonExtensible OleAutomation Dispatchable
// GUID:      {D93F0D0A-5C98-465C-A300-4BEE37B0E172}
// *********************************************************************//
  ICvsInSightDisplay = interface(IDispatch)
    ['{D93F0D0A-5C98-465C-A300-4BEE37B0E172}']
    procedure Set_Enabled(pbool: WordBool); safecall;
    function Get_Enabled: WordBool; safecall;
    procedure Refresh; safecall;
    function Get_AutoConnectString: WideString; safecall;
    procedure Set_AutoConnectString(const pVal: WideString); safecall;
    function Get_Action: WideString; safecall;
    procedure Set_Action(const pVal: WideString); safecall;
    function Get_AutoReconnect: WordBool; safecall;
    procedure Set_AutoReconnect(pVal: WordBool); safecall;
    function Get_ConfirmOnlineTransitions: WordBool; safecall;
    procedure Set_ConfirmOnlineTransitions(pVal: WordBool); safecall;
    function Get_CurrentCell: CvsCellLocation; safecall;
    function Get_CurrentCellExpression: WideString; safecall;
    procedure Set_CurrentCellExpression(const pVal: WideString); safecall;
    function Get_Connected: WordBool; safecall;
    function Get_Edit: _CvsInSightDisplayEdit; safecall;
    function Get_FtpPort: Integer; safecall;
    procedure Set_FtpPort(val: Integer); safecall;
    function Get_GridOffset: CvsCellLocation; safecall;
    function Get_GridOpacity: Double; safecall;
    procedure Set_GridOpacity(Scale: Double); safecall;
    function Get_GridScale: Double; safecall;
    procedure Set_GridScale(Scale: Double); safecall;
    function Get_ImageOffset: PointF; safecall;
    function Get_ImageOrientation: CvsImageOrientation; safecall;
    procedure Set_ImageOrientation(pVal: CvsImageOrientation); safecall;
    function Get_ImageScale: Double; safecall;
    procedure Set_ImageScale(Scale: Double); safecall;
    function Get_ImageZoomMode: CvsDisplayZoom; safecall;
    procedure Set_ImageZoomMode(pVal: CvsDisplayZoom); safecall;
    function Get_InSight: _CvsInSight; safecall;
    procedure _Set_InSight(const InSight: _CvsInSight); safecall;
    function Get_MaxUserAccess: CvsInSightSecurityAccess; safecall;
    procedure Set_MaxUserAccess(pVal: CvsInSightSecurityAccess); safecall;
    function Get_Recorder: _CvsInSightDisplayRecorder; safecall;
    function Get_Results: _CvsResultSet; safecall;
    procedure _Set_Results(const pVal: _CvsResultSet); safecall;
    function Get_ScrollMode: CvsDisplayScrollMode; safecall;
    procedure Set_ScrollMode(pVal: CvsDisplayScrollMode); safecall;
    function Get_SelectedRange: CvsCellRange; safecall;
    function Get_ShiftPartialImage: WordBool; safecall;
    procedure Set_ShiftPartialImage(Enabled: WordBool); safecall;
    function Get_ShowCustomView: WordBool; safecall;
    procedure Set_ShowCustomView(Enabled: WordBool); safecall;
    function Get_ShowDependencyErrorsOnly: WordBool; safecall;
    procedure Set_ShowDependencyErrorsOnly(Enabled: WordBool); safecall;
    function Get_ShowGraphics: WordBool; safecall;
    procedure Set_ShowGraphics(Enabled: WordBool); safecall;
    function Get_ShowGrid: WordBool; safecall;
    procedure Set_ShowGrid(pVal: WordBool); safecall;
    function Get_ShowImage: WordBool; safecall;
    procedure Set_ShowImage(Enabled: WordBool); safecall;
    function Get_ShowImageSaturation: WordBool; safecall;
    procedure Set_ShowImageSaturation(Enabled: WordBool); safecall;
    function Get_ShowScrollBars: WordBool; safecall;
    procedure Set_ShowScrollBars(Enabled: WordBool); safecall;
    function Get_SnippetPath: WideString; safecall;
    procedure Set_SnippetPath(const pVal: WideString); safecall;
    function Get_SoftOnline: WordBool; safecall;
    procedure Set_SoftOnline(pVal: WordBool); safecall;
    function Get_State: CvsDisplayState; safecall;
    function Get_StatusInformation: WideString; safecall;
    function Get_StatusInformationState: CvsStatusInformationState; safecall;
    procedure Connect(const hostName: WideString; const userName: WideString; 
                      const password: WideString; forceConnect: WordBool); safecall;
    procedure Disconnect; safecall;
    procedure EditCellGraphic(row: Integer; column: Integer); safecall;
    procedure EnsureCellIsVisible(row: Integer; column: Integer); safecall;
    function GetPicture: IPictureDisp; safecall;
    procedure SaveBitmap(const fileName: WideString); safecall;
    function GetCellBounds(row: Integer; column: Integer; rows: Integer; columns: Integer): Rectangle; safecall;
    procedure SetCurrentCell(row: Integer; column: Integer); safecall;
    procedure SetGridOffset(row: Integer; column: Integer); safecall;
    procedure SetImageScaleAndOffset(Scale: Double; x: Single; y: Single); safecall;
    procedure SetSelectedRange(row: Integer; column: Integer; rows: Integer; columns: Integer); safecall;
    procedure OpenPropertySheet(row: Integer; column: Integer; const expression: WideString); safecall;
    procedure ClickSpreadsheetControl(row: Integer; column: Integer); safecall;
    property Enabled: WordBool read Get_Enabled write Set_Enabled;
    property AutoConnectString: WideString read Get_AutoConnectString write Set_AutoConnectString;
    property Action: WideString read Get_Action write Set_Action;
    property AutoReconnect: WordBool read Get_AutoReconnect write Set_AutoReconnect;
    property ConfirmOnlineTransitions: WordBool read Get_ConfirmOnlineTransitions write Set_ConfirmOnlineTransitions;
    property CurrentCell: CvsCellLocation read Get_CurrentCell;
    property CurrentCellExpression: WideString read Get_CurrentCellExpression write Set_CurrentCellExpression;
    property Connected: WordBool read Get_Connected;
    property Edit: _CvsInSightDisplayEdit read Get_Edit;
    property FtpPort: Integer read Get_FtpPort write Set_FtpPort;
    property GridOffset: CvsCellLocation read Get_GridOffset;
    property GridOpacity: Double read Get_GridOpacity write Set_GridOpacity;
    property GridScale: Double read Get_GridScale write Set_GridScale;
    property ImageOffset: PointF read Get_ImageOffset;
    property ImageOrientation: CvsImageOrientation read Get_ImageOrientation write Set_ImageOrientation;
    property ImageScale: Double read Get_ImageScale write Set_ImageScale;
    property ImageZoomMode: CvsDisplayZoom read Get_ImageZoomMode write Set_ImageZoomMode;
    property InSight: _CvsInSight read Get_InSight write _Set_InSight;
    property MaxUserAccess: CvsInSightSecurityAccess read Get_MaxUserAccess write Set_MaxUserAccess;
    property Recorder: _CvsInSightDisplayRecorder read Get_Recorder;
    property Results: _CvsResultSet read Get_Results write _Set_Results;
    property ScrollMode: CvsDisplayScrollMode read Get_ScrollMode write Set_ScrollMode;
    property SelectedRange: CvsCellRange read Get_SelectedRange;
    property ShiftPartialImage: WordBool read Get_ShiftPartialImage write Set_ShiftPartialImage;
    property ShowCustomView: WordBool read Get_ShowCustomView write Set_ShowCustomView;
    property ShowDependencyErrorsOnly: WordBool read Get_ShowDependencyErrorsOnly write Set_ShowDependencyErrorsOnly;
    property ShowGraphics: WordBool read Get_ShowGraphics write Set_ShowGraphics;
    property ShowGrid: WordBool read Get_ShowGrid write Set_ShowGrid;
    property ShowImage: WordBool read Get_ShowImage write Set_ShowImage;
    property ShowImageSaturation: WordBool read Get_ShowImageSaturation write Set_ShowImageSaturation;
    property ShowScrollBars: WordBool read Get_ShowScrollBars write Set_ShowScrollBars;
    property SnippetPath: WideString read Get_SnippetPath write Set_SnippetPath;
    property SoftOnline: WordBool read Get_SoftOnline write Set_SoftOnline;
    property State: CvsDisplayState read Get_State;
    property StatusInformation: WideString read Get_StatusInformation;
    property StatusInformationState: CvsStatusInformationState read Get_StatusInformationState;
  end;

// *********************************************************************//
// DispIntf:  ICvsInSightDisplayDisp
// Flags:     (4544) Dual NonExtensible OleAutomation Dispatchable
// GUID:      {D93F0D0A-5C98-465C-A300-4BEE37B0E172}
// *********************************************************************//
  ICvsInSightDisplayDisp = dispinterface
    ['{D93F0D0A-5C98-465C-A300-4BEE37B0E172}']
    property Enabled: WordBool dispid -514;
    procedure Refresh; dispid 1610743810;
    property AutoConnectString: WideString dispid 100;
    property Action: WideString dispid 125;
    property AutoReconnect: WordBool dispid 117;
    property ConfirmOnlineTransitions: WordBool dispid 118;
    property CurrentCell: {??CvsCellLocation}OleVariant readonly dispid 126;
    property CurrentCellExpression: WideString dispid 127;
    property Connected: WordBool readonly dispid 128;
    property Edit: _CvsInSightDisplayEdit readonly dispid 129;
    property FtpPort: Integer dispid 123;
    property GridOffset: {??CvsCellLocation}OleVariant readonly dispid 112;
    property GridOpacity: Double dispid 106;
    property GridScale: Double dispid 130;
    property ImageOffset: {??PointF}OleVariant readonly dispid 113;
    property ImageOrientation: CvsImageOrientation dispid 120;
    property ImageScale: Double dispid 107;
    property ImageZoomMode: CvsDisplayZoom dispid 108;
    property InSight: _CvsInSight dispid 131;
    property MaxUserAccess: CvsInSightSecurityAccess dispid 116;
    property Recorder: _CvsInSightDisplayRecorder readonly dispid 132;
    property Results: _CvsResultSet dispid 133;
    property ScrollMode: CvsDisplayScrollMode dispid 134;
    property SelectedRange: {??CvsCellRange}OleVariant readonly dispid 135;
    property ShiftPartialImage: WordBool dispid 119;
    property ShowCustomView: WordBool dispid 114;
    property ShowDependencyErrorsOnly: WordBool dispid 121;
    property ShowGraphics: WordBool dispid 102;
    property ShowGrid: WordBool dispid 103;
    property ShowImage: WordBool dispid 101;
    property ShowImageSaturation: WordBool dispid 122;
    property ShowScrollBars: WordBool dispid 104;
    property SnippetPath: WideString dispid 124;
    property SoftOnline: WordBool dispid 136;
    property State: CvsDisplayState readonly dispid 137;
    property StatusInformation: WideString readonly dispid 138;
    property StatusInformationState: CvsStatusInformationState readonly dispid 139;
    procedure Connect(const hostName: WideString; const userName: WideString; 
                      const password: WideString; forceConnect: WordBool); dispid 1610743871;
    procedure Disconnect; dispid 1610743872;
    procedure EditCellGraphic(row: Integer; column: Integer); dispid 1610743873;
    procedure EnsureCellIsVisible(row: Integer; column: Integer); dispid 1610743874;
    function GetPicture: IPictureDisp; dispid 1610743875;
    procedure SaveBitmap(const fileName: WideString); dispid 1610743876;
    function GetCellBounds(row: Integer; column: Integer; rows: Integer; columns: Integer): {??Rectangle}OleVariant; dispid 1610743877;
    procedure SetCurrentCell(row: Integer; column: Integer); dispid 1610743878;
    procedure SetGridOffset(row: Integer; column: Integer); dispid 1610743879;
    procedure SetImageScaleAndOffset(Scale: Double; x: Single; y: Single); dispid 1610743880;
    procedure SetSelectedRange(row: Integer; column: Integer; rows: Integer; columns: Integer); dispid 1610743881;
    procedure OpenPropertySheet(row: Integer; column: Integer; const expression: WideString); dispid 1610743882;
    procedure ClickSpreadsheetControl(row: Integer; column: Integer); dispid 1610743883;
  end;

// *********************************************************************//
// DispIntf:  _ICvsInSightDisplayEvents
// Flags:     (4096) Dispatchable
// GUID:      {AAB65625-0DF2-425F-89AE-8AE07431BC50}
// *********************************************************************//
  _ICvsInSightDisplayEvents = dispinterface
    ['{AAB65625-0DF2-425F-89AE-8AE07431BC50}']
    procedure ConnectedChanged; dispid 1;
    procedure CurrentCellChanged; dispid 2;
    procedure GridOffsetChanged; dispid 3;
    procedure GridScaleChanged; dispid 4;
    procedure ImageOffsetChanged; dispid 5;
    procedure ImageScaleChanged; dispid 6;
    procedure InSightChanged; dispid 7;
    procedure ResultsChanged; dispid 8;
    procedure ScrollModeChanged; dispid 9;
    procedure SelectedRangeChanged; dispid 10;
    procedure StateChanged; dispid 11;
    procedure StatusInformationChanged; dispid 12;
    procedure CurrentCellExpressionChanged; dispid 14;
    procedure ConnectCompleted(ErrorNumber: Integer; const ErrorMessage: WideString); dispid 15;
    procedure GridOpacityChanged; dispid 16;
    procedure ImageOrientationChanged; dispid 17;
  end;

// *********************************************************************//
// Interface: ICvsFilmstrip
// Flags:     (4544) Dual NonExtensible OleAutomation Dispatchable
// GUID:      {F2405FF2-CB2B-4221-960A-DA3CC1392081}
// *********************************************************************//
  ICvsFilmstrip = interface(IDispatch)
    ['{F2405FF2-CB2B-4221-960A-DA3CC1392081}']
    procedure Set_Enabled(pbool: WordBool); safecall;
    function Get_Enabled: WordBool; safecall;
    procedure Refresh; safecall;
    function Get_FilmQueue: ICvsFilmQueue; safecall;
    procedure _Set_FilmQueue(const pVal: ICvsFilmQueue); safecall;
    function Get_StatusLevelStyle: CvsStatusLevelStyle; safecall;
    procedure Set_StatusLevelStyle(pVal: CvsStatusLevelStyle); safecall;
    function Get_ShowThumbnailImage: WordBool; safecall;
    procedure Set_ShowThumbnailImage(pVal: WordBool); safecall;
    function Get_HeightScale: CvsFilmstripScale; safecall;
    procedure Set_HeightScale(pVal: CvsFilmstripScale); safecall;
    function Get_MaxThumbnailsDisplayed: Integer; safecall;
    function Get_ThumbnailsDisplayed: Integer; safecall;
    function Get_ShowSummary: WordBool; safecall;
    procedure Set_ShowSummary(pVal: WordBool); safecall;
    property Enabled: WordBool read Get_Enabled write Set_Enabled;
    property FilmQueue: ICvsFilmQueue read Get_FilmQueue write _Set_FilmQueue;
    property StatusLevelStyle: CvsStatusLevelStyle read Get_StatusLevelStyle write Set_StatusLevelStyle;
    property ShowThumbnailImage: WordBool read Get_ShowThumbnailImage write Set_ShowThumbnailImage;
    property HeightScale: CvsFilmstripScale read Get_HeightScale write Set_HeightScale;
    property MaxThumbnailsDisplayed: Integer read Get_MaxThumbnailsDisplayed;
    property ThumbnailsDisplayed: Integer read Get_ThumbnailsDisplayed;
    property ShowSummary: WordBool read Get_ShowSummary write Set_ShowSummary;
  end;

// *********************************************************************//
// DispIntf:  ICvsFilmstripDisp
// Flags:     (4544) Dual NonExtensible OleAutomation Dispatchable
// GUID:      {F2405FF2-CB2B-4221-960A-DA3CC1392081}
// *********************************************************************//
  ICvsFilmstripDisp = dispinterface
    ['{F2405FF2-CB2B-4221-960A-DA3CC1392081}']
    property Enabled: WordBool dispid -514;
    procedure Refresh; dispid 1610743810;
    property FilmQueue: ICvsFilmQueue dispid 200;
    property StatusLevelStyle: CvsStatusLevelStyle dispid 201;
    property ShowThumbnailImage: WordBool dispid 202;
    property HeightScale: CvsFilmstripScale dispid 203;
    property MaxThumbnailsDisplayed: Integer readonly dispid 204;
    property ThumbnailsDisplayed: Integer readonly dispid 205;
    property ShowSummary: WordBool dispid 206;
  end;

// *********************************************************************//
// DispIntf:  _ICvsFilmstripEvents
// Flags:     (4096) Dispatchable
// GUID:      {72AA851A-6FEC-4277-B3D4-574CC3EEE246}
// *********************************************************************//
  _ICvsFilmstripEvents = dispinterface
    ['{72AA851A-6FEC-4277-B3D4-574CC3EEE246}']
  end;

// *********************************************************************//
// Interface: ICvsSensorFilmstripDisplay
// Flags:     (4544) Dual NonExtensible OleAutomation Dispatchable
// GUID:      {3DE3D08D-65B4-4978-8C9B-3E50E217BF98}
// *********************************************************************//
  ICvsSensorFilmstripDisplay = interface(IDispatch)
    ['{3DE3D08D-65B4-4978-8C9B-3E50E217BF98}']
    procedure Set_Enabled(pbool: WordBool); safecall;
    function Get_Enabled: WordBool; safecall;
    procedure Refresh; safecall;
    function Get_AutoConnectString: WideString; safecall;
    procedure Set_AutoConnectString(const pVal: WideString); safecall;
    function Get_Action: WideString; safecall;
    procedure Set_Action(const pVal: WideString); safecall;
    function Get_AutoReconnect: WordBool; safecall;
    procedure Set_AutoReconnect(pVal: WordBool); safecall;
    function Get_ConfirmOnlineTransitions: WordBool; safecall;
    procedure Set_ConfirmOnlineTransitions(pVal: WordBool); safecall;
    function Get_CurrentCell: CvsCellLocation; safecall;
    function Get_CurrentCellExpression: WideString; safecall;
    procedure Set_CurrentCellExpression(const pVal: WideString); safecall;
    function Get_Connected: WordBool; safecall;
    function Get_Edit: _CvsInSightDisplayEdit; safecall;
    function Get_FtpPort: Integer; safecall;
    procedure Set_FtpPort(val: Integer); safecall;
    function Get_GridOffset: CvsCellLocation; safecall;
    function Get_GridOpacity: Double; safecall;
    procedure Set_GridOpacity(Scale: Double); safecall;
    function Get_GridScale: Double; safecall;
    procedure Set_GridScale(Scale: Double); safecall;
    function Get_ImageOffset: PointF; safecall;
    function Get_ImageOrientation: CvsImageOrientation; safecall;
    procedure Set_ImageOrientation(pVal: CvsImageOrientation); safecall;
    function Get_ImageScale: Double; safecall;
    procedure Set_ImageScale(Scale: Double); safecall;
    function Get_ImageZoomMode: CvsDisplayZoom; safecall;
    procedure Set_ImageZoomMode(pVal: CvsDisplayZoom); safecall;
    function Get_InSight: _CvsInSight; safecall;
    procedure _Set_InSight(const InSight: _CvsInSight); safecall;
    function Get_MaxUserAccess: CvsInSightSecurityAccess; safecall;
    procedure Set_MaxUserAccess(pVal: CvsInSightSecurityAccess); safecall;
    function Get_Recorder: _CvsInSightDisplayRecorder; safecall;
    function Get_Results: _CvsResultSet; safecall;
    procedure _Set_Results(const pVal: _CvsResultSet); safecall;
    function Get_ScrollMode: CvsDisplayScrollMode; safecall;
    procedure Set_ScrollMode(pVal: CvsDisplayScrollMode); safecall;
    function Get_SelectedRange: CvsCellRange; safecall;
    function Get_ShiftPartialImage: WordBool; safecall;
    procedure Set_ShiftPartialImage(Enabled: WordBool); safecall;
    function Get_ShowCustomView: WordBool; safecall;
    procedure Set_ShowCustomView(Enabled: WordBool); safecall;
    function Get_ShowDependencyErrorsOnly: WordBool; safecall;
    procedure Set_ShowDependencyErrorsOnly(Enabled: WordBool); safecall;
    function Get_ShowGraphics: WordBool; safecall;
    procedure Set_ShowGraphics(Enabled: WordBool); safecall;
    function Get_ShowGrid: WordBool; safecall;
    procedure Set_ShowGrid(pVal: WordBool); safecall;
    function Get_ShowImage: WordBool; safecall;
    procedure Set_ShowImage(Enabled: WordBool); safecall;
    function Get_ShowImageSaturation: WordBool; safecall;
    procedure Set_ShowImageSaturation(Enabled: WordBool); safecall;
    function Get_ShowScrollBars: WordBool; safecall;
    procedure Set_ShowScrollBars(Enabled: WordBool); safecall;
    function Get_SnippetPath: WideString; safecall;
    procedure Set_SnippetPath(const pVal: WideString); safecall;
    function Get_SoftOnline: WordBool; safecall;
    procedure Set_SoftOnline(pVal: WordBool); safecall;
    function Get_State: CvsDisplayState; safecall;
    function Get_StatusInformation: WideString; safecall;
    function Get_StatusInformationState: CvsStatusInformationState; safecall;
    procedure Connect(const hostName: WideString; const userName: WideString; 
                      const password: WideString; forceConnect: WordBool); safecall;
    procedure Disconnect; safecall;
    procedure EditCellGraphic(row: Integer; column: Integer); safecall;
    procedure EnsureCellIsVisible(row: Integer; column: Integer); safecall;
    function GetPicture: IPictureDisp; safecall;
    procedure SaveBitmap(const fileName: WideString); safecall;
    function GetCellBounds(row: Integer; column: Integer; rows: Integer; columns: Integer): Rectangle; safecall;
    procedure SetCurrentCell(row: Integer; column: Integer); safecall;
    procedure SetGridOffset(row: Integer; column: Integer); safecall;
    procedure SetImageScaleAndOffset(Scale: Double; x: Single; y: Single); safecall;
    procedure SetSelectedRange(row: Integer; column: Integer; rows: Integer; columns: Integer); safecall;
    procedure OpenPropertySheet(row: Integer; column: Integer; const expression: WideString); safecall;
    procedure ClickSpreadsheetControl(row: Integer; column: Integer); safecall;
    function Get_FilmQueue: ICvsFilmQueue; safecall;
    procedure _Set_FilmQueue(const pVal: ICvsFilmQueue); safecall;
    function Get_StatusLevelStyle: CvsStatusLevelStyle; safecall;
    procedure Set_StatusLevelStyle(pVal: CvsStatusLevelStyle); safecall;
    function Get_ShowThumbnailImage: WordBool; safecall;
    procedure Set_ShowThumbnailImage(pVal: WordBool); safecall;
    function Get_HeightScale: CvsFilmstripScale; safecall;
    procedure Set_HeightScale(pVal: CvsFilmstripScale); safecall;
    function Get_MaxThumbnailsDisplayed: Integer; safecall;
    function Get_ThumbnailsDisplayed: Integer; safecall;
    function Get_ShowSummary: WordBool; safecall;
    procedure Set_ShowSummary(pVal: WordBool); safecall;
    function Get_SplitterHeight: Integer; safecall;
    procedure Set_SplitterHeight(pVal: Integer); safecall;
    function Get_FilmstripHeight: Integer; safecall;
    procedure Set_FilmstripHeight(pVal: Integer); safecall;
    function Get_IsSplitterFixed: WordBool; safecall;
    procedure Set_IsSplitterFixed(pVal: WordBool); safecall;
    function Get_Options: _CvsFilmstripResultsQueueOptions; safecall;
    procedure _Set_Options(const ppVal: _CvsFilmstripResultsQueueOptions); safecall;
    function Get_FreezeQueue: ICvsActionToggle; safecall;
    function Get_Settings: _CvsFilmstripResultsQueueSettings; safecall;
    property Enabled: WordBool read Get_Enabled write Set_Enabled;
    property AutoConnectString: WideString read Get_AutoConnectString write Set_AutoConnectString;
    property Action: WideString read Get_Action write Set_Action;
    property AutoReconnect: WordBool read Get_AutoReconnect write Set_AutoReconnect;
    property ConfirmOnlineTransitions: WordBool read Get_ConfirmOnlineTransitions write Set_ConfirmOnlineTransitions;
    property CurrentCell: CvsCellLocation read Get_CurrentCell;
    property CurrentCellExpression: WideString read Get_CurrentCellExpression write Set_CurrentCellExpression;
    property Connected: WordBool read Get_Connected;
    property Edit: _CvsInSightDisplayEdit read Get_Edit;
    property FtpPort: Integer read Get_FtpPort write Set_FtpPort;
    property GridOffset: CvsCellLocation read Get_GridOffset;
    property GridOpacity: Double read Get_GridOpacity write Set_GridOpacity;
    property GridScale: Double read Get_GridScale write Set_GridScale;
    property ImageOffset: PointF read Get_ImageOffset;
    property ImageOrientation: CvsImageOrientation read Get_ImageOrientation write Set_ImageOrientation;
    property ImageScale: Double read Get_ImageScale write Set_ImageScale;
    property ImageZoomMode: CvsDisplayZoom read Get_ImageZoomMode write Set_ImageZoomMode;
    property InSight: _CvsInSight read Get_InSight write _Set_InSight;
    property MaxUserAccess: CvsInSightSecurityAccess read Get_MaxUserAccess write Set_MaxUserAccess;
    property Recorder: _CvsInSightDisplayRecorder read Get_Recorder;
    property Results: _CvsResultSet read Get_Results write _Set_Results;
    property ScrollMode: CvsDisplayScrollMode read Get_ScrollMode write Set_ScrollMode;
    property SelectedRange: CvsCellRange read Get_SelectedRange;
    property ShiftPartialImage: WordBool read Get_ShiftPartialImage write Set_ShiftPartialImage;
    property ShowCustomView: WordBool read Get_ShowCustomView write Set_ShowCustomView;
    property ShowDependencyErrorsOnly: WordBool read Get_ShowDependencyErrorsOnly write Set_ShowDependencyErrorsOnly;
    property ShowGraphics: WordBool read Get_ShowGraphics write Set_ShowGraphics;
    property ShowGrid: WordBool read Get_ShowGrid write Set_ShowGrid;
    property ShowImage: WordBool read Get_ShowImage write Set_ShowImage;
    property ShowImageSaturation: WordBool read Get_ShowImageSaturation write Set_ShowImageSaturation;
    property ShowScrollBars: WordBool read Get_ShowScrollBars write Set_ShowScrollBars;
    property SnippetPath: WideString read Get_SnippetPath write Set_SnippetPath;
    property SoftOnline: WordBool read Get_SoftOnline write Set_SoftOnline;
    property State: CvsDisplayState read Get_State;
    property StatusInformation: WideString read Get_StatusInformation;
    property StatusInformationState: CvsStatusInformationState read Get_StatusInformationState;
    property FilmQueue: ICvsFilmQueue read Get_FilmQueue write _Set_FilmQueue;
    property StatusLevelStyle: CvsStatusLevelStyle read Get_StatusLevelStyle write Set_StatusLevelStyle;
    property ShowThumbnailImage: WordBool read Get_ShowThumbnailImage write Set_ShowThumbnailImage;
    property HeightScale: CvsFilmstripScale read Get_HeightScale write Set_HeightScale;
    property MaxThumbnailsDisplayed: Integer read Get_MaxThumbnailsDisplayed;
    property ThumbnailsDisplayed: Integer read Get_ThumbnailsDisplayed;
    property ShowSummary: WordBool read Get_ShowSummary write Set_ShowSummary;
    property SplitterHeight: Integer read Get_SplitterHeight write Set_SplitterHeight;
    property FilmstripHeight: Integer read Get_FilmstripHeight write Set_FilmstripHeight;
    property IsSplitterFixed: WordBool read Get_IsSplitterFixed write Set_IsSplitterFixed;
    property Options: _CvsFilmstripResultsQueueOptions read Get_Options write _Set_Options;
    property FreezeQueue: ICvsActionToggle read Get_FreezeQueue;
    property Settings: _CvsFilmstripResultsQueueSettings read Get_Settings;
  end;

// *********************************************************************//
// DispIntf:  ICvsSensorFilmstripDisplayDisp
// Flags:     (4544) Dual NonExtensible OleAutomation Dispatchable
// GUID:      {3DE3D08D-65B4-4978-8C9B-3E50E217BF98}
// *********************************************************************//
  ICvsSensorFilmstripDisplayDisp = dispinterface
    ['{3DE3D08D-65B4-4978-8C9B-3E50E217BF98}']
    property Enabled: WordBool dispid -514;
    procedure Refresh; dispid 1610743810;
    property AutoConnectString: WideString dispid 100;
    property Action: WideString dispid 125;
    property AutoReconnect: WordBool dispid 117;
    property ConfirmOnlineTransitions: WordBool dispid 118;
    property CurrentCell: {??CvsCellLocation}OleVariant readonly dispid 126;
    property CurrentCellExpression: WideString dispid 127;
    property Connected: WordBool readonly dispid 128;
    property Edit: _CvsInSightDisplayEdit readonly dispid 129;
    property FtpPort: Integer dispid 123;
    property GridOffset: {??CvsCellLocation}OleVariant readonly dispid 112;
    property GridOpacity: Double dispid 106;
    property GridScale: Double dispid 130;
    property ImageOffset: {??PointF}OleVariant readonly dispid 113;
    property ImageOrientation: CvsImageOrientation dispid 120;
    property ImageScale: Double dispid 107;
    property ImageZoomMode: CvsDisplayZoom dispid 108;
    property InSight: _CvsInSight dispid 131;
    property MaxUserAccess: CvsInSightSecurityAccess dispid 116;
    property Recorder: _CvsInSightDisplayRecorder readonly dispid 132;
    property Results: _CvsResultSet dispid 133;
    property ScrollMode: CvsDisplayScrollMode dispid 134;
    property SelectedRange: {??CvsCellRange}OleVariant readonly dispid 135;
    property ShiftPartialImage: WordBool dispid 119;
    property ShowCustomView: WordBool dispid 114;
    property ShowDependencyErrorsOnly: WordBool dispid 121;
    property ShowGraphics: WordBool dispid 102;
    property ShowGrid: WordBool dispid 103;
    property ShowImage: WordBool dispid 101;
    property ShowImageSaturation: WordBool dispid 122;
    property ShowScrollBars: WordBool dispid 104;
    property SnippetPath: WideString dispid 124;
    property SoftOnline: WordBool dispid 136;
    property State: CvsDisplayState readonly dispid 137;
    property StatusInformation: WideString readonly dispid 138;
    property StatusInformationState: CvsStatusInformationState readonly dispid 139;
    procedure Connect(const hostName: WideString; const userName: WideString; 
                      const password: WideString; forceConnect: WordBool); dispid 1610743871;
    procedure Disconnect; dispid 1610743872;
    procedure EditCellGraphic(row: Integer; column: Integer); dispid 1610743873;
    procedure EnsureCellIsVisible(row: Integer; column: Integer); dispid 1610743874;
    function GetPicture: IPictureDisp; dispid 1610743875;
    procedure SaveBitmap(const fileName: WideString); dispid 1610743876;
    function GetCellBounds(row: Integer; column: Integer; rows: Integer; columns: Integer): {??Rectangle}OleVariant; dispid 1610743877;
    procedure SetCurrentCell(row: Integer; column: Integer); dispid 1610743878;
    procedure SetGridOffset(row: Integer; column: Integer); dispid 1610743879;
    procedure SetImageScaleAndOffset(Scale: Double; x: Single; y: Single); dispid 1610743880;
    procedure SetSelectedRange(row: Integer; column: Integer; rows: Integer; columns: Integer); dispid 1610743881;
    procedure OpenPropertySheet(row: Integer; column: Integer; const expression: WideString); dispid 1610743882;
    procedure ClickSpreadsheetControl(row: Integer; column: Integer); dispid 1610743883;
    property FilmQueue: ICvsFilmQueue dispid 200;
    property StatusLevelStyle: CvsStatusLevelStyle dispid 201;
    property ShowThumbnailImage: WordBool dispid 202;
    property HeightScale: CvsFilmstripScale dispid 203;
    property MaxThumbnailsDisplayed: Integer readonly dispid 204;
    property ThumbnailsDisplayed: Integer readonly dispid 205;
    property ShowSummary: WordBool dispid 206;
    property SplitterHeight: Integer dispid 207;
    property FilmstripHeight: Integer dispid 208;
    property IsSplitterFixed: WordBool dispid 209;
    property Options: _CvsFilmstripResultsQueueOptions dispid 300;
    property FreezeQueue: ICvsActionToggle readonly dispid 301;
    property Settings: _CvsFilmstripResultsQueueSettings readonly dispid 302;
  end;

// *********************************************************************//
// DispIntf:  _ICvsSensorFilmstripDisplayEvents
// Flags:     (4096) Dispatchable
// GUID:      {A351A047-DDA2-4B3D-85A5-9774564801AC}
// *********************************************************************//
  _ICvsSensorFilmstripDisplayEvents = dispinterface
    ['{A351A047-DDA2-4B3D-85A5-9774564801AC}']
    procedure ConnectedChanged; dispid 1;
    procedure CurrentCellChanged; dispid 2;
    procedure GridOffsetChanged; dispid 3;
    procedure GridScaleChanged; dispid 4;
    procedure ImageOffsetChanged; dispid 5;
    procedure ImageScaleChanged; dispid 6;
    procedure InSightChanged; dispid 7;
    procedure ResultsChanged; dispid 8;
    procedure ScrollModeChanged; dispid 9;
    procedure SelectedRangeChanged; dispid 10;
    procedure StateChanged; dispid 11;
    procedure StatusInformationChanged; dispid 12;
    procedure CurrentCellExpressionChanged; dispid 14;
    procedure ConnectCompleted(ErrorNumber: Integer; const ErrorMessage: WideString); dispid 15;
    procedure GridOpacityChanged; dispid 16;
    procedure ImageOrientationChanged; dispid 17;
  end;

// *********************************************************************//
// Interface: ICvsPlaybackFilmstripDisplay
// Flags:     (4544) Dual NonExtensible OleAutomation Dispatchable
// GUID:      {F0C1E0CB-52DB-47EA-AC3C-27169D899B16}
// *********************************************************************//
  ICvsPlaybackFilmstripDisplay = interface(IDispatch)
    ['{F0C1E0CB-52DB-47EA-AC3C-27169D899B16}']
    procedure Set_Enabled(pbool: WordBool); safecall;
    function Get_Enabled: WordBool; safecall;
    procedure Refresh; safecall;
    function Get_AutoConnectString: WideString; safecall;
    procedure Set_AutoConnectString(const pVal: WideString); safecall;
    function Get_Action: WideString; safecall;
    procedure Set_Action(const pVal: WideString); safecall;
    function Get_AutoReconnect: WordBool; safecall;
    procedure Set_AutoReconnect(pVal: WordBool); safecall;
    function Get_ConfirmOnlineTransitions: WordBool; safecall;
    procedure Set_ConfirmOnlineTransitions(pVal: WordBool); safecall;
    function Get_CurrentCell: CvsCellLocation; safecall;
    function Get_CurrentCellExpression: WideString; safecall;
    procedure Set_CurrentCellExpression(const pVal: WideString); safecall;
    function Get_Connected: WordBool; safecall;
    function Get_Edit: _CvsInSightDisplayEdit; safecall;
    function Get_FtpPort: Integer; safecall;
    procedure Set_FtpPort(val: Integer); safecall;
    function Get_GridOffset: CvsCellLocation; safecall;
    function Get_GridOpacity: Double; safecall;
    procedure Set_GridOpacity(Scale: Double); safecall;
    function Get_GridScale: Double; safecall;
    procedure Set_GridScale(Scale: Double); safecall;
    function Get_ImageOffset: PointF; safecall;
    function Get_ImageOrientation: CvsImageOrientation; safecall;
    procedure Set_ImageOrientation(pVal: CvsImageOrientation); safecall;
    function Get_ImageScale: Double; safecall;
    procedure Set_ImageScale(Scale: Double); safecall;
    function Get_ImageZoomMode: CvsDisplayZoom; safecall;
    procedure Set_ImageZoomMode(pVal: CvsDisplayZoom); safecall;
    function Get_InSight: _CvsInSight; safecall;
    procedure _Set_InSight(const InSight: _CvsInSight); safecall;
    function Get_MaxUserAccess: CvsInSightSecurityAccess; safecall;
    procedure Set_MaxUserAccess(pVal: CvsInSightSecurityAccess); safecall;
    function Get_Recorder: _CvsInSightDisplayRecorder; safecall;
    function Get_Results: _CvsResultSet; safecall;
    procedure _Set_Results(const pVal: _CvsResultSet); safecall;
    function Get_ScrollMode: CvsDisplayScrollMode; safecall;
    procedure Set_ScrollMode(pVal: CvsDisplayScrollMode); safecall;
    function Get_SelectedRange: CvsCellRange; safecall;
    function Get_ShiftPartialImage: WordBool; safecall;
    procedure Set_ShiftPartialImage(Enabled: WordBool); safecall;
    function Get_ShowCustomView: WordBool; safecall;
    procedure Set_ShowCustomView(Enabled: WordBool); safecall;
    function Get_ShowDependencyErrorsOnly: WordBool; safecall;
    procedure Set_ShowDependencyErrorsOnly(Enabled: WordBool); safecall;
    function Get_ShowGraphics: WordBool; safecall;
    procedure Set_ShowGraphics(Enabled: WordBool); safecall;
    function Get_ShowGrid: WordBool; safecall;
    procedure Set_ShowGrid(pVal: WordBool); safecall;
    function Get_ShowImage: WordBool; safecall;
    procedure Set_ShowImage(Enabled: WordBool); safecall;
    function Get_ShowImageSaturation: WordBool; safecall;
    procedure Set_ShowImageSaturation(Enabled: WordBool); safecall;
    function Get_ShowScrollBars: WordBool; safecall;
    procedure Set_ShowScrollBars(Enabled: WordBool); safecall;
    function Get_SnippetPath: WideString; safecall;
    procedure Set_SnippetPath(const pVal: WideString); safecall;
    function Get_SoftOnline: WordBool; safecall;
    procedure Set_SoftOnline(pVal: WordBool); safecall;
    function Get_State: CvsDisplayState; safecall;
    function Get_StatusInformation: WideString; safecall;
    function Get_StatusInformationState: CvsStatusInformationState; safecall;
    procedure Connect(const hostName: WideString; const userName: WideString; 
                      const password: WideString; forceConnect: WordBool); safecall;
    procedure Disconnect; safecall;
    procedure EditCellGraphic(row: Integer; column: Integer); safecall;
    procedure EnsureCellIsVisible(row: Integer; column: Integer); safecall;
    function GetPicture: IPictureDisp; safecall;
    procedure SaveBitmap(const fileName: WideString); safecall;
    function GetCellBounds(row: Integer; column: Integer; rows: Integer; columns: Integer): Rectangle; safecall;
    procedure SetCurrentCell(row: Integer; column: Integer); safecall;
    procedure SetGridOffset(row: Integer; column: Integer); safecall;
    procedure SetImageScaleAndOffset(Scale: Double; x: Single; y: Single); safecall;
    procedure SetSelectedRange(row: Integer; column: Integer; rows: Integer; columns: Integer); safecall;
    procedure OpenPropertySheet(row: Integer; column: Integer; const expression: WideString); safecall;
    procedure ClickSpreadsheetControl(row: Integer; column: Integer); safecall;
    function Get_FilmQueue: ICvsFilmQueue; safecall;
    procedure _Set_FilmQueue(const pVal: ICvsFilmQueue); safecall;
    function Get_StatusLevelStyle: CvsStatusLevelStyle; safecall;
    procedure Set_StatusLevelStyle(pVal: CvsStatusLevelStyle); safecall;
    function Get_ShowThumbnailImage: WordBool; safecall;
    procedure Set_ShowThumbnailImage(pVal: WordBool); safecall;
    function Get_HeightScale: CvsFilmstripScale; safecall;
    procedure Set_HeightScale(pVal: CvsFilmstripScale); safecall;
    function Get_MaxThumbnailsDisplayed: Integer; safecall;
    function Get_ThumbnailsDisplayed: Integer; safecall;
    function Get_ShowSummary: WordBool; safecall;
    procedure Set_ShowSummary(pVal: WordBool); safecall;
    function Get_SplitterHeight: Integer; safecall;
    procedure Set_SplitterHeight(pVal: Integer); safecall;
    function Get_FilmstripHeight: Integer; safecall;
    procedure Set_FilmstripHeight(pVal: Integer); safecall;
    function Get_IsSplitterFixed: WordBool; safecall;
    procedure Set_IsSplitterFixed(pVal: WordBool); safecall;
    function Get_RecordActive: WordBool; safecall;
    procedure Set_RecordActive(pVal: WordBool); safecall;
    function Get_RecordFilename: HResult; safecall;
    function Get_RecordFilenameFormat: WideString; safecall;
    procedure Set_RecordFilenameFormat(const pVal: WideString); safecall;
    function Get_RecordFolder: WideString; safecall;
    procedure Set_RecordFolder(const pVal: WideString); safecall;
    function Get_RecordImageCount: Integer; safecall;
    function Get_RecordIndex: Integer; safecall;
    procedure Set_RecordIndex(pVal: Integer); safecall;
    function Get_RecordMax: Integer; safecall;
    procedure Set_RecordMax(pVal: Integer); safecall;
    function Get_RecordMode: CvsRecordMode; safecall;
    procedure Set_RecordMode(pVal: CvsRecordMode); safecall;
    function Get_RecordResolution: CvsImageResolution; safecall;
    procedure Set_RecordResolution(pVal: CvsImageResolution); safecall;
    function Get_RecordBitDepth: CvsColorBitDepth; safecall;
    procedure Set_RecordBitDepth(pVal: CvsColorBitDepth); safecall;
    function Get_PlaybackAllowed: WordBool; safecall;
    function Get_PlayActive: WordBool; safecall;
    procedure Set_PlayActive(pVal: WordBool); safecall;
    function Get_PlaybackContinuous: WordBool; safecall;
    procedure Set_PlaybackContinuous(pVal: WordBool); safecall;
    function Get_PlaybackDelay: Double; safecall;
    procedure Set_PlaybackDelay(pVal: Double); safecall;
    function Get_PlaybackFilename: WideString; safecall;
    procedure Set_PlaybackFilename(const pVal: WideString); safecall;
    function Get_PlaybackFolder: WideString; safecall;
    procedure Set_PlaybackFolder(const pVal: WideString); safecall;
    function Get_PlaybackImageCount: Integer; safecall;
    function Get_PlaybackIndex: Integer; safecall;
    procedure Set_PlaybackIndex(pVal: Integer); safecall;
    function GetPlaybackFiles: PUserType1; safecall;
    procedure PlayFirst; safecall;
    procedure PlayPrevious; safecall;
    procedure PlayNext; safecall;
    procedure PlayLast; safecall;
    property Enabled: WordBool read Get_Enabled write Set_Enabled;
    property AutoConnectString: WideString read Get_AutoConnectString write Set_AutoConnectString;
    property Action: WideString read Get_Action write Set_Action;
    property AutoReconnect: WordBool read Get_AutoReconnect write Set_AutoReconnect;
    property ConfirmOnlineTransitions: WordBool read Get_ConfirmOnlineTransitions write Set_ConfirmOnlineTransitions;
    property CurrentCell: CvsCellLocation read Get_CurrentCell;
    property CurrentCellExpression: WideString read Get_CurrentCellExpression write Set_CurrentCellExpression;
    property Connected: WordBool read Get_Connected;
    property Edit: _CvsInSightDisplayEdit read Get_Edit;
    property FtpPort: Integer read Get_FtpPort write Set_FtpPort;
    property GridOffset: CvsCellLocation read Get_GridOffset;
    property GridOpacity: Double read Get_GridOpacity write Set_GridOpacity;
    property GridScale: Double read Get_GridScale write Set_GridScale;
    property ImageOffset: PointF read Get_ImageOffset;
    property ImageOrientation: CvsImageOrientation read Get_ImageOrientation write Set_ImageOrientation;
    property ImageScale: Double read Get_ImageScale write Set_ImageScale;
    property ImageZoomMode: CvsDisplayZoom read Get_ImageZoomMode write Set_ImageZoomMode;
    property InSight: _CvsInSight read Get_InSight write _Set_InSight;
    property MaxUserAccess: CvsInSightSecurityAccess read Get_MaxUserAccess write Set_MaxUserAccess;
    property Recorder: _CvsInSightDisplayRecorder read Get_Recorder;
    property Results: _CvsResultSet read Get_Results write _Set_Results;
    property ScrollMode: CvsDisplayScrollMode read Get_ScrollMode write Set_ScrollMode;
    property SelectedRange: CvsCellRange read Get_SelectedRange;
    property ShiftPartialImage: WordBool read Get_ShiftPartialImage write Set_ShiftPartialImage;
    property ShowCustomView: WordBool read Get_ShowCustomView write Set_ShowCustomView;
    property ShowDependencyErrorsOnly: WordBool read Get_ShowDependencyErrorsOnly write Set_ShowDependencyErrorsOnly;
    property ShowGraphics: WordBool read Get_ShowGraphics write Set_ShowGraphics;
    property ShowGrid: WordBool read Get_ShowGrid write Set_ShowGrid;
    property ShowImage: WordBool read Get_ShowImage write Set_ShowImage;
    property ShowImageSaturation: WordBool read Get_ShowImageSaturation write Set_ShowImageSaturation;
    property ShowScrollBars: WordBool read Get_ShowScrollBars write Set_ShowScrollBars;
    property SnippetPath: WideString read Get_SnippetPath write Set_SnippetPath;
    property SoftOnline: WordBool read Get_SoftOnline write Set_SoftOnline;
    property State: CvsDisplayState read Get_State;
    property StatusInformation: WideString read Get_StatusInformation;
    property StatusInformationState: CvsStatusInformationState read Get_StatusInformationState;
    property FilmQueue: ICvsFilmQueue read Get_FilmQueue write _Set_FilmQueue;
    property StatusLevelStyle: CvsStatusLevelStyle read Get_StatusLevelStyle write Set_StatusLevelStyle;
    property ShowThumbnailImage: WordBool read Get_ShowThumbnailImage write Set_ShowThumbnailImage;
    property HeightScale: CvsFilmstripScale read Get_HeightScale write Set_HeightScale;
    property MaxThumbnailsDisplayed: Integer read Get_MaxThumbnailsDisplayed;
    property ThumbnailsDisplayed: Integer read Get_ThumbnailsDisplayed;
    property ShowSummary: WordBool read Get_ShowSummary write Set_ShowSummary;
    property SplitterHeight: Integer read Get_SplitterHeight write Set_SplitterHeight;
    property FilmstripHeight: Integer read Get_FilmstripHeight write Set_FilmstripHeight;
    property IsSplitterFixed: WordBool read Get_IsSplitterFixed write Set_IsSplitterFixed;
    property RecordActive: WordBool read Get_RecordActive write Set_RecordActive;
    property RecordFilename: HResult read Get_RecordFilename;
    property RecordFilenameFormat: WideString read Get_RecordFilenameFormat write Set_RecordFilenameFormat;
    property RecordFolder: WideString read Get_RecordFolder write Set_RecordFolder;
    property RecordImageCount: Integer read Get_RecordImageCount;
    property RecordIndex: Integer read Get_RecordIndex write Set_RecordIndex;
    property RecordMax: Integer read Get_RecordMax write Set_RecordMax;
    property RecordMode: CvsRecordMode read Get_RecordMode write Set_RecordMode;
    property RecordResolution: CvsImageResolution read Get_RecordResolution write Set_RecordResolution;
    property RecordBitDepth: CvsColorBitDepth read Get_RecordBitDepth write Set_RecordBitDepth;
    property PlaybackAllowed: WordBool read Get_PlaybackAllowed;
    property PlayActive: WordBool read Get_PlayActive write Set_PlayActive;
    property PlaybackContinuous: WordBool read Get_PlaybackContinuous write Set_PlaybackContinuous;
    property PlaybackDelay: Double read Get_PlaybackDelay write Set_PlaybackDelay;
    property PlaybackFilename: WideString read Get_PlaybackFilename write Set_PlaybackFilename;
    property PlaybackFolder: WideString read Get_PlaybackFolder write Set_PlaybackFolder;
    property PlaybackImageCount: Integer read Get_PlaybackImageCount;
    property PlaybackIndex: Integer read Get_PlaybackIndex write Set_PlaybackIndex;
  end;

// *********************************************************************//
// DispIntf:  ICvsPlaybackFilmstripDisplayDisp
// Flags:     (4544) Dual NonExtensible OleAutomation Dispatchable
// GUID:      {F0C1E0CB-52DB-47EA-AC3C-27169D899B16}
// *********************************************************************//
  ICvsPlaybackFilmstripDisplayDisp = dispinterface
    ['{F0C1E0CB-52DB-47EA-AC3C-27169D899B16}']
    property Enabled: WordBool dispid -514;
    procedure Refresh; dispid 1610743810;
    property AutoConnectString: WideString dispid 100;
    property Action: WideString dispid 125;
    property AutoReconnect: WordBool dispid 117;
    property ConfirmOnlineTransitions: WordBool dispid 118;
    property CurrentCell: {??CvsCellLocation}OleVariant readonly dispid 126;
    property CurrentCellExpression: WideString dispid 127;
    property Connected: WordBool readonly dispid 128;
    property Edit: _CvsInSightDisplayEdit readonly dispid 129;
    property FtpPort: Integer dispid 123;
    property GridOffset: {??CvsCellLocation}OleVariant readonly dispid 112;
    property GridOpacity: Double dispid 106;
    property GridScale: Double dispid 130;
    property ImageOffset: {??PointF}OleVariant readonly dispid 113;
    property ImageOrientation: CvsImageOrientation dispid 120;
    property ImageScale: Double dispid 107;
    property ImageZoomMode: CvsDisplayZoom dispid 108;
    property InSight: _CvsInSight dispid 131;
    property MaxUserAccess: CvsInSightSecurityAccess dispid 116;
    property Recorder: _CvsInSightDisplayRecorder readonly dispid 132;
    property Results: _CvsResultSet dispid 133;
    property ScrollMode: CvsDisplayScrollMode dispid 134;
    property SelectedRange: {??CvsCellRange}OleVariant readonly dispid 135;
    property ShiftPartialImage: WordBool dispid 119;
    property ShowCustomView: WordBool dispid 114;
    property ShowDependencyErrorsOnly: WordBool dispid 121;
    property ShowGraphics: WordBool dispid 102;
    property ShowGrid: WordBool dispid 103;
    property ShowImage: WordBool dispid 101;
    property ShowImageSaturation: WordBool dispid 122;
    property ShowScrollBars: WordBool dispid 104;
    property SnippetPath: WideString dispid 124;
    property SoftOnline: WordBool dispid 136;
    property State: CvsDisplayState readonly dispid 137;
    property StatusInformation: WideString readonly dispid 138;
    property StatusInformationState: CvsStatusInformationState readonly dispid 139;
    procedure Connect(const hostName: WideString; const userName: WideString; 
                      const password: WideString; forceConnect: WordBool); dispid 1610743871;
    procedure Disconnect; dispid 1610743872;
    procedure EditCellGraphic(row: Integer; column: Integer); dispid 1610743873;
    procedure EnsureCellIsVisible(row: Integer; column: Integer); dispid 1610743874;
    function GetPicture: IPictureDisp; dispid 1610743875;
    procedure SaveBitmap(const fileName: WideString); dispid 1610743876;
    function GetCellBounds(row: Integer; column: Integer; rows: Integer; columns: Integer): {??Rectangle}OleVariant; dispid 1610743877;
    procedure SetCurrentCell(row: Integer; column: Integer); dispid 1610743878;
    procedure SetGridOffset(row: Integer; column: Integer); dispid 1610743879;
    procedure SetImageScaleAndOffset(Scale: Double; x: Single; y: Single); dispid 1610743880;
    procedure SetSelectedRange(row: Integer; column: Integer; rows: Integer; columns: Integer); dispid 1610743881;
    procedure OpenPropertySheet(row: Integer; column: Integer; const expression: WideString); dispid 1610743882;
    procedure ClickSpreadsheetControl(row: Integer; column: Integer); dispid 1610743883;
    property FilmQueue: ICvsFilmQueue dispid 200;
    property StatusLevelStyle: CvsStatusLevelStyle dispid 201;
    property ShowThumbnailImage: WordBool dispid 202;
    property HeightScale: CvsFilmstripScale dispid 203;
    property MaxThumbnailsDisplayed: Integer readonly dispid 204;
    property ThumbnailsDisplayed: Integer readonly dispid 205;
    property ShowSummary: WordBool dispid 206;
    property SplitterHeight: Integer dispid 207;
    property FilmstripHeight: Integer dispid 208;
    property IsSplitterFixed: WordBool dispid 209;
    property RecordActive: WordBool dispid 400;
    property RecordFilename: HResult readonly dispid 401;
    property RecordFilenameFormat: WideString dispid 402;
    property RecordFolder: WideString dispid 403;
    property RecordImageCount: Integer readonly dispid 404;
    property RecordIndex: Integer dispid 405;
    property RecordMax: Integer dispid 406;
    property RecordMode: CvsRecordMode dispid 407;
    property RecordResolution: CvsImageResolution dispid 408;
    property RecordBitDepth: CvsColorBitDepth dispid 409;
    property PlaybackAllowed: WordBool readonly dispid 410;
    property PlayActive: WordBool dispid 411;
    property PlaybackContinuous: WordBool dispid 412;
    property PlaybackDelay: Double dispid 413;
    property PlaybackFilename: WideString dispid 414;
    property PlaybackFolder: WideString dispid 415;
    property PlaybackImageCount: Integer readonly dispid 416;
    property PlaybackIndex: Integer dispid 417;
    function GetPlaybackFiles: {??PUserType1}OleVariant; dispid 1610743934;
    procedure PlayFirst; dispid 1610743935;
    procedure PlayPrevious; dispid 1610743936;
    procedure PlayNext; dispid 1610743937;
    procedure PlayLast; dispid 1610743938;
  end;

// *********************************************************************//
// DispIntf:  _ICvsPlaybackFilmstripDisplayEvents
// Flags:     (4096) Dispatchable
// GUID:      {0A9AF383-19B1-489A-985B-4642B0E7DB89}
// *********************************************************************//
  _ICvsPlaybackFilmstripDisplayEvents = dispinterface
    ['{0A9AF383-19B1-489A-985B-4642B0E7DB89}']
    procedure ConnectedChanged; dispid 1;
    procedure CurrentCellChanged; dispid 2;
    procedure GridOffsetChanged; dispid 3;
    procedure GridScaleChanged; dispid 4;
    procedure ImageOffsetChanged; dispid 5;
    procedure ImageScaleChanged; dispid 6;
    procedure InSightChanged; dispid 7;
    procedure ResultsChanged; dispid 8;
    procedure ScrollModeChanged; dispid 9;
    procedure SelectedRangeChanged; dispid 10;
    procedure StateChanged; dispid 11;
    procedure StatusInformationChanged; dispid 12;
    procedure CurrentCellExpressionChanged; dispid 14;
    procedure ConnectCompleted(ErrorNumber: Integer; const ErrorMessage: WideString); dispid 15;
    procedure GridOpacityChanged; dispid 16;
    procedure ImageOrientationChanged; dispid 17;
  end;


// *********************************************************************//
// OLE Control Proxy class declaration
// Control Name     : TCvsInSightDisplay
// Help String      : Cognex  In-Sight Display Control
// Default Interface: ICvsInSightDisplay
// Def. Intf. DISP? : No
// Event   Interface: _ICvsInSightDisplayEvents
// TypeFlags        : (2) CanCreate
// *********************************************************************//
  TCvsInSightDisplayConnectCompleted = procedure(ASender: TObject; ErrorNumber: Integer; 
                                                                   const ErrorMessage: WideString) of object;

  TCvsInSightDisplay = class(TOleControl)
  private
    FOnConnectedChanged: TNotifyEvent;
    FOnCurrentCellChanged: TNotifyEvent;
    FOnGridOffsetChanged: TNotifyEvent;
    FOnGridScaleChanged: TNotifyEvent;
    FOnImageOffsetChanged: TNotifyEvent;
    FOnImageScaleChanged: TNotifyEvent;
    FOnInSightChanged: TNotifyEvent;
    FOnResultsChanged: TNotifyEvent;
    FOnScrollModeChanged: TNotifyEvent;
    FOnSelectedRangeChanged: TNotifyEvent;
    FOnStateChanged: TNotifyEvent;
    FOnStatusInformationChanged: TNotifyEvent;
    FOnCurrentCellExpressionChanged: TNotifyEvent;
    FOnConnectCompleted: TCvsInSightDisplayConnectCompleted;
    FOnGridOpacityChanged: TNotifyEvent;
    FOnImageOrientationChanged: TNotifyEvent;
    FIntf: ICvsInSightDisplay;
    function  GetControlInterface: ICvsInSightDisplay;
  protected
    procedure CreateControl;
    procedure InitControlData; override;
    function Get_CurrentCell: CvsCellLocation;
    function Get_Edit: _CvsInSightDisplayEdit;
    function Get_GridOffset: CvsCellLocation;
    function Get_ImageOffset: PointF;
    function Get_InSight: _CvsInSight;
    procedure _Set_InSight(const InSight: _CvsInSight);
    function Get_Recorder: _CvsInSightDisplayRecorder;
    function Get_Results: _CvsResultSet;
    procedure _Set_Results(const pVal: _CvsResultSet);
    function Get_SelectedRange: CvsCellRange;
  public
    procedure Refresh;
    procedure Connect(const hostName: WideString; const userName: WideString; 
                      const password: WideString; forceConnect: WordBool);
    procedure Disconnect;
    procedure EditCellGraphic(row: Integer; column: Integer);
    procedure EnsureCellIsVisible(row: Integer; column: Integer);
    function GetPicture: IPictureDisp;
    procedure SaveBitmap(const fileName: WideString);
    function GetCellBounds(row: Integer; column: Integer; rows: Integer; columns: Integer): Rectangle;
    procedure SetCurrentCell(row: Integer; column: Integer);
    procedure SetGridOffset(row: Integer; column: Integer);
    procedure SetImageScaleAndOffset(Scale: Double; x: Single; y: Single);
    procedure SetSelectedRange(row: Integer; column: Integer; rows: Integer; columns: Integer);
    procedure OpenPropertySheet(row: Integer; column: Integer; const expression: WideString);
    procedure ClickSpreadsheetControl(row: Integer; column: Integer);
    property  ControlInterface: ICvsInSightDisplay read GetControlInterface;
    property  DefaultInterface: ICvsInSightDisplay read GetControlInterface;
    property CurrentCell: CvsCellLocation read Get_CurrentCell;
    property Connected: WordBool index 128 read GetWordBoolProp;
    property Edit: _CvsInSightDisplayEdit read Get_Edit;
    property GridOffset: CvsCellLocation read Get_GridOffset;
    property ImageOffset: PointF read Get_ImageOffset;
    property InSight: _CvsInSight read Get_InSight write _Set_InSight;
    property Recorder: _CvsInSightDisplayRecorder read Get_Recorder;
    property Results: _CvsResultSet read Get_Results write _Set_Results;
    property SelectedRange: CvsCellRange read Get_SelectedRange;
    property State: TOleEnum index 137 read GetTOleEnumProp;
    property StatusInformation: WideString index 138 read GetWideStringProp;
    property StatusInformationState: TOleEnum index 139 read GetTOleEnumProp;
  published
    property Anchors;
    property  TabStop;
    property  Align;
    property  DragCursor;
    property  DragMode;
    property  ParentShowHint;
    property  PopupMenu;
    property  ShowHint;
    property  TabOrder;
    property  Visible;
    property  OnDragDrop;
    property  OnDragOver;
    property  OnEndDrag;
    property  OnEnter;
    property  OnExit;
    property  OnStartDrag;
    property Enabled: WordBool index -514 read GetWordBoolProp write SetWordBoolProp stored False;
    property AutoConnectString: WideString index 100 read GetWideStringProp write SetWideStringProp stored False;
    property Action: WideString index 125 read GetWideStringProp write SetWideStringProp stored False;
    property AutoReconnect: WordBool index 117 read GetWordBoolProp write SetWordBoolProp stored False;
    property ConfirmOnlineTransitions: WordBool index 118 read GetWordBoolProp write SetWordBoolProp stored False;
    property CurrentCellExpression: WideString index 127 read GetWideStringProp write SetWideStringProp stored False;
    property FtpPort: Integer index 123 read GetIntegerProp write SetIntegerProp stored False;
    property GridOpacity: Double index 106 read GetDoubleProp write SetDoubleProp stored False;
    property GridScale: Double index 130 read GetDoubleProp write SetDoubleProp stored False;
    property ImageOrientation: TOleEnum index 120 read GetTOleEnumProp write SetTOleEnumProp stored False;
    property ImageScale: Double index 107 read GetDoubleProp write SetDoubleProp stored False;
    property ImageZoomMode: TOleEnum index 108 read GetTOleEnumProp write SetTOleEnumProp stored False;
    property MaxUserAccess: TOleEnum index 116 read GetTOleEnumProp write SetTOleEnumProp stored False;
    property ScrollMode: TOleEnum index 134 read GetTOleEnumProp write SetTOleEnumProp stored False;
    property ShiftPartialImage: WordBool index 119 read GetWordBoolProp write SetWordBoolProp stored False;
    property ShowCustomView: WordBool index 114 read GetWordBoolProp write SetWordBoolProp stored False;
    property ShowDependencyErrorsOnly: WordBool index 121 read GetWordBoolProp write SetWordBoolProp stored False;
    property ShowGraphics: WordBool index 102 read GetWordBoolProp write SetWordBoolProp stored False;
    property ShowGrid: WordBool index 103 read GetWordBoolProp write SetWordBoolProp stored False;
    property ShowImage: WordBool index 101 read GetWordBoolProp write SetWordBoolProp stored False;
    property ShowImageSaturation: WordBool index 122 read GetWordBoolProp write SetWordBoolProp stored False;
    property ShowScrollBars: WordBool index 104 read GetWordBoolProp write SetWordBoolProp stored False;
    property SnippetPath: WideString index 124 read GetWideStringProp write SetWideStringProp stored False;
    property SoftOnline: WordBool index 136 read GetWordBoolProp write SetWordBoolProp stored False;
    property OnConnectedChanged: TNotifyEvent read FOnConnectedChanged write FOnConnectedChanged;
    property OnCurrentCellChanged: TNotifyEvent read FOnCurrentCellChanged write FOnCurrentCellChanged;
    property OnGridOffsetChanged: TNotifyEvent read FOnGridOffsetChanged write FOnGridOffsetChanged;
    property OnGridScaleChanged: TNotifyEvent read FOnGridScaleChanged write FOnGridScaleChanged;
    property OnImageOffsetChanged: TNotifyEvent read FOnImageOffsetChanged write FOnImageOffsetChanged;
    property OnImageScaleChanged: TNotifyEvent read FOnImageScaleChanged write FOnImageScaleChanged;
    property OnInSightChanged: TNotifyEvent read FOnInSightChanged write FOnInSightChanged;
    property OnResultsChanged: TNotifyEvent read FOnResultsChanged write FOnResultsChanged;
    property OnScrollModeChanged: TNotifyEvent read FOnScrollModeChanged write FOnScrollModeChanged;
    property OnSelectedRangeChanged: TNotifyEvent read FOnSelectedRangeChanged write FOnSelectedRangeChanged;
    property OnStateChanged: TNotifyEvent read FOnStateChanged write FOnStateChanged;
    property OnStatusInformationChanged: TNotifyEvent read FOnStatusInformationChanged write FOnStatusInformationChanged;
    property OnCurrentCellExpressionChanged: TNotifyEvent read FOnCurrentCellExpressionChanged write FOnCurrentCellExpressionChanged;
    property OnConnectCompleted: TCvsInSightDisplayConnectCompleted read FOnConnectCompleted write FOnConnectCompleted;
    property OnGridOpacityChanged: TNotifyEvent read FOnGridOpacityChanged write FOnGridOpacityChanged;
    property OnImageOrientationChanged: TNotifyEvent read FOnImageOrientationChanged write FOnImageOrientationChanged;
  end;


// *********************************************************************//
// OLE Control Proxy class declaration
// Control Name     : TCvsFilmstrip
// Help String      : Cognex  In-Sight Filmstrip Control
// Default Interface: ICvsFilmstrip
// Def. Intf. DISP? : No
// Event   Interface: _ICvsFilmstripEvents
// TypeFlags        : (2) CanCreate
// *********************************************************************//
  TCvsFilmstrip = class(TOleControl)
  private
    FIntf: ICvsFilmstrip;
    function  GetControlInterface: ICvsFilmstrip;
  protected
    procedure CreateControl;
    procedure InitControlData; override;
    function Get_FilmQueue: ICvsFilmQueue;
    procedure _Set_FilmQueue(const pVal: ICvsFilmQueue);
  public
    procedure Refresh;
    property  ControlInterface: ICvsFilmstrip read GetControlInterface;
    property  DefaultInterface: ICvsFilmstrip read GetControlInterface;
    property FilmQueue: ICvsFilmQueue read Get_FilmQueue write _Set_FilmQueue;
    property MaxThumbnailsDisplayed: Integer index 204 read GetIntegerProp;
    property ThumbnailsDisplayed: Integer index 205 read GetIntegerProp;
  published
    property Anchors;
    property  TabStop;
    property  Align;
    property  DragCursor;
    property  DragMode;
    property  ParentShowHint;
    property  PopupMenu;
    property  ShowHint;
    property  TabOrder;
    property  Visible;
    property  OnDragDrop;
    property  OnDragOver;
    property  OnEndDrag;
    property  OnEnter;
    property  OnExit;
    property  OnStartDrag;
    property Enabled: WordBool index -514 read GetWordBoolProp write SetWordBoolProp stored False;
    property StatusLevelStyle: TOleEnum index 201 read GetTOleEnumProp write SetTOleEnumProp stored False;
    property ShowThumbnailImage: WordBool index 202 read GetWordBoolProp write SetWordBoolProp stored False;
    property HeightScale: TOleEnum index 203 read GetTOleEnumProp write SetTOleEnumProp stored False;
    property ShowSummary: WordBool index 206 read GetWordBoolProp write SetWordBoolProp stored False;
  end;


// *********************************************************************//
// OLE Control Proxy class declaration
// Control Name     : TCvsSensorFilmstripDisplay
// Help String      : Cognex  In-Sight Sensor Filmstrip-Display Control
// Default Interface: ICvsSensorFilmstripDisplay
// Def. Intf. DISP? : No
// Event   Interface: _ICvsSensorFilmstripDisplayEvents
// TypeFlags        : (2) CanCreate
// *********************************************************************//
  TCvsSensorFilmstripDisplayConnectCompleted = procedure(ASender: TObject; ErrorNumber: Integer; 
                                                                           const ErrorMessage: WideString) of object;

  TCvsSensorFilmstripDisplay = class(TOleControl)
  private
    FOnConnectedChanged: TNotifyEvent;
    FOnCurrentCellChanged: TNotifyEvent;
    FOnGridOffsetChanged: TNotifyEvent;
    FOnGridScaleChanged: TNotifyEvent;
    FOnImageOffsetChanged: TNotifyEvent;
    FOnImageScaleChanged: TNotifyEvent;
    FOnInSightChanged: TNotifyEvent;
    FOnResultsChanged: TNotifyEvent;
    FOnScrollModeChanged: TNotifyEvent;
    FOnSelectedRangeChanged: TNotifyEvent;
    FOnStateChanged: TNotifyEvent;
    FOnStatusInformationChanged: TNotifyEvent;
    FOnCurrentCellExpressionChanged: TNotifyEvent;
    FOnConnectCompleted: TCvsSensorFilmstripDisplayConnectCompleted;
    FOnGridOpacityChanged: TNotifyEvent;
    FOnImageOrientationChanged: TNotifyEvent;
    FIntf: ICvsSensorFilmstripDisplay;
    function  GetControlInterface: ICvsSensorFilmstripDisplay;
  protected
    procedure CreateControl;
    procedure InitControlData; override;
    function Get_CurrentCell: CvsCellLocation;
    function Get_Edit: _CvsInSightDisplayEdit;
    function Get_GridOffset: CvsCellLocation;
    function Get_ImageOffset: PointF;
    function Get_InSight: _CvsInSight;
    procedure _Set_InSight(const InSight: _CvsInSight);
    function Get_Recorder: _CvsInSightDisplayRecorder;
    function Get_Results: _CvsResultSet;
    procedure _Set_Results(const pVal: _CvsResultSet);
    function Get_SelectedRange: CvsCellRange;
    function Get_FilmQueue: ICvsFilmQueue;
    procedure _Set_FilmQueue(const pVal: ICvsFilmQueue);
    function Get_Options: _CvsFilmstripResultsQueueOptions;
    procedure _Set_Options(const ppVal: _CvsFilmstripResultsQueueOptions);
    function Get_FreezeQueue: ICvsActionToggle;
    function Get_Settings: _CvsFilmstripResultsQueueSettings;
  public
    procedure Refresh;
    procedure Connect(const hostName: WideString; const userName: WideString; 
                      const password: WideString; forceConnect: WordBool);
    procedure Disconnect;
    procedure EditCellGraphic(row: Integer; column: Integer);
    procedure EnsureCellIsVisible(row: Integer; column: Integer);
    function GetPicture: IPictureDisp;
    procedure SaveBitmap(const fileName: WideString);
    function GetCellBounds(row: Integer; column: Integer; rows: Integer; columns: Integer): Rectangle;
    procedure SetCurrentCell(row: Integer; column: Integer);
    procedure SetGridOffset(row: Integer; column: Integer);
    procedure SetImageScaleAndOffset(Scale: Double; x: Single; y: Single);
    procedure SetSelectedRange(row: Integer; column: Integer; rows: Integer; columns: Integer);
    procedure OpenPropertySheet(row: Integer; column: Integer; const expression: WideString);
    procedure ClickSpreadsheetControl(row: Integer; column: Integer);
    property  ControlInterface: ICvsSensorFilmstripDisplay read GetControlInterface;
    property  DefaultInterface: ICvsSensorFilmstripDisplay read GetControlInterface;
    property CurrentCell: CvsCellLocation read Get_CurrentCell;
    property Connected: WordBool index 128 read GetWordBoolProp;
    property Edit: _CvsInSightDisplayEdit read Get_Edit;
    property GridOffset: CvsCellLocation read Get_GridOffset;
    property ImageOffset: PointF read Get_ImageOffset;
    property InSight: _CvsInSight read Get_InSight write _Set_InSight;
    property Recorder: _CvsInSightDisplayRecorder read Get_Recorder;
    property Results: _CvsResultSet read Get_Results write _Set_Results;
    property SelectedRange: CvsCellRange read Get_SelectedRange;
    property State: TOleEnum index 137 read GetTOleEnumProp;
    property StatusInformation: WideString index 138 read GetWideStringProp;
    property StatusInformationState: TOleEnum index 139 read GetTOleEnumProp;
    property FilmQueue: ICvsFilmQueue read Get_FilmQueue write _Set_FilmQueue;
    property MaxThumbnailsDisplayed: Integer index 204 read GetIntegerProp;
    property ThumbnailsDisplayed: Integer index 205 read GetIntegerProp;
    property Options: _CvsFilmstripResultsQueueOptions read Get_Options write _Set_Options;
    property FreezeQueue: ICvsActionToggle read Get_FreezeQueue;
    property Settings: _CvsFilmstripResultsQueueSettings read Get_Settings;
  published
    property Anchors;
    property  TabStop;
    property  Align;
    property  DragCursor;
    property  DragMode;
    property  ParentShowHint;
    property  PopupMenu;
    property  ShowHint;
    property  TabOrder;
    property  Visible;
    property  OnDragDrop;
    property  OnDragOver;
    property  OnEndDrag;
    property  OnEnter;
    property  OnExit;
    property  OnStartDrag;
    property Enabled: WordBool index -514 read GetWordBoolProp write SetWordBoolProp stored False;
    property AutoConnectString: WideString index 100 read GetWideStringProp write SetWideStringProp stored False;
    property Action: WideString index 125 read GetWideStringProp write SetWideStringProp stored False;
    property AutoReconnect: WordBool index 117 read GetWordBoolProp write SetWordBoolProp stored False;
    property ConfirmOnlineTransitions: WordBool index 118 read GetWordBoolProp write SetWordBoolProp stored False;
    property CurrentCellExpression: WideString index 127 read GetWideStringProp write SetWideStringProp stored False;
    property FtpPort: Integer index 123 read GetIntegerProp write SetIntegerProp stored False;
    property GridOpacity: Double index 106 read GetDoubleProp write SetDoubleProp stored False;
    property GridScale: Double index 130 read GetDoubleProp write SetDoubleProp stored False;
    property ImageOrientation: TOleEnum index 120 read GetTOleEnumProp write SetTOleEnumProp stored False;
    property ImageScale: Double index 107 read GetDoubleProp write SetDoubleProp stored False;
    property ImageZoomMode: TOleEnum index 108 read GetTOleEnumProp write SetTOleEnumProp stored False;
    property MaxUserAccess: TOleEnum index 116 read GetTOleEnumProp write SetTOleEnumProp stored False;
    property ScrollMode: TOleEnum index 134 read GetTOleEnumProp write SetTOleEnumProp stored False;
    property ShiftPartialImage: WordBool index 119 read GetWordBoolProp write SetWordBoolProp stored False;
    property ShowCustomView: WordBool index 114 read GetWordBoolProp write SetWordBoolProp stored False;
    property ShowDependencyErrorsOnly: WordBool index 121 read GetWordBoolProp write SetWordBoolProp stored False;
    property ShowGraphics: WordBool index 102 read GetWordBoolProp write SetWordBoolProp stored False;
    property ShowGrid: WordBool index 103 read GetWordBoolProp write SetWordBoolProp stored False;
    property ShowImage: WordBool index 101 read GetWordBoolProp write SetWordBoolProp stored False;
    property ShowImageSaturation: WordBool index 122 read GetWordBoolProp write SetWordBoolProp stored False;
    property ShowScrollBars: WordBool index 104 read GetWordBoolProp write SetWordBoolProp stored False;
    property SnippetPath: WideString index 124 read GetWideStringProp write SetWideStringProp stored False;
    property SoftOnline: WordBool index 136 read GetWordBoolProp write SetWordBoolProp stored False;
    property StatusLevelStyle: TOleEnum index 201 read GetTOleEnumProp write SetTOleEnumProp stored False;
    property ShowThumbnailImage: WordBool index 202 read GetWordBoolProp write SetWordBoolProp stored False;
    property HeightScale: TOleEnum index 203 read GetTOleEnumProp write SetTOleEnumProp stored False;
    property ShowSummary: WordBool index 206 read GetWordBoolProp write SetWordBoolProp stored False;
    property SplitterHeight: Integer index 207 read GetIntegerProp write SetIntegerProp stored False;
    property FilmstripHeight: Integer index 208 read GetIntegerProp write SetIntegerProp stored False;
    property IsSplitterFixed: WordBool index 209 read GetWordBoolProp write SetWordBoolProp stored False;
    property OnConnectedChanged: TNotifyEvent read FOnConnectedChanged write FOnConnectedChanged;
    property OnCurrentCellChanged: TNotifyEvent read FOnCurrentCellChanged write FOnCurrentCellChanged;
    property OnGridOffsetChanged: TNotifyEvent read FOnGridOffsetChanged write FOnGridOffsetChanged;
    property OnGridScaleChanged: TNotifyEvent read FOnGridScaleChanged write FOnGridScaleChanged;
    property OnImageOffsetChanged: TNotifyEvent read FOnImageOffsetChanged write FOnImageOffsetChanged;
    property OnImageScaleChanged: TNotifyEvent read FOnImageScaleChanged write FOnImageScaleChanged;
    property OnInSightChanged: TNotifyEvent read FOnInSightChanged write FOnInSightChanged;
    property OnResultsChanged: TNotifyEvent read FOnResultsChanged write FOnResultsChanged;
    property OnScrollModeChanged: TNotifyEvent read FOnScrollModeChanged write FOnScrollModeChanged;
    property OnSelectedRangeChanged: TNotifyEvent read FOnSelectedRangeChanged write FOnSelectedRangeChanged;
    property OnStateChanged: TNotifyEvent read FOnStateChanged write FOnStateChanged;
    property OnStatusInformationChanged: TNotifyEvent read FOnStatusInformationChanged write FOnStatusInformationChanged;
    property OnCurrentCellExpressionChanged: TNotifyEvent read FOnCurrentCellExpressionChanged write FOnCurrentCellExpressionChanged;
    property OnConnectCompleted: TCvsSensorFilmstripDisplayConnectCompleted read FOnConnectCompleted write FOnConnectCompleted;
    property OnGridOpacityChanged: TNotifyEvent read FOnGridOpacityChanged write FOnGridOpacityChanged;
    property OnImageOrientationChanged: TNotifyEvent read FOnImageOrientationChanged write FOnImageOrientationChanged;
  end;


// *********************************************************************//
// OLE Control Proxy class declaration
// Control Name     : TCvsPlaybackFilmstripDisplay
// Help String      : Cognex  In-Sight Record/Playback Filmstrip-Display Control
// Default Interface: ICvsPlaybackFilmstripDisplay
// Def. Intf. DISP? : No
// Event   Interface: _ICvsPlaybackFilmstripDisplayEvents
// TypeFlags        : (2) CanCreate
// *********************************************************************//
  TCvsPlaybackFilmstripDisplayConnectCompleted = procedure(ASender: TObject; ErrorNumber: Integer; 
                                                                             const ErrorMessage: WideString) of object;

  TCvsPlaybackFilmstripDisplay = class(TOleControl)
  private
    FOnConnectedChanged: TNotifyEvent;
    FOnCurrentCellChanged: TNotifyEvent;
    FOnGridOffsetChanged: TNotifyEvent;
    FOnGridScaleChanged: TNotifyEvent;
    FOnImageOffsetChanged: TNotifyEvent;
    FOnImageScaleChanged: TNotifyEvent;
    FOnInSightChanged: TNotifyEvent;
    FOnResultsChanged: TNotifyEvent;
    FOnScrollModeChanged: TNotifyEvent;
    FOnSelectedRangeChanged: TNotifyEvent;
    FOnStateChanged: TNotifyEvent;
    FOnStatusInformationChanged: TNotifyEvent;
    FOnCurrentCellExpressionChanged: TNotifyEvent;
    FOnConnectCompleted: TCvsPlaybackFilmstripDisplayConnectCompleted;
    FOnGridOpacityChanged: TNotifyEvent;
    FOnImageOrientationChanged: TNotifyEvent;
    FIntf: ICvsPlaybackFilmstripDisplay;
    function  GetControlInterface: ICvsPlaybackFilmstripDisplay;
  protected
    procedure CreateControl;
    procedure InitControlData; override;
    function Get_CurrentCell: CvsCellLocation;
    function Get_Edit: _CvsInSightDisplayEdit;
    function Get_GridOffset: CvsCellLocation;
    function Get_ImageOffset: PointF;
    function Get_InSight: _CvsInSight;
    procedure _Set_InSight(const InSight: _CvsInSight);
    function Get_Recorder: _CvsInSightDisplayRecorder;
    function Get_Results: _CvsResultSet;
    procedure _Set_Results(const pVal: _CvsResultSet);
    function Get_SelectedRange: CvsCellRange;
    function Get_FilmQueue: ICvsFilmQueue;
    procedure _Set_FilmQueue(const pVal: ICvsFilmQueue);
  public
    procedure Refresh;
    procedure Connect(const hostName: WideString; const userName: WideString; 
                      const password: WideString; forceConnect: WordBool);
    procedure Disconnect;
    procedure EditCellGraphic(row: Integer; column: Integer);
    procedure EnsureCellIsVisible(row: Integer; column: Integer);
    function GetPicture: IPictureDisp;
    procedure SaveBitmap(const fileName: WideString);
    function GetCellBounds(row: Integer; column: Integer; rows: Integer; columns: Integer): Rectangle;
    procedure SetCurrentCell(row: Integer; column: Integer);
    procedure SetGridOffset(row: Integer; column: Integer);
    procedure SetImageScaleAndOffset(Scale: Double; x: Single; y: Single);
    procedure SetSelectedRange(row: Integer; column: Integer; rows: Integer; columns: Integer);
    procedure OpenPropertySheet(row: Integer; column: Integer; const expression: WideString);
    procedure ClickSpreadsheetControl(row: Integer; column: Integer);
    function GetPlaybackFiles: PUserType1;
    procedure PlayFirst;
    procedure PlayPrevious;
    procedure PlayNext;
    procedure PlayLast;
    property  ControlInterface: ICvsPlaybackFilmstripDisplay read GetControlInterface;
    property  DefaultInterface: ICvsPlaybackFilmstripDisplay read GetControlInterface;
    property CurrentCell: CvsCellLocation read Get_CurrentCell;
    property Connected: WordBool index 128 read GetWordBoolProp;
    property Edit: _CvsInSightDisplayEdit read Get_Edit;
    property GridOffset: CvsCellLocation read Get_GridOffset;
    property ImageOffset: PointF read Get_ImageOffset;
    property InSight: _CvsInSight read Get_InSight write _Set_InSight;
    property Recorder: _CvsInSightDisplayRecorder read Get_Recorder;
    property Results: _CvsResultSet read Get_Results write _Set_Results;
    property SelectedRange: CvsCellRange read Get_SelectedRange;
    property State: TOleEnum index 137 read GetTOleEnumProp;
    property StatusInformation: WideString index 138 read GetWideStringProp;
    property StatusInformationState: TOleEnum index 139 read GetTOleEnumProp;
    property FilmQueue: ICvsFilmQueue read Get_FilmQueue write _Set_FilmQueue;
    property MaxThumbnailsDisplayed: Integer index 204 read GetIntegerProp;
    property ThumbnailsDisplayed: Integer index 205 read GetIntegerProp;
    property RecordFilename: Integer index 401 read GetIntegerProp;
    property RecordImageCount: Integer index 404 read GetIntegerProp;
    property PlaybackAllowed: WordBool index 410 read GetWordBoolProp;
    property PlaybackImageCount: Integer index 416 read GetIntegerProp;
  published
    property Anchors;
    property  TabStop;
    property  Align;
    property  DragCursor;
    property  DragMode;
    property  ParentShowHint;
    property  PopupMenu;
    property  ShowHint;
    property  TabOrder;
    property  Visible;
    property  OnDragDrop;
    property  OnDragOver;
    property  OnEndDrag;
    property  OnEnter;
    property  OnExit;
    property  OnStartDrag;
    property Enabled: WordBool index -514 read GetWordBoolProp write SetWordBoolProp stored False;
    property AutoConnectString: WideString index 100 read GetWideStringProp write SetWideStringProp stored False;
    property Action: WideString index 125 read GetWideStringProp write SetWideStringProp stored False;
    property AutoReconnect: WordBool index 117 read GetWordBoolProp write SetWordBoolProp stored False;
    property ConfirmOnlineTransitions: WordBool index 118 read GetWordBoolProp write SetWordBoolProp stored False;
    property CurrentCellExpression: WideString index 127 read GetWideStringProp write SetWideStringProp stored False;
    property FtpPort: Integer index 123 read GetIntegerProp write SetIntegerProp stored False;
    property GridOpacity: Double index 106 read GetDoubleProp write SetDoubleProp stored False;
    property GridScale: Double index 130 read GetDoubleProp write SetDoubleProp stored False;
    property ImageOrientation: TOleEnum index 120 read GetTOleEnumProp write SetTOleEnumProp stored False;
    property ImageScale: Double index 107 read GetDoubleProp write SetDoubleProp stored False;
    property ImageZoomMode: TOleEnum index 108 read GetTOleEnumProp write SetTOleEnumProp stored False;
    property MaxUserAccess: TOleEnum index 116 read GetTOleEnumProp write SetTOleEnumProp stored False;
    property ScrollMode: TOleEnum index 134 read GetTOleEnumProp write SetTOleEnumProp stored False;
    property ShiftPartialImage: WordBool index 119 read GetWordBoolProp write SetWordBoolProp stored False;
    property ShowCustomView: WordBool index 114 read GetWordBoolProp write SetWordBoolProp stored False;
    property ShowDependencyErrorsOnly: WordBool index 121 read GetWordBoolProp write SetWordBoolProp stored False;
    property ShowGraphics: WordBool index 102 read GetWordBoolProp write SetWordBoolProp stored False;
    property ShowGrid: WordBool index 103 read GetWordBoolProp write SetWordBoolProp stored False;
    property ShowImage: WordBool index 101 read GetWordBoolProp write SetWordBoolProp stored False;
    property ShowImageSaturation: WordBool index 122 read GetWordBoolProp write SetWordBoolProp stored False;
    property ShowScrollBars: WordBool index 104 read GetWordBoolProp write SetWordBoolProp stored False;
    property SnippetPath: WideString index 124 read GetWideStringProp write SetWideStringProp stored False;
    property SoftOnline: WordBool index 136 read GetWordBoolProp write SetWordBoolProp stored False;
    property StatusLevelStyle: TOleEnum index 201 read GetTOleEnumProp write SetTOleEnumProp stored False;
    property ShowThumbnailImage: WordBool index 202 read GetWordBoolProp write SetWordBoolProp stored False;
    property HeightScale: TOleEnum index 203 read GetTOleEnumProp write SetTOleEnumProp stored False;
    property ShowSummary: WordBool index 206 read GetWordBoolProp write SetWordBoolProp stored False;
    property SplitterHeight: Integer index 207 read GetIntegerProp write SetIntegerProp stored False;
    property FilmstripHeight: Integer index 208 read GetIntegerProp write SetIntegerProp stored False;
    property IsSplitterFixed: WordBool index 209 read GetWordBoolProp write SetWordBoolProp stored False;
    property RecordActive: WordBool index 400 read GetWordBoolProp write SetWordBoolProp stored False;
    property RecordFilenameFormat: WideString index 402 read GetWideStringProp write SetWideStringProp stored False;
    property RecordFolder: WideString index 403 read GetWideStringProp write SetWideStringProp stored False;
    property RecordIndex: Integer index 405 read GetIntegerProp write SetIntegerProp stored False;
    property RecordMax: Integer index 406 read GetIntegerProp write SetIntegerProp stored False;
    property RecordMode: TOleEnum index 407 read GetTOleEnumProp write SetTOleEnumProp stored False;
    property RecordResolution: TOleEnum index 408 read GetTOleEnumProp write SetTOleEnumProp stored False;
    property RecordBitDepth: TOleEnum index 409 read GetTOleEnumProp write SetTOleEnumProp stored False;
    property PlayActive: WordBool index 411 read GetWordBoolProp write SetWordBoolProp stored False;
    property PlaybackContinuous: WordBool index 412 read GetWordBoolProp write SetWordBoolProp stored False;
    property PlaybackDelay: Double index 413 read GetDoubleProp write SetDoubleProp stored False;
    property PlaybackFilename: WideString index 414 read GetWideStringProp write SetWideStringProp stored False;
    property PlaybackFolder: WideString index 415 read GetWideStringProp write SetWideStringProp stored False;
    property PlaybackIndex: Integer index 417 read GetIntegerProp write SetIntegerProp stored False;
    property OnConnectedChanged: TNotifyEvent read FOnConnectedChanged write FOnConnectedChanged;
    property OnCurrentCellChanged: TNotifyEvent read FOnCurrentCellChanged write FOnCurrentCellChanged;
    property OnGridOffsetChanged: TNotifyEvent read FOnGridOffsetChanged write FOnGridOffsetChanged;
    property OnGridScaleChanged: TNotifyEvent read FOnGridScaleChanged write FOnGridScaleChanged;
    property OnImageOffsetChanged: TNotifyEvent read FOnImageOffsetChanged write FOnImageOffsetChanged;
    property OnImageScaleChanged: TNotifyEvent read FOnImageScaleChanged write FOnImageScaleChanged;
    property OnInSightChanged: TNotifyEvent read FOnInSightChanged write FOnInSightChanged;
    property OnResultsChanged: TNotifyEvent read FOnResultsChanged write FOnResultsChanged;
    property OnScrollModeChanged: TNotifyEvent read FOnScrollModeChanged write FOnScrollModeChanged;
    property OnSelectedRangeChanged: TNotifyEvent read FOnSelectedRangeChanged write FOnSelectedRangeChanged;
    property OnStateChanged: TNotifyEvent read FOnStateChanged write FOnStateChanged;
    property OnStatusInformationChanged: TNotifyEvent read FOnStatusInformationChanged write FOnStatusInformationChanged;
    property OnCurrentCellExpressionChanged: TNotifyEvent read FOnCurrentCellExpressionChanged write FOnCurrentCellExpressionChanged;
    property OnConnectCompleted: TCvsPlaybackFilmstripDisplayConnectCompleted read FOnConnectCompleted write FOnConnectCompleted;
    property OnGridOpacityChanged: TNotifyEvent read FOnGridOpacityChanged write FOnGridOpacityChanged;
    property OnImageOrientationChanged: TNotifyEvent read FOnImageOrientationChanged write FOnImageOrientationChanged;
  end;

procedure Register;

resourcestring
  dtlServerPage = 'ActiveX';

  dtlOcxPage = 'ActiveX';

implementation

uses ComObj;

procedure TCvsInSightDisplay.InitControlData;
const
  CEventDispIDs: array [0..15] of DWORD = (
    $00000001, $00000002, $00000003, $00000004, $00000005, $00000006,
    $00000007, $00000008, $00000009, $0000000A, $0000000B, $0000000C,
    $0000000E, $0000000F, $00000010, $00000011);
  CControlData: TControlData2 = (
    ClassID: '{86012278-F05D-4C41-A313-51CE65032312}';
    EventIID: '{AAB65625-0DF2-425F-89AE-8AE07431BC50}';
    EventCount: 16;
    EventDispIDs: @CEventDispIDs;
    LicenseKey: nil (*HR:$8007007E*);
    Flags: $00000008;
    Version: 401);
begin
  ControlData := @CControlData;
  TControlData2(CControlData).FirstEventOfs := Cardinal(@@FOnConnectedChanged) - Cardinal(Self);
end;

procedure TCvsInSightDisplay.CreateControl;

  procedure DoCreate;
  begin
    FIntf := IUnknown(OleObject) as ICvsInSightDisplay;
  end;

begin
  if FIntf = nil then DoCreate;
end;

function TCvsInSightDisplay.GetControlInterface: ICvsInSightDisplay;
begin
  CreateControl;
  Result := FIntf;
end;

function TCvsInSightDisplay.Get_CurrentCell: CvsCellLocation;
begin
    Result := DefaultInterface.CurrentCell;
end;

function TCvsInSightDisplay.Get_Edit: _CvsInSightDisplayEdit;
begin
    Result := DefaultInterface.Edit;
end;

function TCvsInSightDisplay.Get_GridOffset: CvsCellLocation;
begin
    Result := DefaultInterface.GridOffset;
end;

function TCvsInSightDisplay.Get_ImageOffset: PointF;
begin
    Result := DefaultInterface.ImageOffset;
end;

function TCvsInSightDisplay.Get_InSight: _CvsInSight;
begin
    Result := DefaultInterface.InSight;
end;

procedure TCvsInSightDisplay._Set_InSight(const InSight: _CvsInSight);
  { Warning: The property InSight has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.InSight := InSight;
end;

function TCvsInSightDisplay.Get_Recorder: _CvsInSightDisplayRecorder;
begin
    Result := DefaultInterface.Recorder;
end;

function TCvsInSightDisplay.Get_Results: _CvsResultSet;
begin
    Result := DefaultInterface.Results;
end;

procedure TCvsInSightDisplay._Set_Results(const pVal: _CvsResultSet);
  { Warning: The property Results has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.Results := pVal;
end;

function TCvsInSightDisplay.Get_SelectedRange: CvsCellRange;
begin
    Result := DefaultInterface.SelectedRange;
end;

procedure TCvsInSightDisplay.Refresh;
begin
  DefaultInterface.Refresh;
end;

procedure TCvsInSightDisplay.Connect(const hostName: WideString; const userName: WideString; 
                                     const password: WideString; forceConnect: WordBool);
begin
  DefaultInterface.Connect(hostName, userName, password, forceConnect);
end;

procedure TCvsInSightDisplay.Disconnect;
begin
  DefaultInterface.Disconnect;
end;

procedure TCvsInSightDisplay.EditCellGraphic(row: Integer; column: Integer);
begin
  DefaultInterface.EditCellGraphic(row, column);
end;

procedure TCvsInSightDisplay.EnsureCellIsVisible(row: Integer; column: Integer);
begin
  DefaultInterface.EnsureCellIsVisible(row, column);
end;

function TCvsInSightDisplay.GetPicture: IPictureDisp;
begin
  Result := DefaultInterface.GetPicture;
end;

procedure TCvsInSightDisplay.SaveBitmap(const fileName: WideString);
begin
  DefaultInterface.SaveBitmap(fileName);
end;

function TCvsInSightDisplay.GetCellBounds(row: Integer; column: Integer; rows: Integer; 
                                          columns: Integer): Rectangle;
begin
  Result := DefaultInterface.GetCellBounds(row, column, rows, columns);
end;

procedure TCvsInSightDisplay.SetCurrentCell(row: Integer; column: Integer);
begin
  DefaultInterface.SetCurrentCell(row, column);
end;

procedure TCvsInSightDisplay.SetGridOffset(row: Integer; column: Integer);
begin
  DefaultInterface.SetGridOffset(row, column);
end;

procedure TCvsInSightDisplay.SetImageScaleAndOffset(Scale: Double; x: Single; y: Single);
begin
  DefaultInterface.SetImageScaleAndOffset(Scale, x, y);
end;

procedure TCvsInSightDisplay.SetSelectedRange(row: Integer; column: Integer; rows: Integer; 
                                              columns: Integer);
begin
  DefaultInterface.SetSelectedRange(row, column, rows, columns);
end;

procedure TCvsInSightDisplay.OpenPropertySheet(row: Integer; column: Integer; 
                                               const expression: WideString);
begin
  DefaultInterface.OpenPropertySheet(row, column, expression);
end;

procedure TCvsInSightDisplay.ClickSpreadsheetControl(row: Integer; column: Integer);
begin
  DefaultInterface.ClickSpreadsheetControl(row, column);
end;

procedure TCvsFilmstrip.InitControlData;
const
  CControlData: TControlData2 = (
    ClassID: '{50FDEAE0-F676-489E-BBEB-16AC1628A3CC}';
    EventIID: '';
    EventCount: 0;
    EventDispIDs: nil;
    LicenseKey: nil (*HR:$8007007E*);
    Flags: $00000008;
    Version: 401);
begin
  ControlData := @CControlData;
end;

procedure TCvsFilmstrip.CreateControl;

  procedure DoCreate;
  begin
    FIntf := IUnknown(OleObject) as ICvsFilmstrip;
  end;

begin
  if FIntf = nil then DoCreate;
end;

function TCvsFilmstrip.GetControlInterface: ICvsFilmstrip;
begin
  CreateControl;
  Result := FIntf;
end;

function TCvsFilmstrip.Get_FilmQueue: ICvsFilmQueue;
begin
    Result := DefaultInterface.FilmQueue;
end;

procedure TCvsFilmstrip._Set_FilmQueue(const pVal: ICvsFilmQueue);
  { Warning: The property FilmQueue has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.FilmQueue := pVal;
end;

procedure TCvsFilmstrip.Refresh;
begin
  DefaultInterface.Refresh;
end;

procedure TCvsSensorFilmstripDisplay.InitControlData;
const
  CEventDispIDs: array [0..15] of DWORD = (
    $00000001, $00000002, $00000003, $00000004, $00000005, $00000006,
    $00000007, $00000008, $00000009, $0000000A, $0000000B, $0000000C,
    $0000000E, $0000000F, $00000010, $00000011);
  CControlData: TControlData2 = (
    ClassID: '{42E70435-2112-48FC-B63B-D7CA77AB7C6C}';
    EventIID: '{A351A047-DDA2-4B3D-85A5-9774564801AC}';
    EventCount: 16;
    EventDispIDs: @CEventDispIDs;
    LicenseKey: nil (*HR:$8007007E*);
    Flags: $00000008;
    Version: 401);
begin
  ControlData := @CControlData;
  TControlData2(CControlData).FirstEventOfs := Cardinal(@@FOnConnectedChanged) - Cardinal(Self);
end;

procedure TCvsSensorFilmstripDisplay.CreateControl;

  procedure DoCreate;
  begin
    FIntf := IUnknown(OleObject) as ICvsSensorFilmstripDisplay;
  end;

begin
  if FIntf = nil then DoCreate;
end;

function TCvsSensorFilmstripDisplay.GetControlInterface: ICvsSensorFilmstripDisplay;
begin
  CreateControl;
  Result := FIntf;
end;

function TCvsSensorFilmstripDisplay.Get_CurrentCell: CvsCellLocation;
begin
    Result := DefaultInterface.CurrentCell;
end;

function TCvsSensorFilmstripDisplay.Get_Edit: _CvsInSightDisplayEdit;
begin
    Result := DefaultInterface.Edit;
end;

function TCvsSensorFilmstripDisplay.Get_GridOffset: CvsCellLocation;
begin
    Result := DefaultInterface.GridOffset;
end;

function TCvsSensorFilmstripDisplay.Get_ImageOffset: PointF;
begin
    Result := DefaultInterface.ImageOffset;
end;

function TCvsSensorFilmstripDisplay.Get_InSight: _CvsInSight;
begin
    Result := DefaultInterface.InSight;
end;

procedure TCvsSensorFilmstripDisplay._Set_InSight(const InSight: _CvsInSight);
  { Warning: The property InSight has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.InSight := InSight;
end;

function TCvsSensorFilmstripDisplay.Get_Recorder: _CvsInSightDisplayRecorder;
begin
    Result := DefaultInterface.Recorder;
end;

function TCvsSensorFilmstripDisplay.Get_Results: _CvsResultSet;
begin
    Result := DefaultInterface.Results;
end;

procedure TCvsSensorFilmstripDisplay._Set_Results(const pVal: _CvsResultSet);
  { Warning: The property Results has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.Results := pVal;
end;

function TCvsSensorFilmstripDisplay.Get_SelectedRange: CvsCellRange;
begin
    Result := DefaultInterface.SelectedRange;
end;

function TCvsSensorFilmstripDisplay.Get_FilmQueue: ICvsFilmQueue;
begin
    Result := DefaultInterface.FilmQueue;
end;

procedure TCvsSensorFilmstripDisplay._Set_FilmQueue(const pVal: ICvsFilmQueue);
  { Warning: The property FilmQueue has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.FilmQueue := pVal;
end;

function TCvsSensorFilmstripDisplay.Get_Options: _CvsFilmstripResultsQueueOptions;
begin
    Result := DefaultInterface.Options;
end;

procedure TCvsSensorFilmstripDisplay._Set_Options(const ppVal: _CvsFilmstripResultsQueueOptions);
  { Warning: The property Options has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.Options := ppVal;
end;

function TCvsSensorFilmstripDisplay.Get_FreezeQueue: ICvsActionToggle;
begin
    Result := DefaultInterface.FreezeQueue;
end;

function TCvsSensorFilmstripDisplay.Get_Settings: _CvsFilmstripResultsQueueSettings;
begin
    Result := DefaultInterface.Settings;
end;

procedure TCvsSensorFilmstripDisplay.Refresh;
begin
  DefaultInterface.Refresh;
end;

procedure TCvsSensorFilmstripDisplay.Connect(const hostName: WideString; 
                                             const userName: WideString; 
                                             const password: WideString; forceConnect: WordBool);
begin
  DefaultInterface.Connect(hostName, userName, password, forceConnect);
end;

procedure TCvsSensorFilmstripDisplay.Disconnect;
begin
  DefaultInterface.Disconnect;
end;

procedure TCvsSensorFilmstripDisplay.EditCellGraphic(row: Integer; column: Integer);
begin
  DefaultInterface.EditCellGraphic(row, column);
end;

procedure TCvsSensorFilmstripDisplay.EnsureCellIsVisible(row: Integer; column: Integer);
begin
  DefaultInterface.EnsureCellIsVisible(row, column);
end;

function TCvsSensorFilmstripDisplay.GetPicture: IPictureDisp;
begin
  Result := DefaultInterface.GetPicture;
end;

procedure TCvsSensorFilmstripDisplay.SaveBitmap(const fileName: WideString);
begin
  DefaultInterface.SaveBitmap(fileName);
end;

function TCvsSensorFilmstripDisplay.GetCellBounds(row: Integer; column: Integer; rows: Integer; 
                                                  columns: Integer): Rectangle;
begin
  Result := DefaultInterface.GetCellBounds(row, column, rows, columns);
end;

procedure TCvsSensorFilmstripDisplay.SetCurrentCell(row: Integer; column: Integer);
begin
  DefaultInterface.SetCurrentCell(row, column);
end;

procedure TCvsSensorFilmstripDisplay.SetGridOffset(row: Integer; column: Integer);
begin
  DefaultInterface.SetGridOffset(row, column);
end;

procedure TCvsSensorFilmstripDisplay.SetImageScaleAndOffset(Scale: Double; x: Single; y: Single);
begin
  DefaultInterface.SetImageScaleAndOffset(Scale, x, y);
end;

procedure TCvsSensorFilmstripDisplay.SetSelectedRange(row: Integer; column: Integer; rows: Integer; 
                                                      columns: Integer);
begin
  DefaultInterface.SetSelectedRange(row, column, rows, columns);
end;

procedure TCvsSensorFilmstripDisplay.OpenPropertySheet(row: Integer; column: Integer; 
                                                       const expression: WideString);
begin
  DefaultInterface.OpenPropertySheet(row, column, expression);
end;

procedure TCvsSensorFilmstripDisplay.ClickSpreadsheetControl(row: Integer; column: Integer);
begin
  DefaultInterface.ClickSpreadsheetControl(row, column);
end;

procedure TCvsPlaybackFilmstripDisplay.InitControlData;
const
  CEventDispIDs: array [0..15] of DWORD = (
    $00000001, $00000002, $00000003, $00000004, $00000005, $00000006,
    $00000007, $00000008, $00000009, $0000000A, $0000000B, $0000000C,
    $0000000E, $0000000F, $00000010, $00000011);
  CControlData: TControlData2 = (
    ClassID: '{BDE52065-AE85-4278-8666-124D863847EF}';
    EventIID: '{0A9AF383-19B1-489A-985B-4642B0E7DB89}';
    EventCount: 16;
    EventDispIDs: @CEventDispIDs;
    LicenseKey: nil (*HR:$8007007E*);
    Flags: $00000008;
    Version: 401);
begin
  ControlData := @CControlData;
  TControlData2(CControlData).FirstEventOfs := Cardinal(@@FOnConnectedChanged) - Cardinal(Self);
end;

procedure TCvsPlaybackFilmstripDisplay.CreateControl;

  procedure DoCreate;
  begin
    FIntf := IUnknown(OleObject) as ICvsPlaybackFilmstripDisplay;
  end;

begin
  if FIntf = nil then DoCreate;
end;

function TCvsPlaybackFilmstripDisplay.GetControlInterface: ICvsPlaybackFilmstripDisplay;
begin
  CreateControl;
  Result := FIntf;
end;

function TCvsPlaybackFilmstripDisplay.Get_CurrentCell: CvsCellLocation;
begin
    Result := DefaultInterface.CurrentCell;
end;

function TCvsPlaybackFilmstripDisplay.Get_Edit: _CvsInSightDisplayEdit;
begin
    Result := DefaultInterface.Edit;
end;

function TCvsPlaybackFilmstripDisplay.Get_GridOffset: CvsCellLocation;
begin
    Result := DefaultInterface.GridOffset;
end;

function TCvsPlaybackFilmstripDisplay.Get_ImageOffset: PointF;
begin
    Result := DefaultInterface.ImageOffset;
end;

function TCvsPlaybackFilmstripDisplay.Get_InSight: _CvsInSight;
begin
    Result := DefaultInterface.InSight;
end;

procedure TCvsPlaybackFilmstripDisplay._Set_InSight(const InSight: _CvsInSight);
  { Warning: The property InSight has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.InSight := InSight;
end;

function TCvsPlaybackFilmstripDisplay.Get_Recorder: _CvsInSightDisplayRecorder;
begin
    Result := DefaultInterface.Recorder;
end;

function TCvsPlaybackFilmstripDisplay.Get_Results: _CvsResultSet;
begin
    Result := DefaultInterface.Results;
end;

procedure TCvsPlaybackFilmstripDisplay._Set_Results(const pVal: _CvsResultSet);
  { Warning: The property Results has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.Results := pVal;
end;

function TCvsPlaybackFilmstripDisplay.Get_SelectedRange: CvsCellRange;
begin
    Result := DefaultInterface.SelectedRange;
end;

function TCvsPlaybackFilmstripDisplay.Get_FilmQueue: ICvsFilmQueue;
begin
    Result := DefaultInterface.FilmQueue;
end;

procedure TCvsPlaybackFilmstripDisplay._Set_FilmQueue(const pVal: ICvsFilmQueue);
  { Warning: The property FilmQueue has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.FilmQueue := pVal;
end;

procedure TCvsPlaybackFilmstripDisplay.Refresh;
begin
  DefaultInterface.Refresh;
end;

procedure TCvsPlaybackFilmstripDisplay.Connect(const hostName: WideString; 
                                               const userName: WideString; 
                                               const password: WideString; forceConnect: WordBool);
begin
  DefaultInterface.Connect(hostName, userName, password, forceConnect);
end;

procedure TCvsPlaybackFilmstripDisplay.Disconnect;
begin
  DefaultInterface.Disconnect;
end;

procedure TCvsPlaybackFilmstripDisplay.EditCellGraphic(row: Integer; column: Integer);
begin
  DefaultInterface.EditCellGraphic(row, column);
end;

procedure TCvsPlaybackFilmstripDisplay.EnsureCellIsVisible(row: Integer; column: Integer);
begin
  DefaultInterface.EnsureCellIsVisible(row, column);
end;

function TCvsPlaybackFilmstripDisplay.GetPicture: IPictureDisp;
begin
  Result := DefaultInterface.GetPicture;
end;

procedure TCvsPlaybackFilmstripDisplay.SaveBitmap(const fileName: WideString);
begin
  DefaultInterface.SaveBitmap(fileName);
end;

function TCvsPlaybackFilmstripDisplay.GetCellBounds(row: Integer; column: Integer; rows: Integer; 
                                                    columns: Integer): Rectangle;
begin
  Result := DefaultInterface.GetCellBounds(row, column, rows, columns);
end;

procedure TCvsPlaybackFilmstripDisplay.SetCurrentCell(row: Integer; column: Integer);
begin
  DefaultInterface.SetCurrentCell(row, column);
end;

procedure TCvsPlaybackFilmstripDisplay.SetGridOffset(row: Integer; column: Integer);
begin
  DefaultInterface.SetGridOffset(row, column);
end;

procedure TCvsPlaybackFilmstripDisplay.SetImageScaleAndOffset(Scale: Double; x: Single; y: Single);
begin
  DefaultInterface.SetImageScaleAndOffset(Scale, x, y);
end;

procedure TCvsPlaybackFilmstripDisplay.SetSelectedRange(row: Integer; column: Integer; 
                                                        rows: Integer; columns: Integer);
begin
  DefaultInterface.SetSelectedRange(row, column, rows, columns);
end;

procedure TCvsPlaybackFilmstripDisplay.OpenPropertySheet(row: Integer; column: Integer; 
                                                         const expression: WideString);
begin
  DefaultInterface.OpenPropertySheet(row, column, expression);
end;

procedure TCvsPlaybackFilmstripDisplay.ClickSpreadsheetControl(row: Integer; column: Integer);
begin
  DefaultInterface.ClickSpreadsheetControl(row, column);
end;

function TCvsPlaybackFilmstripDisplay.GetPlaybackFiles: PUserType1;
begin
  Result := DefaultInterface.GetPlaybackFiles;
end;

procedure TCvsPlaybackFilmstripDisplay.PlayFirst;
begin
  DefaultInterface.PlayFirst;
end;

procedure TCvsPlaybackFilmstripDisplay.PlayPrevious;
begin
  DefaultInterface.PlayPrevious;
end;

procedure TCvsPlaybackFilmstripDisplay.PlayNext;
begin
  DefaultInterface.PlayNext;
end;

procedure TCvsPlaybackFilmstripDisplay.PlayLast;
begin
  DefaultInterface.PlayLast;
end;

procedure Register;
begin
  RegisterComponents(dtlOcxPage, [TCvsInSightDisplay, TCvsFilmstrip, TCvsSensorFilmstripDisplay, TCvsPlaybackFilmstripDisplay]);
end;

end.
