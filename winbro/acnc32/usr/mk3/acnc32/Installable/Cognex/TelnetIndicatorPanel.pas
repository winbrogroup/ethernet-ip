unit TelnetIndicatorPanel;

interface
uses CoreCNC, CNCTypes, CNC32, Classes, Windows, Controls, Graphics,StdCtrls,ExtCtrls,
TelnetIndicator,SysUtils;



type


TTelnetIndicatorPanel = class(TPanel)
 private
 BusyIndicator : TNamedIndicator;
 HealthyIndicator: TNamedIndicator;
 ConnectedIndicator: TNamedIndicator;
 LoggedInIndicator: TNamedIndicator;
 OnLineIndicator  : TNamedIndicator;
 CommandResponseDisplay : TNamedIndicator;
    FHealthy: Boolean;
    FBusy: Boolean;                                       
    FConnected: Boolean;
    FNCComm: Boolean;
    FLoggedIn: Boolean;
    FCrValue: Integer;
    FCrStrValue: String;
    FOnline: Boolean;
    procedure RedrawOnResize(Sender : TObject);
    procedure SetBusy(const Value: Boolean);
    procedure SetConnected(const Value: Boolean);
    procedure SetHealthy(const Value: Boolean);
    procedure SetLoggedIn(const Value: Boolean);
    procedure SetCRValue(const Value: Integer);
    procedure SetCRString(const Value: String);
    procedure SetOnLine(const Value: Boolean);
 public
   constructor Create(AOwner: TComponent); override;
   property BusyStatus      : Boolean  read FBusy      write SetBusy;
   property ConnectedStatus : Boolean  read FConnected write SetConnected;
   property HealthyStatus   : Boolean  read FHealthy   write SetHealthy;
   property LoggedInStatus : Boolean  read FLoggedIn    write SetLoggedIn;
   property OnLineStatus   : Boolean  read FOnline    write SetOnLine;
   property CommandReponseValueAsInteger : Integer Read FCrValue write SetCRValue;
   property CommandReponseValueAsString : String Read FCrStrValue write SetCRString;

 end;

implementation

{ TTelnetIndicatorPanel }

constructor TTelnetIndicatorPanel.Create(AOwner: TComponent);
begin
  inherited;
  BevelOuter := bvNone;
  BevelInner := bvNone;
  BusyIndicator := TNamedIndicator.CreateOnParent(self,False);
  with BusyIndicator do
    begin
    Legend := 'Busy';
    OnColour := clRed;
    OffColour := clGray;
    HighLightOnColour := clYellow;
    HighLightOffColour := clSilver;
    end;
  HealthyIndicator:= TNamedIndicator.CreateOnParent(self,False);
  with HealthyIndicator do
    begin
    Legend := 'Healthy';
    OnColour := clGreen;
    OffColour := clGray;
    HighLightOnColour := clYellow;
    HighLightOffColour := clSilver;
    end;

  ConnectedIndicator:= TNamedIndicator.CreateOnParent(self,False);
  with ConnectedIndicator do
    begin
    Legend := 'Connected';
    OnColour := clGreen;
    OffColour := clGray;
    HighLightOnColour := clYellow;
    HighLightOffColour := clSilver;
    end;

  LoggedInIndicator:= TNamedIndicator.CreateOnParent(self,False);
  with LoggedInIndicator do
    begin
    Legend := 'Logged In';
    OnColour := clGreen;
    OffColour := clGray;
    HighLightOnColour := clYellow;
    HighLightOffColour := clSilver;
    end;

  OnLineIndicator:= TNamedIndicator.CreateOnParent(self,False);
  with OnLineIndicator do
    begin
    Legend := 'On Line';
    OnColour := clGreen;
    OffColour := clGray;
    HighLightOnColour := clYellow;
    HighLightOffColour := clSilver;
    end;

  CommandResponseDisplay := TNamedIndicator.CreateOnParent(self,True);
  with CommandResponseDisplay do
    begin
    Legend := 'Cmd Status';
    ValueWidth := 30;
    end;

  OnResize := RedrawOnResize;
end;

procedure TTelnetIndicatorPanel.RedrawOnResize(Sender: TObject);
begin
  try
  with ConnectedIndicator do
    begin
    PanelTop := 5;
    PanelLeft := 3;
    PanelWidth := ClientWidth-6;
    PanelHeight := 24;
    end;

  with LoggedInIndicator do
    begin
    PanelTop := ConnectedIndicator.PanelTop+ConnectedIndicator.PanelHeight+1;
    PanelLeft := 3;
    PanelWidth := ClientWidth-6;
    PanelHeight := 24;
    end;

  with HealthyIndicator do
    begin
    PanelTop := LoggedInIndicator.PanelTop+LoggedInIndicator.PanelHeight+1;
    PanelLeft := 3;
    PanelWidth := ClientWidth-6;
    PanelHeight := 24;
    end;

  with BusyIndicator do
    begin
    PanelTop := HealthyIndicator.PanelTop+HealthyIndicator.PanelHeight+1;
    PanelLeft := 3;
    PanelWidth := ClientWidth-6;
    PanelHeight := 24;
    end;

  with OnLineIndicator do
    begin
    PanelTop := BusyIndicator.PanelTop+BusyIndicator.PanelHeight+1;
    PanelLeft := 3;
    PanelWidth := ClientWidth-6;
    PanelHeight := 24;
    end;

  with CommandResponseDisplay do
    begin
    PanelTop := OnLineIndicator.PanelTop+BusyIndicator.PanelHeight+1;
    PanelLeft := 3;
    PanelWidth := ClientWidth-6;
    PanelHeight := 24;
    end;
  except
  end;

end;

procedure TTelnetIndicatorPanel.SetHealthy(const Value: Boolean);
begin
  FHealthy := Value;
  HealthyIndicator.OnStatus := FHealthy;
end;

procedure TTelnetIndicatorPanel.SetOnLine(const Value: Boolean);
begin
  FOnline := Value;
  OnLineIndicator.OnStatus := FOnline;
end;


procedure TTelnetIndicatorPanel.SetBusy(const Value: Boolean);
begin
  FBusy := Value;
  BusyIndicator.OnStatus := FBusy;
end;

procedure TTelnetIndicatorPanel.SetConnected(const Value: Boolean);
begin
  FConnected := Value;
  ConnectedIndicator.OnStatus := FConnected;
end;


procedure TTelnetIndicatorPanel.SetLoggedIn(const Value: Boolean);
begin
  FLoggedIn := Value;
  LoggedInIndicator.OnStatus := FLoggedIn;
end;

procedure TTelnetIndicatorPanel.SetCRValue(const Value: Integer);
begin
  FCrValue := Value;
  FCrStrValue := IntToStr(FCrValue);
  CommandResponseDisplay.ValueAsString := FCrStrValue;
end;

procedure TTelnetIndicatorPanel.SetCRString(const Value: String);
begin
  FCrStrValue := Value;
  CommandResponseDisplay.ValueAsString := FCrStrValue;
end;


end.
