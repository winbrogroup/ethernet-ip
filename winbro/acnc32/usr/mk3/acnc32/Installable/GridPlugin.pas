unit GridPlugin;

interface

uses Classes, SysUtils, CNCTypes, CNC32, CoreCNC, Grids, CNCAbs, Windows, Named,
     ExtCtrls, Controls, Plugin, StdCtrls, Tokenizer, Graphics, Buttons,
     IGridPlugin, Messages, KBControl;

type
  TPointArray = array [0..32000] of TPoint;
  PPointArray = ^TPointArray;

  TNotifyNudge = procedure (Sender : TObject; Upd : TEditChange; X, Y : Integer) of object;

  TExtStringGrid = class(TStringGrid)
  private
    FOnNudge : TNotifyNudge;
    procedure CNCMNudge( var Msg: TMessage); message CNCM_NUDGE;
  public
    property OnNudge : TNotifyNudge read FOnNudge write FOnNudge;
  end;

  TGridPlugin = class(TExpandDisplay, IFSDGridPluginHost)
  private
    HMod : HMODULE;
    GetFSDGridPluginInterface : TGetFSDGridPluginInterface;
    Grid : TExtStringGrid;
    TitlePanel : TPanel;
    StatusPanel : TPanel;
    InPlaceEdit : TSpeedButton;
    ComboPanel : TPanel;
    Combo : TComboBox;
    SelfDraw : Boolean;
    GridPlugin : IFSDGridPlugin;
    procedure Nudge(Sender : TObject; Upd : TEditChange; X, Y : Integer);
    procedure ReadConfig;
    procedure WriteConfig;
    procedure GridSelectCell(Sender: TObject; ACol, ARow: Longint; var CanSelect: Boolean);
    procedure GridSetEditText(Sender: TObject; ACol, ARow : Integer; const Value : string);
    procedure GridDrawCell(Sender : TObject; ACol, ARow : Integer; Rect : TRect; State : TGridDrawState);
    procedure ComboChange(Sender : TObject);
    procedure InPlaceEditClick(Sender : TObject);
    procedure UpdateEv(GridUT : TGridUpdateType; X, Y : Integer; Data : Pointer); stdcall;
  protected
    procedure UpdatePlugin(aTag : TAbstractTag);
  public
    constructor Create(aOwner : TComponent); override;
    procedure InstallComponent(Params : TParamStrings); override;
    procedure Installed; override;
    procedure IShutdown; override;
    destructor Destroy; override;
  end;

implementation

procedure TGridPlugin.UpdateEv(GridUT : TGridUpdateType; X, Y : Integer; Data : Pointer); stdcall;
var HL : PPointArray;
    I, J : Integer;
begin
  case GridUT of
    guCell : Grid.Cells[X, Y] := PChar(Data);
    guCount : begin
      Grid.ColCount := X;
      Grid.RowCount := Y;
    end;
    guHighLight : begin
      for I := 0 to Grid.ColCount - 1 do
        for J := 0 to Grid.RowCount - 1 do
          Grid.Objects[I, J] := nil;
      HL := Data;
      for I := 0 to X - 1 do begin
        if (HL[I].X < Grid.ColCount) and (HL[I].Y < Grid.RowCount) then
          Grid.Objects[HL[I].X, HL[I].Y] := Pointer(1);
      end;
    end;

    guFixed : begin
      Grid.FixedCols := X;
      Grid.FixedRows := Y;
    end;

    guTitle : TitlePanel.Caption := PChar(Data);
    guStatus : StatusPanel.Caption := PChar(Data);
    guSelfDraw : SelfDraw := X <> 0;
    guCombo : begin
      if X <> 0 then begin
        ComboPanel.Visible := True;
        Combo.Items.Text := PChar(Data);
        Combo.ItemIndex := 0;
      end else begin
        ComboPanel.Visible := False;
      end;
    end;

    guVisible : Grid.Visible := X <> 0;

    guEditing :
      if X = 0 then
       Grid.Options := Grid.Options - [goEditing]
      else
       Grid.Options := Grid.Options + [goEditing];

  end;
end;

constructor TGridPlugin.Create(aOwner : TComponent);
begin
  inherited Create(aOwner);
  TitlePanel := TPanel.Create(Self);
  TitlePanel.Parent := Self;
  TitlePanel.Align := alTop;
  TitlePanel.Caption := '';
  TitlePanel.Height := 22;

  StatusPanel := TPanel.Create(Self);
  StatusPanel.Parent := Self;
  StatusPanel.Align := alBottom;
  StatusPanel.Caption := '';
  StatusPanel.Height := 22;

  InPlaceEdit := TSpeedButton.Create(Self);
  InPlaceEdit.Parent := StatusPanel;
  InPlaceEdit.Caption := 'FTE';
  InPlaceEdit.Anchors := [akRight, akTop, akBottom];
  InPlaceEdit.Width := 100;
  InPlaceEdit.Left := StatusPanel.Width - InPlaceEdit.Width;
  InPlaceEdit.Top := 0;
  InPlaceEdit.Height := StatusPanel.Height;
  InPlaceEdit.OnClick := InPlaceEditClick;

  ComboPanel := TPanel.Create(Self);
  ComboPanel.Parent := Self;
  ComboPanel.Caption := '';
  ComboPanel.Align := alTop;
  ComboPanel.Height := 24;
  ComboPanel.Visible := False;

  Combo := TComboBox.Create(Self);
  Combo.Parent := ComboPanel;
  Combo.Top := 2;
  Combo.Left := 10;
  Combo.OnChange := ComboChange;
  Combo.TabStop := False;

  Grid := TExtStringGrid.Create(Self);
  Grid.Parent := Self;
  Grid.Align := alClient;
  Grid.OnSelectCell := GridSelectCell;
  Grid.OnSetEditText := GridSetEditText;
  Grid.OnDrawCell := GridDrawCell;
  Grid.Options := Grid.Options + [goEditing, goColSizing];
//  ManageEdit := False;
  __KBC.CreateButton( [kbAlpha, kbNumeric, kbNavigation], Self, alTop);
end;

procedure TGridPlugin.InstallComponent(Params : TParamStrings);
var LibName, GridPluginName : string;
begin
  inherited InstallComponent(Params);

  LibName := Params.ReadString('Plugin', '*');
  if LibName = '*' then
    raise ECNCInstallFault.Create('Plugin property not set in profile "Plugin = DllName"');
  HMod := SysObj.Comms.FindPlugin(LibName);
  if HMod = 0 then
    raise ECNCInstallFault.CreateFmt('Unable to load library "%s"', [LibName]);

  GetFSDGridPluginInterface := GetProcAddress(HMod, GetFSDGridPluginInterfaceName);
  if not Assigned(GetFSDGridPluginInterface) then
    raise ECNCInstallFault.CreateFmt('Interface "%s" not present in "%s"', [GetFSDGridPluginInterfaceName, LibName]);

  GridPluginName := Params.ReadString('Object', '*');
  if GridPluginName = '*' then
    raise ECNCInstallFault.Create('Grid plugin property not set in profile "Object = Name"');

  GridPlugin := GetFSDGridPluginInterface(Self, PWideString(GridPluginName));
  if not Assigned(GridPlugin) then
    raise ECNCInstallFault.CreateFmt('GridPlugin "%s" not found in "%s"', [GridPluginName, LibName]);

  GridPlugin.InstallName := Name;
  GridPlugin.Install;
  Grid.Color:= Params.ReadColour('GridColour', clWhite);
end;

procedure TGridPlugin.Installed;
begin
  inherited Installed;
  TagEvent(NameDisplaySweep, EdgeChange, TMethodTag, UpdatePlugin);
  GridPlugin.FinalInstall;
  ReadConfig;
  Grid.OnNudge := Nudge;
end;

procedure TGridPlugin.Nudge(Sender : TObject; Upd : TEditChange; X, Y : Integer);
var I : Integer;
begin
  case TEditChange(Upd) of
    EditChangeUp1: I := 1;
    EditChangeUp2: I := 5;
    EditChangeUp3: I := 20;
    EditChangeDown1: I := -1;
    EditChangeDown2: I := -5;
    EditChangeDown3: I := -20;
  else
    I := 0;
  end;
  GridPlugin.Nudge(X, Y, I);
end;

destructor TGridPlugin.Destroy;
begin
  GridPlugin := nil;
  inherited Destroy;
end;

procedure TGridPlugin.UpdatePlugin(aTag : TAbstractTag);
begin
  try
    GridPlugin.Update;
  except
    on E : Exception do begin
      CNC32.DoCopyOnWriteException(E);
    end;
  end;
end;

procedure TGridPlugin.GridSelectCell(Sender: TObject; ACol, ARow: Longint; var CanSelect: Boolean);
  procedure FindEditCol(Dx : Integer);
  var I : Integer;
  begin
    for I := 1 to Grid.ColCount - 1 do begin
      if (Grid.Col + (I*Dx) >= Grid.ColCount) or
         (Grid.Col + (I*Dx) < 0) then
          Exit;

      if GridPlugin.CanEdit(Grid.Col + (I * Dx), Grid.Row) then begin
        Grid.Col := Grid.Col + (I * Dx);
        Exit;
      end;
    end;
  end;

  procedure FindEditRow(Dy : Integer);
  var I : Integer;
  begin
    for I := 1 to Grid.RowCount - 1 do begin
      if (Grid.Row + (I*Dy) >= Grid.RowCount) or
         (Grid.Row + (I*Dy) < 0) then
          Exit;

      if GridPlugin.CanEdit(Grid.Col, Grid.Row + (I * Dy)) then begin
        Grid.Row := Grid.Row + (I * Dy);
        Exit;
      end;
    end;
  end;

begin
  CanSelect := GridPlugin.CanEdit(ACol, ARow);
  if not CanSelect then begin
    if Grid.Col = ACol - 1 then
      FindEditCol(1)
    else if Grid.Col = ACol + 1 then
      FindEditCol(-1)
    else if Grid.Row = ARow - 1 then
      FindEditRow(1)
    else if Grid.Row = ARow + 1 then
      FindEditRow(-1);
  end;
end;

procedure TGridPlugin.GridDrawCell(Sender : TObject; ACol, ARow : Integer; Rect : TRect; State : TGridDrawState);
  function HighLighted(X, Y : Integer) : Boolean;
  begin
    if (X < 0) or (Y < 0) or (X >= Grid.ColCount) or (Y >= Grid.RowCount) then
      Result := False
    else
      Result := Assigned(Grid.Objects[X, Y]);
  end;
begin
  if SelfDraw then
     GridPlugin.DrawCell(Grid.Canvas.Handle, Rect, ACol, ARow, TGPGridState(State))
  else if HighLighted(ACol, ARow) then begin
    Grid.Canvas.Pen.Color := clRed;
    Grid.Canvas.Pen.Width := 2;
    Grid.Canvas.MoveTo(Rect.Left,  Rect.Top);
    if Highlighted(ACol-1, ARow) then
      Grid.Canvas.MoveTo(Rect.Left, Rect.Bottom)
    else
      Grid.Canvas.LineTo(Rect.Left, Rect.Bottom);

    if Highlighted(ACol, ARow + 1) then
      Grid.Canvas.MoveTo(Rect.Right, Rect.Bottom)
    else
      Grid.Canvas.LineTo(Rect.Right, Rect.Bottom);

    if Highlighted(ACol + 1, ARow) then
      Grid.Canvas.MoveTo(Rect.Right, Rect.Top)
    else
      Grid.Canvas.LineTo(Rect.Right, Rect.Top);

    if HighLighted(ACol, ARow - 1) then
      Grid.Canvas.MoveTo(Rect.Left, Rect.Top)
    else
      Grid.Canvas.LineTo(Rect.Left, Rect.Top);
  end;
end;

procedure TGridPlugin.GridSetEditText(Sender: TObject; ACol, ARow : Integer; const Value : string);
var Buffer : WideString;
begin
  if not Grid.EditorMode then begin
    Buffer := Value;
    GridPlugin.Edit(ACol, ARow, Buffer);
    Grid.Cells[ACol, ARow] := Buffer;
  end;
end;

procedure TGridPlugin.ComboChange(Sender : TObject);
begin
  if Combo.ItemIndex = -1 then
    Combo.ItemIndex := 0;
  GridPlugin.ComboSelect(Combo.ItemIndex);
  if Grid.Visible then
    Grid.SetFocus;
end;

procedure TGridPlugin.InPlaceEditClick(Sender : TObject);
begin
  SysObj.NC.AcquireModeChangeLock;
  try
    if Grid.Visible then
      GridPlugin.InPlaceEdit(Grid.Col, Grid.Row);
  finally
    SysObj.NC.ReleaseModeChangeLock;
  end;
end;

procedure TGridPlugin.IShutdown;
begin
  inherited;
  GridPlugin.Shutdown;
  WriteConfig;
end;

procedure TGridPlugin.ReadConfig;
var S : TQuoteTokenizer;
    Token : string;
  procedure ReadColWidth;
  var I : Integer;
  begin
    I := S.ReadIndex(100);
    if I < Grid.ColCount then
      Grid.ColWidths[I] := S.ReadIntegerAssign;
  end;

begin
  S := TQuoteTokenizer.Create;
  try
    S.Seperator := '=[]{}.';
    S.QuoteChar := '''';
    try
      ReadLive(S, Name);
      S.ExpectedTokens([Name, '=', '{']);
    except
      MessageLog('Configuration file invalid or missing.');
      Exit;
    end;

    Token := LowerCase(S.Read);
    while not S.EndOfStream do begin
      if Token = 'col' then
        ReadColWidth
      else if Token = '}' then
        Exit;
      Token := LowerCase(S.Read);
    end;

  finally
    S.Free;
  end;
end;

procedure TGridPlugin.WriteConfig;
var S : TStringList;
    I : Integer;
begin
  S := TStringList.Create;
  try
    S.Add(Name + ' = {');
    for I := 0 to Grid.ColCount - 1 do begin
      S.Add(Format('Col[%d] = %d', [I, Grid.ColWidths[I]]));
    end;
    S.Add('}');
    WriteLive(S, Name);
  finally
    S.Free;
  end;
end;


procedure TExtStringGrid.CNCMNudge( var Msg: TMessage);
{  procedure IncrementValue(Value : Integer);
  begin
    GridPlugin.Nudge(Grid.Col, Grid.Row, Value);
  end; }

begin
  if Assigned(FOnNudge) then begin
    OnNudge(Self, TEditChange(Msg.WParam), Col, Row);
  end;
end;

initialization
  RegisterCNCClass(TGridPlugin);
end.
