unit Diagnostic;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, CoreCNC, CNCUtils, StdCtrls, IniFiles, Named, CNCTypes, CNC32,
  Term, CNCAbs, Grids, ComCtrls, InterlockEditForm, Tokenizer,
  ImgList, FaultRegistration, KBControl;

type
  TMDITerminal = class(TAbstractDisplay)
  private
    Terminal    : TTerminal;
  protected
    procedure newLine(Sender : TObject; text : string);
  public
    procedure InstallComponent(Params : TParamStrings); override;
    procedure Installed; override;
  end;

  TIODiagnosticPanel = class(TPanel)
  protected
    procedure UpdateObject; virtual; abstract;
  end;

  TIODisplay = class(TIODiagnosticPanel)
  protected
    procedure UpdateObject; override;
  public
  end;

  TIOSubSystemDisplay = class(TIODiagnosticPanel)
  private
    Tab : TTabControl;
    List : TListView;
    FIOSubSystem : TGenericIOSubSystem;
    procedure SetIOSubSystem(aIOSubSystem : TGenericIOSubSystem);
    procedure DoTabChange(Sender : TObject);
  protected
    procedure UpdateObject; override;
  public
    constructor Create(aOwner : TComponent); override;
    property IOSubSystem : TGenericIOSubSystem read FIOSubSystem write SetIOSubSystem;
  end;

  TIODeviceDisplay = class(TIODiagnosticPanel)
  private
    Tab : TTabControl;
    List : TListView;
    FIODevice : TAbstractIODevice;
    procedure SetIODevice(aIODevice : TAbstractIODevice);
    procedure DoTabChange(Sender : TObject);
  protected
    procedure UpdateObject; override;
  public
    constructor Create(aOwner : TComponent); override;
    property IODevice : TAbstractIODevice read FIODevice write SetIODevice;
  end;

  TIOTagDisplay = class(TIODiagnosticPanel)
  private
    Tab : TTabControl;
    List : TListView;
    FIOTag : TAbstractTag;
    InfoPanel, RightPanel : TPanel;
    ConnectCheck : TCheckBox;
    Info : TLabel;
    FOnConnectChange : TNotifyEvent;
    procedure SelectItem(Sender: TObject; Item: TListItem; Selected: Boolean);
    procedure SetIOTag(aIOTag : TAbstractTag);
    procedure DoTabChange(Sender : TObject);
    procedure DoConnectChange(Sender : TObject);
  protected
    procedure UpdateObject; override;
  public
    constructor Create(aOwner : TComponent); override;
    property IOTag : TAbstractTag read FIOTag write SetIOTag;
    property OnConnectChange : TNotifyEvent read FOnConnectChange write FOnConnectChange;
  end;

  TIODiagnostics = class(TExpandDisplay)
  private
  protected
    Tree : TTreeView;
    TagHash : TList;
    DevHash : TList;
    SubSysHash : TList;
    Modified : Boolean;
    FastUpdate : Boolean;
    SystemDisplay : TIOSubSystemDisplay;
    DeviceDisplay : TIODeviceDisplay;
    TagDisplay : TIOTagDisplay;
    IODisplay : TIODisplay;
    ActiveDisplay : TIODiagnosticPanel;
    UpdateCount : Integer;
    TagStateUpdate, DevStateUpdate, SubSysStateUpdate : Integer;

    procedure TreeSelectChange(Sender : TObject; Node : TTreeNode);
    procedure DisplayUpdate(aTag : TAbstractTag);
    procedure SetTagState(aTag : TAbstractTag; Node : TTreeNode);
    procedure SetDeviceState(aDev : TAbstractIODevice; Node : TTreeNode);
    procedure SetSubSystemState(aSub : TGenericIOSubSystem; Node : TTreeNode);
    procedure RefreshMap(aTag : TAbstractTag);
    procedure TreeStartDrag(Sender: TObject; var DragObject: TDragObject);
    procedure ConnectChange(Sender : TObject);
  protected
    procedure Resize; override;
    procedure UserAccessChange(aTag : TAbstractTag); override;
  public
    constructor Create(aOwner : TComponent); override;
    procedure InstallComponent(Params : TParamStrings); override;
    procedure Installed; override;
    procedure WriteMap(aTag : TAbstractTag);
    procedure Open; override;
  end;


  TMemoryDiagnostic = class(TExpandDisplay)
  private
    View : TListView;
    procedure ViewStartDrag(Sender: TObject; var DragObject: TDragObject);
    procedure UpdateDisplay(aTag : TAbstractTag);
    procedure EditInterlock(Sender : TObject);
    procedure ViewClick(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
  public
    procedure Open; override;
    procedure Close; override;
    constructor Create(aOwner : TComponent); override;
    destructor Destroy; override;
    procedure InstallComponent(Params : TParamStrings); override;
    procedure Installed; override;
  end;

  TCustomMemoryDiagnostic = class(TExpandDisplay)
  private
    View : TListView;
    Panel : TPanel;
    Tab : TTabControl;
    procedure OnStartDrag(Sender: TObject; var DragObject: TDragObject);
    procedure OnDragOver(Sender, Source: TObject; X, Y: Integer;
              State: TDragState; var Accept: Boolean);
    procedure OnDragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure EndDrag(Sender, Target: TObject; X, Y: Integer);
    procedure BinDragOver(Sender, Source: TObject; X, Y: Integer;
              State: TDragState; var Accept: Boolean);
    procedure BinDragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure TabChange(Sender : TObject);
    procedure AddTag(aTag : TAbstractTag);
    procedure ReadConfig(S : TStringTokenizer);
    procedure WriteConfig(S : TStringList);
    procedure UpdateDisplay(aTag : TAbstractTag);
    procedure ViewClick(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    function BinaryView(aValue : Integer) : string;
  public
    constructor Create(aOwner : TComponent); override;
    destructor Destroy; override;
    procedure InstallComponent(Params : TParamStrings); override;
    procedure Installed; override;
    procedure CheckInstallation; override;
    procedure IShutdown; override;
  end;

  TPersistentData = class(TExpandDisplay)
  private
    Grid : TStringGrid;
    Panel : TPanel;
    TagList : TList;
    procedure GridSetEditText(Sender: TObject; ACol, ARow : Integer; const Value : string);
    procedure ReadConfiguration;
    procedure WriteConfiguration;
    procedure UpdateTag(ATag: TAbstractTag);
    procedure SavePersistentData(aTag : TAbstractTag);
  public
    constructor Create(aOwner : TComponent); override;
    destructor Destroy; override;
    procedure InstallComponent(Params : TParamStrings); override;
    procedure Installed; override;
    procedure IShutdown; override;
    procedure Open; override;
  end;

  TArrayEditor = class(TExpandDisplay)
  private
    Grid : TStringGrid;
    Data : string;
    A : TArrayTag;
    procedure UpdateData;
    procedure GridSetEditText(Sender: TObject; ACol, ARow : Integer; const Value : string);
    procedure TabChange(Sender : TObject);
  public
    constructor Create(aOwner : TComponent); override;
    destructor Destroy; override;
    procedure InstallComponent(Params : TParamStrings); override;
    procedure Installed; override;
    procedure IShutdown; override;
    procedure CheckInstallation; override;
    procedure Open; override;
  end;

  TMTConnectPublish = class(TCNC)
  private
    ActTag, ComTag, TarTag, FerrTag, AxisTag: TAbstractTag;
    procedure UpdateTags(aTag : TAbstractTag);
  public
    procedure InstallComponent(Params : TParamStrings); override;
    procedure Installed; override;
  end;

implementation

uses
  EditNamedLayerTagForm;

resourcestring
  ChangedAccessOnTag = 'Changed Access on Tag ';
  RemovedAccessOnTag = 'Removed Access on Tag ';
  CustomDiagnosticName = 'CustomDiagnostic';
  DeviceLang = 'Device';
  ConnectionLang = 'Connection';
  DragHereToRemoveLang = 'Drag items on to here to remove';
  BinaryLang = 'Binary';
  PropertyLang = 'Property';


procedure TMDITerminal.newLine(Sender : TObject; text : string);
begin
  SysObj.NC.SetMDIBuffer(text);
  Terminal.AddText(Text);
end;

procedure TMDITerminal.InstallComponent(Params : TParamStrings);
begin
  inherited installComponent(Params);
  Terminal := TTerminal.create(Self);
  Terminal.parent := Self;
  Terminal.align := alClient;
  Terminal.OnNewLine := NewLine;

  __KBC.CreateButton( [kbAlpha, kbNumeric, kbNavigation], Self, alTop);
  Primary     := Terminal.ComboBox;
end;

procedure TMDITerminal.Installed;
begin
  inherited Installed;
  TagEvent(name, edgeChange, TMethodTag, Activate);
end;


function FindInBranch(TN : TTreeNode; This : TObject) : TTreeNode;
var I : Integer;
begin
  for I := 0 to TN.Count - 1 do
    if TObject(TN.Item[I].data) = This then begin
      Result := TN[I];
      Exit;
    end;

  Result := nil;
end;

const
  ioiIOSystem = 0;
  ioiIOSubSystem = 1;
  ioiIODevice = 2;
  ioiOutConnection = 3;
  ioiInConnection = 4;
  ioiStateOK = 5;
  ioiStateNotOk = 6;
  ioiStateDisConnected = 7;
  ioiStateDiagnostic = 8;

constructor TIODiagnostics.Create(aOwner : TComponent);
begin
  inherited Create(aOwner);
  Tree := TTreeView.Create(Self);
  Tree.Align := alLeft;
  Tree.Parent := Self;
  Tree.Images := TImageList.Create(Tree);
  Tree.Images.Width := 16;
  Tree.OnStartDrag := TreeStartDrag;
  Tree.OnChange := TreeSelectChange;

  Tree.Images.ResourceLoad(rtBitMap, 'IO', clWhite);
  Tree.Images.ResourceLoad(rtBitMap, 'BOARD', clWhite);
  Tree.Images.ResourceLoad(rtBitMap, 'PLUG', clWhite);
  Tree.Images.ResourceLoad(rtBitMap, 'OCONNECTION', clWhite);
  Tree.Images.ResourceLoad(rtBitMap, 'ICONNECTION', clWhite);

  Tree.StateImages := Tree.Images;
  Tree.StateImages.Width := 16;

  Tree.StateImages.ResourceLoad(rtBitMap, 'OK', clWhite);
  Tree.StateImages.ResourceLoad(rtBitMap, 'BROKEN', clWhite);
  Tree.StateImages.ResourceLoad(rtBitMap, 'DISCONNECTED', clWhite);
  Tree.StateImages.ResourceLoad(rtBitMap, 'MAINTENANCE', clWhite);

  TagHash := TList.Create;
  DevHash := TList.Create;
  SubSysHash := TList.Create;

  SystemDisplay := TIOSubSystemDisplay.Create(Self);
  SystemDisplay.Parent := Self;
  SystemDisplay.Align := alClient;
  SystemDisplay.Visible := False;

  DeviceDisplay := TIODeviceDisplay.Create(Self);
  DeviceDisplay.Parent := Self;
  DeviceDisplay.Align := alClient;
  DeviceDisplay.Visible := False;

  TagDisplay := TIOTagDisplay.Create(Self);
  TagDisplay.Parent := Self;
  TagDisplay.Align := alClient;
  TagDisplay.Visible := False;
  TagDisplay.OnConnectChange := ConnectChange;

  IODisplay := TIODisplay.Create(Self);
  IODisplay.Parent := Self;
  IODisplay.Align := alClient;
  IODisplay.Visible := False;

  ActiveDisplay := IODisplay;

  ActiveDisplay.Visible := True;
  __KBC.CreateButton( [kbAlpha, kbNumeric, kbNavigation], Self, alTop);
end;

procedure TIODiagnostics.TreeStartDrag(Sender: TObject; var DragObject: TDragObject);
begin
  if Assigned(Tree.Selected.Data) and
     (TObject(Tree.Selected.Data) is TAbstractTag) then begin
    DragObject := TTagDragObject.Create(Self);
    TTagDragObject(DragObject).TTag := TAbstractTag(Tree.Selected.Data);
  end;
end;

procedure TIODiagnostics.ConnectChange(Sender : TObject);
var Node : TTreeNode;
begin
  if Sender = TagDisplay then begin
    Node := Tree.Selected;
    if Node <> nil then begin
      TagDisplay.IOTag.IsConnected := TagDisplay.ConnectCheck.Checked;
      SetTagState(TagDisplay.IOTag, Node);
      Modified := True;
    end;
  end;
end;

procedure TIODiagnostics.Resize;
begin
  inherited Resize;
  Tree.Width := Width div 2;
end;

procedure TIODiagnostics.InstallComponent(Params : TParamStrings);
begin
  inherited InstallComponent(Params);

  FastUpdate := Params.ReadBool('FastUpdate', False);
  ManageEdit := True;
end;

procedure TIODiagnostics.Installed;
begin
  inherited Installed;
  if FastUpdate then
    TagEvent(nameIOSweep, EdgeChange, TIntegerTag, DisplayUpdate)
  else
    TagEvent(nameDisplaySweep,  edgeChange, TIntegerTag, DisplayUpdate);
  TagEvent(Name + 'WriteMap', edgeFalling, TMethodTag, WriteMap);
  TagEvent(Name + 'Refresh', edgeFalling, TMethodTag, RefreshMap);
end;

procedure TIODiagnostics.RefreshMap(aTag : TAbstractTag);
var I : Integer;
    tn, Root : TTreeNode;
begin
  Tree.Items.Clear;
  TagHash.Clear;

  Root := Tree.Items.Add(nil, SysObj.SystemName + ' IO System');
  tn := Root;

  for I := 0 to SysObj.IO.SubSystems.Count - 1 do begin
    tn := Tree.Items.AddChild(Root, TGenericIOSubSystem(SysObj.IO.SubSystems[I]).Name);
    tn.ImageIndex := ioiIOSubSystem;
    tn.SelectedIndex := ioiIOSubSystem;
    tn.StateIndex := ioiStateOK;
    tn.Data := TGenericIOSubSystem(SysObj.IO.SubSystems[I]);
    SubSysHash.Add(tn);
  end;

  for I := 0 to SysObj.IO.Devices.Count - 1 do begin
    tn := FindInBranch(Root, TAbstractIODevice(SysObj.IO.Devices[I]).SubSystem);
    if tn <> nil then begin
      tn := Tree.Items.AddChild(tn, TAbstractIODevice(SysObj.IO.Devices[I]).Name);
      tn.ImageIndex := ioiIODevice;
      tn.SelectedIndex := ioiIODevice;
      tn.StateIndex := ioiStateOK;
      tn.Data := Pointer(SysObj.IO.Devices[I]);
      DevHash.Add(tn);
    end;
  end;

  for I := 0 to SysObj.IO.NamedSize - 1 do begin
    if SysObj.IO.NamedLayer.Tag[I].IsIO and
       (SysObj.IO.NamedLayer.Tag[I] is TIntegerTag) then begin
      with SysObj.IO.NamedLayer.Tag[I] as TIntegerTag do begin
        if Assigned(Device) and not Assigned(AliasedNotUsedYet) then begin
          tn := FindInBranch(Root, Device.SubSystem);
          tn := FindInBranch(tn, Device);
          tn := Tree.Items.AddChild(tn, SysObj.IO.NamedLayer[I]);
          if IsWritable then begin
            tn.ImageIndex := ioiOutConnection;
            tn.SelectedIndex := ioiOutConnection;
            tn.StateIndex := ioiStateOK;
          end else begin
            tn.ImageIndex := ioiInConnection;
            tn.SelectedIndex := ioiInConnection;
            tn.StateIndex := ioiStateOK;
          end;
        end;
        tn.Data := SysObj.IO.NamedLayer.Tag[I];
        TagHash.Add(tn);
      end;
    end;
  end;

  for I := 0 to SysObj.IO.NamedSize - 1 do begin
   if SysObj.IO.NamedLayer.Tag[I].IsIO and
     (SysObj.IO.NamedLayer.Tag[I] is TIntegerTag) then begin
      with SysObj.IO.NamedLayer.Tag[I] as TIntegerTag do begin
         if Assigned(Device) and Assigned(AliasedNotUsedYet) then begin
          tn := FindInBranch(Root, Device.SubSystem);
          tn := FindInBranch(tn, Device);
          tn := FindInBranch(tn, AliasedNotUsedYet);
          tn := Tree.Items.AddChild(tn, SysObj.IO.NamedLayer[I]);
          if IsWritable then begin
            tn.ImageIndex := ioiOutConnection;
            tn.SelectedIndex := ioiOutConnection;
            tn.StateIndex := ioiStateOK;
          end else begin
            tn.ImageIndex := ioiInConnection;
            tn.SelectedIndex := ioiInConnection;
            tn.StateIndex := ioiStateOK;
          end;
        end;
        tn.Data := SysObj.IO.NamedLayer.Tag[I];
        TagHash.Add(tn);
      end;
    end;
  end;
  Tree.Invalidate;
end;

procedure TIODiagnostics.DisplayUpdate(aTag : TAbstractTag);
var Node : TTreeNode;
    I : Integer;
begin
  if Visible then begin
    if ActiveDisplay = TagDisplay then
      ActiveDisplay.UpdateObject
    else begin
      Inc(UpdateCount);
      if(UpdateCount mod 2) = 0 then
        ActiveDisplay.UpdateObject
    end;

    if TagHash.Count > 0 then begin
      for I := 0 to 30 do begin
        TagStateUpdate := TagStateUpdate mod TagHash.Count;
        Node := TTreeNode(TagHash[TagStateUpdate]);
        SetTagState(TIntegerTag(Node.Data), Node);
        Inc(TagStateUpdate);
      end;
    end;

    if DevHash.Count > 0 then begin
      DevStateUpdate := DevStateUpdate mod DevHash.Count;
      Node := TTreeNode(DevHash[DevStateUpdate]);
      SetDeviceState(TAbstractIODevice(Node.Data), Node);
      Inc(DevStateUpdate);
    end;

    if SubSysHash.Count > 0 then begin
      SubSysStateUpdate := SubSysStateUpdate mod SubSysHash.Count;
      Node := TTreeNode(SubSysHash[SubSysStateUpdate]);
      SetSubSystemState(TGenericIOSubSystem(Node.Data), Node);
      Inc(SubSysStateUpdate);
    end;
  end;
end;



procedure TIODiagnostics.WriteMap(aTag : TAbstractTag);
var Stream : TFileStream;
begin
  Stream := TFileStream.Create(LocalPath + '\IOMap.Ini', fmCreate);
  SysObj.IO.WriteIOMap(Stream);
  Stream.Free;
end;


procedure TIODiagnostics.SetTagState(aTag : TAbstractTag; Node : TTreeNode);
var State : Integer;
begin
  if not aTag.OK then
    State := ioiStateNotOK
  else if not aTag.IsConnected then
    State := ioiStateDisConnected
  else
    State := ioiStateOK;

  if Node.StateIndex <> State then
    Node.StateIndex := State;
end;


procedure TIODiagnostics.SetDeviceState(aDev : TAbstractIODevice; Node : TTreeNode);
var State : Integer;
begin
  if not aDev.OK then
    State := ioiStateNotOK
  else
    State := ioiStateOK;

  if State <> Node.StateIndex then
    Node.StateIndex := State;
end;

procedure TIODiagnostics.SetSubSystemState(aSub : TGenericIOSubSystem; Node : TTreeNode);
var State : Integer;
begin
  if not aSub.Healthy then
    State := ioiStateNotOK
  else
    State := ioiStateOK;

  if State <> Node.StateIndex then
    Node.StateIndex := State;
end;

procedure TIODiagnostics.Open;
begin
  if Tree.Items.Count = 0 then begin
    RefreshMap(nil);
  end;

  inherited Open;
end;

procedure TIODiagnostics.TreeSelectChange(Sender : TObject; Node : TTreeNode);
  procedure SetActiveDisplay(aDisplay : TIODiagnosticPanel);
  begin
    if aDisplay <> ActiveDisplay then begin
      IODisplay.Visible := False;
      TagDisplay.Visible := False;
      SystemDisplay.Visible := False;
      DeviceDisplay.Visible := False;
    end;
    ActiveDisplay := aDisplay;
    ActiveDisplay.Visible := True;
  end;

var SelObj : TObject;
begin
  SelObj := TObject(Tree.Selected.Data);
  if SelObj = nil then begin
    SetActiveDisplay(IODisplay);
  end else if SelObj is TMethodTag then begin
    TagDisplay.IOTag := TAbstractTag(SelObj);
    Tree.DragMode := dmAutomatic;
    SetActiveDisplay(TagDisplay);
  end else if SelObj is TGenericIOSubSystem then begin
    SystemDisplay.IOSubSystem := TGenericIOSubSystem(SelObj);
    SetActiveDisplay(SystemDisplay);
    Tree.DragMode := dmManual;
  end else if SelObj is TAbstractIODevice then begin
    DeviceDisplay.IODevice := TAbstractIODevice(SelObj);
    SetActiveDisplay(DeviceDisplay);
    Tree.DragMode := dmManual;
  end else begin
    SetActiveDisplay(IODisplay);
    Tree.DragMode := dmManual;
  end;
end;

procedure TIODiagnostics.UserAccessChange(aTag : TAbstractTag);
var I : Integer;
begin
  if Modified and (AccessLevelMaintenance in SysObj.Comms.AccessLevels) then begin
    for I := 0 to SysObj.IO.NamedSize - 1 do
      with SysObj.IO.NamedLayer.Tag[I] do
        if IsOwned or not IsWritable then
          IsConnected := True;
    Modified := False;
  end;
end;


constructor TMemoryDiagnostic.Create(aOwner : TComponent);
begin
  inherited Create(aOwner);
  View := TListView.Create(Self);
  View.Parent := Self;
  View.DragMode := dmAutomatic;
//  View.HotTrack := True;
//  View.HotTrackStyles := [htHandPoint, htUnderlineHot];
  View.OnMouseDown := ViewClick;
  __KBC.CreateButton( [kbAlpha, kbNumeric, kbNavigation], Self, alTop);
end;

procedure TMemoryDiagnostic.ViewClick(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var LI : TListItem;
begin
  LI := View.Selected;
  if Assigned(LI) and (mbRight = Button) then
    TEditNamedLayerTag.Execute(TAbstractTag(LI.Data));
end;

procedure TCustomMemoryDiagnostic.ViewClick(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var LI : TListItem;
begin
  LI := View.Selected;
  if Assigned(LI) and (mbRight = Button) then
    TEditNamedLayerTag.Execute(TAbstractTag(LI.Data));
end;


destructor TMemoryDiagnostic.Destroy;
begin
  View.Free;
  inherited Destroy;
end;

procedure TMemoryDiagnostic.InstallComponent(Params : TParamStrings);
var lc : TListColumn;
begin
  inherited InstallComponent(Params);
  View.Align := alClient;
  View.ViewStyle := vsReport;
  View.GridLines := True;
  View.OnStartDrag := ViewStartDrag;
  View.OnDblClick := EditInterlock;

  lc := View.Columns.Add;
  lc.Caption := NameLang;
  lc.Width := 200;

  lc := View.Columns.Add;
  lc.Caption := ValueLang;
  lc.Width := 100;

  lc := View.Columns.Add;
  lc.Caption := AliasLang;
  lc.Width := 100;

  lc := View.Columns.Add;
  lc.Caption := OwnerLang;
  lc.Width := 100;

  lc := View.Columns.Add;
  lc.Caption := TypeLang;
  lc.Width := 100;

  lc := View.Columns.Add;
  lc.Caption := SizeLang;
  lc.Width := 100;

  lc := View.Columns.Add;
  lc.Caption := CallbacksLang;
  lc.Width := 100;

  View.Items.Clear;
end;

procedure TMemoryDiagnostic.Installed;
begin
  inherited Installed;
  TagEvent(NameDisplaySweep, edgeChange, TMethodTag, UpdateDisplay);
end;

procedure TMemoryDiagnostic.Open;
var li : TListItem;
    I : Integer;
begin
  View.Items.Clear;
  for I := 0 to SysObj.Comms.NamedSize - 1 do begin
    with SysObj.Comms.NamedLayer.Tag[I] do begin
      LI := View.Items.Add;
      LI.Data := SysObj.Comms.NamedLayer.Tag[I];
      LI.Caption := Name;
      LI.SubItems.Add(AsString);
      if NetWorkAlias = '' then
        LI.SubItems.Add(Name)
      else
        LI.SubItems.Add(NetWorkAlias);

      LI.SubItems.Add(Owner.Name);
      LI.SubItems.Add(ClassName);
      LI.SubItems.Add(IntToStr(DataSize));
      LI.SubItems.Add(IntToStr(Count));
    end;
  end;
  inherited Open;
end;

procedure TMemoryDiagnostic.Close;
begin
  inherited Close;
  View.Items.Clear;
end;

procedure TMemoryDiagnostic.UpdateDisplay(aTag : TAbstractTag);
var I : Integer;
begin
  if Visible then
    if Assigned(View.TopItem) then begin
      for I := View.TopItem.Index to View.VisibleRowCount + View.TopItem.Index - 1 do begin
        with View.Items[I] do begin
          if SubItems[0] <> TAbstractTag(Data).AsShortString then
            SubItems[0] := TAbstractTag(Data).AsShortString;

          if SubItems[4] <> IntToStr(TAbstractTag(Data).Count) then
            SubItems[4] := IntToStr(TAbstractTag(Data).Count)
        end;
      end;
    end;
end;

procedure TMemoryDiagnostic.ViewStartDrag(Sender: TObject; var DragObject: TDragObject);
begin
  DragObject := TTagDragObject.Create(Self);
  TTagDragObject(DragObject).TTag := TAbstractTag(View.ItemFocused.Data);
end;

procedure TMemoryDiagnostic.EditInterlock(Sender : TObject);
var AccFrm : TInterlockEditFrm;
    ThisTag : TAbstractTag;
begin
  if Visible and Assigned(View.ItemFocused) then begin
    SysObj.NC.AcquireModeChangeLock;
    try
      AccFrm := TInterlockEditFrm.Create(Application);
      try
        ThisTag := TAbstractTag(View.ItemFocused.Data);
        if ThisTag.Owner = SysObj.Comms then begin
          if AccessLevelAdministrator in SysObj.Comms.AccessLevels then begin
            AccFrm.AccessGroup.Enabled := True;
            AccFrm.OkBtn.Enabled := True;
            AccFrm.RemoveBtn.Enabled := True;
          end;

          if AccessLevelOEM1 in SysObj.Comms.AccessLevels then begin
            AccFrm.StatusGroup.Enabled := True;
            AccFrm.ModesGroup.Enabled := True;
            AccFrm.ActiveSpeedBtn.Enabled := True;
          end;

        end else if AccessLevelOEM2 in SysObj.Comms.AccessLevels then begin
          AccFrm.AccessGroup.Enabled := True;
          AccFrm.StatusGroup.Enabled := True;
          AccFrm.ModesGroup.Enabled := True;
          AccFrm.ActiveSpeedBtn.Enabled := True;
        end;

        case AccFrm.Execute(ThisTag) of
          mrOK : begin
            ThisTag.WriteAccessToRegistry(AccFrm.Interlock);
            EventLog(ChangedAccessOnTag + ThisTag.Name);
          end;

          mrNo : begin
            ThisTag.ClearAccessFromRegistry;
            EventLog(RemovedAccessOnTag + ThisTag.Name);
          end;
        end;
      finally
        AccFrm.Free;
      end;
    finally
      SysObj.NC.ReleaseModeChangeLock;
    end;
  end;
end;


constructor TCustomMemoryDiagnostic.Create(aOwner : TComponent);
var lc : TListColumn;
begin
  inherited;
  Panel := TPanel.Create(Self);
  Panel.Parent := Self;
  Panel.Caption := DragHereToRemoveLang;
  Panel.Height := 20;
  Panel.Align := alBottom;
  Panel.OnDragDrop := BinDragDrop;
  Panel.OnDragOver := BinDragOver;
  Panel.BevelOuter := bvLowered;

  Tab := TTabControl.Create(Self);
  Tab.Parent := Self;
  Tab.Align := alClient;
  Tab.OnChange := TabChange;
  Tab.MultiLine := True;

  View := TListView.Create(Self);
  View.Parent := Tab;
  View.Align := alClient;
  View.ViewStyle := vsReport;
  View.GridLines := True;
  View.OnDragDrop := OnDragDrop;
  View.OnDragOver := OnDragOver;
  View.OnStartDrag := OnStartDrag;
  View.HotTrack := True;
  View.HotTrackStyles := [htHandPoint, htUnderlineHot];
  View.OnMouseDown := ViewClick;
  OnEndDrag := EndDrag;
  View.DragMode := dmAutomatic;
  lc := View.Columns.Add;
  lc.Caption := NameLang;
  lc.Width := 100;

  lc := View.Columns.Add;
  lc.Caption := ValueLang;
  lc.Width := 100;

  lc := View.Columns.Add;
  lc.Caption := AliasLang;
  lc.Width := 100;

  lc := View.Columns.Add;
  lc.Caption := DeviceLang;
  lc.Width := 100;

  lc := View.Columns.Add;
  lc.Caption := ConnectionLang;
  lc.Width := 100;

  lc := View.Columns.Add;
  lc.Caption := BinaryLang;
  lc.Width := 100;
end;

destructor TCustomMemoryDiagnostic.Destroy;
begin
  inherited;
end;

procedure TCustomMemoryDiagnostic.Installed;
begin
  inherited;
  Tab.TabHeight := 25;
  TagEvent(NameDisplaySweep, EdgeChange, TMethodTag, UpdateDisplay);
end;

procedure TCustomMemoryDiagnostic.CheckInstallation;
var S : TStringTokenizer;
begin
  inherited;

  S := TStringTokenizer.Create;
  try
    try
      ReadLive(S, Name);
    except
      Tab.Tabs.AddObject('1', TList.Create);
      Tab.Tabs.AddObject('2', TList.Create);
      Exit;
    end;
    ReadConfig(S);
  finally
    S.Free;
    Tab.TabIndex := 0;
    TabChange(Self);
  end;
end;


procedure TCustomMemoryDiagnostic.UpdateDisplay(aTag : TAbstractTag);
var I : Integer;
    List : TList;
begin
  try
    if Visible and (Tab.TabIndex <> -1) then begin
      List := TList(Tab.Tabs.Objects[Tab.TabIndex]);
      for I := 0 to List.Count - 1 do begin
        with View.Items[I] do begin
          if SubItems[0] <> TAbstractTag(List[I]).AsString then begin
            SubItems[0] := TAbstractTag(List[I]).AsString;
            if TAbstractTag(List[I]).IsIO then
              SubItems[4] := BinaryView(TAbstractTag(List[I]).AsInteger);
          end;
        end;
      end;
    end;
  except
    on E : Exception do
      SysObj.FaultInterface.SetFault(Self, CoreFaults[cfSysWarning], ['TCustomMemoryDiagnostic raised ' + E.Message], nil);
  end;
end;


procedure TCustomMemoryDiagnostic.InstallComponent(Params : TParamStrings);
begin
  inherited InstallComponent(Params);
end;

procedure TCustomMemoryDiagnostic.IShutdown;
var S : TStringList;
begin
  S := TStringList.Create;
  try
    WriteConfig(S);
    WriteLive(S, Name);
  finally
    S.Free;
  end;
  inherited IShutdown;
end;

procedure TCustomMemoryDiagnostic.WriteConfig(S : TStringList);
var I, J : Integer;
    List : TList;
begin
  S.Add(ClassName + ' = {');
  for I := 0 to 3 do begin
    S.Add(Format('  Width[%d] = %d', [I, View.Columns.Items[I].Width]));
  end;

  for I := 0 to Tab.Tabs.Count - 1 do begin
    S.Add(Format('  Page[%s] = {', [Tab.Tabs[I]]));
    List := TList(Tab.Tabs.Objects[I]);
    for J := 0 to List.Count - 1 do begin
      S.Add('    ' + TAbstractTag(List[J]).Name);
    end;
    S.Add('  }');
  end;
  S.Add('}');
end;

procedure TCustomMemoryDiagnostic.ReadConfig(S : TStringTokenizer);
  procedure ReadPage;
  var Token : string;
      aTag : TAbstractTag;
      List : TList;
  begin
    S.ExpectedToken('[');
    List := TList.Create;
    Tab.Tabs.AddObject(S.Read, List);
    S.ExpectedToken(']');
    S.ExpectedToken('=');
    S.ExpectedToken('{');
    Token := S.Read;
    while not S.EndOfStream do begin
      if Token = '}' then
        Exit;

      aTag := CoreCNC.SysObj.Comms.FindTag(Token);
      if aTag <> nil then
        List.Add(aTag);
      Token := S.Read;
    end;
  end;

var
  aToken : string;
  I : Integer;
begin
  S.Seperator := '[]{}=';

  S.ExpectedTokens([ClassName, '=', '{']);
  aToken := LowerCase(S.Read);
  while not S.EndOfStream do begin
    if aToken = 'width' then begin
      I := S.ReadIndex(4);
      View.Columns.Items[I].Width := S.ReadIntegerAssign;
    end
    else if aToken = 'page' then begin
      ReadPage;
    end
    else if aToken = '}' then
      Exit;
    aToken := LowerCase(S.Read);
  end;
end;

procedure TCustomMemoryDiagnostic.TabChange(Sender : TObject);
var I : Integer;
    List : TList;
begin
  View.Items.Clear;
  List := TList(Tab.Tabs.Objects[Tab.TabIndex]);
  for I := 0 to List.Count - 1 do begin
    AddTag(TAbstractTag(List[I]));
  end;
end;

function TCustomMemoryDiagnostic.BinaryView(aValue : Integer) : string;
var I : Integer;
begin
  Result := '';
  for I := 0 to 31 do begin
    Result := IntToStr(Ord((aValue and 1) <> 0)) + Result;
    aValue := aValue shr 1;
    if aValue = 0 then
      Break;
  end;
end;

procedure TCustomMemoryDiagnostic.AddTag(aTag : TAbstractTag);
var li : TListItem;
    TmpStr : string;
begin
  li := View.Items.Add;
  Li.Caption := aTag.Name;
  Li.Data := aTag;

  Li.SubItems.Add(aTag.AsString);
  if aTag.NetWorkAlias = '' then
    Li.SubItems.Add(aTag.Name)
  else
    Li.SubItems.Add(aTag.NetWorkAlias);
    
  if aTag.IsIO then begin
    with aTag as TIntegerTag do begin
      Li.SubItems.Add(Device.Name);
      if IsWritable then
        TmpStr := 'Q'
      else
        TmpStr := 'X';
      Li.SubItems.Add(TmpStr + IntToStr(Address) + '.' + IntToStr(Mask));
      Li.SubItems.Add(BinaryView(AsInteger));
    end;
  end;
end;

procedure TCustomMemoryDiagnostic.EndDrag(Sender, Target: TObject; X, Y: Integer);
var aTag : TAbstractTag;
    List : TList;
    I : Integer;
begin
  if(View.ItemFocused <> nil) and (Target = Panel) then begin
    aTag := TAbstractTag(View.ItemFocused.Data);
    List := TList(Tab.Tabs.Objects[Tab.TabIndex]);
    I := List.IndexOf(aTag);
    if I <> -1 then
      List.Delete(I);
    View.Items.Delete(View.ItemFocused.Index);
  end;
end;

procedure TCustomMemoryDiagnostic.OnStartDrag(Sender: TObject; var DragObject: TDragObject);
begin
  DragObject := TTagDragObject.Create(Self);
  TTagDragObject(DragObject).TTag := TAbstractTag(View.ItemFocused.Data);
end;

procedure TCustomMemoryDiagnostic.BinDragOver(Sender, Source: TObject; X, Y: Integer;
          State: TDragState; var Accept: Boolean);
begin
  if not (Source is TTagDragObject) then
    Accept := False;
end;

procedure TCustomMemoryDiagnostic.BinDragDrop(Sender, Source: TObject; X, Y: Integer);
begin
end;

procedure TCustomMemoryDiagnostic.OnDragOver(Sender, Source: TObject; X, Y: Integer;
  State: TDragState; var Accept: Boolean);
begin
  if not (Source is TTagDragObject) then
    Accept := False;
end;

procedure TCustomMemoryDiagnostic.OnDragDrop(Sender, Source: TObject; X, Y: Integer);
var I : Integer;
begin
  with Source as TTagDragObject do begin
    if TTag <> nil then begin
      for I := 0 to View.Items.Count - 1 do begin
        if TTag = View.Items[I].Data then
          Exit;
      end;
      TList(Tab.Tabs.Objects[Tab.TabIndex]).Add(TTag);
      AddTag(TTag);
    end;
  end;
end;

constructor TIOSubSystemDisplay.Create(aOwner : TComponent);
var Lc : TListColumn;
begin
  inherited Create(aOwner);
  Tab := TTabControl.Create(Self);
  Tab.Parent := Self;
  Tab.Align := alClient;
  Tab.OnChange := DoTabChange;

  List := TListView.Create(Self);
  List.Parent := Tab;
  List.Align := alClient;
  List.GridLines := True;
  List.ViewStyle := vsReport;

  Lc := List.Columns.Add;
  Lc.Caption := PropertyLang;
  Lc.Width := 150;

  Lc := List.Columns.Add;
  Lc.Caption := ValueLang;
  Lc.Width := 150;
end;

procedure TIOSubSystemDisplay.DoTabChange(Sender : TObject);
var I : Integer;
    S : TStringList;
    LI : TListItem;
begin
  if (Tab.TabIndex <> -1) and (IOSubSystem <> nil) then begin
    S := TStringList.Create;
    List.Items.BeginUpdate;
    try
      List.Items.Clear;
      S.Text := IOSubSystem.InfoPageContent[Tab.TabIndex];
      for I := 0 to (S.Count div 2) - 1 do begin
        LI := List.Items.Add;
        LI.Caption := S[I * 2];
        LI.SubItems.Add(S[(I * 2) + 1]);
      end;
    finally
      List.Items.EndUpdate;
      S.Free;
    end;
  end;
end;

procedure TIOSubSystemDisplay.SetIOSubSystem(aIOSubSystem : TGenericIOSubSystem);
begin
  FIOSubSystem := aIOSubSystem;
  if FIOSubSystem <> nil then begin
    Tab.Tabs.Clear;
    Tab.Tabs.AddStrings(FIOSubSystem.InfoPages);
    Tab.TabIndex := 0;
    DoTabChange(Self);
  end;
end;

procedure TIOSubSystemDisplay.UpdateObject;
var I : Integer;
    S : TStringList;
    LI : TListItem;
begin
  if (Tab.TabIndex <> -1) and (IOSubSystem <> nil) then begin
    S := TStringList.Create;
    try
      S.Text := IOSubSystem.InfoPageContent[Tab.TabIndex];
      for I := 0 to (S.Count div 2) - 1 do begin
        LI := List.Items[I];
        if LI.Caption <> S[I*2] then
          LI.Caption := S[I * 2];
        if LI.SubItems[0] <> S[(I*2)+1] then
          LI.SubItems[0] := S[(I * 2) + 1];
      end;
    finally
      S.Free;
    end;
  end;
end;

constructor TIODeviceDisplay.Create(aOwner : TComponent);
var Lc : TListColumn;
begin
  inherited Create(aOwner);
  Tab := TTabControl.Create(Self);
  Tab.Parent := Self;
  Tab.Align := alClient;
  Tab.OnChange := DoTabChange;

  List := TListView.Create(Self);
  List.Parent := Tab;
  List.Align := alClient;
  List.GridLines := True;
  List.ViewStyle := vsReport;

  Lc := List.Columns.Add;
  Lc.Caption := PropertyLang;
  Lc.Width := 150;

  Lc := List.Columns.Add;
  Lc.Caption := ValueLang;
  Lc.Width := 150;
end;


procedure TIODeviceDisplay.SetIODevice(aIODevice : TAbstractIODevice);
begin
  FIODevice := aIODevice;
  if FIODevice <> nil then begin
    Tab.Tabs.Clear;
    Tab.Tabs.AddStrings(FIODevice.InfoPages);
    Tab.TabIndex := 0;
    DoTabChange(Self);
  end;
end;

procedure TIODeviceDisplay.DoTabChange(Sender : TObject);
var I : Integer;
    S : TStringList;
    LI : TListItem;
begin
  if (Tab.TabIndex <> -1) and (IODevice <> nil) then begin
    S := TStringList.Create;
    List.Items.BeginUpdate;
    try
      List.Items.Clear;
      S.Text := IODevice.InfoPageContent[Tab.TabIndex];
      for I := 0 to (S.Count div 2) - 1 do begin
        LI := List.Items.Add;
        LI.Caption := S[I * 2];
        LI.SubItems.Add(S[(I * 2) + 1]);
      end;
    finally
      List.Items.EndUpdate;
      S.Free;
    end;
  end;
end;

procedure TIODeviceDisplay.UpdateObject;
var I : Integer;
    S : TStringList;
    LI : TListItem;
begin
  if (Tab.TabIndex <> -1) and (IODevice <> nil) then begin
    S := TStringList.Create;
    try
      S.Text := IODevice.InfoPageContent[Tab.TabIndex];
      for I := 0 to (S.Count div 2) - 1 do begin
        LI := List.Items[I];
        if LI.Caption <> S[I * 2] then
          LI.Caption := S[I * 2];
        if LI.SubItems[0] <> S[(I * 2) + 1] then
          LI.SubItems[0] := S[(I * 2) + 1];
      end;
    finally
      S.Free;
    end;
  end;
end;


constructor TIOTagDisplay.Create(aOwner : TComponent);
var Lc : TListColumn;
begin
  inherited Create(aOwner);
  Tab := TTabControl.Create(Self);
  Tab.Parent := Self;
  Tab.Align := alClient;
  Tab.OnChange := DoTabChange;

  RightPanel := TPanel.Create(Self);
  RightPanel.Parent := Self;
  RightPanel.Align := alClient;

  InfoPanel := TPanel.Create(Self);
  InfoPanel.Parent := RightPanel;
  InfoPanel.Height := 30;
  InfoPanel.Align := alTop;

  ConnectCheck := TCheckBox.Create(Self);
  ConnectCheck.Parent := InfoPanel;
  ConnectCheck.Left := 10;
  ConnectCheck.Top := 5;
  ConnectCheck.Caption := 'Connected';
  ConnectCheck.OnClick := DoConnectChange;

  Info := TLabel.Create(Self);
  Info.Parent := InfoPanel;
  Info.Left := 150;
  Info.Top := 5;


  List := TListView.Create(Self);
  List.Parent := RightPanel;
  List.Align := alClient;
  List.GridLines := True;
  List.ViewStyle := vsReport;
  List.HotTrackStyles := [htHandPoint, htUnderlineHot];
  List.OnSelectItem := SelectItem;

  Lc := List.Columns.Add;
  Lc.Caption := 'Property';
  Lc.Width := 150;

  Lc := List.Columns.Add;
  Lc.Caption := 'Value';
  Lc.Width := 150;
end;

procedure TIOTagDisplay.SelectItem(Sender: TObject; Item: TListItem; Selected: Boolean);
var Obj : TObject;
    aTag : TAbstractTag;
begin
  if Selected then begin
    Obj := Item.Data;
    if Obj is TAbstractTag then begin
      aTag := TAbstractTag(Obj);
      if not aTag.IsConnected then begin
        if Item.Index > 0 then begin
          if (aTag.AsInteger and (1 shl (Item.Index - 1))) <> 0 then
             aTag.AsInteger := aTag.AsInteger and not (1 shl (Item.Index - 1))
          else
             aTag.AsInteger := aTag.AsInteger or (1 shl (Item.Index - 1));
          if aTag.IsWritable then
            TIntegerTag(aTag).WriteToDevice;
          List.Selected := nil;
        end;
      end;
    end;
  end;
end;

procedure TIOTagDisplay.SetIOTag(aIOTag : TAbstractTag);
begin
  FIOTag := aIOTag;
  if FIOTag <> nil then begin
    Tab.Tabs.Clear;
    Tab.Tabs.Add('Info');
    Tab.TabIndex := 0;
    ConnectCheck.Checked := FIOTag.IsConnected;

    Info.Caption := FIOTag.Name;

    DoTabChange(Self);
  end;
end;

procedure TIOTagDisplay.DoConnectChange(Sender : TObject);
begin
  if Assigned(FOnConnectChange) then
    FOnConnectChange(Self);
end;

procedure TIOTagDisplay.DoTabChange(Sender : TObject);
var I : Integer;
    LI : TListItem;
    Tmp : Integer;
begin
  if (Tab.TabIndex <> -1) and (IOTag <> nil) then begin
    List.Items.BeginUpdate;
    try
      List.Items.Clear;
      LI := List.Items.Add;
      LI.Caption := 'Value';
      LI.Data := IOTag;
      Tmp := IOTag.AsInteger;
      LI.SubItems.Add(IntToStr(Tmp));
      for I := 0 to 31 do begin
        LI := List.Items.Add;
        LI.Caption := 'Bit.' + IntToStr(I);
        LI.SubItems.Add(BooleanIdent[(Tmp and (1 shl I)) <> 0]);
        LI.Data := IOTag;
      end;
    finally
      List.Items.EndUpdate;
    end;
  end;
end;

procedure TIOTagDisplay.UpdateObject;
var LI : TListItem;
    Tmp, I : Integer;
    TmpStr : string;
begin
  if (Tab.TabIndex <> -1) and (IOTag <> nil) then begin
    try
      LI := List.Items[0];
      Tmp := IOTag.AsInteger;
      if LI.SubItems[0] <> IntToStr(Tmp) then begin
        LI.SubItems[0] := (IntToStr(Tmp));
        LI.Checked := IOTag.IsConnected;
      end;
      for I := 0 to 31 do begin
        LI := List.Items[I+1];
        TmpStr := BooleanIdent[(Tmp and (1 shl I)) <> 0];
        if(LI.SubItems[0] <> TmpStr) then
          LI.SubItems[0] := TmpStr;
      end;
    finally
    end;
  end;
end;

procedure TIODisplay.UpdateObject;
begin
end;


{ TPersistentData }

constructor TPersistentData.Create(aOwner: TComponent);
begin
  inherited;
  TagList := TList.Create;

  Panel := TPanel.Create(Self);
  Panel.Parent := Self;
  Panel.Align := alTop;
  Panel.Caption := 'Persistent Data';

  Grid := TStringGrid.Create(Self);
  Grid.Parent := Self;
  Grid.Align := alClient;
  Grid.Options := Grid.Options + [goColSizing, goEditing, goThumbTracking];
  Grid.OnSetEditText := GridSetEditText;
  Grid.ColCount := 2;
  Grid.RowCount := 50;
  Grid.FixedRows := 1;
  Grid.FixedCols := 1;
  Grid.Cells[0, 0] := 'Title';
  Grid.Cells[1, 0] := 'Value';
  __KBC.CreateButton( [kbAlpha, kbNumeric, kbNavigation], Self, alTop);
end;

destructor TPersistentData.Destroy;
begin
  Grid.Free;
  Panel.Free;
  inherited;
end;

type
  TDeferedTag = class(TObject)
    TagName, TagType, Value, Desc : string;
    Owned : Boolean;
    Tag : TAbstractTag;
    TC : TTagClass;
    Kill : Boolean;
    Token : string;
  end;


procedure TPersistentData.GridSetEditText(Sender: TObject; ACol,
  ARow: Integer; const Value: string);
var D : TDeferedTag;
begin
  if not Grid.EditorMode then
    if Assigned(Grid.Objects[ACol, ARow]) then begin
      D := TDeferedTag(Grid.Objects[ACol, ARow]);
      if not D.Kill then begin
        D.Tag.AsString := Value;
        Grid.Cells[ACol, ARow] := D.Tag.AsString
      end;
    end;
end;

procedure TPersistentData.InstallComponent(Params: TParamStrings);
var I : Integer;
    D : TDeferedTag;
begin
  inherited;
  FMachineSpecificData := Params.ReadBool('MachineSpecific', True);
  try
    ReadConfiguration;
  except
    on E : Exception do
      MessageLogFmt('%s Failed to load configuration because [%s]', [Name, E.Message]);
  end;

  Grid.RowCount := TagList.Count + 1;

  for I := 0 to TagList.Count - 1 do begin
    D := TDeferedTag(TagList[I]);
    if D.Owned then begin
      D.TC := FindTagClass(D.TagType);
      if (D.TagName <> '') and
         (D.TagType <> '') and
         (D.TC <> nil) then begin

        D.Tag := TagPublish(D.TagName, D.TC);
        D.Tag.AsString := D.Value;

        Grid.Objects[1, I + 1] := D;
        Grid.Cells[1, I + 1] := D.Tag.AsString;
        if D.Desc <> '' then
         Grid.Cells[0, I + 1] := D.Desc
        else
         Grid.Cells[0, I + 1] := D.Tag.Name;
      end else begin
        Grid.Objects[1, I + 1] := D;
        Grid.Cells[0, I] := 'Failed';
        D.Kill := True;
      end;
    end;
  end;
end;

procedure TPersistentData.SavePersistentData(aTag : TAbstractTag);
begin
  WriteConfiguration;
  //Sysobj.FaultInterface.SetFault(Self, CoreFaults[cfPersistentSaved], [Name], nil);
end;

procedure TPersistentData.UpdateTag(ATag: TAbstractTag);
var I : Integer;
    D : TDeferedTag;
begin
  for I := 0 to TagList.Count - 1 do begin
    D := TDeferedTag(TagList[I]);
    if D.Tag = ATag then begin
      Grid.Cells[1, I + 1] := D.Tag.AsString;
    end;
  end;
end;

procedure TPersistentData.Installed;
var I : Integer;
    D : TDeferedTag;
begin
  inherited;

  TagEvent(NameReqSavePersistent, EdgeFalling, TMethodTag, SavePersistentData);

  for I := 0 to TagList.Count - 1 do begin
    D := TDeferedTag(TagList[I]);
    if not D.Owned then begin
      D.TC := FindTagClass(D.TagType);
      if (D.TagName <> '') and
         (D.TagType <> '') and
         (D.TC <> nil) then begin
        D.Tag := TagEvent(D.TagName, EdgeChange, D.TC, UpdateTag);
        D.Tag.AsString := D.Value;

        Grid.Objects[1, I + 1] := D;
        Grid.Cells[1, I + 1] := D.Tag.AsString;
        if D.Desc <> '' then
          Grid.Cells[0, I + 1] := D.Desc
        else
          Grid.Cells[0, I + 1] := D.Tag.Name;
      end else begin
        Grid.Objects[1, I + 1] := D;
        Grid.Cells[0, I] := 'Failed';
        D.Kill := True;
      end;
    end;
  end;
end;

procedure TPersistentData.ReadConfiguration;
var S : TQuoteTokenizer;
    Token : string;

  procedure ReadWidth;
  var I : Integer;
  begin
    I := S.ReadIndex(2);
    Grid.ColWidths[I] := S.ReadIntegerAssign;
  end;

  procedure ReadTag;
  var D : TDeferedTag;

    procedure ReadDescription(const T : string);
    begin
      D.Token := T;
      D.Desc := SysObj.Localized.GetString(T);
    end;

  begin
    S.ExpectedTokens(['=', '{']);
    Token := LowerCase(S.Read);
    D := TDeferedTag.Create;
    D.Owned := True;
    TagList.Add(D);
    try
      while not S.EndOfStream do begin
        if Token = 'name' then
          D.TagName := S.ReadStringAssign
        else if Token = 'type' then
          D.TagType := S.ReadStringAssign
        else if Token = 'value' then
          D.Value := S.ReadStringAssign
        else if Token = 'description' then
          ReadDescription(S.ReadStringAssign)
        else if Token = 'owned' then
          D.Owned := S.ReadBooleanAssign
        else if Token = '}' then
          Exit
        else
          raise Exception.CreateFmt('Unexpected token [%s]', [Token]);
        Token := LowerCase(S.Read);
      end;
    finally
    end;
  end;

begin
  inherited;

  S := TQuoteTokenizer.Create;
  try
    ReadLive(S, Name);
    if S.Count = 0 then
      Exit;
    S.Seperator := '[]{}=';
    S.QuoteChar := '''';
    S.ExpectedTokens([Name, '=', '{']);
    Token := LowerCase(S.Read);
    while not S.EndOfStream do begin
      if Token = 'tag' then
        ReadTag
      else if Token = 'width' then
        ReadWidth
      else if Token = '}' then
        Exit
      else
        raise Exception.CreateFmt('Unexpected token [%s]', [Token]);
      Token := LowerCase(S.Read);
    end;
  finally
    S.Free;
  end;
end;

procedure TPersistentData.IShutdown;
begin
  inherited;
  WriteConfiguration;
end;

procedure TPersistentData.Open;
var I : Integer;
    D : TDeferedTag;
begin
  inherited;
  for I := 0 to TagList.Count - 1 do begin
    D := TDeferedTag(TagList[I]);
    Grid.Cells[1, I + 1] := D.Tag.AsString;
  end;
end;

procedure TPersistentData.WriteConfiguration;
var S : TStringList;
    I : Integer;

  procedure WriteTag(Defered : TDeferedTag);
  begin
    if not Defered.Kill then begin
      S.Add('  Tag = {');
      S.Add('    Name = ' + Defered.Tag.Name);
      S.Add('    Value = ''' + Defered.Tag.AsString + '''');
      S.Add('    Type = ' + Defered.Tag.ClassName);
      S.Add('    Description = ''' + Defered.Token + '''');
      S.Add('    Owned = ' + BooleanEnum[Defered.Owned]);
      S.Add('  }');
    end;
  end;

begin
  S := TStringList.Create;
  try
    S.Add(Name + ' = {');
    S.Add(Format('  Width[%d] = %d', [0, Grid.ColWidths[0]]));
    S.Add(Format('  Width[%d] = %d', [1, Grid.ColWidths[1]]));
    for I := 1 to Grid.RowCount - 1 do begin
      if Assigned(Grid.Objects[1, I]) then
        WriteTag(TDeferedTag(Grid.Objects[1, I]));
    end;
    S.Add('}');
    WriteLive(S, Name);
  finally
    S.Free;
  end;
end;

{ TArrayEditor }

procedure TArrayEditor.CheckInstallation;
var aTag : TAbstractTag;
begin
  inherited;
  if Data <> '' then begin
    aTag := SysObj.Comms.FindTag(Data);
    if aTag is TArrayTag then begin
      A := TArrayTag(aTag);
      if A.ItemClass <> TDoubleTag then
        A := nil;
    end;
  end;
end;

constructor TArrayEditor.Create(aOwner: TComponent);
begin
  inherited;
  Grid := TStringGrid.Create(Self);
  Grid.Parent := Self;
  Grid.Align := alClient;
  Grid.OnSetEditText := GridSetEditText;
  Grid.DefaultColWidth := 110;
  Grid.ParentFont := True;
  Grid.Options := Grid.Options + [goEditing];
  __KBC.CreateButton( [kbNumeric, kbNavigation], Self, alTop);
end;

destructor TArrayEditor.Destroy;
begin
  if Assigned(Grid) then
    Grid.Free;
  inherited;
end;

procedure TArrayEditor.GridSetEditText(Sender: TObject; ACol,
  ARow: Integer; const Value: string);
begin
  if Grid.Cells[ACol, ARow] <> '' then begin
    try
      A.SetDoubleValue([ARow - 1, ACol - 1], StrToFloat(Grid.Cells[ACol, ARow]))
    except
      // Do nothing
    end;
//    Grid.Cells[ACol, ARow] := Format('%g', [A.GetDoubleValue([ACol - 1, ARow - 1])]);
  end;
end;

procedure TArrayEditor.InstallComponent(Params: TParamStrings);
begin
  inherited;

  Data := Params.ReadString('Array', '');
end;

procedure TArrayEditor.Installed;
begin
  inherited;
end;

procedure TArrayEditor.IShutdown;
begin
  inherited;

end;

procedure TArrayEditor.Open;
begin
  inherited;
  TabChange(Self);
end;

procedure TArrayEditor.UpdateData;
var I, J : Integer;
begin
  if Assigned(A) then
    for I := 0 to A.Dimensions[0] - 1 do
      for J := 0 to A.Dimensions[1] - 1 do
        if Grid.Cells[J + 1, I + 1] <> Format('%g', [A.GetDoubleValue([I, J])]) then
          Grid.Cells[J + 1, I + 1] := Format('%g', [A.GetDoubleValue([I, J])]);
end;

procedure TArrayEditor.TabChange(Sender: TObject);
  procedure SetGrid(A, B : Integer; Titles : array of string);
  var I : Integer;
  begin
    Grid.RowCount := B;
    Grid.ColCount := A;

    for I := 0 to High(Titles) do
      Grid.Cells[I, 0] := Titles[I];

    if A > 1 then
      Grid.FixedCols := 1;
    if B > 1 then
      Grid.FixedRows := 1;
  end;

  procedure FirstColGrid;
  var I, W, Largest : Integer;
  begin
    if Grid.RowCount < 2 then
      Grid.RowCount := 2;
      
    Largest := 0;
    for I := 0 to Grid.RowCount - 1 do begin
      W := Grid.Canvas.TextWidth(Grid.Cells[0, I]);
      if W > Largest then
        Largest := W;
    end;
    Grid.ColWidths[0] := Largest + 12;
    for I := 1 to Grid.ColCount - 1 do begin
      Grid.ColWidths[I] := (Grid.Width - Grid.ColWidths[0] - 25) div (Grid.ColCount - 1)
    end;
  end;
var I : Integer;
begin
  if Assigned(A) then begin
    SetGrid(A.Dimensions[1] + 1, A.Dimensions[0] + 1, ['Name', 'Data']);
    if A.Dimensions[1] > 1 then begin
      for I := 0 to A.Dimensions[0] - 1 do
        Grid.Cells[0, I + 1] := A.Name + '[' + IntToStr(I);
      for I := 0 to A.Dimensions[1] -1  do
        Grid.Cells[I + 1, 0] := ',' + IntToStr(I) + ']';
    end else begin
      for I := 0 to A.Dimensions[0] - 1 do
        Grid.Cells[0, I + 1] := A.Name + '[' + IntToStr(I) + ']';
      Grid.Cells[1, 0] := 'Data';
    end;
    FirstColGrid;
  end;
  UpdateData;
end;

{ TMTConnectPublish }

procedure TMTConnectPublish.InstallComponent(Params: TParamStrings);
begin
  inherited;
  ActTag:= TagPublish(Name + 'ActualPosition', TStringTag);
  ComTag:= TagPublish(Name + 'CommandedPosition', TStringTag);
  TarTag:= TagPublish(Name + 'TargetPosition', TStringTag);
  FerrTag:= TagPublish(Name + 'FollowError', TStringTag);
  AxisTag:= TagPublish(Name + 'AxisNames', TStringTag);
end;

procedure TMTConnectPublish.Installed;
var N: string;
    I: Integer;
begin
  inherited;
  TagEvent(nameDisplaySweep, edgeChange, TMethodTag, UpdateTags);
  N:= '';
  for I:= 0 to SysObj.NC.AxisCount - 1 do begin
    N:= N + SysObj.NC.AxisName[I];
  end;
  AxisTag.AsString:= N;
end;

procedure TMTConnectPublish.UpdateTags(aTag: TAbstractTag);
var I: Integer;
    com, tar, act, ferr: string;
    ap: TAxisPosition;
    comma: string;
begin
  com:= '';
  tar:= '';
  act:= '';
  ferr:= '';
  comma:= '';
  for I:= 0 to SysObj.NC.AxisCount - 1 do begin
    ap := SysObj.NC.AxisPosition[I];
    com := com + comma;
    com := com + Format('%.3f', [ap[ptCommanded]]);
    act := act + comma;
    act := act + Format('%.3f', [ap[ptDisplay]]);
    tar := tar + comma;
    tar := tar + Format('%.3f', [ap[ptTarget]]);
    ferr := ferr + comma;
    ferr := ferr + Format('%.3f', [ap[ptFerr]]);
    comma:= ',';
  end;
  ActTag.AsString:= act;
  TarTag.AsString:= tar;
  ComTag.AsString:= com;
  FerrTag.AsString:= ferr;
end;

initialization
  RegisterCNCClass(TIODiagnostics);
  RegisterCNCClass(TMDITerminal);
  RegisterCNCClass(TMemoryDiagnostic);
  RegisterCNCClass(TCustomMemoryDiagnostic);
//  RegisterCNCClass(TFlowSystemDiagnostics);
  RegisterCNCClass(TPersistentData);
  RegisterCNCClass(TArrayEditor);
  RegisterCNCClass(TMTConnectPublish, 'MTConnectPublish');
end.


