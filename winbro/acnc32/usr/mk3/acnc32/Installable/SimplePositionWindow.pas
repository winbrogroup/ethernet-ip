unit SimplePositionWindow;

interface

uses CoreCNC, CNCTypes, CNC32, Classes, Windows, Graphics, SysUtils, NCNames,
     CNCAbs, Named, Controls;

type
  TSimplePositionWindow = class(TExpandDisplay)
  private
    IsInstalled : Boolean;
    PositionList : TList;
    RolloverSupressList : array [0..10] of Boolean;
    BackHighRect : TRect;

    FontTable : array [0..72] of TPoint;
    LargeFontSize, SmallFontSize : Integer;
    FPositionColor : TColor;
    FHighlightColor : TColor;
    LeftPos1 : Integer;
    LeftPos2 : Integer;
    MidHeight : Integer;
    Values : array [0..15, TPositionType] of Double;
    ForceRedraw : Boolean;
    RequestAxis : TAbstractTag;
    AutoAxisSelectEnableTag : TAbstractTag;
    AutoSelectedAxisNumberTag : TAbstractTag;
    ResetCompleteTag          : TAbstractTag;
    AutoAxisSelectEnabled : Boolean;
    Last : TPoint;
    Disabled : Boolean;
    NDC : Boolean;
    GGroups : TArrayTag;
    CharactersDisplayed : Integer;
    CurrentMode : Integer;
    LeftBorder : Integer;
    SelectorWidth : Integer;
    TopBorder : Integer;
    SelectorXOffset : Integer;
    FName : String;
    LastSelAxis : Integer;
    ShowSelection : Boolean;
    ShowAutoSelection : Boolean;

    SelectorColor : TColor;
    HighBackColor : TColor;
    DoubleClickSelect : Boolean;

    procedure DrawDisplayPosition;
    function FindFontSize(aWidth, aHeight : Integer) : Integer;
    function NumFormat(X, Y : Integer;
                       ReqAN : Boolean;
                       Index : Integer;
                       pt : TPositionType;
                       RolloverSuppress : Boolean;
                       isHighLighted : Boolean) : String;

    procedure UpdateDisplay(aTag : TAbstractTag);
    procedure AxisSelectChange(aTag : TAbstractTag);
    procedure DoMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure DoAxisSelect(Sender: TObject);
    function GetHighLightColor : TColor;
    function GetPositionColor : TColor;
    procedure DisableDisplay(aTag : TAbstractTag);
    procedure PaintSelector(X, Y, SelWidth: Integer);
    procedure DoAxisSelectOnUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure AutoEnableTagChanged(aTag: TAbstractTag);
    procedure AutoAxisNumberChanged(aTag : TAbstractTag);
    procedure ResetRewindAction(aTag: TAbstractTag);
  protected
    procedure Resize; override;
    procedure Paint; override;
  public
    constructor Create(aOwner : TComponent); override;
    procedure InstallComponent(Params : TParamStrings); override;
    procedure Installed; override;
    procedure Repaint; override;
    property PositionColor : TColor read GetPositionColor write FPositionColor;
    property HighlightColor : TColor read GetHighLightColor write FHighLightColor;
  end;

implementation

const
  LeftFrame = 100;
  RightFrame = 100 - LeftFrame;
  CharactersDisplayedMM = 13;
  CharactersDisplayedIN = 14;


constructor TSimplePositionWindow.Create(aOwner : TComponent);
begin
  inherited Create(aOwner);
  LastSelAxis := -1;
  PositionList := TList.Create;
  CharactersDisplayed := CharactersDisplayedMM;
end;

procedure TSimplePositionWindow.InstallComponent(Params : TParamStrings);
var I : Integer;
SupInt : INteger;
begin
  inherited InstallComponent(Params);
  ParentFont := False;
  Font.Style := [];
  FName := Params.ReadString('FontName', 'Courier New Bold');
  Font.Name := FName;
  Canvas.Font.Assign(Font);
  BevelWidth := Params.ReadInteger('BevelWidth',0);
  ShowSelection := Params.ReadBool('ShowSelection',False);
  ShowAutoSelection := Params.ReadBool('ShowAutoSelection',False);

  SelectorColor := Params.ReadColour('SelectorColour',clGreen);
  DoubleClickSelect := Params.ReadBool('DoubleClickSelect',False);
  SelectorXOffset := Params.ReadInteger('SelectorXOffset',0);
  HighBackColor   := Params.ReadInteger('HighlightBackColour',0);

  OnMouseDown := DoMouseDown;
  NDC := Params.ReadBool('NDC', False);
  if not NDC then
    if DoubleClickSelect then
      OnDblClick := DoAxisSelect
    else
      OnMouseUp := DoAxisSelectOnUp;

  for I := 0 to SysObj.NC.AxisCount - 1 do begin
    if I <10 then
      begin
      SupInt := Params.ReadInteger(Format('SuppressRolloverAxis%d',[I]),0);
      RolloverSupressList[I] := SupInt  = 1;
      end;
  end;

  for I := 1 to 72 do begin
    Canvas.Font.Height := I;
    FontTable[I].Y := Canvas.TextHeight('0');
    FontTable[I].X := Canvas.TextWidth('0');
  end;

  FPositionColor := Params.ReadColour('PositionColour', clBlack);
  FHighLightColor := Params.ReadColour('HighlightColor', clBlue);
  LeftBorder := Params.ReadInteger('LeftBorder', 10);
  TopBorder := Params.ReadInteger('TopBorder', 10);
  if (ShowSelection or ShowAutoSelection) then
    SelectorWidth := (Width) div 8
  else
    SelectorWidth := 0;

   with NCEventF[ncdeReqAxisSelect] do
     RequestAxis := SysObj.Comms.TagPublish(Format(Tag, [1]), Size);

end;


procedure TSimplePositionWindow.AutoEnableTagChanged(aTag : TAbstractTag);
begin
  AutoAxisSelectEnabled :=   (AutoAxisSelectEnableTag.AsInteger <> 0);
  ForceRedraw := True;
end;

procedure TSimplePositionWindow.AutoAxisNumberChanged(aTag : TAbstractTag);
begin
  ForceRedraw := True;
end;

procedure TSimplePositionWindow.ResetRewindAction(aTag : TAbstractTag);
var
FSBuffer : Integer;
begin
AutoSelectedAxisNumberTag.AsInteger := -1;
AutoAxisSelectEnableTag.AsInteger := 0;
end;

procedure TSimplePositionWindow.DoMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  Last.X := X;
  Last.Y := Y;
end;

procedure TSimplePositionWindow.DoAxisSelectOnUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  if ShowSelection or (ShowAutoSelection and AutoAxisSelectEnabled) then
  RequestAxis.AsInteger := ((Last.Y - TopBorder) * SysObj.NC.AxisCount) div (Height - TopBorder);
  if (ShowAutoSelection and AutoAxisSelectEnabled) then
    AutoSelectedAxisNumberTag.AsInteger := RequestAxis.AsInteger;

end;

procedure TSimplePositionWindow.DoAxisSelect(Sender: TObject);
begin
  RequestAxis.AsInteger := ((Last.Y - TopBorder) * SysObj.NC.AxisCount) div (Height - TopBorder);
end;

function TSimplePositionWindow.GetHighLightColor : TColor;
begin
  if Disabled then
    Result := clGray
  else
    Result := FHighLightColor;
end;

function TSimplePositionWindow.GetPositionColor : TColor;
begin
  if Disabled then
    Result := clGray
  else
    Result := FPositionColor;
end;


procedure TSimplePositionWindow.Installed;
var I : Integer;
begin
  inherited Installed;
  for I := 0 to SysObj.NC.AxisCount - 1 do begin
    PositionList.Add(TagEvent(Format(AxisPositionFormat, [SysObj.NC.AxisName[I]]), EdgeChange, TAxisPositionTag, nil));
  end;
  TagEvent(NameDisplaySweep, EdgeChange, TMethodTag, UpdateDisplay);
  with NCPublishedF[ncpActiveAxis] do
    TagEvent(Format(Tag, [1]), EdgeChange, Size, AxisSelectChange);

  TagEvent(Name + 'Disable', EdgeChange, TMethodTag, DisableDisplay);
  GGroups := TArrayTag(TagEvent(GGroupName, EdgeChange, TArrayTag, nil));
  AutoAxisSelectEnableTag :=TagEvent('EnableAutoAxisSelect',        edgeChange, TIntegerTag, AutoEnableTagChanged);
  AutoSelectedAxisNumberTag :=TagEvent('AutoSelectedAxisNumber',        edgeChange, TIntegerTag, AutoAxisNumberChanged);
//  AutoSelectedAxisNumberTag:= SysObj.Comms.TagPublish('AutoSelectedAxisNumber',TIntegerTag); // read only from part prog
  AutoSelectedAxisNumberTag.AsInteger := -1;
  AutoAxisSelectEnabled := False;
  ResetCompleteTag := TagEvent('ResetRewind', EdgeChange, TMethodTag, ResetRewindAction);

end;

procedure TSimplePositionWindow.DisableDisplay(aTag : TAbstractTag);
begin
  Disabled := aTag.AsBoolean;
  ForceRedraw := True;
  Repaint;
end;

procedure TSimplePositionWindow.AxisSelectChange(aTag : TAbstractTag);
begin
  ForceRedraw := True;
  Repaint;
end;

procedure TSimplePositionWindow.UpdateDisplay(aTag : TAbstractTag);
begin
  try
  if CurrentMode <> GGroups.AsArrayInteger[7] then
    begin
    CurrentMode := GGroups.AsArrayInteger[7];
    case CurrentMode of
      21 : CharactersDisplayed := CharactersDisplayedMM;
      20 : CharactersDisplayed := CharactersDisplayedIN;
    end;
    Resize;
    ForceRedraw := True;
    Invalidate;
    end
  else
    begin
    DrawDisplayPosition;
    end;
  except
    EventLog('Failed to UpdateDisplay in Simple Position Window');
  end;
end;

function TSimplePositionWindow.FindFontSize(aWidth, aHeight : Integer) : Integer;
begin
  for Result := 72 downto 1 do begin
    if (aWidth >= FontTable[Result].X) and (aHeight >= FontTable[Result].Y) then
      Exit;
  end;
  Result := 1;
end;

procedure TSimplePositionWindow.Resize;
begin
  try
  Canvas.Brush.Color := Color;
  Canvas.Brush.Style := bsSolid;
  Canvas.Pen.Color := Color;
  Canvas.Rectangle(BevelWidth, BevelWidth, Width - BevelWidth * 2, Height - BevelWidth * 2);
  LargeFontSize := FindFontSize(
                (Width -LeftBorder-SelectorWidth) div (CharactersDisplayed),
                (Height - TopBorder) div SysObj.NC.AxisCount);
  SmallFontSize := FindFontSize(
                (Width -LeftBorder-SelectorWidth) div (2 * CharactersDisplayed),
                (Height - 50) div (SysObj.NC.AxisCount * 2));

  LeftPos1 := (Width * LeftFrame) div 100 + 5;
  LeftPos2 := LeftPos1 + ((Width - LeftPos1) div 2);
  MidHeight := Height div 2;
  except
    EventLog('Failed Resize in SimplePositionWindow');
  end;
end;

procedure TSimplePositionWindow.PaintSelector(X, Y,  SelWidth: Integer);
var
SelectorRect : TRect;
BrushColorBuffer : TColor;
HighlightPos : TPoint;
begin
BrushColorBuffer := Canvas.Brush.Color;
try
try

Canvas.Pen.Color := clBlack;
SelectorRect.Top := Y-(SelWidth div 2);
SelectorRect.Bottom := Y+(SelWidth div 2);
SelectorRect.Left := X-(SelWidth div 2);
SelectorRect.Right := X+(SelWidth div 2);
Canvas.Ellipse(SelectorRect);
Canvas.Brush.Color := SelectorColor;
Canvas.FloodFill(x,y,clBlack,fsBorder);
HighlightPos.x := (X-SElwidth div 4+1);
HighlightPos.Y := (Y-SElwidth div 4+1);
SelectorRect.Top :=HighlightPos.Y-2;
SelectorRect.Bottom :=HighlightPos.Y+2;
SelectorRect.Left :=HighlightPos.X-2;
SelectorRect.Right :=HighlightPos.X+2;
Canvas.Pen.Color := clYellow;
Canvas.Ellipse(SelectorRect);
Canvas.Brush.Color := clYellow;
Canvas.FloodFill(HighlightPos.x,HighlightPos.y,clYellow,fsBorder);
except
  EventLog('Exception Painting Poswindow Selector');
end;
finally
Canvas.Brush.Color := BrushColorBuffer;
end;
//Canvas.TextOut(1,1,Format('%d,%d',[X,Y]));
end;

function TSimplePositionWindow.NumFormat(X, Y : Integer;
                                         ReqAN : Boolean;
                                         Index : Integer;
                                         pt : TPositionType;
                                         RolloverSuppress : Boolean;
                                         isHighLighted : Boolean) : String;
var TmpStr : string;
    Value : Double;
    NumTextExtents : TSize;
    BufferFS : Integer;
begin
  Result := 'Error';
//  CurrentMode := 0;
  try
  if ForceRedraw or (Values[Index, pt] <> TAxisPositionTag(PositionList[Index]).FValue[pt]) then begin

    Values[Index, pt] := TAxisPositionTag(PositionList[Index]).FValue[pt];
    if ReqAN then
      TmpStr := SysObj.NC.AxisName[Index] + ''
    else
      TmpStr := '';

    Value := Values[Index, pt];

    if Value < 0 then begin
      TmpStr := TmpStr + '-'
    end else begin
      TmpStr := TmpStr + '+'
    end;

    Value := Abs(Value);
    if RolloverSuppress then
      begin
      if (Value >=359.9985) and (Value < 360.0015) then Value := 0.0;
      end;

    case CurrentMode of
      21 : begin
        if SysObj.NC.AxisType[Index] = atLinear then
          FmtStr(Result, '%s%8.3fmm', [TmpStr, Value])
        else
          FmtStr(Result, '%s%8.3fdg', [TmpStr, Value]);
        Canvas.TextOut(X, Y, Result);
      end;

      20 : begin
        if SysObj.NC.AxisType[Index] = atLinear then
          FmtStr(Result, '%s%8.4fIn', [TmpStr, Value])
        else
          FmtStr(Result, '%s%8.3fdg', [TmpStr, Value]);
        Canvas.TextOut(X, Y, Result);
      end;
    end; // case
  if Result <> '' then
  begin
  if isHighLighted then
    Canvas.Brush.Color := HighBackColor
  else
  Canvas.Brush.Color := Color;
  NumTextExtents := Canvas.TextExtent(Result);
  BackHighRect.Left := LeftBorder+SelectorWidth+NumTextExtents.cx;
  BackHighRect.Right := Width-1;
  Canvas.FillRect(BackHighRect);
  end;
  end;
  except
    EventLog('Failed to Format Number in SimplePositionWindow');
  end;

end;

procedure TSimplePositionWindow.DrawDisplayPosition;
var I,
Seperator : Integer;
YPos        : INteger;
TExtents    : TSize;
NumTextExtents : TSize;
FontGap : Integer;
HighlightIndex : Integer;
begin
  if ShowSelection then
    HighlightIndex := SysObj.NC.ActiveAxis
  else if ShowAutoSelection and AutoAxisSelectEnabled then
    HighlightIndex := AutoSelectedAxisNumberTag.AsInteger
  else
    HighlightIndex := -1;
    if assigned(Canvas) then
      begin
      with Canvas do
      begin
      try
      Font.Color := PositionColor;
      Font.Height := LargeFontSize;
      Font.Style := [];
      Seperator := (Height - TopBorder) div SysObj.NC.AxisCount;
      TExtents := TextExtent('0');
      except
      EventLog('Error accessing Canvas 1');
      end;
      for I := 0 to SysObj.NC.AxisCount - 1 do
        begin
        try
        BackHighRect.Top := TopBorder+(I * Seperator);
        BackHighRect.Bottom := BackHighRect.Top+ TExtents.cy;
        BackHighRect.Left := 2;
        BackHighRect.Right := LeftBorder+SelectorWidth;
        except
          EventLog(Format('Error Drawing Posn start of Loop %d',[I]));
        end;
        if (HighlightIndex = I)  then
          begin
          Font.Color := HighlightColor;
//          Canvas.Brush.Color := HighBackColor;
          Brush.Color := HighBackColor;
          if (I <> LastSelAxis) then // or (ForceRedraw) then
            begin
            try
            Brush.Color := HighBackColor;
            FillRect(BackHighRect);
            YPos := TopBorder + (I * Seperator)+(TExtents.cy div 2);
            PaintSelector(LeftBorder+(SelectorWidth div 2)+SelectorXOffset, YPos,SelectorWidth div 3 *2);
            LastSelAxis := I;
            except
             EventLog(Format('Error Drawing Non Highlighted Posn Inside Loop %d',[I]));
            end
            end;
          NumFormat(LeftBorder+SelectorWidth, TopBorder + I * Seperator, True, I, ptDisplay,RolloverSupressList[I],True);
          end
        else
          begin
          try
          Brush.Color := Color;
          FillRect(BackHighRect);
          Font.Color := PositionColor;
          except
           EventLog(Format('Error Drawing Non Highlighted Posn Inside Loop %d',[I]));
          end;
          NumFormat(LeftBorder+SelectorWidth, TopBorder + I * Seperator, True, I, ptDisplay,RolloverSupressList[I],False);
          end;
        Brush.Color := Color;
        end; // For loop
      end;
      Font.Color := PositionColor;
      Font.Height := SmallFontSize;
      Font.Style := [];
    end
  else
    begin
    EventLog('Canvas Does not exist in Position Window');
    end
end;

procedure TSimplePositionWindow.Paint;
begin
  inherited;
  if assigned(Canvas) then
    begin
    try
    Canvas.Font.Name := FName;
    Canvas.Brush.Style := bsSolid;
    Canvas.Brush.Color := Color;
    Canvas.Font.Height := SmallFontSize;
    ForceRedraw := True;
    LastSelAxis := -1;
    if InstallState >= InstallStateDone  then
      DrawDisplayPosition;
    ForceRedraw := False;
    except
      EventLog('Failed Paint in Simple Position Window');
    end;
    end
  else
    begin
      EventLog('Canvas Not Assigned in Simple Position Window Paint procedure');
    end;
end;

procedure TSimplePositionWindow.Repaint;
begin
  inherited;
end;

initialization
  RegisterCNCClass(TSimplePositionWindow);
end.
