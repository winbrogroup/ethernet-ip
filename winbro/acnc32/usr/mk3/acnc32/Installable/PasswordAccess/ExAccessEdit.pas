unit ExAccessEdit;
  { This unit is the form that gets displayed when the "Access Admin" button is pressed
    It allows the addition of passwords for various levels of access to be created. }
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, CheckLst, Buttons, ExtCtrls, SimpleTapReader, CNCTypes,
  AccessEditForm, CoreCNC;

type
  TAccessesEditFrm = class(TForm)
    Panel1: TPanel;
    OKBtn: TBitBtn;
    CancelBtn: TBitBtn;
    RemoveBtn: TBitBtn;
    AddBtn: TBitBtn;
    Panel2: TPanel;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    AccessListBox: TCheckListBox;
    UserNameList: TListBox;
    procedure FormCreate(Sender: TObject);
    procedure OKBtnClick(Sender: TObject);
    procedure RemoveBtnClick(Sender: TObject);
    procedure AddBtnClick(Sender: TObject);
    procedure UserNameListClick(Sender: TObject);
    procedure AccessListBoxClickCheck(Sender: TObject);
  private
    FileName : string;
    FSecurityFile : TSecurityFile;
    AccessRecord : TAccessRecord;
    procedure SetSecurityFile(aSecurityFile : TSecurityFile);
    function SecurityCheck(aText : string) : Boolean;
    procedure DisplayAccessLevels(AccessLevels : TAccessLevels);
    procedure SetAccessLevels(var AccessLevels : TAccessLevels);
  public
    class procedure Execute(aFileName : string); overload;
    property SecurityFile : TSecurityFile read FSecurityFile write SetSecurityFile;
  end;

var
  AccessesEditFrm: TAccessesEditFrm;

implementation

{$R *.DFM}

class procedure TAccessesEditFrm.Execute(aFileName : string);
var
  AccEdit : TAccessesEditFrm;
  SecurityFile : TSecurityFile;
begin
  SecurityFile := TSecurityFile.Create(True);
  SecurityFile.LoadFromFile(aFileName);

  AccEdit := TAccessesEditFrm.Create(Application);

  try
    AccEdit.FileName := aFileName;
    AccEdit.SecurityFile := SecurityFile;
    AccEdit.ShowModal;
  finally
    AccEdit.Release;
  end;
end;


procedure TAccessesEditFrm.OKBtnClick(Sender: TObject);
begin
  SecurityFile.SaveToFile(FileName);
end;

procedure TAccessesEditFrm.RemoveBtnClick(Sender: TObject);
begin
  if UserNameList.ItemIndex <> -1 then
    SecurityFile.DeleteAccess(UserNameList.ItemIndex);

  SecurityFile := SecurityFile;
end;

procedure TAccessesEditFrm.AddBtnClick(Sender: TObject);
var aText : string;
begin
  if InputQuery( SysObj.Localized.GetString('$NEW_USER'),
                 SysObj.Localized.GetString('$ENTER_USER_NAME'), aText) then begin
    if aText <> '' then begin
      AccessRecord.UserName := aText;
      AccessRecord.AccessLevels := [AccessLevelOperator];
      AccessRecord.PassWord := '';
      AccessRecord.CardID := '';
      AccessRecord.ClockID := '';
    end;
    if TSimpleTapForm.Execute(Format(SysObj.Localized.GetString('$ENTER_S_SECURITY_PASSWORD'),
      [AccessRecord.UserName]), SecurityCheck) then begin
      aText := AccessRecord.CardID;

      if TSimpleTapForm.Execute(
           Format(SysObj.Localized.GetString('$ENTER_S_SECURITY_PASSWORD_AGAIN'),
          [AccessRecord.UserName]), SecurityCheck) then begin
        if aText = AccessRecord.CardID then begin
          SecurityFile.AddAccessRecord(AccessRecord);
          SecurityFile := SecurityFile;
        end;
      end;
    end;
  end;
end;

procedure TAccessesEditFrm.SetSecurityFile(aSecurityFile : TSecurityFile);
var I : Integer;
begin
  FSecurityFile := aSecurityFile;
  UserNameList.Items.Clear;
  for I := 0 to SecurityFile.Count - 1 do begin
    UserNameList.Items.Add(SecurityFile.Access[I]^.UserName);
  end;

  UserNameList.ItemIndex := 0;

  if UserNameList.Items.Count > 0 then begin
    UserNameList.ItemIndex := 0;
    DisplayAccessLevels(SecurityFile.Access[0]^.AccessLevels);
  end;
end;


function TAccessesEditFrm.SecurityCheck(aText : string) : Boolean;
begin
  AccessRecord.CardID := aText;
  Result := True;
end;


procedure TAccessesEditFrm.UserNameListClick(Sender: TObject);
begin
  if UserNameList.ItemIndex <> -1 then begin
    if UserNameList.ItemIndex < SecurityFile.Count then begin
      DisplayAccessLevels(SecurityFile.Access[UserNameList.ItemIndex]^.AccessLevels);
    end;
  end;
end;

procedure TAccessesEditFrm.DisplayAccessLevels(AccessLevels : TAccessLevels);
begin
  AccessListBox.Checked[0] := AccessLevelNone in AccessLevels;
  AccessListBox.Checked[1] := AccessLevelOperator in AccessLevels;
  AccessListBox.Checked[2] := AccessLevelSupervisor in AccessLevels;
  AccessListBox.Checked[3] := AccessLevelProgrammer in AccessLevels;
  AccessListBox.Checked[4] := AccessLevelMaintenance in AccessLevels;
  AccessListBox.Checked[5] := AccessLevelAdministrator in AccessLevels;
  AccessListBox.Checked[6] := AccessLevelOEM1 in AccessLevels;
end;

procedure TAccessesEditFrm.SetAccessLevels(var AccessLevels : TAccessLevels);
  procedure SetLevel(A : TAccessLevel; aOn : Boolean);
  begin
    if aOn then
      Include(AccessLevels, A)
    else
      Exclude(AccessLevels, A);
  end;
begin
  SetLevel(AccessLevelNone, AccessListBox.Checked[0]);
  SetLevel(AccessLevelOperator, AccessListBox.Checked[1]);
  SetLevel(AccessLevelSupervisor, AccessListBox.Checked[2]);
  SetLevel(AccessLevelProgrammer, AccessListBox.Checked[3]);
  SetLevel(AccessLevelMaintenance, AccessListBox.Checked[4]);
  SetLevel(AccessLevelAdministrator, AccessListBox.Checked[5]);
  SetLevel(AccessLevelOEM1, AccessListBox.Checked[6]);
end;

procedure TAccessesEditFrm.AccessListBoxClickCheck(Sender: TObject);
begin
  if UserNameList.ItemIndex <> -1 then
    SetAccessLevels(SecurityFile.Access[UserNameList.ItemIndex]^.AccessLevels);
end;

procedure TAccessesEditFrm.FormCreate(Sender: TObject);
begin
  Caption:= Sysobj.Localized.GetString('$CONFIG_ACCESS');
  OKBtn.Caption:= Sysobj.Localized.GetString('$OK');
  CancelBtn.Caption:= Sysobj.Localized.GetString('$CANCEL');
  RemoveBtn.Caption:= Sysobj.Localized.GetString('$REMOVE');
  AddBtn.Caption:= Sysobj.Localized.GetString('$ADD');

  AccessListBox.Clear;
  AccessListBox.Items.Add(Sysobj.Localized.GetString('$ACCESS_NONE'));
  AccessListBox.Items.Add(Sysobj.Localized.GetString('$ACCESS_OPERATOR'));
  AccessListBox.Items.Add(Sysobj.Localized.GetString('$ACCESS_SUPERVISOR'));
  AccessListBox.Items.Add(Sysobj.Localized.GetString('$ACCESS_PROGRAMMER'));
  AccessListBox.Items.Add(Sysobj.Localized.GetString('$ACCESS_MAINTENANCE'));
  AccessListBox.Items.Add(Sysobj.Localized.GetString('$ACCESS_ADMINISTRATOR'));
  AccessListBox.Items.Add(Sysobj.Localized.GetString('$ACCESS_OEM1'));
end;

end.
