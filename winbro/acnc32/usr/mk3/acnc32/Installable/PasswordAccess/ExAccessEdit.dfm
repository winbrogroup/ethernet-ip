object AccessesEditFrm: TAccessesEditFrm
  Left = 200
  Top = 443
  BorderStyle = bsSingle
  Caption = 'Configure Access'
  ClientHeight = 409
  ClientWidth = 457
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -16
  Font.Name = 'Verdana'
  Font.Style = [fsBold]
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 18
  object Panel1: TPanel
    Left = 0
    Top = 368
    Width = 457
    Height = 41
    Align = alBottom
    BevelOuter = bvLowered
    TabOrder = 0
    object OKBtn: TBitBtn
      Left = 8
      Top = 6
      Width = 100
      Height = 30
      TabOrder = 0
      OnClick = OKBtnClick
      Kind = bkOK
    end
    object CancelBtn: TBitBtn
      Left = 112
      Top = 6
      Width = 100
      Height = 30
      TabOrder = 1
      Kind = bkCancel
    end
    object RemoveBtn: TBitBtn
      Left = 216
      Top = 6
      Width = 100
      Height = 30
      Cancel = True
      Caption = '&Remove'
      TabOrder = 2
      OnClick = RemoveBtnClick
      Glyph.Data = {
        DE010000424DDE01000000000000760000002800000024000000120000000100
        0400000000006801000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        3333333333333FFFFF333333000033333388888833333333333F888888FFF333
        000033338811111188333333338833FFF388FF33000033381119999111833333
        38F338888F338FF30000339119933331111833338F388333383338F300003391
        13333381111833338F8F3333833F38F3000039118333381119118338F38F3338
        33F8F38F000039183333811193918338F8F333833F838F8F0000391833381119
        33918338F8F33833F8338F8F000039183381119333918338F8F3833F83338F8F
        000039183811193333918338F8F833F83333838F000039118111933339118338
        F3833F83333833830000339111193333391833338F33F8333FF838F300003391
        11833338111833338F338FFFF883F83300003339111888811183333338FF3888
        83FF83330000333399111111993333333388FFFFFF8833330000333333999999
        3333333333338888883333330000333333333333333333333333333333333333
        0000}
      NumGlyphs = 2
    end
    object AddBtn: TBitBtn
      Left = 320
      Top = 6
      Width = 100
      Height = 30
      Caption = 'Add'
      TabOrder = 3
      OnClick = AddBtnClick
      Glyph.Data = {
        DE010000424DDE01000000000000760000002800000024000000120000000100
        0400000000006801000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00344446333334
        44433333FFFF333333FFFF33000033AAA43333332A4333338833F33333883F33
        00003332A46333332A4333333383F33333383F3300003332A2433336A6633333
        33833F333383F33300003333AA463362A433333333383F333833F33300003333
        6AA4462A46333333333833FF833F33330000333332AA22246333333333338333
        33F3333300003333336AAA22646333333333383333F8FF33000033444466AA43
        6A43333338FFF8833F383F330000336AA246A2436A43333338833F833F383F33
        000033336A24AA442A433333333833F33FF83F330000333333A2AA2AA4333333
        333383333333F3330000333333322AAA4333333333333833333F333300003333
        333322A4333333333333338333F333330000333333344A433333333333333338
        3F333333000033333336A24333333333333333833F333333000033333336AA43
        33333333333333833F3333330000333333336663333333333333333888333333
        0000}
      NumGlyphs = 2
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 457
    Height = 368
    Align = alClient
    BevelOuter = bvLowered
    TabOrder = 1
    object GroupBox1: TGroupBox
      Left = 1
      Top = 1
      Width = 270
      Height = 366
      Align = alClient
      Caption = 'Users'
      TabOrder = 0
      object UserNameList: TListBox
        Left = 2
        Top = 20
        Width = 266
        Height = 344
        Align = alClient
        ItemHeight = 18
        TabOrder = 0
        OnClick = UserNameListClick
      end
    end
    object GroupBox2: TGroupBox
      Left = 271
      Top = 1
      Width = 185
      Height = 366
      Align = alRight
      Caption = 'Access'
      TabOrder = 1
      object AccessListBox: TCheckListBox
        Left = 2
        Top = 20
        Width = 181
        Height = 344
        OnClickCheck = AccessListBoxClickCheck
        Align = alClient
        ItemHeight = 18
        Items.Strings = (
          'None'
          'Operator'
          'Supervisor'
          'Programmer'
          'Maintenance'
          'Administrator'
          'OEM1')
        TabOrder = 0
      end
    end
  end
end
