unit SimpleTapReader;
  { This unit is the button display that is shown when the password id required to be entered.  }
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, Mask, ExtCtrls, CNC32, CoreCNC;

type
  TSimpleTapForm = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    Panel3: TPanel;
    Panel4: TPanel;
    Panel5: TPanel;
    Panel6: TPanel;
    Panel7: TPanel;
    Panel8: TPanel;
    Panel9: TPanel;
    Panel10: TPanel;
    PromptLabel: TLabel;
    MaskEdit: TMaskEdit;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    procedure Panel1MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Panel1MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FormCreate(Sender: TObject);
  private
//    Password : string;
  public
    constructor Create(aOwner : TComponent); override;
    class function Execute(const Prompt : string; aSecurityCheck : TSecurityCheck) : Boolean;
  end;

var
  SimpleTapForm: TSimpleTapForm;

implementation

{$R *.DFM}


class function TSimpleTapForm.Execute(const Prompt : string; aSecurityCheck : TSecurityCheck) : Boolean;
var Form : TSimpleTapForm;
    Res : TModalResult;
    Resp : string;
begin
  Form := TSimpleTapForm.Create(Application);
  Form.MaskEdit.PasswordChar := '*';
  SysObj.NC.AcquireModeChangeLock;
  try
    Form.PromptLabel.Caption := Prompt;
    repeat
      Form.MaskEdit.Text := '';
      res := Form.ShowModal;
      if (res = mrOk) then begin
        Resp := Form.MaskEdit.Text;
        if Assigned(aSecurityCheck) then begin
          if not aSecurityCheck(Resp) then begin
            Form.PromptLabel.Caption := SysObj.Localized.GetString('$INVALID_ENTRY_TRY_AGAIN');
            res := mrNone;
          end;
        end;
      end;
    until ((res = mrOK) or (res = mrCancel));
    Result := res = mrOK;
  finally
    SysObj.NC.ReleaseModeChangeLock;
    Form.Free;
  end;
end;


constructor TSimpleTapForm.Create(aOwner : TComponent);
begin
  inherited;
  Font.Assign(Sysobj.VisualParent.Font);
  Font.Size:= 14;
  PromptLabel.Caption := SysObj.Localized.GetString('$ENTER_ACCESS_SECURITY_CODE');
  BitBtn1.Caption := SysObj.Localized.GetString('$CANCEL');
  BitBtn2.Caption := SysObj.Localized.GetString('$OK');
end;

procedure TSimpleTapForm.FormCreate(Sender: TObject);
begin
//
end;


procedure TSimpleTapForm.Panel1MouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  with Sender as TPanel do begin
    BevelOuter := bvLowered;
    Font.Color := clYellow;
    MaskEdit.Text := MaskEdit.Text + IntToStr(Tag);
  end;
end;

procedure TSimpleTapForm.Panel1MouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  with Sender as TPanel do begin
    BevelOuter := bvRaised;
    Font.Color := clBlack;
  end;
end;


end.
