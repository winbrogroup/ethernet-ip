unit UseKeyboardA;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Mask, Buttons, CNC32, CoreCNC;

type
  TAccessKeyboardA = class(TForm)
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    MaskEdit: TMaskEdit;
    PromptLabel: TLabel;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    class function Execute(const Prompt : string; aSecurityCheck : TSecurityCheck) : Boolean;
  end;

var
  AccessKeyboardA: TAccessKeyboardA;

implementation

{$R *.dfm}

class function TAccessKeyboardA.Execute(const Prompt : string; aSecurityCheck : TSecurityCheck) : Boolean;
var Form : TAccessKeyboardA;
    Res : TModalResult;
    Resp : string;
begin
  Form := TAccessKeyboardA.Create(Application);
  Form.MaskEdit.PasswordChar := '*';
  SysObj.NC.AcquireModeChangeLock;
  try
    Form.PromptLabel.Caption := Prompt;
    repeat
      Form.MaskEdit.Text := '';
      res := Form.ShowModal;
      if (res = mrOk) then begin
        Resp := Form.MaskEdit.Text;
        if Assigned(aSecurityCheck) then begin
          if not aSecurityCheck(Resp) then begin
            Form.PromptLabel.Caption := SysObj.Localized.GetString('$INVALID_ENTRY_TRY_AGAIN');
            res := mrNone;
          end;
        end;
      end;
    until ((res = mrOK) or (res = mrCancel));
    Result := res = mrOK;
  finally
    SysObj.NC.ReleaseModeChangeLock;
    Form.Free;
  end;
end;

procedure TAccessKeyboardA.FormCreate(Sender: TObject);
begin
  Font.Assign(Sysobj.VisualParent.Font);
  Font.Size:= 14;
  PromptLabel.Caption := SysObj.Localized.GetString('$ENTER_ACCESS_SECURITY_CODE');
  BitBtn1.Caption := SysObj.Localized.GetString('$CANCEL');
  BitBtn2.Caption := SysObj.Localized.GetString('$OK');
  Self.caption :=  SysObj.Localized.GetString('$KEYBOARD_ACCESS');
end;

end.
