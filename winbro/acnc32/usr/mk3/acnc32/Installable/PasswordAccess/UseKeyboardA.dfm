object AccessKeyboardA: TAccessKeyboardA
  Left = 100
  Top = 186
  BorderStyle = bsSingle
  Caption = 'AccessKeyboard'
  ClientHeight = 208
  ClientWidth = 432
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object PromptLabel: TLabel
    Left = 0
    Top = 0
    Width = 432
    Height = 13
    Align = alTop
    Alignment = taCenter
    Caption = 'Enter Access Security Code'
    WordWrap = True
  end
  object BitBtn1: TBitBtn
    Left = 72
    Top = 104
    Width = 121
    Height = 75
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 0
    TabStop = False
  end
  object BitBtn2: TBitBtn
    Left = 240
    Top = 104
    Width = 121
    Height = 75
    Caption = 'OK'
    ModalResult = 1
    TabOrder = 1
    TabStop = False
  end
  object MaskEdit: TMaskEdit
    Left = 80
    Top = 48
    Width = 273
    Height = 21
    TabOrder = 2
  end
end
