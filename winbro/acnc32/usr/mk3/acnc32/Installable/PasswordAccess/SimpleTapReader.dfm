object SimpleTapForm: TSimpleTapForm
  Left = 564
  Top = 186
  BorderStyle = bsSingle
  Caption = 'SimpleTapForm'
  ClientHeight = 478
  ClientWidth = 391
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -21
  Font.Name = 'Comic Sans MS'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 29
  object PromptLabel: TLabel
    Left = 0
    Top = 0
    Width = 391
    Height = 29
    Align = alTop
    Alignment = taCenter
    Caption = 'Enter Access Security Code'
    WordWrap = True
  end
  object Panel1: TPanel
    Tag = 2
    Left = 136
    Top = 248
    Width = 120
    Height = 80
    Caption = '2'
    TabOrder = 0
    OnMouseDown = Panel1MouseDown
    OnMouseUp = Panel1MouseUp
  end
  object Panel2: TPanel
    Tag = 3
    Left = 264
    Top = 248
    Width = 120
    Height = 80
    Caption = '3'
    TabOrder = 1
    OnMouseDown = Panel1MouseDown
    OnMouseUp = Panel1MouseUp
  end
  object Panel3: TPanel
    Tag = 6
    Left = 264
    Top = 160
    Width = 120
    Height = 80
    Caption = '6'
    TabOrder = 2
    OnMouseDown = Panel1MouseDown
    OnMouseUp = Panel1MouseUp
  end
  object Panel4: TPanel
    Tag = 5
    Left = 136
    Top = 160
    Width = 120
    Height = 80
    Caption = '5'
    TabOrder = 3
    OnMouseDown = Panel1MouseDown
    OnMouseUp = Panel1MouseUp
  end
  object Panel5: TPanel
    Tag = 4
    Left = 8
    Top = 160
    Width = 120
    Height = 80
    Caption = '4'
    TabOrder = 4
    OnMouseDown = Panel1MouseDown
    OnMouseUp = Panel1MouseUp
  end
  object Panel6: TPanel
    Tag = 9
    Left = 264
    Top = 72
    Width = 120
    Height = 80
    Caption = '9'
    TabOrder = 5
    OnMouseDown = Panel1MouseDown
    OnMouseUp = Panel1MouseUp
  end
  object Panel7: TPanel
    Tag = 8
    Left = 136
    Top = 72
    Width = 120
    Height = 80
    Caption = '8'
    TabOrder = 6
    OnMouseDown = Panel1MouseDown
    OnMouseUp = Panel1MouseUp
  end
  object Panel8: TPanel
    Tag = 7
    Left = 8
    Top = 72
    Width = 120
    Height = 80
    Caption = '7'
    TabOrder = 7
    OnMouseDown = Panel1MouseDown
    OnMouseUp = Panel1MouseUp
  end
  object Panel9: TPanel
    Tag = 1
    Left = 8
    Top = 248
    Width = 120
    Height = 80
    Caption = '1'
    TabOrder = 8
    OnMouseDown = Panel1MouseDown
    OnMouseUp = Panel1MouseUp
  end
  object Panel10: TPanel
    Left = 136
    Top = 336
    Width = 120
    Height = 81
    Caption = '0'
    TabOrder = 9
    OnMouseDown = Panel1MouseDown
    OnMouseUp = Panel1MouseUp
  end
  object MaskEdit: TMaskEdit
    Left = 8
    Top = 432
    Width = 377
    Height = 37
    TabOrder = 10
  end
  object BitBtn1: TBitBtn
    Left = 8
    Top = 338
    Width = 121
    Height = 79
    TabOrder = 11
    Kind = bkCancel
  end
  object BitBtn2: TBitBtn
    Left = 264
    Top = 336
    Width = 121
    Height = 81
    TabOrder = 12
    Kind = bkOK
  end
end
