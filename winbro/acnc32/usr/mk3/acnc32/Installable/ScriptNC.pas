unit ScriptNC;

interface

uses CoreCNC, CNC32, CNCTypes, ACNCScript, NCNames, Tokenizer, Classes,
     ScriptTypeInfo, SysUtils, CompoundFiles, ScriptEditor, Controls,
     Named, ScriptFunctions, CNCAbs, ScriptFile, FaultRegistration;


const
  NameScriptNCTTL = 'ScriptNCTTL';

type
  TClientServerACNCScriptNC = class(TAbstractNC)
  protected
    Project : TACNCScriptProject;
    FScriptRTL : TScriptRTL;
    FTTL : Integer;
    procedure SetProject;
    procedure SetPartProgram(aTag : TAbstractTag); override;
  public
    procedure InstallComponent(Params : TParamStrings); override;
    procedure Installed; override;
    procedure ClientDualEvent(cevent : TNCEvent; aTag : TAbstractTag); override;
    property ScriptRTL : TScriptRTL read FScriptRTL write FScriptRTL;
  end;

  TACNCScriptNC = class(TClientServerACNCScriptNC)
  protected
    ProgramError : Boolean;
    ReqProgramRun : Boolean;
    procedure ChangeTTL(aTag : TAbstractTag);
    function BadProgram(Fid : FHandle) : Boolean;
    procedure UpdateNC(aTag : TAbstractTag);
    procedure SetPartProgram(aTag : TAbstractTag); override;
    procedure NewFaultLevel(aTag : TAbstractTag); override;
  public
    procedure Installed; override;
    procedure ClientDualEvent(cevent : TNCEvent; aTag : TAbstractTag); override;
//    property ScriptRTL : TScriptRTL read FScriptRTL write FScriptRTL;
  end;

  TACNCScriptSectionEditor = class(TSectionEditor)
  private
    Form : TScriptEditForm;
    ScriptNC : TClientServerACNCScriptNC;
    procedure UpdateEditor(aTag : TAbstractTag);
  public
    function Execute(Manager: TCompoundFile): Boolean; override;
    procedure InstallComponent(Params: TParamStrings); override;
    procedure Installed; override;
    function SectionName : string; override;
  end;

  TACNCScriptNCDisplay = class(TExpandDisplay)
  private
    ScriptNC : TClientServerACNCScriptNC;
  public
    procedure Resize; override;
    procedure Paint; override;
    procedure InstallComponent(Params : TParamStrings); override;
  end;

implementation

const
  ACNCScriptSectionName = 'ACNCScript';

//resourcestring
//  ModeChangeNotWhileEditingLang = 'Mode Change Not Allowed While Editing';
//  ModeChangeNotWhileInCycle = 'Mode Change Inhibited While In Cycle';

procedure TClientServerACNCScriptNC.InstallComponent(Params : TParamStrings);
var
  RTLClass : TScriptRTLClass;
begin
  FAxisCount := 2;
  FAxisNames := 'XY';
  inherited InstallComponent(Params);
  TagPb[ncpAxisName].AsString := 'XY';

  Project := TACNCScriptProject.Create(Self);
  RTLClass := TScriptRTL.FindRTLClass(Params.ReadString('ScriptRTL', 'TScriptRTL'));
  FTTL := Params.ReadInteger('TTL', 500);
  if RTLClass = nil then
    raise ECNCInstallFault.Create('ScriptRTLClass not found');
  Project.RTL := RTLClass.Create(Self);
  ScriptRTL := Project.RTL;
end;

procedure TClientServerACNCScriptNC.Installed;
begin
  inherited Installed;
  TagPb[ncpActiveAxis].AsInteger := ActiveAxis;
  TagPb[ncpInCycle].AsBoolean := False;
  TagPb[ncpInMotion].AsBoolean := False;
  TagPb[ncpHomed].AsBoolean := True;
  TagPb[ncpSingleBlock].AsBoolean := False;
  TagPb[ncpOptionalStop].AsBoolean := False;
  TagPb[ncpPartProgram].Refresh;
  ScriptRTL.Install;
end;

procedure TClientServerACNCScriptNC.ClientDualEvent(cevent : TNCEvent; aTag : TAbstractTag);
begin
end;

procedure TClientServerACNCScriptNC.SetPartProgram(aTag : TAbstractTag);
var ScriptFile : TACNCScriptFile;
begin
  if SysObj.FileManager.ActiveFileName = '' then
    Exit;

  if CNCMode <> [CNCModeEdit] then begin
    ScriptFile := TACNCScriptFile.Create(Self);
    try
      try
        ScriptFile.Project := Project;
        SysObj.FileManager.SectionRead(ACNCScriptSectionName, ScriptFile);
        ScriptFile.ReadFromStrings;
        try
          Project.Compile('Main');
          SetProject;
        except
          on E : EACNCScriptError do
            Exit;
        end;
      except
        on E : ECompoundFileError do
          Exit;
      end;
    finally
      Scriptfile.Free;
    end;
  end;
end;

procedure TACNCScriptNC.NewFaultLevel(aTag : TAbstractTag);
var NewFaultLevel : TFaultLevel;
begin
  NewFaultLevel := TFaultLevel(aTag.AsInteger);
  if NewFaultLevel > FaultWarning then begin
    TagPb[ncpSingleBlock].AsBoolean := not SingleBlock;
    SetProject;
  end;
end;


procedure TACNCScriptNC.Installed;
begin
  inherited;
  TagEvent(NameDisplaySweep, edgeChange, TMethodTag, UpdateNC);
  TagEvent(NameScriptNCTTL, edgeChange, TIntegerTag, ChangeTTL);
  SetProject;
end;

procedure TACNCScriptNC.ClientDualEvent(cevent : TNCEvent; aTag : TAbstractTag);
  procedure ReleaseProgram;
  begin
    Project.Clean;
    SetProject;
  end;

  procedure ReloadProgram;
  begin
    Project.Clean;
    SetPartProgram(nil);
//    SetProject;
  end;

begin
  case cevent of
    ncdeReqSingleBlock : begin
      TagPb[ncpSingleBlock].AsBoolean := not SingleBlock;
      SetProject;
    end;

    ncdeReqCycleStart :
      if (not ProgramError) and (FLines.Count > 0) then begin
        if CNCMode <> [CNCModeAuto] then
          Exit;
        if SysObj.FaultInterface.FaultLevel < FaultStopCycle then begin
          ReqProgramRun := True;
          TagPb[ncpInCycle].AsBoolean := True;
        end else
          SysObj.FaultInterface.SetFault(Self, CoreFaults[cfFunctionNotPermitted], [], nil);
      end;

    ncdeReqOptionalStop : begin
      TagPb[ncpOptionalStop].AsBoolean := not OptionalStop;
      SetProject;
    end;

    ncdeReqFeedHold : begin
      TagPb[ncpFeedHold].AsBoolean := not FeedHold;
      SetProject;
    end;

    ncdeReqRewind : begin
      if not ReqProgramRun then begin
        TagPb[ncpProgramLine].AsInteger := 0;
        if Assigned(ScriptRTL) then
          ScriptRTL.CleanUp;
        SetPartProgram(nil);
      end;
    end;

    ncdeReqDryRun :
      TagPb[ncpDryRun].AsBoolean := not TagPb[ncpDryRun].AsBoolean;

    ncdeReqManualMode ,
    ncdeReqAutoMode   ,
    ncdeReqMDIMode    ,
    ncdeReqTeachMode  ,
    ncdeReqEditMode   : begin
      if cstEditing in SysObj.Comms.StatusType then begin
        SysObj.FaultInterface.SetFault(Self, CoreFaults[cfNoModeChangeWhileEditing], [], nil);
        exit;
      end;
      if InCycle then begin
        SysObj.FaultInterface.SetFault(Self, CoreFaults[cfNoModeChangeWhileInCycle], [], nil);
        Exit;
      end;
      case cevent of
        ncdeReqManualMode : begin
          TagPb[ncpActiveMode].AsInteger := Ord(CNCModeManual);
        end;

        ncdeReqAutoMode   : begin
          TagPb[ncpActiveMode].AsInteger := Ord(CNCModeAuto);
          ReloadProgram;
        end;

        ncdeReqMDIMode    : begin
          TagPb[ncpActiveMode].AsInteger := Ord(CNCModeMDI);
        end;

        ncdeReqTeachMode  : begin
          TagPb[ncpActiveMode].AsInteger := Ord(CNCModeTeachIn);
        end;

        ncdeReqEditMode   : begin
          TagPb[ncpActiveMode].AsInteger := Ord(CNCModeEdit);
          ReleaseProgram;
        end;
      end;
    end;
  else
  end;
end;

procedure TACNCScriptNC.SetPartProgram(aTag : TAbstractTag);
var ScriptFile : TACNCScriptFile;
begin
  if CNCMode <> [CNCModeEdit] then begin
    ScriptFile := TACNCScriptFile.Create(Self);
    try
      try
        ScriptFile.Project := Project;
        SysObj.FileManager.SectionRead(ACNCScriptSectionName, ScriptFile);
        ScriptFile.ReadFromStrings;
        try
          Project.Compile('Main');
        except
          on E : EACNCScriptError do begin
            SysObj.FaultInterface.SetFault(Self, CoreFaults[cfSysStopCycle], [E.Message], BadProgram);
            ProgramError := True;
            Exit;
          end;
        end;
      except
        on E : ECompoundFileError do Exit;
      end;
    finally
      Scriptfile.Free;
    end;
    SetProject;
    ProgramError := False;
  end;
end;

function TACNCScriptNC.BadProgram(Fid : FHandle) : Boolean;
begin
  Result := not ProgramError;
end;

procedure TClientServerACNCScriptNC.SetProject;
begin
  Project.SingleBlock := True;
  if SingleBlock then
    Project.TTL := 0
  else
    Project.TTL := FTTL;

  if Assigned(Project.TraceScript) then begin
    FLines := Project.TraceScript;
    TagPb[ncpProgramLine].AsInteger := Project.LineNumber;
  end else begin
    FLines := nil;
    TagPb[ncpProgramLine].AsInteger := 0;
  end;
  TagPb[ncpPartProgram].AsString := Project.ActiveFile;
end;

procedure TACNCScriptNC.UpdateNC(aTag : TAbstractTag);
var CurrentScript : TAbstractScript;
begin
  if (not ProgramError) and
     (Project.TraceScript <> nil) then begin
     CurrentScript := Project.TraceScript;
     if ReqProgramRun and (not FeedHold) then begin
       Project.Run;
       if SingleBlock or (not Project.InProcess) then begin
         ReqProgramRun := False;
         TagPb[ncpInCycle].AsBoolean := ReqProgramRun;
       end;
       if CurrentScript <> Project.TraceScript then
         FLines := Project.TraceScript;

       TagPb[ncpProgramLine].AsInteger := Project.LineNumber;
     end;
  end;
  if TagPb[ncpPartProgram].AsString <> Project.ActiveFile then
    TagPb[ncpPartProgram].AsString := Project.ActiveFile;
end;

procedure TACNCScriptNC.ChangeTTL(aTag : TAbstractTag);
begin
  FTTL := aTag.AsInteger;
  SetProject;
end;

procedure TACNCScriptNCDisplay.InstallComponent(Params : TParamStrings);
begin
  if SysObj.NC is TClientServerACNCScriptNC then
    ScriptNC := TClientServerACNCScriptNC(SysObj.NC)
  else
    raise ECNCInstallFault.Create(Self.Name + 'Requires TACNCScriptNC Installed');

  inherited;

  ScriptNC.Project.RTL.ClientCanvas := Canvas;
end;

procedure TACNCScriptNCDisplay.Resize;
begin
  if Assigned(ScriptNC.Project.RTL) then
    ScriptNC.Project.RTL.Resize;
end;

procedure TACNCScriptNCDisplay.Paint;
begin
  inherited;
  ScriptNC.Project.RTL.DoPaint;
end;



function TACNCScriptSectionEditor.Execute(Manager: TCompoundFile): Boolean;
var
  ScriptFile : TACNCScriptFile;
  Project : TACNCScriptProject;
begin
  Form := TScriptEditForm.Create(Self);
  ScriptFile := TACNCScriptFile.Create(ScriptNC);
  Project := TACNCScriptProject.Create(ScriptNC);
  Project.RTL := ScriptNC.Project.RTL;
  try
    Manager.SectionOpen(SectionName, ScriptFile);
    ScriptFile.Project := Project;
    ScriptFile.ReadFromStrings;
    Form.ScriptProject := Project;
    Result := (Form.ShowModal = mrOK) and Form.Modified;
    ScriptFile.WriteToStrings;
    Manager.SectionClose(ScriptFile, Result)
  finally
    Form.Free;
    Form := nil;
    Scriptfile.Free;
    Project.RTL.CleanUp;
    Project.Free;
  end;
end;

procedure TACNCScriptSectionEditor.InstallComponent(Params: TParamStrings);
begin
  inherited InstallComponent(Params);
  if not(SysObj.NC is TClientServerACNCScriptNC) then
    raise ECNCInstallFault.Create(Name + ' Requires TACNCScriptNC');

  ScriptNC := TACNCScriptNC(SysObj.NC);
end;

procedure TACNCScriptSectionEditor.Installed;
begin
  inherited;
  TagEvent(NameDisplaySweep, edgeChange, TMethodTag, UpdateEditor);
end;

procedure TACNCScriptSectionEditor.UpdateEditor(aTag : TAbstractTag);
begin
  if Assigned(Form) then
    Form.DoUpdate;
end;


function TACNCScriptSectionEditor.SectionName : string;
begin
  Result := ACNCScriptSectionName;
end;

initialization
//  RegisterCNCClass(TClientServerACNCScriptNC);
//  RegisterCNCClass(TACNCScriptNC);
//  RegisterCNCClass(TACNCScriptSectionEditor);
//  RegisterCNCClass(TACNCScriptNCDisplay);
end.
