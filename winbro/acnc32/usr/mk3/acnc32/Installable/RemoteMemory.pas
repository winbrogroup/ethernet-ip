unit RemoteMemory;

interface

uses Classes, CoreCNC, CNC32, ScktComp, WinSock, CNCTypes, Windows, SysUtils,
     StdCtrls, Controls, Forms, GenericIO, FaultRegistration;

const
  CNCMemoryPort = 12121;
  MaxDataSize = 32096;

type

  { Domains

    PS Published By TCNC Server in Server owner is TCNC
    PC Published By TCNC Client in Server owner is Self
    ES Event on PS
    EC Event on PC

    Client Connection Requests First.

    For Each Item that is Published by Self
      1. Event Requested from Server with an ndx of 0..Total requested
      2. As each Item is found it is added to the ClientList using ndx
      3. Wait for acknowledge from server rdtAck / rdtNack

    Then Sends rdtConfigured.

    The Server then sends requests for events
      1. Seach for the requested name and determine that the class type is
         compatable with the request.
         If the name is not found respond with rdtNack. If this doesn't bomb the
         connection then a place holder must be added to keep ndx integraty.
      2. Add the NamedLayer Item to Remote Event list and ensure a callback is
         added to Tag.
      3. Send rtdAck

    This is repeated until rtdConfigured is received.

    The Server connections spec can be implied from the above.
  }

  TDummyTag = class(TStringTag)
  public
    function GetAsString : string; override;
  end;

implementation


const
  TagNotFound = 'Tag Not Found';
  BadIndex = 'Bad Index';
  BadTagSize = 'Bad Tag Size';



function TDummyTag.GetAsString : string;
begin
  Result := 'Bad Reference';
end;

initialization
  //RegisterTagClass(TDummyTag);
end.
