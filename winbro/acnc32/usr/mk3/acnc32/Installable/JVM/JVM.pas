unit JVM;

interface

uses SysUtils, CoreCNC, Cnc32, CncTypes, JNI, JNIUtils;


type
  TJVM = class(TCNC)
  private
    JVMPath: string;
    FJavaVM: TJavaVM;
    FJNIEnv: TJNIEnv;
    ClassPath: string;
    procedure LoadJVM;
  public
    procedure InstallComponent(Params: TParamStrings); override;
    procedure IShutdown; override;
    destructor Destroy; override;
  end;

implementation

{ TJVM }

destructor TJVM.Destroy;
begin
  FreeAndNil(FJavaVM);
  JNI.UnloadJVM;
  inherited;
end;

procedure TJVM.InstallComponent(Params: TParamStrings);
begin
  inherited;

  JVMPath:= Params.ReadString('jvmpath', 'jvm.dll');
  ClassPath:= Params.ReadString('classpath', '.');

  LoadJVM;
end;

procedure TJVM.IShutdown;
begin
  inherited;
end;

procedure TJVM.LoadJVM;
var
  ClassPath: string;
  Errcode: Integer;
  VM_args: JavaVMInitArgs;
  Options: array [0..10] of JavaVMOption;
begin

  EventLog('Loading Java VM...');

  // Set up the options for the VM
  SetCurrentDir( ProfilePath );
  FillChar(Options, SizeOf(Options), #0);
  Options[0].optionString := PChar('-Djava.class.path=' + ClassPath);
  Options[1].optionString := PChar('-Djava.library.path=lib');
  VM_args.version  := JNI_VERSION_1_6;
  VM_args.options  := @Options;
  VM_args.nOptions := 2;

  // Create the wrapper for the VM
  //FJavaVM := TJavaVM.Create(JNI_VERSION_1_2, 'C:\Program Files\Java\jre6\lib\i386\jvm.dll');
  FJavaVM := TJavaVM.Create(JNI_VERSION_1_6, JVMPath);
  // Load the VM
  Errcode := FJavaVM.LoadVM(VM_args);
  if Errcode < 0 then
  begin
    // Loading the VM more than once will cause this error
    if Errcode = JNI_EEXIST then
      raise Exception.Create(
        'Java VM has already been loaded. Only one JVM can be loaded.')
    else
      raise Exception.Create(
        Format('Error creating JavaVM, code = %d', [Errcode]));
  end;

  // Create the Env class
  FJNIEnv := TJNIEnv.Create(FJavaVM.Env);
  EventLog(Format('Java VM %d.%d loaded', [FJNIEnv.MajorVersion, FJNIEnv.MinorVersion]));
  CoreCNC.SysObj.Comms.PluginSetJNI( FJNIEnv.Env );
end;

initialization
  CoreCNC.RegisterCNCClass(TJVM, 'JVM');
end.
