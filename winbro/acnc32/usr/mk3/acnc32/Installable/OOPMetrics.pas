unit OOPMetrics;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, CoreCNC, CNC32, CNCTypes, IniFiles, ComCtrls, StdCtrls;

type
  TInfoClass = class(TObject)
    this : TClass;
    list : TList;  // List of instanced components
    constructor Create;
    destructor Destroy; override;
  end;
  PTInfoClass = ^TInfoClass;

  TOOPMetrics = class(TAbstractDisplay)
  private
    tree          : TTreeView;
    treePanel     : TPanel;
    contents      : TPanel;
    cont          : TPaintBox;

    currentNode   : TTreeNode;
    List          : TStringList;
    procedure TreeSelect(Sender: TObject; Node: TTreeNode);
    procedure GetAncestry(it : TComponent; list : TStringList);
//    function  AddListToTree(list : TstringList) : TTreeNode;
    function  CreateInfo(itClass : TClass; it : TComponent; obj : TObject) : TInfoClass;
//    function  presentInTree(comp : string) : integer;
    function  FindInTree(Comp : TComponent) : TTreeNode;
    function  FindInBranch(TN : TTreeNode; This : TClass) : TTreeNode;
    procedure DrawContentsPanel(Info : TInfoClass);
    procedure ContentsDraw(Sender : TObject);
    procedure Recurse(comp : TComponent);
  protected
  public
    constructor Create(Aowner : TComponent); override;
    procedure InstallComponent(Params : TParamStrings); override;
    procedure Installed; override;
    procedure Resize; override;
  published
  end;

implementation

uses Grids;

const
  MIN_HEIGHT     = 60;
  INSTANCE_TOP   = 50;
  TEXT_Y_SPACE   = 20;
  CONTENTS_WIDTH = 150;

constructor TOOPMetrics.create(Aowner : TComponent);
begin
  inherited Create(Aowner);
//  Font.name := 'Arial';
  Font.size := 9;
end;

procedure TOOPMetrics.installComponent(Params : TParamStrings);
begin
  TreePanel := TPanel.create(Self);
  TreePanel.parent := Self;

  tree := TTreeView.create(TreePanel);
  tree.parent := TreePanel;
  tree.align := alClient;
  tree.showLines := true;

  contents := TPanel.create(Self);
  contents.parent := Self;

  cont := TPaintBox.create(Self);
  cont.parent  := contents;
  cont.align   := alClient;
  cont.onPaint := contentsDraw;

  inherited installComponent(Params);
end;

procedure TOOPMetrics.installed;
var root     : TObject;
begin
  inherited installed;

  TagEvent(name, edgeRising, TMethodTag, activate);

  root := TObject.create;
  list := TstringList.create;
  Tree.items.addObjectFirst(Tree.selected, root.className, CreateInfo(root.ClassType, nil, nil));

  Recurse(parent);

  root.Free;
  list.Free;
  Tree.fullExpand;
  Tree.onChange  := treeSelect;
  Tree.items[0].selected := true;
end;

procedure TOOPMetrics.Recurse(comp : TComponent);
var I : integer;
begin
  for i := 0 to comp.ComponentCount - 1 do Recurse(comp.components[i]);

  List.clear;
  GetAncestry(comp, list);

  FindInTree(comp);
end;

procedure TOOPMetrics.resize;
begin
  inherited resize;

  if installState = installStateIdle then exit;

  TreePanel.left   := CNCBorder;
  TreePanel.top    := CNCBorder;
  TreePanel.width  := width - (CNCBorder * 2);
  TreePanel.height := height -(CNCBorder * 2);
end;

procedure TOOPMetrics.GetAncestry(it : TComponent; list : TStringList);
var regress : TClass;
begin
  list.clear;
  list.Add(it.className);
  List.Objects[0] := CreateInfo(it.classType, it, list.objects[0]);

  regress := it.classParent;

  while regress <> nil do begin
    list.insert(0, regress.className);
    list.objects[0] := CreateInfo(regress, nil, list.objects[0]);
    regress := regress.classParent;
  end;
end;


procedure TOOPMetrics.treeSelect(Sender: TObject; Node: TTreeNode);
var Info : TInfoClass;
begin
  if installState <> installStateDone then exit;
  currentNode := Node;
  Info := Tree.items[Node.AbsoluteIndex].data;
  drawContentsPanel(Info);
end;

procedure TOOPMetrics.contentsDraw(Sender : TObject);
var Info      : TInfoClass;
    i, j      : integer;
    state     : TInstallState;
    flag      : string;
    partial   : TObject;
begin
  if currentNode = nil then exit;
  info := currentNode.data;
  cont.canvas.pen.color := clBlack;
  cont.canvas.TextOut(10, 10, 'Instance Size : ' + inttostr(Info.this.instanceSize));

  if info.list = nil then begin
    cont.canvas.TextOut(10, 30, 'No Instances');
    exit;
  end else begin
    cont.canvas.TextOut(10, 30, 'Instances : ' + inttostr(info.list.count));
  end;


  j := 0;
  for i := 0 to info.list.count - 1 do begin
    if TComponent(Info.list[i]).name = '' then continue;
    partial := info.list[i];
    state := installStateIdle;
    if partial is TCNC then begin
      state := TCNC(partial).installState;
    end else if partial is TAbstractDisplay then begin
      state := TAbstractDisplay(partial).installState;
    end;

    flag := '';
    if state <> installStateIdle then flag := ' *';

    cont.canvas.TextOut(10, INSTANCE_TOP + (j * TEXT_Y_SPACE),
                            TComponent(Info.list[i]).name + flag);
    inc(j);
  end;


end;

procedure TOOPMetrics.drawContentsPanel(Info : TInfoClass);
var i, Total  : integer;
begin
  contents.left := Tree.ClientWidth - CONTENTS_WIDTH;
  contents.top  := Tree.ClientHeight - MIN_HEIGHT;
  contents.width  := CONTENTS_WIDTH;
  Total := 0;
  if Info.list = nil then begin
    contents.height := MIN_HEIGHT;
    cont.repaint;
    exit;
  end else begin
    for i := 0 to info.list.count - 1 do begin
      if TComponent(info.list[i]).name <> '' then Inc(Total);
    end;
    contents.height := MIN_HEIGHT + (Total * TEXT_Y_SPACE);
    contents.top := Tree.ClientHeight -  contents.height;

    if contents.height > clientHeight then begin
      contents.height := clientHeight;
      contents.top    := 0;
    end;
    cont.repaint;
  end;
end;

function TOOPMetrics.CreateInfo(itClass : TClass; it : TComponent; obj : TObject) : TInfoClass;
var ret : TInfoClass;
begin
  if obj = nil then ret := TInfoClass.create
               else ret := obj as TInfoClass;

  if it <> nil then begin
    ret.list.add(it);
  end;

  ret.this := itClass;
  result := ret;
end;

function TOOPMetrics.FindInBranch(TN : TTreeNode; This : TClass) : TTreeNode;
var I : Integer;
begin
  for I := 0 to TN.Count - 1 do
    if TInfoClass(TN.Item[I].data).this = This then begin
      Result := TN[I];
      Exit;
    end;

  Result := nil;
end;

{function TOOPMetrics.presentInTree(comp : string) : integer; }
function TOOPMetrics.FindInTree(Comp : TComponent) : TTreeNode;
var I, Rem : integer;
    ListTotal : Integer;
    TN : TTreeNode;
begin
  ListTotal := List.Count;
  Result := nil;

  TN := Tree.Items[0]; // This is assumed to be TObject
  for I := 1 to ListTotal - 1 do begin // Start search at persistent
    Result := FindInBranch(TN, TInfoClass(List.Objects[I]).This);
    if Result = nil then begin
      // Remove the duplicated classes
      for Rem := 0 to I - 1 do begin
        List.Objects[0].Free;
        List.Delete(0);
      end;

      while List.Count > 0 do begin
        TN := Tree.items.AddChildObject(TN, list[0], list.objects[0]);
        List.Delete(0);
      end;
      Exit;
    end;
    TN := Result;
  end;
  if TObject(TN.Data) is TInfoClass then
    TInfoClass(TN.Data).List.Add(Comp);
{  Info := Tree.items[TN.AbsoluteIndex].Data;
  Info.List.Add(Comp); }
end;

constructor TInfoClass.Create;
begin
  inherited Create;
  List := TList.Create;
end;

destructor TInfoClass.Destroy;
begin
  List.Free;
  inherited Destroy;
end;

begin
  RegisterCNCClass(TOOPMetrics);
end.
