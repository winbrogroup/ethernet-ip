unit TPMSetup;

interface

uses Classes, SysUtils, Windows, CNC32, CNCTypes, CoreCNC, CNCAbs, Controls,
     ExtCtrls, ComCtrls, Tokenizer, Named, PlcItems, Graphics, Buttons, CommCtrl,
     FaultRegistration, CNCRegistry, DynamicHelp;


type
  TTPMType = (
    tpmOnTimer,
    tpmOffTimer,
    tpmRiseCounter,
    tpmFallCounter,
    tpmChangeCounter
  );

  TTPMItem = class;

  // Contains a list of TPMItemList, each member of the TPMItem list represents
  // a single tag with a list of associated items
  TTPMTagList = class(TObject)
  private
    TagList : TList; //
  public
    constructor Create;
    procedure Add(aItem : TTPMItem);
    procedure SetOutputs;
  end;

  TTPMItemList = class(TObject)
  private
    ItemList : TList;
  public
    Tag : TAbstractTag;
    constructor Create(aItem : TTPMItem);
    procedure Add(aItem : TTPMItem);
    function CheckOn : Boolean;
  end;

  TTPMItem = class(TObject)
  private
    Name : string;
    Input : TAbstractTag;
    InputName : string;
    Output : TAbstractTag;
    OutputName : string;
    WarnLimit : Double;
    Limit : Int64;
    TPMType : TTPMType;
    TPMMessage : string;
    Total : Int64;
    FID : FHandle;        // Handle to active fault
    FaultLevel : TFaultLevel;  // Updated with the current fault level
    AccessLevels : TAccessLevels;
    Help : string;
  public
    constructor Create;
    procedure Configure(S : TStringTokenizer);
  end;

  TTPMSetup = class(TExpandDisplay)
  private
    Status : TStatusBar;
    ListView : TListView;
    Panel : TPanel;
    Reg : TCNCRegistry;
    ItemList : TList;
    LastTick : Int64;
    UpBtn : TSpeedButton;
    DownBtn : TSpeedButton;
    ResetBtn : TSpeedButton;
    MyBrush : TBrush;
    TPMTagList : TTPMTagList;
//    procedure DrawItem(Sender: TCustomListView; const ARect: TRect; var DefaultDraw: Boolean);
    procedure CustomDrawItem(Sender: TCustomListView; Item: TListItem; State: TCustomDrawState; var DefaultDraw: Boolean);
    procedure UpdateDisplayItem(Index : Integer; Item : TTPMItem);
    procedure UpdateDisplayItemTotal(Index : Integer; Item : TTPMItem);
    procedure TagEvents(aTag : TAbstractTag);
    function ItemFaultReset(FID : FHandle) : Boolean;
    procedure UpdateDisplay(aTag : TAbstractTag);
    function HoursFormat(Ticks : Int64; var DisplayText :String) : Integer;
    procedure Configure(S : TQuoteTokenizer);
    procedure SetItemTotal(Index : Integer; Item : TTPMItem; Value : Int64);
    procedure SetItemFault(Item : TTPMItem);
    procedure ResetCounterQuestion(Sender : TObject);
    procedure DoClick(Sender : TObject);
//    procedure SelectItem(Sender: TObject; Item: TListItem; Selected: Boolean);
  public
    constructor Create(aOwner : TComponent); override;
    destructor Destroy; override;
    procedure InstallComponent(Params : TParamStrings); override;
    procedure Installed; override;
    procedure IShutdown; override;
    procedure Open; override;
  end;

implementation

uses Acknowledge;

type
  TTPMFaults = (
    tpmfInterlock,
    tpmfWarning,
    tpmfStopCycle,
    tpmfEStop,
    tpmfFatal,
    tpmfHigherUserLevel
  );
const
  TPMFaults : array [TTPMFaults] of TFaultRegistration = (
    ( Ndx : 3000; Text : '%s'; Level : FaultInterlock ),
    ( Ndx : 3001; Text : '%s'; Level : FaultWarning ),
    ( Ndx : 3002; Text : '%s'; Level : FaultStopCycle),
    ( Ndx : 3003; Text : '%s'; Level : FaultEStop ),
    ( Ndx : 3004; Text : '%s'; Level : FaultFatal ),
    ( Ndx : 3005; Text : 'Action requires a higher user level'; Level : FaultInterlock )
  );

constructor TTPMSetup.Create(aOwner : TComponent);
var lc : TListColumn;
begin
  inherited Create(aOwner);
  Panel := TPanel.Create(Self);
  Panel.Parent := Self;
  Panel.Align := alTop;
  Panel.Height := 50;
  Panel.BevelOuter := bvLowered;

  UpBtn := TSpeedButton.Create(Self);
  UpBtn.Parent := Panel;
  UpBtn.Width := 100;
  UpBtn.Height := 46;
  UpBtn.Top := 2;
  UpBtn.Left := 10;
  UpBtn.Caption := UpLang;
  UpBtn.OnClick := DoClick;

  DownBtn := TSpeedButton.Create(Self);
  DownBtn.Parent := Panel;
  DownBtn.Width := 100;
  DownBtn.Height := 46;
  DownBtn.Top := 2;
  DownBtn.Left := 120;
  DownBtn.Caption := DownLang;
  DownBtn.OnClick := DoClick;

  ResetBtn := TSpeedButton.Create(Self);
  ResetBtn.Parent := Panel;
  ResetBtn.Width := 100;
  ResetBtn.Height := 46;
  ResetBtn.Top := 2;
  ResetBtn.Left := 230;
  ResetBtn.Caption := ResetLang;
  ResetBtn.OnClick := DoClick;


  Status := TStatusBar.Create(Self);
  Status.Parent := Self;
  Status.Panels.Add;

  ListView := TListView.Create(Self);
  ListView.Parent := Self;
  ListView.Align := alClient;
  ListView.ViewStyle := vsReport;
  ListView.HideSelection := True;
  ListView.GridLines := True;
  ListView.OnCustomDrawItem := CustomDrawItem;
  ListView.ReadOnly := True;

  ItemList := TList.Create;

  lc := ListView.Columns.Add;
  lc.Caption := NameLang;
  lc.Width := 150;

  lc := ListView.Columns.Add;
  lc.Caption := TotalLang;
  lc.Width := 100;

  lc := ListView.Columns.Add;
  lc.Caption := LimitLang;
  lc.Width := 100;

  lc := ListView.Columns.Add;
  lc.Caption := MessageLang;
  lc.Width := 450;

  MyBrush := TBrush.Create;
  MyBrush.Color := clYellow;

  TPMTagList := TTPMTagList.Create;
end;

{procedure TTPMSetup.DrawItem(Sender: TCustomListView; const ARect: TRect; var DefaultDraw: Boolean);
begin
end; }

procedure TTPMSetup.CustomDrawItem(Sender: TCustomListView; Item: TListItem; State: TCustomDrawState; var DefaultDraw: Boolean);
var TPM : TTPMItem;
    Widths : array [0..10] of Integer;

  procedure DrawSub(const aRect : TRect; SubItem: Integer);
  var I : Integer;
      Left : Integer;
      Rect : TRect;
  begin
      Widths[SubItem] := ListView_GetColumnWidth(ListView.Handle, SubItem);
      Left := aRect.Left;
      for I := 0 to SubItem - 1 do
        Inc(Left, Widths[I]);

      Rect := Item.DisplayRect(drBounds);
      Rect.Left := Left;
      Rect.Right := Rect.Left + Widths[SubItem];

      ListView.Canvas.TextRect(Rect, Rect.Left + 5, Rect.Top, Item.SubItems[SubItem - 1]);
  end;
var I : Integer;
    Rect : TRect;
begin
  TPM := TTPMItem(Item.Data);
  if cdsSelected in State then
    ListView.Canvas.Brush.Color := clYellow
  else if TPM.FaultLevel > FaultWarning then
    ListView.Canvas.Brush.Color := $c0c0ff
  else if TPM.FaultLevel > FaultNone then
    ListView.Canvas.Brush.Color := $ffc0c0
  else if Odd(Item.Index) then
    ListView.Canvas.Brush.Color := $e0e0e0
  else
    ListView.Canvas.Brush.Color := $e8e8e8;

  Widths[0] := ListView_GetColumnWidth(ListView.Handle, 0);
  Rect := Item.DisplayRect(drBounds);
//  Rect.Left := 0;
  Rect.Right := Rect.Left + Widths[0];
  ListView.Canvas.TextRect(Rect, Rect.Left + 5, Rect.Top, Item.Caption);

  for I := 0 to Item.SubItems.Count - 1 do begin
    DrawSub(Rect, I + 1);
  end;
  defaultdraw := False;
end;

destructor TTPMSetup.Destroy;
begin
  if Assigned(ItemList) then
    ItemList.Free;

  if Assigned(Reg) then
    Reg.Free;

  Inherited Destroy;
end;

procedure TTPMSetup.InstallComponent(Params : TParamStrings);
var Config : TQuoteTokenizer;
    Item : TTPMItem;
    I : Integer;
begin
  inherited;
  InstallKeySet(Params.ReadKeys);
  Reg := TCNCRegistry.Create;
  Reg.OpenKey(RegistryBase + '\' + Name, true);

  Config := TQuoteTokenizer.Create;
  try
    ReadLive(Config, Name);
    try
      Configure(Config);
    except
      on E : Exception do
        raise ECNCInstallFault.CreateFmt('Error in %s Configuration : [%s]', [Name, E.Message]);
    end;
  finally
    Config.Free;
  end;

  for I := 0 to ItemList.Count - 1 do begin
    Item := TTPMItem(ItemList[I]);
    if Item.OutputName <> '' then begin
      Item.Output := TagPublish(Item.OutputName, TMethodTag);
      Self.TPMTagList.Add(Item);
    end;
    if Item.Name <> '' then begin
      Reg.OpenKey(RegistryBase + '\' + Name + '\' + Item.Name, true);
      if Reg.ValueExists('Total') then
        try
          Item.Total := Round(Reg.ReadFloat('Total'));
        except
          Item.Total := 0;
        end;
    end;
  end;
  LastTick := CNC32.PlcTick.I64;
end;

procedure TTPMSetup.Installed;
var Item : TTPMItem;
    I : Integer;
    li : TListItem;
begin
  inherited;
  TagEvent(NameDisplaySweep, EdgeChange, TMethodTag, UpdateDisplay);
  for I := 0 to ItemList.Count - 1 do begin
    Item := TTPMItem(ItemList[I]);
    if Item.InputName <> '' then
      Item.Input := TagEvent(Item.InputName, EdgeChange, TMethodTag, TagEvents);

    LI := ListView.Items.Add;
    LI.Data := Item;
    LI.Caption := Item.Name;
    LI.SubItems.Add(IntToStr(Item.Total));
    LI.SubItems.Add(IntToStr(Item.Limit));
    LI.SubItems.Add(Item.TPMMessage);
  end;
  LastTick := CNC32.PlcTick.I64;
end;

procedure TTPMSetup.IShutdown;
var Item : TTPMItem;
    I : Integer;
begin
  for I := 0 to ItemList.Count - 1 do begin
    Item := TTPMItem(ItemList[I]);
    if Item.Name <> '' then begin
      Reg.OpenKey(RegistryBase + '\' + Name + '\' + Item.Name, true);
      try
        Reg.WriteFloat('Total', Item.Total);
      except
      end;
    end;
  end;

  inherited;
end;

// Whenever the display is opened ensure the data is rendered correctly
procedure TTPMSetup.Open;
var Item : TTPMItem;
    I : Integer;
begin
  for I := 0 to ItemList.Count - 1 do begin
    Item := TTPMItem(ItemList[I]);
    UpdateDisplayItem(I, Item);
  end;
  inherited Open;
end;


procedure TTPMSetup.UpdateDisplayItem(Index : Integer; Item : TTPMItem);
var Tmp : string;
begin
  case Item.TPMType of
    tpmFallCounter, tpmRiseCounter, tpmChangeCounter :
      Tmp := IntToStr(Item.Limit);
    tpmOnTimer, tpmOffTimer :
      HoursFormat(Item.Limit, Tmp);
  end;
  if Tmp <> ListView.Items[Index].SubItems[1] then
    ListView.Items[Index].SubItems[1] := Tmp;

  UpdateDisplayItemTotal(Index, Item);
end;

procedure TTPMSetup.DoClick(Sender : TObject);
begin
  if (Sender = UpBtn) then begin
    if Assigned(ListView.Selected) then begin
      if ListView.Selected.Index > 0 then
        ListView.Selected := ListView.Items[ListView.Selected.Index - 1];
    end else begin
      ListView.Selected := ListView.Items[0];
    end;
  end;

  if (Sender = DownBtn) then begin
    if Assigned(ListView.Selected) then begin
      if ListView.Selected.Index < ListView.Items.Count - 1 then
        ListView.Selected := ListView.Items[ListView.Selected.Index + 1];
    end else begin
      ListView.Selected := ListView.Items[0];
    end;
  end;

  if (Sender = ResetBtn) then
    ResetCounterQuestion(Self);

  ListView.SetFocus;
end;

{procedure TTPMSetup.SelectItem(Sender: TObject; Item: TListItem; Selected: Boolean);
var TPM : TTPMItem;
begin
  if Selected then begin
    TPM := TTPMItem(Item.Data);
    Status.Panels[0].Text := TPM.Name;
  end;
end; }

procedure TTPMSetup.UpdateDisplayItemTotal(Index : Integer; Item : TTPMItem);
var Tmp : string;
begin
  case Item.TPMType of
    tpmFallCounter, tpmRiseCounter, tpmChangeCounter :  Tmp := IntToStr(Item.Total);
    tpmOnTimer, tpmOffTimer : HoursFormat(Item.Total, Tmp);
  end;
  if Tmp <> ListView.Items[Index].SubItems[0] then
    ListView.Items[Index].SubItems[0] := Tmp;
end;

// !!!!! This is the routine that does everything !!!!
procedure TTPMSetup.SetItemTotal(Index : Integer; Item : TTPMItem; Value : Int64);
begin
  Item.Total := Value;
  if not (cstCycle in SysObj.IO.StatusType) then
    SetItemFault(Item);
  if Visible then begin
    UpdateDisplayItemTotal(Index, Item);
  end;
end;

procedure TTPMSetup.SetItemFault(Item : TTPMItem);
var NewFault : TFaultLevel;
begin
  if Item.Total >= Item.Limit then
    NewFault := FaultStopCycle
  else if Item.Total >= Item.Limit * (Item.WarnLimit / 100) then
    NewFault := FaultWarning
  else
    NewFault := FaultNone;

  if Item.FaultLevel <> NewFault then begin
    if Item.FID <> 0 then
      SysObj.FaultInterface.FaultResetID(Item.FID);
    if NewFault <> FaultNone then begin
      Item.FaultLevel := NewFault;
      case Item.FaultLevel of
        FaultInterlock :
          Item.FID := SysObj.FaultInterface.SetFault(SysObj.Comms, TPMFaults[tpmfInterlock], [Item.TPMMessage], ItemFaultReset);
        FaultWarning :
          Item.FID := SysObj.FaultInterface.SetFault(SysObj.Comms, TPMFaults[tpmfWarning], [Item.TPMMessage], ItemFaultReset);
        FaultStopCycle :
          Item.FID := SysObj.FaultInterface.SetFault(SysObj.Comms, TPMFaults[tpmfStopCycle], [Item.TPMMessage], ItemFaultReset);
        FaultEStop :
          Item.FID := SysObj.FaultInterface.SetFault(SysObj.Comms, TPMFaults[tpmfEStop], [Item.TPMMessage], ItemFaultReset);
        FaultFatal :
          Item.FID := SysObj.FaultInterface.SetFault(SysObj.Comms, TPMFaults[tpmfFatal], [Item.TPMMessage], ItemFaultReset);
      else
        Item.FID := SysObj.FaultInterface.SetFault(SysObj.Comms, TPMFaults[tpmfEStop], [Item.TPMMessage + ' *Invalid Level*'], ItemFaultReset);
      end;
      DynamicHelp.DynamicHelpSystem.Add(Item.Help, '<h3>TPMEvent </h3>' + Item.TPMMessage);
    end else begin
      Item.FID := 0;
      Item.FaultLevel := FaultNone;
    end;
    ListView.Invalidate;
  end;
end;

procedure TTPMSetup.TagEvents(aTag : TAbstractTag);
var Item : TTPMItem;
    I : Integer;
begin
  for I := 0 to ItemList.Count - 1 do begin
    Item := TTPMItem(ItemList[I]);
    if Item.Input = aTag then begin
      case Item.TPMType of
        tpmRiseCounter : if aTag.AsBoolean then
                             Inc(Item.Total);
        tpmFallCounter : if not aTag.AsBoolean then
                             Inc(Item.Total);
        tpmChangeCounter :
                             Inc(Item.Total);
      end;
    end;
  end;
end;

function TTPMSetup.ItemFaultReset(FID : FHandle) : Boolean;
begin
  Result := False;
end;

procedure TTPMSetup.ResetCounterQuestion(Sender : TObject);
var Item : TTPMItem;
begin
  if Assigned(ListView.Selected) then begin
    Item := TTPMItem(ListView.Selected.Data);
    if (SysObj.Comms.AccessLevels * Item.AccessLevels) <> [] then begin
      if AreYouSure('Reset the counter for ' + Item.Name) then
        SetItemTotal(ListView.Selected.Index, Item, 0);
    end else begin
      SysObj.FaultInterface.SetFault(Self, TPMFaults[tpmfHigherUserLevel], [], nil);
    end;
  end;
end;

procedure TTPMSetup.UpdateDisplay(aTag : TAbstractTag);
var Item : TTPMItem;
    I : Integer;
begin
  for I := 0 to ItemList.Count - 1 do begin
    Item := TTPMItem(ItemList[I]);
    case Item.TPMType of
      tpmOnTimer : if Item.Input.AsBoolean then
                     Item.Total := Item.Total + (CNC32.PlcTick.I64 - LastTick);
      tpmOffTimer : if not Item.Input.AsBoolean then
                     Item.Total := Item.Total + (CNC32.PlcTick.I64 - LastTick);
    end;
    SetItemTotal(I, Item, Item.Total);
  end;

  TPMTagList.SetOutputs;

  LastTick := CNC32.PlcTick.I64;
end;

function  TTPMSetup.HoursFormat(Ticks : Int64; var DisplayText :String) : Integer;
var
  Seconds : Integer;
  Minutes : Integer;
  Hours : Integer;
  Remainder : Integer;
begin
  Ticks := Ticks div 1000;
  Hours   := Ticks div (3600);
  Remainder := Ticks - (Hours*3600);
  Minutes := Remainder div 60;
  Seconds := Ticks-((Minutes*60) + (Hours*3600));
  DisplayText := Format('%.2d:%.2d:%.2d',[Hours,Minutes,Seconds]);
  Result := Hours;
end;


procedure TTPMSetup.Configure(S : TQuoteTokenizer);
  procedure ReadItem;
  var Item : TTPMItem;
  begin
    Item := TTPMItem.Create;
    ItemList.Add(Item);
    Item.Configure(S);
  end;

var Token : string;
begin
  S.Rewind;
  S.Seperator := '={}[]';
  S.QuoteChar := '''';
  S.ExpectedTokens([Name, '=', '{']);
  Token := LowerCase(S.Read);

  while not S.EndOfStream do begin
    if Token = 'item' then
      ReadItem
    else if Token = '}' then
      Exit
    else
      raise Exception.CreateFmt('Unexpected Token %s', [Token]);
    Token := LowerCase(S.Read);
  end;
end;


{ TTPMTagList }

procedure TTPMTagList.Add(aItem: TTPMItem);
var I : Integer;
    Item : TTPMItemList;
begin
  for I := 0 to TagList.Count - 1 do begin
    Item := TTPMItemList(TagList[I]);
    if Item.Tag = aItem.Output then begin
      Item.Add(aItem);
      Exit;
    end;
  end;
  TagList.Add(TTPMItemList.Create(aItem));
end;

constructor TTPMTagList.Create;
begin
  inherited Create;
  TagList := TList.Create;
end;

procedure TTPMTagList.SetOutputs;
var I : Integer;
    ItemList : TTPMItemList;
    B : Boolean;
begin
  for I := 0 to TagList.Count - 1 do begin
    ItemList := TTPMItemList(TagList[I]);
    B := ItemList.CheckOn;
    if B <> ItemList.Tag.AsBoolean then
      ItemList.Tag.AsBoolean := B;
  end;
end;

{ TTPMItemList }

procedure TTPMItemList.Add(aItem: TTPMItem);
begin
  ItemList.Add(aItem);
end;

function TTPMItemList.CheckOn: Boolean;
var Item : TTPMItem;
    I : Integer;
begin
  Result := False;
  for I := 0 to ItemList.Count - 1 do begin
    Item := TTPMItem(ItemList[I]);
    if Item.Total > Item.Limit then
      Result := True;
  end;
end;

constructor TTPMItemList.Create(aItem: TTPMItem);
begin
  inherited Create;
  ItemList := TList.Create;
  Tag := aItem.Output;
  ItemList.Add(aItem);
end;

{ TTPMItem }

constructor TTPMItem.Create;
begin
  inherited Create;
  AccessLevels := [AccessLevelMaintenance..AccessLevelOEM2];
  Input := nil;
  Output := nil;
  WarnLimit := 90;
  Limit := 1000;
  TPMType := tpmRiseCounter;
  TPMMessage := 'NOT SET';
  Total := 0;
  FID := 0;
  FaultLevel := FaultWarning;
end;

procedure TTPMItem.Configure(S: TStringTokenizer);
var Token : string;

  function ReadDoubleAssign : Double;
  begin
    S.ExpectedToken('=');
    Result := StrToFloat(S.Read);
  end;

  function ReadTPMType : TTPMType;
  begin
    S.ExpectedToken('=');
    Token := LowerCase(S.Read);
    if Token = 'ontimer' then
      Result := tpmOnTimer
    else if Token = 'offtimer' then
      Result := tpmOffTimer
    else if Token = 'risecounter' then
      Result := tpmRiseCounter
    else if Token = 'fallcounter' then
      Result := tpmFallCounter
    else if Token = 'changecounter' then
      Result := tpmChangeCounter
    else
      raise Exception.CreateFmt('Invalid TPM type [%s]', [Token]);
  end;

var Tmp : Double;
begin
  S.ExpectedTokens(['=', '{']);
  Token := LowerCase(S.Read);
  Tmp := 10;

  try
    while not S.EndOfStream do begin
      if Token = 'name' then
        Name := S.ReadStringAssign
      else if Token = 'input' then
        InputName := S.ReadStringAssign
      else if Token = 'message' then
        TPMMessage := SysObj.Localized.GetString( S.ReadStringAssign )
      else if Token = 'output' then
        OutputName := S.ReadStringAssign
      else if Token = 'warnlimit' then
        WarnLimit := ReadDoubleAssign
      else if Token = 'limit' then
        Tmp := ReadDoubleAssign
      else if Token = 'type' then
        TPMType := ReadTPMType
      else if Token = 'accesslevel' then
        AccessLevels := TAccessLevels(Byte(S.ReadIntegerAssign mod (Ord(High(TAccessLevel))+1)))
      else if Token = 'help' then
        Help := S.ReadStringAssign
      else if Token = '}' then
        Exit
      else
        raise Exception.CreateFmt('Unexpected Token %s', [Token]);
      Token := LowerCase(S.Read);
    end;
  finally
    case TPMType of
      tpmOffTimer, tpmOnTimer : Limit := Round(Tmp * 3600000);
    else
      Limit := Round(Tmp);
    end;
  end;
end;

initialization
  RegisterCNCClass(TTPMSetup);
  RegisterFaultMap('TPM', TPMFaults, Length(TPMFaults), 3000);
end.
