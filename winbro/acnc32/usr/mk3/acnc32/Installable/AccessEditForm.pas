unit AccessEditForm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, CNCTypes, CNC32, SimpleTapReader, CoreCNC;

type
  PAccessRecord = ^TAccessRecord;
  TAccessRecord = record
    UserName : string;
    PassWord : string;
    CardID : string;
    ClockID : string;
    case Byte of
      0 : ( AccessLevel : TAccessLevel );
      1 : ( AccessLevels : TAccessLevels );
  end;

  TSecurityFile = class(TList)
  private
    ExStyle : Boolean;
    function GetAccessRecord(Index : Integer) : PAccessRecord;
    function Encrypt(const InString:string; StartKey,MultKey,AddKey:Integer): string;
    function Decrypt(const InString:string; StartKey,MultKey,AddKey:Integer): string;
  public
    constructor Create(aExStyle : Boolean);
    destructor Destroy; override;
    procedure LoadFromFile(aFileName : string);
    procedure SaveToFile(aFileName : string);
    property Access [Index : Integer] : PAccessRecord read GetAccessRecord;
    procedure AddAccessRecord(const aAccess : TAccessRecord);
    procedure DeleteAccess(Index : Integer);
    function Check(aText : string) : PAccessRecord;
  end;

  TAccessEditFrm = class(TForm)
    AccessPanel: TPanel;
    AccessGroup: TRadioGroup;
    GroupBox1: TGroupBox;
    UserNameList: TListBox;
    Panel1: TPanel;
    OKBtn: TBitBtn;
    CancelBtn: TBitBtn;
    RemoveBtn: TBitBtn;
    AddBtn: TBitBtn;
    procedure OKBtnClick(Sender: TObject);
    procedure RemoveBtnClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure AddBtnClick(Sender: TObject);
    procedure AccessGroupClick(Sender: TObject);
    procedure UserNameListClick(Sender: TObject);
  private
    FileName : string;
    FSecurityFile : TSecurityFile;
    AccessRecord : TAccessRecord;
    procedure SetSecurityFile(aSecurityFile : TSecurityFile);
    function SecurityCheck(aText : string) : Boolean;
  public
    class procedure Execute(aFileName : string); overload;
    property SecurityFile : TSecurityFile read FSecurityFile write SetSecurityFile;
  end;

var
  AccessEditFrm: TAccessEditFrm;

implementation

{$R *.DFM}

const
  NullAccessRecord : TAccessRecord = (
     UserName   : 'None' ;
     PassWord   : '' ;
     CardID     : '' ;
     ClockID    : '';
     AccessLevel: AccessLevelNone;
  );

class procedure TAccessEditFrm.Execute({Comm : TSwipeComm;} aFileName : string);
var
  AccEdit : TAccessEditFrm;
  SecurityFile : TSecurityFile;
begin
  SecurityFile := TSecurityFile.Create(False);
  SecurityFile.LoadFromFile(aFileName);

  AccEdit := TAccessEditFrm.Create(Application);
  try
    AccEdit.FileName := aFileName;
    AccEdit.SecurityFile := SecurityFile;
//    AccEdit.Comm := Comm;
    AccEdit.ShowModal;
  finally
    AccEdit.Release;
  end;
end;


procedure TAccessEditFrm.SetSecurityFile(aSecurityFile : TSecurityFile);
var I : Integer;
begin
  FSecurityFile := aSecurityFile;
  UserNameList.Items.Clear;
  for I := 0 to SecurityFile.Count - 1 do begin
    UserNameList.Items.Add(SecurityFile.Access[I]^.UserName);
  end;
  if UserNameList.Items.Count > 0 then begin
    UserNameList.ItemIndex := 0;
    AccessGroup.ItemIndex := Ord(SecurityFile.Access[0]^.AccessLevel)
  end;
end;

procedure TAccessEditFrm.AccessGroupClick(Sender: TObject);
begin
  if UserNameList.ItemIndex <> -1 then begin
    SecurityFile.Access[UserNameList.ItemIndex]^.AccessLevel :=
      TAccessLevel(AccessGroup.ItemIndex);
  end;
end;

procedure TAccessEditFrm.OKBtnClick(Sender: TObject);
begin
  SecurityFile.SaveToFile(FileName);
end;

procedure TAccessEditFrm.RemoveBtnClick(Sender: TObject);
begin
  if UserNameList.ItemIndex <> -1 then
    SecurityFile.DeleteAccess(UserNameList.ItemIndex);

  SecurityFile := SecurityFile;
end;


procedure TAccessEditFrm.AddBtnClick(Sender: TObject);
var aText : string;
begin
  if InputQuery( SysObj.Localized.GetString('$NEW_USER'),
                 SysObj.Localized.GetString('$ENTER_USER_NAME'), aText) then begin
    if aText <> '' then begin
      AccessRecord.UserName := aText;
      AccessRecord.AccessLevel := AccessLevelOperator;
      AccessRecord.PassWord := '';
      AccessRecord.CardID := '';
      AccessRecord.ClockID := '';
    end;
{    if Comm <> nil then begin
      if TSwipeReaderDlg.Read(Comm, Format(NewPromptFormat, [AccessRecord.UserName]), SecurityCheck) then begin
        aText := AccessRecord.CardID;
        if TSwipeReaderDlg.Read(Comm, Format(NewAgainPromptFormat, [AccessRecord.UserName]), SecurityCheck) then begin
          if aText = AccessRecord.CardID then begin
            SecurityFile.AddAccessRecord(AccessRecord);
            SecurityFile := SecurityFile;
          end;
        end;
      end;
    end else begin }

    if TSimpleTapForm.Execute(Format(SysObj.Localized.GetString('$ENTER_S_SECURITY_PASSWORD'),
      [AccessRecord.UserName]), SecurityCheck) then begin
      aText := AccessRecord.CardID;

      if TSimpleTapForm.Execute(
           Format(SysObj.Localized.GetString('$ENTER_S_SECURITY_PASSWORD_AGAIN'),
          [AccessRecord.UserName]), SecurityCheck) then begin
          if aText = AccessRecord.CardID then begin
            SecurityFile.AddAccessRecord(AccessRecord);
            SecurityFile := SecurityFile;
          end;
        end;
      end;
//    end;
  end;
end;

function TAccessEditFrm.SecurityCheck(aText : string) : Boolean;
begin
  AccessRecord.CardID := aText;
  Result := True;
end;


function TSecurityFile.GetAccessRecord(Index : Integer) : PAccessRecord;
begin
  Result := PAccessRecord(Items[Index]);
end;


function TSecurityFile.Check(aText : string) : PAccessRecord;
var I : Integer;
begin
  Result := @NullAccessRecord;
  for I := 0 to Self.Count - 1 do begin
    if Access[I]^.CardID = aText then
      Result := Access[I];
  end;
end;

procedure TAccessEditFrm.UserNameListClick(Sender: TObject);
begin
  if UserNameList.ItemIndex <> -1 then begin
    if UserNameList.ItemIndex < SecurityFile.Count then begin
      AccessGroup.ItemIndex := Ord(SecurityFile.Access[UserNameList.ItemIndex]^.AccessLevel);
    end;
  end;
end;

procedure TAccessEditFrm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  SecurityFile.Free;
end;

procedure TSecurityFile.AddAccessRecord(const aAccess : TAccessRecord);
var AccessRecord : PAccessRecord;
begin
  New(AccessRecord);
  AccessRecord^ := aAccess;
  Self.Add(AccessRecord);
end;

procedure TSecurityFile.DeleteAccess(Index : Integer);
begin
  Dispose(Access[Index]);
  Self.Delete(Index);
end;

constructor TSecurityFile.Create(aExStyle : Boolean);
begin
  ExStyle := aExStyle;
end;

destructor TSecurityFile.Destroy;
begin
  while Count > 0 do begin
    Dispose(Access[0]);
    Self.Delete(0);
  end;
  inherited;
end;

const
  StartKey= 981;  {Start default key}
  MultKey  = 12674;{Mult default key}
  AddKey  = 35891;{Add default key}
{*******************************************************
 * Standard Encryption algorithm - Copied from Borland *
 *******************************************************}
function TSecurityFile.Encrypt(const InString:string; StartKey,MultKey,AddKey:Integer): string;
var
  I : Byte;
begin
  Result := '';
  for I := 1 to Length(InString) do begin
    Result := Result + CHAR(Byte(InString[I]) xor (StartKey shr 8));
    StartKey := (Byte(Result[I]) + StartKey) * MultKey + AddKey;
  end;
end;
{*******************************************************
 * Standard Decryption algorithm - Copied from Borland *
 *******************************************************}
function TSecurityFile.Decrypt(const InString:string; StartKey,MultKey,AddKey:Integer): string;
var
  I : Byte;
begin
  Result := '';
  for I := 1 to Length(InString) do begin
    Result := Result + CHAR(Byte(InString[I]) xor (StartKey shr 8));
    StartKey := (Byte(InString[I]) + StartKey) * MultKey + AddKey;
  end;
end;


procedure TSecurityFile.LoadFromFile(aFileName : string);
var S : TStringList;
    W : string;
    I : Integer;
    AccessREcord : TAccessRecord;
    Stream : TFileStream;
    Character : Char;
begin
  S := TStringList.Create;
  try
    Stream := TFileStream.Create(aFileName, fmOpenRead);
    try
      while Stream.Read(Character, 1) = 1 do begin
        W := W + Character;
      end;

      S.Text := Decrypt(W, StartKey, MultKey, AddKey);

      try
        for I := 0 to S.Count - 1 do begin
          W := S[I];
          AccessRecord.UserName := ReadDelimited(W, '~');
          AccessRecord.AccessLevel := TAccessLevel(StrToInt(ReadDelimited(W, '~')));
          AccessRecord.CardID := ReadDelimited(W, '~');
          AccessRecord.PassWord := ReadDelimited(W, '~');
          AccessRecord.ClockID := ReadDelimited(W, '~');

          AddAccessRecord(AccessRecord);
        end;
      except
        on E : Exception do Exit;
      end;
    finally
      S.Free;
      Stream.Free;
    end;
  except
    on E : EFOpenError do Exit;
  end;
end;

procedure TSecurityFile.SaveToFile(aFileName : string);
var I : Integer;
    S : TStringList;
    Stream : TFileStream;
    W : string;
begin
  S := TStringList.Create;
  try
    for I := 0 to Self.Count - 1 do begin
      S.Add(
            Format('%s~ %d~ %s~ %s~ %s', [
                   Access[I]^.UserName,
                   Ord(Access[I]^.AccessLevel),
                   Access[I]^.CardID,
                   Access[I]^.PassWord,
                   Access[I]^.ClockID ]));
    end;

    try
      Stream := TFileStream.Create(aFileName, fmCreate);
      try
        W := Encrypt(S.Text, StartKey, MultKey, AddKey);
        Stream.Write(PChar(W)^, Length(W));
      finally
        Stream.Free;
      end;
    except
      on E : EFOpenError do
        Exit;
    end;

  finally
    S.Free;
  end;
end;


{Contributor: ANATOLY PODGORETSKY

unit Crypt32;

*************************************************************************
* Name:Crypt32.Pas  *
* Description:32 bits encode/decode module  *
*        2^96 variants it is very high to try hack*
* Purpose:  Good for encrypting passwors and text*
* Security: avoid use StartKey less than 256*
*if it use only for internal use you may use default *
*key, but MODIFY unit before compiling*
* Call:Encrypted := Encrypt(InString,StartKey,MultKey,AddKey)*
*Decrypted := Decrypt(InString,StartKey)  *
* Parameters:InString  = long string (max 2 GB) that need to encrypt*
*decrypt  *
*MultKey= MultKey key*
*AddKey= Second key        *
*StartKey= Third key*
*(posible use defaults from interface)*
* Return:OutString= result string*
* Editor:Besr viewed with Tab stops = 2, Courier new*
* Started:01.08.1996*
* Revision:22.05.1997 - ver.2.01 converted from Delphi 1*
*and made all keys as parameters, before only start key*
* Platform:Delphi 2.0, 3.0   *
* work in Delphi 1.0, 2^48 variants, 0..255 strings*
* Author:Anatoly Podgoretsky  *
* Base alghoritm from Borland*
* Address:Vahe 4-31, Johvi, Estonia, EE2045, tel. 61-142    *
*kvk@estpak.ee  *
* Status:Freeware, but any sponsor help will be appreciated here*
*need to buy books, shareware products, tools etc*
*************************************************************************
* Modified:     Supports Delphi 1.0 & 2.0                         *
*               Overflow checking removed                         *
* By:           Martin Djerns                                    *
* e-mail:       djernaes@einstein.ot.dk                           *
* web:          einstein.ot.dk/~djernaes                          *
*************************************************************************
}

end.
