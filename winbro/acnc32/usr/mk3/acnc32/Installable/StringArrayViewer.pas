unit StringArrayViewer;

interface

uses SysUtils, Classes, CoreCNC, CNCAbs, IStringArray, ComCtrls, StdCtrls, ExtCtrls,
     Controls, Windows, Graphics, CommCtrl, Named, CNCTypes, CNC32;

type
  TStringArrayViewer = class(TExpandDisplay, IACNC32StringArrayNotify, IACNC32StringArrayNotifyA)
  private
    ComboBox: TComboBox;
    Panel: TPanel;
    ListView: TListView;
    Table : IACNC32StringArrayA;
    ExtentColumn: array [0..100] of Integer;
    DefaultTable: string;
    FirstTime: Boolean;
    UseIndexColumn: Boolean;
    TableTag: TAbstractTag;

    procedure DisplaySweep(ATag: TAbstractTag);
    procedure RedrawTable(WithExtent: Boolean = False);
    procedure InitializeTable;
    procedure ComboBoxChange(Sender: TObject);
    procedure UpdateComboBox;
    procedure ConnectToTable(TableName: string);
    procedure CustomDrawItem(Sender: TCustomListView; Item: TListItem; State: TCustomDrawState; var DefaultDraw: Boolean);
    procedure CustomDraw(Sender: TCustomListView; const ARect: TRect; var DefaultDraw: Boolean);
    procedure ListViewOnSelectItem(Sender: TObject; Item: TListItem; Selected: Boolean);
    procedure SelectedTableChange(ATag: TAbstractTag);
  public
    constructor Create(AOwner: TComponent); override;
    procedure InstallComponent(Params: TParamStrings); override;
    procedure Installed; override;
    procedure TableChange(Table : IACNC32StringArray); stdcall;
    procedure CellChange(Table : IACNC32StringArray; Row, Col : Integer); stdcall;
    procedure CursorChange(Table: IACNC32StringArrayA; ACursor: Integer); stdcall;
    function GetName: WideString; stdcall;
    procedure Open; override;
  end;

implementation


{ TStringArrayViewer }

procedure TStringArrayViewer.CellChange(Table: IACNC32StringArray; Row,
  Col: Integer);
var V : WideString;
begin
  Table.GetValue(Row, Col, V);
  if not UseIndexColumn then begin
    if Col = 0 then
      ListView.Items[Row].Caption:= V
    else
      ListView.Items[Row].SubItems[Col - 1]:= V;
  end else begin
    ListView.Items[Row].SubItems[Col]:= V;
  end;
end;

procedure TStringArrayViewer.ComboBoxChange(Sender: TObject);
var ArrayName: string;
begin
  if ComboBox.ItemIndex <> -1 then begin
    ArrayName:= ComboBox.Items[ComboBox.ItemIndex];
    ConnectToTable(ArrayName);
  end;
end;

procedure TStringArrayViewer.ConnectToTable(TableName: string);
var T: IAcnc32StringArray;
begin
  if Table <> nil then begin
    SysObj.StringArraySet.ReleaseArray(Self, Table);
    Table:= nil;
  end;
  SysObj.StringArraySet.UseArrayA(TableName, Self, Table);
  if Table = nil then begin
    SysObj.StringArraySet.CreateArray(TableName, Self, T);
    SysObj.StringArraySet.ReleaseArray(Self, T);
    SysObj.StringArraySet.UseArrayA(TableName, Self, Table);
  end;


  InitializeTable;
end;

constructor TStringArrayViewer.Create(AOwner: TComponent);
begin
  inherited;
  FirstTime:= True;
  ParentFont:= False;
  Font.Name:= SysObj.FontName;
  Font.Style:= [fsBold];

  ComboBox:= TComboBox.Create(Self);
  Panel:= TPanel.Create(Self);
  ListView:= TListView.Create(Self);
  ListView.Parent:= Self;
  ListView.Align:= alClient;
  ListView.HideSelection := True;
  ListView.GridLines := True;
  ListView.OnCustomDrawItem := CustomDrawItem;
  ListView.OnCustomDraw:= CustomDraw;
  ListView.OnSelectItem := ListViewOnSelectItem;
  ListView.ReadOnly := True;
  ListView.ViewStyle := vsReport;
end;

procedure TStringArrayViewer.CursorChange(Table: IACNC32StringArrayA;
  ACursor: Integer);
begin
end;

procedure TStringArrayViewer.CustomDraw(Sender: TCustomListView;
  const ARect: TRect; var DefaultDraw: Boolean);
begin
  DefaultDraw:= True;
end;

procedure TStringArrayViewer.CustomDrawItem(Sender: TCustomListView;
  Item: TListItem; State: TCustomDrawState; var DefaultDraw: Boolean);

var Widths : array [0..100] of Integer;

  procedure DrawSub(const aRect : TRect; SubItem: Integer);
  var I : Integer;
      Left : Integer;
      Rect : TRect;
  begin
    Widths[SubItem] := ListView_GetColumnWidth(ListView.Handle, SubItem);
    Left := aRect.Left;
    for I := 0 to SubItem - 1 do
      Inc(Left, Widths[I]);

    Rect := Item.DisplayRect(drBounds);
    Rect.Left := Left;
    Rect.Right := Rect.Left + Widths[SubItem];

    ListView.Canvas.TextRect(Rect, Rect.Left + 5, Rect.Top, Item.SubItems[SubItem - 1]);
  end;

var I: Integer;
    Rect: TRect;
begin
  if Assigned(Table) then begin
    if Table.Cursor = Item.Index then
      ListView.Canvas.Brush.Color := $ffa0a0
    else if Odd(Item.Index) then
      ListView.Canvas.Brush.Color := $fffff8
    else
      ListView.Canvas.Brush.Color := $f0e8f0;

    Widths[0] := ListView_GetColumnWidth(ListView.Handle, 0);
    Rect := Item.DisplayRect(drBounds);
    Rect.Right := Rect.Left + Widths[0];
    ListView.Canvas.TextRect(Rect, Rect.Left + 5, Rect.Top, Item.Caption);

    for I := 0 to Item.SubItems.Count - 1 do begin
      DrawSub(Rect, I + 1);
    end;
    defaultdraw := False;
  end;
end;

procedure TStringArrayViewer.DisplaySweep(ATag: TAbstractTag);
var S: TListItem;
    SR: TRect;
begin
  if Table <> nil then
    if Table.Cursor <> ListView.ItemIndex then
      if ListView.Items.Count > Table.Cursor then begin
         ListView.ItemIndex:= Table.Cursor;
         S:= ListView.Selected;
         SR:= S.DisplayRect(drBounds);

         if SR.Top > Height div 2 then begin
           ListView.Scroll(0, SR.Top - (Height div 2));
         end;

         if SR.Top < 0 then
           ListView.Scroll(0, SR.Top);

      end;
end;

function TStringArrayViewer.GetName: WideString;
begin
  Result:= Name;
end;

procedure TStringArrayViewer.InitializeTable;
var I : Integer;
    S : WideString;
    lc : TListColumn;
begin
  ListView.Items.BeginUpdate;
  try

    if Table <> nil then begin
      ListView.Columns.Clear;
      ListView.Items.Clear;

      if UseIndexColumn then begin
        lc := ListView.Columns.Add;
        lc.Caption := 'Index';
      end;

      for I:= 0 to Table.FieldCount - 1 do begin
        Table.FieldNameAtIndex(I, S);
        lc := ListView.Columns.Add;
        lc.Caption := S;
        lc.Width := 100;
      end;
    end;

    RedrawTable(True);
    for I:= 0 to ListView.Columns.Count - 1 do begin
      ListView.Columns[I].Width:= ExtentColumn[I];
    end;
  finally
    ListView.Items.EndUpdate
  end;
end;

procedure TStringArrayViewer.InstallComponent(Params: TParamStrings);
var I: Integer;
begin
  inherited;
  Panel.Parent:= Self;
  ComboBox.Parent:= Panel;
  Panel.Align:= alTop;
  Panel.Visible:= Params.ReadBool('CanSelectTable', True);
  Panel.Height:= 22;
  ComboBox.OnChange:= ComboBoxChange;

  DefaultTable:= Params.ReadString('DefaultTable', '');
  ListView.Canvas.Font:= Font;
  //if DefaultTable <> '' then
  //  ConnectToTable(DefaultTable);
  UseIndexColumn:= Params.ReadBool('UseIndexColumn', False);

  UpdateComboBox;

  for I:= 0 to ComboBox.Items.Count - 1 do begin
    if CompareText(ComboBox.Items[I], DefaultTable) = 0 then begin
      ComboBox.ItemIndex:= I;
      Break;
    end;
  end;

  TableTag:= Self.TagPublish(Name + 'Table', TStringTag);
end;

procedure TStringArrayViewer.Installed;
begin
  inherited;
  if DefaultTable <> '' then
    ConnectToTable(DefaultTable);

  TagEvent(NameDisplaySweep, EdgeChange, TMethodTag, DisplaySweep);
  TagEvent(Name + 'Table', EdgeChange, TStringTag, SelectedTableChange);
end;

procedure TStringArrayViewer.ListViewOnSelectItem(Sender: TObject;
  Item: TListItem; Selected: Boolean);
begin
  if Assigned(Table) then
    Table.SetCursor(Item.Index, Self);
end;

procedure TStringArrayViewer.Open;
begin
  inherited;
end;

procedure TStringArrayViewer.RedrawTable(WithExtent: Boolean = False);
var I, K : Integer;
    S : WideString;
    LI: TListItem;
    Tmp: Integer;
begin
  if WithExtent then begin
    for K:= 0 to ListView.Columns.Count - 1 do
      ExtentColumn[K]:= ListView.Canvas.TextWidth(ListView.Columns[K].Caption) + 15;
  end;

  for I:= 0 to Table.RecordCount - 1 do begin
    LI := ListView.Items.Add;

    if UseIndexColumn then
      LI.Caption := IntToStr(I);

    for K:= 0 to Table.FieldCount - 1 do begin
      Table.GetValue(I, K, S);

      if not UseIndexColumn and (K = 0) then
        LI.Caption:= S
      else
        LI.SubItems.Add(S);

      if WithExtent then begin
        Tmp:= ListView.Canvas.TextWidth(S) + 15;
        if Tmp > ExtentColumn[K + 1] then
          ExtentColumn[K + 1]:= Tmp;
      end;
    end;
  end;
end;

procedure TStringArrayViewer.SelectedTableChange(ATag: TAbstractTag);
begin
  ComboBox.Text:= ATag.AsString;
  try
   ConnectToTable(ATag.AsString);
  except
    on E: Exception do
      EventLog('Selected table change - ' + E.Message);
  end;
end;

procedure TStringArrayViewer.TableChange(Table: IACNC32StringArray);
begin
  try
    InitializeTable;
  except
    on E: Exception do begin
      EventLog('TStringArrayViewer.TableChange Exception: ' + E.Message);
    end;
  end;
end;

procedure TStringArrayViewer.UpdateComboBox;
var S : WideString;
    I : Integer;
begin
  ComboBox.Items.Clear;
  for I := 0 to SysObj.StringArraySet.ArrayCount - 1 do begin
    SysObj.StringArraySet.AliasAtIndex(I, S);
    ComboBox.Items.Add(S);
  end;
end;

initialization
  CoreCNC.RegisterCNCClass(TStringArrayViewer, 'MDTViewer');
end.
