object StdEditorForm: TStdEditorForm
  Left = 1002
  Top = 195
  Width = 531
  Height = 424
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -10
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = True
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 368
    Width = 523
    Height = 29
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    object CursorLabel: TLabel
      Left = 192
      Top = 8
      Width = 7
      Height = 13
      Caption = 'X'
    end
    object OK: TBitBtn
      Left = 29
      Top = 0
      Width = 65
      Height = 26
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      Kind = bkOK
    end
    object Cancel: TBitBtn
      Left = 107
      Top = 0
      Width = 65
      Height = 26
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      Kind = bkCancel
    end
  end
end
