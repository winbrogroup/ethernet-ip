unit ICImagingControl3_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// PASTLWTR : 1.2
// File generated on 24/05/2011 09:51:20 from Type Library described below.

// ************************************************************************  //
// Type Lib: C:\Program Files\Common Files\IC Imaging Control 3.2\ActiveX\icimagingcontrol.ocx (1)
// LIBID: {E0110BE7-EBF0-4612-B2F8-817194557140}
// LCID: 0
// Helpfile: C:\Program Files\Common Files\IC Imaging Control 3.2\ActiveX\ICImagingControl.chm
// HelpString: ICImagingControl 3.0 Type Library
// DepndLst: 
//   (1) v2.0 stdole, (C:\WINDOWS\system32\stdole2.tlb)
// Errors:
//   Hint: Symbol 'Type' renamed to 'type_'
//   Hint: Symbol 'Type' renamed to 'type_'
//   Hint: Parameter 'Type' of IVideoFormat.FrameType changed to 'Type_'
//   Hint: Parameter 'Type' of IAviCompressor.FrameType changed to 'Type_'
//   Hint: Member 'String' of 'IVCDMapStringsProperty' changed to 'String_'
//   Hint: Parameter 'String' of IVCDMapStringsProperty.String changed to 'String_'
//   Hint: Parameter 'String' of IVCDMapStringsProperty.String changed to 'String_'
//   Hint: Parameter 'Type' of IBaseSink.SinkType changed to 'Type_'
//   Hint: Parameter 'Type' of IMediaStreamSink.Codec changed to 'Type_'
//   Hint: Parameter 'Type' of IMediaStreamSink.Codec changed to 'Type_'
//   Hint: Parameter 'Object' of IICImagingControl.OverlayBitmap changed to 'Object_'
//   Hint: Parameter 'String' of IICImagingControl.SaveDeviceState changed to 'String_'
//   Hint: Parameter 'String' of IICImagingControl.LoadDeviceState changed to 'String_'
//   Hint: Parameter 'Object' of IICImagingControl.OverlayBitmapAtPath changed to 'Object_'
//   Error creating palette bitmap of (TCICPropertyPage) : List index out of bounds (37)
//   Error creating palette bitmap of (TMediaStreamSink) : List index out of bounds (78)
//   Error creating palette bitmap of (TFrameType) : List index out of bounds (79)
//   Error creating palette bitmap of (TFrameTypes) : List index out of bounds (81)
//   Error creating palette bitmap of (TFrameHandlerSink) : List index out of bounds (83)
// ************************************************************************ //
// *************************************************************************//
// NOTE:                                                                      
// Items guarded by $IFDEF_LIVE_SERVER_AT_DESIGN_TIME are used by properties  
// which return objects that may need to be explicitly created via a function 
// call prior to any access via the property. These items have been disabled  
// in order to prevent accidental use from within the object inspector. You   
// may enable them by defining LIVE_SERVER_AT_DESIGN_TIME or by selectively   
// removing them from the $IFDEF blocks. However, such items must still be    
// programmatically created via a method of the appropriate CoClass before    
// they can be used.                                                          
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
{$WARN SYMBOL_PLATFORM OFF}
{$WRITEABLECONST ON}
{$VARPROPSETTER ON}
interface

uses Windows, ActiveX, Classes, Graphics, OleCtrls, OleServer, StdVCL, Variants;
  


// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  ICImagingControl3MajorVersion = 1;
  ICImagingControl3MinorVersion = 0;

  LIBID_ICImagingControl3: TGUID = '{E0110BE7-EBF0-4612-B2F8-817194557140}';

  IID_IDevice: TGUID = '{E0110BE7-EBF0-4612-B2F8-817194557DFE}';
  IID_IDevices: TGUID = '{E0110BE7-EBF0-4612-B2F8-817194557E00}';
  IID_IFrameType: TGUID = '{E0110BE7-EBF0-4612-B2F8-817194557E20}';
  IID_IImageBuffer: TGUID = '{E0110BE7-EBF0-4612-B2F8-817194557DFA}';
  IID_IImageBuffers: TGUID = '{E0110BE7-EBF0-4612-B2F8-817194557DFC}';
  IID_IVideoFormat: TGUID = '{E0110BE7-EBF0-4612-B2F8-817194557E02}';
  IID_IVideoFormats: TGUID = '{E0110BE7-EBF0-4612-B2F8-817194557E04}';
  IID_IVideoNorm: TGUID = '{E0110BE7-EBF0-4612-B2F8-817194557E06}';
  IID_IVideoNorms: TGUID = '{E0110BE7-EBF0-4612-B2F8-817194557E08}';
  IID_IInputChannel: TGUID = '{E0110BE7-EBF0-4612-B2F8-817194557E0A}';
  IID_IInputChannels: TGUID = '{E0110BE7-EBF0-4612-B2F8-817194557E0C}';
  IID_IDeviceFrameRates: TGUID = '{E0110BE7-EBF0-4612-B2F8-817194557E12}';
  IID_IAviCompressor: TGUID = '{E0110BE7-EBF0-4612-B2F8-817194557E0E}';
  IID_IAviCompressors: TGUID = '{E0110BE7-EBF0-4612-B2F8-817194557E10}';
  IID_IOverlayBitmap: TGUID = '{E0110BE7-EBF0-4612-B2F8-817194557E14}';
  CLASS_OverlayBitmap: TGUID = '{E0110BE7-EBF0-4612-B2F8-817194557E15}';
  CLASS_ImageBuffer: TGUID = '{E0110BE7-EBF0-4612-B2F8-817194557DFB}';
  CLASS_ImageBuffers: TGUID = '{E0110BE7-EBF0-4612-B2F8-817194557DFD}';
  CLASS_VideoFormat: TGUID = '{E0110BE7-EBF0-4612-B2F8-817194557E03}';
  CLASS_VideoFormats: TGUID = '{E0110BE7-EBF0-4612-B2F8-817194557E05}';
  CLASS_Device: TGUID = '{E0110BE7-EBF0-4612-B2F8-817194557DFF}';
  CLASS_Devices: TGUID = '{E0110BE7-EBF0-4612-B2F8-817194557E01}';
  CLASS_VideoNorm: TGUID = '{E0110BE7-EBF0-4612-B2F8-817194557E07}';
  CLASS_VideoNorms: TGUID = '{E0110BE7-EBF0-4612-B2F8-817194557E09}';
  CLASS_InputChannel: TGUID = '{E0110BE7-EBF0-4612-B2F8-817194557E0B}';
  CLASS_InputChannels: TGUID = '{E0110BE7-EBF0-4612-B2F8-817194557E0D}';
  CLASS_DeviceFrameRates: TGUID = '{E0110BE7-EBF0-4612-B2F8-817194557E13}';
  CLASS_AviCompressor: TGUID = '{E0110BE7-EBF0-4612-B2F8-817194557E0F}';
  CLASS_AviCompressors: TGUID = '{E0110BE7-EBF0-4612-B2F8-817194557E11}';
  CLASS_CICPropertyPage: TGUID = '{E0110BE7-EBF0-4612-B2F8-817194557E16}';
  IID_IVCDPropertyElement: TGUID = '{99B44938-BFE1-4083-ADA1-BE703F4B8E03}';
  IID_IVCDPropertyInterface: TGUID = '{99B44938-BFE1-4083-ADA1-BE703F4B8E05}';
  IID_IVCDPropertyItem: TGUID = '{99B44938-BFE1-4083-ADA1-BE703F4B8E01}';
  IID_IVCDPropertyElements: TGUID = '{99B44938-BFE1-4083-ADA1-BE703F4B8E02}';
  IID_IBSTRCollection: TGUID = '{99B44939-BFE1-4083-ADA1-BE703F4B8E00}';
  IID_IVCDCategoryMap: TGUID = '{99B44939-BFE1-4083-ADA1-BE703F4B8E01}';
  IID_IVCDPropertyItems: TGUID = '{99B44938-BFE1-4083-ADA1-BE703F4B8E00}';
  IID_IVCDRangeProperty: TGUID = '{99B44940-BFE1-4083-ADA1-BE703F4B8E03}';
  IID_IVCDSwitchProperty: TGUID = '{99B44940-BFE1-4083-ADA1-BE703F4B8E04}';
  IID_IVCDButtonProperty: TGUID = '{99B44940-BFE1-4083-ADA1-BE703F4B8E05}';
  IID_IVCDMapStringsProperty: TGUID = '{99B44940-BFE1-4083-ADA1-BE703F4B8E06}';
  IID_IVCDAbsoluteValueProperty: TGUID = '{99B44940-BFE1-4083-ADA1-BE703F4B8E08}';
  CLASS_BSTRCollection: TGUID = '{E0110BE7-EBF0-4612-B2F8-817194557F01}';
  CLASS_VCDCategoryMap: TGUID = '{E0110BE7-EBF0-4612-B2F8-817194557F02}';
  CLASS_VCDPropertyItems: TGUID = '{E0110BE7-EBF0-4612-B2F8-817194557F11}';
  CLASS_VCDPropertyItem: TGUID = '{E0110BE7-EBF0-4612-B2F8-817194557F12}';
  CLASS_VCDPropertyElements: TGUID = '{E0110BE7-EBF0-4612-B2F8-817194557F13}';
  CLASS_VCDPropertyElement: TGUID = '{E0110BE7-EBF0-4612-B2F8-817194557F14}';
  CLASS_VCDPropertyInterface: TGUID = '{E0110BE7-EBF0-4612-B2F8-817194557F16}';
  CLASS_VCDRangeProperty: TGUID = '{E0110BE7-EBF0-4612-B2F8-817194557F03}';
  CLASS_VCDSwitchProperty: TGUID = '{E0110BE7-EBF0-4612-B2F8-817194557F04}';
  CLASS_VCDButtonProperty: TGUID = '{E0110BE7-EBF0-4612-B2F8-817194557F05}';
  CLASS_VCDMapStringsProperty: TGUID = '{E0110BE7-EBF0-4612-B2F8-817194557F06}';
  CLASS_VCDAbsoluteValueProperty: TGUID = '{E0110BE7-EBF0-4612-B2F8-817194557F07}';
  IID_IFrameFilterInfo: TGUID = '{99B44937-BFE0-4083-ADA1-BE703F4B8E26}';
  CLASS_FrameFilterInfo: TGUID = '{99B44937-BFE0-4083-ADA1-BE703F4B8E27}';
  IID_IFrameFilterInfos: TGUID = '{99B44937-BFE0-4083-ADA1-BE703F4B8E28}';
  CLASS_FrameFilterInfos: TGUID = '{99B44937-BFE0-4083-ADA1-BE703F4B8E29}';
  IID_IFrameFilter: TGUID = '{E0110BE7-EBF0-4612-B2F8-817194557E24}';
  CLASS_FrameFilter: TGUID = '{E0110BE7-EBF0-4612-B2F8-817194557E25}';
  IID_IFrameFilters: TGUID = '{E0110BE7-EBF0-4612-B2F8-817194557E26}';
  CLASS_FrameFilters: TGUID = '{E0110BE7-EBF0-4612-B2F8-817194557E27}';
  IID_IBaseSink: TGUID = '{E0110BE7-EBF0-4612-B2F8-817194557E17}';
  CLASS_BaseSink: TGUID = '{99B44937-BFE0-4083-ADA1-BE703F4B8E2A}';
  IID_IMediaStreamContainer: TGUID = '{E0110BE7-EBF0-4612-B2F8-817194557E1C}';
  CLASS_MediaStreamContainer: TGUID = '{E0110BE7-EBF0-4612-B2F8-817194557E1D}';
  IID_IMediaStreamContainers: TGUID = '{E0110BE7-EBF0-4612-B2F8-817194557E1E}';
  CLASS_MediaStreamContainers: TGUID = '{E0110BE7-EBF0-4612-B2F8-817194557E1F}';
  IID_IMediaStreamSink: TGUID = '{E0110BE7-EBF0-4612-B2F8-817194557E18}';
  CLASS_MediaStreamSink: TGUID = '{E0110BE7-EBF0-4612-B2F8-817194557E19}';
  CLASS_FrameType: TGUID = '{E0110BE7-EBF0-4612-B2F8-817194557E21}';
  IID_IFrameTypes: TGUID = '{E0110BE7-EBF0-4612-B2F8-817194557E22}';
  CLASS_FrameTypes: TGUID = '{E0110BE7-EBF0-4612-B2F8-817194557E23}';
  IID_IFrameHandlerSink: TGUID = '{E0110BE7-EBF0-4612-B2F8-817194557E1A}';
  CLASS_FrameHandlerSink: TGUID = '{E0110BE7-EBF0-4612-B2F8-817194557E1B}';
  IID_IICImagingControl: TGUID = '{E0110BE7-EBF0-4612-B2F8-817194557DE8}';
  DIID__IICImagingControlEvents: TGUID = '{E0110BE7-EBF0-4612-B2F8-817194557DE9}';
  CLASS_ICImagingControl: TGUID = '{E0110BE7-EBF0-4612-B2F8-817194557141}';

// *********************************************************************//
// Declaration of Enumerations defined in Type Library                    
// *********************************************************************//
// Constants for enum ICImagingControlErrors
type
  ICImagingControlErrors = TOleEnum;
const
  ICError_NOERROR = $000A0000;
  ICError_UNKNOWN = $800A0001;
  ICError_DSHOWLIB_EXCEPTION = $800A0002;
  ICError_UNEXPECTED_DSHOWLIB_BEHAVIOUR = $800A0003;
  ICError_OUT_OF_MEMORY = $800A0004;
  ICError_WRITE_ERROR = $800A0005;
  ICError_NO_VIDEO_HARDWARE_FOUND = $800A0006;
  ICError_INVALID_PARAM_VAL = $800A0007;
  ICError_AUTOMATION_ENABLED = $800A0008;
  ICError_NO_CURRENT_VALUE = $800A0009;
  ICError_INVALID_MEMBUFFER = $800A000A;
  ICError_MODE_ALREADY_ACTIVE = $800A000B;
  ICError_NO_DEVICE_OPENED = $800A000C;
  ICError_DEVICE_INVALID = $800A000D;
  ICError_DRIVER_INSTALLATION = $800A000E;
  ICError_NOT_AVALILABLE_WITH_CURRENT_DEVICE = $800A000F;
  ICError_AUTOMATION_NOT_AVAILABLE = $800A0010;
  ICError_DEVICE_NOT_FOUND = $800A0011;
  ICError_ITEM_DOES_NOT_FIT_TO_DEV = $800A0012;
  ICError_FUNC_NOT_AVAIL_IN_LIVEMODE = $800A0013;
  ICError_FUNC_ONLY_AVAIL_IN_LIVEMODE = $800A0014;
  ICError_NOT_INITIALIZED = $800A0015;
  ICError_NO_FRAMEGRABBER_SINK = $800A0016;
  ICError_SERIALNUMBER_INVALID = $800A0017;
  ICError_VIDEOFORMAT_INVALID = $800A0018;
  ICError_UNEXPECTED_SINKFORMAT_CHANGE = $800A0019;
  ICError_NO_EXTERNALTRANSPORT_AVAILABLE = $800A001A;
  ICError_TIMEOUT_PREMATURLY_ELAPSED = $800A001B;
  ICError_PASSED_DATA_DOES_NOT_FIT_TO_COMPRESSOR = $800A001C;
  ICError_OPTION_NOT_AVAILABLE = $800A001D;
  ICError_COMPONENT_NOT_FOUND = $800A001E;
  ICError_NO_CODECS_FOUND = $800A001F;
  ICError_INCOMPATIBLE_VERSION = $800A0020;
  ICError_READ_ERROR = $800A0021;
  ICError_INCOMPLETE = $800A0022;
  ICError_AVI_MODE_RUNNING = $800A0065;

// Constants for enum ICImagingControlColorformats
type
  ICImagingControlColorformats = TOleEnum;
const
  ICRGB32 = $00000001;
  ICRGB24 = $00000002;
  ICRGB565 = $00000003;
  ICRGB555 = $00000004;
  ICY8 = $00000005;
  ICUYVY = $00000006;
  ICY800 = $00000007;
  ICYGB1 = $00000008;
  ICYGB0 = $00000009;
  ICBY8 = $0000000A;

// Constants for enum ExternalTransportModes
type
  ExternalTransportModes = TOleEnum;
const
  ET_MODE_PLAY = $000010C8;
  ET_MODE_STOP = $000010C9;
  ET_MODE_FREEZE = $000010CA;
  ET_MODE_REWIND = $000010CD;
  ET_MODE_FASTFORWARD = $000010CC;
  ET_MODE_RECORD = $000010CE;
  ET_MODE_RECORD_STROBE = $000010CF;
  ET_MODE_STEP_FWD = $000010D0;
  ET_MODE_STEP_REV = $00001329;

// Constants for enum DeBayerModes
type
  DeBayerModes = TOleEnum;
const
  DEBAYER_NEARESTCOLOR = $00000000;
  DEBAYER_BILINEAR = $00000001;
  DEBAYER_EDGESENSING = $00000002;

// Constants for enum PathPositions
type
  PathPositions = TOleEnum;
const
  PATHPOSITION_NONE = $00000000;
  PATHPOSITION_DEVICE = $00000001;
  PATHPOSITION_SINK = $00000002;
  PATHPOSITION_DISPLAY = $00000004;

// Constants for enum DeBayerStartPatterns
type
  DeBayerStartPatterns = TOleEnum;
const
  DEBAYERPATTERN_BG = $00000000;
  DEBAYERPATTERN_GB = $00000001;
  DEBAYERPATTERN_GR = $00000002;
  DEBAYERPATTERN_RG = $00000003;

// Constants for enum OverlayColorModes
type
  OverlayColorModes = TOleEnum;
const
  OVERLAY_COLORMODE_BESTFIT = $00000000;
  OVERLAY_COLORMODE_GRAYSCALE = $00000001;
  OVERLAY_COLORMODE_COLOR = $00000002;

// Constants for enum BaseSinkTypes
type
  BaseSinkTypes = TOleEnum;
const
  BASESINKTYPE_FRAMEHANDLERSINK = $00000001;
  BASESINKTYPE_MEDIASTREAMSINK = $00000002;

// Constants for enum AbsDimFunction
type
  AbsDimFunction = TOleEnum;
const
  eAbsDimFunc_Linear = $00000001;
  eAbsDimFunc_Log = $00000002;
  eAbsDimFunc_Other = $00000003;

type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary                    
// *********************************************************************//
  IDevice = interface;
  IDeviceDisp = dispinterface;
  IDevices = interface;
  IDevicesDisp = dispinterface;
  IFrameType = interface;
  IFrameTypeDisp = dispinterface;
  IImageBuffer = interface;
  IImageBufferDisp = dispinterface;
  IImageBuffers = interface;
  IImageBuffersDisp = dispinterface;
  IVideoFormat = interface;
  IVideoFormatDisp = dispinterface;
  IVideoFormats = interface;
  IVideoFormatsDisp = dispinterface;
  IVideoNorm = interface;
  IVideoNormDisp = dispinterface;
  IVideoNorms = interface;
  IVideoNormsDisp = dispinterface;
  IInputChannel = interface;
  IInputChannelDisp = dispinterface;
  IInputChannels = interface;
  IInputChannelsDisp = dispinterface;
  IDeviceFrameRates = interface;
  IDeviceFrameRatesDisp = dispinterface;
  IAviCompressor = interface;
  IAviCompressorDisp = dispinterface;
  IAviCompressors = interface;
  IAviCompressorsDisp = dispinterface;
  IOverlayBitmap = interface;
  IOverlayBitmapDisp = dispinterface;
  IVCDPropertyElement = interface;
  IVCDPropertyElementDisp = dispinterface;
  IVCDPropertyInterface = interface;
  IVCDPropertyInterfaceDisp = dispinterface;
  IVCDPropertyItem = interface;
  IVCDPropertyItemDisp = dispinterface;
  IVCDPropertyElements = interface;
  IVCDPropertyElementsDisp = dispinterface;
  IBSTRCollection = interface;
  IBSTRCollectionDisp = dispinterface;
  IVCDCategoryMap = interface;
  IVCDCategoryMapDisp = dispinterface;
  IVCDPropertyItems = interface;
  IVCDPropertyItemsDisp = dispinterface;
  IVCDRangeProperty = interface;
  IVCDRangePropertyDisp = dispinterface;
  IVCDSwitchProperty = interface;
  IVCDSwitchPropertyDisp = dispinterface;
  IVCDButtonProperty = interface;
  IVCDButtonPropertyDisp = dispinterface;
  IVCDMapStringsProperty = interface;
  IVCDMapStringsPropertyDisp = dispinterface;
  IVCDAbsoluteValueProperty = interface;
  IVCDAbsoluteValuePropertyDisp = dispinterface;
  IFrameFilterInfo = interface;
  IFrameFilterInfoDisp = dispinterface;
  IFrameFilterInfos = interface;
  IFrameFilterInfosDisp = dispinterface;
  IFrameFilter = interface;
  IFrameFilterDisp = dispinterface;
  IFrameFilters = interface;
  IFrameFiltersDisp = dispinterface;
  IBaseSink = interface;
  IBaseSinkDisp = dispinterface;
  IMediaStreamContainer = interface;
  IMediaStreamContainerDisp = dispinterface;
  IMediaStreamContainers = interface;
  IMediaStreamContainersDisp = dispinterface;
  IMediaStreamSink = interface;
  IMediaStreamSinkDisp = dispinterface;
  IFrameTypes = interface;
  IFrameTypesDisp = dispinterface;
  IFrameHandlerSink = interface;
  IFrameHandlerSinkDisp = dispinterface;
  IICImagingControl = interface;
  IICImagingControlDisp = dispinterface;
  _IICImagingControlEvents = dispinterface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                       
// (NOTE: Here we map each CoClass to its Default Interface)              
// *********************************************************************//
  OverlayBitmap = IOverlayBitmap;
  ImageBuffer = IImageBuffer;
  ImageBuffers = IImageBuffers;
  VideoFormat = IVideoFormat;
  VideoFormats = IVideoFormats;
  Device = IDevice;
  Devices = IDevices;
  VideoNorm = IVideoNorm;
  VideoNorms = IVideoNorms;
  InputChannel = IInputChannel;
  InputChannels = IInputChannels;
  DeviceFrameRates = IDeviceFrameRates;
  AviCompressor = IAviCompressor;
  AviCompressors = IAviCompressors;
  CICPropertyPage = IUnknown;
  BSTRCollection = IBSTRCollection;
  VCDCategoryMap = IVCDCategoryMap;
  VCDPropertyItems = IVCDPropertyItems;
  VCDPropertyItem = IVCDPropertyItem;
  VCDPropertyElements = IVCDPropertyElements;
  VCDPropertyElement = IVCDPropertyElement;
  VCDPropertyInterface = IVCDPropertyInterface;
  VCDRangeProperty = IVCDRangeProperty;
  VCDSwitchProperty = IVCDSwitchProperty;
  VCDButtonProperty = IVCDButtonProperty;
  VCDMapStringsProperty = IVCDMapStringsProperty;
  VCDAbsoluteValueProperty = IVCDAbsoluteValueProperty;
  FrameFilterInfo = IFrameFilterInfo;
  FrameFilterInfos = IFrameFilterInfos;
  FrameFilter = IFrameFilter;
  FrameFilters = IFrameFilters;
  BaseSink = IBaseSink;
  MediaStreamContainer = IMediaStreamContainer;
  MediaStreamContainers = IMediaStreamContainers;
  MediaStreamSink = IMediaStreamSink;
  FrameType = IFrameType;
  FrameTypes = IFrameTypes;
  FrameHandlerSink = IFrameHandlerSink;
  ICImagingControl = IICImagingControl;


// *********************************************************************//
// Declaration of structures, unions and aliases.                         
// *********************************************************************//
  POleVariant1 = ^OleVariant; {*}
  PWideString1 = ^WideString; {*}


// *********************************************************************//
// Interface: IDevice
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {E0110BE7-EBF0-4612-B2F8-817194557DFE}
// *********************************************************************//
  IDevice = interface(IDispatch)
    ['{E0110BE7-EBF0-4612-B2F8-817194557DFE}']
    function Get_Name: WideString; safecall;
    function GetSerialNumber(out SerialNumber: WideString): WordBool; safecall;
    property Name: WideString read Get_Name;
  end;

// *********************************************************************//
// DispIntf:  IDeviceDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {E0110BE7-EBF0-4612-B2F8-817194557DFE}
// *********************************************************************//
  IDeviceDisp = dispinterface
    ['{E0110BE7-EBF0-4612-B2F8-817194557DFE}']
    property Name: WideString readonly dispid -524;
    function GetSerialNumber(out SerialNumber: WideString): WordBool; dispid -523;
  end;

// *********************************************************************//
// Interface: IDevices
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {E0110BE7-EBF0-4612-B2F8-817194557E00}
// *********************************************************************//
  IDevices = interface(IDispatch)
    ['{E0110BE7-EBF0-4612-B2F8-817194557E00}']
    function Get__NewEnum: IUnknown; safecall;
    function Get_Item(n: Integer): IDevice; safecall;
    function Get_Count: Integer; safecall;
    function FindIndex(const DeviceName: WideString): Integer; safecall;
    property _NewEnum: IUnknown read Get__NewEnum;
    property Item[n: Integer]: IDevice read Get_Item; default;
    property Count: Integer read Get_Count;
  end;

// *********************************************************************//
// DispIntf:  IDevicesDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {E0110BE7-EBF0-4612-B2F8-817194557E00}
// *********************************************************************//
  IDevicesDisp = dispinterface
    ['{E0110BE7-EBF0-4612-B2F8-817194557E00}']
    property _NewEnum: IUnknown readonly dispid -4;
    property Item[n: Integer]: IDevice readonly dispid 0; default;
    property Count: Integer readonly dispid 1;
    function FindIndex(const DeviceName: WideString): Integer; dispid 2;
  end;

// *********************************************************************//
// Interface: IFrameType
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {E0110BE7-EBF0-4612-B2F8-817194557E20}
// *********************************************************************//
  IFrameType = interface(IDispatch)
    ['{E0110BE7-EBF0-4612-B2F8-817194557E20}']
    function Get_Name: WideString; safecall;
    procedure Set_Name(const NameStr: WideString); safecall;
    function Get_type_: WideString; safecall;
    procedure Set_type_(const TypeStr: WideString); safecall;
    function Get_Width: Integer; safecall;
    procedure Set_Width(CurWidth: Integer); safecall;
    function Get_Height: Integer; safecall;
    procedure Set_Height(CurHeight: Integer); safecall;
    function Get_Buffersize: Integer; safecall;
    procedure Set_Buffersize(CurBuffersize: Integer); safecall;
    function Get_BitsPerPixel: Integer; safecall;
    function Get_IsBottomUp: WordBool; safecall;
    function Get_SubtypeGUID: WideString; safecall;
    procedure Set_SubtypeGUID(const guid: WideString); safecall;
    property Name: WideString read Get_Name write Set_Name;
    property type_: WideString read Get_type_ write Set_type_;
    property Width: Integer read Get_Width write Set_Width;
    property Height: Integer read Get_Height write Set_Height;
    property Buffersize: Integer read Get_Buffersize write Set_Buffersize;
    property BitsPerPixel: Integer read Get_BitsPerPixel;
    property IsBottomUp: WordBool read Get_IsBottomUp;
    property SubtypeGUID: WideString read Get_SubtypeGUID write Set_SubtypeGUID;
  end;

// *********************************************************************//
// DispIntf:  IFrameTypeDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {E0110BE7-EBF0-4612-B2F8-817194557E20}
// *********************************************************************//
  IFrameTypeDisp = dispinterface
    ['{E0110BE7-EBF0-4612-B2F8-817194557E20}']
    property Name: WideString dispid 0;
    property type_: WideString dispid 1;
    property Width: Integer dispid 2;
    property Height: Integer dispid 3;
    property Buffersize: Integer dispid 4;
    property BitsPerPixel: Integer readonly dispid 5;
    property IsBottomUp: WordBool readonly dispid 6;
    property SubtypeGUID: WideString dispid 7;
  end;

// *********************************************************************//
// Interface: IImageBuffer
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {E0110BE7-EBF0-4612-B2F8-817194557DFA}
// *********************************************************************//
  IImageBuffer = interface(IDispatch)
    ['{E0110BE7-EBF0-4612-B2F8-817194557DFA}']
    function Get_Picture: IUnknown; safecall;
    procedure SaveImage(const Filename: WideString); safecall;
    function GetImageData: OleVariant; safecall;
    procedure ReleaseImageData(var Buffer: OleVariant); safecall;
    function GetDib: Integer; safecall;
    function Get_Locked: WordBool; safecall;
    procedure Lock; safecall;
    procedure Unlock; safecall;
    procedure ForceUnlock; safecall;
    function Get_WasLockedFlag: WordBool; safecall;
    function Get_SampleStartTime: Double; safecall;
    function Get_SampleEndTime: Double; safecall;
    procedure SaveAsJpeg(const Filename: WideString; qualitiy: Integer); safecall;
    procedure SaveAsBitmap(const Filename: WideString; Colorformat: ICImagingControlColorformats); safecall;
    function Get_ImageDataPtr: Integer; safecall;
    function Get_PixelPerLine: Integer; safecall;
    function Get_Lines: Integer; safecall;
    function Get_FrameType: IFrameType; safecall;
    function Get_Index: Integer; safecall;
    property Picture: IUnknown read Get_Picture;
    property Locked: WordBool read Get_Locked;
    property WasLockedFlag: WordBool read Get_WasLockedFlag;
    property SampleStartTime: Double read Get_SampleStartTime;
    property SampleEndTime: Double read Get_SampleEndTime;
    property ImageDataPtr: Integer read Get_ImageDataPtr;
    property PixelPerLine: Integer read Get_PixelPerLine;
    property Lines: Integer read Get_Lines;
    property FrameType: IFrameType read Get_FrameType;
    property Index: Integer read Get_Index;
  end;

// *********************************************************************//
// DispIntf:  IImageBufferDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {E0110BE7-EBF0-4612-B2F8-817194557DFA}
// *********************************************************************//
  IImageBufferDisp = dispinterface
    ['{E0110BE7-EBF0-4612-B2F8-817194557DFA}']
    property Picture: IUnknown readonly dispid 1;
    procedure SaveImage(const Filename: WideString); dispid 2;
    function GetImageData: OleVariant; dispid 3;
    procedure ReleaseImageData(var Buffer: OleVariant); dispid 4;
    function GetDib: Integer; dispid 5;
    property Locked: WordBool readonly dispid 6;
    procedure Lock; dispid 7;
    procedure Unlock; dispid 8;
    procedure ForceUnlock; dispid 9;
    property WasLockedFlag: WordBool readonly dispid 10;
    property SampleStartTime: Double readonly dispid 11;
    property SampleEndTime: Double readonly dispid 12;
    procedure SaveAsJpeg(const Filename: WideString; qualitiy: Integer); dispid 13;
    procedure SaveAsBitmap(const Filename: WideString; Colorformat: ICImagingControlColorformats); dispid 14;
    property ImageDataPtr: Integer readonly dispid 15;
    property PixelPerLine: Integer readonly dispid 16;
    property Lines: Integer readonly dispid 17;
    property FrameType: IFrameType readonly dispid 18;
    property Index: Integer readonly dispid 19;
  end;

// *********************************************************************//
// Interface: IImageBuffers
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {E0110BE7-EBF0-4612-B2F8-817194557DFC}
// *********************************************************************//
  IImageBuffers = interface(IDispatch)
    ['{E0110BE7-EBF0-4612-B2F8-817194557DFC}']
    function Get__NewEnum: IUnknown; safecall;
    function Get_Item(n: Integer): IImageBuffer; safecall;
    function Get_Count: Integer; safecall;
    function Get_CurrentIndex: Integer; safecall;
    function Get_Active: IImageBuffer; safecall;
    property _NewEnum: IUnknown read Get__NewEnum;
    property Item[n: Integer]: IImageBuffer read Get_Item; default;
    property Count: Integer read Get_Count;
    property CurrentIndex: Integer read Get_CurrentIndex;
    property Active: IImageBuffer read Get_Active;
  end;

// *********************************************************************//
// DispIntf:  IImageBuffersDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {E0110BE7-EBF0-4612-B2F8-817194557DFC}
// *********************************************************************//
  IImageBuffersDisp = dispinterface
    ['{E0110BE7-EBF0-4612-B2F8-817194557DFC}']
    property _NewEnum: IUnknown readonly dispid -4;
    property Item[n: Integer]: IImageBuffer readonly dispid 0; default;
    property Count: Integer readonly dispid 1;
    property CurrentIndex: Integer readonly dispid 10;
    property Active: IImageBuffer readonly dispid 11;
  end;

// *********************************************************************//
// Interface: IVideoFormat
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {E0110BE7-EBF0-4612-B2F8-817194557E02}
// *********************************************************************//
  IVideoFormat = interface(IDispatch)
    ['{E0110BE7-EBF0-4612-B2F8-817194557E02}']
    function Get_Name: WideString; safecall;
    function Get_Width: Integer; safecall;
    function Get_Height: Integer; safecall;
    function Get_BitsPerPixel: Integer; safecall;
    function Get_FrameType: IFrameType; safecall;
    property Name: WideString read Get_Name;
    property Width: Integer read Get_Width;
    property Height: Integer read Get_Height;
    property BitsPerPixel: Integer read Get_BitsPerPixel;
    property FrameType: IFrameType read Get_FrameType;
  end;

// *********************************************************************//
// DispIntf:  IVideoFormatDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {E0110BE7-EBF0-4612-B2F8-817194557E02}
// *********************************************************************//
  IVideoFormatDisp = dispinterface
    ['{E0110BE7-EBF0-4612-B2F8-817194557E02}']
    property Name: WideString readonly dispid -524;
    property Width: Integer readonly dispid -523;
    property Height: Integer readonly dispid -522;
    property BitsPerPixel: Integer readonly dispid -521;
    property FrameType: IFrameType readonly dispid -520;
  end;

// *********************************************************************//
// Interface: IVideoFormats
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {E0110BE7-EBF0-4612-B2F8-817194557E04}
// *********************************************************************//
  IVideoFormats = interface(IDispatch)
    ['{E0110BE7-EBF0-4612-B2F8-817194557E04}']
    function Get__NewEnum: IUnknown; safecall;
    function Get_Item(n: Integer): IVideoFormat; safecall;
    function Get_Count: Integer; safecall;
    function FindIndex(const VideoFormatString: WideString): Integer; safecall;
    property _NewEnum: IUnknown read Get__NewEnum;
    property Item[n: Integer]: IVideoFormat read Get_Item; default;
    property Count: Integer read Get_Count;
  end;

// *********************************************************************//
// DispIntf:  IVideoFormatsDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {E0110BE7-EBF0-4612-B2F8-817194557E04}
// *********************************************************************//
  IVideoFormatsDisp = dispinterface
    ['{E0110BE7-EBF0-4612-B2F8-817194557E04}']
    property _NewEnum: IUnknown readonly dispid -4;
    property Item[n: Integer]: IVideoFormat readonly dispid 0; default;
    property Count: Integer readonly dispid 1;
    function FindIndex(const VideoFormatString: WideString): Integer; dispid 2;
  end;

// *********************************************************************//
// Interface: IVideoNorm
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {E0110BE7-EBF0-4612-B2F8-817194557E06}
// *********************************************************************//
  IVideoNorm = interface(IDispatch)
    ['{E0110BE7-EBF0-4612-B2F8-817194557E06}']
    function Get_Name: WideString; safecall;
    property Name: WideString read Get_Name;
  end;

// *********************************************************************//
// DispIntf:  IVideoNormDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {E0110BE7-EBF0-4612-B2F8-817194557E06}
// *********************************************************************//
  IVideoNormDisp = dispinterface
    ['{E0110BE7-EBF0-4612-B2F8-817194557E06}']
    property Name: WideString readonly dispid -524;
  end;

// *********************************************************************//
// Interface: IVideoNorms
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {E0110BE7-EBF0-4612-B2F8-817194557E08}
// *********************************************************************//
  IVideoNorms = interface(IDispatch)
    ['{E0110BE7-EBF0-4612-B2F8-817194557E08}']
    function Get__NewEnum: IUnknown; safecall;
    function Get_Item(n: Integer): IVideoNorm; safecall;
    function Get_Count: Integer; safecall;
    function FindIndex(const DeviceName: WideString): Integer; safecall;
    property _NewEnum: IUnknown read Get__NewEnum;
    property Item[n: Integer]: IVideoNorm read Get_Item; default;
    property Count: Integer read Get_Count;
  end;

// *********************************************************************//
// DispIntf:  IVideoNormsDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {E0110BE7-EBF0-4612-B2F8-817194557E08}
// *********************************************************************//
  IVideoNormsDisp = dispinterface
    ['{E0110BE7-EBF0-4612-B2F8-817194557E08}']
    property _NewEnum: IUnknown readonly dispid -4;
    property Item[n: Integer]: IVideoNorm readonly dispid 0; default;
    property Count: Integer readonly dispid 1;
    function FindIndex(const DeviceName: WideString): Integer; dispid 2;
  end;

// *********************************************************************//
// Interface: IInputChannel
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {E0110BE7-EBF0-4612-B2F8-817194557E0A}
// *********************************************************************//
  IInputChannel = interface(IDispatch)
    ['{E0110BE7-EBF0-4612-B2F8-817194557E0A}']
    function Get_Name: WideString; safecall;
    property Name: WideString read Get_Name;
  end;

// *********************************************************************//
// DispIntf:  IInputChannelDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {E0110BE7-EBF0-4612-B2F8-817194557E0A}
// *********************************************************************//
  IInputChannelDisp = dispinterface
    ['{E0110BE7-EBF0-4612-B2F8-817194557E0A}']
    property Name: WideString readonly dispid -524;
  end;

// *********************************************************************//
// Interface: IInputChannels
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {E0110BE7-EBF0-4612-B2F8-817194557E0C}
// *********************************************************************//
  IInputChannels = interface(IDispatch)
    ['{E0110BE7-EBF0-4612-B2F8-817194557E0C}']
    function Get__NewEnum: IUnknown; safecall;
    function Get_Item(n: Integer): IInputChannel; safecall;
    function Get_Count: Integer; safecall;
    function FindIndex(const DeviceName: WideString): Integer; safecall;
    property _NewEnum: IUnknown read Get__NewEnum;
    property Item[n: Integer]: IInputChannel read Get_Item; default;
    property Count: Integer read Get_Count;
  end;

// *********************************************************************//
// DispIntf:  IInputChannelsDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {E0110BE7-EBF0-4612-B2F8-817194557E0C}
// *********************************************************************//
  IInputChannelsDisp = dispinterface
    ['{E0110BE7-EBF0-4612-B2F8-817194557E0C}']
    property _NewEnum: IUnknown readonly dispid -4;
    property Item[n: Integer]: IInputChannel readonly dispid 0; default;
    property Count: Integer readonly dispid 1;
    function FindIndex(const DeviceName: WideString): Integer; dispid 2;
  end;

// *********************************************************************//
// Interface: IDeviceFrameRates
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {E0110BE7-EBF0-4612-B2F8-817194557E12}
// *********************************************************************//
  IDeviceFrameRates = interface(IDispatch)
    ['{E0110BE7-EBF0-4612-B2F8-817194557E12}']
    function Get__NewEnum: IUnknown; safecall;
    function Get_Item(n: Integer): Single; safecall;
    function Get_Count: Integer; safecall;
    function FindIndex(ValueToFind: Single): Integer; safecall;
    property _NewEnum: IUnknown read Get__NewEnum;
    property Item[n: Integer]: Single read Get_Item; default;
    property Count: Integer read Get_Count;
  end;

// *********************************************************************//
// DispIntf:  IDeviceFrameRatesDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {E0110BE7-EBF0-4612-B2F8-817194557E12}
// *********************************************************************//
  IDeviceFrameRatesDisp = dispinterface
    ['{E0110BE7-EBF0-4612-B2F8-817194557E12}']
    property _NewEnum: IUnknown readonly dispid -4;
    property Item[n: Integer]: Single readonly dispid 0; default;
    property Count: Integer readonly dispid 1;
    function FindIndex(ValueToFind: Single): Integer; dispid 2;
  end;

// *********************************************************************//
// Interface: IAviCompressor
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {E0110BE7-EBF0-4612-B2F8-817194557E0E}
// *********************************************************************//
  IAviCompressor = interface(IDispatch)
    ['{E0110BE7-EBF0-4612-B2F8-817194557E0E}']
    function Get_Name: WideString; safecall;
    function Get_PropertyPageAvailable: WordBool; safecall;
    procedure ShowPropertyPage; safecall;
    function Get_CompressorDataSize: Integer; safecall;
    function Get_CompressorData: OleVariant; safecall;
    procedure Set_CompressorData(var Data: OleVariant); safecall;
    function Get_FrameType: WideString; safecall;
    property Name: WideString read Get_Name;
    property PropertyPageAvailable: WordBool read Get_PropertyPageAvailable;
    property CompressorDataSize: Integer read Get_CompressorDataSize;
    property FrameType: WideString read Get_FrameType;
  end;

// *********************************************************************//
// DispIntf:  IAviCompressorDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {E0110BE7-EBF0-4612-B2F8-817194557E0E}
// *********************************************************************//
  IAviCompressorDisp = dispinterface
    ['{E0110BE7-EBF0-4612-B2F8-817194557E0E}']
    property Name: WideString readonly dispid -524;
    property PropertyPageAvailable: WordBool readonly dispid -523;
    procedure ShowPropertyPage; dispid -522;
    property CompressorDataSize: Integer readonly dispid -520;
    function CompressorData: OleVariant; dispid -521;
    property FrameType: WideString readonly dispid -519;
  end;

// *********************************************************************//
// Interface: IAviCompressors
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {E0110BE7-EBF0-4612-B2F8-817194557E10}
// *********************************************************************//
  IAviCompressors = interface(IDispatch)
    ['{E0110BE7-EBF0-4612-B2F8-817194557E10}']
    function Get__NewEnum: IUnknown; safecall;
    function Get_Item(n: Integer): IAviCompressor; safecall;
    function Get_Count: Integer; safecall;
    function FindIndex(const DeviceName: WideString): Integer; safecall;
    property _NewEnum: IUnknown read Get__NewEnum;
    property Item[n: Integer]: IAviCompressor read Get_Item; default;
    property Count: Integer read Get_Count;
  end;

// *********************************************************************//
// DispIntf:  IAviCompressorsDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {E0110BE7-EBF0-4612-B2F8-817194557E10}
// *********************************************************************//
  IAviCompressorsDisp = dispinterface
    ['{E0110BE7-EBF0-4612-B2F8-817194557E10}']
    property _NewEnum: IUnknown readonly dispid -4;
    property Item[n: Integer]: IAviCompressor readonly dispid 0; default;
    property Count: Integer readonly dispid 1;
    function FindIndex(const DeviceName: WideString): Integer; dispid 2;
  end;

// *********************************************************************//
// Interface: IOverlayBitmap
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {E0110BE7-EBF0-4612-B2F8-817194557E14}
// *********************************************************************//
  IOverlayBitmap = interface(IDispatch)
    ['{E0110BE7-EBF0-4612-B2F8-817194557E14}']
    function Get_Picture: IPicture; safecall;
    procedure Set_Picture(const PictureItem: IPicture); safecall;
    function Get_DropOutColor: Integer; safecall;
    procedure Set_DropOutColor(Color: Integer); safecall;
    function Get_FlipVertical: WordBool; safecall;
    procedure Set_FlipVertical(Flip: WordBool); safecall;
    function GetDC: Integer; safecall;
    procedure ReleaseDC(DC: Integer); safecall;
    function Get_Enable: WordBool; safecall;
    procedure Set_Enable(Enabled: WordBool); safecall;
    function Get_Font: IFont; safecall;
    procedure Set_Font(const CurFont: IFont); safecall;
    function Get_FontTransparent: WordBool; safecall;
    procedure Set_FontTransparent(TransparentMode: WordBool); safecall;
    function Get_FontBackColor: Integer; safecall;
    procedure Set_FontBackColor(Color: Integer); safecall;
    procedure DrawText(TextForeColor: Integer; x: Integer; y: Integer; const Text: WideString); safecall;
    procedure DrawLine(Color: Integer; XStart: Integer; YStart: Integer; XEnd: Integer; 
                       YEnd: Integer); safecall;
    procedure DrawSolidRect(Color: Integer; x0: Integer; y0: Integer; x1: Integer; y1: Integer); safecall;
    procedure Fill(Color: Integer); safecall;
    procedure DrawFrameRect(Color: Integer; x0: Integer; y0: Integer; x1: Integer; y1: Integer); safecall;
    procedure DrawSolidEllipse(Color: Integer; x0: Integer; y0: Integer; x1: Integer; y1: Integer); safecall;
    procedure DrawFrameEllipse(Color: Integer; x0: Integer; y0: Integer; x1: Integer; y1: Integer); safecall;
    function Get_PathPosition: PathPositions; safecall;
    function Get_OverlayFrameType: IFrameType; safecall;
    function Get_ColorMode: OverlayColorModes; safecall;
    procedure Set_ColorMode(CurrentColorMode: OverlayColorModes); safecall;
    property Picture: IPicture read Get_Picture write Set_Picture;
    property DropOutColor: Integer read Get_DropOutColor write Set_DropOutColor;
    property FlipVertical: WordBool read Get_FlipVertical write Set_FlipVertical;
    property Enable: WordBool read Get_Enable write Set_Enable;
    property Font: IFont read Get_Font write Set_Font;
    property FontTransparent: WordBool read Get_FontTransparent write Set_FontTransparent;
    property FontBackColor: Integer read Get_FontBackColor write Set_FontBackColor;
    property PathPosition: PathPositions read Get_PathPosition;
    property OverlayFrameType: IFrameType read Get_OverlayFrameType;
    property ColorMode: OverlayColorModes read Get_ColorMode write Set_ColorMode;
  end;

// *********************************************************************//
// DispIntf:  IOverlayBitmapDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {E0110BE7-EBF0-4612-B2F8-817194557E14}
// *********************************************************************//
  IOverlayBitmapDisp = dispinterface
    ['{E0110BE7-EBF0-4612-B2F8-817194557E14}']
    property Picture: IPicture dispid 1;
    property DropOutColor: Integer dispid 2;
    property FlipVertical: WordBool dispid 3;
    function GetDC: Integer; dispid 4;
    procedure ReleaseDC(DC: Integer); dispid 5;
    property Enable: WordBool dispid 6;
    property Font: IFont dispid 7;
    property FontTransparent: WordBool dispid 8;
    property FontBackColor: Integer dispid 9;
    procedure DrawText(TextForeColor: Integer; x: Integer; y: Integer; const Text: WideString); dispid 10;
    procedure DrawLine(Color: Integer; XStart: Integer; YStart: Integer; XEnd: Integer; 
                       YEnd: Integer); dispid 11;
    procedure DrawSolidRect(Color: Integer; x0: Integer; y0: Integer; x1: Integer; y1: Integer); dispid 12;
    procedure Fill(Color: Integer); dispid 13;
    procedure DrawFrameRect(Color: Integer; x0: Integer; y0: Integer; x1: Integer; y1: Integer); dispid 14;
    procedure DrawSolidEllipse(Color: Integer; x0: Integer; y0: Integer; x1: Integer; y1: Integer); dispid 15;
    procedure DrawFrameEllipse(Color: Integer; x0: Integer; y0: Integer; x1: Integer; y1: Integer); dispid 16;
    property PathPosition: PathPositions readonly dispid 17;
    property OverlayFrameType: IFrameType readonly dispid 18;
    property ColorMode: OverlayColorModes dispid 19;
  end;

// *********************************************************************//
// Interface: IVCDPropertyElement
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {99B44938-BFE1-4083-ADA1-BE703F4B8E03}
// *********************************************************************//
  IVCDPropertyElement = interface(IDispatch)
    ['{99B44938-BFE1-4083-ADA1-BE703F4B8E03}']
    function Get__NewEnum: IUnknown; safecall;
    function Get_Item(n: Integer): IVCDPropertyInterface; safecall;
    function Get_Count: Integer; safecall;
    function Get_Name: WideString; safecall;
    function Get_ElementID: WideString; safecall;
    function Get_ElementGUID: TGUID; safecall;
    function FindInterface(const InterfaceID: WideString): IVCDPropertyInterface; safecall;
    function Get_Parent: IVCDPropertyItem; safecall;
    procedure Update; safecall;
    property _NewEnum: IUnknown read Get__NewEnum;
    property Item[n: Integer]: IVCDPropertyInterface read Get_Item; default;
    property Count: Integer read Get_Count;
    property Name: WideString read Get_Name;
    property ElementID: WideString read Get_ElementID;
    property ElementGUID: TGUID read Get_ElementGUID;
    property Parent: IVCDPropertyItem read Get_Parent;
  end;

// *********************************************************************//
// DispIntf:  IVCDPropertyElementDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {99B44938-BFE1-4083-ADA1-BE703F4B8E03}
// *********************************************************************//
  IVCDPropertyElementDisp = dispinterface
    ['{99B44938-BFE1-4083-ADA1-BE703F4B8E03}']
    property _NewEnum: IUnknown readonly dispid -4;
    property Item[n: Integer]: IVCDPropertyInterface readonly dispid 0; default;
    property Count: Integer readonly dispid 1;
    property Name: WideString readonly dispid 2;
    property ElementID: WideString readonly dispid 3;
    property ElementGUID: {??TGUID}OleVariant readonly dispid 4;
    function FindInterface(const InterfaceID: WideString): IVCDPropertyInterface; dispid 5;
    property Parent: IVCDPropertyItem readonly dispid 6;
    procedure Update; dispid 7;
  end;

// *********************************************************************//
// Interface: IVCDPropertyInterface
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {99B44938-BFE1-4083-ADA1-BE703F4B8E05}
// *********************************************************************//
  IVCDPropertyInterface = interface(IDispatch)
    ['{99B44938-BFE1-4083-ADA1-BE703F4B8E05}']
    function Get_InterfaceID: WideString; safecall;
    function Get_InterfaceGUID: TGUID; safecall;
    function Get_Parent: IVCDPropertyElement; safecall;
    function Get_Available: WordBool; safecall;
    function Get_ReadOnly: WordBool; safecall;
    procedure Update; safecall;
    property InterfaceID: WideString read Get_InterfaceID;
    property InterfaceGUID: TGUID read Get_InterfaceGUID;
    property Parent: IVCDPropertyElement read Get_Parent;
    property Available: WordBool read Get_Available;
    property ReadOnly: WordBool read Get_ReadOnly;
  end;

// *********************************************************************//
// DispIntf:  IVCDPropertyInterfaceDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {99B44938-BFE1-4083-ADA1-BE703F4B8E05}
// *********************************************************************//
  IVCDPropertyInterfaceDisp = dispinterface
    ['{99B44938-BFE1-4083-ADA1-BE703F4B8E05}']
    property InterfaceID: WideString readonly dispid 0;
    property InterfaceGUID: {??TGUID}OleVariant readonly dispid 1;
    property Parent: IVCDPropertyElement readonly dispid 2;
    property Available: WordBool readonly dispid 3;
    property ReadOnly: WordBool readonly dispid 4;
    procedure Update; dispid 5;
  end;

// *********************************************************************//
// Interface: IVCDPropertyItem
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {99B44938-BFE1-4083-ADA1-BE703F4B8E01}
// *********************************************************************//
  IVCDPropertyItem = interface(IDispatch)
    ['{99B44938-BFE1-4083-ADA1-BE703F4B8E01}']
    function Get_Name: WideString; safecall;
    function Get_Elements: IVCDPropertyElements; safecall;
    function Get_ItemID: WideString; safecall;
    function Get_ItemGUID: TGUID; safecall;
    procedure Update; safecall;
    procedure Save(var pXmlStr: WideString); safecall;
    procedure Load(const dataStr: WideString); safecall;
    property Name: WideString read Get_Name;
    property Elements: IVCDPropertyElements read Get_Elements;
    property ItemID: WideString read Get_ItemID;
    property ItemGUID: TGUID read Get_ItemGUID;
  end;

// *********************************************************************//
// DispIntf:  IVCDPropertyItemDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {99B44938-BFE1-4083-ADA1-BE703F4B8E01}
// *********************************************************************//
  IVCDPropertyItemDisp = dispinterface
    ['{99B44938-BFE1-4083-ADA1-BE703F4B8E01}']
    property Name: WideString readonly dispid 0;
    property Elements: IVCDPropertyElements readonly dispid 1;
    property ItemID: WideString readonly dispid 2;
    property ItemGUID: {??TGUID}OleVariant readonly dispid 3;
    procedure Update; dispid 4;
    procedure Save(var pXmlStr: WideString); dispid 5;
    procedure Load(const dataStr: WideString); dispid 6;
  end;

// *********************************************************************//
// Interface: IVCDPropertyElements
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {99B44938-BFE1-4083-ADA1-BE703F4B8E02}
// *********************************************************************//
  IVCDPropertyElements = interface(IDispatch)
    ['{99B44938-BFE1-4083-ADA1-BE703F4B8E02}']
    function Get__NewEnum: IUnknown; safecall;
    function Get_Item(n: Integer): IVCDPropertyElement; safecall;
    function Get_Count: Integer; safecall;
    function FindElement(const ElementID: WideString): IVCDPropertyElement; safecall;
    function FindInterface(const InterfacePath: WideString): IVCDPropertyInterface; safecall;
    property _NewEnum: IUnknown read Get__NewEnum;
    property Item[n: Integer]: IVCDPropertyElement read Get_Item; default;
    property Count: Integer read Get_Count;
  end;

// *********************************************************************//
// DispIntf:  IVCDPropertyElementsDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {99B44938-BFE1-4083-ADA1-BE703F4B8E02}
// *********************************************************************//
  IVCDPropertyElementsDisp = dispinterface
    ['{99B44938-BFE1-4083-ADA1-BE703F4B8E02}']
    property _NewEnum: IUnknown readonly dispid -4;
    property Item[n: Integer]: IVCDPropertyElement readonly dispid 0; default;
    property Count: Integer readonly dispid 1;
    function FindElement(const ElementID: WideString): IVCDPropertyElement; dispid 2;
    function FindInterface(const InterfacePath: WideString): IVCDPropertyInterface; dispid 3;
  end;

// *********************************************************************//
// Interface: IBSTRCollection
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {99B44939-BFE1-4083-ADA1-BE703F4B8E00}
// *********************************************************************//
  IBSTRCollection = interface(IDispatch)
    ['{99B44939-BFE1-4083-ADA1-BE703F4B8E00}']
    function Get__NewEnum: IUnknown; safecall;
    function Get_Item(n: Integer): WideString; safecall;
    function Get_Count: Integer; safecall;
    function FindItem(const Item: WideString): Integer; safecall;
    property _NewEnum: IUnknown read Get__NewEnum;
    property Item[n: Integer]: WideString read Get_Item; default;
    property Count: Integer read Get_Count;
  end;

// *********************************************************************//
// DispIntf:  IBSTRCollectionDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {99B44939-BFE1-4083-ADA1-BE703F4B8E00}
// *********************************************************************//
  IBSTRCollectionDisp = dispinterface
    ['{99B44939-BFE1-4083-ADA1-BE703F4B8E00}']
    property _NewEnum: IUnknown readonly dispid -4;
    property Item[n: Integer]: WideString readonly dispid 0; default;
    property Count: Integer readonly dispid 1;
    function FindItem(const Item: WideString): Integer; dispid 2;
  end;

// *********************************************************************//
// Interface: IVCDCategoryMap
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {99B44939-BFE1-4083-ADA1-BE703F4B8E01}
// *********************************************************************//
  IVCDCategoryMap = interface(IDispatch)
    ['{99B44939-BFE1-4083-ADA1-BE703F4B8E01}']
    function Get_Categories: IBSTRCollection; safecall;
    function Get_ItemsInCategory(const Category: WideString): IBSTRCollection; safecall;
    property Categories: IBSTRCollection read Get_Categories;
    property ItemsInCategory[const Category: WideString]: IBSTRCollection read Get_ItemsInCategory;
  end;

// *********************************************************************//
// DispIntf:  IVCDCategoryMapDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {99B44939-BFE1-4083-ADA1-BE703F4B8E01}
// *********************************************************************//
  IVCDCategoryMapDisp = dispinterface
    ['{99B44939-BFE1-4083-ADA1-BE703F4B8E01}']
    property Categories: IBSTRCollection readonly dispid 0;
    property ItemsInCategory[const Category: WideString]: IBSTRCollection readonly dispid 1;
  end;

// *********************************************************************//
// Interface: IVCDPropertyItems
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {99B44938-BFE1-4083-ADA1-BE703F4B8E00}
// *********************************************************************//
  IVCDPropertyItems = interface(IDispatch)
    ['{99B44938-BFE1-4083-ADA1-BE703F4B8E00}']
    function Get__NewEnum: IUnknown; safecall;
    function Get_Item(n: Integer): IVCDPropertyItem; safecall;
    function Get_Count: Integer; safecall;
    procedure Add(const pItem: IVCDPropertyItem); safecall;
    procedure Update; safecall;
    function Save: WideString; safecall;
    procedure Load(const LoadString: WideString); safecall;
    function Get_Supported(const ItemName: WideString): WordBool; safecall;
    function FindItem(const ItemID: WideString): IVCDPropertyItem; safecall;
    function FindElement(const ElementPath: WideString): IVCDPropertyElement; safecall;
    function FindInterface(const InterfacePath: WideString): IVCDPropertyInterface; safecall;
    function Get_CategoryMap: IVCDCategoryMap; safecall;
    property _NewEnum: IUnknown read Get__NewEnum;
    property Item[n: Integer]: IVCDPropertyItem read Get_Item; default;
    property Count: Integer read Get_Count;
    property Supported[const ItemName: WideString]: WordBool read Get_Supported;
    property CategoryMap: IVCDCategoryMap read Get_CategoryMap;
  end;

// *********************************************************************//
// DispIntf:  IVCDPropertyItemsDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {99B44938-BFE1-4083-ADA1-BE703F4B8E00}
// *********************************************************************//
  IVCDPropertyItemsDisp = dispinterface
    ['{99B44938-BFE1-4083-ADA1-BE703F4B8E00}']
    property _NewEnum: IUnknown readonly dispid -4;
    property Item[n: Integer]: IVCDPropertyItem readonly dispid 0; default;
    property Count: Integer readonly dispid 1;
    procedure Add(const pItem: IVCDPropertyItem); dispid 2;
    procedure Update; dispid 3;
    function Save: WideString; dispid 4;
    procedure Load(const LoadString: WideString); dispid 5;
    property Supported[const ItemName: WideString]: WordBool readonly dispid 6;
    function FindItem(const ItemID: WideString): IVCDPropertyItem; dispid 7;
    function FindElement(const ElementPath: WideString): IVCDPropertyElement; dispid 8;
    function FindInterface(const InterfacePath: WideString): IVCDPropertyInterface; dispid 9;
    property CategoryMap: IVCDCategoryMap readonly dispid 10;
  end;

// *********************************************************************//
// Interface: IVCDRangeProperty
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {99B44940-BFE1-4083-ADA1-BE703F4B8E03}
// *********************************************************************//
  IVCDRangeProperty = interface(IVCDPropertyInterface)
    ['{99B44940-BFE1-4083-ADA1-BE703F4B8E03}']
    function Get_Value: Integer; safecall;
    procedure Set_Value(pVal: Integer); safecall;
    function Get_RangeMin: Integer; safecall;
    function Get_RangeMax: Integer; safecall;
    function Get_Default: Integer; safecall;
    function Get_Delta: Integer; safecall;
    property Value: Integer read Get_Value write Set_Value;
    property RangeMin: Integer read Get_RangeMin;
    property RangeMax: Integer read Get_RangeMax;
    property Default: Integer read Get_Default;
    property Delta: Integer read Get_Delta;
  end;

// *********************************************************************//
// DispIntf:  IVCDRangePropertyDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {99B44940-BFE1-4083-ADA1-BE703F4B8E03}
// *********************************************************************//
  IVCDRangePropertyDisp = dispinterface
    ['{99B44940-BFE1-4083-ADA1-BE703F4B8E03}']
    property Value: Integer dispid 11;
    property RangeMin: Integer readonly dispid 12;
    property RangeMax: Integer readonly dispid 13;
    property Default: Integer readonly dispid 14;
    property Delta: Integer readonly dispid 15;
    property InterfaceID: WideString readonly dispid 0;
    property InterfaceGUID: {??TGUID}OleVariant readonly dispid 1;
    property Parent: IVCDPropertyElement readonly dispid 2;
    property Available: WordBool readonly dispid 3;
    property ReadOnly: WordBool readonly dispid 4;
    procedure Update; dispid 5;
  end;

// *********************************************************************//
// Interface: IVCDSwitchProperty
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {99B44940-BFE1-4083-ADA1-BE703F4B8E04}
// *********************************************************************//
  IVCDSwitchProperty = interface(IVCDPropertyInterface)
    ['{99B44940-BFE1-4083-ADA1-BE703F4B8E04}']
    function Get_Switch: WordBool; safecall;
    procedure Set_Switch(pVal: WordBool); safecall;
    property Switch: WordBool read Get_Switch write Set_Switch;
  end;

// *********************************************************************//
// DispIntf:  IVCDSwitchPropertyDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {99B44940-BFE1-4083-ADA1-BE703F4B8E04}
// *********************************************************************//
  IVCDSwitchPropertyDisp = dispinterface
    ['{99B44940-BFE1-4083-ADA1-BE703F4B8E04}']
    property Switch: WordBool dispid 11;
    property InterfaceID: WideString readonly dispid 0;
    property InterfaceGUID: {??TGUID}OleVariant readonly dispid 1;
    property Parent: IVCDPropertyElement readonly dispid 2;
    property Available: WordBool readonly dispid 3;
    property ReadOnly: WordBool readonly dispid 4;
    procedure Update; dispid 5;
  end;

// *********************************************************************//
// Interface: IVCDButtonProperty
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {99B44940-BFE1-4083-ADA1-BE703F4B8E05}
// *********************************************************************//
  IVCDButtonProperty = interface(IVCDPropertyInterface)
    ['{99B44940-BFE1-4083-ADA1-BE703F4B8E05}']
    procedure Push; safecall;
  end;

// *********************************************************************//
// DispIntf:  IVCDButtonPropertyDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {99B44940-BFE1-4083-ADA1-BE703F4B8E05}
// *********************************************************************//
  IVCDButtonPropertyDisp = dispinterface
    ['{99B44940-BFE1-4083-ADA1-BE703F4B8E05}']
    procedure Push; dispid 11;
    property InterfaceID: WideString readonly dispid 0;
    property InterfaceGUID: {??TGUID}OleVariant readonly dispid 1;
    property Parent: IVCDPropertyElement readonly dispid 2;
    property Available: WordBool readonly dispid 3;
    property ReadOnly: WordBool readonly dispid 4;
    procedure Update; dispid 5;
  end;

// *********************************************************************//
// Interface: IVCDMapStringsProperty
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {99B44940-BFE1-4083-ADA1-BE703F4B8E06}
// *********************************************************************//
  IVCDMapStringsProperty = interface(IVCDRangeProperty)
    ['{99B44940-BFE1-4083-ADA1-BE703F4B8E06}']
    function Get_Strings: IBSTRCollection; safecall;
    function Get_String_: WideString; safecall;
    procedure Set_String_(const String_: WideString); safecall;
    property Strings: IBSTRCollection read Get_Strings;
    property String_: WideString read Get_String_ write Set_String_;
  end;

// *********************************************************************//
// DispIntf:  IVCDMapStringsPropertyDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {99B44940-BFE1-4083-ADA1-BE703F4B8E06}
// *********************************************************************//
  IVCDMapStringsPropertyDisp = dispinterface
    ['{99B44940-BFE1-4083-ADA1-BE703F4B8E06}']
    property Strings: IBSTRCollection readonly dispid 16;
    property String_: WideString dispid 17;
    property Value: Integer dispid 11;
    property RangeMin: Integer readonly dispid 12;
    property RangeMax: Integer readonly dispid 13;
    property Default: Integer readonly dispid 14;
    property Delta: Integer readonly dispid 15;
    property InterfaceID: WideString readonly dispid 0;
    property InterfaceGUID: {??TGUID}OleVariant readonly dispid 1;
    property Parent: IVCDPropertyElement readonly dispid 2;
    property Available: WordBool readonly dispid 3;
    property ReadOnly: WordBool readonly dispid 4;
    procedure Update; dispid 5;
  end;

// *********************************************************************//
// Interface: IVCDAbsoluteValueProperty
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {99B44940-BFE1-4083-ADA1-BE703F4B8E08}
// *********************************************************************//
  IVCDAbsoluteValueProperty = interface(IVCDPropertyInterface)
    ['{99B44940-BFE1-4083-ADA1-BE703F4B8E08}']
    function Get_Value: Double; safecall;
    procedure Set_Value(CurrentValue: Double); safecall;
    function Get_RangeMin: Double; safecall;
    function Get_RangeMax: Double; safecall;
    function Get_Default: Double; safecall;
    function Get_DimType: WideString; safecall;
    function Get_DimFunction: AbsDimFunction; safecall;
    property Value: Double read Get_Value write Set_Value;
    property RangeMin: Double read Get_RangeMin;
    property RangeMax: Double read Get_RangeMax;
    property Default: Double read Get_Default;
    property DimType: WideString read Get_DimType;
    property DimFunction: AbsDimFunction read Get_DimFunction;
  end;

// *********************************************************************//
// DispIntf:  IVCDAbsoluteValuePropertyDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {99B44940-BFE1-4083-ADA1-BE703F4B8E08}
// *********************************************************************//
  IVCDAbsoluteValuePropertyDisp = dispinterface
    ['{99B44940-BFE1-4083-ADA1-BE703F4B8E08}']
    property Value: Double dispid 11;
    property RangeMin: Double readonly dispid 12;
    property RangeMax: Double readonly dispid 13;
    property Default: Double readonly dispid 14;
    property DimType: WideString readonly dispid 15;
    property DimFunction: AbsDimFunction readonly dispid 16;
    property InterfaceID: WideString readonly dispid 0;
    property InterfaceGUID: {??TGUID}OleVariant readonly dispid 1;
    property Parent: IVCDPropertyElement readonly dispid 2;
    property Available: WordBool readonly dispid 3;
    property ReadOnly: WordBool readonly dispid 4;
    procedure Update; dispid 5;
  end;

// *********************************************************************//
// Interface: IFrameFilterInfo
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {99B44937-BFE0-4083-ADA1-BE703F4B8E26}
// *********************************************************************//
  IFrameFilterInfo = interface(IDispatch)
    ['{99B44937-BFE0-4083-ADA1-BE703F4B8E26}']
    function Get_Name: WideString; safecall;
    function Get_ModuleName: WideString; safecall;
    function Get_ModulePath: WideString; safecall;
    property Name: WideString read Get_Name;
    property ModuleName: WideString read Get_ModuleName;
    property ModulePath: WideString read Get_ModulePath;
  end;

// *********************************************************************//
// DispIntf:  IFrameFilterInfoDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {99B44937-BFE0-4083-ADA1-BE703F4B8E26}
// *********************************************************************//
  IFrameFilterInfoDisp = dispinterface
    ['{99B44937-BFE0-4083-ADA1-BE703F4B8E26}']
    property Name: WideString readonly dispid 0;
    property ModuleName: WideString readonly dispid 1;
    property ModulePath: WideString readonly dispid 2;
  end;

// *********************************************************************//
// Interface: IFrameFilterInfos
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {99B44937-BFE0-4083-ADA1-BE703F4B8E28}
// *********************************************************************//
  IFrameFilterInfos = interface(IDispatch)
    ['{99B44937-BFE0-4083-ADA1-BE703F4B8E28}']
    function Get__NewEnum: IUnknown; safecall;
    function Get_Item(n: Integer): IFrameFilterInfo; safecall;
    function Get_Count: Integer; safecall;
    property _NewEnum: IUnknown read Get__NewEnum;
    property Item[n: Integer]: IFrameFilterInfo read Get_Item; default;
    property Count: Integer read Get_Count;
  end;

// *********************************************************************//
// DispIntf:  IFrameFilterInfosDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {99B44937-BFE0-4083-ADA1-BE703F4B8E28}
// *********************************************************************//
  IFrameFilterInfosDisp = dispinterface
    ['{99B44937-BFE0-4083-ADA1-BE703F4B8E28}']
    property _NewEnum: IUnknown readonly dispid -4;
    property Item[n: Integer]: IFrameFilterInfo readonly dispid 0; default;
    property Count: Integer readonly dispid 1;
  end;

// *********************************************************************//
// Interface: IFrameFilter
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {E0110BE7-EBF0-4612-B2F8-817194557E24}
// *********************************************************************//
  IFrameFilter = interface(IDispatch)
    ['{E0110BE7-EBF0-4612-B2F8-817194557E24}']
    function Get_Name: WideString; safecall;
    function Get_AvailableParameters: IBSTRCollection; safecall;
    function Get_Parameter(const ParamName: WideString): OleVariant; safecall;
    procedure Set_Parameter(const ParamName: WideString; var CurrentValue: OleVariant); safecall;
    function Get_DataSize(const ParamName: WideString): Integer; safecall;
    function Get_Data(const ParamName: WideString): OleVariant; safecall;
    procedure Set_Data(const ParamName: WideString; var CurrentValue: OleVariant); safecall;
    function GetBaseFilter: Integer; safecall;
    procedure SetBaseFilter(FilterPtr: Integer); safecall;
    function Get_FilterInfo: IFrameFilterInfo; safecall;
    function Get_HasDialog: WordBool; safecall;
    procedure ShowDialog; safecall;
    function Get_FilterData: WideString; safecall;
    procedure Set_FilterData(const Data: WideString); safecall;
    procedure SaveFilterData(const Filename: WideString); safecall;
    procedure LoadFilterData(const Filename: WideString); safecall;
    procedure BeginParameterTransfer; safecall;
    procedure EndParameterTransfer; safecall;
    property Name: WideString read Get_Name;
    property AvailableParameters: IBSTRCollection read Get_AvailableParameters;
    property DataSize[const ParamName: WideString]: Integer read Get_DataSize;
    property FilterInfo: IFrameFilterInfo read Get_FilterInfo;
    property HasDialog: WordBool read Get_HasDialog;
    property FilterData: WideString read Get_FilterData write Set_FilterData;
  end;

// *********************************************************************//
// DispIntf:  IFrameFilterDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {E0110BE7-EBF0-4612-B2F8-817194557E24}
// *********************************************************************//
  IFrameFilterDisp = dispinterface
    ['{E0110BE7-EBF0-4612-B2F8-817194557E24}']
    property Name: WideString readonly dispid 0;
    property AvailableParameters: IBSTRCollection readonly dispid 1;
    function Parameter(const ParamName: WideString): OleVariant; dispid 2;
    property DataSize[const ParamName: WideString]: Integer readonly dispid 3;
    function Data(const ParamName: WideString): OleVariant; dispid 4;
    function GetBaseFilter: Integer; dispid 5;
    procedure SetBaseFilter(FilterPtr: Integer); dispid 6;
    property FilterInfo: IFrameFilterInfo readonly dispid 7;
    property HasDialog: WordBool readonly dispid 8;
    procedure ShowDialog; dispid 9;
    property FilterData: WideString dispid 10;
    procedure SaveFilterData(const Filename: WideString); dispid 11;
    procedure LoadFilterData(const Filename: WideString); dispid 12;
    procedure BeginParameterTransfer; dispid 13;
    procedure EndParameterTransfer; dispid 14;
  end;

// *********************************************************************//
// Interface: IFrameFilters
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {E0110BE7-EBF0-4612-B2F8-817194557E26}
// *********************************************************************//
  IFrameFilters = interface(IDispatch)
    ['{E0110BE7-EBF0-4612-B2F8-817194557E26}']
    function Get__NewEnum: IUnknown; safecall;
    function Get_Item(n: Integer): IFrameFilter; safecall;
    procedure Set_Item(n: Integer; const ppItem: IFrameFilter); safecall;
    function Get_Count: Integer; safecall;
    procedure Add(const NewType: IFrameFilter); safecall;
    procedure Remove(Index: Integer); safecall;
    procedure Clear; safecall;
    property _NewEnum: IUnknown read Get__NewEnum;
    property Item[n: Integer]: IFrameFilter read Get_Item write Set_Item; default;
    property Count: Integer read Get_Count;
  end;

// *********************************************************************//
// DispIntf:  IFrameFiltersDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {E0110BE7-EBF0-4612-B2F8-817194557E26}
// *********************************************************************//
  IFrameFiltersDisp = dispinterface
    ['{E0110BE7-EBF0-4612-B2F8-817194557E26}']
    property _NewEnum: IUnknown readonly dispid -4;
    property Item[n: Integer]: IFrameFilter dispid 0; default;
    property Count: Integer readonly dispid 1;
    procedure Add(const NewType: IFrameFilter); dispid 3;
    procedure Remove(Index: Integer); dispid 4;
    procedure Clear; dispid 5;
  end;

// *********************************************************************//
// Interface: IBaseSink
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {E0110BE7-EBF0-4612-B2F8-817194557E17}
// *********************************************************************//
  IBaseSink = interface(IDispatch)
    ['{E0110BE7-EBF0-4612-B2F8-817194557E17}']
    function Get_SinkType: BaseSinkTypes; safecall;
    function Get_SinkModeRunning: WordBool; safecall;
    procedure Set_SinkModeRunning(IsRunning: WordBool); safecall;
    procedure Attach(var pAttachTarget: Pointer); safecall;
    property SinkType: BaseSinkTypes read Get_SinkType;
    property SinkModeRunning: WordBool read Get_SinkModeRunning write Set_SinkModeRunning;
  end;

// *********************************************************************//
// DispIntf:  IBaseSinkDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {E0110BE7-EBF0-4612-B2F8-817194557E17}
// *********************************************************************//
  IBaseSinkDisp = dispinterface
    ['{E0110BE7-EBF0-4612-B2F8-817194557E17}']
    property SinkType: BaseSinkTypes readonly dispid 0;
    property SinkModeRunning: WordBool dispid 1;
    procedure Attach(var pAttachTarget: {??Pointer}OleVariant); dispid 2;
  end;

// *********************************************************************//
// Interface: IMediaStreamContainer
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {E0110BE7-EBF0-4612-B2F8-817194557E1C}
// *********************************************************************//
  IMediaStreamContainer = interface(IDispatch)
    ['{E0110BE7-EBF0-4612-B2F8-817194557E1C}']
    function Get_Name: WideString; safecall;
    function Get_ID: WideString; safecall;
    function Get_CustomCodecSupported: WordBool; safecall;
    function Get_PreferredFileExtension: WideString; safecall;
    property Name: WideString read Get_Name;
    property ID: WideString read Get_ID;
    property CustomCodecSupported: WordBool read Get_CustomCodecSupported;
    property PreferredFileExtension: WideString read Get_PreferredFileExtension;
  end;

// *********************************************************************//
// DispIntf:  IMediaStreamContainerDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {E0110BE7-EBF0-4612-B2F8-817194557E1C}
// *********************************************************************//
  IMediaStreamContainerDisp = dispinterface
    ['{E0110BE7-EBF0-4612-B2F8-817194557E1C}']
    property Name: WideString readonly dispid 0;
    property ID: WideString readonly dispid 1;
    property CustomCodecSupported: WordBool readonly dispid 2;
    property PreferredFileExtension: WideString readonly dispid 3;
  end;

// *********************************************************************//
// Interface: IMediaStreamContainers
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {E0110BE7-EBF0-4612-B2F8-817194557E1E}
// *********************************************************************//
  IMediaStreamContainers = interface(IDispatch)
    ['{E0110BE7-EBF0-4612-B2F8-817194557E1E}']
    function Get__NewEnum: IUnknown; safecall;
    function Get_Item(n: Integer): IMediaStreamContainer; safecall;
    function Get_Count: Integer; safecall;
    function FindIndex(const ContainerName: WideString): Integer; safecall;
    property _NewEnum: IUnknown read Get__NewEnum;
    property Item[n: Integer]: IMediaStreamContainer read Get_Item; default;
    property Count: Integer read Get_Count;
  end;

// *********************************************************************//
// DispIntf:  IMediaStreamContainersDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {E0110BE7-EBF0-4612-B2F8-817194557E1E}
// *********************************************************************//
  IMediaStreamContainersDisp = dispinterface
    ['{E0110BE7-EBF0-4612-B2F8-817194557E1E}']
    property _NewEnum: IUnknown readonly dispid -4;
    property Item[n: Integer]: IMediaStreamContainer readonly dispid 0; default;
    property Count: Integer readonly dispid 1;
    function FindIndex(const ContainerName: WideString): Integer; dispid 2;
  end;

// *********************************************************************//
// Interface: IMediaStreamSink
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {E0110BE7-EBF0-4612-B2F8-817194557E18}
// *********************************************************************//
  IMediaStreamSink = interface(IBaseSink)
    ['{E0110BE7-EBF0-4612-B2F8-817194557E18}']
    function Get_Filename: WideString; safecall;
    procedure Set_Filename(const SinkFilename: WideString); safecall;
    function Get_StreamContainer: IMediaStreamContainer; safecall;
    procedure Set_StreamContainer(const CurrentContainer: IMediaStreamContainer); safecall;
    function Get_Codec: IAviCompressor; safecall;
    procedure Set_Codec(const Type_: IAviCompressor); safecall;
    function Get_FrameFilters: IFrameFilters; safecall;
    function Get_FrameTimeCorrection: WordBool; safecall;
    procedure Set_FrameTimeCorrection(Enabled: WordBool); safecall;
    property Filename: WideString read Get_Filename write Set_Filename;
    property StreamContainer: IMediaStreamContainer read Get_StreamContainer write Set_StreamContainer;
    property Codec: IAviCompressor read Get_Codec write Set_Codec;
    property FrameFilters: IFrameFilters read Get_FrameFilters;
    property FrameTimeCorrection: WordBool read Get_FrameTimeCorrection write Set_FrameTimeCorrection;
  end;

// *********************************************************************//
// DispIntf:  IMediaStreamSinkDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {E0110BE7-EBF0-4612-B2F8-817194557E18}
// *********************************************************************//
  IMediaStreamSinkDisp = dispinterface
    ['{E0110BE7-EBF0-4612-B2F8-817194557E18}']
    property Filename: WideString dispid 3;
    property StreamContainer: IMediaStreamContainer dispid 4;
    property Codec: IAviCompressor dispid 5;
    property FrameFilters: IFrameFilters readonly dispid 6;
    property FrameTimeCorrection: WordBool dispid 7;
    property SinkType: BaseSinkTypes readonly dispid 0;
    property SinkModeRunning: WordBool dispid 1;
    procedure Attach(var pAttachTarget: {??Pointer}OleVariant); dispid 2;
  end;

// *********************************************************************//
// Interface: IFrameTypes
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {E0110BE7-EBF0-4612-B2F8-817194557E22}
// *********************************************************************//
  IFrameTypes = interface(IDispatch)
    ['{E0110BE7-EBF0-4612-B2F8-817194557E22}']
    function Get__NewEnum: IUnknown; safecall;
    function Get_Item(n: Integer): IFrameType; safecall;
    function Get_Count: Integer; safecall;
    procedure Add(const NewType: IFrameType); safecall;
    procedure Remove(Index: Integer); safecall;
    procedure Clear; safecall;
    procedure Set_ReadOnly(Param1: WordBool); safecall;
    property _NewEnum: IUnknown read Get__NewEnum;
    property Item[n: Integer]: IFrameType read Get_Item; default;
    property Count: Integer read Get_Count;
    property ReadOnly: WordBool write Set_ReadOnly;
  end;

// *********************************************************************//
// DispIntf:  IFrameTypesDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {E0110BE7-EBF0-4612-B2F8-817194557E22}
// *********************************************************************//
  IFrameTypesDisp = dispinterface
    ['{E0110BE7-EBF0-4612-B2F8-817194557E22}']
    property _NewEnum: IUnknown readonly dispid -4;
    property Item[n: Integer]: IFrameType readonly dispid 0; default;
    property Count: Integer readonly dispid 1;
    procedure Add(const NewType: IFrameType); dispid 3;
    procedure Remove(Index: Integer); dispid 4;
    procedure Clear; dispid 5;
    property ReadOnly: WordBool writeonly dispid 10;
  end;

// *********************************************************************//
// Interface: IFrameHandlerSink
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {E0110BE7-EBF0-4612-B2F8-817194557E1A}
// *********************************************************************//
  IFrameHandlerSink = interface(IBaseSink)
    ['{E0110BE7-EBF0-4612-B2F8-817194557E1A}']
    function Get_SnapMode: WordBool; safecall;
    procedure Set_SnapMode(SnapModeEnabled: WordBool); safecall;
    procedure SnapImage(timeout: Integer); safecall;
    procedure SnapImageSequence(Count: Integer; timeout: Integer); safecall;
    procedure SnapImageSequenceAsync(Count: Integer); safecall;
    function Get_ImageBuffers: IImageBuffers; safecall;
    function Get_LastAcquiredBuffer: IImageBuffer; safecall;
    function Get_BufferCount: Integer; safecall;
    procedure Set_BufferCount(CurrentBufferCount: Integer); safecall;
    function Get_FrameTypes: IFrameTypes; safecall;
    function Get_FrameFilters: IFrameFilters; safecall;
    function Get_FrameCount: Integer; safecall;
    property SnapMode: WordBool read Get_SnapMode write Set_SnapMode;
    property ImageBuffers: IImageBuffers read Get_ImageBuffers;
    property LastAcquiredBuffer: IImageBuffer read Get_LastAcquiredBuffer;
    property BufferCount: Integer read Get_BufferCount write Set_BufferCount;
    property FrameTypes: IFrameTypes read Get_FrameTypes;
    property FrameFilters: IFrameFilters read Get_FrameFilters;
    property FrameCount: Integer read Get_FrameCount;
  end;

// *********************************************************************//
// DispIntf:  IFrameHandlerSinkDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {E0110BE7-EBF0-4612-B2F8-817194557E1A}
// *********************************************************************//
  IFrameHandlerSinkDisp = dispinterface
    ['{E0110BE7-EBF0-4612-B2F8-817194557E1A}']
    property SnapMode: WordBool dispid 3;
    procedure SnapImage(timeout: Integer); dispid 4;
    procedure SnapImageSequence(Count: Integer; timeout: Integer); dispid 5;
    procedure SnapImageSequenceAsync(Count: Integer); dispid 6;
    property ImageBuffers: IImageBuffers readonly dispid 7;
    property LastAcquiredBuffer: IImageBuffer readonly dispid 8;
    property BufferCount: Integer dispid 18;
    property FrameTypes: IFrameTypes readonly dispid 19;
    property FrameFilters: IFrameFilters readonly dispid 20;
    property FrameCount: Integer readonly dispid 21;
    property SinkType: BaseSinkTypes readonly dispid 0;
    property SinkModeRunning: WordBool dispid 1;
    procedure Attach(var pAttachTarget: {??Pointer}OleVariant); dispid 2;
  end;

// *********************************************************************//
// Interface: IICImagingControl
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {E0110BE7-EBF0-4612-B2F8-817194557DE8}
// *********************************************************************//
  IICImagingControl = interface(IDispatch)
    ['{E0110BE7-EBF0-4612-B2F8-817194557DE8}']
    procedure Refresh; safecall;
    function Get_Device: WideString; safecall;
    procedure Set_Device(const CurrentDevice: WideString); safecall;
    function Get_LiveVideoRunning: WordBool; safecall;
    procedure LiveStart; safecall;
    procedure LiveStop; safecall;
    function Get_VideoFormat: WideString; safecall;
    procedure Set_VideoFormat(const CurrentValue: WideString); safecall;
    function Get_ImageWidth: Integer; safecall;
    function Get_ImageHeight: Integer; safecall;
    function Get_VideoNormAvailable: WordBool; safecall;
    function Get_VideoNorm: WideString; safecall;
    procedure Set_VideoNorm(const CurrentValue: WideString); safecall;
    function Get_InputChannelAvailable: WordBool; safecall;
    function Get_InputChannel: WideString; safecall;
    procedure Set_InputChannel(const CurrentValue: WideString); safecall;
    function Get_MemoryCurrentGrabberColorformat: ICImagingControlColorformats; safecall;
    procedure Set_MemoryCurrentGrabberColorformat(CurrenValue: ICImagingControlColorformats); safecall;
    procedure MemorySaveImage(const Filename: WideString); safecall;
    procedure MemorySaveImageSequence(SequenceLength: Integer; const Filename: WideString); safecall;
    procedure MemorySnapImage; safecall;
    procedure MemorySnapImageSequence(ImageCount: Integer); safecall;
    function MemoryGetImageData: OleVariant; safecall;
    procedure MemoryReleaseImageData(var Buffer: OleVariant); safecall;
    function MemoryGetDib: Integer; safecall;
    procedure AviStartCapture(const Filename: WideString; const Compressor: WideString); safecall;
    procedure AviStopCapture; safecall;
    function Get_Brightness: Integer; safecall;
    procedure Set_Brightness(CurrentBrightnessValue: Integer); safecall;
    function Get_BrightnessAuto: WordBool; safecall;
    procedure Set_BrightnessAuto(CurrentBrightnessAutoValue: WordBool); safecall;
    function Get_BrightnessAvailable: WordBool; safecall;
    function Get_BrightnessAutoAvailable: WordBool; safecall;
    function Get_BrightnessRange: OleVariant; safecall;
    function Get_Contrast: Integer; safecall;
    procedure Set_Contrast(CurrentContrastValue: Integer); safecall;
    function Get_ContrastAuto: WordBool; safecall;
    procedure Set_ContrastAuto(CurrentContrastAutoValue: WordBool); safecall;
    function Get_ContrastAvailable: WordBool; safecall;
    function Get_ContrastAutoAvailable: WordBool; safecall;
    function Get_ContrastRange: OleVariant; safecall;
    function Get_Hue: Integer; safecall;
    procedure Set_Hue(CurrentHueValue: Integer); safecall;
    function Get_HueAuto: WordBool; safecall;
    procedure Set_HueAuto(CurrentHueAutoValue: WordBool); safecall;
    function Get_HueAvailable: WordBool; safecall;
    function Get_HueAutoAvailable: WordBool; safecall;
    function Get_HueRange: OleVariant; safecall;
    function Get_Saturation: Integer; safecall;
    procedure Set_Saturation(CurrentSaturationValue: Integer); safecall;
    function Get_SaturationAuto: WordBool; safecall;
    procedure Set_SaturationAuto(CurrentSaturationAutoValue: WordBool); safecall;
    function Get_SaturationAvailable: WordBool; safecall;
    function Get_SaturationAutoAvailable: WordBool; safecall;
    function Get_SaturationRange: OleVariant; safecall;
    function Get_Sharpness: Integer; safecall;
    procedure Set_Sharpness(CurrentSharpnessValue: Integer); safecall;
    function Get_SharpnessAuto: WordBool; safecall;
    procedure Set_SharpnessAuto(CurrentSharpnessAutoValue: WordBool); safecall;
    function Get_SharpnessAvailable: WordBool; safecall;
    function Get_SharpnessAutoAvailable: WordBool; safecall;
    function Get_SharpnessRange: OleVariant; safecall;
    function Get_Gamma: Integer; safecall;
    procedure Set_Gamma(CurrentGammaValue: Integer); safecall;
    function Get_GammaAuto: WordBool; safecall;
    procedure Set_GammaAuto(CurrentGammaAutoValue: WordBool); safecall;
    function Get_GammaAvailable: WordBool; safecall;
    function Get_GammaAutoAvailable: WordBool; safecall;
    function Get_GammaRange: OleVariant; safecall;
    function Get_ColorEnable: Integer; safecall;
    procedure Set_ColorEnable(CurrentColorEnableValue: Integer); safecall;
    function Get_ColorEnableAuto: WordBool; safecall;
    procedure Set_ColorEnableAuto(CurrentColorEnableAutoValue: WordBool); safecall;
    function Get_ColorEnableAvailable: WordBool; safecall;
    function Get_ColorEnableAutoAvailable: WordBool; safecall;
    function Get_ColorEnableRange: OleVariant; safecall;
    function Get_WhiteBalance: Integer; safecall;
    procedure Set_WhiteBalance(CurrentWhiteBalanceValue: Integer); safecall;
    function Get_WhiteBalanceAuto: WordBool; safecall;
    procedure Set_WhiteBalanceAuto(CurrentWhiteBalanceAutoValue: WordBool); safecall;
    function Get_WhiteBalanceAvailable: WordBool; safecall;
    function Get_WhiteBalanceAutoAvailable: WordBool; safecall;
    function Get_WhiteBalanceRange: OleVariant; safecall;
    function Get_BacklightCompensation: Integer; safecall;
    procedure Set_BacklightCompensation(CurrentBacklightCompensationValue: Integer); safecall;
    function Get_BacklightCompensationAuto: WordBool; safecall;
    procedure Set_BacklightCompensationAuto(CurrentBacklightCompensationAutoValue: WordBool); safecall;
    function Get_BacklightCompensationAvailable: WordBool; safecall;
    function Get_BacklightCompensationAutoAvailable: WordBool; safecall;
    function Get_BacklightCompensationRange: OleVariant; safecall;
    function Get_Gain: Integer; safecall;
    procedure Set_Gain(CurrentGainValue: Integer); safecall;
    function Get_GainAuto: WordBool; safecall;
    procedure Set_GainAuto(CurrentGainAutoValue: WordBool); safecall;
    function Get_GainAvailable: WordBool; safecall;
    function Get_GainAutoAvailable: WordBool; safecall;
    function Get_GainRange: OleVariant; safecall;
    function Get_Pan: Integer; safecall;
    procedure Set_Pan(CurrentPanValue: Integer); safecall;
    function Get_PanAuto: WordBool; safecall;
    procedure Set_PanAuto(CurrentPanAutoValue: WordBool); safecall;
    function Get_PanAvailable: WordBool; safecall;
    function Get_PanAutoAvailable: WordBool; safecall;
    function Get_PanRange: OleVariant; safecall;
    function Get_Tilt: Integer; safecall;
    procedure Set_Tilt(CurrentTiltValue: Integer); safecall;
    function Get_TiltAuto: WordBool; safecall;
    procedure Set_TiltAuto(CurrentTiltAutoValue: WordBool); safecall;
    function Get_TiltAvailable: WordBool; safecall;
    function Get_TiltAutoAvailable: WordBool; safecall;
    function Get_TiltRange: OleVariant; safecall;
    function Get_Roll: Integer; safecall;
    procedure Set_Roll(CurrentRollValue: Integer); safecall;
    function Get_RollAuto: WordBool; safecall;
    procedure Set_RollAuto(CurrentRollAutoValue: WordBool); safecall;
    function Get_RollAvailable: WordBool; safecall;
    function Get_RollAutoAvailable: WordBool; safecall;
    function Get_RollRange: OleVariant; safecall;
    function Get_Zoom: Integer; safecall;
    procedure Set_Zoom(CurrentZoomValue: Integer); safecall;
    function Get_ZoomAuto: WordBool; safecall;
    procedure Set_ZoomAuto(CurrentZoomAutoValue: WordBool); safecall;
    function Get_ZoomAvailable: WordBool; safecall;
    function Get_ZoomAutoAvailable: WordBool; safecall;
    function Get_ZoomRange: OleVariant; safecall;
    function Get_Exposure: Integer; safecall;
    procedure Set_Exposure(CurrentExposureValue: Integer); safecall;
    function Get_ExposureAuto: WordBool; safecall;
    procedure Set_ExposureAuto(CurrentExposureAutoValue: WordBool); safecall;
    function Get_ExposureAvailable: WordBool; safecall;
    function Get_ExposureAutoAvailable: WordBool; safecall;
    function Get_ExposureRange: OleVariant; safecall;
    function Get_Iris: Integer; safecall;
    procedure Set_Iris(CurrentIrisValue: Integer); safecall;
    function Get_IrisAuto: WordBool; safecall;
    procedure Set_IrisAuto(CurrentIrisAutoValue: WordBool); safecall;
    function Get_IrisAvailable: WordBool; safecall;
    function Get_IrisAutoAvailable: WordBool; safecall;
    function Get_IrisRange: OleVariant; safecall;
    function Get_Focus: Integer; safecall;
    procedure Set_Focus(CurrentFocusValue: Integer); safecall;
    function Get_FocusAuto: WordBool; safecall;
    procedure Set_FocusAuto(CurrentFocusAutoValue: WordBool); safecall;
    function Get_FocusAvailable: WordBool; safecall;
    function Get_FocusAutoAvailable: WordBool; safecall;
    function Get_FocusRange: OleVariant; safecall;
    function Get_MemorySnapTimeout: Integer; safecall;
    procedure Set_MemorySnapTimeout(CurrentTimeoutValue: Integer); safecall;
    function Get_ExternalTransportAvailable: WordBool; safecall;
    function Get_ExternalTransportMode: ExternalTransportModes; safecall;
    procedure Set_ExternalTransportMode(CurrentMode: ExternalTransportModes); safecall;
    function Get_Picture: IUnknown; safecall;
    function Get_LiveDisplay: WordBool; safecall;
    procedure Set_LiveDisplay(CurrentValue: WordBool); safecall;
    function Get_LiveCaptureLastImage: WordBool; safecall;
    procedure Set_LiveCaptureLastImage(CurrentValue: WordBool); safecall;
    function Get_LiveCaptureContinuous: WordBool; safecall;
    procedure Set_LiveCaptureContinuous(CurrentValue: WordBool); safecall;
    function Get_DeviceFrameRateAvailable: WordBool; safecall;
    function Get_DeviceFrameRate: Single; safecall;
    procedure Set_DeviceFrameRate(CurrentValue: Single); safecall;
    function Get_DeviceFrameRates: IDeviceFrameRates; safecall;
    function Get_DeviceCurrentActualFrameRate: Single; safecall;
    function Get_ImageBuffers: IImageBuffers; safecall;
    function Get_ImageRingBufferSize: Integer; safecall;
    procedure Set_ImageRingBufferSize(CurrenRingBufferSize: Integer); safecall;
    function Get_ImageBitsPerPixel: Integer; safecall;
    procedure Display; safecall;
    procedure DisplayImageBuffer(const ImageBufferToDisplay: IImageBuffer); safecall;
    function Get_DeviceTriggerAvailable: WordBool; safecall;
    function Get_DeviceTrigger: WordBool; safecall;
    procedure Set_DeviceTrigger(CurrentValue: WordBool); safecall;
    function Get_VideoFormats: IVideoFormats; safecall;
    function Get_Devices: IDevices; safecall;
    function Get_VideoNorms: IVideoNorms; safecall;
    function Get_InputChannels: IInputChannels; safecall;
    function Get_AviCompressors: IAviCompressors; safecall;
    function Get_DeviceValid: WordBool; safecall;
    function Get_LiveCapturePause: WordBool; safecall;
    procedure Set_LiveCapturePause(Val: WordBool); safecall;
    function Get_LiveDisplayWidth: Integer; safecall;
    procedure Set_LiveDisplayWidth(Val: Integer); safecall;
    function Get_LiveDisplayHeight: Integer; safecall;
    procedure Set_LiveDisplayHeight(Val: Integer); safecall;
    function Get_LiveDisplayLeft: Integer; safecall;
    procedure Set_LiveDisplayLeft(Val: Integer); safecall;
    function Get_LiveDisplayTop: Integer; safecall;
    procedure Set_LiveDisplayTop(Val: Integer); safecall;
    function Get_LiveDisplayDefault: WordBool; safecall;
    procedure Set_LiveDisplayDefault(Val: WordBool); safecall;
    function Get_DeviceFlipVerticalAvailable: WordBool; safecall;
    function Get_DeviceFlipVertical: WordBool; safecall;
    procedure Set_DeviceFlipVertical(Val: WordBool); safecall;
    function Get_DeviceFlipHorizontalAvailable: WordBool; safecall;
    function Get_DeviceFlipHorizontal: WordBool; safecall;
    procedure Set_DeviceFlipHorizontal(Val: WordBool); safecall;
    function Get_OverlayBitmap: IOverlayBitmap; safecall;
    function Get_OverlayUpdateEventEnable: WordBool; safecall;
    procedure Set_OverlayUpdateEventEnable(Enabled: WordBool); safecall;
    function Get_LiveDisplayZoomFactor: Single; safecall;
    procedure Set_LiveDisplayZoomFactor(Val: Single); safecall;
    function Get_ScrollbarsEnabled: WordBool; safecall;
    procedure Set_ScrollbarsEnabled(Val: WordBool); safecall;
    function Get_ReferenceTimeCurrent: Double; safecall;
    function Get_ReferenceTimeStart: Double; safecall;
    function Get_BrightnessDefault: Integer; safecall;
    function Get_ContrastDefault: Integer; safecall;
    function Get_HueDefault: Integer; safecall;
    function Get_SaturationDefault: Integer; safecall;
    function Get_SharpnessDefault: Integer; safecall;
    function Get_GammaDefault: Integer; safecall;
    function Get_ColorEnableDefault: Integer; safecall;
    function Get_WhiteBalanceDefault: Integer; safecall;
    function Get_BacklightCompensationDefault: Integer; safecall;
    function Get_GainDefault: Integer; safecall;
    function Get_PanDefault: Integer; safecall;
    function Get_TiltDefault: Integer; safecall;
    function Get_RollDefault: Integer; safecall;
    function Get_ZoomDefault: Integer; safecall;
    function Get_ExposureDefault: Integer; safecall;
    function Get_IrisDefault: Integer; safecall;
    function Get_FocusDefault: Integer; safecall;
    function Get_VCDPropertyItems: IVCDPropertyItems; safecall;
    function Get_SignalDetectedAvailable: WordBool; safecall;
    function Get_SignalDetected: WordBool; safecall;
    function Get_LivePause: WordBool; safecall;
    procedure Set_LivePause(IsLivePauseSet: WordBool); safecall;
    function Get_DeviceCountOfFramesDropped: Integer; safecall;
    function Get_DeviceCountOfFramesNotDropped: Integer; safecall;
    function GetGrabberPtr: Integer; safecall;
    function SaveDeviceState: WideString; safecall;
    procedure LoadDeviceState(const String_: WideString; OpenDev: WordBool); safecall;
    procedure SaveDeviceStateToFile(const Filename: WideString); safecall;
    procedure LoadDeviceStateFromFile(const Filename: WideString; OpenDev: WordBool); safecall;
    function Get_DeviceUniqueName: WideString; safecall;
    procedure Set_DeviceUniqueName(const CurrentDevice: WideString); safecall;
    procedure ShowPropertyDialog; safecall;
    procedure ShowDeviceSettingsDialog; safecall;
    function Get_Sink: IBaseSink; safecall;
    procedure Set_Sink(const CurrentSink: IBaseSink); safecall;
    function Get_MediaStreamContainers: IMediaStreamContainers; safecall;
    procedure Set_FrameFilterPath(const Param1: WideString); safecall;
    function Get_FrameFilterInfos: IFrameFilterInfos; safecall;
    function FrameFilterCreate(const Info: IFrameFilterInfo): IFrameFilter; safecall;
    function FrameFilterCreateString(const FilterName: WideString; const ModuleName: WideString): IFrameFilter; safecall;
    function Get_DeviceFrameFilters: IFrameFilters; safecall;
    function Get_LiveDisplayOutputWidth: Integer; safecall;
    function Get_LiveDisplayOutputHeight: Integer; safecall;
    function Get_ImageActiveBuffer: IImageBuffer; safecall;
    function Get_DeBayerActive: WordBool; safecall;
    function Get_DeBayerStartPattern: DeBayerStartPatterns; safecall;
    procedure Set_DeBayerStartPattern(StartPattern: DeBayerStartPatterns); safecall;
    function Get_DeBayerMode: DeBayerModes; safecall;
    procedure Set_DeBayerMode(Mode: DeBayerModes); safecall;
    function Get_SinkCompatibilityMode: WordBool; safecall;
    procedure Set_SinkCompatibilityMode(CurMode: WordBool); safecall;
    procedure _SetDirty(Val: Integer); safecall;
    procedure Set_OverlayBitmapPosition(CurrentPosition: PathPositions); safecall;
    function Get_OverlayBitmapPosition: PathPositions; safecall;
    function Get_DisplayFrameFilters: IFrameFilters; safecall;
    function FrameFilterCreate_(FilterPtr: Integer): IFrameFilter; safecall;
    function Get_OverlayBitmapAtPath(OverlayToFetch: PathPositions): IOverlayBitmap; safecall;
    procedure Set_LiveShowLastBuffer(ShowLastBuffer: WordBool); safecall;
    function Get_LiveShowLastBuffer: WordBool; safecall;
    procedure MemorySaveImageJpeg(const Filename: WideString; Quality: Integer); safecall;
    property Device: WideString read Get_Device write Set_Device;
    property LiveVideoRunning: WordBool read Get_LiveVideoRunning;
    property VideoFormat: WideString read Get_VideoFormat write Set_VideoFormat;
    property ImageWidth: Integer read Get_ImageWidth;
    property ImageHeight: Integer read Get_ImageHeight;
    property VideoNormAvailable: WordBool read Get_VideoNormAvailable;
    property VideoNorm: WideString read Get_VideoNorm write Set_VideoNorm;
    property InputChannelAvailable: WordBool read Get_InputChannelAvailable;
    property InputChannel: WideString read Get_InputChannel write Set_InputChannel;
    property MemoryCurrentGrabberColorformat: ICImagingControlColorformats read Get_MemoryCurrentGrabberColorformat write Set_MemoryCurrentGrabberColorformat;
    property Brightness: Integer read Get_Brightness write Set_Brightness;
    property BrightnessAuto: WordBool read Get_BrightnessAuto write Set_BrightnessAuto;
    property BrightnessAvailable: WordBool read Get_BrightnessAvailable;
    property BrightnessAutoAvailable: WordBool read Get_BrightnessAutoAvailable;
    property BrightnessRange: OleVariant read Get_BrightnessRange;
    property Contrast: Integer read Get_Contrast write Set_Contrast;
    property ContrastAuto: WordBool read Get_ContrastAuto write Set_ContrastAuto;
    property ContrastAvailable: WordBool read Get_ContrastAvailable;
    property ContrastAutoAvailable: WordBool read Get_ContrastAutoAvailable;
    property ContrastRange: OleVariant read Get_ContrastRange;
    property Hue: Integer read Get_Hue write Set_Hue;
    property HueAuto: WordBool read Get_HueAuto write Set_HueAuto;
    property HueAvailable: WordBool read Get_HueAvailable;
    property HueAutoAvailable: WordBool read Get_HueAutoAvailable;
    property HueRange: OleVariant read Get_HueRange;
    property Saturation: Integer read Get_Saturation write Set_Saturation;
    property SaturationAuto: WordBool read Get_SaturationAuto write Set_SaturationAuto;
    property SaturationAvailable: WordBool read Get_SaturationAvailable;
    property SaturationAutoAvailable: WordBool read Get_SaturationAutoAvailable;
    property SaturationRange: OleVariant read Get_SaturationRange;
    property Sharpness: Integer read Get_Sharpness write Set_Sharpness;
    property SharpnessAuto: WordBool read Get_SharpnessAuto write Set_SharpnessAuto;
    property SharpnessAvailable: WordBool read Get_SharpnessAvailable;
    property SharpnessAutoAvailable: WordBool read Get_SharpnessAutoAvailable;
    property SharpnessRange: OleVariant read Get_SharpnessRange;
    property Gamma: Integer read Get_Gamma write Set_Gamma;
    property GammaAuto: WordBool read Get_GammaAuto write Set_GammaAuto;
    property GammaAvailable: WordBool read Get_GammaAvailable;
    property GammaAutoAvailable: WordBool read Get_GammaAutoAvailable;
    property GammaRange: OleVariant read Get_GammaRange;
    property ColorEnable: Integer read Get_ColorEnable write Set_ColorEnable;
    property ColorEnableAuto: WordBool read Get_ColorEnableAuto write Set_ColorEnableAuto;
    property ColorEnableAvailable: WordBool read Get_ColorEnableAvailable;
    property ColorEnableAutoAvailable: WordBool read Get_ColorEnableAutoAvailable;
    property ColorEnableRange: OleVariant read Get_ColorEnableRange;
    property WhiteBalance: Integer read Get_WhiteBalance write Set_WhiteBalance;
    property WhiteBalanceAuto: WordBool read Get_WhiteBalanceAuto write Set_WhiteBalanceAuto;
    property WhiteBalanceAvailable: WordBool read Get_WhiteBalanceAvailable;
    property WhiteBalanceAutoAvailable: WordBool read Get_WhiteBalanceAutoAvailable;
    property WhiteBalanceRange: OleVariant read Get_WhiteBalanceRange;
    property BacklightCompensation: Integer read Get_BacklightCompensation write Set_BacklightCompensation;
    property BacklightCompensationAuto: WordBool read Get_BacklightCompensationAuto write Set_BacklightCompensationAuto;
    property BacklightCompensationAvailable: WordBool read Get_BacklightCompensationAvailable;
    property BacklightCompensationAutoAvailable: WordBool read Get_BacklightCompensationAutoAvailable;
    property BacklightCompensationRange: OleVariant read Get_BacklightCompensationRange;
    property Gain: Integer read Get_Gain write Set_Gain;
    property GainAuto: WordBool read Get_GainAuto write Set_GainAuto;
    property GainAvailable: WordBool read Get_GainAvailable;
    property GainAutoAvailable: WordBool read Get_GainAutoAvailable;
    property GainRange: OleVariant read Get_GainRange;
    property Pan: Integer read Get_Pan write Set_Pan;
    property PanAuto: WordBool read Get_PanAuto write Set_PanAuto;
    property PanAvailable: WordBool read Get_PanAvailable;
    property PanAutoAvailable: WordBool read Get_PanAutoAvailable;
    property PanRange: OleVariant read Get_PanRange;
    property Tilt: Integer read Get_Tilt write Set_Tilt;
    property TiltAuto: WordBool read Get_TiltAuto write Set_TiltAuto;
    property TiltAvailable: WordBool read Get_TiltAvailable;
    property TiltAutoAvailable: WordBool read Get_TiltAutoAvailable;
    property TiltRange: OleVariant read Get_TiltRange;
    property Roll: Integer read Get_Roll write Set_Roll;
    property RollAuto: WordBool read Get_RollAuto write Set_RollAuto;
    property RollAvailable: WordBool read Get_RollAvailable;
    property RollAutoAvailable: WordBool read Get_RollAutoAvailable;
    property RollRange: OleVariant read Get_RollRange;
    property Zoom: Integer read Get_Zoom write Set_Zoom;
    property ZoomAuto: WordBool read Get_ZoomAuto write Set_ZoomAuto;
    property ZoomAvailable: WordBool read Get_ZoomAvailable;
    property ZoomAutoAvailable: WordBool read Get_ZoomAutoAvailable;
    property ZoomRange: OleVariant read Get_ZoomRange;
    property Exposure: Integer read Get_Exposure write Set_Exposure;
    property ExposureAuto: WordBool read Get_ExposureAuto write Set_ExposureAuto;
    property ExposureAvailable: WordBool read Get_ExposureAvailable;
    property ExposureAutoAvailable: WordBool read Get_ExposureAutoAvailable;
    property ExposureRange: OleVariant read Get_ExposureRange;
    property Iris: Integer read Get_Iris write Set_Iris;
    property IrisAuto: WordBool read Get_IrisAuto write Set_IrisAuto;
    property IrisAvailable: WordBool read Get_IrisAvailable;
    property IrisAutoAvailable: WordBool read Get_IrisAutoAvailable;
    property IrisRange: OleVariant read Get_IrisRange;
    property Focus: Integer read Get_Focus write Set_Focus;
    property FocusAuto: WordBool read Get_FocusAuto write Set_FocusAuto;
    property FocusAvailable: WordBool read Get_FocusAvailable;
    property FocusAutoAvailable: WordBool read Get_FocusAutoAvailable;
    property FocusRange: OleVariant read Get_FocusRange;
    property MemorySnapTimeout: Integer read Get_MemorySnapTimeout write Set_MemorySnapTimeout;
    property ExternalTransportAvailable: WordBool read Get_ExternalTransportAvailable;
    property ExternalTransportMode: ExternalTransportModes read Get_ExternalTransportMode write Set_ExternalTransportMode;
    property Picture: IUnknown read Get_Picture;
    property LiveDisplay: WordBool read Get_LiveDisplay write Set_LiveDisplay;
    property LiveCaptureLastImage: WordBool read Get_LiveCaptureLastImage write Set_LiveCaptureLastImage;
    property LiveCaptureContinuous: WordBool read Get_LiveCaptureContinuous write Set_LiveCaptureContinuous;
    property DeviceFrameRateAvailable: WordBool read Get_DeviceFrameRateAvailable;
    property DeviceFrameRate: Single read Get_DeviceFrameRate write Set_DeviceFrameRate;
    property DeviceFrameRates: IDeviceFrameRates read Get_DeviceFrameRates;
    property DeviceCurrentActualFrameRate: Single read Get_DeviceCurrentActualFrameRate;
    property ImageBuffers: IImageBuffers read Get_ImageBuffers;
    property ImageRingBufferSize: Integer read Get_ImageRingBufferSize write Set_ImageRingBufferSize;
    property ImageBitsPerPixel: Integer read Get_ImageBitsPerPixel;
    property DeviceTriggerAvailable: WordBool read Get_DeviceTriggerAvailable;
    property DeviceTrigger: WordBool read Get_DeviceTrigger write Set_DeviceTrigger;
    property VideoFormats: IVideoFormats read Get_VideoFormats;
    property Devices: IDevices read Get_Devices;
    property VideoNorms: IVideoNorms read Get_VideoNorms;
    property InputChannels: IInputChannels read Get_InputChannels;
    property AviCompressors: IAviCompressors read Get_AviCompressors;
    property DeviceValid: WordBool read Get_DeviceValid;
    property LiveCapturePause: WordBool read Get_LiveCapturePause write Set_LiveCapturePause;
    property LiveDisplayWidth: Integer read Get_LiveDisplayWidth write Set_LiveDisplayWidth;
    property LiveDisplayHeight: Integer read Get_LiveDisplayHeight write Set_LiveDisplayHeight;
    property LiveDisplayLeft: Integer read Get_LiveDisplayLeft write Set_LiveDisplayLeft;
    property LiveDisplayTop: Integer read Get_LiveDisplayTop write Set_LiveDisplayTop;
    property LiveDisplayDefault: WordBool read Get_LiveDisplayDefault write Set_LiveDisplayDefault;
    property DeviceFlipVerticalAvailable: WordBool read Get_DeviceFlipVerticalAvailable;
    property DeviceFlipVertical: WordBool read Get_DeviceFlipVertical write Set_DeviceFlipVertical;
    property DeviceFlipHorizontalAvailable: WordBool read Get_DeviceFlipHorizontalAvailable;
    property DeviceFlipHorizontal: WordBool read Get_DeviceFlipHorizontal write Set_DeviceFlipHorizontal;
    property OverlayBitmap: IOverlayBitmap read Get_OverlayBitmap;
    property OverlayUpdateEventEnable: WordBool read Get_OverlayUpdateEventEnable write Set_OverlayUpdateEventEnable;
    property LiveDisplayZoomFactor: Single read Get_LiveDisplayZoomFactor write Set_LiveDisplayZoomFactor;
    property ScrollbarsEnabled: WordBool read Get_ScrollbarsEnabled write Set_ScrollbarsEnabled;
    property ReferenceTimeCurrent: Double read Get_ReferenceTimeCurrent;
    property ReferenceTimeStart: Double read Get_ReferenceTimeStart;
    property BrightnessDefault: Integer read Get_BrightnessDefault;
    property ContrastDefault: Integer read Get_ContrastDefault;
    property HueDefault: Integer read Get_HueDefault;
    property SaturationDefault: Integer read Get_SaturationDefault;
    property SharpnessDefault: Integer read Get_SharpnessDefault;
    property GammaDefault: Integer read Get_GammaDefault;
    property ColorEnableDefault: Integer read Get_ColorEnableDefault;
    property WhiteBalanceDefault: Integer read Get_WhiteBalanceDefault;
    property BacklightCompensationDefault: Integer read Get_BacklightCompensationDefault;
    property GainDefault: Integer read Get_GainDefault;
    property PanDefault: Integer read Get_PanDefault;
    property TiltDefault: Integer read Get_TiltDefault;
    property RollDefault: Integer read Get_RollDefault;
    property ZoomDefault: Integer read Get_ZoomDefault;
    property ExposureDefault: Integer read Get_ExposureDefault;
    property IrisDefault: Integer read Get_IrisDefault;
    property FocusDefault: Integer read Get_FocusDefault;
    property VCDPropertyItems: IVCDPropertyItems read Get_VCDPropertyItems;
    property SignalDetectedAvailable: WordBool read Get_SignalDetectedAvailable;
    property SignalDetected: WordBool read Get_SignalDetected;
    property LivePause: WordBool read Get_LivePause write Set_LivePause;
    property DeviceCountOfFramesDropped: Integer read Get_DeviceCountOfFramesDropped;
    property DeviceCountOfFramesNotDropped: Integer read Get_DeviceCountOfFramesNotDropped;
    property DeviceUniqueName: WideString read Get_DeviceUniqueName write Set_DeviceUniqueName;
    property Sink: IBaseSink read Get_Sink write Set_Sink;
    property MediaStreamContainers: IMediaStreamContainers read Get_MediaStreamContainers;
    property FrameFilterPath: WideString write Set_FrameFilterPath;
    property FrameFilterInfos: IFrameFilterInfos read Get_FrameFilterInfos;
    property DeviceFrameFilters: IFrameFilters read Get_DeviceFrameFilters;
    property LiveDisplayOutputWidth: Integer read Get_LiveDisplayOutputWidth;
    property LiveDisplayOutputHeight: Integer read Get_LiveDisplayOutputHeight;
    property ImageActiveBuffer: IImageBuffer read Get_ImageActiveBuffer;
    property DeBayerActive: WordBool read Get_DeBayerActive;
    property DeBayerStartPattern: DeBayerStartPatterns read Get_DeBayerStartPattern write Set_DeBayerStartPattern;
    property DeBayerMode: DeBayerModes read Get_DeBayerMode write Set_DeBayerMode;
    property SinkCompatibilityMode: WordBool read Get_SinkCompatibilityMode write Set_SinkCompatibilityMode;
    property OverlayBitmapPosition: PathPositions read Get_OverlayBitmapPosition write Set_OverlayBitmapPosition;
    property DisplayFrameFilters: IFrameFilters read Get_DisplayFrameFilters;
    property OverlayBitmapAtPath[OverlayToFetch: PathPositions]: IOverlayBitmap read Get_OverlayBitmapAtPath;
    property LiveShowLastBuffer: WordBool read Get_LiveShowLastBuffer write Set_LiveShowLastBuffer;
  end;

// *********************************************************************//
// DispIntf:  IICImagingControlDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {E0110BE7-EBF0-4612-B2F8-817194557DE8}
// *********************************************************************//
  IICImagingControlDisp = dispinterface
    ['{E0110BE7-EBF0-4612-B2F8-817194557DE8}']
    procedure Refresh; dispid 1;
    property Device: WideString dispid 4;
    property LiveVideoRunning: WordBool readonly dispid 5;
    procedure LiveStart; dispid 8;
    procedure LiveStop; dispid 9;
    property VideoFormat: WideString dispid 11;
    property ImageWidth: Integer readonly dispid 12;
    property ImageHeight: Integer readonly dispid 13;
    property VideoNormAvailable: WordBool readonly dispid 14;
    property VideoNorm: WideString dispid 16;
    property InputChannelAvailable: WordBool readonly dispid 17;
    property InputChannel: WideString dispid 19;
    property MemoryCurrentGrabberColorformat: ICImagingControlColorformats dispid 21;
    procedure MemorySaveImage(const Filename: WideString); dispid 22;
    procedure MemorySaveImageSequence(SequenceLength: Integer; const Filename: WideString); dispid 23;
    procedure MemorySnapImage; dispid 24;
    procedure MemorySnapImageSequence(ImageCount: Integer); dispid 25;
    function MemoryGetImageData: OleVariant; dispid 26;
    procedure MemoryReleaseImageData(var Buffer: OleVariant); dispid 27;
    function MemoryGetDib: Integer; dispid 28;
    procedure AviStartCapture(const Filename: WideString; const Compressor: WideString); dispid 30;
    procedure AviStopCapture; dispid 31;
    property Brightness: Integer dispid 32;
    property BrightnessAuto: WordBool dispid 33;
    property BrightnessAvailable: WordBool readonly dispid 35;
    property BrightnessAutoAvailable: WordBool readonly dispid 36;
    property BrightnessRange: OleVariant readonly dispid 34;
    property Contrast: Integer dispid 37;
    property ContrastAuto: WordBool dispid 38;
    property ContrastAvailable: WordBool readonly dispid 40;
    property ContrastAutoAvailable: WordBool readonly dispid 41;
    property ContrastRange: OleVariant readonly dispid 39;
    property Hue: Integer dispid 42;
    property HueAuto: WordBool dispid 43;
    property HueAvailable: WordBool readonly dispid 45;
    property HueAutoAvailable: WordBool readonly dispid 46;
    property HueRange: OleVariant readonly dispid 44;
    property Saturation: Integer dispid 47;
    property SaturationAuto: WordBool dispid 48;
    property SaturationAvailable: WordBool readonly dispid 50;
    property SaturationAutoAvailable: WordBool readonly dispid 51;
    property SaturationRange: OleVariant readonly dispid 49;
    property Sharpness: Integer dispid 52;
    property SharpnessAuto: WordBool dispid 53;
    property SharpnessAvailable: WordBool readonly dispid 55;
    property SharpnessAutoAvailable: WordBool readonly dispid 56;
    property SharpnessRange: OleVariant readonly dispid 54;
    property Gamma: Integer dispid 57;
    property GammaAuto: WordBool dispid 58;
    property GammaAvailable: WordBool readonly dispid 60;
    property GammaAutoAvailable: WordBool readonly dispid 61;
    property GammaRange: OleVariant readonly dispid 59;
    property ColorEnable: Integer dispid 62;
    property ColorEnableAuto: WordBool dispid 63;
    property ColorEnableAvailable: WordBool readonly dispid 65;
    property ColorEnableAutoAvailable: WordBool readonly dispid 66;
    property ColorEnableRange: OleVariant readonly dispid 64;
    property WhiteBalance: Integer dispid 67;
    property WhiteBalanceAuto: WordBool dispid 68;
    property WhiteBalanceAvailable: WordBool readonly dispid 70;
    property WhiteBalanceAutoAvailable: WordBool readonly dispid 71;
    property WhiteBalanceRange: OleVariant readonly dispid 69;
    property BacklightCompensation: Integer dispid 72;
    property BacklightCompensationAuto: WordBool dispid 73;
    property BacklightCompensationAvailable: WordBool readonly dispid 75;
    property BacklightCompensationAutoAvailable: WordBool readonly dispid 76;
    property BacklightCompensationRange: OleVariant readonly dispid 74;
    property Gain: Integer dispid 77;
    property GainAuto: WordBool dispid 78;
    property GainAvailable: WordBool readonly dispid 80;
    property GainAutoAvailable: WordBool readonly dispid 81;
    property GainRange: OleVariant readonly dispid 79;
    property Pan: Integer dispid 82;
    property PanAuto: WordBool dispid 83;
    property PanAvailable: WordBool readonly dispid 85;
    property PanAutoAvailable: WordBool readonly dispid 86;
    property PanRange: OleVariant readonly dispid 84;
    property Tilt: Integer dispid 87;
    property TiltAuto: WordBool dispid 88;
    property TiltAvailable: WordBool readonly dispid 90;
    property TiltAutoAvailable: WordBool readonly dispid 91;
    property TiltRange: OleVariant readonly dispid 89;
    property Roll: Integer dispid 92;
    property RollAuto: WordBool dispid 93;
    property RollAvailable: WordBool readonly dispid 95;
    property RollAutoAvailable: WordBool readonly dispid 96;
    property RollRange: OleVariant readonly dispid 94;
    property Zoom: Integer dispid 97;
    property ZoomAuto: WordBool dispid 98;
    property ZoomAvailable: WordBool readonly dispid 100;
    property ZoomAutoAvailable: WordBool readonly dispid 101;
    property ZoomRange: OleVariant readonly dispid 99;
    property Exposure: Integer dispid 102;
    property ExposureAuto: WordBool dispid 103;
    property ExposureAvailable: WordBool readonly dispid 105;
    property ExposureAutoAvailable: WordBool readonly dispid 106;
    property ExposureRange: OleVariant readonly dispid 104;
    property Iris: Integer dispid 107;
    property IrisAuto: WordBool dispid 108;
    property IrisAvailable: WordBool readonly dispid 110;
    property IrisAutoAvailable: WordBool readonly dispid 111;
    property IrisRange: OleVariant readonly dispid 109;
    property Focus: Integer dispid 112;
    property FocusAuto: WordBool dispid 113;
    property FocusAvailable: WordBool readonly dispid 115;
    property FocusAutoAvailable: WordBool readonly dispid 116;
    property FocusRange: OleVariant readonly dispid 114;
    property MemorySnapTimeout: Integer dispid 120;
    property ExternalTransportAvailable: WordBool readonly dispid 117;
    property ExternalTransportMode: ExternalTransportModes dispid 118;
    property Picture: IUnknown readonly dispid 121;
    property LiveDisplay: WordBool dispid 122;
    property LiveCaptureLastImage: WordBool dispid 123;
    property LiveCaptureContinuous: WordBool dispid 124;
    property DeviceFrameRateAvailable: WordBool readonly dispid 126;
    property DeviceFrameRate: Single dispid 125;
    property DeviceFrameRates: IDeviceFrameRates readonly dispid 127;
    property DeviceCurrentActualFrameRate: Single readonly dispid 128;
    property ImageBuffers: IImageBuffers readonly dispid 130;
    property ImageRingBufferSize: Integer dispid 131;
    property ImageBitsPerPixel: Integer readonly dispid 141;
    procedure Display; dispid 139;
    procedure DisplayImageBuffer(const ImageBufferToDisplay: IImageBuffer); dispid 140;
    property DeviceTriggerAvailable: WordBool readonly dispid 132;
    property DeviceTrigger: WordBool dispid 133;
    property VideoFormats: IVideoFormats readonly dispid 134;
    property Devices: IDevices readonly dispid 135;
    property VideoNorms: IVideoNorms readonly dispid 136;
    property InputChannels: IInputChannels readonly dispid 137;
    property AviCompressors: IAviCompressors readonly dispid 138;
    property DeviceValid: WordBool readonly dispid 142;
    property LiveCapturePause: WordBool dispid 143;
    property LiveDisplayWidth: Integer dispid 144;
    property LiveDisplayHeight: Integer dispid 145;
    property LiveDisplayLeft: Integer dispid 146;
    property LiveDisplayTop: Integer dispid 147;
    property LiveDisplayDefault: WordBool dispid 148;
    property DeviceFlipVerticalAvailable: WordBool readonly dispid 149;
    property DeviceFlipVertical: WordBool dispid 150;
    property DeviceFlipHorizontalAvailable: WordBool readonly dispid 151;
    property DeviceFlipHorizontal: WordBool dispid 152;
    property OverlayBitmap: IOverlayBitmap readonly dispid 153;
    property OverlayUpdateEventEnable: WordBool dispid 154;
    property LiveDisplayZoomFactor: Single dispid 155;
    property ScrollbarsEnabled: WordBool dispid 156;
    property ReferenceTimeCurrent: Double readonly dispid 157;
    property ReferenceTimeStart: Double readonly dispid 158;
    property BrightnessDefault: Integer readonly dispid 160;
    property ContrastDefault: Integer readonly dispid 161;
    property HueDefault: Integer readonly dispid 162;
    property SaturationDefault: Integer readonly dispid 163;
    property SharpnessDefault: Integer readonly dispid 164;
    property GammaDefault: Integer readonly dispid 165;
    property ColorEnableDefault: Integer readonly dispid 166;
    property WhiteBalanceDefault: Integer readonly dispid 167;
    property BacklightCompensationDefault: Integer readonly dispid 168;
    property GainDefault: Integer readonly dispid 169;
    property PanDefault: Integer readonly dispid 170;
    property TiltDefault: Integer readonly dispid 171;
    property RollDefault: Integer readonly dispid 172;
    property ZoomDefault: Integer readonly dispid 173;
    property ExposureDefault: Integer readonly dispid 174;
    property IrisDefault: Integer readonly dispid 175;
    property FocusDefault: Integer readonly dispid 176;
    property VCDPropertyItems: IVCDPropertyItems readonly dispid 177;
    property SignalDetectedAvailable: WordBool readonly dispid 178;
    property SignalDetected: WordBool readonly dispid 179;
    property LivePause: WordBool dispid 180;
    property DeviceCountOfFramesDropped: Integer readonly dispid 181;
    property DeviceCountOfFramesNotDropped: Integer readonly dispid 182;
    function GetGrabberPtr: Integer; dispid 183;
    function SaveDeviceState: WideString; dispid 184;
    procedure LoadDeviceState(const String_: WideString; OpenDev: WordBool); dispid 185;
    procedure SaveDeviceStateToFile(const Filename: WideString); dispid 186;
    procedure LoadDeviceStateFromFile(const Filename: WideString; OpenDev: WordBool); dispid 187;
    property DeviceUniqueName: WideString dispid 188;
    procedure ShowPropertyDialog; dispid 189;
    procedure ShowDeviceSettingsDialog; dispid 190;
    property Sink: IBaseSink dispid 191;
    property MediaStreamContainers: IMediaStreamContainers readonly dispid 192;
    property FrameFilterPath: WideString writeonly dispid 193;
    property FrameFilterInfos: IFrameFilterInfos readonly dispid 194;
    function FrameFilterCreate(const Info: IFrameFilterInfo): IFrameFilter; dispid 195;
    function FrameFilterCreateString(const FilterName: WideString; const ModuleName: WideString): IFrameFilter; dispid 196;
    property DeviceFrameFilters: IFrameFilters readonly dispid 197;
    property LiveDisplayOutputWidth: Integer readonly dispid 198;
    property LiveDisplayOutputHeight: Integer readonly dispid 199;
    property ImageActiveBuffer: IImageBuffer readonly dispid 200;
    property DeBayerActive: WordBool readonly dispid 201;
    property DeBayerStartPattern: DeBayerStartPatterns dispid 202;
    property DeBayerMode: DeBayerModes dispid 203;
    property SinkCompatibilityMode: WordBool dispid 204;
    procedure _SetDirty(Val: Integer); dispid 205;
    property OverlayBitmapPosition: PathPositions dispid 206;
    property DisplayFrameFilters: IFrameFilters readonly dispid 207;
    function FrameFilterCreate_(FilterPtr: Integer): IFrameFilter; dispid 208;
    property OverlayBitmapAtPath[OverlayToFetch: PathPositions]: IOverlayBitmap readonly dispid 209;
    property LiveShowLastBuffer: WordBool dispid 210;
    procedure MemorySaveImageJpeg(const Filename: WideString; Quality: Integer); dispid 211;
  end;

// *********************************************************************//
// DispIntf:  _IICImagingControlEvents
// Flags:     (4096) Dispatchable
// GUID:      {E0110BE7-EBF0-4612-B2F8-817194557DE9}
// *********************************************************************//
  _IICImagingControlEvents = dispinterface
    ['{E0110BE7-EBF0-4612-B2F8-817194557DE9}']
    procedure MouseDown(Button: Integer; Shift: Integer; XPos: Smallint; YPos: Smallint); dispid 1;
    procedure MouseUp(Button: Integer; Shift: Integer; XPos: Smallint; YPos: Smallint); dispid 2;
    procedure MouseMove(Button: Integer; Shift: Integer; XPos: Smallint; YPos: Smallint); dispid 3;
    procedure ImageAvailable(BufferIndex: Integer); dispid 4;
    procedure DeviceLost; dispid 5;
    procedure OnHScroll(XPos: Integer; YPos: Integer); dispid 7;
    procedure OnVScroll(XPos: Integer; YPos: Integer); dispid 8;
    procedure OverlayUpdate(const Overlay: OverlayBitmap; SampleStartTime: Double; 
                            SampleEndTime: Double); dispid 6;
  end;

// *********************************************************************//
// The Class CoOverlayBitmap provides a Create and CreateRemote method to          
// create instances of the default interface IOverlayBitmap exposed by              
// the CoClass OverlayBitmap. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoOverlayBitmap = class
    class function Create: IOverlayBitmap;
    class function CreateRemote(const MachineName: string): IOverlayBitmap;
  end;

// *********************************************************************//
// The Class CoImageBuffer provides a Create and CreateRemote method to          
// create instances of the default interface IImageBuffer exposed by              
// the CoClass ImageBuffer. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoImageBuffer = class
    class function Create: IImageBuffer;
    class function CreateRemote(const MachineName: string): IImageBuffer;
  end;

// *********************************************************************//
// The Class CoImageBuffers provides a Create and CreateRemote method to          
// create instances of the default interface IImageBuffers exposed by              
// the CoClass ImageBuffers. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoImageBuffers = class
    class function Create: IImageBuffers;
    class function CreateRemote(const MachineName: string): IImageBuffers;
  end;

// *********************************************************************//
// The Class CoVideoFormat provides a Create and CreateRemote method to          
// create instances of the default interface IVideoFormat exposed by              
// the CoClass VideoFormat. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoVideoFormat = class
    class function Create: IVideoFormat;
    class function CreateRemote(const MachineName: string): IVideoFormat;
  end;

// *********************************************************************//
// The Class CoVideoFormats provides a Create and CreateRemote method to          
// create instances of the default interface IVideoFormats exposed by              
// the CoClass VideoFormats. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoVideoFormats = class
    class function Create: IVideoFormats;
    class function CreateRemote(const MachineName: string): IVideoFormats;
  end;

// *********************************************************************//
// The Class CoDevice provides a Create and CreateRemote method to          
// create instances of the default interface IDevice exposed by              
// the CoClass Device. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoDevice = class
    class function Create: IDevice;
    class function CreateRemote(const MachineName: string): IDevice;
  end;

// *********************************************************************//
// The Class CoDevices provides a Create and CreateRemote method to          
// create instances of the default interface IDevices exposed by              
// the CoClass Devices. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoDevices = class
    class function Create: IDevices;
    class function CreateRemote(const MachineName: string): IDevices;
  end;

// *********************************************************************//
// The Class CoVideoNorm provides a Create and CreateRemote method to          
// create instances of the default interface IVideoNorm exposed by              
// the CoClass VideoNorm. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoVideoNorm = class
    class function Create: IVideoNorm;
    class function CreateRemote(const MachineName: string): IVideoNorm;
  end;

// *********************************************************************//
// The Class CoVideoNorms provides a Create and CreateRemote method to          
// create instances of the default interface IVideoNorms exposed by              
// the CoClass VideoNorms. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoVideoNorms = class
    class function Create: IVideoNorms;
    class function CreateRemote(const MachineName: string): IVideoNorms;
  end;

// *********************************************************************//
// The Class CoInputChannel provides a Create and CreateRemote method to          
// create instances of the default interface IInputChannel exposed by              
// the CoClass InputChannel. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoInputChannel = class
    class function Create: IInputChannel;
    class function CreateRemote(const MachineName: string): IInputChannel;
  end;

// *********************************************************************//
// The Class CoInputChannels provides a Create and CreateRemote method to          
// create instances of the default interface IInputChannels exposed by              
// the CoClass InputChannels. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoInputChannels = class
    class function Create: IInputChannels;
    class function CreateRemote(const MachineName: string): IInputChannels;
  end;

// *********************************************************************//
// The Class CoDeviceFrameRates provides a Create and CreateRemote method to          
// create instances of the default interface IDeviceFrameRates exposed by              
// the CoClass DeviceFrameRates. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoDeviceFrameRates = class
    class function Create: IDeviceFrameRates;
    class function CreateRemote(const MachineName: string): IDeviceFrameRates;
  end;

// *********************************************************************//
// The Class CoAviCompressor provides a Create and CreateRemote method to          
// create instances of the default interface IAviCompressor exposed by              
// the CoClass AviCompressor. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoAviCompressor = class
    class function Create: IAviCompressor;
    class function CreateRemote(const MachineName: string): IAviCompressor;
  end;

// *********************************************************************//
// The Class CoAviCompressors provides a Create and CreateRemote method to          
// create instances of the default interface IAviCompressors exposed by              
// the CoClass AviCompressors. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoAviCompressors = class
    class function Create: IAviCompressors;
    class function CreateRemote(const MachineName: string): IAviCompressors;
  end;

// *********************************************************************//
// The Class CoCICPropertyPage provides a Create and CreateRemote method to          
// create instances of the default interface IUnknown exposed by              
// the CoClass CICPropertyPage. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCICPropertyPage = class
    class function Create: IUnknown;
    class function CreateRemote(const MachineName: string): IUnknown;
  end;


// *********************************************************************//
// OLE Server Proxy class declaration
// Server Object    : TCICPropertyPage
// Help String      : IC Property Page
// Default Interface: IUnknown
// Def. Intf. DISP? : No
// Event   Interface: 
// TypeFlags        : (2) CanCreate
// *********************************************************************//
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  TCICPropertyPageProperties= class;
{$ENDIF}
  TCICPropertyPage = class(TOleServer)
  private
    FIntf:        IUnknown;
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
    FProps:       TCICPropertyPageProperties;
    function      GetServerProperties: TCICPropertyPageProperties;
{$ENDIF}
    function      GetDefaultInterface: IUnknown;
  protected
    procedure InitServerData; override;
  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    procedure Connect; override;
    procedure ConnectTo(svrIntf: IUnknown);
    procedure Disconnect; override;
    property DefaultInterface: IUnknown read GetDefaultInterface;
  published
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
    property Server: TCICPropertyPageProperties read GetServerProperties;
{$ENDIF}
  end;

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
// *********************************************************************//
// OLE Server Properties Proxy Class
// Server Object    : TCICPropertyPage
// (This object is used by the IDE's Property Inspector to allow editing
//  of the properties of this server)
// *********************************************************************//
 TCICPropertyPageProperties = class(TPersistent)
  private
    FServer:    TCICPropertyPage;
    function    GetDefaultInterface: IUnknown;
    constructor Create(AServer: TCICPropertyPage);
  protected
  public
    property DefaultInterface: IUnknown read GetDefaultInterface;
  published
  end;
{$ENDIF}


// *********************************************************************//
// The Class CoBSTRCollection provides a Create and CreateRemote method to          
// create instances of the default interface IBSTRCollection exposed by              
// the CoClass BSTRCollection. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoBSTRCollection = class
    class function Create: IBSTRCollection;
    class function CreateRemote(const MachineName: string): IBSTRCollection;
  end;

// *********************************************************************//
// The Class CoVCDCategoryMap provides a Create and CreateRemote method to          
// create instances of the default interface IVCDCategoryMap exposed by              
// the CoClass VCDCategoryMap. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoVCDCategoryMap = class
    class function Create: IVCDCategoryMap;
    class function CreateRemote(const MachineName: string): IVCDCategoryMap;
  end;

// *********************************************************************//
// The Class CoVCDPropertyItems provides a Create and CreateRemote method to          
// create instances of the default interface IVCDPropertyItems exposed by              
// the CoClass VCDPropertyItems. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoVCDPropertyItems = class
    class function Create: IVCDPropertyItems;
    class function CreateRemote(const MachineName: string): IVCDPropertyItems;
  end;

// *********************************************************************//
// The Class CoVCDPropertyItem provides a Create and CreateRemote method to          
// create instances of the default interface IVCDPropertyItem exposed by              
// the CoClass VCDPropertyItem. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoVCDPropertyItem = class
    class function Create: IVCDPropertyItem;
    class function CreateRemote(const MachineName: string): IVCDPropertyItem;
  end;

// *********************************************************************//
// The Class CoVCDPropertyElements provides a Create and CreateRemote method to          
// create instances of the default interface IVCDPropertyElements exposed by              
// the CoClass VCDPropertyElements. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoVCDPropertyElements = class
    class function Create: IVCDPropertyElements;
    class function CreateRemote(const MachineName: string): IVCDPropertyElements;
  end;

// *********************************************************************//
// The Class CoVCDPropertyElement provides a Create and CreateRemote method to          
// create instances of the default interface IVCDPropertyElement exposed by              
// the CoClass VCDPropertyElement. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoVCDPropertyElement = class
    class function Create: IVCDPropertyElement;
    class function CreateRemote(const MachineName: string): IVCDPropertyElement;
  end;

// *********************************************************************//
// The Class CoVCDPropertyInterface provides a Create and CreateRemote method to          
// create instances of the default interface IVCDPropertyInterface exposed by              
// the CoClass VCDPropertyInterface. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoVCDPropertyInterface = class
    class function Create: IVCDPropertyInterface;
    class function CreateRemote(const MachineName: string): IVCDPropertyInterface;
  end;

// *********************************************************************//
// The Class CoVCDRangeProperty provides a Create and CreateRemote method to          
// create instances of the default interface IVCDRangeProperty exposed by              
// the CoClass VCDRangeProperty. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoVCDRangeProperty = class
    class function Create: IVCDRangeProperty;
    class function CreateRemote(const MachineName: string): IVCDRangeProperty;
  end;

// *********************************************************************//
// The Class CoVCDSwitchProperty provides a Create and CreateRemote method to          
// create instances of the default interface IVCDSwitchProperty exposed by              
// the CoClass VCDSwitchProperty. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoVCDSwitchProperty = class
    class function Create: IVCDSwitchProperty;
    class function CreateRemote(const MachineName: string): IVCDSwitchProperty;
  end;

// *********************************************************************//
// The Class CoVCDButtonProperty provides a Create and CreateRemote method to          
// create instances of the default interface IVCDButtonProperty exposed by              
// the CoClass VCDButtonProperty. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoVCDButtonProperty = class
    class function Create: IVCDButtonProperty;
    class function CreateRemote(const MachineName: string): IVCDButtonProperty;
  end;

// *********************************************************************//
// The Class CoVCDMapStringsProperty provides a Create and CreateRemote method to          
// create instances of the default interface IVCDMapStringsProperty exposed by              
// the CoClass VCDMapStringsProperty. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoVCDMapStringsProperty = class
    class function Create: IVCDMapStringsProperty;
    class function CreateRemote(const MachineName: string): IVCDMapStringsProperty;
  end;

// *********************************************************************//
// The Class CoVCDAbsoluteValueProperty provides a Create and CreateRemote method to          
// create instances of the default interface IVCDAbsoluteValueProperty exposed by              
// the CoClass VCDAbsoluteValueProperty. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoVCDAbsoluteValueProperty = class
    class function Create: IVCDAbsoluteValueProperty;
    class function CreateRemote(const MachineName: string): IVCDAbsoluteValueProperty;
  end;

// *********************************************************************//
// The Class CoFrameFilterInfo provides a Create and CreateRemote method to          
// create instances of the default interface IFrameFilterInfo exposed by              
// the CoClass FrameFilterInfo. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoFrameFilterInfo = class
    class function Create: IFrameFilterInfo;
    class function CreateRemote(const MachineName: string): IFrameFilterInfo;
  end;

// *********************************************************************//
// The Class CoFrameFilterInfos provides a Create and CreateRemote method to          
// create instances of the default interface IFrameFilterInfos exposed by              
// the CoClass FrameFilterInfos. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoFrameFilterInfos = class
    class function Create: IFrameFilterInfos;
    class function CreateRemote(const MachineName: string): IFrameFilterInfos;
  end;

// *********************************************************************//
// The Class CoFrameFilter provides a Create and CreateRemote method to          
// create instances of the default interface IFrameFilter exposed by              
// the CoClass FrameFilter. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoFrameFilter = class
    class function Create: IFrameFilter;
    class function CreateRemote(const MachineName: string): IFrameFilter;
  end;

// *********************************************************************//
// The Class CoFrameFilters provides a Create and CreateRemote method to          
// create instances of the default interface IFrameFilters exposed by              
// the CoClass FrameFilters. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoFrameFilters = class
    class function Create: IFrameFilters;
    class function CreateRemote(const MachineName: string): IFrameFilters;
  end;

// *********************************************************************//
// The Class CoBaseSink provides a Create and CreateRemote method to          
// create instances of the default interface IBaseSink exposed by              
// the CoClass BaseSink. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoBaseSink = class
    class function Create: IBaseSink;
    class function CreateRemote(const MachineName: string): IBaseSink;
  end;

// *********************************************************************//
// The Class CoMediaStreamContainer provides a Create and CreateRemote method to          
// create instances of the default interface IMediaStreamContainer exposed by              
// the CoClass MediaStreamContainer. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoMediaStreamContainer = class
    class function Create: IMediaStreamContainer;
    class function CreateRemote(const MachineName: string): IMediaStreamContainer;
  end;

// *********************************************************************//
// The Class CoMediaStreamContainers provides a Create and CreateRemote method to          
// create instances of the default interface IMediaStreamContainers exposed by              
// the CoClass MediaStreamContainers. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoMediaStreamContainers = class
    class function Create: IMediaStreamContainers;
    class function CreateRemote(const MachineName: string): IMediaStreamContainers;
  end;

// *********************************************************************//
// The Class CoMediaStreamSink provides a Create and CreateRemote method to          
// create instances of the default interface IMediaStreamSink exposed by              
// the CoClass MediaStreamSink. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoMediaStreamSink = class
    class function Create: IMediaStreamSink;
    class function CreateRemote(const MachineName: string): IMediaStreamSink;
  end;


// *********************************************************************//
// OLE Server Proxy class declaration
// Server Object    : TMediaStreamSink
// Help String      : Sink object used for writing to a media container like avi.
// Default Interface: IMediaStreamSink
// Def. Intf. DISP? : No
// Event   Interface: 
// TypeFlags        : (2) CanCreate
// *********************************************************************//
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  TMediaStreamSinkProperties= class;
{$ENDIF}
  TMediaStreamSink = class(TOleServer)
  private
    FIntf:        IMediaStreamSink;
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
    FProps:       TMediaStreamSinkProperties;
    function      GetServerProperties: TMediaStreamSinkProperties;
{$ENDIF}
    function      GetDefaultInterface: IMediaStreamSink;
  protected
    procedure InitServerData; override;
    function Get_SinkType: BaseSinkTypes;
    function Get_SinkModeRunning: WordBool;
    procedure Set_SinkModeRunning(IsRunning: WordBool);
    function Get_Filename: WideString;
    procedure Set_Filename(const SinkFilename: WideString);
    function Get_StreamContainer: IMediaStreamContainer;
    procedure Set_StreamContainer(const CurrentContainer: IMediaStreamContainer);
    function Get_Codec: IAviCompressor;
    procedure Set_Codec(const Type_: IAviCompressor);
    function Get_FrameFilters: IFrameFilters;
    function Get_FrameTimeCorrection: WordBool;
    procedure Set_FrameTimeCorrection(Enabled: WordBool);
  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    procedure Connect; override;
    procedure ConnectTo(svrIntf: IMediaStreamSink);
    procedure Disconnect; override;
    procedure Attach(var pAttachTarget: Pointer);
    property DefaultInterface: IMediaStreamSink read GetDefaultInterface;
    property SinkType: BaseSinkTypes read Get_SinkType;
    property FrameFilters: IFrameFilters read Get_FrameFilters;
    property SinkModeRunning: WordBool read Get_SinkModeRunning write Set_SinkModeRunning;
    property Filename: WideString read Get_Filename write Set_Filename;
    property StreamContainer: IMediaStreamContainer read Get_StreamContainer write Set_StreamContainer;
    property Codec: IAviCompressor read Get_Codec write Set_Codec;
    property FrameTimeCorrection: WordBool read Get_FrameTimeCorrection write Set_FrameTimeCorrection;
  published
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
    property Server: TMediaStreamSinkProperties read GetServerProperties;
{$ENDIF}
  end;

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
// *********************************************************************//
// OLE Server Properties Proxy Class
// Server Object    : TMediaStreamSink
// (This object is used by the IDE's Property Inspector to allow editing
//  of the properties of this server)
// *********************************************************************//
 TMediaStreamSinkProperties = class(TPersistent)
  private
    FServer:    TMediaStreamSink;
    function    GetDefaultInterface: IMediaStreamSink;
    constructor Create(AServer: TMediaStreamSink);
  protected
    function Get_SinkType: BaseSinkTypes;
    function Get_SinkModeRunning: WordBool;
    procedure Set_SinkModeRunning(IsRunning: WordBool);
    function Get_Filename: WideString;
    procedure Set_Filename(const SinkFilename: WideString);
    function Get_StreamContainer: IMediaStreamContainer;
    procedure Set_StreamContainer(const CurrentContainer: IMediaStreamContainer);
    function Get_Codec: IAviCompressor;
    procedure Set_Codec(const Type_: IAviCompressor);
    function Get_FrameFilters: IFrameFilters;
    function Get_FrameTimeCorrection: WordBool;
    procedure Set_FrameTimeCorrection(Enabled: WordBool);
  public
    property DefaultInterface: IMediaStreamSink read GetDefaultInterface;
  published
    property SinkModeRunning: WordBool read Get_SinkModeRunning write Set_SinkModeRunning;
    property Filename: WideString read Get_Filename write Set_Filename;
    property StreamContainer: IMediaStreamContainer read Get_StreamContainer write Set_StreamContainer;
    property Codec: IAviCompressor read Get_Codec write Set_Codec;
    property FrameTimeCorrection: WordBool read Get_FrameTimeCorrection write Set_FrameTimeCorrection;
  end;
{$ENDIF}


// *********************************************************************//
// The Class CoFrameType provides a Create and CreateRemote method to          
// create instances of the default interface IFrameType exposed by              
// the CoClass FrameType. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoFrameType = class
    class function Create: IFrameType;
    class function CreateRemote(const MachineName: string): IFrameType;
  end;


// *********************************************************************//
// OLE Server Proxy class declaration
// Server Object    : TFrameType
// Help String      : Object descibing a video format.
// Default Interface: IFrameType
// Def. Intf. DISP? : No
// Event   Interface: 
// TypeFlags        : (2) CanCreate
// *********************************************************************//
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  TFrameTypeProperties= class;
{$ENDIF}
  TFrameType = class(TOleServer)
  private
    FIntf:        IFrameType;
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
    FProps:       TFrameTypeProperties;
    function      GetServerProperties: TFrameTypeProperties;
{$ENDIF}
    function      GetDefaultInterface: IFrameType;
  protected
    procedure InitServerData; override;
    function Get_Name: WideString;
    procedure Set_Name(const NameStr: WideString);
    function Get_type_: WideString;
    procedure Set_type_(const TypeStr: WideString);
    function Get_Width: Integer;
    procedure Set_Width(CurWidth: Integer);
    function Get_Height: Integer;
    procedure Set_Height(CurHeight: Integer);
    function Get_Buffersize: Integer;
    procedure Set_Buffersize(CurBuffersize: Integer);
    function Get_BitsPerPixel: Integer;
    function Get_IsBottomUp: WordBool;
    function Get_SubtypeGUID: WideString;
    procedure Set_SubtypeGUID(const guid: WideString);
  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    procedure Connect; override;
    procedure ConnectTo(svrIntf: IFrameType);
    procedure Disconnect; override;
    property DefaultInterface: IFrameType read GetDefaultInterface;
    property BitsPerPixel: Integer read Get_BitsPerPixel;
    property IsBottomUp: WordBool read Get_IsBottomUp;
    property Name: WideString read Get_Name write Set_Name;
    property type_: WideString read Get_type_ write Set_type_;
    property Width: Integer read Get_Width write Set_Width;
    property Height: Integer read Get_Height write Set_Height;
    property Buffersize: Integer read Get_Buffersize write Set_Buffersize;
    property SubtypeGUID: WideString read Get_SubtypeGUID write Set_SubtypeGUID;
  published
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
    property Server: TFrameTypeProperties read GetServerProperties;
{$ENDIF}
  end;

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
// *********************************************************************//
// OLE Server Properties Proxy Class
// Server Object    : TFrameType
// (This object is used by the IDE's Property Inspector to allow editing
//  of the properties of this server)
// *********************************************************************//
 TFrameTypeProperties = class(TPersistent)
  private
    FServer:    TFrameType;
    function    GetDefaultInterface: IFrameType;
    constructor Create(AServer: TFrameType);
  protected
    function Get_Name: WideString;
    procedure Set_Name(const NameStr: WideString);
    function Get_type_: WideString;
    procedure Set_type_(const TypeStr: WideString);
    function Get_Width: Integer;
    procedure Set_Width(CurWidth: Integer);
    function Get_Height: Integer;
    procedure Set_Height(CurHeight: Integer);
    function Get_Buffersize: Integer;
    procedure Set_Buffersize(CurBuffersize: Integer);
    function Get_BitsPerPixel: Integer;
    function Get_IsBottomUp: WordBool;
    function Get_SubtypeGUID: WideString;
    procedure Set_SubtypeGUID(const guid: WideString);
  public
    property DefaultInterface: IFrameType read GetDefaultInterface;
  published
    property Name: WideString read Get_Name write Set_Name;
    property type_: WideString read Get_type_ write Set_type_;
    property Width: Integer read Get_Width write Set_Width;
    property Height: Integer read Get_Height write Set_Height;
    property Buffersize: Integer read Get_Buffersize write Set_Buffersize;
    property SubtypeGUID: WideString read Get_SubtypeGUID write Set_SubtypeGUID;
  end;
{$ENDIF}


// *********************************************************************//
// The Class CoFrameTypes provides a Create and CreateRemote method to          
// create instances of the default interface IFrameTypes exposed by              
// the CoClass FrameTypes. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoFrameTypes = class
    class function Create: IFrameTypes;
    class function CreateRemote(const MachineName: string): IFrameTypes;
  end;


// *********************************************************************//
// OLE Server Proxy class declaration
// Server Object    : TFrameTypes
// Help String      : Collection of FrameTypes
// Default Interface: IFrameTypes
// Def. Intf. DISP? : No
// Event   Interface: 
// TypeFlags        : (2) CanCreate
// *********************************************************************//
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  TFrameTypesProperties= class;
{$ENDIF}
  TFrameTypes = class(TOleServer)
  private
    FIntf:        IFrameTypes;
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
    FProps:       TFrameTypesProperties;
    function      GetServerProperties: TFrameTypesProperties;
{$ENDIF}
    function      GetDefaultInterface: IFrameTypes;
  protected
    procedure InitServerData; override;
    function Get__NewEnum: IUnknown;
    function Get_Item(n: Integer): IFrameType;
    function Get_Count: Integer;
    procedure Set_ReadOnly(Param1: WordBool);
  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    procedure Connect; override;
    procedure ConnectTo(svrIntf: IFrameTypes);
    procedure Disconnect; override;
    procedure Add(const NewType: IFrameType);
    procedure Remove(Index: Integer);
    procedure Clear;
    property DefaultInterface: IFrameTypes read GetDefaultInterface;
    property _NewEnum: IUnknown read Get__NewEnum;
    property Item[n: Integer]: IFrameType read Get_Item; default;
    property Count: Integer read Get_Count;
    property ReadOnly: WordBool write Set_ReadOnly;
  published
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
    property Server: TFrameTypesProperties read GetServerProperties;
{$ENDIF}
  end;

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
// *********************************************************************//
// OLE Server Properties Proxy Class
// Server Object    : TFrameTypes
// (This object is used by the IDE's Property Inspector to allow editing
//  of the properties of this server)
// *********************************************************************//
 TFrameTypesProperties = class(TPersistent)
  private
    FServer:    TFrameTypes;
    function    GetDefaultInterface: IFrameTypes;
    constructor Create(AServer: TFrameTypes);
  protected
    function Get__NewEnum: IUnknown;
    function Get_Item(n: Integer): IFrameType;
    function Get_Count: Integer;
    procedure Set_ReadOnly(Param1: WordBool);
  public
    property DefaultInterface: IFrameTypes read GetDefaultInterface;
  published
  end;
{$ENDIF}


// *********************************************************************//
// The Class CoFrameHandlerSink provides a Create and CreateRemote method to          
// create instances of the default interface IFrameHandlerSink exposed by              
// the CoClass FrameHandlerSink. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoFrameHandlerSink = class
    class function Create: IFrameHandlerSink;
    class function CreateRemote(const MachineName: string): IFrameHandlerSink;
  end;


// *********************************************************************//
// OLE Server Proxy class declaration
// Server Object    : TFrameHandlerSink
// Help String      : Sink object used for writing the video frames into ImageBuffer objects.
// Default Interface: IFrameHandlerSink
// Def. Intf. DISP? : No
// Event   Interface: 
// TypeFlags        : (2) CanCreate
// *********************************************************************//
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  TFrameHandlerSinkProperties= class;
{$ENDIF}
  TFrameHandlerSink = class(TOleServer)
  private
    FIntf:        IFrameHandlerSink;
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
    FProps:       TFrameHandlerSinkProperties;
    function      GetServerProperties: TFrameHandlerSinkProperties;
{$ENDIF}
    function      GetDefaultInterface: IFrameHandlerSink;
  protected
    procedure InitServerData; override;
    function Get_SinkType: BaseSinkTypes;
    function Get_SinkModeRunning: WordBool;
    procedure Set_SinkModeRunning(IsRunning: WordBool);
    function Get_SnapMode: WordBool;
    procedure Set_SnapMode(SnapModeEnabled: WordBool);
    function Get_ImageBuffers: IImageBuffers;
    function Get_LastAcquiredBuffer: IImageBuffer;
    function Get_BufferCount: Integer;
    procedure Set_BufferCount(CurrentBufferCount: Integer);
    function Get_FrameTypes: IFrameTypes;
    function Get_FrameFilters: IFrameFilters;
    function Get_FrameCount: Integer;
  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    procedure Connect; override;
    procedure ConnectTo(svrIntf: IFrameHandlerSink);
    procedure Disconnect; override;
    procedure Attach(var pAttachTarget: Pointer);
    procedure SnapImage(timeout: Integer);
    procedure SnapImageSequence(Count: Integer; timeout: Integer);
    procedure SnapImageSequenceAsync(Count: Integer);
    property DefaultInterface: IFrameHandlerSink read GetDefaultInterface;
    property SinkType: BaseSinkTypes read Get_SinkType;
    property ImageBuffers: IImageBuffers read Get_ImageBuffers;
    property LastAcquiredBuffer: IImageBuffer read Get_LastAcquiredBuffer;
    property FrameTypes: IFrameTypes read Get_FrameTypes;
    property FrameFilters: IFrameFilters read Get_FrameFilters;
    property FrameCount: Integer read Get_FrameCount;
    property SinkModeRunning: WordBool read Get_SinkModeRunning write Set_SinkModeRunning;
    property SnapMode: WordBool read Get_SnapMode write Set_SnapMode;
    property BufferCount: Integer read Get_BufferCount write Set_BufferCount;
  published
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
    property Server: TFrameHandlerSinkProperties read GetServerProperties;
{$ENDIF}
  end;

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
// *********************************************************************//
// OLE Server Properties Proxy Class
// Server Object    : TFrameHandlerSink
// (This object is used by the IDE's Property Inspector to allow editing
//  of the properties of this server)
// *********************************************************************//
 TFrameHandlerSinkProperties = class(TPersistent)
  private
    FServer:    TFrameHandlerSink;
    function    GetDefaultInterface: IFrameHandlerSink;
    constructor Create(AServer: TFrameHandlerSink);
  protected
    function Get_SinkType: BaseSinkTypes;
    function Get_SinkModeRunning: WordBool;
    procedure Set_SinkModeRunning(IsRunning: WordBool);
    function Get_SnapMode: WordBool;
    procedure Set_SnapMode(SnapModeEnabled: WordBool);
    function Get_ImageBuffers: IImageBuffers;
    function Get_LastAcquiredBuffer: IImageBuffer;
    function Get_BufferCount: Integer;
    procedure Set_BufferCount(CurrentBufferCount: Integer);
    function Get_FrameTypes: IFrameTypes;
    function Get_FrameFilters: IFrameFilters;
    function Get_FrameCount: Integer;
  public
    property DefaultInterface: IFrameHandlerSink read GetDefaultInterface;
  published
    property SinkModeRunning: WordBool read Get_SinkModeRunning write Set_SinkModeRunning;
    property SnapMode: WordBool read Get_SnapMode write Set_SnapMode;
    property BufferCount: Integer read Get_BufferCount write Set_BufferCount;
  end;
{$ENDIF}



// *********************************************************************//
// OLE Control Proxy class declaration
// Control Name     : TICImagingControl
// Help String      : ICImagingControl Class
// Default Interface: IICImagingControl
// Def. Intf. DISP? : No
// Event   Interface: _IICImagingControlEvents
// TypeFlags        : (2) CanCreate
// *********************************************************************//
  TICImagingControlMouseDown = procedure(ASender: TObject; Button: Integer; Shift: Integer; 
                                                           XPos: Smallint; YPos: Smallint) of object;
  TICImagingControlMouseUp = procedure(ASender: TObject; Button: Integer; Shift: Integer; 
                                                         XPos: Smallint; YPos: Smallint) of object;
  TICImagingControlMouseMove = procedure(ASender: TObject; Button: Integer; Shift: Integer; 
                                                           XPos: Smallint; YPos: Smallint) of object;
  TICImagingControlImageAvailable = procedure(ASender: TObject; BufferIndex: Integer) of object;
  TICImagingControlOnHScroll = procedure(ASender: TObject; XPos: Integer; YPos: Integer) of object;
  TICImagingControlOnVScroll = procedure(ASender: TObject; XPos: Integer; YPos: Integer) of object;
  TICImagingControlOverlayUpdate = procedure(ASender: TObject; const Overlay: OverlayBitmap; 
                                                               SampleStartTime: Double; 
                                                               SampleEndTime: Double) of object;

  TICImagingControl = class(TOleControl)
  private
    FOnMouseDown: TICImagingControlMouseDown;
    FOnMouseUp: TICImagingControlMouseUp;
    FOnMouseMove: TICImagingControlMouseMove;
    FOnImageAvailable: TICImagingControlImageAvailable;
    FOnDeviceLost: TNotifyEvent;
    FOnHScroll: TICImagingControlOnHScroll;
    FOnVScroll: TICImagingControlOnVScroll;
    FOnOverlayUpdate: TICImagingControlOverlayUpdate;
    FIntf: IICImagingControl;
    function  GetControlInterface: IICImagingControl;
  protected
    procedure CreateControl;
    procedure InitControlData; override;
    function Get_BrightnessRange: OleVariant;
    function Get_ContrastRange: OleVariant;
    function Get_HueRange: OleVariant;
    function Get_SaturationRange: OleVariant;
    function Get_SharpnessRange: OleVariant;
    function Get_GammaRange: OleVariant;
    function Get_ColorEnableRange: OleVariant;
    function Get_WhiteBalanceRange: OleVariant;
    function Get_BacklightCompensationRange: OleVariant;
    function Get_GainRange: OleVariant;
    function Get_PanRange: OleVariant;
    function Get_TiltRange: OleVariant;
    function Get_RollRange: OleVariant;
    function Get_ZoomRange: OleVariant;
    function Get_ExposureRange: OleVariant;
    function Get_IrisRange: OleVariant;
    function Get_FocusRange: OleVariant;
    function Get_Picture: IUnknown;
    function Get_DeviceFrameRates: IDeviceFrameRates;
    function Get_ImageBuffers: IImageBuffers;
    function Get_VideoFormats: IVideoFormats;
    function Get_Devices: IDevices;
    function Get_VideoNorms: IVideoNorms;
    function Get_InputChannels: IInputChannels;
    function Get_AviCompressors: IAviCompressors;
    function Get_OverlayBitmap: IOverlayBitmap;
    function Get_VCDPropertyItems: IVCDPropertyItems;
    function Get_Sink: IBaseSink;
    procedure Set_Sink(const CurrentSink: IBaseSink);
    function Get_MediaStreamContainers: IMediaStreamContainers;
    function Get_FrameFilterInfos: IFrameFilterInfos;
    function Get_DeviceFrameFilters: IFrameFilters;
    function Get_ImageActiveBuffer: IImageBuffer;
    function Get_DisplayFrameFilters: IFrameFilters;
    function Get_OverlayBitmapAtPath(OverlayToFetch: PathPositions): IOverlayBitmap;
  public
    procedure Refresh;
    procedure LiveStart;
    procedure LiveStop;
    procedure MemorySaveImage(const Filename: WideString);
    procedure MemorySaveImageSequence(SequenceLength: Integer; const Filename: WideString);
    procedure MemorySnapImage;
    procedure MemorySnapImageSequence(ImageCount: Integer);
    function MemoryGetImageData: OleVariant;
    procedure MemoryReleaseImageData(var Buffer: OleVariant);
    function MemoryGetDib: Integer;
    procedure AviStartCapture(const Filename: WideString; const Compressor: WideString);
    procedure AviStopCapture;
    procedure Display;
    procedure DisplayImageBuffer(const ImageBufferToDisplay: IImageBuffer);
    function GetGrabberPtr: Integer;
    function SaveDeviceState: WideString;
    procedure LoadDeviceState(const String_: WideString; OpenDev: WordBool);
    procedure SaveDeviceStateToFile(const Filename: WideString);
    procedure LoadDeviceStateFromFile(const Filename: WideString; OpenDev: WordBool);
    procedure ShowPropertyDialog;
    procedure ShowDeviceSettingsDialog;
    function FrameFilterCreate(const Info: IFrameFilterInfo): IFrameFilter;
    function FrameFilterCreateString(const FilterName: WideString; const ModuleName: WideString): IFrameFilter;
    procedure _SetDirty(Val: Integer);
    function FrameFilterCreate_(FilterPtr: Integer): IFrameFilter;
    procedure MemorySaveImageJpeg(const Filename: WideString; Quality: Integer);
    property  ControlInterface: IICImagingControl read GetControlInterface;
    property  DefaultInterface: IICImagingControl read GetControlInterface;
    property LiveVideoRunning: WordBool index 5 read GetWordBoolProp;
    property ImageWidth: Integer index 12 read GetIntegerProp;
    property ImageHeight: Integer index 13 read GetIntegerProp;
    property VideoNormAvailable: WordBool index 14 read GetWordBoolProp;
    property InputChannelAvailable: WordBool index 17 read GetWordBoolProp;
    property BrightnessAvailable: WordBool index 35 read GetWordBoolProp;
    property BrightnessAutoAvailable: WordBool index 36 read GetWordBoolProp;
    property BrightnessRange: OleVariant index 34 read GetOleVariantProp;
    property ContrastAvailable: WordBool index 40 read GetWordBoolProp;
    property ContrastAutoAvailable: WordBool index 41 read GetWordBoolProp;
    property ContrastRange: OleVariant index 39 read GetOleVariantProp;
    property HueAvailable: WordBool index 45 read GetWordBoolProp;
    property HueAutoAvailable: WordBool index 46 read GetWordBoolProp;
    property HueRange: OleVariant index 44 read GetOleVariantProp;
    property SaturationAvailable: WordBool index 50 read GetWordBoolProp;
    property SaturationAutoAvailable: WordBool index 51 read GetWordBoolProp;
    property SaturationRange: OleVariant index 49 read GetOleVariantProp;
    property SharpnessAvailable: WordBool index 55 read GetWordBoolProp;
    property SharpnessAutoAvailable: WordBool index 56 read GetWordBoolProp;
    property SharpnessRange: OleVariant index 54 read GetOleVariantProp;
    property GammaAvailable: WordBool index 60 read GetWordBoolProp;
    property GammaAutoAvailable: WordBool index 61 read GetWordBoolProp;
    property GammaRange: OleVariant index 59 read GetOleVariantProp;
    property ColorEnableAvailable: WordBool index 65 read GetWordBoolProp;
    property ColorEnableAutoAvailable: WordBool index 66 read GetWordBoolProp;
    property ColorEnableRange: OleVariant index 64 read GetOleVariantProp;
    property WhiteBalanceAvailable: WordBool index 70 read GetWordBoolProp;
    property WhiteBalanceAutoAvailable: WordBool index 71 read GetWordBoolProp;
    property WhiteBalanceRange: OleVariant index 69 read GetOleVariantProp;
    property BacklightCompensationAvailable: WordBool index 75 read GetWordBoolProp;
    property BacklightCompensationAutoAvailable: WordBool index 76 read GetWordBoolProp;
    property BacklightCompensationRange: OleVariant index 74 read GetOleVariantProp;
    property GainAvailable: WordBool index 80 read GetWordBoolProp;
    property GainAutoAvailable: WordBool index 81 read GetWordBoolProp;
    property GainRange: OleVariant index 79 read GetOleVariantProp;
    property PanAvailable: WordBool index 85 read GetWordBoolProp;
    property PanAutoAvailable: WordBool index 86 read GetWordBoolProp;
    property PanRange: OleVariant index 84 read GetOleVariantProp;
    property TiltAvailable: WordBool index 90 read GetWordBoolProp;
    property TiltAutoAvailable: WordBool index 91 read GetWordBoolProp;
    property TiltRange: OleVariant index 89 read GetOleVariantProp;
    property RollAvailable: WordBool index 95 read GetWordBoolProp;
    property RollAutoAvailable: WordBool index 96 read GetWordBoolProp;
    property RollRange: OleVariant index 94 read GetOleVariantProp;
    property ZoomAvailable: WordBool index 100 read GetWordBoolProp;
    property ZoomAutoAvailable: WordBool index 101 read GetWordBoolProp;
    property ZoomRange: OleVariant index 99 read GetOleVariantProp;
    property ExposureAvailable: WordBool index 105 read GetWordBoolProp;
    property ExposureAutoAvailable: WordBool index 106 read GetWordBoolProp;
    property ExposureRange: OleVariant index 104 read GetOleVariantProp;
    property IrisAvailable: WordBool index 110 read GetWordBoolProp;
    property IrisAutoAvailable: WordBool index 111 read GetWordBoolProp;
    property IrisRange: OleVariant index 109 read GetOleVariantProp;
    property FocusAvailable: WordBool index 115 read GetWordBoolProp;
    property FocusAutoAvailable: WordBool index 116 read GetWordBoolProp;
    property FocusRange: OleVariant index 114 read GetOleVariantProp;
    property ExternalTransportAvailable: WordBool index 117 read GetWordBoolProp;
    property Picture: IUnknown index 121 read GetIUnknownProp;
    property DeviceFrameRateAvailable: WordBool index 126 read GetWordBoolProp;
    property DeviceFrameRates: IDeviceFrameRates read Get_DeviceFrameRates;
    property DeviceCurrentActualFrameRate: Single index 128 read GetSingleProp;
    property ImageBuffers: IImageBuffers read Get_ImageBuffers;
    property ImageBitsPerPixel: Integer index 141 read GetIntegerProp;
    property DeviceTriggerAvailable: WordBool index 132 read GetWordBoolProp;
    property VideoFormats: IVideoFormats read Get_VideoFormats;
    property Devices: IDevices read Get_Devices;
    property VideoNorms: IVideoNorms read Get_VideoNorms;
    property InputChannels: IInputChannels read Get_InputChannels;
    property AviCompressors: IAviCompressors read Get_AviCompressors;
    property DeviceValid: WordBool index 142 read GetWordBoolProp;
    property DeviceFlipVerticalAvailable: WordBool index 149 read GetWordBoolProp;
    property DeviceFlipHorizontalAvailable: WordBool index 151 read GetWordBoolProp;
    property OverlayBitmap: IOverlayBitmap read Get_OverlayBitmap;
    property ReferenceTimeCurrent: Double index 157 read GetDoubleProp;
    property ReferenceTimeStart: Double index 158 read GetDoubleProp;
    property BrightnessDefault: Integer index 160 read GetIntegerProp;
    property ContrastDefault: Integer index 161 read GetIntegerProp;
    property HueDefault: Integer index 162 read GetIntegerProp;
    property SaturationDefault: Integer index 163 read GetIntegerProp;
    property SharpnessDefault: Integer index 164 read GetIntegerProp;
    property GammaDefault: Integer index 165 read GetIntegerProp;
    property ColorEnableDefault: Integer index 166 read GetIntegerProp;
    property WhiteBalanceDefault: Integer index 167 read GetIntegerProp;
    property BacklightCompensationDefault: Integer index 168 read GetIntegerProp;
    property GainDefault: Integer index 169 read GetIntegerProp;
    property PanDefault: Integer index 170 read GetIntegerProp;
    property TiltDefault: Integer index 171 read GetIntegerProp;
    property RollDefault: Integer index 172 read GetIntegerProp;
    property ZoomDefault: Integer index 173 read GetIntegerProp;
    property ExposureDefault: Integer index 174 read GetIntegerProp;
    property IrisDefault: Integer index 175 read GetIntegerProp;
    property FocusDefault: Integer index 176 read GetIntegerProp;
    property VCDPropertyItems: IVCDPropertyItems read Get_VCDPropertyItems;
    property SignalDetectedAvailable: WordBool index 178 read GetWordBoolProp;
    property SignalDetected: WordBool index 179 read GetWordBoolProp;
    property DeviceCountOfFramesDropped: Integer index 181 read GetIntegerProp;
    property DeviceCountOfFramesNotDropped: Integer index 182 read GetIntegerProp;
    property MediaStreamContainers: IMediaStreamContainers read Get_MediaStreamContainers;
    property FrameFilterPath: WideString index 193 write SetWideStringProp;
    property FrameFilterInfos: IFrameFilterInfos read Get_FrameFilterInfos;
    property DeviceFrameFilters: IFrameFilters read Get_DeviceFrameFilters;
    property LiveDisplayOutputWidth: Integer index 198 read GetIntegerProp;
    property LiveDisplayOutputHeight: Integer index 199 read GetIntegerProp;
    property ImageActiveBuffer: IImageBuffer read Get_ImageActiveBuffer;
    property DeBayerActive: WordBool index 201 read GetWordBoolProp;
    property DisplayFrameFilters: IFrameFilters read Get_DisplayFrameFilters;
    property OverlayBitmapAtPath[OverlayToFetch: PathPositions]: IOverlayBitmap read Get_OverlayBitmapAtPath;
  published
    property Anchors;
    property  TabStop;
    property  Align;
    property  DragCursor;
    property  DragMode;
    property  ParentShowHint;
    property  PopupMenu;
    property  ShowHint;
    property  TabOrder;
    property  Visible;
    property  OnDragDrop;
    property  OnDragOver;
    property  OnEndDrag;
    property  OnEnter;
    property  OnExit;
    property  OnStartDrag;
    property Device: WideString index 4 read GetWideStringProp write SetWideStringProp stored False;
    property VideoFormat: WideString index 11 read GetWideStringProp write SetWideStringProp stored False;
    property VideoNorm: WideString index 16 read GetWideStringProp write SetWideStringProp stored False;
    property InputChannel: WideString index 19 read GetWideStringProp write SetWideStringProp stored False;
    property MemoryCurrentGrabberColorformat: TOleEnum index 21 read GetTOleEnumProp write SetTOleEnumProp stored False;
    property Brightness: Integer index 32 read GetIntegerProp write SetIntegerProp stored False;
    property BrightnessAuto: WordBool index 33 read GetWordBoolProp write SetWordBoolProp stored False;
    property Contrast: Integer index 37 read GetIntegerProp write SetIntegerProp stored False;
    property ContrastAuto: WordBool index 38 read GetWordBoolProp write SetWordBoolProp stored False;
    property Hue: Integer index 42 read GetIntegerProp write SetIntegerProp stored False;
    property HueAuto: WordBool index 43 read GetWordBoolProp write SetWordBoolProp stored False;
    property Saturation: Integer index 47 read GetIntegerProp write SetIntegerProp stored False;
    property SaturationAuto: WordBool index 48 read GetWordBoolProp write SetWordBoolProp stored False;
    property Sharpness: Integer index 52 read GetIntegerProp write SetIntegerProp stored False;
    property SharpnessAuto: WordBool index 53 read GetWordBoolProp write SetWordBoolProp stored False;
    property Gamma: Integer index 57 read GetIntegerProp write SetIntegerProp stored False;
    property GammaAuto: WordBool index 58 read GetWordBoolProp write SetWordBoolProp stored False;
    property ColorEnable: Integer index 62 read GetIntegerProp write SetIntegerProp stored False;
    property ColorEnableAuto: WordBool index 63 read GetWordBoolProp write SetWordBoolProp stored False;
    property WhiteBalance: Integer index 67 read GetIntegerProp write SetIntegerProp stored False;
    property WhiteBalanceAuto: WordBool index 68 read GetWordBoolProp write SetWordBoolProp stored False;
    property BacklightCompensation: Integer index 72 read GetIntegerProp write SetIntegerProp stored False;
    property BacklightCompensationAuto: WordBool index 73 read GetWordBoolProp write SetWordBoolProp stored False;
    property Gain: Integer index 77 read GetIntegerProp write SetIntegerProp stored False;
    property GainAuto: WordBool index 78 read GetWordBoolProp write SetWordBoolProp stored False;
    property Pan: Integer index 82 read GetIntegerProp write SetIntegerProp stored False;
    property PanAuto: WordBool index 83 read GetWordBoolProp write SetWordBoolProp stored False;
    property Tilt: Integer index 87 read GetIntegerProp write SetIntegerProp stored False;
    property TiltAuto: WordBool index 88 read GetWordBoolProp write SetWordBoolProp stored False;
    property Roll: Integer index 92 read GetIntegerProp write SetIntegerProp stored False;
    property RollAuto: WordBool index 93 read GetWordBoolProp write SetWordBoolProp stored False;
    property Zoom: Integer index 97 read GetIntegerProp write SetIntegerProp stored False;
    property ZoomAuto: WordBool index 98 read GetWordBoolProp write SetWordBoolProp stored False;
    property Exposure: Integer index 102 read GetIntegerProp write SetIntegerProp stored False;
    property ExposureAuto: WordBool index 103 read GetWordBoolProp write SetWordBoolProp stored False;
    property Iris: Integer index 107 read GetIntegerProp write SetIntegerProp stored False;
    property IrisAuto: WordBool index 108 read GetWordBoolProp write SetWordBoolProp stored False;
    property Focus: Integer index 112 read GetIntegerProp write SetIntegerProp stored False;
    property FocusAuto: WordBool index 113 read GetWordBoolProp write SetWordBoolProp stored False;
    property MemorySnapTimeout: Integer index 120 read GetIntegerProp write SetIntegerProp stored False;
    property ExternalTransportMode: TOleEnum index 118 read GetTOleEnumProp write SetTOleEnumProp stored False;
    property LiveDisplay: WordBool index 122 read GetWordBoolProp write SetWordBoolProp stored False;
    property LiveCaptureLastImage: WordBool index 123 read GetWordBoolProp write SetWordBoolProp stored False;
    property LiveCaptureContinuous: WordBool index 124 read GetWordBoolProp write SetWordBoolProp stored False;
    property DeviceFrameRate: Single index 125 read GetSingleProp write SetSingleProp stored False;
    property ImageRingBufferSize: Integer index 131 read GetIntegerProp write SetIntegerProp stored False;
    property DeviceTrigger: WordBool index 133 read GetWordBoolProp write SetWordBoolProp stored False;
    property LiveCapturePause: WordBool index 143 read GetWordBoolProp write SetWordBoolProp stored False;
    property LiveDisplayWidth: Integer index 144 read GetIntegerProp write SetIntegerProp stored False;
    property LiveDisplayHeight: Integer index 145 read GetIntegerProp write SetIntegerProp stored False;
    property LiveDisplayLeft: Integer index 146 read GetIntegerProp write SetIntegerProp stored False;
    property LiveDisplayTop: Integer index 147 read GetIntegerProp write SetIntegerProp stored False;
    property LiveDisplayDefault: WordBool index 148 read GetWordBoolProp write SetWordBoolProp stored False;
    property DeviceFlipVertical: WordBool index 150 read GetWordBoolProp write SetWordBoolProp stored False;
    property DeviceFlipHorizontal: WordBool index 152 read GetWordBoolProp write SetWordBoolProp stored False;
    property OverlayUpdateEventEnable: WordBool index 154 read GetWordBoolProp write SetWordBoolProp stored False;
    property LiveDisplayZoomFactor: Single index 155 read GetSingleProp write SetSingleProp stored False;
    property ScrollbarsEnabled: WordBool index 156 read GetWordBoolProp write SetWordBoolProp stored False;
    property LivePause: WordBool index 180 read GetWordBoolProp write SetWordBoolProp stored False;
    property DeviceUniqueName: WideString index 188 read GetWideStringProp write SetWideStringProp stored False;
    property Sink: IBaseSink read Get_Sink write Set_Sink stored False;
    property DeBayerStartPattern: TOleEnum index 202 read GetTOleEnumProp write SetTOleEnumProp stored False;
    property DeBayerMode: TOleEnum index 203 read GetTOleEnumProp write SetTOleEnumProp stored False;
    property SinkCompatibilityMode: WordBool index 204 read GetWordBoolProp write SetWordBoolProp stored False;
    property OverlayBitmapPosition: TOleEnum index 206 read GetTOleEnumProp write SetTOleEnumProp stored False;
    property LiveShowLastBuffer: WordBool index 210 read GetWordBoolProp write SetWordBoolProp stored False;
    property OnMouseDown: TICImagingControlMouseDown read FOnMouseDown write FOnMouseDown;
    property OnMouseUp: TICImagingControlMouseUp read FOnMouseUp write FOnMouseUp;
    property OnMouseMove: TICImagingControlMouseMove read FOnMouseMove write FOnMouseMove;
    property OnImageAvailable: TICImagingControlImageAvailable read FOnImageAvailable write FOnImageAvailable;
    property OnDeviceLost: TNotifyEvent read FOnDeviceLost write FOnDeviceLost;
    property OnHScroll: TICImagingControlOnHScroll read FOnHScroll write FOnHScroll;
    property OnVScroll: TICImagingControlOnVScroll read FOnVScroll write FOnVScroll;
    property OnOverlayUpdate: TICImagingControlOverlayUpdate read FOnOverlayUpdate write FOnOverlayUpdate;
  end;

procedure Register;

resourcestring
  dtlServerPage = 'ActiveX';

  dtlOcxPage = 'ActiveX';

implementation

uses ComObj;

class function CoOverlayBitmap.Create: IOverlayBitmap;
begin
  Result := CreateComObject(CLASS_OverlayBitmap) as IOverlayBitmap;
end;

class function CoOverlayBitmap.CreateRemote(const MachineName: string): IOverlayBitmap;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_OverlayBitmap) as IOverlayBitmap;
end;

class function CoImageBuffer.Create: IImageBuffer;
begin
  Result := CreateComObject(CLASS_ImageBuffer) as IImageBuffer;
end;

class function CoImageBuffer.CreateRemote(const MachineName: string): IImageBuffer;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_ImageBuffer) as IImageBuffer;
end;

class function CoImageBuffers.Create: IImageBuffers;
begin
  Result := CreateComObject(CLASS_ImageBuffers) as IImageBuffers;
end;

class function CoImageBuffers.CreateRemote(const MachineName: string): IImageBuffers;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_ImageBuffers) as IImageBuffers;
end;

class function CoVideoFormat.Create: IVideoFormat;
begin
  Result := CreateComObject(CLASS_VideoFormat) as IVideoFormat;
end;

class function CoVideoFormat.CreateRemote(const MachineName: string): IVideoFormat;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_VideoFormat) as IVideoFormat;
end;

class function CoVideoFormats.Create: IVideoFormats;
begin
  Result := CreateComObject(CLASS_VideoFormats) as IVideoFormats;
end;

class function CoVideoFormats.CreateRemote(const MachineName: string): IVideoFormats;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_VideoFormats) as IVideoFormats;
end;

class function CoDevice.Create: IDevice;
begin
  Result := CreateComObject(CLASS_Device) as IDevice;
end;

class function CoDevice.CreateRemote(const MachineName: string): IDevice;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_Device) as IDevice;
end;

class function CoDevices.Create: IDevices;
begin
  Result := CreateComObject(CLASS_Devices) as IDevices;
end;

class function CoDevices.CreateRemote(const MachineName: string): IDevices;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_Devices) as IDevices;
end;

class function CoVideoNorm.Create: IVideoNorm;
begin
  Result := CreateComObject(CLASS_VideoNorm) as IVideoNorm;
end;

class function CoVideoNorm.CreateRemote(const MachineName: string): IVideoNorm;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_VideoNorm) as IVideoNorm;
end;

class function CoVideoNorms.Create: IVideoNorms;
begin
  Result := CreateComObject(CLASS_VideoNorms) as IVideoNorms;
end;

class function CoVideoNorms.CreateRemote(const MachineName: string): IVideoNorms;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_VideoNorms) as IVideoNorms;
end;

class function CoInputChannel.Create: IInputChannel;
begin
  Result := CreateComObject(CLASS_InputChannel) as IInputChannel;
end;

class function CoInputChannel.CreateRemote(const MachineName: string): IInputChannel;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_InputChannel) as IInputChannel;
end;

class function CoInputChannels.Create: IInputChannels;
begin
  Result := CreateComObject(CLASS_InputChannels) as IInputChannels;
end;

class function CoInputChannels.CreateRemote(const MachineName: string): IInputChannels;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_InputChannels) as IInputChannels;
end;

class function CoDeviceFrameRates.Create: IDeviceFrameRates;
begin
  Result := CreateComObject(CLASS_DeviceFrameRates) as IDeviceFrameRates;
end;

class function CoDeviceFrameRates.CreateRemote(const MachineName: string): IDeviceFrameRates;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_DeviceFrameRates) as IDeviceFrameRates;
end;

class function CoAviCompressor.Create: IAviCompressor;
begin
  Result := CreateComObject(CLASS_AviCompressor) as IAviCompressor;
end;

class function CoAviCompressor.CreateRemote(const MachineName: string): IAviCompressor;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_AviCompressor) as IAviCompressor;
end;

class function CoAviCompressors.Create: IAviCompressors;
begin
  Result := CreateComObject(CLASS_AviCompressors) as IAviCompressors;
end;

class function CoAviCompressors.CreateRemote(const MachineName: string): IAviCompressors;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_AviCompressors) as IAviCompressors;
end;

class function CoCICPropertyPage.Create: IUnknown;
begin
  Result := CreateComObject(CLASS_CICPropertyPage) as IUnknown;
end;

class function CoCICPropertyPage.CreateRemote(const MachineName: string): IUnknown;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CICPropertyPage) as IUnknown;
end;

procedure TCICPropertyPage.InitServerData;
const
  CServerData: TServerData = (
    ClassID:   '{E0110BE7-EBF0-4612-B2F8-817194557E16}';
    IntfIID:   '{00000000-0000-0000-C000-000000000046}';
    EventIID:  '';
    LicenseKey: nil;
    Version: 500);
begin
  ServerData := @CServerData;
end;

procedure TCICPropertyPage.Connect;
var
  punk: IUnknown;
begin
  if FIntf = nil then
  begin
    punk := GetServer;
    Fintf:= punk as IUnknown;
  end;
end;

procedure TCICPropertyPage.ConnectTo(svrIntf: IUnknown);
begin
  Disconnect;
  FIntf := svrIntf;
end;

procedure TCICPropertyPage.DisConnect;
begin
  if Fintf <> nil then
  begin
    FIntf := nil;
  end;
end;

function TCICPropertyPage.GetDefaultInterface: IUnknown;
begin
  if FIntf = nil then
    Connect;
  Assert(FIntf <> nil, 'DefaultInterface is NULL. Component is not connected to Server. You must call ''Connect'' or ''ConnectTo'' before this operation');
  Result := FIntf;
end;

constructor TCICPropertyPage.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  FProps := TCICPropertyPageProperties.Create(Self);
{$ENDIF}
end;

destructor TCICPropertyPage.Destroy;
begin
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  FProps.Free;
{$ENDIF}
  inherited Destroy;
end;

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
function TCICPropertyPage.GetServerProperties: TCICPropertyPageProperties;
begin
  Result := FProps;
end;
{$ENDIF}

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
constructor TCICPropertyPageProperties.Create(AServer: TCICPropertyPage);
begin
  inherited Create;
  FServer := AServer;
end;

function TCICPropertyPageProperties.GetDefaultInterface: IUnknown;
begin
  Result := FServer.DefaultInterface;
end;

{$ENDIF}

class function CoBSTRCollection.Create: IBSTRCollection;
begin
  Result := CreateComObject(CLASS_BSTRCollection) as IBSTRCollection;
end;

class function CoBSTRCollection.CreateRemote(const MachineName: string): IBSTRCollection;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_BSTRCollection) as IBSTRCollection;
end;

class function CoVCDCategoryMap.Create: IVCDCategoryMap;
begin
  Result := CreateComObject(CLASS_VCDCategoryMap) as IVCDCategoryMap;
end;

class function CoVCDCategoryMap.CreateRemote(const MachineName: string): IVCDCategoryMap;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_VCDCategoryMap) as IVCDCategoryMap;
end;

class function CoVCDPropertyItems.Create: IVCDPropertyItems;
begin
  Result := CreateComObject(CLASS_VCDPropertyItems) as IVCDPropertyItems;
end;

class function CoVCDPropertyItems.CreateRemote(const MachineName: string): IVCDPropertyItems;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_VCDPropertyItems) as IVCDPropertyItems;
end;

class function CoVCDPropertyItem.Create: IVCDPropertyItem;
begin
  Result := CreateComObject(CLASS_VCDPropertyItem) as IVCDPropertyItem;
end;

class function CoVCDPropertyItem.CreateRemote(const MachineName: string): IVCDPropertyItem;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_VCDPropertyItem) as IVCDPropertyItem;
end;

class function CoVCDPropertyElements.Create: IVCDPropertyElements;
begin
  Result := CreateComObject(CLASS_VCDPropertyElements) as IVCDPropertyElements;
end;

class function CoVCDPropertyElements.CreateRemote(const MachineName: string): IVCDPropertyElements;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_VCDPropertyElements) as IVCDPropertyElements;
end;

class function CoVCDPropertyElement.Create: IVCDPropertyElement;
begin
  Result := CreateComObject(CLASS_VCDPropertyElement) as IVCDPropertyElement;
end;

class function CoVCDPropertyElement.CreateRemote(const MachineName: string): IVCDPropertyElement;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_VCDPropertyElement) as IVCDPropertyElement;
end;

class function CoVCDPropertyInterface.Create: IVCDPropertyInterface;
begin
  Result := CreateComObject(CLASS_VCDPropertyInterface) as IVCDPropertyInterface;
end;

class function CoVCDPropertyInterface.CreateRemote(const MachineName: string): IVCDPropertyInterface;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_VCDPropertyInterface) as IVCDPropertyInterface;
end;

class function CoVCDRangeProperty.Create: IVCDRangeProperty;
begin
  Result := CreateComObject(CLASS_VCDRangeProperty) as IVCDRangeProperty;
end;

class function CoVCDRangeProperty.CreateRemote(const MachineName: string): IVCDRangeProperty;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_VCDRangeProperty) as IVCDRangeProperty;
end;

class function CoVCDSwitchProperty.Create: IVCDSwitchProperty;
begin
  Result := CreateComObject(CLASS_VCDSwitchProperty) as IVCDSwitchProperty;
end;

class function CoVCDSwitchProperty.CreateRemote(const MachineName: string): IVCDSwitchProperty;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_VCDSwitchProperty) as IVCDSwitchProperty;
end;

class function CoVCDButtonProperty.Create: IVCDButtonProperty;
begin
  Result := CreateComObject(CLASS_VCDButtonProperty) as IVCDButtonProperty;
end;

class function CoVCDButtonProperty.CreateRemote(const MachineName: string): IVCDButtonProperty;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_VCDButtonProperty) as IVCDButtonProperty;
end;

class function CoVCDMapStringsProperty.Create: IVCDMapStringsProperty;
begin
  Result := CreateComObject(CLASS_VCDMapStringsProperty) as IVCDMapStringsProperty;
end;

class function CoVCDMapStringsProperty.CreateRemote(const MachineName: string): IVCDMapStringsProperty;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_VCDMapStringsProperty) as IVCDMapStringsProperty;
end;

class function CoVCDAbsoluteValueProperty.Create: IVCDAbsoluteValueProperty;
begin
  Result := CreateComObject(CLASS_VCDAbsoluteValueProperty) as IVCDAbsoluteValueProperty;
end;

class function CoVCDAbsoluteValueProperty.CreateRemote(const MachineName: string): IVCDAbsoluteValueProperty;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_VCDAbsoluteValueProperty) as IVCDAbsoluteValueProperty;
end;

class function CoFrameFilterInfo.Create: IFrameFilterInfo;
begin
  Result := CreateComObject(CLASS_FrameFilterInfo) as IFrameFilterInfo;
end;

class function CoFrameFilterInfo.CreateRemote(const MachineName: string): IFrameFilterInfo;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_FrameFilterInfo) as IFrameFilterInfo;
end;

class function CoFrameFilterInfos.Create: IFrameFilterInfos;
begin
  Result := CreateComObject(CLASS_FrameFilterInfos) as IFrameFilterInfos;
end;

class function CoFrameFilterInfos.CreateRemote(const MachineName: string): IFrameFilterInfos;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_FrameFilterInfos) as IFrameFilterInfos;
end;

class function CoFrameFilter.Create: IFrameFilter;
begin
  Result := CreateComObject(CLASS_FrameFilter) as IFrameFilter;
end;

class function CoFrameFilter.CreateRemote(const MachineName: string): IFrameFilter;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_FrameFilter) as IFrameFilter;
end;

class function CoFrameFilters.Create: IFrameFilters;
begin
  Result := CreateComObject(CLASS_FrameFilters) as IFrameFilters;
end;

class function CoFrameFilters.CreateRemote(const MachineName: string): IFrameFilters;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_FrameFilters) as IFrameFilters;
end;

class function CoBaseSink.Create: IBaseSink;
begin
  Result := CreateComObject(CLASS_BaseSink) as IBaseSink;
end;

class function CoBaseSink.CreateRemote(const MachineName: string): IBaseSink;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_BaseSink) as IBaseSink;
end;

class function CoMediaStreamContainer.Create: IMediaStreamContainer;
begin
  Result := CreateComObject(CLASS_MediaStreamContainer) as IMediaStreamContainer;
end;

class function CoMediaStreamContainer.CreateRemote(const MachineName: string): IMediaStreamContainer;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_MediaStreamContainer) as IMediaStreamContainer;
end;

class function CoMediaStreamContainers.Create: IMediaStreamContainers;
begin
  Result := CreateComObject(CLASS_MediaStreamContainers) as IMediaStreamContainers;
end;

class function CoMediaStreamContainers.CreateRemote(const MachineName: string): IMediaStreamContainers;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_MediaStreamContainers) as IMediaStreamContainers;
end;

class function CoMediaStreamSink.Create: IMediaStreamSink;
begin
  Result := CreateComObject(CLASS_MediaStreamSink) as IMediaStreamSink;
end;

class function CoMediaStreamSink.CreateRemote(const MachineName: string): IMediaStreamSink;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_MediaStreamSink) as IMediaStreamSink;
end;

procedure TMediaStreamSink.InitServerData;
const
  CServerData: TServerData = (
    ClassID:   '{E0110BE7-EBF0-4612-B2F8-817194557E19}';
    IntfIID:   '{E0110BE7-EBF0-4612-B2F8-817194557E18}';
    EventIID:  '';
    LicenseKey: nil;
    Version: 500);
begin
  ServerData := @CServerData;
end;

procedure TMediaStreamSink.Connect;
var
  punk: IUnknown;
begin
  if FIntf = nil then
  begin
    punk := GetServer;
    Fintf:= punk as IMediaStreamSink;
  end;
end;

procedure TMediaStreamSink.ConnectTo(svrIntf: IMediaStreamSink);
begin
  Disconnect;
  FIntf := svrIntf;
end;

procedure TMediaStreamSink.DisConnect;
begin
  if Fintf <> nil then
  begin
    FIntf := nil;
  end;
end;

function TMediaStreamSink.GetDefaultInterface: IMediaStreamSink;
begin
  if FIntf = nil then
    Connect;
  Assert(FIntf <> nil, 'DefaultInterface is NULL. Component is not connected to Server. You must call ''Connect'' or ''ConnectTo'' before this operation');
  Result := FIntf;
end;

constructor TMediaStreamSink.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  FProps := TMediaStreamSinkProperties.Create(Self);
{$ENDIF}
end;

destructor TMediaStreamSink.Destroy;
begin
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  FProps.Free;
{$ENDIF}
  inherited Destroy;
end;

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
function TMediaStreamSink.GetServerProperties: TMediaStreamSinkProperties;
begin
  Result := FProps;
end;
{$ENDIF}

function TMediaStreamSink.Get_SinkType: BaseSinkTypes;
begin
    Result := DefaultInterface.SinkType;
end;

function TMediaStreamSink.Get_SinkModeRunning: WordBool;
begin
    Result := DefaultInterface.SinkModeRunning;
end;

procedure TMediaStreamSink.Set_SinkModeRunning(IsRunning: WordBool);
begin
  DefaultInterface.Set_SinkModeRunning(IsRunning);
end;

function TMediaStreamSink.Get_Filename: WideString;
begin
    Result := DefaultInterface.Filename;
end;

procedure TMediaStreamSink.Set_Filename(const SinkFilename: WideString);
  { Warning: The property Filename has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.Filename := SinkFilename;
end;

function TMediaStreamSink.Get_StreamContainer: IMediaStreamContainer;
begin
    Result := DefaultInterface.StreamContainer;
end;

procedure TMediaStreamSink.Set_StreamContainer(const CurrentContainer: IMediaStreamContainer);
begin
  DefaultInterface.Set_StreamContainer(CurrentContainer);
end;

function TMediaStreamSink.Get_Codec: IAviCompressor;
begin
    Result := DefaultInterface.Codec;
end;

procedure TMediaStreamSink.Set_Codec(const Type_: IAviCompressor);
begin
  DefaultInterface.Set_Codec(Type_);
end;

function TMediaStreamSink.Get_FrameFilters: IFrameFilters;
begin
    Result := DefaultInterface.FrameFilters;
end;

function TMediaStreamSink.Get_FrameTimeCorrection: WordBool;
begin
    Result := DefaultInterface.FrameTimeCorrection;
end;

procedure TMediaStreamSink.Set_FrameTimeCorrection(Enabled: WordBool);
begin
  DefaultInterface.Set_FrameTimeCorrection(Enabled);
end;

procedure TMediaStreamSink.Attach(var pAttachTarget: Pointer);
begin
  DefaultInterface.Attach(pAttachTarget);
end;

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
constructor TMediaStreamSinkProperties.Create(AServer: TMediaStreamSink);
begin
  inherited Create;
  FServer := AServer;
end;

function TMediaStreamSinkProperties.GetDefaultInterface: IMediaStreamSink;
begin
  Result := FServer.DefaultInterface;
end;

function TMediaStreamSinkProperties.Get_SinkType: BaseSinkTypes;
begin
    Result := DefaultInterface.SinkType;
end;

function TMediaStreamSinkProperties.Get_SinkModeRunning: WordBool;
begin
    Result := DefaultInterface.SinkModeRunning;
end;

procedure TMediaStreamSinkProperties.Set_SinkModeRunning(IsRunning: WordBool);
begin
  DefaultInterface.Set_SinkModeRunning(IsRunning);
end;

function TMediaStreamSinkProperties.Get_Filename: WideString;
begin
    Result := DefaultInterface.Filename;
end;

procedure TMediaStreamSinkProperties.Set_Filename(const SinkFilename: WideString);
  { Warning: The property Filename has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.Filename := SinkFilename;
end;

function TMediaStreamSinkProperties.Get_StreamContainer: IMediaStreamContainer;
begin
    Result := DefaultInterface.StreamContainer;
end;

procedure TMediaStreamSinkProperties.Set_StreamContainer(const CurrentContainer: IMediaStreamContainer);
begin
  DefaultInterface.Set_StreamContainer(CurrentContainer);
end;

function TMediaStreamSinkProperties.Get_Codec: IAviCompressor;
begin
    Result := DefaultInterface.Codec;
end;

procedure TMediaStreamSinkProperties.Set_Codec(const Type_: IAviCompressor);
begin
  DefaultInterface.Set_Codec(Type_);
end;

function TMediaStreamSinkProperties.Get_FrameFilters: IFrameFilters;
begin
    Result := DefaultInterface.FrameFilters;
end;

function TMediaStreamSinkProperties.Get_FrameTimeCorrection: WordBool;
begin
    Result := DefaultInterface.FrameTimeCorrection;
end;

procedure TMediaStreamSinkProperties.Set_FrameTimeCorrection(Enabled: WordBool);
begin
  DefaultInterface.Set_FrameTimeCorrection(Enabled);
end;

{$ENDIF}

class function CoFrameType.Create: IFrameType;
begin
  Result := CreateComObject(CLASS_FrameType) as IFrameType;
end;

class function CoFrameType.CreateRemote(const MachineName: string): IFrameType;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_FrameType) as IFrameType;
end;

procedure TFrameType.InitServerData;
const
  CServerData: TServerData = (
    ClassID:   '{E0110BE7-EBF0-4612-B2F8-817194557E21}';
    IntfIID:   '{E0110BE7-EBF0-4612-B2F8-817194557E20}';
    EventIID:  '';
    LicenseKey: nil;
    Version: 500);
begin
  ServerData := @CServerData;
end;

procedure TFrameType.Connect;
var
  punk: IUnknown;
begin
  if FIntf = nil then
  begin
    punk := GetServer;
    Fintf:= punk as IFrameType;
  end;
end;

procedure TFrameType.ConnectTo(svrIntf: IFrameType);
begin
  Disconnect;
  FIntf := svrIntf;
end;

procedure TFrameType.DisConnect;
begin
  if Fintf <> nil then
  begin
    FIntf := nil;
  end;
end;

function TFrameType.GetDefaultInterface: IFrameType;
begin
  if FIntf = nil then
    Connect;
  Assert(FIntf <> nil, 'DefaultInterface is NULL. Component is not connected to Server. You must call ''Connect'' or ''ConnectTo'' before this operation');
  Result := FIntf;
end;

constructor TFrameType.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  FProps := TFrameTypeProperties.Create(Self);
{$ENDIF}
end;

destructor TFrameType.Destroy;
begin
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  FProps.Free;
{$ENDIF}
  inherited Destroy;
end;

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
function TFrameType.GetServerProperties: TFrameTypeProperties;
begin
  Result := FProps;
end;
{$ENDIF}

function TFrameType.Get_Name: WideString;
begin
    Result := DefaultInterface.Name;
end;

procedure TFrameType.Set_Name(const NameStr: WideString);
  { Warning: The property Name has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.Name := NameStr;
end;

function TFrameType.Get_type_: WideString;
begin
    Result := DefaultInterface.type_;
end;

procedure TFrameType.Set_type_(const TypeStr: WideString);
  { Warning: The property type_ has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.type_ := TypeStr;
end;

function TFrameType.Get_Width: Integer;
begin
    Result := DefaultInterface.Width;
end;

procedure TFrameType.Set_Width(CurWidth: Integer);
begin
  DefaultInterface.Set_Width(CurWidth);
end;

function TFrameType.Get_Height: Integer;
begin
    Result := DefaultInterface.Height;
end;

procedure TFrameType.Set_Height(CurHeight: Integer);
begin
  DefaultInterface.Set_Height(CurHeight);
end;

function TFrameType.Get_Buffersize: Integer;
begin
    Result := DefaultInterface.Buffersize;
end;

procedure TFrameType.Set_Buffersize(CurBuffersize: Integer);
begin
  DefaultInterface.Set_Buffersize(CurBuffersize);
end;

function TFrameType.Get_BitsPerPixel: Integer;
begin
    Result := DefaultInterface.BitsPerPixel;
end;

function TFrameType.Get_IsBottomUp: WordBool;
begin
    Result := DefaultInterface.IsBottomUp;
end;

function TFrameType.Get_SubtypeGUID: WideString;
begin
    Result := DefaultInterface.SubtypeGUID;
end;

procedure TFrameType.Set_SubtypeGUID(const guid: WideString);
  { Warning: The property SubtypeGUID has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.SubtypeGUID := guid;
end;

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
constructor TFrameTypeProperties.Create(AServer: TFrameType);
begin
  inherited Create;
  FServer := AServer;
end;

function TFrameTypeProperties.GetDefaultInterface: IFrameType;
begin
  Result := FServer.DefaultInterface;
end;

function TFrameTypeProperties.Get_Name: WideString;
begin
    Result := DefaultInterface.Name;
end;

procedure TFrameTypeProperties.Set_Name(const NameStr: WideString);
  { Warning: The property Name has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.Name := NameStr;
end;

function TFrameTypeProperties.Get_type_: WideString;
begin
    Result := DefaultInterface.type_;
end;

procedure TFrameTypeProperties.Set_type_(const TypeStr: WideString);
  { Warning: The property type_ has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.type_ := TypeStr;
end;

function TFrameTypeProperties.Get_Width: Integer;
begin
    Result := DefaultInterface.Width;
end;

procedure TFrameTypeProperties.Set_Width(CurWidth: Integer);
begin
  DefaultInterface.Set_Width(CurWidth);
end;

function TFrameTypeProperties.Get_Height: Integer;
begin
    Result := DefaultInterface.Height;
end;

procedure TFrameTypeProperties.Set_Height(CurHeight: Integer);
begin
  DefaultInterface.Set_Height(CurHeight);
end;

function TFrameTypeProperties.Get_Buffersize: Integer;
begin
    Result := DefaultInterface.Buffersize;
end;

procedure TFrameTypeProperties.Set_Buffersize(CurBuffersize: Integer);
begin
  DefaultInterface.Set_Buffersize(CurBuffersize);
end;

function TFrameTypeProperties.Get_BitsPerPixel: Integer;
begin
    Result := DefaultInterface.BitsPerPixel;
end;

function TFrameTypeProperties.Get_IsBottomUp: WordBool;
begin
    Result := DefaultInterface.IsBottomUp;
end;

function TFrameTypeProperties.Get_SubtypeGUID: WideString;
begin
    Result := DefaultInterface.SubtypeGUID;
end;

procedure TFrameTypeProperties.Set_SubtypeGUID(const guid: WideString);
  { Warning: The property SubtypeGUID has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.SubtypeGUID := guid;
end;

{$ENDIF}

class function CoFrameTypes.Create: IFrameTypes;
begin
  Result := CreateComObject(CLASS_FrameTypes) as IFrameTypes;
end;

class function CoFrameTypes.CreateRemote(const MachineName: string): IFrameTypes;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_FrameTypes) as IFrameTypes;
end;

procedure TFrameTypes.InitServerData;
const
  CServerData: TServerData = (
    ClassID:   '{E0110BE7-EBF0-4612-B2F8-817194557E23}';
    IntfIID:   '{E0110BE7-EBF0-4612-B2F8-817194557E22}';
    EventIID:  '';
    LicenseKey: nil;
    Version: 500);
begin
  ServerData := @CServerData;
end;

procedure TFrameTypes.Connect;
var
  punk: IUnknown;
begin
  if FIntf = nil then
  begin
    punk := GetServer;
    Fintf:= punk as IFrameTypes;
  end;
end;

procedure TFrameTypes.ConnectTo(svrIntf: IFrameTypes);
begin
  Disconnect;
  FIntf := svrIntf;
end;

procedure TFrameTypes.DisConnect;
begin
  if Fintf <> nil then
  begin
    FIntf := nil;
  end;
end;

function TFrameTypes.GetDefaultInterface: IFrameTypes;
begin
  if FIntf = nil then
    Connect;
  Assert(FIntf <> nil, 'DefaultInterface is NULL. Component is not connected to Server. You must call ''Connect'' or ''ConnectTo'' before this operation');
  Result := FIntf;
end;

constructor TFrameTypes.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  FProps := TFrameTypesProperties.Create(Self);
{$ENDIF}
end;

destructor TFrameTypes.Destroy;
begin
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  FProps.Free;
{$ENDIF}
  inherited Destroy;
end;

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
function TFrameTypes.GetServerProperties: TFrameTypesProperties;
begin
  Result := FProps;
end;
{$ENDIF}

function TFrameTypes.Get__NewEnum: IUnknown;
begin
    Result := DefaultInterface._NewEnum;
end;

function TFrameTypes.Get_Item(n: Integer): IFrameType;
begin
    Result := DefaultInterface.Item[n];
end;

function TFrameTypes.Get_Count: Integer;
begin
    Result := DefaultInterface.Count;
end;

procedure TFrameTypes.Set_ReadOnly(Param1: WordBool);
begin
  DefaultInterface.Set_ReadOnly(Param1);
end;

procedure TFrameTypes.Add(const NewType: IFrameType);
begin
  DefaultInterface.Add(NewType);
end;

procedure TFrameTypes.Remove(Index: Integer);
begin
  DefaultInterface.Remove(Index);
end;

procedure TFrameTypes.Clear;
begin
  DefaultInterface.Clear;
end;

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
constructor TFrameTypesProperties.Create(AServer: TFrameTypes);
begin
  inherited Create;
  FServer := AServer;
end;

function TFrameTypesProperties.GetDefaultInterface: IFrameTypes;
begin
  Result := FServer.DefaultInterface;
end;

function TFrameTypesProperties.Get__NewEnum: IUnknown;
begin
    Result := DefaultInterface._NewEnum;
end;

function TFrameTypesProperties.Get_Item(n: Integer): IFrameType;
begin
    Result := DefaultInterface.Item[n];
end;

function TFrameTypesProperties.Get_Count: Integer;
begin
    Result := DefaultInterface.Count;
end;

procedure TFrameTypesProperties.Set_ReadOnly(Param1: WordBool);
begin
  DefaultInterface.Set_ReadOnly(Param1);
end;

{$ENDIF}

class function CoFrameHandlerSink.Create: IFrameHandlerSink;
begin
  Result := CreateComObject(CLASS_FrameHandlerSink) as IFrameHandlerSink;
end;

class function CoFrameHandlerSink.CreateRemote(const MachineName: string): IFrameHandlerSink;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_FrameHandlerSink) as IFrameHandlerSink;
end;

procedure TFrameHandlerSink.InitServerData;
const
  CServerData: TServerData = (
    ClassID:   '{E0110BE7-EBF0-4612-B2F8-817194557E1B}';
    IntfIID:   '{E0110BE7-EBF0-4612-B2F8-817194557E1A}';
    EventIID:  '';
    LicenseKey: nil;
    Version: 500);
begin
  ServerData := @CServerData;
end;

procedure TFrameHandlerSink.Connect;
var
  punk: IUnknown;
begin
  if FIntf = nil then
  begin
    punk := GetServer;
    Fintf:= punk as IFrameHandlerSink;
  end;
end;

procedure TFrameHandlerSink.ConnectTo(svrIntf: IFrameHandlerSink);
begin
  Disconnect;
  FIntf := svrIntf;
end;

procedure TFrameHandlerSink.DisConnect;
begin
  if Fintf <> nil then
  begin
    FIntf := nil;
  end;
end;

function TFrameHandlerSink.GetDefaultInterface: IFrameHandlerSink;
begin
  if FIntf = nil then
    Connect;
  Assert(FIntf <> nil, 'DefaultInterface is NULL. Component is not connected to Server. You must call ''Connect'' or ''ConnectTo'' before this operation');
  Result := FIntf;
end;

constructor TFrameHandlerSink.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  FProps := TFrameHandlerSinkProperties.Create(Self);
{$ENDIF}
end;

destructor TFrameHandlerSink.Destroy;
begin
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  FProps.Free;
{$ENDIF}
  inherited Destroy;
end;

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
function TFrameHandlerSink.GetServerProperties: TFrameHandlerSinkProperties;
begin
  Result := FProps;
end;
{$ENDIF}

function TFrameHandlerSink.Get_SinkType: BaseSinkTypes;
begin
    Result := DefaultInterface.SinkType;
end;

function TFrameHandlerSink.Get_SinkModeRunning: WordBool;
begin
    Result := DefaultInterface.SinkModeRunning;
end;

procedure TFrameHandlerSink.Set_SinkModeRunning(IsRunning: WordBool);
begin
  DefaultInterface.Set_SinkModeRunning(IsRunning);
end;

function TFrameHandlerSink.Get_SnapMode: WordBool;
begin
    Result := DefaultInterface.SnapMode;
end;

procedure TFrameHandlerSink.Set_SnapMode(SnapModeEnabled: WordBool);
begin
  DefaultInterface.Set_SnapMode(SnapModeEnabled);
end;

function TFrameHandlerSink.Get_ImageBuffers: IImageBuffers;
begin
    Result := DefaultInterface.ImageBuffers;
end;

function TFrameHandlerSink.Get_LastAcquiredBuffer: IImageBuffer;
begin
    Result := DefaultInterface.LastAcquiredBuffer;
end;

function TFrameHandlerSink.Get_BufferCount: Integer;
begin
    Result := DefaultInterface.BufferCount;
end;

procedure TFrameHandlerSink.Set_BufferCount(CurrentBufferCount: Integer);
begin
  DefaultInterface.Set_BufferCount(CurrentBufferCount);
end;

function TFrameHandlerSink.Get_FrameTypes: IFrameTypes;
begin
    Result := DefaultInterface.FrameTypes;
end;

function TFrameHandlerSink.Get_FrameFilters: IFrameFilters;
begin
    Result := DefaultInterface.FrameFilters;
end;

function TFrameHandlerSink.Get_FrameCount: Integer;
begin
    Result := DefaultInterface.FrameCount;
end;

procedure TFrameHandlerSink.Attach(var pAttachTarget: Pointer);
begin
  DefaultInterface.Attach(pAttachTarget);
end;

procedure TFrameHandlerSink.SnapImage(timeout: Integer);
begin
  DefaultInterface.SnapImage(timeout);
end;

procedure TFrameHandlerSink.SnapImageSequence(Count: Integer; timeout: Integer);
begin
  DefaultInterface.SnapImageSequence(Count, timeout);
end;

procedure TFrameHandlerSink.SnapImageSequenceAsync(Count: Integer);
begin
  DefaultInterface.SnapImageSequenceAsync(Count);
end;

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
constructor TFrameHandlerSinkProperties.Create(AServer: TFrameHandlerSink);
begin
  inherited Create;
  FServer := AServer;
end;

function TFrameHandlerSinkProperties.GetDefaultInterface: IFrameHandlerSink;
begin
  Result := FServer.DefaultInterface;
end;

function TFrameHandlerSinkProperties.Get_SinkType: BaseSinkTypes;
begin
    Result := DefaultInterface.SinkType;
end;

function TFrameHandlerSinkProperties.Get_SinkModeRunning: WordBool;
begin
    Result := DefaultInterface.SinkModeRunning;
end;

procedure TFrameHandlerSinkProperties.Set_SinkModeRunning(IsRunning: WordBool);
begin
  DefaultInterface.Set_SinkModeRunning(IsRunning);
end;

function TFrameHandlerSinkProperties.Get_SnapMode: WordBool;
begin
    Result := DefaultInterface.SnapMode;
end;

procedure TFrameHandlerSinkProperties.Set_SnapMode(SnapModeEnabled: WordBool);
begin
  DefaultInterface.Set_SnapMode(SnapModeEnabled);
end;

function TFrameHandlerSinkProperties.Get_ImageBuffers: IImageBuffers;
begin
    Result := DefaultInterface.ImageBuffers;
end;

function TFrameHandlerSinkProperties.Get_LastAcquiredBuffer: IImageBuffer;
begin
    Result := DefaultInterface.LastAcquiredBuffer;
end;

function TFrameHandlerSinkProperties.Get_BufferCount: Integer;
begin
    Result := DefaultInterface.BufferCount;
end;

procedure TFrameHandlerSinkProperties.Set_BufferCount(CurrentBufferCount: Integer);
begin
  DefaultInterface.Set_BufferCount(CurrentBufferCount);
end;

function TFrameHandlerSinkProperties.Get_FrameTypes: IFrameTypes;
begin
    Result := DefaultInterface.FrameTypes;
end;

function TFrameHandlerSinkProperties.Get_FrameFilters: IFrameFilters;
begin
    Result := DefaultInterface.FrameFilters;
end;

function TFrameHandlerSinkProperties.Get_FrameCount: Integer;
begin
    Result := DefaultInterface.FrameCount;
end;

{$ENDIF}

procedure TICImagingControl.InitControlData;
const
  CEventDispIDs: array [0..7] of DWORD = (
    $00000001, $00000002, $00000003, $00000004, $00000005, $00000007,
    $00000008, $00000006);
  CLicenseKey: array[0..12] of Word = ( $0048, $0061, $006C, $006C, $006F, $002C, $0020, $0057, $0065, $006C, $0074
    , $0021, $0000);
  CControlData: TControlData2 = (
    ClassID: '{E0110BE7-EBF0-4612-B2F8-817194557141}';
    EventIID: '{E0110BE7-EBF0-4612-B2F8-817194557DE9}';
    EventCount: 8;
    EventDispIDs: @CEventDispIDs;
    LicenseKey: @CLicenseKey;
    Flags: $00000000;
    Version: 401);
begin
  ControlData := @CControlData;
  TControlData2(CControlData).FirstEventOfs := Cardinal(@@FOnMouseDown) - Cardinal(Self);
end;

procedure TICImagingControl.CreateControl;

  procedure DoCreate;
  begin
    FIntf := IUnknown(OleObject) as IICImagingControl;
  end;

begin
  if FIntf = nil then DoCreate;
end;

function TICImagingControl.GetControlInterface: IICImagingControl;
begin
  CreateControl;
  Result := FIntf;
end;

function TICImagingControl.Get_BrightnessRange: OleVariant;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.BrightnessRange;
end;

function TICImagingControl.Get_ContrastRange: OleVariant;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.ContrastRange;
end;

function TICImagingControl.Get_HueRange: OleVariant;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.HueRange;
end;

function TICImagingControl.Get_SaturationRange: OleVariant;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.SaturationRange;
end;

function TICImagingControl.Get_SharpnessRange: OleVariant;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.SharpnessRange;
end;

function TICImagingControl.Get_GammaRange: OleVariant;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.GammaRange;
end;

function TICImagingControl.Get_ColorEnableRange: OleVariant;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.ColorEnableRange;
end;

function TICImagingControl.Get_WhiteBalanceRange: OleVariant;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.WhiteBalanceRange;
end;

function TICImagingControl.Get_BacklightCompensationRange: OleVariant;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.BacklightCompensationRange;
end;

function TICImagingControl.Get_GainRange: OleVariant;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.GainRange;
end;

function TICImagingControl.Get_PanRange: OleVariant;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.PanRange;
end;

function TICImagingControl.Get_TiltRange: OleVariant;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.TiltRange;
end;

function TICImagingControl.Get_RollRange: OleVariant;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.RollRange;
end;

function TICImagingControl.Get_ZoomRange: OleVariant;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.ZoomRange;
end;

function TICImagingControl.Get_ExposureRange: OleVariant;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.ExposureRange;
end;

function TICImagingControl.Get_IrisRange: OleVariant;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.IrisRange;
end;

function TICImagingControl.Get_FocusRange: OleVariant;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.FocusRange;
end;

function TICImagingControl.Get_Picture: IUnknown;
begin
    Result := DefaultInterface.Picture;
end;

function TICImagingControl.Get_DeviceFrameRates: IDeviceFrameRates;
begin
    Result := DefaultInterface.DeviceFrameRates;
end;

function TICImagingControl.Get_ImageBuffers: IImageBuffers;
begin
    Result := DefaultInterface.ImageBuffers;
end;

function TICImagingControl.Get_VideoFormats: IVideoFormats;
begin
    Result := DefaultInterface.VideoFormats;
end;

function TICImagingControl.Get_Devices: IDevices;
begin
    Result := DefaultInterface.Devices;
end;

function TICImagingControl.Get_VideoNorms: IVideoNorms;
begin
    Result := DefaultInterface.VideoNorms;
end;

function TICImagingControl.Get_InputChannels: IInputChannels;
begin
    Result := DefaultInterface.InputChannels;
end;

function TICImagingControl.Get_AviCompressors: IAviCompressors;
begin
    Result := DefaultInterface.AviCompressors;
end;

function TICImagingControl.Get_OverlayBitmap: IOverlayBitmap;
begin
    Result := DefaultInterface.OverlayBitmap;
end;

function TICImagingControl.Get_VCDPropertyItems: IVCDPropertyItems;
begin
    Result := DefaultInterface.VCDPropertyItems;
end;

function TICImagingControl.Get_Sink: IBaseSink;
begin
    Result := DefaultInterface.Sink;
end;

procedure TICImagingControl.Set_Sink(const CurrentSink: IBaseSink);
begin
  DefaultInterface.Set_Sink(CurrentSink);
end;

function TICImagingControl.Get_MediaStreamContainers: IMediaStreamContainers;
begin
    Result := DefaultInterface.MediaStreamContainers;
end;

function TICImagingControl.Get_FrameFilterInfos: IFrameFilterInfos;
begin
    Result := DefaultInterface.FrameFilterInfos;
end;

function TICImagingControl.Get_DeviceFrameFilters: IFrameFilters;
begin
    Result := DefaultInterface.DeviceFrameFilters;
end;

function TICImagingControl.Get_ImageActiveBuffer: IImageBuffer;
begin
    Result := DefaultInterface.ImageActiveBuffer;
end;

function TICImagingControl.Get_DisplayFrameFilters: IFrameFilters;
begin
    Result := DefaultInterface.DisplayFrameFilters;
end;

function TICImagingControl.Get_OverlayBitmapAtPath(OverlayToFetch: PathPositions): IOverlayBitmap;
begin
    Result := DefaultInterface.OverlayBitmapAtPath[OverlayToFetch];
end;

procedure TICImagingControl.Refresh;
begin
  DefaultInterface.Refresh;
end;

procedure TICImagingControl.LiveStart;
begin
  DefaultInterface.LiveStart;
end;

procedure TICImagingControl.LiveStop;
begin
  DefaultInterface.LiveStop;
end;

procedure TICImagingControl.MemorySaveImage(const Filename: WideString);
begin
  DefaultInterface.MemorySaveImage(Filename);
end;

procedure TICImagingControl.MemorySaveImageSequence(SequenceLength: Integer; 
                                                    const Filename: WideString);
begin
  DefaultInterface.MemorySaveImageSequence(SequenceLength, Filename);
end;

procedure TICImagingControl.MemorySnapImage;
begin
  DefaultInterface.MemorySnapImage;
end;

procedure TICImagingControl.MemorySnapImageSequence(ImageCount: Integer);
begin
  DefaultInterface.MemorySnapImageSequence(ImageCount);
end;

function TICImagingControl.MemoryGetImageData: OleVariant;
begin
  Result := DefaultInterface.MemoryGetImageData;
end;

procedure TICImagingControl.MemoryReleaseImageData(var Buffer: OleVariant);
begin
  DefaultInterface.MemoryReleaseImageData(Buffer);
end;

function TICImagingControl.MemoryGetDib: Integer;
begin
  Result := DefaultInterface.MemoryGetDib;
end;

procedure TICImagingControl.AviStartCapture(const Filename: WideString; const Compressor: WideString);
begin
  DefaultInterface.AviStartCapture(Filename, Compressor);
end;

procedure TICImagingControl.AviStopCapture;
begin
  DefaultInterface.AviStopCapture;
end;

procedure TICImagingControl.Display;
begin
  DefaultInterface.Display;
end;

procedure TICImagingControl.DisplayImageBuffer(const ImageBufferToDisplay: IImageBuffer);
begin
  DefaultInterface.DisplayImageBuffer(ImageBufferToDisplay);
end;

function TICImagingControl.GetGrabberPtr: Integer;
begin
  Result := DefaultInterface.GetGrabberPtr;
end;

function TICImagingControl.SaveDeviceState: WideString;
begin
  Result := DefaultInterface.SaveDeviceState;
end;

procedure TICImagingControl.LoadDeviceState(const String_: WideString; OpenDev: WordBool);
begin
  DefaultInterface.LoadDeviceState(String_, OpenDev);
end;

procedure TICImagingControl.SaveDeviceStateToFile(const Filename: WideString);
begin
  DefaultInterface.SaveDeviceStateToFile(Filename);
end;

procedure TICImagingControl.LoadDeviceStateFromFile(const Filename: WideString; OpenDev: WordBool);
begin
  DefaultInterface.LoadDeviceStateFromFile(Filename, OpenDev);
end;

procedure TICImagingControl.ShowPropertyDialog;
begin
  DefaultInterface.ShowPropertyDialog;
end;

procedure TICImagingControl.ShowDeviceSettingsDialog;
begin
  DefaultInterface.ShowDeviceSettingsDialog;
end;

function TICImagingControl.FrameFilterCreate(const Info: IFrameFilterInfo): IFrameFilter;
begin
  Result := DefaultInterface.FrameFilterCreate(Info);
end;

function TICImagingControl.FrameFilterCreateString(const FilterName: WideString; 
                                                   const ModuleName: WideString): IFrameFilter;
begin
  Result := DefaultInterface.FrameFilterCreateString(FilterName, ModuleName);
end;

procedure TICImagingControl._SetDirty(Val: Integer);
begin
  DefaultInterface._SetDirty(Val);
end;

function TICImagingControl.FrameFilterCreate_(FilterPtr: Integer): IFrameFilter;
begin
  Result := DefaultInterface.FrameFilterCreate_(FilterPtr);
end;

procedure TICImagingControl.MemorySaveImageJpeg(const Filename: WideString; Quality: Integer);
begin
  DefaultInterface.MemorySaveImageJpeg(Filename, Quality);
end;

procedure Register;
begin
  RegisterComponents(dtlOcxPage, [TICImagingControl]);
  RegisterComponents(dtlServerPage, [TCICPropertyPage, TMediaStreamSink, TFrameType, TFrameTypes, 
    TFrameHandlerSink]);
end;

end.
