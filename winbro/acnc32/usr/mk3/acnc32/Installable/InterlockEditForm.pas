unit InterlockEditForm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, CNC32, CNCTypes;

type
  TInterlockEditFrm = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    AccessGroup: TGroupBox;
    StatusPanel: TPanel;
    StatusGroup: TGroupBox;
    SpeedButton13: TSpeedButton;
    OperatorSpeedBtn: TSpeedButton;
    OperatorLabel: TStaticText;
    SupervisorSpeedBtn: TSpeedButton;
    SupervisorLabel: TStaticText;
    ProgrammerSpeedBtn: TSpeedButton;
    ProgrammerLabel: TStaticText;
    MaintenanceLabel: TStaticText;
    MaintenanceSpeedBtn: TSpeedButton;
    AdministratorSpeedBtn: TSpeedButton;
    AdministratorLabel: TStaticText;
    OEM1SpeedBtn: TSpeedButton;
    OEM1Label: TStaticText;
    StaticText12: TStaticText;
    StaticText13: TStaticText;
    InMotionLabel: TStaticText;
    InCycleLabel: TStaticText;
    HomedLabel: TStaticText;
    EditingLabel: TStaticText;
    IncInMotionSpeedBtn: TSpeedButton;
    IncInCycleSpeedBtn: TSpeedButton;
    IncHomeSpeedBtn: TSpeedButton;
    IncEditingSpeedBtn: TSpeedButton;
    ExcInMotionSpeedBtn: TSpeedButton;
    ExcInCycleSpeedBtn: TSpeedButton;
    ExcHomeSpeedBtn: TSpeedButton;
    ExcEditingSpeedBtn: TSpeedButton;
    OkBtn: TBitBtn;
    BitBtn2: TBitBtn;
    NoneSpeedBtn: TSpeedButton;
    NoneLabel: TStaticText;
    Panel3: TPanel;
    ModesGroup: TGroupBox;
    ManualSpeedBtn: TSpeedButton;
    AutoSpeedBtn: TSpeedButton;
    MDISpeedBtn: TSpeedButton;
    TeachInSpeedBtn: TSpeedButton;
    EditSpeedBtn: TSpeedButton;
    ManualLabel: TStaticText;
    AutoLabel: TStaticText;
    MDILabel: TStaticText;
    TeachinLabel: TStaticText;
    EditLabel: TStaticText;
    Panel4: TPanel;
    ActiveSpeedBtn: TSpeedButton;
    ActiveLabel: TStaticText;
    RemoveBtn: TBitBtn;
    procedure ManualSpeedBtnClick(Sender: TObject);
    procedure ActiveSpeedBtnClick(Sender: TObject);
  private
    ThisTag : TAbstractTag;
    procedure DoModeChange(aMode : TCNCMode);
    procedure DoAccessChange(aAccess : TAccessLevel);
    procedure ExclusiveStatus(aStatus : TCNCStatusType);
    procedure InclusiveStatus(aStatus : TCNCStatusType);
    procedure RenderView;
  public
    Interlock : TInterlock;
    function Execute(aTag : TAbstractTag) : TModalResult;
  end;

var
  InterlockEditFrm: TInterlockEditFrm;

implementation

{$R *.DFM}

const
  NullInterlock : TInterlock = (
     Active : False ;
     Exclusion : [] ;
     Inclusion : [] ;
     Modes : [Low(TCNCMode)..High(TCNCMode)] ;
     Access : [Low(TAccessLevel)..High(TAccessLevel)]
  );

resourcestring
  AccessAdministrationName = 'Access Administration';
  ActiveLang = 'Active';
  InactiveLang = 'Inactive';

function TInterlockEditFrm.Execute(aTag : TAbstractTag) : TModalResult;
begin
  Self.Caption := AccessAdministrationName + ' [' + aTag.Name + ']';
  ThisTag := aTag;

  if ThisTag.HasInterlock then
    Interlock := ThisTag.Interlock^
  else
    Interlock := NullInterlock;

  RenderView;
  Result := ShowModal;
end;

procedure TInterlockEditFrm.ManualSpeedBtnClick(Sender: TObject);
begin
  if Sender is TSpeedButton then begin
    with Sender as TSpeedButton do begin
      if Tag < 10 then begin
        DoModeChange(TCNCMode(Tag));
      end else if Tag < 20 then begin
        DoAccessChange(TAccessLevel(Tag - 10));
      end else if Tag < 30 then begin
        InclusiveStatus(TCNCStatusType(Tag - 20));
      end else if Tag < 40 then begin
        ExclusiveStatus(TCNCStatusType(Tag - 30));
      end;
    end;
  end;
  RenderView;
end;


procedure TInterlockEditFrm.DoModeChange(aMode : TCNCMode);
begin
  if aMode in Interlock.Modes then
    Exclude(Interlock.Modes, aMode)
  else
    Include(Interlock.Modes, aMode)
end;

procedure TInterlockEditFrm.DoAccessChange(aAccess : TAccessLevel);
begin
  if aAccess in Interlock.Access then
    Exclude(Interlock.Access, aAccess)
  else
    Include(Interlock.Access, aAccess)
end;

procedure TInterlockEditFrm.InclusiveStatus(aStatus : TCNCStatusType);
begin
  if aStatus in Interlock.Inclusion then begin
    Exclude(Interlock.Inclusion, aStatus);
  end else begin
    Include(Interlock.Inclusion, aStatus);
    Exclude(Interlock.Exclusion, aStatus);
  end;
end;

procedure TInterlockEditFrm.ExclusiveStatus(aStatus : TCNCStatusType);
begin
  if aStatus in Interlock.Exclusion then begin
    Exclude(Interlock.Exclusion, aStatus);
  end else begin
    Include(Interlock.Exclusion, aStatus);
    Exclude(Interlock.Inclusion, aStatus);
  end;
end;

procedure TInterlockEditFrm.ActiveSpeedBtnClick(Sender: TObject);
begin
  Interlock.Active := ActiveSpeedBtn.Down;
  RenderView;
end;

procedure TInterlockEditFrm.RenderView;
const
  AColor : array [Boolean] of TColor = ( clRed, clLime  );
begin
  ManualLabel.Color := AColor[CNCModeManual in Interlock.Modes];
  AutoLabel.Color   := AColor[CNCModeAuto in Interlock.Modes];
  MDILabel.Color    := AColor[CNCModeMDI in Interlock.Modes];
  TeachInLabel.Color:= AColor[CNCModeTeachIn in Interlock.Modes];
  EditLabel.Color   := AColor[CNCModeEdit in Interlock.Modes];

  NoneLabel.Color := AColor[AccessLevelNone in Interlock.Access];
  OperatorLabel.Color := AColor[AccessLevelOperator in Interlock.Access];
  SupervisorLabel.Color := AColor[AccessLevelSupervisor in Interlock.Access];
  ProgrammerLabel.Color := AColor[AccessLevelProgrammer in Interlock.Access];
  MaintenanceLabel.Color := AColor[AccessLevelMaintenance in Interlock.Access];
  AdministratorLabel.Color := AColor[AccessLevelAdministrator in Interlock.Access];
  OEM1Label.Color := AColor[AccessLevelOEM1 in Interlock.Access];

  if cstMotion in Interlock.Inclusion then
    InMotionLabel.Color := AColor[True]
  else if cstMotion in Interlock.Exclusion then
    InMotionLabel.Color := AColor[False]
  else
    InMotionLabel.Color := clSilver;

  if cstCycle in Interlock.Inclusion then
    InCycleLabel.Color := AColor[True]
  else if cstCycle in Interlock.Exclusion then
    InCycleLabel.Color := AColor[False]
  else
    InCycleLabel.Color := clSilver;

  if cstHomed in Interlock.Inclusion then
    HomedLabel.Color := AColor[True]
  else if cstHomed in Interlock.Exclusion then
    HomedLabel.Color := AColor[False]
  else
    HomedLabel.Color := clSilver;

  if cstEditing in Interlock.Inclusion then
    EditingLabel.Color := AColor[True]
  else if cstEditing in Interlock.Exclusion then
    EditingLabel.Color := AColor[False]
  else
    EditingLabel.Color := clSilver;

  ActiveLabel.Color := AColor[Interlock.Active];
  if Interlock.Active then begin
    ActiveSpeedBtn.Down := True;
    ActiveLabel.Caption := ActiveLang;
  end else begin
    ActiveLabel.Caption := InactiveLang;
    ActiveSpeedBtn.Down := False;
  end;

end;

end.
