unit DT3155;

interface

uses
  Windows, SysUtils, Classes, Graphics, Controls, CNC32, CNCTypes, CoreCNC,
  CNCAbs;

type
  TDLL_INIT_GRABBER = function(HWnd: Integer; HDC: Integer): Integer; stdcall;
  TDLL_CLOSE_GRABBER = function(HWnd: Integer; HDC: Integer): Integer; stdcall;
  TDLL_GRAB = function(HWnd: Integer; Target_DC: Integer; var extra: Integer; var centroid_Y: Integer; var min_rad: Integer; var centroid_X: Integer): Integer; stdcall;
  TDLL_START_PASSTHRU_XY = function(HWnd, X, Y: Integer): Integer; stdcall;
  TDLL_END_PASSTHRU = function: Integer; stdcall;
  TDLL_Radius = function: Integer; stdcall;
  TDLL_X = function: Integer; stdcall;
  TDLL_Y = function: Integer; stdcall;
  TDLL_roi = function(Width, Height, rad2: Integer): Integer; stdcall;
  TDLL_SetVars = function(thresh_percent: Integer; hole_negative: Integer): Integer; stdcall;
  TDLL_ExtendedVideoFunction = function(Hwnd: Integer; N, V, V2: PChar): Integer; stdcall;

  TWGTGrabberDll = class(TExpandDisplay)
  private
    Failed: Boolean;
    Blocking: Boolean;

    DllInitGrabber: TDLL_INIT_GRABBER;
    DllCloseGrabber: TDLL_CLOSE_GRABBER;
    //Grab: TDLL_GRAB;
    DllStart: TDLL_START_PASSTHRU_XY;
    DllStop: TDLL_END_PASSTHRU;
    //Radius: TDLL_Radius;
    //X: TDLL_X;
    //Y: TDLL_Y;
    //Roi: TDLL_roi;
    //SetVars: TDLL_SetVars;
    DllExtendedFunction: TDLL_ExtendedVideoFunction;

    HMod: Integer;

    StartTag: TAbstractTag;
    StopTag: TAbstractTag;
    CameraRunning: TAbstractTAg;
    PT_Started: Boolean;

    XPosTag: TAbstractTag;
    YPosTag: TAbstractTag;
    ExtTag: TAbstractTag;

    Open: Boolean;
    procedure Callback(ATag: TAbstractTag);
    function GetDontRun: Boolean;
  public
    constructor Create(AOwner: TComponent); override;
    procedure Resize; override;
    procedure InstallComponent(Params: TParamStrings); override;
    procedure Installed; override;
    procedure IShutdown; override;
    procedure Start;
    procedure Stop;
    property DontRun: Boolean read GetDontRun;
  end;

implementation

uses
  Tokenizer;

procedure TWGTGrabberDll.Callback(ATag: TAbstractTag);
var Tmp, N, V1, V2: string;
    Ret: Integer;
begin
  if ATag.AsBoolean then begin
    if ATag = StartTag then
      Start
    else if ATag = StopTag then
      Stop
  end;

  if (ATag = XPosTag) or (ATag = YPosTag) then begin
    if PT_Started then begin
      Stop;
      Start;
    end;
  end;

  if ATag = ExtTag then begin
    Tmp:= ATag.AsString;
    N:= CncTypes.ReadDelimited(Tmp, ' ');
    V1:= CncTypes.ReadDelimited(Tmp, ' ');
    V2:= CncTypes.ReadDelimited(Tmp, ' ');

    if (Assigned(Self.DllExtendedFunction)) then begin
      Ret:= DllExtendedFunction(Handle, PChar(N), PChar(V1), PChar(V2));
      EventLog('DLL_Extended_Video_Function: ' + IntToStr(Ret));
    end;
  end;
end;

procedure TWGTGrabberDll.InstallComponent(Params: TParamStrings);
begin
  inherited;

  if not Failed then begin
    CameraRunning:= TagPublish('CameraRunning', TIntegerTag);
  end;
end;

procedure TWGTGrabberDll.Installed;
begin
  inherited;
  if not Failed then begin
    StartTag:=  TagEvent('CameraStart', EdgeChange, TIntegerTag, Callback);
    StopTag:=  TagEvent('CameraStop', EdgeChange, TIntegerTag, Callback);
    XPosTag:= TagEvent('CameraCrossX', EdgeChange, TIntegerTag, Callback);
    YPosTag:= TagEvent('CameraCrossY', EdgeChange, TIntegerTag, Callback);
    ExtTag:= TagEvent('CameraExtendedFunction', EdgeChange, TStringTag, Callback);

    StopTag.IsVCL:= False;
    StartTag.IsVCL:= False;
    XPosTag.IsVCL:= False;
    YPosTag.IsVCL:= False;
    ExtTag.IsVCL:= False;
  end;
  Blocking:= False;
end;

constructor TWGTGrabberDll.Create(AOwner: TComponent);

  function GetProcAddressX(HMod: Integer; AName: PChar): Pointer;
  begin
    Result:= GetProcAddress(HMod, AName);
    if not Assigned(Result) then begin
      EventLog(Format('Interface "%s" not present in "%s"', [AName, 'wgtgrabber.dll']));
      raise Exception.Create('Interface not present in WGTGrabber.dll: ' + AName);
    end;
  end;

begin
  inherited;

  Blocking:= True;
  HMod := LoadLibrary(PChar(ProfilePath + 'lib\wgtgrabber.dll'));

  if HMod = 0 then begin
    EventLog('******* Unable to open WGTGrabber.dll');
    Failed:= True;
    Exit;
  end;

  DllInitGrabber := GetProcAddressX(HMod, 'DLL_INIT_GRABBER');
  DllCloseGrabber := GetProcAddressX(HMod, 'DLL_CLOSE_GRABBER');
  //Grab := GetProcAddressX(HMod, 'DLL_GRAB');
  DllStart := GetProcAddressX(HMod, 'DLL_START_PASSTHRU_XY');
  DllStop := GetProcAddressX(HMod, 'DLL_END_PASSTHRU');
  //Radius := GetProcAddressX(HMod, 'DLL_Radius');
  //X := GetProcAddressX(HMod, 'DLL_X');
  //Y := GetProcAddressX(HMod, 'DLL_Y');
  //Roi := GetProcAddressX(HMod, 'DLL_roi');
  //SetVars := GetProcAddressX(HMod, 'DLL_SetVars');
  DllExtendedFunction:= GetProcAddress(HMod, 'DLL_EXTENDED_VIDEO_FUNCTION');
  if (not Assigned(DllExtendedFunction)) then
    EventLog('Dll does not support: ' + 'DLL_EXTENDED_VIDEO_FUNCTION');

  EventLog('******* Exiting create');
end;

procedure TWGTGrabberDll.Resize;
begin
  if not DontRun then begin
    if Open then begin
      Stop;
      DllCloseGrabber(Self.Handle, Canvas.Handle);
    end;
    DllInitGrabber(Self.Handle, Canvas.Handle);
    Open:= True;

    Start;
  end;
end;

procedure TWGTGrabberDll.Start;
begin
  if not PT_Started then begin
    DllStart(Handle, XPosTag.AsInteger, YPosTag.AsInteger);
    PT_Started:= True;
    CameraRunning.AsBoolean := PT_Started;
  end;
end;

procedure TWGTGrabberDll.Stop;
begin
  if PT_Started then begin
    DllStop;
    PT_Started:= False;
    CameraRunning.AsBoolean := PT_Started;
  end;
end;

function TWGTGrabberDll.GetDontRun: Boolean;
begin
  Result:= Blocking or Failed;
end;

procedure TWGTGrabberDll.IShutdown;
begin
  if not DontRun then begin
    if Open then begin
      Stop;
      DllCloseGrabber(Self.Handle, Canvas.Handle);
    end;
  end;
  inherited;
end;

initialization
  CoreCNC.RegisterCNCClass(TWGTGrabberDll);
end.

