object FileListForm: TFileListForm
  Left = 475
  Top = 302
  BorderStyle = bsDialog
  Caption = 'Dialog'
  ClientHeight = 297
  ClientWidth = 324
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = True
  Position = poScreenCenter
  OnCloseQuery = FormCloseQuery
  PixelsPerInch = 96
  TextHeight = 16
  object GroupBox1: TGroupBox
    Left = 0
    Top = 0
    Width = 324
    Height = 224
    Align = alClient
    Caption = 'File List'
    TabOrder = 0
    object FileList: TFileListBox
      Left = 2
      Top = 18
      Width = 320
      Height = 204
      Align = alClient
      ExtendedSelect = False
      ItemHeight = 16
      TabOrder = 0
      OnClick = FileListClick
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 224
    Width = 324
    Height = 73
    Align = alBottom
    TabOrder = 1
    object EditLabel: TLabel
      Left = 25
      Top = 18
      Width = 56
      Height = 16
      Caption = '&Filename'
      FocusControl = FilenameEdit
    end
    object FilenameEdit: TEdit
      Left = 90
      Top = 10
      Width = 150
      Height = 24
      TabOrder = 0
      Text = '*.*'
    end
    object OKBtn: TButton
      Left = 13
      Top = 39
      Width = 76
      Height = 24
      Caption = 'OK'
      Default = True
      ModalResult = 1
      TabOrder = 1
    end
    object CancelBtn: TButton
      Left = 107
      Top = 39
      Width = 75
      Height = 24
      Cancel = True
      Caption = 'Cancel'
      ModalResult = 2
      TabOrder = 2
    end
  end
end
