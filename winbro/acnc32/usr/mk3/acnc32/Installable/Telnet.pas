unit Telnet;

interface

uses
  SysUtils, Classes, IdGlobal, IdTCPServer, IdSocketHandle, CoreCNC, CNC32, CNCTypes,
  FaultRegistration, IdContext, IdTCPConnection, Windows, Messages;

type
  TSpoolThread = class(TThread)
  private
    procedure Spool(const NetName, Value: string);
  public
    procedure Execute; override;
  end;

  TAcnc32Telnet = class(TCNC)
  private
    IdTCPServer: TIdTCPServer;
    ConnectCountTag: TAbstractTag;
    DefaultPort: Integer;
    FuncWnd: HWND;
    ProgramName: string;

    procedure FuncProc(var aMessage : TMessage);
    procedure IdTCPServerConnect(AContext: TIdContext);
    procedure IdTCPServerExecute(AContext:TIdContext);
    procedure IdTCPServerDisconnect(AContext:TIdContext);
    procedure WriteHelp(Connection : TIdTCPConnection);
    function  TransportString(const Raw: string) : string;
  public
    constructor Create(AOwner: TComponent); override;
    procedure Installed; override;
    procedure InstallComponent(Params : TParamStrings); override;
    procedure CheckInstallation; override;
  end;

implementation

{ TAcnc32Telnet }

const DefaultPrompt = '%Acnc32:';
      LF = #$d + #$a;

constructor TAcnc32Telnet.Create(AOwner: TComponent);
begin
  inherited;
  IdTCPServer:= TIdTCPServer.Create(Self);
  DefaultPort:= 23;
  IdTCPServer.OnConnect := IdTCPServerConnect;
  IdTCPServer.OnExecute := IdTCPServerExecute;
  IdTCPServer.OnDisconnect := IdTCPServerDisconnect;
  FuncWnd := Classes.AllocateHwnd(FuncProc);
end;

procedure TAcnc32Telnet.FuncProc(var aMessage : TMessage);
begin
  if aMessage.Msg = CNCM_LOCAL_EVENT then
    CoreCnc.SysObj.FileManager.LoadActiveFile(ProgramName);
end;

procedure TAcnc32Telnet.IdTCPServerConnect(AContext:TIdContext);
begin
  with AContext.Connection do begin
    WriteHelp(AContext.Connection);
    IOHandler.Write(DefaultPrompt);
    ConnectCountTag.AsInteger:= ConnectCountTag.AsInteger + 1;
  end;
end;

procedure TAcnc32Telnet.IdTCPServerDisconnect(AContext:TIdContext);
begin
  ConnectCountTag.AsInteger:= ConnectCountTag.AsInteger - 1;
end;

procedure TAcnc32Telnet.WriteHelp(Connection : TIdTCPConnection);
begin
  with Connection.IOHandler do begin
    Write('=Welcome to the Acnc32 terminal interface.' + LF);
    Write('=Commands Understood -' + LF);
//    Write('MODE <OPC | RAW> "Sets the current mode' + LF);
    Write('=GET <name>        Gets the current value of name' + LF);
    Write('=SET <name, value> Sets (if possible) name=value' + LF);
    Write('=ADD <name>        Adds the name to the current list' + LF);
    Write('=SUB <name>        Removes the name from the current list' + LF);
    Write('=LIST [ALL]        Lists name value pairs in the current list' + LF);
    Write('=TAGTYPE <name>    Returns TagType=<name> <tagtype>' + LF);
    Write('=MANIFEST          List all name value pairs' + LF);
    Write('=NOTIFY            Interlock error message' + LF);
    Write('=ERROR             Reset rewind error message' + LF);
    Write('=LIST              Lists name value pairs in the current list' + LF);
    Write('=PROMPT            Sets the system prompt' + LF);
    Write('=QUIT | BYE        Terminates the session' + LF);
    Write('=Any other strings will be returned "ERROR <command>"' + LF);
  end;
end;

function TAcnc32Telnet.TransportString(const Raw: string) : string;
var I : Integer;
begin
  Result := '';
  for I := 1 to Length(Raw) do begin
    case Raw[I] of
      #0..#31, '=', '%': Result := Result + '%' + IntToHex(Byte(Raw[I]), 2);
    else
      Result := Result + Raw[I];
    end;
  end;
end;

function ReadEscape(EscapeText : string) : Char;
var Tmp : Byte;
begin
  LowerCase(EscapeText);
  if Length(EscapeText) >= 3 then begin
    case EscapeText[2] of
      '0'..'9' : Tmp := (Byte(EscapeText[2]) - Byte('0')) * 16;
      'A'..'F' : Tmp := (Byte(EscapeText[2]) - Byte('A') + 10) * 16;
      'a'..'f' : Tmp := (Byte(EscapeText[2]) - Byte('a') + 10) * 16;
    else
      Tmp := 0;
    end;

    case EscapeText[3] of
      '0'..'9' : Tmp := Tmp + Byte(EscapeText[3]) - Byte('0');
      'A'..'F' : Tmp := Tmp + Byte(EscapeText[3]) - Byte('A') + 10;
      'a'..'f' : Tmp := Tmp + Byte(EscapeText[3]) - Byte('a') + 10;
    else
      Tmp := 0;
    end;

    if Tmp = 0 then
      Result := ' '
    else
      Result := Char(Tmp);
  end else begin
    Result := '%';
  end;
end;

function ParseMessage(const Msg : string) : string;
var I: Integer;
    Tmp: string;
begin
  // Remove Terminal escape triplets
  I := 1;
  Tmp := Msg;
  Result := '';
  while I <= Length(Tmp) do begin
    case Byte(Tmp[I]) of
      240..255: begin
        Inc(I, 3);
      end;
    else
      Result := Result + Tmp[I];
      Inc(I);
    end;
  end;


  // Remove backspace #$8
  I := 1;
  Tmp := Result;
  Result := '';
  while I <= Length(Tmp) do begin
    case Byte(Tmp[I]) of
      $8: begin  // Backspace
        if Length(Result) > 0 then begin
          System.Delete(Result, Length(Result), 1);
        end;
        Inc(I);
      end;
    else
      Result := Result + Tmp[I];
      Inc(I);
    end;
  end;


  // Convert % escape sequence
  I := 1;
  Tmp := Result;
  Result := '';
  while I <= Length(Tmp) do begin
    case Byte(Tmp[I]) of
      Byte('%') : begin // two digit hex escape
        Result := Result + ReadEscape(Copy(Tmp, I, 3));
        Inc(I, 3);
      end;
    else
      Result := Result + Tmp[I];
      Inc(I);
    end;
  end;
end;


type
  TTelnetMode = (
    tmSU,
    tmOPC
  );

  TTelnetModes = set of TTelnetMode;

//
//  Execute method for Telnet, each client connection exists as a different
//  instance of the calling thread, all the variable sit on the stack and
//  so multiple clients can connect.
//
//  Its fairly easy to determine from the main loop of this routine whats going
//  on.  The tricky bit is that there are two modes of operation, OPC and RAW
//  In OPC mode the client is restricted to the variables declared in OPC.cfg
//  In RAW mode the entire named layer is available.
//
procedure TAcnc32Telnet.IdTCPServerExecute(AContext: TIdContext);
var TagList : TStringList;
    TagValueList : TStringList;
    Modes : TTelnetModes;
    Prompt: string;

  procedure TagListClear;
  begin
    TagList.Clear;
    TagValueList.Clear;
  end;

  procedure TagListDelete(Index: Integer);
  begin
    TagList.Delete(Index);
    TagValueList.Delete(Index);
  end;

  procedure TagListAdd(Tag: TAbstractTag);
  begin
    if tmOPC in Modes then
      TagList.AddObject(Tag.NetWorkAlias, Tag)
    else
      TagList.AddObject(Tag.Name, Tag);
    TagValueList.Add(''); //Tag.AsString
  end;

  procedure NameValuePair(const Name: string; Tag: TAbstractTag);
  begin
    if Assigned(Tag) then begin
      if tmOPC in Modes then
        AContext.Connection.IOHandler.Write(Tag.NetWorkAlias + '=' + TransportString(Tag.AsString) + LF)
      else
        AContext.Connection.IOHandler.Write(Tag.Name + '=' + TransportString(Tag.AsString) + LF);
    end else begin
      AContext.Connection.IOHandler.Write(Name + '=Unknown Tag' + LF);
    end;
  end;

  function FindTag(const Name : string) : TAbstractTag;
  var OPCTag: TOPCTagRecord;
      I : Integer;
  begin
    Result := nil;
    if tmOPC in Modes then begin
      if SysObj.Comms.FOPCList.Find(LowerCase(Name), I) then begin
        OPCTag := TOPCTagRecord(SysObj.Comms.FOPCList.Objects[I]);
        if Assigned(OPCTag) then
          Result := OPCTag.Tag;
      end;
    end else begin
      Result := SysObj.Comms.FindTag(Name);
    end;
  end;

  procedure SetTelnetMode(const Mode : string);
  begin
    if (Mode = 'opc') and (Prompt <> '') then begin
      if not (tmOPC in Modes) then begin
        AContext.Connection.IOHandler.Write(Name + '=Mode now OPC, current update list discarded' + LF);
        TagListClear;
      end;
      Include(Modes, tmOPC);
    end else if Mode = 'raw' then begin
      if (tmOPC in Modes) and (Prompt <> '') then begin
        AContext.Connection.IOHandler.Write(Name + '=Mode now RAW, current update list discarded' + LF);
        TagListClear;
      end;
      Exclude(Modes, tmOPC);
    end else if Mode = 'su' then begin
      Include(Modes, tmSU);
      AContext.Connection.IOHandler.Write(Name + '=Mode now SU' + LF);
    end else if Mode = 'user' then begin
      Exclude(Modes, tmSU);
      AContext.Connection.IOHandler.Write(Name + '=Mode now USER' + LF);
    end else begin
      if tmOPC in Modes then
        AContext.Connection.IOHandler.Write('=Current mode is OPC' + LF)
      else
        AContext.Connection.IOHandler.Write('=Current mode is RAW' + LF);
    end;
  end;

  procedure GetValue(const Name : string);
  var Tag: TAbstractTag;
  begin
    Tag := FindTag(Name);
    NameValuePair(Name, Tag);
  end;

  procedure SetValue(Comb : string);
  var Tag: TAbstractTag;
      Name, Value : string;
  begin
    Name := ReadDelimited(Comb, ' ');
    Value := Trim(Comb);

    if Name <> 'telnetmode' then begin
      Tag := FindTag(Name);
      if Assigned(Tag) then begin
        //if not Tag.IsOwned or (tmSU in Modes) then begin
          Tag.AsString := Value;
        //end;
      end;
      NameValuePair(Name, Tag);
    end else begin
      SetTelnetMode(Value);
    end;
  end;

  procedure AddName(const Name : string);
  var Tag: TAbstractTag;
  begin
    Tag := FindTag(Name);
    if Assigned(Tag) then
      TagListAdd(Tag)
    else
      AContext.Connection.IOHandler.Write(Name + ': Not found' + LF);
  end;

  procedure SubName(const Name: string);
  var I: Integer;
  begin
    I := TagList.IndexOf(LowerCase(Name));
    if I <> -1 then
      TagListDelete(I);
  end;

  procedure ReturnTagType(const Name: string);
  var Tag: TAbstractTag;
  begin
    Tag := FindTag(Name);
    if Assigned(Tag) then
      AContext.Connection.IOHandler.Write('tagtype=' + Name + ' ' + Tag.ClassName + LF)
    else
      AContext.Connection.IOHandler.Write('tagtype=error ' + Name + LF);
  end;

  procedure ListNames(const Verb: string);
  var I : Integer;
      Tag : TAbstractTag;
      Tmp : string;
  begin
    for I := 0 to TagList.Count - 1 do begin
      Tag := TAbstractTag(TagList.Objects[I]);
      Tmp := Tag.AsString;
      if Verb = 'all' then begin
        TagValueList[I] := Tmp;
        NameValuePair(TagList[I], Tag);
      end else begin
        if TagValueList[I] <> Tmp then begin
          TagValueList[I] := Tmp;
          NameValuePair(TagList[I], Tag);
        end;
      end;
    end;
  end;

  procedure Manifest;
  var OPCTag: TOPCTagRecord;
      I: Integer;
  begin
    with SysObj.Comms do begin
      if tmOPC in Modes then begin
        AContext.Connection.IOHandler.Write('manifest=' + IntToStr(FOPCList.Count) + LF);
        for I := 0 to FOPCList.Count - 1 do begin
          OPCTag := TOPCTagRecord(FOPCList.Objects[I]);
          NameValuePair(OPCTag.NetAlias, OPCTag.Tag);
        end;
      end else begin
        AContext.Connection.IOHandler.Write('manifest=' + IntToStr(NamedSize) + LF);
        for I := 0 to NamedSize - 1 do begin
          NameValuePair(NamedLayer.Tag[I].Name, NamedLayer.Tag[I]);
        end;
      end;
    end;
  end;

  procedure InterlockMessage(Msg : string);
  begin
    SysObj.FaultInterface.SetFault(Self, CoreFaults[cfTelnetNotify], [Msg], nil);
  end;

  procedure ErrorMessage(Msg : string);
  begin
    SysObj.FaultInterface.SetFault(Self, CoreFaults[cfTelnetError], [Msg], nil);
  end;

  procedure RxProgram(Msg: string);
  var ProgramName: string;
      Size: Integer;
      Buffer: TIdBytes;
      ProgramFile : File ;
  begin
    try
      Size:= StrToInt(ReadDelimited(Msg, ' '));
      if(Size < 2E7) then begin
        ProgramName := ReadDelimited(Msg, ' ');

        AContext.Connection.IOHandler.Write('sendprogram=' + ProgramName + LF);
        SetLength(Buffer, Size);
        try
          try
            AContext.Connection.IOHandler.ReadBytes(Buffer, Size, False);

            AssignFile(ProgramFile, LocalPath + ProgramName);
            ReWrite(ProgramFile, 1);   // Define a single 'record' as 4 bytes
            BlockWrite(ProgramFile, Buffer[0], Size);
            CloseFile(ProgramFile);
            AContext.Connection.IOHandler.Write('sendcomplete=ok'+ LF);

          except
            CoreCnc.MessageLog('Failed to receive file ' + ProgramName);
          end;
        finally
          SetLength(Buffer, 0);
        end;
      end else begin
        AContext.Connection.IOHandler.Write('sendprogram=errortoobig' + LF);
      end;

    except
      AContext.Connection.IOHandler.Write('sendprogram=error' + LF);
    end;
  end;

  procedure MakeProgramActive(Msg: string);
  begin
    ProgramName:= LocalPath + Trim(Msg);
    SendMessage(FuncWnd, CNCM_LOCAL_EVENT, 0, 0);
    AContext.Connection.IOHandler.Write('makeactive=1' + LF);
  end;

var Msg, ThisMessage, Noun : string;
begin
  TagList := TStringList.Create;
  TagValueList := TStringList.Create;
  Prompt := DefaultPrompt;
  Modes := [tmOPC];

  try
    while AContext.Connection.IOHandler.Connected do begin
      ThisMessage := AContext.Connection.IOHandler.ReadLn;
      Msg := ParseMessage(ThisMessage);
      Noun := LowerCase(Trim(ReadDelimited(Msg, ' ')));
      if Noun = 'get' then
        GetValue(Msg)
      else if Noun = 'set' then
        SetValue(Msg)
      else if Noun = 'add' then
        AddName(Msg)
      else if Noun = 'sub' then
        SubName(Msg)
      else if Noun = 'tagtype' then
        ReturnTagType(Msg)
      else if Noun = 'list' then
        ListNames(Msg)
      else if Noun = 'manifest' then
        Manifest
      else if Noun = 'notify' then
        InterlockMessage(Msg)
      else if Noun = 'error' then
        ErrorMessage(Msg)
      else if Noun = 'prompt' then
        Prompt := Msg
      else if Noun = '?' then
        WriteHelp(AContext.Connection)
      else if (Noun = 'quit') or (Noun = 'bye') then
        AContext.Connection.Disconnect
      else if Noun = 'sendprogram' then
        RxProgram(Msg)
      else if Noun = 'makeactive' then
        MakeProgramActive(Msg)
      else if Noun <> '' then
        AContext.Connection.IOHandler.Write('ERROR="' + ThisMessage + '"' + LF);

      AContext.Connection.IOHandler.Write(Prompt);
    end;
  finally
    TagList.Free;
    TagValueList.Free;
  end;
end;

procedure TAcnc32Telnet.InstallComponent(Params: TParamStrings);
begin
  inherited;
  DefaultPort:= Params.ReadInteger('port', 23);
  ConnectCountTag:= Self.TagPublish('TelnetConnectCount', TIntegerTag)
end;

procedure TAcnc32Telnet.Installed;
begin
  inherited;
  IdTCPServer.DefaultPort := DefaultPort;
end;

procedure TAcnc32Telnet.CheckInstallation;
begin
  inherited;
  IdTCPServer.Active:= True;
end;

{ TSpoolThread }

procedure TSpoolThread.Execute;
var I: Integer;
    Tags: TStringList;
    Tag: TAbstractTag;
    TagValue: string;
begin
  Tags:= TStringList.Create;
  for I := 0 to SysObj.Comms.FOPCList.Count - 1 do begin
    Tag:= TAbstractTag(SysObj.Comms.FOPCList.Objects[I]);
    Tags.AddObject('', Tag);
  end;

  while not Terminated do begin
    Sleep(2000);
    for I := 0 to Tags.Count - 1 do begin
      Tag:= TAbstractTag(Tags.Objects[I]);
      TagValue:= Tag.AsString;
      if TagValue <> Tags[I] then begin
        Tags[I]:= TagValue;
        Spool(Tag.NetWorkAlias, TagValue);
      end;
    end;
  end;
end;

procedure TSpoolThread.Spool(const NetName, Value: string);
begin

end;

initialization
  RegisterCNCClass(TAcnc32Telnet);
end.
