object CalibrationGraphForm: TCalibrationGraphForm
  Left = 0
  Top = 0
  BorderStyle = bsNone
  ClientHeight = 350
  ClientWidth = 767
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Chart1: TChart
    Left = 0
    Top = 0
    Width = 767
    Height = 280
    BackWall.Brush.Color = clWhite
    BackWall.Brush.Style = bsClear
    Gradient.EndColor = 8454143
    Gradient.Visible = True
    Title.Text.Strings = (
      'Calibration Data')
    View3D = False
    Align = alClient
    TabOrder = 0
    object Series1: TLineSeries
      Marks.ArrowLength = 8
      Marks.Visible = False
      SeriesColor = clRed
      Pointer.InflateMargins = True
      Pointer.Style = psRectangle
      Pointer.Visible = False
      XValues.DateTime = False
      XValues.Name = 'X'
      XValues.Multiplier = 1.000000000000000000
      XValues.Order = loAscending
      YValues.DateTime = False
      YValues.Name = 'Y'
      YValues.Multiplier = 1.000000000000000000
      YValues.Order = loNone
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 280
    Width = 767
    Height = 70
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    object BottomSelectPanel: TPanel
      Left = 0
      Top = 35
      Width = 767
      Height = 35
      Align = alBottom
      BevelInner = bvLowered
      TabOrder = 0
    end
    object TopSelectPanel: TPanel
      Left = 0
      Top = 0
      Width = 767
      Height = 35
      Align = alClient
      BevelInner = bvLowered
      TabOrder = 1
    end
  end
end
