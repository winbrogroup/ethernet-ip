unit CalDataUtils;

interface
uses SysUtils;

implementation

function SlashSep(const Path, S: String): String;
begin
  if AnsiLastChar(Path)^ <> '\' then
    Result := Path + '\' + S
  else
    Result := Path + S;
end;

function NextToken(var InStr : String):String;
var
CommaPos : Integer;
begin
Result := '';
CommaPos := Pos(',',InStr);
if CommaPos > 0 then
  begin
  Result := Trim(Copy(InStr,1,CommaPos-1));
  InStr := Copy(Instr,CommaPos+1,1000);
  end
else
  begin
  Result := Instr;
  Instr := '';
  end;
end;


end.
