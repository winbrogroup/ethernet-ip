unit CalDataLists;

interface
uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls,IniFiles,Grids;
type
TInputDataListChangeEvent = procedure(ZShift : Double; Valid : Boolean) of object;
TOutputDataListChangeEvent = procedure(Valid : Boolean) of object;
TIndexedInputDataChangeEvent = procedure(DataIndex: Integer;ZShift : Double; Valid : Boolean) of object;
TIndexedOutputDataChangeEvent = procedure(DataIndex: Integer;Valid : Boolean) of object;
TDataEntryChangedEvent = procedure(Acol : Integer;ARow : Integer;StrVal : String; FloatVal : Double) of object;
TAvailabityandStatusData = Class;
TInputDataGrid = class;

TCompPowerupModes = (cpuUnknown,cpuDetermineState,
                     cpuBlankFirstStart,cpuNonBlankFirstStart,
                     cpuBlankNoId,cpuNonBlankNoId,
                     cpuImportRestartWorkInProgress,
                     cpuCalibrateRestartWorkInProgress,
                     cpuRegisteredButNotRestarted,
                     cpuFullyRegistered);

TFirstPowerupModes = (tfmUnresolved,tfmFromBlank,tfmFromImport);

TDefaultSettingEntry = Class(TObject)
  AxisName : String;
  MotorNumber : Integer;
  IsLinear : Boolean;
  FirstEntryPos : Double;
  EncoderCountsPerUnit : Double;
  Step : Double;
  NumEntries : Integer;
  DoublePointsOnOutput : Boolean;
  UseBackLashTable     : Boolean;
  end;

TDefaultSettingsList = class(TList)
 public
 AxisListString : String;
 procedure Clean;
 function EntryForAxisName(AxName : String):TDefaultSettingEntry;
 Function LoadFromFile(FileName: String): String;
 end;

TNCConfigDataRecord = Class(TObject)
  MotorNumber : Integer;
  AxisLetter  : String;
  EncoderCountsPerUnit : Double;
  end;

TNCConfigList = class(TList)
  private
    function GetIndexofNextTarget(Target : String;
                                  var StartIndex : Integer ;
                                  SList : TStringList;
                                  TerminateAt : String;
                                  var FoundLineText: String) :Boolean;

    function ReadEquateValue(InputLine : String):String;
  public
    function ReadFromDirectory(DirName : String): Integer;
    procedure Clean;
  end;

TInputDataRecord = Class(TObject)
  Position                : Double;
  InputForwardError       : Double;
  InputForwardAdjust      : Double;
  InputReverseError       : Double;
  InputReverseAdjust      : Double;
  ZeroShiftedForwardData  : Double;
  ZeroShiftedReverseData  : Double;
  procedure AssignFrom(ExtEntry : TInputDataRecord);
  end;

TAdjustData = Class(TObject)
  InputForwardAdjust      : Double;
  InputReverseAdjust      : Double;
  function AsString : String;
  function FromString(InStr : String): Boolean;
  end;

TInputDataList= class(TList)
  private
    FZShift: Double;
    FOnInputDataListUpdated: TInputDataListChangeEvent;
    FADjustWidth: Integer;
    FStandardWidth: Integer;
    FPosLimit : Double;
    FNegLimit : Double;
    function CalcultateZeroShiftForHomeOffsetOf(HOffset: Double;var CalculatedShift : Double): Boolean;
    procedure SetNegLimit(const Value: Double);
    procedure SetPosLimit(const Value: Double);
  public
    ListIndex : Integer;
    AxisLetter : String;
    function CompareTo(ExtDataList :TInputDataList): Boolean;
    procedure CopyTo(ExtDataList :TInputDataList);
    function UpdateShiftedDataForHomeOffset(HOffset: Double): Boolean;
    function ShiftedCompAtPosition(Posn : Double;Var FwdCompResult : Double;Var RevCompResult : Double):Boolean;
    property CalculatedZeroShift : Double Read FZShift;
    procedure Clean;
    procedure aaInitialise(First : Double;Step : Double;NumEntries : INteger);
    property OnUpdated : TInputDataListChangeEvent read FOnInputDataListUpdated write FOnInputDataListUpdated;
    property NegLimit : Double read FNegLimit write SetNegLimit;
    property PosLimit : Double read FPosLimit write SetPosLimit;
end;


 TOutputDataRecord = Class(TObject)
  private
  public
    ActualPosition        : Double;
    EncoderCounts         : Double;
    CompValue             : Double;
    RevCompValue          : Double;
    Backlash              : Double;
    function AsCompText(CompMultiplier : Double) : String;
    function AsBackLashText(Offset : Double;CompMultiplier : Double): String;
  end;

TOutputDataList= class(TList)
  private
    FZShift: Double;
    FEncoderSpan : Integer;
    FOnOutputDataListUpdated: TOutputDataListChangeEvent;
    FMotorNumber : Integer;
    FPosLimit : Double;
    FNegLimit : Double;
    FIsLinear: Boolean;
    procedure SetEncCountsPerMM(const Value: Double);
    procedure SetAxisLetter(const Value: String);
    function GetEncoderCountSpan: Integer;
    procedure SetMotorNumber(const Value: Integer);
    function GetMotorNumber: Integer;
    procedure SetNegLimit(const Value: Double);
    procedure SetPosLimit(const Value: Double);
  public
    ListIndex : Integer;
    FAxisLetter : String;
    FEncCountsPerMM : Double;
    aaHomeOffset : Double;
    BacklashAtEncoderZero : Double;
    DoublePOints : Boolean;
    constructor Create;
    procedure Clean;
    function UpdateFromInputDataList(InList : TInputDataList) : Boolean;
    property OnUpdated : TOutputDataListChangeEvent read FOnOutputDataListUpdated write FOnOutputDataListUpdated;
    property aEncCountsPerMM : Double read FEncCountsPerMM write SetEncCountsPerMM;
    property AxisLetter : String read FAxisLetter write SetAxisLetter;
    property MotorNumber : Integer read GetMotorNumber write SetMotorNumber;
    property EncoderCountSpan : Integer read GetEncoderCountSpan;
    property aaPosLimit : Double read FPosLimit write SetPosLimit;
    property aaNegLimit : Double read FNegLimit write SetNegLimit;
    property IsLinearAxis : Boolean read FIsLinear write FIsLinear;
  end;

TFwdRevDataRecord = Class(TObject)
  OutputCompValueEncoderUnits   : Double;
  CompValueRealUnits            : Double;
  CompValueShiftedRealUnits     : Double;
  Position                      : Double;
  EncoderCounts                 : Integer;
  end;


// One Per Axis of these CONTAINED WITHIN TSINGLEAXISDATA
TSingleAxisCompDataLists = class(TObject)
  private
    FBackLashUsed: Boolean;
    FFirstPosition: Double;
    FNumberOfEntries: Integer;
    FCompIncrement    : Double;
    FCompHomeOffset   : Double;
    FManualBacklashMicron  : Integer;
    FaxisIsLinear    : Boolean;
    FAxisLetter: String;
    FEncodeCountsPerUnit : Double;
    EncoderOffsetValid : Boolean;
    RealUnitsDataShift : Double;
    DataTouched : Boolean;
    FOnInputDataUpdated: TInputDataListChangeEvent;
    FOnOutputDataUpdated: TOutputDataListChangeEvent;
    FDoublePoints: Boolean;
    LazyOutputUpdateRequired : Boolean;
    FMakeOutput: Boolean;
    FManualBacklashAdjust: Integer;
    function  GetCalculatedShift(var CompShift : Double): Boolean;
    procedure SetHomeOffset(const Value: Double);
    procedure InDataUpdated(ZShift : Double; Valid : Boolean);
    procedure OutDataUpdated(Valid : Boolean);
    procedure SetEncodeCountsPerUnit(const Value: Double);
    procedure SetDoublePOints(const Value: Boolean);
    procedure SetAxisLetter(const Value: String);
    procedure SetMotorNumber(const Value: INteger);
    function  GetMotorNumber: INteger;
    procedure SetBackLashUsed(const Value: Boolean);
    function  IntReadFromWorkingFile(FileName : String):Boolean;
    procedure SetNumberEntries(const Value: Integer);
    procedure SetFirstPosition(const Value: Double);
    procedure SetCompIncrement(const Value: Double);
    function  GetCompNegativeLimit: Double;
    function  GetCompPositiveLimit: Double;
    procedure UpdateListLimits;
    procedure SetManualBacklash(const Value: Integer);
    procedure SetManualBacklashAdjust(const Value: Integer);
    procedure SetAxisIsLinear(const Value: Boolean);

   public
   CurrentInputDataList : TInputDataList;
   OutputDataList : TOutputDataList;
   constructor Create;
   destructor  Destroy; override;
   procedure   Clean;
   procedure   WriteToFile(FileName : String);
   function    ReadFromExportFile(FileName : String):Boolean;
   procedure   AddInputEntry(Pos,Fwd,Rev : Double);


   //  Calculations
   function UpdateInputDataForHomeOffsetof(HOffset : Double):Boolean;
   property BacklashUsed : Boolean read FBackLashUsed write SetBackLashUsed;
   property FirstPosition : Double read FFirstPosition Write SetFirstPosition;
   property NumberOfEntries : Integer read FNumberOfEntries write SetNumberEntries;
   property CompIncrement     : Double read FCompIncrement write SetCompIncrement;
   property CompHomeOffset    : Double read FCompHomeOffset write SetHomeOffset;
   property CompPositiveLimit : Double read GetCompPositiveLimit;// write SetPOsLimit;
   property CompNegativeLimit : Double read GetCompNegativeLimit;// write SetNegLimit;
   property DoublePointsOnOutput : Boolean read FDoublePoints write SetDoublePOints;
   property ManualBacklashMicron    : Integer read FManualBacklashMicron write SetManualBacklash;
   property ManualBackLashAdjust    : Integer read FManualBacklashAdjust write SetManualBacklashAdjust;
   property AxisIsLinear      : Boolean read FaxisIsLinear write SetAxisIsLinear;
   property AxisMotorNumber : INteger read GetMotorNumber write SetMotorNumber;
   property AxisLetter : String read FAxisLetter write SetAxisLetter;
   property MakeOutput : Boolean read FMakeOutput write FMakeOutPut;
   property aaEncoderCountsPerUnit : Double read FEncodeCountsPerUnit write SetEncodeCountsPerUnit;

   // Events
   property OnInputDataUpdated : TInputDataListChangeEvent read FOnInputDataUpdated write FOnInputDataUpdated;
   property OnOutputDataUpdated : TOutputDataListChangeEvent read FOnOutputDataUpdated write FOnOutputDataUpdated;
   end;

TAxisBuffer = class(TObject)
  public
  DoublePoints : Boolean;
  BacklashUsed : Boolean;
  IsLinear : Boolean;
  MNumber : Integer;
  MakeOutPut : Boolean;
  DiffList : TStringList;
  EncoderCountsPerUnit : Double;
  FirstPosition : Double;
  NumberOfEntries : Integer;
  CompIncrement     : Double;
  HomeOffset    : Double;
  PositiveLimit : Double;
  NegativeLimit : Double;
  ManualBacklashMicron    : Integer;
  constructor create;
  destructor Destroy; override;
  function CompareTo(ExtBuffer :TAxisBuffer): Boolean;
  end;

TSingleAxisSnapshot = class(TObject)
  private
    FParametersMatch: Boolean;
    FInputDataMatch: Boolean;
    function GetDiffList: TStringList;
  public
    ParameterBuffer : TAxisBuffer;
    GridInputData : TInputDataList;
    procedure ClearDown;
    constructor create;
    destructor Destroy; override;
    procedure CompareTo(ExtSnapshot : TSingleAxisSnapshot);
    property ParametersMatch : Boolean read FParametersMatch;
    property InputDataMatch : Boolean read FInputDataMatch;
    property DiffList : TStringList read GetDiffList;
  end;


TSingleAxisData = class(TObject)
  private
    FMachineHomeOffset: Double;
    FOnInputDataUpdated: TIndexedInputDataChangeEvent;
    FOnOutputDataUpdated: TIndexedOutputDataChangeEvent;
    FDoublePoints: Boolean;
    RegisteredStatusBuffer:TSingleAxisSnapshot;// TAxisBuffer;
    FAxisName : String;
//    FMakeOutput: Boolean;
    function GetEncoderCountsPerUnit: Double;
    procedure SetEncoderCountsPerUnit(const Value: Double);
    procedure InputDataUpdated(ZShift : Double; Valid : Boolean);
    procedure OutputDataUpdated(Valid : Boolean);
    procedure SetDoublePoints(const Value: Boolean);
    procedure SetAxisName(const Value: String);
    procedure SetMotorNumber(const Value: Integer);
    function GetMotorNumber: Integer;
    function GetMakeOutput: Boolean;
    procedure SetMakeOutput(const Value: Boolean);
    public
      WorkingFileStatusBuffer  :TSingleAxisSnapshot;// TAxisBuffer;
      CurrentStatusBuffer :TSingleAxisSnapshot;// TAxisBuffer;
      CompareStatusBuffer : TSingleAxisSnapshot;
      Valid : Boolean;
      CompensationDataLists : TSingleAxisCompDataLists;
      ParentListIndex : Integer;
      FixedBacklashAdjust : Single;
      procedure BufferStatusTo(ABuffer : TAxisBuffer);
      function aaCompareTo(CAxisData :TSingleAxisData;Availability : TAvailabityandStatusData):Boolean;
      procedure RecordStatus;
//      function RevertToBuffer: Boolean;
      function RevertToSnapShot(Snap : TSingleAxisSnapshot): Boolean;
      procedure TakeSnapshotFrom(SourceAxis: TSingleAxisData;DestBuffer :TSingleAxisSnapshot);
      procedure CompareCurrentStatusToSnapshot(CSnap : TSingleAxisSnapshot); // Resiults Go Into Snapshot;
      procedure CompareCurrentToWorkingSnapshot;
//      function SettingsMatchesRecordedStatus : Boolean;
      function ReadFromWorkingFile(FileName : String): Boolean;
      procedure UpdateChangeStrings(Changes : TStringList;Grid :TInputDataGrid);
      procedure WriteToWorkingFile(FilePath : String);
      procedure InitialiseFromDefaults(Default : TDefaultSettingEntry);
      property EncoderCountsPerUnit : Double read GetEncoderCountsPerUnit write SetEncoderCountsPerUnit;
      property OnInputDataUpdated : TIndexedInputDataChangeEvent read FOnInputDataUpdated write FOnInputDataUpdated;
      property OnOutputDataUpdated : TIndexedOutputDataChangeEvent read FOnOutputDataUpdated write FOnOutputDataUpdated;
      property MakeOutput : Boolean read GetMakeOutput write SetMakeOutput;
      property DoublePointsOnOutput : Boolean read FDoublePoints write SetDoublePoints;
      property AxisName : String Read FAxisName write SetAxisName;
      property AxisMotorNumber : Integer read GetMotorNumber write SetMotorNumber;
      constructor Create;
      destructor Destroy; override;
    end;


TCompDataStates = (tcdUnavailable,tcdAvailable,tcdLoaded,tcdImportButNoComp,tcdNoCompNoImport,tcdNoImportButComp);

TAvailabilityAndStatusRecord = record
  AxisIndex : Integer;
  Required : Boolean;
  WorkingDataState        : TCompDataStates;
  ImportDataState         : TCompDataStates;
  CompDataExist           : Boolean;
  ImportExists            : Boolean;
  DataMatchByOutput       : Boolean;
  DateLastCalculated      : TDateTime;
  Touched                 : Boolean;
  ExistingCompEntries     : Integer;
  InternalCompEntries     : Integer;
  RegisteredSettingsMatch : Boolean;
  RegisteredDataMatch     : Boolean;
  end;

  TAvailabityandStatusData = Class(TObject)
   private
   FWorkingFileExists : Boolean;
   FFirstPowerUpMode : TFirstPowerupModes;
    procedure SetWorkingFileExists(const Value: Boolean);
    procedure SetFPUM(const Value: TFirstPowerupModes);
   public
   XData : TAvailabilityAndStatusRecord;
   YData : TAvailabilityAndStatusRecord;
   ZData : TAvailabilityAndStatusRecord;
   AData : TAvailabilityAndStatusRecord;
   BData : TAvailabilityAndStatusRecord;
   CData : TAvailabilityAndStatusRecord;
   AllImportLoaded : Boolean;
   ImportDataAvailableAllAxes : Boolean;
   AllImportLoadedandMatched : Boolean;
   PowerUpMode : TCompPowerupModes;//CompFullyRegistered : Boolean;
   WorkingMode : TCompPowerupModes;//CompFullyRegistered : Boolean;
   FirstRegPending : Boolean;
   NewCompPending  : Boolean;
   FullCompFileExists  : Boolean;
   BlankCompFileExists  : Boolean;
   PartialCompFileExists: Boolean;
   WorkingFileLoaded : Boolean;
   function NoDataAvailable : Boolean;
   constructor Create;
   procedure DataForAxisNameChanged(Axisname : String);
//   procedure TestForCompFile(FileName : String);
   function RegisteredStatusForAxis(AxisName : String;var StateStr : String; var ReasonString : String): Boolean;
   function DataForAxisName(AxisName : String;var Found : Boolean):TAvailabilityAndStatusRecord;
   function DataForAxisWithIndex(AxIndex : Integer;var Found : Boolean):TAvailabilityAndStatusRecord;
   function DataExistsForAxisWithIndex(AxIndex : Integer): Boolean;
   procedure UpdateFromDefaultSettings(DefaultSettings: TDefaultSettingsList);
   Function IsSatisfied: Boolean;
   function IsOKForFirstLoadConfirm : Boolean;
   Function AllImportAvailable : Boolean;
   Function ImportloadedAndMatched : Boolean;
   function CheckAllExistingCompSatisfiedandMatched:Boolean;
   procedure SetRegisteredMatchForAxis(AxisName: String;SettingsMatch:Boolean;DataMatch : Boolean);

   function IsValidBlankSystem : Boolean; // Blank CompFile , No Imports, No Working File
   function IsUnusable : Boolean; // non blank comp file, No Working File, No Imports
   function IsStandardStartUp : Boolean;    // non blank comp file, Working File
   Function Importloaded : Boolean;
   Function DataHasBeenTouched : Boolean;
   function WorkingLoadedandMatches : Boolean;
   function SetWorkingFileMacId(FilePath : String;IdStr : String): Boolean;
   function WorkingMachineIDMatches(FilePath : String;CurrentID : String;var FileIdString : String): Boolean;
   function DataMatchesForAxis(AxIndex : Integer): Boolean;
   function ResolveEntryCountForAxisAtIndex(AxIndex : Integer;var AxisCompData : TSingleAxisData;var MadeAChange : Boolean): Boolean;
   property WorkingFileExists : Boolean read FWorkingFileExists write SetWorkingFileExists;
   property FirstPOwerUpMode : TFirstPowerupModes read FFirstPowerUpMode write SetFPUM;
  end;


// List of all Axis Data
TCalAxisList = class(TList)
  private
    CompareList : TList;
    FOnInputaDataUpdated: TIndexedInputDataChangeEvent;
    FOnOutputaDataUpdated: TIndexedOutputDataChangeEvent;
    procedure InputDataHasChanged(LIndex :Integer;ZShift : Double; Valid : Boolean);
    procedure OutputDataHasChanged(LIndex :Integer; Valid : Boolean);
    procedure CleanCompareList;
  public
    Availability : TAvailabityandStatusData;
    constructor Create;
    destructor Destroy; override;
    procedure Clean;
    function UpdateFromNCData(NCList :TNCConfigList): Boolean;
    function CompareToArchive(ArchivePath : String;ForceLoad : Boolean;BufferRegisteredData : Boolean;var ArchiveExists : Boolean): Boolean;
    function AddAxisCalled(Axname : String;ForCompare : Boolean):TSingleAxisData;
    function EntryAtIndex(Index : INteger): TSingleAxisData;
    function EntryWithMotorNumber(MNumber : Integer): TSingleAxisData;
    function EntryWithAxisName(AxName : String): TSingleAxisData;
    function LoadFromDataFile(IFile : String):Boolean;
    function LoadCompareListFromDataFile(IFile : String):Boolean;
    function InitFromDefaultData(DefaultSettings : TDefaultSettingsList):Boolean;
    property OnInputDataUpdated : TIndexedInputDataChangeEvent read FOnInputaDataUpdated write FOnInputaDataUpdated;
    property OnOutputDataUpdated : TIndexedOutputDataChangeEvent read FOnOutputaDataUpdated write FOnOutputaDataUpdated;
end;

TAdjustBuffer = class(TList)
 public
 procedure Clean;
 function WriteToFile(WorkingFile : String;AxisName : String):Boolean;
 function ReadFromFile(WorkingFile : String;AxisName : String):Boolean;
 end;

TInputDataGrid = class(TStringGrid)
    private
      CurrentRow : Integer;
      CurrentCol : Integer;
      CurrentStrVal : String;
      StrValOnEntry : String;
      FEntryEnabled : Boolean;
      FAutoIncrement: Boolean;
      FFirstPos : Double;
      FNumEntries : INteger;
      FStep : Double;
      Processed : Boolean;
      LastAdjustBuffer : TAdjustBuffer;
      FOnDataDifferent: TDataEntryChangedEvent;
      FOnDataReMatch: TNotifYEvent;
      FAdjustMode: Boolean;
      FStandardWidth: Integer;
      FADjustWidth: Integer;
      LastDataMatch : Boolean;
//      SelCol,SelRow : Integer;
      CurrentSnapShot : TSingleAxisSnapshot;
      FShowEdits: Boolean; // externally set and handled . Used for detection of data change
      procedure CellKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
      procedure CellEdited(Sender: TObject; ACol, ARow: Longint; const Value: string);
      procedure CellSelected(Sender: TObject; ACol, ARow: Integer; var CanSelect: Boolean);
      procedure CellExited(Sender : Tobject);
      procedure SetAdjustMode(const Value: Boolean);
      procedure SetAdjustWidth(const Value: Integer);
      procedure SetStandardWidth(const Value: Integer);
      procedure CellFocus(CCol,CRow : Integer);
      procedure MyDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
      procedure BufferAndStoreAdjustments(AxisName : String;CFilePath : String);
      function GetDataMatches: Boolean;
      function CellMatchesSnapshot(ACol,Arow : INteger): Boolean;

    procedure SetShowEdits(const Value: Boolean);
    public
      TemporaryLoadBuffer : TList;
      constructor Create(AOwner: TComponent); override;
      procedure CleanLoadBuffer;
      procedure SaveToTempLoadBuffer;
      procedure CleanAdjustBuffer;
      procedure LoadFromLoadBufferandClean;
      procedure GoToTop;
      procedure GoToBottom;
      procedure BufferContentsToSnapShot(SnapShot :TSingleAxisSnapshot);
      procedure RegisterSnapshot(SnapShot :TSingleAxisSnapshot);
      function AllCellsValid : Boolean;
      function MergeAndCollapse : Boolean;
      function MatchesSnapShot: Boolean;
//      procedure LoadFromDataList(InData :TSingleAxisCompDataLists);//;SaveToBuffer : Boolean);
      procedure LoadFromSnapshot(Snap :TSingleAxisSnapshot);
      function SaveToDataList(InData :TSingleAxisCompDataLists): Boolean;
      procedure Initialise(FirstPos: Double;NumEntries : INteger;Step : Double);
      property AutoIncrement : Boolean read FAutoIncrement write FAutoIncrement;

      property OnDataDifferent : TDataEntryChangedEvent read FOnDataDifferent write FOnDataDifferent;
      property OnDataReMatch   : TNotifYEvent read FOnDataReMatch write FOnDataReMatch;

      property AdjustWidth : Integer read FADjustWidth write SetAdjustWidth;
      property StandardWidth : Integer read FStandardWidth write SetStandardWidth;
      property AdjustMode : Boolean read FAdjustMode write SetAdjustMode;
      property DataMatches : Boolean read GetDataMatches;
      property ShowEdits : Boolean read FShowEdits write SetShowEdits;
      property FirstPosition : Double read FFirstPos;

  end;


implementation
uses CompFileParser,AxisCalibrationForm;

function SlashSep(const Path, S: String): String;
begin
  if AnsiLastChar(Path)^ <> '\' then
    Result := Path + '\' + S
  else
    Result := Path + S;
end;

function NextToken(var InStr : String):String;
var
CommaPos : Integer;
begin
Result := '';
CommaPos := Pos(',',InStr);
if CommaPos > 0 then
  begin
  Result := Trim(Copy(InStr,1,CommaPos-1));

  InStr := Copy(Instr,CommaPos+1,1000);
  end
else
  begin
  Result := Instr;
  Instr := '';
  end;
end;



function TSingleAxisCompDataLists.intReadFromWorkingFile(FileName: String):Boolean;
var
InFile : TiniFile;
ENum : INteger;
Pos,Fwd,Rev : Double;
KeyStr : String;
SectionName : String;
Buffer : String;
begin
Clean;
Result := False;
if FileExists(FileName) then
  begin
  InFile := TIniFile.Create(Filename);
  try
  SectionName := Format('AXIS%s',[FAxisLetter]);
  if not InFile.SectionExists(SectionName) then
    begin
    exit;
    end;
  AxisMotorNumber :=  Infile.ReadInteger(SectionName,'MotorNumber',-1);
  BackLashUsed     := Infile.ReadBool(SectionName,'UseBacklashTable',False);
  FMakeOutput      := Infile.ReadBool(SectionName,'MakeOutput',True);

  FNumberOfEntries  := Infile.ReadInteger(SectionName,'ManCompEntries',0);
  CompIncrement  := Infile.ReadFloat(SectionName,'ManCompIncrement',0); // use property to ensure update of limits
  FirstPosition  := Infile.ReadFloat(SectionName,'FirstCompPosition',0);// use property to ensure update of limits
  FCompHomeOffset := Infile.ReadFloat(SectionName,'HomeOffset',0);
  DoublePointsOnOutput := Infile.ReadBool(SectionName,'DoublePoints',True);
//  CompPositiveLimit := Infile.ReadFloat(SectionName,'PosLimit',0);
//  CompNegativeLimit := Infile.ReadFloat(SectionName,'NegLimit',0);
  FManualBacklashMicron  := Infile.ReadInteger(SectionName,'ManualBacklash',0);
  Buffer := Infile.ReadString(SectionName,'AxisType','Linear');
  aaEncoderCountsPerUnit   := Infile.ReadFloat(SectionName,'CountsPerUnit',0);

  if (Buffer[1] = 'L') or (Buffer[1] = 'l') then
    AxisIsLinear := True

  else
    AxisIsLinear := False;


  if FNumberOfEntries > 0 then
    begin
    SectionName := Format('AXIS%sDATA',[FAxisLetter]);
    For Enum := 1 to FNumberOfEntries do
      begin
      KeyStr := Format('Entry%dTarget',[Enum]);
      POs :=  Infile.ReadFloat(SectionName,KeyStr,0);
      KeyStr := Format('Entry%dForward',[Enum]);
      Fwd :=  Infile.ReadFloat(SectionName,KeyStr,0);
      KeyStr := Format('Entry%dReverse',[Enum]);
      Rev :=  Infile.ReadFloat(SectionName,KeyStr,0);
      AddInputEntry(POs,Fwd,Rev);
      end;
    Result :=True;
    end;
  finally
  if Result Then
    begin
    UpdateInputDataForHomeOffsetof(FCompHomeOffset);
    end;
  InFile.Free;
  end;
  end;
end;
// THis writes The Table and Input Data for an axis to the working file under its own section
procedure TSingleAxisCompDataLists.WriteToFile(FileName: String);
var
OutFile : TiniFile;
ENum : INteger;
KeyStr : String;
SectionName : String;
FEntry : TInputDataRecord;

begin
  OutFile := TIniFile.Create(Filename);
  try
  SectionName := Format('AXIS%s',[FAxisLetter]); // Section Name example = 'AXISX'
  OutFile.WriteInteger(SectionName,'MotorNumber',AxisMotorNumber);
  OutFile.WriteBool(SectionName,'UseBacklashTable',FBackLashUsed);
  Outfile.WriteBool(SectionName,'MakeOutput',FMakeOutput);
  Outfile.WriteInteger(SectionName,'ManCompEntries',FNumberOfEntries);
  Outfile.WriteFloat(SectionName,'ManCompIncrement',FCompIncrement);
  OutFile.WriteBool(SectionName,'DoublePoints',DoublePointsOnOutput);
  Outfile.WriteFloat(SectionName,'FirstCompPosition',FFirstPosition);
  Outfile.WriteFloat(SectionName,'HomeOffset',FCompHomeOffset);
  Outfile.WriteFloat(SectionName,'PosLimit',CompPositiveLimit);
  Outfile.WriteFloat(SectionName,'NegLimit',CompNegativeLimit);
  Outfile.WriteInteger(SectionName,'ManualBacklash',FManualBacklashMicron);
  Outfile.WriteFloat(SectionName,'CountsPerUnit',FEncodeCountsPerUnit);
  if FaxisIsLinear then
    Outfile.WriteString(SectionName,'AxisType','Linear')
  else
    Outfile.WriteString(SectionName,'AxisType','Rotary');

  SectionName := Format('AXIS%sDATA',[FAxisLetter]);
  For Enum := 1 to FNumberOfEntries do
    begin
    FEntry := CurrentInputDataList.Items[Enum-1];
    KeyStr := Format('Entry%dTarget',[Enum]);
    OutFile.WriteFloat(SectionName,KeyStr,FEntry.Position);
    KeyStr := Format('Entry%dForward',[Enum]);
    OutFile.WriteFloat(SectionName,KeyStr,FEntry.InputForwardError);
    KeyStr := Format('Entry%dReverse',[Enum]);
    OutFile.WriteFloat(SectionName,KeyStr,FEntry.InputReverseError);
    end;

  finally
  OutFile.Free;
  end;
end;


function TSingleAxisCompDataLists.ReadFromExportFile(FileName: String): Boolean;
var
InFile : TiniFile;
ENum : INteger;
Pos,Fwd,Rev : Double;
KeyStr : String;
Buffer : String;
Hoff : Double;
begin
Result := False;
Clean;
Hoff := 0;
if FileExists(FileName) then
  begin
  InFile := TIniFile.Create(Filename);
  try
  Buffer := Infile.ReadString('SETUP','UseBacklashTable','No');
  BackLashUsed  := (Buffer[1] = 'y') or (Buffer[1] = 'Y');
  FNumberOfEntries  := Infile.ReadInteger('SETUP','ManCompEntries',0);
  FCompIncrement  := Infile.ReadFloat('SETUP','ManCompIncrement',0);
  FFirstPosition  := Infile.ReadFloat('SETUP','FirstCompPosition',0);
  OutputDataList.MotorNumber := Infile.ReadInteger('SETUP','MotorNumber',0);
  Hoff := Infile.ReadFloat('SETUP','HomeOffset',-9999999);
  if Hoff > -9999998 then
    begin
    CompHomeOffset := Hoff;
    EncoderOffsetValid := True;
    end
  else
    begin
    CompHomeOffset := 0;
    EncoderOffsetValid := False;
    end;
//  CompPositiveLimit := Infile.ReadFloat('SETUP','PosLimit',0);
//  CompNegativeLimit := Infile.ReadFloat('SETUP','NegLimit',0);
  FManualBacklashMicron  := Infile.ReadInteger('SETUP','ManualBacklash',0);
  Buffer := Infile.ReadString('SETUP','AxisType','Linear');

  if (Buffer[1] = 'L') or (Buffer[1] = 'l') then
    begin
    AxisIsLinear := True;
    end
  else
    begin
    AxisIsLinear := False;
    end;

  aaEncoderCountsPerUnit   := Infile.ReadFloat('SETUP','CountsPerUnit',0);
  if FNumberOfEntries > 0 then
    begin
    For Enum := 1 to FNumberOfEntries do
      begin
      KeyStr := Format('Entry%dTarget',[Enum]);
      POs :=  Infile.ReadFloat('DATA',KeyStr,0);
      KeyStr := Format('Entry%dForward',[Enum]);
      Fwd :=  Infile.ReadFloat('DATA',KeyStr,0);
      KeyStr := Format('Entry%dReverse',[Enum]);
      Rev :=  Infile.ReadFloat('DATA',KeyStr,0);
      AddInputEntry(POs,Fwd,Rev);
      end;
    end;
  finally
  if EncoderOffsetValid = True then
    begin
    Result := CurrentInputDataList.UpdateShiftedDataForHomeOffset(Hoff);
    end;
  InFile.Free;
  end;
  end;
end;



procedure TSingleAxisCompDataLists.Clean;
begin
//RegisteredInputDataList.Clean;
CurrentInputDataList.Clean;
OutputDataList.Clean;
inherited;
end;

procedure TSingleAxisCompDataLists.AddInputEntry(Pos, Fwd, Rev: Double);
var
NewEntry : TInputDataRecord;
begin
NewEntry := TInputDataRecord.Create;
NewEntry.Position := Pos;
NewEntry.InputForwardError := Fwd;
NewEntry.InputReverseError := Rev;
CurrentInputDataList.Add(NewEntry);
end;

{ TInputDataList }

function TInputDataList.CompareTo(ExtDataList: TInputDataList): Boolean;
var
Enum : Integer;
LEntry : TInputDataRecord;
ExtEntry : TInputDataRecord;
begin
Result := True;
if Count = ExtDataList.Count then
  begin
  ENum := 0;
  while (Result = True) and (Enum < Count) do
    begin
    try
    LEntry := Items[Enum];
    ExtEntry := ExtDataList.Items[Enum];
    if Lentry.Position <> ExtEntry.Position then Result := False
    else if Lentry.InputForwardError <> ExtEntry.InputForwardError then Result := False
    else if Lentry.InputReverseError <> ExtEntry.InputReverseError then Result := False
    else if Lentry.InputForwardAdjust <> ExtEntry.InputForwardAdjust then Result := False
    else if Lentry.InputReverseAdjust <> ExtEntry.InputReverseAdjust then Result := False;
    finally
    inc(Enum);
    end;
    end
  end
else
  begin
  Result := False;
  end
end;

procedure TInputDataList.CopyTo(ExtDataList: TInputDataList);
var
Enum : Integer;
LEntry : TInputDataRecord;
ExtEntry : TInputDataRecord;
begin
ExtDataList.Clean;
if Count > 0 then
  begin
  For ENum := 0 to Count-1 do
    begin
    LEntry := Items[Enum];
    ExtEntry := TInputDataRecord.Create;
    ExtEntry.AssignFrom(LEntry);
    ExtDataList.Add(ExtEntry);
    end
  end;

end;

function TInputDataList.UpdateShiftedDataForHomeOffset(HOffset: Double): Boolean;
var
CurrentEntry : TInputDataRecord;
ENum : INteger;
begin
Result := false;
if Count < 2 then exit;
if  CalcultateZeroShiftForHomeOffsetOf(HOffset,FZShift) then
  begin
  For Enum := 0 to Count-1 do
    begin
    CurrentEntry := Items[Enum];
    CurrentEntry.ZeroShiftedForwardData := CurrentEntry.InputForwardError-FZshift;
    CurrentEntry.ZeroShiftedReverseData := CurrentEntry.InputReverseError-FZshift;
    end;
  if assigned(FOnInputDataListUpdated) then FOnInputDataListUpdated(FZShift,True)
  end
else
  begin
  Result := False;
  exit;
//  if assigned(FOnInputDataListUpdated) then FOnInputDataListUpdated(0,False)
  end;
  Result := True;
end;

procedure TInputDataList.SetPosLimit(const Value: Double);
begin
  FPosLimit := Value;
end;

procedure TInputDataList.SetNegLimit(const Value: Double);
begin
  FNegLimit := Value;
end;


function TInputDataList.CalcultateZeroShiftForHomeOffsetOf(HOffset: Double;var CalculatedShift : Double): Boolean;
var
PosBelowZero : Double;
PosAboveZero : Double;
CompBelowZero : Double;
CompAboveZero : Double;
NoSecondaryShift : Boolean;
ENum : INteger;
FoundAbove : Boolean;
CurrentEntry,LastEntry : TInputDataRecord;
PosDiff : Double;
PosRatio : Double;
PosDist : Double;
CompDiff: Double;
begin
Result := false;
if Count > 1 then
  begin
  NoSecondaryShift := False;
  FoundAbove:= False;
  LastEntry := Items[0];
  if LastEntry.Position > Hoffset then
    begin
    exit;
    end;
  Enum := 1;
  while (Enum < Count) and (Not FoundAbove) and (not NoSecondaryShift) do
    begin
    try
    CurrentEntry := Items[Enum];
    If CurrentEntry.Position = HOffset then
      begin
      PosAboveZero := CurrentEntry.Position;
      PosAboveZero := CurrentEntry.InputForwardError;
      NoSecondaryShift := True;
      end
    else If CurrentEntry.Position > HOffset then
      begin
      PosBelowZero := LastEntry.Position;
      CompBelowZero := LastEntry.InputForwardError;
      PosAboveZero := CurrentEntry.Position;
      CompAboveZero := CurrentEntry.InputForwardError;
      FoundAbove := True;
      end
    else
      begin
      LastEntry := CurrentEntry;
      end
    finally
    inc(ENum)
    end;
    end;
  if NoSecondaryShift then
    begin
    Result := True;
    CalculatedShift := PosAboveZero;
    end
  else If FoundAbove Then
    begin
    PosDiff := PosAboveZero-PosBelowZero;
    CompDiff:= CompAboveZero-CompBelowZero;
    PosDist := abs(Hoffset-PosBelowZero);
    PosRatio := Abs(PosDist/PosDiff);
    CalculatedShift := CompBelowZero+(PosRatio*CompDiff);
    Result := True;
    end;

  end;
end;

procedure TInputDataList.Clean;
var
LEntry : TInputDataRecord;
begin
while Count > 0 do
 begin
 Lentry := Items[Count-1];
 LEntry.Free;
 Delete(Count-1);
 end;
end;


function TInputDataList.ShiftedCompAtPosition(Posn : Double;Var FwdCompResult : Double;Var RevCompResult : Double):Boolean;
var
PosAbove : Boolean;
PosBelow : Boolean;
PosDist : Double;
PosRatio : Double;
FoundAbove : Boolean;
PosDiff,FwdCompDiff,RevCompDiff : Double;
CurrentEntry,LastEntry : TInputDataRecord;
Enum : Integer;
begin
Result := False;
FwdCompResult := 0;
RevCompResult := 0;
if Count < 2 then exit;
FoundAbove := False;
LastEntry := Items[0];
If LastEntry.Position > Posn then
  begin
  if Posn < NegLimit then
    begin
    LastEntry := Items[0];
    CurrentEntry := Items[1];
    PosDiff := CurrentEntry.Position-LastEntry.Position;
    // here to extrapolate backwards if within 2 compdists
    if abs(Posn-NegLimit) <= abs(PosDiff*2) then
      begin
      Result := True;
      PosRatio := abs(POsn-NegLimit)/abs(PosDiff);
      FwdCompDiff := LastEntry.ZeroShiftedForwardData-CurrentEntry.ZeroShiftedForwardData;
      RevCompDiff := LastEntry.ZeroShiftedReverseData-CurrentEntry.ZeroShiftedReverseData;
      FwdCompResult := LastEntry.ZeroShiftedForwardData+(FwdCompDiff*PosRatio);
      RevCompResult := LastEntry.ZeroShiftedReverseData+(RevCompDiff*PosRatio);
      end ;
    exit;
    end
  end;
Enum := 1;
while (not FoundAbove) and (Enum < Count) do
  begin
  try
  CurrentEntry := Items[Enum];
  if CurrentEntry.Position >= Posn then
    begin
    FoundAbove := True;
    PosDiff := CurrentEntry.Position-LastEntry.Position;
    FwdCompDiff := CurrentEntry.ZeroShiftedForwardData-LastEntry.ZeroShiftedForwardData;
    RevCompDiff := CurrentEntry.ZeroShiftedReverseData-LastEntry.ZeroShiftedReverseData;
    PosDist := Posn-LastEntry.Position;
    PosRatio := abs(PosDist/PosDiff);
    FwdCompResult := LastEntry.ZeroShiftedForwardData+(FwdCompDiff*PosRatio);
    RevCompResult := LastEntry.ZeroShiftedReverseData+(RevCompDiff*PosRatio);
    Result := True;
    end
  else
    begin
    LastEntry := CurrentEntry
    end;
  finally
  Inc(Enum)
  end;
  end;

  if Not Result then
    begin
    // Check if within 2 increments of Pos Limit and do forward Extrapolation if true
    // Repeat Last Calculation
    if 1=1 then //Posn > PosLimit then
      begin
      LastEntry := Items[Count-2];
      CurrentEntry := Items[Count-1];
      PosDiff := CurrentEntry.Position-LastEntry.Position;
      if abs(Posn-PosLimit) <= abs(PosDiff) then
        begin
        Result := True;
        PosRatio := abs(Posn-PosLimit)/abs(PosDiff);
        FwdCompDiff := CurrentEntry.ZeroShiftedForwardData-LastEntry.ZeroShiftedForwardData;
        RevCompDiff := CurrentEntry.ZeroShiftedReverseData-LastEntry.ZeroShiftedReverseData;
        FwdCompResult := CurrentEntry.ZeroShiftedForwardData+(FwdCompDiff*PosRatio);
        RevCompResult := CurrentEntry.ZeroShiftedReverseData+(RevCompDiff*PosRatio);
        end ;
    Result := True;
    end;
  end;
end;


procedure TInputDataList.aaInitialise(First, Step: Double; NumEntries: INteger);
var
NewEntry : TInputDataRecord;
ENum : Integer;
CurrentPos : Double;
begin
Clean;
CurrentPos := First;
FNegLimit := First;
FPosLimit := First+((NumEntries-1)*Step);
if NumEntries > 0 then
  begin
  For Enum := 0 to NumEntries-1 do
   begin
   NewEntry := TInputDataRecord.Create;
   Newentry.Position := CurrentPos;
   NewEntry.InputForwardError := 0;
   NewEntry.InputReverseError := 0;
   NewEntry.InputForwardAdjust := 0;
   NewEntry.InputReverseAdjust := 0;
   Add(NewEntry);
   CurrentPos := CurrentPos + Step;
   end;
  end;
end;


{ TInputDataGrid }
constructor TInputDataGrid.Create(AOwner: TComponent);
begin
  inherited;
  FFirstPos := 0;
  FShowEdits := True;
  ColCount := 5;
  AdjustMode := False;
  FixedCols := 1;
  OnSelectCell := CellSelected;
  OnSetEditText := CellEdited;
  OnKeyDown := CellKeyDown;
  OnDrawCell := MyDrawCell;

  DefaultDrawing := False;
  OnExit := CellExited;
  FAutoIncrement := True;
  FFirstPos := -100;
  FNumEntries := 21;
  FStep := 10;
  Options := OPtions-[goRangeSelect];
  Options := Options + [goEditing];
  Options := OPtions-[goDrawFocusSelected];
//  BufferList := TList.Create;
  CurrentSnapshot := nil;
  TemporaryLoadBuffer := TList.Create;
  AdjustMode := False;
end;

procedure TInputDataGrid.CellKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
var
Valid : Boolean;
NumVal : Double;
NewStrVal : String;
//Wrap : Boolean;
begin
if (Key = 36) then // Home key
  begin
  if ssCtrl in Shift then
    begin
    CurrentRow := 1;
    if FAdjustMode then
      begin
      CurrentCol := 3;
      end
    else
      begin
      CurrentCol := 1;
      end;
    CellFocus(CurrentCol,CurrentRow);
    end
  end
else if key = 35 then // End Key
  begin
  if ssCtrl in Shift then
    begin
    CurrentRow := RowCount-1;
    if FAdjustMode then
      begin
      CurrentCol := 4;
      end
    else
      begin
      CurrentCol := 2;
      end;
    CellFocus(CurrentCol,CurrentRow);
    end
  end

else if key = 13 then
  begin
  NewStrVal := Cells[CurrentCol,CurrentRow];
  try
  Valid := True;
  NumVal := StrToFloat(NewStrVal);
  except
  Valid := False;
  end;
  If Valid then
    if NewStrVal <> StrValOnEntry then
      begin
      CurrentStrVal := NewStrVal;
      Processed := True;
//      if assigned(FOnDataChanged) then FOnDataChanged(CurrentCol,CurrentRow,CurrentStrVal,NumVal);
      end;
  if AutoIncrement and Valid then
    begin
    if CurrentRow < RowCount-1 then
      inc(CurrentRow);
    CellFocus(CurrentCol,CurrentRow);
    end
  end;
end;

procedure TInputDataGrid.CellEdited(Sender: TObject; ACol, ARow: Longint; const Value: string);
var
NewVal : String;
FloatVal : Double;
ValOk : Boolean;
begin
 ValOk := True;
 NewVal := Cells[ACol, ARow];
 If NewVal <> '' then
  begin
  try
  FloatVal := StrToFloat(NewVal)
  except
  if (Trim(NewVal) = '-') or (Trim(NewVal)='+') then
    begin
    FloatVal := 0;
    NewVal:='0';
    end
  else
    Begin
    ValOk := False;
    end;
  end;
 if ValOk then
   begin
   if NewVal <> CurrentStrVal then
      begin
      CurrentStrVal := NewVal;
      Processed := True;
 //     if assigned(FOnDataChanged) then FOnDataChanged(ACol,Arow,CurrentStrVal,FloatVal);
      end
   end
 else
    begin
    CurrentStrVal := StrValOnEntry;
    Cells[ACol, ARow] := StrValOnEntry;
//    if assigned(FOnDataChanged) then FOnDataChanged(ACol,Arow,'Revert',FloatVal);
    end;
 end;

 if not MatchesSnapShot then
  begin
//  if LastDataMatch then
//    begin
    if assigned(FOnDataDifferent) then FOnDataDifferent(ACol,Arow,CurrentStrVal,FloatVal);
//    end;
  LastDataMatch := False;
  end
else
  begin
//  if Not LastDataMatch then
//    begin
    if assigned(FOnDataReMatch) then FOnDataReMatch(Self);
    LastDataMatch := True;
//    end
  end;

end;

procedure TInputDataGrid.CellSelected(Sender: TObject; ACol, ARow: Integer; var CanSelect: Boolean);
var
NewVal : String;
begin
 try
 NewVal := Cells[CurrentCol,CurrentRow];
 if (NewVal = '-') or (NewVal = '+') then
  begin
  Cells[CurrentCol,CurrentRow] := '0.0';
  end;
 except
 end;
  CurrentCol := ACol;
  CurrentRow := ARow;
  Processed := False;
  StrValOnEntry := Cells[ACol,ARow];
end;

procedure TInputDataGrid.CellExited(Sender: TObject);
var
NewVal : String;
NewFloatVal : Double;
FloatVal : Double;
ValOk : Boolean;
Valid : Boolean;
begin
 exit;
end;


{procedure TInputDataGrid.EnableEntry(EnableState: Boolean);
begin
if EnableState then
  begin
  Enabled := True;
  Color := clWebIvory;
  Options := Options + [goEditing];
  CurrentRow := 1;
  CurrentCol := 1;
  CellFocus(CurrentCol,CurrentRow);

  end
else
  begin
  Enabled := False;
  Color := clWebLightgrey;
  Options := Options - [goEditing]
  end
end;}

procedure TInputDataGrid.Initialise(FirstPos: Double;NumEntries : INteger;Step : Double);
var
RowNum : Integer;
CurrentPos : Double;
begin
  RowCount := 4;
  FixedRows :=1;
  FixedCols := 1;
  Cells[0,0] := 'Position';
  Cells[1,0] := 'Fwd. Error';
  Cells[2,0] := 'Rev. Error';
  Cells[3,0] := 'Fwd. Adj.';
  Cells[4,0] := 'Rev. Adj';
  RowCount := NumEntries+1;
  CurrentPos := FirstPos;
  FFirstPos := FirstPos;
  For RowNum := 1 to NumEntries do
    begin
    Cells[0,RowNum] := Format('%5.3f',[CurrentPos]);
    Cells[1,RowNum] := '0.0';
    Cells[2,RowNum] := '0.0';
    Cells[3,RowNum] := '0.0';
    Cells[4,RowNum] := '0.0';
    CurrentPos := CurrentPos+Step;
    end;
 if not MatchesSnapShot then
  begin
  if LastDataMatch then
    begin
    if assigned(FOnDataDifferent) then FOnDataDifferent(0,1,'0',0);
    LastDataMatch := False;
    end;
  end
else
  begin
  if Not LastDataMatch then
    begin
    if assigned(FOnDataReMatch) then FOnDataReMatch(Self);
    LastDataMatch := True;
    end
  end;

end;

procedure TInputDataGrid.LoadFromSnapshot(Snap: TSingleAxisSnapshot);
var
RowNum : Integer;
InEntry : TInputDataRecord;
begin
if assigned(Snap) then
  begin
  RowCount := Snap.GridInputData.Count+1;
  For RowNum := 1 to Snap.GridInputData.Count do
    begin
    InEntry := Snap.GridInputData[RowNum-1];
    Cells[0,Rownum] := Format('%5.3f',[Inentry.Position]);
    Cells[1,Rownum] := Format('%4.1f',[Inentry.InputForwardError]);
    Cells[2,Rownum] := Format('%4.1f',[Inentry.InputReverseError]);
    Cells[3,Rownum] := Format('%4.1f',[Inentry.InputForwardAdjust]);
    Cells[4,Rownum] := Format('%4.1f',[Inentry.InputReverseAdjust]);
    end;
  end;
end;

{procedure TInputDataGrid.LoadFromDataList(InData :TSingleAxisCompDataLists);//;SaveToBuffer : Boolean);
var
RowNum : Integer;
InEntry : TInputDataRecord;
begin
if InData.CurrentInputDataList.Count > 0 then
  begin
  RowCount := InData.CurrentInputDataList.Count+1;
  For RowNum := 1 to InData.CurrentInputDataList.Count do
    begin
    InEntry := InData.CurrentInputDataList[RowNum-1];
    Cells[0,Rownum] := Format('%5.3f',[Inentry.Position]);
    Cells[1,Rownum] := Format('%4.1f',[Inentry.InputForwardError]);
    Cells[2,Rownum] := Format('%4.1f',[Inentry.InputReverseError]);
    Cells[3,Rownum] := Format('%4.1f',[Inentry.InputForwardAdjust]);
    Cells[4,Rownum] := Format('%4.1f',[Inentry.InputReverseAdjust]);
    end;
  end;
//if SaveToBuffer then BufferContentsToSnapShot(Current
end;}


function TInputDataGrid.MergeAndCollapse: Boolean;
var
RowNum : Integer;
Val1,Val2 : Double;
begin
if AllCellsValid then
  begin
  For RowNum := 1 to RowCount-1 do
    begin
    Val1 := StrToFloatDef(Cells[1,RowNum],0);
    Val2 := StrToFloatDef(Cells[3,RowNum],0);
    Cells[1,RowNum] := Format('%5.3f',[Val1+Val2]);
    Val1 := StrToFloatDef(Cells[2,RowNum],0);
    Val2 := StrToFloatDef(Cells[4,RowNum],0);
    Cells[2,RowNum] := Format('%5.3f',[Val1+Val2]);
    Cells[3,RowNum] := '0.0';
    Cells[4,RowNum] := '0.0';
    end;
  AdjustMode := False;
  end;
end;

function TInputDataGrid.SaveToDataList(InData: TSingleAxisCompDataLists):Boolean;
var
RowNum : Integer;
OutEntry : TInputDataRecord;
DataValid : Boolean;
TestVal : Double;
begin
Result := false;
If assigned(InData.CurrentInputDataList) then
  begin
  InData.CurrentInputDataList.Clean;
  //FirstPass checks all data Valid
  DataValid := True;
  For RowNum := 1 to RowCount-1 do
    begin
    try
    TestVal := StrToFloat(Cells[0,Rownum]);
    except
    DataValid := False;
    Cells[0,Rownum] := '0.0'
    end;
    try
    TestVal := StrToFloat(Cells[1,Rownum]);
    except
    DataValid := False;
    Cells[1,Rownum] := '0.0'
    end;
    try
    TestVal := StrToFloat(Cells[2,Rownum]);
    except
    DataValid := False;
    Cells[2,Rownum] := '0.0'
    end;
    try
    TestVal := StrToFloat(Cells[3,Rownum]);
    except
    DataValid := False;
    Cells[3,Rownum] := '0.0'
    end;
    try
    TestVal := StrToFloat(Cells[4,Rownum]);
    except
    DataValid := False;
    Cells[4,Rownum] := '0.0'
    end;
    end;
    For RowNum := 1 to RowCount-1 do
      begin
      OutEntry :=  TInputDataRecord.Create;
      Outentry.Position := StrToFloat(Cells[0,Rownum]);
      Outentry.InputForwardError:= StrToFloat(Cells[1,Rownum]);
      Outentry.InputReverseError := StrToFloat(Cells[2,Rownum]);
      Outentry.InputForwardAdjust:= StrToFloat(Cells[3,Rownum]);
      Outentry.InputReverseAdjust := StrToFloat(Cells[4,Rownum]);
      InData.CurrentInputDataList.Add(OutEntry);
      end;
  end;
  Result := DataValid;
end;

constructor TSingleAxisCompDataLists.Create;
begin
   DataTouched := False;
//   RegisteredInputDataList := TInputDataList.Create;
   CurrentInputDataList:= TInputDataList.Create;
   CurrentInputDataList.OnUpdated := InDataUpdated;
   OutputDataList:= TOutputDataList.Create;
   OutputDataList.OnUpdated := OutDataUpdated;
   AxisIsLinear := True;
   EncoderOffsetValid := False;
   FMakeOutput := True;
end;

destructor TSingleAxisCompDataLists.Destroy;
begin
Clean;
OutputDataList.Free;
CurrentInputDataList.Free;
//RegisteredInputDataList.Free;
end;


{ TAvailabityRecord }


constructor TAvailabityandStatusData.Create;
begin
   ImportDataAvailableAllAxes := False;
   WorkingFileLoaded := False;
   WorkingFileLoaded := False;
   PowerUpMode := cpuUnknown;
end;

procedure TAvailabityandStatusData.DataForAxisNameChanged(Axisname: String);
begin
if Uppercase(AxisName) = 'X'then
  begin
  XData.Touched := True;
  end else
if Uppercase(AxisName) = 'Y' then
  begin
  YData.Touched := True;
  end else
if Uppercase(AxisName) = 'Z' then
  begin
  ZData.Touched := True;
  end else
if Uppercase(AxisName) = 'A' then
  begin
  AData.Touched := True;
  end else
if Uppercase(AxisName) = 'B' then
  begin
  BData.Touched := True;
  end else
if Uppercase(AxisName) = 'C' then
  begin
  CData.Touched := True;
  end;
end;

function TAvailabityandStatusData.Importloaded: Boolean;
begin
ImportloadedAndMatched; // this performs test and also conditions th e AllImportLoaded variable
result := AllImportLoaded;
end;

function TAvailabityandStatusData.DataHasBeenTouched: Boolean;
begin
Result := False;
if  Xdata.Required and (Xdata.Touched) then Result := True;
if  YData.Required and (YData.Touched) then Result := True;
if  ZData.Required and (ZData.Touched) then Result := True;
if  AData.Required and (AData.Touched) then Result := True;
if  BData.Required and (BData.Touched) then Result := True;
if  CData.Required and (CData.Touched) then Result := True;

end;

function TAvailabityandStatusData.AllImportAvailable: Boolean;
begin
Result := True;
if  Xdata.Required and Not(Xdata.ImportExists) then Result := False;
if  YData.Required and Not(YData.ImportExists) then Result := False;
if  ZData.Required and Not(ZData.ImportExists) then Result := False;
if  AData.Required and Not(AData.ImportExists) then Result := False;
if  BData.Required and Not(BData.ImportExists) then Result := False;
if  CData.Required and Not(CData.ImportExists) then Result := False;
end;


function TAvailabityandStatusData.WorkingMachineIDMatches(FilePath : String;CurrentID : String;var FileIdString: String): Boolean;
var
EFile : TIniFile;
begin
Result := False;
if FileExists(FilePath) then
  begin
  EFile := TIniFile.Create(FilePath);
  FileIdString := Efile.ReadString('Dynamic','MachineID','0000');
  Result := FileIdString = CurrentID;
  end;
end;

function TAvailabityandStatusData.SetWorkingFileMacId(FilePath : String;IdStr: String): Boolean;
var
EFile : TIniFile;
begin
Result := False;
if FileExists(FilePath) then
  begin
  EFile := TIniFile.Create(FilePath);
  try
  Efile.WriteString('Dynamic','MachineID',IdStr);
  Result := True;
  finally
  EFile.Free
  end;
  end;
end;

function TAvailabityandStatusData.WorkingLoadedandMatches: Boolean;
begin
Result :=  IsSatisfied;
if Result then
  begin
  if PowerUpMode = cpuFullyRegistered then
    begin
    if  Xdata.Required and Not(XData.RegisteredSettingsMatch) then Result := False;
    if  Ydata.Required and Not(YData.RegisteredSettingsMatch) then Result := False;
    if  Zdata.Required and Not(ZData.RegisteredSettingsMatch) then Result := False;
    if  Adata.Required and Not(AData.RegisteredSettingsMatch) then Result := False;
    if  Bdata.Required and Not(BData.RegisteredSettingsMatch) then Result := False;
    if  Cdata.Required and Not(CData.RegisteredSettingsMatch) then Result := False;
    end
  else
    begin
    if  Xdata.Required and Not(XData.DataMatchByOutput) then Result := False;
    if  Ydata.Required and Not(YData.DataMatchByOutput) then Result := False;
    if  Zdata.Required and Not(ZData.DataMatchByOutput) then Result := False;
    if  Adata.Required and Not(AData.DataMatchByOutput) then Result := False;
    if  Bdata.Required and Not(BData.DataMatchByOutput) then Result := False;
    if  Cdata.Required and Not(CData.DataMatchByOutput) then Result := False;
    end;
  end;
end;

function TAvailabityandStatusData.DataMatchesForAxis(AxIndex: Integer): Boolean;
var
AxData : TAvailabilityAndStatusRecord;
Found : Boolean;
begin
Result := False;
AxData := DataForAxisWithIndex(AxIndex,Found);
if Found then
  begin
  Result := AxData.DataMatchByOutput;
  end;
end;


function TAvailabityandStatusData.ResolveEntryCountForAxisAtIndex(AxIndex: Integer;var AxisCompData : TSingleAxisData;var MadeAChange : Boolean): Boolean;
var
AxData : TAvailabilityAndStatusRecord;
Found : Boolean;
DataRatio : Double;
begin
Result := False;
MadeAChange := False;
AxData := DataForAxisWithIndex(AxIndex,Found);
if Found then
  begin
  if Axdata.ExistingCompEntries <> Axdata.InternalCompEntries then
    begin
    if Axdata.InternalCompEntries > 0 then
      begin
      DataRatio := Axdata.ExistingCompEntries/Axdata.InternalCompEntries;
      if (DataRatio < 0.6) and (DataRatio > 0.4) then
        begin
        AxisCompData.DoublePointsOnOutput := False;
        MadeAChange := True;
        end
      else if (DataRatio < 2.3) and (DataRatio > 1.8) then
        begin
        AxisCompData.DoublePointsOnOutput := True;
        MadeAChange := True;
        end
      else
        begin
        Result := False;
        end
      end
    else
       begin
       Result := False;
       end
    end;
  end;
end;


{function TAvailabityandStatusData.ImportDataExistsForAxisWithname(AxName : String): Boolean;
var
AxData : TAvailabilityAndStatusRecord;
Found : Boolean;
begin
Result := False;
AxData := DataForAxisName(AxName,Found);
if Found then
  begin
  Result := AxData.
  end
end;}

function TAvailabityandStatusData.DataExistsForAxisWithIndex(AxIndex: Integer): Boolean;
var
AxData : TAvailabilityAndStatusRecord;
Found : Boolean;
begin
Result := False;
AxData := DataForAxisWithIndex(AxIndex,Found);
if Found then
 begin
 Result :=  not ((AXdata.WorkingDataState = tcdUnavailable) and (AxData.ImportDataState = tcdNoCompNoImport));
 end

end;

function RegStat(Data : TAvailabilityAndStatusRecord;var State : String;var ReasonStr : String): Boolean;
begin
State := 'MisMatch';
Result := False;
If Data.RegisteredSettingsMatch and Data.RegisteredDataMatch then
  begin
  State := 'OK';
  ReasonStr  := '';
  Result := True;
  end
else If (not Data.RegisteredSettingsMatch) and (not Data.RegisteredDataMatch) then
  ReasonStr  := 'Data and Settings Mismatch'
else If Data.RegisteredSettingsMatch and not Data.RegisteredDataMatch then
  ReasonStr  := 'Data Mismatch'
else
  ReasonStr  := 'Settings Mismatch';


end;

function TAvailabityandStatusData.RegisteredStatusForAxis(AxisName: String;var StateStr : String;var ReasonString : String): Boolean;
begin
Result := False;
ReasonString := '';
if not (PowerUpMode = cpuFullyRegistered) then exit;
if Axisname = 'X'  then
  begin
  Result := RegStat(XData,StateStr,ReasonString);
  end
else if Axisname = 'Y'  then
  begin
  Result := RegStat(YData,StateStr,ReasonString);
  end
else if Axisname = 'Z'  then
  begin
  Result := RegStat(ZData,StateStr,ReasonString);
  end
else if Axisname = 'A'  then
  begin
  Result := RegStat(AData,StateStr,ReasonString);
  end
else if Axisname = 'B'  then
  begin
  Result := RegStat(BData,StateStr,ReasonString);
  end
else if Axisname = 'C'  then
  begin
  Result := RegStat(CData,StateStr,ReasonString);
  end;

end;

function TAvailabityandStatusData.DataForAxisName(AxisName: String;var Found : Boolean): TAvailabilityAndStatusRecord;
var
TestStr : String;
begin
Found := False;
TestStr := Uppercase(AxisName);
if Axisname = 'X'  then
  begin
  Result := XData;
  Found := True;
  end
else if Axisname = 'Y'  then
  begin
  Result := YData;
  Found := True;
  end
else if Axisname = 'Z'  then
  begin
  Result := ZData;
  Found := True;
  end
else if Axisname = 'A'  then
  begin
  Result := AData;
  Found := True;
  end
else if Axisname = 'B'  then
  begin
  Result := BData;
  Found := True;
  end
else if Axisname = 'C'  then
  begin
  Result := CData;
  Found := True;
  end;
end;

function TAvailabityandStatusData.DataForAxisWithIndex(AxIndex: Integer;var Found : Boolean): TAvailabilityAndStatusRecord;
begin
Found := False;
if Xdata.AxisIndex = AxIndex then
  begin
  Found := True;
  Result := XData;
  end
else if Ydata.AxisIndex = AxIndex then
  begin
  Found := True;
  Result := YData;
  end
else if Zdata.AxisIndex = AxIndex then
  begin
  Found := True;
  Result := ZData
  end
else if Adata.AxisIndex = AxIndex then
  begin
  Found := True;
  Result := AData
  end
else if Bdata.AxisIndex = AxIndex then
  begin
  Found := True;
  Result := BData
  end
else if Cdata.AxisIndex = AxIndex then
  begin
  Found := True;
  Result := CData
  end
end;

function TAvailabityandStatusData.ImportloadedAndMatched: Boolean;
begin
Result := True;
AllImportLoaded := False;
AllImportLoadedandMatched := True;
if  Xdata.Required and Not(Xdata.ImportDataState = tcdLoaded) then Result := False;
if  YData.Required and Not(YData.ImportDataState = tcdLoaded) then Result := False;
if  ZData.Required and Not(ZData.ImportDataState = tcdLoaded) then Result := False;
if  AData.Required and Not(AData.ImportDataState = tcdLoaded) then Result := False;
if  BData.Required and Not(BData.ImportDataState = tcdLoaded) then Result := False;
if  CData.Required and Not(CData.ImportDataState = tcdLoaded) then Result := False;

if Result Then
  begin
  AllImportLoaded := True;
  if  Xdata.Required and Not(XData.DataMatchByOutput) then Result := False;
  if  Ydata.Required and Not(YData.DataMatchByOutput) then Result := False;
  if  Zdata.Required and Not(ZData.DataMatchByOutput) then Result := False;
  if  Adata.Required and Not(AData.DataMatchByOutput) then Result := False;
  if  Bdata.Required and Not(BData.DataMatchByOutput) then Result := False;
  if  Cdata.Required and Not(CData.DataMatchByOutput) then Result := False;
  end;
AllImportLoadedandMatched := Result;
end;

procedure TAvailabityandStatusData.SetRegisteredMatchForAxis(AxisName: String; SettingsMatch, DataMatch: Boolean);
begin
if AxisName = 'X' then
  begin
  XData.RegisteredSettingsMatch := SettingsMatch;
  XData.RegisteredDataMatch := DataMatch;
  end
else if AxisName = 'Y' then
  begin
  YData.RegisteredSettingsMatch := SettingsMatch;
  YData.RegisteredDataMatch := DataMatch;
  end
else if AxisName = 'Z' then
  begin
  ZData.RegisteredSettingsMatch := SettingsMatch;
  ZData.RegisteredDataMatch := DataMatch;
  end
else if AxisName = 'A' then
  begin
  AData.RegisteredSettingsMatch := SettingsMatch;
  AData.RegisteredDataMatch := DataMatch;
  end
else if AxisName = 'B' then
  begin
  BData.RegisteredSettingsMatch := SettingsMatch;
  BData.RegisteredDataMatch := DataMatch;
  end
else if AxisName = 'C' then
  begin
  CData.RegisteredSettingsMatch := SettingsMatch;
  CData.RegisteredDataMatch := DataMatch;
  end;
end;


function TAvailabityandStatusData.CheckAllExistingCompSatisfiedandMatched:Boolean;
begin
Result := True;
AllImportLoaded := False;
AllImportLoadedandMatched := True;
if  EnforcedCompExistence.XCompExists and Not(Xdata.ImportDataState = tcdLoaded) then Result := False;
if  EnforcedCompExistence.YCompExists and Not(YData.ImportDataState = tcdLoaded) then Result := False;
if  EnforcedCompExistence.ZCompExists and Not(ZData.ImportDataState = tcdLoaded) then Result := False;
if  EnforcedCompExistence.ACompExists and Not(AData.ImportDataState = tcdLoaded) then Result := False;
if  EnforcedCompExistence.BCompExists and Not(BData.ImportDataState = tcdLoaded) then Result := False;
if  EnforcedCompExistence.CCompExists and Not(CData.ImportDataState = tcdLoaded) then Result := False;

if Result Then
  begin
  AllImportLoaded := True;
  if  EnforcedCompExistence.XCompExists and Not(XData.DataMatchByOutput) then Result := False;
  if  EnforcedCompExistence.YCompExists and Not(YData.DataMatchByOutput) then Result := False;
  if  EnforcedCompExistence.ZCompExists and Not(ZData.DataMatchByOutput) then Result := False;
  if  EnforcedCompExistence.ACompExists and Not(AData.DataMatchByOutput) then Result := False;
  if  EnforcedCompExistence.BCompExists and Not(BData.DataMatchByOutput) then Result := False;
  if  EnforcedCompExistence.CCompExists and Not(CData.DataMatchByOutput) then Result := False;
  end;
AllImportLoadedandMatched := Result;
end;


function TAvailabityandStatusData.IsSatisfied : Boolean;
begin
if POwerUpMode = cpuFullyRegistered then
  begin
  Result := True;
  //// TBD

  end
else
  begin
  Result := True;
  if  EnforcedCompExistence.XCompExists and Not(Xdata.WorkingDataState = tcdLoaded) then Result := False;
  if  EnforcedCompExistence.YCompExists and Not(YData.WorkingDataState = tcdLoaded) then Result := False;
  if  EnforcedCompExistence.ZCompExists and Not(ZData.WorkingDataState = tcdLoaded) then Result := False;
  if  EnforcedCompExistence.ACompExists and Not(AData.WorkingDataState = tcdLoaded) then Result := False;
  if  EnforcedCompExistence.BCompExists and Not(BData.WorkingDataState = tcdLoaded) then Result := False;
  if  EnforcedCompExistence.CCompExists and Not(CData.WorkingDataState = tcdLoaded) then Result := False;
  end;
end;

function TAvailabityandStatusData.IsOKForFirstLoadConfirm: Boolean;
begin
Result := True;
if XData.Required and not Xdata.ImportExists then
  begin
  if EnforcedCompExistence.XCompExists then Result := False;
  end;

if YData.Required and not Ydata.ImportExists then
  begin
  if EnforcedCompExistence.YCompExists then Result := False;
  end;

if ZData.Required and not Zdata.ImportExists then
  begin
  if EnforcedCompExistence.ZCompExists then Result := False;
  end;

if AData.Required and not Adata.ImportExists then
  begin
  if EnforcedCompExistence.ACompExists then Result := False;
  end;

if BData.Required and not Bdata.ImportExists then
  begin
  if EnforcedCompExistence.BCompExists then Result := False;
  end;

if CData.Required and not Cdata.ImportExists then
  begin
  if EnforcedCompExistence.CCompExists then Result := False;
  end;

end;


procedure TAvailabityandStatusData.UpdateFromDefaultSettings(DefaultSettings: TDefaultSettingsList);
var
AxNum  : Integer;
Defaults : TDefaultSettingEntry;
LoopCount : Integer;
Fault : Boolean;
begin
AxNum := 0;
LoopCount := 0;
Fault := False;
XData.Required := False;
YData.Required := False;
ZData.Required := False;
AData.Required := False;
BData.Required := False;
CData.Required := False;
while (AxNum < DefaultSettings.Count) and (not Fault) do
  begin
  try
  Defaults := DefaultSettings[AxNum];
  if Uppercase(Defaults.AxisName) = 'X' then
    begin
    Defaults.AxisName := 'X';
    XData.Required := True;
    XData.AxisIndex := Axnum;
    inc(Axnum);
    end
  else if Uppercase(Defaults.AxisName) = 'Y' then
    begin
    Defaults.AxisName := 'Y';
    YData.Required := True;
    YData.AxisIndex := Axnum;
    inc(Axnum);
    end
  else if Uppercase(Defaults.AxisName) = 'Z' then
    begin
    Defaults.AxisName := 'Z';
    ZData.Required := True;
    ZData.AxisIndex := Axnum;
    inc(Axnum);
    end
  else if Uppercase(Defaults.AxisName) = 'A' then
    begin
    Defaults.AxisName := 'A';
    AData.Required := True;
    AData.AxisIndex := Axnum;
    inc(Axnum);
    end
  else if Uppercase(Defaults.AxisName) = 'B' then
    begin
    Defaults.AxisName := 'B';
    BData.Required := True;
    BData.AxisIndex := Axnum;
    inc(Axnum);
    end
  else if Uppercase(Defaults.AxisName) = 'C' then
    begin
    Defaults.AxisName := 'C';
    CData.Required := True;
    CData.AxisIndex := Axnum;
    inc(Axnum);
    end;
  finally
  inc(LoopCount);
  Fault := (LoopCount <> AxNum)
  end;
  end;
end;


procedure TInputDataGrid.GoToBottom;
begin
   if AdjustMode then
     begin
     CurrentCol := 3;
     end
   else
     begin
     CurrentCol := 1;
     end;
   CurrentRow := RowCount-1;
   CellFocus(CurrentCol,CurrentRow);
end;

procedure TInputDataGrid.GoToTop;
begin
   If AdjustMode Then
    begin
    CurrentCol := 3;
    end
   else
    begin
    CurrentCol := 1;
    end;
   CurrentRow := 1;
   CellFocus(CurrentCol,CurrentRow);
   inherited;
end;

function TInputDataGrid.AllCellsValid: Boolean;
var
R : Integer;
NumVal : Double;
begin
  result := True;
  R := 1;
  while (R <RowCount) and Result do
    begin
    try
    NumVal := StrToFloat(Cells[1,R]);
    except
    Result := False;
    end;
  if Result then
    begin
    try
    NumVal := StrToFloat(Cells[2,R]);
    except
    Result := False;
    end
    end;
  if AdjustMode and Result then
    begin
    try
    NumVal := StrToFloat(Cells[3,R]);
    except
    Result := False;
    end
    end;
  if Result then
    begin
    try
    NumVal := StrToFloat(Cells[4,R]);
    except
    Result := False;
    end
    end;
  Inc(R);
  end;
end;

procedure TInputDataGrid.BufferContentsToSnapShot(SnapShot: TSingleAxisSnapshot);
var
ENum : INteger;
BEntry : TInputDataRecord;
Val : Double;
begin

Snapshot.GridInputData.Clean;
if RowCount > 1 then
  begin
  For ENum := 1 To RowCount-1 do
    begin
    BEntry := TInputDataRecord.Create;
    BEntry.Position := StrToFloatDef(Cells[0,ENum],0);
    Bentry.InputForwardError := StrToFloatDef(Cells[1,ENum],0);
    Bentry.InputReverseError := StrToFloatDef(Cells[2,ENum],0);
    Bentry.InputForwardAdjust := StrToFloatDef(Cells[3,ENum],0);
    Bentry.InputReverseAdjust := StrToFloatDef(Cells[4,ENum],0);
    Snapshot.GridInputData.Add(Bentry);
    end;
  end;

end;

procedure TInputDataGrid.RegisterSnapshot(SnapShot: TSingleAxisSnapshot);
begin
CurrentSnapShot := SnapShot;
end;

{procedure TInputDataGrid.BufferContents;
var
ENum : INteger;
BEntry : TInputDataRecord;
Val : Double;
begin
CleanBuffer;
if RowCount > 1 then
  begin
  For ENum := 1 To RowCount-1 do
    begin
    BEntry := TInputDataRecord.Create;
    BEntry.Position := StrToFloatDef(Cells[0,ENum],0);
    Bentry.InputForwardError := StrToFloatDef(Cells[1,ENum],0);
    Bentry.InputReverseError := StrToFloatDef(Cells[2,ENum],0);
    Bentry.InputForwardAdjust := StrToFloatDef(Cells[3,ENum],0);
    Bentry.InputReverseAdjust := StrToFloatDef(Cells[4,ENum],0);
    BufferList.Add(Bentry);
    end;
  end;
  if Not LastDataMatch then
    begin
    if assigned(FOnDataReMatch) Then FOnDataReMatch(Self);
    LastDataMatch := True;
    end
end;}

{function TInputDataGrid.RevertToBuffer: Boolean;
var
ENum : INteger;
BEntry : TInputDataRecord;
Val : Double;
Rownum : Integer;
begin
if BufferList.Count > 1 then
  begin
  RowCount := BufferList.Count+1;
  For ENum := 0 To BufferList.Count-1 do
    begin
    RowNum := Enum +1;
    BEntry := BufferList[Enum];
    Cells[0,Rownum] := Format('%5.3f',[BEntry.Position]);
    Cells[1,Rownum] := Format('%4.1f',[BEntry.InputForwardError]);
    Cells[2,Rownum] := Format('%4.1f',[BEntry.InputReverseError]);
    Cells[3,Rownum] := Format('%4.1f',[BEntry.InputForwardAdjust]);
    Cells[4,Rownum] := Format('%4.1f',[BEntry.InputReverseAdjust]);
    end
  end;
  LastDataMatch := True;
end;}

procedure TInputDataGrid.SaveToTempLoadBuffer;
var
BEntry : TInputDataRecord;
ENum : INteger;
begin
CleanLoadBuffer;
For ENum := 1 to Rowcount-1 do
  begin
  BEntry := TInputDataRecord.Create;
  Bentry.Position := StrToFloatDef(Cells[0,Enum],0);
  Bentry.InputForwardError := StrToFloatDef(Cells[1,Enum],0);
  BEntry.InputReverseError := StrToFloatDef(Cells[2,Enum],0);
  BEntry.InputForwardAdjust := 0;
  BEntry.InputReverseAdjust := 0;
  TemporaryLoadBuffer.Add(BEntry);
  end;
end;

procedure TInputDataGrid.CleanLoadBuffer;
var
BEntry : TInputDataRecord;
begin
while TemporaryLoadBuffer.Count > 0 do
  begin
  Bentry := TemporaryLoadBuffer[0];
  BEntry.Free;
  TemporaryLoadBuffer.Delete(0);
  end;
end;

procedure TInputDataGrid.LoadFromLoadBufferandClean;
var
BEntry : TInputDataRecord;
Rownum : Integer;
begin
if TemporaryLoadBuffer.Count > 0 then
  begin
  RowCount := TemporaryLoadBuffer.Count+1;
  For Rownum := 0 to TemporaryLoadBuffer.Count-1 do
    begin
    Bentry := TemporaryLoadBuffer[Rownum];
    Cells[0,Rownum+1] := Format('%5.3f',[BEntry.Position]);
    Cells[1,Rownum+1] := Format('%4.1f',[BEntry.InputForwardError]);
    Cells[2,Rownum+1] := Format('%4.1f',[BEntry.InputReverseError]);
    Cells[3,Rownum+1] := Format('%4.1f',[BEntry.InputForwardAdjust]);
    Cells[4,Rownum+1] := Format('%4.1f',[BEntry.InputReverseAdjust]);
    end;
  end;
  CleanLoadBuffer;

 if not MatchesSnapShot then
  begin
  if LastDataMatch then
    begin
    if assigned(FOnDataDifferent) then FOnDataDifferent(-1,-1,'0',0);
    end;
  LastDataMatch := False;
  end
else
  begin
  if Not LastDataMatch then
    begin
    if assigned(FOnDataReMatch) then FOnDataReMatch(Self);
    LastDataMatch := True;
    end
  end;


end;


{procedure TInputDataGrid.CleanBuffer;
var
BEntry : TInputDataRecord;
begin
while BufferList.Count > 0 do
  begin
  Bentry := BufferList[0];
  BEntry.Free;
  BufferList.Delete(0);
  end;
end;}

{destructor TInputDataGrid.Destroy;
begin
//CleanBuffer;
//BufferList.Free;
end;}

function TInputDataGrid.MatchesSnapShot: Boolean;
var
BEntry : TInputDataRecord;
RNum : INteger;
begin
Result := True;
if not assigned(CurrentSnapshot) then
  begin
  Result :=False;
  exit;
  end;
If CurrentSnapshot.GridInputData.Count = RowCount-1 then
  begin
  RNum := 1;
  While (RNum < RowCount) and (Result = True) do
    begin
    try
    Bentry := CurrentSnapshot.GridInputData[RNum-1];
    if Bentry.Position <> StrToFloatDef(Cells[0,RNum],0) then Result := False
    else if Bentry.InputForwardError <> StrToFloatDef(Cells[1,RNum],0) then Result := False
    else if Bentry.InputReverseError <> StrToFloatDef(Cells[2,RNum],0) then Result := False
    else if Bentry.InputForwardAdjust <> StrToFloatDef(Cells[3,RNum],0) then Result := False
    else if Bentry.InputReverseAdjust <> StrToFloatDef(Cells[4,RNum],0) then Result := False
    finally
    inc(RNum)
    end;
    end;
  end
else
  begin
  Result := False
  end
end;


procedure TInputDataGrid.SetAdjustMode(const Value: Boolean);
begin
  Visible := False;
  try
  FAdjustMode := Value;
  if  FAdjustMode then
    begin
    Width := FAdjustWidth;
    ColWidths[0] := 60;
    ColWidths[1] := ((FAdjustWidth-60)div 4)-8;
    ColWidths[2] := ColWidths[1];
    ColWidths[3] := ColWidths[1];
    ColWidths[4] := ColWidths[1];
    FixedCols := 3;
    end
  else
    begin
    Width := StandardWidth;
    ColWidths[0] := 60;
    ColWidths[1] := ((StandardWidth-60)div 2)-15;
    ColWidths[2] := ColWidths[1];
    ColWidths[3] := 0;
    ColWidths[4] := 0;
    FixedCols := 1;
    end;
  CurrentRow := 1;
  if  FAdjustMode then
    begin
    CurrentCol := 3;
    end
  else
    begin
    CurrentCol := 1;
    end;
  finally
  Visible := True;
  CellFocus(CurrentCol,CurrentRow);
  end;
  try
  SetFocus;
  except
  end;
end;

procedure TInputDataGrid.SetStandardWidth(const Value: Integer);
begin
  FStandardWidth := Value;
  if not AdjustMode then SetAdjustMode(False);
end;

procedure TInputDataGrid.SetAdjustWidth(const Value: Integer);
begin
  FADjustWidth := Value;
  if AdjustMode then SetAdjustMode(True);
end;

procedure TInputDataGrid.CellFocus(CCol,CRow : Integer);
var
Valid : Boolean;
begin
  Valid := True;
  try
  FocusCell(CCol,CRow,True);
  except
  Valid := False;
  end;
  if Valid then  CurrentStrVal := Cells[CCol,CRow];
end;


procedure TInputDataGrid.CleanAdjustBuffer;
begin
LastAdjustBuffer.Clean;
end;

procedure TInputDataGrid.BufferAndStoreAdjustments(AxisName : String; CFilePath : String);
var
RowNum : Integer;
AdjustEntry : TAdjustData;
begin
CleanAdjustBuffer;
if FileExists(CFilePath) then
  begin
  if RowCount > 1 then
    begin
    For RownUm := 1 to RowCount-1 do
      begin
      AdjustEntry := TAdjustData.Create;
      AdjustEntry.InputForwardAdjust := StrToFloatDef(Cells[3,Rownum],0.0);
      AdjustEntry.InputReverseAdjust := StrToFloatDef(Cells[4,Rownum],0.0);
      end;
    end;
  end;






end;


function TInputDataGrid.GetDataMatches: Boolean;
begin
Result := MatchesSnapShot;
end;


function TInputDataGrid.CellMatchesSnapshot(ACol,Arow : INteger): Boolean;
var
BEntry : TInputDataRecord;
begin;
Result := True;
if assigned(CurrentSnapShot) then
  begin
  if (ARow) > CurrentSnapshot.GridInputData.Count then
    begin
    Result := False;
    exit;
    end;
  if (ARow < RowCount) and (ARow > 0) then
    begin
    try
    Bentry := CurrentSnapshot.GridInputData[ARow-1];
    except
    Result := False;
    exit;
    end;
    case ACol of
    0: if Bentry.Position <> StrToFloatDef(Cells[0,ARow],0) then Result := False;
    1: if Bentry.InputForwardError <> StrToFloatDef(Cells[1,ARow],0) then Result := False;
    2: if Bentry.InputReverseError <> StrToFloatDef(Cells[2,ARow],0) then Result := False;
    3: if Bentry.InputForwardAdjust <> StrToFloatDef(Cells[3,ARow],0) then Result := False;
    4: if Bentry.InputReverseAdjust <> StrToFloatDef(Cells[4,ARow],0) then Result := False;
    end
  end;
end;
end;

procedure TInputDataGrid.MyDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
ValStr : String;
Toffset : Integer;
StdColor1 : TColor;
StdColor2 : TColor;
FColor    : TColor;
begin
try
TOffset := 2;
StdColor1 := clWebBeige;
StdColor2 := clwebIvory;
FColor := ClBlack;
if assigned(CurrentSnapShot) and FShowEdits then
  begin
  StdColor1 := clWebBeige;
  StdColor2 := clwebIvory;
  if CellMatchesSnapshot(ACol,ARow) then
    begin
    FColor := ClBlack;
//    StdColor1 := clWebBeige;
//    StdColor2 := clwebIvory;
    end
  else
    begin
    FColor  := clRed;
    
//    StdColor1 := clWebLightSalmon;
//    StdColor2 := clWebLightSalmon;
    end;
  end;

  with Canvas do
  begin
  If (Acol = CurrentCol) and (ARow = CurrentRow) then
    begin
    Font.Color := clWhite;
    Brush.Color := clWebDodgerBlue;
    end
  else If (ACol < FixedCols) or (ARow< FixedRows) then
    begin
    Brush.Color := clLtGray;
    Font.Color := clBlack;
    if ARow=0 then TOffset := 6;
    end
  else
    begin
    Font.Color := FColor;
    if ((ARow div 2)*2) = ARow then
      begin
      Brush.Color := StdColor1;
      end
    else
      begin
      Brush.Color := StdColor2;
      end
    end;

    FillRect(Rect);
    ValStr := Cells[ACol,Arow];
    TextOut(Rect.Left+TOffset,Rect.Top+2,ValStr);
  end;
  except
  end;
end;


procedure TInputDataGrid.SetShowEdits(const Value: Boolean);
begin
  FShowEdits := Value;
  Invalidate;
end;

{ TCalAxisList }
procedure TCalAxisList.Clean;
var
AxEntry : TSingleAxisData;
begin
 while Count > 0 do
  begin
  AxEntry := Items[Count-1];
  AXentry.Free;
  Delete(Count-1);
  end;
end;

procedure TCalAxisList.CleanCompareList;
var
AxEntry : TSingleAxisData;
CLCount : Integer;
begin
 while CompareList.Count > 0 do
  begin
  AxEntry := CompareList.Items[0];
  try
  AxEntry.Free;
  CompareList.Delete(0);
  except
  CompareList.Delete(0);
  end;
  end;
end;

function TCalAxisList.AddAxisCalled(Axname: String;ForCompare : Boolean): TSingleAxisData;
begin
Result := TSingleAxisData.Create;
Result.AxisName :=AxName;
if ForCompare then
  begin
  Result.ParentListIndex := CompareList.Count;
  Result.OnInputDataUpdated := nil;
  Result.OnOutputDataUpdated := nil;
  if assigned(CompareList) then CompareList.Add(Result)
  end
else
  begin
  Result.OnInputDataUpdated := InputDataHasChanged;
  Result.OnOutputDataUpdated := OutputDataHasChanged;
  Result.ParentListIndex := Count;
  Add(Result);
  end;
end;


function TCalAxisList.EntryAtIndex(Index: INteger): TSingleAxisData;
begin
Result := nil;
if (Index < Count) and (Index >=0) then
  begin
  Result := TSingleAxisData(Items[Index]);
  end
end;

function TCalAxisList.EntryWithMotorNumber(MNumber: Integer): TSingleAxisData;
var
Enum : Integer;
Entry : TSingleAxisData;
Found : Boolean;
begin
Result := nil;
Found := False;
ENum := 0;
while (Enum < Count) and (not Found) do
  begin
  try
  Entry := Items[Enum];
  Found := Entry.AxisMotorNumber = MNumber;
  Finally
  inc(Enum)
  end;
  end;
If Found then Result := Entry;
end;


procedure TCalAxisList.InputDataHasChanged(LIndex: Integer; ZShift: Double; Valid: Boolean);
begin
if assigned(FOnInputaDataUpdated) then FOnInputaDataUpdated(Lindex,ZShift,Valid);
end;

procedure TCalAxisList.OutputDataHasChanged(LIndex: Integer; Valid: Boolean);
begin
if assigned(FOnOutputaDataUpdated) then FOnOutputaDataUpdated(Lindex,Valid);
end;

constructor TSingleAxisData.Create;
begin
 WorkingFileStatusBuffer:= TSingleAxisSnapshot.Create;
 CurrentStatusBuffer    := TSingleAxisSnapshot.Create;
 RegisteredStatusBuffer := TSingleAxisSnapshot.Create;
 CompareStatusBuffer    := TSingleAxisSnapshot.Create;
 CompensationDataLists  := TSingleAxisCompDataLists.Create;
 CompensationDataLists.OnInputDataUpdated := InputDataUpdated;
 CompensationDataLists.OnOutputDataUpdated := OutputDataUpdated;
 ParentListIndex := -1;
 FAxisName := '';
end;

destructor TSingleAxisData.Destroy;
begin
WorkingFileStatusBuffer.Free;
CurrentStatusBuffer.Free;
CompareStatusBuffer.Free;
CompensationDataLists.Clean;
CompensationDataLists.Free;
inherited;
end;


function TSingleAxisData.GetEncoderCountsPerUnit: Double;
begin
Result := 0;
if assigned(CompensationDataLists) then Result := CompensationDataLists.aaEncoderCountsPerUnit;
end;


procedure TSingleAxisData.SetEncoderCountsPerUnit(const Value: Double);
begin
if assigned(CompensationDataLists) then CompensationDataLists.aaEncoderCountsPerUnit := Value;
end;

procedure TSingleAxisData.InputDataUpdated(ZShift: Double; Valid: Boolean);
begin
if assigned(FOnInputDataUpdated) then  FOnInputDataUpdated(ParentListIndex,ZShift,Valid);
end;

procedure TSingleAxisData.OutputDataUpdated(Valid : Boolean);
begin
if assigned(FOnOutputDataUpdated) then  FOnOutputDataUpdated(ParentListIndex,Valid);
end;

procedure TSingleAxisData.SetDoublePoints(const Value: Boolean);
begin
  FDoublePoints := Value;
  if assigned(CompensationDataLists) then CompensationDataLists.DoublePointsOnOutput := FDoublePoints;
end;

procedure TSingleAxisData.SetAxisName(const Value: String);
begin
  FAxisName := Value;
  CompensationDataLists.AxisLetter := Value;
end;

procedure TSingleAxisData.SetMotorNumber(const Value: Integer);
begin
  CompensationDataLists.AxisMotorNumber := Value;
end;

function TSingleAxisData.GetMotorNumber: Integer;
begin
Result := -1;
if assigned(CompensationDataLists) then
  begin
  Result := CompensationDataLists.AxisMotorNumber;
  end;
end;

procedure TSingleAxisData.WriteToWorkingFile(FilePath: String);
begin
if assigned(CompensationDataLists) then
    begin
    CompensationDataLists.WriteToFile(FilePath);
    RecordStatus;
    end;
end;

procedure TSingleAxisData.BufferStatusTo(ABuffer: TAxisBuffer);
begin
if assigned(CompensationDataLists) then
  begin
  ABuffer.DoublePoints := CompensationDataLists.DoublePointsOnOutput;
  ABuffer.BacklashUsed := CompensationDataLists.BacklashUsed;
  ABuffer.IsLinear := CompensationDataLists.AxisIsLinear;
  ABuffer.MNumber := CompensationDataLists.AxisMotorNumber;
  ABuffer.EncoderCountsPerUnit := EncoderCountsPerUnit;
  ABuffer.FirstPosition := CompensationDataLists.FirstPosition;
  ABuffer.NumberOfEntries := CompensationDataLists.NumberOfEntries;
  ABuffer.CompIncrement := CompensationDataLists.CompIncrement;
  ABuffer.HomeOffset := CompensationDataLists.CompHomeOffset;
  ABuffer.PositiveLimit := CompensationDataLists.CompPositiveLimit;
  ABuffer.NegativeLimit  := CompensationDataLists.CompNegativeLimit;
  ABuffer.ManualBacklashMicron  := CompensationDataLists.ManualBacklashMicron;
  ABuffer.MakeOutPut := MakeOutput;
  end;
end;


procedure TSingleAxisData.InitialiseFromDefaults(Default: TDefaultSettingEntry);
begin
  AxisName := Default.AxisName;
  EncoderCountsPerUnit := Default.EncoderCountsPerUnit;
  CompensationDataLists.AxisIsLinear := Default.IsLinear;
  CompensationDataLists.FirstPosition := Default.FirstEntryPos;
  CompensationDataLists.CompIncrement := Default.Step;
  CompensationDataLists.NumberOfEntries   := Default.NumEntries;
  CompensationDataLists.DoublePointsOnOutput := Default.DoublePointsOnOutput;
  CompensationDataLists.BacklashUsed := Default.UseBackLashTable;
//  CompensationDataLists.CompPositiveLimit := Default.PosLimit;
//  CompensationDataLists.CompNegativeLimit := Default.NegLimit;
  CompensationDataLists.CompHomeOffset := 0;
  CompensationDataLists.ManualBacklashMicron := 0;
  CompensationDataLists.MakeOutput := False;
  AxisMotorNumber := Default.MotorNumber;
  CompensationDataLists.CurrentInputDataList.aaInitialise(CompensationDataLists.FirstPosition,CompensationDataLists.CompIncrement,CompensationDataLists.NumberOfEntries);
  CompensationDataLists.OutputDataList.UpdateFromInputDataList(CompensationDataLists.CurrentInputDataList);
  RecordStatus;
end;


{TCalAxisList}

function TCalAxisList.EntryWithAxisName(AxName: String): TSingleAxisData;
var
Enum : Integer;
Entry : TSingleAxisData;
Found : Boolean;
begin
Result := nil;
Found := False;
ENum := 0;
while (Enum < Count) and (not Found) do
  begin
  try
  Entry := Items[Enum];
  Found := Entry.FAxisName = AxName;
  Finally
  inc(Enum)
  end;
  end;
If Found then Result := Entry;
end;



function TCalAxisList.InitFromDefaultData(DefaultSettings: TDefaultSettingsList): Boolean;
var
 SingleAxisList : TSingleAxisData;
 Defaults       : TDefaultSettingEntry;
begin
Clean;
if Availability.XData.Required then
  begin
  SingleAxisList := AddAxisCalled('X',False);
  Defaults := DefaultSettings.EntryForAxisName('X');
  SingleAxisList.InitialiseFromDefaults(Defaults);
  end;
if Availability.YData.Required then
  begin
  SingleAxisList := AddAxisCalled('Y',False);
  Defaults := DefaultSettings.EntryForAxisName('Y');
  SingleAxisList.InitialiseFromDefaults(Defaults);
  end;
if Availability.ZData.Required then
  begin
  SingleAxisList := AddAxisCalled('Z',False);
  Defaults := DefaultSettings.EntryForAxisName('Z');
  SingleAxisList.InitialiseFromDefaults(Defaults);
  end;
if Availability.AData.Required then
  begin
  SingleAxisList := AddAxisCalled('A',False);
  Defaults := DefaultSettings.EntryForAxisName('A');
  SingleAxisList.InitialiseFromDefaults(Defaults);
  end;
if Availability.BData.Required then
  begin
  SingleAxisList := AddAxisCalled('B',False);
  Defaults := DefaultSettings.EntryForAxisName('B');
  SingleAxisList.InitialiseFromDefaults(Defaults);
  end;
if Availability.CData.Required then
  begin
  SingleAxisList := AddAxisCalled('C',False);
  Defaults := DefaultSettings.EntryForAxisName('C');
  SingleAxisList.InitialiseFromDefaults(Defaults);
  end;
end;

function TCalAxisList.LoadCompareListFromDataFile(IFile: String): Boolean;
var
 SingleAxisList : TSingleAxisData;
begin
CleanCompareList;
Availability.WorkingFileExists := FileExists(IFile);
if Availability.WorkingFileExists then
  begin
  Result := True;
  if Availability.XData.Required then
    begin
    SingleAxisList := AddAxisCalled('X',True);
    SingleAxisList.ReadFromWorkingFile(IFile);
    end;

  if Availability.YData.Required then
    begin
    SingleAxisList := AddAxisCalled('Y',True);
    SingleAxisList.ReadFromWorkingFile(IFile);
  end;

  if Availability.ZData.Required then
    begin
    SingleAxisList := AddAxisCalled('Z',True);
    SingleAxisList.ReadFromWorkingFile(IFile);
    end;

  if Availability.AData.Required then
    begin
    SingleAxisList := AddAxisCalled('A',True);
    SingleAxisList.ReadFromWorkingFile(IFile);
  end;


  if Availability.BData.Required then
    begin
    SingleAxisList := AddAxisCalled('B',True);
    SingleAxisList.ReadFromWorkingFile(IFile);
    end;

  if Availability.CData.Required then
    begin
    SingleAxisList := AddAxisCalled('C',True);
    SingleAxisList.ReadFromWorkingFile(IFile);
    end;
  end;
end;


function TCalAxisList.LoadFromDataFile(IFile: String):Boolean;
var
 SingleAxisList : TSingleAxisData;
begin
Clean;
Availability.WorkingFileExists := FileExists(IFile);
if Availability.WorkingFileExists then
  begin
  Result := True;
  if Availability.XData.Required then
    begin
    SingleAxisList := AddAxisCalled('X',False);
    if not SingleAxisList.ReadFromWorkingFile(IFile) then
      begin
      Availability.XData.WorkingDataState := tcdUnavailable;
      Result := False;
      end
    else
      begin
      Availability.XData.WorkingDataState  := tcdLoaded;
      end;
  end;

  if Availability.YData.Required then
    begin
    SingleAxisList := AddAxisCalled('Y',False);
    if not SingleAxisList.ReadFromWorkingFile(IFile) then
      begin
      Availability.YData.WorkingDataState := tcdUnavailable;
      Result := False;
      end
    else
      begin
      Availability.YData.WorkingDataState  := tcdLoaded;
      end;
  end;

  if Availability.ZData.Required then
    begin
    SingleAxisList := AddAxisCalled('Z',False);
    if not SingleAxisList.ReadFromWorkingFile(IFile) then
      begin
      Availability.ZData.WorkingDataState := tcdUnavailable;
      Result := False;
      end
    else
      begin
      Availability.ZData.WorkingDataState  := tcdLoaded;
      end;
  end;

  if Availability.AData.Required then
    begin
    SingleAxisList := AddAxisCalled('A',False);
    if not SingleAxisList.ReadFromWorkingFile(IFile) then
      begin
      Availability.AData.WorkingDataState := tcdUnavailable;
      Result := False;
      end
    else
      begin
      Availability.AData.WorkingDataState  := tcdLoaded;
      end;
  end;


  if Availability.BData.Required then
    begin
    SingleAxisList := AddAxisCalled('B',False);
    if not SingleAxisList.ReadFromWorkingFile(IFile) then
      begin
      Availability.BData.WorkingDataState := tcdUnavailable;
      Result := False;
      end
    else
      begin
      Availability.BData.WorkingDataState  := tcdLoaded;
      end;
  end;

  if Availability.CData.Required then
    begin
    SingleAxisList := AddAxisCalled('C',False);
    if not SingleAxisList.ReadFromWorkingFile(IFile) then
      begin
      Availability.CData.WorkingDataState := tcdUnavailable;
      Result := False;
      end
    else
      begin
      Availability.CData.WorkingDataState  := tcdLoaded;
      end;
    end;
  end;
end;


constructor TCalAxisList.Create;
begin
Availability := TAvailabityandStatusData.Create;
CompareList := TList.Create;
end;

destructor TCalAxisList.Destroy;
begin
CleanCompareList;
CompareList.Free;
Availability.Free;
inherited;
end;


function TSingleAxisCompDataLists.GetCalculatedShift(var CompShift : Double): Boolean;
begin
Result := CurrentInputDataList.CalcultateZeroShiftForHomeOffsetOf(FCompHomeOffset,CompShift);
end;

procedure TSingleAxisCompDataLists.UpdateListLimits;
begin
  if assigned(OutputDataList) then OutputDataList.aaNegLimit := FFirstPosition;
  if assigned(CurrentInputDataList) then CurrentInputDataList.NegLimit :=  FFirstPosition;
  if assigned(OutputDataList) then OutputDataList.aaPosLimit := FFirstPosition+((FNumberOfEntries-1)* FCompIncrement);
  if assigned(CurrentInputDataList) then CurrentInputDataList.PosLimit := FFirstPosition+((FNumberOfEntries-1)* FCompIncrement);
end;

procedure TSingleAxisCompDataLists.SetNumberEntries(const Value: Integer);
begin
  FNumberOfEntries := Value;
  UpdateListLimits
end;

procedure TSingleAxisCompDataLists.SetFirstPosition(const Value: Double);
begin
  FFirstPosition := Value;
  UpdateListLimits
end;

procedure TSingleAxisCompDataLists.SetAxisIsLinear(const Value: Boolean);
begin
  FaxisIsLinear := Value;
  if assigned(OutputDataList) then OutputDataList.IsLinearAxis := Value;
end;


procedure TSingleAxisCompDataLists.SetCompIncrement(const Value: Double);
begin
  FCompIncrement := Value;
  UpdateListLimits
end;

function TSingleAxisCompDataLists.GetCompNegativeLimit: Double;
begin
Result := FFirstPosition;
end;

function TSingleAxisCompDataLists.GetCompPositiveLimit: Double;
begin
    Result := FFirstPosition+((FNumberOfEntries-1)*FCompIncrement);
end;

procedure TSingleAxisCompDataLists.SetHomeOffset(const Value: Double);
begin
  EncoderOffsetValid := True;
  if Value <> FCompHomeOffset then
    begin
    FCompHomeOffset := Value;
    UpdateInputDataForHomeOffsetof(FCompHomeOffset);
    if assigned(OutputDataList) then OutputDataList.aaHomeOffset := FCompHomeOffset;
    DataTouched := True;
    end;
end;

function TSingleAxisCompDataLists.UpdateInputDataForHomeOffsetof(HOffset: Double): Boolean;
begin
Result := False;
if CurrentInputDataList.Count > 1 then
  begin
  Result := CurrentInputDataList.UpdateShiftedDataForHomeOffset(HOffset);
  if Result then
    begin
    OutputDataList.aaHomeOffset := HOffset;
    OutputDataList.UpdateFromInputDataList(CurrentInputDataList);
    end
  end;
end;

procedure TSingleAxisCompDataLists.InDataUpdated(ZShift: Double; Valid: Boolean);
begin
if assigned(FOnInputDataUpdated) then FOnInputDataUpdated(ZShift,Valid);
end;



function TCalAxisList.UpdateFromNCData(NCList: TNCConfigList): Boolean;
var
NCEntry : TNCConfigDataRecord;
AxEntry : TSingleAxisData;
AxNum : Integer;
begin
Result := False;
if Count < 1 then exit;
if (NCList.Count) = Count then
  begin
  For Axnum := 0 to Count-1 do
    begin
    NCEntry := NCList[Axnum];
    AxEntry := Items[Axnum];
    if NCentry.AxisLetter = Axentry.FAxisName then
      begin
      AxEntry.EncoderCountsPerUnit := NCEntry.EncoderCountsPerUnit;
      end;
    end;
  end;
end;

{procedure TCalAxisList.UpdateFromImports(ImportHandler: TAxCalImportFileList);
begin

end;}

function TCalAxisList.CompareToArchive(ArchivePath: String;ForceLoad : Boolean;BufferRegisteredData : Boolean;var ArchiveExists : Boolean): Boolean;
var
ArFile : String;
LoadedAxis : TSingleAxisData;
CompareAxis: TSingleAxisData;
AxNum : INteger;
begin
Result := False;
If DirectoryExists(ArchivePath) then
ArFile := SlashSep(ArchivePath,'CompEdit.txt');
If FileExists(ArFile) then
  begin
  if ForceLoad then
    begin
    ArchiveExists := True;
    CleanCompareList;
    LoadCompareListFromDataFile(ArFile);
    end;
  end
else
  begin
  if ForceLoad then
    begin
    ArchiveExists := False;
    end;
  end;

  if CompareList.Count = Count then
    begin
    For AxNum := 0 to Count-1 do
      begin
      LoadedAxis := Items[Axnum];
      CompareAxis:= CompareList[AxNum];
      if BufferRegisteredData then
        begin
        LoadedAxis.TakeSnapshotFrom(CompareAxis,LoadedAxis.RegisteredStatusBuffer);
//        CompareAxis.CompensationDataLists.CurrentInputDataList.CopyTo(LoadedAxis.CompensationDataLists.RegisteredInputDataList);
        end;
      LoadedAxis.aaCompareTo(CompareAxis,Availability);
      end;
    end;
end;


{ TOutputDataList }

procedure TOutputDataList.Clean;
var
LEntry : TOutputDataRecord;
begin
while Count > 0 do
 begin
 Lentry := Items[Count-1];
 LEntry.Free;
 Delete(Count-1);
 end;
end;

procedure TSingleAxisCompDataLists.OutDataUpdated(Valid: Boolean);
begin
 if Assigned(FOnOutputDataUpdated) then FOnOutputDataUpdated(Valid);
end;

function TOutputDataList.GetEncoderCountSpan: Integer;
begin
if Count > 0 then
  result := FEncoderSpan
else
  result := 0;
end;


procedure TOutputDataList.SetAxisLetter(const Value: String);
begin
  FAxisLetter := Value;
end;

procedure TOutputDataList.SetEncCountsPerMM(const Value: Double);
begin
  FEncCountsPerMM := Value;
end;

procedure TOutputDataList.SetMotorNumber(const Value: Integer);
begin
  FMotorNumber := Value;
end;
function TOutputDataList.GetMotorNumber: Integer;
begin
 Result := FMotorNumber;
end;


procedure TOutputDataList.SetPosLimit(const Value: Double);
begin
  FPosLimit := Value;
end;

procedure TOutputDataList.SetNegLimit(const Value: Double);
begin
  FNegLimit := Value;
end;


function TOutputDataList.UpdateFromInputDataList(InList: TInputDataList): Boolean;
var
NewEntry : TOutputDataRecord;
InDataEntry,NextInDataEntry : TInputDataRecord;
CurrentActPos,ModDist : Double;
CurrentEncoderCounts : Double;
EncoderIncrement : Double;
Enum : Integer;
PosDist : Double;
TotalCounts : Integer;
Failed : Boolean;
HasRolledOver : Boolean;
RolloverLimit : Double;
RemainingIndexes : INteger;
AddCount : Integer;
begin
Clean;
Result := false;
CurrentActPos := aaHomeOffset;
ModDist := aaHomeOffset;
CurrentEncoderCounts := 0;
Failed := false;
HasRolledOver := False;

  if InList.Count < 2 then exit;
  if IsLinearAxis then AddCount := 1 else AddCount := 0;
  CurrentActPos := aaHomeOffset;
  InDataEntry := Inlist[0];
  NextInDataEntry := Inlist[1];
  PosDist := NextInDataEntry.Position-InDataEntry.Position;
  RolloverLimit := aaPOsLimit+PosDist;
  if DoublePoints then
    begin
    POsDist := POsDist/2;
    TotalCounts := (InList.Count*2)+ (AddCount*2)
    end
  else
    begin
    TotalCounts := InList.Count+ AddCount
    end;
  EncoderIncrement := Round(aEncCountsPerMM)*PosDist;

  While (Count < TotalCounts) and (Not Failed) do
    begin
    try
    NewEntry := TOutputDataRecord.Create;
    NewEntry.EncoderCounts := CurrentEncoderCounts;
    NewEntry.ActualPosition := CurrentActPos;
    if InList.ShiftedCompAtPosition(CurrentActPos,NewEntry.CompValue,NewEntry.RevCompValue) then
      begin
      NewEntry.Backlash := NewEntry.CompValue-NewEntry.RevCompValue;
      if NewEntry.EncoderCounts = 0 then
        begin
        BacklashAtEncoderZero := NewEntry.Backlash;
        end
      end
    else
      begin
      Failed := True;
      end
    finally
    Add(NewEntry);
    // Here for Revert to Negtive
    CurrentActPos := CurrentActPos+ PosDist;
    if CurrentActPos >= RolloverLimit then
      begin
      RemainingIndexes := TotalCounts-Count;
      CurrentActPos := aaHomeOffset-(RemainingIndexes*PosDist);
      end;
    CurrentEncoderCounts := CurrentEncoderCounts+EncoderIncrement;
    end;
    end;
  Result := Not Failed;
  FEncoderSpan := Round(CurrentEncoderCounts);
  if assigned (FOnOutputDataListUpdated) then FOnOutputDataListUpdated(Result)
end;

procedure TSingleAxisCompDataLists.SetEncodeCountsPerUnit(const Value: Double);
begin
  FEncodeCountsPerUnit := Value;
  if assigned(OutputDataList) then
      OutputDataList.aEncCountsPerMM := FEncodeCountsPerUnit;
end;

{procedure TSingleAxisCompDataLists.SetNegLimit(const Value: Double);
begin
  FCompNegativeLimit := Value;
  if assigned(OutputDataList) then OutputDataList.NegLimit := FCompNegativeLimit;
  if assigned(CurrentInputDataList) then CurrentInputDataList.NegLimit := FCompNegativeLimit;
end;

procedure TSingleAxisCompDataLists.SetPOsLimit(const Value: Double);
begin
  FCompPositiveLimit := Value;
  if assigned(OutputDataList) then OutputDataList.PosLimit := FCompPositiveLimit;
  if assigned(CurrentInputDataList) then CurrentInputDataList.PosLimit := FCompPositiveLimit;
end;}

procedure TSingleAxisCompDataLists.SetDoublePOints(const Value: Boolean);
begin
  FDoublePoints := Value;
  if assigned(OutputDataList) then
    begin
    OutputDataList.DoublePoints := FDoublePoints;
    OutputDataList.UpdateFromInputDataList(CurrentInputDataList);
    end;

end;

procedure TSingleAxisCompDataLists.SetAxisLetter(const Value: String);
begin
  FAxisLetter := Value;
  if assigned(OutputDataList) then OutputDataList.AxisLetter := Value;
end;

constructor TOutputDataList.Create;
begin
inherited;
FMotorNumber := -100;
end;


{ TOutputDataRecord }

function TOutputDataRecord.AsBackLashText(Offset: Double;CompMultiplier : Double): String;
var
PaddedCounts : String;
PaddedPosn : String;
OffsetValue : Integer;
begin
PaddedCounts := IntToStr(Round((Backlash-Offset)*-CompMultiplier));
While Length(PaddedCounts) < 15 do
  begin
  PaddedCounts := PaddedCounts+' ';
  end;
PaddedPosn := Format('%5.3f',[ActualPosition]);
While Length(PaddedPosn) < 15 do
  begin
  PaddedPosn := PaddedPosn+' ';
  end;
Result := Format('%s;POS = %sENCODER = %d',[PaddedCounts,PaddedPosn,Round(EncoderCounts)]);
end;

function TOutputDataRecord.AsCompText(CompMultiplier : Double): String;
var
PaddedCounts : String;
PaddedPosn : String;

begin
PaddedCounts := IntToStr(Round(CompValue*CompMultiplier));
While Length(PaddedCounts) < 15 do
  begin
  PaddedCounts := PaddedCounts+' ';
  end;
PaddedPosn := Format('%5.3f',[ActualPosition]);
While Length(PaddedPosn) < 15 do
  begin
  PaddedPosn := PaddedPosn+' ';
  end;
Result := Format('%s;POS = %sENCODER = %d',[PaddedCounts,PaddedPosn,Round(EncoderCounts)]);
end;

procedure TSingleAxisCompDataLists.SetMotorNumber(const Value: INteger);
begin
  if assigned(OutputDataList) then
    OutputDataList.MotorNumber := Value;
end;

function TSingleAxisCompDataLists.GetMotorNumber: INteger;
begin
  if assigned(OutputDataList) then
   Result := OutputDataList.MotorNumber;
end;

{ TNCConfigList }

procedure TNCConfigList.Clean;
var
CEntry : TNCConfigDataRecord;
begin
while Count > 0 do
  begin
  CEntry := Items[0];
  Centry.Free;
  Delete(0);
  end;
end;

function TNCConfigList.ReadFromDirectory(DirName: String): Integer;
var
FileName : String;
FileStrings : TStringList;
CurrentLine : INteger;
LoadDone : Boolean;
CurrentWork : String;
NextTargetFound : Boolean;
NextTarget : String;
AxisNames : String;
AxNum : Integer;
AxName : String;
CoordStartIndex : Integer;
MotorStartIndex : Integer;
MotorEntry : String;
TotalAxisCount : Integer;
Buffer : String;
NewEntry : TNCConfigDataRecord;
begin
Clean;
Result := 0;
FileName := SlashSep(DirName,'NC.cfg');
if FileExists(Filename) then
  begin
  FileStrings := TStringList.Create;
  try
    FileStrings.LoadFromFile(Filename);
    CurrentLine := 0;
    LoadDone := False;
    while not LoadDone do
      begin
      if GetIndexofNextTarget('CoordinateSystem[0]',CurrentLine,FileStrings,'',CurrentWork) then
        begin
        begin
        CoordStartIndex := CurrentLine;
        if GetIndexofNextTarget('AxisNames',CurrentLine,FileStrings,'',CurrentWork) then
          begin
          AxisNames := Uppercase(ReadEquateValue(CurrentWork));
          end;
          TotalAxisCount := Length(AxisNames);
          For AxNum := 1 to TotalAxisCount do
              begin
              if AxisNames[AxNum] <> 'U' then
                begin
                Result := Result+1;
                NewEntry := TNCConfigDataRecord.Create;
                NewEntry.AxisLetter := AxisNames[AxNum];
                MotorEntry := Format('Motor[%s]',[NewEntry.AxisLetter]);
                CurrentLine := CoordStartIndex;
                if GetIndexofNextTarget(MotorEntry,CurrentLine,FileStrings,'',CurrentWork) then
                  begin
                  MotorStartIndex := CurrentLine;
                  if GetIndexofNextTarget('CountsPerUnit',CurrentLine,FileStrings,'}',CurrentWork) then
                    begin
                    Buffer := ReadEquateValue(CurrentWork);
                    NewEntry.EncoderCountsPerUnit := StrToFloatDef(Buffer,0);
                    end;

                  CurrentLine := MotorStartIndex;
                  if GetIndexofNextTarget('Connection',CurrentLine,FileStrings,'}',CurrentWork) then
                    begin
                    Buffer := ReadEquateValue(CurrentWork);
                    NewEntry.MotorNumber := StrToIntDef(Buffer,0);
                    end;
                  Add(NewEntry);
                  end
                else
                  begin
                  // Motor not found
                  NewEntry.Free;
                  LoadDone := True
                  end
                end;
              end;
        LoadDone := True
        end;
        end;
      end;

  finally
  FileStrings.Free;
  end;
  end;
end;

function TNCConfigList.GetIndexofNextTarget(Target : String;
                              var StartIndex : Integer ;
                              SList : TStringList;
                              TerminateAt : String;
                              var FoundLineText: String) :Boolean;
var
UCTarget : String;
FPos : INteger;
Terminated : Boolean;
begin
Result := False;
UCTarget := Uppercase(Target);
Terminated := False;
while (StartIndex < SList.Count) and (Result = False) and (Not Terminated)do
  begin
  FoundLineText := Uppercase(Slist[StartIndex]);
  try
  FPos := Pos(UCTarget,FoundLineText);
  Result := FPOs > 0;
  if (Not result) and (TerminateAt <> '') then
    begin
    FPos := Pos(TerminateAt,FoundLineText);
    Terminated :=Fpos > 0;
    end
  finally
  inc(StartIndex);
  end;
  end
end;

function TNCConfigList.ReadEquateValue(InputLine : String):String;
var
Fpos : Integer;
LLength : INteger;
begin
Result := '';
FPos := Pos('=',InputLine);
If Fpos > 0 then
  begin
  LLength := Length(InputLine);
  Result :=Copy(InputLine,Fpos+1,LLength-Fpos);
  Result := Trim(Result);
  end;
end;

procedure TSingleAxisCompDataLists.SetBackLashUsed(const Value: Boolean);
begin
  FBackLashUsed := Value;
end;


{ TDefaultSettingsList }

procedure TDefaultSettingsList.Clean;
var
DEntry : TDefaultSettingEntry;
begin
 while Count > 0 do
  begin
  DEntry := Items[0];
  DEntry.Free;
  Delete(0);
  end;
end;

function TDefaultSettingsList.EntryForAxisName(AxName: String): TDefaultSettingEntry;
var
DEntry : TDefaultSettingEntry;
Found : Boolean;
ENum : INteger;
SString : String;
begin
Result := Nil;
Found := False;
ENum := 0;
SString := UpperCase(Axname);
if Count > 0 then
  begin
  while (Enum < Count) and (not Found) do
    begin
    try
    Dentry := Items[Enum];
    if UpperCase(Dentry.AxisName) = SString then
      begin
      Result := DEntry;
      Found := True;
      end
    finally
    inc(Enum);
    end;
    end;
  end;
end;

// Returns a null string if the NC.cfg Path is not defined in the config file
// Otherwise Full path returns path to NC.cfg
Function TDefaultSettingsList.LoadFromFile(FileName: String): String;
var
InFile : TiniFile;
ENum : INteger;
DEntry : TDefaultSettingEntry;
AxCount : INteger;
AxName : String;
SectionName : String;
Buffer : String;
begin
Clean;
Result := '';
if FileExists(FileName) then
  begin
  InFile := TIniFile.Create(Filename);
  try
  AxisListString := InFile.ReadString('GENERAL','AxisList','XYZABC');
  AxCount := Length(Trim(AxisListString));
  for Enum := 0 to AxCount-1 do
    begin
    AxName := AxisListString[Enum+1];
    SectionName := Format('AXIS%S',[AxName]);
    DEntry := TDefaultSettingEntry.Create;
    Dentry.AxisName := AxName;
    Buffer := InFile.ReadString(SectionName,'IsLinear','True');
    Dentry.IsLinear := (Buffer[1] = 't') or (Buffer[1]='T');
    Dentry.EncoderCountsPerUnit := InFile.ReadFloat(SectionName,'CountsPerUnit',10000);
    Dentry.FirstEntryPos := InFile.ReadFloat(SectionName,'FirstEntryPosition',-100);
    Dentry.Step := InFile.ReadFloat(SectionName,'PositionIncrement',10);
    Dentry.NumEntries := InFile.ReadInteger(SectionName,'NumberOfEntries',21);
    Buffer := InFile.ReadString(SectionName,'DoublePoints','True');
    Dentry.DoublePointsOnOutput := (Buffer[1] = 't') or (Buffer[1]='T');
    Buffer := InFile.ReadString(SectionName,'UseBacklashTable','True');
    Dentry.UseBackLashTable     := (Buffer[1] = 't') or (Buffer[1]='T');
//    Dentry.PosLimit     := InFile.ReadFloat(SectionName,'PositiveLimit',+100);
//    Dentry.NegLimit     := InFile.ReadFloat(SectionName,'NegativeLimit',-100);
    DEntry.MotorNumber   :=InFile.ReadInteger(SectionName,'MotorNumber',-1);
    if DEntry.MotorNumber = -1 then DEntry.MotorNumber := Count+1;
    Add(Dentry);
    end;
  Result := InFile.ReadString('GENERAL','NCConfigPath','')
  finally
  InFile.Free;
  end;
  end;

end;


function TAvailabityandStatusData.NoDataAvailable: Boolean;
begin
Result := (Not WorkingFileLoaded) and (not ImportDataAvailableAllAxes);
end;




function TSingleAxisData.GetMakeOutput: Boolean;
begin
Result := CompensationDataLists.MakeOutput;
end;

procedure TSingleAxisData.SetMakeOutput(const Value: Boolean);
begin
CompensationDataLists.MakeOutput := Value;
end;

{ TAxisBuffer }



function TAxisBuffer.CompareTo(ExtBuffer: TAxisBuffer): Boolean;
begin
Result := True;
  DiffList.Clear;
  if ExtBuffer.DoublePoints <> DoublePoints then Result := False
  else if ExtBuffer.BacklashUsed <> BacklashUsed then
    begin
    Result := False;
    Difflist.Add('BacklashUsed Differ');
    end;
   if ExtBuffer.IsLinear <> IsLinear then
    begin
    Result := False;
    Difflist.Add('Is Linear Axis Differ');
    end;
   if ExtBuffer.MNumber <> MNumber then
    begin
    Result := False;
    Difflist.Add('Motor Number Differ');
    Difflist.Add(Format('%d -> %d',[ExtBuffer.MNumber,MNumber]));
    end;
  if ExtBuffer.EncoderCountsPerUnit <> EncoderCountsPerUnit   then
    begin
    Result := False;
    Difflist.Add('Encoder Counts per Unit Differ');
    Difflist.Add(Format('%7.5f -> %7.5f',[ExtBuffer.EncoderCountsPerUnit,EncoderCountsPerUnit]));
    end;
  if ExtBuffer.FirstPosition <> FirstPosition then
    begin
    Result := False;
    Difflist.Add('First Comp Posn. Differ');
    Difflist.Add(Format('%7.5f -> %7.5f',[ExtBuffer.FirstPosition,FirstPosition]));
    end;
  if ExtBuffer.NumberOfEntries <> NumberOfEntries then
    begin
    Difflist.Add('First Num. Entries Differ');
    Difflist.Add(Format('%d -> %d',[ExtBuffer.NumberOfEntries,NumberOfEntries]));
    Result := False;
    end;
  if ExtBuffer.CompIncrement <> CompIncrement then
    begin
    Difflist.Add('Comp Increment Differ');
    Difflist.Add(Format('%7.5f -> %7.5f',[ExtBuffer.CompIncrement,CompIncrement]));
    Result := False;
    end;
  if ExtBuffer.HomeOffset <>  HomeOffset  then
    begin
    Result := False;
    Difflist.Add('Home Offsets Differ');
    Difflist.Add(Format('%7.5f -> %7.5f',[ExtBuffer.HomeOffset,HomeOffset]));
    end;
  if ExtBuffer.PositiveLimit <> PositiveLimit then
    begin
    Result := False;
    Difflist.Add('Positive Limit Differ');
    Difflist.Add(Format('%7.5f -> %7.5f',[ExtBuffer.PositiveLimit,PositiveLimit]));
    end;
  if ExtBuffer.NegativeLimit <> NegativeLimit then
    begin
    Result := False;
    Difflist.Add('Negative Limit Differ');
    Difflist.Add(Format('%7.5f -> %7.5f',[ExtBuffer.NegativeLimit,NegativeLimit]));
    end;
  if ExtBuffer.MakeOutPut <> MakeOutPut then
    begin
    Result := False;
    Difflist.Add('Make Output Differ');
    end;
  if ExtBuffer.ManualBacklashMicron <> ManualBacklashMicron then
    begin
    Result := False;
    Difflist.Add('Manual Backlash Values Differ');
    Difflist.Add(Format('%d -> %d',[ExtBuffer.ManualBacklashMicron,ManualBacklashMicron]));
    end;
end;


function TSingleAxisData.aaCompareTo(CAxisData: TSingleAxisData; Availability: TAvailabityandStatusData):Boolean;
begin
TakeSnapshotFrom(CAxisData,CaxisData.CompareStatusBuffer);
TakeSnapshotFrom(Self,CompareStatusBuffer);
CompareStatusBuffer.CompareTo(CAxisData.CompareStatusBuffer);
Result := CompareStatusBuffer.ParametersMatch and CompareStatusBuffer.InputDataMatch;
Availability.SetRegisteredMatchForAxis(AxisName,CompareStatusBuffer.ParametersMatch,CompareStatusBuffer.InputDataMatch);
end;


procedure TSingleAxisData.RecordStatus;
begin
TakeSnapshotFrom(Self,WorkingFileStatusBuffer);
TakeSnapshotFrom(Self,CurrentStatusBuffer);
end;


function TSingleAxisData.ReadFromWorkingFile(FileName: String):Boolean;
begin
  Result := CompensationDataLists.intReadFromWorkingFile(Filename);
  RecordStatus;
end;


procedure TSingleAxisData.TakeSnapshotFrom(SourceAxis: TSingleAxisData; DestBuffer: TSingleAxisSnapshot);
begin
if assigned(SourceAxis) then
  begin
  DestBuffer.ClearDown;
  SourceAxis.CompensationDataLists.CurrentInputDataList.CopyTo(DestBuffer.GridInputData);
  SourceAxis.BufferStatusTo(DestBuffer.ParameterBuffer);
  end;
end;

procedure TSingleAxisData.CompareCurrentStatusToSnapshot(CSnap: TSingleAxisSnapshot);
var
CompBuff : TSingleAxisSnapshot;
begin
CompBuff := TSingleAxisSnapshot.create;
try
TakeSnapshotFrom(Self,CompBuff);
CSnap.CompareTo(CompBuff);
finally
CompBuff.Free;
end;
end;

procedure TSingleAxisData.CompareCurrentToWorkingSnapshot;
begin
TakeSnapshotFrom(Self,WorkingFileStatusBuffer);
if assigned(WorkingFileStatusBuffer) and assigned(CurrentStatusBuffer) then
  begin
  CurrentStatusBuffer.CompareTo(WorkingFileStatusBuffer);
  end;
end;


function TSingleAxisData.RevertToSnapShot(Snap: TSingleAxisSnapshot): Boolean;
begin
if assigned(CompensationDataLists) then
  begin
  DoublePointsOnOutput := Snap.ParameterBuffer.DoublePoints ;
  CompensationDataLists.BacklashUsed:= Snap.ParameterBuffer.BacklashUsed;
  CompensationDataLists.AxisIsLinear:= Snap.ParameterBuffer.IsLinear;
  CompensationDataLists.AxisMotorNumber:= Snap.ParameterBuffer.MNumber;
  EncoderCountsPerUnit:= Snap.ParameterBuffer.EncoderCountsPerUnit;
  CompensationDataLists.FirstPosition:= Snap.ParameterBuffer.FirstPosition;
  CompensationDataLists.NumberOfEntries:= Snap.ParameterBuffer.NumberOfEntries;
  CompensationDataLists.CompIncrement:= Snap.ParameterBuffer.CompIncrement;
  CompensationDataLists.CompHomeOffset:= Snap.ParameterBuffer.HomeOffset;
//  CompensationDataLists.CompPositiveLimit:= Snap.ParameterBuffer.PositiveLimit;
//  CompensationDataLists.CompNegativeLimit:=  Snap.ParameterBuffer.NegativeLimit;
  CompensationDataLists.ManualBacklashMicron:= Snap.ParameterBuffer.ManualBacklashMicron;
  MakeOutput := Snap.ParameterBuffer.MakeOutPut;
  Snap.GridInputData.CopyTo(CompensationDataLists.CurrentInputDataList);
  RecordStatus; // matches snapshots again
  end;
end;


function TAvailabityandStatusData.IsUnusable: Boolean;
begin
Result := False;
end;

function TAvailabityandStatusData.IsValidBlankSystem: Boolean;
begin
Result := BlankCompFileExists and not WorkingFileExists;
end;

function TAvailabityandStatusData.IsStandardStartUp: Boolean;
begin
Result := WorkingFileExists;
if Result then
  begin
  Result := IsSatisfied;
  end;
end;


constructor TAxisBuffer.create;
begin
DiffList := TStringList.Create;
end;

destructor TAxisBuffer.Destroy;
begin
  DiffList.clear;
  DiffList.Free;
  inherited;
end;

{ TAdjustBuffer }
procedure TAdjustBuffer.Clean;
var
AxEntry : TAdjustData;
begin
 while Count > 0 do
  begin
  AxEntry := Items[Count-1];
  AXentry.Free;
  Delete(Count-1);
  end;
end;



function TAdjustBuffer.WriteToFile(WorkingFile: String;AxisName : String): Boolean;
var
CFile : TIniFile;
SectionName : String;
ENum : Integer;
AEntry : TAdjustData;
KeyName : String;
begin
Result := False;
if Count < 1 then exit;
if FileExists(WorkingFile) then
  begin
  Cfile := TIniFile.Create(WorkingFile);
  SectionName := Format('%sAdjustBuffer',[AxisName]);
  if Cfile.SectionExists(SectionName) then Cfile.EraseSection(SectionName);
  Cfile.WriteInteger(SectionName,'EntryCount',Count);
  For Enum := 0 to Count-1 do
    begin
    AEntry := Items[Enum];
    KeyName := Format('AdjustPair%d',[Enum]);
    Cfile.WriteString(SectionName,KeyName,Aentry.AsString);
    end;
  Result := True;
  end;
end;

function TAdjustBuffer.ReadFromFile(WorkingFile: String;AxisName : String): Boolean;
var
CFile : TIniFile;
SectionName : String;
ENum : Integer;
AEntry : TAdjustData;
Entries : INteger;
StrVal : String;
KeyName : String;
begin
Result := True;
Clean;
if FileExists(WorkingFile) then
  begin
  Cfile := TIniFile.Create(WorkingFile);
  SectionName := Format('%sAdjustBuffer',[AxisName]);
  if Cfile.SectionExists(SectionName) then
    begin
    Entries := Cfile.ReadInteger(SectionName,'EntryCount',0);
    if Entries > 0 then
      begin
      AEntry := TAdjustData.Create;
      KeyName := Format('AdjustPair%d',[Enum]);
      StrVal := CFile.ReadString(SectionName,KeyName,'0,0');
      AEntry.FromString(Strval);
      Add(AEntry);
      end;
    end
  end;

end;

{ TAdjustData }

function TAdjustData.FromString(InStr: String): Boolean;
var
StrVal : String;
begin
Result := True;
try
StrVal := AxisCalibrationUtility.NextToken(InStr);
InputForwardAdjust := StrToFloatDef(StrVal,0.0);
StrVal := AxisCalibrationUtility.NextToken(InStr);
InputReverseAdjust := StrToFloatDef(StrVal,0.0);
except
InputForwardAdjust := 0;
InputReverseAdjust := 0;
Result := False;
end
end;

function TAdjustData.AsString: String;
begin
Result := Format('%5.4f,%5.4f',[InputForwardAdjust,InputReverseAdjust])
end;


procedure TSingleAxisData.UpdateChangeStrings(Changes: TStringList;Grid :TInputDataGrid);
begin
if assigned(Changes) then
  begin
  Changes.Clear;
//  If not (SettingsMatchesRecordedStatus) then
    begin

    end;
  end;
end;


{ TInputDataRecord }

procedure TInputDataRecord.AssignFrom(ExtEntry: TInputDataRecord);
begin
  Position                := ExteNtry.Position;
  InputForwardError       := ExteNtry.InputForwardError;
  InputForwardAdjust      := ExteNtry.InputForwardAdjust;
  InputReverseError       := ExteNtry.InputReverseError;
  InputReverseAdjust      := ExteNtry.InputReverseAdjust;
  ZeroShiftedForwardData  := ExteNtry.ZeroShiftedForwardData;
  ZeroShiftedReverseData  := ExteNtry.ZeroShiftedReverseData;
end;

{ TSingleAxisSnapshot }

procedure TSingleAxisSnapshot.ClearDown;
begin
GridInputData.Clean;
end;

procedure TSingleAxisSnapshot.CompareTo(ExtSnapshot: TSingleAxisSnapshot);
begin
FParametersMatch := ParameterBuffer.CompareTo(ExtSnapshot.ParameterBuffer);
FInputDataMatch := GridInputData.CompareTo(ExtSnapshot.GridInputData);
end;

constructor TSingleAxisSnapshot.create;
begin
  GridInputData := TInputDataList.Create;
  ParameterBuffer := TAxisBuffer.Create;
end;

destructor TSingleAxisSnapshot.Destroy;
begin
  GridInputData.Clean;
  GridInputData.Free;
  ParameterBuffer.Free;
  inherited;
end;



function TSingleAxisSnapshot.GetDiffList: TStringList;
begin
Result := ParameterBuffer.DiffList;
end;


procedure TAvailabityandStatusData.SetWorkingFileExists(const Value: Boolean);
begin
  FWorkingFileExists := Value;
end;

procedure TAvailabityandStatusData.SetFPUM(const Value: TFirstPowerupModes);
begin
  FFirstPowerUpMode := Value;
end;

procedure TSingleAxisCompDataLists.SetManualBacklash(const Value: Integer);
begin
  FManualBacklashMicron := Value;
end;

procedure TSingleAxisCompDataLists.SetManualBacklashAdjust(const Value: Integer);
begin
  FManualBacklashAdjust := Value;
end;


end.
