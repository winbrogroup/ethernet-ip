object CalMsg: TCalMsg
  Left = 0
  Top = 0
  Width = 115
  Height = 112
  BorderIcons = []
  BorderStyle = bsSizeToolWin
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object MessLabel: TLabel
    Left = 20
    Top = 24
    Width = 49
    Height = 13
    Caption = 'MessLabel'
  end
  object OKButton: TButton
    Left = 16
    Top = 48
    Width = 75
    Height = 25
    Caption = 'OK'
    TabOrder = 0
    OnClick = OKButtonClick
  end
end
