unit CalArchive;

Interface
uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls,IniFiles,Grids,CalDataLists,StdCtrls;
type
TArchiveLogEntry = class(Tobject)
  private
  Date : String;
  Time : String;
  Comment : String;
  IsFullArchive : Boolean;
  ArchFolder : String;
  public
  Function AsReportString : String;
  Function AsCSVEntry     : String;
  function LoadFromCSVString(InStr : String): Boolean;
  end;


TCalibrationArchiver = class(TStringGrid)
  private
    WorkingFileName : String;
    FCompBasePath: String;
    FArchiveBasePath : String;
    Initialised : Boolean;
    IsClean : Boolean;
//    LogList : TList;
    procedure GridResize(Sender : Tobject);
    procedure SetCompBasePath(const Value: String);
    procedure SaveAsReportFile(FilePath : String);
    procedure SaveToWorkingFile(IsFirstEntry : Boolean);
    procedure SaveToFile(FName : String);
    procedure MyDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);

    procedure Clean;
    procedure UpdateVisuals;
  public
    constructor Create(AOwner: TComponent); override;
    procedure LoadArchiveFromWorkingFile;
    procedure AddLogEntry(Comment : String;IsArchive : Boolean;ArchFolder : String);
    procedure ArchiveAndRestartLog(CompBaseDir : String);
    function RestorePreviousCompFileFrom(ArFolder : String; CurrentCompFilePath : String) : Boolean;
    function DoFullArchive(CurrentCompFilePath : String;EditFilePath : String): Boolean;
    function DoFullArchiveToDirectory(ArDir : String;CurrentCompFilePath : String;EditFilePath : String): Boolean;
    function CreateArchiveAndSaveCurrentComp(CompFilePath : String): String; // returns newly created archive path
    Property CompBasePath : String read FCompBasePath write SetCompBasePath;
  end;

implementation
uses    AxisCalibrationForm;


function NextToken(var InStr : String):String;
var
CommaPos : Integer;
begin
Result := '';
CommaPos := Pos(',',InStr);
if CommaPos > 0 then
  begin
  Result := Trim(Copy(InStr,1,CommaPos-1));

  InStr := Copy(Instr,CommaPos+1,1000);
  end
else
  begin
  Result := Instr;
  Instr := '';
  end;
end;

{ TCalibrationArchiver }

constructor TCalibrationArchiver.Create;
begin
  inherited;
  IsClean := True;
  RowCount := 2;
  ColCount := 3;
  Width := 450;
  FixedRows := 1;
  FixedCols := 0;
  ColWidths[0] := 90;
  ColWidths[1] := 70;
  DefaultRowHeight := 18;
  Cells[0,0] := 'Date';
  Cells[1,0] := 'Time';
  Cells[2,0] := 'Event';
  OnResize := GridResize;
  OnDrawCell := MyDrawCell;
  Font.Size := 7;

end;


function TCalibrationArchiver.DoFullArchiveToDirectory(ArDir : String;CurrentCompFilePath, EditFilePath: String): Boolean;
var
DirName : String;
EFName : String;
OutMess : String;
begin
Result := True;
if DirectoryExists(ArDir) then
  begin
  try
  if FileExists(EditFilePath) then
    begin
    EFName := ExtractFileName(EditFilePath);
    EFname := Format('%s\%s',[ArDir,EFName]);
    if not CopyFile(pAnsichar(EditFilePath),pAnsichar(EFName),True) then
      begin
      Result := False;
      AddLogEntry('Archival of Working File Unsuccessful',False,'');
      end;
    end;
  if FileExists(CurrentCompFilePath) then
    begin
    EFName := ExtractFileName(CurrentCompFilePath);
    EFname := Format('%s\%s',[ArDir,EFName]);
    if not CopyFile(pAnsichar(CurrentCompFilePath),pAnsichar(EFName),True) then
      begin
      Result := False;
      AddLogEntry('Archival of Compensation File Unsuccessful',False,'');
      end;
    end;
  except
  Result := False;
  AddLogEntry('Unsuccessful Archive',False,'');
  end;
  If Result then
    begin
    OutMess := Format('New Compensation File created and Previous Comp Data archived to %s',[EFname]);
    AddLogEntry(OutMess,True,EFname);
    end;
  end;
end;


Function TCalibrationArchiver.DoFullArchive(CurrentCompFilePath : String;EditFilePath : String): Boolean;
var
ArDir   : String;
DirName : String;
EFName : String;
begin
Result := True;
try
DirName := Format('Comp_%s_%s',[AxisCalibrationUtility.CurrentDateAsFileSafeString,AxisCalibrationUtility.CurrentTimeAsFileSafeString]);
ArDir := Format('%s\Archive\%s',[FCompBasePath,DirName]);
ForceDirectories(ArDir);
if FileExists(EditFilePath) then
  begin
  EFName := ExtractFileName(EditFilePath);
  EFname := Format('%s\%s',[ArDir,EFName]);
  if not CopyFile(pAnsichar(EditFilePath),pAnsichar(EFName),True) then
    begin
    Result := False;
    AddLogEntry('Archival of Working File Unsuccessful',False,'');
    end;
  end;
if FileExists(CurrentCompFilePath) then
  begin
  EFName := ExtractFileName(CurrentCompFilePath);
  EFname := Format('%s\%s',[ArDir,EFName]);
  if not CopyFile(pAnsichar(CurrentCompFilePath),pAnsichar(EFName),True) then
    begin
    Result := False;
    AddLogEntry('Archival of Compensation File Unsuccessful',False,'');
    end;
  end;
except
Result := False;
AddLogEntry('Unsuccessful Archive',False,'');
end;
If Result then AddLogEntry('Successful Archival of Working File and Compenstaion File',False,EFname);
end;

procedure TCalibrationArchiver.GridResize(Sender: Tobject);
begin
ColWidths[2] := ClientWidth-164;
end;


procedure TCalibrationArchiver.SetCompBasePath(const Value: String);

begin
  FCompBasePath := Value;
  FArchiveBasePath := Format('%s\Archive',[FCompBasePath]);
  ForceDirectories(FArchiveBasePath);
  try
  WorkingFileName := Format('%s\%s',[FArchiveBasePath,'ArchiveLog.csv']);
  if Not FileExists(WorkingFileName) then
    begin
    AddLogEntry('Log File Created',False,'');
    end
  else
    begin
    LoadArchiveFromWorkingFile;
    end
  except
  exit;
  end;
  Initialised := True;
end;

procedure TCalibrationArchiver.SaveAsReportFile(FilePath: String);
begin

end;

procedure TCalibrationArchiver.LoadArchiveFromWorkingFile;
var
LEntry : TArchiveLogEntry;
FileLines : TStringList;
ENum : INteger;
LineString : String;
begin
FileLines := TStringList.Create;
try
if FileExists(WorkingFileName) then
  begin
  Clean;
  FileLines.LoadFromFile(WorkingFileName);
  if FileLines.Count > 0 then
    begin
    IsClean := False;
    RowCount := FileLines.Count+1;
    For Enum := 0 to FileLines.Count-1 do
      begin
      LineString := FileLines[ENum];
      LEntry := TArchiveLogEntry.Create;
      if Lentry.LoadFromCSVString(LineString) then
        begin
        Objects[0,Enum+1] := LEntry;
        end
      else
        begin
        LEntry.Free
        end
      end;
    end
  else
    begin
    IsClean := True;
    RowCount := 2;
    end;
   end;
finally
UpdateVisuals;
end;
end;


procedure TCalibrationArchiver.SaveToFile(FName: String);
var
LEntry : TArchiveLogEntry;
FileLines : TStringList;
ENum : INteger;
begin
FileLines := TStringList.Create;
try
For Enum := 1 to RowCount-1 do
  begin
  Lentry := TArchiveLogEntry(Objects[0,Enum]);
  if assigned(LEntry) then FileLines.Add(Lentry.AsCSVEntry);
  end;
FileLines.SaveToFile(FName);
finally
FileLines.Free;
end;
end;

procedure TCalibrationArchiver.SaveToWorkingFile(IsFirstEntry : Boolean);
var
LEntry : TArchiveLogEntry;
FileLines : TStringList;
ENum : INteger;
begin
FileLines := TStringList.Create;
try
For Enum := 1 to RowCount-1 do
  begin
  Lentry := TArchiveLogEntry(Objects[0,Enum]);
  if assigned (Lentry) then FileLines.Add(Lentry.AsCSVEntry)
  end;
FileLines.SaveToFile(WorkingFileName);
finally
FileLines.Free;
end;
end;

procedure TCalibrationArchiver.Clean;
var
LEntry : TArchiveLogEntry;
begin
while RowCount > 2 do
  begin
  Lentry := TArchiveLogEntry(Objects[0,RowCount-1]);
  if assigned(LEntry) then Lentry.Free;
  DeleteRow(RowCount-1);
  end;

  Lentry := TArchiveLogEntry(Objects[0,RowCount-1]);
  if assigned(Lentry) then Lentry.Free;
  Cells[0,1] :='';
  Cells[1,1] :='';
  Cells[2,1] :='';
  IsClean := True;

end;

procedure TCalibrationArchiver.ArchiveAndRestartLog(CompBaseDir : String);
var
DateTimeString : String;
ArFilePath : String;
begin
if FileExists(WorkingFileName) then
  begin
  DateTimeString := AxisCalibrationUtility.CurrentDateAsFileSafeString;
  DateTimeString := DateTimeString+'_'+AxisCalibrationUtility.CurrentTimeAsFileSafeString;
  ArFilePath:= ExtractFilePath(WorkingFileName);
  ArFilePath := Format('%s\LogBackup%s.txt',[ArFilePath,DateTimeString]);
  SaveToFile(ArFilePath);
  Clean;
  end;
end;





procedure TCalibrationArchiver.AddLogEntry(Comment : String;IsArchive : Boolean;ArchFolder : String);
var
DStr,TStr : String;
LogEntry : TArchiveLogEntry;
begin
DStr := '0:0:0';
TStr := '0:0:0';
LogEntry := TArchiveLogEntry.Create;
try
if assigned(AxisCalibrationUtility) then
  begin
  DStr := AxisCalibrationUtility.StdDateString;
  TStr := AxisCalibrationUtility.StdTimeString;
  end;
if not IsClean then RowCount := RowCount+1;

LogEntry.IsFullArchive := IsArchive;
if IsArchive then
  begin
  LogEntry.ArchFolder := ArchFolder;
  end
else
  begin
  LogEntry.ArchFolder := '';
  end;
LogEntry.Time := TStr;
LogEntry.Date := DStr;
LogEntry.Comment := Comment;
Cells[0,RowCount-1] := LogEntry.Date;
Cells[1,RowCount-1] := LogEntry.Time;
Cells[2,RowCount-1] := LogEntry.Comment;
Objects[0,RowCount-1] := LogEntry;

SaveToWorkingFile(IsClean);
IsClean := False;
except
LogEntry.Free;
end;
end;

procedure TCalibrationArchiver.MyDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
ValStr : String;
LogEntry : TArchiveLogEntry;
Toffset : Integer;
begin
TOffset := 2;
  with Canvas do
  begin
  if ARow > 0 then
    begin
    LogEntry := TArchiveLogEntry(Objects[0,Arow]);
    if LogEntry.IsFullArchive then
      begin
      Brush.Color := clWebLemonChiffon;
      Font.Color := clBlue;
      end
    else
      begin
      Brush.Color := clWhite;
      Font.Color := clBlack;
      end
    end
  else
    begin
    TOffset := 6;
    Brush.Color := clLtGray;
    Font.Color := clBlack;
    end;
  FillRect(Rect);
  ValStr := Cells[ACol,Arow];
  TextOut(Rect.Left+TOffset,Rect.Top+2,ValStr);
  end;

end;


procedure TCalibrationArchiver.UpdateVisuals;
var
RNum : INteger;
LogEntry : TArchiveLogEntry;
begin
for RNum := RowCount-1 downto 1 do
  begin
  LogEntry := TArchiveLogEntry(Objects[0,RNum]);
  if assigned(LogEntry) then
    begin
    If LogEntry.IsFullArchive then
      begin
  //    Font.Color
      end
    else
      begin
      end;
    Cells[0,RNum] := LogEntry.Date;
    Cells[1,RNum] := LogEntry.Time;
    Cells[2,RNum] := LogEntry.Comment;
    end;
  end;
end;

function TCalibrationArchiver.CreateArchiveAndSaveCurrentComp(CompFilePath : String): String;
var
ArDir   : String;
DirName : String;
EFName : String;
BFName : String;
FileStrings : TStringList;
begin
DirName := Format('Comp_%s_%s',[AxisCalibrationUtility.CurrentDateAsFileSafeString,AxisCalibrationUtility.CurrentTimeAsFileSafeString]);
Result  := Format('%s\Archive\%s',[FCompBasePath,DirName]);
ForceDirectories(Result);
if FileExists(CompFilePath) then
  begin
  FileStrings := TStringList.Create;
  try
  FileStrings.LoadFromFile(CompFilePath);
  BFName := Format('%s\%s',[Result,'PreviousCompFile.txt']);
  FileStrings.SaveToFile(BFName);

  finally
  FileStrings.Free;
  end;
end;

end;


function TCalibrationArchiver.RestorePreviousCompFileFrom(ArFolder: String; CurrentCompFilePath : String): Boolean;
var
RestFile : String;
FileStrings : TStringList;
begin
Result := False;
RestFile := Format('%s\%s',[ArFolder,'PreviousCompFile.txt']);
if FileExists(RestFile) then
  begin
  If FileExists(CurrentCompFilePath) then
    begin
    if DeleteFile(CurrentCompFilePath) then
      begin
      FileStrings := TStringList.Create;
      try
      FileStrings.LoadFromFile(RestFile);
      FileStrings.SaveToFile(CurrentCompFilePath);
      Result := True;
      finally
      FileStrings.Free;
      end;
      end;
    end
  end
end;




{ TArchiveLogEntry }
function TArchiveLogEntry.AsCSVEntry: String;
begin
if IsFullArchive then
  begin
  Result := Format('1,%s,%s,%s,%s',[Date,Time,Comment,ArchFolder]);
  end
else
  begin
  Result := Format('0,%s,%s,%s,',[Date,Time,Comment]);
  end
end;

function TArchiveLogEntry.AsReportString: String;
begin
Result := Format('%s,%s,%s',[Date,Time,Comment]);
end;

function TArchiveLogEntry.LoadFromCSVString(InStr: String): Boolean;
var
Buffer : String;
Val : String;
begin
  Result := True;
  try
  Buffer := InStr;
  Val := NextToken(Buffer);
  if Val = '1' then
    begin
    IsFullArchive := True;
    end
  else
    begin
    IsFullArchive := False;
    end;
  Date := NextToken(Buffer);
  Time := NextToken(Buffer);
  Comment := NextToken(Buffer);
  if IsFullArchive then
    begin
    ArchFolder := NextToken(Buffer);
    end
  else
    begin
    ArchFolder := '';
    end
  except
  Result := False;
  end;
end;
end.
