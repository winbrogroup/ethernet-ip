unit RenishawImport;

Interface
uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls,IniFiles,Grids,CalDataLists;
type

TRenLoadErrors = (rlOk,rlWtongAxis,rlBadFile,rlNoFile,rlTooShort,rlStartMisMatch,rlSpacingMismatch,rlNumEntriesMismatch,rlModeMismatch);


TRenishawImporter = class(TStringList)
  private
    FCheckFileValidity: Boolean;
    function AddEntryFromText(InText : String; DataList : TList;BLUsed : Boolean): Boolean;
    function UpdateEntryAdjustFromText(InText: String; var DataList: TList; EntryIndex: Integer;BLUsed : Boolean): Boolean;
  public
    function CheckFileValidForAxisCalled(FilePathName : String;AxisName : String) : Boolean;
    function LoadFromFileToAxisAndGrid(FilePathName : String; AxisLists : TSingleAxisData;Grid : TInputDataGrid): TRenLoadErrors;
    function LoadAsAdjustToAxisAndGrid(FilePathName : String; AxisLists : TSingleAxisData;Grid : TInputDataGrid): TRenLoadErrors;
    property CheckFileValidity : Boolean read FCheckFileValidity write FCheckFileValidity;
  end;

implementation


function NextSpacedToken(var InStr : String):String;
var
Found : Boolean;
InLen : Integer;
Cnum : INteger;
Buffer : String;
begin
Buffer := Instr;
Result := '';
InLen := Length(Buffer);
Cnum := 1;
While (Buffer[CNum] = ' ') and (Cnum < InLen) do
  begin
  inc(CNum);
  end;
While (Buffer[CNum] <> ' ') and (Cnum <= InLen) do
  begin
  Result := Result+Buffer[Cnum];
  inc(CNum);
  end;
InStr := Copy(Instr,Cnum,Inlen-Cnum+1);
end;


{ TRenishawImporter }

function TRenishawImporter.UpdateEntryAdjustFromText(InText: String; var DataList : TList;EntryIndex : Integer;BLUsed : Boolean): Boolean;
var
Chunk : String;
LEntry : TInputDataRecord;
Index : INteger;
Pos,Fwd,Rev : Double;
begin
try
Result := False;
if EntryIndex >= DataList.count then
  begin
  exit;
  end;
Lentry :=Datalist[EntryIndex];
Result := True;
Chunk := NextSpacedToken(InText);
Index := StrToInt(Chunk);
if Index = (EntryIndex+1) then
  begin
  Chunk := NextSpacedToken(InText);
  POs := StrToFloat(Chunk);
  Chunk := NextSpacedToken(InText);
  Fwd := StrToFloat(Chunk);
  if BLUsed  then
    begin
    Chunk := NextSpacedToken(InText);
    Rev := StrToFloat(Chunk);
    end
  else
    begin
    Rev := 0;
    end;
  if Lentry.Position = Pos then
    begin
    Lentry.InputForwardAdjust := Fwd;
    Lentry.InputReverseAdjust := Rev;
    end
  else
    begin
    Lentry.InputForwardAdjust := 0;
    Lentry.InputReverseAdjust := 0;
    Result := False;
    end
  end
else
  begin
  Result := False;
  end
except
if assigned(LEntry) then Lentry.Free;
Result := False;
end;
end;


function TRenishawImporter.AddEntryFromText(InText: String; DataList : TList;BLUsed : Boolean): Boolean;
var
Chunk : String;
LEntry : TInputDataRecord;
Index : INteger;
Pos,Fwd,Rev : Double;
begin
Result := True;
try
Chunk := NextSpacedToken(InText);
Index := StrToInt(Chunk);
if BLUsed then
  begin
  if Index = (DataList.Count+1) then
    begin
    Chunk := NextSpacedToken(InText);
    POs := StrToFloat(Chunk);
    Chunk := NextSpacedToken(InText);
    Fwd := StrToFloat(Chunk);
    Chunk := NextSpacedToken(InText);
    Rev := StrToFloat(Chunk);
    LEntry := TInputDataRecord.Create;
    Lentry.Position := Pos;
    Lentry.InputForwardError := Fwd;
    Lentry.InputReverseError := Rev;
    DataList.Add(Lentry);
    end
  else
    begin
    Result := False;
    end
  end
else
  begin
  if Index = (DataList.Count+1) then
    begin
    Chunk := NextSpacedToken(InText);
    POs := StrToFloat(Chunk);
    Chunk := NextSpacedToken(InText);
    Fwd := StrToFloat(Chunk);
    Rev := 0;
    LEntry := TInputDataRecord.Create;
    Lentry.Position := Pos;
    Lentry.InputForwardError := Fwd;
    Lentry.InputReverseError := Rev;
    DataList.Add(Lentry);
    end
  end;
except
if assigned(LEntry) then Lentry.Free;
Result := False;
end;
end;

function TRenishawImporter.CheckFileValidForAxisCalled(FilePathName : String;AxisName: String): Boolean;
var
CurrentLine : String;
LNum : Integer;
Found : Boolean;
TextPos : Integer;
Chunk : String;
begin
Result := False;
  LoadFromFile(FilePathName);
  if Count > 10 then
    begin
    try
    CurrentLine := '';
    Found := False;
    LNum := 0;
    // Stage 1 Get File Id
    while (Not Found) and (Lnum < Count) do
      begin
      try
      CurrentLine := Uppercase(Strings[Lnum]);
      TextPos := Pos('FILE',CurrentLine);
      Found := TextPos > 0;
      finally
      inc(Lnum);
      end;
      end;
    If Found then
      begin
      CurrentLine := Copy(CurrentLine,TextPos+4,100);
      Chunk := NextSpacedToken(CurrentLine);
      If Chunk = AxisName then
        begin
        Result := True;
        exit
        end
      end;
    finally
    Clear;
    end
    end


end;

function TRenishawImporter.LoadAsAdjustToAxisAndGrid(FilePathName: String; AxisLists: TSingleAxisData;
  Grid: TInputDataGrid): TRenLoadErrors;
var
BEntry : TInputDataRecord;
CurrentLine : String;
LNum : Integer;
Found : Boolean;
TextPos : Integer;
TargetAxisName : String;
Chunk : String;
Fval : Double;
IntVal : Integer;
StartPos : Double;
EndPos : Double;
Spacing : Double;
ENum : Integer;
AdjustBLUsed : Boolean;
begin
Result := rlOk;
TargetAxisName := AxisLists.AxisName;
if FileExists(FilePathName) then
  begin
  LoadFromFile(FilePathName);
  if Count > 10 then
    begin
    try
    CurrentLine := '';
    Found := False;
    LNum := 0;
    if CheckFileValidity then
      begin
      // Stage 1 Get File Id
      while (Not Found) and (Lnum < Count) do
        begin
        try
        CurrentLine := Uppercase(Strings[Lnum]);
        TextPos := Pos('FILE',CurrentLine);
        Found := TextPos > 0;
        finally
        inc(Lnum);
        end;
        end;
      // Check is for correct Axis
      If Found then
        begin
        CurrentLine := Copy(CurrentLine,TextPos+4,100);
        Chunk := NextSpacedToken(CurrentLine);
        If Chunk <> TargetAxisName then
          begin
          Result := rlWtongAxis;
          exit
          end
        end;
      end;

    // Stage 2 Check Get Table type
    Found := False;
    while (Not Found) and (Lnum < Count) do
      begin
      try
      CurrentLine := Uppercase(Strings[Lnum]);
      TextPos := Pos('TABLE TYPE',CurrentLine);
      Found := TextPos > 0;
      finally
      inc(Lnum);
      end;
      end;
    If Found then
      begin
      CurrentLine := Copy(CurrentLine,11,100);
      Chunk := NextSpacedToken(CurrentLine);
      try
      AdjustBLUsed := True;
      If Chunk = 'COMBINED' then AdjustBLUsed := False;
      except
      Result := rlBadFile;
      exit;
      end;
      if AdjustBLUsed <> AxisLists.CompensationDataLists.BacklashUsed then
        begin
        Result := rlModeMismatch;
        exit;
        end;
      end;

    // Stage 3 Check Get Start Position
    Found := False;
    while (Not Found) and (Lnum < Count) do
      begin
      try
      CurrentLine := Uppercase(Strings[Lnum]);
      TextPos := Pos('COMPENSATION START',CurrentLine);
      Found := TextPos > 0;
      finally
      inc(Lnum);
      end;
      end;
    If Found then
      begin
      CurrentLine := Copy(CurrentLine,19,100);
      Chunk := NextSpacedToken(CurrentLine);
      try
      FVal := StrToFloat(Chunk);
      except
      Result := rlBadFile;
      exit;
      end;
      end;
    StartPos := FVal;
    if FVal <> Axislists.CompensationDataLists.FirstPosition then
      begin
      Result := rlStartMisMatch;
      exit;
      end;
    // Stage 4 Get end
    Found := False;
    while (Not Found) and (Lnum < Count) do
      begin
      try
      CurrentLine := Uppercase(Strings[Lnum]);
      TextPos := Pos('COMPENSATION END',CurrentLine);
      Found := TextPos > 0;
      finally
      inc(Lnum);
      end;
      end;
    If Found then
      begin
      CurrentLine := Copy(CurrentLine,17,100);
      Chunk := NextSpacedToken(CurrentLine);
      try
      FVal := StrToFloat(Chunk);
      except
      Result := rlBadFile;
      exit;
      end;
      end;
    EndPos := FVal;
    // Stage 5 Check Spacing
    Found := False;
    while (Not Found) and (Lnum < Count) do
      begin
      try
      CurrentLine := Uppercase(Strings[Lnum]);
      TextPos := Pos('COMPENSATION SPACING',CurrentLine);
      Found := TextPos > 0;
      finally
      inc(Lnum);
      end;
      end;
    If Found then
      begin
      CurrentLine := Copy(CurrentLine,21,100);
      Chunk := NextSpacedToken(CurrentLine);
      try
      FVal := StrToFloat(Chunk);
      except
      Result := rlBadFile;
      exit;
      end;
      end;
    Spacing := FVal;
    if Spacing <> AxisLists.CompensationDataLists.CompIncrement  then
      begin
      Result := rlSpacingMismatch;
      exit;
      end;
    if Round(abs(StartPos-EndPos)/Spacing)+1 <> AxisLists.CompensationDataLists.NumberOfEntries then
      begin
      Result := rlNumEntriesMismatch;
      exit;
      end;

    Found := False;
    If not Axislists.CompensationDataLists.BacklashUsed then
      begin
      // Stage 6 IF UNIDIRECTIONAL get fixed backlash
      while (Not Found) and (Lnum < Count) do
        begin
        try
        CurrentLine := Uppercase(Strings[Lnum]);
        TextPos := Pos('BACKLASH VALUE',CurrentLine);
        Found := TextPos > 0;
        finally
        inc(Lnum);
        end;
        end;
      If Found then
        begin
        CurrentLine := Copy(CurrentLine,15,100);
        Chunk := NextSpacedToken(CurrentLine);
        try
        FVal := StrToFloat(Chunk);
        IntVal := Round(Fval*1000)
        except
        Result := rlBadFile;
        exit;
        end;
        end;
      AxisLists.CompensationDataLists.ManualBackLashAdjust := IntVal;
      end;
    Found := False;


    if AxisLists.CompensationDataLists.NumberOfEntries> 1 then
      begin
      Found := False;
      while (Not Found) and (Lnum < Count) do
        begin
        try
        CurrentLine := Uppercase(Strings[Lnum]);
        TextPos := Pos('NO',CurrentLine);
        Found := TextPos =1;
        finally
        inc(Lnum);
        end;
        end;
      inc(LNum);
      CurrentLine := Uppercase(Strings[Lnum]);
      Grid.SaveToTempLoadBuffer; // zeros out adjust data
      if CurrentLine[1] = '1' then
        begin
        Enum := 0;
        try
        while(Lnum<Count) and (Enum < Grid.TemporaryLoadBuffer.Count) do
          begin
          try
          CurrentLine := Uppercase(Strings[Lnum]);
          UpdateEntryAdjustFromText(CurrentLine,Grid.TemporaryLoadBuffer,Enum,AdjustBLUsed);
          finally
          inc(Enum);
          inc(LNum);
          end;
          end
        finally

        end;
        end;
      end;
    except
    Result := rlBadFile;
    end; // try except
    end // If Count > 10
  else
    begin
    Result := rlTooShort;
    end
  end //if FileExists(
end;

function TRenishawImporter.LoadFromFileToAxisAndGrid(FilePathName: String; AxisLists: TSingleAxisData;Grid : TInputDataGrid): TRenLoadErrors;
var
CurrentLine : String;
LNum : Integer;
Found : Boolean;
TextPos : Integer;
TargetAxisName : String;
Chunk : String;
IVal : Integer;
Fval : Double;
IntVal : Integer;
StartPos : Double;
EndPos : Double;
Spacing : Double;
ENum : Integer;
begin
Result := rlOk;
TargetAxisName := AxisLists.AxisName;
if FileExists(FilePathName) then
  begin
  LoadFromFile(FilePathName);
  if Count > 10 then
    begin
    try
    CurrentLine := '';
    Found := False;
    LNum := 0;
    // Stage 1 Get File Id
    if CheckFileValidity then
      begin
      while (Not Found) and (Lnum < Count) do
        begin
        try
        CurrentLine := Uppercase(Strings[Lnum]);
        TextPos := Pos('FILE',CurrentLine);
        Found := TextPos > 0;
        finally
        inc(Lnum);
        end;
        end;
      // Check is for correct Axis
      If Found then
        begin
        CurrentLine := Copy(CurrentLine,TextPos+4,100);
        Chunk := NextSpacedToken(CurrentLine);
        If Chunk <> TargetAxisName then
          begin
          Result := rlWtongAxis;
          exit
          end
        end;
      end;

    // Stage 2 Check Get Table type
    Found := False;
    while (Not Found) and (Lnum < Count) do
      begin
      try
      CurrentLine := Uppercase(Strings[Lnum]);
      TextPos := Pos('TABLE TYPE',CurrentLine);
      Found := TextPos > 0;
      finally
      inc(Lnum);
      end;
      end;
    If Found then
      begin
      CurrentLine := Copy(CurrentLine,11,100);
      Chunk := NextSpacedToken(CurrentLine);
      try
      AxisLists.CompensationDataLists.BacklashUsed := True;
      If Chunk = 'COMBINED' then
        begin
        AxisLists.CompensationDataLists.BacklashUsed := False;
        end;
      except
      Result := rlBadFile;
      exit;
      end;
      end;

    // Stage 3 Check Get Start Position
    Found := False;
    while (Not Found) and (Lnum < Count) do
      begin
      try
      CurrentLine := Uppercase(Strings[Lnum]);
      TextPos := Pos('COMPENSATION START',CurrentLine);
      Found := TextPos > 0;
      finally
      inc(Lnum);
      end;
      end;
    If Found then
      begin
      CurrentLine := Copy(CurrentLine,19,100);
      Chunk := NextSpacedToken(CurrentLine);
      try
      FVal := StrToFloat(Chunk);
      except
      Result := rlBadFile;
      exit;
      end;
      end;
    StartPos := FVal;
    Axislists.CompensationDataLists.FirstPosition := FVal;
    // Stage 4 Check Get end
    Found := False;
    while (Not Found) and (Lnum < Count) do
      begin
      try
      CurrentLine := Uppercase(Strings[Lnum]);
      TextPos := Pos('COMPENSATION END',CurrentLine);
      Found := TextPos > 0;
      finally
      inc(Lnum);
      end;
      end;
    If Found then
      begin
      CurrentLine := Copy(CurrentLine,17,100);
      Chunk := NextSpacedToken(CurrentLine);
      try
      FVal := StrToFloat(Chunk);
      except
      Result := rlBadFile;
      exit;
      end;
      end;
    EndPos := FVal;
    // Stage 5 Check Spacing
    Found := False;
    while (Not Found) and (Lnum < Count) do
      begin
      try
      CurrentLine := Uppercase(Strings[Lnum]);
      TextPos := Pos('COMPENSATION SPACING',CurrentLine);
      Found := TextPos > 0;
      finally
      inc(Lnum);
      end;
      end;
    If Found then
      begin
      CurrentLine := Copy(CurrentLine,21,100);
      Chunk := NextSpacedToken(CurrentLine);
      try
      FVal := StrToFloat(Chunk);
      except
      Result := rlBadFile;
      exit;
      end;
      end;
    Spacing := FVal;
    with AxisLists.CompensationDataLists do
      begin
      CompIncrement := Spacing;
      NumberOfEntries :=  Round(abs(StartPos-EndPos)/Spacing)+1;
//      CompNegativeLimit := AxisLists.CompensationDataLists.FirstPosition;
//      CompPositiveLimit := EndPos;
      end;

    Found := False;
    If not Axislists.CompensationDataLists.BacklashUsed then
      begin
      // Stage 6 IF UNIDIRECTIONAL get fixed backlash
      while (Not Found) and (Lnum < Count) do
        begin
        try
        CurrentLine := Uppercase(Strings[Lnum]);
        TextPos := Pos('BACKLASH VALUE',CurrentLine);
        Found := TextPos > 0;
        finally
        inc(Lnum);
        end;
        end;
      If Found then
        begin
        CurrentLine := Copy(CurrentLine,15,100);
        Chunk := NextSpacedToken(CurrentLine);
        try
        FVal := StrToFloat(Chunk);
        IntVal := Round(Fval*1000)
        except
        Result := rlBadFile;
        exit;
        end;
        end;
      AxisLists.CompensationDataLists.ManualBacklashMicron := IntVal;
      end;
    Found := False;

    if AxisLists.CompensationDataLists.NumberOfEntries> 1 then
      begin
      Found := False;
      while (Not Found) and (Lnum < Count) do
        begin
        try
        CurrentLine := Uppercase(Strings[Lnum]);
        TextPos := Pos('NO',CurrentLine);
        Found := TextPos =1;
        finally
        inc(Lnum);
        end;
        end;
      inc(LNum);
      CurrentLine := Uppercase(Strings[Lnum]);
      Grid.CleanLoadBuffer;
      if CurrentLine[1] = '1' then
        begin
        Enum := 0;
        try
        while(Lnum<Count) and (Grid.TemporaryLoadBuffer.Count < AxisLists.CompensationDataLists.NumberOfEntries) do
          begin
          try
          CurrentLine := Uppercase(Strings[Lnum]);
          AddEntryFromText(CurrentLine,Grid.TemporaryLoadBuffer,AxisLists.CompensationDataLists.BacklashUsed);
          finally
          inc(Lnum);
          end;
          end
        finally

        end;
        end;
      end;
    except
    Result := rlBadFile;
    end; // try except
    end // If Count > 10
  else
    begin
    Result := rlTooShort;
    end
  end //if FileExists(
end;

end.
