unit ImportHandler;
interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls,IniFiles,Grids,CalDataLists;

type
 TFileAxisName = (tfaX,tfaY,tfaZ,tfaA,tfaB,tfaC);

 TFileTestRecord = record
   AxisLetter : TFileAxisName;
   Date : INteger;
   FullFilePath : String;
   end;

 TImportFileEntry = class(TObject)
  FileName : String;
  FileDate : Integer;
  AxisName : TFileAxisName;
  end;

 TAxCalImportFileList = class(TList)
  public
  function EntryForAxis(AxisLetter: TFileAxisName):TImportFileEntry;
  procedure AddFileIfLatest(TestRec : TFileTestRecord);
  public
  procedure LoadFromPath(LoadPath : String;var Availability : TAvailabityandStatusData);
  function UpdateAxisList(var Axislist : TCalAxisList): Boolean;
  function AvailabliltyMatchesAxisString(AxisString :String) : Boolean;
  procedure Clean;
  end;

implementation

{ TImportFileList }

procedure TAxCalImportFileList.AddFileIfLatest(TestRec: TFileTestRecord);
var
NewEntry : TImportFileEntry;
ExistingEntry : TImportFileEntry;
begin
  ExistingEntry := EntryForAxis(TestRec.AxisLetter);
  if assigned(ExistingEntry) then
    begin
    if ExistingEntry.FileDate < TestRec.Date then
      begin
      ExistingEntry.AxisName := TestRec.AxisLetter;
      ExistingEntry.FileName := TestRec.FullFilePath;
      ExistingEntry.FileDate := TestRec.Date;
      end
    end
   else
    begin
    NewEntry := TImportFileEntry.Create;
    NewEntry.AxisName := TestRec.AxisLetter;
    NewEntry.FileName := TestRec.FullFilePath;
    NewEntry.FileDate := TestRec.Date;
    Add(NewEntry);
    end
end;

function TAxCalImportFileList.AvailabliltyMatchesAxisString(AxisString: String): Boolean;

begin
if Count = Length(AxisString) then
  begin


  end;
end;

procedure TAxCalImportFileList.Clean;
var
FEntry : TImportFileEntry;
begin
while Count > 0 do
  begin
  FEntry := Items[Count-1];
  Fentry.Free;
  Delete(Count-1);
  end;
end;

function TAxCalImportFileList.EntryForAxis(AxisLetter: TFileAxisName): TImportFileEntry;
var
Enum : INteger;
FEntry : TImportFileEntry;
Found : Boolean;
begin
Result := nil;
if Count > 0 then
  begin
  Found := False;
  Enum := 0;
  while (Enum<Count) and (Not Found) do
    begin
    try
    Fentry := Items[Enum];
    If Fentry.AxisName =AxisLetter then
      begin
      Found := True;
      Result := Fentry;
      end;
    finally
    Inc(Enum)
    end;
    end
  end;
end;

procedure TAxCalImportFileList.LoadFromPath(LoadPath: String;var Availability : TAvailabityandStatusData);

var
SRec : TSearchRec;
CPOs : Integer;
FName : String;
FRec : TFileTestRecord;
begin
Clean;
Availability.XData.ImportExists := False;
Availability.YData.ImportExists := False;
Availability.ZData.ImportExists := False;
Availability.AData.ImportExists := False;
Availability.BData.ImportExists := False;
Availability.CData.ImportExists := False;
if DirectoryExists(LoadPath) then
  begin
  FindFirst(LoadPath+'*.*',faAnyFile,Srec);
  If Srec.Name <> '' then
    begin
    FName := Srec.Name;
    CPOs := Pos('Comp',FName);
    if Cpos = 2 then
      begin
      end;
    end;
  while FindNext(Srec) = 0 do
    begin
    FName := Srec.Name;
    CPOs := Pos('Comp',FName);
    if Cpos = 2 then
      begin
      if Fname[1] = 'X' then
        begin
        Frec.AxisLetter:= tfaX;
        Availability.XData.ImportExists := True;
        end
      else if Fname[1] = 'Y' then
        begin
        Frec.AxisLetter:= tfaY;
        Availability.YData.ImportExists := True;
        end
      else if Fname[1] = 'Z' then
        begin
        Frec.AxisLetter:= tfaZ;
        Availability.ZData.ImportExists := True;
        end
      else if Fname[1] = 'A' then
        begin
        Frec.AxisLetter:= tfaA;
        Availability.AData.ImportExists := True;
        end
      else if Fname[1] = 'B' then
        begin
        Frec.AxisLetter:= tfaB;
        Availability.BData.ImportExists := True;
        end
      else if Fname[1] = 'C' then
        begin
        Frec.AxisLetter:= tfaC;
        Availability.CData.ImportExists := True;
        end;
      Frec.Date := Srec.Time;
      Frec.FullFilePath := LoadPath+Fname;
      AddFileIfLatest(FRec);
     end;
    end;
  end;
 FindClose(SRec);
end;

function TAxCalImportFileList.UpdateAxisList(var Axislist: TCalAxisList): Boolean;
var
ListEntry :TImportFileEntry;
AxEntry : TSingleAxisData;
begin
  Result  := False;
  with Axislist.Availability do
    begin // with
{    XData.NotComped := False;
    YData.NotComped := False;
    ZData.NotComped := False;
    AData.NotComped := False;
    BData.NotComped := False;
    CData.NotComped := False;}
    if XData.Required and  not (XData.WorkingDataState = tcdAvailable) then
      begin
      ListEntry := EntryForAxis(tfaX);
      if Assigned(ListEntry) then
        begin
        Result := True;
        AxEntry := AxisList.EntryWithAxisName('X');
        if assigned(AxEntry) then
          begin
          if AxEntry.CompensationDataLists.ReadFromExportFile(ListEntry.FileName) then
            begin
            XData.ImportDataState := tcdAvailable;
            XData.ImportExists := True;
            end
          else
            begin
            XData.ImportExists := False;
          end;
        end
      else
        begin
        XData.ImportExists := True;
        end;
      end;

    if YData.Required and Not (YData.WorkingDataState = tcdAvailable) then
      begin
      ListEntry := EntryForAxis(tfaY);
      if Assigned(ListEntry) then
        begin
        Result := True;
        AxEntry := AxisList.EntryWithAxisName('Y');
        if assigned(AxEntry) then
          begin
          if AxEntry.CompensationDataLists.ReadFromExportFile(ListEntry.FileName) then
            begin
            YData.ImportExists := True;
            YData.ImportDataState := tcdAvailable;
            end
          else
            begin
            YData.ImportExists := False;
            end;
          end;
        end
      else
        begin
        YData.ImportExists := True;
        end;
      end;

    if ZData.Required and Not (ZData.WorkingDataState = tcdAvailable) then
      begin
      ListEntry := EntryForAxis(tfaZ);
      if Assigned(ListEntry) then
        begin
        Result := True;
        AxEntry := AxisList.EntryWithAxisName('Z');
        if assigned(AxEntry) then
          begin
          if AxEntry.CompensationDataLists.ReadFromExportFile(ListEntry.FileName) then
            begin
            ZData.ImportExists := True;
            ZData.ImportDataState := tcdAvailable;
            end
          else
            begin
            ZData.ImportExists := False;
            end;
          end;
        end
      else
        begin
        ZData.ImportExists := False;
        end;
      end;

    if AData.Required and Not (AData.WorkingDataState = tcdAvailable) then
      begin
      ListEntry := EntryForAxis(tfaA);
      if Assigned(ListEntry) then
        begin
        Result := True;
        AxEntry := AxisList.EntryWithAxisName('A');
        if assigned(AxEntry) then
          begin
          if AxEntry.CompensationDataLists.ReadFromExportFile(ListEntry.FileName) then
            begin
            AData.ImportExists := True;
            AData.ImportDataState := tcdAvailable;
            end
          else
            begin
            AData.ImportExists := False;
            end;
          end;
        end
      else
        begin
        AData.ImportExists := False;
        end;
      end;

    if BData.Required and Not (BData.WorkingDataState = tcdAvailable) then
      begin
      ListEntry := EntryForAxis(tfaB);
      if Assigned(ListEntry) then
        begin
        Result := True;
        AxEntry := AxisList.EntryWithAxisName('B');
        if assigned(AxEntry) then
          begin
          if AxEntry.CompensationDataLists.ReadFromExportFile(ListEntry.FileName) then
            begin
            BData.ImportExists := True;
            BData.ImportDataState := tcdAvailable;
            end
          else
            begin
            BData.ImportExists := False;
            end;
          end;
        end
      else
        begin
        BData.ImportExists := False;
        end;
      end;

    if CData.Required and Not (CData.WorkingDataState = tcdAvailable) then
      begin
      ListEntry := EntryForAxis(tfaC);
      if Assigned(ListEntry) then
        begin
        Result := True;
        AxEntry := AxisList.EntryWithAxisName('C');
        if assigned(AxEntry) then
          begin
          if AxEntry.CompensationDataLists.ReadFromExportFile(ListEntry.FileName) then
            begin
            CData.ImportExists := True;
            CData.ImportDataState := tcdAvailable;
            end
          else
            begin
            CData.ImportExists := False;
            end;
          end;
        end
      else
        begin
        CData.ImportExists := False;
        end;
      end;
    end;  // with
  end;
end;

end.
