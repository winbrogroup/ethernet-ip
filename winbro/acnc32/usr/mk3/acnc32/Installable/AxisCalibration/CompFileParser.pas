unit CompFileParser;

Interface
uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls,IniFiles,Grids,CalDataLists;

type
TCompareEntry = Class(Tobject)
  LeftValStr : String;
  RightValStr: String;
  IsOK : Boolean;
  constructor Create;
  procedure AddBooleanResult(Left,Right: Boolean);
  procedure AddIntegerResult(Left,Right: Integer);
  procedure AddFloatResult(Left,Right :Double);
  procedure AddStringResult(Left,Right :String ;Pass : Boolean);
  function ResultAsString(Prefix : String): String;
  end;

TCompExistanceRecord = record
  XCompExists : Boolean;
  YCompExists : Boolean;
  ZCompExists : Boolean;
  ACompExists : Boolean;
  BCompExists : Boolean;
  CCompExists : Boolean;
  end;

TCompFileCompareResults = Class(Tobject)
  MotorNumber : TCompareEntry ;
  HomeOffset  : TCompareEntry ;
  EncoderCounts : TCompareEntry ;
  NumEntries : TCompareEntry ;
  Span : TCompareEntry ;
  Data : TCompareEntry ;
  //MakeOutput : TCompareEntry ;
  constructor Create;
  destructor Destroy; override;
  procedure WriteErrorsToStrings(OutList : TStrings);
  procedure WriteAllStatusToStrings(OutList : TStrings);
  procedure Initialise;
  function AllOK : Boolean;
 end;


TExistingCompEntry = Class(TObject)
 CompVal : Integer;
 Position : Double;
 EncoderPosition : Double;
 function ReadFromString(InString : String): Boolean;
 end;

TExistingCompData = Class(TList)
  private
    function FindIndexFrom(StartLine : Integer;Lines : TStringList;Search : String;var RemLine : String): Integer;
    function ReadFromCompFileForAxis(AxName, CompFileName: String): Boolean;
  public
    HomeOffset : Double;
    EncoderCountsPerUnit : Double;
    NumEntries : Integer;
    Span : Double;
    MotorNumber : Integer;
    AxisName : String;
    RollOverIndex : Integer;
//    MakeOutput : Boolean;
    procedure Clean;
    destructor Destroy; override;
    function CompareDataTo(CompareData : TExistingCompData;var CompResult : TCompFileCompareResults):Boolean;
    function ReadHeaderFromStartIndex(SIndex : INteger;Lines : TStringList; Var NextIndex: Integer): Boolean;
    function ReadCompDataFromStartIndex(StartLine : INteger;Lines : TStringList): Boolean;
  end;

// List of TExistingCompEntry
TExistingBacklashData = Class(TList)
  private
    function FindIndexFrom(StartLine : Integer;Lines : TStringList;Search : String;var RemLine : String): Integer;
    function ReadFromCompFileForAxis(AxName, CompFileName: String): Boolean;
  public
    HomeOffset : Double;
    EncoderCountsPerUnit : Double;
    NumEntries : Integer;
    Span : Double;
    MotorNumber : Integer;
    AxisName : String;
    OverallBackLash : Double;
    RollOverIndex : Integer;
    procedure Clean;
    destructor Destroy; override;
    function CompareDataTo(CompareData : TExistingBacklashData;var CompResult : TCompFileCompareResults):Boolean;
    function ReadHeaderFromStartIndex(SIndex : INteger;Lines : TStringList; Var NextIndex: Integer): Boolean;
    function ReadCompDataFromStartIndex(StartLine : INteger;Lines : TStringList): Boolean;
  end;

TCompareAxisData = class(TObject)
  private
  public
    AxisName : String;
    HasBacklashData : Boolean;
    CompData : TExistingCompData;
    BLCompData : TExistingBacklashData;
    CompExists : Boolean;
    OriginalInputEntryCount : Integer; // externally set to determin whether or not double output points used
    procedure Clean;
    Constructor Create;
    Destructor Destroy; override;
    end;


TExistingAxisData = class(TObject)
  private
  public
    AxisName : String;
    HasBacklashData : Boolean;
    CompData : TExistingCompData;
    BLCompData : TExistingBacklashData;
    CompareResult : TCompFileCompareResults;
    BLCompareResult: TCompFileCompareResults;
    CompExists : Boolean;
    OriginalInputEntryCount : Integer; // externally set to determin whether or not double output points used
    procedure Clean;
    Constructor Create;
    Destructor Destroy; override;
    function CompareCompAndBLDataTo(CompEntry : TCompareAxisData):Boolean;
  end;

TExistingCompList = Class(TList)
  procedure Clean;
  function EntryForAxisCalled(AxName : String): TExistingAxisData;
  function CompResultsForAxisCalled(AxName : String): TCompFileCompareResults;
  function BLCompResultsForAxisCalled(AxName : String): TCompFileCompareResults;
  function IsBlank(var CompCount : INteger) : Boolean;
end;

TCompareCompList = Class(TList)
  procedure Clean;
  function EntryForAxisCalled(AxName : String): TCompareAxisData;
//  function CompResultsForAxisCalled(AxName : String): TCompFileCompareResults;
//  function BLCompResultsForAxisCalled(AxName : String): TCompFileCompareResults;
  function IsBlank(var CompCount : INteger) : Boolean;
end;


 TCompFileParser = Class(Tobject)
  private
    function IndexofAxisCompData(AxisName : String): Integer;
    function IndexofBackLashData(AxisName : String): Integer;
    Function LoadCompareFileFromStrings(FileLines: TStringList;AxisString : String): Boolean;
    function LoadCompFileFromStrings(FileLines: TStringList; AxisString: String): Boolean;

  public
    FileLines : TStringList;
    CompFilename : String;
    ExistingCompList : TExistingCompList; // List of TExistingAxisData Current CompFile
    CompareCompList  : TCompareCompList;  // List of TExistingAxisData for insipient Compfile
    FAxisString : String;
    constructor Create;
    destructor Destroy; override;
    function CompDataForAxisNameExists(AxName : String): Boolean;
    function ExistingDataForAxisName(AxName : String): TExistingAxisData;
    function LoadCompFile(FileName : String;AxisString : String): Boolean;
    function CompareCompFileToStrings(CompStrings : TStrings;var Availability :TAvailabityandStatusData): Boolean;
    procedure CompareNumberEntries(CompStrings : TStrings;var Availability :TAvailabityandStatusData);
    function SetInputEntriesForAxisCalled(Axname : String;NumEntries: Integer):Boolean;
    Function EntryWithNameUsesDoublePoints(Axname : String):Boolean;
    procedure CheckCompFileStatus(FileName : String;var Availability :TAvailabityandStatusData;AxString : String);

  end;
var
EnforcedCompExistence : TCompExistanceRecord;

implementation
function SlashSep(const Path, S: String): String;
begin
  if AnsiLastChar(Path)^ <> '\' then
    Result := Path + '\' + S
  else
    Result := Path + S;
end;

function NextToken(var InStr : String):String;
var
CommaPos : Integer;
begin
Result := '';
CommaPos := Pos(',',InStr);
if CommaPos > 0 then
  begin
  Result := Trim(Copy(InStr,1,CommaPos-1));

  InStr := Copy(Instr,CommaPos+1,1000);
  end
else
  begin
  Result := Instr;
  Instr := '';
  end;
end;

{ TExistingCompData }

procedure TExistingCompData.Clean;
var
Lentry : TExistingCompEntry;
begin
 while(Count > 0) do
  begin
  Lentry := Items[0];
  Lentry.Free;
  Delete(0);
  end
end;

destructor TExistingCompData.Destroy;
begin
  Clean;
  inherited;
end;

function TExistingCompData.ReadFromCompFileForAxis(AxName, CompFileName: String): Boolean;
begin
Result := False;
if FileExists(CompFileName) then
  begin

  end;
end;


function TExistingCompData.FindIndexFrom(StartLine : Integer;Lines : TStringList;Search : String;var RemLine : String): Integer;
var
SString : String;
CLine : String;
LNum : Integer;
SPos : Integer;
Found : Boolean;
Fail : Boolean;
begin
Result := -1;
Found := False;
Fail := False;
SString := UpperCase(Search);
LNum := StartLine;
while (Found = False) and (LNum < Lines.Count) and (Fail = False)do
  begin
  try
  CLine := UpperCase(Lines[LNum]);
  Spos := Pos(SString,CLine);
  If SPOs > 0 then
    begin
    Found := True;
    Result := LNum;
    RemLine := Trim(Copy(CLine,Spos+Length(SString),Length(Cline)));
    end;
  Spos := POs('COMPENSATION',Cline);
  if Spos > 0 then
    begin
    Fail := True;
    end;
  finally
  inc(LNum);
  end;
  end;

end;

function TExistingCompData.ReadHeaderFromStartIndex(SIndex: INteger; Lines: TStringList; Var NextIndex : Integer): Boolean;
var
PIndex : Integer;
FPos : Integer;
Remainder : String;
StrVal : String;
begin
  Result := False;
  NextIndex := SIndex;
  If Lines.Count > SIndex Then
    begin
    // Get Home Offset
    PIndex := FindIndexFrom(SIndex,Lines,'Home Offset',Remainder);
    If PIndex > 0 then
      begin
      FPos := POs('AT',Remainder);
      Remainder := Trim(Copy(Remainder,Fpos+2));
      FPos := Pos(' ',Remainder);
      StrVal :=  Copy(Remainder,1,Fpos);
      try
      HomeOffset := StrToFloat(StrVal);
      except
      Exit;
      end;
      end;

    // Get Encoder Counts per Unit
    PIndex := FindIndexFrom(SIndex,Lines,'Encoder Counts per',Remainder);
    If PIndex > 0 then
      begin
      FPos := POs('=',Remainder);
      StrVal := Trim(Copy(Remainder,Fpos+1));
      try
      EncoderCountsPerUnit := StrToFloat(StrVal);
      except
      Exit;
      end;
      end;

    // Get Motor Num
    PIndex := FindIndexFrom(SIndex,Lines,'Uses Motor Number',Remainder);
    If PIndex > 0 then
      begin
      StrVal := Trim(NextToken(Remainder));
      try
      MotorNumber := StrToInt(StrVal);
      except
      Exit;
      end;
      end;
    // Get NumEntries and Span
    PIndex := FindIndexFrom(SIndex,Lines,'DEFINE COMP',Remainder);
    If PIndex > 0 then
      begin
      StrVal := Trim(NextToken(Remainder));
      try
      NumEntries := StrToInt(StrVal);
      except
      Exit;
      end;
      StrVal := Trim(NextToken(Remainder));
      try
      Span := StrToFloat(StrVal);
      except
      Exit;
      end;
      end;
    Result := True;
    NextIndex := PIndex+1;
    end;
end;

function TExistingCompData.ReadCompDataFromStartIndex(StartLine : INteger;Lines: TStringList): Boolean;
var
CLine : String;
Lnum : Integer;
NewEntry : TExistingCompEntry;
FoundNeg : Boolean;
begin
Clean;
Result := false;
If Lines.Count > StartLine+NumEntries then
  begin
  FoundNeg := False;
  For Lnum := 0 to NumEntries-1 do
    begin
    NewEntry := TExistingCompEntry.Create;
    CLine := Lines[Startline+Lnum];
    if NewEntry.ReadFromString(CLine) then
      begin
      Add(NewEntry);
      if Not FoundNeg then
        begin
        If NewEntry.Position < 0 then
          begin
          FoundNeg := True;
          RollOverIndex := LNum-1;
          end;
        end;
      end;
    end
  end;
end;

function TExistingCompData.CompareDataTo(CompareData : TExistingCompData;var CompResult : TCompFileCompareResults):Boolean;
var
FoundError : Boolean;
ENum : Integer;
CEntry,CompareEntry : TExistingCompEntry;
begin
Result := False;
CompResult.HomeOffset.AddFloatResult(CompareData.HomeOffset,HomeOffset);
CompResult.EncoderCounts.AddFloatResult(CompareData.EncoderCountsPerUnit,EncoderCountsPerUnit);
CompResult.NumEntries.AddIntegerResult(CompareData.NumEntries,NumEntries);
CompResult.Span.AddFloatResult(CompareData.Span,Span);
//CompResult.MakeOutput.AddBooleanResult(CompareData.MakeOutput,MakeOutput);
CompResult.MotorNumber.AddIntegerResult(CompareData.MotorNumber,MotorNumber);

if Count = CompareData.Count then
  begin
  FoundError := False;
  ENum := 0;
  while (Enum<Count) and (not FoundError) do
    begin
    try
    CEntry := Items[ENum];
    CompareEntry := CompareData.Items[ENum];
    if (Centry.Position <> CompareEntry.Position) or (Centry.EncoderPosition <> CompareEntry.EncoderPosition) then
      begin
      //Positions not the same therfore an error
      FoundError := True;
      end
    else
      begin
      if abs(Centry.CompVal - CompareEntry.CompVal) > 4 then
        begin
        //Only an error if not near limit of travel
        if abs(RollOverIndex-Enum) > 3 then
              FoundError := True;
        end
      end;
    finally
    inc(Enum);
    end;
    end;
  Result := not FoundError;
  if Result then
    begin
    CompResult.Data.AddStringResult('','',True);
    end
  else
    begin
    CompResult.Data.AddStringResult('','',False);
    end
  end;
end;


{ TCompFileParser }

Function TCompFileParser.LoadCompFile(FileName: String;AxisString : String): Boolean;
begin
Result := False;
ExistingCompList.Clean;
If FileExists(FileName) then
  begin
  FileLines := TStringList.Create;
  try
  FileLines.LoadFromFile(FileName);
  Result := LoadCompFileFromStrings(FileLines,AxisString);
  if Result then FAxisString := AxisString;
  finally
  FileLines.Free;
  end;
  end;
  EnforcedCompExistence.XCompExists := CompDataForAxisNameExists('X');
  EnforcedCompExistence.YCompExists := CompDataForAxisNameExists('Y');
  EnforcedCompExistence.ZCompExists := CompDataForAxisNameExists('Z');
  EnforcedCompExistence.ACompExists := CompDataForAxisNameExists('A');
  EnforcedCompExistence.BCompExists := CompDataForAxisNameExists('B');
  EnforcedCompExistence.CCompExists := CompDataForAxisNameExists('C');
end;



Function TCompFileParser.LoadCompareFileFromStrings(FileLines: TStringList;AxisString : String): Boolean;
var
AxCount : Integer;
AxName : String;
AxNum : INteger;
StartIndex : Integer;
NextIndex : INteger;
CLEntry : TCompareAxisData;
  begin
  Result := False;
  CompareCompList.Clean;
  AxCount := Length(Trim(AxisString));
  if AxCount > 0 then
    begin
    For AxNum := 0 to AxCount-1 do
      begin
      CLEntry := TCompareAxisData.Create;
      AxName := AxisString[Axnum+1];
      CLentry.AxisName := AxName;
      StartIndex := IndexofAxisCompData(AxName);
      if StartIndex > 0 then
        begin
        if CLEntry.CompData.ReadHeaderFromStartIndex(StartIndex+1,FileLines,NextIndex) then
          begin
          CLEntry.CompData.ReadCompDataFromStartIndex(NextIndex,FileLines);
          CLEntry.CompExists := True;
          end;
        end;
      StartIndex := IndexofBackLashData(AxName);
      if StartIndex > 0 then
        begin
        if CLEntry.BLCompData.ReadHeaderFromStartIndex(StartIndex+1,FileLines,NextIndex) then
          begin
          CLEntry.BLCompData.ReadCompDataFromStartIndex(NextIndex,FileLines);
          CLEntry.HasBacklashData := True;
          end;
        end
      else
        begin
        CLEntry.HasBacklashData := False;
        end;
      CompareCompList.Add(CLEntry)
      end;
    end;
    Result := True;
end;

Function TCompFileParser.LoadCompFileFromStrings(FileLines: TStringList;AxisString : String): Boolean;
var
AxCount : Integer;
AxName : String;
AxNum : INteger;
StartIndex : Integer;
NextIndex : INteger;
CLEntry : TExistingAxisData;
  begin
  Result := False;
  AxCount := Length(Trim(AxisString));
  if AxCount > 0 then
    begin
    For AxNum := 0 to AxCount-1 do
      begin
      CLEntry := TExistingAxisData.Create;
      AxName := AxisString[Axnum+1];
      CLentry.AxisName := AxName;
      StartIndex := IndexofAxisCompData(AxName);
      if StartIndex > 0 then
        begin
        if CLEntry.CompData.ReadHeaderFromStartIndex(StartIndex+1,FileLines,NextIndex) then
          begin
          CLEntry.CompData.ReadCompDataFromStartIndex(NextIndex,FileLines);
          CLEntry.CompExists := True;
          end;
        end;
      StartIndex := IndexofBackLashData(AxName);
      if StartIndex > 0 then
        begin
        if CLEntry.BLCompData.ReadHeaderFromStartIndex(StartIndex+1,FileLines,NextIndex) then
          begin
          CLEntry.BLCompData.ReadCompDataFromStartIndex(NextIndex,FileLines);
          CLEntry.HasBacklashData := True;
          end;
        end
      else
        begin
        CLEntry.HasBacklashData := False;
        end;
      ExistingCompList.Add(CLEntry)
      end;
    end;
    Result := True;
end;

function TCompFileParser.IndexofBackLashData(AxisName: String): Integer;
var
Found : Boolean;
LNum : Integer;
SString : String;
CurrentLine : String;
FPOs : Integer;
begin
Result := -1;
If FileLines.Count > 0 then
  begin
  Found := False;
  LNum := 0;
  SString := Format(';%S AXIS BACKLASH COMPENSATION',[UpperCase(AxisName)]);
  while (Found = False) and (LNum < FileLines.Count) do
    begin
    try
    CurrentLine := UpperCase(FileLines[LNum]);
    FPos := Pos(SString,CurrentLine);
    Found := Fpos > 0;
    if Found then
      Result := LNum;
    finally
    Inc(Lnum)
    end;
    end;
  end;
end;

function TCompFileParser.CompDataForAxisNameExists(AxName: String): Boolean;
var
TestEntry :TExistingAxisData;
begin
Result := false;
if assigned(ExistingCompList) then
  begin
  TestEntry := ExistingCompList.EntryForAxisCalled(AxName);
  if assigned(TestEntry) then
    begin
    Result := TestEntry.CompExists;
    end
  else
    begin
    Result := False;
    end
  end;
inherited;
end;


function TCompFileParser.ExistingDataForAxisName(AxName: String): TExistingAxisData;
begin
Result := nil;
if assigned(ExistingCompList) then
  begin
  Result := ExistingCompList.EntryForAxisCalled(AxName);
  end;
end;


function TCompFileParser.IndexofAxisCompData(AxisName : String): Integer;
var
Found : Boolean;
LNum : Integer;
SString : String;
CurrentLine : String;
FPOs : Integer;
begin
Result := -1;
If FileLines.Count > 0 then
  begin
  Found := False;
  LNum := 0;
  SString := Format(';%S AXIS COMPENSATION',[UpperCase(AxisName)]);
  while (Found = False) and (LNum < FileLines.Count) do
    begin
    try
    CurrentLine := UpperCase(FileLines[LNum]);
    FPos := Pos(SString,CurrentLine);
    Found := Fpos > 0;
    if Found then Result := LNum;
    finally
    Inc(Lnum)
    end;
    end;
  end;
end;


constructor TCompFileParser.Create;
begin
ExistingCompList := TExistingCompList.Create;
CompareCompList  := TCompareCompList.Create;
EnforcedCompExistence.XCompExists := False;
EnforcedCompExistence.YCompExists := False;
EnforcedCompExistence.ZCompExists := False;
EnforcedCompExistence.ACompExists := False;
EnforcedCompExistence.BCompExists := False;
EnforcedCompExistence.CCompExists := False;
end;

destructor TCompFileParser.Destroy;
begin
ExistingCompList.Clean;
ExistingCompList.Free;
CompareCompList.Clean;
CompareCompList.Free;
inherited;
end;

procedure TCompFileParser.CompareNumberEntries(CompStrings: TStrings; var Availability: TAvailabityandStatusData);
var
AxNum : Integer;
LNum : Integer;
EnforcedEntry : TExistingAxisData;
CompareEntry  : TCompareAxisData;
begin
 // First Load CompStrings Data
 FileLines := TStringList.Create;
 try
 For LNum := 0 to CompStrings.Count-1 do
  begin
  FileLines.add(CompStrings[LNum])
  end;
 LoadCompareFileFromStrings(FileLines,FAxisString);
 if ExistingCompList.Count = ComPareCompList.count then
   begin
   For AxNum := 0 to ExistingCompList.Count-1 do
    begin
    EnforcedEntry := ExistingCompList[AxNum];
    CompareEntry :=  CompareCompList.EntryForAxisCalled(EnforcedEntry.AxisName);
    if assigned(CompareEntry) then
      begin
      if EnforcedEntry.AxisName = 'X' then
        begin
        Availability.XData.ExistingCompEntries := EnforcedEntry.CompData.Count;
        Availability.XData.InternalCompEntries := CompareEntry.CompData.Count;
        end
      else if EnforcedEntry.AxisName = 'Y' then
        begin
        Availability.YData.ExistingCompEntries := EnforcedEntry.CompData.Count;
        Availability.YData.InternalCompEntries := CompareEntry.CompData.Count;
        end
      else if EnforcedEntry.AxisName = 'Z' then
        begin
        Availability.ZData.ExistingCompEntries := EnforcedEntry.CompData.Count;
        Availability.ZData.InternalCompEntries := CompareEntry.CompData.Count;
        end
      else if EnforcedEntry.AxisName = 'A' then
        begin
        Availability.AData.ExistingCompEntries := EnforcedEntry.CompData.Count;
        Availability.AData.InternalCompEntries := CompareEntry.CompData.Count;
        end
      else if EnforcedEntry.AxisName = 'B' then
        begin
        Availability.BData.ExistingCompEntries := EnforcedEntry.CompData.Count;
        Availability.BData.InternalCompEntries := CompareEntry.CompData.Count;
        end
      else if EnforcedEntry.AxisName = 'C' then
        begin
        Availability.CData.ExistingCompEntries := EnforcedEntry.CompData.Count;
        Availability.CData.InternalCompEntries := CompareEntry.CompData.Count;
        end
      end;
    end;
   end;
 finally
 FileLines.Free;
 end;
end;


function TCompFileParser.CompareCompFileToStrings(CompStrings: TStrings;var Availability :TAvailabityandStatusData): Boolean;
var
AxNum : Integer;
LNum : Integer;
EnforcedEntry : TExistingAxisData;
CompareEntry  : TCompareAxisData;

begin
 Result := False;
 // First Load CompStrings Data
 FileLines := TStringList.Create;
 try
 For LNum := 0 to CompStrings.Count-1 do
  begin
  FileLines.add(CompStrings[LNum])
  end;
 LoadCompareFileFromStrings(FileLines,FAxisString);
 if ExistingCompList.Count = ComPareCompList.count then
   begin
   For AxNum := 0 to ExistingCompList.Count-1 do
    begin
    EnforcedEntry := ExistingCompList[AxNum];
    CompareEntry :=  ComPareCompList.EntryForAxisCalled(EnforcedEntry.AxisName);
    if assigned(CompareEntry) then
      begin
      if (not CompareEntry.CompExists) and (not EnforcedEntry.CompExists) then
        begin
        Result := EnforcedEntry.CompareCompAndBLDataTo(CompareEntry);
        if EnforcedEntry.AxisName = 'X' then Availability.XData.DataMatchByOutput := Result
        else if EnforcedEntry.AxisName = 'Y' then Availability.YData.DataMatchByOutput := Result
        else if EnforcedEntry.AxisName = 'Z' then Availability.ZData.DataMatchByOutput := Result
        else if EnforcedEntry.AxisName = 'A' then Availability.AData.DataMatchByOutput := Result
        else if EnforcedEntry.AxisName = 'B' then Availability.BData.DataMatchByOutput := Result
        else if EnforcedEntry.AxisName = 'C' then Availability.CData.DataMatchByOutput := Result;
        end;
      if (CompareEntry.CompExists) and (EnforcedEntry.CompExists) then
        begin
        Result := EnforcedEntry.CompareCompAndBLDataTo(CompareEntry);
        if EnforcedEntry.AxisName = 'X' then Availability.XData.DataMatchByOutput := Result
        else if EnforcedEntry.AxisName = 'Y' then Availability.YData.DataMatchByOutput := Result
        else if EnforcedEntry.AxisName = 'Z' then Availability.ZData.DataMatchByOutput := Result
        else if EnforcedEntry.AxisName = 'A' then Availability.AData.DataMatchByOutput := Result
        else if EnforcedEntry.AxisName = 'B' then Availability.BData.DataMatchByOutput := Result
        else if EnforcedEntry.AxisName = 'C' then Availability.CData.DataMatchByOutput := Result;
        end
      else if (CompareEntry.CompExists) <> (EnforcedEntry.CompExists) then
        begin
        if EnforcedEntry.AxisName = 'X' then Availability.XData.DataMatchByOutput := False
        else if EnforcedEntry.AxisName = 'Y' then Availability.YData.DataMatchByOutput := False
        else if EnforcedEntry.AxisName = 'Z' then Availability.ZData.DataMatchByOutput := False
        else if EnforcedEntry.AxisName = 'A' then Availability.AData.DataMatchByOutput := False
        else if EnforcedEntry.AxisName = 'B' then Availability.BData.DataMatchByOutput := False
        else if EnforcedEntry.AxisName = 'C' then Availability.CData.DataMatchByOutput := False;
        end
      else
        begin
        if EnforcedEntry.AxisName = 'X' then Availability.XData.DataMatchByOutput := True
        else if EnforcedEntry.AxisName = 'Y' then Availability.YData.DataMatchByOutput := True
        else if EnforcedEntry.AxisName = 'Z' then Availability.ZData.DataMatchByOutput := True
        else if EnforcedEntry.AxisName = 'A' then Availability.AData.DataMatchByOutput := True
        else if EnforcedEntry.AxisName = 'B' then Availability.BData.DataMatchByOutput := True
        else if EnforcedEntry.AxisName = 'C' then Availability.CData.DataMatchByOutput := True;
        end
      end;
    end;
   end;
 finally
 FileLines.Free;
 end;

end;


function TCompFileParser.EntryWithNameUsesDoublePoints(Axname: String): Boolean;
begin

end;

Function TCompFileParser.SetInputEntriesForAxisCalled(Axname: String; NumEntries: Integer): Boolean;
var
ExistingData : TExistingAxisData;
begin
Result := False;
ExistingData := ExistingDataForAxisName(AxName);
if assigned(ExistingData) then
  begin
  ExistingData.OriginalInputEntryCount := NumEntries;
  end
end;


procedure TCompFileParser.CheckCompFileStatus(FileName: String; var Availability: TAvailabityandStatusData;AxString : String);
var
CompCount : Integer;
begin
ExistingCompList.Clean;
Availability.FullCompFileExists := False;
Availability.BlankCompFileExists := False;
Availability.PartialCompFileExists := False;
If FileExists(FileName) then
  begin
  LoadCompFile(Filename,AxString);
  if ExistingCompList.Count > 0 then
    begin
    if  ExistingCompList.isblank(CompCount) then
      begin
      Availability.BlankCompFileExists := True;
      end
    else
      begin
      if CompCount = Length(AxString) then
        begin
        Availability.FullCompFileExists := True;
        end
      else
        begin
        Availability.PartialCompFileExists := True;
        end
      end
    end
  end;
ExistingCompList.Clean;
end;


{ TExistingCompEntry }
function TExistingCompEntry.ReadFromString(InString: String): Boolean;
var
FPos : Integer;
Remainder : String;
Work : String;
begin
Result := False;
Remainder := InString;
FPOs := POs(';',Remainder);
if Fpos > 1 then
  begin
  Work := Trim(Copy(Remainder,1,Fpos-1));
  Remainder := Copy(Remainder,Fpos+1,Length(InString));
  try
  CompVal := StrToInt(Work);
  except
  exit;
  end;
  Fpos := POs('POS =',Remainder);
  If Fpos > 0 then
    begin
    Remainder := Copy(Remainder,FPos+5,Length(InString));
    FPOs := POs('ENCODER = ',Remainder);
    If Fpos > 0 then
      begin
      Work := Trim(Copy(Remainder,1,FPos-1));
      try
      Position := StrToFloat(Work);
      except
      Exit;
      end;
      Remainder := Trim(Copy(Remainder,FPOs+10,Length(InString)));
      try
      EncoderPosition := StrToInt(Remainder);
      Result := True;
      except
      exit;
      end;
      end
    end;
  end;
end;

{ TExistingBacklashData }

procedure TExistingBacklashData.Clean;
var
Lentry : TExistingCompEntry;
begin
 while(Count > 0) do
  begin
  Lentry := Items[0];
  Lentry.Free;
  Delete(0);
  end
end;

function TExistingBacklashData.ReadFromCompFileForAxis(AxName, CompFileName: String): Boolean;
begin

end;

function TExistingBacklashData.ReadCompDataFromStartIndex(StartLine: INteger; Lines: TStringList): Boolean;
var
CLine : String;
Lnum : Integer;
NewEntry : TExistingCompEntry;
FoundNeg : Boolean;
begin
Clean;
Result := false;
If Lines.Count > StartLine+NumEntries then
  begin
  FoundNeg := False;
  For Lnum := 0 to NumEntries-1 do
    begin
    NewEntry := TExistingCompEntry.Create;
    CLine := Lines[Startline+Lnum];
    if NewEntry.ReadFromString(CLine) then
      begin
      Add(NewEntry);
      if Not FoundNeg then
        begin
        If NewEntry.Position < 0 then
          begin
          FoundNeg := True;
          RollOverIndex := LNum-1;
          end;
        end;
      end;
    end
  end;
end;

function TExistingBacklashData.ReadHeaderFromStartIndex(SIndex: INteger; Lines: TStringList; var NextIndex: Integer): Boolean;
var
PIndex : Integer;
FPos : Integer;
Remainder : String;
StrVal : String;
n86Str : String;
Work : String;
begin
  Result := False;
  NextIndex := SIndex;
  If Lines.Count > SIndex Then
    begin
    // Get Home Offset
    PIndex := FindIndexFrom(SIndex,Lines,'Home Offset',Remainder);
    If PIndex > 0 then
      begin
      FPos := POs('AT',Remainder);
      Remainder := Trim(Copy(Remainder,Fpos+2));
      FPos := Pos(' ',Remainder);
      StrVal :=  Copy(Remainder,1,Fpos);
      try
      HomeOffset := StrToFloat(StrVal);
      except
      Exit;
      end;
      end;

    // Get Encoder Counts per Unit
    PIndex := FindIndexFrom(SIndex,Lines,'Encoder Counts per',Remainder);
    If PIndex > 0 then
      begin
      FPos := POs('=',Remainder);
      StrVal := Trim(Copy(Remainder,Fpos+1));
      try
      EncoderCountsPerUnit := StrToFloat(StrVal);
      except
      Exit;
      end;
      end;

    // Get Motor Num
    PIndex := FindIndexFrom(SIndex,Lines,'Uses Motor Number',Remainder);
    If PIndex > 0 then
      begin
      StrVal := Trim(NextToken(Remainder));
      try
      MotorNumber := StrToInt(StrVal);
      except
      Exit;
      end;
      end;

    // Get overall backlash
    n86Str := Format('I%d86 = ',[MotorNumber]);
    PIndex := FindIndexFrom(SIndex,Lines,n86Str,Remainder);
    If PIndex > 0 then
      begin
      Work := Remainder;
      FPos := POs(';',Remainder);
      Work := Trim(Copy(Work,1,Fpos-1));
      try
      OverallBackLash := StrToInt(Work);
      except
      Exit;
      end;
      end;
    // Get NumEntries and Span
    PIndex := FindIndexFrom(SIndex,Lines,'DEFINE BLCOMP',Remainder);
    If PIndex > 0 then
      begin
      StrVal := Trim(NextToken(Remainder));
      try
      NumEntries := StrToInt(StrVal);
      except
      Exit;
      end;
      StrVal := Trim(NextToken(Remainder));
      try
      Span := StrToFloat(StrVal);
      except
      Exit;
      end;
      end;
    Result := True;
    NextIndex := PIndex+1;
    end;
end;

function TExistingBacklashData.FindIndexFrom(StartLine: Integer; Lines: TStringList; Search: String; var RemLine: String): Integer;
var
SString : String;
CLine : String;
LNum : Integer;
SPos : Integer;
Found : Boolean;
Fail : Boolean;
begin
Result := -1;
Found := False;
Fail := False;
SString := UpperCase(Search);
LNum := StartLine;
while (Found = False) and (LNum < Lines.Count) and (Fail = False)do
  begin
  try
  CLine := UpperCase(Lines[LNum]);
  Spos := Pos(SString,CLine);
  If SPOs > 0 then
    begin
    Found := True;
    Result := LNum;
    RemLine := Trim(Copy(CLine,Spos+Length(SString),Length(Cline)));
    end;
  Spos := POs('COMPENSATION',Cline);
  if Spos > 0 then
    begin
    Fail := True;
    end;
  finally
  inc(LNum);
  end;
  end;

end;

destructor TExistingBacklashData.Destroy;
begin

  inherited;
end;

function TExistingBacklashData.CompareDataTo(CompareData : TExistingBacklashData;var CompResult : TCompFileCompareResults):Boolean;
var
FoundError : Boolean;
ENum : Integer;
CEntry,CompareEntry : TExistingCompEntry;
begin
Result := False;
CompResult.HomeOffset.AddFloatResult(CompareData.HomeOffset,HomeOffset);
CompResult.EncoderCounts.AddFloatResult(CompareData.EncoderCountsPerUnit,EncoderCountsPerUnit);
CompResult.NumEntries.AddIntegerResult(CompareData.NumEntries,NumEntries);
CompResult.Span.AddFloatResult(CompareData.Span,Span);
CompResult.MotorNumber.AddIntegerResult(CompareData.MotorNumber,MotorNumber);

{CompResult.HomeOffset.IsOK       := CompareData.HomeOffset = HomeOffset;
CompResult.EncoderCountsOk.IsOK    := CompareData.EncoderCountsPerUnit = EncoderCountsPerUnit;
CompResult.NumEntriesOk.IsOK       := CompareData.NumEntries = NumEntries;
CompResult.SpanOK.IsOK             := CompareData.Span = Span;
CompResult.MotorNumberOK.IsOK      := CompareData.MotorNumber = MotorNumber;}
if Count = CompareData.Count then
  begin
  FoundError := False;
  ENum := 0;
  while (Enum<Count) and (not FoundError) do
    begin
    try
    CEntry := Items[ENum];
    CompareEntry := CompareData.Items[Enum];
    if (Centry.Position <> CompareEntry.Position) or (Centry.EncoderPosition <> CompareEntry.EncoderPosition) then
      begin
      //Positions not the same therfore an error
      FoundError := True;
      end
    else
      begin
      if abs(Centry.CompVal - CompareEntry.CompVal) > 4 then
        begin
        //Only an error if not near limit of travel
        if abs(RolloverIndex-Enum) > 3 then
              FoundError := True;
        end
      end;
    finally
    inc(Enum)
    end;
    end;
  Result := not FoundError;
  if Result then
    begin
    CompResult.Data.AddStringResult('','',True);
    end
  else
    begin
    CompResult.Data.AddStringResult('','',False);
    end
  end;
end;

{ TExistingCompList }


procedure TExistingCompList.Clean;
var
CLEntry : TExistingAxisData;
begin
 while(Count > 0) do
  begin
  CLentry := Items[0];
  CLentry.Free;
  Delete(0);
  end
end;

function TExistingCompList.CompResultsForAxisCalled(AxName: String): TCompFileCompareResults;
var
REntry : TExistingAxisData;
begin
Result := Nil;
REntry := EntryForAxisCalled(AxName);
if Assigned(Rentry) then
  begin
  Result := REntry.CompareResult;
  end;
end;

function TExistingCompList.BLCompResultsForAxisCalled(AxName: String): TCompFileCompareResults;
var
REntry : TExistingAxisData;

begin
Result := Nil;
REntry := EntryForAxisCalled(AxName);
if Assigned(Rentry) then
  begin
  Result := REntry.BLCompareResult;
  end;
end;

function TExistingCompList.IsBlank(var CompCount : INteger): Boolean;
var
REntry : TExistingAxisData;
Enum : INteger;
begin
Result := True;
if count > 0 then
  begin
  CompCount := 0;
  For Enum := 0 to Count-1 do
    begin
    REntry := Items[Enum];
    if Rentry.CompExists then Inc(CompCount);
    end;
  Result := CompCount = 0;
  end;
end;

function TExistingCompList.EntryForAxisCalled(AxName: String): TExistingAxisData;
var
Found : Boolean;
ENum : Integer;
Entry : TExistingAxisData;
begin
Result := nil;
if Count > 0  then
  begin
  Found := false;
  Enum := 0;
  while (Enum < Count) and (not Found) do
    begin
    try
    Entry := Items[Enum];
    Found := Entry.AxisName = AxName;
    if Found then Result := Entry;
    finally
    inc(Enum)
    end;
    end;
  end;
end;


{ TExistingAxisData }

procedure TExistingAxisData.Clean;
begin
  CompData.Clean;
  BLCompData.Clean;
end;

function TExistingAxisData.CompareCompAndBLDataTo(CompEntry: TCompareAxisData):Boolean;
var
CompResult : Boolean;
begin
CompResult :=  CompData.CompareDataTo(CompEntry.CompData,CompareResult);
Result := BLCompData.CompareDataTo(CompEntry.BLCompData,BLCompareResult);
//CompEntry.CompareResult := CompareResult;
//CompEntry.BLCompareResult := BLCompareResult;
Result := Result and CompResult;
end;

constructor TExistingAxisData.Create;
begin
CompareResult := TCompFileCompareResults.Create;
BLCompareResult := TCompFileCompareResults.Create;
CompData := TExistingCompData.Create;
BLCompData := TExistingBacklashData.Create;
CompExists := False;
end;

destructor TExistingAxisData.Destroy;
begin
Clean;
CompData.Free;
BLCompData.Free;
CompareResult.Free;
BLCompareResult.Free;
inherited;
end;

{ TCompFileComapreResults }

function TCompFileCompareResults.AllOK: Boolean;
begin
try
Result := MotorNumber.IsOK and
          HomeOffset.IsOK and
          EncoderCounts.IsOK and
          NumEntries.IsOK and
          Span.IsOK and
          Data.IsOK;
except
Result := False;
end;
end;

constructor TCompFileCompareResults.Create;
begin
//  MakeOutput  := TCompareEntry.Create;
  MotorNumber := TCompareEntry.Create;
  HomeOffset  := TCompareEntry.Create;
  EncoderCounts := TCompareEntry.Create;
  NumEntries := TCompareEntry.Create;
  Span := TCompareEntry.Create;
  Data  := TCompareEntry.Create;
  Initialise;
end;

destructor TCompFileCompareResults.Destroy;
begin
//  MakeOutput.Free;
  MotorNumber.Free;
  HomeOffset.Free;
  EncoderCounts.Free;
  NumEntries.Free;
  Span.Free;
  Data.Free;
  inherited;
end;

procedure TCompFileCompareResults.Initialise;
begin
  MotorNumber.IsOK := False;
  HomeOffset.IsOK  := False;
  EncoderCounts.IsOK := False;
  NumEntries.IsOK := False;
  Span.IsOK := False;
  Data.IsOK := False;
end;

{ TCompareEntry }
procedure TCompareEntry.AddBooleanResult(Left, Right: Boolean);
begin
  if Left Then LeftValStr  := '1' else LeftValStr := '0';
  if Right then RightValStr:= '1' else LeftValStr := '0';
  IsOK := Left=right;
end;

procedure TCompareEntry.AddIntegerResult(Left, Right: Integer);
begin
  LeftValStr := IntToStr(Left);
  RightValStr:= IntToStr(Right);
  IsOK := Left=right;
end;

procedure TCompareEntry.AddFloatResult(Left, Right: Double);
begin
  LeftValStr := FloatToStr(Left);
  RightValStr:= FloatToStr(Right);
  IsOK := Left=right;
end;

procedure TCompareEntry.AddStringResult(Left, Right: String; Pass: Boolean);
begin
  LeftValStr := Left;
  RightValStr:= Right;
  IsOK := Pass;
end;

procedure TCompFileCompareResults.WriteAllStatusToStrings(OutList: TStrings);
begin
if assigned(OutList) then
  begin
  OutList.Add(MotorNumber.ResultAsString('Motor Number'));
  OutList.Add(HomeOffset.ResultAsString('Home Offset'));
  OutList.Add(EncoderCounts.ResultAsString('Encoder Counts'));
  OutList.Add(NumEntries.ResultAsString('Number of Entries'));
  OutList.Add(Span.ResultAsString('Span'));
  OutList.Add(Data.ResultAsString('Data Content'));
  end;
end;

procedure TCompFileCompareResults.WriteErrorsToStrings(OutList: TStrings);
begin
  if not MotorNumber.IsOk then OutList.Add(MotorNumber.ResultAsString('Motor Number'));
  if not HomeOffset.IsOk then OutList.Add(HomeOffset.ResultAsString('Home Offset'));
  if not EncoderCounts.IsOk then OutList.Add(EncoderCounts.ResultAsString('Encoder Counts'));
  if not NumEntries.IsOk then OutList.Add(NumEntries.ResultAsString('Number of Entries'));
  if not Span.IsOk then OutList.Add(Span.ResultAsString('Span'));
  if not Data.IsOk then OutList.Add(Data.ResultAsString('Data Content'));
end;

function TCompareEntry.ResultAsString(Prefix : String): String;
begin
If IsOk then
  begin
  Result := Format('%s  Pass: %s ,%s',[Prefix,LeftValStr,RightValStr])
  end
else
  begin
  Result := Format('%s  Fail: %s ,%s',[Prefix,LeftValStr,RightValStr])
  end
end;


constructor TCompareEntry.Create;
begin
  LeftValStr := '';
  RightValStr:= '';
  IsOK := True;
end;


{ TCompareAxisData }

constructor TCompareAxisData.Create;
begin
CompData := TExistingCompData.Create;
BLCompData := TExistingBacklashData.Create;
CompExists := False;
end;

procedure TCompareAxisData.Clean;
begin
 CompData.Clean;
 BLCompData.Clean;
end;

destructor TCompareAxisData.Destroy;
begin
  CompData.Clean;
  CompData.Free;
  BLCompData.Clean;
  BLCompData.Free;
  inherited;
end;

{ TCompareCompList }

procedure TCompareCompList.Clean;
var
CLEntry : TCompareAxisData;
begin
 while(Count > 0) do
  begin
  CLentry := Items[0];
  CLentry.Free;
  Delete(0);
  end
end;

function TCompareCompList.EntryForAxisCalled(AxName: String): TCompareAxisData;
var
Found : Boolean;
ENum : Integer;
Entry : TCompareAxisData;
begin
Result := nil;
if Count > 0  then
  begin
  Found := false;
  Enum := 0;
  while (Enum < Count) and (not Found) do
    begin
    try
    Entry := Items[Enum];
    Found := Entry.AxisName = AxName;
    if Found then Result := Entry;
    finally
    inc(Enum)
    end;
    end;
  end;
end;


function TCompareCompList.IsBlank(var CompCount: INteger): Boolean;
var
REntry : TCompareAxisData;
Enum : INteger;
begin
Result := True;
if count > 0 then
  begin
  CompCount := 0;
  For Enum := 0 to Count-1 do
    begin
    REntry := Items[Enum];
    if Rentry.CompExists then Inc(CompCount);
    end;
  Result := CompCount = 0;
  end;
end;


end.
