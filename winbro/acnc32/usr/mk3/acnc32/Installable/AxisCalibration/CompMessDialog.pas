unit CompMessDialog;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type
  TCompMessOkEvent = procedure(Sender : Tobject; ReturnTag : Integer) of object;

  TCalMsg = class(TForm)
    OKButton: TButton;
    MessLabel: TLabel;

    procedure OKButtonClick(Sender: TObject);
  private
    FReturnTag : INteger;
    FOnOkPressed: TCompMessOkEvent;
//    procedure SetOnOkPressed(const Value: TCompMessOkEvent);
    { Private declarations }
  public
    { Public declarations }
    procedure ShowWithMessage(Mess : String;ReturnTag : Integer);
    property OnOkPressed :TCompMessOkEvent read FOnOkPressed write FOnOkPressed;
  end;

var
  CalMsg: TCalMsg;

implementation

{$R *.dfm}

{ TForm2 }



procedure TCalMsg.ShowWithMessage(Mess: String;ReturnTag : Integer);
var
MessWidth : Integer;
begin
FReturnTag := ReturnTag;
MessLabel.Caption := Mess;
MessWidth := Canvas.TextWidth(Mess);
ClientWidth := MessWidth+40;
OKButton.Left := (ClientWidth div 2)-(OKButton.width div 2);
Top := 300;
Left := (1024-Width) div 2;
Visible := True;
end;

procedure TCalMsg.OKButtonClick(Sender: TObject);
begin
  If assigned(FOnOkPressed) then FOnOkPressed(Self,FReturnTag);
  Visible := False;
end;

end.
