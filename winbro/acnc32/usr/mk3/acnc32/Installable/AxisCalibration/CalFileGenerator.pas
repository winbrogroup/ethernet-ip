unit CalFileGenerator;

interface
uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls,IniFiles,Grids,CalDataLists;

type

TCalTextGenerator = class(TStringList)
 private
 public
   procedure ClearAndMakeHeader(AxisList : TCalAxisList;
                                MachineType : String;
                                MachineId : String;
                                Customer : String;
                                DateStr : String;
                                TimeStr : String);
   procedure MakeCompText(AxisList: TCalAxisList);
   function MakeBacklashText(AxisList: TCalAxisList): Boolean;
 end;

implementation

{ TCalTextGenerator }

procedure TCalTextGenerator.ClearAndMakeHeader(AxisList : TCalAxisList;
                                MachineType : String;
                                MachineId : String;
                                Customer  : String;
                                DateStr   : String;
                                TimeStr   : String);
var
AxNum : Integer;
begin
Clear;
Add(';Winbro Integrated Axis Compenstation Generator');
Add(Format(';Machine Type   : %s',[MachineType]));
Add(Format(';Machine Id     : %s',[MachineId]));
Add(Format(';Customer       : %s',[Customer]));
Add(';');
Add(Format(';Date       : %s',[DateStr]));
Add(Format(';Time       : %s',[TimeStr]));
Add('DELETE GATHER');
Add('DELETE ROTARY');
For AxNum := 1 to AxisList.Count do
 begin
 Add(Format('#%d DELETE BLCOMP',[AxNum]));
 end;

For AxNum := 1 to AxisList.Count do
 begin
 Add(Format('#%d DELETE COMP',[AxNum]));
 Add(Format('I%d86 = 0          ; clear overall backlash',[AxNum]));
 end;
end;


function TCalTextGenerator.MakeBacklashText(AxisList: TCalAxisList): Boolean;
var
AxNum : Integer;
AxEntry : TSingleAxisData;
AxLetter : String;
CompEntry : TOutputDataRecord;
OutList : TOutputDataList;
BackLashOffset : Double;
ManBackLashInt : Integer;
Enum : Integer;
CMultiplier : Double;
MadeBL : Boolean;
begin
Result := False;
MadeBl := False;
For AxNum := AxisList.Count-1 downto 0 do
  begin
  AxEntry := AxisList[Axnum];
  if AxEntry.MakeOutput then
    begin
    MadeBL := False;
    CMultiplier := 16*AXentry.CompensationDataLists.aaEncoderCountsPerUnit/1000;
    if AxEntry.MakeOutput then
      begin
      if AXentry.CompensationDataLists.BacklashUsed then
        begin
        MadeBL := True;
        Result := True;
        OutList := AxEntry.CompensationDataLists.OutputDataList;
        BackLashOffset := OutList.BacklashAtEncoderZero;
        if OutList.Count > 1 then
          begin
          AxLetter := AxEntry.AxisName;
          Add(Format(';%s Axis Backlash Compensation',[AxLetter]));
          Add(';Encoder Direction is not Reversed');
          Add(Format(';Home Offset at %5.3f mm.',[AxEntry.CompensationDataLists.CompHomeOffset]));
          Add(Format(';Encoder Counts per mm. = %d',[Round(AxEntry.CompensationDataLists.aaEncoderCountsPerUnit)]));
          Add(Format(';%d Entries over %d encoder Counts',[OutList.Count,OutList.EncoderCountSpan]));
          Add(Format(';Uses Motor Number %d',[AxEntry.AxisMotorNumber]));
          Add(Format('#%d',[AxEntry.AxisMotorNumber]));
          Add(Format('I%d86 = %d              ; set overall backlash',[AxEntry.CompensationDataLists.AxisMotorNumber,Round(OutList.BacklashAtEncoderZero*-CMultiplier)]));
          Add(Format('DEFINE BLCOMP %D,%D',[OutList.Count,OutList.EncoderCountSpan]));
          For Enum := 1 to AxEntry.CompensationDataLists.OutputDataList.Count-1 do
            begin
            CompEntry := OutList[Enum];
            Add(CompEntry.AsBackLashText(BackLashOffset,CMultiplier));
            end;
          CompEntry := OutList[0];
          Add(CompEntry.AsBackLashText(BackLashOffset,CMultiplier));
          Add(Format('I%d85 = 32',[AxEntry.CompensationDataLists.AxisMotorNumber]));
          Add(';');
          Add(';');
          Add(';');
          end
        end;
       end;
       if (not MadeBL) and (Not AxEntry.CompensationDataLists.BacklashUsed) then
         begin
         // Here to use the Manually entered Backlash
         ManBacklashInt := Round(AxEntry.CompensationDataLists.ManualBacklashMicron*CMultiplier);
         Add(Format('I%d86 = %d  ; set manually entered backlash for %s axis',[AxEntry.CompensationDataLists.AxisMotorNumber,ManBacklashInt,AxEntry.AxisName]));
         Add(Format('I%d85 = 32',[AxEntry.CompensationDataLists.AxisMotorNumber]));
         Add(';');
         Add(';');
         Add(';');
         end;
       end;
     end;

  If Result then Add('I51 = 1');

end;

procedure TCalTextGenerator.MakeCompText(AxisList : TCalAxisList);
var
AxNum : Integer;
AxEntry : TSingleAxisData;
AxLetter : String;
CompEntry : TOutputDataRecord;
OutList : TOutputDataList;
Enum : Integer;
CMultiplier : Double;
begin


For AxNum := AxisList.Count-1 downto 0 do
  begin
  AxEntry := AxisList[Axnum];
  if AxEntry.MakeOutput then
    begin
    OutList := AxEntry.CompensationDataLists.OutputDataList;
    CMultiplier := 16*AXentry.CompensationDataLists.aaEncoderCountsPerUnit/1000;
    if OutList.Count > 1 then
      begin
      AxLetter := AxEntry.AxisName;
      Add(Format(';%s Axis Compensation',[AxLetter]));
      Add(';Encoder Direction is not Reversed');
      Add(Format(';Home Offset at %5.3f mm.',[AxEntry.CompensationDataLists.CompHomeOffset]));
      Add(Format(';Encoder Counts per mm. = %d',[Round(AxEntry.CompensationDataLists.aaEncoderCountsPerUnit)]));
      Add(Format(';%d Entries over %d encoder Counts',[OutList.Count,OutList.EncoderCountSpan]));
      Add(Format(';Uses Motor Number %d',[AxEntry.AxisMotorNumber]));
      Add(Format('#%d',[AxEntry.AxisMotorNumber]));
      Add(Format('DEFINE COMP %D,%D',[OutList.Count,OutList.EncoderCountSpan]));
      For Enum := 1 to AxEntry.CompensationDataLists.OutputDataList.Count-1 do
        begin
        CompEntry := OutList[Enum];
        Add(CompEntry.AsCompText(CMultiplier));
        end;
      CompEntry := OutList[0];
      Add(CompEntry.AsCompText(CMultiplier));
      Add(';');
      Add(';');
      Add(';');
      end
    end;
  end;
end;

end.
