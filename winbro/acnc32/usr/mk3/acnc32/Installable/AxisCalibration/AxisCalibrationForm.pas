unit AxisCalibrationForm;
(*
Power Up States.
If No edit.txt file then is new/upgrade system First PowerUp
If File Exists then File contains current sate of play and Machine ID for verification.
These are:
*)

{$DEFINE HIGHDEBUG}
{$DEFINE GRID_MODE_InVALID}
interface
uses CoreCNC, CNCTypes, CNC32, Classes, Windows, Graphics, SysUtils, NCNames,TelnetIndicator,TelnetIndicatorPanel,
  CNCAbs, Named, Controls,ExtCtrls,ActiveX,CognexTelnetClient,StdCtrls,Grids,Buttons,TelnetDisplay,CognexActions,VisionErrorStack,
  AbstractScript,IScriptInterface,INamedInterface,IniFiles,Forms,CogNexCommunicator,TextInputCombo,CognexTYpes,VisionCommand,
  FTPPanel,FaultRegistration,CalGraphForm,CalDataLists,ImportHandler,CalDataStatusGrid,CalFileGenerator,CompFileParser,
  CvsInSightDisplayOcx_TLB,CognexInSight_TLB,CognexInSightDisplay_TLB,Dialogs,CalArchive,RenishawImport;

const
BWidth = 100;
EditLeft = 160;
EditSpacing = 32;
EditTop = 40;
EBWidth = 90;
type

TCalDisplayModes=(tcdmNone,tcdmBlankNoMacId,tcdmNonBlankFirstStart,tcdmStatus,tcdmNonBlankNoMacId,tcdmSetup,tcdmDataIn,tcdmConditioned,tcdmGraph,tcdmGenerate,tcdmHistory);

TEditorChangeEvent = procedure(Sender : Tobject) of Object;

TCalEditBox = class(TEdit)
  private
    EntryText : String;
    ExitText  : String;
    FHasChanged: TEditorChangeEvent;
    FValAsDouble: Double;
    FValAsInteger: Integer;
    procedure EditEntered(Sender : TObject);
    procedure EditExited(Sender : TObject);
    procedure EditKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure SetValAsDouble(const Value: Double);
    procedure SetValAsInteger(const Value: Integer);
    function GetValAsDouble: Double;
    function GetValAsInteger: Integer;

  public
    IdTag : Integer;
    TextOnEntry : String;
    TextOnExit  : String;
    DoubleValid : Boolean;
    IntegerValid: Boolean;
    property ValAsDouble : Double read GetValAsDouble write SetValAsDouble;
    property ValAsInteger: Integer read GetValAsInteger write SetValAsInteger;
    constructor Create(AOwner: TComponent); override;
    property HasChanged : TEditorChangeEvent read FHasChanged write FHasChanged;
end;

TAxisCalDisplay = class(TExpandDisplay)
  private
   DefaultFileCreated : Boolean;
   IsClosing : Boolean;
   CalGraph : TCalibrationGraphForm;
   CurrentDisplayMode : TCalDisplayModes;
   CalStatusGrid : TCalDataStatusGrid;
   CurrentAxisData : TSingleAxisData;
   FirstShow : Boolean;
   CompDataBasePath : String;
   RenishawImportPath : String;
   CompDataFilePath : String;
   CurrentCompFile  : String;
   CompImportPath   : String;
   CompDataOutPath  : String;
   //LivePath : String;
   ContractPath     : String;
   ConfigFilePath   : String;

   SecurityLevel : Integer;

   CalDataOk : TAbstractTag;
   NewCalibrationTag : TAbstractTag;
   CalRegisteredTag  : TAbstractTag;
   CheckFileValidity : Boolean;

   A1DataMisMatch : TAbstractTag;
   A2DataMisMatch : TAbstractTag;
   A3DataMisMatch : TAbstractTag;
   A4DataMisMatch : TAbstractTag;
   A5DataMisMatch : TAbstractTag;
   A6DataMisMatch : TAbstractTag;


   MachineIdTag : TAbstractTag;
   FTableInitRequired : Boolean;
   Archiver : TCalibrationArchiver;
   RenishawImporter : TRenishawImporter;

// CalibrationPageOPen : TAbstractTag;
     procedure MyCellDataChanged(ACol: Integer;ARow : Integer;StrVal :String;FloatVal : Double);
     procedure GridDataMatches(Sender : TObject);
     procedure ShowConditionedData(Sender: TOBject);
     procedure UpdateForImportData(Sender : TObject);
     procedure DoSatusReport(Sender : TObject);
     procedure DoImportClick(Sender : TObject);
     procedure DoClearCompClick(Sender : TObject);

     procedure LoadImportData;
     procedure CreateDefaultWorkingFile(MacId : String);
     procedure DoSubmit(Sender : TObject);
     procedure DoRevert(Sender : TObject);
     procedure ShowStatus(Sender: TOBject);
     procedure ShowDataIn(Sender: TOBject);
     procedure ShowGenerate(Sender: TOBject);
     procedure ShowHistory(Sender: TOBject);

     procedure AxisSelected(Sender: TOBject);
     procedure InitialiseDisplayForAxis(InAxisData : TSingleAxisData;GoToPage : Boolean;BufferContents : Boolean);
     procedure InputCheckBoxClicked(Sender : TObject);
     procedure InitInputTable(Sender : TObject);
     procedure TopofTable(Sender : TObject);
     procedure BottomOfTable(Sender : TObject);
     procedure MakeOutputFile(Sender : TObject);
     procedure TableAdjustModeRequest(Sender : TObject);
     procedure DoAdjustRequest(Sender : TObject);
     procedure DoMergeandSaveRequest(Sender : TObject);
     procedure AbandonAdjustMode(Sender : TObject);
     procedure DoWriteRequest(Sender : TObject);
     procedure RequestFirstReg(Sender : TObject);
     procedure RequestFullArchive(Sender : TObject);
     procedure RequestFileLoad(Sender : TObject);

     procedure RequestAdjustFileLoad(Sender : TObject);
     procedure TableInitRequired(State : Boolean);
     procedure CalEditChange(Sender : TObject);
     procedure MakeComponents;
     procedure SetDisplayMode(DMode : TCalDisplayModes);
     function  InternalCompare : Boolean;
     procedure UpdateMessage;
     procedure UpdateRestartMessage;
     function  MakeDateString: String;
     function  DisplayableDateTimeString: String;
     procedure ShowEditMemo(EditMess1 : String);
     procedure ConditionSubmitButton;
     procedure SetDisplayModeSpacing(LinearIsForced: Boolean);
     procedure WriteConfigStringParam(Param : String;Value : String);
     procedure WriteConfigIntegerParam(Param : String; Value: Integer);
     function  ReadConfigString(Param : String):String;
     function  ReadConfigInteger(Param : String):Integer;
     procedure StoreDisplayStatusToSnapshot(Snap :TSingleAxisSnapshot);
     procedure MachineIDChange(ATag: TAbstractTag);
     function  WorkingFileIdMatch(IDStr : String): Boolean;
     procedure GetZeroOffsets(ZOString: String);
    function TestCREntriesValid: Integer;
   protected
    OutDataVisible : Boolean;
    DataUpdating : Boolean;
    ForceLinearOption : Boolean;

    DataInputPanel : TPanel;
    AxisDisplayBox : TPanel;
    TitlePanel     : TPanel;
    SetupPanel     : TPanel;
    SetupSubPanel  : TPanel;
    InDataSubPanel : TPanel;
    HistoryPanel   : TPanel;

    AxisStatusPanel: TPanel;
    AxisStatusMess  : TLabel;
    AxisEdited      : TLabel;
    RegStatusPanel  : TPanel;

    CondInputDataSubPanel : TPanel;
    CondOutputDataSubPanel: TPanel;
    CondInputTitlePanel   : TPanel;
    CondInputTitlePanel2  : TPanel;
    CondOutputTitlePanel  : TPanel;

    UnshiftedTitlePanel : TPanel;
    ShiftedTitlePanel : TPanel;
    ImportPanel : TPanel;
    ConditionDataPanel : TPanel;
    GraphPanel:TPanel;
    GeneratePanel:TPanel;
    SetupTitlePanel : TPanel;
    InputTitlePanel : TPanel;

    ReportStatusButton      : TButton;
    DoImportButtonButton    : TButton;
    DoClearAndRestartButton : TButton;
    InitInputTableButton    : TButton;
    AdjustModeButton        : TButton;
    MergeAdjustButton       : TButton;
    TableTopButton          : TButton;
    TableEndButton          : TButton;
    CompareButton           : TButton;
    WriteCompFileButton     : TButton;
    FirstFullRegButton      : TButton;
    ArchiveButton           : TButton;
    LoadFileButton          : TButton;
    LoadAdjustFileButton    : TButton;


    StatusMessage : TMemo;
    MessageMemo   : TMemo;
    MessageList   : TStringList;
//    EditMemo      : TMemo;
    AxisSelect       : TComboBox;
    ConDataButton    : TSpeedButton;
    StatusButton     : TSpeedButton;
    DataInputButton  : TSpeedButton;
    GenerateButton   : TSpeedButton;
    HistoryButton    : TSpeedButton;
    OutGenButton     : TButton;

    ZeroOffsets      : TAbstractTag;
    XCompDataMissing : Boolean;
    YCompDataMissing : Boolean;
    ZCompDataMissing : Boolean;
    ACompDataMissing : Boolean;
    BCompDataMissing : Boolean;
    ConInDataGrid    : TConditionedDataInDataGrid;
    COnOutDataGrid   : TConditionedDataOutDataGrid;

    // OptionsPage Components
    MCIDLabel        : TLabel;
    MCIDDisplay      : TEdit;
    //OptionMessMemo   : TMemo;




    //SetupPage components
    DCLabel          : TLabel;
    BLLabel          : TLabel;
    MBLLabel         : TLabel;
    MBAdJustLabel    : TLabel;

    ICRLabel          : TLabel;
    CPULabel         : TLabel;
    HOLabel          : TLabel;
    DoubleCountCheckBox : TCheckBox;
    MakeOutputCheckBox  : TCheckBox;
    CRCheckBox       : TCheckBox;
    BacklashCheckBox : TCheckBox;

    ManBackEdit      : TCalEditBox;
    ManAdjustDisplay : TEdit;
    CompIncrEdit     : TCalEditBox;
    NumEntriesEdit   : TCalEditBox;
    CPMEdit          : TCalEditBox;
    HomeOffsetEdit   : TCalEditBox;
//    NegLimitEdit     : TEdit;
    FirstCompEdit    : TCalEditBox;
    //PosLimitEdit     : TEdit;

//    PosLimitLabel    : TLabel;
//    NegLimitLabel    : TLabel;
    CalLabel         : TLabel;
    RDLabel          : TLabel;
    RTLabel          : TLabel;
    SubmitButton     : TButton;
    RevertButton     : TButton;

    FirstrCompLabel  : TLabel;
    CompIncrLabel    : TLabel;
    CompEntriesLabel : TLabel;
    LastCompLabel    : TLabel;
    LastCompValue    : TLabel;

    ManDataGrid      : TInputDataGrid;
    CompParser       : TCompFileParser;
    DefaultSettings  : TDefaultSettingsList;
    Activating       : Boolean;

    OutputMemo       : TMemo;
    CustomerId       : String;
    MachineType      : String;
    RenOpenDialogue  : TOpenDialog;
    InitialiseComp      : Boolean;
//    CompFullyRegistered : Boolean;
    RegisteredArchiveFolder : String;
    RegDisplayDate          : String;
//    ImportStage             : Integer;


    procedure Resize; override;
    procedure AxisInDataChanged(DataIndex: Integer;ZShift : Double; Valid : Boolean);
    procedure AxisOutDataChanged(DataIndex: Integer;Valid : Boolean);
    procedure DoLazyUpdate(Sender : TObject);
    function UpdateStatusFromAxisList(ReportErrorToLog : Boolean):Boolean;
    procedure CheckCompFileStatus(AxisList : TCalAxisList);
    procedure GetCompStatusFrom(ConfigPath : String;WorkingFileExists : Boolean);
    procedure AddMessage(Mess: String;ClearFirst : Boolean);
    procedure UpdateMessageMemo;
    procedure UpdateOptionsPage;
    procedure AddOPtionsMessage(Mess : String; ClearFirst : Boolean);

  public
    AxisCount        : Integer;
    AxisList         : TCalAxisList;
    NCConfigList     : TNCConfigList;
    MachineIdStr : String;
    function NextToken(var InStr: String): String;
    function SlashSep(const Path, S: String): String;
    function CurrentTimeAsFileSafeString: String;
    function CurrentDateAsFileSafeString: String;
    function StdTimeString: String;
    function StdDateString: String;
    constructor Create(aOwner : TComponent); override;
    procedure   InstallComponent(Params : TParamStrings); override;
    procedure   Installed; override;
    procedure   IShutdown; override;
    procedure   Activate(aTag : TAbstractTag); override;
 end;


var AxisCalibrationUtility : TAxisCalDisplay;

implementation

function DDigit(Num : INteger): String;
  begin
  Result := '00';
  if Num > 9 then
    Result := IntToStr(Num)
  else
    Result := Format('0%d',[Num]);
  end;


function TAxisCalDisplay.SlashSep(const Path, S: String): String;
begin
  if AnsiLastChar(Path)^ <> '\' then
    Result := Path + '\' + S
  else
    Result := Path + S;
end;

function TAxisCalDisplay.NextToken(var InStr : String):String;
var
CommaPos : Integer;
begin
Result := '';
CommaPos := Pos(',',InStr);
if CommaPos > 0 then
  begin
  Result := Trim(Copy(InStr,1,CommaPos-1));

  InStr := Copy(Instr,CommaPos+1,1000);
  end
else
  begin
  Result := Instr;
  Instr := '';
  end;
end;


constructor TAxisCalDisplay.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
  CheckFileValidity := False;
  DefaultFileCreated := False;
  AxisList := TCalAxisList.Create;
  AxisList.OnInputDataUpdated := AxisInDataChanged;
  AxisList.OnOutputDataUpdated := AxisOutDataChanged;
  NCConfigList     := TNCConfigList.Create;
  MakeComponents;
  CurrentDisplayMode := tcdmNone;
  FirstShow := True;
  AxisCalibrationUtility := Self;
end;


Procedure TAxisCalDisplay.MakeComponents;
begin
 Self.Name := 'CalDisplay';
 DataUpdating := False;
 TitlePanel := TPanel.Create(Self);
 TitlePanel.Parent := Self;
 with TitlePanel do
  begin
  Color := clWebLinen;
  Align := alTop;
  BevelInner := bvLowered;
  Height := 30;
  Alignment := taLeftJustify;
  Caption := '';
  end;

 ImportPanel := TPanel.Create(Self);
 ImportPanel.Parent := Self;
 with ImportPanel do
    begin
    Color := clWebGhostWhite;
    BevelInner := bvLowered;
    end;

  CalStatusGrid := TCalDataStatusGrid.Create(nil);
  CalStatusGrid.Parent := ImportPanel;

  with CalStatusGrid do
    begin
    Top := 80;
    Left := 20;
    end;

 ConditionDataPanel := TPanel.Create(Self);
 ConditionDataPanel.Parent := Self;
 with ConditionDataPanel do
    begin
    Color := clWebSlateGray;
    BevelInner := bvLowered;
    end;

   CondInputDataSubPanel := TPanel.Create(nil);
   CondInputDataSubPanel.Parent := ConditionDataPanel;
   with CondInputDataSubPanel do
      begin
      Width := 380;
      align := alLeft;
      Color := clWebIvory;
      end;

   CondInputTitlePanel:= TPanel.Create(nil);
   CondInputTitlePanel.Parent := CondInputDataSubPanel;
   with  CondInputTitlePanel do
    begin
    align := alTop;
    Height := 25;
    Caption := 'Conditioned Input Data';
    BevelInner := bvLowered;
    Alignment := taCenter;
    end;

   CondInputTitlePanel2:= TPanel.Create(nil);
   CondInputTitlePanel2.Parent := CondInputDataSubPanel;
   with  CondInputTitlePanel2 do
    begin
    align := alTop;
    Height := 25;
    BevelInner := bvNone;
    BevelOuter := bvNone;
    end;

   UnshiftedTitlePanel:= TPanel.Create(nil);
   UnshiftedTitlePanel.Parent := CondInputTitlePanel2;
   with  UnshiftedTitlePanel do
    begin
    align := alNone;
    Top :=0;
    Left := 72;
    Width :=142;
    Height := 25;
    BevelInner := bvLowered;
    Caption := 'As Entered';
    Alignment := taCenter;
    end;

   ShiftedTitlePanel := TPanel.Create(nil);
   ShiftedTitlePanel.Parent := CondInputTitlePanel2;
   with  ShiftedTitlePanel do
    begin
    align := alNone;
    Top :=0;
    Left := 72+142;
    Width :=142;
    Height := 25;
    BevelInner := bvLowered;
    Caption := 'Zero Corrected';
    Alignment := taCenter;
    end;

   CondOutputDataSubPanel := TPanel.Create(nil);
   CondOutputDataSubPanel.Parent := ConditionDataPanel;
   with CondOutputDataSubPanel do
      begin
      align := alRight;
      Color := clWebMintcream;
      Width := 480;
      end;

   CondOutputTitlePanel := TPanel.Create(nil);
   CondOutputTitlePanel.Parent := CondOutputDataSubPanel;
   with  CondOutputTitlePanel do
    begin
    align := alTop;
    Height := 25;
    Caption := 'Conditioned Output Data';
    Alignment := taCenter;
    end;


  ConInDataGrid := TConditionedDataInDataGrid.create(Nil);
  ConInDataGrid.Parent := CondInputDataSubPanel;
  with ConInDataGrid do
    begin
    align := alClient;
    Visible := True;
    end;

  COnOutDataGrid := TConditionedDataOutDataGrid.create(Nil);
  ConOutDataGrid.Parent := CondOutputDataSubPanel;
  with ConOutDataGrid do
    begin
    align := alClient;
    Visible := True;
    end;

 SetupPanel := TPanel.Create(Self);
 SetupPanel.Parent := Self;
 with SetupPanel do
    begin
    Color := clWebHoneydew;
    BevelInner := bvLowered;
    end;

{ OptionsPanel   := TPanel.Create(Self);
 OptionsPanel.Parent := Self;
 with OptionsPanel do
    begin
    Color := clWebHoneydew;
    BevelInner := bvLowered;
    end;}

 MCIDLabel := TLabel.Create(nil);
 MCIDLabel.Parent := ImportPanel;
 with MCIDLabel do
  begin
  Caption := 'Machine ID';
  Left := 10;
  Top := 45;
  end;

 MCIDDisplay := TEdit.Create(nil);
 MCIDDisplay.Parent := ImportPanel;
 with MCIDDisplay do
  begin
  Top := 40;
  Left := 100;
  ReadOnly := True;
  Text := '0000';
  end;

{ OptionMessMemo := TMemo.Create(nil);
 OptionMessMemo.Parent := OptionsPanel;
 with OptionMessMemo do
  begin
  Align := alBottom;
  Height := 120;
  end;}



 HistoryPanel := TPanel.Create(Self);
 HistoryPanel.Parent := Self;
 with HistoryPanel do
    begin
    Color := clWebPapayaWhip;
    BevelInner := bvLowered;
    end;

  AxisStatusPanel := TPanel.Create(nil);
  AxisStatusPanel.Parent := SetupPanel;
  with AxisStatusPanel do
      begin
      Color := clWebGhostWhite;
      BevelInner := bvLowered;
      Align := alTop;
      Height := 25;
      end;

  AxisStatusMess := TLabel.create(nil);
  AxisStatusMess.Parent := AxisStatusPanel;
  with AxisStatusMess do
      begin
      Top := 4;
      Left := 2;
      Font.Color := clBlack;
      end;

  AxisEdited := TLabel.create(nil);
  AxisEdited.Parent := AxisStatusPanel;
  with AxisEdited do
      begin
      Caption := 'Edited';
      Font.Color := clRed;
      Font.Size := 11;
      Font.Style := [fsbold];
      Top := 4;
      Left :=600;
      end;

  RegStatusPanel := TPanel.Create(nil);
  RegStatusPanel.Parent := ImportPanel;
  with RegStatusPanel do
      begin
      Color := clWebGhostWhite;
      BevelInner := bvLowered;
      Align := alTop;
      Height := 25;
      end;



  SetupSubPanel := TPanel.Create(nil);
  SetupSubPanel.Parent := SetupPanel;
  with SetupSubPanel do
      begin
      Color := clWebHoneydew;
      BevelInner := bvLowered;
      Align := alLeft;
      Width := 260;
      end;

  SetupTitlePanel := TPanel.Create(Self);
  SetupTitlePanel.Parent := SetupSubPanel;
  with SetupTitlePanel do
    begin
    Color := clWebIvory;
    BevelInner := bvLowered;
    Caption := 'Setup Data';
    Align := alTop;
    Height := 25;
    Alignment := taCenter;
    end;



  AxisDisplayBox := TPanel.Create(nil);
  AxisDisplayBox.Parent := SetupSubPanel;
  with AxisDisplayBox do
    begin
    Caption := '';
    Font.Size := 14;
    Width := 25;
    Height := 25;
    BevelInner := bvLowered;
    Color := clWebCornSilk;
    Top := 40;
    Left := 10;
    end;


    DCLabel := TLabel.Create(Nil);
    DCLabel.Parent := SetupSubPanel;
    with DCLabel do
      begin
      Caption := 'Use Double Points';
      Left := 10;
      Top := EditTop;
      end;

    DoubleCountCheckBox := TCheckBox.Create(nil);
    DoubleCountCheckBox.Parent := SetupSubPanel;
    with DoubleCountCheckBox do
      begin
      Left := EditLeft;
      Top := EditTop;
      OnClick := InputCheckBoxClicked;
      end;

    ICRLabel := TLabel.Create(Nil);
    ICRLabel.Parent := SetupSubPanel;
    with ICRLabel do
      begin
      Caption := 'Continuous Rotary';
      Left := 10;
      Top := EditTop+EditSpacing;
      end;

    CRCheckBox := TCheckBox.Create(nil);
    CRCheckBox.Parent := SetupSubPanel;
    with CRCheckBox do
      begin
      Left := EditLeft;
      Top := EditTop+EditSpacing;
      OnClick := InputCheckBoxClicked;
      end;

    CalLabel  := TLabel.Create(nil);
    CalLabel.Parent := SetupSubPanel;
    with CalLabel do
      begin
      Caption := 'Include In Output';
      Left := 10;
      Top := CRCheckBox.Top+EditSpacing;
      end;

    MakeOutputCheckBox := TCheckBox.Create(nil);
    MakeOutputCheckBox.Parent := SetupSubPanel;
    with MakeOutputCheckBox do
      begin
      Left := EditLeft;
      Top := CRCheckBox.Top+EditSpacing;
      OnClick := InputCheckBoxClicked;
      end;


    CPULabel := TLabel.Create(Nil);
    CPULabel.Parent := SetupSubPanel;
    with CPULabel do
      begin
      Caption := 'Counts Per Unit';
      Left := 10;
      Top := MakeOutputCheckBox.Top+EditSpacing;
      end;

    CPMEdit := TCalEditBox.Create(nil);
    CPMEdit.Parent := SetupSubPanel;
    with CPMEdit do
      begin
      IdTag := 0;
      Left := EditLeft;
      Top := MakeOutputCheckBox.Top+EditSpacing;
      HasChanged := CalEditChange;
      end;


    HOLabel := TLabel.Create(nil);
    HOLabel.Parent := SetupSubPanel;
    with HOLabel do
      begin
      Caption := 'Home Offset (mm)';
      Left := 10;
      Top := CPMEdit.Top+EditSpacing;
      end;

    HomeOffsetEdit:= TCalEditBox.Create(nil);
    HomeOffsetEdit.Parent := SetupSubPanel;
    with HomeOffsetEdit do
      begin
      IdTag := 1;
      Left := EditLeft;
      Top := CPMEdit.Top+EditSpacing;
      HasChanged := CalEditChange;
      end;

{    PosLimitLabel  := TLabel.Create(nil);
    PosLimitLabel.Parent := SetupSubPanel;
    with PosLimitLabel do
      begin
      Caption := 'Pos Limit (mm)';
      Left := 10;
      Top := HomeOffsetEdit.Top+EditSpacing;
      end;

    PosLimitEdit  := TEdit.Create(nil);
    PosLimitEdit.Parent := SetupSubPanel;
    with PosLimitEdit do
      begin
      ReadOnly := True;
      Left := EditLeft;
      Top := HomeOffsetEdit.Top+EditSpacing;
      Width := 95;
      end;

    NegLimitLabel  := TLabel.Create(nil);
    NegLimitLabel.Parent := SetupSubPanel;
    with NegLimitLabel do
      begin
      Caption := 'Neg Limit (mm)';
      Left := 10;
      Top := PosLimitEdit.Top+EditSpacing;
      end;

    NegLimitEdit  := TEdit.Create(nil);
    NegLimitEdit.Parent := SetupSubPanel;
    with NegLimitEdit do
      begin
      ReadOnly := True;
      Left := EditLeft;
      Top := PosLimitEdit.Top+EditSpacing;
      Width := 95;
      end;}


   BLLabel := TLabel.create(Nil);
   BLLabel.Parent := SetupSubPanel;
    with BLLabel do
      begin
      Caption := 'Use Backlash table';
      Left := 10;
//      Top := NegLimitEdit.Top+EditSpacing;
      end;

    BacklashCheckBox := TCheckBox.Create(nil);
    BacklashCheckBox.Parent := SetupSubPanel;
    with BacklashCheckBox do
      begin
      Left := EditLeft;
//      Top := NegLimitEdit.Top+EditSpacing;
      OnClick := InputCheckBoxClicked;
      end;

    MBLLabel := TLabel.create(Nil);
    MBLLabel.Parent := SetupSubPanel;
    with MBLLabel do
      begin
      Caption := 'Backlash (micron)';
      Left := 10;
      Top := BacklashCheckBox.Top+EditSpacing;
      end;

    ManBackEdit   := TCalEditBox.Create(nil);
    ManBackEdit.Parent := SetupSubPanel;
    with ManBackEdit do
      begin
      IdTag := 7;
      Left := EditLeft+10;
      Width := 85;
      Top := BacklashCheckBox.Top+EditSpacing;
      HasChanged := CalEditChange;
      end;

    MBAdJustLabel := TLabel.create(Nil);
    MBAdJustLabel.Parent := SetupSubPanel;
    with MBAdJustLabel do
      begin
      Caption := 'Backlash Adjust';
      Left := 10;
      Top := ManBackEdit.Top+EditSpacing;
      Visible := False;
      end;

    ManAdjustDisplay  := TEdit.Create(nil);
    ManAdjustDisplay.Parent := SetupSubPanel;
    with ManAdjustDisplay do
      begin
//      IdTag := 8;
      Left := EditLeft+10;
      Top := ManBackEdit.Top+EditSpacing;
      ReadOnly := True;
      Visible := False;
      Width := 85;
//      HasChanged := CalEditChange;
      end;


  InDataSubPanel := TPanel.Create(nil);
  InDataSubPanel.Parent := SetupPanel;
  with InDataSubPanel        do
     begin
     Color := clWebHoneydew;
     BevelInner := bvLowered;
     Align := alClient;
     end;

  InputTitlePanel := TPanel.Create(Nil);
  InputTitlePanel.Parent := InDataSubPanel;

  with InputTitlePanel do
    begin
    Color := clWebIvory;
    BevelInner := bvLowered;
    Caption := 'Data Input';
    Align := alTop;
    Height := 25;
    Alignment := taCenter;
    end;


  ManDataGrid := TInputDataGrid.Create(nil);
  ManDataGrid.Parent := InDataSubPanel;
  with ManDataGrid do
      begin
      Top := 50;
      StandardWidth :=240;
      AdjustWidth:= 380;
      Font.Size := 10;
      DefaultRowHeight := 18;
      Align := AlRight;
      Initialise(-100,21,100);
      TabStop := True;
      OnDataDifferent:= MyCellDataChanged;
      OnDataReMatch := GridDataMatches;
      end;

{  EditMemo := TMemo.create(nil);
  EditMemo.Parent := InDataSubPanel;
  with EditMemo do
      begin
      Align := alNone;
      Height := 80;
      Width := 220;
      Left := 10;
      Top := 270;
      Visible := False;
      end;}


    FirstrCompLabel := TLabel.Create(nil);
    FirstrCompLabel.Parent := InDataSubPanel;
    with FirstrCompLabel do
      begin
      Caption := 'First Comp Posn.';
      Left := 10;
      Top := 80;
      end;

    FirstCompEdit := TCalEditBox.Create(nil);
    FirstCompEdit.Parent := InDataSubPanel;
    with FirstCompEdit do
      begin
      IdTag := 4;
      Width  := 90;
      Left := EditLeft-10;
      Top := 80;
      HasChanged := CalEditChange;
      end;


    CompIncrLabel := TLabel.Create(nil);
    CompIncrLabel.Parent := InDataSubPanel;
    with CompIncrLabel do
      begin
      Caption := 'Comp Increment';
      Left := 10;
      Top := 110;
      end;

    CompIncrEdit := TCalEditBox.Create(nil);
    CompIncrEdit.Parent := InDataSubPanel;
    with CompIncrEdit do
      begin
      IdTag := 5;
      Width  := 90;
      Left := EditLeft-10;
      Top := 110;
      HasChanged := CalEditChange;
      end;

    CompEntriesLabel := TLabel.Create(nil);
    CompEntriesLabel.Parent := InDataSubPanel;
    with CompEntriesLabel do
      begin
      Caption := 'No. Entries';
      Left := 10;
      Top := 140;
      end;

    NumEntriesEdit:= TCalEditBox.Create(nil);
    NumEntriesEdit.Parent := InDataSubPanel;
    with NumEntriesEdit do
      begin
      IdTag := 6;
      Width  := 90;
      Left := EditLeft-10;
      Top := 140;
      HasChanged := CalEditChange;
      end;

    LastCompLabel :=  TLabel.Create(nil);
    LastCompLabel.Parent := InDataSubPanel;
    with LastCompLabel do
      begin
      Caption := 'Last Comp Posn.';
      Left := 10;
      Top := 170;
      end;

    LastCompValue :=  TLabel.Create(nil);
    LastCompValue.Parent := InDataSubPanel;
    with LastCompValue do
      begin
      Caption := '???.????';
      Left := EditLeft-10;
      Top := 170;
      end;

    TableTopButton := TButton.Create(nil);
    TableTopButton.Parent := InputTitlePanel;
    with TableTopButton do
      begin
      Height := 20;
      Width := EBWidth;
      Left := InputTitlePanel.Width-230;
      Top := 3;
      Caption := 'To Top';
      OnClick := TopofTable;
      end;

    TableEndButton := TButton.Create(nil);
    TableEndButton.Parent := InputTitlePanel;
    with TableEndButton do
      begin
      Height := 20;
      Width := EBWidth;
      Left := InputTitlePanel.Width-TableEndButton.Width-10;
      Top := 3;
      Caption := 'To End';
      OnClick := BottomOfTable;
      end;

    InitInputTableButton := TButton.Create(nil);
    InitInputTableButton.Parent := InDataSubPanel;
    with InitInputTableButton do
      begin
      Width := EBWidth;
      Left := 10;
      Top := 170;
      Caption := 'Init. Table';
      OnClick := InitInputTable;
      end;

    LoadFileButton := TButton.Create(nil);
    LoadFileButton.Parent := InDataSubPanel;
    with LoadFileButton do
      begin
      Width := EBWidth;
      Left := 10;
      Top := 270;
      Caption := 'Load';
      OnClick := RequestFileLoad;
      end;

    LoadAdjustFileButton := TButton.Create(nil);
    LoadAdjustFileButton.Parent := InDataSubPanel;
    with LoadAdjustFileButton do
      begin
      Width := EBWidth+50;
      Left := 10;
      Top := 270;
      Caption := 'Load Adjustment';
      OnClick := RequestAdjustFileLoad;
      Visible := False;
      end;

    AdjustModeButton  := TButton.Create(nil);
    AdjustModeButton.Parent := InDataSubPanel;
    with AdjustModeButton do
      begin
      Width := 130;
      Left := 10;
      Top := 360;
      Caption := 'Adjust Mode';
      OnClick := TableAdjustModeRequest;
      end;

    MergeAdjustButton := TButton.Create(nil);
    MergeAdjustButton.Parent := InDataSubPanel;
    with MergeAdjustButton do
      begin
      Width := 130;
      Left := 10;
      Top := 330;
      Caption := 'Do Adjust';
      OnClick := DoAdjustRequest;
      Visible := False;
      end;



    SubmitButton := TButton.Create(nil);
    SubmitButton.Parent := InDataSubPanel;
    with SubmitButton do
      begin
      Left := 10;
      Width := EBWidth;
      Top := 230;
      Caption := 'Submit';
      Enabled := False;
      OnClick := DoSubmit;
      end;

    RevertButton := TButton.Create(nil);
    RevertButton.Parent := AxisStatusPanel;
    with RevertButton do
      begin
      Width := 130;
      Top := 4;
      Height := 20;
      Caption := 'Cancel Changes';
      Visible := False;
      OnClick := DoRevert;
      end;

  GraphPanel:= TPanel.Create(Self);
  GraphPanel.Parent := Self;
  with GraphPanel do
    begin
    Color := clWebLemonChiffon;
    BevelInner := bvLowered;
    end;

{  DataInputPanel:= TPanel.Create(Self);
  DataInputPanel.Parent := Self;
  with DataInputPanel do
    begin
    Color := clWebMoccasin;
    BevelInner := bvLowered;
    end;}

  GeneratePanel:= TPanel.Create(Self);
  GeneratePanel.Parent := Self;
  with GeneratePanel do
    begin
    Color := clWebBisque;
    BevelInner := bvLowered;
    end;

  OutputMemo := TMemo.Create(nil);
  OutputMemo.Parent := GeneratePanel;
  with OutputMemo do
    begin
    Font.Size := 8;
    Font.Style := [fsBold];
    Font.Name := 'Courier New';
    Align := alRight;
    Width := 600;
    ScrollBars := ssVertical;
    end;

  OutGenButton := TButton.Create(nil);
  OutGenButton.Parent := GeneratePanel;
  with OutGenButton do
    begin
    Caption := 'Make Output';
    Left := 10;
    Width := 110;
    Height := 40;
    Top := 10;
    OnClick := MakeOutputFile;
    end;

  WriteCompFileButton  := TButton.Create(nil);
  WriteCompFileButton.Parent := GeneratePanel;
  with WriteCompFileButton do
    begin
    Caption := 'Write Comp';
    Left := 10;
    Height := 40;
    Width := 110;
    Top := 70;
    OnClick := DoWriteRequest;
    Enabled := False;
    end;

  FirstFullRegButton   := TButton.Create(nil);
  FirstFullRegButton.Parent := GeneratePanel;
  with FirstFullRegButton do
    begin
    Caption := 'Register Compensation';
    Left := 10;
    Height := 40;
    Width := 180;
    Top := 130;
    OnClick := RequestFirstReg ;
//    Visible:= True;
    end;

  ArchiveButton := TButton.Create(nil);
  ArchiveButton.Parent := HistoryPanel;
  with ArchiveButton do
    begin
    Caption := 'Archive';
    Top := 10;
    Left := 710;
    Width := 110;
    Visible := False;
    OnClick := RequestFullArchive;
    end;


 CalGraph := TCalibrationGraphForm.Create(nil);
 CalGraph.Parent := GraphPanel;
 with CalGraph do
  begin
  Top := 10;
  Left := 10;
  Width := 250;
  Height := 150;
  end;


 AxisSelect := TComboBox.Create(self);
 AxisSelect.Parent := TitlePanel;
 AxisSelect.OnChange := AxisSelected;
 AxisSelect.Width := 45;
 AxisSelect.Visible := False;


  ReportStatusButton := TBitBtn.create(Nil);
  ReportStatusButton.Parent := ImportPanel;
  with ReportStatusButton do
    begin
    Top := 240;
    Left := 20;
    Width := 100;
    Caption := 'Report';
    OnClick := DoSatusReport;
    Visible := True;
    end;

  DoImportButtonButton := TBitBtn.create(Nil);
  DoImportButtonButton.Parent := ImportPanel;
  with DoImportButtonButton do
    begin
    Top := 150;
    Left := 20;
    Width := 200;
    Caption := 'Import Calibration Data';
    OnClick := DoImportClick;
    Visible := False;
    end;

  DoClearAndRestartButton := TBitBtn.create(Nil);
  DoClearAndRestartButton.Parent := ImportPanel;
  with DoClearAndRestartButton do
    begin
    Top := 280;
    Left := 20;
    Width := 200;
    Caption := 'Clear Comp and Restart';
    OnClick := DoClearCompClick;
    Visible := False;
    end;

{  ConfirmStatusButton := TBitBtn.create(Nil);
  ConfirmStatusButton.Parent := ImportPanel;
  with ConfirmStatusButton do
    begin
    Top := 300;
    Left := 20;
    Width := 120;
    Caption := 'Confirm Data';
    Visible := False;
    OnClick := DoImportConfirm;
    end;}

  StatusMessage := TMemo.Create(nil);
  StatusMessage.Parent := ImportPanel;
  with StatusMessage do
    begin
    Align := alRight;
    Width := 320;
    ScrollBars := ssVertical;
    end;


  MessageMemo := TMemo.Create(nil);
  MessageMemo.Parent := ImportPanel;
  with MessageMemo do
    begin
    Align := alBottom;
    Height := 80;
    end;
  MessageList   := TStringList.create;


  ConDataButton := TSpeedButton.Create(Nil);
  ConDataButton.Parent := TitlePanel;
  with ConDataButton do
    begin
    Caption := 'Out Data';
    OnClick := ShowConditionedData;
    GroupIndex := 1;
    AllowAllUp := False;
    Visible := OutDataVisible;
    end;

  StatusButton := TSpeedButton.Create(Nil);
  StatusButton.Parent := TitlePanel;
  with StatusButton do
    begin
    Caption := 'Status';
    OnClick := ShowStatus;
    GroupIndex := 1;
    AllowAllUp := False;
    Font.Color := clBlack;
    end;

  DataInputButton := TSpeedButton.Create(Nil);
  DataInputButton.Parent := TitlePanel;
  with DataInputButton do
    begin
    Caption := 'Data Input';
    OnClick := ShowDataIn;
    GroupIndex := 1;
    AllowAllUp := False;
    end;

  GenerateButton := TSpeedButton.Create(Nil);
  GenerateButton.Parent := TitlePanel;
  with GenerateButton do
    begin
    Caption := 'Make Output';
    OnClick := ShowGenerate;
    GroupIndex := 1;
    AllowAllUp := False;
    end;

  HistoryButton := TSpeedButton.Create(Nil);
  HistoryButton.Parent := TitlePanel;
  with HistoryButton do
    begin
    Caption := 'History';
    OnClick := ShowHistory;
    GroupIndex := 1;
    AllowAllUp := False;
    end;


  Archiver := TCalibrationArchiver.Create(Nil);
  Archiver.Parent := HistoryPanel;
  with Archiver do
    begin
    Top := 10;
    Left := 10;
    Width := 900;
    Height := 350;
    end;


end;



procedure TAxisCalDisplay.MachineIDChange(ATag: TAbstractTag);
begin
MachineIdStr := ATag.AsString;
MCIDDisplay.Text := MachineIdStr;
case AxisList.Availability.PowerUpMode of
  cpuRegisteredButNotRestarted,cpuFullyRegistered:
  begin
  Archiver.AddLogEntry(Format('Machine Id Changed to %s after Comp Fully Registered',[MachineIdStr]),False,'');
  end;

  cpuBlankNoId:
  begin
  AxisList.Availability.SetWorkingFileMacId(CompDataFilePath,MachineIdStr);
  Archiver.AddLogEntry(Format('Machine Id Changed to %s After Blank Comp Start',[MachineIdStr]),False,'');
  end;

  cpuNonBlankNoId:
  begin
  AxisList.Availability.SetWorkingFileMacId(CompDataFilePath,MachineIdStr);
  Archiver.AddLogEntry(Format('Machine Id Changed to %s After NON Blank Comp Start',[MachineIdStr]),False,'');
  end;

  cpuCalibrateRestartWorkInProgress:
  begin
  AxisList.Availability.SetWorkingFileMacId(CompDataFilePath,MachineIdStr);
  Archiver.AddLogEntry(Format('Machine Id Changed to %s After NON Blank Comp Start',[MachineIdStr]),False,'');
  end;

else
  begin
  AxisList.Availability.SetWorkingFileMacId(CompDataFilePath,MachineIdStr);
  Archiver.AddLogEntry(Format('Machine Id Changed to %s After NON Blank Comp Start',[MachineIdStr]),False,'');
  end;
end;

end;

procedure TAxisCalDisplay.GetZeroOffsets(ZOString : String);
var
AxNum : Integer;
AxEntry : TSingleAxisData;
OffString : String;
begin
    if ZOString <> '' then
      begin
      For AxNum :=0 To AxisCount-1 do
        begin
        AxEntry := AxisList.EntryWithMotorNumber(AxNum+1);
        if assigned(AxEntry) then
          begin
          if assigned(AxEntry.CompensationDataLists) then
            begin
            OffString := NextToken(ZOString);
            AxEntry.CompensationDataLists.CompHomeOffset := StrToFloatDef(OffString,0);
            end;
          end;
        end;
     end;
end;

procedure TAxisCalDisplay.Installed;
var
ZeroOffsetsString : String;
AxNum : Integer;
AxEntry : TSingleAxisData;
OffString : String;
IHandler  : TAxCalImportFileList;
CompCount : INteger;
FoundArchive : Boolean;
begin
  inherited Installed;
  CalDataOk.AsInteger := 1;
  A1DataMisMatch.AsInteger := 0;
  A2DataMisMatch.AsInteger := 0;
  A3DataMisMatch.AsInteger := 0;
  A4DataMisMatch.AsInteger := 0;
  A5DataMisMatch.AsInteger := 0;
  A6DataMisMatch.AsInteger := 0;
  MachineIdTag := TagEvent('MachineId' , EdgeFalling, TIntegerTag, MachineIDChange);
  ZeroOffsets := TagEvent('#ZeroOffset',edgeNoChange, TStringTag,nil);

  MachineIdStr := MachineIdTag.AsString;


  AxisList.Availability.UpdateFromDefaultSettings(DefaultSettings);
  Archiver.CompBasePath := CompDataBasePath;
  CompParser := TCompFileParser.Create;

  // First Check For Existence of Edit.Txt File.
  // Text File Now holds curent Calibration State and Machine ID string;
  // If text File does not exist then state becomes either NOId ( if Machine Id is not set
  // OR cpuDeterminestate
  try
  case AxisList.Availability.PowerUpMode of

   cpuDetermineState :
    begin
    // here after First Start with no CompEdit.txt
//    WriteWorkingStringParam('MachineId',MachineIDStr);
    Archiver.ArchiveAndRestartLog(CompDataBasePath);
    CompParser.LoadCompFile(CurrentCompFile,DefaultSettings.AxisListString);
   // CheckCompFileStatus(AxisList);
    ZeroOffsetsString := ZeroOffsets.AsString;
    if assigned(CompParser.ExistingCompList) then
      begin
      if CompParser.ExistingCompList.IsBlank(CompCount) then
        begin
        // Here for Clean Start do Comp route
        AxisList.Availability.FirstPowerUpMode := tfmFromBlank;
        WriteConfigStringParam('OriginalPowerUpMode','FromBlankSystem');
        WriteConfigStringParam('UnRegisteredPowerUpMode', 'FromBlankUnRegRestart');
        if (MachineIDStr = '0000') or (MachineIDStr = '') then
          begin
          AxisList.Availability.PowerUpMode := cpuBlankNoId;
          AxisList.Availability.WorkingMode := cpuBlankNoId;
          WriteConfigStringParam('UnRegisteredPowerUpMode', 'NoIdFromBlankUnRegRestart');
          end
        else
          begin
          AxisList.Availability.PowerUpMode := cpuBlankFirstStart;
          AxisList.Availability.WorkingMode := cpuBlankFirstStart;
          end;
        AxisList.InitFromDefaultData(DefaultSettings);
        AxisList.UpdateFromNCData(NCConfigList);
        GetZeroOffsets(ZeroOffsetsString);
        CreateDefaultWorkingFile(MachineIdStr);
        CurrentAxisData := nil;
        AxisList.LoadFromDataFile(CompDataFilePath);
        If AxisList.Availability.IsSatisfied then
           begin
           AxisList.Availability.WorkingFileLoaded := True;
           AddMessage('Default Working File created',False);
           end
       else
          begin
          AddMessage('Working File created but not all data available',False);
          end;
       end
     else
        begin
        // Here for Import route
        AxisList.InitFromDefaultData(DefaultSettings);
        AxisList.UpdateFromNCData(NCConfigList);
        GetZeroOffsets(ZeroOffsetsString);
//        CreateDefaultWorkingFile(MachineIdStr); // Dont make working file Yet
        CurrentAxisData := nil;
        WriteConfigStringParam('OriginalPowerUpMode','ImportUpgrade');
//        AxisList.LoadFromDataFile(CompDataFilePath);
        WriteConfigStringParam('UnRegisteredPowerUpMode', 'ImportUnRegRestart');
        if MachineIDStr = '0000' then
          begin
          AxisList.Availability.FirstPowerUpMode := tfmUnresolved;
          AxisList.Availability.PowerUpMode := cpuNonBlankNoId;
          AxisList.Availability.WorkingMode := cpuNonBlankNoId;
          WriteConfigStringParam('UnRegisteredPowerUpMode', 'NoIdFromImportUnRegRestart');
          end
        else
          begin
          AxisList.Availability.FirstPowerUpMode := tfmFromImport;
          AxisList.Availability.PowerUpMode := cpuNonBlankFirstStart;
          AxisList.Availability.WorkingMode := cpuNonBlankFirstStart;
          // THis is the Upgrade route.    //Check for import Files     // Check Data Match
          // Request Full register if good
          // set to WIP if not
//        WriteConfigStringParam('Origin','FromInport');
//        Archiver.AddLogEntry('Compdata First Started in  Import Mode',False,'');
          CompParser.LoadCompFile(CurrentCompFile,DefaultSettings.AxisListString);
          IHandler := TAxCalImportFileList.Create;
            try
            IHandler.LoadFromPath(CompImportPath,AxisList.Availability);
            if IHandler.UpdateAxisList(AxisList) then
              begin
              UpdateForImportData(Nil)
              end
            else
              begin
              end;
            finally
            IHandler.Free;
//            WriteConfigIntegerParam('ImportStage',0);
//            AxisList.LoadFromDataFile(CompDataFilePath);
{            If AxisList.Availability.IsSatisfied then
               begin
               AxisList.Availability.WorkingFileLoaded := True;
               AddMessage('Working File created',False);
               WriteConfigIntegerParam('ImportStage',1);
               end
             else
               begin
               AddMessage('Working File created but not all data available',False);
               WriteConfigIntegerParam('ImportStage',-1);
               end;}
              CalStatusGrid.aaUpdateFromAxisList(AxisList);
              InternalCompare;
              AxisList.Availability.ImportloadedAndMatched;
              UpdateStatusFromAxisList(False);

            end;
          end;
        end
      end
    end;

   cpuFullyRegistered :
    begin
    if ReadConfigInteger('FirstRegPending') = 1 then
      begin
      Archiver.AddLogEntry('System restarted to load First Full Registered Compensation',False,'');
      end
    else if ReadConfigInteger('NewCompPending') = 1 then
      begin
      Archiver.AddLogEntry('System restarted to load New Comp File',False,'');
      end;
    WriteConfigIntegerParam('FirstRegPending',0);
    WriteConfigIntegerParam('NewCompPending',0);
    AxisList.LoadFromDataFile(CompDataFilePath);
    CompParser.LoadCompFile(CurrentCompFile,DefaultSettings.AxisListString);

    AxisList.CompareToArchive(RegisteredArchiveFolder,True,True,FoundArchive);
    if FoundArchive then
      begin
      If AxisList.Availability.IsSatisfied then
         begin
         AxisList.Availability.WorkingFileLoaded := True;
         end
      end
    else
       begin
       AddMessage('Can Not Find Registered Archive',False);
       end;
    end;

   // Here for subsequent restarts pre fully registered
   cpuCalibrateRestartWorkInProgress,cpuImportRestartWorkInProgress:
     begin
     // Working file should exist ready to continue and eventually register
     CompParser.LoadCompFile(CurrentCompFile,DefaultSettings.AxisListString);
     AxisList.LoadFromDataFile(CompDataFilePath);
        If AxisList.Availability.IsSatisfied then
         begin
         AxisList.Availability.WorkingFileLoaded := True;
         end;
     Archiver.AddLogEntry('System Restarted without Compenstaion Fully Registered',False,'');

(*      case AxisList.Availability.FirstPowerUpMode of
       cpuInitialise:
       begin
        Archiver.AddLogEntry('System Restarted',False,'');
       end;

       cpuInitialImport:
       begin
       end;

       cpuUnknown:
       begin
       end;
      end; // inner case*)
     end;

(*  cpuInitialise : // FirstStart
    begin
    // Blank CompFile Must exist here
    // Initialise from Default and set status to Work in progress
    // Check Status and Show Options Page
    Archiver.ArchiveAndRestartLog(CompDataBasePath);
    Archiver.AddLogEntry('Compdata initialised on First Load',False,'');
    WriteConfigStringParam('Origin','FromBlankSystem');
    // NO IMPORTS JUST LOAD AND Make Default Blank compfile exists
    CompParser.LoadCompFile(CurrentCompFile,DefaultSettings.AxisListString);
    CheckCompFileStatus(AxisList);

    AxisList.InitFromDefaultData(DefaultSettings);
    AxisList.UpdateFromNCData(NCConfigList);
    ZeroOffsets := TagEvent('#ZeroOffset',edgeNoChange, TStringTag,nil);
    ZeroOffsetsString := ZeroOffsets.AsString;
    if ZeroOffsetsString <> '' then
      begin
      For AxNum :=0 To AxisCount-1 do
        begin
        AxEntry := AxisList.EntryWithMotorNumber(AxNum+1);
        if assigned(AxEntry) then
          begin
          OffString := NextToken(ZeroOffsetsString);
          AxEntry.MachineHomeOffset := StrToFloatDef(OffString,0);
          end;
        end;
     end;
//    CreateDefaultWorkingFile;
    CurrentAxisData := nil;
    AxisList.LoadFromDataFile(CompDataFilePath);
    If AxisList.Availability.IsSatisfied then
      begin
      AxisList.Availability.WorkingFileLoaded := True;
      AddMessage('Default Working File created',False);
     end
    else
      begin
      AddMessage('Working File created but not all data available',False);
      end;
    end ;


   cpuInitialImport :
     begin
     // THis is the Upgrade route.    //Check for import Files     // Check Data Match
     // Request Full register if good
     // set to WIP if not
      WriteConfigStringParam('Origin','FromInport');
      Archiver.AddLogEntry('Compdata First Started in  Import Mode',False,'');
      AxisList.InitFromDefaultData(DefaultSettings);
      AxisList.UpdateFromNCData(NCConfigList);
      ZeroOffsets := TagEvent('#ZeroOffset',edgeNoChange, TStringTag,nil);
      ZeroOffsetsString := ZeroOffsets.AsString;
      if ZeroOffsetsString <> '' then
        begin
        For AxNum :=0 To AxisCount-1 do
          begin
          AxEntry := AxisList.EntryWithMotorNumber(AxNum+1);
          if assigned(AxEntry) then
            begin
            OffString := NextToken(ZeroOffsetsString);
            AxEntry.MachineHomeOffset := StrToFloatDef(OffString,0);
            end;
          end;
       end;
      CompParser.LoadCompFile(CurrentCompFile,DefaultSettings.AxisListString);
      IHandler := TAxCalImportFileList.Create;
      try
      IHandler.LoadFromPath(CompImportPath,AxisList.Availability);
      if IHandler.UpdateAxisList(AxisList) then
        begin
        UpdateForImportData(Nil)
        end
      else
        begin
        end;
      finally
      IHandler.Free;
      WriteConfigIntegerParam('ImportStage',0);
      end;
     end;*)


(*   cpuFirstStartWorkInProgress:
     begin
     // Working file should exist ready to continue and eventually register
      CompParser.LoadCompFile(CurrentCompFile,DefaultSettings.AxisListString);
      AxisList.LoadFromDataFile(CompDataFilePath);

      If AxisList.Availability.IsSatisfied then
       begin
       AxisList.Availability.WorkingFileLoaded := True;
       end;

      case AxisList.Availability.FirstPowerUpMode of
       cpuInitialise:
       begin

       end;

       cpuInitialImport:
       begin

       end;

       cpuUnknown:
       begin
       end;
      end; // inner case
     end;*)
   end; // case
(*  else
    begin
    // Here only if NOT registered
    CompParser := TCompFileParser.Create;
    CheckCompFileStatus(AxisList.Availability.CompFullyRegistered);
    if AxisList.Availability.WorkingFileExists then
      begin
      AxisList.LoadFromDataFile(CompDataFilePath);
      end;

    if AxisList.Availability.IsStandardStartUp then // Full Comp Exists Working File Exists
      begin
      // NO IMPORTS JUST LOAD AND COMPARE
      CompParser.LoadCompFile(CurrentCompFile,DefaultSettings.AxisList);
      If AxisList.Availability.IsSatisfied then
         begin
         AxisList.Availability.WorkingFileLoaded := True;
         end
      end

    else If AxisList.Availability.IsValidBlankSystem then     // Blank Comp No Working File
      begin
      WriteConfigStringParam('Origin','FromBlankSystem');
      // NO IMPORTS JUST LOAD AND Make Default Blank compfile exists
      CompParser.LoadCompFile(CurrentCompFile,DefaultSettings.AxisList);
      AxisList.InitFromDefaultData(DefaultSettings);
      AxisList.UpdateFromNCData(NCConfigList);
      ZeroOffsets := TagEvent('#ZeroOffset',edgeNoChange, TStringTag,nil);
      ZeroOffsetsString := ZeroOffsets.AsString;
      if ZeroOffsetsString <> '' then
        begin
        For AxNum :=0 To AxisCount-1 do
          begin
          AxEntry := AxisList.EntryWithMotorNumber(AxNum+1);
          if assigned(AxEntry) then
            begin
            OffString := NextToken(ZeroOffsetsString);
            AxEntry.MachineHomeOffset := StrToFloatDef(OffString,0);
            end;
          end;
       end;
      CreateDefaultWorkingFile;
      CurrentAxisData := nil;
      AxisList.LoadFromDataFile(CompDataFilePath);
      If AxisList.Availability.IsSatisfied then
        begin
        AxisList.Availability.WorkingFileLoaded := True;
        MessageMemo.Lines.Add('Default Working File created')
       end
      else
        begin
        MessageMemo.Lines.Add('Working File created but not all data available')
        end;


      end

     else If AxisList.Availability.IsUpgradeStartUp then // Full Compfile No Working File
        begin
        AxisList.InitFromDefaultData(DefaultSettings);
        AxisList.UpdateFromNCData(NCConfigList);
        ZeroOffsets := TagEvent('#ZeroOffset',edgeNoChange, TStringTag,nil);
        ZeroOffsetsString := ZeroOffsets.AsString;
        if ZeroOffsetsString <> '' then
          begin
          For AxNum :=0 To AxisCount-1 do
            begin
            AxEntry := AxisList.EntryWithMotorNumber(AxNum+1);
            if assigned(AxEntry) then
              begin
              OffString := NextToken(ZeroOffsetsString);
              AxEntry.MachineHomeOffset := StrToFloatDef(OffString,0);
              end;
            end;
         end;
  //      CreateDefaultWorkingFile;
        CompParser.LoadCompFile(CurrentCompFile,DefaultSettings.AxisList);
        IHandler := TAxCalImportFileList.Create;
        try
        IHandler.LoadFromPath(CompImportPath,AxisList.Availability);
        if IHandler.UpdateAxisList(AxisList) then
          begin
          UpdateForImportData(Nil)
          end
        else
          begin
          end;
        finally
        IHandler.Free;
        end;
        end;
     end;*)
   // all cases from now on!!

   InternalCompare;
   CalStatusGrid.aaUpdateFromAxisList(AxisList);
   AxisList.Availability.ImportloadedAndMatched;

   if AxisList.Availability.WorkingFileExists then
    begin
    UpdateStatusFromAxisList(True);
    end
   else
    begin
    UpdateStatusFromAxisList(False);
    end ;
   AxisSelect.Items.Clear;
   if AxisList.Availability.IsSatisfied then
      begin
      If AxisList.Count > 0 then
        begin
        For AxNum := 0 to AxisList.Count-1 do
          begin
          AxEntry := AxisList[Axnum];
          AxisSelect.Items.Add(AxEntry.AxisName);
          end;
        AxisSelect.Text := AxisSelect.Items[0];
        AxisSelected(nil);
        end;
      end;
   finally
//   UpdateRestartMessage;
{   if assigned(CompParser) then
    begin
    CompParser.Free;
    CompParser := nil;
    end;}
   end;
end;

procedure TAxisCalDisplay.UpdateRestartMessage;
begin
   MessageMemo.Clear;
   if AxisList.Availability.WorkingFileLoaded then
     begin
     if AxisList.Availability.WorkingLoadedandMatches then
        begin
        MessageMemo.Lines.Add('Compensation Input Data Matched Current Compensation File at Last Power Up');
        CalDataOk.AsInteger := 1;
        end
     else
        begin
        MessageMemo.Lines.Add('Mis-match between input data and Current Comp file at Last Power Up');
        CalDataOk.AsInteger := 0;
        end
     end
   else
     begin
     CalDataOk.AsInteger := 0;
     if (AxisList.Availability.IsOKForFirstLoadConfirm) then
       begin
       AxisList.Availability.CheckAllExistingCompSatisfiedandMatched;
       if AxisList.Availability.AllImportLoadedandMatched then
         begin
         MessageMemo.Lines.Add('Matching Import Data Loaded for all Axes In existing Comp File. Press Confirm Data to Save Input Data');
         end
       else
         begin
         MessageMemo.Lines.Add('Import Data available for All axes in comp file but does not Match current Compensation File.');
         end
       end
     else
       begin
       MessageMemo.Lines.Add('Some or all compensation input Data is missing.');
       end
     end;

end;

procedure TAxisCalDisplay.UpdateMessage;
begin
end;

(*   case AxisList.Availability.WorkingMode of
     cpuRegisteredButNotRestarted:
     begin
     AddMessage('System should be restared',False);
     end;

     cpuFullyRegistered:
     begin
     MessageMemo.Clear;
     if AxisList.Availability.WorkingLoadedandMatches then
        begin
        AddMessage('Compensation Input Data Matched Current Compensation File at Last Power Up',False);
        CalDataOk.AsInteger := 1;
        end
     else
        begin
        AddMessage('Mis-match between input data and Current Comp file at Last Power Up',False);
        CalDataOk.AsInteger := 0;
        end
     end;

     cpuBlankFirstStart:
     begin
     MessageMemo.Clear;
     AddMessage('Compenstaion Data is in Initial state. No compensation active',False);
     Archiver.AddLogEntry('System First Started with Valid Blank Compensation File',False,'');
     CalDataOk.AsInteger := 0;
     end;

     cpuBlankNoId:
     begin
     MessageMemo.Clear;
     AddMessage('Compenstaion Data is in Initial state. No compensation active',False);
     AddMessage('Machine Id is not set. Calibration Utility is disabled',False);
     Archiver.AddLogEntry('System First Started with Valid Blank Compensation File but No Machine ID',False,'');
     CalDataOk.AsInteger := 0;
     end;

     cpuNonBlankNoId:
     begin
     MessageMemo.Clear;
     AddMessage('A compensation File exists but the Machine ID has not been set',False);
     AddMessage('Calibration Utility is disabled',False);
     Archiver.AddLogEntry('System First Started with Non Blank Compensation File but No Machine ID',False,'');
     CalDataOk.AsInteger := 0;
     end;

{     cpuNonBlankFirstStart:
     begin
     MessageMemo.Clear;
     AddMessage('A compensation File exists which has not been generated by this machine',False);
     AddMessage('Please select a start up mode or disable the comp Utility for this machine',False);
     Archiver.AddLogEntry('System First Started with Non Blank Compensation and Machine Id set',False,'');
     CalDataOk.AsInteger := 0;
     end;}


    cpuFirstStartWorkInProgress:
     begin
     MessageMemo.Clear;
     Case AxisList.Availability.FirstPowerUpMode of
       cpuInitialise:
       begin
       if AxisList.Availability.IsValidBlankSystem then
        begin
        AddMessage('Compenstaion Data is in Initial state. No compensation active',False);
        Archiver.AddLogEntry('System First Started with Valid Blank Compensation File',False,'');
        CalDataOk.AsInteger := 0;
        end
       else
        begin
        AddMessage('System Has been started for initialisation with a non blank Comp File loaded',False);
        Archiver.AddLogEntry('System First Started with Valid NON Blank Compensation File',False,'');
        CalDataOk.AsInteger := 0;
        end
       end;

       cpuNonBlankFirstStart:
//       cpuInitialImport:
       begin
       MessageMemo.Clear;
       CalDataOk.AsInteger := 0;
       if (AxisList.Availability.IsOKForFirstLoadConfirm) then
         begin
         LoadImportData;
         AxisList.Availability.CheckAllExistingCompSatisfiedandMatched;
         if AxisList.Availability.AllImportLoadedandMatched then
           begin
           AddMessage('Matching Import Data Loaded for all Axes In existing Comp File.',False);
           Archiver.AddLogEntry('System First Started in Import Mode with all Axes having Valid Import Data',False,'');
           end
         else
           begin
           AddMessage('Import Data available for All axes in comp file but does not Match current Compensation File.',False);
           Archiver.AddLogEntry('System First Started in Import Mode but Import Data does Not Match',False,'');
           end
         end
       else
         begin
         AddMessage('Some or all compensation input Data is missing.',False);
         end;
       end;
       end; // Inner case
     end;

     cpuCalibrateRestartWorkInProgress:
     begin
     MessageMemo.Clear;
     Case AxisList.Availability.FirstPowerUpMode of
       cpuInitialise:
       begin
       if AxisList.Availability.IsValidBlankSystem then
        begin
        AddMessage('Compenstaion Data is in Initial state. No compensation active',False);
        CalDataOk.AsInteger := 0;
        end
       else
        begin
        AddMessage('System Has been started for initialisation with a non blank Comp File loaded',False);
        CalDataOk.AsInteger := 0;
        end
       end;

       cpuInitialImport:
       begin
       MessageMemo.Clear;
       // check here if import data was accepted on initial load
       end;
       end; // Inner case
     end;

   end; // Outer case

end;*)

procedure TAxisCalDisplay.InstallComponent(Params: TParamStrings);
var
  Buffer : String;
  AppDir : String;
  ConfigFileTest : String;
  NCPath : String;
begin
  inherited InstallComponent(Params);
{  LazyUpdateTimer := TTimer.create(Nil);
  with LazyUpdateTimer do
    begin
    Enabled := false;
    Interval := 1000;
    OnTimer := DoLazyUpdate;
    end;}

  CalDataOk   := TagPublish(Name + 'DataOk',TIntegerTag);
  NewCalibrationTag := TagPublish(Name + 'CalibrationChanged',TIntegerTag);
  CalRegisteredTag  := TagPublish(Name + 'CalibrationRegistered',TIntegerTag);
  A1DataMisMatch   := TagPublish(Name + 'Axis1DataMismatch',TIntegerTag);
  A2DataMisMatch   := TagPublish(Name + 'Axis2DataMismatch',TIntegerTag);
  A3DataMisMatch   := TagPublish(Name + 'Axis3DataMismatch',TIntegerTag);
  A4DataMisMatch   := TagPublish(Name + 'Axis4DataMismatch',TIntegerTag);
  A5DataMisMatch   := TagPublish(Name + 'Axis5DataMismatch',TIntegerTag);
  A6DataMisMatch   := TagPublish(Name + 'Axis6DataMismatch',TIntegerTag);
  CompDataBasePath := Format('%s..\machspec\CompData',[ProfilePath]);
  RenishawImportPath := Format('%s\CompInputData\InputFiles',[CompDataBasePath]);
  CompImportPath := SlashSep(CompDataBasePath,'Import\');
  NewCalibrationTag.AsInteger := 0;

  if (not DirectoryExists(CompDataBasePath)) then ForceDirectories(CompDataBasePath);
  CurrentCompFile := SlashSep(ProfilePath,'..\machSpec\CompFile.txt');
//  AxisList.Availability.TestForCompFile(CurrentCompFile);
  CompDataFilePath := SlashSep(CompDataBasePath,'CompInputData');

  //  AxisList.Availability.
  ForceDirectories(CompDataFilePath);
  CompDataFilePath := SlashSep(CompDataFilePath,'CompEdit.Txt');

  AxisList.Availability.WorkingFileExists := FileExists(CompDataFilePath);
  CustomerId := Params.ReadString('CustomerID','Unspecified');
  MachineType:= Params.ReadString('MachineType','Unspecified');
  Buffer:= Params.ReadString('ForceLinear','False');
  ForceLinearOption := (Buffer[1] = 'T') or (Buffer[1] = 't');
  If ForceLinearOption then
    begin
    ICRLabel.Visible := False;
    CRCheckBox.Visible := False;
    end;

  CompDataOutPath := Params.ReadString('CompDataOutPath','..\machspec\');
  SecurityLevel   := Params.ReadInteger('SL',0);
  Buffer :=  Params.ReadString('OutDataVisible','False');
  OutDataVisible := (Buffer[1] = 'T') or (Buffer[1] = 't');
  ContractPath := SlashSep(ProfilePath,'..\contspec\Live');
  ConfigFilePath :=  ContractPath;
  ConfigFileTest := SlashSep(ContractPath,'Calibration.cfg');
  if  not FileExists(ConfigFileTest) then
    begin
    ConfigFileTest := SlashSep(ProfilePath,'..\Machspec\Live\Calibration.cfg');
    if FileExists(ConfigFileTest) then ConfigFilePath :=  SlashSep(ProfilePath,'..\MachSpec\Live')
    end;
  DefaultSettings := TDefaultSettingsList.Create;
  NCPath := DefaultSettings.LoadFromFile(SlashSep(ConfigFilePath,'Calibration.cfg'));
  if NCPath = '' then NCPath := slashSep(ProfilePath,'Live');
//  aLivePath := slashSep(ProfilePath,'Live');
  AxisCount := NCConfigList.ReadFromDirectory(NCPath);
  AppDir := ExtractFilePath(Application.ExeName);
  GetCompStatusFrom(SlashSep(ConfigFilePath,'Calibration.cfg'),AxisList.Availability.WorkingFileExists);
end;


procedure TAxisCalDisplay.Resize;
begin
  inherited;
  AxisSelect.Left := TitlePanel.ClientWidth- AxisSelect.Width-10;
  AxisSelect.top := 5;
//  TableTopButton.Left := InputTitlePanel.Width-220;
//  TableEndButton.Left := InputTitlePanel.Width-220+95;
  TableTopButton.Left := ManDataGrid.Left+ 20;
  TableEndButton.Left := TableTopButton.Left+TableEndButton.Width+5;
end;


procedure TAxisCalDisplay.IShutDown;
begin
  CompParser.Free;
  inherited IShutDown;
  IsClosing := True;
end;

procedure TAxisCalDisplay.SetDisplayModeSpacing(LinearIsForced: Boolean);
var
Spacing : Integer;
TopPos : Integer;
begin
if LinearIsForced then Spacing := 40 else Spacing := EditSpacing;
TopPos := 85;
DCLabel.Top := TopPos;
DoubleCountCheckBox.Top := TopPos;
TopPos := TopPos+Spacing;

if not LinearIsForced then
  begin
  ICRLabel.Top := TopPos;
  CRCheckBox.Top := TopPos;
  TopPos := TopPos+Spacing;
  end;

CalLabel.Top := TopPos;
MakeOutputCheckBox.Top := TopPos;
TopPos := TopPos+Spacing;

CPULabel.Top := TopPos;
CPMEdit.Top := TopPos;
TopPos := TopPos+Spacing;

HOLabel.Top := TopPos;
HomeOffsetEdit.Top := TopPos;
TopPos := TopPos+Spacing;

//PosLimitLabel.Top := TopPos;
//PosLimitEdit.Top := TopPos;
TopPos := TopPos+Spacing;

//NegLimitLabel.Top := TopPos;
//NegLimitEdit.Top := TopPos;
TopPos := TopPos+Spacing;

BLLabel.Top := TopPos;
BacklashCheckBox.Top := TopPos;
TopPos := TopPos+Spacing;

MBLLabel.Top := TopPos;
ManBackEdit.Top := TopPos;
TopPos := TopPos+Spacing;

MBAdJustLabel.Top := TopPos;
ManAdjustDisplay.Top := TopPos;

TopPos := 50;
Spacing := 35;
FirstrCompLabel.Top := TopPos;
FirstCompEdit.Top := TopPos;
TopPos := TopPos+Spacing;

CompIncrLabel.Top := TopPos;
CompIncrEdit.Top := TopPos;
TopPos := TopPos+Spacing;

CompEntriesLabel.Top := TopPos;
NumEntriesEdit.Top := TopPos;
TopPos := TopPos+Spacing;

LastCompLabel.Top := TopPos;
LastCompValue.Top := TopPos;

TopPos := TopPos+Spacing;

InitInputTableButton.Top := TopPos;

//TopPos := TopPos+Spacing;

TopPos := TopPos+Spacing;

SubmitButton.Top := TopPos;


end;

procedure TAxisCalDisplay.SetDisplayMode(DMode: TCalDisplayModes);
var
FoundArchive : Boolean;
begin
If DMode <> CurrentDisplayMode then
  begin
  AxisSelect.Visible := False;
  SetupPanel.Visible := False;
  ImportPanel.Visible := False;
  GraphPanel.Visible := False;
  ConditionDataPanel.Visible := False;
  HistoryPanel.Visible := False;
  StatusButton.Enabled := True;
  DataInputButton.Enabled := True;
  GenerateButton.Enabled := True;
  HistoryButton.Enabled := True;
  AxisSelect.Enabled := True;

  HistoryPanel.Parent := Nil;
  SetupPanel.Parent := Nil;
  GraphPanel.Parent := Nil;
  ImportPanel.Parent := Nil;
  GeneratePanel.Parent := Nil;
  ConditionDataPanel.Parent := Nil;
  MCIDDisplay.Text := MachineIdStr;
//  OptionsPanel.Parent := Nil;

  case DMode of
    tcdmStatus:
      begin
      AxisList.CompareToArchive(RegisteredArchiveFolder,True,False,FoundArchive);
      if FoundArchive then
        begin
        CalStatusGrid.aaUpdateFromAxisList(AxisList);
        end;
      ImportPanel.Parent:= Self;
      ImportPanel.align := alClient;
      ImportPanel.Visible := True;
      StatusMessage.Clear;
      UpdateMessageMemo;
      end;

    tcdmBlankNoMacId:
      begin
      CalStatusGrid.aaUpdateFromAxisList(AxisList);
      ImportPanel.Parent:= Self;
      ImportPanel.align := alClient;
      ImportPanel.Visible := True;
      StatusMessage.Clear;
      UpdateMessageMemo;
      StatusButton.Enabled := False;
      DataInputButton.Enabled := False;
      GenerateButton.Enabled := False;
//      HistoryButton.Enabled := False ;
      AxisSelect.Enabled := False;
      UpdateOptionsPage;
      end;

    tcdmNonBlankNoMacId:
      begin
      CalstatusGrid.Visible := False;
      ReportStatusButton.Visible := True;
      ImportPanel.Parent:= Self;
      ImportPanel.align := alClient;
      ImportPanel.Visible := True;
      StatusMessage.Clear;
      UpdateMessageMemo;
      StatusButton.Enabled := False;
      DataInputButton.Enabled := False;
      GenerateButton.Enabled := False;
//      HistoryButton.Enabled := False ;
      AxisSelect.Enabled := False;
      UpdateOptionsPage;
      end;

    tcdmNonBlankFirstStart:
      begin
//      AxisList.CompareToArchive(RegisteredArchiveFolder,True,False,FoundArchive);
//      CalStatusGrid.aaUpdateFromAxisList(AxisList);
//      CalstatusGrid.Visible := False;
//      ReportStatusButton.Visible := False;
      ImportPanel.Parent:= Self;
      ImportPanel.align := alClient;
      ImportPanel.Visible := True;
      StatusMessage.Clear;
//      UpdateMessageMemo;
//      StatusButton.Enabled := False;
//      DataInputButton.Enabled := False;
//      GenerateButton.Enabled := False;
//      HistoryButton.Enabled := False ;
//      AxisSelect.Enabled := False;
       MessageMemo.Clear;
       CalDataOk.AsInteger := 0;
       if (AxisList.Availability.IsOKForFirstLoadConfirm) then
         begin
         LoadImportData;
         AxisList.Availability.CheckAllExistingCompSatisfiedandMatched;
         if AxisList.Availability.AllImportLoadedandMatched then
           begin
           AddMessage('Matching Import Data Loaded for all Axes In existing Comp File.',False);
           Archiver.AddLogEntry('System First Started in Import Mode with all Axes having Valid Import Data',False,'');
           end
         else
           begin
           AddMessage('Import Data available for All axes in comp file but does not Match current Compensation File.',False);
           Archiver.AddLogEntry('System First Started in Import Mode but Import Data does Not Match',False,'');
           end
         end
       else
         begin
         AddMessage('Some or all compensation input Data is missing.',False);
         end;
      end;


    tcdmConditioned:
      begin
      ConditionDataPanel.Parent:= Self;
      ConditionDataPanel.align := alClient;
      ConditionDataPanel.Visible := True;
      If assigned(CurrentAxisData) then
        begin
        ConInDataGrid.UpdateFromAxisData(CurrentAxisData);
        if CurrentAxisData.CompensationDataLists.OutputDataList.Count > 1 then
          begin
          COnOutDataGrid.UpdateFromAxisData(CurrentAxisData);
          end
        end;
      end;

    tcdmDataIn,tcdmSetup:
      begin
      AxisSelect.Visible := True;
      SetupPanel.Parent:= Self;
      SetupPanel.align := alClient;
      SetupPanel.Visible := True;
      SetDisplayModeSpacing(ForceLinearOption);
      end;

    tcdmHistory   :
      begin
      HistoryPanel.Parent:= Self;
      HistoryPanel.align := alClient;
      HistoryPanel.Visible := True;
      end;

    tcdmGraph:
      begin
      GraphPanel.Parent:= Self;
      GraphPanel.align := alClient;
      GraphPanel.Visible := True;
      end;

    tcdmGenerate:
      begin
      GeneratePanel.Parent:= Self;
      GeneratePanel.align := alClient;
      GeneratePanel.Visible := True;
      end;
   end;
  end;

end;


procedure TAxisCalDisplay.ShowConditionedData(Sender: TOBject);
//var
//AxData : TSingleAxisData;
begin
SetDisplayMode(tcdmConditioned);
{if AxisList.Count > 1 then
  begin
  AxData := AxisList.EntryAtIndex(0);
{  if AxData.InputDataLists.GetZeroShift(ZShift) then
    DebugEdit.Text := Format('%5.3f',[ZShift])
  else
    DebugEdit.Text := 'Failed';
  end;}
end;

procedure TAxisCalDisplay.ShowStatus(Sender: TOBject);
begin
SetDisplayMode(tcdmStatus);
end;

procedure TAxisCalDisplay.ShowGenerate(Sender: TOBject);
begin
SetDisplayMode(tcdmGenerate);
end;

procedure TAxisCalDisplay.ShowHistory(Sender: TOBject);
begin
SetDisplayMode(tcdmHistory);
end;


procedure TAxisCalDisplay.ShowDataIn(Sender: TOBject);
begin
SetDisplayMode(tcdmDataIn);
end;

procedure TAxisCalDisplay.AxisSelected(Sender: TOBject);
var
SelText : String;
ShowPage : Boolean;
begin
SelText := AxisSelect.Text;
if Sender = nil then
  ShowPage := false
else
  ShowPage := True;

CurrentAxisData := AxisList.EntryWithAxisName(SelText);
if assigned(CurrentAxisData) then
  begin
  InitialiseDisplayForAxis(CurrentAxisData,ShowPage,True);
 end;
end;


procedure TAxisCalDisplay.Activate(aTag: TAbstractTag);
begin
  IF ATag.AsInteger = 1 THEN exit;
  if FirstShow then
    begin
    FirstShow := False;
    StatusButton.Top :=3;
    StatusButton.Left :=3;
    StatusButton.Width := BWidth;
    DataInputButton.Top :=3;
    DataInputButton.Left := StatusButton.Left+BWidth+2;
    DataInputButton.Width := BWidth;
    GenerateButton.Top :=3;
    GenerateButton.Left := DataInputButton.Left+BWidth+2;
    GenerateButton.Width := BWidth;
    HistoryButton.Top :=3;
    HistoryButton.Left := GenerateButton.Left+BWidth+2;
    HistoryButton.Width := BWidth;
    ConDataButton.Top :=3;
    ConDataButton.Width := BWidth;
    ConDataButton.Left := HistoryButton.Left+BWidth+2;
    ManDataGrid.Visible := True;
    end;

case AxisList.Availability.WorkingMode of
   cpuImportRestartWorkInProgress:
   begin
      Archiver.AddLogEntry('System Restarted after Import. Not Yet Fully registered',False,'');
      SetDisplayMode(tcdmStatus);
      StatusButton.Down := True;
   end;

   cpuCalibrateRestartWorkInProgress:
   begin
   SetDisplayMode(tcdmStatus);
   Archiver.AddLogEntry('System Restarted in Data Input Mode. Not Yet Fully registered',False,'');
   StatusButton.Down := True;
   end;

   cpuRegisteredButNotRestarted:
   begin
      SetDisplayMode(tcdmStatus);
      StatusButton.Down := True;
   end;

   cpuFullyRegistered:
   begin

    if AxisList.Availability.IsSatisfied then
      begin
      SetDisplayMode(tcdmSetup);
      DataInputButton.Down := True;
      end
    else
      begin
      SetDisplayMode(tcdmStatus);
      StatusButton.Down := True;
      end

   end;


   cpuBlankNoId:
   begin
   SetDisplayMode(tcdmBlankNoMacId);
   MessageMemo.Clear;
   AddMessage('Compenstaion Data is in Initial state. No compensation active',False);
   AddMessage('Machine Id is not set. Calibration Utility is disabled',False);
   Archiver.AddLogEntry('System First Started with Valid Blank Compensation File but No Machine ID',False,'');
   CalDataOk.AsInteger := 0;
   end;

   cpuBlankFirstStart:
   begin
   SetDisplayMode(tcdmStatus);
   MessageMemo.Clear;
   AddMessage('Compenstaion Data is in Initial state. No compensation active',False);
   Archiver.AddLogEntry('System First Started with Valid Blank Compensation File',False,'');
   CalDataOk.AsInteger := 0;
   end;

   cpuNonBlankFirstStart:
   begin
   SetDisplayMode(tcdmNonBlankFirstStart);
   MessageMemo.Clear;
   AddMessage('A compensation File exists which has not been generated by this machine',False);
   Archiver.AddLogEntry('System First Started with Non Blank Compensation and Machine Id set',False,'');
   If AxisList.Availability.AllImportLoadedandMatched then
      begin
      Archiver.AddLogEntry('Matching Import Data Available for All axes on First Start',False,'');
      end
   else
      begin
      Archiver.AddLogEntry('Import Data does not match current Comp File',False,'');
      end;

   CalDataOk.AsInteger := 0;
   end;

   cpuNonBlankNoId:
   begin
   SetDisplayMode(tcdmNonBlankNoMacId);
   MessageMemo.Clear;
   AddMessage('A compensation File exists but the Machine ID has not been set',False);
   AddMessage('Calibration Utility is disabled',False);
   Archiver.AddLogEntry('System First Started with Non Blank Compensation File but No Machine ID',False,'');
   CalDataOk.AsInteger := 0;
   end;

else
  begin
  // Not First Show
  if AxisSelect.Items.Count > 0 then
    begin
    Activating := True  ;
    try
    AxisSelect.Text := AxisSelect.Items[0];
    CurrentAxisData := AxisList.EntryWithAxisName(AxisSelect.Items[0]);
    if assigned(CurrentAxisData) then
      begin
      InitialiseDisplayForAxis(CurrentAxisData,True,True);
      end;
    finally
    Activating := False;
    end
    end;
  end; // case

  end;
  inherited;
  SysObj.Layout.Expand(Self, 0);
end;


procedure TAxisCalDisplay.InitialiseDisplayForAxis(InAxisData: TSingleAxisData;GoToPage : Boolean;BufferContents : Boolean);
var AxName : String;
begin
AxisDisplayBox.Caption := InAxisData.AxisName;
AxName := UpperCase(InAxisData.AxisName);

ManDataGrid.LoadFromSnapshot(CurrentAxisData.CurrentStatusBuffer);
ManDataGrid.RegisterSnapshot(InAxisData.WorkingFileStatusBuffer);
MergeAdjustButton.Visible := False;
MBAdJustLabel.Visible := False;
ManAdjustDisplay.Visible := False;

with InAxisData.CompensationDataLists do
  begin
  BacklashCheckBox.Checked := BackLashUsed;
  if BackLashUsed then
    begin
    MBLLabel.Enabled := False;
    ManBackEdit.Enabled := False;
    ManBackEdit.Text := 'N/A'
    end
  else
    begin
    MBLLabel.Enabled := True;
    ManBackEdit.Enabled := True;
    ManBackEdit.Text := Format('%d',[ManualBacklashMicron]);
    end ;
  CRCheckBox.Checked := not AxisIsLinear;

  CPMedit.Text := Format('%5.3f',[aaEncoderCountsPerUnit]);
  FirstCompEdit.Text := Format('%5.3f',[FirstPosition]);
  CompIncrEdit.Text := Format('%5.3f',[CompIncrement]);
  NumEntriesEdit.Text := Format('%d',[NumberOfEntries]);
  HomeOffsetEdit.Text := Format('%5.3f',[CompHomeOffset]);
  LastCompValue.Caption := Format('%5.3f',[CompPositiveLimit]);
//  NegLimitEdit.Text := Format('%5.3f',[CompNegativeLimit]);
  MakeOutputCheckBox.Checked := MakeOutput;
  DoubleCountCheckBox.Checked := DoublePointsOnOutput;
  if (AxName = 'A') or (AxName = 'B') or (AxName = 'C') then
    begin
    HOLabel.Caption := 'Home Offset (deg)';
//    PosLimitLabel.Caption := 'Pos Limit (deg)';
//    NegLimitLabel.Caption := 'Neg Limit (deg)';
    MBLLabel.Caption := 'Backlash (deg/1000)';
    end
  else
    begin
    HOLabel.Caption := 'Home Offset (mm)';
//    PosLimitLabel.Caption := 'Pos Limit (mm)';
//    NegLimitLabel.Caption := 'Neg Limit (mm)';
    MBLLabel.Caption := 'Backlash (micron)';
    end
  end;

if GoToPage then
  begin
  SetDisplayMode(tcdmSetup);
  DataInputButton.Down := True;
  ConditionSubmitButton;
  ManDataGrid.AdjustMode := False;
  AdjustModeButton.Caption := 'Adjust Mode';
  LoadAdjustFileButton.Visible := False;
 end;
end;



procedure TAxisCalDisplay.UpdateForImportData(Sender: TObject);
var
AxData : TSingleAxisData;
begin
  AxisSelect.Clear;
  if AxisList.Count > 0 then
    begin
    if assigned(AxisList.Availability) then
      begin
      if AxisList.Availability.XData.ImportDataState = tcdAvailable then
        begin
        AxData := AxisList.EntryWithAxisName('X');
        if assigned(AxData) then
          begin
          if AxData.CompensationDataLists.CurrentInputDataList.Count > 0 then
            begin
            if AxData.CompensationDataLists.CurrentInputDataList.UpdateShiftedDataForHomeOffset(AxData.CompensationDataLists.CompHomeOffset) then
              begin
              AxData.CompensationDataLists.OutputDataList.UpdateFromInputDataList(AxData.CompensationDataLists.CurrentInputDataList);
              AxisList.Availability.XData.ImportDataState := tcdLoaded;
              AxisSelect.Items.Add('X');
              AxData.MakeOutput := True;
              end
            end;
          end;
        end;
      end;

      if AxisList.Availability.YData.ImportDataState = tcdAvailable then
        begin
        AxData := AxisList.EntryWithAxisName('Y');
        if assigned(AxData) then
          begin
          if AxData.CompensationDataLists.CurrentInputDataList.Count > 0 then
            begin
            if AxData.CompensationDataLists.CurrentInputDataList.UpdateShiftedDataForHomeOffset(AxData.CompensationDataLists.CompHomeOffset) then
              begin
              AxData.CompensationDataLists.OutputDataList.UpdateFromInputDataList(AxData.CompensationDataLists.CurrentInputDataList);
              AxisList.Availability.YData.ImportDataState := tcdLoaded;
              AxisSelect.Items.Add('Y');
              AxData.MakeOutput := True;
              end
            end;
          end;
        end;

      if AxisList.Availability.ZData.ImportDataState = tcdAvailable then
        begin
        AxData := AxisList.EntryWithAxisName('Z');
        if assigned(AxData) then
          begin
          if AxData.CompensationDataLists.CurrentInputDataList.Count > 0 then
            begin
            if AxData.CompensationDataLists.CurrentInputDataList.UpdateShiftedDataForHomeOffset(AxData.CompensationDataLists.CompHomeOffset) then
              begin
              AxData.CompensationDataLists.OutputDataList.UpdateFromInputDataList(AxData.CompensationDataLists.CurrentInputDataList);
              AxisList.Availability.ZData.ImportDataState := tcdLoaded;
              AxisSelect.Items.Add('Z');
              AxData.MakeOutput := True;
              end
            end;
          end;
        end;

      if AxisList.Availability.AData.ImportDataState = tcdAvailable then
        begin
        AxData := AxisList.EntryWithAxisName('A');
        if assigned(AxData) then
          begin
          if AxData.CompensationDataLists.CurrentInputDataList.Count > 0 then
            begin
            if AxData.CompensationDataLists.CurrentInputDataList.UpdateShiftedDataForHomeOffset(AxData.CompensationDataLists.CompHomeOffset) then
              begin
              AxData.CompensationDataLists.OutputDataList.UpdateFromInputDataList(AxData.CompensationDataLists.CurrentInputDataList);
              AxisList.Availability.AData.ImportDataState := tcdLoaded;
              AxisSelect.Items.Add('A');
              AxData.MakeOutput := True;
              end
            end;
          end;
        end;

      if AxisList.Availability.BData.ImportDataState = tcdAvailable then
        begin
        AxData := AxisList.EntryWithAxisName('B');
        if assigned(AxData) then
          begin
          if AxData.CompensationDataLists.CurrentInputDataList.Count > 0 then
            begin
            if AxData.CompensationDataLists.CurrentInputDataList.UpdateShiftedDataForHomeOffset(AxData.CompensationDataLists.CompHomeOffset) then
              begin
              AxData.CompensationDataLists.OutputDataList.UpdateFromInputDataList(AxData.CompensationDataLists.CurrentInputDataList);
              AxisList.Availability.BData.ImportDataState := tcdLoaded;
              AxisSelect.Items.Add('B');
              AxData.MakeOutput := True;
              end
            end;
          end;
        end;

      if AxisList.Availability.CData.ImportDataState = tcdAvailable then
        begin
        AxData := AxisList.EntryWithAxisName('C');
        if assigned(AxData) then
          begin
          if AxData.CompensationDataLists.CurrentInputDataList.Count > 0 then
            begin
            if AxData.CompensationDataLists.CurrentInputDataList.UpdateShiftedDataForHomeOffset(AxData.CompensationDataLists.CompHomeOffset) then
              begin
              AxData.CompensationDataLists.OutputDataList.UpdateFromInputDataList(AxData.CompensationDataLists.CurrentInputDataList);
              AxisList.Availability.CData.ImportDataState := tcdLoaded;
              AxisSelect.Items.Add('C');
              AxData.MakeOutput := True;
              end
            end;
          end;
        end;
    end;

   if AxisSelect.Items.Count > 0 then AxisSelect.Text := AxisSelect.Items[0];
   AxisSelected(Nil);
end;


procedure TAxisCalDisplay.AxisInDataChanged(DataIndex: Integer; ZShift: Double; Valid: Boolean);
begin
if assigned(CurrentAxisData) then
  begin
  if DataIndex = CurrentAxisData.ParentListIndex then
    begin
    if Valid then
      begin
      ConInDataGrid.UpdateFromAxisData(CurrentAxisData);
      end;
    end;
  end;
end;

procedure TAxisCalDisplay.AxisOutDataChanged(DataIndex: Integer; Valid: Boolean);
begin
if assigned(CurrentAxisData) then
  begin
  if DataIndex = CurrentAxisData.ParentListIndex then
    begin
    if Valid then
      begin
      ConOutDataGrid.UpdateFromAxisData(CurrentAxisData);
      end;
    end;
  end;
end;

procedure TAxisCalDisplay.InitInputTable(Sender: TObject);
begin
if assigned(CurrentAxisData) then
  begin
  ManDataGrid.AdjustMode := False;
  ManDataGrid.Initialise(CurrentAxisData.CompensationDataLists.FirstPosition,
                         CurrentAxisData.CompensationDataLists.NumberOfEntries,
                         CurrentAxisData.CompensationDataLists.CompIncrement);
  end;
  StoreDisplayStatusToSnapshot(CurrentAxisData.CurrentStatusBuffer);
  TableInitRequired(False);
  ConditionSubmitButton;
end;

procedure TAxisCalDisplay.TopofTable(Sender: TObject);
begin
ManDataGrid.GoToTop;
end;

procedure TAxisCalDisplay.BottomOfTable(Sender: TObject);
begin
ManDataGrid.GotoBottom;
end;

function TAxisCalDisplay.TestCREntriesValid : Integer;
var
FloatEntries : Single;
IntEntries : Integer;
Remainder :Single;
begin
Result := -1;
try
FloatEntries := 360/CurrentAxisData.CompensationDataLists.CompIncrement;
IntEntries   := Round(360/CurrentAxisData.CompensationDataLists.CompIncrement);
Remainder := FloatEntries-IntEntries;
If Remainder < 0.000001 then Result := IntEntries;
except
Result := -1;
end;
end;

procedure TAxisCalDisplay.InputCheckBoxClicked(Sender: TObject);
var
NumEntries : Integer;
begin
if Not assigned(CurrentAxisData) then exit;
if Sender = DoubleCountCheckBox then
  begin
  if assigned(CurrentAxisData) then CurrentAxisData.DoublePointsOnOutput := DoubleCountCheckBox.Checked;
  end
else if Sender =  CRCheckBox  then
  begin
  if assigned(CurrentAxisData) then CurrentAxisData.CompensationDataLists.AxisIsLinear := not CRCheckBox.Checked;
  if not CurrentAxisData.CompensationDataLists.AxisIsLinear then
    begin
    NumEntriesEdit.Enabled := False;
    NumEntries := TestCREntriesValid;
    if NumEntries > 0 then
      begin
      NumEntriesEdit.Text := IntToStr(NumEntries);
      CurrentAxisData.CompensationDataLists.NumberOfEntries := NumEntries;
      end;
    end
  else
    begin
    NumEntriesEdit.Enabled := True;
    end;
  end
else if Sender =  MakeOutputCheckBox then
  begin
  if assigned(CurrentAxisData) then CurrentAxisData.MakeOutput := MakeOutputCheckBox.Checked;
  end
else if Sender =  BacklashCheckBox then
  begin
  if assigned(CurrentAxisData.CompensationDataLists) then
    begin
    CurrentAxisData.CompensationDataLists.BacklashUsed :=BacklashCheckBox.Checked;
    ManBackEdit.Enabled := not BacklashCheckBox.Checked;
    MBLLabel.Enabled := not BacklashCheckBox.Checked;
    if BacklashCheckBox.Checked then
        ManBackEdit.Text := 'N/A'
    else
        ManBackEdit.Text := Format('%d',[CurrentAxisData.CompensationDataLists.ManualBacklashMicron]);
    end;
  end;
  if Not Activating then
    begin
    ConditionSubmitButton;
    end
end;

procedure TAxisCalDisplay.ConditionSubmitButton;
begin

CurrentAxisData.CompareCurrentToWorkingSnapshot;
If (not CurrentAxisData.CurrentStatusBuffer.ParametersMatch)
or  (not CurrentAxisData.CurrentStatusBuffer.InputDataMatch)
//or (CurrentAxisData.AdjustmentLoaded)
    then
      begin
      SubmitButton.Enabled := True;
      RevertButton.Visible := True;
      AxisEdited.Left := AxisStatusPanel.Width-AxisEdited.Width-10;
      AxisEdited.Visible := True;
      RevertButton.Left := AxisEdited.Left-RevertButton.Width-10;
      RevertButton.Visible := True;
      end
  else
      begin
      SubmitButton.Enabled := False;
      RevertButton.Visible := False;
      AxisEdited.Visible := False;
      RevertButton.Visible := false;
      end
end;





function TAxisCalDisplay.InternalCompare: Boolean;
var
TextGen : TCalTextGenerator;
DateStr : String;
TimeStr : String;
AxNum : Integer;
AxData : TSingleAxisData;
MadeAChange : Boolean;
ChangeMade : Boolean;
begin
  Result := False;
  if assigned(CompParser) then
    begin
    if AxisList.Count > 0 then
      begin
      // Pass 1 check for mismatch in output entries to condition Use of Double Output Points
      TextGen := TCalTextGenerator.Create;
      try
      DateStr := StdDateString;
      TimeStr := StdTimeString;
      MachineIdStr := MachineIdTag.AsString;
      TextGen.ClearAndMakeHeader(AxisList,MachineType,MachineIdStr,CustomerId,DateStr,TimeStr);
      TextGen.MakeCompText(AxisList);
      TextGen.MakeBacklashText(AxisList);
      CompParser.CompareNumberEntries(TextGen,AxisList.Availability);
      MadeAChange := False;
      For Axnum := 0 to AxisList.Count-1 do
        begin
        AxData := AxisList[AxNum];
        AxisList.Availability.ResolveEntryCountForAxisAtIndex(AXnum,AxData,ChangeMade);
        if ChangeMade then MadeAChange := True;
        end;

      // Pass 2 remake if different and check content
      if MadeAChange then
        begin
        TextGen.ClearAndMakeHeader(AxisList,MachineType,MachineIdStr,CustomerId,DateStr,TimeStr);
        TextGen.MakeCompText(AxisList);
        TextGen.MakeBacklashText(AxisList);
        end;
      Result := CompParser.CompareCompFileToStrings(TextGen,AxisList.Availability);
      if AxisList.Availability.DataMatchesForAxis(0) then A1DataMisMatch.AsInteger := 0 else A1DataMisMatch.AsInteger := 1;
      if AxisList.Availability.DataMatchesForAxis(1) then A2DataMisMatch.AsInteger := 0 else A2DataMisMatch.AsInteger := 1;
      if AxisList.Availability.DataMatchesForAxis(2) then A3DataMisMatch.AsInteger := 0 else A3DataMisMatch.AsInteger := 1;
      if AxisList.Availability.DataMatchesForAxis(3) then A4DataMisMatch.AsInteger := 0 else A4DataMisMatch.AsInteger := 1;
      if AxisList.Availability.DataMatchesForAxis(4) then A5DataMisMatch.AsInteger := 0 else A5DataMisMatch.AsInteger := 1;
      if AxisList.Availability.DataMatchesForAxis(5) then A6DataMisMatch.AsInteger := 0 else A6DataMisMatch.AsInteger := 1;
      finally
      TextGen.Free;
      end;
      end;
    end;
end;

function TAxisCalDisplay.MakeDateString: String;
var
ST : TSystemTime;
Month,Day,Year : String;
Hour,Min,Sec : String;
begin
GetLocalTime(ST);
Year := DDigit(St.wYear);
Month := DDigit(St.wMonth);
Day := DDigit(St.wDay);
Hour := DDigit(St.wHour);
Min := DDigit(St.wMinute);
Sec := DDigit(St.wSecond);
Result:= Format('%s_%s_%s_%s_%s_%s',[Day,Month,Year,Hour,Min,Sec]);
end;

function TAxisCalDisplay.CurrentTimeAsFileSafeString: String;
var
ST : TSystemTime;
Hour,Min,Sec : String;
begin
GetLocalTime(ST);
Hour := DDigit(St.wHour);
Min := DDigit(St.wMinute);
Sec := DDigit(St.wSecond);
Result:= Format('%s_%s_%s',[Hour,Min,Sec]);
end;

function TAxisCalDisplay.CurrentDateAsFileSafeString: String;
var
ST : TSystemTime;
Month,Day,Year : String;
begin
GetLocalTime(ST);
Year := DDigit(St.wYear);
Month := DDigit(St.wMonth);
Day := DDigit(St.wDay);
Result:= Format('%s_%s_%s',[Day,Month,Year]);
end;

function TAxisCalDisplay.DisplayableDateTimeString: String;
var
ST : TSystemTime;
Month,Day,Year : String;
Hour,Min,Sec : String;
begin
GetLocalTime(ST);
Year := DDigit(St.wYear);
Month := DDigit(St.wMonth);
Day := DDigit(St.wDay);
Hour := DDigit(St.wHour);
Min := DDigit(St.wMinute);
Sec := DDigit(St.wSecond);
Result:= Format('%s:%s:%s at %s:%s:%s',[Day,Month,Year,Hour,Min,Sec]);
end;

function TAxisCalDisplay.StdDateString: String;
var
ST : TSystemTime;
Month,Day,Year : String;
begin
GetLocalTime(ST);
Year := DDigit(St.wYear);
Month := DDigit(St.wMonth);
Day := DDigit(St.wDay);
Result:= Format('%s/%s/%s',[Day,Month,Year]);
end;

function TAxisCalDisplay.StdTimeString: String;
var
ST : TSystemTime;
Hour,Min,Sec : String;
begin
GetLocalTime(ST);
Hour := DDigit(St.wHour);
Min := DDigit(St.wMinute);
Sec := DDigit(St.wSecond);
Result:= Format('%s:%s:%s',[Hour,Min,Sec]);
end;


// This performed for new machine after comp is complete in order to fully register machines initial comp and create an archive
procedure TAxisCalDisplay.RequestFirstReg(Sender: TObject);
var
ArFolder : String;
begin
WriteConfigStringParam('CompFullyRegistered','True');
WriteConfigStringParam('RegDate',DisplayableDateTimeString);
CalRegisteredTag.AsInteger := 1;
ArFolder := ReadConfigString('LastArchiveFolder');
WriteConfigIntegerParam('FirstRegPending',1);
WriteConfigStringParam('RegisteredArchive',ArFolder);
AxisList.Availability.WorkingMode := cpuFullyRegistered;
Archiver.AddLogEntry(Format('Compensation Registered and Archived to %s',[ArFolder]),True,ArFolder);
AxisList.Availability.WorkingMode := cpuRegisteredButNotRestarted;
//UpdateMessage;
MessageDlg('Compensation Registration Complete', mtInformation, [mbOK], 0);
AddMessage('Compensation Registration Complete', False);
AddMessage('System should be restared',False);
end;


procedure TAxisCalDisplay.DoWriteRequest(Sender: TObject);
var
ArFolder : String;
begin
  if MessageDlg('Writing a new calibration file will require that the system is restarted before the data takes effect. Are you sure you wish to continue?', mtConfirmation, [mbOK,MbCancel], 0) = mrOk then
    begin
    // Create a new archive directory and store current version of comp.
    ArFolder := Archiver.CreateArchiveAndSaveCurrentComp(CurrentCompFile);
    if DirectoryExists(ArFolder) then
      begin
      // Now replace existing Comp File with new on and set Update Tag;
      if DeleteFile(CurrentCompFile) then
        begin
        try
        OutputMemo.Lines.SaveToFile(CurrentCompFile);
        // Now archive New File and InputData
         if Archiver.DoFullArchiveToDirectory(ArFolder,CurrentCompFile,CompDataFilePath) then
            begin
            //Save last comp directory to cofig file
            WriteConfigStringParam('LastArchiveFolder',ArFolder);
            NewCalibrationTag.AsInteger := 1;
            WriteConfigIntegerParam('NewCompPending',1);
            if not (AxisList.Availability.PowerUpMode = cpuFullyRegistered) then
              begin
              FirstFullRegButton.Visible := True;
              MessageDlg('First Compensation File has been created. If Compensation complete, then press ''Register Compenstaion'' Button to register', mtInformation, [mbOK], 0);
              end
            else
              begin
              MessageDlg('New Compensation File Created and registered. ACNC should now be restarted', mtInformation, [mbOK], 0);
              WriteConfigIntegerParam('NewCompRestartPending',1); // react to this on next power up to RE Register Comp
              ArFolder := ReadConfigString('LastArchiveFolder');
              WriteConfigStringParam('RegisteredArchive',ArFolder);
              WriteConfigStringParam('RegDate',DisplayableDateTimeString);
              end;
            end
          else
            begin
            Archiver.RestorePreviousCompFileFrom(ArFolder,CurrentCompFile);
            MessageDlg('An error occured. Compensation File has not been updated', mtInformation, [mbOK], 0);
            end
         except
         end
        end;
      end;
    end
   else
    begin
      MessageDlg('No action taken', mtInformation, [mbOK], 0);
    end;
end;

procedure TAxisCalDisplay.RequestFullArchive(Sender: TObject);
begin
Archiver.DoFullArchive(CurrentCompFile,CompDataFilePath)
end;

procedure TAxisCalDisplay.RequestAdjustFileLoad(Sender: TObject);
var
AMess : String;
RenError : TRenLoadErrors;
FileValid : Boolean;
begin
 RenOpenDialogue := TOpenDialog.Create(nil);
 RenishawImporter := TRenishawImporter.Create;
 RenishawImporter.CheckFileValidity := CheckFileValidity ;
 try
 with RenOpenDialogue do
  begin
  InitialDir := RenishawImportPath;
  options :=  [ofReadOnly,ofHideReadOnly,ofNoChangeDir,ofEnableSizing];
  end;
  if RenOPenDialogue.execute then
    begin
    If (RenOPenDialogue.FileName <> '') and FileExists(RenOPenDialogue.FileName) then
      begin
      if CheckFileValidity then
        begin
        FileValid := RenishawImporter.CheckFileValidForAxisCalled(RenOPenDialogue.FileName,CurrentAxisData.AxisName);
        end
      else
        begin
        FileValid := True;
        end;
      If FileValid then
        begin
        RenError := RenishawImporter.LoadAsAdjustToAxisAndGrid(RenOPenDialogue.Filename,AxisList.EntryWithAxisName(CurrentAxisData.AxisName),ManDataGrid);
        if RenError = rlOK then
          begin
          InitialiseDisplayForAxis(CurrentAxisData,False,False);
          MBAdJustLabel.Visible := True;
          ManAdjustDisplay.Visible := True;
          MergeAdjustButton.Visible := True;
          ManDataGrid.LoadFromLoadBufferandClean;
          ManAdjustDisplay.Text := IntToStr(CurrentAxisData.CompensationDataLists.ManualBackLashAdjust);
          StoreDisplayStatusToSnapshot(CurrentAxisData.CurrentStatusBuffer);
          AMess := Format('Adjustment File  %s Loaded for %s Axis',[ExtractFilename(RenOPenDialogue.Filename),CurrentAxisData.AxisName]);
          Archiver.AddLogEntry(AMess,False,'');
          ConditionSubmitButton;
          end
        else
          begin
          case RenError of
            rlWtongAxis         : MessageDlg('Adjust File is for Wrong Axis', mtInformation, [mbOK], 0);
            rlModeMismatch      : MessageDlg('Adjust File Backlash Mode Mismatch', mtInformation, [mbOK], 0);
            rlBadFile           : MessageDlg('Invalid File. Check Format', mtInformation, [mbOK], 0);
            rlNoFile            : MessageDlg('Invalid File. Check Format', mtInformation, [mbOK], 0);
            rlTooShort          : MessageDlg('Invalid File. Check Format', mtInformation, [mbOK], 0);
            rlStartMisMatch     : MessageDlg('Adjust File Start Position Does not match current Data', mtInformation, [mbOK], 0);
            rlSpacingMismatch   : MessageDlg('Adjust File Spacing does not match current Data', mtInformation, [mbOK], 0);
            rlNumEntriesMismatch: MessageDlg('Adjust File entry count does not match Current Data', mtInformation, [mbOK], 0);
            end;
          end;
        end
      else
        begin
        MessageDlg(Format('Invalid File for %s axis',[CurrentAxisData.AxisName]), mtInformation, [mbOK], 0);
        end;
      end
    else
     begin
     MessageDlg('Invalid File Name', mtInformation, [mbOK], 0);
     end;
    end;
 finally
 RenOpenDialogue.Free;
 RenishawImporter.Free;
 end;

end;
procedure TAxisCalDisplay.StoreDisplayStatusToSnapshot(Snap : TSingleAxisSnapshot);
begin
ManDataGrid.BufferContentsToSnapShot(Snap);
CurrentAxisData.BufferStatusTo(Snap.ParameterBuffer);
end;

procedure TAxisCalDisplay.RequestFileLoad(Sender: TObject);
var
AMess : String;
NumEntries : Integer;
FileValid : Boolean;
begin
 RenOpenDialogue := TOpenDialog.Create(nil);
 RenishawImporter := TRenishawImporter.Create;
 RenishawImporter.CheckFileValidity := CheckFileValidity;
 try
 with RenOpenDialogue do
  begin
  InitialDir := RenishawImportPath;
  options :=  [ofReadOnly,ofHideReadOnly,ofNoChangeDir,ofEnableSizing];
  end;
  if RenOPenDialogue.execute then
    begin
    If (RenOPenDialogue.FileName <> '') and FileExists(RenOPenDialogue.FileName) then
      begin
      if CheckFileValidity then
        begin
        FileValid := RenishawImporter.CheckFileValidForAxisCalled(RenOPenDialogue.FileName,CurrentAxisData.AxisName);
        end
      else
        begin
        FileValid := True;
        end;
      If FileValid then
        begin
        if RenishawImporter.LoadFromFileToAxisAndGrid(RenOPenDialogue.Filename,AxisList.EntryWithAxisName(CurrentAxisData.AxisName),ManDataGrid) = rlOK then
          begin
          InitialiseDisplayForAxis(CurrentAxisData,False,False);
          ManDataGrid.LoadFromLoadBufferandClean;
          StoreDisplayStatusToSnapshot(CurrentAxisData.CurrentStatusBuffer);
          AMess := Format('Imported File %s for %s Axis',[ExtractFilename(RenOPenDialogue.Filename),CurrentAxisData.AxisName]);
          Archiver.AddLogEntry(AMess,False,'');
          if not CurrentAxisData.CompensationDataLists.AxisIsLinear then
            begin
            // check data is valid for continuous rotary
            NumEntries := TestCREntriesValid;
            if NumEntries <> CurrentAxisData.CompensationDataLists.NumberOfEntries then
              begin
              MessageDlg('Input file is not valid for a continuous 360 degree axis', mtInformation, [mbOK], 0);
              end;
            end
          end
        else
          begin
          MessageDlg('Load Failed. Please check File validity', mtInformation, [mbOK], 0);
          end;
        end
      else
        begin
        MessageDlg(Format('Invalid File for %s axis',[CurrentAxisData.AxisName]), mtInformation, [mbOK], 0);
        end;
      end
    else
     begin
     MessageDlg('Invalid File Name', mtInformation, [mbOK], 0);
     end;
    end;

 finally
 RenOpenDialogue.Free;
 RenishawImporter.Free;
 end;
 ConditionSubmitButton;
 end;

procedure TAxisCalDisplay.DoMergeandSaveRequest(Sender: TObject);
begin

if ManDataGrid.AdjustMode then
  begin
  ManDataGrid.MergeAndCollapse;
  ConditionSubmitButton;
//  DoRegister(Nil)
  end;
end;

procedure TAxisCalDisplay.DoAdjustRequest(Sender: TObject);
begin
 if ManDataGrid.AdjustMode then
  begin
  ManDataGrid.MergeAndCollapse;
  end;
 if assigned(CurrentAxisData) then
    begin
    CurrentAxisData.CompensationDataLists.ManualBacklashMicron := CurrentAxisData.CompensationDataLists.ManualBacklashMicron+CurrentAxisData.CompensationDataLists.ManualBacklashAdjust;
    CurrentAxisData.CompensationDataLists.ManualBacklashAdjust := 0;
    MBAdJustLabel.Visible := False;
    ManAdjustDisplay.Visible := False;
    ManBackEdit.Text := Format('%d',[CurrentAxisData.CompensationDataLists.ManualBacklashMicron]);
    end;
 AbandonAdjustMode(Nil);
 ConditionSubmitButton;

end;

procedure TAxisCalDisplay.TableAdjustModeRequest(Sender: TObject);
begin
ManDataGrid.SetFocus;
ManDataGrid.AdjustMode := True;
LoadAdjustFileButton.Visible := True;
MergeAdjustButton.Visible := True;
LoadFileButton.Visible := False;
MBAdJustLabel.Visible := True;
ManAdjustDisplay.Visible := True;
with AdjustModeButton do
  begin
  Caption := 'Abandon Adjust';
  OnClick := AbandonAdjustMode;
  end
end;



procedure TAxisCalDisplay.AbandonAdjustMode(Sender: TObject);
begin
ManDataGrid.AdjustMode := False;
with AdjustModeButton do
  begin
  Caption := 'Adjust Mode';
  OnClick := TableAdjustModeRequest;
  end;
LoadAdjustFileButton.Visible := False;
LoadFileButton.Visible := True;
MergeAdjustButton.Visible := False;
MBAdJustLabel.Visible := False;
ManAdjustDisplay.Visible := False;
ConditionSubmitButton;
end;

procedure TAxisCalDisplay.MakeOutputFile(Sender: TObject);
var
TextGen : TCalTextGenerator;
DateStr : String;
TimeStr : String;
begin
OutputMemo.Clear;
if AxisList.Count > 0 then
  begin
  TextGen := TCalTextGenerator.Create;
  try
  DateStr := StdDateString;
  TimeStr := StdTimeString;
  MachineIdStr := MachineIdTag.AsString;
  TextGen.ClearAndMakeHeader(AxisList,MachineType,MachineIdStr,CustomerId,DateStr,TimeStr);
  TextGen.MakeCompText(AxisList);
  TextGen.MakeBacklashText(AxisList);
  If TextGen.Count > 0 then
    begin
    OutputMemo.Lines := TextGen;
    WriteCompFileButton.Enabled := True;
    end;
  finally
  TextGen.Free;
  end;
  end;
end;


{ TCalEditBox }

constructor TCalEditBox.Create(AOwner: TComponent);
begin
  inherited;
  IdTag := -1;
  OnEnter := EditEntered;
  OnExit := EditExited;
  OnKeyDown := EditKeyDown;
  Width := 95;
end;

procedure TCalEditBox.EditExited(Sender: TObject);
begin
  if Text <> EntryText then
    begin
    ExitText := Text;
    if assigned(FHasChanged) then
      begin
      try
      DoubleValid := True;
      FValAsDouble := StrToFloat(ExitText);
      except
      FValASDouble := 0;
      DoubleValid := False;
      end;

      try
      IntegerValid := True;
      FValAsInteger := StrToInt(ExitText);
      except
      FValAsInteger := 0;
      IntegerValid := False;
      Text := EntryText;
      ExitText := Text;
      end;
      end;
    if assigned(FHasChanged) then FHasChanged(Self);
    end
end;

procedure TCalEditBox.EditEntered(Sender: TObject);
begin
   EntryText := Text;
end;

procedure TCalEditBox.EditKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
if key = 13 then
  begin

  if Text <> EntryText then
    begin
    ExitText := Text;
    if assigned(FHasChanged) then
      begin
      try
      DoubleValid := True;
      ValAsDouble := StrToFloat(ExitText);
      except
      ValASDouble := 0;
      DoubleValid := False;
      end;
      try
      IntegerValid := True;
      ValAsInteger := StrToInt(ExitText);
      except
      IntegerValid := False;
      if DoubleValid = False then
        begin
        Text := EntryText;
        ExitText := EntryText;
        exit;
        end;
      end;
      end;
    if assigned(HasChanged) then HasChanged(Self);
    EntryText := ExitText;
    end;
  end;
end;

procedure TAxisCalDisplay.CalEditChange(Sender: TObject);
var
FirstPos : Double;
Increment : Double;
LastPos : Double;
NumEntries : Integer;
begin
if not assigned(CurrentAxisData) then exit;
With Sender  do
  begin
  case TCalEditBox(Sender).IdTag of
    0:  //CPMEdit
    begin
    if TCalEditBox(Sender).DoubleValid then
      begin
      CurrentAxisData.CompensationDataLists.aaEncoderCountsPerUnit := TCalEditBox(Sender).ValAsDouble;
      AxisList.Availability.DataForAxisNameChanged(CurrentAxisData.AxisName);
      end
    else
      begin
      TCalEditBox(Sender).Text := TCalEditBox(Sender).TextOnEntry;
      end;
    end;

    1: // HomeOffsetEdit
    begin
    if TCalEditBox(Sender).DoubleValid then
      begin
      CurrentAxisData.CompensationDataLists.CompHomeOffset := TCalEditBox(Sender).ValAsDouble;
      AxisList.Availability.DataForAxisNameChanged(CurrentAxisData.AxisName);
      end
    else
      begin
      TCalEditBox(Sender).Text := TCalEditBox(Sender).TextOnEntry;
      end;
    end;

    2: //PosLimitEdit
    begin
    if TCalEditBox(Sender).DoubleValid then
      begin
//      CurrentAxisData.CompensationDataLists.CompPositiveLimit := TCalEditBox(Sender).ValAsDouble;
//      AxisList.Availability.DataForAxisNameChanged(CurrentAxisData.AxisName);
      end
    else
      begin
//      TCalEditBox(Sender).Text := TCalEditBox(Sender).TextOnEntry;
      end;
    end;


    4: // FirstCompEdit
    begin
    if TCalEditBox(Sender).DoubleValid then
      begin
      CurrentAxisData.CompensationDataLists.FirstPosition:= TCalEditBox(Sender).ValAsDouble;
      if not CurrentAxisData.CompensationDataLists.AxisIsLinear then
          begin
          NumEntries := TestCREntriesValid;
          if NumEntries > 0 then
            begin
            NumEntriesEdit.Text := IntToStr(NumEntries);
            CurrentAxisData.CompensationDataLists.NumberOfEntries := NumEntries;
            end;
          end;
      AxisList.Availability.DataForAxisNameChanged(CurrentAxisData.AxisName);
      TableInitRequired(True);
      end
    else
      begin
      TCalEditBox(Sender).Text := TCalEditBox(Sender).TextOnEntry;
      end;
    with CurrentAxisData.CompensationDataLists do
      begin
      FirstPosition := TCalEditBox(Sender).ValAsDouble;
//      CompPositiveLimit := FirstPosition+(CompIncrement*(NumberOfEntries-1));
//      CompNegativeLimit := FirstPosition;
      LastCompValue.Caption := Format('%5.3f',[CompPositiveLimit]);
//      NegLimitEdit.Text := Format('%5.3f',[CompNegativeLimit]);
      end;
    end;

    5: //CompIncrEdit
    begin
    if TCalEditBox(Sender).DoubleValid then
      begin
      CurrentAxisData.CompensationDataLists.CompIncrement := TCalEditBox(Sender).ValAsDouble;
      if not CurrentAxisData.CompensationDataLists.AxisIsLinear then
          begin
          NumEntries := TestCREntriesValid;
          if NumEntries > 0 then
            begin
            NumEntriesEdit.Text := IntToStr(NumEntries);
            CurrentAxisData.CompensationDataLists.NumberOfEntries := NumEntries;
            end;
          end;
      AxisList.Availability.DataForAxisNameChanged(CurrentAxisData.AxisName);
      TableInitRequired(True);
      end
    else
      begin
      TCalEditBox(Sender).Text := TCalEditBox(Sender).TextOnEntry;
      end;
    with CurrentAxisData.CompensationDataLists do
      begin
//      CompPositiveLimit := FirstPosition+(CompIncrement*(NumberOfEntries-1));
//      CompNegativeLimit := FirstPosition;
      LastCompValue.Caption := Format('%5.3f',[CompPositiveLimit]);
//      NegLimitEdit.Text := Format('%5.3f',[CompNegativeLimit]);
      end;
    end;

    6: //NumEntriesEdit
    begin
    if TCalEditBox(Sender).IntegerValid then
      begin
      CurrentAxisData.CompensationDataLists.NumberOfEntries := TCalEditBox(Sender).ValAsInteger;
      AxisList.Availability.DataForAxisNameChanged(CurrentAxisData.AxisName);
      TableInitRequired(True);
      end
    else
      begin
      TCalEditBox(Sender).Text := TCalEditBox(Sender).TextOnEntry;
      end;
    with CurrentAxisData.CompensationDataLists do
      begin
//      CompPositiveLimit := FirstPosition+(CompIncrement*(NumberOfEntries-1));
//      CompNegativeLimit := FirstPosition;
      LastCompValue.Caption := Format('%5.3f',[CompPositiveLimit]);
//      NegLimitEdit.Text := Format('%5.3f',[CompNegativeLimit]);
      end;
    end;

    7: //ManBackEdit
    begin
    if TCalEditBox(Sender).DoubleValid then
      begin
      CurrentAxisData.CompensationDataLists.ManualBacklashMicron := TCalEditBox(Sender).ValAsInteger;
      AxisList.Availability.DataForAxisNameChanged(CurrentAxisData.AxisName);
      end
    else
      begin
      TCalEditBox(Sender).Text := TCalEditBox(Sender).TextOnEntry;
      end;
    end;
  end; // case
  end;

ConditionSubmitButton;
end;

procedure TAxisCalDisplay.DoLazyUpdate(Sender: TObject);
begin
{ LazyUpdateTimer.Enabled := False;
 if not DataUpdating then
  begin
  AxisList.KickLazyUpdate(False);
  end;}
end;

(*procedure TAxisCalDisplay.UpdateStatusFromAxisList;
var
AxEntry : TSingleAxisData;
AxNum : Integer;
AxName : String;
CResult : TCompFileCompareResults;
BLResult : TCompFileCompareResults;
AllAxesOK : Boolean;
begin
AllAxesOK := True;
case AxisList.Availability.WorkingMode of
  cpuFullyRegistered:
    begin
    // Here to check current Input Data matches Registered
    end;

//  cpuFirstStartWorkInProgress:
  cpuNonBlankFirstStart:
    begin
    case AxisList.Availability.FirstPowerUpMode of
      tfmUnresolved :
        begin
        // Here whilst Doing on mnachine Comp pre-registration
        end;

      tfmFromScratch :
        begin
        // Here whilst Doing on mnachine Comp pre-registration
        end;

      tfmFromImport:
        begin
        if assigned(CompParser) then
          begin
          if assigned(CompParser.ExistingCompList) then
            begin
            if AxisList.Count > 0 then
              begin
              for AxNum := 0 to AxisList.Count-1 do
                begin
                AxEntry := AxisList[AxNum];
                AxName := AxEntry.AxisName;
                // this is comparison of actual Comp Files vs what would be made by input data
                CResult := CompParser.ExistingCompList.CompResultsForAxisCalled(AxName);
                BLResult := CompParser.ExistingCompList.BLCompResultsForAxisCalled(AxName);
                if assigned(CResult) then
                  begin
                  CalStatusGrid.UpdateEntryFromCompResult(AxName,CResult);
                  if (CResult.AllOK) and (Axentry.CompensationDataLists.BacklashUsed) then
                    begin
                    CalStatusGrid.UpdateEntryFromCompResult(AxName,BLResult);
                    if not BLResult.AllOK then
                      AllAxesOK := False;
                    end
                  else
                    begin
                    AllAxesOK := False;
                    end
                  end
                else
                  begin
                  AllAxesOK := False;
                  end
                end;
              end;
            end;
          end;
          if AllAxesOK then
            begin
            StatusButton.Font.Color := clWebDarkOliveGreen;
            end
          else
            begin
            StatusButton.Font.Color := clWebRosyBrown;
            end;
        end;
      end ; //Inner Case
    end;
  end; // case


end;*)
function TAxisCalDisplay.UpdateStatusFromAxisList(ReportErrorToLog : Boolean): Boolean;
var
AxEntry : TSingleAxisData;
AxNum : Integer;
AxName : String;
CResult : TCompFileCompareResults;
BLResult : TCompFileCompareResults;
AllAxesOK : Boolean;
begin
Result := False;
AllAxesOK := True;
if assigned(CompParser) then
  begin
  if assigned(CompParser.ExistingCompList) then
    begin
    if AxisList.Count > 0 then
      begin
      for AxNum := 0 to AxisList.Count-1 do
        begin
        AxEntry := AxisList[AxNum];
        AxName := AxEntry.AxisName;
        CResult := CompParser.ExistingCompList.CompResultsForAxisCalled(AxName);
        BLResult := CompParser.ExistingCompList.BLCompResultsForAxisCalled(AxName);
        if assigned(CResult) then
          begin
          CalStatusGrid.UpdateEntryFromCompResult(AxName,CResult);
          if not (CResult.AllOK) then Archiver.AddLogEntry(Format('Error In %s Forward CompData at Power up',[AxName]),False,'');
          if (CResult.AllOK) and (Axentry.CompensationDataLists.BacklashUsed) then
            begin
            CalStatusGrid.UpdateEntryFromCompResult(AxName,BLResult);
            if not BLResult.AllOK then
              begin
              AllAxesOK := False;
              if ReportErrorToLog then
                  Archiver.AddLogEntry(Format('Error In %s Reverse CompData at Power up',[AxName]),False,'');
              end
            end
          end
        else
          begin
          AllAxesOK := False;
          end
        end;
      end;
    end;
  end;
  if AllAxesOK then
    begin
    StatusButton.Font.Color := clWebDarkOliveGreen;
    Result := True;
    end
  else
    begin
    StatusButton.Font.Color := clWebRosyBrown;
    Result := False;
    end;
end;

(*procedure TAxisCalDisplay.UpdateStatusFromAxisList;
var
AxEntry : TSingleAxisData;
AxNum : Integer;
AxName : String;
CResult : TCompFileCompareResults;
BLResult : TCompFileCompareResults;
AllAxesOK : Boolean;
begin
AllAxesOK := True;
case AxisList.Availability.WorkingMode of
  cpuFullyRegistered:
    begin
    // Here to check current Input Data matches Registered
    end;

  cpuFirstStartWorkInProgress:
    begin
    case AxisList.Availability.FirstPowerUpMode of
      cpuInitialise :
        begin
        // Here whilst Doing on mnachine Comp pre-registration
        end;

      cpuInitialImport,cpuNonBlankFirstStart :
        begin
        if assigned(CompParser) then
          begin
          if assigned(CompParser.ExistingCompList) then
            begin
            if AxisList.Count > 0 then
              begin
              for AxNum := 0 to AxisList.Count-1 do
                begin
                AxEntry := AxisList[AxNum];
                AxName := AxEntry.AxisName;
                // this is comparison of actual Comp Files vs what would be made by input data
                CResult := CompParser.ExistingCompList.CompResultsForAxisCalled(AxName);
                BLResult := CompParser.ExistingCompList.BLCompResultsForAxisCalled(AxName);
                if assigned(CResult) then
                  begin
                  CalStatusGrid.UpdateEntryFromCompResult(AxName,CResult);
                  if (CResult.AllOK) and (Axentry.CompensationDataLists.BacklashUsed) then
                    begin
                    CalStatusGrid.UpdateEntryFromCompResult(AxName,BLResult);
                    if not BLResult.AllOK then
                      AllAxesOK := False;
                    end
                  else
                    begin
                    AllAxesOK := False;
                    end
                  end
                else
                  begin
                  AllAxesOK := False;
                  end
                end;
              end;
            end;
          end;
          if AllAxesOK then
            begin
            StatusButton.Font.Color := clWebDarkOliveGreen;
            end
          else
            begin
            StatusButton.Font.Color := clWebRosyBrown;
            end;
        end;
      end ; //Inner Case
    end;
  end; // case


end;
*)
procedure TAxisCalDisplay.ShowEditMemo(EditMess1 : String);
begin
AxisStatusPanel.Caption := EditMess1;
end;



procedure TAxisCalDisplay.DoSubmit(Sender: TObject);
begin
// here to transfer grid to current input data and process

if assigned(CurrentAxisData) then
 begin
 if ManDataGrid.AdjustMode then
  begin
  ManDataGrid.MergeAndCollapse;
  end;
 if (ManDataGrid.RowCount-1) <>  CurrentAxisData.CompensationDataLists.NumberOfEntries then
  begin
  MessageDlg('Number of Entries does not match Table. Table Must be initialised', mtInformation, [mbOK], 0);
  exit;
  end;


 if  ManDataGrid.SaveToDataList(CurrentAxisData.CompensationDataLists) then
    begin
//    EditMemo.Visible := False;
    CalStatusGrid.aaUpdateFromAxisList(AxisList);
    if CurrentAxisData.CompensationDataLists.CurrentInputDataList.UpdateShiftedDataForHomeOffset(CurrentAxisData.CompensationDataLists.CompHomeOffset) then
         begin
         CurrentAxisData.CompensationDataLists.OutputDataList.UpdateFromInputDataList(CurrentAxisData.CompensationDataLists.CurrentInputDataList);
         InternalCompare;
         if not UpdateStatusFromAxisList(False) then
          begin
          Archiver.AddLogEntry(Format('Data For %s Axis Altered and submitted',[CurrentAxisData.AxisName]),False,'');
          end;
         CurrentAxisData.WriteToWorkingFile(CompDataFilePath);
         StoreDisplayStatusToSnapshot(CurrentAxisData.WorkingFileStatusBuffer);
         StoreDisplayStatusToSnapshot(CurrentAxisData.CurrentStatusBuffer);
//         ManDataGrid.BufferContentsToSnapShot(CurrentAxisData.WorkingFileStatusBuffer); //TBD and parameters!!! full snapshot
         ConditionSubmitButton;
         end
      else
        begin
        MessageDlg('HomeOffset Is Outside data range. Can Not Submit', mtInformation, [mbOK], 0);
        end;
    end
 else
    begin
    ShowEditMemo('Data not Valid');
    end
 end;
end;

procedure TAxisCalDisplay.DoRevert(Sender: TObject);
begin
// here to transfer grid to current inout data and process
if assigned(CurrentAxisData) then
 begin
 CurrentAxisData.RevertToSnapShot(CurrentAxisData.WorkingFileStatusBuffer)
 end;
 InitialiseDisplayForAxis(CurrentAxisData,False,False);
 TableInitRequired(False);
 ConditionSubmitButton;
end;

procedure TAxisCalDisplay.CreateDefaultWorkingFile(MacId : String);
var
AxEntry : TSingleAxisData;
AxNum : Integer;
CFile : TIniFile;
begin
MessageMemo.Clear;
if AxisList.Count > 0 then
  begin
  for AxNum := 0 to AxisList.Count-1 do
    begin
    AxEntry := AxisList[AxNum];
    AxEntry.WriteToWorkingFile(CompDataFilePath);
    end;
  end;
   CurrentAxisData:= nil;
   AxisList.LoadFromDataFile(CompDataFilePath);
   if AxisList.Availability.IsSatisfied then
      begin
      If AxisList.Count > 0 then
        begin
        AxisSelect.Items.Clear;
        For AxNum := 0 to AxisList.Count-1 do
          begin
          AxEntry := AxisList[Axnum];
          AxisSelect.Items.Add(AxEntry.AxisName);
          end;
          AxisSelect.Text := AxisSelect.Items[0];
          AxisSelected(nil);
         end;
      end;
   if FileExists(CompDataFilePath) then
     begin
     AxisList.Availability.SetWorkingFileMacId(CompDataFilePath,MacId);
     end;
   DefaultFileCreated := True;
end;


procedure TAxisCalDisplay.LoadImportData;
var
AxEntry : TSingleAxisData;
AxNum : Integer;
begin
MessageMemo.Clear;
if AxisList.Count > 0 then
  begin
  If not FileExists(CompDataFilePath) then
    begin
    for AxNum := 0 to AxisList.Count-1 do
      begin
      AxEntry := AxisList[AxNum];
      AxEntry.WriteToWorkingFile(CompDataFilePath);
      end;
    end;
  end;
   CurrentAxisData := nil;
   AxisList.LoadFromDataFile(CompDataFilePath);
   If AxisList.Availability.IsSatisfied then
     begin
     AxisList.Availability.WorkingFileLoaded := True;
     AddMessage('Working File created',False);
//     WriteConfigIntegerParam('ImportStage',1);
     end
   else
     begin
     AddMessage('Working File created but not all data available',False);
//     WriteConfigIntegerParam('ImportStage',-1);
     end;
    CalStatusGrid.aaUpdateFromAxisList(AxisList);
    InternalCompare;
    AxisList.Availability.ImportloadedAndMatched;
    UpdateStatusFromAxisList(False);
//    ConfirmStatusButton.Visible := False;

   if AxisList.Availability.IsSatisfied then
      begin
      If AxisList.Count > 0 then
        begin
        AxisSelect.Items.Clear;
        For AxNum := 0 to AxisList.Count-1 do
          begin
          AxEntry := AxisList[Axnum];
          AxisSelect.Items.Add(AxEntry.AxisName);
          end;
          AxisSelect.Text := AxisSelect.Items[0];
          AxisSelected(nil);
         end;
      end;
end;


procedure TAxisCalDisplay.DoImportClick(Sender: TObject);
begin
  if MessageDlg('If Import Files exist please press OK otherwise press cancel', mtConfirmation, [mbOK,mbCancel], 0)= mrOk then
    begin

    end
  else
    begin
    end
end;

procedure TAxisCalDisplay.DoClearCompClick(Sender: TObject);
begin
  if MessageDlg('Are you sure you want to delete the current comp file and perform complete axis comp from start?', mtConfirmation, [mbOK,mbCancel], 0)= mrOk then
    begin

    end
  else
    begin

    end
end;



procedure TAxisCalDisplay.DoSatusReport(Sender: TObject);
var
CResult : TCompFileCompareResults;
BLResult : TCompFileCompareResults;
AxNum : Integer;
AxEntry : TSingleAxisData;
AxName : String;
Found : Boolean;
AxData : TAvailabilityAndStatusRecord;
MessList : TStringList;
DiffNum : INteger;
FoundArchive : Boolean;
begin
StatusMessage.Clear;
if AxisList.Availability.PowerUpMode = cpuFullyRegistered then
  begin
  MessList := TStringList.Create;
  try
  AxisList.CompareToArchive(RegisteredArchiveFolder,True,False,FoundArchive);
  if not FoundArchive then
    begin
    StatusMessage.Lines.Add('Can Not Find Archive For Comparison');
    exit;
    end;
  for AxNum := 0 to AxisList.Count-1 do
    begin
    AxEntry := AxisList[AxNum];
    AxName := AxEntry.AxisName;
    StatusMessage.Lines.Add(Format('----Axis %s ----- ',[AxName]));
    if Axentry.CompareStatusBuffer.DiffList.Count > 0 then
      begin
      For DiffNum := 0 to Axentry.CompareStatusBuffer.DiffList.Count-1 do
        begin
        StatusMessage.Lines.Add(Axentry.CompareStatusBuffer.DiffList[DiffNum]);
        end;
      end
    else
      begin
      StatusMessage.Lines.Add('Input Settiungs Match');
      end;
    AxData := AxisList.Availability.DataForAxisName(AxName,Found);
    if Found then
      begin
      If not AxData.RegisteredDataMatch then
        begin
        StatusMessage.Lines.Add('Compenstation Data Mismatch');
        end
      else
        begin
        StatusMessage.Lines.Add('Compenstation Data Matches');
        end;
      end;
    StatusMessage.Lines.Add('');
    end;
  finally
  MessList.Free;
  end;
  end
else
  begin
  if assigned(CompParser) then
    begin
    if assigned(CompParser.ExistingCompList) then
      begin
      if AxisList.Count > 0 then
        begin
        for AxNum := 0 to AxisList.Count-1 do
          begin
          AxEntry := AxisList[AxNum];
          AxName := AxEntry.AxisName;
          if AxisList.Availability.DataExistsForAxisWithIndex(AxNum) then
            begin
            if not CompParser.CompDataForAxisNameExists(Axname) then
                begin
                StatusMessage.Lines.Add(Format('----Axis %s ----- ',[AxName]));
                StatusMessage.Lines.Add(' NO COMPENSATION ');
                end
            else
                begin
                CResult := CompParser.ExistingCompList.CompResultsForAxisCalled(AxName);
                if CResult.AllOK then
                  begin
                  StatusMessage.Lines.Add(Format('----Axis %s ----- Status GOOD',[AxName]));
                  end
                else
                  begin
                  StatusMessage.Lines.Add(Format('----Axis %s ----- STATUS ERROR',[AxName]));
                  end;
                StatusMessage.Lines.Add('-- Comp Data --') ;
                Cresult.WriteAllStatusToStrings(StatusMessage.Lines);
                if Axentry.CompensationDataLists.BacklashUsed then
                  begin
                  StatusMessage.Lines.Add('-- BackLash Data --') ;
                  BLResult := CompParser.ExistingCompList.BLCompResultsForAxisCalled(AxName);
                  BLResult.WriteAllStatusToStrings(StatusMessage.Lines);
                  end;
                 end
                end
            else
              begin
              StatusMessage.Lines.Add(Format('No Input data for Axis %s',[AxName]));
              end;
            StatusMessage.Lines.Add('') ;
            StatusMessage.Lines.Add('') ;
            end;
          end; //if AxisList.Count > 0 then
      end; // if assigned(CompParser.ExistingCompList) then
    end; //if assigned CompParser
  end;

end;



procedure TAxisCalDisplay.MyCellDataChanged(ACol, ARow: Integer; StrVal: String; FloatVal: Double);
begin

if assigned(CurrentAxisData) then
  begin
  ManDataGrid.BufferContentsToSnapShot(CurrentAxisData.CurrentStatusBuffer);
  ConditionSubmitButton;
  end;
end;

procedure TAxisCalDisplay.GridDataMatches(Sender: TObject);
begin
if assigned(CurrentAxisData) then
  begin
  ManDataGrid.BufferContentsToSnapShot(CurrentAxisData.CurrentStatusBuffer);
  ConditionSubmitButton;
  end;
end;

procedure TAxisCalDisplay.CheckCompFileStatus(AxisList : TCalAxisList);
begin
  CompParser.CheckCompFileStatus(CurrentCompFile,AxisList.Availability,DefaultSettings.AxisListString);
 (* case AxisList.Availability.PowerUpMode of
    cpuUnknown,cpuBlankNoId,cpuNonBlankNoId:
      begin
      CompParser.CheckCompFileStatus(CurrentCompFile,AxisList.Availability,DefaultSettings.AxisListString);
      end;

    cpuInitialise:
      begin
      CompParser.CheckCompFileStatus(CurrentCompFile,AxisList.Availability,DefaultSettings.AxisListString);
      end;

    cpuInitialImport:
      begin
      CompParser.CheckCompFileStatus(CurrentCompFile,AxisList.Availability,DefaultSettings.AxisListString);
      end;

{    cpuFirstStartWorkInProgress:
      begin
      CompParser.CheckCompFileStatus(CurrentCompFile,AxisList.Availability,DefaultSettings.AxisListString);
      end;}

    cpuFullyRegistered:
      begin
      CompParser.CheckCompFileStatus(CurrentCompFile,AxisList.Availability,DefaultSettings.AxisListString);
      end;
    end; //case*)

end;

procedure TAxisCalDisplay.TableInitRequired(State: Boolean);
begin
FTableInitRequired := State;
if state then
  begin
//  ManDataGrid.EntryEnabled := False;
  InitInputTableButton.Enabled := True;
  end
else
  begin
//  ManDataGrid.EntryEnabled := False;
  InitInputTableButton.Enabled := True;
  end
end;




procedure TAxisCalDisplay.GetCompStatusFrom(ConfigPath: String;WorkingFileExists : Boolean);
var
Cfile : TIniFile;
Buffer : String;
Val : Integer;
begin
RegisteredArchiveFolder := '';
RegDisplayDate := '';
if FileExists(ConfigPath) then
  begin
  Cfile := TIniFile.Create(ConfigPath);
  try
  Buffer := Cfile.ReadString('General','CheckFileValidity','F');
  if (Buffer[1] = 'T') or (Buffer[1] = 't') then CheckFileValidity := True;
  if Not WorkingFileExists then
    begin
    Cfile.WriteInteger('Dynamic','FirstRegPending',0);
    Cfile.WriteString('Dynamic','CompFullyRegistered','False');
    Cfile.WriteString('Dynamic','UnRegisteredPowerUpMode', '');
    RegStatusPanel.Caption := 'Compensation Not Registered';
    AxisList.Availability.FirstRegPending := False;
    AxisList.Availability.FirstPowerUpMode :=tfmUnresolved;
    AxisList.Availability.PowerUpMode :=cpuDetermineState;
    AxisList.Availability.WorkingMode := cpuDetermineState;
    end
  else
    begin
    Buffer := CFile.ReadString('Dynamic','OriginalPowerUpMode','');
    if (Buffer = '') or (Buffer = 'FromBlankSystem') then
      begin
      AxisList.Availability.FirstPowerUpMode := tfmFromBlank;
      end
    else if Buffer = 'ImportUpgrade' then
      begin
      AxisList.Availability.FirstPowerUpMode :=tfmFromImport
      end
    else if Buffer = 'cpuInitialise' then // Backward compatibility only
      begin
      AxisList.Availability.FirstPowerUpMode :=tfmFromBlank
      end
    else
      begin
      AxisList.Availability.FirstPowerUpMode :=tfmUnresolved;
      end;

    Val := ReadConfigInteger('FirstRegPending');
    If Val = 1 then
      begin
      AxisList.Availability.FirstRegPending := True;
      FirstFullRegButton.Visible := True;
      end
    else
      begin
      AxisList.Availability.FirstRegPending := False;
      FirstFullRegButton.Visible := False;
      end;

    Buffer := CFile.ReadString('Dynamic','CompFullyRegistered','False');
     if (Buffer[1] = 't') or (Buffer[1] = 'T') then
      begin
      AxisList.Availability.PowerUpMode := cpuFullyRegistered;
      AxisList.Availability.WorkingMode := cpuFullyRegistered;
      RegisteredArchiveFolder :=Cfile.ReadString('Dynamic','RegisteredArchive','');
      RegDisplayDate :=Cfile.ReadString('Dynamic','RegDate','Unknown Date');
      RegStatusPanel.Caption := Format('Compensation Last Registered on  %s',[RegDisplayDate]);
      Cfile.WriteString('Dynamic','UnRegisteredPowerUpMode', 'Registered');
      CalRegisteredTag.AsInteger := 1;
      end
    else
      begin
      //HERE IF NOT FULLY REGISTERED
      CalRegisteredTag.AsInteger := 0;
      RegStatusPanel.Caption := 'Compensation Not Registered';
      Buffer := CFile.ReadString('Dynamic','UnRegisteredPowerUpMode', 'BlankCompFirstRun');
      if (Buffer = '') or (Buffer = 'FromBlankUnRegRestart')  then
        begin
        // Balnk Comp First Re-run
        InitialiseComp := True;
        AxisList.Availability.FirstPowerUpMode:= tfmFromBlank;
        CalRegisteredTag.AsInteger := 0;
        Cfile.WriteString('Dynamic','RegDate','');
        AxisList.Availability.PowerUpMode := cpuCalibrateRestartWorkInProgress;
        AxisList.Availability.WorkingMode := cpuCalibrateRestartWorkInProgress;
        end

      else if Buffer = 'NoIdFromBlankUnRegRestart'  then
        begin
        AxisList.Availability.FirstPowerUpMode:= tfmFromBlank;
        AxisList.Availability.PowerUpMode := cpuDetermineState;
        AxisList.Availability.WorkingMode := cpuDetermineState;
        end

      else if Buffer = 'ImportUnRegRestart' then
        begin
        // Import First Rerun
        AxisList.Availability.FirstPowerUpMode:= tfmFromImport;
        AxisList.Availability.PowerUpMode := cpuImportRestartWorkInProgress;
        AxisList.Availability.WorkingMode := cpuImportRestartWorkInProgress;
        end
      else if Buffer = 'NoIdImportUnRegRestart'  then
        begin
        AxisList.Availability.FirstPowerUpMode:= tfmUnresolved;
        AxisList.Availability.PowerUpMode := cpuDetermineState;
        AxisList.Availability.WorkingMode := cpuDetermineState;
        end
      else if Buffer = 'WIP'  then // Backward Compatibility
        begin
        AxisList.Availability.FirstPowerUpMode:= tfmUnresolved;
        AxisList.Availability.PowerUpMode := cpuDetermineState;
        AxisList.Availability.WorkingMode := cpuDetermineState;
        end
      else
        begin
        AxisList.Availability.PowerUpMode := cpuDetermineState;
        AxisList.Availability.FirstPowerUpMode:= tfmUnresolved;
        end;
      end
    end;
  finally
  CFile.Free;
  end;
  end;
end;

procedure TAxisCalDisplay.WriteConfigIntegerParam(Param : String; Value: Integer);
var
CFileName : String;
CFile : TIniFile;
begin
  CFileName := SlashSep(ConfigFilePath,'Calibration.cfg');
  if FileExists(CFileName) then
    begin
    Cfile := TIniFile.Create(CFileName);
    try
    CFile.WriteInteger('Dynamic',Param,Value);
    finally
    Cfile.Free;
    end;
    end;
end;

procedure TAxisCalDisplay.WriteConfigStringParam(Param, Value: String);
var
CFileName : String;
CFile : TIniFile;
begin
  CFileName := SlashSep(ConfigFilePath,'Calibration.cfg');
  if FileExists(CFileName) then
    begin
    Cfile := TIniFile.Create(CFileName);
    try
    CFile.WriteString('Dynamic',Param,Value);
    finally
    Cfile.Free;
    end;
    end;
end;


function TAxisCalDisplay.ReadConfigString(Param: String): String;
var
CFileName : String;
CFile : TIniFile;
begin
  CFileName := SlashSep(ConfigFilePath,'Calibration.cfg');
  if FileExists(CFileName) then
    begin
    Cfile := TIniFile.Create(CFileName);
    try
    Result := CFile.ReadString('Dynamic',Param,'');
    finally
    Cfile.Free;
    end;
    end;
end;



function TAxisCalDisplay.ReadConfigInteger(Param: String): Integer;
var
CFileName : String;
CFile : TIniFile;
begin
  CFileName := SlashSep(ConfigFilePath,'Calibration.cfg');
  if FileExists(CFileName) then
    begin
    Cfile := TIniFile.Create(CFileName);
    try
    Result := CFile.ReadInteger('Dynamic',Param,0);
    finally
    Cfile.Free;
    end;
    end;
end;

procedure TAxisCalDisplay.AddMessage(Mess: String;ClearFirst : Boolean);
begin
if ClearFirst then MessageList.Clear;
MessageList.Add(Mess);
UpdateMessageMemo;
end;

procedure TAxisCalDisplay.UpdateMessageMemo;
var
LNum : INteger;
begin
if ImportPanel.HasParent then
  begin
  MessageMemo.Clear;
  For Lnum := 0 to MessageList.Count-1 do
    begin
    MessageMemo.Lines.Add(MessageList[Lnum])
    end;
  end;
end;


procedure TAxisCalDisplay.UpdateOptionsPage;
var
FileId : String;

begin
MCIDDisplay.Text := MachineIdStr;
if MachineIdStr = '0000' then
  begin
  AddOPtionsMessage('Machine ID String has not been set. Comp Utility is disabled',True);
  end
else
  begin
  if AxisList.Availability.WorkingFileExists then
    begin
    if AxisList.Availability.WorkingMachineIDMatches(CompDataFilePath,MachineIdStr,FileId) then
      begin
//      CurrentStatus =
      end
    else
      begin
      end
    end
  end
end;


procedure TAxisCalDisplay.AddOPtionsMessage(Mess: String; ClearFirst: Boolean);
begin
if ClearFirst then StatusMessage.Lines.Clear;
StatusMessage.Lines.Add(Mess);
end;

function TAxisCalDisplay.WorkingFileIdMatch(IDStr: String): Boolean;
var
FStr : String;
WFile : TIniFile;
begin
Result := False;
If FileExists(CompDataFilePath) then
  begin
  WFile :=  TIniFile.Create(CompDataFilePath);
  FStr := WFile.ReadString('Dynamic','MachineID','0000');
  Result :=  FStr = IDStr;
  end;
end;

function TCalEditBox.GetValAsInteger: Integer;
begin
  try
  IntegerValid := True;
  Result := StrToInt(ExitText);
  FValAsInteger := Result;
  except
  Result := 0;
//FValAsInteger
  IntegerValid := False;
  end;
end;

function TCalEditBox.GetValAsDouble: Double;
begin
  try
  IntegerValid := True;
  Result := StrToFloat(Text);
  FValAsDouble := Result;
  except
  Result := 0;
  FValAsDouble := 0;
  DoubleValid := False;
  end;
end;

procedure TCalEditBox.SetValAsDouble(const Value: Double);
begin
  FValAsDouble := Value;
end;

procedure TCalEditBox.SetValAsInteger(const Value: Integer);
begin
  FValAsInteger := Value;
end;

initialization
  RegisterCNCClass(TAxisCalDisplay);
end.
