unit CalDataStatusGrid;

interface
uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls,IniFiles,Grids,CalDataLists,CompFileParser;
type

TCalDataStatusGrid = class(TStringGrid)
  private
      function RowIndexForAxisName(AxName: String) : Integer;
  public
      procedure aaUpdateFromAxisList(AList : TCalAxisList);
      function UpdateEntryFromCompResult(AxisName : String; CResult : TCompFileCompareResults):Boolean;
      constructor Create(AOwner: TComponent); override;
      destructor Destroy; override;
end;

TConditionedDataInDataGrid = class(TStringGrid)
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure UpdateFromAxisData(AxData : TSingleAxisData);
end;

TConditionedDataOutDataGrid = class(TStringGrid)
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure UpdateFromAxisData(AxData : TSingleAxisData);
end;

implementation

{ TCalDataStatusGrid }

constructor TCalDataStatusGrid.Create(AOwner: TComponent);
begin
  inherited;
  RowCount := 3;
  ColCount := 3;
  FixedRows := 1;
  FixedCols := 1;
  Width := 550;
  DefaultRowHeight := 20;
  Height := (6*DefaultRowHeight)+ 12;
  Font.Size := 10;
  Cells[0,0] := 'Axis';
  Cells[1,0] := 'Data Match';
  Cells[2,0] := 'Status';
  ColWidths[0] := 30;
  ColWidths[1] := 80;
  ColWidths[2] := Width-120;
end;

destructor TCalDataStatusGrid.Destroy;
begin

  inherited;
end;

function TCalDataStatusGrid.RowIndexForAxisName(AxName: String): Integer;
var
Found : Boolean;
RowNum : INteger;
begin
Result := -1;
Found := False;
RowNum := 1;
if RowCount > 1 then
  begin
  while (RowNum < RowCount) and (Not Found) do
    begin
    try
    Found := Cells[0,RowNum] = AxName;
     if Found then Result := RowNum;
    finally
    Inc(Rownum)
    end;
    end;
  end;
end;

Function TCalDataStatusGrid.UpdateEntryFromCompResult(AxisName: String; CResult: TCompFileCompareResults):Boolean;
var
RIndex : INteger;
begin
try
Result := True;
RIndex := RowIndexForAxisName(AxisName);
if Rindex > 0 then
  begin
  If CResult.AllOK then
    Cells[1,Rindex] := 'OK'
  else
    Cells[1,Rindex] := 'Mismatch';
  end;
except
Result := False;
end;
(*  if AxisName  = 'X' then
    begin
    if not CompExistence.XCompExists then
      if CResult.NumEntries.IsOK then
        begin
        Cells[1,1] := 'N/A';
        end
      else
        begin
        Cells[1,1] := 'Mismatch';
        end;
    end
  else  if AxisName  = 'Y' then
    begin
    if not CompExistence.YCompExists then
      if CResult.NumEntries.IsOK then
        begin
        Cells[1,2] := 'N/A';
        end
      else
        begin
        Cells[1,2] := 'Mismatch';
        end;
    end
  else  if AxisName  = 'Z' then
    begin
    if not CompExistence.ZCompExists then
      if CResult.NumEntries.IsOK then
        begin
        Cells[1,3] := 'N/A';
        end
      else
        begin
        Cells[1,3] := 'Mismatch';
        end;
    end
  else  if AxisName  = 'A' then
    begin
    if not CompExistence.ACompExists then Cells[1,4] := 'N/A';
    end
  else  if AxisName  = 'B' then
    begin
    if not CompExistence.BCompExists then Cells[1,5] := 'N/A';
    end
  else  if AxisName  = 'C' then
    begin
    if not CompExistence.CCompExists then Cells[1,6] := 'N/A';
    end*)
end;

function MessageFromState (State : TCompDataStates):String;
begin
Result := '';
case State of
      tcdLoaded : Result := 'Import Data Loaded';
      tcdAvailable  : Result := 'Import Data Loaded';
      tcdImportButNoComp  : Result := 'Import exists But No Comp in Comp File';
      tcdNoCompNoImport  : Result := 'No Comp and No Import';
      tcdNoImportButComp  : Result :='Comp Exists But No Import';
      else
        Result  := 'No Data Available';
      end;

end;

{procedure TCalDataStatusGrid.NewUpdateFromAxisList(AList: TCalAxisList);
var
AxEntry : TSingleAxisData;
AxNum : INteger;
AxName : String;
begin
if AList.Count > 0 then
  begin
  For AxNum := 0 to AList.Count do
    begin
    AxName := Cells[

    end;
  end;
end;}

procedure TCalDataStatusGrid.aaUpdateFromAxisList(AList: TCalAxisList);
var
AxEntry : TSingleAxisData;
AxNum : INteger;
Reason : String;
Status : String;
begin
if AList.Availability.PowerUpMode = cpuFullyRegistered then
  begin
  if AList.Count > 0 then
    begin
    AxNum := 1;
    RowCount := AList.Count+1;
    with Alist.Availability do
      begin
      if XData.Required then
        begin
        Cells[0,AxNum] := 'X';
        RegisteredStatusForAxis('X',Status,Reason);
        Cells[1,AxNum] := Status;
        Cells[2,AxNum] := Reason;
        end;
      if YData.Required then
        begin
        inc(Axnum);
        Cells[0,AxNum] := 'Y';
        RegisteredStatusForAxis('Y',Status,Reason);
        Cells[1,AxNum] := Status;
        Cells[2,AxNum] := Reason;
        end;
      if ZData.Required then
        begin
        inc(Axnum);
        Cells[0,AxNum] := 'Z';
        RegisteredStatusForAxis('Z',Status,Reason);
        Cells[1,AxNum] := Status;
        Cells[2,AxNum] := Reason;
        end;
      if AData.Required then
        begin
        inc(Axnum);
        Cells[0,AxNum] := 'A';
        RegisteredStatusForAxis('A',Status,Reason);
        Cells[1,AxNum] := Status;
        Cells[2,AxNum] := Reason;
        end;
      if BData.Required then
        begin
        inc(Axnum);
        Cells[0,AxNum] := 'B';
        RegisteredStatusForAxis('B',Status,Reason);
        Cells[1,AxNum] := Status;
        Cells[2,AxNum] := Reason;
        end;
      if CData.Required then
        begin
        inc(Axnum);
        Cells[0,AxNum] := 'C';
        RegisteredStatusForAxis('C',Status,Reason);
        Cells[1,AxNum] := Status;
        Cells[2,AxNum] := Reason;
        end;

    end; // with
    end; // if AList.Count > 0
  end
else
  begin
  if AList.Count > 0 then
    begin
    AxNum := 1;
    RowCount := AList.Count+1;
    with Alist.Availability do
      begin
      if XData.Required then
        begin
        Cells[0,AxNum] := 'X';
        if XData.WorkingDataState = tcdLoaded then
          Cells[2,AxNum] := 'Working Data Loaded'
        else
          Cells[2,AxNum] := MessageFromState(XData.ImportDataState);
        if not EnforcedCompExistence.XCompExists then Cells[2,AxNum] := 'No Compensation'
        end;

      if YData.Required then
        begin
        inc(Axnum);
        Cells[0,AxNum] := 'Y';
        if YData.WorkingDataState = tcdLoaded then
          Cells[2,AxNum] := 'Working Data Loaded'
        else
          Cells[2,AxNum] := MessageFromState(YData.ImportDataState);
        if not EnforcedCompExistence.YCompExists then Cells[2,AxNum] := 'No Compensation'
        end;

      if ZData.Required then
        begin
        inc(Axnum);
        Cells[0,AxNum] := 'Z';
        if ZData.WorkingDataState = tcdLoaded then
          Cells[2,AxNum] := 'Working Data Loaded'
        else
          Cells[2,AxNum] := MessageFromState(ZData.ImportDataState);
        if not EnforcedCompExistence.ZCompExists then Cells[2,AxNum] := 'No Compensation'
        end;

      if AData.Required then
        begin
        inc(Axnum);
        Cells[0,AxNum] := 'A';
        if AData.WorkingDataState = tcdLoaded then
          Cells[2,AxNum] := 'Working Data Loaded'
        else
          Cells[2,AxNum] := MessageFromState(AData.ImportDataState);
        if not EnforcedCompExistence.ACompExists then Cells[2,AxNum] := 'No Compensation'
        end;

      if BData.Required then
        begin
        inc(Axnum);
        Cells[0,AxNum] := 'B';
        if BData.WorkingDataState = tcdLoaded then
          Cells[2,AxNum] := 'Working Data Loaded'
        else
          Cells[2,AxNum] := MessageFromState(BData.ImportDataState);
        if not EnforcedCompExistence.BCompExists then Cells[2,AxNum] := 'No Compensation'
        end;

      if CData.Required then
        begin
        inc(Axnum);
        Cells[0,AxNum] := 'C';
        if CData.WorkingDataState = tcdLoaded then
          Cells[2,AxNum] := 'Working Data Loaded'
        else
          Cells[2,AxNum] := MessageFromState(CData.ImportDataState);
        if not EnforcedCompExistence.CCompExists then Cells[2,AxNum] := 'No Compensation'
        end;
      end; //with
    end; // if*
  end;
  Height := ((AxNum+1)*DefaultRowHeight)+ 12;
end;

{ TConditionedDataInDataGrid }

constructor TConditionedDataInDataGrid.Create(AOwner: TComponent);
begin
  inherited;
  RowCount := 3;
  ColCount := 5;
  FixedRows := 1;
  FixedCols := 1;
  Width := 550;
  Height := 200;
  DefaultRowHeight := 20;
  Font.Size := 10;
  Cells[0,0] := 'Target';
  Cells[1,0] := 'Posn. Error';
  Cells[2,0] := 'Rev. Error';
  Cells[3,0] := 'Posn. Error';
  Cells[4,0] := 'Rev. Error';
  ColWidths[0] := 70;
  ColWidths[1] := 70;
  ColWidths[2] := 70;
  ColWidths[3] := 70;
  ColWidths[4] := 70;

end;

destructor TConditionedDataInDataGrid.Destroy;
begin

  inherited;
end;

procedure TConditionedDataInDataGrid.UpdateFromAxisData(AxData: TSingleAxisData);
var
Enum : Integer;
LEntry : TInputDataRecord;
begin
if AxData.CompensationDataLists.CurrentInputDataList.Count > 0 then
 begin
 RowCount := AxData.CompensationDataLists.CurrentInputDataList.Count+1;
 For Enum := 0 to AxData.CompensationDataLists.CurrentInputDataList.Count-1 do
  begin
  LEntry := AxData.CompensationDataLists.CurrentInputDataList[Enum];
  Cells[0,Enum+1] := Format('%5.3f',[Lentry.Position]);
  Cells[1,Enum+1] := Format('%5.3f',[Lentry.InputForwardError]);
  Cells[2,Enum+1] := Format('%5.3f',[Lentry.InputReverseError]);
  Cells[3,Enum+1] := Format('%5.3f',[Lentry.ZeroShiftedForwardData]);
  Cells[4,Enum+1] := Format('%5.3f',[Lentry.ZeroShiftedReverseData]);
  end
 end;
end;

{ TConditionedDataOutDataGrid }

constructor TConditionedDataOutDataGrid.Create(AOwner: TComponent);
begin
  inherited;
  RowCount := 27;
  ColCount := 5;
  FixedRows := 1;
  FixedCols := 1;
  Width := 550;
  Height := 200;
  DefaultRowHeight := 20;
  Font.Size := 10;
  Cells[0,0] := 'Act. Posn.';
  Cells[1,0] := 'Encoder';
  Cells[2,0] := 'Compensation';
  Cells[3,0] := 'Rev. Compensation';
  Cells[4,0] := 'Backlash';
  ColWidths[0] := 70;
  ColWidths[1] := 90;
  ColWidths[2] := 90;
  ColWidths[3] := 110;
  ColWidths[4] := 90;
end;

destructor TConditionedDataOutDataGrid.Destroy;
begin

  inherited;
end;

procedure TConditionedDataOutDataGrid.UpdateFromAxisData(AxData: TSingleAxisData);
var
Enum : Integer;
LEntry : TOutputDataRecord;
begin
if AxData.CompensationDataLists.OutputDataList.Count > 0 then
 begin
 RowCount := AxData.CompensationDataLists.OutputDataList.Count+1;
 For Enum := 0 to AxData.CompensationDataLists.OutputDataList.Count-1 do
  begin
  LEntry := AxData.CompensationDataLists.OutputDataList[Enum];
  Cells[0,Enum+1] := Format('%5.3f',[Lentry.ActualPosition]);
  Cells[1,Enum+1] := Format('%d',[Round(Lentry.EncoderCounts)]);
  Cells[2,Enum+1] := Format('%5.3f',[Lentry.CompValue]);
  Cells[3,Enum+1] := Format('%5.3f',[Lentry.RevCompValue]);
  Cells[4,Enum+1] := Format('%5.3f',[Lentry.Backlash]);
  end
 end;
end;

end.
