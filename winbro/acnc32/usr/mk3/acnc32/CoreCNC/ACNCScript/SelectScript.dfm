object ScriptSelectForm: TScriptSelectForm
  Left = 325
  Top = 179
  Width = 178
  Height = 211
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object ButtonPanel: TPanel
    Left = 0
    Top = 158
    Width = 170
    Height = 26
    Align = alBottom
    TabOrder = 0
    object Button1: TButton
      Left = 16
      Top = 6
      Width = 49
      Height = 17
      Caption = 'OK'
      TabOrder = 0
      OnClick = ListBoxDblClick
    end
    object Button2: TButton
      Left = 72
      Top = 6
      Width = 49
      Height = 17
      Cancel = True
      Caption = 'Cancel'
      TabOrder = 1
    end
  end
  object ListBox: TListBox
    Left = 0
    Top = 0
    Width = 170
    Height = 158
    Align = alClient
    ItemHeight = 13
    TabOrder = 1
    OnDblClick = ListBoxDblClick
  end
end
