unit ACNCScript;

{
    NOTES
/    1. Add for next loop
/    2. Create PCode Engine.
/    3. Use the MathParser to Compile but if it never calls resolve
/       for anything except constants or MathFunctions then Replace with result.
/       -- Added IsConstant
/    4. Use the As[DataType] for function results and differentiate
/       procedure / function some other way. -- Added HasValue
/  4.5  Differentiate constants and variables in a stronger manner
/       Different List ?. Different Ancestor ? NewField ? -- Added IsVariable
/    5. Determine rules for casting and add methods to abstract accordingly
/       Search for *AssignmentRules* for applications.
/    6. Add functionallity for Boolean, integer and string.
/    7. Attach to NamedLayer.
/    8. Atach to IO System.
    9. Add proper boolean evaluation.
/   10. Revisit TGenericTag with a view to removing Interlock if not needed
/   10.5 Check Scope of variables System, Project, Unit, Procedure.
/   10.6 Add checking for redefinition of variable name within scope.
   11. Create Project classes for Project, UDFB and Event.
-----   12. Add a pure PCode version for embedded use. (CANNOT IMPLEMENT)
/   13. Create an ancestor for TMethodScriptTag for use as template for
/       Delphi code objects
/   14. Add Named array variables e.g. AxisPosition, GCode etc.
/   15. Optional no line numbers to skip single block. ( REVISIT @- etc.
/   16. Create TTL Run action
/   17. Implement var parameter passing and alter IsWritable on Parameter tags
/       accordingly.
   18. Type checking of entry method.  This may vary according to the required mode
       of operation Project / Event / UDFB.
/   19. Index property of TScriptMethodTag must be removed and replaced with some
/       form of script instance method. **** THIS MUST BE DONE ****
   20. Add Unit Scope Methods.
   21. Add Method Scope Methods.
   22. UpdateMathParser to Include mod, div Ordinal operators. This is in conjunction
       with #9.
   23. Change the datatype of the MP to account for contained data (in conj with 22).
   24. Either here or above project level. Store project files in registry.
/   25. Redirect ScriptParameterTag to the specific instance.
/   26. Verify method prototypes are fulfulled.
}
interface

uses CoreCNC, CNCTypes, CNC32, Tokenizer, SysUtils, Classes,
     ScriptTypeInfo, Forms, Windows, ScriptFunctions, NCMath;

type
  { TACNCScriptProject is a wrapper for a group of TAbstractScripts it adds the
    highest level of scope for a script.

    To use this the owner must directly write to the units property with preloaded
    AbstractScripts these must be set with an apropriate tagowner, there project
    property must be set to this.

    Once the ground is set use the following
    1. Compile
    2. Run
    3. SingleBlock
  }
  TACNCScript = class(TAbstractScript)
  private
    MathParser : TNCMathParser;
    procedure ReadVariable(Scope : TScriptVariableScope);
    procedure CompileMethod(Resultant : Boolean);
    procedure Clean;
    procedure ReadPreprocessor;
  protected
    function ReadConstant : TAbstractTag; override;
  public
    constructor Create(aTagOwner : TTagOwner);
    destructor Destroy; override;
    procedure ReadInterface;
    procedure ReadImplementation;
    function  FindToken(aToken : string) : Boolean; override;
    function  FindTypeList(aName, Suffix : string) : TAbstractTag; override;
  end;

  TACNCScriptProject = class(TAbstractACNCScriptProject)
  private
    FErrorPosition : TPoint;
    FErrorScript : Integer;
    FRTL : TScriptRTL;
    FOwnerCleans : Boolean;
    BlockReentry : Boolean;
    function GetLineNumber : Integer;
    function GetActiveFile : string;
  protected
    Script : TACNCScript;
  public
    constructor Create(aOwner : TTagOwner);
    procedure Compile(aEntry : string);
    procedure SetEntryMethod(Method: TScriptMethodTag);
    procedure Run(aTag: TAbstractTag = nil); override;
    procedure Rewind; override;
    function  FindTypeList(aName, Suffix : string) : TAbstractTag; override;
    function SimpleResolve(aName, Parse : string) : TAbstractTag; override;
    function  Owns(aTag : TAbstractTag) : Boolean; override;
//    property TagOwner : TTagOwner read FTagOwner;
    property LineNumber : Integer read GetLineNumber;
    property ActiveFile : string read GetActiveFile;
    property ErrorPosition : TPoint read FErrorPosition;
    property ErrorScript : Integer read FErrorScript;
    property RTL : TScriptRTL read FRTL write FRTL;
    property OwnerCleans: Boolean read FOwnerCleans write FOwnerCleans;
  end;

  TACNCScriptUDFB = class(TACNCScriptProject)
  end;

  TACNCScriptEvent = class(TACNCScriptProject)
  end;

  TACNCScriptLibrary = class(TACNCScriptProject)
  protected
  public
    constructor Create(aOwner : TTagOwner);
    procedure SetEntryMethod(aMethod : TScriptMethodTag);
    procedure Clean; override;
    function  FindTypeList(aName, Suffix : string) : TAbstractTag; override;
  end;

implementation

uses
  TypInfo, StdCtrls;

constructor TACNCScript.Create(aTagOwner : TTagOwner);
begin
  inherited Create;
  LineInfo := True;
  TypeList := TScriptVariableScope.Create;
  TagOwner := aTagOwner;
  MathParser := TNCMathParser.Create(TagOwner, 'LocalMath');
end;

procedure TACNCScript.Clean;
begin
  TypeList.Clean;
end;

destructor TACNCScript.Destroy;
begin
  Clean;
  MathParser.Free;
  TypeList.Free;
  inherited Destroy;
end;

{ Finds a token that is not commented out
  Check that '//' has not been deprecated by modified read }
function  TACNCScript.FindToken(aToken : string) : Boolean;
var I :Integer;
begin
  Result := True;
  while not EndOfStream do
    if inherited FindToken(aToken) then begin
      I := Pos(CurrentLine, '//');
      if (I = 0) or (I > TokenPos) then
        Exit;
    end;

  Result := False;
end;

//
// Purpose reads a variable upto a ';'
// Because of the syntax there can be more than 1. but are of the same class e.g.
//
// var [a, b, c : Double;]
// var [a : Double;]
//
resourcestring
  CannotApplyOverrideToScope = 'Cannot apply override to Scope variables';
  ValueIsNotConstant = 'Value Is not a simple constant';
  PreprocessorMustStartWith = 'A Preprocessor must start with [- or +]';
  InvalidPreprocessorFormat = 'Invalid Preprocessor [%s]]';
  InvalidTypeLang = 'Invalid Type';
  NoInterfaceLang = 'No Interface';
  NoPrototypeForMethodFormat = 'No Prototype for method [%s]';
  MixingFunctionProcedureFormat = 'Mixing function and procedure for method [%s]';
  NotAMethodFormat = '[%s] is not a method';
  IncorrectNumberOfParametersFormat = '[%s] Incorrect number of parameters';
  ParameterNotMatchPrototypeFormat = 'Parameter [%s] Does not Match Prototype of [%s]';
  NoImplementationLang = 'No Implementation';
  UnsatifiedFormwardReferenceLang = ' Unsatified procedure reference';
  ErrorLineFormat = '%s : Line %d: X %d';
  UndefinedFormatReferenceFormat = 'Undefined forward reference [%s]';
  EntryNotDefinedFormat = 'Entry method [%s] not defined';
  EntryIsNotMethodOrFunction = 'Entry [%s] Is Not a Method or Function';

procedure TACNCScript.ReadVariable(Scope : TScriptVariableScope);
var ST : TVariableList;
    ParmType : TParmPassing;
begin
  ST := TVariableList.Create;
  try
    ReadVariableList(ST, ParmType);
    if ParmType <> passByValue then
      raise EACNCScriptError.Create(CannotApplyOverrideToScope);
    AddVariables(Scope, ST);
    ExpectedToken(';');
  finally
    ST.Free;
  end;
end;

//
// Create a nameless tag from simple / single data types
//
function TACNCScriptProject.SimpleResolve(aName, Parse : string) : TAbstractTag;
var Value : Double;
    I : Integer;
    Code : Integer;
begin
  Result := nil;
  if IsNumeric(Parse[1]) or
              (Parse[1] = '-') or
              (Parse[1] = '+') or
              (Parse[1] = '$') then begin
    Val(Parse, Value, Code);
    if Code = 0 then begin
      if Round(Value) = Value then begin
        Result := TOrdinalTag.Create(TagOwner, aName);
        TOrdinalTag(Result).AsInteger := Round(Value);
      end else begin
        Result := TFloatTag.Create(TagOwner, aName);
        TFloatTag(Result).AsDouble := Value;
      end;
      Exit;
    end;
  end else if Parse[1] = '''' then begin
    for I := 2 to Length(Parse) - 1 do
      if (Parse[I] = '''') then
        Exit;

    Result := TAlphaTag.Create(TagOwner, aName);
    Result.AsString := SysObj.Localized.GetString(Copy(Parse, 2, Length(Parse) - 2));
  end;
end;

function TACNCScript.ReadConstant : TAbstractTag;
var aName, Token, Working : string;
begin
  aName := Self.Read;
  ValidSymbol(aName);
  ExpectedToken('=');

  Token := Self.Read;
  Working := '';
  while Token <> ';' do begin
    Working := Working + Token;
    Token := Self.Read;
    CheckStream;
  end;
  Result := Project.SimpleResolve(aName, Working);
  if Result <> nil then begin
    Result.IsConstant := True;
    Result.IsWritable := False;
  end else
    raise EACNCScriptError.Create(ValueIsNotConstant);
end;

procedure TACNCScript.ReadPreprocessor;
var Token : string;
    Opt : Boolean;
    I : Integer;
begin
  Token := Self.Read;
  if Token = '-' then
    Opt := False
  else if Token = '+' then
    Opt := True
  else
    raise EACNCScriptError.Create(PreprocessorMustStartWith);

  Token := Self.Read;
  for I := 1 to Length(Token) do begin
    Token := UpperCase(Token);
    case Token[I] of
      'L' : LineInfo := Opt;
    else
      raise EACNCScriptError.CreateFmt(InvalidPreprocessorFormat, [Token[I]]);
    end;
  end;
end;


procedure TACNCScript.ReadInterface;
  procedure ReadType;
  var
    Token : string;
    Method : TScriptMethodTag;
  begin
    Token := Self.Read;
    Method := TScriptMethodTag.Create(TagOwner, Self.Read);

    if CompareText(Token, ScriptLexicon[slProcedure]) = 0 then
      Method.ReadPrototype(Self, False)
    else if CompareText(Token, ScriptLexicon[slFunction]) = 0 then
      Method.ReadPrototype(Self, True)
    else
      raise EACNCScriptError.Create(InvalidTypeLang);

    Project.TypeList.Add(Method);
  end;

var
  TopLevel : TScriptLexicon;
  aTag : TAbstractTag;
begin
  LineInfo := True;
  Self.Rewind;
  FindToken(ScriptLexicon[slInterface]);
  if EndOfStream then
    raise EACNCScriptError.Create(NoInterfaceLang);

  TopLevel := slInvalid;

  while TopLevel <> slImplementation do begin
    case SymbolType of
      slImplementation : TopLevel := slImplementation;
      slType           : TopLevel := slType;
      slVariable       : TopLevel := slVariable;
      slConstant       : TopLevel := slConstant;
      slPreprocessor   : ReadPreprocessor;
    else
      Self.Push;
      case TopLevel of
        slType    : ReadType;
        slVariable: ReadVariable(Project.TypeList);
        slConstant: begin
                      aTag := ReadConstant;
                      Project.TypeList.Add(aTag);
                    end;
      else
        raise EACNCScriptError.CreateFmt(UnExpectedFormat, [Self.Read]);
      end;
    end;
    CheckStream;
  end;
end;

function TACNCScript.FindTypeList(aName, Suffix : string) : TAbstractTag;
var I : Integer;
begin
  aName := Trim(aName);
  for I := 0 to TypeList.Count - 1 do begin
    Result := TAbstractTag(TypeList[I]);
    if CompareText(aName, Result.Name) = 0 then begin
      Exit;
    end;
  end;

  Result := Project.FindTypeList(aName, Suffix);
end;


procedure TACNCScript.CompileMethod(Resultant : Boolean);
var aTag : TAbstractTag;
    Token : string;
    Proto : TScriptMethodTag;
    Method : TScriptMethodTag;
    I : Integer;
begin
  Token := Self.Read; // Grab the name
  aTag := FindTypeList(Token, '');

  if aTag = nil then
    raise EACNCScriptError.CreateFmt(NoPrototypeForMethodFormat, [Token]);

  if aTag.HasValue <> Resultant then
    raise EACNCScriptError.CreateFmt(MixingFunctionProcedureFormat, [Token]);

  if aTag is TScriptMethodTag then begin
    Method := aTag as TScriptMethodTag;
  end else
    raise EACNCScriptError.CreateFmt(NotAMethodFormat, [Token]);

  Proto := TScriptMethodTag.Create(TagOwner, '');
  try
    Proto.ReadPrototype(Self, Resultant);

    if Method.Prototype.Count <> Proto.Prototype.Count then
      raise EACNCScriptError.CreateFmt(IncorrectNumberOfParametersFormat, [Method.Name]);

    for I := 0 to Method.Prototype.Count - 1 do begin
// PARAMETERS Change prototype comparison ?????
      if (CompareText(Method.ProtoType[I].Name, Proto.Prototype[I].Name) <> 0) or
         (Method.Prototype[I].TagClass <> Proto.Prototype[I].TagClass) or
         (Method.ProtoType[I].ParmType <> Proto.Prototype[I].ParmType) then
        raise EACNCScriptError.CreateFmt(ParameterNotMatchPrototypeFormat, [Method.Prototype[I], Method.Name]);
    end;
  finally
    Proto.Free;
  end;
  Method.Compile(Self);
end;

procedure TACNCScript.ReadImplementation;
  procedure ReadType;
  var
    Token : string;
  begin
    Token := Self.Read;
    if CompareText(Token, ScriptLexicon[slProcedure]) = 0 then
      CompileMethod(False)
    else if CompareText(Token, ScriptLexicon[slFunction]) = 0 then
      CompileMethod(True)
    else
      raise EACNCScriptError.Create(InvalidTypeLang);
  end;

var
  TopLevel : TScriptLexicon;
  aTag : TAbstractTag;
  I : Integer;
begin
  Self.Rewind;
  FindToken(ScriptLexicon[slImplementation]);
  if EndOfStream then
    raise EACNCScriptError.Create(NoImplementationLang);

  TopLevel := slInvalid;

  while TopLevel <> slImplementation do begin
    case SymbolType of
      slImplementation : TopLevel := slImplementation;
      slFunction,
      slProcedure,
      slType           : begin
                           TopLevel := slType;
                           Self.Push;
                           ReadType;
                         end;
      slVariable       : TopLevel := slVariable;
      slConstant       : TopLevel := slConstant;
      slPreprocessor   : ReadPreprocessor;
    else
      if EndofStream then
        Exit;
      Self.Push;
      case TopLevel of
        slVariable: ReadVariable(TypeList);
        slConstant: begin
                      aTag := ReadConstant;
                      TypeList.Add(aTag);
                    end;
      else
        raise EACNCScriptError.CreateFmt(UnExpectedFormat, [Self.Read]);
      end;
    end;
    if EndofStream then
      raise EACNCScriptError.CreateFmt(UnExpectedFormat, [EndOfFileText]);
  end;

  for  I := 0 to TypeList.Count do
    if TAbstractTag(TypeList[I]) is TScriptMethodTag then
      with TScriptMethodTag(TypeList[I]) do
        if Code.Count = 0 then
          raise EACNCScriptError.Create(Name + UnsatifiedFormwardReferenceLang);
end;

constructor TACNCScriptProject.Create(aOwner : TTagOwner);
begin
  inherited Create(aOwner);
  BlockReentry:= False;
end;

procedure TACNCScriptProject.Compile(aEntry : string);
  procedure DoCompileError(E : Exception; S : TACNCScript);
  begin
    FActiveScript := nil;
    FActiveMethod := nil;
    if S <> nil then begin
      FErrorPosition.X := S.Position;
      FErrorPosition.Y := S.Line;
      FErrorScript := Units.IndexOf(S);
      raise EACNCScriptError.CreateFmt(ErrorLineFormat, [E.Message, S.Line, S.Position])
    end else
      raise EACNCScriptError.Create('This is bad');
  end;

var I : Integer;
    aTag : TAbstractTag;
begin
  FActiveScript := nil;
  FActiveMethod := nil;
  Script := nil;
  Clean;
  try
    for I := 0 to Units.Count - 1 do begin
      Script := TACNCScript(Units[I]);
      Script.Clean;
      Script.ReadInterface;
    end;
  except
    On E : EACNCScriptError do
        DoCompileError(E, Script);
  end;

  try
    for I := 0 to Units.Count - 1 do begin
      Script := TACNCScript(Units[I]);
      Script.ReadImplementation;
    end;
  except
    On E : EACNCScriptError do
      DoCompileError(E, Script);
    On E : EMathExecuteError do
      DoCompileError(E, Script); // This must be caught for resolving consts
  end;

  for I := 0 to TypeList.Count - 1 do begin
    aTag := TypeList[I];
    if aTag is TScriptMethodTag then
      with aTag as TScriptMethodTag do
        if Code.Count = 0 then
          raise EACNCScriptError.CreateFmt(UndefinedFormatReferenceFormat, [aTag.Name]);
  end;


  aTag := FindTypeList(aEntry, '');
  if (aTag = nil) then
    raise EACNCScriptError.CreateFmt(EntryNotDefinedFormat, [aEntry]);

  if not (aTag is TScriptMethodTag)then
    raise EACNCScriptError.CreateFmt(EntryIsNotMethodOrFunction, [aEntry]);

  FCompiled := True;
  SetEntryMethod(aTag as TScriptMethodTag);
end;

procedure TAcncScriptProject.SetEntryMethod(Method: TScriptMethodTag);
begin
  EntryMethod := Method;
  FActiveScript := EntryMethod.Script;
  FTraceScript := EntryMethod.Script;
  FActiveMethod := EntryMethod;
end;

procedure TACNCScriptProject.Rewind;
begin
  inherited Rewind;
  if EntryMethod <> nil then begin
    FActiveScript := EntryMethod.Script;
    FActiveMethod := EntryMethod;
  end;
  if Assigned(RTL) then
    RTL.CleanUp;
end;

procedure TACNCScriptProject.Run(aTag: TAbstractTag);
begin
  if not BlockReentry then begin
    BlockReentry:= True;
    try
      if not Compiled then
        Exit;

      ThisTTL := TTL;
      inherited Run(aTag);
      if not InProcess and not OwnerCleans then
        if Assigned(RTL) then
          RTL.CleanUp;
    finally
      BlockReentry:= False;
    end;
  end;
end;

function TACNCScriptProject.GetLineNumber : Integer;
begin
  if ActiveScript = nil then
    Result := 0
  else
    Result := TraceScript.LineNumber;
end;

function TACNCScriptProject.GetActiveFile : string;
begin
  if ActiveScript = nil then
    Result := ''
  else
    if ActiveScript.FileName <> '' then
      Result := ActiveScript.FileName
    else
      Result := ActiveScript.Name;
end;

function TACNCScriptProject.FindTypeList(aName, Suffix : string) : TAbstractTag;
var I : Integer;
begin
  aName := Trim(aName);
  for I := 0 to TypeList.Count - 1 do begin
    Result := TAbstractTag(TypeList[I]);
    if CompareText(Result.Name, aName) = 0 then
      Exit;
  end;

  Result := nil;

  if Pos('IO.', aName) = 1 then begin
    Result := SysObj.Comms.FindTag(System.Copy(aName, 4, Length(aName)));
    if Result <> nil then
      if Result.IsWritable and (not Result.IsOwned) then
        if OwnedIO.IndexOf(Result) = -1 then begin
          OwnedIO.Add(Result);
//          Result.IsOwned := True;
        end;
  end;

  if (Result = nil) and Assigned(FRTL) then begin
    Result := RTL.FindTypeList(Script, aName);
    if Result <> nil then
      TypeList.Add(Result);
  end;
end;


function  TACNCScriptProject.Owns(aTag : TAbstractTag) : Boolean;
begin
  Result := OwnedIO.IndexOf(aTag) <> -1;
end;

constructor TACNCScriptLibrary.Create(aOwner : TTagOwner);
begin
  inherited;
  OwnerCleans := True;
end;

procedure TACNCScriptLibrary.Clean;
begin
  inherited Clean;
  while Units.Count > 0 do begin
    TObject(Units[0]).Free;
    Units.Delete(0);
  end;
end;


function TACNCScriptLibrary.FindTypeList(aName, Suffix : string) : TAbstractTag;
var I : Integer;
begin
  Result := nil;
  aName := Trim(aName);
  for I := 0 to TypeList.Count - 1 do begin
    Result := TAbstractTag(TypeList[I]);
    if CompareText(Result.Name, aName) = 0 then
      Exit;
  end;

  if Assigned(FRTL) then begin
    Script := TACNCScript.Create(TagOwner);
    Script.Name := aName;
    Script.Project := Self;
    Units.Add(Script);
    Result := RTL.FindTypeList(Script, aName);
    if Result <> nil then
      TypeList.Add(Result);
  end;
end;


procedure TACNCScriptLibrary.SetEntryMethod(aMethod : TScriptMethodTag);
begin
  FCompiled := True;
  if Assigned(aMethod) then
    FActiveScript := aMethod.Script
  else
    FActiveScript := nil;
    
  EntryMethod := aMethod;
end;

end.
