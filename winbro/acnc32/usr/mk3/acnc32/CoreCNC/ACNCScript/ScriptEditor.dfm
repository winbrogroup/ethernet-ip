object ScriptEditForm: TScriptEditForm
  Left = 176
  Top = 228
  Width = 766
  Height = 372
  Caption = 'Script Editor'
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -15
  Font.Name = 'Verdana'
  Font.Style = []
  FormStyle = fsStayOnTop
  Menu = MainMenu1
  OldCreateOrder = True
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnDeactivate = FormDeactivate
  OnPaint = FormPaint
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 18
  object Splitter1: TSplitter
    Left = 254
    Top = 0
    Height = 280
  end
  object Panel1: TPanel
    Left = 0
    Top = 280
    Width = 758
    Height = 46
    Align = alBottom
    TabOrder = 0
    object SingleBtn: TSpeedButton
      Left = 482
      Top = 7
      Width = 65
      Height = 25
      AllowAllUp = True
      GroupIndex = 1
      Down = True
      Caption = 'Single'
      Enabled = False
      OnClick = SingleBtnClick
    end
    object CompileBtn: TButton
      Left = 7
      Top = 7
      Width = 90
      Height = 30
      Caption = 'Compile'
      TabOrder = 0
      OnClick = CompileBtnClick
    end
    object StepBtn: TButton
      Left = 102
      Top = 7
      Width = 90
      Height = 30
      Caption = 'Step'
      TabOrder = 1
      OnClick = StepBtnClick
    end
    object RunBtn: TButton
      Left = 196
      Top = 7
      Width = 90
      Height = 30
      Caption = 'Run'
      TabOrder = 2
      OnClick = RunBtnClick
    end
    object EditTTL: TEdit
      Left = 391
      Top = 7
      Width = 59
      Height = 26
      TabOrder = 3
      Text = '0'
    end
    object UpDownTTL: TUpDown
      Left = 450
      Top = 7
      Width = 25
      Height = 26
      Associate = EditTTL
      Max = 32000
      Increment = 10
      TabOrder = 4
      OnChanging = UpDownTTLChanging
    end
    object RewindBtn: TButton
      Left = 289
      Top = 7
      Width = 90
      Height = 30
      Caption = 'Rewind'
      TabOrder = 5
      OnClick = RewindBtnClick
    end
    object MathBtn: TButton
      Left = 657
      Top = 23
      Width = 72
      Height = 20
      Caption = 'Math Debug'
      TabOrder = 6
      Visible = False
      OnClick = MathBtnClick
    end
    object OKBtn: TBitBtn
      Left = 560
      Top = 6
      Width = 90
      Height = 30
      Caption = 'Save'
      TabOrder = 7
      OnClick = OKBtnClick
      Kind = bkOK
    end
    object CancelBtn: TBitBtn
      Left = 656
      Top = 6
      Width = 90
      Height = 30
      Caption = 'Close'
      TabOrder = 8
      OnClick = CancelBtnClick
      Kind = bkClose
    end
  end
  object ScopeTab: TTabControl
    Left = 0
    Top = 0
    Width = 254
    Height = 280
    Align = alLeft
    TabOrder = 1
    Tabs.Strings = (
      'Script')
    TabIndex = 0
    OnChange = ScopeTabChange
    OnResize = ScopeTabResize
    object VGrid: TStringGrid
      Left = 4
      Top = 29
      Width = 246
      Height = 247
      Align = alClient
      ColCount = 2
      DefaultColWidth = 120
      RowCount = 2
      FixedRows = 0
      TabOrder = 0
      OnDblClick = VGridDblClick
    end
  end
  object EditTab: TTabControl
    Left = 257
    Top = 0
    Width = 501
    Height = 280
    Align = alClient
    PopupMenu = PopupMenu1
    TabOrder = 2
    Tabs.Strings = (
      'Script')
    TabIndex = 0
    OnChange = EditTabChange
    OnChanging = EditTabChanging
    object Paint: TPaintBox
      Left = 4
      Top = 29
      Width = 17
      Height = 232
      Align = alLeft
    end
    object ScrollPanel: TPanel
      Left = 484
      Top = 29
      Width = 13
      Height = 232
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 0
      object ScrollBar: TScrollBar
        Left = -3
        Top = 0
        Width = 16
        Height = 163
        Kind = sbVertical
        PageSize = 0
        TabOrder = 0
      end
    end
    object StatusPanel: TPanel
      Left = 4
      Top = 261
      Width = 493
      Height = 15
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 1
      object CurPosLabel: TLabel
        Left = 20
        Top = 2
        Width = 27
        Height = 18
        Caption = '1:1'
      end
    end
  end
  object MainMenu1: TMainMenu
    Left = 472
    Top = 27
    object File1: TMenuItem
      Caption = 'Script'
      object New1: TMenuItem
        Caption = 'New'
        OnClick = New1Click
      end
      object Import1: TMenuItem
        Caption = '&Import'
        OnClick = Import1Click
      end
      object Export1: TMenuItem
        Caption = 'Export'
        OnClick = Export1Click
      end
      object Remove1: TMenuItem
        Caption = 'Remove'
        OnClick = Remove1Click
      end
      object Rename1: TMenuItem
        Caption = 'Rename'
        OnClick = Rename1Click
      end
      object Exit1: TMenuItem
        Caption = '&Exit'
        OnClick = CloseBtnClick
      end
    end
    object Compile1: TMenuItem
      Caption = '&Project'
      object Compile2: TMenuItem
        Caption = 'Compile'
        OnClick = CompileBtnClick
      end
      object Step1: TMenuItem
        Caption = 'Step'
        OnClick = StepBtnClick
      end
      object Run1: TMenuItem
        Caption = 'Run'
        OnClick = RunBtnClick
      end
      object SingleMenu: TMenuItem
        Caption = 'Single'
        Checked = True
        OnClick = SingleMenuClick
      end
    end
  end
  object OpenDialog: TOpenDialog
    Left = 504
    Top = 19
  end
  object SaveDialog: TSaveDialog
    DefaultExt = 'Script'
    Filter = 'Script Files|*.Script'
    Left = 470
  end
  object PopupMenu1: TPopupMenu
    Left = 347
    Top = 120
    object Rename2: TMenuItem
      Caption = 'Rename'
      OnClick = Rename1Click
    end
    object Remove2: TMenuItem
      Caption = 'Remove'
      OnClick = Remove2Click
    end
  end
end
