unit SelectScript;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, ScriptTypeInfo;

type
  TScriptSelectForm = class(TForm)
    ButtonPanel: TPanel;
    ListBox: TListBox;
    Button1: TButton;
    Button2: TButton;
    procedure ListBoxDblClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
  public
    class function SelectScript(aList : TList) : Integer;
  end;

implementation

{$R *.DFM}
class function TScriptSelectForm.SelectScript(aList : TList) : Integer;
var
  Form : TScriptSelectForm;
  I : INteger;
begin
  Form := TScriptSelectForm.Create(Application);
  for  I := 0 to aList.Count - 1 do begin
    Form.ListBox.Items.Add(TAbstractScript(aList[I]).Name);
  end;

  if Form.ShowModal = mrOK then
    Result := Form.ListBox.ItemIndex
  else
    Result := -1;

  Form.Free;
end;


procedure TScriptSelectForm.ListBoxDblClick(Sender: TObject);
begin
  with ListBox do 
    if (ItemIndex >= 0) and (ItemIndex < Items.Count) then
      ModalResult := mrOK
    else
      ModalResult := mrCancel;
end;

procedure TScriptSelectForm.FormShow(Sender: TObject);
begin
  ListBox.ItemIndex := 0;
end;

end.
