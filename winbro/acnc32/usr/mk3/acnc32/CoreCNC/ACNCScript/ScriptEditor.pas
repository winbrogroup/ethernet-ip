unit ScriptEditor;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ACNCScript, Editor, CNC32, CoreCNC, ScriptTypeInfo, CNCTypes, StdCtrls, ExtCtrls,
  AbstractEditor, ComCtrls, Grids, Menus, SelectScript, Buttons,
  Acknowledge, Named, ArrayViewForm;

type
  TACNCScriptEditor = class(TSyntaxEditor)
  private
    Parser : TACNCScriptParser;
  protected
    function  ParseLine(aText : string; Line : Integer) : string; override;
  public
    constructor Create(AOwner : TComponent); override;
  end;

  TScriptEditForm = class(TForm)
    Panel1: TPanel;
    CompileBtn: TButton;
    StepBtn: TButton;
    RunBtn: TButton;
    EditTTL: TEdit;
    UpDownTTL: TUpDown;
    ScopeTab: TTabControl;
    VGrid: TStringGrid;
    EditTab: TTabControl;
    Paint: TPaintBox;
    ScrollPanel: TPanel;
    ScrollBar: TScrollBar;
    MainMenu1: TMainMenu;
    File1: TMenuItem;
    Import1: TMenuItem;
    Compile1: TMenuItem;
    Compile2: TMenuItem;
    Step1: TMenuItem;
    Run1: TMenuItem;
    OpenDialog: TOpenDialog;
    Exit1: TMenuItem;
    Remove1: TMenuItem;
    StatusPanel: TPanel;
    CurPosLabel: TLabel;
    SingleMenu: TMenuItem;
    SingleBtn: TSpeedButton;
    RewindBtn: TButton;
    MathBtn: TButton;
    OKBtn: TBitBtn;
    CancelBtn: TBitBtn;
    New1: TMenuItem;
    Rename1: TMenuItem;
    Export1: TMenuItem;
    SaveDialog: TSaveDialog;
    PopupMenu1: TPopupMenu;
    Rename2: TMenuItem;
    Remove2: TMenuItem;
    Splitter1: TSplitter;
    procedure CompileBtnClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure StepBtnClick(Sender: TObject);
    procedure RunBtnClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure UpDownTTLChanging(Sender: TObject; var AllowChange: Boolean);
    procedure ScopeTabChange(Sender: TObject);
    procedure EditTabChange(Sender: TObject);
    procedure EditTabChanging(Sender: TObject; var AllowChange: Boolean);
    procedure EditorPageTracking(Sender: TObject);
    procedure Import1Click(Sender: TObject);
    procedure CloseBtnClick(Sender: TObject);
    procedure Remove1Click(Sender: TObject);
    procedure FormPaint(Sender: TObject);
    procedure EditorCursorMove(Sender: TObject);
    procedure SingleBtnClick(Sender: TObject);
    procedure SingleMenuClick(Sender: TObject);
    procedure RewindBtnClick(Sender: TObject);
    procedure MathBtnClick(Sender: TObject);
    procedure New1Click(Sender: TObject);
    procedure Rename1Click(Sender: TObject);
    procedure Export1Click(Sender: TObject);
    procedure Remove2Click(Sender: TObject);
    procedure OKBtnClick(Sender: TObject);
    procedure EditorEscape(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure VGridDblClick(Sender: TObject);
    procedure FormDeactivate(Sender: TObject);
    procedure CancelBtnClick(Sender: TObject);
    procedure ScopeTabResize(Sender: TObject);
  private
    DebugH, DebugY : Integer;
    FRunActive : Boolean;
    FScriptProject : TACNCScriptProject;
    FOnSaveFiles : TNotifyEvent;
    FOnLoadFiles : TNotifyEvent;
    FModified : Boolean;
    ArrayForm : TArrayView;
    Editor: TACNCScriptEditor;
    FOnSave : TNotifyEvent;
    procedure DoStep;
    procedure UpdateVariableGrid;
    procedure UpdateEditorAndPointer;
    procedure BuildVariableReference;
    procedure BuildScriptReference;
    procedure LoadEditFiles(aName : string);
    procedure SaveEditFiles;
    procedure SingleBlockStatus(S : Boolean);
    procedure SetProject(aProject : TACNCScriptProject);
    function AddFileFromDisk : TACNCScript;
    procedure SetBlankUnit(Script : TACNCScript);
    procedure SetModified(AVal: Boolean);
  public
    procedure DoUpdate;
    procedure ActiveChanged; override;
    property  ScriptProject : TACNCScriptProject read FScriptProject write SetProject;
    property  OnSaveFiles : TNotifyEvent read FOnSaveFiles write FOnSaveFiles;
    property  OnLoadFiles : TNotifyEvent read FOnLoadFiles write FOnLoadFiles;
    property  Modified : Boolean read FModified write SetModified;
    property  RunActive : Boolean read FRunActive write FRunActive;
    procedure SetTTL(aTTL : Integer);
    property OnSave : TNotifyEvent read FOnSave write FOnSave;
  end;

  TEditInfo = class(TObject)
    CursorPos : TPoint;
    TopLine : Integer;
    Script : TAbstractScript;
    constructor Create;
    destructor Destroy; override;
  end;

var
  ScriptEditForm: TScriptEditForm;

implementation

{$R *.DFM}


function APoint(X, Y : Integer) : TPoint;
begin
  Result.X := X;
  Result.Y := Y;
end;

// ToDo Reject Request if Editor dirty
//
// Called of every request to run
// After running the conditions set by TTL and SingleBlock then
// Variable Grid and Editor position are updated
procedure TScriptEditForm.DoStep;
begin
  try
    ScriptProject.Run;
    if Visible then begin
      UpdateVariableGrid;
      UpdateEditorAndPointer;
      Editor.CursorPos := APoint(1, ScriptProject.LineNumber);
    end;
  except
    On E : Exception do begin
      RunActive := False;
      Application.ShowException(E);
    end;
  end;
end;

procedure TScriptEditForm.ActiveChanged;
begin
end;

// Maintians tracking of ScriptProject ActiveScript and indicates
// the next line to run.
procedure TScriptEditForm.UpdateEditorAndPointer;
var AllowChange : Boolean;
begin
  if ScriptProject.TraceScript <> nil then begin
    if ScriptProject.TraceScript <> TEditInfo(EditTab.Tabs.Objects[EditTab.TabIndex]).Script then begin
      EditTabChanging(Self, AllowChange);
      EditTab.TabIndex := EditTab.Tabs.IndexOf(ScriptProject.TraceScript.Name);
      EditTabChange(Self);
    end;
  end else begin
    EditTabChanging(Self, AllowChange);
    EditTab.TabIndex := 0;
    EditTabChange(Self)
  end;
  EditorPageTracking(Self);
end;

//
// Updates the text variables for the displayed scope
procedure TScriptEditForm.UpdateVariableGrid;
var I : Integer;
    aTag : TAbstractTag;
    Tmp, WidthMax : Integer;
begin
  if ScopeTab.TabIndex = -1 then
    Exit;

  if not ScriptProject.Compiled then
    Exit;

  if Assigned(ArrayForm) and ArrayForm.Visible then
    ArrayForm.UpdateView;

  WidthMax := 10;

  with ScopeTab.Tabs.Objects[ScopeTab.TabIndex] as TScriptVariableScope do begin
    VGrid.RowCount := Count;
    for I := 0 to Count - 1 do begin
      aTag :=  Tags[I];
      Tmp := VGrid.Canvas.TextWidth(aTag.Name);
      if Tmp > WidthMax then
        WidthMax := Tmp;
      if VGrid.Cells[0, I] <> aTag.Name then
        VGrid.Cells[0, I] := aTag.Name;
      if aTag.HasValue then begin
        if aTag is TScriptParameterTag then begin
          if ScriptProject.ActiveMethod = TScriptParameterTag(aTag).Instance then
            VGrid.Cells[1, I] := aTag.AsString
          else
            VGrid.Cells[1, I] := 'Not In Scope';
        end else
          VGrid.Cells[1, I] := aTag.AsString
      end else
        VGrid.Cells[1, I] := 'No Value';
    end;
    VGrid.ColWidths[0] := WidthMax + 10;
  end;
end;

//
// After compilation all scope typelists are linked to a tab
procedure TScriptEditForm.BuildVariableReference;
var I : Integer;
    aTag : TAbstractTag;
    Script : TAbstractScript;
begin
  ScopeTab.Tabs.Clear;
  ScopeTab.Tabs.AddObject('Global', ScriptProject.TypeList);

  for I := 0 to ScriptProject.Units.Count - 1 do begin
    Script := TAbstractScript(ScriptProject.Units[I]);
    ScopeTab.Tabs.AddObject(Script.Name, Script.TypeList);
  end;

  with ScriptProject.TypeList do begin
    for I := 0 to Count - 1 do begin
      aTag := Tags[I];
      if aTag is TScriptMethodTag then
        ScopeTab.Tabs.AddObject(aTag.Name, TScriptMethodTag(aTag).TypeList);
    end;
  end;
end;

procedure TScriptEditForm.BuildScriptReference;
var I : Integer;
begin
  for I := 0 to ScriptProject.Units.Count - 1 do
    TEditInfo(EditTab.Tabs.Objects[I]).Script := TAbstractScript(ScriptProject.Units[I]);
end;

procedure TScriptEditForm.CompileBtnClick(Sender: TObject);
var AllowChange : Boolean;
begin
  RunActive := False;
  ScriptProject.Rewind;
  try
    if Assigned(ArrayForm) then
      ArrayForm.Close;
      
    SaveEditFiles;
    ScriptProject.Compile('Main');
    SingleBlockStatus(True);
    BuildVariableReference;
    BuildScriptReference;
    UpdateVariableGrid;
  except
    on E : Exception do begin
      Application.ShowException(E);
      Editor.SetFocus;
      Editor.CursorPos := ScriptProject.ErrorPosition;
      EditTabChanging(Self, AllowChange);
      EditTab.TabIndex := ScriptProject.ErrorScript;
      EditTabChange(Self);
    end;
  end;
end;

const
  ACNCScriptExtension = 'ACNCScript';
  ACNCScriptFilter = 'Acnc Scripts|*.' + ACNCScriptExtension;
  ACNCScriptDirectory = 'ACNCScripts\';

procedure TScriptEditForm.FormCreate(Sender: TObject);
begin
  Editor:= TACNCScriptEditor.Create(Self);
  Editor.Parent := EditTab;
  Editor.Align := alClient;
  Editor.OnCursorMove := EditorCursorMove;
  Editor.OnEscape := EditorEscape;
  Editor.OnPageTracking := EditorPageTracking;
  Editor.ScrollBar := ScrollBar;

  // Add any defaults
  Editor.Font.Height := 18;
  Editor.Left := 25;
  EditTab.TabIndex := 0;
  OpenDialog.Filter := ACNCScriptFilter;
  OpenDialog.InitialDir := LocalPath + ACNCScriptDirectory;
  OpenDialog.DefaultExt := ACNCScriptExtension;

  SaveDialog.Filter := ACNCScriptFilter;
  SaveDialog.InitialDir := OpenDialog.InitialDir;
  SaveDialog.DefaultExt := ACNCScriptExtension;
end;

procedure TScriptEditForm.SetProject(aProject : TACNCScriptProject);
var Script : TACNCScript;
begin
  FScriptProject := aProject;
  if aProject.Units.Count = 0 then begin
    Script := TACNCScript.Create(ScriptProject.TagOwner);
    Script.Name := 'Main';
    Script.Project := ScriptProject;
    ScriptProject.Units.Add(Script);
    SetBlankUnit(Script);
  end;

  LoadEditFiles(TAbstractScript(ScriptProject.Units[0]).Name);
  EditTabChange(Self);
end;

procedure TScriptEditForm.StepBtnClick(Sender: TObject);
begin
  RunActive := False;
  ScriptProject.TTL := 0;
  SingleBlockStatus(True);
  DoStep;
end;

procedure TScriptEditForm.RunBtnClick(Sender: TObject);
begin
  ScriptProject.TTL := UpDownTTL.Position;
  RunActive := True;
end;

procedure TScriptEditForm.DoUpdate;
begin
  if RunActive then begin
    DoStep;
    if RunActive then // Do Step may turn off run active because of an exception
      RunActive := ScriptProject.InProcess;
  end;
end;

procedure TScriptEditForm.FormResize(Sender: TObject);
begin
  ScrollBar.Height := ScrollPanel.Height;
end;

procedure TScriptEditForm.SetTTL(aTTL : Integer);
var AllowChange : Boolean;
begin
  UpDownTTL.Position := aTTL;
  UpDownTTLChanging(Self, AllowChange);
end;

procedure TScriptEditForm.UpDownTTLChanging(Sender: TObject;
  var AllowChange: Boolean);
begin
  ScriptProject.TTL := UpDownTTL.Position;
end;

procedure TScriptEditForm.ScopeTabChange(Sender: TObject);
begin
  UpdateVariableGrid;
end;

procedure TScriptEditForm.LoadEditFiles(aName : string);
var I : Integer;
    EI : TEditInfo;
begin
  while EditTab.Tabs.Count > 0 do begin
    EditTab.Tabs.Objects[0].Free;
    EditTab.Tabs.Delete(0);
  end;

  EditTab.Tabs.Clear;
  for I := 0 to ScriptProject.Units.Count - 1 do begin
    EI := TEditInfo.Create;
    try
      EI.Script := TAbstractScript(ScriptProject.Units[I]);
      EditTab.Tabs.AddObject(EI.Script.Name, EI)
    except
      EI.Free;
    end;
  end;

  EditTab.TabIndex := EditTab.Tabs.IndexOf(aName);

  if Assigned(FOnLoadFiles) then
    OnLoadFiles(Self);
  EditTabChange(Self);
end;

procedure TScriptEditForm.SaveEditFiles;
var AllowChange : Boolean;
begin
  AllowChange := True;
  EditTabChanging(Self, AllowChange);
  if Assigned(FOnSaveFiles) then
    OnSaveFiles(Self);
end;

procedure TScriptEditForm.EditTabChange(Sender: TObject);
begin
  with EditTab do begin
    if TabIndex <> -1 then begin
      with TEditInfo(EditTab.Tabs.Objects[TabIndex]) do begin
        Editor.Text.Text := Script.Text;
        Editor.CursorPos := CursorPos;
        Editor.TopLine :=  TopLine;
        Editor.PaintPage;
      end;
    end;
  end;
end;

procedure TScriptEditForm.EditTabChanging(Sender: TObject;
  var AllowChange: Boolean);
begin
  with EditTab do begin
    if TabIndex <> -1 then begin
      with TEditInfo(EditTab.Tabs.Objects[TabIndex]) do begin
        if Editor.Modified then
          FModified := True;
        Script.Text := Editor.Text.Text;
        CursorPos := Editor.CursorPos;
        TopLine := Editor.TopLine;
      end;
    end;
  end;
end;

constructor TEditInfo.Create;
begin
  inherited;
  Script := nil;
  CursorPos := aPoint(1, 0);
  TopLine := 0;
end;

destructor TEditInfo.Destroy;
begin
  inherited Destroy;
end;


procedure TScriptEditForm.EditorPageTracking(Sender: TObject);
begin
  with Paint do begin
    Canvas.Brush.Color := Color;
    Canvas.Pen.Color := Color;
    Canvas.Ellipse(5, DebugY, 5 + DebugH, DebugY + DebugH);

    Canvas.Brush.Color := clRed;
    Canvas.Pen.Color := clBlack;
    DebugH := ((Editor.Font.Height * 2) div 3) - 2;
    DebugY := Editor.YCursorPixelPos(ScriptProject.LineNumber) + (DebugH div 2) + 2;
    Canvas.Ellipse(5, DebugY, 5 + DebugH, DebugY + DebugH);
  end;
end;

procedure TScriptEditForm.SetBlankUnit(Script : TACNCScript);
begin
  Script.Add('Interface');
  Script.Add('');
  Script.Add('Implementation');
  Script.Add('');
end;

procedure TScriptEditForm.Import1Click(Sender: TObject);
var Script : TACNCScript;
begin
{  if Assigned(FOnAddFile) then
    OnAddFile(Self); }
  Script := AddFileFromDisk;
  if Assigned(Script) then
    LoadEditFiles(Script.Name);
end;

procedure TScriptEditForm.Export1Click(Sender: TObject);
var Script : TAbstractScript;
begin
  Script := TAbstractScript(ScriptProject.Units[EditTab.TabIndex]);
  with SaveDialog do begin
    FileName := Script.Name + '.' + ACNCScriptExtension;
    if Execute then
      Script.SaveToFile(FileName);
  end;
end;

function TScriptEditForm.AddFileFromDisk : TACNCScript;
var P : Integer;
begin
  Result := TACNCScript.Create(ScriptProject.TagOwner);
  try
    if OpenDialog.Execute then begin
      Result.FileName := OpenDialog.FileName;
      Result.Name := StripPath(OpenDialog.FileName);
      Result.Project := ScriptProject;
      P := Pos('.', Result.Name);
      Delete(Result.Name, P, Length(Result.Name));
      if not FileExists(OpenDialog.FileName) then begin
        SetBlankUnit(Result);
        try
          Result.SaveToFile(OpenDialog.FileName);
        except
          on E : Exception do Application.ShowException(E);
        end;
      end;

      Result.LoadFromFile(Result.FileName);
      ScriptProject.Units.Add(Result);
    end else begin
      Result.Free;
      Result := nil;
    end;
  except
    on E : Exception do begin
      Application.ShowException(E);
      Result.Free;
      Result := nil;
    end;
  end;
end;

procedure TScriptEditForm.CloseBtnClick(Sender: TObject);
begin
  ModalResult := mrOK;
  Close;
end;

procedure TScriptEditForm.Remove1Click(Sender: TObject);
var I : Integer;
begin
  if ScriptProject.Units.Count > 1 then begin
    I := TScriptSelectForm.SelectScript(ScriptProject.Units);
    if I <> -1 then begin
      TAbstractScript(ScriptProject.Units[I]).Free;
      ScriptProject.Units.Delete(I);
    end;
    LoadEditFiles(TAbstractScript(ScriptProject.Units[0]).Name);
    BuildScriptReference;
    BuildVariableReference;
  end;
end;

procedure TScriptEditForm.FormPaint(Sender: TObject);
begin
  if ScriptProject <> nil then
    EditorPageTracking(Self);
end;

procedure TScriptEditForm.EditorCursorMove(Sender: TObject);
begin
  CurPosLabel.Caption := Format('%3d:%3d', [Editor.CursorPos.X, Editor.CursorPos.Y]);
end;

procedure TScriptEditForm.SingleBtnClick(Sender: TObject);
begin
  SingleBlockStatus(SingleBtn.Down = True);
end;

procedure TScriptEditForm.SingleMenuClick(Sender: TObject);
begin
  SingleBlockStatus(not SingleMenu.checked)
end;

procedure TScriptEditForm.SingleBlockStatus(S : Boolean);
begin
  ScriptProject.SingleBlock := S;
  SingleBtn.Down := S;
  SingleMenu.Checked := S;
end;

procedure TScriptEditForm.RewindBtnClick(Sender: TObject);
begin
  RunActive := False;
  ScriptProject.Rewind;
end;

procedure TScriptEditForm.MathBtnClick(Sender: TObject);
begin
//  MathPartial.Show;
end;

resourcestring
  ScriptNameForLang = 'New Name For Script';
  ScriptRenameLang = 'Rename Script';
  NewLang = 'New';

procedure TScriptEditForm.New1Click(Sender: TObject);
var Script : TACNCScript;
begin
  Script := TACNCScript.Create(ScriptProject.TagOwner);
  Script.Name := NewLang;
  SetBlankUnit(Script);
  Script.Project := ScriptProject;
  ScriptProject.Units.Add(Script);
  LoadEditFiles(Script.Name);
end;

procedure TScriptEditForm.Rename1Click(Sender: TObject);
  function ProjectContains(aName : string) : Boolean;
  var I : Integer;
  begin
    Result := True;
    for I := 0 to ScriptProject.Units.Count - 1 do begin
      if CompareText(aName, TAbstractScript(ScriptProject.Units[I]).Name) = 0 then
        Exit;
    end;
    Result := False;
  end;

var Script : TAbstractScript;
    NewName : string;
begin
  if EditTab.Tabs.Count > 1 then begin
    Script := TEditInfo(EditTab.Tabs.Objects[EditTab.TabIndex]).Script;
    NewName := Script.Name;
    if InputQuery(ScriptRenameLang, ScriptNameForLang + Name, NewName) then
      if (Trim(NewName) <> '') and (CompareText(NewName, Script.Name) <> 0) then
        if not ProjectContains(NewName) then begin
          Script.Name := NewName;
          LoadEditFiles(NewName);
        end;
  end;
end;

procedure TScriptEditForm.Remove2Click(Sender: TObject);
var I : Integer;
begin
  if Acknowledge.AreYouSure('Do you realy want to remove this?') then begin
    if ScriptProject.Units.Count > 1 then begin
      I := EditTab.TabIndex;
      if I <> -1 then begin
        TAbstractScript(ScriptProject.Units[I]).Free;
        ScriptProject.Units.Delete(I);
      end;
      LoadEditFiles(TAbstractScript(ScriptProject.Units[0]).Name);
      BuildScriptReference;
      BuildVariableReference;
    end;
  end;
end;

procedure TScriptEditForm.OKBtnClick(Sender: TObject);
var AllowChange : Boolean;
begin
  EditTabChanging(Self, AllowChange);
  if Assigned(FOnSave) then
    FOnSave(Self);
end;

procedure TScriptEditForm.EditorEscape(Sender: TObject);
begin
  Close;
end;

procedure TScriptEditForm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  if Assigned(ArrayForm) then begin
//    ArrayForm.Close;
    ArrayForm.Release;
  end;

  if Editor.Modified then
    FModified := True;

  if (ModalResult = mrCancel) and Modified then
    if not Acknowledge.AreYouSure(DataLossVerify) then
      Action := caNone;
end;

procedure TScriptEditForm.VGridDblClick(Sender: TObject);
begin
  if ScopeTab.TabIndex = -1 then
    Exit;

  if not ScriptProject.Compiled then
    Exit;

  with ScopeTab.Tabs.Objects[ScopeTab.TabIndex] as TScriptVariableScope do begin
    if Tags[VGrid.Row] is TMethodArrayTag then begin
      if not Assigned(ArrayForm) then
        ArrayForm := TArrayView.Create(Application);
      ArrayForm.SetArray(TMethodArrayTag(Tags[VGrid.Row]));
      ArrayForm.Show;
    end;
  end;
end;




function  TACNCScriptEditor.ParseLine(aText : string; Line : Integer) : string;
var I : Integer;
    FontOn : Boolean;
begin
//  Result := inherited ParseLine(aText, Line);
//  Exit;
  if Text.Count = 0 then
    Exit;

  Parser.Text := aText;
  Parser.Rewind;
  FontOn := False;

  Result := '';
  while Parser.Peek <> #0 do begin
    I := Parser.Position;
    case Parser.SymbolType of
      slInvalid :  if (Length(Parser.LastToken) > 0) and
                      (Parser.LastToken[1] = '''') then begin
                     AddFont(Result, [], $000000C0);
                     FontOn := False;
                   end else if FontOn then begin
                     AddFont(Result, [], 0);
                     FontOn := False;
      end;

      slComment :  begin
                     AddFont(Result, [fsItalic], $00b03000);
                     Result := Result + Copy(Parser[0], I, Length(Parser[0]));
                     Exit;
      end;
    else
      if not FontOn then begin
        AddFont(Result, [fsBold], 0);
        FontOn := True;
      end;
    end;
    Result := Result + Copy(Parser[0], I, Parser.Position - I);
  end;
end;

constructor TACNCScriptEditor.Create(AOwner : TComponent);
begin
  inherited Create(aOwner);
  Parser := TACNCScriptParser.Create;
  Parser.DontStripComments := True;
end;

procedure TScriptEditForm.FormDeactivate(Sender: TObject);
begin
//  Close;
end;

procedure TScriptEditForm.CancelBtnClick(Sender: TObject);
begin
  if Editor.Modified then
    FModified := True;
  Close;
end;

procedure TScriptEditForm.ScopeTabResize(Sender: TObject);
begin
  VGrid.ColWidths[1] := VGrid.Width - VGrid.ColWidths[0] - 4;
end;

procedure TScriptEditForm.SetModified(AVal: Boolean);
begin
  Editor.Modified:= AVal;
  FModified:= AVal;
end;

end.

