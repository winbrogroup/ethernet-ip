unit ArrayViewForm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Grids, ScriptTypeInfo, StdCtrls, Buttons;

type
  TArrayView = class(TForm)
    AGrid: TStringGrid;
    Panel1: TPanel;
    BitBtn1: TBitBtn;
  private
    ArrayTag : TMethodArrayTag;
  public
    procedure SetArray(AMATag : TMethodArrayTag);
    procedure UpdateView;
  end;

implementation

{$R *.DFM}

procedure TArrayView.SetArray(AMATag : TMethodArrayTag);
var I : Integer;
begin
  ArrayTag := AMATag;
  Caption := ArrayTag.Name;
  AGrid.RowCount := ArrayTag.Dimensions[0] + 1;
  AGrid.ColCount := 2;
  for I := 0 to ArrayTag.Dimensions[0] - 1 do begin
    AGrid.Cells[0, I + 1] := IntToStr(I);
  end;

  if ArrayTag.OrdinateCount > 1 then begin
    AGrid.ColCount := ArrayTag.Dimensions[1] + 1;
    for I := 0 to ArrayTag.Dimensions[0] - 1 do begin
      AGrid.Cells[I + 1, 0] := IntToStr(I);
    end;
  end;
end;

procedure TArrayView.UpdateView;
var I, J : Integer;
    Tmp : string;
begin
  if Assigned(ArrayTag) then begin
    if ArrayTag.OrdinateCount = 1 then
      for I := AGrid.TopRow - 1 to ArrayTag.Dimensions[0] - 1 do begin
        if I > AGrid.TopRow + AGrid.VisibleRowCount then
          Break;
        Tmp := ArrayTag.AsArrayString[I];
        if Tmp <> AGrid.Cells[1, I + 1] then
          AGrid.Cells[1, I + 1] := Tmp;
      end
    else
      for J := AGrid.LeftCol - 1 to ArrayTag.Dimensions[1] - 1 do begin
        if J > AGrid.LeftCol + AGrid.VisibleColCount then
          Break;

        for I := AGrid.TopRow - 1 to ArrayTag.Dimensions[0] - 1 do begin
          if I > AGrid.TopRow + AGrid.VisibleRowCount then
            Break;
          Tmp := ArrayTag.AsArrayString[I + (J * ArrayTag.IndexOfOrdinate(1))];
          if Tmp <> AGrid.Cells[J + 1, I + 1] then
            AGrid.Cells[J + 1, I + 1] := Tmp;
        end;
      end;
  end;
end;

end.
