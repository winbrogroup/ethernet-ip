unit ScriptFile;

interface

uses
  Classes, SysUtils, Tokenizer, ScriptTypeInfo, ACNCScript, CNC32;

type
  TACNCScriptFile = class(TStringTokenizer)
  private
    TagOwner : TTagOwner;
    function GetScriptCount : Integer;
    function GetScript(Index : Integer) : TACNCScript;
    procedure Clean;
  public
    Project : TACNCScriptProject;
    constructor Create(aTagOwner : TTagOwner);
    destructor Destroy; override;
    procedure ReadFromStrings;
    procedure WriteToStrings;
    property ScriptCount : Integer read GetScriptCount;
    property Scripts [Index : Integer] : TACNCScript read GetScript;
  end;

  EACNCScriptFile = class(Exception);

implementation

resourcestring
  InvalidDataFormat = 'ACNCScript File Invalid Data Format';
  InvalidDataConversion = 'ACNCScript File Invalid Data Conversion';

constructor TACNCScriptFile.Create(aTagOwner : TTagOwner);
begin
  inherited Create;
  TagOwner := aTagOwner;
end;

destructor TACNCScriptFile.Destroy;
begin
  inherited;
end;

procedure TACNCScriptFile.Clean;
begin
  if Assigned(Project) then
    while Project.Units.Count > 0 do begin
      TObject(Project.Units[0]).Free;
      Project.Units.Delete(0);
    end;
end;


const
  ScriptDesig = 'ACNCScriptFile';
  ScriptNameDesig = 'ACNCScriptFileName';


procedure TACNCScriptFile.ReadFromStrings;
  function ReadInteger : Integer;
  begin
    try
      Result := StrToInt(Self.Read);
    except
      raise EACNCScriptFile.Create(InvalidDataConversion);
    end;
  end;

  procedure ExpectedToken(aToken : string);
  begin
    if Self.Read <> aToken then
      raise EACNCScriptFile.Create(InvalidDataFormat);
  end;

var  StartLine, I, ScriptCount, C : Integer;
begin
  Self.Clean;
  Self.Rewind;

  if Self.Count = 0 then begin
    Self.Add('');
    Exit;
  end;

  if EndOfStream then
    Exit;

  ExpectedToken(ScriptDesig);
  ScriptCount := ReadInteger;

  for I := 0 to ScriptCount - 1 do begin
    ExpectedToken(ScriptNameDesig);
    Project.Units.Add(TACNCScript.Create(TagOwner));
    Scripts[I].Name := Self.Read;
    Scripts[I].Project := Project;

    StartLine := Self.Line + 1;
    FindToken(ScriptNameDesig);
    Self.Push;

    with Scripts[I] do
      for C := StartLine to Self.Line - 1 do
        Add(Self[C]);
  end;
end;

procedure TACNCScriptFile.WriteToStrings;
var I, C : Integer;
begin
  Self.Clear;
  Self.Add(ScriptDesig + ' ' + IntToStr(ScriptCount));

  for I := 0 to ScriptCount - 1 do begin
//    Self.Add('');
    Self.Add(ScriptNameDesig + ' ' + Scripts[I].Name);
    for C := 0 to Scripts[I].Count - 1 do begin
      Self.Add(Scripts[I].Strings[C]);
    end;
  end;
end;

function TACNCScriptFile.GetScriptCount : Integer;
begin
  Result := Project.Units.Count;
end;

function TACNCScriptFile.GetScript(Index : Integer) : TACNCScript;
begin
  Result := TACNCScript(Project.Units[Index]);
end;

end.
 