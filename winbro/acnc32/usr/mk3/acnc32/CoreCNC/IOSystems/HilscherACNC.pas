unit HilscherACNC;

interface

uses CNCTypes, CNC32, CoreCNC, Hilscher, SysUtils, FaultRegistration;

type
  HilscherIOSubSystem = class(TAbstractIOSubSystem)
  private
    Hdb : THilscherIO_DLL;
    BoardInfo : THilscherBoardInfo;
    DriverInfo : THilscherDriverInfo;
    BoardVersionInfo : THilscherBoardVersionInfo;
    FirmwareVersionInfo : THilscherFirmwareInfo;
    TaskInfo : THilscherTaskInfo;
    RCSInfo : THilscherRCSInfo;
    DeviceInfo : THilscherDeviceInfo;
    IOInfo : THilscherIOInfo;
    ParentInfoPageCount : Integer;
    DeviceOK : Boolean;
    FLastError : string;
    LastDeviceError: Boolean;

    NoReset: Boolean;
    procedure DoError(aText : string; Level : THilscherError);
  protected
    function GetInfoPageContent(aPage : Integer) : string; override;
  public
    constructor Create(aName : string; var Params : string); override;
    destructor Destroy; override;
    procedure UpdateShadowToReal; override;
    procedure UpdateRealToShadow; override;
    function OK : Boolean; override;
    procedure Reset; override;
    function LastError : string; override;
  end;

  HilscherNoResetIOSubSystem = class(HilscherIOSubSystem)
  private
  public
    constructor Create(aName : string; var Params : string); override;
  end;

  TDPRIO = class(TAbstractIOSubSystem)
  protected
    DPRin : PAIOMap;
    DPRout : PAIOMap;
  public
    constructor Create(aName : string; var Params : string); override;
    procedure UpdateShadowToReal; override;
    procedure UpdateRealToShadow; override;
  end;



implementation

resourcestring
  BoardInfoLang = 'Board';
  DriverInfoLang = 'Driver';
  BoardVersionInfoLang = 'Board Version';
  FirmwareVersionInfoLang = 'Firmware version';
  TaskInfoLang = 'Tasks';
  RCSInfoLang = 'RCS';
  DeviceInfoLang = 'Devices';
  IOInfoLang = 'IO';
  CommStateLang = 'CommState';


constructor HilscherIOSubSystem.Create(aName : string; var Params : string);
var Board: THilscherDevBoard;
begin
  inherited Create(aName, Params);

  EventLog('********** DO ME A LEMON ************');
  if (FInputBase > Ord(High(THilscherDevBoard))) or
     (FInputBase < Ord(Low(THilscherDevBoard))) then
    raise ECNCInstallFault.CreateFmt('Invalid board address for [%s]', [Name]);

  Board:= hdb1;

  if Params <> '' then begin
    Board:= THilscherDevBoard(StrToIntDef(ReadDelimited(Params), 0));
  end;

  try
    HDb := THilscherIO_DLL.Create( Board, NoReset);
    HDb.OnErrorEvent := DoError;
  except
    on E : Exception do begin
      raise ECNCInstallFault.CreateFmt('Hilscher Driver failed [%s]', [E.Message]);
    end;
  end;

  ParentInfoPageCount := FInfoPages.Count;
  FInfoPages.Add(BoardInfoLang);
  FInfoPages.Add(DriverInfoLang);
  FInfoPages.Add(BoardVersionInfoLang);
  FInfoPages.Add(FirmwareVersionInfoLang);
  FInfoPages.Add(TaskInfoLang);
  FInfoPages.Add(RCSInfoLang);
  FInfoPages.Add(DeviceInfoLang);
  FInfoPages.Add(IOInfoLang);
  FInfoPages.Add(CommStateLang);
  DeviceOK := True;
end;

destructor HilscherIOSubSystem.Destroy;
begin
  if Assigned(Hdb) then
    Hdb.Free;

  inherited Destroy;
end;

procedure HilscherIOSubSystem.Reset;
begin
  if not DeviceOK then begin
    Hdb.Reset(hrtWarmStart);
    FLastError := '';
    DeviceOK := True;
  end;
end;

function HilscherIOSubSystem.LastError : string;
begin
  Result := FLastError;
end;

procedure HilscherIOSubSystem.UpdateShadowToReal;
begin
  WriteTags;
  if OK then
    HDb.WriteToDevice(ShadowOut, FInputSize);

  if HDb.DeviceError <> LastDeviceError then
    SysObj.FaultInterface.SetFault(SysObj.IO, CoreFaults[cfSysWarning], ['bHostFlags error bit set'], nil);

  LastDeviceError := HDb.DeviceError;
end;

procedure HilscherIOSubSystem.UpdateRealToShadow;
begin
  if OK then
    HDb.ReadFromDevice(ShadowIn, FOutputSize);

  ReadTags;
end;

{ changed to allow reseting of IO errors }
procedure HilscherIOSubSystem.DoError(aText : string; Level : THilscherError);
begin
  if Level <> heNone then begin
    FLastError := aText;
    if OK then begin
      case Level of
        heWarning : begin
          EventLog(aText);
          DeviceOK := False;
        end;
        heSevere : begin
          EventLog(aText);
          DeviceOK := False;
        end;

        heCritical : begin
          EventLog(aText);
          DeviceOK := False;
        end;
      else
        DeviceOK := True;
      end;
    end;
  end else begin
    DeviceOK := True;
  end;
end;

function HilscherIOSubSystem.OK : Boolean;
begin
  Result := DeviceOK;
end;

function HilscherIOSubSystem.GetInfoPageContent(aPage : Integer) : string;
var Brd : THilscherDevBoard;
begin
  Result := '';

  if aPage < ParentInfoPageCount then begin
    Result := inherited GetInfoPageContent(aPage);
  end else begin
    aPage := aPage - ParentInfoPageCount;
    case aPage of
      0 : begin
        BoardInfo := HDb.BoardInfo;
        Result :=
          'Last Error' + CarRet + LastError + CarRet +
          'abDriverVersion' + CarRet + BoardInfo.abDriverVersion + CarRet;
        for Brd := Low(THilscherDevBoard) to High(THilscherDevBoard) do begin
          Result := Result + 'Board' + CarRet + Format('%d', [Ord(Brd)]) + CarRet +
          'usNumber' + CarRet + IntToStr(BoardInfo.Boards[Brd].usNumber) + CarRet +
          'usAvailable' + CarRet + BooleanIdent[BoardInfo.Boards[Brd].usAvailable <> 0] + CarRet +
          'usPhysicalBoardAddress' + CarRet + IntToStr(BoardInfo.Boards[Brd].usPhysicalBoardAddress) + CarRet +
          'usIrqNumber' + CarRet + IntToStr(BoardInfo.Boards[Brd].usIrqNumber) + CarRet +
          CarRet + CarRet;
        end;
      end;

      1 : begin
        DriverInfo := HDb.DriverInfo;
        Result :=
          'ulOpenCnt'     + CarRet + IntToStr(DriverInfo.ulOpenCnt) + CarRet +
          'ulCloseCnt'    + CarRet + IntToStr(DriverInfo.ulCloseCnt) + CarRet +
          'ulReadCnt'     + CarRet + IntToStr(DriverInfo.ulReadCnt) + CarRet +
          'ulWriteCnt'    + CarRet + IntToStr(DriverInfo.ulWriteCnt) + CarRet +
          'ulIRQCnt'      + CarRet + IntToStr(DriverInfo.ulIRQCnt) + CarRet +
          'bInitMsgFlag'  + CarRet + IntToStr(DriverInfo.bInitMsgFlag) + CarRet +
          'bReadMsgFlag'  + CarRet + IntToStr(DriverInfo.bReadMsgFlag) + CarRet +
          'bWriteMsgFlag' + CarRet + IntToStr(DriverInfo.bWriteMsgFlag) + CarRet +
          'bLastFunction' + CarRet + IntToStr(DriverInfo.bLastFunction) + CarRet +
          'bWriteState'   + CarRet + IntToStr(DriverInfo.bWriteState) + CarRet +
          'bReadState'    + CarRet + IntToStr(DriverInfo.bReadState) + CarRet +
          'bHostFlags'    + CarRet + IntToStr(DriverInfo.bHostFlags) + CarRet +
          'bMyDevFlags'   + CarRet + IntToStr(DriverInfo.bMyDevFlags) + CarRet +
          'bExIOFlag'     + CarRet + IntToStr(DriverInfo.bExIOFlag) + CarRet +
          'ulExIOCnt'     + CarRet + IntToStr(DriverInfo.ulExIOCnt) + CarRet;
      end;

      2 : begin
        BoardVersionInfo := HDb.BoardVersionInfo;
        Result :=
          'ulOpenCnt'       + CarRet + IntToStr(BoardVersionInfo.ulDate) + CarRet +
          'ulDeviceNo'      + CarRet + IntToStr(BoardVersionInfo.ulDeviceNo) + CarRet +
          'ulSerialNumber'  + CarRet + IntToStr(BoardVersionInfo.ulSerialNumber) + CarRet +
          'abPcOsName0'     + CarRet + Copy(BoardVersionInfo.abPcOsName0, 1, 4) + CarRet +
          'abPcOsName1'     + CarRet + Copy(BoardVersionInfo.abPcOsName1, 1, 4) + CarRet +
          'abPcOsName2'     + CarRet + Copy(BoardVersionInfo.abPcOsName2, 1, 4) + CarRet +
          'abOEMIdentifier' + CarRet + Copy(BoardVersionInfo.abOEMIdentifier, 1, 4) + CarRet;
      end;

      3 : begin
        FirmwareVersionInfo := HDb.FirmwareVersionInfo;
        Result :=
          'abFirmwareName'    + CarRet + FirmwareVersionInfo.abFirmwardName + CarRet +
          'abFirmwareVersion' + CarRet + FirmwareVersionInfo.abFirmwareVersion + CarRet;
      end;

      4 : begin
        TaskInfo := HDb.TaskInfo;
        Result :=
          'abFirmwardName' + CarRet + TaskInfo.abTaskName + CarRet +
          'abFirmwardName' + CarRet + IntToStr(TaskInfo.bTaskCondition) + CarRet;
      end;

      5 : begin
        RCSInfo := HDb.RCSInfo;
        Result :=
          'usRcsVersion'   + CarRet + IntToStr(RCSInfo.usRcsVersion) + CarRet +
          'bRcsError'      + CarRet + IntToStr(RCSInfo.bRcsError) + CarRet +
          'bHostWatchDog'  + CarRet + IntToStr(RCSInfo.bHostWatchDog) + CarRet +
          'bDevWatchDog'   + CarRet + IntToStr(RCSInfo.bDevWatchDog) + CarRet +
          'bSegmentCount'  + CarRet + IntToStr(RCSInfo.bSegmentCount) + CarRet +
          'bDeviceAddress' + CarRet + IntToStr(RCSInfo.bDeviceAddress) + CarRet +
          'bDriverType'    + CarRet + IntToStr(RCSInfo.bDriverType) + CarRet;
      end;

      6 : begin
        DeviceInfo := HDb.DeviceInfo;
        Result :=
          'bDpmSize'         + CarRet + IntToStr(DeviceInfo.bDpmSize) + CarRet +
          'bDevType'         + CarRet + IntToStr(DeviceInfo.bDevType) + CarRet +
          'bDevModel'        + CarRet + IntToStr(DeviceInfo.bDevModel) + CarRet +
          'abDevIndentifier' + CarRet + DeviceInfo.abDevIndentifier + CarRet;
      end;

      7 : begin
        IOInfo := HDb.IOInfo;
        Result :=
          'bComBit'         + CarRet + IntToStr(IOInfo.bComBit) + CarRet +
          'bIOExchangeMode' + CarRet + IntToStr(IOInfo.bIOExchangeMode) + CarRet +
          'ulExchangeCnt'   + CarRet + IntToStr(IOInfo.ulExchangeCnt) + CarRet;
      end;

      8 : begin
        Result :=
          'usMode'          + CarRet + IntToStr(HDb.CommState.usMode) + CarRet +
          'usStateFlag'     + CarRet + IntToStr(HDb.CommState.usStateFlag) + CarRet +
          'bGlobalBits'     + CarRet + '$' + IntToHex(Byte(HDb.CommState.bGlobalBits), 2) + CarRet +
          'bIBM_State'      + CarRet + '$' + IntToHex(Byte(HDb.CommState.bIBM_State), 2) + CarRet +
          'bErr_Dev_Adr'    + CarRet + IntToStr(HDb.CommState.bErr_Dev_Adr) + CarRet +
          'bError_Event'    + CarRet + IntToStr(HDb.CommState.bErr_Event) + CarRet +
          'DefectiveCycles' + CarRet + IntToStr(HDb.CommState.usNumOfDefectiveDataCycles) + CarRet +
          'NetwordReinits'  + CarRet + IntToStr(HDb.CommState.usNumOfNetworkReinits) + CarRet +
          '*RetryRate'      + CarRet + IntToStr(HDb.RetryRate) + CarRet;
//          'abS1_cfg'        + CarRed + HexArray(HDb.CommState.
//          'abS1_state'      + CarRed + HexArray(HDb.CommState.
//          'abS1_diag'       + CarRed + HexArray(HDb.CommState.
      end;
    else
      Result := 'Unknown page' + CarRet + CarRet;
    end;
  end;
end;

constructor TDPRIO.Create(aName : string; var Params : string);
begin
  inherited Create(aName, Params);
  DPRin  := Pointer(FInputBase);
  DPROut := Pointer(FOutputBase);
end;

procedure TDPRIO.UpdateShadowToReal;
begin
  WriteTags;
  System.Move(ShadowOut^, DPROut^, FOutputSize);
end;

procedure TDPRIO.UpdateRealToShadow;
begin
  System.Move(DPRIn^, ShadowIn^, FInputSize);
  ReadTags;
end;



{ HilscherNoResetIOSubSystem }

constructor HilscherNoResetIOSubSystem.Create(aName: string; var Params: string);
begin
  NoReset:= True;
  inherited;
end;

initialization
  RegisterIOClass(HilscherIOSubSystem);
  RegisterIOClass(HilscherNoResetIOSubSystem);
end.
