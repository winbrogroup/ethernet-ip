unit BaseKeyBoards;

interface

uses
Windows, Messages, WinProcs,SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
ExtCtrls, Buttons, stdctrls, comctrls;

resourcestring
Altkeycaption = 'Alt';
Ctrlkeycaption = 'Ctrl';

Tabkeycaption = 'Tab';
CapsKeyCaption   = 'Caps';
ShiftKeyCaption = 'Shift';
EnterKeyCaption = 'Enter';
SpaceKeyCaption = ' ';
EscapeKeyCaption = 'ESC';

const
  DefaultWidth  = 45;
  DefaultHeight = 45;
  DefaultDepth  = 3;
  MAX_ROWS = 5;
  MAX_KEYS = 20;



{$resource fpad.res}
type
// keyboard Key Component
  TButtonDirection = (bdBottomUp, bdLeftUp, bdNone, bdRightUp, bdTopUp);
  TDecSeperator    = (dsPoint,dsComma,dsDoublePoint,dsMinus);
  TLEDColor        = (lcAqua, lcBlue, lcFuchsia, lcGray, lcLime, lcRed, lcWhite, lcYellow);
  TNumGlyphs       = 1..4;



  TKBKey = class(TGraphicControl)
  private
    FBeveled:          boolean;
    FBorderStyle:      TBorderStyle;
    FButtonDirection:  TButtonDirection;
    FColor:            TColor;
    FColorHighlight:   TColor;
    FColorLED:         TLEDColor;
    FColorShadow:      TColor;
    FDepth:            integer;
    FDown:             boolean;
    FFont:             TFont;
    FShiftFont:         TFont;
    FGlyph:            TBitmap;
    FNumGlyphs:        TNumGlyphs;
    FShowLED:          Boolean;
    FStateOn:          Boolean;
    FSwitching:        Boolean;
    FShowShiftChar:    Boolean;
    FShiftCaption:     String;
    FMainCaption:      String;
    FCanRepeat:        Boolean;


    FOnClick:          TNotifyEvent;

  protected
    procedure Paint;  override;
    procedure MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
       override;
    procedure MouseUp(Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
       override;

    procedure SetBeveled(newValue: boolean);
    procedure SetBorderStyle(newBorderStyle: TBorderStyle);
    procedure SetButtonDirection(NewDirection: TButtonDirection);
    procedure SetMainCaption(Value : String);
    procedure SetColor(newColor: TColor);
    procedure SetColorLED(newColor: TLEDColor);
    procedure SetDepth(newValue: integer);
    procedure SetFont(newFont: TFont);
    procedure SetShiftFont(newFont: TFont);
    procedure SetGlyph(newGlyph: TBitmap);
    procedure SetNumGlyphs(newNumGlyphs: TNumGlyphs);
    procedure SetShowLED(newValue: boolean);
    procedure SetStateOn(newValue: boolean);
    procedure SetShowShiftChar(Value : Boolean);
    procedure SetShiftCaption(Value : String);
    procedure DrawBorder(Dest:TRect);
    procedure DrawCaption(Dest:TRect);
    procedure DrawShiftCaption(Dest:TRect);
    procedure DrawGlyph(Dest:TRect);
    procedure DrawLED(var Dest:TRect);

  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;


  published
    property Beveled: boolean read FBeveled write SetBeveled;
    property BorderStyle: TBorderStyle read FBorderStyle write SetBorderStyle;
    property ButtonDirection: TButtonDirection read FButtonDirection write SetButtonDirection;
    property MainCaption : String read FMainCaption write SetMainCaption;
    property ShiftCaption: String read FShiftCaption write SetShiftCaption;
    property Color: TColor read FColor write SetColor;
    property ColorLED: TLEDColor read FColorLED write SetColorLED;
    property Depth: integer read FDepth write SetDepth;
    property Enabled;
    property Font: TFont read FFont write SetFont;
    property ShiftFont:TFont read FShiftFont write SetShiftFont;
    property Glyph: TBitmap read FGlyph write SetGlyph;
    property NumGlyphs: TNumGlyphs read FNumGlyphs write SetNumGlyphs default 1;
    property ParentFont;
    property ParentShowHint;
    property ShowHint;
    property ShowLED: boolean read FShowLED write SetShowLED;
    property StateOn: boolean read FStateOn write SetStateOn;
    property Switching: boolean read FSwitching write FSwitching;
    property ShowShiftedCharacter : Boolean read FShowShiftChar write SetShowShiftChar;
    property CanRepeat : Boolean read FCanRepeat write FCanRepeat;
    property Temp : Boolean read FCanRepeat write FCanRepeat;
    property Visible;
    property OnClick;
    property OnMouseDown;
    property OnMouseMove;
    property OnMouseUp;
  end;


type
  // type for keyboard buttons
  TKeys = array[1..MAX_ROWS, 1..MAX_KEYS] of TKBKey;
  TKeyMode = (kmDown,kmUp,kmMomentary);

  // type for special keys
  TSpecialKey = (skTab, skCaps, skLeftShift, skRightShift, skEnter, skBackspace, skEscape,
skF1, skF2, skF3, skF4, skF5, skF6, skF7, skF8, skF9, skF10, skF11, skF12);

  // keyboard mode
  TKbMode = (kbmAlphaNumeric, kbmAlphaNumericFunction);

  // type for Options property
  TOptionItems = (DisableTab, DisableCaps, DisableLeftShift, DisableRightShift, DisableSpace,
                  DisableEnter, DisableBackspace, DisableEscape, HideTab, HideCaps,
  HideLeftShift, HideRightShift, HideSpace, HideEnter,
  HideBackspace, HideEscape, BoldShiftIndicator,DisableCtrl,HideCtrl,DisableAlt,HideAlt);
  TOptions = set of TOptionItems;
  TKeyset = set of Char;
  KeyDataP = ^ TKeyData;
  TKeyData = record
            NKey : TKbKey;
            Row : Integer;
            Col : Integer;
            Output : Byte;
            isSpecial : Boolean;
  end;



  // event types
  TKeyEvent = procedure(Sender: TObject; Key: AnsiChar) of object;
  TSpecialKeyEvent = procedure(Sender: TObject; SpecialKey: TSpecialKey) of object;
  TShiftStateChange = procedure(Sender: TObject; ShiftOn: Boolean) of object;
  TCapsStateChange = procedure(Sender: TObject; CapsOn: Boolean) of object;
  TMultiKeyEvent = procedure (Sender: TObject; KeyCode: Integer) of object;

// Function Key  Component
  TBaseFPad = class(TCustomPanel)
  private
    //KeyState      : TKeyboardState;
    FSelectState  : Boolean;
    FuncKeyList   : TList;
    NumKeyList    : Tlist;
    FuncPage      : TPanel;
    FTargetMode                : Boolean;
    FExternalKeyMode    : TKeyMode; // for external operation Up,Down,Momentary
    FExternalKeyCode    : Integer;
    FExternalShift      : TShiftState;
    FChangeCursorKeys   : Boolean;
    FNavigationKeys : Boolean;
    FOnMultikey         : TMultiKeyEvent;

    NumPage                    : TPanel;
    RepeatDelTimer             : TTimer;
    KeyLitTimer                : TTimer;
    isRepeating                : Boolean;
    CurrentKey                 : TKBKey;
    FuncKey                    : TKBKey;
    NumKey                     : TKBKey;
    CurrentKeyColor            : TColor;

    CLeftCode : Integer;
    CRightCode : Integer;
    CUpCode : Integer;
    CDownCode : Integer;

    procedure SendKey(VCode: Integer;Shift : TShiftState;Mode : TKeyMode);
    procedure RepTimerOut(Sender: TObject);
    procedure KeyLitTimerOut(Sender: TObject);
    procedure DoMultiKeyPressed(Sender : TObject;KeyCode : Integer);
    //procedure ToggleState(Ctrl : word);
    //function  State(Ctrl : Word) : boolean;
  protected
    FOnKey: TKeyEvent;
    procedure SetOnMultiKey(Value : TMultiKeyEvent);
    procedure SetTargetMode(Value : Boolean);
    procedure SetModifiedCursorKeys(Value : Boolean);
    procedure SetNavigationKeys(Nav : Boolean);
  public
    FocusControl :TWinControl;
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure Resize; override;
    procedure FKeyMouseDown(Sender: TObject; Button: TMouseButton;
             Shift: TShiftState; X, Y: Integer);

    procedure KeyMouseUp(Sender: TObject; Button: TMouseButton;
             Shift: TShiftState; X, Y: Integer);

    published
    property SelectState  : Boolean read FSelectState;
    property ExternalMode : Boolean read FTargetMode write SetTargetMode;
    property ExternalShift : TShiftState read FExternalShift;
    property ExternalKeyMode : TKeyMode read FExternalKeyMode;
    property ExternalKeyCode : Integer read FExternalKeyCode;
    property ChangeCursorKeys : Boolean read FChangeCursorKeys write SetModifiedCursorKeys;

    property OnKey: TKeyEvent read FOnKey write FOnKey;
    property OnMultiKey : TMultiKeyEvent read FOnMultikey write setOnMultiKey;
    property NavigationKeys : Boolean read FNavigationKeys write SetNavigationKeys;
  end;

  TBaseKB = class(TPanel)
  private
    KeyState      : TKeyboardState;
    PermanentShift: Boolean;
    Repeating     : Boolean;
    CurrentKey    : TKBKey;
    CurrentKeyColor : TColor;
    KeyIdCount    : Integer;
    EventKeyCode  : Integer;


    procedure SendKey(VCode: Integer;Shift : TShiftState;Mode : TKeyMode);
    function  State(Ctrl : Word) : boolean;
    //procedure SetState(Ctrl : word; stOn : boolean);
    procedure ToggleState(Ctrl : word);
  protected
    FKbMode: TKbMode;// keyboard mode
    FHighlightColor: TColor;
    FShiftState         : Boolean;
    FNumlock            : Boolean;
    FCapsLock           : Boolean;
    FInsert             : Boolean;
    FScrollLock         : Boolean;
    FRepeatDelay        : Integer;
    FTargetMode         : Boolean;
    FFocusbyHandle      : Boolean;
    FFocusHandle        : HWND;
    FSecondFont         : TFont;
    FRepeatDelTimer     : TTimer;
    CheckTimer          : TTimer;
    FExternalKeyMode    : TKeyMode; // for external operation Up,Down,Momentary
    FExternalKeyCode    : Integer;
    FExternalShift      : TShiftState;
    FSelectClick        : TNotifyEvent;
    FControlState       : Boolean;
    FAltState           : Boolean;
    ControlCount        : Integer;
    AltCount            : Integer;



    FOptions: TOptions;
    FOnKey: TKeyEvent;
    FOnSpecialKey: TSpecialKeyEvent;
    FOnShiftStateChange: TShiftStateChange;
    FOnCapsStateChange: TCapsStateChange;

    Keys: TKeys;// hold pointers to buttons representing normal keys

    // pointers for special key buttons
    KeyTab: TKBKey;
    KeyCaps: TKBKey;
    KeyLeftShift: TKBKey;
    KeyEnter: TKBKey;
    KeyBackspace: TKBKey;
    KeySpace: TKBKey;
    KeyPlus: TKBKey;
    KeyESC : TKBKey;
    KeyCtrl : TKBKey;
    KeyAlt  : TKBKey;

    // property methods
    procedure SetKbMode(KbModeToSet: TKbMode);
    procedure SetOptions(OptionsToSet: TOptions);
    procedure SetRepeatDelay(Value : Integer);
    procedure SetSecondFont(Value : TFont);
    procedure SetTargetMode(Value : Boolean);
    procedure SetSelectClick(Value : TNotifyEvent);
    procedure RepTimerOut(Sender: TObject);
    procedure CheckTimerOut(Sender: TObject);
    procedure SetFocusbyHandle(Value : Boolean);
    procedure SetFocusHandle(Value : HWND);


    // Key creation
    procedure CreateKeys;
    procedure CreateAlphaNumericKeys;

    // resizing
    procedure Resize; override;
    procedure ResizeAlphaNumericKeys;

    // aspect changes
    procedure SetKeyCaptions;
    procedure RefreshKeysAspect;

    // event handlers
    procedure KeyMouseDown(Sender: TObject; Button: TMouseButton;
              Shift: TShiftState; X, Y: Integer);
    procedure KeyMouseUp(Sender: TObject; Button: TMouseButton;
              Shift: TShiftState; X, Y: Integer);
    procedure DoKeyPressed ; dynamic;
  public
    FocusControl :TWinControl;
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure UpdateKBState;
    Procedure ExternalKeyPressed; // required to remove stickshift

  published
    property KeyboardMode: TKbMode read FKbMode write SetKbMode;
    property HighlightColor: TColor read FHighlightColor write FHighlightColor;
    property Options: TOptions read FOptions write SetOptions;
    property SecondaryFont: TFont read FSecondFont write SetSecondFont;
    property CapsLock : Boolean  read FCapsLock;
    property NumLock : Boolean  read FNumLock;
    property InsertMode : Boolean  read FInsert;
    property ScrollLock : Boolean  read FScrollLock;
    property SingleShift: Boolean read FShiftState;
    property CtrlState  : Boolean read FControlState;
    property AltState  : Boolean read FAltState;
    property RepeatDelay : Integer read FRepeatDelay write SetRepeatDelay;
    property ExternalMode : Boolean read FTargetMode write SetTargetMode;
    property FocusByHandle : Boolean read FFocusbyHandle write SetFocusbyHandle;
    property FocusHandle : HWND read FFocusHandle write SetFocusHandle;

    property ExternalShift : TShiftState read FExternalShift;
    property ExternalKeyMode : TKeyMode read FExternalKeyMode;
    property ExternalKeyCode : Integer read FExternalKeyCode;

    // event properties
    property OnKey: TKeyEvent read FOnKey write FOnKey;
    property OnSpecialKey: TSpecialKeyEvent read FOnSpecialKey write FOnSpecialKey;
    property OnShiftStateChange: TShiftStateChange read FOnShiftStateChange write FOnShiftStateChange;
    property OnCapsStateChange: TCapsStateChange read FOnCapsStateChange write FOnCapsStateChange;
    property SelectClick : TNotifyEvent read FSelectClick write SetSelectClick;
  end;

implementation

function ChangeBrightness(Color:TColor;Percentage:longint):TColor;
var RGBColor       : longint;
    Red,Green,Blue : byte;
    NewR,NewG,NewB : longint;
    Overflow       : longint;
begin
  RGBColor:=ColorToRGB(Color);
  //Overflow:=0;
  {RED}
  Red:=GetRValue(RGBColor);
  NewR:=Red+(Percentage*Red div 100);
  if NewR>255 then begin
    Overflow:=NewR-255;
    NewG:=Overflow;
    NewB:=Overflow;
  end
  else begin
    NewG:=0;
    NewB:=0;
  end;
  {Green}
  Green:=GetGValue(RGBColor);
  NewG:=NewG+Green+(Percentage*Green div 100);
  if NewG>255 then begin
    Overflow:=NewG-255;
    NewR:=NewR+Overflow;
    NewB:=Overflow;
  end;
  {Blue}
  Blue:=GetBValue(RGBColor);
  NewB:=NewB+Blue+(Percentage*Blue div 100);
  if NewB>255 then begin
    Overflow:=NewB-255;
    if NewG<=255 then
      NewR:=NewR+Overflow;
  end;
  if NewR>255 then
    NewR:=255;
  if NewG>255 then
    NewG:=255;
  if NewB>255 then
    NewB:=255;
  if NewR<0 then
    NewR:=0;
  if NewG<0 then
    NewG:=0;
  if NewB<0 then
    NewB:=0;
  Result:=NewR+(NewG shl 8)+(NewB shl 16);
end;


const
// special key captions

// keyboard layout constants
ANLRow1: array[1..13] of string = ('`', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '-', '=');
CodeRow1: array[1..13] of Integer = (223,49,50,51,52,53,54,55,56,57,48,189,187);
ANLSRow1:       array[1..13] of string = ('', '!', '"', '', '$', '%', '^', '&&', '*', '(', ')', '_', '+');
ANLRow2: array[1..13] of string= ('q', 'w', 'e', 'r', 't', 'y', 'u', 'i', 'o', 'p', '[', ']', '\');
ANLSRow2:       array[1..13] of string= ('Q', 'W', 'E', 'R', 'T', 'Y', 'U', 'I', 'O', 'P', '{', '}', '|');
CodeRow2:       array[1..13] of Integer= (81,87,69,82,84,89,85,73,79,80,219,221,220);
ANLRow3: array[1..11] of string = ('a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', ';', '''');
ANLSRow3:       array[1..11] of string = ('A', 'S', 'D', 'F', 'G', 'H', 'J', 'K', 'L', ':', '@');
CodeRow3:       array[1..11] of Integer = (65,83,68,70,71,72,74,75,76,186,192);
ANLRow4: array[1..11] of string = ('z', 'x', 'c', 'v', 'b', 'n', 'm', ',', '.', '/','#');
ANLSRow4:       array[1..11] of string = ('Z', 'X', 'C', 'V', 'B', 'N', 'M', '<', '>', '?','~');
CodeRow4:       array[1..11] of Integer = (90,88,67,86,66,78,77,188,190,191,222);
ANLFRow: array[1..13] of string = ('Esc', 'F1', 'F2', 'F3', 'F4', 'F5', 'F6', 'F7', 'F8', 'F9', 'F10', 'F11', 'F12');
CodeFRow:       array[1..13] of Integer = (VK_ESCAPE,VK_F1,VK_F2,VK_F3,VK_F4,VK_F5,VK_F6,VK_F7,VK_F8,VK_F9,VK_F10,VK_F11,VK_F12);

NumberKeys: array[1..16] of string = ('7', '8', '9', '-', '4', '5', '6', '+', '1', '2', '3', '/', '*','0','.','ENT');
NumberKeyCodes:array[1..16] of Integer = (55,56,57,189,52,53,54,107,49,50,51,191,106,48,190,13);



//---------------------------------------------------------------------------------------------
// Create
//---------------------------------------------------------------------------------------------
constructor TBaseKB.Create(AOwner: TComponent);
var
i, j: Integer;
 //       LogoMap : TBitmap;
begin
inherited;
  FTargetMode := False;


  CheckTimer := TTimer.Create(Self);
  FSecondFont := TFont.Create;
  FSecondFont.Assign(Font);
  // set default value for BevelOuter to bvNone which is probably the
  // preferred setting for TBaseKB
  BevelOuter := bvNone;
  FControlState := False;
  FAltState := False;
  ControlCount :=0;

  // initialize button pointers to nil
  // special keys
  KeyTab := nil;
  KeyCaps := nil;
  KeyLeftShift := nil;
  KeyEnter := nil;
  KeyBackspace := nil;
  KeySpace := nil;
  KeyPlus := nil;
//  KeyHelp :=nil;
  KeyESC := nil;
  KeyAlt := nil;
  KeyCtrl := nil;

  // normal keys
  for j := 1 to MAX_ROWS do
  for i := 1 to MAX_KEYS do
  Keys[j, i] := nil;
          // create keys buttons
  CreateKeys;

  FShiftState := False;
  PermanentShift := False;
  UpdateKBState;
  with CheckTimer do
       begin
       Interval := 1000;
       OnTimer := CheckTimerOut;
       Enabled := True;
       end;

SetRepeatDelay(500);
end;


destructor  TBaseKB.Destroy;
begin
  if PermanentShift or FShiftState then
     begin
     keybd_event(VK_SHIFT,0,KEYEVENTF_KEYUP,0);
     end;
  if assigned(FSecondFont) then FSecondFont.Free;
  if assigned(CheckTimer) then CheckTimer.Free;
  if assigned(FRepeatDelTimer) then  FRepeatDelTimer.Free;
  inherited Destroy;
end;


procedure TBaseKB.SetKbMode(KbModeToSet: TKbMode);
begin
FKbMode := KbModeToSet;

// recreate keys when KbMode changes
CreateKeys;
end;

procedure TBaseKB.CheckTimerOut(Sender: TObject);
begin
if KeyIdCount > 0 then
   begin
   dec(KeyIdCount);
   if KeyIdCount = 0 then
      begin
      if assigned(CurrentKey) then CurrentKey.Color := CurrentKeyColor;
      end
   end;
UpdateKBState;
end;

procedure TBaseKB.RepTimerOut(Sender: TObject);
begin
if not Repeating then
   begin
   Repeating := True;
   with FRepeatDelTimer do
        begin
        Interval := 50;
        end;
   end
else
    begin
    if assigned(CurrentKey) then
    KeyMouseDown(CurrentKey,mbLeft,[],0,0);
    end;


end;
procedure TBaseKB.SetRepeatDelay(Value : Integer);
begin
FrepeatDelay := Value;
if FrepeatDelay > 0 then
   begin
   if not assigned(FRepeatDelTimer) then
      begin
      FRepeatDelTimer := TTimer.Create(Self);
      end;
   with FRepeatDelTimer do
        begin
        Enabled := False;
        Interval := FRepeatDelay;
        OnTimer := RepTimerOut;
        end;

   end
else
   begin
   if assigned(FRepeatDelTimer) then
      begin
      FRepeatDelTimer.Free;
      end;
   end;
end;


procedure TBaseKB.SetTargetMode(Value : Boolean);
begin
FTargetMode :=Value;
if Value then FFocusbyHandle := False;
end;
procedure TBaseKB.SetFocusHandle(Value : HWND);
begin
FFocusHandle := Value;
end;

procedure TBaseKB.SetFocusbyHandle(Value : Boolean);
begin
FFocusbyHandle := Value;
if Value then FTargetMode :=False;
end;

procedure TBaseKB.SetSelectClick(Value : TNotifyEvent);
begin
FSelectClick := Value;
end;

procedure TBaseKB.SetSecondFont(Value : TFont);
var
i : Integer;
begin
if assigned(FSecondFont) then
   begin
   if value = FSecondFont then exit;
   FSecondFont.Assign(Value);
   for i := Low(ANLFRow) to High(ANLFRow) do
       begin
       Keys[1, i].ShiftFont.Assign(FSecondFont);
       end;

   for i := Low(ANLRow1) to High(ANLRow1) do
       begin
       Keys[2, i].ShiftFont.Assign(FSecondFont);
       end;

   end;

end;

procedure TBaseKB.SetOptions(OptionsToSet: TOptions);
begin
FOptions := OptionsToSet;
RefreshKeysAspect;
end;

//---------------------------------------------------------------------------------------------
// CreateKeys
//
// COMMENT:
//   Destroys all existing keys and recreates keys depending on keyboard mode.
//---------------------------------------------------------------------------------------------
procedure TBaseKB.CreateKeys;
var
i, j: Integer;
begin
// destroy all existing keys
// special keys
KeyTab.Free;
KeyCaps.Free;
KeyLeftShift.Free;
KeyEnter.Free;
KeyBackspace.Free;
KeySpace.Free;
KeyPlus.Free;
//        KeyHelp.Free;
        KeyESC.Free;
        KeyAlt.Free;
        KeyCtrl.Free;

KeyTab := nil;
KeyCaps := nil;
KeyLeftShift := nil;
KeyEnter := nil;
KeyBackspace := nil;
KeySpace := nil;
KeyPlus := nil;
//        KeyHelp := nil;
        KeyESC := nil;
        KeyAlt:= nil;
        KeyCtrl:= nil;

// normal keys
for j := 1 to MAX_ROWS do
for i := 1 to MAX_KEYS do
if assigned(Keys[j, i]) then
begin
Keys[j, i].Free;
Keys[j, i] := nil;
end;


// create Keys depending on keyboard mode
        CreateAlphaNumericKeys;
RefreshKeysAspect;
end;

//---------------------------------------------------------------------------------------------
// CreateAlphaNumericKeys
// COMMENT:
//   Create keys for AlphaNumeric and AlphaNumericFunction keyboard mode
//---------------------------------------------------------------------------------------------
procedure TBaseKB.CreateAlphaNumericKeys;
var
i         : Integer;
Picture   : TBitmap;
begin
// create special keys
KeyCtrl := TKBKey.Create(Self);
        with KeyCtrl do
             begin
             Parent := Self;
     MainCaption := CtrlkeyCaption;
             Color := clsilver;
             StateOn := False;
             ShowLED := True;
             ColorLED := lcYellow;
             end;
        KeyCtrl.Font := Font;

KeyAlt := TKBKey.Create(Self);
        with KeyAlt do
             begin
             Parent := Self;
     MainCaption := AltkeyCaption;
             Color := clsilver;
             StateOn := False;
             ShowLED := True;
             ColorLED := lcYellow;
             end;
        KeyAlt.Font := Font;

KeyTab := TKBKey.Create(Self);
        with KeyTab do
             begin
             Parent := Self;
     MainCaption := TabkeyCaption;
             Color := clsilver;
             StateOn := False;
             ShowLED := False;
             end;
        KeyTab.Font := Font;

KeyCaps := TKBKey.Create(Self);
        with KeyCaps do
             begin
     Parent := Self;
             MainCaption := CapsKeyCaption;
             Color := clsilver;
             StateOn := False;
             ColorLED := lcYellow;
             end;
        KeyCaps.Font := Font;
KeyLeftShift := TKBKey.Create(Self);
        with KeyLeftShift do
             begin
             Parent := Self;
     MainCaption := ShiftKeyCaption;
             Color := clsilver;
             StateOn := False;
             ColorLED := lcYellow;
             end;
        KeyLeftShift.Font := Font;

KeyEnter := TKBKey.Create(Self);
with KeyEnter do
             begin
             Parent := Self;
     MainCaption := EnterKeyCaption;
             Color := clsilver;
             StateOn := False;
             ShowLed := False;
             end;
        KeyEnter.Font := Font;

KeyBackspace := TKBKey.Create(Self);
with KeyBackspace do
             begin
             Parent := Self;
     MainCaption := '';//BACKSPACE_CAPTION;
             Picture := TBitmap.create;
             try
             Picture.LoadFromResourceName(HInstance,'BACKSPACEARROW');
             Glyph := Picture;
             finally
             Picture.free;
             end;
             Color := clsilver;
             StateOn := False;
             ShowLed := False;
             CanRepeat := True;
             end;
        KeyBackspace.Font := Font;

KeySpace := TKBKey.Create(Self);
with KeySpace do
             begin
             Parent := Self;
     MainCaption := SpaceKeyCaption;
             Color := clsilver;
             StateOn := False;
             ShowLed      := False;
             end;
        KeySpace.Font := Font;

KeyESC := TKBKey.Create(Self);
with KeyESC do
             begin
             Parent := Self;
     MainCaption := EscapeKeyCaption;
             Color := clsilver;
             StateOn := False;
             ShowLed      := False;
             end;
        KeyESC.Font := Font;
// set event handler for shifts
KeyLeftShift.OnMouseDown := KeyMouseDown;

// set event handler for caps
KeyCaps.OnMouseDown := KeyMouseDown;


// set event handler for the rest of the special keys
KeyEnter.OnMouseDown := KeyMouseDown;
KeyEnter.OnMouseUp := KeyMouseUp;
KeyBackspace.OnMouseDown := KeyMouseDown;
KeyBackspace.OnMouseUp := KeyMouseUp;
KeyTab.OnMouseDown := KeyMouseDown;
KeyTab.OnMouseUp := KeyMouseUp;
KeySpace.OnMouseDown := KeyMouseDown;
KeySpace.OnMouseUp := KeyMouseUp;
//KeyHelp.OnMouseDown := KeyMouseDown;
//KeyHelp.OnMouseUp := KeyMouseUp;
        KeyAlt.OnMouseDown  := KeyMouseDown;
KeyAlt.OnMouseUp := KeyMouseUp;
        KeyCtrl.OnMouseDown  := KeyMouseDown;
KeyCtrl.OnMouseUp := KeyMouseUp;

// create function key buttons when KbMode is kbmAlphaNumericFunction
        if FKbMode = kbmAlphaNumericFunction then
for i := Low(ANLFRow) to High(ANLFRow) do
begin
Keys[1, i] := TKBKey.Create(Self);
Keys[1, i].Parent := Self;
Keys[1, i].OnMouseDown := KeyMouseDown;
Keys[1, i].OnMouseUp := KeyMouseUp;
                        Keys[1, i].Switching := False;
                        Keys[1, i].ShowLED   := False;
                        Keys[1, i].Font := Font;

end;

// create first row of buttons
for i := Low(ANLRow1) to High(ANLRow1) do
begin
Keys[2, i] := TKBKey.Create(Self);
Keys[2, i].Parent := Self;
Keys[2, i].OnMouseDown := KeyMouseDown;
Keys[2, i].OnMouseUp := KeyMouseUp;
                Keys[2, i].Switching := False;
                Keys[2, i].ShowLED   := False;
                Keys[2, i].Font := Font;

end;

// create second row of buttons
for i := Low(ANLRow2) to High(ANLRow2) do
begin
Keys[3, i] := TKBKey.Create(Self);
Keys[3, i].Parent := Self;
Keys[3, i].OnMouseDown := KeyMouseDown;
Keys[3, i].OnMouseUp := KeyMouseUp;
                Keys[3, i].Switching := False;
                Keys[3, i].ShowLED   := False;
                Keys[3, i].Font := Font;
end;

// create third row of buttons
for i := Low(ANLRow3) to High(ANLRow3) do
begin
Keys[4, i] := TKBKey.Create(Self);
Keys[4, i].Parent := Self;
Keys[4, i].OnMouseDown := KeyMouseDown;
Keys[4, i].OnMouseUp := KeyMouseUp;
                Keys[4, i].Switching := False;
                Keys[4, i].ShowLED   := False;
                Keys[4, i].Font := Font;

end;

// create fourth row of buttons
for i := Low(ANLRow4) to High(ANLRow4) do
begin
Keys[5, i] := TKBKey.Create(Self);
Keys[5, i].Parent := Self;
Keys[5, i].OnMouseDown := KeyMouseDown;
                Keys[5, i].Switching := False;
Keys[5, i].OnMouseUp := KeyMouseUp;
                Keys[5, i].ShowLED   := False;
                Keys[5, i].Font := Font;
end;

Resize;
SetKeyCaptions;
end;



//---------------------------------------------------------------------------------------------
// SetKeyCaptions
// COMMENT:
//   Sets the captions of the keys in AlphaNumeric and AlphaNumericFunction keyboard mode.
//   SetKeyCaptions is called also to change the key captions when putting keyboard into shift
//   or caps mode
//---------------------------------------------------------------------------------------------
procedure TBaseKB.SetKeyCaptions;
var
i: Integer;
begin
// function key buttons when KbMode is kbmAlphaNumericFunction
if FKbMode = kbmAlphaNumericFunction then
for i := Low(ANLFRow) to High(ANLFRow) do
                        begin
Keys[1, i].MainCaption := ANLFRow[i];
Keys[1, i].Tag :=  CodeFRow[i];
                        end;
// first row
for i := Low(ANLRow1) to High(ANLRow1) do
                begin
                Keys[2, i].Tag :=  CodeRow1[i];

if not (FShiftState {xor FCapsLock}) then
                        begin
Keys[2, i].MainCaption := ANLRow1[i];
Keys[2, i].ShiftCaption := ANLSRow1[i];
Keys[2, i].ShowShiftedCharacter := True;
                        end
else
                        begin
Keys[2, i].MainCaption := ANLSRow1[i];
Keys[2, i].ShowShiftedCharacter := False;
                        end;
                end;

// second row
for i := Low(ANLRow2) to 10 do
                begin
                Keys[3, i].Tag :=  CodeRow2[i];

if not (FShiftState xor FCapsLock) then
Keys[3, i].MainCaption := ANLRow2[i]
else
Keys[3, i].MainCaption := ANLSRow2[i];
                end;

for i := 11 to High(ANLRow2) do
                begin
                Keys[3, i].Tag :=  CodeRow2[i];
if not (FShiftState) then
                        begin
Keys[3, i].MainCaption := ANLRow2[i];
Keys[3, i].ShiftCaption := ANLSRow2[i];
Keys[3, i].ShowShiftedCharacter := True;
                        end
else
                        begin
Keys[3, i].MainCaption := ANLSRow2[i];
Keys[3, i].ShowShiftedCharacter := False;
                        end;
                end;

// third row
for i := Low(ANLRow3) to 9 do
                begin
                Keys[4, i].Tag :=  CodeRow3[i];
if not (FShiftState xor FCapsLock) then
Keys[4, i].MainCaption := ANLRow3[i]
else
Keys[4, i].MainCaption := ANLSRow3[i];
                end;

for i := 10 to High(ANLRow3) do
                begin
                Keys[4, i].Tag :=  CodeRow3[i];
if not (FShiftState) then
                        begin
Keys[4, i].MainCaption := ANLRow3[i];
Keys[4, i].ShiftCaption := ANLSRow3[i];
Keys[4, i].ShowShiftedCharacter := True;
                        end
else
                        begin
Keys[4, i].MainCaption := ANLSRow3[i];
Keys[4, i].ShowShiftedCharacter := False;
                        end;
                end;
// fourth row
for i := Low(ANLRow4) to 7 do
                begin
                Keys[5, i].Tag :=  CodeRow4[i];
if not (FShiftState xor FCapsLock) then
Keys[5, i].MainCaption := ANLRow4[i]
else
Keys[5, i].MainCaption := ANLSRow4[i];
                end;

for i := 8 to High(ANLRow4) do
                begin
                Keys[5, i].Tag :=  CodeRow4[i];
if not (FShiftState) then
                        begin
Keys[5, i].MainCaption := ANLRow4[i];
Keys[5, i].ShiftCaption := ANLSRow4[i];
Keys[5, i].ShowShiftedCharacter := True;
                        end
else
                        begin
Keys[5, i].MainCaption := ANLSRow4[i];
Keys[5, i].ShowShiftedCharacter := False;
                        end;
                end;
end;

//---------------------------------------------------------------------------------------------
// Resize
// COMMENT:
//   Position keys within the client area depending on keyboard mode. Called when
//   creating the component, on keyboard mode switch and every time the component
//   is resized.
//---------------------------------------------------------------------------------------------
procedure TBaseKB.Resize;
begin
inherited;
// resize key buttons and reposition them
ResizeAlphaNumericKeys;
end;

//---------------------------------------------------------------------------------------------
// ResizeAlphaNumericKeys
// COMMENT:
//   Position keys within client area for AlphaNumeric and AlphaNumericFunction
//   keyboard layout
//---------------------------------------------------------------------------------------------
procedure TBaseKB.ResizeAlphaNumericKeys;
var
i: Integer;
nKWidth, nKHeight: Integer;
nFKeyOffset: Integer;
nLeftBase, nTopBase: Integer;
nBevelWidth: Integer;
begin
// calculate width of all Bevels on one side
nBevelWidth := 0;
if BevelInner <> bvNone then
nBevelWidth := nBevelWidth + BevelWidth;
if BevelOuter <> bvNone then
nBevelWidth := nBevelWidth + BevelWidth;

// if we have function keys we shift all the other keys down
if FKbMode = kbmAlphaNumericFunction then
begin
nKHeight := Trunc((Height - 2 * nBevelWidth - 2 * BorderWidth) / 6.2);
nFKeyOffset := Trunc(1.2 * nKHeight);
end
else
begin
nKHeight := Trunc((Height - 2 * nBevelWidth - 2 * BorderWidth) / 5);
nFKeyOffset := 0;
end;

nKWidth := Trunc((Width - 2 * nBevelWidth - 2 * BorderWidth) / 14.5);

nLeftBase := nBevelWidth + BorderWidth;
nTopBase := nBevelWidth + nFKeyOffset + BorderWidth;

// resize special keys
KeyTab.Height := nKHeight;
KeyTab.Width := Round(1.5 * nKWidth);
KeyTab.Left := nLeftBase;
KeyTab.Top := nTopBase + nKHeight;
KeyCaps.Height := nKHeight;
KeyCaps.Width := Round(2 * nKWidth);
KeyCaps.Left := nLeftBase;
KeyCaps.Top := nTopBase + 2 * nKHeight;
KeyLeftShift.Height := nKHeight;
KeyLeftShift.Width := Round(2.5 * nKWidth);
KeyLeftShift.Left := nLeftBase;
KeyLeftShift.Top := nTopBase + 3 * nKHeight;
KeyEnter.Height := nKHeight;
KeyEnter.Width := Round(1.5 * nKWidth);
KeyEnter.Left := nLeftBase + 13 * nKWidth;
KeyEnter.Top := nTopBase + 2 * nKHeight;
KeyBackspace.Height := nKHeight;
KeyBackspace.Width := Round(1.5 * nKWidth);
KeyBackspace.Left := nLeftBase + 13 * nKWidth;
KeyBackspace.Top := nTopBase;
KeySpace.Height := nKHeight;
KeySpace.Width := Round(8.5 * nKWidth);
KeySpace.Left := nLeftBase + Trunc(3 * nKWidth);
KeySpace.Top := nTopBase + 4 * nKHeight;
//KeyHelp.Height := nKHeight;
//KeyHelp.Width := KeyTab.Width;
//KeyHelp.Left := KeyTab.Left;
//KeyHelp.Top := KeySpace.Top;
KeyESC.Height := nKHeight;
KeyESC.Width := KeyTab.Width;
KeyESC.Left := KeyEnter.Left;
KeyESC.Top := KeySpace.Top;
        KeyCtrl.Top := KeyESC.Top;
        KeyCtrl.Height := nKHeight;
KeyCtrl.Width := KeyTab.Width;
        KeyCtrl.Left := KeySpace.Left+KeySpace.Width+1;
        KeyAlt.Top := KeyESC.Top;
        KeyAlt.Height := nKHeight;
KeyAlt.Width := KeyTab.Width;
        KeyAlt.Left := KeyTab.Left+{KeyHelp.Width+}1;

// resize function keys
for i := Low(ANLFRow) to High(ANLFRow) do
if assigned(Keys[1, i]) then
begin
Keys[1, i].Width := nKWidth;
Keys[1, i].Height := nKHeight;

// leave a half key space between Esc and the function keys
if i > 1 then
Keys[1, i].Left := nLeftBase + (i - 1) * nKWidth + Trunc(nKWidth / 2)
else
Keys[1, i].Left := nLeftBase;

Keys[1, i].Top := nTopBase - nFKeyOffset;
end;

// resize first row
for i := Low(ANLRow1) to High(ANLRow1) do
if assigned(Keys[2, i]) then
begin
Keys[2, i].Width := nKWidth;
Keys[2, i].Height := nKHeight;
Keys[2, i].Left := nLeftBase + (i - 1) * nKWidth;
Keys[2, i].Top := nTopBase;
end;

// resize second row of buttons
for i := Low(ANLRow2) to High(ANLRow2) do
if assigned(Keys[3, i]) then
begin
Keys[3, i].Width := nKWidth;
Keys[3, i].Height := nKHeight;
Keys[3, i].Left := nLeftBase + (i - 1) * nKWidth + Round(1.5 * nKWidth);
Keys[3, i].Top := nTopBase + nKHeight;
end;

// resize third row of buttons
for i := Low(ANLRow3) to High(ANLRow3) do
if assigned(Keys[4, i]) then
begin
Keys[4, i].Width := nKWidth;
Keys[4, i].Height := nKHeight;
Keys[4, i].Left := nLeftBase + (i - 1) * nKWidth + 2 * nKWidth;
Keys[4, i].Top := nTopBase + 2 * nKHeight;
end;

// resize fourth row of buttons
for i := Low(ANLRow4) to High(ANLRow4) do
if assigned(Keys[5, i]) then
begin
Keys[5, i].Width := nKWidth;
Keys[5, i].Height := nKHeight;
Keys[5, i].Left := nLeftBase + (i - 1) * nKWidth + Round(2.5 * nKWidth);
Keys[5, i].Top := nTopBase + 3 * nKHeight;
end;
end;

procedure TBaseKB.KeyMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
if assigned(FRepeatDelTimer) then
   begin
   FRepeatDelTimer.Enabled := False;
   FRepeatDelTimer.Interval :=FRepeatDelay;
   end;
Repeating := False;
end;


procedure TBaseKB.KeyMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var
        KeyC     : Integer;
begin
if assigned(FocusControl) and (FTargetMode = False) then
   begin
   FocusControl.SetFocus;
   end;

if FFocusbyHandle and (FFocusHandle <> 0) then
   begin
   if FFocusHandle <> 0 then Windows.SetFocus(FFocusHandle);
   end;
try
if Sender = Self then
   begin
   if not PermanentShift then
      begin
      FShiftState := False;
      UpdateKBState;
      end;
   exit;
   end;
if Sender = KeyCtrl then
   begin
   with KeyCtrl do
       begin
       StateOn := True;
       ControlCount := 2;
       exit;
       end;
   end;

if Sender = KeyAlt then
   begin
   with KeyAlt do
       begin
       StateOn := True;
       AltCount := 2;
       exit;
       end;
   end;

if Sender = KeyLeftShift then
   begin
   if PermanentShift then
      with KeyLeftShift do
           begin
           PermanentShift := False;
           ColorLED := lcYellow;
           FShiftState := False;
           UpdateKBState;
           exit;
           end;

   if (FShiftState) then
      begin
      PermanentShift := True;
      with KeyLeftShift do
           begin
           ColorLED := lcLime;
           invalidate;// just the shift key
           exit;
           end;
      end;
   PermanentShift := False;
   FShiftState := True;
   UpdateKBState;
   exit;
   end;

if Sender = KeyCaps then
   begin
   GetKeyBoardState(KeyState);
   ToggleState(VK_CAPITAL);
   UpdateKBState;
   exit;
   end;

if Sender is TKBKey then
   begin
   KeyIdCount := 2;
   If not Repeating then
      begin
      if assigned(CurrentKey) then
         begin
         CurrentKey.Color := CurrentKeyColor;
         end;
         CurrentKey := TKBKey(Sender);
         CurrentKeyColor := CurrentKey.Color;
         TKBKey(Sender).Color := clLime;
         end;
      end;
if FRepeatDelay>0 then
   begin
   if assigned(FRepeatDelTimer) then FRepeatDelTimer.Enabled := True;
   end;

if Sender = KeyEnter then
   begin
   Sendkey(13,shift,kmMomentary);
   exit;
   end;

if Sender = KeyBackspace then
   begin
   Sendkey(8,shift,kmMomentary);
   exit;
   end;
if Sender = KeyTab then
   begin
   Sendkey(VK_TAB,shift,kmMomentary);
   exit;
   end;
if Sender = KeySpace then
   begin
   Sendkey(32,shift,kmMomentary);
   exit;
   end;

if Sender = KeyEsc then
   begin
   Sendkey(VK_ESCAPE,shift,kmMomentary);
   exit;
   end;

Keyc := (Sender as TKBKey).Tag;

if FShiftState then
   begin
   shift := [ssShift];
   Sendkey(Keyc,shift,kmMomentary);
   if not PermanentShift then
      begin
      FShiftState := False;
      UpdateKBState;
      end;
   end
else
    begin
    Sendkey(Keyc,shift,kmMomentary);
    end;
finally
Case ControlCount of
     2:
     begin
     ControlCount := 1;
     FControlState := True;
     end;
     1:
     begin
     ControlCount := 0;
     FControlState := False;
     KeyCtrl.StateOn := False;
     end;
     end; //case;

Case AltCount of
     2:
     begin
     AltCount := 1;
     FAltState := True;
     end;
     1:
     begin
     AltCount := 0;
     FAltState := False;
     KeyAlt.StateOn := False;
     end;
     end; //case;

end;

end;
//---------------------------------------------------------------------------------------------
// RefreshKeysAspect
// COMMENT:
//   Sets flat style of keys. Enable/disable, show/hide special keys.
//---------------------------------------------------------------------------------------------
procedure TBaseKB.RefreshKeysAspect;
var
j: Integer;
begin
// normal keys
for j := 1 to MAX_ROWS do

// special keys
if assigned(KeyTab) then
begin
        KeyTab.Enabled := not (DisableTab in FOptions);
KeyTab.Visible := not (HideTab in FOptions);
end;

if assigned(KeyCtrl) then
begin
        with KeyCtrl do
                     begin
                     Enabled := not (DisableCtrl in FOptions);
     Visible := not (HideCtrl in FOptions);
                     Switching := False;
                     ShowLED   := True;
                     end;
        end;

if assigned(KeyAlt) then
begin
        with KeyAlt do
                     begin
                     Enabled := not (DisableAlt in FOptions);
     Visible := not (HideAlt in FOptions);
                     Switching := False;
                     ShowLED   := True;
                     end;
        end;
if assigned(KeyCaps) then
begin
        with KeyCaps do
                     begin
                     Enabled := not (DisableCaps in FOptions);
     Visible := not (HideCaps in FOptions);
                     Switching := False;
                     ShowLED   := True;
//                     Color := FMainKeyColour;
                     end;
end;

if assigned(KeyLeftShift) then
begin
with KeyLeftShift do
                     begin
                     Enabled := not (DisableCaps in FOptions);
     Visible := not (HideCaps in FOptions);
                     Switching := False;
                     ShowLED   := True;
                     end;
end;


if assigned(KeyEnter) then
begin
KeyEnter.Enabled := not (DisableEnter in FOptions);
KeyEnter.Visible := not (HideEnter in FOptions);
end;

if assigned(KeyBackspace) then
begin
KeyBackspace.Enabled := not (DisableBackspace in FOptions);
KeyBackspace.Visible := not (HideBackspace in FOptions);
end;

if assigned(KeySpace) then
begin
KeySpace.Enabled := not (DisableSpace in FOptions);
KeySpace.Visible := not (HideSpace in FOptions);
end;


// show/hide or enable/disable Escape key
if FkbMode = kbmAlphaNumericFunction then
begin
Keys[1, 1].Enabled := not (DisableEscape in FOptions);
Keys[1, 1].Visible := not (HideEscape in FOptions);
end;
end;

procedure TBaseKB.DoKeyPressed;
begin
if assigned(FOnKey) then FOnkey(Self,Char(EventKeyCode));
end;

procedure TBaseKB.SendKey(VCode: Integer;Shift : TShiftState;Mode : TKeyMode);
var
Code : Byte;
begin
if FTargetMode then
   begin
   FExternalKeyMode := Mode;
   FExternalKeyCode := VCode;
   FExternalShift   := Shift;
   EventKeyCode := Byte(Vcode);
   DoKeyPressed;
  end
else
    Begin
    if ssShift in shift then
       begin
       keybd_event(VK_SHIFT,0,0,0);
       end;
     Code := Byte(Vcode);
     case Mode of
          kmDown:
          begin
          keybd_event(Code,0,0,0);
          end;

          kmUp:
          begin
          keybd_event(Code,0,KEYEVENTF_KEYUP,0);
          end;

          kmMomentary:
          begin
          keybd_event(Code,0,0,0);
          keybd_event(Code,0,KEYEVENTF_KEYUP,0);
          end;
     end;// case

     if ssShift in shift then
        begin
        keybd_event(VK_SHIFT,0,KEYEVENTF_KEYUP,0);
        end;

     end;

end;

Procedure TBaseKB.ExternalKeyPressed; // required to remove stickshift
begin
KeyMouseDown(Self,mbRight,[],0,0);
end;

procedure TBaseKB.UpdateKBState;
begin
GetKeyBoardState(KeyState);
FNumlock :=State(vk_NumLock);
FScrollLock := State(vk_Scroll);
FInsert := State(vk_Insert);
FCapsLock := State(VK_CAPITAL);
if assigned(KeyCaps) then
   begin
   KeyCaps.StateOn := FCapsLock;
   end;
if assigned(KeyLeftShift) then
   begin
   KeyLeftShift.StateOn := FShiftState;
   end;

SetKeyCaptions;
end;

function TBaseKB.State(Ctrl : Word) : boolean;
begin
  Result:=((KeyState[ctrl] and 1)=1);
end;

(*
procedure TBaseKB.SetState(Ctrl : word; stOn : boolean);
function IsNT : boolean;
begin
  Result:=(GetVersion<$80000000);
end;
begin
  GetKeyboardState(KeyState);
  { Toggle KeyState if changed }
  if (State(ctrl) or stOn) then
  begin
    { Toggle KeyState SystemWide }
    keybd_event(Ctrl, 0, 0, 0);
    keybd_event(Ctrl, 0, KEYEVENTF_KEYUP, 0);
  end;
  { if not Windows NT this has to be done. }
  if not IsNT then
  begin
    Application.ProcessMessages; { Has to be here. Otherwise Win95 lose control. }
    { Set KeyState }
    KeyState[Ctrl]:=Byte(stOn);
    SetKeyboardState(KeyState);
  end;
end;
  *)


procedure TBaseKB.ToggleState(Ctrl : word);
function IsNT : boolean;
begin
  Result:=(GetVersion<$80000000);
end;
var
InitialState : Boolean;

begin
  GetKeyboardState(KeyState);
  InitialState := State(Ctrl);
 { Toggle KeyState SystemWide }
  keybd_event(Ctrl, 0, 0, 0);
  keybd_event(Ctrl, 0, KEYEVENTF_KEYUP, 0);
  { if not Windows NT this has to be done. }
  if not IsNT then
  begin
    Application.ProcessMessages; { Has to be here. Otherwise Win95 lose control. }
    { Set KeyState }
    KeyState[Ctrl]:=Byte(not InitialState);
    SetKeyboardState(KeyState);
  end;
end;


constructor TKBKey.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);

  FBeveled:=true;
  FBorderStyle:=bsSingle;
  FButtonDirection:=bdBottomUp;
  FColor:=clGray;
  FColorHighlight:=ChangeBrightness(FColor,50);
  FColorShadow:=ChangeBrightness(FColor,-50);
  FColorLED:=lcBlue;
  FDepth:=DefaultDepth;
  FDown:=false;
  FFont:=TFont.Create;
  FShiftFont:=TFont.Create;
  FGlyph:=TBitmap.Create;
  FNumGlyphs:=1;
  FShowLED:=true;
  FStateOn:=false;
  FSwitching:=true;
  Height:=DefaultHeight;
  Width:=DefaultWidth;

end;

destructor  TKBKey.Destroy;
begin
  FFont.Free;
  FShiftFont.Free;
  FGlyph.Free;
  inherited Destroy;
end;

procedure TKBKey.SetBeveled(NewValue: boolean);
begin
  FBeveled:=NewValue;
  Invalidate;
end;

procedure TKBKey.SetBorderStyle(NewBorderStyle: TBorderStyle);
begin
  FBorderStyle:=NewBorderStyle;
  Invalidate;
end;
procedure TKBKey.SetMainCaption(Value :String);
begin
if Value = FMainCaption then exit;
FMainCaption := Value;
Caption := FMainCaption;
Invalidate;
end;
procedure TKBKey.SetButtonDirection(NewDirection: TButtonDirection);
begin
  FButtonDirection:=NewDirection;
  Invalidate;
end;

procedure TKBKey.SetColor(newColor: TColor);
begin
  FColor:=newColor;
  FColorHighlight:=ChangeBrightness(FColor,50);
  FColorShadow:=ChangeBrightness(FColor,-50);
  Invalidate;
end;

procedure TKBKey.SetColorLED(newColor: TLEDColor);
begin
  FColorLED:=newColor;
  Invalidate;
end;

procedure TKBKey.SetDepth(newValue: integer);
begin
  FDepth:=newValue;
  Invalidate;
end;

procedure TKBKey.SetFont(newFont: TFont);
begin
  FFont.Assign(NewFont);
  Invalidate;
end;
procedure TKBKey.SetShiftFont(newFont: TFont);
begin
  FShiftFont.Assign(NewFont);
  Invalidate;
end;

procedure TKBKey.SetGlyph(newGlyph: TBitmap);
begin
  if(Assigned(FGlyph)) then begin
    FGlyph.Assign(newGlyph);

    if (csDesigning in ComponentState) then begin
      { Glyph 1: Normal, 2: Disabled, 3: Down;
        Mu die Ausmae (Height * NumGlyphs) = Width  haben}
      if (newGlyph.width mod newGlyph.height = 0) then
        FNumGlyphs:= newGlyph.width div newGlyph.height
      else
        FNumGlyphs:= 1;
    end;

    Invalidate;
  end;
end;

procedure TKBKey.SetNumGlyphs(newNumGlyphs: TNumGlyphs);
begin
  FNumGlyphs:= newNumGlyphs;
  Invalidate;
end;

procedure TKBKey.SetShowLED(newValue: boolean);
begin
  FShowLED := newValue;
  Invalidate;
end;

procedure TKBKey.SetStateOn(newValue: boolean);
begin
  if newValue = FStateOn then exit;
  FStateOn := newValue;
  Invalidate;
end;

procedure TKBKey.SetShowShiftChar(Value : Boolean);
begin
  FShowShiftChar := Value;
end;

procedure TKBKey.SetShiftCaption(Value : String);
begin
  If Value = FShiftCaption then exit;
  FShiftCaption := Value;
  InValidate;
end;


procedure TKBKey.DrawBorder(Dest:TRect);
var i : integer;
begin
  Dest:=GetClientRect;
  with Canvas do begin
    if FBorderStyle=bsSingle then begin
      Brush.Color:=clWindowFrame;
      FrameRect(Dest);
      InflateRect(Dest,-1,-1);
    end;
    Pen.Width:=1;
    if FButtonDirection=bdBottomUp then begin
      Pen.Color:=FColorHighlight;
      {oben}
      MoveTo(Dest.Right-1,Dest.Top);
      LineTo(Dest.Left,Dest.Top);
      {links}
      if not FBeveled then begin
        MoveTo(Dest.Left,Dest.Top);
        LineTo(Dest.Left,Dest.Bottom-1);
      end
      else
        for i:=0 to FDepth do begin
          MoveTo(Dest.Left,Dest.Top);
          if FDown then
            LineTo(Dest.Left+(i div 2),Dest.Bottom-1)
          else
            LineTo(Dest.Left+i,Dest.Bottom-i-1);
        end;
      Pen.Color:=FColorShadow;
      {rechts}
      if not FBeveled then begin
        MoveTo(Dest.Right-1,Dest.Top);
        LineTo(Dest.Right-1,Dest.Bottom);
      end
      else
        for i:=0 to FDepth do begin
          MoveTo(Dest.Right-1,Dest.Top);
          if FDown then
            LineTo(Dest.Right-(i div 2)-1,Dest.Bottom-1)
          else
            LineTo(Dest.Right-i-1,Dest.Bottom-i-1);
        end;
      {unten}
      if FDown then begin
        MoveTo(Dest.Left,Dest.Bottom-1);
        LineTo(Dest.Right-1,Dest.Bottom-1);
      end
      else
        for i:=0 to FDepth do begin
          if not FBeveled then begin
            MoveTo(Dest.Left,Dest.Bottom-i-1);
            LineTo(Dest.Right-1,Dest.Bottom-i-1);
          end
          else begin
            MoveTo(Dest.Left+i,Dest.Bottom-i-1);
            LineTo(Dest.Right-i-1,Dest.Bottom-i-1);
          end;
        end;
    end;
    if FButtonDirection=bdTopUp then begin
      Pen.Color:=FColorHighlight;
      {oben}
      if FDown then begin
        MoveTo(Dest.Left,Dest.Top);
        LineTo(Dest.Right-1,Dest.Top);
      end
      else
        for i:=0 to FDepth do begin
          if not FBeveled then begin
            MoveTo(Dest.Left,Dest.Top+i);
            LineTo(Dest.Right-1,Dest.Top+i);
          end
          else begin
            MoveTo(Dest.Left+i,Dest.Top+i);
            LineTo(Dest.Right-i-1,Dest.Top+i);
          end;
        end;
      {links}
      if not FBeveled then begin
        MoveTo(Dest.Left,Dest.Top);
        LineTo(Dest.Left,Dest.Bottom-1);
      end
      else
        for i:=0 to FDepth do begin
          MoveTo(Dest.Left,Dest.Bottom-1);
          if FDown then
            LineTo(Dest.Left+(i div 2),Dest.Top)
          else
            LineTo(Dest.Left+i,Dest.Top+i);
        end;
      Pen.Color:=FColorShadow;
      {rechts}
      if not FBeveled then begin
        MoveTo(Dest.Right-1,Dest.Top);
        LineTo(Dest.Right-1,Dest.Bottom);
      end
      else
        for i:=0 to FDepth do begin
          MoveTo(Dest.Right-1,Dest.Bottom-1);
          if FDown then
            LineTo(Dest.Right-(i div 2)-1,Dest.Top)
          else
            LineTo(Dest.Right-i-1,Dest.Top+i);
        end;
      {unten}
      MoveTo(Dest.Right-1,Dest.Bottom-1);
      LineTo(Dest.Left,Dest.Bottom-1);
    end;
    if FButtonDirection=bdLeftUp then begin
      Pen.Color:=FColorHighlight;
      {oben}
      if not FBeveled then begin
        MoveTo(Dest.Left,Dest.Top);
        LineTo(Dest.Right-1,Dest.Top);
      end
      else
        for i:=0 to FDepth do begin
          MoveTo(Dest.Right-1,Dest.Top);
          if FDown then
            LineTo(Dest.Left,Dest.Top+(i div 2))
          else
            LineTo(Dest.Left+i,Dest.Top+i);
        end;
      {links}
      if FDown then begin
        MoveTo(Dest.Left,Dest.Top);
        LineTo(Dest.Left,Dest.Bottom-1);
      end
      else
        for i:=0 to FDepth do begin
          if not FBeveled then begin
            MoveTo(Dest.Left+i,Dest.Top);
            LineTo(Dest.Left+i,Dest.Bottom-1);
          end
          else begin
            MoveTo(Dest.Left+i,Dest.Top+i);
            LineTo(Dest.Left+i,Dest.Bottom-i-1);
          end;
        end;
      Pen.Color:=FColorShadow;
      {rechts}
      MoveTo(Dest.Right-1,Dest.Top);
      LineTo(Dest.Right-1,Dest.Bottom-1);
      {unten}
      if not FBeveled then begin
        MoveTo(Dest.Right-1,Dest.Bottom-1);
        LineTo(Dest.Left,Dest.Bottom-1);
      end
      else
        for i:=0 to FDepth do begin
          MoveTo(Dest.Right-1,Dest.Bottom-1);
          if FDown then
            LineTo(Dest.Left,Dest.Bottom-(i div 2)-1)
          else
            LineTo(Dest.Left+i,Dest.Bottom-i-1);
        end;
    end;
    if FButtonDirection=bdRightUp then begin
      Pen.Color:=FColorHighlight;
      {oben}
      if not FBeveled then begin
        MoveTo(Dest.Left,Dest.Top);
        LineTo(Dest.Right-1,Dest.Top);
      end
      else
        for i:=0 to FDepth do begin
          MoveTo(Dest.Left,Dest.Top);
          if FDown then
            LineTo(Dest.Right-1,Dest.Top+(i div 2))
          else
            LineTo(Dest.Right-1-i,Dest.Top+i);
        end;
      {links}
      MoveTo(Dest.Left,Dest.Top);
      LineTo(Dest.Left,Dest.Bottom-1);
      Pen.Color:=FColorShadow;
      {rechts}
      if FDown then begin
        MoveTo(Dest.Right-1,Dest.Top);
        LineTo(Dest.Right-1,Dest.Bottom-1);
      end
      else
        for i:=0 to FDepth do begin
          if not FBeveled then begin
            MoveTo(Dest.Right-1-i,Dest.Top);
            LineTo(Dest.Right-1-i,Dest.Bottom-1);
          end
          else begin
            MoveTo(Dest.Right-1-i,Dest.Top+i);
            LineTo(Dest.Right-1-i,Dest.Bottom-i-1);
          end;
        end;
      {unten}
      if not FBeveled then begin
        MoveTo(Dest.Left,Dest.Bottom-1);
        LineTo(Dest.Right-1,Dest.Bottom-1);
      end
      else
        for i:=0 to FDepth do begin
          MoveTo(Dest.Left,Dest.Bottom-1);
          if FDown then
            LineTo(Dest.Right-1,Dest.Bottom-(i div 2)-1)
          else
            LineTo(Dest.Right-1-i,Dest.Bottom-i-1);
        end;
    end;
  end;
end;

procedure TKBKey.DrawCaption(Dest:TRect);
var OutText : array [0..79] of char;
begin
  with Canvas do begin
    Brush.Style:=bsClear;
    StrPCopy(OutText, Caption);
    Font.Assign(FFont);
    if not Enabled then
      Font.Color:=clGrayText;
      DrawText(Handle, OutText, length(Caption), Dest, dt_center or dt_vcenter or dt_singleline);
  end;
end;

procedure TKBKey.DrawShiftCaption(Dest:TRect);
var
OutText : array [0..79] of char;
SmallRect : TRect;

begin
SmallRect := Dest;
SmallRect.Bottom := (SmallRect.Bottom div 2);
SmallRect.Right :=  (SmallRect.Bottom div 2)+3;
SmallRect.Left :=  SmallRect.Left+3;

  with Canvas do begin
    Brush.Style:=bsClear;
    StrPCopy(OutText, FShiftCaption);
    Font.Assign(FShiftFont);
    if not Enabled then
      Font.Color:=clGrayText;
      DrawText(Handle, OutText, length(FShiftCaption), SmallRect, dt_center or dt_vcenter or dt_singleline);
  end;


end;


procedure TKBKey.DrawGlyph(Dest:TRect);
var
  Source    : TRect;
  outWidth  : integer;
  outHeight : integer;
begin
  with Canvas do begin
    { Gre des Destination-Rechtecks }
    outWidth:=  FGlyph.Width div FNumGlyphs;
    outHeight:= FGlyph.Height;
    with Source do begin
      Top:=0;
      Bottom:=FGlyph.Height;
      { Glyph 1: Normal, 2: Disabled, 3: Down;}
      if Enabled then begin
        if FStateOn and (FNumGlyphs>2) then
          Left:=(outWidth*2)+1
        else
          Left:=0;
      end
      else
        Left:=outWidth+1;
      Right:= Left+outWidth;
    end;
    Dest.Left:=  ((Dest.Right +Dest.Left - outWidth)  shr 1);
    Dest.Right:= Dest.Left+outWidth;
    Dest.Top:=   ((Dest.Bottom + Dest.Top - outHeight) shr 1);
    Dest.Bottom:=Dest.Top+outHeight;
    Pen.Color := Color;
    BrushCopy(Dest,FGlyph,Source,FGlyph.Canvas.Pixels[0,FGlyph.Height-1]);
  end;
end;

procedure TKBKey.DrawLED(var Dest:TRect);
begin
  with Canvas do begin
    if FStateOn then
      case FColorLED of
        lcAqua : Brush.Color:=clAqua;
        lcBlue : Brush.Color:=clBlue;
        lcFuchsia : Brush.Color:=clFuchsia;
        lcGray : Brush.Color:=clGray;
        lcLime : Brush.Color:=clLime;
        lcRed : Brush.Color:=clRed;
        lcWhite : Brush.Color:=clWhite;
        lcYellow : Brush.Color:=clYellow;
      end
    else
      case FColorLED of
        lcAqua : Brush.Color:=clTeal;
        lcBlue : Brush.Color:=clNavy;
        lcFuchsia : Brush.Color:=clPurple;
        lcGray : Brush.Color:=clBlack;
        lcLime : Brush.Color:=clGreen;
        lcRed : Brush.Color:=clMaroon;
        lcWhite : Brush.Color:=clSilver;
        lcYellow : Brush.Color:=clOlive;
      end;
    if not Enabled then
      Brush.Color:=FColor;
    case ButtonDirection of
      bdLeftUp : begin
        if FDown then
          OffsetRect(Dest,-FDepth div 2,0);
        Rectangle(Dest.Left+FDepth+9,Dest.Top+FDepth+3,Dest.Left+FDepth+4,Dest.Bottom-FDepth-3);
        Dest.Left:=Dest.Left+FDepth+9;
      end;
      bdRightUp : begin
        if FDown then
          OffsetRect(Dest,FDepth div 2,0);
        Rectangle(Dest.Right-FDepth-9,Dest.Top+FDepth+3,Dest.Right-FDepth-4,Dest.Bottom-FDepth-3);
        Dest.Right:=Dest.Right-FDepth-9;
      end;
      bdTopUp : begin
        if FDown then
          OffsetRect(Dest,0,-FDepth div 2);
        Rectangle(Dest.Left+FDepth+3,Dest.Top+FDepth+4,Dest.Right-FDepth-3,Dest.Top+FDepth+9);
        Dest.Top:=Dest.Top+FDepth+7;
      end;
      else begin
        if FDown then
          OffsetRect(Dest,0,FDepth div 2);
        Rectangle(Dest.Left+FDepth+3,Dest.Bottom-FDepth-9,Dest.Right-FDepth-3,Dest.Bottom-FDepth-4);
        Dest.Bottom:=Dest.Bottom-FDepth-7;
      end;
    end;
  end;
end;

procedure TKBKey.Paint;
var ARect : TRect;
begin
  Canvas.Font.Assign(Font);
  with Canvas do begin
    ARect:=GetClientRect;
    Brush.Style:=bsSolid;
    Brush.Color:=FColor;
    FillRect(ARect);
    DrawBorder(ARect);
    Pen.Color:=clBlack;
    if FShowLED then
      DrawLED(ARect);
    if (Caption<>'') then
      DrawCaption(ARect);
    if (FShiftCaption <>'')and (FShowShiftChar) then
       DrawShiftCaption(ARect);
    Brush.Color:=Self.Color;
    if Assigned(FGlyph)  then
      DrawGlyph(ARect);
  end;
end;


procedure TKBKey.MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  if Enabled then begin
    FDown:=true;
    Invalidate;
  end;

  inherited;
end;

procedure TKBKey.MouseUp(Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  if Enabled then begin
    FDown:=false;
    if FSwitching then
      FStateOn:=not FStateOn;
    Paint;
    if Assigned(FOnClick) then
       FOnClick(Self);
  end;
  inherited;
end;



constructor TBaseFPad.Create(AOwner: TComponent);
var
KeyP : KeyDataP;
KeyCount : INteger;
Picture  : TBitmap;
begin
inherited;
Height := 250;
Width := 200;
ParentFont := True;
FSelectState := False;
KeyLitTimer := TTimer.Create(Self);
SetModifiedCursorKeys(False);

FuncKey := TKbKey.Create(Self);
with FuncKey do
     begin
     Parent := Self;
     Switching := False;
     StateOn := True;
     MainCaption := 'Cursor';
     Color := clGray;
     ColorLed := lcRed;
     Top := 5;
     Left := 2;
     Height := 35;
     Width := 48;
     Tag := 16;
     OnMouseDown := FKeyMouseDown;
     OnMouseUp := KeyMouseUp;
     end;
FuncKey.Font := Font;
NumKey := TKbKey.Create(Self);
with NumKey do
     begin
     Parent := Self;
     Switching := False;
     StateOn := False;
     MainCaption := 'NUM';
     Color := clGray;
     ColorLed := lcRed;
     Top := 5;
     Left := 52;
     Height := 35;
     Width := 48;
     Tag := 17;
     OnMouseDown := FKeyMouseDown;
     OnMouseUp := KeyMouseUp;
     end;

with KeyLitTimer do
     begin
     Enabled := False;
     Interval := 500;
     OnTimer := KeyLitTimerOut;
     end;
RepeatDelTimer := TTimer.Create(Self);
with RepeatDelTimer do
     begin
     Enabled := False;
     Interval := 200;
     OnTimer := RepTimerOut;
     end;
isRepeating := False;
CurrentKey := nil;
NumPage := TPanel.Create(Self);
with NumPage do
     begin
     Parent := Self;
     Height := 200;
     Visible := False;
     ParentFont := True;
     end;
NumPage.Font := Font;
NumPage.Top := Height-201;
FuncPage := TPanel.Create(Self);
with FuncPage do
     begin
     Parent := Self;
     Height := 200;
     Visible := True;
     ParentFont := True;
     end;
FuncPage.Font := Font;
FuncPage.Top := Height-201;
FuncKeyList := TList.Create;
NumKeyList := TList.Create;
FuncKeyList.Clear;
NumKeyList.Clear;
// function key creation
for KeyCount := 0 to 15 do
    begin
    New(KeyP);
    KeyP.NKey := TKbKey.Create(Self);
    KeyP.NKey.Font := Font;
    KeyP.NKey.OnMouseDown := FKeyMouseDown;
    KeyP.NKey.OnMouseUp := KeyMouseUp;
    KeyP.NKey.Tag := KeyCount;
    KeyP.NKey.ShowLED := False;
    KeyP.NKey.Parent  := FuncPage;
    KeyP.Row := KeyCount div 4;
    KeyP.Col := KeyCount - (4*KeyP.Row);
    KeyP.isSpecial := False;
    FuncKeyList.Add(KeyP);
    end;

Keyp := FuncKeyList[0];
Keyp.Output := VK_HOME;
with Keyp.Nkey do
     begin
     MainCaption := 'HOME';
     Color := clTeal;
     Font.Color := clYellow;
     Font.Style := [fsbold];
     CanRepeat := False;
     end;

Keyp := FuncKeyList[1];
Keyp.Output := CUpCode;
with Keyp.Nkey do
     begin
     MainCaption := '';
     NumGlyphs := 2;
     Picture := TBitmap.create;
     try
     Picture.LoadFromResourceName(HInstance,'UPARROW');
     Glyph := Picture;
     finally
     Picture.free;
     end;
     CanRepeat := True;
     end;

Keyp := FuncKeyList[2];
Keyp.Output := VK_PRIOR;
with Keyp.Nkey do
     begin
     Color := clTeal;
     Font.Color := clYellow;
     Font.Style := [fsbold];
     MainCaption := 'PgUp';
     CanRepeat := False;
     end;

Keyp := FuncKeyList[3];
Keyp.Output := VK_INSERT;
with Keyp.Nkey do
     begin
     Color := clSilver;
     Font.Style := [fsbold];
     MainCaption := 'Insert';
     CanRepeat := False;
     end;



Keyp := FuncKeyList[4];
Keyp.Output := CLeftCode;


with Keyp.Nkey do
     begin
     MainCaption := '';
     Picture := TBitmap.create;
     NumGlyphs := 2;
     try
     Picture.LoadFromResourceName(HInstance,'LEFTARROW');
     Glyph := Picture;
     finally
     Picture.free;
     end;
     CanRepeat := True;
     end;

Keyp := FuncKeyList[5];
Keyp.Output := 0;
Keyp.isSpecial := True;
with Keyp.Nkey do
     begin
     Color := clMaroon;
     ShowLed := True;
     ColorLED := lcLime;
     Font.Color := clYellow;
     Font.Style := [fsbold];
     MainCaption := 'Select';
     Switching := False;
     CanRepeat := False;
     end;

Keyp := FuncKeyList[6];
Keyp.Output := CRightCode;
with Keyp.Nkey do
     begin
     MainCaption := '';
     Picture := TBitmap.create;
     NumGlyphs := 2;
     try
     Picture.LoadFromResourceName(HInstance,'RIGHTARROW');
     Glyph := Picture;
     finally
     Picture.free;
     end;
     CanRepeat := True;
     end;

Keyp := FuncKeyList[7];
Keyp.Output := VK_DELETE;
with Keyp.Nkey do
     begin
     Color := clSilver;
     Font.Style := [fsbold];
     MainCaption := 'Delete';
     CanRepeat := False;
     end;

Keyp := FuncKeyList[8];
Keyp.Output := VK_END;
with Keyp.Nkey do
     begin
     Color := clTeal;
     Font.Color := clYellow;
     Font.Style := [fsbold];
     MainCaption := 'END';
     CanRepeat := False;
     end;

Keyp := FuncKeyList[9];
Keyp.Output := CDownCode;
with Keyp.Nkey do
     begin
     MainCaption := '';
     Picture := TBitmap.create;
     NumGlyphs := 2;
     try
     Picture.LoadFromResourceName(HInstance,'DOWNARROW');
     Glyph := Picture;
     finally
     Picture.free;
     end;
     CanRepeat := True;
     end;

Keyp := FuncKeyList[10];
Keyp.Output := VK_NEXT;
with Keyp.Nkey do
     begin
     Color := clTeal;
     Font.Color := clYellow;
     Font.Style := [fsbold];
     MainCaption := 'PgDn';
     CanRepeat := False;
     end;

Keyp := FuncKeyList[11];
Keyp.Output := 0;
Keyp.isSpecial := True;
with Keyp.Nkey do
     begin
     Color := clSilver;
     Font.Style := [fsbold];
     MainCaption := 'Copy';
     CanRepeat := False;
     end;

Keyp := FuncKeyList[12];
Keyp.Output := 0;
Keyp.isSpecial := True;
with Keyp.Nkey do
     begin
     Color := clSilver;
     Font.Style := [fsbold];
     MainCaption := 'Prev';
     CanRepeat := True;
     end;

Keyp := FuncKeyList[13];
Keyp.Output := 0;
Keyp.isSpecial := True;
with Keyp.Nkey do
     begin
     Color := clSilver;
     Font.Style := [fsbold];
     MainCaption := 'Next';
     CanRepeat := True;
     end;

Keyp := FuncKeyList[14];
Keyp.Output := 0;
Keyp.isSpecial := True;

with Keyp.Nkey do
     begin
     Color := clSilver;
     Font.Style := [fsbold];
     MainCaption := 'Cut';
     CanRepeat := False;
     end;

Keyp := FuncKeyList[15];
Keyp.Output := 0;
Keyp.isSpecial := True;

with Keyp.Nkey do
     begin
     Color := clSilver;
     Font.Style := [fsbold];
     MainCaption := 'Paste';
     CanRepeat := False;
     end;

 // Numeric Key Pad Keys
 for KeyCount := 0 to 15 do
    begin
    New(KeyP);
    KeyP.NKey := TKbKey.Create(Self);
    KeyP.NKey.MainCaption := NumberKeys[KeyCount+1];
    KeyP.NKey.Font := Self.Font;
    KeyP.NKey.Font.Size := 12;
    KeyP.NKey.Font.Style := [fsbold];
    KeyP.NKey.CanRepeat := True;
    KeyP.Output := NumberKeyCodes[KeyCount+1];
    KeyP.NKey.OnMouseDown := FKeyMouseDown;
    KeyP.NKey.OnMouseUp := KeyMouseUp;
    KeyP.NKey.Tag := KeyCount+20;
    KeyP.NKey.ShowLED := False;
    KeyP.NKey.Parent  := NumPage;
    KeyP.Row := KeyCount div 4;
    KeyP.Col := KeyCount - (4*KeyP.Row);
    KeyP.isSpecial := False;
    NumKeyList.Add(KeyP);
    end;

 Resize;
end;

destructor  TBaseFPad.Destroy;
begin
FuncKeyList.Free;
NumKeyList.Free;
if assigned(KeyLitTimer) then KeyLitTimer.Free;
if assigned(RepeatDelTimer) then  RepeatDelTimer.Free;

inherited;
end;

procedure TBaseFPad.Resize;
var
KeyCount : Integer;
KeyWidth,KeyHeight : Integer;
begin
if not(assigned(FuncPage) and assigned(NumPage))then exit;
//FuncPage.Align := alBottom;
//NumPage.Align := alBottom;
FuncPage.Height := (height div 5)*4;
FuncPage.Width := Width-2;
FuncPage.Top := (height div 5);
FuncPage.Left :=1;

NumPage.Height := FuncPage.Height;
NumPage.Width := FuncPage.Width;
NumPage.Top := FuncPage.Top;
NumPage.Left :=FuncPage.Left;
KeyHeight := FuncPage.Height div 4;
KeyWidth := FuncPage.Width div 4;
NumKey.Height := (height div 8);
FuncKey.Height := (height div 8);
NumKey.Width := KeyWidth-1;
FuncKey.Left := 1;
NumKey.Left := KeyWidth+2;
FuncKey.Width := KeyWidth-1;
for KeyCount := 0 to FuncKeyList.Count-1 do
    begin
    KeyDataP(FuncKeyList[KeyCount]).NKey.Height := KeyHeight;
    KeyDataP(FuncKeyList[KeyCount]).NKey.Width := KeyWidth;
    KeyDataP(FuncKeyList[KeyCount]).NKey.Top := KeyDataP(FuncKeyList[KeyCount]).Row * KeyHeight;
    KeyDataP(FuncKeyList[KeyCount]).NKey.Left := KeyDataP(FuncKeyList[KeyCount]).Col * KeyWidth;
    KeyDataP(NumKeyList[KeyCount]).NKey.Height := KeyHeight;
    KeyDataP(NumKeyList[KeyCount]).NKey.Width := KeyWidth;
    KeyDataP(NumKeyList[KeyCount]).NKey.Top := KeyDataP(NumKeyList[KeyCount]).Row * KeyHeight;
    KeyDataP(NumKeyList[KeyCount]).NKey.Left := KeyDataP(NumKeyList[KeyCount]).Col * KeyWidth;
    end;



end;

procedure TBaseFPad.SetTargetMode(Value : Boolean);
begin
FTargetMode :=Value;
end;

procedure TBaseFPad.SetModifiedCursorKeys(Value : Boolean);
begin
FChangeCursorKeys := Value;
if FChangeCursorKeys then
   begin
   CUpCode := VK_F13;
   CDownCode:= VK_F14;
   CLeftCode   := VK_F15;
   CRightCode := VK_F16;
   end
else
   begin
   CLeftCode := VK_LEFT;
   CRightCode:= VK_RIGHT;
   CUpCode   := VK_UP;
   CDownCode := VK_DOWN;
   end;

end;

procedure TBaseFPad.DoMultiKeyPressed(Sender : TObject; KeyCode : INteger);
begin
if assigned(FOnMultiKey) then FOnMultikey(Sender,KeyCode);
end;


procedure TBaseFPad.SetOnMultiKey(Value : TMultiKeyEvent);
begin
FOnMultiKey := Value;
end;

procedure TBaseFPad.FKeyMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var
KeyIndex : Integer;
KeyC     : Byte;
KeyP     : KeyDataP;
begin
inherited;

if Assigned(FocusControl) and (FTargetMode = False) then
   begin
   FocusControl.SetFocus;
   end;
with Sender as TKbKey do
     begin
     KeyIndex := Tag;
     if assigned(CurrentKey) then
        begin
        CurrentKey.Color := CurrentKeyColor;
        end;

     case KeyIndex of
       1, 4, 6, 9 : Exit;
     end;

     CurrentKey := TKbKey(Sender);
     CurrentKeyColor := CurrentKey.Color;
     CurrentKey.Color := clLime;
     KeyLitTimer.Enabled := True;
     if KeyIndex < 16 then
        begin
        if CurrentKey.CanRepeat then RepeatDelTimer.Enabled := True;

        if KeyDataP(FuncKeyList[KeyIndex]).Output > 0 then
           begin
           if  FSelectState then
               begin
               keybd_event(VK_SHIFT,0,0,0);
               end;
           KeyC := KeyDataP(FuncKeyList[KeyIndex]).Output;
           Sendkey(KeyC,shift,kmMomentary);

           if  FSelectState then keybd_event(VK_SHIFT,0,KEYEVENTF_KEYUP,0);
           end
        else
            begin
            if FTargetMode then
               begin
               case KeyIndex of

                     5:
                     begin
                     // Select Mode Toggle
                     FSelectState := not FSelectState;
                     StateOn := FSelectState;
                     end;


                     11:
                     begin
                     // Copy Key == Ctrl Ins
                     DoMultiKeyPressed(Self,KeyIndex);
                     if  FSelectState then
                         begin
                         FSelectState := False;
                         Keyp := FuncKeyList[5];
                         Keyp.NKey.StateOn := False;
                         end;
                     end;

                     12:
                     // Previous Key == CTRL and LEFT
                     begin
                     DoMultiKeyPressed(Self,KeyIndex);
                     if  FSelectState then
                         begin
                         keybd_event(VK_SHIFT,0,KEYEVENTF_KEYUP,0);
                         end;
                     end;

                     13:
                     begin
                     // Next Key == CTRL and Right
                     DoMultiKeyPressed(Self,KeyIndex);
                     if  FSelectState then
                         begin
                         keybd_event(VK_SHIFT,0,KEYEVENTF_KEYUP,0);
                         end;

                     end;
                     14:
                     begin
                     // Cut Key = Copy then Delete
                     DoMultiKeyPressed(Self,KeyIndex);
                     if  FSelectState then
                         begin
                         FSelectState := False;
                         Keyp := FuncKeyList[5];
                         Keyp.NKey.StateOn := False;
                         end;

                     end;

                     15:
                     begin
                     // Paste Key == Shift Ins
                     DoMultiKeyPressed(Self,KeyIndex);
                     if  FSelectState then
                         begin
                         FSelectState := False;
                         Keyp := FuncKeyList[5];
                         Keyp.NKey.StateOn := False;
                         end;
                     end;
                     end;// case


               end
            else
                begin
                case KeyIndex of

                     5:
                     begin
                     // Select Mode Toggle
                     FSelectState := not FSelectState;
                     StateOn := FSelectState;
                     end;


                     11:
                     begin
                     // Copy Key == Ctrl Ins
                     keybd_event(VK_CONTROL,0,0,0);
                     keybd_event(VK_INSERT,0,0,0);
                     keybd_event(VK_INSERT,0,KEYEVENTF_KEYUP,0);
                     keybd_event(VK_CONTROL,0,KEYEVENTF_KEYUP,0);
                     if  FSelectState then
                         begin
                         FSelectState := False;
                         Keyp := FuncKeyList[5];
                         Keyp.NKey.StateOn := False;
                         end;
                     end;

                     12:
                     begin
                     // Previous Key == CTRL and LEFT
                     if  FSelectState then keybd_event(VK_SHIFT,0,0,0);
                     keybd_event(VK_CONTROL,0,0,0);
                     keybd_event(CLeftCode,0,0,0);
                     keybd_event(CLeftCode,0,KEYEVENTF_KEYUP,0);
                     keybd_event(VK_CONTROL,0,KEYEVENTF_KEYUP,0);
                     if  FSelectState then
                         begin
                         keybd_event(VK_SHIFT,0,KEYEVENTF_KEYUP,0);
                         end;
                     end;

                     13:
                     begin
                     // Next Key == CTRL and Right
                     if  FSelectState then keybd_event(VK_SHIFT,0,0,0);
                     keybd_event(VK_CONTROL,0,0,0);
                     keybd_event(CRightCode,0,0,0);
                     keybd_event(CRightCode,0,KEYEVENTF_KEYUP,0);
                     keybd_event(VK_CONTROL,0,KEYEVENTF_KEYUP,0);
                     if  FSelectState then
                         begin
                         keybd_event(VK_SHIFT,0,KEYEVENTF_KEYUP,0);
                         end;
                     end;

                     14:
                     begin
                     // Cut Key = Copy then Delete
                     keybd_event(VK_CONTROL,0,0,0);
                     keybd_event(VK_INSERT,0,0,0);
                     keybd_event(VK_INSERT,0,KEYEVENTF_KEYUP,0);
                     keybd_event(VK_CONTROL,0,KEYEVENTF_KEYUP,0);
                     keybd_event(VK_DELETE,0,0,0);
                     keybd_event(VK_DELETE,0,KEYEVENTF_KEYUP,0);
                     if  FSelectState then
                         begin
                         FSelectState := False;
                         Keyp := FuncKeyList[5];
                         Keyp.NKey.StateOn := False;
                         end;

                     end;

                     15:
                     begin
                     // Paste Key == Shift Ins
                     keybd_event(VK_SHIFT,0,0,0);
                     keybd_event(VK_INSERT,0,0,0);
                     keybd_event(VK_INSERT,0,KEYEVENTF_KEYUP,0);
                     keybd_event(VK_SHIFT,0,KEYEVENTF_KEYUP,0);
                     if  FSelectState then
                         begin
                         FSelectState := False;
                         Keyp := FuncKeyList[5];
                         Keyp.NKey.StateOn := False;
                         end;
                     end;
                end; //case;
                end; // if TargetMode
            end; // if output
          end
        else
            begin
            if KeyIndex = 16 then begin
              NavigationKeys := True;
            end;
            if KeyIndex = 17 then begin
              NavigationKeys := False;
            end;
            if (KeyIndex >=20) and (KeyIndex <= 36) then
               begin
               if CurrentKey.CanRepeat then RepeatDelTimer.Enabled := True;
               // Here for Numeric keys wigth Key Index from 20 to 36
               KeyC := KeyDataP(NumKeyList[KeyIndex-20]).Output;
               Sendkey(KeyC,shift,kmMomentary);
               end;
            end;

     end; // with sender


end;

procedure TBaseFPad.SendKey(VCode: Integer;Shift : TShiftState;Mode : TKeyMode);
function IsNT : boolean;
begin
  Result:=(GetVersion<$80000000);
end;

var
Code : Byte;
begin
if FTargetMode then
   begin
   FExternalKeyMode := Mode;
   FExternalKeyCode := VCode;
   FExternalShift   := Shift;
   Code := Byte(Vcode);
   if assigned(FOnKey) then FOnkey(Self,Char(Code));
  end
else
    Begin
    if assigned(FocusControl) then
       begin
       FocusControl.SetFocus;
       end;

    if ssShift in shift then
       begin
       keybd_event(VK_SHIFT,0,0,0);
       end;
     Code := Byte(Vcode);
     case Mode of
          kmDown:
          begin
          keybd_event(Code,0,0,0);
          end;

          kmUp:
          begin
          keybd_event(Code,0,KEYEVENTF_KEYUP,0);
          end;

          kmMomentary:
          begin
          keybd_event(Code,0,KEYEVENTF_KEYUP,0);
          keybd_event(Code,0,0,0);
          keybd_event(Code,0,KEYEVENTF_KEYUP,0);
          end;
     end;// case

     if ssShift in shift then
        begin
        keybd_event(VK_SHIFT,0,KEYEVENTF_KEYUP,0);
        end;

     end;

end;


procedure TBaseFPad.RepTimerOut(Sender: TObject);
begin
if not isRepeating then
   begin
   isRepeating := True;
   RepeatDelTimer.Interval := 50;
   end
else
    begin
    if assigned(CurrentKey) then
       begin
       if CurrentKey.CanRepeat then FKeyMouseDown(CurrentKey,mbLeft,[],0,0);
       end;
    end;
end;
procedure TBaseFPad.KeyLitTimerOut(Sender: TObject);
begin
if assigned(CurrentKey) then CurrentKey.Color := CurrentKeyColor;
KeyLitTimer.Enabled  := False;
end;



procedure TBaseFPad.KeyMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  with Sender as TKbKey do begin
    case Tag of
      1, 4, 6, 9 :
           Sendkey(KeyDataP(FuncKeyList[Tag]).Output, shift, kmMomentary);
    end;
  end;

if assigned(RepeatDelTimer) then
   begin
   RepeatDelTimer.Enabled := False;
   RepeatDelTimer.Interval :=500;
   end;
isRepeating := False;
end;


procedure TBaseFPad.SetNavigationKeys(Nav: Boolean);
begin
  if Nav then begin
    NumKey.StateOn := False;
    FuncKey.StateOn := True;
    NumPage.Visible := False;
    FuncPage.Visible := True;
  end else begin
    NumKey.StateOn := True;
    FuncKey.StateOn := False;
    NumPage.Visible := True;
    FuncPage.Visible := False;
  end;
end;

end.
