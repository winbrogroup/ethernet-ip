unit KBControl;

interface

uses CoreCNC, Buttons, Classes, Controls, ExtCtrls;

type
  TKBType = (
    kbNumeric,
    kbAlpha,
    kbNavigation
  );
  TKBTypes = set of TKBType;

  TKBControlHelper = class(TObject)
  private
    procedure DoPress(Sender : TObject);
  public
    function CreateButton(Style: TKBTypes; Display: TAbstractDisplay; Align : TAlign): TPanel;
  end;

var
  __KBC : TKBControlHelper;

implementation

{ KBControlHelper }

type
  TKBSelectButton = class(TSpeedButton)
  public
    Style : TKBType;
    Display : TAbstractDisplay;
    constructor Create(aOwner : TComponent); override;
  end;


function TKBControlHelper.CreateButton(Style: TKBTypes;
  Display: TAbstractDisplay; Align : TAlign): TPanel;
var Button : TKBSelectButton;
    I : TKBType;
    Count : Integer;
begin
  Count := 0;
  Result := TPanel.Create(Display);
  Result.Parent := Display;
  Result.Align := Align;
  Result.Height := 36;

  for I := Low(TKBType) to High(TKBType) do begin
    if I in Style then begin
      Button := TKBSelectButton.Create(Result);
      Button.Parent := Result;
      Button.OnClick := DoPress;
      Button.Style := I;
      Button.Left := Count * 54;
      Button.Top := 2;
      Button.Display := Display;
      case I of
        kbNumeric : Button.Glyph.LoadFromResourceName(HInstance, 'KBNUMERIC');
        kbAlpha : Button.Glyph.LoadFromResourceName(HInstance, 'KBALPHA');
        kbNavigation : Button.Glyph.LoadFromResourceName(HInstance, 'KBARROW');
      end;
      Inc(Count);
    end;
  end;
end;

procedure TKBControlHelper.DoPress(Sender: TObject);
begin
  with Sender as TKBSelectButton do begin
    case Style of
      kbAlpha :
        if SysObj.Layout.KeyboardVisible then
          SysObj.Layout.HideKeyboard
        else
          SysObj.Layout.ShowKeyboard(Display);
      kbNumeric :
        if SysObj.Layout.NumericPadVisible then
          SysObj.Layout.HideNumericPad
        else
          SysObj.Layout.ShowNumericPad(Display);

      kbNavigation :
        if SysObj.Layout.NavigationVisible then
          SysObj.Layout.HideNavigation
        else
          SysObj.Layout.ShowNavigation(Display);

    end;
  end;
end;

{ TKBSelectButton }

constructor TKBSelectButton.Create(aOwner: TComponent);
begin
  inherited;
  Top := 2;
  Width := 52;
  Height := 32;
end;

initialization
  __KBC := TKBControlHelper.Create;
end.
