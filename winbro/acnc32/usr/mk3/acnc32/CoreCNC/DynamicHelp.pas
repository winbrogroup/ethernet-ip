unit DynamicHelp;

interface

uses SysUtils, Classes, Windows, CNC32, CNCTypes, IFSDServer;

type
  THelpRecord = class(TObject)
    Path : string;
    Hint : string;
    TimeDateStamp : TDateTime;
  end;

  TCGIStringList = class (TInterfacedObject, ICGIStringList)
  private
    FStrings: TStringList;
  public
    constructor Create;
    function GetValue(Name: WideString): WideString; stdcall;
    property Strings: TStringList read FStrings;
  end;

  TDynamicHelp = class(TInterfacedObject, IHttpServerComponent)
  private
    List : TList;
    ComponentList : TList;
    FMaxHistory : Integer;
    function GetHistory(Index: Integer): THelpRecord;
    procedure WriteMETAInfo(hFile : THandle; CGIR : ICGIStringList; var Page : Integer);
  public
    Path : string;
    constructor Create;
    destructor Destroy; override;
    procedure Add(const aHelp, Hint : string);
    procedure WriteHTTPFile(hFile : THANDLE; const F : string);
    function HTTPAddStreaming(Stream : THandle; CGIR : WideString; Method : IHTTPServer; DT : Integer) : Boolean; stdcall;
    procedure HTTPRemoveStreaming(Stream : THandle; CGIR : WideString); stdcall;
    function HTTPGeneratePage(HFile: THANDLE; var CGIPath : WideString; CGIRequest: ICGIStringList): Boolean; stdcall;
    procedure RegisterComponent(Comp : TObject);
    property MaxHistory : Integer read FMaxHistory write FMaxHistory;
    property History [Index : Integer] : THelpRecord read GetHistory;
  end;

var
  DynamicHelpSystem : TDynamicHelp;

implementation

uses CoreCNC;

{ TDynamicHelp }

procedure TDynamicHelp.Add(const aHelp, Hint : string);
var Help : THelpRecord;
begin
  Help := THelpRecord.Create;
  Help.Path := aHelp;
  Help.Hint := Hint;
  Help.TimeDateStamp := Now;
  List.Insert(0, Help);

  while List.Count > MaxHistory do begin
    TObject(List[List.Count - 1]).Free;
    List.Delete(List.Count - 1);
  end;
end;

constructor TDynamicHelp.Create;
var I : Integer;
begin
  inherited Create;
  List := TList.Create;
  ComponentList := TList.Create;
  MaxHistory := 20;

  I := Languages.IndexOf(SysLocale.DefaultLCID);
  Path := Copy(Languages.Ext[I], 1, 2);
  Path := 'help\' + Path + '\';
  SysObj.HttpServerHost.AddComponentPath(HttpMaintenanceSection, 'EventHelp', 'Event context help', Self);
end;

destructor TDynamicHelp.Destroy;
begin
  inherited Destroy;
end;

procedure TDynamicHelp.RegisterComponent(Comp : TObject);
begin
  ComponentList.Add(Comp);
end;

procedure TDynamicHelp.WriteHTTPFile(hFile : THANDLE; const F : string);
var FileStream : TFileStream;
    C : Char;
    Count : Cardinal;
begin
  if F <> '' then begin
    try
      FileStream := TFileStream.Create(LivePath + Path + F, fmOpenRead);
      try
        while FileStream.Read(C, 1) <> 0 do
          WriteFile(hFile, C, 1, Count, nil);
      finally
        FileStream.Free;
      end;
    except
      WriteStringToHandle(hFile, '<p>Help file ' + F + ' not found</p>'  + CarRet);
    end;
  end else begin
    WriteStringToHandle(hFile, '<p>No help available</p>'  + CarRet);
  end;
end;

function TDynamicHelp.GetHistory(Index: Integer): THelpRecord;
begin
  Result := THelpRecord(List[Index]);
end;

procedure TDynamicHelp.WriteMETAInfo(hFile : THandle; CGIR : ICGIStringList; var Page : Integer);
begin
  WriteStringToHandle(hFile, '<META Name = "EventContext" Content = "True">' + CarRet);
  if Page < List.Count - 1 then
    WriteStringToHandle(hFile, '<META Name = "NextPage" Content = "?Page=' + IntToStr(Page + 1) + '">' + CarRet);
  if (Page > 0) and (List.Count > 1) then
    WriteStringToHandle(hFile, '<META Name = "PreviousPage" Content = "?Page=' + IntToStr(Page - 1) + '">' + CarRet);
end;

function TDynamicHelp.HTTPGeneratePage(HFile: THANDLE; var CGIPath: WideString;
  CGIRequest: ICGIStringList): Boolean;

  procedure WriteSimpleIndex;
  begin
    WriteStringToHandle(hFile, '<table border="0"><tr>');
    WriteStringToHandle(hFile, '<td><a href="/cgi-bin/help/contents/">Help contents</a></td>');
    WriteStringToHandle(hFile, '<td><a href="/cgi-bin/">Home Page</a></td>');
    WriteStringToHandle(hFile, '</tr></table>');
  end;

var Help : THelpRecord;
    TopLevel : string;
    Page : Integer;
begin
  TopLevel := LowerCase(CNCTypes.ReadDelimitedW(CGIPath, '/'));

  if CompareText(TopLevel, '') = 0 then begin
    if List.Count = 0 then begin
      WriteACNC32HTTPTitle(hFile, 'Acnc32 Help');
      WriteStringToHandle(hFile, '<p>Nothing that requires help has occured</p>'  + CarRet);
    end else begin
      try
        Page := StrToInt(CGIRequest.GetValue('Page'));
      except
        Page := 0;
      end;

      Help := History[Page];

      WriteMETAInfo(hFile, CGIRequest, Page);
      WriteACNC32HTTPTitle(hFile, 'Acnc32 Help');
      WriteStringToHandle(hFile, '<h2>ACNC32 Help </h2>');
      WriteStringToHandle(hFile, Format('<p>Page %d of %d : %s</p>', [Page + 1, List.Count, DateTimeToStr(Help.TimeDateStamp)]));
      WriteStringToHandle(hFile, Format('<h3>%s</h3>', [Help.Hint]));
      WriteHTTPFile(hFile, Help.Path);
      WriteStringToHandle(hFile, '<hr>');
      WriteStringToHandle(hFile, '<table border="0"><tr>');

//      Page := List.Count - Page;
      if (Page > 0) and (List.Count > 1) then
        WriteStringToHandle(hFile, Format('<td><a href="?Page=%d">Previous</a></td>', [Page - 1]));
      if Page < List.Count - 1 then
        WriteStringToHandle(hFile, Format('<td><a href="?Page=%d">Next</a></td>', [Page + 1]));
      WriteStringToHandle(hFile, '</tr></table>');
    end;
    WriteStringToHandle(hFile, '<hr>');
    WriteSimpleIndex;
  end else begin
    Result:= False;
    Exit;
  end;


  WriteStringToHandle(hFile, '</body></html>');
  Result := True;
end;

function TDynamicHelp.HTTPAddStreaming(Stream: THandle; CGIR: WideString;
  Method: IHTTPServer; DT: Integer): Boolean;
begin
  Result:= False;
end;

procedure TDynamicHelp.HTTPRemoveStreaming(Stream: THandle;
  CGIR: WideString);
begin

end;

{ TCGIStringList }

constructor TCGIStringList.Create;
begin
  FStrings:= TStringList.Create;
end;

function TCGIStringList.GetValue(Name: WideString): WideString;
begin
  Result:= FStrings.Values[Name];
end;


end.
