object About: TAbout
  Left = 403
  Top = 337
  BorderStyle = bsToolWindow
  Caption = 'ACNC32 About'
  ClientHeight = 396
  ClientWidth = 474
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -16
  Font.Name = 'Verdana'
  Font.Style = [fsBold]
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 18
  object Panel1: TPanel
    Left = 0
    Top = 355
    Width = 474
    Height = 41
    Align = alBottom
    BevelOuter = bvLowered
    TabOrder = 0
    object BitBtn1: TBitBtn
      Left = 176
      Top = 6
      Width = 90
      Height = 30
      TabOrder = 0
      Kind = bkClose
    end
  end
  object VersionPanel: TPanel
    Left = 0
    Top = 0
    Width = 474
    Height = 41
    Align = alTop
    BevelOuter = bvLowered
    Caption = 'Version'
    TabOrder = 1
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 41
    Width = 474
    Height = 314
    Align = alClient
    Caption = 'Active Plugins'
    TabOrder = 2
    object StringGrid: TStringGrid
      Left = 2
      Top = 20
      Width = 470
      Height = 292
      Align = alClient
      ColCount = 2
      DefaultColWidth = 222
      FixedCols = 0
      RowCount = 20
      TabOrder = 0
    end
  end
end
