unit SelectTagForm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, CNC32, CoreCNC;

type
  TSelectTag = class(TForm)
    Panel1: TPanel;
    ListBox: TListBox;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    procedure ListBoxDblClick(Sender: TObject);
  private
    { Private declarations }
  public
    class function Execute(args : array of TTagClass) : TAbstractTag;
  end;

implementation

{$R *.DFM}

var
  SelectTag: TSelectTag;

class function TSelectTag.Execute(Args : array of TTagClass) : TAbstractTag;
var I, J : Integer;
begin
  Result := nil;
  if SelectTag = nil then begin
    SelectTag := TSelectTag.Create(Application);
    try
      with SelectTag do begin
        ListBox.Items.AddObject('None', nil);
        for I := 0 to CoreCNC.SysObj.Comms.NamedLayer.Count - 1 do begin
          for J := 0 to High(Args) do begin
            if CoreCNC.SysObj.Comms.NamedLayer.Tag[I] is Args[J] then begin
              ListBox.Items.AddObject(CoreCNC.SysObj.Comms.NamedLayer.Tag[I].Name, CoreCNC.SysObj.Comms.NamedLayer.Tag[I]);
              Break;
            end;
          end;
        end;

        if ShowModal = mrOk then
          if ListBox.ItemIndex <> -1 then
            Result := TAbstractTag(ListBox.Items.Objects[ListBox.ItemIndex]);
      end;
    finally
      SelectTag.Free;
      SelectTag := nil;
    end;
  end;
end;

procedure TSelectTag.ListBoxDblClick(Sender: TObject);
begin
  ModalResult := mrOk;
end;

end.
