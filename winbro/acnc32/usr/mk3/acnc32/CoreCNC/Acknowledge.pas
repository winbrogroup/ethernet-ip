unit Acknowledge;

interface

uses Windows, SysUtils, Classes, Graphics, Forms, Controls, StdCtrls,
  Buttons, ExtCtrls;

type
  TOKBottomDlg = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    MessageLab: TLabel;
    BtnCancel: TBitBtn;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
  private
    procedure SetCaptionText(aText : string);
  public
  property
    Text : String write SetCaptionText;
  end;

var
  OKBottomDlg: TOKBottomDlg;

function AreYouSure(aText : string) : Boolean;
function YesNoCancel(aText : string) : TModalResult;

implementation

uses CoreCNC;

resourceString
  ConfirmText = 'Confirm';
{$R *.DFM}

function AreYouSure(aText : string) : Boolean;
var Dlg : TOKBottomDlg;
begin
  Dlg := TOKBottomDlg.Create(Application);
  SysObj.NC.AcquireModeChangeLock;
  try
    Dlg.Text := aText;
    Dlg.Caption := ConfirmText;
    Dlg.BtnCancel.Visible := False;
    case Dlg.ShowModal of
      mrOk : result := True;
    else
      result := False;
    end;
  finally
    Dlg.Free;
    SysObj.NC.ReleaseModeChangeLock;
  end;
end;

function YesNoCancel(aText : string) : TModalResult;
var Dlg : TOKBottomDlg;
begin
  Dlg := TOKBottomDlg.Create(Application);
  SysObj.NC.AcquireModeChangeLock;
  try
    Dlg.Text := aText;
    Dlg.Caption := ConfirmText;
    Result := Dlg.ShowModal;
  finally
    SysObj.NC.ReleaseModeChangeLock;
    Dlg.Free;
  end;
end;

procedure TOKBottomDlg.SetCaptionText(aText : string);
begin
  MessageLab.Caption := aText;
end;


end.
