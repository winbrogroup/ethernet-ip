unit SectionEditorPlugin;

interface

uses Classes, Windows, CoreCNC, CompoundFiles, SysUtils, CNC32, CNCTypes, Forms;

type
  TOPPPluginEditorExecute = function ( SectionName : PChar;
                                       Buffer : PChar;
                                       MaxSize : Integer
  ) : Boolean; stdcall;


  TSectionEditorPlugin = class(TSectionEditor)
  private
    OPPPluginEditorExecute : TOPPPluginEditorExecute;
  public
    HMod : HMODULE;
    Section : string;
    procedure InstallComponent(Params : TParamStrings); override;
    function Execute(Manager: TCompoundFile): Boolean; override;
    function SectionName: string; override;
  end;

implementation

procedure TSectionEditorPlugin.InstallComponent(Params : TParamStrings);
  function GetProcAddressX(hModule: HMODULE; lpProcName: LPCSTR): FARPROC;
  begin
    Result := GetProcAddress(hModule, lpProcName);
    if not Assigned(Result) then
      raise Exception.CreateFmt('Library interface missing [%s]', [lpProcName]);
  end;
begin
  inherited InstallComponent(Params);
  OPPPluginEditorExecute := GetProcAddressX(HMod, 'OPPPluginEditorExecute');
end;

function TSectionEditorPlugin.Execute(Manager: TCompoundFile): Boolean;
var Buffer : PChar;
    S : TStringList;
begin
  Result:= false;
  S := TStringList.Create;
  GetMem(Buffer, $80000);
  try
    try
      Manager.SectionOpen(SectionName, S);
      StrPLCopy(Buffer, S.Text, $80000);
      Result := OPPPluginEditorExecute(PChar(SectionName), Buffer, $80000);
      S.Text := Buffer;
    except
      on E: Exception do
        Application.ShowException(E)
    end
  finally
    Manager.SectionClose(S, Result);
    S.Free;
    Dispose(Buffer);
  end;
end;

function TSectionEditorPlugin.SectionName: string;
begin
  Result := Section;
end;

end.
