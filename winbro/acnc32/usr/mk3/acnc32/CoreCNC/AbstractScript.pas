unit AbstractScript;

interface

uses SysUtils, Classes, IScriptInterface, INamedInterface;

type
  (* Class to wrap IScriptMethod internally *)
  TScriptMethod = class(TInterfacedObject, IScriptMethod)
  private
    FHost: IScriptHost;
  public
    procedure Reset; virtual; stdcall;
    function Name: WideString; virtual; stdcall;
    function IsFunction: Boolean; virtual; stdcall;
    function ParameterPassing(Index: Integer): Integer; virtual; stdcall;
    function ParameterType(Index: Integer): Integer; virtual; stdcall;
    function ParameterName(Index: Integer): WideString; virtual; stdcall;
    function ReturnType: Integer; virtual; stdcall;
    function ParameterCount: Integer; virtual; stdcall;
    procedure Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer); virtual; stdcall;
    procedure SetScriptHost(Host: IScriptHost); stdcall;
    function GetScriptHost: IScriptHost; stdcall;
    property Host: IScriptHost read GetScriptHost write SetScriptHost;
  end;

implementation

{ TDefaultScriptLibrary }


{ TScriptMethod }

procedure TScriptMethod.Execute(const Res: Pointer; const Params: array of Pointer;
  ParamCount: Integer);
begin
end;

function TScriptMethod.GetScriptHost: IScriptHost;
begin
  Result:= FHost;
end;

function TScriptMethod.IsFunction: Boolean;
begin
  Result:= False;
end;

function TScriptMethod.Name: WideString;
begin
  Result:= 'NotSet';
end;

function TScriptMethod.ParameterCount: Integer;
begin
  Result:= 0;
end;

function TScriptMethod.ParameterName(Index: Integer): WideString;
begin
  Result:= 'Parm' + IntToStr(Index);
end;

function TScriptMethod.ParameterPassing(Index: Integer): Integer;
begin
  Result:= PassAsConst;
end;

function TScriptMethod.ParameterType(Index: Integer): Integer;
begin
  Result:= dtString;
end;

procedure TScriptMethod.Reset;
begin
end;

function TScriptMethod.ReturnType: Integer;
begin
  Result:= dtString;
end;

procedure TScriptMethod.SetScriptHost(Host: IScriptHost);
begin
  FHost:= Host;
end;

end.
