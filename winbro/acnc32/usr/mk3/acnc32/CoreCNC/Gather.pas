unit Gather;
interface
{Sample period is in Seconds!}
{Must sort out callbacks on installable gather}
{see zSigEditor.pas for Synopsis of Signature Analysis}
uses
  SysUtils, Classes, CoreCNC, CNC32, CNCTypes, Curves{, zExpr};

const
  GatherStartFormat = 'Gather%dStart';
  GatherDefName = 'GatherDef';

  MaxGatherFields = 4;

type
  TAbstractGather = class;
  TGatherHead = 0..3;
  TGatherHeads = set of TGatherHead;

  TGatherField = 0..MaxGatherFields-1; {these are TCurve 'Channels'}
  TGatherFields = set of TGatherField;
  PInteger = ^Integer;

  TGatherInfo = record
    StartRecord : Integer;
    StopRecord : Integer;
  end;


  TGatherStatus = (
                    gsUndefined,
                    gsDefined,
                    gsGathering,
                    gsRingBufferCrash,
                    gsTooMuchData
  );


  TGatherFilter = class;

  TGatherCurve = class(TCurve)
  private
    FHead: TGatherHead;
    Owner: TAbstractGather;
    FFilter: array[TGatherField] of TGatherFilter;
    function GetRawSampleData(i, Ch: Integer): Integer;
  protected
    function GetSamplePeriod: Double; override;
    procedure SetSamplePeriod( Value: Double); override;
    function GetSampleData(i: Integer; Ch: Integer): Integer; override;
    function GetSampleCount: Integer; override;
    function GetChannelCount: Integer; override;
    procedure SetChannelCount( Val: Integer); override;
    function GetChannelName( Ch: Integer): String; override;
    procedure SetChannelName( Ch: Integer; const Val: String); override;
  public
    constructor Create( aOwner: TAbstractGather; aHead: TGatherHead);
    destructor Destroy; override;
    procedure InstallFilter(Ch: TGatherField; Value: TGatherFilter);
    function RemoveFilter(Ch: TGatherField): TGatherFilter;
    property Head : TGatherHead read FHead;
  end;

  {GatherFilter: General notes
    A Curve filter is an auxilliary object which may be attached to a
    GatherCurve in order to process its data. A Filter applies to only one
    channel and must be installed by using the method
    TGatherCurve.InstallFilter. Once the filter is installed then the output of
    the curve is filtered. The 'raw' data (the input to the filter) is available
    to the filter implementation though the protected method
    TGatherFilter.RawData.

    Note that at the moment there is no provision for using both the raw
    and filtered data from one 'gather channel' as input into the signature
    analysis system. This is because currently there is a one to one
    relationship between gather channels and Sig analysis input channels.
    This may well be a shortcoming which needs to be addressed in the future.
    To get around it you could gather the same address on two channels and
    filter one of them. This is a very inefficient workaround, however.

    For notes specific to the three different types of gather filter,
    see the comments following the class definitions
  }

  TGatherFilterClass = class of TGatherFilter;

  TGatherFilter = class private
    FCurve: TGatherCurve;
    FChannel: Integer;
  protected
    function FilteredOutput(i: Cardinal): Integer; virtual; abstract;
    {don't call FCurve.SampleData in here or you will crash the stack.
     Use RawData instead}
    procedure NewSamples(PrevCount: Integer); virtual;
    {called whenever a batch of samples are added to the gather...}
  public
    constructor Create; virtual;
    function ChannelName: String;
             {no more than Curve.ChannelName[Channel]}
    function RawData(i: Integer): Integer;
             {unfiltered data from curve}
    function DRawDt(i: Integer; Len: Integer): Double;
             {The derivative of RawData , averaged over Len samples.
              Undefined for i = 0 (returns 0.0)
              noisy for i < Len}
    function DRawHz(I: Integer; Len: Integer): Double;
             { Rate of change of direction of raw data }
    function D2RawDt2(i: Integer; Len1, Len2: Integer): Double;
             {The derivative of DRawdt(i, Len1) averaged over Len2 samples.
              Undefined for i = 0 (returns 0.0)
              noisy for i < (Len1 + Len2)}
    function RawDataMax: Integer;
             {from 0 to Curve.SampleCount - 1}
    function RawDataMin: Integer;
             {ditto}
    function RawMean(I: Integer; Len: Integer): Double;
             { Raw mean }
    property Channel: Integer read FChannel;
    property Curve: TGatherCurve read FCurve;
  end;

 { Notes on TGatherFilter:
    use this as base class for a curve filter if:
    (a) The filter calculation overhead is moderate and you can reasonably
        calculate the output from the raw data each time it is required.
    (b) The filter can be calculated on a 'random access' basis i.e
        FilteredOutput can be called for any value of i at random

  TGatherFilter does not store or maintain a copy of its output; it goes
  to the raw data whenever a value is required.

  Implement FilteredOutput to provide a 'random access' value for the
  filtered output. This function will be called whenever the underlying
  Curve requires a value for sample data.

  Note that when a filter is attached to a Gather Curve then the 'SampleData'
  from that curve is the <filtered> output. In order to access the raw data
  (to calculate the filter) use TGatherFilter.RawData. If you call
  Curve.SampleData to calculate a filter you will swallow your tail (infinite
  recusive loop)

  Example - You might implement a simple differentiator (TSimpleDifferentiator)
  as follows:

  function TSimpleDifferentiator.FilteredOutput(i: Integer): Integer;
  begin
    Result:= Round(DRawDt(i, 5))
  end;

  }

  PSeqDataBuf = ^TSeqDataBuf;
  TSeqDataBuf = array[0..0] of Integer;
  TSequentialFilter = class(TGatherFilter)
  private
    DataBuf: PSeqDataBuf;
    InitialisedRecords,
    AllocatedRecords: Cardinal;
    function DataPtr(i: Integer): PInteger;
    procedure ReAlloc(Records: Cardinal);
  protected
    function FilteredOutput(i: Cardinal): Integer; override;
    procedure NewSamples(PrevCount: Integer); override;
    {this function is always called in sequence from 0 to n.
    If i = 0 then restarting}
    function CalculateFilterValue(i: Integer): Integer; virtual; abstract;
  public
    constructor Create; override;
    destructor Destroy; override;
  end;

  {this class keeps a record of its output.
   use this as base class for a curve filter if:
    (a) The filter calculation overhead is severe and once calculated you
        want to hang on to it (for things like screen redraws).
        CalculateFilterValue is called once, for each record in turn, shortly
        after the raw data (maintained in Filter.Curve.RawSampleData) is
        collected.
    (b) The filter cannot be calculated on a 'random access' basis -
        each point must be calculated in time sequence from the start of
        the 'gather' (consider Max Forward position) filter).

    Note that the gather component retrieves records from the hardware in
    'bunches' (cf Win32 latency with RTC or PMAC latency) and does not normally
    take any actions 'per record'. The addition of SequentialFilter forces
    this behaviour, but only for the channels for which a filter is attached.

    When Gather.Poll gets a batch of records it calls
    Gather.NewSamples(PrevCount) This call is passed, in turn to all filters
    attached to the gather. If the filter is a TGatherFilter, the call does
    nothing. If it is a sequential filter then the filter calls
    CalculateFilterValue for all records between PrevCount and
    (Curve.SampleCount - 1), and saves the result. Subsequent calls to its
    parents GetSampleData (say to draw a curve on the screen) will return the
    saved result thus ensuring that CalculateFilterValue is not accessed at
    random.

    If, when NewSamples is called, the PrevCount is greater than the present
    count, then the filter recalculates its output starting from the beginning.
    Implementors of Sequential filters must appreciate that if
    CalculateFilterValue is called with a value of 0, then this is a new scan
    and any state variables should be initialised.
  }


  {this class duplicates a lot of the stuff in TCurve, but there is no harm}
  TAbstractGather = class(TCNC) {(TCurve)}
  private
    FCurve: array[TGatherHead] of TGatherCurve;
    FHeads: TGatherHeads;
    FHeadCount: Integer;    {updated by SetHeads - needed for efficiency}
    FHoleProgram: array[TGatherHead] of Integer;
    GatProgram_IH : array[TGatherHead] of TAbstractTag;

    procedure SetHeads( aHeads: TGatherHeads);
    function GetCurve( Head: TGatherHead): TGatherCurve;
    function GetHoleProgram(Head: TGatherHead): Integer;
//    function GetTotalLength: Double;
    function ValidChannel(Ch: Integer): Boolean;
  protected
    FChannelName: array[TGatherField] of string;
    FScaling : array[TGatherField] of Double;
    FUnits : array [TGatherField] of string;
    FChannelCount: Integer;
    FHeadsGathering : TGatherHeads; // Used by decendants
    FGatherStatus : TGatherStatus;
    Start : array [TGatherHead] of TAbstractTag;
    HeadGathering : TGatherHeads;
    FAllowableFrequencies : TStringList;
    function GetSamplePeriod: Double; virtual; abstract;
    procedure SetSamplePeriod( Value: Double); virtual; abstract;
    function GetSampleCount(aHead : Integer) : Integer; virtual; abstract;

    function GetHeadData(i: Integer; Ch: Integer; Head: TGatherHead): Integer; virtual; abstract;
    {remember that HeadGatherCurve calls back to this procedure to retrieve data....}
    function GetChannelCount: Integer; //override;
    procedure SetChannelCount( Val: Integer); //override;
    function GetChannelName( Ch: Integer): string; //override;
    procedure SetChannelName( Ch: Integer; const Val: string); //override;
    function GetUnits(Ch : Integer) : string;
    procedure SetUnits(Ch : Integer; U : string);
    function GetScaling(Ch : Integer) : Double;
    procedure SetScaling(Ch : Integer; S : Double);

    {define source data precise meaning of 'address' may vary}
    procedure DefineGather; virtual; abstract; {define configuration and allocate resources associated with Gathering}
    procedure DeleteGather; virtual; abstract; {release resources}
    procedure StartGather(aHead : Integer); virtual; abstract;  {cannot start until after define}
    procedure StopGather(aHead : Integer); virtual; abstract;
    procedure NewSamples(PrevCount: Integer);
    function GetSamplePeriodIndex(Index : Integer) : Double; virtual; abstract;
    function GetIndexOfSamplePeriod(Index : Double) : Integer; virtual; abstract;
  public
    CurrentFaultLevel: TFaultLevel;            {reset to faultNone on StartCallback}
    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;
    procedure InstallComponent(Params : TParamStrings); override;
    procedure Installed; override;
    procedure IShutdown; override;
    procedure StartCallback(aTag : TAbstractTag); virtual;
    procedure DefCallback(aTag : TAbstractTag);
    procedure Poll;  virtual; abstract;
    class procedure RegisterFilterClass(FC : TGatherFilterClass);
    class function FindFilterClass(aName : string) : TGatherFilterClass;
    class procedure CurrentFilterList(List : TStrings);

    property Heads: TGatherHeads read FHeads write SetHeads;
    property HeadCount: Integer read FHeadCount;
    property GatherStatus : TGatherStatus read FGatherStatus;
    property Curve[Head: TGatherHead]: TGatherCurve read GetCurve; default;

    property HoleProgram[Head: TGatherHead]: Integer read GetHoleProgram;
    property SampleCount [Index : Integer] : Integer read GetSampleCount;
    property SamplePeriod: Double read GetSamplePeriod write SetSamplePeriod;

    property HeadData[i, Ch: Integer; Head: TGatherHead]: Integer read GetHeadData;
//    property TotalLength: Double read GetTotalLength;
    property ChannelName[Ch: Integer]: string read GetChannelName write SetChannelName;
    property Units[Ch : Integer] : string read GetUnits write SetUnits;
    property Scaling[Ch : Integer] : Double read GetScaling write SetScaling;
    property ChannelCount: Integer read GetChannelCount write SetChannelCount;
    property HeadsGathering : TGatherHeads read FHeadsGathering;
    property AllowableFrequencies : TStringList read FAllowableFrequencies;
    property SamplePeriodIndex [Index : Integer] : Double read GetSamplePeriodIndex;
    property IndexOfSamplePeriod [Index : Double] : Integer read GetIndexOfSamplePeriod;
  published
  end;

  TVelocityFilter_05 = class(TGatherFilter)
  protected
    function FilteredOutput(i: Cardinal): Integer; override;
  end;

  TVelocityFilter_10 = class(TGatherFilter)
  protected
    function FilteredOutput(i: Cardinal): Integer; override;
  end;

  TVelocityFilter_20 = class(TGatherFilter)
  protected
    function FilteredOutput(i: Cardinal): Integer; override;
  end;

  TFrequencyFilter_20 = class(TGatherFilter)
  protected
    function FilteredOutput(i: Cardinal): Integer; override;
  end;

  TFrequencyFilter_50 = class(TGatherFilter)
  protected
    function FilteredOutput(i: Cardinal): Integer; override;
  end;

  TFrequencyFilter_100 = class(TGatherFilter)
  protected
    function FilteredOutput(i: Cardinal): Integer; override;
  end;

  TMeanFilter_40 = class(TGatherFilter)
  protected
    function FilteredOutput(i: Cardinal): Integer; override;
  end;

  TMeanFilter_20 = class(TGatherFilter)
  protected
    function FilteredOutput(i: Cardinal): Integer; override;
  end;

  TMeanFilter_10 = class(TGatherFilter)
  protected
    function FilteredOutput(i: Cardinal): Integer; override;
  end;

  TMeanFilter_2 = class(TGatherFilter)
  protected
    function FilteredOutput(i: Cardinal): Integer; override;
  end;

  TMeanFilter_3 = class(TGatherFilter)
  protected
    function FilteredOutput(i: Cardinal): Integer; override;
  end;

  TMeanFilter_4 = class(TGatherFilter)
  protected
    function FilteredOutput(i: Cardinal): Integer; override;
  end;

  TMeanFilter_5 = class(TGatherFilter)
  protected
    function FilteredOutput(i: Cardinal): Integer; override;
  end;


{  TForwardVelocityFilter = class(TSequentialFilter)
  protected
    MaxValue: Integer;
    PrevList : TList;
    function CalculateFilterValue(i: Integer): Integer; override;
  public
    constructor Create; override;
    destructor Destroy; override;
  end;

  TZeroBasedFilter = class(TGatherFilter)
  protected
    function FilteredOutput(i: Cardinal): Integer; override;
  end;

  T16BitFilter = class(TGatherFilter)
  protected
    function FilteredOutput(i: Cardinal): Integer; override;
  end;

  TUpper16BitFilter = class(TGatherFilter)
  protected
    function FilteredOutput(I : Cardinal): Integer; override;
  end;

  TMaxValueFilter = class(TSequentialFilter)
  protected
    MaxValue: Integer;
    function CalculateFilterValue(i: Integer): Integer; override;
  end;

  TZeroBasedMaxValueFilter = class(TMaxValueFilter)
  protected
    function CalculateFilterValue(i: Integer): Integer; override;
  end; }

  EGatherError = class(Exception);

const
  GatherErrors = [gsRingBufferCrash..High(TGatherStatus)];
  EDMHeadPrefix = 'Head';

{notes on program ID:
  There may be problems in this area:
  TAbstractGather.HoleProgram[Head] should return the program to which
  the currently held data relates. This property is set by
  TAbstractGather.StartCallback, which reads 'HeadNProgram' from the
  'Memory'. This should be the current hole program at the moment that
  the gather is started.

  It is important, therefore, that the code a program is selected before the
  gather is started.

  See also notes in Signature.pas called 'LiveData compatibility'

  A Filter could be implemented by TGatherCurve by overwriting
  TGatherCurve.GetSampleData;
  }

resourcestring
  ParamBadAddressFormat = '%s: Bad Address format';

implementation
uses
  TypInfo, {EDMHead, }Windows;


const
  DefaultFieldNames = 'Position;Velocity;Voltage';
  DefaultFilterNames = '';
  FieldNamesName = 'Fields';
  FilterNamesName = 'Filters';
  SamplePeriodName = 'SamplePeriod';
  SamplePeriodDefault = 0.10;  {100 ms}
  AddressName = 'Head%dSource';
  AddressDefault = 0;
  PollIntervalName = 'PollInterval';
  PollIntervalDefault = 250; {ms}

var
  FilterList : TStringList;

class procedure TAbstractGather.RegisterFilterClass(FC : TGatherFilterClass);
var I : Integer;
begin
  if not Assigned(FilterList) then
    FilterList := TStringList.Create;

  I := FilterList.IndexOf(FC.ClassName);
  if I = -1 then
    FilterList.AddObject(FC.ClassName, Pointer(FC));
end;

class function TAbstractGather.FindFilterClass(aName : string) : TGatherFilterClass;
var I : Integer;
begin
  if not Assigned(FilterList) then
    raise ECNCInstallFault.Create('Filters have not been installed');

  I := FilterList.IndexOf(aName);
  if I <> -1 then
    Result := TGatherFilterClass(FilterList.Objects[I])
  else
    Result := nil;
end;

class procedure TAbstractGather.CurrentFilterList(List : TStrings);
begin
  List.Clear;
  List.AddObject('None', nil);
  List.AddStrings(FilterList);
end;

{function TAbstractGather.GetTotalLength: Double;
begin
  if SampleCount < 2 then
    Result:= 0
  else
    Result:= (SampleCount - 1)*SamplePeriod
end; }

function TGatherCurve.GetSamplePeriod: Double;
begin
  Result:= Owner.SamplePeriod
end;

procedure TGatherCurve.SetSamplePeriod( Value: Double);
begin  {Set owner's sample period: this will effect all heads}
  Owner.SamplePeriod:= Value
end;

constructor TGatherFilter.Create;
begin
  inherited Create;
end;

function TGatherFilter.RawData(i: Integer): Integer;
begin
  Result:= FCurve.GetRawSampleData(i, FChannel)
end;

procedure TGatherFilter.NewSamples(PrevCount: Integer);
begin
end;

function TGatherFilter.ChannelName: String;
begin
  Result:= Curve.ChannelName[Channel]
end;


function TGatherFilter.DRawdt(i: Integer; Len: Integer): Double;
{len = 0 is more properly an error}
begin
  if (i > 0) and (Len > 0) then begin
    if i < Len then
      Len:= i;
    Result:= (RawData(i) - RawData(i-Len))/(Len*Curve.SamplePeriod)
  end else begin {actually undefined...}
    Result:= 0.0
  end
end;

function TGatherFilter.RawMean(I: Integer; Len: Integer): Double;
var C: Integer;
    Sum: Integer;
begin
  if (i > 0) and (Len > 0) then begin
    if i < Len then
      Len:= i;
    Sum:= 0;

    for C:= I - Len + 1 to I do begin
      Sum:= Sum + RawData(C);
    end;

    Result:= Sum / Len;
  end else begin
    Result:= RawData(I);
  end
end;

function TGatherFilter.DRawHz(I: Integer; Len: Integer): Double;
var Count: Integer;
    Up: Boolean;
    Capture, Tmp: Integer;
begin
  Up:= True;
  Capture:= RawData(I - Len);
  Result:= 0;
  if I >= Len then begin //Curve.SampleCount
    for Count:=I - Len to I do begin
      Tmp:= RawData(Count);
      if Up then begin
        if Tmp < Capture then begin
          Up:= False;
          Result:= Result + 1;
        end;
      end else begin
        if Tmp > Capture then begin
          Up:= True;
        end;
      end;
    end;
    Result := Result / (Len*Curve.SamplePeriod);
  end;
end;

function TGatherFilter.D2RawDt2(i: Integer; Len1, Len2: Integer): Double;
{there is certainly a more efficient way of doing this...}
begin
  if (i > 0) and (Len2 > 0) then
  begin
    if i < Len2 then Len2:= i;
    Result:= (DRawDt(i, Len1) - DRawDt(i-Len2, Len1))/(Len2*Curve.SamplePeriod)
  end else
  begin {actually undefined...}
    Result:= 0.0
  end
end;

function TGatherFilter.RawDataMax: Integer;
var
  i, rd: Integer;
begin
  if Curve.SampleCount = 0 then
  begin
    Result:= 0
  end else
  begin
    Result:= RawData(0);
    for i:= 0 to Curve.SampleCount - 1 do
    begin
      rd:= RawData(i);
      if rd > Result then
        Result:= rd
    end
  end
end;

function TGatherFilter.RawDataMin: Integer;
var
  i, rd: Integer;
begin
  if Curve.SampleCount = 0 then
  begin
    Result:= 0
  end else
  begin
    Result:= RawData(0);
    for i:= 0 to Curve.SampleCount - 1 do
    begin
      rd:= RawData(i);
      if rd < Result then
        Result:= rd
    end
  end
end;

function TGatherCurve.GetSampleData(i: Integer; Ch: Integer): Integer;
begin
  if Assigned(FFilter[Ch]) then
    Result:= FFilter[Ch].FilteredOutput(i)
  else
    Result:= GetRawSampleData(i, Ch)
end;

function TGatherCurve.GetRawSampleData(i, Ch: Integer): Integer;
begin
  Result:= Owner.GetHeadData(i, Ch, Head)
end;

procedure TGatherCurve.InstallFilter(Ch: TGatherField; Value: TGatherFilter);
begin
  if not ValidChannel(Ch) then
    raise EGatherError.CreateFmt('%d is invalid filter channel', [Ch]);
  FFilter[Ch].Free;
  FFilter[Ch]:= Value;
  if Assigned(Value) then
  begin
    Value.FCurve:= Self;
    Value.FChannel:= Ch
  end
end;

function TGatherCurve.RemoveFilter(Ch: TGatherField): TGatherFilter;
begin
  Result:= FFilter[Ch];
  FFilter[Ch]:= nil
end;

function TGatherCurve.GetSampleCount: Integer;
begin
  Result:= Owner.SampleCount[Head];
end;

function TGatherCurve.GetChannelCount: Integer;
begin
  Result:= Owner.ChannelCount
end;

procedure TGatherCurve.SetChannelCount( Val: Integer);
begin
  Owner.ChannelCount:= Val
end;

function TGatherCurve.GetChannelName( Ch: Integer): String;
begin
  Result:= Owner.ChannelName[Ch]
end;

procedure TGatherCurve.SetChannelName( Ch: Integer; const Val: String);
begin
  Owner.ChannelName[Ch]:= Val
end;

constructor TGatherCurve.Create( aOwner: TAbstractGather; aHead: TGatherHead);
begin
  inherited Create;
  Owner:= aOwner;
  FHead:= aHead
end;

destructor TGatherCurve.Destroy;
var
  i: TGatherField;
begin
  for i:= Low(TGatherField) to High(TGatherField) do
    FFilter[i].Free;
  inherited Destroy
end;

procedure TAbstractGather.NewSamples(PrevCount: Integer);
var
  i: TGatherHead;
  j: TGatherField;
begin
  for i:= Low(TGatherHead) to High(TGatherHead) do
    if Assigned(FCurve[i]) then
      for j:= Low(TGatherField) to High(TGatherField) do
        if Assigned(FCurve[i].FFilter[j]) then
          FCurve[i].FFilter[j].NewSamples(PrevCount)
end;

function TAbstractGather.GetHoleProgram(Head: TGatherHead): Integer;
begin
  Result:= FHoleProgram[Head]
end;

constructor TAbstractGather.Create(aOwner: TComponent);
var
  I : Integer;
begin
  inherited Create(aOwner);
  for I := Low(TGatherHead) to High(TGatherHead) do
    FHoleProgram[i]:= -1;

  for I := Low(TGatherField) to High(TGatherField) do
    Scaling[I] := 1;

  FAllowableFrequencies := TStringList.Create;;
end;

procedure TAbstractGather.SetHeads(aHeads: TGatherHeads);
var
  k: TGatherHead;
begin
  FHeadCount:= 0;
  FHeads:= aHeads;
  for k:= Low(TGatherHead) to High(TGatherHead) do begin
    FCurve[k].Free;
    if k in aHeads then begin
      FCurve[k]:= TGatherCurve.Create(Self, k);
      Inc(FHeadCount);
    end else begin
      FCurve[k]:= nil
    end
  end
end;

function TAbstractGather.GetChannelCount: Integer;
begin
  Result:= FChannelCount
end;

procedure TAbstractGather.SetChannelCount( Val: Integer);
begin
  if Val > (High(TGatherField) + 1) then
    Val:=  (High(TGatherField) + 1);
  FChannelCount:= Val
end;

function TAbstractGather.ValidChannel(Ch: Integer): Boolean;
begin
  Result:= (Ch >= 0) and (Ch < ChannelCount)
end;

function TAbstractGather.GetUnits(Ch : Integer) : string;
begin
  if ValidChannel(Ch) then
    Result := FUnits[Ch]
  else
    Result := 'INVALID';
end;

function TAbstractGather.GetScaling(Ch : Integer) : Double;
begin
  if ValidChannel(Ch) then
    Result := FScaling[Ch]
  else
    Result := 1;
end;

procedure TAbstractGather.SetUnits(Ch : Integer; U : string);
begin
  if ValidChannel(Ch) then
    FUnits[Ch] := U;
end;

procedure TAbstractGather.SetScaling(Ch : Integer; S : Double);
begin
  if ValidChannel(Ch) then
    FScaling[Ch] := S;
end;

function TAbstractGather.GetChannelName( Ch: Integer): string;
begin
  if ValidChannel(Ch) then
    Result:= FChannelName[Ch]
end;

procedure TAbstractGather.SetChannelName( Ch: Integer; const Val: string);
begin
  if ValidChannel(Ch) then begin
    FChannelName[Ch]:= Val;
  end
end;

function TAbstractGather.GetCurve( Head: TGatherHead): TGatherCurve;
begin
  Result:= FCurve[Head]
end;

procedure TAbstractGather.InstallComponent(Params : TParamStrings);
begin
  inherited InstallComponent(Params);

  SamplePeriod:= Params.ReadDouble(SamplePeriodName, SamplePeriodDefault);
end;

procedure TAbstractGather.Installed;
var I : Integer;
begin
  inherited Installed;
  for I := 0 to SysObj.Heads - 1 do begin
    Start[I] := TagEvent(Format(GatherStartFormat, [I+1]), edgeChange, TMethodTag, StartCallback);
    Include(FHeads, I);

    GatProgram_IH[i] := TagEvent(
     Format('%s%dProgram', [EDMHeadPrefix, i + 1]),
          edgeChange, TIntegerTag, nil);
  end;
  TagEvent(GatherDefName, edgeRising, TMethodTag, DefCallback);
end;

procedure TAbstractGather.StartCallback(aTag : TAbstractTag);
  function HeadFromTag : TGatherHead;
  begin
    for Result := 0 to SysObj.Heads - 1 do begin
      if IsDualTag(aTag, Start[Result]) then
        Exit;
    end;
    Result := 0;
  end;
var
  Head : TGatherHead;
begin
  Head := HeadFromTag;

  if aTag.AsBoolean then begin
    if GatherStatus = gsUndefined then begin
      DefCallback(aTag)
    end;
    FHoleProgram[Head]:= GatProgram_IH[Head].AsInteger;
    CurrentFaultLevel:= faultNone;
    StartGather(Head);
  end else begin
    StopGather(Head);
  end;
end;

procedure TAbstractGather.DefCallback(aTag : TAbstractTag);
begin
  DefineGather;
end;

procedure TAbstractGather.IShutdown;
var I : Integer;
begin
  if GatherStatus = gsGathering then
    for I := 0 to SysObj.Heads - 1 do
      StopGather(I);
  if GatherStatus = gsDefined then
    DeleteGather;
end;

destructor TAbstractGather.Destroy;
var
  i: TGatherHead;
begin
  for i:= Low(TGatherHead) to High(TGatherHead) do
  begin
    FCurve[i].Free;
    FCurve[i]:= nil
  end;
  inherited Destroy
end;

const
  AllocDelta = $100;  {records}

function TSequentialFilter.DataPtr(i: Integer): PInteger;
begin
  Result:= @DataBuf^[i]
end;

function TSequentialFilter.FilteredOutput(i: Cardinal): Integer;
begin
  if (i < InitialisedRecords) then
    Result:= DataPtr(i)^
  else
    Result:= 0
end;

procedure TSequentialFilter.NewSamples(PrevCount: Integer);
var
  i, Start: Integer;
begin
  ReAlloc(Curve.SampleCount);
  if Curve.SampleCount < PrevCount then {start from the beginning}
    Start:= 0
  else
    Start:= PrevCount;
  for i:= Start to Curve.SampleCount - 1 do
    DataPtr(i)^:= CalculateFilterValue(i);
  InitialisedRecords:= Curve.SampleCount;
end;

procedure TSequentialFilter.ReAlloc(Records: Cardinal);
var
  i: Cardinal;
begin
  i:= (Records div AllocDelta + 1)*AllocDelta;
  if i  <> AllocatedRecords then
  begin
    ReallocMem(DataBuf, i * SizeOf(Integer));
    AllocatedRecords:= i;
  end
end;

constructor TSequentialFilter.Create;
begin
  inherited Create;
  ReAlloc(0)
end;

destructor TSequentialFilter.Destroy;
begin
  FreeMem(DataBuf);
  inherited Destroy
end;



const SampleLength = 5;

function TVelocityFilter_05.FilteredOutput(i: Cardinal): Integer;
begin
  Result:= Round(DRawDt(i, 5))
end;

function TVelocityFilter_10.FilteredOutput(i: Cardinal): Integer;
begin
  Result:= Round(DRawDt(i, 10))
end;

function TVelocityFilter_20.FilteredOutput(i: Cardinal): Integer;
begin
  Result:= Round(DRawDt(i, 20))
end;

function TMeanFilter_40.FilteredOutput(i: Cardinal): Integer;
begin
  Result:= Round(RawMean(i, 40));
end;

function TMeanFilter_20.FilteredOutput(i: Cardinal): Integer;
begin
  Result:= Round(RawMean(i, 20));
end;

function TMeanFilter_10.FilteredOutput(i: Cardinal): Integer;
begin
  Result:= Round(RawMean(i, 10));
end;

function TMeanFilter_2.FilteredOutput(i: Cardinal): Integer;
begin
  Result:= Round(RawMean(i, 2));
end;

function TMeanFilter_3.FilteredOutput(i: Cardinal): Integer;
begin
  Result:= Round(RawMean(i, 3));
end;

function TMeanFilter_4.FilteredOutput(i: Cardinal): Integer;
begin
  Result:= Round(RawMean(i, 4));
end;

function TMeanFilter_5.FilteredOutput(i: Cardinal): Integer;
begin
  Result:= Round(RawMean(i, 5));
end;


{ TFrequencyFilter20 }

function TFrequencyFilter_20.FilteredOutput(i: Cardinal): Integer;
begin
  Result:= Round(DRawHz(i, 20))
end;

function TFrequencyFilter_50.FilteredOutput(i: Cardinal): Integer;
begin
  Result:= Round(DRawHz(i, 50))
end;

function TFrequencyFilter_100.FilteredOutput(i: Cardinal): Integer;
begin
  Result:= Round(DRawHz(i, 100))
end;

{

function TMaxValueFilter.CalculateFilterValue(i: Integer): Integer;
begin
  if i = 0 then
    MaxValue:= RawData(i)
  else
  if RawData(i) > MaxValue then
     MaxValue:= RawData(i);
  Result:= MaxValue
end;

function TZeroBasedMaxValueFilter.CalculateFilterValue(i: Integer): Integer;
begin
  Result := inherited CalculateFilterValue(I) - RawData(0);
end;


constructor TForwardVelocityFilter.Create;
begin
  inherited Create;
  PreVList := TList.Create;
end;

destructor TForwardVelocityFilter.Destroy;
begin
  PrevList.Free;
  inherited;
end;

function TForwardVelocityFilter.CalculateFilterValue(i: Integer): Integer;
  procedure AppendPrev;
  begin
    // while the sample list is too big, delete the first sample
    while PrevList.Count >=SampleLength do
      PrevList.Delete(0);
    PrevList.Add(Pointer(MaxValue));
  end;

begin
  Result := 0;
  if i = 0 then begin
    MaxValue:= RawData(0);
    PrevList.Clear;
  end else begin
    if RawData(i) > MaxValue then begin
      if PrevList.Count >= SampleLength then begin
        MaxValue:= RawData(I);
        Result:= Round( (MaxValue - Integer(PrevList[0])) /((SampleLength+1)*Curve.SamplePeriod));
      end;
    end;
  end;
  AppendPrev;
end;


function TZeroBasedFilter.FilteredOutput(i: Cardinal): Integer;
begin
  Result := RawData(I) - RawData(0);
end;

function TUpper16BitFilter.FilteredOutput(I : Cardinal) : Integer;
begin
  Result := RawData(I) shr 8;
end;

function T16BitFilter.FilteredOutput(i: Cardinal): Integer;
begin
  Result := SmallInt(RawData(I));
end;
 }

{ TFrequencyFilter }


initialization
  TAbstractGather.RegisterFilterClass(TVelocityFilter_05);
  TAbstractGather.RegisterFilterClass(TVelocityFilter_10);
  TAbstractGather.RegisterFilterClass(TVelocityFilter_20);
  TAbstractGather.RegisterFilterClass(TFrequencyFilter_20);
  TAbstractGather.RegisterFilterClass(TFrequencyFilter_50);
  TAbstractGather.RegisterFilterClass(TFrequencyFilter_100);
  TAbstractGather.RegisterFilterClass(TMeanFilter_2);
  TAbstractGather.RegisterFilterClass(TMeanFilter_3);
  TAbstractGather.RegisterFilterClass(TMeanFilter_4);
  TAbstractGather.RegisterFilterClass(TMeanFilter_5);
  TAbstractGather.RegisterFilterClass(TMeanFilter_10);
  TAbstractGather.RegisterFilterClass(TMeanFilter_20);
  TAbstractGather.RegisterFilterClass(TMeanFilter_40);
//  TAbstractGather.RegisterFilterClass(TZeroBasedFilter);
//  TAbstractGather.RegisterFilterClass(T16BitFilter);
//  TAbstractGather.RegisterFilterClass(TMaxValueFilter);
//  TAbstractGather.RegisterFilterClass(TZeroBasedMaxValueFilter);
//  TAbstractGather.RegisterFilterClass(TForwardVelocityFilter);
//  TAbstractGather.RegisterFilterClass(TUpper16BitFilter);
end.
