unit CNC32;

interface

uses SysUtils, Classes, CNCTypes, ExtCtrls, Windows, TypInfo, StreamTokenizer, Graphics,
     Registry, Math, Controls, IFSDServer, INamedInterface, CNCRegistry, SyncObjs,
     IniFiles;

resourceString
  ModuleNameNotFound = 'Device name not found [%s]';
  ModuleNumberInvalid = 'Device number not found [%s]';
  InvalidBaseAddressFormat = 'Invalid base address format in %s';
  InvalidFaultListIndex = 'Invalid Fault List Index';

  ImpossibleException     = 'INVALID SYSTEM STATE [%s]';
  InputLang               = 'Input';
  OutputLang              = 'Output';
  IOSubSystemLang         = 'IO Sub System';
  OutputBaseLang          = 'Output Base';
  InputBaseLang           = 'Input Base';
  OutputSizeLang          = 'Output Size';
  InputSizeLang           = 'Input Size';

const
  RegistryBase = '\Software\Amchem\ACNC';

type
  TAbstractTag = class;
  TCallback         = procedure (aTag : TAbstractTag) of Object;
  TfaultResetMethod = function  (Fid : FHandle) : boolean of Object;
                      {returns true if it is ok to delete the fault}
  TNudgeEvent       = procedure (change : TEditChange) of Object;
  TKeyBoardEvent    = procedure (key : word) of Object;
  TAssertMethod     = procedure (sender : TObject; text : string) of Object;
  TInterlockFault   = procedure (Text : string) of Object;
  TResetComplete    = function  (Sender : TObject) : Boolean of Object;
  TTagHandle        = Integer;
  TSecurityCheck = function(aPassword : string) : Boolean of Object;
  TOPCSubscriptionEvent = procedure (const Value: OleVariant; Quality: Word) of object;

  PCallbackRecord = ^TCallbackRecord;
  TCallbackRecord = record
    Callback : TCallback;
    Edge     : TedgeType;              { Used for edge detection on callback }
    Hwnd     : HWND;                   { Used for thread switching }
  end;

  PTEventLL = ^TEventLL;
  TEventLL = record                    { Cludge in creating multi-threaded IO }
    Callback : TCallback;
    Tag      : TAbstractTag;
    next     : PTEventLL;
  end;

//  PDataStreamRecord = ^TDataStreamRecord;
  THTTPDataStream = class(TObject)
    Name : string;
    LastData : string;
    Callback : IHTTPServer;
    Stream : THandle;
    Data : Pointer;
    DT : Integer;
    LastTick : Int64;
    Dead : Boolean;
  end;

  TXYZ = record
    X, Y, Z : Double;
  end;

  TCompRec = packed record
  case Byte of
    0: (C: Comp);
    1: (L0: Longint);
    2: (R0, R1, R2: Byte;
        L1: Longint);
    3: (B: array[0..7] of Byte);
    4: (IA : array[0..1] of Integer);
  end;

  { This is the callback storage for the new Multi-threaded event model }
  TIOEventList = class(TObject)
  private
    Events : PTEventLL;
  public
    constructor Create;
    destructor  Destroy; override;
    procedure Add(aTag : TAbstractTag; callback : TCallback);
    function  Get(aTag : TAbstractTag) : TCallback;
  end;

  PInterlock = ^TInterlock;
  TInterlock     = record
    Active       : boolean;            // Enable interlock
    Exclusion    : TCNCStatusTypes;    // Must not be any of these Status's
    Inclusion    : TCNCStatusTypes;    // Must be one of these Status's
    Modes        : TCNCModes;          // Must be one of these Modes
    Access       : TAccessLevels;       // Must be one of these AccessLevels
  end;

  TTagName = string[MaxNamedLength];

  TClientLink = 0..15;
  TClientLinks = set of TClientLink;

  TNamedLayerType = (
    nltVCL,       // Does not require Thread crossing to VCL
    nltSystem,    // Tag belongs to communication system
    nltWritable,  // Value is assignable. In IO System Input/Output switch
    nltHasValue,  // Tag is readable : All tags are readable but this indicates
                  // the process is pointless.
    nltIO,        // Tag is connected through the IO layer
    nltConstant,  // Tag is Invariable
    nltInterlock, // Tag has Interlock
    nltOwned,     // Used by ACNCScript
    nltConnected, // IO Tag is translated to device
    nltEvented,   // Generate Events when Refreshed
    nltPersistent,// Stored through power-off
    nltConvert    // Tag is unit converted. (inch mm switching)
  );

  TNamedLayerTypes = set of TNamedLayerType;

  TTagOwner = class(TCustomPanel)
  public
  property Font;
  end;

  TTagClass = class of TAbstractTag;
  TAbstractTag = class(TObject)
  private
    FName : string;
    FNetworkAlias : string;
    FOwner : TTagOwner;
    FInterlock : PInterlock;
    //FOPCCallback : TOPCSubscriptionEvent;
    function GetVCL : Boolean;
    function GetSystem : Boolean;
    function GetWritable : Boolean;
    function GetHasValue : Boolean;
    function GetConstant : Boolean;
    function GetInterlock : Boolean;
    function GetOwned : Boolean;
    function GetConnected : Boolean;
    function GetEvented : Boolean;
    function GetIO : Boolean;
    function GetPersistent : Boolean;
    function GetConvert : Boolean;
    procedure SetVCL(aVCL : Boolean);
    procedure SetSystem(aSystem : Boolean);
    procedure SetWritable(aWritable : Boolean);
    procedure SetHasValue(aHasValue : Boolean);
    procedure SetConstant(aConstant : Boolean);
    procedure SetOwned(aOwned : Boolean);
    procedure SetConnected(aConnected : Boolean);
    procedure SetEvented(aEvented : Boolean);
    procedure SetIO(aIO : Boolean);
    procedure SetPersistent(aPersistent : Boolean);
    procedure SetConvert(aConvert : Boolean);
    function GetCallback(Index : Integer) : PCallbackRecord;
    function GetCount : Integer;
    function GetItems(Index : Integer) : Pointer;
    procedure SetItems(Index : Integer; aItem : Pointer);
    procedure SetInterlock(aInterlock : PInterlock);
  protected
    FCallbacks : TList;
    function GetDataSize : Integer; virtual; abstract;
    function GetDataPtr : Pointer; virtual; abstract;
    function GetIOSize : Integer; virtual; abstract;

    function GetAsString : string; virtual; abstract;
    function GetAsDouble : Double; virtual; abstract;
    function GetAsInteger : Integer; virtual; abstract;
    function GetAsBoolean : Boolean; virtual; abstract;
    function GetAsShortString : string; virtual;
    function GetAsOleVariant : OleVariant; virtual; abstract;

    function GetInfoName: string; virtual;

    procedure SetAsString(const aString : string); virtual; abstract;
    procedure SetAsDouble(aDouble : Double); virtual; abstract;
    procedure SetAsInteger(aInteger : Integer); virtual; abstract;
    procedure SetAsBoolean(aBoolean : Boolean); virtual; abstract;
    procedure SetAsOleVariant(const V : OleVariant); virtual; abstract;
    function GetAddress : Integer; virtual;
    procedure SetAddress(aAddress : Integer); virtual;
  public
    ClientLinks : TClientLinks;
    NamedType : TNamedLayerTypes;
    LoggingPermissive : Boolean;

    constructor Create(aOwner : TTagOwner; aName : string); virtual;
    procedure AddCallback(CallBack : Tcallback; edge : TEdgeType; hw : Hwnd);
    procedure RemoveCallback(CallBack : Tcallback);
    function  OK : Boolean; virtual;
    destructor Destroy; override;
    property Name : string read FName;
    property Callback [Index : Integer] : PCallbackRecord read GetCallback;

    property HasValue : Boolean read GetHasValue write SetHasValue;
    property IsConstant : Boolean read GetConstant write SetConstant;
    property HasInterlock : Boolean read GetInterlock;
    property DataSize : Integer read GetDataSize;
    property DataPtr : Pointer read GetDataPtr;
    property IsVCL : Boolean read GetVCL write SetVCL;
    property IsSystem : Boolean read GetSystem write SetSystem;
    property IsWritable : Boolean read GetWritable write SetWritable;
    property IsOwned : Boolean read GetOwned write SetOwned;
    property IsConnected : Boolean read GetConnected write SetConnected;
    property IsEvented : Boolean read GetEvented write SetEvented;
    property IsIO : Boolean read GetIO write SetIO;
    property IsPersistent : Boolean read GetPersistent write SetPersistent;
    procedure Refresh; virtual; abstract;
    procedure CopyCallbacksFrom(aTag : TAbstractTag);
    procedure Assign(aTag : TAbstractTag); virtual; abstract;
    procedure ReadFromBuf(var Buf; aLength : Integer); virtual;
    property Owner : TTagOwner read FOwner write FOwner;
    property Size : Integer read GetIOSize;
    property Interlock : PInterlock read FInterlock write SetInterlock;
    property InfoName: string read GetInfoName;
    property Count : Integer read GetCount;
    property Callbacks [Index : Integer] : Pointer read GetItems write SetItems; default;
    class function TagCount : Integer;

    property AsInteger : Integer read GetAsInteger write SetAsInteger;
    property AsBoolean : Boolean read GetAsBoolean write SetAsBoolean;
    property AsDouble : Double read GetAsDouble write SetAsDouble;
    property AsString : string read GetAsString write SetAsString;
    property AsShortString : string read GetAsShortString;
    property AsOleVariant : OleVariant read GetAsOleVariant write SetAsOleVariant;
    property Convert : Boolean read GetConvert write SetConvert;

    property Address : Integer read GetAddress write SetAddress;
    procedure ReadAccessFromRegistry;
    procedure WriteAccessToRegistry(const aInterlock : TInterlock);
    procedure ClearAccessFromRegistry;
    procedure ReadFromRegistry(Reg : TCNCRegistry);
    procedure WriteToRegistry(Reg : TCNCRegistry);
    //property OPCCallback : TOPCSubscriptionEvent read  FOPCCallback write FOPCCallback;
    property NetWorkAlias : string read FNetWorkAlias write FNetWorkAlias;
  end;

  TOPCTagRecord = class
    TagName : string;
    NetAlias : string;
    Tag : TAbstractTag;
    TagClass : TTagClass;
    Input : Boolean;
  end;


  TTagList = class(TStringList)
  private
    function GetTag(I : Integer) : TAbstractTag;
    procedure SetTag(I : Integer; Tag : TAbstractTag);
  public
    constructor Create;
    property Tag[I : Integer] : TAbstractTag read GetTag write SetTag;
    function AddTag(aTag : TAbstractTag) : TTagHandle;
  end;

  TIOAddress      = word;

  PIOAddressPair = ^TIOAddressPair;
  TIOAddressPair = packed record
    AddrIn, InSize,
    AddrOut, OutSize: Word;
  end;


  TFaultRegistration = record
    Ndx  : Integer;
    Text : string;
    Level : TFaultLevel;
    Dispatch : T32Bits;
    Help : string;
  end;

  PFaultRegistration = ^TFaultRegistration;
  AFaultRegistration = array [0..0] of TFaultRegistration;
  PAFaultRegistration = ^AFaultRegistration;

  // Used for storing active faults
  TFaultRecord = class(TObject)
    Reg : PFaultRegistration;
    Text : string;
    Owner : TComponent;
    Level : TFaultLevel;
    FaultH : FHandle;
    Reset : TFaultResetMethod;
    ResetStdCall : TFaultResetStdCall;
    constructor Create;
    destructor Destroy; override;
  end;


  TNamedRegistration = record
    tag    : String;
    size   : TTagClass;
    edge   : TEdgeType;
  end;

  PAIOMap         = ^AIOMap;
  AIOMap          = array [0..8191] of byte;

  TIOMessage      = packed record
    Msg    : Cardinal;
    case Byte of
      0: (edge   : TEdgeType;
          pad    : byte;
          Tag    : TAbstractTag);
      1: (WParam: Longint;
          LParam: Longint;
          Result: Longint);
  end;

  TSnapShotMessage = packed record
    Msg    : Cardinal;
    case Byte of
      0: (Bmp   : TBitMap;
          pad1   : Integer;
          pad2   : Integer);
      1: (WParam: Longint;
          LParam: Longint;
          Result: Longint);
  end;

  TAbstractIODevice = class;

  TAbstractIOAccessorClass = class of TAbstractIOAccessor;
  TAbstractIOAccessor = class(TInterfacedObject, IIOSystemAccessor)
    function GetInfoPageContent(aPage : Integer) : WideString; virtual; stdcall; abstract;
    procedure Initialise(Host: IIOSystemAccessorHost; const Params : WideString);  virtual; stdcall; abstract;
    procedure UpdateShadowToReal;  virtual; stdcall; abstract;
    procedure UpdateRealToShadow;  virtual; stdcall; abstract;
    function Healthy : Boolean;  virtual; stdcall; abstract;
    procedure Reset;  virtual; stdcall; abstract;
    function LastError : WideString;  virtual; stdcall; abstract;
    function GetInputArea: Pointer;  virtual; stdcall; abstract;
    function GetOutputArea: Pointer;  virtual; stdcall; abstract;
    function GetInputSize: Integer;  virtual; stdcall; abstract;
    function GetOutputSize: Integer;  virtual; stdcall; abstract;
    function TextDefinition: WideString;  virtual; stdcall; abstract;
    procedure Shutdown;  virtual; stdcall; abstract;
  end;

  TGenericIOSubSystem = class(TInterfacedObject, IIOSystemAccessorHost)
  protected
    FInputSize : Integer;
    FOutputSize : Integer;
    ShadowIn : PAIOMap;
    ShadowOut : PAIOMap;
    FInfoPages : TStrings;
    Devices: TList;
    FAccessor: IIOSystemAccessor;
    FName : string;
    function GetInfoPageContent(aPage : Integer) : string; virtual;
  public
    constructor Create(const AName: string);
    procedure SetAccessor(AAccessor: IIOSystemAccessor);
    procedure CheckValidity; virtual;
    procedure AddDevice(Device : TAbstractIODevice); virtual;
    function  TextDefinition : string; virtual;
    procedure ReadTags;
    procedure WriteTags;
    procedure UpdateShadowToReal;
    procedure UpdateRealToShadow;
    function  Healthy : Boolean; virtual;
    function  _DeviceOK(aDevice : TAbstractIODevice) : Boolean; virtual;
    procedure SetAddressing(aDevice : TAbstractIODevice);
    function  LastError : string; virtual;
    procedure Reset; virtual;
    property  InfoPages : TStrings read FInfoPages;
    property  InfoPageContent[aIndex : Integer] : string read GetInfoPageContent;
    property InputSize: Integer read FInputSize;
    property OutputSize: Integer read FOutputSize;
    function DeviceCount: Integer;
    function GetDevice(Index: Integer): TAbstractIODevice;
    function AddPage(AName: WideString): Integer; stdcall;
    property Name: string read FName write FName;
    property Accessor: IIOSystemAccessor read FAccessor;
    procedure Shutdown;
    function _Release: Integer; stdcall;
  end;


  TAbstractIODeviceClass = class of TAbstractIODevice;
  TAbstractIODevice = class(TInterfacedObject)
  private
    FName : string;
    FInputBase  : Integer;
    FOutputBase : Integer;
    FInputSize  : Integer;
    FOutputSize : Integer;
    FInfoPages  : TStrings;
    function GetInputByte(Index : Integer) : Byte;
    function GetOutputByte(Index : Integer) : Byte;
    function GetInputWord(Index : Integer) : Word;
    function GetOutputWord(Index : Integer) : Word;
    function GetInputDWord(Index : Integer) : Integer;
    function GetOutputDWord(Index : Integer) : Integer;
    procedure SetInputByte(Index : Integer; Value : Byte);
    procedure SetOutputByte(Index : Integer; Value : Byte);
    procedure SetInputWord(Index : Integer; Value : Word);
    procedure SetOutputWord(Index : Integer; Value : Word);
    procedure SetInputDWord(Index : Integer; Value : Integer);
    procedure SetOutputDWord(Index : Integer; Value : Integer);
    function GetInfoPageContent(aPage : Integer) : string;
  protected
    Tags: TList;
  public
    SubSystem : TGenericIOSubSystem;
    ShadowIn, ShadowOut : PAIOMap;
    constructor Create(aName : string; var Params : string; aSubSystem : TGenericIOSubSystem); virtual;
    function  TextDefinition : string; virtual;
    procedure AddTag(aTag : TAbstractTag);
    procedure ReadTags; virtual; abstract;
    procedure WriteTags; virtual; abstract;
    function  OK : Boolean; virtual;
    property Name : string read FName;
    property InputBase  : Integer read FInputBase;
    property OutputBase : Integer read FOutputBase;
    property InputSize  : Integer read FInputSize;
    property OutputSize : Integer read FOutputSize;
    property InputByte  [Index : Integer] : Byte read GetInputByte write SetInputByte;
    property OutputByte [Index : Integer] : Byte read GetOutputByte write SetOutputByte;
    property InputWord  [Index : Integer] : Word read GetInputWord write SetInputWord;
    property OutputWord [Index : Integer] : Word read GetOutputWord write SetOutputWord;
    property InputDWord  [Index : Integer] : Integer read GetInputDWord write SetInputDWord;
    property OutputDWord [Index : Integer] : Integer read GetOutputDWord write SetOutputDWord;
    property InfoPages : TStrings read FInfoPages;
    property InfoPageContent[aPage : Integer] : string read GetInfoPageContent;
    function TagCount: Integer;
    function GetTag(Index: Integer): TAbstractTag;
    function _Release: Integer; stdcall;
  end;

  THashTable = class(TObject)
  private
    X, Y : TList;
    function GetXHash(Index : Integer) : Pointer;
    function GetYHash(Index : Integer) : Pointer;
  public
    constructor Create;
    destructor Destroy; override;
    property XHash[Index : Integer] : Pointer read GetXHash;
    property YHash[Index : Integer] : Pointer read GetYHash;
    procedure AddEntry(aX, aY : Pointer);
  end;


  TAcnc32Keywords = class(TObject)
  private
    FKeywords : TStringList;
    function GetEnableName(Index: Integer): string;
    function GetEnable(const Name : string) : Integer;
  public
    constructor Create;
    destructor Destroy; override;
    procedure ReadConfig(const FileName : string);
    property EnableNames[Index : Integer] : string read GetEnableName;
    function EnableCount : Integer;
    property Enable[const Name : string] : Integer read GetEnable;
  end;

  THSB = record
    hue, saturation, brightness : Double;
  end;
  THTTPWriteMethod = procedure(HFile : THandle; Data : Pointer; Size : Cardinal); stdcall;

//function ReadDelimited(var S : string; Delimiter : Char) : string;
function GetAbsolutePathLocal( const Path: String): String;
procedure TokenizeString(var aText : string);

function RGBToHSB(aColor : TColor) : THSB;
function HSBToRGB(aHSB : THSB) : TColor;

function StorageDate(DT: TDateTime): WideString;

{if Path = nil then Result:= PathEXE
if Path contains Drive (Local c: or UNC \\ServerName\ShareName\) then
return Path
if Path starts with '\' then return DriveExe + Path
else
Return PathEXE + Path

Result always ends in \ regardless of whether or not Path does
}

{function HexToInt( const S: String): Integer; }
{inverse of IntToHex from RTL - raise EConvertError}

function StripPath(const fp : string) : string;
function FixedSpace(Total : Integer; const aText : string) : string;
function ReadColor(S : TStreamTokenizer) : TColor;

function GetProcAddressX(hModule: HMODULE; lpProcName: LPCSTR): FARPROC;
procedure DoCopyOnWriteException(E : Exception);
function ReadVersionInfoData(FileName : string) : PVSFixedFileInfo;
function HMSFormat(aSeconds : Double) : string;

var
  LastSweep : Int64;
  ProfilePath, ProfileDrive, LoggingPath, TemporaryPath, LocalPath,
  LocalDrive, MachSpecPath, LivePath, ContractSpecificPath, ParserConfiguration, NCConfig : string;
  FakeIOSystem, FakeNCSystem, LanguageLogging : Boolean;

  LocaleOverride : string;

  PlcTick : TAllInt;

implementation

var TagList : TList;

// NOT THREAD SAFE .. To fix pass VersionInfo in as a parameter
var VersionInfo: PChar;

function HMSFormat(aSeconds : Double) : string;
var
  H, M : Integer;
  S, SS : Double;
begin
  ss := Abs(aSeconds / 1000);
  H := Round(ss) div 3600;
  M := (Round(ss) - (H* 3600)) div 60;
  S := ss - (H * 3600) - (M * 60);
  if H > 0 then
    Result := Format('%.2d:%.2d:%.2d', [H, M, Round(S)])
  else if M > 0 then
    Result:= Format('%.2d:%.2d', [M, Round(S)])
  else
    Result:= Format('%.1fs', [S]);

  if aSeconds < 0 then
    Result := '-' + Result;
end;

function ReadVersionInfoData(FileName : string) : PVSFixedFileInfo;
var
  QueryLen: UINT;
  Dummy: DWORD;
  VersionInfoSize: DWORD;
begin
  if Assigned(VersionInfo) then
    FreeMem(VersionInfo);
  VersionInfo := nil;

  VersionInfoSize := GetFileVersionInfoSize(PChar(FileName), Dummy);
  if VersionInfoSize = 0 then begin
    raise ECNCInstallFault.CreateFmt('Corrupt Version Info in %s: Last Error %d', [FileName, GetLastError]);
  end else begin
    GetMem(VersionInfo, VersionInfoSize);
    GetFileVersionInfo(PChar(FileName), Dummy, VersionInfoSize, VersionInfo);
    VerQueryValue(VersionInfo, '\', Pointer(Result), QueryLen);
//    FreeMem(VersionInfo);
  end;
end;

// END NOT THREAD SAFE

procedure DoCopyOnWriteException(E: Exception);
var Buffer : array [0..1023] of Char;
begin
  StrPLCopy(Buffer, E.Message, 1023);
  raise Exception.Create(Buffer);
end;

function GetProcAddressX(hModule: HMODULE; lpProcName: LPCSTR): FARPROC;
var Tmp : string;
    Buffer : array [0..1023] of Char;
begin
  Result := GetProcAddress(hModule, lpProcName);

  if not Assigned(Result) then begin
    GetModuleFileName(HModule, Buffer, 1023);
    Tmp := Buffer;
    raise Exception.CreateFmt('Library interface missing [%s] from [%s]', [lpProcName, Tmp]);
  end;
end;


function ReadColor(S : TStreamTokenizer) : TColor;
var TmpStr : string;
begin
  S.ExpectedToken('=');
  TmpStr := S.Read;
  if TmpStr[1] = '$' then begin
    try
      Result := StrToInt(TmpStr);
    except
      on E : EConvertError do begin
        Exit;
      end;
    end;
    Exit;
  end;

  if LowerCase(Copy(TmpStr,1, 2)) <> 'cl' then
    TmpStr := 'cl' + TmpStr;

  if not identtocolor(tmpstr, Integer(Result)) then begin
    Result := clWhite;
  end;

end;

procedure TokenizeString(var aText : string);
var I : Integer;
begin
  if aText <> '' then begin
    case aText[1] of
      'a'..'z',
      'A'..'Z' : aText[1] := aText[1];
    else
      aText[1] := '_';
    end;

    for I := 2 to Length(aText) do begin
      case aText[I] of
        'a'..'z',
        'A'..'Z',
        '0'..'9' : Continue;
      else
        aText[I] := '_';
      end;
    end;
  end;
end;

{ Extract the Filename part from a full path decriptor }
function StripPath(const fp : string) : string;
var p : integer;
begin
  result := fp;
  while true do begin
    p := Pos('\', result);
    if p = 0 then exit;
    Delete(result, 1, p);
  end;
end;

constructor TIOEventList.Create;
begin
  inherited Create;
  events := nil;
end;

destructor TIOEventList.Destroy;
var p : PTEventLL;
begin
  while events <> nil do begin
    p := events.next;
    dispose(events);
    events := p;
  end;
  inherited destroy;
end;

procedure TIOEventList.Add(aTag : TAbstractTag; Callback : TCallback);
var p : PTEventLL;
begin
  new(p);
  p.callback := callback;
  p.Tag      := aTag;
  p.next     := events;
  events     := p;
end;

function  TIOEventList.Get(aTag : TAbstractTag) : TCallback;
var p : PTEventLL;
begin
  p := events;
  while p <> nil do begin
    if p.Tag = aTag then begin
      Result := p.Callback;
      exit;
    end;
    p := p.next;
  end;
  Result := nil;
end;

function GetAbsolutePathLocal( const Path: String): String;
begin
  if Path = '' then
  begin
    Result:= LocalPath
  end else
  begin
    if ExtractFileDrive(Path) = '' then
    begin
      if Path[1] = '\' then
        Result:= LocalDrive + Path
      else
        Result:= LocalPath + Path
    end else
    begin
      Result:= Path
    end;
    Result:= Trim(Result);
    if Result[Length(Result)]  <> '\' then
      Result:= Result + '\'
  end
end;


constructor TTagList.Create;
begin
  inherited Create;
  // Sorted := True ?????? 21/11/99 Changed for display reasons?
  Self.Sorted := False;
end;

function TTagList.AddTag(aTag : TAbstractTag) : TTagHandle;
begin
  Result := Self.AddObject(aTag.Name, aTag);
end;

function TTagList.GetTag(I : Integer) : TAbstractTag;
begin
  Result := TAbstractTag(Objects[I]);
end;

procedure TTagList.SetTag(I : Integer; Tag : TAbstractTag);
begin
  if I > Count then
    raise ECNCInstallFault.CreateFmt(ImpossibleException, ['SetTag out of range']);
  if I = Count then
    Self.AddObject(Tag.Name, Tag)
  else
    Self.InsertObject(I, Tag.Name, Tag);
end;

var J : Integer;
constructor TAbstractTag.Create(aOwner : TTagOwner; aName : string);
begin
  inherited Create;
  HasValue := True;
  IsWritable := False;
  IsConnected := False;
  IsEvented := True;
  IsIO := False;
  IsSystem := False;
  Convert := False;

  FName := aName;
  FOwner := aOwner;
  if FOwner = nil then
    Inc(J);
  ClientLinks := [];

  TagList.Add(Self);

  LoggingPermissive := True;
end;

destructor TAbstractTag.Destroy;
var
  NewCall : PCallbackRecord;
  I : Integer;
begin
  while Count > 0 do begin
    NewCall := Callbacks[0];
    Dispose(NewCall);
    FCallbacks.Delete(0);
  end;

  I := TagList.IndexOf(Self);
  if I <> -1 then
    TagList.Delete(I);
  inherited Destroy;
end;

class function TAbstractTag.TagCount : Integer;
begin
  Result := TagList.Count;
end;

procedure TAbstractTag.AddCallback(CallBack : Tcallback; edge : TEdgeType; hw : Hwnd);
var
  NewCall : PCallbackRecord;
  I : Integer;
  A, B : TMethod;
begin
  A := TMethod(Callback);
  if Assigned(FCallbacks) then begin
    I := 0;
    while I < FCallbacks.Count do begin
      B := TMethod(PCallbackRecord(FCallbacks[I]).Callback);
      if (A.Code = B.Code) and (A.Data = B.Data) and (Edge = PCallbackRecord(FCallbacks[I]).Edge) then
        Exit;
      Inc(I);
    end;
  end;

  if Assigned(Callback) or (Hw <> 0) then begin
    new(NewCall);
    NewCall.callback := Callback;
    NewCall.edge     := edge;
    NewCall.hwnd     := hw;
    if not Assigned(FCallbacks) then
      FCallbacks := TList.Create;

    FCallbacks.Add(NewCall);
  end;
end;

procedure TAbstractTag.RemoveCallback(CallBack : Tcallback);
var A, B : TMethod;
  NewCall : PCallbackRecord;
  I : Integer;
begin
  I := 0;
  while I < FCallbacks.Count do begin
    NewCall := PCallbackRecord(FCallbacks[I]);
    A := TMethod(NewCall.Callback);
    B := TMethod(Callback);
    if (A.Code = B.Code) and (A.Data = B.Data) then begin
      FCallbacks.Delete(I);
      Dispose(NewCall);
    end;
    Inc(I);
  end;
end;

function  TAbstractTag.OK : Boolean;
begin
  Result := True;
end;

procedure TAbstractTag.CopyCallbacksFrom(aTag : TAbstractTag);
var
  NewCall : PCallbackRecord;
  I : Integer;
begin
  for I := 0 to aTag.Count - 1 do begin
    NewCall := aTag.Callbacks[I];
    with NewCall^ do
      AddCallback(Callback, edge, hwnd);
  end;
end;

procedure TAbstractTag.ReadFromBuf( var Buf; aLength : Integer);
begin
  if aLength <= DataSize then begin
    CopyMemory(DataPtr, @Buf, aLength);
    Refresh;
  end;
end;

procedure TAbstractTag.SetInterlock(aInterlock : PInterlock);
begin
  if aInterlock <> nil then begin
    if not (nltInterlock in NamedType) then
      if not Assigned(FInterlock) then
        New(FInterlock);
    Include(NamedType, nltInterlock);
    FInterlock^ := aInterlock^;
  end else begin
    if FInterlock <> nil then
      Dispose(FInterlock);
    Exclude(NamedType, nltInterlock);
  end;
end;

function TAbstractTag.GetAddress : Integer;
begin
  Result := 0;
end;

function TAbstractTag.GetInfoName: string;
begin
  Result:= Name;
end;

procedure TAbstractTag.SetAddress(aAddress : Integer);
begin
end;

function TAbstractTag.GetVCL : Boolean;
begin
  Result := ([nltVCL] <= NamedType);
end;

function TAbstractTag.GetSystem : Boolean;
begin
  Result := [nltSystem] <= NamedType;
end;

procedure TAbstractTag.SetVCL(aVCL : Boolean);
begin
  if aVCL then Include(NamedType, nltVCL)
          else Exclude(NamedType, nltVCL);
end;

procedure TAbstractTag.SetSystem(aSystem : Boolean);
begin
  if aSystem then Include(NamedType, nltSystem)
             else Exclude(NamedType, nltSystem);
end; 

function TAbstractTag.GetWritable : Boolean;
begin
  Result := [nltWritable] <= NamedType;
end;

procedure TAbstractTag.SetWritable(aWritable : Boolean);
begin
  if aWritable then Include(NamedType, nltWritable)
               else Exclude(NamedType, nltWritable);
end;

function TAbstractTag.GetHasValue : Boolean;
begin
  Result := [nltHasValue] <= NamedType;
end;

procedure TAbstractTag.SetHasValue(aHasValue : Boolean);
begin
  if aHasValue then Include(NamedType, nltHasValue)
               else Exclude(NamedType, nltHasValue);
end;

function TAbstractTag.GetConstant : Boolean;
begin
  Result := [nltConstant] <= NamedType;
end;

procedure TAbstractTag.SetConstant(aConstant : Boolean);
begin
  if aConstant then Include(NamedType, nltConstant)
               else Exclude(NamedType, nltConstant);
end;

function TAbstractTag.GetInterlock : Boolean;
begin
  Result := [nltInterlock] <= NamedType;
end;

function TAbstractTag.GetOwned : Boolean;
begin
  Result := [nltOwned] <= NamedType;
end;

procedure TAbstractTag.SetOwned(aOwned : Boolean);
begin
  if aOwned then begin
    Include(NamedType, nltOwned);
    Include(NamedType, nltConnected);
  end else begin
    Exclude(NamedType, nltOwned);
    Exclude(NamedType, nltConnected);
  end;
end;

function TAbstractTag.GetConnected : Boolean;
begin
  Result := [nltConnected] <= NamedType;
end;

procedure TAbstractTag.SetConnected(aConnected : Boolean);
begin
  if aConnected then Include(NamedType, nltConnected)
                else Exclude(NamedType, nltConnected);
end;

function TAbstractTag.GetEvented : Boolean;
begin
  Result := [nltEvented] <= NamedType;
end;

procedure TAbstractTag.SetEvented(aEvented : Boolean);
begin
  if aEvented then Include(NamedType, nltEvented)
              else Exclude(NamedType, nltEvented);
end;

function TAbstractTag.GetConvert: Boolean;
begin
  Result := nltConvert in NamedType;
end;

procedure TAbstractTag.SetConvert(aConvert: Boolean);
begin
  if aConvert then Include(NamedType, nltConvert)
              else Exclude(NamedType, nltConvert);
end;


function TAbstractTag.GetIO : Boolean;
begin
  Result := nltIO in NamedType;
end;

procedure TAbstractTag.SetIO(aIO : Boolean);
begin
  if aIO then Include(NamedType, nltIO)
         else Exclude(NamedType, nltIO);
end;

function TAbstractTag.GetPersistent : Boolean;
begin
  Result := nltPersistent in NamedType;
end;

procedure TAbstractTag.SetPersistent(aPersistent : Boolean);
begin
  if aPersistent then Include(NamedType, nltPersistent)
                 else Exclude(NamedType, nltPersistent);
end;

function TAbstractTag.GetAsShortString : string;
begin
  Result := AsString;
end;

function TAbstractTag.GetCallback(Index : Integer) : PCallbackRecord;
begin
  Result := PCallbackRecord(Callbacks[Index]);
end;

function TAbstractTag.GetCount : Integer;
begin
  if Assigned(FCallbacks) then
    Result := FCallbacks.Count
  else
    Result := 0;
end;

function TAbstractTag.GetItems(Index : Integer) : Pointer;
begin
{  CallbackLock.Enter;
  try }
    Result := FCallbacks[Index];
{  finally
    CallbackLock.Release;
  end; }
end;

procedure TAbstractTag.SetItems(Index : Integer; aItem : Pointer);
begin
  FCallbacks[Index] := aItem;
end;

const
  InterlockPath = 'Interlock\';
  ActiveName = 'Active';
  ExclusionName = 'Exclusion';
  InclusionName = 'Inclusion';
  ModesName = 'Modes';
  AccessName = 'Access';

procedure TAbstractTag.ReadAccessFromRegistry;
var Reg : TCNCRegistry;
    aInterlock, Nin : TInterlock;
begin
  Nin.Modes := [Low(TCNCMode)..High(TCNCMode)];
  Nin.Access := [Low(TAccessLevel)..High(TAccessLevel)];
  Nin.Inclusion := [];
  Nin.Exclusion := [];
  Nin.Active := False;

  Reg := TCNCRegistry.Create;
  try
    if Reg.OpenKey(RegistryBase + '\' + InterlockPath + Name, False) then begin
      if Reg.ValueExists(ActiveName) then begin
        aInterlock := Nin;
        aInterlock.Active := Reg.ReadBool(ActiveName);
        if Reg.ValueExists(ExclusionName) then
          aInterlock.Exclusion := TCNCStatusTypes(Byte(Reg.ReadInteger(ExclusionName)));
        if Reg.ValueExists(InclusionName) then
          aInterlock.Inclusion := TCNCStatusTypes(Byte(Reg.ReadInteger(InclusionName)));
        if Reg.ValueExists(ModesName) then
          aInterlock.Modes := TCNCModes(Byte(Reg.ReadInteger(ModesName)));
        if Reg.ValueExists(AccessName) then
          aInterlock.Access := TAccessLevels(Byte(Reg.ReadInteger(AccessName)));
        Interlock := @aInterlock;
      end;
      Reg.CloseKey;
    end;
  finally
    Reg.Free;
  end;
end;

procedure TAbstractTag.WriteAccessToRegistry(const aInterlock : TInterlock);
var Reg : TCNCRegistry;
begin
  Reg := TCNCRegistry.Create;
  try
    if Reg.OpenKey(RegistryBase + '\' + InterlockPath + Name, True) then begin
      Reg.WriteBool(ActiveName,  aInterlock.Active);
      Reg.WriteInteger(ExclusionName, Byte(aInterlock.Exclusion));
      Reg.WriteInteger(InclusionName, Byte(aInterlock.Inclusion));
      Reg.WriteInteger(ModesName, Byte(aInterlock.Modes));
      Reg.WriteInteger(AccessName, Byte(aInterlock.Access));
      Reg.CloseKey;
    end;
    Interlock := @aInterlock;
  finally
    Reg.Free;
  end;
end;

procedure TAbstractTag.ClearAccessFromRegistry;
var Reg : TCNCRegistry;
begin
  Reg := TCNCRegistry.Create;
  try
    if Reg.OpenKey(RegistryBase + '\' + InterlockPath + Name, False) then begin
      Reg.CloseKey;
      Reg.DeleteKey(RegistryBase + '\' + InterlockPath + Name);
      Interlock := nil;
    end;
  finally
    Reg.Free;
  end;
end;

procedure TAbstractTag.ReadFromRegistry(Reg : TCNCRegistry);
var Buf : Pointer;
    ThisSize : Integer;
begin
  if Reg.ValueExists(Name) then begin
    ThisSize := 4095;
    GetMem(Buf, ThisSize);
    try
      ThisSize := Reg.ReadBinaryData(Name, Buf^, ThisSize);
      ReadFromBuf(Buf^, ThisSize);
    finally
      FreeMem(Buf);
    end;
  end;
//  Refresh;
end;

procedure TAbstractTag.WriteToRegistry(Reg : TCNCRegistry);
begin
  try
    Reg.WriteBinaryData(Name, DataPtr^, DataSize);
  except
  end;
end;


constructor TGenericIOSubSystem.Create(const AName : string);
begin
  inherited Create;
  Devices:= TList.Create;
  Name := AName;
  FInfoPages := TStringList.Create;
  FInfoPages.Add('Info');
end;

procedure TGenericIOSubSystem.SetAccessor(AAccessor: IIOSystemAccessor);
begin
  Self.FAccessor:= AAccessor;
  ShadowIn:= Accessor.GetInputArea;
  ShadowOut:= Accessor.GetOutputArea;

  FInputSize:= Accessor.GetInputSize;
  FOutputSize:= Accessor.GetOutputSize;
end;

function  TGenericIOSubSystem.LastError : string;
begin
  Result := '';
end;

procedure TGenericIOSubSystem.Reset;
begin
  if Assigned(Accessor) then
    Accessor.Reset;
end;

function TGenericIOSubSystem.DeviceCount: Integer;
begin
  Result:= Devices.Count;
end;


function TGenericIOSubSystem.GetDevice(Index: Integer): TAbstractIODevice;
begin
  Result:= TAbstractIODevice(Devices[Index]);
end;

function TGenericIOSubSystem.AddPage(AName: WideString): Integer;
begin
  Result:= FInfoPages.Add(AName);
end;

procedure TGenericIOSubSystem.Shutdown;
begin
  if Assigned(Accessor) then
    Accessor.Shutdown;
end;

function TGenericIOSubSystem._Release: Integer;
begin
  // Dont release
  Result:= 1;
end;

function TGenericIOSubSystem.GetInfoPageContent(aPage : Integer) : string;
begin
  if aPage = 0 then begin
    Result := InputSizeLang + CarRet + IntToStr(FInputSize) + CarRet +
              OutputSizeLang + CarRet + IntToStr(FOutputSize) + CarRet;
  end else
    Result:= Accessor.GetInfoPageContent(aPage - 1);
end;

procedure TGenericIOSubSystem.AddDevice(Device : TAbstractIODevice);
begin
  if (Device.InputBase + Device.InputSize > FInputSize) or
     (Device.OutputBase + Device.OutputSize > FOutputSize) then
    raise ECNCInstallFault.CreateFmt('Cannot Add Device [%s] to [%s] Range Invalid',
                                             [Device.Name, Name]);

  Devices.Add(Device);
end;

procedure TGenericIOSubSystem.CheckValidity;
begin
end;

procedure TGenericIOSubSystem.SetAddressing(aDevice : TAbstractIODevice);
begin
  aDevice.ShadowIn  := Addr(ShadowIn^[aDevice.FInputBase]);
  aDevice.ShadowOut := Addr(ShadowOut^[aDevice.FOutputBase]);
end;

procedure TGenericIOSubSystem.UpdateShadowToReal;
begin
  if Healthy then begin
    Accessor.UpdateShadowToReal;
    WriteTags;
  end;
end;

procedure TGenericIOSubSystem.UpdateRealToShadow;
begin
  if Healthy then begin
    Accessor.UpdateRealToShadow;
    ReadTags;
  end;
end;

procedure TGenericIOSubSystem.ReadTags;
var I : Integer;
begin
  for I := 0 to Devices.Count - 1 do
    TAbstractIODevice(Devices.Items[i]).ReadTags;
end;

procedure TGenericIOSubSystem.WriteTags;
var I : Integer;
begin
  for I := 0 to Devices.Count - 1 do
    TAbstractIODevice(Devices.Items[I]).WriteTags;
end;

function  TGenericIOSubSystem.Healthy : Boolean;
begin
  if Assigned(Accessor) then
    Result:= Accessor.Healthy
  else
    Result:= False;
end;

function  TGenericIOSubSystem.TextDefinition : string;
begin
  Result := Accessor.TextDefinition;
    {
     ClassName + ';  ' +
    '$' + IntToHex(FInputSize,  4) + '; ' +
    '$' + IntToHex(FOutputSize, 4);
    }
end;

{procedure TAbstractIOSubSystem.Properties(SL : TStringList);
begin
  SL.Clear;
end; }

function  TGenericIOSubSystem._DeviceOK(aDevice : TAbstractIODevice) : Boolean;
begin
  Result := Healthy;
end;

function TAbstractIODevice.TextDefinition : string;
begin
  Result := ClassName + '; ' +
    '$' + IntToHex(InputBase,  4) + '; ' +
    '$' + IntToHex(OutputBase, 4) + '; ' +
    '$' + IntToHex(InputSize,  4) + '; ' +
    '$' + IntToHex(OutputSize, 4);
end;

function TAbstractIODevice.GetInputByte(Index : Integer) : Byte;
begin
  Result := ShadowIn^[Index];
end;

procedure TAbstractIODevice.SetInputByte(Index : Integer; Value : Byte);
begin
  ShadowIn^[Index] := Value;
end;

function TAbstractIODevice.GetOutputByte(Index : Integer) : Byte;
begin
  Result := ShadowOut^[Index];
end;

procedure TAbstractIODevice.SetOutputByte(Index : Integer; Value : Byte);
begin
  ShadowOut^[Index] := Value;
end;

function TAbstractIODevice.GetInputWord(Index : Integer) : Word;
var T : PWORD;
begin
  T := Addr(ShadowIn^[Index]);
  Result := T^;
end;

procedure TAbstractIODevice.SetInputWord(Index : Integer; Value : Word);
var T : PWORD;
begin
  T := Addr(ShadowIn^[Index]);
  T^ := Value;
end;


function TAbstractIODevice.GetOutputWord(Index : Integer) : Word;
var T : PWORD;
begin
  T := Addr(ShadowOut^[Index]);
  Result := T^;
end;

procedure TAbstractIODevice.SetOutputWord(Index : Integer; Value : Word);
var T : PWORD;
begin
  T := Addr(ShadowOut^[Index]);
  T^ := Value;
end;

function TAbstractIODevice.GetInputDWord(Index : Integer) : Integer;
var T : PDWORD;
begin
  T := Addr(ShadowIn^[Index]);
  Result := T^;
end;

procedure TAbstractIODevice.SetInputDWord(Index : Integer; Value : Integer);
var T : PDWORD;
begin
  T := Addr(ShadowIn^[Index]);
  T^ := Value;
end;

function TAbstractIODevice.GetOutputDWord(Index : Integer) : Integer;
var T : PDWORD;
begin
  T := Addr(ShadowOut^[Index]);
  Result := T^;
end;

procedure TAbstractIODevice.SetOutputDWord(Index : Integer; Value : Integer);
var T : PDWORD;
begin
  T := Addr(ShadowOut^[Index]);
  T^ := Value;
end;

constructor TAbstractIODevice.Create(aName : string; var Params : string; aSubSystem : TGenericIOSubSystem);
begin
  inherited Create;
  Tags:= TList.Create;
  FName := aName;
  try
    FInputBase  := StrToInt(ReadDelimited(Params, ';'));
    FOutputBase := StrToInt(ReadDelimited(Params, ';'));
    FInputSize  := StrToInt(ReadDelimited(Params, ';'));
    FOutputSize := StrToInt(ReadDelimited(Params, ';'));
  except
    on E : EConvertError do
      raise ECNCInstallFault.CreateFmt('Illegal Device format in [%s]', [aName]);
  end;

  SubSystem := aSubSystem;
  SubSystem.SetAddressing(Self);

  FInfoPages := TStringList.Create;
  FInfoPages.Add(InputLang);
  FInfoPages.Add(OutputLang);
end;

function TAbstractIODevice.GetInfoPageContent(aPage : Integer) : string;
var I : Integer;
begin
  if aPage = 0 then begin
    Result :=
      IOSubSystemLang + CarRet + SubSystem.Name + CarRet +
      InputBaseLang + CarRet + IntToStr(FInputBase) + CarRet +
      InputSizeLang + CarRet + IntToStr(FInputSize) + CarRet;

    for I := 0 to FInputSize - 1 do begin
      Result := Result + 'X' + IntToStr(I) + CarRet + IntToStr(InputByte[I]) + CarRet;
    end;

  end else if aPage = 1 then begin
    Result :=
      IOSubSystemLang + CarRet + SubSystem.Name + CarRet +
      OutputBaseLang + CarRet + IntToStr(FInputBase) + CarRet +
      OutputSizeLang + CarRet + IntToStr(FOutputSize) + CarRet;

    for I := 0 to FOutputSize - 1 do begin
      Result := Result + 'Q' + IntToStr(I) + CarRet + IntToStr(OutputByte[I]) + CarRet;
    end;
  end;
end;

procedure TAbstractIODevice.AddTag(aTag : TAbstractTag);
var TPos, IOSize : Integer;
begin
  // TPos represents the end address of this item
  TPos := aTag.Address + aTag.DataSize - 1;
  if aTag.IsWritable then IOSize := FOutputSize
                     else IOSize := FInputSize;

  if TPos > IOSize then
    raise ECNCInstallFault.CreateFmt('Cannot Add Tag [%s] to [%s] Range Invalid',
                                             [aTag.Name, Name]);

  Tags.Add(aTag);
end;

function  TAbstractIODevice.OK : Boolean;
begin
  if Assigned(Subsystem) then
    Result := Subsystem._DeviceOK(Self)
  else
    Result := True;
end;

function TAbstractIODevice.TagCount: Integer;
begin
  Result:= Tags.Count;
end;

function TAbstractIODevice.GetTag(Index: Integer): TAbstractTag;
begin
  Result:= TAbstractTag(Tags[Index]);
end;

function TAbstractIODevice._Release: Integer;
begin
  // Dont release
  Result:= 1;
end;

function FixedSpace(Total : Integer; const aText : string) : string;
var i : integer;
begin
  Result := aText;
  for I := Length(aText) to Total do begin
    Result := Result + ' ';
  end;
end;

constructor THashTable.Create;
begin
  inherited;
  X := TList.Create;
  Y := TList.Create;
end;

destructor THashTable.Destroy;
begin
  X.Free;
  Y.Free;
  inherited;
end;

procedure THashTable.AddEntry(aX, aY : Pointer);
begin
  X.Add(aX);
  Y.Add(aY);
end;

function THashTable.GetXHash(Index : Integer) : Pointer;
begin
  Result := XHash[Index];
end;

function THashTable.GetYHash(Index : Integer) : Pointer;
begin
  Result := YHash[Index];
end;

function RGBToHSB(aColor : TColor) : THSB;
var Cmax, Cmin : Double;
    R, G , B : Integer;
    RedC, GreenC, BlueC : Double;
begin
  R := aColor and $FF;
  G := (aColor shr 8) and $FF;
  B := (aColor shr 16) and $FF;

  if R > G then
    CMax := R
  else
    CMax := G;

  if B > CMax then
    CMax := B;

  if R < G then
    Cmin := R
  else
    CMin := G;

  if B < Cmin then
    Cmin := B;

  Result.Brightness := Cmax / 255;
  if Cmax <> 0 then
    Result.Saturation := (Cmax - Cmin) / Cmax
  else
    Result.Saturation := 0;

  if Result.Saturation = 0 then
    Result.Hue := 0
  else begin
    RedC := (Cmax - R) / (Cmax - Cmin);
    GreenC := (Cmax - G) / (Cmax - Cmin);
    BlueC := (Cmax - B) / (Cmax - Cmin);

    if R = Cmax then
      Result.Hue := BlueC - GreenC
    else if G = Cmax then
      Result.Hue :=  2 + RedC - BlueC
    else
      Result.Hue := 4 + GreenC - RedC;

    if Result.Hue < 0 then
      Result.Hue := Result.Hue + 1;
  end;
end;

function HSBToRGB(aHSB : THSB) : TColor;
var R, G, B : Cardinal;
    h, f, p, q, t : Double;
begin
   R := 0;
   G := 0;
   B := 0;
   if (aHSB.Saturation = 0) then begin
     R := Round(aHSB.Brightness * 255);
     G := Round(aHSB.Brightness * 255);
     B := Round(aHSB.Brightness * 255);
   end else begin
     h := (aHSB.hue - Floor(aHSB.hue)) * 6.0;
     f := h - floor(h);
     p := aHSB.Brightness * (1.0 - aHSB.saturation);
     q := aHSB.Brightness * (1.0 - aHSB.saturation * f);
     t := aHSB.Brightness * (1.0 - (aHSB.saturation * (1.0 - f)));
     case Round(h) of
       0: begin
		r := Round (aHSB.brightness * 255);
		g := Round (t * 255);
		b := Round (p * 255);
       end;
       1: begin
		r := Round (q * 255);
		g := Round (aHSB.brightness * 255);
		b := Round (p * 255);
       end;
       2: begin
		r := Round (p * 255);
		g := Round (aHSB.brightness * 255);
		b := Round (t * 255);
       end;
       3: begin
		r := Round (p * 255);
		g := Round (q * 255);
		b := Round (aHSB.brightness * 255);
      end;
       4: begin
		r := Round (t * 255);
		g := Round (p * 255);
		b := Round (aHSB.brightness * 255);
       end;
       5: begin
		r := Round (aHSB.brightness * 255);
		g := Round (p * 255);
		b := Round (q * 255);
       end;
     end;
   end;


   Result := $ff000000 or (R shl 16) or (g shl 8) or (b shl 0);
end;


{ TAcnc32Keywords }

constructor TAcnc32Keywords.Create;
begin
  FKeywords := TStringList.Create;
  FKeywords.Sorted := True;
end;

destructor TAcnc32Keywords.Destroy;
begin
  FKeywords.Free;
  inherited;
end;

function TAcnc32Keywords.GetEnableName(Index: Integer): string;
begin
  Result := FKeywords.Names[Index];
end;

function TAcnc32Keywords.EnableCount: Integer;
begin
  Result := FKeywords.Count;
end;

function TAcnc32Keywords.GetEnable(const name: string): Integer;
var Tmp : string;
begin
  Tmp := FKeywords.Values[Name];
  Result := StrToIntDef(Tmp, 0); // (Tmp <> '') and (Tmp <> '0');
end;

procedure TAcnc32Keywords.ReadConfig(const FileName: string);
var Ini : TIniFile;
    S : TStringList;
begin
  Ini := TIniFile.Create(FileName);
  S:= TStringList.Create;
  try
    Ini.ReadSectionValues('Enables', FKeywords);
    Ini.ReadSectionValues('General', S);
    FKeyWords.AddStrings(S);
  finally
    Ini.Free;
    S.Free;
  end;
end;

function StorageDate(DT: TDateTime): WideString;
var ST : TSystemTime;
begin
  DateTimeToSystemTime(DT, ST);
  // YYYY-MM-DD hh:mm:ss  This seems to be an SQL standard as it Ascii / chronologically the same
  Result := Format('%4d-%.2d-%.2d %.2d:%.2d:%.2d',
     [ST.wYear, ST.wMonth, ST.wDay, ST.wHour, ST.wMinute, ST.wSecond ]  );
end;



var
  I : Integer;
  S: string;
{ TFaultRecord }

var RecordCount: Integer;
constructor TFaultRecord.Create;
begin
  Inc(RecordCount);
end;

destructor TFaultRecord.Destroy;
begin
  Dec(RecordCount);
  inherited;
end;

initialization
  TagList := TList.Create;


  ParserConfiguration:= 'default.ini';
  ProfilePath := '';
  NCConfig:= 'nc';

  for I := 1 to ParamCount do begin
    if (ParamStr(I)[1] = '-') and (Length(ParamStr(I)) > 1) then begin
      case UpperCase(ParamStr(I))[2] of
        'P' : begin
            ProfilePath := Copy(ParamStr(I), 3, Length(ParamStr(I)));
            if Length(ProfilePath) > 0 then
              ProfilePath := SysUtils.IncludeTrailingPathdelimiter(ProfilePath);
            ProfileDrive := ExtractFileDrive(ProfilePath);
            ProfilePath := ExtractFilePath(ProfilePath);
        end;

        'F' : begin
          FakeIOSystem := True;
          FakeNCSystem := True;
        end;

        'I' : begin
          FakeIOSystem := True;
        end;

        'N' : begin
          if ParamStr(I) = '-N' then begin
            FakeNCSystem := True;
          end else if ParamStr(I)[3] = 'C' then begin
            S:= Copy(ParamStr(I), 4, LEngth(ParamStr(I)));
            NCConfig:= ReadQuotedString(S);
          end;
        end;

        'L' : begin
          LocaleOverride := Copy(ParamStr(I), 3, Length(ParamStr(I)));
        end;

        'O': begin
          LanguageLogging:= True;
        end;

        'C': begin
          ParserConfiguration:= Copy(ParamStr(I), 3, Length(ParamStr(I)));
        end;

      end;
    end;
  end;

  if ProfilePath <> '' then
    ProfilePath := SysUtils.IncludeTrailingPathdelimiter(ProfilePath);

  if ProfilePath = '' then begin
    ProfilePath := ExtractFilePath(ParamStr(0));
    ProfileDrive := ExtractFileDrive(ProfilePath);
  end;


{  FakeIOSystem := True;
  FakeNCSystem := True; }

  LoggingPath := ProfilePath;
  LivePath := ProfilePath + 'live\';
  TemporaryPath := LoggingPath;
  LocalPath := ProfilePath;
  LocalDrive := ProfileDrive;
  MachSpecPath := ProfilePath;

finalization
  TagList.Free;
end.
