unit DdeBin;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DDEML;

type
  TDdeBin       = class;
  TDdeBinClient = class;
  TDdeBinItem   = class;

  TDdeBinClient = class(TComponent)
  private
    DdeBin        : TDdeBin;
    FItems        : TList;
    FOnDisconnect : TNotifyEvent;
  protected
    procedure   OnAttach (item : TDdeBinItem);
    procedure   OnDetach(item : TDdeBinItem);
  public
    constructor create(Aowner : TComponent); override;
    destructor  destroy; override;
    function    SetLink(service, topic : string) : boolean;
    function    connect : boolean;
    procedure   disconnect;
    function    ExecuteMacro(Cmd: PChar; waitFlg: Boolean): Boolean;

    procedure   DataChange(DdeMsg: HDDEData; hszIt: HSZ);
    procedure   srvrDisconnect;
    procedure   xactComplete;
  published
    property    OnDisconnect : TNotifyEvent read FOnDisconnect write FOnDisconnect;
  end;

  TDdeBinItem = class(TComponent)
  private
    DdeBin        : TDdeBin;
    FItem         : string;
    hszItem       : HSZ;
    FConversation : TDdeBinClient;
    FSize         : Longint;
    Fdata         : Pointer;
    FOnAdvise     : TNotifyEvent;
    FOnDisconnect : TNotifyEvent;
    Fpassive      : boolean;
    procedure SetConversation(conv : TDdeBinClient);
  protected
    procedure DataChange(DdeMsg: HDDEData; hszIt: HSZ);
  public
    constructor create(Aowner : TComponent); override;
    destructor  destroy; override;
    function startAdvise : boolean;
    function stopAdvise  : boolean;
    function PokeData    : Boolean;
    function requestData : boolean;
    property Data : Pointer read Fdata write Fdata;
  published
    property Item         : string        read FItem         write Fitem;
    property Conversation : TDdeBinClient read FConversation write setConversation;
    property Size         : Longint       read Fsize         write Fsize;
    property OnAdvise     : TNotifyEvent  read FOnAdvise     write FOnAdvise;
    property OnDisconnect : TNotifyEvent  read FOnDisconnect write FOnDisconnect;
    property Passive      : boolean       read Fpassive      write Fpassive;
  end;

  TDdeBin = class(TComponent)
  private
  protected
  public
    ConvInfo   : TConvInfo;
    IdInst     : Longint;
    Service    : string;
    Topic      : string;
    hszService : HSZ;
    hszTopic   : HSZ;
    wFmt       : word;
    Conv       : Longint;
    FwaitStat  : boolean;
    constructor create(Aowner : TComponent); override;
    destructor  destroy; override;
    function    createHandles : boolean;
    procedure   freeHandles;
  published
  end;

{procedure Register;} {MGL 15/10}

implementation

function DdeCallBack(CallType, Fmt : UINT; Conv: HConv; hsz1, hsz2: HSZ;
             Data: HDDEData; Data1, Data2: DWORD): HDDEData; stdcall;
var
  ci     : TConvInfo;
  ddeCli : TDdeBinClient;
  xID    : Integer;
begin
  result := 0;
  ci.cb := sizeof(TConvInfo);
  if CallType = XTYP_XACT_COMPLETE then
    xID := Data1
  else
    xID := QID_SYNC;
  if DdeQueryConvInfo(Conv, xID, @ci) = 0 then Exit;

  case CallType of
      XTYP_XACT_COMPLETE:  begin
          ddeCli := TDdeBinClient(ci.hUser);
          if assigned(ddeCli) then ddeCli.xactComplete;
        end;

      XTYP_ADVDATA: begin
          ddeCli := TDdeBinClient(ci.hUser);
          ddeCli.DataChange(Data, hsz2);
          result := DDE_FACK;
        end;

      XTYP_DISCONNECT:  begin
          ddeCli := TDdeBinClient(ci.hUser);
          ddeCli.srvrDisconnect;
        end;
    end;
end;

constructor TDdeBin.create(Aowner : TComponent);
begin
  inherited create(Aowner);
  IdInst     := 0;
  if ( DdeInitialize ( IdInst, DdeCallback, APPCLASS_STANDARD, 0 ) <> DMLERR_NO_ERROR) then
    MessageDlg ('TDdeBinClient Failed DdeInitialize', mtInformation, [mbOK], 0);
  wFmt       := 0;
  Conv       := 0;
  hszService := 0;
  hszTopic   := 0;
end;

destructor TDdeBin.destroy;
begin
  freeHandles;
//  DdeUninitialize(FDdeIdInst) // This is a bit weird
                              // it crashes sometimes ??
                              // However not calling it causes
                              // no problems.
  inherited destroy;
end;

function  TDdeBin.createHandles : boolean;
begin
  result := false;
  if hszService = 0 then
    hszService := DdeCreateStringHandle ( IdInst, pChar(Service), CP_WINANSI );
  if hszService = 0 then exit;
  if hszTopic = 0 then
    hszTopic   := DdeCreateStringHandle ( IdInst, pChar(Topic),  CP_WINANSI );
  result := hszTopic <> 0;
end;

procedure TDdeBin.freeHandles;
begin
  if hszService <> 0 then DdeFreeStringHandle ( IdInst, hszService );
  if hszTopic   <> 0 then DdeFreeStringHandle ( IdInst, hszTopic );
end;

constructor TDdeBinClient.create(Aowner : TComponent);
begin
  inherited create(Aowner);
  FItems := TList.create;
  DdeBin := TDdeBin.create(Self);
end;

destructor TDdeBinClient.destroy;
begin
  disconnect;
  DdeBin.destroy;
  inherited Destroy;
end;

procedure TDdeBinClient.disconnect;
begin
  if DdeBin.Conv <> 0 then DdeSetUserHandle(DdeBin.Conv, QID_SYNC, 0);
  if DdeBin.Conv <> 0 then DdeDisconnect(DdeBin.Conv);
  DdeBin.Conv := 0;
end;

procedure TDdeBinClient.srvrDisconnect;
var
  ItemLnk: TDdeBinItem;
  i: word;
begin
  i := 0;
  if assigned(OnDisconnect) then OnDisconnect(Self);
  while i < FItems.Count do begin
    ItemLnk := TDdeBinItem(FItems [i]);
      if assigned(ItemLnk.OnDisconnect) then ItemLnk.OnDisconnect(ItemLnk);
    Inc(i);
  end;
end;

procedure TDdeBinClient.xactComplete;
begin
  DdeBin.FwaitStat := False;
end;

function TDdeBinClient.setLink(Service, Topic : string) : boolean;
begin
  DdeBin.Service := Service;
  DdeBin.Topic   := Topic;
  result := DdeBin.createHandles;
end;

function TDdeBinClient.connect : boolean;
var Context : TConvContext;
begin
  result := False;

  FillChar(Context, SizeOf(Context), 0);
  with Context do begin
    cb := SizeOf(TConvConText);
    iCodePage := CP_WINANSI;
  end;

  DdeBin.Conv := DdeConnect ( DdeBin.IdInst, DdeBin.hszService, DdeBin.hszTopic, @Context);

  if DdeBin.Conv <> 0 then begin
    DdeBin.ConvInfo.cb := sizeof(TConvInfo);
    DdeQueryConvInfo(DdeBin.Conv, QID_SYNC, @DdeBin.ConvInfo);
    DdeSetUserHandle(DdeBin.Conv, QID_SYNC, LongInt(Self));
    result := True;
  end else begin
    case DdeGetLastError(DdeBin.IdInst) of
      DMLERR_DLL_NOT_INITIALIZED,
      DMLERR_INVALIDPARAMETER   ,
      DMLERR_NO_CONV_ESTABLISHED,
      DMLERR_NO_ERROR           : Exit;
    else Exit;
    end;
  end;

  if DdeBin.ConvInfo.wFmt <> 0 then DdeBin.wFmt := DdeBin.ConvInfo.wFmt
  else DdeBin.wFmt := CF_TEXT;
end;

function TDdeBinClient.ExecuteMacro(Cmd: PChar; waitFlg: Boolean): Boolean;
var
  hszCmd: HDDEData;
  hdata: HDDEData;
  ddeRslt: integer; //LongInt;
begin
  Result := False;
  if DdeBin = nil then exit;
  if DdeBin.Conv = 0 then Exit;

  if DdeBin.FwaitStat = True then begin
    result := True;
    exit;
  end;

  hszCmd := DdeCreateDataHandle(DdeBin.IdInst, Cmd, StrLen(Cmd) + 1, 0, 0, DdeBin.wFmt, 0);
  if hszCmd = 0 then Exit;

  hdata := DdeClientTransaction(Pointer(hszCmd), -1, DdeBin.Conv, 0, DdeBin.wFmt,
     XTYP_EXECUTE, TIMEOUT_ASYNC, @ddeRslt);
  if hdata <> 0 then  Result := True;

  DdeFreeDataHandle(hszCmd);
end;

procedure  TDdeBinClient.DataChange(DdeMsg: HDDEData; hszIt: HSZ);
var
  ItemLnk: TDdeBinItem;
  i: word;
begin
  i := 0;
  while i < FItems.Count do begin
    ItemLnk := TDdeBinItem(FItems [i]);
    if (ItemLnk.hszItem = hszIt) then begin
 { data has changed and we found a link that might be interested }
      ItemLnk.DataChange(DdeMsg, hszIt);
    end;
    Inc(i);
  end;
end;

procedure   TDdeBinClient.OnAttach (item : TDdeBinItem);
begin
  item.DdeBin := DdeBin;
  FItems.add(item);
end;

procedure   TDdeBinClient.OnDetach(item : TDdeBinItem);
begin
  item.DdeBin := nil;
  FItems.remove(item);
end;


constructor TDdeBinItem.create(Aowner : TComponent);
begin
  inherited create(Aowner);
  DdeBin        := nil;
  FConversation := nil;
  Fpassive      := False;
end;

destructor  TDdeBinItem.destroy;
begin
  StopAdvise;
  Conversation := nil;
  if DdeBin <> nil then
    if hszItem <> 0 then DdeFreeStringHandle ( DdeBin.IdInst, hszItem );
  inherited destroy;
end;

function TDdeBinItem.StartAdvise: Boolean;
var
  ddeRslt : integer; //LongInt;
  hdata   : HDDEData;
  nodata  : word;
begin
  Result := False;
  if DdeBin = nil then exit;
  if DdeBin.Conv = 0 then Exit;
  if hszItem = 0 then
    hszItem := DdeCreateStringHandle ( DdeBin.IdInst, pChar(Item), CP_WINANSI );

  nodata := 0;
  if not Fpassive then nodata := XTYPF_NODATA;

  hdata := DdeClientTransaction(nil, -1, DdeBin.Conv, hszItem,
    DdeBin.wFmt, XTYP_ADVSTART or nodata, TIMEOUT_ASYNC, @ddeRslt);

  if hdata = 0 then begin
    case DdeGetLastError(DdeBin.IdInst) of
         DMLERR_ADVACKTIMEOUT        ,
         DMLERR_BUSY                 ,
         DMLERR_DATAACKTIMEOUT       ,
         DMLERR_DLL_NOT_INITIALIZED  ,
         DMLERR_DLL_USAGE            ,
         DMLERR_EXECACKTIMEOUT       ,
         DMLERR_INVALIDPARAMETER     ,
         DMLERR_LOW_MEMORY           ,
         DMLERR_MEMORY_ERROR         ,
         DMLERR_NO_CONV_ESTABLISHED  ,
         DMLERR_NOTPROCESSED         ,
         DMLERR_POKEACKTIMEOUT       ,
         DMLERR_POSTMSG_FAILED       ,
         DMLERR_REENTRANCY           ,
         DMLERR_SERVER_DIED          ,
         DMLERR_SYS_ERROR            ,
         DMLERR_UNFOUND_QUEUE_ID     : Exit;
    end;
  end else begin
    Result := True;
  end;
end;

function TDdeBinItem.StopAdvise: Boolean;
var
  ddeRslt: integer; //LongInt;
begin
  result := False;
  if DdeBin = nil then exit;
  if DdeBin.Conv <> 0 then begin
    if hszItem <> 0 then
      DdeClientTransaction(nil, -1, DdeBin.Conv, hszItem, DdeBin.wFmt, XTYP_ADVSTOP, TIMEOUT_ASYNC, @ddeRslt);
  end;
  DdeFreeStringHandle(DdeBin.IdInst, hszItem);
  hszItem := 0;
  Result := True;
end;

procedure TDdeBinItem.DataChange(DdeMsg: HDDEData; hszIt: HSZ);
var DataLen  : DWORD;
    pvalue   : PWORD;
    ddeRslt  : integer; //LongInt;
    DdeDat   : HDDEData;
begin
  if DdeBin = nil then exit;
  if DdeMsg = 0 then begin
    if (DdeBin.Conv <> 0) and (hszItem <> 0) then begin
      DdeDat := DdeClientTransaction(nil, -1, DdeBin.Conv, hszItem,
        DdeBin.wFmt, XTYP_REQUEST, 1000, @ddeRslt);
      if DdeDat = 0 then Exit
      else begin
        pvalue := DdeAccessData(DdeDat, @DataLen);
        if DataLen > size then begin
          CopyMemory(Fdata, pvalue, Fsize);
          if assigned(FOnAdvise) then FOnAdvise(Self);
        end;
        DdeUnaccessData(DdeDat);
        DdeFreeDataHandle(DdeDat);
      end;
    end;
  end;
end;

function TDdeBinItem.PokeData : Boolean;
var
  hszDat : HDDEData;
  hdata  : HDDEData;
begin
  Result := False;
  if DdeBin = nil then exit;
  if (DdeBin.Conv = 0) or (DdeBin.FWaitStat) then Exit;

  hszDat := DdeCreateDataHandle (DdeBin.IdInst, FData, Fsize,
    0, hszItem, DdeBin.wFmt, 0);
  if hszDat <> 0 then begin
    hdata := DdeClientTransaction(Pointer(hszDat), -1, DdeBin.Conv, hszItem,
      DdeBin.wFmt, XTYP_POKE, TIMEOUT_ASYNC, nil);
    Result := hdata <> 0;
  end;
  DdeFreeStringHandle (DdeBin.IdInst, hszItem);
end;

function TDDEBinItem.requestData : boolean;
var
  hData   : HDDEData;
  ddeRslt : DWORD;
  DataLen : integer;
  pvalue  : PWORD;
begin
  Result := False;
  if (DdeBin.Conv = 0) or (DdeBin.FWaitStat) then Exit;
  if hszItem <> 0 then  begin
    hData := DdeClientTransaction(nil, 0, DdeBin.Conv, hszItem, DdeBin.wFmt,
          XTYP_REQUEST, 10000, @ddeRslt);

    if hData <> 0 then
    try
      pvalue := DdeAccessData(hData, @DataLen);
      try
        if DataLen >= Fsize then begin
           CopyMemory(Fdata, pvalue, Fsize);
           result := True;
           if assigned(FOnAdvise) then FOnAdvise(Self);
        end;
      finally  DdeUnaccessData(hData); end;
    finally  DdeFreeDataHandle(hData); end;
  end;
end;

procedure TDdeBinItem.setConversation(conv : TDdeBinClient);
begin
  if FConversation <> nil then FConversation.OnDetach(Self);
  Fconversation := conv;
  if FConversation <> nil then conv.onAttach(Self);
end;

(*
procedure Register;
begin
  RegisterComponents('CNC Utils', [TDdeBinClient, TDdeBinItem]);
end;
*)

end.
