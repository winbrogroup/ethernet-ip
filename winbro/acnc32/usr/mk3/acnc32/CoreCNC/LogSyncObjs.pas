unit LogSyncObjs;

interface

uses SyncObjs;

type
  TLoggingCriticalSection = class(TObject)
  private
    Lock : TCriticalSection;
    Name : string;
  public
    constructor Create(aName : string);
    destructor Destroy; override;
    procedure Enter(const ID : string);
    procedure Leave(const ID : string);
  end;


implementation

uses CoreCNC, CNC32, CNCTypes;

{ TLoggingCriticalSection }

constructor TLoggingCriticalSection.Create(aName : string);
begin
  Lock := TCriticalSection.Create;
  Name := aName;
end;

destructor TLoggingCriticalSection.Destroy;
begin
  Lock.Free;
  inherited;
end;

procedure TLoggingCriticalSection.Enter(const ID : string);
begin
//  EventLogFmt('Entering critical section %s : %s', [Name, ID]);
  Lock.Enter;
//  EventLogFmt('Entered critical section %s : %s', [Name, ID]);
end;

procedure TLoggingCriticalSection.Leave(const ID : string);
begin
//  EventLogFmt('Leaving critical section %s : %s', [Name, ID]);
  Lock.Leave;
end;


end.
