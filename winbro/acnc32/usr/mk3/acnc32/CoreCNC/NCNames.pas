unit NCNames;

interface

uses CNC32, CNCTypes;


type
  TNCEvent = (
         ncdeReqAxisIndex  ,
         ncdeReqAxisSelect ,
         ncdeReqJogModeInc ,
         ncdeReqJogModeCont,
         ncdeReqJogModeMPG ,
         ncdeReqJogModeHome,
         ncdeReqJogIncrement,
         ncdeReqFeedOvrEna ,
         ncdeReqFeedOVR    ,
         ncdeReqRapidOVR   ,
         ncdeReqCycleStart ,
         ncdeReqCycleStop  ,
         ncdeReqCycleAbort ,
         ncdeReqFeedHold   ,
         ncdeReqHomeAxis   ,
         ncdeReqHomeProg   ,
         ncdeReqDryRun     ,
         ncdeReqBlockDelete,
         ncdeReqJogPlus    ,
         ncdeReqJogMinus   ,
         ncdeReqManualMode ,
         ncdeReqAutoMode   ,
         ncdeReqMDIMode    ,
         ncdeReqTeachMode  ,
         ncdeReqEditMode   ,
         ncdeReqProgramLine,
         ncdeReqSingleBlock,
         ncdeReqOptionalStop,
         ncdeReqRewind     ,
         ncdeMCodeAFC      ,
         ncdeTestPoint     ,
         ncdeReqExtFunc1   ,
         ncdeReqExtFunc2   ,
         ncdeReqExtFunc3   ,
         ncdeReqExtFunc4   ,
         ncdeReqExtFunc5   ,
         ncdeReqExtFunc6   ,
         ncdeReqExtFunc7   ,
         ncdeReqExtFunc8   ,
         ncdeReqExtFunc9   ,
         ncdeReqExtFunc10,
         ncdeReqZeroAdjustAxis,
         ncdeReqZeroAdjust,
         ncdeReqHalfZeroAdjustAxis,
         ncdeReqClearZeroAdjustAxis,
         ncdeReqClearZeroAdjust,
         ncdeReqPresetZeroAdjust,
         ncdeReqCopyPosition,
         ncdeMacroExecute,
         ncdeReleaseAxis ,
         ncdeNoCycle
  );


  TNCPublished = (
         ncpActiveAxis  ,
         ncpActiveMode  ,
         ncpInCycle     ,
         ncpInMotion    ,
         ncpAxisInMotion,
         ncpAxisWarnFollowError,
         ncpAxisFatalFollowError,
         ncpAxisOpenLoop,
         ncpAxisPositiveLimit,
         ncpAxisNegativeLimit,
         ncpAxisInPosition,
         ncpAxisHomed   ,
         ncpAxisAmplifierFault,
         ncpEncoderLoss,

         ncpSingleBlock ,
         ncpOptionalStop,
         ncpBlockDelete ,
         ncpFeedHold    ,
         ncpDryRun      ,
         ncpProgramLine ,
         ncpJogMode     ,
         ncpHomed       ,
         ncpFeedOVREna  ,
         ncpFeedOVR     ,
         ncpPartProgram ,
         ncpMDIBuffer   ,
         ncpVectorFeedRate,
         ncpTargetFeedRate,
         ncpAxisName    ,
         ncpJogIncrement,
         ncpMCode       ,
         ncpSCode       ,
         ncpTCode
  );

const
  NC32GCodeSection = 'NC32GCode';

  AxisPositionFormat = 'P%s';

  ZeroOffsetName = '#ZeroOffset';
  ZeroOffsetAdjustName = '#ZeroOffsetAdjust';
  PlusSoftLimitName = '#PlusSoftLimit';
  MinusSoftLimitName = '#MinusSoftLimit';

  HashlessPlusSoftLimitName = 'PlusSoftLimit';
  HashlessMinusSoftLimitName = 'MinusSoftLimit';

  StowNameA = 'StowA';
  StowNameB = 'StowB';
  StowNameC = 'StowC';
  ResultName = '#Result';
  IHWName = '#IHW';
  OHWName = '#OHW';
  GGroupName = '#G';
  HProbeName = 'GEO';
  CMMName = '#CMM';


  NameNC1NormalTermination = 'NC1NormalTermination';
  NameNC1CStartPermissive = 'NC1CStartPermissive';
implementation


end.
