unit GEOPlugin;

interface

uses Windows, SysUtils, CoreCNC, CNC32, IScriptInterface, AbstractScript,NamedPlugin;

type
  TGEOPluginCreate = procedure; stdcall;
  TGEOPluginDestroy = procedure; stdcall;
  TGEOPluginReset = procedure; stdcall;
  TGEOPluginSetAxisPositions = procedure(X,Y,Z,A,B,C : Double); stdcall;
  TGEOPluginSetPoint = procedure(aName : PChar; P1 : TXYZ); stdcall;
  TGEOPluginGetProbeVector = procedure(aName : PChar; A, B, C : Double; var P1, P2 : TXYZ); stdcall;
  TGEOPluginGetPoint = function(aName : PChar; var P1 : TXYZ) : Boolean; stdcall;
  TGEOPluginShow = procedure; stdcall;
  TGEOPluginHide = procedure; stdcall;
  TGEOPluginLoadConfig = procedure(aName : PChar; Sqr, Retry : Integer; Fit : Double); stdcall;
  TGEOPluginSetToolOffset = procedure(P1 : TXYZ); stdcall;
  TGEOPluginProbePositionToMachineRelativePoint = procedure(aNAme : PChar; X,Y,Z,A,B,C : Double);stdcall;
  TGEOPluginMachinePosForPoint = procedure(aNAme : PChar; A, B, C : Double; var XYZ : TXYZ); stdcall;
  TGEOPluginCorrectForStackingAxis = procedure(UseC : Boolean; var X, Y, Z, A, B, C : Double); stdcall;
  TGEOPluginGetStackingAxis = procedure(var P1, P2, P3, P4, P5, P6 : TXYZ); stdcall;
  TGEOPluginSetStackingAxis = procedure(P1, P2, P3, P4, P5, P6 : TXYZ); stdcall;
  TGEOPluginSetPCMStackingAxis = procedure(P1, P2, P3, P4, P5, P6 : TXYZ); stdcall;
  TGEOPluginGetFitQuality = procedure(var Nominal, Mount : array of Double; var Max : Integer); stdcall;
  TGEOPluginNDPReset = procedure; stdcall;
  TGEOPluginSetNDPOffset = procedure(Index: Integer; Offset: TXYZ; A, B, C: Double); stdcall;
  TGEOPluginSetNDPVector = function(P1, P2: TXYZ): Integer; stdcall;
  TGEOPluginGetMachineNDP = procedure(CRot: Double; var ResLinear, ResRotary: TXYZ; Parm1, Parm2: Integer); stdcall;
//  TGEOPluginGetGTMachineNDP = procedure(FixedRotary: TXYZ;AxArray: TXYZ; var ResLinear, ResRotary: TXYZ; Parm1, Parm2: Integer); stdcall;
  TGEOPluginSetToolApproach = procedure (I, J, K: Double); stdcall;
  TGEOPluginBeginUpdate = procedure; stdcall;
  TGEOPluginEndUpdate = procedure; stdcall;
  TGEOPluginSetPCMState = procedure (P: PChar) stdcall;
  TGEOPluginGetPCMState = procedure (P: PChar; MaxLen: Integer) stdcall;

  TGEOPlugin = class(TObject)
  private
    GEOPluginCreate : TGEOPluginCreate;
    GEOPluginDestroy : TGEOPluginDestroy;
    GEOPluginSetAxisPositions :TGEOPluginSetAxisPositions;
    GEOPluginSetProbePoint : TGEOPluginSetPoint;
    GEOPluginGetProbeVector : TGEOPluginGetProbeVector;
    GEOPluginShow : TGEOPluginShow;
    GEOPluginHide : TGEOPluginHide;
    GEOPluginLoadConfig : TGEOPluginLoadConfig;
    GEOPluginReset : TGEOPluginReset;
    GEOPluginAddPartRelativePoint : TGEOPluginSetPoint;
    GEOPluginAddToolRelativePoint : TGEOPluginSetPoint;
    GEOPluginAddMachineRelativePoint : TGEOPluginSetPoint;
    GEOPluginSetToolOffset : TGEOPluginSetToolOffset;
    GEOPluginGetPoint : TGEOPluginGetPoint;
    GEOPluginProbePositionToMachineRelativePoint : TGEOPluginProbePositionToMachineRelativePoint;
    GEOPluginMachinePosForPoint : TGEOPluginMachinePosForPoint;
    GEOPluginCorrectForStackingAxis : TGEOPluginCorrectForStackingAxis;
    GEOPluginSetStackingAxis : TGEOPluginSetStackingAxis;
    GEOPluginSetPCMStackingAxis : TGEOPluginSetStackingAxis;
    GEOPluginGetStackingAxis : TGEOPluginGetStackingAxis;
    GEOPluginGetFitQuality : TGEOPluginGetFitQuality;
    GEOPluginNDPReset: TGEOPluginNDPReset;
    GEOPluginSetNDPOffset: TGEOPluginSetNDPOffset;
    GEOPluginSetNDPVector: TGEOPluginSetNDPVector;
    GEOPluginGetMachineNDP: TGEOPluginGetMachineNDP;
//    GEOPluginGetGTMachineNDP: TGEOPluginGetGTMachineNDP;

    GEOPluginSetToolApproach: TGEOPluginSetToolApproach;
    GEOPluginBeginUpdate: TGEOPluginBeginUpdate;
    GEOPluginEndUpdate: TGEOPluginEndUpdate;

    GEOPluginSetPCMState: TGEOPluginSetPCMState;
    GEOPluginGetPCMState: TGEOPluginGetPCMState;
    FDataTransfer : TTriDoubleArrayTag;
    function GetProcAddressX(hModule: HMODULE; lpProcName: LPCSTR): FARPROC;
    function GetProcAddressTest(hModule: HMODULE; lpProcName: LPCSTR): FARPROC;
  public
    constructor Create(HLib : HModule);
    destructor Destroy; override;
    procedure SetAxisPosition(X, Y, Z, A, B, C : Double);
    procedure SetProbePoint(aName : string; const P : TXYZ);
    procedure GetProbeVector(aName : string; A, B, C : Double; var P1, P2 : TXYZ);
    procedure LoadConfig(aConfig : string; Sqr, Retry : Integer; Fit : Double);
//    procedure LoadConfig(aConfig : string);
    procedure AddPartRelativePoint(aName : string; const P1 : TXYZ);
    procedure AddToolRelativePoint(aName : string; const P1 : TXYZ);
    procedure AddMachineRelativePoint(aName : string; const P1 : TXYZ);
    function GetPoint(aName : string; var P1 : TXYZ) : Boolean;
    procedure ProbePositionToMachineRelativePoint(aName : string; X, Y, Z, A, B, C : Double);
    procedure MachinePosForPoint(aName : string; A, B, C : Double; var XYZ : TXYZ);
    procedure CorrectForStackingAxis(UseC : Boolean; var X, Y, Z, A, B, C : Double);
    procedure SetStackingAxis(P1, P2, P3, P4, P5, P6 : TXYZ);
    procedure SetPCMStackingAxis(P1, P2, P3, P4, P5, P6 : TXYZ);
    procedure GetStackingAxis(var P1, P2, P3, P4, P5, P6 : TXYZ);
    procedure NDPReset;
    procedure SetNDPOffset(Index: Integer; const Offset: TXYZ; A, B, C: Double);
    function SetNDPVector(const P1, P2: TXYZ): Integer;
    procedure GetMachineNDP(CRot: Double; var ResLinear, ResRotary: TXYZ; Parm1, Parm2: Integer);
//    procedure GetGTMachineNDP(FixedRots : TXYZ;AxArray: TXYZ; var ResLinear, ResRotary: TXYZ; Parm1, Parm2: Integer);
    procedure SetToolApproach(I, J, K: Double);

    procedure BeginUpdate;
    procedure EndUpdate;

    procedure Reset;
    procedure Show;
    procedure Hide;
    procedure SetToolOffset(P1 : TXYZ);
    procedure GetFitQuality(var Nominal, Mount : array of Double; var Max : Integer);
    property DataTransfer : TTriDoubleArrayTag read FDataTransfer write FDataTransfer;

    procedure SetPCMState(state: string);
    function GetPCMState: string;
  end;

implementation

function TGEOPlugin.GetProcAddressX(hModule: HMODULE; lpProcName: LPCSTR): FARPROC;
begin
  Result := GetProcAddress(hModule, lpProcName);
  if not Assigned(Result) then
    raise Exception.CreateFmt('GEO does not contain method "%s"', [lpProcName]);
end;


function TGEOPlugin.GetProcAddressTest(hModule: HMODULE; lpProcName: LPCSTR): FARPROC;
begin
  Result := GetProcAddress(hModule, lpProcName);
  if not Assigned(Result) then
    EventLogFmt('GEO does not contain method [%s]', [lpProcName]);
end;


constructor TGEOPlugin.Create(HLib : HModule);
begin
  GEOPluginCreate := GetProcAddressX(HLib, 'GEOPluginCreate');
  GEOPluginDestroy := GetProcAddressX(HLib, 'GEOPluginCreate');
  GEOPluginSetAxisPositions := GetProcAddressX(HLib,'GEOPluginSetAxisPositions');
  GEOPluginSetProbePoint := GetProcAddressX(HLib,'GEOPluginSetProbePoint');
  GEOPluginGetProbeVector := GetProcAddressX(HLib,'GEOPluginGetProbeVector');
  GEOPluginHide := GetProcAddressX(HLib, 'GEOPluginHide');
  GEOPluginShow := GetProcAddressX(HLib, 'GEOPluginShow');
  GEOPluginLoadConfig := GetProcAddressX(HLib, 'GEOPluginLoadConfig');
  GEOPluginReset := GetProcAddressX(HLib, 'GEOPluginReset');
  GEOPluginAddPartRelativePoint := GetProcAddressX(HLib, 'GEOPluginAddPartRelativePoint');
  GEOPluginAddToolRelativePoint := GetProcAddressX(HLib, 'GEOPluginAddToolRelativePoint');
  GEOPluginAddMachineRelativePoint := GetProcAddressX(HLib, 'GEOPluginAddMachineRelativePoint');
  GEOPluginSetToolOffset := GetProcAddressX(HLib, 'GEOPluginSetToolOffset');
  GEOPluginGetPoint := GetProcAddressX(HLib, 'GEOPluginGetPoint');
  GEOPluginProbePositionToMachineRelativePoint := GetProcAddressX(HLib, 'GEOPluginProbePositionToMachineRelativePoint');
  GEOPluginMachinePosForPoint := GetProcAddressX(HLib, 'GEOPluginMachinePosForPoint');
  GEOPluginCorrectForStackingAxis := GetPRocAddressX(HLib, 'GEOPluginCorrectForStackingAxis');
  GEOPluginSetStackingAxis := GetProcAddressX(HLib, 'GEOPluginSetStackingAxis');
  GEOPluginSetPCMStackingAxis := GetProcAddressTest(HLib, 'GEOPluginSetPCMStackingAxis');
  GEOPluginGetStackingAxis := GetProcAddressX(HLib, 'GEOPluginGetStackingAxis');
  GEOPluginGetFitQuality := GetProcAddressX(HLib, 'GEOPluginGetFitQuality');

  GEOPluginNDPReset:= GetProcAddressTest(HLib, 'GEOPluginNDPReset');
  GEOPluginSetNDPOffset:= GetProcAddressTest(HLib, 'GEOPluginSetNDPOffset');
  GEOPluginSetNDPVector:= GetProcAddressTest(HLib, 'GEOPluginSetNDPVector');
  GEOPluginGetMachineNDP:= GetProcAddressTest(HLib, 'GEOPluginGetMachineNDP');
//  GEOPluginGetGTMachineNDP:= GetProcAddressTest(HLib, 'GEOPluginGetGTMachineNDP');
  GEOPluginSetToolApproach:= GetProcAddressTest(HLib, 'GEOPluginSetToolApproach');

  GEOPluginBeginUpdate:= GetProcAddressTest(HLib, 'GEOPluginBeginUpdate');
  GEOPluginEndUpdate:= GetProcAddressTest(HLib, 'GEOPluginEndUpdate');
  GEOPluginSetPCMState:= GetProcAddressTest(HLib, 'GEOPluginSetPCMState');
  GEOPluginGetPCMState:= GetProcAddressTest(HLib, 'GEOPluginGetPCMState');

  GEOPluginCreate;
end;

destructor TGEOPlugin.Destroy;
begin
  if Assigned(GEOPluginDestroy) then
    GEOPluginDestroy;
end;


procedure TGEOPlugin.SetAxisPosition(X, Y, Z, A, B, C : Double);
begin
  GEOPluginSetAxisPositions(X, Y, Z, A, B, C);
end;

procedure TGEOPlugin.SetProbePoint(aName : string; const P : TXYZ);
begin
  GEOPluginSetProbePoint(PChar(aName), P);
end;

procedure TGEOPlugin.GetProbeVector(aName : string; A, B, C : Double; var P1, P2 : TXYZ);
begin
  GEOPluginGetProbeVector(PChar(aName), A, B, C, P1, P2);
end;

procedure TGEOPlugin.LoadConfig(aConfig : string; Sqr, Retry : Integer; Fit : Double);
begin
  GEOPluginLoadConfig(PChar(aConfig), Sqr, Retry, Fit);
end;

procedure TGEOPlugin.Reset;
begin
  GEOPluginReset;
end;

procedure TGEOPlugin.AddPartRelativePoint(aName : string; const P1 : TXYZ);
begin
  GEOPluginAddPartRelativePoint(PChar(aName), P1);
end;

procedure TGEOPlugin.AddToolRelativePoint(aName : string; const P1 : TXYZ);
begin
  GEOPluginAddToolRelativePoint(PChar(aName), P1);
end;

procedure TGEOPlugin.AddMachineRelativePoint(aName : string; const P1 : TXYZ);
begin
  GEOPluginAddMachineRelativePoint(PChar(aName), P1);
end;

procedure TGEOPlugin.SetToolOffset(P1 : TXYZ);
begin
  GEOPluginSetToolOffset(P1);
end;

function TGEOPlugin.GetPoint(aName : string; var P1 : TXYZ) : Boolean;
begin
  Result := GEOPluginGetPoint(PChar(aName), P1);
end;

procedure TGEOPlugin.ProbePositionToMachineRelativePoint(aName : string; X, Y, Z, A, B, C : Double);
begin
  GEOPluginProbePositionToMachineRelativePoint(PChar(aName), X, Y, Z, A, B, C);
end;

procedure TGEOPlugin.MachinePosForPoint(aName : string; A, B, C : Double; var XYZ : TXYZ);
begin
  Self.GEOPluginMachinePosForPoint(PChar(aName), A, B, C, XYZ);
end;

procedure TGEOPlugin.CorrectForStackingAxis(UseC : Boolean; var X, Y, Z, A, B, C : Double);
begin
  GEOPluginCorrectForStackingAxis(UseC, X, Y, Z, A, B, C);
end;

procedure TGEOPlugin.GetStackingAxis(var P1, P2, P3, P4, P5, P6: TXYZ);
begin
  GEOPluginGetStackingAxis(P1, P2, P3, P4, P5, P6);
end;

procedure TGEOPlugin.SetStackingAxis(P1, P2, P3, P4, P5, P6: TXYZ);
begin
  GEOPluginSetStackingAxis(P1, P2, P3, P4, P5, P6);
end;

procedure TGEOPlugin.SetPCMStackingAxis(P1, P2, P3, P4, P5, P6: TXYZ);
begin
  if Assigned(GEOPluginSetPCMStackingAxis) then
    GEOPluginSetPCMStackingAxis(P1, P2, P3, P4, P5, P6)
  else
    EventLog('GEO does not contain the method GEOPluginSetPCMStackingAxis');
end;


procedure TGEOPlugin.GetFitQuality(var Nominal, Mount : array of Double; var Max : Integer);
begin
  GEOPluginGetFitQuality(Nominal, Mount, Max);
end;

procedure TGEOPlugin.Hide;
begin
  GEOPluginHide;
end;

procedure TGEOPlugin.Show;
begin
  GEOPluginShow;
end;

function TGEOPlugin.SetNDPVector(const P1, P2: TXYZ): Integer;
begin
  if Assigned(GEOPluginSetNDPVector) then begin
    Result := GEOPluginSetNDPVector(P1, P2);
  end else begin
    raise Exception.Create('GEO Plugin does not support function GEOSetNDPVector');
  end;
end;

{procedure TGEOPlugin.GetGTMachineNDP(FixedRots : TXYZ;AxArray: TXYZ; var ResLinear, ResRotary: TXYZ; Parm1, Parm2: Integer);
begin
  if Assigned(GEOPluginGetGTMachineNDP) then begin
    GEOPluginGetGTMachineNDP(FixedRots, AxArray, ResLinear, ResRotary, Parm1, Parm2);
  end else begin
    raise Exception.Create('GEO Plugin does not support function GEOGetGTMachineNDP');
  end;
end;}

procedure TGEOPlugin.GetMachineNDP(CRot: Double; var ResLinear, ResRotary: TXYZ; Parm1, Parm2: Integer);
begin
  if Assigned(GEOPluginGetMachineNDP) then begin
    GEOPluginGetMachineNDP(CRot, ResLinear, ResRotary, Parm1, Parm2);
  end else begin
    raise Exception.Create('GEO Plugin does not support function GEOGetMachineNDP');
  end;
end;

procedure TGEOPlugin.NDPReset;
begin
  if Assigned(GEOPluginNDPReset) then begin
    GEOPluginNDPReset;
  end else begin
    raise Exception.Create('GEO Plugin does not support function GEONDPReset');
  end;
end;

procedure TGEOPlugin.SetNDPOffset(Index: Integer; const Offset: TXYZ; A, B, C: Double);
begin
  if Assigned(GEOPluginSetNDPOffset) then begin
    GEOPluginSetNDPOffset(Index, Offset, A, B, C);
  end else begin
    raise Exception.Create('GEO Plugin does not support function GEOSetNDPOffset');
  end;
end;

procedure TGEOPlugin.SetToolApproach(I, J, K: Double);
begin
  if Assigned(GEOPluginSetToolApproach) then begin
    GEOPluginSetToolApproach(I, J, K);
  end else begin
    raise Exception.Create('GEO Plugin does not support function GEOSetToolApproach');
  end;
end;

procedure TGEOPlugin.BeginUpdate;
begin
  if Assigned(GEOPluginBeginUpdate) then
    GEOPluginBeginUpdate;
end;

procedure TGEOPlugin.EndUpdate;
begin
  if Assigned(GEOPluginEndUpdate) then
    GEOPluginEndUpdate;
end;

function TGEOPlugin.GetPCMState: string;
var Buffer: array [0..1024] of Char;
begin
  GEOPluginGetPCMState(Buffer, 1024);
  Result:= Buffer;
end;

procedure TGEOPlugin.SetPCMState(state: string);
begin
  GEOPluginSetPCMState(PChar(state));
end;

end.
