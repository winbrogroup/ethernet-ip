unit CMMFileV1;

interface

uses SysUtils, Classes, StreamTokenizer, CoreCNC;

{  CMMFile V1.0
   Op=1 X=  0.081 Y= -0.097 Z= -0.295  A=  0.103 B=  0.206 C= -0.047
   Op=19 X= -0.065 Y=  0.080 Z=  0.085 A=  0.264 B= -0.019 C= -0.125
 }
type
  TCMMFile = class(TObject)
  private
//    procedure ClearOffsets;
  public
//    Offsets : array [0..31, 0..5] of Double;
    Offsets : TGenericDoubleArrayTag;
    procedure Read(aFileName : string);
  end;

implementation

procedure TCMMFile.Read(aFileName : string);
var S : TStreamTokenizer;
    Token : string;

  procedure ReadLine;
  var Index, Ax : Integer;
  begin
    Index := S.ReadIntegerAssign mod Offsets.ArrayLength;
    Token := LowerCase(S.Read);
    while (Token <> 'op') and (not S.EndOfStream) do begin
      if Length(Token) <> 1 then
        raise Exception.Create('Invalid axis name ' + Token);
      case Token[1] of
        'x' : Ax := 0;
        'y' : Ax := 1;
        'z' : Ax := 2;
        'a' : Ax := 3;
        'b' : Ax := 4;
        'c' : Ax := 5;
      else
        raise Exception.Create('Invalid axis name ' + Token);
      end;
      Offsets.SetDoubleValue([Index, Ax], S.ReadDoubleAssign);
      Token := LowerCase(S.Read);
    end;
  end;

begin
 S := TStreamTokenizer.Create;
 try
   S.Seperator := '''[]=';
   try
     S.SetStream(TFileStream.Create(aFileName, fmOpenRead));
   except
     raise Exception.Create('Cant find file ' + aFileName);
   end;

   S.ExpectedTokens(['CMMFile', 'V1.0']);
   S.ExpectedToken('op');
   while not S.EndOfStream do begin
     ReadLine
   end;

 finally
   S.Free;
 end;
end;

{procedure TCMMFile.ClearOffsets;
var I, J : Integer;
begin
  for I := 0 to 31 do
    for J := 0 to 5 do
      Offsets.SetDoubleValue([I, J], 0);
end;}

end.
