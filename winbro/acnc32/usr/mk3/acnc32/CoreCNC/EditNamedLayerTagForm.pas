unit EditNamedLayerTagForm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  CoreCNC, CNC32, CNCTypes, StdCtrls, Buttons, ExtCtrls, CheckLst, ComCtrls;

type
  TEditNamedLayerTag = class(TForm)
    Panel1: TPanel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    GroupBox1: TGroupBox;
    EditMemo: TMemo;
    GroupBox2: TGroupBox;
    ListView: TListView;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    FNLTag : TAbstractTag;
    procedure SetNLTag(aNLTag : TAbstractTag);
  public
    class procedure Execute(aTag : TAbstractTag);
    property NLTag : TAbstractTag read FNLTag write SetNLTag;
  end;

implementation

{$R *.DFM}

var
  EditNamedLayerTag: TEditNamedLayerTag;

class procedure TEditNamedLayerTag.Execute(aTag : TAbstractTag);
begin
  if not Assigned(aTag) then
    Exit;

  EditNamedLayerTag := TEditNamedLayerTag.Create(SysObj.VisualParent);
  EditNamedLayerTag.Caption := aTag.Name;
  EditNamedLayerTag.NLTag := aTag;
  EditNamedLayerTag.Font.Name := SysObj.FontName;
  EditNamedLayerTag.Font.Size := 11;
  EditNamedLayerTag.Font.Style := [fsBold];
  SysObj.NC.AcquireModeChangeLock;
  try
    EditNamedLayerTag.ShowModal;
  finally
    SysObj.NC.ReleaseModeChangeLock;
    EditNamedLayerTag.Free;
  end;
end;

procedure TEditNamedLayerTag.SetNLTag(aNLTag : TAbstractTag);
  procedure AddListRow(const aProp, aValue : string);
  var LI : TListItem;
  begin
    LI := ListView.Items.Add;
    LI.Caption := aProp;
    LI.SubItems.Add(aValue);
  end;

begin
  FNLTag := aNLTag;
  EditMemo.ReadOnly := (not ((NLTag is TMethodTag) or
                             (NLTag is TStringTag) or
                             (NLTag is TDoubleTag))) or
                        (NLTag.IsOwned);

  EditMemo.Lines.Text := NLTag.AsString;
  AddListRow('IsVCL', BooleanIdent[NLTag.IsVCL]);
  AddListRow('IsSystem', BooleanIdent[NLTag.IsSystem]);
  AddListRow('IsWritable', BooleanIdent[NLTag.IsWritable]);
  AddListRow('HasValue', BooleanIdent[NLTag.HasValue]);
  AddListRow('IsConstant', BooleanIdent[NLTag.IsConstant]);
  AddListRow('HasInterlock', BooleanIdent[NLTag.HasInterlock]);
  AddListRow('IsOwned', BooleanIdent[NLTag.IsOwned]);
  AddListRow('IsConnected', BooleanIdent[NLTag.IsConnected]);
  AddListRow('IsEvented', BooleanIdent[NLTag.IsEvented]);
  AddListRow('IsIO', BooleanIdent[NLTag.IsIO]);
  AddListRow('IsPersistent', BooleanIdent[NLTag.IsPersistent]);
  AddListRow('OK', BooleanIdent[NLTag.OK]);
  AddListRow('Owner', NLTag.Owner.Name);
  AddListRow('Callbacks', IntToStr(NLTag.Count));
//  AddListRow('ClientLinks', IntToStr(Ord(NLTag.ClientLinks)));
  AddListRow('Size', IntToStr(NLTag.DataSize));
  AddListRow('InstanceSize', IntToStr(NLTag.InstanceSize));
  AddListRow('ClassName', NLTag.ClassName);
  AddListRow('ClassParentName', NLTag.ClassParent.ClassName);
end;

procedure TEditNamedLayerTag.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  if not EditMemo.ReadOnly and (ModalResult = mrOK) then begin
    NLTag.AsString := EditMemo.Lines.Text;
  end;
end;

end.
 