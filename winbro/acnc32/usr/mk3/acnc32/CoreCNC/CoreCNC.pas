{*************************************************************************
 *
 *  Focus Software Development 2000
 *
 *  Title :  CoreCNC
 *  CSPEC :
 *  Description :  These are the core components of the Advanced CNC.  These
 *                 components give a named layer to IO and memory services,
 *                 also to fulfill the requirements General purpose CNC a
 *
 * Copyright (c) 2000 Focus Software Development
 *
 * Originated    Pete Davis 9/97 Transtec Advanced Machines
 *
 * Revised  Date     Author     Reason
 * 01       2/98     PAD        Multithreading support for IO refer to
 *                              TAbstractIO.NVCLregisterEvent and
 * TCNC.registerIOEvent for an explanation.
 *
 * 02       3/98     PAD        Increased the namedLayer functionallity to
 *                              include pointers. Refer to
 * TAbstractMemory.publishEx. Currently the data is passed by pointer reference
 * in the read and callback methods, this I believe will be unacceptable and may
 * be changed to a method in later revision.
 *
 * 03       3/98     PAD        As refered above the TAbstractMemory.readEx is
 *                              now included for Pointers to namedLayer objects.
 *
 * 04       4/98     PAD        The AbstractMemory has been expanded to allow
 *                              TIOMethod this enables multiple
 *
 * 05       6/98     PAD/MGL    Changed installComponent in TCNC to utilize
 *                              TParamStrings.
 *
 * 06       6/98     PAD        Mcodes are assumed to use a separate interface
 *                              from the abstractNC. The mechanism must place
 *                              Each of the required data straight into the Io
 *                              System.
 *
 *
 * 07      01/99     PAD        Changed NamedLayer from array to list of TAbstractTag
 *                              Derivatives. This entailed Dumping ReadEx / WriteEx
 *                              PublishEx (Thank god). TCallback merely returns the
 * Requested tag.  DualPublished Items will have the same tag existant in both lists
 * All members of TAbstractIO.NamedLayer must be derived from TIOTag. Because the
 * Publisher Creates the Tag the act of publishing must occur before Registering
 * any event, this means that Event registering must occur after inheriting
 * installed and publishing is assumed to of occured between InstallComponent and
 * Installed.
 *
 * 08      2/99       PAD        Reconfigured IO System to work as
 *                               IO.SubSystem.Device.Connection
 *
 * 09     10/00       PAD        Combined TAbstractMemory and TAbstractIO in
 *                               to TAbstractCommunications.
 *
 * 10     10/00       PAD        Taken all global values in corecnc and wrapped
 *                               them up in to TSystemObject.  This should make
 *                               the writing of Dll's easier.
 *
 * If it were painless what would all the masochists do.
 **************************************************************************}


unit CoreCNC;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  IniFiles, ExtCtrls, StdCtrls, Buttons, Named, CNCTypes, CNC32, {NCConsts,}
  Tokenizer, NCNames, Registry, CompoundFiles, JPeg, CNCRegistry,
  IFSDServer, INamedInterface, FaultRegistration, IToolLife, IOCT, DynamicHelp,
  StringArray, IStringArray, INCInterface, SyncObjs, StreamTokenizer,
  IScriptInterface, PSDLoader, ThreeLineButton, Menus, ISignature,
  ParsingIniFile, INCParser, IPlc, JNI;

type
{ Forward class refenerence }
  TCNC                    = class;
  TAbstractNC             = class;
  TAbstractLayoutManager  = class;
  TAbstractSoftkey        = class;
  TAbstractFaultInterface = class;
  TAbstractPLC            = class;
//  TIntrospection          = class;

  TGenericTag = class(TAbstractTag)
  protected
    function GetDataPtr : Pointer; override;
    function GetDataSize : Integer; override;
    function GetIOSize : Integer; override;
    function GetAsInteger : Integer; override;
    function GetAsBoolean : Boolean; override;
    function GetAsDouble : Double; override;
    function GetAsString : string; override;
    function GetAsOleVariant : OleVariant; override;
    procedure SetAsInteger(aValue : Integer); override;
    procedure SetAsBoolean(aValue : Boolean); override;
    procedure SetAsDouble(aValue : Double); override;
    procedure SetAsString(const aValue : string); override;
    procedure SetAsOleVariant(const V : OleVariant); override;
  public
    constructor Create(aOwner : TTagOwner; aName : string); override;
    class function Duplicate(aTag : TAbstractTag) : TAbstractTag;
    procedure Refresh; override;
    procedure Assign(aTag : TAbstractTag); override;
  end;

  TMethodTag = class(TGenericTag)
  private
    FValue : Integer;
  protected
    function GetDataPtr : Pointer; override;
    function GetDataSize : Integer; override;
    function GetIOSize : Integer; override;
    function GetAsInteger : Integer; override;
    function GetAsBoolean : Boolean; override;
    procedure SetAsInteger(aValue : Integer); override;
    procedure SetAsBoolean(aValue : Boolean); override;
  public
    constructor Create(aOwner : TTagOwner; aName : string); override;
    procedure Refresh; override;
    procedure Assign(aTag : TAbstractTag); override;
    property Value : Integer read FValue write FValue;
  end;

  //
  // Notes on IOSystem
  // The IOSystem will refresh from hardware any tag that is declared un owned
  // If any low level ( Plc, Diagnostic ) device requires a tag for a period
  // of time (for writing), then it should check ownership prior to setting
  // ownership and finally release ownership.
  // This prevents the IOSystem overwriting this device and signals other potential
  // Clients of a conflict.
  //
  // Aliasing will be added.  This allows a large IOConnection to hold multiple
  // smaller IO Connections with a minumum of conflict.
  TIntegerTag = class(TMethodTag)
  protected
    function GetIOSize : Integer; override;
    function GetDataSize : Integer; override;
  public
    Mask : Byte;
    Device : TAbstractIODevice;
    AliasedNotUsedYet : TIntegerTag; // Will Revisit
    IOTagSize : Integer;
    Colour : TColor;
    FAddress : Integer;
    constructor Create(aOwner : TTagOwner; aName : string); override;
    procedure ReadFromDevice;
    procedure WriteToDevice;
    function  AliasOf(aTag : TIntegerTag) : Boolean;
    function  TextDefinition : string;
    function  OK : Boolean; override;
    function GetAddress : Integer; override;
    procedure SetAddress(aAddress : Integer); override;
  end;


  TStringTag = class(TGenericTag)
  private
    Value : string;
  protected
    function GetIOSize : Integer; override;
    function GetDataPtr : Pointer; override;
    function GetDataSize : Integer; override;
    function GetAsDouble : Double; override;
    function GetAsString : string; override;
    function GetAsInteger : Integer; override;
    function GetAsBoolean : Boolean; override;
    procedure SetAsString(const aValue : string); override;
  public
    constructor Create(aOwner : TTagOwner; aName : string); override;
    procedure ReadFromBuf(var Buf; aLength : Integer); override;
    procedure Assign(aTag : TAbstractTag); override;
  end;

  TDoubleTag = class(TGenericTag)
  private
  protected
    function GetIOSize : Integer; override;
    function GetDataPtr : Pointer; override;
    function GetDataSize : Integer; override;
    function GetAsInteger : Integer; override;
    function GetAsDouble : Double; override;
    procedure SetAsDouble(aValue : Double); override;
  public
    Value : Double;
    constructor Create(aOwner : TTagOwner; aName : string); override;
    procedure Assign(aTag : TAbstractTag); override;
  end;

  TFloatTag = class(TDoubleTag)
  private
  protected
  public
    constructor Create(aOwner : TTagOwner; aName : string); override;
    procedure Refresh; override;
  end;

  TOrdinalTag = class(TIntegerTag)
  private
  protected
  public
    constructor Create(aOwner : TTagOwner; aName : string); override;
    procedure Refresh; override;
  end;

  TAlphaTag = class(TStringTag)
  private
  public
    procedure Refresh; override;
  end;

  TArrayOrdinate = 0..6;
  TArrayDims = array [TArrayOrdinate] of Integer;

  PString = ^string;
  PArrayUnion = ^TArrayUnion;
  TArrayUnion = record
    case Integer of
      varInteger : (
                  IntegerArray : array [0..8195] of Integer;
      );

      varDouble : (
                  DoubleArray : array [0..8195] of Double;
      );

      varString : (
                  StringArray : array [0..8195] of Pstring;
      );

      varByRef : (
        PData : Pointer;
      );
  end;

  TArrayTag = class(TGenericTag)
  protected
    FData : PArrayUnion;
    FOrdinateCount : Integer;
    CurrentConvert : Double;
    Dims: TArrayDims;
    ItemSize : Integer;
    FItemClass : TTagClass;
    function GetDataPtr : Pointer; override;
    function TotalItemCount : Integer;
    function GetArrayBoolean(aOrd : Integer) : Boolean;
    function GetArrayInteger(aOrd : Integer) : Integer;
    function GetArrayDouble(aOrd : Integer) : Double;
    function GetArrayString(aOrd : Integer) : string;
    function GetDimensions(Index : TArrayOrdinate) : Integer;
    procedure SetArrayBoolean(aOrd : Integer; aValue : Boolean);
    procedure SetArrayInteger(aOrd : Integer; aValue : Integer);
    procedure SetArrayDouble(aOrd : Integer; aValue : Double);
    procedure SetArrayString(aOrd : Integer; aValue : string);
    function GetAsString : string; override;
    function GetAsShortString : string; override;
    function GetArrayLength : Integer;
  public
    procedure ChangeUnits(aConvert : Double; Axis : Boolean = True);
    function IndexOfOrdinate(aOrd : Integer) : Integer;
    function IndexOfOrdinates(const aCoords : array of Integer) : Integer;
//    procedure ClearArray;

    procedure ArrayAssign(aOrd : Integer; aTag : TAbstractTag); virtual;
    property AsArrayBoolean[aOrd : Integer] : Boolean read GetArrayBoolean write SetArrayBoolean;
    property AsArrayInteger[aOrd : Integer] : Integer read GetArrayInteger write SetArrayInteger;
    property AsArrayString[aOrd : Integer] : string read GetArrayString write SetArrayString;
    property AsArrayDouble[aOrd : Integer] : Double read GetArrayDouble write SetArrayDouble;
    property OrdinateCount : Integer read FOrdinateCount;
    property ItemClass : TTagClass read FItemClass;
    property Dimensions[Index : TArrayOrdinate] : Integer read GetDimensions;
    property Data : PArrayUnion read FData;
    property ArrayLength : Integer read GetArrayLength;
    function GetDoubleValue(const aDims : array of Integer) : Double;
    procedure SetDoubleValue(const aDims : array of Integer; Value : Double);
  end;


  TArrayAccessTag = class(TGenericTag)
  protected
    function GetAsInteger : Integer; override;
    function GetAsBoolean : Boolean; override;
    function GetAsDouble : Double; override;
    function GetAsString : string; override;
    procedure SetAsInteger(aValue : Integer); override;
    procedure SetAsDouble(aValue : Double); override;
    procedure SetAsBoolean(aValue : Boolean); override;
    procedure SetAsString(const aValue : string); override;
  public
    Index : TAbstractTag;
    DataSource : TArrayTag;
    procedure Assign(aTag : TAbstractTag); override;
    procedure Refresh; override;
  end;

  TCNCkeys = record
    Name, Func : string;
    Key : TAbstractTag;
    Keyword : string;
    Enabled : Boolean;
  end;

  PTKeyArray = ^TKeyArray;
  TKeyArray = array [TFunctionKey] of TCNCKeys;

  PTKeyLinkedList = ^TKeyLinkedList;
  TKeyLinkedList = record
    keys   : TKeyArray;
    handle : word;
    next   : PTKeyLinkedList;
  end;


  TParamStrings = class(TStringList)
  private
    function GetDelimitedText(Delimiter: Char): String;
    procedure SetDelimitedText(Delimiter: Char; const Value: String);
  public
    function ReadBool       (const Ident: string; Default: Boolean): Boolean;
    function ReadInteger    (const Ident: string; Default: Longint): Longint;
    function ReadString     (const Ident, Default: string): string;
    function ReadColour     (const Ident : string; Default: TColor) : TColor;
    function ReadDouble     (const Ident : string; Default: double) : double;
    function ReadIOSystem   (const Ident : string) : TGenericIOSubSystem;
    function ReadIODevice   (const Ident : string; aSubSystem : TGenericIOSubSystem) : TAbstractIODevice;
    procedure ReadIOConnection(const Ident : string; aTag : TIntegerTag; Input : boolean);
    function ReadKeys : TKeyArray;
    function ReadPrefixKeys(const Prefix : string) : TKeyArray;
    property DelimitedText[Delimiter: Char]: String read GetDelimitedText write SetDelimitedText;
  end;

  TAbstractCNCClass = class of TAbstractCNC;
  TAbstractCNC = class(TTagOwner, IHTTPServerComponent)
  protected
    FinstallState : TinstallState;
    procedure DoEndDrag(Target: TObject; X, Y: Integer); override;
  public
    procedure InstallComponent(Params : TParamStrings); dynamic; abstract;
    procedure Installed; dynamic; abstract;
    procedure CheckInstallation; dynamic; abstract;
    function  IShutdownPermissive : boolean; dynamic; abstract;
    procedure IShutdown; dynamic; abstract;
    property InstallState : TInstallState read FinstallState;
    { IHTTPServerComponent }
    function HTTPGeneratePage(HFile: THANDLE; var CGIPath : WideString; CGIRequest: ICGIStringList): Boolean; virtual; stdcall;
    function HTTPAddStreaming(Stream: THAndle; CGIR : WideString; Method : IHTTPServer; DT : Integer): Boolean; virtual; stdcall;
    procedure HTTPRemoveStreaming(Stream: THandle; CGIR: WideString); virtual; stdcall;
  end;

  TTagDragObject = class(TDragObject)
  private
    FControl: TAbstractCNC;
  protected
    procedure Finished(Target: TObject; X, Y: Integer; Accepted: Boolean); override;
  public
    TTag : TAbstractTag;
    constructor Create(AControl: TAbstractCNC);
    property Control: TAbstractCNC read FControl;
  end;

  TAxisPositionTag = class(TArrayTag)
  private
  protected
    function GetDataSize : Integer; override;
    function GetTypeValue(Index : TPositionType) : Double;
    procedure SetTypeValue(Index : TPositionType; aValue : Double);
  public
    FValue : TAxisPosition;
    constructor Create(aOwner : TTagOwner; aName : string); override;
    procedure Write(const aValue : TAxisPosition);
    function AsAxisPosition : TAxisPosition;
    property TypeValue[Index : TPositionType] : Double read GetTypeValue write SetTypeValue;
  end;

  TUnaryDoubleArrayTag = class(TArrayTag)
  private
  protected
    function GetDataSize : Integer; override;
  public
    constructor Create(aOwner : TTagOwner; aName : string); override;
    destructor Destroy; override;
    procedure SetElementCount(aCount : Integer);
  end;

  TTriDoubleArrayTag = class(TUnaryDoubleArrayTag)
  private
  protected
    function GetD2(A, B : Integer) : Double;
    procedure SetD2(A, B : Integer; aValue : Double);
    function GetDataSize : Integer; override;
  public
    constructor Create(aOwner : TTagOwner; aName : string); override;
    property D2[A, B : Integer] : Double read GetD2 write SetD2;
  end;

  TUnaryIntegerArrayTag = class(TArrayTag)
  private
  protected
    function GetDataSize : Integer; override;
  public
    constructor Create(aOwner : TTagOwner; aName : string); override;
    destructor Destroy; override;
    procedure SetElementCount(aCount : Integer);
  end;

  TGenericDoubleArrayTag = class(TArrayTag)
  private
  protected
    function GetDataSize : Integer; override;
  public
    constructor Create(aOwner : TTagOwner; aName : string); override;
    destructor Destroy; override;
    procedure SetN(const aDims : array of Integer);
  end;

  TCNC = class(TAbstractCNC)
  private
  protected
    FClient : Boolean;
    FMachineSpecificData : Boolean;
    FContractSpecificData : Boolean;
    procedure CNCReset(aTag : TAbstractTag); virtual;
    procedure CNCResetRW(aTag : TAbstractTag); virtual;
    function  IsDualTag(aTag, Comp : TAbstractTag) : Boolean;
    procedure ReadLive(S : TStringList; Config : string); dynamic;
    function OpenLiveStream(const Config : string) : TStream;
    procedure WriteLive(S : TStringList; Config : string); dynamic;
    procedure SnapShot(var Message : TSnapShotMessage); message CNCM_SNAPSHOT;
    function InterpretToken(const aToken : string) : string; dynamic;
  public
    function  TagPublish(aName : string; TC : TTagClass) : TAbstractTag;
    function  TagEvent(aName : string; edge :TedgeType; TC : TTagClass; Callback : TCallback) : TAbstractTag;
    constructor Create   (AOwner : TComponent); override;
    destructor Destroy; override;
    procedure InstallComponent(Params : TParamStrings); override;
    procedure Installed; override;
    procedure CheckInstallation; override;
    procedure LoseFocus; virtual;
    function  IShutdownPermissive : boolean; override;
    procedure IShutdown; override;
    property Client : Boolean read FClient;
    class function  GetComponent(Ident : string) : TAbstractCNC;
    property MachineSpecificData : Boolean read FMachineSpecificData;
    property ContractSpecificData : Boolean read FContractSpecificData;
    function ResetRewindComplete: Boolean; virtual;
    function ResetComplete: Boolean; virtual;
  end;

  TOutputChange = procedure(address : word) of object;
{ Private thread owned by the IO system. A thread of this ancestry must be
  created by the decendant IO class.
  The thread will be destroyed by the TAbstractIO during the shutdown procedure }
  TAbstractIOThread = class(TThread)
  private
  protected
//    Last : TDateTime;
  public
    SleepTime : Integer;
    onSweep : procedure of object;
    procedure Execute; override;
    constructor Create;
  end;

{ Basic abstract component for all error handling, It acts as a sub-component
  of the IO system. Although a derived class must be in the install list</p><p>
  This component holds the global error list and the current active fault level,
  It publishes two values the current fault level (TFaultLevel) and FaultChange
  which should be used as an edge change event for client items. </p><p>
  Its only global methods are setFault which can be called to update the current
  fault list and faultResetID. </p><p>
  It uses the following names
  <ul>
  <li>MessageAck  : Event from both Memory and IO systems
  <li>userAccess  : The current user access is required for handling the reset
  <li>ReqReset    : Event From the Memory or IO systems.
  <li>ReqResetRW  : Event From the Memory or IO systems.
  <li>IOResetComplete : Event from the Memory or IO systems.
  <li>
  <li>ResetRW     : Published in both memory and IO
  <li>Reset       : Published in both memory and IO
  </ul>

  This component differs from other components by being an installed non
  visual component that may become visual, this is used for interlocking faults
  }
  TAbstractFaultInterface = class (TCNC)
  private
  protected
    FaultLevelTag  : TAbstractTag;     // FFaultLevel
    FaultChangeTag : TAbstractTag;
    ResetRWTag     : TAbstractTag;
    ResetTag       : TAbstractTag;

    ReqResetTag    : TAbstractTag;
    ReqResetRWTag  : TAbstractTag;
    IOResetCompleteTag : TAbstractTag;

    IOFaultTag     : TAbstractTag;
    FFaultLevel    : TFaultLevel;
//    FAccessLevel   : TAccessLevel;
    FFaultList     : TStringList;
    FInterlockFault: TInterlockFault;
    ResetRWCompleteList : TList;
    ResetCompleteList : TList;
    FaultLock : TCriticalSection;
    function GetFaultLevel : TFaultLevel;
    procedure FaultReset(aTag : TAbstractTag); virtual;
    procedure IOResetComplete(aTag : TAbstractTag); virtual;
    procedure Reset(aTag : TAbstractTag); virtual; abstract;
    procedure ResetRW(TagH : TAbstractTag); virtual; abstract;
  public
    constructor Create(Aowner : TComponent); override;
    destructor Destroy; override;
    procedure EnterFaultLock;
    procedure ExitFaultLock;
    procedure InstallComponent(Params : TParamStrings); override;
    procedure Installed; override;
    procedure AddResetComplete(Comp: TCNC);
    procedure AddResetRWComplete(Comp: TCNC);
    procedure DoResetRewind;
    function SetFault(aOwner : TCNC; const F : TFaultRegistration; const Params : array of const; Reset : TFaultResetMethod) : FHandle; virtual;
    function SetFaultStdCall(aOwner : TCNC; const F : TFaultRegistration; const Params : array of const; Reset : TFaultResetStdcall) : FHandle; virtual; abstract;

    procedure UpdateFault(FID : FHandle; const Args : array of const); virtual; abstract;
    procedure ResetInterlock; virtual; abstract;
    procedure FaultResetID(FiD : FHandle); virtual;
    property FaultLevel : TFaultLevel read GetFaultLevel;
//    property FaultList  : TStringList read FFaultList;
    function GetFault(Index : Integer; var Text : string; var Level : TFaultLevel): PFaultRegistration;
    function FaultCount : Integer;
    procedure ResetRewind; virtual; abstract;
  end;

  THttpComponentPath = class(TObject)
    Path, Description: WideString;
    Component: IHTTPServerComponent;
    Section: Integer;
  end;

  TPluginLibrarySet = class
  private
    ClientList : array of IScriptLibrary;
  public
    constructor Create;
    destructor Destroy; override;
    procedure Clear;
    procedure SetHost(Host: IScriptHost);
    function Resolve(AName: string): IScriptMethod;
    procedure Reset;
  end;

  TAbstractCommunications = class(TCNC, IHTTPServerHost, IFSDNamedInterfaceA)
  private
    CallbackList : TList;
    StackCount    : integer;
    StackFault    : boolean;
    Frozen        : Boolean;
    SweepTag      : TAbstractTag;     // FPlcSweep
    FPLC          : TAbstractPlc;
    PlcName       : string;
    IOSweepList   : TList;
    Fake          : TAbstractTag;
    LogNamedLayer : Boolean;

    FAccessLevels  : TAccessLevels;
    FCNCModes     : TCNCModes;
    FStatusType   : TCNCStatusTypes;

    AccesssTag : TAbstractTag; // Access mode change
    ModeTag  : TAbstractTag; // NC Mode Change
    MotionTag : TAbstractTag; // InMotion Change
    HomedTag : TAbstractTag; // Homed change
    CycleTag : TAbstractTag; // In Cycle change
    EditTag : TAbstractTag; // Section Editor dialog is open
    AccessInterlock : TMethodTag; // Used to signal an access interlock fault
    AccessUserNameTag: TAbstractTag; // Current User Name
    StreamingList : TList;

    OPCSubscriptionCountTag : TAbstractTag;
    OPCConnectionCountTag : TAbstractTag;
    StringArrayUpdateCountTag : TAbstractTag;

    MaxLagTag : TAbstractTag;
    AverageLagSum : Int64;
    AverageLagCount : Integer;
    AverageLagTag : TAbstractTag;
    ResetMaxLagTag : TAbstractTag;
    BlockIOTag: TAbstractTag;

    ComponentPaths: TList;

    PSDE: TPSDLoader;

    // Overrun trap
    CallbackCriticalSection: TCriticalSection;
    CurrentCallbackList: TStringList;
    CurrentCallbackReportTag: TAbstractTag;

    procedure UpdateTick;
    procedure ResetLagValues(aTag : TAbstractTag);
    procedure ResetIOSubSystems(ATag: TAbstractTag);
    function  GetNamedSize : Integer;
    procedure UpdateData  (aTag : TAbstractTag);
    function GetAccessUserName : string;
    procedure DoCallbacks (aTag : TAbstractTag; edge : TEdgeType);
    procedure ThreadDispatch(var Message : TIOMessage); message CNCM_IO_EVENT;
    function DispatchCallback(aTag : TAbstractTag; Callback : TCallback) : Boolean;
    function InterlockedTag(aTag : TAbstractTag) : Boolean;
    procedure PluginPublish;
    procedure PluginRegister;
    procedure AttachPlugins;
    procedure DetachPlugins;
    procedure PluginNewActiveFile(aTag : TAbstractTag);
    procedure UpdateStreaming(aTag : TAbstractTag);
    procedure ReadOPC;
  protected
    FPlcSweep       : longint;
    IOThread        : TAbstractIOThread;
    DataSize        : array [TIOType] of integer;
    OnOutputChange  : TOutputChange;
    SleepTime       : Integer;
    IOHealthyTag    : TAbstractTag;
    IOResetTag: TAbstractTag;
    FBlockOutputTranslation: Boolean;

    function FindIndex    (aName : string) : Integer; virtual;
    function AddPlaceHolderTag(aName : string; aSize : TTagClass) : TAbstractTag; virtual;
    procedure Sweep; virtual;
    procedure Clean; virtual;
    procedure CNCReset(aTag : TAbstractTag); override;
    function InterpretToken(const aToken : string) : string; override;
    procedure DisplayAbout(aTag : TAbstractTag);
    function GetIOSystemAccessor(AName: string): IIOSystemAccessor;

    { IFSDServerHost }
    procedure ProcessCGI(HFile : THANDLE; CGIPath, CGIRequest : WideString); stdcall;
    function AddStreaming(Stream : THandle; CGIPath, CGIRequest : WideString; DataStream : IHTTPServer; DT : Integer) : Boolean; stdcall;
    procedure RemoveStreaming(Stream : THandle; CGIPath, CGIRequest : WideString); stdcall;
    procedure SystemTokenValue(const aToken : WideString; out Res : WideString); stdcall;
    procedure AddComponentPath(Section: Integer; const Path, Description: WideString; Comp: IHTTPServerComponent); stdcall;

    { IFSDNamedInterface }
    function AddTag(aName, aType : PChar) : TTagRef; stdcall;
    function AquireTag(aName, aType : PChar; Callback : TNamedCallbackEv) : TTagRef; stdcall;
    procedure GetDataType(aTag : TTagRef; aType : PChar; MaxLen : Integer);
    function GetAsInteger(aTag : TTagRef) : Integer; stdcall;
    function GetAsBoolean(aTag : TTagRef) : Boolean; stdcall;
    function GetAsDouble(aTag : TTagRef) : Double; stdcall;
    procedure GetAsString(aTag: TTagRef; Buffer : PChar; MaxLen : Integer); stdcall;
    function GetAsData(aTag : TTagRef; var Length : Integer) : Pointer; stdcall;
    function GetAsOleVariant(aTag : TTagRef) : OleVariant; stdcall;
    procedure SetAsInteger(aTag : TTagRef; Value : Integer); stdcall;
    procedure SetAsBoolean(aTag : TTagRef; Value : Boolean); stdcall;
    procedure SetAsDouble(aTag : TTagRef; Value : Double); stdcall;
    procedure SetAsString(aTag : TTagRef; Value : PChar); stdcall;
    procedure SetAsOleVariant(aTag : TTagRef; Value : OleVariant); stdcall;
    procedure ReleaseCallback(aTag : TTagRef; Callback : TNamedCallbackEv); stdcall;
    procedure PluginRedirect(aTag : TAbstractTag);
    procedure AddRedirect(aTag : TAbstractTag; Callback: TNamedCallbackEv);
    procedure SetFault(Name : PChar; Text : PChar; Level : TFaultLevel); stdcall;
    function Heads : Integer; stdcall;

    { IFSDNamedInterfaceA }
    procedure EventLog(const Msg : WideString; NtEvent : Word = EVENTLOG_INFORMATION_TYPE); stdcall;
    procedure MessageLog(const Msg : WideString); stdcall;
    function LoadOPP(const FileName : WideString) : Boolean; stdcall;
    function LoadOCT(const FileName : WideString) : Boolean; stdcall;
    function SetFaultA(Name, Text : WideString; Level : TFaultLevel; Reset : TFaultResetStdcall) : FHandle; stdcall;
    procedure FaultResetID(FID : FHandle); stdcall;
    procedure UpdateFault(FID : FHandle; Text : WideString); stdcall;
    procedure ResetInterlock; stdcall;
    procedure SetPersistent(aTag : TTagRef; aOn : Boolean); stdcall;

    function IsPersistent(aTag : TTagRef) : Boolean; stdcall;
    function IsVCL(aTag : TTagRef) : Boolean; stdcall;
    function IsSystem(aTag : TTagRef) : Boolean; stdcall;
    function IsWritable(aTag : TTagRef) : Boolean; stdcall;
    function IsOwned(aTag : TTagRef) : Boolean; stdcall;
    function IsConnected(aTag : TTagRef) : Boolean; stdcall;
    function IsEvented(aTag : TTagRef) : Boolean; stdcall;
    function IsIO(aTag : TTagRef) : Boolean; stdcall;

  public
    SubSystems      : TList;
    Devices         : TList;
    NamedLayer   : TTagList;
    FSDWebServer : IHTTPServer;
    FOPCList      : TStringList;
    function FindTag(aName : string) : TAbstractTag; virtual;
    function IOFindTag(aName : string) : TMethodTag; virtual;

    constructor Create    (Aowner : Tcomponent); override;
    destructor Destroy; override;
    procedure InstallComponent(Params : TParamStrings); override;
    procedure Installed; override;
    procedure CheckInstallation; override;
    procedure IShutdown; override;

    procedure ReadIOMap(ini : TCustomIniFile);
    procedure WriteIOMap(Stream : TStream); virtual;

    function HTTPGeneratePage(HFile : THANDLE; var CGIPath : WideString; CGIRequest: ICGIStringList) : Boolean; override;
    function HTTPAddStreaming(Stream : THandle; CGIR: WideString; Method : IHTTPServer; DT : Integer) : Boolean; override;
    procedure HTTPRemoveStreaming(Stream : THandle; CGIR: WideString); override;

    function FindPlugin(aName : string) : HModule;

    procedure DefineAliasTags;
    function IOSizeToString(size : Integer) : string;
    function ReadNamedTag  (aName : string) : TAbstractTag;
    function GetTagIndex(Index : Integer): TAbstractTag;
    function AddNamedLayerTag(aTag : TAbstractTag) : TTagHandle;
    procedure Freeze;
    procedure CheckCallbacks;
    function  FindDevice(aName : string) : TAbstractIODevice;
    function  FindSubSystem(aName : string) : TGenericIOSubSystem;
    function OKToLoadFile : Boolean; // Check by file manager.
    function OKToEditSection(SectionName : string) : Boolean;
    function PluginOperatorDialogue(const Breed, Item : WideString): TNCDialogueResult;
    procedure PluginOperatorDialogueClose;
    procedure PluginSetJNI(AJNIEnv: PJNIEnv);

    procedure AddToIOSweep(Method : TCallback);
    procedure Terminate;
    procedure AddDevice (Device : TAbstractIODevice);

    procedure CNCFormChange;

    function GetHttpPath(Index: Integer): THttpComponentPath;
    function HttpPathCount : Integer;
    property HttpPath[Index: Integer] : THttpComponentPath read GetHttpPath;

    procedure RestartACNC; virtual;

    function CommsTagPublish(aOwner : TCNC; aName : string; TC : TTagClass) : TAbstractTag; virtual;
    function CommsTagEvent(aCaller : TCNC; aName : string; edge :TedgeType; TC : TTagClass; Callback : TCallback) : TAbstractTag; virtual;
    procedure Write(aTag : TAbstractTag; Edge : TEdgeType); virtual;

    function GetPluginScriptSet: TPluginLibrarySet;
    procedure AddLanguageFile(const Filename: WideString); stdcall;

    property NamedSize : integer read GetNamedSize;
    property PlcSweep : Integer read FPlcSweep;
    property Tag [Index : Integer] : TAbstractTag read GetTagIndex;
    property PLC : TAbstractPlc  read FPLC;
    property AccessLevels   : TAccessLevels read FAccessLevels;
    property CNCModes     : TCNCModes read FCNCModes;
    property StatusType   : TCNCStatusTypes read FStatusType;
    property AccessUserName : string read GetAccessUserName;

    function GetSignatureHost: ISignatureHost; stdcall;

    function GetConfigurationParser: IConfigurationParser;
    property BlockOutputTranslation: Boolean read FBlockOutputTranslation write FBlockOutputTranslation;
  end;

  { Base class for NC, This defines the interface for the minimum NC system
    No implementation details are included }
  TAbstractNC = class(TCNC, IFSDNCInterface)
  private
    procedure DualEventCallback(aTag : TAbstractTag);
  protected
    FCycleStartLock : Integer;
    FModeChangeLock : Integer;
    FLines : TStrings;
    TagEv : array [TNCEvent] of TAbstractTag;
    TagPb : array [TNCPublished] of TAbstractTag;
    FAxisPosition : TList;
    FAxisCount : Integer;
    FAxisNames : string;
    FCoordinateCount : Integer;
    FIXUP_InchModeDefault : Boolean;
    procedure ClientDualEvent(cevent : TNCEvent; aTag : TAbstractTag); virtual; abstract;
    procedure NewFaultLevel(aTag : TAbstractTag); virtual;
    function GetLines : TStrings; virtual;
    function GetSubRoutineLines(Index : Integer) : TStringList; virtual;

    // IFSDNCInterface
    function GetJogMode : TJogModes; stdcall;
    function GetSingleBlock : Boolean; stdcall;
    function GetOptionalStop : Boolean; stdcall;
    function GetBlockDelete : Boolean; stdcall;
    function GetActiveAxis : Integer; stdcall;
    function GetFeedHold : Boolean; stdcall;
    function GetCNCMode : TCNCModes; stdcall;
    function GetFeedOVR : Integer; stdcall;
    function GetAxisInMotion : Integer; stdcall;
    function GetDryRun : Boolean; stdcall;
    function GetAxisName(Index : integer) : TAxisName; stdcall;
    function GetAxisType(Index : integer) : TAxisType; virtual; stdcall;
    function GetAxisPosition(Index : Integer) : TAxisPosition; stdcall;
    function GetAxisCount : Integer; stdcall;
    function GetInCycle : Boolean; stdcall;
    function GetHomed : Boolean; stdcall;
    function GetInMotion : Boolean; stdcall;
    function GetInchModeDefault : Boolean; stdcall;

    function GetProgramLine : Integer; virtual; stdcall;
    function GetSubCount : Integer; virtual; stdcall;
    function GetActiveSubRoutine : Integer; virtual; stdcall;
    function GetSubRoutine(Index : Integer) : WideString; virtual; stdcall;


    function GetIsRotary(Index : Integer) : Boolean; virtual; abstract;
    procedure SetPartProgram(aTag : TAbstractTag); virtual; abstract;

  public
    function OrdinalNCMode : TCNCMode;
    property IsRotary[Index : Integer] : Boolean read GetIsRotary;
    procedure InstallComponent(Params : TParamStrings); override;
    procedure Installed; override;
    function IsPluginGCode(Major, Minor: Integer): Boolean;
    function GetPluginGCode(Major, Minor: Integer): INCExtGCode;

    // IFSDNCInterface continued
    procedure AcquireCycleStartLock; stdcall;
    procedure ReleaseCycleStartLock; stdcall;
    procedure AcquireModeChangeLock; stdcall;
    procedure ReleaseModeChangeLock; stdcall;

    procedure SetMDIBuffer(mdi : string); virtual; abstract;
    function GetMDIBuffer: string; virtual; abstract;
    procedure ExecuteMacro(Filename: string); virtual; abstract;
    property  AxisPosition[Index : Integer] : TAxisPosition read GetAxisPosition; // write WritePosition;
    property  AxisName    [Index : Integer] : TAxisName read GetAxisName;
    property  AxisType    [Index : Integer] : TAxisType read GetAxisType;
    property  AxisCount   : Integer  read GetAxisCount;
    property  ActiveAxis  : Integer  read GetActiveAxis;
    property  OptionalStop : Boolean  read GetOptionalStop;
    property  SingleBlock  : boolean  read GetSingleBlock;
    property  CNCMode      : TCNCModes read GetCNCMode;
    property  FeedOvr      : Integer  read GetFeedOvr;
    property  ProgramLine  : Integer  read GetProgramLine;
    property  Lines        : TStrings read GetLines;
    property  BlockDelete  : Boolean read GetBlockDelete;
    property  InCycle      : Boolean read GetInCycle;
    property  InMotion     : Boolean read GetInMotion;
    property  AxisInMotion : Integer read GetAxisInMotion;
    property  Homed        : Boolean read GetHomed;
    property  FeedHold     : Boolean read GetFeedHold;
    property  DryRun       : Boolean read GetDryRun;
    property  JogMode      : TJogModes read GetJogMode;
    property  CoordinateCount : Integer read FCoordinateCount;
    property ActiveSubroutine : Integer read GetActiveSubroutine;

    property SubRoutineLines[Index : Integer] : TStringList read GetSubRoutineLines;
  end;

{ This is the ancestor for all visual components with in the CNC System.
  It holds the open and close events of the derived component so ensuring
  the Display handler maintains control </p><p>

  Any derived component must not write to the following parameters.
  <dl>
  <dd>Width
  <dd>Height
  <dd>left
  <dd>top
  <dd>visible
  </dl>
  These are maintained by the TAbstractLayoutManager component, however it is vital
  the derived class overrides the 'resize' method and attempts to manage its
  children accordingly </p><p>

  By using the loadKeys method a set of function keys are loaded from the CNC.ini
  file these can then be passed to installKeySet which will register them within
  the menu (TAbstractSoftkey) system.</p><p>

  Derived classes may set primary to set a TContol that gains focus on opening </p><p>

  ManageEdit must be set true if the derived component wishes to override the default
  Edit handler. }
  TAbstractDisplay = class(TCNC)
  private
    //FContextHelp   : string;
  protected
    KeySetHandle : Word;
    Primary      : TWinControl;
    ManageEdit   : Boolean;
    FActiveDisplay : string;
    EditLevel : TAccessLevels;
    FTitle: string;
    TitlePanel: TPanel;
    FUseTitle: Boolean;
    procedure InstallKeySet(keys : TKeyArray);
    procedure UserAccessChange(aTag : TAbstractTag); dynamic;
    procedure NormalDisplay(aTag : TAbstractTag); dynamic;
    procedure SetTitle(const ATitle: string);
  public
    LayoutDemanded : Boolean;
    ExternalLayout : Boolean; // If this display is being managed by something other than the normal LayoutManager.
    VisibleTag: TAbstractTag;
    constructor Create     (Aowner : TComponent); override;
    destructor  Destroy; override;
    procedure InstallComponent(Params : TParamStrings); override;
    procedure Installed; override;
    procedure Activate     (aTag : TAbstractTag); virtual;
    function HTTPGeneratePage(HFile: THANDLE; var CGIPath: WideString; CGIRequest: ICGIStringList) : Boolean; override;
    procedure Closing; virtual;
    procedure IShutdown; override;
    procedure Open; virtual;
    procedure Close; virtual;
    property Title: string read FTitle write SetTitle;
    property UseTitle: Boolean read FUseTitle;
    procedure ShowTitlePanel(AHeight: Integer);
    procedure HideTitlePanel;

    //property ContextHelp   : string      read FContextHelp   write FContextHelp;
    property ActiveDisplay : string read FActiveDisplay;
  published
    property caption;
  end;

  { This is an abstract class for the management of all visual components.
    The <a href=#add>add</a> method is for installing each component into the
    system the <a href=#open>open</a> and <a href=#close>close</a> methods are
    called via the <a href=TabstractDisplay.html>TabstractDisplay</a> to show or hide the component.
    <a href=#normal>normal</a> is used to default the display. </p><p>
    The following procedures are merely place holders for the derived class</p>
    <ul>
    <li>add : called during installation to register a visual component
    <li>open : called when a component is requests visibility
    <li>close : called when an installed display wishes to become invisible
    <li>normal : Forces the default NC display
    <li>validDisplay : returns a valid display number
    <li>move : moves this display to another display (temporary)
    <li>makePanel : creates a new display area for this component using its width and height
    <li>summary : Produces a textual summary of the whole display set
    </ul>

    It is vitally important that a derived class must override all abstract even
    if they do nothing.
}
  TAbstractLayoutManager = class(TCNC)
  private
    DisplaySweepTag  : TAbstractTag;
    FlashTag         : TAbstractTag;
    FlashValue       : boolean;
    TimeTag          : TAbstractTag;
    DateTag          : TAbstractTag;
    NowTag           : TAbstractTag;
    //NormalDisplayTag : TAbstractTag;
    procedure NextWindow(aTag : TAbstractTag);
  protected
    YearTag : TAbstractTag;
    MonthTag : TAbstractTag;
    DayTag : TAbstractTag;
    WDayTag : TAbstractTag;
    HourTag : TAbstractTag;
    MinuteTag : TAbstractTag;
    SecondTag : TAbstractTag;
    FlashTimer   : TTimer;
    DisplayTimer : TTimer;
    DisplayList  : TList;
    FDisplayWidth : Integer;
    FDisplayHeight : Integer;
    FScreenSaver : Boolean;
    FKeyboardVisible : Boolean;
    FNumericPadVisible : Boolean;
    FNavigationVisible : Boolean;
    procedure UpdateDisplay(Sender : TObject); virtual;
    procedure UpdateFlash(Sender : TObject); virtual;
    function  ValidDisplay(D : Integer) : Integer; virtual; abstract;
    procedure Open  (Display : TAbstractDisplay); virtual; abstract;
    procedure Close (Display : TAbstractDisplay); virtual; abstract;
    procedure SetScreenSaver(aSaver : Boolean); virtual; abstract;
    function GetDisplay(Index : Integer) : TAbstractDisplay;
    function GetDisplayCount : Integer;
    procedure NormalDisplay(ATag: TAbstractTag); virtual;
  public
    constructor Create(aOwner : TComponent); override;
    procedure Add   (Display : TAbstractDisplay); virtual;
    procedure Normal; virtual;
    procedure Swap  (display : TAbstractDisplay); virtual; abstract;
    procedure Expand (display : TAbstractDisplay; n : integer); virtual; abstract;
    procedure InstallComponent(Params : TParamStrings); override;
    procedure Installed; override;
    procedure IShutdown; override;
    procedure ShowKeyboard(Display : TAbstractDisplay); virtual; abstract;
    procedure ShowNumericPad(Display : TAbstractDisplay); virtual; abstract;
    procedure ShowNavigation(Display : TAbstractDisplay); virtual; abstract;
    procedure HTTPCreateImageMap(hFile : THandle); virtual; abstract;
    procedure HideKeyboard; virtual; abstract;
    procedure HideNumericPad; virtual; abstract;
    procedure HideNavigation; virtual; abstract;
    property KeyboardVisible : Boolean read FKeyboardVisible;
    property NumericPadVisible : Boolean read FNumericPadVisible;
    property NavigationVisible : Boolean read FNavigationVisible;
    property  Displays [Index : Integer] : TAbstractDisplay read GetDisplay;
    property  DisplayCount : Integer read GetDisplayCount;
    property  DisplayWidth : Integer read FDisplayWidth;
    property  DisplayHeight : Integer read FDisplayHeight;
    property ScreenSaver : Boolean read FScreenSaver write SetScreenSaver;
  end;

{ TAbstractMenu is a derivative of TAbstractDisplay (it is a visual component) and
  receives the F1 to F12 Functions keys into the abstract keyEvent procedure
  this must be overriden by the derived class </p><p>

  This component actually does nothing with them, it mearly maintains the extra
  keys sets loaded by TAbstractDisplay and its decendants. </p><p>

  The abstract procedure keyChange is called when an installed component requires
  the display of a defined key set, the handle passed in is same as that returned
  by registerKeyset (This should be called during installation).
}
  TAbstractSoftKey = class(TAbstractDisplay)
  private
    keySets      : PTKeyLinkedList;
    handleCount  : integer;
  protected
    FEnabled     : Boolean;
//    KeyOwnerShip : Boolean;
    function Add            (keys : TkeyArray) : word;
    function Find           (handle : word; var keys : TKeyArray) : Boolean;
  public
    constructor Create      (Aowner : TComponent); override;
    procedure InstallComponent(Params : TParamStrings); override;
    procedure WndKey        (var message : TMessage); message CNCM_BUTTON_EVENT;
    procedure KeyEvent      (key : integer; edge : TedgeType); virtual; abstract;
    procedure KeyChange     (handle : word); virtual; abstract;
    procedure KeyClose      (handle : word); virtual; abstract;
    function  RegisterKeySet(newKeys : TKeyArray): word; virtual;
    property  Enabled : Boolean read FEnabled write SetEnabled;
    procedure Reset; virtual; abstract; // invoke top layer keys
  published
  end;

{ This is the minimum interface abstraction for a soft PLC. A derived class must
  be thread safe within the execute method.  }
  TAbstractPLC = class(TCNC, IPlcHost)
  protected
  public
    TextColor : TColor;
    ActiveColor : TColor;
    InactiveColor : TColor;
    X_MARGIN : Integer;
    Y_MARGIN : Integer;
//    SweepCount : Integer;
    procedure Execute; virtual; abstract;
    procedure Stop; virtual; abstract;
    function GetItemValue(Item: Pointer): Integer; virtual; stdcall;
    procedure SetItemValue(Item: Pointer; Value: Integer); virtual; stdcall;
  end;

  TSectionEditor = class(TCNC)
  public
    procedure InstallComponent(Params : TParamStrings); override;
    function Execute(Manager: TCompoundFile): Boolean; virtual; abstract;
    function SectionName: String; virtual; abstract;
  end;


  { Core Component for managing on-line and off-line file manipulation the
    decendant component should be the the only method of accessing CNC
    application specific files.
    The TAbstractFileManager Registers events for mode change and LoadActive.
    It also Publishes SectionEditing used for interlocking}
  TAbstractFileManager = class(TAbstractDisplay)
  protected
    FFilemask: String;
    FFilePath: String;
    FileChangedTag: TAbstractTag; // Used within the local system.
    FileForceTag : TAbstractTag; // Published in Client System.
    RemoteFileTag : TAbstractTag; // Published by Server System.
    OPPFilePathTag: TAbstractTag;
    SectionEditingTag: TAbstractTag;
    procedure DoLoadActive(aTag : TAbstractTag); dynamic; abstract;
    procedure DoFollowLoadActive(aTag: TAbstractTag); dynamic; abstract;
    function  GetActiveFileState : TFileState; virtual; abstract;
    function  GetActiveFileName : String; virtual; abstract;
    function  GetEditMode: Boolean;
    procedure CNCModeChanged(aTag : TAbstractTag); virtual; abstract;
  public
    {the following three methods all relate to the ACTIVE file}
    procedure InstallComponent(Params : TParamStrings); override;
    procedure Installed; override;
    procedure SectionRead( const SectionName: String; Data: TStrings); virtual; abstract;{read only}
    procedure SectionOpen( const SectionName: String; Data: TStrings); virtual; abstract;{(file = open) and (sect = closed)}
    procedure SectionClose( Data: TStrings; SaveData: Boolean); virtual; abstract;
    function LoadActiveFile(aFileName: String) : Boolean; virtual; abstract;
    function IsActiveFile( F: TObject): Boolean; virtual; abstract;
             {F might be a TStrings or a TCompoundFile...}
    property ActiveFileState: TFileState read GetActiveFileState;
    property ActiveFileName: String read GetActiveFileName;
    property EditMode: Boolean read GetEditMode;
    procedure AttachEditor(aEditor : TSectionEditor; aDescription : string); virtual; abstract;
  end;

  TLocalization = class(TInterfacedObject, INCParserResolver)
  private
    LanguageType : string;
    Table : IACNC32StringArrayA;
    //Table : TStringArray;
    Column : Integer;
  public
    constructor Create;
    procedure AppendLanguageFile(const Filename: string);
    function GetString(const aToken : string) : string;
    function Resolve(const Token: WideString; out Value: WideString): Boolean; stdcall;
  end;


  TSystemObject = class(TObject)
  private
    OCXLibraryCount: Integer;
    OCXLibrarys : array of IScriptLibrary;
  public
    VisualParent    : TForm;
    FCNCComponents  : TStringList;
    SystemName      : string;
    Heads           : Integer;
    ClientPath      : string;
    InstalledList   : TParamStrings;
    PluginList : TStringList;
    Translate: TParamStrings;

    ConfigParser: TParsingIniFile;

    IO              : TAbstractCommunications;
    Comms           : TAbstractCommunications;
    NC              : TAbstractNC;
    FaultInterface  : TAbstractFaultInterface;
    Layout          : TAbstractLayoutManager;
    SoftKey         : TAbstractSoftKey;
    FileManager     : TAbstractFileManager;
    ToolLife        : IFSDToolLife; // Do not assume this is set.
    OCT             : IFSDOCT;
    HttpServerHost  : IHttpServerHost;

    { A ClientSystem Supplies Dialogues to the end user and publishs the
      Expected results of these dialogues }
    ClientSystem    : Boolean;
    { A ServerSystem Does not produce any dialogues but Registers events
      for the results of the normal dialogues }
    ServerSystem    : Boolean;
    FontName        : string;
    MonoFontName    : string;
    AlternateLoad : TGeneralPluginSelectProgram;
    StringArraySet : TStringArraySet;
    Localized : TLocalization;
    Keywords : TAcnc32Keywords;
    InstallComp: string;
    StrictMDTUsage: Boolean;
    StrictComparison: Boolean;
    PluginLibraryList: TList;

    SignatureHost: ISignatureHost;

    JNIEnv: PJNIEnv;
    constructor Create;
    procedure ClearPluginLibraries;
    procedure AddOCXLibrary(ISL: IScriptLibrary);
    procedure RemoveOCXLibrary(ISL: IScriptLibrary);
  end;


procedure RegisterCNCClass(ClassType: TAbstractCNCClass; AName: string='');
procedure RegisterTagClass(ClassType : TTagClass); overload;
procedure RegisterTagClass(ClassType : TTagClass; const AName: string); overload;
procedure RegisterIODeviceClass(ClassType : TAbstractIODeviceClass);
procedure RegisterIOAccessorClass(ClassType: TAbstractIOAccessorClass; AName: string='');

function FindCNCClass( const Name, TypeName: String): TAbstractCNC;
function FindTagClass(const aTypeName : string): TTagClass;
function FindIODeviceClass(const aTypeName : string) : TAbstractIODeviceClass;
function FindIOAccessorClass(const aTypeName : string) : TAbstractIOAccessorClass;

procedure MessageLog (const msg : string);
procedure MessageLogFmt (const msg : string; const Args : array of const);
//procedure MessageError (const msg : string);
procedure EventLog(const Msg : string; NtEvent : Word = EVENTLOG_INFORMATION_TYPE);
procedure EventLogFmt (const msg : string; const Args : array of const; NtEvent : Word = EVENTLOG_INFORMATION_TYPE);
procedure LocalizeForm(Form : TForm);
function ReadColor(S : TStreamTokenizer) : TColor;

var
  SysObj : TSystemObject;

const
   NCEventF : array [TNCEvent] of TNamedRegistration = (
      ( tag : 'ReqNC%dAxisIndex'  ; size : TMethodTag ; edge :  edgeRising),
      ( tag : 'ReqNC%dAxisSelect' ; size : TIntegerTag ; edge :  edgeChange),
      ( tag : 'ReqNC%dJogModeInc' ; size : TMethodTag ; edge :  edgeRising),
      ( tag : 'ReqNC%dJogModeCont'; size : TMethodTag ; edge :  edgeRising),
      ( tag : 'ReqNC%dJogModeMPG' ; size : TMethodTag ; edge :  edgeRising),
      ( tag : 'ReqNC%dJogModeHome'; size : TMethodTag ; edge :  edgeRising),
      ( tag : 'ReqNC%dJogIncrement';size : TIntegerTag ; edge :  edgeChange),
      ( tag : 'ReqNC%dFeedOvrEna' ; size : TMethodTag ; edge :  edgeChange),
      ( tag : 'ReqNC%dFeedOVR'    ; size : TIntegerTag ; edge :  edgeChange),
      ( tag : 'ReqNC%dRapidOVR'   ; size : TIntegerTag ; edge :  edgeChange),
      ( tag : 'ReqNC%dCycleStart' ; size : TMethodTag ; edge :  edgeChange),
      ( tag : 'ReqNC%dCycleStop'  ; size : TMethodTag ; edge :  edgeChange),
      ( tag : 'ReqNC%dCycleAbort'  ; size : TMethodTag ; edge :  edgeChange),
      ( tag : 'ReqNC%dFeedHold'   ; size : TMethodTag ; edge :  edgeChange),
      ( tag : 'ReqNC%dHomeAxis'   ; size : TMethodTag ; edge :  edgeRising),
      ( tag : 'ReqNC%dHomeProg'   ; size : TMethodTag ; edge :  edgeRising),
      ( tag : 'ReqNC%dDryRun'     ; size : TMethodTag ; edge :  edgeRising),
      ( tag : 'ReqNC%dBlockDelete'; size : TMethodTag ; edge :  edgeRising),
      ( tag : 'ReqNC%dJogPlus'    ; size : TMethodTag ; edge :  edgeChange),
      ( tag : 'ReqNC%dJogMinus'   ; size : TMethodTag ; edge :  edgeChange),
      ( tag : 'ReqNC%dManualMode' ; size : TMethodTag ; edge :  edgeFalling),
      ( tag : 'ReqNC%dAutoMode'   ; size : TMethodTag ; edge :  edgeFalling),
      ( tag : 'ReqNC%dMDIMode'    ; size : TMethodTag ; edge :  edgeFalling),
      ( tag : 'ReqNC%dTeachMode'  ; size : TMethodTag ; edge :  edgeFalling),
      ( tag : 'ReqNC%dEditMode'   ; size : TMethodTag ; edge :  edgeFalling),
      ( tag : 'ReqNC%dProgramLine'; size : TIntegerTag  ; edge :  edgeChange),
      ( tag : 'ReqNC%dSingleBlock' ; size : TMethodTag ; edge :  edgeChange),
      ( tag : 'ReqNC%dOptionalStop'; size : TMethodTag ; edge :  edgeRising),
      ( tag : 'ReqNC%dRewindProg' ; size : TMethodTag ; edge :  edgeRising),
      ( tag : 'ReqNC%dMCodeAFC'   ; Size : TMethodTag ; Edge :  edgeChange),
      ( tag : 'TestPoint'     ; size : TMethodTag ; edge :  edgeFalling ),
      ( tag : 'ReqNC%dExtFunc1'; size : TMethodTag ; edge : EdgeChange ),
      ( tag : 'ReqNC%dExtFunc2'; size : TMethodTag ; edge : EdgeChange ),
      ( tag : 'ReqNC%dExtFunc3'; size : TMethodTag ; edge : EdgeChange ),
      ( tag : 'ReqNC%dExtFunc4'; size : TMethodTag ; edge : EdgeChange ),
      ( tag : 'ReqNC%dExtFunc5'; size : TMethodTag ; edge : EdgeChange ),
      ( tag : 'ReqNC%dExtFunc6'; size : TMethodTag ; edge : EdgeChange ),
      ( tag : 'ReqNC%dExtFunc7'; size : TMethodTag ; edge : EdgeChange ),
      ( tag : 'ReqNC%dExtFunc8'; size : TMethodTag ; edge : EdgeChange ),
      ( tag : 'ReqNC%dExtFunc9'; size : TMethodTag ; edge : EdgeChange ),
      ( tag : 'ReqNC%dExtFunc10'; size : TMethodTag ; edge : EdgeChange ),
      ( tag : 'ReqNC%dZeroAdjustAxis'; size : TMethodTag ; edge : EdgeFalling ),
      ( tag : 'ReqNC%dZeroAdjust'; size : TMethodTag ; edge : EdgeFalling ),
      ( tag : 'ReqNC%dHalfZeroAdjustAxis'; size : TMethodTag ; edge : EdgeFalling ),
      ( tag : 'ReqNC%dClearZeroAdjustAxis'; size : TMethodTag ; edge : EdgeFalling ),
      ( tag : 'ReqNC%dClearZeroAdjust'; size : TMethodTag ; edge : EdgeFalling ),
      ( tag : 'ReqNC%dPresetZeroAdjust'; size : TMethodTag ; edge : EdgeFalling ),
      ( tag : 'ReqNC%dCopyPosition'; size : TMethodTag ; edge : EdgeFalling ),
      ( tag : 'ReqNC%dMacroExecute'; size : TIntegerTag ; edge : EdgeFalling ),
      ( tag : 'ReqNC%dReleaseAxes'; size : TIntegerTag ; edge : EdgeChange ),
      ( tag : 'ReqNC%dNoCycle'; size : TIntegerTag ; edge : EdgeChange )
   );

  NCPublishedF : array[TNCPublished] of TNamedRegistration = (
      ( tag : 'NC%dActiveAxis'    ; size :  TIntegerTag ),
      ( tag : 'NC%dActiveMode'    ; size :  TIntegerTag ),
      ( tag : 'NC%dInCycle'       ; size :  TMethodTag  ),
      ( tag : 'NC%dInMotion'      ; size :  TMethodTag ),
      ( tag : 'NC%dAxisInMotion'  ; size :  TIntegerTag ),

      ( tag : 'NC%dAxisWarnFollowError'  ; size :  TIntegerTag ),
      ( tag : 'NC%dAxisFatalFollowError'  ; size :  TIntegerTag ),
      ( tag : 'NC%dAxisOpenLoop'  ; size :  TIntegerTag ),
      ( tag : 'NC%dAxisPositiveLimit'  ; size :  TIntegerTag ),
      ( tag : 'NC%dAxisNegativeLimit'  ; size :  TIntegerTag ),
      ( tag : 'NC%dAxisInPosition'  ; size :  TIntegerTag ),
      ( tag : 'NC%dAxisHomed'     ; size :  TIntegerTag ),
      ( tag : 'NC%dAxisAmplifierFault'; Size : TIntegerTag ),
      ( tag : 'NC%dAxisEncoderLoss'; Size : TIntegerTag ),
      ( tag : 'NC%dSingleBlock'   ; size :  TMethodTag ),
      ( tag : 'NC%dOptionalStop'  ; size :  TMethodTag ),
      ( tag : 'NC%dBlockDelete'   ; size :  TMethodTag ),
      ( tag : 'NC%dFeedHold'      ; size :  TMethodTag ),
      ( tag : 'NC%dDryRun'        ; size :  TMethodTag ),
      ( tag : 'NC%dProgramLine'   ; size :  TIntegerTag ),
      ( tag : 'NC%dJogMode'       ; size :  TIntegerTag ),
      ( tag : 'NC%dHomed'         ; size :  TMethodTag ),
      ( tag : 'NC%dFeedOVREna'  ; size :  TMethodTag ),
      ( tag : 'NC%dFeedOVR'     ; size :  TIntegerTag ),
      ( tag : 'NC%dPartProgram' ;  size :   TStringTag ),
      ( tag : 'NC%dMDIBuffer'   ;  size :   TStringTag ),
      ( tag : 'NC%dVectorFeedRate';  size :   TDoubleTag ),
      ( tag : 'NC%dTargetFeedRate';  size :   TDoubleTag ),
      ( tag : 'NC%dAxisName'    ;  size :   TStringTag  ),
      ( tag : 'NC%dJogIncrement';  size :   TIntegerTag  ),
      ( tag : 'NC%dMCode'       ;  Size :   TIntegerTag ), // Naughty thread crossing
      ( tag : 'NC%dSCode'       ;  Size :   TIntegerTag ), // Naughty thread crossing
      ( tag : 'NC%dTCode'       ;  Size :   TIntegerTag )  // Naughty thread crossing
  );



var
  InstallableList : TStringList;
  DelayedFatalError: TStringList;

implementation

uses Consts, TypInfo, Plugin, SectionEditorPlugin, Installer,
     {OPC,} NCMath, AboutForm, FileCtrl, Variants;

var
  RegTagList      : TStrings;
  RegDeviceList   : TStrings;
  RegIOAccessorList : TStrings;
  SystemLock : TCriticalSection;
  AtomLock : TCriticalSection;
  LoggingList : TStringList;
  BootLogList: TStringList;

  const
{  NamedSizeInit = 100;
  NamedSizeDelta = 30; }


  { TAbstractFileManager ini file parameters }
  FilemaskKey = 'Filemask';
  DefaultFileMask = '*.nc';

var BootLogFirstTime: Boolean;

{ Consistent status reporting to Bootlog  }
procedure MessageLog(const Msg : string);
var Stream : TFileStream;
    FileName : string;
    Tmp: string;
begin
  SystemLock.Enter;
  try
    BootLogList.Add(Msg);
    if Assigned(SysObj.Comms) then begin
      FileName := LoggingPath + 'Boot.Log';

      try
        if not FileExists(FileName) or BootLogFirstTime then
          Stream := TFileStream.Create(FileName, fmCreate)
        else
          Stream := TFileStream.Create(FileName, fmOpenWrite);
        BootLogFirstTime:= False;
        try
          Stream.Seek(0, soFromEnd);
          while BootLogList.Count > 0 do begin
            Tmp:= BootLogList[0] + CarRet;
            Stream.Write(PChar(Tmp)^, Length(Tmp));
            BootLogList.Delete(0);
          end;
        finally
          Stream.Free;
        end;
      except
  //      raise Exception.Create('Logging Stream Failed Write Operation');
        Exit;
      end;
    end;
  finally
    SystemLock.Release;
  end;
end;

procedure MessageLogFmt (const Msg : string; const Args : array of const);
begin
  MessageLog(Format(Msg, Args));
end;

procedure EventLog(const Msg : string; NtEvent : Word = EVENTLOG_INFORMATION_TYPE);
var Stream : TFileStream;
    FileName, Username : string;
    MMsg : string;
begin
  SystemLock.Enter;
  try
    if Assigned(SysObj.Comms) then begin
      FileName := LoggingPath + 'Event.Log';

      if Assigned(SysObj.Comms) and (SysObj.Comms.InstallState <> InstallStateClosed) then
        Username:= SysObj.Comms.AccessUserName
      else
        Username:= '';

      LoggingList.Add(CSVField(Username) + ',' +
                      CSVField(Msg) + ',' + CSVField(StorageDate(Now))); //DateTimeToStr
      try
        if not FileExists(FileName) then
          Stream := TFileStream.Create(FileName, fmCreate)
        else
          Stream := TFileStream.Create(FileName, fmOpenWrite);
        try
          Stream.Seek(0, soFromEnd);
          while LoggingList.Count > 0 do begin
            MMsg := LoggingList[0];
            Stream.Write(PChar(MMsg + CarRet)^, Length(MMsg + CarRet));
            LoggingList.Delete(0);
          end;
        finally
          Stream.Free;
        end;
      except
  //      raise Exception.Create('Logging Stream Failed Write Operation');
        Exit;
      end;
    end else begin
      LoggingList.Add('Boot' + ',' +
                      CSVField(Msg) + ',' + CSVField(StorageDate(Now))); //DateTimeToStr
    end;
  finally
    SystemLock.Release;
  end;
end;

procedure EventLogFmt (const msg : string; const Args : array of const; NtEvent : Word = EVENTLOG_INFORMATION_TYPE);
begin
  EventLog(Format(Msg, Args));
end;

procedure DoSnapShot(HFile, WinHandle : THANDLE);
var Jpeg : TJpegImage;
    Stream : THandleStream;
    Lock : TCriticalSection;
    BMap : TBitMap;
begin
  Jpeg := TJpegImage.Create;
  Stream := THandleStream.Create(HFile);
  Lock := TCriticalSection.Create;
  BMap := TBitMap.Create;
  Lock.Enter;
  try
    try
      // A really poor mans mutex
      SendMessage(WinHandle, CNCM_SNAPSHOT, Integer(Bmap), 0);
      Sleep(1);
      Jpeg.Assign(BMap);
    except
      WriteStringToHandle(hFile, '<HTML>'+ '<HEAD>' + CarRet + '<TITLE>ACNC32 Component list</TITLE>' + '</HEAD>' + CarRet);
      WriteStringToHandle(hFile, '<h1>Temporary HTTP failure</h3>');
      WriteStringToHandle(hFile, '</body></html>');
    end;
  finally
    try
      Jpeg.SaveToStream(Stream);
    finally
      Bmap.Free;
      Jpeg.Free;
      Stream.Free;
    end;
    Lock.Release;
    Lock.Free;
  end;
end;

procedure ReadHTTPParameters(var P : string; S : TCGIStringList);
var TTT : string;
begin
  while P <> '' do begin
    {I := }S.Strings.Add(ReadDelimited(P, '&'));
//    S.Values[S[I]] := ReadDelimited(P, '&');
    TTT := S.Strings.Values['name'];
  end;
end;


function TAbstractCNC.HTTPGeneratePage(HFile : THANDLE; var CGIPath: WideString; CGIRequest: ICGIStringList) : Boolean;
var Tmp : string;
begin
  Result := False;
  Tmp := CGIPath;
  Tmp := ReadDelimited(Tmp, '/');
{  if Tmp = '' then begin
    WriteACNC32HTTPTitle(HFile, Name);
    WriteACNC32HTTPHeading(hFile, 1, Name);
  end else } if CompareText(Tmp, 'screenshot.jpg') = 0 then begin
    DoSnapShot(HFile, Self.Handle);
    Result := True;
  end;
end;

function TAbstractCNC.HTTPAddStreaming(Stream : THandle; CGIR: WideString; Method : IHTTPServer; DT : Integer) : Boolean;
begin
  Result := False;
end;

procedure TAbstractCNC.HTTPRemoveStreaming(Stream : THandle; CGIR : WideString);
begin
end;

procedure GenerateFileNotFound(HFile : THANDLE; CGIPath : string);
begin
  WriteStringToHandle(hFile, '<HTML>'+ '<HEAD>' + CarRet + '<TITLE>ACNC32 Component: Not found' + CGIPath + '</TITLE>' + '</HEAD>' + CarRet);
  WriteStringToHandle(hFile, '<body><h1>' + CGIPath + '</h1><h2>Object does not exist.</h2></body>');
end;

procedure GenerateNoInformation(HFile : THANDLE; CGIPath : string);
begin
  WriteStringToHandle(hFile, '<HTML>'+ '<HEAD>' + CarRet + '<TITLE>ACNC32 Component: Not found' + CGIPath + '</TITLE>' + '</HEAD>' + CarRet);
  WriteStringToHandle(hFile, '<body><h1>' + CGIPath + '</h1><h2>No information is available on this object.</h2></body>');
end;

procedure GenerateDirectoryOfInstallation(HFile : THANDLE);
var I : Integer;
    HttpComponent: THttpComponentPath;
begin
  WriteACNC32HTTPTitle(HFile, 'Home Page');
  WriteStringToHandle(hFile, '<center><h2>Help Contents</h2></center>' + CarRet);
  WriteStringToHandle(hFile, '<table border="1" align="center">');
  for I := 0 to SysObj.IO.HttpPathCount - 1 do begin
    HttpComponent:= THttpComponentPath(SysObj.IO.HttpPath[I]);
    if HttpComponent.Section = HttpHelpSection then
      WriteStringToHandle(hFile, Format('<tr><td><a href="component/%s">%s</a></td></tr>' + CarRet,
                 [HttpComponent.Path,
                  HttpComponent.Description]));
  end;
//  WriteStringToHandle(hFile, '<tr><td><a href="/cgi-bin/help">Context help</a></td><td></td></tr>' + CarRet);
  WriteStringToHandle(hFile, '</table>');

  WriteStringToHandle(hFile, '<br />');

  WriteStringToHandle(hFile, '<center><h2>Maintenance Contents</h2></center>' + CarRet);
  WriteStringToHandle(hFile, '<table border="1" align="center">');
  for I := 0 to SysObj.IO.HttpPathCount - 1 do begin
    HttpComponent:= THttpComponentPath(SysObj.IO.HttpPath[I]);
    if HttpComponent.Section = HttpMaintenanceSection then
      WriteStringToHandle(hFile, Format('<tr><td><a href="component/%s">%s</a></td></tr>' + CarRet,
                 [HttpComponent.Path,
                  HttpComponent.Description]));
  end;
//  WriteStringToHandle(hFile, '<tr><td><a href="/cgi-bin/help">Context help</a></td><td></td></tr>' + CarRet);
  WriteStringToHandle(hFile, '</table>');


end;


function TCNC.InterpretToken(const aToken : string) : string;
begin
  Result := Format('--UKNOWN.TOKEN [%s]--', [aToken]);
end;


procedure TCNC.SnapShot(var Message : TSnapShotMessage);
begin
  Message.Bmp.Canvas.Lock;
  try
    try
      Message.Bmp.Width := Width;
      Message.Bmp.Height := Height;
      Self.PaintTo(Message.Bmp.Canvas.Handle, 0, 0);
    except
      Message.Bmp.Canvas.TextOut(20, 20, 'Bugger it');
    end;
  finally
    Message.Bmp.Canvas.Unlock;
  end;
end;


procedure TAbstractCommunications.SystemTokenValue(const aToken : WideString; out Res : WideString); stdcall;
var I : Integer;
    ACNC : TCNC;
    Tmp, Query : string;
begin
  Query := aToken;
  Tmp := ReadDelimited(Query, '.');
  for I := 0 to SysObj.FCNCComponents.Count - 1 do begin
    ACNC := TCNC(SysObj.FCNCComponents.Objects[I]);
    if CompareText(ACNC.Name, Tmp) = 0 then begin
      Res := ACNC.InterpretToken(Query);
      Exit;
    end;
  end;
  Res := Format('--TOKEN.NOT.FOUND [%s]--', [aToken]);
end;

procedure TAbstractCommunications.AddComponentPath(Section: Integer; const Path, Description: WideString; Comp: IHTTPServerComponent); stdcall;
var Page: THttpComponentPath;
begin
  Page:= THttpComponentPath.Create;
  Page.Path:= Path;
  Page.Description:= Description;
  Page.Component:= Comp;
  Page.Section:= Section;

  ComponentPaths.Add(Page);
end;

function TAbstractCommunications.HttpPathCount : Integer;
begin
  Result:= ComponentPaths.Count;
end;

function TAbstractCommunications.GetHttpPath(Index: Integer): THttpComponentPath ;
begin
  Result:= THttpComponentPath(ComponentPaths[Index]);
end;

procedure TAbstractCommunications.ProcessCGI(HFile : THANDLE; CGIPath, CGIRequest : WideString); stdcall;
  function FindHttpComponent(const CName: WideString): THttpComponentPath;
  var I: Integer;
  begin
    for I := 0 to HttpPathCount - 1 do begin
      Result:= GetHttpPath(I);
      if CompareText(Result.Path, CName) = 0 then begin
        Exit;
      end;
    end;
    Result:= nil;
  end;

var CGIP, TopLevel : WideString;
    CGITmp: string;
    HttpComp : THttpComponentPath;
    CGIR : TCGIStringList;
begin
  CGIP := CGIPath;
  CGITmp := CGIRequest;
  CGIR := TCGIStringList.Create;

  try
    if Length(CGITmp) > 0 then begin
      if CGITmp[1] = '?' then
        Delete(CGITmp, 1, 1);
      ReadHTTPParameters(CGITmp, CGIR);
    end;

    if Length(CGIP) > 0 then begin
      if CGIP[1] = '/' then
        Delete(CGIP, 1, 1);
    end;

    TopLevel := CNCTypes.ReadDelimitedW(CGIP, '/');
    if TopLevel <> '' then begin
      if CompareText(TopLevel, 'screenshot.jpg') = 0 then begin
        DoSnapShot(HFile, CNCInstaller.Handle);
      end else if  CompareText(TopLevel, 'component') = 0 then begin
        TopLevel := CNCTypes.ReadDelimitedW(CGIP, '/');
        HttpComp := FindHttpComponent(TopLevel);
        if Assigned(HttpComp) then begin
          if not HttpComp.Component.HTTPGeneratePage(HFile, CGIP, CGIR) then
            GenerateNoInformation(HFile, CGIP);
        end else begin
          GenerateFileNotFound(HFile, TopLevel);
        end;
      end else begin
        GenerateNoInformation(HFile, CGIP);
      end;
    end else begin
      GenerateDirectoryOfInstallation(HFile);
    end;
  finally
    WriteStringToHandle(hFile, '</body></html>');
    //CGIR:= nil;
  end;
end;

function TAbstractCommunications.AddStreaming(Stream : THandle; CGIPath, CGIRequest : WideString; DataStream : IHTTPServer; DT : Integer) : Boolean; stdcall;
var CGIR, TopLevel : string;
    ACNC : TAbstractCNC;
begin
  CGIR := CGIRequest;
  TopLevel := CNCTypes.ReadDelimited(CGIR, '.');
  if TopLevel <> '' then begin
    ACNC := TCNC.GetComponent(TopLevel);
    if Assigned(ACNC) then
      Result := ACNC.HTTPAddStreaming(Stream, CGIR, DataStream, DT)
    else
      Result := False;
  end else begin
    Result := False;
  end;
end;

procedure TAbstractCommunications.RemoveStreaming(Stream : THandle; CGIPath, CGIRequest : WideString); stdcall;
var CGIR, TopLevel : string;
    ACNC : TAbstractCNC;
begin
  CGIR := CGIRequest;
  TopLevel := CNCTypes.ReadDelimited(CGIR, '.');
  if TopLevel <> '' then begin
    ACNC := TCNC.GetComponent(TopLevel);
    if Assigned(ACNC) then
      ACNC.HTTPRemoveStreaming(Stream, CGIR);
  end;
end;

function TParamStrings.ReadBool (const Ident: string; Default: Boolean): Boolean;
var
  Val: String;
begin
  Val:= Values[Ident];
  if Val = '' then begin
    MessageLogFmt(ReadBoolNotProgrammed, [Ident, Ord(Default)]);
    Result := Default;
  end else if CompareText('false', Val) = 0 then begin
    Result := False
  end else if CompareText('true', Val) = 0 then begin
    Result := True
  end else begin
    try
      Result:= StrToInt(Val) <> 0
    except
      on EConvertError do begin
        MessageLogFmt(ReadBoolInvalid, [Ident]);
        Result:= Default
      end;
    end
  end;
end;

function TParamStrings.ReadInteger(const Ident: string; Default: Longint): Longint;
var
  Val: String;
begin
  Val:= Values[Ident];
  try
    if Val = '' then Abort;

    Result:= StrToInt(Val)
  except
    on EConvertError do begin
      Result:= Default;
      MessageLogFmt(ReadIntegerInvalid, [Ident]);
    end;

    on EAbort do begin
      Result := Default;
      MessageLogFmt(ReadIntegerNotProgrammed, [Ident, Default]);
    end;
  end
end;

{ Reads item name from section section in the specified ini file. The d item
  is returned if no value is read }
function TParamStrings.ReadString( const Ident, Default: string): string;
var Tmp : string;
begin
  if IndexOfName(Ident) = -1 then begin
    Result:= Default;
    MessageLogFmt(ReadStringNotProgrammed, [Ident, Default]);
  end else begin
    if Assigned(SysObj.Localized) then begin
      Tmp := Values[Ident];
      if (Length(Tmp) > 0) and (Tmp[1] = '$') then
        Result:= SysObj.Localized.GetString(Values[Ident])
      else
        Result:= Values[Ident];
    end else begin
      Result:= Values[Ident];
    end;
  end;
end;

{ Loads a colour value from this components section of the CNC.ini file. Logs to
  boot.log file any discrepancies }
function TParamStrings.ReadColour(const Ident : string; Default : TColor): TColor;
var
  tmpstr : string;
begin
  Result := Default;
  if IndexOfName(Ident) = -1 then begin
    MessageLogFmt(ReadColourNotProgrammed, [Ident, Default]);
    Exit;
  end;

  tmpstr := values[Ident];
  if Tmpstr = '' then begin
    MessageLogFmt(ReadColourNotProgrammed, [Ident, Default]);
    Exit;
  end;

  if TmpStr[1] = '$' then begin
    try
      Result := StrToInt(TmpStr);
    except
      on E : EConvertError do begin
        MessageLogFmt(ReadColourInvalid, [Ident]);
        Exit;
      end;
    end;
    Exit;
  end;

  if LowerCase(Copy(TmpStr,1, 2)) <> 'cl' then
    TmpStr := 'cl' + TmpStr;

  if not identtocolor(tmpstr, Integer(Result)) then begin
    MessageLogFmt(ReadColourInvalid, [Ident]);
    exit;
  end;
end;

{ Reads item name from section section in the specified ini file. The default item
  is returned if no value is read }
function TParamStrings.ReadDouble(const Ident : string; default : double) : double;
var tmpstr : string;
    code   : integer;
begin
  result := Default;
  TmpStr := ReadString(Ident, 'NAN');
  if tmpstr = 'NAN' then begin
    MessageLogFmt(ReadDoubleNotProgrammed, [Ident, Default]);
    Exit;
  end;

  Val(tmpstr, Result, code);
  if code <> 0 then begin
    MessageLogFmt(ReadDoubleInvalid, [Ident]);
    result := default;
  end;
end;



function TParamStrings.ReadIOSystem   (const Ident : string) : TGenericIOSubSystem;
var SystemType, Value : string;
    Accessor: IIOSystemAccessor;
begin
  Value := ReadString(Ident, '');
  SystemType := ReadDelimited(Value, ';');

  Result := TGenericIOSubSystem.Create(Ident);

  Accessor:= Sysobj.Comms.GetIOSystemAccessor(SystemType);
  Accessor.Initialise(Result, Value);
  if not Assigned(Accessor) then
    raise ECNCInstallFault.Create('Cannot create IO accessor: ' + SystemType);

  Result.SetAccessor(Accessor);
end;

function TParamStrings.ReadIODevice  (const Ident : string; aSubSystem : TGenericIOSubSystem) : TAbstractIODevice;
var Value : string;
    IODeviceClass : TAbstractIODeviceClass;
    DeviceType : string;
begin
  Value := ReadString(Ident, '');
  DeviceType := ReadDelimited(Value, ';');
  IODeviceClass := FindIODeviceClass(DeviceType);
  if IODeviceClass = nil then
    raise ECNCInstallFault.CreateFmt('Invalid Type Name [%s] in [%s]', [DeviceType, IDent]);
  Result := IODeviceClass.Create(Ident, Value, aSubSystem);
end;

procedure TParamStrings.ReadIOConnection(const Ident : string; aTag : TIntegerTag; Input : boolean);
var Value, DeviceName, Tmp : string;
    Device : TAbstractIODevice;
    Mask : Integer;
begin
  Value := ReadString(Ident, '');
  try
    DeviceName := ReadDelimited(Value, '.');
    Device := SysObj.Comms.FindDevice(DeviceName);
    if Device = nil then
      raise ECNCInstallFault.CreateFmt('IO Device Referenced does not exist [%s] in [%s]', [DeviceName, Ident]);

    aTag.Device  := Device;
    aTag.Address := StrToInt(ReadDelimited(Value, '.'));

    Tmp := ReadDelimited(Value, '.');
    case Tmp[1] of
      'b', 'B' : aTag.IOTagSize := varByte;
      'w', 'W' : aTag.IOTagSize := varSmallInt;
      'd', 'D' : aTag.IOTagSize := varInteger;
    else
      Mask := StrToInt(Tmp);
      aTag.Mask := 1 shl Mask;
      aTag.IOTagSize := varBoolean;
    end;

    if Input then
      aTag.IsWritable := False;

    Device.AddTag(aTag);

  except
    On E : EConvertError do
      raise ECNCInstallFault.CreateFmt('Invalid Connection Format [%s]', [Ident]);
  end;

  if Value <> '' then
    raise ECNCInstallFault.CreateFmt('Invalid Connection Format [%s]', [Ident]);
end;


function TParamStrings.ReadKeys : TKeyArray;
begin
  Result := ReadPrefixKeys('');
end;

function TParamStrings.ReadPrefixKeys(const Prefix : string) : TKeyArray;
var
  i   : TFunctionKey;
  keystr : string;
begin
  FillChar(Result, sizeof(TKeyArray), 0);
  for I := Low(TFunctionKey) to High(TFunctionKey) do begin
    Result[I].key  := nil;
    keystr := Prefix + 'F' + inttostr(Ord(i) + 1);
    Result[I].name := '';
    if IndexOfName(keystr + 'T') = -1 then continue;
    Result[I].Name      := ReadString(keystr + 'T', '');
    Result[I].Func      := ReadString(keystr + 'F', '');
    Result[I].KeyWord := ReadString(KeyStr + 'Keyword', '');
    if (Result[I].Keyword <> '') and not (SysObj.Keywords.Enable[Result[I].Keyword] = 0) then begin
      Result[I].Name := '';
      Result[I].Func := '';
    end;
  end;
end;

function TParamStrings.GetDelimitedText(Delimiter: Char): string;
var
  i: Integer;
begin
  Result := '';
  for i:= 0 to Count - 1 do
    if i = 0 then
      Result:= Trim(Get(i))
    else
      Result:= Result + Delimiter + Trim(Get(i))
end;

procedure TParamStrings.SetDelimitedText(Delimiter: Char; const Value: string);
var
  P, P1: PChar;
  S: string;
begin
  BeginUpdate;
  try
    Clear;
    P:= PChar(Value);
    P1:= P;
    while P^ <> #0 do
    begin
      if P^ = Delimiter then
      begin
        SetString(S, P1, P - P1);
        Add(Trim(S));
        P1:= P + 1;
      end;
      Inc(P)
    end;
    SetString(S, P1, P - P1);
    Add(Trim(S));
  finally
    EndUpdate;
  end
end;


{ Sets the installState to installStateIdle. If this procedure is overriden then it must
  be inherited }
constructor TCNC.Create(AOwner : TComponent);
begin
  inherited create(AOwner);
  FinstallState := InstallStateIdle;
  Caption       := ClassName;
  visible       := False;
//  parentFont    := True;
end;

destructor TCNC.Destroy;
begin
  MessageLog('Destroying ' + Name);
  inherited Destroy;
end;

{ Sets the installState to installStateActive. If this procedure is overriden then it must
  be inherited }
procedure TCNC.InstallComponent(Params : TParamStrings);

begin
  if installState <> InstallStateIdle then begin
    MessageLog(Format(ComponentIsInstalled, [name]));
    raise EduplicateInstalation.Create(Format(componentIsInstalled, [name]));
  end;
  HandleNeeded;
  FinstallState := InstallStateActive;
  FClient := Params.ReadBool('DebugClient', False);

  if not FContractSpecificData then
    FContractSpecificData := Params.ReadBool('ContractSpecific', False);
  if not FMachineSpecificData then
    FMachineSpecificData := Params.ReadBool('MachineSpecific', False);
end;

{ Sets the installState to installStateDone. If this procedure is overriden then it must
  be inherited }
procedure TCNC.Installed;
begin
  FinstallState := InstallStateDone;
  TagEvent(NameReset, EdgeRising, TMethodTag, CNCReset);
  TagEvent(NameResetRW, EdgeRising, TMethodTag, CNCresetRW);
end;

procedure TCNC.CheckInstallation;
begin
end;

procedure TCNC.LoseFocus;
begin
end;


{ checks if the component is in a valid close down state. If it is not overriden
  it returns true }
function TCNC.IShutdownPermissive : boolean;
begin
  Result := true;
end;

{ Sets the installState to installStateClosed. If this procedure is overriden then it must
  be inherited }
procedure TCNC.IShutdown;
begin
  FinstallState := InstallStateClosed;
end;


procedure TCNC.CNCreset(aTag : TAbstractTag);
begin
end;

procedure TCNC.CNCresetRW(aTag : TAbstractTag);
begin
end;

function TCNC.ResetRewindComplete: Boolean;
begin
  Result:= True;
end;

function TCNC.ResetComplete: Boolean; 
begin
  Result:= True;
end;

function  TCNC.IsDualTag(aTag, Comp : TAbstractTag) : Boolean;
begin
  Result := aTag = Comp;
end;

function TCNC.OpenLiveStream(const Config : string) : TStream;
var Filename: string;
    Tmp: string;
begin
  if MachineSpecificData then
    Filename:= MachSpecPath + 'live\' + Config + '.Cfg'
  else if ContractSpecificData then
    Filename:= ContractSpecificPath + 'live\' + Config + '.Cfg'
  else
    Filename:= LivePath + Config + '.Cfg';

  try
    Tmp:= SysObj.ConfigParser.ParseString(Filename);
    Result:= TStringStream.Create(Tmp);
  except
    on E: Exception do
      EventLog('*** Config "' + Filename + '" Parse Error: ' + E.Message);
  end;
end;

procedure TCNC.ReadLive(S : TStringList; Config : string);
var Filename: string;
    Tmp: string;
begin
  if MachineSpecificData then
    Filename:= MachSpecPath + 'live\' + Config + '.Cfg'
  else if ContractSpecificData then
    Filename:= ContractSpecificPath + 'live\' + Config + '.Cfg'
  else
    Filename:= LivePath + Config + '.Cfg';

  try
    Tmp:= SysObj.ConfigParser.ParseString(Filename);
    S.Text:= Tmp;
  except
    on E: Exception do begin
      EventLog('*** Config "' + Filename + '" Parse Error: ' + E.Message);
      if(Self.InstallState >= InstallStateActive) then begin
        SysObj.FaultInterface.SetFault(Self, CoreFaults[cfSysFatal], ['Failed Preprop: "' + Filename + '" :' + E.Message], nil);
      end;
    end;
  end;
end;

procedure TCNC.WriteLive(S : TStringList; Config : string);
begin
  if MachineSpecificData then
    S.SaveToFile(MachSpecPath + 'live\' + Config + '.Cfg')
  else if ContractSpecificData then
    S.SaveToFile(ContractSpecificPath + 'live\' + Config + '.Cfg')
  else
    S.SaveToFile(LivePath + Config + '.Cfg');
end;

{ This returns a pointer to the named component if it is either a TCNC or a
  TAbstractDisplay component and it is installed }
class function  TCNC.GetComponent(Ident : string) : TAbstractCNC;
var i : integer;
begin
  Result := nil;
  i := SysObj.FCNCComponents.IndexOf(Ident);
  if i = -1 then exit;

  Result := TAbstractCNC(SysObj.FCNCComponents.Objects[i]);
end;


function TAbstractCommunications.InterlockedTag(aTag : TAbstractTag) : Boolean;
  function DefineStatusNames(Stat : TCNCStatusTypes) : string;
  begin
    Result := '';
    if cstMotion in Stat then
      Result := SysObj.Localized.GetString('$INMOTION') + ': ';
    if cstCycle in Stat then
      Result := Result + SysObj.Localized.GetString('$INCYCLE') + ': ';
    if cstHomed in Stat then
      Result := Result + SysObj.Localized.GetString('$HOMED') + ': ';
    if cstEditing in Stat then
      Result := Result + SysObj.Localized.GetString('$EDITING') + ': ';
  end;

  procedure FailedEvent(aText : string);
  begin
    EventLog('REJECTED: ' + '[' + aTag.Name + ']');
    SysObj.FaultInterface.SetFault(Self, CoreFaults[cfSysInterlock], [aText], nil);
  end;

begin
  // Interlock text must be made language independant.
  Result := True;
  if aTag.HasInterlock then begin
    with aTag.Interlock^ do begin
      if Active then begin
        if not (SysObj.Comms.CNCModes <= Modes) then begin
          FailedEvent(SysObj.Localized.GetString('$NOT_PREMITTED_MODE'));
          Exit;
        end;
        if (Inclusion * SysObj.Comms.StatusType) <> Inclusion then begin
          FailedEvent(Format(SysObj.Localized.GetString('$CONDITION_S_MUST_BE_PRESENT'), [DefineStatusNames(Inclusion - SysObj.Comms.StatusType)]));
          Exit;
        end;
        if (Exclusion - SysObj.Comms.StatusType) <> Exclusion then begin
          FailedEvent(Format(SysObj.Localized.GetString('$CONDITION_S_MUST_NOT_BE_PRESENT'), [DefineStatusNames(Exclusion - SysObj.Comms.StatusType)]));
          Exit;
        end;
        if (AccessLevels * Access) = [] then begin
//            SysObj.Comms.DoCallbacks(SysObj.Comms.AccessInterlock, EdgeRising);
//            if not (SysObj.Comms.AccessLevel in Access) then begin
            FailedEvent(SysObj.Localized.GetString('$NOT_PERMITTED_ACCESS'));
            Exit;
//            end;
        end;
        EventLog('[' + aTag.Name + '=' + aTag.AsShortString + ']');
      end;
    end;
  end;
  Result := False;
end;

{ Message handler for multi-threaded IOEvents this recieves IOEvents from the
  IODispatch and forward the event as a conventional callback }
function TAbstractCommunications.DispatchCallback(aTag : TAbstractTag; Callback : TCallback) : Boolean;
begin
  if not InterlockedTag(aTag)then begin
    Callback(aTag);
    Result := True;
  end else begin
    Result := False;
  end;
end;


function  TCNC.TagEvent(aName : string; edge :TedgeType; TC : TTagClass; Callback : TCallback) : TAbstractTag;
begin
  Result := SysObj.Comms.CommsTagEvent(Self, aName, edge, TC, Callback);
  if (InstallState <> InstallStateDone) and (Result = nil) then
    raise ECNCInstallFault.Create(Name + ': Attempting to RegisterEvent ' + aName + ' During Installation');
end;

function  TCNC.TagPublish(aName : string; TC : TTagClass) : TAbstractTag;
begin
  if not Client then
    Result := SysObj.Comms.CommsTagPublish(Self, aName, TC)
  else
    Result := SysObj.Comms.CommsTagPublish(SysObj.Comms, aName, TC)
end;

constructor TAbstractCommunications.Create(Aowner : Tcomponent);
begin
  inherited Create(Aowner);
  Frozen := False;
  NamedLayer := TTagList.Create;
  NamedLayer.Sorted := True;
  CallbackList := Tlist.Create;

  // IO Specific
  SubSystems  := TList.Create;
  Devices     := TList.Create;
  IOSweepList := TList.Create;
  FOPCList := TStringList.Create;
  FOPCList.Sorted := True;
  StreamingList := TList.Create;
  ComponentPaths := TList.Create;

  CallbackCriticalSection:= TCriticalSection.Create;
  CurrentCallbackList:= TStringList.Create;
  //CurrentCallbackList.Sorted:= False;

  SysObj.HttpServerHost:= Self;

  PSDE:= TPSDLoader.Create;
  AttachPlugins;
end;

destructor TAbstractCommunications.Destroy;
var Reg : TCNCRegistry;
    I : Integer;
begin
  FSDWebServer := nil;

  PSDE.Close;
  PSDE:= nil;

  if CNCInstaller.ShutdownComplete then begin
    Reg := TCNCRegistry.Create;
    try
      Reg.OpenKey(RegistryBase + '\Persistent', True);
      for I := 0 to NamedLayer.Count - 1 do begin
        if NamedLayer.Tag[I].IsPersistent then
          NamedLayer.Tag[I].WriteToRegistry(Reg);
      end;
    finally
      Reg.Free;
    end;
  end;

  {
  for I := 0 to Devices.Count - 1 do
    TObject(Devices[I]).Free;
  }
  {
  for I := 0 to SubSystems.Count - 1 do
    TObject(SubSystems[I]).Free;
  }
  DetachPlugins;

  while FOPCList.Count > 0 do begin
    FOPCList.Objects[0].Free;
    FOPCList.Delete(0);
  end;

  IOSweepList.Free;
  StreamingList.Free;
  Devices.Free;
  SubSystems.Free;

  Clean;
  inherited Destroy;
end;

procedure TAbstractCommunications.Clean;
begin
  while NamedLayer.Count > 0 do begin
    NamedLayer.Tag[0].Free;
    NamedLayer.Delete(0);
  end;
end;

procedure TAbstractCommunications.CNCReset(aTag : TAbstractTag);
//var I : Integer;
begin
{  for I := 0 to SubSystems.Count - 1 do
    TGenericIOSubSystem(SubSystems[I]).Reset; }
end;

procedure TAbstractCommunications.ReadOPC;
var S : TStreamQuoteTokenizer;
    Token : string;
    P : TOPCTagRecord;

  procedure ReadTag;
  var TagName, TagType, Alias : string;
      Input : Boolean;
      TC : TTagClass;
  begin
    S.ExpectedTokens(['=', '{']);
    TagType := '';
    TagName := '';
    Input := False;

    Token := LowerCase(S.Read);
    try
      while not S.EndOfStream do begin
        if Token = 'name' then
          TagName := S.ReadStringAssign
        else if Token = 'alias' then
          Alias := S.ReadStringAssign
        else if Token = 'input' then
          Input := S.ReadBooleanAssign
        else if Token = 'tagtype' then
          TagType := S.ReadStringAssign
        else if Token = '}' then
          Exit
        else
          raise Exception.CreateFmt('Unexpected Token [%s] at line [%s]', [Token, S.Line]);
        Token := LowerCase(S.Read);
      end;
    finally
      if (S.LastToken = '}') and
         (TagName <> '') and
         (TagType <> '') then begin
         TC := FindTagClass(TagType);
         if TC <> nil then begin
           P := TOPCTagRecord.Create;
           P.TagName := TagName;
           P.TagClass := TC;
           P.Input := Input;
           if Alias <> '' then
             P.NetAlias := Alias
           else
             P.NetAlias := TagName;

           FOPCList.AddObject(LowerCase(P.NetAlias), P);
         end;
      end;
    end;
  end;
begin
  inherited;
  S := TStreamQuoteTokenizer.Create;
  S.Seperator := '[]{}';
  S.QuoteChar := '''';
  S.CommentChar := '#';

  try
    try
      S.SetStream(OpenLiveStream('OPC'));
      S.ExpectedTokens(['OPC', '=', '{']);
      Token := LowerCase(S.Read);
      while not S.EndOfStream do begin
        if Token = 'tag' then
          ReadTag
        else if Token = '}' then
          Exit
        else
          raise Exception.CreateFmt('Unexpected Token [%s] at line [%s]', [Token, S.Line]);
        Token := LowerCase(S.Read);
      end;
    except
      on E : Exception do
        MessageLogFmt('%s failed to load configuration file "%s"', [Name, E.Message]);
    end;
  finally
    S.Free;
  end;
end;

procedure TranslateRewrite;
var srcname, destname: string;
    I: Integer;
    S: TStringList;
begin
  S:= TStringList.Create;
  try
    for I:= 0 to SysObj.Translate.Count - 1 do begin
      destname:= SysObj.Translate.Names[i];
      srcname:= SysObj.Translate.Values[destname];

      destname:= ProfilePath + destname;
      srcname:= ProfilePath + srcname;
      try
        S.Text:= SysObj.ConfigParser.ParseString(srcname);
        S.SaveToFile(destname);
      except
        EventLog('Failed to parse config: ' + srcname);
      end;
    end;
  finally
    S.Free;
  end;

end;

procedure TAbstractCommunications.InstallComponent(Params : TParamStrings);
var
  IOini : TCustomIniFile;
  I : Integer;
  Tmp : TAbstractTag;
begin
  inherited InstallComponent(Params);
  InstallableList.Sort;
  TranslateRewrite;

  EventLog('List of Available Components');
  EventLog(CoreCNC.InstallableList.Text);

  OPCSubscriptionCountTag := TagPublish('OPCSubscriptionCount', TIntegerTag);
  OPCConnectionCountTag := TagPublish('OPCConnectionCount', TIntegerTag);
  StringArrayUpdateCountTag := TagPublish('MDTCallbacks', TIntegerTag);

  CurrentCallbackReportTag:= TagPublish('CommunicationsCallbackReport', TIntegerTag);
  MaxLagTag := TagPublish('CommunicationsMaxLag', TIntegerTag);
  AverageLagSum := 0;
  AverageLagTag := TagPublish('CommunicationsAverageLag', TIntegerTag);
  BlockIOTag:= TagPublish('CommunicationBlockupdate', TIntegerTag);
  LastSweep := 0;

  ReadOPC;

  IOini := SysObj.ConfigParser.ParseIni(ProfilePath + IOMapName);
  try
    ReadIOMap(IOIni);
  finally
    IOini.Free
  end;

  Fake := TagPublish('ACNC32Fake', TMethodTag);
  PlcName := Params.ReadString('AbstractPlc', '');
  SweepTag := CommsTagPublish(Self, nameIOSweep, TIntegerTag);
  SweepTag.LoggingPermissive := False;

  SleepTime := Params.ReadInteger('UpdateRateMS', 20);
  if SleepTime < 10 then
    SleepTime := 10;

  AccessInterlock := TMethodTag(CommsTagPublish(SysObj.FaultInterface, NameAccessInterlock, TMethodTag));
  PluginPublish;

  IOHealthyTag := TagPublish('IOHealthy', TIntegerTag);
  IOHealthyTag.IsVCL := False;

  SysObj.Keywords.ReadConfig(ContractSpecificPath + 'Acnc32Options.cfg');

  for I := 0 to SysObj.Keywords.EnableCount - 1 do begin
    Tmp := CommsTagPublish(Self, '_Option' + SysObj.Keywords.EnableNames[I], TIntegerTag);
    Tmp.AsInteger := SysObj.Keywords.Enable[SysObj.Keywords.EnableNames[I]];
  end;

  AddComponentPath(HttpMaintenanceSection, Name, 'System Communications', Self);
  DynamicHelp.DynamicHelpSystem := TDynamicHelp.Create;
end;

procedure TAbstractCommunications.Freeze;
begin
  Frozen := True;
end;

function CoreClass(ClassType : TClass; var Visual : Boolean) : Boolean;
begin
  Result := False;
  Visual := False;

  while ClassType <> TObject do begin
    if ClassType = TAbstractCommunications then
      Result := True;
    if ClassType = TAbstractSoftkey then
      Result := True;
    if ClassType = TAbstractFileManager then
      Result := True;
    if ClassType = TAbstractNC then
      Result := True;
    if ClassType = TAbstractFaultInterface then
      Result := True;
    if ClassType = TAbstractLayoutManager then
      Result := True;
    if ClassType = TAbstractDisplay then
      Visual := True;
    if ClassType = TCNC then
      Exit;

    ClassType := ClassType.ClassParent;
  end;
end;


procedure HTTPTokenReplace(S : TStringList; ClassType : TAbstractCNCClass; DocumentationSrcPath : string);

  procedure InsertFile(Output : TStringList; const FileName : string);
  var T : TStringList;
  begin
    T := TStringList.Create;
    try
      if FileExists(FileName) then begin
        T.LoadFromFile(FileName);
        Output.AddStrings(T);
      end else begin
        ForceDirectories(ExtractFilePath(FileName));
        T.SaveToFile(FileName);
      end;
    finally
      T.Free;
    end;
  end;


var Core, Visual : Boolean;
    I, J : Integer;
    Tmp : TStringList;
begin
  Tmp := TStringList.Create;
  try
    if ClassType <> nil then begin
      Core := CoreClass(ClassType, Visual)
    end else begin
      Core := False;
    end;

    I := 0;
    while I < S.Count do begin
      if CompareText(S[I], '$CLASSNAME$') = 0 then begin
        Tmp.Add(ClassType.ClassName);
      end else if CompareText(S[I], '$VISUAL$') = 0 then begin
        if Visual then Tmp.Add('X') else Tmp.Add('&nbsp');
      end else if CompareText(S[I], '$CORECOMPONENT$') = 0 then begin
        if Core then Tmp.Add('X') else Tmp.Add('&nbsp');
      end else if CompareText(S[I], '$DEPRECATED$') = 0 then begin
        Tmp.Add('&nbsp');
      end else if CompareText(S[I], '$FILE.DESCRIPTION$') = 0 then begin
        InsertFile(Tmp, DocumentationSrcPath + 'description.txt');
      end else if CompareText(S[I], '$FILE.NAMEDINTERFACE$') = 0 then begin
        InsertFile(Tmp, DocumentationSrcPath + 'namedinterface.txt');
      end else if CompareText(S[I], '$FILE.CONFIGURATION$') = 0 then begin
        InsertFile(Tmp, DocumentationSrcPath + 'configuration.txt');
      end else if CompareText(S[I], '$FILE.KNOWNPROBLEMS$') = 0 then begin
        InsertFile(Tmp, DocumentationSrcPath + 'knownproblems.txt');
      end else if CompareText(S[I], '$COMPONENTINDEX$') = 0 then begin
        for J := 0 to InstallableList.Count - 1 do begin
          Tmp.Add(Format('<h4><a href="%s.html">%s</a>', [TClass(InstallableList.Objects[J]).ClassName,
                                                    TClass(InstallableList.Objects[J]).ClassName]));
        end;
      end else
        Tmp.Add(S[I]);

      Inc(I);
    end;
    S.Clear;
    S.AddStrings(Tmp);
  finally
    Tmp.Free;
  end;
end;

procedure WriteHTMLDescriptor(ClassType : TAbstractCNCClass);
var S : TStringList;
begin
  S := TStringList.Create;
  try
    S.LoadFromFile('\usr\acnc32\doc\componenttemplate.txt');
    HTTPTokenReplace(S, ClassType, '\usr\acnc32\doc\' + ClassType.ClassName + '\');
    S.SaveToFile(ProfilePath + 'acnc32.doc\components\' + ClassType.ClassName + '.html');
  finally
    S.Free;
  end;
end;

{
procedure WriteHTMLComponentIndex;
var S : TStringList;
    I : Integer;
begin
  CreateDirectory(PChar(ProfilePath + 'acnc32.doc/components/'), nil);
  if AutoDocument then begin
    for I := 0 to InstallableList.Count - 1 do begin
      WriteHTMLDescriptor(TAbstractCNCClass(InstallableList.Objects[I]));
    end;

    S := TStringList.Create;
    try
      S.LoadFromFile('\usr\acnc32\doc\componentindextemplate.txt');
      HTTPTokenReplace(S, TAbstractCommunications, '');
      S.SaveToFile(ProfilePath + 'acnc32.doc/components/index.html');
    finally
      S.Free;
    end;
  end;
end;
 }

{ Runs through this named layer and reports any names that are used }
procedure TAbstractCommunications.Installed;
var Reg : TCNCRegistry;
    I : Integer;
begin
  inherited Installed;

  IOResetTag:= Self.TagEvent('IOSubSystemReset', EdgeFalling, TIntegerTag, ResetIOSubSystems);
  //WriteHTMLComponentIndex;

  Fake.AsBoolean := CNC32.FakeIOSystem or FakeNCSystem;
  if PlcName <> '' then begin
    FPlc := GetComponent(PlcName) as TAbstractPLC;
    if Plc = nil then
      raise ECNCINstallFault.Create(PlcName + ' Is Required by IO System');
  end;

  Reg := TCNCRegistry.Create;
  try
    Reg.OpenKey(RegistryBase + '\Persistent', True);
    for I := 0 to NamedLayer.Count - 1 do begin
      if NamedLayer.Tag[I].IsPersistent then
        NamedLayer.Tag[I].ReadFromRegistry(Reg);
    end;
  finally
    Reg.Free;
  end;

  AccesssTag := TagEvent(nameAccessLevels, EdgeChange, TIntegerTag, UpdateData);
  with NCPublishedF[ncpActiveMode] do
    ModeTag := TagEvent(Format(Tag, [1]), edgeChange, Size, UpdateData);
  with NCPublishedF[ncpInMotion] do
    MotionTag := TagEvent(Format(Tag, [1]), edgeChange, Size, UpdateData);
  with NCPublishedF[ncpHomed] do
    HomedTag := TagEvent(Format(Tag, [1]), edgeChange, Size, UpdateData);
  with NCPublishedF[ncpInCycle] do
    CycleTag := TagEvent(Format(Tag, [1]), edgeChange, Size, UpdateData);
  EditTag := TagEvent(nameSectionEditingMode,        edgeChange, TIntegerTag, UpdateData);
  AccessUserNameTag := TagEvent(NameAccessUserName,  edgeChange, TIntegerTag, nil);

  PluginRegister;

  for I := 0 to SubSystems.Count - 1 do
    TGenericIOSubSystem(SubSystems[I]).CheckValidity;

  TagEvent(NameNewActiveFile, EdgeChange, TMethodTag, PluginNewActiveFile);
  TagEvent(NameDisplaySweep, EdgeChange, TMethodTag, UpdateStreaming);
  TagEvent('ACNC32About', EdgeFalling, TMethodTag, DisplayAbout);


  ResetMaxLagTag := TagEvent('CommunicationsResetLag', EdgeFalling, TMethodTag, ResetLagValues);



  for I:= 0 to DelayedFatalError.Count - 1 do begin
    SysObj.FaultInterface.SetFault(Self, CoreFaults[cfSysFatal], [DelayedFatalError[I]], nil);
  end;

end;

procedure TAbstractCommunications.ResetLagValues(aTag : TAbstractTag);
begin
  MaxLagTag.AsInteger := 0;
  AverageLagSum := 0;
  AverageLagCount := 0;
  AverageLagTag.AsInteger := 0;
  LastSweep := CNC32.PlcTick.I64;
end;

procedure TAbstractCommunications.ResetIOSubSystems(ATag: TAbstractTag);
var I: Integer;
    IOS: TGenericIOSubSystem;
begin
  if not ATag.AsBoolean then
    for I:= 0 to SubSystems.Count - 1 do begin
      IOS:= TGenericIOSubSystem(SubSystems[I]);
      if not IOS.Healthy then
        IOS.Reset;
    end;
end;

procedure TAbstractCommunications.UpdateTick;
var ThisTick : Cardinal;
begin
  ThisTick := Windows.GetTickCount;
  if CNC32.PlcTick.Low > ThisTick then
    Inc(CNC32.PlcTick.Hi);
  CNC32.PlcTick.Low := ThisTick;
end;


procedure TAbstractCommunications.DisplayAbout(aTag : TAbstractTag);
begin
  EventLog('About dialogue');
  TAbout.Execute;
end;

procedure TAbstractCommunications.CheckInstallation;
var I : Integer;
    P : TOPCTagRecord;
    aTag : TAbstractTag;
begin
  inherited;
  for I := 0 to FOPCList.Count - 1 do begin
    P := TOPCTagRecord(FOPCList.Objects[I]);
    aTag := FindTag(P.TagName);
    if not Assigned(aTag) then
      aTag := TagEvent(P.TagName, EdgeChange, P.TagClass, nil)
{    else
      Inc(TTT)};
    P.Tag := aTag;
    aTag.NetworkAlias := P.NetAlias;
    P.TagClass := TTagClass(aTag.ClassType);
    P.Input := aTag.IsWritable and not aTag.IsOwned;
  end;
end;

{ Shuts down gracefully the Active IO Thread }
procedure TAbstractCommunications.IShutdown;
var I : Integer;
begin
  Frozen:= True;

  if Assigned(FSDWebServer) then
    FSDWebServer.Shutdown;

  for I := 0 to SysObj.PluginList.Count - 1 do begin
    if Assigned(SysObj.PluginList.Objects[I]) then
      TNamedPlugin(SysObj.PluginList.Objects[I]).DoShutdown;
  end;

  for I := 0 to SubSystems.Count - 1 do
    TGenericIOSubSystem(SubSystems[I]).Shutdown;

  //DetachPlugins;

  Terminate;
  inherited IShutdown;
end;

procedure TAbstractCommunications.CheckCallbacks;
var
  I : integer;
begin
  for I := 0 to NamedSize - 1 do begin
    if NamedLayer.Tag[I].Count = 0 then begin
      MessageLog(Format(NoCallbackSet, [NamedLayer.Tag[i].Name]));
    end;
  end;
end;

function TAbstractCommunications.CommsTagPublish(aOwner : TCNC; aName : string; TC : TTagClass) : TAbstractTag;
begin
  if Length(aName) >= MaxNamedLength then
    raise ECNCInstallFault.CreateFmt(nameTooLong, [aName]);

  Result := FindTag(aName);
  if Result = nil  then begin
    Result := TC.Create(aOwner, aName);
    Result.IsSystem := True;
    Result.IsOwned := aOwner <> Self;
    Self.AddNamedLayerTag(Result);
    Result.ReadAccessFromRegistry;
  end else begin
    if (Result.IsOwned and (aOwner <> Result.Owner)) or not Result.IsWritable then
      raise ECNCInstallFault.CreateFmt('Cannot take ownership of [%s] it is already owned by [%s]', [aName, Result.Owner.Name]);
  end;

  if aOwner <> Self then
    Result.IsOwned := True;

  Result.IsVCL := Result.IsOwned; // Simple rule if a component is publishing
                                // then connect to vcl otherwise dont.
end;

function TAbstractCommunications.CommsTagEvent(aCaller : TCNC; aName : string;
         edge :TedgeType; TC : TTagClass; Callback : TCallback) : TAbstractTag;
begin
  if Length(aName) >= MaxNamedLength then
    raise ECNCInstallFault.CreateFmt(nameTooLong, [aName]);

  Result := FindTag(aName);
  if Result = nil then begin
    Result := TC.Create(Self, aName);
    Result.IsSystem := True;
    Self.AddNamedLayerTag(Result);
    Result.ReadAccessFromRegistry;
    MessageLogFmt('Tag [%s] does not exist, Callback has created [%s] type', [Result.Name, Result.ClassName]);
  end;

  if Assigned(Callback) then begin
    if CompareText(aName, 'DisplaySweep') = 0 then
      EventLogFmt('DisplaySweep callback %d: [%s] ', [Result.Count, SysObj.InstallComp]);
    Result.AddCallback(Callback, Edge, Self.Handle);
  end;
end;

{ Funtions checks that the requested name is unique (binding error on failure)
  if the current size of the callback array is too small then the array is
  resized and added }
function TAbstractCommunications.AddNamedLayerTag(aTag : TAbstractTag) : TTagHandle;
begin
  if FindIndex(aTag.Name) <> -1 then
    raise ECNCInstallFault.CreateFmt(nameIsNotUnique, [aTag.Name]);

  Result := NamedLayer.AddTag(aTag);
end;

{ Find searches the NamedLayer for the requested name. }
function TAbstractCommunications.FindIndex(aName : string) : Integer;
var
  i : integer;
begin
  for i := 0 to NamedSize - 1 do begin
    if CompareText(aName, NamedLayer.Tag[i].Name) = 0 then begin
      Result := I;
      Exit;
    end;
  end;
  Result := -1;
end;

function TAbstractCommunications.FindTag(aName : string) : TAbstractTag;
var I : Integer;
begin
  for I := 0 to NamedSize - 1 do begin
    Result := NamedLayer.Tag[i];
    if CompareText(aName, Result.Name) = 0 then begin
      Exit;
    end;
  end;
  for I := 0 to NamedSize - 1 do begin
    Result := NamedLayer.Tag[i];
    if CompareText(aName, Result.NetWorkAlias) = 0 then begin
      Exit;
    end;
  end;
  Result := nil;
end;

function TAbstractCommunications.IOFindTag(aName : string) : TMethodTag;
var Tmp : TAbstractTag;
begin
  Tmp := FindTag(aName);
  if (Tmp <> nil) and (Tmp is TMethodTag) then
    Result := TMethodTag(Tmp)
  else
    Result := nil;
end;

function TAbstractCommunications.GetTagIndex(Index: Integer): TAbstractTag;
begin
  Result:= NamedLayer.Tag[Index]
end;

function TAbstractCommunications.AddPlaceHolderTag(aName : string; aSize : TTagClass) : TAbstractTag;
begin
  Result := aSize.Create(Self, aName);
  NamedLayer.AddTag(Result);
end;

//const ValidIntegerTypes =  [IOSizeBit..IOSizeDWord];

function  TAbstractCommunications.GetNamedSize : Integer;
begin
  Result := NamedLayer.Count;
end;


function TAbstractCommunications.IOSizeToString(size : Integer) : string;
begin
  case Size of
    varEmpty    : Result := 'Empty';
    varNull     : Result := 'Null';
    varSmallint : Result := 'SmallInt';
    varInteger  : Result := 'Integer';
    varSingle   : Result := 'Single';
    varDouble   : Result := 'Double';
    varCurrency : Result := 'Currency';
    varDate     : Result := 'Date';
    varOleStr   : Result := 'OleStr';
    varDispatch : Result := 'Dispatch';
    varError    : Result := 'Error';
    varBoolean  : Result := 'Boolean';
    varVariant  : Result := 'Variant';
    varUnknown  : Result := 'Unknown';
    varByte     : Result := 'Byte';
    varStrArg   : Result := 'StrArg';
    varString   : Result := 'String';
    varAny      : Result := 'Any';
    varTypeMask : Result := 'TypeMask';
    varArray    : Result := 'Array';
    varByRef    : Result := 'ByRef';
  end;

//  Result:= TypInfo.GetEnumName(TypeInfo(TIOSize), Integer(Size)) {MGL 13/5}
end;


function TAbstractCommunications.ReadNamedTag  (aName : string) : TAbstractTag;
begin
  Result := FindTag(aName);
end;


procedure TAbstractCommunications.Terminate;
begin
  SysObj.FaultInterface.SetFault(Self, CoreFaults[cfShuttingDown], [], nil);
  if not IOThread.Suspended then begin
    Sleep(100);
    IOThread.Terminate;
    IOThread.WaitFor
  end;
end;

{ This is called from the IOThread Execute method }
procedure TAbstractCommunications.Sweep;
var I   : integer;
    Tmp : Integer;
begin
  FPlcSweep := FPlcSweep + 1;
  if Frozen then
    Exit;

  Tmp := 0;
  for I := 0 to SubSystems.Count - 1 do begin
    TGenericIOSubSystem(SubSystems[I]).UpdateRealToShadow;
    Tmp := Tmp + Ord(TGenericIOSubSystem(SubSystems[I]).Healthy);
  end;
  IOHealthyTag.AsBoolean := Tmp = SubSystems.Count;

  for I := 0 to IOSweepList.Count - 1 do
    PCallbackRecord(IOSweepList[I])^.Callback(SweepTag);

  UpdateTick;

  if PLC <> nil then
    PLC.Execute; // Only for ObjectPLC

  SweepTag.AsInteger := FPlcSweep;

  //--------------- Sweep metric ---------------------
  if CNC32.PlcTick.I64 - LastSweep > MaxLagTag.AsInteger then begin
    MaxLagTag.AsInteger := CNC32.PlcTick.I64 - LastSweep;
  end;

  if AverageLagCount >= 10 then begin
    AverageLagTag.AsInteger := AverageLagSum div 10;
    AverageLagSum := 0;
    AverageLagCount := 0;
  end;

  AverageLagSum := AverageLagSum + CNC32.PlcTick.I64 - LastSweep;
  Inc(AverageLagCount);

  LastSweep := CNC32.PlcTick.I64;
  //--------------- END Sweep metric ------------------


  if BlockOutputTranslation then begin
    BlockIOTag.AsInteger:= BlockIOTag.AsInteger + 1;
    Exit;
  end;

  for I := 0 to SubSystems.Count - 1 do
    TGenericIOSubSystem(SubSystems[I]).UpdateShadowToReal;
end;


procedure TAbstractCommunications.ReadIOMap(Ini : TCustomIniFile);
var Systems, Devs, Connections : TParamStrings;
    S, D, C : Integer;
    System : TGenericIOSubSystem;
    Device : TAbstractIODevice;
    aTag : TIntegerTag;
begin
  Systems := TParamStrings.Create;
  Devs := TParamStrings.Create;
  Connections := TParamStrings.Create;
  try
    Ini.ReadSectionValues('Systems', Systems);
    for S := 0 to Systems.Count - 1 do begin
      System := Systems.ReadIOSystem(Systems.Names[S]);
      SubSystems.Add(System);

      Devs.Clear;
      Ini.ReadSectionValues(Systems.Names[S], Devs);
      for D := 0 to Devs.Count - 1 do begin
        Device := Devs.ReadIODevice(Devs.Names[D], System);
        Device.SubSystem := System;
        Self.AddDevice(Device);
      end;
    end;

    Connections.Clear;
    Ini.ReadSectionValues('Inputs', Connections);
    for C := 0 to Connections.Count - 1 do begin
      aTag := TIntegerTag.Create(Self, Connections.Names[C]);
      aTag.IsWritable := False;
      aTag.IsConnected := True;
      aTag.IsSystem := True;
      aTag.IsIO := True;
      aTag.IsVCL := False;
      Connections.ReadIOConnection(Connections.Names[C], aTag, True);
      AddNamedLayerTag(aTag);
    end;

    Connections.Clear;
    Ini.ReadSectionValues('Outputs', Connections);
    for C := 0 to Connections.Count - 1 do begin
      aTag := TIntegerTag.Create(Self, Connections.Names[C]);
      aTag.IsSystem := True;
      aTag.IsIO := True;
      Connections.ReadIOConnection(Connections.Names[C], aTag, False);
      AddNamedLayerTag(aTag);
    end;

//    DefineAliasTags;

  finally
    Devs.Free;
    Connections.Free;
    Systems.Free;
  end;
end;


procedure TAbstractCommunications.DefineAliasTags;
var S, D, C, A : Integer;
    System : TGenericIOSubSystem;
    Device : TAbstractIODevice;
    aTag, AliasTag : TIntegerTag;
begin
  // For Each SubSystem in IO System
  for S := 0 to SubSystems.Count - 1 do begin
    System := TGenericIOSubSystem(SubSystems[S]);

    // For Each Device in SubSystem
    for D := 0 to System.DeviceCount - 1 do begin
      Device := System.GetDevice(D);

      // For Each Connection in Device
      for C := 0 to Device.TagCount - 1 do begin
        aTag := TIntegerTag(Device.GetTag(C));

        // Check This Tag against each other Tag in This device
        for A := 0 to Device.TagCount - 1 do begin
          AliasTag := TIntegerTag(Device.GetTag(A));
          if aTag <> AliasTag then begin

            // Check is this is an alias
            if aTag.AliasOf(AliasTag) then begin
              if Assigned(AliasTag.AliasedNotUsedYet) then
                raise ECNCInstallFault.CreateFmt('Aliasing Too Deep [%s], [%s]',
                      [aTag.Name, AliasTag.Name]);
              aTag.AliasedNotUsedYet := AliasTag
            end;
          end;
        end;
      end;
    end;
  end;
end;

procedure TAbstractCommunications.AttachPlugins;
var Plugin : TNamedPlugin;
    I, J : Integer;
    HMod : HMODULE;
    Editor : TSectionEditorPlugin;
    PName : string;
    Tmp : string;
begin
  for I := 0 to SysObj.PluginList.Count - 1 do begin
    PName := SysObj.PluginList.Names[I];
    HMod := LoadLibrary(PChar(ProfilePath + 'lib\' + PName));
    try
      if HMod = 0 then
        raise ECNCInstallFault.CreateFmt('Plugin [%s] not found', [PName]);

      Tmp := '';
      if SysObj.PluginList.Values[PName] <> '1' then begin
        Tmp := SysObj.PluginList.Values[PName];
        SysObj.PluginList[I] := ReadDelimited(Tmp, ',');
        Tmp := ReadDelimited(Tmp, ',');
      end else begin
        SysObj.PluginList[I] := PName;
      end;

      Plugin := TNamedPlugin.Create(HMod, Self, ReadDelimited(Tmp, ','));
      SysObj.PluginList.Objects[I] := Plugin;
      for J := 0 to Plugin.EditorList.Count - 1 do begin
        Editor := TSectionEditorPlugin.Create(SysObj.VisualParent);
        Editor.Parent := SysObj.VisualParent;
        Editor.Name := 'Editor' + Plugin.EditorList[J];
        Editor.HMod := HMod;
        Editor.Section := Plugin.EditorList[J];
        SysObj.FCNCComponents.AddObject(Editor.Name, Editor);
      end;
      PSDE.AddDll(HMod);
    except
      on E: Exception do begin
        DelayedFatalError.Add('Plugin failed to initialize plugin: ' + PName + CarRet + E.Message);
      end;
    end;

  end;
end;

procedure TAbstractCommunications.DetachPlugins;
var I : Integer;
begin
  for I := 0 to SysObj.PluginList.Count - 1 do begin
    if Assigned(SysObj.PluginList.Objects[I]) then
      TNamedPlugin(SysObj.PluginList.Objects[I]).Free;
  end;
end;

function TAbstractCommunications.FindPlugin(aName : string) : HModule;
var I : Integer;
begin
  for I := 0 to SysObj.PluginList.Count - 1 do begin
    if CompareText(SysObj.PluginList[I], aName) = 0 then begin
      if Assigned(SysObj.PluginList.Objects[I]) then begin
        Result := TNamedPlugin(SysObj.PluginList.Objects[I]).HMod;
        Exit;
      end;
    end;
  end;
  Result := 0;
end;


procedure TAbstractCommunications.PluginPublish;
var I : Integer;
begin
  for I := 0 to SysObj.PluginList.Count - 1 do begin
    if Assigned(SysObj.PluginList.Objects[I]) then
      TNamedPlugin(SysObj.PluginList.Objects[I]).DoPublish;
  end;
end;

function TAbstractCommunications.PluginOperatorDialogue(const Breed, Item : WideString): TNCDialogueResult;
var I : Integer;
begin
  for I := 0 to SysObj.PluginList.Count - 1 do begin
    if TNamedPlugin(SysObj.PluginList.Objects[I]).ContainsOperatorDialogue(Breed) then begin
      if Assigned(SysObj.PluginList.Objects[I]) then begin
        Result := TNamedPlugin(SysObj.PluginList.Objects[I]).OperatorDialogue(Breed, Item);
        Exit;
      end;
    end;
  end;
  Result := ncdrError;
end;

procedure TAbstractCommunications.PluginOperatorDialogueClose;
var I : Integer;
begin
  for I := 0 to SysObj.PluginList.Count - 1 do begin
    if Assigned(SysObj.PluginList.Objects[I]) then
      TNamedPlugin(SysObj.PluginList.Objects[I]).OperatorDialogueClose;
  end;
end;

procedure TAbstractCommunications.PluginRegister;
var I : Integer;
begin
  for I := 0 to SysObj.PluginList.Count - 1 do begin
    if Assigned(SysObj.PluginList.Objects[I]) then
      TNamedPlugin(SysObj.PluginList.Objects[I]).DoRegister;
  end;
end;

procedure TAbstractCommunications.PluginNewActiveFile(aTag : TAbstractTag);
var I : Integer;
begin
  for I := 0 to SysObj.PluginList.Count - 1 do begin
    if Assigned(SysObj.PluginList.Objects[I]) then
      TNamedPlugin(SysObj.PluginList.Objects[I]).DoConsume;
  end;
end;

procedure TAbstractCommunications.PluginSetJNI(AJNIEnv: PJNIEnv);
var I : Integer;
begin
  Sysobj.JNIEnv:= AJNIEnv;
  for I := 0 to SysObj.PluginList.Count - 1 do begin
    if Assigned(SysObj.PluginList.Objects[I]) then
      TNamedPlugin(SysObj.PluginList.Objects[I]).SetJNIEnv(AJNIEnv);
  end;
end;


function TAbstractCommunications.OKToLoadFile : Boolean;
var I : Integer;
begin
  Result := False;
  for I := 0 to SysObj.PluginList.Count - 1 do begin
    if Assigned(SysObj.PluginList.Objects[I]) then
      if TNamedPlugin(SysObj.PluginList.Objects[I]).IsDirty then
        Exit;
  end;
  Result := True;
end;

{ This is a bit of a nightmare, only one section may declare itself as not ok to edit
  if more than one section is in this state you will never be able to edit
  without acknowledging the loss of data! }
function TAbstractCommunications.OKToEditSection(SectionName : string) : Boolean;
var I : Integer;
begin
  Result := False;
  for I := 0 to SysObj.PluginList.Count - 1 do begin
    if Assigned(SysObj.PluginList.Objects[I]) then
      if not TNamedPlugin(SysObj.PluginList.Objects[I]).OKToEditSection(SectionName) then
        Exit;
  end;
  Result := True;
end;


const
  SystemProfileName = '[Systems]' + CarRet;
  InputProfileName = '[Inputs]' + CarRet;
  OutputProfileName = '[Outputs]' + CarRet;

procedure TAbstractCommunications.WriteIOMap(Stream : TStream);
  procedure WriteString(aText : string);
  begin
    Stream.Write(pChar(aText)^, Length(aText));
  end;

var S, D, C : Integer;
    System : TGenericIOSubSystem;
    Device : TAbstractIODevice;
    aTag : TIntegerTag;
begin
    // Write the system profile
    WriteString(SystemProfileName);
    for S := 0 to SubSystems.Count -1 do begin
      System := TGenericIOSubSystem(SubSystems[S]);
      WriteString( FixedSpace(25, System.Name) + '= ' +
                   System.TextDefinition + CarRet);
    end;

    WriteString(CarRet);

    // Write The DeviceList attached to each system
    for S := 0 to SubSystems.Count -1 do begin
      System := TGenericIOSubSystem(SubSystems[S]);
      WriteString(CarRet + '[' + System.Name + ']' + CarRet);

      for D := 0 to System.DeviceCount - 1 do begin
        Device := System.GetDevice(D);
        WriteString( FixedSpace(25, Device.Name) + '= ' +
                     Device.TextDefinition + CarRet);
      end;
    end;

    WriteString(CarRet);
    WriteString(InputProfileName);

    for S := 0 to SubSystems.Count - 1 do begin
      System := TGenericIOSubSystem(SubSystems[S]);
      WriteString(';**' + System.Name);

      for D := 0 to System.DeviceCount - 1 do begin
        Device := System.GetDevice(D);
        if Device.InputSize > 0 then
          WriteString(CarRet);
        for C := 0 to Device.TagCount - 1 do begin
          aTag := TIntegerTag(Device.GetTag(C));
          if not aTag.IsWritable then
            WriteString( FixedSpace(25, aTag.Name) +
                         '= ' + aTag.TextDefinition + CarRet);
        end;
      end;
    end;


    WriteString(CarRet);
    WriteString(OutputProfileName);

    for S := 0 to SubSystems.Count - 1 do begin
      System := TGenericIOSubSystem(SubSystems[S]);
      WriteString(';**' + System.Name);

      for D := 0 to System.DeviceCount - 1 do begin
        Device := System.GetDevice(D);
        if Device.OutputSize > 0 then
          WriteString(CarRet);
        for C := 0 to Device.TagCount - 1 do begin
          aTag := TIntegerTag(Device.GetTag(C));
          if aTag.IsWritable then
            WriteString( FixedSpace(25, aTag.Name) +
                         '= ' + aTag.TextDefinition + CarRet);
        end;
      end;
    end;
end;


function TAbstractCommunications.InterpretToken(const aToken : string) : string;
var aTag : TAbstractTag;
    Tmp : string;
begin
  Result := aToken;
  Tmp := ReadDelimited(Result, '.');
  if CompareText(Tmp, 'namedlayer') = 0 then begin
    aTag := SysObj.Comms.FindTag(Result);
    if Assigned(aTag) then
      Result := aTag.AsString
    else
      Result := Format('--TAG NOT FOUND [%s]--', [Result]);
  end else
    Result := inherited InterpretToken(aToken);
end;


function TAbstractCommunications.HTTPGeneratePage(HFile : THANDLE; var CGIPath : WideString; CGIRequest : ICGIStringList) : Boolean;
var I : Integer;
    Tmp : string;
    aTag : TAbstractTag;
begin
  Result := inherited HTTPGeneratePage(HFile, CGIPath, CGIRequest);
  if not Result then begin
    Tmp := ReadDelimitedW(CGIPath, '/');
    if Tmp = '' then begin
      Result := True;
      WriteACNC32HTTPTitle(hFile, 'IO');
      WriteACNC32HTTPHeading(hFile, 2, 'NameLayer');
      WriteStringToHandle(hFile, '<table border="1" align="center" width="80%">' + CarRet);
      WriteStringToHandle(hFile, '<tr><th>Name</th><th>Value</th></tr>' + CarRet);
      WriteStringToHandle(hFile, '<tr><td><a href="IO/StringArray">StringArray''s</a></td></tr>' + CarRet);
      for I := 0 to NamedSize - 1 do begin
        WriteStringToHandle(hFile, Format('<tr><td>%s</td><td>%s</td></tr>' + CarRet, [NamedLayer.Tag[I].Name, NamedLayer.Tag[I].AsString]));
      end;
      WriteStringToHandle(hFile, '</table>' + CarRet);
//      WriteStringToHandle(hFile, '</body></html>' + CarRet);
    end else if CompareText(Tmp, 'StringArray') = 0 then begin
      Result := SysObj.StringArraySet.HTTPGeneratePage(HFile, CGIPath, CGIRequest);
    end else if CompareText(Tmp, 'modifyrequest') = 0 then begin //????? do i want to do this ?
      Result := False;
    end else if CompareText(Tmp, 'modify') = 0 then begin //????? do i want to do this ?
      Tmp := CGIRequest.GetValue('name');
      aTag := Self.FindTag(Tmp);
      if Assigned(ATag) then begin
        aTag.AsString := CGIRequest.GetValue('value');
        WriteACNC32HTTPHeading(hFile, 2, 'NameLayer');
        WriteStringToHandle(hFile, Format('<p>New value of %s is %s</p>', [aTag.Name, aTag.AsString]));
        Result := True;
      end;
    end else begin
      Result := False;
    end;
  end;
end;

function TAbstractCommunications.HTTPAddStreaming(Stream : THandle; CGIR : WideString; Method : IHTTPServer; DT : Integer) : Boolean;
var DS : THTTPDataStream;
    aTag : TAbstractTag;
    Tmp : string;
    I : Integer;
begin
  for I := StreamingList.Count - 1 downto 0 do begin
    DS := THTTPDataStream(StreamingList[I]);
    if DS.Dead then begin
      DS.Free;
      StreamingList.Delete(I);
    end;
  end;

  Result := False;
  Tmp := CGIR;
  aTag := nil;
  if CompareText(ReadDelimited(Tmp, '.'), 'namedlayer') = 0 then begin
    aTag := SysObj.Comms.FindTag(Tmp);
    if not Assigned(aTag) then
      Exit;
  end;

  DS := THTTPDataStream.Create;
  DS.Name := CGIR;
  DS.Stream := Stream;
  DS.Callback := Method;
  DS.Data := aTag;
  DS.DT := DT;
  DS.LastTick := CNC32.PlcTick.I64;
  DS.Dead := False;
  Self.StreamingList.Add(DS);
  Result := True;
end;

procedure TAbstractCommunications.HTTPRemoveStreaming(Stream : THandle; CGIR : WideString);
var DS : THTTPDataStream;
    I : Integer;
begin
  for I := 0 to StreamingList.Count - 1 do begin
    DS := THTTPDataStream(StreamingList[I]);
    if not (DS.Dead) and (DS.Name = CGIR) and (DS.Stream = Stream) then begin
      DS.Dead := True;
      Exit;
    end;
  end;
end;


procedure TAbstractCommunications.UpdateStreaming(aTag : TAbstractTag);
var DS : THTTPDataStream;
    I : Integer;
    Tmp : string;
begin
  //OPCSubscriptionCountTag.AsInteger := ACNCOPCServer.SubscriptionCount;
  //OPCConnectionCountTag.AsInteger := ACNCOPCServer.ClientCount;

  StringArrayUpdateCountTag.AsInteger := SysObj.StringArraySet.TotalEvents;

  for I := 0 to StreamingList.Count - 1 do begin
    DS := THTTPDataStream(StreamingList[I]);
    if not DS.Dead and (DS.LastTick + DS.DT < CNC32.PlcTick.I64) then begin
       try
         tmp := TAbstractTag(DS.Data).AsString;
         if (Tmp <> DS.LastData) { or (DS.LastTick + (DS.DT * 30) < CNC32.PlcTick.I64) } then begin

           if DS.Callback.DataStream(DS.Stream, '"io.' + DS.Name + '"  "' + Tmp + '"') then
             DS.LastTick := CNC32.PlcTick.I64
           else
             DS.Dead := True;
           DS.LastData := Tmp;
         end;
       except
         DS.Dead := True;
       end;
    end;
  end;
end;


procedure TAbstractCommunications.AddToIOSweep(Method : TCallback);
var Notify : PCallbackRecord;
begin
  New(Notify);
  Notify^.Callback := Method;
  IOSweepList.Add(Notify);
end;


function  TAbstractCommunications.FindDevice(aName : string) : TAbstractIODevice;
var I : Integer;
begin
  for I := 0 to Devices.Count - 1 do begin
    Result := TAbstractIODevice(Devices[I]);
    if CompareText(Result.Name, aName) = 0 then
      Exit;
  end;

  Result := nil;
end;

procedure TAbstractCommunications.AddDevice (Device : TAbstractIODevice);
begin
  Device.SubSystem.AddDevice(Device);
  Devices.Add(Device);
end;

function TAbstractCommunications.GetIOSystemAccessor(AName: string): IIOSystemAccessor;
var I : Integer;
    IOAccC: TAbstractIOAccessorClass;
begin
  if CNC32.FakeIOSystem then
    AName:= 'VirtualIO';

  for I := 0 to SysObj.PluginList.Count - 1 do begin
    if Assigned(SysObj.PluginList.Objects[I]) then
      Result:= TNamedPlugin(SysObj.PluginList.Objects[I]).GetIOSystemAccessor(AName);
      if Result <> nil then
        Exit;
  end;

  IOAccC:= FindIOAccessorClass(AName);
  if IOAccC <> nil then
    Result:= IOAccC.create
  else
    Result:= nil;
end;


function  TAbstractCommunications.FindSubSystem(aName : string) : TGenericIOSubSystem;
var I : Integer;
begin
  for I := 0 to SubSystems.Count - 1 do begin
    Result := TGenericIOSubSystem(SubSystems[I]);
    if CompareText(Result.Name, aName) = 0 then
      Exit;
  end;

  Result := nil;
end;


function TAbstractCommunications.GetAccessUserName : string;
begin
  if Assigned(AccessUserNameTag) then
    Result := AccessUserNameTag.AsString
  else
    Result := '';
end;

procedure TAbstractCommunications.UpdateData  (aTag : TAbstractTag);
begin
  if aTag = AccesssTag then begin
    FAccessLevels := TAccessLevels(Byte(aTag.AsInteger));
  end else if aTag = ModeTag then begin
    FCNCModes := TCNCModes(Byte(aTag.AsInteger));
  end else if aTag = MotionTag then begin
    if aTag.AsBoolean then include(FStatusType, cstMotion)
                      else exclude(FStatusType, cstMotion);
  end else if aTag = HomedTag then begin
    if aTag.AsBoolean then include(FStatusType, cstHomed)
                      else exclude(FStatusType, cstHomed);
  end else if aTag = CycleTag then begin
    if aTag.AsBoolean then include(FStatusType, cstCycle)
                      else exclude(FStatusType, cstcycle);
  end else if aTag = EditTag then begin
    if aTag.AsBoolean then Include(FStatusType, cstEditing)
                      else Exclude(FStatusType, cstEditing);
  end;
end;

procedure TAbstractCommunications.CNCFormChange;
var I : Integer;
    This : TCNC;
begin
  for I := 0 to SysObj.FCNCComponents.Count - 1 do begin
    This := TCNC(SysObj.FCNCComponents.Objects[I]);
    This.LoseFocus;
  end;
end;


procedure TAbstractCommunications.RestartACNC;
var
  ProcessInformation : TProcessInformation;
  StartupInfo : TStartupInfo;
  ApplicationName : string;
  CommandLine : string;
  I : Integer;
//  Active : Cardinal;
begin
  CommandLine := '';

  ApplicationName := ProfilePath + 'Restart.exe';
  for I := 0 to ParamCount - 1 do
    CommandLine := CommandLine + ' ' + ParamStr(I);

  StartUpInfo.cb := SizeOf(TStartupInfo);
  StartupInfo.lpDesktop := nil;
  StartupInfo.lpTitle := PChar(ExtractFileName(ApplicationName));
  StartUpInfo.dwFlags := 0;

  if not CreateProcess (nil , PChar(ApplicationName + ' ' + CommandLine),
                  nil, nil, False,
                  CREATE_NEW_CONSOLE,
                  nil,
                  nil,
                  StartupInfo,
                  ProcessInformation) then

    raise ECNCInstallFault.Create('Unable to initiate restart');

  Sleep(100);
  Application.Terminate;
end;

procedure TAbstractCommunications.DoCallbacks(aTag : TAbstractTag; edge : TEdgeType);
{ Runs through the callback list for the handle and executes the callback
  on the following conditions, the calling edge matches the predefined edge,
  the calling edge is edgeChange, the predefined edge is edge change.
  All callbacks from derived classes go through this procedure and so this routine
  automatically detects stack overflow. If the callback stack is forced too deep
  (MAX_NAMED_STACK_DEPTH) then a Stack to deep profesional error is generated. }

  procedure DisplayCallbackStatus(const EventName: string);
  begin
    EventLogFmt('Callback report %s: %s  Stackdepth: %d %sCallbackListCount: %d %s CallbackList: %s%s%s------',
      [EventName, CarRet, StackCount, CarRet, CurrentCallbackList.Count, CarRet, CarRet, CurrentCallbackList.Text, CarRet]);
  end;

var
  Msg: TIOMessage;
  Current : PCallbackRecord;
  I : Integer;
begin
  if aTag = CurrentCallbackReportTag then begin
    if aTag.AsBoolean then begin
      CallbackCriticalSection.Acquire;
      try
        DisplayCallbackStatus('Request');
      finally
        CallbackCriticalSection.Release;
      end;
    end;
  end;

  if StackCount > MAX_NAMED_STACK_DEPTH then begin
    if not StackFault then begin
      SysObj.FaultInterface.SetFault(Self, CoreFaults[cfSysFatal], ['TNamed.DoCallbacks : callbacks too deep'], nil);
      CallbackCriticalSection.Acquire;
      try
        DisplayCallbackStatus('Entry');
      finally
        CallbackCriticalSection.Release;
      end;
    end;
    StackFault := True;

    Exit;
  end;

  CallbackCriticalSection.Acquire;
  try
    StackCount := StackCount + 1;
    CurrentCallbackList.Add(aTag.Name);
  finally
    CallbackCriticalSection.Release;
  end;

  try
      if aTag.IsVCL then begin
        for I := 0 to aTag.Count - 1 do begin
          try
            if I > aTag.Count then
              Exit;
            Current := aTag[I];
            if (current.edge = edgeChange) or
               (edge = current.edge) or
               (edge = edgeChange) then begin

              if Assigned(Current.Callback) then
                if not DispatchCallback(aTag, Current.Callback) then
                  Exit;
            end;
          except
            on E : Exception do
              //SysObj.FaultInterface.SetFault(Self, CoreFaults[cfSysWarning],
              //[Format('++ Caught potential system failure, TAbstractCommunications.DoCallbacks: Name [%s] index [%d] original error [%s]', [ aTag.Name, I, E.Message])], nil);
              EventLogFmt('++ Caught potential system failure, TAbstractCommunications.ThreadDispatch: Name [%s] index [%d] original error [%s]', [ aTag.Name, I, E.Message]);
          end;

        end;
      end else begin
        Msg.edge:= edge;
        Msg.Tag := aTag;
        with Msg do
          PostMessage(Self.Handle, CNCM_IO_EVENT, wParam, lParam)
      end;
  finally
    CallbackCriticalSection.Acquire;
    try
      StackCount := StackCount - 1;
      I:= CurrentCallbackList.IndexOf(ATag.Name);
      if(I <> -1) then
        CurrentCallBackList.Delete( I )
      else
        EventLogFmt('DoCallbacks: Index of [%s] not found in CurrentCallbackList', [ATag.Name]);

    finally
      CallbackCriticalSection.Release;
    end;
  end;

  if StackFault then begin
    DisplayCallbackStatus('Exit');
    stackFault := False;
  end;
end;

resourcestring
  MemoryWriteFail = '[%s] Unhandled Exception [%s] raised [%s]';

procedure TAbstractCommunications.ThreadDispatch(var Message : TIOMessage);
var
  Current : PCallbackRecord;
  aTag : TAbstractTag;
  Edge : TEdgeType;
  I : Integer;
begin
  aTag := Message.Tag;
  Edge := Message.Edge;

  I := 0;
  while I < aTag.Count do begin
    // Interesting piece of logic, a client may remove a callback
    Current := aTag.Callback[I];
    if (current.edge = edgeChange) or
       (edge = current.edge) or
       (edge = edgeChange) then begin

      try
        if Assigned(Current.Callback) then begin
          if not DispatchCallback(aTag, Current.Callback) then
            Exit;
        end;
      except
        on E : Exception do
          //SysObj.FaultInterface.SetFault(Self, CoreFaults[cfSysWarning],
          //[Format('++ Caught potential system failure, TAbstractCommunications.ThreadDispatch: Name [%s] index [%d] original error [%s]', [ aTag.Name, I, E.Message])], nil);
          EventLogFmt('++ Caught potential system failure, TAbstractCommunications.ThreadDispatch: Name [%s] index [%d] original error [%s]', [ aTag.Name, I, E.Message]);
      end;
    end;
    Inc(I);
  end;
end;

procedure TAbstractCommunications.Write(aTag : TAbstractTag; Edge : TEdgeType);
var Buffer : array [0..255] of Char;
begin
  if Frozen then
    Exit;
  try
    if aTag.LoggingPermissive and LogNamedLayer then
      EventLogFmt('WT %s', [aTag.Name]);

    DoCallbacks(aTag, Edge);
  except
    on E : Exception do begin
//      Frozen := True;
      ExceptionErrorMessage(ExceptObject, ExceptAddr, Buffer, 255);
      raise ECNCInstallFault.CreateFmt(MemoryWriteFail, [Name, aTag.Name, Buffer]);
    end;
  end;
end;


{ Creates the StringList FaultList for use by the derived class }
constructor TAbstractFaultInterface.Create(Aowner : TComponent);
begin
  inherited Create(Aowner);
  FFaultList := TStringList.create;
  FfaultLevel := faultMessageOnly;
  ResetRWCompleteList := TList.Create;
  ResetCompleteList := TList.Create;
  FaultLock := TCriticalSection.Create;
end;

destructor TAbstractFaultInterface.Destroy;
begin
  if Assigned(FaultLock) then
    FaultLock.Free;
  inherited;
end;

procedure TAbstractFaultInterface.EnterFaultLock;
begin
  FaultLock.Enter;
end;

procedure TAbstractFaultInterface.ExitFaultLock;
begin
  FaultLock.Leave;
end;

function TAbstractFaultInterface.GetFault(Index : Integer; var Text : string; var Level : TFaultLevel): PFaultRegistration;
var Fault : TFaultRecord;
begin
  EnterFaultLock;
  try
    if(Index < FFaultList.Count) then begin
      Fault := TFaultRecord(FFaultList.Objects[Index]);
      Text := Fault.Text;
      Level := Fault.Level;
      Result:= Fault.Reg;
    end else begin
      Text := '';
      Level := FaultNone;
      Result:= nil;
    end;
  finally
    ExitFaultLock;
  end;
end;

function TAbstractFaultInterface.FaultCount : Integer;
begin
  Result := FFaultList.Count;
end;

procedure TAbstractFaultInterface.AddResetComplete(Comp: TCNC);
begin
  ResetCompleteList.Add(Comp);
end;

procedure TAbstractFaultInterface.AddResetRWComplete(Comp : TCNC);
begin
  ResetRWCompleteList.Add(Comp);
end;

{ Publishes faultLevel and faultChange and registers MessageAck event for use by
  the decendant class }
procedure TAbstractFaultInterface.InstallComponent(Params : TParamStrings);
begin
  inherited InstallComponent(Params);
  FFaultLevel := FaultNone;

  FaultLevelTag  := TagPublish(nameIOFaultLevel, TIntegerTag);
  FaultChangeTag := TagPublish(nameIOFaultChange,TIntegerTag);
  ResetTag       := TagPublish(nameReset, TIntegerTag);
  ResetRWTag     := TagPublish(nameResetRW, TIntegerTag);
  IOFaultTag     := TagPublish(nameIOFault, TStringTag);
end;

procedure TabstractFaultInterface.IOResetComplete(aTag : TAbstractTag);
begin
end;

{ Ensures the current fault level is written to both NamedLayers }
procedure TAbstractFaultInterface.Installed;
begin
  inherited installed;
//  TagEvent(nameAccessLevel, EdgeChange, TIntegerTag, UserAccessChange);

  IOResetCompleteTag := TagEvent(nameResetComplete, EdgeRising, TIntegerTag, IOResetComplete);
{  if IOResetComplete_IH = nil then
    raise ECNCINstallFault.CreateFmt(IOTagRequiredText, [Name, nameResetComplete]); }
  FaultLevelTag.AsInteger := Byte(FFaultLevel);
  ReqResetTag    := TagEvent  (nameReqReset,     edgeRising, TMethodTag, Reset);
  ReqResetRWTag  := TagEvent  (nameReqResetRW,   edgeRising, TMethodTag, ResetRW);
  TagEvent (nameReqFaultAck, EdgeRising, TMethodTag, FaultReset);
end;

{ This is the only visible method within this class and must be overridden by
  the decendant class for application specific useage }
function TabstractFaultInterface.SetFault( aOwner : TCNC;
                                           const F : TFaultRegistration;
                                           const Params : array of const;
                                           Reset : TFaultResetMethod) : FHandle;
begin
  Result := 0;
end;

{ The calling function has determined the fault handle is no longer active }
procedure TAbstractFaultInterface.FaultResetID(FiD : FHandle);
begin
end;

function TAbstractFaultInterface.GetFaultLevel : TFaultLevel;
begin
  Result := TFaultLevel(FaultLevelTag.AsInteger);
end;

procedure TAbstractFaultInterface.DoResetRewind;
begin
  ResetRW(ReqResetRWTag);
end;

{ This method is setup by this class to be called on a messageAck event, this
  is purely to allow the derived class to override it.  }
procedure TabstractFaultInterface.FaultReset(aTag : TAbstractTag);
begin
end;

{ TAbstractNC publishes and registers a lot of named space refer to the class
  definition above for its footprint }
procedure TAbstractNC.InstallComponent(Params : TParamStrings);
var  TmpAxis : TAxisPositionTag;
     DP : TNCPublished;
     I : Integer;
begin
  inherited InstallComponent(Params);

//  FLines := TStringList.Create;

  for DP := Low(TNCPublished) to High(TNCPublished) do
    with NCPublishedF[DP] do
      TagPb[DP] := TagPublish(Format(Tag, [1]), Size);

  FAxisPosition := TList.Create;

  TagPb[ncpAxisName].AsString := FAxisNames;
  TagPb[ncpPartProgram].AsString := '';
  TagPb[ncpMDIBuffer].AsString := '';

  for I := 0 to AxisCount - 1 do begin
    TmpAxis := TAxisPositionTag(TagPublish(Format(AxisPositionFormat, [AxisName[I]]), TAxisPositionTag));
    FAxisPosition.Add(TmpAxis);
  end;
end;

procedure TAbstractNC.Installed;
var de : TNCEvent;
begin
  inherited Installed;
  for de := Low(TNCEvent) to High(TNCEvent) do
    with NCEventF[de] do
      TagEv[de] := TagEvent(Format(Tag, [1]), Edge, Size, DualEventCallback);

  TagEvent(nameNewActiveFile, edgeChange, TMethodTag, SetPartProgram);
  TagEvent(nameIOFaultLevel, edgeChange, TMethodTag, NewFaultLevel);
end;

procedure TAbstractNC.AcquireCycleStartLock;
begin
  Inc(FCycleStartLock);
end;

procedure TAbstractNC.ReleaseCycleStartLock;
begin
  Dec(FCycleStartLock);
end;

procedure TAbstractNC.AcquireModeChangeLock;
begin
  Inc(FModeChangeLock);
end;

procedure TAbstractNC.ReleaseModeChangeLock;
begin
  Dec(FModeChangeLock);
end;

function TAbstractNC.GetLines : TStrings;
begin
  Result := FLines;
end;

function TAbstractNC.GetProgramLine : Integer;
begin
  Result := TagPb[ncpProgramLine].AsInteger;
end;

function TAbstractNC.GetSubCount : Integer;
begin
  Result := 0;
end;

function TAbstractNC.GetActiveSubRoutine : Integer;
begin
  Result := 0;
end;

function TAbstractNC.GetSubRoutine(Index : Integer) : WideString;
begin
  Result := '';
end;

function TAbstractNC.GetSubRoutineLines(Index : Integer) : TStringList;
begin
  Result := nil;
end;

function TAbstractNC.GetJogMode : TJogModes;
begin
  Result := TJogModes( Byte(TagPb[ncpJogMode].AsInteger) );
end;

function TAbstractNC.GetSingleBlock : Boolean;
begin
  Result := TagPb[ncpSingleBlock].AsBoolean;
end;

function TAbstractNC.GetCNCMode : TCNCModes;
begin
  Result := TCNCModes(Byte(TagPb[ncpActiveMode].AsInteger));
end;

function TAbstractNC.GetFeedOVR : Integer;
begin
  Result := TagPb[ncpFeedOVR].AsInteger;
end;

function TAbstractNC.GetAxisInMotion : Integer;
begin
  Result := TagPb[ncpAxisInMotion].AsInteger;
end;

function TAbstractNC.GetDryRun : Boolean;
begin
  Result := TagPb[ncpDryRun].AsBoolean;
end;

function TAbstractNC.GetOptionalStop : Boolean;
begin
  Result := TagPb[ncpOptionalStop].AsBoolean;
end;

function TAbstractNC.GetBlockDelete : Boolean;
begin
  Result := TagPb[ncpBlockDelete].AsBoolean;
end;

function TAbstractNC.GetActiveAxis : Integer;
begin
  Result := TagPb[ncpActiveAxis].AsInteger;
end;

function TAbstractNC.OrdinalNCMode : TCNCMode;
begin
  for Result := Low(TCNCMode) to High(TCNCMode) do begin
    if Result in CNCMode then
      Exit;
  end;
  Result := CNCModeManual;
end;

function TAbstractNC.IsPluginGCode(Major, Minor: Integer): Boolean;
var I: Integer;
    Strings: TStringList;
    Majors: string;
begin
  Majors:= IntToStr(Major);
  Strings:= TStringList.Create;
  try
    for I := 0 to SysObj.PluginList.Count - 1 do begin
      TNamedPlugin(SysObj.PluginList.Objects[I]).ExtGCodeList(Strings);
    end;
    for I:= 0 to Strings.Count - 1 do begin
      if Majors = Strings[I] then begin
        Result:= True;
        Exit;
      end;
    end;
  finally
    Strings.Free;
  end;
  Result:= False;
end;

function TAbstractNC.GetPluginGCode(Major, Minor: Integer): INCExtGCode;
var I: Integer;
    Majors: string;
begin
  Majors:= IntToStr(Major);
  // Try each dll in turn for candidate
  for I := 0 to SysObj.PluginList.Count - 1 do begin
    Result:= TNamedPlugin(SysObj.PluginList.Objects[I]).GetExtGCodeLib(Majors);
    if Assigned(Result) then
      Exit;
  end;
  raise Exception.Create('Plugin GCode not found');
end;

function TAbstractNC.GetFeedHold : Boolean;
begin
  Result := TagPb[ncpFeedHold].AsBoolean;
end;

function TAbstractNC.GetAxisPosition(Index : Integer) : TAxisPosition;
begin
  Result := TAxisPositionTag(FAxisPosition[Index]).AsAxisPosition;
end;

function TAbstractNC.GetAxisCount : Integer;
begin
  Result := FAxisCount;
end;

function TAbstractNC.GetAxisName    (Index : integer) : TAxisName;
begin
  Result := TagPb[ncpAxisName].AsString[Index + 1];
end;

function TAbstractNC.GetAxisType    (Index : integer) : TAxisType;
begin
  Result := atLinear;
end;

procedure TAbstractNC.NewFaultLevel(aTag : TAbstractTag);
begin
end;

procedure TAbstractNC.DualEventCallback(aTag : TAbstractTag);
var de : TNCEvent;
begin
  for de := Low(TNCEvent) to High(TNCEvent) do
    if aTag = TagEv[de] then begin
      ClientDualEvent(de, aTag);
      Exit;
    end;
end;

function TAbstractNC.GetInCycle : Boolean;
begin
  Result := TagPb[ncpInCycle].AsBoolean;
end;

function TAbstractNC.GetHomed : Boolean;
begin
  Result := TagPB[ncpHomed].AsBoolean;
end;

function TAbstractNC.GetInMotion : Boolean;
begin
  Result := TagPB[ncpInMotion].AsBoolean;
end;

function TAbstractNC.GetInchModeDefault : Boolean; stdcall;
begin
  Result := FIXUP_InchModeDefault;
end;

constructor TAbstractDisplay.Create(AOwner : TComponent);
begin
  inherited Create(AOwner);
  KeySetHandle := 0;
  TabStop := False;
  TitlePanel:= TPanel.Create(Self);
end;

{ Destroys the TCNC class then inherits the TCustomPanel Destroy }
destructor TAbstractDisplay.Destroy;
begin
  inherited Destroy;
end;

{ This calls the open method display of the installed display manager and
  informs the Menu manager if a key set is required }
procedure TAbstractDisplay.Open;
begin
  SysObj.Layout.Open(Self);
  SysObj.FaultInterface.ResetInterlock;
  if KeySetHandle > 0 then
    SysObj.Softkey.KeyChange(KeySetHandle);

  if (Primary <> nil) and
     (SysObj.VisualParent.Visible) and
      Primary.Visible then begin
    if Primary.Enabled then Primary.SetFocus;
  end;
end;

{ This call the close method of the installed display manager }
procedure TAbstractDisplay.Close;
begin
  if KeySetHandle > 0 then
    SysObj.Softkey.KeyClose(KeySetHandle);
  SysObj.Layout.Close(Self);
end;

{ Override this method to capture the event of this window closing this
  window can veto the close by return false. }
procedure TAbstractDisplay.Closing;
begin
end;

procedure TAbstractDisplay.SetTitle(const ATitle: string);
begin
  TitlePanel.Caption:= ATitle;
end;

procedure TAbstractDisplay.ShowTitlePanel(AHeight: Integer);
begin
  TitlePanel.Width:= Width;
  TitlePanel.Height:= AHeight;
  TitlePanel.Left:= Left;
  TitlePanel.Top:= Top - AHeight;
  TitlePanel.Visible:= True;
  TitlePanel.BringToFront;
end;

procedure TAbstractDisplay.HideTitlePanel;
begin
  TitlePanel.Visible:= False;
end;


procedure TAbstractDisplay.InstallComponent(Params : TParamStrings);
var AccessLevels : TAccessLevels;
    I: Integer;
begin
  inherited installComponent(Params);
  AccessLevels := [AccessLevelProgrammer..AccessLevelOEM1];
  caption := '';
  SysObj.Layout.add(Self);
  color := Params.ReadColour('Colour', clbtnFace);
  BevelOuter := bvLowered;
  BevelWidth := 3;
  FActiveDisplay := Params.ReadString('ActiveDisplay', '');
  I:= Params.ReadInteger('EditLevel', Byte(AccessLevels));
  EditLevel := TAccessLevels(Byte(I));
  FTitle:= Params.ReadString('Title', Name);
  FUseTitle:= Params.ReadBool('UseTitle', False);
  TitlePanel.Parent:= SysObj.VisualParent;
  TitlePanel.Caption:= Title;
  TitlePanel.BevelOuter:= bvLowered;
  TitlePanel.BevelWidth := 2;
  TitlePanel.Color:= $F0E0E8;
  TitlePanel.Visible:= False;
end;

procedure TAbstractDisplay.Installed;
begin
  inherited installed;
  TagEvent(nameAccessLevels, edgeChange, TIntegerTag, userAccessChange);
  TagEvent(NameNormalDisplay, edgeChange, TMethodTag, NormalDisplay);
end;

procedure TAbstractDisplay.IShutdown;
begin
  inherited IShutdown;
end;


{ This has been created purely to reduce the ammount of code required.  Its function
  is identical to <a href=#open>open</a> but the parameters are TCallback thus a
  single line in the installed method can tie a softkey or other event
  directly to the open task. If you override the activate procedure there is no
  need to call the inherited activate just call open}
procedure TAbstractDisplay.Activate(aTag : TAbstractTag);
begin
  if not ATag.AsBoolean then
    Open;
end;

{ This loads a standard keyboard control set from this components section of the CNC.ini
  file.  The returned data is suitable for the installKeySet routine (only) }
{function TAbstractDisplay.loadKeys(Params : TParamStrings): TKeyArray;
begin
  result := Params.LoadKeys;
end; }

{ The data returned from loadKeys can be used install a set of softkeys for use
  by the Menu system }
procedure TAbstractDisplay.InstallKeySet(keys : TKeyArray);
var
  i      : TFunctionKey;
begin
  for i := Low(TFunctionKey) to High(TFunctionKey) do begin
    if keys[i].name <> '' then begin
      KeySetHandle := SysObj.Softkey.RegisterKeySet(keys);
      exit;
    end;
  end;
end;

{ Traverses the component list and enables / disables TCustomEdit components }
procedure TAbstractDisplay.UserAccessChange(aTag : TAbstractTag);
var mode : boolean;
    i       : integer;
begin
  Mode := ((TAccessLevels(Byte(aTag.AsInteger))) * EditLevel) <> [];
  if ManageEdit then
    Exit;
  for i := 0 to componentCount -1 do begin
    if components[i] is TControl then
      TControl(components[i]).enabled := mode;
  end;
end;

procedure TAbstractDisplay.NormalDisplay(aTag : TAbstractTag);
begin
end;

function TAbstractDisplay.HTTPGeneratePage(HFile : THANDLE; var CGIPath : WideString; CGIRequest : ICGIStringList) : Boolean;
//var Tmp : string;
begin
  Result := inherited HTTPGeneratePage(HFile, CGIPath, CGIRequest);
{  if not Result then begin
    Tmp := CGIPath;
    Tmp := ReadDelimited(Tmp, '/');
    if Tmp = '' then begin
      WriteStringToHandle(hFile, Format('<a href="%s/screenshot.jpg">Screen Shot</a><br>' + CarRet, [Name]));
    end;
  end; }
end;

constructor TAbstractLayoutManager.Create(aOwner : TComponent);
begin
  inherited Create(aOwner);
  DisplayList := TList.Create;
end;

{ This loads the interval value for display update and Memory.publish(s) the name
  'DisplaySweep' to allow all components to update at a common frequency
}
procedure TAbstractLayoutManager.InstallComponent(Params : TParamStrings);
begin
  inherited InstallComponent(Params);
  DisplayTimer := TTimer.Create(Self);
  DisplayTimer.interval := Params.ReadInteger('interval', 100);
  DisplayTimer.enabled  := False;
  DisplayTimer.onTimer  := UpdateDisplay;

  FlashTimer := TTimer.create(Self);
  FlashTimer.interval := 700;
  FlashTimer.enabled  := False;
  FlashTimer.OnTimer  := UpdateFlash;

  YearTag := TagPublish('CronYear', TIntegerTag);
  MonthTag := TagPublish('CronMonth', TIntegerTag);
  DayTag := TagPublish('CronDay', TIntegerTag);
  WDayTag := TagPublish('CronDayOfWeek', TIntegerTag);
  HourTag := TagPublish('CronHour', TIntegerTag);
  MinuteTag := TagPublish('CronMinute', TIntegerTag);
  SecondTag := TagPublish('CronSecond', TIntegerTag);
  TimeTag := TagPublish(NameTime, TStringTag);
  DateTag := TagPublish(NameDate, TStringTag);
  NowTag := TagPublish(NameNow, TStringTag);

  DisplaySweepTag := TagPublish(NameDisplaySweep, TIntegerTag);
  DisplaySweepTag.LoggingPermissive := False;

  FlashTag := TagPublish(nameFlashTimer, TIntegerTag);
  //NormalDisplayTag := TagPublish(NameNormalDisplay, TIntegerTag);
end;

procedure TAbstractLayoutManager.Installed;
begin
  inherited Installed;
  DisplayTimer.enabled := True;
  FlashTimer.enabled   := True;
  TagEvent(nameNextWindow, edgeRising, TMethodTag, NextWindow);
  TagEvent(NameNormalDisplay, edgeChange, TIntegerTag, NormalDisplay);
end;

procedure TAbstractLayoutManager.NormalDisplay(ATag: TAbstractTag);
begin
  if not ATag.AsBoolean then
    Normal;
end;

{ Disables the displaySweep to prevent display update }
procedure TAbstractLayoutManager.IShutdown;
begin
  inherited IShutdown;
  DisplayTimer.Enabled := False;
end;

procedure TAbstractLayoutManager.NextWindow(aTag : TAbstractTag);
var i : integer;
begin
  for i := Screen.FormCount -1 downto 0 do
    if Screen.Forms[i].Visible then begin
      Screen.Forms[i].SetFocus;
      exit;
    end;
end;

procedure TAbstractLayoutManager.UpdateDisplay(Sender : TObject);
var ST : TSystemTime;
begin

  if (DisplaySweepTag.AsInteger mod 4) = 0 then begin
    if TimeTag.AsString <> TimeToStr(Now) then
      TimeTag.AsString := TimeToStr(Now);
    if DateTag.AsString <> DateToStr(Now) then
      DateTag.AsString := DateToStr(Now);
    NowTag.AsString := Format('%.4f', [Now]);

    GetLocalTime(ST);
    if (YearTag.AsInteger <> ST.wYear) then
      YearTag.AsInteger := ST.wYear;
    if (MonthTag.AsInteger <> ST.wMonth) then
      MonthTag.AsInteger := ST.wMonth;
    if (DayTag.AsInteger <> ST.wDay) then
      DayTag.AsInteger := ST.wDay;
    if (WDayTag.AsInteger <> ST.wDayOfWeek) then
      WDayTag.AsInteger := ST.wDayOfWeek;
    if (HourTag.AsInteger <> ST.wHour) then
      HourTag.AsInteger := ST.wHour;
    if (MinuteTag.AsInteger <> ST.wMinute) then
      MinuteTag.AsInteger := ST.wMinute;
    if (SecondTag.AsInteger <> ST.wSecond) then
      SecondTag.AsInteger := ST.wSecond;
  end;
  try
    DisplaySweepTag.AsInteger := DisplaySweepTag.AsInteger + 1;
  except
    on E : Exception do
      SysObj.FaultInterface.SetFault(Self, CoreFaults[cfSysWarning], [E.Message], nil);
  end;
end;

{ Maintains the display flash update : generated by flashTimer }
procedure TAbstractLayoutManager.updateFlash(Sender : TObject);
begin
  if not SysObj.Comms.Frozen then begin
    FlashValue := not FlashValue;
    FlashTag.AsBoolean := FlashValue;
  end;
end;

procedure TAbstractLayoutManager.Add(Display : TAbstractDisplay);
begin
  DisplayList.Add(Display);
end;

function TAbstractLayoutManager.GetDisplay(Index : Integer) : TAbstractDisplay;
begin
  Result := TAbstractDisplay(DisplayList[Index]);
end;

function TAbstractLayoutManager.GetDisplayCount : Integer;
begin
  Result := DisplayList.Count;
end;

procedure TAbstractLayoutManager.Normal;
begin
  SysObj.FaultInterface.ResetInterlock;
  //NormalDisplayTag.AsBoolean := True;
end;

constructor TAbstractSoftkey.create(Aowner : TComponent);
begin
  inherited create(Aowner);
  keySets := nil;
  handleCount := 0;
end;

procedure TAbstractSoftkey.InstallComponent(Params : TParamStrings);
begin
  inherited;
end;

procedure TAbstractSoftkey.WndKey (var message : TMessage);
begin
  if message.lparam = Ord(edgeRising) then
    keyEvent(message.wparam, edgeRising)
  else
    keyEvent(message.wparam, edgeFalling);
end;

{ An internal function, used for the management of key sets }
function TAbstractSoftkey.Add(keys : TkeyArray) : word;
var
  KeyPtr : PTKeyLinkedList;
  i      : TFunctionKey;
  Count : Integer;
begin
  new(KeyPtr);
  KeyPtr.keys   := keys;
  Count := 0;
  for i := Low(TFunctionKey) to High(TFunctionKey) do begin
    with KeyPtr.Keys[i] do begin
      if func <> '' then begin
        Inc(Count);
        Key := TMethodTag(SysObj.Comms.TagPublish(func, TMethodTag));
      end;
    end;
  end;
  if Count > 0 then begin
    handleCount   := handleCount + 1;
    KeyPtr.handle := handleCount;
    Result        := handleCount;
    KeyPtr.next   := keySets;
    KeySets       := keyptr;
  end else begin
    Dispose(KeyPtr);
    Result := 0;
  end;
end;

{ An internal function, used for the management of keys sets }
function  TAbstractSoftkey.Find(handle : word; var keys : TKeyArray) : Boolean;
var
  current : PTKeyLinkedList;
begin
  Result := True;
  current := keySets;
  while current <> nil do begin
    if current^.handle = handle then begin
      Keys := current^.keys;
      exit;
    end;
    current := current^.next;
  end;
  Result := False;
end;

{ Called by TabstractDisplay when installing a keyset. This function must not be
  used by any derived class !!!. }
function  TAbstractSoftkey.RegisterKeySet(newKeys : TKeyArray) : word;
begin
  Result := Add(newKeys);
end;

procedure TAbstractIOThread.Execute;
var Lock : TCriticalSection;
begin
//  priority := tpLowest;
  priority := tpTimeCritical;
  Lock := TCriticalSection.Create;

  SysObj.Comms.UpdateTick;
  LastSweep := CNC32.PlcTick.I64;
  try
    while True do begin
      if Assigned(OnSweep) then begin
        Lock.Enter;
        try
          OnSweep;
        finally
          Lock.Leave;
        end;
      end;
      if Terminated then
        Exit;
      Sleep(SleepTime);
    end;
  finally
    Lock.Free;
  end;
end;

constructor TAbstractIOThread.Create;
begin
  // True is created suspended
  inherited Create(True);
  SleepTime := 20;
//  last := 2000000;
end;


{&&& warning - ClassName must be unique. Compiler will not
enforce this as the classes may be in different units}
procedure RegisterCNCClass(ClassType: TAbstractCNCClass; AName: string);
begin
  if InstallableList = nil then
    InstallableList:= TStringList.Create;

  if AName = '' then
    AName:= ClassType.ClassName;

  if InstallableList.IndexOf(AName) <> -1 then
    raise ECNCInstallFault.CreateFmt(ClassNameNotUnique, [ClassType.ClassName]);

  InstallableList.AddObject(AName, Pointer(ClassType))
end;

procedure RegisterTagClass(ClassType : TTagClass);
begin
  if RegTagList = nil then RegTagList:= TStringList.Create;

  if RegTagList.IndexOf(ClassType.ClassName) <> -1 then
    raise ECNCInstallFault.CreateFmt(TagClassNameNotUnique, [ClassType.ClassName]);

  RegTagList.AddObject(ClassType.ClassName, Pointer(ClassType))
end;

procedure RegisterTagClass(ClassType : TTagClass; const AName: string);
begin
  if RegTagList = nil then RegTagList:= TStringList.Create;

  if RegTagList.IndexOf(ClassType.ClassName) <> -1 then
    raise ECNCInstallFault.CreateFmt(TagClassNameNotUnique, [ClassType.ClassName]);

  RegTagList.AddObject(AName, Pointer(ClassType))
end;

procedure RegisterIODeviceClass(ClassType : TAbstractIODeviceClass);
begin
  if RegDeviceList = nil then RegDeviceList:= TStringList.Create;

  if RegDeviceList.IndexOf(ClassType.ClassName) <> -1 then
    raise ECNCInstallFault.CreateFmt(TagClassNameNotUnique, [ClassType.ClassName]);

  RegDeviceList.AddObject(ClassType.ClassName, Pointer(ClassType))
end;

procedure RegisterIOAccessorClass(ClassType: TAbstractIOAccessorClass; AName: string='');
begin
  if RegIOAccessorList = nil then RegIOAccessorList:= TStringList.Create;

  if AName = '' then
    RegIOAccessorList.AddObject(ClassType.ClassName, Pointer(ClassType))
  else
    RegIOAccessorList.AddObject(AName, Pointer(ClassType));
end;

function FindCNCClass( const Name, TypeName: String): TAbstractCNC;
var
  i: Integer;
begin
  if Assigned(InstallableList) then begin
    i:= InstallableList.IndexOf(TypeName);
    if i = -1 then
                Result:= nil
              else begin
                Result:= TAbstractCNCClass(InstallableList.Objects[i]).Create(SysObj.VisualParent);
                Result.Name := Name;
                Result.Parent := SysObj.VisualParent; // ???? Frig that may be removed
              end;
  end else begin
    Result:= nil
  end
end;

function FindTagClass(const aTypeName : string): TTagClass;
  var
    i: Integer;
begin
  if Assigned(RegTagList) then begin
    i:= RegTagList.IndexOf(aTypeName);
    if i = -1 then

                Result:= nil
              else begin
                Result:= TTagClass(RegTagList.Objects[i]);
              end;
  end else begin
    Result:= nil
  end
end;


function FindIODeviceClass(const aTypeName : string) : TAbstractIODeviceClass;
  var
    i: Integer;
begin
  if Assigned(RegDeviceList) then begin
    i:= RegDeviceList.IndexOf(aTypeName);
    if i = -1 then
                Result:= nil
              else begin
                Result:= TAbstractIODeviceClass(RegDeviceList.Objects[i]);
              end;
  end else begin
    Result:= nil
  end
end;

function FindIOAccessorClass(const aTypeName : string) : TAbstractIOAccessorClass;
var I: Integer;
begin
  if Assigned(RegIOAccessorList) then begin
    I:=  RegIOAccessorList.IndexOf(aTypeName);
    if I <> -1 then begin
      Result:= TAbstractIOAccessorClass(RegIOAccessorList.Objects[I]);
      Exit;
    end;
  end;
  Result:= nil;
end;

procedure TSectionEditor.InstallComponent(Params : TParamStrings);
var Tmp : string;
begin
  inherited InstallComponent(Params);
  Tmp := Params.ReadString('SectionTitle', '');

  if Tmp = '' then
    Tmp := Self.SectionName;
  SysObj.FileManager.AttachEditor(Self, Tmp);
end;

procedure TAbstractFileManager.InstallComponent(Params : TParamStrings);
begin
  inherited InstallComponent(Params);

  FileChangedTag := TagPublish(NameNewActiveFile, TStringTag);

  if SysObj.ClientSystem then
    FileForceTag := TagPublish(NameForceActiveFile, TStringTag)
  else
    FileForceTag := TagPublish('Not' + NameForceActiveFile, TStringTag);

  RemoteFileTag := TagPublish(NameRemoteFileLoad, TStringTag);
  RemoteFileTag.NetWorkAlias:= NameOPPFileName;
  FFilemask:= Params.ReadString(FilemaskKey, DefaultFilemask);
  OPPFilePathTag:= TagPublish(NameOPPFilePath, TSTringTag);
  FFilePath := LocalPath + '\programs';
  CreateDirectory(PChar(FFilePath), nil);
//  FFilePath:= Params.ReadString(FilepathKey, DefaultFilepath);
//  FFilePath := SysObj.ClientPath + FFilePath;
end;

procedure TAbstractFileManager.Installed;
begin
  inherited Installed;
  TagEvent(NameProgramSelect, edgeFalling, TMethodTag, DoLoadActive);
  if SysObj.ClientSystem then
    TagEvent(NameRemoteFileLoad, edgeChange, TStringTag, DoFollowLoadActive)
  else
    TagEvent(NameForceActiveFile, edgeChange, TStringTag, DoFollowLoadActive);
end;

function TAbstractFileManager.GetEditMode: Boolean;
begin
  Result:= SysObj.Comms.CNCModes = [CNCModeEdit];
end;

constructor TGenericTag.Create(aOwner : TTagOwner; aName : string);
begin
  inherited Create(aOwner, aName);
  if FindTagClass(Self.ClassName) = nil then
    raise ECNCInstallFault.Create(Self.ClassName + ' Not Registered');

  IsWritable := True;
end;

class function TGenericTag.Duplicate(aTag : TAbstractTag) : TAbstractTag;
begin
  if aTag is TIntegerTag then
    Result := TOrdinalTag.Create(aTag.Owner, aTag.Name)
  else if aTag is TDoubleTag then
    Result := TFloatTag.Create(aTag.Owner, aTag.Name)
  else if aTag is TStringTag then
    Result := TAlphaTag.Create(aTag.Owner, aTag.Name)
  else if aTag is TArrayAccessTag then
    Result := TArrayAccessTag(aTag).DataSource.ItemClass.Create(aTag.Owner, aTag.Name)
  else
    Result := TAlphaTag.Create(aTag.Owner, aTag.Name);

  Result.NamedType := aTag.NamedType;
  Result.Assign(aTag);
end;

function TGenericTag.GetDataPtr : Pointer;
begin
  Result := nil;
end;

function TGenericTag.GetDataSize : Integer;
begin
  Result := 0;
end;

function TGenericTag.GetIOSize : Integer;
begin
  Result := varByRef;
end;

function TGenericTag.GetAsInteger : Integer;
begin
  case Size of
    varBoolean,
    varByte,
    varSmallInt,
    varInteger : Result := AsInteger;
    varDouble : Result := Round(AsDouble);
    varString : try
                  Result := StrToInt(AsString);
                except
                  Result := 0;
                end;
  else
    Result := 0;
  end;
end;

function TGenericTag.GetAsBoolean : Boolean;
begin
  case Size of
    varBoolean,
    varByte,
    varSmallInt,
    varInteger : Result := AsInteger <> 0;
    varDouble : Result := Round(AsDouble) <> 0;
    varString : Result := AsInteger <> 0;
  else
    Result := False;
    EventLogFmt('Unknown tag type %s', [Name]);
  end;
end;

function TGenericTag.GetAsDouble : Double;
begin
  Result := AsInteger;
end;

procedure TGenericTag.SetAsInteger(aValue : Integer);
begin
  case Size of
    varBoolean,
    varByte,
    varSmallInt,
    varInteger : AsInteger := aValue;
    varDouble : AsDouble := aValue;
    varString : AsString := IntToStr(aValue);
  else
    EventLogFmt('Unknown tag type %s', [Name]);
  end;
end;

procedure TGenericTag.SetAsBoolean(aValue : Boolean);
begin
  case Size of
    varBoolean,
    varByte,
    varSmallInt,
    varInteger : AsInteger := Ord(aValue);
    varDouble : AsDouble := Ord(aValue);
    varString : AsString := IntToStr(Ord(aValue));
  else
    EventLogFmt('Unknown tag type %s', [Name]);
  end;
end;

function TGenericTag.GetAsOleVariant: OleVariant;
begin
  case Size of
    varBoolean : Result := AsBoolean;
    varByte,
    varSmallInt,
    varInteger : Result := AsInteger;
    varDouble : Result := AsDouble;
    varString : Result := AsString;
  end;
end;

procedure TGenericTag.SetAsOleVariant(const V: OleVariant);
begin
  case VarType(V) of
    varBoolean : AsBoolean := V;
    varByte,
    varSmallInt,
    varInteger : AsInteger := V;
    varDouble : AsDouble := V;
    varOleStr,
    varString : AsString := V;
  end;
end;

procedure TGenericTag.SetAsDouble(aValue : Double);
begin
  case Size of
    varBoolean,
    varByte,
    varSmallInt,
    varInteger : AsInteger := Round(aValue);
    varDouble : AsDouble := aValue;
    varString : AsString := FloatToStr(aValue);
  else
    EventLogFmt('Unknown tag type %s', [Name]);
  end;
end;

procedure TGenericTag.SetAsString(const aValue : string);
var Fvar : Double;
    Ivar : Integer;
begin
  case Size of
    varBoolean,
    varByte,
    varSmallInt,
    varInteger : begin
      try
        if aValue = '' then
          Ivar := 0
        else
          Ivar := StrToInt(aValue);
      except
        on E : Exception do begin
          if lowercase(aValue) = 'true' then
            Ivar := 1
          else
            Ivar := 0;
        end;
      end;
      AsInteger := Round(Ivar);
    end;

    varDouble : begin
      try
        if aValue = '' then
          Fvar := 0
        else
          Fvar := StrToFloat(aValue);
      except
        on E : Exception do begin
          if lowercase(aValue) = 'true' then
            Fvar := 1
          else
            Fvar := 0;
        end;
      end;
      AsDouble := Fvar;
    end;
  else
    EventLogFmt('Unknown tag type %s', [Name]);
  end;
end;

procedure TGenericTag.Assign(aTag : TAbstractTag);
begin
// Do nothing ??
end;

const BooleanIdent : array [Boolean] of pChar =
  (  'False', 'True'  );

function TGenericTag.GetAsString : string;
begin
  case Size of
    varBoolean : Result := IntToStr(Ord(AsBoolean)); //BooleanIdent[AsBoolean];
    varByte,
    varSmallInt,
    varInteger : Result := IntToStr(AsInteger);
    varDouble : Result := Format('%.6g', [AsDouble]);
    varByRef : Result := ClassName;
  else
    Result := '';
    EventLogFmt('Unknown tag type %s', [Name]);
  end;
end;

procedure TGenericTag.Refresh;
begin
  if IsEvented then
    SysObj.Comms.Write(Self, EdgeChange);
end;

procedure TMethodTag.Refresh;
var Edge : TEdgeType;
begin
  if IsEvented then begin
    case Size of
      varBoolean : if FValue <> 0 then
                    Edge := EdgeRising
                  else
                    Edge := EdgeFalling;
    else
      Edge := EdgeChange;
    end;
    SysObj.Comms.Write(Self, Edge);
  end;
end;

constructor TIntegerTag.Create(aOwner : TTagOwner; aName : string);
begin
  inherited Create(aOwner, aName);
  IOTagSize := varInteger;
end;

constructor TMethodTag.Create(aOwner : TTagOwner; aName : string);
begin
  inherited Create(aOwner, aName);
end;

function TMethodTag.GetAsInteger : Integer;
begin
  Result := FValue;
end;

function TMethodTag.GetAsBoolean : Boolean;
begin
  Result := FValue <> 0;
end;

procedure TMethodTag.SetAsBoolean(aValue : Boolean);
begin
  SetAsInteger(Ord(aValue));
end;

procedure TMethodTag.SetAsInteger(aValue : Integer);
begin
  if NamedType >= [nltVCL] then begin
    case Size of
      varBoolean : FValue := Ord(aValue <> 0);
    else
      FValue := aValue;
    end;

    Refresh;
  end else
    if (FValue <> aValue){ and (NamedType >= [nltConnected])} then begin
      FValue := aValue;
      Refresh;
    end;
end;

procedure TMethodTag.Assign(aTag : TAbstractTag);
begin
  case Size of
    varBoolean : FValue := Ord(aTag.AsBoolean);
  else
    FValue := aTag.AsInteger;
  end;
  Refresh;
end;

function TMethodTag.GetIOSize : Integer;
begin
  Result := varBoolean;
end;

function TMethodTag.GetDataPtr : Pointer;
begin
  Result := @FValue;
end;

function TMethodTag.GetDataSize : Integer;
begin
  Result := Sizeof(FValue);
end;

function TIntegerTag.GetIOSize : Integer;
begin
  Result := Self.IOTagSize;
end;


function TIntegerTag.GetDataSize : Integer;
begin
  case Size of
    varBoolean, varByte : Result := 1;
    varSmallInt : Result := 2;
    varInteger : Result := 4;
  else
    Result := 1;
  end;
end;


procedure TIntegerTag.ReadFromDevice;
var NewValue : Integer;
begin
  NewValue := FValue;
  if not IsWritable then begin
    case Size of
      varBoolean:   NewValue := Ord((Device.InputByte[Address] and Mask) <> 0);
      varByte:  NewValue := Device.InputByte[Address];
      varSmallInt:  NewValue := Device.InputWord[Address];
      varInteger: NewValue := Device.InputDWord[Address];
    end;
    if NewValue <> FValue then
      Self.AsInteger := NewValue;
  end else if not IsOwned then begin
    case Size of
      varBoolean:   FValue := Device.OutputByte[Address] and Mask;
      varByte:  FValue := Device.OutputByte[Address];
      varSmallInt:  FValue := Device.OutputWord[Address];
      varInteger: FValue := Device.OutputDWord[Address];
    end;
  end;
end;

procedure TIntegerTag.WriteToDevice;
var Tmp : Byte;
begin
  if not IsWritable then begin
    case Size of
      varBoolean,
      varByte,
      varSmallInt,
      varInteger : Exit;
    end;
  end else begin
    case Size of
      varBoolean:   begin
                   tmp := Device.OutputByte[Address];
                   if AsBoolean then
                      Device.OutputByte[Address] := tmp or Mask
                   else
                      Device.OutputByte[Address] := tmp and not Mask

      end;
      varByte:  Device.OutputByte[Address] := AsInteger;
      varSmallInt:  Device.OutputWord[Address] := AsInteger;
      varInteger: Device.OutputDWord[Address]:= AsInteger;
    end;
  end;
end;

function  TIntegerTag.TextDefinition : string;
var MaskText : string;
begin
  case Size of
    varBoolean : begin
      case Mask of
        1   : MaskText := '0';
        2   : MaskText := '1';
        4   : MaskText := '2';
        8   : MaskText := '3';
        16  : MaskText := '4';
        32  : MaskText := '5';
        64  : MaskText := '6';
        128 : MaskText := '7';
      else
        MaskText := '0';
      end;
    end;

    varByte  : MaskText := 'b';
    varSmallInt : MaskText := 'w';
    varInteger : MaskText := 'd';
  end;
  Result := Device.Name + '.' + IntToStr(Address) + '.' + MaskText;
end;

function TIntegerTag.OK : Boolean;
begin
  if Assigned(Device) then
    Result := Device.OK
  else
    Result := True;
end;

function  TIntegerTag.AliasOf(aTag : TIntegerTag) : Boolean;
begin
  if (Address >= aTag.Address) and
     (Address <= aTag.Address + aTag.DataSize - 1) and
     (Size < aTag.Size) and
     (IsWritable = aTag.IsWritable) then begin
    Result := True;
  end else
    Result := False;
end;

function TIntegerTag.GetAddress : Integer;
begin
  Result := FAddress;
end;

procedure TIntegerTag.SetAddress(aAddress : Integer);
begin
  FAddress := aAddress;
end;

constructor TStringTag.Create(aOwner : TTagOwner; aName : string);
begin
  inherited Create(aOwner, aName);
end;

function TStringTag.GetIOSize : Integer;
begin
  Result := varString;
end;

function TStringTag.GetDataSize : Integer;
begin
  Result := Length(Value) + 1;
end;

procedure TStringTag.SetAsString(const aValue : string);
begin
  AtomLock.Enter;
  try
    Value := aValue;
  finally
    AtomLock.Release;
  end;
  Refresh;
end;

procedure TStringTag.Assign(aTag : TAbstractTag);
begin
  AtomLock.Enter;
  try
    Value := aTag.AsString;
  finally
    AtomLock.Release;
  end;
  Refresh;
end;


function TStringTag.GetAsString : string;
begin
  AtomLock.Enter;
  try
    Result := Value;
  finally
    AtomLock.Release;
  end;
end;

function TStringTag.GetAsDouble : Double;
var Code : Integer;
begin
  Val(AsString, Result, Code);
  if Code <> 0 then
    Result := 0;
end;

function TStringTag.GetAsInteger : Integer;
begin
  Result := Round(StrToFloatDef(AsString, 0));
end;

function TStringTag.GetAsBoolean : Boolean;
begin
  Result := AsInteger <> 0;
end;

procedure TStringTag.ReadFromBuf(var Buf; aLength : Integer);
begin
  Value := pChar(@Buf);
  Refresh;
end;

function TStringTag.GetDataPtr : Pointer;
begin
  Result := pChar(Value);
end;

constructor TDoubleTag.Create(aOwner : TTagOwner; aName : string);
begin
  inherited Create(aOwner, aName);
end;

function TDoubleTag.GetIOSize : Integer;
begin
  Result := varDouble;
end;

procedure TDoubleTag.SetAsDouble(aValue : Double);
begin
  Value := aValue;
  Refresh;
end;

procedure TDoubleTag.Assign(aTag : TAbstractTag);
begin
  AsDouble := aTag.AsDouble;
  //Refresh;
end;

function TDoubleTag.GetAsDouble : Double;
begin
  Result := Value;
end;

function TDoubleTag.GetAsInteger : Integer;
begin
  try
    Result := Round(AsDouble);
  except
    on E : EInvalidOp do
      Result := 0;
  end;
end;

function TDoubleTag.GetDataPtr : Pointer;
begin
  Result := @Value;
end;

function TDoubleTag.GetDataSize : Integer;
begin
  Result := Sizeof(Value);
end;

constructor TAxisPositionTag.Create(aOwner : TTagOwner; aName : string);
begin
  inherited Create(aOwner, aName);
  FOrdinateCount := 1;
  Dims[0] := Ord(High(TPositionType)) + 1;
  Dims[1] := 1;
  ItemSize := varDouble;
  FItemClass := TFloatTag;
  FData := Pointer(@FValue);
end;

procedure TAxisPositionTag.Write(const aValue : TAxisPosition);
begin
  FValue := aValue;
  Refresh;
end;

function TAxisPositionTag.AsAxisPosition : TAxisPosition;
begin
  Result := FValue;
end;

function TAxisPositionTag.GetTypeValue(Index : TPositionType) : Double;
begin
  Result := FValue[Index];
end;

procedure TAxisPositionTag.SetTypeValue(Index : TPositionType; aValue : Double);
begin
  FValue[Index] := aValue;
end;


function TAxisPositionTag.GetDataSize : Integer;
begin
  Result := Sizeof(FValue)
end;

{function TIOFaultTag.GetDataPtr : Pointer;
begin
  Result := @IOFault;
end;

function TIOFaultTag.GetDataSize : Integer;
begin
  Result := Sizeof(IOFault);
end;

constructor TIOFaultTag.Create(aOwner : TTagOwner; aName : string);
begin
  inherited Create(aOwner, aName);
end;

procedure TIOFaultTag.Write(const aValue : TIOFaultRecord);
begin
  IOFault := aValue;
  Refresh;
end;

function TIOFaultTag.AsIOFault : TIOFaultRecord;
begin
  Result := IOFault;
end; }

function TArrayTag.GetArrayBoolean(aOrd : Integer) : Boolean;
begin
  Result := Round(AsArrayDouble[aOrd]) <> 0;
end;

function TArrayTag.GetArrayInteger(aOrd : Integer) : Integer;
begin
  Result := Round(AsArrayDouble[aOrd]);
end;

function TArrayTag.GetDataPtr : Pointer;
begin
  Result := Data;
end;

resourcestring
  ArrayIndicesOutOfRange = 'Array Indices out of range';
  ArrayAssignInvalidFor = 'Array Assign is Invalid for ';
  DoesNotSupportNumberOfArgs = ' Does not support number of args';
  UnknownLang = 'Unknown';

function TArrayTag.TotalItemCount : Integer;
var I : Integer;
begin
  Result := 1;

  for I := 0 to OrdinateCount - 1 do begin
    Result := Result * Dims[I];
  end;
end;

function TArrayTag.GetArrayString(aOrd : Integer) : string;
begin
  if aOrd >= TotalItemCount then
    raise ETagError.Create(ArrayIndicesOutOfRange);

  with Data^ do begin
    case ItemSize of
//      IOSizeMethod,
      varBoolean : Result := BooleanIdent[AsArrayBoolean[aOrd]];
      varByte,
      varSmallInt,
      varInteger : Result := IntToStr(AsArrayInteger[aOrd]);
      varDouble : Result := Format('%.6g', [AsArrayDouble[aOrd]]);
      varByRef : Result := ClassName;
      varString : Result := StringArray[aOrd]^;
    else
      Result := UnknownLang;
    end;
  end;
end;

function TArrayTag.GetArrayDouble(aOrd : Integer) : Double;
var Code : Integer;
begin
  if aOrd >= TotalItemCount then
    raise ETagError.Create(ArrayIndicesOutOfRange);

  with Data^ do begin
    case ItemSize of
      varBoolean,
      varByte,
      varSmallInt,
      varInteger : Result := IntegerArray[aOrd];
      varDouble : Result := DoubleArray[aOrd];
      varString : begin
                      Val(StringArray[aOrd]^, Result, Code);
                      if Code <> 0 then
                        Result := 0;
      end;
    else
      Result := 0;
    end;
  end;
end;

procedure TArrayTag.SetArrayBoolean(aOrd : Integer; aValue : Boolean);
begin
  if aOrd >= TotalItemCount then
    raise ETagError.Create(ArrayIndicesOutOfRange);

  with Data^ do begin
    case ItemSize of
      varBoolean,
      varByte,
      varSmallInt,
      varInteger : IntegerArray[aOrd] := Ord(aValue);
      varDouble : DoubleArray[aOrd] := Ord(aValue);
      varString : StringArray[aOrd]^ := IntToStr(Ord(aValue));
    else
      raise ETagError.Create(ArrayAssignInvalidFor + Name);
    end;
  end;

  Self.Refresh;
end;

procedure TArrayTag.SetArrayInteger(aOrd : Integer; aValue : Integer);
begin
  if aOrd >= TotalItemCount then
    raise ETagError.Create(ArrayIndicesOutOfRange);

  with Data^ do begin
    case ItemSize of
      varBoolean,
      varByte,
      varSmallInt,
      varInteger : IntegerArray[aOrd] := aValue;
      varDouble : DoubleArray[aOrd] := aValue;
      varString : StringArray[aOrd]^ := IntToStr(aValue);
    else
      raise ETagError.Create(ArrayAssignInvalidFor + Name);
    end;
  end;
  Self.Refresh;
end;

procedure TArrayTag.SetArrayDouble(aOrd : Integer; aValue : Double);
begin
  if aOrd >= TotalItemCount then
    raise ETagError.Create(ArrayIndicesOutOfRange);

  with Data^ do begin
    case ItemSize of
      varBoolean,
      varByte,
      varSmallInt,
      varInteger : IntegerArray[aOrd] := Round(aValue);
      varDouble : DoubleArray[aOrd] := aValue;
      varString : StringArray[aOrd]^ := FloatToStr(aValue);
    else
      raise ETagError.Create(ArrayAssignInvalidFor + Name);
    end;
  end;
  Self.Refresh;
end;

procedure TArrayTag.SetArrayString(aOrd : Integer; aValue : string);
begin
  if aOrd >= TotalItemCount then
    raise ETagError.Create(ArrayIndicesOutOfRange);

  with Data^ do begin
    case ItemSize of
      varBoolean,
      varByte,
      varSmallInt,
      varInteger : IntegerArray[aOrd] := StrToInt(aValue);
      varDouble : DoubleArray[aOrd] := StrToFloat(aValue);
      varString : StringArray[aOrd]^ := aValue;
    else
      raise ETagError.Create(ArrayAssignInvalidFor + Name);
    end;
  end;
  Self.Refresh;
end;

procedure TArrayTag.ArrayAssign(aOrd : Integer; aTag : TAbstractTag);
begin
  if aOrd >= TotalItemCount then
    raise ETagError.Create(ArrayIndicesOutOfRange);

  with Data^ do begin
    case ItemSize of
      varBoolean,
      varByte,
      varSmallInt,
      varInteger : IntegerArray[aOrd] := aTag.AsInteger;
      varDouble : DoubleArray[aOrd] := aTag.AsDouble;
      varString : StringArray[aOrd]^ := aTag.AsString;
    else
      raise ETagError.Create(ArrayAssignInvalidFor + Name);
    end;
  end;
  Self.Refresh;
end;

function TArrayTag.IndexOfOrdinate(aOrd : Integer) : Integer;
var I : Integer;
begin
  Result := 1;
  for I := 0 to aOrd - 1 do begin
    Result := Result * Dims[I];
  end;
end;

function TArrayTag.IndexOfOrdinates(const aCoords : array of Integer) : Integer;
var I : Integer;
begin
  Result := 0;
  for I := 0 to Length(aCoords) - 1 do begin
    Result := Result + (IndexOfOrdinate(I) * aCoords[I]);
  end;

//  Result := Result + aCoords[Length(aCoords) - 1];

{ [5,6,7] of [10,11,12]

  iteration   I   PreResult    aCoords[I]      Dims[I+1]  PostResult
  1           0   1            5               11       55
  2           1   50           6               12       55*12
}
end;

function TArrayTag.GetArrayLength : Integer;
var I : Integer;
begin
  Result := 1;
  for I := 0 to FOrdinateCount - 1 do begin
    Result := Result * Dims[I];
  end;
end;

function TArrayTag.GetAsString : string;
var I : Integer;
begin
  Result := '';
  for I := 0 to ArrayLength - 1 do begin
    if Result <> '' then
      Result := Result + ', ';
    if ItemSize = varDouble then
      Result := Result + Format('%.6f', [AsArrayDouble[I]])
    else
      Result := Result + Format('%d', [AsArrayInteger[I]]);
  end;
end;

function TArrayTag.GetAsShortString : string;
var I, Max : Integer;
begin
  Result := '';
  Max := ArrayLength;
  if Max > 20 then
    Max := 20;

  for I := 0 to Max - 1 do begin
    if Result <> '' then
      Result := Result + ', ';
    if ItemSize = varDouble then
      Result := Result + Format('%.6f', [AsArrayDouble[I]])
    else
      Result := Result + Format('%d', [AsArrayInteger[I]]);
  end;
end;

// The assumption is that an IsAxis array only converts the linear axis elements
// by display order of the array
procedure TArrayTag.ChangeUnits(aConvert : Double; Axis : Boolean = True);
var I : Integer;
begin
  if Axis then begin
    if aConvert <> Self.CurrentConvert then begin
      for I := 0 to SysObj.NC.AxisCount - 1 do begin
        if SysObj.NC.AxisType[I] = atLinear then
          AsArrayDouble[I] := AsArrayDouble[I] * CurrentConvert / aConvert;
      end;
    end;
    CurrentConvert := aConvert;
  end else begin
    if aConvert <> Self.CurrentConvert then begin
      for I := 0 to Self.TotalItemCount - 1 do begin
        AsArrayDouble[I] := AsArrayDouble[I] * CurrentConvert / aConvert;
      end;
    end;
    CurrentConvert := aConvert;
  end;
end;

function TArrayTag.GetDoubleValue(const aDims : array of Integer) : Double;
begin
  Result := FData.DoubleArray[IndexOfOrdinates(aDims)];
end;

procedure TArrayTag.SetDoubleValue(const aDims : array of Integer; Value : Double);
begin
  FData.DoubleArray[IndexOfOrdinates(aDims)] := Value;
end;


{procedure TArrayTag.ClearArray;
begin
  FillChar(FData, DataSize, 0);
end; }

function TArrayTag.GetDimensions(Index : TArrayOrdinate) : Integer;
begin
  Result := Dims[Index];
end;

procedure TArrayAccessTag.Assign(aTag : TAbstractTag);
begin
  DataSource.ArrayAssign(Index.AsInteger, aTag);
end;

procedure TArrayAccessTag.Refresh;
begin
  // Dont invoke callbacks on this
end;

function TArrayAccessTag.GetAsBoolean : Boolean;
begin
  Result := AsInteger <> 0;
end;

function TArrayAccessTag.GetAsDouble : Double;
begin
  Result := DataSource.AsArrayDouble[Index.AsInteger];
end;

function TArrayAccessTag.GetAsInteger : Integer;
begin
  Result := DataSource.AsArrayInteger[Index.AsInteger];
end;

function TArrayAccessTag.GetAsString : string;
begin
  Result := DataSource.AsArrayString[Index.AsInteger];
end;

procedure TArrayAccessTag.SetAsInteger(aValue : Integer);
begin
  DataSource.AsArrayInteger[Index.AsInteger] := aValue;
end;

procedure TArrayAccessTag.SetAsDouble(aValue : Double);
begin
  DataSource.AsArrayDouble[Index.AsInteger] := aValue;
end;

procedure TArrayAccessTag.SetAsBoolean(aValue : Boolean);
begin
  DataSource.AsArrayBoolean[Index.AsInteger] := aValue;
end;

procedure TArrayAccessTag.SetAsString(const aValue : string);
begin
  DataSource.AsArrayString[Index.AsInteger] := aValue;
end;

function TUnaryDoubleArrayTag.GetDataSize : Integer;
begin
  Result := Dims[0] * SizeOf(Double);
end;

constructor TUnaryDoubleArrayTag.Create(aOwner : TTagOwner; aName : string);
begin
  inherited Create(aOwner, aName);
  FOrdinateCount := 1;
  Dims[0] := 100;
  Dims[1] := 1;
  ItemSize := varDouble;
  FItemClass := TFloatTag;
  GetMem(FData, DataSize);
  FillChar(FData^, DataSize, 0);
end;

destructor TUnaryDoubleArrayTag.Destroy;
begin
  FreeMem(FData);
  inherited;
end;

procedure TUnaryDoubleArrayTag.SetElementCount(aCount : Integer);
begin
  Dims[0] := aCount;
  ReallocMem(FData, DataSize);
  FillChar(FData^, DataSize, 0);
end;


{ TDualDoubleArrayTag }
constructor TTriDoubleArrayTag.Create(aOwner: TTagOwner; aName: string);
begin
  inherited;
  FOrdinateCount := 2;
  Dims[0] := 100;
  Dims[1] := 3;
  Dims[2] := 0;
  ItemSize := varDouble;
  FItemClass := TFloatTag;
  GetMem(FData, DataSize);
  FillChar(FData^, DataSize, 0);
end;


function TTriDoubleArrayTag.GetDataSize: Integer;
begin
  Result := Dims[0] * Dims[1] * SizeOf(Double);
end;

function TTriDoubleArrayTag.GetD2(A, B : Integer) : Double;
begin
  Result := AsArrayDouble[A + B * Dimensions[0]];
end;

procedure TTriDoubleArrayTag.SetD2(A, B : Integer; aValue : Double);
begin
  AsArrayDouble[A + B * Dimensions[0]] := aValue;
end;

{ TGenericDoubleArrayTag }
constructor TGenericDoubleArrayTag.Create(aOwner: TTagOwner;
  aName: string);
begin
  inherited;
  FOrdinateCount := 2;
  Dims[0] := 100;
  Dims[1] := 6;
  ItemSize := varDouble;
  FItemClass := TFloatTag;
  GetMem(FData, DataSize);
  FillChar(FData^, DataSize, 0);
end;

destructor TGenericDoubleArrayTag.Destroy;
begin
  FreeMem(FData);
  inherited;
end;

function TGenericDoubleArrayTag.GetDataSize: Integer;
begin
  Result := ArrayLength * SizeOf(Double);
end;

procedure TGenericDoubleArrayTag.SetN(const aDims : array of Integer);
var I : Integer;
begin
  FOrdinateCount := Length(aDims);
  for I := 0 to Length(aDims) - 1 do begin
    Self.Dims[I] := aDims[I];
  end;

  ReallocMem(FData, GetDataSize);
end;

function TUnaryIntegerArrayTag.GetDataSize : Integer;
begin
  Result := Dims[0] * SizeOf(Integer);
end;

constructor TUnaryIntegerArrayTag.Create(aOwner : TTagOwner; aName : string);
begin
  inherited Create(aOwner, aName);
  FOrdinateCount := 1;
  Dims[0] := 100;
  ItemSize := varInteger;
  FItemClass := TOrdinalTag;
  GetMem(FData, Dims[0] * Sizeof(Integer));
  FillChar(FData^, Dims[0] * Sizeof(Integer), 0);
end;

destructor TUnaryIntegerArrayTag.Destroy;
begin
  FreeMem(FData);
  inherited;
end;


procedure TUnaryIntegerArrayTag.SetElementCount(aCount : Integer);
begin
  Dims[0] := aCount;
  Dims[1] := 1;
  ReallocMem(FData, Dims[0] * Sizeof(Integer));
end;

procedure TAbstractCNC.DoEndDrag(Target: TObject; X, Y: Integer);
begin
  if Assigned(OnEndDrag) then OnEndDrag(Self, Target, X, Y);
end;

constructor TTagDragObject.Create(aControl : TAbstractCNC);
begin
  inherited Create;
  FControl := aControl;
end;

procedure TTagDragObject.Finished(Target: TObject; X, Y: Integer; Accepted: Boolean);
begin
  if not Accepted then
    Target := nil;
  Control.DoEndDrag(Target, X, Y);
end;

{ TNamedInterface }

function TAbstractCommunications.AddTag(aName, aType: PChar): TTagRef;
var aTag : TAbstractTag;
    TC : TTagClass;
begin
  aTag := FindTag(aName);
  TC := FindTagClass(aType);
  if TC = nil then begin
    MessageLogFmt('Unable to Add tag %s with invalid tag type %s', [aName, aType]);
    Result := nil;
    Exit;
  end;

  if aTag <> nil then begin
    Result := aTag;
  end else begin
    Result := TagPublish(aName, TC);
  end;

  if Assigned(Result) then
    TAbstractTag(Result).IsVCL := False;
end;

type
  PNamedInterfaceCallback = ^TAbstractCommunicationsCallback;
  TAbstractCommunicationsCallback = record
    Tag : TAbstractTag;
    Method : TNamedCallbackEv;
  end;


procedure TAbstractCommunications.PluginRedirect(aTag : TAbstractTag);
var P : PNamedInterfaceCallback;
    I : Integer;
begin
  for I := 0 to CallbackList.Count - 1 do begin
    P := PNamedInterfaceCallback(CallbackList[I]);
    if P^.Tag = aTag then
      P^.Method(P^.Tag);
  end;
end;

procedure TAbstractCommunications.AddRedirect(aTag : TAbstractTag; Callback: TNamedCallbackEv);
var P : PNamedInterfaceCallback;
begin
  New(P);

  P^.Tag := aTag;
  P^.Method := Callback;

  CallbackList.Add(P);
end;

function TAbstractCommunications.Heads : Integer; stdcall;
begin
  Result := SysObj.Heads;
end;

procedure TAbstractCommunications.SetFault(Name : PChar; Text : PChar; Level : TFaultLevel);
begin
  SetFaultA(Name, Text, Level, nil);
end;

function TAbstractCommunications.SetFaultA(Name, Text : WideString; Level : TFaultLevel; Reset : TFaultResetStdcall) : FHandle; stdcall;
var cf : TCoreFaults;
begin
  Result := 0;
  case Level of
    faultNone : Exit;
    faultMessageOnly,
    faultWarning : cf := cfSysWarning;
    faultRewind : cf := cfSysRewind;
    faultStopCycle : cf := cfSysStopCycle;
    faultEstop : cf := cfSysEStop;
    faultFatal : cf := cfSysFatal;
    faultInterlock : cf := cfSysInterlock;
  else
    Exit;
  end;
  if InstallState <= InstallStateActive then begin
    EventLog('The following error was raised by a plugin before the system was stable,' +
    CarRet + 'it has be raised to a fatal error as there is no way to handle ' + CarRet + 'anything else this early in the system boot up process');
    EventLog(Text);
    EventLog('Sorry:)');
    DelayedFatalError.Add('Plugin initialise error: ' + Text)
  end else begin
    Result := SysObj.FaultInterface.SetFaultStdCall(Self, CoreFaults[cf], [Text], Reset)
  end;
end;

procedure TAbstractCommunications.FaultResetID(FID : FHandle);
begin
  SysObj.FaultInterface.FaultResetID(FID);
end;

procedure TAbstractCommunications.UpdateFault(FID : FHandle; Text : WideString);
begin
  SysObj.FaultInterface.UpdateFault(FID, [Text]);
end;

procedure TAbstractCommunications.ResetInterlock;
begin
  SysObj.FaultInterface.ResetInterlock;
end;

function TAbstractCommunications.AquireTag(aName, aType: PChar;
  Callback: TNamedCallbackEv): TTagRef;
var TC : TTagClass;
begin
  TC := FindTagClass(aType);
  if TC <> nil then begin
    if Assigned(Callback) then begin
      Result := TagEvent(aName, EdgeChange, TC, PluginRedirect);
      AddRedirect(Result, Callback);
    end else begin
      Result := TagEvent(aName, EdgeChange, TC, nil);
    end;
  end else begin
    Result := nil;
  end;
end;

function TAbstractCommunications.GetAsBoolean(aTag: TTagRef): Boolean;
begin
  Result := TAbstractTag(aTag).AsBoolean;
end;

function TAbstractCommunications.GetAsData(aTag : TTagRef; var Length : Integer) : Pointer; stdcall;
begin
  Result := TAbstractTag(aTag).DataPtr;
  Length := TAbstractTag(aTag).DataSize;
end;

function TAbstractCommunications.GetAsDouble(aTag: TTagRef): Double;
begin
  Result := TAbstractTag(aTag).AsDouble;
end;

function TAbstractCommunications.GetAsInteger(aTag: TTagRef): Integer;
begin
  Result := TAbstractTag(aTag).AsInteger;
end;

function TAbstractCommunications.GetAsOleVariant(aTag: TTagRef): OleVariant;
begin
  Result := TAbstractTag(aTag).AsOleVariant;
end;

procedure TAbstractCommunications.GetAsString(aTag: TTagRef; Buffer : PChar; MaxLen : Integer);
begin
  StrPLCopy(Buffer, TAbstractTag(aTag).AsString, MaxLen);
end;

procedure TAbstractCommunications.GetDataType(aTag: TTagRef; aType: PChar;
  MaxLen: Integer);
begin
  StrPLCopy(aType, TAbstractTag(aTag).ClassName, MaxLen);
end;

procedure TAbstractCommunications.ReleaseCallback(aTag: TTagRef;
  Callback: TNamedCallbackEv);
var I : Integer;
    P : PNamedInterfaceCallback;
begin
  for I := 0 to CallbackList.Count - 1 do begin
    P := PNamedInterfaceCallback(CallbackList[I]);
    if (TAbstractTag(aTag) = P^.Tag) and
       (Addr(Callback) = Addr(P^.Method)) then begin
       CallbackList.Delete(I);
       Dispose(P);
    end;
  end;
end;

function TAbstractCommunications.LoadOCT( const FileName: WideString): Boolean;
begin
  Result := False;
  if Assigned(SysObj.OCT) then begin
    SysObj.OCT.LoadOCT(FileName);
    Result := True;
  end;
end;

function TAbstractCommunications.LoadOPP(const FileName: WideString): Boolean;
begin
  EventLog('File load via named interface');
  Result := SysObj.FileManager.LoadActiveFile(FileName);
end;

procedure TAbstractCommunications.EventLog(const Msg: WideString;
  NtEvent: Word = EVENTLOG_INFORMATION_TYPE);
begin
  CoreCNC.EventLog(Msg, NtEvent);
end;

procedure TAbstractCommunications.MessageLog(const Msg : WideString); stdcall;
begin
  CoreCNC.MessageLog(Msg);
end;

procedure TAbstractCommunications.SetAsBoolean(aTag: TTagRef; Value: Boolean);
begin
  TAbstractTag(aTag).AsBoolean := Value;
end;

procedure TAbstractCommunications.SetAsDouble(aTag: TTagRef; Value: Double);
begin
  TAbstractTag(aTag).AsDouble := Value;
end;

procedure TAbstractCommunications.SetAsInteger(aTag: TTagRef; Value: Integer);
begin
  TAbstractTag(aTag).AsInteger := Value;
end;

procedure TAbstractCommunications.SetAsOleVariant(aTag: TTagRef; Value: OleVariant);
begin
  TAbstractTag(aTag).AsOleVariant := Value;
end;

procedure TAbstractCommunications.SetAsString(aTag: TTagRef; Value: PChar);
begin
  TAbstractTag(aTag).AsString := Value;
end;


function TAbstractCommunications.IsConnected(aTag: TTagRef): Boolean;
begin
  Result := TAbstractTag(aTag).IsConnected;
end;

function TAbstractCommunications.IsEvented(aTag: TTagRef): Boolean;
begin
  Result := TAbstractTag(aTag).IsEvented;
end;

function TAbstractCommunications.IsIO(aTag: TTagRef): Boolean;
begin
  Result := TAbstractTag(aTag).IsIO;
end;

function TAbstractCommunications.IsOwned(aTag: TTagRef): Boolean;
begin
  Result := TAbstractTag(aTag).IsOwned;
end;

function TAbstractCommunications.IsPersistent(aTag: TTagRef): Boolean;
begin
  Result := TAbstractTag(aTag).IsPersistent;
end;

function TAbstractCommunications.IsSystem(aTag: TTagRef): Boolean;
begin
  Result := TAbstractTag(aTag).IsSystem;
end;

function TAbstractCommunications.IsVCL(aTag: TTagRef): Boolean;
begin
  Result := TAbstractTag(aTag).IsVCL;
end;

function TAbstractCommunications.IsWritable(aTag: TTagRef): Boolean;
begin
  Result := TAbstractTag(aTag).IsWritable;
end;

procedure TAbstractCommunications.SetPersistent(aTag: TTagRef;
  aOn: Boolean);
begin
  TAbstractTag(aTag).IsPersistent := aOn;
end;

procedure TAbstractCommunications.AddLanguageFile(const Filename: WideString); stdcall;
begin
  if Assigned(SysObj.Localized) then begin
    SysObj.Localized.AppendLanguageFile(Filename);
  end
  else begin
    EventLog('Can''t add language file ' + Filename + ' Too early in system startup');
  end;
end;

function TAbstractCommunications.GetSignatureHost: ISignatureHost; stdcall;
begin
  Result:= SysObj.SignatureHost;
end;


function TAbstractCommunications.GetConfigurationParser: IConfigurationParser;
begin
  Result:= SysObj.ConfigParser;
end;

constructor TLocalization.Create;
var I : Integer;
    Lang : WideString;
    Tmp: IAcnc32StringArray;
begin
  inherited Create;

  SysObj.StringArraySet.UseArrayA('language', nil, Table);
  if not Assigned(Table) then
    SysObj.StringArraySet.CreateArray('language', nil, Tmp);
  SysObj.StringArraySet.UseArrayA('language', nil, Table);

  try
    Table.ImportFromCSV(ProfilePath + 'core_language.csv', False);
  except
    EventLog('Unable to open ' + ProfilePath + 'core_language.csv');
  end;

  try
    Table.ImportFromCSV(LivePath + 'profile_language.csv', Table.RecordCount > 2);
  except
    EventLog('Unable to open ' + LivePath + 'profile_language.csv');
  end;

  if LocaleOverride = '' then begin
    I := Languages.IndexOf(SysLocale.DefaultLCID);

    LanguageType := Copy(Languages.Ext[I], 1, 2);
    EventLog('Using Language type: ' + Languages.Ext[I] + ' CodePage: ' + IntToStr(Windows.GetACP));
  end else begin
    LanguageType := LocaleOverride;
  end;

  Column := 0;
  for I := 1 to Table.FieldCount - 1 do begin
    Table.FieldNameAtIndex(I, Lang);
    if CompareText(Lang, LanguageType) = 0 then begin
      Column := I;
      Break;
    end;
  end;

  for I := 0 to Table.RecordCount - 1 do begin
    Table.SetValue(I, 0, '0');
  end;
end;

procedure TLocalization.AppendLanguageFile(const Filename: string);
begin
   Table.ImportFromCSV(Filename, true);
end;

function TLocalization.GetString(const aToken: string): string;
var Index, Tmp : Integer;
    Lang : WideString;
begin
try
  //EventLog('Getting token: ' + aToken);
  if (Length(aToken) > 0) and
     (aToken[1] = '$') and
     (Column <> 0) then begin
      if Table.FindRecord(0, 1, aToken, Index) = 0 then begin
        Table.GetValue(Index, Column, Lang);
        if Lang = '' then
          Table.GetValue(Index, 2, Lang);

        Result := Lang;
        Table.GetValue(Index, 0, Lang);
        try
          Tmp := StrToInt(Lang);
        except
          Tmp := 0;
        end;
        Inc(Tmp);
        Table.SetValue(Index, 0, IntToStr(Tmp));

      end else begin
        Result := aToken;
        if LanguageLogging then
          EventLog('Language not found: ' + aToken);
      end;
    end else
      Result := aToken;
except
  on E: Exception do begin
    EventLog('Language ' + aToken + ' caused ' + e.Message);
    Result:= 'aToken';
  end;
end;
end;


function TLocalization.Resolve(const Token: WideString;
  out Value: WideString): Boolean;
begin
  Value:= GetString('$' + Token);
  Result:= True;
end;


procedure LocalizeForm(Form : TForm);
var I : Integer;
    GIndex :Integer;
begin
  Form.Caption := SysObj.Localized.GetString(Form.Caption);
  for I := 0 to Form.ComponentCount - 1 do begin
    if Form.Components[I] is TLabel then
      TLabel(Form.Components[I]).Caption := SysObj.Localized.GetString(TLabel(Form.Components[I]).Caption)
    else if Form.Components[I] is TButton then
      TButton(Form.Components[I]).Caption := SysObj.Localized.GetString(TButton(Form.Components[I]).Caption)
    else if Form.Components[I] is TGroupBox then
      TGroupBox(Form.Components[I]).Caption := SysObj.Localized.GetString(TGroupBox(Form.Components[I]).Caption)
    else if Form.Components[I] is TThreeLineButton then
      TThreeLineButton(Form.Components[I]).Legend := SysObj.Localized.GetString(TThreeLineButton(Form.Components[I]).Legend)
    else if Form.Components[I] is TPanel then
      TPanel(Form.Components[I]).Caption := SysObj.Localized.GetString(TPanel(Form.Components[I]).Caption)
    else if Form.Components[I] is TSpeedButton then
      TSpeedButton(Form.Components[I]).Caption := SysObj.Localized.GetString(TSpeedButton(Form.Components[I]).Caption)
    else if Form.Components[I] is  TRadioGroup then begin
      TRadioGroup(Form.Components[I]).Caption := SysObj.Localized.GetString(TRadioGroup(Form.Components[I]).Caption);
      if TRadioGroup(Form.Components[I]).Items.Count > 0 then begin
        for GIndex := 0 to TRadioGroup(Form.Components[I]).Items.Count-1 do begin
          TRadioGroup(Form.Components[I]).Items[GIndex] := SysObj.Localized.GetString(TRadioGroup(Form.Components[I]).Items[GIndex])
        end;
      end;
    end
    else if Form.Components[I] is  TMenuItem then begin
      TMenuItem(Form.Components[I]).Caption := SysObj.Localized.GetString(TMenuItem(Form.Components[I]).Caption);
    end
  end;
end;


constructor TFloatTag.Create(aOwner: TTagOwner; aName: string);
begin
  inherited;
  Convert := True;
end;

procedure TFloatTag.Refresh;
begin
  // Ensures this change does not attempt to invoke callbacks
end;

constructor TOrdinalTag.Create(aOwner: TTagOwner; aName: string);
begin
  inherited;
end;

procedure TOrdinalTag.Refresh;
begin
  // Ensures this change does not attempt to invoke callbacks
end;

procedure TAlphaTag.Refresh;
begin
  // Ensures this change does not attempt to invoke callbacks
end;

{ TSystemObject }

constructor TSystemObject.Create;
begin
  Keywords := TAcnc32Keywords.Create;
  InstalledList := TParamStrings.create;
  PluginList := TParamStrings.Create;
  SetLength(OCXLibrarys, 10);
  OCXLibraryCount:= 0;
  PluginLibraryList:= TList.Create;
  Translate:= TParamStrings.Create;
end;

procedure TSystemObject.ClearPluginLibraries;
begin
  while PluginLibraryList.Count > 0 do begin
    TPluginLibrarySet(PluginLibraryList[0]).Clear;
    PluginLibraryList.Delete(0);
  end;
end;


procedure TSystemObject.AddOCXLibrary(ISL: IScriptLibrary);
begin
  OCXLibrarys[OCXLibraryCount]:= ISL;
  Inc(OCXLibraryCount);
  if OCXLibraryCount >= Length(OCXLibrarys) then
    SetLength(OCXLibrarys, Length(OCXLibrarys) + 10);
end;

procedure TSystemObject.RemoveOCXLibrary(ISL: IScriptLibrary);
var I: Integer;
begin
 for I:= 0 to OCXLibraryCount - 1 do begin
   OCXLibrarys[I]:= nil;
 end;
end;


{ TPluginScriptWrapper }

constructor TPluginLibrarySet.Create;
var I: Integer;
begin
  SetLength(ClientList, SysObj.PluginList.Count + SysObj.OCXLibraryCount);
  for I:= 0 to SysObj.OCXLibraryCount - 1 do begin
    ClientList[I]:= SysObj.OCXLibrarys[I];
  end;
  for I:= 0 to SysObj.PluginList.Count - 1 do begin
    if Assigned(SysObj.PluginList.Objects[I]) then
      ClientList[I + SysObj.OCXLibraryCount]:= TNamedPlugin(SysObj.PluginList.Objects[I]).ScriptLibrary;
  end;
  SysObj.PluginLibraryList.Add(Self);
end;

procedure TPluginLibrarySet.Clear;
var I: Integer;
begin
  for I:= 0 to Length(ClientList) - 1 do begin
    if ClientList[I] <> nil then begin
      ClientList[I].SetScriptHost(nil);
      ClientList[I]:= nil;
    end;
  end;
  SetLength(ClientList, 0);
end;

destructor TPluginLibrarySet.Destroy;
begin
  Clear;
  inherited;
end;

procedure TPluginLibrarySet.Reset;
var I: Integer;
begin
  for I:= 0 to Length(ClientList) - 1 do begin
    if(ClientList[I] <> nil) then begin
      ClientList[I].Reset;
    end;
  end;
end;

function TPluginLibrarySet.Resolve(AName: string): IScriptMethod;
var I: Integer;
    CName: string;
begin
  AName:= UpperCase(AName);
  for I:= 0 to Length(ClientList) - 1 do begin
    if(ClientList[I] <> nil) then begin
      CName:= ClientList[I].GetLibraryName;
      if(Pos(CName + '.', AName) = 1) then begin
        System.Delete(AName, 1, Length(CName) + 1);
        Result:= ClientList[I].GetMethodByName(AName);
        if Assigned(Result) then
          Exit;
      end;
    end;
  end;
  Result:= nil;
end;

function TAbstractCommunications.GetPluginScriptSet: TPluginLibrarySet;
begin
  Result:= TPluginLibrarySet.Create;
end;

procedure TPluginLibrarySet.SetHost(Host: IScriptHost);
var I: Integer;
begin
  for I:= 0 to Length(ClientList) - 1 do begin
    if(ClientList[I] <> nil) then begin
      ClientList[I].SetScriptHost(Host);
    end;
  end;
end;

function ReadColor(S : TStreamTokenizer) : TColor;
var TmpStr : string;
begin
  S.ExpectedToken('=');
  TmpStr := S.Read;
  if TmpStr[1] = '$' then begin
    try
      Result := StrToInt(TmpStr);
    except
      on E : EConvertError do begin
        MessageLogFmt(ReadColourInvalid, [TmpStr]);
        Exit;
      end;
    end;
    Exit;
  end;

  if LowerCase(Copy(TmpStr,1, 2)) <> 'cl' then
    TmpStr := 'cl' + TmpStr;

  if not identtocolor(tmpstr, Integer(Result)) then begin
    MessageLogFmt(ReadColourInvalid, [TmpStr]);
  end;
end;


{ TAbstractPLC }

function TAbstractPLC.GetItemValue(Item: Pointer): Integer;
begin
  Result:= 0;
end;

procedure TAbstractPLC.SetItemValue(Item: Pointer; Value: Integer);
begin

end;

initialization
  AtomLock := TCriticalSection.Create;
  SystemLock := TCriticalSection.Create;
  LoggingList := TStringList.Create;
  BootLogList:= TStringList.Create;
  DelayedFatalError:= TStringList.Create;
  BootLogFirstTime:= True;

  SysObj := TSystemObject.Create;
//  RegisterCNCClass(TAbstractCommunications);

//  RegisterTagClass(TGenericTag);
  RegisterTagClass(TIntegerTag);
  RegisterTagClass(TDoubleTag);
  RegisterTagClass(TStringTag);
  RegisterTagClass(TAxisPositionTag);
  RegisterTagClass(TMethodTag);
//  RegisterTagClass(TIOFaultTag);
  RegisterTagClass(TArrayAccessTag);
  RegisterTagClass(TUnaryDoubleArrayTag);
  RegisterTagClass(TTriDoubleArrayTag);
  RegisterTagClass(TUnaryIntegerArrayTag);
  RegisterTagClass(TGenericDoubleArrayTag);
end.
