unit OPC;

interface

uses
  CNC32, CNCTypes, CoreCNC, prOPCServer, prOPCError, prOPCDa, Variants;

type
  TACNCOPCServer = class(TOPCItemServer)
  private
  protected
    function SubscribeToItem(ItemHandle : TItemHandle; UpdateEvent : TSubscriptionEvent): Boolean; override;
    procedure UnsubscribeToItem(ItemHandle : TItemHandle); override;
    procedure OnClientConnect(aServer : TClientInfo); override;
    procedure OnClientDisconnect(aServer : TClientInfo); override;
  public
    SubscriptionCount : Integer;
    SubscriptionCountTag : TAbstractTag;
    procedure OPCCallback(aTag : TAbstractTag);
    function GetItemInfo(const ItemID: String; var AccessRights: TAccessRights): Integer; override;
//    function GetItemInfo(const ItemID: String; var AccessPath: String; var AccessRights: TAccessRights): Integer; override;
    procedure ListItemIDs(List : TItemIdList); override;
    function GetItemValue(ItemHandle : TItemHandle; var Quality : Word) : OleVariant; override;
    procedure SetItemValue(ItemHandle : TItemHandle; const Value : OleVariant); override;
  end;

var
  ACNCOPCServer : TACNCOPCServer;

implementation

uses Installer;

{ TACNCOPCServer }

{function TACNCOPCServer.GetItemInfo(const ItemID: string;
  var AccessPath: String; var AccessRights: TAccessRights): Integer; }
function TACNCOPCServer.GetItemInfo(const ItemID: String; var AccessRights: TAccessRights): Integer;
var aTag : TAbstractTag;
begin
  aTag := SysObj.Comms.FindTag(ItemID);
  if aTag <> nil then begin
    Result := Integer(aTag)
  end else
    raise eOpcError.Create(OPC_E_INVALIDITEMID);
end;

function TACNCOPCServer.GetItemValue(ItemHandle: TItemHandle;
  var Quality: Word): OleVariant;
begin
  Result := TAbstractTag(ItemHandle).AsOleVariant;
end;

procedure TACNCOPCServer.ListItemIDs(List: TItemIdList);
  procedure AddTag(P : TOPCTagRecord; V : Integer);
  begin
    if not P.Input then
      List.AddItemID(P.NetAlias, [iaRead], V)
    else
      List.AddItemID(P.NetAlias, [iaRead, iaWrite], V)
  end;

var I : Integer;
    P : TOPCTagRecord;
begin
  for I := 0 to SysObj.Comms.FOPCList.Count - 1 do begin
    P := TOPCTagRecord(SysObj.Comms.FOPCList.Objects[I]);
    if P.TagClass = TIntegerTag then
      AddTag(P, varInteger)
    else if P.TagClass = TMethodTag then
      AddTag(P, varBoolean)
    else if P.TagClass = TDoubleTag then
      AddTag(P, varDouble)
    else if (P.TagClass = TStringTag) then
      AddTag(P, varString);
  end;
end;

procedure TACNCOPCServer.OnClientConnect(aServer: TClientInfo);
begin
  inherited;
end;

procedure TACNCOPCServer.OnClientDisconnect(aServer: TClientInfo);
begin
  inherited;
end;

procedure TACNCOPCServer.SetItemValue(ItemHandle: TItemHandle;
  const Value: OleVariant);
begin
  case VarType(Value) of
    varBoolean : TAbstractTag(ItemHandle).AsBoolean := Value;
    varByte, varSmallInt, varInteger : TAbstractTag(ItemHandle).AsInteger := Value;
    varDouble, varSingle : TAbstractTag(ItemHandle).AsDouble := Value;
    varOleStr, varString : TAbstractTag(ItemHandle).AsString := Value;
  end;
end;

function TACNCOPCServer.SubscribeToItem(ItemHandle: TItemHandle;
  UpdateEvent: TSubscriptionEvent): Boolean;
var aTag : TAbstractTag;
begin
  Inc(SubscriptionCount);
  aTag := TAbstractTag(ItemHandle);
  aTag.OPCCallback := UpdateEvent;
  Result := True;
end;

procedure TACNCOPCServer.OPCCallback(aTag : TAbstractTag);
begin
  if Assigned(aTag.OPCCallback) and (ClientCount > 0) then
    aTag.OPCCallback(aTag.AsOleVariant, OPC_QUALITY_GOOD);
end;

procedure TACNCOPCServer.UnsubscribeToItem(ItemHandle: TItemHandle);
begin
  Dec(SubscriptionCount);
  TAbstractTag(ItemHandle).OPCCallback := nil;
end;

initialization
  ACNCOPCServer := TACNCOPCServer.Create;
  RegisterOPCServer(OPCServerGUID, ServerVersion, ServerDesc, ServerVendor, ACNCOPCServer);
end.
