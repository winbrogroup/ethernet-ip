unit Term;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Dialogs,
  StdCtrls, ExtCtrls;

type
  TTerminalNewLine = procedure(Sender : TObject; text : string) of Object;
  TTerminal = class(TPanel)
  private
    FOnNewLine   : TTerminalNewLine;
    FEventEnable : boolean;
    ListBox      : TListBox;
    OkBtn        : TButton;
    Cursor       : TPoint;
    procedure AdjustBox;
    procedure KeyPressed(Sender: TObject);
    procedure TextAutoSelect(Sender : TObject);
    procedure ListBoxMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
  protected
    procedure Resize; override;
  public
    ComboBox     : TComboBox;
    constructor Create(Aowner : TComponent); override;
    procedure AddText(const Text : string);
    procedure SaveToFile(filename : string);
    property EventEnable : boolean read FEventEnable write FEventEnable;
    property OnNewLine : TTerminalNewLine read FOnNewLine write FOnNewLine;
  end;


{procedure Register;}{MGL 15/10}

implementation

uses CNCTypes;

constructor TTerminal.Create(Aowner : TComponent);
begin
  inherited Create(Aowner);
  ComboBox := TComboBox.Create(Aowner);
  ComboBox.Parent := Self;

  ListBox  := TListBox.Create(Aowner);
  ListBox.Parent := Self;
  ListBox.OnDblClick := TextAutoSelect;
  ListBox.OnMouseMove := ListBoxMouseMove;

  OkBtn := TButton.Create(Self);
  OkBtn.Parent  := Self;
  OkBtn.Default := True;
  OkBtn.OnClick := KeyPressed;

  AdjustBox;
end;

procedure TTerminal.KeyPressed(Sender: TObject);
var ind : integer;
    Item : String;
begin
  if ComboBox.Text = '' then
    Exit;

  with ComboBox do begin
    Item := Text;
//    AddText(Item);
    ind := Items.IndexOf(Item);
    if ind <> -1 then
      Items.Delete(ind);
    Items.Insert(0, Item);
    If Assigned(FOnNewLine) then
      FOnNewLine(Self, Item);
    Text := '';
  end;
end;

procedure TTerminal.AddText(const Text : string);
var p : integer;
    tmp : string;
    working : string;
begin
  Working := Text;
  with ListBox do begin
    Items.BeginUpdate;
    try
      P := System.Pos(#$D, Working);

      while P <> 0 do begin
        tmp := System.Copy(Working, 1, P - 1);
        Items.Add(tmp);
        System.Delete(Working, 1, P);
        P := System.Pos(#$D, Working);
      end;

      if Working <> '' then ListBox.Items.Add(Working);
      while Items.Count > 500 do begin
        Items.Delete(0);
      end;
    finally
      TopIndex := ListBox.Items.Count - 1;
      Items.EndUpdate;
    end;
  end;
end;

procedure TTerminal.TextAutoSelect(Sender : TObject);
var working : string;
    ItemSel : integer;
begin
  ItemSel := ListBox.ItemAtPos(Cursor, True);
  If ItemSel = -1 then
    Exit;
  working := ListBox.Items[ItemSel];
  if Assigned(FOnNewLine) then begin
    ComboBox.Text := working;
  end;
end;

procedure TTerminal.ListBoxMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
begin
  Cursor.X := X;
  Cursor.Y := Y;
end;

procedure TTerminal.SaveToFile(FileName : string);
begin
  ListBox.Items.SaveToFile(FileName);
end;

procedure TTerminal.Resize;
begin
  inherited resize;
  AdjustBox;
end;

procedure TTerminal.AdjustBox;
begin
  ListBox.Top    := 0;
  ListBox.Left   := 0;
  ListBox.Width  := width;
  ListBox.Height := Height - 30;

  ComboBox.Top    := Height - 30;
  ComboBox.Left   := 0;
  ComboBox.Width  := width;
  ComboBox.Height := 30;

  OkBtn.Left := width + 20;
end;

end.
