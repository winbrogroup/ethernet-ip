object SelectTag: TSelectTag
  Left = 988
  Top = 364
  Width = 222
  Height = 301
  Caption = 'Select Tag'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = True
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 233
    Width = 214
    Height = 41
    Align = alBottom
    TabOrder = 0
    object BitBtn1: TBitBtn
      Left = 16
      Top = 8
      Width = 75
      Height = 25
      TabOrder = 0
      Kind = bkOK
    end
    object BitBtn2: TBitBtn
      Left = 104
      Top = 8
      Width = 75
      Height = 25
      TabOrder = 1
      Kind = bkCancel
    end
  end
  object ListBox: TListBox
    Left = 0
    Top = 0
    Width = 214
    Height = 233
    Align = alClient
    ItemHeight = 13
    TabOrder = 1
    OnDblClick = ListBoxDblClick
  end
end
