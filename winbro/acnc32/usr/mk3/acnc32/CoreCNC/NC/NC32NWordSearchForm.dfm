object NC_NWordSearch: TNC_NWordSearch
  Left = 371
  Top = 48
  Width = 270
  Height = 716
  Caption = 'N Word Search'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -24
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = True
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 29
  object NList: TListBox
    Left = 0
    Top = 0
    Width = 262
    Height = 637
    Align = alClient
    ItemHeight = 29
    TabOrder = 0
  end
  object Panel1: TPanel
    Left = 0
    Top = 637
    Width = 262
    Height = 52
    Align = alBottom
    TabOrder = 1
    object BitBtn1: TBitBtn
      Left = 8
      Top = 8
      Width = 110
      Height = 40
      TabOrder = 0
      Kind = bkOK
    end
    object BitBtn2: TBitBtn
      Left = 136
      Top = 8
      Width = 110
      Height = 40
      TabOrder = 1
      Kind = bkCancel
    end
  end
end
