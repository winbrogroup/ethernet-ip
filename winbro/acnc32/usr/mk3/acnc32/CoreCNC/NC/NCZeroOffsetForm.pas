unit NCZeroOffsetForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, CoreCNC, StdCtrls, Buttons, ExtCtrls;

type
  TZeroOffsetForm = class(TForm)
    Panel1: TPanel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    InputPanel: TPanel;
    procedure FormCreate(Sender: TObject);
  private
    Labels: array[0..8] of TLabel;
  public
    Edits: array[0..8] of TEdit;
  end;

var
  ZeroOffsetForm: TZeroOffsetForm;

implementation

{$R *.dfm}

procedure TZeroOffsetForm.FormCreate(Sender: TObject);
var I: Integer;
begin
  for I:= 0 to 8 do begin
    Labels[I]:= TLabel.Create(Self);
    Labels[I].Parent:= InputPanel;
    Labels[I].Left:= 20;
    Labels[I].Top:= 20 + (I * 30);

    Edits[I]:= TEdit.Create(Self);
    Edits[I].Parent:= Self;
    Edits[I].Left:= 50;
    Edits[I].Top:= Labels[I].Top;

    if I < SysObj.NC.AxisCount then begin
      Labels[I].Caption:= SysObj.NC.AxisName[I];
    end
    else begin
      Labels[I].Caption:= '';
      Edits[I].Enabled:= False;
    end;
  end;
end;

end.
