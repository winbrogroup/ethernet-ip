unit NC32VariableView;

interface

uses Classes, SysUtils, CoreCNC, CncAbs, Cnc32, NC32, ComCtrls, Grids, Controls, Named,
     CncTypes, NCNames, NCProgram, NC32Types;

type
  TNC32VariableView = class(TExpandDisplay)
  private
    NC32 : TNC32;
    Tab : TTabControl;
    Grid : TStringGrid;
    procedure UpdateView(aTag : TAbstractTag);
    procedure AddData(aName : string; Data : TObject);
    procedure TabChange(Sender : TObject);
  protected
  public
    constructor Create(aOwner : TComponent); override;
    procedure InstallComponent(Params : TParamStrings); override;
    procedure Installed; override;
    procedure Open; override;
//    procedure Resize; override;
  end;

implementation

constructor TNC32VariableView.Create(aOwner : TComponent);
begin
  inherited Create(aOwner);
  Tab := TTabControl.Create(Self);
  Tab.Parent := Self;
  Tab.Align := alClient;
  Tab.OnChange := TabChange;
  Tab.MultiLine := True;

  Grid := TStringGrid.Create(Self);
  Grid.Parent := Tab;
  Grid.Align := alClient;
  Grid.DefaultColWidth := 110;
  Grid.ColWidths[0] := 140;
  Grid.ColCount := 2;
  Grid.Cells[0, 0] := NameLang;
  Grid.Cells[0, 1] := ValueLang;
end;

procedure TNC32VariableView.InstallComponent(Params : TParamStrings);
begin
  inherited InstallComponent(Params);
  if SysObj.NC is TNC32 then
    NC32 := TNC32(SysObj.NC)
  else
    NC32 := nil;
end;

var PositionTypes : array [TPositionType] of string;

procedure TNC32VariableView.Installed;
var I : Integer;
begin
  inherited Installed;

  PositionTypes[ptDisplay] :=      SysObj.Localized.GetString('$DISPLAY');
  PositionTypes[ptCommanded] :=    SysObj.Localized.GetString('$COMMANDED');
  PositionTypes[ptMachine] :=      SysObj.Localized.GetString('$MACHINE');
  PositionTypes[ptTarget] :=       SysObj.Localized.GetString('$TARGET');
  PositionTypes[ptDisttogo] :=     SysObj.Localized.GetString('$DISTTOGO');
  PositionTypes[ptFerr] :=         SysObj.Localized.GetString('$LAG');
  PositionTypes[ptBias] :=         SysObj.Localized.GetString('$BIAS');
  PositionTypes[ptCompensation] := SysObj.Localized.GetString('$COMPENSATION');
  PositionTypes[ptCPU] :=          SysObj.Localized.GetString('$CPU');
  PositionTypes[ptGridShift] :=    SysObj.Localized.GetString('$GRIDSHIFT');
  PositionTypes[ptTimeRIM] :=      SysObj.Localized.GetString('$TIMERIM');
  PositionTypes[ptMaster] :=       SysObj.Localized.GetString('$MASTER');
  PositionTypes[ptAcceleration] := SysObj.Localized.GetString('$ACCELERATION');
  PositionTypes[ptVelocity] :=     SysObj.Localized.GetString('$VELOCITY');

  Tab.TabHeight := 25;
  if Assigned(NC32) then begin
    AddData(SubroutineLang, nil);
    AddData(ProgramLang, NC32.PartProgram);
    AddData('#', NC32.NCHAL.HArray);
    AddData('##', NC32.NCHAL.HHArray);
    AddData('GEO', NC32.NCHAL.HProbe);
    AddData('G50', NC32.NCHAL.G50);
    AddData('G51', NC32.NCHAL.G51);
    AddData('G52', NC32.NCHAL.G52);
    AddData('G92', NC32.NCHAL.G92);
    AddData('G54', NC32.NCHAL.CoordSys[0]);
    AddData('G55', NC32.NCHAL.CoordSys[1]);
    AddData('G56', NC32.NCHAL.CoordSys[2]);
    AddData('G57', NC32.NCHAL.CoordSys[3]);
    AddData('G58', NC32.NCHAL.CoordSys[4]);
    AddData('G59', NC32.NCHAL.CoordSys[5]);
    AddData('GGroup', NC32.NCHAL.ActiveGGroup);
    AddData('SysA', NC32.NCHAL.SysVarA);
    AddData('SysU', NC32.NCHAL.SysVarU);
    AddData(StowNameA, NC32.NCHAL.StowA);
    AddData(StowNameB, NC32.NCHAL.StowB);
    AddData(StowNameC, NC32.NCHAL.StowC);
    AddData(ResultName, NC32.NCHAL.ResultA);
    AddData(IHWName, NC32.NCHAL.IHW);
    AddData(OHWName, NC32.NCHAL.OHW);
    AddData(ZeroOffsetAdjustName, NC32.NCHAL.ZeroOffsetAdjust);
    AddData(CMMName, NC32.NCHAL.CMMFile.Offsets);

    for I := 0 to SysObj.NC.AxisCount - 1 do
      AddData(SysObj.NC.AxisName[I], NC32.LAxisPosition[I]);
    TagEvent(NameDisplaySweep, EdgeChange, TMethodTag, UpdateView);
  end;
end;

{procedure TNC32VariableView.Resize;
begin
  inherited;
  TabChange(Self);
end; }

procedure TNC32VariableView.Open;
begin
  inherited;
  TabChange(Tab);
end;

procedure TNC32VariableView.UpdateView(aTag : TAbstractTag);
var I, J : Integer;
    Data : TObject;
    S : TStrings;
    A : TArrayTag;
begin
  if Visible then
  try
    if Tab.TabIndex <> -1 then begin
      if not NC32.PartProgram.Loaded then
        Tab.Tabs.Objects[0] := nil
      else if NC32.NCHAL.PartProgram.Loaded then
        Tab.Tabs.Objects[0] := NC32.PartProgram.SubRoutine[NC32.NCHAL.PPBGUnit].VariableList;

      Data := TObject(Tab.Tabs.Objects[Tab.TabIndex]);
      if Assigned(Data) then begin
        if Data is TNC32PartProgramFile then begin
          with Data as TNC32PartProgramFile do begin
            for I := 0 to VariableCount - 1 do
              if Grid.Cells[1, I + 1] <> Variable[I].AsString then
                Grid.Cells[1, I + 1] := Variable[I].AsString;
          end;
        end else if Data is TStrings then begin
          S := TStrings(Data);
          for I := 0 to S.Count - 1 do
            if Grid.Cells[1, I + 1] <> TAbstractTag(S.Objects[I]).AsString then
             Grid.Cells[1, I + 1] := TAbstractTag(S.Objects[I]).AsString;
        end else if Data is TArrayTag then begin
          A := TArrayTag(Data);
          if A.Dimensions[1] > 1 then begin
            for I := 0 to A.Dimensions[0] - 1 do
              for J := 0 to A.Dimensions[1] - 1 do
                if Grid.Cells[J + 1, I + 1] <> Format('%g', [A.GetDoubleValue([I, J])]) then
                  Grid.Cells[J + 1, I + 1] := Format('%g', [A.GetDoubleValue([I, J])]);
          end else begin
            for I := 0 to A.Dimensions[0] - 1 do
              if Grid.Cells[1, I + 1] <> A.AsArrayString[I] then
                Grid.Cells[1, I + 1] := A.AsArrayString[I];
          end;
        end else begin
          Grid.RowCount := 2;
        end;
      end;
    end;
  except
   // silent failure
  end;
end;

procedure TNC32VariableView.AddData(aName : string; Data : TObject);
begin
  Tab.Tabs.AddObject(aName, Data);
end;

{const PositionTypes : array [TPositionType] of string = (
   DisplayLang,
   CommandedLang,
   MachineLang,
   TargetLang,
   DisttogoLang,
   FerrLang,
   BiasLang,
   CompensationLang,
   CPULang,
   GridShiftLang,
   TimeRIMLang,
   MasterLang,
   Acceleration
); }

procedure TNC32VariableView.TabChange(Sender: TObject);
  procedure SetGrid(A, B : Integer; Titles : array of string);
  var I : Integer;
  begin
    Grid.RowCount := B;
    Grid.ColCount := A;

    for I := 0 to High(Titles) do
      Grid.Cells[I, 0] := Titles[I];

    if A > 1 then
      Grid.FixedCols := 1;
    if B > 1 then
      Grid.FixedRows := 1;
  end;

  procedure FirstColGrid;
  var I, W, Largest : Integer;
  begin
    if Grid.RowCount < 2 then
      Grid.RowCount := 2;

    Largest := 0;
    for I := 0 to Grid.RowCount - 1 do begin
      W := Grid.Canvas.TextWidth(Grid.Cells[0, I]);
      if W > Largest then
        Largest := W;
    end;
    Grid.ColWidths[0] := Largest + 12;
    for I := 1 to Grid.ColCount - 1 do begin
      Grid.ColWidths[I] := (Grid.Width - Grid.ColWidths[0] - 25) div (Grid.ColCount - 1)
    end;
  end;

var I : Integer;
    Data : TObject;
    S : TStrings;
    A : TArrayTag;
begin
  Tab.Tabs.Objects[0] := NC32.NCHAL.PartProgram;
  Grid.ColCount := 2;
  if Tab.TabIndex <> -1 then begin
    Data := TObject(Tab.Tabs.Objects[Tab.TabIndex]);
    if Assigned(Data) then begin
      if Tab.TabIndex = 0 then begin
        SetGrid(2, 27, ['Name', 'Data']);
        for I := 0 to 26 do begin
          Grid.Cells[0, I+1] := '_' + NCParamNames[TSFAParameter(I)]
        end;
      end else if Data is TNC32PartProgramFile then begin
        with Data as TNC32PartProgramFile do begin
          SetGrid(2, VariableCount + 1, ['Name', 'Data']);
          for I := 0 to VariableCount - 1 do
            Grid.Cells[0, I + 1] := Variable[I].Name;
        end;
//        FirstColGrid;
      end else if Data is TStrings then begin
        S := TStrings(Data);
        SetGrid(2, S.Count + 1, ['Name', 'Data']);
        for I := 0 to S.Count - 1 do
          Grid.Cells[0, I + 1] := TAbstractTag(S.Objects[I]).Name;
{      end else if Data is TTriDoubleArrayTag then begin
        A := TArrayTag(Data);
        SetGrid(4, A.Dimensions[0] + 1, ['Name', 'X', 'Y', 'Z']);
        for I := 0 to A.Dimensions[0] do
          Grid.Cells[0, I + 1] := A.Name + IntToStr(I); }
      end else if Data is TArrayTag then begin
        A := TArrayTag(Data);
        SetGrid(A.Dimensions[1] + 1, A.Dimensions[0] + 1, ['Name', 'Data']);
        if A.Dimensions[1] > 1 then begin
          for I := 0 to A.Dimensions[0] - 1 do
            Grid.Cells[0, I + 1] := A.Name + '[' + IntToStr(I);
          for I := 0 to A.Dimensions[1] -1  do
            Grid.Cells[I + 1, 0] := ',' + IntToStr(I) + ']';
        end else if Data is TAxisPositionTag then begin
          for I := 0 to A.Dimensions[0] - 1 do
            Grid.Cells[0, I + 1] := A.Name + '[' + IntToStr(I) + '] (' + PositionTypes[TPositionType(I)] + ')';
          Grid.Cells[1, 0] := 'Data';
        end else begin
          for I := 0 to A.Dimensions[0] - 1 do
            Grid.Cells[0, I + 1] := A.Name + '[' + IntToStr(I) + ']';
          Grid.Cells[1, 0] := 'Data';
        end;
      end else begin
        Grid.RowCount := 2;
      end;
    end;
    FirstColGrid;
  end;
  UpdateView(nil);
end;

initialization
  RegisterCNCClass(TNC32VariableView);
end.

