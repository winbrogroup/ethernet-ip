unit NC32;

interface

uses Classes, CNCTypes, CNC32, CoreCNC, Windows, NCNames, Named,
     NCProgram, CompoundFiles, SyncObjs, Forms, NCMath, ScriptTypeInfo,
     SysUtils, ScriptFunctions, NC32Types, Controls, CNCAbs,
     NC32ConfigurationForm, GEOPlugin, FaultRegistration, CMMFileV1, CNCRegistry,
     LogSyncObjs, INCInterface, IStringArray;

// Program functions required
// 2. NO Recursion ?
// 3. DONE Two forms of line number information HW when running BGLine when single block
// 5. DONE    Determine synchronization policy
// 6. Determine variable is constant for block look-ahead?
// 9. Restore modal state on exit from sub.
// Other support stuff
// 7. (CREATE Plc Item That interogates necessary bits) NCHAL Error messaging.

type
  TNC32 = class;

  TFIDMap = array [T32Bit] of FHandle;

  TNCState = (  ncsInCycle,
                ncsInMotion,
                ncsFeedHold,
                ncsHomed,
                ncsSingleBlock,
                ncsOptionalStop,
                ncsBlockDelete,
                ncsFeedOverrideEnable,
                nscJustPlayingAbout // delete this
  );

  TNCStates = set of TNCState;

  TCutterCompensation = (
    ccNone,
    ccLeft,
    ccRight
  );

  TInterpolationMode = (
    imRapid,
    imLinear,
    imCW,
    imCCW,
    imThreadConstant,
    imThreadIncreasing,
    imThreadDecreasing,
    imCWSpherical,
    imCCWSpherical,
    imParabolic
  );

  TPlaneSelection = (
    psXY,
    psXZ,
    psYZ
  );

  TAxisOffsets = array [0..16] of Double;

  TNCProgramStack = class(TList)
  public
    procedure Pop;
    procedure Push(aUnit, aInst, aLine : Integer);
    function TopUnit : Integer;
    function TopInst : Integer;
    function TopLine : Integer;
  end;

  TNCHAL = class(TThread)
  private
    FPartProgram : TNC32PartProgramFile;
    FHArray : TUnaryDoubleArrayTag;
    FHHArray : TUnaryDoubleArrayTag;
    FSysVarA, FSysVarU : TUnaryDoubleArrayTag;
    NC32 : TNC32;
    procedure SetPartProgram(aPartProgram : TNC32PartProgramFile);
  protected
    TagOwner : TNC32;
    Blocking : Boolean;
    LookAHead : Integer;
    HWSynch : Boolean;
    ActiveConfig : TNC32Configure;
    FUnits : Double;
    Lock : TLoggingCriticalSection;

    RotaryCount : TAbstractTag;
    StartPermissiveTag : TAbstractTag;
    ResetCompleteTag : TAbstractTag;
    SweepsPerSecondTag: TAbstractTag;
    MinSweepsPerSecondTag: TAbstractTag;
    SweepsPerSecondResetTag: TAbstractTag;

    Stack : TNCProgramStack;
    SPP : TSubPartProgram;

    TraceIntoC3: TAbstractTag;  // Option prevent reporting of line numbers in C3 unless in single block
    DisplayProgramLine : Integer; // Program line for display when not tracing into C3
    DisplayProgramUnit : Integer; // Unit index for display when not tracing into C3

    PPHWUnit : Integer; // Unit running in hardware
    PPHWLine : Integer; // Line running in hardware

    FNCStates : TNCStates;
    FAxisCount : Integer;
    FProgramLine : Integer;
    FRequestCNCMode : TCNCMode;
    FCNCModes : TCNCModes;
    FJogMode : TJogModes;
    FJogIncrement : Integer;
    FActiveAxis : Integer;
    FDryRun : Boolean;
    FNoCycle: Boolean;

    RunBlockDelete : Boolean;
    NormalLookAhead : Integer;

    ZeroOffset : TUnaryDoubleArrayTag;
    SoftLimitPlus : TUnaryDoubleArrayTag;
    SoftLimitMinus : TUnaryDoubleArrayTag;

    NormalTermination : TAbstractTag;

    // Data applied to rotary buffer commands
    LocalOffset : TAxisOffsets;
    CoordOffset : TAxisOffsets;
    ShiftOffset : TAxisOffsets;
    TotalOffset : TAxisOffsets; // Total offset, sum of local and coord
    Incremental : Boolean;
    CuttingMode : Boolean;  // Exact stop = false
    CutterCompensation : TCutterCompensation;
    InterpolationMode : TInterpolationMode;
    CoordinateOffset : Integer;
    PlaneSelection : TPlaneSelection;
    InverseTimeFeed : Boolean;

    // Actual state of running motion program
    ActiveLocalOffset : TAxisOffsets;
    ActiveCoordOffset : TAxisOffsets;
    ActiveShiftOffset : TAxisOffsets;
    ActiveOffset : TAxisOffsets; // Total offset, sum of local and coord
    ActiveIncremental : Boolean;
    ActiveExactStop : Boolean;
    ActiveCutterCompensation : TCutterCompensation;
    ActiveInterpolationMode : TInterpolationMode;
    ActiveCoordinateOffset : Integer;
    ActivePlaneSelection : TPlaneSelection;
    ActiveInverseTimeFeed : Boolean;
    ActiveNWord : TAbstractTag;

    // Debug timing routines
    SweepsPerSecond : Double;
    SweepCount : Integer;
    LastSecondStart : Integer;

    FSweepCapture : TNotifyEvent;

    AxisInMotion : T32Bits;
    AxisWarnFollowError : T32Bits;
    AxisFatalFollowError : T32Bits;
    AxisHomed : T32Bits;
    AxisOpenLoop : T32Bits;
    AxisPositiveLimit : T32Bits;
    AxisNegativeLimit : T32Bits;
    AxisInPosition : T32Bits;
    AxisAmplifierFault : T32Bits;
    AxisEncoderLoss : T32Bits;

    ProgramStartTime : TDateTime;
    Interrupted : Boolean;  // Sets to true if the program is halted for any reason
    RewindComplete : Boolean;
    MExtTag : TAbstractTag;

    ResetRewindRequest : Boolean; // Used to signal RW entered : prevents thread lock
    RetraceList: TList;
    RetraceEnable: Boolean;

    ExecuteMacroOnResetComplete: Boolean;
    MacroExecute: Boolean;

    // NWord set before ResetRewind
    PreNWordLabel : string;
    PreNWordIndex : Integer;
    PreNWordLineNumber: Integer;

    procedure SetAxisPositionTags(APList : TList); virtual; abstract;
    procedure SetJogPlus(aAxis : Integer; aOn : Boolean); virtual; abstract;
    procedure SetJogMinus(aAxis : Integer; aOn : Boolean); virtual; abstract;
    procedure SetCycleStart(aOn : Boolean); virtual; abstract;
    procedure SetFeedHold(aOn : Boolean); virtual; abstract;
    procedure SetCycleStop(aOn : Boolean); virtual; abstract;
    procedure SetCycleAbort(aOn: Boolean); virtual; abstract;
    procedure SetBlockDelete(aOn : Boolean); virtual;
    procedure SetFeedOverride(aFeed : Double); virtual; abstract;
    procedure SetFeedOverrideEnable(aOn : Boolean); virtual; abstract;
    procedure SetJogMode(aMode : TJogModes); virtual; abstract;
    procedure SetJogIncrement(aIncrement : Integer); virtual; abstract;

    function GetJogPlus(aAxis : Integer) : Boolean; virtual; abstract;
    function GetJogMinus(aAxis : Integer) : Boolean; virtual; abstract;
    function GetCycleStart : Boolean; virtual; abstract;
    function GetFeedHold : Boolean; virtual; abstract;
    function GetCycleStop : Boolean; virtual; abstract;
    function GetCycleAbort: Boolean; virtual; abstract;
    function GetAxisNames : string;  virtual; abstract;
    function GetTargetFeedrate : Double; virtual; abstract;
    function GetActualFeedrate : Double; virtual; abstract;
    function GetBlockDelete : Boolean; virtual;
    function GetFeedOverride : Double; virtual; abstract;
    function GetFeedOverrideEnable : Boolean;
    function GetHomed : Boolean;

    procedure SetRequestCNCMode(aMode : TCNCMode); virtual; abstract;

    procedure SetSingleBlock(aOn : Boolean); virtual;
    function  GetSingleBlock : Boolean; virtual;
    procedure SetOptionalStop(aOn : Boolean); virtual;
    function GetOptionalStop : Boolean; virtual;
    function GetAxisType(Index : Integer) : TAxisType; virtual; abstract;

    procedure UpdateHardware; virtual; abstract;
    procedure Execute; override;

    procedure ExecutePartProgram; virtual;
    procedure RunAssign;
    procedure RunReturn(var NextBGInst : Integer);
    procedure RunIF(var NextBGInst : Integer);
    procedure RunIndexVar;
    procedure RunICompare(var NextBGInst : Integer);
    procedure RunHash(var NextBGInst : Integer);
    procedure RunCall(var NextBGInst : Integer);
    procedure RunScriptCall;
    procedure UpdatePersistent(aTag : TAbstractTag); virtual;
    procedure SetActiveAxis(aAxis : Integer); virtual;
    procedure ExtFunction(aFunc : Integer); dynamic;

    property SweepCapture : TNotifyEvent read FSweepCapture write FSweepCapture;
    procedure SetUnits(aUnits : Double); virtual;
    procedure SetZeroOffset; virtual;
    procedure SetZeroOffsetAxis; virtual;
    procedure SetHalfZeroOffset; virtual;
    procedure ClearZeroOffsetAxis; virtual;
    procedure ClearZeroOffset; virtual;
    procedure PresetZeroOffset; virtual;
    procedure UpdateActiveOffset; virtual; abstract;
    procedure CopyCurrentPosition; virtual;

    procedure SetReleaseAxes(Release: Boolean); virtual;
    function ResetRewindComplete: Boolean; virtual;
    procedure SetNoCycle(NoC: Boolean); virtual;
  public
    // These are public but should NOT be written to by anything
    PPBGUnit : Integer; // Unit running in Background
    PPBGInst : Integer; // Instruction running in Background
    PPBGLine : Integer; // Line running in Background

    StowA : TUnaryDoubleArrayTag;
    StowB : TUnaryDoubleArrayTag;
    StowC : TUnaryDoubleArrayTag;

    ResultA : TUnaryDoubleArrayTag;
    IHW : TUnaryDoubleArrayTag;
    OHW : TUnaryDoubleArrayTag;

    HProbe : TTriDoubleArrayTag;
    CMMFile : TCMMFile;

    MCode : TAbstractTag;
    SCode : TAbstractTag;
    TCode : TAbstractTag;
    MCodeAFC : TAbstractTag;

    // Storage of offset data
    CoordSys : array [0..5] of TUnaryDoubleArrayTag;
    G50 : TUnaryDoubleArrayTag;
    G51 : TUnaryDoubleArrayTag;
    G52 : TUnaryDoubleArrayTag;
    G92 : TUnaryDoubleArrayTag;

    ActiveGGroup : TUnaryIntegerArrayTag;
    ZeroOffsetAdjust : TUnaryDoubleArrayTag;
    //-- END OF -- You have been warned --------------

    ErrorString : string;
    constructor Create(aTagOwner : TNC32; DeviceIndex : Integer); virtual;
    destructor Destroy; override;
    function CreateConfig : TNC32Configure; dynamic; abstract;
    procedure Configure; virtual; abstract;
    procedure TestPoint; virtual; abstract;
    procedure Clean; virtual; abstract;
    procedure Reset; virtual; abstract;
    procedure ResetRewind; virtual;
    procedure VCLUpdate; virtual;
    procedure TraceStart; virtual;
    procedure TraceStop; virtual;
    procedure TraceClear; virtual;
    function RetraceStepFromCurrent: TRetraceStep;
    property PartProgram : TNC32PartProgramFile read FPartProgram write SetPartProgram;
    property JogPlus[Axis : Integer] : Boolean read GetJogPlus write SetJogPlus;
    property JogMinus[Axis : Integer] : Boolean read GetJogMinus write SetJogMinus;
    property CycleStart : Boolean read GetCycleStart write SetCycleStart;
    property FeedHold : Boolean read GetFeedHold write SetFeedHold;
    property CycleStop : Boolean read GetCycleStop write SetCycleStop;
    property CycleAbort : Boolean read GetCycleAbort write SetCycleAbort;
    property NCStates : TNCStates read FNCStates;
    property AxisCount : Integer read FAxisCount;
    property AxisNames : string read GetAxisNames;
    property FeedOverride : Double read GetFeedOverride write SetFeedOverride;
    property FeedOverrideEnable : Boolean read GetFeedOverrideEnable write SetFeedOverrideEnable;
    property ProgramLine : Integer read FProgramLine;
    property RequestMode : TCNCMode read FRequestCNCMode write SetRequestCNCMode;
    property CNCModes : TCNCModes read FCNCModes;
    property SingleBlock : Boolean read GetSingleBlock write SetSingleBlock;
    property OptionalStop : Boolean read GetOptionalStop write SetOptionalStop;
    property BlockDelete : Boolean read GetBlockDelete write SetBlockDelete;
    property TargetFeedRate : Double read GetTargetFeedRate;
    property ActualFeedRate : Double read GetActualFeedRate;
    property AxisType[Index : Integer] : TAxisType read GetAxisType;
    property JogMode : TJogModes read FJogMode write SetJogMode;
    property Homed : Boolean read GetHomed;
    property JogIncrement : Integer read FJogIncrement write SetJogIncrement;
    property ActiveAxis : Integer read FActiveAxis write SetActiveAxis;
    property DryRun : Boolean read FDryRun write FDryRun;
    property Units : Double read FUnits;
    property NoCycle: Boolean read FNoCycle write FNoCycle;

    property HArray: TUnaryDoubleArrayTag read FHArray;
    property HHArray: TUnaryDoubleArrayTag read FHHArray;
    property SysVarA: TUnaryDoubleArrayTag read FSysVarA;
    property SysVarU: TUnaryDoubleArrayTag read FSysVarU;

  end;

  TNCHALClass = class of TNCHAL;

  TNC32ClientServerSystem = class(TAbstractNC)
  end;

  TNC32 = class(TNC32ClientServerSystem)
  private
    FNCHAL : TNCHAL;
    FConfig : TNC32Configure;
    FPartProgram: TNC32PartProgramFile;
    MDIPartProgram: TNC32PartProgramFile;
    MacroPartProgram: TNC32PartProgramFile;
    C3 : TNC32PartProgramFile;

    AxisWarnFollowErrorFID : TFIDMap;
    AxisFatalFollowErrorFID : TFIDMap;
    AxisHomedFID : TFIDMap;
    AxisOpenLoopFID : TFIDMap;
    AxisPositiveLimitFID : TFIDMap;
    AxisNegativeLimitFID : TFIDMap;
    AxisAmplifierFaultFID : TFIDMap;
    AxisEncoderLossFID : TFIDMap;

    InchDefaultTag : TAbstractTag;
    ActiveFault : FHandle;

    GEOName : string;
    GEO : TGEOPlugin;
    ReloadC3FID : FHandle;
    SafeProgramPath : string;
    FOperatorDialogueActive : Boolean;
    OperatorDialogueTag: TAbstractTag;
    ReportTag : TAbstractTag;
    LoggingTags : TStringList;

    WasResetRewind: Boolean;

    SeperateOpenLoopMessages: Boolean;

    procedure NWordSearch(aTag : TAbstractTag);
    function ResetFault(aFault : FHandle) : Boolean;
    procedure NCConfigure(aTag : TAbstractTag);
    procedure NCReloadC3(aTag : TAbstractTag);
    function ResetFailedC3(Handle : FHandle) : Boolean;
    procedure GEOShow(aTag : TAbstractTag);
    procedure GEOHide(aTag : TAbstractTag);
    procedure SavePersistentData(aTag : TAbstractTag);
    procedure InternalResetRewind;
  protected
    procedure SetPartProgram(aTag : TAbstractTag); override;
    function ResolveVariable(Sender : TObject; const aName : string) : TAbstractTag;
    function ResolveFunction(Sender : TObject; const aName, aFunc : string) : TAbstractTag;
    function ResetProgramLoadError(aFault : FHandle) : Boolean;
    function ResetMDIProgramLoadError(aFault : FHandle) : Boolean;
    function ResetMacroProgramLoadError(aFault : FHandle) : Boolean;
    procedure CNCReset(aTag : TAbstractTag); override;
    procedure CNCResetRW(aTag : TAbstractTag); override;
    function GetAxisType(Index : integer) : TAxisType; override;
    function GetSubCount : Integer; override;
    function GetActiveSubRoutine : Integer; override;
    function GetSubRoutine(Index : Integer) : WideString; override;
    function GetSubRoutineLines(Index : Integer) : TStringList; override;
    function GetProgramLine : Integer; override;
    procedure CreateReportString; virtual;
    procedure SetOperatorDialogueActive(A: Boolean);
  public
    LAxisPosition : TList;
    constructor Create(aOwner : TComponent); override;
    destructor Destroy; override;
    procedure InstallComponent(Params : TParamStrings); override;
    procedure Installed; override;
    procedure CheckInstallation; override;
    procedure IShutdown; override;
    procedure ClientDualEvent(cevent : TNCEvent; aTag : TAbstractTag); override;
    procedure UpdateClients(aTag : TAbstractTag);
    property NCHAL : TNCHAL read FNCHAL;
    property Config : TNC32Configure read FConfig;
    procedure SetMDIBuffer(aMDI : string); override;
    function GetMDIBuffer: string; override;
    procedure ExecuteMacro(Filename: string); override;
    procedure OperatorDialogue(const Breed, Item : string);
    procedure TraceOn;
    procedure TraceClear;
    procedure TraceSuspend;
    procedure TraceResume;
    procedure MDTReadRow(SA: IACNC32StringArray; Prefix: string; Row: Integer);
    procedure MDTWriteRow(SA: IACNC32StringArray; Prefix: string; Row: Integer);
    function ResetRewindComplete: Boolean; override;
    property PartProgram: TNC32PartProgramFile read FPartProgram;
    property OperatorDialogueActive : Boolean read FOperatorDialogueActive write SetOperatorDialogueActive;
  end;

  TNC32PositionGather = class(TExpandDisplay)
  private
    NC32 : TNC32;
    AxisList : TList;
    StartUpdate : Integer;
    BlockingThread : Boolean;
    Scale : Double;
    XOrigin : Double;
    YOrigin : Double;
    procedure UpdateDisplay(aTag : TAbstractTag);
    procedure NCPositionCapture(Sender : TObject);
    procedure ClearHistory(aTag : TAbstractTag);
    procedure SetScale(aTag : TAbstractTag);
    procedure SetXOrigin(aTag : TAbstractTag);
    procedure SetYOrigin(aTag : TAbstractTag);
  protected
    procedure Paint; override;
  public
    constructor Create(aOwner : TComponent); override;
    destructor Destroy; override;
    procedure InstallComponent(Params : TParamStrings); override;
    procedure Installed; override;
  end;

procedure RegisterNCDeviceClass(ClassType : TNCHALClass);
function FindNCDeviceClass(const aTypeName : string) : TNCHALClass;

implementation

uses
  NC32NWordSearchForm,
  Graphics,
  NCZeroOffsetForm,
  ClipBrd;

var
  RegNCList : TStringList;

resourcestring
  CannotSavePersistentData = 'Cannot save persistent data unless in manual';


const
  NCClassNameNotUnique = 'NC Classname [%s] is not unique';


constructor TNCHAL.Create(aTagOwner : TNC32; DeviceIndex : Integer);
begin
  inherited Create(True);
  Lock := TLoggingCriticalSection.Create('NC32');
  TagOwner := aTagOwner;
  NC32 := aTagOwner;
  Stack := TNCProgramStack.Create;
  FCNCModes := [CNCModeManual];
  NormalLookAhead := 4;
  CMMFile := TCMMFile.Create;
  RetraceList:= TList.Create;
end;

destructor TNCHAL.Destroy;
begin
  if Assigned(Lock) then
    Lock.Free;
  if Assigned(Stack) then
    Stack.Free;
  if Assigned(RetraceList) then
    RetraceList.Free;
  inherited Destroy;
end;

procedure TNCHAL.ResetRewind;
begin
  try
    if Assigned(ResetCompleteTag) then
      ResetCompleteTag.AsBoolean := True;
    PPBGInst := 0;
    PPBGUnit := 0;
    PPHWLine := 0;
    PPHWUnit := 0;
    ActiveNWord.AsString := '';
    MExtTag.AsInteger := 0;
    MacroExecute:= False;

    if Assigned(NC32.PartProgram) then begin
      try
        NC32.PartProgram.ScriptLibrary.SetEntryMethod(nil);
        NC32.PartProgram.ScriptLibrary.Rewind;
      except
        on E: Exception do
        SysObj.FaultInterface.SetFault(NC32, CoreFaults[cfSysWarning], ['Reset-Rewind Program library Failed: ' + E.Message ], nil);
      end;
    end;

    if Assigned(NC32.MDIPartProgram) then begin
      try
        NC32.MDIPartProgram.ScriptLibrary.SetEntryMethod(nil);
        NC32.MDIPartProgram.ScriptLibrary.Rewind;
      except
        SysObj.FaultInterface.SetFault(NC32, CoreFaults[cfSysStopCycle], ['Reset-Rewind MDI library Failed'], nil);
      end;
    end;

    if Assigned(NC32.MacroPartProgram) then begin
      try
        NC32.MacroPartProgram.ScriptLibrary.SetEntryMethod(nil);
        NC32.MacroPartProgram.ScriptLibrary.Rewind;
      except
        SysObj.FaultInterface.SetFault(NC32, CoreFaults[cfSysStopCycle], ['Reset-Rewind Macro library Failed'], nil);
      end;
    end;

    if Assigned(NC32.C3) then begin
      try
        NC32.C3.ScriptLibrary.SetEntryMethod(nil);
        NC32.C3.ScriptLibrary.Rewind;
      except
        SysObj.FaultInterface.SetFault(NC32, CoreFaults[cfSysStopCycle], ['Reset-Rewind C3 library Failed'], nil);
      end;
    end;

    if not ActiveConfig.CoordinateSetup[0].InchDefault then
      GCodeGroupDefaults[gcgI] := 21
    else
      GCodeGroupDefaults[gcgI] := 20;

    // Changed defaulted to MDI part program if not in auto
    if CNCModeAuto in CNCModes then
      PartProgram := TNC32(SysObj.NC).PartProgram
    else if CNCModeMDI in CNCModes then
      PartProgram := TNC32(SysObj.NC).MDIPartProgram
    else
      PartProgram := TNC32(SysObj.NC).MacroPartProgram;

    if PartProgram.Loaded then
      SPP := PartProgram.SubRoutine[PPBGUnit];

    Incremental := False;
    //FeedHold := False;
    if ProgramStartTime <> 0 then begin
      RewindComplete := True;
      NC32.CreateReportString;
    end;
  except
    SysObj.FaultInterface.SetFault(NC32, CoreFaults[cfSysStopCycle], ['Reset-Rewind Failed'], nil);
  end;
end;

procedure TNCHAL.VCLUpdate;
begin
end;

procedure TNCHAL.SetSingleBlock(aOn : Boolean);
begin
  if aOn then begin
    if not SingleBlock then
      Include(FNCStates, ncsSingleBlock)
    else
      Exclude(FNCStates, ncsSingleBlock);
  end;
end;

function  TNCHAL.GetSingleBlock : Boolean;
begin
  Result := ncsSingleBlock in FNCStates;
end;

procedure TNCHAL.SetOptionalStop(aOn : Boolean);
begin
  if aOn then
    Include(FNCStates, ncsOptionalStop)
  else
    Exclude(FNCStates, ncsOptionalStop);
end;

function TNCHAL.GetOptionalStop : Boolean;
begin
  Result := ncsOptionalStop in FNCStates;
end;

function TNCHAL.GetHomed : Boolean;
begin
  Result := ncsHomed in FNCStates;
end;

procedure TNCHAL.SetBlockDelete(aOn : Boolean);
begin
  if aOn then
    Include(FNCStates, ncsBlockDelete)
  else
    Exclude(FNCStates, ncsBlockDelete);
end;

function TNCHAL.GetBlockDelete : Boolean;
begin
  Result := ncsBlockDelete in FNCStates;
end;

constructor TNC32.Create(aOwner : TComponent);
begin
  inherited Create(aOwner);
  LoggingTags := TStringList.Create;
end;

destructor TNC32.Destroy;
begin
  C3.Free;
  PartProgram.Free;
  MDIPartProgram.Free;
  MacroPartProgram.Free;
  NCHAL.Free;
  inherited;
end;

function TNC32.ResolveFunction(Sender : TObject; const aName, aFunc : string) : TAbstractTag;
begin
  Result := nil;
end;

procedure TNC32.IShutdown;
begin
  inherited;
  if Assigned(NCHAL) then begin
    NCHAL.SetUnits(1);
    NCHAL.Terminate;
  end;
  PartProgram.Clean;
  C3.Clean;
  MDIPartProgram.Clean;
  MacroPartProgram.Clean;
end;

function TNC32.ResetProgramLoadError(aFault : FHandle) : Boolean;
begin
  Result := NCHAL.PartProgram.Loaded;
end;

function TNC32.ResetMDIProgramLoadError(aFault : FHandle) : Boolean;
begin
  // ???? Check this logic should we allow reset when not in MDI?
  Result := MDIPartProgram.Loaded or not (CNCModeMDI in NCHAL.CNCModes);
end;

function TNC32.ResetMacroProgramLoadError(aFault : FHandle) : Boolean;
begin
  Result:= True;
end;


{ Time to complete for differing Sleep times
  SweepsPerSecond set to 50

  Sleep      FSDVideo  FSD-Dual
  10         0:39      0:47        39   47
  20         1:11      1:34        71   94
  30         1:39      1:34        99   94
  40         2:07      2:21       127  141
  50         2:34      3:08       154  188

}


procedure TNCHAL.Execute;
var Tick : Integer;
begin
  try
  while not Terminated do begin
    Tick := Windows.GetTickCount;
    Inc(SweepCount);
    if Tick > LastSecondStart + 4000 then begin
      LastSecondStart := Tick;
      SweepsPerSecond := SweepCount / 4;
      SweepCount := 0;
    end;

    Lock.Enter('EX');
    try
      if SysObj.FaultInterface.FaultLevel >= FaultStopCycle then
        if ncsInCycle in NCStates then
          if not (ncsFeedHold in NCStates) then
            FeedHold := True;

      UpdateHardware;

      if ResetRewindRequest then begin
        ResetRewind;
        ResetRewindRequest := False;
      end;

      if PartProgram.Loaded and ((FCNCModes <= ValidCycleStartMode) or MacroExecute) then
         ExecutePartProgram;
    finally
      Lock.Leave('EX');
    end;

    if Assigned(FSweepCapture) then
      SweepCapture(Self);
    Sleep(ActiveConfig.CoordinateSetup[0].ThreadKindness);
  end;

  except
    on E: Exception do
      CoreCNC.EventLog('Leaving NC Thread: ' + e.Message);
  end;
end;

procedure TNC32.CNCReset(aTag : TAbstractTag);
begin
  inherited;
  NCHAL.Reset;
end;

procedure TNC32.CNCResetRW(aTag : TAbstractTag);
begin
  inherited;
  if aTag.AsBoolean then begin
    InchDefaultTag.AsBoolean := NCHAL.ActiveConfig.CoordinateSetup[0].InchDefault;

    FIXUP_InchModeDefault := NCHAL.ActiveConfig.CoordinateSetup[0].InchDefault;

    InternalResetRewind;
  end;
end;

procedure TNCHAL.RunAssign;
var TTag, STag : TAbstractTag;
begin
  TTag := TAbstractTag(SPP.InstructionList[PPBGInst + 1]);
  STag := TAbstractTag(SPP.InstructionList[PPBGInst + 2]);
  TTag.Assign(STag);
end;

procedure TNCHAL.RunIF(var NextBGInst : Integer);
var BTag : TAbstractTag;
begin
  BTag := TAbstractTag(SPP.InstructionList[PPBGInst + 1]);
  if not BTag.AsBoolean then
    NextBGInst := Integer(SPP.InstructionList[PPBGInst + 2]);
end;

procedure TNCHAL.RunIndexVar;
var ITag : TAbstractTag;
begin
  ITag := TAbstractTag(SPP.InstructionList[PPBGInst + 1]);
  ITag.AsInteger := ITag.AsInteger + 1;
end;

procedure TNCHAL.RunICompare(var NextBGInst : Integer);
var C1, C2 : TAbstractTag;
begin
  // C1, C1, C1 <= C2 Target, C1 > C2 Target
  C1 := TAbstractTag(SPP.InstructionList[PPBGInst + 1]);
  C2 := TAbstractTag(SPP.InstructionList[PPBGInst + 2]);
  if C1.AsInteger <= C2.AsInteger then
    NextBGInst := Integer(SPP.InstructionList[PPBGInst + 3])
  else
    NextBGInst := Integer(SPP.InstructionList[PPBGInst + 4]);
end;

procedure TNCHAL.RunHash(var NextBGInst : Integer);
var aInstr : TNCInstructionRecord;
    aTag : TAbstractTag;
    Hash : Integer;
    Tmp : Integer;
    I : Integer;
begin
  aTag := TAbstractTag(SPP.InstructionList[PPBGInst + 1]);
  Hash := Integer(SPP.InstructionList[PPBGInst + 2]);
  aInstr.Ptr := SPP.InstructionList[Hash];
  Tmp := aTag.AsInteger;

  for I := 0 to aInstr.ParameterCount div 2 do begin
    if Tmp = Integer(SPP.InstructionList[Hash + I * 2 + 1]) then begin
      NextBGInst := Integer(SPP.InstructionList[Hash + I * 2 + 2]);
      Exit;
    end;
  end;
end;

procedure TNCHAL.RunCall(var NextBGInst : Integer);
var Sub : TSubPartProgram;
    P : TSFAParameter;
    I : Integer;
    NextUnit : Integer;
begin
  Stack.Push(PPBGUnit, NextBGInst, PPBGLine);

  NextUnit := Integer(SPP.InstructionList[PPBGInst + 1]);

  if (PPBGUnit >= 0) and (NextUnit < 0) then begin
    DisplayProgramUnit := PPBGUnit;
    DisplayProgramLine := PPBGLine;
  end;

  PPBGUnit := NextUnit;

  Sub := SPP.PartProgram.SubRoutine[PPBGUnit];
  Sub.RunTimeParameters := TSFAParameters(SPP.InstructionList[PPBGInst + 2]);
  I := 3;
  for P := Low(TSFAParameter) to High(TSFAParameter) do begin
    if P in Sub.RunTimeParameters then begin
      TAbstractTag(Sub.VariableList.Objects[Ord(P)]).AsDouble := TAbstractTag(SPP.InstructionList[PPBGInst + I]).AsDouble;
      Inc(I);
    end;
  end;
  NextBGInst := 0;
  SPP := Sub;
end;

procedure TNCHAL.RunScriptCall;
var Method : TScriptMethodTag;
    I : Integer;
    aTag : TAbstractTag;
begin
  Method := TScriptMethodTag(SPP.InstructionList[PPBGInst + 1]);
//  EventLogFmt('Script %s <<', [Method.Name]);
  try
    for I := 0 to Method.Prototype.Count - 1 do begin
      case Method.Prototype[I].ParmType of
        passByReference,
        passAsConst : aTag := TAbstractTag(SPP.InstructionList[PPBGInst + 2 + I]);
      else
        aTag := TGenericTag.Duplicate(TAbstractTag(SPP.InstructionList[PPBGInst + 2 + I]));
      end;
      SPP.PartProgram.ScriptLibrary.CallParameters[I] := aTag;
    end;

    SPP.PartProgram.ScriptLibrary.SetEntryMethod(Method);
    SPP.PartProgram.ScriptLibrary.Run;

    for I := 0 to Method.Prototype.Count - 1 do
      if Method.Prototype[I].ParmType = passByValue then
        SPP.PartProgram.ScriptLibrary.CallParameters[I].Free;
  finally
//    EventLog('Script >>');
  end;
end;

procedure TNCHAL.RunReturn(var NextBGInst : Integer);
begin
  if Stack.Count = 0 then begin
    NextBGInst := 0;
    PPBGInst := 0;
    PPBGUnit := 0;
  end else begin
    PPBGUnit := Stack.TopUnit;
    NextBGInst := Stack.TopInst;
    PPBGLine := Stack.TopLine;
    SPP := PartProgram.SubRoutine[PPBGUnit];
    Stack.Pop;
  end;
end;

procedure TNCHAL.ExecutePartProgram;
begin
end;


procedure TNCHAL.SetPartProgram(aPartProgram : TNC32PartProgramFile);
begin
  FPartProgram := aPartProgram;
end;

function TNCHAL.GetFeedOverrideEnable : Boolean;
begin
  Result := ncsFeedOverrideEnable in NCStates;
end;

procedure TNCHAL.UpdatePersistent(aTag : TAbstractTag);
begin
end;

procedure TNCHAL.SetActiveAxis(aAxis : Integer);
begin
  if not (ncsSingleBlock in NCStates) then
    FActiveAxis := aAxis;
end;

procedure TNCHAL.SetUnits(aUnits : Double);
begin
  FUnits := aUnits;
  CoordSys[0].ChangeUnits(Units);
  CoordSys[1].ChangeUnits(Units);
  CoordSys[2].ChangeUnits(Units);
  CoordSys[3].ChangeUnits(Units);
  CoordSys[4].ChangeUnits(Units);
  CoordSys[5].ChangeUnits(Units);
  G50.ChangeUnits(Units);
  G51.ChangeUnits(Units);
  G52.ChangeUnits(Units);
  G92.ChangeUnits(Units);
  SysVarU.ChangeUnits(Units, False);

  ZeroOffset.ChangeUnits(Units);
  ZeroOffsetAdjust.ChangeUnits(Units);
  SoftLimitPlus.ChangeUnits(Units);
  SoftLimitMinus.ChangeUnits(Units);
  StowA.ChangeUnits(Units);
  StowB.ChangeUnits(Units);
  StowC.ChangeUnits(Units);
end;

procedure TNCHAL.ExtFunction(aFunc : Integer);
begin
end;

procedure TNC32.InstallComponent(Params : TParamStrings);
var NCHALName : string;
    NCHALClass : TNCHALClass;
    I: Integer;
begin
  SeperateOpenLoopMessages:= Params.ReadBool('SeperateOpenLoopMessages', False);

  if CNC32.FakeNCSystem then
    NCHALName := 'TFakeNCHAL'
  else
    NCHALName := Params.ReadString('NCHAL', 'TPmacNC');

  FMachineSpecificData := Params.ReadBool('MachineSpecific', True);
  NCHALClass := NC32.FindNCDeviceClass(NCHALName);
  if NCHALClass = nil then
    raise ECNCInstallFault.CreateFmt('NCHAL Class [%s] not found', [NCHALName]);

  GEOName := Params.ReadString('GEO', '-');

  SafeProgramPath := Params.ReadString('SafeProgramPath', ProfilePath + 'Safe.opp');

  FNCHAL := NCHALClass.Create(Self, Params.ReadInteger('NCHALDevice', 0));
  FConfig := NCHAL.CreateConfig;
  NCHAL.ActiveConfig := FConfig;
  ReadLive(Config, NCConfig);
  NCHAL.Configure;

  FAxisCount := NCHAL.AxisCount;
  FAxisNames := NCHAL.AxisNames;

  inherited InstallComponent(Params);
  NCProgram.CreatePartProgramStrings;
  NCHAL.NormalLookAhead:= Params.ReadInteger('Lookahead', 4);
  
  InchDefaultTag := TagPublish('NC1InchModeDefault', TMethodTag);
  InchDefaultTag.AsBoolean := NCHAL.ActiveConfig.CoordinateSetup[0].InchDefault;

  NCHAL.ActiveNWord := TagPublish('NC1ActiveNWord', TStringTag);
  NCHAL.ActiveNWord.IsVCL := False;

  OperatorDialogueTag:= TagPublish('NC1OperatorDialogue', TMethodTag);

  NCHAL.MExtTag := TagPublish('NC1MExt', TIntegerTag);
  NCHAL.MExtTag.IsVCL := False;

  NCHAL.SetAxisPositionTags(FAxisPosition);
  LAxisPosition := FAxisPosition;

  NCHAL.FHArray := TUnaryDoubleArrayTag(SysObj.Comms.TagPublish('#', TUnaryDoubleArrayTag));
  NCHAL.HArray.SetElementCount(1000);

  NCHAL.FHHArray := TUnaryDoubleArrayTag(SysObj.Comms.TagPublish('##', TUnaryDoubleArrayTag));
  NCHAL.HHArray.SetElementCount(100);
  NCHAL.HHArray.IsPersistent := True;

  for I := 0 to 5 do begin
    NCHAL.CoordSys[I] := TUnaryDoubleArrayTag(SysObj.Comms.TagPublish('##C' + Char(Ord('A') + I), TUnaryDoubleArrayTag));
    NCHAL.CoordSys[I].SetElementCount(100);
    NCHAL.CoordSys[I].IsPersistent := True;
  end;

  // The following are place holders, although they are published here, no ownership
  // is presumed.
  NCHAL.ZeroOffset := TUnaryDoubleArrayTag(SysObj.Comms.TagPublish(ZeroOffsetName, TUnaryDoubleArrayTag));
  NCHAL.ZeroOffset.SetElementCount(100);
  NCHAL.ZeroOffset.IsPersistent := True;

  NCHAL.ZeroOffsetAdjust := TUnaryDoubleArrayTag(SysObj.Comms.TagPublish(ZeroOffsetAdjustName, TUnaryDoubleArrayTag));
  NCHAL.ZeroOffsetAdjust.SetElementCount(100);
  NCHAL.ZeroOffsetAdjust.IsPersistent := True;

  NCHAL.SoftLimitPlus := TUnaryDoubleArrayTag(SysObj.Comms.TagPublish(PlusSoftLimitName, TUnaryDoubleArrayTag));
  NCHAL.SoftLimitPlus.SetElementCount(100);
  NCHAL.SoftLimitPlus.IsPersistent := True;

  NCHAL.SoftLimitMinus := TUnaryDoubleArrayTag(SysObj.Comms.TagPublish(MinusSoftLimitName, TUnaryDoubleArrayTag));
  NCHAL.SoftLimitMinus.SetElementCount(100);
  NCHAL.SoftLimitMinus.IsPersistent := True;

  NCHAL.SoftLimitMinus.NetWorkAlias:= HashlessMinusSoftLimitName;
  NCHAL.SoftLimitPlus.NetWorkAlias:= HashlessPlusSoftLimitName;

  NCHAL.StowA := TUnaryDoubleArrayTag(SysObj.Comms.TagPublish(StowNameA, TUnaryDoubleArrayTag));
  NCHAL.StowA.SetElementCount(100);
  NCHAL.StowA.IsPersistent := True;

  NCHAL.StowB := TUnaryDoubleArrayTag(SysObj.Comms.TagPublish(StowNameB, TUnaryDoubleArrayTag));
  NCHAL.StowB.SetElementCount(100);
  NCHAL.StowB.IsPersistent := True;

  NCHAL.StowC := TUnaryDoubleArrayTag(SysObj.Comms.TagPublish(StowNameC, TUnaryDoubleArrayTag));
  NCHAL.StowC.SetElementCount(100);
  NCHAL.StowC.IsPersistent := True;

  NCHAL.ResultA := TUnaryDoubleArrayTag(SysObj.Comms.TagPublish(ResultName, TUnaryDoubleArrayTag));
  NCHAL.ResultA.SetElementCount(100);

  NCHAL.IHW := TUnaryDoubleArrayTag(SysObj.Comms.TagPublish(IHWName, TUnaryDoubleArrayTag));
  NCHAL.IHW.SetElementCount(10);

  NCHAL.OHW := TUnaryDoubleArrayTag(SysObj.Comms.TagPublish(OHWName, TUnaryDoubleArrayTag));
  NCHAL.OHW.SetElementCount(10);

  NCHAL.HProbe := TTriDoubleArrayTag(SysObj.Comms.TagPublish(HProbeName, TTriDoubleArrayTag));
  NCHAL.HProbe.SetElementCount(300);
  // End of declaring placeholders

  NCHAL.CMMFile.Offsets := TGenericDoubleArrayTag(SysObj.Comms.TagPublish(CMMName, TGenericDoubleArrayTag));
  NCHAL.CMMFile.Offsets.SetN([1000, 6]);
//  NCHAL.CMMFile.Offsets.ClearArray;

  NCHAL.MCode := TagPb[ncpMCode];
  NCHAL.SCode := TagPb[ncpSCode];
  NCHAL.TCode := TagPb[ncpTCode];

  NCHAL.G50 := TUnaryDoubleArrayTag(SysObj.Comms.TagPublish('#CI', TUnaryDoubleArrayTag));
  NCHAL.G50.SetElementCount(100);
  NCHAL.G51 := TUnaryDoubleArrayTag(SysObj.Comms.TagPublish('#CJ', TUnaryDoubleArrayTag));
  NCHAL.G51.SetElementCount(100);
  NCHAL.G52 := TUnaryDoubleArrayTag(SysObj.Comms.TagPublish('#CK', TUnaryDoubleArrayTag));
  NCHAL.G52.SetElementCount(100);

  NCHAL.G92 := TUnaryDoubleArrayTag(TagPublish('#CP', TUnaryDoubleArrayTag));
  NCHAL.G92.SetElementCount(AxisCount);
  NCHAL.G92.IsVCL := False;

  NCHAL.ActiveGGroup := TUnaryIntegerArrayTag(TagPublish(GGroupName, TUnaryIntegerArrayTag));
  NCHAL.ActiveGGroup.SetElementCount(11);

  NCHAL.FSysVarU := TUnaryDoubleArrayTag(SysObj.Comms.TagPublish('SysU', TUnaryDoubleArrayTag));
  NCHAL.SysVarU.SetElementCount(100);
  NCHAL.SysVarU.IsPersistent := True;

  NCHAL.FSysVarA := TUnaryDoubleArrayTag(SysObj.Comms.TagPublish('SysA', TUnaryDoubleArrayTag));
  NCHAL.SysVarA.SetElementCount(100);
  NCHAL.SysVarA.IsPersistent := True;


  NCHAL.RotaryCount := TagPublish('#Rotary', TIntegerTag);
  NCHAL.RotaryCount.IsEvented := False;
  NCHAL.SweepsPerSecond := 1000 / Config.CoordinateSetup[0].ThreadKindness;
  NCHAL.SweepCount := 0;
  NCHAL.LastSecondStart := Windows.GetTickCount;
  NCHAL.PartProgram := PartProgram;

  NCHAL.BlockDelete := False;
  NCHAL.SingleBlock := False;
  NCHAL.OptionalStop := False;
  NCHAL.SetUnits(1); // Power up in mm

  NCHAL.StartPermissiveTag := TagPublish(NameNC1CStartPermissive, TMethodTag);
  NCHAL.StartPermissiveTag.IsVCL := False;

  NCHAL.NormalTermination := TagPublish(NameNC1NormalTermination, TMethodTag);

  NCHAL.SweepsPerSecondTag:= TagPublish('NC1SweepsPerSecond', TDoubleTag);
  NCHAL.SweepsPerSecondTag.IsVCL:= False;

  NCHAL.MinSweepsPerSecondTag:= TagEvent('NC1SweepsPerSecondMin', edgeChange, TDoubleTag, nil);
  NCHAL.MinSweepsPerSecondTag.IsVCL:= False;
  NCHAL.MinSweepsPerSecondTag.AsDouble:= 1000;

  NCHAL.SweepsPerSecondResetTag:= TagEvent('NC1SweepsPerSecondReset', edgeChange, TIntegerTag, nil);
  NCHAL.SweepsPerSecondResetTag.IsVCL:= False;



  ReportTag := TagPublish('NC1RunReport', TStringTag);
  ReportTag.IsVCL := False;

  for I := 0 to Params.ReadInteger('LoggingTagCount', 0) do begin
    LoggingTags.Add(Params.ReadString('LoggingTag' + IntToStr(I), NotConfiguredTagName));
  end;
end;

function TNC32.ResolveVariable(Sender : TObject; const aName : string) : TAbstractTag;
begin
  Result := SysObj.Comms.FindTag(aName);
end;

procedure TNC32.Installed;
var HMod : HModule;
    Tmp : Boolean;
    I : Integer;
begin
  inherited Installed;
  FPartProgram := TNC32PartProgramFile.Create(Self);
  PartProgram.TagOwner := Self;

  MDIPartProgram := TNC32PartProgramFile.Create(Self);
  MDIPartProgram.TagOwner := Self;

  MacroPartProgram := TNC32PartProgramFile.Create(Self);
  MacroPartProgram.TagOwner := Self;

  C3 := TNC32PartProgramFile.Create(Self);
  C3.TagOwner := Self;

  PartProgram.MacroExecutive := C3;
  MDIPartProgram.MacroExecutive := C3;
  MacroPartProgram.MacroExecutive := C3;

  PartProgram.OnResolveVariable := ResolveVariable;
  PartProgram.OnResolveFunction := ResolveFunction;
  MDIPartProgram.OnResolveVariable := ResolveVariable;
  MDIPartProgram.OnResolveFunction := ResolveFunction;
  MacroPartProgram.OnResolveVariable := ResolveVariable;
  MacroPartProgram.OnResolveFunction := ResolveFunction;
  C3.OnResolveVariable := ResolveVariable;
  C3.OnResolveFunction := ResolveFunction;
  
  SysObj.FaultInterface.AddResetRWComplete(Self);
  NCHAL.MCodeAFC := TagEv[ncdeMCodeAFC];
  NCHAL.TraceIntoC3 := TagEvent(Name + 'TraceIntoC3', EdgeChange, TMethodTag, nil);
  TagEvent(NameDisplaySweep, EdgeChange, TMethodTag, UpdateClients);
  TagEvent(Name + 'NWordSearch', edgeFalling, TMethodTag, NWordSearch);
  TagEvent(Name + 'Config', edgeFalling, TMethodTag, NCConfigure);
  TagEvent(Name + 'ReloadC3', edgeFalling, TMethodTag, NCReloadC3);

  // To generate the first event in the system set these tags to an invalid condition
  TagPB[ncpAxisHomed].AsInteger := -1;
  TagPB[ncpAxisFatalFollowError].AsInteger := -1;
  TagPB[ncpAxisOpenLoop].AsInteger := -1;
  TagPB[ncpAxisPositiveLimit].AsInteger := -1;
  TagPB[ncpAxisNegativeLimit].AsInteger := -1;
  TagPB[ncpAxisAmplifierFault].AsInteger := -1;

  TagEvent(ZeroOffsetName, edgeChange, TUnaryDoubleArrayTag, NCHAL.UpdatePersistent);
  TagEvent(PlusSoftLimitName, edgeChange, TUnaryDoubleArrayTag, NCHAL.UpdatePersistent);
  TagEvent(MinusSoftLimitName, edgeChange, TUnaryDoubleArrayTag, NCHAL.UpdatePersistent);
  TagEvent(StowNameA, edgeChange, TUnaryDoubleArrayTag, NCHAL.UpdatePersistent);
  TagEvent(StowNameB, edgeChange, TUnaryDoubleArrayTag, NCHAL.UpdatePersistent);


  TagEvent(NameReqSavePersistent, EdgeFalling, TMethodTag, SavePersistentData);
  NCHAL.ResetCompleteTag := TagEvent('NC1ResetComplete', EdgeChange, TMethodTag, nil);
  NCHAL.ResetCompleteTag.IsVCL := False;

  NCHAL.StowA.Refresh;
  NCHAL.StowB.Refresh;
  NCHAL.StowC.Refresh;

  TagPb[ncpActiveMode].AsInteger := Byte(NCHAL.CNCModes);
//  NCHAL.Resume;
//  NCHAL.Suspend;
  NCHAL.RequestMode := CNCModeManual;
  NCHAL.SetActiveAxis(0);
  NCHAL.SetJogMode([jmContinuous]);

  if not Config.CoordinateSetup[0].InchDefault then
    NCHAL.SetUnits(1)
  else
    NCHAL.SetUnits(25.4);

  Tmp := FMachineSpecificData;
  Self.FMachineSpecificData := False;
  ReadLive(C3, 'C3');
  Self.FMachineSpecificData := Tmp;
  //DoResetRewind; ?????


  if GEOName <> '-' then begin
    HMod := SysObj.Comms.FindPlugin(GEOName);
    try
      GEO := TGEOPlugin.Create(HMod);
      GEO.DataTransfer := NCHAL.HProbe;
      PartProgram.GEO := GEO;
      MDIPartProgram.GEO := GEO;
      MacroPartProgram.GEO := GEO;
      C3.GEO := GEO;
    except
      on E : Exception do
        SysObj.FaultInterface.SetFault(Self, CoreFaults[cfSysFatal], [Format('GEO Instantiation failure "%s"', [E.Message])], nil);
        //raise ECNCInstallFault.CreateFmt('GEO Instantiation failure "%s"', [E.Message]);
    end;

    SysObj.Comms.TagEvent(Name + 'GEOShow', EdgeFalling, TMethodTag, GEOShow);
    SysObj.Comms.TagEvent(Name + 'GEOHide', EdgeFalling, TMethodTag, GEOHide);
  end;

  PartProgram.CMMFile := NCHAL.CMMFile;
  MDIPartProgram.CMMFile := NCHAL.CMMFile;
  MacroPartProgram.CMMFile := NCHAL.CMMFile;
  C3.CMMFile := NCHAL.CMMFile;

  for I := 0 to LoggingTags.Count - 1 do begin
    LoggingTags.Objects[I] := TagEvent(LoggingTags[I], EdgeChange, TStringTag, nil);
  end;
  NCHAL.Resume;
end;

procedure TNC32.GEOShow(aTag : TAbstractTag);
begin
  if Assigned(GEO) then
    GEO.Show;
end;

procedure TNC32.GEOHide(aTag : TAbstractTag);
begin
  if Assigned(GEO) then
    GEO.Hide;
end;

procedure TNC32.CheckInstallation;
begin
  try
    if C3.Count > 0 then begin
      C3.LoadFromStrings;
      C3.Compile;
    end;
  except
    on E : Exception do begin
      SysObj.FaultInterface.SetFault(Self, CoreFaults[cfSysFatal], [Format('C3 Failed to compile [%s]', [E.Message])], nil);
      //raise ECNCInstallFault.CreateFmt('C3 Failed to compile [%s]', [E.Message]);
    end;
  end;
end;

procedure TNC32.NCReloadC3(aTag : TAbstractTag);
var Tmp : Boolean;
begin
  try
    Tmp := FMachineSpecificData;
    try
      try
        Self.FMachineSpecificData := False;
        ReadLive(C3, 'C3');
      except
        on E : Exception do
          MessageLog('Unable to load C3: ' + E.Message);
      end;
    finally
      Self.FMachineSpecificData := Tmp;
//      DoResetRewind;
    end;

    try
      if C3.Count > 0 then begin
        C3.LoadFromStrings;
        C3.Compile;
      end;
    except
      on E : Exception do begin
        raise ECNCInstallFault.CreateFmt('C3 Failed to compile [%s]', [E.Message]);
      end;
    end;

    SysObj.FileManager.LoadActiveFile(SafeProgramPath);
    if ReloadC3FID <> 0 then begin
      SysObj.FaultInterface.FaultResetID(ReloadC3FID);
    end;
    ReloadC3FID := 0;

  except
    on E: Exception do
      ReloadC3FID := SysObj.FaultInterface.SetFault(Self, NCFaults[ncfFailedC3], [E.Message], ResetFailedC3)
  end;
  //DoResetRewind;
  Sysobj.FaultInterface.ResetRewind;
end;

function TNC32.ResetFailedC3(Handle : FHandle) : Boolean;
begin
  Result := ReloadC3FID = 0;
end;

procedure TNC32.NCConfigure(aTag : TAbstractTag);
begin
  if TNC32Configuration.Execute(Config) then begin
    WriteLive(Config, 'NC[0]');
    CoreCNC.SysObj.Comms.RestartACNC;
  end;
end;

procedure TNC32.NWordSearch(aTag : TAbstractTag);
var I : Integer;
    Tmp : string;
begin
  if PartProgram.Loaded and
     (CNCModeAuto in NCHAL.CNCModes) and not
     (ncsInCycle in NCHAL.NCStates) then begin
    I := TNC_NWordSearch.Execute(PartProgram.SubRoutine[0].NWords, Tmp);
    if I <> -1 then begin
      //DoResetRewind;
      NCHAL.PreNWordLabel := Tmp;
      NCHAL.PreNWordIndex := I;
      NCHAL.PreNWordLineNumber:= PartProgram.GetLineNumberFromInstruction(0, I);
      Sysobj.FaultInterface.ResetRewind;
    end;
  end;
end;

function TNC32.ResetFault(aFault : FHandle) : Boolean;
begin
  Result := False;
end;

var RoundRobin : Integer;

procedure TNC32.UpdateClients(aTag : TAbstractTag);
  procedure SetResetFault(Bits : T32Bits; var FID : TFIDMap; Active : Boolean; F : TNCFault);
  var I : Integer;
  begin
    for I := 0 to AxisCount - 1 do begin
      if (I in Bits) = Active then begin
        if FID[I] = 0 then
          FID[I] := SysObj.FaultInterface.SetFault(Self, NCFaults[F], [FAxisNames[I+1]], ResetFault)
      end else begin
        if FID[I] <> 0 then begin
          SysObj.FaultInterface.FaultResetID(FID[I]);
          FID[I] := 0;
        end;
      end;
    end;
  end;

  procedure UpdateGEO;
    function MachinePos(SFA : TSFAParameter) : Double;
    var Tmp : TMotorSetup;
    begin
      Tmp := Config.CoordinateSetup[0].AxisByName[SFA];
      if Assigned(Tmp) then
        //Result := Tmp.AxisPositionTag.FValue[ptMachine]
        Result := Tmp.AxisPositionTag.FValue[ptDisplay]
      else
        Result := 0;
    end;

  var X, Y, Z, A, B, C: Double;
  begin
    X:= MachinePos(nfParamX);
    Y:= MachinePos(nfParamY);
    Z:= MachinePos(nfParamZ);
    A:= MachinePos(nfParamA);
    B:= MachinePos(nfParamB);
    C:= MachinePos(nfParamC);
    //EventLogFmt('%.3f,%.3f,%.3f,%.3f,%.3f,%.3f', [X,Y,Z,A,B,C]);
    GEO.SetAxisPosition(X, Y, Z, A, B, C );
  end;
begin
  try
    try
      if Assigned(GEO) then
        UpdateGEO;
    except
      on E: Exception do
        EventLog('NC32.UpdateGEO Error: ' + E.Message);
    end;

    NCHAL.VCLUpdate;

    if NCHAL.ErrorString <> '' then begin
      SysObj.FaultInterface.SetFault(Self, CoreFaults[cfSysRewind], [NCHAL.ErrorString], nil);
      NCHAL.ErrorString := '';
    end;

    TagPb[ncpVectorFeedRate].AsDouble := NCHAL.ActualFeedRate;

    if NCHAL.FeedOverride <> FeedOVR then
      TagPb[ncpFeedOVR].AsInteger := Round(NCHAL.FeedOverride);

    if NCHAL.FeedOverrideEnable <> TagPb[ncpFeedOVREna].AsBoolean then
      TagPb[ncpFeedOVREna].AsBoolean := NCHAL.FeedOverrideEnable;


    // NOTE this line only makes sense because GetProgramLine is overriden.
    // In the ancestor ProgramLine always equals TagPb[ncpProgramLine].AsInteger
    if ProgramLine <> TagPb[ncpProgramLine].AsInteger then
      TagPb[ncpProgramLine].AsInteger := ProgramLine;

    if NCHAL.CNCModes <> CNCMode then
      TagPb[ncpActiveMode].AsInteger := Byte(NCHAL.CNCModes);

    if NCHAL.SingleBlock <> Self.SingleBlock then
      TagPB[ncpSingleBlock].AsBoolean := NCHAL.SingleBlock;

    if NCHAL.BlockDelete <> Self.BlockDelete then
      TagPB[ncpBlockDelete].AsBoolean := NCHAL.BlockDelete;

    if NCHAL.OptionalStop <> Self.OptionalStop then
      TagPB[ncpOptionalStop].AsBoolean := NCHAL.OptionalStop;

    if (ncsInCycle in (NCHAL.NCStates)) <> InCycle then begin
      TagPb[ncpInCycle].AsBoolean := ncsInCycle in (NCHAL.NCStates);
      if NCHAL.RewindComplete  and
        (CncModeAuto in CNCMode) and
        (ncsInCycle in NCHAL.NCStates)then begin
        NCHAL.ProgramStartTime := Now;
        NCHAL.RewindComplete := False;
        NCHAL.Interrupted:= False;
      end;
    end;

    if (ncsFeedhold in NCHAL.NCStates) <> Feedhold then
      TagPb[ncpFeedhold].AsBoolean := ncsFeedhold in NCHAL.NCStates;

    if (ncsInMotion in NCHAL.NCStates) <> InMotion then
      TagPb[ncpInMotion].AsBoolean := ncsInMotion in NCHAL.NCStates;

    if NCHAL.TargetFeedRate <> TagPB[ncpTargetFeedRate].AsDouble then
      TagPB[ncpTargetFeedRate].AsDouble := NCHAL.TargetFeedRate;

    try
    if NCHAL.PartProgram.Loaded and (NCHAL.PPHWUnit < NCHAL.SPP.PartProgram.SubRoutineCount) then begin
        if FLines <> NCHAL.SPP.PartProgram.SubRoutine[NCHAL.PPHWUnit].Strings then begin
          FLines := NCHAL.SPP.PartProgram.SubRoutine[NCHAL.PPHWUnit].Strings;
          TagPB[ncpPartProgram].AsString := NCHAL.SPP.PartProgram.SubRoutine[NCHAL.PPHWUnit].Name;
        end
    end
    else begin
      if TagPB[ncpPartProgram].AsString <> 'NONE' then begin
        TagPB[ncpPartProgram].AsString := 'NONE';
      end;
      FLines := nil;
    end;
    except
      raise;
    end;

    /// ------------------- ABOVE HERE

    if Integer(NCHAL.AxisInMotion) <> TagPB[ncpAxisInMotion].AsInteger then
      TagPB[ncpAxisInMotion].AsInteger := Integer(NCHAL.AxisInMotion);

    if Integer(NCHAL.AxisWarnFollowError) <> TagPB[ncpAxisWarnFollowError].AsInteger then begin
      TagPB[ncpAxisWarnFollowError].AsInteger := Integer(NCHAL.AxisWarnFollowError);
      SetResetFault(NCHAL.AxisWarnFollowError, AxisWarnFollowErrorFID, True, ncfWarnFollowError);
    end;

    if Integer(NCHAL.AxisFatalFollowError) <> TagPB[ncpAxisFatalFollowError].AsInteger then begin
      TagPB[ncpAxisFatalFollowError].AsInteger := Integer(NCHAL.AxisFatalFollowError);
      SetResetFault(NCHAL.AxisFatalFollowError, AxisFatalFollowErrorFID, True, ncfFatalFollowError);
    end;

    if Integer(NCHAL.AxisHomed) <> TagPB[ncpAxisHomed].AsInteger then begin
      TagPB[ncpAxisHomed].AsInteger := Integer(NCHAL.AxisHomed);
      SetResetFault(NCHAL.AxisHomed, AxisHomedFID, False, ncfNotHomed);
    end;

    if Integer(NCHAL.AxisEncoderLoss) <> TagPB[ncpEncoderLoss].AsInteger then begin
      TagPB[ncpEncoderLoss].AsInteger := Integer(NCHAL.AxisEncoderLoss);
      SetResetFault(NCHAL.AxisEncoderLoss, AxisEncoderLossFID, True, ncfEncoderLossDetect);
    end;

    if NCHAL.Homed <> TagPB[ncpHomed].AsBoolean then
      TagPB[ncpHomed].AsBoolean := NCHAL.Homed;

    if SeperateOpenLoopMessages then begin
      if Integer(NCHAL.AxisOpenLoop) <> TagPB[ncpAxisOpenLoop].AsInteger then begin
        TagPB[ncpAxisOpenLoop].AsInteger := Integer(NCHAL.AxisOpenLoop);
        if not Config.PlcManagedOpenLoop then
          SetResetFault(NCHAL.AxisOpenLoop, AxisOpenLoopFID, True, ncfAxisOpenLoop);
      end;
    // The following assumes a single axis on open loop is all axes in open loop
    end else begin
      if Integer(NCHAL.AxisOpenLoop) <> TagPB[ncpAxisOpenLoop].AsInteger then begin
        TagPB[ncpAxisOpenLoop].AsInteger := Integer(NCHAL.AxisOpenLoop);
        if not Config.PlcManagedOpenLoop then begin
          if Integer(NCHAL.AxisOpenLoop) <> 0 then begin
            if AxisOpenLoopFID[0] = 0 then
              AxisOpenLoopFID[0] := SysObj.FaultInterface.SetFault(Self, NCFaults[ncfAxesOpenLoop], [], ResetFault)
          end else begin
            if AxisOpenLoopFID[0] <> 0 then begin
              SysObj.FaultInterface.FaultResetID(AxisOpenLoopFID[0]);
              AxisOpenLoopFID[0] := 0;
            end;
          end;
        end;
      end;
    end;

    if Integer(NCHAL.AxisPositiveLimit) <> TagPB[ncpAxisPositiveLimit].AsInteger then begin
      TagPB[ncpAxisPositiveLimit].AsInteger := Integer(NCHAL.AxisPositiveLimit);
      if not Config.PlcManagedLimits then
        SetResetFault(NCHAL.AxisPositiveLimit, AxisPositiveLimitFID, True, ncfPositiveLimit);
    end;

    if Integer(NCHAL.AxisNegativeLimit) <> TagPB[ncpAxisNegativeLimit].AsInteger then begin
      TagPB[ncpAxisNegativeLimit].AsInteger := Integer(NCHAL.AxisNegativeLimit);
      if not Config.PlcManagedLimits then
        SetResetFault(NCHAL.AxisNegativeLimit, AxisNegativeLimitFID, True, ncfNegativeLimit);
    end;

    if Integer(NCHAL.AxisInPosition) <> TagPB[ncpAxisInPosition].AsInteger then
      TagPB[ncpAxisInPosition].AsInteger := Integer(NCHAL.AxisInPosition);

    if Integer(NCHAL.AxisAmplifierFault) <> TagPB[ncpAxisAmplifierFault].AsInteger then begin
      TagPB[ncpAxisAmplifierFault].AsInteger := Integer(NCHAL.AxisAmplifierFault);
      SetResetFault(NCHAL.AxisAmplifierFault, AxisAmplifierFaultFID, True, ncfAmplifierFault);
    end;

    if NCHAL.JogIncrement <> TagPB[ncpJogIncrement].AsInteger then begin
      TagPB[ncpJogIncrement].AsInteger := NCHAL.JogIncrement;
    end;

    if Byte(NCHAL.JogMode) <> TagPB[ncpJogMode].AsInteger then begin
      TagPB[ncpJogMode].AsInteger := Byte(NCHAL.JogMode);
    end;

    if NCHAL.DryRun <> TagPB[ncpDryRun].AsBoolean then
      TagPB[ncpDryRun].AsBoolean := NCHAL.DryRun;

    if NCHAL.RewindComplete and not WasResetRewind then begin
      if (CNCModeAuto in NCHAL.CNCModes) and PartProgram.Loaded then
        TagPb[ncpPartProgram].AsString := PartProgram.SubRoutine[0].Name
      else if (CNCModeMDI in NCHAL.CNCModes) and MDIPartProgram.Loaded then
        TagPb[ncpPartProgram].AsString := MDIPartProgram.SubRoutine[0].Name
      else if MacroPartProgram.Loaded then
        TagPb[ncpPartProgram].AsString := MacroPartProgram.SubRoutine[0].Name;

    end;
    WasResetRewind:= NCHAL.RewindComplete;

    SysObj.Comms.Write(TAxisPositionTag(LAxisPosition[RoundRobin]), EdgeChange);
    Inc(RoundRobin);
    RoundRobin := RoundRobin mod AxisCount;
  except
    on E: Exception do
      EventLog('NC32.VCLUpdate Error: ' + E.Message);
  end;
end;

procedure TNC32.SavePersistentData(aTag : TAbstractTag);
var OldUnits : Double;
var Reg : TCNCRegistry;
    I : Integer;
begin
  //if CNCModeManual in NCHAL.CNCModes then begin
    OldUnits := NCHAL.Units;
    NCHAL.SetUnits(1);
    Reg := TCNCRegistry.Create;
    try
      Reg.OpenKey(RegistryBase + '\Persistent', True);

      for I := 0 to SysObj.Comms.NamedLayer.Count - 1 do begin
        if SysObj.Comms.NamedLayer.Tag[I].IsPersistent then
          SysObj.Comms.NamedLayer.Tag[I].WriteToRegistry(Reg);
      end;
      //Sysobj.FaultInterface.SetFault(Self, CoreFaults[cfPersistentSaved], ['Registry'], nil);

      Reg.Mode64:= False;
      if not Reg.SaveBranch(RegistryBase, MachSpecPath + 'ACNC32.reg') then begin
        SysObj.FaultInterface.SetFault(Self, CoreFaults[cfSysInterlock], ['Failed file write'], nil);
      end;
      Reg.Mode64:= True;
      if not Reg.SaveBranch(RegistryBase, MachSpecPath + 'ACNC64.reg') then begin
        SysObj.FaultInterface.SetFault(Self, CoreFaults[cfSysInterlock], ['Failed file write'], nil);
      end;
      Reg.Mode64:= False;
      //Sysobj.FaultInterface.SetFault(Self, CoreFaults[cfPersistentSaved], [MachSpecPath + 'ACNC32.reg'], nil);

    finally
      Reg.Free;
      NCHAL.SetUnits(OldUnits);
    end;
  //end else begin
  //  SysObj.FaultInterface.SetFault(Self, CoreFaults[cfSysInterlock], [CannotSavePersistentData], nil);
  //end;
end;


procedure TNC32.SetPartProgram(aTag : TAbstractTag);
var C4: TStringList;

{
  procedure AppendIncludes;
  var I: Integer;
      S: TStringList;
  begin
    for I:= 0 to PartProgram.IncludeCount - 1 do begin
      S:= TStringList.Create;
      try
        try
          S.LoadFromFile(CNC32.LocalPath + 'Include\' + PartProgram.Include[I]);
          PartProgram.AddStrings(S);
        except
          raise EGCodeRead.Create('Unable to include file: ' + PartProgram.Include[I]);
        end;
      finally
        S.Free;
      end;
    end;
  end;
 }

begin
  if ActiveFault <> 0 then begin
    SysObj.FaultInterface.FaultResetID(ActiveFault);
    ActiveFault := 0;
  end;

  NCHAL.Lock.Enter('PP');
  try
    try
      SysObj.FileManager.SectionRead(NC32GCodeSection, PartProgram);
    except
      On E : ECompoundFileError do Application.ShowException(E);
    end;

    try
      C4:= TStringList.Create;
      try
        try
          C4.LoadFromFile(CNC32.ContractSpecificPath + 'Live/C4.cfg');
          PartProgram.AddStrings(C4);
        except
          EventLog('Failed C4 Load');
        end;
      finally
        C4.Free;
      end;
      //AppendIncludes;
      PartProgram.LoadFromStrings;
    except
      on E : EGCodeRead do begin
        FLines := nil;
        ActiveFault := SysObj.FaultInterface.SetFault(Self, CoreFaults[cfSysStopCycle], [E.Message], nil);
        Exit;
      end;
    end;

    try
      PartProgram.Compile;
    except
      on E : EGCodeCompile do begin
        FLines := nil;
        ActiveFault := SysObj.FaultInterface.SetFault(Self, CoreFaults[cfSysStopCycle], [E.Message], ResetProgramLoadError);
        Exit;
      end;
    end;

    NCHAL.PartProgram := PartProgram;
    NCHAL.SPP := PartProgram.SubRoutine[0];
    FLines := PartProgram.SubRoutine[0].Strings;
  finally
    NCHAL.Lock.Leave('PP');
  end;
  TagPb[ncpPartProgram].AsString := PartProgram.SubRoutine[0].Name;
  //DoResetRewind;
  Sysobj.FaultInterface.ResetRewind;
  ActiveFault := 0;
end;

procedure TNC32.SetMDIBuffer(aMDI : string);
begin
  MDIPartProgram.Text := aMDI;
  MDIPartProgram.Insert(0, '%MDI');
  MDIPartProgram.Add('M30');
  if CNCMode = [CNCModeMDI] then begin
    try
      MDIPartProgram.LoadFromStrings;
      MDIPartProgram.Compile;
    except
      on E : EGCodeCompile do begin
        SysObj.FaultInterface.SetFault(Self, CoreFaults[cfSysRewind], [E.Message], ResetMDIProgramLoadError);
        MDIPartProgram.Text:= '';
        Exit;
      end;
    end;
    Sysobj.FaultInterface.ResetRewind;
    //DoResetRewind;
  end;
end;

function TNC32.GetMDIBuffer: string;
var List: TStringList;
begin
  List:= TStringList.Create;
  try
    List.Text:= MDIPartProgram.Text;
    if List.Count > 2 then begin
      List.Delete(0);
      List.Delete(List.Count - 1);
    end
    else
      List.Text:= '';
    Result:= List.Text;
  finally
    List.Free;
  end;
end;

function TNC32.ResetRewindComplete: Boolean;
begin
  Result:= NCHAL.ResetRewindComplete;
end;

procedure TNC32.ExecuteMacro(Filename: string);
var S: TStringList;
begin
  if not NCHAL.NoCycle and NCHal.StartPermissiveTag.AsBoolean then begin
    if Homed then  begin
      S:= TStringList.Create;
      try
        S.LoadFromFile(Filename);
        MacroPartProgram.Text:= S.Text;
        MacroPartProgram.Add('M30');
        if CNCMode = [CNCModeManual] then begin
          try
            MacroPartProgram.LoadFromStrings;
            MacroPartProgram.Compile;
            Sysobj.FaultInterface.ResetRewind;
            //DoResetRewind;
            NCHAL.ExecuteMacroOnResetComplete:= True;
          except
            on E: EGCodeCompile do begin
              SysObj.FaultInterface.SetFault(Self, CoreFaults[cfSysRewind], [E.Message], ResetMacroProgramLoadError);
              Exit;
            end;
          end;
        end;
      finally
        S.Free;
      end;
    end;
  end else begin
    Sysobj.FaultInterface.SetFault(Sysobj.NC, NCFaults[ncfCycleNotPermitted], [], nil)
  end;
end;

procedure TNC32.OperatorDialogue(const Breed, Item : string);
  procedure StopProgram;
  begin
    NCHAL.CycleStop := True;
//    Sleep(100);
    NCHAL.CycleStop := False;
//    Sleep(100);
  end;
begin
  OperatorDialogueActive := True;
  try
    try
      case SysObj.Comms.PluginOperatorDialogue(Breed, Item) of
        ncdrStop : begin
          StopProgram;
        end;

        ncdrError : begin
          StopProgram;
          SysObj.FaultInterface.SetFault(Self, NCFaults[ncfFailedOperatorDialogue], ['Not found or internal error'], nil)
        end;
      end;
    except
      on E: Exception do begin
        StopProgram;
        SysObj.FaultInterface.SetFault(Self, NCFaults[ncfFailedOperatorDialogue], [E.Message], nil)
      end;
    end;
  finally
    OperatorDialogueActive := False;
  end;
end;

procedure TNC32.InternalResetRewind;
begin
  NCHAL.ResetRewindRequest := True;
  NCHAL.StartPermissiveTag.AsBoolean := False;
  //EventLog('--ResetRewind Start');

  if OperatorDialogueActive then begin
    SysObj.Comms.PluginOperatorDialogueClose;
  end;
end;

function TNC32.GetAxisType(Index : integer) : TAxisType;
begin
  Result := NCHAL.AxisType[Index];
end;

function TNC32.GetActiveSubRoutine: Integer;
begin
  if NCHAL.TraceIntoC3.AsBoolean or SingleBlock or not InCycle  or (NCHAL.PPHWUnit >= 0) then
    Result := NCHAL.PPHWUnit
  else
    Result := NCHAL.DisplayProgramUnit;

//  Result := NCHAL.PPHWUnit;
end;

function TNC32.GetSubCount: Integer;
begin
  Result := NCHAL.PartProgram.SubRoutineCount;
end;

function TNC32.GetProgramLine : Integer;
begin
  if NCHAL.TraceIntoC3.AsBoolean or SingleBlock or not InCycle or (NCHAL.PPHWUnit >= 0) then
    Result := NCHAL.ProgramLine
  else
    Result := NCHAL.DisplayProgramLine;
//  Result := NCHAL.ProgramLine
end;

procedure TNC32.CreateReportString;
var Tmp: string;
    I: Integer;
begin
  SysObj.FileManager.ActiveFileName;
  Tmp := Format('%s,%s,%s,%d', [   StorageDate(NCHAL.ProgramStartTime),
                                   StorageDate(Now),
                                   CNCTypes.CSVField( ExtractFileName(SysObj.FileManager.ActiveFileName) ),
                                   Ord(not NCHAL.NormalTermination.AsBoolean)]);

  for I := 0 to LoggingTags.Count - 1 do begin
    Tmp := Tmp + ',' + CNCTypes.CSVField( TAbstractTag(LoggingTags.Objects[I]).AsString );
  end;

  ReportTag.AsString := Tmp;
  NCHAL.ProgramStartTime := 0;
end;

function TNC32.GetSubRoutineLines(Index : Integer) : TStringList;
begin
  if NCHAL.PartProgram.Loaded then begin
    try
      Result := NCHAL.PartProgram.SubRoutine[Index].Strings;
    except
      Result := nil;
    end;
  end else
    Result := nil;
end;


function TNC32.GetSubRoutine(Index: Integer): WideString;
var Sub : TSubPartProgram;
begin
  try
    Sub := NCHAL.PartProgram.SubRoutine[Index];
    Result := Sub.Strings.Text;
  except
    Result := 'OUT OF BOUNDS';
  end;

{  if (Index >= 0) and (Index < NCHAL.PartProgram.SubRoutineCount)then
    Result := NCHAL.SPP.PartProgram.SubRoutine[Index].Text
  else
    Result := 'OUT OF BOUNDS'; }
end;


procedure TNC32.ClientDualEvent(cevent : TNCEvent; aTag : TAbstractTag);
  procedure SetCNCMode(aMode : TCNCMode);
  begin
    if FModeChangeLock = 0 then
      if NCHAL.RequestMode <> aMode then
        NCHAL.RequestMode := aMode;
  end;

begin
  case cevent of
    ncdeReqFeedHold   : NCHAL.FeedHold := aTag.AsBoolean;
    ncdeReqCycleStart : if aTag.AsBoolean then begin
                          if not PartProgram.Loaded and (CNCMode = [CNCModeAuto]) then
                            SysObj.FaultInterface.SetFault(Self, NCFaults[ncfNoProgramLoaded], [], nil)
                          else if not NCHAL.Homed then
                            SysObj.FaultInterface.SetFault(Self, NCFaults[ncfNotHomedInterlock], [], nil)
                          else if not MDIPartProgram.Loaded and (CNCMode = [CNCModeMDI]) then
                            SysObj.FaultInterface.SetFault(Self, NCFaults[ncfNoProgramLoaded], [], nil)
                          else if not (CNCMode <= ValidCycleStartMode) then
                            SysObj.FaultInterface.SetFault(Self, NCFaults[ncfIncorrectModeForCycleStart], [], nil)
                          else if SysObj.FaultInterface.FaultLevel > faultWarning then
                            SysObj.FaultInterface.SetFault(Self, NCFaults[ncfFaultTooHigh], [], nil)
                          else if FCycleStartLock <> 0 then
                            SysObj.FaultInterface.SetFault(Self, NCFaults[ncfInterlockIsActive], [], nil)
                          else if NCHAL.NoCycle then
                            Sysobj.FaultInterface.SetFault(Sysobj.NC, NCFaults[ncfCycleNotPermitted], [], nil)
                          else
                            NCHAL.CycleStart := True;
//                          EventLog('Cycle start requested');
                        end else
                          NCHAL.CycleStart := False;
    ncdeReqCycleStop  : NCHAL.CycleStop := aTag.AsBoolean;
    ncdeReqCycleAbort :  begin
                      if ATag.AsBoolean and (NCHAL.ProgramLine <> 0) and (ncsInCycle in NCHAL.NCStates) then
                        Sysobj.FaultInterface.SetFault(Self, NCFaults[ncfProgramAborted], [], nil);

                      NCHAL.CycleAbort := aTag.AsBoolean;
    end;
    ncdeReqJogPlus    : if CNCMode <= ValidJogMode then
                          NCHAL.JogPlus[ActiveAxis] := aTag.AsBoolean;
    ncdeReqJogMinus   : if CNCMode <= ValidJogMode then
                          NCHAL.JogMinus[ActiveAxis] := aTag.AsBoolean;
    ncdeReqAxisSelect : begin
                           NCHAL.ActiveAxis := aTag.AsInteger mod AxisCount;
                           TagPb[ncpActiveAxis].AsInteger := NCHAL.ActiveAxis;
    end;

    ncdeReqFeedOVR    : NCHAL.FeedOverride := aTag.AsDouble;
    ncdeTestPoint     : begin
        NCHAL.TestPoint;
    end;
    ncdeReqManualMode : SetCNCMode(CNCModeManual);
    ncdeReqAutoMode   : SetCNCMode(CNCModeAuto);
    ncdeReqMDIMode    : SetCNCMode(CNCModeMDI);
    ncdeReqEditMode   : SetCNCMode(CNCModeEdit);
    ncdeReqTeachMode  : SetCNCMode(CNCModeTeachIn);
    ncdeReqAxisIndex  : begin
                          NCHAL.ActiveAxis := (TagPb[ncpActiveAxis].AsInteger + 1) mod AxisCount;
                          TagPb[ncpActiveAxis].AsInteger := NCHAL.ActiveAxis;
    end;
    ncdeReqSingleBlock  : NCHAL.SingleBlock := aTag.AsBoolean;
    ncdeReqBlockDelete  : NCHAL.BlockDelete := not NCHAL.BlockDelete;
    ncdeReqOptionalStop : NCHAL.OptionalStop := not NCHAL.OptionalStop;
    ncdeReqJogModeCont  : NCHAL.JogMode := [jmContinuous];
    ncdeReqJogModeInc   : NCHAL.JogMode := [jmIncremental];
    ncdeReqJogModeHome  : NCHAL.JogMode := [jmHome];
    ncdeReqJogModeMPG   : NCHAL.JogMode := [jmMPG];
    ncdeReqFeedOVRENA   : NCHAL.FeedOverrideEnable := aTag.AsBoolean;
    ncdeReqJogIncrement : NCHAL.JogIncrement := aTag.AsInteger;
    ncdeReqDryRun       : NCHAL.DryRun := not NCHAL.DryRun;
    ncdeReqExtFunc1..ncdeReqExtFunc10 : begin
      if NCHAL.NoCycle then
        Sysobj.FaultInterface.SetFault(Sysobj.NC, NCFaults[ncfCycleNotPermitted], [], nil)
      else

        NCHAL.ExtFunction(Ord(cevent)- Ord(ncdeReqExtFunc1));
    end;
    ncdeReqZeroAdjustAxis : NCHAL.SetZeroOffsetAxis;
    ncdeReqZeroAdjust : NCHAL.SetZeroOffset;
    ncdeReqHalfZeroAdjustAxis : NCHAL.SetHalfZeroOffset;
    ncdeReqClearZeroAdjustAxis : NCHAL.ClearZeroOffsetAxis;
    ncdeReqClearZeroAdjust : NCHAL.ClearZeroOffset;
    ncdeReqPresetZeroAdjust : NCHAL.PresetZeroOffset;
    ncdeReqCopyPosition: NCHAL.CopyCurrentPosition;
    ncdeMacroExecute: begin
      if (ATag.AsInteger <> 0) and Homed and not InCycle and not(NCHAL.CNCModes <= ValidCycleStartMode)then
        try
          ExecuteMacro(LivePath + 'macro\' + IntToStr(ATag.AsInteger) + '.txt');
        except
          on E: Exception do
            SysObj.FaultInterface.SetFault(Self, CoreFaults[cfSysWarning], [E.Message], nil);
        end;
        TagEv[ncdeMacroExecute].AsInteger:= 0;
    end;
    ncdeReleaseAxis: begin
      NCHAL.SetReleaseAxes(ATag.AsBoolean);
    end;
    ncdeNoCycle: begin
      NCHAL.SetNoCycle(ATag.AsBoolean);
    end;
  end;
end;


procedure RegisterNCDeviceClass(ClassType : TNCHALClass);
begin
  if RegNCList = nil then
    RegNCList := TStringList.Create;

  if RegNCList.IndexOf(ClassType.ClassName) <> -1 then
    raise ECNCInstallFault.CreateFmt(NCClassNameNotUnique, [ClassType.ClassName]);

  RegNCList.AddObject(ClassType.ClassName, Pointer(ClassType))
end;


function FindNCDeviceClass(const aTypeName : string): TNCHALClass;
var I : Integer;
begin
  if Assigned(RegNCList) then begin
    I := RegNCList.IndexOf(aTypeName);
    if I = -1 then
      Result:= nil
    else
      Result:= TNCHALClass(RegNCList.Objects[i]);
  end else
    Result:= nil
end;

procedure TNCProgramStack.Pop;
begin
  if Count > 0 then
    Self.Delete(Count - 1);
  if Count > 0 then
    Self.Delete(Count - 1);
  if Count > 0 then
    Self.Delete(Count - 1);
end;

procedure TNCProgramStack.Push(aUnit, aInst, aLine : Integer);
begin
  Self.Add(Pointer(aUnit));
  Self.Add(Pointer(aInst));
  Self.Add(Pointer(aLine));
end;

function TNCProgramStack.TopUnit : Integer;
begin
  Result := 0;
  if Count > 1 then
    Result := Integer(Items[Count - 3]);
end;

function TNCProgramStack.TopInst : Integer;
begin
  Result := 0;
  if Count > 0 then
    Result := Integer(Items[Count - 2]);
end;

function TNCProgramStack.TopLine : Integer;
begin
  Result := 0;
  if Count > 2 then
    Result := Integer(Items[Count - 1]);
end;


constructor TNC32PositionGather.Create(aOwner : TComponent);
begin
  inherited;
  AxisList := TList.Create;;
end;

destructor TNC32PositionGather.Destroy;
begin
  if Assigned(AxisList) then begin
    while AxisList.Count > 0 do begin
      TObject(AxisList[0]).Free;
      AxisList.Delete(0);
    end;
    AxisList.Free;
  end;
  inherited Destroy;
end;

procedure TNC32PositionGather.InstallComponent(Params : TParamStrings);
var I : Integer;
begin
  inherited InstallComponent(Params);
  if SysObj.NC is TNC32 then
    NC32 := TNC32(SysObj.NC)
  else
    NC32 := nil;

  if Assigned(NC32) then begin
    NC32.NCHAL.SweepCapture := NCPositionCapture;

    for I := 0 to 1 do begin
      AxisList.Add(TList.Create);
    end;
  end;
end;

procedure TNC32PositionGather.NCPositionCapture(Sender : TObject);
var I : Integer;
begin
  if NC32.InCycle then begin
    BlockingThread := True;
    for I := 0 to 1 do begin
      with TList(AxisList[I]) do begin
        Add(Pointer(Round(TAxisPositionTag(NC32.LAxisPosition[I]).FValue[ptMachine] * 100)));
        while Count > 8000 do begin
          Delete(0);
          Dec(StartUpdate);
        end;
        if StartUpdate < 0 then
          StartUpdate := 0;
      end;
    end;
    BlockingThread := False;
  end;
end;

procedure TNC32PositionGather.Paint;
begin
  inherited;
  StartUpdate := 0;
end;

procedure TNC32PositionGather.Installed;
begin
  inherited;
  if Assigned(NC32) then begin
    Scale := 10;
    TagEvent(NameDisplaySweep, edgeChange, TMethodTag, UpdateDisplay);
    TagEvent(Name + 'Clear', EdgeFalling, TMethodTag, ClearHistory);
    TagEvent(Name + 'Scale', EdgeChange, TIntegerTag, SetScale);
    TagEvent(Name + 'XOrigin', EdgeChange, TIntegerTag, SetXOrigin);
    TagEvent(Name + 'YOrigin', EdgeChange, TIntegerTag, SetYOrigin);
  end;
end;

procedure TNC32PositionGather.SetScale(aTag : TAbstractTag);
begin
  Scale := aTag.AsDouble;
  Refresh;
end;

procedure TNC32PositionGather.SetXOrigin(aTag : TAbstractTag);
begin
  XOrigin := aTag.AsDouble;
  Refresh;
end;

procedure TNC32PositionGather.SetYOrigin(aTag : TAbstractTag);
begin
  YOrigin := aTag.AsDouble;
  Refresh;
end;

procedure TNC32PositionGather.ClearHistory(aTag : TAbstractTag);
begin
  TList(AxisList[0]).Clear;
  TList(AxisList[1]).Clear;
  StartUpdate := 0;
  Self.Refresh;
end;

procedure TNC32PositionGather.UpdateDisplay(aTag : TAbstractTag);
  function XPos(Index : Integer) : Double;
  begin
    Result := Integer(TList(AxisList[0]).Items[Index]);
    Result := ((Result / 100) -  XOrigin) * Scale;
    Result := Result + (Width div 2);
  end;

  function YPos(Index : Integer) : Double;
  begin
    Result := Integer(TList(AxisList[1]).Items[Index]);
    Result := ((Result / 100) - YOrigin) * Scale;
    Result := Height - (Result + (Height div 2));
  end;

var I : Integer;
begin
  if not BlockingThread then begin
    if Visible then begin
      Canvas.Pen.Color := clBlack;
      if StartUpdate < TList(AxisList[0]).Count then
        Canvas.MoveTo(Round(XPos(StartUpdate)), Round(YPos(StartUpdate)));


      for I := StartUpdate to TList(AxisList[0]).Count - 1 do begin
        Canvas.LineTo(Round(XPos(I)), Round(YPos(I)));
      end;

      StartUpdate := TList(AxisList[0]).Count - 1;
      if StartUpdate < 0 then
        Startupdate := 0;
    end;
  end;
end;


procedure TNC32.TraceClear;
begin
  NCHAL.TraceClear;
end;

procedure TNC32.TraceOn;
begin
  NCHAL.TraceClear;
  NCHAL.TraceStart;
end;

procedure TNC32.TraceResume;
begin
  NCHAL.TraceStart;
end;

procedure TNC32.TraceSuspend;
begin
  NCHAL.TraceStop;
end;

procedure TNCHAL.TraceClear;
var I: Integer;
begin
  inherited;
  RetraceEnable:= False;
  try
    for I:= 0 to RetraceList.Count - 1 do begin
      TObject(RetraceList[I]).Free;
    end;
  finally
    RetraceList.Clear;
  end;
end;

function TNCHAL.RetraceStepFromCurrent: TRetraceStep;
  function MachinePos(SFA : TSFAParameter) : Double;
  var Tmp : TMotorSetup;
  begin
    Tmp := ActiveConfig.CoordinateSetup[0].AxisByName[SFA];
    if Assigned(Tmp) then
      Result := Tmp.AxisPositionTag.FValue[ptMachine]
    else
      Result := 0;
  end;
begin
  Result:= TRetraceStep.Create;
  Result.Position[nfParamX]:= MachinePos(nfParamX);
  Result.Position[nfParamY]:= MachinePos(nfParamY);
  Result.Position[nfParamZ]:= MachinePos(nfParamZ);
  Result.Position[nfParamA]:= MachinePos(nfParamA);
  Result.Position[nfParamB]:= MachinePos(nfParamB);
  Result.Position[nfParamC]:= MachinePos(nfParamC);
  Result.Position[nfParamU]:= MachinePos(nfParamU);
  Result.Position[nfParamV]:= MachinePos(nfParamV);
  Result.Position[nfParamW]:= MachinePos(nfParamW);
end;

procedure TNCHAL.TraceStart;
begin
  inherited;
  RetraceList.Add(RetraceStepFromCurrent);
  RetraceEnable:= True;
end;

procedure TNCHAL.TraceStop;
begin
  inherited;
  RetraceEnable:= False;
end;


procedure TNC32.MDTReadRow(SA: IACNC32StringArray; Prefix: string; Row: Integer);
var I: Integer;
    N: WideString;
    S: string;
    Tag: TAbstractTag;
begin
  if Row >= SA.RecordCount then
    raise Exception.Create('MDTReadRow: Attempting to read beyond end of table');
  for I:= 0 to SA.FieldCount - 1 do begin
    SA.FieldNameAtIndex(I, N);
    S:= Prefix + N;
    Tag:= NCHAL.FPartProgram.ResolveVariable(Self, S);
    if not Assigned(Tag) then
      raise Exception.Create('MDTReadRow Variable cannot be resolved: ' + S);

    SA.GetValue(Row, I, N);
    Tag.AsString:= N;
  end;
end;

procedure TNC32.MDTWriteRow(SA: IACNC32StringArray; Prefix: string; Row: Integer);
var I: Integer;
    N: WideString;
    S: string;
    Tag: TAbstractTag;
begin
  if Row >= SA.RecordCount then
    raise Exception.Create('MDTReadRow: Attempting to write beyond end of table');
  for I:= 0 to SA.FieldCount - 1 do begin
    SA.FieldNameAtIndex(I, N);
    S:= Prefix + N;
    Tag:= NCHAL.FPartProgram.ResolveVariable(Self, S);
    if not Assigned(Tag) then
      raise Exception.Create('MDTWriteRow Variable cannot be resolved: ' + S);
    N:= Tag.AsString;
    SA.SetValue(Row, I, N);
  end;
end;

procedure TNCHAL.ClearZeroOffset;
var I: Integer;
begin
  for I := 0 to AxisCount - 1 do begin
    ZeroOffsetAdjust.AsArrayDouble[I]:= 0;
  end;
  UpdateActiveOffset;
end;

procedure TNCHAL.PresetZeroOffset;
var Form: TZeroOffsetForm;
    I: Integer;
    V, Tmp: Double;
begin
  Form:= TZeroOffsetForm.Create(Application);
  try
    if Form.ShowModal = mrOK then begin
      for I:= 0 to AxisCount - 1 do begin
        if Form.Edits[I].Text <> '' then begin
          try
            V:= StrToFloat(Form.Edits[I].Text);
            Tmp:= NC32.Config.CoordinateSetup[0].AxisByDisplay[I].AxisPositionTag.FValue[ptMachine];
            V:= Tmp - V;
            ZeroOffsetAdjust.AsArrayDouble[I]:= V;
          except
          end;
        end;
      end;
    end;
  finally
    Form.Free;
  end;
  UpdateActiveOffset;
end;

procedure TNCHAL.CopyCurrentPosition;
var Tmp: string;
    I: Integer;
begin
  Tmp:= '';
  for I:= 0 to AxisCount -1 do begin
    Tmp:= Tmp + NC32.FAxisNames[I+1];
    Tmp:= Tmp + Format('%.3f ', [NC32.Config.CoordinateSetup[0].AxisByDisplay[I].AxisPositionTag.FValue[ptDisplay]]);
  end;
  ClipBrd.Clipboard.AsText := Tmp;
end;


function TNCHAL.ResetRewindComplete: Boolean;
begin
  Result:= True;
end;

procedure TNCHAL.ClearZeroOffsetAxis;
begin
  ZeroOffsetAdjust.AsArrayDouble[ActiveAxis]:= 0;
  UpdateActiveOffset;
end;

procedure TNCHAL.SetHalfZeroOffset;
var Tmp, Tmp1: Double;
begin
  Tmp:= ZeroOffsetAdjust.AsArrayDouble[ActiveAxis];
  Tmp1:= NC32.Config.CoordinateSetup[0].AxisByDisplay[FActiveAxis].AxisPositionTag.FValue[ptMachine];
  Tmp := (Tmp + Tmp1) / 2;
  ZeroOffsetAdjust.AsArrayDouble[ActiveAxis]:= Tmp;
  UpdateActiveOffset;
end;

procedure TNCHAL.SetZeroOffset;
var I: Integer;
begin
  for I := 0 to AxisCount - 1 do begin
    ZeroOffsetAdjust.AsArrayDouble[I]:= NC32.Config.CoordinateSetup[0].AxisByDisplay[I].AxisPositionTag.FValue[ptMachine];
  end;
  UpdateActiveOffset;
end;

procedure TNCHAL.SetZeroOffsetAxis;
begin
  ZeroOffsetAdjust.AsArrayDouble[ActiveAxis]:= NC32.Config.CoordinateSetup[0].AxisByDisplay[FActiveAxis].AxisPositionTag.FValue[ptMachine];
  UpdateActiveOffset;
end;


procedure TNCHAL.SetReleaseAxes(Release: Boolean);
begin

end;

procedure TNCHAL.SetNoCycle(NoC: Boolean);
begin
  NoCycle:= NoC;
end;

procedure TNC32.SetOperatorDialogueActive(A: Boolean);
begin
  FOperatorDialogueActive := A;
  OperatorDialogueTag.AsBoolean := A;
end;

initialization
  RegisterCNCClass(TNC32);
  RegisterCNCClass(TNC32PositionGather);
end.

