unit NC32Geometry;

interface

uses Classes, CNCTypes, CNC32;

type
  TAxis = class(TObject)
  private
    function GetChild(Index: Integer): TAxis;
    function GetChildCount: Integer;
  protected
    FZeroPosition: TXYZ;
    FDirection: TXYZ;
    FParent: TAxis;
    Children: TList;
    FName: string;
  public
    constructor Create;
    procedure AddChild(AChild: TAxis);
    property ZeroPosition: TXYZ read FZeroPosition write FZeroPosition;
    property Direction: TXYZ read FDirection write FDirection;
    property Parent: TAxis read FParent write FParent;
    property ChildCount: Integer read GetChildCount;
    property Child[Index: Integer]: TAxis read GetChild;
    property Name: string read FName write FName;
  end;

  TRotaryAxis = class(TObject)
  protected
  end;

  TLinearAxis = class(TObject)
  protected
  end;


// Describe HSD6II
// Machine 0, 0, 0
// A point in space described by the intersection of
// A,B rotaries when they are inline with the Lower erowa
//
// Z Front erowa centre relative to
//

implementation

{ TAxis }

procedure TAxis.AddChild(AChild: TAxis);
begin
  Children.Add(AChild);
end;

constructor TAxis.Create;
begin
  Children:= TList.Create;
end;

function TAxis.GetChild(Index: Integer): TAxis;
begin
  Result:= TAxis(Children[Index]);
end;

function TAxis.GetChildCount: Integer;
begin
  Result:= Children.Count;
end;


end.
