unit NC32ProbeOffset;

interface

uses Classes, SysUtils, CNC32;

type
  TAdjustPlane = (
    apX,
    apY,
    apZ
  );

  TProbeOffset = class(TObject)
  private
    FName : string;
    Adjust : array [TAdjustPlane, Boolean] of Double;
    New : Boolean;
    procedure SetName(const Value: string);
    procedure SetRadius(M : Double);
    procedure GetPrimaryPlane(const P1 : TXYZ; var P : TAdjustPlane; var D : Boolean);
//    function NearZero(Value : Double) : Boolean;
  public
    constructor Create(aName : string);
    function GetProbeRadius(const P1 : TXYZ) : Double;
    procedure AddProbeOffset(R : Double; const P1 : TXYZ);
    procedure Clear;
    property Name : string read FName write SetName;
  end;

  TProbeOffsetList = class(TObject)
  private
    List : TStringList;
    procedure Clean;
  public
    constructor Create;
    destructor Destroy; override;
    function NewProbeOffset(aName : string) : TProbeOffset;
    function FindProbeOffset(aName : string) : TProbeOffset;
  end;

implementation

{ TProbeOffset }

procedure TProbeOffset.SetRadius(M : Double);
var A : TAdjustPlane;
    I : Boolean;
begin
  for A := Low(TAdjustPlane) to High(TAdjustPlane) do
    for I := False to True do
       Adjust[A, I] := M;
end;

procedure TProbeOffset.AddProbeOffset(R: Double; const P1: TXYZ);
var //M : Double;
    P : TAdjustPlane;
    D : Boolean;
begin
//  M := (P1.X*P1.X)+(P1.Y*P1.Y)+(P1.Z*P1.Z);
  GetPrimaryPlane(P1, P, D);
  Adjust[P, D] := R;
  if New then begin
    SetRadius(R);
    New := False;
  end;
end;

constructor TProbeOffset.Create(aName: string);
begin
  inherited Create;
  FName := aName;
  SetRadius(0.000);
  New := True;
end;

{function TProbeOffset.NearZero(Value : Double) : Boolean;
begin
  Result := Abs(Value) < 0.06;
end; }

function TProbeOffset.GetProbeRadius(const P1: TXYZ): Double;
var P : TAdjustPlane;
    D : Boolean;
begin
  GetPrimaryPlane(P1, P, D);
  Result := Adjust[P, D];
{
  if NearZero(P1.X) and NearZero(P1.Y) then begin
    // Adjustment is only Z
    Result := Adjust[apZ, P1.Z < 0];
    Exit;
  end;

  if NearZero(P1.Y) and NearZero(P1.Z) then begin
    Result := Adjust[apX, P1.X < 0];
    Exit;
  end;

  if NearZero(P1.X) and NearZero(P1.Z) then begin
    Result := Adjust[apY, P1.Y < 0];
    Exit;
  end;

  raise Exception.Create('Probe compensation not incident with a plane'); }
end;

procedure TProbeOffset.SetName(const Value: string);
begin
  FName := Value;
end;

procedure TProbeOffset.Clear;
begin
  SetRadius(0);
  New := True;
end;

procedure TProbeOffset.GetPrimaryPlane(const P1: TXYZ; var P: TAdjustPlane;
  var D: Boolean);
begin
  if (Abs(P1.X) > Abs(P1.Y)) and (Abs(P1.X) > Abs(P1.Z)) then begin
    P := apX;
    D := P1.X < 0;
  end else if (Abs(P1.Y) > Abs(P1.X)) and (Abs(P1.Y) > Abs(P1.Z)) then begin
    P := apY;
    D := P1.Y < 0;
  end else begin
    P := apZ;
    D := P1.Z < 0;
  end;
end;

{ TProbeOffsetList }

procedure TProbeOffsetList.Clean;
begin
  while List.Count > 0 do begin
    List.Objects[0].Free;
    List.Delete(0);
  end;
end;

constructor TProbeOffsetList.Create;
begin
  List := TStringList.Create;
end;

destructor TProbeOffsetList.Destroy;
begin
  Clean;
  List.Free;
  inherited;
end;

function TProbeOffsetList.FindProbeOffset(aName: string): TProbeOffset;
var I : Integer;
begin
  for I := 0 to List.Count - 1 do begin
    if CompareText(List[I], aName) = 0 then begin
      Result := TProbeOffset(List.Objects[I]);
      Exit;
    end;
  end;
  Result := nil;
end;

function TProbeOffsetList.NewProbeOffset(aName: string): TProbeOffset;
begin
  Result := FindProbeOffset(aName);
  if Result <> nil then begin
    Result.Clear;
    Exit;
  end else begin
    Result := TProbeOffset.Create(aName);
    List.AddObject(aName, Result);
  end;
end;

end.
