unit AGEPlugin;

interface

uses Windows, SysUtils, CoreCNC;

type
  TXYZ = record
    X, Y, Z : Double;
  end;

  TAGEPluginCreate = procedure; stdcall;
  TAGEPluginDestroy = procedure; stdcall;
  TAGEPluginReset = procedure; stdcall;
  TAGESetAxisPositions = procedure(X,Y,Z,A,B,C : Double); stdcall;
  TAGEPluginSetProbePoint = procedure(aName : PChar; P1 : TXYZ); stdcall;
  TAGEPluginGetProbeVector = procedure(aName : PChar; var P1, P2 : TXYZ); stdcall;
  TAGEPluginShow = procedure; stdcall;
  TAGEPluginHide = procedure; stdcall;
  TAGEPluginLoadConfig = procedure(aName : PChar); stdcall;

  TAgePlugin = class(TObject)
  private
    AGEPluginCreate : TAGEPluginCreate;
    AGEPluginDestroy : TAGEPluginDestroy;
    AGEPluginSetAxisPositions :TAGESetAxisPOsitions;
    AGEPluginSetProbePoint : TAGEPluginSetProbePoint;
    AGEPluginGetProbeVector : TAGEPluginGetProbeVector;
    AGEPluginShow : TAGEPluginShow;
    AGEPluginHide : TAGEPluginHide;
    AGEPluginLoadConfig : TAGEPluginLoadConfig;
    AGEPluginReset : TAGEPluginReset;
    FDataTransfer : TUnaryDoubleArrayTag;
    function GetProcAddressX(hModule: HMODULE; lpProcName: LPCSTR): FARPROC;
  public
    constructor Create(HLib : HModule);
    destructor Destroy; override;
    procedure SetAxisPosition(X, Y, Z, A, B, C : Double);
    procedure SetProbePoint(aName : string; const P : TXYZ);
    procedure GetProbeVector(aName : string; var P1, P2 : TXYZ);
    procedure LoadConfig(aConfig : string);
    procedure Reset;
    procedure Show;
    procedure Hide;
    property DataTransfer : TUnaryDoubleArrayTag read FDataTransfer write FDataTransfer;
  end;

implementation

function TAgePlugin.GetProcAddressX(hModule: HMODULE; lpProcName: LPCSTR): FARPROC;
begin
  Result := GetProcAddress(hModule, lpProcName);
  if not Assigned(Result) then
    raise Exception.CreateFmt('Axis Geometry engine does not contain method "%s"', [lpProcName]);
end;

constructor TAgePlugin.Create(HLib : HModule);
begin
  AGEPluginCreate := GetProcAddressX(HLib, 'AGEPluginCreate');
  AGEPluginDestroy := GetProcAddressX(HLib, 'AGEPluginCreate');
  AGEPluginSetAxisPositions := GetProcAddressX(HLib,'AGEPluginSetAxisPositions');
  AGEPluginSetProbePoint := GetProcAddressX(HLib,'AGEPluginSetProbePoint');
  AGEPluginGetProbeVector := GetProcAddressX(HLib,'AGEPluginGetProbeVector');
  AGEPluginHide := GetProcAddressX(HLib, 'AGEPluginHide');
  AGEPluginShow := GetProcAddressX(HLib, 'AGEPluginShow');
  AGEPluginLoadConfig := GetProcAddressX(HLib, 'AGEPluginLoadConfig');
  AGEPluginReset := GetProcAddressX(HLib, 'AGEPluginReset');

  AGEPluginCreate;
end;

destructor TAgePlugin.Destroy;
begin
  if Assigned(AGEPluginDestroy) then
    AGEPluginDestroy;
end;


procedure TAgePlugin.SetAxisPosition(X, Y, Z, A, B, C : Double);
begin
  AGEPluginSetAxisPositions(X, Y, Z, A, B, C);
end;

procedure TAgePlugin.SetProbePoint(aName : string; const P : TXYZ);
begin
  AGEPluginSetProbePoint(PChar(aName), P);
end;

procedure TAgePlugin.GetProbeVector(aName : string; var P1, P2 : TXYZ);
begin
  AGEPluginGetProbeVector(PChar(aName), P1, P2);
end;

procedure TAgePlugin.LoadConfig(aConfig : string);
begin
  AGEPluginLoadConfig(PChar(aConfig));
end;

procedure TAGEPlugin.Reset;
begin
  AGEPluginReset;
end;

{AGEPluginSetNominalStackingAxis(5,5,20,4,9,120,30,50,20);
AGEPluginSetActualStackingAxis(15,15,20,-14,29,130,41,55,20);
AGESetAxisPositions(130,5,-195,10,89,2); }


procedure TAgePlugin.Hide;
begin
  AGEPluginHide;
end;

procedure TAgePlugin.Show;
begin
  AGEPluginShow;
end;

end.
