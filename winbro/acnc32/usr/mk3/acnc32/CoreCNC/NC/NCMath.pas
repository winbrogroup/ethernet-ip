unit NCMath;

interface

uses Classes, CNC32, CNCTypes, CoreCNC, SysUtils, Named;

type
  TMathOperator = (
             moPlus,
             moMinus,
             moProduct,
             moDivide,
             moLessThanOrEqual,
             moGreaterThanOrEqual,
             moNotEqual,
             moEqual,
             moGreaterThan,
             moLessThan,
             moMod,
             moDiv,
             moAnd,
             moOr,
             moXor,
             moShl,
             moShr,
             moBitwiseAnd,
             moBitwiseOr,
             moBitwiseXor,
             moInvalid,
             moEnd
  );

  TMathInstruction = (
    miNone,
    miNegative,
    miPositive,
    miPush,
    miPop,
    miPushNegative,
    miEnd
  );

  TResolveVariable = function (Sender : TObject; const aName : string) : TAbstractTag of Object;
  TResolveFunction = function (Sender : TObject; const Func, Parameters : string) : TAbstractTag of Object;


  TMathRecord = record
    Instruction : TMathInstruction;
    Operand : TAbstractTag;
    Operator : TMathOperator;
  end;

  AMathRecord = array [0..0] of TMathRecord;
  PAMathRecord = ^AMathRecord;
  PMathRecord = ^TMathRecord;

  TNCMathParser = class(TFloatTag)
  private
    OperandList : TList;
    MathList : PAMathRecord;
    Allocated : Integer;
    Count : Integer;
    FResolveVariable : TResolveVariable;
    FResolveFunction : TResolveFunction;
    FResolveArray : TResolveFunction;
    FCompiledLine : string;
    procedure ReadOperand(var aLine : string; var aMath : TMathRecord);
    function ReadOperator(var aLine : string) : TMathOperator;
    procedure Clean;
    procedure CompileChunk(var aChunk : string);
    function CreateTagFromString(aName : string) : TAbstractTag;
    function PrecedenceLevel(aOp : TMathOperator) : Integer;
    function AddRecord(const aMath : TMathRecord) : Integer;
    procedure AddPush;
    procedure AddPop;
    function ExecuteChunk(var Index : Integer) : Double;
    function ReadNextOperator(aLine : string) : TMathOperator;
    function RemoveEnclosingParentheses(var aLine : string; Index : Integer) : string;
    function RemoveEnclosingBraces(var aLine : string; Index : Integer) : string;
  protected
  public
    constructor Create(aOwner : TTagOwner; aName : string); override;
    destructor Destroy; override;
    procedure Compile(aMath : string);
    function Execute : Double;
    function GetAsDouble : Double; override;
    property OnResolveVariable : TResolveVariable read FResolveVariable write FResolveVariable;
    property OnResolveFunction : TResolveFunction read FResolveFunction write FResolveFunction;
    property OnResolveArray : TResolveFunction read FResolveArray write FResolveArray;
    property CompiledLine : string read FCompiledLine;
    function IsSimple: Boolean;
    function ExtractSimpleUnderlyingTag: TAbstractTag;
  end;

  EMathCompileError = class(Exception);
  EMathExecuteError = class(Exception);

  function IsNumeric(C : Char) : Boolean;

implementation

{var
  Stream : TFileStream; }

function IsNumeric(C : Char) : Boolean;
begin
  Result := False;
  case C of
    '0'..'9', '.', '$' : Result := True;
  end;
end;

const
  OperatorSymbols : array[TMathOperator] of PChar = (
            '+',      //   moPlus,
            '-',      //  moMinus,
            '*',      //   moProduct,
            '/',      //   moDivide,
            '<=',     //   moLessThanOrEqual,
            '>=',     //   moGreaterThanOrEqual,
            '<>',     //   moNotEqual,
            '=',      //   moEqual,
            '>',      //   moGreaterThan,
            '<',      //   moLessThan,
            'mod',    //   moMod,
            'div',    //   moDiv,
            'and',    //   moAnd,
            'or',     //   moOr,
            'xor',    //   moXor,
            'shl',    //   moShl,
            'shr',    //   moShr
            '&',      //   moBitwiseAnd
            '|',      //   moBitwiseOr
            '^',      //   moBitwiseXor
            '+',      //   moInvalid. Ok I know + is valid, but the parser
                      //              will never find this one as it will have
                      //              found the first one.
            '+'       //   moEnd
  );

  InstructionSymbols : array [TMathInstruction] of string = (
         'None',  // miNone,
         'Neg',   // miNegative,
         'Pos',   // miPositive,
         'Push',  // miPush,
         'Pop',   // miPop,
         '-Push', // miPushNegative,
         '-Pop'   // miEnd
  );

constructor TNCMathParser.Create(aOwner : TTagOwner; aName : string);
begin
  inherited Create(aOwner, aName);
  GetMem(MathList, Sizeof(TMathRecord) * 10);
  Allocated := 10;
  OperandList := TList.Create;
end;

destructor TNCMathParser.Destroy;
begin
  Clean;
  OperandList.Free;
  FreeMem(MathList);
  inherited Destroy;
end;

procedure TNCMathParser.Clean;
begin
  if Assigned(MathList) then
    ReallocMem(MathList, Sizeof(TMathRecord) * 10);
  Allocated := 10;
  Count := 0;
  if Assigned(OperandList) then begin
    while OperandList.Count > 0 do begin
      TObject(OperandList[0]).Free;
      OperandList.Delete(0);
    end;

    OperandList.Clear;
  end;
end;

function TNCMathParser.GetAsDouble : Double;
begin
  Result := Execute;
end;

function TNCMathParser.PrecedenceLevel(aOp : TMathOperator) : Integer;
begin
  case aOp of
    moProduct,
    moDivide,
    moMod,
    moDiv,
    moAnd,
    moShl,
    moShr,
    moBitWiseAnd : Result := 3;

    moPlus,
    moMinus,
    moOr,
    moXor,
    moBitwiseOr,
    moBitwiseXor : Result := 2;


    moEqual,
    moGreaterThan,
    moLessThan,
    moLessThanOrEqual,
    moGreaterThanOrEqual,
    moNotEqual : Result := 1;

    moEnd : Result := 0;
  else
    raise EMathCompileError.CreateFmt(ImpossibleException, ['TNCMathParser.PrecedenceLevel Invalid Operator']);
  end;
end;

function TNCMathParser.CreateTagFromString(aName : string) : TAbstractTag;
var Value : Double;
begin
  if IsNumeric(aName[1]) then begin
    try
      Value := StrToFloat(aName);
      Result := TFloatTag.Create(Owner, aName);
      Result.IsConstant := True;
      TFloatTag(Result).Value := Value;

      OperandList.Add(Result);
    except
      raise EMathCompileError.Create('Cant convert this');
    end;
  end else begin
    if Assigned(OnResolveVariable) then begin
      Result := OnResolveVariable(Self, aName);
      if Result = nil then
        raise EMathCompileError.CreateFmt('Unknown Variable "%s"', [aName] );
    end else
      raise EMathCompileError.CreateFmt('Unknown Variable "%s"', [aName] );
  end;
end;

{ Sequence
 1. find inner parentheses



 *, /, div, mod	First level multiply operators
+,-,		Second level addition operators
=, <>, <, >, <=, >=	Third level Comparative operators
And, Or, Xor	Forth level Bitwise operators


 Example 2 + 3 * 4 or 12 > 3

                      Stack       Acc
 2                                2
 Push Acc +           2 +         0
 3 * 4                2 +         12
 Pop Op                           14
 Push Acc Or          14 or

 Push 12 >            14or 12>
 3                                3
 Pop Op                           1
 Pop Op                           0

 Example 3 * 4 + 6 > 12 or 3 * 6 and 7 / 4

         (((3 * 4) + 6) > 12) or (3 * 6) and (7 / 4)

                      Stack      Acc
 3 * 4 + 6                       18
 Push Acc >           18>        0
 Push 12 or           12or       0
 3 * 6                           18
 Push Acc >           12or 18>   0


 Example 3 * 4 + 6 > 12 or 3 * 6 and 7 / 4

        '12' + 6 > 12 or 3 * 6 and 7 / 4
        '18' > 12 or 3 * 6 and 7 / 4
        '18' > 12 or '18' and 7 / 4
        '18' > 12 or '2' / 4
        '18' > 12 or '0.5'
        '18' > '0'
        '1'

 Example 3 * (4 + ((6 > 12) or 3) * 6 and 7) / 4

 3 * ~Inner1~ / 4
 ~Inner1~ = 4 + ~Inner2~ * 6 and 7
 ~Inner2~ = ~Inner3~ or 3
 ~Inner3~ = 6 > 12


 Symbolic instruction set

 3 * Recurse / 4
 4 + Recurse * 6 and 7
 Recurse or 3
 6 > 12

 (3 * 4) + 7 * (5 / 4)

 Recurse + 7 * Recurse

 1. If open parenthesis then find close and recurse.
 2.

}

procedure DebugOut(const aLog : string);
//var OutTxt : string;
begin
{  OutTxt := aLog + CarRet;
  Stream.Write(PChar(OutTxt)^, Length(OutTxt)); }
end;

  function IsOperand(aChar : Char) : Boolean;
  begin
    case aChar of
      '>',
      '<',
      '=',
      '*',
      '/',
      '+',
      '-',
      '(',
      '[',
      ']',
      ' ' : Result := False;
    else
      Result := True;
    end;
  end;

// RemoveEnclosingBraces removes all the eclosing braces stating at index and returns
// a string with the braces trimmed off.
function TNCMathParser.RemoveEnclosingParentheses(var aLine : string; Index : Integer) : string;
var I : Integer;
    PareCount : Integer;
begin
  PareCount := 0;
  for I := 1 to Length(aLine) do begin
    case aLine[I] of
      '(' : Inc(PareCount);
      ')' : Dec(PareCount);
    end;
    if PareCount = 0 then begin
      Result := System.Copy(aLine, 2, I - 2);
      System.Delete(aLine, 1, I);
      Exit;
    end;
  end;
end;

// RemoveEnclosingBraces removes all the eclosing braces stating at index and returns
// a string with the braces trimmed off.
function TNCMathParser.RemoveEnclosingBraces(var aLine : string; Index : Integer) : string;
var I : Integer;
    PareCount : Integer;
begin
  PareCount := 0;
  for I := 1 to Length(aLine) do begin
    case aLine[I] of
      '[' : Inc(PareCount);
      ']' : Dec(PareCount);
    end;
    if PareCount = 0 then begin
      Result := System.Copy(aLine, 2, I - 2);
      System.Delete(aLine, 1, I);
      Exit;
    end;
  end;
end;

procedure TNCMathParser.ReadOperand(var aLine : string; var aMath : TMathRecord);
var  I : Integer;
     Tmp, OpName : string;
     Sign : Boolean;
begin
// The first character of an operand can be -, +, ( or any other non operator character
//
// On
// + Set the instruction to miPositive
// - Set the instruction to miNegative
// ( Set the instruction to miPush and call compile chunk with enclosed braces
//
// if after reading the operand a '(' is encountered then the operand must be assumed to
// be a function and the onFunction event is generated
//
// After reading an operand a '[' is the next character the operand is checked
//
  aLine := Trim(aLine);
  I := 1;

  Sign := True;

  case aLine[1] of
    '+',
    '-' : begin
           Sign := aLine[1] = '+';
           Delete(aLine, 1, 1);
    end;
  end;

  if aLine[1] = '(' then begin
    Tmp := RemoveEnclosingParentheses(aLine, 1);
    CompileChunk(Tmp);
    Self.AddPop;
    if Sign then
      aMath.Instruction := miPush
    else
      aMath.Instruction := miPushNegative;
    Exit
  end;

  while IsOperand(aLine[I]) and
        (I <= Length(aLine)) do begin
    Inc(I);
  end;

  OpName := Copy(aLine, 1, I-1);
  Delete(aLine, 1, I-1);

  aLine := TrimLeft(aLine);

  if Sign then
    aMath.Instruction := miPositive
  else
    aMath.Instruction := miNegative;

  if Length(aLine) > 0 then begin
    if aLine[1] = '(' then begin
      Tmp := RemoveEnclosingParentheses(aLine, 1);
      if Assigned(OnResolveFunction) then
        aMath.Operand := OnResolveFunction(Self, OpName, Tmp)
      else
        raise EMathCompileError.Create('Functions are not supported');

      Exit;
    end else if aLine[1] = '[' then begin
      Tmp := RemoveEnclosingBraces(aLine, 1);
      if Assigned(OnResolveArray) then
        aMath.Operand := OnResolveArray(Self, OpName, Tmp)
      else
        raise EMathCompileError.Create('Arrays are not supported');

      Exit;
    end;
  end;

  aMath.Operand := CreateTagFromString(OpName);
end;

function TNCMathParser.ReadNextOperator(aLine : string) : TMathOperator;
var I, BraceCount, PareCount : Integer;
    Tmp : string;
begin
  // Dont read IsOperand unless the parenthases are matched
  PareCount := 0;
  BraceCount := 0;
  aLine := TrimLeft(aLine);
  for I := 1 to Length(aLine) do begin
    case aLine[I] of
      '(' : Inc(PareCount);
      '[' : Inc(BraceCount);
      ')' : Dec(PareCount);
      ']' : Dec(BraceCount);
    else
      if not IsOperand(aLine[I]) and
         (PareCount = 0) and
         (BraceCount = 0) then begin
         Tmp := Copy(aLine, I, Length(aLine));
         Result := ReadOperator(Tmp);
         Exit;
      end;
    end;
  end;
  Result := moEnd;
end;

function TNCMathParser.ReadOperator(var aLine : string) : TMathOperator;
var mo : TMathOperator;
begin
  aLine := TrimLeft(aLine);

  Result := moEnd;

  if aLine = '' then
    Exit;

  for mo := Low(TMathOperator) to High(TMathOperator) do
    if OperatorSymbols[mo] =
       LowerCase(Copy(aLine, 1, Length(OperatorSymbols[mo]))) then begin

      Result := mo;
      Delete(aLine, 1, Length(OperatorSymbols[mo]));
      Exit;
    end;

  raise EMathCompileError.Create('Invalid operand');
end;


procedure TNCMathParser.Compile(aMath : string);
//var I : Integer;
begin
//  DebugOut(PChar('++ Compiling ' + aMath));
  Clean;
  FCompiledLine := aMath;
  CompileChunk(aMath);
{  for I := 0 to Count - 1 do begin
    with MathList^[I] do
      if Assigned(Operand) then
        DebugOut( Operand.Name + ' ' +
                  InstructionSymbols[Instruction] + ' ' +
                  OperatorSymbols[Operator])
      else
        DebugOut( InstructionSymbols[Instruction] + ' ' +
                  OperatorSymbols[Operator]);

  end;
  DebugOut(PChar('-- End')); }
end;

function TNCMathParser.AddRecord(const aMath : TMathRecord) : Integer;
begin
  if Count >= Allocated then begin
    Inc(Allocated, 10);
    ReallocMem(MathList, Sizeof(TMathRecord) * Allocated);
  end;
  MathList^[Count] := aMath;
  Result := Count;

  Inc(Count);
end;

procedure TNCMathParser.AddPush;
var aMath : TMathRecord;
begin
  aMath.Instruction := miPush;
  aMath.Operand := nil;
  aMath.Operator := moInvalid;
  AddRecord(aMath);
end;

procedure TNCMathParser.AddPop;
var aMath : TMathRecord;
begin
  aMath.Instruction := miPop;
  aMath.Operand := nil;
  aMath.Operator := moInvalid;
  AddRecord(aMath);
end;

// Using the rules of bound-to, read an operand and if operator
// read operator and operand
// If there is another instruction then apply the rules of bound-to
procedure TNCMathParser.CompileChunk(var aChunk : string);
var Op1 : TMathOperator;
    MathRecord : TMathRecord;
    Rec : Integer;
begin
  while Length(aChunk) > 0 do begin
    MathRecord.Instruction := miNone;
    MathRecord.Operand := nil;

    Rec := AddRecord(MathRecord);
    ReadOperand(aChunk, MathRecord);
    MathRecord.Operator := ReadOperator(aChunk);
    MathList^[Rec] := MathRecord;

    if MathRecord.Operator = moEnd then
      Exit;

    Op1 := ReadNextOperator(aChunk);

    if PrecedenceLevel(MathRecord.Operator) <
       PrecedenceLevel(Op1) then begin
      AddPush;
      CompileChunk(aChunk);
      AddPop;
    end;
  end;
end;

function TNCMathParser.ExecuteChunk(var Index : Integer) : Double;
var Op, Op1 : TMathOperator;
    aTag : TAbstractTag;
    Inst : TMathInstruction;
    N : Double;
const MDP = 10E-6;
begin
  Result := 0;
  if Count = 0 then
    Exit;

  //Set8087CW($1372);
  Set8087CW($1332);
  // Prime the pump with the first term of the expression
  with MathList^[Index] do begin
    Inst := Instruction;
    Op := Operator;
    aTag := Operand;
  end;
  Inc(Index);

  case Inst of
    miPush : Result := ExecuteChunk(Index);
    miPop : Exit;
    miPushNegative : Result := -ExecuteChunk(Index);
  else
    Result := aTag.AsDouble;
    if Inst = miNegative then
      Result := -Result;
  end;
  // End of prime the pump

  while Index < Count do begin
    with MathList^[Index] do begin
      Inst := Instruction;
      aTag := Operand;
      Op1 := Operator;
    end;

    Inc(Index);
    if Index > Count then
      Exit;

    case Inst of
      miPush : N := ExecuteChunk(Index);
      miPop : Exit;
      miPushNegative : N := -ExecuteChunk(Index);
    else
{      if aTag.Convert and Owner.ConvertEnable then
        N := aTag.AsDouble * Owner.Units
      else }
        N := aTag.AsDouble;

      if Inst = miNegative then
        N := -N;
    end;

    case Op of
      moProduct : Result := Result * N;
      moDivide : Result := Result / N;
      moMod : Result := Round(Result) mod Round(N);
      moDiv : Result := Round(Result) div Round(N);
      moAnd : Result := Round(Result) and Round(N);
      moShl : Result := Round(Result) shl Round(N);
      moShr : Result := Round(Result) shr Round(N);

      moPlus : Result := Result + N;
      moMinus: Result := Result - N;
      moOr : Result := Round(Result) or Round(N);
      moXor : Result := Round(Result) xor Round(N);

      moEqual : if SysObj.StrictComparison
                  then Result := Ord(Result = N)
                  else Result := Ord(Abs(Result - N) < MDP);

      moGreaterThan : Result := Ord(Result > N);
      moLessThan : Result := Ord(Result < N);

      moLessThanOrEqual : if SysObj.StrictComparison
                  then Result := Ord(Result <= N)
                  else Result := Ord((Result <= N) or (Abs(Result - N) < MDP));
      moGreaterThanOrEqual : if SysObj.StrictComparison
                  then Result := Ord(Result >= N)
                  else Result := Ord((Result >= N) or (Abs(Result - N) < MDP));
      moNotEqual : if SysObj.StrictComparison
                  then Result := Ord(Result <> N)
                  else Result := Ord(Abs(Result - N) > MDP);


      moBitwiseAnd : Result := Ord((Round(Result) <> 0) and (Round(N) <> 0));
      moBitwiseOr : Result := Ord((Round(Result) <> 0) or (Round(N) <> 0));
      moBitwiseXOr : Result := Ord((Round(Result) <> 0) xor (Round(N) <> 0));
      moEnd : Result := Result * 1;
    else
      raise EMathExecuteError.CreateFmt(ImpossibleException, ['TNCMathParser.Execute Invalid Operator']);
    end;
    Op := Op1;
  end;
end;

function TNCMathParser.Execute : Double;
var Index : Integer;
begin
  Index := 0;
  Result := ExecuteChunk(Index);
end;


function TNCMathParser.ExtractSimpleUnderlyingTag: TAbstractTag;
begin
  if IsSimple then
    Result:= MathList^[0].Operand
  else
    Result:= nil;
end;

function TNCMathParser.IsSimple: Boolean;
var PR: TMathRecord;
begin
  Result:= False;
  if Count = 1 then begin
    PR:= MathList^[0];

    if (PR.Instruction = miPositive) and (PR.Operator = moEnd) then
      Result:= True;
  end;
end;

initialization
//  Stream := TFileStream.Create(ProfilePath + 'MathOut.txt', fmCreate);
  RegisterTagClass(TFloatTag);
  RegisterTagClass(TAlphaTag);
  RegisterTagClass(TOrdinalTag);
  RegisterTagClass(TNCMathParser);
finalization
//  Stream.Free;
end.
