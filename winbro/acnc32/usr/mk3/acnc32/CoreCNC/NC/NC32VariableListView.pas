unit NC32VariableListView;

interface

uses Classes, SysUtils, CoreCNC, CncAbs, Cnc32, NC32, ComCtrls, Grids, Controls, Named,
     CncTypes, NCNames, NCProgram, NC32Types;

type
  TNC32VariableListView = class(TExpandDisplay)
  private
    NC32 : TNC32;
    Tab : TTabControl;
    List : TListView;
    procedure UpdateView(aTag : TAbstractTag);
    procedure AddData(aName : string; Data : TObject);
    procedure TabChange(Sender : TObject);
  protected
  public
    constructor Create(aOwner : TComponent); override;
    procedure InstallComponent(Params : TParamStrings); override;
    procedure Installed; override;
    procedure Open; override;
    procedure Resize; override;
  end;

implementation

{ TNC32VariableListView }
var Blogg: Integer;
procedure TNC32VariableListView.UpdateView(aTag: TAbstractTag);
var I, J : Integer;
    Data : TObject;
    S : TStrings;
    A : TArrayTag;
    LI: TListItem;
begin
  if Visible then begin
    if Tab.TabIndex <> -1 then begin
      if not NC32.PartProgram.Loaded then
        Tab.Tabs.Objects[0] := nil
      else if NC32.NCHAL.PartProgram.Loaded then
        Tab.Tabs.Objects[0] := NC32.PartProgram.SubRoutine[NC32.NCHAL.PPBGUnit].VariableList;

      Data := TObject(Tab.Tabs.Objects[Tab.TabIndex]);
      if Assigned(Data) then begin

        if Tab.TabIndex = 0 then begin
          S := TStrings(Data);
          for I := 0 to S.Count - 1 do begin
            Li:= List.Items[I];
            if Li.SubItems[0] <> TAbstractTag(S.Objects[I]).AsString then
             Li.SubItems[0] := TAbstractTag(S.Objects[I]).AsString;
          end;
        end
        else if Data is TNC32PartProgramFile then begin
          with Data as TNC32PartProgramFile do begin
            Blogg:= VariableCount;
            for I := 0 to VariableCount - 1 do begin
              Li:= List.Items[I];
              if Li.SubItems[0] <> Variable[I].AsString then
                Li.SubItems[0] := Variable[I].AsString;
            end;
          end;
        end

        else if Data is TArrayTag then begin
          A := TArrayTag(Data);
          if A.Dimensions[1] > 1 then begin
            for I := 0 to A.Dimensions[0] - 1 do begin
              Li:= List.Items[I];
              for J := 0 to A.Dimensions[1] - 1 do
                if Li.SubItems[J] <> Format('%g', [A.GetDoubleValue([I, J])]) then
                  LI.SubItems[J] := Format('%g', [A.GetDoubleValue([I, J])]);
            end;
          end else begin
            for I := 0 to A.Dimensions[0] - 1 do begin
              Li:= List.Items[I];
              if Li.SubItems[0] <> A.AsArrayString[I] then
                Li.SubItems[0] := A.AsArrayString[I];
            end;
          end;
        end;
      end;
    end;
  end;
end;

constructor TNC32VariableListView.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
  Tab := TTabControl.Create(Self);
  Tab.Parent := Self;
  Tab.Align := alClient;
  Tab.OnChange := TabChange;
  Tab.MultiLine := True;

  List := TListView.Create(Self);
  List.Parent := Tab;
  List.Align := alClient;
  List.ViewStyle := vsReport;
  List.GridLines := True;
end;

procedure TNC32VariableListView.AddData(aName: string; Data: TObject);
begin
  Tab.Tabs.AddObject(aName, Data);
end;

var PositionTypes : array [TPositionType] of string;

procedure TNC32VariableListView.Installed;
var I: Integer;
begin
  inherited;

  PositionTypes[ptDisplay] :=      SysObj.Localized.GetString('$DISPLAY');
  PositionTypes[ptCommanded] :=    SysObj.Localized.GetString('$COMMANDED');
  PositionTypes[ptMachine] :=      SysObj.Localized.GetString('$MACHINE');
  PositionTypes[ptTarget] :=       SysObj.Localized.GetString('$TARGET');
  PositionTypes[ptDisttogo] :=     SysObj.Localized.GetString('$DISTTOGO');
  PositionTypes[ptFerr] :=         SysObj.Localized.GetString('$LAG');
  PositionTypes[ptBias] :=         SysObj.Localized.GetString('$BIAS');
  PositionTypes[ptCompensation] := SysObj.Localized.GetString('$COMPENSATION');
  PositionTypes[ptCPU] :=          SysObj.Localized.GetString('$CPU');
  PositionTypes[ptGridShift] :=    SysObj.Localized.GetString('$GRIDSHIFT');
  PositionTypes[ptTimeRIM] :=      SysObj.Localized.GetString('$TIMERIM');
  PositionTypes[ptMaster] :=       SysObj.Localized.GetString('$MASTER');
  PositionTypes[ptAcceleration] := SysObj.Localized.GetString('$ACCELERATION');
  PositionTypes[ptVelocity] :=     SysObj.Localized.GetString('$VELOCITY');

  Tab.TabHeight := 25;
  if Assigned(NC32) then begin
    AddData(SubroutineLang, nil);
    AddData(ProgramLang, NC32.PartProgram);
    AddData('#', NC32.NCHAL.HArray);
    AddData('##', NC32.NCHAL.HHArray);
    AddData('GEO', NC32.NCHAL.HProbe);
    AddData('G50', NC32.NCHAL.G50);
    AddData('G51', NC32.NCHAL.G51);
    AddData('G52', NC32.NCHAL.G52);
    AddData('G92', NC32.NCHAL.G92);
    AddData('G54', NC32.NCHAL.CoordSys[0]);
    AddData('G55', NC32.NCHAL.CoordSys[1]);
    AddData('G56', NC32.NCHAL.CoordSys[2]);
    AddData('G57', NC32.NCHAL.CoordSys[3]);
    AddData('G58', NC32.NCHAL.CoordSys[4]);
    AddData('G59', NC32.NCHAL.CoordSys[5]);
    AddData('GGroup', NC32.NCHAL.ActiveGGroup);
    AddData('SysA', NC32.NCHAL.SysVarA);
    AddData('SysU', NC32.NCHAL.SysVarU);
    AddData(StowNameA, NC32.NCHAL.StowA);
    AddData(StowNameB, NC32.NCHAL.StowB);
    AddData(StowNameC, NC32.NCHAL.StowC);
    AddData(ResultName, NC32.NCHAL.ResultA);
    AddData(IHWName, NC32.NCHAL.IHW);
    AddData(OHWName, NC32.NCHAL.OHW);
    AddData(ZeroOffsetAdjustName, NC32.NCHAL.ZeroOffsetAdjust);
    AddData(CMMName, NC32.NCHAL.CMMFile.Offsets);

    for I := 0 to SysObj.NC.AxisCount - 1 do
      AddData(SysObj.NC.AxisName[I], NC32.LAxisPosition[I]);
    TagEvent(NameDisplaySweep, EdgeChange, TMethodTag, UpdateView);
  end;

  Tab.TabIndex:= 0;
  TabChange(nil);
end;

procedure TNC32VariableListView.Open;
begin
  inherited;

end;

const ColumnWidth = 150;

procedure TNC32VariableListView.InstallComponent(Params: TParamStrings);
begin
  inherited;
  if SysObj.NC is TNC32 then
    NC32 := TNC32(SysObj.NC)
  else
    NC32 := nil;
end;

procedure TNC32VariableListView.TabChange(Sender: TObject);
var S: TStrings;
    LI: TListItem;
    Data: TObject;
    I, J: Integer;
    LC: TListColumn;
    A: TArrayTag;
begin
  List.Clear;
  List.Columns.Clear;

  if Tab.TabIndex <> -1 then begin
    Data := TObject(Tab.Tabs.Objects[Tab.TabIndex]);

    if Tab.TabIndex = 0 then begin
      LC := List.Columns.Add;
      LC.Caption := NameLang;
      LC.Width := ColumnWidth;

      LC := List.Columns.Add;
      LC.Caption := ValueLang;
      LC.Width := ColumnWidth;

      for I := 0 to 25 do begin
        LI:= List.Items.Add;
        LI.Caption:= '_' + NCParamNames[TSFAParameter(I)];
        LI.SubItems.Add('0');
      end;
    end else if Data is TNC32PartProgramFile then begin
      LC := List.Columns.Add;
      LC.Caption := NameLang;
      LC.Width := ColumnWidth;

      LC := List.Columns.Add;
      LC.Caption := ValueLang;
      LC.Width := ColumnWidth;

      with Data as TNC32PartProgramFile do begin

        for I := 0 to VariableCount - 1 do begin
          LI:= List.Items.Add;
          LI.Caption:= Variable[I].Name;
          LI.SubItems.Add(Variable[I].AsString);
        end;
      end;


    end else if Data is TStrings then begin
      S := TStrings(Data);

      LC := List.Columns.Add;
      LC.Caption := NameLang;
      LC.Width := ColumnWidth;

      LC := List.Columns.Add;
      LC.Caption := ValueLang;
      LC.Width := ColumnWidth;

      for I:= 0 to S.Count - 1 do begin
        LI:= List.Items.Add;
        LI.Caption:= TAbstractTag(S.Objects[I]).Name;
        LI.SubItems.Add(TAbstractTag(S.Objects[I]).AsString);
      end;

    end
    else if Data is TArrayTag then begin
      A := TArrayTag(Data);

      LC := List.Columns.Add;
      LC.Caption := NameLang;
      LC.Width := ColumnWidth;

      if Data is TAxisPositionTag then begin

        LC := List.Columns.Add;
        LC.Caption := ValueLang;
        LC.Width := ColumnWidth;

        for I := 0 to A.Dimensions[0] - 1 do begin
          LI:= List.Items.Add;
          LI.Caption:= A.Name + '[' + IntToStr(I) + '] (' + PositionTypes[TPositionType(I)] + ')';
          LI.SubItems.Add('0');
        end;

      end
      else if A.Dimensions[1] > 1 then begin
        for I := 0 to A.Dimensions[1] - 1 do begin
          LC := List.Columns.Add;
          LC.Caption := ',' + IntToStr(I) + ']';
          LC.Width := 80;
        end;

        for I:= 0 to A.Dimensions[0] - 1 do begin
          LI:= List.Items.Add;
          LI.Caption:= A.Name + '[' + IntToStr(I) + ',';
          for J:= 0 to A.Dimensions[1] - 1 do begin
            LI.SubItems.Add('0');
          end;
        end;

      end else begin  // A.Dimentions[1] < 2
        LC := List.Columns.Add;
        LC.Caption := ValueLang;
        LC.Width := ColumnWidth;

        for I:= 0 to A.Dimensions[0] - 1 do begin
          LI:= List.Items.Add;
          LI.Caption:= A.Name + '[' + IntToStr(I) + ']';
          LI.SubItems.Add('0');
        end;
      end;
    end;
  end;
  UpdateView(nil);
end;

procedure TNC32VariableListView.Resize;
begin
  inherited;
  Tab.MultiLine:= (Height > 300) and (Width > 400);
end;

initialization
  RegisterCNCClass(TNC32VariableListView);
end.
