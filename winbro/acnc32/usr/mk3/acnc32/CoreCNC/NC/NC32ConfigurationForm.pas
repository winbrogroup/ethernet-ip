unit NC32ConfigurationForm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Tokenizer, CoreCNC, NC32Types, CNC32, CNCTypes, StdCtrls, ComCtrls,
  Buttons, ExtCtrls, Grids, Named, CNCRegistry;

type
  TNCGlobalParam = (
    ncglEnd,
    ncglPreparatoryFile,
    ncglPostFile,
    ncglCoordinateSystem,
    ncglHardwareFlags,
    ncglPlcManagedLimits,
    ncglPlcManagedOpenLoop
  );

  TNCGlobalParams = set of TNCGlobalParam;

  TNCCoordinateParam = (
    nccoEnd,
    nccoAxisNames,
    nccoMaxFeedrate,
    nccoThreadKindness,
    nccoFindMotionLookahead,
    nccoBlockLookahead,
    nccoInchDefault,
    nccoBlocktime,
    nccoServotime,
    nccoMotor
  );

  TNCCoordinateParams = set of TNCCoordinateParam;

  TNCMotorParam = (
    ncmoEnd,
    ncmoAxisDesignation,
    ncmoConnection,
    ncmoCPUnit, // Counts per MM / Deg
    ncmoShortestPath,
    ncmoDualEncoder,
    ncmoHomeProgram,
    ncmoInPositionWindow,
    ncmoFatalFollowError,
    ncmoWarnFollowError,
    ncmoMaximumRapidFeed,
    ncmoMaximumJogFeed,
    ncmoHomeDogSearchSense,
    ncmoHomeDogSearch,
    ncmoHomeFlagSearch,
    ncmoProportional,
    ncmoIntegrator,
    ncmoDerivative,
    ncmoAcceleration,
    ncmoScaling
  );

  TNCMotorParams = set of TNCMotorParam;

  TNC32Configure = class;
  TCoordinateSetup = class;

  TMotorSetup = class(TObject)
    Enabled : Boolean;
    CoordinateSystem : TCoordinateSetup;
    Connection : Integer;
    Display : Integer;
    CPUnit : Double;
    ShortestPath : Boolean;
    HomeProgram : Integer;
    DualEncoder : Integer;
    InPositionWindow : Double;
    FatalFollowError : Double;
    WarnFollowError : Double;
    MotorParams : TNCMotorParams;
    AxisDesignation : Char;
    MaximumRapidFeed : Double;
    MaximumJogFeed : Double;
    AxisPositionTag : TAxisPositionTag;
    HomeDogSearchSense : Boolean;
    HomeDogSearch : Double;
    HomeFlagSearch : Double;
    Proportional, Integrator, Derivative : Double;
    Acceleration: Double;
    Scaling: Integer;
    constructor Create;
    procedure WriteToStrings(S : TStrings);
    procedure ReadFromStrings(S : TNC32Configure; CSys : TCoordinateSetup);
  end;

  TCoordinateSetup = class(TObject)
  public
    Enabled : Boolean;
    AxisByConnection : array [0..7] of TMotorSetup;
    AxisByDisplay : array [0..7] of TMotorSetup;
    AxisByName : array [nfParamX..nfParamW] of TMotorSetup;
    AxisNames : string;
    NumberOfAxis : Integer;
    MaxFeedrate : Double;
    LegalAxisParameters : TSFAParameters;
    CSysIndex : Integer;
    ThreadKindness : Integer;
    FindMotionLookahead : Integer;
    BlockLookahead : Integer;
    Blocktime: Integer;
    Servotime: Double;
    InchDefault : Boolean;
    constructor Create;
    procedure WriteToStrings(S : TStrings);
    procedure ReadFromStrings(S : TNC32Configure);
  end;

  TNC32Configure = class(TStringTokenizer)
  private
    MotorList : TList;
    CoordList : TList;
    CommentChar : Char;
    DummyMotor : TMotorSetup;
    DummyCoord : TCoordinateSetup;
    FNCIndex : Integer;
    FPlcManagedLimits: Boolean;
    FPlcManagedOpenLoop: Boolean;
  protected
    PreparatoryFile : string;
    PostFile : string;
    HardwareFlags : string;
    function GetAxisNames(Index : Integer) : string;
    function GetAxisCount(Index : Integer) : Integer;
    function GetCoordinateSetup(Index : Integer) : TCoordinateSetup;
    function GetMotorSetup(Index : Integer) : TMotorSetup;
    function ReadDoubleAssign : Double;
    function ReadIndex(MaxVal : Integer) : Integer;
  public
    MetricStorageBias: array[0..31] of Double;
    MetricStorageCommand: array[0..31] of Double;

    procedure WriteMetricStorage;
    property CoordinateSetup[Index : Integer] : TCoordinateSetup read GetCoordinateSetup;
    property MotorSetup[Index : Integer] : TMotorSetup read GetMotorSetup;
    constructor Create;
    destructor Destroy; override;
    function Read : string; override;
    procedure ExpectedToken(aToken : string);
    procedure ReadFromStrings;
    procedure WriteToStrings;
    function AxisIndexFromName(aCoord : Integer; aName : Char) : Integer;
    property AxisCount[Index : Integer] : Integer read GetAxisCount;
    property AxisNames[Index : Integer] : string read GetAxisNames;
    property NCIndex : Integer read FNCIndex;
    function TextParser(aText : string) : string; dynamic;
    property PlcManagedLimits: Boolean read FPlcManagedLimits;
    property PlcManagedOpenLoop: Boolean read FPlcManagedOpenLoop;
  end;

  TNC32Configuration = class(TForm)
    Panel1: TPanel;
    OkBtn: TBitBtn;
    CancelBtn: TBitBtn;
    Panel4: TPanel;
    BitBtnDeleteCoordSys: TBitBtn;
    BitBtnNewCoordSys: TBitBtn;
    PostFileClick: TSpeedButton;
    PrepFileClick: TSpeedButton;
    Label2: TLabel;
    Label1: TLabel;
    OpenDialog: TOpenDialog;
    GroupBox1: TGroupBox;
    TabControl1: TTabControl;
    CMaxFeedRateEdit: TGroupBox;
    MotorTabControl: TTabControl;
    GroupBox3: TGroupBox;
    Panel2: TPanel;
    BtnFirstAxis: TSpeedButton;
    Panel3: TPanel;
    BitBtnNewMotor: TBitBtn;
    BitBtnDeleteMotor: TBitBtn;
    MGrid: TStringGrid;
    CB: TComboBox;
    Edit: TEdit;
    PrepFileLabel: TLabel;
    PostFileLabel: TLabel;
    CMaxFeedEdit: TEdit;
    Label3: TLabel;
    GroupBox2: TGroupBox;
    HardwareMemo: TMemo;
    procedure MGridSelectCell(Sender: TObject; Col, Row: Integer;
      var CanSelect: Boolean);
    procedure EditExit(Sender: TObject);
    procedure CBExit(Sender: TObject);
    procedure MGridTopLeftChanged(Sender: TObject);
    procedure MotorTabControlChanging(Sender: TObject;
      var AllowChange: Boolean);
    procedure BitBtnNewMotorClick(Sender: TObject);
    procedure MotorTabControlChange(Sender: TObject);
    procedure PrepFileClickClick(Sender: TObject);
    procedure PostFileClickClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BitBtnDeleteMotorClick(Sender: TObject);
    procedure BtnFirstAxisClick(Sender: TObject);
  private
    Config : TNC32Configure;
    InPlaceEdit : TWinControl;
    ThisRow, ThisCol : Integer;
    procedure Setup;
  protected
    procedure WriteToStrings;
  public
    class function Execute(aConfig : TNC32Configure) : Boolean;
  end;

implementation

{$R *.DFM}

var
  NC32Configuration: TNC32Configuration;

resourcestring
  ErrorInConfiguration = '%s Occured at line %d in NC configuration [%s]';
  UnknownDirective = 'Unexpected token [%s]';
  MotorNotDefinedEnough = 'Motor has insufficient setup';
  IndexFieldTooHigh = 'Index must be less than [%d] : actual value [%d]';
  ConnectionAlreadyDefined = 'Motor Connection [%d] already allocated';

const
  GlobalNames : array[TNCGlobalParam] of Pchar = (
    '}',
    'PreparatoryFile',
    'PostFile',
    'CoordinateSystem',
    'HardwareFlags',
    'PlcManagedLimits',
    'PlcManagedOpenLoop'
  );

  CoordinateNames : array[TNCCoordinateParam] of Pchar = (
    '}',
    'AxisNames',
    'MaxFeedrate',
    'ThreadKindness',
    'FindMotionLookahead',
    'BlockLookahead',
    'InchDefault',
    'Blocktime',
    'Servotime',
    'Motor'
  );

  MotorNames : array[TNCMotorParam] of Pchar = (
    '}',
    '**',      // Axis Designation not used in file
    'Connection',
    'CountsPerUnit',
    'ShortestPath',
    'DualEncoder',
    'HomeProgram',
    'InPositionWindow',
    'FatalFollowError',
    'WarnFollowError',
    'MaxRapidFeed',
    'MaxJogFeed',
    'HomeDogSearchSense',
    'HomeDogSearch',
    'HomeFlagSearch',
    'Proportional',
    'Integrator',
    'Derivative',
    'Acceleration',
    'Scaling'

  );


  MotorParameters : array [TNCMotorParam] of string = (
    '' ,
    'Axis Name',
    'Connection',
    'Counts per mm',
    'Shortest Path',
    'Dual Encoder',
    'Home Program',
    'In Position window',
    'Fatal Follow Error',
    'Warn Follow Error',
    'Max Rapid Feed',
    'Max Jog Feed',
    'Dog Search Sense',
    'Dog Search',
    'Home Flag Search',
    'Proportional',
    'Integrator',
    'Derivative',
    'Acceleration',
    'Scaling'
  );

constructor TMotorSetup.Create;
begin
  Enabled := False;
  CoordinateSystem := nil;
  Connection := 0;
  Display := 0;
  CPUnit := 1000;
  ShortestPath := False;
  HomeProgram := 1;
  DualEncoder := -1;
  InPositionWindow := 0.003;
  FatalFollowError := 1;
  WarnFollowError := 0.5 ;
  MotorParams := [];
  MaximumRapidFeed := 100;
  MaximumJogFeed := 100;
  HomeDogSearchSense := True;
  HomeDogSearch := 1;
  HomeFlagSearch := -1;
  Acceleration:= 1;
  Scaling:= 96;
end;

procedure TMotorSetup.WriteToStrings(S : TStrings);
const Indent = '      ';
begin
    S.Add(Format('    Motor[%s] = {', [AxisDesignation]));
    S.Add(Format(Indent + '%s = %d', [MotorNames[ncmoConnection], Connection]));
    S.Add(Format(Indent + '%s = %f', [MotorNames[ncmoCPUnit],            CPUnit]));
    S.Add(Format(Indent + '%s = %d', [MotorNames[ncmoShortestPath],      Ord(ShortestPath)]));
    S.Add(Format(Indent + '%s = %d', [MotorNames[ncmoDualEncoder],       Ord(DualEncoder)]));
    S.Add(Format(Indent + '%s = %d', [MotorNames[ncmoHomeProgram],       HomeProgram]));
    S.Add(Format(Indent + '%s = %f', [MotorNames[ncmoInPositionWindow],  InPositionWindow]));
    S.Add(Format(Indent + '%s = %f', [MotorNames[ncmoFatalFollowError],  FatalFollowError]));
    S.Add(Format(Indent + '%s = %f', [MotorNames[ncmoWarnFollowError],   WarnFollowError]));
    S.Add(Format(Indent + '%s = %f', [MotorNames[ncmoMaximumRapidFeed],  MaximumRapidFeed]));
    S.Add(Format(Indent + '%s = %f', [MotorNames[ncmoMaximumJogFeed],    MaximumJogFeed]));
    S.Add(Format(Indent + '%s = %d', [MotorNames[ncmoHomeDogSearchSense],Ord(HomeDogSearchSense)]));
    S.Add(Format(Indent + '%s = %f', [MotorNames[ncmoHomeDogSearch],     HomeDogSearch]));
    S.Add(Format(Indent + '%s = %f', [MotorNames[ncmoHomeFlagSearch],    HomeFlagSearch]));
    S.Add(Format(Indent + '%s = %f', [MotorNames[ncmoProportional],      Proportional]));
    S.Add(Format(Indent + '%s = %f', [MotorNames[ncmoIntegrator],        Integrator]));
    S.Add(Format(Indent + '%s = %f', [MotorNames[ncmoDerivative],        Derivative]));
    S.Add(Format(Indent + '%s = %f', [MotorNames[ncmoAcceleration],      Acceleration]));
    S.Add(Format(Indent + '%s = %d', [MotorNames[ncmoScaling],           Scaling]));
    S.Add('    }');
end;

procedure TMotorSetup.ReadFromStrings(S : TNC32Configure; CSys : TCoordinateSetup);
var mo : TNCMotorParam;
    Token : string;
    Unhandled : Boolean;
begin
  S.ExpectedToken('[');
  AxisDesignation := S.Read[1];
  S.ExpectedToken(']');
  S.ExpectedToken('=');
  S.ExpectedToken('{');
  Token := UpperCase(S.Read);

  MotorParams := [];
  Enabled := True;
  CoordinateSystem := CSys;
  Display :=  Pos(AxisDesignation, CoordinateSystem.AxisNames) - 1;
  if Display < 0 then
    raise Exception.CreateFmt('Axis name not in coordinate system [%c]', [AxisDesignation]);
  AxisDesignation := CoordinateSystem.AxisNames[Display + 1];
  try
    while not S.EndOfStream do begin
      Unhandled := True;
      for mo := Low(TNCMotorParam) to High(TNCMotorParam) do begin
        if Token = UpperCase(MotorNames[mo]) then begin
          case mo of
            ncmoConnection : begin
              Connection := S.ReadIntegerAssign;
            end;
            ncmoCPUnit : CPUnit := S.ReadDoubleAssign;
            ncmoShortestPath : ShortestPath := S.ReadBooleanAssign;
            ncmoDualEncoder : DualEncoder := S.ReadIntegerAssign;
            ncmoHomeProgram : HomeProgram := S.ReadIntegerAssign;
            ncmoInPositionWindow : InPositionWindow := S.ReadDoubleAssign;
            ncmoFatalFollowError : FatalFollowError := S.ReadDoubleAssign;
            ncmoWarnFollowError : WarnFollowError := S.ReadDoubleAssign;
            ncmoMaximumRapidFeed : MaximumRapidFeed := S.ReadDoubleAssign;
            ncmoMaximumJogFeed : MaximumJogFeed := S.ReadDoubleAssign;
            ncmoHomeDogSearchSense : HomeDogSearchSense := S.ReadBooleanAssign;
            ncmoHomeDogSearch : HomeDogSearch := S.ReadDoubleAssign;
            ncmoHomeFlagSearch : HomeFlagSearch := S.ReadDoubleAssign;
            ncmoProportional : Proportional := S.ReadDoubleAssign;
            ncmoIntegrator : Integrator := S.ReadDoubleAssign;
            ncmoDerivative : Derivative := S.ReadDoubleAssign;
            ncmoAcceleration : Acceleration := S.ReadDoubleAssign;
            ncmoScaling : Scaling := S.ReadIntegerAssign;
            ncmoEnd : begin
              Exit;
            end
          end;
          Include(MotorParams, mo);
          Unhandled := False;
          Break; // Exit loop
        end;
      end;
      if UnHandled then
        raise Exception.CreateFmt(UnknownDirective, [S.LastToken]);
      Token := UpperCase(S.Read);
    end;
  finally
    CoordinateSystem.AxisByDisplay[Display] := Self;
    if CoordinateSystem.AxisByConnection[Connection] <> nil then
      raise Exception.CreateFmt(ConnectionAlreadyDefined, [Connection]);
    CoordinateSystem.AxisByConnection[Connection] := Self;
  end;
end;



constructor TCoordinateSetup.Create;
begin
  inherited Create;
  Enabled := False;
  AxisNames := '';
  NumberOfAxis := 0;
  MaxFeedrate := 1;
  ThreadKindness := 20;
  FindMotionLookahead := 500;
  BlockLookahead := 5;
  Blocktime:= 100;
  Servotime:= 2259;
end;

procedure TCoordinateSetup.WriteToStrings(S : TStrings);
const Indent = '    ';
var I : Integer;
begin
  S.Add(Format('  CoordinateSystem[%d] = {', [CSysIndex]));
  S.Add(Format(Indent + '%s = %s', [CoordinateNames[nccoAxisNames], AxisNames]));
  S.Add(Format(Indent + '%s = %f', [CoordinateNames[nccoMaxFeedrate], MaxFeedrate]));
  S.Add(Format(Indent + '%s = %d', [CoordinateNames[nccoThreadKindness], ThreadKindness]));
  S.Add(Format(Indent + '%s = %d', [CoordinateNames[nccoFindMotionLookahead], FindMotionLookahead]));
  S.Add(Format(Indent + '%s = %d', [CoordinateNames[nccoBlockLookahead], BlockLookahead]));
  S.Add(Format(Indent + '%s = %d', [CoordinateNames[nccoInchDefault], Ord(InchDefault)]));
  S.Add(Format(Indent + '%s = %d', [CoordinateNames[nccoBlocktime], Blocktime]));
  for I := 0 to NumberOfAxis - 1 do
    AxisByDisplay[I].WriteToStrings(S);
  S.Add('  }');
end;

procedure TCoordinateSetup.ReadFromStrings(S : TNC32Configure);
var pr : TNCCoordinateParam;
    Token : string;
    Handled : Boolean;
    Motor : TMotorSetup;
begin
  CSysIndex := S.ReadIndex(8);
  S.CoordList[CSysIndex] := Self;
  S.ExpectedToken('=');
  S.ExpectedToken('{');

  Token := UpperCase(S.Read);
  Enabled := True;
  InchDefault := False;

  while not S.EndOfStream do begin
    Handled := False;
    for pr := Low(TNCCoordinateParam) to High(TNCCoordinateParam) do begin
      if Token = UpperCase(CoordinateNames[pr]) then begin
        case pr of
          nccoAxisNames : begin
            AxisNames := S.ReadStringAssign;
            NumberOfAxis := Length(AxisNames);
          end;
          nccoMaxFeedrate : MaxFeedRate := S.ReadDoubleAssign;
          nccoMotor : begin
            Motor := TMotorSetup.Create;
            Motor.ReadFromStrings(S, Self);
            S.MotorList[Motor.Connection] := Motor;
          end;
          nccoThreadKindness : ThreadKindness := S.ReadIntegerAssign;
          nccoFindMotionLookahead : FindMotionLookahead := S.ReadIntegerAssign;
          nccoBlockLookahead : BlockLookahead := S.ReadIntegerAssign;
          nccoInchDefault : InchDefault := S.ReadBooleanAssign;
          nccoBlocktime : Blocktime := S.ReadIntegerAssign;
          nccoServotime : Servotime := S.ReadDoubleAssign;
          nccoEnd : Exit;
        end;
        Handled := True;
      end;
    end;
    if not Handled then
      raise Exception.CreateFmt(UnknownDirective, [S.LastToken]);
    Token := UpperCase(S.Read);
  end;
end;



constructor TNC32Configure.Create;
var I : Integer;
begin
  inherited Create;
  MotorList := TList.Create;
  CoordList := TList.Create;

  DummyCoord := TCoordinateSetup.Create;
  DummyMotor := TMotorSetup.Create;
  // Reserve a bit of space for motor pointers
  for I := 0 to 8 do begin
    MotorList.Add(nil);
    CoordList.Add(nil);
  end;
end;

destructor TNC32Configure.Destroy;
begin
  while MotorList.Count > 0 do begin
   if Assigned(MotorList[0]) and (MotorList[0] <> DummyMotor) then
      TObject(MotorList[0]).Free;
    MotorList.Delete(0);
  end;

  DummyMotor.Free;

  while CoordList.Count > 0 do begin
    if Assigned(TObject(CoordList[0])) and (CoordList[0] <> DummyCoord) then
      TObject(CoordList[0]).Free;
    CoordList.Delete(0);
  end;

  DummyCoord.Free;

  MotorList.Free;
  CoordList.Free;
  inherited Destroy;
end;


function TNC32Configure.GetAxisNames(Index : Integer) : string;
begin
  Result := CoordinateSetup[Index].AxisNames;
end;

function TNC32Configure.GetAxisCount(Index : Integer) : Integer;
begin
  Result := Length(CoordinateSetup[Index].AxisNames);
end;

procedure TNC32Configure.ExpectedToken(aToken : string);
begin
  if Read <> aToken then
    raise ECNCInstallFault.CreateFmt('Expected Token %s at Line %d', [aToken, Line]);
end;

function TNC32Configure.Read : string;
begin
  Result := inherited Read;
  if not EndOfStream and
     (Length(Result) > 0)and
     (Result[1] = CommentChar) then begin
       ReadLine;
       Result := Read;
  end;
end;

function TNC32Configure.GetCoordinateSetup(Index : Integer) : TCoordinateSetup;
begin
  Result := TCoordinateSetup(CoordList[Index]);
end;

function TNC32Configure.GetMotorSetup(Index : Integer) : TMotorSetup;
begin
  Result := TMotorSetup(MotorList[Index]);
end;


function TNC32Configure.ReadDoubleAssign : Double;
begin
  ExpectedToken('=');
  Result := StrToFloat(Read);
end;

function TNC32Configure.ReadIndex(MaxVal : Integer) : Integer;
begin
  ExpectedToken('[');
  Result := StrToInt(Read);
  ExpectedToken(']');
  if Result >= MaxVal then
    raise Exception.CreateFmt(IndexFieldTooHigh, [MaxVal, Result]);
end;

function TNC32Configure.AxisIndexFromName(aCoord : Integer; aName : Char) : Integer;
begin
  for Result := 0 to Length(CoordinateSetup[aCoord].AxisNames) - 1 do
    if CoordinateSetup[aCoord].AxisNames[Result + 1] = aName then
      Exit;

  Result := -1;
end;


procedure TNC32Configure.WriteToStrings;
begin
  Self.Clear;
  Self.Add(Format('NC[%d] = {', [NCIndex]));
  Self.Add(Format('%s = %s', [GlobalNames[ncglPreparatoryFile], PreparatoryFile]));
  Self.Add(Format('%s = %s', [GlobalNames[ncglPostFile], PostFile]));

  Self.Add(Format('%s = {', [GlobalNames[ncglHardwareFlags]]));
  Self.Add(HardwareFlags);
  Self.Add('}');
  // NOTE: Only one coord-sys written at the moment
  Self.CoordinateSetup[0].WriteToStrings(Self);
end;

procedure TNC32Configure.ReadFromStrings;
  procedure ClearNCSetup;
  var I : Integer;
  begin
    for I := 0 to 7 do begin
      if Assigned(MotorList[I]) and (MotorList[I] <> DummyMotor) then
        TObject(MotorList[I]).Free;
      MotorList[I] := nil; // DefaultMotorSetup;
      if Assigned(CoordList[I]) and (CoordList[I] <> DummyCoord) then
        TObject(CoordList[I]).Free;
      CoordList[I] := nil; //DefaultCoordinateSetup;
    end;
  end;


  function ReadHardwareFlags : string;
  var Token : string;
  begin
    ExpectedToken('=');
    ExpectedToken('{');
    Result := '';
    Token := Read;
    while Token <> '}' do begin
      Result := Result + LowerCase(Token) + CarRet;
      Token := Read;
    end;
  end;

  procedure CheckAxisSetup;
  var I : Integer;
  begin
    for I := 0 to 7 do begin
      if not Assigned(MotorList[I]) then
        MotorList[I] := DummyMotor;
      if not Assigned(CoordList[I]) then
        CoordList[I] := DummyCoord;
    end;
  end;

  procedure ReadGlobal(Token : string);
  var gl : TNCGlobalParam;
      CSys : TCoordinateSetup;
  begin
    for gl := Low(TNCGlobalParam) to High(TNCGlobalParam) do begin
      if Token = UpperCase(GlobalNames[gl]) then begin
        case gl of
          ncglPreparatoryFile : PreparatoryFile := ReadStringAssign;
          ncglPostFile : PostFile := ReadStringAssign;
          ncglCoordinateSystem : begin
            CSys := TCoordinateSetup.Create;
            CSys.ReadFromStrings(Self);
          end;
          ncglHardwareFlags : HardwareFlags := ReadHardwareFlags;
          ncglPlcManagedLimits : FPlcManagedLimits:= ReadBooleanAssign;
          ncglPlcManagedOpenLoop: FPlcManagedOpenLoop:= ReadBooleanAssign;
        end;
        Exit;
      end;
    end;
    raise Exception.CreateFmt(UnknownDirective, [Token]);
  end;

var
  Token : string;
begin
  Seperator := '[]=#{}()';
  Rewind;
  CommentChar := '#';

  ClearNCSetup;

  try
    ExpectedToken('NC');
    FNCIndex := ReadIndex(1);
    ExpectedToken('=');
    ExpectedToken('{');
    Token := UpperCase(Read);

    while not EndOfStream do begin
      ReadGlobal(Token);
      Token := UpperCase(Read);
    end;
  except
    on E : Exception do
      raise ECNCInstallFault.CreateFmt(ErrorInConfiguration, [E.Message, Line, 'NC']);
  end;

  try
    CheckAxisSetup;
  except
    on E : Exception do
      raise ECNCInstallFault.Create(E.Message);
  end;
end;

function TNC32Configure.TextParser(aText : string) : string;
begin
  Result := aText;
end;


class function TNC32Configuration.Execute(aConfig : TNC32Configure) : Boolean;
begin
  Result := False;
  if not Assigned(NC32Configuration) then begin
    NC32Configuration := TNC32Configuration.Create(Application);
    NC32Configuration.Config := aConfig;
    NC32Configuration.Setup;
    try
      Result := NC32Configuration.ShowModal = mrOK;
    finally
      NC32Configuration.Free;
      NC32Configuration := nil;
    end;
  end;
end;

procedure TNC32Configuration.WriteToStrings;
var I : Integer;
begin
  // Here we will have to rebuild the Coordinate System to account for motor
  // changes.
  Config.PreparatoryFile := Self.PrepFileLabel.Caption;
  Config.PostFile := Self.PostFileLabel.Caption;

  Config.HardwareFlags := Self.HardwareMemo.Lines.Text;
  with Config.CoordinateSetup[0] do begin
    MaxFeedrate := StrToFloat( CMaxFeedEdit.Text );
    AxisNames := '';
    for I := 0 to Self.MotorTabControl.Tabs.Count - 1 do begin
      AxisByDisplay[I] := TMotorSetup(MotorTabControl.Tabs.Objects[I]);
      AxisNames := AxisNames + AxisByDisplay[I].AxisDesignation;
    end;
    NumberOfAxis := Self.MotorTabControl.Tabs.Count;
  end;
  Config.WriteToStrings;
end;

procedure TNC32Configuration.FormClose(Sender: TObject;
  var Action: TCloseAction);
var Changing : Boolean;
begin
  if ModalResult = mrOK then begin
    try
      Self.MotorTabControlChanging(Self, Changing);
      WriteToStrings;
    except
      on E : Exception do begin
        Application.ShowException(E);
        Action := caNone;
      end;
    end;
  end;
end;


procedure TNC32Configuration.Setup;
var I : TNCMotorParam;
    M : Integer;
begin
  MGrid.RowCount := 30;
  MGrid.Cells[0, 0] := 'Data Type';
  MGrid.Cells[1, 0] := 'Value';

  HardwareMemo.Lines.Text := Config.HardwareFlags;

  for I := Low(TNCMotorParam) to High(TNCMotorParam) do begin
    if MotorParameters[I] <> '' then begin
      MGrid.Cells[0, Ord(I) + 1] := MotorParameters[I];
      MGrid.Objects[0, Ord(I) + 1] := Pointer(I);
    end else
      MGrid.Cells[0, Ord(I) + 1] := 'Not used';
  end;

  PrepFileLabel.Caption := Config.PreparatoryFile;
  PostFileLabel.Caption := Config.PostFile;
  MotorTabControl.Tabs.Clear;
  with Config.CoordinateSetup[0] do begin
    CMaxFeedEdit.Text := Format('%.5f', [MaxFeedrate]);
    for M := 0 to NumberOfAxis - 1 do
      MotorTabControl.Tabs.AddObject(AxisNames[M+1], AxisByDisplay[M]);
  end;

  MotorTabControl.TabIndex := 0;
  MotorTabControlChange(Self);
end;

procedure TNC32Configuration.MGridSelectCell(Sender: TObject; Col,
  Row: Integer; var CanSelect: Boolean);

  procedure CBSelect(const Args : array of string);
  var I : Integer;
  begin
    CB.Items.Clear;

    for I := 0 to High(Args) do begin
      CB.Items.Add(Args[I]);
    end;
    CB.Text := MGrid.Cells[1, Row];
    InPlaceEdit := CB;
  end;

  procedure EditSelect;
  begin
    InPlaceEdit := Edit;
    Edit.Text := MGrid.Cells[1, Row];
  end;

begin
  case TNCMotorParam(MGrid.Objects[0, Row]) of
    ncmoAxisDesignation : CBSelect(['X', 'Y', 'Z', 'A', 'B', 'C', 'U', 'V', 'W']);
    ncmoConnection : CBSelect(['0', '1', '2', '3', '4', '5', '6', '7', '8']);
    ncmoCPUnit : EditSelect;
    ncmoShortestPath : CBSelect([TrueLang, FalseLang]);
    ncmoDualEncoder : EditSelect;
    ncmoHomeProgram : EditSelect;
    ncmoInPositionWindow : EditSelect;
    ncmoFatalFollowError : EditSelect;
    ncmoWarnFollowError : EditSelect;
    ncmoMaximumRapidFeed : EditSelect;
    ncmoMaximumJogFeed : EditSelect;
    ncmoHomeDogSearchSense : CBSelect([TrueLang, FalseLang]);
    ncmoHomeDogSearch : EditSelect;
    ncmoHomeFlagSearch : EditSelect;
    ncmoProportional : EditSelect;
    ncmoIntegrator : EditSelect;
    ncmoDerivative : EditSelect;
    ncmoAcceleration : EditSelect;
  else
    InPlaceEdit := nil;
  end;

  if Assigned(InPlaceEdit) then begin
    InPlaceEdit.Left := MGrid.ColWidths[0] + MGrid.Left + 2;
    InPlaceEdit.Top := (MGrid.DefaultRowHeight + 1) * ((Row + 1) - MGrid.TopRow) + MGrid.Top + 2;
    InPlaceEdit.Width := MGrid.ColWidths[1];
    InPlaceEdit.Visible := True;
    InPlaceEdit.Enabled := True;
    InPlaceEdit.BringToFront;
    InPlaceEdit.SetFocus;
    ThisRow := Row;
    ThisCol := Col;
  end;
end;

procedure TNC32Configuration.EditExit(Sender: TObject);
begin
  MGrid.Cells[1, ThisRow] := Edit.Text;
  Edit.Visible := False;
end;

procedure TNC32Configuration.CBExit(Sender: TObject);
begin
  MGrid.Cells[1, ThisRow] := CB.Text;
  CB.Visible := False;
end;

procedure TNC32Configuration.MGridTopLeftChanged(Sender: TObject);
var Can : Boolean;
begin
  if CB.Visible or Edit.Visible then
    MGridSelectCell(Self, ThisCol, ThisRow, Can);
end;


procedure TNC32Configuration.MotorTabControlChanging(Sender: TObject;
  var AllowChange: Boolean);

  function ValueOf(aValue : string) : Double;
  var Code : Integer;
  begin
    System.Val(aValue, Result, Code);
    if Code <> 0 then
      Result := 0;
  end;

begin
  AllowChange := True;
  if CB.Visible then
    CBExit(CB);
  if Edit.Visible then
    EditExit(Edit);

  with MotorTabControl.Tabs.Objects[MotorTabControl.TabIndex] as TMotorSetup do begin
    AxisDesignation := MGrid.Cells[1, 2][1];
    Connection := Round(ValueOf(MGrid.Cells[1, 3]));
    CPUnit := ValueOf(MGrid.Cells[1, 4]);
    ShortestPath := MGrid.Cells[1, 5] = TrueLang;
    DualEncoder := Round(ValueOf(MGrid.Cells[1, 6]));
    HomeProgram := Round(ValueOf(MGrid.Cells[1, 7]));
    InPositionWindow := ValueOf(MGrid.Cells[1, 8]);
    FatalFollowError := ValueOf(MGrid.Cells[1, 9]);
    WarnFollowError := ValueOf(MGrid.Cells[1, 10]);
    MaximumRapidFeed := ValueOf(MGrid.Cells[1, 11]);
    MaximumJogFeed := ValueOf(MGrid.Cells[1, 12]);
    HomeDogSearchSense := MGrid.Cells[1, 13] = TrueLang;
    HomeDogSearch := ValueOf(MGrid.Cells[1, 14]);
    HomeFlagSearch := ValueOf(MGrid.Cells[1, 15]);
    Proportional := ValueOf(MGrid.Cells[1, 16]);
    Integrator := ValueOf(MGrid.Cells[1, 17]);
    Derivative := ValueOf(MGrid.Cells[1, 18]);
    Acceleration := ValueOf(MGrid.Cells[1, 19]);
    MotorTabControl.Tabs[MotorTabControl.TabIndex] := AxisDesignation;
  end;

end;

procedure TNC32Configuration.BitBtnNewMotorClick(Sender: TObject);
var Change : Boolean;
    M : TMotorSetup;
begin
  MotorTabControlChanging(Self, Change);
  M := TMotorSetup.Create;
  M.Display := MotorTabControl.Tabs.Count;
  M.AxisDesignation := 'X';
  MotorTabControl.Tabs.AddObject('X', M);
end;

procedure TNC32Configuration.BitBtnDeleteMotorClick(Sender: TObject);
var Change : Boolean;
begin
  if MotorTabControl.Tabs.Count > 1 then begin
    with MotorTabControl do begin
      MotorTabControlChanging(Self, Change);
      Tabs.Objects[TabIndex].Free;
      Tabs.Delete(TabIndex);
      TabIndex := 0;
    end;
  end;
end;

procedure TNC32Configuration.BtnFirstAxisClick(Sender: TObject);
begin
  if MotorTabControl.Tabs.Count > 1 then begin
    with MotorTabControl do 
      Tabs.Move(TabIndex, 0);
  end;
end;

procedure TNC32Configuration.MotorTabControlChange(Sender: TObject);
  function MFValue(aValue : Double) : string;
  begin
    Result := Format('%0.6f', [aValue]);
  end;

begin
  with MotorTabControl.Tabs.Objects[MotorTabControl.TabIndex] as TMotorSetup do begin

    MGrid.Cells[1, 2] := AxisDesignation;
    MGrid.Cells[1, 3] := IntToStr(Connection);
    MGrid.Cells[1, 4] := MFValue(CPUnit);
    MGrid.Cells[1, 5] := BooleanIdent[ShortestPath];
    MGrid.Cells[1, 6] := IntToStr(DualEncoder);
    MGrid.Cells[1, 7] := IntToStr(HomeProgram);
    MGrid.Cells[1, 8] := MFValue(InPositionWindow);
    MGrid.Cells[1, 9] := MFValue(FatalFollowError);
    MGrid.Cells[1, 10] := MFValue(WarnFollowError);
    MGrid.Cells[1, 11] := MFValue(MaximumRapidFeed);
    MGrid.Cells[1, 12] := MFValue(MaximumJogFeed);
    MGrid.Cells[1, 13] := BooleanIdent[HomeDogSearchSense];
    MGrid.Cells[1, 14] := MFValue(HomeDogSearch);
    MGrid.Cells[1, 15] := MFValue(HomeFlagSearch);
    MGrid.Cells[1, 16] := MFValue(Proportional);
    MGrid.Cells[1, 17] := MFValue(Integrator);
    MGrid.Cells[1, 18] := MFValue(Derivative);
    MGrid.Cells[1, 19] := MFValue(Acceleration);
  end;
end;

procedure TNC32Configuration.PrepFileClickClick(Sender: TObject);
begin
  with OpenDialog do begin
    InitialDir := ProfilePath;
    if Execute then
      Self.PrepFileLabel.Caption := FileName;
  end;
end;

procedure TNC32Configuration.PostFileClickClick(Sender: TObject);
begin
  with OpenDialog do begin
    InitialDir := ProfilePath;
    if Execute then
      Self.PostFileLabel.Caption := FileName;
  end;
end;


procedure TNC32Configure.WriteMetricStorage;
var I: Integer;
    Reg: TCNCRegistry;
    Tmp: string;
    T: TMotorSetup;
begin
  Tmp:= '';
  Reg:= TCNCRegistry.Create;
  try
    Reg.OpenKey(RegistryBase + '\Persistent\Command', True);
    for I:= 0 to CoordinateSetup[0].NumberOfAxis - 1 do begin
      T:= CoordinateSetup[0].AxisByDisplay[I];
      Reg.WriteString(T.AxisDesignation, Format('%.5f', [MetricStorageCommand[I]]));
    end;
    Reg.OpenKey(RegistryBase + '\Persistent\Bias', True);
    for I:= 0 to CoordinateSetup[0].NumberOfAxis - 1 do begin
      T:= CoordinateSetup[0].AxisByDisplay[I];
      Reg.WriteString(T.AxisDesignation, Format('%.5f', [MetricStorageBias[I]]));
    end;
  finally
    Reg.Free;
  end;
end;



end.
