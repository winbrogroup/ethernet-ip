unit FakeNCHal;

interface

uses NC32, CoreCNC, TurboPmac32NCConfigure, NCProgram, Windows, CNCTypes, Classes,
     CNC32, NC32Types, SysUtils, NC32ConfigurationForm, FaultRegistration, INamedInterface,
     InLineTimer;

type
  TFakeAxis = class(TObject)
  private
    Position : Double;
    TargetPosition : Double;
    DistanceToGo : Double;
    StartPosition : Double;
    JogPlus : Boolean;
    JogMinus : Boolean;
//    State : TMotorStates;
    Name : Char;
    constructor Create(aName : Char);
    procedure Reset;
  end;

  TFakeMotion = class(TObject)
  private
    ParmList : TList;
  public
    Instruction : TNCInstruction;
    Parameters : TSFAParameters;
    TimeToComplete : Integer;
    StartTick : Integer;
    Line : Integer;
    Sub : Integer;
//    Sub : TSubPartProgram;
    constructor Create;
    destructor Destroy; override;
  end;

  TRotaryBlock = class(TList)
  private
    function GetMotion(Index : Integer) : TFakeMotion;
  public
    IsMotion : Boolean;
    constructor Create;
    destructor Destroy; override;
    procedure Clean;
    property Motion[Index : Integer] : TFakeMotion read GetMotion;
  end;

  TFakeNCHAL = class(TNCHAL)
  private
    Config : TTurboPmacNCConfigure;
    AxisList : TList;
    FeedOVR : Double;
    RotaryList : TList; // List of List of FakeMotion
    FTargetFeedRate : Double;
    Synchro : Boolean;
    LookAHead : Integer;
    HWSynch : Boolean;
    HWBlocking: Boolean;
    ForceBlocking : Boolean;
    PrimeCycleStart: Boolean;
    WriteMetricTimer: TInlineTimer;
    function GetAxis(Index : Integer) : TFakeAxis;
    function RunBlock : Boolean;
    function RunSynchroBlock(Block : TRotaryBlock): Boolean;
  protected
    procedure SetAxisPositionTags(APList : TList); override; // List of TAxisPositionTag
    procedure SetJogPlus(aAxis : Integer; aOn : Boolean); override;
    procedure SetJogMinus(aAxis : Integer; aOn : Boolean); override;
    procedure SetCycleStart(aOn : Boolean); override;
    procedure SetFeedHold(aOn : Boolean); override;
    procedure SetCycleStop(aOn : Boolean); override;
    procedure SetCycleAbort(aOn: Boolean); override;
    procedure SetFeedOverride(aFeed : Double); override;
    procedure SetFeedOverrideEnable(aOn : Boolean); override;
    procedure SetJogMode(aMode : TJogModes); override;
    procedure SetJogIncrement(aIncrement : Integer); override;

    function GetJogPlus(aAxis : Integer) : Boolean; override;
    function GetJogMinus(aAxis : Integer) : Boolean; override;
    function GetCycleStart : Boolean; override;
    function GetFeedHold : Boolean; override;
    function GetCycleStop : Boolean; override;
    function GetCycleAbort: Boolean; override;
    function GetFeedOverride : Double; override;
    function GetAxisNames : string; override;
    procedure SetRequestCNCMode(aMode : TCNCMode); override;
    function GetTargetFeedrate : Double; override;
    function GetActualFeedrate : Double; override;
    function GetAxisType(Index : Integer) : TAxisType; override;

    function PeekProgramLine: Integer;
    procedure UpdateHardware; override;
    property Axis[Index : Integer] : TFakeAxis read GetAxis;
    procedure ExecutePartProgram; override;
    procedure UpdateActiveOffset; override;
    function ResetRewindComplete: Boolean; override;
  public
    constructor Create(aTagOwner : TNC32; DeviceIndex : Integer); override;
    destructor Destroy; override;
    function CreateConfig : TNC32Configure; override;
    procedure Configure; override;
    procedure TestPoint; override;
    procedure Clean; override;
    procedure Reset; override;
    procedure ResetRewind; override;
  end;

implementation

const
  AxisParameters : TSFAParameters = [nfParamA..nfParamC, nfParamU..nfParamZ];

var
  RotaryBlockCount : Integer;
  FakeMotionCount: Integer;

constructor TFakeNCHAL.Create(aTagOwner : TNC32; DeviceIndex : Integer);
begin
  inherited Create(aTagOwner, DeviceIndex);
//  Config := TPmacNCConfigure.Create;
  AxisList := TList.Create;
  RotaryList := TList.Create;
  LookAhead := 1;
  SweepsPerSecond := 50;
  WriteMetricTimer:= TInlineTimer.Create;
  WriteMetricTimer.Timeout:= 1;
end;

destructor TFakeNCHAL.Destroy;
begin
  Clean;

  if Assigned(Config) then
    Config.Free;

  if Assigned(RotaryList) then
    RotaryList.Free;

  inherited Destroy;
end;

procedure TFakeNCHAL.Clean;
begin
  Reset;
  Sleep(10);
  if Assigned(RotaryList) then
    while RotaryList.Count > 0 do begin
      TObject(RotaryList[0]).Free;
      RotaryList.Delete(0);
    end;
  //Synchro := False;
  PPHWLine := 0;
  PPHWUnit := 0;
  FProgramLine := 0;
  HWBlocking := False;
end;

procedure TFakeNCHAL.Reset;
begin
  CycleStop := True;
  AxisHomed := [0..31];
end;

procedure TFakeNCHAL.ResetRewind;
var I, C : Integer;
begin
  inherited;
  MCode.AsInteger := 0;

  Include(FNCStates, ncsHomed);
  // Win duties
  FillChar(TotalOffset, Sizeof(ActiveOffset), 0);
  FillChar(ActiveOffset, Sizeof(ActiveOffset), 0);
  for C := 0 to AxisCount - 1 do begin
    TotalOffset[C] := ZeroOffsetAdjust.AsArrayDouble[C];
    ActiveOffset[C]:= TotalOffset[C];
  end;
  
  FillChar(LocalOffset, Sizeof(ActiveOffset), 0);
  FillChar(G50.Data^, G50.DataSize, 0);
  FillChar(G51.Data^, G51.DataSize, 0);
  FillChar(G52.Data^, G52.DataSize, 0);
  FillChar(G92.Data^, FAxisCount * Sizeof(Double), 0);

  FillChar(CoordOffset, SizeOf(CoordOffset), 0);
  FillChar(ActiveLocalOffset, Sizeof(ActiveOffset), 0);
  FillChar(ActiveCoordOffset, Sizeof(ActiveOffset), 0);
  FillChar(ActiveShiftOffset, Sizeof(ActiveOffset), 0);
  Clean;

  ForceBlocking := False;
  Incremental := False;
  RunBlockDelete := False;

  ActiveGGroup.AsArrayInteger[0] := GCodeGroupDefaults[gcgA];
  ActiveGGroup.AsArrayInteger[1] := GCodeGroupDefaults[gcgB];
  ActiveGGroup.AsArrayInteger[2] := GCodeGroupDefaults[gcgC];
  ActiveGGroup.AsArrayInteger[3] := GCodeGroupDefaults[gcgD];
  ActiveGGroup.AsArrayInteger[4] := GCodeGroupDefaults[gcgF];
  ActiveGGroup.AsArrayInteger[5] := GCodeGroupDefaults[gcgG];
  ActiveGGroup.AsArrayInteger[6] := GCodeGroupDefaults[gcgH];
  if not Config.CoordinateSetup[0].InchDefault then
    ActiveGGroup.AsArrayInteger[7] := GCodeGroupDefaults[gcgI]
  else
    ActiveGGroup.AsArrayInteger[7] := 20;
  ActiveGGroup.AsArrayInteger[8] := GCodeGroupDefaults[gcgJ];
  ActiveGGroup.AsArrayInteger[9] := GCodeGroupDefaults[gcgUnique];
  ActiveGGroup.AsArrayInteger[10] := GCodeGroupDefaults[gcgCoordSelect];

  ActiveInverseTimeFeed := False;
  ActiveInterpolationMode := imLinear;
  ActiveCoordinateOffset := 0;
  //&&&Units
  if Config.CoordinateSetup[0].InchDefault then begin
    SetUnits(25.4);
//    DisplayUnits := 25.4;
  end else begin
    SetUnits(1);
//    DisplayUnits := 1;
  end;

{  if not Config.CoordinateSetup[0].InchDefault then
    FUnits := 1 //&&& Units
  else
    FUnits := 25.4; }

  for I := 0 to FAxisCount - 1 do
    Axis[I].Reset;
  StartPermissiveTag.AsBoolean := True;
  HWBlocking := False;
  ForceBlocking := False;
  RewindComplete := True;
end;

function TFakeNCHAL.CreateConfig : TNC32Configure;
begin
  Config := TTurboPmacNCConfigure.Create;
  Result := Config;
end;

procedure TFakeNCHAL.Configure;
var I : Integer;
begin
  Config.ReadFromStrings;
  FAxisCount := Config.AxisCount[0];
  for I := 0 to AxisCount - 1 do begin
    AxisList.Add(TFakeAxis.Create(GetAxisNames[I+1]));
  end;
  Config.Output.SaveToFile(ProfilePath + NCPmacDirectory + 'NC.Txt');
end;

function TFakeNCHAL.GetAxis(Index : Integer) : TFakeAxis;
begin
  Result := TFakeAxis(AxisList[Index]);
end;

procedure TFakeNCHAL.SetAxisPositionTags(APList : TList); // List of TAxisPositionTag
var I : Integer;
begin
  with Config.CoordinateSetup[0] do begin
    for I := 0 to FAxisCount - 1 do begin
      AxisByDisplay[I].AxisPositionTag := TAxisPositionTag(APList[I]);
    end;
  end;
end;

procedure TFakeNCHAL.TestPoint;
begin
end;

procedure TFakeNCHAL.ExecutePartProgram;
var Instr : TNCInstructionRecord;
    NextBGLine : Integer;
    FM : TFakeMotion;
    Tmp : Single;
    Block : TRotaryBlock;

  procedure SetOffset;
  var I : Integer;
  begin
    for I := 0 to FAxisCount - 1 do begin
      TotalOffset[I] := LocalOffset[I] + CoordOffset[I] + ZeroOffsetAdjust.AsArrayDouble[I];
    end;
  end;

  procedure SetOffsetTable(Inst : TNCInstruction; aOffset : TUnaryDoubleArrayTag);
  var P : TSFAParameter;
      I : Integer;
      Ptr : Integer;
      Tmp : Single;
  begin
    FM.Instruction := Inst;
    FM.Parameters := TSFAParameters(SPP.InstructionList[PPBGInst + 1]);
    Ptr := 2;
    for P := Low(TSFAParameter) to High(TSFAParameter) do begin
      if P in FM.Parameters then begin
        I := Config.AxisIndexFromName(0, NCParamNames[P]);
        if I <> -1 then begin
          aOffset.Data^.DoubleArray[I] := TAbstractTag(SPP.InstructionList[PPBGInst + Ptr]).AsDouble;
        end;
        Inc(Ptr);
      end;
    end;

    for I := 0 to FAxisCount - 1 do begin
      LocalOffset[I] := G50.Data^.DoubleArray[I] +
                        G51.Data^.DoubleArray[I] +
                        G52.Data^.DoubleArray[I];
    end;

    SetOffset;

    FM.Parameters := Config.CoordinateSetup[0].LegalAxisParameters;
    for I := 0 to FAxisCount - 1 do begin
      Tmp := LocalOffset[I];
      FM.ParmList.Add(Pointer(Tmp));
    end;
    Block.IsMotion := True;
  end;

  procedure SetShiftOffset;
  var P : TSFAParameter;
      I : Integer;
      Ptr : Integer;
      Tmp : Single;
  begin
    ForceBlocking := True;
    Ptr := 2;
    if TSFAParameters(SPP.InstructionList[PPBGInst + 1]) <> [] then begin
      for P := Low(TSFAParameter) to High(TSFAParameter) do begin
        if P in TSFAParameters(SPP.InstructionList[PPBGInst + 1]) then begin
          I := Config.AxisIndexFromName(0, NCParamNames[P]);
          if I <> -1 then begin
             if not Config.IsRotary(Config.CoordinateSetup[0].AxisByName[P]) then begin
               FM.ParmList.Add(Pointer(Tmp));
               G92.Data^.DoubleArray[I]:=
                 TAbstractTag(SPP.InstructionList[PPBGInst + Ptr]).AsDouble * Units -
                 Config.CoordinateSetup[0].AxisByDisplay[I].AxisPositionTag.FValue[ptTarget];
             end
          end;
          Inc(Ptr);
        end;
      end;
    end else begin
      for I:= 0 to FAXisCount - 1 do begin
        G92.Data^.DoubleArray[I]:= 0;
      end;
    end;
    SetOffset;
    ForceBlocking := True;
  end;

  procedure SetCoordOffset(aOffset : Integer);
  var I : Integer;
      Tmp : Single;
  begin
    FM.Parameters := Config.CoordinateSetup[0].LegalAxisParameters;
    for I := 0 to FAxisCount - 1 do begin
      CoordOffset[I] := CoordSys[aOffset].Data^.DoubleArray[I];
    end;

    SetOffset;

    for I := 0 to FAxisCount - 1 do begin
      Tmp := CoordOffset[I];
      FM.ParmList.Add(Pointer(Tmp));
    end;
  end;

  procedure ClearCoordOffset;
  var I : Integer;
  begin
    FillChar(CoordOffset, Sizeof(CoordOffset), 0);
    for I := 0 to FAxisCount - 1 do begin
      LocalOffset[I] := 0;
    end;
    SetOffset;
  end;

  function NewBlock : TRotaryBlock;
  begin
    Result := TRotaryBlock.Create;
    Result.IsMotion := False;
  end;

  function NewFM : TFakeMotion;
  begin
    Result := TFakeMotion.Create;
    Result.Instruction := nfData;
    Result.Line := PPBGLine;
    Result.Sub := PPBGUnit; //SPP.SPPIndex;
    Block.Add(Result);
  end;

  procedure DoMCode;
  var MExt, P : Integer;
      STmp : string;
  begin
    FM.Instruction := nfMCode;
    FM.Parameters := [nfParamM];
    Tmp := TAbstractTag(SPP.InstructionList[PPBGInst + 1]).AsDouble;

    STmp := Format('%.4f', [Tmp - Round(Tmp)]);
    MExt := 0;
    P := Pos('.', STmp);
    if P > 0 then begin
      Delete(STmp, 1, P);
      MExt := StrToInt(STmp);
    end;

    MExtTag.AsInteger := MExt;

    case Round(Tmp) of
      0 : ForceBlocking := True;
      1 : ForceBlocking := True;
      30 : ForceBlocking := True;
    end;
    FM.ParmList.Add(Pointer(Tmp));
    Block.IsMotion := True;
  end;

  procedure DoSCode;
  begin
    FM.Instruction := nfSCode;
    FM.Parameters := [nfParamS];
    Tmp := TAbstractTag(SPP.InstructionList[PPBGInst + 1]).AsDouble;
    FM.ParmList.Add(Pointer(Tmp));
    Block.IsMotion := True;
  end;

  procedure DoTCode;
  begin
    FM.Instruction := nfTCode;
    FM.Parameters := [nfParamT];
    Tmp := TAbstractTag(SPP.InstructionList[PPBGInst + 1]).AsInteger;
    FM.ParmList.Add(Pointer(Tmp));
    Block.IsMotion := True;
  end;

  procedure DoFCode;
  begin
    FM.Instruction := nfFCode;
    FM.Parameters := [nfParamF];
    Tmp := TAbstractTag(SPP.InstructionList[PPBGInst + 1]).AsDouble;
    FM.ParmList.Add(Pointer(Tmp));
    Block.Delete(Block.Count - 1);
    Block.Insert(0, FM);
    Block.IsMotion := True;
  end;

  procedure DoGCode;
  begin
    FM.Instruction := nfGCode;
    FM.Parameters := [nfParamG];
    Tmp :=  TNCInstructionRecord(SPP.InstructionList[PPBGInst + 1]).NI;
    FM.ParmList.Add(Pointer(Tmp));
    case Round(Tmp) of
      20 : SetUnits(25.4);
      21 : SetUnits(1);
      70 : SetUnits(25.4);
      71 : SetUnits(1);
      53 : ClearCoordOffset;
      54 : SetCoordOffset(0);
      55 : SetCoordOffset(1);
      56 : SetCoordOffset(2);
      57 : SetCoordOffset(3);
      58 : SetCoordOffset(4);
      59 : SetCoordOffset(5);
      90 : Incremental := False;
      91 : Incremental := True;
    end;
    Block.IsMotion := True;
  end;

  procedure DoSFA;
  var I, X : Integer;
      P : TSFAParameter;
  begin
    FM.Instruction := nfSFA;
    FM.Parameters := TSFAParameters(SPP.InstructionList[PPBGInst + 1]);
    I := 2;
    for P := Low(TSFAParameter) to High(TSFAParameter) do begin
      if P in FM.Parameters then begin
        Tmp := TAbstractTag(SPP.InstructionList[PPBGInst + I]).AsDouble;
        X := Config.AxisIndexFromName(0, NCParamNames[P]);
        if X <> -1 then
          if not Incremental then
            Tmp := Tmp + TotalOffset[X] + ActiveShiftOffset[X];
        Inc(I);
        FM.ParmList.Add(Pointer(Tmp));
      end;
    end;
    Block.IsMotion := True;
  end;

  procedure DoDwell;
  begin
    FM.Instruction := nfDwell;
    FM.Parameters := TSFAParameters(SPP.InstructionList[PPBGInst + 1]);
    if FM.Parameters <> [] then begin
      Tmp := TAbstractTag(SPP.InstructionList[PPBGInst + 2]).AsDouble;
      FM.ParmList.Add(Pointer(Tmp));
    end;
    ForceBlocking := True;
    Block.IsMotion := True;
  end;

  procedure DoLineNumber;
  begin
    FM.Instruction := nfBStop;
    PPBGLine := Integer(SPP.InstructionList[PPBGInst + 1]);
    FM.Line := PPBGLine;
    if (RotaryList.Count >= LookAhead) or ForceBlocking then
      HWBlocking := True;
    RunBlockDelete := False;
  end;

  procedure DoReturn;
  begin
    RunReturn(NextBGLine);;
    FM.Instruction := nfReturn;
    FM.Line := PPBGLine;
    FM.Sub := PPBGUnit;
  end;

  procedure DoCall;
  begin
    FM.Instruction := nfDwell;
    FM.Parameters := [];
    Block.IsMotion := True;
    ForceBlocking := True;


    if not (RunBlockDelete and (ncsBlockDelete in NCStates)) then begin
      RunCall(NextBGLine);
      FM.Line := 0;
      FM.Sub := PPBGUnit;
    end;
  end;

  procedure DoBlockDelete;
  begin
    FM.Instruction := nfBlockDelete;
    RunBlockDelete := True;
  end;

  procedure DoAssign;
  begin
    if not (RunBlockDelete and (ncsBlockDelete in NCStates)) then begin
      RunAssign;
    end;
  end;


  procedure DoPluginGCode;
  var ECG: INCExtGCode;
  begin
    ECG:= INCExtGCode(SPP.InstructionList[PPBGInst + 1]);
    ECG.Execute;
  end;


var
  CountDown  : Integer;
begin
  CountDown := 1400;

  if RotaryList.Count = 0 then
    HWBlocking := False;

  if HWBlocking then
    Exit;

  if ncsSingleBlock in NCStates then
    LookAHead := 0
  else
    LookAHead := 1;

  HWSynch := False;
  if PrimeCycleStart then begin
    Include(FNCStates, ncsInCycle);
    PrimeCycleStart:= False;
  end;


  try
    Block := NewBlock;
    while not HWBlocking and not HWSynch and not ResetRewindRequest and (ncsIncycle in FNCStates) do begin

      FM := NewFM;
      Instr.Ptr := SPP.InstructionList[PPBGInst];
      NextBGLine := PPBGInst + Instr.ParameterCount;
      case Instr.Instruction of
        nfSCode : DoSCode;
        nfTCode : DoTCode;
        nfFCode : DoFCode;
        nfMCode : DoMCode;
        nfGCode : DoGCode;
        nfG50 : SetOffsetTable(nfG50, G50);
        nfG51 : SetOffsetTable(nfG51, G51);
        nfG52 : SetOffsetTable(nfG52, G52);
        nfG92 : SetShiftOffset;
        nfDwell : DoDwell;
        nfSFA : DoSFA;
        nfMacroCall,
        nfCall : DoCall;
        nfAssign : DoAssign;
        nfJump : NextBGLine := Integer(SPP.InstructionList[PPBGInst + 1]);
    //    nfData,           // Just data so skip
        nfBlockDelete : DoBlockDelete;
        nfIf : RunIF(NextBGLine);
        nfHash : RunHash(NextBGLine);
        nfIndexVar : RunIndexVar;
        nfICompare : RunICompare(NextBGLine);
        nfLineNumber : DoLineNumber;
        nfReturn : DoReturn;
        nfScriptCall : Synchronize(RunScriptCall);
        nfHWSynch : HWSynch := True;
        nfPlugin: DoPluginGCode;
      end;
      PPBGInst := NextBGLine;

      // Decide if we want the block in the rotary buffer
      // And then add or delete
      if FM.Instruction = nfBStop then begin
        if (ncsSingleBlock in NCStates) or Block.IsMotion then begin
          if ForceBlocking then begin
            HWBlocking:= True;
            ForceBlocking:= False;
          end;

          RotaryList.Add(Block);
          if not HWBlocking then
            Block := NewBlock;
        end else begin
          Block.Clean;
          if CountDown < 0 then begin
            Block.Free;
            Exit;
          end;
        end;
      end;
      Dec(CountDown);
    end;
  except
    on E : Exception do begin
      ErrorString := Format(E.Message + ' [%s:%d]', [SPP.Name, PPBGLine]);
      ResetRewind;
    end;
  end;
  if Block.Count = 0 then
     Block.Free;
end;

procedure TFakeNCHAL.SetJogMode(aMode : TJogModes);
begin
  FJogMode := aMode;
end;

procedure TFakeNCHAL.SetJogIncrement(aIncrement : Integer);
begin
  FJogIncrement := aIncrement;
end;


procedure TFakeNCHAL.SetJogPlus(aAxis : Integer; aOn : Boolean);
begin
  Axis[aAxis].JogPlus := aOn;
end;

procedure TFakeNCHAL.SetJogMinus(aAxis : Integer; aOn : Boolean);
begin
  Axis[aAxis].JogMinus := aOn;
end;

procedure TFakeNCHAL.SetCycleStart(aOn : Boolean);
begin
  if aOn and not (ncsInCycle in FNCStates) then begin
    ActiveNWord.AsString := '';
    //Include(FNCStates, ncsInCycle);
    PrimeCycleStart:= True;
    NormalTermination.AsBoolean := False;
  end;
end;

procedure TFakeNCHAL.SetFeedHold(aOn : Boolean);
begin
  if aOn then
    Include(FNCStates, ncsFeedHold);
end;

procedure TFakeNCHAL.SetCycleStop(aOn : Boolean);
begin
  if aOn then
    Exclude(FNCStates, ncsInCycle);
end;


procedure TFakeNCHAL.SetCycleAbort(aOn: Boolean);
begin
  if aOn then
    Exclude(FNCStates, ncsInCycle);
end;

function TFakeNCHAL.GetCycleAbort: Boolean;
begin
  Result:= False;
end;

procedure TFakeNCHAL.SetFeedOverride(aFeed : Double);
begin
  FeedOVR := aFeed;
end;

procedure TFakeNCHAL.SetFeedOverrideEnable(aOn : Boolean);
begin
  if aOn then
    Include(FNCStates, ncsFeedOverrideEnable)
  else
    Exclude(FNCStates, ncsFeedOverrideEnable);
end;

function TFakeNCHAL.GetTargetFeedrate : Double;
begin
  Result := FTargetFeedrate;
end;

function TFakeNCHAL.GetActualFeedrate : Double;
begin
  Result := 0;
end;

function TFakeNCHAL.GetJogPlus(aAxis : Integer) : Boolean;
begin
  Result := Axis[aAxis].JogPlus;
end;

function TFakeNCHAL.GetJogMinus(aAxis : Integer) : Boolean;
begin
  Result := Axis[aAxis].JogMinus;
end;

function TFakeNCHAL.GetCycleStart : Boolean;
begin
  Result := False;
end;

function TFakeNCHAL.GetFeedHold : Boolean;
begin
  Result := False;
end;

function TFakeNCHAL.GetCycleStop : Boolean;
begin
  Result := False;
end;

function TFakeNCHAL.GetFeedOverride : Double;
begin
  Result := FeedOVR;
end;

function TFakeNCHAL.GetAxisNames : string;
begin
  Result := Config.AxisNames[0];
end;

procedure TFakeNCHAL.UpdateHardware;
var I : Integer;
    AP : TAxisPosition;
begin
  for I := 0 to AxisCount - 1 do begin
    if Axis[I].JogPlus then begin
      Axis[I].Position := Axis[I].Position + Config.MotorSetup[I].MaximumJogFeed * (FeedOVR / 600000);
      Axis[I].TargetPosition := Axis[I].Position;
    end else if Axis[I].JogMinus then begin
      Axis[I].Position := Axis[I].Position - Config.MotorSetup[I].MaximumJogFeed * (FeedOVR / 600000);
      Axis[I].TargetPosition := Axis[I].Position;
    end;

    with Config.CoordinateSetup[0].AxisByDisplay[I].AxisPositionTag do begin
      AP[ptDisplay] := Axis[I].Position - ActiveOffset[I];
      AP[ptTarget] := Axis[I].TargetPosition - ActiveOffset[I];
      AP[ptDistToGo] := Axis[I].DistanceToGo;
      AP[ptMachine] := Axis[I].Position;
      AP[ptFerr] := 0;
      FValue := AP;

      Config.MetricStorageBias[I]:= 0;
      Config.MetricStorageCommand[I]:= Axis[I].TargetPosition;
    end;
  end;

  RotaryCount.AsInteger := RotaryList.Count;

  if RunBlock and (ncsIncycle in FNCStates) and not PrimeCycleStart then begin
    if ncsSingleBlock in FNCStates then begin
      Exclude(FNCStates, ncsInCycle);
      FProgramLine:= PeekProgramLine;
    end;
  end;
  if WriteMetricTimer.Expired then begin
    Config.WriteMetricStorage;
    WriteMetricTimer.Start;
  end;

end;

function TFakeNCHAL.PeekProgramLine: Integer;
var Block : TRotaryBlock;
    FM: TFakeMotion;
    I: Integer;
begin
  if RotaryList.Count > 0 then begin
    Block:= TRotaryBlock(RotaryList[0]);
    for I:= 0 to Block.Count - 1 do begin
      FM:= Block.GetMotion(I);
      if FM.Instruction = nfLineNumber then begin
        Result:= FM.Line;
        Exit;
      end;
    end;
  end else begin
    FProgramLine:= PPBGLine;
  end;
  Result:= FProgramLine;
end;

{ Returns true if the current block is complete }
function TFakeNCHAL.RunSynchroBlock(Block : TRotaryBlock): Boolean;
var I : Integer;
    Tmp : Single;
    FM : TFakeMotion;
begin
  FM := Block.Motion[0];
  Result:= False;
  case FM.Instruction of
    nfSFA : begin
      Dec(FM.TimeToComplete);
      if FM.TimeToComplete > 0 then begin
        for I := 0 to AxisCount - 1 do begin
          Axis[I].DistanceToGo := Axis[I].DistanceToGo - (Axis[I].DistanceToGo / FM.TimeToComplete);
          Axis[I].Position := Axis[I].TargetPosition - Axis[I].DistanceToGo;
        end;
      end else begin
        for I := 0 to AxisCount - 1 do begin
          Axis[I].Position := Axis[I].TargetPosition;
        end;
        Result:= True;
      end;
    end;

    nfMCode : begin
      if MCodeAFC.AsBoolean then begin
        MCode.AsInteger := 0;
        Result:= True;
      end;
    end;

    nfSCode : begin
      Result:= True;
    end;

    nfTCode : begin
      Result:= True;
    end;

    nfDwell : begin
      if FM.ParmList.Count > 0 then begin
        Tmp := Single(FM.ParmList[0]);
        Tmp := Tmp - (1 / SweepsPerSecond);
        FM.ParmList[0] := Pointer(Tmp);
      end else
        Tmp := -1;

      if Tmp < 0 then begin
        Result:= True;
      end;
    end;
  else
    Result:= True;
  end;
end;

procedure TFakeNCHAL.UpdateActiveOffset;
var I : Integer;
begin
  for I := 0 to FAxisCount - 1 do
    ActiveOffset[I] := ActiveLocalOffset[I] +
                       ActiveCoordOffset[I] +
                       ActiveShiftOffset[I] +
                       ZeroOffsetAdjust.AsArrayDouble[I];
end;


{ Returns true if no more blocks to process }
function TFakeNCHAL.RunBlock : Boolean;
var FM : TFakeMotion;
    Block : TRotaryBlock;
    Tmp : Double;
    DistToGo : Double;

  procedure DoSFA;
  var P : TSFAParameter;
      TTC : Double;
      I, Index : Integer;
  begin
    Index := 0;
    DistToGo := 0;
    for P := Low(TSFAParameter) to High(TSFAParameter) do begin
      if P in FM.Parameters then begin
        I := Config.AxisIndexFromName(0, NCParamNames[P]);
        if I <> -1 then begin
          Tmp := Single(FM.ParmList[Index]);
          Axis[I].StartPosition := Axis[I].Position;
          if ActiveIncremental then begin
            Axis[I].TargetPosition := Axis[I].Position + Tmp;
          end else begin
            Axis[I].TargetPosition := Tmp;
          end;
          Axis[I].DistanceToGo := Axis[I].TargetPosition - Axis[I].Position;
          DistToGo := DistToGo + (Axis[I].DistanceToGo * Axis[I].DistanceToGo);
        end;
        Inc(Index);
      end;
    end;
    DistToGo := Sqrt(DistToGo);
    case ActiveInterpolationMode of
      imRapid : TTC := (SweepsPerSecond * 60 * DistToGo) / 5000;
    else
      case ActiveInverseTimeFeed of
        False : if TargetFeedRate = 0 then
                  TTC := $7FFFFFFF
                else
                  TTC := (SweepsPerSecond * 60 * DistToGo) / TargetFeedRate;
        True : TTC := TargetFeedRate * SweepsPerSecond;
      else
        TTC := 0; // This is here just to make the compiler happy
      end;
    end;
    FM.TimeToComplete := Round(TTC);
    Synchro := True;
    Exit;
  end;

  procedure UpdateLocalOffset;
  var I : Integer;
  begin
    for I := 0 to FAxisCount - 1 do
      ActiveLocalOffset[I] := Single(FM.ParmList[I]);
    UpdateActiveOffset;
  end;

  procedure RemoveCoordOffset;
  begin
    ActiveGGroup.AsArrayInteger[10] := 53;
    FillChar(ActiveCoordOffset, Sizeof(TAxisOffsets), 0);
    FillChar(ActiveLocalOffset, Sizeof(TAxisOffsets), 0);
    UpdateActiveOffset;
  end;

  procedure SelectCoordOffset(aCoord : Integer);
  var I : Integer;
  begin
    ActiveGGroup.AsArrayInteger[10] := aCoord;
    for I := 0 to FAxisCount - 1 do
      ActiveCoordOffset[I] := Single(FM.ParmList[I + 1]);

    UpdateActiveOffset;
  end;

  procedure UpdateShiftOffset;
  var P : TSFAParameter;
      I, Index : Integer;
  begin
    Index := 0;
    if FM.Parameters = [] then begin
      for I := 0 to AxisCount - 1 do begin
        G92.AsArrayDouble[I] := 0;
        ActiveShiftOffset[I] := 0;
      end;
    end else begin
      for P := Low(TSFAParameter) to High(TSFAParameter) do begin
        if P in FM.Parameters then begin
          I := Config.AxisIndexFromName(0, NCParamNames[P]);
          if I <> -1 then begin
            Tmp := Single(FM.ParmList[Index]);
            G92.AsArrayDouble[I] := Axis[I].Position - Tmp;
            ActiveShiftOffset[I] := Axis[I].Position - Tmp - ZeroOffsetAdjust.AsArrayDouble[I];
          end;
          Inc(Index);
        end;
      end;
    end;
    UpdateActiveOffset;
    ForceBlocking := False;
  end;

  procedure SetActiveInterpolationMode(aMode : TInterpolationMode; aNum : Integer);
  begin
    ActiveGGroup.AsArrayInteger[0] := aNum;
    ActiveInterpolationMode := aMode;
  end;

  procedure SetActivePlaneSelection(aMode : TPlaneSelection; aNum : Integer);
  begin
    ActiveGGroup.AsArrayInteger[2] := aNum;
    ActivePlaneSelection := aMode;
  end;

  procedure SetActiveIncremental(aMode : Boolean; aNum : Integer);
  begin
    ActiveIncremental := aMode;
    ActiveGGroup.AsArrayInteger[4] := aNum;
  end;

  procedure SetInverseTimeFeed(aOn : Boolean; aNum : Integer);
  begin
    ActiveInverseTimeFeed := aOn;
    ActiveGGroup.AsArrayInteger[5] := aNum;
  end;

begin
  Result := False;

  if RotaryList.Count = 0 then
    Exit;

  Block := TRotaryBlock(RotaryList[0]);

  if Synchro then begin
    Result:= RunSynchroBlock(Block);
    if Result then begin
      Synchro:= False;
      HWBlocking:= False;
      Block.Free;
      RotaryList.Delete(0);
    end;
    Exit;
  end;

  while not Result and not Synchro do begin
    FM := Block.Motion[0];
    FProgramLine := FM.Line;
    PPHWUnit := FM.Sub;

    case FM.Instruction of
      nfGCode : begin
        case Round(Single(FM.ParmList[0])) of
          90 : SetActiveIncremental(False, 90);
          91 : SetActiveIncremental(True, 91);

          0 : SetActiveInterpolationMode(imRapid, 0);
          1 : SetActiveInterpolationMode(imLinear, 1);
          2 : SetActiveInterpolationMode(imCCW, 2);
          3 : SetActiveInterpolationMode(imCW, 3);
          6 : SetActiveInterpolationMode(imParabolic, 6);
          32 : SetActiveInterpolationMode(imThreadConstant, 32);
          33 : SetActiveInterpolationMode(imThreadIncreasing, 33);
          34 : SetActiveInterpolationMode(imThreadDecreasing, 34);
          72 : SetActiveInterpolationMode(imCWSpherical, 72);
          73 : SetActiveInterpolationMode(imCCWSpherical, 73);
          20 : ActiveGGroup.AsArrayInteger[7] := 20;
          21 : ActiveGGroup.AsArrayInteger[7] := 21;


          17 : SetActivePlaneSelection(psXY, 17);
          18 : SetActivePlaneSelection(psXZ, 18);
          19 : SetActivePlaneSelection(psYZ, 19);

          93 : SetInverseTimeFeed(True, 93);
          94 : SetInverseTimeFeed(False, 94);


          53 : RemoveCoordOffset;
          54..59 : SelectCoordOffset(Round(Single(FM.ParmList[0])));
        end;
      end;
      nfFCode : FTargetFeedRate := Single(FM.ParmList[0]);
      nfMCode :
        case Round(Single(FM.ParmList[0])) of
          0 : begin
            Exclude(FNCStates, ncsInCycle);
            ForceBlocking := False;
          end;

          1 : begin
            if OptionalStop then
              Exclude(FNCStates, ncsInCycle);
            ForceBlocking := False;
          end;

          30 : begin
            ResetRewind;
            NormalTermination.AsBoolean := True;
            ForceBlocking := False;
            Exclude(FNCStates, ncsInCycle);
            Exit;
          end;
        else
          MCode.AsInteger := Round(Single(FM.ParmList[0]));
          Synchro := True;
        end;

      nfSCode : begin
        SCode.AsInteger := Round(Single(FM.ParmList[0]));
        Synchro := True;
      end;

      nfTCode : begin
        TCode.AsInteger := Round(Single(FM.ParmList[0]));
        Synchro := True;
      end;

      nfG50,
      nfG51,
      nfG52 : UpdateLocalOffset;
      nfG92 : UpdateShiftOffset;

      nfBlockDelete : begin
        if ncsBlockDelete in NCStates then begin
          while Block.Count > 1 do begin
            Block.Motion[0].Free;
            Block.Delete(0);
          end;
          FProgramLine := Block.Motion[0].Line;
        end;
      end;
      nfDwell : Synchro := True;
      nfSFA : begin
        DoSFA;
      end;
      nfBStop : begin
      end;
    end;

    if not Synchro then begin
      if Block.Count > 0 then begin
        TObject(Block[0]).Free;
        Block.Delete(0);
      end;

      if Block.Count = 0 then begin
        Block.Free;
        RotaryList.Delete(0);
        Result:= RotaryList.Count = 0;
        if RotaryList.Count > 0 then
          Block := TRotaryBlock(RotaryList[0]);
      end;
    end;
  end;

  Result:= not Synchro;

  {
  if (RotaryList.Count < 5) and not ForceBlocking and
     not (ncsSingleBlock in FNCStates) then
    Blocking := False;
   }
end;

procedure TFakeNCHAL.SetRequestCNCMode(aMode : TCNCMode);
begin
  if not SysObj.NC.InCycle then begin
    FRequestCNCMode := aMode;
    FCNCModes := [aMode];
    Self.ResetRewind;
  end else
    SysObj.FaultInterface.SetFault(TCNC(TagOwner), CoreFaults[cfNoModeChangeWhileInCycle], [], nil);
//    SysObj.IO.SetFault(SysObj.NC, 'Mode change not permitted while in cycle', FaultInterlock, nil);
end;

function TFakeNCHAL.GetAxisType(Index : Integer) : TAxisType;
begin
  if Config.IsRotary(Config.CoordinateSetup[0].AxisByDisplay[Index]) then
    Result := atRotary
  else
    Result := atLinear;
end;

constructor TFakeMotion.Create;
begin
  inherited Create;
  ParmList := TList.Create;
  Inc(FakeMotionCount);
end;

destructor TFakeMotion.Destroy;
begin
  Dec(FakeMotionCount);
  ParmList.Free;
  inherited Destroy;
end;

constructor TFakeAxis.Create(aName : Char);
begin
  Name := aName;
end;

procedure TFakeAxis.Reset;
begin
  TargetPosition := Position;
  DistanceToGo := 0;
  StartPosition := Position;
  JogPlus := False;
  JogMinus := False;
end;


function TRotaryBlock.GetMotion(Index : Integer) : TFakeMotion;
begin
  Result := TFakeMotion(Items[Index]);
end;

constructor TRotaryBlock.Create;
begin
  inherited Create;
  Inc(RotaryBlockCount);
end;

destructor TRotaryBlock.Destroy;
begin
  while Count > 0 do begin
    TObject(Items[0]).Free;
    Delete(0);
  end;
  Dec(RotaryBlockCount);
  inherited;
end;

procedure TRotaryBlock.Clean;
begin
  while Count > 0 do begin
    Motion[0].Free;
    Self.Delete(0);
  end;
end;

function TFakeNCHAL.ResetRewindComplete: Boolean;
begin
  Result:= True;
  ForceBlocking := True;
  RewindComplete := True;
  if ExecuteMacroOnResetComplete then begin
      MacroExecute:= True;
      if SingleBlock then
        SingleBlock:= True;

      CycleStart:= True;
  end;

  ExecuteMacroOnResetComplete:= False;
  ForceBlocking:= True;
  StartPermissiveTag.AsBoolean := True;
  //PComm.Log('Resetrewind Complete');
end;

initialization
  RegisterNCDeviceClass(TFakeNCHAL);
end.
