unit NCProgram;

interface

uses
  Classes, SysUtils, CNC32, CNCTypes, NCMath, CoreCNC, ScriptTypeInfo, ACNCScript,
  ScriptFunctions, NC32Types, NC32ScriptRTL, GEOPlugin, CMMFileV1, IStringArray, INamedInterface;

type
  TNCInstruction = (
    nfSFA,            // SFASet, ListOfParameters
    nfAssign,         // Target + Source
    nfJump,           // Index
    nfCall,           // Target + SFASet, ListOfParameters
    nfIf,             // Boolean + FalseTarget
    nfHash,           // Source + HashTablePtr
    nfData,           // Just data so skip
    nfGCode,          // NI,NF
    nfMCode,          // Tag
    nfSCode,          // Tag
    nfTCode,          // Tag
    nfFCode,          // Tag
    nfG50,            // G50 Parameters
    nfG51,            // G51 Parameters
    nfG52,            // G52 Parameters
    nfG92,            // G92 Parameters
    nfIndexVar,       // Variable
    nfICompare,       // C1, C1, C1 <= C2 Target, C1 > C2 Target
    nfLineNumber,     // Line Number
    nfBlockDelete,    //
    nfDwell,          // SFASet, ListOfParameters
    nfScriptCall,     // ScriptMethodTag, ListOfParameters
    nfHWSynch,        // Force Hardware synch at runtime
    nfHardware,
    nfMacroCall,
    nfReturn,         //
//    nfBStart,         //
    nfPlugin,
    nfBStop           //
  );

  TNCInstructionRecord = packed record
    case Byte of
      0: (Instruction : TNCInstruction;
          ParameterCount : Word; );
      1 : (Ptr : Pointer);

      2: (NI, NF : Word);
  end;


  TNCLanguage = (
    nlIf,
    nlWhile,
    nlFor,
    nlRepeat,
    nlCase,
    nlCall,
    nlGoto,
    nlReturn,
    nlVar,
    nlConst,
    nlSynch,
    nlHardware,
    nlInvalid
  );

  TSubPartProgram = class;
  NullScript = class(TAbstractScriptMethodTag)
  protected
    procedure ExecuteSpecial; override;
    function GetAsInteger : Integer; override;
    function GetAsDouble : Double; override;
    function GetAsString : string; override;
  public
  end;


  TNC32PartProgramFile = class(TStringList)
  private
    FOnResolveVariable : TResolveVariable;
    FOnResolveFunction : TResolveFunction;
    SubList : TList;
    VariableList : TStringList;
    //IncludeFiles: TStringList;
    FLoaded : Boolean;
    OwnedTags : TList;
    FGEO : TGEOPlugin;
    FCMMFile : TCMMFile;
    Unique: Integer;
    M30 : TFloatTag;
    FalseTag: NullScript; // Just a throwaway tag
    function FindProgramStart(const S : string; Index : Integer) : Integer;
    function PositionOf(const S : string; C : Char; Index : Integer) : Integer;
    function GetSubRoutine(Index : Integer) : TSubPartProgram;
    function GetSubRoutineCount : Integer;
    function GetVariableCount : Integer;
    function GetVariable(aIndex : Integer) : TAbstractTag;
    procedure SetGEOPlugin(aGEO : TGEOPlugin);
    procedure SetCMMFile(aO : TCMMFile);
  protected
    procedure AddVariable(aTag : TAbstractTag);
  public
    TagOwner : TAbstractNC;
    ScriptLibrary : TACNCScriptLibrary;
    MacroExecutive : TNC32PartProgramFile;
    constructor Create(aTagOwner : TAbstractNC);
    destructor Destroy; override;
    property OnResolveVariable : TResolveVariable read FOnResolveVariable write FOnResolveVariable;
    property OnResolveFunction : TResolveFunction read FOnResolveFunction write FOnResolveFunction;
    procedure LoadFromStrings;
    procedure SaveToStrings;
    procedure Compile;
    property Variable[Index : Integer] : TAbstractTag read GetVariable;
    property VariableCount : Integer read GetVariableCount;
    property SubRoutine[Index : Integer] : TSubPartProgram read GetSubRoutine;
    property SubRoutineCount : Integer read GetSubroutineCount;
    function ResolveVariable(Sender : TObject; const aName : string) : TAbstractTag;
    function ResolveFunction(Sender : TObject; const aName, aFunc : string) : TAbstractTag;
    property Loaded : Boolean read FLoaded;
    function GetLineNumberFromInstruction(aSub, aIndex : Integer) : Integer;
    function NextInstruction(aSub, aIndex : Integer) : Integer;
    procedure Clean;
    property GEO : TGEOPlugin read FGEO write SetGEOPlugin;
    property CMMFile : TCMMFile read FCMMFile write SetCMMFile;
  end;

  TSubPartProgram = class(TObject, INCCompiler)
  private
    FStrings: TStringList;
    FPartProgram : TNC32PartProgramFile; // Reference to parent
    Tags : TStringList;                 // Locally created tags
    Labels : TStringList;
    GotoList : TStringList;
    ThisLine, BlockDepth : Integer;
    LastLine : Integer;
    FName : string;
    SFAParms : array [TSFAParameter] of TAbstractTag;
    FSPPIndex : Integer;
    LastAttemptedVariable : string;
    RefCount: Integer;
    ExtGCodeList: TList;
    function ReadStartToken(var aLine, aSub : string) : Boolean;
//    procedure ReadName;
    function NextLine : string;
    function NextLineNE : string; // No exception
    function StripComments : string;
    function CompileBlock(Term : string) : string;
    function CompileLanguage(var aLine : string; const Term : string) : Boolean;
    procedure CompileIf(var aLine : string);
    procedure CompileFor(var aLine : string);
    procedure CompileWhile(var aLine : string);
    procedure CompileRepeat(var aLine : string);
    procedure CompileCase(var aLine : string);
    procedure CompileCall(var aLine : string);
    procedure CompileGoto(var aLine : string);
    procedure CompileReturn(var aLine : string);
    procedure CompileVar(aLine : string);
    procedure CompileConst(aLine : string);
    procedure CompileGCodeLine(var aLine : string);
    procedure CompileNonGCodeLine(var aLine : string);
    procedure CompileHardware(var aLine : string);
    procedure CompileSynch(var aLine : string);
    function CompileSFA(var aLine : string; aLegal : TSFAParameters; Inst : TNCInstruction; Space : Integer) : Integer;
    procedure CompileEndLine(var aLine : string);
    procedure CompileAssignment(var aLine : string);
    function ReadExpression(var aLine : string) : string;
    function ReadExpressionTag(var aLine : string) : TAbstractTag;
    function ReadVariable(var aLine : string) : TAbstractTag;
    procedure ExpectedToken(var aLine : string; const Token : string);
    procedure ExpectedEndToken(var aLine : string; const Token : string);
    function AddInstruction(Inst : TNCInstruction; ParmCount : Integer) : Integer;
    function AddInstructionNL(Inst : TNCInstruction; ParmCount : Integer) : Integer;
    function AddLineNumber : Integer;
    function AddData(Data : Pointer) : Integer;
    procedure AddLabel(aLabel : string; aCodePtr : Integer);
    function NewMathParser(aLine : string) : TAbstractTag;
    function VerySimpleResolve(aLine : string; var Value :  Double) : Boolean;
    function VerySimpleInteger(aLine : string; var Value : Integer) : Boolean;
    function SimpleResolve(aLine : string) : TAbstractTag;
    function ResolveVariable(Sender : TObject; const aName : string) : TAbstractTag;
    function ResolveFunction(Sender : TObject; const aName, aFunc : string) : TAbstractTag;
    function ResolveArray(Sender : TObject; const aName, aFunc : string) : TAbstractTag;
    function GenerateArrayAccessor(aTag : TArrayTag; Suffix : string) : TArrayAccessTag;
    procedure DoMethodCompile(Sm : TAbstractScriptMethodTag; aFunc : string);
    function DoParameterCompile(var aFunc : string) : TAbstractTag;
    procedure Clean;
    procedure ExpandMDTVariables(Variables: string);
  public
    VariableList : TStringList; // accessed at runtime
    RunTimeParameters : TSFAParameters; // Used at runtime
    InstructionList : TList; // Public because execution of PP occurs elsewhere
    procedure SetName;
    constructor Create(aPartProgram : TNC32PartProgramFile; aIndex : Integer);
    procedure Compile;
    destructor Destroy; override;
    procedure AddTag(aRef : string; aTag : TAbstractTag);
    property Name : string read FName;
    property SPPIndex : Integer read FSPPIndex;
    property NWords : TStringList read Labels;
    property PartProgram : TNC32PartProgramFile read FPartProgram;
    property Strings: TStringList read FStrings;

    function ResolveNCVariable(TagValue: WideString): TTagRef;
    function QueryInterface(const IID: TGUID; out Obj): HResult; stdcall;
    function _AddRef: Integer; stdcall;
    function _Release: Integer; stdcall;
  end;

  TNCAssignedTag = class(TMethodTag)
  protected
    function GetAsInteger : Integer; override;
    function GetAsBoolean: Boolean; override;
  public
    Parameter : TSFAParameter;
    SubProgram : TSubPartProgram;
    constructor Create(aOwner : TTagOwner; aName : string); override;
  end;

procedure CreatePartProgramStrings;

var
  LegalAxisNames : TSFAParameters;

implementation

{resourcestring
  MissingOpenParentheses = 'Missing open parenthases';
  MismatchedParentheses = 'Mismatched parentheses';
  ExpectedTokenFmt = 'Expected Token [%s]';
  UnExpectedTokenFmt = 'UnExpected Token [%s]';
  GCodesMustStartLine = 'GCodes must be at the start of the line';
  MCodesMustEndLine = 'MCodes must be set at the end of the line';
  DuplicateParameter = 'Duplicate Parameter';
  GMCodeValueOutOfRange = 'G or M Code value out of range';
  InvalidGCodeValue = 'Invalid GCode value';
  UnexpectedCharactersFmt = 'Unexpected characters at end of line "%s"';
  UnknownVariableFmt = 'Unknown Variable "%s"';
  InvalidAssignmentTargetFmt = 'Variable "%s" Is an invalid assignment target';
  VariableIsNotArrayFmt = 'Variable "%s" is not an array';
  UnexpectedEndOfFile = 'Unexpected end of file';
  MissingStartDesignator = 'Missing start designator "%"';
  CaseRequiresColon = 'Each instance of case requires a colon';
  CaseSimpleInteger = 'Each instance of a case must be a simple integer constant';
  InvalidIntegerConversion = 'Data is not a valid integer';
  NoTargetForGotoFmt = 'No target for goto [%s]';
  UnknownVariableTypeFmt = 'Unknown Variable type [%s]';
  UnknownGCodeFmt = 'Unknown GCode [G%d]';
  InvalidParameterFmt = 'Invalid Parameter [%s]';
  GotoTargetNotFoundFmt = 'Target "%s" for Goto not found';
  ConstExpressionRequiresEquateFmt = 'Constant expression [%s] requires "="';
  InvalidCharactersFmt = 'Name "%s" contains invalid characters';
  ValueIsNotNumericFmt = 'Value "%s" is not numeric';
  ErrorAtLineFmt = 'Error [%s:%d] %s';
  MathErrorAtLineFmt = 'Math Error [%s:%d] %s';
  UnknownErrorAtLineFmt = 'Unknown Error [%s:%d] %s';
  ArraySuffixMalformedFmt = 'Array [%s] Suffix malformed';
  UnknownFunctionFmt = 'Unknown Function [%s]';
  VariableIsReadOnlyFmt = 'Variable [%s] is Read only';
  InvalidLabelNameFmt = 'Invalid format for Label [%s]';
  NameTooShortFmt = 'Variable name to Short "%s"';
  IllegalNameFmt = 'Illegal Variable Name "%s"';
  UnknownSubProgramFmt = 'Unknown subprogram "%s"';
  FollowingCrapFmt = 'Unexpected text [%s]';
  NotEnoughParametersFmt = 'Not enough parameters [%s]';
  DuplicateLabelDefinedFmt = 'Label defined more than once [%s]';
 }
const
  BlockDeleteChar = '/';
  CommentChar = ';';
  SubRoutineToken = '%';

  AxisParameters = [ nfParamX..nfParamW ];

  NonGCodeParameters = [   nfParamX..nfParamW,
                           nfParamI..nfParamK,
                           nfParamR,
                           nfParamP
  ];

  StdParameters = [ nfParamA..nfParamE,
                    nfParamH..nfParamL,
                    nfParamP..nfParamR,
                    nfParamU..nfParamZ
  ];

  AllParameters = [ Low(TSFAParameter)..High(TSFAParameter)];


  EndLineParameters = [    nfParamF,
                           nfParamS,
                           nfParamT,
                           nfParamM
  ];


  NCLexicon : array [TNCLanguage] of PChar = (
    'IF',
    'WHILE',
    'FOR',
    'REPEAT',
    'CASE',
    'CALL',
    'GOTO',
    'RETURN',
    'VAR',
    'CONST',
    'SYNCH',
    'HW',
    ' '
  );

var
  // Language Strings
  MissingOpenParentheses,
  MismatchedParentheses,
  ExpectedTokenFmt,
  UnExpectedTokenFmt,
  GCodesMustStartLine,
  MCodesMustEndLine,
  DuplicateParameter,
  GMCodeValueOutOfRange,
  InvalidGCodeValue,
  UnexpectedCharactersFmt,
  UnknownVariableFmt,
  InvalidAssignmentTargetFmt,
  VariableIsNotArrayFmt,
  UnexpectedEndOfFile,
  CaseRequiresColon,
  CaseSimpleInteger,
  InvalidIntegerConversion,
  NoTargetForGotoFmt,
  UnknownVariableTypeFmt,
  InvalidParameterFmt,
  GotoTargetNotFoundFmt,
  ConstExpressionRequiresEquateFmt,
  InvalidCharactersFmt,
  ValueIsNotNumericFmt,
  ErrorAtLineFmt,
  MathErrorAtLineFmt,
  UnknownErrorAtLineFmt,
  ArraySuffixMalformedFmt,
  UnknownFunctionFmt,
  VariableIsReadOnlyFmt,
  InvalidLabelNameFmt,
  NameTooShortFmt,
  IllegalNameFmt,
  UnknownSubProgramFmt,
  FollowingCrapFmt,
  UnknownGCodeFmt,
  NotEnoughParametersFmt,
  MissingStartDesignator,
  DuplicateLabelDefinedFmt : string;

  // It is assumed all lines may include optional block delete and line number
  // A line without preparatory codes may only contain
  // [Axisnames] F S T M I J K
  // Modal gcodes of the same group may not be mixed on a line
  // Unique G Codes may not be mixed with anything except F S T M
  //
  // A Axis name
  // B Axis name
  // C Axis name
  // D Undefined
  // E Undefined
  // F Feedrate      fixed
  // G Prep code     fixed
  // H Undefined
  // I Circle centre
  // J Circle center
  // K Circle center
  // L Undefined
  // M Misc code     fixed
  // N Line number   fixed
  // O Reserved      fized
  // P Undefined
  // Q Undefined
  // R Radius
  // S Spindle
  // T Tool
  // U Axis name
  // V Axis name
  // W Axis name
  // X Axis name
  // Y Axis name
  // Z Axis name
  //
  // Parameters passed as axisnames will be translated to machine position unless
  // they are programmed using an undefined unique GCode
  //
  // Parameters marked as fixed are treated as is and will not be used for parameter passing
  //
function IsAlpha(aChar : Char) : Boolean;
begin
  case aChar of
    'a'..'z',
    'A'..'Z',
    '_' : Result := True;
  else
    Result := False;
  end;
end;

function IsNumeric(aChar : Char) : Boolean;
begin
  case aChar of
    '0'..'9', '.', '-', '+' : Result := True;
  else
    Result := False;
  end;
end;

function IsNumber(aChar : Char) : Boolean;
begin
  case aChar of
    '0'..'9', '.' : Result := True;
  else
    Result := False;
  end;
end;

procedure CreatePartProgramStrings;
begin
  MissingOpenParentheses  := SysObj.Localized.GetString('$MissingOpenParentheses');
  MismatchedParentheses   := SysObj.Localized.GetString('$MismatchedParentheses');
  ExpectedTokenFmt        := SysObj.Localized.GetString('$ExpectedTokenFmt');
  UnExpectedTokenFmt      := SysObj.Localized.GetString('$UnExpectedTokenFmt');
  GCodesMustStartLine     := SysObj.Localized.GetString('$GCodesMustStartLine');
  MCodesMustEndLine       := SysObj.Localized.GetString('$MCodesMustEndLine');
  DuplicateParameter      := SysObj.Localized.GetString('$DuplicateParameter');
  GMCodeValueOutOfRange   := SysObj.Localized.GetString('$GMCodeValueOutOfRange');
  InvalidGCodeValue       := SysObj.Localized.GetString('$InvalidGCodeValue');
  UnexpectedCharactersFmt := SysObj.Localized.GetString('$UnexpectedCharactersFmt');
  UnknownVariableFmt      := SysObj.Localized.GetString('$UnknownVariableFmt');
  InvalidAssignmentTargetFmt := SysObj.Localized.GetString('$InvalidAssignmentTargetFmt');
  VariableIsNotArrayFmt   := SysObj.Localized.GetString('$VariableIsNotArrayFmt');
  UnexpectedEndOfFile     := SysObj.Localized.GetString('$UnexpectedEndOfFile');
  MissingStartDesignator  := SysObj.Localized.GetString('$MissingStartDesignator');
  CaseRequiresColon       := SysObj.Localized.GetString('$CaseRequiresColon');
  CaseSimpleInteger       := SysObj.Localized.GetString('$CaseSimpleInteger');
  InvalidIntegerConversion:= SysObj.Localized.GetString('$InvalidIntegerConversion');
  NoTargetForGotoFmt      := SysObj.Localized.GetString('$NoTargetForGotoFmt');
  UnknownVariableTypeFmt  := SysObj.Localized.GetString('$UnknownVariableTypeFmt');
  UnknownGCodeFmt         := SysObj.Localized.GetString('$UnknownGCodeFmt');
  InvalidParameterFmt     := SysObj.Localized.GetString('$InvalidParameterFmt');
  GotoTargetNotFoundFmt   := SysObj.Localized.GetString('$GotoTargetNotFoundFmt');
  ConstExpressionRequiresEquateFmt:= SysObj.Localized.GetString('$ConstExpressionRequiresEquateFmt');
  InvalidCharactersFmt    := SysObj.Localized.GetString('$InvalidCharactersFmt');
  ValueIsNotNumericFmt    := SysObj.Localized.GetString('$ValueIsNotNumericFmt');
  ErrorAtLineFmt          := SysObj.Localized.GetString('$ErrorAtLineFmt');
  MathErrorAtLineFmt      := SysObj.Localized.GetString('$MathErrorAtLineFmt');
  UnknownErrorAtLineFmt   := SysObj.Localized.GetString('$UnknownErrorAtLineFmt');
  ArraySuffixMalformedFmt := SysObj.Localized.GetString('$ArraySuffixMalformedFmt');
  UnknownFunctionFmt      := SysObj.Localized.GetString('$UnknownFunctionFmt');
  VariableIsReadOnlyFmt   := SysObj.Localized.GetString('$VariableIsReadOnlyFmt');
  InvalidLabelNameFmt     := SysObj.Localized.GetString('$InvalidLabelNameFmt');
  NameTooShortFmt         := SysObj.Localized.GetString('$NameTooShortFmt');
  IllegalNameFmt          := SysObj.Localized.GetString('$IllegalNameFmt');
  UnknownSubProgramFmt    := SysObj.Localized.GetString('$UnknownSubProgramFmt');
  FollowingCrapFmt        := SysObj.Localized.GetString('$FollowingCrapFmt');
  NotEnoughParametersFmt  := SysObj.Localized.GetString('$NotEnoughParametersFmt');
  DuplicateLabelDefinedFmt:= SysObj.Localized.GetString('$DuplicateLabelDefinedFmt');
end;

function NCLanguage(aToken : string) : TNCLanguage;
begin
  for Result := Low(TNCLanguage) to High(TNCLanguage) do begin
    if NCLexicon[Result] = aToken then
      Exit;
  end;

  Result := nlInvalid;
end;

constructor TSubPartProgram.Create(aPartProgram : TNC32PartProgramFile; aIndex : Integer);
var SFA : TSFAParameter;
    Tmp : string;
begin
  inherited Create;
  FStrings:= TStringList.Create;
  FPartProgram := aPartProgram;
  FSPPIndex := aIndex;
  ExtGCodeList:= TList.Create;

  Tags := TStringList.Create;;
  Labels := TStringList.Create;
  GotoList := TStringList.Create;
  InstructionList := TList.Create;
  VariableList := TStringList.Create;
  for SFA := Low(TSFAParameter) to High(TSFAParameter) do begin
    Tmp := '_' + NCParamNames[SFA];
    VariableList.AddObject(Tmp, TFloatTag.Create(PartProgram.TagOwner, Tmp));
  end;
  Clean;
end;

destructor TSubPartProgram.Destroy;
begin
  FStrings.Free;
  Clean;
  Tags.Free;
  Labels.Free;
  GotoList.Free;
  InstructionList.Free;

  while VariableList.Count > 0 do begin
    VariableList.Objects[0].Free;
    VariableList.Delete(0);
  end;

  ExtGCodeList.Free;
  VariableList.Free;
  inherited Destroy;
end;

procedure TSubPartProgram.Clean;
begin
  ThisLine := 0;
  BlockDepth := 0;
  LastLine := -1;
  InstructionList.Clear;

  while ExtGCodeList.Count > 0 do begin
    INCExtGCode(ExtGCodeList[0]).Cleanup;
    ExtGCodeList.Delete(0);
  end;

  while Tags.Count > 0 do begin
    Tags.Objects[0].Free;
    Tags.Delete(0);
  end;

  GotoList.Clear;
  Labels.Clear;
end;

function TSubPartProgram.ResolveVariable(Sender : TObject; const aName : string) : TAbstractTag;
var Func, Index : string;
    I : Integer;
begin
  I := VariableList.IndexOf(aName);
  if I <> -1 then begin
    Result := TAbstractTag(VariableList.Objects[I]);
    Exit;
  end;

  if aName[1] = '#' then begin
    for I := 1 to Length(aName) do begin
      if IsNumber(aName[I]) then begin
        Func := Copy(aName, 1, I - 1);
        Index := Copy(aName, I, Length(aName));
        Result := ResolveArray(Self, Func, Index);
        Exit;
      end;
    end;
  end;

  Result := PartProgram.ResolveVariable(Self, aName);
end;

function TSubPartProgram.ResolveFunction(Sender : TObject; const aName, aFunc : string) : TAbstractTag;
var MethodTag : TAbstractSCriptMethodTag;
    P : Integer;
begin
  if aName = 'ASSIGNED' then begin
    P := VariableList.IndexOf(Trim(aFunc));
    if P <> -1 then begin
      Result := TNCAssignedTag.Create(PartProgram.TagOwner, 'Assigned' + aFunc);
      TNCAssignedTag(Result).Parameter := TSFAParameter(P);
      TNCAssignedTag(Result).SubProgram := Self;
      AddTag(Result.Name, Result);
    end else
      raise EGCodeCompile.Create('Invalid parameter for assigned: ' + aFunc);
  end else if aName = 'MDTEXPAND' then begin
    ExpandMDTVariables(aFunc);
    Result:= FPartProgram.FalseTag;
  end else begin
    Result := PartProgram.ResolveFunction(Sender, aName, aFunc);
    if Assigned(Result) and (Result is TAbstractScriptMethodTag) then begin
      MethodTag := Result as TAbstractScriptMethodTag;
      DoMethodCompile(MethodTag, aFunc);
    end else
      raise EGCodeCompile.CreateFmt(UnknownFunctionFmt, [aName]);
  end;
end;

procedure TSubPartProgram.ExpandMDTVariables(Variables: string);
var T: IACNC32StringArray;
    I: Integer;
    Tag: TAbstractTag;
    N: WideString;
    Field, Table, Prefix: string;
begin
  Table := CNCTypes.ReadDelimitedQuote(Variables);
  Prefix := CNCTypes.ReadDelimitedQuote(Variables);
  SysObj.StringArraySet.UseArray(Table, nil, T);

  if Assigned(T) then begin
    for I:= 0 to T.FieldCount - 1 do begin
      T.FieldNameAtIndex(I, N);
      Field:= N;
      //CNC32.TokenizeString(Field);
      Field:= UpperCase(Prefix + Field);
      Tag:= FPartProgram.ResolveVariable(Self, Field);

      if not Assigned(Tag) then begin
        Tag:= TStringTag.Create(PartProgram.TagOwner, Field);
        FPartProgram.AddVariable(Tag);
      end;
    end;
  end else begin
    raise Exception.Create('MDTExpand: Table does not exist: ' + Table);
  end
end;


procedure TSubPartProgram.DoMethodCompile(Sm : TAbstractScriptMethodTag; aFunc : string);
var
  I : Integer;
  aTag : TAbstractTag;
  Parameters : TList;
begin
  Parameters := TList.Create;
  try
    for I := 0 to Sm.Prototype.Count - 1 do begin
      aTag := DoParameterCompile(aFunc);
      if aTag = nil then
        raise EGCodeCompile.CreateFmt(NotEnoughParametersFmt, [Sm.Name]);
      Parameters.Add(aTag);
    end;
    if Trim(aFunc) <> '' then
      raise EGCodeCompile.CreateFmt(FollowingCrapFmt, [aFunc]);
    AddInstruction(nfScriptCall, 2 + Sm.Prototype.Count);
    AddData(Sm);
    for I := 0 to Sm.Prototype.Count - 1 do begin
      if (Sm.Prototype[I].ParmType = passByReference) and
         not TAbstractTag(Parameters[I]).IsWritable then
         raise EACNCScriptError.CreateFmt(ParameterIsVarFormat, [Sm.Prototype[I].Name]);
      AddData(Parameters[I]);
    end;

  finally
    Parameters.Free;
  end;
end;


// Entry Conditions
// The method name and the outer parentheses have been read
//
// This function must Read each comma seperated parameter note that
// Commas that are nested in brackets are ignored.
//
// Do not assume S = Script
function TSubPartProgram.DoParameterCompile(var aFunc : string) : TAbstractTag;
var PareCount, BraceCount : Integer;
    Token : Char;
    Working : string;
    Index : Integer;
begin
  Parecount := 0;
  BraceCount := 0;
  Index := 1;
  Result := nil;

  if aFunc = '' then
    Exit;

  repeat
    Token := aFunc[Index];
    if (Token = ')') then
      Dec(PareCount)
    else if (Token = '(') then
      Inc(PareCount)
    else if (Token = ']') then
      Dec(BraceCount)
    else if (Token = '[') then
      Inc(BraceCount);

    if BraceCount < 0 then
      raise EGCodeCompile.Create(ImbalancedSquareBracesLang);

    Inc(Index);
  until (Index > Length(aFunc)) or
        ((Token = ',') and (PareCount = 0) and (BraceCount = 0));


  if Index <= Length(aFunc) then
    Working := System.Copy(aFunc, 1, Index - 2)
  else
    Working := aFunc;

  System.Delete(aFunc, 1, Index - 1);
  Result := NewMathParser(Working);
end;

function TSubPartProgram.ResolveArray(Sender : TObject; const aName, aFunc : string) : TAbstractTag;
begin
  Result := ResolveVariable(Sender, aName);
  if not Assigned(Result) then
    raise EGCodeCompile.CreateFmt(UnknownVariableFmt, [aName]);
  if not (Result is TArrayTag) then
    raise EGCodeCompile.CreateFmt(VariableIsNotArrayFmt, [aName]);

  Result := GenerateArrayAccessor(TArrayTag(Result), aFunc)
end;

function TSubPartProgram.GenerateArrayAccessor(aTag : TArrayTag; Suffix : string) : TArrayAccessTag;
var I, Index : Integer;
    Working : string;
    Token : Char;
    MPCompile : string;
    Comma : Integer;
begin
  MPCompile := '';
  Working := '';

  Index := 1;

  for I := 0 to aTag.OrdinateCount - 1 do begin
    if Length(Suffix) < Index then
      raise EACNCScriptError.CreateFmt(IncorrectNumberOfIndices, [aTag.OrdinateCount, aTag.Name]);

    Comma := -1;
    while (Comma < 0) and (Length(Suffix) >= Index) do begin
      Token := Suffix[Index];
      case Token of
        '(', '[' : Dec(Comma);
        ')', ']', ',' : Inc(Comma);
      end;
      if (Comma < 0) or (Token <> ',') then
        Working := Working + Token;
      Inc(Index);
    end;
    if I = 0 then
      MPCompile := Working
    else
      MPCompile := MPCompile + '+((' + Working + ')*' + IntToStr(aTag.IndexOfOrdinate(I)) + ')';

    Working := '';
  end;

  Result := TArrayAccessTag.Create(PartProgram.TagOwner, aTag.Name + '[' + MPCompile + ']');
  Result.DataSource := aTag;
  Result.IsOwned := aTag.IsOwned;
  AddTag(Result.Name, Result);
  Result.Index := NewMathParser(MPCompile);
end;

// ReadToken uses the following tokenizing rules
// if any white space or non or parenthesis are encountered the that substring
// is returned.
//
// In the event of the first character being being alpha and the second
// numeric then all subsequent numerics are read and that substring returned
// and the function returns True.
function TSubPartProgram.ReadStartToken(var aLine, aSub : string) : Boolean;
  function IsSFAStart(C : Char) : Boolean;
  begin
    case C of
      '0'..'9',
      '.', '(',
      '+', '-' : Result := True;
    else
      Result := False;
    end;
  end;

var I : Integer;
    PareCount : Integer;
begin
  aLine := TrimLeft(aLine);
  Result := False;

  if (Length(aLine) > 1) and IsAlpha(aLine[1]) then begin
     if IsSFAStart(aLine[2]) then begin
       Result := True;
       PareCount := 0;
       for I := 2 to Length(aLine) do begin
         case aLine[I] of
           '(' : Inc(PareCount);
           ')' : Dec(PareCount);
         else
           if (PareCount = 0) and not IsNumeric(aLine[I]) then begin
             aSub := System.Copy(aLine, 1, I-1);
             System.Delete(aLine, 1, I-1);
             Exit;
           end;
         end;
       end;
       aSub := aLine;
       aLine := '';
       Exit;
     end;
  end;

  for I := 1 to Length(aLine) do begin
    case aLine[I] of
      '.', '_',
      'a'..'z',
      'A'..'Z',
      '0'..'9' : Continue;
    else
      aSub := System.Copy(aLine, 1, I-1);
      System.Delete(aLine, 1, I-1);
      Exit;
    end;
  end;
  aSub := aLine;
  aLine := '';
end;

constructor TNC32PartProgramFile.Create(aTagOwner : TAbstractNC);
begin
  inherited Create;
  TagOwner := aTagOwner;
  SubList := TList.Create;
  VariableList := TStringList.Create;
  //IncludeFiles:= TStringList.Create;
  ScriptLibrary := TACNCScriptLibrary.Create(TagOwner);
  ScriptLibrary.RTL := TNC32ScriptRTL.Create(TagOwner);
  OwnedTags := TList.Create;
  M30 := TFloatTag.Create(TagOwner, 'M30');
  M30.AsDouble := 30;
  M30.IsConstant := True;
  FalseTag:= NullScript.Create(TagOwner, 'False');
  FalseTag.IsConstant:= True;
end;

destructor TNC32PartProgramFile.Destroy;
begin
  Clean;
  SubList.Free;
  VariableList.Free;
  ScriptLibrary.Free;
  //IncludeFiles.Free;
  OwnedTags.Free;
  M30.Free;
  FalseTag.Free;
  inherited Destroy;
end;

procedure TNC32PartProgramFile.Clean;
begin
  while SubList.Count > 0 do begin
    TSubPartProgram(SubList[0]).Free;
    SubList.Delete(0);
  end;

  while VariableList.Count > 0 do begin
    VariableList.Objects[0].Free;
    VariableList.Delete(0);
  end;

  while OwnedTags.Count > 0 do begin
    if TAbstractTag(OwnedTags[0]).IsWritable then
      TAbstractTag(OwnedTags[0]).IsOwned := False;
    OwnedTags.Delete(0);
  end;

  //IncludeFiles.Clear;

  ScriptLibrary.Clean;
end;

procedure TNC32PartProgramFile.SetGEOPlugin(aGEO : TGEOPlugin);
begin
  TNC32ScriptRTL(ScriptLibrary.RTL).GEO := aGEO;
end;

procedure TNC32PartProgramFile.SetCMMFile(aO : TCMMFile);
begin
  TNC32ScriptRTL(ScriptLibrary.RTL).CMMFile := aO;
end;

function TNC32PartProgramFile.GetVariableCount : Integer;
begin
  Result := VariableList.Count;
end;

// Returns the next instruction in the partprogram
function TNC32PartProgramFile.NextInstruction(aSub, aIndex : Integer) : Integer;
var Inst : TNCInstructionRecord;
    SPP : TSubPartProgram;
begin
  SPP := Subroutine[aSub];
  Inst := TNCInstructionRecord(SPP.InstructionList[aIndex]);
  Result := aIndex + Inst.ParameterCount;
end;

function TNC32PartProgramFile.GetLineNumberFromInstruction(aSub, aIndex : Integer) : Integer;
var Inst : TNCInstructionRecord;
    SPP : TSubPartProgram;
begin
  SPP := Subroutine[aSub];
  Inst := TNCInstructionRecord(SPP.InstructionList[aIndex]);
  if Inst.Instruction = {Ord}(nfLineNumber) then
    Result := Integer(SPP.InstructionList[aIndex + 1])
  else
    raise EGCodeRead.Create('Run-time profesional error, malformed compiled part program');
end;

function TNC32PartProgramFile.GetVariable(aIndex : Integer) : TAbstractTag;
begin
  Result := TAbstractTag(VariableList.Objects[aIndex]);
end;


function TNC32PartProgramFile.PositionOf(const S : string; C : Char; Index : Integer) : Integer;
var Hit : Integer;
    InQuote: Boolean;
    InComment: Boolean;
begin
  Hit := 0;
  InQuote:= False;
  InComment:= False;
  for Result := 1 to Length(S) do begin
    if S[Result] = '''' then
      InQuote:= not InQuote;

    if S[Result] = CommentChar then
      InComment:= True;

    if S[Result] = #$0a then begin
      InComment:= False;
      InQuote:= False;
    end;

    if (S[Result] = C) and not InQuote and not InComment then begin
      Inc(Hit);
      if Hit = Index then
        Exit;
    end;
  end;
  Result := 0;
end;

procedure TNC32PartProgramFile.AddVariable(aTag : TAbstractTag);
begin
  VariableList.AddObject(aTag.Name, aTag);
end;

function TNC32PartProgramFile.GetSubRoutine(Index : Integer) : TSubPartProgram;
begin
  if Index >= 0 then
    Result := TSubPartProgram(SubList[Index])
  else if not Assigned(MacroExecutive) then
    Result := TSubPartProgram(SubList[-(Index + 1)])
  else
    Result := Self.MacroExecutive.SubRoutine[-(Index + 1)];
end;

function TNC32PartProgramFile.GetSubRoutineCount : Integer;
begin
  Result := SubList.Count;
end;

function TNC32PartProgramFile.ResolveVariable(Sender : TObject; const aName : string) : TAbstractTag;
var I : Integer;
begin
  Result := nil;
  I := VariableList.IndexOf(aName);
  if I <> -1 then begin
    Result := TAbstractTag(VariableList.Objects[I]);
    Exit;
  end;

  if Assigned(OnResolveVariable) then
    Result := OnResolveVariable(Sender, aName);

  if Assigned(Result) then begin
    if not Result.IsOwned then begin
      Result.IsOwned := True;
      OwnedTags.Add(Result);
    end;
  end;
end;

var LocalShit : String;

function TNC32PartProgramFile.ResolveFunction(Sender : TObject; const aName, aFunc : string) : TAbstractTag;
begin
  Result := ScriptLibrary.FindTypeList(aName, aFunc);
  if not (Result is TAbstractScriptMethodTag) then
    Result := nil;
end;

function TNC32PartProgramFile.FindProgramStart(const S : string; Index : Integer) : Integer;
begin
  {
  Result := PositionOf(S, '%', Index);
  I := -1;
  while Result + I > 1 do begin
    case S[Result + I] of
      #$0d, #$0a : Exit;
      #$09, ' ' : Dec(I);
    else
      Result := FindProgramStart(S, Index + 1);
      Exit;
    end;
    Dec(I);
  end;
  }
  Result:= PositionOf(S, '%', Index);
end;

procedure TNC32PartProgramFile.LoadFromStrings;
  function ResolveInclude(Inc: string): string;
  begin
    if Length(Inc) < 1 then
      raise EGCodeRead.Create('Include filename not supplied: ');
    if FileExists(Inc) then begin
      Result:= Inc;
      Exit;
    end;

    if FileExists(CNC32.LocalPath + 'NCInclude\' + Inc) then begin
      Result:= CNC32.LocalPath + 'NCInclude\' + Inc;
      Exit;
    end;

    if FileExists(CNC32.ContractSpecificPath + 'NCInclude\' + Inc) then begin
      Result:= CNC32.ContractSpecificPath + 'NCInclude\' + Inc;
      Exit;
    end;

    if FileExists(CNC32.MachSpecPath + 'NCInclude\' + Inc) then begin
      Result:= CNC32.MachSpecPath + 'NCInclude\' + Inc;
      Exit;
    end;

    raise EGCodeRead.Create('Include file not found: ' + Inc);
  end;

  function ReadIncludeFile(Inc: string): string;
  var ST: TStringList;
  begin
    ST:= TStringList.Create;
    try
      ST.LoadFromFile(Inc);
      Result:= ST.Text;
    finally
      ST.Free;
    end;
  end;

var P, I : Integer;
    S, H, Token : string;
    SL: TStringList;
begin
  Clean;
  FLoaded := False;
  S := Text;
  P := FindProgramStart(S, 1); //PositionOf(S, '%', 1);
  if P = 0 then
    raise EGCodeRead.Create(MissingStartDesignator);

  SL:= TStringList.Create;
  try
    SL.Text:= System.Copy(S, 1, P-1);
    for I:= 0 to SL.Count - 1 do begin
      H:= SL[I];
      Token:= LowerCase(ReadDelimited(H, ' '));
      if Trim(Token) = 'include' then begin
        H:= ResolveInclude(Trim(H));
        S:= S + ReadIncludeFile(H);
      end;
    end;
  finally
    SL.Free;
  end;

  if P > 1 then
    System.Delete(S, 1, P-1);

  P := FindProgramStart(S, 2); //P := PositionOf(S, '%', 2);
  while P > 0 do begin
    I := SubList.Add(TSubPartProgram.Create(Self, SubList.Count));
    SubRoutine[I].Strings.Text := System.Copy(S, 1, P-1);
    SubRoutine[I].SetName;
    System.Delete(S, 1, P-1);
    P := FindProgramStart(S, 2); //P := PositionOf(S, '%', 2);
  end;

  I := SubList.Add(TSubPartProgram.Create(Self, SubList.Count));
  SubRoutine[I].Strings.Text := S;
  SubRoutine[I].SetName;
  LocalShit := SubRoutine[I].Name;
end;

procedure TNC32PartProgramFile.SaveToStrings;
var S : string;
    I : Integer;
begin
  S := '';
  for I := 0 to SubList.Count - 1 do begin
    S := S + SubRoutine[I].Strings.Text;
  end;
end;

procedure TNC32PartProgramFile.Compile;
var I : Integer;
    SPP : TSubPartProgram;
begin
  Unique:= 0;
  SPP := SubRoutine[0];
  SPP.Strings.Add('');
  try
    for I := 0 to SubList.Count - 1 do begin
      SPP := SubRoutine[I];
      SPP.Compile;
//      SPP.DumpData;
    end;

  except
    on E : EGCodeCompile do begin
      raise EGCodeCompile.CreateFmt(ErrorAtLineFmt, [SPP.Name, SPP.ThisLine, E.Message]);
    end;

    on E : EMathCompileError do begin
      raise EGCodeCompile.CreateFmt(MathErrorAtLineFmt, [SPP.Name, SPP.ThisLine, E.Message]);
    end;

    on E : Exception do begin
      raise EGCodeCompile.CreateFmt(UnknownErrorAtLineFmt, [SPP.Name, SPP.ThisLine, E.Message]);
    end;
  end;
  FLoaded := True;
end;



{ raises an exception if the next line is the endoffile }
function TSubPartProgram.NextLine : string;
begin
  Result := NextLineNE;
  if ThisLine >= Strings.Count then
    raise EGCodeCompile.Create(UnexpectedEndOfFile);
end;

function TSubPartProgram.NextLineNE : string;
begin
  repeat
    Inc(ThisLine);
    if ThisLine >= Strings.Count then begin
      Result := '';
      Exit;
    end;
    Result := Strings[ThisLine];
    if Result <> '' then
      Result := StripComments;
  until(Result <> '');
end;

procedure TSubPartProgram.AddTag(aRef : string; aTag : TAbstractTag);
begin
  if Pos(',', aRef) <> 0 then // ????? despicable
    Tags.AddObject(Format('%s %d', [aRef, ThisLine]), aTag)
  else
    Tags.AddObject(aRef, aTag);
end;

function TSubPartProgram.VerySimpleResolve(aLine : string; var Value :  Double) : Boolean;
var Code : Integer;
begin
  System.Val(aLine, Value, Code);
  Result := Code = 0;
end;

function TSubPartProgram.VerySimpleInteger(aLine : string; var Value : Integer) : Boolean;
begin
  Result := True;
  try
    Value := StrToInt(Trim(aLine));
  except
    Result := False;
  end;
end;

function TSubPartProgram.SimpleResolve(aLine : string) : TAbstractTag;
var Value : Double;
    Index : Integer;
begin
  aLine := Trim(aLine);

  Index := PartProgram.VariableList.IndexOf(aLine);
  if Index <> -1 then begin
    Result := TAbstractTag(PartProgram.VariableList.Objects[Index]);
    Exit;
  end;

// Check if this tag already exists
  Index := Tags.IndexOf(Trim(aLine));
  if Index <> -1 then begin
    Result := TAbstractTag(Tags.Objects[Index]);
    Exit;
  end;

// Check for all the basic types
  if VerySimpleResolve(aLine, Value) then begin
    Result := TFloatTag.Create(PartProgram.TagOwner, aLine);
    Result.AsDouble := Value;
    Result.IsConstant := True;
    AddTag(aLine, Result);
    Exit;
  end;

  Result := SysObj.Comms.FindTag(aLine);
  if Assigned(Result) then
    Exit;

  if aLine[1] = '''' then begin
    System.Delete(aLine, 1, 1);
    System.Delete(aLine, Length(aLine), 1);

    Result := TAlphaTag.Create(PartProgram.TagOwner, aLine);
    Result.AsString := SysObj.Localized.GetString(aLine);
    Result.IsConstant := True;
    Result.IsWritable := False;
    AddTag(aLine, Result);
    Exit;
  end;
end;

function TSubPartProgram.NewMathParser(aLine : string) : TAbstractTag;
var MP : TNCMathParser;
begin
  Result := SimpleResolve(aLine);

  aLine := Trim(aLine);
  if (Result = nil) and IsAlpha(aLine[1]) then
    Result := ResolveVariable(Self, aLine);

  if Result = nil then begin
    MP := TNCMathParser.Create(PartProgram.TagOwner, aLine + '_F' + IntToStr(PartProgram.Unique));
    Inc(PartProgram.Unique);
    MP.OnResolveVariable := ResolveVariable;
    MP.OnResolveFunction := ResolveFunction;
    MP.OnResolveArray := ResolveArray;
    MP.Compile(aLine);
    Result:= MP.ExtractSimpleUnderlyingTag;
    if Result <> nil then begin
      MP.Free;
      Exit;
    end;
    AddTag(MP.Name, MP);
    Result := MP;
  end;
end;

function TSubPartProgram.ReadExpression(var aLine : string) : string;
var I : Integer;
    PareCount : Integer;
begin
  aLine := TrimLeft(aLine);
  if aLine[1] <> '(' then
    raise EGCodeCompile.Create(MissingOpenParentheses);

  PareCount := 0;
  for I := 1 to Length(aLine) do begin
    case aLine[I] of
      '(' : Inc(PareCount);
      ')' : Dec(PareCount);
    end;
    if PareCount = 0 then begin
      Result := Copy(aLine, 2, I-2);
      System.Delete(aLine, 1, I);
      Exit;
    end;
  end;
  raise EGCodeCompile.Create(MismatchedParentheses);
end;

function TSubPartProgram.ReadExpressionTag(var aLine : string) : TAbstractTag;
var NewName : string;
begin
  NewName := ReadExpression(aLine);
  Result := NewMathParser(NewName);
end;

procedure TSubPartProgram.ExpectedToken(var aLine : string; const Token : string);
begin
  aLine := Trim(aLine);
  if Token = Copy(aLine, 1, Length(Token)) then begin
    System.Delete(aLine, 1, Length(Token));
    Exit;
  end else
    raise EGCodeCompile.CreateFmt(ExpectedTokenFmt, [Token]);
end;

procedure TSubPartProgram.ExpectedEndToken(var aLine : string; const Token : string);
begin
  ExpectedToken(aLine, Token);
  if TrimLeft(aLine) <> '' then
    raise EGCodeCompile.CreateFmt(UnExpectedTokenFmt, [aLine]);
end;

function TSubPartProgram.AddData(Data : Pointer) : Integer;
begin
  Result := InstructionList.Add(Data);
end;

procedure TSubPartProgram.AddLabel(aLabel : string; aCodePtr : Integer);
begin
  if Labels.IndexOf(aLabel) <> -1 then
    raise EGCodeCompile.CreateFmt(DuplicateLabelDefinedFmt, [aLabel]);
  Labels.AddObject(aLabel, Pointer(aCodePtr));
end;

function TSubPartProgram.AddInstructionNL(Inst : TNCInstruction; ParmCount : Integer) : Integer;
var NCInst : TNCInstructionRecord;
begin
  NCInst.Instruction := {Ord}(Inst);
  NCInst.ParameterCount := ParmCount;
  Result := AddData(NCInst.Ptr);
end;

function TSubPartProgram.AddLineNumber;
var NCInst : TNCInstructionRecord;
begin
  NCInst.Instruction := {Ord}(nfLineNumber);
  NCInst.ParameterCount := 2;
  AddData(NCInst.Ptr);
  Result := AddData(Pointer(ThisLine));
  LastLine := ThisLine;
end;

function TSubPartProgram.AddInstruction(Inst : TNCInstruction; ParmCount : Integer) : Integer;
begin
  if ThisLine <> LastLine then begin
    AddLineNumber;
  end;
  Result := AddInstructionNL(Inst, ParmCount);
end;

procedure TSubPartProgram.SetName;
begin
  FName := UpperCase(Copy(Strings[0], 2, Length(Strings[0])));
end;


{procedure TSubPartProgram.SetName;
begin
  FName := UpperCase(Strings[0]);
  System.Delete(FName, 1, 1);
end; }

procedure TSubPartProgram.CompileIf(var aLine : string);
var aTag : TAbstractTag;
    FailPoint : Integer;
    FinalExit : Integer;
    aSub : string;
begin
  aTag := ReadExpressionTag(aLine);
  ExpectedEndToken(aLine, 'THEN');
  AddInstruction(nfIf, 3);
  AddData(aTag);
  FailPoint := AddData(nil);

  NextLine;
  aLine := CompileBlock('END');
  AddInstruction(nfJump, 2);
  FinalExit := AddData(nil);

  aLine := Trim(aLine);
  if aLine <> '' then begin
    ReadStartToken(aLine, aSub);
    if aSub = 'ELSE' then begin
      InstructionList[FailPoint] := Pointer(InstructionList.Count);

      if aLine = '' then begin
        NextLine;
        CompileBlock('END');
      end else begin
        ReadStartToken(aLine, aSub);
        if aSub = 'IF' then begin
          CompileIf(aLine);
        end else
          raise EGCodeCompile.CreateFmt('Unexpected Token [%s]', [aSub]);
      end;

    end else
      raise EGCodeCompile.CreateFmt('Unexpected Token [%s]', [aSub]);
  end else begin
    InstructionList[FailPoint] := Pointer(InstructionList.Count);
  end;
  InstructionList[FinalExit] := Pointer(InstructionList.Count);
end;

procedure TSubPartProgram.CompileWhile(var aLine : string);
var EntryPoint, ExitPoint : Integer;
    aTag : TAbstractTag;
begin
  EntryPoint := InstructionList.Count;
  aTag := ReadExpressionTag(aLine);
  ExpectedEndToken(aLine, 'DO');
  {EntryPoint :=} AddInstruction(nfIf, 3);
  AddData(aTag);
  ExitPoint := AddData(nil);
  NextLine;
  CompileBlock('END');
  AddInstruction(nfJump, 2);
  AddData(Pointer(EntryPoint));
  InstructionList[ExitPoint] := Pointer(InstructionList.Count);
end;

function TSubPartProgram.ReadVariable(var aLine : string) : TAbstractTag;
  function ReadSuffix(const Text : string; aOpen, aClose : Char) : string;
  var BraceCount : Integer;
      I : Integer;
  begin
    BraceCount := 0;
    for I := 1 to Length(Text) do begin
      if Text[I] = aOpen then
        Inc(BraceCount)
      else if Text[I] = aClose then
        Dec(BraceCount);
        
      if BraceCount = 0 then begin
        Result := System.Copy(Text, 1, I);
        Exit;
      end;
    end;
    raise EGCodeCompile.CreateFmt(ArraySuffixMalformedFmt, [Text]);
  end;

var aVar, Suffix : string;
    I : Integer;
begin
  aLine := TrimLeft(aLine);
  aVar := aLine;
  Result := nil;
  I := 1;
  Suffix := '';

  try
    while I < Length(aLine) do begin
      if not ( IsNumeric(aLine[I]) or
               IsAlpha(aLine[I]) or
               (aLine[I] = '#') ) then begin

        aVar := Copy(aLine, 1, I-1);

        if aLine[I] = '[' then begin
          aLine := TrimLeft(aLine);
          Suffix := ReadSuffix(Copy(aLine, I, Length(aLine)), '[', ']');
        end else if aLine[I] = '(' then begin
          aLine := TrimLeft(aLine);
          Suffix := ReadSuffix(Copy(aLine, I, Length(aLine)), '(', ')');
        end;
        Exit;
      end;
      Inc(I);
    end;
  finally
    System.Delete(aLine, 1, Length(aVar) + Length(Suffix));
    Suffix := Trim(Suffix);
    LastAttemptedVariable := aVar;
    if Suffix = '' then
      Result := ResolveVariable(Self, aVar)
    else if Suffix[1] = '[' then
      Result := ResolveArray(Self, aVar, Copy(Suffix, 2, Length(Suffix) - 2))
    else if Suffix[1] = '(' then
      Result := ResolveFunction(Self, aVar, Copy(Suffix, 2, Length(Suffix) - 2))
    else
      raise EGCodeCompile.CreateFmt(UnknownVariableFmt, [aVar]);
  end;
end;

procedure TSubPartProgram.CompileFor(var aLine : string);
  function ReadTermExpression(const aTerm : string) : TAbstractTag;
  var I : Integer;
      aExp : string;
  begin
    for I := 2 to Length(aLine) - Length(aTerm) + 1 do begin
      if aLine[I] = aTerm[1] then begin
        if Copy(aLine, I, Length(aTerm)) = aTerm then begin
           aExp := Copy(aLine, 1, I-1);
           System.Delete(aLine, 1, I-1);
           Result := NewMathParser(aExp);
           Exit;
        end;
      end;
    end;
    raise EGCodeCompile.CreateFmt(ExpectedTokenFmt, [aTerm]);
  end;

var ITag, STag, TTag : TAbstractTag;
    Index : Integer;
begin
  ITag := ReadVariable(aLine);
  if not ITag.IsWritable {or ITag.IsOwned} then
    raise EGCodeCompile.CreateFmt(InvalidAssignmentTargetFmt, [ITag.Name]);
  ExpectedToken(aLine, '=');
  STag := ReadTermExpression('TO');
  ExpectedToken(aLine, 'TO');
  TTag := ReadTermExpression('DO');
  ExpectedToken(aLine, 'DO');
  if aLine <> '' then
    raise EGCodeCompile.CreateFmt(UnexpectedCharactersFmt, [aLine]);

  AddInstruction(nfAssign, 3);
  AddData(ITag);
  AddData(STag);
  Index := InstructionList.Count;
  NextLine;
  CompileBlock('END');
  AddInstruction(nfIndexVar, 2);
  AddData(ITag);
  AddInstruction(nfICompare, 5);
  AddData(ITag);
  AddData(TTag);
  AddData(Pointer(Index)); // On less than or equal
  AddData(Pointer(InstructionList.Count + 1));
end;

procedure TSubPartProgram.CompileRepeat(var aLine : string);
var EntryPoint : Integer;
    aTag : TAbstractTag;
begin
  EntryPoint := InstructionList.Count;
  ExpectedEndToken(aLine, '');
  NextLine;
  aLine := CompileBlock('UNTIL');
//  ExpectedToken(aLine, 'UNTIL');
  aTag := ReadExpressionTag(aLine);
  ExpectedEndToken(aLine, '');
  AddInstruction(nfIf, 3);
  AddData(aTag);
  AddData(Pointer(EntryPoint)); // On true jump over this and jump back
end;

// Code format
// nfHashTable,
procedure TSubPartProgram.CompileCase(var aLine : string);
var aTag : TAbstractTag;
    HashTablePtr, IntVal : Integer;
    HashTable : TList;
    JumpOut : TList;
    ElseIn : Integer;
    P : Integer;
    Tmp : string;
begin
  aTag := ReadExpressionTag(aLine);
  ExpectedEndToken(aLine, 'OF');

  AddInstruction(nfHash, 3);
  AddData(aTag);
  HashTablePtr := AddData(nil);
  AddInstruction(nfJump, 2);
  ElseIn := AddData(nil);
  Tmp := NextLine;

  HashTable := TList.Create;
  JumpOut := TList.Create;
  try
    while True do begin
      if Tmp = 'ELSE' then begin
        InstructionList[ElseIn] := Pointer(InstructionList.Count);
        NextLine;
        CompileBlock('END');
        Exit;
      end else if Tmp = 'END' then begin
        InstructionList[ElseIn] := Pointer(InstructionList.Count);
        Exit;
      end else begin
        P := System.Pos(':', Tmp);
        if P = 0 then
          raise EGCodeCompile.Create(CaseRequiresColon);
        if VerySimpleInteger(Copy(Tmp, 1, P-1), IntVal) then begin
          HashTable.Add(Pointer(IntVal));
          HashTable.Add(Pointer(InstructionList.Count));
          System.Delete(Tmp, 1, P);
          CompileLanguage(Tmp, '');
          AddInstruction(nfJump, 2);
          JumpOut.Add(Pointer(AddData(nil)));
          Tmp := NextLine;
        end else
          raise EGCodeCompile.Create(CaseSimpleInteger);
      end;
    end;
  finally
    InstructionList[HashTablePtr] := Pointer(InstructionList.Count);
    AddInstructionNL(nfData, HashTable.Count + 1);
    while HashTable.Count > 0 do begin
      InstructionList.Add(HashTable[0]);
      HashTable.Delete(0);
    end;
    while JumpOut.Count > 0 do begin
      InstructionList[Integer(JumpOut[0])] := Pointer(InstructionList.Count);
      JumpOut.Delete(0);
    end;

    HashTable.Free;
    JumpOut.Free;
  end;
end;

procedure TSubPartProgram.CompileCall(var aLine : string);
var P : Integer;
    Index : Integer;
    VarName : string;
    NCInst : TNCInstructionRecord;
begin
  aLine := TrimLeft(aLine);
  if Length(aLine) < 3 then
    raise EGCodeCompile.CreateFmt(NameTooShortFmt, [aLine]);
  if not IsAlpha(aLine[1]) {or not IsAlpha(aLine[2])} then
    raise EGCodeCompile.CreateFmt(IllegalNameFmt, [aLine]);

  P := System.Pos(' ', aLine);
  if P = 0 then begin
    VarName := Trim(aLine);
    Index := AddInstruction(nfCall, 3);
    AddData(nil);
    AddData(nil); // empty set
  end else begin
    VarName := Trim(System.Copy(aLine, 1, P-1));
    System.Delete(aLine, 1, P);
    Index := CompileSFA(aLine, AllParameters, nfCall, 1);
  end;

  for P := 0 to PartProgram.SubList.Count - 1 do begin
    if PartProgram.SubRoutine[P].Name = VarName then begin
      if Assigned(PartProgram.MacroExecutive) then
        InstructionList[Index + 1] := Pointer(P)
      else
        InstructionList[Index + 1] := Pointer(-(P + 1));  // &&&&&&&&????????
      Exit;
    end;
  end;

  if Assigned(PartProgram.MacroExecutive) then
    for P := 0  to PartProgram.MacroExecutive.SubList.Count - 1 do begin
      if PartProgram.MacroExecutive.SubRoutine[P].Name = VarName then begin
        InstructionList[Index + 1] := Pointer(-(P + 1));
        NCInst.Ptr := InstructionList[Index];
        NCInst.Instruction := nfMacroCall;
        InstructionList[Index] := NCInst.Ptr;
        Exit;
      end;
    end;

  raise EGCodeCompile.CreateFmt(UnknownSubProgramFmt, [VarName]);
end;

procedure TSubPartProgram.CompileGoto(var aLine : string);
var Index : Integer;
begin
  aLine := Trim(aLine);
  if (aLine <> '') then begin
    AddInstruction(nfJump, 2);
    Index := AddData(nil);
    GotoList.AddObject(aLine, Pointer(Index));
  end else
    raise EGCodeCompile.CreateFmt(NoTargetForGotoFmt, [aLine]);
end;

procedure TSubPartProgram.CompileReturn(var aLine : string);
begin
  AddInstruction(nfReturn, 1);
end;

procedure TSubPartProgram.CompileVar(aLine : string);
var I : Integer;
    VarName : string;
begin
  I := Pos(':', aLine);
  if I = 0 then begin
    VarName := Trim(aLine);
    aLine := 'DOUBLE';
  end else begin
    VarName := Trim(System.Copy(aLine, 1, I-1));
    System.Delete(aLine, 1, I);
    aLine := Trim(aLine);
  end;

  if Length(VarName) < 3 then
    raise EGCodeCompile.CreateFmt(NameTooShortFmt, [VarName]);
  if not IsAlpha(VarName[1]) or not IsAlpha(VarName[2]) then
    raise EGCodeCompile.CreateFmt(IllegalNameFmt, [VarName]);

  for I := 3 to Length(VarName) do begin
    if not IsNumeric(VarName[I]) and not IsAlpha(VarName[I]) then
      raise EGCodeCompile.CreateFmt(InvalidCharactersFmt, [VarName]);
  end;
  if aLine = 'INTEGER' then
    PartProgram.AddVariable(TOrdinalTag.Create(PartProgram.TagOwner, VarName))
  else if aLine = 'DOUBLE' then
    PartProgram.AddVariable(TFloatTag.Create(PartProgram.TagOwner, VarName))
  else if aLine = 'STRING' then
    PartProgram.AddVariable(TAlphaTag.Create(PartProgram.TagOwner, VarName))
  else
    raise EGCodeCompile.CreateFmt(UnknownVariableTypeFmt, [aLine]);
end;

procedure TSubPartProgram.CompileConst(aLine : string);
var I : Integer;
    VarName : string;
    aTag : TAbstractTag;
    Temp: Double;
begin
  I := Pos('=', aLine);
  if I = 0 then
    raise EGCodeCompile.CreateFmt(ConstExpressionRequiresEquateFmt, [aLine]);

  VarName := Trim(System.Copy(aLine, 1, I-1));
  System.Delete(aLine, 1, I);
  aLine := Trim(aLine);

  if Length(VarName) < 3 then
    raise EGCodeCompile.CreateFmt(NameTooShortFmt, [VarName]);
  if not IsAlpha(VarName[1]) or not IsAlpha(VarName[2]) then
    raise EGCodeCompile.CreateFmt(IllegalNameFmt, [VarName]);

  for I := 3 to Length(VarName) do begin
    if not IsNumeric(VarName[I]) and not IsAlpha(VarName[I]) then
      raise EGCodeCompile.CreateFmt(InvalidCharactersFmt, [VarName]);
  end;

  aLine := Trim(aLine);
  if aLine[1] = '''' then begin
    if aLine[Length(aLine)] <> '''' then
      raise EGCodeCompile.CreateFmt('Malformed string constant [%s]', [aLine]);

    aLine := System.Copy(aLine, 2, Length(aLine)-2);
    aTag := TAlphaTag.Create(PartProgram.TagOwner, VarName);
    aTag.AsString := SysObj.Localized.GetString(aLine);

  end else begin
    aTag := TFloatTag.Create(PartProgram.TagOwner, VarName);
    try
      if aLine[1] = '$' then
        aTag.AsDouble := StrToInt(aLine)
      else
        aTag.AsDouble := StrToFloat(aLine);
    except
      raise EGCodeCompile.CreateFmt(ValueIsNotNumericFmt, [aLine]);
    end;
  end;
  PartProgram.AddVariable(aTag);
  aTag.IsWritable := False;
end;

function GCodeToGroup(aGCode : Integer) : TGCodeGroup;
var I : Integer;
begin
  for Result := Low(TGCodeGroup) to High(TGCodeGroup) do begin
    for I := 0 to GCodeGroups[Result].Count - 1 do begin
      if aGCode = GCodeGroups[Result].Codes[I] then
        Exit;
    end;
  end;

  if Sysobj.NC.IsPluginGCode(aGCode, 0) then begin
    Result:= gcgPlugin;
    Exit;
  end;
  raise EGCodeCompile.CreateFmt(UnknownGCodeFmt, [aGCode]);
end;

function GenerateGMShortForm(aValue : Double) : TNCInstructionRecord;
begin
  if aValue >= 1024 then
    raise EGCodeCompile.Create(GMCodeValueOutOfRange);
  Result.NI := Round(aValue);
  Result.NF := Round((aValue - Round(aValue)) * 10000);
end;


procedure TSubPartProgram.CompileGCodeLine(var aLine : string);

  procedure DoCompileSFA(aLegal : TSFAParameters; Inst : TNCInstruction);
  begin
    CompileSFA(aLine, aLegal, Inst, 0);
    CompileEndLine(aLine);
  end;

var GCGS : TGCodeGroups;
    GCG : TGCodeGroup;
    aSub : string;
    Value : Double;
    NCR : TNCInstructionRecord;
    PGC: INCExtGCode;
begin
  GCGS := [];
  while (aLine <> '') and (aLine[1] = 'G') do begin
    ReadStartToken(aLine, aSub);
    System.Delete(aSub, 1, 1);
    if VerySimpleResolve(aSub, Value) then begin
      NCR := GenerateGMShortForm(Value);
      GCG := GCodeToGroup(NCR.NI);
      if GCodeGroups[GCG].Unique then begin
        if GCGS <> [] then
          raise EGCodeCompile.Create('G-Code Must be unique');
        case NCR.NI of
          4 : DoCompileSFA([nfParamF], nfDwell);
          50: DoCompileSFA(AxisParameters, nfG50);
          51: DoCompileSFA(AxisParameters, nfG51);
          52: DoCompileSFA(AxisParameters, nfG52);
          92: begin
            // Insert nfDwell
            AddInstruction(nfDwell, 2);
            AddData(nil);
            DoCompileSFA(AxisParameters, nfG92);
          end;
          96: DoCompileSFA([], nfG92); // Compile a G96 as a G92 with no parameters
        else
          raise EGCodeCompile.CreateFmt(UnknownGCodeFmt, [NCR.NI]);
        end;
        Exit;
      end else if GCodeGroups[GCG].Plugin then begin
        PGC:= Sysobj.NC.GetPluginGCode(NCR.NI, 0);
        PGC.Compile(Self, aLine);
        ExtGcodeList.Add(Pointer(PGC));
        aLine:= '';
        AddInstruction(nfPlugin, 2);
        AddData(Pointer(PGC));
      end else begin
        if GCG in GCGS then
          raise EGCodeCompile.Create('Mutually exclusive G-Codes');
        Include(GCGS, GCG);
        AddInstruction(nfGCode, 2);
        AddData(NCR.Ptr);
      end;
    end else
      raise EGCodeCompile.Create(InvalidIntegerConversion);
    aLine := TrimLeft(aLine);
  end;
  CompileNonGCodeLine(aLine);
end;

procedure TSubPartProgram.CompileEndLine(var aLine : string);
var aSub : string;
    aTag : TAbstractTag;
begin
  while (aLine <> '') and ReadStartToken(aLine, aSub) do begin
    case aSub[1] of
      'F' : AddInstruction(nfFCode, 2);
      'M' : AddInstruction(nfMCode, 2);
      'S' : AddInstruction(nfSCode, 2);
      'T' : AddInstruction(nfTCode, 2);
    else
      raise EGCodeCompile.CreateFmt(InvalidParameterFmt, [aSub]);
    end;
    System.Delete(aSub, 1, 1);
    aTag := NewMathParser(aSub);
    AddData(aTag);
  end;
end;

function TSubPartProgram.CompileSFA(var aLine : string; aLegal : TSFAParameters;
   Inst : TNCInstruction; Space : Integer) : Integer;
var NCR : TNCInstructionRecord;
    Parm : TSFAParameter;
    Parms : TSFAParameters;
    aSub : string;
    I : Integer;
begin
  Parms := [];
  aLine := TrimLeft(aLine);
  FillChar(SFAParms, Sizeof(SFAParms), 0);
  Result := -1;

  try
    while aLine <> '' do begin
        Parm := GenerateSFAParameter(aLine[1]);
        if not (Parm in aLegal) and (Parm in EndLineParameters) then
          Exit;
        if not (Parm in aLegal) then
          raise EGCodeCompile.CreateFmt(InvalidParameterFmt, [Copy(aLine, 1, 1)]);

        if ReadStartToken(aLine, aSub) then begin
          Parm := GenerateSFAParameter(aSub[1]);
          if Parm in Parms then
            raise EGCodeCompile.Create('Duplicate Parameter');

          System.Delete(aSub, 1, 1);
          Include(Parms, Parm);
          SFAParms[Parm] := NewMathParser(aSub);
        end else
          raise EGCodeCompile.Create('Invalid SFA');
      aLine := TrimLeft(aLine);
    end;
  finally
    Result := AddInstruction(Inst, 2);
    for I := 0 to Space - 1 do
      AddData(nil);
    AddData(Pointer(Parms));
    for Parm := Low(TSFAParameter) to High(TSFAParameter) do begin
      if Assigned(SFAParms[Parm]) then
        AddData(SFAParms[Parm]);
    end;
    NCR.Ptr := InstructionList[Result];
    NCR.ParameterCount := InstructionList.Count - Result;
    InstructionList[Result] := NCR.Ptr;
  end;
end;

procedure TSubPartProgram.CompileNonGCodeLine(var aLine : string);
begin
  aLine := TrimLeft(aLine);
  if aLine = '' then
    Exit;

  case aLine[1] of
    'M', 'S', 'T', 'F' : CompileEndLine(aLine);
  else
    CompileSFA(aLine, NonGCodeParameters, nfSFA, 0);
    CompileEndLine(aLine);
  end;
end;

procedure TSubPartProgram.CompileHardware(var aLine : string);
var aTag : TAbstractTag;
begin
  aLine := TrimLeft(aLine);
  // Special use the name of the tag to store the value
  aTag := TAlphaTag.Create(PartProgram.TagOwner, aLine);
  AddInstruction(nfHardware, 2);
  AddData(aTag);
end;


procedure TSubPartProgram.CompileAssignment(var aLine : string);
var TTag, STag : TAbstractTag;
begin
  TTag := ReadVariable(aLine);
  if not Assigned(TTag) then
    raise EGCodeCompile.CreateFmt('Unknown variable [%s]', [LastAttemptedVariable]);

  if TTag is TAbstractScriptMethodTag then begin
    if Trim(aLine) <> '' then
      raise EGCodeCompile.CreateFmt(FollowingCrapFmt, [aLine]);
    Exit;
  end else if {not TTag.IsOwned and} TTag.IsWritable then begin
    ExpectedToken(aLine, '=');
    STag := NewMathParser(aLine);
    AddInstruction(nfAssign, 3);
    AddData(TTag);
    AddData(STag);
  end else
    raise EGCodeCompile.CreateFmt(VariableIsReadOnlyFmt, [TTag.Name]);
end;

procedure TSubPartProgram.CompileSynch(var aLine : string);
begin
  AddInstruction(nfHWSynch, 1);
  if Trim(aLine) <> '' then
    raise EGCodeCompile.CreateFmt(FollowingCrapFmt, [aLine]);
end;

procedure TSubPartProgram.Compile;
var I, P : Integer;
begin
  Clean;
  SetName;
  ThisLine := 1;
  CompileBlock('');
  AddInstruction(nfMCode, 2);
  AddData(PartProgram.M30);
  AddInstruction(nfReturn, 1);
  for I := 0 to GotoList.Count - 1 do begin
    P := Labels.IndexOf(GotoList[I]);
    if P <> -1 then
      InstructionList[Integer(GotoList.Objects[I])] := Labels.Objects[P]
    else
      raise EGCodeCompile.CreateFmt(GotoTargetNotFoundFmt, [GotoList[I]]);
  end;
end;

function TSubPartProgram.CompileLanguage(var aLine : string; const Term : string) : Boolean;
var
  aSub : string;
begin
  Result := False;
  if ReadStartToken(aLine, aSub) then begin
//    aLine := TrimLeft(Strings[ThisLine]);
    aLine := aSub + aLine;
    case aLine[1] of
      'G' : CompileGCodeLine(aLine);
    else
      CompileNonGCodeLine(aLine);
    end;
  end else
    case NCLanguage(aSub) of
      nlIf     : CompileIf(aLine);
      nlWhile  : CompileWhile(aLine);
      nlFor    : CompileFor(aLine);
      nlRepeat : CompileRepeat(aLine);
      nlCase   : CompileCase(aLine);
      nlCall   : CompileCall(aLine);
      nlGoto   : CompileGoto(aLine);
      nlReturn : CompileReturn(aLine);
      nlVar    : CompileVar(aLine);
      nlConst  : CompileConst(aLine);
      nlSynch  : CompileSynch(aLine);
      nlHardware : CompileHardware(aLine);
    else
      if (Term <> '') and (aSub = Term) then begin
        Exit;
      end else begin
        aLine := aSub + aLine;
        CompileAssignment(aLine)
      end;
    end;

  Result := True;
end;

function TSubPartProgram.StripComments : string;
var P : Integer;
    aStart : string;
    BlockDelete : Boolean;
    InQuote: Boolean;
begin
  BlockDelete := False;
  Result := TrimLeft(Strings[ThisLine]);
  Result := UpperCase(Result);
  if Result = '' then
    Exit;

  //P := System.Pos(CommentChar, Result);
  InQuote:= False;
  for P:= 1 to Length(Result) do begin
    if Result[P] = '''' then
      InQuote:= not InQuote;
    if (Result[P] = CommentChar) and not InQuote then
      Break;
  end;
  if P <= Length(Result) then begin
    System.Delete(Result, P, Length(Result));
  end;

  if Result = '' then
    Exit;

  if Result[1] = BlockDeleteChar then begin
    BlockDelete := True;
    System.Delete(Result, 1, 1);
    Result := TrimLeft(Result);
  end;

  if (Length(Result) > 1) and
    (Result[1] = 'N') and
     IsNumeric(Result[2]) then begin
    ReadStartToken(Result, aStart);
    try
      StrToInt(Copy(aStart, 2, Length(aStart)));
    except
      raise EGCodeCompile.CreateFmt(InvalidLabelNameFmt, [aStart]);
    end;
    AddLabel(aStart, InstructionList.Count);
  end else begin
    P := Pos('@', Result);
    if P <> 0 then begin
      aStart := System.Copy(Result, 1, P-1);
      System.Delete(Result, 1, P);

      if aStart = '' then
        raise EGCodeCompile.CreateFmt(InvalidLabelNameFmt, [aStart]);

      for P := 1 to Length(aStart) do
        if aStart[P] = ' ' then
          raise EGCodeCompile.CreateFmt(InvalidLabelNameFmt, [aStart]);
      if not (IsAlpha(aStart[1]) and IsAlpha(aStart[2])) then
        raise EGCodeCompile.CreateFmt(InvalidLabelNameFmt, [aStart]);
      AddLabel(aStart, InstructionList.Count);
    end;
  end;

  if BlockDelete then
    AddInstruction(nfBlockDelete, 1);
end;

function TSubPartProgram.CompileBlock(Term : string) : string;
begin
  Result := StripComments;
  while ThisLine < Strings.Count{ - 1} do begin
    Result := Trim(Result);
    if Result <> '' then
      if not CompileLanguage(Result, Term) then
        Exit;
    Result := NextLineNE
  end;
end;

function TNCAssignedTag.GetAsInteger : Integer;
begin
  if Assigned(SubProgram) then
    Result := Ord(Parameter in SubProgram.RunTimeParameters)
  else
    Result := 0;
end;

function TNCAssignedTag.GetAsBoolean: Boolean;
begin
  Result:= AsInteger <> 0;
end;

constructor TNCAssignedTag.Create(aOwner : TTagOwner; aName : string);
begin
  inherited Create(aOwner, aName);
  IsWritable := False;
  Parameter := nfParamA;
  SubProgram := nil;
end;

{ NullScript }

procedure NullScript.ExecuteSpecial;
begin
end;

function NullScript.GetAsDouble: Double;
begin
  Result:= 0;
end;

function NullScript.GetAsInteger: Integer;
begin
  Result:= 0;
end;

function NullScript.GetAsString: string;
begin
  Result:= '';
end;

function TSubPartProgram.ResolveNCVariable(TagValue: WideString): TTagRef;
begin
  if Length(TagValue) > 0 then begin
    Result:= Self.NewMathParser(TagValue);
  end else
    raise EGCodeCompile.CreateFmt(InvalidParameterFmt, [TagValue]);
end;

function TSubPartProgram.QueryInterface(const IID: TGUID; out Obj): HResult;
begin
  if GetInterface(IID, Obj) then
    Result := 0
  else
    Result := E_NOINTERFACE;
end;

function TSubPartProgram._AddRef: Integer;
begin
  Inc(RefCount);
  Result:= RefCount;
end;

function TSubPartProgram._Release: Integer;
begin
  Dec(RefCount);
  Result:= 10;
end;

initialization
  RegisterTagClass(TNCAssignedTag);
  RegisterTagClass(NullScript);
end.

