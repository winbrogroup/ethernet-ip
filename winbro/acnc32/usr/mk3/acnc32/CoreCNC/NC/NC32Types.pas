unit NC32Types;

interface

uses SysUtils, CNCTypes, CNC32, FaultRegistration;
type
  EGCodeCompile = class(Exception);
  EGCodeRead = class(Exception);

  TSFAParameter = (
    nfParamX,
    nfParamY,
    nfParamZ,
    nfParamA,
    nfParamB,
    nfParamC,
    nfParamU,
    nfParamV,
    nfParamW,
    nfParamD,
    nfParamE,
    nfParamF,
    nfParamG,
    nfParamH,
    nfParamI,
    nfParamJ,
    nfParamK,
    nfParamL,
    nfParamM,
    nfParamN,
    nfParamO,
    nfParamP,
    nfParamQ,
    nfParamR,
    nfParamS,
    nfParamT
  );

  TSFAParameters = set of TSFAParameter;

  TGCodeGroupRecord = record
    Name : string;
    Unique : Boolean;
    Count : Integer;
    Codes : Array [0..10] of Integer;
    Plugin: Boolean;
  end;

  TGCodeGroup = (
    gcgA,
    gcgB,
    gcgC,
    gcgD,
//    gcgE,
    gcgF,
    gcgG,
    gcgH,
    gcgI, // added 20 and 21
    gcgJ,
    gcgUnique,  // added 52 and 53
    gcgCoordSelect, // added 54..59
    gcgPlugin
  );

  TGCodeGroups = set of TGCodeGroup;


  function GenerateSFAParameter(aChar : Char) : TSFAParameter;

const
  NCParamNames : array [TSFAParameter] of Char = (
    'X', 'Y', 'Z', 'A', 'B', 'C', 'U', 'V', 'W', 'D', 'E', 'F', 'G',
    'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T'
  );


  GCodeGroups : array [TGCodeGroup] of TGCodeGroupRecord = (
    ( Name : 'Interpolation'; Unique : False; Count : 10;
             Codes : (0, 1, 2, 3, 6, 33 ,34 ,35 ,72 ,73, 0) ), // Interpolation
    ( Name : 'Axis Selection'; Unique : False; Count : 3;
             Codes : (13, 14, 15, 0, 0, 0 ,0 ,0 ,0 ,0, 0) ), // Axis selection
    ( Name : 'Plane Selection'; Unique : False; Count : 4;
             Codes : (16, 17, 18, 19, 0, 0, 0 ,0 ,0 ,0 ,0) ), // Plane selection
    ( Name : 'Cutter Compensation'; Unique : False; Count : 5;
             Codes : (40, 41, 42, 43, 44, 0, 0, 0, 0, 0, 0) ), // Cutter compensation
//    ( Name : 'Fixed Cycles'; Unique : True; Count : 10;
//             Codes : (80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 0) ), // Fixed Cycles
    ( Name : 'Abs / Inc'; Unique : False; Count : 2;
             Codes : (90, 91, 70, 71, 0, 0, 0, 0, 0, 0, 0 ) ), // Abs / Inc
    ( Name : 'Feed Rate Type'; Unique : False; Count : 3;
             Codes : (93, 94, 95, 0, 0, 0, 0, 0, 0, 0, 0 ) ), // Feed rate type
    ( Name : 'Spindle Feed Type'; Unique : False; Count : 1;
             Codes : (97, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ) ), // Spindle speed type
    ( Name : 'Units'; Unique : False; Count : 4;
             Codes : (70, 71, 20, 21, 0, 0, 0, 0, 0, 0, 0 ) ), // Units
    ( Name : 'Constant contouring Mode'; Unique : False; Count : 2;
             Codes : (61, 64, 0, 0, 0, 0, 0, 0, 0, 0, 0 ) ), // Cutting mode
    ( Name : 'Miscellaneous'; Unique : True; Count : 6;
             Codes : (4, 92, 50, 51, 52, 96, 9, 0, 0, 0, 0) ), // Misc
    ( Name : 'Coordinate System'; Unique : False; Count : 7;
             Codes : (53, 54, 55, 56, 57, 58, 59, 0, 0, 0, 0) ), // Coordinate
    ( Name : 'Plugin GCode'; Unique: False; Count: 1;
             Codes : ( 0,0,0,0,0,0,0,0,0,0,0 ); Plugin: True )
  );

  ValidCycleStartMode = [CNCModeAuto, CNCModeMDI];
  ValidJogMode = [CNCModeManual, CNCModeEdit, CNCModeTeachIn];

  GCodeGroupDefaults : array [TGCodeGroup] of Integer = (
    1, 13, 17, 40, 90, 94, 96, 21, 61, 0, 53, -1
  );


type
  TNCFault = (
    ncfAxisOpenLoop,
    ncfPositiveLimit,
    ncfNegativeLimit,
    ncfAmplifierFault,
    ncfNotHomed,
    ncfFatalFollowError,
    ncfWarnFollowError,
    ncfNoProgramLoaded,
    ncfIncorrectModeForCycleStart,
    ncfFaultTooHigh,
    ncfNoIncJogBeforeHomed,
    ncfNoModeChangeInCycle,
    ncfNotHomedInterlock,
    ncfInterlockIsActive,
    ncfFailedC3,
    ncfFailedOperatorDialogue,
    ncfEncoderLossDetect,
    ncfProgramAborted,
    ncfAxesOpenLoop,
    ncfCycleNotPermitted,
    ncfWatchDogFailed
  );

  TRetraceStep = class(TObject)
  public
    Position: array[nfParamX..nfParamW] of Double;
    MotionMode: Integer;
    I, J, K, R: Double;
  end;

const
  NCFaults : array [TNCFault] of TFaultRegistration = (
    ( Ndx : 4000; Text : 'Axis %s open loop'; Level : FaultStopCycle; Dispatch : [] ),
    ( Ndx : 4001; Text : 'Axis %s Positive Limit'; Level : FaultStopCycle; Dispatch : [] ),
    ( Ndx : 4002; Text : 'Axis %s Negative Limit'; Level : FaultStopCycle; Dispatch : [] ),
    ( Ndx : 4003; Text : 'Axis %s Amplifier Fault'; Level : FaultStopCycle; Dispatch : [] ),
    ( Ndx : 4004; Text : 'Axis %s Not Homed'; Level : FaultWarning; Dispatch : [] ),
    ( Ndx : 4005; Text : 'Axis %s Fatal Follow Error'; Level : FaultEStop; Dispatch : [] ),
    ( Ndx : 4006; Text : 'Axis %s Warn Follow Error'; Level : FaultWarning; Dispatch : [] ),
    ( Ndx : 4007; Text : 'No Program Loaded'; Level : FaultInterlock; Dispatch : [] ),
    ( Ndx : 4008; Text : 'Incorrect mode for cycle start'; Level : FaultInterlock; Dispatch : [] ),
    ( Ndx : 4009; Text : 'Error level too high for cycle start'; Level : FaultInterlock; Dispatch : [] ),
    ( Ndx : 4010; Text : 'Incremental jog is not possible before is NC homed'; Level : FaultInterlock; Dispatch : [] ),
    ( Ndx : 4011; Text : 'Mode change not permitted while in cycle'; Level : FaultInterlock; Dispatch : [] ),
    ( Ndx : 4012; Text : 'Not permitted before NC is homed'; Level : FaultInterlock; Dispatch : [] ),
    ( Ndx : 4013; Text : 'Cannot cycle start with Interlock active'; Level : FaultWarning; Dispatch : [] ),
    ( Ndx : 4014; Text : 'Reload C3: %s'; Level : FaultStopCycle; Dispatch : [] ),
    ( Ndx : 4015; Text : 'Operator Dialogue error [%s]'; Level : FaultRewind; Dispatch : [] ),
    ( Ndx : 4016; Text : 'Axis %s Encoder loss detect'; Level : FaultFatal; Dispatch : [] ),
    ( Ndx : 4017; Text : 'Program aborted Please press Reset Rewind'; Level : faultRewind; Dispatch : [] ),
    ( Ndx : 4018; Text : 'Axes Open Loop'; Level : faultStopCycle; Dispatch : [] ),
    ( Ndx : 4019; Text : '$CYCLE_NO_PERMITTED'; Level : faultInterlock; Dispatch : [] ),
    ( Ndx : 4020; Text : '$WATCH_DOG_FAILED'; Level : FaultFatal; Dispatch : [] )
  );


implementation


function GenerateSFAParameter(aChar : Char) : TSFAParameter;
begin
  for Result := Low(TSFAParameter) to High(TSFAParameter) do
    if NCParamNames[Result] = aChar then
      Exit;

  raise EGCodeCompile.Create('Invalid SFA Parameter');
end;


initialization
  FaultRegistration.RegisterFaultMap('NC32', NCFaults, Length(NCFaults), 4000);
end.
