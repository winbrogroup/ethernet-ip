unit NC32NWordSearchForm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls;

type
  TNC_NWordSearch = class(TForm)
    NList: TListBox;
    Panel1: TPanel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
  private
    procedure DetermineWidth;
  public
    class function Execute(aList : TStrings; var NWord : string) : Integer;
  end;

var
  NC_NWordSearch: TNC_NWordSearch;

implementation

{$R *.DFM}

var Form : TNC_NWordSearch;

// Returns the instruction index associated with the label.
class function TNC_NWordSearch.Execute(aList : TStrings; var NWord : string) : Integer;
begin
  Result := -1;
  if not Assigned(Form) then begin
    Form := TNC_NWordSearch.Create(Application);
    try
      Form.NList.Items.AddStrings(aList);
      Form.DetermineWidth;
      if (Form.ShowModal = mrOK) and (Form.NList.ItemIndex <> -1) then begin
        Result := Integer(Form.NList.Items.Objects[Form.NList.ItemIndex]);
        NWord := aList[Form.NList.ItemIndex];
      end else
        Result := -1;
    finally
      Form.Free;
      Form := nil;
    end;
  end;
end;

procedure TNC_NWordSearch.DetermineWidth;
var I, W, W1 : Integer;
begin
  W := Form.Width - 20;
  for I := 0 to NList.Items.Count - 1 do begin
    W1 := Canvas.TextWidth(NList.Items[I]);
    if W1 > W then
      W := W1;
  end;

  Form.Width := W + 20;
end;

end.
