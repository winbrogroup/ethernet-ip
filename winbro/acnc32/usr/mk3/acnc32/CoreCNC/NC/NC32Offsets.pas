unit NC32Offsets;

interface

uses Classes, CoreCNC, CNC32, CNCTypes, NudgeGrid, Controls, CNCAbs, NC32, NCNames,
     Grids, Extctrls, KBControl;

type
  TNC32Offsets = class(TExpandDisplay)
  private
    Grid : TNudgeGrid;
    NC32 : TNC32;

    ZeroOffset,
    SoftLimitPlus,
    SoftLimitMinus,
    StowA,
    StowB,
    StowC: TUnaryDoubleArrayTag;
    SelfChanging : Boolean;

    Panel : TPanel;

    procedure UpdateValues(aTag : TAbstractTag);
    procedure RedisplayValues;
    procedure GridFloatValue(Sender: TObject; ACol, ARow: Integer; Value: Double);
  protected
  public
    constructor Create(aOwner : TComponent); override;
    procedure InstallComponent(Params : TParamStrings); override;
    procedure Installed; override;
    procedure Activate(aTag : TAbstractTag); override;
  end;

implementation

resourcestring
  OffsetLang = 'Offset';
  PlusLimitLang = 'Plus Limit';
  MinusLimitLang = 'Minus Limit';
  StowLang = 'Stow';

constructor TNC32Offsets.Create(aOwner : TComponent);
begin
  inherited;

  Grid := TNudgeGrid.Create(Self);
  Grid.Parent := Self;
  Grid.Align := alClient;
  Grid.Options := Grid.Options + [goEditing];

  Grid.RowCount := 7;
  Grid.FixedRows := 1;
  Grid.FixedCols := 1;

  Grid.DefaultColWidth := 90;

  Panel := TPanel.Create(Self);
  Panel.Parent := Self;
  Panel.Align := alBottom;
  Panel.Height := 30;

  __KBC.CreateButton( [kbNumeric, kbAlpha], Self, alTop);
end;

procedure TNC32Offsets.InstallComponent(Params : TParamStrings);
begin
  inherited;
end;

procedure TNC32Offsets.Installed;
var I : Integer;
begin
  inherited;

  if SysObj.NC is TNC32 then
    NC32 := SysObj.NC as TNC32
  else
    NC32 := nil;

  Grid.ColCount := SysObj.NC.AxisCount + 1;
  for I := 0 to SysObj.NC.AxisCount - 1 do begin
    Grid.Cells[1 + I, 0] := SysObj.NC.AxisName[I];
    Grid.DefineFloatColumn(I + 1, -10000000, 1000000, 0.001, 4);
  end;

  Grid.Cells[0, 1] := OffsetLang;
  Grid.Cells[0, 2] := PlusLimitLang;
  Grid.Cells[0, 3] := MinusLimitLang;
  Grid.Cells[0, 4] := StowLang + ' A';
  Grid.Cells[0, 5] := StowLang + ' B';
  Grid.Cells[0, 6] := StowLang + ' C';

  Grid.OnFloatValue := GridFloatValue;

{  if Assigned(NC32) then
    TagEvent(Name, edgeFalling, TMethodTag, Activate); }

  ZeroOffset := TUnaryDoubleArrayTag(SysObj.Comms.TagEvent(ZeroOffsetName, edgeChange, TUnaryDoubleArrayTag, UpdateValues));
  SoftLimitPlus := TUnaryDoubleArrayTag(SysObj.Comms.TagEvent(PlusSoftLimitName, edgeChange, TUnaryDoubleArrayTag, UpdateValues));
  SoftLimitMinus := TUnaryDoubleArrayTag(SysObj.Comms.TagEvent(MinusSoftLimitName, edgeChange, TUnaryDoubleArrayTag, UpdateValues));
  StowA := TUnaryDoubleArrayTag(SysObj.Comms.TagEvent(StowNameA, edgeChange, TUnaryDoubleArrayTag, UpdateValues));
  StowB := TUnaryDoubleArrayTag(SysObj.Comms.TagEvent(StowNameB, edgeChange, TUnaryDoubleArrayTag, UpdateValues));
  StowC := TUnaryDoubleArrayTag(SysObj.Comms.TagEvent(StowNameC, edgeChange, TUnaryDoubleArrayTag, UpdateValues));
end;

procedure TNC32Offsets.Activate(aTag : TAbstractTag);
begin
  inherited;
  RedisplayValues;
end;

procedure TNC32Offsets.RedisplayValues;
var I : Integer;
begin
  SelfChanging := True;
  try
    for I := 0 to SysObj.NC.AxisCount - 1 do begin
      Grid.FloatValue[I+1, 1] := ZeroOffset.AsArrayDouble[I];
      Grid.FloatValue[I+1, 2] := SoftLimitPlus.AsArrayDouble[I];
      Grid.FloatValue[I+1, 3] := SoftLimitMinus.AsArrayDouble[I];
      Grid.FloatValue[I+1, 4] := StowA.AsArrayDouble[I];
      Grid.FloatValue[I+1, 5] := StowB.AsArrayDouble[I];
      Grid.FloatValue[I+1, 6] := StowC.AsArrayDouble[I];
    end;
  finally
    SelfChanging := False;
  end;
end;


procedure TNC32Offsets.UpdateValues(aTag : TAbstractTag);
begin
  if Visible and not SelfChanging then
    RedisplayValues;
end;

procedure TNC32Offsets.GridFloatValue(Sender: TObject;
                              ACol, ARow: Integer; Value: Double);
begin
  if not SelfChanging then begin
    SelfChanging := True;
    try
      case ARow of
        1 : ZeroOffset.AsArrayDouble[ACol - 1] := Value;
        2 : SoftLimitPlus.AsArrayDouble[ACol - 1] := Value;
        3 : SoftLimitMinus.AsArrayDouble[ACol - 1] := Value;
        4 : StowA.AsArrayDouble[ACol - 1] := Value;
        5 : StowB.AsArrayDouble[ACol - 1] := Value;
        6 : StowC.AsArrayDouble[ACol - 1] := Value;
      end;
    finally
      SelfChanging := False;
    end;
  end;
end;

initialization
  RegisterCNCClass(TNC32Offsets);
end.
