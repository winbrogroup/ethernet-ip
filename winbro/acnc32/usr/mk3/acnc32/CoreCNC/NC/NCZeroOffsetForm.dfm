object ZeroOffsetForm: TZeroOffsetForm
  Left = 334
  Top = 171
  Width = 228
  Height = 374
  Caption = 'Offsets Adjust'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 306
    Width = 220
    Height = 41
    Align = alBottom
    BevelOuter = bvLowered
    TabOrder = 0
    DesignSize = (
      220
      41)
    object BitBtn1: TBitBtn
      Left = 18
      Top = 8
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      TabOrder = 0
      Kind = bkOK
    end
    object BitBtn2: TBitBtn
      Left = 114
      Top = 8
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      TabOrder = 1
      Kind = bkCancel
    end
  end
  object InputPanel: TPanel
    Left = 0
    Top = 0
    Width = 220
    Height = 306
    Align = alClient
    BevelOuter = bvLowered
    TabOrder = 1
  end
end
