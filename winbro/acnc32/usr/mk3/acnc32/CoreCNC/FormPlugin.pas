unit FormPlugin;

interface

uses Classes, CoreCNC, CNCTypes, CNC32, IFormInterface, Windows, CNCAbs, Forms,
     Named;

type
  TPanelPlugin = class(TExpandDisplay, IHostFormInterface)
  private
    Form : IFSDFormInterface;
    FormName : string;
    GetFormInterface : TGetFSDFormInterface;
    InstallIndex : Integer;
    SectionEditTag: TAbstractTag;
    procedure DisplaySweep(aTag : TAbstractTag);
  public
    constructor Create(aOwner : TComponent); override;
    procedure InstallComponent(Params : TParamStrings); override;
    procedure Installed; override;
    procedure IShutdown; override;
    procedure Resize; override;
    procedure Open; override;
    procedure Close; override;
    procedure ClientClose; stdcall;
    procedure Help; stdcall;
  end;

  TDialogPlugin = class(TCNC, IHostFormInterface)
  private
    Form : IFSDFormInterface;
    FormName : string;
    GetFormInterface : TGetFSDFormInterface;
    procedure InvokeDialog(aTag : TAbstractTag);
    procedure CloseDialog(aTag : TAbstractTag);
    procedure DisplaySweep(aTag : TAbstractTag);
  public
    constructor Create(aOwner : TComponent); override;
    procedure InstallComponent(Params : TParamStrings); override;
    procedure Installed; override;
    procedure IShutdown; override;
    procedure ClientClose; stdcall;
    procedure Help; stdcall;
  end;

implementation

type
  T32Bit = 0..31;
  T32Bits = set of T32Bit;

var VisibleSet : T32Bits;
    InstallCount : Integer;

{ TPanelPlugin }

procedure TPanelPlugin.ClientClose;
begin
  Close;
end;

procedure TPanelPlugin.Help;
begin

end;

const
  GetFSDFormInterfaceName = 'GetFSDFormInterface';

constructor TPanelPlugin.Create(aOwner : TComponent);
begin
  inherited;
  InstallIndex := InstallCount;
  Inc(InstallCount);
end;

procedure TPanelPlugin.Open;
begin
  inherited;
  Form.Visible := Visible;
end;

procedure TPanelPlugin.Close;
begin
  if Assigned(Form) then begin
    Form.Visible := False;
    Visible:= False;
  end;
  inherited;
end;

var V, M, F : Boolean;

procedure TPanelPlugin.DisplaySweep(aTag : TAbstractTag);
begin
  V := Visible;
  M := SysObj.VisualParent.Focused;
  F := Form.Focused;

  if Visible and not SysObj.VisualParent.Active and (VisibleSet = []) then
    Form.Visible := False
  else if Visible and (SysObj.VisualParent.Active or (VisibleSet <> [])) then
    Form.Visible := True;

  if SectionEditTag.AsBoolean and Form.Visible then
    Form.Visible:= False;
    
  if Form.Focused then
    Include(VisibleSet, InstallIndex)
  else
    Exclude(VisibleSet, InstallIndex);
  Form.DisplaySweep;
end;

procedure TPanelPlugin.InstallComponent(Params: TParamStrings);
var LibName : string;
    HMod : HModule;
begin
  inherited InstallComponent(Params);
  LibName := Params.ReadString('Plugin', '*');
  if LibName = '*' then
    raise ECNCInstallFault.Create('Plugin property not set in profile "Plugin = DllName"');
  HMod := SysObj.Comms.FindPlugin(LibName);
  if HMod = 0 then
    Exit;
    //raise ECNCInstallFault.CreateFmt('Unable to load library "%s"', [LibName]);


  GetFormInterface := GetProcAddress(HMod, GetFSDFormInterfaceName);
  if not Assigned(GetFormInterface) then
    raise ECNCInstallFault.CreateFmt('Interface "%s" not present in "%s"', [GetFSDFormInterfaceName, LibName]);

  FormName := Params.ReadString('FormName', '*');
  if FormName = '*' then
    raise ECNCInstallFault.Create('Plugin property not set in profile "FormName = Name"');

  Form := GetFormInterface(Self, PChar(FormName));
  if not Assigned(Form) then
    raise ECNCInstallFault.CreateFmt('FormName "%s" not found in "%s"', [FormName, LibName]);

  Form.InstallName := Name;
  Form.Install;
  Form.BorderIcons := [];
  Form.BorderStyle := bsNone;
  //Form.FormStyle := fsStayOnTop;
  Form.Visible := False;
end;

procedure TPanelPlugin.Installed;
begin
  inherited;
  if Assigned(Form) then begin
    Form.FinalInstall;
    TagEvent(NameDisplaySweep, EdgeChange, TMethodTag, DisplaySweep);
    SectionEditTag:= TagEvent(NameSectionEditingMode, EdgeChange, TMEthodTag, nil);
  end;
end;

procedure TPanelPlugin.Resize;
begin
  inherited;
  if Assigned(Form) then begin
    Form.Left := Left;
    Form.Top := Top;
    Form.Height := Height;
    Form.Width := Width;
  end;
end;

procedure TPanelPlugin.IShutdown;
begin
  inherited;
  if Assigned(Form) then begin
    try
      Form.Shutdown;
    except
      EventLog(Name + ' Does not implement shutdown correctly, upgrade to latest SDK');
    end;
  end;

  Form := nil;
end;

{ TDialogPlugin }

procedure TDialogPlugin.ClientClose;
begin

end;

constructor TDialogPlugin.Create(aOwner: TComponent);
begin
  inherited;

end;

procedure TDialogPlugin.Help;
begin

end;

procedure TDialogPlugin.InstallComponent(Params: TParamStrings);
var LibName : string;
    HMod : HModule;
begin
  inherited;
  LibName := Params.ReadString('Plugin', '*');
  if LibName = '*' then
    raise ECNCInstallFault.Create('Plugin property not set in profile "Plugin = DllName"');
  HMod := SysObj.Comms.FindPlugin(LibName);
  if HMod = 0 then
    Exit;
    //raise ECNCInstallFault.CreateFmt('Unable to load library "%s"', [LibName]);


  GetFormInterface := GetProcAddress(HMod, GetFSDFormInterfaceName);
  if not Assigned(GetFormInterface) then
    raise ECNCInstallFault.CreateFmt('Interface "%s" not present in "%s"', [GetFSDFormInterfaceName, LibName]);

  FormName := Params.ReadString('FormName', '*');
  if FormName = '*' then
    raise ECNCInstallFault.Create('Plugin property not set in profile "FormName = Name"');

  Form := GetFormInterface(Self, PChar(FormName));
  if not Assigned(Form) then
    raise ECNCInstallFault.CreateFmt('FormName "%s" not found in "%s"', [FormName, LibName]);

  Form.InstallName := Name;
  Form.Install;
  Form.Visible := False;
end;

procedure TDialogPlugin.Installed;
begin
  inherited;
  if Assigned(Form) then begin
    Form.FinalInstall;
    TagEvent(Name, EdgeFalling, TMethodTag, InvokeDialog);
    TagEvent(Name + 'Close', EdgeFalling, TMethodTag, CloseDialog);
    TagEvent(NameDisplaySweep, EdgeChange, TMethodTag, DisplaySweep);
  end;
end;

procedure TDialogPlugin.InvokeDialog(aTag: TAbstractTag);
begin
  if (not ATag.AsBoolean) then
    Form.Execute;
end;

procedure TDialogPlugin.CloseDialog(aTag : TAbstractTag);
begin
  Form.CloseForm;
end;

procedure TDialogPlugin.DisplaySweep(aTag : TAbstractTag);
begin
  Form.DisplaySweep;
end;

procedure TDialogPlugin.IShutdown;
begin
  inherited;
  if Assigned(Form) then begin
    try
      Form.Shutdown;
    except
      EventLog(Name + ' Does not implement shutdown correctly, upgrade to latest SDK');
    end;
  end;
  Form := nil;
end;

initialization
  RegisterCNCClass(TPanelPlugin);
  RegisterCNCClass(TDialogPlugin);
end.
