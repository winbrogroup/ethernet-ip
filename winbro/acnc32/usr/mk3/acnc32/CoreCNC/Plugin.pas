unit Plugin;

interface

uses CNC32, Windows, CNCTypes, Classes, SysUtils, CoreCNC, zFileList,
     IFSDServer, INamedInterface, FaultRegistration, StringArray, IStringArray,
     INCInterface, IToolLife, IScriptInterface, AccessorPlugin, JNI;


type
  TagPointer = TAbstractTag;

  TTagEv = procedure(
    Tag: TagPointer
  ); stdcall;

  TACNC32PluginVersion = function : Cardinal; stdcall;

  TNamedPluginPublish = procedure (A, B : Integer); stdcall;
  TNamedPluginRegister = procedure (A, B : Integer); stdcall;
  TNamedPluginShutdown = procedure; stdcall;

  TNamedPluginSetNamedInterface = procedure(Named : IFSDNamedInterfaceA); stdcall;
  TNamedPluginSetNCInterface = procedure(NC : IFSDNCInterface); stdcall;

  TGetPChar = procedure(Buffer : PChar; MaxSize : Integer); stdcall;

  TNCOperatorDialogueNames = procedure (var Buffer : Widestring); stdcall;
  TNCOperatorDialogueExecute = function (const Breed, Item: Widestring): TNCDialogueResult; stdcall;
  TNCOperatorDialogueClose = procedure; stdcall;

  TOPPPluginConsume = procedure (SectionName, Buffer : PChar); stdcall;
  TOPPPluginConsumerIsDirty = function (SectionName : PChar) : Boolean; stdcall;

  TGeneralPluginSetJNIEnv = procedure (AJNIEnv: PJNIEnv); stdcall;
  TGetExtGCodes = function: WideString; stdcall;
  TGetExtGCode = function(Pattern: WideString): INCExtGcode; stdcall;


  TNamedPlugin = class(TObject)
  private
    FHMod : HMODULE;
    PluginVersion : TACNC32PluginVersion;
    NamedPluginPublish : TNamedPluginPublish;
    NamedPluginRegister : TNamedPluginRegister;
    NamedPluginShutdown : TNamedPluginShutdown;

    NamedPluginSetNamedInterface : TNamedPluginSetNamedInterface;
    NamedPluginSetNCInterface : TNamedPluginSetNCInterface;
    OPPPluginConsumerNames : TGetPChar;
    OPPPluginConsume : TOPPPluginConsume;
    OPPPluginEditorNames : TGetPChar;
    OPPPluginConsumerIsDirty : TOPPPluginConsumerIsDirty;
    GeneralPluginIOSweep : TGeneralPluginIOSweep;
    GeneralPluginFontName : TGeneralPluginFontName;
    GeneralPluginSelectProgram : TGeneralPluginSelectProgram;
    GeneralPluginSetProfilePath : TGeneralPluginSetProfilePath;
    GeneralPluginSetLoggingPath : TGeneralPluginSetProfilePath;
    GeneralPluginSetLocalPath : TGeneralPluginSetProfilePath;
    GeneralPluginSetMachSpecPath : TGeneralPluginSetProfilePath;
    GeneralPluginSetContSpecPath : TGeneralPluginSetProfilePath;
    GeneralPluginSetStringArrayInterface : TGeneralPluginSetStringArrayInterface;
    GeneralPluginSetToolLifeInterface : TGeneralPluginSetToolLifeInterface;
    GeneralPluginSetHttpServerHost: TGeneralPluginSetHttpServerHost;
    GetFSDWebServer : TGetFSDWebServer2;
    NCOperatorDialogueNames: TNCOperatorDialogueNames;
    NCOperatorDialogueExecute: TNCOperatorDialogueExecute;
    NCOperatorDialogueClose: TNCOperatorDialogueClose;
    GetScriptLibrary: TGetScriptLibrary;
    GetExtGCodes: TGetExtGCodes;
    GetExtGCode: TGetExtGCode;

    _GetIOSystemAccessor: TGetIOSystemAccessor;
    _GeneralPluginSetJNIEnv: TGeneralPluginSetJNIEnv;

    FFileName : string;
    function GetPluginVersion : string;
    function GetPluginRevision : string;
  public
    ConsumerList : TStringList;
    EditorList : TStringList;
    NCDialogueList : TStringList;
    constructor Create(aHMod : HMODULE; Comms : TAbstractCommunications; Revision : string);
    destructor Destroy; override;
    procedure DoPublish;
    procedure DoRegister;
    procedure DoConsume;
    procedure DoShutdown;
    procedure SetJNIEnv(AJNIEnv: PJNIEnv);
    function OperatorDialogue(const Breed, Item : WideString): TNCDialogueResult;
    procedure OperatorDialogueClose;
    function ContainsOperatorDialogue(const Breed: WideString): Boolean;

    function IsDirty : Boolean;
    function OkToEditSection(SectionName : string) : Boolean;
    property HMod : HMODULE read FHMod;
    property Version : string read GetPluginVersion;
    property Revision : string read GetPluginRevision;
    property FileName : string read FFileName;
    function ScriptLibrary: IScriptLibrary;
    procedure ExtGCodeList(List: TStrings);
    function GetExtGCodeLib(Pattern: string): INCExtGCode;
    function GetIOSystemAccessor(AName: string): IIOSystemAccessor;
  end;


implementation

var ActivePlugin : TNamedPlugin;

function DefaultSelectProgram (ReqPath, RespPath : PChar; Load : Boolean) : Boolean; stdcall;
var S : string;
begin
  S := ReqPath;
  if Load then
    Result := zFileList.SelectFile([sfoCanEditFilename], S)
  else
    Result := zFileList.SelectFile([], S);

  if Result then
    StrPLCopy(RespPath, S, 1023);
end;

function RevisionText(aVer : Cardinal) : string;
begin
  Result := Format('%.2x.%.2x.%.2x.%.2x', [
                aVer div $1000000,
                (aVer div $10000) and $ff,
                (aVer div $100) and $ff,
                (aVer mod $100)]);
end;

function ImageVersionNumber(FileName : string) : string;
var Data: PVSFixedFileInfo;
begin
  try
    Data := ReadVersionInfoData(FileName);
    Result := Format('%.2x.%.2x.%.2x.%.2x', [
      HiWord(Data^.dwFileVersionMS),
      LoWord(Data^.dwFileVersionMS),
      HiWord(Data^.dwFileVersionLS),
      LoWord(Data^.dwFileVersionLS) ]);
  except
    Result := '00.00.00.00';
  end;
end;

constructor TNamedPlugin.Create(aHMod : HMODULE; Comms : TAbstractCommunications; Revision : string);

var Buffer : array [0..255] of Char;
    WBuffer : WideString;
//    Tmp : string;
begin
  FHMod := aHMod;

  if (GetModuleFileName(HMod, Buffer, 256) = 0) then
    raise ECNCInstallFault.CreateFmt('Failed to obtain plugin filename winerr[%d]', [GetLastError]);
  FFileName := Buffer;

  PluginVersion := GetProcAddress(HMod, 'ACNC32PluginVersion');
  if not Assigned(PluginVersion) then
    raise Exception.CreateFmt('Requested plugin [%s] Does not export version information', [FileName]);

  if PluginVersion > SDKPluginVersion then begin
    MessageLogFmt('The plugin [%s] built with version %s of the SDK, you are attempting to install, cannot be ' +
                  'guarenteed to work with this version of ACNC32 as the SDK version is %s. '+
                  'You will need to update to a later version of ACNC32' + CarRet + CarRet,
                  [FileName, RevisionTexT(PluginVersion), RevisionText(SDKPluginVersion)]);
    raise Exception.Create('Plugin too new, check message.log');
  end else if PluginVersion < SDKPluginVersion then begin
    MessageLogFmt('The plugin [%s] built with  version %s of the SDK you are attempting to install, cannot be ' +
                  'guarenteed to work with this version of ACNC32. '+
                  'You will need to update to a version of the dll built with version %s of the SDK' + CarRet + CarRet,
                  [FileName, RevisionText(PluginVersion), RevisionText(SDKPluginVersion)]);
    raise Exception.Create('Plugin too old, check message.log');
  end;

//  PluginRevision := GetProcAddress(HMod, 'ACNC32PluginRevision');
  NamedPluginPublish := GetProcAddress(HMod, 'NamedPluginPublish');
  NamedPluginRegister := GetProcAddress(HMod, 'NamedPluginRegister');
  NamedPluginShutdown := GetProcAddress(HMod, 'NamedPluginShutdown');

  NamedPluginSetNamedInterface := GetProcAddress(HMod, 'NamedPluginSetNamedInterface');
  NamedPluginSetNCInterface := GetProcAddress(HMod, 'NamedPluginSetNCInterface');

  OPPPluginConsumerNames := GetProcAddress(HMod, 'OPPPluginConsumerNames');
  OPPPluginConsume := GetProcAddress(HMod, 'OPPPluginConsume');
  OPPPluginConsumerIsDirty := GetProcAddress(HMod, 'OPPPluginConsumerIsDirty');

  OPPPluginEditorNames := GetProcAddress(HMod, 'OPPPluginEditorNames');

  GeneralPluginIOSweep := GetProcAddress(HMod, 'GeneralPluginIOSweep');
  GeneralPluginFontName := GetProcAddress(HMod, 'GeneralPluginFontName');
  GeneralPluginSelectProgram := GetProcAddress(HMod, 'GeneralPluginSelectProgram');
  GeneralPluginSetProfilePath := GetProcAddress(HMod, 'GeneralPluginSetProfilePath');
  GeneralPluginSetLoggingPath := GetProcAddress(HMod, 'GeneralPluginSetLoggingPath');

  GeneralPluginSetStringArrayInterface := GetProcAddress(HMod, 'GeneralPluginSetStringArrayInterface');
  GeneralPluginSetLocalPath := GetProcAddress(HMod, 'GeneralPluginSetLocalPath');
  GeneralPluginSetMachSpecPath := GetProcAddress(HMod, 'GeneralPluginSetMachSpecPath');
  GeneralPluginSetContSpecPath := GetProcAddress(HMod, 'GeneralPluginSetContSpecPath');
  GeneralPluginSetToolLifeInterface := GetProcAddress(HMod, 'GeneralPluginSetToolLifeInterface');
  GeneralPluginSetHttpServerHost:= GetProcAddress(HMod, 'GeneralPluginSetHttpServerHost');
  NCOperatorDialogueExecute := GetProcAddress(HMod, 'NCOperatorDialogueExecute');
  NCOperatorDialogueNames := GetProcAddress(HMod, 'NCOperatorDialogueNames');
  NCOperatorDialogueClose := GetProcAddress(HMod, 'NCOperatorDialogueClose');
  GetFSDWebServer := GetProcAddress(HMod, 'GetFSDWebServer2');
  GetScriptLibrary := GetProcAddress(HMod, 'GetScriptLibrary');
  _GetIOSystemAccessor:= GetProcAddress(HMod, 'GetIOSystemAccessor');

  _GeneralPluginSetJNIEnv:= GetProcAddress(HMod, 'GeneralPluginSetJNIEnv');

  GetExtGCodes:= GetProcAddress(HMod, 'GetExtGCodes');
  GetExtGCode:= GetProcAddress(HMod, 'GetExtGCode');
  
  ConsumerList := TStringList.Create;
  EditorList := TStringList.Create;
  NCDialogueList := TStringList.Create;

  if Assigned(GeneralPluginSetLocalPath) then begin
    GeneralPluginSetLocalPath(PChar(LocalPath))
  end;

  if Assigned(GeneralPluginSetMachSpecPath) then begin
    GeneralPluginSetMachSpecPath(PChar(MachSpecPath))
  end;

  if Assigned(GeneralPluginSetContSpecPath) then begin
    GeneralPluginSetContSpecPath(PChar(ContractSpecificPath))
  end;

  if Assigned(GeneralPluginSetProfilePath) then begin
    GeneralPluginSetProfilePath(PChar(LivePath))
  end;

  if Assigned(GeneralPluginSetLoggingPath) then begin
    GeneralPluginSetLoggingPath(PChar(LoggingPath))
  end;

  if Assigned(OPPPluginConsumerNames) then begin
    OPPPluginConsumerNames(Buffer, 256);
    ConsumerList.Text := Buffer;
  end;

  if Assigned(OPPPluginEditorNames) then begin
    OPPPluginEditorNames(Buffer, 256);
    EditorList.Text := Buffer;
  end;

  if Assigned(NCOperatorDialogueNames) then begin
    NCOperatorDialogueNames(WBuffer);
    NCDialogueList.Text := UpperCase(WBuffer);
  end;

  if Assigned(GeneralPluginSetStringArrayInterface) then
    GeneralPluginSetStringArrayInterface(SysObj.StringArraySet);

  if Assigned(NamedPluginSetNamedInterface) then
    NamedPluginSetNamedInterface(Comms);

  if Assigned(GeneralPluginIOSweep) then
    GeneralPluginIOSweep(@PlcTick.I64);

  if Assigned(GeneralPluginFontName) then
    GeneralPluginFontName(PChar(SysObj.FontName));

  if Assigned(GeneralPluginSelectProgram) and not Assigned(SysObj.AlternateLoad) then
    SysObj.AlternateLoad := GeneralPluginSelectProgram;

  if not Assigned(Comms.FSDWebServer) and Assigned(GetFSDWebServer) then begin
    Comms.FSDWebServer := GetFSDWebServer(Comms);
    Comms.FSDWebServer.SetActive(True);
  end;

  if Assigned(GeneralPluginSetHttpServerHost) then
    GeneralPluginSetHttpServerHost(SysObj.HttpServerHost);

  if Revision <> '' then begin
    if CompareText(Revision, Self.Revision) <> 0 then begin
      MessageLogFmt('The plugin [%s] %s you are attempting to install, failed ' +
                  'authentication.'+
                  'CNC.INI requested %s' + CarRet + CarRet,
                  [FileName, Self.Revision, Revision]);
      raise Exception.Create('Plugin wrong revision, check message.log');
    end;
  end;
end;

procedure TNamedPlugin.DoShutdown;
begin
  if Assigned(NamedPluginShutdown) then
    NamedPluginShutdown;

  if Assigned(GeneralPluginSetToolLifeInterface) then
    GeneralPluginSetToolLifeInterface(nil);
end;

destructor TNamedPlugin.Destroy;
begin
  if Assigned(NamedPluginSetNamedInterface) then
    NamedPluginSetNamedInterface(nil);

  if Assigned(GeneralPluginSetStringArrayInterface) then
    GeneralPluginSetStringArrayInterface(SysObj.StringArraySet);

  FreeLibrary(HMod);
  inherited Destroy;
end;



function TNamedPlugin.GetPluginVersion : string;
begin
  Result := RevisionText(PluginVersion);
end;

function TNamedPlugin.GetPluginRevision : string;
begin
  Result := ImageVersionNumber(FileName);
end;


procedure TNamedPlugin.DoPublish;
begin
  if Assigned(NamedPluginSetNCInterface) then
    NamedPluginSetNCInterface(SysObj.NC);

  ActivePlugin := Self;
  if Assigned(NamedPluginPublish) then
    NamedPluginPublish(SysObj.Heads, 0);
end;

procedure TNamedPlugin.DoRegister;
begin
  ActivePlugin := Self;
  if Assigned(GeneralPluginSetToolLifeInterface) then
    GeneralPluginSetToolLifeInterface(SysObj.ToolLife);

  if Assigned(NamedPluginRegister) then
    NamedPluginRegister(SysObj.Heads, 0);
end;

procedure TNamedPlugin.DoConsume;
var I : Integer;
    S : TStringList;
    Buffer : array [0..1023] of Char;
begin
  if Assigned(OPPPluginConsume) then begin
    S := TStringList.Create;
    try
      for I := 0 to ConsumerList.Count - 1 do begin
        S.Clear;
        SysObj.FileManager.SectionRead(ConsumerList[I], S);
        try
          OPPPluginConsume(PChar(ConsumerList[I]), PChar(S.Text));
        except
          on E: Exception do begin
            StrPLCopy(Buffer, E.Message, 1023);
            SysObj.FaultInterface.SetFault(SysObj.Comms, CoreFaults[cfSysWarning], [Buffer], nil);
          end;
        end
      end;
    finally
      S.Free
    end;
  end;
end;

function TNamedPlugin.IsDirty : Boolean;
var I : Integer;
begin
  Result := True;
  if Assigned(OPPPluginConsumerIsDirty) then begin
    for I := 0 to ConsumerList.Count - 1 do begin
      if OPPPluginConsumerIsDirty(PChar(ConsumerList[I])) then
        Exit;
    end;
  end;
  Result := False;
end;

{ This logic only works if only one section can be dirty! }
function TNamedPlugin.OkToEditSection(SectionName : string) : Boolean;
var I : Integer;
begin
  Result := False;
  if Assigned(OPPPluginConsumerIsDirty) then begin
    for I := 0 to ConsumerList.Count - 1 do begin
      if OPPPluginConsumerIsDirty(PChar(ConsumerList[I])) then begin
        if CompareText(ConsumerList[I], SectionName) = 0 then begin
          Result := True;
          Exit;
        end else begin
          Exit;
        end;
      end;
    end;
  end;
  Result := True;
end;

function TNamedPlugin.OperatorDialogue(const Breed,
  Item: WideString): TNCDialogueResult;
begin
  if Assigned(NCOperatorDialogueExecute) then begin
    if Assigned(NCOperatorDialogueClose) then begin
      Result := NCOperatorDialogueExecute(Breed, Item);
    end else begin
      raise Exception.Create('Close method for dialogue not registed, this is unsafe, please contact your vendor');
    end;
  end else begin
    Result := ncdrError;
  end;
end;

procedure TNamedPlugin.OperatorDialogueClose;
begin
  if Assigned(NCOperatorDialogueClose) then begin
    NCOperatorDialogueClose;
  end;
end;

function TNamedPlugin.ContainsOperatorDialogue(
  const Breed: WideString): Boolean;
begin
  Result := NCDialogueList.IndexOf(Uppercase(Breed)) <> -1;
end;

function TNamedPlugin.ScriptLibrary: IScriptLibrary;
begin
  Result:= nil;
  if Assigned(GetScriptLibrary) then
    Result:= GetScriptLibrary;
end;

function TNamedPlugin.GetIOSystemAccessor(AName: string): IIOSystemAccessor;
begin
  Result:= nil;
  if Assigned(_GetIOSystemAccessor) then begin
    Result:= _GetIOSystemAccessor(AName);
  end;
end;

procedure TNamedPlugin.SetJNIEnv(AJNIEnv: PJNIEnv);
begin
  if Assigned(_GeneralPluginSetJNIEnv) then begin
    _GeneralPluginSetJNIEnv(AJNIEnv);
  end else
    EventLog(' +++' + Self.FileName + ' does not support JNI');
end;

function TNamedPlugin.GetExtGCodeLib(Pattern: string): INCExtGCode;
var Tmp: WideString;
begin
  Tmp:= Pattern;
  Result:= nil;
  if Assigned(GetExtGCode) then
    Result:= GetExtGCode(Tmp);
end;

procedure TNamedPlugin.ExtGCodeList(List: TStrings);
begin
  if Assigned(GetExtGCodes) then begin
    List.Text:= List.Text + GetExtGCodes;
  end;
end;

end.
