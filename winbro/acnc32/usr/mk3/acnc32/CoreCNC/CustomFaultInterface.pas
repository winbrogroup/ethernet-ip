unit CustomFaultInterface;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  CoreCNC, CNC32, CNCTypes, Named, IniFiles, InterlockForm, FaultRegistration,
  DynamicHelp, SyncObjs, INamedInterface, LogSyncObjs, ExtCtrls;

const
  NameNewLevelMessage = 'NewFaultLevelMessage';

  { Fault logging added 03-07-2002

    Purpose : To log faults on a set of predetermined conditions.
    Method : On an external permissive condition each SetFault will
             providing the fault is high enough (user selectable)
             add a new row to /logging/fault.cvs. This row will contain
             the following data.

             Date Time, Fault text, Fault level [, user, selectable, tags ]
    Interface : [Name]LoggingEnable < External signal to enable subsystem
    Configuration : cnc.ini profile section
                LogTagCount = Number of tags to append
                LogTagn = NamedLayerTag
  }
type
  TCustomFaultInterface = class(TAbstractFaultInterface)
  private
    FFaultChange   : Boolean;
    FaultCount     : Integer;            // Maintanined for unique handles
    {
    PLCFaultMap    : array [0..MAX_PLC_ERRORS_DW - 1] of Cardinal;
    PLCFaultMap_IH  : array [0..MAX_PLC_ERRORS_DW - 1] of TAbstractTag;
    PlcFid : array [0..MAX_PLC_ERRORS_DW - 1, T32Bit] of FHandle;
    }
    NewLevelMessage : TAbstractTag;
//    FaultLock : TLoggingCriticalSection;

    // Fault logging sub system
    LoggingTags : array [0..9] of TAbstractTag;
    LoggingTagNames : array [0..9] of string;
    LoggingTagCount : Integer;
    LoggingEnableTag : TAbstractTag;
    LoggingLevelTag : TAbstractTag;
    DispatchTimer : TTimer;
    DispatchList : TStringList;
    DispatchTag : TAbstractTag;

    procedure DispatchRecord(Sender : TObject);
    procedure LoggingFault(const F : TFaultRegistration; const Err : string);
    //procedure PLCFault(aTag : TAbstractTag);
    procedure ResetUpdate(aTag : TAbstractTag);
    //function  PLCFaultReset(handle : FHandle) : boolean;
    //procedure RefreshFaultMap;
    function FaultTxText(aOwner : TCNC; const F : TFaultRegistration; Error : string) : string;
    function FaultReportText(aOwner : TCNC; const F : TFaultRegistration; Error : string) : string;
  protected
    procedure SetFaultLevel;
    procedure FaultReset(aTag : TAbstractTag); override;
    procedure IOResetComplete(aTag : TAbstractTag); override;
    procedure InterlockFault(Text : string);
    procedure CNCMFaultInterlock(var Message : TMessage); message CNCM_FAULT_INTERLOCK;
    procedure Reset(aTag : TAbstractTag); override;
    procedure ResetRW(aTag : TAbstractTag); override;
  public
    constructor Create(aOwner : TComponent); override;
    destructor Destroy; override;
    procedure InstallComponent(Params : TParamStrings); override;
    procedure Installed; override;
    procedure CheckInstallation; override;
    procedure FaultResetID(handle : FHandle); override;
    function SetFault(aOwner : TCNC; const F : TFaultRegistration; const Params : array of const; Reset : TFaultResetMethod) : FHandle; override;
    function SetFaultStdCall(aOwner : TCNC; const F : TFaultRegistration; const Params : array of const; Reset : TFaultResetStdcall) : FHandle; override;
    procedure UpdateFault(FID : FHandle; const Args : array of const); override;
    procedure ResetInterlock; override;
    procedure ResetRewind; override;
  end;

implementation


constructor TCustomFaultInterface.Create(aOwner : TComponent);
begin
  inherited;
  DispatchTimer := TTimer.Create(Self);
  DispatchList := TStringList.Create;
//  FaultLock := TLoggingCriticalSection.Create('Fault');
end;


destructor TCustomFaultInterface.Destroy;
begin
{  if Assigned(FaultLock) then
    FaultLock.Free; }
  DispatchTimer.Free;
  inherited destroy;
end;


procedure TCustomFaultInterface.InstallComponent(Params : TParamStrings);
var I : Integer;
begin
  inherited InstallComponent(Params);

  {
  for i := 0 to MAX_PLC_ERRORS_DW - 1 do begin
    PLCfaultMap_IH[i] := TagEvent(namePLCFaultMap + Inttostr(i), edgeChange, TIntegerTag, PLCFault);
    if PLCfaultMap_IH[i] = nil then
      raise ECNCINstallFault.CreateFmt(TagRequiredText, [Self.Name, namePLCFaultMap + inttostr(i)]);
  end;
  }

  LoggingTagCount := Params.ReadInteger('LoggingTagCount', 0);
  LoggingTagCount := LoggingTagCount mod 10;
  for I := 0 to LoggingTagCount - 1 do begin
    LoggingTagNames[I] := Params.ReadString('LoggingTag' + IntToStr(I), NotConfiguredTagName);
  end;

  NewLevelMessage := TagPublish(NameNewLevelMessage, TStringTag);
  DispatchTag := TagPublish(Name + 'Report', TStringTag);
end;

procedure TCustomFaultInterface.Installed;
begin
  inherited installed;
  FFaultLevel := FaultEStop;
  FaultlevelTag.AsInteger := Ord(FFaultlevel);
  //RefreshFaultMap;
  TagEvent (NameDisplaySweep, edgeChange, TMethodTag, ResetUpdate);
  DispatchTimer.OnTimer := DispatchRecord;
  DispatchTimer.Interval := 5000;
  DispatchTimer.Enabled := True;

  LoggingEnableTag := TagEvent(Name + 'LoggingEnable', EdgeChange, TMethodTag, nil);
  LoggingLevelTag := TagEvent(Name + 'LoggingLevel', EdgeChange, TIntegerTag, nil);
end;

procedure TCustomFaultInterface.CheckInstallation;
var I : Integer;
begin
  inherited;

  for I := 0 to LoggingTagCount - 1 do begin
    LoggingTags[I] := SysObj.Comms.TagEvent(LoggingTagNames[I], EdgeChange, TStringTag, nil);
  end;
end;



function TCustomFaultInterface.FaultTxText(aOwner : TCNC; const F : TFaultRegistration; Error : string) : string;
begin
  Result := IntToHex(Cardinal(F.Dispatch), 8) + ':' +
            IntToHex(Ord(F.Level), 2) + ':' +
            FaultReportText(aOwner, F, Error);
end;

function TCustomFaultInterface.FaultReportText(aOwner : TCNC; const F : TFaultRegistration; Error : string) : string;
begin
  Result := IntToStr(F.Ndx) + ': ' + Error;
end;


procedure TCustomFaultInterface.DispatchRecord(Sender : TObject);
var I: Integer;
begin
  if DispatchList.Count > 0 then begin
    DispatchTag.AsString := Self.DispatchList[0];
    DispatchList.Delete(0);
  end;

  for I:= DispatchList.Count - 1 downto 100 do begin
    DispatchList.Delete(I);
  end;
end;

// Date Time, Fault text, Fault level [, user, selectable, tags ]

procedure TCustomFaultInterface.LoggingFault(const F : TFaultRegistration; const Err : string);
var Stream : TFileStream;
    Tmp : string;
    I : Integer;
    FileName : string;
begin
  if (InstallState = InstallStateDone) then
    if (F.Level <> FaultInterlock) and
       Assigned(LoggingEnableTag) and
       LoggingEnableTag.AsBoolean and
       (LoggingLevelTag.AsInteger <= Ord(F.Level)) then begin
      try
        FileName := LoggingPath + 'FaultLog.csv';

        if not FileExists(FileName) then
          Stream := TFileStream.Create(FileName, fmCreate)
        else
          Stream := TFileStream.Create(FileName, fmOpenWrite);

        try
          Tmp := CSVField(StorageDate(Now)) + ',' +
                 CSVField(Err) + ',' +
                 CSVField(IntToStr(Ord(F.Level))) + ',';

          for I := 0 to LoggingTagCount - 1 do begin
            if Assigned(LoggingTags[I]) then
              Tmp := Tmp + CSVField(LoggingTags[I].AsString) + ',';
          end;

          DispatchList.Add(Tmp);
          Tmp := Tmp + CarRet;
          Stream.Seek(0, soFromEnd);
          Stream.Write(PChar(Tmp)^, Length(Tmp));
        finally
          Stream.Free;
        end;
      except
        on E : Exception do
          EventLogFmt('Unable to write fault logging "%s"', [E.Message] );
      end;
    end;
end;


function TCustomFaultInterface.SetFault(aOwner : TCNC; const F : TFaultRegistration; const Params : array of const; Reset : TFaultResetMethod) : FHandle;
  // This assumes the caller has locked the list
  function FindError(Error : string) : TFaultRecord;
  var I : Integer;
  begin
    I := FFaultList.IndexOf(Error);
    Result := nil;
    if I <> -1 then
      Result := TFaultRecord(FFaultList.Objects[I]);
  end;

var
  ThisFault : TFaultRecord;
  ThisChanged : Boolean;
  Error : string;
  FaultReport : string;
begin
  SysObj.Layout.ScreenSaver := False;
  Error := Format(F.Text, Params);
  FaultReport := FaultReportText(aOwner, F, Error);

  DynamicHelp.DynamicHelpSystem.Add(F.Help, FaultReport);

  if F.Level = faultInterlock then begin
    InterlockFault(FaultReport);
    IOFaultTag.AsString := FaultTxText(aOwner, F, Error);
    Result := 0;
    Exit;
  end;

  ThisChanged := False;
  EnterFaultLock;

  try
    // Moved logging fault into critical section
    LoggingFault(F, FaultReport);

    ThisFault := FindError(FaultReportText(aOwner, F, Error));
    { if the fault exists ensure the new error level is maintained }
    if Assigned(ThisFault) then begin
      if ThisFault.Level <> F.Level then begin
        ThisFault.Level := F.Level;
        ThisFault.Reset := Reset;
        ThisChanged := True;
      end;
      Result := ThisFault.FaultH;
      Exit; // The exit is delayed until after the finally clause.
    end;
  finally
    ExitFaultLock;
    if(ThisChanged) then begin
      SetFaultLevel;
      FaultChangeTag.AsInteger := Ord(FfaultChange);
      IOFaultTag.AsString := FaultTxText(aOwner, F, Error);
      EventLog(FaultReport);
    end;
  end;

  { if the fault did not exist, add it to the fault list }
  Inc(FaultCount);
  Result := FaultCount;

  ThisFault := TFaultRecord.Create;
  ThisFault.Text   := FaultReport;
  ThisFault.Owner  := aOwner;
  ThisFault.Level  := F.Level;
  ThisFault.Reset  := Reset;
  ThisFault.FaultH := FaultCount;
  ThisFault.Reg:= @F;

  EnterFaultLock;
  try
    FFaultList.AddObject(FaultReport, ThisFault);
  finally
    ExitFaultLock;
  end;

  SetFaultLevel;

  FFaultChange := not FFaultChange;
  FaultChangeTag.AsInteger := Ord(FfaultChange);
  IOFaultTag.AsString := FaultTxText(aOwner, F, Error);
  EventLog(FaultReport);
end;

function TCustomFaultInterface.SetFaultStdCall(aOwner : TCNC; const F : TFaultRegistration; const Params : array of const; Reset : TFaultResetStdcall) : FHandle;
var Faults : TFaultRecord;
    I : Integer;
begin
  Result := SetFault(aOwner, F, Params, nil);
  EnterFaultLock;
  try
    for I := 0 to FFaultList.Count - 1 do begin
      Faults := FFaultList.Objects[i] as TFaultRecord;
      if Faults.FaultH = Result then begin
        Faults.ResetStdCall := Reset;
        Exit;
      end;
    end;
  finally
    ExitFaultLock;
  end;
//  EventLog('TCustomFaultInterface.SetFaultStdCall failed to set callback');
end;

procedure TCustomFaultInterface.SetFaultLevel;
var i         : integer;
    faults    : TFaultRecord;
    level     : TFaultLevel;
    ErrorText : string;
    LevelText : string;
begin
  level := faultNone;
  EnterFaultLock;
  try
    for I := 0 to FFaultList.Count -1 do begin
      Faults := FFaultList.Objects[i] as TFaultRecord;
      if Ord(Faults.Level) > Ord(level) then begin
        Level := Faults.level;
        ErrorText := Faults.Text;
      end;
    end;
  finally
    ExitFaultLock;
  end;

  if Level <> FFaultLevel then begin
    FfaultLevel := level;
    case level of
      faultMessageOnly:  LevelText := 'Normal';
      faultWarning:      LevelText := 'Warning';
      faultStopCycle:    LevelText := 'CycleStop';
      faultRewind:       LevelText := 'Rewind';
      faultEstop:        LevelText := 'EStop';
      faultFatal:        LevelText := 'Fatal';
      faultInterlock:    LevelText := 'Interlock';
    end;

    if ErrorText <> '' then begin
      NewLevelMessage.AsString := Format('%s,%s', [LevelText,ErrorText]);
    end else begin
      NewLevelMessage.AsString := Format('%d,NoError', [Ord(FfaultLevel)]);
    end;

    FaultLevelTag.AsInteger := Ord(FfaultLevel);
  end;
end;

procedure TCustomFaultInterface.FaultResetID(handle : FHandle);
var
  faults      : TFaultRecord;
  I           : Integer;
begin
  EnterFaultLock;
  try
    for I := 0 to FFaultList.Count - 1 do begin
      faults := FFaultList.Objects[i] as TFaultRecord;
      if Faults.FaultH = handle then begin
        Faults.Level := faultNone;
        faults.Reset := nil;
        Exit;
      end;
    end;
  finally
    ExitFaultLock;
    SetFaultLevel;
    FaultChangeTag.AsInteger := Ord(FFaultChange);
  end;

  EventLog('TCustomFaultInterface.faultResetID fault handle not found');
end;

procedure TCustomFaultInterface.UpdateFault(FID : FHandle; const Args : array of const);
var Faults : TFaultRecord;
    I : Integer;
    Tmp : string;
begin
  EnterFaultLock;
  try
    for I := 0 to FFaultList.Count - 1 do begin
      Faults := FFaultList.Objects[I] as TFaultRecord;
      if Faults.FaultH = Fid then begin
        Tmp := Format(Faults.Reg.Text, Args);
        Faults.Text   := FaultReportText(TCNC(Faults.Owner), Faults.Reg^, Tmp);
        FFaultList[I] := Faults.Text;
        Exit;
      end;
    end;
  finally
    ExitFaultLock;
  end;
end;

procedure TCustomFaultInterface.FaultReset(aTag : TAbstractTag);
  procedure DeleteFault(var position : integer; const Faults : TFaultRecord);
  begin
    if Assigned(Faults.Reset) then begin
      if not Faults.Reset(Faults.FaultH) then begin
        Inc(Position);
        Exit;
      end;
    end else if Assigned(Faults.ResetStdcall) then begin
      if not Faults.ResetStdcall(Faults.FaultH) then begin
        Inc(Position);
        Exit;
      end;
    end;

    if Faults.Level = FaultRewind then begin
      Inc(Position);
      Exit;
    end;

    Faults.Level := FaultNone;
    Faults.Free;
    FFaultlist.Delete(Position);
  end;
var
  faults      : TFaultRecord;
  i, position : integer;
begin
  TInterlockFrm.CloseInterlock;

  Position := 0;
  EnterFaultLock;
  try
    for I := 0 to FfaultList.count - 1 do begin
      Faults := FFaultList.Objects[position] as TFaultRecord;
      case Faults.level of
        faultNone,
        faultMessageOnly,
        faultWarning,
        faultStopCycle,
        faultRewind,
        faultEstop : begin
          DeleteFault(position, faults);
        end;
        faultFatal : begin
{          if FAccessLevel = AccessLevelOEM2 then
            DeleteFault(Position, Faults)
          else }
            Position := Position + 1;
        end;
      end;
    end;
  finally
    ExitFaultLock;
  end;

  SetFaultLevel;
  FfaultChange := true;
  FaultChangeTag.AsInteger := Ord(FfaultChange);
end;


{ Reset responds to the input ReqReset This then Events all interested parties }
procedure TCustomFaultInterface.Reset(aTag : TAbstractTag);
begin
  ResetTag.AsBoolean := True;
end;

procedure TCustomFaultInterface.ResetUpdate(aTag : TAbstractTag);
var I, RCount, RWCount : Integer;
    Comp: TCNC;
begin
  if ResetTag.AsBoolean then begin
    RCount := 0;
    for I := 0 to ResetCompleteList.Count - 1 do begin
      Comp := TCNC(ResetCompleteList[I]);
      if Comp.ResetComplete then
        Inc(RCount);
    end;
    if (RCount = ResetCompleteList.Count) and
       IOResetCompleteTag.AsBoolean then
       ResetTag.AsBoolean := False;
  end;

  if ResetRWTag.AsBoolean then begin
    RWCount := 0;
    for I := 0 to ResetRWCompleteList.Count - 1 do begin
      Comp := TCNC(ResetRWCompleteList[I]);
      if Comp.ResetRewindComplete then
        Inc(RWCount);
    end;
    if (RWCount = ResetRWCompleteList.Count) and
       IOResetCompleteTag.AsBoolean then begin
       ResetRWTag.AsBoolean := False;
       SysObj.NC.ReleaseModeChangeLock;
    end;
  end;
end;

procedure TCustomFaultInterface.IOResetComplete(aTag : TAbstractTag);
begin
end;


type
  TFaultMessenger = class
    Text : string;
  end;

procedure TCustomFaultInterface.InterlockFault(Text : string);
var Fault : TFaultMessenger;
begin
  Fault := TFaultMessenger.Create;
  Fault.Text := Text;
  PostMessage(Self.Handle, CNCM_FAULT_INTERLOCK, Integer(Fault), 0);
end;

procedure TCustomFaultInterface.CNCMFaultInterlock(var Message : TMessage);
begin
  TInterlockFrm.Execute(TFaultMessenger(Message.WParam).Text);
  SysObj.Comms.CNCFormChange;
end;

procedure TCustomFaultInterface.ResetInterlock;
begin
  TInterlockFrm.CloseInterlock;
end;


{ ResetRW responds to the input ReqResetRW This then Events all interested parties }
procedure TCustomFaultInterface.ResetRW(aTag : TAbstractTag);
begin
  if ATag.AsBoolean then
    ResetRewind;
end;


procedure TCustomFaultInterface.ResetRewind;
var I : Integer;
    Faults : TFaultRecord;
begin
  EnterFaultLock;
  try
    for I := 0 to FFaultList.Count - 1 do begin
      Faults := FFaultList.Objects[I] as TFaultRecord;
      if (Faults.Level = FaultRewind) then begin
        Faults.Level := FaultStopCycle;
      end;
    end;
  finally
    ExitFaultLock;
  end;
  if not ResetRWTag.AsBoolean then begin
    ResetRWTag.AsBoolean:= True;
    SysObj.NC.AcquireModeChangeLock;
  end;
end;

initialization

  RegisterCNCClass(TCustomFaultInterface);
end.
