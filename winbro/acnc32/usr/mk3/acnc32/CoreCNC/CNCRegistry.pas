unit CNCRegistry;

interface

uses SysUtils, Classes, Registry, Windows;

type
  TCNCRegistry = class (TRegistry)
  private
    procedure RecurseSave(S : TStringList; Branch : string);
    function RootKeyName(Key : HKEY; WithTrailing : Boolean = True) : string;
    procedure WrapString(var aText : string);
    function MangledPath(path: string): string;
  public
    Mode64: Boolean;
    constructor Create;
    function SaveBranch(const Branch, FileName : string) : Boolean;
  end;

implementation


constructor TCNCRegistry.Create;
begin
  inherited;
  Self.RootKey := Windows.HKEY_LOCAL_MACHINE;
end;

procedure TCNCRegistry.WrapString(var aText : string);
var I : Integer;
begin
  I := 1;
  while I <= Length(aText) do begin
    if (aText[I] = '\') or (aText[I] = '"') then begin
      System.Insert('\', aText, I);
      Inc(I);
    end;

    Inc(I);
  end;

  System.Insert('"', aText, 1);
  System.Insert('"', aText, Length(aText) + 1);
end;

function TCNCRegistry.RootKeyName(Key : HKEY; WithTrailing : Boolean = True) : string;
begin
  case Key of
    HKEY_CLASSES_ROOT : Result := 'HKEY_CLASSES_ROOT';
    HKEY_CURRENT_USER : Result := 'HKEY_CURRENT_USER';
    HKEY_LOCAL_MACHINE : Result := 'HKEY_LOCAL_MACHINE';
    HKEY_USERS : Result := 'HKEY_USERS';
    HKEY_PERFORMANCE_DATA : Result := 'HKEY_PERFORMANCE_DATA';
    HKEY_CURRENT_CONFIG : Result := 'HKEY_CURRENT_CONFIG';
    HKEY_DYN_DATA : Result := 'HKEY_DYN_DATA';
  else
    Result := 'HUNKNOWN_KEY';
  end;

  if WithTrailing then
    Result := Result + '\';
end;

type
  AByte = array [0..65535] of Byte;
  PAByte = ^AByte;

procedure TCNCRegistry.RecurseSave(S : TStringList; Branch : string);

  procedure SaveBinaryData(const aValue : string);
  var Buffer : PAByte;
      BufSize : Integer;
      I : Integer;
      Tmp : string;
  begin
    BufSize := GetDataSize(aValue);
    if BufSize > 0 then begin
      GetMem(Buffer, BufSize + 10);
      try
        Tmp := '"' + aValue + '"=hex:';
        ReadBinaryData(aValue, Buffer^, BufSize);
        for I := 0 to BufSize - 2 do begin
          if Length(Tmp) > 75 then begin
            S.Add(Tmp + '\');
            Tmp := '  ';
          end;

          Tmp := Tmp + IntToHex(Buffer[I], 2) + ',';
        end;

        Tmp := Tmp + IntToHex(Buffer[BufSize-1], 2);
        S.Add(Tmp);
      finally
        FreeMem(Buffer);
      end;
    end;
  end;



var Local : TStringList;
    I : Integer;
    Tmp : string;
    CPath : string;
begin
  Local := TStringList.Create;
  try
    if OpenKey(Branch, False) then begin
      GetValueNames(Local);
      S.Add('');
      S.Add('['+ RootKeyName(RootKey) + MangledPath(CurrentPath) + ']');

      for I := 0 to Local.Count - 1 do begin
        case GetDataType(Local[I]) of
          rdUnknown : begin
          end;
          rdString, rdExpandString : begin
            Tmp := ReadString(Local[I]);
            WrapString(Tmp);
            S.Add('"' + Local[I] + '"=' + Tmp);
          end;
          rdInteger : begin
            S.Add('"' + Local[I] + '"=DWORD:' + IntToHex(ReadInteger(Local[I]), 8));
          end;
          rdBinary : begin
            SaveBinaryData(Local[I]);
          end;
        end;
      end;

      GetKeyNames(Local);
      for I := 0 to Local.Count - 1 do begin
        CPath := '\' + CurrentPath;
        RecurseSave(S, Local[I]);
        OpenKey(CPath, False);
      end;
    end;
  finally
    Local.Free;
  end;
end;

const RegEditFormat5 = 'Windows Registry Editor Version 5.00';
const RegEditFormat4 = 'REGEDIT4';

function TCNCRegistry.SaveBranch(const Branch, FileName : string) : Boolean;
var S : TStringList;
begin
  Result := False;
  S := TStringList.Create;
  try
    try
      S.Add(RegEditFormat4);
      if OpenKey(Branch, False) then begin
        RecurseSave(S, Branch);
        S.Add('');
        S.SaveToFile(FileName);
        Result := True;
      end;
    except
      Result := False;
    end;
  finally
    S.Free;
  end;
end;


function TCNCRegistry.MangledPath(path: string): string;
begin
  if Mode64 then begin
    Result:= 'Software\Wow6432Node' + Copy(path, Length('Software') + 1, length(path));
  end else begin
    result:= path;
  end;
end;

end.
