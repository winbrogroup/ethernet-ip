unit HeidenhainTable;

interface

uses Classes, SysUtils, StreamTokenizer;

type
  THColumnType = (
    hColNumeric,
    hColText
  );

  THLanguage = (
    hLangEnglish,
    hLangGerman,
    hLangCzech,
    hLangFrench,
    hLangItalian,
    hLangSpanish,
    hLangPortugue,
    hLangSwedish,
    hLangDanish,
    hLangFinnish,
    hLangDutch,
    hLangPolish,
    hLangHungaria
  );

  THeidenhainTable = class;

  THeidenhainColumn = class
  private
    Name: string;
    HType: THColumnType;
    FWidth: Integer;
    Decimal: Integer;
    Language: array [THLanguage] of string;
    Numeric: array of Double;
    Text: array of string;
    Table: THeidenhainTable;
    function FieldValue(Row: Integer): string;
    function GetWidth: Integer;
    function PackedName: string;
    procedure SetSize(R: Integer);

  protected
    procedure Write(Output: TStream);
    procedure Read(S: TStreamTokenizer);
    procedure Add(Value: string);
    constructor Create(Table: THeidenhainTable);
  end;

  THeidenhainTable = class
  private
    Columns: TList;
    FRows: Integer;
    FFileName: string;
    function GetColumnByIndex(Col: Integer): THeidenhainColumn;
    function GetColumn(Col: string): THeidenhainColumn;
    function GetDecimalValue(Col: THeidenhainColumn; Row: Integer): Double;
    function GetTextValue(Col: THeidenhainColumn; Row: Integer): string;
    function GetDecimal(Col: string; Row: Integer): Double;
    function GetText(Col: string; Row: Integer): string;
    procedure SetDecimal(Col: string; Row: Integer; const Value: Double);
    procedure SetText(Col: string; Row: Integer; const Value: string);
    function GetColumnName(Index: Integer): string;
    procedure SetColumnName(Index: Integer; const Value: string);
    procedure SetFileName(const Value: string);
    procedure SetRowCount(const Value: Integer);
  public
    property Decimal[Col: string; Row: Integer]: Double read GetDecimal write SetDecimal;
    property Text[Col: string; Row: Integer]: string read GetText write SetText;
    procedure Read(Stream: TStream);
    procedure Write(Stream: TStream);
    function ColumnCount: Integer;
    property RowCount: Integer read FRows write SetRowCount;
    property ColumnName[Index: Integer]: string read GetColumnName write SetColumnName;
    constructor Create;
    property FileName: string read FFileName write SetFileName;
    procedure Clear;
  end;


implementation

const
  TableHeaderA = 'BEGIN';
  TableHeaderB = 'HD-PAGE4.TAB';
  StructureBegin = '#STRUCTBEGIN';
  StructureEnd = '#STRUCTEND';
  CarRet = #$d + #$a;
  EndTable = '[END]';

  HLanguage: array[THLanguage] of PChar = (
    'DIA-ENGLISH',
    'DIA-GERMAN',
    'DIA-CZECH',
    'DIA-FRENCH',
    'DIA-ITALIAN',
    'DIA-SPANISH',
    'DIA-PORTUGUE',
    'DIA-SWEDISH',
    'DIA-DANISH',
    'DIA-FINNISH',
    'DIA-DUTCH',
    'DIA-POLISH',
    'DIA-HUNGARIA'
  );

  HTypeText: array[THColumnType] of PChar = (
    'N',
    'C'
  );

  const Space = '                                          ';

procedure StreamWrite(Stream: TStream; const Value: string);
begin
  Stream.Write(PChar(Value)^, Length(Value));
end;

function FixedWidth(ident: string; Width: Integer): string;
begin
  if Length(Ident) > Width then
    System.Delete(Ident, Width, Length(Ident));
  Result:= IDent + Copy(Space, 1, Width - Length(Ident));
end;

{ THeidenhainTable }

procedure THeidenhainTable.Clear;
begin
  while(Columns.Count > 0) do begin
    TObject(Columns[0]).Free;
    Columns.Delete(0);
  end;
  FRows:= 0;
end;

function THeidenhainTable.ColumnCount: Integer;
begin
  Result:= Columns.Count;
end;

constructor THeidenhainTable.Create;
begin
  Columns:= TList.Create;
  FRows:= 0;
end;

function THeidenhainTable.GetColumn(Col: string): THeidenhainColumn;
var I: Integer;
begin
  for I:=0 to ColumnCount - 1 do begin
    Result:= GetColumnByIndex(I);
    if Col = Result.Name then
      Exit;
  end;

  raise Exception.Create('Unknown column: ' + Col);
end;

function THeidenhainTable.GetColumnByIndex(Col: Integer): THeidenhainColumn;
begin
  Result:= THeidenhainColumn(Columns[Col]);
end;

function THeidenhainTable.GetColumnName(Index: Integer): string;
begin
  Result:= GetColumnbyIndex(Index).Name;
end;

function THeidenhainTable.GetDecimal(Col: string; Row: Integer): Double;
begin
  Result:= GetDecimalValue(GetColumn(Col), Row);
end;

function THeidenhainTable.GetDecimalValue(Col: THeidenhainColumn; Row: Integer): Double;
begin
  case Col.HType of
    hColNumeric: Result:= Col.Numeric[Row];
    hColText: Result:= StrToFloatDef(Col.Text[Row], 0);
  else
    raise Exception.Create('Invalid table type, email pete@fsdev.co.uk');
  end;

end;

function THeidenhainTable.GetText(Col: string; Row: Integer): string;
begin
  Result:= GetTextValue(GetColumn(Col), Row);
end;

function THeidenhainTable.GetTextValue(Col: THeidenhainColumn; Row: Integer): string;
begin
  case Col.HType of
    hColNumeric: Result:= Format('%g', [Col.Numeric[Row]]);
    hColText: Result:= Col.Text[Row];
  else
    raise Exception.Create('Invalid table type, email pete@fsdev.co.uk');
  end;
end;

procedure THeidenhainTable.Read(Stream: TStream);
var S: TStreamTokenizer;
    Column: THeidenhainColumn;
    I: Integer;
begin
  S:= TStreamTokenizer.Create;
  S.SetStream(Stream);
  S.ManageStream:= True;

  S.ExpectedToken(TableHeaderA);
  FileName:= S.ReadLine;
  S.ExpectedToken(StructureBegin);

  Self.Clear;

  while S.Read <> StructureEnd do begin
    S.Push;
    Column := THeidenhainColumn.Create(Self);
    Column.Read(S);
    Columns.Add(Column)
  end;

  S.Read; // Dump NR and get onto the headings line
  S.ReadLine; // Dump the headings

  while (Trim(S.ReadFixed(8)) <> EndTable) and not S.EndOfStream do begin
    // The index number of the columns has been swallowed
    for I:=0 to ColumnCount - 1 do begin
      Column:= GetColumnByIndex(I);
      Column.Add(Trim(S.ReadFixed(Column.GetWidth + 1)));
    end;
    Inc(FRows);
  end;
end;

procedure THeidenhainTable.SetColumnName(Index: Integer; const Value: string);
begin
  GetColumnByIndex(Index).Name:= Value;
end;

procedure THeidenhainTable.SetDecimal(Col: string; Row: Integer; const Value: Double);
var Column: THeidenhainColumn;
begin
  Column:= GetColumn(Col);
  case Column.HType of
    hColNumeric: Column.Numeric[Row]:= Value;
    hColText: Column.Text[Row] := FloatToStr(Value);
  else
    raise Exception.Create('Invalid table type, email pete@fsdev.co.uk');
  end;
end;

procedure THeidenhainTable.SetFileName(const Value: string);
begin
  FFileName := Value;
end;

procedure THeidenhainTable.SetRowCount(const Value: Integer);
var I: Integer;
    Col: THeidenhainColumn;
begin
  FRows := Value;
  for I:= 0 to Self.ColumnCount - 1 do begin
    Col:= Self.GetColumnByIndex(I);
    Col.SetSize(FRows);
  end;
end;

procedure THeidenhainTable.SetText(Col: string; Row: Integer; const Value: string);
var Column: THeidenhainColumn;
begin
  Column:= GetColumn(Col);
  case Column.HType of
    hColNumeric: Column.Numeric[Row]:= StrToFloatDef(Value, 0);
    hColText: Column.Text[Row] := Value;
  else
    raise Exception.Create('Invalid table type, email pete@fsdev.co.uk');
  end;
end;

procedure THeidenhainTable.Write(Stream: TStream);
var I, N: Integer;
begin
  StreamWrite(Stream, TableHeaderA + ' ' + FileName + CarRet);
  StreamWrite(Stream, StructureBegin + CarRet);

  for I:=0 to ColumnCount - 1 do begin
    GetColumnByIndex(I).Write(Stream);
  end;

  StreamWrite(Stream, StructureEnd + CarRet);

  StreamWrite(Stream, 'NR      ');

  for N:= 0 to ColumnCount - 1 do
    StreamWrite(Stream, GetColumnByIndex(N).PackedName);

  StreamWrite(Stream, CarRet);

  for I:= 0 to FRows - 1 do begin
    StreamWrite(Stream, FixedWidth(IntToStr(I), 8));
    for N:= 0 to ColumnCount - 1 do begin
      StreamWrite(Stream, GetColumnByIndex(N).FieldValue(I));
    end;
    StreamWrite(Stream, CarRet);
  end;

  StreamWrite(Stream, EndTable + CarRet);
end;

{ THeidenhainColumn }

procedure THeidenhainColumn.SetSize(R: Integer);
begin
  case HType of
    hColNumeric: SetLength(Numeric, R + 3);
  else
    SetLength(Text, R + 3);
  end;
end;

procedure THeidenhainColumn.Add(Value: string);
begin
  case HType of
    hColNumeric: begin
      if Length(Numeric) <= Table.RowCount then begin
        SetLength(Numeric, Table.RowCount * 2);
      end;
      Numeric[Table.RowCount] := StrToFloatDef(Value, 0);
    end;

    hColText: begin
      if Length(Text) <= Table.RowCount then begin
        SetLength(Text, Table.RowCount * 2);
      end;
      Text[Table.RowCount] := Value;
    end;
  end;
end;

constructor THeidenhainColumn.Create(Table: THeidenhainTable);
begin
  Self.Table:= Table;
  SetLength(Numeric, 10);
  SetLength(Text, 10);
end;

function DitchTrailingZeros(Tmp: string): string;
var I: Integer;
begin
  Result:= Tmp;
  if System.Pos('.', Tmp) <> 0 then begin
    for I:= Length(Tmp) downto 1 do begin
      case(Result[I]) of
        '.': begin
          Result[I]:= ' ';
          Exit
        end;

        '0': begin         ;
          Result[I]:= ' ';
        end;

        ' ': begin         
        end;
      else
        Exit;
      end;
    end;
  end;
end;

function THeidenhainColumn.FieldValue(Row: Integer): string;
var FormatStr: string;
begin
  case HType of
    hColNumeric: begin
      FormatStr:= '%.' + IntToStr(Decimal) + 'f';
      Result:= Format(FormatStr, [Numeric[Row]]);
      Result:= DitchTrailingZeros(Result);
    end;

    hColText: begin
      Result:= Text[Row];
    end;
  end;

  Result:= FixedWidth(Result, GetWidth + 1);
end;

function THeidenhainColumn.GetWidth: Integer;
begin
  Result:= FWidth;
  if Length(Name) > FWidth then
    Result:= Length(Name);
end;

function THeidenhainColumn.PackedName: string;
begin
  Result:= FixedWidth(Name, GetWidth + 1);
end;

procedure THeidenhainColumn.Read(S: TStreamTokenizer);
var T: string;
    I: THLanguage;
    L: Integer;
begin
  S.ExpectedToken('NAME');
  Name:= S.ReadStringAssign;

  S.ExpectedToken('TYPE');
  T:= S.ReadStringAssign;
  if T = HTypeText[hColNumeric] then
    HType:= hColNumeric
  else
    HType:= hColText;

  S.ExpectedToken('WIDTH');
  FWidth:= S.ReadIntegerAssign;

  S.ExpectedToken('DEC');
  Decimal:= S.ReadIntegerAssign;

  for I:=Low(THLanguage) to High(THLanguage) do begin
    S.ExpectedToken(HLanguage[I]);
    L:= S.Line;
    S.ExpectedToken('=');
    if L = S.Line then
      Language[I]:= S.ReadLine
    else
      Language[I]:= '';
  end;

  T:= S.Read;
  while Pos('DIA-', T) = 1 do begin
    S.ReadLine;
    T:= S.Read;
  end;

  S.Push;
end;

procedure THeidenhainColumn.Write(Output: TStream);
var I: THLanguage;
begin
  StreamWrite(Output, '   NAME = ' + Name + CarRet);
  StreamWrite(Output, '     TYPE = ' + HTypeText[HType] + CarRet);
  StreamWrite(Output, '     WIDTH = ' + IntToStr(FWidth) + CarRet);
  StreamWrite(Output, '     DEC = ' + IntToStr(Decimal) + CarRet);

  for I:=Low(THLanguage) to High(THLanguage) do begin
    StreamWrite(Output, '     ' + HLanguage[I] + ' = ' + Language[I] + CarRet);
  end;
end;

end.
