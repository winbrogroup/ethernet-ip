unit LSV2Manager;

interface

uses Classes, SysUtils, CoreCNC, CNC32, CNCTypes, CncAbs, HeidenhainTable, LSV2,
     StreamTokenizer, IStringArray, Named, FaultRegistration, StdCtrls, Controls;

type
  HTableState = (
    htIdle,
    htReadFile,
    htWriteFile,
    htHandshake,
    htComplete
  );


  THeidenhainTableFile = class
  private
    Index: Integer;
    HTableName: string;
    MDTName: string;

    HTable: THeidenhainTable;
    MDT: IACNC32StringArray;
    constructor Create;
    procedure MDTFromHTable;
    procedure HTableFromMDT;
  end;

  THeidenhainTableManager = class(TExpandDisplay)
  private
    TableList: TList;
    H2MDTSignal: TAbstractTag;
    MDT2HSignal: TAbstractTag;
    CompleteSignal: TAbstractTag;
    ErrorSignal: TAbstractTag;

    LocalHTableStore: string;

    H2MDTSignalName: string;
    MDT2HSignalName: string;

    TNCPath: string;

    MainState: HTableState;
    ActiveFile: THeidenhainTableFile;
    TNCResponse: Boolean;
    FileError: string;
    MultiBlock: Boolean;
    ListBox: TListBox;

    procedure ReadConfig;
    function GetHeidenhainTableFile(Index: Integer): THeidenhainTableFile;
    procedure DisplaySweep(ATag: TAbstractTag);
  public
    constructor Create(AOwner: TComponent); override;
    procedure InstallComponent(Params: TParamStrings); override;
    procedure Installed; override;
    procedure CheckInstallation; override;
    property Tables[Index: Integer]: THeidenhainTableFile read GetHeidenhainTableFile;
    function TableCount: Integer;
  end;

implementation

{ THeidenhainFileManager }

constructor THeidenhainTableManager.Create(AOwner: TComponent);
begin
  inherited;
  TableList:= TList.Create;
  MultiBlock:= False;
  ListBox:= TListBox.Create(Self);
  ListBox.Parent:= Self;
  ListBox.Align:= alClient;
end;

procedure THeidenhainTableManager.CheckInstallation;
var I: Integer;
    Table: THeidenhainTableFile;
    LocalFile: string;
    Stream: TFileStream;
begin
  inherited;

  for I:= 0 to TableCount - 1 do begin
    Table:= Tables[I];
    SysObj.StringArraySet.UseArray(Table.MDTName, nil, Table.MDT);
    if Table.MDT = nil then begin
      SysObj.StringArraySet.CreateArray( Table.MDTName, nil, Table.MDT);
    end;

    Table.HTable.FileName:= Table.HTableName;
    LocalFile:= Self.LocalHTableStore + Table.HTable.FileName;
    try
      Stream:= TFileStream.Create(LocalFile, fmOpenRead);
      Table.HTable.Read(Stream);
      Table.MDTFromHTable;
      Stream.Free;
    except
      on E: Exception do
        raise ECNCInstallFault.Create(Name + ': Unable to load ' + LocalFile + ' ' + E.Message);
    end;
  end;
end;

procedure THeidenhainTableManager.InstallComponent(Params: TParamStrings);
var I: Integer;
begin
  inherited;
  LocalHTableStore:= LocalPath + 'HTable\';
  SysUtils.ForceDirectories(LocalHTableStore);
  ReadConfig;
  for I:= 0 to TableCount - 1 do begin
    ListBox.Items.Add(IntToStr(Tables[I].Index) + ': ' + Tables[I].HTableName + ': ' + Tables[I].MDTName);
  end
end;

procedure THeidenhainTableManager.Installed;
begin
  inherited;
  Self.TagEvent(NameDisplaySweep, EdgeChange, TIntegerTag, DisplaySweep);
  H2MDTSignal:= Self.TagEvent(H2MDTSignalName, EdgeChange, TIntegerTag, nil);
  MDT2HSignal:= Self.TagEvent(MDT2HSignalName, EdgeChange, TIntegerTag, nil);
end;

procedure THeidenhainTableManager.ReadConfig;
var Token: string;

  function AddFile(S: TStreamTokenizer): THeidenhainTableFile;
  begin
    Result:= THeidenhainTableFile.Create;
    Result.Index:= TableList.Count + 1;
    S.ExpectedTokens(['=', '{']);
    Token:= LowerCase(S.Read);
    while not S.EndOfStream do begin
      if Token = 'tab' then
        Result.HTableName:= S.ReadStringAssign
      else if Token = 'index' then
        Result.Index:= S.ReadIntegerAssign
      else if Token = 'mdt' then
        Result.MDTName:= S.ReadStringAssign
      else if Token = '}' then
        Exit
      else
        raise Exception.CreateFmt('Unexpected Token [%s] at line [%d]', [Token, S.Line]);
      Token := LowerCase(S.Read);
    end;
    raise Exception.CreateFmt('Unexpected end of file at line [%d]', [S.Line]);
  end;

var S: TStreamQuoteTokenizer;
begin
  S := TStreamQuoteTokenizer.Create;
  S.Seperator := '[]{}';
  S.QuoteChar := '''';
  S.CommentChar := '#';

  try
    try
      S.SetStream(OpenLiveStream(Name));
      S.ExpectedTokens(['THeidenhainTableManager', '=', '{']);
      Token := LowerCase(S.Read);
      while not S.EndOfStream do begin
        if Token = 'completesignal' then
          CompleteSignal:= Self.TagPublish(S.ReadStringAssign, TIntegerTag)
        else if Token = 'errorsignal' then
          ErrorSignal:= Self.TagPublish(S.ReadStringAssign, TIntegerTag)
        else if Token = 'sendsignal' then
          MDT2HSignalName:= S.ReadStringAssign
        else if Token = 'receivesignal' then
          H2MDTSignalName:= S.ReadStringAssign
        else if Token = 'tncpath' then
          TNCPath:= S.ReadStringAssign
        else if Token = 'file' then
          TableList.Add(AddFile(S))
        else if Token = '}' then
          Exit
        else
          raise Exception.CreateFmt('Unexpected Token [%s] at line [%d]', [Token, S.Line]);
        Token := LowerCase(S.Read);
      end;
      raise Exception.CreateFmt('Unexpected end of file at line [%d]', [S.Line]);
    except
      on E : Exception do
        raise ECNCInstallFault.CreateFmt('%s failed to load configuration file [%s]', [Name, E.Message]);
    end;
  finally
    S.Free;
  end;
end;

procedure THeidenhainTableManager.DisplaySweep(ATag: TAbstractTag);
  function FindFileByIndex(Index: Integer): THeidenhainTableFile;
  var I: Integer;
  begin
    for I:= 0 to Self.TableCount - 1 do begin
      Result:= Self.Tables[I];
      if Result.Index = Index then
        Exit;
    end;
    Result:= nil;
  end;

var LocalPath, RemotePath: string;
    TableIndex: Integer;
    Stream: TFileStream;
begin
  if not MultiBlock  and Assigned(LSV2.TNCThread) and not CNC32.FakeIOSystem then begin
    case MainState of
      htIdle: begin
        if H2MDTSignal.AsBoolean or MDT2HSignal.AsBoolean then begin
          TableIndex:= -1;
          if H2MDTSignal.AsBoolean then begin
            TableIndex:= H2MDTSignal.AsInteger;
            MainState:= htReadFile;
          end else if MDT2HSignal.AsBoolean then begin
            TableIndex:= MDT2HSignal.AsInteger;
            MainState:= htWriteFile;
          end;

          ActiveFile:= FindFileByIndex(TableIndex);
          if not Assigned(ActiveFile) then begin
            MainState:= htComplete;
            ErrorSignal.AsBoolean:= True;
          end;
        end;
      end;

      htReadFile: begin
        MultiBlock:= True;
        LocalPath:= Self.LocalHTableStore + ActiveFile.HTableName;
        RemotePath:= TNCPath + ActiveFile.HTableName;
        if TNCThread.Connected then begin
          TNCResponse:= LSV2.TNCThread.ReceiveFile(LocalPath, RemotePath);
          try
            Stream:= TFileStream.Create(Self.LocalHTableStore + ActiveFile.HTableName, fmOpenRead);
            try
              ActiveFile.HTable.Read(Stream);
              ActiveFile.MDTFromHTable;
            finally
              Stream.Free;
            end;
          except
            TNCResponse:= False;
          end;
        end else begin
          TNCResponse:= False;
          SysObj.FaultInterface.SetFault(Self, CoreFaults[cfSysWarning], ['TNC Control not connected'], nil)
        end;
        MainState:= htHandshake;
        MultiBlock:= False;
      end;

      htWriteFile: begin
        MultiBlock:= True;
        try
          if TNCThread.Connected then begin
            LocalPath:= Self.LocalHTableStore + ActiveFile.HTableName;
            RemotePath:= TNCPath + ActiveFile.HTableName;
            ActiveFile.HTableFromMDT;
            try
              Stream:= TFileStream.Create(Self.LocalHTableStore + ActiveFile.HTableName, fmCreate);
              try
                ActiveFile.HTable.Write(Stream);
              finally
                Stream.Free;
              end;

              TNCResponse:= LSV2.TNCThread.SendFile(LocalPath, RemotePath);
              if not TNCResponse then
                SysObj.FaultInterface.SetFault(Self, CoreFaults[cfSysWarning], [LSV2.TNCThread.LastFileError], nil)
            except
              on E: Exception do begin
                TNCResponse:= False;
                SysObj.FaultInterface.SetFault(Self, CoreFaults[cfSysWarning], [E.Message], nil)
              end;
            end;
          end else begin
            TNCResponse:= False;
            SysObj.FaultInterface.SetFault(Self, CoreFaults[cfSysWarning], ['TNC Control not connected'], nil)
          end;
        finally
          MainState:= htHandshake;
          MultiBlock:= False;
        end;
      end;

      htHandshake: begin
        if TNCResponse then begin
          CompleteSignal.AsBoolean:= True
        end else begin
          ErrorSignal.AsBoolean:= True;
          FileError:= LSV2.TNCThread.LastFileError;
        end;
        MainState:= htComplete;
      end;

      htComplete: begin
        if not (MDT2HSignal.AsBoolean or H2MDTSignal.AsBoolean) then begin
          CompleteSignal.AsBoolean:= False;
          ErrorSignal.AsBoolean:= False;
          MainState:= htIdle;
        end;
      end;
    end;
  end;
end;

function THeidenhainTableManager.GetHeidenhainTableFile(Index: Integer): THeidenhainTableFile;
begin
  Result:= THeidenhainTableFile(TableList[Index]);
end;

function THeidenhainTableManager.TableCount: Integer;
begin
  Result:= TableList.Count;
end;

{ THeidenhainFile }

constructor THeidenhainTableFile.Create;
begin
  Index:= 0;

  HTable:= THeidenhainTable.Create;
  MDT:= nil;

  HTableName:= 'NOT_SET.TAB';
  MDTName:= 'NOTABLE';
end;


procedure THeidenhainTableFile.HTableFromMDT;
var I, J: Integer;
    Tmp: WideString;
begin
  HTable.RowCount:= MDT.RecordCount;

  for I:= 0 to HTable.RowCount - 1 do begin
    for J:= 0 to HTable.ColumnCount - 1 do begin
      MDT.GetValue(I, J, Tmp);
      HTable.Decimal[HTable.ColumnName[J], I]:= StrToFloatDef(Tmp, 0);
    end;
  end;
end;

var Blog: Integer;

procedure THeidenhainTableFile.MDTFromHTable;
var I, J: Integer;
    Tmp: WideString;
begin
  MDT.BeginUpdate;
  try
    for I:= MDT.FieldCount - 1 downto 0 do begin
      MDT.FieldNameAtIndex(I, Tmp);
      MDT.DeleteField(Tmp);
    end;

    if MDT.FieldCount <> 0 then
      Blog:= 12;

    for I:= 0 to HTable.ColumnCount - 1 do begin
      Tmp:= HTable.ColumnName[I];
      MDT.AddField(Tmp);
    end;

    MDT.Clear;
    MDT.Append(HTable.RowCount);

    for I:= 0 to HTable.ColumnCount - 1 do begin
      Tmp:= HTable.ColumnName[I];
      for J:= 0 to HTable.RowCount - 1 do begin
        MDT.SetValue(J, I, HTable.Text[Tmp, J], nil);
      end;
    end;

  finally
    MDT.EndUpdate;
  end;
end;

initialization
  RegisterCNCClass(THeidenhainTableManager);
end.
