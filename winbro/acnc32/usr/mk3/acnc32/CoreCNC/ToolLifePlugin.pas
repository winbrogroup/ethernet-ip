unit ToolLifePlugin;

interface

uses Classes, SysUtils, CoreCNC, CNCTypes, CNC32, IToolLife, CNCAbs, Windows,
     ComCtrls, ExtCtrls, Controls, Messages, Menus, Forms, Named;

type
  TToolLifePlugin = class(TExpandDisplay, IFSDToolLifeHost)
  private
    ToolLifeName : string;
    GetFSDToolLifeInterface : TGetFSDToolLifeInterface;
    Panel : TPanel;
    ListView : TListView;
    PopupMenu : TPopupMenu;
    procedure RemoveTool(Sender : TObject);
    procedure RemoveAll(Sender : TObject);
    procedure ListViewClick(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure ToolLifeChange(ToolLife : IFSDToolLife); stdcall;
    procedure CMToolLifeChange(var Msg : TMessage); message CNCM_TOOL_LIFE;
//    procedure ListViewEditing(Sender: TObject; Item: TListItem; var AllowEdit: Boolean);
  public
//    ToolLife : IFSDToolLife;
    constructor Create(aOwner : TComponent); override;
    procedure InstallComponent(Params : TParamStrings); override;
    procedure CheckInstallation; override;
    procedure IShutdown; override;
    destructor Destroy; override;
  end;

//var InstalledToolLife : TToolLifePlugin;

implementation

uses StdCtrls;

{ TToolLifePlugin }

procedure TToolLifePlugin.CheckInstallation;
begin
  inherited;
  if Assigned(SysObj.ToolLife) then
    SysObj.ToolLife.FinalInstall;
end;

procedure TToolLifePlugin.CMToolLifeChange(var Msg: TMessage);
var I, J : Integer;
    LI : TListItem;
    Buffer, Ex1ToolType : WideString;
    SN, Ex1SN : Integer;
    Total : Double;
    Remaining : Double;
begin
  I := SysObj.ToolLife.GetToolCount;
  if I <> ListView.Items.Count then begin
    while ListView.Items.Count < SysObj.ToolLife.GetToolCount do begin
      Li := ListView.Items.Add;
      LI.SubItems.Add('');
      LI.SubItems.Add('');
      LI.SubItems.Add('');
      LI.SubItems.Add('');
      LI.SubItems.Add('');
    end;
    while ListView.Items.Count > SysObj.ToolLife.GetToolCount do begin
      ListView.Items.Delete(ListView.Items.Count - 1);
    end;
  end;

  for I := 0 to SysObj.ToolLife.GetToolCount - 1 do begin
    SysObj.ToolLife.GetToolType(I, Buffer);
    SysObj.ToolLife.GetToolSN(I, SN);
    SysObj.ToolLife.GetTotal(Buffer, Total);
    SysObj.ToolLife.GetRemaining(Buffer, SN, Remaining);

    ListView.Items[I].Caption := Buffer;
    ListView.Items[I].SubItems[0] := IntToStr(SN);
    ListView.Items[I].SubItems[1] := FloatToStr(Total);
    ListView.Items[I].SubItems[2] := FloatToStr(Remaining);

    for J := 0 to 8 do begin
      if SysObj.ToolLife.GetEx1ToolType(J, Ex1ToolType) = trOK then begin
        if Ex1ToolType = Buffer then begin
          SysObj.ToolLife.GetEx1ToolSN(J, Ex1SN);
          if Ex1SN = SN then begin
            ListView.Items[I].SubItems[3] := IntToStr(J);
            SysObj.ToolLife.GetExt2(J, Ex1SN);
            ListView.ITems[I].SubItems[4] := IntToStr(Ex1SN);
          end;
        end;
      end;
    end;
  end;
end;

procedure TToolLifePlugin.RemoveTool(Sender : TObject);
var T : WideString;
    Index : Integer;
begin
  if Assigned(ListView.Selected) then begin
    T := ListView.Selected.Caption;
    Index := StrToInt(ListView.Selected.SubItems[0]);
    SysObj.ToolLife.RemoveTool(T, Index);
  end;
end;

procedure TToolLifePlugin.RemoveAll(Sender : TObject);
var I : Integer;
    Tmp : WideString;
    SN : Integer;
begin
  for I := SysObj.ToolLife.GetToolCount - 1 downto 0 do begin
    SysObj.ToolLife.GetToolType(I, Tmp);
    SysObj.ToolLife.GetToolSN(I, SN);
    SysObj.ToolLife.RemoveTool(Tmp, SN);
  end;
end;

{procedure TToolLifePlugin.ListViewEditing(Sender: TObject; Item: TListItem; var AllowEdit: Boolean);
begin
  AllowEdit := False;
end; }

procedure TToolLifePlugin.ListViewClick(Sender: TObject;
      Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  if (Button = mbRight) and Assigned(ListView.Selected) then begin
    PopupMenu.Tag := ListView.Selected.Index;
    PopupMenu.Popup(Mouse.CursorPos.X, Mouse.CursorPos.Y);
  end;
end;

constructor TToolLifePlugin.Create(aOwner: TComponent);
var LC : TListColumn;
    T : TMenuItem;
begin
  inherited;
  PopupMenu := TPopupMenu.Create(Self);
  T := TMenuItem.Create(PopupMenu);
  T.Caption := '&Remove';
  T.OnClick := RemoveTool;
  PopupMenu.ITems.Add(T);

  T := TMenuItem.Create(PopupMenu);
  T.Caption := '&Clear All';
  T.OnClick := RemoveAll;
  PopupMenu.ITems.Add(T);


  Panel := TPanel.Create(Self);
  Panel.Parent := Self;
  Panel.Align := alTop;
  Panel.Caption := ToolLifeLang;
  Panel.Height := 25;
  Panel.BevelOuter := bvLowered;

  ListView := TListView.Create(Self);
  ListView.Parent := Self;
  ListView.Align := alClient;
  ListView.ViewStyle := vsReport;
  ListView.GridLines := True;
  ListView.OnMouseUp := ListViewclick;

  LC := ListView.Columns.Add;
  LC.Caption := ToolTypeLang;
  LC.Width := -2;

  LC := ListView.Columns.Add;
  LC.Caption := 'SN';
  LC.Width := -2;

  LC := ListView.Columns.Add;
  LC.Caption := TotalLang;
  LC.Width := -2;

  LC := ListView.Columns.Add;
  LC.Caption := RemainingLang;
  LC.Width := -2;

  LC := ListView.Columns.Add;
  LC.Caption := 'Ex1';
  LC.Width := -2;

  LC := ListView.Columns.Add;
  LC.Caption := 'Ex2';
  LC.Width := -2;

//  InstalledToolLife := Self;
end;

destructor TToolLifePlugin.Destroy;
begin
  inherited;
  SysObj.ToolLife := nil;
end;

procedure TToolLifePlugin.InstallComponent(Params: TParamStrings);
var LibName : string;
    HMod : HModule;
begin
  inherited;


  try
  LibName := Params.ReadString('Plugin', '*');
  if LibName = '*' then
    raise ECNCInstallFault.Create('Plugin property not set in profile "Plugin = DllName"');
  HMod := SysObj.Comms.FindPlugin(LibName);
  if HMod = 0 then
    raise ECNCInstallFault.CreateFmt('Unable to load library "%s"', [LibName]);

  GetFSDToolLifeInterface := GetProcAddress(HMod, GetFSDToolLifeInterfaceName);
  if not Assigned(GetFSDToolLifeInterface) then
    raise ECNCInstallFault.CreateFmt('Interface "%s" not present in "%s"', [GetFSDToolLifeInterfaceName, LibName]);

  ToolLifeName := Params.ReadString('Object', '*');
  if ToolLifeName = '*' then
    raise ECNCInstallFault.Create('Plugin property not set in profile "FormName = Name"');

  SysObj.ToolLife := GetFSDToolLifeInterface(Self, PWideString(ToolLifeName));
//  SysObj.ToolLife := ToolLife;
  if not Assigned(SysObj.ToolLife) then
    raise ECNCInstallFault.CreateFmt('ToolLife "%s" not found in "%s"', [ToolLifeName, LibName]);

  SysObj.ToolLife.InstallName := Name;
  SysObj.ToolLife.Install;
  except
    On E: Exception do
      CoreCNC.DelayedFatalError.Add(E.Message);
  end;
end;

procedure TToolLifePlugin.IShutdown;
begin
  if Assigned(SysObj.ToolLife) then
    SysObj.ToolLife.Shutdown;
end;

procedure TToolLifePlugin.ToolLifeChange(ToolLife: IFSDToolLife);
begin
  SendMessage(Self.Handle, CNCM_TOOL_LIFE, 0, 0);
end;

initialization
  RegisterCNCClass(TToolLifePlugin);
end.
