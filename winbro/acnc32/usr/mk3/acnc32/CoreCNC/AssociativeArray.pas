unit AssociativeArray;

interface

uses IDB, Classes, SysUtils, CNCTypes, StreamTokenizer, FileCtrl;

type
  TDBTableInstance = class;

  TDBTable = class(TObject)
  private
    RefCount : Integer;
    DB : TStringList;
    Persistent : Boolean;
    InstanceList : TList;
    Alias : string;
    FileName : string;
    function GetData(Index : Integer) : TStringList;
  protected
    function AddInstance(aInstance : TDBTableInstance) : Integer;
    function RemoveInstance(aInstance : TDBTableInstance) : Integer;
    procedure Append;
    procedure Insert(Index : Integer);
    procedure SetField(Col, Row : Integer; const Value : string);
    function GetField(Col, Row : Integer) : string;
    procedure AddField(aFieldName : string);
    procedure DeleteField(I : Integer);
    procedure LoadDB(aFileName : string);
    procedure SaveDB(aFileName : string);
  public
    constructor Create(aFileName : string);
    destructor Destroy; override;
    function RecordCount : Integer;
    property Data[Index : Integer] : TStringList read GetData;
  end;

  TDBTableInstance = class(TInterfacedObject, IACNC32DBTable)
  private
    Table : TDBTable;
    CurrentRecord : Integer;
    CurrentField : Integer;
    CurrentSearch : string;
    FAttachCount : Integer;
  public
    constructor Create(aTable : TDBTable);
    destructor Destroy; override;
    function FieldCount : Integer; stdcall;
    function RecordCount : Integer; stdcall;
    function FieldNameAtIndex(Index : Integer; out FieldName : WideString) : TDB_RESULT; stdcall;
    function AddField(FieldName : WideString) : TDB_RESULT; stdcall;
    function DeleteField(FieldName : WideString) : TDB_RESULT; stdcall;
    function Find(FieldName, Value : WideString) : TDB_RESULT; stdcall;
    function FindNext(FieldName, Value : WideString) : TDB_RESULT; stdcall;
    function FindPrior(FieldName, Value : WideString) : TDB_RESULT; stdcall;

    function GetFieldValue(FieldName : WideString; out Value : WideString) : TDB_RESULT; stdcall;
    function SetFieldValue(FieldName, Value : WideString) : TDB_RESULT; stdcall;
    function Next : TDB_RESULT; stdcall;
    function Prior : TDB_RESULT; stdcall;
    function First : TDB_RESULT; stdcall;
    function Drop : TDB_RESULT; stdcall;
    function Append : TDB_RESULT; stdcall;
    function Insert : TDB_RESULT; stdcall;
    function SaveToCSV(FileName : WideString) : TDB_RESULT; stdcall;
    function LoadFromCSV(FileName : WideString) : TDB_RESULT; stdcall;
    function GetAttachCount : Integer; stdcall;
    procedure SetAttachCount(Ac : Integer); stdcall;
  end;

  TDB = class(TInterfacedObject, IACNC32DB)
  private
    TableList : TStringList;
    Path : string;
    procedure Clean;
    function GetTable(Index : Integer) : TDBTable;
  protected
    property Tables [Index : Integer] : TDBTable read GetTable;
  public
    constructor Create(aPath : string);
    destructor Destroy; override;
    function TableCount : Integer; stdcall;
    function AliasIndex(Index : Integer; out Alias : WideString) : TDB_RESULT; stdcall;
    function UseTable(Alias : WideString; out Table : IACNC32DBTable) : TDB_RESULT; stdcall;
    function CreateTable(Alias : WideString; out Table : IACNC32DBTable) : TDB_RESULT; stdcall;
    function DeleteTable(Alias : WideString) : TDB_RESULT;
  end;


implementation


{ TDB }

constructor TDB.Create(aPath : string);
var SR : TSearchRec;
begin
  ForceDirectories(aPath);
  TableList := TStringList.Create;
  TableList.Sorted := True;
  Path := aPath;
  if FindFirst(aPath + '*', 0, SR) = 0 then begin
    repeat
      TableList.AddObject(UpperCase(SR.Name), TDBTable.Create(aPath + SR.Name));
    until FindNext(SR) <> 0;
  end;
end;

destructor TDB.Destroy;
begin
  Clean;
  TableList.Free;
  inherited;
end;

function TDB.AliasIndex(Index: Integer; out Alias: WideString): TDB_RESULT;
begin
  Result := DBR_OK;
  if Index < TableList.Count then begin
    Alias := TableList[Index];
  end else begin
    Alias := '';
    Result := DBR_OUT_OF_BOUNDS;
  end;
end;

function TDB.CreateTable(Alias: WideString; out Table : IACNC32DBTable): TDB_RESULT;
var I : Integer;
    DBTable : TDBTable;
begin
  I := TableList.IndexOf(UpperCase(Alias));
  if I <> -1 then begin
    Result := DBR_ALREADY_EXISTS;
  end else begin
    DBTable := TDBTable.Create(Path + Alias);
    Table := TDBTableInstance.Create(DBTable);
    TableList.AddObject(UpperCase(Alias), DBTable);
    Result := DBR_OK;
  end;
end;

function TDB.DeleteTable(Alias: WideString): TDB_RESULT;
var I : Integer;
begin
  I := TableList.IndexOf(UpperCase(Alias));
  Result := DBR_OK;
  if I <> -1 then begin
    if Tables[I].RefCount > 0 then
      Tables[I].Persistent := False
    else
      TableList.Objects[I].Free;
  end else begin
    Result := DBR_NOT_FOUND;
  end;
end;

function TDB.TableCount: Integer;
begin
  Result := TableList.Count;
end;

function TDB.UseTable(Alias: WideString; out Table : IACNC32DBTable): TDB_RESULT;
var I : Integer;
begin
  Result := DBR_OK;
  I := TableList.IndexOf(UpperCase(Alias));
  if I <> -1 then begin
    Table := TDBTableInstance.Create(Tables[I]);
  end else begin
    Result := DBR_NOT_FOUND;
  end;
end;

function TDB.GetTable(Index: Integer): TDBTable;
begin
  if Index < TableList.Count then begin
    Result := TDBTable(TableList.Objects[Index])
  end else begin
    Result := nil;
  end;
end;

procedure TDB.Clean;
begin
  TableList.Clear; // ?? Not freeing memory yet, I may use COM for TDBTable;
end;

{ TDBTableInstance }


function TDBTableInstance.Append: TDB_RESULT;
begin
  Table.Append;
  Result := DBR_OK;
end;

function TDBTableInstance.Insert: TDB_RESULT;
begin
  Table.Insert(CurrentRecord);
  Result := DBR_OK;
end;

function TDBTableInstance.AddField(FieldName: WideString): TDB_RESULT;
begin
  Table.AddField(FieldName);
  Result := DBR_OK;
end;

function TDBTableInstance.DeleteField(FieldName: WideString): TDB_RESULT;
var I : Integer;
begin
  I := Table.DB.IndexOf(UpperCase(FieldName));
  if I <> -1 then begin
    Table.DeleteField(I);
    Result := DBR_OK;
  end else begin
    Result := DBR_NOT_FOUND;
  end;
end;

function TDBTableInstance.Drop: TDB_RESULT;
begin

end;

function TDBTableInstance.FieldCount: Integer;
begin
  Result := Table.DB.Count;
end;

function TDBTableInstance.FieldNameAtIndex(Index: Integer;
  out FieldName: WideString): TDB_RESULT;
begin
  FieldName := Table.DB[Index];
  Result := DBR_OK;
end;

function TDBTableInstance.FindRecord(FieldName, Value: WideString): TDB_RESULT;
begin

end;

function TDBTableInstance.First: TDB_RESULT;
begin
  CurrentRecord := 0;
end;

function TDBTableInstance.GetFieldValue(FieldName: WideString;
  out Value: WideString): TDB_RESULT;
var I : Integer;
begin
  Result := DBR_OK;
  I := Table.DB.IndexOf(UpperCase(FieldName));
  if I <> -1 then
    Value := Table.GetField(I, CurrentRecord)
  else
    Result := DBR_NOT_FOUND;
end;

function TDBTableInstance.LoadFromCSV(FileName: WideString): TDB_RESULT;
begin
  Table.LoadDB(FileName);
end;

function TDBTableInstance.Next: TDB_RESULT;
begin
end;

function TDBTableInstance.Previous: TDB_RESULT;
begin
end;

function TDBTableInstance.RecordCount: Integer;
begin
  Result := Table.RecordCount;
end;

function TDBTableInstance.SaveToCSV(FileName: WideString): TDB_RESULT;
begin

end;

function TDBTableInstance.SetFieldValue(FieldName, Value: WideString): TDB_RESULT;
begin

end;

constructor TDBTableInstance.Create(aTable: TDBTable);
begin
  Self.Table := aTable;
  Table.AddInstance(Self);
end;

destructor TDBTableInstance.Destroy;
begin
  Table.RemoveInstance(Self);
  inherited;
end;

function TDBTableInstance.GetAttachCount: Integer;
begin
  Result := FAttachCount;
end;

procedure TDBTableInstance.SetAttachCount(Ac: Integer);
begin
  FAttachCount := Ac;
end;

{ TDBTable }

procedure TDBTable.Append;
var I : Integer;
begin
  for I := 0 to DB.Count - 1 do begin
    Data[I].Add('');
  end;
end;

constructor TDBTable.Create(aFileName : string);
begin
   FileName := aFileName;
   Alias := UpperCase(ExtractFileName(aFileName));
   DB := TStringList.Create;
   LoadDB(FileName);
   InstanceList := TList.Create;
end;

destructor TDBTable.Destroy;
begin
  SaveDB(FileName);
  inherited;
end;

function TDBTable.GetField(Col, Row: Integer): string;
begin
  Result := GetData(Col)[Row];
end;

procedure TDBTable.Insert(Index: Integer);
var I : Integer;
begin
  for I := 0 to DB.Count - 1 do begin
    Data[I].Insert(Index, '');
  end;
end;

function TDBTable.RecordCount: Integer;
begin
  Result := Data[0].Count;
end;

function TDBTable.AddInstance(aInstance: TDBTableInstance): Integer;
begin
  if aInstance.GetAttachCount = 0 then begin
    InstanceList.Add(aInstance);
    LoadDB(FileName);
  end;
  aInstance.SetAttachCount(aInstance.GetAttachCount + 1);
end;


function TDBTable.RemoveInstance(aInstance: TDBTableInstance): Integer;
var I : Integer;
begin
  aInstance.SetAttachCount(aInstance.GetAttachCount - 1);
  if aInstance.GetAttachCount <= 0 then begin
    I := InstanceList.IndexOf(aInstance);
    if I <> -1 then
      InstanceList.Delete(I);

    if InstanceList.Count = 0 then begin
      SaveDB(FileName);
    end;
  end;
end;

procedure TDBTable.SetField(Col, Row: Integer; const Value: string);
begin
  Data[Col][Row] := Value;
end;

procedure TDBTable.AddField(aFieldName: string);
var S : TSTringList;
    I : Integer;
begin
  I := DB.IndexOf(UpperCase(aFieldName));
  if I = -1 then begin
    S := TStringList.Create;
    DB.AddObject(UpperCase(aFieldName), S);
    for I := 0 to Data[0].Count - 1 do begin
      S.Add('');
    end;
  end;
end;

procedure TDBTable.DeleteField(I : Integer);
begin
  Data[I].Free;
  DB.Delete(I);
end;

procedure TDBTable.LoadDB(aFileName: string);
var S : TStringList;
    T, Token : string;
    I, J : INteger;
begin
  S := TStringList.Create;
  try
    try
      S.LoadFromFile(aFileName);
    except
      Exit;
    end;
    if S.Count = 0 then
      Exit;

    T := S[0];

    Token := ReadQuotedString(T);
    while Token <> '' do begin
      AddField(Token);
      Token := ReadQuotedString(T);
    end;

    Append;
    for I := 1 to S.Count - 1 do begin
      T := S[I];
      for J := 0 to DB.Count - 1 do
        SetField(J, I, ReadQuotedString(T));
      Append;
    end;
  finally
    S.Free;
  end;
end;

procedure TDBTable.SaveDB(aFileName: string);
var I, J : Integer;
    S : TStringList;
    T : string;
begin
  S := TStringList.Create;
  try
    T := '';
    for I := 0 to DB.Count - 1 do begin
      T := T + ' ' + QuotedString(DB[I]);
    end;

    S.Add(T);

    for I := 0 to Data[0].Count - 1 do begin
      T := '';
      for J := 0 to DB.Count - 1 do begin
        T := T + ' ' + QuotedString(GetField(J, I));
      end;
    end;

    S.SaveToFile(FileName);
  finally
    S.Free;
  end;
end;

function TDBTable.GetData(Index: Integer): TStringList;
begin
  Result := TStringList(DB.Objects[Index]);
end;

end.
