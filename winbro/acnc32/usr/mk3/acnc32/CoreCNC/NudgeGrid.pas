unit NudgeGrid;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Grids, CNCTypes;

{This is a quick implementation of a 'nudgeable' string grid. It does
not allow for columns to be set up at design time. This would be possible
by implementing an appropriate TCollectionItem and using the
CollectionEditor}

{Validation:
  ValidationEvents:
    OnValidateInteger:  function ( Sender: TObject;
                               ACol, ARow: Integer;
                                var Value: Integer): Boolean;
    OnValidateFloat: function ( Sender: TObject;
                            ACol, ARow: Integer;
                             var Value: Double): Boolean;

    These are called with a 'Trial' value in the 'Value' Parameter. This
    value may be tested and/or modified at will. If the Validation Event
    returns false then the value is not posted.

  ValidationMode:
    lmFindLimits:
             Values are forced to lie between limits before being sent
             to ValidationEvents (OnValidateInteger or OnValidateFloat).
             If a ValidationEvent then moves the value outside limits, this
             is allowed (i.e Validation events know best) - providing, of
             course that the ve returns true.
    lmIgnoreLimits:
             Limits are ignored. A validation event may implement them
             without reference to any properties of the grid. If a
             validation event is not attached, then there are no limits!
    lmAlwaysFailOutsideLimits:
             Values that lie outside the limits (after external validation)
             are not accepted, even if the ValidationEvent returns true}


{Important:
   It is not possible to mix integer and float column properties - to do so
   would cause serious and confusing and seriously confusing errors. There
   is (for reasons of efficiency) nothing to stop a client setting IntLow
   on an ndFloat field, but this would corrupt the setting of FloatLow)
   In general all properties should be set together - hence the methods
   DefineIntColumn and DefineFloatColumn.
   Note that if ValidationMode = lmIgnoreLimits then then the limits are not
   important anyway. By chance, it is OK to set the 'Decimals' property on
   an Integer field, but it doesn't do anything}

type
  TNudgeDataType = (ndString, ndInteger, ndFloat);

  TValidationMode = (lmFindLimits, lmIgnoreLimits, lmAlwaysFailOutsideLimits);

  PNudgeData = ^TNudgeData;
  TNudgeData =
  record
    case DataType: TNudgeDataType of
      ndInteger: (IntLow, IntHigh, IntInc: Integer);
      ndFloat: (FloatLow, FloatHigh, FloatInc: Single;
                Decimals: Integer);
  end;

  TValidateIntegerEvent = function ( Sender: TObject;
                                 ACol, ARow: Integer;
                                  var Value: Integer): Boolean of object;

  TValidateFloatEvent = function ( Sender: TObject;
                               ACol, ARow: Integer;
                                var Value: Double): Boolean of object;

  TFloatValueEvent = procedure ( Sender: TObject;
                              ACol, ARow: Integer;
                                Value: Double) of object;

  TIntValueEvent = procedure ( Sender: TObject;
                              ACol, ARow: Integer;
                                Value: Integer) of object;


  TNudgeGrid = class(TStringGrid)
  private
    FNIList: TList;
    LastKey: Char;
    FOnValidateInteger: TValidateIntegerEvent;
    FOnValidateFloat: TValidateFloatEvent;
    FOnFloatValue: TFloatValueEvent;
    FOnIntValue: TIntValueEvent;
    FValidationMode: TValidationMode;
    function GetNudgeData(ACol: Integer): PNudgeData;
    function ColHasData(ACol: Integer): Boolean;
    function CreateNudgeData(ACol: Integer): PNudgeData;  {allocate data block if none exists}
    function GetIntLow(ACol: Integer): Integer;
    procedure SetIntLow(ACol: Integer; Value: Integer);
    function GetIntHigh(ACol: Integer): Integer;
    procedure SetIntHigh(ACol: Integer; Value: Integer);
    function GetIntInc(ACol: Integer): Integer;
    procedure SetIntInc(ACol: Integer; Value: Integer);
    function GetFloatLow(ACol: Integer): Double;
    procedure SetFloatLow(ACol: Integer; Value: Double);
    function GetFloatHigh(ACol: Integer): Double;
    procedure SetFloatHigh(ACol: Integer; Value: Double);
    function GetFloatInc(ACol: Integer): Double;
    procedure SetFloatInc(ACol: Integer; Value: Double);
    function GetNudgeType(ACol: Integer): TNudgeDataType;
    procedure SetNudgeType(ACol: Integer; Value: TNudgeDataType);
    procedure UpdateInt(ACol, ARow: Integer; var Res: Integer);
    procedure UpdateFloat(ACol, ARow: Integer; var Res: Double);
    function GetIntValue(ACol: Integer; ARow: Integer): Integer;
    procedure SetIntValue(ACol: Integer; ARow: Integer; Value: Integer);
    function GetFloatValue(ACol: Integer; ARow: Integer): Double;
    procedure SetFloatValue(ACol: Integer; ARow: Integer; Value: Double);
    function RCOK(ACol: Integer; ARow: Integer): Boolean;
    function GetDecimals(ACol: Integer): Integer;
    procedure SetDecimals(ACol: Integer; Value: Integer);
{   procedure CheckFloat(nt: TNudgeDataType; const Op: String);
    procedure CheckInt(nt: TNudgeDataType; const Op: String); }
  protected
    function CanEditAcceptKey( Key: Char): Boolean; override;
    function GetEditText(ACol, ARow: Longint): string; override;
    procedure SetEditText(ACol, ARow: Longint; const Value: string); override;
    procedure KeyPress( var Key: Char); override;
    procedure IncrementValue( Inc: Integer);
    procedure CNCMNudge( var Msg: TMessage); message CNCM_NUDGE;
    function GetEditLimit: Integer; override;
  public
    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;
    procedure DefineIntColumn(ACol: Integer; LowLimit, HighLimit, Increment: Integer);
    procedure DefineFloatColumn(ACol: Integer; LowLimit, HighLimit, Increment: Single; DecimalPlaces: Integer);
    property IntLow[ACol: Integer]: Integer read GetIntLow write SetIntLow;
    property IntHigh[ACol: Integer]: Integer read GetIntHigh write SetIntHigh;
    property IntInc[ACol: Integer]: Integer read GetIntInc write SetIntInc;
    property FloatLow[ACol: Integer]: Double read GetFloatLow write SetFloatLow;
    property FloatHigh[ACol: Integer]: Double read GetFloatHigh write SetFloatHigh;
    property FloatInc[ACol: Integer]: Double read GetFloatInc write SetFloatInc;
    property NudgeType[ACol: Integer]: TNudgeDataType read GetNudgeType write SetNudgeType;
    property IntValue[ACol, ARow: Integer]: Integer read GetIntValue write SetIntValue;
    property FloatValue[ACol: Integer; ARow: Integer]: Double read GetFloatValue write SetFloatValue;
    property Decimals[ACol: Integer]: Integer read GetDecimals write SetDecimals;
  published
    property ValidationMode: TValidationMode read FValidationMode write FValidationMode;
    property OnValidateInteger: TValidateIntegerEvent read FOnValidateInteger write FOnValidateInteger;
    property OnValidateFloat: TValidateFloatEvent read FOnValidateFloat write FOnValidateFloat;
    property OnIntValue: TIntValueEvent read FOnIntValue write FOnIntValue;
    property OnFloatValue: TFloatValueEvent read FOnFloatValue write FOnFloatValue;
  end;

{ EInvalidNudgeType = class(Exception); }

procedure Register;

implementation

resourcestring
  InvalidInput = '%s is not valid input';
{ NotAllowedMsg = '%s not allowed on %s column';
  FloatCol = 'floating point';
  IntCol = 'integer'; }

const
  defIntLow = 0;
  defIntHigh = 100;
  defIntInc = 1;
  defFloatLow = 0.0;
  defFloatHigh = 100.0;
  defFloatInc = 1.0;
  defDecimals = 1;

{
procedure TNudgeGrid.CheckFloat(nt: TNudgeDataType; const Op: String);
begin
  if nt <> ndFloat then
    raise EInvalidNudgeType.CreateFmt(NotAllowedMsg, [Op, IntCol])
end;

procedure TNudgeGrid.CheckInt(nt: TNudgeDataType; const Op: String);
begin
  if nt <> ndInteger then
    raise EInvalidNudgeType.CreateFmt(NotAllowedMsg, [Op, FloatCol])
end;
}

constructor TNudgeGrid.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
  FNIList:= TList.Create
end;

destructor TNudgeGrid.Destroy;
var
  i: Integer;
begin
  for i:= 0 to FNIList.Count - 1 do
  if FNIList[i] <> nil then
    Dispose(PNudgeData(FNIList[i]));
  FNIList.Free;
  inherited Destroy
end;

function TNudgeGrid.GetNudgeData(ACol: Integer): PNudgeData;
{ACol must have data - do not test ACol again}
begin
  Result:= PNudgeData(FNIList[ACol])
end;

function TNudgeGrid.ColHasData(ACol: Integer): Boolean;
begin
  Result:= (ACol >= 0) and (ACol < FNIList.Count) and (FNIList[ACol] <> nil)
end;

function TNudgeGrid.CreateNudgeData(ACol: Integer): PNudgeData;
begin
  while ACol >= FNIList.Count do FNIList.Add(nil);
  Result:= FNIList[ACol];
  if Result = nil then
  begin
    New(Result);
    Result^.DataType:= ndInteger;
    Result^.IntLow:= defIntLow;
    Result^.IntHigh:= defIntHigh;
    Result^.Decimals:= defDecimals; {does not overlap int data...}
    FNIList[ACol]:= Result
  end
end;

procedure Register;
begin
  RegisterComponents('ACNC Editors', [TNudgeGrid]);
end;

function TNudgeGrid.GetIntLow(ACol: Integer): Integer;
begin
  if ColHasData(ACol) then
    Result:= GetNudgeData(ACol)^.IntLow
  else
    Result:= DefIntLow
end;

procedure TNudgeGrid.SetIntLow(ACol: Integer; Value: Integer);
begin
  CreateNudgeData(ACol)^.IntLow:= Value
end;

function TNudgeGrid.GetIntHigh(ACol: Integer): Integer;
begin
  if ColHasData(ACol) then
    Result:= GetNudgeData(ACol)^.IntHigh
  else
    Result:= DefIntHigh
end;

procedure TNudgeGrid.SetIntHigh(ACol: Integer; Value: Integer);
begin
  CreateNudgeData(ACol)^.IntHigh:= Value
end;

function TNudgeGrid.GetIntInc(ACol: Integer): Integer;
begin
  if ColHasData(ACol) then
    Result:= GetNudgeData(ACol)^.IntInc
  else
    Result:= DefIntInc
end;

procedure TNudgeGrid.SetIntInc(ACol: Integer; Value: Integer);
begin
  CreateNudgeData(ACol)^.IntInc:= Value
end;

function TNudgeGrid.GetFloatLow(ACol: Integer): Double;
begin
  if ColHasData(ACol) then
    Result:= GetNudgeData(ACol)^.FloatLow
  else
    Result:= DefFloatLow
end;

procedure TNudgeGrid.SetFloatLow(ACol: Integer; Value: Double);
begin
  CreateNudgeData(ACol)^.FloatLow:= Value
end;

function TNudgeGrid.GetFloatHigh(ACol: Integer): Double;
begin
  if ColHasData(ACol) then
    Result:= GetNudgeData(ACol)^.FloatHigh
  else
    Result:= DefFloatHigh
end;

procedure TNudgeGrid.SetFloatHigh(ACol: Integer; Value: Double);
begin
  CreateNudgeData(ACol)^.FloatHigh:= Value
end;

function TNudgeGrid.GetFloatInc(ACol: Integer): Double;
begin
  if ColHasData(ACol) then
    Result:= GetNudgeData(ACol)^.FloatInc
  else
    Result:= DefFloatInc
end;

procedure TNudgeGrid.SetFloatInc(ACol: Integer; Value: Double);
begin
  CreateNudgeData(ACol)^.FloatInc:= Value
end;

function TNudgeGrid.GetNudgeType(ACol: Integer): TNudgeDataType;
begin
  if ColHasData(ACol) then
    Result:= GetNudgeData(ACol)^.DataType
  else
    Result:= ndString
end;

procedure TNudgeGrid.SetNudgeType(ACol: Integer; Value: TNudgeDataType);
begin
  CreateNudgeData(ACol)^.DataType:= Value
end;

function TNudgeGrid.GetDecimals(ACol: Integer): Integer;
begin
  if ColHasData(ACol) then
    Result:= GetNudgeData(ACol)^.Decimals
  else
    Result:= defDecimals
end;

procedure TNudgeGrid.SetDecimals(ACol: Integer; Value: Integer);
begin
  CreateNudgeData(ACol)^.Decimals:= Value
end;

function TNudgeGrid.CanEditAcceptKey( Key: Char): Boolean;
var
  nt: TNudgeDataType;
  S: String;
function Signed(ACol: Integer): Boolean;
begin
  Result:= false;
  case nt of
    ndInteger: Result:= IntLow[ACol] < 0;
    ndFloat: Result:= FloatLow[ACol] < 0.0;
  end
end;

begin
  nt:= NudgeType[Col];
  if nt = ndString then
  begin
    Result:= true
  end else
  with InplaceEditor do
  begin
    S:= Text;
    Delete(S, SelStart + 1, SelLength);
    case Key of
      '0'..'9': Result:= not ((SelStart < Length(S)) and (S[SelStart+1] = '-'));
      '-': Result:= (Signed(Col) and ((Length(S) = 0) or (SelStart = 0)));
      '.': Result:= (nt = ndFloat) and (Decimals[Col] > 0) and (Pos('.', S) = 0);
    else
      Result:= false
    end
  end
end;

function TNudgeGrid.GetEditText(ACol, ARow: Longint): string;
begin
  Result:= inherited GetEditText(ACol, ARow)
end;

procedure TNudgeGrid.UpdateInt(ACol, ARow: Integer; var Res: Integer);
begin
  if ValidationMode = lmFindLimits then
  begin
    if (Res < IntLow[ACol]) then
      Res:= IntLow[ACol]
    else
    if (Res > IntHigh[ACol]) then
      Res:= IntHigh[ACol]
  end;
  if (not Assigned(FOnValidateInteger) or
      FOnValidateInteger(Self, Col, Row, Res)) and
     ((ValidationMode <> lmAlwaysFailOutsideLimits) or
     ((Res >= IntLow[ACol]) and
      (Res <= IntHigh[ACol]))) then
    IntValue[ACol, ARow]:= Res
end;

procedure TNudgeGrid.UpdateFloat(ACol, ARow: Integer; var Res: Double);
begin
  if ValidationMode = lmFindLimits then
  begin
    if (Res < FloatLow[ACol]) then
      Res:= FloatLow[ACol]
    else
    if (Res > FloatHigh[ACol]) then
      Res:= FloatHigh[ACol]
  end;
  if (not Assigned(FOnValidateFloat) or
      FOnValidateFloat(Self, Col, Row, Res)) and
     ((ValidationMode <> lmAlwaysFailOutsideLimits) or
     ((Res >= FloatLow[ACol]) and
      (Res <= FloatHigh[ACol]))) then
    FloatValue[ACol, ARow]:= Res
end;

procedure TNudgeGrid.SetEditText(ACol, ARow: Longint; const Value: string);
var
  nt: TNudgeDataType;
  ResF: Double;
  ResI, Code: Integer;

function CodeOK: Boolean;
begin
  Result:= Code = 0;
  if not Result then
    MessageDlg(Format(InvalidInput, [Value]), mtError, [mbOK], 0)
end;

begin
  nt:= NudgeType[ACol];
  if nt = ndString then
  begin
    inherited SetEditText(ACol, ARow, Value);
  end else
  if not EditorMode and (LastKey <> #27) then
  begin
    case nt of
      ndInteger:
      begin
        Val(Value, ResI, Code);
        if CodeOK then UpdateInt(Col, Row, ResI)
      end;
      ndFloat:
      begin
        Val(Value, ResF, Code);
        if CodeOK then UpdateFloat(Col, Row, ResF)
      end;
    end
  end;
end;

procedure TNudgeGrid.KeyPress( var Key: Char);
begin
  LastKey:= Key;
  if (Key = #27) and
     EditorMode then
    HideEditor
  else
    inherited KeyPress(Key)
end;

procedure TNudgeGrid.IncrementValue( Inc: Integer);
var
  ResI: Integer;
  ResF: Double;
begin
  case NudgeType[Col] of
    ndInteger:
    begin
      ResI:= IntValue[Col, Row];
      ResI:= ResI + Inc*IntInc[Col];
      UpdateInt(Col, Row, ResI)
    end;
    ndFloat:
    begin
      ResF:= FloatValue[Col, Row];
      ResF:= ResF + Inc*FloatInc[Col];
      UpdateFloat(Col, Row, ResF)
    end;
  end;
end;

procedure TNudgeGrid.CNCMNudge( var Msg: TMessage);
begin
  if (goEditing in Options) and (NudgeType[Col] <> ndString) then
  case TEditChange(Msg.wParam) of
    EditChangeUp1: IncrementValue(1);
    EditChangeUp2: IncrementValue(5);
    EditChangeUp3: IncrementValue(100);
    EditChangeDown1: IncrementValue(-1);
    EditChangeDown2: IncrementValue(-5);
    EditChangeDown3: IncrementValue(-100);
    EditChangeNone:  IncrementValue(0)
  end;
end;

function TNudgeGrid.GetEditLimit: Integer;
begin
  case NudgeType[Col] of
    ndInteger: Result:= 6;
    ndFloat: Result:= 12;
  else
    Result:= inherited GetEditLimit;
  end
end;

function TNudgeGrid.GetIntValue(ACol: Integer; ARow: Integer): Integer;
var
  Code: Integer;
begin
  Result:= 0;
  if RCOK(ACol, ARow) then
    Val(Cells[ACol, ARow], Result, Code)
end;

procedure TNudgeGrid.SetIntValue(ACol: Integer; ARow: Integer; Value: Integer);
var
  S: String;
begin
  if RCOK(ACol, ARow) then
  begin
    Str(Value, S);
    Cells[ACol, ARow]:= S;
    if Assigned(FOnIntValue) then
      FOnIntValue(Self, ACol, ARow, Value)
  end
end;

function TNudgeGrid.GetFloatValue(ACol: Integer; ARow: Integer): Double;
var
  Code: Integer;
begin
  Result:= 0;
  if RCOK(ACol, ARow) then
    Val(Cells[ACol, ARow], Result, Code)
end;

procedure TNudgeGrid.SetFloatValue(ACol: Integer; ARow: Integer; Value: Double);
var
  S: String;
begin
  if RCOK(ACol, ARow) then
  begin
    Str(Value:0:Decimals[ACol], S);
    Cells[ACol, ARow]:= S;
    if Assigned(FOnFloatValue) then
      FOnFloatValue(Self, ACol, ARow, Value);
  end
end;

function TNudgeGrid.RCOK(ACol: Integer; ARow: Integer): Boolean;
begin
  Result:= (ACol >= 0) and (ACol < ColCount) and (ARow >= 0) and (ARow < RowCount)
end;

procedure TNudgeGrid.DefineIntColumn(ACol: Integer; LowLimit, HighLimit, Increment: Integer);
begin
  NudgeType[ACol]:= ndInteger;
  IntLow[ACol]:= LowLimit;
  IntHigh[ACol]:= HighLimit;
  IntInc[ACol]:= Increment
end;

procedure TNudgeGrid.DefineFloatColumn(ACol: Integer; LowLimit, HighLimit, Increment: Single; DecimalPlaces: Integer);
begin
  NudgeType[ACol]:= ndFloat;
  FloatLow[ACol]:= LowLimit;
  FloatHigh[ACol]:= HighLimit;
  FloatInc[ACol]:= Increment;
  Decimals[ACol]:= DecimalPlaces
end;

end.
