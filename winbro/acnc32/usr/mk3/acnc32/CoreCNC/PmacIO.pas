unit PmacIO;

interface

uses Classes, SysUtils, CNC32, PComm32;

type
  PmacIOSubSystem = class(TAbstractIOSubSystem)
  private
    PComm : TPcomm32;
    Device : Integer;
  protected
    function GetInfoPageContent(aPage : Integer) : string; override;
  public
    constructor Create(aName : string; var Params : string); override;
    destructor Destroy; override;
    procedure UpdateShadowToReal; override;
    procedure UpdateRealToShadow; override;
    function OK : Boolean; override;
    procedure Reset; override;
    function LastError : string; override;
  end;

implementation

{ PmacIOSubSystem }

constructor PmacIOSubSystem.Create(aName: string; var Params: string);
begin
  inherited;
  Device := StrToInt(ReadDelimited(Params, ';'));
  Pcomm := TPComm32.Create;
  if not Pcomm.OpenPmacNumber(Device) then
    raise Exception.Create('Unable to create pcomm device');
end;

destructor PmacIOSubSystem.Destroy;
begin
  inherited;
  Pcomm.ClosePmacNumber;
  Pcomm.Free;
end;

function PmacIOSubSystem.GetInfoPageContent(aPage: Integer): string;
begin

end;

function PmacIOSubSystem.LastError: string;
begin
  Result := '';
end;

function PmacIOSubSystem.OK: Boolean;
begin
  Result := True;
end;

procedure PmacIOSubSystem.Reset;
begin
  inherited;
end;

procedure PmacIOSubSystem.UpdateRealToShadow;
begin
  Pcomm.GetDPRMem(FInputBase, FInputSize, ShadowIn);
  ReadTags;
end;

procedure PmacIOSubSystem.UpdateShadowToReal;
begin
  Pcomm.SetDPRMem(FOutputBase, FOutputSize, ShadowOut);
  WriteTags;
end;

end.
