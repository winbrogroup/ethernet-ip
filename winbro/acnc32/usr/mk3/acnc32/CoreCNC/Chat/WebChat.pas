unit WebChat;

interface

uses Classes, SysUtils, Windows, CoreCNC, CNC32, CNCTypes, IFSDServer;

type
  TWebChat = class(TCNC)
  private
    ChatName : string;
    ChatConnected : Boolean;
    ChatStream : THandle;
    procedure ShowChat(aTag : TAbstractTag);
  public
    procedure InstallComponent(Params : TParamStrings); override;
    procedure Installed; override;
    function HTTPGeneratePage(HFile : THANDLE; var CGIPath: WideString; CGIRequest: ICGIStringList) : Boolean; override;
    function HTTPAddStreaming(Stream : THandle; CGIR : WideString; Method : IHTTPServer; DT : Integer) : Boolean; override;
    procedure HTTPRemoveStreaming(Stream : THandle; CGIR: WideString); override;
  end;

implementation

{ TWebChat }

function TWebChat.HTTPAddStreaming(Stream: THandle; CGIR: WideString;
  Method: IHTTPServer; DT: Integer): Boolean;
var Tmp : string;
begin
  Tmp := CGIR;
  if not ChatConnected and (CompareText(ReadDelimited(Tmp, '.'), 'chat') = 0) then begin
    ChatName := CGIR;
    ChatStream := Stream;
    ChatConnected := True;
    Result := True;
  end else
    Result := False;
end;

function TWebChat.HTTPGeneratePage(HFile: THANDLE; var CGIPath: WideString;
   CGIRequest: ICGIStringList) : Boolean;
begin
  Result := False;
end;

procedure TWebChat.InstallComponent(Params: TParamStrings);
begin
  inherited;

end;

procedure TWebChat.Installed;
begin
  inherited;
  TagEvent(Name + 'Show', EdgeFalling, TMethodTag, ShowChat);
end;

procedure TWebChat.HTTPRemoveStreaming(Stream: THandle; CGIR: WideString);
var Tmp : string;
begin
  inherited;
  Tmp := CGIR;
  if ChatConnected and (CompareText(ReadDelimited(Tmp, '.'), 'chat') = 0) then begin
    ChatConnected := False;
  end;
end;

procedure TWebChat.ShowChat(aTag: TAbstractTag);
begin

end;

end.
