unit Named;

interface

const
   NameReqNoAccess            = 'ReqNoAccess';
   NameReqOperatorAccess      = 'ReqOperatorAccess';
   NameReqProgrammerAccess    = 'ReqProgrammerAccess';
   NameReqSupervisorAccess    = 'ReqSupervisorAccess';
   NameReqMaintenanceAccess   = 'ReqMaintenanceAccess';
   NameReqAdministratorAccess = 'ReqAdministratorAccess';
   NameReqOEM1Access          = 'ReqOEM1Access';
   NameReqOEM2Access          = 'ReqOEM2Access';

   NameAccessInterlock        = 'AccessInterlock';
   NameAccessUserName         = 'AccessUserName';

   nameReqReset         = 'ReqReset';        //
   nameReqResetRW       = 'ReqResetRewind';  //
   nameReset           = 'Reset';
   nameResetRW         = 'ResetRewind';
   nameIOFaultLevel    = 'IOFaultLevel';
   nameIOFaultChange   = 'IOFaultChange';
//   nameIOEStop         = 'IOEStop';
   NameIOFault         = 'IOFault'; // TIOFaultTag
   nameReqFaultAck      = 'ReqFaultAck';
   nameInputMPG         = 'InputMPG';
   nameMPGOVR           = 'MPGOVR';

{ Configurable IO System Mem Input }
   namePLCFaultMap      = 'PLCErrorMap';     { DWord array of PLC faults }
//   namePLCEStop         = 'PLCEStop';        { PLC force EStop condition }
//   namePLCSafeStop      = 'PLCSafeStop';     { PLC force Safe Stop }
   nameResetComplete    = 'ResetComplete';

{ Published by IO system  (TAbstractIO}
   nameIOSweep          = 'IOSweep';
   nameIOSweepBit       = 'IOSweepBit';
   nameIOShutDown       = 'IOShutDown';

{ Published by the Access controller component (TAbstractAccessLevel}
   nameAccessLevel      = 'AccessLevel';      // byte representing current access
   NameAccessLevels     = 'AccessLevels';


{ Published by the display handler (TAbstractLayoutManager) }
   nameDisplaySweep    = 'DisplaySweep';    // written each display sweep
   nameFlashTimer      = 'FlashTimer';      // maintains a coordinated flash
   NameNormalDisplay   = 'NormalDisplay';
   NameTime            = 'Time';
   NameDate            = 'Date';
   NameNow             = 'Now';
   
   NamePrimaryDisplay  = 'LayoutPrimaryDisplay';
   
   RotateMessageName   = 'RotateMessage';

{ Published by CNCInstaller }
   nameKeyBoard        = 'keyBoard';
   NameMainFormHasFocus = 'MainFormHasFocus';

{ Published by program selector }
   nameVerifyNCLoad    = 'verifyNCLoad';


   nameNextWindow      = 'NextWindow';  // Event to change the active window

   nameSectionEditingMode = 'EditingSection'; //section editor is open

   nameDoorStatus = 'DoorStatus'; // Open = False

{ String Event signalling a new active file is loaded }
   nameNewActiveFile   = 'NewActiveFile';
   NameNextOCTOPP      = 'MextOCT_OPP';

{ Event that triggers a new file }
   nameProgramSelect   = 'ProgramSelect';
   nameOCTMode         = 'OCTModeSelected'; // True = OCT Mode
   NameForceActiveFile = 'ForceLoadActive'; // Published by Client FileManager
   NameRemoteFileLoad  = 'RemoteFileLoad';  // Published by Server FileManager
   NameOPPFilePath     = 'OPPFilePath'; // Path component of loaded file
   NameOPPFileName     = 'OPPFileName'; // Name component of loaded file

{ Plc Watch dog, Expected to mirror PlcSweep }
   NamePlcWatchDog     = 'PlcWatchDog';
   NamePlcVersion      = 'PlcVersion';

   NameNCMCode         = 'NCMCode';
   NameMCodeStrobe     = 'MCodeStrb';

   // Operation Number : has a different meaning dependent upon process
   NameOpNumber        = 'OpNumber';
   NamePartLoaded      = 'PartLoaded'; // Input event of partloaded
   NamePartID          = 'PartID'; // Memory only
   NameOperationOffset      = 'OperationOffsets';
   NameRemoteProgramLoaded  = 'NCRemoteProgramLoaded';


   NameRemoteScriptError  = 'RemoteScriptError';
   NameRemoteScript       = 'RemoteScript';
   NameRemoteScriptRun    = 'RemoteScriptRun';
   NameRemoteScriptTTL    = 'RemoteScriptTTL';
   NameRemoteScriptInCycle= 'RemoteScriptInCycle';
   NameRemoteScriptLineNo = 'RemoteScriptLineNumber';

   NameReqSavePersistent = 'ReqSavePersistent';

resourcestring
    CompIsInstalledLog  = 'Component %s already installed';
    CompIsInstalledBind = '%s attempting binding after installation';
    InvalidKey          = 'An invalid handle (%d) submitted to %s';
    InvalidIOType       = 'Invalid IO Type';
    IOSizeNotCateredFor = 'IO Size not catered for';
    MemoryNotOwner      = '%s Attempt to write to %s without ownership';
    UnhandledException  = 'An Unhandled Exception Occured';
    IOMapInvalidText    = 'IO Definition Invalid [%s]';

    componentIsInstalled = '%s is already installed';
    componentInstalling  = '++ Installing component [%s]';
    finalInstalling      = '++ Final installation of [%s]';
    CheckInstall         = '++ Checking installation of [%s]';
    iniLoadInvalid       = 'Value of %s is invalid';
    iniLoadNotFound      = 'Item %s not found';
    NoCallbackSet        = 'No callback set for [%s]';
    nameIsNotUnique         = '%s is not unique';
    nameTooLong             = '%s Name is too long EXITING';
    nameDoesNotExist        = '%s : name does not exist ';
    iniErrorNumberInvalid   = 'Invalid error number if IOmap.ini faults section : %d';
    ActiveDisplayLog        = 'Active Display = %d';
    ReadIntegerInvalid      = 'Integer value invalid [%s]';
    ReadIntegerNotProgrammed= 'Integer value [%s] Not Programmed: Defaulting to [%d]';
    ReadStringNotProgrammed = 'String value [%s] Not Programmed: Defaulting to "%s"';
    ReadBoolInvalid         = 'Boolean value invalid [%s]';
    ReadBoolNotProgrammed   = 'Boolean value [%s] Not Programmed: Defaulting to [%d]';
    ReadColourInvalid       = 'colour value invalid [%s]';
    ReadColourNotProgrammed = 'Floating point [%s] Not Programmed: Defaulting to [%d]';
    ReadDoubleInvalid       = 'Floating point invalid [%s]';
    ReadDoubleNotProgrammed = 'Floating point [%s] Not Programmed: Defaulting to [%f]';
    InvalidPLCFaultLevel    = 'Invalid PLC fault level in fault list %s';
    NamedTagNotFound        = '[%s] Tag not found in [%s]';
    ParameterIsRequired     = '[%s] Parameter is required';
    InvalidInterlockText    = '[%s] Invalid Interlock';
    DataLossVerify          = 'Data is modified; are you sure you wish to lose your modifications?';
{    ManualModeText          = 'Manual';
    AutoModeText            = 'Auto';
    MdiModeText             = 'MDI';
    TeachinModeText         = 'Teach In';
    EditModeText            = 'Edit';

    NoneAccessText          = 'None';
    OperatorAccessText      = 'Operator';
    SupervisorAccessText    = 'PD';
    ProgrammerAccessText    = 'Programmer';
    MaintenanceAccessText   = 'Maintenance';
    AdministratorAccessText = 'Administrator';
    OEM1AccessText          = 'OEM 1';
    OEM2AccessText          = 'OEM 2'; }

    InMotionText            = 'Motion';
    InCycleText             = 'Cycle';
    HomedText               = 'Homed';
    EditingText             = 'Editing';

    ClassNameNotUnique      = 'Cannot register class %s more than once';
    TagClassNameNotUnique   = 'Cannot register TagClass %s more than once';
    ClassNotFound           = 'Cannot create %s. Class %s not registered';
    CannotHaveTwo           = 'Cannot install %s. %s:%s is already installed';
    MustHaveDescendent      = 'System requires a component descended from %s';
    NameUsedTwice           = 'Cannot name component [%s], already used';

{    DisplayLang = 'Display';
    CommandedLang = 'Commanded';
    MachineLang = 'Machine';
    TargetLang = 'Target';
    DisttogoLang = 'Dist to go';
    FerrLang = 'Lag';
    BiasLang = 'Bias';
    CompensationLang = 'Compensation';
    CPULang = 'CPU';
    GridShiftLang = 'GridShift';
    TimeRIMLang = 'TimeRIM';
    MasterLang = 'Master';
    Acceleration = 'Acceleration';
    OKLang = 'OK';
    NotSetLang = 'Not set'; }


    IOTagRequiredText = '%s Requires IO Connection %s';
    TagRequiredText = '%s Requires %s Data to be present';

//    HeadLangFormat = 'Head %d';
//    ShortHeadLangFormat = 'H %d';
//    HoleProgramLangFormat = 'Hole Program %d';

    DeviceNotFoundText = '[%s] Not found in IO System';
    DeviceWrongTypeText = '[%s] Device is wrong type Should be [%s]';

    NotConfiguredTagName = 'NOT_SET';

var
    NameLang: string;
    ValueLang: string;
    AliasLang: string;
    OwnerLang: string;
    TypeLang: string;
    SizeLang: string;
    CallbacksLang: string;
    TotalLang: string;
    LimitLang: string;
    MessageLang: string;
    UpLang: string;
    DownLang: string;
    ResetLang: string;
    ProgramLang: string;
    SubroutineLang: string;

    RemainingLang: string;
    ToolLifeLang: string;
    ToolTypeLang: string;
    TrueLang: string;
    FalseLang: string;


implementation

end.

