unit FaultRegistration;

interface

uses Classes, SysUtils, CNCTypes, CNC32, StreamTokenizer, INamedInterface;

type
  TCoreFaults = (
    cfCantShutdown,
    cfShuttingDown,
    cfLoadFailed,
    cfCantLoadNeedRewind,
    cfFailedSave,
    cfSysInterlock,
    cfSysWarning,
    cfSysStopCycle,
    cfSysEStop,
    cfSysFatal,
    cfGEONotInstalled,
    cfGEOError,
    cfFailedConnect,
    cfFunctionNotPermitted,
    cfNoModeChangeWhileEditing,
    cfNoModeChangeWhileInCycle,
    cfFailedLoad,
    cfOEEActive,
    cfSysRewind,
    cfLoadFailedSerious,
    cfTelnetNotify,
    cfTelnetError,
    cfScriptInterlock,
    cfScriptWarning,
    cfScriptRewind,
    cfScriptEStop,
    cfScriptFatal,
    cfPersistentSaved1,
    cf1028,
    cf1029
  );



const
  CoreFaults : array [TCoreFaults] of TFaultRegistration = (
    ( Ndx : 1000; Text : 'Unable to shutdown'; Level : FaultFatal; Dispatch : [] ),
    ( Ndx : 1001; Text : 'DOWN DOWN'; Level : FaultFatal; Dispatch : [] ),
    ( Ndx : 1002; Text : 'Load failed : [%s]'; Level : FaultStopCycle ),
    ( Ndx : 1003; Text : 'Cannot Load program unless program rewound'; Level : FaultInterlock ),
    ( Ndx : 1004; Text : 'Failed save [%s]'; Level : FaultStopCycle ),
    ( Ndx : 1005; Text : '%s'; Level : FaultInterlock ),
    ( Ndx : 1006; Text : '%s'; Level : FaultWarning ),
    ( Ndx : 1007; Text : '%s'; Level : FaultStopCycle),
    ( Ndx : 1008; Text : '%s'; Level : FaultEStop ),
    ( Ndx : 1009; Text : '%s'; Level : FaultFatal ),
    ( Ndx : 1010; Text : 'GEO Not Installed'; Level : FaultRewind ),
    ( Ndx : 1011; Text : 'GEO Error "%s"'; Level : FaultRewind ),
    ( Ndx : 1012; Text : 'Failed connect "%s"'; Level : FaultInterlock ),
    ( Ndx : 1013; Text : 'Function not Permitted Due to the Current Error Level'; Level : FaultInterlock ),
    ( Ndx : 1014; Text : 'Mode Change Not Allowed While Editing'; Level : FaultInterlock ),
    ( Ndx : 1015; Text : 'Mode Change Inhibited While In Cycle'; Level : FaultInterlock ),
    ( Ndx : 1016; Text : 'Failed Load %s'; Level : FaultStopCycle ),
    ( Ndx : 1017; Text : 'OEE Active'; Level : FaultStopCycle ),
    ( Ndx : 1018; Text : '%s'; Level : FaultRewind ),
    ( Ndx : 1019; Text : 'File manager failed to load OPP "%s"'; Level : FaultStopCycle ),
    ( Ndx : 1020; Text : 'Remote: %s'; Level : FaultInterlock ),
    ( Ndx : 1021; Text : 'Remote: %s'; Level : FaultRewind ),
    ( Ndx : 1022; Text : 'Script: %s'; Level : FaultInterlock ),
    ( Ndx : 1023; Text : 'Script: %s'; Level : FaultWarning ),
    ( Ndx : 1024; Text : 'Script: %s'; Level : FaultRewind ),
    ( Ndx : 1025; Text : 'Script: %s'; Level : FaultEStop ),
    ( Ndx : 1026; Text : 'Script: %s'; Level : FaultFatal ),
    ( Ndx : 1027; Text : '$PERSISTENT_SAVED_S'; Level : FaultWarning ),
    ( Ndx : 1028; Text : ''; Level : FaultInterlock ),
    ( Ndx : 1029; Text : ''; Level : FaultInterlock )
  );

procedure RegisterFaultMap(Name : string; const Reg : array of TFaultRegistration; Size : Integer; CodeBase : Integer);
procedure ReadRegisteredFaults(FileName : string);
procedure WriteRegisteredFaults(FileName : string);
function GetDispatchNames: TStrings;

implementation

uses CoreCNC;

type
  TRegisteredFault = class
    Size : Integer;
    Reg : PAFaultRegistration;
    Name : string;
    CodeBase : Integer;
  end;

var RegisteredFaults, DispatchNames : TStringList;
    FaultEnums     : TStringList;

procedure RegisterFaultMap(Name : string; const Reg : array of TFaultRegistration; Size : Integer; CodeBase : Integer);
var TReg : TRegisteredFault;
begin
  Treg := TRegisteredFault.Create;

  Treg.Size := Size;
  Treg.Name := Name;
  Treg.Reg := @Reg;
  Treg.CodeBase := CodeBase;

  RegisteredFaults.AddObject(Name, Treg);
end;

procedure ReadRegisteredFaults(FileName : string);
var S : TStreamQuoteTokenizer;
    Token : string;

  procedure ReadDispatch;
  begin
    S.ExpectedTokens(['=', '{']);

    Token := LowerCase(S.Read);
    if Token <> '}' then
      DispatchNames.Add(Token);

    Token := LowerCase(S.Read);
    while not S.EndOfStream do begin
      if Token = '}' then
        Exit
      else if Token = ',' then
        DispatchNames.Add(S.Read)
      else
        raise Exception.Create('Malformed Dispatch section');
      Token := LowerCase(S.Read);
    end;
    raise Exception.Create('Unexpected end of configuration');
  end;

  function ReadFaultLevel : TFaultLevel;
  var I : Integer;
  begin
    Token := S.ReadStringAssign;
    for I := 0 to FaultEnums.Count - 1 do begin
      if CompareText(Token, FaultEnums[I]) = 0 then begin
        Result := TFaultLevel(I);
        Exit;
      end;
    end;
    raise Exception.CreateFmt('Unknown fault level "%s"', [Token]);
  end;

  function ReadItemDispatch : T32Bits;
  var I : Integer;
  begin
    S.ExpectedTokens(['=', '[']);
    Result := [];
    Token := LowerCase(S.Read);
    repeat
      if Token = ']' then
        Exit
      else if Token <> ',' then begin
        I := DispatchNames.IndexOf(Token);
        if I <> -1 then
          Include(Result, I + 1)
        else begin
          try
           I := StrToInt(Token);
           Include(Result, I mod 32);
          except
            raise Exception.CreateFmt('Invalid dispatch target "%s"', [Token]);
          end;
        end;
      end;
      Token := LowerCase(S.Read);
    until S.EndOfStream;
    raise Exception.Create('Unexpected end of configuration');
  end;

  procedure ReadMapAssignment(CodeBase : Integer; Treg : TRegisteredFault);
  var I : Integer;
  begin
    I := S.ReadIndex(Treg.Size);
    Treg.Reg[I].Ndx := CodeBase + I;
    S.ExpectedTokens(['=', '{']);
    Token := Lowercase(S.Read);
    while not S.EndOfStream do begin
      if Token = 'text' then
        Treg.Reg[I].Text := SysObj.Localized.GetString(S.ReadStringAssign)
      else if Token = 'level' then
        Treg.Reg[I].Level := {TFaultLevel(S.ReadIntegerAssign) //}ReadFaultLevel
      else if Token = 'dispatch' then
        Treg.Reg[I].Dispatch := ReadItemDispatch
      else if Token = 'help' then
        Treg.Reg[I].Help := S.ReadStringAssign
      else if Token = '}' then
        Exit
      else
        raise Exception.CreateFmt('Unexpected token "%s" in %s', [Token, Treg.Name]);
      Token := Lowercase(S.Read);
    end;
    raise Exception.Create('Unexpected end of configuration');
  end;

  procedure ReadMapSet(const Name : string);
  var Treg : TRegisteredFault;
      I : Integer;
  begin
    S.ExpectedTokens(['=', '{']);
    Treg := nil;
    for I := 0 to RegisteredFaults.Count - 1 do begin
      if CompareText(Name, RegisteredFaults[I]) = 0 then begin
        Treg := TRegisteredFault(RegisteredFaults.Objects[I]);
        Break;
      end;
    end;
    if not Assigned(Treg) then
      raise Exception.CreateFmt('Invalid map reference "%s"', [Name]);

    Token := LowerCase(S.Read);
    while not S.EndOfStream do begin
      if Token = 'codebase' then
        Treg.CodeBase := S.ReadIntegerAssign
      else if Token = 'error' then
        ReadMapAssignment(Treg.CodeBase, Treg)
      else if Token = '}' then
        Exit
      else
        raise Exception.Create('Malformed ErrorMaps section');
      Token := LowerCase(S.Read);
    end;
    raise Exception.Create('Unexpected end of configuration');
  end;

  procedure ReadErrorMaps;
  begin
    S.ExpectedTokens(['=', '{']);
    Token := LowerCase(S.Read);
    while not S.EndOfStream do begin
      if Token = '}' then
        Exit
      else
        ReadMapSet(Token);
      Token := LowerCase(S.Read);
    end;
    raise Exception.Create('Unexpected end of errors"');
  end;

begin
  S := TStreamQuoteTokenizer.Create;
  try
    try
      S.Buffered := True;
      S.SetStream(TFileStream.Create(FileName, fmOpenRead));
      S.Seperator := '[]{}#=,';
      S.QuoteChar := '''';
      S.ExpectedTokens(['Errors', '=', '{']);
      Token := LowerCase(S.Read);
      while not S.EndOfStream do begin
        if Token = 'dispatch' then
          ReadDispatch
        else if Token = 'errormaps' then
          ReadErrorMaps
        else if Token = '}' then
          Exit;
        Token := LowerCase(S.Read);
      end;
      raise Exception.Create('Unexpected end of configuration"');
    except
      on E : Exception do
        raise Exception.CreateFmt('[%s] failed loading due to "%s" at line %d', [FileName, E.Message, S.Line]);
    end;
  finally
    S.Free;
  end;
end;

procedure WriteRegisteredFaults(FileName : string);
var Stream : TFileStream;
    I, J : Integer;
    Treg : TRegisteredFault;
  procedure WriteLine(aText : string);
  begin
    Stream.WriteBuffer(PChar(aText + CarRet)^, Length(aText + CarRet));
  end;

  function DispatchString(D : T32Bits) : string;
  var I : Integer;
  begin
    Result := '[';
    for I := 0 to High(T32Bit) do begin
      if I in D then begin
        if I < DispatchNames.Count then
          Result := Result + DispatchNames[I] + ', '
        else
          Result := Result + IntToStr(I) + ', ';
      end;
    end;
    if Length(Result) > 2 then
      Delete(Result, Length(Result) - 1, 2);
    Result := Result + ']';
  end;
begin
  Stream := TFileStream.Create(FileName, fmCreate);
  try
   WriteLine('Errors = {');
   WriteLine('  Dispatch = {');
   for I := 0 to DispatchNames.Count - 1 do begin
     if I <> DispatchNames.Count - 1 then
       WriteLine('    ' + DispatchNames[I] + ',')
     else
       WriteLine('    ' + DispatchNames[I])
   end;
   WriteLine('  }');

   WriteLine('  ErrorMaps = {');
   for I := 0 to RegisteredFaults.Count - 1 do begin
     Treg := TRegisteredFault(RegisteredFaults.Objects[I]);
     WriteLine('    ' + Treg.Name + ' = {');
     WriteLine('      CodeBase = ' + IntToStr(Treg.CodeBase));
     for J := 0 to Treg.Size - 1 do begin
       WriteLine(Format('      Error[%d] = {', [J]));
       WriteLine('        Text = ''' + Treg.Reg[J].Text + '''');
       WriteLine('        Level = ' + {IntToStr(Ord(Treg.Reg[J].Level))); // }FaultEnums[Ord(Treg.Reg[J].Level)]);
       if Treg.Reg[J].Dispatch <> [] then
         WriteLine('        Dispatch = ' + DispatchString(Treg.Reg[J].Dispatch));
       if Treg.Reg[J].Help <> '' then
         WriteLine('        Help = ' + Treg.Reg[J].Help);
       WriteLine('      }');
     end;
     WriteLine('    }');
   end;
   WriteLine('  }');
   WriteLine('}');
  finally
    Stream.Free;
  end;
end;

function GetDispatchNames: TStrings;
begin
  Result:= DispatchNames;
end;



initialization
  RegisteredFaults := TStringList.Create;
  DispatchNames := TStringList.Create;
  FaultEnums   := TStringList.Create;
  GetEnumerationNames(TypeInfo(TFaultLevel), FaultEnums);

  RegisterFaultMap('CoreSystem', CoreFaults, Length(CoreFaults), 1000);

end.
