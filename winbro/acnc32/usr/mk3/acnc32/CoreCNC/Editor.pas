unit Editor;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, CoreCNC, IniFiles, CNC32, CNCTypes, named, AbstractEditor,
  Tokenizer{, NCConsts};

const
  EditCallDesig = 'CALL';
  GCodeOffsetDesig = 'G64.1';
  GCodeCallDesig = 'M98';

type
  TComment = ( cfStarts, cfEnds, cfAll, cfContinue, cfNone );
  TComments = set of TComment;
  PTCommentRecord = ^TCommentRecord;
  TCommentRecord = record
    start  : TPoint;
    finish : TPoint;
    next   : PTCommentRecord;
  end;

  TSyntaxEditor = class(TAbstractEditor)
    protected
      FSyntaxOk : boolean;
      function GetSyntaxOK : Boolean; virtual;
    public
      property SyntaxOk : Boolean read GetSyntaxOK;
  end;

  TCommentField = class(TObject)
  private
    start : PTCommentRecord;
  public
    procedure clear;
    procedure add(b, e : TPoint);
    function CommentLine(Y : integer) : TComments;
    function startPos(Y : integer) : TPoint;
    function endPos(Y : integer) : TPoint;
  end;

  TPlcEditor = class(TSyntaxEditor)
  private
  protected
    comments : TCommentField;
    procedure Key(Sender : TObject);
    function  ParseLine(aText : string; Line : Integer) : string; override;
  public
    constructor Create(AOwner : TComponent); override;
    destructor Destroy; override;
  published
  end;

  TStdEditor = class(TAbstractEditor);
  TGCodeGroup = 0..31;
  TGCodeGroups = set of TGCodeGroup;

  TPermissiveGCode = class(TStringList)
  private
    Groups : TGCodeGroups;
  public
    procedure AddGCode(const GName : string; const GCodes : array of Word);
    function  CodeOK(GCode : Word) : Boolean;
    procedure NewLine;
  end;

  TGCodeEditor = class(TSyntaxEditor)
  private
    FGCodeGroups     : TPermissiveGCode;
    tmpflo           : double;
    FstyleAxis       : TFontStyles;
    FstyleGCode      : TFontStyles;
    FstyleMCode      : TFontStyles;
    FstyleLineNumber : TFontStyles;
    FstyleTool       : TFontStyles;
    FstyleIJK        : TFontStyles;
    FstyleComment    : TFontStyles;
    FStyleSub        : TFontStyles;
    FColourAxis      : TColor;
    FColourGCode     : TColor;
    FColourMCode     : TColor;
    FColourLineNumber: TColor;
    FColourTool      : TColor;
    FColourIJK       : TColor;
    FColourComment   : TColor;
    FColourSub       : TColor;
    FSyntaxError     : Boolean;
    FSyntaxLine      : Integer;
    ParsingLine      : Integer;
    function isAxisName  (a : string) : boolean;
    function ReadGCode   (var d : string; var s : string) : boolean;
    function ReadDouble  (var d : string; var s : string; var v : double) : boolean;
    function ReadCardinal(var d : string; var s : string; var v : cardinal) : boolean;
    function ReadMCode   (var d : string; var s : string; var m : boolean) : boolean;
    function ReadSubroutine(var d, s : string; Selector : Char) : boolean;
    procedure ErrorExit  (var sf : string; var td : string; c : char);

  protected
    function  ParseLine(aText : string; aLine : Integer) : string; override;
//    procedure ParseLine(Y : integer); override;
    function GetSyntaxOK : Boolean; override;
  public
    NC : TAbstractNC;
    constructor Create(AOwner : TComponent); override;
    property SyntaxError     : Boolean read FSyntaxError;
    property SyntaxLine      : Integer read FSyntaxLine;
    function SyntaxCheckLine(aText : string; var Success : Boolean) : string;
    procedure AddGCode(const GName : string; const GCodes : array of Word);
  published
    property StyleAxis       : TFontStyles read FstyleAxis       write FstyleAxis;
    property StyleGCode      : TFontStyles read FstyleGCode      write FstyleGCode;
    property StyleMCode      : TFontStyles read FstyleMCode      write FstyleMCode;
    property StyleLineNumber : TFontStyles read FstyleLineNumber write FstyleLineNumber;
    property StyleTool       : TFontStyles read FstyleTool       write FstyleTool;
    property StyleIJK        : TFontStyles read FstyleIJK        write FstyleIJK;
    property StyleComment    : TFontStyles read FstyleComment    write FstyleComment;
    property StyleSub        : TFontStyles read FstyleSub        write FStyleSub;
    property ColourAxis      : Tcolor     read FColourAxis       write FColourAxis;
    property ColourGCode     : Tcolor     read FColourGCode      write FColourGCode;
    property ColourMCode     : Tcolor     read FColourMCode      write FColourMCode;
    property ColourLineNumber: Tcolor     read FColourLineNumber write FColourLineNumber;
    property ColourTool      : Tcolor     read FColourTool       write FColourTool;
    property ColourIJK       : Tcolor     read FColourIJK        write FColourIJK;
    property ColourComment   : Tcolor     read FColourComment    write FColourComment;
    property ColourSub       : Tcolor     read FColourSub        write FColourSub;
  end;

procedure Register;

implementation
//{$R *.DCR}

function TSyntaxEditor.GetSyntaxOK : Boolean;
begin
  Result := True;
end;

constructor TPlcEditor.create(AOwner : TComponent);
begin
  inherited create(AOwner);
  Text.OnChange := key;
  comments := TCommentField.create;
  FSyntaxOK := True;
end;

destructor TPlcEditor.Destroy;
begin
  Comments.Free;
  inherited Destroy
end;

procedure TPlcEditor.key(Sender : TObject);
var line : string;
    incomment : boolean;
    StartM    : TPoint;
    EndM      : TPoint;
    i, count  : integer;
begin
  comments.clear;
  incomment := False;
  for i := 0 to Text.count - 1 do begin
    line := Text[i];
    for count := 1 to Length(line) do begin
      if line[count] = '{' then begin
        if not incomment then begin
          StartM.X := count;
          StartM.Y := i;
          incomment := True;
        end;
      end;

      if line[count] = '}' then begin
        if incomment then begin
          EndM.X := count;
          EndM.Y := i;
          incomment := False;
          comments.add(StartM, EndM);
        end;
      end;
    end;
  end;
  if incomment then begin
    EndM.X := length(line);
    EndM.Y := Text.count;
    comments.add(StartM, EndM);
  end;
end;

function  TPlcEditor.ParseLine(aText : string; Line : Integer) : string;
//procedure ParseLine(Y : integer);
var stp, working : string;
    p            : integer;
    comm         : TComments;
    point1, point2 : TPoint;
begin
  stp := aText;
  working := '';

  comm := comments.commentLine(Line);
  if cfAll in comm then begin
    working := '';
    addFont(working, [fsItalic, fsBold], $00b03000);
    Result := working + stp;
    exit;
  end;

  if (cfEnds in comm) and not (cfStarts in comm) then begin
    working := '';
    addFont(working, [fsItalic, fsBold], $00b03000);
    point1 := comments.EndPos(Line);
    working := working + Copy(stp, 1, point1.X);
    addFont(working, [], $0);
    Result := working + Copy(stp, point1.X + 1, length(stp));
    exit;
  end;

  if cfStarts in comm then begin
    point1 := comments.startPos(Line);
    working := Copy(stp, 1, point1.X - 1);
    addFont(working, [fsItalic, fsBold], $00b03000);

    if cfEnds in comm then begin
      point2 := comments.endPos(Line);
      working := working + Copy(stp, point1.X, point2.X - point1.X + 1);
      addFont(working, [], $0);
      Result := working + Copy(stp, point2.X + 1, length(stp));

    end else begin
      Result := working + Copy(stp, point1.X, length(stp));
    end;

    exit;
  end;

  p := Pos('//', stp);
  if p <> 0 then begin
    working := Copy(stp, 1, p - 1);
    Delete(stp, 1, p - 1);
    AddFont(working, [fsItalic, fsBold], $00b03000);
    Result := working + stp;
    exit;
  end;

  Result := stp;
end;

procedure TCommentField.clear;
var working : PTCommentRecord;
begin
  while start <> nil do begin
    working := start.next;
    Dispose(start);
    start := working;
  end;
end;

procedure TCommentField.add(b, e : TPoint);
var working : PTCommentRecord;
begin
  working := start;
  getMem(start, sizeof(TCommentRecord));
  start.next   := working;
  start.start  := b;
  start.finish := e;
end;

function TCommentField.CommentLine(Y : integer) : TComments;
var working : PTCommentRecord;
begin
  working := start;
  result := [];
  while working <> nil do begin
    if (working.Start.Y <= Y) and (working.Finish.Y >= Y) then begin
      if (working.Start.Y < Y) and (working.Finish.Y > Y) then Include(result, cfAll);
      if working.Start.Y = Y  then Include(result, cfStarts);
      if working.Finish.Y = Y then Include(result, cfEnds);
      exit;
    end;
    working := working.next;
  end;
  result := [cfNone];
end;

function TCommentField.startPos(Y : integer) : TPoint;
var working : PTCommentRecord;
begin
  working := start;
  result.Y := Y;
  while working <> nil do begin
    if working.Start.Y = Y  then begin
      result.X := working.start.X;
      exit;
    end;
    working := working.next;
  end;
  result.X := 0;
  result.Y := 0;
end;

function TCommentField.endPos(Y : integer) : TPoint;
var working : PTCommentRecord;
begin
  working := start;
  result.Y := Y;
  while working <> nil do begin
    if working.finish.Y = Y  then begin
      result.X := working.finish.X;
      exit;
    end;
    working := working.next;
  end;
  result.X := 0;
  result.Y := 0;
end;

constructor TGCodeEditor.create(AOwner : TComponent);
begin
  inherited create(Aowner);

  FGCodeGroups := TPermissiveGCode.Create;
  FstyleAxis       := [fsBold];
  FstyleGCode      := [fsBold];
  FstyleMCode      := [fsBold];
  FstyleLineNumber := [fsItalic];
  FstyleTool       := [fsBold];
  FstyleIJK        := [fsBold];
  FstyleComment    := [fsItalic];
  FStyleSub        := [fsBold, fsItalic];
  FColourAxis      :=  clBlue;
  FColourGCode     :=  clMaroon;
  FColourMCode     :=  clTeal;
  FColourLineNumber:=  clBlack;
  FColourTool      :=  clBlue;
  FColourIJK       :=  clGreen;
  FColourComment   :=  clNavy;
  FColourSub       :=  clAqua;

  AddGCode('Motion', [0, 1, $0101, 2, 3]);
  AddGCode('MotionMode', [61, 62]);
  AddGCode('Plane', [17, 18, 19]);
  AddGCode('CoordSys', [54, 55, 56, 57, 58, 59]);
  AddGCode('Units', [20, 21]);
  AddGCode('PositionMode', [90, 91]);
  AddGCode('Misc', [4]);
  // NOTE G65.1 may be programmed as $0141 or 256 + 65
  // NOTE G68.1 may be programmed as $0144 or 256 + 68

  AddGCode('UDGC', [$013c, $013d, $013e, $013f, $0140, $0141, $0142, $0144]);

  FSyntaxOK := True;
end;

function TGCodeEditor.isAxisName(a : string) : boolean;
var i : integer;
begin
  result := true;
  if Not Assigned(NC) then exit;
  
  for i := 0 to NC.AxisCount - 1 do begin
    if NC.AxisName[i] = a then exit;
  end;
  result := False;
end;

function TGCodeEditor.ReadGCode(var d : string; var s : string) : boolean;
  function ConvertDoubleToGCode(td : Double) : Word;
  begin
    Result := Trunc(td);
    td := (td - Trunc(td)) * 10;
    Result := Result + Round(td) shl 8;
  end;

var RData : Double;
    Code  : Word;
begin
  result := False;
  if not ReadDouble(d, s, RData) then exit;

  Code := ConvertDoubleToGCode(RData);
  if FGCodeGroups.CodeOK(Code) then
     result := True
  else
     insert(d, s, 1); // put back the text
end;

{ This function must not destroy or affect s on failure : d is nulled before start}
function TGCodeEditor.ReadDouble(var d : string; var s : string; var v : double) : boolean;
var sign, deci : boolean;
    i, code    : integer;

begin
  sign   := False;
  deci   := False;
  result := False;
  d      := '';

  for i := 1 to length(s) do begin
    case s[i] of
      '0'..'9': begin
               sign := True;
               d := d + s[i];
             end;

      '.'  : begin
               if deci then exit;
               deci := True;
               d := d + s[i];
             end;

      '+', '-' : begin
               if sign then exit;
               sign := True;
               d := d + s[i];
             end;
    else
      break;
    end;
  end;
  Val(d, tmpflo, code);
  if code = 0 then begin
    v := tmpflo;
    Delete(s, 1, length(d));
    result := True;
  end;
end;

{ This function must not destroy or affect s on failure : d is nulled before start}
function TGCodeEditor.ReadCardinal(var d : string; var s : string; var v : cardinal) : boolean;
var i : integer;
begin
  result := False;
  d      := '';

  for i := 1 to length(s) do begin
    case s[i] of
      '0'..'9' : d := d + s[i];
    else
      break;
    end;
  end;
  try
    v := strtoint(d);
    Delete(s, 1, length(d));
    result := True;
  except
    v := 0;
  end;
end;

function TGCodeEditor.ReadMCode(var d : string; var s : string; var m : boolean) : boolean;
var code : cardinal;
begin
  result := False;
  if m then exit; // One MCode per line
  m := True;
  if not ReadCardinal(d, s, code) then exit;
  if (code > 99) then begin
    System.Insert(d, s, 1);
    exit;
  end;
  result := True;
end;

{ Compares Selector + First token with SubroutineDesig. If successful Reads
  the next token assuming this to be the subroutine name (This is not
  verified here but at loading in the NC the subroutine integraty is
  checked).
  returns True if OK with d containing the parsed text and source trimmed else
  False with D and S unaltered }
function TGCodeEditor.ReadSubroutine(var d, s : string; Selector : Char) : boolean;
  procedure ReadToken(var Dst, Src : string);
  begin
    while (Length(Src) > 0) and (Src[1] <> ' ') do begin
      Dst := Dst + Src[1];
      System.Delete(Src, 1, 1);
    end
  end;

  procedure ReadWhiteSpace(var Dst, Src : string);
  begin
    while (Length(Src) > 0) and (Src[1] = ' ') do begin
      Dst := Dst + Src[1];
      System.Delete(Src, 1, 1);
    end;
  end;

var Tmp, SHold : string;
begin
  Result := False;
  SHold := S;
  Tmp := System.Copy(SHold, 1, 3);
  if Selector + Tmp = EditCallDesig then begin
    System.Delete(SHold, 1, 3);
    ReadWhiteSpace(Tmp, SHold);
    if Length(SHold) < 1 then Exit;
    ReadToken(Tmp, SHold);
    Result := True;
    D := Tmp;
    S := SHold;
  end;
  Exit;
end;

{ Add the error font and copy the remaining source to dest }
procedure TGCodeEditor.ErrorExit(var sf : string; var td : string; c : char);
begin
  AddFont(sf, [fsItalic], clRed);
  sf := sf + c + td;
  td := '';
  FSyntaxError := True;
  FSyntaxLine := ParsingLine;
end;

function TGCodeEditor.GetSyntaxOK : Boolean;
var I, OldVisTop : Integer;
begin
  OldVisTop := VisibleTop;
  FSyntaxError := False;
  for I := 0 to Text.Count - 1 do begin
    VisibleTop := I;
    ParseLine(Text[VisibleTop], VisibleTop);
    if SyntaxError then Break;
  end;

  VisibleTop := OldVisTop;
  Result := not SyntaxError;
end;

function TGCodeEditor.ParseLine(aText : string; aLine : integer) : string;
var stp, src, tmp : string;
    selector      : char;
    mused         : boolean;
    dv            : double;
    cv            : Cardinal;
    FPos          : Integer;
begin
  src := aText;
  stp := '';

  ParsingLine := aLine;  // Used for syntax error
  FGCodeGroups.NewLine;
  mused        := False;

  while true do begin
    if length(src) < 1 then begin
      Result := stp;
      exit;
    end;

    selector := src[1];
    System.Delete(src, 1, 1);

    case selector of
      ' '      : stp := stp + ' ';
      {'g',} 'G' : begin
                   if ReadGCode(tmp, src) then begin
                     AddFont(stp, StyleGCode, ColourGCode);
                     stp := stp + selector;
                     stp := stp + tmp;
                   end else begin
                     ErrorExit(stp, src, selector);
                   end;
                 end;

//      'd'..'f',
      'D'..'F',
//      'p'..'s',
      'P'..'S',
//      'h'..'l',
      'H'..'L' :   if ReadDouble(tmp, src, dv) then begin
                     addFont(stp, styleIJK, ColourIJK);
                     stp := stp + selector;
                     stp := stp + tmp;
                   end else begin
                     ErrorExit(stp, src, selector);
                   end;

//      'n'..'o',
      'N'..'O' :   if ReadCardinal(tmp, src, cv) then begin
                     addFont(stp, styleLineNumber, ColourLineNumber);
                     stp := stp + selector;
                     stp := stp + tmp;
                   end else begin
                     ErrorExit(stp, src, selector);
                   end;

      {'t',} 'T' :   if ReadCardinal(tmp, src, cv) then begin
                     addFont(stp, styleTool, ColourTool);
                     stp := stp + selector;
                     stp := stp + tmp;
                   end else begin
                     ErrorExit(stp, src, selector);
                   end;

      {'m',} 'M' :   if ReadMCode(tmp, src, mused) then begin
                     AddFont(stp, styleMCode, ColourMCode);
                     stp := stp + selector + tmp;
                   end else begin
                     ErrorExit(stp, src, selector);
                   end;

      {'c',} 'C' : if ReadDouble(tmp, src, dv) then begin
                   if isAxisName(selector) then AddFont(stp, styleAxis, ColourAxis)
                                           else AddFont(stp, styleIJK, ColourIJK);
                   stp := stp + selector;
                   stp := stp + tmp;
                 end else if ReadSubroutine(tmp, src, selector) then begin
                   AddFont(stp, StyleSub, ColourSub);
                   stp := stp + selector + tmp
                 end else begin
                   ErrorExit(stp, src, selector);
                 end;

//      'a'..'b',
      'A'..'B',
//      'u'..'z',
      'U'..'Z' : if ReadDouble(tmp, src, dv) then begin
                   if isAxisName(selector) then AddFont(stp, styleAxis, ColourAxis)
                                           else AddFont(stp, styleIJK, ColourIJK);
                   stp := stp + selector;
                   stp := stp + tmp;
                 end else begin
                   ErrorExit(stp, src, selector);
                 end;

      ';' :      begin
                   AddFont(Stp, styleComment, ColourComment);
                   Stp := Stp + Selector;
                   FPos := Pos('(', Src);
                   if FPos <> 0 then begin
                     Stp := Stp + Copy(Src, 1, FPos);
                     Delete(Src, 1, FPos);
                     AddFont(Stp, [fsItalic], clRed);
                     Stp := Stp + Src;
                     Src := '';
                     FSyntaxError := True;
                     FSyntaxLine := ParsingLine;
                   end else begin
                     Stp := Stp + Src;
                     Src := '';
                   end;
                 end;

      '/' :      stp := stp + selector;
    else
      ErrorExit(stp, src, selector);
    end;
  end;
  Result := stp;
end;

procedure TGCodeEditor.AddGCode(const GName : string; const GCodes : array of Word);
begin
  FGCodeGroups.AddGCode(GName, GCodes);
end;

function TGCodeEditor.SyntaxCheckLine(aText : string; var Success : Boolean) : string;
begin
  Result := ParseLine(aText, 0);
  Success := SyntaxError;
end;

procedure TPermissiveGCode.AddGCode(const GName : string; const GCodes : array of Word);
var I, J: Integer;
    Codes : TList;
begin
  for I := 0 to Count - 1 do begin
    if CompareText(Strings[I], GName) = 0 then begin
      Codes := Objects[I] as TList;
      for J := 0 to High(GCodes) do
        Codes.Add(TObject(GCodes[J]));
      Exit;
    end;
  end;

  Codes := TList.Create;
  for J := 0 to High(GCodes) do
    Codes.Add(TObject(GCodes[J]));

  AddObject(GName, Codes);
end;

function  TPermissiveGCode.CodeOK(GCode : Word) : Boolean;
var Codes : TList;
    I, J : Integer;
begin
  Result := False;

  for I := 0 to Count - 1 do begin
    Codes := TList(Objects[I]);
    for J := 0 to TList(Objects[I]).Count - 1 do begin
      if Integer(Codes[J]) = GCode then begin
        if TGCodeGroup(I) in Groups then Exit;
        Include(Groups, TGCodeGroup(I));
        Result := True;
        Exit;
      end;
    end;
  end;
end;

procedure TPermissiveGCode.NewLine;
begin
  Groups := [];
end;



procedure Register;
begin
  RegisterComponents('ACNC Editors', [TPlcEditor, TGCodeEditor, TStdEditor]);
end;

end.
