unit OCTPlugin;

interface

uses SysUtils, Classes, CoreCNC, CNC32, CNCTypes, CNCAbs, ComCtrls, CheckLst, Controls,
     Named, ExtCtrls, Windows, NCNames, StdCtrls, Graphics, Buttons, ImgList,
     FaultRegistration, IOCT, IToolLife, ToolLifePlugin;

type


  TOCTPlugin = class(TCNC {TExpandDisplay}, IFSDOCTHost)
  private
    OCTList : TStringList;
    GetFSDOCTInterface : TGetFSDOCTInterface;
    HOCT : IFSDOCT;

    ModeTag : TAbstractTag;
    CycleStartTag : TAbstractTag;
    InCycleTag : TAbstractTag;
    NormalTermination : TAbstractTag;
    OCTRunningTag : TAbstractTag;
    OCTModeTag : TAbstractTag;
    OCTSingleTag : TAbstractTag;
    OCTCompleteTag : TAbstractTag;
    OCTIndexTag : TAbstractTag;
    OCTTotalTag : TAbstractTag;
    CStartPermissive : TAbstractTag;

    OCTStatus : TOCTPluginStatii;
    Loading : Boolean;

    CStartCounter : Integer;
    Pending : Boolean;
    OCTLoaded : Boolean;

//    ToolLife : IFSDToolLife;
    procedure DisplaySweep(aTag : TAbstractTag);
    procedure LoadOCT(aTag : TAbstractTag);
    procedure LoadActiveOCT(aTag : TAbstractTag);
    procedure RunOCT(aTag : TAbstractTag);
    procedure ResetOCT(aTag : TAbstractTag);
    procedure SingleOCT(aTag : TAbstractTag);
    procedure SkipOPP(aTag : TAbstractTag);
    procedure CancelOCT(aTag : TAbstractTag);

    function UpdateState(HHHOCT : IFSDOCT; Command : TOCTPluginCommand; Data : Pointer) : Boolean; stdcall;
    function OCTLoadOK(FID : FHandle) : Boolean;
    function DoLoadOCT(const FileName : string) : Boolean;
  public
    constructor Create(aOwner : TComponent); override;
    procedure InstallComponent(Params : TParamStrings); override;
    procedure Installed; override;
    procedure IShutdown; override;
    procedure CheckInstallation; override;
    destructor Destroy; override;
    function GetToolLifeInterface : IFSDToolLife; stdcall;
  end;

implementation

resourcestring
  UnableToLoadOCTLang = 'Unable to load OCT "%s"';

var OCTInstance : TOCTPlugin;

{ TOCTPlugin }
function TOCTPlugin.UpdateState(HHHOCT : IFSDOCT; Command : TOCTPluginCommand; Data : Pointer) : Boolean; stdcall;
begin
  case Command of
    opcCycleStart : begin
      if not CycleStartTag.AsBoolean then begin
        CycleStartTag.AsBoolean := True;
        CStartCounter := 0;
      end else
        Pending := True;
    end;

    opcLoadOPP : begin
      if(OCTModeTag.AsBoolean) then begin
        Loading := True;
        try
          SysObj.FileManager.LoadActiveFile(PChar(Data));
        finally
          Loading := False;
        end;
      end;
    end;

    opcSetOPPList : begin
      OCTList.Text := PChar(Data);
      OCTTotalTag.AsInteger := OCTList.Count;
//      DoLoadOCT(PChar(Data));
      ResetOCT(nil);
      OCTModeTag.AsBoolean := True;
      OCTLoaded := True;
    end;

    opcSetTitle : begin
    end;

    opcSetStatus : begin
    end;

    opcRunning : begin
      if OCTRunningTag.AsBoolean <> (Integer(Data) <> 0) then
        OCTRunningTag.AsBoolean := (Integer(Data) <> 0);
    end;

    opcSetIndex : begin
      OCTIndexTag.AsInteger := Integer(Data);
    end;

    opcOPPComplete : begin
    end;

    opcOCTComplete : begin
      OCTCompleteTag.AsBoolean := Integer(Data) <> 0;
    end;
  end;
  Result := True;
end;



function TOCTPlugin.GetToolLifeInterface : IFSDToolLife;
begin
  Result := SysObj.ToolLife;
end;

constructor TOCTPlugin.Create(aOwner: TComponent);
begin
  inherited;

  OCTList := TStringList.Create;

  if Assigned(OCTInstance) then
    raise ECNCInstallFault.Create('Only a single OCT manager may be installed');
  OCTInstance := Self;
end;

destructor TOCTPlugin.Destroy;
begin
  inherited;
end;

procedure TOCTPlugin.DisplaySweep(aTag: TAbstractTag);
begin
  if NormalTermination.AsBoolean then
    Include(OCTStatus, opsOperationComplete)
  else
    Exclude(OCTStatus, opsOperationComplete);

  if CNCModeAuto in TCNCModes(Byte(ModeTag.AsInteger)) then
    Include(OCTStatus, opsAutoMode)
  else
    Exclude(OCTStatus, opsAutoMode);

  if CStartPermissive.AsBoolean then
    Include(OCTStatus, opsCStartPermissive)
  else
    Exclude(OCTStatus, opsCStartPermissive);

  if InCycleTag.AsBoolean then
    Include(OCTStatus, opsInCycle)
  else
    Exclude(OCTStatus, opsInCycle);

  if OCTModeTag.AsBoolean then
    Include(OCTSTatus, opsOCTMode)
  else
    Exclude(OCTSTatus, opsOCTMode);

  if (opsSkipOPP in OCTStatus) and (opsInCycle in OCTStatus) then
    Exclude(OCTStatus, opsSkipOPP);

  if not CycleStartTag.AsBoolean and Pending then begin
    CycleStartTag.AsBoolean := True;
    CStartCounter := 0;
    Pending := False;
  end;

  if CycleStartTag.AsBoolean and
     (CStartCounter > 5) then
    CycleStartTag.AsBoolean := False;

  Inc(CStartCounter);

  HOCT.Update(OCTStatus);

  Exclude(OCTStatus, opsSkipOPP)
end;

procedure TOCTPlugin.InstallComponent(Params: TParamStrings);
  function GetProcAddressX(hModule: HMODULE; lpProcName: LPCSTR): FARPROC;
  begin
    Result := GetProcAddress(hModule, lpProcName);
    if not Assigned(Result) then
      raise Exception.CreateFmt('Library interface missing [%s]', [lpProcName]);
  end;
var HLib : HModule;
    LibName : string;
    OCTName : string;
begin
  inherited;

  try
    LibName := Params.ReadString('Plugin', '*');
    if LibName = '*' then
      raise ECNCInstallFault.Create('Plugin property not set in profile "Plugin = DllName"');

    OCTName := Params.ReadString('Object', '*');

    HLib := SysObj.Comms.FindPlugin(LibName);
    if HLib = 0 then
      raise ECNCInstallFault.CreateFmt('Unable to load library "%s"', [LibName]);
    GetFSDOCTInterface := GetProcAddressX(HLib, GetFSDOCTInterfaceName);

    try
      HOCT := GetFSDOCTInterface(Self, OCTName);
    except
      on E : Exception do begin
        raise ECNCInstallFault.CreateFmt('Unable to Create OCT Plugin "%s"', [E.Message]);
      end;
    end;

    if not Assigned(HOCT) then
      raise ECNCInstallFault.CreateFmt('OCT "%s" not found in "%s"', [Name, LibName]);

    HOCT.LoadPath := LocalPath + 'Programs\';
    OCTRunningTag := TagPublish('OCTRunning', TMethodTag);
    OCTModeTag := TagPublish('OCTMode', TMethodTag);
    OCTSingleTag := TagPublish('OCTSingleStep', TMethodTag);
    OCTCompleteTag := TagPublish('OCTComplete', TMethodTag);
    OCTIndexTag := TagPublish('OCTIndex', TIntegerTag);
    OCTTotalTag := TagPublish('OCTTotal', TIntegerTag);
    HOCT.InstallName := Self.Name;
    SysObj.OCT := HOCT;
    HOCT.Install;
  except
    on E: Exception do
      CoreCNC.DelayedFatalError.Add(E.Message);
  end;
end;

procedure TOCTPlugin.Installed;
begin
  inherited;
  if Assigned(HOCT) then begin

    TagEvent(NameDisplaySweep, EdgeChange, TMethodTag, DisplaySweep);
    TagEvent('OCTReqRun', EdgeFalling, TMethodTag, RunOCT);
    TagEvent('OCTReqSingleStep', EdgeFalling, TMethodTag, SingleOCT);
    TagEvent('OCTReqReset', EdgeFalling, TMethodTag, ResetOCT);
    TagEvent('OCTReqLoad', EdgeFalling, TMethodtag, LoadOCT);
    TagEvent('OCTReqSkipOPP', EdgeFalling, TMethodTag, SkipOPP);
    TagEvent('OCTReqLoadActive', EdgeChange, TStringTag, LoadActiveOCT);
    TagEvent(NameNewActiveFile, EdgeChange, TStringTag, CancelOCT);

    with NCPublishedF[ncpActiveMode] do
      ModeTag := TagEvent(Format(Tag, [1]), EdgeChange, Size, nil);

    CycleStartTag := TagPublish('OCTCycleStart', TMethodTag);

    with NCPublishedF[ncpInCycle] do
      InCycleTag := TagEvent(Format(Tag, [1]), EdgeChange, Size, nil);

    NormalTermination := TagEvent(NameNC1NormalTermination, EdgeChange, TMethodTag, nil);
    CStartPermissive := TagEvent(NameNC1CStartPermissive, EdgeChange, TMethodTag, nil);

    OCTModeTag.AsBoolean := False;

    HOCT.FinalInstall;
  end;
end;

procedure TOCTPlugin.IShutdown;
begin
  if Assigned(HOCT) then
    HOCT.Shutdown;
end;

procedure TOCTPlugin.SkipOPP(aTag : TAbstractTag);
begin
  if OCTModeTag.AsBoolean then
    Include(OCTStatus, opsSkipOPP);
end;

procedure TOCTPlugin.LoadOCT(aTag: TAbstractTag);
var PPath : WideString;
    ThisPath, DefaultPath : array [0..1023] of Char;
begin
  if not ATag.AsBoolean then begin
    if not (opsInCycle in OCTStatus) then begin
      PPath := HOCT.LoadPath;
      StrPCopy(DefaultPath, PPath);
      StrCat(DefaultPath, '\*.oct');
      if Assigned(SysObj.AlternateLoad) then begin
        if SysObj.AlternateLoad(DefaultPath, ThisPath, True) then begin
          DoLoadOCT(ThisPath);
        end;
      end;
    end;
  end;
end;

procedure TOCTPlugin.LoadActiveOCT(aTag : TAbstractTag);
begin
  DoLoadOCT(aTag.AsString);
end;

function TOCTPlugin.DoLoadOCT(const FileName : string) : Boolean;
begin
  if not (opsInCycle in OCTStatus) then begin
    try
      HOCT.LoadOCT(FileName);
    except
      on E : Exception do begin
        SysObj.FaultInterface.SetFault(SysObj.Comms, CoreFaults[cfSysStopCycle], [Format(UnableToLoadOCTLang, [E.Message])], OCTLoadOK);
        OCTLoaded := False
      end;
    end;
  end;
  Result := OCTLoaded;
end;


procedure TOCTPlugin.ResetOCT(aTag: TAbstractTag);
begin
  if Assigned(HOCT) then
    if not InCycleTag.AsBoolean and OCTModeTag.AsBoolean then begin
      Pending := False;
      HOCT.Reset;
    end;
end;

procedure TOCTPlugin.RunOCT(aTag: TAbstractTag);
begin
  if Assigned(HOCT) then
    HOCT.Run;
end;

procedure TOCTPlugin.SingleOCT(aTag: TAbstractTag);
begin
  if opsSingle in OCTStatus then
    Exclude(OCTStatus, opsSingle)
  else
    Include(OCTStatus, opsSingle);

  OCTSingleTag.AsBoolean := opsSingle in OCTStatus;
end;

procedure TOCTPlugin.CancelOCT(aTag: TAbstractTag);
begin
//  EventLog('TOCTPlugin.CancelOCT');
  if not Loading then
    OCTModeTag.AsBoolean := False;
end;

procedure TOCTPlugin.CheckInstallation;
begin
  inherited;
//  HOCT.FinalInstall;
end;

function TOCTPlugin.OCTLoadOK(FID : FHandle) : Boolean;
begin
  Result := OCTLoaded or not OCTModeTag.AsBoolean
end;


initialization
  RegisterCNCClass(TOCTPlugin);
end.
