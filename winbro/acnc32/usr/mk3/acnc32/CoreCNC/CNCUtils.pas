unit CNCUtils;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, buttons, StdCtrls, CoreCNC, CNCTypes, named, CNC32;

const
  MAX_BUTTON_LINES   = 2;   // 2 = 3 when zero based

type
  TLedEvent = procedure (Sender : TObject; aBit : Integer) of Object;
  TIOLED = class(TGraphicControl)
  private
    FActive         : byte;
    FActiveColour   : TColor;
    FID             : string;
    FIDchar         : array [0..7] of char;
    FInActiveColour : TColor;
    Fvalue          : byte;
    FLedEvent       : TLedEvent;
    procedure SetValue(aValue : byte);
    procedure SetID(str : string);
    procedure setActive(mask : byte);
  protected
    procedure Paint; override;
    procedure PaintTag(I : Integer; C : TColor); virtual;
    procedure MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
  public
    constructor Create(AOwner : TComponent); override;
    procedure Resize; override;
    procedure Repaint; override;
    procedure SetBounds(ALeft, ATop, AWidth, AHeight: Integer); override;
  published
    property Value : byte read fvalue write SetValue;
    property ID : string read FID write SetID;
    property ActiveColour : TColor read FActiveColour write FActiveColour;
    property InActiveColour : TColor read FInActiveColour write FInActiveColour;
    property ActiveMask     : byte   read FActive         write setActive;
    property LedEvent : TLedEvent read FLedEvent write FLedEvent;
  end;

  TQuadIOLED = class(TGraphicControl)
  private
    FActiveColour, FInActiveColour : TColor;
    FValue : Integer;
    FMask : Integer;
    FOnLedEvent : TLedEvent;
    procedure SetValue(aValue : Integer);
    procedure SetMask(aMask : Integer);
  protected
    procedure MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
  public
    constructor Create(aOwner : TComponent); override;
    procedure Repaint; override;
    procedure Paint; override;
//    procedure SetBounds(ALeft, ATop, AWidth, AHeight: Integer); override;
    property Value : Integer read FValue write SetValue;
    property ActiveColour : TColor read FActiveColour write FActiveColour;
    property InActiveColour : TColor read FInActiveColour write FInActiveColour;
    property Mask : Integer   read FMask write SetMask;
    property OnLedEvent : TLedEvent read FOnLedEvent write FOnLedEvent;
  end;

  TonPressedEvent = procedure(Sender : TObject; value : boolean) of Object;

  TCNCButton = class(TCustomPanel)
  private
    FValue     : boolean;
    FText      : string;
    TextList   : array [0..MAX_BUTTON_LINES] of TLabel;
    FCount     : word;
    FonChange  : TonPressedEvent;
    FBitMapOn  : TBitMap;
    FBitMapOff : TBitMap;
    FEnabled   : Boolean;
    FFontName  : string;
    FFontStyle : TFontStyles;
    procedure setValue(aVal : boolean);
    procedure setText(s : string);
    procedure SetDown(aDown : Boolean);
    procedure labelPress(Sender : TObject; Button: TMouseButton;
          Shift: TShiftState; X, Y: Integer);
    procedure labelRelease(Sender : TObject; Button: TMouseButton;
          Shift: TShiftState; X, Y: Integer);
  protected
    procedure Nudge(change : TEditChange); virtual;
    procedure CMnudge (var Message : TMessage); message CNCM_NUDGE;
    procedure Resize; override;
    procedure MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
    procedure MouseUp(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
    procedure SetEnabled(aEna : Boolean); override;
  public
    constructor Create(Aowner : TComponent); override;
    constructor SetCreate(Aowner : TWinControl; callback : TonPressedEvent; text : string);
    procedure   Paint; override;
    procedure   Repaint; override;
  published
    property value     : boolean  read Fvalue write setValue;
    property Down      : Boolean read FValue     write SetDown;
    property Text      : string   read FText  write setText;
    property LineCount : word     read FCount;
    property onPressed : TonPressedEvent read FonChange write FonChange;
    property TabStop;
    property BevelWidth;
    property FontName : string read FFontName write FFontName;
    property FontStyle: TFontStyles read FFontStyle write FFontStyle;
    property BitMapOn  : TBitMap read FBitMapOn  write FBitMapOn;
    property BitMapOff : TBitMap read FBitMapOff write FBitMapOff;
    property Enabled   : Boolean read FEnabled   write SetEnabled;
  end;


  TStatusModifier = (
    smNone,
    smMStoHMS,
    smString,
    smInteger
  );

  TStatusControl = class(TCustomPanel)
  private
    FColourAlarm    : TColor;
    FColourActive   : TColor;
    FColourInactive : TColor;
    FTextActive     : string;
    FTextInActive   : string;
    FActive         : boolean;
    Fvalue          : double;
    FonUpperLimit   : TNotifyEvent;
    FonLowerLimit   : TNotifyEvent;
    FLimitLower     : double;
    FLimitUpper     : double;
    FFontName  : string;
    FFontStyle : TFontStyles;
    procedure setActive(act : boolean);
    procedure SetValue (aVal : double);
    procedure SetTextActive(aText : string);
    procedure SetTextInActive(aText : string);
    procedure UpdateControl;
  protected
    procedure Resize; override;
  public
    Tag : TAbstractTag;
    TagName : string;
    Polled : Boolean;
    Divisor : Double;

    ActiveTag     : TAbstractTag;
    ActiveTagName : string;
    ActivePolled  : Boolean;
    Modifier      : TStatusModifier;
    constructor Create(AOwner : TComponent); override;
  published
    property ColourAlarm    : TColor read FColourAlarm    write FColourAlarm;
    property ColourActive   : TColor read FColourActive   write FColourActive;
    property ColourInActive : TColor read FColourInactive write FColourInactive;
    property Active       : boolean  read FActive         write setActive;
    property value        : double   read Fvalue          write setValue;
    property TextActive   : string   read FTextActive     write SetTextActive;
    property TextInActive : string   read FTextInActive   write SetTextInActive;
    property onUpperLimit : TNotifyEvent read FonUpperLimit write FonUpperLimit;
    property onLowerLimit : TNotifyEvent read FonLowerLimit write FonLowerLimit;
    property LimitUpper   : double       read FLimitUpper   write FLimitUpper;
    property LimitLower   : double       read FLimitLower   write FLimitLower;
    property FontName : string read FFontName write FFontName;
    property FontStyle: TFontStyles read FFontStyle write FFontStyle;
  end;

implementation

constructor TStatusControl.create(AOwner : TComponent);
begin
  inherited create(AOwner);
  Divisor         := 1;
  Value           := 1;
  FColourAlarm    := clRed;
  FColourActive   := clYellow;
  FColourInActive := clLime;
  FTextActive     := 'ON %.1f';
  FTextInActive   := 'OFF';
  BevelWidth      := 2;
  BevelOuter      := bvRaised;
  Active          := False;
  Value           := 0;
  updateControl;
end;

procedure TStatusControl.Resize;
begin
  inherited Resize;
  Font.Style := FontStyle;
  Font.Name := FontName; 
  Font.Height := -(Height - 10);
end;

procedure TStatusControl.setActive(act : boolean);
begin
  if act = FActive then exit;
  FActive := act;
  updateControl;
end;

procedure TStatusControl.SetValue (aVal : double);
begin
  if FValue <> aVal then begin
    FValue := aVal;
    UpdateControl;
  end;
end;

procedure TStatusControl.SetTextActive(aText : string);
begin
  FTextActive := aText;
  UpdateControl;
end;

procedure TStatusControl.SetTextInActive(aText : string);
begin
  FTextInActive := aText;
  UpdateControl;
end;

procedure TStatusControl.UpdateControl;
var Tmp, Text : String;
    HSB : THSB;
    I, H, M, S : Integer;
begin
  if not Active then begin
    color := FColourInactive;
  end else begin
    if (value > FLimitUpper) then begin
       color := FColourAlarm;
       if assigned(onUpperLimit) then onUpperLimit(Self);
    end else if (value < FLimitLower) then begin
       color := FColourAlarm;
       if assigned(onLowerLimit) then onLowerLimit(Self);
    end else
       color := FColourActive;
  end;
  case Modifier of
    smNone, smInteger : begin
      if FActive then  fmtStr(Text, FTextActive, [FValue / Divisor])
      else             fmtStr(Text, FTextInActive, [FValue / Divisor]);
    end;

    smMSToHMS : begin
      I := Round(FValue / 1000);
      H := I div 3600;
      I := I mod 3600;
      M := I div 60;
      S := I mod 60;

      if H <> 0 then
        Tmp := Format('%d:%.2d:%.2d', [H, M, S])
      else
        Tmp := Format('%d:%.2d', [M, S]);


      if FActive then  fmtStr(Text, FTextActive, [Tmp])
      else             fmtStr(Text, FTextInActive, [Tmp]);
    end;

    smString : begin
      Text := FTextActive;
    end;
  end;
  HSB := CNC32.RGBToHSB(Color);
  if HSB.brightness > 0.5 then
    Font.Color := clBlack
  else
    Font.Color := clWhite;

  Caption := Text;
end;

constructor TCNCButton.Create(Aowner : TComponent);
var i : integer;
begin
  inherited create(Aowner);

  FFontName := Font.Name;
  for i := 0 to MAX_BUTTON_LINES do begin
    TextList[i]             := TLabel.create(Self);
    TextList[i].parent      := self;
    TextList[i].transparent := True;
    TextList[i].FocusControl:= Self;
    TextList[i].onMouseDown := labelPress;
    TextList[i].onMouseUp   := labelRelease;
  end;
  FBitMapOn := TBitMap.Create;
  FBitMapOn.Transparent := True;
  FBitMapOff := TBitMap.Create;
  FBitMapOff.Transparent := True;

  Value              := False;
  width              := 90;
  height             := 50;
  tabStop            := True;
  caption            := '';
  Text               := 'line 1~line 2~line 3';
end;

constructor TCNCButton.setCreate(Aowner : TWinControl; callback : TonPressedEvent; text : string);
begin
  Self := TCNCButton.create(Aowner);
  Self.parent := Aowner;
  Self.onPressed := callback;
  Self.Text := Text;
end;


procedure TCNCButton.labelPress(Sender : TObject; Button: TMouseButton;
          Shift: TShiftState; X, Y: Integer);
begin
  mousedown(Button, Shift, X, Y);
end;

procedure TCNCButton.labelRelease(Sender : TObject; Button: TMouseButton;
          Shift: TShiftState; X, Y: Integer);
begin
  mouseup(Button, Shift, X, Y);
end;

procedure TCNCButton.SetValue(aVal : boolean);
begin
  if FValue <> aVal then begin
    Down := aVal;
    if Assigned(FonChange) then
      FOnChange(self, FValue);
  end;
end;

procedure TCNCButton.SetDown(aDown : Boolean);
var I : integer;
begin
  if FValue <> aDown then begin
    Fvalue := aDown;
    if FValue then begin
              BevelOuter := bvLowered;
              for I := 0 to FCount do begin
                TextList[I].left := TextList[I].left + 1;
              end;

    end else begin
              BevelOuter := bvRaised;
              for i := 0 to FCount do begin
                TextList[I].left := TextList[I].left - 1;
              end;
    end;
    Repaint;
  end;
end;

procedure TCNCButton.paint;
begin
  inherited paint;

  if Fvalue then  canvas.draw(width - BitMapOn.width - CNCBorder, CNCBorder, BitMapOn)
            else  canvas.draw(width - BitMapOff.width - CNCBorder - 2, CNCBorder, BitMapOff);

  if Focused then Canvas.pen.color := clGray
  else            Canvas.pen.color := color;
//  Canvas.rectangle (2, 2, width - 2, height - 2); 
end;

procedure TCNCButton.repaint;
begin
  inherited repaint;
end;

procedure TCNCButton.nudge(change : TEditChange);
begin
  case change of
    EditChangeUp1,
    EditChangeUp2,
    EditChangeUp3    : Value := True;
    EditChangeDown1,
    EditChangeDown2,
    EditChangeDown3  : Value := False;
    EditChangeNone:  exit;
  end;
end;

procedure TCNCButton.CMnudge(var Message : TMessage);
begin
  nudge(TEditChange(Message.wparam));
end;

procedure   TCNCButton.SetText(s : string);
var  Position    : Integer;
begin
  if FText = s then exit;
  FText    := s;
  FCount   := 0;
  Position := Pos('~', s);
  while Position > 0 do begin
    TextList[Fcount].caption := Copy(s, 1, Position-1);
    Delete(s, 1, Position);
    Position := Pos('~', s);
    FCount   := FCount + 1;
    if FCount > MAX_BUTTON_LINES then exit;
  end;
  TextList[Fcount].caption := s;
  Resize;
end;

procedure TCNCButton.Resize;
var  hdisp : Integer;
     i     : integer;
begin
  inherited resize;

  for i := 0 to FCount do begin
    TextList[i].Font.Name := FontName;
    TextList[i].Font.Style := FontStyle;
    TextList[i].Font.Height := -Round(Height / 3.5)
  end;
  hdisp := height div 2;
  hdisp := hdisp -((TextList[0].height) * (FCount + 1)) div 2;

  for i := 0 to FCount do begin
    TextList[i].left := (width div 2) - (TextList[i].width div 2);
    TextList[i].top  :=  hdisp + (TextList[i].height) * i;
    TextList[i].visible := True;
  end;

  for i := FCount + 1 to MAX_BUTTON_LINES do begin
    TextList[i].visible := false;
  end;
end;

procedure TCNCButton.mousedown(Button: TMouseButton; Shift: TShiftState;
          X, Y: Integer);
begin
  inherited mouseDown(Button, Shift, X, Y);
  if TabStop and (not focused) then setFocus;
  if ssLeft in Shift then value := True;
end;

procedure TCNCButton.mouseUp(Button: TMouseButton; Shift: TShiftState;
          X, Y: Integer);
begin
  inherited mouseUp(Button, Shift, X, Y);;
  if not (ssLeft in Shift) then value := false;
end;

{procedure TCNCButton.setFontStyle(fs : TFontStyles);
var i : integer;
begin
  if fs = FFontStyle then exit;
  FFontStyle := fs;
  for i := 0 to MAX_BUTTON_LINES do
    TextList[i].font.style := fs;
end; }

{procedure TCNCButton.setFontSize(s : integer);
var i : integer;
begin
  if s = FFontSize then exit;

  FFontSize := s;
  for i := 0 to MAX_BUTTON_LINES do
    TextList[i].font.Size := s;
end;}

procedure TCNCButton.SetEnabled(aEna : Boolean);
var i : integer;
    tmp : TColor;
begin
  if aEna = FEnabled then
    Exit;

  FEnabled := aEna;

  if FEnabled then tmp := clBlack
              else tmp := clGray;
  for i := 0 to MAX_BUTTON_LINES do
    TextList[i].font.Color := tmp;
  inherited SetEnabled(aEna);
end;


constructor TQuadIOLed.Create(aOwner : TComponent);
begin
  inherited Create(AOwner);
  InActiveColour := clNavy;
  ActiveColour   := clYellow;
  FMask         := -1;
  ParentFont     := False;
end;

procedure TQuadIOLed.SetValue(aValue : Integer);
begin
  if aValue <> FValue then begin
    FValue := aValue;
    Repaint;
  end;
end;

procedure TQuadIOLed.SetMask(aMask : Integer);
begin
  if aMask <> FMask then begin
    FMask := aMask;
    Repaint;
  end;
end;

procedure TQuadIOLed.Repaint;
var I, Left : Integer;
    UnitWidth : Integer;
    Test : Integer;
begin
  UnitWidth := Width div 40;
  with Canvas do begin
    Pen.Color := clBlack;
    for I := 0 to 31 do begin
      Test := $80000000 shr I;
      if (FMask and Test) <> 0 then begin
        if (FValue and Test) <> 0 then
          Brush.Color := ActiveColour
        else
          Brush.Color := InActiveColour;
      end else
        Brush.Color := clSilver;

      Pen.Color := Brush.Color;
      Left := ((Width * I) div 34);
      Left := Left + (((I div 8) * UnitWidth) div 2) + (UnitWidth div 2);
      Rectangle(Left, 2, Left + UnitWidth, Height - 4);
    end;
  end;
end;

procedure TQuadIOLed.Paint;
var Topleft, BottomRight : array [0..2] of TPoint;
begin
  TopLeft[0].X := 0;
  TopLeft[0].Y := Height;
  TopLeft[1].X := 0;
  TopLeft[1].Y := 0;
  TopLeft[2].X := Width;
  TopLeft[2].Y := 0;

  BottomRight[0].X := Width;
  BottomRight[0].Y := 0;
  BottomRight[1].X := Width;
  BottomRight[1].Y := Height;
  BottomRight[2].X := 0;
  BottomRight[2].Y := Height;

  with Canvas do begin
    Pen.Color := clGray;
    Brush.Color := clGray;
    Rectangle(2, 2, Width - 4, Height - 4);
    Pen.Color := clWhite;
    MoveTo(0, Height);
    LineTo(0, 0);
    LineTo(Width, 0);
    LineTo(Width - 1, 1);
    LineTo(1, 1);
    LineTo(1, Height - 1);

    Pen.Color := clGray;
    LineTo(Width - 1, Height - 1);
    LineTo(Width - 1, 1);
    LineTo(Width, 0);
    LineTo(Width, Height);
    LineTo(0, Height);
    Repaint;
  end;
end;

{procedure TQuadIOLed.SetBounds(ALeft, ATop, AWidth, AHeight: Integer);
begin
  inherited SetBounds(ALeft, ATop, AWidth, AHeight);
// XXXXXXXX XXXXXXXX XXXXXXXX XXXXXXXX
// Each Led UnitWidth = width := Width div (2/4 + 4*8 + 3/2) [34]
// The Left of Nth Led would be
// (UnitWidth * (N + 1/4)) * UnitWidth + ((N mod 8) * (1/2 UnitWidth))
//
// UnitHeight = Height - 4
// The Entire Width / Height constitutes a psuedo panel
end; }

procedure TQuadIOLed.Mousedown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  if Assigned(FOnLedEvent) then
    FOnLedEvent(Self, Abs(31 - (((X * 32) div Width) mod 32)));
end;

constructor TIOLED.Create(AOwner : TComponent);
begin
  inherited Create(AOwner);
  InActiveColour := clNavy;
  ActiveColour   := clYellow;
  FActive        := $ff;
  ParentFont     := false;
end;

procedure TIOLED.SetValue (aValue : Byte);
begin
  if aValue = FValue then
    Exit;
  FValue := aValue;
  Repaint;
end;

procedure TIOLED.Paint;
var
  i     : integer;
  space : integer;
  Horiz : Integer;
begin
  inherited paint;
  Horiz := width div 12;

  with Canvas do begin
    for i := 0 to 7 do begin
      Space := (width div 8) * i;
      Pen.color := clWhite;
      Moveto(space + horiz + 1 + (horiz div 2), 2);
      Lineto(space + (horiz div 2), 2);
      Lineto(space + (horiz div 2), (height - 4));

      Pen.color := clGray;
      Moveto(space + (horiz div 2), (height - 4));
      Lineto(space + horiz + 1 + (horiz div 2), (height - 4));
      Lineto(space + horiz + 1 + (horiz div 2), 2);
      Brush.color := clBtnFace;
      Rectangle(space + 1 + (horiz div 2), 3, (space) + horiz + (horiz div 2), (height - 4));
    end;
  end;
  Repaint;
end;

procedure TIOLED.Repaint;
var
  I : integer;
  C : TColor;
  Space : integer;
  Horiz : Integer;
begin
  with Canvas do begin
    Horiz := width div 12;
    Pen.Color := clSilver;
    for I := 0 to 7 do begin
      Space := (width div 8) * i;
      if (($80 shr I) and FActive) <> 0 then begin
        if (($80 shr I) and Value) = 0 then begin
          Brush.Color := InActiveColour;
          C := ActiveColour;
        end else begin
          Brush.Color := ActiveColour;
          C := InActiveColour;
        end;
        Rectangle(Space + (Horiz div 2) + 1, 3, Space + Horiz + (Horiz div 2) , (Height - 4));
        PaintTag(7 - I, C);
      end;
    end;
  end;
end;

procedure TIOLED.PaintTag(I : Integer; C : TColor);
var
  Horiz, Space : Integer;
begin
  with Canvas do begin
    if FIDchar[I] <> #0 then begin
      Horiz := Width div 12;
      Space := (width div 8) * (7 - I);
      font.style := [];
      font.color := c;
      TextOut(space + (horiz div 2) + 4 , 4, FIDChar[i]);
    end;
  end;
end;

procedure TIOLED.MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  if Assigned(FLedEvent) then
    FLedEvent(Self, ((X * 8) div Width) mod 8);
end;

procedure TIOLED.SetBounds(ALeft, ATop, AWidth, AHeight: Integer);
begin
  inherited setBounds(ALeft, ATop, AWidth, AHeight);
  Resize;
end;

procedure TIOLED.resize;
var
  testSize : TLabel;
  i        : integer;
begin
  if length(FID) = 0 then exit;

  testSize := TLabel.create(Self);
  testSize.parentFont := true;
//  testsize.font.name  := 'Courier New';
  testSize.Caption    := 'Q';
  testSize.Font.Style := [];

//  canvas.font.name    := 'Courier New';
  canvas.Font.Style   := [];

  for i := 20 downto 1 do begin
    testSize.Font.Size  := i;
    if (testsize.height < height - 8) and (testsize.width < (width div 14)) then begin
      canvas.font.size := i;
      testsize.Free;
      exit;
    end;
  end;
  canvas.font.size := 1;
  testsize.free;
  Repaint;
end;

procedure TIOLED.setID(str : string);
var
  i : integer;
begin
  FID := str;
  for i := 0 to 7 do
    FIDchar[i] := #0;

  for i := 1 to length(str) do
    FIDChar[i-1] := FID[i];

  Repaint;
end;

procedure TIOLED.setActive(mask : byte);
begin
  if Factive = mask then exit;
  Factive := mask;
  paint;
end;


end.
