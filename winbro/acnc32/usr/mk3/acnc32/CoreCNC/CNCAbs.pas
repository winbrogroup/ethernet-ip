unit CNCAbs;

interface

uses Classes, CNCTypes, CNC32, CoreCNC;

type
  { An implementation of a TAbstractDisplay that
    1. Reads Parameter ExpandDisplay
    2. Registers Activate on the component 'name'
    The following sequence occurs on callback
    1. If not visible then call Show and Layout.Open's
    2. If visible Layout.Expand to the current ExpandToggle

    This component requires a decendant to override the abstract
    Show method }
  TExpandDisplay = class(TAbstractDisplay)
  private
  protected
    ExpandToggle : Boolean;
    DontExpand: Boolean;
  public
    procedure InstallComponent(Params : TParamStrings); override;
    procedure Installed; override;
    procedure Activate(aTag : TAbstractTag); override;
  end;

implementation


procedure TExpandDisplay.InstallComponent(Params : TParamStrings);
begin
  inherited InstallComponent(Params);
  DontExpand:= Params.ReadBool('DontExpand', False);

end;

procedure TExpandDisplay.Installed;
begin
  inherited Installed;
  TagEvent(Name, edgeFalling, TMethodTag, Activate);
end;

procedure TExpandDisplay.Activate(aTag : TAbstractTag);
begin
  // Changed to ensure only falling edge
  if not aTag.AsBoolean then begin
    if Self.DontExpand then begin
      Open;
    end
    else if not Visible then begin
      Open;
      SysObj.SoftKey.KeyChange(KeySetHandle);
      ExpandToggle := False;
    end
    else if not ExpandToggle then begin
      SysObj.Layout.Expand(Self, 0);
      SysObj.SoftKey.KeyChange(KeySetHandle);
      ExpandToggle := True;
    end
    else begin
      Close;
      Open;
      ExpandToggle := False;
    end;
  end;
end;


end.
