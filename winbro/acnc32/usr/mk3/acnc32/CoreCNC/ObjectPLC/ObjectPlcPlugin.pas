unit ObjectPlcPlugin;

interface

uses Classes, PlcItems, Plc32, PlcValueItem;

// A plugin must be determined at boot-time so that it may publish its required
// values to the named layer
type
  TObjectPlcPlugin = class(TPlcValueItem)
  public
    constructor Create(St : TPlcTokenizer; aName : string); override;
    procedure   Compile(Ele : PTPlcElement; St : TPlcTokenizer; Write : boolean); override;
    procedure   Write(Ele : PTPlcElement; line : boolean); override;
    function    Read(Ele : PTPlcElement) : boolean; override;
    function    GateName(Ele : PTPlcElement) : string; override;
    procedure   GatePost; override;
    procedure   ItemProperties(Ele : PTPlcElement; SL : TStrings); override;
  end;


implementation

constructor TObjectPlcPlugin.Create(St : TPlcTokenizer; aName : string);
begin
  inherited Create(St, aName);
end;

procedure TObjectPlcPlugin.Compile(Ele : PTPlcElement; St : TPlcTokenizer; Write : Boolean);
begin
end;

procedure   TObjectPlcPlugin.Write(Ele : PTPlcElement; line : boolean);
begin
end;

function    TObjectPlcPlugin.Read(Ele : PTPlcElement) : boolean;
begin
  Result := False;
end;

function    TObjectPlcPlugin.GateName(Ele : PTPlcElement) : string;
begin
  Result := '';
end;

procedure   TObjectPlcPlugin.GatePost;
begin
end;

procedure   TObjectPlcPlugin.ItemProperties(Ele : PTPlcElement; SL : TStrings);
begin
end;


end.
