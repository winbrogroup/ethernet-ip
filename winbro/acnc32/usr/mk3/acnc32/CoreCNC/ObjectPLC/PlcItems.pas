unit PlcItems;

interface

uses Plc32, CNCTypes, CNC32, SysUtils, Graphics, Windows, CoreCNC,
     StreamTokenizer, Classes;

type
  TPlcItem = class;

  EPlcTokenizer = class(Exception);
  TPlcTokenizer = class(TStreamQuoteTokenizer)
  private
    FTagOwner  : TTagOwner;
  protected
    function ParsedInputStream(Filename: string): TStream;
  public
    PlcIndex : Integer;
    Comment : string;
    Name     : string;
    function  skipComment : string;
    function  FindToken(token : string) : Boolean; override;
    procedure StdSeperator;
    procedure FloatSeperator;
    function  ReadDouble : Double;
//    function  ReadInteger : Integer;
    function  FindOrAddData (name : string) : TPlcItem; virtual; abstract;
    procedure AddData  (st : TPlcTokenizer; name, dtype : string); virtual; abstract;
    property  TagOwner : TTagOwner read FTagOwner;
  end;

  TEmbeddedData = record
    OData : TPlcItem;
    Command : Integer;
    DCommand : Integer; // Used by derived components
  end;

  TPlcLineState = (
    plsHasRisen,
    plsHasFallen,
    plsLastResult
  );

  TPlcLineStates = set of TPlcLineState;

  TPlcGraphicInfo = record
    Line      : Word;
    Rung      : Word;
    PosX      : Byte;
    PosY      : Byte;
    ConnectX  : Byte;
    ConnectY  : Byte;
    State : TPlcLineStates;
  end;

  TPlcElement = record
    OpCode    : TPlcOpCode;
    Embedded  : TEmbeddedData;
    Item      : TPlcItem;
    Line      : Boolean;
    Graphics  : TPlcGraphicInfo;
    PlcIndex : Integer;
  end;

  PTPlcElement = ^TPlcElement;
  ATPlcElement = array [0..MAX_ELEMENTS] of TPlcElement;
  PATPlcElement = ^ATPlcElement;

  ECompileItemFail = class(Exception);
  ECreateItemFail = class(Exception)
  public
    constructor Create(Text : string);
  end;

  IObjectPlcItem = interface
    ['{533BC6C0-FA50-408B-863D-F4999A65BC4C}']
    procedure Compile(Ele : PTPlcElement; St : TPlcTokenizer; Write : Boolean);
    procedure SetValue(Val : Double);
    function Read(Ele : PTPlcElement) : Boolean;
    procedure Write(ele : PTPlcElement; Line : Boolean);
    procedure GatePre;
    procedure GatePost;
  end;


  TPlcItemClass = class of TPlcItem;
  PTPLCItem = ^TPLCItem;
  TPLCItem = class(TInterfacedObject, IObjectPlcItem)
  private
  protected
    FPlcThread : TPlcTokenizer;
    FValue  : Integer;
    function FindOrAddItem(token : string) : TPlcItem;
    function GetDevice(Device : string) : TAbstractIODevice;
  public
    Name   : string;
    // Compile functions
    constructor Create(st : TPlcTokenizer; aName : string); virtual;
    destructor Destroy; override;
    procedure   Compile(ele : PTPlcElement; st : TPlcTokenizer; write : boolean); virtual;
    procedure   SetValue(val : double); virtual;
    // Runtime functions
    function    Read(ele : PTPlcElement) : boolean; virtual;
    procedure   Write(ele : PTPlcElement; line : boolean); virtual;
    procedure   GatePre; virtual;
    procedure   GatePost; virtual;
    class procedure InstallItem; virtual;
    class procedure FinalInstallItem; virtual;
    // Draw function
    procedure GatePaint(X, Y : integer; ele : PTPlcElement; canvas : TCanvas; IWidth, IHeight : Integer); virtual;
    function  GateName(Ele : PTPlcElement) : string; virtual;
    procedure ItemProperties(Ele : PTPlcElement; S : TStrings); virtual;
    property Value   : Integer read FValue write FValue;
    property PlcThread : TPlcTokenizer read FPlcThread;
    procedure UpdateTransientDisplay(Ele : PTPlcElement);
    class function PlcItemCount : Integer;
    class function RequiresMapping : Boolean; virtual;
    function _AddRef : Integer; stdcall;
    function _Release : Integer; stdcall;
  end;

  TPlcPlcValue = (
    plcplcFalse,
    plcplcTrue,
    plcplcPlc
  );

  TPlcPlcItem = class(TPlcItem)
  private
  public
    Enabled : array [0..MAX_NBR_OBJECT_PLC] of boolean;
    constructor Create(st : TPlcTokenizer; aName : string); override;
    procedure Compile(ele : PTPlcElement; st : TPlcTokenizer; write : boolean); override;
    function  Read(ele : PTPlcElement) : boolean; override;
    procedure Write(ele : PTPlcElement; line : boolean); override;
    function  GateName(ele : PTPlcElement) : string; override;
    procedure ItemProperties(Ele : PTPlcElement; S : TStrings); override;
    procedure DisableAll;
  end;

  TPlcForceCondition = (
    plcfcForceOn,
    plcfcForceOff,
    plcfcForceInvert
  );

  TPlcForceItem = class(TPlcItem)
  private
    FCondition : TPlcForceCondition;
    FForcedItem : TPlcItem;
  public
    Element : PTPlcElement;
    constructor Create(st : TPlcTokenizer; aName : string); override;
    procedure Compile(ele : PTPlcElement; st : TPlcTokenizer; write : boolean); override;
    function  Read(ele : PTPlcElement) : boolean; override;
    procedure Write(ele : PTPlcElement; line : boolean); override;
    function  GateName(ele : PTPlcElement) : string; override;
    procedure GatePre; override;
    procedure GatePost; override;
    procedure ItemProperties(Ele : PTPlcElement; S : TStrings); override;
    procedure GatePaint(X, Y : Integer; Ele : PTPlcElement; Canvas : TCanvas; IWidth, IHeight : Integer); override;
    property Condition : TPlcForceCondition read FCondition write FCondition;
    property ForcedItem : TPlcItem read FForcedItem write FForcedItem;
  end;


  ATPlcItem = array [0..MaximumPlcItems] of TPlcItem;
  PATPlcItem = ^ATPlcItem;

  procedure RegisterPlcItemClass(aName : string; PlcClass : TPlcItemClass);
  function FindPlcItemClass(aName : string) : TPlcItemClass;

var
  LastPlcTick : Cardinal;
  PlcClassList : TStringList;

{const
  PLC_ITEM_DRAW_HEIGHT  = 50;
  PLC_ITEM_DRAW_WIDTH   = 70; }


implementation

resourcestring
  DataConversionError = 'Data converion error on [%s]';
  BlockDeviceNotFoundFormat = 'Block Device Not Found [%s]';

const
  PlcValueLocalBit = $0001;

var
  PlcItemList : TList;

procedure RegisterPlcItemClass(aName : string; PlcClass : TPlcItemClass);
begin
  if not Assigned(PlcClassList) then
    PlcClassList := TStringList.Create;

  PlcClassList.AddObject(UpperCase(aName), Pointer(PlcClass));
end;

function FindPlcItemClass(aName : string) : TPlcItemClass;
var I : Integer;
begin
  Result := nil;
  I := PlcClassList.IndexOf(UpperCase(aName));
  if I <> -1 then
    Result := TPlcItemClass(PlcClassList.Objects[I]);
end;

constructor TPlcItem.Create(st : TPlcTokenizer; aName : string);
begin
  Self.name := aName;
  FPlcThread := St;
  PlcItemList.Add(Self);
end;

destructor TPlcItem.Destroy;
var I : Integer;
begin
  I := PlcItemList.Indexof(Self);
  if I <> -1 then
    PlcItemList.Delete(I);

  inherited Destroy;
end;

class function TPlcItem.PlcItemCount : Integer;
begin
  Result := PlcItemList.Count;
end;

class function TPlcItem.RequiresMapping : Boolean;
begin
  Result := False;
end;

procedure TPlcItem.Compile(ele : PTPlcElement; st : TPlcTokenizer; write : boolean);
begin
end;

function TPlcItem.Read(ele : PTPlcElement) : boolean;
begin
  Result := False;
end;

procedure TPlcItem.Write(ele : PTPlcElement; line : boolean);
begin end;

procedure TPlcItem.GatePre;
begin
end;

procedure TPlcItem.GatePost;
begin
end;

class procedure TPlcItem.InstallItem;
begin
end;

class procedure TPlcItem.FinalInstallItem;
begin
end;

procedure TPlcItem.SetValue(val : double);
begin
  FValue:= Round(Val);
end;

procedure TPlcItem.GatePaint(X, Y : integer; ele : PTPlcElement; canvas : TCanvas; IWidth, IHeight : Integer);
var Negative : Boolean;
    DRAW_Y_CENTER, DRAW_X_CENTER, DRAW_Y_BAR, DRAW_X_BAR : Integer;
    Tmp : Boolean;
begin
  DRAW_Y_CENTER    := IHeight div 2;
  DRAW_X_CENTER    := IWidth div 2;
  DRAW_Y_BAR       := IHeight div 7;
  DRAW_X_BAR       := IWidth div 10;

  with Canvas do begin
    Negative := Ele^.OpCode in NegatingPlcOpCodes;
    //Tmp := Self.Read(Ele);
    Tmp := Ele.Line;
    if Tmp xor Negative
      then Pen.Color := InstalledPlc.ActiveColor
      else Pen.Color := InstalledPlc.InactiveColor;

    if Ele.OpCode = PLCIoutN then
      Negative:= True;

    if ((plsHasRisen in Ele.Graphics.State) and not Tmp) or
       ((plsHasFallen in Ele.Graphics.State) and Tmp) or
        ([plsHasFallen, plsHasRisen] <= Ele.Graphics.State) then
      Pen.Color := clBlue;

    Ele.Graphics.State := Ele.Graphics.State - [plsHasFallen, plsHasRisen];

    X := X + DRAW_X_CENTER;
    Y := Y + DRAW_Y_CENTER;

    Moveto(X - DRAW_X_CENTER, Y);
    Lineto(X - DRAW_X_BAR,    Y);
    Moveto(X + DRAW_X_CENTER, Y);
    Lineto(X + DRAW_X_BAR,    Y);
    if Ele^.OpCode in OutputPLCOpCodes then begin
      Canvas.Ellipse(X-DRAW_Y_BAR, Y-DRAW_Y_BAR, X+DRAW_Y_BAR, Y+DRAW_Y_BAR);
    end else begin
           Moveto(X+DRAW_X_BAR, Y+DRAW_Y_BAR);
           Lineto(X+DRAW_X_BAR, Y-DRAW_Y_BAR);
           Moveto(X-DRAW_X_BAR, Y+DRAW_Y_BAR);
           Lineto(X-DRAW_X_BAR, Y-DRAW_Y_BAR);
    end;

    if Negative then begin
      Moveto(X-DRAW_X_BAR - 10,  Y-DRAW_Y_BAR);
      Lineto(X+DRAW_X_BAR + 10,  Y+DRAW_Y_BAR);
    end;
  end;
end;

function TPlcItem.GateName(ele : PTPlcElement) : string;
begin
  Result := '';
end;

function TPlcItem._AddRef : Integer; stdcall;
begin
  Result := -1;
end;

function TPlcItem._Release : Integer; stdcall;
begin
  Result := -1;
end;

procedure TPlcItem.UpdateTransientDisplay(Ele : PTPlcElement);
begin
  if Ele.Line <> (plsLastResult in Ele.Graphics.State) then begin
    if Ele.Line then
      Include(Ele.Graphics.State, plsHasRisen)
    else
      Include(Ele.Graphics.State, plsHasFallen)
  end;

  if Ele.Line then
    Include(Ele.Graphics.State, plsLastResult)
  else
    Exclude(Ele.Graphics.State, plsLastResult)
end;

procedure TPlcItem.ItemProperties(Ele : PTPlcElement; S : TStrings);
begin
end;


function TPlcItem.GetDevice(Device : string) : TAbstractIODevice;
begin
  Result := SysObj.IO.FindDevice(Device);
  if Result = nil then
    raise ECreateItemFail.CreateFmt(BlockDeviceNotFoundFormat, [Device]);
end;

function TPlcItem.FindOrAddItem(token : string) : TPlcItem;
begin
  if Assigned(PlcThread) then begin
    Result := PlcThread.FindOrAddData(token);
  end else begin
    raise ECreateItemFail.CreateFmt(ItemHasNoProperties, [Token])
  end;
end;

constructor TPlcPlcItem.Create(St : TPlcTokenizer; aName : string);
begin
  inherited Create(st, aName);
  FPlcThread := St;
end;

procedure TPlcPlcItem.Compile(Ele : PTPlcElement; St : TPlcTokenizer; write : boolean);
var token : string;
begin
  Token := St.SkipComment;
  if Token <> '.' then begin
    raise ECompileItemFail.CreateFmt(MethodNameMissing, [name]);
  end;

  Token := St.SkipComment;

  if CompareText('True', token) = 0 then begin
    Ele^.Embedded.Command := Ord(plcplcTrue);
  end else if CompareText('False', token) = 0 then begin
    Ele^.embedded.Command := Ord(plcplcFalse);
  end else begin
    try
      Ele^.Embedded.OData := FindOrAddItem(Token);
      Ele^.Embedded.Command := Ord(plcplcPlc);
    except On EConvertError Do
      raise ECompileItemFail.CreateFmt(DataConversionError, [token]);
    end;
  end;
end;

function TPlcPlcItem.Read(ele : PTPlcElement) : boolean;
begin
  case TPlcPlcValue(Ele.Embedded.Command) of
    plcplcFalse : Result := False;
    plcplcTrue : Result := True;
    plcplcPlc : Result := Enabled[Ele^.Embedded.OData.Value];
  else
    Result := False;
  end;
end;

procedure TPlcPlcItem.Write(ele : PTPlcElement; line : boolean);
begin
  // Note the only possible writable is plcplcPlc
//  if Line <> Ele^.Line then begin
    Enabled[Ele^.Embedded.OData.Value] := line;
{    if Line then
      FValue := FValue or (1 shl Ele^.Embedded.OData.Value)
    else
      FValue := FValue and not (1 shl Ele^.Embedded.OData.Value); }
//  end;
end;

function TPlcPlcItem.GateName(ele : PTPlcElement) : string;
begin
  case TPlcPlcValue(Ele^.Embedded.Command) of
    plcplcFalse : Result := BooleanIdent[False];
    plcplcTrue : Result := BooleanIdent[True];
    plcplcPlc : Result := Ele^.Embedded.OData.Name;
  end;
end;


procedure TPlcPlcItem.ItemProperties(Ele : PTPlcElement; S : TStrings);
var I : Integer;
begin
  for I := 1 to MAX_NBR_OBJECT_PLC do begin
    S.Add('Plc.' + IntToStr(I));
    S.Add(BooleanIdent[Enabled[I]]);
  end;
  S.Add('Plc.' + BooleanIdent[True]);
  S.Add(BooleanIdent[True]);
  S.Add('Plc.' + BooleanIdent[False]);
  S.Add(BooleanIdent[False]);
end;

procedure TPlcPlcItem.DisableAll;
var I : Integer;
begin
  for I := 1 to MAX_NBR_OBJECT_PLC do begin
    Enabled[I]:= False;
  end;
end;

constructor ECreateItemFail.Create(text : string);
begin
  inherited Create(text);
end;

procedure TPlcTokenizer.StdSeperator;
begin
  Seperator := './;!%^&*()[]{}';
end;

procedure TPlcTokenizer.FloatSeperator;
begin
  Seperator := '/;!%^&*()[]{}';
end;

function TPlcTokenizer.ReadDouble : Double;
var Tmp : string;
    code : Integer;
begin
  FloatSeperator;
  Tmp := Read;
  StdSeperator;
  Val(tmp, Result, code);
  if Code <> 0 then
    raise EPlcTokenizer.createFmt(DataConversionError, [Tmp]);
end;

{function TPlcTokenizer.ReadInteger : Integer;
var Tmp : string;
begin
  try
    Tmp := Read;
    Result := StrToInt(Tmp);
  except
    on EConvertError do
      raise EPlcTokenizer.createFmt(DataConversionError, [Tmp]);
  end;
end; }


function TPlcTokenizer.SkipComment : string;
var
  tmp      : string;
begin
  Result := Self.Read;
  if Result = '''' then begin
    Result := Self.Read;
    while Result = '''' do begin
      Result := Self.Read;
    end;
  end;

  if Result = '{' then begin
    Comment := '';
    while true do begin
      tmp := read;
      if tmp = '}' then begin
        Result := SkipComment;
        exit;
      end else
        Comment := Comment + tmp;
    end;
  end;

  if Result = '/' then begin
    if Self.Read = '/' then begin
      Comment := (Readline);
      Result := SkipComment;
      Exit;
    end else Push;
  end;
end;

function TPLCTokenizer.FindToken(token : string) : Boolean;
begin
  while (compareText(skipComment, token) <> 0) and
        (not EndOfStream) do;
  Result := True;
end;

constructor TPlcForceItem.Create(st : TPlcTokenizer; aName : string);
begin
  inherited Create(St, aName);
end;

procedure TPlcForceItem.Compile(ele : PTPlcElement; st : TPlcTokenizer; write : boolean);
begin
end;

function  TPlcForceItem.Read(ele : PTPlcElement) : boolean;
begin
  case Condition of
    plcfcForceOn : Result := True;
    plcfcForceOff : Result := False;
    plcfcForceInvert : Result := not ForcedItem.Read(Ele);
  else
    Result := False;
  end;
end;

procedure TPlcForceItem.Write(ele : PTPlcElement; line : boolean);
begin
  case Condition of
    plcfcForceOn : ForcedItem.Write(Ele, True);
    plcfcForceOff : ForcedItem.Write(Ele, False);
    plcfcForceInvert : ForcedItem.Write(Ele, not Line);
  end;
end;

procedure TPlcForceItem.GatePre;
begin
  ForcedItem.GatePre;
end;

procedure TPlcForceItem.GatePost;
begin
  ForcedItem.GatePost;
end;

function  TPlcForceItem.GateName(ele : PTPlcElement) : string;
begin
  Result := ForcedItem.GateName(Ele);
end;

procedure TPlcForceItem.ItemProperties(Ele : PTPlcElement; S : TStrings);
begin
  ForcedItem.ItemProperties(Ele, S);
end;

procedure TPlcForceItem.GatePaint(X, Y : integer; Ele : PTPlcElement; Canvas : TCanvas; IWidth, IHeight : Integer);
var PenColor : TColor;
begin
  inherited GatePaint(X, Y, Ele, Canvas, IWidth, IHeight);
  PenColor := Canvas.Pen.Color;
  Canvas.Pen.Color := clRed;
  X := X + (IWidth div 2);
  Y := Y + (IHeight div 2);
  Canvas.Ellipse(X-4, Y-4, X+4, Y+4);
  Canvas.Pen.Color := PenColor;
end;

function TPlcTokenizer.ParsedInputStream(Filename: string): TStream;
begin
  Result:= TStringStream.Create( SysObj.ConfigParser.ParseString(Filename) );
end;

initialization
  PlcItemList := TList.Create;
finalization
  PlcItemList.Free;
end.

