object PlcGatherConfigForm: TPlcGatherConfigForm
  Left = 531
  Top = 572
  Width = 749
  Height = 360
  Caption = 'Plc Gather Configuration'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -15
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 16
  object TabControl: TTabControl
    Left = 0
    Top = 0
    Width = 741
    Height = 273
    Align = alClient
    TabOrder = 0
    Tabs.Strings = (
      '1')
    TabIndex = 0
    OnChange = TabControlChange
    object Label1: TLabel
      Left = 16
      Top = 64
      Width = 59
      Height = 16
      Caption = 'Channel 1'
    end
    object Label2: TLabel
      Left = 16
      Top = 120
      Width = 59
      Height = 16
      Caption = 'Channel 1'
    end
    object Label3: TLabel
      Left = 16
      Top = 176
      Width = 59
      Height = 16
      Caption = 'Channel 1'
    end
    object Label4: TLabel
      Left = 16
      Top = 232
      Width = 59
      Height = 16
      Caption = 'Channel 1'
    end
    object Label5: TLabel
      Left = 96
      Top = 32
      Width = 25
      Height = 16
      Caption = 'Tag'
    end
    object Label6: TLabel
      Left = 232
      Top = 32
      Width = 26
      Height = 16
      Caption = 'Title'
    end
    object Label7: TLabel
      Left = 344
      Top = 32
      Width = 29
      Height = 16
      Caption = 'Filter'
    end
    object Label8: TLabel
      Left = 504
      Top = 32
      Width = 30
      Height = 16
      Caption = 'Units'
    end
    object Label9: TLabel
      Left = 624
      Top = 32
      Width = 45
      Height = 16
      Caption = 'Scaling'
    end
    object Tag1StaticText: TStaticText
      Left = 94
      Top = 56
      Width = 130
      Height = 30
      Alignment = taCenter
      AutoSize = False
      BorderStyle = sbsSunken
      TabOrder = 0
      OnClick = Tag1StaticTextClick
    end
    object Tag2StaticText: TStaticText
      Tag = 1
      Left = 94
      Top = 112
      Width = 130
      Height = 30
      Alignment = taCenter
      AutoSize = False
      BorderStyle = sbsSunken
      TabOrder = 1
      OnClick = Tag1StaticTextClick
    end
    object Tag3StaticText: TStaticText
      Tag = 2
      Left = 94
      Top = 168
      Width = 130
      Height = 30
      Alignment = taCenter
      AutoSize = False
      BorderStyle = sbsSunken
      TabOrder = 2
      OnClick = Tag1StaticTextClick
    end
    object Tag4StaticText: TStaticText
      Tag = 3
      Left = 94
      Top = 224
      Width = 130
      Height = 30
      Alignment = taCenter
      AutoSize = False
      BorderStyle = sbsSunken
      TabOrder = 3
      OnClick = Tag1StaticTextClick
    end
    object Field1Title: TEdit
      Left = 232
      Top = 56
      Width = 100
      Height = 24
      TabOrder = 4
      Text = 'Field1Title'
    end
    object Field2Title: TEdit
      Left = 232
      Top = 112
      Width = 100
      Height = 24
      TabOrder = 5
      Text = 'Edit1'
    end
    object Field3Title: TEdit
      Left = 232
      Top = 168
      Width = 100
      Height = 24
      TabOrder = 6
      Text = 'Edit1'
    end
    object Field4Title: TEdit
      Left = 232
      Top = 224
      Width = 100
      Height = 24
      TabOrder = 7
      Text = 'Edit1'
    end
    object FilterCombo1: TComboBox
      Left = 348
      Top = 56
      Width = 145
      Height = 24
      ItemHeight = 16
      TabOrder = 8
      Text = 'FilterCombo1'
      OnChange = FilterCombo1Change
    end
    object FilterCombo2: TComboBox
      Tag = 1
      Left = 348
      Top = 112
      Width = 145
      Height = 24
      ItemHeight = 16
      TabOrder = 9
      Text = 'ComboBox1'
      OnChange = FilterCombo1Change
    end
    object FilterCombo3: TComboBox
      Tag = 2
      Left = 348
      Top = 168
      Width = 145
      Height = 24
      ItemHeight = 16
      TabOrder = 10
      Text = 'ComboBox1'
      OnChange = FilterCombo1Change
    end
    object FilterCombo4: TComboBox
      Tag = 3
      Left = 348
      Top = 224
      Width = 145
      Height = 24
      ItemHeight = 16
      TabOrder = 11
      Text = 'ComboBox1'
      OnChange = FilterCombo1Change
    end
    object Unit1Edit: TEdit
      Left = 504
      Top = 56
      Width = 100
      Height = 24
      TabOrder = 12
    end
    object Unit2Edit: TEdit
      Left = 504
      Top = 112
      Width = 100
      Height = 24
      TabOrder = 13
    end
    object Unit3Edit: TEdit
      Left = 504
      Top = 168
      Width = 100
      Height = 24
      TabOrder = 14
    end
    object Unit4Edit: TEdit
      Left = 504
      Top = 224
      Width = 100
      Height = 24
      TabOrder = 15
    end
    object Scaling1Edit: TEdit
      Left = 624
      Top = 56
      Width = 100
      Height = 24
      TabOrder = 16
    end
    object Scaling2Edit: TEdit
      Left = 624
      Top = 112
      Width = 100
      Height = 24
      TabOrder = 17
    end
    object Scaling3Edit: TEdit
      Left = 624
      Top = 168
      Width = 100
      Height = 24
      TabOrder = 18
    end
    object Scaling4Edit: TEdit
      Left = 624
      Top = 224
      Width = 100
      Height = 24
      TabOrder = 19
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 314
    Width = 741
    Height = 19
    Panels = <>
    SimplePanel = False
  end
  object Panel1: TPanel
    Left = 0
    Top = 273
    Width = 741
    Height = 41
    Align = alBottom
    TabOrder = 2
    object BitBtn1: TBitBtn
      Left = 24
      Top = 2
      Width = 90
      Height = 34
      Caption = 'OK'
      Default = True
      TabOrder = 0
      OnClick = BitBtn1Click
      Glyph.Data = {
        DE010000424DDE01000000000000760000002800000024000000120000000100
        0400000000006801000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        3333333333333333333333330000333333333333333333333333F33333333333
        00003333344333333333333333388F3333333333000033334224333333333333
        338338F3333333330000333422224333333333333833338F3333333300003342
        222224333333333383333338F3333333000034222A22224333333338F338F333
        8F33333300003222A3A2224333333338F3838F338F33333300003A2A333A2224
        33333338F83338F338F33333000033A33333A222433333338333338F338F3333
        0000333333333A222433333333333338F338F33300003333333333A222433333
        333333338F338F33000033333333333A222433333333333338F338F300003333
        33333333A222433333333333338F338F00003333333333333A22433333333333
        3338F38F000033333333333333A223333333333333338F830000333333333333
        333A333333333333333338330000333333333333333333333333333333333333
        0000}
      NumGlyphs = 2
    end
    object BitBtn2: TBitBtn
      Left = 128
      Top = 2
      Width = 90
      Height = 34
      TabOrder = 1
      Kind = bkCancel
    end
  end
end
