unit PlcGather;

interface
uses
  SysUtils, Classes, Gather, CoreCNC, Windows, PlcItems, CNC32, PlcGatherConfig,
  Tokenizer;


type
  TLinearBuffer = array [0..1000000] of Integer;
  PLinearBuffer = ^TLinearBuffer;
  TGatherBuffer = class(TObject)
  private
    FSamplePeriod: Double;
    FCapacity : Cardinal;
    FCount : Cardinal;
    Buffer : array [0..3] of PLinearBuffer;
    Tags : array [0..3] of TAbstractTag;
    NextSampleTick : Int64;
    StartTick : Int64;
    function GetValue(Field : TGatherField; Index : Cardinal) : Integer;
    procedure SetValue(Field : TGatherField; Index : Cardinal; Value : Integer);
    procedure SetCapacity(aCapacity : Cardinal);
  public
    constructor Create;
    procedure Clear;
    procedure Read;
    procedure Start;
    property Capacity : Cardinal read FCapacity write SetCapacity;
    property Values[Field : TGatherField; Index : Cardinal] : Integer read GetValue write SetValue; default;
    property Count : Cardinal read FCount;
    property SamplePeriod : Double read FSamplePeriod write FSamplePeriod;
  end;

  TPlcGather = class(TAbstractGather)
  private
    IOInterruptFrequency : Integer;
    Buffers : array [TGatherHead] of TGatherBuffer;
    MaxSamplesPerField : Integer;
    FilterClass : array [TGatherField] of TGatherFilterClass;

    procedure NewLinearBuffer;
    procedure FreeLinearBuffer;
  protected
    procedure SetSamplePeriod(Value : Double); override;
    function GetSamplePeriod: Double; override;
    function GetSampleCount(aHead : Integer): Integer; override;
    function GetHeadData( i: Integer; Ch: Integer; Head: TGatherHead ): Integer; override;

    procedure DeleteGather; override;
    procedure StopGather(aHead : Integer); override;
    procedure StartGather(aHead : Integer); override;
    procedure DefineGather; override;
    function GetSamplePeriodIndex(aIndex : Integer) : Double; override;
    function GetIndexOfSamplePeriod(Index : Double) : Integer; override;
  protected
    procedure Config(aTag : TAbstractTag);
  public
    constructor Create(aOwner : TComponent); override;
    destructor Destroy; override;
    procedure InstallComponent(Params: TParamStrings); override;
    procedure Installed; override;
    procedure IShutdown; override;
    function GetAsInteger( i: Integer; Head: TGatherHead; Field: TGatherField): Integer;
    procedure Poll; override;
  end;

implementation

uses
  CNCTypes, TypInfo;

resourcestring
  CannotWhileStatus = 'Cannot %s while gather status is %s';
  StartOp = 'start gather';


function TPlcGather.GetSamplePeriodIndex(aIndex : Integer) : Double;
begin
  Result := 1/Integer(FAllowableFrequencies.Objects[aIndex]);
end;

function TPlcGather.GetIndexOfSamplePeriod(Index : Double) : Integer;
begin
  for Result := 0 to AllowableFrequencies.Count - 1 do begin
    if Round(1/Index) = Integer(AllowableFrequencies.Objects[Result]) then
      Exit;
  end;
  Result := 0;
end;

procedure TPlcGather.SetSamplePeriod(Value : Double);
var H : Integer;
begin
  for H := 0 to CoreCNC.SysObj.Heads - 1 do begin
    Buffers[H].SamplePeriod := Value;
  end;
end;

function TPlcGather.GetSamplePeriod: Double;
begin
  Result:= Buffers[0].SamplePeriod
end;

function TPlcGather.GetHeadData( i: Integer; Ch: Integer; Head: TGatherHead ): Integer;
begin
  Result:= GetAsInteger(i, Head, Ch)
end;

function TPlcGather.GetAsInteger( i: Integer; Head: TGatherHead; Field: TGatherField): Integer;
begin
//  ValidateBufferRead(i, Head, Field);
  Result := Integer(Buffers[Head][Field, I]);
end;


procedure TPlcGather.NewLinearBuffer;
begin
  FreeLinearBuffer;
end;

procedure TPlcGather.FreeLinearBuffer;
var I : Integer;
begin
  for I := 0 to High(TGatherField) do begin
    Buffers[I].Clear;
  end;
end;

function TPlcGather.GetSampleCount(aHead : Integer): Integer;
begin
  Result:= Buffers[aHead].Count;
end;


constructor TPlcGather.Create(aOwner : TComponent);
var I : Integer;
begin
  ChannelCount := 4;
  inherited Create(aOwner);
  for I := 0 to 3 do begin
    Buffers[I] := TGatherBuffer.Create;
  end;
end;

destructor TPlcGather.Destroy;
var I : Integer;
begin
  for I := 0 to High(TGatherField) do begin
    Buffers[I].Free;
  end;
  inherited Destroy;
end;


procedure TPlcGather.InstallComponent(Params: TParamStrings);
var I : Integer;
    TmpHeads : TGatherHeads;
begin
  IOInterruptFrequency := 100;
  for I := 1 to IOInterruptFrequency do
    if (IOInterruptFrequency div I) * I = IOInterruptFrequency then begin
      AllowableFrequencies.AddObject(Format('%.1fHz', [IOInterruptFrequency / I]), Pointer(IOInterruptFrequency div I));
    end;

  inherited InstallComponent(Params);

  MaxSamplesPerField := Params.ReadInteger('MaxSamplesPerField', 6000);

  for I := 0 to CoreCNC.SysObj.Heads - 1 do begin
    Buffers[I].Capacity := MaxSamplesPerField;
  end;


  TmpHeads := [];
  for I := 0 to CoreCNC.SysObj.Heads - 1 do begin
    Include(TmpHeads, I);
  end;
  Heads := TmpHeads;
end;

procedure TPlcGather.Installed;
var S : TQuoteTokenizer;
    Token : string;

  procedure ReadChannelName;
  var I : Integer;
  begin
    I := S.ReadIndex(4);
    ChannelName[I] := S.ReadStringAssign
  end;

  procedure ReadScaling;
  var I : Integer;
  begin
    I := S.ReadIndex(4);
    try
      Scaling[I] := StrToFloat(S.ReadStringAssign);
    except
      Scaling[I] := 1;
    end;
  end;

  procedure ReadUnits;
  var I : Integer;
  begin
    I := S.ReadIndex(4);
    Units[I] := S.ReadStringAssign
  end;

  procedure ReadFilter;
  var I : Integer;
  begin
    I := S.ReadIndex(4);
    FilterClass[I] := TAbstractGather.FindFilterClass(S.ReadStringAssign);
  end;

  procedure ReadHead;
    procedure ReadTag(H : Integer);
    var I : Integer;
    begin
      I := S.ReadIndex(4);
      Token := S.ReadStringAssign;
      if Token <> '' then
        Buffers[H].Tags[I] := TagEvent(Token, EdgeChange, TMethodTag, nil)
    end;

  var I : Integer;
  begin
    I := S.ReadIndex(SysObj.Heads);
    S.ExpectedTokens(['=', '{']);
    Token := LowerCase(S.Read);
    while not S.EndOfStream do begin
      if Token = 'tag' then
        ReadTag(I)
      else if Token = '}' then
        Exit
      else
        raise Exception.Create('Unexpected Token');
      Token := LowerCase(S.Read);
    end;
  end;

var H, F : Integer;
begin
  inherited Installed;

  TagEvent(Name + 'Config', EdgeFalling, TMethodTag, Config);

  S := TQuoteTokenizer.Create;
  try
    S.Seperator := '{}[]=';
    S.QuoteChar := '''';
    try
      ReadLive(S, Name);
      if S.Count > 0 then begin
        S.ExpectedTokens([Name, '=', '{']);
        Token := LowerCase(S.Read);
        while not S.EndOfStream do begin
          if Token = 'head' then
            ReadHead
          else if Token = 'field' then
            ReadChannelName
          else if Token = 'filter' then
            ReadFilter
          else if Token = 'scaling' then
            ReadScaling
          else if Token = 'units' then
            ReadUnits
          else if Token = '}' then
            Exit
          else
            raise Exception.Create('Unexpected Token');
          Token := LowerCase(S.Read);
        end;
      end;
    except
      on E : Exception do begin
        MessageLogFmt('Error at line [%d] in %s configuration', [S.Line, Name]);
      end;
    end;
  finally
    for F := 0 to High(TGatherField) do begin
      if Assigned(FilterClass[F]) then
        for H := 0 to CoreCNC.SysObj.Heads - 1 do
          Curve[H].InstallFilter(F, FilterClass[F].Create);
    end;
    S.Free;
  end;
end;

procedure TPlcGather.Config(aTag : TAbstractTag);
var Form : TPlcGatherConfigForm;
    H, F : Integer;
begin
  Form := TPlcGatherConfigForm.Create(Self);

  try
    for F := 0 to High(TGatherField) do begin
      Form.Scaling[F] := Scaling[F];
      Form.Units[F] := Units[F];
      Form.Filters[F] := FilterClass[F];
      Form.ChannelName[F] := ChannelName[F];
      for H := 0 to CoreCNC.SysObj.Heads - 1 do begin
        Form.Tags[H,F] := Buffers[H].Tags[F];
      end;
    end;
    if Form.Execute then begin
      for F := 0 to High(TGatherField) do begin
        FilterClass[F] := Form.Filters[F];
        ChannelName[F] := Form.ChannelName[F];
        Units[F] := Form.Units[F];
        Scaling[F] := Form.Scaling[F];
        CNC32.TokenizeString(FChannelName[F]);
        for H := 0 to CoreCNC.SysObj.Heads - 1 do begin
          if Assigned(FilterClass[F]) then
            Curve[H].InstallFilter(F, FilterClass[F].Create)
          else
            Curve[H].RemoveFilter(F);

          Buffers[H].Tags[F] := Form.Tags[H,F];
        end;
      end;
    end;
  finally
    Form.Free;
  end;
end;


procedure TPlcGather.IShutdown;
var S : TStringList;
  procedure WriteBuffer(H : Integer);
  var I : Integer;
  begin
    S.Add(Format('  Head[%d] = {', [H]));
    for I := 0 to 3 do
      if Assigned(Buffers[H].Tags[I]) then
        S.Add(Format('    Tag[%d] = ''%s''', [I, Buffers[H].Tags[I].Name]));


    S.Add('  }');
  end;

var I : Integer;
begin
  if GatherStatus = gsGathering then
    for I := 0 to SysObj.Heads - 1 do
      StopGather(I);
  DeleteGather;

  S := TStringList.Create;
  try
    S.Add(Name + ' = {');
    for I := 0 to 3 do begin
      S.Add(Format('  Field[%d] = ''%s''', [I, ChannelName[I]]));
      S.Add(Format('  Scaling[%d] = %f', [I, Scaling[I]]));
      S.Add(Format('  Units[%d] = ''%s''', [I, Units[I]]));
      if Assigned(FilterClass[I]) then
        S.Add(Format('  Filter[%d] = ''%s''', [I, FilterClass[I].ClassName]));
    end;
    for I := 0 to 3 do begin
      WriteBuffer(I);
    end;
    S.Add('}');
    WriteLive(S, Name);
  finally
    S.Free;
  end;
  inherited IShutDown;
end;


procedure TPlcGather.Poll;
var I : Integer;
begin
  if (GatherStatus = gsGathering) then begin
    for I := 0 to SysObj.Heads - 1 do begin
      if I in HeadsGathering then begin
        Buffers[I].Read;
      end;
    end;
  end
end;


procedure TPlcGather.DeleteGather;
begin
  if GatherStatus <> gsGathering then begin
    FGatherStatus:= gsUndefined
  end
end;

procedure TPlcGather.DefineGather;
var I : Integer;
begin
  if GatherStatus = gsGathering then
    for I := 0 to SysObj.Heads - 1 do
      StopGather(I);

  DeleteGather;
  FGatherStatus:= gsDefined
end;

procedure TPlcGather.StopGather(aHead : Integer);
begin
  if (GatherStatus = gsGathering) then
    Poll;

  Exclude(FHeadsGathering, aHead);

  if FHeadsGathering = [] then begin
    FGatherStatus:= gsDefined
  end;
end;

procedure TPlcGather.StartGather(aHead : Integer);
begin
  if FHeadsGathering = [] then begin
    NewLinearBuffer;
    FGatherStatus:= gsGathering
  end;

  Include(FHeadsGathering, aHead);
  Buffers[aHead].Start;
end;


constructor TGatherBuffer.Create;
begin
  inherited Create;
  Capacity := 1000;
  FCount := 0;
end;

procedure TGatherBuffer.Clear;
begin
  FCount := 0;
end;

function TGatherBuffer.GetValue(Field : TGatherField; Index : Cardinal) : Integer;
begin
  Result := 0;
  if Index < Capacity then
    Result := Buffer[Field, Index];
end;

procedure TGatherBuffer.SetValue(Field : TGatherField; Index : Cardinal; Value : Integer);
begin
  if Index < Capacity then
    Buffer[Field, Index] := Value;
end;

procedure TGatherBuffer.SetCapacity(aCapacity : Cardinal);
var I : Integer;
begin
  FCapacity := aCapacity;
  for I := 0 to High(TGatherField) do
    ReallocMem(Buffer[I], (Capacity + 1) * 4);
end;

procedure TGatherBuffer.Read;
var I : Integer;
begin
  while (FCount < Capacity) and (PlcTick.I64 >= NextSampleTick) do begin
    for I := 0 to High(TGatherField) do begin
      if Assigned(Tags[I]) then begin
        Buffer[I, Count] := Tags[I].AsInteger;
      end;
    end;
    Inc(FCount);
    NextSampleTick := StartTick + Round(Count * SamplePeriod * 1000);
  end;
end;

procedure TGatherBuffer.Start;
begin
  Clear;
  StartTick := PlcTick.I64;
  NextSampleTick := StartTick;
end;


initialization
  RegisterCNCClass(TPlcGather);
end.
