unit PlcGatherConfig;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, ComCtrls, Gather, CNC32, CoreCNC;

type
  TPlcGatherConfigForm = class(TForm)
    TabControl: TTabControl;
    Tag1StaticText: TStaticText;
    Tag2StaticText: TStaticText;
    Tag3StaticText: TStaticText;
    Tag4StaticText: TStaticText;
    StatusBar1: TStatusBar;
    Panel1: TPanel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Field1Title: TEdit;
    Field2Title: TEdit;
    Field3Title: TEdit;
    Field4Title: TEdit;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    FilterCombo1: TComboBox;
    FilterCombo2: TComboBox;
    FilterCombo3: TComboBox;
    FilterCombo4: TComboBox;
    Unit1Edit: TEdit;
    Unit2Edit: TEdit;
    Unit3Edit: TEdit;
    Unit4Edit: TEdit;
    Label8: TLabel;
    Label9: TLabel;
    Scaling1Edit: TEdit;
    Scaling2Edit: TEdit;
    Scaling3Edit: TEdit;
    Scaling4Edit: TEdit;
    procedure Tag1StaticTextClick(Sender: TObject);
    procedure TabControlChange(Sender: TObject);
    procedure FilterCombo1Change(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
  private
    procedure UpdateDisplay;
  public
    Tags : array [TGatherHead, TGatherField] of TAbstractTag;
    Titles : array [TGatherField] of string;
    Filters : array [TGatherField] of TGatherFilterClass;
    Units : array [TGatherField] of string;
    Scaling : array [TGatherField] of Double;
    ChannelName : array [TGatherField] of string;
    function Execute : Boolean;
  end;

var
  PlcGatherConfigForm: TPlcGatherConfigForm;

implementation

{$R *.DFM}

uses SelectTagForm;

function TPlcGatherConfigForm.Execute : Boolean;
var I : Integer;
begin

  Field1Title.Text := ChannelName[0];
  Field2Title.Text := ChannelName[1];
  Field3Title.Text := ChannelName[2];
  Field4Title.Text := ChannelName[3];

  Unit1Edit.Text := Units[0];
  Unit2Edit.Text := Units[1];
  Unit3Edit.Text := Units[2];
  Unit4Edit.Text := Units[3];

  Scaling1Edit.Text := Format('%.4f', [Scaling[0]]);
  Scaling2Edit.Text := Format('%.4f', [Scaling[1]]);
  Scaling3Edit.Text := Format('%.4f', [Scaling[2]]);
  Scaling4Edit.Text := Format('%.4f', [Scaling[3]]);

  TabControl.Tabs.Clear;
  for I := 0 to CoreCNC.SysObj.Heads - 1 do begin
    TabControl.Tabs.Add(IntToStr(I + 1));
  end;

  TAbstractGather.CurrentFilterList(FilterCombo1.Items);
  TAbstractGather.CurrentFilterList(FilterCombo2.Items);
  TAbstractGather.CurrentFilterList(FilterCombo3.Items);
  TAbstractGather.CurrentFilterList(FilterCombo4.Items);

  TabControl.TabIndex := 0;
  UpdateDisplay;
  Result := ShowModal = mrOK;

  ChannelName[0] := Field1Title.Text;
  ChannelName[1] := Field2Title.Text;
  ChannelName[2] := Field3Title.Text;
  ChannelName[3] := Field4Title.Text;

  Units[0] := Unit1Edit.Text;
  Units[1] := Unit2Edit.Text;
  Units[2] := Unit3Edit.Text;
  Units[3] := Unit4Edit.Text;
end;

procedure TPlcGatherConfigForm.Tag1StaticTextClick(Sender: TObject);
begin
  with Sender as TStaticText do
    Tags[TabControl.TabIndex, Tag] := TSelectTag.Execute([TMethodTag]);

  UpdateDisplay;
end;

procedure TPlcGatherConfigForm.FilterCombo1Change(Sender: TObject);
begin
  with Sender as TComboBox do
    Filters[Tag] := TGatherFilterClass(Items.Objects[ItemIndex])
end;


procedure TPlcGatherConfigForm.UpdateDisplay;
  procedure SetText(aText : TStaticText; aTag : TAbstractTag);
  begin
    if Assigned(aTag) then
      aText.Caption := aTag.Name
    else
      aText.Caption := 'None';
  end;

  procedure SetCombo(aCombo : TComboBox; Filter : TGatherFilterClass);
  begin
    if Assigned(Filter) then
      aCombo.ItemIndex := aCombo.Items.IndexOf(Filter.ClassName)
    else
      aCombo.ItemIndex := 0;
  end;

begin
  SetText(Tag1StaticText, Tags[TabControl.TabIndex, 0]);
  SetText(Tag2StaticText, Tags[TabControl.TabIndex, 1]);
  SetText(Tag3StaticText, Tags[TabControl.TabIndex, 2]);
  SetText(Tag4StaticText, Tags[TabControl.TabIndex, 3]);

  SetCombo(FilterCombo1, Filters[0]);
  SetCombo(FilterCombo2, Filters[1]);
  SetCombo(FilterCombo3, Filters[2]);
  SetCombo(FilterCombo4, Filters[3]);
end;

procedure TPlcGatherConfigForm.TabControlChange(Sender: TObject);
begin
  UpdateDisplay;
end;

procedure TPlcGatherConfigForm.BitBtn1Click(Sender: TObject);
begin
  try
    Scaling[0] := StrToFloat(Scaling1Edit.Text);
    Scaling[1] := StrToFloat(Scaling2Edit.Text);
    Scaling[2] := StrToFloat(Scaling3Edit.Text);
    Scaling[3] := StrToFloat(Scaling4Edit.Text);
    ModalResult := mrOK;
  except
    MessageDlg( 'Invalid scaling values', mtError, [mbOK], 0);
  end;
end;

end.
