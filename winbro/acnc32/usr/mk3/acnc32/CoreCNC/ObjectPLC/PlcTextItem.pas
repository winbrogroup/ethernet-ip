unit PlcTextItem;

interface

uses CoreCNC, CNC32, CNCTypes, Plc32, PlcItems, Windows, SysUtils, Classes;

type
  TPlcTextItem = class(TPlcItem)
  private
    FText : string;
    State : Boolean;
  public
    constructor Create(st : TPlcTokenizer; aName : string); override;
    destructor  Destroy; override;
    procedure Compile(Ele : PTPlcElement; St : TPlcTokenizer; Write : boolean); override;
    procedure  Write(Ele : PTPlcElement; Line : boolean); override;
    function   Read(Ele : PTPlcElement) : boolean; override;
    procedure ItemProperties(Ele : PTPlcElement; S : TStrings); override;
    function    GateName(Ele : PTPlcElement) : string; override;

    class procedure SetTextCount(aCount : Integer);
    class procedure InstallItem; override;
    property Text: string read FText;
  end;

implementation

uses
  PlcGORItem;

var
  PlcTextMPList : TList;
  PlcTextCount : Integer;

const
  PlcMessageTextFormat = 'PlcMessageText%d';

resourcestring
  ThisTextLang = 'This Text';

class procedure TPlcTextItem.SetTextCount(aCount : Integer);
begin
  PlcTextCount := aCount;
end;

constructor TPlcTextItem.Create(st : TPlcTokenizer; aName : string);
begin
  inherited Create(St, aName);

  FText :=   SysObj.Localized.GetString( St.Read );
  if St.Read <> ';' then
    raise ECreateItemFail.CreateFmt(SemicolonMissing, [aName]);
  FText := Trim(FText);
end;

destructor  TPlcTextItem.Destroy;
begin
  GORList.RemoveText(Self);
  inherited Destroy;
end;

procedure TPlcTextItem.Compile(Ele : PTPlcElement; St : TPlcTokenizer; Write : boolean);
begin
  if St.Read <> '.' then
    raise ECompileItemFail.CreateFmt(MethodNameMissing, [Name]);
  Ele^.Embedded.OData := FindOrAddItem(St.Read);
end;

procedure  TPlcTextItem.Write(Ele : PTPlcElement; Line : boolean);
var Tmp : Integer;
begin
  if Line and not Ele^.Line then begin
    Tmp := Ele^.Embedded.OData.Value;
    Dec(Tmp);
    if (Tmp < PlcTextMPList.Count) and (Tmp >= 0) then begin
      TAbstractTag(PlcTextMPList[Tmp]).AsString := Text;
      PostMessage(InstalledPlc.Handle, CNCM_PLC_EVENT, 0, Integer(PlcTextMPList[Tmp]));
    end;
  end;

  State := Line;
end;

function   TPlcTextItem.Read(Ele : PTPlcElement) : boolean;
begin
  Result := State;
end;

procedure TPlcTextItem.ItemProperties(Ele : PTPlcElement; S : TStrings);
var I : Integer;
begin
  S.Add(ThisTextLang);
  S.Add(Text);
  for I := 0 to PlcTextMPList.Count - 1 do begin
    S.Add(Format(PlcMessageTextFormat, [I+1]));
    S.Add(TAbstractTag(PlcTextMPList[I]).AsString);
  end;
end;

class procedure TPlcTextItem.InstallItem;
var STag : TAbstractTag;
    I : Integer;
begin
  for I := 0 to PlcTextCount - 1 do begin
    STag := InstalledPlc.TagPublish(Format(PlcMessageTextFormat, [I+1]), TStringTag);
    STag.IsVCL := False;
    PlcTextMPList.Add(STag);
  end;
end;


function TPlcTextItem.GateName(Ele: PTPlcElement): string;
begin
  Result:= Name;
end;

initialization
  PlcTextMPList := TList.Create;
  RegisterPlcItemClass('Text', TPlcTextItem);
finalization
  PlcTextMPList.Free
end.
