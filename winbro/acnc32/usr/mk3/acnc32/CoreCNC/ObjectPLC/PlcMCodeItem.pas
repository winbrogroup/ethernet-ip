unit PlcMCodeItem;

interface

uses PlcItems, Plc32, CNC32, SysUtils, CNCTypes, Classes, PlcValueItem;

type
  TPlcMCodeItem = class(TPlcNamedItemR)
    ICode : array [0..3] of Integer;
    ResetActive : Boolean;
  public
    constructor Create(st : TPlcTokenizer; name : string); override;
    procedure   Compile(ele : PTPlcElement; st : TPlcTokenizer; write : boolean); override;
    procedure   Write(ele : PTPlcElement; line : boolean); override;
    function    Read(ele : PTPlcElement) : boolean; override;
    function    GateName(ele : PTPlcElement) : string; override;
    procedure   GatePost; override;
    class function RequiresMapping : Boolean; override;
    procedure   ItemProperties(Ele : PTPlcElement; S : TSTrings); override;
  end;

implementation

const
  ActiveDataType    = $0;
  InterlockDataType = $1;
  MCodeDataType     = $2;
  ResetType         = $3;
  MCodeActiveType   = $4;

  InterlockLang = 'AFC';
  ResetName = 'Reset';
  ActiveLang = 'Active';

//var
//  ICode : array [0..3] of Integer;

constructor TPlcMCodeItem.Create(st : TPlcTokenizer; name : string);
begin
  inherited Create(st, name);
  ICode[0] := 0;
  ICode[1] := 0;
  ICode[2] := 0;
  ICode[3] := 0;
end;

class function TPlcMCodeItem.RequiresMapping : Boolean;
begin
  Result := True;
end;


procedure   TPlcMCodeItem.Compile(Ele : PTPlcElement; St : TPlcTokenizer; Write : boolean);
var Token : string;
begin
  Token := St.Read;

  if Token <> '.' then
    raise ECompileItemFail.CreateFmt(MethodNameMissing, [name]);

  Token := st.Read;

  if CompareText(Token, InterlockLang) = 0 then begin
    Ele.Embedded.Command := ActiveDataType;
    if Write then
      raise ECompileItemFail.CreateFmt(ObjectIsReadOnly, [Name]);
  end else if CompareText(Token, ResetName) = 0 then begin
    Ele^.Embedded.Command := ResetType;
  end else if CompareText(Token, ActiveLang) = 0 then begin
    Ele^.Embedded.Command:= MCodeActiveType;
  end else begin
    case Token[1] of
      'I' : Ele.Embedded.Command := InterlockDataType;
      'M' : Ele.Embedded.Command := MCodeDataType;
    else
      raise ECompileItemFail.CreateFmt(InvalidMethodName, [Token]);
    end;
    System.Delete(Token, 1, 1);
    Ele.Embedded.OData := FindOrAddItem(Token);
  end;
end;

procedure TPlcMCodeItem.GatePost;
begin
end;


procedure   TPlcMCodeItem.Write(ele : PTPlcElement; line : boolean);
  procedure SetInterlock(N, B : Integer);
  begin
    ICode[N] := ICode[N] or (1 shl B)
  end;

  procedure ResetInterlock(N, B : Integer);
  begin
    ICode[N] := ICode[N] and not(1 shl B)
  end;

var
  N, B : Byte;
begin
  case Ele.Embedded.Command of
    InterlockDataType : begin
      N := Ele.Embedded.OData.Value div 32;
      B := Ele.Embedded.OData.Value mod 32;
      if Line then SetInterlock(N, B)
              else ResetInterlock(N, B);
    end;

    ResetType : begin
      ResetActive := Line;
      if Line then begin
        ICode[0] := 0;
        ICode[1] := 0;
        ICode[2] := 0;
        ICode[3] := 0;
      end;
    end;
  end;
end;

function    TPlcMCodeItem.Read(ele : PTPlcElement) : boolean;
begin
  case Ele.Embedded.Command of
    ActiveDataType :
                     Result := (ICode[0] = 0) and
                               (ICode[1] = 0) and
                               (ICode[2] = 0) and
                               (ICode[3] = 0) and
                               (Value <> 0);
    InterlockDataType :
                      Result := (ICode[Ele.Embedded.OData.Value div 32] and
                                (1 shl (Ele.Embedded.OData.Value mod 32))) <> 0;

    MCodeDataType :
                    Result := Value = Ele.Embedded.OData.Value;

    MCodeActiveType: Result:= Value <> 0;
  else
    Result := False;
  end;


end;

function    TPlcMCodeItem.GateName(ele : PTPlcElement) : string;
begin
  case Ele^.Embedded.Command of
    ActiveDataType : Result := InterlockLang;
    InterLockDataType : Result := 'I' + IntToStr(Ele.Embedded.OData.Value);
    MCodeDataType : Result := 'M' + IntToStr(Ele.Embedded.OData.Value);
    ResetType : Result := ResetName;
  end;
end;

procedure TPlcMCodeItem.ItemProperties(Ele : PTPlcElement; S : TSTrings);
begin
  S.Add(InterlockLang);
  S.Add(BooleanIdent[(ICode[0] = 0) and
                     (ICode[1] = 0) and
                     (ICode[2] = 0) and
                     (ICode[3] = 0) and
                     (Value <> 0)]);

  S.Add('MCode');
  S.Add(IntToStr(FValue));
  S.Add('I0..31');
  S.Add(IntToStr(ICode[0]));
  S.Add('I32..63');
  S.Add(IntToStr(ICode[1]));
  S.Add('I64..95');
  S.Add(IntToStr(ICode[2]));
  S.Add('I96..127');
  S.Add(IntToStr(ICode[3]));
  S.Add(ResetName);
  S.Add(BooleanIdent[ResetActive]);
end;

initialization
  RegisterPlcItemClass('MCode', TPlcMCodeItem);
end.
