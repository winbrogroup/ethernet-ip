unit PlcAlarm;

interface

uses PlcItems, Plc32, CNC32, SysUtils, CNCTypes, Classes, Graphics, CoreCNC,
     PlcValueItem, PlcTextItem, SyncObjs, Named, FaultRegistration;


const BaseFault = 9000;

type
  TAlarm = 0..1023;

  TPlcAlarm = class(TPlcItem)
  private
    AlarmActive: array[TAlarm] of Boolean;
    AlarmPrevious: array[TAlarm] of Boolean;
    AlarmFID: array[TAlarm] of FHandle;
  public
    constructor Create(St : TPlcTokenizer; aName : string); override;
    procedure   Compile(Ele : PTPlcElement; St : TPlcTokenizer; Write : boolean); override;
    procedure   Write(Ele : PTPlcElement; line : boolean); override;
    function    Read(Ele : PTPlcElement) : boolean; override;
    function    GateName(Ele : PTPlcElement) : string; override;
    procedure   ItemProperties(Ele : PTPlcElement; SL : TStrings); override;
    procedure GatePost; override;
  end;

  TFixedHandler = class
    function ResetFault(FID: FHandle): Boolean;
  end;

implementation

var
  PlcFaultRegistration : array [TAlarm] of TFaultRegistration;
  FixedHandler: TFixedHandler;



{ TPlcAlarm }

procedure TPlcAlarm.Write(Ele: PTPlcElement; line: boolean);
var I: Integer;
begin
  I:= Ele.Embedded.Command - BaseFault;
  if (I >= Low(TAlarm)) and (I <= High(TAlarm)) then
    AlarmActive[I]:= Line;
end;

procedure TPlcAlarm.Compile(Ele: PTPlcElement; St: TPlcTokenizer; Write: boolean);
var Tmp: string;
    Val: Integer;
begin
  if St.Read <> '.' then
    raise ECompileItemFail.CreateFmt(MethodNameMissing, [name]);

  try
    Tmp:= St.Read;
    Val:= StrToInt(Tmp);
  except
    on E: Exception do
      raise ECompileItemFail.Create(E.Message);
  end;

  if (Val < BaseFault) or (Val > (BaseFault + High(TAlarm))) then
      raise ECompileItemFail.Create('Error must be in the range [' + IntToStr(BaseFault) + '..' + IntToStr(High(TAlarm)) + ']');


  Ele.Embedded.Command:= Val;
end;

function TPlcAlarm.GateName(Ele: PTPlcElement): string;
begin
  Result:= IntToStr(Ele.Embedded.Command);
end;

procedure TPlcAlarm.ItemProperties(Ele: PTPlcElement; SL: TStrings);
var I: TAlarm;
begin
  for I:= Low(TAlarm) to High(TAlarm) do begin
    SL.Add(IntToStr(BaseFault + I));
    SL.Add(BooleanIdent[AlarmActive[I]]);
  end;
end;

function TPlcAlarm.Read(Ele: PTPlcElement): boolean;
var I: Integer;
begin
  I:= Ele.Embedded.Command - BaseFault;
  if (I >= Low(TAlarm)) and (I <= High(TAlarm)) then
    Result:= AlarmActive[I]
  else
    Result:= False;
end;

procedure TPlcAlarm.GatePost;
var I: TAlarm;
begin
  for I:= Low(I) to High(I) do begin
    if AlarmActive[I] <> AlarmPrevious[I] then begin
      if AlarmActive[I] then begin
        try
          AlarmFID[I]:= Sysobj.FaultInterface.SetFault(SysObj.IO, PlcFaultRegistration[I], [], FixedHandler.ResetFault)
        except
          on E: Exception do
            SysObj.FaultInterface.SetFault(SysObj.IO, CoreFaults[cfSysFatal], ['Plc language error on Alarm.' + IntToStr(9000 + I)], nil);
        end;
      end else begin
        Sysobj.FaultInterface.FaultResetID(AlarmFID[I]);
      end;
    end;
    AlarmPrevious[I]:= AlarmActive[I];
  end;
end;

var AReg: TAlarm;

constructor TPlcAlarm.Create(St: TPlcTokenizer; aName: string);
begin
  inherited;
  St.ExpectedToken(';');
end;

{ TFixedHandler }

function TFixedHandler.ResetFault(FID: FHandle): Boolean;
begin
  Result:= False;
end;

initialization
  for AReg := Low(AReg) to High(AReg) do begin
    PlcFaultRegistration[AReg].Ndx := BaseFault + AReg;
    PlcFaultRegistration[AReg].Text := Format('$ALARM%d', [BaseFault + AReg]);
    PlcFaultRegistration[AReg].Level := FaultWarning;
    PlcFaultRegistration[AReg].Dispatch := [];
  end;
  FixedHandler:= TFixedHandler.Create;
  RegisterFaultMap(NamePlcFaultMap, PlcFaultRegistration, High(TAlarm) + 1, BaseFault);
  RegisterPlcItemClass('Alarm', TPlcAlarm);
end.
