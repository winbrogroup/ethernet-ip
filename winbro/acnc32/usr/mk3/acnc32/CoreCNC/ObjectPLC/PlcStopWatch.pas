unit PlcStopWatch;

interface

uses
  Classes, PlcItems, Plc32, CoreCNC, CNC32, CNCTypes, PlcValueItem, SysUtils;

type
  TStopWatchValue = (
    swvStart,
    swvReset
  );

  TStopWatchValues = set of TStopWatchValue;

  TPlcStopWatchItem = class(TPlcNamedItem)
  private
    SWInputs : TStopWatchValues;
    Elapsed, LastStart : Int64;
  protected
    function DontInheritValue : Boolean; override;
  public
    constructor Create(st : TPlcTokenizer; aName : string); override;
    destructor Destroy; override;
    procedure   Compile(Ele : PTPlcElement; st : TPlcTokenizer; Write : boolean); override;
    procedure   Write(Ele : PTPlcElement; line : boolean); override;
    function    Read(Ele : PTPlcElement) : boolean; override;
    function    GateName(Ele : PTPlcElement) : string; override;
    procedure   GatePost; override;
    procedure   ItemProperties(Ele : PTPlcElement; SL : TStrings); override;
  end;

implementation

const
  StopWatchNames : array [TStopWatchValue] of TValuePair = (
        ( value : 'Start'; Reado : False ),
        ( value : 'Reset'; Reado : False )
  );

resourcestring
  StopWatchMustBeWritable = 'StopWatch [%s] IO Connection must be writable';
  OnlyStartStopWritable = 'StopWatch [%s] Only Start and Reset are Writable';

constructor TPlcStopWatchItem.Create(st : TPlcTokenizer; aName : string);
begin
  inherited Create(St, aName);
  if not Tag.IsWritable then
    raise ECreateItemFail.CreateFmt(StopWatchMustBeWritable, [aName]);

{  if St.Read <> ';' then
    raise ECreateItemFail.CreateFmt(SemicolonMissing, [aName]); }
end;

destructor TPlcStopWatchItem.Destroy;
begin
  inherited;
end;

procedure   TPlcStopWatchItem.Compile(Ele : PTPlcElement; St : TPlcTokenizer; Write : boolean);
var I : TStopWatchValue;
    Token : string;
begin
  if St.Read <> '.' then
    raise ECompileItemFail.CreateFmt(MethodNameMissing, [name]);

  Token := St.SkipComment;

  for I := Low(TStopWatchValue) to High(TStopWatchValue) do begin
    if CompareText(StopWatchNames[I].Value, Token) = 0 then begin
      if StopWatchNames[I].ReadO and Write then
        raise ECompileItemFail.CreateFmt(ItemIsReadOnly, [Name]);
      Ele^.Embedded.DCommand := Ord(I);
      Exit;
    end;
  end;

  if Write then
    raise ECompileItemFail.CreateFmt(OnlyStartStopWritable, [Name]);
  CompileValue(Ele, St, Write, Token);
  Ele^.Embedded.DCommand := -1;
end;

procedure   TPlcStopWatchItem.Write(Ele : PTPlcElement; line : boolean);
begin
  if Line <> Ele^.Line then begin

    case TStopWatchValue(Ele^.Embedded.DCommand) of
      swvStart : if not Line then
                    Elapsed := Elapsed + (PlcTick.I64 - LastStart)
                 else
                    LastStart := PlcTick.I64;

      swvReset : if Line then begin
                   Elapsed := 0;
                   LastStart := PlcTick.I64;
                 end;
    end;
  end;
  
  if Line then
    Include(SWInputs, TStopWatchValue(Ele^.Embedded.DCommand))
  else
    Exclude(SWInputs, TStopWatchValue(Ele^.Embedded.DCommand));
end;

function    TPlcStopWatchItem.Read(Ele : PTPlcElement) : boolean;
begin
  if Ele^.Embedded.DCommand <> -1 then
    Result := TStopWatchValue(Ele^.Embedded.DCommand) in SWInputs
  else
    Result := Inherited Read(Ele);
end;

function    TPlcStopWatchItem.GateName(Ele : PTPlcElement) : string;
begin
  if Ele^.Embedded.DCommand <> -1 then begin
    if (TStopWatchValue(Ele^.Embedded.DCommand) >= Low(TStopWatchValue)) and
       (TStopWatchValue(Ele^.Embedded.DCommand) <= High(TStopWatchValue)) then
       Result := StopWatchNames[TStopWatchValue(Ele^.Embedded.DCommand)].Value
    else
      Result := '?';
  end else
      Result := inherited GateName(Ele);
end;

procedure   TPlcStopWatchItem.GatePost;
begin
  if swvStart in SWInputs then
    FValue := Elapsed + (PlcTick.I64 - LastStart)
  else
    FValue := Elapsed;
  inherited GatePost;
end;

procedure   TPlcStopWatchItem.ItemProperties(Ele : PTPlcElement; SL : TStrings);
var I : TStopWatchValue;
begin
  SL.Clear;
  SL.Add('Value');
  SL.Add(IntToStr(Value) + 'ms');
  for I := Low(TStopWatchValue) to High(TStopWatchValue) do begin
    SL.Add(StopWatchNames[I].Value);
    SL.Add(BooleanIdent[I in SWInputs]);
  end;
end;

function TPlcStopWatchItem.DontInheritValue : Boolean;
begin
  Result := False;
end;

initialization
  RegisterPlcItemClass('StopWatch', TPlcStopWatchItem);
end.
