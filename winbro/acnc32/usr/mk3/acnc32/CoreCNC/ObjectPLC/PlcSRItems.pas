unit PlcSRItems;

interface

uses PlcItems, Plc32, CNC32, CNCTypes, SysUtils, Graphics, Windows, Classes;

type
  TPlcSRValue  = (  srtSet,
                    srtReset,
                    srtOut,
                    srtAssign );

  TPlcSRValues = set of TPlcSRValue;

  TPlcSRItem = class(TPLCItem)
    Values : TPlcSRValues;
    LastSet : Boolean;
    LastReset : Boolean;
    constructor Create(st : TPlcTokenizer; aName : string); override;
    procedure  Compile(ele : PTPlcElement; st : TPlcTokenizer; write : boolean); override;
    function  Read(ele : PTPlcElement) : boolean; override;
    procedure Write(ele : PTPlcElement; line : boolean); override;
    function  GateName(ele : PTPlcElement) : string; override;
    procedure   ItemProperties(Ele : PTPlcElement; SL : TStrings); override;
  end;

  TPlcTimerItem = class(TPlcSRItem)
  private
    Running : Int64;
    StartTick : Int64;
    Accumulated : Integer;
  public
    constructor Create(st : TPlcTokenizer; aName : string); override;
    procedure   ItemProperties(Ele : PTPlcElement; SL : TStrings); override;
    procedure Write(ele : PTPlcElement; line : boolean); override;
    procedure GatePre; override;
  end;

  TPlcCounterItem = class(TPlcSRItem)
  private
    Current : Integer;
  public
    constructor Create(st : TPlcTokenizer; aName : string); override;
    procedure   ItemProperties(Ele : PTPlcElement; SL : TStrings); override;
    procedure Write(ele : PTPlcElement; line : boolean); override;
    procedure GatePre; override;
  end;

  TPlcSRFlipFlopItem = class(TPlcSRItem)
  public
    constructor Create(St : TPlcTokenizer; aName : string); override;
    procedure Write(Ele : PTPlcElement; Line : boolean); override;
  end;

  TPlcToggleValue = (
    plctClock,
    plctReset,
    plctOut
  );

  TPlcToggleValues = set of TPlcToggleValue;

  TPlcToggleItem = class(TPlcItem)
  private
//    State : Boolean;
    Values : TPlcToggleValues;
  public
    constructor Create(St : TPlcTokenizer; aName : string); override;
    procedure  Compile(Ele : PTPlcElement; ST : TPlcTokenizer; Write : Boolean); override;
    procedure Write(Ele : PTPlcElement; Line : boolean); override;
    function  Read(ele : PTPlcElement) : boolean; override;
    function  GateName(ele : PTPlcElement) : string; override;
    procedure   ItemProperties(Ele : PTPlcElement; SL : TStrings); override;
  end;

implementation

resourcestring
  TargetValueLang = 'Target';
  CurrentValueLang = 'Current';

type
  TSRCommands = array [TPlcSRValue] of TValuePair;

const
  SRCommands : TSRCommands = (
        ( value : 'Set';    reado : False ),
        ( value : 'Reset';  reado : False ),
        ( value : 'Out';    reado : true ),
        ( value : 'Assign'; reado : False )
  );

  ToggleCommands : array [TPlcToggleValue] of TValuePair = (
        ( value : 'Clock';  reado : False ),
        ( value : 'Reset';  reado : False ),
        ( value : 'Out';    reado : True )
  );


constructor TPlcSRItem.Create(st : TPlcTokenizer; aName : string);
begin
  inherited;
end;

procedure TPlcSRItem.Compile(ele : PTPlcElement; st : TPlcTokenizer; write : boolean);
var token : string;
    I  : TPlcSRValue;
begin
  token := st.skipComment;
  if token <> '.' then
    raise ECompileItemFail.CreateFmt(MethodNameMissing, [name]);

  token := st.skipComment;

  for I := Low(TPlcSRValue) to High(TPlcSRValue) do begin
    if CompareText(token, SRCommands[i].Value) = 0 then begin
      Ele.Embedded.Command := Ord(I);
      if I = srtAssign then begin
        if St.Read <> '.' then
          raise ECompileItemFail.CreateFmt(MethodNameMissing, [name]);
        Token := St.Read;
        Ele.Embedded.OData := FindOrAddItem(Token);
      end;
      Exit;
    end;
  end;
  raise ECompileItemFail.CreateFmt(InvalidMethodName, [token]);
end;

function TPlcSRItem.Read(ele : PTPlcElement) : boolean;
begin
  Result := TPlcSRValue(Ele.Embedded.Command) in Values;
end;

procedure   TPlcSRItem.ItemProperties(Ele : PTPlcElement; SL : TStrings);
var I : TPlcSRValue;
begin
  for I := Low(TPlcSRValue) to High(TPlcSRValue) do begin
    SL.Add(SRCommands[I].Value);
    SL.Add(BooleanIdent[I in Values]);
  end;
end;

procedure TPlcSRItem.Write(ele : PTPlcElement; line : boolean);
begin
  if Line then Include(Values, TPlcSRValue(Ele.Embedded.Command))
          else Exclude(Values, TPlcSRValue(Ele.Embedded.Command));
end;

function TPlcSRItem.GateName(ele : PTPlcElement) : string;
begin
  if (Ele.Embedded.Command < Ord(Low(TPlcSRValue))) or
     (Ele.Embedded.Command > Ord(High(TPlcSRValue))) then begin
    result := '.Err';
  end else
    result := SRCommands[TPlcSRValue(Ele.Embedded.Command)].value;
end;


constructor TPlcTimerItem.Create(st : TPlcTokenizer; aName : string);
begin
  inherited Create(st, aName);
  FValue := Round(St.ReadDouble * 1000);
  if St.Read <> ';' then
    raise ECreateItemFail.createFmt(SemicolonMissing, [name]);
end;

procedure   TPlcTimerItem.ItemProperties(Ele : PTPlcElement; SL : TStrings);
begin
  SL.Add(TargetValueLang);
  SL.Add(IntToStr(FValue));
  SL.Add(CurrentValueLang);
  SL.Add(IntToStr(Accumulated + Running));
  inherited;
end;

procedure TPlcTimerItem.GatePre;
begin
  if SrtReset in Values then begin // Force off
    Accumulated := 0;
    Exclude(Values, SrtOut);
    LastSet := False;
  end else begin                   // Set not Reset
    if SrtSet in Values then begin
      if not LastSet then begin    // On rising edge
        StartTick := CNC32.PlcTick.I64;
        Running := 0;
      end else if not (SrtOut in Values) then begin
        if Running + Accumulated > FValue then begin // Success condition
          Include(Values, SrtOut);
          Accumulated := FValue;
          Running := 0;
        end else begin  // Still counting
          Running := PlcTick.I64 - StartTick;
        end;
      end;
      LastSet := True;
    end else begin
      if not LastSet then begin // On falling edge
        Inc(Accumulated, Running);
        Running := 0;
      end;
      LastSet := False;
    end;
  end;
end;

procedure TPlcTimerItem.Write(ele : PTPlcElement; line : boolean);
begin
  if TPlcSRValue(Ele.Embedded.Command) = srtAssign then begin
    if Line then
      FValue := Ele.Embedded.OData.Value;
  end else
    inherited;
end;

constructor TPlcCounterItem.Create(st : TPlcTokenizer; aName : string);
begin
  inherited Create(st, aName);
  FValue := St.ReadInteger;
  if St.Read <> ';' then
    raise ECreateItemFail.createFmt(SemicolonMissing, [name]);
end;

procedure TPlcCounterItem.ItemProperties(Ele : PTPlcElement; SL : TStrings);
begin
  SL.Add(TargetValueLang);
  SL.Add(IntToStr(FValue));
  SL.Add(CurrentValueLang);
  SL.Add(IntToStr(Current));
  inherited;
end;

procedure TPlcCounterItem.GatePre;
begin
  if (srtReset in Values) then begin
    current := 0;
    Exclude(Values, srtOut);
    Exit;
  end;

  if (srtSet in Values) and not lastset then begin
    Inc(Current);
    if Current >= FValue then Include(Values, srtOut);
  end;
  lastset := srtSet in Values;
end;

var JJ : Integer;

procedure TPlcCounterItem.Write(ele : PTPlcElement; line : boolean);
begin
  if TPlcSRValue(Ele.Embedded.Command) = srtAssign then begin
    if line then
      FValue := Ele.Embedded.OData.Value;
  end else
    if Line then
      Include(Values, TPlcSRValue(Ele.Embedded.Command))
    else
      Exclude(Values, TPlcSRValue(Ele.Embedded.Command));


  if Line then begin
    case TPlcSRValue(Ele.Embedded.Command) of
      srtSet : Inc(JJ);
      srtReset : Dec(JJ);
      srtOut : Inc(JJ);
    end;
  end;
end;

constructor TPlcSrFlipFlopItem.Create(st : TPlcTokenizer; aName : string);
begin
  inherited Create(st, aName);
  if St.Read = ';' then Exit;
  St.Push;
  if St.ReadInteger <> 0 then
    Include(Values, srtOut);
  if St.Read <> ';' then
    raise ECreateItemFail.CreateFmt(SemicolonMissing, [name]);
end;

procedure TPlcSrFlipFlopItem.Write(ele : PTPlcElement; line : boolean);
begin
  if Line then begin
    case TPlcSRValue(Ele.Embedded.Command) of
      srtSet   : if not (srtReset in Values) then Include(Values, srtOut);
      srtReset : Exclude(Values, srtOut);
    end;
    Include(Values, TPlcSRValue(Ele.Embedded.Command));
  end else begin
    Exclude(Values, TPlcSRValue(Ele.Embedded.Command));
  end;
end;

constructor TPlcToggleItem.Create(St : TPlcTokenizer; aName : string);
var Token : string;
begin
  inherited Create(St, aName);
  Token := St.Read;
  if Token <> ';' then begin
    try
      StrToInt(Token);
      Include(Values, plctOut);
    except
    end;
    if St.Read <> ';' then
      raise ECreateItemFail.CreateFmt(SemicolonMissing, [name]);
  end;
end;

procedure TPlcToggleItem.Compile(Ele : PTPlcElement; ST : TPlcTokenizer; Write : Boolean);
var Token : string;
    I : TPlcToggleValue;
begin
  if St.Read <> '.' then
    raise ECompileItemFail.CreateFmt(MethodNameMissing, [Name]);

  Token := St.Read;

  for I := Low(TPlcToggleValue) to High(TPlcToggleValue) do begin
    if CompareText(Token, ToggleCommands[I].Value) = 0 then begin
      if ToggleCommands[I].ReadO and Write then
        raise ECompileItemFail.CreateFmt(ItemIsReadOnly, [Name]);

      Ele^.Embedded.Command := Ord(I);
      Exit;
    end;
  end;
end;

procedure TPlcToggleItem.Write(Ele : PTPlcElement; Line : boolean);
begin
  if Line then begin
    Include(Values, TPlcToggleValue(Ele^.Embedded.Command));
    if not Ele^.Line and
       not(plctReset in Values) and
       (TPlcToggleValue(Ele^.Embedded.Command) = plctClock) then begin
          if not (plctOut in Values) then
             Include(Values, plctOut)
          else
             Exclude(Values, plctOut);
    end;
    if TPlcToggleValue(Ele^.Embedded.Command) = plctReset then
       Exclude(Values, plctOut);
  end else begin
    Exclude(Values, TPlcToggleValue(Ele^.Embedded.Command));
  end;
end;


function  TPlcToggleItem.Read(ele : PTPlcElement) : boolean;
begin
  Result := TPlcToggleValue(Ele^.Embedded.Command) in Values;
end;

function  TPlcToggleItem.GateName(ele : PTPlcElement) : string;
begin
  if (Ele^.Embedded.Command >= Ord(Low(TPlcToggleValue))) and
     (Ele^.Embedded.Command <= Ord(High(TPlcToggleValue))) then
    Result := ToggleCommands[TPlcToggleValue(Ele^.Embedded.Command)].Value
  else
    Result := 'Unknown';
end;

procedure   TPlcToggleItem.ItemProperties(Ele : PTPlcElement; SL : TStrings);
var I : TPlcToggleValue;
begin
  for I := Low(TPlcToggleValue) to High(TPlcToggleValue) do begin
    SL.Add(ToggleCommands[I].Value);
    SL.Add(BooleanIdent[I in Values]);
  end;
end;


initialization
  RegisterPlcItemClass('Toggle', TPlcToggleItem);
  RegisterPlcItemClass('Timer', TPlcTimerItem);
  RegisterPlcItemClass('Counter', TPlcCounterItem);
  RegisterPlcItemClass('SR', TPlcSRFlipFlopItem);
end.
