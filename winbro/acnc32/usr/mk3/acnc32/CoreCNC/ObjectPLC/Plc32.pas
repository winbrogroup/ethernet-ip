unit Plc32;

interface

uses CoreCNC, CNC32, CNCtypes, SysUtils, FaultRegistration;

resourceString
  DataItemNotFound = '[%s] Data Item Not Found';
  SemicolonMissing = '[%s] Semicolon Missing';
  EDMHeadRequiresParameter = '%s EDMHead requires parameter [%s]';
  PmacMemoryRequiresParameter = '%s PmacMemory requires parameter [%s]';  
  NotFoundInSymbolLibrary = '%s Not found in symbol library';
  ItemIsReadOnly = '[%s] Item is read only';
  ObjectIsReadOnly = '[%s] Object is read only';

  MethodNameMissing = 'Method name missing on [%s]';
  DataConversionError = 'Data converion error on [%s]';
  InvalidMethodName = 'Invalid method name [%s]';

  FileNotFound = 'File Not Found [%s]';

  NotExistOrNoValue = '[%s] Does not exist or has no value';
  InvalidHeaderFormat = 'Invalid header format';
  InvalidDataType = '[%s] Invalid data type';
  InvalidOpCode = '[%s] Invalid Op Code';
  ContactSupplier = 'Software Failure [%s] Invalid Opcode';
  ItemNotLocalOrPublic = 'Item not declared public or local [%s]';
  ItemHasNoValue = '[%s] Item has no value';
  IOAndNameAreNotSet = 'IO and named are not set';
  MaximumDataObjects = 'Too many data items defined in [%s]';
  MissingLocalSection = 'Missing Local Section';
  NumericExpected = 'Numeric expected';
  EDMHeadInvalidOptionType = '[%s] Bloody Electrical Design [%s]';
  InvalidNumberOfHeads = '[%s][%s] Flow Heads [1..4]';
  InvalidBaseAddress   = '[%s] Invalid Base Address [%s]';
  ItemMustBeFlowStation = '[%s] Item Must be FlowStation [%s]';
  IntValueOutOfRange = '[%d] Value out of range in [%s]';
  DeviceMustBe = '[%s] Must be a IODevice of Class [%s]';
  NameNotInIOSystem = '[%s] Does not exist in IO System';
  TooManyCloseParentheses = 'Too many close parenthesis '')'' ';
  GlobalFileCorrupt = 'PlcGlobal.H : File Corrupt';
  ItemHasNoProperties = '[%s] Has no intrinsic properties';
  OrCannotFollowOut = '"OR" cannot directly follow "OUT"';
  ParenthasisSeperationError = 'Parenthasis can only be seperated by OR or AND';
  TooFewCloseParenthasis = 'Too few close parenthesis';
//  ItemNotPermittedAsGlobal =  '[%s] Global Item Cannot be of Type [%s]';

const
  MAX_ELEMENTS          = 1000;
  MAX_NBR_OBJECT_PLC    = 64;

  PlcPath     = 'Plc\';

type
  TPLCOpCode = ( PLCIbad,
                 PLCIif,
                 PLCIand,
                 PLCIor,
                 PLCIStrAnd,
                 PLCIStrOr,
                 PLCIandPop,
                 PLCIorPop,
                 PLCIout,
                 PLCIswitch,
                 PLCIOldLoad,
                 PLCIifN,
                 PLCIandN,
                 PLCIorN,
                 PLCIoutN,
                 PLCIOldLoadN,
                 PLCIEnd,
                 PLCIError  );



  TPlcFault = (
    plcfForceJoking,
    plcfEditingRequiresMaint,
    plcfItemInterlock,
    plcfItemError,
    plcfInternalError,
    plcfFatalFault
  );

  TValuePair = record
    value : PChar;
    reado : boolean;
  end;

const
  MaximumPlcItems = 200;


  PlcFaults : array [TPlcFault] of TFaultRegistration = (
    ( Ndx : 2000; Text : 'May the force be with you'; Level : FaultInterlock; Dispatch : [] ),
    ( Ndx : 2001; Text : 'Plc Editing requires maintenance access'; Level : FaultInterlock; Dispatch : [] ),
    ( Ndx : 2002; Text : '$PLC_INHIBIT_S_S_S'; Level : FaultInterlock; Dispatch : [] ),
    ( Ndx : 2003; Text : '$PLC_INHIBIT_S_S_S'; Level : FaultRewind; Dispatch : [] ),
    ( Ndx : 2004; Text : '$PLC_INTERNAL_ERROR_S_S'; Level : FaultRewind; Dispatch : [] ),
    ( Ndx : 2005; Text : '$PLC_FATAL_FAULT_S'; Level : FaultFatal; Dispatch : [] )
  );

  NPLCOpcode: array[TPLCOpcode] of PChar =
      ('# #',
       'if',
       'and',
       'or',
       'and push',
       'or push',
       ')',
       ')',
       'out',
       'switch',
       'ld',
       'nif',
       'nand',
       'nor',
       'nout',
       'nld',
       'End',
       '# #');

  NegatingPlcOpCodes = [
                 PLCIifN,
                 PLCIandN,
                 PLCIorN,
                 PLCIOldLoadN];

  //PLCIifN..PLCIError];
  OutputPlcOpCodes =   [PLCIout, PLCIoutN];


var
  InstalledPlc : TAbstractPlc;

implementation

initialization
  FaultRegistration.RegisterFaultMap('ObjectPlcFaults', PlcFaults, Length(PlcFaults), 2000);
end.
