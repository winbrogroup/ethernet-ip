unit PlcStack;

interface

const
  MAX_PLC_STACK = 100;

type
  TPlcStack = class(TObject)
  private
    stack    : array [0..MAX_PLC_STACK] of boolean;
//    instance : TDateTime;
  public
    top      : Integer;
    procedure push(line : boolean);
    function  pop : boolean;
    procedure reset;
  end;


implementation

procedure TPlcStack.push(line : boolean);
begin
  stack[Self.top] := line;
  inc(Self.top);
//  if Top > MAX_PLC_STACK then // Add this check for compiler if necessary.
//    Top := Top mod MAX_PLC_STACK;
end;

function TPlcStack.pop : boolean;
begin
  dec(Self.top);
//  if Top < 0 then
//    Top := 0;
  result := stack[top];
end;

procedure TPlcStack.Reset;
begin
  Self.top := 0;
end;

end.
 