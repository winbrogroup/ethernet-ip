unit PlcMono;

interface

uses Classes, SysUtils, PlcItems, Plc32, Cnc32, CncTypes;

type
  TPlcMonoItem = class(TPlcItem)
  private
    StartTime : Int64;
    Hold : Int64;
    Output : Boolean;
  public
    constructor Create(st : TPlcTokenizer; aName : string); override;
    procedure Compile(ele : PTPlcElement; st : TPlcTokenizer; write : boolean); override;
    function  Read(ele : PTPlcElement) : boolean; override;
    procedure Write(ele : PTPlcElement; line : boolean); override;
    function  GateName(ele : PTPlcElement) : string; override;
    procedure GatePost; override;
    procedure ItemProperties(Ele : PTPlcElement; S : TStrings); override;
  end;


implementation

{ TPlcMonoItem }

procedure TPlcMonoItem.Compile(ele: PTPlcElement; st: TPlcTokenizer;
  write: boolean);
begin
// Does nothing, there are no properties
end;

constructor TPlcMonoItem.Create(st: TPlcTokenizer; aName: string);
begin
  inherited Create(st, aName);
  Hold := Round(St.ReadDouble * 1000);
  if St.Read <> ';' then
    raise ECreateItemFail.createFmt(SemicolonMissing, [name]);
end;

function TPlcMonoItem.GateName(ele: PTPlcElement): string;
begin
  Result := '';
end;

procedure TPlcMonoItem.GatePost;
begin
  Output := StartTime + Hold > CNC32.PlcTick.I64;
end;

procedure TPlcMonoItem.ItemProperties(Ele: PTPlcElement; S: TStrings);
var Tmp : Int64;
begin
  S.Add('Output');
  S.Add(BooleanIdent[Output]);
  S.Add('Current');
  Tmp := StartTime + Hold - CNC32.PlcTick.I64;
  if Tmp >= 0 then
    S.Add(FloatToStr(Tmp))
  else
    S.Add('Timed out');
  S.Add('Time constant');
  S.Add(FloatToStr(Hold));
end;

function TPlcMonoItem.Read(ele: PTPlcElement): boolean;
begin
  Result := Output;
end;

procedure TPlcMonoItem.Write(ele: PTPlcElement; line: boolean);
begin
  if Line then
    StartTime := CNC32.PlcTick.I64;
end;

initialization
  RegisterPlcItemClass('Mono', TPlcMonoItem);
end.
