unit PlcPlugin;

interface

uses Plc32, CNCTypes, CNC32, SysUtils, Graphics, Windows, CoreCNC, Tokenizer,
     Classes, PlcValueItem, PlcItems, IPlc;

type
  HANDLE = Integer;

  TPlcPluginReadEv = function(LibH : HANDLE) : PChar; stdcall;

  TPlcPluginResolveItemValueEv = function(
                           LibH : Handle;
                           aItem : Pointer
  ) : Integer; stdcall;


  TPlcPluginResolveItemEv = function (
                        LibH : HANDLE;
                        aName : PChar;
                        var IsConstant : Boolean;
                        var ResolveItemValueEv : TPlcPluginResolveItemValueEv
  ) : Pointer; stdcall;

  TPlcPluginResolveDeviceEv = function (
                         LibH : HANDLE;
                         aName : PChar;
                         var ISize, OSize : Integer;
                         var IBuf : Pointer;
                         var OBuf : Pointer
  ) : Boolean; stdcall;

  TPlcPluginObjects = procedure (Buffer : PChar; MaxSize : Cardinal); stdcall;

  TPlcPluginProperties = procedure ( ObjName : PChar;
                             Buffer : PChar;
                             MaxSize : Cardinal); stdcall;

  TPlcPluginCreate = function( ObjName, Parameters : PChar;
                                ResolveDeviceEv : TPlcPluginResolveDeviceEv;
                                ResolveItemEv : TPlcPluginResolveItemEv) : HANDLE; stdcall;

  TPlcPluginCompile = procedure(
                            LibH : HANDLE;
                            var Embedded : TEmbeddedData;
                            aReadEv : TPlcPluginReadEv;
                            ResolveItemEv : TPlcPluginResolveItemEv;
                            Write : Boolean
                          ); stdcall;

  TPlcPluginProc = procedure(LibH : HANDLE); stdcall;

  TPlcPluginRead = function( LibH : HANDLE;
                        var Embedded : TEmbeddedData) : Boolean; stdcall;

  TPlcPluginWrite = procedure( LibH : HANDLE;
                          var Embedded : TEmbeddedData;
                          Line, LastLine : Boolean); stdcall;

  TPlcPluginValue = function (LibH : HANDLE) : Integer; stdcall;

  TPlcPluginItemProperties = procedure ( LibH : HANDLE;
                                        Buffer : PChar;
                                        MaxSize : Integer); stdcall;

  TPlcPluginGatePost = function (LibH : HANDLE) : Integer; stdcall;


  TPlcPluginGateName = procedure (LibH : HANDLE; var Embedded : TEmbeddedData; GateName : PChar); stdcall;
  TPlcPluginSetHost = procedure (LibH: Handle; Host: IPlcHost); stdcall;

//  TPlcPluginInstall = procedure(ObjName : PChar); stdcall;

  TPlcPluginWrapper = class(TObject)
  public
    PlcPluginObjects : TPlcPluginObjects;
    PlcPluginProperties : TPlcPluginProperties;
    PlcPluginCreate : TPlcPluginCreate;
    PlcPluginCompile : TPlcPluginCompile;
    PlcPluginGatePre : TPlcPluginProc;
    PlcPluginGatePost : TPlcPluginGatePost;
    PlcPluginRead : TPlcPluginRead;
    PlcPluginWrite : TPlcPluginWrite;
    PlcPluginItemProperties : TPlcPluginItemProperties;
    PlcPluginGateName : TPlcPluginGateName;
    PlcPluginDestroy : TPlcPluginProc;
    PlcPluginSetHost: TPlcPluginSetHost;

    List : TStringList;
    LibCode : HModule;
    constructor Create(aLibCode : HModule);
  end;

  TPlcPlugin = class(TPlcValueItem)
  private
    Wrapper : TPlcPluginWrapper;
    ObjName : string;
    HPlugin : HANDLE;
  protected
    function DontInheritValue : Boolean; override;
  public
    constructor BindCreate(aObjName : string; St : TPlcTokenizer; aName : string);
    constructor Create(St : TPlcTokenizer; aName : string); override;
    destructor Destroy; override;
    procedure   Compile(Ele : PTPlcElement; St : TPlcTokenizer; Write : boolean); override;
    procedure   Write(Ele : PTPlcElement; line : boolean); override;
    function    Read(Ele : PTPlcElement) : boolean; override;
    function    GateName(Ele : PTPlcElement) : string; override;
    procedure   GatePost; override;
    procedure   GatePre; override;
    procedure   ItemProperties(Ele : PTPlcElement; SL : TStrings); override;
    procedure SetHost(Host: IPlcHost);
    class procedure BindPlugin(HMod : HMODULE);
    class procedure InstallItem; override;
    class procedure FinalInstallItem; override;
  end;


implementation

var
  PluginList : TStringList;
  ActiveStream : TPlcTokenizer;
  ActiveItem : TPlcPlugin;

constructor TPlcPlugin.BindCreate(aObjName : string; St : TPlcTokenizer; aName : string);
var I : Integer;
begin
  ObjName := aObjName;
  for I := 0 to PluginList.Count - 1 do begin
    if CompareText(PluginList[I], ObjName) = 0 then begin
      Wrapper := TPlcPluginWrapper(PluginList.Objects[I]);
      Create(St, aName);
      Exit;
    end;
  end;
end;

function ResolveDeviceEv(LibH : HANDLE;
                         aName : PChar;
                         var ISize, OSize : Integer;
                         var IBuf : Pointer;
                         var OBuf : Pointer ) : Boolean; stdcall;
var IODevice : TAbstractIODevice;
begin
  IODevice := SysObj.Comms.FindDevice(aName);
  if Assigned(IODevice) then begin
    ISize := IODevice.InputSize;
    OSize := IODevice.OutputSize;
    IBuf := IODevice.ShadowIn;
    OBuf := IODevice.ShadowOut;
    Result := True;
  end else begin
    Result := False;
  end;
end;

function ResolveItemValueEv(LibH : HANDLE; Item : Pointer) : Integer; stdcall;
begin
  Result := TPlcValueItem(Item).Value;
end;

function ResolveItemEv( LibH : HANDLE;
                        aName : PChar;
                        var IsConstant : Boolean;
                        var aResolveItemValueEv : TPlcPluginResolveItemValueEv) : Pointer; stdcall;
begin
  Result := ActiveItem.FindOrAddItem(aName);
  IsConstant := Assigned(Result) and not(TPlcItem(Result) is TPlcNumericItem);
  aResolveItemValueEv := ResolveItemValueEv;
end;

function ReadEv(LibH : HANDLE) : PChar; stdcall;
begin
  Result := PChar(ActiveStream.Read);
end;

constructor TPlcPlugin.Create(St : TPlcTokenizer; aName : string);
var Token, Parms : string;
begin
  inherited Create(nil, aName);
  try
    St.FloatSeperator;
    Self.FPlcThread := St;
    Token := St.SkipComment;
    Parms := '';
    while Token <> ';' do begin
      Parms := Parms + Token + ';';
      Token := St.SkipComment
    end;
    ActiveStream := St;
    ActiveItem := Self;
    try
      HPlugin := Wrapper.PlcPluginCreate(PChar(ObjName), PChar(Parms), ResolveDeviceEv, ResolveItemEv);
      SetHost(Sysobj.IO.PLC);
      //Wrapper.PlcPluginSetHost(HPlugin, SysObj.IO.PLC );
    except
      on E : Exception do
        CNC32.DoCopyOnWriteException(E);
    end;
  finally
    St.StdSeperator;
  end;
end;

destructor TPlcPlugin.Destroy;
begin
  Wrapper.PlcPluginDestroy(HPlugin);
  inherited;
end;

procedure TPlcPlugin.Compile(Ele : PTPlcElement; St : TPlcTokenizer; Write : boolean);
begin
  ActiveStream := St;
  try
    Wrapper.PlcPluginCompile(HPlugin, Ele^.Embedded, ReadEv, ResolveItemEv, Write);
  except
    on E : Exception do
      CNC32.DoCopyOnWriteException(E);
  end;
end;

procedure TPlcPlugin.Write(Ele : PTPlcElement; Line : Boolean);
begin
  Wrapper.PlcPluginWrite(HPlugin, Ele^.Embedded, Line, Ele^.Line);
end;

function TPlcPlugin.Read(Ele : PTPlcElement) : Boolean;
begin
  Result := Wrapper.PlcPluginRead(HPlugin, Ele^.Embedded);
end;

function TPlcPlugin.GateName(Ele : PTPlcElement) : string;
var Tmp : array [0..127] of Char;
begin
  Wrapper.PlcPluginGateName(HPlugin, Ele^.Embedded, @Tmp);
  Result := Tmp;
end;

procedure TPlcPlugin.GatePre;
begin
  Wrapper.PlcPluginGatePre(HPlugin);
end;

procedure TPlcPlugin.GatePost;
begin
  FValue := Wrapper.PlcPluginGatePost(HPlugin);
end;

procedure TPlcPlugin.ItemProperties(Ele : PTPlcElement; SL : TStrings);
var Tmp : array [0..4095] of Char;
begin
  Wrapper.PlcPluginItemProperties(HPlugin, @Tmp, 4096);
  SL.Text := Tmp;
end;

function TPlcPlugin.DontInheritValue : Boolean;
begin
  Result := True;
end;

class procedure TPlcPlugin.InstallItem;
begin
{  if Assigned(Wrapper.PlcPluginInstallItem) then
    Wrapper.PlcPluginInstallItem(PChar(ObjName)); }
end;

class procedure TPlcPlugin.FinalInstallItem;
begin
{  if Assigned(Wrapper.PlcPluginFinalInstallItem) then
    Wrapper.PlcPluginFinalInstallItem(PChar(ObjName)); }
end;

class procedure TPlcPlugin.BindPlugin(HMod : HMODULE);
var Wrapper : TPlcPluginWrapper;
    I : Integer;
begin
{  LibCode := LoadLibrary(PChar(LibName));
  if Libcode = 0 then
    raise ECNCInstallFault.CreateFmt('Unable to load DLL [%s]', [LibName]); }

//  SysObj.Comms.AttachPlugin(HMod);
  Wrapper := TPlcPluginWrapper.Create(HMod);
  if Wrapper.List.Count > 0 then begin
    for I := 0 to Wrapper.List.Count - 1 do begin
      RegisterPlcItemClass(Wrapper.List[I], TPlcPlugin);
      PluginList.AddObject(Wrapper.List[I], Wrapper);
    end;
  end else begin
    Wrapper.Free;
  end;
end;

constructor TPlcPluginWrapper.Create(aLibCode : HModule);
var
    Buffer : array [0..255] of Char;
begin
  LibCode := aLibCode;
  List := TStringList.Create;
  PlcPluginObjects := GetProcAddress(LibCode, 'PlcPluginObjects');
  if Assigned(PlcPluginObjects) then begin
    PlcPluginObjects(@Buffer, 256);
    List.Text := Buffer;

    if List.Count > 0 then begin
      PlcPluginProperties := GetProcAddressX(LibCode, 'PlcPluginProperties');
      PlcPluginCreate := GetProcAddressX(LibCode, 'PlcPluginCreate');
      PlcPluginCompile := GetProcAddressX(LibCode, 'PlcPluginCompile');
      PlcPluginGatePre := GetProcAddressX(LibCode, 'PlcPluginGatePre');
      PlcPluginGatePost := GetProcAddressX(LibCode, 'PlcPluginGatePost');
      PlcPluginRead := GetProcAddressX(LibCode, 'PlcPluginRead');
      PlcPluginWrite := GetProcAddressX(LibCode, 'PlcPluginWrite');
      PlcPluginItemProperties := GetProcAddressX(LibCode, 'PlcPluginItemProperties');
      PlcPluginGateName := GetProcAddressX(LibCode, 'PlcPluginGateName');
      PlcPluginDestroy := GetProcAddressX(LibCode, 'PlcPluginDestroy');
      PlcPluginSetHost:= GetProcAddress(LibCode, 'PlcPluginSetHost');
    end;
  end;
end;

procedure TPlcPlugin.SetHost(Host: IPlcHost);
begin
  if Assigned(Wrapper.PlcPluginSetHost) then
    Wrapper.PlcPluginSetHost(HPlugin, Host);
end;

initialization
  PluginList := TStringList.Create;
end.
