unit ViewPlcItem;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  PlcItems, Plc32, StdCtrls, Buttons, checklst, ExtCtrls, Grids, ObjectPlc;

type
  TPlcViewItemForm = class(TForm)
    ItemViewPanel: TPanel;
    TitlePanel: TPanel;
    PropGrid: TStringGrid;
    ForcePanel: TPanel;
    ForceActiveCheckBox: TCheckBox;
    ForceTypeRadioGroup: TRadioGroup;
    BitBtn1: TBitBtn;
    procedure BitBtn1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ForceActiveCheckBoxClick(Sender: TObject);
    procedure UpdateForce;
    procedure ForcePanelResize(Sender: TObject);
    procedure ForceTypeRadioGroupClick(Sender: TObject);
  private
    FElement : PTPlcElement;
    SL : TStringList;
    procedure SetElement(aElement : PTPlcElement);
  public
    PLC : TObjectPlc;
    PLCN : Integer;
    procedure UpdateItems;
    property Element : PTPlcElement read FElement write SetElement;
  end;

var
  PlcViewItemForm: TPlcViewItemForm;

implementation

{$R *.DFM}

procedure TPlcViewItemForm.SetElement(aElement : PTPlcElement);
var I : Integer;
begin
  FElement := aElement;
  if aElement <> nil then begin
    if Assigned(Element.Item) then begin
      TitlePanel.Caption := Element.Item.Name + '.' + Element.Item.GateName(Element);
      Sl.Clear;
      Element.Item.ItemProperties(Element, SL);
      PropGrid.RowCount := Sl.Count div 2;
      for I := 0 to SL.Count - 1 do begin
        if not Odd(I) then
          PropGrid.Cells[0, I div 2] := SL[I]
        else
          PropGrid.Cells[1, I div 2] := SL[I];
      end;
    end;
    UpdateForce;
  end;
end;

procedure TPlcViewItemForm.UpdateForce;
begin
  if Element.Item is TPlcForceItem then begin
    ForceActiveCheckBox.Checked := True;
    ForceTypeRadioGroup.Enabled := True;
    ForceTypeRadioGroup.ItemIndex := Ord(TPlcForceItem(Element.Item).Condition);
   end else begin
    ForceActiveCheckBox.Checked := False;
    ForceTypeRadioGroup.Enabled := False;
   end;
end;

procedure TPlcViewItemForm.UpdateItems;
var I : Integer;
begin
  SL.Clear;
  if Element <> nil then begin
    Element.Item.ItemProperties(Element, SL);
    PropGrid.RowCount := Sl.Count div 2;
    for I := 0 to SL.Count - 1 do begin
      if Odd(I) and (PropGrid.Cells[1, I div 2] <> SL[I]) then
        PropGrid.Cells[1, I div 2] := SL[I];
    end;
  end;
end;

procedure TPlcViewItemForm.BitBtn1Click(Sender: TObject);
begin
  ItemViewPanel.Visible := False;
  Element := nil;
end;

procedure TPlcViewItemForm.FormCreate(Sender: TObject);
begin
  SL := TStringList.Create;
end;

procedure TPlcViewItemForm.ForceActiveCheckBoxClick(Sender: TObject);
var
  Force : TPlcForceItem;
begin
  if ForceActiveCheckBox.Checked = True then begin
    Force := Plc.SetForce(PlcN, Element);
    if Force <> nil then
      ForceTypeRadioGroup.Enabled := True
    else
      ForceActiveCheckBox.Checked := False;
  end else begin
    Plc.RemoveForce(PlcN, Element);
    ForceTypeRadioGroup.Enabled := False;
  end;
end;

procedure TPlcViewItemForm.ForcePanelResize(Sender: TObject);
begin
  Self.BitBtn1.Top := ForcePanel.Height - (BitBtn1.Height + 2)
end;

procedure TPlcViewItemForm.ForceTypeRadioGroupClick(Sender: TObject);
begin
  with Element.Item as TPlcForceItem do begin
    Condition := TPlcForceCondition(ForceTypeRadioGroup.ItemIndex);
  end;
end;

end.
