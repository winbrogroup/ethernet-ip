unit ObjectPLC;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  CNC32, CNCTypes, CoreCNC, PlcStack, PlcItems, Plc32, IniFiles, Syncobjs,
  PlcTextItem, PlcValueItem, LogicAnalyser, Tokenizer, PlcPlugin, Plugin,
  IFSDServer;

const
  GRAPH_X_WIDTH = 22;

type
  TObjectPlcThread = class;
  TPlcPaintStack = class;
  TObjectPlcGlobalThread = class;

  TObjectPLC = class(TAbstractPLC)
  private
    FPause       : boolean;
    FFaultString : string;
    FFaultPos    : TPoint;
    FFaultPlc    : Integer;
    Thread       : array [0..MAX_NBR_OBJECT_PLC] of TObjectPlcThread;
    PlcControl   : TPlcPlcItem; // Globals and Data Interchange between PLC's
    Ftemplate    : TStringList;
    Lock         : TCriticalSection;
    GlobalThread : TObjectPlcGlobalThread;
    CompileThread : TObjectPlcThread;
    FRightMargin : Integer;
    LogicAnalyser : TLogicAnalyser;
    LibNames : TStringList;
    SweepCount : Integer;
    TransientDisplay : Boolean;

//    procedure UpdateTick;
    procedure LiveConfigure(S : TStringTokenizer; InstallComponent : Boolean);
    procedure SetPause(aVal : boolean);
    procedure SetFaultString(aFault : string; St : TPlcTokenizer);
    function GetRungCount(Index : Integer) : Integer;
    //function GetLineCount(Index : Integer) : Integer; ???? FixME
    function  ReadResident(ind : integer) : boolean;
    function  ReadActive(ind : integer) : boolean;
    function  ReadDataCount(ind : integer) : integer;
    function  GetForceCount(Index : Integer) : Integer;
    procedure DoLogicAnalyser(aTag : TAbstractTag);
    function  GetThreadName(Index : Integer) : string;
  protected
    procedure WndProc(var Message : TMessage); override;
    function InterpretToken(const aToken : string) : string; override;
  public
    constructor Create(Aowner : TComponent); override;
    destructor Destroy; override;
    procedure InstallComponent(Params : TParamStrings); override;
    procedure Installed; override;
    procedure IShutdown; override;
    procedure Execute; override;
    procedure Stop; override;
    function HTTPGeneratePage(HFile : THANDLE; var CGIPath: WideString; CGIRequest : ICGIStringList) : Boolean; override;
    procedure Clean;
    procedure Initialize;
    function  LoadFromFile(plcn : integer; filename : string; Inc : Boolean) : boolean;
    function  Template(p : integer) : TStringList;
    procedure ClientPaint(plc : Integer; var Rung : Integer; Canvas : TCanvas; IWidth, IHeight : Integer);
    function GetRungFromLine(aPlc, aLine : Integer) : Integer;
    function GetLineFromRung(aPlc, aRung : Integer) : Integer;
    function FindItem(aPlc, X, Y, aRung, IWidth, IHeight : Integer) : PTPlcElement;
    function SetForce(Index : Integer; Element : PTPlcElement) : TPlcForceItem;
    procedure RemoveForce(Index : Integer; Element : PTPlcElement);
    procedure DeleteAllForces;
    procedure SetRightMargin(aRightMargin, IWidth : Integer);
    procedure GenerateCrossReference(FileName : string);
    property  FaultString : string read FfaultString;
    property  FaultPos : TPoint read FFaultPos;
    property  FaultPlc : Integer read FFaultPlc;
    property  Resident [Index : integer] : Boolean read ReadResident;
    property  Active   [Index : integer] : Boolean read ReadActive;
    property  DataCount[Index : integer] : Integer read ReadDataCount;
    property  ForceCount[Index : integer] : Integer read GetForceCount;
    property  RungCount[Index : Integer] : Integer read GetRungCount;
    //property  LineCount[Index : Integer] : Integer read GetLineCount;  ????FixME
    property  Pause : boolean read FPause write SetPause;
    property  Names[Index : Integer] : string read GetThreadName;
    function GetItemValue(Item: Pointer): Integer; override; stdcall;
    procedure SetItemValue(Item: Pointer; Value: Integer); override; stdcall;
  end;

  TObjectPLCThread = class(TPlcTokenizer)
  private
    Loaded     : Boolean;
    Data       : ATPlcItem;
    DataCnt    : Integer;
    Stack      : TPlcStack;
    PProgram   : PATPlcElement;
    PCount     : Integer;

    ForceList  : TList;

    InRung : Integer;
    Rung : Integer;
    CPos       : TPoint;     // Used by paint
    PStack     : TPlcPaintStack; // USed by Paint
    PProgramSize : Integer;
    SweepCount : Integer;

    ObjectPlc : TObjectPlc;
    FRightMargin : Integer;

    procedure RwIO(ele : PTPlcElement; write: boolean);
    function AddInstruction(ele : PTPlcElement) : Boolean;
    procedure ReadHeader;
    procedure ReadLocal;
    procedure ReadImplementation;
    procedure ReadInstructions;
    procedure Clean;
    function GetRungFromLine(aLine : Integer) : Integer;
    function GetLineFromRung(aRung : Integer) : Integer;
    function FindRung(aRung : Integer) : Integer;
    procedure GraphicCompile;
    function GetForceCount : Integer;
    function  FindData1 (aName : string) : TPlcItem;
    procedure GenerateHTTPPage(HFile : THANDLE; var CGIPath: WideString; CGIRequest : ICGIStringList);
    function InterpretToken(const aToken : string) : string;
  public
    constructor Create(aPlc : TObjectPlc);
    procedure Execute;
    procedure Compile;
    function  DataCount : word;
    procedure Paint(var aRung : Integer; Canvas : TCanvas; IWidth, IHeight : Integer);
    function  FindItem(X, Y, aRung, IWidth, IHeight : Integer) : PTPlcElement;
    function  FindOrAddData(aName : string) : TPlcItem; override;
    procedure AddData  (st : TPlcTokenizer; name, dtype : string); override;
    function SetForce(Element : PTPlcElement) : TPlcForceItem;
    procedure DeleteAllForces;
    property  ForceCount : Integer read GetForceCount;
    procedure SetRightMargin(aRightMargin, IWidth : Integer);
  end;

  TObjectPlcGlobalThread = class(TPlcTokenizer)
  private
    GlobalList : TStringList;
    IOList : TStringList;
    ObjectPlc : TObjectPlc;
    procedure CleanGlobals;
    function  AddNumeric(St : TPlcTokenizer; aName : string) : TPlcNumericItem;
    function  FindData1 (aName : string) : TPlcItem;
    function  FindNumeric (aName : string) : TPlcNumericItem;
    function  FindIOItem (aName : string) : TPlcItem;
    function InterpretToken(const aToken : string) : string;
  public
    constructor Create(aPlc : TObjectPlc);
    destructor Destroy; override;
    function FindOrAddData (aName : string) : TPlcItem; override;
    procedure GenerateHTTPPage(HFile : THANDLE; var CGIPath: WideString; CGIRequest : ICGIStringList);
    procedure AddData  (St : TPlcTokenizer; aName, DType : string); override;
    procedure AddGlobal  (St : TPlcTokenizer; aName, DType : string);
    procedure Compile;
    procedure GatePre;
    procedure GatePost;
  end;

  TPlcPaintStack = class(TObject)
  private
    Points : array [0..20] of TPoint;
    Pointer: Integer;
  public
    procedure Push(p : TPoint);
    function  Pop : TPoint;
    function  IsAtTop : boolean;
    procedure Reset;
    function Peek : TPoint;
  end;

implementation

const
  LNG_PLC_SELECTOR      = 'Unit';
  LNG_LOCAL_SECTION     = 'Local';
  LNG_IMPLEMENT_SECTION = 'Implementation';

  PlcGlobalFileName = 'PlcGlobal.H';
  
resourcestring
  BlankPlcFormat =
               LNG_PLC_SELECTOR + ' 0' +
               CarRet + CarRet +
               LNG_LOCAL_SECTION + CarRet +
               '//Add local definitions here' +
               CarRet + CarRet +
               LNG_IMPLEMENT_SECTION + CarRet +
               '//Code Begins here' +
               CarRet + CarRet +
               'end';


constructor TObjectPLC.Create(Aowner : TComponent);
var i : integer;
begin
  inherited create(Aowner);
  InstalledPlc := Self;

  for i := 0 to MAX_NBR_OBJECT_PLC do begin
    Thread[i] := TObjectPlcThread.Create(Self);
    Thread[i].Data[0] := TPlcItem(PlcControl);
    Thread[I].PlcIndex := I;
  end;

  PlcControl := TPlcPlcItem.Create(Thread[0], 'PLC');

  for i := 0 to MAX_NBR_OBJECT_PLC do begin
    Thread[i].DataCnt := 1;
    Thread[i].Data[0] := TPlcItem(PlcControl);
  end;

  Ftemplate := TStringList.create;
  FTemplate.Text := BlankPlcFormat;

  TextColor := clYellow;
  InactiveColor := clBlue;
  ActiveColor := clYellow;
  X_MARGIN := -2;
  Y_MARGIN := 10;
//  UpdateTick;
end;

destructor TObjectPlc.Destroy;
begin
  Stop;
  Clean;
  inherited;
end;

procedure TObjectPLC.InstallComponent(Params : TParamStrings);
var I : Integer;
begin
  inherited InstallComponent(Params);
  Lock := TCriticalSection.Create;

  TPlcTextItem.SetTextCount(Params.ReadInteger('PlcTextCount', 3));
  GlobalThread := TObjectPlcGlobalThread.Create(Self);
  TransientDisplay := Params.ReadBool('TransientDisplay', False);
  GlobalThread.Name := 'Global.H';

  for I := 0 to SysObj.PluginList.Count - 1 do begin
    if Assigned(SysObj.PluginList.Objects[I]) then 
      TPlcPlugin.BindPlugin(TNamedPlugin(SysObj.PluginList.Objects[I]).HMod)
  end;

  //SysObj.Localized.AppendLanguageFile(ProfilePath + PlcPath + 'plc_language.csv');

  for I := 0 to PlcClassList.Count - 1 do
    TPlcItemClass(PlcClassList.Objects[I]).InstallItem;

  LogicAnalyser := TLogicAnalyser.Create;
  LibNames := TStringList.Create;

  SysObj.HttpServerHost.AddComponentPath(HttpMaintenanceSection, Name, 'PLC Logic', Self);
end;

procedure TObjectPlc.Installed;
var I : Integer;
    S : TStringTokenizer;
begin
  inherited Installed;
  for I := 0 to PlcClassList.Count - 1 do
    TPlcItemClass(PlcClassList.Objects[I]).FinalInstallItem;

  TagEvent(Name + 'LogicAnalyser', EdgeFalling, TMethodTag, DoLogicAnalyser);
  S := TStringTokenizer.Create;
  SysObj.Comms.AddLanguageFile(CNC32.ProfilePath + 'live\ObjectPlc\ObjectPlcLanguage.csv');
  try
    S.Seperator := '()={}[]';
    try
      ReadLive(S, '\ObjectPlc\' + Name);
    except
      EventLog('No configuration file found');
    end;
    try
      LiveConfigure(S, False);
    except
      //raise ECNCInstallFault.CreateFmt('Invalid Format in %s at line %d', [Name, S.Line]);
    end;
  finally
    S.Free;
  end;
end;

procedure TObjectPlc.IShutDown;
var S : TStringList;
    I : Integer;
begin
  S := TStringList.Create;
  try
    S.Add(Name + ' = {');
    S.Add('  LogicAnalyser = {');
    for I := 0 to LogicAnalyser.Traces.Count - 1 do begin
      S.Add(Format('    %s = %d', [LogicAnalyser.Trace[I].Tag.Name, LogicAnalyser.Trace[I].Mask]));
    end;
    S.Add('  }');
    S.Add('}');
    try
      WriteLive(S, '\ObjectPlc\' + Name);
    except
      EventLog('Failed to write ObjectPlc\' + Name + ' configuration file');
    end;
  finally
    S.Free;
  end;
  inherited IShutdown;
end;

function TObjectPlc.HTTPGeneratePage(HFile : THANDLE; var CGIPath: WideString; CGIRequest : ICGIStringList) : Boolean;
var I : Integer;
    Tmp : string;
begin
  Result := True;
  if not inherited HTTPGeneratePage(HFile, CGIPath, CGIRequest) then begin
    if CGIPath = '' then begin
      WriteACNC32HTTPTitle(HFile, 'ObjectPlc');
      WriteACNC32HTTPHeading(hFile, 2, 'ObjectPlc List of PLCs');
      WriteStringToHandle(hFile, '<table border="1" align="center">');
      WriteStringToHandle(hFile, '<tr><th>Unit</th><th>Title</th><th>Loaded</th><th>Enabled</th></tr>');

      WriteStringToHandle(hFile, Format('<tr><td>SweepCount</td><td>%d</td><td></td><td></td></tr>' + CarRet,
                             [SweepCount]));
      WriteStringToHandle(hFile, Format('<tr><td><a href="%s/global">Global.h</a></td><td></td><td></td><td></td></tr>' + CarRet,
                             [Name]));

      for I := 0 to MAX_NBR_OBJECT_PLC do begin
        WriteStringToHandle(hFile, Format('<tr><td><a href="%s/plc%d">PLC %d</a></td><td>%s</td><td>%s</td><td>%s</td></tr>' + CarRet,
                             [Name, I, I, Thread[I].Name, BooleanIdent[Thread[I].Loaded], BooleanIDent[PlcControl.Enabled[I]]]));
      end;
      WriteStringToHandle(hFile, '</table>');
      WriteStringToHandle(hFile, '</body></html>');
    end else begin
      Tmp := ReadDelimitedW(CGIPath, '/');
      for I := 0 to MAX_NBR_OBJECT_PLC do begin
        if CompareText(Format('plc%d', [I]), Tmp) = 0 then begin
          Thread[I].GenerateHTTPPage(HFile, CGIPath, CGIRequest);
          Exit;
        end;
      end;
      if CompareText(Tmp, 'global') = 0 then begin
        GlobalThread.GenerateHTTPPage(HFile, CGIPath, CGIRequest);
        Exit;
      end;
    end;
  end;
  Result := False;
end;

function TObjectPlc.InterpretToken(const aToken : string) : string;
var Tmp : string;
    I : Integer;
begin
  Result := aToken;
  Tmp := ReadDelimited(Result, '.');
  if CompareText(Tmp, 'plc') = 0 then begin
    Tmp := ReadDelimited(Result, '.');
    try
      I := StrToInt(Tmp);
    except
      Result := '-- Invalid Plc Index --';
      Exit;
    end;

    I := I mod (MAX_NBR_OBJECT_PLC + 1);

    Result := Thread[I].InterpretToken(Result);
  end else if CompareText(Tmp, 'global') = 0 then begin
    Result := GlobalThread.InterpretToken(Result);
  end else
    Result := '-- Unknown Token --';
end;

function InterpretData(Item : TPlcItem; aToken : string) : string;
var List : TStringList;
    I : Integer;
begin
  if CompareText(aToken, 'value') = 0 then begin
    Result := IntToStr(Item.Value);
  end else begin
    List := TStringList.Create;
    try
      Item.ItemProperties(nil, List);
      for I := 0 to (List.Count - 1) div 2 do begin
        if CompareText(List[I * 2], aToken) = 0 then begin
          Result := List[I*2+1];
          Exit;
        end;
      end;
      Result := Format(' -- UNKNOWN PLC VALUE %s --', [aToken]);
    finally
      List.Free;
    end;
  end;
end;

function TObjectPlcGlobalThread.InterpretToken(const aToken : string) : string;
var I : Integer;
    Tmp : string;
begin
  Result := aToken;
  Tmp := ReadDelimited(Result, '.');
  for I := 0 to IOList.Count - 1 do begin
    if CompareText(Tmp, TPlcItem(IOList.Objects[I]).Name) = 0 then begin
      Result := InterpretData(TPlcItem(IOList.Objects[I]), Result);
      Exit;
    end;
  end;
  for I := 0 to GlobalList.Count - 1 do begin
    if CompareText(Tmp, TPlcItem(GlobalList.Objects[I]).Name) = 0 then begin
      Result := InterpretData(TPlcItem(GlobalList.Objects[I]), Result);
      Exit;
    end;
  end;
  Result := Format('-- UNKNOWN PLC DATA %s --', [aToken]);
end;


function TObjectPlcThread.InterpretToken(const aToken : string) : string;
var I : Integer;
    Tmp : string;
begin
  Result := aToken;
  Tmp := ReadDelimited(Result, '.');
  for I := 0 to DataCount - 1 do begin
    if CompareText(Tmp, Data[I].Name) = 0 then begin
      Result := InterpretData(Data[I], Result);
      Exit;
    end;
  end;
  Result := Format('-- UNKNOWN PLC DATA %s --', [aToken]);
end;

procedure TObjectPlcThread.GenerateHTTPPage(HFile : THANDLE; var CGIPath: WideString; CGIRequest : ICGIStringList);
var I, J : Integer;
    Tmp : string;
    List : TStringList;
begin
  if CGIPath = '' then begin
    WriteACNC32HTTPTitle(HFile, 'PlcThread');
    WriteStringToHandle(hFile, '<table border="1" align="center">');
    WriteStringToHandle(hFile, '<tr><th>Item</th><th>Value</th></tr>');
    for I := 0 to DataCount - 1 do begin
      WriteStringToHandle(hFile, Format('<tr><td><a href="plc%d/%s">%s</a></td><td>%d</td></tr>', [PlcIndex, Data[I].Name, Data[I].Name, Data[I].Value]));
    end;
    WriteStringToHandle(hFile, '</table>');
    //WriteStringToHandle(hFile, '<pre>' + Text + '</pre>'); ????? FIXME
  end else begin
    Tmp := ReadDelimitedW(CGIPath, '/');
    for I := 0 to DataCount - 1 do begin
      if CompareText(Tmp, Data[I].Name) = 0 then begin
        List := TStringList.Create;
        try
          WriteACNC32HTTPTitle(HFile, Format('Plc%d : %s', [PlcIndex, Data[I].Name]));
          WriteACNC32HTTPHeading(HFile, 1, Format('Plc%d', [PlcIndex]));
          WriteACNC32HTTPHeading(HFile, 2, 'Item = ' + Data[I].Name);
          Data[I].ItemProperties(nil, List);
          WriteStringToHandle(hFile, '<table border="1" align="center">');
          WriteStringToHandle(hFile, '<tr><th>Item</th><th>Value</th></tr>');
          for J := 0 to (List.Count - 1) div 2 do begin
            WriteStringToHandle(hFile, Format('<tr><td>%s</td><td>%s</td></tr>',
                    [List[J * 2], List[J*2+1]]));
          end;
          WriteStringToHandle(hFile, '</table>');
        finally
          List.Free;
        end;
      end;
    end;
  end;
end;

procedure TObjectPlcGlobalThread.GenerateHTTPPage(HFile : THANDLE; var CGIPath: WideString; CGIRequest: ICGIStringList);
var I : Integer;
begin
  if CGIPath = '' then begin
    WriteACNC32HTTPTitle(HFile, 'ObjectPlc Globals');
    WriteACNC32HTTPHeading(hFile, 1, Format('Object Plc Unit %d "%s"', [PlcIndex, Name]));;
    WriteStringToHandle(hFile, '<table border="1" align="center">');
    WriteStringToHandle(hFile, '<tr><th>Item</th><th>Value</th></tr>');
    for I := 0 to IOList.Count - 1 do begin
      WriteStringToHandle(hFile, Format('<tr><td>%s</td><td>%d</td></tr>', [IOList[I], TPlcItem(IOList.Objects[I]).Value]));
    end;
    for I := 0 to GlobalList.Count - 1 do begin
      WriteStringToHandle(hFile, Format('<tr><td>%s</td><td>%d</td></tr>', [GlobalList[I], TPlcItem(GlobalList.Objects[I]).Value]));
    end;
    WriteStringToHandle(hFile, '</table>');
    //WriteStringToHandle(hFile, '<pre>' + Text + '</pre>'); ????? FIXME
  end else begin
  end;
end;

procedure TObjectPlc.LiveConfigure(S : TStringTokenizer; InstallComponent : Boolean);
  procedure ReadLogicAnalyser;
  var Token : string;
    aTag : TAbstractTag;
    Bit : Integer;
  begin
    S.ExpectedTokens(['=', '{']);
    Token := LowerCase(S.Read);
    while not S.EndOfStream do begin
      if Token = '}' then begin
        Exit;
      end;

      aTag := SysObj.Comms.FindTag(Token);
      Bit := S.ReadIntegerAssign;
      if not InstallComponent then
        if aTag <> nil then
          LogicAnalyser.AddTag(aTag, Bit);

      Token := LowerCase(S.Read);
    end;
  end;


var Token : string;
begin
  // This is a little bit sad, I'm forced to write this with two entry conditions
  // InstallComponent : Boolean, if true the tagpublish if false then tagevent
  S.ExpectedTokens([Name, '=', '{']);

  Token := LowerCase(S.Read);
  while not S.EndOfStream do begin
    if Token = 'logicanalyser' then
      ReadLogicAnalyser
    else if Token = '}' then
      Exit
    else
      raise ECNCInstallFault.CreateFmt('Invalid format in %s.cfg', [Name]);
    Token := LowerCase(S.Read);
  end;
end;

procedure TObjectPlc.DoLogicAnalyser(aTag : TAbstractTag);
begin
  TLogicAnalyserForm.Execute(LogicAnalyser);
end;

var tt : Integer;
{ Called each time the logic is to be processed }
procedure TObjectPLC.Execute;
var i : integer;
begin
try
  Inc(SweepCount);
//  UpdateTick;
  if Pause then
    Exit;
  GlobalThread.GatePre;
  for i := 1 to MAX_NBR_OBJECT_PLC do begin
    if PlcControl.enabled[i] and thread[i].loaded then begin
      try
        Thread[i].Execute;
      except
        on E: Exception do begin
          SysObj.FaultInterface.SetFault(Self, PlcFaults[plcfFatalFault], [E.Message], nil);
          PlcControl.Enabled[I]:= False;
        end;
      end;
    end;
  end;
  GlobalThread.GatePost;
  LogicAnalyser.UpdateTrace;
except
  on E: Exception do begin
    SysObj.FaultInterface.SetFault(Self, PlcFaults[plcfFatalFault], [E.Message], nil);
    PlcControl.DisableAll;
    Inc(tt);
  end;
end;
end;

{ If in a pause state then exit. else calls stop then runs PLC 0 }
procedure TObjectPLC.Initialize;
begin
  if Pause then
    Exit;
  Stop;

  if Thread[0].Loaded then
    Thread[0].Execute;
end;

procedure TObjectPLC.Clean;
var I : Integer;
begin
  for I := 1 to MAX_NBR_OBJECT_PLC do begin
    Thread[I].Clean;
    //Thread[I].Free;
  end;
  Self.GlobalThread.CleanGlobals;
end;

{ Disables all plc's }
procedure TObjectPLC.Stop;
var I : Integer;
begin
  for I := 1 to MAX_NBR_OBJECT_PLC do
    PlcControl.Enabled[I] := False;
end;

{ returns true if the indexed PLC has been loaded }
function  TObjectPlc.readResident(ind : integer) : boolean;
begin
  ind := ind mod (MAX_NBR_OBJECT_PLC);
  Result := thread[ind].loaded;
end;

function TObjectPlc.GetRungCount(Index : Integer) : Integer;
begin
  Index := Index mod (MAX_NBR_OBJECT_PLC );
  Result := Thread[Index].Rung;
end;

{ ????? FIXME
function TObjectPlc.GetLineCount(Index : Integer) : Integer;
begin
  Index := Index mod (MAX_NBR_OBJECT_PLC );
  Result := Thread[Index].Count;
end;
}
{ returns true if the indexed PLC is enabled }
function  TObjectPlc.readActive(ind : integer) : boolean;
begin
  ind := ind mod (MAX_NBR_OBJECT_PLC);
  Result := PlcControl.enabled[ind];
end;

function  TObjectPlc.GetForceCount(Index : Integer) : Integer;
begin
  Index := Index mod (MAX_NBR_OBJECT_PLC);
  Result := Thread[Index].ForceCount;
end;

function  TObjectPlc.GetThreadName(Index : Integer) : string;
begin
  Index := Index mod (MAX_NBR_OBJECT_PLC);
  Result := Thread[Index].Name;
end;

function TObjectPlc.SetForce(Index : Integer; Element : PTPlcElement) : TPlcForceItem;
begin
  Result := Thread[Index].SetForce(Element);
end;

procedure TObjectPlc.RemoveForce(Index : Integer; Element : PTPlcElement);
var I : Integer;
    Obj : TObject;
    Force : TPlcForceItem;
begin
  I := Thread[Index].ForceList.IndexOf(Element.Item);
  if I <> -1 then begin
    Obj := Thread[Index].ForceList[I];
    if Obj is TPlcForceItem then begin
       Force := TPlcForceItem(Obj);
       Element.Item := Force.ForcedItem;
    end;
    Thread[Index].ForceList.Delete(I);
    Obj.Free;
  end;
end;

procedure TObjectPlc.DeleteAllForces;
var I : Integer;
begin
  for I := 0 to MAX_NBR_OBJECT_PLC do begin
    Thread[I].DeleteAllForces;
  end;
end;

procedure TObjectPlcThread.DeleteAllForces;
var Force : TPlcForceItem;
begin
  while ForceList.Count > 0 do begin
    Force := TPlcForceItem(ForceList[0]);
    Force.Element.Item := Force.ForcedItem;
    Force.Free;
    ForceList.Delete(0);
  end;
end;

{ returns the number of active data objects used by the indexed plc }
function  TObjectPlc.readDataCount(ind : integer) : integer;
begin
  ind := ind mod (MAX_NBR_OBJECT_PLC + 1);
  Result := thread[ind].DataCount;
end;

function TObjectPlc.template(p : integer) : TStringList;
begin
  Ftemplate[0] :=  LNG_PLC_SELECTOR + ' ' + inttostr(p);
  Result := Ftemplate;
end;

function TObjectPlc.GetRungFromLine(aPlc, aLine : Integer) : Integer;
begin
  aPlc := aPlc mod (MAX_NBR_OBJECT_PLC + 1);
  Result := Thread[aPlc].GetRungFromLine(aLine);
end;

function TObjectPlc.GetLineFromRung(aPlc, aRung : Integer) : Integer;
begin
  aPlc := aPlc mod (MAX_NBR_OBJECT_PLC + 1);
  Result := Thread[aPlc].GetLineFromRung(aRung);
end;

function TObjectPlcThread.GetRungFromLine(aLine : Integer) : Integer;
var I : Integer;
begin
  I := 0;
  Result := 0;
  while PPRogram^[I].Graphics.Line < aLine do begin
    if PProgram^[I].OpCode = PLCIEnd then
      Exit;
    Inc(I);
  end;
  Result := PProgram^[I].Graphics.Rung;
end;

function TObjectPlcThread.GetLineFromRung(aRung : Integer) : Integer;
var I : Integer;
begin
  I := 0;
  Result := 1;
  while PPRogram^[I].Graphics.Rung < aRung do begin
    if PProgram^[I].OpCode = PLCIEnd then
      Exit;
    Inc(I);
    Result := PProgram^[I].Graphics.Line;
  end;
end;

constructor TObjectPlcGlobalThread.Create(aPlc : TObjectPlc);
begin
  inherited Create;
  Self.QuoteChar:= '"';

  GlobalList := TStringList.Create;
  GlobalList.Sorted := True;

  IOList := TStringList.Create;
  IOList.Sorted := True;

  StdSeperator;
  ObjectPlc := aPlc;
end;

destructor TObjectPlcGlobalThread.Destroy;
begin
  CleanGlobals;
  inherited Destroy;
end;

procedure TObjectPlcGlobalThread.AddData(St : TPlcTokenizer; aName, DType : string);
begin
  ObjectPlc.GlobalThread.AddNumeric(St, aName);
end;

procedure TObjectPlcGlobalThread.AddGlobal  (St : TPlcTokenizer; aName, DType : string);
var ItemClass : TPlcItemClass;
    Item : TPlcItem;
begin
  ItemClass := FindPlcItemClass(DType);
  if Assigned(ItemClass) then begin
    if ItemClass = TPlcNumericItem then begin
      AddNumeric(Self, aName);
    end else begin
      if ItemClass.RequiresMapping then begin
        if SysObj.IO.IOFindTag(aName) = nil then
          InstalledPlc.TagEvent(aName, edgeChange, TIntegerTag, nil);
      end;

      if FindIOItem(aName) = nil then begin
        Item := ItemClass.Create(Self, aName);
        IOList.AddObject(aName, Item);
      end else
        while not St.EndOfStream and (St.Read <> ';') do;
      Exit;
    end;
  end else
    raise ECompileItemFail.CreateFmt(InvalidDataType, [DType]);
end;

procedure TObjectPlcGlobalThread.GatePre;
var I : Integer;
begin
  for I := 0 to IOList.Count - 1 do begin
    TPlcItem(IOList.Objects[I]).GatePre;
  end;
end;


procedure TObjectPlcGlobalThread.GatePost;
var I : Integer;
begin
  for I := 0 to IOList.Count - 1 do begin
    TPlcItem(IOList.Objects[I]).GatePost;
  end;
end;

function TObjectPlcGlobalThread.AddNumeric(St : TPlcTokenizer; aName : string) : TPlcNumericItem;
begin
  Result := FindNumeric(aName);
  if Result = nil then begin
    Result := TPlcNumericItem.Create(St, aName);
    GlobalList.AddObject(UpperCase(aName), Result);
  end else begin
    if Assigned(St) then // Clean up the line
      St.ReadLine;
  end;
end;

function  TObjectPlcGlobalThread.FindNumeric(aName : string) : TPlcNumericItem;
var I : Integer;
begin
  Result := nil;
  I := GlobalList.IndexOf(UpperCase(aName));
  if I <> -1 then
    Result := TPlcNumericItem(GlobalList.Objects[I]);
end;

function TObjectPlcThread.FindOrAddData(aName : string) : TPlcItem;
begin
  Result := ObjectPlc.GlobalThread.FindOrAddData(aName);
end;

function TObjectPlcGlobalThread.FindOrAddData(aName : string) : TPlcItem;
var I : Integer;
begin
  Result := FindData1(aName);
{  if (Result = nil) and
      Assigned(ObjectPlc.CompileThread) then
    Result := ObjectPlc.CompileThread.FindData1(aName); }

  if Result = nil then begin
    try
      I := StrToInt(aName);
      Result := AddNumeric(nil, aName);
      Result.SetValue(I);
    except
      raise ECreateItemFail.CreateFmt(NotExistOrNoValue, [aName]);
    end;
  end;
end;

function  TObjectPlcGlobalThread.FindData1(aName : string) : TPlcItem;
var I : Integer;
begin
  if Assigned(ObjectPlc.CompileThread) then begin
    with ObjectPlc.CompileThread do
      for i := 0 to DataCount - 1 do begin
        if Data[i] <> nil then // ensure this space has been allocated
          if CompareText(Data[i].Name, aName) = 0 then begin
            Result := Data[I];
            Exit;
          end;
      end;
  end;

  Result := FindNumeric(aName);
  if Result = nil then
    Result := FindIOItem(aName);
end;

function  TObjectPlcGlobalThread.FindIOItem(aName : string) : TPlcItem;
var I : Integer;
begin
  Result := FindNumeric(aName);
  if Result = nil then begin
    I := IOList.IndexOf(UpperCase(aName));
    if I <> -1 then
      Result := TPlcItem(IOList.Objects[I]);
  end;
end;

procedure TObjectPlcGlobalThread.CleanGlobals;
begin
  while GlobalList.Count > 0 do begin
    GlobalList.Objects[0].Free;
    GlobalList.Delete(0);
  end;

  while IOList.Count > 0 do begin
    IOList.Objects[0].Free;
    IOList.Delete(0);
  end;
end;

procedure TObjectPlcGlobalThread.Compile;
var
  aName, DType : string;
  FileStream: TStream;
begin
  //LoadFromFile(ProfilePath + PlcPath + PlcGlobalFileName);
  FileStream:= ParsedInputStream(ProfilePath + PlcPath + PlcGlobalFileName);
  //FileStream:= TFileStream.Create(ProfilePath + PlcPath + PlcGlobalFileName, fmOpenRead);
  Self.SetStream(FileStream);
  try
    Rewind;
    while not Self.Endofstream do begin
      // read the name token
      aName := Self.SkipComment;
      DType := Self.SkipComment;
      AddGlobal(Self, aName, DType);
      Read;
      Push;
    end;
  finally
    ReleaseStream;
  end;
end;

{ returns true if there were no errors compiling filename. Sets faultString and
  returns false if an error occured. readResident will also reflect a successful
  compile }
function TObjectPLC.LoadFromFile(PlcN : integer; Filename : string; Inc : Boolean) : Boolean;
var FileStream: TStream;
begin
  Result := False;
  Lock.Enter;
  try
    Pause := True;
    try
      Thread[PlcN].StdSeperator;
      FileStream:= Thread[PlcN].ParsedInputStream(FileName);
      //FileStream:= TFileStream.Create(FileName, fmOpenRead);
      Thread[PlcN].SetStream(FileStream);
      try
        CompileThread := Thread[PlcN];
        try
          if Inc then begin
            try
              GlobalThread.Compile;
            except
              on E : Exception do begin
                SetFaultString(E.Message, GlobalThread);
                Exit;
              end;
            end;
          end;

          Thread[PlcN].Compile;
          Pause := False;
          if PlcN = 0 then
            Initialize;

          Result := True;
        except
          on E : Exception do begin
             SetFaultString(E.Message, Thread[PlcN]);
          end;
        end;
      finally
        Thread[PlcN].ReleaseStream;
      end;
    except
      begin
        SetFaultString(Format(FileNotFound, [filename]), Thread[PlcN]);
      end;
    end;
  finally
    CompileThread := nil;
    Lock.Leave;
  end;
end;

type
  TXRef = class(TList)
    FirstName : string;
    Element : PTPlcElement;
  end;

procedure TObjectPlc.GenerateCrossReference(FileName : string);
var I, J, K : Integer;
    Stream : TFileStream;
    Tmp : string;
    XRef : TXRef;
    SL : TStringList;
    Element : PTPlcElement;
begin
  SL := TStringList.Create;
  try
    Stream := TFileStream.Create(FileName, fmCreate);
    try
      for I := 0 to MAX_NBR_OBJECT_PLC do begin
        if Thread[I].Loaded then begin
          Tmp := CarRet + '# Plc' + Thread[I].Name + ': Local Definitions' + CarRet;
          Stream.Write(PChar(Tmp)^, Length(Tmp));
          for J := 1 to Thread[I].DataCnt - 1 do begin
            if (Thread[I].Data[J] is TPlcNamedItem) or
               (Thread[I].Data[J] is TPlcNamedItemR) then begin

            end else begin // local ref
              Tmp := Thread[I].Data[J].Name;
              Tmp := Tmp + StringOfChar(' ', 20 - Length(Tmp));
              Tmp := Tmp + Thread[I].Data[J].ClassName + CarRet;
              Stream.Write(PChar(Tmp)^, Length(Tmp));
            end;
          end;

          J := 0;
          with Thread[I] do begin
            while Thread[I].PProgram[J].OpCode <> PLCIEnd do begin
              if (PProgram[J].Item <> nil){ and
                ((PProgram[J].Item is TPlcNamedItem) or
                 (PProgram[J].Item is TPlcNamedItemR)) } then begin
                K := SL.IndexOf(UpperCase(PProgram[J].Item.Name));
                if K = -1 then begin
                  XRef := TXRef.Create;

                  XRef.FirstName := Thread[I].PProgram[J].Item.Name;
                  SL.AddObject(UpperCase(PProgram[J].Item.Name), XRef);
                end else begin
                  XRef := TXRef(SL.Objects[K]);
                end;
                XRef.Add(@Thread[I].PProgram[J]);
              end;
              Inc(J);
            end;
          end;
        end;
      end;

      SL.Sort;

      Tmp := CarRet + '# Global References' + CarRet;
      Tmp := Tmp + '# The Case of the name is as per its first reference' + CarRet;
      Tmp := Tmp + Format('# SYNTAX : Plc0..%d[W] Line ; Rung [*]', [MAX_NBR_OBJECT_PLC]) + CarRet;
      Tmp := Tmp + '# Where :-' + CarRet;
      Tmp := Tmp + '# W = Declared Writable IO' + CarRet;
      Tmp := Tmp + '# Line = The Line Number in the source' + CarRet;
      Tmp := Tmp + '# Rung = The Index of the Graphical Rung' + CarRet;
      Tmp := Tmp + '# * The item is being written to' + CarRet;
      Stream.Write(PChar(Tmp)^, Length(Tmp));

      for I := 0 to SL.Count - 1 do begin
        XRef := TXRef(SL.Objects[I]);
        Tmp := CarRet + XRef.FirstName + CarRet;
        Stream.Write(PChar(Tmp)^, Length(Tmp));
        for J := 0 to XRef.Count - 1 do begin
          Element := PTPlcElement(XRef[J]);
          Tmp := 'Plc' + IntToStr(Element^.PlcIndex); //.Item.PlcThread.PlcIndex);
          if Element^.Item is TPlcNamedItem then
            Tmp := Tmp + 'W';
          Tmp := Tmp + StringOfChar(' ', 8 - Length(Tmp));
          Tmp := Tmp + Element.Item.GateName(Element);
          Tmp := Tmp + StringOfChar(' ', 30 - Length(Tmp)) + ' ';
          Tmp := Tmp + IntToStr(Element^.Graphics.Line) + ';' + IntToStr(Element^.Graphics.Rung);
          if Element^.OpCode in OutputPlcOpCodes then
            Tmp := Tmp + '*';

          Tmp := Tmp + CarRet;
          Stream.Write(PChar(Tmp)^, Length(Tmp));
        end;
      end;

      Tmp := CarRet + CarRet + 'Globals' + CarRet;
      Stream.Write(PChar(Tmp)^, Length(Tmp));

      for I := 0 to GlobalThread.GlobalList.Count - 1 do begin
        Tmp := TPlcItem(GlobalThread.GlobalList.Objects[I]).Name;
        Tmp := Tmp + StringOfChar(' ', 20 - Length(Tmp));
        Tmp := Tmp + IntToStr(TPlcNumericItem(GlobalThread.GlobalList.Objects[I]).Value) + CarRet;
        Stream.Write(PChar(Tmp)^, Length(Tmp));
      end;

      Tmp := CarRet + CarRet;
      Stream.Write(PChar(Tmp)^, Length(Tmp));

    finally
      Stream.Free;
    end;
  finally
    while SL.Count > 0 do begin
      SL.Objects[0].Free;
      SL.Delete(0);
    end;
    SL.Free;
  end;
end;

procedure TObjectPlc.ClientPaint(PLC : Integer; var Rung : Integer; Canvas : TCanvas; IWidth, IHeight : Integer);
begin
  Thread[PLC].Paint(Rung, Canvas, IWidth, IHeight);
end;

procedure TObjectPlc.WndProc(var Message : TMessage);
begin
  if Message.Msg = CNCM_PLC_EVENT then
    TAbstractTag(Message.LParam).Refresh
  else
    inherited WndProc(Message);
end;

function TObjectPlc.FindItem(aPlc, X, Y, aRung, IWidth, IHeight : Integer) : PTPlcElement;
begin
  Result := Thread[aPlc].FindItem(X, Y, aRung, IWidth, IHeight);
end;

{ Pauses the execution of all PLC's }
procedure TObjectPlc.SetPause(aVal : boolean);
begin
  FPause := aVal;
end;

procedure TObjectPlc.SetRightMargin(aRightMargin, IWidth : Integer);
var I : Integer;
begin
  FRightMargin := aRightMargin;
  for I := 0 to MAX_NBR_OBJECT_PLC do
    Thread[I].SetRightMargin(FRightMargin, IWidth);
end;

procedure TObjectPlc.SetFaultString(aFault : string; St : TPlcTokenizer);
begin
  FFaultString := Format('PLC[%s]:Line[%d]: %s ', [St.Name, St.Line, aFault]);
  FFaultPos.X := 1;
  FFaultPos.Y := St.Line;
  FFaultPlc := St.PlcIndex;
end;


procedure TObjectPlcThread.Clean;
var i : integer;
begin
  Loaded := False;
  DataCnt := 1;
  for i := 1 to MaximumPlcItems do begin
    Data[i].free;
    Data[i] := nil;
  end;

  while ForceList.Count > 0 do begin
    TPlcForceItem(ForceList[0]).Free;
    ForceList.Delete(0);
  end;

  PProgramSize := 10;
  ReallocMem(PProgram, PProgramSize * Sizeof(TPlcElement));
  FillChar(PProgram^, PProgramSize * Sizeof(TPlcElement), 0);
end;

procedure TObjectPlcThread.Compile;
begin
  Clean;
  ReadHeader;
  ReadLocal;
  ReadImplementation;

  loaded := True;
end;

procedure TObjectPlcThread.ReadHeader;
var token : string;
begin
  Self.Rewind;
  token := Self.SkipComment;
  if CompareText(token, LNG_PLC_SELECTOR) = 0 then begin
//    token := Self.SkipComment;
    Name := Trim(ReadLine);
  end else raise ECompileItemFail.Create(InvalidHeaderFormat);
end;

procedure TObjectPlcThread.ReadLocal;
var token, datatype : string;
begin
  Self.Rewind;
  Self.FindToken(LNG_LOCAL_SECTION);

  if Self.EndofStream then
    raise ECompileItemFail.Create(MissingLocalSection);

  while not Self.Endofstream do begin
    // read the name token
    token := Self.SkipComment;
    if CompareText(token, LNG_IMPLEMENT_SECTION) = 0 then exit;

    datatype := Self.SkipComment;
    AddData(Self, token, DataType);
  end;
end;

{ Called to create a data item, this uses the const PlcItemClass array to create
  the required Item. The create of an item may intern call addData }
procedure TObjectPlcThread.AddData(st : TPlcTokenizer; name, dtype : string);
var co : integer;
    ItemClass : TPlcItemClass;
begin
  co := dataCount; // Because this function recurses, data count must be
                   // captured on the stack
  ItemClass := FindPlcItemClass(Dtype);
  if Assigned(ItemClass) then begin
    if ItemClass = TPlcNumericItem then begin
      ObjectPlc.GlobalThread.AddNumeric(St, Name)
    end else if ItemClass = TPlcPlugin then begin
      Inc(DataCnt);
      if DataCnt >= MaximumPlcItems then
        raise ECompileItemFail.CreateFmt(MaximumDataObjects, [Self.name]);
      Data[co] := TPlcPlugin.BindCreate(DType, St, Name);
    end else begin
      Inc(DataCnt);
      if DataCnt >= MaximumPlcItems then
        raise ECompileItemFail.CreateFmt(MaximumDataObjects, [Self.name]);
      Data[co] := ItemClass.Create(st, Name);
    end;
  end else
    raise ECompileItemFail.CreateFmt(InvalidDataType, [dtype]);
end;

{ Reads the logic section of a plc script, compiling the code into the data and
  pProgram arrays accordingly }
procedure TObjectPlcThread.ReadImplementation;
begin
  Rung := 0;
  InRung := 0;
  PCount := 0;
  ReadInstructions;
  GraphicCompile;
end;

procedure TObjectPlcThread.ReadInstructions;
  procedure CheckProgramSize(aCount : Integer);
  begin
    if aCount + 5 > PProgramSize then begin
      Inc(PProgramSize, 50);
      System.ReallocMem(PProgram, PProgramSize * Sizeof(TPlcElement));
    end;
  end;

var Op    : string;
    OpCode, LastOpCode: TPLCOpCode;
    Found: Boolean;
begin
  LastOpCode := PLCIend;
  while not Self.EndofStream do begin
    op := Self.SkipComment;
    if Self.EndofStream then begin
      PProgram^[PCount].OpCode := PLCIEND;
      Exit;
    end;
    Found:= False;
    for OpCode:= Low(TPLCOpCode) to High(TPLCOpCode) do
      if CompareText(NPLCOpCode[Opcode], Op) = 0  then begin
        Found:= True;
        Break
      end;

    if not Found then
      raise ECompileItemFail.CreateFmt(InvalidOpCode, [op]);

    PProgram^[PCount].OpCode := OpCode;
    PProgram^[PCount].Graphics.Line := Self.Line;
    PProgram^[PCount].Graphics.Rung := Rung;
    PProgram^[PCount].Item := nil;
    PProgram^[PCount].Line := False;
    PProgram^[PCount].PlcIndex := PlcIndex;

    if AddInstruction(@PProgram^[PCount]) then // Popping the stack
      if InRung = 0 then
        raise ECompileItemFail.Create(TooManyCloseParentheses)
      else
        Exit;

    if (OpCode = PLCIor) and (LastOpCode = PLCIout) then
      raise ECompileItemFail.Create(OrCannotFollowOut);
    LastOpCode := OpCode;

    if OpCode = PlcIEnd then
      Exit;
    Inc(PCount);
    CheckProgramSize(PCount);
  end;
end;


// Returns true on a closing stack operation
function TObjectPlcThread.AddInstruction(ele : PTPlcElement) : Boolean;
begin
  Result := False;
  case Ele.OpCode of
       PLCIif,
       PLCIifN,
       PLCIOldload,
       PLCIOldloadN : begin
                      if InRung = 0 then
                        Inc(Rung);
                      Ele^.Graphics.Rung := Rung; // Reflect the change
                      RwIO(Ele, False);
                    end;
       PLCIand,
       PLCIor,
       PLCIandN,
       PLCIswitch,
       PLCIorN   : RwIO(ele, False);
       PLCIStrOr,
       PLCIStrAnd: Exit;
       PLCIandPop,
       PLCIorPop : Result := True; // This should be the return from recursion
       PLCIout,
       PLCIoutN  : RwIO(ele, True);
       PLCIEND   : if InRung <> 0 then
                     raise ECompileItemFail.Create(TooFewCloseParenthasis);
  else
    raise ECompileItemFail.CreateFmt(ContactSupplier, ['TObjectPlc.addInstruction']);
  end;
end;

procedure TObjectPlcThread.rwIO(ele : PTPlcElement; Write: boolean);
var token : string;
    Hold : TPlcElement;
begin
  token := Self.SkipComment;

  if Token = '(' then begin
  // A stack operation has arrived
    Hold := Ele^;
    case Ele^.OpCode of
      PLCIor : begin
               Hold.OpCode := PLCIorPop;
               Hold.Item := nil;
               Ele^.OpCode := PLCIStrOr;
      end;

      PLCIand : begin
               Hold.OpCode := PLCIandPop;
               Hold.Item := nil;
               Ele^.OpCode := PLCIStrAnd;
      end;
    else
      raise ECompileItemFail.Create(ParenthasisSeperationError);
    end;
    Inc(PCount);
    Inc(InRung);
    ReadInstructions; // Recursion point !@!!
    Dec(InRung);
    PProgram^[PCount] := Hold;
  end else begin
    Ele^.Item := FindData1(token);
    if Ele^.Item = nil then
      raise ECompileItemFail.CreateFmt(ItemNotLocalOrPublic, [token]);

    Ele^.Item.Compile(Ele, Self, write);
  end;
end;

function TObjectPlcThread.FindData1(aName : string) : TPlcItem;
begin
  Result := ObjectPlc.GlobalThread.FindData1(aName);
end;

constructor TObjectPLCThread.Create(aPlc : TObjectPlc);
begin
  inherited Create;
  ObjectPlc := aPlc;
  Self.QuoteChar:= '"';
  Stack    := TPlcStack.create;
  PStack   := TPlcPaintStack.Create;
  ForceList := TList.Create;
  PProgramSize := 50;
  GetMem(PProgram, Sizeof(TPlcElement) * PProgramSize);
end;

procedure TObjectPLCThread.Execute;
var i       : Integer;
    Line    : Boolean;
    Element : PTPlcElement;
begin
  Line := true;
  Inc(SweepCount);

  for i := 0 to DataCount - 1 do begin
    Data[i].GatePre;
  end;

//  if Stack.Top <> 0 then // Add this check for compiler if necessary.
    Stack.Top := 0;

  I := 0;
  while PProgram^[i].OpCode <> PLCIEND do begin
    Element := @PProgram^[i];
    case element.opcode of
      PLCIBad: begin
        Inc(i);
        Dec(i);
      end;
      PLCIOldload,
      PLCIif : begin
        Element^.Line := Element.Item.Read(Element);
        Line := Element^.Line;
      end;
      PLCIOldloadN,
      PLCIifN    : begin
        Element^.Line := Element.Item.Read(Element);
        Line := not Element^.Line;
      end;
      PLCIand    : begin
        Element^.Line := Element.Item.Read(Element);
        Line := Line and Element^.Line;
      end;
      PLCIor     : begin
        Element^.Line := Element.Item.Read(Element);
        Line := Element^.Line or Line;
      end;
      PLCIandN : begin
        Element^.Line := Element.Item.Read(Element);
        Line := Line and not Element^.Line;
      end;
      PLCIorN : begin
        Element^.Line := Element.Item.Read(Element);
        Line := Line or not Element^.Line;
      end;
      PLCIStrOr,
      PLCIStrAnd : begin
        Stack.Push(line);
      end;
      PLCIandPop : begin
        Line := Stack.Pop and line; {26/5/98}
      end;
      PLCIorPop  : begin
        Line := Stack.Pop or Line; {23/11/99}
      end;
      PLCIout : begin
        Element.Item.Write(element, Line);
        Element^.Line := Line;
      end;
      PLCIoutN   : begin
        Element.Item.Write(Element, not Line);
        Element^.Line := not Line;
      end;
      PLCIswitch : begin
         Line := Element.Item.Read(Element);
         Element^.Line := Line;
         if not line then begin
           Inc(i);
           while (PProgram^[i].opCode <> PLCIswitch) and
                 (PProgram^[i].opCode <> PLCIif) and
                 (PProgram^[i].opCode <> PLCIifN) and
                 (PProgram^[i].opCode <> PLCIOldload) and
                 (PProgram^[i].opCode <> PLCIOldloadN) and
                 (PProgram^[i].opCode <> PLCIEND) do
                 Inc(i);
           end;
      end;
    else
    end;
    if ObjectPLC.TransientDisplay then
      Element.Item.UpdateTransientDisplay(Element);
    Inc(i);
  end;

  for i := 0 to dataCount - 1 do begin
    Data[i].GatePost;
  end;
end;

function TObjectPLCThread.DataCount : word;
begin
  Result := DataCnt;
end;

function TObjectPlcThread.FindRung(aRung : Integer) : Integer;
begin
  // Find the first OpCode using aRung
  Result := 0;
  while PProgram^[Result].Graphics.Rung <> aRung do begin
    if PProgram^[Result].OpCode = PLCIEND then
      Exit;
    Inc(Result);
  end;
end;

function TObjectPlcThread.GetForceCount : Integer;
begin
  Result := ForceList.Count;
end;

function TObjectPlcThread.SetForce(Element : PTPlcElement) : TPlcForceItem;
begin
//  Result := nil;
//  if Element.Item.PlcThread = Self then begin
    if Element.Item is TPlcForceItem then begin
      Result := TPlcForceItem(Element.Item);
    end else begin
      Result := TPlcForceItem.Create(Self, Element.Item.Name);
      Result.ForcedItem := Element.Item;
      Result.Element := Element;
      Element.Item := Result;
      ForceList.Add(Result);
    end;
//  end;
end;

procedure TObjectPlcThread.SetRightMargin(aRightMargin, IWidth : Integer);
begin
  FRightMargin := (aRightMargin div IWidth) - 1;
  if Loaded then
    GraphicCompile;
end;


procedure TObjectPlcThread.Paint(var aRung : Integer; Canvas : TCanvas; IWidth, IHeight : Integer);
var I : Integer;
  Ele : PTPlcElement;
  YOff, BottomY : Integer;

  function PaintPosX(X : Integer) : integer;
  begin
    Result := (X * IWidth) + InstalledPlc.X_MARGIN;
  end;

  function PaintPosY(Y : Integer) : integer;
  begin
    Y := Y + YOff;
    if Y > BottomY then
      BottomY := Y;

    Result := (Y * IHeight) + InstalledPlc.Y_MARGIN;
  end;

  function CenterY(Y : INteger) : Integer;
  begin
    Y := Y + YOff;
    if Y > BottomY then
      BottomY := Y;

    Result := (Y * IHeight) + (IHeight div 2) + InstalledPlc.Y_MARGIN;
  end;

  {function RungWillFit(Tmp : Integer) : Boolean;
  var MaxPosY, ThisRung : Integer;
  begin
    MaxPosY := (Canvas.ClipRect.Bottom - InstalledPlc.Y_MARGIN) div (IHeight + 1);
    ThisRung := PProgram^[Tmp].Graphics.Rung;
    Result := True;
    while Result and (PProgram^[Tmp].Graphics.Rung = ThisRung) do begin
      Result := PProgram^[Tmp].Graphics.PosY + BottomY <= MaxPosY;
      Inc(Tmp);
    end;
  end;}

begin
  if not Loaded then
    Exit;

  if PProgram^[0].OpCode = PLCIBad then
    Exit;

  I := FindRung(aRung);

  Canvas.Pen.Width := 2;
  Canvas.Font.Color := InstalledPlc.TextColor;
  Canvas.Font.Size := 7;

  YOff := 0;
  BottomY := 0;

  while PaintPosY(0) < ((Canvas.ClipRect.Bottom * 10) div 11) do begin
    while (PProgram^[I].Graphics.Rung = aRung) do begin
      Ele := @PProgram^[I];
      with Ele^.Graphics do begin
        if Ele^.Item <> nil then begin
          Canvas.Textout(PaintPosX(PosX) + 4, PaintPosY(PosY) - 7, Ele^.Item.Name);
          Canvas.Textout(PaintPosX(PosX) + 4, PaintPosY(PosY) - 7 + Canvas.TextHeight('Q'), Ele^.Item.GateName(Ele));
          Ele^.Item.GatePaint(  PaintPosX(PosX),
                                PaintPosY(PosY) ,
                                Ele, Canvas, IWidth, IHeight);
        end;
        case Ele^.OpCode of
          PLCIOldload,
          PLCIif,
          PLCIOldloadN,
          PLCIifN,
          PLCIand,
          PLCIswitch,
          PLCIandN : begin
            Canvas.MoveTo(PaintPosX(PosX), CenterY(PosY));
            Canvas.LineTo(PaintPosX(ConnectX), CenterY(ConnectY));
          end;

          PLCIor,
          PLCIorN : begin
            Canvas.MoveTo(PaintPosX(PosX), CenterY(PosY));
            Canvas.LineTo(PaintPosX(PosX), CenterY(ConnectY));
            Canvas.MoveTo(PaintPosX(PosX + 1), CenterY(PosY));
            Canvas.LineTo(PaintPosX(ConnectX), CenterY(PosY));
            Canvas.LineTo(PaintPosX(ConnectX), CenterY(ConnectY));
          end;
          PLCIout,
          PLCIoutN : begin
            Canvas.MoveTo(PaintPosX(PosX), CenterY(PosY));
// Use the following when OUT AND OUT is not allowed, currently it is.
// The full solution would involve a second parse in GraphicCompile
// with some form of ConnectX2 parameter thats only used by outputs.
//            Canvas.LineTo(PaintPosX(PosX), CenterY(ConnectY));
//            Canvas.LineTo(PaintPosX(ConnectX), CenterY(ConnectY));
            Canvas.LineTo(PaintPosX(ConnectX), CenterY(PosY));
            Canvas.LineTo(PaintPosX(ConnectX), CenterY(ConnectY));
          end;
          PLCIOrPop : begin
            Canvas.MoveTo(PaintPosX(PosX), CenterY(PosY));
            if ConnectX < PosX then begin
              Canvas.LineTo(PaintPosX(PosX), CenterY(ConnectY));
              Canvas.LineTo(PaintPosX(ConnectX), CenterY(ConnectY));
            end else begin 
              Canvas.LineTo(PaintPosX(ConnectX), CenterY(PosY));
              Canvas.LineTo(PaintPosX(ConnectX), CenterY(ConnectY));
            end;
          end;
          PLCIStrOr : begin
            Canvas.MoveTo(PaintPosX(PosX), CenterY(PosY));
            Canvas.LineTo(PaintPosX(ConnectX), CenterY(ConnectY));
          end;

          PLCIBad, PLCIEnd : begin
            aRung := -1;
            Exit;
          end;
        end;
      end;
      Inc(I);
    end;
    Inc(BottomY);
    aRung := aRung + 1;
    YOff := BottomY;
    {if not Rungwillfit(I) then
      Exit;}
  end;
end;

procedure TObjectPlcThread.GraphicCompile;
var XDepth     : array [0..100] of Integer;

  function FindOutputPath(X : Integer) : Integer;
  var I : Integer;
  begin
    Result := XDepth[X];
    for I := X to GRAPH_X_WIDTH do begin
      if XDepth[I] > Result then
        Result := XDepth[I];
    end;

    for I := X to GRAPH_X_WIDTH do
      XDepth[I] := Result + 1
  end;

  function OrDeepest(StartPos : Integer) : Integer;
  var I : Integer;
  begin
    Result := XDepth[StartPos];
    for I := StartPos to CPos.X do begin
      if XDepth[I] > Result then
        Result := XDepth[I];
    end;
    for I := StartPos to CPos.X - 1 do
      XDepth[I] := Result + 1
  end;

var
  Ele : PTPlcElement;
  I, J : Integer;
  aRung, RungStart : Integer;
  CposHigh : Integer;
  Tmp : TPoint;
begin
  I := 0;

  while PProgram^[I].OpCode <> PLCIEnd do begin

    for J := 0 to GRAPH_X_WIDTH do
      XDepth[J] := 0;

    CPos.X := 0;
    CPos.Y := 0;
    PStack.Reset;
    PStack.Push(CPos);
    aRung := PProgram^[I].Graphics.Rung;

    RungStart := I;
    CPosHigh := 0;

    try
      while PProgram^[I].Graphics.Rung = aRung do begin
        Ele := @PProgram^[I];

        case Ele^.OpCode of
          PLCIBad,
          PLCIOldload,
          PLCIif,
          PLCIOldloadN,
          PLCIifN,
          PLCIand,
          PLCIswitch,
          PLCIandN : begin
                     Ele^.Graphics.ConnectX := CPos.X;
                     Ele^.Graphics.ConnectY := CPos.Y;
                     if CPos.Y < XDepth[CPos.X] then begin
                       Inc(CPos.Y);
                       Inc(XDepth[CPos.X]);
                     end;
                     Ele^.Graphics.PosX := CPos.X;
                     Ele^.Graphics.PosY := CPos.Y;
                     Inc(XDepth[CPos.X]);
                     Inc(CPos.X);
          end;

          PLCIor,
          PLCIorN : begin
                     Ele^.Graphics.PosX := PStack.Peek.x;
                     Ele^.Graphics.PosY := OrDeepest(Ele^.Graphics.PosX);
                     Ele^.Graphics.ConnectX := CPos.X;
                     Ele^.Graphics.ConnectY := CPos.Y;
  //                   Inc(XDepth[Ele^.Graphics.PosX]);
          end;

          PLCIStrOr : begin
                     Ele^.Graphics.PosX := PStack.Peek.X;
                     Ele^.Graphics.PosY := PStack.Peek.Y;
                     Ele^.Graphics.ConnectX := PStack.Peek.X;
                     Ele^.Graphics.ConnectY := XDepth[PStack.Peek.X];
                     PStack.Push(Cpos);
                     CPos.X := Ele^.Graphics.ConnectX;
                     CPos.Y := Ele^.Graphics.ConnectY;
                     PStack.Push(CPos);

          end;

          PLCIStrAnd : begin
                      PStack.Push(Cpos);
          end;

          PLCIandPop: begin
                      PStack.Pop;
          end;
          PLCIorPop : begin
                      Ele^.Graphics.PosX := CPos.X;
                      Ele^.Graphics.PosY := CPos.Y;
                      PStack.Pop;
                      Tmp := PStack.Pop;
                      Ele^.Graphics.ConnectX := Tmp.X;
                      Ele^.Graphics.ConnectY := Tmp.Y;
                      if CPos.X < Tmp.X then
                        CPos.X := Tmp.X;
          end;
          PLCIout,
          PLCIoutN : begin
                     Ele^.Graphics.PosX := GRAPH_X_WIDTH;
                     Ele^.Graphics.PosY := FindOutputPath(Cpos.X);
                     Ele^.Graphics.ConnectX := CPos.X;
                     Ele^.Graphics.ConnectY := CPos.Y;
          end;
          PLCIEnd : Exit;
        end;

        if CPos.X > CPosHigh then
          CPosHigh := CPos.X;

        Inc(I);
      end;

    finally
      if CPosHigh < FRightMargin then
        CPosHigh := FRightMargin;

      while PProgram^[RungStart].Graphics.Rung = aRung do begin
        Ele := @PProgram^[RungStart];
        case Ele^.OpCode of
          PLCIout,
          PLCIoutN : begin
            Ele^.Graphics.PosX := CPosHigh;
          end;
        end;
        Inc(RungStart);
      end;
    end;
  end;
end;


function TObjectPlcThread.FindItem(X, Y, aRung, IWidth, IHeight : Integer) : PTPlcElement;
var I : Integer;
begin
  I := FindRung(aRung);

  X := (X - InstalledPlc.X_MARGIN) div IWidth;
  Y := (Y - InstalledPlc.Y_MARGIN) div IHeight;

  Result := nil;
  while(PProgram^[I].Graphics.Rung = aRung) do begin
    if (PProgram^[I].Graphics.PosX = X) and
       (PProgram^[I].Graphics.PosY = Y) then begin
       Result := @PProgram^[I];
       Exit;
    end;
    Inc(I);
  end;
end;

procedure TPlcPaintStack.push(P : TPoint);
begin
  Points[Pointer] := p;
  Inc(Pointer);
end;

function  TPlcPaintStack.Pop : TPoint;
begin
  Dec(pointer);
  Result := Points[Pointer];
end;

function  TPlcPaintStack.IsAtTop : boolean;
begin
  Result := Pointer = 0;
end;

procedure TPlcPaintStack.Reset;
begin
  Pointer := 0;
end;

function TPlcPaintStack.Peek : TPoint;
begin
  if Pointer = 0 then
    Result := Points[0] // Error
  else
    Result := Points[Pointer - 1];
end;


function TObjectPLC.GetItemValue(Item: Pointer): Integer;
begin
  Result:= TPlcItem(Item).Value;
end;

procedure TObjectPLC.SetItemValue(Item: Pointer; Value: Integer);
begin
  TPlcItem(Item).SetValue(Value);
end;

begin
  RegisterCNCClass(TObjectPLC);
end.
