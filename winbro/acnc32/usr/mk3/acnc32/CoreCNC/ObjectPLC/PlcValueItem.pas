unit PlcValueItem;

interface

uses PlcItems, Plc32, CNC32, SysUtils, CNCTypes, Classes, Graphics, CoreCNC;

type
  TPlcEmbedValue = (
         PlcValueAdd,
         PlcValueSub,
         PlcValueMul,
         PlcValueDiv,
         PlcValueShl,
         PlcValueShr,
         PlcValueClr,
         PlcValueNeg,
         PlcValueRev,
         PlcValueLsn,
         PlcValueMsn,
         PlcValueBit,
         PlcValueAssign,
         PlcValueGT,
         PlcValueLT,
         PlcValueEQ,
         PlcValueRising,
         PlcValueFalling,
         PlcValueChanging,
         PlcValueAnd,
         PlcValueOr,
         PlcValueXOR
  );



  TPlcEmbedSet = set of TPlcEmbedValue;

type
{  TMappedDWord = packed record
    case Byte of
    0 : ( Value : Integer );
    1 : ( Bytes : array [0..3] of Byte );
  end;
  PMAppedDWord = ^TMappedDWord; }

  TPlcValueItem = class(TPLCItem)
  private
    FIsWritable : Boolean;
//    function GetSize : TIOSize; virtual;
  protected
    Inputs  : TPlcEmbedSet;
    Tag : TAbstractTag;
    LastValue : Integer;
    procedure CompileValue(Ele : PTPlcElement; St : TPlcTokenizer; Write : Boolean; aName : string);
    function DontInheritValue : Boolean; dynamic;
  public
//    property Size    : TIOSize read GetSize;
    constructor Create(St : TPlcTokenizer; aName : string); override;
    procedure   Compile(Ele : PTPlcElement; St : TPlcTokenizer; Write : boolean); override;
    procedure   Write(Ele : PTPlcElement; line : boolean); override;
    function    Read(Ele : PTPlcElement) : boolean; override;
    function    GateName(Ele : PTPlcElement) : string; override;
    procedure   GatePost; override;
    procedure   ItemProperties(Ele : PTPlcElement; SL : TStrings); override;
    property    IsWritable : Boolean read FIsWritable;
  end;

  TPlcDWordItem = class(TPlcValueItem)
  private
  public
    constructor Create(St : TPlcTokenizer; aName : string); override;
    destructor Destroy; override;
  end;

  TPlcNamedItem = class(TPlcValueItem)
  private
  public
    constructor Create(St : TPlcTokenizer; aName : string); override;
    destructor Destroy; override;
    procedure   GatePre; override;
    procedure   GatePost; override;
    class function RequiresMapping : Boolean; override;
  end;

  TPlcFloatItem = class(TPlcValueItem)
  private
    IntMultiplier: Integer;
  public
    constructor Create(St : TPlcTokenizer; aName : string); override;
    destructor Destroy; override;
    procedure   GatePre; override;
    procedure   GatePost; override;
    function DontInheritValue : Boolean; override;
    class function RequiresMapping : Boolean; override;
  end;

  TPlcNamedItemR = class(TPlcValueItem)
  public
    constructor Create(St : TPlcTokenizer; aName : string); override;
    procedure   GatePre; override;
    class function RequiresMapping : Boolean; override;
  end;

  TPlcReverseIntegerItem = class(TPlcNamedItemR)
  public
    procedure   GatePre; override;
  end;

  TPlcNumericItem = class(TPlcValueItem)
  public
    constructor Create(St : TPlcTokenizer; aName : string); override;
    procedure SetValue(val : double); override;
  end;

  TPlcDebounceItem = class(TPlcNamedItemR)
  private
    LastState : Integer;
    StabilityTime : Integer;
    ThisStabilityTime : Integer;
  public
    constructor Create(St : TPlcTokenizer; aName : string); override;
    function DontInheritValue : Boolean; override;
    procedure GatePre; override;
  end;

implementation

type
  TPlcValueCommands = array [TPlcEmbedValue] of TValuePair;

const
  PlcEmbedName : TPlcValueCommands = (
        ( value : 'Add'; reado : False ),
        ( value : 'Sub'; reado : False ),
        ( value : 'Mul'; reado : False ),
        ( value : 'Div'; reado : False ),
        ( value : 'shl'; reado : False ),
        ( value : 'shr'; reado : False ),
        ( value : 'clr'; reado : False ),
        ( value : 'Neg'; reado : False ),
        ( value : 'Rev'; reado : False ),
        ( value : 'Lsn'; reado : False ),
        ( value : 'Msn'; reado : False ),
        ( value : 'Bit'; reado : False ),
        ( value : 'Assign'; reado : False ),
        ( value : 'GT';  reado : True  ),
        ( value : 'LT';  reado : True  ),
        ( value : 'EQ';  reado : True  ),
        ( value : 'Rising'; reado : True ),
        ( value : 'Falling'; reado : True ),
        ( value : 'Changing';reado : True ),
        ( value : 'And'; reado : False ),
        ( value : 'Or'; reado : False ),
        ( value : 'XOR'; reado : False )
  );
{ Syntax
  Name Type [Optional] [InitialValue];
  Derivatives of TPlcValueItem Read the optional section before inheriting
  the initial value
}
constructor TPlcValueItem.Create(st : TPlcTokenizer; aName : string);
var token, build : string;
    code         : integer;
begin
  inherited Create(St, aName);

  FIsWritable := True;

  if St = nil then
    Exit;

  if DontInheritValue then
    Exit;

  Token := St.skipComment;
  Build := '';
  code  := St.Line;
  while (token <> ';') and (code = st.line) do begin
    build := build + token;
    token := st.skipComment;
  end;

// ???? Revisit
// 1. the initial value may be '' so exit
// 2. The initial value may be garbage so raise exception
  if token = ';' then begin
    val(build, FValue, code);
//    if code <> 0 then
//      FValue := 0;
  end else
    raise ECreateItemFail.CreateFmt(SemicolonMissing, [aName]);

  LastValue := 0;
end;

function TPlcValueItem.DontInheritValue : Boolean;
begin
  Result := False;
end;

procedure TPlcValueItem.Compile(Ele : PTPlcElement; St : TPlcTokenizer; Write : boolean);
var token : string;
begin
  if Write and not IsWritable then
    raise ECompileItemFail.CreateFmt(ItemIsReadOnly, [Name]);

  if ST.Peek <> '.' then begin
    Ele^.Embedded.OData := FindOrAddItem('0');
    Ele^.Embedded.Command := Ord(PlcValueBit);
    Exit;
  end;

  Token := St.SkipComment;

  if token <> '.' then
    raise ECompileItemFail.CreateFmt(MethodNameMissing, [name]);

  token := St.SkipComment;

  CompileValue(Ele, St, Write, Token);
end;

procedure TPlcValueItem.CompileValue(Ele : PTPlcElement; St : TPlcTokenizer; Write : Boolean; aName : string);
var I : TPlcEmbedValue;
    Token : string;
begin
  for i := Low(TPlcEmbedValue) to High(TPlcEmbedValue) do begin
    if CompareText(PlcEmbedName[i].Value, aName) = 0 then begin
      if PlcEmbedName[I].ReadO and Write then
        raise ECompileItemFail.CreateFmt(ItemIsReadOnly, [Name]);

      Ele.Embedded.Command := Ord(i);
      case I of
        PlcValueAdd,
        PlcValueSub,
        PlcValueMul,
        PlcValueDiv,
        PlcValueShl,
        PlcValueShr,
        PlcValueBit,
        PlcValueAssign,
        PlcValueGT ,
        PlcValueLT ,
        PlcValueEQ,
        PlcValueAnd,
        PlcValueOr,
        PlcValueXOR : begin
            if St.Read <> '.' then
              raise ECompileItemFail.CreateFmt(MethodNameMissing, [Name]);
            Token := St.Read;
            Ele.Embedded.OData := FindOrAddItem(Token);
        end;
      end;
      Exit;
    end;
  end;

  try
//    StrToFloat(aName);
    Ele.Embedded.Command := Ord(PlcValueBit);
    Ele.Embedded.OData := FindOrAddItem(aName);
    Exit;
  except
    raise ECompileItemFail.CreateFmt(InvalidMethodName, [aName]);
  end;
end;

procedure TPlcValueItem.Write(ele : PTPlcElement; line : boolean);
{var tmp, count, i : integer;}
begin
  with Ele.Embedded do begin
    if Line then begin
      Include(Inputs, TPlcEmbedValue(Command));
      if not Ele^.Line then begin
        case TPlcEmbedValue(Command) of
          PlcValueAdd : Inc(FValue, OData.Value);
          PlcValueSub : Dec(FValue, OData.Value);
          PlcValueMul : FValue := FValue * OData.Value;
          PlcValueDiv : with OData do
                          if Value <> 0 then
                            FValue := FValue div Value
                          else
                            FValue := 0;

          PlcValueShl : FValue := FValue shl OData.Value;
          PlcValueShr : FValue := FValue shr OData.Value;
          PlcValueNeg : FValue := not FValue;
{          PlcValueRev : begin
                        tmp := FValue;
                        FValue := 0;
                        for i := 0 to 31 do begin
                          if ((1 shl i) and tmp) <> 0 then
                            FValue := FValue or (1 shl (count - i));
                        end;
                      end; }

          PlcValueLsn : FValue := FValue and $0f;
          PlcValueMsn : begin
                        FValue := FValue and $f0;
                        FValue := FValue shr 4;
                      end;
          PlcValueAnd : FValue := FValue and OData.Value;
          PlcValueOr  : FValue := FValue or OData.Value;
          PlcValueXoR : FValue := FValue xor OData.Value;
          PlcValueAssign : FValue := OData.Value;
          PlcValueClr : FValue := 0;
        end;
      end;
    end else begin
      if TPlcEmbedValue(Command) in Inputs then begin
        Exclude(Inputs, TPlcEmbedValue(Command));
      end;
    end;

    { Force single bit outputs every sweep }
    if TPlcEmbedValue(Command) = PlcValueBit then begin
      if Line then
        FValue := FValue or (1 shl OData.Value)
      else
        FValue := FValue and not (1 shl OData.Value);
    end;
  end;
end;

function TPlcValueItem.GateName(Ele : PTPlcElement) : string;
begin
  if (Ele.Embedded.Command >= Ord(Low(TPlcEmbedValue))) and
     (Ele.Embedded.Command <= Ord(High(TPlcEmbedValue))) then begin
       Result := PlcEmbedName[TPlcEmbedValue(Ele.Embedded.Command)].Value;
       case TPlcEmbedValue(Ele^.Embedded.Command) of
         PlcValueAdd,
         PlcValueSub,
         PlcValueMul,
         PlcValueDiv,
         PlcValueShl,
         PlcValueShr,
         PlcValueBit,
         PlcValueAssign,
         PlcValueGT,
         PlcValueLT,
         PlcValueEQ,
         PlcValueAnd,
         PlcValueOr,
         PlcValueXOR : Result := Result + '.' + Ele^.Embedded.Odata.Name;

         PlcValueClr,
         PlcValueChanging,
         PlcValueRising,
         PlcValueFalling,
         PlcValueNeg,
         PlcValueRev,
         PlcValueLsn,
         PlcValueMsn : Exit;
       end;
  end else Result := '';
end;

procedure TPlcValueItem.GatePost;
begin
  inherited GatePost;
  LastValue := FValue;
end;

procedure TPlcValueItem.ItemProperties(Ele : PTPlcElement; SL : TStrings);
var I : TPlcEmbedValue;
begin
  SL.Clear;
  for I := Low(TPlcEmbedValue) to High(TPlcEmbedValue) do begin
    SL.Add(PlcEmbedName[I].Value);
    case I of
      //PlcValueBit : SL.Add(IntToHex(FValue, 8));
      PlcValueBit : SL.Add(IntToStr(FValue));
    else
      SL.Add(BooleanIdent[I in Inputs])
    end;
  end;
end;

{function TPlcValueItem.GetSize : TIOSize;
begin
  Result := Tag.Size;
end; }

function TPlcValueItem.Read(Ele : PTPlcElement) : Boolean;
begin
  case TPlcEmbedValue(Ele.Embedded.Command) of
    PlcValueBit : Result := (FValue and (1 shl Ele.Embedded.OData.Value)) <> 0;
    PlcValueGT : Result := FValue > Ele.Embedded.OData.Value;
    PlcValueLT : Result := FValue < Ele.Embedded.OData.Value;
    PlcValueEQ : Result := FValue = Ele.Embedded.OData.Value;
    PlcValueRising :  Result := FValue > LastValue;
    PlcValueFalling : Result := FValue < LastValue;
    PlcValueChanging : Result := FValue <> LastValue;
  else
    Result :=  TPlcEmbedValue(Ele.Embedded.Command) in Inputs;
  end;
end;

constructor TPlcDwordItem.Create(st : TPlcTokenizer; aName : string);
begin
  inherited Create(St, aName);
  Tag := TOrdinalTag.Create(St.TagOwner, aName);
end;

destructor TPlcDWordItem.Destroy;
begin
  Tag.Free;
  inherited Destroy;
end;

constructor TPlcNamedItem.Create(St : TPlcTokenizer; aName : string);
begin
  Tag := SysObj.Comms.IOFindTag(aName);
  if Tag = nil then
    raise ECreateItemFail.CreateFmt(NameNotInIOSystem, [aName]);

  inherited Create(St, aName);

  if (not Tag.IsOwned) and Tag.IsWritable then begin
    Tag.IsOwned := True;
//    Tag.AsInteger := 0;
  end else
    FIsWritable := False;
end;

class function TPlcNamedItem.RequiresMapping : Boolean;
begin
  Result := True;
end;

destructor TPlcNamedItem.Destroy;
begin
  if IsWritable then
    Tag.IsOwned := False;

  inherited Destroy;
end;


procedure   TPlcNamedItem.GatePre;
begin
  FValue := Tag.AsInteger;
end;


constructor TPlcDebounceItem.Create(St : TPlcTokenizer; aName : string);
begin
  inherited Create(St, aName);
  StabilityTime := St.ReadInteger;
  if St.Read <> ';' then
    raise ECreateItemFail.CreateFmt(SemicolonMissing, [aName]);
end;

function TPlcDebounceItem.DontInheritValue : Boolean;
begin
  Result := True;
end;

procedure TPlcDebounceItem.GatePre;
var Tmp : Integer;
begin
  Tmp := Tag.AsInteger;
  if Tmp = LastState then
    Inc(ThisStabilityTime)
  else begin
    LastState := Tmp;
    ThisStabilityTime := 0;
  end;

  if ThisStabilityTime > StabilityTime then
    FValue := Tmp;
end;


procedure   TPlcNamedItem.GatePost;
begin
  inherited GatePost;
  if IsWritable and (Value <> Tag.AsInteger) then
    TMethodTag(Tag).AsInteger := FValue;
end;

constructor TPlcNamedItemR.Create(St : TPlcTokenizer; aName : string);
begin
  Tag := SysObj.Comms.IOFindTag(aName);
  if Tag = nil then
    raise ECreateItemFail.CreateFmt(NameNotInIOSystem, [aName]);

  inherited Create(St, aName);
  FIsWritable := False;
end;

class function TPlcNamedItemR.RequiresMapping : Boolean;
begin
  Result := True;
end;


procedure TPlcNamedItemR.GatePre;
begin
  FValue := Tag.AsInteger;
end;

constructor TPlcNumericItem.Create(St : TPlcTokenizer; aName : string);
begin
  inherited Create(St, aName);

  FIsWritable:= False;
end;

procedure TPlcNumericItem.SetValue(val : double);
begin
  FValue := Round(val);
end;

{ TPlcFloatItem }

constructor TPlcFloatItem.Create(St: TPlcTokenizer; aName: string);
var Tmp: string;
begin
  Tag:= SysObj.Comms.FindTag(aName);
  if not Assigned(Tag) then
    Tag:= SysObj.Comms.TagEvent(aName, EdgeChange, TDoubleTag, nil);
  inherited;

  Tmp:= St.Read;
  if Tmp <> ';' then begin
    try
      IntMultiplier:= StrToInt(Tmp);
    except
      raise ECreateItemFail.CreateFmt(DataConversionError, [aName]);
    end;

    if IntMultiplier = 0 then
      IntMultiplier:= 10000;

    if St.Read <> ';' then
      raise ECreateItemFail.CreateFmt(SemicolonMissing, [aName]);
  end else begin
    IntMultiplier:= 10000;
  end;
end;

destructor TPlcFloatItem.Destroy;
begin
  inherited;
end;

function TPlcFloatItem.DontInheritValue: Boolean;
begin
  Result:= True;
end;

procedure TPlcFloatItem.GatePost;
begin
  if LastValue <> Value then
    Tag.AsDouble:= Value / IntMultiplier;
end;

procedure TPlcFloatItem.GatePre;
begin
  Value:= Round(Tag.AsDouble * IntMultiplier);
  LastValue:= Value;
end;

class function TPlcFloatItem.RequiresMapping: Boolean;
begin
  Result:= False;
end;

{ TPlcReverseIntegerItem }

procedure TPlcReverseIntegerItem.GatePre;
var A, B, V: Integer;
begin
  V:= Tag.AsInteger;
  A:= V and $ff;
  B:= (V and $ff00) shr 8;
  Value:= A * 256 + B;
end;

initialization
  RegisterPlcItemClass('Numeric', TPlcNumericItem);
  RegisterPlcItemClass('IO', TPlcNamedItem);
  RegisterPlcItemClass('IOR', TPlcNamedItemR);
  RegisterPlcItemClass('Item', TPlcDWordItem);
  RegisterPlcItemClass('Debounce', TPlcDebounceItem);
  RegisterPlcItemClass('Float', TPlcFloatItem);
  RegisterPlcItemClass('ReverseWord', TPlcReverseIntegerItem);
end.
