unit PlcStateMachine;

interface

uses PlcItems, Plc32, CNC32, SysUtils, CNCTypes, Classes, Graphics, NCMath,
     CoreCNC;

type
  TPlcStateMachineValue = (
    psmIndex,
    psmState,
    psmInc,
    psmDec
  );

  TPlcStateMachineValues = set of TPlcStateMachineValue;



  TPlcStateMachine = class(TPlcItem)
  private
    Tag : TOrdinalTag;
    States : TList;
    FIndex : Integer;
    Value : TPlcStateMachineValues;
  public
    constructor Create(st : TPlcTokenizer; aName : string); override;
    destructor  Destroy; override;
    procedure   Compile(ele : PTPlcElement; st : TPlcTokenizer; Write : boolean); override;
    procedure   Write(ele : PTPlcElement; line : boolean); override;
    function    Read(ele : PTPlcElement) : boolean; override;
    function    GateName(ele : PTPlcElement) : string; override;
    procedure   GatePost; override;
    procedure   ItemProperties(Ele : PTPlcElement; SL : TStrings); override;
    class function RequiresMapping : Boolean; override;
  end;

implementation

const
  PlcStateCommands : array[TPlcStateMachineValue] of TValuePair = (
        ( Value : 'Index'; ReadO : False ),
        ( Value : 'State'; ReadO : True  ),
        ( Value : 'Inc';   ReadO : False ),
        ( Value : 'Dec';   ReadO : False )
  );

constructor TPlcStateMachine.Create(st : TPlcTokenizer; aName : string);
begin
  inherited Create(St, aName);

  Tag := TOrdinalTag(SysObj.IO.IOFindTag(aName));
  if Tag <> nil then begin
//    raise ECreateItemFail.CreateFmt(NameNotInIOSystem, [aName]);

    if TAbstractTag(Tag).IsOwned or not Tag.IsWritable then begin
      Tag := nil;
      raise ECreateItemFail.CreateFmt(ItemIsReadOnly, [Name]);
    end;
    Tag.IsOwned := True;
    Tag.AsInteger := 0;
  end;

  States := TList.Create;

  while St.Read <> ';' do begin
    St.Push;
    States.Add(Pointer(St.ReadInteger));
  end;
end;

class function TPlcStateMachine.RequiresMapping : Boolean;
begin
  Result := True;
end;

destructor TPlcStateMachine.Destroy;
begin
  States.Free;
  if Assigned(Tag) then
    Tag.IsOwned := False;
  inherited Destroy;
end;

procedure TPlcStateMachine.Compile(Ele : PTPlcElement; St : TPlcTokenizer; Write : Boolean);
var I : TPlcStateMachineValue;
begin
  inherited Compile(Ele, St, Write);

  if St.Read <> '.' then
    raise ECompileItemFail.CreateFmt(MethodNameMissing, [Name]);

  St.Read;

  for I := Low(TPlcStateMachineValue) to High(TPlcStateMachineValue) do begin
    if CompareText(PlcStateCommands[I].Value, ST.LastToken) = 0 then begin
      Ele^.Embedded.Command := Ord(I);
      case I of
        psmState : if Write then
                     raise ECompileItemFail.CreateFmt(ItemIsReadOnly, [Name]);
        psmInc   ,
        psmDec   : Exit;
        psmIndex : begin
          if St.Read <> '.' then
            raise ECompileItemFail.CreateFmt(MethodNameMissing, [Name]);
          Ele^.Embedded.OData := FindOrAddItem(St.Read);
        end;
      end;
      Exit;
    end;
  end;
end;

procedure TPlcStateMachine.Write(ele : PTPlcElement; line : boolean);
begin
  if Line and not Ele^.Line then begin
    if Line then begin
      Include(Value, TPlcStateMachineValue(Ele.Embedded.Command));
      case TPlcStateMachineValue(Ele.Embedded.Command) of
        psmIndex :FIndex := Ele^.Embedded.OData.Value;
        psmInc   : Inc(FIndex);
        psmDec   : Dec(FIndex);
      else
        FIndex := 0;
      end;
      FIndex := Abs(FIndex) mod States.Count;
    end else
      Exclude(Value, TPlcStateMachineValue(Ele.Embedded.Command))
  end;
end;

function TPlcStateMachine.Read(Ele : PTPlcElement) : Boolean;
begin
  Result := TPlcStateMachineValue(Ele.Embedded.Command) in Value;
end;


function    TPlcStateMachine.GateName(ele : PTPlcElement) : string;
begin
  Result := PlcStateCommands[TPlcStateMachineValue(Ele^.Embedded.Command)].Value
end;

procedure   TPlcStateMachine.GatePost;
begin
  FValue := Integer(States[FIndex]);
  if Assigned(Tag) then
    Tag.AsInteger := FValue;
end;

procedure   TPlcStateMachine.ItemProperties(Ele : PTPlcElement; SL : TStrings);
var I : Integer;
begin
  Sl.Clear;
  for I := 0 to States.Count - 1 do begin
    SL.Add(Format('Index %d', [I]));
    SL.Add(Format('%x', [Integer(States[I])] ));
  end;
end;


initialization
  RegisterPlcItemClass('StateMachine', TPlcStateMachine);
end.