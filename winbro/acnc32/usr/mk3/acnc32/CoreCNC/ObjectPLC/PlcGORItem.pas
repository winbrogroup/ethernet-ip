unit PlcGORItem;

interface

uses PlcItems, Plc32, CNC32, SysUtils, CNCTypes, Classes, Graphics, CoreCNC,
     PlcValueItem, PlcTextItem, SyncObjs;

type

  TRequestorValue = (
    rvInhibit,
    rvRequest,
    rvReset,
    rvExec,
    rvInh,
    rvBlocked
  );
  TRequestorValues = set of TRequestorValue;

  TInhibitorValue = (
    ivInhibit,
    ivInh
  );
  TInhibitorValues = set of TInhibitorValue;


  TPlcGORItem = class(TPlcItem)
  protected
    XList: TList;
    RList: TList;
    XLock: TCriticalSection;
    Title: string;
    Parent: TPlcGorItem;
    procedure SetBit(Val: Boolean; Item: TPlcItem; List: TList);
    procedure SetInhibit(Val: Boolean; Item: TPlcItem);
    procedure RemoveText(Txt: TPlcTextItem); virtual; abstract;
    procedure ORList(SL: TStrings); virtual;
    function Inhibited: Boolean; virtual;
    function GetActiveInhibits(Separator: string): string;
    function GetActiveRequests(Separator: string): string;
  public
    constructor Create(St : TPlcTokenizer; aName : string); override;
    destructor Destroy; override;
  end;

  TPlcInhibitORItem = class(TPlcGORItem)
  private
    Inputs: TInhibitorValues;
  protected
    procedure RemoveText(Txt: TPlcTextItem); override;
  public
    procedure Compile(Ele : PTPlcElement; St : TPlcTokenizer; Write : boolean); override;
    procedure Write(Ele : PTPlcElement; line : boolean); override;
    function Read(Ele : PTPlcElement) : boolean; override;
    function GateName(Ele : PTPlcElement) : string; override;
    procedure ItemProperties(Ele : PTPlcElement; SL : TStrings); override;
  end;

  TPlcRequestORItem = class(TPlcGORItem)
  private
    Inputs: TRequestorValues;
    procedure SetFault; virtual;
    procedure FaultReset; virtual;
    procedure SetRequest(Val: Boolean; Item: TPlcItem);
  protected
    procedure RemoveText(Txt: TPlcTextItem); override;
    procedure ORList(SL: TStrings); override;
  public
    constructor Create(St : TPlcTokenizer; aName : string); override;
    procedure Compile(Ele : PTPlcElement; St : TPlcTokenizer; Write : boolean); override;
    procedure Write(Ele : PTPlcElement; line : boolean); override;
    function Read(Ele : PTPlcElement) : boolean; override;
    function GateName(Ele : PTPlcElement) : string; override;
    procedure GatePost; override;
    procedure ItemProperties(Ele : PTPlcElement; SL : TStrings); override;
  end;

  TPlcRequestORItemE = class(TPlcRequestORItem)
  private
    FID: FHandle;
    procedure FaultReset; override;
    procedure SetFault; override;
    function ResetFault(FID: FHandle): Boolean;
  protected
  end;

  { Functions of GORList do not require locking as they are being called from the
    Foreground thread while the PLC is not running }
  TGORList = class
  private
    List: TList;
    Problem: Integer;
  public
    constructor Create;
    procedure AddInhibit(Item: TPlcGORItem);
    procedure RemoveInhibit(Item: TPlcGORItem);
    procedure RemoveText(Txt: TPlcTextItem);
    function FindGorItem(AName: string): TPlcGORItem;
  end;

var
  GORList: TGORList;


implementation

const
  RequestorNames : array [TRequestORValue] of TValuePair = (
        ( value : 'Inhibit'; Reado : False ),
        ( value : 'Request'; Reado : False ),
        ( value : 'Reset'; Reado : False ),
        ( value : 'Exec'; Reado : True ),
        ( value : 'Inh'; Reado : True ),
        ( value : 'Blocked'; Reado : True )
  );

  InhibitorNames : array [TInhibitORValue] of TValuePair = (
        ( value : 'Inhibit'; Reado : False ),
        ( value : 'Inh'; Reado : True )
  );

{ TPlcInhibitItem }

procedure TPlcRequestORItem.Write(Ele: PTPlcElement; line: boolean);
begin
  if Line <> Ele^.Line then begin
    case TRequestORValue(Ele^.Embedded.Command) of
      rvInhibit : begin
        if Line then begin
          SetInhibit(True, Ele.Embedded.OData)
        end
        else begin
          SetInhibit(False, Ele.Embedded.OData);
        end;
        if Inhibited then begin
          Include(Inputs, rvInhibit);
        end
        else begin
          Exclude(Inputs, rvInhibit);
        end;
      end;

      rvRequest: begin
        if Line then begin
          SetRequest(True, Ele.Embedded.OData)
        end
        else begin
          SetRequest(False, Ele.Embedded.OData);
        end;
        if RList.Count > 0 then begin
          Include(Inputs, rvRequest);
        end
        else begin
          Exclude(Inputs, rvRequest);
        end;
      end;

      rvReset: begin
        if Line then
          FaultReset;
      end;
    end;
  end;
end;

constructor TPlcRequestORItem.Create(St: TPlcTokenizer; aName: string);
begin
  inherited;
end;


procedure TPlcRequestORItem.Compile(Ele: PTPlcElement; St: TPlcTokenizer;
  Write: boolean);
var
  Token: string;
  I: TRequestORValue;
begin
  if St.Read <> '.' then
    raise ECompileItemFail.CreateFmt(MethodNameMissing, [name]);

  Token := St.SkipComment;

  for I := Low(I) to High(I) do begin
    if CompareText(RequestorNames[I].Value, Token) = 0 then begin
      if RequestorNames[I].ReadO and Write then
        raise ECompileItemFail.CreateFmt(ItemIsReadOnly, [Name]);
      Ele^.Embedded.Command := Ord(I);

      if (I = rvInhibit) or (I = rvRequest) then begin
        if St.Read <> '.' then
          raise ECompileItemFail.CreateFmt(MethodNameMissing, [Name]);
        Token := St.Read;
        Ele.Embedded.OData := FindOrAddItem(Token);
        if not (Ele.Embedded.OData is TPlcTextItem) then
          raise ECompileItemFail.CreateFmt('Only text items may be attached to an interlock [%s]', [Name]);
      end;
      Exit;
    end;
  end;
  raise ECompileItemFail.CreateFmt('Unknown property name: ', [Token]);
end;

function TPlcRequestORItem.GateName(Ele: PTPlcElement): string;
begin
  if Ele^.Embedded.Command <> -1 then begin
    if (TRequestORValue(Ele^.Embedded.Command) >= Low(TRequestORValue)) and
       (TRequestORValue(Ele^.Embedded.Command) <= High(TRequestORValue)) then
       Result := RequestorNames[TRequestORValue(Ele^.Embedded.Command)].Value
    else
      Result := '?';
  end;
end;

procedure TPlcRequestORItem.ItemProperties(Ele: PTPlcElement; SL: TStrings);
var I : TRequestORValue;
begin
  SL.Clear;
  for I := Low(I) to High(I) do begin
    SL.Add(RequestorNames[I].Value);
    SL.Add(BooleanIdent[I in Inputs]);
  end;
  ORList(SL);
end;

function TPlcRequestORItem.Read(Ele: PTPlcElement): boolean;
begin
  Result := TRequestORValue(Ele^.Embedded.Command) in Inputs
end;


procedure TPlcRequestORItem.SetFault;
var Tmp: string;
    Tmp1: string;
begin
  try
    Tmp := GetActiveInhibits(CarRet);
    Tmp1:= GetActiveRequests(' ');
    SysObj.FaultInterface.SetFault(TCNC(PlcThread.TagOwner), PlcFaults[plcfItemInterlock], [Title, Tmp, Tmp1], nil);
  except
    on E: Exception do
      SysObj.FaultInterface.SetFault(TCNC(PlcThread.TagOwner), PlcFaults[plcfInternalError], [Title, E.Message], nil);
  end;
end;


procedure TPlcRequestORItem.ORList(SL: TStrings);
var I: Integer;
begin
  XLock.Enter;
  try
    for I:= 0 to RList.Count - 1 do begin
      SL.Add('*Request');
      SL.Add(TPlcTextItem(RList[I]).Text);
    end;
    inherited ORList(SL);
  finally
    XLock.Leave;
  end;
end;

procedure TPlcRequestORItem.SetRequest(Val: Boolean; Item: TPlcItem);
begin
  SetBit(Val, Item, RList);
end;

procedure TPlcRequestORItem.RemoveText(Txt: TPlcTextItem);
begin
  SetInhibit(False, Txt);
  SetRequest(False, Txt);
end;

{ TGORList }

constructor TGORList.Create;
begin
  Self.List:= TList.Create;
end;

procedure TGORList.RemoveInhibit(Item: TPlcGORItem);
var I: Integer;
begin
  I:= List.IndexOf(Item);
  if I <> -1 then
    List.Delete(I)
  else
    Problem:= Problem + 1;

end;

procedure TGORList.AddInhibit(Item: TPlcGORItem);
var I: Integer;
begin
  I:= List.IndexOf(Item);
  if I = -1 then
    List.Add(Item)
  else
    Problem:= Problem + 1;

end;

procedure TGORList.RemoveText(Txt: TPlcTextItem);
var I: Integer;
begin
  for I:= 0 to List.Count - 1 do begin
    TPlcGORItem(List[I]).RemoveText(Txt);
  end;
end;

function TGORList.FindGorItem(AName: string): TPlcGORItem;
var I: Integer;
begin

  for I:= 0 to List.Count - 1 do begin
    if CompareText(TPlcGORItem(List[I]).Name, AName) = 0 then begin
      Result:= TPlcGORItem(List[I]);
      Exit;
    end;
  end;
  Result:= nil;
end;

{ TPlcGORItem }

constructor TPlcGORItem.Create(St: TPlcTokenizer; aName: string);
var Tmp: string;
begin
  inherited;
  XList:= TList.Create;
  RList:= TList.Create;
  XLock:= TCriticalSection.Create;
  GORList.AddInhibit(Self);
  Title:= AName;
  Tmp:= SysObj.Localized.GetString( St.Read );
  if Tmp = ';' then
    Exit;

  Title:= Tmp;

  Tmp:= St.Read;
  if Tmp = ';' then
    Exit;

  Parent:= GORList.FindGorItem(Tmp);
  if Parent = nil then
    raise ECompileItemFail.CreateFmt('Cannot find parent inhibit [%s]', [AName]);

  St.ExpectedToken(';');
end;

destructor TPlcGORItem.Destroy;
begin
  GORList.RemoveInhibit(Self);
  if Assigned(XList) then
    XList.Free;
  if Assigned(RList) then
    RList.Free;
  if Assigned(XLock) then
    XLock.Free;
  inherited;
end;

procedure TPlcGORItem.SetInhibit(Val: Boolean; Item: TPlcItem);
begin
  SetBit(Val, Item, XList);
end;

procedure TPlcGORItem.SetBit(Val: Boolean; Item: TPlcItem; List: TList);
var I: Integer;
begin
  if Item is TPlcTextItem then begin
    XLock.Enter;
    try
      if Val then begin
        List.Add(Item);
      end
      else begin
        I:= List.IndexOf(Item);
        if I <> -1 then
          List.Delete(I);
      end;
    finally
      XLock.Leave;
    end;
  end;
end;




procedure TPlcGORItem.ORList(SL: TStrings);
var I: Integer;
begin
  for I:= 0 to XList.Count - 1 do begin
    SL.Add('*Inhibit: ' + Title);
    SL.Add(TPlcTextItem(XList[I]).Text);
  end;

  if Parent <> nil then
    Parent.ORList(SL);
end;

function TPlcGORItem.Inhibited: Boolean;
begin
  try
    if Parent <> nil then
      Result:= Parent.Inhibited or (XList.Count > 0)
    else 
      Result:= XList.Count > 0;
  except
    Result:= True;
  end;
end;

function TPlcGORItem.GetActiveInhibits(Separator: string): string;
var I: Integer;
begin
  try
    if Parent <> nil then
      Result:= Parent.GetActiveInhibits(Separator)
    else
      Result:= Separator;
    for I:= 0 to XList.Count - 1 do begin
      Result:= Result + TPlcTextItem(XList[I]).Text + Separator;
    end;
  except
    Result:= '';
  end;
end;

function TPlcGORItem.GetActiveRequests(Separator: string): string;
var I: Integer;
begin
  try
    if Parent <> nil then
      Result:= Parent.GetActiveRequests(Separator)
    else
      Result:= Separator;
    for I:= 0 to RList.Count - 1 do begin
      Result:= Result + TPlcTextItem(RList[I]).Text + Separator;
    end;
  except
    Result:= '';
  end;
end;

{ TPlcInhibitORItem }

procedure TPlcInhibitORItem.Compile(Ele: PTPlcElement; St: TPlcTokenizer;
  Write: boolean);
var
  Token: string;
  I: TInhibitORValue;
begin
  if St.Read <> '.' then
    raise ECompileItemFail.CreateFmt(MethodNameMissing, [name]);

  Token := St.SkipComment;

  for I := Low(I) to High(I) do begin
    if CompareText(InhibitorNames[I].Value, Token) = 0 then begin
      if InhibitorNames[I].ReadO and Write then
        raise ECompileItemFail.CreateFmt(ItemIsReadOnly, [Name]);
      Ele^.Embedded.Command := Ord(I);

      if I = ivInhibit then begin
        if St.Read <> '.' then
          raise ECompileItemFail.CreateFmt(MethodNameMissing, [Name]);
        Token := St.Read;
        Ele.Embedded.OData := FindOrAddItem(Token);
        if not (Ele.Embedded.OData is TPlcTextItem) then
          raise ECompileItemFail.CreateFmt('Only text items may be attached to an inhibit [%s]', [Name]);
      end;
      Exit;
    end;
  end;
  raise ECompileItemFail.CreateFmt('Unknown property name: ', [Token]);
end;

function TPlcInhibitORItem.GateName(Ele: PTPlcElement): string;
begin
  if Ele^.Embedded.Command <> -1 then begin
    if (TInhibitORValue(Ele^.Embedded.Command) >= Low(TInhibitORValue)) and
       (TInhibitORValue(Ele^.Embedded.Command) <= High(TInhibitORValue)) then
       Result := InhibitorNames[TInhibitORValue(Ele^.Embedded.Command)].Value
    else
      Result := '?';
  end;
end;

procedure TPlcInhibitORItem.ItemProperties(Ele: PTPlcElement; SL: TStrings);
var I: TInhibitORValue;
begin
  try
  SL.Clear;
  for I := Low(I) to High(I) do begin
    SL.Add(InhibitorNames[I].Value);
    SL.Add(BooleanIdent[I in Inputs]);
  end;
  ORList(SL);
  except
    SL.Clear;
  end;
end;

function TPlcInhibitORItem.Read(Ele: PTPlcElement): boolean;
begin
  Result := TInhibitORValue(Ele^.Embedded.Command) in Inputs
end;

procedure TPlcInhibitORItem.RemoveText(Txt: TPlcTextItem);
begin
  SetInhibit(False, Txt);
end;


procedure TPlcInhibitORItem.Write(Ele: PTPlcElement; line: boolean);
begin
  if Line <> Ele^.Line then begin
    case TRequestORValue(Ele^.Embedded.Command) of
      rvInhibit : begin
        if Line then begin
          SetInhibit(True, Ele.Embedded.OData)
        end
        else begin
          SetInhibit(False, Ele.Embedded.OData);
        end;
        if XList.Count > 0 then begin
          Include(Inputs, ivInhibit);
        end
        else begin
          Exclude(Inputs, ivInhibit);
        end;
      end;
    end;
  end;
end;

{ TPlcRequestORItemE }

procedure TPlcRequestORItemE.FaultReset;
begin
  if FID <> 0 then
    SysObj.FaultInterface.FaultResetID(FID);
  FID:= 0;
end;

function TPlcRequestORItemE.ResetFault(FID: FHandle): Boolean;
begin
  Result:= False;
end;

procedure TPlcRequestORItemE.SetFault;
var Tmp: string;
    Tmp1: string;
begin
  try
    if FID <> 0 then
      SysObj.FaultInterface.FaultResetID(FID);

    Tmp := GetActiveInhibits(' ');
    Tmp1:= GetActiveRequests(' ');
    FID:= SysObj.FaultInterface.SetFault(TCNC(PlcThread.TagOwner), PlcFaults[plcfItemError], [Title, Tmp, Tmp1], ResetFault);
  except
    On E: Exception do
      FID:= SysObj.FaultInterface.SetFault(TCNC(PlcThread.TagOwner), PlcFaults[plcfInternalError], [Title, E.Message], ResetFault);
  end;
end;

procedure TPlcRequestORItem.FaultReset;
begin

end;

procedure TPlcRequestORItem.GatePost;
begin
  if Inhibited and (rvRequest in Inputs) then begin
    Exclude(Inputs, rvExec);
    Include(Inputs, rvBlocked);
    Include(Inputs, rvInh);
    SetFault;
  end
  else if not Inhibited and (rvRequest in Inputs) then begin
    Include(Inputs, rvExec);
    Exclude(Inputs, rvBlocked);
    Exclude(Inputs, rvInh);
  end
  else if Inhibited and not (rvRequest in Inputs) then begin
    Exclude(Inputs, rvExec);
    Exclude(Inputs, rvBlocked);
    Include(Inputs, rvInh);
  end
  else begin
    Exclude(Inputs, rvExec);
    Exclude(Inputs, rvBlocked);
    Exclude(Inputs, rvInh);
  end;
end;

initialization
  GORList:= TGORList.Create;
  RegisterPlcItemClass('RequestOR', TPlcRequestORItem);
  RegisterPlcItemClass('RequestORE', TPlcRequestORItemE);
  RegisterPlcItemClass('InhibitOR', TPlcRequestORItem);
end.
