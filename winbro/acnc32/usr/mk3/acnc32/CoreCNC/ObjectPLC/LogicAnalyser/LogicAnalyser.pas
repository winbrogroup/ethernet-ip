unit LogicAnalyser;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  CoreCNC, CNC32, CNCTypes, Plc32, PlcItems, StdCtrls, Buttons, ExtCtrls,
  SelectTagForm, Spin, Menus;

const
  MAX_RINGBUFFER_SIZE = 1000;

type
  TRingBuffer = class(TObject)
  private
    Head : Integer;
    Wrapped : Boolean;
    Data : array [0..MAX_RINGBUFFER_SIZE - 1] of Integer;
    function GetValue(Index : Integer) : Integer;
  public
    constructor Create;
    destructor Destroy; override;
    function Count : Integer;
    procedure Add(Value : Integer);
    procedure Clear;
    property Value[Index : Integer] : Integer read GetValue;
  end;

  TLogicAnalyserTrace = class(TObject)
  private
    Data : TRingBuffer;
    FTag : TAbstractTag;
    LastValue : Boolean;
    FStartTick : Int64;
    FMask : Cardinal;
    procedure SetMask(aMask : Cardinal);
  public
    constructor Create(aTag : TAbstractTag; aMask : Integer);
    procedure CheckForNewEvent;
    property StartTick : Int64 read FStartTick;
    function Transitions : Integer;
    property Mask : Cardinal read FMask write SetMask;
    procedure Paint(Canvas : TCanvas; Left, Top, Width, Height : Integer; MSPerPixel : Double; Offset : Int64);
    property Tag : TAbstractTag read FTag;
  end;

  TLogicAnalyser = class(TObject)
  private
    FMSPerPixel : Double;
    Offset : Int64;
    FFrozen : Boolean;
    FreezeTime, FFrozenFor : Int64;
    DeleteTrace : Integer;
    FDisplayTime : Double;
    FTPU : Double; // Time Per Unit
    ActiveTrace : Integer;
    function GetTrace(Index : Integer) : TLogicAnalyserTrace;
    procedure SetFrozen(aFrozen : Boolean);
    procedure SetMSPerPixel(aMSPerPixel : Double);
  public
    Traces : TList;
    constructor Create;
    procedure UpdateTrace;
    procedure AddTag(aTag : TAbstractTag; aMask : Integer);
    procedure Paint(Canvas : TCanvas; Left, Top, Width, Height : Integer);
    property Trace[Index : Integer] : TLogicAnalyserTrace read GetTrace; default;
    property Frozen : Boolean read FFrozen write SetFrozen;
    property DisplayTime : Double read FDisplayTime;
    property FrozenFor : Int64 read FFrozenFor;
    property MSPerPixel : Double read FMSPerPixel write SetMSPerPixel;
    property TPU : Double read FTPU;
  end;

  TLogicAnalyserForm = class(TForm)
    Panel1: TPanel;
    GraphArea: TPaintBox;
    Timer1: TTimer;
    CPanel: TPanel;
    BtnFreeze: TSpeedButton;
    BitBtn1: TBitBtn;
    SpeedButton1: TSpeedButton;
    Timer2: TTimer;
    LabelTPU: TLabel;
    LabelAT: TLabel;
    Panel2: TPanel;
    btnScaleMinus: TSpeedButton;
    Label2: TLabel;
    LabelScale: TLabel;
    BtnScalePlus: TSpeedButton;
    Panel3: TPanel;
    BtnOffsetMinus: TSpeedButton;
    LabelOffset: TLabel;
    Label3: TLabel;
    BtnOffsetPlus: TSpeedButton;
    BtnReset: TSpeedButton;
    procedure BtnResetClick(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure GraphAreaMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FormDestroy(Sender: TObject);
    procedure BtnFreezeClick(Sender: TObject);
    procedure btnScaleMinusClick(Sender: TObject);
    procedure BtnScalePlusClick(Sender: TObject);
    procedure BtnOffsetMinusClick(Sender: TObject);
    procedure BtnOffsetPlusClick(Sender: TObject);
    procedure BtnMouseDown(Sender: TObject;
      Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure BtnMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Timer2Timer(Sender: TObject);
    procedure GraphAreaMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
  private
    LogicAnalyser : TLogicAnalyser;
    Bmap : TBitMap;
    MaskPopup : TPopupMenu;
    EditingTrace : Integer;
    Acceleration : Integer;
    procedure GraphPaint;
    procedure DoMaskChange(Sender : TObject);
    procedure DoDeleteTrace(Sender : TObject);
    procedure UpdateTotalTime;
  public
    class procedure Execute(aLogicAnalyser : TLogicAnalyser);
  end;


implementation

const MaxDataStorage = 1000;
{$R *.DFM}

var
  LogicAnalyserForm: TLogicAnalyserForm;


constructor TLogicAnalyserTrace.Create(aTag : TAbstractTag; aMask : Integer);
begin
  inherited Create;
  Data := TRingBuffer.Create;
  FTag := aTag;
  Mask := aMask;
end;

procedure TLogicAnalyserTrace.SetMask(aMask : Cardinal);
begin
  Data.Clear;
  FMask := aMask;
  FStartTick := CNC32.PlcTick.I64;
  LastValue := (Tag.AsInteger and Mask) <> 0;
end;

procedure TLogicAnalyserTrace.CheckForNewEvent;
var Tmp : Boolean;
//    ITmp : Int64;
//    I : Integer;
begin
  Tmp := (Tag.AsInteger and Mask) <> 0;
  if Tmp <> LastValue then begin
    LastValue := Tmp;
    Data.Add(CNC32.PlcTick.I64 - StartTick);
{    if Data.Count > MaxDataStorage then begin
      ITmp := 0;
      while Data.Count > MaxDataStorage do begin
        ITmp := ITmp + Cardinal(Data[0]);
        Data.Delete(0);
      end;
      FStartTick := StartTick + ITmp;
      for I := 0 to Data.Count - 1 do begin
        Data[I] := Pointer(Cardinal(Data[I]) - ITmp);
      end;
    end; }
  end;
end;

function TLogicAnalyserTrace.Transitions : Integer;
begin
  Result := Data.Count;
end;

procedure TLogicAnalyserTrace.Paint(Canvas : TCanvas; Left, Top, Width, Height : Integer; MSPerPixel : Double; Offset : Int64);
var I : Integer;
    DisplayStart, Tmp : Int64;
    Value : Boolean;

  function DisplayY(aOn : Boolean) : Integer;
  begin
  // Divide the height into 6 parts
  // 1/6th top border
  // 4/6th 0 to 1 drawing
  // 1/6th bottom border
    if aOn then
      Result := Top + Height div 6
    else
      Result := Top + (Height * 5) div 6;
  end;

  function DisplayX(aTime : Int64) : Integer;
  begin
    Result := Round((aTime - DisplayStart) / MSPerPixel) + Left
  end;

begin
  DisplayStart := CNC32.PlcTick.I64 - Round((Width * MSPerPixel) - Offset);

  Value := LastValue;
  Canvas.MoveTo(Left + Width, DisplayY(Value));

  for I := Data.Count - 1 downto 0 do begin
    Tmp := Cardinal(Data.Value[I]) + StartTick;
    if Tmp > (CNC32.PlcTick.I64 + Offset) then begin
      Canvas.MoveTo(Left + Width, DisplayY(not Value))

    end else if Tmp < DisplayStart then begin
      Canvas.LineTo(Left, DisplayY(Value));
      Exit

    end else begin
      Canvas.LineTo(DisplayX(Tmp), DisplayY(Value));
      Canvas.LineTo(DisplayX(Tmp), DisplayY(not Value));
    end;

    Value := not Value;
  end;
end;

procedure TLogicAnalyserForm.GraphPaint;
begin
  with Bmap do
    LogicAnalyser.Paint(Canvas, 0, 0, Width, Height);

  GraphArea.Canvas.Draw(0, 0, Bmap);
  UpdateTotalTime;
end;

procedure TLogicAnalyser.UpdateTrace;
var I : Integer;
begin
  for I := 0 to Traces.Count - 1 do begin
    Trace[I].CheckForNewEvent;
  end;

  if DeleteTrace <> -1 then begin
    Trace[DeleteTrace].Free;
    Traces.Delete(DeleteTrace);
    DeleteTrace := -1;
    ActiveTrace := 0;
  end;
end;

constructor TLogicAnalyser.Create;
begin
  Traces := TList.Create;
  MSPerPixel := 50;
  Offset := 0;
  DeleteTrace := -1;
end;

procedure TLogicAnalyser.Paint(Canvas : TCanvas; Left, Top, Width, Height : Integer);
var IH, I, ThisTop : Integer;
    ThisOffset : Int64;
    Spacing, CheckDisp : Double;
const LMargin = 0; //100;
      Border = 3;
begin
  if Frozen then begin
    ThisOffset := Offset + (FreezeTime - CNC32.PlcTick.I64);
    FFrozenFor := CNC32.PlcTick.I64 - FreezeTime;
  end else begin
    ThisOffset := Offset;
    FFrozenFor := 0;
  end;

  Canvas.Brush.Color := clBtnFace;
  Canvas.Rectangle(Left, Top, Width + Left, Height + Top);

  FDisplayTime := (Width - LMargin) * (MSPerPixel / 1000);

  CheckDisp := 2;
  while CheckDisp < (DisplayTime  / 2) do
    CheckDisp := CheckDisp * 10;

  if CheckDisp > DisplayTime then
    Spacing := CheckDisp / 40
  else
    Spacing := CheckDisp / 20;

  Spacing := (Spacing * 1000) / MSPerPixel;

  FTPU := Spacing * MSPerPixel / 1000;

  if Spacing > 0 then begin
    Canvas.Pen.Color := clBlue;
    for I := 0 to Round((Width - LMargin) / Spacing) do begin
      Canvas.MoveTo(Width - Round(I * Spacing), 0);
      Canvas.LineTo(Width - Round(I * Spacing), Height);
    end;
  end;

  if Traces.Count > 0 then begin
    IH := Height div Traces.Count;
    for I := 0 to Traces.Count - 1 do begin
      ThisTop := Top + (IH * I);
      Canvas.Pen.Color := clBlack;
      Canvas.MoveTo(0, ThisTop);
      Canvas.LineTo(Width, ThisTop);
      with Trace[I] do begin
        if I = ActiveTrace then begin
          Canvas.Brush.Color := $808080;
          Canvas.Pen.Color := $808080;
          Canvas.Rectangle(0, ThisTop, Width, ThisTop + Border);
          Canvas.Rectangle(0, ThisTop, Border, ThisTop + IH);
          Canvas.Brush.Color := $e0e0e0;
          Canvas.Pen.Color := $e0e0e0;
          Canvas.Rectangle(0, ThisTop + IH - Border, Width, ThisTop + IH);
          Canvas.Pen.Color := clBlack;
          Canvas.Brush.Color := clBtnFace;
        end;
//        Canvas.TextOut(5, ThisTop + Border, Tag.Name);
//        Canvas.TextOut(5, ThisTop + Border + Canvas.TextHeight(Tag.Name), IntToHex(Mask, 8));
        Paint(Canvas, LMargin, ThisTop, Width - LMargin, IH, MSPerPixel, ThisOffset);
      end;
    end;
  end;
end;

function TLogicAnalyser.GetTrace(Index : Integer) : TLogicAnalyserTrace;
begin
  Result := TLogicAnalyserTrace(Traces[Index]);
end;

procedure TLogicAnalyser.SetFrozen(aFrozen : Boolean);
begin
  if aFrozen then
    FreezeTime := CNC32.PlcTick.I64;
  FFrozen := aFrozen;
end;

procedure TLogicAnalyser.SetMSPerPixel(aMSPerPixel : Double);
begin
  if aMSPerPixel < 1 then
    aMSPerPixel := 1;

  FMSPerPixel := aMSPerPixel;
end;

procedure TLogicAnalyser.AddTag(aTag : TAbstractTag; aMask : Integer);
begin
  Traces.Add(TLogicAnalyserTrace.Create(aTag, aMask));
end;

class procedure TLogicAnalyserForm.Execute(aLogicAnalyser : TLogicAnalyser);
begin
  if not Assigned(LogicAnalyserForm) then begin
    LogicAnalyserForm := TLogicAnalyserForm.Create(Application);
    try
      LogicAnalyserForm.LogicAnalyser := aLogicAnalyser;
      LogicAnalyserForm.BtnFreeze.Down := aLogicAnalyser.Frozen;
      LogicAnalyserForm.ShowModal;
    finally
      LogicAnalyserForm.Free;
      LogicAnalyserForm := nil;
    end;
  end;
end;

procedure TLogicAnalyserForm.Timer1Timer(Sender: TObject);
begin
  GraphPaint;
end;

procedure TLogicAnalyserForm.FormResize(Sender: TObject);
begin
  Bmap.Width := GraphArea.Width;
  Bmap.Height := GraphArea.Height;
end;

function mHMSFormat(millis: Double): string;
begin
  if millis > 1000 then
    Result:= HMSFormat(millis)
  else
    Result:= Format('%.2fs', [millis / 1000]);
end;

procedure TLogicAnalyserForm.UpdateTotalTime;
var Tmp : string;
begin
  if LogicAnalyser.Traces.Count > 0 then begin
    Tmp := mHMSFormat(LogicAnalyser.DisplayTime * 1000);
    if Tmp <> LabelScale.Caption then
      LabelScale.Caption := Tmp;

    //  Tmp := Format('%.2fs', [(LogicAnalyser.Offset / 1000)]);
    Tmp := mHMSFormat(LogicAnalyser.Offset);
    if Tmp <> LabelOffset.Caption then
      LabelOffset.Caption := Tmp;

    //  Tmp := Format('%.2fs', [(LogicAnalyser.FrozenFor / 1000)]);
    Tmp := mHMSFormat(LogicAnalyser.FrozenFor);
    if Tmp <> BtnFreeze.Caption then
      BtnFreeze.Caption :=  Sysobj.Localized.GetString('$FREEZE') + ' ' + Tmp;

    //  Tmp := Format('%.2fs', [LogicAnalyser.TPU]);
    Tmp := mHMSFormat(LogicAnalyser.TPU * 1000);
    if Tmp <> LabelTPU.Caption then
      LabelTPU.Caption := Tmp + '/U';

    with LogicAnalyser.Trace[LogicAnalyser.ActiveTrace] do
      Tmp := Format('Name=%s; Mask=%d; Storage=%.2fs : %.1f%%', [
          Tag.Name, Mask,
          (StartTick - CNC32.PlcTick.I64) / 1000,
          (Data.Count / MaxDataStorage) * 100]);
      if Tmp <> LabelAT.Caption then
        LabelAT.Caption := Tmp;
  end;
end;

procedure TLogicAnalyserForm.FormCreate(Sender: TObject);
var I : Integer;
    MI : TMenuItem;
begin
  Bmap := TBitMap.Create;
  MaskPopup := TPopupMenu.Create(Self);
  for I := 0 to 15 do begin
    MI := TMenuItem.Create(MaskPopup);
    MI.Caption := 'Bit' + IntToStr(I);
    MI.Checked := False;
    MI.Tag := I;
    MI.OnClick := DoMaskChange;
    MaskPopup.Items.Add(MI);
  end;
  MI := TMenuItem.Create(MaskPopup);
  MI.Caption := 'Delete';
  MI.OnClick := DoDeleteTrace;
  MaskPopup.Items.Add(MI);

  Label2.Caption:= Sysobj.Localized.GetString('$SCALE');
  Label3.Caption:= Sysobj.Localized.GetString('$POSITION');
  BitBtn1.Caption:= Sysobj.Localized.GetString('$CLOSE');
  BtnFreeze.Caption:= Sysobj.Localized.GetString('$FREEZE');
  SpeedButton1.Caption:= Sysobj.Localized.GetString('$NEW');
end;

procedure TLogicAnalyserForm.SpeedButton1Click(Sender: TObject);
var aTag : TAbstractTag;
begin
  aTag := TSelectTag.Execute([TMethodTag, TIntegerTag]);
  if aTag <> nil then
    LogicAnalyser.AddTag(aTag, 1);
end;

procedure TLogicAnalyserForm.GraphAreaMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var I : Integer;
begin
  if LogicAnalyser.Traces.Count > 0 then
    if Button = mbRight then begin
      EditingTrace := LogicAnalyser.ActiveTrace;
      with LogicAnalyser[EditingTrace] do begin
        for I := 0 to 15 do begin
          MaskPopup.Items[I].Checked := ((1 shl I) and Mask) <> 0;
        end;
      MaskPopup.Popup(Left + X, Top + Y);
      end;
    end;
end;

procedure TLogicAnalyserForm.FormDestroy(Sender: TObject);
begin
  MaskPopup.Free;
  Bmap.Free;
end;

procedure TLogicAnalyserForm.DoMaskChange(Sender : TObject);
var Tmp : Integer;
begin
  Tmp := LogicAnalyser.Trace[EditingTrace].Mask;
  Tmp := Tmp xor 1 shl TComponent(Sender).Tag;

  if Tmp <> 0 then
    LogicAnalyser.Trace[EditingTrace].Mask := Tmp;
end;

procedure TLogicAnalyserForm.DoDeleteTrace(Sender : TObject);
begin
  if LogicAnalyser.Traces.Count > 0 then
    LogicAnalyser.DeleteTrace := EditingTrace;
end;

procedure TLogicAnalyserForm.BtnFreezeClick(Sender: TObject);
begin
  LogicAnalyser.Frozen := BtnFreeze.Down;
end;

procedure TLogicAnalyserForm.BtnResetClick(Sender: TObject);
begin
  LogicAnalyser.Frozen:= False;
  LogicAnalyser.Offset:= 0;
  LogicAnalyser.MSPerPixel:= 50;
end;

procedure TLogicAnalyserForm.btnScaleMinusClick(Sender: TObject);
begin
  LogicAnalyser.MSPerPixel := LogicAnalyser.MSPerPixel - 1;
end;

procedure TLogicAnalyserForm.BtnScalePlusClick(Sender: TObject);
begin
  LogicAnalyser.MSPerPixel := LogicAnalyser.MSPerPixel + 1;
end;

procedure TLogicAnalyserForm.BtnOffsetMinusClick(Sender: TObject);
begin
  LogicAnalyser.Offset := LogicAnalyser.Offset - 1000;
end;

procedure TLogicAnalyserForm.BtnOffsetPlusClick(Sender: TObject);
begin
  LogicAnalyser.Offset := LogicAnalyser.Offset + 1000;
end;

procedure TLogicAnalyserForm.BtnMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  Timer2.Interval := 200;
  Timer2.Enabled := True;
  Acceleration := 1;
  Timer2.Tag := Integer(Sender);
end;

procedure TLogicAnalyserForm.BtnMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  Timer2.Enabled := False;
end;


procedure TLogicAnalyserForm.Timer2Timer(Sender: TObject);
var Tmp : Integer;
begin
  Timer2.Interval := 90;
  Inc(Acceleration, Acceleration div 10 + 1);
  if Acceleration > 10000 then
    Acceleration:= 10000;
    
  for Tmp := 0 to Acceleration div 2 do
    TSpeedButton(Timer2.Tag).OnClick(Self);
end;

procedure TLogicAnalyserForm.GraphAreaMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
var H : Double;
begin
  if LogicAnalyser.Traces.Count > 0 then begin
    H := GraphArea.Height / LogicAnalyser.Traces.Count;
    H := Y / H;

    LogicAnalyser.ActiveTrace := Trunc(H) mod LogicAnalyser.Traces.Count;
  end;
end;

{ TRingBuffer }

procedure TRingBuffer.Add(Value: Integer);
begin
  Data[Head] := Value;
  Inc(Head);
  if Head >= MAX_RINGBUFFER_SIZE then
    Wrapped := True;
  Head := Head mod MAX_RINGBUFFER_SIZE;
end;

procedure TRingBuffer.Clear;
begin
  Head := 0;
  Wrapped := False;
end;

function TRingBuffer.Count: Integer;
begin
  if not Wrapped then
    Result := Head
  else
    Result := MAX_RINGBUFFER_SIZE;
end;

constructor TRingBuffer.Create;
begin
  Clear;
end;

destructor TRingBuffer.Destroy;
begin
  inherited;
end;

function TRingBuffer.GetValue(Index: Integer): Integer;
begin
  if Wrapped then begin
    Result := Data[(Index + Head) mod MAX_RINGBUFFER_SIZE];
  end else begin
    Result := Data[Index];
  end;
end;

end.
