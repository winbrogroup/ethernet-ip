object LogicAnalyserForm: TLogicAnalyserForm
  Left = 485
  Top = 411
  Width = 651
  Height = 395
  BorderIcons = [biSystemMenu]
  Caption = 'Object PLC Logic Analyser'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object GraphArea: TPaintBox
    Left = 0
    Top = 34
    Width = 643
    Height = 307
    Align = alClient
    OnMouseMove = GraphAreaMouseMove
    OnMouseUp = GraphAreaMouseUp
  end
  object Panel1: TPanel
    Left = 0
    Top = 341
    Width = 643
    Height = 27
    Align = alBottom
    BevelOuter = bvLowered
    Color = 15965740
    TabOrder = 0
    DesignSize = (
      643
      27)
    object LabelTPU: TLabel
      Left = 8
      Top = 8
      Width = 57
      Height = 13
      Caption = 'LabelTPU'
      Color = 16031791
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clYellow
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
    end
    object LabelAT: TLabel
      Left = 104
      Top = 8
      Width = 48
      Height = 13
      Caption = 'LabelAT'
      Color = 16031791
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clYellow
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
    end
    object BitBtn1: TBitBtn
      Left = 567
      Top = 1
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      TabOrder = 0
      Kind = bkClose
    end
  end
  object CPanel: TPanel
    Left = 0
    Top = 0
    Width = 643
    Height = 34
    Align = alTop
    BevelOuter = bvLowered
    TabOrder = 1
    object BtnFreeze: TSpeedButton
      Left = 430
      Top = 1
      Width = 97
      Height = 32
      AllowAllUp = True
      GroupIndex = 1
      Caption = 'Freeze'
      OnClick = BtnFreezeClick
    end
    object SpeedButton1: TSpeedButton
      Left = 568
      Top = 1
      Width = 65
      Height = 32
      Caption = 'New Trace'
      OnClick = SpeedButton1Click
    end
    object BtnReset: TSpeedButton
      Left = 318
      Top = 2
      Width = 91
      Height = 32
      AllowAllUp = True
      Caption = 'Reset'
      OnClick = BtnResetClick
    end
    object Panel2: TPanel
      Left = 5
      Top = 0
      Width = 136
      Height = 34
      BevelOuter = bvLowered
      Color = 15965740
      TabOrder = 0
      object btnScaleMinus: TSpeedButton
        Left = 4
        Top = 2
        Width = 30
        Height = 31
        Caption = '-'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -14
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        OnClick = btnScaleMinusClick
        OnMouseDown = BtnMouseDown
        OnMouseUp = BtnMouseUp
      end
      object Label2: TLabel
        Left = 36
        Top = 2
        Width = 57
        Height = 13
        Alignment = taCenter
        AutoSize = False
        Caption = 'Scale'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clYellow
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object LabelScale: TLabel
        Left = 36
        Top = 16
        Width = 57
        Height = 13
        Alignment = taCenter
        AutoSize = False
        Caption = 'Label4'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clYellow
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object BtnScalePlus: TSpeedButton
        Left = 100
        Top = 2
        Width = 33
        Height = 31
        Caption = '+'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -14
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        OnClick = BtnScalePlusClick
        OnMouseDown = BtnMouseDown
        OnMouseUp = BtnMouseUp
      end
    end
    object Panel3: TPanel
      Left = 160
      Top = 0
      Width = 136
      Height = 34
      BevelOuter = bvLowered
      Color = 15965740
      TabOrder = 1
      object BtnOffsetMinus: TSpeedButton
        Left = 4
        Top = 2
        Width = 30
        Height = 31
        Caption = '-'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -14
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        OnClick = BtnOffsetMinusClick
        OnMouseDown = BtnMouseDown
        OnMouseUp = BtnMouseUp
      end
      object LabelOffset: TLabel
        Left = 36
        Top = 16
        Width = 57
        Height = 13
        Alignment = taCenter
        AutoSize = False
        Caption = 'LabelOffset'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clYellow
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label3: TLabel
        Left = 36
        Top = 2
        Width = 57
        Height = 13
        Alignment = taCenter
        AutoSize = False
        Caption = 'Position'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clYellow
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object BtnOffsetPlus: TSpeedButton
        Left = 100
        Top = 2
        Width = 33
        Height = 31
        Caption = '+'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -14
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        OnClick = BtnOffsetPlusClick
        OnMouseDown = BtnMouseDown
        OnMouseUp = BtnMouseUp
      end
    end
  end
  object Timer1: TTimer
    Interval = 200
    OnTimer = Timer1Timer
    Left = 288
    Top = 224
  end
  object Timer2: TTimer
    Enabled = False
    Interval = 400
    OnTimer = Timer2Timer
    Left = 320
    Top = 224
  end
end
