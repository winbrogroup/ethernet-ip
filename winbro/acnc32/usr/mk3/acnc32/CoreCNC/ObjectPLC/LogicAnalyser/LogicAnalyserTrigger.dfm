object Form2: TForm2
  Left = 623
  Top = 438
  BorderStyle = bsToolWindow
  Caption = 'Logic Analyser Trigger'
  ClientHeight = 214
  ClientWidth = 306
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 0
    Top = 0
    Width = 306
    Height = 89
    Align = alTop
    Caption = 'Enable'
    TabOrder = 0
    object RadioButton1: TRadioButton
      Left = 16
      Top = 24
      Width = 113
      Height = 17
      Caption = 'Free run'
      TabOrder = 0
    end
    object RadioButton2: TRadioButton
      Left = 16
      Top = 48
      Width = 113
      Height = 17
      Caption = 'Signal'
      TabOrder = 1
    end
    object ComboBox1: TComboBox
      Left = 88
      Top = 48
      Width = 145
      Height = 21
      ItemHeight = 13
      TabOrder = 2
      Text = 'ComboBox1'
    end
    object CheckBox1: TCheckBox
      Left = 240
      Top = 48
      Width = 97
      Height = 17
      Caption = 'Falling'
      TabOrder = 3
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 173
    Width = 306
    Height = 41
    Align = alBottom
    TabOrder = 1
    object BitBtn1: TBitBtn
      Left = 32
      Top = 8
      Width = 75
      Height = 25
      TabOrder = 0
      Kind = bkOK
    end
    object BitBtn2: TBitBtn
      Left = 120
      Top = 8
      Width = 75
      Height = 25
      TabOrder = 1
      Kind = bkCancel
    end
  end
  object GroupBox2: TGroupBox
    Left = 0
    Top = 89
    Width = 306
    Height = 84
    Align = alClient
    Caption = 'Disable'
    TabOrder = 2
    object RadioButton3: TRadioButton
      Left = 16
      Top = 24
      Width = 113
      Height = 17
      Caption = 'Invert Enable Signal'
      TabOrder = 0
    end
    object RadioButton4: TRadioButton
      Left = 16
      Top = 48
      Width = 113
      Height = 17
      Caption = 'Signal'
      TabOrder = 1
    end
    object ComboBox2: TComboBox
      Left = 88
      Top = 48
      Width = 145
      Height = 21
      ItemHeight = 13
      TabOrder = 2
      Text = 'ComboBox1'
    end
    object CheckBox2: TCheckBox
      Left = 240
      Top = 48
      Width = 97
      Height = 17
      Caption = 'Falling'
      TabOrder = 3
    end
  end
end
