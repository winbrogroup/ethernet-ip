unit PlcLamptest;

interface

uses PlcItems, Plc32, CNC32, SysUtils, CNCTypes, Classes, Graphics, CoreCNC,
     PlcValueItem;

type
  TLampTestValue = (
    ltvOn,
    ltvFlash
  );

  TLampTestValues = set of TLampTestValue;


  TPlcLampItem = class (TPlcNamedItem)
  public
    procedure GatePost; override;
  end;

  TPlcLampTestItem = class(TPlcItem)
  private
    FIsWritable: Boolean;
  public
    constructor Create(St : TPlcTokenizer; aName : string); override;
    procedure   Compile(Ele : PTPlcElement; St : TPlcTokenizer; Write : boolean); override;
    procedure   Write(Ele : PTPlcElement; line : boolean); override;
    function    Read(Ele : PTPlcElement) : boolean; override;
    function    GateName(Ele : PTPlcElement) : string; override;
    procedure   ItemProperties(Ele : PTPlcElement; SL : TStrings); override;
    property    IsWritable : Boolean read FIsWritable;
  end;


implementation

var LampTestValues : TLampTestValues;

type
  TPlcValueCommands = array [TLampTestValue] of TValuePair;

const
  PlcEmbedName : TPlcValueCommands = (
        ( value : 'On'; reado : False ),
        ( value : 'Flash'; reado : False )
  );

{ TPlcLampItem }

procedure TPlcLampItem.GatePost;
begin
//  inherited;
  LastValue := FValue;

  if not (ltvOn in LampTestValues) then begin
    if IsWritable and (Value <> Tag.AsInteger) then
      TMethodTag(Tag).AsInteger := FValue;
  end else
    TMethodTag(Tag).AsBoolean := True;
end;


{ TPlcLampTestItem }

procedure TPlcLampTestItem.Compile(Ele: PTPlcElement; St: TPlcTokenizer;
  Write: boolean);
var token : string;
    I  : TLampTestValue;
begin
  token := st.skipComment;
  if token <> '.' then
    raise ECompileItemFail.CreateFmt(MethodNameMissing, [name]);

  token := st.skipComment;

  for I := Low(I) to High(I) do begin
    if CompareText(token, PlcEmbedName[i].Value) = 0 then begin
      Ele.Embedded.Command := Ord(I);
      Exit;
    end;
  end;
  raise ECompileItemFail.CreateFmt(InvalidMethodName, [token]);
end;

constructor TPlcLampTestItem.Create(St: TPlcTokenizer; aName: string);
begin
  inherited;
  if St.Read <> ';' then
    raise ECreateItemFail.CreateFmt(SemicolonMissing, [name]);
end;

function TPlcLampTestItem.GateName(Ele: PTPlcElement): string;
begin
  if (Ele.Embedded.Command < Ord(Low(TLampTestValue))) or
     (Ele.Embedded.Command > Ord(High(TLampTestValue))) then begin
    Result := '.Err';
  end else
    Result := PlcEmbedName[TLampTestValue(Ele.Embedded.Command)].Value;
end;

procedure TPlcLampTestItem.ItemProperties(Ele: PTPlcElement; SL: TStrings);
var I : TLampTestValue;
begin
  for I := Low(I) to High(I) do begin
    SL.Add(PlcEmbedName[I].Value);
    SL.Add(BooleanIdent[I in LampTestValues]);
  end;
end;

function TPlcLampTestItem.Read(Ele: PTPlcElement): boolean;
begin
  Result := TLampTestValue(Ele^.Embedded.Command) in LampTestValues;
end;

procedure TPlcLampTestItem.Write(Ele: PTPlcElement; line: boolean);
begin
  if Line <> Ele^.Line then begin
    if Line then
      Include(LampTestValues, TLampTestValue(Ele^.Embedded.Command))
    else
      Exclude(LampTestValues, TLampTestValue(Ele^.Embedded.Command));
  end;
end;

initialization
  RegisterPlcItemClass('Lamp', TPlcLampItem);
  RegisterPlcItemClass('LampTest', TPlcLamptestItem);
end.
