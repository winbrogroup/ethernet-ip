object EditNamedLayerTag: TEditNamedLayerTag
  Left = 456
  Top = 212
  Width = 546
  Height = 346
  Caption = 'Edit Tag'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 278
    Width = 538
    Height = 41
    Align = alBottom
    TabOrder = 0
    object BitBtn1: TBitBtn
      Left = 24
      Top = 8
      Width = 75
      Height = 25
      TabOrder = 0
      Kind = bkOK
    end
    object BitBtn2: TBitBtn
      Left = 112
      Top = 8
      Width = 75
      Height = 25
      TabOrder = 1
      Kind = bkCancel
    end
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 0
    Width = 217
    Height = 278
    Align = alLeft
    Caption = 'New Value'
    TabOrder = 1
    object EditMemo: TMemo
      Left = 2
      Top = 15
      Width = 213
      Height = 261
      Align = alClient
      TabOrder = 0
    end
  end
  object GroupBox2: TGroupBox
    Left = 217
    Top = 0
    Width = 321
    Height = 278
    Align = alClient
    Caption = 'Properties'
    TabOrder = 2
    object ListView: TListView
      Left = 2
      Top = 15
      Width = 317
      Height = 261
      Align = alClient
      Columns = <
        item
          Caption = 'Property'
          Width = 150
        end
        item
          Caption = 'Value'
          Width = 150
        end>
      GridLines = True
      TabOrder = 0
      ViewStyle = vsReport
    end
  end
end
