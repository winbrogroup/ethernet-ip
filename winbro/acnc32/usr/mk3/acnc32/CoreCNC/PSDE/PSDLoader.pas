unit PSDLoader;

interface

uses Windows, ProcessEditor, SysUtils, PSDPlugin, Classes, CncTypes;

type
  { Acts as an array of IProcessEditors
    To initialize create the class then call AddDll with the full path
    to the Dll to load.

    The default property is the array of IProcessEditor's }
  TPSDLoader = class(TInterfacedObject, IPSDELibrary)
  private
    FPSDE: array of IPSDEditor;
    FPSDECount: Integer;
    PluginCount: PSD_PluginCount;
    GetPlugin: PSD_GetPlugin;
    SetLibrary: PSD_SetLibrary;
    PSDInitialise: PSD_Initialise;

    function GetPSDE(Index: Integer): IPSDEditor; stdcall;
  public
    constructor Create;
    procedure Close;
    procedure AddDll(HMod: HMODULE);
    property PSDE[Index: Integer]: IPSDEditor read GetPSDE; default;
    function Count: Integer; stdcall;
  end;

implementation

uses CoreCNC;

{ TPSDLoader }

procedure TPSDLoader.AddDll(HMod: HMODULE);
var I: Integer;
begin
  PluginCount := GetProcAddress(HMod, PSD_PluginCount_Name);
  GetPlugin := GetProcAddress(HMod, PSD_GetPlugin_Name);
  SetLibrary:= GetProcAddress(HMod, PSD_SetLibrary_Name);
  PSDInitialise:= GetProcAddress(HMod, PSD_Initialise_Name);

  if Assigned(PSDInitialise) then
    PSDInitialise;

  if Assigned(PluginCount) and Assigned(GetPlugin) then begin
    for I:= 0 to PluginCount - 1 do begin
      try
      FPSDE[FPSDECount + I]:= GetPlugin(I);
      except
        on E: Exception do begin
          raise Exception.Create('PSDE Failed to load: ' + CarRet + 'Check Event.Log for more information');
        end;
      end;
    end;
    Inc(FPSDECount, PluginCount);
  end;

  if Assigned(SetLibrary) then
    SetLibrary(Self);
end;

function TPSDLoader.Count: Integer;
begin
  Result:= FPSDECount;
end;

constructor TPSDLoader.Create;
begin
  SetLength(FPSDE, 100);
  FPSDECount:= 0;
end;

procedure TPSDLoader.Close;
var I: Integer;
begin
  for I:= 0 to Count - 1 do
    FPSDE[I]:= nil;
end;

function TPSDLoader.GetPSDE(Index: Integer): IPSDEditor;
begin
  if (Index < FPSDECount) and (Index >= 0) then
    Result:= FPSDE[Index]
  else
    raise Exception.Create('Process Editor does not exist at this index: ' + IntToStr(Index));
end;

end.
