unit StringArray;

interface

uses IStringArray, Classes, SysUtils, CNCTypes, StreamTokenizer, SyncObjs,
     FileCtrl, Windows, CNC32, IFSDServer, WideStrUtils;

type
  TStringArraySet = class;

  TClientList = class(TObject)
  private
    _ClientList : array of IACNC32StringArrayNotify;
    ClientListA : array of IACNC32StringArrayNotifyA;
    _FClientCount : Integer;
    FClientCountA : Integer;
    function _GetClient(Index: Integer): IACNC32StringArrayNotify;
    function GetClientA(Index: Integer): IACNC32StringArrayNotifyA;
    function GetAllClient(Index: Integer): IACNC32StringArrayNotify;
  public
    constructor Create;
    procedure AddClient(Client : IACNC32StringArrayNotify);
    procedure DeleteClient(Client : IACNC32StringArrayNotify);
    function _ClientCount : Integer;
    function ClientCountA : Integer;

    function AllClientCount: Integer;
    property _Clients[Index : Integer] : IACNC32StringArrayNotify read _GetClient; default;
    property ClientsA[Index : Integer] : IACNC32StringArrayNotifyA read GetClientA;
    property AllClients[Index : Integer] : IACNC32StringArrayNotify read GetAllClient;
  end;

  TStringArray = class(TInterfacedObject, IACNC32StringArray, IACNC32StringArrayA)
  private
    StringArraySet : TStringArraySet;
    Persistent : Boolean;
    FData : TStringList;
    FAlias : string;
    FClients : TClientList;
    Lock : TCriticalSection;
    UpdateCount : Integer;
    Cursor: Integer;
    procedure Clean;
    procedure SetData(Row, Col : Integer; const Value : string; Client : IACNC32StringArrayNotify = nil);  // L
    function GetData(Row, Col : Integer) : string;  // L
    function GetColumn(Index : Integer) : TStringList;
    property Data[Rec, Field : Integer] : string read GetData;
    property Column[Index : Integer] : TStringList read GetColumn;
  public
    constructor Create(SAS : TStringArraySet; Alias : string);
    destructor Destroy; override;
    function FieldCount : Integer; stdcall;
    function RecordCount : Integer; stdcall;
    function Alias : WideString; stdcall;
    function FieldNameAtIndex(Index : Integer; out FieldName : WideString) : TAA_RESULT; stdcall;
    function AddField(FieldName : WideString; Client : IACNC32StringArrayNotify = nil) : TAA_RESULT; stdcall;  // L
    function DeleteField(FieldName : WideString; Client : IACNC32StringArrayNotify = nil) : TAA_RESULT; stdcall; // L
    function FindRecord(Index, Col : Integer; Value : WideString; out Ptr : Integer) : TAA_RESULT; stdcall;
    function FindRecordByField(Index : Integer; FieldName, Value : WideString; out Ptr : Integer) : TAA_RESULT; stdcall;

    function GetFieldValue(Index : Integer; FieldName : WideString; out Value : WideString) : TAA_RESULT; stdcall;
    function SetFieldValue(Index : Integer; FieldName, Value : WideString; Client : IACNC32StringArrayNotify = nil) : TAA_RESULT; stdcall;
    function GetValue(Index, Col : Integer; out Value : WideString) : TAA_RESULT; stdcall;
    function SetValue(Index, Col : Integer; Value : WideString; Client : IACNC32StringArrayNotify = nil) : TAA_RESULT; stdcall;

    function Append(Count : Integer; Client : IACNC32StringArrayNotify = nil) : TAA_RESULT; stdcall; // L
    function Insert(Index : Integer; Client : IACNC32StringArrayNotify = nil) : TAA_RESULT; stdcall; // L
    function Delete(Index : Integer; Client : IACNC32StringArrayNotify = nil) : TAA_RESULT; stdcall; // L

    function ExportToCSV(FileName : WideString) : TAA_RESULT; stdcall;
    function ImportFromCSV(FileName : WideString; Append : Boolean; Client : IACNC32StringArrayNotify = nil) : TAA_RESULT; stdcall;

    function Swap(A, B : Integer; Client : IACNC32StringArrayNotify = nil) : TAA_RESULT; stdcall;
    function Copy(Dest, Source : Integer; Client : IACNC32StringArrayNotify = nil) : TAA_RESULT; stdcall;

    procedure BeginUpdate; stdcall;
    procedure EndUpdate(const Client : IACNC32StringArrayNotify = nil); stdcall;
    procedure Clear(Client : IACNC32StringArrayNotify = nil); stdcall;

    function __FindRecord(Index, Col : Integer; Value : string) : Integer;
    function HTTPGeneratePage(HFile : THANDLE; var CGIPath : WideString; CGIRequest : ICGIStringList) : Boolean;

    procedure SetCursor(ACursor: Integer; const Client: IACNC32StringArrayNotifyA = nil); stdcall;
    function GetCursor: Integer; stdcall;

  end;

  TStringArraySet = class(TInterfacedObject, IACNC32StringArraySet, IACNC32StringArraySetA)
  private
    TableList : TStringList;
    Path : string;
    FTotalEvents : Integer;
    procedure Clean;
    function GetStringArray(Index : Integer) : TStringArray;
  protected
    property Tables [Index : Integer] : TStringArray read GetStringArray;
    procedure RemoveArray(SA: TStringArray);
  public
    constructor Create(aPath : string);
    destructor Destroy; override;
    function ArrayCount : Integer; stdcall;
    function AliasAtIndex(Index : Integer; out Alias : WideString) : TAA_RESULT; stdcall;
    function UseArray(Alias : WideString; Client : IACNC32StringArrayNotify; out Table : IACNC32StringArray) : TAA_RESULT; stdcall;
    function UseArrayA(Alias : WideString; Client : IACNC32StringArrayNotifyA; out Table : IACNC32StringArrayA) : TAA_RESULT; stdcall;
    function ReleaseArray(Client: IACNC32StringArrayNotify; Table : IACNC32StringArray) : TAA_RESULT; stdcall;
    function CreateArray(Alias: WideString; Client: IACNC32StringArrayNotify; out Table : IACNC32StringArray): TAA_RESULT;
    function DeleteArray(Alias : WideString) : TAA_RESULT;
    function HTTPGeneratePage(HFile : THANDLE; var CGIPath : WideString; CGIRequest : ICGIStringList) : Boolean;
    property TotalEvents : Integer read FTotalEvents;
  end;


implementation

uses CoreCNC;

{ TStringArraySet }

constructor TStringArraySet.Create(aPath : string);
var SR : TSearchRec;
begin
  ForceDirectories(aPath);
  TableList := TStringList.Create;
  TableList.Sorted := True;
  TableList.Duplicates := dupError;
  Path := aPath;
  if FindFirst(aPath + '*', 0, SR) = 0 then begin
    repeat
      TableList.AddObject(UpperCase(SR.Name), nil);
    until FindNext(SR) <> 0;
  end;
end;

destructor TStringArraySet.Destroy;
begin
  Clean;
  TableList.Free;
  inherited;
end;

function TStringArraySet.AliasAtIndex(Index: Integer; out Alias: WideString): TAA_RESULT;
begin
  Result := AAR_OK;
  if Index < TableList.Count then begin
    Alias := TableList[Index];
  end else begin
    Alias := '';
    Result := AAR_OUT_OF_BOUNDS;
  end;
end;

function TStringArraySet.CreateArray(Alias: WideString; Client: IACNC32StringArrayNotify; out Table : IACNC32StringArray): TAA_RESULT;
var I : Integer;
    SA : TStringArray;
begin
  Table := nil;
  I := TableList.IndexOf(UpperCase(Alias));
  if I <> -1 then begin
    Result := AAR_ALREADY_EXISTS;
  end else begin
    SA := TStringArray.Create(Self, Alias);
    Table := SA;
    TableList.AddObject(UpperCase(Alias), SA);
    if Assigned(Client) then
      SA.FClients.AddClient(Client);
    Result := AAR_OK;
  end;
end;

function TStringArraySet.DeleteArray(Alias: WideString): TAA_RESULT;
var I : Integer;
begin
  I := TableList.IndexOf(UpperCase(Alias));
  Result := AAR_OK;
  if I <> -1 then begin
    Tables[I].Persistent := False;
    TableList.Objects[I] := nil;
    TableList.Delete(I);
  end else begin
    Result := AAR_NOT_FOUND;
  end;
end;

function TStringArraySet.ArrayCount: Integer;
begin
  Result := TableList.Count;
end;

function TStringArraySet.UseArrayA(Alias: WideString; Client: IACNC32StringArrayNotifyA; out Table : IACNC32StringArrayA): TAA_RESULT;
var I : Integer;
    AA : TStringArray;
begin
  Result := AAR_OK;
  I := TableList.IndexOf(UpperCase(Alias));
  if I <> -1 then begin
    AA := TStringArray(Tables[I]);
    if AA = nil then begin
      AA := TStringArray.Create(Self, UpperCase(Alias));
      TableList.Objects[I] := AA;
    end;
    Table := AA;

    AA.FClients.AddClient(Client);
  end else begin
    Result := AAR_NOT_FOUND;
  end;
end;

function TStringArraySet.UseArray(Alias: WideString; Client: IACNC32StringArrayNotify; out Table : IACNC32StringArray): TAA_RESULT;
var I : Integer;
    AA : TStringArray;
begin
  Result := AAR_OK;
  I := TableList.IndexOf(UpperCase(Alias));
  if I <> -1 then begin
    AA := TStringArray(Tables[I]);
    if AA = nil then begin
      AA := TStringArray.Create(Self, UpperCase(Alias));
      TableList.Objects[I] := AA;
    end;
    Table := AA;
//    if Assigned(Client) then
      AA.FClients.AddClient(Client);
  end else begin
    Result := AAR_NOT_FOUND;
  end;
end;

function TStringArraySet.GetStringArray(Index: Integer): TStringArray;
begin
  if Index < TableList.Count then begin
    Result := TStringArray(TableList.Objects[Index])
  end else begin
    Result := nil;
  end;
end;

procedure TStringArraySet.Clean;
begin
  TableList.Clear;
end;

procedure TStringArraySet.RemoveArray(SA: TStringArray);
var I : Integer;
begin
  I := TableList.IndexOf(SA.Alias);
  if I <> -1 then
    TableList.Objects[I] := nil;
end;

function TStringArraySet.ReleaseArray(Client: IACNC32StringArrayNotify;
  Table: IACNC32StringArray): TAA_RESULT;
var I : Integer;
    AA : TStringArray;
begin
  I := TableList.IndexOf(Table.Alias);
  if I <> -1 then begin
    AA := TStringArray(TableList.Objects[I]);
    if Assigned(AA) then begin
      AA.FClients.DeleteClient(Client);
      if AA.Persistent then
        AA.ExportToCSV(Path + AA.Alias);

      if AA.FClients.AllClientCount = 0 then begin
        AA.Clean;
        TableList.Objects[I] := nil;
      end;
    end;
  end;
  Result := AAR_OK;
end;

function TStringArraySet.HTTPGeneratePage(HFile: THANDLE;
  var CGIPath: WideString; CGIRequest: ICGIStringList): Boolean;
var Table : TStringArray;
    I : Integer;
    AA : TStringArray;
    IA : IACNC32StringArray;
    Tmp : string;
begin
  Result := False;
  if CGIPath = '' then begin
    Result := True;
    WriteACNC32HTTPTitle(HFile, 'StringArray');
    WriteACNC32HTTPHeading(hFile, 2, 'StringArray');
    WriteStringToHandle(hFile, '<table border="1" align="center" width="80%">' + CarRet);
    WriteStringToHandle(hFile, '<tr><th>Name</th><th>Columns</th><th>Rows</th></tr>' + CarRet);
    for I := 0 to TableList.Count - 1 do begin
      if Assigned(TableList.Objects[I]) then begin
        Table := Tables[I];
        WriteStringToHandle(hFile, Format('<tr><td><a href="StringArray/%s">%s</a></td><td>%d</td><td>%d</td></tr>',
             [TableList[I], TableList[I], Table.FieldCount, Table.RecordCount]) + CarRet);
      end else begin
        WriteStringToHandle(hFile, Format('<tr><td><a href="StringArray/%s">%s</a></td><td>?</td><td>?</td></tr>',
             [TableList[I], TableList[I]]) + CarRet);
      end;
    end;
  end else begin
    Tmp := ReadDelimitedW(CGIPath, '/');
    for I := 0 to TableList.Count - 1 do begin
      if CompareText(TableList[I], Tmp) = 0 then begin
        Result := True;
        if not Assigned(Tables[I]) then
          UseArray(TableList[I], nil, IA);

        AA := Tables[I];
        AA.HTTPGeneratePage(HFile, CGIPath, CGIRequest);
        if Assigned(IA) then
          IA := nil;

        Exit;
      end;
    end;
  end;
end;


{ TStringArray }


function TStringArray.AddField(FieldName: WideString; Client : IACNC32StringArrayNotify = nil): TAA_RESULT;
var I : Integer;
    S : TStringList;
begin
  Lock.Acquire;
  BeginUpdate;
  try
    Result := AAR_ALREADY_EXISTS;

    if FData.IndexOf(UpperCase(FieldName)) <> -1 then
      Exit;
    S := TStringList.Create;
    FData.AddObject(FieldName, S);
    for I := 0 to RecordCount - 1 do
      S.Add('');

    Result := AAR_OK;
  finally
    Lock.Release;
    EndUpdate(Client);
  end;
end;

function TStringArray.Alias: WideString;
begin
  Result := FAlias;
end;

function TStringArray.Append(Count: Integer; Client : IACNC32StringArrayNotify = nil): TAA_RESULT;
var I, J : Integer;
begin
  Lock.Acquire;
  BeginUpdate;
  try
    for I := 0 to Count - 1 do
      for J := 0 to FieldCount - 1 do
        Column[J].Add('');

    Result := AAR_OK;
  finally
    Lock.Release;
    EndUpdate(Client);
  end;
end;

procedure TStringArray.Clean;
var I : Integer;
begin
  for I := 0 to FieldCount - 1 do
    Column[I].Free;
  FData.Clear;
end;

constructor TStringArray.Create(SAS: TStringArraySet; Alias: string);
begin
  inherited Create;
  Lock := TCriticalSection.Create;
  Self.StringArraySet := SAS;
//  FClients := TList.Create;
  FClients := TClientList.Create;
  FAlias := Alias;
  FData := TStringList.Create;
  Persistent := True;
  ImportFromCSV(SAS.Path + Alias, False);
end;

function TStringArray.Delete(Index: Integer; Client : IACNC32StringArrayNotify = nil): TAA_RESULT;
var I : Integer;
begin
  Lock.Acquire;
  BeginUpdate;
  try
    Result := AAR_OK;
    if Index < RecordCount  then
      for I := 0 to FieldCount - 1 do begin
        Column[I].Delete(Index);
      end
    else
      Result := AAR_OUT_OF_BOUNDS;
  finally
    Lock.Release;
    EndUpdate(Client);
  end;
end;

function TStringArray.DeleteField(FieldName: WideString; Client : IACNC32StringArrayNotify = nil): TAA_RESULT;
var I : Integer;
begin
  Lock.Acquire;
  BeginUpdate;
  try
    I := FData.IndexOf(UpperCase(FieldName));
    Result := AAR_NOT_FOUND;
    if I <> -1 then begin
      FData.Objects[I].Free;
      FData.Delete(I);
      Result := AAR_OK;
    end;
  finally
    Lock.Release;
    EndUpdate(Client);
  end;
end;

destructor TStringArray.Destroy;
begin
  //EventLog('Releasing array ' + Self.Alias);
  Clean;
  StringArraySet.RemoveArray(Self);
  FClients.Free;
  FData.Free;
  Lock.Free;
  inherited;
end;

function TStringArray.ExportToCSV(FileName: WideString): TAA_RESULT;
var I, J : Integer;
    Stream : TFileStream;
    T, Text : string;
begin
  Result := AAR_FAILED;
  try
    Stream := TFileStream.Create(FileName, fmCreate);
  except
    Exit;
  end;

  try
    Text := '';
    for J := 0 to FieldCount - 1 do begin
      T := FData[J];
      if J <> 0 then
        Text := Text + ',' + T
      else
        Text := T;
    end;
    Stream.WriteBuffer(PChar(Text)^, Length(Text));
    Stream.WriteBuffer(PChar(CarRet)^, Length(CarRet));
    for I := 0 to RecordCount - 1 do begin
      for J := 0 to FieldCount - 1 do begin
        T := CSVField(Data[I, J]);
        if J <> 0 then
          Text := Text + ',' + T
        else
          Text := T;
      end;
      Stream.WriteBuffer(PChar(Text)^, Length(Text));
      Stream.WriteBuffer(PChar(CarRet)^, Length(CarRet));
    end;
  finally
    Stream.Free;
    Result := AAR_OK;
  end;
end;

function TStringArray.FieldCount: Integer;
begin
  Result := FData.Count;
end;

function TStringArray.FieldNameAtIndex(Index: Integer;
  out FieldName: WideString): TAA_RESULT;
begin
  FieldName := '';
  Result := AAR_OK;
  if Index < FieldCount then
    FieldName := FData[Index]
  else
    Result := AAR_OUT_OF_BOUNDS;
end;

function TStringArray.__FindRecord(Index, Col : Integer; Value : string) : Integer;
begin
  Result := -1;
  if (Index < RecordCount) and (Col < FieldCount) then begin
    Result := Column[Col].IndexOf(Value);
  end;
end;

function TStringArray.FindRecord(Index, Col : Integer; Value : WideString; out Ptr : Integer) : TAA_RESULT; stdcall;
var I : Integer;
    S : TStringList;
begin
  Result := AAR_OUT_OF_BOUNDS;
  if (Index < RecordCount) and (Col < FieldCount) then begin
    S := Column[Col];
    Result := AAR_OK;
    for I := Index to RecordCount - 1 do begin
      if CompareText(Value, S[I]) = 0 then begin
        Ptr := I;
        Exit;
      end;
    end;

    Result := AAR_NOT_FOUND;
  end;
end;

function TStringArray.FindRecordByField(Index: Integer; FieldName,
  Value: WideString; out Ptr: Integer): TAA_RESULT;
var I : Integer;
begin
  Result := AAR_NOT_FOUND;
  I := FData.IndexOf(UpperCase(FieldName));
  if I <> -1 then
    Result := FindRecord(Index, I, Value, Ptr);
end;

function TStringArray.GetColumn(Index: Integer): TStringList;
begin
  Result := TStringList(FData.Objects[Index]);
end;

function TStringArray.GetData(Row, Col: Integer): string;
begin
  Result := Column[Col][Row];
end;

function TStringArray.GetFieldValue(Index: Integer; FieldName: WideString;
  out Value: WideString): TAA_RESULT;
var I : Integer;
begin
  I := FData.IndexOf(UpperCase(FieldName));
  if I <> -1 then
    Result := GetValue(Index, I, Value)
  else
    Result := AAR_NOT_FOUND;
end;

function TStringArray.GetValue(Index, Col: Integer;
  out Value: WideString): TAA_RESULT;
begin
  Lock.Acquire;
  try
    Result := AAR_OK;
    if (Index < RecordCount) and (Col < FieldCount) then
      Value := Data[Index, Col]
    else
      Result := AAR_OUT_OF_BOUNDS;
  finally
    Lock.Release;
  end;
end;

function TStringArray.ImportFromCSV(FileName: WideString;
  Append: Boolean; Client : IACNC32StringArrayNotify = nil): TAA_RESULT;
var S : TStringList;

  procedure ReadHeadings(aText : string);
  var T : string;
  begin
    T := ReadCSVField(aText);
    if (T = '') and (aText <> '') then
      T := 'Fix' + IntToStr(FData.Count + 1);
    while T <> '' do begin
      FData.AddObject(T, TStringList.Create);
      T := ReadCSVField(aText);
      if (T = '') and (aText <> '') then
        T := 'Fix' + IntToStr(FData.Count + 1);
    end;
  end;

var Start, I, J : Integer;
    T : string;

begin
  S := TStringList.Create;
  Result := AAR_OK;
  BeginUpdate;
  try
    try

      S.LoadFromFile(FileName);
      if DetectUTF8Encoding(s.Text) = etUTF8 then begin
        T:= Utf8ToAnsiEx(S.Text, Windows.GetACP);
        S.Text:= T;
      end;

    except
      Result := AAR_FAILED;
      Exit;
    end;

    if S.Count = 0 then
      Exit;

    Start := 0;
    if not Append then begin
      Clean;
      ReadHeadings(S[Start]);
      S.Delete(0);
    end else begin
      Start := RecordCount;
    end;


    for I := 0 to S.Count - 1 do begin
      T := S[I];
      if Trim(T) <> '' then begin
        Self.Append(1);
        for J := 0 to FieldCount - 1 do
          SetData(Start, J, ReadCSVField(T));
        Inc(Start);
      end;
    end;
  finally
    S.Free;
    EndUpdate(Client);
  end;
end;

function TStringArray.Insert(Index: Integer; Client : IACNC32StringArrayNotify = nil): TAA_RESULT;
var I : Integer;
begin
  Lock.Acquire;
  BeginUpdate;
  try
    Result := AAR_OK;
    if Index < RecordCount then
      for I := 0 to FieldCount - 1 do
        Column[I].Insert(Index, '')
    else
      Result := AAR_OUT_OF_BOUNDS;
  finally
    Lock.Release;
    EndUpdate(Client);
  end;
end;

function TStringArray.RecordCount: Integer;
begin
  if FieldCount > 0 then
    Result := Column[0].Count
  else
    Result := 0;
end;

procedure TStringArray.SetData(Row, Col: Integer; const Value: string;
                  Client : IACNC32StringArrayNotify = nil);
var I : Integer;
begin
  Column[Col][Row] := Value;
  if UpdateCount = 0 then begin
    SetCursor(Row);
    for I := 0 to Self.FClients.AllClientCount - 1 do begin
      if Assigned(FClients.AllClients[I]) and (Client <> FClients.AllClients[I]) then begin
        FClients.AllClients[I].CellChange(Self, Row, Col);
        Inc(StringArraySet.FTotalEvents);
      end;
    end;
  end;
end;

function TStringArray.SetFieldValue(Index: Integer; FieldName,
  Value: WideString; Client : IACNC32StringArrayNotify = nil): TAA_RESULT;
var I : Integer;
begin
  I := FData.IndexOf(UpperCase(FieldName));
  if I <> -1 then
    Result := SetValue(Index, I, Value, Client)
  else
    Result := AAR_NOT_FOUND;
end;

function TStringArray.SetValue(Index, Col: Integer;
  Value: WideString; Client : IACNC32StringArrayNotify = nil): TAA_RESULT;
begin
  Lock.Acquire;
  try
    Result := AAR_OK;
    if (Index < RecordCount) and (Col < FieldCount) then
      SetData(Index, Col, Value, Client)
    else
      Result := AAR_OUT_OF_BOUNDS;
  finally
    Lock.Release;
  end;
end;

procedure TStringArray.BeginUpdate; stdcall;
begin
  Inc(UpdateCount);
end;

procedure TStringArray.EndUpdate(const Client : IACNC32StringArrayNotify = nil); stdcall;
var I : Integer;
begin
  Dec(UpdateCount);
  if UpdateCount = 0 then begin
    for I := 0 to Self.FClients.AllClientCount - 1 do begin
      if Assigned(FClients.AllClients[I]) and (Client <> FClients.AllClients[I]) then begin
        FClients.AllClients[I].TableChange(Self);
        Inc(StringArraySet.FTotalEvents);
      end;
    end;
  end;
end;

procedure TStringArray.Clear(Client : IACNC32StringArrayNotify = nil);
begin
  BeginUpdate;
  try
    while RecordCount > 0 do
      Self.Delete(0);
  finally
    EndUpdate(Client);
  end;
end;


function TStringArray.Copy(Dest, Source: Integer; Client : IACNC32StringArrayNotify = nil): TAA_RESULT;
var I : Integer;
begin
  BeginUpdate;
  Result := AAR_OK;
  try
    if (Dest < RecordCount) and (Dest >= 0) and (Source < RecordCount) and (Source >= 0) then begin
      for I := 0 to FieldCount - 1 do begin
        SetData(Dest, I, Data[Source, I]);
      end;
    end else
      Result := AAR_OUT_OF_BOUNDS;
  finally
    EndUpdate(Client);
  end;
end;

function TStringArray.Swap(A, B: Integer; Client : IACNC32StringArrayNotify = nil): TAA_RESULT;
var T : string;
    I : Integer;
begin
  BeginUpdate;
  Result := AAR_OK;
  try
    if (A < RecordCount) and (A >= 0) and (B < RecordCount) and (B >= 0) then begin
      for I := 0 to FieldCount - 1 do begin
        T := Data[A, I];
        SetData(A, I, Data[B, I]);
        SetData(B, I, T);
      end;
    end else
      Result := AAR_OUT_OF_BOUNDS;
  finally
    EndUpdate(Client);
  end;
end;

function TStringArray.HTTPGeneratePage(HFile: THANDLE; var CGIPath: WideString;
  CGIRequest: ICGIStringList): Boolean;
var I, J : Integer;
begin
  if CGIPath = '' then begin
    Result := True;
    WriteACNC32HTTPTitle(HFile, 'StringArray: ' + Alias);
    WriteACNC32HTTPHeading(hFile, 2, 'StringArray: ' + Alias);
    WriteStringToHandle(hFile, '<table border="1" align="center" width="80%">' + CarRet);
    WriteStringToHandle(hFile, '<tr>' + CarRet);
    for I := 0 to FieldCount - 1 do begin
      WriteStringToHandle(hFile, Format('<th>%s</th>', [FData[I]]));
    end;
    WriteStringToHandle(hFile, '</tr>' + CarRet);

    for J := 0 to RecordCount - 1 do begin
      WriteStringToHandle(hFile, '<tr>' + CarRet);
      for I := 0 to FieldCount - 1 do begin
        WriteStringToHandle(HFile, Format('<td>%s</td>', [Data[J, I]]));
      end;
      WriteStringToHandle(hFile, '</tr>' + CarRet);
    end;
  end else begin
    Result := False;
  end;
end;


function TStringArray.GetCursor: Integer;
begin
  Result:= Cursor;
end;


procedure TStringArray.SetCursor(ACursor: Integer; const Client: IACNC32StringArrayNotifyA = nil);
var I: Integer;
begin
  if Cursor <> ACursor then begin
    Cursor:= ACursor;
    if UpdateCount = 0 then begin
      for I := 0 to Self.FClients.ClientCountA - 1 do begin
        if Assigned(FClients.ClientsA[I]) and (Client <> FClients.ClientsA[I]) then begin
          FClients.ClientsA[I].CursorChange(Self, Cursor);
          Inc(StringArraySet.FTotalEvents);
        end;
      end;
    end;
  end;
end;

{ TClientList }

{ When adding clients all nil references end up in ClientListA
  because nil can be cast to IACNC32StringArrayNotifyA }
procedure TClientList.AddClient(Client: IACNC32StringArrayNotify);
begin
  try
  {
    if not Assigned(Client) then begin
      if ClientCount >= Length(ClientList) then begin
        SetLength(ClientList, Length(ClientList) + 5);
      end;

      ClientList[ClientCount] := Client;
      Inc(FClientCount);
      Exit;
    end;
    }

    with Client as IACNC32StringArrayNotifyA do begin
      if ClientCountA >= Length(ClientListA) then begin
        SetLength(ClientListA, Length(ClientListA) + 5);
      end;

      ClientListA[ClientCountA] := Client as IACNC32StringArrayNotifyA;
      Inc(FClientCountA);
    end;
  except
    if _ClientCount >= Length(_ClientList) then begin
      SetLength(_ClientList, Length(_ClientList) + 5);
    end;

    _ClientList[_ClientCount] := Client;
    Inc(_FClientCount);
  end;
end;

function TClientList.AllClientCount: Integer;
begin
  Result:= FClientCountA + _FClientCount;
end;

function TClientList._ClientCount: Integer;
begin
  Result := _FClientCount;
end;

function TClientList.ClientCountA: Integer;
begin
  Result := FClientCountA;
end;

constructor TClientList.Create;
begin
  SetLength(_ClientList, 5);
  SetLength(ClientListA, 5);
end;

procedure TClientList.DeleteClient(Client: IACNC32StringArrayNotify);
var I, J : Integer;
begin
  for I := 0 to ClientCountA - 1 do begin
    if Client = ClientListA[I] then begin
      for J := I to ClientCountA - 2 do begin
        ClientListA[J] := ClientListA[J + 1];
      end;
      ClientListA[ClientCountA - 1] := nil;
      Dec(FClientCountA);
      if Length(ClientListA) > (2 * ClientCountA) + 10 then
        SetLength(ClientListA, ClientCountA + 5);
      Exit; // Only delete a single client even on duplicate
    end;
  end;

  for I := 0 to _ClientCount - 1 do begin
    if Client = _ClientList[I] then begin
      for J := I to _ClientCount - 2 do begin
        _ClientList[J] := _Clients[J + 1];
      end;
      _ClientList[_ClientCount - 1] := nil;
      Dec(_FClientCount);
      if Length(_ClientList) > (2 * _ClientCount) + 10 then
        SetLength(_ClientList, _ClientCount + 5);
      Exit; // Only delete a single client even on duplicate
    end;
  end;
end;

function TClientList.GetAllClient(
  Index: Integer): IACNC32StringArrayNotify;
begin
  if Index >= ClientCountA then
    Result:= _Clients[Index - ClientCountA]
  else
    Result:= ClientsA[Index];
end;

function TClientList._GetClient(Index: Integer): IACNC32StringArrayNotify;
begin
  Result := _ClientList[Index];
end;

function TClientList.GetClientA(Index: Integer): IACNC32StringArrayNotifyA;
begin
  Result := ClientListA[Index];
end;

end.
