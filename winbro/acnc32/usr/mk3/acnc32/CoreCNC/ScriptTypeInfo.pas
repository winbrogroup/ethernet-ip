unit ScriptTypeInfo;

interface

uses CoreCnc, CNC32, CNCTypes, Classes, Tokenizer, SysUtils, NCMath;


type
  TScriptLexicon = (
      slInterface,
      slImplementation,
      slVariable,
      slConstant,
      slType,
      slProcedure,
      slFunction,
      slConditional,
      slIndexLoop,
      slConditionalLoop,
      slStartBlock,
      slEndBlock,
      slAssignment,
      slElse,
      slTo,
      slDownTo,
      slThen,
      slDo,
      slLineEnd,
      slCase,
      slOf,
      slColon,
      slComment,
      slPreprocessor,
      slInvalid
  );

  TScriptInstruction = (
      siCall,
      siAssign,
      siConditional,
      siOnBoolean,
      siLineNumber,
      siJump,
      siInc,
      siDec,
      siHashTable,
      siSpecial,
      siComplete
  );

  TBlockCompile = (
      bcMustBegin, // Begin .. End
      bcMayBegin,  // Begin .. End or 1 line of code
      bcNoBegin    // ..End
  );

  TParmPassing = (
      PassByValue,
      PassByReference,
      PassAsConst
  );


  TScriptDataType = (
      dtInteger,
      dtDouble,
      dtString,
      dtBoolean,
      dtArray,
      dtVariant
  );

  TComparatorType = (
      cEqual,
      cGreaterThan,
      cLessThan,
      cLessThanOrEqual,
      cGreaterThanOrEqual,
      cNotEqual,
      cInvalid
  );

  TPCode = packed record
    case Byte of
      0 : (
            Instruction : TScriptInstruction;
            Parameters  : Byte;
            Data        : SmallInt;
      );
      1 : (
            Addr        : Pointer;
      );
  end;


  TVariableDefRecord = class
    Name : string;
    TagClass : TTagClass;
    ItemClass : TTagClass;
    Dims : TArrayDims;
    ParmType : TParmPassing;
    OrdinateCount : Integer;
  end;

  TMethodArrayTag = class(TArrayTag)
  public
    destructor Destroy; override;
    procedure SetArray(const Def : TVariableDefRecord);
  end;


  PRunTimeParameters = ^TRunTimeParameters;
  TRunTimeParameters = array [0..9] of TAbstractTag;
  TParameterVarType = array [0..9] of TParmPassing;

  EACNCScriptError = class(Exception);

  TAbstractACNCScriptProject = class;

  TScriptVariableScope = class(TObject)
  private
    List : TList;
    procedure SetTag(Index : Integer; aTag : TAbstractTag);
    function  GetTag(Index : Integer) : TAbstractTag;
    function  GetCount : Integer;
  public
    constructor Create;
    destructor Destroy; override;
    function Add(aTag : TAbstractTag) : Integer;
    procedure Clean;
    procedure Insert(Index : Integer; aTag : TAbstractTag);
    property Tags [Index : Integer] : TAbstractTag read GetTag write SetTag; default;
    property Count : Integer read GetCount;
  end;

  TVariableList = class(TObject)
  private
    LList : TList;
    function  GetCount : Integer;
    function GetDef(Index : Integer) : TVariableDefRecord;
  public
    constructor Create;
    destructor Destroy; override;
    function Add(aName : string) : Integer;
    procedure Clean;
    property VariableDef [Index : Integer] : TVariableDefRecord read GetDef; default;
    property Count : Integer read GetCount;
    function Data: PPointerList;
  end;

  TACNCScriptParser = class(TStringTokenizer)
  private
  protected
  public
    DontStripComments : Boolean;
    procedure CheckStream;
    procedure ExpectedToken(Token : string);
    procedure ValidSymbol(Symbol : string);
    constructor Create;
    function Read : string; override;
    procedure ReadVariable(var Variable, Suffix : string);
    function SymbolType : TScriptLexicon;
  end;


  TAbstractScript = class(TACNCScriptParser)
  private
    FLineNumber : Integer;
    FProject : TAbstractACNCScriptProject;
    FTagOwner : TTagOwner;
  protected
    function FindTypeList(aName, Suffix : string) : TAbstractTag; virtual;
    function ReadConstant : TAbstractTag; virtual;
    function GetSingleBlock : Boolean;
    procedure ReadVariableList(S : TVariableList; var ParmType : TParmPassing);
    procedure AddVariables(Scope : TScriptVariableScope; S : TVariableList);
  public
    LineInfo : Boolean;
    TypeList : TScriptVariableScope;
    Name : string;
    function ConvertDefToTag(Def : TVariableDefRecord) : TAbstractTag;
    function ConvertScriptToTagType(S : TACNCScriptParser) : TVariableDefRecord;
    property LineNumber : Integer read FLineNumber;
    property TagOwner : TTagOwner read FTagOwner write FTagOwner;
    property SingleBlock : Boolean read GetSingleBlock;
    property Project : TAbstractACNCScriptProject read FProject write FProject;
  end;

  TAbstractScriptMethodTag = class(TGenericTag)
  protected
    LastScriptLine : Integer;
    FScript : TAbstractScript;
    FInstance : Integer;
    function GetStackParameter(Index : Integer) : TAbstractTag;
    function AddInstruction(Inst : TScriptInstruction; aParameters : Byte; aData : SmallInt) : Integer;
    procedure ExecuteSpecial; virtual; abstract;
    function GetAsInteger : Integer; override;
    function GetAsDouble : Double; override;
    function GetAsString : String; override;
    function GetStackParameters: TRunTimeParameters;
  public
    Prototype : TVariableList;
    TypeList : TScriptVariableScope;
    Code : TList;
    constructor Create(aOwner : TTagOwner; aName : string); override;
    destructor Destroy; override;
    procedure Clean; virtual;
    property Script : TAbstractScript read FScript;
    property Instance : Integer read FInstance write FInstance;
    property StackParameter [Index : Integer] : TAbstractTag read GetStackParameter;
  end;

  TScriptMethodTag = class(TAbstractScriptMethodTag)
  private
    MathCompile : Integer;
    procedure CompileBlock(BC : TBlockCompile);
    function FindTypeList(aName, Suffix : string) : TAbstractTag;
    function IsVariable(aTag : TAbstractTag; TC : TTagClass) : Boolean;
    function DoParameterCompile(S : TACNCScriptParser) : TAbstractTag;
    procedure DoMethodCompile(Sm : TAbstractScriptMethodTag; S : TACNCScriptParser);
    procedure DoConditional;
    procedure DoConditionalLoop;
    procedure DoIndexedLoop;
    procedure DoCase;
    procedure DoAssignment(Target : TAbstractTag);
    function  DoBoolean : TAbstractTag;
    function NewMathParser(Parse : string) : TAbstractTag;
    function ResolveVariable(Sender : TObject; const aName : string) : TAbstractTag;
    function ResolveFunction(Sender : TObject; const Func, Params : string) : TAbstractTag;
    function ResolveArray(Sender : TObject; const aName, Suffix : string) : TAbstractTag;
    function GenerateArrayAccessor(aTag : TArrayTag; Suffix : string) : TArrayAccessTag;
    function OrdinalConstant(Token : string) : Integer;
//    function GetStackParameter(Index : Integer) : TAbstractTag; override;
  protected
    function GetIOSize : Integer; override;
    procedure ExecuteSpecial; override;
  public
    MPList : TList;
    procedure Clean; override;
    constructor Create(aOwner : TTagOwner; aName : string); override;
    destructor Destroy; override;
    procedure Compile(ST : TAbstractScript);
    procedure ReadPrototype(S : TAbstractScript; Resultant : Boolean);
  end;

  TScriptParameterTag = class(TOrdinalTag)
  private
    FInstance : TScriptMethodTag; // Top of stack parameters
    Index : Integer;             // This parameter index
  protected
    function GetDataPtr : Pointer; override;
    function GetDataSize : Integer; override;
    function GetAsInteger : Integer; override;
    function GetAsBoolean : Boolean; override;
    function GetAsDouble : Double; override;
    function GetAsString : string; override;
  public
    procedure Refresh; override;
    procedure Assign(aTag : TAbstractTag); override;
    property Instance : TScriptMethodTag read FInstance;
  end;

  PRunTimeStackRecord = ^TRunTimeStackRecord;
  TRunTimeStackRecord = record
    Script : TAbstractScript;
    Method : TScriptMethodTag;
    CodePointer : Integer;
    IParms : TRunTimeParameters;
    IParmCount : Integer;
  end;

  TRunTimeStack = class
  private
    List : TList;
    function GetCount : Integer;
    function GetItem(Index : Integer) : PRunTimeStackRecord;
  public
    constructor Create;
    destructor Destroy; override;
    procedure Clean;
    function Add(RTR : PRunTimeStackRecord) : Integer;
    procedure DeleteTop;
    function  Top : PRunTimeStackRecord;
    property Count : Integer read GetCount;
    property Items [Index : Integer] : PRunTimeStackRecord read GetItem; default;
  end;


  TAbstractACNCScriptProject = class(TObject)
  protected
    FTagOwner : TTagOwner;
    FMathPartial : TStrings;
    ThisTTL : Integer;
    FTTL : Integer;
    FActiveScript : TAbstractScript;
    FActiveMethod : TScriptMethodTag;
    FTraceScript : TAbstractScript; // Only set if the current function has line info
    Stack : TRunTimeStack;
    OwnedIO : TList;
    FCompiled : Boolean;
    Index : Integer;
    EntryMethod : TScriptMethodTag;
    function GetInProcess : Boolean;
    property ActiveScript : TAbstractScript read FActiveScript;
  public
    TypeList : TScriptVariableScope;
    SingleBlock : Boolean;
    Units : TList;
    CallParameters : TRunTimeParameters;
    constructor Create(aOwner : TTagOwner);
    destructor Destroy; override;
    procedure Clean; dynamic;
    procedure Run(ATag: TAbstractTag = nil); virtual;
    procedure Rewind; dynamic;
    function  FindTypeList(aName, Suffix : string) : TAbstractTag; virtual; abstract;
    function  Owns(aTag : TAbstractTag) : Boolean; virtual; abstract;
    function SimpleResolve(aName, Parse : string) : TAbstractTag; virtual; abstract;
    property TagOwner : TTagOwner read FTagOwner;
    property TTL : Integer read FTTL write FTTL;
    property ActiveMethod : TScriptMethodTag read FActiveMethod;
    property TraceScript : TAbstractScript read FTraceScript;
    property Compiled : Boolean read FCompiled;
    property InProcess : Boolean read GetInProcess;
    property MathPartial : TStrings read FMathPartial write FMathPartial;
  end;


const
  ScriptLexicon : array [TScriptLexicon] of PChar = (
                'Interface',
                'Implementation',
                'Var',
                'Const',
                'Type',
                'Procedure',
                'function',
                'if',
                'for',
                'while',
                'begin',
                'end',
                ':=',
                'Else',
                'to',
                'downto',
                'then',
                'do',
                ';',
                'case',
                'of',
                ':',
                '//',
                '@',
                ''
  );

  DataType : array [TScriptDataType] of pChar = (
      'Integer',
      'Double',
      'String',
      'Boolean',
      'Array',
      'Variant'
  );

  DataTypeClass : array [TScriptDataType] of TTagClass = (
      TIntegerTag,
      TDoubleTag,
      TStringTag,
      TMethodTag,
      TArrayTag,
      TStringTag
  );

  DefaultSeperator = ',:();=+-/*@[]''';

{  ZeroParms : TRunTimeParameters = (
            nil, nil, nil, nil, nil, nil, nil, nil, nil, nil
  ); }

resourcestring
  ExpectedFormat = 'Expected [%s]';
  UnexpectedFormat = 'Unexpected [%s]';
  EndOfFileText = 'End Of File';

resourcestring
  NoResultFromProcedure = 'Item Has no Value';
  UnBalancedParenthesis = 'Unbalanced Parenthesis';
  CannotAssign = '[%s] is an invalid assignment target';
  UnknownProcedureOrVariable = 'Unknown Procedure Or Variable [%s]';
  InvalidSymbolLang = 'Invalid Symbol';
  InvalidConditional = 'Invalid Conditional ??';
  InvalidTypeLang = 'Invalid Type';
  VariableAlreadyExistsFormat = 'Variable Already Exists [%s]';
  HasNoValueLang = 'Has no Value';
  InvalideNumberOfParameters = 'Invalid Number of parameters in call';
  ParameterIsVarFormat = 'Parameter [%s] is a Var parameter';
  IncompatableAssignmentFormat = 'Incompatable Assignment [%s] -> [%s]';
  IndexVariableConstantFormat = 'Index Variable [%s] is a Constant';
  IsNotOrdinalConstant = '%s is not an ordinal constant';
  ForLoopExpectedFormat ='For loop expectected [%s] or [%s]';
  SyntaxErrorLang = 'Syntax Error';
  ImbalancedSquareBracesLang = 'Imbalanced Square Braces';
  MissingArraySubscriptFormat = '[%s] Missing Array Subscript';
  IsNotAVariableFormat = '[%s] is not a Variable';
  IsNotAFunctionFormat = '[%s] is not a Function';
  IsNotNumericFormat = '[%s] Is not numeric';
  IsNotArrayFormat = '[%s] Is not an array';
  ImbalancedParenthesisLang = 'Imbalanced Parenthesis';
  ArrayFormatErrorLang = 'Array Format Error';
  WrongArgsForArrayLang = 'Wrong number of args for array';
  ArraySuffixErrorLang = 'Array suffix broken';
  TooManyDimensionsLang = 'Too many dimensions in array';
  ZeroDimensionsLang = 'Array has zero dimension';
  VariableNotArray = 'Variable [%s] is not an array';
  MissingCloseParethesis = 'Missing close parenthesis';
  IncorrectNumberOfIndices = 'Incorrect Number of indices [%d] for array [%s]';

implementation

constructor TAbstractACNCScriptProject.Create(aOwner : TTagOwner);
begin
  inherited Create;
  Stack := TRunTimeStack.Create;
  Units := TList.Create;
  TypeList := TScriptVariableScope.Create;
  OwnedIO := TList.Create;
  FTagOwner := aOwner;
end;

procedure TAbstractACNCScriptProject.Clean;
begin
  Stack.Clean;  // This must be done prior to removing methods
  TypeList.Clean;

  while OwnedIO.Count > 0 do begin
    TAbstractTag(OwnedIO[0]).IsOwned := False;
    OwnedIO.Delete(0);
  end;
  FCompiled := False;
  FActiveScript := nil;
  FTraceScript := nil;
end;

destructor TAbstractACNCScriptProject.Destroy;
var I : Integer;
begin
  Clean;
  for I := 0 to Units.Count - 1 do begin
    if Assigned(Units[I]) then
      TACNCScriptParser(Units[I]).Free;
  end;
  Units.Free;
  TypeList.Free;
  OwnedIO.Free;
  inherited Destroy;
end;

function TAbstractACNCScriptProject.GetInProcess : Boolean;
begin
  Result := Stack.Count > 0;
end;

constructor TACNCScriptParser.Create;
begin
  inherited Create;
  Self.Seperator := DefaultSeperator;
end;


function TACNCScriptParser.Read : string;
var P : Integer;
begin
  Result := Inherited Read;
  try
    if (Result = ':') and (Peek = '=') then
      Result := Result + Read
    else if (Result = '/') and (Peek = '/') then begin
      if DontStripComments then begin
        Result := Result + Read
      end else begin
        Line := Line + 1;
        Result := Read;
      end;
    end else begin
      if Result = '''' then begin
        for P := Position to Length(CurrentLine) do begin
          if CurrentLine[P] =  '''' then begin
            Result := Result + Copy(CurrentLine, Position, P - Position + 1);
            FPosition := P + 1;
            Exit;
          end;
        end;
        Result := Result + Copy(CurrentLine, Position, Length(CurrentLine));
        FPosition := Length(CurrentLine) + 1;
      end;
    end;
  finally
    Self.Working := Result;
  end;
end;

//??????? Change read variable to return
// 1. the variable name
// 2. Any suffix information
// This should allow the later code to work much cleaner.
//
procedure TACNCScriptParser.ReadVariable(var Variable, Suffix : string);
var
  BraceCount : Integer;
  A : Char;
begin
  Suffix := '';

  Variable := Read;
  ValidSymbol(Variable);

  if Peek = '[' then begin
    BraceCount := 1;
    Read;
    repeat
      A := ReadChar;
      case A of
        '[' : Inc(BraceCount);
        ']' : Dec(BraceCount);
      end;
      CheckStream;
      if BraceCount <> 0 then
        Suffix := Suffix + A;
    until (BraceCount = 0);
  end;
end;

procedure TACNCScriptParser.ValidSymbol(Symbol : string);
var I : Integer;
begin
  if Length(Symbol) = 0 then
    raise EACNCScriptError.Create('Unexpected End Of File');
  for I := 1 to Length(Symbol) do begin
    case Symbol[I] of
      #0..#45, #47, #58..#64 :
        raise EACNCScriptError.Create(InvalidSymbolLang);
    end;
  end;
  case Symbol[1] of
    '0'..'9' :
        raise EACNCScriptError.Create(InvalidSymbolLang);
  end;
end;

procedure TACNCScriptParser.CheckStream;
begin
  if EndOfStream then
    raise EACNCScriptError.CreateFmt(UnexpectedFormat, [EndOfFileText]);
end;

procedure TACNCScriptParser.ExpectedToken(Token : string);
begin
  if LowerCase(Read) <> Token then
    raise EACNCScriptError.CreateFmt(ExpectedFormat, [Token]);
end;

function TACNCScriptParser.SymbolType : TScriptLexicon;
var Token : string;
begin
  Token := Self.Read;
  for Result := Low(TScriptLexicon) to High(TScriptLexicon) do
    if Length(ScriptLexicon[Result]) = Length(Token) then
      if CompareText(ScriptLexicon[Result], Token) = 0 then
        Exit;

  Result := slInvalid;
end;

function TAbstractScript.GetSingleBlock : Boolean;
begin
  Result := Project.SingleBlock;
end;

function TAbstractScript.FindTypeList(aName, Suffix : string) : TAbstractTag;
begin
  Result := nil;
end;

function TAbstractScript.ReadConstant : TAbstractTag;
begin
  Result := nil;
end;

// Purpose : Reads a variablelist of the syntax
//
// [TParmPassing] Name1 [, Name2 .. , NameN] : VarClass
// The Results are added to S in the form String[n] = Name Object[N] = ClassType
procedure TAbstractScript.ReadVariableList(S : TVariableList; var ParmType : TParmPassing);
var Token, Name : string;
    I, Start : Integer;
//    TagClass : TTagClass;
    Def : TVariableDefRecord;
begin
  ParmType := passByValue;

  Token := Self.Read;
  if CompareText(ScriptLexicon[slConstant], Token) = 0 then
    ParmType := passAsConst
  else if CompareText(ScriptLexicon[slVariable], Token) = 0 then
    ParmType := passByReference
  else
    Self.Push;

  Start := S.Count;
  Token := ',';
  while Token = ',' do begin
    Name := Self.Read;
    Token := Self.Read;
    S.Add(Name);
  end;

  Self.Push;
  ExpectedToken(':');

  Def := ConvertScriptToTagType(Self {, S[Start]});
  for I := Start to S.Count - 1 do begin
    S[I].TagClass := Def.TagClass;
    S[I].ParmType := Def.ParmType;
    S[I].ItemClass := Def.ItemClass;
    S[I].Dims := Def.Dims;
    S[I].OrdinateCount := Def.OrdinateCount;
  end;
end;

procedure TAbstractScript.AddVariables(Scope : TScriptVariableScope; S : TVariableList);
var
  Variable : TAbstractTag;
  I : Integer;
begin
  for I := 0 to S.Count -1 do begin
    Variable := ConvertDefToTag(S[I]);
//    Variable := S[I]^.TagClass.Create(TagOwner, S[I]^.Name);
    Scope.Add(Variable);
  end;
end;


function TAbstractScript.ConvertScriptToTagType(S : TACNCScriptParser) : TVariableDefRecord;
var Token : string;
    aTag : TAbstractTag;
begin
  Result:= TVariableDefRecord.Create;
  Token := S.Read;
  if CompareText(Token, DataType[dtDouble]) = 0  then
    Result.TagClass := TFloatTag
  else if CompareText(Token, DataType[dtInteger]) = 0 then
    Result.TagClass := TOrdinalTag
  else if CompareText(Token, DataType[dtString]) = 0 then
    Result.TagClass := TAlphaTag
  else if CompareText(Token, DataType[dtBoolean]) = 0 then
    Result.TagClass := TOrdinalTag
  else if CompareText(Token, DataType[dtArray]) = 0 then begin
    ExpectedToken('[');
    Result.OrdinateCount := 0;
    Result.TagClass := TMethodArrayTag;
    Token := S.Read;
    FillChar(Result.Dims, Sizeof(Result.Dims), 0);
    while Token <> ']' do begin
      if Result.OrdinateCount > High(TArrayOrdinate) then
        raise EACNCScriptError.Create(TooManyDimensionsLang);
      try
        aTag := FindTypeList(S.LastToken, '');
        if aTag <> nil then begin
          if aTag.IsConstant and aTag.HasValue then
            Result.Dims[Result.OrdinateCount] := aTag.AsInteger;
        end else begin
          Result.Dims[Result.OrdinateCount] := StrToInt(S.LastToken);
        end;
        if Result.Dims[Result.OrdinateCount] <= 0 then
          raise EACNCScriptError.Create(ZeroDimensionsLang);
        Token := S.Read;
        Inc(Result.OrdinateCount);
        if Token = ',' then begin
          Token := S.Read;
        end;
      except
        on E : EConvertError do raise EACNCScriptError.Create('Array definition error');
      end;
    end;
    ExpectedToken(ScriptLexicon[slOf]);
    Result.ItemClass := ConvertScriptToTagType(S).TagClass;
  end else
    raise EACNCScriptError.Create('Invalid Variable Type');
end;

function TAbstractScript.ConvertDefToTag(Def : TVariableDefRecord) : TAbstractTag;
begin
  if Def.TagClass <> TMethodArrayTag then
    Result := Def.TagClass.Create(TagOwner, Def.Name)
  else begin
    Result := TMethodArrayTag.Create(TagOwner, Def.Name);
    TMethodArrayTag(Result).SetArray(Def);
  end;
end;


constructor TAbstractScriptMethodTag.Create(aOwner : TTagOwner; aName : string);
begin
  inherited;
  IsWritable := False;
  ProtoType := TVariableList.Create;
  TypeList := TScriptVariableScope.Create;
  Code := TList.Create;
end;

procedure TAbstractScriptMethodTag.Clean;
begin
  TypeList.Clean;
end;

destructor TAbstractScriptMethodTag.Destroy;
begin
  Clean;
  Prototype.Free;
  TypeList.Free;
  Code.Free;
  inherited Destroy;
end;

function TAbstractScriptMethodTag.AddInstruction(Inst : TScriptInstruction; aParameters : Byte; aData : SmallInt) : Integer;
var Pc : TPCode;
begin
  if Script.LineInfo and (Script.Line <> LastScriptLine) then begin
    Pc.Instruction := siLineNumber;
    Pc.Parameters := 1;
    Pc.Data := 0;

    Code.Add(Pointer(Pc));
    Code.Add(Pointer(Script.Line));
    LastScriptLine := Script.Line;
  end;

  Pc.Instruction := Inst;
  Pc.Parameters := aParameters;
  Pc.Data := aData;
  Result := Code.Add(Pointer(Pc));
end;

constructor TScriptMethodTag.Create(aOwner : TTagOwner; aName : string);
begin
  inherited;
  MPList := TList.Create;
end;

procedure TScriptMethodTag.Clean;
var I : Integer;
begin
  inherited Clean;
  Code.Clear;

  for I := 0 to MPList.Count - 1 do
    TObject(MPList[I]).Free;
end;

destructor TScriptMethodTag.Destroy;
begin
  Clean;
  MPList.Free;
  inherited Destroy;
end;

function TAbstractScriptMethodTag.GetStackParameter(Index : Integer) : TAbstractTag;
begin
  Result := Script.Project.Stack[Instance].IParms[Index];
end;

function TAbstractScriptMethodTag.GetStackParameters: TRunTimeParameters;
begin
  Result:= Script.Project.Stack[Instance].IParms;
end;

procedure TScriptMethodTag.ReadPrototype(S : TAbstractScript; Resultant : Boolean);
  procedure ReadParameterList;
  var Token : string;
    ParmType : TParmPassing;
    Prev, I : Integer;
  begin
    Token := S.Read;
    if Token = '(' then begin
      while Token <> ')' do begin
        Prev := Prototype.Count;
        S.ReadVariableList(Prototype, ParmType);
        for I := Prev to Prototype.Count - 1 do
          Prototype[I].ParmType := ParmType;
        Token := S.Read; // Next Token is either ')' or new var
        S.CheckStream;
      end;
    end else
      S.Push;
  end;
var
  aTag : TAbstractTag;
  Def : TVariableDefRecord;
begin
  ReadParameterList;

  if Resultant then begin
    S.ExpectedToken(':');
    Def := S.ConvertScriptToTagType(S);
    Def.Name := 'Result';
    aTag := S.ConvertDefToTag(Def);
    HasValue := True;
    TypeList.Insert(0, aTag);
  end else begin
    HasValue := False;
  end;
  IsWritable := False;
  S.ExpectedToken(';');
end;


function TScriptMethodTag.IsVariable(aTag : TAbstractTag; TC : TTagClass) : Boolean;
begin
  if aTag = nil then
    Result := False
  else
    Result := (aTag is TC) and aTag.IsWritable;
end;


function TScriptMethodTag.FindTypeList(aName, Suffix : string) : TAbstractTag;
var I : Integer;
begin
  Result := nil;
  aName := Trim(aName);
  try
    for I := 0 to TypeList.Count - 1 do begin
      Result := TAbstractTag(TypeList[I]);
      if CompareText(aName, Result.Name) = 0 then begin
        Exit;
      end;
    end;

    Result := Script.FindTypeList(aName, '');
  finally
    if (Result <> nil) then begin
      if (Suffix <> '') and (Result is TArrayTag) then
        Result := GenerateArrayAccessor(TArrayTag(Result), Suffix)
      else if Result is TArrayTag then
        raise EACNCScriptError.CreateFmt(MissingArraySubscriptFormat, [Result.Name])
      else if (Suffix <> '') and not(Result is TArrayTag) then
        raise EACNCScriptError.CreateFmt(VariableNotArray, [Result.Name])
    end;
  end;
end;


//
// Called from math parser
function TScriptMethodTag.ResolveVariable(Sender : TObject; const aName : string) : TAbstractTag;
begin
  Result := FindTypeList(aName, '');
  if Result = nil then
    raise EACNCScriptError.CreateFmt(UnknownProcedureOrVariable, [aName]);

  if not Result.HasValue then
    raise EACNCScriptError.CreateFmt(IsNotAVariableFormat, [aName]);

  if not(Result is TGenericTag) then
    raise EACNCScriptError.CreateFmt(IsNotNumericFormat, [aName]);

  if Result is TAbstractScriptMethodTag then begin
    AddInstruction(siCall, 1, 0);
    Code.Add(Result);
  end;
end;


function TScriptMethodTag.ResolveFunction(Sender : TObject; const Func, Params : string) : TAbstractTag;
var S : TACNCScriptParser;
    Tmp : string;
begin
  Tmp := Trim(Func);
  Result := FindTypeList(Tmp, '');
  if Result <> nil then begin
    if not Result.HasValue then
      raise EACNCScriptError.CreateFmt(IsNotAVariableFormat, [Tmp]);
    if not (Result is TAbstractScriptMethodTag) then
      raise EACNCScriptError.CreateFmt(IsNotAFunctionFormat, [Tmp]);
    S := TACNCScriptParser.Create;
    try
      S.Text := '(' + Params + ')';
      DoMethodCompile(TAbstractScriptMethodTag(Result), S);
    finally
      S.Free;
    end;
    Exit;
  end else
    raise EACNCScriptError.CreateFmt(UnknownProcedureOrVariable, [Tmp]);

  Result := nil;
end;

function TScriptMethodTag.ResolveArray(Sender : TObject; const aName, Suffix : string) : TAbstractTag;
begin
  Result := FindTypeList(aName, Suffix);
end;

// Given the input of [Formula1, Formula2, ..  Formulan] where N is the total number of ordinates
// Generate the following code for MP compilation
// ((Formula1) * IndexOfOrdinate[N-1]) + ((Formula2) * IndexOfOrdinate[N-2]) .. + FormulaN
function TScriptMethodTag.GenerateArrayAccessor(aTag : TArrayTag; Suffix : string) : TArrayAccessTag;
var I, Index : Integer;
    Working : string;
    Token : Char;
    MPCompile : string;
    Comma : Integer;
begin
  MPCompile := '';
  Working := '';

  Index := 1;

  for I := 0 to aTag.OrdinateCount - 1 do begin
    if Length(Suffix) < Index then
      raise EACNCScriptError.CreateFmt(IncorrectNumberOfIndices, [aTag.OrdinateCount, aTag.Name]);

    Comma := -1;
    while (Comma < 0) and (Length(Suffix) >= Index) do begin
      Token := Suffix[Index];
      case Token of
        '(', '[' : Dec(Comma);
        ')', ']', ',' : Inc(Comma);
      end;
      if (Comma < 0) or (Token <> ',') then
        Working := Working + Token;
      Inc(Index);
    end;
    if I = 0 then
      MPCompile := Working
    else
      MPCompile := MPCompile + '+((' + Working + ')*' + IntToStr(aTag.IndexOfOrdinate(I)) + ')';

    Working := '';
  end;

  Result := TArrayAccessTag.Create(Script.TagOwner, aTag.Name + 'Accessor');
  Result.DataSource := aTag;
  MPList.Add(Result); // NOTE Adding an array accessor here may be cleaned up
                      // on remove NewMathParser constant checking
                      // I will have to revisit constant checking to ensure that
                      // ArrayAccessors do not get cleaned.
  Result.Index := NewMathParser(MPCompile);

//  MPList.Add(Result.Index);
end;

function TScriptMethodTag.NewMathParser(Parse : string) : TAbstractTag;
var //Entry, I : Integer;
//    FTag : TFloatTag;
//    Constant : Boolean;
    MTag : TNCMathParser;
begin
//  Entry := MPList.Count;

  Parse := Trim(Parse);
  Result := Script.Project.SimpleResolve('#', Parse);
  if Result <> nil then begin
    MPList.Add(Result);
    Exit;
  end;
  if Result = nil then
    Result := FindTypeList(Parse, '');
  if Result <> nil then begin
    if Result is TArrayTag then
      raise EACNCScriptError.CreateFmt(MissingArraySubscriptFormat, [Result.Name]);
    Exit;
  end;

  Inc(MathCompile);
  try
    MTag := TNCMathParser.Create(Owner, 'Math');
    try
      MTag.OnResolveVariable := ResolveVariable;
      MTag.OnResolveFunction := ResolveFunction;
      MTag.OnResolveArray := ResolveArray;
  //    MTag.Partial := Script.Project.MathPartial;
      MTag.IsConstant := True;
      MTag.Compile(Parse);
      Result:= MTag.ExtractSimpleUnderlyingTag;
      if Result <> nil then begin
        MTag.Free;
        Exit;
      end
      else
        MPList.Add(MTag);
    except
      on E : Exception do begin
        MTag.Free;
        raise EACNCScriptError.Create(E.Message);
      end;
    end;
  finally
    Dec(MathCompile);
  end;

  Result := MTag;

{  Constant := True;
  if MathCompile = 0 then begin
    for I := Entry to MPList.Count - 1 do
      if not TNCMathParser(MPList[I]).IsConstant then
        Constant := False;

    if Constant then begin
      FTag := TFloatTag.Create(Script.TagOwner, '');
      FTag.AsDouble := TNCMathParser(MPList[Entry]).AsDouble;

      while MPList.Count > Entry do begin
        TAbstractTag(MPList[Entry]).Free;
        MPList.Delete(Entry);
      end;

      MPList.Add(FTag);
      Result := FTag;
    end;
  end; }
end;

// Entry Conditions
// The method name and the opening bracket have been read
//
// This function must Read each comma seperated parameter note that
// Commas that are nested in brackets are ignored.
//
// Do not assume S = Script
function TScriptMethodTag.DoParameterCompile(S : TACNCScriptParser) : TAbstractTag;
var PareCount, BraceCount : Integer;
    Token : Char;
    Working : string;
begin
  Working := '';
  Parecount := 1; // The opening has been read
  BraceCount := 0;

  repeat
    Token := S.ReadChar;
    if (Token = ')') then
      Dec(PareCount)
    else if (Token = '(') then
      Inc(PareCount)
    else if (Token = ']') then
      Dec(BraceCount)
    else if (Token = '[') then
      Inc(BraceCount);

    if BraceCount < 0 then
      raise EACNCScriptError.Create(ImbalancedSquareBracesLang);

    Working := Working + Token;

    if Token = ';' then
      raise  EACNCScriptError.CreateFmt(UnexpectedFormat, [';']);
    if S.EndOfStream then
      raise  EACNCScriptError.Create(SyntaxErrorLang);

    S.CheckStream;
  until (PareCount = 0) or
        ((Token = ',') and (PareCount = 1) and (BraceCount = 0));


  System.Delete(Working, Length(Working), 1); // Remove the last character
  Result := NewMathParser(Working);
end;

//
// Syntax
//
// if (Boolean)
//   Block
// [else [if]]
//
procedure TScriptMethodTag.DoConditional;
var aTag : TAbstractTag;
    FalseDest, TrueDest : Integer;
begin
  aTag := DoBoolean;

  //Condition : 3 Parameters LS, RS, FalsePtr
  AddInstruction(siOnBoolean, 2, 0);
  Code.Add(aTag);

  FalseDest := Code.Add(Pointer(0)); // Reserve space for fail destination
  Script.ExpectedToken(ScriptLexicon[slThen]);

// ???????  CompileBlock(bcMustBegin);
  CompileBlock(bcNoBegin);

  if CompareText(Script.Read, ScriptLexicon[slElse]) = 0 then begin
    AddInstruction(siJump, 1, 0);     // At the end of the compiled block
    TrueDest := Code.Add(Pointer(0)); // Reserve space for ommiting the else
    Code[FalseDest] := Pointer(Code.Count); // Set the destination for condition fail
    if Script.Read = ScriptLexicon[slConditional] then begin
      DoConditional; // Read if so recurse
    end else begin
      Script.Push;
//????????      CompileBlock(bcMustBegin); // Else Else
      CompileBlock(bcNoBegin); // Else Else
    end;
    Code[TrueDest] := Pointer(Code.Count) // If (else / else if) set the true dest
  end else begin
    Script.Push;
    Code[FalseDest] := Pointer(Code.Count); // no else then fail go here
  end;
end;

// Syntax
//
// while ( Boolean )
//   Block
procedure TScriptMethodTag.DoConditionalLoop;
var aTag : TAbstractTag;
    StartDest, EndDest : Integer;
begin
  StartDest := Code.Count;
  aTag := DoBoolean;
  Script.ExpectedToken(ScriptLexicon[slDo]);
  //Condition : 4 Parameters, Boolean, FalsePtr
  AddInstruction(siOnBoolean, 2, 0);
  Code.Add(aTag);
  EndDest := Code.Add(Pointer(0)); // Reserve space for fail destination

//????????????  CompileBlock(bcMustBegin);
  CompileBlock(bcNoBegin); // Else Else

  AddInstruction(siJump, 1, 0);
  Code.Add(Pointer(StartDest));
  Code[EndDest] := Pointer(Code.Count); // Set the destination for condition fail
end;

// Syntax
// for Variable := Numeric to Numeric
//   Block
procedure TScriptMethodTag.DoIndexedLoop;
var IndexVar : TAbstractTag;
    StartNum, EndNum : TAbstractTag;
    Working, Suffix : string;
    ForStart, ForEnd : Integer;
    IndexType : TScriptInstruction;
begin
  Script.ReadVariable(Working, Suffix);
  IndexVar := FindTypeList(Working, Suffix);
  if IndexVar = nil then
    raise EACNCScriptError.CreateFmt(UnknownProcedureOrVariable, [Working]);
  if not IsVariable(IndexVar, TIntegerTag) then
    raise EACNCScriptError.CreateFmt(IndexVariableConstantFormat, [IndexVar.Name]);

  Script.ExpectedToken(ScriptLexicon[slAssignment]);

// Replace with ReadNumeric
  Working := '';
  while Script.SymbolType = slInvalid do begin
    Working := Working + Script.LastToken;
    Script.CheckStream;
  end;

  StartNum := NewMathParser(Working);

  if CompareText(Script.LastToken, ScriptLexicon[slTo]) = 0 then
    IndexType := siInc
  else if CompareText(Script.LastToken, ScriptLexicon[slDownTo]) = 0 then
    IndexType := siDec
  else
    raise EACNCScriptError.CreateFmt(ForLoopExpectedFormat,
          [ScriptLexicon[slTo], ScriptLexicon[slDownTo]]);

// Replace with ReadNumeric
  Working := '';
  while Script.SymbolType = slInvalid do begin
    Working := Working + Script.LastToken;
    Script.CheckStream;
  end;


  EndNum := NewMathParser(Working);
  Script.Push;
  Script.ExpectedToken(ScriptLexicon[slDo]);

  AddInstruction(siAssign, 2, 0); // Assign the index start
  Code.Add(IndexVar);
  Code.Add(StartNum);

  // Add Conditional and record position
  if IndexType = siInc then
    ForStart := AddInstruction(siConditional, 3, Ord(cLessThanOrEqual))
  else
    ForStart := AddInstruction(siConditional, 3, Ord(cGreaterThanOrEqual));

  Code.Add(IndexVar);
  Code.Add(EndNum);
  ForEnd := Code.Add(nil);

//?????????  CompileBlock(bcMustBegin);
  CompileBlock(bcNoBegin); // Else Else

  AddInstruction(IndexType, 1, 1);
  Code.Add(IndexVar);
  AddInstruction(siJump, 1, 0);
  Code.Add(Pointer(ForStart));

  Code[ForEnd] := Pointer(Code.Count);
end;

function TScriptMethodTag.OrdinalConstant(Token : string) : Integer;
begin
  try
    Result := StrToInt(Token);
  except
    on E : EConvertError do
      raise EACNCScriptError.CreateFmt(IsNotOrdinalConstant, [Token]);
  end;
end;


procedure TScriptMethodTag.DoCase;
var
  HashTable, ExitTable : TList;
  IndexVar : TAbstractTag;
  HashTableEntry : Integer;
  I : Integer;
  PC : TPCode;
  aName, Suffix : string;
begin
  Script.ReadVariable(aName, Suffix);
  IndexVar := FindTypeList(aName, Suffix);
  if IndexVar = nil then
    raise EACNCScriptError.CreateFmt(UnknownProcedureOrVariable, [Script.LastToken]);
//  if not IsVariable(IndexVar, TIntegerTag) then
//    raise EACNCScriptError.CreateFmt(IndexVariableConstantFormat, [IndexVar.Name]);

  Script.ExpectedToken(ScriptLexicon[slOf]);

  AddInstruction(siJump, 1, 0);
  HashTableEntry := Code.Add(nil);

  HashTable := TList.Create;
  ExitTable := TList.Create;
  try
    while Script.SymbolType = slInvalid do begin
      HashTable.Add(Pointer(OrdinalConstant(Script.LastToken)));
      HashTable.Add(Pointer(Code.Count));
      Script.ExpectedToken(ScriptLexicon[slColon]);
      CompileBlock(bcMayBegin);
      AddInstruction(siJump, 1, 0);
      ExitTable.Add(Pointer(Code.Add(nil)));
    end;

    Code[HashTableEntry] := Pointer(Code.Count);
    // NOTE I Dont use AddInstruction to avoid line number info here
    PC.Instruction := siHashTable;
    PC.Parameters := HashTable.Count + 1;
    PC.Data := 0;
    Code.Add(PC.Addr);
    Code.Add(IndexVar);
    for I := 0 to HashTable.Count - 1 do
      Code.Add(HashTable[I]);

    if CompareText(Script.LastToken, ScriptLexicon[slElse]) = 0 then
      CompileBlock(bcNoBegin)
    else begin
      Script.Push;
      Script.ExpectedToken(ScriptLexicon[slEndBlock]);
    end;

    for I := 0 to ExitTable.Count - 1 do
      Code[Integer(ExitTable[I])] := Pointer(Code.Count);

  finally
    HashTable.Free;
    ExitTable.Free;
  end;
end;

//
// A Boolean is expressed as
// ( Expression [<, >, =, <>, <=, >=] Expression )
//
function  TScriptMethodTag.DoBoolean : TAbstractTag;
var Working : string;
    Token : Char;
    PareCount : Integer;
begin
  Script.ExpectedToken('(');

  Working := '';
  PareCount := 1;

  while PareCount > 0 do begin
    Token := Script.ReadChar;
    case Token of
      '(' : Inc(PareCount);
      ')' : Dec(PareCount);
      ';' : raise EACNCScriptError.Create(MissingCloseParethesis);
    end;

    Script.CheckStream;

    if PareCount > 0 then
      Working := Working + Token;
  end;

  Result := NewMathParser(Working);
end;


procedure AssignmentClassValid(TC : TTagClass; Src : TAbstractTag);
//var Sc : TTagClass;
begin
{
  Sc := TTagClass(Src.ClassType);
  if Src is TArrayAccessTag then
    Sc := TArrayAccessTag(Src).DataSource.ItemClass;
  if (TC.InheritsFrom(TIntegerTag)) or (TC.InheritsFrom(TDoubleTag)) then
    if not Sc.InheritsFrom(TIntegerTag) and
       not Sc.InheritsFrom(TDoubleTag) then
//           Sc.InheritsFrom(TMathParser) then
      raise EACNCScriptError.CreateFmt(IncompatableAssignmentFormat, [Src.Name, TC.ClassName]);
 }
//  if (TC.InheritsFrom(TStringTag)) and not Sc.InheritsFrom(TStringTag) then
//    raise EACNCScriptError.CreateFmt('Incompatable Assignment [%s] -> [%s]', [Src.Name, TC.ClassName]);
end;

(*
procedure TScriptMethodTag.AssignmentValid(Target, Src : TAbstractTag);
begin
  if Script.Project.OwnedIO.IndexOf(Target) <> -1 then
    Target.IsOwned := True;

{  if not Target.IsScript then
    raise EACNCScriptError.CreateFmt(CannotAssign, [Target.Name]); }

  if not Target.IsWritable then
    raise EACNCScriptError.CreateFmt(CannotAssign, [Target.Name]);

  if Target.IsSystem and not Script.Project.Owns(Target) then
    raise EACNCScriptError.CreateFmt(CannotAssign, [Target.Name]);

  AssignmentClassValid(TTagClass(Target.ClassType), Src);
end;
*)
//
// Variable := Numeric
// Entry condition is Variable := has been read
procedure TScriptMethodTag.DoAssignment(Target : TAbstractTag);
var Working : string;
    MP : TAbstractTag;
    Token : Char;
begin
  Working := '';

  Token := Script.ReadChar;
  while Token <> ScriptLexicon[slLineEnd] do begin
    Working := Working + Token;
    Token := Script.ReadChar;
  end;

  MP := FindTypeList(Working, '');
  if not Assigned(MP) then
    MP := NewMathParser(Working);

//  AssignmentValid(Target, MP);

  AddInstruction(siAssign, 2, 0);
  Code.Add(Target);
  Code.Add(MP);
end;


procedure TScriptMethodTag.DoMethodCompile(Sm : TAbstractScriptMethodTag; S : TACNCScriptParser);
var
  I : Integer;
  aTag : TAbstractTag;
  Parameters : TList;
begin
  if Sm.Prototype.Count > 0 then
    S.ExpectedToken('(');

  Parameters := TList.Create;
  try
    for I := 0 to Sm.Prototype.Count - 1 do begin
      aTag := DoParameterCompile(S);
      if aTag = nil then
        raise EACNCScriptError.CreateFmt(UnknownProcedureOrVariable, ['I appear to have mislaid the name SORRY']);
      AssignmentClassValid(Sm.Prototype[I].TagClass, aTag);
      Parameters.Add(aTag);
    end;
    AddInstruction(siCall, 1 + Sm.Prototype.Count, 0);
    Code.Add(Sm);
    for I := 0 to Sm.Prototype.Count - 1 do begin
      if (Sm.Prototype[I].ParmType = passByReference) and
         not TAbstractTag(Parameters[I]).IsWritable then
         raise EACNCScriptError.CreateFmt(ParameterIsVarFormat, [Sm.Prototype[I].Name]);
      Code.Add(Parameters[I]);
    end;

  finally
    Parameters.Free;
  end;
end;



// Syntax
// {
// Code
// {
procedure TScriptMethodTag.CompileBlock(BC : TBlockCompile);
var
    aTag : TAbstractTag;
    TopLevel : TScriptLexicon;
    Token, Suffix : string;
begin
  case BC of
    bcMustBegin :
      if CompareText(Script.Read, ScriptLexicon[slStartBlock]) <> 0 then
        raise EACNCScriptError.CreateFmt(ExpectedFormat, [ScriptLexicon[slStartBlock]]);
    bcMayBegin :
      if CompareText(Script.Read, ScriptLexicon[slStartBlock]) = 0 then
        BC := bcMustBegin
      else
        Script.Push;
  end;

  TopLevel := slInvalid;

  while TopLevel <> slEndBlock do begin
    case Script.SymbolType of
      slConditional : DoConditional;
      slIndexLoop : DoIndexedLoop;
      slConditionalLoop : DoConditionalLoop;
      slEndBlock : TopLevel := slEndBlock;
      slCase : DoCase;
      slInvalid : begin
        Script.Push;
        Script.ReadVariable(Token, Suffix);
        aTag := FindTypeList(Token, Suffix);
        if aTag = nil then
          raise EACNCScriptError.CreateFmt(UnknownProcedureOrVariable, [Token]);
        if aTag is TAbstractScriptMethodTag then begin
          DoMethodCompile(TAbstractScriptMethodTag(aTag), Script);
          Script.ExpectedToken(';');
{        end else if aTag is TArrayTag then begin
          aTag := GenerateArrayAccessor(TArrayTag(aTag));
          Script.ExpectedToken(ScriptLexicon[slAssignment]);
          DoAssignment(aTag); }
        end else begin
          Script.ExpectedToken(ScriptLexicon[slAssignment]);
          DoAssignment(aTag);
        end;
      end;
    else
      raise EACNCScriptError.CreateFmt(UnexpectedFormat, [Script.LastToken]);
    end;
    if BC = bcMayBegin then
      TopLevel := slEndBlock;
  end;
end;


// Syntax
// [Var ParameterList]
// [Const ConstDefinition]
// Block
procedure TScriptMethodTag.Compile(ST : TAbstractScript);
var
  TopLevel : TScriptLexicon;
  S : TVariableList;
  aTag : TAbstractTag;
  PTag : TScriptParameterTag;
  I : Integer;
  ParmType : TParmPassing;
begin
  FScript := St;
  TopLevel := slInvalid;
  MathCompile := 0; // Depth of call for math compilations
  for I := 0 to Prototype.Count - 1 do begin
    PTag := TScriptParameterTag.Create(Script.TagOwner, Prototype[I].Name);
    PTag.FInstance := Self;
    PTag.Index := I;
    if Prototype[I].ParmType = passAsConst then
      PTag.IsWritable := False;

    TypeList.Add(PTag);
  end;

  while TopLevel <> slImplementation do begin
    case St.SymbolType of
      slStartBlock     : begin
                           ST.Push; // A bit OTT but it does mean all is compiled
                           CompileBlock(bcMustBegin);
                           AddInstruction(siComplete, 0, 0);
                           Exit;
                         end;
      slVariable       : TopLevel := slVariable;
      slConstant       : TopLevel := slConstant;
      slInvalid        : begin
                           ST.Push;
                           case TopLevel of
                             slVariable: begin
                                           S := TVariableList.Create;
                                           try
                                             ST.ReadVariableList(S, ParmType);
                                             Script.AddVariables(TypeList, S);
                                             Script.ExpectedToken(';');
                                           finally
                                             S.Free;
                                           end;
                                         end;

                             slConstant: begin
                                           aTag := St.ReadConstant;
                                           TypeList.Add(aTag);
                                         end;
                           else
                             raise EACNCScriptError.CreateFmt(UnExpectedFormat, ['Command']);
                           end;
                         end;
    else
      ST.Push;
      raise EACNCScriptError.CreateFmt(UnExpectedFormat, [ST.Read]);
    end;
  end;
end;

procedure TAbstractACNCScriptProject.Rewind;
begin
  Stack.Clean;
end;

procedure TAbstractACNCScriptProject.Run(ATag: TAbstractTag);
var
  PC : TPCode;
  Code : TList;
  RunTime : PRunTimeStackRecord;

  procedure SetActiveMethod;
  begin
    with PRunTimeStackRecord(Stack[Stack.Count - 1])^ do begin
      FActiveMethod := Method;
      FActiveScript := Method.Script;
      ActiveMethod.Instance := Stack.Count - 1;
      Index := CodePointer;
    end;

    Code := ActiveMethod.Code;
  end;


  procedure RunConditional;
  var
    LS, RS : TAbstractTag;
    B : Boolean;
  begin
    LS := Code[Index+1];
    RS := Code[Index+2];
    case TComparatorType(PC.Data) of
      cEqual               : B := LS.AsDouble = RS.AsDouble;
      cGreaterThan         : B := LS.AsDouble > RS.AsDouble;
      cLessThan            : B := LS.AsDouble < RS.AsDouble;
      cLessThanOrEqual     : B := LS.AsDouble <= RS.AsDouble;
      cGreaterThanOrEqual  : B := LS.AsDouble >= RS.AsDouble;
      cNotEqual            : B := LS.AsDouble <> RS.AsDouble;
    else
      raise EACNCScriptError.Create(InvalidConditional);
    end;
    if B then Inc(Index, PC.Parameters + 1)
         else Index := Integer(Code[Index + 3]);
  end;

  procedure RunDoBoolean;
  var
    aTag : TAbstractTag;
  begin
    aTag := Code[Index+1];
    if aTag.AsBoolean then
      Inc(Index, PC.Parameters + 1)
    else
      Index := Integer(Code[Index + 2]);
  end;

  procedure RunAssign;
  begin
    TAbstractTag(Code[Index+1]).Assign(TAbstractTag(Code[Index+2]));
    Inc(Index, PC.Parameters + 1);
  end;

  procedure RunCall;
  var I : Integer;
      aTag : TAbstractTag;
  begin
    Inc(Index);
    FActiveMethod := Code[Index];
    FActiveScript := ActiveMethod.Script;
    New(RunTime);
    System.FillChar(RunTime^.IParms, 0, Sizeof(RunTime^.IParms));

    if ActiveMethod.Prototype.Count + 1 <> PC.Parameters then
      raise EACNCScriptError.Create(InvalideNumberOfParameters);
    for I := 0 to ActiveMethod.Prototype.Count - 1 do begin
      Inc(Index);
      case ActiveMethod.Prototype[I].ParmType of
        passByReference,
        passAsConst : aTag := TAbstractTag(Code[Index]);
      else
        aTag := TGenericTag.Duplicate(TAbstractTag(Code[Index]));
      end;
      RunTime^.IParms[I] := aTag;
    end;
    Inc(Index);

    Stack.Top.CodePointer := Index;

    RunTime^.Script := ActiveScript;
    RunTime^.Method := ActiveMethod;
    RunTime^.IParmCount := ActiveMethod.Prototype.Count;
    RunTime^.CodePointer := 0;
    ActiveMethod.Instance := Stack.Add(RunTime);
    SetActiveMethod;
    Index := 0;
  end;

  procedure RunHashTable;
  var Hash : Integer;
      I : Integer;
  begin
    Hash := TAbstractTag(Code[Index + 1]).AsInteger;
    I := 2;
    while I < PC.Parameters do begin
      if Hash = Integer(Code[Index + I]) then begin
        Index := Integer(Code[Index + I + 1]);
        Exit;
      end;
      Inc(I, 2);
    end;
    Inc(Index, PC.Parameters + 1);
  end;

begin

  if Stack.Count = 0 then begin
    New(RunTime);
    RunTime^.IParms := CallParameters;
    RunTime^.Method := EntryMethod;
    RunTime^.Script := EntryMethod.Script;
    RunTime^.CodePointer := 0;

    if aTag <> nil then begin
      RunTime^.IParms[0] := aTag;
      RunTime^.IParmCount := 0;
    end
    else begin
      RunTime^.IParmCount := 0;
    end;
    
    Stack.Add(RunTime);
  end;

  SetActiveMethod;

//  try
    try
      while Index < Code.Count do begin
        Dec(ThisTTL);
        if ThisTTL < -20000 then
          Exit;
        PC.Addr := Code[Index];
        case PC.Instruction of
          siCall        : RunCall;
          siAssign      : RunAssign;
          siConditional : RunConditional;
          siOnBoolean   : RunDoBoolean;
          siLineNumber  : begin
                            ActiveScript.FLineNumber := Integer(Code[Index+1]);
                            FTraceScript := ActiveScript;
                            Inc(Index, PC.Parameters + 1);
                            if ActiveScript.SingleBlock and (ThisTTL < 0) then
                              Exit;
                          end;
          siJump        : Index := Integer(Code[Index+1]);

          siInc       : with TAbstractTag(Code[Index+1]) do begin
                            AsInteger := AsInteger + 1;
                            Inc(Index, PC.Parameters + 1);
                          end;

          siDec       : with TAbstractTag(Code[Index+1]) do begin
                            AsInteger := AsInteger - 1;
                            Inc(Index, PC.Parameters + 1);
                          end;

          siHashTable : RunHashTable;

          siComplete    : begin
                              Stack.DeleteTop;
                              if Stack.Count > 0 then
                                SetActiveMethod
                              else
                                Exit;
                          end;
          siSpecial : begin
                        ActiveMethod.ExecuteSpecial;
                        Inc(Index);
                      end;
        end;
      end;
    finally
      if Stack.Count > 0 then
        Stack.Top.CodePointer := Index;
    end;
{  except
    raise
  end; }
end;


function TAbstractScriptMethodTag.GetAsInteger : Integer;
begin
  if HasValue then
    Result := TAbstractTag(TypeList[0]).AsInteger
  else
    raise EACNCScriptError.Create(Name + ' ' + HasNoValueLang);
end;

function TAbstractScriptMethodTag.GetAsDouble : Double;
begin
  if HasValue then
    Result := TAbstractTag(TypeList[0]).AsDouble
  else
    raise EACNCScriptError.Create(Name + ' ' + HasNoValueLang);
end;

function TAbstractScriptMethodTag.GetAsString : String;
begin
  if HasValue then
    if TypeList.Count > 0 then
      Result := TAbstractTag(TypeList[0]).AsString
    else
      Result:= 'Not Resolvable'
  else
    raise EACNCScriptError.Create(Name + ' ' + HasNoValueLang);
end;

function TScriptMethodTag.GetIOSize : Integer;
begin
  Result := TAbstractTag(TypeList[0]).Size;
end;

procedure TScriptMethodTag.ExecuteSpecial;
begin
end;

function TScriptParameterTag.GetDataPtr : Pointer;
begin
  Result := Instance.StackParameter[Index].DataPtr;
end;

function TScriptParameterTag.GetDataSize : Integer;
begin
  Result := Instance.StackParameter[Index].DataSize;
end;

function TScriptParameterTag.GetAsInteger : Integer;
begin
  Result := Instance.StackParameter[Index].AsInteger;
end;

function TScriptParameterTag.GetAsBoolean : Boolean;
begin
  Result := Instance.StackParameter[Index].AsBoolean;
end;

function TScriptParameterTag.GetAsDouble : Double;
begin
  Result := Instance.StackParameter[Index].AsDouble;
end;

function TScriptParameterTag.GetAsString : string;
begin
  Result := Instance.StackParameter[Index].AsString;
end;

procedure TScriptParameterTag.Refresh;
begin
  Instance.StackParameter[Index].Refresh;
end;

procedure TScriptParameterTag.Assign(aTag : TAbstractTag);
begin
  Instance.StackParameter[Index].Assign(aTag);
end;


procedure TScriptVariableScope.SetTag(Index : Integer; aTag : TAbstractTag);
begin
  if Assigned(List[Index]) then
    Tags[Index].Free;
  List[Index] := aTag;
end;

function  TScriptVariableScope.GetTag(Index : Integer) : TAbstractTag;
begin
  Result := TAbstractTag(List[Index]);
end;

constructor TScriptVariableScope.Create;
begin
  inherited;
  List := TList.Create;
end;

destructor TScriptVariableScope.Destroy;
begin
  List.Free;
  inherited Destroy;
end;

function  TScriptVariableScope.GetCount : Integer;
begin
  Result := List.Count;
end;

function TScriptVariableScope.Add(aTag : TAbstractTag) : Integer;
var I : Integer;
begin
  // Add this variable first before detecting error. The
  // Rule is once add has been called the list owns the
  // Tag
  Result := List.Add(aTag);
  for I := 0 to Count - 2 do
    if CompareText(aTag.Name, Tags[I].Name) = 0 then
      raise EACNCScriptError.CreateFmt(VariableAlreadyExistsFormat, [aTag.Name])
end;

procedure TScriptVariableScope.Clean;
var
  I : Integer;
begin
  for I := 0 to Count - 1 do
    Tags[I].Free;

  List.Clear;
end;

procedure TScriptVariableScope.Insert(Index : Integer; aTag : TAbstractTag);
begin
  List.Insert(Index, aTag);
end;


constructor TRunTimeStack.Create;
begin
  inherited Create;
  List := TList.Create;
end;

destructor TRunTimeStack.Destroy;
begin
  Clean;
  List.Free;
  inherited Destroy;
end;

procedure TRunTimeStack.Clean;
begin
  while List.Count > 0 do
    DeleteTop;
end;

function TRunTimeStack.Add(RTR : PRunTimeStackRecord) : Integer;
begin
  Result := List.Add(RTR);
end;

procedure TRunTimeStack.DeleteTop;
var I : Integer;
    RunTime : PRunTimeStackRecord;
begin
  if List.Count > 0 then begin
    RunTime := List[List.Count - 1];

    with RunTime^ do
      for I := 0 to RunTime^.IParmCount - 1 do
        if Method.Prototype[I].ParmType = passByValue then
          IParms[I].Free;

    Dispose(RunTime);
    List.Delete(List.Count - 1);
  end;
end;

function TRunTimeStack.GetCount : Integer;
begin
  Result := List.Count;
end;

function TRunTimeStack.GetItem(Index : Integer) : PRunTimeStackRecord;
begin
  Result := PRunTimeStackRecord(List[Index]);
end;


function  TRunTimeStack.Top : PRunTimeStackRecord;
begin
  if List.Count > 0 then
    Result := PRunTimeStackRecord(List[List.Count - 1])
  else
    Result := nil;
end;

constructor TVariableList.Create;
begin
  inherited Create;
  LList := TList.Create;
end;

destructor TVariableList.Destroy;
begin
  Clean;
  LList.Free;
  inherited Destroy;
end;

function TVariableList.Add(aName : string) : Integer;
var Def : TVariableDefRecord;
begin
  Def:= TVariableDefRecord.Create;
  Def.TagClass := nil;
  Def.Name := aName;
  Result := LList.Add(Def);
end;

procedure TVariableList.Clean;
begin
  while LList.Count > 0 do begin
    TVariableDefRecord(VariableDef[0]).Free;
    LList.Delete(0);
  end;
end;

function  TVariableList.GetCount : Integer;
begin
  Result := LList.Count;
end;

function TVariableList.GetDef(Index : Integer) : TVariableDefRecord;
begin
  Result := TVariableDefRecord(LList[Index]);
end;

procedure TMethodArrayTag.SetArray(const Def : TVariableDefRecord);
var I : Integer;
    MemberCount, UnitSize : Integer;
begin
  FOrdinateCount := Def.OrdinateCount;
  Dims := Def.Dims;
  FItemClass := Def.ItemClass;
  if Def.ItemClass.InheritsFrom(TIntegerTag) then
    ItemSize := varInteger
  else if Def.ItemClass.InheritsFrom(TDoubleTag) then
    ItemSize := varDouble
  else if Def.ItemClass.InheritsFrom(TStringTag) then
    ItemSize := varString
  else
    raise EACNCScriptError.Create(InvalidTypeLang);

  MemberCount := 1;
  for I := 0 to OrdinateCount - 1 do begin
    MemberCount := MemberCount * Def.Dims[I];
  end;

  case ItemSize of
    varBoolean,
    varInteger   : UnitSize := Sizeof(Integer);
    varDouble    : UnitSize := Sizeof(Double);
    varString    : UnitSize := Sizeof(Pointer);
  else
    UnitSize := Sizeof(Integer);
  end;

  GetMem(FData, MemberCount * UnitSize);
  FillChar(Data^, MemberCount * UnitSize, 0);
end;

destructor TMethodArrayTag.Destroy;
begin
  if Assigned(Data) then
    FreeMem(Data);

  inherited Destroy;
end;


function TVariableList.Data: PPointerList;
begin
  Result:= LList.List;
end;

initialization
  RegisterTagClass(TScriptMethodTag);
  RegisterTagClass(TScriptParameterTag);
  RegisterTagClass(TMethodArrayTag);
end.
