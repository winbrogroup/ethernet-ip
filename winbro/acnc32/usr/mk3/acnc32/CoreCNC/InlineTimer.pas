unit InlineTimer;

interface

uses Cnc32;

type
  TInlineTimer = class
  private
    FTimeout: Double;
    StartTime: Int64;
    procedure SetTimeout(const Value: Double);
    function GetExpired: Boolean;
  public
    procedure Start;
    property Timeout: Double read FTimeout write SetTimeout;
    property Expired: Boolean read GetExpired;
  end;

implementation

{ TInlineTimer }


{ TInlineTimer }

function TInlineTimer.GetExpired: Boolean;
begin
  Result:= (StartTime + (1000 * Timeout)) < Cnc32.PlcTick.I64;
end;

procedure TInlineTimer.SetTimeout(const Value: Double);
begin
  FTimeout := Value;
end;

procedure TInlineTimer.Start;
begin
  StartTime:= CNC32.PlcTick.I64;
end;

end.
