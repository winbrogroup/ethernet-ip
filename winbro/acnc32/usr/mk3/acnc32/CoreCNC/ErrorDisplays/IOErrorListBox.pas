unit IOErrorListBox;

interface

uses Classes, StdCtrls, Controls, Windows, Graphics, CNCTypes, Cnc32;

type
  TErrorRecord = class
    FReg: PFaultRegistration;
    Level: TFaultLevel;
    constructor Create(FReg: PFaultRegistration; Level: TFaultLevel);
  end;

  TIOErrorListBox = class(TListBox)
  protected
  public
    procedure Clean;
    constructor Create(AOwner: TComponent); override;
    procedure DoDrawItem(Control: TWinControl; Index: Integer; Rect: TRect; State: TOwnerDrawState);
    class function LevelToColour(Level: TFaultLevel): TColor;
  end;

implementation

procedure TIOErrorListBox.Clean;
begin
  while Items.Count > 0 do begin
    Items.Objects[0].Free;
    Items.Delete(0);
  end;
end;

constructor TIOErrorListBox.Create(AOwner: TComponent);
begin
  inherited;
  Style       := lbOwnerDrawFixed;
  align       := alClient;
  Font.Style  := [fsBold];
  Font.Size   := 12;
  TabStop     := False;
  OnDrawItem  := DoDrawItem;
end;

procedure TIOErrorListBox.DoDrawItem(Control: TWinControl; Index: Integer; Rect: TRect; State: TOwnerDrawState);
var
//  Level : TFaultLevel;
  col         : TColor;
begin
  if Index >= Items.count then
    Exit;

  //col:= LevelToColour(TFaultLevel(Items.Objects[Index]));
  col:= LevelToColour(TErrorRecord(Items.Objects[Index]).Level);
  Canvas.Font.Color := col;
  Canvas.FillRect(Rect);
  Canvas.TextOut(Rect.Left, Rect.Top, Items[Index]);
end;


class function TIOErrorListBox.LevelToColour(Level: TFaultLevel): TColor;
begin
  case Level of
    faultNone        : Result := clGreen;
    faultMessageOnly : Result := clYellow;
    faultWarning     : Result := clBlue;
    faultStopCycle   : Result := clRed;
    faultRewind      : Result := clRed;
    faultEstop       : Result := clRed;
    faultFatal       : Result := clRed;
  else
    Result := clBlack;
  end;
end;

{ TErrorRecord }

constructor TErrorRecord.Create(FReg: PFaultRegistration; Level: TFaultLevel);
begin
  Self.FReg:= FReg;
  Self.Level:= Level;
end;

end.
