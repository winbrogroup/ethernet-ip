unit DispatchErrorDisplay;

interface

uses Sysutils, Classes, CNCTypes, CNC32, CoreCNC, Named, CncAbs, ExtCtrls, Controls,
     Windows, Graphics, IOErrorListBox, ComCtrls, FaultRegistration;

type
  TDispatchErrorWindow = class(TExpandDisplay)
  private
    window     : TIOErrorListBox;
    updateData : boolean;
    Tab : TTabControl;
    EDispatch: T32Bit;
    HelpTag: TAbstractTag;
    HelpPattern: string;
    ScrollableWidth : Integer;
    procedure TabChange(Sender : TObject);
    procedure CryForHelp(Sender: TObject);
  protected
    procedure dataChange(aTag : TAbstractTag);
    procedure UpdateDisplay(aTag : TAbstractTag);
  public
    procedure InstallComponent(Params : TParamStrings); override;
    procedure Installed; override;
  end;


implementation


procedure TDispatchErrorWindow.InstallComponent(Params : TParamStrings);
var I: Integer;
    S: TStrings;
    HelpTagName: string;
    Buffer : String;
begin
  inherited installComponent(Params);
  ManageEdit:= True;
  ParentFont:= True;
  Tab := TTabControl.Create(Self);
  Tab.Parent:= Self;
  Tab.Align:= alClient;
  Tab.OnChange := TabChange;
  S:= FaultRegistration.GetDispatchNames;
  ScrollableWidth := Params.ReadInteger('ScrollWidth',0);
  HelpTagName:= Params.ReadString('HelpTag', '');
  HelpPattern:= Params.ReadString('HelpPattern', '%d');
  if HelpTagName <> '' then
    HelpTag:= Sysobj.Comms.TagEvent(HelpTagName, edgeChange, TStringTag, nil);

  Tab.Tabs.AddObject(SysObj.Localized.GetString('$ALL'), Pointer(0));
  for I:= 0 to S.Count - 1 do
    Tab.Tabs.AddObject(SysObj.Localized.GetString('$' + S[I]), Pointer(1 shl I));

  Window             := TIOErrorListBox.Create(Self);
  Window.Parent := Tab;
  Window.ItemHeight  := Abs(Window.Font.Height) + 4;
  Window.ParentFont:= True;
  Window.Font.Style  := [fsBold];
  Window.Font.Size   := 12;
  primary            := window;
  Window.OnDblClick:= CryForHelp;
end;

procedure TDispatchErrorWindow.Installed;
begin
  inherited Installed;
  Window.ScrollWidth := ScrollableWidth;
  TagEvent(NameIOFaultChange, edgeChange, TIntegerTag, dataChange);
  TagEvent(nameDisplaySweep,  edgeRising, TIntegerTag, updateDisplay);
end;


procedure TDispatchErrorWindow.dataChange(aTag : TAbstractTag);
begin
  updateData := True;
end;

procedure TDispatchErrorWindow.updateDisplay(aTag : TAbstractTag);
var I : Integer;
    TmpList : TStringList;
    Text : string;
    Level : TFaultLevel;
    FReg: PFaultRegistration;
begin
  if not UpdateData then
    Exit;
  Window.Clean;
  TmpList := TStringList.Create;
  try
    with SysObj.FaultInterface do begin
      SysObj.FaultInterface.EnterFaultLock;
      try
        for I := 0 to FaultCount - 1 do begin
          FReg:= GetFault(I, Text, Level);
          if (EDispatch in FReg^.Dispatch) or (EDispatch = 0) then
            TmpList.AddObject(Text, TErrorRecord.Create(FReg, Level));
        end;
      finally
        ExitFaultLock;
      end;
    end;

    Window.Items.AddStrings(TmpList);
  finally
    TmpList.Free;
  end;
  UpdateData := False;
end;

procedure TDispatchErrorWindow.TabChange(Sender : TObject);
begin
  EDispatch:= Integer(Tab.Tabs.Objects[Tab.TabIndex]);
  UpdateData:= True;
  updateDisplay(nil);
end;


procedure TDispatchErrorWindow.CryForHelp(Sender: TObject);
var I, Ndx: Integer;
    Tmp: string;
begin
  if Assigned(HelpTag) then begin
    I:= Window.ItemIndex;
    if I <> -1 then begin
      Ndx:= TErrorRecord(Window.Items.Objects[I]).FReg^.Ndx;
      Tmp:= Format(HelpPattern, [Ndx]);
      if FileExists(Tmp) then
        HelpTag.AsString:= Tmp;
    end;
  end;
end;

initialization
  RegisterCNCClass(TDispatchErrorWindow, 'DispatchErrorWindow');
end.

