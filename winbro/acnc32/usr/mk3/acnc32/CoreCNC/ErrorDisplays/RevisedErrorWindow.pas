unit RevisedErrorWindow;

interface

uses Classes, CNCTypes, CNC32, CoreCNC, Named, CncAbs, ExtCtrls, Controls,
     Windows, Graphics, IOErrorListBox;

type
  TRevisedErrorWindow = class(TExpandDisplay)
  private
    window     : TIOErrorListBox;
    updateData : boolean;
  protected
    procedure dataChange(aTag : TAbstractTag);
    procedure UpdateDisplay(aTag : TAbstractTag);
  public
    procedure InstallComponent(Params : TParamStrings); override;
    procedure Installed; override;
  end;




implementation


procedure TRevisedErrorWindow.InstallComponent(Params : TParamStrings);
begin
  inherited installComponent(Params);
  ParentFont:= True;
  ManageEdit         := True;
  Window             := TIOErrorListBox.Create(Self);
  Window.Parent := Self;
  Window.ParentFont:= True;
  Window.Font.Style  := [fsBold];
  Window.Font.Size   := 12;

  Window.ItemHeight  := Abs(Window.Font.Height) + 4;
  primary            := window;
end;

procedure TRevisedErrorWindow.Installed;
begin
  inherited Installed;
  TagEvent(NameIOFaultChange, edgeChange, TIntegerTag, dataChange);
  TagEvent(nameDisplaySweep,  edgeRising, TIntegerTag, updateDisplay);
end;


procedure TRevisedErrorWindow.dataChange(aTag : TAbstractTag);
begin
  updateData := True;
end;

procedure TRevisedErrorWindow.updateDisplay(aTag : TAbstractTag);
var I : Integer;
    TmpList : TStringList;
    Text : string;
    Level : TFaultLevel;
    FReg: PFaultRegistration;
begin
  if not UpdateData then
    Exit;
  Window.Items.Clear;
  TmpList := TStringList.Create;
  try
    with SysObj.FaultInterface do begin
      SysObj.FaultInterface.EnterFaultLock;
      try
        for I := 0 to FaultCount - 1 do begin
          FReg:= GetFault(I, Text, Level);
          //GetFault(I, Text, Level);
          TmpList.AddObject(Text, TErrorRecord.Create(FReg, Level)); //Pointer(Ord(Level)));
        end;
      finally
        ExitFaultLock;
      end;
    end;

    Window.Items.AddStrings(TmpList);
  finally
    TmpList.Free;
  end;
  UpdateData := False;
end;

initialization
  RegisterCNCClass(TRevisedErrorWindow);
end.
