unit AbstractEditor;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, CNCTypes, ClipBrd, StdCtrls;

const Border = 2;

type
  { This component attempts to implement all the functionallity of the
    basic windows TMemo component while allowing a decendant class to
    override the ParseLine function to insert custom font information
    within the displayed text. }
  TEditList = class(TStringList)
  private
//    Clipboard    : TClipboard;
    mstart       : integer;
    mend         : integer;
    FselectStart : TPoint;
    FSelectEnd   : TPoint;
    FOrdStart    : TPoint;
    FordEnd      : TPoint;
    procedure orderSelectPoint;
    procedure orderSelect;
    procedure SetSelectStart(point : TPoint);
    procedure SetSelectEnd  (point : TPoint);
  protected
    function GetTextStr : string; override;
  public
    constructor Create;
    function SelectionLine(Y : integer) : boolean;
    procedure Cut;
    procedure Paste(var Point : TPoint);
    procedure Copy;
    procedure Remove;
    function FindSubstring(const from : TPoint; var newpos : TPoint; search : string) : boolean;
    function SelectStartBefore(X, Y : integer) : boolean;
    function SelectEndAfter(X, Y : integer) : boolean;
    property SelectStart : TPoint read FordStart write setSelectStart;
    property SelectEnd   : TPoint read FordEnd   write setSelectEnd;
  end;


  { Base class for colourful editors. This should be used primarily by overriding
    the parseLine method, this allows a decendant class to add font information
    using the AddFont method. The component then displays this parsed version
    using the original stringList (Text) and the inserted font info</p><p>
    This implementation attempts to imitate the functionality of TMemo but falls
    short in the following areas
    </ul>
    <li> Undo / Redo
    <li> Page up / down : cursor doesnt hardstop (fixed)
    <li> <Ctrl>(<Left>,<Right>) does not traverse a word.
    </ul>
    For simple colourful text displays the displayLine method may be overridden.
    }
  TAbstractEditor = class(TCustomPanel)
  private
    ShiftKey,
    InsertKey,
    CtrlKey    : boolean;
    FreadOnly  : boolean;
    FontHeight : integer;
    fontWidth  : integer;
    FPageColor : TColor;
    FSelecting : boolean;
    FSearching : boolean;
    FSearchStr : string;
    FModified  : Boolean;
    SearchStart : TPoint;
    FOnSearchChange : TNotifyEvent;
    FText      : TEditList;
    FOnCursorMove : TNotifyEvent;
    FOnCarriageReturn : TNotifyEvent;
    FOnEscape : TNotifyEvent;
    FScroll      : TScrollBar;
    FOnPageTracking : TNotifyEvent;
    ScrollInhibit : Boolean;
    LastYPosition : Integer; // Used for cleaning up after a move.
    procedure DoSearch;
    procedure DoSearchAgain;
    procedure SetSearchStr(search : string);
    function  CursorRange : boolean;
    function  RecurseCursorRange : boolean;
    procedure ProcessChar(key : integer);
    procedure ProcessNormalKey(var Message : TMessage);
    procedure ProcessShiftKey(var Message : TMessage);
    procedure ProcessCtrlKey(var Message : TMessage);
    procedure ProcessKey(var Message : TMessage);
    procedure ProcessFunction(var Message : TMessage);
    procedure SetCursorPosition(X, Y : integer);
    procedure MoveX(d : integer);
    procedure MoveY(d : integer);
    procedure MovePage(d : integer);
    procedure PaintFrom(Y : longint);
    procedure PaintRange(Y1, Y2  : longint);
    procedure PaintLine(Y : longint);
    procedure StartSelection;
    procedure OptstartSelection;
    procedure CopySelection;
    procedure CutSelection;
    procedure PasteSelection;
    procedure RemoveSelection;
    procedure DoTextOut(X, Y : integer; s : string);
    procedure CaretControl(show : boolean);
    procedure SetAbsC(aAbsC : TPoint);
    procedure CheckScroll;
    procedure SetVisibleTop(aTop : Integer);
    procedure ScrollChange(Sender : TObject);
    procedure SetScroll(aScroll : TScrollBar);
    procedure MoveWordLeft;
    procedure MoveWordRight;
    function NextChar : Boolean;
    function PreviousChar : Boolean;
    function IsSpace : Boolean;
  protected
    AbsC         : TPoint;
    VisibleTop   : Integer;
    VisibleHeight: Integer;
    VisibleLeft  : Integer;
    VisibleRect  : TRect;
    VisibleText  : TStringList;
    BM           : TBitMap;
    SelectBM     : TBitMap;
    procedure AddFont(var s : string; style : TFontStyles; c : TColor);
    procedure InsertFont(var S : string; P : Integer; style : TFontStyles; c : TColor);
    procedure DoKey(key : char; pos : TPoint); virtual;
    procedure MoveCursor; virtual;
    function  AbsCursorXPos(X, Y : integer) : Integer; virtual;
    function  CursorXPos(X, Y : integer) : Integer; virtual;
    function  CursorYPos(Y : integer) : Integer; virtual;
    procedure DrawLine(list : TStringList; Y : Integer); virtual;
    procedure DisplayLine(Y : integer); virtual;
    function  ParseLine(aText : string; Y : Integer) : string; virtual;
    procedure WndProc(var Message : TMessage); override;
    procedure SetSelecting(s : boolean);
    procedure SetSearching(s : boolean);
    procedure AdjustDimensions;
    procedure Resize; override;
    procedure Paint; override;
    procedure DoEnter; override;
    procedure DoExit; override;
    property  Selecting : boolean read Fselecting write setSelecting;
    property  Searching : boolean read Fsearching write setSearching;
  public
    constructor Create(Aowner : TComponent); override;
    destructor  Destroy; override;
    procedure Repaint; override;
    procedure   SaveToFile(Filename : string); virtual;
    procedure   LoadFromFile(Filename : string); virtual;
    procedure   PaintPage;
    property    Text : TEditList read FText write FText;
    function    YCursorPixelPos(aY : Integer) : Integer;
  published
    property    ReadOnly : boolean read FreadOnly write FreadOnly;
    property    Font;
    property    Align;
    property    ParentFont;
    property    Left;
    property    Top;
    property    Width;
    property    Height;
    property    CursorPos : TPoint read AbsC write SetAbsC;
    property  SearchStr : string  read FSearchStr write SetSearchStr;
    property  OnSearchChange : TNotifyEvent read FOnSearchChange write FOnSearchChange;
    property  Modified : Boolean read FModified write FModified;
    property  OnCursorMove : TNotifyEvent read FOnCursorMove write FOnCursorMove;
    property  TopLine : Integer read VisibleTop write SetVisibleTop;
    property  OnCarriageReturn : TNotifyEvent read FOnCarriageReturn write FOnCarriageReturn;
    property  ScrollBar : TScrollBar read FScroll write SetScroll;
    property  OnPageTracking : TNotifyEvent read FOnPageTracking write FOnPageTracking;
    property  OnEscape : TNotifyEvent read FOnEscape write FOnEscape;

  end;

implementation

constructor TAbstractEditor.Create(Aowner : TComponent);
begin
  inherited Create(Aowner);
  TabStop      := True;
  FText        := TEditList.Create;
  VisibleText  := TStringList.Create;
  BM           := TBitMap.Create;
  SelectBM     := TBitMap.Create;
  AbsC.X       := 1;
  AbsC.Y       := 0;
  VisibleTop   := 0;
  VisibleLeft  := 0;
  FPageColor   := clWindow; //clWhite;
  Color        := FPageColor;
  BevelOuter   := bvLowered;
  BevelWidth   := 1;
  InsertKey    := True;

  AdjustDimensions;
  if csDesigning in ComponentState then begin
    Text.Add('G01 X12.1 M04');
    Text.Add('M30');
  end else Text.add('');
end;

destructor TAbstractEditor.Destroy;
begin
  Text.OnChange := nil;
  Text.Free;
  BM.Free;
  SelectBM.Free;
  VisibleText.Free;
  inherited Destroy;
end;

procedure TAbstractEditor.SetCursorPosition(X, Y : integer);
var i : integer;
begin
  AbsC.Y := VisibleTop + (Y div FontHeight);
  if AbsC.Y > Text.count - 1 then
    AbsC.Y := Text.count - 1;

  if AbsC.Y < 0 then begin
    AbsC.Y := 0;
    Exit;
  end;

  AbsC.X := Length(Text[AbsC.Y]) + 1;
  for I := 1 to length(Text[AbsC.Y]) do begin
    if X < CursorXPos(I, AbsC.Y) then begin
      AbsC.X := I - 1;
      Break;
    end;
  end;
  MoveCursor;
end;


procedure TAbstractEditor.MoveCursor;
begin
  if Assigned(FOnCursorMove) then
    OnCursorMove(Self);

  if Focused then
    SetCaretPos(CursorXPos(AbsC.X, AbsC.Y) + BevelWidth, CursorYPos(AbsC.Y) + BevelWidth);
end;

function  TAbstractEditor.YCursorPixelPos(aY : Integer) : Integer;
begin
  Result := CursorYPos(aY);
end;

function TAbstractEditor.CursorYPos(Y : integer) : Integer;
begin
  Result := (FontHeight * ( Y - VisibleTop));
end;

function TAbstractEditor.CursorXPos(X, Y : integer) : Integer;
begin
  if (Text.Count <= Y) or (Y < 0) then begin
    Result := 0;
    Exit;
  end;

  Result := (fontWidth * (X - 1)) + 0 + VisibleLeft;
end;

function TAbstractEditor.AbsCursorXPos(X, Y : integer) : integer;
begin
  Result := (FontWidth * (X - 1));
end;

{ addFont is used by decendant classes to insert a font change into the editor }
procedure TAbstractEditor.AddFont(var s : string; style : TFontStyles; c : TColor);
var  B : char;
begin
  B := #0;
  TFontStyles(B) := style;
  s := s + #5 + B + Char(c) +
                    Char(c div $100) +
                    Char(c div $10000) + #0;
end;

procedure TAbstractEditor.InsertFont(var S : string; P : Integer; style : TFontStyles; c : TColor);
var B : Char;
begin
  B := #0;
  TFontStyles(B) := style;
  Insert(#5 + B + Char(c) +
                   Char(c div $100) +
                   Char(c div $10000) + #0,
          S, P);
end;

{ Used only by decendant classes to do special processing on a visual key event
  The use of this simplifies overriding WndProc just to detect a key event.
  If the editor is required to change its characteristics then WndProc must be
  overriden }
procedure TAbstractEditor.DoKey(key : Char; pos : TPoint);
begin
end;

{ This is the final parsing of the output text at this point the colour and font
  style are set but now the selection style is added prior to printing to the
  underlying bitmap }
procedure TAbstractEditor.DoTextOut(X, Y : integer; s : string);
begin
  BM.Canvas.TextOut(CursorXPos(X, Y), CursorYPos(Y), S);
end;

{ This reads a syntax parsed line and produces request to doTextOut with the
  font colour and sytle set }
procedure TAbstractEditor.DrawLine(list : TStringList; Y : integer);
var working, stp : string;
    c, i, s, l   : integer;
    ptr          : pointer;
begin
  c := 1;   // start count for each word
  i := 0;   // source string count
  s := 1;   // start of source string
  l := 0;   // length of the current string

  with BM do begin
    stp := list[Y-VisibleTop];
    Canvas.font.style := [];
    Canvas.font.color := clBlack;

    while i < length(stp) do begin
      if stp[i] = #5 then begin
        if l > 1 then begin
          working := Copy(stp, s, l - 1);
          DoTextOut(c, y, working);
          c := c + length(working);
        end;
        Canvas.font.style := TFontStyles(stp[i + 1]);
        ptr := @stp[i + 2];
        Canvas.font.color := TColor(ptr^);
        Inc(i, 6);
        s := i;
        l := 1;
      end else begin;
        Inc(l);
        Inc(i);
      end;
    end;
    if l > 0 then begin
      working := Copy(stp, s, l);
      DoTextOut(c, y, working);
    end;
    if Selecting then begin
      BM.Canvas.CopyMode := cmSrcInvert;
      try
        if Text.SelectionLine(Y) then begin
          BM.Canvas.Draw(0, CursorYPos(Y), SelectBM);
        end else if (Text.SelectStart.Y = Y) and (Text.selectEnd.Y = Y) then begin
          BM.Canvas.CopyRect(Rect(CursorXPos(Text.SelectStart.X, Y),
                                  CursorYPos(Y),
                                  CursorXPos(Text.SelectEnd.X, Y),
                                  CursorYPos(Y) + SelectBM.Height),
                             SelectBM.Canvas,
                             Rect(0, 0, SelectBM.Width, SelectBM.Height));

        end else if (Text.SelectStart.Y = Y) then begin
          BM.Canvas.Draw(CursorXPos(Text.SelectStart.X, Y), CursorYPos(Y), SelectBM);
        end else if (Text.SelectEnd.Y = Y) then begin
          BM.Canvas.CopyRect(Rect(0,
                                  CursorYPos(Y),
                                  CursorXPos(Text.SelectEnd.X, Y),
                                  CursorYPos(Y) + SelectBM.Height),
                             SelectBM.Canvas,
                             Rect(0, 0, SelectBM.Width, SelectBM.Height));
        end;
      finally
        BM.Canvas.CopyMode := cmSrcCopy;
      end;
    end;
  end;
end;

{ The decendant class must override this procedure to fill the VisibleText string
  list with the parsed lines where VisibleText[Y-VisibleTop] is the line to fill from the
  source Text[Y]. Do not inherit this function }
function  TAbstractEditor.ParseLine(aText : string; Y : Integer) : string;
begin
  Result := aText;
end;

{ Moves the Cursor by d in the X plane with no range checking }
procedure TAbstractEditor.MoveX(d : integer);
begin
  AbsC.X := AbsC.X + d;
end;

{ Moves the Cursor by d in the Y plane with no range checking }
procedure TAbstractEditor.MoveY(d : integer);
begin
  AbsC.Y := AbsC.Y + d;
end;

function TAbstractEditor.NextChar : Boolean;
var Tmp : string;
begin
  Result := True;
  Inc(AbsC.X);
  Tmp := TrimRight(Text[AbsC.Y]);
  if AbsC.x > Length(Tmp) then begin
    Inc(AbsC.Y);
    if AbsC.Y >= Text.Count then begin
      AbsC.Y := Text.Count - 1;
      AbsC.X := Length(Text[AbsC.Y]) + 1;
      Result := False;
    end else begin
      AbsC.X := 1;
    end;
  end;
end;

function TAbstractEditor.PreviousChar : Boolean;
begin
  Result := True;
  Dec(AbsC.X);
  if AbsC.X < 1 then begin
    Dec(AbsC.Y);
    if AbsC.Y < 0 then begin
      AbsC.Y := 0;
      AbsC.X := 1;
      Result := False;
      Exit;
    end else
      AbsC.X := Length(Text[AbsC.Y]) + 1;
  end;
end;

function TAbstractEditor.IsSpace : Boolean;
begin
  if Text[AbsC.Y] = '' then begin
    Result := True;
    Exit;
  end;

  case Text[AbsC.Y][AbsC.X] of
    '0'..'9',
    'a'..'z',
    'A'..'Z' : Result := False;
  else
    Result := True;
  end;
end;

procedure TAbstractEditor.MoveWordLeft;
begin
  PreviousChar;

  if not IsSpace then begin
    while not IsSpace do begin
      if not PreviousChar then
        Exit;
    end;
    NextChar;
    Exit;
  end;

  while IsSpace do
    if not PreviousChar then
      Exit;

  while not IsSpace do
    if not PreviousChar then
      Exit;

  NextChar;
end;

procedure TAbstractEditor.MoveWordRight;
begin
  if IsSpace then begin
    while IsSpace do
      if not NextChar then
        Exit;
    Exit;
  end;

  while not IsSpace do
    if not NextChar then
      Exit;

  while IsSpace do
    if not NextChar then
      Exit;
end;

{ Moving the page will, providing a boundary is not hit maintain the cursor
  position within the page.  The calling parameter 'd' is the proposed distance
  to move the page top where plus is down }
procedure TAbstractEditor.MovePage(d : integer);
begin
  Inc(VisibleTop, d);
  Inc(AbsC.Y, d);
  CursorRange;
  PaintPage;
end;

var CaretCount : Integer;
{ The carets (text cursor) visiblity is controlled through this uniform
  interface.  If the editor is readOnly then it performs no function }
procedure TAbstractEditor.CaretControl(show : boolean);
begin
  if ReadOnly then
    Exit;

  if Show then Inc(CaretCount)
          else Dec(CaretCount);

  if show then ShowCaret(handle)
          else Hidecaret(handle);
end;

procedure TAbstractEditor.SetAbsC(aAbsC : TPoint);
begin
  AbsC := aAbsC;
  if CursorRange then
    PaintPage;
  MoveCursor;
end;

{ Identical to PaintFrom(VisibleTop); }
procedure TAbstractEditor.PaintPage;
begin
  PaintFrom(VisibleTop);
end;

{ Redraws multiple lines from the current selected position }
procedure TAbstractEditor.PaintFrom(Y : longint);
var i    : integer;
begin
  CaretControl(False);

  BM.Canvas.Brush.Color := Color;
  BM.Canvas.FillRect(Rect( 0,
                           CursorYPos(Y),
                           VisibleRect.right,
                           VisibleRect.bottom));

  for i := Y to VisibleTop + VisibleHeight do
    DisplayLine(i);

  Canvas.Draw(BevelWidth, BevelWidth, BM);
  moveCursor;
  caretControl(True);
end;

{ Redraws to the screen a single line of text }
procedure TAbstractEditor.PaintLine(Y : longint);
begin
  CaretControl(False);
  BM.Canvas.FillRect( Rect( 0,
                            CursorYPos(Y),
                            VisibleRect.Right,
                            CursorYPos(Y+1)));

  DisplayLine(Y);

  Canvas.CopyRect( Rect(BevelWidth, CursorYPos(Y) + BevelWidth, VisibleRect.Right + BevelWidth, CursorYPos(Y+1)+BevelWidth),
                   BM.Canvas,
                   Rect(0, CursorYPos(Y), VisibleRect.Right, CursorYPos(Y+1)));
  CaretControl(True);
end;

{ Paints a range of lines Y1 to Y2 inclusive }
procedure TAbstractEditor.PaintRange(Y1, Y2  : longint);
var i    : integer;
begin
  CaretControl(False);

  if Y1 < VisibleTop then Y1 := VisibleTop;
  if Y2 > VisibleTop + VisibleHeight then Y2 := VisibleTop + VisibleHeight;

  BM.Canvas.FillRect( Rect(  0,
                             CursorYPos(Y1),
                             VisibleRect.right,
                             CursorYPos(Y2 + 1) ));

  for i := Y1 to Y2 do
    DisplayLine(i);

  Canvas.Draw(BevelWidth, BevelWidth, BM);
  MoveCursor;
  CaretControl(True);
end;

{ On focus the caret is created and displayed }
procedure TAbstractEditor.DoEnter;
begin
  inherited DoEnter;
  CreateCaret(handle, 0, 2, FontHeight);
  MoveCursor;
  CaretControl(True);
end;

{ On Loss of focus the caret is destroyed }
procedure TAbstractEditor.DoExit;
begin
  inherited DoExit;
  CaretControl(False);
  DestroyCaret;
end;

procedure TAbstractEditor.Repaint;
begin
  CaretControl(False);
  inherited Repaint;
  CaretControl(True);
end;

procedure TAbstractEditor.Paint;
begin
  if not Visible then exit;
  CaretControl(False);
  inherited paint;
  PaintPage;
  MoveCursor;
  CaretControl(True);
end;


function TAbstractEditor.CursorRange : Boolean;
begin
  Result := RecurseCursorRange;
  if (LastYPosition <> AbsC.Y) and (LastYPosition < Text.Count) then
    Text[LastYPosition] := TrimRight(Text[LastYPosition]);

  LastYPosition := AbsC.Y;
end;

{ A nightmare of a function that updates VisibleTop, VisibleLeft, AbsC.Y and AbsC.X to
  achieve a valid and safe condition !. It returns True if the new position
  will require a redraw. Because of the number of interacting parameters the
  the routine is recursive !. A review of this function is needed }
function TAbstractEditor.RecurseCursorRange : boolean;
var oldVisibleTop, oldVisibleLeft : Longint;
    I : Integer;
begin
  oldVisibleTop := VisibleTop;
  oldVisibleLeft := VisibleLeft;

  Result := False;
  if VisibleTop < 0 then
    VisibleTop := 0;

  if VisibleTop >= Text.Count then
    VisibleTop := Text.Count - 1;

  if AbsC.Y >= Text.Count then
    AbsC.Y := Text.Count - 1;

  if AbsC.Y < 0 then
    AbsC.Y := 0;

  for I := Length(Text[AbsC.Y]) - 1 to AbsC.X do
    Text[AbsC.Y] := Text[AbsC.Y] + ' ';

  if AbsC.X < 1 then
    AbsC.X := 1;

  if AbsC.Y > VisibleTop + VisibleHeight then begin
     VisibleTop := AbsC.Y - VisibleHeight;
  end;

  if AbsC.Y < VisibleTop then begin
    VisibleTop := AbsC.Y;
  end;

//  if CursorXPos(AbsC.X, AbsC.Y) > (width - Border) then begin
  if CursorXPos(AbsC.X, AbsC.Y) > VisibleRect.Right then begin
    VisibleLeft := ((width div 3) * 2) - AbsCursorXPos(AbsC.X, AbsC.Y);
  end else if CursorXPos(AbsC.X, AbsC.Y) < 0 then begin
    VisibleLeft := 0;
  end;

  if (VisibleTop <> oldVisibleTop) or (VisibleLeft <> oldVisibleLeft) then
    Result := True;

  // because a redraw is required then any one of the paramters may now be
  // out of scope so recurse.
  if Result = true then begin
    RecurseCursorRange;
    CheckScroll;
    if Assigned(FOnPageTracking) then
      OnPageTracking(Self);
  end;
end;

procedure TAbstractEditor.CheckScroll;
begin
  if Assigned(FScroll) then begin
    FScroll.LargeChange := VisibleHeight;
    FScroll.SmallChange := 1;
    try
      ScrollInhibit := True;
      FScroll.SetParams(AbsC.Y, 0, Text.Count);
    finally
      ScrollInhibit := False;
    end;
  end;
end;

procedure TAbstractEditor.SetScroll(aScroll : TScrollBar);
begin
  FScroll := aScroll;
  FScroll.OnChange := ScrollChange;
end;

procedure TAbstractEditor.ScrollChange(Sender : TObject);
begin
  if not ScrollInhibit then begin
    TopLine := FScroll.Position;
    PaintPage;
  end;
  if Assigned(FOnPageTracking) then
    OnPageTracking(Self);
end;

procedure TAbstractEditor.SetVisibleTop(aTop : Integer);
begin
  if (aTop >= 0) and (aTop < Text.Count) then begin
    VisibleTop := aTop;
    PaintPage;
  end;
end;

procedure TAbstractEditor.DoSearch;
var tmp1 : TPoint;
begin
   tmp1 := AbsC;
   if Text.FindSubstring(AbsC, tmp1, SearchStr) then begin
     AbsC := tmp1;
     Inc(tmp1.X, Length(SearchStr));
     Text.SelectStart := AbsC;
     Text.SelectEnd   := tmp1;
     Selecting := True;
     MoveCursor;
     CursorRange;
     PaintPage;
   end;
end;

procedure TAbstractEditor.DoSearchAgain;
var tmp1, tmp2 : TPoint;
begin
   tmp1  := AbsC;
   Inc(tmp1.X);

   if Text.FindSubstring(tmp1, tmp2, SearchStr) then begin
     AbsC := tmp2;
     Inc(tmp2.X, Length(SearchStr));
     Text.SelectStart := AbsC;
     Text.SelectEnd   := tmp2;
     Selecting := True;
     MoveCursor;
     CursorRange;
     PaintPage;
   end;
end;

procedure TAbstractEditor.SetSearchStr(search : string);
begin
  FSearchStr := search;
  if Assigned(FOnSearchChange) then FOnSearchChange(Self);
end;

{ Display line displays a line given the current absolute Y ordinate of the
  line. It could very well not be VisibleText }
procedure TAbstractEditor.DisplayLine(Y : integer);
begin
  if Y >= Text.Count then exit;
  CaretControl(False);

  VisibleText[Y-VisibleTop] := ParseLine(Text[Y], Y);
  DrawLine(VisibleText, Y);

  MoveCursor;
  CaretControl(True);
end;

{ Called from WndProc when a visible character requires adding }
procedure TAbstractEditor.ProcessChar(key : integer);
var  tmp    : string;
     I : Integer;
begin
  if ReadOnly then
    Exit;

  if Selecting and not Searching and not CtrlKey then begin
    RemoveSelection;
    Selecting := false;
  end;
  case key of
    $06      : begin
                 if Searching then
                   DoSearchAgain
                 else
                   SearchStr := '';
                 Searching := True;
             end;

    $20..$7e : begin
                 if Searching then begin
                   SearchStr := SearchStr + Char(key);
                   DoSearch;
                   exit;
                 end;
                 Modified := True;
                 tmp := Text[AbsC.Y];
                 if insertKey then begin
                   insert(char(key), tmp, AbsC.X);
                 end else begin
                   if AbsC.X > length(tmp) then insert(' ', tmp, AbsC.X);
                   tmp[AbsC.X] := char(key);
                 end;
                 MoveX(1);
                 text[AbsC.Y] := tmp;
                 DoKey(Char(key), Text.SelectStart);
                 PaintLine(AbsC.Y);
             end;

    VK_ESCAPE : if Assigned(FOnEscape) then
                  OnEscape(Self);

    VK_ENTER : begin
               Modified := True;
               Searching := False;
               Trim(text[AbsC.Y]);
               if insertKey then begin
                 tmp := text[AbsC.Y];
                 text.insert(AbsC.Y + 1, Copy(tmp, AbsC.X, length(tmp) - AbsC.X + 1));
                 System.Delete(tmp, AbsC.X, length(tmp) - AbsC.X + 1);
                 text[AbsC.Y] := tmp;
               end else begin
                 if AbsC.Y >= text.count - 1 then
                   text.add('');
               end;
               Text[AbsC.Y] := TrimRight(Text[AbsC.Y]);
               AbsC.X := 1;
               for I := 1 to Length(Text[AbsC.Y]) do
                 if Text[AbsC.Y][I] = ' ' then begin
                   Text[AbsC.Y + 1] := ' ' + Text[AbsC.Y + 1];
                   AbsC.X := I + 1
                 end else
                   Break;
               MoveY(1);
               if CursorRange then PaintPage
                              else PaintFrom(AbsC.y - 1);
             end;
    VK_BACK  : begin
               if Searching  then begin
                 tmp := SearchStr;
                 System.Delete(tmp, Length(SearchStr), 1);
                 SearchStr := tmp;
                 AbsC := SearchStart;
                 if SearchStr = '' then begin
                 // Assumes searching will remove selecting and repaint
                   Searching := False;
                   exit;
                 end else begin
                   DoSearch;
                   exit;
                 end;
               end;

               Modified := True;
               Searching := False;
               if AbsC.X = 1 then begin
                 if AbsC.Y > 0 then begin
                   tmp := Text[AbsC.Y];
                   Text.Delete(AbsC.Y);
                   MoveY(-1);
                   AbsC.X := length(Text[AbsC.Y]) + 1;
                   Text[AbsC.Y] := Text[AbsC.Y] + tmp;
                   if AbsC.Y < VisibleTop then CursorRange;
                   PaintFrom(AbsC.Y);
                 end;
               end else begin
                 tmp := Text[AbsC.Y];
                 Delete(tmp, AbsC.X - 1, 1);
                 text[AbsC.Y] := tmp;
                 MoveX(-1);
                 PaintLine(AbsC.Y);
               end;
             end;
  end;
  if CursorRange then
    PaintPage;
end;

{ Called when a neither shift or control is depressed with a cursor control key }
procedure TAbstractEditor.ProcessNormalKey(var Message : TMessage);
var tmp : string;
begin
  case message.wparam of
    VK_UP      : Dec(AbsC.Y);
    VK_DOWN    : Inc(AbsC.Y);
    VK_LEFT    : Dec(AbsC.X);
    VK_RIGHT   : Inc(AbsC.X);
    VK_HOME    : AbsC.X := 1;
    VK_PRIOR   : MovePage(-VisibleHeight);
    VK_NEXT    : MovePage(VisibleHeight);
    VK_END     : begin
                 Tmp := TrimRight(Text[AbsC.Y]);
                 Text[AbsC.Y] := Tmp;
                 AbsC.X := length(Text[AbsC.Y]) + 1;
             end;
    VK_DELETE : begin
               if ReadOnly then
                 Exit;
               if Selecting then begin
                 RemoveSelection;
                 Selecting := False;
               end else begin
                 tmp := TrimRight(Copy(Text[AbsC.Y], 1, Length(Text[AbsC.Y])));
                 if AbsC.X = length(Tmp) + 1 then begin
                   if AbsC.Y < Text.Count - 1 then begin
                     Tmp := Tmp + Text[AbsC.Y + 1];
                     Text[AbsC.Y] := Tmp;
                     Text.Delete(AbsC.Y + 1);
                     PaintFrom(AbsC.Y);
                   end;
                 end else begin;
                   System.Delete(tmp, AbsC.X, 1);
                   Text[AbsC.Y] := tmp;
                   PaintLine(AbsC.Y);
                 end;
               end;
             end;
  else
    Exit;
  end;
  Searching := False;
  Selecting := False;
  if CursorRange then
    PaintPage;
//????    paintFrom(VisibleTop);
end;

{ Called when the shift key is depressed with a cursor control key the
  functionality is expanded to include text selection }
procedure TAbstractEditor.ProcessShiftKey(var Message : TMessage);
var oldStart, oldEnd : TPoint;
    I, J : Integer;
    Tmp : string;
  procedure UpdateSelection;
  begin
    Text.SelectEnd := AbsC;
    if oldStart.y > Text.SelectStart.Y then
      oldStart.Y := Text.SelectStart.Y;
    if oldEnd.Y   < Text.SelectEnd.Y then
      oldEnd.Y   := Text.SelectEnd.Y;
  end;

begin
  oldStart := Text.SelectStart;
  oldEnd   := Text.SelectEnd;

  case message.wparam of
    VK_UP      : begin OptStartSelection; Dec(AbsC.Y); end;
    VK_DOWN    : begin OptStartSelection; Inc(AbsC.Y); end;
    VK_LEFT    : begin
                 OptStartSelection;
                 if not CtrlKey then
                   Dec(AbsC.X)
                 else
                   MoveWordLeft;
    end;
    VK_RIGHT   : begin
                 OptStartSelection;
                 if not CtrlKey then
                   Inc(AbsC.X)
                 else
                   MoveWordRight;
    end;
    VK_HOME    : begin OptStartSelection; AbsC.X := 1; end;
    VK_PRIOR   : begin OptStartSelection; MovePage(-VisibleHeight); end;
    VK_NEXT    : begin OptStartSelection; MovePage(VisibleHeight); end;
    VK_END     : begin
                   OptStartSelection;
                   Tmp := TrimRight(Text[AbsC.Y]);
                   Text[AbsC.Y] := Tmp;
                   AbsC.X := length(Text[AbsC.Y]) + 1;
    end;
    VK_I       : if CtrlKey and Selecting then begin
                   if Text.SelectEnd.X <> 1 then
                     J := Text.SelectEnd.Y
                   else
                     J := Text.SelectEnd.Y - 1;

                   for I := Text.SelectStart.Y to J do
                     if Text[I] <> '' then
                       Text[I] := ' ' + Text[I];
                 end;

    VK_U      : if CtrlKey and Selecting then begin
                   if Text.SelectEnd.X <> 1 then
                     J := Text.SelectEnd.Y
                   else
                     J := Text.SelectEnd.Y - 1;

                   for I := Text.SelectStart.Y to J do
                     if (Text[I] <> '') and (Text[I][1] = ' ') then
                       Text[I] := Copy(Text[I], 2, Length(Text[I]));
                 end;
    VK_DELETE  : CutSelection;
  else
    exit;
  end;
  Searching := False;
  if CursorRange then begin
                   UpdateSelection;
                   PaintPage;
                 end else begin
                   UpdateSelection;
                   PaintRange(oldStart.Y, oldEnd.Y);
                 end;
end;

{ Called when the control key is depressed with a cursor control key the
  functionality is expanded partially in line with the standard edit control }
procedure TAbstractEditor.ProcessCtrlKey(var Message : TMessage);
var Tmp : string;
begin
  case message.wparam of
    VK_C       : CopySelection;
    VK_V       : PasteSelection;
    VK_X       : CutSelection;
    VK_UP      : MovePage(-1);
    VK_DOWN    : MovePage(1);
    VK_LEFT    : MoveWordLeft;
    VK_RIGHT   : MoveWordRight;
    VK_HOME    : begin AbsC.X := 1; AbsC.Y := 0; end;
    VK_PRIOR   : MovePage(-VisibleHeight);
    VK_NEXT    : MovePage(VisibleHeight);
    VK_END     : begin
                   AbsC.Y := Text.Count - 1;
                   Tmp := TrimRight(Text[AbsC.Y]);
                   Text[AbsC.Y] := Tmp;
                   AbsC.X := length(Text[AbsC.Y]) + 1;
    end;
    VK_DELETE  : CopySelection;
  else
    Exit;
  end;

  if CursorRange then
    PaintPage;
end;

procedure TAbstractEditor.processKey(var Message : TMessage);
begin
  case message.wparam of
    VK_SHIFT   : ShiftKey  := True;
    VK_CONTROL : CtrlKey   := True;
    VK_INSERT  : begin
                   if ShiftKey then PasteSelection
                   else InsertKey := not InsertKey;
                 end;
    VK_TAB     : PostMessage(TForm(GetParentForm(Self)).handle, WM_NEXTDLGCTL, 0, 0);
  else
    if ShiftKey then
      ProcessShiftKey(Message)
    else if CtrlKey then
      ProcessCtrlKey(Message)
    else ProcessNormalKey(Message);
  end;
  MoveCursor;
end;

procedure TAbstractEditor.ProcessFunction(var Message : TMessage);
begin
  case message.wparam of
    VK_SHIFT   : begin ShiftKey := False; end;
    VK_CONTROL : begin CtrlKey  := False; end;
  end;
end;

procedure TAbstractEditor.WndProc(var Message : TMessage);
//var tmp : TPoint;
begin
  if not (csDestroying in ComponentState) then begin
    if Assigned(Text) and (Text.Count = 0) then Text.Add('');
    case Message.Msg of
      WM_CHAR           : ProcessChar(Message.wparam);
      WM_KEYDOWN        : ProcessKey(Message);
      WM_KEYUP          : ProcessFunction(Message);
      CM_WANTSPECIALKEY : begin Message.Result := 1; Exit; end;
      WM_CUT            : Cutselection;
      WM_PASTE          : Pasteselection;
      WM_COPY           : Copyselection;
      WM_LBUTTONDOWN    : begin
                            SetCursorPosition(LOWORD(Message.lParam), HIWORD(Message.lParam));
                            if not Focused then
                              SetFocus;
//                          StartSelection;
                            Selecting := False;
                          end;
//      WM_LBUTTONUP      : Dragging := False;
      WM_RBUTTONDOWN    : begin
                            SetCursorPosition(LOWORD(Message.lParam), HIWORD(Message.lParam));
                            if not Focused then
                              SetFocus;
                            Selecting := False;
                          end;
      WM_LBUTTONDBLCLK  : begin
                            SetCursorPosition(LOWORD(Message.lParam), HIWORD(Message.lParam));
                            if not Focused then
                              SetFocus;
                            Selecting := False;
                          end;
  {    WM_MOUSEMOVE      : if dragging then begin
                            tmp := AbsC;
                            SetCursorPosition(LOWORD(Message.lParam), HIWORD(Message.lParam));
                            Text.selectEnd := AbsC;
                            if (AbsC.X <> tmp.X) or (AbsC.Y <> tmp.Y) then paintFrom(VisibleTop);
                          end; }
    else
    end;
  end;
  inherited WndProc(Message);
end;

procedure TAbstractEditor.Resize;
begin
  inherited Resize;
  if not (csDestroying in ComponentState) then
    AdjustDimensions;
end;

procedure TAbstractEditor.AdjustDimensions;
var i      : integer;
begin
  Canvas.Font.Name := 'Courier New';
  Canvas.Font.Size := Font.Size;
  Canvas.Font.Style:= [];

  BM.Canvas.Font := Canvas.Font;

  FontHeight := BM.Canvas.TextHeight('Q');
  FontWidth  := BM.Canvas.TextWidth('Q');

  VisibleText.Clear;
  VisibleHeight := Height div FontHeight;

  if VisibleHeight <= 0 then
    VisibleHeight := 2;

  for i := 0 to VisibleHeight + 1 do
    VisibleText.Add('');

  Dec(VisibleHeight);

  VisibleRect.Left   := BevelWidth;
  VisibleRect.Top    := BevelWidth;
  VisibleRect.Right  := Width - (BevelWidth * 2);
  VisibleRect.Bottom := Height - (BevelWidth * 2);

  if VisibleRect.Bottom <= 0 then
    VisibleRect.Bottom := BevelWidth;

  if VisibleRect.Right <= 0 then
    VisibleRect.Right := BevelWidth;

  BM.Width     := VisibleRect.Right;
  BM.Height    := VisibleRect.Bottom;

  SelectBM.Width := BM.Width;
  SelectBM.Height:= FontHeight;
  SelectBM.Canvas.Brush.Color := clYellow;
  SelectBM.Canvas.Pen.Color := clYellow;
  SelectBM.Canvas.Rectangle(0, 0, SelectBM.Width, SelectBM.Height);
  CheckScroll;
end;

procedure   TAbstractEditor.SaveToFile(filename : string);
begin
  text.SaveToFile(filename);
  Modified := False;
end;

procedure   TAbstractEditor.LoadFromFile(filename : string);
begin
  Text.LoadFromFile(filename);
  VisibleTop := 0;
  AbsC.X := 1;
  AbsC.Y := 0;
  PaintFrom(VisibleTop);
  Modified := False;
end;

procedure TAbstractEditor.SetSelecting(s : boolean);
begin
  if Selecting then begin
    FSelecting := s;
    PaintFrom(VisibleTop);
  end;
  FSelecting := s;
end;

procedure TABstractEditor.SetSearching(s : boolean);
begin
  if S and not Searching then
    SearchStart := AbsC;

  if not s and Searching then
    Selecting := False;

  FSearching := s;
  if not Searching then
    SearchStr := '';
end;

procedure TAbstractEditor.StartSelection;
begin
  Text.SelectStart := AbsC;
  Text.SelectEnd   := AbsC;
  Selecting := true;
end;

procedure TAbstractEditor.OptStartSelection;
begin
  if Selecting then
    Exit;

  StartSelection;
end;

procedure TAbstractEditor.CopySelection;
begin
  Text.Copy;
end;

procedure TAbstractEditor.RemoveSelection;
begin
  if readOnly then
    Exit;

  Text.Remove;
  AbsC := Text.selectStart;
  CursorRange;
  paintFrom(VisibleTop);
end;

procedure TAbstractEditor.CutSelection;
begin
  if ReadOnly then
    Exit;

  Text.Cut;
  AbsC := Text.SelectStart;
  Selecting := False;
  PaintFrom(VisibleTop);
end;

procedure TAbstractEditor.PasteSelection;
begin
  if Selecting then
    RemoveSelection;
  Text.Paste(AbsC);
  CursorRange;
  PaintFrom(VisibleTop);
end;

constructor TEditList.Create;
begin
  inherited create;
end;

function TEditList.GetTextStr: string;
var
  I, L, Size, Count: Integer;
  P: PChar;
  S: string;
begin
  Count := GetCount;
  Size := 0;
  for I := 0 to Count - 1 do  begin
    if (I = selectStart.Y) then begin
      mstart := Size + selectStart.X;
    end;
    if (I = selectEnd.Y) then begin
      mend := Size + selectEnd.X;
    end;
    Inc(Size, Length(Get(I)) + 2)
  end;

  SetString(Result, nil, Size);
  P := Pointer(Result);
  for I := 0 to Count - 1 do begin
    S := Get(I);
    L := Length(S);
    if L <> 0 then  begin
      System.Move(Pointer(S)^, P^, L);
      Inc(P, L);
    end;
    P^ := #13;
    Inc(P);
    P^ := #10;
    Inc(P);
  end;
  OrderSelect;
end;

procedure TEditList.SetSelectStart(point : TPoint);
begin
  FSelectStart := point;
  OrderSelectPoint;
end;

procedure TEditList.SetSelectEnd  (point : TPoint);
begin
  FSelectEnd   := Point;
  OrderSelectPoint;
end;

procedure TEditList.OrderSelectPoint;
var tmp : TPoint;
begin
  FOrdEnd   := FSelectEnd;
  FOrdStart := FSelectStart;
  if FSelectEnd.Y < FSelectStart.Y then begin
    tmp := FSelectEnd;
    FOrdEnd   := FSelectStart;
    FOrdStart := tmp;
    exit;
  end;

  if FSelectEnd.Y = FSelectStart.Y then begin
    if FSelectEnd.X < FSelectStart.X then begin
      tmp := FSelectEnd;
      FOrdEnd   := FSelectStart;
      FOrdStart := tmp;
      exit;
    end;
  end;
end;

procedure TEditList.orderSelect;
var tmp : integer;
begin
  if mstart < mend then
    Exit;
  tmp := mstart;
  mstart := mend;
  mend   := tmp
end;

function TEditList.SelectionLine(Y : integer) : boolean;
begin
  result := False;
  if (Y > SelectStart.Y) and (Y < SelectEnd.Y) then
    Result := True;
end;

function TEditList.SelectStartBefore(X, Y : integer) : boolean;
begin
  result := True;
  if SelectStart.Y < Y then
    Exit;
  if (SelectStart.Y = Y) and (SelectStart.X < X) then
    Exit;

  Result := False;
end;

function TEditList.SelectEndAfter(X, Y : integer) : boolean;
begin
  Result := True;
  if selectEnd.Y > Y then
    Exit;
  if (SelectEnd.Y = Y) and (SelectEnd.X > X) then exit;
  Result := False;
end;


procedure TEditList.Cut;
begin
  Copy;
  Remove;
end;

procedure TEditList.Copy;
var Stp : string;
begin
  Stp := System.Copy(Text, mstart, mend - mstart);
  Clipboard.AsText := Stp;
end;

procedure TEditList.Remove;
var stp : string;
begin
  stp := Text;
  System.Delete(stp, mstart, mend - mstart);
  Text := stp;
end;

procedure TEditList.Paste(var Point : TPoint);
  function DetermineNewCursor(P : TPoint; S : string) : TPoint;
  var I, X, Y : Integer;
  begin
    X := 0;  Y := 0;
    for I := 1 to Length(S) do begin
      if S[I] = #$0d then begin
        X := 0;
        Inc(Y);
      end else begin
        Inc(X);
      end;
    end;
    Result.Y := P.Y + Y;
    if Y > 0 then
      Result.X := X
    else
      Result.X := P.X + X;
  end;

var Clip : TStringList;
    Stp, ClipFlat : string;
begin
  Clip := TStringList.Create;
  try
    if ClipBoard.HasFormat(CF_TEXT) then begin
      SelectStart := Point;
      SelectEnd   := Point;

      Stp := Text;
      Clip.Text := ClipBoard.AsText;
      ClipFlat := ClipBoard.AsText;
      System.Insert(ClipFlat, Stp, MStart);
      Text := stp;

      Point := DetermineNewCursor(Point, ClipFlat);
    end;
  finally
    Clip.Free;
  end;
end;

function TEditList.FindSubstring(const from : TPoint; var newpos : TPoint;
                                       search : string) : boolean;
var tmp    : integer;
    stp    : string;
begin
  result    := True;
  NewPos    := From;

  // first check the current line from cursor X
  Stp := System.Copy(Strings[From.Y], From.X, length(Strings[From.Y]));
  NewPos.X := Pos(UpperCase(search), UpperCase(stp));
  if NewPos.X <> 0 then begin
    Inc(NewPos.X, From.X - 1);
    exit;
  end;

  NewPos := From;
  // If not found then search subsequent strings
  for tmp := From.Y + 1 to GetCount - 1 do begin
    NewPos.Y := tmp;
    NewPos.X :=Pos(UpperCase(search), UpperCase(Strings[tmp]));
    if NewPos.X <> 0 then exit;
  end;

  result := False;
end;

end.
