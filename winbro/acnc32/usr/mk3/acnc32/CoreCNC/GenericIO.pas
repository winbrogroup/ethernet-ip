unit GenericIO;

interface

uses Classes, CNCTypes, CNC32, CoreCNC, Windows, SysUtils, Graphics, INamedInterface;

type
  TIODevice = class(TAbstractIODevice)
  public
    procedure ReadTags; override;
    procedure WriteTags; override;
  end;

  TGenericCommunications = class(TAbstractCommunications)
  private
  protected
  public
    constructor Create(aOwner : TComponent); override;
    procedure Installed; override;
    procedure InstallComponent(Params : TParamStrings); override;
  end;

  TVirtualIO = class(TAbstractIOAccessor)
  private
    Host: IIOSystemAccessorHost;
    FTextDef: string;
    FInputSize: Integer;
    FOutputSize: Integer;
    FInputArea: Pointer;
    FOutputArea: Pointer;
  public
    function GetInfoPageContent(aPage : Integer) : WideString; override;
    procedure Initialise(Host: IIOSystemAccessorHost; const Params : WideString); override;
    procedure UpdateShadowToReal; override;
    procedure UpdateRealToShadow; override;
    function Healthy : Boolean; override;
    procedure Reset; override;
    function LastError : WideString; override;
    function GetInputArea: Pointer; override;
    function GetOutputArea: Pointer; override;
    function GetInputSize: Integer; override;
    function GetOutputSize: Integer; override;
    function TextDefinition: WideString; override;
    procedure Shutdown; override;
  end;

implementation

constructor TGenericCommunications.Create(aOwner : TComponent);
begin
  inherited Create(aOwner);
end;

procedure TGenericCommunications.InstallComponent(Params : TParamStrings);
begin
  inherited InstallComponent(Params);
  IOThread := TAbstractIOThread.Create;
end;

procedure TGenericCommunications.Installed;
begin
  inherited Installed;
  IOThread.OnSweep := Sweep;
  IOThread.SleepTime := SleepTime;
  IOThread.Priority := tpTimeCritical;
  IOThread.Resume;
end;

procedure TIODevice.ReadTags;
var I : Integer;
    aTag : TIntegerTag;
begin
  for I := 0 to TagCount - 1 do begin
    aTag := TIntegerTag(GetTag(I));
    if (nltConnected in aTag.NamedType) and
       not (nltWritable in aTag.NamedType) then
      aTag.ReadFromDevice;
  end;
end;

procedure TIODevice.WriteTags;
var I : Integer;
    aTag : TIntegerTag;
begin
  for I := 0 to TagCount - 1 do begin
    aTag := TIntegerTag(GetTag(I));
    if aTag.NamedType >= [nltWritable, nltConnected] then
      aTag.WriteToDevice;
  end;
end;


{ TVirtualIO }

function TVirtualIO.GetOutputSize: Integer;
begin
  Result:= FOutputSize;
end;

function TVirtualIO.GetInfoPageContent(aPage: Integer): WideString;
begin

end;

function TVirtualIO.GetOutputArea: Pointer;
begin
  Result:= FOutputArea;
end;

function TVirtualIO.GetInputSize: Integer;
begin
  Result:= FInputSize;
end;

procedure TVirtualIO.Reset;
begin

end;

function TVirtualIO.GetInputArea: Pointer;
begin
  Result:= FInputArea;
end;

function TVirtualIO.TextDefinition: WideString;
begin
  Result:= FTextDef;
end;

function TVirtualIO.Healthy: Boolean;
begin
  Result:= True;
end;

function TVirtualIO.LastError: WideString;
begin
  Result:= '';
end;

procedure TVirtualIO.Initialise(Host: IIOSystemAccessorHost; const Params: WideString);
var Tmp: string;
begin
  Self.Host:= Host;
  FTextDef:= Params;
  Tmp:= Params;

  FInputSize:= StrToInt(CncTypes.ReadDelimited(Tmp, ';'));
  FOutputSize:= StrToInt(CncTypes.ReadDelimited(Tmp, ';'));

  FInputSize:= CncTypes.RangeInt(FInputSize, 1, 32000);
  FOutputSize:= CncTypes.RangeInt(FOutputSize, 1, 32000);

  GetMem(FInputArea, FInputSize);
  GetMem(FOutputArea, FOutputSize);
  FillChar(FInputArea^, FInputSize, 0);
end;

procedure TVirtualIO.UpdateRealToShadow;
begin

end;

procedure TVirtualIO.UpdateShadowToReal;
begin

end;

procedure TVirtualIO.Shutdown;
begin
  Host:= nil;
end;

initialization
  RegisterIODeviceClass(TIODevice);
  RegisterCNCClass(TGenericCommunications, 'GenericCommunications');
  RegisterIOAccessorClass(TVirtualIO, 'VirtualIO');
end.
