unit OEE;

interface

uses SysUtils, Classes, CNCTypes, CNC32, CoreCNC, StreamTokenizer, Named,
     FaultRegistration, CustomFaultInterface, Plc32, KBControl, Controls,
     StdCtrls, Buttons, Mask, ExtCtrls, Graphics, CNCRegistry;

// TODO
// ~Create visual
// ~Write documentation
// ~Create write config
// ~Configuration required for output tag(s)
// Dont display codeedit when no mask programmed.

{ Notes
  OEEEnabled turns on / off OEE funtionallity (via config / frontend )
  EnableTag turns on / off OEE functionallity (via Plc / script )

  OEE monitors when both OEEEnabled and ActiveTag are true.

  New output tag
  OEEInhibit - this will be true when MID is required

  New core interface similar to ModeLock, for CycleStart

}
type
  TSubTopic = class(TObject)
  private
    Title : string;
    Mask : string;
    Code : string;
    TextList : TStringList;
    CodeList : TStringList;
    AuthenticationList : TStringList;
  public
    constructor Create;
    destructor Destroy; override;
  end;

  TTopic = class(TObject)
  private
    List : TStringList;
    Title : string;
    Code : string;
    function GetSubTopic(Index: Integer): TSubTopic;
  public
    constructor Create;
    destructor Destroy; override;
    procedure Clean;
    procedure AddSubTopic(aTopic : TSubTopic);
    property SubTopics[Index : Integer] : TSubTopic read GetSubTopic;
  end;

  TOEE = class(TAbstractDisplay)
  private
    OEEEnabled : Boolean;
    List : TStringList;

    ActivateTag : TAbstractTag;
    ActivateTagName : string;

    EntryRequiredTag : TAbstractTag;
    EntryRequiredTagName : string;

    TopicTag : TAbstractTag;
    TopicTagName : string;

    OEEStartTag : TAbstractTag;
    UserNameTag : TAbstractTag;

    EnabledTagName : string;
    EnabledTag : TAbstractTag;

    KBPanel : TPanel;

    ActiveSubTopic : TSubTopic;

    TopicList : TListBox;
    SubTopicList : TListBox;

    CtrlPanel: TPanel;
    MainPanel: TPanel;
    InfoPanel: TPanel;

    CodeEdit : TEdit;
    ActivateBtn : TButton;
    SubTopicListGroup : TGroupBox;
    TopicListGroup : TGroupBox;
    //CodeList : TListBox;
    //CodeListGroup : TGroupBox;
    NoteLabel : TStaticText;

    ClassicDateFormat : Boolean;

    function AuthenticationRequired : Boolean;
    procedure ReadConfig(S : TStreamQuoteTokenizer);
    function GetTopic(Index: Integer): TTopic;
    function AddTopic(aTopic : TTopic) : Integer;
    procedure ActivateOEE(aTag : TAbstractTag);
    procedure UserNameChange(aTag : TAbstractTag);
    procedure DeactivateOEE(Sender : TObject);
    procedure EnableControls(aOn : Boolean);
    procedure TopicSelect(Sender: TObject);
    procedure SubTopicSelect(Sender: TObject);
    //procedure CodeSelect(Sender: TObject);
    procedure SetSubList(Sub : TSubTopic);
    procedure SetOEEStartTag(When : TDateTime);
  public
    constructor Create(aOwner : TComponent); override;
    procedure InstallComponent(Params : TParamStrings); override;
    procedure Installed; override;
    procedure IShutdown; override;
    property Topics[Index : Integer] : TTopic read GetTopic;
    procedure Resize; override;
    procedure Activate(aTag : TAbstractTag); override;
  end;

implementation

{ TOEE }

function TOEE.AddTopic(aTopic: TTopic): Integer;
begin
  Result := List.AddObject(aTopic.Title, aTopic);
end;

// ------ Visual control (method and event) ----------------

var
  OEETopicLang: string; // = 'OEE Topic';
  OEESubTopicLang: string; // = 'OEE Sub-Topic';
  OEECodeLang: string; // = 'OEE Code';
  NoEntryRequiredLang: string; // = 'No entry required';
  FillInFormLang: string; // = 'Please select a topic and sub-topic from the list';

constructor TOEE.Create(aOwner: TComponent);
begin
  inherited;
  List := TStringList.Create;

  MainPanel:= TPanel.Create(Self);
  MainPanel.Parent:= Self;
  MainPanel.Align:= alClient;

  TopicListGroup := TGroupBox.Create(Self);
  TopicListGroup.Parent := MainPanel;
  TopicListGroup.Caption := OEETopicLang;
  TopicListGroup.Height:= 100;
  TopicListGroup.Align:= alTop;

  TopicList := TListBox.Create(Self);
  TopicList.Parent := TopicListGroup;
  TopicList.Align := alClient;
  TopicList.OnClick := TopicSelect;

  SubTopicListGroup := TGroupBox.Create(Self);
  SubTopicListGroup.Parent := MainPanel;
  SubTopicListGroup.Caption := OEESubTopicLang;
  SubTopicListGroup.Align:= alClient;

  SubTopicList := TListBox.Create(Self);
  SubTopicList.Parent := SubTopicListGroup;
  SubTopicList.Align := alClient;
  SubTopicList.OnClick := SubTopicSelect;

  //CodeListGroup := TGroupBox.Create(Self);
  //CodeListGroup.Parent := Self;
  //CodeListGroup.Caption := OEECodeLang;
  //CodeListGroup.Visible := False;

  //CodeList := TListBox.Create(Self);
  //CodeList.Parent := CodeListGroup;
  //CodeList.Align := alClient;
  //CodeList.OnClick := CodeSelect;

  InfoPanel:= TPanel.Create(Self);
  InfoPanel.Parent:= Self;
  InfoPanel.Align:= alBottom;
  InfoPanel.Height:= 140;

  CtrlPanel:= TPanel.Create(Self);
  CtrlPanel.Parent := InfoPanel;
  CtrlPanel.Align:= alBottom;

  CodeEdit := TEdit.Create(Self);
  CodeEdit.Parent := CtrlPanel;
  CodeEdit.Width := 200;

  ActivateBtn := TButton.Create(Self);
  ActivateBtn.Parent := CtrlPanel;
  ActivateBtn.OnClick := DeactivateOEE;
  ActivateBtn.Caption := SysObj.Localized.GetString('$OK');
  ActivateBtn.Left:= 250;

  NoteLabel := TStaticText.Create(Self);
  NoteLabel.Parent := InfoPanel;
  NoteLabel.Align := alClient;
  NoteLabel.BorderStyle := sbsSunken;

  KBPanel := KBControl.__KBC.CreateButton([kbNumeric, kbAlpha, kbNavigation], Self, alTop);
end;

function SimpleDateFormat(Time : TDateTime):String;
var Hour,Min,Sec,msec : Word;
    Day, Month, Year : Word;
begin
   DecodeTime(Time,Hour,Min,Sec,msec);
   DecodeDate(Time,Year, Month, Day);

   Result := Format('%.2d-%.2d-%.4d %.2d:%.2d:%.2d', [
        Day, Month, Year, Hour, Min, Sec ]);
end;

const FifteenMinutes = 1 / (24 * 4);

procedure TOEE.DeactivateOEE(Sender : TObject);
var SubTopic : TSubTopic;
    Topic : TTopic;
    Tmp, Tmp1 : string;
    I : Integer;
begin
  if (SubTopicList.ItemIndex <> -1) and (TopicList.ItemIndex <> -1) then begin
    SubTopic := TSubTopic(SubTopicList.Items.Objects[SubTopicList.ItemIndex]);
    Topic := Topics[TopicList.ItemIndex];

    if OEEStartTag.AsDouble < 1 then begin
      EventLog('OEE Start time fudged');
      SetOEEStartTag(Now - FifteenMinutes);
    end;

    EnableControls(False);
    if ClassicDateFormat then begin
      Tmp := Format('%s,%s,%s,%s,',
                     [SimpleDateFormat(OEEStartTag.AsDouble),
                      SimpleDateFormat(Now),
                      Topic.Code,
                      SubTopic.Code]);
    end else begin
      Tmp := Format('%s,%s,%s,%s,',
                     [StorageDate(OEEStartTag.AsDouble),
                      StorageDate(Now),
                      Topic.Code,
                      SubTopic.Code]);
    end;


    //if Assigned(SubTopic.CodeList) then begin
    //  TopicTag.AsString := Tmp + SubTopic.CodeList[CodeList.ItemIndex]
    //end else begin
      if SubTopic.Mask <> '' then begin
        Tmp1 := CodeEdit.Text;
        for I := 1 to Length(Tmp1) do begin
          if Tmp1[I] = ',' then
            Tmp1[I] := '.';
        end;
        TopicTag.AsString := Tmp + Tmp1;
      end else
        TopicTag.AsString := Tmp;
    //end;

    NoteLabel.Caption := SysObj.Localized.GetString('$OEE_INACTIVE');
    OEEStartTag.AsDouble := 0;
    EntryRequiredTag.AsBoolean := False;
    Close;
  end;
end;

procedure TOEE.SetOEEStartTag(When : TDateTime);
var Reg : TCNCRegistry;
begin
  OEEStartTag.AsDouble := Now;
  Reg := TCNCRegistry.Create;
  try
    Reg.OpenKey(RegistryBase + '\Persistent', True);
    OEEStartTag.WriteToRegistry(Reg);
  finally
    Reg.Free;
  end;
end;

procedure TOEE.ActivateOEE(aTag : TAbstractTag);
begin
  if not EntryRequiredTag.AsBoolean and OEEEnabled then begin
    EnableControls(True);
    EntryRequiredTag.AsBoolean := True;
    SetOEEStartTag(Now);
//    OEEStartTag.AsDouble := Now;
    CodeEdit.Text := '';
    TopicList.ItemIndex := -1;
    SubTopicList.Items.Clear;
    //CodeList.Items.Clear;
    ActiveSubTopic := nil;
  end;
end;

procedure TOEE.UserNameChange(aTag : TAbstractTag);
begin
  if EntryRequiredTag.AsBoolean then
    SetSubList(ActiveSubTopic);
end;

procedure TOEE.EnableControls(aOn : Boolean);
begin
  TopicList.Enabled := aOn;
  SubTopicList.Enabled := aOn;
  CodeEdit.Enabled := aOn;
  //CodeList.Enabled := aOn;
  ActivateBtn.Enabled := False;
  ActiveSubTopic := nil;
  AuthenticationRequired;
end;

procedure TOEE.Resize;
begin
{
  TopicListGroup.Left := 0;
  TopicListGroup.Top := KBPanel.Top + KBPanel.Height;
  TopicListGroup.Height := Height div 4;
  TopicListGroup.Width := Width;

  SubTopicListGroup.Left := 0;
  SubTopicListGroup.Top := TopicListGroup.Top + TopicListGroup.Height;
  SubTopicListGroup.Height := Height div 3;
  SubTopicListGroup.Width := Width;
 }
  //CodeListGroup.Left := 0;
  //CodeListGroup.Top := SubTopicListGroup.Top + SubTopicListGroup.Height;
  //CodeListGroup.Height := Height div 4;
  //CodeListGroup.Width := Width;

  //CodeEdit.Top := Height div 2 + 40;
  //CodeEdit.Left := 10;

  ActivateBtn.Left := Width div 2;
end;

procedure TOEE.TopicSelect(Sender : TObject);
begin
  if TopicList.ItemIndex <> -1 then begin
    SubTopicList.Items.Clear;
    SubTopicList.Items.AddStrings(Topics[TopicList.ItemIndex].List);
    ActiveSubTopic := nil;
  end else begin
    SubTopicList.Items.Clear;
  end;
  ActivateBtn.Enabled := False;
end;

procedure TOEE.SubTopicSelect(Sender : TObject);
var Sub : TSubTopic;
begin
  if SubTopicList.ItemIndex <> -1 then begin
    Sub := TSubTopic(SubTopicList.Items.Objects[SubTopicList.ItemIndex]);
    SetSubList(Sub);
  end;
end;

function TOEE.AuthenticationRequired : Boolean;
var Tmp : string;
    I : Integer;
begin
  Result := False;
  if Assigned(UserNameTag) and EntryRequiredTag.AsBoolean then begin
    Tmp := UserNameTag.AsString;
    if Assigned(ActiveSubTopic) and
       Assigned(ActiveSubTopic.AuthenticationList) and
      (ActiveSubTopic.AuthenticationList.Count > 0) then begin
      for I := 0 to ActiveSubTopic.AuthenticationList.Count - 1 do begin
        if CompareText(Tmp, ActiveSubTopic.AuthenticationList[I]) = 0 then begin
          NoteLabel.Caption := SysObj.Localized.GetString('$OEE_USER_ACCEPTED');
          Exit;
        end;
      end;
      Tmp := SysObj.Localized.GetString('$OEE_ONE_OF_THE_FOLLOWING') + ActiveSubTopic.AuthenticationList[0];

      for I := 1 to ActiveSubTopic.AuthenticationList.Count - 1 do
        Tmp := Tmp + ', ' + ActiveSubTopic.AuthenticationList[I];

      Tmp := Tmp + ' ]';

      NoteLabel.Caption := Tmp;
      Result := True;
    end else begin
      NoteLabel.Caption := FillInFormLang;
    end;
  end else begin
    NoteLabel.Caption := SysObj.Localized.GetString('$OEE_INACTIVE');
  end;
end;

{
procedure TOEE.CodeSelect(Sender : TObject);
begin
  //ActivateBtn.Enabled := (CodeList.ItemIndex <> -1) and not AuthenticationRequired;
end;
}
procedure TOEE.SetSubList(Sub : TSubTopic);
begin
  ActiveSubTopic := Sub;
  ActivateBtn.Enabled := not AuthenticationRequired;
  if Assigned(Sub) and False then begin
    if Assigned(Sub.CodeList) then begin
      //CodeListGroup.Visible := True;
      //CodeList.Items.Clear;
      //CodeList.Items.AddStrings(Sub.TextList);
      //CodeEdit.Visible := False;
      //ActivateBtn.Top := CodeListGroup.Top + CodeListGroup.Height;
      //ActivateBtn.Enabled := False; //(CodeList.ItemIndex <> -1) and not AuthenticationRequired
    end else begin
      //CodeListGroup.Visible := False;
      //CodeEdit.EditMask := Sub.Mask;
      CodeEdit.Visible := True;
      if Sub.Mask = '' then begin
        CodeEdit.Enabled := False;
        CodeEdit.Text := NoEntryRequiredLang;
      end else begin
        CodeEdit.Enabled := True;
        CodeEdit.Text := '';
      end;
      ActivateBtn.Top := SubTopicListGroup.Top + SubTopicListGroup.Height;
      ActivateBtn.Enabled := not AuthenticationRequired;
    end;
    NoteLabel.Height := Height - 4 - ActivateBtn.Top - ActivateBtn.Height;
  end;
end;

procedure TOEE.Activate(aTag : TAbstractTag);
begin
  if Self.EntryRequiredTag.AsBoolean then
    NoteLabel.Caption := FillInFormLang;
  NoteLabel.Font.Color := clRed;
  EnableControls(EntryRequiredTag.AsBoolean);
  inherited;
end;


// ------ END OF Visual control (method and event) ----------------


function TOEE.GetTopic(Index: Integer): TTopic;
begin
  Result := TTopic(List.Objects[Index])
end;

procedure TOEE.InstallComponent(Params: TParamStrings);
var S : TStringList;
    Q : TStreamQuoteTokenizer;
    I : Integer;
begin
  inherited;
  S := TStringList.Create;
  Q := TStreamQuoteTokenizer.Create;
  try
    ReadLive(S, Name);
    Q.SetStream(TStringStream.Create(S.Text));
    ReadConfig(Q);
  finally
    S.Free;
    Q.Free;
  end;

  TopicTag := TagPublish(TopicTagName, TStringTag);
  OEEStartTag := TagPublish(Name + 'EventStart', TDoubleTag);
  OEEStartTag.IsPersistent := True;

  ClassicDateFormat := Params.ReadBool('ClassicDateFormat', True);
  EnabledTag := TagPublish(EnabledTagName, TMethodTag);

  EntryRequiredTag := TagPublish(EntryRequiredTagName, TMethodTag);

  for I := 0 to List.Count - 1 do begin
    TopicList.Items.Add(Topics[I].Title);
  end;

  OEETopicLang:= SysObj.Localized.GetString('$OEE_TOPIC'); // = OEE Topic
  OEESubTopicLang:= SysObj.Localized.GetString('$OEE_SUB_TOPIC'); // = 'OEE Sub-Topic';
  OEECodeLang:= SysObj.Localized.GetString('$OEE_CODE'); // = 'OEE Code';
  NoEntryRequiredLang:= SysObj.Localized.GetString('$OEE_NO_ENTRY_REQUIRED'); // = 'No entry required';
  FillInFormLang:= SysObj.Localized.GetString('$OEE_SELECT_TOPIC'); // = 'Please select a topic and sub-topic from the list';

end;

procedure TOEE.Installed;
begin
  inherited;
  Font.Size := 12;
  ActivateTag := TagEvent(ActivateTagName, EdgeRising, TMethodTag, ActivateOEE);
  TagEvent(Name, EdgeFalling, TMethodTag, Activate);
  if OEEEnabled then begin
    EnableControls(True);
    EntryRequiredTag.AsBoolean := True;
    if OEEStartTag.AsDouble < Now - 365 then
      SetOEEStartTag(Now);
  end;
  UserNameTag := TagEvent(NameAccessUserName, EdgeChange, TStringTag, UserNameChange);
  EnabledTag.AsBoolean := OEEEnabled;
end;

procedure TOEE.ReadConfig(S: TStreamQuoteTokenizer);
var Token : string;

  procedure ReadListElement(ST : TSubTopic);
  begin
    ST.TextList.Add(S.Read);
    ST.CodeList.Add(S.Read);
    S.ExpectedToken(')');
  end;

  procedure ReadAuthentication(ST : TSubTopic);
  begin
    S.ExpectedTokens(['=', '{']);
    ST.AuthenticationList := TStringList.Create;
    Token := LowerCase(S.Read);
    while not S.EndOfStream do begin
      if Token = '}' then
        Exit
      else
        ST.AuthenticationList.Add(Token);

      Token := LowerCase(S.Read);
    end;
    raise Exception.Create('Unexpected end of file');
  end;

  procedure ReadList(ST : TSubTopic);
  begin
    S.ExpectedTokens(['=', '{']);
    Token := LowerCase(S.Read);
    ST.CodeList := TStringList.Create;
    ST.TextList := TStringList.Create;

    while not S.EndOfStream do begin
      if Token = '}' then
        Exit
      else if Token = '(' then
        ReadListElement(ST)
      else
        raise Exception.CreateFmt('Unexpected token %s', [Token]);
      Token := LowerCase(S.Read);
    end;
    raise Exception.Create('Unexpected end of file');
  end;

  function ReadSubTopic : TSubTopic;
  begin
    S.ExpectedTokens(['=', '{']);
    Result := TSubTopic.Create;
    Result.Mask := '';
    try
      Token := LowerCase(S.Read);
      while not S.EndOfStream do begin
        if Token = 'title' then
          Result.Title := SysObj.Localized.GetString( S.ReadStringAssign )
        else if Token = 'mask' then
          Result.Mask := S.ReadStringAssign
        else if Token = 'list' then
          ReadList(Result)
        else if Token = 'code' then
          Result.Code := S.ReadStringAssign
        else if Token = 'authentication' then
          ReadAuthentication(Result)
        else if Token = '}' then
          Exit
        else
          raise Exception.CreateFmt('Unexpected token [%s]', [Token]);
        Token := LowerCase(S.Read);
      end;
      raise Exception.Create('Unexpected end of file');
    except
      Result.Free;
      raise;
    end;
    Result := nil;
  end;

  function ReadTopic : TTopic;
  begin
    S.ExpectedTokens(['=', '{']);
    Result := TTopic.Create;
    Token := LowerCase(S.Read);
    while not S.EndOfStream do begin
      if Token = 'title' then
        Result.Title := SysObj.Localized.GetString( S.ReadStringAssign )
      else if Token = 'code' then
        Result.Code := S.ReadStringAssign
      else if Token = 'subtopic' then
        Result.AddSubTopic(ReadSubTopic)
      else if Token = '}' then
        Exit
      else
        raise Exception.CreateFmt('Unexpected token [%s]', [Token]);
      Token := LowerCase(S.Read);
    end;
    raise Exception.Create('Unexpected end of file');
  end;

begin
  S.Seperator := '{}=[]';
  S.QuoteChar := '''';
  S.Rewind;
  try
    S.ExpectedTokens([Name, '=', '{']);
  except
    Exit;
  end;

  OEEEnabled := True;

  TopicTagName := 'OEETopic';

  ActivateTagName := 'OEEActivate';
  EntryRequiredTagName := 'OEEEntryRequired';
  EnabledTagName := 'EnabledTag';

  try
    Token := LowerCase(S.Read);
    while not S.EndOfStream do begin
      if Token = 'topic' then
        AddTopic(ReadTopic)
      else if Token = 'enabled' then
        OEEEnabled := S.ReadBooleanAssign
      else if Token = 'activatetag' then
        ActivateTagName := S.ReadStringAssign
      else if Token = 'entryrequiredtag' then
        EntryRequiredTagName := S.ReadStringAssign
      else if Token = 'topictag' then
        TopicTagName := S.ReadStringAssign
      else if Token = 'enabledtag' then
        EnabledTagName := S.ReadStringAssign
      else if Token = '}' then
        Exit
      else
        raise Exception.CreateFmt('Unexpected token [%s]', [Token]);
      Token := LowerCase(S.Read);
    end;
    raise Exception.Create('Unexpected end of file');
  except
    on E : Exception do
      raise Exception.CreateFmt('%s at line %d in %s.cfg', [E.Message, S.Line, Name]);
  end;
end;

procedure TOEE.IShutdown;
begin
  if not EntryRequiredTag.AsBoolean then
    SetOEEStartTag(Now);

  inherited;
end;

{ TTopic }

procedure TTopic.AddSubTopic(aTopic: TSubTopic);
begin
  List.AddObject(aTopic.Title, aTopic);
end;

procedure TTopic.Clean;
begin
  while List.Count > 0 do begin
    List.Objects[0].Free;
    List.Delete(0);
  end;
end;

constructor TTopic.Create;
begin
  List := TStringList.Create;
  Title := SysObj.Localized.GetString('$NOTSET');
  Code := SysObj.Localized.GetString('$NOTSET');
end;

destructor TTopic.Destroy;
begin
  Clean;
  inherited;
end;

function TTopic.GetSubTopic(Index: Integer): TSubTopic;
begin
  Result := TSubTopic(List.Objects[Index]);
end;


{ TSubTopic }

constructor TSubTopic.Create;
begin
  Title := SysObj.Localized.GetString('$NOTSET');
  Code := SysObj.Localized.GetString('$NOTSET');
end;

destructor TSubTopic.Destroy;
begin
  if Assigned(TextList) then
    TextList.Free;
  if Assigned(CodeList) then
    CodeList.Free;
  inherited;
end;

initialization
  RegisterCNCClass(TOEE);
end.
