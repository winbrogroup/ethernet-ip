unit CompoundFiles;
interface
uses
  Windows,  SysUtils,  Classes,  StdCtrls, CNCTypes, Acknowledge, FaultRegistration
  {, , CNC32};

type
  {if section state = fsSaved then File matches disk image
   if section state = fsEdited then File has edits not saved}

  {use existing SaveToFile, LoadToFile for fragments}
  TCompoundFile = class;

  ECompoundFileError = class(Exception);

  {&&& should I make specific provision for opening read-only files. This could
  be another TFileState... These files don't need to be closed}

  TCompoundFile =  class
  private
    FSections: TStringList; {Name + TStrings}
    FFileName: String;
    FFileState: TFileState;
    OpenSection: Integer;
    procedure FreeSections;
    procedure CheckSectionOperation( const Msg: String);
  protected
    procedure FileNameChanged; virtual;
    procedure FileStateChanged; virtual;
    function GetFileState: TFileState;
    function GetFilename: String;
  public
    constructor Create;
    destructor Destroy; override;
    procedure SectionRead( const SectionName: String; Data: TStrings);{read only}
    procedure SectionOpen( const SectionName: String; Data: TStrings);{(file = open) and (sect = closed)}
    procedure SectionClose( Data: TStrings; SaveData: Boolean);
    procedure SectionNew( const SectionName: String);
    procedure SectionDelete( const SectionName: String);{(file = open) and (sect = closed)}
    procedure SectionRename( const OldName, NewName: String);{(file = open) and (sect = closed)}
    procedure FileOpen( const aFileName: String);{(FileState = fsClosed)}
    procedure FileSave;{(file = open) and (sect = closed)}
    procedure FileSaveAs( const aFileName: String);{(file = open) and (sect = closed)}
    procedure FileNew (const aFileName: String); { (file = closed) }
    function FileClose: Boolean;{(file = open) and (sect = closed)}
    property FileState: TFileState read GetFileState;
    property FileName: String read GetFileName;
  published
  end;


{Format issues
--------------
A compound file is a text file parsed into lines by the TStrings methods
LoadFromFile and SaveToFile. These accept $0D $0A or both as line delimiters.
SaveToFile uses $0D/$0A (CR/LF) I think - (confirm &&&)

a section starts with a start of section marker thus
[SectionName]

and ends with either EoF or another section marker

Within a section private formats must not use valid section markers as this will
mess up section parsing.
A valid section marker must
(i) have  [ as the FIRST non-blank character on the line
(ii) not contain the characters [ or ] other than as the first and last characters
on the line
The section name (within the []) should not contain leading or trailing blanks. If
present these will be ignored.
Section names are <not> case sensitive.
A section marker <may> have trailing spaces. These will be ignored.

Duplicate SectionNames are not allowed if they are detected in an incoming
file they throw an exception.
(NB could increase efficiency considerably by using sorted list for sections. This would
mean that sections would be saved in alphabetical order - prob. not desirable)

Blank lines are ignored in fileload

General Uses for TCompoundFile
To Open a file and extract section

CFM.FileOpen( name: String);
  CFM.FileState must be fsClosed. Only one file can be opened at a time.

CFM.OpenSection( SectionName: String; S: TStrings);
  Copies contents of [SectionName] into S.
  S must be initialised.
  S will be cleared
  This call will fail if another section is already open

General Comment
---------------
Consider example CF: TCompound file, acted on by 3 editors, A, B, and C.
FC may have one file open at a time. On opening, the file is parsed into sections,
which are stored in the list FSections. The file is now ready for editing.

In order to edit a section an editor must open a section by calling SectionOpen. Only one
editor is allowed to work on the open file at a time. If another section is open already,
the call will fail.

To Open a file call
  CF.FileOpen.

To Edit a section an editor must call
  CF.SectionOpen  then edit the string returned.
When the editor has finished it must call
  CF.SectionClose passing SaveData as true or false to indicate whether edits should
be abandoned or stored. If SaveData is true then the Data passed back is stored by
  CF but not saved to disk

  The entire file is

(more info &&&)

what about:
rename/delete section? Renaming or Deleting are inappropriate with a system

{TOffline editor owns its own manager. You don't
need to instantiate one in [Components]

[Components]
OfflineEditor=TOfflineEditor
FileManager=TCompoundFile (not needed for offline)

[OfflineEditor]
;add SECTION=EditorName, Section Description
;Section desc is optional If not present use SectionName
Section.StdEditor = Operation Description
Section.MicroholeEditor = EDM Process
Section.GCodeEditor = CNC Program
Section.StdEditor = Signature Data
FileMask=programs\*.nc
;relative to exe
;Note that each editor 'knows' its section name.
;The names in use are declared in Named.Pas.
;The 'value' of the Section.Editor key is the
;'informal' name of the section - i.e the name
;that appears in the section list
}



implementation
uses
  Controls,
  Dialogs,
  CoreCNC;

resourcestring
  SectionAlreadyExists = 'Cannot %s. Section already exists'; {[xxxOperation]}
  SectionDoesNotExist = 'Cannot %s. Section does not exist';  {[xxxOperation]}
  SectionNotAvailable = 'Cannot %s while section %s is open'; {[xxxOperation]}
  SectionCannotRead = 'Cannot %s while it is open for editing';
  FileNotAvailable = 'Cannot %s unless file is open';  {[xxxOperation]}
  FileOpenError = 'Cannot open file %s';
  FileSaveAsError = '%s is not a valid filename';


  ErrorHeader = 'Error editing %s: ';                  {[Filename]}


  {xxxOperation}
  CreateSectionOperation = 'create section %s';        {[SectionName]}
  ReadOperation = 'read section %s';
  OpenOperation = 'edit section %s';                   {[SectionName]}
  DeleteOperation = 'delete section %s';               {[SectionName]}
  RenameOperation = 'rename section %s';               {[SectionName]}

  FileSaveOperation = 'save';
  FileSaveAsOperation = 'save as';
  FileCloseOperation = 'close';

  CannotCloseSection = 'cannot close section';

  CannotOpenNewFile = 'Cannot open file %s while file %s is still open';
  DoYouWantToSaveMsg = 'File %s has been edited. Do you want to save your changes?';



constructor TCompoundFile.Create;
begin
  inherited Create;
  FSections:= TStringList.Create;
  OpenSection:= -1
end;

function TCompoundFile.GetFileState: TFileState;
begin
  Result:= FFileState
end;

procedure TCompoundFile.FileNameChanged;
begin
end;

procedure TCompoundFile.FileStateChanged;
begin
end;

function TCompoundFile.GetFilename: String;
begin
{  if FileState = fsClosed then
    Result:= NoFileText
  else }
    Result:= FFileName
end;

procedure TCompoundFile.FreeSections;
var
  i: Integer;
begin
  for i:= 0 to (FSections.Count - 1) do
    TStrings(FSections.Objects[i]).Free;
  FSections.Clear;
  OpenSection:= -1
end;

destructor TCompoundFile.Destroy;
{var
  i: Integer; }
begin
  FreeSections;
  FSections.Free;
{  for i:= 0 to (ListBox.Items.Count - 1) do
    TEditorListItem(ListBox.Items.Objects[i]).Free; }
    {cannot destroy because parent is missing! &&&}
  inherited Destroy
end;

{SectionRead
 Copy the contents of a section to Data.
 This procedure does not 'Open' the section - do not close.
 This procedure will fail (by raising ECompoundFileError) if
   (a) File is not open
   (b) Section does not exist
   (c) Section is open for editing

 Weakness: Once a section is read using ReadSection, there is nothing to
 prevent subsequent editing operations which would cause the 'Data' to
 become out of date}

procedure TCompoundFile.SectionRead( const SectionName: String; Data: TStrings);
var
  i: Integer;
begin
  if FileState = fsClosed then
      raise ECompoundFileError.CreateFmt(FileNotAvailable,
      [Format(ReadOperation, [SectionName])]);
  i:= FSections.IndexOf(UpperCase(SectionName));

  if (OpenSection <> -1) and (i = OpenSection) then
    raise ECompoundFileError.CreateFmt(SectionCannotRead,
         [Format(ReadOperation, [SectionName])]);
  Data.Clear;
  if i > -1 then  //MGL 8/9/98
    Data.AddStrings(TStrings(FSections.Objects[i]))
end;

{Open section for editing. Section must be closed later.
 Only one section can be open for editing at any moment.
 While open for editing a section cannot be read with Section Read}
procedure TCompoundFile.SectionOpen( const SectionName: String; Data: TStrings);
var
  i: Integer;
begin
  {raise exception if (FileState = fsClosed) or (OpenSection <> -1)}
  CheckSectionOperation(Format(OpenOperation, [SectionName]));
  i:= FSections.IndexOf(UpperCase(SectionName));
  if i = -1 then
  begin
    SectionNew(SectionName);
    i:= FSections.IndexOf(UpperCase(SectionName))
  end;
  Data.Clear;
  Data.AddStrings(TStrings(FSections.Objects[i]));
  OpenSection:= i;
end;

{Sections opened for editing with SectionOpen must be closed using SectionClose
it is assumed that the most recently opened section is being closed, as
SectionOpen will fail if any section is open}
procedure TCompoundFile.SectionClose( Data: TStrings; SaveData: Boolean);
begin
  if (OpenSection = -1) then
    raise ECompoundFileError.CreateFmt(ErrorHeader + CannotCloseSection,
                                              [FileName]);
  if SaveData then
  with TStrings(FSections.Objects[OpenSection]) do
  begin
    Clear;
    AddStrings(Data);
    FFileState:= fsEdited;
    FileStateChanged
  end;
  OpenSection:= -1
end;

{Create a new section. This will fail if
  (a) file is not open
  (b) SectionName already exists}
procedure TCompoundFile.SectionNew( const SectionName: String);
begin
  if FileState = fsClosed then
    raise ECompoundFileError.CreateFmt(FileNotAvailable,
         [Format(CreateSectionOperation, [SectionName])]);
  if FSections.IndexOf(UpperCase(SectionName)) <> -1 then
    raise ECompoundFileError.CreateFmt(SectionAlreadyExists,
         [Format(CreateSectionOperation, [SectionName])]);
  FSections.AddObject(UpperCase(SectionName), TStringList.Create);
end;

{Section Delete
 fails if
   (a) File not open
   (b) Any section is open for editing
     &&&Why maybe just section being edited}

procedure TCompoundFile.SectionDelete( const SectionName: String);
var
  i: Integer;
begin
  CheckSectionOperation(Format(DeleteOperation, [SectionName]));
  i:= FSections.IndexOf(UpperCase(SectionName));
  if i <> -1 then
  begin
    TStrings(FSections.Objects[i]).Free;
    FSections.Delete(i)
  end
end;

{Section Rename
 fails if
   (a) File not open
   (b) Any section is open for editing
     &&&Why maybe just section being edited}

procedure TCompoundFile.SectionRename(const OldName, NewName: String);
var
  i: Integer;
begin
{check for duplicates &&&}
  CheckSectionOperation(Format(RenameOperation, [OldName]));
  i:= FSections.IndexOf(UpperCase(OldName));
  if i <> -1 then
    FSections[i]:= UpperCase(NewName)
  {&&& raise error if not found?}
end;

procedure TCompoundFile.FileOpen( const aFileName: String);
          {only if FileState = fsClosed}
var
  Inf: TStringList;
  Line: Integer;
  Sect: Integer;
  SectName: String;

  function IsHeading: Boolean;
  begin
    SectName:= Trim(Inf[Line]);
    Result:= (Length(SectName) > 2) and
             (SectName[1] = '[') and
             (SectName[Length(SectName)] = ']');
    if Result then
      SectName:= UpperCase(Trim(Copy(SectName, 2, Length(SectName) -2)))
  end;

begin
  if FileState <> fsClosed then
    ECompoundFileError.CreateFmt(CannotOpenNewFile, [aFileName, FileName]);
  FreeSections;
  Inf:= TStringList.Create;
  try
    try
      Inf.LoadFromFile(aFileName);
    except
      on EFOpenError do
        raise ECompoundFileError.CreateFmt(FileOpenError, [aFileName]);
    end;
    Sect:= -1;  {ignore anything prior to first section}
    for Line:= 0 to (Inf.Count - 1) do
    begin
{      if (Inf[Line] <> '') then  (allow formatting lines in files... mgl 4/8)
      begin }
      if IsHeading then
        Sect:= FSections.AddObject(SectName, TStringList.Create)
      else
        if Sect > -1 then
          TStrings(FSections.Objects[Sect]).Add(Inf[Line])
{      end; }
    end;
    FFileState:= fsSaved;
    FFilename:= aFileName;
    FileNameChanged;
    FileStateChanged;
  finally
    Inf.Free
  end
end;

procedure TCompoundFile.FileNew (const aFileName: String);
begin
  if FileState <> fsClosed then
    ECompoundFileError.CreateFmt(CannotOpenNewFile, [aFileName, FileName]);
  FreeSections;
  FFileState:= fsSaved;
  FFilename:= aFileName;
  FileNameChanged;
  FileStateChanged;
end;

procedure TCompoundFile.FileSave;
var
  OutF: TStrings;
  Sect: Integer;
begin
  {raise exception if (FileState = fsClosed) or (OpenSection <> -1)}
  CheckSectionOperation(FileSaveOperation);
  if FFileState = fsEdited then  {otherwise no point}
  begin
    OutF:= TStringList.Create;
    try
      for Sect:= 0 to (FSections.Count - 1) do
      begin
        OutF.Add(Format('[%s]', [FSections[Sect]]));
        OutF.AddStrings(TStrings(FSections.Objects[Sect]))
      end;
      OutF.SaveToFile(FileName);
      FFileState:= fsSaved;
      FileStateChanged
    finally
      OutF.Free
    end
  end
end;

procedure TCompoundFile.FileSaveAs( const aFileName: String);
var
  H: THandle;
begin
  {raise exception if (FileState = fsClosed) or (OpenSection <> -1)}
  CheckSectionOperation(FileSaveAsOperation);
  h:= FileCreate(aFilename);
  CloseHandle(h);
  if h = INVALID_HANDLE_VALUE then
    raise ECompoundFileError.CreateFmt(FileSaveAsError, [aFilename]);
  FFilename:= aFileName;
  FFileState:= fsEdited; {force save}
  FileSave;
  FileNameChanged;
  FileStateChanged
end;

{Returns true if file closed or False if user refuses to accept edits}
function TCompoundFile.FileClose: Boolean;
begin
  {raise exception if (FileState = fsClosed) or (OpenSection <> -1)}
  {throwaway edits, or raise dialog...}
  if FileState = fsClosed then
  begin
    Result:= true
  end else
  begin
    CheckSectionOperation(FileCloseOperation);
    if FileState = fsEdited then
    begin
      Result := Acknowledge.AreYouSure(Format(DoYouWantToSaveMsg, [FileName]));
{      Result:=  MessageDlg(Format(DoYouWantToSaveMsg, [FileName]), mtConfirmation,
                 [mbOK, mbCancel], 0) = mrOK; }
{      case MessageDlg(Format(mbYesNoCancel, [FileName]), mtConfirmation,
                 [mbOK, mbCancel], 0) of
       mrNo    :
       mrYes   :
       mrCancel: }
      if Result then
        FileSave
    end;
    Result:= true;  // Sort this out later Just because you dont want to save
                    // does not mean you dont want to load
    if Result then begin
      FreeSections;
      FFilename:= '';
      FFileState:= fsClosed;
      FileNameChanged;
      FileStateChanged
    end
  end
end;

procedure TCompoundFile.CheckSectionOperation( const Msg: String);
begin
  if FileState = fsClosed then
    raise ECompoundFileError.CreateFmt(FileNotAvailable, [Msg]);
  if OpenSection <> -1 then
    raise ECompoundFileError.CreateFmt(ErrorHeader + SectionNotAvailable,
             [FileName, Msg, FSections[OpenSection]]);
end;



end.
