unit PComm32;

interface

uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, ExtCtrls, CNCTypes, Response, CNC32;

const
  PmacLibraryfunction = 'Pmac.dll library function missing [%s]';
  PmaclibraryNotFound = 'Pmac.dll Not found on path';

type
  TPCommVersion = (
    pvPmac,
    pvTurbo
  );

  TPmacMotorStatus1 = (
    pms1RapidMoveSelect, // Turbo
    pms1AlternateCommandOutput, // Turbo
    pms1SoftwareCaptureEnable, // Turbo
    pms1TriggerOnErrorEnable, // Turbo
    pms1PositionFollowingEnable, // Turbo
    pms1PositionFollowingOffsetMode, // Turbo
    pms1CommutationEnable, // Turbo
    pms1YAddressCommutationEncoder, // Turbo
    pms1UserWrittenServoEnable, // Turbo
    pms1UserWrittenPhaseEnable, // Turbo
    pms1HomeSearch,
    pms1BlockRequest,
    pms1AbortDeceleration,
    pms1ZeroCommanded,
    pms1DataBlockError,
    pms1DwellInProgress,
    pms1IntegrationMode,
    pms1RunningDefinedMove,
    pms1OpenLoop,
    pms1PhasedMotor,
    pms1HandWheelEnabled,
    pms1PositiveLimit,
    pms1NegativeLimit,
    pms1MotorActivated
  );

  TPmacMotorStatus2 = (
    pms2InPosition,
    pms2WarnFollowError,
    pms2FatalFollowError,
    pms2AmplifierFault,

    pms2BacklashDirection,
    pms2IITAmplifierFault,
    pms2FatalFollowErrorLatched,
    pms2TriggerMove,

    pms2PhasingSearchError,
    pms2Reserved9,
    pms2HomeComplete,
    pms2StoppedOnPositionLimit,

    pms2Reserved12,
    pms2Reserved13,
    pms2AmplifierEnable,
    pms2Reserved15,
    pms2Reserved16,
    pms2Reserved17,
    pms2Reserved18,
    pms2Reserved19,
    pms2CoordSystem0,
    pms2CoordSystem1,
    pms2CoordSystem2,
    pms2AssignedToCoordSystem
  );


  TPmacGlobalStatus1 = (
    pgs1SerialAddressing,
    pgs1AllCardsAddressed,
    pgs1Reserver2,
    pgs1Reserver3,
    pgs1Reserver4,
    pgs1MacroCommunicationError,
    pgs1TWSParityError,
    pgs1Internal7,
    pgs1Internal8,
    pgs1Internal9,
    pgs1EAROMCSumError,
    pgs1DPRAMError,
    pgs1PROMCSumError,
    pgs1MemoryCSumError,
    pgs1LeadScrewCompensationOn,
    pgs1Reserved15,
    pgs1Reserved16,
    pgs1DataGatherOnTrigger,
    pgs1DataGatherOnServo,
    pgs1DataGatherActive,
    pgs1ServoError,
    pgs1ServoActive,
    pgs1RealTimeInterruptRetry,
    pgs1RealTimeInterruptActive
  );

  TPmacCoordSystemStatus1 = (
    pcs1ProgramRunning,
    pcs1SingleStepMode,
    pcs1ContinuousMotionMode,
    pcs1MoveByTimeMode,
    pcs1ContinuousMotionRequest,
    pcs1IncRadiusVector,
    pcs1AAxisIncMode,
    pcs1AAxisFrax,
    pcs1BAxisIncMode,
    pcs1BAxisFrax,
    pcs1CAxisIncMode,
    pcs1CAxisFrax,
    pcs1UAxisIncMode,
    pcs1UAxisFrax,
    pcs1VAxisIncMode,
    pcs1VAxisFrax,
    pcs1WAxisIncMode,
    pcs1WAxisFrax,
    pcs1XAxisIncMode,
    pcs1XAxisFrax,
    pcs1YAxisIncMode,
    pcs1YAxisFrax,
    pcs1ZAxisIncMode,
    pcs1ZAxisFrax
  );

  TPmacCoordSystemStatus2 = (
    pcs2MotionTypeBit0,
    pcs2MotionTypeBit1,
    pcs2CutterCompensationOn,
    pcs2CutterCompensationLeft,
    pcs2MotionTypeBit4,
    pcs2SegMoveStopRequest,
    pcs2SegMoveAcceleration,
    pcs2SegMoveInProgress,
    pcs2ExecutingJogMoveTo,
    pcs2Internal9,
    pcs2CutterCompensationStopRequest,
    pcs2CutterCompensationOutsideMove,
    pcs2Internal12,
    pcs2SynchronousMVariable,
    pcs2EndOfBlockStop,
    pcs2Internal15,
    pcs2RotaryBufferRequest,
    pcs2InPosition,
    pcs2WarnFollowError,
    pcs2FatalFollowError,
    pcs2AmplifierFaultError,
    pcs2CircleRadiusError,
    pcs2RunTimeError,
    pcs2ProgramHoldStop
  );

  TPmacGlobalStatus2 = (
    pgs2Reserved0,
    pgs2Reserved1,
    pgs2Reserved2,
    pgs2Reserved3,
    pgs2Reserved4,
    pgs2Reserved5,
    pgs2Reserved6,
    pgs2Reserved7,
    pgs2Internal8,
    pgs2Internal9,
    pgs2Internal10,
    pgsFixedBufferFull,
    pgs2Internal11,
    pgs2Internal12,
    pgs2VMECommunications,
    pgs2PlcBufferOpen,
    pgs2RotaryBufferOpen,
    pgs2ProgramBufferOpen,
    pgs2Unused20,
    pgs2Unused21,
    pgs2HostCommumicationMode,
    pgs2Unused23
  );

  TPmacUlMask = (
    ulInPosition,
    ulBufferRequest,
    ulFatalFollowError,
    ulWarnFollowError,
    ulHostRequest,
    ulUser0,
    ulUser1,
    ulUser2
  );

  TPmacUlMasks = set of TPmacUlMask;

  TPmacMotorStatii1 = set of TPmacMotorStatus1;
  TPmacMotorStatii2 = set of TPmacMotorStatus2;
  TPmacCoordSystemStatii1 = set of TPmacCoordSystemStatus1;
  TPmacCoordSystemStatii2 = set of TPmacCoordSystemStatus2;
  TPmacGlobalStatii1 = set of TPmacGlobalStatus1;
  TPmacGlobalStatii2 = set of TPmacGlobalStatus2;


  TIntegers = array [0..0] of Integer;
  TDoubles = array [0..0] of Double;

  PIntegers = ^TIntegers;
  PDoubles = ^TDoubles;

  I64Union = record
    case Byte of
      0 : (I64 : Int64;);
      1 : (I : array [0..1] of Integer;);
      2 : (B : array [0..7] of Byte );
  end;

  IUnion = record
    case Byte of
      1 : (I : Integer;);
      2 : (B : array [0..3] of Byte );
      3 : (W : array [0..1] of Word );
  end;

  TPmacMotorServoData = packed record
    CommandedPosition : I64Union;
    ActualPosition : I64Union;
    MasterPosition : I64Union;
    Compensation : I64Union;

    PreviousDAC : Integer;
    ServoStatus : Cardinal;
    ActualVelocity : Integer;
    TimeLeftInMove : Integer;
    HandwheelPointer : Integer;
    Spare : Cardinal;
    Spare1 : Cardinal;
  end;

  // Starting at 0x48
  TPmacServoFixedData = packed record
    Data : array [0..7] of TPmacMotorServoData;
  end;

  PPmacServoFixedData = ^TPmacServoFixedData;


  TPmacCoordinateData = packed record
    MotorTargetPosition : I64Union;
    PositionBias : I64Union;
    MotorStatusWord : Cardinal; // returned by ?
    SystemStatusDef : I64Union; // returned by ??
    TargetPosition : array [0..8] of I64Union; // Note this array is always ABCUVWXYZ
    ProgExecTime : Integer;
    ProgramLinesRemaining : Integer;
    TimeRemainingInMove : Integer;
    TimeRemainingInAccel : Integer;
    ProgramExecAddress : Integer;
    MotorAverageActualVelocity : Integer;
  end;

  TPmacCoordinateFixedData = packed record
    Data : array [0..7] of TPmacCoordinateData; // Note thier are 8 members
  end;
  PPmacCoordinateFixedData = ^TPmacCoordinateFixedData;

  // Turbo DPR structures

  {
    Follow Error = D:$88 + D:$8d + D:$90 - D:$8b
                  CmdPos + MasterPos - CompPos - ActPos

    NetActPos    = D:$8b + D:$cc + D:$90 - D:$8d *X:$B0.5
                   ActPos + Bias + CompPos - MasterPos


    CmdPos + Master - CompPos - Act = FERR
    Act + Bias + CompPos - Master = NetActPos

    CmdPos + Bias = FERR + NetActPos
  }
  TTurboPmacMotorServoData = packed record
    FollowError: I64Union;     // $74  D:$88 + D:$8d + D:$90 - D:$8b
    ServoCommand: Cardinal;    // $7c
    MotorGeneralStatus: TPmacMotorStatii1; // $84 From Y:$bo
    MotorServoStatus: TPmacMotorStatii2;// $80 From Y:$c0
    MotorPositionBias: I64Union; // $88 from D:$cc /
    MotorFilteredActualVelocity: Integer; // $90
    MotorMasterPosition: I64Union; // $94 from D:$8d /
    MotorNetActualPosition: I64Union; // $9c D:$8b + D:$cc + D:$90 - D:$8d *X:$B0.5
  end;

  TTurboPmacCoordinateSystemData = packed record
    ProgrammedFeedRate: I64Union; // $69c units / msec      /
    FeedrateOverride: Cardinal; // $6a4 units of I10              /
    //FeedrateOverrideSource: Cardinal; // $6a8 units of I10              /
    CoordStatus1: TPmacCoordSystemStatii1; // $6ac from D:$2040
    TargetPosition: array[0..8] of I64Union; // $6b4 ABCUVWXYZ order
    ProgramStatus: TPmacCoordSystemStatii2; // $6fc from Y:$203f
    RotaryRemaining: Cardinal; // $700
    TimeRemainingInMove: Cardinal; // $704
    TimeRemainingInAccDec: Cardinal; // $708
    ProgramExecutionOffset: Integer; // $70c
    Reserver1: array[0..4] of Integer; // $710
  end;


const
  vbgwDataType0 = 1; // 0=Y; 1=L; 2=X; 3=Special
  vbgwDataType1 = 2;
  vbgwDataType2 = 4;
  vbgwwidth0    = 8; // Only 1,4,8,12,16,20 are valid: 0=24bits
  vbgwwidth1    = $10;
  vbgwwidth2    = $20;
  vbgwwidth13   = $40;
  vbgwwidth14   = $80;
  vbgwOffset0   = $100;    // 0..23
  vbgwOffset1   = $200;
  vbgwOffset2   = $400;
  vbgwOffset3   = $800;
  vbgwOffset4   = $1000;
  vbgwSpecialType0 = $2000;// your guess is as good as mine?
  vbgwSpecialType1 = $4000;
  vbgwSpecialType2 = $8000;

  // VBG Write structure
type
  TVBGWFormat = packed record
    type_addr : Integer;
    data1, data2 : Integer;
  end;

  AVBGWFormat = array [0..31] of TVBGWFormat;
  PAVBGWFormat = ^AVBGWFormat;


const
  DprOK = 0;
  DprBufBsy = 1;
  DprEOF = 2;
  DprFltErr = -1;
  DprStrToErr = -2;
  DprMtrErr = -3;
  DprPlcErr = -4;
  DprAxisErr = -5;
  DprCmdErr = -6;
  DprCmdMaxErr = -7;
  DprIntErr = -8;
  DprBufErr = -9;
  DprOutputFileErr = -10;
  DprInputFileErr = -11;

type
  TPmacDownLoadMessageProc = procedure(msg : PChar; NewLine : boolean); stdcall;
  TPmacProgressProc        = procedure(aPercent : Integer); stdcall;
  TPmacDevice              = function (DN : Dword): Boolean; stdcall;
  TPmacDPR_Dev_I           = function (DN : Dword): Integer; stdcall;
  TPmacFlush               = procedure(DN : Dword); stdcall;
  TPmacGetResponse         = function (DN : Dword; rx : pChar; max : word; cmd : pChar) : integer;stdcall;
  TPmacGetControlResponse  = function (DN : Dword; rx : pChar; max : word; cmd : Char) : integer;stdcall;
  TPmacGetLine             = function (DN : Dword; rx : pChar; max : word) : integer;stdcall;
  TPmacReadReady           = function (DN : Dword) : boolean;stdcall;
  TPmacSendChar            = function (DN : Dword;cmd : Byte): Boolean;stdcall;
  TPmacSendLine            = function (DN : Dword; Text : pChar) : integer;stdcall;
  TPmacSendCommand         = procedure(DN : Dword; Text : pChar);stdcall;
  TPmacGetVariable         = function (DN : Dword; Vtype : Char;Number : short;  def : Double ): Double;stdcall;
  TPmacSetVariable         = procedure(DN : Dword; Vtype : Char;Number : short;  Value : Double )stdcall;
  TPmacDownLoad            = function ( DN : Dword;
                                        msg: TPmacDownLoadMessageProc;
                                        get : pointer;
                                        Progress : TPmacProgressProc;
                                        filename : PChar;
                                        macro, map, log, dnld : boolean) : boolean; stdcall;
  TPmacDPR_DevI_I          = function (DN : Dword; Offset : Integer) : Integer;stdcall;
  TPmacDPR_DevIIP_P        = function (DN : Dword; Offset, Count : Integer; Data : Pointer) : Pointer;stdcall;
  TPmacDPR_DevII           = procedure(DN, M_CS, Data : Integer); stdcall;
  TPmacDPR_DevII_I         = function(DN, M_CS, Data : Integer) : Integer; stdcall;
  TPmacDPR_DevI            = procedure(DN, Data : Integer); stdcall;
  TPmacDPR_DevPcI_B        = function(DN : Integer; T : PChar; Buffer : Integer) : Byte; stdcall;

  TPmacDPR_DevIII_I        = function(DN, A, B, C : Integer) : Integer; stdcall;
  TPmacDPR_DevPcI_Pc       = function(DN : Integer; T : PChar; I : Integer) : PChar; stdcall;
  TPmacDPR_DevIIP_I        = function(DN, A, B : Integer; P : Pointer) : Integer; stdcall;
  TPmacDPR_DevIP_I         = function(DN, A : Integer; P : Pointer) : Integer; stdcall;
  TPmacDPR_DevID_D         = function(DN, A : Integer; B : Double) : Double; stdcall;
  TPmacDPR_DevID           = procedure(DN, A : Integer; B : Double); stdcall;
  TPmacDPR_DevI_D         = function(DN, A : Integer) : Double; stdcall;
  TPmacDRPGetCompensationPos = procedure(DN, Motor : Integer; Units : Double; var Value : Double); stdcall;
  TPmacDPRBackgroundEx = procedure(DN, Ena: Integer; Upd, Coord: Word); stdcall;

  TPmacDPRVectorVelocity = function(DN, Count : Integer; const Motors : TIntegers; const Units : TDoubles) : Double;
//double PmacDPRVectorVelocity(DWORD dwDevice,int num,int motor[], double units[]);

//  TPmacDPR_Dev_I           = function(DN : Integer) : Integer; stdcall;

  TVBGWriteFormat = record
    TypeAddress : Integer;
    Data1 : Integer;
    Data2 : Integer;
  end;

  TPcomm32 = class(TObject)
  private
    Version: TPCommVersion;
    FDeviceNumber   : Integer;
    LibCode : THandle;

    FControlPanel     : Boolean;
    FRealTime         : Boolean;
    FBackGround       : Boolean;

    OpenPMACDevice    : TPmacDevice;
    ClosePMACDevice   : TPmacDevice;
    PmacFlush         : TPmacFlush;
    GetResponse       : TPmacGetResponse;
    GetControlResponse: TPmacGetControlResponse;
    GetLine           : TPmacGetLine;
    IsLineWaiting     : TPmacReadReady;
    SendChar          : TPmacSendChar;
    SendLine          : TPmacSendLine;
    SendCmnd          : TPmacSendCommand;
    GetVariable       : TPmacGetVariable;
    SetVariable       : TPmacSetVariable;
    DownLoad          : TPmacDownLoad;
    DPRGetDWord       : TPmacDPR_DevI_I;
    DPRSetDWord       : TPmacDPR_DevII;
    DPRGetFloat       : TPmacDPR_DevI_D;
    DPRSetFloat       : TPmacDPR_DevID;
    DPRGetMem         : TPmacDPR_DevIIP_P;
    DPRSetMem         : TPmacDPR_DevIIP_P;

    DPRSetJogPositiveBit : TPmacDPR_DevII;
    DPRSetJogNegativeBit : TPmacDPR_DevII;
    DPRSetJogReturnBit: TPmacDPR_DevII;
    DPRSetRun         : TPmacDPR_DevII;
    DPRSetHome        : TPmacDPR_DevII;
    DPRSetStop        : TPmacDPR_DevII;
    DPRSetHold        : TPmacDPR_DevII;
    DPRSetStep        : TPmacDPR_DevII;

    DPRSetJogReturn   : TPmacDPR_DEVI;

    DPRGetJogPositive : TPmacDPR_DevI_I;
    DPRGetJogNegative : TPmacDPR_DevI_I;
    DPRGetJogReturn   : TPmacDPR_DevI_I;
    DPRGetRun         : TPmacDPR_DevI_I;
    DPRGetStop        : TPmacDPR_DevI_I;
    DPRGetHome        : TPmacDPR_DevI_I;
    DPRGetHold        : TPmacDPR_DevI_I;
    DPRGetStep        : TPmacDPR_DevI_I;

    DPRGetFOEnable    : TPmacDPR_DevI_I;
    DPRSetFOEnable    : TPmacDPR_DevII;

    DPRGetFOValue     : TPmacDPR_DevI_I;
    DPRSetFOValue     : TPmacDPR_DevII;

    DPRControlPanel   : TPmacDPR_DevI_I;

    DPRGetRequestBit  : TPmacDPR_DevI_I;
    DPRSetRequestBit  : TPmacDPR_DevII;

    DPRRealTime       : TPmacDPR_DevII_I;
    DPRSetMotors      : TPmacDPR_DevI;
    DPRSetHostBusy    : TPmacDPR_DevI;
    DPRGetHostBusy    : TPmacDPR_Dev_I;
    DPRGetPmacBusy    : TPmacDPR_Dev_I;

    PmacDPRAsciiStrToRotA: TPmacDPR_DevPcI_B;
    PmacDPRRotBufInit : TPmacDPR_DevI_I;
    PmacDPRRotBuf : TPmacDPR_DevI_I;
    PmacDPRRotBufRemove   : TPmacDPR_DevI_I;
    PmacDPRRotBufChange   : TPmacDPR_DevII_I;
    PmacDPRRotBufClr      : TPmacDPR_DevI;
    DPRRotBuf         : TPmacDPR_DevI_I;

    INTRFireEventInit : TPmacDPR_DevII_I;
    INTRWndMsgInit    : TPmacDPR_DevIII_I;

    PmacGetRomDate    : TPmacDPR_DevPcI_Pc;
    PmacGetRomVersion : TPmacDPR_DevPcI_Pc;
    PmacGetType       : TPmacDPR_Dev_I;




    PmacDPRBackground         : TPmacDPR_DevI_I;
    PmacDPRBackgroundEx : TPmacDPRBackgroundEx;
    PmacDPRBufLast            : TPmacDPR_Dev_I;
    PmacDPRGetVBGAddress      : TPmacDPR_DevII_I;
    PmacDPRGetVBGNumEntries   : TPmacDPR_DevI_I;
    PmacDPRGetVBGDataOffset   : TPmacDPR_DevI_I;
    PmacDPRGetVBGAddrOffset   : TPmacDPR_DevI_I;
    PmacDPRGetVBGServoTimer   : TPmacDPR_Dev_I;
    PmacDPRGetVBGStartAddr    : TPmacDPR_Dev_I;
    PmacDPRGetVBGTotalEntries : TPmacDPR_Dev_I;
    PmacDPRVarBufChange       : TPmacDPR_DevIIP_I;
    PmacDPRVarBufInit         : TPmacDPR_DevIP_I;
    PmacDPRVarBufRead         : TPmacDPR_DevIIP_I;
    PmacDPRVarBufRemove       : TPmacDPR_DevI_I;
    PmacDPRWriteBuffer        : TPmacDPR_DevIP_I;

    PmacDPRPe                 : TPmacDPR_DevI_I;
    PmacDPRProgRemaining      : TPmacDPR_DevI_I;
    PmacDPRTimeRemInMove      : TPmacDPR_DevI_I;
    PmacDPRProgRunning        : TPmacDPR_DevI_I;

    PmacDPRGetCommandedPos    : TPmacDPR_DevID_D;
    PmacDPRPosition           : TPmacDPR_DevID_D;
    PmacDPRFollowError        : TPmacDPR_DevID_D;
    PmacDPRGetVel             : TPmacDPR_DevID_D;
//    PmacDPRGetMasterPos      : TPmacDPR_DevIDPd_D;
    PmacDPRGetMoveTime        : TPmacDPR_DevI_I;
    PmacDPRGetPrevDAC         : TPmacDPR_DevI_I;
    PmacDPRMotorServoStatus   : TPmacDPR_DevI_I;

    PmacDRPGetCompensationPos : TPmacDRPGetCompensationPos;
    PmacDPRGetTargetPos       : TPmacDPR_DevID_D;
    PmacDPRVectorVelocity     : TPmacDPRVectorVelocity;
    PmacDPRGetBIASPos         : TPmacDPR_DevID_D;


    ServoFixedData : PPmacServoFixedData;
    CoordinateFixedData : PPmacCoordinateFixedData;

    OutDPR: array[0..255] of Integer;
    InDPR: array[0..255] of Integer;
    TurboPmacMotorServoData: array[0..9] of TTurboPmacMotorServoData;
    TurboPmacCoordinateSystemData: array [0..8] of TTurboPmacCoordinateSystemData;

    function GetMCodeValue(Index : Integer): Double;
    function GetICodeValue(Index : Integer): Double;
    function GetPCodeValue(Index : Integer): Double;
    function  GetQCodeValue(Index : Integer): Double;
    procedure SetMCodeValue(Index : Integer ;Value : Double);
    procedure SetICodeValue(Index : Integer ;Value : Double);
    procedure SetPCodeValue(Index : Integer ;Value : Double);
    procedure SetQCodeValue(Index : Integer ;Value : Double);

    function GetProcAddressX(hModule: HMODULE; lpProcName: LPCSTR) : FARPROC; stdcall;

    function    GetIDPR(Index : Integer) : Integer;
    procedure   SetIDPR(Index, aValue : Integer);
    function    GetFDPR(Index : Integer) : Double;
    procedure   SetFDPR(Index : Integer; aValue : Double);
    procedure   SetControlPanel(aOn : Boolean);
    procedure   SetJogPositive(Motor : Integer; aOn : Boolean);
    procedure   SetJogNegative(Motor : Integer; aOn : Boolean);
    procedure   SetJogReturn(Motor : Integer; aOn : Boolean);
    procedure   SetRun(CS : Integer; aOn : Boolean);
    procedure   SetStop(CS : Integer; aOn : Boolean);
    procedure   SetAbort(CS : Integer; aOn : Boolean);
    procedure   SetHome(CS : Integer; aOn : Boolean);
    procedure   SetHold(CS : Integer; aOn : Boolean);
    procedure   SetStep(CS : Integer; aOn : Boolean);
    function    GetJogPositive(Motor : Integer) : Boolean;
    function    GetJogNegative(Motor : Integer) : Boolean;
    function    GetJogReturn(Motor : Integer) : Boolean;
    function    GetRun(CS : Integer) : Boolean;
    function    GetStop(CS : Integer) : Boolean;
    function    GetAbort(CS : Integer) : Boolean;
    function    GetHome(CS : Integer) : Boolean;
    function    GetHold(CS : Integer) : Boolean;
    function    GetStep(CS : Integer) : Boolean;

    procedure   SetFOEnable(CS : Integer; aOn : Boolean);
    function    GetFOEnable(CS : Integer) : Boolean;
    procedure   SetFO(CS, Rate : Integer);
    function    GetFO(CS : Integer) : Integer;
    procedure   SetCPR(M_CS : Integer; aOn : Boolean);
    function    GetCPR(M_CS : Integer) : Boolean;
    function    GetRealTimeHostBusy : Boolean;
    procedure   SetRealTimeHostBusy(aOn : Boolean);
    function    GetRealTimePmacBusy : Boolean;

    function GetCommandedPosition(Motor : Integer) : Double;
    function GetActualPosition(Motor : Integer) : Double;
    function GetFollowError(Motor : Integer) : Double;
    function GetMotorVelocity(Motor : Integer) : Double;
    function GetDAC(Motor : Integer) : Integer;
    function GetTargetPosition(Motor : Integer) : Double;
    function GetCompensation(Motor : Integer) : Double;

    function GetBiasPosition(Motor : Integer) : Double;
    function GetMotorStatus1(Motor : Integer) : TPmacMotorStatii1;
    function GetMotorStatus2(Motor : Integer) : TPmacMotorStatii2;

    function GetRotaryRemaining(CS : Integer) : Integer;
    //function GetProgramRunning(CS : Integer) : Boolean;  // Dump ?
    //function GetCoordStatus2(CS : Integer) : TPmacCoordSystemStatii2; // Dump ??
    function GetCoordStatus1(CS : Integer) : TPmacCoordSystemStatii1; // Dump ??
    function    GetRomDate : string;
    function    GetRomVersion : string;
    function    GetPmacType : Integer;

    procedure   DoRequest(M_CS : Integer);

  public
    PulsesPerUnit : array [0..7] of Double;
    Scaling: array [0..7] of Integer;
    VelocityAxes : array[0..7] of Integer;
    VelocityAxesCount : Integer;
    AxesByName : array [0..8] of Integer;

    InputStart: Integer;
    InputLength: Integer;
    OutputStart: Integer;
    OutputLength: Integer;

    ServoRate: Double;

    constructor Create(Version: TPCommVersion = pvTurbo; StartLog: Boolean = False);
    destructor  Destroy; override;
    function    SendCommand(text : string) : integer;
    function    SendControl(cmd : byte) : boolean;
    function    Receive : string;
    function    Converse(Cmd : string) : string;
    function    ConverseFlat(Cmd : string) : string;
    function    ConverseNL(Cmd : string) : string;
    function    ControlConverse(cmd : char) : string;
    function    LinePending : boolean;
    function    GetVar(Vtype : Char;Number : short; def : Double):Double;
    procedure   SetVar(Vtype : Char;Number : short; Value : Double);
    procedure   DownLoadFile;
    function    DownLoadFileName(filename : string; aShow : Boolean) : boolean;
    function OpenPmacNumber(DeviceNumber : Integer): Boolean;
    function ClosePmacNumber : Boolean;
    procedure Log(Txt: string);

    property MCode[Index : Integer] : Double read GetMCodeValue write SetMCodeValue;
    property PCode[Index : Integer] : Double read GetPCodeValue write SetPCodeValue;
    property QCode[Index : Integer] : Double read GetQCodeValue write SetQCodeValue;
    property ICode[Index : Integer] : Double read GetICodeValue write SetICodeValue;
    property IDPR[Index : Integer] : Integer read GetIDPR write SetIDPR;
    property FDPR[Index : Integer] : Double read GetFDPR write SetFDPR;
    procedure GetDPRMem(Index, Count : Integer; Data : Pointer);
    procedure SetDPRMem(Index, Count : Integer; Data : Pointer);

    property ControlPanel : Boolean read FControlPanel write SetControlPanel;

    property JogPositive[Motor : Integer] : Boolean read GetJogPositive write SetJogPositive;
    property JogNegative[Motor : Integer]  : Boolean read GetJogNegative write SetJogNegative;
    property JogReturn[Motor : Integer]  : Boolean read GetJogReturn write SetJogReturn;
    property Run[CS : Integer]  : Boolean read GetRun write SetRun;
    property Stop[CS : Integer] : Boolean read GetStop write SetStop;
    property Abort[CS : Integer] : Boolean read GetAbort write SetAbort;
    property Home[CS : Integer] : Boolean read GetHome write SetHome;
    property Hold[CS : Integer] : Boolean read GetHold write SetHold;
    property Step[CS : Integer] : Boolean read GetStep write SetStep;


    property FOEnable[CS : Integer] : Boolean read GetFOEnable write SetFOEnable;
    property FeedrateOverride[CS : Integer] : Integer read GetFO write SetFO;

    property ControlPanelRequest[MCS : Integer] : Boolean read GetCPR write SetCPR;

    procedure SetPmacRealTimeBuffer(aPeriod : Cardinal; aOn : Boolean);
    procedure SetPmacRealTimeMotors(aMotors : Cardinal);
    property HostBusy : Boolean read GetRealTimeHostBusy write SetRealTimeHostBusy;
    property PmacBusy : Boolean read GetRealTimePmacBusy;

    property CommandedPosition[Motor : Integer] : Double read GetCommandedPosition;
    property ActualPosition[Motor : Integer] : Double read GetActualPosition;
    property FollowError[Motor : Integer] : Double read GetFollowError;
    property MotorVelocity[Motor : Integer] : Double read GetMotorVelocity;
    property DAC [Motor : Integer] : Integer read GetDAC;
    property TargetPosition[Motor : Integer] : Double read GetTargetPosition;
    property BIASPosition[Motor : Integer] : Double read GetBiasPosition;
    //property MoveTime[Motor : Integer] : Integer read GetMoveTime;
    property Compensation[Motor : Integer] : Double read GetCompensation;
    property RotaryRemaining[CS : Integer] : Integer read GetRotaryRemaining;
    //property ProgramRunning[CS : Integer] : Boolean read GetProgramRunning;

    //property CoordStatus2[CS : Integer] : TPmacCoordSystemStatii2 read GetCoordStatus2;
    property CoordStatus1[CS : Integer] : TPmacCoordSystemStatii1 read GetCoordStatus1;
    property MotorStatus1[Motor : Integer] : TPmacMotorStatii1 read GetMotorStatus1;
    property MotorStatus2[Motor : Integer] : TPmacMotorStatii2 read GetMotorStatus2;

    procedure SetPmacBackground(aOn : Boolean);
    procedure SetPmacBackgroundEx(aOn : Boolean; Rate: Word);
//    procedure VGBInit(aSize : Integer; aList : TList);

    function AsciiToRot(const aText : string; Buffer : Integer) : Byte;
    function RotBufInit(Buffer : Integer) : ShortInt;
    function RotBufRemove(Buffer : Integer) : Boolean;
    function RotBufChange(aSize, Buffer : Integer) : Integer;
    procedure RotBufClr(Buffer : Integer);
    function RotBuf(aOn : Boolean) : Boolean;

    function FireEventInit(hEvent : THandle; ulMask : Cardinal) : Boolean;
    function WndMsgInit(Handle : HWND; Msg : Integer; ulMask : Cardinal) : Boolean;
    function CoordVelocity(aCoord, Motors : Integer) : Double; // uses velocity axes defined above.

    procedure Pmatch(Motor : Integer);

    property ROMData : string read GetROMDate;
    property ROMVersion : string read GetROMVersion;
    property PmacType : Integer read GetPmacType;

    function DPRWriteBuffer(NumEntries : Integer; Data : PAVBGWFormat) : Integer;

    procedure ReadFixedBuffers(Coords : Integer);
    procedure WriteFixedBuffers;

    function SActualPosition(Axis : Integer) : Double;
    function SMasterPosition(Axis : Integer) : Double;
    function SCommandedPosition(Axis : Integer) : Double;
    function SPositionBias(Axis : Integer) : Double;
    function SCompensation(Axis : Integer) : Double;
    function STargetPosition(Axis : Integer) : Double;
    function SAxisVelocity(Axis: Integer) : Double;
    function SMotorTargetPosition(Axis : Integer) : Double;
    function SProgramLinesRemaining : Integer;
    function SFollowError(Axis: Integer): Double;
    function STimeRemainingInMove(CS: Integer): Double;
    function SCoordVelocity: Double;
    function SGetMoveTime(Motor : Integer) : Integer;

    function GetInputBase: Pointer;
    function GetOutputBase: Pointer;
  end;

  EPcommError = class(Exception);

function GetIEEE32BitFloat(X: Integer): Double;
function SetIEEE32BitFloat(X: Double): Cardinal;
function Pmac64BitDouble(Y, X: Integer): Double;

var
  ResponseBox : TTextDisplay;

implementation

uses ComCtrls, Math;

resourcestring
  CommunicationsFailureLang = 'Pcomm communications error';
  PcommDeviceNotOpen = 'Pcomm32 Device not open';

procedure PmacMessage(msg : PChar; NewLine : boolean); stdcall;
begin
  ResponseBox.AddText(StrPas(msg));
end;

procedure PmacProgress(aPercent : Integer); stdcall;
begin
  ResponseBox.Progress := aPercent;
end;

const
  WM_PMAC_NOTIFY = WM_USER + 100;
  PCommLibrary = 'Pmac.dll';
  TurboPCommLibrary = 'PComm32.dll';

function TPcomm32.GetProcAddressX(hModule: HMODULE; lpProcName: LPCSTR): FARPROC;
begin
  Result := GetProcAddress(hModule, lpProcName);
  if not Assigned(Result) then
    raise Exception.CreateFmt(PmacLibraryFunction, [lpProcName]);
end;

constructor TPcomm32.Create(Version: TPCommVersion = pvTurbo; StartLog: Boolean = False);
var Stream : TFileStream;
begin
  inherited Create;
  Self.Version:= Version;

  PulsesPerUnit[0]:= 1;
  PulsesPerUnit[1]:= 1;
  PulsesPerUnit[2]:= 1;
  PulsesPerUnit[3]:= 1;
  PulsesPerUnit[4]:= 1;
  PulsesPerUnit[5]:= 1;
  PulsesPerUnit[6]:= 1;
  PulsesPerUnit[7]:= 1;

  ServoRate:= 2250;

  if(Version = pvPmac) then
    LibCode := LoadLibrary(PcommLibrary)
  else
    LibCode := LoadLibrary(TurboPcommLibrary);

  FDeviceNumber := -1;
  if LibCode = 0 then
    raise Exception.Create(PmaclibraryNotFound);

  try
    OpenPmacDevice       := GetProcAddressX(LibCode, 'OpenPmacDevice');
    ClosePmacDevice      := GetProcAddressX(LibCode, 'ClosePmacDevice');
    PmacFlush            := GetProcAddressX(LibCode, 'PmacFlush');
    GetResponse          := GetProcAddressX(LibCode, 'PmacGetResponseA');
    GetControlResponse   := GetProcAddressX(LibCode, 'PmacGetControlResponseA');
    GetLine              := GetProcAddressX(LibCode, 'PmacGetLineA');
    IsLineWaiting        := GetProcAddressX(LibCode, 'PmacReadReady');
    SendChar             := GetProcAddressX(LibCode, 'PmacSendCharA');
    SendLine             := GetProcAddressX(LibCode, 'PmacSendLineA');
    SendCmnd             := GetProcAddressX(LibCode, 'PmacSendCommandA');
    GetVariable          := GetProcAddressX(LibCode, 'PmacGetVariableDouble');
    SetVariable          := GetProcAddressX(LibCode, 'PmacSetVariableDouble');
    DownLoad             := GetProcAddressX(LibCode, 'PmacDownloadA');
    DPRGetDWord          := GetProcAddressX(LibCode, 'PmacDPRGetDWord');
    DPRSetDWord          := GetProcAddressX(LibCode, 'PmacDPRSetDWord');
    DPRGetFloat          := GetProcAddressX(LibCode, 'PmacDPRGetFloat');
    DPRSetFloat          := GetProcAddressX(LibCode, 'PmacDPRSetFloat');
    DPRGetMem            := GetProcAddressX(LibCode, 'PmacDPRGetMem');
    DPRSetMem            := GetProcAddressX(LibCode, 'PmacDPRSetMem');

    { Control panel functions }
    DPRControlPanel      := GetProcAddressX(LibCode, 'PmacDPRControlPanel');

    DPRSetJogPositiveBit := GetProcAddressX(LibCode, 'PmacDPRSetJogPosBit');
    DPRSetJogNegativeBit := GetProcAddressX(LibCode, 'PmacDPRSetJogNegBit');
    DPRSetJogReturnBit   := GetProcAddressX(LibCode, 'PmacDPRSetJogReturnBit');
    DPRSetJogReturn      := GetProcAddressX(LibCode, 'PmacDPRSetJogReturn');
    DPRSetRun            := GetProcAddressX(LibCode, 'PmacDPRSetRunBit');
    DPRSetStop           := GetProcAddressX(LibCode, 'PmacDPRSetStopBit');
    DPRSetHome           := GetProcAddressX(LibCode, 'PmacDPRSetHomeBit');
    DPRSetHold           := GetProcAddressX(LibCode, 'PmacDPRSetHoldBit');
    DPRSetStep           := GetProcAddressX(LibCode, 'PmacDPRSetStepBit');

    DPRSetJogReturn      := GetProcAddressX(LibCode, 'PmacDPRSetJogReturn');

    DPRGetJogPositive    := GetProcAddressX(LibCode, 'PmacDPRGetJogPosBit');
    DPRGetJogNegative    := GetProcAddressX(LibCode, 'PmacDPRGetJogNegBit');
    DPRGetJogReturn      := GetProcAddressX(LibCode, 'PmacDPRGetJogReturnBit');
    DPRGetRun            := GetProcAddressX(LibCode, 'PmacDPRGetRunBit');
    DPRGetStop           := GetProcAddressX(LibCode, 'PmacDPRGetStopBit');
    DPRGetHome           := GetProcAddressX(LibCode, 'PmacDPRGetHomeBit');
    DPRGetHold           := GetProcAddressX(LibCode, 'PmacDPRGetHoldBit');
    DPRGetStep           := GetProcAddressX(LibCode, 'PmacDPRGetStepBit');

    DPRGetFOEnable       := GetProcAddressX(LibCode, 'PmacDPRGetFOEnableBit');
    DPRSetFOEnable       := GetProcAddressX(LibCode, 'PmacDPRSetFOEnableBit');

    DPRGetFOValue        := GetProcAddressX(LibCode, 'PmacDPRGetFOValue');
    DPRSetFOValue        := GetProcAddressX(LibCode, 'PmacDPRSetFOValue');

    DPRGetRequestBit     := GetProcAddressX(LibCode, 'PmacDPRGetRequestBit');
    DPRSetRequestBit     := GetProcAddressX(LibCode, 'PmacDPRSetRequestBit');

    { Real-Time Fixed Data Buffer functions }
    DPRRealTime          := GetProcAddressX(LibCode, 'PmacDPRRealTime');
    DPRSetMotors         := GetProcAddressX(LibCode, 'PmacDPRSetMotors');

    if(Version <> pvTurbo) then begin
      DPRSetHostBusy       := GetProcAddressX(LibCode, 'PmacDPRSetHostBusyBit');
      DPRGetHostBusy       := GetProcAddressX(LibCode, 'PmacDPRGetHostBusyBit');
      DPRGetPmacBusy       := GetProcAddressX(LibCode, 'PmacDPRGetPmacBusyBit');
    end;


    PmacDPRGetCommandedPos := GetProcAddressX(LibCode, 'PmacDPRGetCommandedPos');
    PmacDPRPosition        := GetProcAddressX(LibCode, 'PmacDPRPosition');
    PmacDPRFollowError     := GetProcAddressX(LibCode, 'PmacDPRFollowError');
    PmacDPRGetVel         := GetProcAddressX(LibCode, 'PmacDPRGetVel');
//    PmacDPRGetMasterPos   := GetProcAddressX(LibCode, 'PmacDPRGetMasterPos');
    PmacDPRGetMoveTime    := GetProcAddressX(LibCode, 'PmacDPRGetMoveTime');
    PmacDPRGetPrevDAC     := GetProcAddressX(LibCode, 'PmacDPRGetPrevDAC');
    PmacDPRGetTargetPos := GetProcAddressX(LibCode, 'PmacDPRGetTargetPos');
    PmacDPRVectorVelocity := GetProcAddressX(Libcode, 'PmacDPRVectorVelocity');
    PmacDPRGetBIASPos := GetProcAddressX(LibCode, 'PmacDPRGetBiasPos');
    PmacDPRMotorServoStatus := GetProcAddressX(LibCode, 'PmacDPRMotorServoStatus');
    PmacDRPGetCompensationPos := GetProcAddressX(LibCode, 'PmacDPRGetCompensationPos');

    { Background Fixed Data Buffer functions }
    PmacDPRBackground     := GetProcAddressX(LibCode, 'PmacDPRBackground');
    PmacDPRBackgroundEx:= GetProcAddressX(LibCode, 'PmacDPRBackgroundEx');
    PmacDPRPe         := GetProcAddressX(LibCode, 'PmacDPRPe');
    PmacDPRProgRemaining := GetProcAddressX(LibCode, 'PmacDPRProgRemaining');
    PmacDPRTimeRemInMove := GetProcAddressX(LibCode, 'PmacDPRTimeRemInMove');
    PmacDPRProgRunning   := GetProcAddressX(LibCode, 'PmacDPRProgRunning');

    { Binary Rotary-buffer functions }
    PmacDPRAsciiStrToRotA:= GetProcAddressX(LibCode, 'PmacDPRAsciiStrToRotA');
    PmacDPRRotBufInit    := GetProcAddressX(LibCode, 'PmacDPRRotBufInit');
    PmacDPRRotBuf    := GetProcAddressX(LibCode, 'PmacDPRRotBuf');
    PmacDPRRotBufRemove      := GetProcAddressX(LibCode, 'PmacDPRRotBufRemove');
    PmacDPRRotBufChange      := GetProcAddressX(LibCode, 'PmacDPRRotBufChange');
    PmacDPRRotBufClr         := GetProcAddressX(LibCode, 'PmacDPRRotBufClr');
    //DPRRotBuf := GetProcAddressX(LibCode, 'PmacDPRRotBufOpen');
    DPRRotBuf            := GetProcAddressX(LibCode, 'PmacDPRRotBuf');

    { Interrupt routines }
    INTRFireEventInit    := GetProcAddressX(LibCode, 'PmacINTRFireEventInit');
    INTRWndMsgInit       := GetProcAddressX(LibCode, 'PmacINTRWndMsgInit');

    { Gather functions }

    { Utility functions }
    PmacGetRomDate       := GetProcAddressX(LibCode, 'PmacGetRomDateA');
    PmacGetRomVersion    := GetProcAddressX(LibCode, 'PmacGetRomVersionA');
    PmacGetType          := GetProcAddressX(LibCode, 'PmacGetPmacType');

    { DPR Variable background Read/Write Data Buffer }
//    @PmacDPRBackground         := GetProcAddressX(LibCode, 'PmacDPRBackGround');
    PmacDPRBufLast       := GetProcAddressX(LibCode, 'PmacDPRBufLast');
    PmacDPRGetVBGAddress := GetProcAddressX(LibCode, 'PmacDPRGetVBGAddress');
    PmacDPRGetVBGNumEntries := GetProcAddressX(LibCode, 'PmacDPRGetVBGNumEntries');
    PmacDPRGetVBGDataOffset := GetProcAddressX(LibCode, 'PmacDPRGetVBGDataOffset');
    PmacDPRGetVBGAddrOffset := GetProcAddressX(LibCode, 'PmacDPRGetVBGAddrOffset');
    PmacDPRGetVBGServoTimer := GetProcAddressX(LibCode, 'PmacDPRGetVBGServoTimer');
    PmacDPRGetVBGStartAddr  := GetProcAddressX(LibCode, 'PmacDPRGetVBGStartAddr');
    PmacDPRGetVBGTotalEntries := GetProcAddressX(LibCode, 'PmacDPRGetVBGTotalEntries');
    PmacDPRVarBufChange := GetProcAddressX(LibCode, 'PmacDPRVarBufChange');
    PmacDPRVarBufInit := GetProcAddressX(LibCode, 'PmacDPRVarBufInit');
    PmacDPRVarBufRead := GetProcAddressX(LibCode, 'PmacDPRVarBufRead');
    PmacDPRVarBufRemove := GetProcAddressX(LibCode, 'PmacDPRVarBufRemove');
    PmacDPRWriteBuffer := GetProcAddressX(LibCode, 'PmacDPRWriteBuffer');
  except
    FreeLibrary(LibCode);
    LibCode := 0;
    FDeviceNumber := -1;
    raise;
  end;

  New(ServoFixedData);
  New(CoordinateFixedData);

  FDeviceNumber := -3;
  if StartLog then begin
    Stream := TFileStream.Create(TemporaryPath + 'Pcomm.Txt', fmCreate);
    Stream.Free;
  end;

  //ResetControlPanel;
end;

{ Ensure Pcomm is closed prior to closing library and destroying component }
destructor  TPcomm32.Destroy;
begin
  if Assigned(ServoFixedData) then
    Dispose(ServoFixedData);

  if Assigned(CoordinateFixedData) then
    Dispose(CoordinateFixedData);

  if FDeviceNumber > -1 then
    ClosePmacNumber;

  if LibCode <> 0 then
    FreeLibrary(LibCode);

  inherited Destroy;
end;


{ Sends a command to the PMAC, returns the number of characters downloaded }
function TPcomm32.SendCommand(text : string) : integer;
var  tx : array [0..255] of char;
begin
  Result := 0;
  if FDeviceNumber < 0  then
    Exit;
  StrPLcopy(tx, text, 255);
  SendCmnd(FDeviceNumber, tx);
  Result := 1;
end;

{ Recieves the last current string from pcomm }
function TPcomm32.Receive : string;
var  rx : array [0..255] of char;
begin
  Result := '';
  if FDeviceNumber < 0 then exit;
  rx[0] := #0;
  GetLine(FDeviceNumber, rx, 255);
  result := StrPas(rx);
//  PmacFlush(FDeviceNumber);
end;

function TPcomm32.SendControl(cmd : Byte) : boolean;
begin
  result := false;
  if FDeviceNumber < 0 then exit;
  result := SendChar(FDeviceNumber, cmd);
end;

{ returns a string resulting from the string command }
function TPcomm32.Converse(cmd : string) : string;
var  tx : array [0..4095] of char;
     rx : array [0..4095] of char;
begin
  Result := '';
  if FDeviceNumber < 0 then exit;

  strPLcopy(tx, cmd, 4095);
  GetResponse(FDeviceNumber, rx, 4095, Tx);
  Result := RX;
  if Length(Result) <> 0 then
    Log(TX + CarRet + Result)
  else
    Log(TX);

end;

procedure TPcomm32.Log(Txt: string);
var Stream : TFileStream;
begin
  try
    Stream := TFileStream.Create(TemporaryPath + 'Pcomm.Txt', fmOpenReadWrite);
    try
      Stream.Seek(0, soFromEnd);
      Txt:= Txt + CarRet;
      Stream.Write(PChar(Txt)^, Length(Txt));
    finally
      Stream.Free;
    end;
  except
  end;
end;

{ returns a string resulting from the string command }
function TPcomm32.ConverseNL(cmd : string) : string;
var  tx : array [0..4095] of char;
     rx : array [0..4095] of char;
begin
  Result := '';
  if FDeviceNumber < 0 then
    Exit;

  strPLcopy(tx, cmd, 4095);
  GetResponse(FDeviceNumber, rx, 4095, tx);
  Result := StrPas(rx);
end;

//
// Remove control characters (CR, LF etc) from the end of the response
//
function TPcomm32.ConverseFlat(Cmd : string) : string;
var I : Integer;
begin
  Result := Converse(Cmd);
  for I := Length(Result) downto 1 do begin
    case Result[I] of
      '0'..'9',
      '$',
      'A'..'Z',
      'a'..'z' : begin
        Result := Copy(Result, 1, I);
        Exit;
      end;
    end;
  end;
end;

{ Returns a string resulting from the single character command }
function TPcomm32.ControlConverse(cmd : char) : string;
var  rx : array [0..1023] of char;
begin
  Result := '';
  if FDeviceNumber <0 then
    Exit;

  GetControlResponse(FDeviceNumber, rx, 1023, cmd);
  Result := StrPas(rx);
end;

{ An encapsulation of isLineWaiting, it returns true if a line is pending from
  the pcomm.  Renamed for clarity </p><p>
  if isLinePending then does not scan <br>
  If LinePending then does. <br>
}
function TPcomm32.LinePending : boolean;
begin
  result := false;
  if FDeviceNumber < 0 then exit;

  result := isLineWaiting(FDeviceNumber)
end;


//METHODS
function TPcomm32.OpenPmacNumber(DeviceNumber : Integer): Boolean;
begin
  Result :=  OpenPMACDevice(DeviceNumber);
  if Result = True then
    FDeviceNumber := DeviceNumber;
end;

function TPcomm32.ClosePmacNumber : Boolean;
begin
  Result := True;
  if FDeviceNumber <> -1 then begin
    Result :=  ClosePMACDevice(FDeviceNumber);
    FDeviceNumber := -1;
  end;
end;


// This is implementation  of general PMacGetVaraiableDouble() in DLL.
function TPcomm32.GetVar(Vtype : Char; Number : short; def : Double):Double;
begin
  Result := 0;
  if FDeviceNumber <0 then exit;
  Result := GetVariable(FDeviceNumber,Vtype,Number,def);
end;


// This is implementation  of general PMacSetVaraiableDouble() in DLL.
procedure TPcomm32.SetVar(Vtype : Char;Number : short; Value : Double);
begin
  if FDeviceNumber < 0 then exit;
  SetVariable(FDeviceNumber,VType,Number,Value);
end;

const UnlikelyNumber = -1.010101;

//This is derivative of PMacSetVaraiableDouble() in DLL.
// As it is specified as M code read the sender does not have to specify
// Variable Type as an input parameter;
function TPcomm32.GetMCodeValue(Index : Integer): Double;
begin
  if FDeviceNumber < 0  then
    raise EPcommError.Create(PcommDeviceNotOpen);

  Result := GetVariable(FDeviceNumber, 'M', Index, UnlikelyNumber);
end;

//This is derivative of PMacSetVaraiableDouble() in DLL.
// As it is specified as I code read the sender does not have to specify
// Variable Type as an input parameter;
function TPcomm32.GetICodeValue(Index : Integer): Double;
begin
  if FDeviceNumber < 0  then
    raise EPcommError.Create(PcommDeviceNotOpen);
  Result := GetVariable(FDeviceNumber, 'I', Index, UnlikelyNumber);
end;

//This is derivative of PMacSetVaraiableDouble() in DLL.
// As it is specified as P code read the sender does not have to specify
// Variable Type as an input parameter;
function TPcomm32.GetPCodeValue(Index : Integer): Double;
begin
  if FDeviceNumber < 0  then
    raise EPcommError.Create(PcommDeviceNotOpen);
  Result := GetVariable(FDeviceNumber, 'P', Index, UnlikelyNumber);
end;

//This is derivative of PMacSetVaraiableDouble() in DLL.
// As it is specified as Q code read the sender does not have to specify
// Variable Type as an input parameter;
function TPcomm32.GetQCodeValue(Index : Integer): Double;
begin
  if FDeviceNumber < 0  then
    raise EPcommError.Create(PcommDeviceNotOpen);
  Result := GetVariable(FDeviceNumber, 'Q', Index, UnlikelyNumber);
end;


Procedure TPcomm32.SetMCodeValue(Index : Integer; Value : Double);
begin
  if FDeviceNumber < 0  then
    raise EPcommError.Create(PcommDeviceNotOpen);
  SetVar('M', Index, Value);
end;

Procedure TPcomm32.SetICodeValue(Index : Integer; Value : Double);
begin
  if FDeviceNumber < 0  then
    raise EPcommError.Create(PcommDeviceNotOpen);
  SetVar('I', Index, Value);
end;

Procedure TPcomm32.SetPCodeValue(Index : Integer; Value : Double);
begin
  if FDeviceNumber < 0  then
    raise EPcommError.Create(PcommDeviceNotOpen);
  SetVar('P', Index, Value);
end;

Procedure TPcomm32.SetQCodeValue(Index : Integer; Value : Double);
begin
  if FDeviceNumber < 0  then
    raise EPcommError.Create(PcommDeviceNotOpen);
  SetVar('Q', Index, Value);
end;

procedure TPcomm32.DownLoadFile;
var
    OpenFile   : TOpenDialog;
begin
  OpenFile   := TOpenDialog.Create(Application);
  OpenFile.InitialDir := MachSpecPath;
  with OpenFile do begin
    if Execute then
      DownLoadFileName(FileName, True);
    Free;
  end;
end;

function TPcomm32.DownLoadFileName(Filename : string; aShow : Boolean) : boolean;
begin
  ResponseBox.Clear;

  if aShow then
    ResponseBox.Show;
  Result := DownLoad(FDeviceNumber, PmacMessage, nil, PmacProgress, PChar(FileName),
            True, False, False, True);
  if aShow then begin
    ResponseBox.Close;
    ResponseBox.ShowModal;
  end;
end;

function    TPcomm32.GetIDPR(Index : Integer) : Integer;
begin
  if (Version = pvTurbo) and (Index >= InputStart) and (Index < (InputStart + InputLength)) then
    Result:= InDPR[(Index - InputStart) div 4]
  else if (Version = pvTurbo) and (Index >= OutputStart) and (Index < (OutputStart + OutputLength)) then
    Result:= OutDPR[(Index - OutputStart) div 4]
  else
    Result := DPRGetDWord(FDeviceNumber, Index);
end;

procedure TPComm32.SetIDPR(Index, aValue : Integer);
begin
  if (Version = pvTurbo) and (Index >= OutputStart) and (Index < (OutputStart + OutputLength)) then
    OutDPR[(Index - OutputStart) div 4]:= aValue
  else if (Version = pvTurbo) and (Index >= InputStart) and (Index < (InputStart + InputLength)) then
    raise Exception.Create('Writing to Input area')
  else begin
    //Self.Log('Writing Int ' + FloatToStr(AValue) + ' to ' + IntToStr(Index));
    DPRSetDWord(FDeviceNumber, Index, aValue);
  end;
end;

function    TPComm32.GetFDPR(Index : Integer) : Double;
begin
  if (Version = pvTurbo) and (Index >= InputStart) and (Index < (InputStart + InputLength)) then
    Result:= GetIEEE32BitFloat(InDPR[(Index - InputStart) div 4])
  else if (Version = pvTurbo) and (Index >= OutputStart) and (Index < (OutputStart + OutputLength)) then
    Result:= GetIEEE32BitFloat(OutDPR[(Index - OutputStart) div 4])
  else
    Result := DPRGetFloat(FDeviceNumber, Index);
end;

procedure   TPComm32.SetFDPR(Index : Integer; aValue : Double);
begin
  if (Version = pvTurbo) and (Index >= OutputStart) and (Index < (OutputStart + OutputLength)) then
    OutDPR[(Index - OutputStart) div 4]:= SetIEEE32BitFloat(aValue)
  else if (Version = pvTurbo) and (Index >= InputStart) and (Index < (InputStart + InputLength)) then
    raise Exception.Create('Writing to Input area')
  else begin
    Self.Log('Writing Float ' + FloatToStr(AValue) + ' to ' + IntToStr(Index));
    DPRSetFloat(FDeviceNumber, Index, aValue);
  end
end;

procedure TPcomm32.GetDPRMem(Index, Count : Integer; Data : Pointer);
begin
  DPRGetMem(FDeviceNumber, Index, Count, Data);
end;

procedure TPcomm32.SetDPRMem(Index, Count : Integer; Data : Pointer);
begin
  DPRSetMem(FDeviceNumber, Index, Count, Data);
end;

procedure   TPcomm32.SetControlPanel(aOn : Boolean);
begin
  FControlPanel := Boolean(DPRControlPanel(FDeviceNumber, Ord(aOn)));
end;

procedure   TPcomm32.SetJogPositive(Motor : Integer; aOn : Boolean);
var Command: string;
    MotorS: Char;
begin
  if(Version <> pvTurbo) then begin
    DPRSetJogNegativeBit(FDeviceNumber, Motor + 1, Ord(not aOn));
    DoRequest(Motor);
  end else begin
    MotorS := Char(Motor + Ord('1'));
    if AOn then
      Command:= '#' + MotorS + 'J+'
    else
      Command:= '#' + MotorS + 'J/';
    Converse(Command);
  end;
end;

procedure   TPcomm32.SetJogNegative(Motor : Integer; aOn : Boolean);
var Command: string;
    MotorS: Char;
begin
  if(Version <> pvTurbo) then begin
    DPRSetJogPositiveBit(FDeviceNumber, Motor + 1, Ord(not aOn));
    DoRequest(Motor);
  end else begin
    MotorS := Char(Motor + Ord('1'));
    if AOn then
      Command:= '#' + MotorS + 'J-'
    else
      Command:= '#' + MotorS + 'J/';
    Converse(Command);
  end;
end;

procedure   TPcomm32.SetJogReturn(Motor : Integer; aOn : Boolean);
begin
  if(Version <> pvTurbo) then begin
    DPRSetJogReturnBit(FDeviceNumber, Motor + 1, Ord(aOn));
    DoRequest(Motor);
  end;
end;

procedure TPcomm32.Pmatch(Motor : Integer);
begin
  Self.Log('****** Pmatch: ');
  if(Version <> pvTurbo) then begin
    DPRSetJogReturn(FDeviceNumber, Motor {+ 1});
  //  DoRequest(Motor);
    Sleep(1);
  end;
end;

procedure   TPcomm32.SetRun(CS : Integer; aOn : Boolean);
var Command: string;
    CSS: Char;
begin
  if(Version <> pvTurbo) then begin
    DPRSetRun(FDeviceNumber, CS + 1, Ord(aOn));
    DoRequest(CS);
  end else begin
    CSS := Char(CS + Ord('1'));
    if AOn then begin
      Command:= '&' + CSS + 'B0R';
      Converse(Command);
    end;
  end;
end;

procedure   TPcomm32.SetStop(CS : Integer; aOn : Boolean);
var Command: string;
    CSS: Char;
begin
  if AOn then begin
    CSS := Char(CS + Ord('1'));
    Command:= '&' + CSS + 'H';
    Converse(Command);
  end;
end;

procedure TPcomm32.SetAbort(CS: Integer; aOn: Boolean);
begin
  Self.Converse('A');
end;

procedure   TPcomm32.SetHome(CS : Integer; aOn : Boolean);
begin
  if(Version <> pvTurbo) then begin
    DPRSetHome(FDeviceNumber, CS + 1, Ord(aOn));
    DoRequest(CS);
  end;
end;

procedure   TPcomm32.SetHold(CS : Integer; aOn : Boolean);
var Command: string;
    CSS: Char;
begin
  if(Version <> pvTurbo) then begin
    DPRSetHold(FDeviceNumber, CS + 1, Ord(aOn));
    DoRequest(CS);
    DPRSetHold(FDeviceNumber, CS + 1, Ord(aOn));
  end else begin
    CSS := Char(CS + Ord('1'));
    if AOn then  begin
      Command:= '&' + CSS + 'H';
      Converse(Command);
    end;
  end;
end;

procedure   TPcomm32.SetStep(CS : Integer; aOn : Boolean);
var Command: string;
    CSS: Char;
begin
  if(Version <> pvTurbo) then begin
    DPRSetStep(FDeviceNumber, CS + 1, Ord(aOn));
    DoRequest(CS);
  end else begin
    CSS := Char(CS + Ord('1'));
    if AOn then begin
      Command:= '&' + CSS + 'S';
      Converse(Command);
    end;
  end;
end;

function    TPcomm32.GetJogPositive(Motor : Integer) : Boolean;
begin
  Result := DPRGetJogPositive(FDeviceNumber, Motor + 1) <> 0;
end;

function    TPcomm32.GetJogNegative(Motor : Integer) : Boolean;
begin
  Result := DPRGetJogNegative(FDeviceNumber, Motor + 1) <> 0;
end;

function    TPcomm32.GetJogReturn(Motor : Integer) : Boolean;
begin
  Result := DPRGetJogReturn(FDeviceNumber, Motor + 1) <> 0;
end;

function    TPcomm32.GetRun(CS : Integer) : Boolean;
begin
  Result := DPRGetRun(FDeviceNumber, CS + 1) <> 0;
end;

function    TPcomm32.GetStop(CS : Integer) : Boolean;
begin
  Result := False;
end;

function TPcomm32.GetAbort(CS: Integer): Boolean;
begin
  Result:= False;
end;

function    TPcomm32.GetHome(CS : Integer) : Boolean;
begin
  Result := False;
end;

function    TPcomm32.GetHold(CS : Integer) : Boolean;
begin
  Result := DPRGetHold(FDeviceNumber, CS + 1) <> 0;
end;

function    TPcomm32.GetStep(CS : Integer) : Boolean;
begin
  Result := DPRGetStep(FDeviceNumber, CS + 1) <> 0;
end;

procedure   TPcomm32.SetFOEnable(CS : Integer; aOn : Boolean);
begin
  DPRSetFOEnable(FDeviceNumber, CS + 1, Ord(aOn));
end;

function    TPcomm32.GetFOEnable(CS : Integer) : Boolean;
begin
  Result := DPRGetFOEnable(FDeviceNumber, CS + 1) <> 0;
end;

procedure   TPcomm32.SetFO(CS, Rate : Integer);
begin
  DPRSetFOValue(FDeviceNumber, CS + 1, Rate);
end;

function    TPcomm32.GetFO(CS : Integer) : Integer;
begin
  Result := DPRGetFOValue(FDeviceNumber, CS + 1);
end;

procedure   TPcomm32.SetCPR(M_CS : Integer; aOn : Boolean);
begin
  Self.Log('****** Set CPR: ');
  DPRSetRequestBit(FDeviceNumber, M_CS + 1, Ord(aOn));
end;

function    TPcomm32.GetCPR(M_CS : Integer) : Boolean;
begin
  Result := DPRGetRequestBit(FDeviceNumber, M_CS + 1) <> 0;
end;

procedure TPcomm32.SetPmacRealTimeBuffer(aPeriod : Cardinal; aOn : Boolean);
begin
  FRealTime := DPRRealTime(FDeviceNumber, aPeriod, Ord(aOn)) <> 0;
end;

procedure TPcomm32.SetPmacRealTimeMotors(aMotors : Cardinal);
begin
  //if FDeviceNumber < 0  then
  //  raise EPcommError.Create(PcommDeviceNotOpen);
  DPRSetMotors(FDeviceNumber, aMotors);
end;

function    TPcomm32.GetRealTimeHostBusy : Boolean;
begin
  if Assigned(DPRGetHostBusy) then
    Result := DPRGetHostBusy(FDeviceNumber) <> 0
  else
    Result:= False;
end;

procedure   TPcomm32.SetRealTimeHostBusy(aOn : Boolean);
begin
  if Assigned(DPRSetHostBusy) then
    DPRSetHostBusy(FDeviceNumber, Ord(aOn));
end;

function    TPcomm32.GetRealTimePmacBusy : Boolean;
begin
  if Assigned(DPRGetPmacBusy) then
    Result := DPRGetPmacBusy(FDeviceNumber) <> 0
  else
    Result:= False;
end;

procedure TPcomm32.SetPmacBackground(aOn : Boolean);
begin
  FBackground := PmacDPRBackground(FDeviceNumber, Ord(aOn)) <> 0;
end;

procedure TPcomm32.SetPmacBackgroundEx(aOn : Boolean; Rate: Word);
begin
  PmacDPRBackgroundEx(FDeviceNumber, Ord(aOn), Rate, 1);
  FBackground:= aOn;
end;


function TPcomm32.AsciiToRot(const aText : string; Buffer : Integer) : Byte;
begin
  Result := PmacDPRAsciiStrToRotA(FDeviceNumber, PChar(aText), Buffer);
  //Log('AsciiToRot: ' + aText);
  if Result <> 0 then
    Log('AsciiToRot: ' + IntToStr(Result) + ' ' + aText);
end;

function TPcomm32.RotBufInit(Buffer : Integer) : ShortInt;
var I: Integer;
begin
  I := PmacDPRRotBufInit(FDeviceNumber, Buffer);
  if I <> 0 then
    Self.Log('PmacDPRRotBufInit: ' + IntToStr(I));

  Result:= I;
end;

function TPcomm32.RotBufRemove(Buffer : Integer) : Boolean;
var I: Integer;
begin
  I := PmacDPRRotBufRemove(FDeviceNumber, Buffer);
  if I <> 0 then
    Self.Log('PmacDPRRotBufRemove: ' + IntToStr(I));

  Result:= I <> 0;
end;

function TPcomm32.RotBufChange(aSize, Buffer : Integer) : Integer;
var I: Integer;
begin
  I := PmacDPRRotBufChange(FDeviceNumber, Buffer, aSize);
  if I <> 0 then
    Self.Log('PmacDPRRotBufChange: ' + IntToStr(I));

  if aSize > 0 then
    Result:= PmacDPRRotBuf(FDeviceNumber, 1)
  else
    Result:= PmacDPRRotBuf(FDeviceNumber, 0) ;
end;

procedure TPcomm32.RotBufClr(Buffer : Integer);
begin
  PmacDPRRotBufClr(FDeviceNumber, Buffer);
end;

function TPcomm32.RotBuf(aOn : Boolean) : Boolean;
var I: Integer;
begin
  I := DPRRotBuf(FDeviceNumber, Ord(aOn));
  if I <> 0 then
    Self.Log('PmacDPRRotBuf: ' + IntToStr(I));

  Result:= I <> 0;
end;

function TPcomm32.FireEventInit(hEvent : THandle; ulMask : Cardinal) : Boolean;
begin
  Result := INTRFireEventInit(FDeviceNumber, hEvent, ulMask) <> 0;
end;

function TPcomm32.WndMsgInit(Handle : HWND; Msg : Integer; ulMask : Cardinal) : Boolean;
begin
  Result := INTRWndMsgInit(FDeviceNumber, Handle, Msg, ulMask) <> 0;
end;


function    TPcomm32.GetRomDate : string;
var A : array [0..1023] of Char;
begin
  PmacGetROMDate(FDeviceNumber, A, 1023);
  Result := A;
end;

function    TPcomm32.GetRomVersion : string;
var A : array [0..1023] of Char;
begin
  PmacGetROMVersion(FDeviceNumber, A, 1023);
  Result := A;
end;

function    TPcomm32.GetPmacType : Integer;
begin
  Result := PmacGetType(FDeviceNumber);
end;


procedure TPcomm32.DoRequest(M_CS : Integer);
begin
  if ControlPanelRequest[M_CS] then begin
    Sleep(10);
    if ControlPanelRequest[M_CS] then
      raise EPcommError.Create(CommunicationsFailureLang);
  end;

  ControlPanelRequest[M_CS] := True;
end;

function TPcomm32.GetCommandedPosition(Motor : Integer) : Double;
begin
  Result := PmacDPRGetCommandedPos(FDeviceNumber, Motor, PulsesPerUnit[Motor]);
end;

function TPcomm32.GetActualPosition(Motor : Integer) : Double;
begin
  Result := PmacDPRPosition(FDeviceNumber, Motor, PulsesPerUnit[Motor]);
end;

function TPcomm32.GetFollowError(Motor : Integer) : Double;
begin
  Result := PmacDPRFollowError(FDeviceNumber, Motor, PulsesPerUnit[Motor]);
end;

function TPcomm32.GetMotorVelocity(Motor : Integer) : Double;
begin
  Result := PmacDPRGetVel(FDeviceNumber, Motor, PulsesPerUnit[Motor]);
end;

function TPcomm32.CoordVelocity(aCoord, Motors : Integer) : Double; // uses velocity axes defined above.
begin
  if Version = pvTurbo then begin
    //Coorddata ProgrammedFeedRate: I64Union; // $69c units / msec      /
    //FeedrateOverride: Cardinal; // $6a4 units of I10              /
  end;
end;

function TPcomm32.GetDAC(Motor : Integer) : Integer;
begin
  Result := PmacDPRGetPrevDAC(FDeviceNumber, Motor);
end;

function TPcomm32.GetTargetPosition(Motor : Integer) : Double;
begin
  Result := PmacDPRGetTargetPos(FDeviceNumber, Motor, PulsesPerUnit[Motor]);
end;

function TPcomm32.GetCompensation(Motor : Integer) : Double;
begin
  PmacDRPGetCompensationPos(FDeviceNumber, Motor, PulsesPerUnit[Motor], Result);
end;

function TPcomm32.GetBiasPosition(Motor : Integer) : Double;
begin
  Result := PmacDPRGetBIASPos(FDeviceNumber, Motor, PulsesPerUnit[Motor]);
end;

function TPcomm32.GetMotorStatus1(Motor : Integer) : TPmacMotorStatii1;
begin
  if Version = pvTurbo then
    Result:= TurboPmacMotorServoData[Motor].MotorGeneralStatus
  else
    Result := TPmacMotorStatii1(PmacDPRMotorServoStatus(FDeviceNumber, Motor));
end;

function TPcomm32.GetMotorStatus2(Motor : Integer) : TPmacMotorStatii2;
begin
  if Version = pvTurbo then
    Result:= TurboPmacMotorServoData[Motor].MotorServoStatus
  else
    Result := TPmacMotorStatii2(IDPR[$25C + (Motor * $7C)]);
end;


function TPcomm32.GetRotaryRemaining(CS : Integer) : Integer;
begin
  if Version = pvTurbo then
    Result:= TurboPmacCoordinateSystemData[CS].RotaryRemaining
  else
    Result := PmacDPRProgRemaining(FDeviceNumber, CS);
end;

{
function TPcomm32.GetProgramRunning(CS : Integer) : Boolean;
begin
  Result := PmacDPRProgRunning(FDeviceNumber, CS) <> 0;
end;
}
{
function TPcomm32.GetCoordStatus2(CS : Integer) : TPmacCoordSystemStatii2;
begin
  Result := TPmacCoordSystemStatii2(PmacDPRPe(FDeviceNumber, CS));
end;
}
function TPcomm32.GetCoordStatus1(CS : Integer) : TPmacCoordSystemStatii1;
var I : Integer;
begin
  if Version = pvTurbo then
    I := Integer(TurboPmacCoordinateSystemData[CS].CoordStatus1)
  else
    I := IDPR[$264] and $FFFFFF;
  Result := TPmacCoordSystemStatii1(I);
end;

function TPcomm32.DPRWriteBuffer(NumEntries : Integer; Data : PAVBGWFormat) : Integer;
begin
  Result := PmacDprWriteBuffer(FDeviceNumber, NumEntries, Data);
end;


procedure TPcomm32.ReadFixedBuffers(Coords : Integer);
begin
  if Version = pvTurbo then begin
    GetDPRMem($74, Sizeof(TTurboPmacMotorServoData) * 8, @TurboPmacMotorServoData);
    GetDPRMem($6a4, Sizeof(TTurboPmacCoordinateSystemData) * Coords, @TurboPmacCoordinateSystemData);
    GetDPRMem(InputStart, InputLength, @InDPR);
  end else begin
    GetDPRMem($48, SizeOf(ServoFixedData^), ServoFixedData);
    GetDPRMem($24c, SizeOf(TPmacCoordinateData) * 8, CoordinateFixedData);
  end;
end;

procedure TPcomm32.WriteFixedBuffers;
begin
  if Version = pvTurbo then begin
    SetDPRMem(OutputStart, OutputLength, @OutDPR);
  end;
end;

{
var Blog: Integer;
I0 : Integer;
I1 : Integer;
}
function PmacI64(D : I64Union) : Int64;
var Upper, Lower: Integer;
    Upper64, Lower64: Int64;
begin
{
  Result := D.I[0] and $00FFFFFF;
  Result := Result + (D.I[1] * $1000000);
  }

  Lower:=  D.I[0] and $00FFFFFF;
  Upper:=  D.I[1] and $00FFFFFF;

  if (Upper and $00800000) <> 0 then
    Upper:= Upper or $FF000000;

  Upper64:= Upper;
  Lower64:= Lower;

  Result:= (Upper64 * $1000000) + Lower64;

  {
  I0 := D.I[0];
  I1 := D.I[1];
  if Result > 10000000 then
    Blog:= 1;
    }
end;

function TPcomm32.SActualPosition(Axis : Integer) : Double;
begin
  if Version = pvTurbo then
    Result:= PmacI64(TurboPmacMotorServoData[Axis].MotorNetActualPosition) / (Scaling[Axis] * 32 * PulsesPerUnit[Axis])
  else
    Result := PmacI64(ServoFixedData^.Data[Axis].ActualPosition) / (Scaling[Axis] * 32 * PulsesPerUnit[Axis]);
end;

function TPcomm32.SGetMoveTime(Motor : Integer) : Integer;
begin
  if Version = pvTurbo then
    Result:= TurboPmacCoordinateSystemData[0].TimeRemainingInMove
  else
    Result := 0;
end;


function TPcomm32.SMasterPosition(Axis : Integer) : Double;
begin
  if Version = pvTurbo then
    Result:= PmacI64(TurboPmacMotorServoData[Axis].MotorMasterPosition) / (Scaling[Axis] * 32 * PulsesPerUnit[Axis])
  else
    Result := PmacI64(ServoFixedData^.Data[Axis].MasterPosition) / (Scaling[Axis] * 32 * PulsesPerUnit[Axis]);
end;

function TPcomm32.SCommandedPosition(Axis : Integer) : Double;
begin
  if Version = pvTurbo then
    Result:= Self.SActualPosition(Axis) + Self.SFollowError(Axis)
    //Result:= PmacI64(TurboPmacCoordinateSystemData[0].TargetPosition[Axis]) / (96 * 32 * PulsesPerUnit[Axis])
  else
    Result := PmacI64(ServoFixedData^.Data[Axis].CommandedPosition) / (Scaling[Axis] * 32 * PulsesPerUnit[Axis]);
end;

function TPcomm32.SPositionBias(Axis : Integer) : Double;
begin
  if Version = pvTurbo then
    Result:= PmacI64(TurboPmacMotorServoData[Axis].MotorPositionBias) / (Scaling[Axis] * 32 * PulsesPerUnit[Axis])
  else
    Result := PmacI64(CoordinateFixedData^.Data[Axis].PositionBias) / (Scaling[Axis] * 32 * PulsesPerUnit[Axis]);
end;

function TPcomm32.SCompensation(Axis : Integer) : Double;
begin
  if Version = pvTurbo then
    Result:= 0
  else
    Result := PmacI64(ServoFixedData^.Data[Axis].Compensation) / (Scaling[Axis] * 32 * PulsesPerUnit[Axis]);
end;

function Pmac64BitDouble(Y, X: Integer): Double;
var M : Double;
    Exp : Integer;
    Tmp : Integer;
begin
  Tmp := X;
  if (Tmp and $800000) <> 0 then
    Tmp := Tmp or $FF000000; // Sign Extend to 32bits

  m:= Tmp;

  if (m = 0.0) then begin
    Result:= 0;
    Exit
  end;

  Exp:= (Y and $FFF) - 2070;
  if Abs(Exp) > 30 then
    Result := 0
  else
    Result := Power(2.0, exp) * m;
end;

function GetIEEE32BitFloat(X: Integer): Double;
var Sign: Boolean;
    E: Integer;
    F: Integer;
begin
  F:= X and $7fffff;
  X:= X shr 23;
  E:= (X and $ff) - 127;
  Sign:= (X and $100) <> 0;

  if (E = $ff) and (F <> 0) then begin
    Result:= 0; // This is NAN
  end
  else if (E = $ff) and (F = 0) and Sign then begin
    Result:= MinDouble;
  end
  else if (E = $ff) and (F = 0) and not Sign then begin
    Result:= MaxDouble;
  end
  else if Sign then begin
    Result:= -1 * Power(2.0, E) * (1 + Power(2.0, -23) * F);
  end
  else begin
    Result:= 1 * Power(2.0, E) * (1 + Power(2.0, -23) * F);
  end;
end;

function SetIEEE32BitFloat(X: Double): Cardinal;
var Sign: Boolean;
    E: Cardinal;
    F: Cardinal;
    Tmp: Double;
begin
  Result:= 0;
  if X < 0 then begin
    Sign:= True;
    X:= Abs(X);
  end
  else
    Sign:= False;

  Tmp:= X;
  E:= 0;

  if Abs(Tmp) > 0.000000001 then begin
    if Tmp >= 2 then begin
      while(Tmp >= 2) do begin
        Tmp:= Tmp / 2;
        Inc(E);
      end;
    end
    else begin
      while(Tmp < 1) do begin
        Tmp:= Tmp * 2;
        Dec(E);
      end;
    end;
  end
  else begin
    Result:= 0;
    Exit;
  end;
  E:= E + 127;

  Tmp:= Power(2.0, 23) * Tmp;
  F:= Round(Tmp);

  if Sign then
    Result:= Result or $100;
  //E:= E-1;
  Result:= Result or E;
  Result:= Result shl 23;
  Result:= Result or (F and $7fffff);
end;

function TPComm32.SFollowError(Axis: Integer): Double;
begin
  if Version = pvTurbo then
    with TurboPmacMotorServoData[Axis].FollowError do
      //Result:= Pmac64BitDouble(I[0], I[1])
      Result:= PmacI64(TurboPmacMotorServoData[Axis].FollowError) / (Scaling[Axis] * 32 * PulsesPerUnit[Axis])
  else
    Result:= 0; // Derived elsewhere
end;

function TPComm32.STargetPosition(Axis : Integer) : Double;
begin
  if Version = pvTurbo then
    with TurboPmacCoordinateSystemData[0].TargetPosition[AxesByName[Axis]] do begin
      Result := Pmac64BitDouble(I[0], I[1]);
    end
  else
    with CoordinateFixedData^.Data[0].TargetPosition[AxesByName[Axis]] do begin
      Result := Pmac64BitDouble(I[0], I[1]);
    end;
end;

function TPComm32.STimeRemainingInMove(CS: Integer): Double;
begin
  if Version = pvTurbo then
    Result:= TurboPmacCoordinateSystemData[0].TimeRemainingInMove / 1000
  else
    Result:= 0;
end;

function TPComm32.SMotorTargetPosition(Axis : Integer) : Double;
begin
  if Version = pvTurbo then
    Result:= Self.SActualPosition(Axis) + Self.SFollowError(Axis)
  else
    Result := PmacI64(CoordinateFixedData^.Data[Axis].MotorTargetPosition) / (Scaling[Axis] * 32 * PulsesPerUnit[Axis]);
end;

function TPComm32.SProgramLinesRemaining : Integer;
begin
  if Version = pvTurbo then
    Result:= TurboPmacCoordinateSystemData[0].RotaryRemaining
  else
    Result := CoordinateFixedData^.Data[0].ProgramLinesRemaining;
end;


function TPcomm32.GetOutputBase: Pointer;
begin
  Result:= @OutDPR;
end;

function TPcomm32.GetInputBase: Pointer;
begin
  Result:= @InDPR;
end;

function TPcomm32.SCoordVelocity: Double;
var I: Integer;
    Tmp: Double;
    Sum: Double;
begin
  Sum:= 0;
  for I:= 0 to Self.VelocityAxesCount - 1 do begin
    Tmp:= TurboPmacMotorServoData[I].MotorFilteredActualVelocity;
    Tmp:= Tmp * ServoRate * 60 / (Scaling[I] * 32 * Self.PulsesPerUnit[I]);
    Sum:= Sum + (Tmp * Tmp);
  end;
  Result:= Sqrt(Sum) * 1.000; // This is an appalling frig ????
  //Result := CoordinateFixedData^.Data[0].MotorAverageActualVelocity;
end;

function TPcomm32.SAxisVelocity(Axis: Integer): Double;
begin
  Result:= TurboPmacMotorServoData[Axis].MotorFilteredActualVelocity * ServoRate * 60 / (3072 * Self.PulsesPerUnit[Axis]);
end;

initialization
  ResponseBox := TTextDisplay.Create(Application);
end.
