object TextDisplay: TTextDisplay
  Left = 350
  Top = 322
  Width = 458
  Height = 374
  Caption = 'Pmac Download Response'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = True
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object ResponseMemo: TMemo
    Left = 0
    Top = 0
    Width = 450
    Height = 303
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'Courier New'
    Font.Style = []
    ParentFont = False
    ReadOnly = True
    ScrollBars = ssBoth
    TabOrder = 0
    WordWrap = False
  end
  object Panel1: TPanel
    Left = 0
    Top = 303
    Width = 450
    Height = 44
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    object ProgressBar: TProgressBar
      Left = 0
      Top = 0
      Width = 450
      Height = 16
      Align = alTop
      Min = 0
      Max = 100
      TabOrder = 0
    end
    object BitBtn1: TBitBtn
      Left = 168
      Top = 18
      Width = 75
      Height = 25
      TabOrder = 1
      Kind = bkClose
    end
  end
end
