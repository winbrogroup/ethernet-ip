unit PComm32Edit;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Menus, StdCtrls, PComm32, CNCTypes, CNC32, ExtCtrls;

type
  TPmacEdit = class(TForm)
    Text: TMemo;
    MainMenu1: TMainMenu;
    File1: TMenuItem;
    Open1: TMenuItem;
    Close1: TMenuItem;
    Save1: TMenuItem;
    SaveAs1: TMenuItem;
    Download1: TMenuItem;
    OpenDialog1: TOpenDialog;
    SaveDialog1: TSaveDialog;
    New1: TMenuItem;
    procedure Open1Click(Sender: TObject);
    procedure Close1Click(Sender: TObject);
    procedure Save1Click(Sender: TObject);
    procedure SaveAs1Click(Sender: TObject);
    procedure Download1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure New1Click(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
  public
    PComm32 : TPComm32;
  end;

var
  PmacEdit: TPmacEdit;

implementation

{$R *.DFM}

resourcestring
  NewFile = 'NewFile';

procedure TPmacEdit.Open1Click(Sender: TObject);
begin
  with OpenDialog1 do
    if Execute then begin
      Text.Lines.LoadFromFile(Filename);
      Caption := FileName;
    end;
end;

procedure TPmacEdit.Close1Click(Sender: TObject);
begin
  Close;
end;

procedure TPmacEdit.Save1Click(Sender: TObject);
begin
  if Caption = NewFile then SaveAs1Click(Self)
                  else Text.Lines.SaveToFile(Caption);
end;

procedure TPmacEdit.SaveAs1Click(Sender: TObject);
begin
  with SaveDialog1 do
    if Execute then begin
       Caption := Filename;
       Text.Lines.SaveToFile(Caption);
    end;
end;

procedure TPmacEdit.Download1Click(Sender: TObject);
begin
  Save1Click(Self);
  if Caption = NewFile then exit;
  PComm32.DownLoadFileName(Caption, True);
end;

procedure TPmacEdit.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Self.Free;
end;

procedure TPmacEdit.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  CanClose := True;
end;

procedure TPmacEdit.New1Click(Sender: TObject);
begin
  Text.Lines.Clear;
  Caption := NewFile;
end;

procedure TPmacEdit.FormActivate(Sender: TObject);
begin
  OpenDialog1.InitialDir := ProfilePath + 'pmaccode';
  SaveDialog1.InitialDir := ProfilePath + 'pmaccode';
end;

procedure TPmacEdit.FormCreate(Sender: TObject);
begin
  Caption := NewFile;
end;

end.
