unit Response;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, ComCtrls, Buttons;

type
  TTextDisplay = class(TForm)
    ResponseMemo: TMemo;
    Panel1: TPanel;
    ProgressBar: TProgressBar;
    BitBtn1: TBitBtn;
  private
    FProgress : Integer;
    procedure SetProgress(aProgress : Integer);
    function GetLines : TStrings;
  public
    property Progress : Integer read FProgress write SetProgress;
    procedure AddText(aText : string);
    procedure Clear;
    property Lines : TStrings read GetLines;
  end;

var
  TextDisplay: TTextDisplay;

implementation

{$R *.DFM}

procedure TTextDisplay.AddText(aText : string);
begin
  ResponseMemo.Lines.Add(aText);
end;

procedure TTextDisplay.SetProgress(aProgress : Integer);
begin
  ProgressBar.Position := aProgress;
end;

procedure TTextDisplay.Clear;
begin
  ProgressBar.Position := 0;
  ResponseMemo.Lines.Clear;
end;

function TTextDisplay.GetLines : TStrings;
begin
  Result := ResponseMemo.Lines;
end;

end.
