object PmacEdit: TPmacEdit
  Left = 181
  Top = 96
  Width = 519
  Height = 374
  Caption = 'Pmac32 Edit'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Menu = MainMenu1
  OldCreateOrder = True
  Position = poScreenCenter
  Visible = True
  OnActivate = FormActivate
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Text: TMemo
    Left = 0
    Top = 0
    Width = 511
    Height = 328
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Courier New'
    Font.Style = []
    ParentFont = False
    ScrollBars = ssBoth
    TabOrder = 0
    WantTabs = True
    WordWrap = False
  end
  object MainMenu1: TMainMenu
    Left = 384
    Top = 112
    object File1: TMenuItem
      Caption = 'File'
      object New1: TMenuItem
        Caption = '&New'
        OnClick = New1Click
      end
      object Open1: TMenuItem
        Caption = '&Open'
        OnClick = Open1Click
      end
      object Close1: TMenuItem
        Caption = '&Close'
        OnClick = Close1Click
      end
      object Save1: TMenuItem
        Caption = '&Save'
        OnClick = Save1Click
      end
      object SaveAs1: TMenuItem
        Caption = 'Save &As'
        OnClick = SaveAs1Click
      end
      object Download1: TMenuItem
        Caption = '&Download'
        OnClick = Download1Click
      end
    end
  end
  object OpenDialog1: TOpenDialog
    Filter = 
      'Pmac Files|*.h;*.plc;*.pmc|Header File|*.h|Pmac PLC|*.plc|Pmac M' +
      'otion Program|*.pmc'
    Left = 112
    Top = 48
  end
  object SaveDialog1: TSaveDialog
    Filter = 
      'Pmac Files|*.h;*.plc;*.pmc|Header File|*.h|Pmac PLC|*.plc|Pmac M' +
      'otion Program|*.pmc'
    Left = 144
    Top = 48
  end
end
