unit TagFilter;

interface

uses SysUtils, Classes, CNCTypes, CNC32, CoreCNC, CNCAbs, StreamTokenizer;

type
  TFilterOperation = (
    foTimeStampRising,
    foTimeStampFalling
  );

  TFilter = class
  private
    FInputTagName: string;
    FOutputTagName: string;
    FInputTag: TAbstractTag;
    FOutputTag: TAbstractTag;
    Operation: TFilterOperation;
    LastBValue: Boolean;
  end;

  TTagFilter = class(TExpandDisplay)
  private
    Filters: TList;
    procedure TagChange(ATag: TAbstractTag);
    procedure LoadConfig;
  public
    constructor Create(AOwner: TComponent); override;
    procedure InstallComponent(Params: TParamStrings); override;
    procedure Installed; override;
    procedure CheckInstallation; override;
  end;

implementation

const
  FilterOperations: array [TFilterOperation] of string = (
    'timestamprising',
    'timestampfalling'
  );


{ TTagFilter }

procedure TTagFilter.CheckInstallation;
var I: Integer;
    F: TFilter;
begin
  inherited;
  for I:= 0 to Filters.Count - 1 do begin
    F:= TFilter(Filters[I]);
    case F.Operation of
      foTimeStampRising,
      foTimeStampFalling: begin
        F.FInputTag:= Self.TagEvent(F.FInputTagName, EdgeChange, TIntegerTag, TagChange);
        F.FOutputTag:= Self.TagPublish(F.FOutputTagName, TStringTag);
      end;
    end;
  end;
end;

constructor TTagFilter.Create(AOwner: TComponent);
begin
  inherited;
  Filters:= TList.Create;
end;

procedure TTagFilter.InstallComponent(Params: TParamStrings);
begin
  inherited;
  LoadConfig;
end;

procedure TTagFilter.Installed;
begin
  inherited;
end;

procedure TTagFilter.LoadConfig;
var S: TStreamQuoteTokenizer;
    Token: string;

  function ReadFilterOperation(const AFop: string): TFilterOperation;
  var Fop: string;
  begin
    Fop:= LowerCase(AFop);
    for Result:= Low(Result) to High(Result) do begin
      if Fop = FilterOperations[Result] then
        Exit;
    end;
    raise Exception.CreateFmt('Invalid FilterOperation [%s]', [AFop]);
  end;

  procedure ReadFilter;
  var F: TFilter;
  begin
    S.ExpectedTokens(['=', '{']);
    F:= TFilter.Create;
    Filters.Add(F);
    Token := LowerCase(S.Read);
    while not S.EndOfStream do begin
      if Token = 'inputtag' then
        F.FInputTagName:= S.ReadStringAssign
      else if Token = 'outputtag' then
        F.FOutputTagName:= S.ReadStringAssign
      else if Token = 'operation' then
        F.Operation:= ReadFilterOperation(S.ReadStringAssign)
      else if Token = '}' then
        Exit
      else
        raise Exception.CreateFmt('Unexpected Token [%s]', [Token]);

      Token := LowerCase(S.Read);
    end;
    raise Exception.Create('Unexpected end of file');
  end;


begin
  S := TStreamQuoteTokenizer.Create;
  S.Seperator := '[]{}';
  S.QuoteChar := '''';
  S.CommentChar := '#';

  try
    try
      S.SetStream(OpenLiveStream(Name));
      S.ExpectedTokens(['TagFilter', '=', '{']);
      Token := LowerCase(S.Read);
      while not S.EndOfStream do begin
        if Token = 'filter' then
          ReadFilter
        else if Token = '}' then
          Exit
        else
          raise Exception.CreateFmt('Unexpected Token [%s] at line [%s]', [Token, S.Line]);
        Token := LowerCase(S.Read);
      end;
    except
      on E: Exception do
      raise Exception.CreateFmt('"%s" failed to load configuration file "%s"', [Name, E.Message]);
    end;
  finally
    S.Free;
  end;
end;

procedure TTagFilter.TagChange(ATag: TAbstractTag);
var I: Integer;
    F: TFilter;
    B: Boolean;
begin
  inherited;
  for I:= 0 to Filters.Count - 1 do begin
    F:= TFilter(Filters[I]);
    if F.FInputTag = ATag then begin
      case F.Operation of
        foTimeStampRising: begin
          B:= F.FInputTag.AsBoolean;
          if B and not F.LastBValue then begin
            F.FOutputTag.AsString:= CncTypes.StorageDate(Now);
          end;
          F.LastBValue:= B;
        end;

        foTimeStampFalling: begin
          B:= F.FInputTag.AsBoolean;
          if not B and F.LastBValue then begin
            F.FOutputTag.AsString:= CncTypes.StorageDate(Now);
          end;
          F.LastBValue:= B;
        end;
      end;
    end;
  end;
end;

initialization
  CoreCNC.RegisterCNCClass(TTagFilter);
end.
