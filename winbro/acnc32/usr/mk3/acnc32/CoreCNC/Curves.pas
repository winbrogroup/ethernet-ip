unit Curves;

interface
uses
  Graphics, Classes;
{17/6 comment updated mgl
      max data, min data added}
{Each Curve is defined at RecordCount evenly-spaced point moments in time,
separated by SamplePeriod The basic data type is integer. Data can be
retrieved at specific points (SampleData) or at an arbitrary point in time
(Data). GetData presently uses linear interpolation but descendents may wish
to implement more sophisicated schemes - eg cubic or bezier splines.
The default property - Data - retrives the value of a curve at an arbitrary
time

Data is defined from 0 to SampleLength (Generally Seconds)

the parameter to the curve, t is arbitrary, but will generally be time. In
all present applications of this class (2/7/98) t is in seconds

It is important to appreciate the difference between the parameters i and t.
i is the 'sample number'. Basically for a given sample point, t = i * SamplePeriod
}

type
  TCurve = class
  private
    FShow : Boolean;
    function GetMaxData( Ch: Integer): Integer;
    function GetMinData( Ch: Integer): Integer;
    function GetLength: Double;
  protected
    function GetSamplePeriod: Double; virtual; abstract;
    procedure SetSamplePeriod( Value: Double); virtual; abstract;
    function GetSampleData(i: Integer; Ch: Integer): Integer; virtual; abstract;
    function GetSampleCount: Integer; virtual; abstract;

    function GetIndexAtTime(T : Double) : Integer;

    function GetChannelCount: Integer; virtual; abstract;
    procedure SetChannelCount( Val: Integer); virtual; abstract;
    function GetChannelName( Ch: Integer): String; virtual; abstract;
    procedure SetChannelName( Ch: Integer; const Val: String); virtual; abstract;

    function GetData(t: Double; Ch: Integer): Integer; virtual;
  public
    Color: TColor;  {ad hoc - please forgive...}
    function ValidChannel(Ch: Integer): Boolean;
    function DefinedAt(t: Double): Boolean;


    procedure DefineChannels(const Names: array of String); virtual;   {for convenience}
    function Compatible( Curve: TCurve): Boolean;  {fields and period the same}

    property SampleData[i: Integer; Ch: Integer]: Integer read GetSampleData;
    property SampleCount: Integer read GetSampleCount;
    property SamplePeriod: Double read GetSamplePeriod write SetSamplePeriod;
    property IndexAtTime[T : Double] : Integer read GetIndexAtTime;

    property Data[t: Double; Ch: Integer]: Integer read GetData; default;
    property Length: Double read GetLength;
    property ChannelName[Ch: Integer]: String read GetChannelName write SetChannelName;
    property ChannelCount: Integer read GetChannelCount write SetChannelCount;

    property MaxData[Ch: Integer]: Integer read GetMaxData;
    property MinData[Ch: Integer]: Integer read GetMinData;
    property Show : Boolean read FShow write FShow; // Used for display only
  end;

{should probably implement a property to return the first (and possibly
second) derivatives of Data - get back to this}

implementation
uses
  SysUtils;

const
  Eps30 = 1e-30;

function TCurve.GetMaxData( Ch: Integer): Integer;
var
  i, sd: Integer;
begin
  if SampleCount = 0 then
  begin
    Result:= 0
  end else
  begin
    Result:= SampleData[0, Ch];
    for i:= 0 to SampleCount - 1 do
    begin
      sd:= SampleData[i, Ch];
      if  sd > Result then
        Result:= sd
    end
  end
end;

function TCurve.GetMinData( Ch: Integer): Integer;
var
  i, sd: Integer;
begin
  if SampleCount = 0 then
  begin
    Result:= 0
  end else
  begin
    Result:= SampleData[0, Ch];
    for i:= 0 to SampleCount - 1 do
    begin
      sd:= SampleData[i, Ch];
      if sd < Result then
        Result:= sd
    end
  end
end;

function TCurve.GetLength: Double;
{length of sample is 0 until there are two sample
points...}
begin
  if SampleCount < 2 then
    Result:= 0
  else
    Result:= (SampleCount - 1)*SamplePeriod
end;

function TCurve.GetIndexAtTime(T : Double) : Integer;
begin
  Result := Round(T / SamplePeriod);
end;

function TCurve.DefinedAt( t: Double): Boolean;
begin
  Result:= (t >= 0) and (t <= Length)
end;

function TCurve.GetData(t: Double; Ch: Integer): Integer;
{this is a simple linear interpolation.
 It may be appropriate to implement a more sophisticated spline:
 say cubic or bezier. This could be done in descendents by
 overriding this method}

{when is the result defined? The sample begins at
T=0.
        X   X
    X           X
X

|---|---|---|---|---|
0   10  20  30  40  50

consider the case above. dt=10 units and Buffer.Count = 5.
The profile is defined for all 0 >= t <= 40.

when there is only one point defined the result is not defined
except in the special case where t = 0.}

var
  Sample: Integer;
  nPeriods: Double;
begin
  if DefinedAt(t) and ValidChannel(Ch) then
  begin
    if (t < eps30) and (SampleCount > 0) then {special case}
    begin
      Result:= GetSampleData(0, Ch)
    end else
    begin
      nPeriods:= t/SamplePeriod;
      Sample:= Trunc(nPeriods);
      if Sample >= SampleCount then  {unlikely but possible}
      begin
        Result:= GetSampleData(SampleCount - 1, Ch)
      end else
      begin
        Result:= GetSampleData(Sample, Ch);
        Inc(Sample);
        if Sample < SampleCount then {practically certain}
          Result:= Round(Result + Frac(nPeriods)*(GetSampleData(Sample, Ch) - Result))
      end
    end
  end else
  begin
    Result:= 0
  end;
end;

procedure TCurve.DefineChannels(const Names: array of String);
var
  i: Integer;
begin
  ChannelCount:= High(Names) + 1;
  for i:= 0 to High(Names) do
    ChannelName[i]:= Names[i]
end;

function TCurve.ValidChannel(Ch: Integer): Boolean;
begin
  Result:= (Ch >= 0) and (Ch < ChannelCount)
end;

{
function TCurve.GetChannelName(Ch: Integer): String;
begin
  if ValidChannel(Ch) then
    Result:= CHList[Ch]
  else
    Result:= ''
end;
}

function TCurve.Compatible( Curve: TCurve): Boolean;  {fields and period the same}
const
  PeriodMargin = 1e-6;
var
  i: Integer;
begin
  Result:= (abs(SamplePeriod - Curve.SamplePeriod) < PeriodMargin) and
           (ChannelCount = Curve.ChannelCount);
  if Result then
  for i:= 0 to ChannelCount - 1 do
  if CompareText(ChannelName[i], Curve.ChannelName[i]) <> 0 then
  begin
    Result:= false;
    break
  end
end;

end.
