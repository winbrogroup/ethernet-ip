unit AboutForm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, CoreCNC, Plugin, Grids, CNC32{, Splash};

type
  TAbout = class(TForm)
    Panel1: TPanel;
    BitBtn1: TBitBtn;
    VersionPanel: TPanel;
    GroupBox1: TGroupBox;
    StringGrid: TStringGrid;
  private
    procedure BuildDisplay;
  public
    class procedure Execute;
  end;

var
  About: TAbout;

implementation

uses Splash;

{$R *.DFM}

{ TAbout }

procedure TAbout.BuildDisplay;
var I : Integer;
    NP : TNamedPlugin;
    Tag: TAbstractTag;
begin
  VersionPanel.Caption := 'ACNC32 ' + SplashForm.Version;
  StringGrid.Cells[0, 0] := 'Plugin';
  StringGrid.Cells[1, 0] := 'Version';
  for I := 0 to SysObj.PluginList.Count - 1 do begin
    NP := TNamedPlugin(SysObj.PluginList.Objects[I]);
    StringGrid.Cells[0, I + 1] := ExtractFileName(NP.FileName);
    StringGrid.Cells[1, I + 1] := NP.Revision;
  end;

  I:= SysObj.PluginList.Count + 1;

  tag:= SysObj.Comms.FindTag('MachineVersion');
  if(tag <> nil) then begin
    StringGrid.Cells[0, I] := 'Machine';
    StringGrid.Cells[1, I] := tag.asString;
    Inc(I)
  end;

  // if there are any more things to add follow the above pattern
end;

class procedure TAbout.Execute;
begin
  if not Assigned(About) then begin
    About := TAbout.Create(Application);
    try
      About.BuildDisplay;
      About.ShowModal;
    finally
      About.Free;
      About := nil;
    end;
  end;
end;

end.
