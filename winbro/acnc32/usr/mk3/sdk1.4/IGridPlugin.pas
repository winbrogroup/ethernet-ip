unit IGridPlugin;

interface

uses Windows, INamedInterface;

type
  TGPGridState = set of ( OgdSelected,  OgdFocused, OgdFixed );

  TGridUpdateType = (
    guCell,
    guCount,
    guHighLight,
    guFixed,
    guTitle,
    guStatus,
    guSelfDraw,
    guCombo,
    guVisible,
    guEditing
  );

  IFSDGridPlugin = interface(IInstallablePlugin)
  ['{AA2C5604-8E5E-11D5-9F56-0800460222F0}']
    procedure Update; stdcall;
    function CanEdit(X, Y : Integer) : Boolean; stdcall;
    procedure Edit(X, Y : Integer; var Req : WideString); stdcall;
    procedure DrawCell(aHDC : HDC; Rect : TRect; X, Y : Integer; State : TGPGridState); stdcall;
    procedure ComboSelect(aIndex : Integer); stdcall;
    procedure InPlaceEdit(X, Y : Integer); stdcall;
    procedure Nudge(X, Y : Integer; Upd : Integer); stdcall;
  end;

  IFSDGridPluginHost = interface
  ['{AA2C5605-8E5E-11D5-9F56-0800460222F0}']
    procedure UpdateEv(GridUT : TGridUpdateType; X, Y : Integer; Data : Pointer); stdcall;
  end;

  TGetFSDGridPluginInterface = function(Host : IFSDGridPluginHost; Name : PWideString) : IFSDGridPlugin; stdcall;

const
  GetFSDGridPluginInterfaceName = 'GetFSDGridPluginInterface';

implementation

end.
