unit AbstractFormPlugin;

interface

uses Classes, Sysutils, Forms, INamedInterface, IFormInterface;

type
  TAbstractFormPlugin = class(TObject, IFSDFormInterface, IInstallablePlugin)
  private
    ThisForm : TInstallForm;
    RefCount : Integer;
  public
    constructor Create(Host : IHostFormInterface; FormClass : TInstallFormClass);
    function QueryInterface(const IID: TGUID; out Obj): HResult; stdcall;
    function _AddRef : Integer; stdcall;
    function _Release : Integer; stdcall;
    procedure Show; stdcall;
    procedure Hide; stdcall;

    procedure SetLeft(aLeft : Integer); stdcall;
    function GetLeft : Integer; stdcall;
    procedure SetTop(aTop : Integer); stdcall;
    function GetTop : Integer; stdcall;
    procedure SetWidth(aWidth : Integer); stdcall;
    function GetWidth : Integer; stdcall;
    procedure SetHeight(aHeight : Integer); stdcall;
    function GetHeight : Integer; stdcall;
    procedure SetCaption(aCaption : WideString); stdcall;
    function GetCaption : WideString; stdcall;

    procedure SetBorderIcons(aIcons : TBorderIcons); stdcall;
    function GetBorderIcons : TBorderIcons; stdcall;

    procedure SetBorderStyle(aBorderStyle : TBorderStyle); stdcall;
    function GetBorderStyle : TBorderStyle; stdcall;

    procedure SetFormStyle(aFormStyle : TFormStyle); stdcall;
    function GetFormStyle : TFormStyle; stdcall;

    procedure SetVisible(aVisible : Boolean); stdcall;
    function GetVisible : Boolean; stdcall;

    procedure SetEnabled(aEnabled : Boolean); stdcall;
    function GetEnabled : Boolean; stdcall;

    procedure SetFocus(aFocus : Boolean); stdcall;
    function Focused : Boolean; stdcall;

    procedure SetInstallName(aInstallName : WideString); stdcall;
    function GetInstallName : WideString; stdcall;

    procedure DisplaySweep; stdcall;

    class procedure AddPlugin(aName : string; aFormClass : TInstallFormClass);
    class function FindPlugin(aName : string) : TInstallFormClass;
    procedure Install; stdcall;
    procedure FinalInstall; stdcall;
    procedure Shutdown; stdcall;
    procedure Execute; stdcall;
    procedure CloseForm; stdcall;
  end;

implementation

var PluginList : TStringList;

class procedure TAbstractFormPlugin.AddPlugin(aName : string; aFormClass : TInstallFormClass);
begin
  PluginList.AddObject(aName, Pointer(aFormClass));
end;

class function TAbstractFormPlugin.FindPlugin(aName : string) : TInstallFormClass;
var I : Integer;
begin
  Result := nil;
  for I := 0 to PluginList.Count - 1 do begin
    if CompareText(aName, PluginList[I]) = 0 then begin
      Result := TInstallFormClass(PluginList.Objects[I]);
      Exit;
    end;
  end;
end;

procedure TAbstractFormPlugin.Install;
begin
  ThisForm.Install;
end;

procedure TAbstractFormPlugin.FinalInstall;
begin
  ThisForm.FinalInstall;
end;

procedure TAbstractFormPlugin.Shutdown;
begin
  ThisForm.Shutdown;
end;


function TAbstractFormPlugin.QueryInterface(const IID: TGUID; out Obj): HResult; stdcall;
const
  E_NOINTERFACE = HResult($80004002);
begin
  if GetInterface(IID, Obj) then Result := 0 else Result := E_NOINTERFACE;
end;


constructor TAbstractFormPlugin.Create(Host : IHostFormInterface; FormClass : TInstallFormClass);
begin
  inherited Create;

  ThisForm := FormClass.Create(nil);
  ThisForm.Host := Host;
end;

function TAbstractFormPlugin._AddRef : Integer; stdcall;
begin
  Inc(RefCount);
  Result := RefCount;
end;

function TAbstractFormPlugin._Release : Integer; stdcall;
begin
  Dec(RefCount);
  Result := RefCount;
  if RefCount = 0 then begin
    ThisForm.Hide;
    ThisForm.Free;
    Free;
  end;
end;

procedure TAbstractFormPlugin.Show;
begin
  ThisForm.Show;
end;

procedure TAbstractFormPlugin.Hide;
begin
  ThisForm.Hide;
end;

function TAbstractFormPlugin.GetWidth: Integer;
begin
  Result := ThisForm.Width;
end;

procedure TAbstractFormPlugin.SetWidth(aWidth: Integer);
begin
  ThisForm.Width := aWidth;
end;

procedure TAbstractFormPlugin.DisplaySweep;
begin
  ThisForm.DisplaySweep;
end;

function TAbstractFormPlugin.GetCaption: WideString;
begin
  Result := ThisForm.Caption;
end;

function TAbstractFormPlugin.GetHeight: Integer;
begin
  Result := ThisForm.Height;
end;

function TAbstractFormPlugin.GetLeft: Integer;
begin
  Result := ThisForm.Left;
end;

function TAbstractFormPlugin.GetTop: Integer;
begin
  Result := ThisForm.Top;
end;

procedure TAbstractFormPlugin.SetCaption(aCaption: WideString);
begin
  ThisForm.Caption := aCaption;
end;

procedure TAbstractFormPlugin.SetHeight(aHeight: Integer);
begin
  ThisForm.Height := aHeight;
end;

procedure TAbstractFormPlugin.SetLeft(aLeft: Integer);
begin
  ThisForm.Left := aLeft;
end;

procedure TAbstractFormPlugin.SetTop(aTop: Integer);
begin
  ThisForm.Top := aTop;
end;

function TAbstractFormPlugin.GetBorderIcons: TBorderIcons;
begin
  Result := ThisForm.BorderIcons;
end;

function TAbstractFormPlugin.GetBorderStyle: TBorderStyle;
begin
  Result := ThisForm.BorderStyle;
end;

function TAbstractFormPlugin.GetEnabled: Boolean;
begin
  Result := ThisForm.Enabled;
end;

function TAbstractFormPlugin.GetVisible: Boolean;
begin
  Result := ThisForm.Visible;
end;

procedure TAbstractFormPlugin.SetBorderIcons(aIcons: TBorderIcons);
begin
  ThisForm.BorderIcons := aIcons;
end;

procedure TAbstractFormPlugin.SetBorderStyle(aBorderStyle: TBorderStyle);
begin
  ThisForm.BorderStyle := aBorderStyle;
end;

procedure TAbstractFormPlugin.SetEnabled(aEnabled: Boolean);
begin
  ThisForm.Enabled := aEnabled;
end;

procedure TAbstractFormPlugin.SetVisible(aVisible: Boolean);
begin
  ThisForm.Visible := aVisible;
end;

function TAbstractFormPlugin.GetFormStyle: TFormStyle;
begin
  Result := ThisForm.FormStyle;
end;

procedure TAbstractFormPlugin.SetFormStyle(aFormStyle: TFormStyle);
begin
  ThisForm.FormStyle := aFormStyle;
end;

function TAbstractFormPlugin.GetInstallName: WideString;
begin
  Result := ThisForm.InstallName;
end;

procedure TAbstractFormPlugin.SetInstallName(aInstallName: WideString);
begin
  ThisForm.InstallName := aInstallName;
end;

// Instance creation.

function GetFSDFormInterface(Host : IHostFormInterface; aName : PChar) : IFSDFormInterface; stdcall;
var TC : TInstallFormClass;
begin
  TC := TAbstractFormPlugin.FindPlugin(aName);
  if Assigned(TC) then
    Result := TAbstractFormPlugin.Create(Host, TC)
  else
    Result := nil;
end;


procedure GetFormObjects(Objects : PChar; MaxLen : Integer); stdcall;
begin
  StrPLCopy(Objects, PluginList.Text, MaxLen);
end;

exports
  GetFSDFormInterface,
  GetFormObjects;

function TAbstractFormPlugin.Focused: Boolean;
begin
  Result := ThisForm.Active;
end;

procedure TAbstractFormPlugin.SetFocus(aFocus: Boolean);
begin
  if aFocus then
    ThisForm.SetFocus;
end;

procedure TAbstractFormPlugin.Execute;
begin
  ThisForm.Execute;
end;

procedure TAbstractFormPlugin.CloseForm;
begin
  ThisForm.CloseForm;
end;

initialization
  PluginList := TStringList.Create;
end.
