unit AbstractToolLife;

interface

uses IToolLife, SysUtils, Classes, INamedInterface;

type
  TAbstractToolLifePluginClass = class of TAbstractToolLifePlugin;

  TAbstractToolLifePlugin = class(TObject, IFSDToolLife, IInstallablePlugin)
  private
    RefCount : Integer;
    function QueryInterface(const IID: TGUID; out Obj): HResult; stdcall;
    function _AddRef : Integer; stdcall;
    function _Release : Integer; stdcall;
    procedure SetInstallName(aInstallName : WideString); stdcall;
    function GetInstallName : WideString; stdcall;
  protected
    InstallName : string;
    Host : IFSDToolLifeHost;
  public
    function AddTool(Tool : WideString; SN : Integer; Total, Current : Double; Ex1, Ex2 : Integer) : TToolResult; virtual; stdcall;
    function ReplenishTool(Tool : WideString; SN : Integer) : TToolResult; virtual; stdcall;
    function RemoveTool(Tool : WideString; SN : Integer) : TToolResult; virtual; stdcall;
    function GetTotal(Tool : WideString; out Total : Double) : TToolResult; virtual; stdcall;
    function SetRemaining(Tool : WideString; SN : Integer; Used : Double) : TToolResult; virtual; stdcall;
    function GetRemaining(Tool : WideString; SN : Integer; out Remaining : Double) : TToolResult; virtual; stdcall;
    function GetToolCount : Integer; virtual; stdcall;
    function GetToolType(Index : Integer; out ToolType : WideString) : TToolResult; virtual; stdcall;
    function GetToolSN(Index : Integer; out SN : Integer) : TToolResult; virtual; stdcall;
    constructor Create(Host : IFSDToolLifeHost); virtual;
    function GetEx1ToolType(Ex1 : Integer; out ToolType : WideString) : TToolResult; virtual; stdcall;
    function GetEx1ToolSN(Ex1 : Integer; out SN : Integer) : TToolResult; virtual; stdcall;
    function ChangeEx1(Ex1Old, Ex1New : Integer) : TToolResult; virtual; stdcall;
    function ChangeEx2(Ex1, Ex2New : Integer) : TToolResult; virtual; stdcall;
    function GetExt2(Ex1 : Integer; out Ex2 : Integer) : TToolResult; virtual; stdcall;
    function GetMinimumTool(Tool : WideString; out Ex1 : Integer) : TToolResult; virtual; stdcall;

    class procedure AddPlugin(aName : string; aClass : TAbstractToolLifePluginClass);
    class function FindPlugin(aName : string) : TAbstractToolLifePluginClass;
    procedure Install; virtual; stdcall;
    procedure FinalInstall; virtual; stdcall;
    procedure Shutdown; virtual; stdcall;
  end;

function GetFSDToolLifeInterface(Host : IFSDToolLifeHost; Name : PWideString) : IFSDToolLife; stdcall;

exports
  GetFSDToolLifeInterface;

implementation

var PluginList : TStringList;

function GetFSDToolLifeInterface(Host : IFSDToolLifeHost; Name : PWideString) : IFSDToolLife; stdcall;
var
  ToolLifeClass : TAbstractToolLifePluginClass;
begin
  ToolLifeClass := TAbstractToolLifePlugin.FindPlugin(PChar(Name));
  if ToolLifeClass <> nil then begin
    Result := ToolLifeClass.Create(Host);
  end else
    Result := nil;
end;


{ TAbstractFormPlugin }

constructor TAbstractToolLifePlugin.Create(Host : IFSDToolLifeHost);
begin
  Self.Host := Host;
end;


function TAbstractToolLifePlugin._AddRef: Integer;
begin
  Inc(RefCount);
  Result := RefCount;
end;

function TAbstractToolLifePlugin._Release: Integer;
begin
  Dec(RefCount);
  Result := RefCount;
  if RefCount = 0 then begin
    Free;
  end;
end;

function TAbstractToolLifePlugin.QueryInterface(const IID: TGUID;
  out Obj): HResult;
const
  E_NOINTERFACE = HResult($80004002);
begin
  if GetInterface(IID, Obj) then Result := 0 else Result := E_NOINTERFACE;
end;


class procedure TAbstractToolLifePlugin.AddPlugin(aName: string;
  aClass: TAbstractToolLifePluginClass);
begin
  PluginList.AddObject(aName, Pointer(aClass));
end;

class function TAbstractToolLifePlugin.FindPlugin(
  aName: string): TAbstractToolLifePluginClass;
var I : Integer;
begin
  for I := 0 to PluginList.Count - 1 do begin
    if CompareText(aName, PluginList[I]) = 0 then begin
      Result := TAbstractToolLifePluginClass(PluginList.Objects[I]);
      Exit;
    end;
  end;
  Result := nil;
end;


procedure TAbstractToolLifePlugin.SetInstallName(aInstallName : WideString);
begin
  InstallName := aInstallName;
end;



procedure TAbstractToolLifePlugin.FinalInstall;
begin
end;

procedure TAbstractToolLifePlugin.Shutdown;
begin
end;

function TAbstractToolLifePlugin.GetInstallName: WideString;
begin
  Result := InstallName;
end;

function TAbstractToolLifePlugin.GetToolCount: Integer;
begin
  Result := 0;
end;


procedure TAbstractToolLifePlugin.Install;
begin
end;


function TAbstractToolLifePlugin.AddTool(Tool: WideString; SN: Integer;
  Total, Current: Double; Ex1, Ex2 : Integer): TToolResult;
begin
  Result := trNotSupported;
end;

function TAbstractToolLifePlugin.SetRemaining(Tool : WideString; SN : Integer;
   Used : Double) : TToolResult; stdcall;
begin
  Result := trNotSupported;
end;

function TAbstractToolLifePlugin.GetToolSN(Index : Integer; out SN : Integer) : TToolResult; stdcall;
begin
  Result := trNotSupported;
end;

function TAbstractToolLifePlugin.GetEx1ToolType(Ex1 : Integer; out ToolType : WideString) : TToolResult;
begin
  Result := trNotSupported;
end;

function TAbstractToolLifePlugin.GetEx1ToolSN(Ex1 : Integer; out SN : Integer) : TToolResult;
begin
  Result := trNotSupported;
end;

function TAbstractToolLifePlugin.ChangeEx1(Ex1Old, Ex1New : Integer) : TToolResult;
begin
  Result := trNotSupported;
end;

function TAbstractToolLifePlugin.ChangeEx2(Ex1, Ex2New : Integer) : TToolResult;
begin
  Result := trNotSupported;
end;

function TAbstractToolLifePlugin.GetExt2(Ex1 : Integer; out Ex2 : Integer) : TToolResult;
begin
  Result := trNotSupported;
end;

function TAbstractToolLifePlugin.GetToolType(Index: Integer; out ToolType : WideString) : TToolResult;
begin
  Result := trNotSupported;
end;

function TAbstractToolLifePlugin.ReplenishTool(Tool: WideString;
  SN: Integer): TToolResult;
begin
  Result := trNotSupported;
end;

function TAbstractToolLifePlugin.RemoveTool(Tool: WideString;
  SN: Integer): TToolResult;
begin
  Result := trNotSupported;
end;

function TAbstractToolLifePlugin.GetRemaining(Tool: WideString;
  SN: Integer; out Remaining: Double): TToolResult;
begin
  Result := trNotSupported;
end;

function TAbstractToolLifePlugin.GetMinimumTool(Tool : WideString; out Ex1 : Integer) : TToolResult;
begin
  Result := trNotSupported;
end;

function TAbstractToolLifePlugin.GetTotal(Tool: WideString;
  out Total: Double): TToolResult;
begin
  Result := trNotSupported;
end;

initialization
  Pluginlist := TStringList.Create;
end.
