unit IFormInterface;

interface

uses Forms, INamedInterface;

type
  IFSDFormInterface = interface(IInstallablePlugin)
  ['{9B9C7226-9CF2-4B39-9984-3EAC1F616F67}']
    procedure SetLeft(aLeft : Integer); stdcall;
    function GetLeft : Integer; stdcall;
    procedure SetTop(aTop : Integer); stdcall;
    function GetTop : Integer; stdcall;
    procedure SetWidth(aWidth : Integer); stdcall;
    function GetWidth : Integer; stdcall;
    procedure SetHeight(aHeight : Integer); stdcall;
    function GetHeight : Integer; stdcall;
    procedure SetCaption(aCaption : WideString); stdcall;
    function GetCaption : WideString; stdcall;

    procedure SetBorderIcons(aIcons : TBorderIcons); stdcall;
    function GetBorderIcons : TBorderIcons; stdcall;

    procedure SetBorderStyle(aBorderStyle : TBorderStyle); stdcall;
    function GetBorderStyle : TBorderStyle; stdcall;

    procedure SetVisible(aVisible : Boolean); stdcall;
    function GetVisible : Boolean; stdcall;

    procedure SetEnabled(aEnabled : Boolean); stdcall;
    function GetEnabled : Boolean; stdcall;

    procedure SetFormStyle(aFormStyle : TFormStyle); stdcall;
    function GetFormStyle : TFormStyle; stdcall;

    procedure SetFocus(aFocus : Boolean); stdcall;
    function Focused : Boolean; stdcall;

    procedure DisplaySweep; stdcall;
    procedure Install; stdcall;
    procedure FinalInstall; stdcall;
    procedure Execute; stdcall; // Only called when running as a dialogue
    procedure CloseForm; stdcall; // Only called when running as a dialogue

    property Left : Integer read GetLeft write SetLeft;
    property Top : Integer read GetTop write SetTop;
    property Width : Integer read GetWidth write SetWidth;
    property Height : Integer read GetHeight write SetHeight;
    property Caption : WideString read GetCaption write SetCaption;
    property Visible : Boolean read GetVisible write SetVisible;
    property Enabled : Boolean read GetEnabled write SetEnabled;
    property BorderIcons : TBorderIcons read GetBorderIcons write SetBorderIcons;
    property BorderStyle : TBorderStyle read GetBorderStyle write SetBorderStyle;
    property FormStyle : TFormStyle read GetFormStyle write SetFormStyle;
  end;

  IHostFormInterface = interface
  ['{5CE93136-3864-444C-A3F0-5C2D2B9E4953}']
    procedure ClientClose; stdcall;
    procedure Help; stdcall;
  end;

  TInstallFormClass = class of TInstallForm;

  TInstallForm = class(TForm)
  protected
  public
    Host : IHostFormInterface;
    InstallName : string;
    procedure Execute; virtual; stdcall;
    procedure CloseForm; virtual; stdcall;
    procedure Install; virtual; stdcall;
    procedure Shutdown; virtual; stdcall;
    procedure FinalInstall; virtual; stdcall;
    procedure DisplaySweep; virtual; stdcall;
  end;

  TGetFSDFormInterface = function(Host : IHostFormInterface; aName : PChar) : IFSDFormInterface; stdcall;

implementation

{ TInstallForm }


procedure TInstallForm.FinalInstall;
begin
end;

procedure TInstallForm.Install;
begin
end;

procedure TInstallForm.Execute;
begin
end;

procedure TInstallForm.CloseForm;
begin
end;

procedure TInstallForm.DisplaySweep;
begin
end;

procedure TInstallForm.Shutdown;
begin
end;
end.
