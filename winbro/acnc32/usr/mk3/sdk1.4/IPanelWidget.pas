unit IPanelWidget;

interface

uses WinTypes;

type
{  TPWEv = procedure(aHandle : THandle; aToken, aValue : PChar); stdcall;
  TPWSetCallback = procedure(aHandle : THandle; aName : PChar; A : TPWEv); stdcall; }

  IACNC32PanelWidget = interface
    ['{527C1BA0-6179-11D5-9F56-0800460222F0}']
    procedure Paint(HDC : THandle);
    procedure SetWidth(aWidth : Integer);
    procedure SetHeight(aHeight : Integer);
    procedure SetTop(aTop : Integer);
    procedure SetLeft(aLeft : Integer);
    function GetWidth : Integer;
    function GetHeight : INteger;
    function GetTop : Integer;
    function GetLeft : Integer;
  end;

  TAbstractCNC32PanelWidget = class(TInterfacedObject)
  end;

implementation

end.
