unit ISecurity;

interface

uses INamedInterface, CNCTypes;

type
  IFSDSecurity = interface(IInstallablePlugin)
  ['{AA2C5606-8E5E-11D5-9F56-0800460222F0}']
    function Logon(var UserName : WideString) : TAccessLevels; stdcall;
    procedure ForceSecurity(Access : TAccessLevels); stdcall;
    procedure Admin; stdcall;
    procedure LogOff; stdcall;
    function CurrentUser : WideString; stdcall;
    function CurrentAccess : TAccessLevels; stdcall;
    function Poll : Boolean; stdcall;
  end;

  IFSDSecurityHost = interface
  ['{AA2C5607-8E5E-11D5-9F56-0800460222F0}']
  end;

  TGetSecurityInterface = function(Host : IFSDSecurityHost;
                        Name : WideString;
                        Parms : WideString ) : IFSDSecurity; stdcall;

const
  GetFSDSecurityInterfaceName = 'GetFSDSecurityInterface';

implementation

end.
 