unit IOCT;

interface

uses INamedInterface, IToolLife;

type
  TOCTPluginCommand = (
    opcCycleStart,
    opcLoadOPP,
    opcSetOPPList,
    opcSetTitle,
    opcSetStatus,
    opcSetIndex,
    opcRunning,
    opcOPPComplete,
    opcOCTComplete
  );

  TOCTPluginStatus = (
    opsSingle,
    opsInCycle,
    opsOperationComplete,
    opsAutoMode,
    opsCStartPermissive,
    opsSkipOPP,
    opsOCTMode
  );
  TOCTPluginStatii = set of TOCTPluginStatus;

  IFSDOCT = interface(IInstallablePlugin)
  ['{AA2C5602-8E5E-11D5-9F56-0800460222F0}']
    procedure LoadOCT(aFileName : WideString); stdcall;
    procedure Run; stdcall;
    procedure Reset; stdcall;
    procedure Update(Status : TOCTPluginStatii); stdcall;
    function GetLoadPath : WideString; stdcall;
    procedure SetLoadPath(Path : WideString); stdcall;
    procedure OPPEnable(Index : Integer; aOn : Boolean); stdcall;
    property LoadPath : WideString read GetLoadPath write SetLoadPath;
    procedure Sort(PreferedTool : WideString); stdcall;
    function GetToolUsage(Tool : WideString) : Double; stdcall;
    function GetMinimumToolUsage(Tool : WideString; out Value : Double) : Boolean; stdcall;
  end;

  IFSDOCTHost = Interface
  ['{AA2C5603-8E5E-11D5-9F56-0800460222F0}']
    function UpdateState(HHHOCT : IFSDOCT; Command : TOCTPluginCommand; Data : Pointer) : Boolean; stdcall;
    function GetToolLifeInterface : IFSDToolLife; stdcall;
  end;


  TGetFSDOCTInterface = function(Host : IFSDOCTHost; Name : WideString) : IFSDOCT; stdcall;

const
  GetFSDOCTInterfaceName = 'GetFSDOCTInterface';

implementation

end.
