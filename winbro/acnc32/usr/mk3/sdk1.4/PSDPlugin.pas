unit PSDPlugin;

interface

uses ProcessEditor;

type
  PSD_PluginCount = function : Integer; stdcall;
  PSD_GetPlugin = function (Index: Integer): IPSDEditor; stdcall;
  PSD_SetLibrary = procedure (Lib: IPSDELibrary); stdcall;
  PSD_Initialise = procedure; stdcall;

const
  PSD_PluginCount_Name = 'PSD_PluginCount';
  PSD_GetPlugin_Name = 'PSD_GetPlugin';
  PSD_SetLibrary_Name = 'PSD_SetLibrary';
  PSD_Initialise_Name = 'PSD_Initialise';

implementation

end.
