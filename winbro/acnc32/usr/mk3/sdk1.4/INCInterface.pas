unit INCInterface;

interface

uses CNCTypes;

type
  IFSDNCInterface = interface
    function GetJogMode : TJogModes; stdcall;
    function GetSingleBlock : Boolean; stdcall;
    function GetOptionalStop : Boolean; stdcall;
    function GetBlockDelete : Boolean; stdcall;
    function GetActiveAxis : Integer; stdcall;
    function GetFeedHold : Boolean; stdcall;
    function GetCNCMode : TCNCModes; stdcall;
    function GetFeedOVR : Integer; stdcall;
    function GetAxisInMotion : Integer; stdcall;
    function GetDryRun : Boolean; stdcall;
    function GetAxisName(Index : integer) : TAxisName; stdcall;
    function GetAxisType(Index : integer) : TAxisType; stdcall;
    function GetAxisPosition(Index : Integer) : TAxisPosition; stdcall;
    function GetAxisCount : Integer; stdcall;
    function GetInCycle : Boolean; stdcall;
    function GetHomed : Boolean; stdcall;
    function GetInMotion : Boolean; stdcall;
    function GetInchModeDefault : Boolean; stdcall;

    function GetProgramLine : Integer; stdcall;
    function GetSubCount : Integer; stdcall;
    function GetActiveSubRoutine : Integer; stdcall;
    function GetSubRoutine(Index : Integer) : WideString; stdcall;

    procedure AcquireCycleStartLock; stdcall;
    procedure ReleaseCycleStartLock; stdcall;
    procedure AcquireModeChangeLock; stdcall;
    procedure ReleaseModeChangeLock; stdcall;

    property JogMode : TJogModes read GetJogMode;
    property SingleBlock : Boolean read GetSingleBlock;
    property OptionalStop : Boolean read GetOptionalStop;
    property BlockDelete : Boolean read GetBlockDelete;
    property ActiveAxis : Integer read GetActiveAxis;
    property FeedHold : Boolean read GetFeedHold;
    property CNCMode : TCNCModes read GetCNCMode;
    property FeedOvr : Integer  read GetFeedOvr;
    property AxisInMotion : Integer read GetAxisInMotion;
    property DryRun : Boolean read GetDryRun;
    property AxisName[Index : Integer] : TAxisName read GetAxisName;
    property AxisType[Index : Integer] : TAxisType read GetAxisType;
    property AxisPosition[Index : Integer] : TAxisPosition read GetAxisPosition;
    property AxisCount : Integer  read GetAxisCount;
    property InCycle : Boolean read GetInCycle;
    property Homed : Boolean read GetHomed;
    property InMotion : Boolean read GetInMotion;
    property InchModeDefault : Boolean read GetInchModeDefault;

    property ProgramLine : Integer read GetProgramLine;
    property SubCount : Integer read GetSubCount;
    property ActiveSubRoutine : Integer read GetActiveSubroutine;
    property SubRoutine[Index : Integer] : WideString read GetSubRoutine;
  end;

  TNCDialogueResult = (
    ncdrStop,
    ncdrContinue,
    ncdrError
  );

implementation


end.
