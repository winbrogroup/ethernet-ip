unit WidgetTypes;

interface

uses WinTypes;

type
  TPWEv = procedure(aHandle : THandle; aToken, aValue : PChar); stdcall;
  TPWSetCallback = procedure(aHandle : THandle; aName : PChar; A : TPWEv); stdcall;

  IMachinePanelWidget = interface
    ['{7BDDB328-5242-4D44-A6C2-08454FA97C39}']
    procedure SetWidgetBounds(aRect : TRect);
    function LibName : string;
    function DeviceName : string;
    function SetProperty(aName, aValue : string) : Boolean;
    function GetProperties : string;
    function Dims : TRect;
  end;


implementation

end.
 