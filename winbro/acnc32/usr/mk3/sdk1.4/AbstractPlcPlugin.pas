unit AbstractPlcPlugin;
{ Version 1.0.0
  Created FSD  24/12/2000 }

interface

uses IPlc;

const
  CarRet = #$d + #$a;
  BooleanIdent : array [Boolean] of PChar = ( 'False', 'True' );

type
  TPlcProperty = record
    Name : string;
    ReadOnly : Boolean;
  end;

  TEmbeddedData = record
    OData : Pointer;
    Command : Integer;
    DCommand : Integer;
  end;


  TAbstractPlcPlugin = class;

  HANDLE = TAbstractPlcPlugin;

  TPlcPluginReadEv = function(LibH : HANDLE) : PChar; stdcall;

  TPlcPluginResolveItemValueEv = function(
                           LibH : Handle;
                           aItem : Pointer
  ) : Integer; stdcall;


  TPlcPluginResolveItemEv = function (
                        LibH : HANDLE;
                        aName : PChar;
                        var IsConstant : Boolean;
                        var ResolveItemValueEv : TPlcPluginResolveItemValueEv
  ) : Pointer; stdcall;

  TPlcPluginResolveDeviceEv = function (
                         LibH : HANDLE;
                         aName : PChar;
                         var ISize, OSize : Integer;
                         var IBuf : Pointer;
                         var OBuf : Pointer
  ) : Boolean; stdcall;


  TAbstractPlcPluginClass = class of TAbstractPlcPlugin;
  TAbstractPlcPlugin = class(TObject)
  protected
    FValue : Integer;
    RefCount: Integer;
    Host: IPlcHost;
  public
    constructor Create( Parameters : string;
                        ResolveDeviceEv : TPlcPluginResolveDeviceEv;
                        ResolveItemEv : TPlcPluginResolveItemEv); virtual;
    procedure Compile ( var Embedded : TEmbeddedData;
                        aReadEv : TPlcPluginReadEv;
                        ResolveItemEv : TPlcPluginResolveItemEv;
                        Write : Boolean);  virtual; abstract;
    procedure GatePre; virtual; abstract;
    function GatePost : Integer; virtual; abstract;
    function Read(var Embedded : TEmbeddedData) : Boolean; virtual; abstract;
    procedure Write(var Embedded : TEmbeddedData; Line, LastLine : Boolean); virtual; abstract;
    function GateName(var Embedded : TEmbeddedData) : string; virtual; abstract;
    function ItemProperties : string; virtual; abstract;
    property Value : Integer read FValue;
    procedure SetHost(AHost: IPlcHost); virtual;
    class procedure AddPlugin(PluginName : string; PluginClass : TAbstractPlcPluginClass);
    class function FindClass(PluginName : string) : TAbstractPlcPluginClass;
    class function Properties : string; virtual; abstract;
    class function PluginObjects : string;
    class procedure Install; virtual;
    class procedure FinalInstall; virtual;
    class procedure Shutdown; virtual;

    function QueryInterface(const IID: TGUID; out Obj): HResult; stdcall;
    function _AddRef : Integer; stdcall;
    function _Release: Integer; stdcall;
  end;


  procedure PlcPluginObjects(Buffer : PChar; MaxSize : Cardinal); stdcall;
  procedure PlcPluginProperties( ObjName : PChar;
                             Buffer : PChar;
                             MaxSize : Cardinal); stdcall;
  function PlcPluginCreate( ObjName, Parameters : PChar;
                                ResolveDeviceEv : TPlcPluginResolveDeviceEv;
                                ResolveItemEv : TPlcPluginResolveItemEv) : HANDLE; stdcall;
  procedure PlcPluginCompile(
                            LibH : HANDLE;
                            var Embedded : TEmbeddedData;
                            aReadEv : TPlcPluginReadEv;
                            ResolveItemEv : TPlcPluginResolveItemEv;
                            Write : Boolean

                          ); stdcall;

  procedure PlcPluginGatePre(LibH : HANDLE); stdcall;
  procedure PlcPluginGatePost(LibH : HANDLE); stdcall;
  function PlcPluginRead( LibH : HANDLE;
                        var Embedded : TEmbeddedData) : Boolean; stdcall;
  procedure PlcPluginWrite( LibH : HANDLE;
                          var Embedded : TEmbeddedData;
                          Line, LastLine : Boolean); stdcall;
  function PlcPluginValue(LibH : HANDLE) : Integer; stdcall;
  procedure PlcPluginItemProperties( LibH : HANDLE;
                                        Buffer : PChar;
                                        MaxSize : Integer); stdcall;
  procedure PlcPluginGateName(LibH : HANDLE; var Embedded : TEmbeddedData; GateName : PChar); stdcall;
  procedure PlcPluginDestroy(LibH : HANDLE); stdcall;
  procedure PlcPluginSetHost(LibH: Handle; Host: IPlcHost); stdcall;

exports
  // PlcPlugin
  PlcPluginObjects name 'PlcPluginObjects',
  PlcPluginProperties name 'PlcPluginProperties',
  PlcPluginCreate name 'PlcPluginCreate',
  PlcPluginCompile name 'PlcPluginCompile',
  PlcPluginGatePre name 'PlcPluginGatePre',
  PlcPluginGatePost name 'PlcPluginGatePost',
  PlcPluginRead name 'PlcPluginRead',
  PlcPluginWrite name 'PlcPluginWrite',
  PlcPluginValue name 'PlcPluginValue',
  PlcPluginItemProperties name 'PlcPluginItemProperties',
  PlcPluginGateName name 'PlcPluginGateName',
  PlcPluginDestroy name 'PlcPluginDestroy',
  PlcPluginSetHost;

implementation

uses Classes, SysUtils, PluginInterface;

var PluginList : TStringList;



procedure PlcPluginObjects(Buffer : PChar; MaxSize : Cardinal); stdcall;
begin
  StrPLCopy(Buffer, TAbstractPlcPlugin.PluginObjects, MaxSize);
end;

procedure PlcPluginProperties( ObjName : PChar;
                           Buffer : PChar;
                           MaxSize : Cardinal); stdcall;
var PClass : TAbstractPlcPluginClass;
begin
  PClass := TAbstractPlcPlugin.FindClass(ObjName);
  if PClass <> nil then begin
    StrPLCopy(Buffer, PChar(PClass.Properties), MaxSize);
  end;
  raise Exception.CreateFmt('Plugin not found [%s]', [ObjName]);
end;

function PlcPluginCreate( ObjName, Parameters : PChar;
                              ResolveDeviceEv : TPlcPluginResolveDeviceEv;
                              ResolveItemEv : TPlcPluginResolveItemEv) : HANDLE; stdcall;
var PClass : TAbstractPlcPluginClass;
begin
  PClass := TAbstractPlcPlugin.FindClass(ObjName);
  if PClass <> nil then begin
    Result := PClass.Create(Parameters, ResolveDeviceEv, ResolveItemEv);
    Exit;
  end;
  raise Exception.CreateFmt('Plugin not found [%s]', [ObjName]);
end;

procedure PlcPluginCompile(
                          LibH : HANDLE;
                          var Embedded : TEmbeddedData;
                          aReadEv : TPlcPluginReadEv;
                          ResolveItemEv : TPlcPluginResolveItemEv;
                          Write : Boolean

                        ); stdcall;
begin
  LibH.Compile(Embedded, aReadEv, ResolveItemEv, Write);
end;

procedure PlcPluginGatePre(LibH : HANDLE); stdcall;
begin
  LibH.GatePre;
end;

procedure PlcPluginGatePost(LibH : HANDLE); stdcall;
begin
  LibH.GatePost;
end;

function PlcPluginRead( LibH : HANDLE;
                      var Embedded : TEmbeddedData) : Boolean; stdcall;
begin
  Result := LibH.Read(Embedded);
end;

procedure PlcPluginWrite( LibH : HANDLE;
                        var Embedded : TEmbeddedData;
                        Line, LastLine : Boolean); stdcall;
begin
  LibH.Write(Embedded, Line, LastLine);
end;

function PlcPluginValue(LibH : HANDLE) : Integer; stdcall;
begin
  Result := LibH.Value;
end;

procedure PlcPluginItemProperties( LibH : HANDLE;
                                      Buffer : PChar;
                                      MaxSize : Integer); stdcall;
begin
  StrPLCopy(Buffer, LibH.ItemProperties, MaxSize);
end;

procedure PlcPluginGateName(LibH : HANDLE; var Embedded : TEmbeddedData; GateName : PChar); stdcall;
begin
  StrPLCopy(GateName, LibH.GateName(Embedded), 128);
end;

procedure PlcPluginDestroy(LibH : HANDLE); stdcall;
begin
  LibH.Free;
end;

procedure PlcPluginSetHost(LibH: Handle; Host: IPlcHost); stdcall;
begin
  LibH.SetHost(Host);
end;




constructor TAbstractPlcPlugin.Create( Parameters : string;
                        ResolveDeviceEv : TPlcPluginResolveDeviceEv;
                        ResolveItemEv : TPlcPluginResolveItemEv);
begin
  inherited Create;
end;

class procedure TAbstractPlcPlugin.AddPlugin(PluginName : string; PluginClass : TAbstractPlcPluginClass);
begin
  PluginList.AddObject(PluginName, Pointer(PluginClass));
end;

class function TAbstractPlcPlugin.FindClass(PluginName : string) : TAbstractPlcPluginClass;
var I : Integer;
begin
  for  I := 0 to PluginList.Count - 1 do begin
    if CompareText(PluginName, PluginList[I]) = 0 then begin
      Result := TAbstractPlcPluginClass(PluginList.Objects[I]);
      Exit;
    end;
  end;
  raise Exception.CreateFmt('Plugin Object not found [%s]', [PluginName]);
end;

class function TAbstractPlcPlugin.PluginObjects : string;
begin
  Result := PluginList.Text;
end;

class procedure TAbstractPlcPlugin.FinalInstall;
begin
end;

class procedure TAbstractPlcPlugin.Install;
begin
end;

class procedure TAbstractPlcPlugin.Shutdown;
begin
end;

function TAbstractPlcPlugin.QueryInterface(const IID: TGUID; out Obj): HResult;
const
  E_NOINTERFACE = HResult($80004002);
begin
  if GetInterface(IID, Obj) then Result := 0 else Result := E_NOINTERFACE;
end;

function TAbstractPlcPlugin._AddRef: Integer;
begin
  Inc(RefCount);
  Result := RefCount;
end;

function TAbstractPlcPlugin._Release: Integer;
begin
 Dec(RefCount);
 Result:= RefCount;
end;

procedure TAbstractPlcPlugin.SetHost(AHost: IPlcHost);
begin
  Host:= AHost;
end;

initialization
  PluginList := TStringList.Create;
end.
