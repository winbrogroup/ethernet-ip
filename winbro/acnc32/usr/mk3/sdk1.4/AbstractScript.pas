unit AbstractScript;

interface

uses SysUtils, Classes, IScriptInterface, INamedInterface;

type
  TDefaultScriptLibrary = class;

  TScriptMethodClass = class of TScriptMethod;
  TScriptMethod = class(TInterfacedObject, IScriptMethod)
  private
    FHost: IScriptHost;
  public
    constructor Create; virtual;
    procedure Reset; virtual; stdcall;
    function Name: WideString; virtual; stdcall;
    function IsFunction: Boolean; virtual; stdcall;
    function ParameterPassing(Index: Integer): Integer; virtual; stdcall;
    function ParameterType(Index: Integer): Integer; virtual; stdcall;
    function ParameterName(Index: Integer): WideString; virtual; stdcall;
    function ReturnType: Integer; virtual; stdcall;
    function ParameterCount: Integer; virtual; stdcall;
    procedure Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer); virtual; stdcall;
    procedure SetScriptHost(Host: IScriptHost); stdcall;
    function GetScriptHost: IScriptHost; stdcall;
    property Host: IScriptHost read GetScriptHost write SetScriptHost;
  end;

  TDefaultScriptLibrary = class(TInterfacedObject, IScriptLibrary)
  private
    InstanceMethods: array of IScriptMethod;
    FHost: IScriptHost;
  public
    constructor Create;
    procedure Reset; stdcall;
    function MethodCount: Integer; stdcall;
    function GetMethodByIndex(Index: Integer): IScriptMethod; stdcall;
    function GetMethodByName(AName: WideString): IScriptMethod; stdcall;
    function GetLibraryName: WideString; stdcall;
    procedure SetScriptHost(Host: IScriptHost); stdcall;
    property Host: IScriptHost read FHost;
    class procedure AddMethod(Method: TScriptMethodClass);
    class procedure SetName(AName: WideString);
  end;

function GetScriptLibrary: IScriptLibrary; stdcall;

exports
  GetScriptLibrary;

implementation

var
  Methods: TList;
  LibName: WideString;

function GetScriptLibrary: IScriptLibrary;
begin
  Result:= TDefaultScriptLibrary.Create;
end;

{ TDefaultScriptLibrary }

class procedure TDefaultScriptLibrary.AddMethod(Method: TScriptMethodClass);
begin
  Methods.Add(Method);
end;

constructor TDefaultScriptLibrary.Create;
var I: Integer;
    M: TScriptMethod;
begin
  SetLength(InstanceMethods, Methods.Count);
  for I:= 0 to Methods.Count - 1 do begin
    M:= TScriptMethodClass(Methods[I]).Create;
    InstanceMethods[I]:= M;
  end;
end;

function TDefaultScriptLibrary.GetMethodByIndex(Index: Integer): IScriptMethod;
begin
  if Index < Length(InstanceMethods) then begin
    Result:= InstanceMethods[Index];
  end else
    raise Exception.Create('Method index too large');
end;

function TDefaultScriptLibrary.GetMethodByName(AName: WideString): IScriptMethod;
var I: Integer;
begin
  for I:= 0 to Length(InstanceMethods) - 1 do begin
    if UpperCase(AName) = UpperCase(InstanceMethods[I].Name) then begin
      Result:= InstanceMethods[I];
      Exit;
    end;
  end;
  Result:= nil;
end;

function TDefaultScriptLibrary.MethodCount: Integer;
begin
  Result:= Methods.Count;
end;

function TDefaultScriptLibrary.GetLibraryName: WideString;
begin
  Result:= LibName;
end;

procedure TDefaultScriptLibrary.Reset;
var I: Integer;
begin
  for I:= 0 to MethodCount - 1 do begin
    InstanceMethods[I].Reset;
  end;
end;

class procedure TDefaultScriptLibrary.SetName(AName: WideString);
begin
  LibName:= UpperCase(AName);
end;

procedure TDefaultScriptLibrary.SetScriptHost(Host: IScriptHost);
var I: Integer;
begin
  FHost:= Host;
  for I:= 0 to Length(InstanceMethods) - 1 do begin
    InstanceMethods[I].SetScriptHost(Host);
  end;
end;

{ TScriptMethod }

constructor TScriptMethod.Create;
begin
end;

procedure TScriptMethod.Execute(const Res: Pointer; const Params: array of Pointer;
  ParamCount: Integer);
begin
end;

function TScriptMethod.GetScriptHost: IScriptHost;
begin
  Result:= FHost;
end;

function TScriptMethod.IsFunction: Boolean;
begin
  Result:= False;
end;

function TScriptMethod.Name: WideString;
begin
  Result:= 'NotSet';
end;

function TScriptMethod.ParameterCount: Integer;
begin
  Result:= 0;
end;

function TScriptMethod.ParameterName(Index: Integer): WideString;
begin
  Result:= 'Parm' + IntToStr(Index);
end;

function TScriptMethod.ParameterPassing(Index: Integer): Integer;
begin
  Result:= PassAsConst;
end;

function TScriptMethod.ParameterType(Index: Integer): Integer;
begin
  Result:= dtString;
end;

procedure TScriptMethod.Reset;
begin
end;

function TScriptMethod.ReturnType: Integer;
begin
  Result:= dtString;
end;

procedure TScriptMethod.SetScriptHost(Host: IScriptHost);
begin
  FHost:= Host;
end;

initialization
  Methods := TList.Create;
end.
