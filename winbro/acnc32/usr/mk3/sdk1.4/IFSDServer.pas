unit IFSDServer;

interface

uses Windows;

const
  HttpHelpSection = 0;
  HttpMaintenanceSection = 1;

type
  IHTTPServerComponent = interface;

  IHTTPServer = interface
    ['{3AB47E5E-218F-4992-8233-616A87A65F3D}']
    function DataStream(Handle : THandle; Text : WideString) : Boolean; stdcall;
    procedure SetActive(aEn : Boolean); stdcall;
    function GetActive : Boolean; stdcall;
    procedure Shutdown; stdcall;
  end;

  IHTTPServerHost = interface
    ['{FAD589FF-C664-4504-90B4-C17786FCB5A7}']
    procedure ProcessCGI(HFile : THANDLE; CGIPath, CGIRequest : WideString); stdcall;
    function AddStreaming(Stream : THandle; CGIPath, CGIRequest : WideString; DataStream : IHTTPServer; DT : Integer) : Boolean; stdcall;
    procedure RemoveStreaming(Stream : THandle; CGIPath, CGIRequest : WideString); stdcall;
    procedure SystemTokenValue(const aToken : WideString; out Res : WideString); stdcall;
    procedure AddComponentPath(Section: Integer; const Path, Description: WideString; Comp: IHTTPServerComponent); stdcall;
  end;

  ICGIStringList = interface
    ['{29381A5E-7716-495E-826C-32664C402D6C}']
    function GetValue(Index: WideString): WideString; stdcall;
  end;

  IHTTPServerComponent = interface
    ['{1D0AA744-F60C-4AE8-BE0A-CB6B5BA14333}']
    function HTTPGeneratePage(HFile : THANDLE; var CGIPath : WideString; CGIRequest: ICGIStringList) : Boolean; stdcall;
    function HTTPAddStreaming(Stream : THandle; CGIR : WideString; Method : IHTTPServer; DT : Integer) : Boolean; stdcall;
    procedure HTTPRemoveStreaming(Stream : THandle; CGIR : WideString); stdcall;
  end;

  TGetFSDWebServer2 = function(Host : IHTTPServerHost) : IHTTPServer; stdcall;
  TGeneralPluginIOSweep = procedure (IOSweep : PInt64); stdcall;
  TGeneralPluginFontName = procedure (FontName : PChar); stdcall;
  TGeneralPluginSetProfilePath = procedure(ProfilePath : PChar); stdcall;
  TGeneralPluginSetHttpServerHost = procedure(Host: IHTTPServerHost); stdcall;

implementation

end.
