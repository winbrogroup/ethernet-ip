unit AbstractNCOperatorDialogue;

interface

uses SysUtils, Classes, INCInterface;

type
  TAbstractNCOperatorDialogueClass = class of TAbstractNCOperatorDialogue;
  TAbstractNCOperatorDialogue = class
  public
    class function Execute(Item : string) : TNCDialogueResult; virtual; abstract;
    class procedure Close; virtual; abstract;
    class procedure AddPlugin(aName : string; aClass : TAbstractNCOperatorDialogueClass);
    class function PluginObjects : string;
    class function FindClass(aName : string) : TAbstractNCOperatorDialogueClass;
    class procedure CloseAll;
  end;

implementation

var NCDialogueList: TStringList;

{ TAbstractNCOperatorDialogue }

class procedure TAbstractNCOperatorDialogue.AddPlugin(aName: string;
  aClass: TAbstractNCOperatorDialogueClass);
begin
  NCDialogueList.AddObject(Uppercase(aName), Pointer(aClass));
end;

class procedure TAbstractNCOperatorDialogue.CloseAll;
var I : Integer;
begin
  for I := 0 to NCDialogueList.Count - 1 do begin
    TAbstractNCOperatorDialogueClass(NCDialogueList.Objects[I]).Close;
  end;
end;

class function TAbstractNCOperatorDialogue.FindClass(
  aName: string): TAbstractNCOperatorDialogueClass;
var I : Integer;
begin
  Result := nil;
  I := NCDialogueList.IndexOf(Uppercase(aName));
  if I <> -1 then
    Result := TAbstractNCOperatorDialogueClass(NCDialogueList.Objects[I]);
end;

class function TAbstractNCOperatorDialogue.PluginObjects: string;
begin
  Result := NCDialogueList.Text;
end;

initialization
  NCDialogueList := TStringList.Create;
end.
