unit IScriptInterface;

interface

uses INamedInterface, Classes;


const
      PassByValue = 0;
      PassByReference = 1;
      PassAsConst = 2;

      dtInteger = 0;
      dtDouble = 1;
      dtString = 2;
      dtBoolean = 3;
      dtArray = 4;
      dtVariant = 5;
      dtUnknown = 6;

type
  IScriptHost = interface
  ['{811657E1-F27C-457C-8911-473A006C46B1}']
    procedure SetTransferData(Row: Integer; Col: Integer; Value: Double);
    function GetTransferData(Row: Integer; Col: Integer): Double;
  end;

  IScriptMethod = interface
  ['{5BE08288-CB24-4142-9A48-423B291E7951}']
    function Name: WideString; stdcall;
    procedure Reset; stdcall;
    function IsFunction: Boolean; stdcall;
    function ParameterPassing(Index: Integer): Integer; stdcall;
    function ParameterType(Index: Integer): Integer; stdcall;
    function ParameterName(Index: Integer): WideString; stdcall;
    function ReturnType: Integer; stdcall;
    function ParameterCount: Integer; stdcall;
    procedure Execute(const Res: Pointer; const Params: array of Pointer; ParamCount: Integer); stdcall;
    procedure SetScriptHost(Host: IScriptHost); stdcall;
    function GetScriptHost: IScriptHost; stdcall;
  end;

  IScriptLibrary = interface
  ['{B8C03D84-EE53-4354-AEB5-8C0B452369F7}']
    procedure Reset; stdcall;
    function MethodCount: Integer; stdcall;
    function GetMethodByIndex(Index: Integer): IScriptMethod; stdcall;
    function GetMethodByName(Name: WideString): IScriptMethod; stdcall;
    function GetLibraryName: WideString; stdcall;
    procedure SetScriptHost(Host: IScriptHost); stdcall;
  end;


type
  TGetScriptLibrary = function: IScriptLibrary; stdcall;

implementation

end.
