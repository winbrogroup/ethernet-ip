unit AbstractPSD;

interface

uses
  SysUtils, Classes, ProcessEditor, XmlIntf, IStringArray, CNCTypes, XmlDom, NamedPlugin;

type
  TAbstractPSDClass = class of TAbstractPSD;

  TAbstractPSD = class(TInterfacedObject, IPSDEditor)
  private
  protected
    FDisplayName: string;
    FUniqueName: string;
    FCapability: string;
    FCanMultiAssign : string;
    FFeatureBased : Boolean;
    FMustPostUsingModdedVectors : Boolean;

  public
    constructor Create(const UniqueName: string); virtual; abstract;

    function GetRootNode(Document: IXmlDocument): IXmlNode;
    procedure SetRootNode(Document: IXmlDocument; NewRoot: IXmlNode);

    //function QueryInterface(const IID: TGUID; out Obj): HResult; stdcall;
    //function _AddRef : Integer; stdcall;
    //function _Release : Integer; stdcall;

    function ShowModal(Host: IPSDHost; const Reference: WideString; X, Y: Integer; Title: WideString): WideString; virtual; stdcall;
    function GetMustPostUsingModdedVectors: Boolean; virtual; stdcall;
    function GetCapability: WideString; virtual; stdcall;
    function GetUniqueName: WideString; virtual; stdcall;
    function GetDisplayName: WideString; virtual; stdcall;
    property DisplayName: WideString read GetDisplayName;
    property UniqueName: WideString read GetUniqueName;
    procedure BeginPost(Host: IPSDHost); virtual; stdcall;
    procedure EndPost; virtual; stdcall;
    procedure SetReference(const Reference: WideString); virtual; stdcall;
    function Resolve(const AToken: WideString; out Res: WideString): Boolean; virtual; stdcall;
    function CanMultiAssign: boolean; virtual;stdcall;
    function IsFeatureBased: boolean; virtual; stdcall;
    procedure FirstFeature; virtual; stdcall;
    procedure NextFeature; virtual; stdcall;
    function ReAssert(Host: IPSDHost; const Reference: WideString; X, Y: Integer; Title: WideString): WideString; virtual; stdcall;
    function ReAssertRequired : Boolean; virtual; stdcall;

    procedure SetUniqueName(AUniqueName: WideString);
    procedure SetDisplayName(ADisplayName: WideString);
    property MustHaveModdedVectors : Boolean read GetMustPostUsingModdedVectors write FMustPostUsingModdedVectors;

    class procedure AddEditor(const UniqueName: string; clazz: TAbstractPSDClass);
    class function GetEditor(Index: Integer): IPSDEditor;
    class function EditorCount: Integer;
  end;

  function PSD_PluginCount: Integer; stdcall;
  function PSD_GetPlugin(Index: Integer): IPSDEditor; stdcall;
  { Called before PluginCount to allow generic editors configure}

exports
  PSD_PluginCount,
  PSD_GetPlugin;

implementation

var Editors: TStringList;

{ TAbstractPSD }

function PSD_PluginCount: Integer; stdcall;
begin
  Result:= Editors.Count;
end;

function PSD_GetPlugin(Index: Integer): IPSDEditor; stdcall;
begin
  Result:= TAbstractPSD.GetEditor(Index);
end;

class procedure TAbstractPSD.AddEditor(const UniqueName: string; clazz: TAbstractPSDClass);
begin
  Editors.AddObject(UniqueName, Pointer(clazz));
end;

procedure TAbstractPSD.BeginPost(Host: IPSDHost);
begin

end;

class function TAbstractPSD.EditorCount: Integer;
begin
  Result:= Editors.Count;
end;

function TAbstractPSD.GetCapability: WideString;
begin
  Result:= FCapability;
end;

procedure TAbstractPSD.EndPost;
begin

end;

function TAbstractPSD.GetDisplayName: WideString;
begin
  Result:= FDisplayName;
end;

class function TAbstractPSD.GetEditor(Index: Integer): IPSDEditor;
var Tmp: TAbstractPSDClass;
begin
  try
    Tmp:= TAbstractPSDClass(Editors.Objects[Index]);
    Result:= Tmp.Create(Editors[Index]);
  except
    on E: Exception do begin
      Named.EventLog('PSDE Initialize failure: ' + E.Message);
      raise Exception(E.Message);
    end;
  end;
end;

{ General purpose routine to find the root node for this PSDE
  If its not found, a node is created }
function TAbstractPSD.GetRootNode(Document: IXmlDocument): IXmlNode;
var Root, Parameters: IXmlNode;
    NL, PNL: IXmlNodeList;
    I, J: Integer;
    foundParameters: Boolean;
begin
  Root:= Document.DocumentElement;
  NL:= Root.ChildNodes;
  foundParameters:= False;

  for I:= 0 to NL.Count -1 do begin
    Parameters:= NL[I];
    if Parameters.NodeName = 'parameters' then begin
      foundParameters:= True;
      PNL:= Parameters.ChildNodes;
      for J:= 0 to PNL.Count -1 do begin
        Result:= PNL[J];
        if Result.NodeName = UniqueName then
          Exit;
      end;
    end;
  end;

  if not foundParameters then
    Parameters:= Root.AddChild('parameters');

  Result:= Parameters.AddChild(UniqueName);
end;

procedure TAbstractPSD.SetRootNode(Document: IXmlDocument; NewRoot: IXmlNode);
var Root, Parameters: IXmlNode;
    NL: IXmlNodeList;
    I: Integer;
begin
  Root:= Document.DocumentElement;
  NL:= Root.ChildNodes;

  for I:= 0 to NL.Count -1 do begin
    Parameters:= NL[I];
    if Parameters.NodeName = 'parameters' then begin
      Parameters.ChildNodes.Delete(GetUniqueName);
      Parameters.ChildNodes.Add(NewRoot);
    end;
  end;
end;


function TAbstractPSD.GetUniqueName: WideString;
begin
  Result:= FUniqueName;
end;

function TAbstractPSD.Resolve(const AToken: WideString;
  out Res: WideString): Boolean;
begin
  raise Exception.Create('TAbstractPSD.Resolve should never be called');
end;

procedure TAbstractPSD.SetReference(const Reference: WideString);
begin
  raise Exception.Create('TAbstractPSD.SetReference should never be called');
end;

function TAbstractPSD.ShowModal(Host: IPSDHost; const Reference: WideString; X,
  Y: Integer; Title: WideString): WideString;
begin
  raise Exception.Create('TAbstractPSD.ShowModal should never be called');
end;

procedure TAbstractPSD.SetDisplayName(ADisplayName: WideString);
begin
  FDisplayName:= ADisplayName;
end;

procedure TAbstractPSD.SetUniqueName(AUniqueName: WideString);
begin
  FUniqueName:= AUniqueName;
end;

function TAbstractPSD.IsFeatureBased: boolean;
begin
  Result:= FFeatureBased;
end;

function TAbstractPSD.CanMultiAssign: Boolean;
begin
  result := False;
  if FCanMultiAssign <> '' then
    begin
    Result:= (FCanMultiAssign[1] = 't') or (FCanMultiAssign[1] = 'T') 
    end;

end;

procedure TAbstractPSD.NextFeature;
begin
  raise Exception.Create('TAbstractPSD.NextFeature should never be called');
end;

procedure TAbstractPSD.FirstFeature;
begin
  raise Exception.Create('TAbstractPSD.FirstFeature should never be called');

end;


function TAbstractPSD.ReAssert(Host: IPSDHost; const Reference: WideString; X, Y: Integer; Title: WideString): WideString;
begin
  raise Exception.Create('TAbstractPSD.ReAssert should never be called');
end;



function TAbstractPSD.GetMustPostUsingModdedVectors: Boolean;
begin
  Result := False; // Overide in Custom PSDEs that may have modded vectors
end;

function TAbstractPSD.ReAssertRequired: Boolean;
begin
Result := False;
end;

initialization
  Editors:= TStringList.Create;
end.
