unit AbstractOCTPlugin;

interface

uses Classes, SysUtils, IOCT, IToolLife;

type
  TAbstractOCTPlugin = class;

  TAbstractOCTPluginClass = class of TAbstractOCTPlugin;
  TAbstractOCTPlugin = class(TObject, IFSDOCT)
  protected
    FLoadPath : WideString;
    RefCount : Integer;
    Host : IFSDOCTHost;
    ToolLife : IFSDToolLife;
    InstallName : WideString;
    function QueryInterface(const IID: TGUID; out Obj): HResult; stdcall;
    function _AddRef : Integer; stdcall;
    function _Release : Integer; stdcall;
    procedure SetInstallName(aInstallName : WideString); stdcall;
    function GetInstallName : WideString; stdcall;
  public
    constructor Create(Host : IFSDOCTHost); virtual;
    procedure LoadOCT(aFileName : WideString); virtual; stdcall;
    procedure Run; virtual; stdcall;
    procedure Reset; virtual; stdcall;
    procedure Update(Status : TOCTPluginStatii); virtual; stdcall;
    function GetLoadPath : WideString; stdcall;
    procedure SetLoadPath(Path : WideString); stdcall;
    procedure OPPEnable(Index : Integer; aOn : Boolean); virtual; stdcall;
//    procedure SetToolLife(aToolLife : IFSDToolLife); virtual; stdcall;
    procedure Sort(PreferedTool : WideString); virtual; stdcall;
    function GetToolUsage(Tool : WideString) : Double; virtual; stdcall;
    function GetMinimumToolUsage(Tool : WideString; out Value : Double) : Boolean; virtual; stdcall;

    class procedure AddPlugin(PluginName : string; PluginClass : TAbstractOCTPluginClass);
    class function FindPlugin(PluginName : string) : TAbstractOCTPluginClass;
    class function PluginObjects : string;
    procedure Install; virtual; stdcall;
    procedure FinalInstall; virtual; stdcall;
    procedure Shutdown; virtual; stdcall;
  end;


function GetFSDOCTInterface(Host : IFSDOCTHost; Name : WideString) : IFSDOCT; stdcall;

exports
  GetFSDOCTInterface;

implementation

var PluginList : TStringList;

{ TAbstractOCTPlugin }

function GetFSDOCTInterface(Host : IFSDOCTHost; Name : WideString) : IFSDOCT; stdcall;
var OCTClass : TAbstractOCTPluginClass;
begin
  OCTClass := TAbstractOCTPlugin.FindPlugin(Name);
  if OCTClass <> nil then
    Result := OCTClass.Create(Host)
  else
    Result := nil;
end;

function TAbstractOCTPlugin._AddRef: Integer;
begin
  Inc(RefCount);
  Result := RefCount;
end;

function TAbstractOCTPlugin._Release: Integer;
begin
  Dec(RefCount);
  Result := RefCount;
  if RefCount = 0 then begin
    Free;
  end;
end;


function TAbstractOCTPlugin.QueryInterface(const IID: TGUID;
  out Obj): HResult;
const
  E_NOINTERFACE = HResult($80004002);
begin
  if GetInterface(IID, Obj) then Result := 0 else Result := E_NOINTERFACE;
end;



class procedure TAbstractOCTPlugin.AddPlugin(PluginName: string;
  PluginClass: TAbstractOCTPluginClass);
begin
  PluginList.AddObject(LowerCase(PluginName), Pointer(PluginClass));
end;

constructor TAbstractOCTPlugin.Create(Host : IFSDOCTHost);
begin
  inherited Create;
  Self.Host := Host;
end;

procedure TAbstractOCTPlugin.FinalInstall;
begin
  ToolLife := Host.GetToolLifeInterface;
end;

procedure TAbstractOCTPlugin.Shutdown;
begin
end;

class function TAbstractOCTPlugin.FindPlugin(PluginName: string): TAbstractOCTPluginClass;
var I : Integer;
begin
  I := PluginList.IndexOf(LowerCase(PluginName));
  if I = -1 then
    raise Exception.CreateFmt('Cannot find plugin class "%s"', [PluginName]);

  Result := TAbstractOCTPluginClass(PluginList.Objects[I]);
end;

{procedure TAbstractOCTPlugin.SetToolLife(aToolLife : IFSDToolLife);
begin
  ToolLife := aToolLife;
end; }

function TAbstractOCTPlugin.GetInstallName: WideString;
begin
  Result := InstallName;
end;

procedure TAbstractOCTPlugin.Install;
begin
end;

procedure TAbstractOCTPlugin.LoadOCT(aFileName: WideString);
begin

end;

procedure TAbstractOCTPlugin.Sort;
begin
end;

function TAbstractOCTPlugin.GetLoadPath: WideString;
begin
  Result := FLoadPath;
end;

procedure TAbstractOCTPlugin.SetLoadPath(Path : WideString);
begin
  FLoadPath := Path;
end;

procedure TAbstractOCTPlugin.OPPEnable(Index: Integer; aOn: Boolean);
begin

end;

class function TAbstractOCTPlugin.PluginObjects: string;
begin
  Result := PluginList.Text;
end;

procedure TAbstractOCTPlugin.Reset;
begin

end;

procedure TAbstractOCTPlugin.Run;
begin

end;


procedure TAbstractOCTPlugin.SetInstallName(aInstallName: WideString);
begin
  InstallName := aInstallName;
end;

procedure TAbstractOCTPlugin.Update;
begin

end;

function TAbstractOCTPlugin.GetToolUsage(Tool: WideString): Double;
begin
  Result := 0;
end;

function TAbstractOCTPlugin.GetMinimumToolUsage(Tool: WideString;
  out Value: Double): Boolean;
begin
  Result := False;
end;

initialization
  PluginList := TStringList.Create;
//finalization
//  PluginList.Free;
end.
