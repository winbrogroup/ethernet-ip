unit IToolLife;

interface

uses INamedInterface;

type
  TToolResult = (
    trOK,
    trNotFound,
    trNotPermitted,
    trUnknown,
    trNotSupported,
    trAlreadyexists
  );

  IFSDToolLife = interface(IInstallablePlugin)
  ['{AA2C5600-8E5E-11D5-9F56-0800460222F0}']
    function AddTool(Tool : WideString; SN : Integer; Total, Current : Double; Ex1, Ex2 : Integer) : TToolResult; stdcall;
    function ReplenishTool(Tool : WideString; SN : Integer) : TToolResult; stdcall;
    function RemoveTool(Tool : WideString; SN : Integer) : TToolResult; stdcall;
    function GetTotal(Tool : WideString; out Total : Double) : TToolResult; stdcall;
    function SetRemaining(Tool : WideString; SN : Integer; Used : Double) : TToolResult; stdcall;
    function GetRemaining(Tool : WideString; SN : Integer; out Remaining : Double) : TToolResult; stdcall;
    function GetToolCount : Integer; stdcall;
    function GetToolType(Index : Integer; out ToolType : WideString) : TToolResult; stdcall;
    function GetToolSN(Index : Integer; out SN : Integer) : TToolResult; stdcall;
    function GetEx1ToolType(Ex1 : Integer; out ToolType : WideString) : TToolResult; stdcall;
    function GetEx1ToolSN(Ex1 : Integer; out SN : Integer) : TToolResult; stdcall;
    function ChangeEx1(Ex1Old, Ex1New : Integer) : TToolResult; stdcall;
    function ChangeEx2(Ex1, Ex2New : Integer) : TToolResult; stdcall;
    function GetExt2(Ex1 : Integer; out Ex2 : Integer) : TToolResult; stdcall;
    function GetMinimumTool(Tool : WideString; out Ex1 : Integer) : TToolResult; stdcall;
  end;

  IFSDToolLifeHost = interface
  ['{AA2C5601-8E5E-11D5-9F56-0800460222F0}']
    procedure ToolLifeChange(ToolLife : IFSDToolLife); stdcall;
  end;

  TGetFSDToolLifeInterface = function(Host : IFSDToolLifeHost; Name : PWideString) : IFSDToolLife; stdcall;
  TGeneralPluginSetToolLifeInterface = procedure(aToolLife : IFSDToolLife); stdcall;

const
  GetFSDToolLifeInterfaceName = 'GetFSDToolLifeInterface';
(*
  ['{AA2C560B-8E5E-11D5-9F56-0800460222F0}']
  ['{AA2C560C-8E5E-11D5-9F56-0800460222F0}']
  ['{AA2C560D-8E5E-11D5-9F56-0800460222F0}'] *)


implementation

end.
