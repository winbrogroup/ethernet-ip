unit AbstractGridPlugin;
{ Version 1.0.0
  Created FSD  24/12/2000
  Converted to GridPlugin Interface 31/08/2001
  }
interface

uses Classes, SysUtils, Windows, INamedInterface, IGridPlugin;

type
  TAbstractGridPluginClass = class of TAbstractGridPlugin;
  TAbstractGridPlugin = class(TObject, IFSDGridPlugin, IInstallablePlugin)
  private
    RefCount : Integer;
    procedure SetInstallName(aInstallName : WideString); stdcall;
    function GetInstallName : WideString; stdcall;
  protected
    InstallName : string;
    Host : IFSDGridPluginHost;
    function QueryInterface(const IID: TGUID; out Obj): HResult; stdcall;
    function _AddRef : Integer; stdcall;
    function _Release : Integer; stdcall;
  public
    constructor Create(aHost : IFSDGridPluginHost); virtual;
    destructor Destroy; override;
    procedure Update; virtual; stdcall;
    function CanEdit(X, Y : Integer) : Boolean; virtual; stdcall;
    procedure Edit(X, Y : Integer; var Req : WideString); virtual; stdcall;
    procedure DrawCell(aHDC : HDC; Rect : TRect; X, Y : Integer; State : TGPGridState); virtual; stdcall;
    procedure ComboSelect(aIndex : Integer); virtual; stdcall;
    procedure InPlaceEdit(X, Y : Integer); virtual; stdcall;
    class procedure AddPlugin(aName : string; aClass : TAbstractGridPluginClass);
    class function PluginObjects : string;
    class function FindPlugin(aName : string) : TAbstractGridPluginClass;
    procedure Install; virtual; stdcall;
    procedure FinalInstall; virtual; stdcall;
    procedure Shutdown; virtual; stdcall;
    procedure Nudge(X, Y : Integer; Value : Integer); virtual; stdcall;
  end;

function GetFSDGridPluginInterface(Host : IFSDGridPluginHost; Name : PWideString) : IFSDGridPlugin; stdcall;

exports
  GetFSDGridPluginInterface;

implementation

var
  PluginList : TStringList;

function GetFSDGridPluginInterface(Host : IFSDGridPluginHost; Name : PWideString) : IFSDGridPlugin; stdcall;
var
  GridPluginClass : TAbstractGridPluginClass;
begin
  GridPluginClass := TAbstractGridPlugin.FindPlugin(PChar(Name));
  if GridPluginClass <> nil then begin
    Result := GridPluginClass.Create(Host);
  end else
    Result := nil;
end;


constructor TAbstractGridPlugin.Create(aHost : IFSDGridPluginHost);
begin
  inherited Create;
  Host := aHost;
end;

destructor TAbstractGridPlugin.Destroy;
begin
  inherited Destroy;
end;

procedure TAbstractGridPlugin.Update;
begin
end;

function TAbstractGridPlugin.CanEdit(X, Y : Integer) : Boolean;
begin
  Result := False;
end;

procedure TAbstractGridPlugin.Edit(X, Y : Integer; var Req : WideString);
begin
end;

procedure TAbstractGridPlugin.DrawCell(aHDC : HDC; Rect : TRect; X, Y : Integer; State : TGPGridState);
begin
end;

procedure TAbstractGridPlugin.ComboSelect(aIndex : Integer);
begin
end;

procedure TAbstractGridPlugin.InPlaceEdit(X, Y : Integer);
begin
end;

class procedure TAbstractGridPlugin.AddPlugin(aName : string; aClass : TAbstractGridPluginClass);
begin
  if not Assigned(PluginList) then
    PluginList := TStringList.Create;

  PluginList.AddObject(aName, Pointer(aClass));
end;

class function TAbstractGridPlugin.PluginObjects : string;
begin
  if Assigned(PluginList) then
    Result := PluginList.Text
  else
    Result := '';
end;

class function TAbstractGridPlugin.FindPlugin(aName : string) : TAbstractGridPluginClass;
var I : Integer;
begin
  Result := nil;
  if not Assigned(PluginList) then
    Exit;
  for I := 0 to PluginList.Count - 1 do begin
    if CompareText(aName, PluginList[I]) = 0 then begin
      Result := TAbstractGridPluginClass(PluginList.Objects[I]);
      Exit;
    end;
  end;
end;

procedure TAbstractGridPlugin.FinalInstall;
begin
end;

procedure TAbstractGridPlugin.Install;
begin
end;

procedure TAbstractGridPlugin.Shutdown;
begin
end;

procedure TAbstractGridPlugin.Nudge(X, Y : Integer; Value : Integer);
begin
end;

function TAbstractGridPlugin._AddRef: Integer;
begin
  Inc(RefCount);
  Result := RefCount;
end;

function TAbstractGridPlugin._Release: Integer;
begin
  Dec(RefCount);
  Result := RefCount;
  if RefCount = 0 then begin
    Free;
  end;
end;

function TAbstractGridPlugin.GetInstallName: WideString;
begin
  Result := InstallName;
end;

function TAbstractGridPlugin.QueryInterface(const IID: TGUID;
  out Obj): HResult;
const
  E_NOINTERFACE = HResult($80004002);
begin
  if GetInterface(IID, Obj) then Result := 0 else Result := E_NOINTERFACE;
end;

procedure TAbstractGridPlugin.SetInstallName(aInstallName: WideString);
begin
  InstallName := aInstallName;
end;

end.
