unit AbstractIOSystemAccessor;

interface

uses AccessorPlugin, INamedInterface, Classes;

type
  TAbstractIOSystemAccessorClass = class of TAbstractIOSystemAccessor;
  TAbstractIOSystemAccessor = class(TInterfacedObject, IIOSystemAccessor)
  public
    constructor Create; virtual;
    function GetInfoPageContent(aPage : Integer) : WideString; virtual; stdcall; abstract;
    procedure Initialise(Host: IIOSystemAccessorHost; const Params : WideString); virtual; stdcall; abstract;
    procedure UpdateShadowToReal; virtual; stdcall; abstract;
    procedure UpdateRealToShadow; virtual; stdcall; abstract;
    function Healthy : Boolean; virtual; stdcall; abstract;
    procedure Reset; virtual; stdcall; abstract;
    function LastError : WideString; virtual; stdcall; abstract;
    function GetInputArea: Pointer; virtual; stdcall; abstract;
    function GetOutputArea: Pointer; virtual; stdcall; abstract;
    function GetInputSize: Integer; virtual; stdcall; abstract;
    function GetOutputSize: Integer; virtual; stdcall; abstract;
    function TextDefinition: WideString; virtual; stdcall; abstract;
    procedure Shutdown; virtual; stdcall; abstract;
    class procedure AddIOSystemAccessor(AName: string; Accessor: TAbstractIOSystemAccessorClass);
    class function GetIOSystemAccessor(AName: string): IIOSystemAccessor;
  end;

  function GetIOSystemAccessor(AName: WideString): IIOSystemAccessor; stdcall;

exports
  GetIOSystemAccessor;


implementation

function GetIOSystemAccessor(AName: WideString): IIOSystemAccessor; stdcall;
begin
  Result:= TAbstractIOSystemAccessor.GetIOSystemAccessor(AName);
{  if Result <> nil then begin
    Tmp:= Result.LastError;
  end; }
end;

{ TAbstractIOSystemAccessor }

var List: TStringList;

var Blog: Integer;

class function TAbstractIOSystemAccessor.GetIOSystemAccessor(
  AName: string): IIOSystemAccessor;
var I: Integer;
    IOClass: TAbstractIOSystemAccessorClass;
begin
  if Assigned(List) then begin
    I:= List.IndexOf(AName);
    if I <> -1 then begin
      IOClass:= TAbstractIOSystemAccessorClass(List.Objects[I]);
      Result:= IOClass.Create;
      Exit;
    end;

    Result:= nil;
  end
  else
    Result:= nil;
end;

class procedure TAbstractIOSystemAccessor.AddIOSystemAccessor(AName: string;
  Accessor: TAbstractIOSystemAccessorClass);
begin
  if not Assigned(List) then
    List:= TStringList.Create;

  List.AddObject(AName, Pointer(Accessor))
end;

constructor TAbstractIOSystemAccessor.Create;
begin
  // Dummy constructor for debug purposes
  Inc(Blog);
end;

end.
