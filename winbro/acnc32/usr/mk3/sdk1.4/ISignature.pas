unit ISignature;

interface

type
  ISignatureClient = interface;

  ICriterionDomain = interface
  ['{B20C53B3-C6F5-44E4-A04F-C71B3FBE5D0F}']
    function GetFieldLow: Integer; stdcall;
    procedure SetFieldLow(AFieldLow: Integer);  stdcall;
    function GetFieldHigh: Integer;  stdcall;
    procedure SetFieldHigh(AFieldHigh: Integer);  stdcall;
    function GetTimeLow: Double;  stdcall;
    procedure SetTimeLow(ATimeLow: Double);  stdcall;
    function GetTimeHigh: Double;  stdcall;
    procedure SetTimeHigh(ATimeHigh: Double);  stdcall;
    property FieldLow: Integer read GetFieldLow write SetFieldLow;
    property FieldHigh: Integer read GetFieldHigh write SetFieldHigh;
    property TimeLow: Double read GetTimeLow write SetTimeLow;
    property TimeHigh: Double read GetTimeHigh write SetTimeHigh;
  end;

  ISignatureHost = interface
  ['{12701255-FA23-4562-9D0D-78058B110C4B}']
    procedure AddClient(Client: ISignatureClient); stdcall;
    procedure RemoveClient(Client: ISignatureClient); stdcall;
  end;

  ISignatureClient = interface
  ['{786AE8CC-6AFE-4CF3-BA58-E65FA4EB9ABF}']
    procedure GateEnabled(Field: Integer; Domain: ICriterionDomain); stdcall;
  end;

implementation

end.
