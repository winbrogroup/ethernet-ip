unit PluginInterface;
{ Version 1.0.0
  Created FSD  24/12/2000 }

interface

uses AbstractPlcPlugin, AbstractOPPPlugin, AbstractGridPlugin, NamedPlugin,
     Windows, INamedInterface, Classes, AbstractNCOperatorDialogue,
     INCInterface, ProcessEditor, PSDPlugin;

//  You must use a function of the following prototype to override the default file load / save.
//  TGeneralPluginSelectProgram = function(ReqPath, RespPath : PChar; Load : Boolean) : Boolean; stdcall;
//  And then export it using exacly the following export name
//exports
//  GeneralPluginSelectProgram name 'GeneralPluginSelectProgram';
//
// NOTE
// The response buffer is only 1024bytes big.

  procedure NamedPluginPublish(A, B : Integer); stdcall;
  procedure NamedPluginRegister(A, B : Integer ); stdcall;
  procedure NamedPluginShutdown; stdcall;

  procedure OPPPluginEditorNames( Buffer : PChar;
                                  MaxSize : Integer); stdcall;
  function OPPPluginEditorExecute( SectionName : PChar;
                                   Buffer : PChar;
                                   MaxSize : Integer) : Boolean; stdcall;
  procedure OPPPluginConsumerNames( Buffer : PChar;
                                    MaxSize : Integer); stdcall;
  procedure OPPPluginConsume(SectionName, Buffer : PChar); stdcall;
  function OPPPluginConsumerIsDirty(SectionName : PChar) : Boolean; stdcall;
  procedure NCOperatorDialogueNames(var Buffer: WideString); stdcall;
  procedure NCOperatorDialogueClose; stdcall;
  function NCOperatorDialogueExecute(const Breed, Item: WideString): TNCDialogueResult; stdcall;
  procedure PSD_SetLibrary(Lib: IPSDELibrary); stdcall;

exports
  // NamedPlugin
  NamedPluginPublish name 'NamedPluginPublish',
  NamedPluginRegister name 'NamedPluginRegister',
  NamedPluginShutdown name 'NamedPluginShutdown',

  // OPPPlugin
  OPPPluginEditorNames name 'OPPPluginEditorNames',
  OPPPluginEditorExecute name 'OPPPluginEditorExecute',
  OPPPluginConsumerNames name 'OPPPluginConsumerNames',
  OPPPluginConsumerIsDirty name 'OPPPluginConsumerIsDirty',
  OPPPluginConsume name 'OPPPluginConsume',
  PSD_SetLibrary name PSD_SetLibrary_Name,
  NCOperatorDialogueNames,
  NCOperatorDialogueExecute,
  NCOperatorDialogueClose;

implementation

uses SysUtils;

// NamedPlugin
procedure NamedPluginPublish(
                 A, B : Integer); stdcall;
var S : TStringList;
    EditorClass : TAbstractOPPPluginEditorClass;
    Consumer : TAbstractOPPPluginConsumer;
    PlcClass : TAbstractPlcPluginClass;
    I : Integer;
begin
//  NamedPlugin.Publish(A, B); // Deprecated

  S := TStringList.Create;
  try
    S.Text := TAbstractOPPPluginEditorClass.PluginObjects;
    for I := 0 to S.Count - 1 do begin
      EditorClass := TAbstractOPPPluginEditorClass.FindClass(S[I]);
      if Assigned(EditorClass) then
        EditorClass.Install;
    end;

    S.Text := TAbstractOPPPluginConsumerClass.PluginObjects;
    for I := 0 to S.Count - 1 do begin
      Consumer := TAbstractOPPPluginConsumerClass.FindInstance(S[I]);
      if Assigned(Consumer) then
        Consumer.Install;
    end;

    S.Text := TAbstractPlcPluginClass.PluginObjects;
    for I := 0 to S.Count - 1 do begin
      PlcClass := TAbstractPlcPluginClass.FindClass(S[I]);
      if Assigned(PlcClass) then
        PlcClass.Install;
    end;
  finally
    S.Free;
  end;
end;



procedure NamedPluginRegister(
             A, B : Integer); stdcall;
var S : TStringList;
    EditorClass : TAbstractOPPPluginEditorClass;
    Consumer : TAbstractOPPPluginConsumer;
    PlcClass : TAbstractPlcPluginClass;
    I : Integer;
begin
//  NamedPlugin.Register(A, B); // Deprecated

  S := TStringList.Create;
  try
    S.Text := TAbstractOPPPluginEditorClass.PluginObjects;
    for I := 0 to S.Count - 1 do begin
      EditorClass := TAbstractOPPPluginEditorClass.FindClass(S[I]);
      if Assigned(EditorClass) then
        EditorClass.FinalInstall;
    end;

    S.Text := TAbstractOPPPluginConsumerClass.PluginObjects;
    for I := 0 to S.Count - 1 do begin
      Consumer := TAbstractOPPPluginConsumerClass.FindInstance(S[I]);
      if Assigned(Consumer) then
        Consumer.FinalInstall;
    end;

    S.Text := TAbstractPlcPluginClass.PluginObjects;
    for I := 0 to S.Count - 1 do begin
      PlcClass := TAbstractPlcPluginClass.FindClass(S[I]);
      if Assigned(PlcClass) then
        PlcClass.FinalInstall;
    end;
  finally
    S.Free;
  end;
end;

procedure NamedPluginShutdown; stdcall;
var S : TStringList;
    EditorClass : TAbstractOPPPluginEditorClass;
    Consumer : TAbstractOPPPluginConsumer;
    PlcClass : TAbstractPlcPluginClass;
    I : Integer;
begin
  S := TStringList.Create;
  try
    S.Text := TAbstractOPPPluginEditorClass.PluginObjects;
    for I := 0 to S.Count - 1 do begin
      EditorClass := TAbstractOPPPluginEditorClass.FindClass(S[I]);
      if Assigned(EditorClass) then
        EditorClass.Shutdown;
    end;

    S.Text := TAbstractOPPPluginConsumerClass.PluginObjects;
    for I := 0 to S.Count - 1 do begin
      Consumer := TAbstractOPPPluginConsumerClass.FindInstance(S[I]);
      if Assigned(Consumer) then
        Consumer.Shutdown;
    end;

    S.Text := TAbstractPlcPluginClass.PluginObjects;
    for I := 0 to S.Count - 1 do begin
      PlcClass := TAbstractPlcPluginClass.FindClass(S[I]);
      if Assigned(PlcClass) then
        PlcClass.Shutdown;
    end;
  finally
    S.Free;
  end;
end;


//OPPPluginEditor
procedure OPPPluginEditorNames( Buffer : PChar;
                                MaxSize : Integer); stdcall;
begin
  StrPLCopy(Buffer, TAbstractOPPPluginEditor.PluginObjects, MaxSize);
end;


function OPPPluginEditorExecute( SectionName : PChar;
                                 Buffer : PChar;
                                 MaxSize : Integer) : Boolean; stdcall;
var Tmp : string;
    Edit : TAbstractOPPPluginEditorClass;
begin
  Tmp := Buffer;
  Edit := TAbstractOPPPluginEditor.FindClass(SectionName);
  if Assigned(Edit) then begin
    Result := Edit.Execute(Tmp);
  end else
    raise Exception.CreateFmt('Editor for section %s does not exist in plugin', [SectionName]);
  if Result then
    StrPLCopy(Buffer, Tmp, MaxSize);
end;

//OPPPluginConsumer
procedure OPPPluginConsumerNames( Buffer : PChar;
                                  MaxSize : Integer); stdcall;
begin
  StrPLCopy(Buffer, TAbstractOPPPluginConsumer.PluginObjects, MaxSize);
end;

function OPPPluginConsumerIsDirty(SectionName : PChar) : Boolean; stdcall;
begin
  Result := TAbstractOPPPluginConsumer.IsDirty(SectionName);
end;

procedure OPPPluginConsume(SectionName, Buffer : PChar); stdcall;
begin
  TAbstractOPPPluginConsumer.DoConsume(SectionName, Buffer);
end;

//OperatorDialogues
procedure NCOperatorDialogueNames(var Buffer: WideString); stdcall;
begin
  Buffer := TAbstractNCOperatorDialogue.PluginObjects;
end;

function NCOperatorDialogueExecute(const Breed, Item: WideString): TNCDialogueResult; stdcall;
var NCD: TAbstractNCOperatorDialogueClass;
begin
  NCD := TAbstractNCOperatorDialogueClass.FindClass(Breed);
  Result := NCD.Execute(Item);
end;

procedure NCOperatorDialogueClose;
begin
  TAbstractNCOperatorDialogue.CloseAll;
end;

procedure PSD_SetLibrary(Lib: IPSDELibrary);
begin
  PSDELibrary:= Lib;
end;


end.
