unit AbstractExtGCode;

interface

uses Classes, INamedInterface;

type
  TAbstractNCExtGCodeClass = class of TAbstractNCExtGCode;
  TAbstractNCExtGCode = class(TObject, INCExtGcode)
  private
    RefCount: Integer;
  public
    procedure Compile(Compiler: INCCompiler; Line: WideString); virtual; stdcall;
    procedure Execute; virtual; stdcall;
    procedure Cleanup; virtual; stdcall;
    class procedure AddNCExtGCode(AName: WideString; ExtG: TAbstractNCExtGCodeClass);
    class function GetNCExtGCode(AName: WideString): INCExtGcode;
    function QueryInterface(const IID: TGUID; out Obj): HResult; stdcall;
    function _AddRef: Integer; stdcall;
    function _Release: Integer; stdcall;
  end;

function GetExtGCodes: WideString; stdcall;
function GetExtGCode(Pattern: WideString): INCExtGCode; stdcall;

exports
  GetExtGCodes,
  GetExtGCode;

implementation

var List: TStringList;

function GetExtGCodes: WideString; stdcall;
begin
  Result:= List.Text;
end;

function GetExtGCode(Pattern: WideString): INCExtGCode; stdcall;
begin
  Result:= TAbstractNCExtGCode.GetNCExtGCode(Pattern);
end;

{ TAbstractNCExtGCode }
class function TAbstractNCExtGCode.GetNCExtGCode(AName: WideString): INCExtGcode;
var I: Integer;
begin
  for I:= 0 to List.Count - 1 do begin
    if AName = List[i] then begin
      Result:= TAbstractNCExtGCodeClass(List.Objects[i]).Create;
      Exit;
    end;
  end;
  Result:= nil;
end;

class procedure TAbstractNCExtGCode.AddNCExtGCode(AName: WideString; ExtG: TAbstractNCExtGCodeClass);
begin
  if not Assigned(List) then
    List:= TStringList.Create;

  List.AddObject(AName, Pointer(ExtG));
end;

procedure TAbstractNCExtGCode.Compile(Compiler: INCCompiler; Line: WideString);
begin

end;

procedure TAbstractNCExtGCode.Execute;
begin

end;

procedure TAbstractNCExtGCode.Cleanup;
begin
  Self.Free;
end;

function TAbstractNCExtGCode._AddRef: Integer; stdcall;
begin
  Inc(RefCount);
  Result:= RefCount;
end;

function TAbstractNCExtGCode._Release: Integer; stdcall;
begin
  Dec(RefCount);
  Result:= 10;
end;

function TAbstractNCExtGCode.QueryInterface(const IID: TGUID; out Obj): HResult; stdcall;
begin
  if GetInterface(IID, Obj) then
    Result := 0
  else
    Result := E_NOINTERFACE;
end;

end.
