unit AbstractSecurityPlugin;

interface

uses Classes, SysUtils, ISecurity, INamedInterface, CNCTypes;

type
  TAbstractSecurityPluginClass = class of TAbstractSecurityPlugin;

  TAbstractSecurityPlugin = class(TObject, IFSDSecurity)
    RefCount : Integer;
    function QueryInterface(const IID: TGUID; out Obj): HResult; stdcall;
    function _AddRef : Integer; stdcall;
    function _Release : Integer; stdcall;
    procedure SetInstallName(aInstallName : WideString); stdcall;
    function GetInstallName : WideString; stdcall;
  protected
    InstallName : string;
    Host : IFSDSecurityHost;
  public
    constructor Create(aHost : IFSDSecurityHost; Parms : string); virtual;
    function LogOn(var UserName : WideString) : TAccessLevels; virtual; stdcall;
    procedure ForceSecurity(Access : TAccessLevels); virtual; stdcall;
    procedure Admin; virtual; stdcall;
    procedure LogOff; virtual; stdcall;
    function CurrentUser : WideString; virtual; stdcall;
    function CurrentAccess : TAccessLevels; virtual; stdcall;
    function Poll : Boolean; virtual; stdcall;


    class procedure AddPlugin(aName : string; aClass : TAbstractSecurityPluginClass);
    class function FindPlugin(aName : string) : TAbstractSecurityPluginClass;
    procedure Install; virtual; stdcall;
    procedure FinalInstall; virtual; stdcall;
    procedure Shutdown; virtual; stdcall;
  end;

function GetFSDSecurityInterface(
   Host : IFSDSecurityHost;
   Name : WideString;
   Parms : WideString ) : IFSDSecurity; stdcall;

exports
  GetFSDSecurityInterface;

implementation

var PluginList : TStringList;

function GetFSDSecurityInterface(
   Host : IFSDSecurityHost;
   Name : WideString;
   Parms : WideString ) : IFSDSecurity; stdcall;
var
  SecurityClass : TAbstractSecurityPluginClass;
begin
  SecurityClass := TAbstractSecurityPlugin.FindPlugin(Name);
  if SecurityClass <> nil then begin
    Result := SecurityClass.Create(Host, string(Parms));
  end else
    Result := nil;
end;

{ TAbstractSecurityPlugin }

constructor TAbstractSecurityPlugin.Create(aHost: IFSDSecurityHost; Parms : string);
begin
  Self.Host := Host;
end;

function TAbstractSecurityPlugin._AddRef: Integer;
begin
  Inc(RefCount);
  Result := RefCount;
end;

function TAbstractSecurityPlugin._Release: Integer;
begin
  Dec(RefCount);
  Result := RefCount;
  if RefCount = 0 then begin
    Free;
  end;
end;

class procedure TAbstractSecurityPlugin.AddPlugin(aName: string;
  aClass: TAbstractSecurityPluginClass);
begin
  PluginList.AddObject(aName, Pointer(aClass));
end;


procedure TAbstractSecurityPlugin.SetInstallName(aInstallName: WideString);
begin
  InstallName := aInstallName;
end;

class function TAbstractSecurityPlugin.FindPlugin(
  aName: string): TAbstractSecurityPluginClass;
var I : Integer;
begin
  for I := 0 to PluginList.Count - 1 do begin
    if CompareText(aName, PluginList[I]) = 0 then begin
      Result := TAbstractSecurityPluginClass(PluginList.Objects[I]);
      Exit;
    end;
  end;
  Result := nil;
end;

function TAbstractSecurityPlugin.GetInstallName: WideString;
begin
  Result := InstallName;
end;

procedure TAbstractSecurityPlugin.Install;
begin

end;

procedure TAbstractSecurityPlugin.Shutdown;
begin

end;







procedure TAbstractSecurityPlugin.Admin;
begin

end;

function TAbstractSecurityPlugin.LogOn(
  var UserName: WideString): TAccessLevels;
begin

end;

function TAbstractSecurityPlugin.CurrentAccess: TAccessLevels;
begin

end;

function TAbstractSecurityPlugin.Poll : Boolean;
begin
  Result := False;
end;

function TAbstractSecurityPlugin.CurrentUser: WideString;
begin

end;

procedure TAbstractSecurityPlugin.FinalInstall;
begin

end;

procedure TAbstractSecurityPlugin.ForceSecurity(Access: TAccessLevels);
begin

end;

procedure TAbstractSecurityPlugin.LogOff;
begin

end;

function TAbstractSecurityPlugin.QueryInterface(const IID: TGUID;
  out Obj): HResult;
const
  E_NOINTERFACE = HResult($80004002);
begin
  if GetInterface(IID, Obj) then Result := 0 else Result := E_NOINTERFACE;
end;



initialization
  Pluginlist := TStringList.Create;
end.
