unit IStringArray;

interface

const
    AAR_OK = 0;
    AAR_FAILED = -1;
    AAR_NOT_SUPPORTED = -2;
    AAR_NOT_FOUND = -3;
    AAR_ALREADY_EXISTS = -4;
    AAR_OUT_OF_BOUNDS = -5;

type
  TAA_RESULT = Integer;

  IACNC32StringArrayNotify = interface;

  IACNC32StringArray = interface
  ['{AA2C5608-8E5E-11D5-9F56-0800460222F0}']
    function FieldCount : Integer; stdcall;
    function RecordCount : Integer; stdcall;
    function Alias : WideString; stdcall;
    function FieldNameAtIndex(Index : Integer; out FieldName : WideString) : TAA_RESULT; stdcall;
    function AddField(FieldName : WideString; Client : IACNC32StringArrayNotify = nil) : TAA_RESULT; stdcall;
    function DeleteField(FieldName : WideString; Client : IACNC32StringArrayNotify = nil) : TAA_RESULT; stdcall;
    function FindRecord(Index, Col : Integer; Value : WideString; out Ptr : Integer) : TAA_RESULT; stdcall;
    function FindRecordByField(Index : Integer; FieldName, Value : WideString; out Ptr : Integer) : TAA_RESULT; stdcall;

    function GetFieldValue(Index : Integer; FieldName : WideString; out Value : WideString) : TAA_RESULT; stdcall;
    function SetFieldValue(Index : Integer; FieldName, Value : WideString; Client : IACNC32StringArrayNotify = nil) : TAA_RESULT; stdcall;
    function GetValue(Index, Col : Integer; out Value : WideString) : TAA_RESULT; stdcall;
    function SetValue(Index, Col : Integer; Value : WideString; Client : IACNC32StringArrayNotify = nil) : TAA_RESULT; stdcall;

    function Append(Count : Integer; Client : IACNC32StringArrayNotify = nil) : TAA_RESULT; stdcall;
    function Insert(Index : Integer; Client : IACNC32StringArrayNotify = nil) : TAA_RESULT; stdcall;
    function Delete(Index : Integer; Client : IACNC32StringArrayNotify = nil) : TAA_RESULT; stdcall;

    function Swap(A, B : Integer; Client : IACNC32StringArrayNotify = nil) : TAA_RESULT; stdcall;
    function Copy(Dest, Source : Integer; Client : IACNC32StringArrayNotify = nil) : TAA_RESULT; stdcall;

    procedure BeginUpdate; stdcall;
    procedure EndUpdate(const Client : IACNC32StringArrayNotify = nil); stdcall;
    procedure Clear(Client : IACNC32StringArrayNotify = nil); stdcall;

    function ExportToCSV(FileName : WideString) : TAA_RESULT; stdcall;
    function ImportFromCSV(FileName : WideString; Append : Boolean; Client : IACNC32StringArrayNotify = nil) : TAA_RESULT; stdcall;
  end;

  IACNC32StringArraySet = interface
  ['{AA2C5609-8E5E-11D5-9F56-0800460222F0}']
    function ArrayCount : Integer; stdcall;
    function AliasAtIndex(Index : Integer; out Alias : WideString) : TAA_RESULT; stdcall;
    function UseArray(Alias : WideString; Client : IACNC32StringArrayNotify; out Table : IACNC32StringArray) : TAA_RESULT; stdcall;
    function ReleaseArray(Client: IACNC32StringArrayNotify; Table : IACNC32StringArray) : TAA_RESULT; stdcall;
    function CreateArray(Alias: WideString; Client: IACNC32StringArrayNotify; out Table : IACNC32StringArray): TAA_RESULT;
    function DeleteArray(Alias : WideString) : TAA_RESULT;
  end;

  IACNC32StringArrayNotify = interface
  ['{AA2C560A-8E5E-11D5-9F56-0800460222F0}']
    procedure CellChange(Table : IACNC32StringArray; Row, Col : Integer); stdcall;
    procedure TableChange(Table : IACNC32StringArray); stdcall;
  end;

  IACNC32StringArrayNotifyA = interface;

  IACNC32StringArrayA = interface(IACNC32StringArray)
  ['{DD7C8CF6-CA1B-41FA-98AB-548680EEB54F}']
    function GetCursor: Integer; stdcall;
    procedure SetCursor(ACursor: Integer; const Client: IACNC32StringArrayNotifyA = nil); stdcall;
    property Cursor: Integer read GetCursor;
  end;

  IACNC32StringArrayNotifyA = interface(IACNC32StringArrayNotify)
  ['{B33A2E07-0CDD-4FC8-BEC6-9F1D44C4F25D}']
    procedure CursorChange(Table : IACNC32StringArrayA; Cursor: Integer); stdcall;
    function GetName: WideString; stdcall;
  end;

  IACNC32StringArraySetA = interface
  ['{A66E74AA-600B-48D9-9766-A66190501F9F}']
    function UseArrayA(Alias : WideString; Client : IACNC32StringArrayNotifyA; out Table : IACNC32StringArrayA) : TAA_RESULT; stdcall;
  end;

  TGeneralPluginSetStringArrayInterface = procedure (SA : IACNC32StringArraySet); stdcall;

implementation

end.
