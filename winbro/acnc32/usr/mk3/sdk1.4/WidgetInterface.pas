unit WidgetInterface;

interface

uses
  Classes,
  WinTypes,
  SysUtils,
  WidgetTypes,
  Graphics;

type
  TPluginWidgetClass = class of TPluginWidget;
  TPluginWidget = class(TObject)
  private
  protected
    Width : Integer;
    Height : Integer;
    HDC : THANDLE;
    FValue : Double;
    function TextBoxFont(aText : string; aRect : TRect; aFont : TFont) : TSize;
    function XYInRect(X, Y : Integer; Rect : TRect) : Boolean;
  public
    constructor Create; virtual;
    procedure Paint; virtual; abstract;
    function SetProperty(aToken, aValue : PChar) : Boolean; virtual;
    procedure MouseDown(Button: Integer; Shift: Byte; X, Y: Integer); virtual; abstract;
    procedure MouseUp(Button: Integer; Shift: Byte; X, Y: Integer); virtual; abstract;
    procedure SetCallback(aName : PChar; Callback : TPWEv); virtual; abstract;
    procedure SetBounds(aWidth, aHeight : Integer); virtual; abstract;
    procedure SetIntegerInput(const aName : string; aValue : Double); virtual; abstract;
    procedure SetStringInput(const aName, aValue : string); virtual;
    class procedure RegisterPanelMeterClass(aClass : TPluginWidgetClass; aName : string);
    class function FindPanelMeterClass(aName : string) : TPluginWidgetClass;
    class function GetProperties : PChar; virtual;
    property Value : Double read FValue;
  end;


  function ColorValue(ColStr : String) : TColor;
  function DoubleValue(aValue : string) : Double;
  function IntegerValue(aValue : string) : Integer;
  function BooleanValue(aValue : string) : Boolean;

function ACNCWidgetOpen(aName : PChar) : TPluginWidget; stdcall;
procedure ACNCWidgetClose(aHandle : TPluginWidget); stdcall;
procedure ACNCWidgetSetHDC(aHandle : TPluginWidget; aHDC : THandle); stdcall;
procedure ACNCWidgetSetBounds(aHandle : TPluginWidget; HDC : THANDLE; aWidth, aHeight : Integer); stdcall;
function ACNCWidgetSetProperty(aHandle : TPluginWidget; HDC : THANDLE; aToken, aValue : PChar) : Boolean; stdcall;
procedure ACNCWidgetPaint(aHandle : TPluginWidget; HDC : THANDLE); stdcall;
procedure ACNCWidgetMouseDown(aHandle : TPluginWidget; HDC : THANDLE; Button: Integer; Shift: Byte; X, Y: Integer); stdcall;
procedure ACNCWidgetMouseUp(aHandle : TPluginWidget; HDC : THANDLE; Button: Integer; Shift: Byte; X, Y: Integer); stdcall;
function ACNCWidgetSetIntegerInput(aHandle : TPluginWidget; aName : PChar; aValue : Integer) : Boolean; stdcall;
function ACNCWidgetSetStringInput(aHandle : TPluginWidget; aName, aValue : PChar) : Boolean; stdcall;
procedure ACNCWidgetSetCallback(aHandle : TPluginWidget; aName : PChar; Callback : TPWEv); stdcall;
function ACNCWidgetGetProperties(aHandle : TPluginWidget) : PChar; stdcall;

exports
  ACNCWidgetOpen name 'ACNCWidgetOpen',
  ACNCWidgetClose name 'ACNCWidgetClose',
  ACNCWidgetSetHDC name 'ACNCWidgetSetHDC',
  ACNCWidgetSetBounds name 'ACNCWidgetSetBounds',
  ACNCWidgetSetProperty name 'ACNCWidgetSetProperty',
  ACNCWidgetPaint name 'ACNCWidgetPaint',
  ACNCWidgetMouseDown name 'ACNCWidgetMouseDown',
  ACNCWidgetMouseUp name 'ACNCWidgetMouseUp',
  ACNCWidgetSetIntegerInput name 'ACNCWidgetSetIntegerInput',
  ACNCWidgetSetStringInput name 'ACNCWidgetSetStringInput',
  ACNCWidgetSetCallback name 'ACNCWidgetSetCallback',
  ACNCWidgetGetProperties name 'ACNCWidgetGetProperties';

implementation

var
  PanelMeterList : TStringList;

const
  CarRet = #$d + #$a;

function ACNCWidgetOpen(aName : PChar) : TPluginWidget; stdcall;
var aClass : TPluginWidgetClass;
begin
  aClass := TPluginWidget.FindPanelMeterClass(aName);
  if aClass <> nil then
    Result := aClass.Create
  else
    raise Exception.CreateFmt('Panel meter class [%s] not found', [aName]);
end;

procedure ACNCWidgetClose(aHandle : TPluginWidget); stdcall;
begin
  TPluginWidget(aHandle).Free;
end;

procedure ACNCWidgetSetHDC(aHandle : TPluginWidget; aHDC : THandle); stdcall;
begin
  aHandle.HDC := aHDC;
end;

procedure ACNCWidgetSetBounds(aHandle : TPluginWidget; HDC : THANDLE; aWidth, aHeight : Integer); stdcall;
begin
  aHandle.HDC := HDC;
  aHandle.SetBounds(aWidth, aHeight);
//  aHandle.Width := aWidth;
//  aHandle.Height := aHeight;
end;


function ACNCWidgetSetProperty(aHandle : TPluginWidget; HDC : THANDLE; aToken, aValue : PChar) : Boolean; stdcall;
begin
  aHandle.HDC := HDC;
  Result := aHandle.SetProperty(aToken, aValue);
end;

procedure ACNCWidgetPaint(aHandle : TPluginWidget; HDC : THANDLE); stdcall;
begin
  aHandle.HDC := HDC;
  aHandle.Paint;
end;

procedure ACNCWidgetMouseDown(aHandle : TPluginWidget; HDC : THANDLE; Button: Integer; Shift: Byte; X, Y: Integer); stdcall;
begin
  aHandle.HDC := HDC;
  aHandle.MouseDown(Button, Shift, X, Y);
end;

procedure ACNCWidgetMouseUp(aHandle : TPluginWidget; HDC : THANDLE; Button: Integer; Shift: Byte; X, Y: Integer); stdcall;
begin
  aHandle.HDC := HDC;
  aHandle.MouseUp(Button, Shift, X, Y);
end;

// Although this interface allows multiplexing of inputs, this implimentation does not support it.
function ACNCWidgetSetIntegerInput(aHandle : TPluginWidget; aName : PChar; aValue : Integer) : Boolean; stdcall;
begin
  aHandle.SetIntegerInput(aName, aValue);
  Result := True;
end;

function ACNCWidgetSetStringInput(aHandle : TPluginWidget; aName, aValue : PChar) : Boolean; stdcall;
begin
  aHandle.SetStringInput(aName, aValue);
  Result := True;
end;

procedure ACNCWidgetSetCallback(aHandle : TPluginWidget; aName : PChar; Callback : TPWEv); stdcall;
begin
  aHandle.SetCallback(aName, Callback);
end;

function ACNCWidgetGetProperties(aHandle : TPluginWidget) : PChar; stdcall;
begin
  Result := aHandle.GetProperties;
end;

  function IntegerValue(aValue : string) : Integer;
  var Code : Integer;
  begin
    Val(aValue, Result, Code);
  end;

  function DoubleValue(aValue : string) : Double;
  var Code : Integer;
  begin
    Val(aValue, Result, Code);
  end;

  function BooleanValue(aValue : string) : Boolean;
  begin
    if LowerCase(aValue) = 'true' then
      Result := True
    else begin
      Result := IntegerValue(aValue) <> 0;
    end;
  end;

  function ColorValue(ColStr : String) : TColor;
  begin
    if ColStr[1] = '$' then begin
      try
        Result := StrToInt(ColStr);
      except
        Result := clWhite;
      end;
      Exit;
    end;

    if LowerCase(Copy(ColStr,1, 2)) <> 'cl' then
      ColStr := 'cl' + ColStr;

    if not identtocolor(ColStr, Integer(Result)) then begin
      Result := clWhite;
    end;
  end;

function TPluginWidget.SetProperty(aToken, aValue : PChar) : Boolean;
begin
  Result := False;
end;

constructor TPluginWidget.Create;
begin
  inherited Create;
end;

function TPluginWidget.XYInRect(X, Y : Integer; Rect : TRect) : Boolean;
begin
  Result := (X > Rect.Left) and
            (X < Rect.Right) and
            (Y > Rect.Top) and
            (Y < Rect.Bottom);
end;

procedure TPluginWidget.SetStringInput(const aName, aValue : string);
begin
end;


function TPluginWidget.TextBoxFont(aText : string; aRect : TRect; aFont : TFont) : TSize;
var Retry : Integer;
begin
  Retry := 1;
  repeat
    Inc(Retry);
    aFont.Size := (aRect.Bottom - aRect.Top) div Retry;
    SelectObject(HDC, aFont.Handle);
    GetTextExtentPoint(HDC, PChar(aText), Length(aText), Result);
  until((Result.CX < aRect.Right - aRect.Left) and
        (Result.CY < aRect.Bottom - aRect.Top) or
        ((Retry - 2) >= (aRect.Bottom - aRect.Top)));
end;

class function TPluginWidget.GetProperties : PChar;
begin
  Result := '';
end;

class procedure TPluginWidget.RegisterPanelMeterClass(aClass : TPluginWidgetClass; aName : string);
begin
  PanelMeterList.AddObject(UpperCase(aName), Pointer(aClass));
end;

class function TPluginWidget.FindPanelMeterClass(aName : string) : TPluginWidgetClass;
var I : Integer;
begin
  I := PanelMeterList.IndexOf(aName);
  if I = -1 then
    Result := nil
  else
    Result := TPluginWidgetClass(PanelMeterList.Objects[I]);
end;

initialization
  PanelMeterList := TStringList.Create;
end.
