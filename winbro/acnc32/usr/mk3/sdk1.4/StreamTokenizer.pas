unit StreamTokenizer;
// Added fixed record read, this requires checking
interface

uses
  Classes, SysUtils, Graphics, Windows;

type
  EendOfFile = class(Exception);
  ETokenizerError = class(Exception);


  TStreamTokenizer = class (TObject)
  private
    Stream : TStream;
    FSeperator   : string;
    FFileName    : string;
    FLine        : Integer;
    FtokenPos    : Integer;
    FEndOfStream : Boolean;
    FBuffered : Boolean;
    LineComments : Boolean;
    FCommentChar : Char;
    FManageStream : Boolean;
    Defines: TStringList;
//    function GetCurrentLine : string;
    function IsWhiteSpace(const C : Char) : boolean;
    function IsSeperatorChar(C : Char) : Boolean;
    procedure SetLine(aLine : Integer);
    procedure PushChar;
    function GetWorking : string;
    procedure SetCommentChar(C : Char);
    procedure DoNextLine;
    procedure ConvertDefine(var Text: string);
  protected
    FPosition    : Integer;
    Working : string;
  public
    constructor Create;
    destructor Destroy; override;
    function Peek : Char;
    function  FindToken(aToken : string) : Boolean; virtual;
    function Read : string; virtual;
    procedure ReadChar(var C : Char);
    procedure Push;
    procedure Rewind;
    function  ReadLine : string;  // Reads the rest of the line as is.
    procedure ExpectedToken(aToken : string);
    procedure ExpectedTokens(const Args : array of string);
    function ReadInteger : Integer;
    function ReadBoolean : Boolean;
    function ReadDouble : Double;
    function ReadColor : TColor;
    function ReadIntegerAssign : Integer;
    function ReadStringAssign : string;
    function ReadBooleanAssign : Boolean;
    function ReadDoubleAssign : Double;
    function ReadColorAssign : TColor;
    procedure ReadList(S : TStrings);
    procedure ReadListAssign(S : TStrings);

    function ReadIndex(Max : Integer) : Integer;
    function ReadFixed(Length: Integer): string;
    procedure SetStream(aStream : TStream);
    function GetStream: TStream;
    procedure ReleaseStream;
    procedure AddDefine(const DefName, DefValue: string);
    property Seperator: string read FSeperator write FSeperator;
    property EndOfStream : boolean read FEndOfStream;
//    property CurrentLine : string read GetCurrentLine;
    property Line : Integer read FLine write SetLine;
    property TokenPos : Integer read FTokenPos;
    property LastToken : string read GetWorking;
    property Position : Integer read FPosition;
    property FileName : string read FFileName write FFilename;
    property Buffered : Boolean read FBuffered write FBuffered;
    property CommentChar : Char read FCommentChar write SetCommentChar;
    property ManageStream : Boolean read FManageStream write FManageStream;
  end;

  TStreamQuoteTokenizer = class(TStreamTokenizer)
  private
    FQuoteChar : Char;
    procedure SetQuoteChar(aChar : Char);
  public
    function Read : string; override;
    property QuoteChar : Char read FQuoteChar write SetQuoteChar;
  end;

implementation

constructor TStreamTokenizer.Create;
begin
  FManageStream := True;
  Buffered := False;
  Defines:= TStringList.Create;
  //Defines.Sorted:= True;
end;

destructor TStreamTokenizer.Destroy;
begin
  ReleaseStream;
  Defines.Free;
  inherited;
end;

function TStreamTokenizer.GetWorking : string;
begin
  Result := Working;
end;

function  TStreamTokenizer.FindToken(aToken : string) : Boolean;
begin
  Result := True;
  while not FEndOfStream do
    if AnsiCompareText(Read, aToken) = 0 then Exit;
  Result := False;
end;

procedure TStreamTokenizer.SetCommentChar(C : Char);
begin
  if C <> #$0 then
    LineComments := True
  else
    LineComments := False;

  FCommentChar := C;
end;

procedure TStreamTokenizer.SetLine(aLine : Integer);
var C : Char;
begin
  Rewind;
  FLine := 0;
  while not EndOfStream and (FLine <> aLine) do
    ReadChar(C);
end;

procedure TStreamTokenizer.DoNextLine;
var ThisLine : Integer;
    C : Char;
begin
  ThisLine := Line + 1;
  while not EndOfStream and (FLine <> ThisLine) do
    ReadChar(C);
end;


function TStreamTokenizer.Read : string;
var C : Char;
begin
  Working := '';
  try
    try
      FTokenPos   := Stream.Position;
      ReadChar(C);
      while not EndOfStream and IsWhiteSpace(C) do
        ReadChar(C);

      if IsSeperatorChar(C) then begin
        Working := C;
        Exit;
      end;

      if LineComments and (C = CommentChar) then begin
        DoNextLine;
        Result := Read;
      end;


      while not EndOfStream do begin
        Working := Working + C;
        ReadChar(C);
        if IsWhiteSpace(C) then
          Break;
        if IsSeperatorChar(C) then begin
          PushChar;
          Exit;
        end;
        if LineComments and (C = CommentChar) then begin
          DoNextLine;
          Exit;
        end;

      end;

      while not EndOfStream do begin
        ReadChar(C);
        if not IsWhiteSpace(C) then begin
          PushChar;
          Break;
        end;
      end;

    except
    end;
  finally
    ConvertDefine(Working);
    Result := Working;
  end;
end;

function TStreamTokenizer.ReadFixed(Length: Integer): string;
var I: Integer;
    Tmp: Char;
begin
  Result:= '';
  I:= 0;
  while (I < Length) and not EndOfStream do begin
    ReadChar(Tmp);
    if (Tmp <> #$0a) and (Tmp <> #$0d) then begin
      Result:= Result + Tmp;
      Inc(I);
    end;
  end;
  Working:= Result;
end;

function TStreamTokenizer.Peek : Char;
begin
  ReadChar(Result);
  PushChar;
end;

procedure TStreamTokenizer.Push;
begin
  Stream.Seek(FTokenPos, soFromBeginning);
end;

{function TStringTokenizer.GetCurrentLine : string;
begin
  Result := Strings[FLine];
end; }

procedure TStreamTokenizer.Rewind;
begin
  FEndOfStream := False;
  Stream.Seek(0, soFromBeginning);
end;

// Reads the rest of the line as is.
function  TStreamTokenizer.Readline : string;
var C : Char;
begin
  Result := '';

  while not EndOfStream do begin
    ReadChar(C);
    if (C <> #$0a) and (C <> #$0d) then
      Result := Result + C
    else
      Exit;
  end;
end;

function TStreamTokenizer.GetStream: TStream;
begin
  Result:= Stream;
end;

procedure TStreamTokenizer.SetStream(aStream : TStream);
begin
  FLine:= 0;
  ReleaseStream;
  if Buffered then begin
    Stream := TMemoryStream.Create;
    Stream.CopyFrom(aStream, aStream.Size);
    Stream.Seek(0, soFromBeginning);
    aStream.Free;
  end else begin
    Stream := aStream;
  end;
end;

procedure TStreamTokenizer.ReleaseStream;
begin
  if ManageStream then begin
    if Assigned(Stream) then
      Stream.Free;
  end;

  Stream := nil;
end;

procedure TStreamTokenizer.ReadChar(var C : Char);
begin
  if Stream.Read(C, 1) = 0 then begin
    FEndOfStream := True
  end else begin
    if C = #$0d then
      Inc(FLine);
  end;
end;

procedure TStreamTokenizer.PushChar;
begin
  Stream.Seek(Stream.Position - 1, soFromBeginning);
end;


function TStreamTokenizer.IsWhitespace(const C : Char) : Boolean;
begin
  Result := True;

  case C of
    '!'..'~' : Result := False;
  end;
end;

function TStreamTokenizer.IsSeperatorChar(C : Char) : Boolean;
var I : Integer;
begin
  for I := 1 to Length(Seperator) do begin
    if Seperator[I] = C then begin
      Result := True;
      Exit;
    end;
  end;
  Result := False;
end;

procedure TStreamTokenizer.ExpectedToken(aToken : string);
begin
  if CompareText(Read, aToken) <> 0 then
    raise Exception.CreateFmt('Unexpected token [%s] should be [%s]', [LastToken, aToken]);
end;

procedure TStreamTokenizer.ExpectedTokens(const Args : array of string);
var I : Integer;
begin
  for I := 0 to High(Args) do begin
    ExpectedToken(Args[I]);
  end;
end;

function TStreamTokenizer.ReadInteger : Integer;
begin
  Result := StrToInt(Read);
end;

function TStreamTokenizer.ReadIntegerAssign : Integer;
begin
  ExpectedToken('=');
  Result := ReadInteger;
end;

function TStreamTokenizer.ReadBoolean : Boolean;
begin
  if lowercase(Self.Read) = 'true' then
    Result := True
  else if lowercase(LastToken) = 'false' then
    Result := False
  else
    try
      Result := Round(StrToFloat(LastToken)) <> 0;
    except
      Result := False;
    end;
end;

function TStreamTokenizer.ReadDouble : Double;
begin
  Result := StrToFloat(Read);
end;

function TStreamTokenizer.ReadColor : TColor;
var TmpStr : string;
begin
  Result := 0;

  TmpStr := Read;

  if TmpStr = '' then
    Exit;

  if TmpStr[1] = '$' then begin
    Result := StrToInt(TmpStr);
  end else if LowerCase(Copy(TmpStr,1, 2)) = 'cl' then begin
    IdentToColor(TmpStr, Integer(Result));
  end else begin
    TmpStr := 'cl' + TmpStr;
    IdentToColor(TmpStr, Integer(Result));
  end;
end;

function TStreamTokenizer.ReadColorAssign : TColor;
begin
  ExpectedToken('=');
  Result := ReadColor;
end;

function TStreamTokenizer.ReadBooleanAssign : Boolean;
begin
  ExpectedToken('=');
  Result := ReadBoolean;
end;

function TStreamTokenizer.ReadDoubleAssign : Double;
begin
  ExpectedToken('=');
  Result := ReadDouble;
end;

function TStreamTokenizer.ReadStringAssign : string;
begin
  ExpectedToken('=');
  Result := Read;
end;

function TStreamTokenizer.ReadIndex(Max : Integer) : Integer;
begin
  ExpectedToken('[');
  Result := ReadInteger mod Max;
  ExpectedToken(']');
end;

procedure TStreamTokenizer.ReadListAssign(S : TStrings);
begin
  ExpectedToken('=');
  ReadList(S);
end;

procedure TStreamTokenizer.ReadList(S : TStrings);
var Token : string;
begin
  ExpectedToken('[');
  Token := Read;
  while not EndOfStream do begin
    if Token = ']' then begin
      Exit
    end else begin
      S.Add(Token);
      Token := Read;
      if Token = ']' then
        Exit
      else if Token <> ',' then
        raise Exception.Create('Unexpected token ''');
    end;
    Token := Read;
  end;
  raise Exception.Create('Unexpected end of file');
end;



procedure TStreamQuoteTokenizer.SetQuoteChar(aChar : Char);
begin
{  if Seperator[Length(Seperator)] = QuoteChar then
    FSeperator[Length(Seperator)] := aChar
  else
    Seperator := Seperator + aChar;
 }
  FQuoteChar := aChar;
end;

function TStreamQuoteTokenizer.Read : string;
var C : Char;
begin
  Working := '';
  try
    try
      ReadChar(C);
      while not EndOfStream and IsWhiteSpace(C) do
        ReadChar(C);

      FTokenPos   := Stream.Position - 1;

      if IsSeperatorChar(C) then begin
        Working := C;
        Exit;
      end;


      if C = QuoteChar then begin
        ReadChar(C);
        while not EndOfStream do begin
          if C = QuoteChar then
            Exit;
          Working := Working + C;
          ReadChar(C);
        end;
      end else if LineComments and (C = CommentChar) then begin
        DoNextLine;
        Result := Read;
      end else begin
        while not EndOfStream do begin
          Working := Working + C;
          ReadChar(C);
          if IsWhiteSpace(C) then
            Break;
          if IsSeperatorChar(C) then begin
            PushChar;
            Exit;
          end;
        end;
      end;

{      while not EndOfStream do begin
        ReadChar(C);
        if not IsWhiteSpace(C) then begin
          PushChar;
          Break;
        end;
      end; }
    except
    end;
  finally
    ConvertDefine(Working);
    Result := Working;
  end;
end;

procedure TStreamTokenizer.AddDefine(const DefName, DefValue: string);
begin
  Defines.Add(DefName);
  Defines.Values[DefName]:= DefValue;
end;

procedure TStreamTokenizer.ConvertDefine(var Text: string);
var I: Integer;
begin
  I:= Defines.IndexOf(Text);
  if I <> -1 then
    Text:= Defines.Values[Text];
end;

end.
