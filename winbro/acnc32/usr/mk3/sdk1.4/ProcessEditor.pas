unit ProcessEditor;

interface

uses XMLIntf, IStringArray, CNCTypes;
type

  IPSDHost = interface(IUnknown)
  ['{DB2F0C94-2D75-4FC4-971A-E208429403C7}']
    { Returns the currently active document root }
    function GetDocument: IXMLDocument; stdcall;

    { Returns the number of features in the currently selected set }
    function GetFeatureCount: Integer; stdcall;
    procedure PutModifiedFeatureCount(MFCount: Integer); stdcall;
    procedure PutModifiedFeatureatIndex(out OrgIndex: Integer;
                                        out NewIndex : Integer;
                                        out XYZ: TXYZ;
                                        out IJK: TXYZ;
                                        out ColorNumber : Integer); stdcall;

    { Gets the position and approach vector of the indexed feature }
    procedure GetFeature(Index: Integer; out XYZ: TXYZ; out IJK: TXYZ); stdcall;

    { General purpose interface that returns any other feature parameters at the
      specified index by name }
    function GetPropertyByName(Index: Integer; Name: string): WideString; stdcall;

    { Property methods for current tool }
    procedure SetCurrentTool(ATool: WideString); stdcall;
    function GetCurrentTool: WideString; stdcall;
    function GetIsModdedData: Boolean; stdcall;
    property CurrentTool: WideString read GetCurrentTool write SetCurrentTool;
    property IsModdedData : Boolean read GetIsModdedData;
  end;

  { All methods will throw an exception that must be caught by the host application
    When thrown the exception message will give a detailed description of
    the problem.

    As a general rule a form has no persistence, it merely acts upon the document
    when its presented }
  IPSDEditor = interface(IUnknown)
  ['{E678888C-0B86-4B3F-A5CE-2D4B167ED585}']
    { Shows a model editor based on the screen location X, Y with the specified
      title.

      The Host is an interface to the hosting application, the reference
      should be a valid reference to a current parameter set.  How the component
      deals with an invalid reference is not defined but should be graceful.

      As a note the function should only update the document when the user
      selects OK

      Returns True if the user has accepted the result, the value of reference
      guarenteed to be unchanged when the result is false}
    function ShowModal(Host: IPSDHost; const Reference: WideString; X, Y: Integer; Title: WideString): WideString; stdcall;

//      Does the same as show modal but without the show and using the current Reference
//    function Re_Assert(Host: IPSDHost; const Reference: WideString): WideString; stdcall;
    { Returns the transient Integer reference of Named parameter set }
    // REMOVED function GetIndexOfReference(Reference: WideString): Integer; stdcall;

    { Probably comma seperated values that should be checked against the
      host system }
    function GetCapability: WideString; stdcall;

    { Gets the parameter unique name.
      This name should be used for persistent storage of the editor is used.
      It is possible that two different editors use the same name, providing
      they interpret the data in the same way and only one is installed. }
    function GetUniqueName: WideString; stdcall;

    { Name used for menus etc. }
    function GetDisplayName: WideString; stdcall;

    { PublishData Creates the datatables}
    // REMOVED procedure PublishData(Document: IXMLDocument); stdcall;

    property DisplayName: WideString read GetDisplayName;
    property UniqueName: WideString read GetUniqueName;

    { Called before starting the post procedure to ensure the current data-set
      is upto date }
    procedure BeginPost(Host: IPSDHost); stdcall;

    { Called after post has completed, this allows the PSDE to free up resouces
      when sitting idle.}
    procedure EndPost; stdcall;

    { Updates the current values to use when resolve is called, if parameters
      set does not exist, this method will throw an exception }
    procedure SetReference(const Reference: WideString); stdcall;

    { Looks into the current name-space and returns the appropriate value for
      the specified Token, Returns True if the token is value.}
    function Resolve(const AToken: WideString; out Res: WideString): Boolean; stdcall;

    { Returns True if this PSDE works on features }
    function isFeatureBased: Boolean; stdcall;

    function CanMultiAssign: Boolean; stdcall;

    { This is used only by Custom Psdes and allows the implimentataion
      of per feature PSDE parameters within PSDEs}
    procedure FirstFeature; stdcall;

    { This is used only by Custom Psdes and allows the implimentataion
      of per feature PSDE parameters within PSDEs}
    procedure NextFeature ; stdcall;

    { This is used only by Custom Psdes and allows the recalculation
      followinf FDF or CLS reload}
    function ReAssert(Host: IPSDHost; const Reference: WideString; X, Y: Integer; Title: WideString): WideString; stdcall;
    function ReAssertRequired: Boolean; stdcall;

  end;


  IPSDELibrary = interface
  ['{1CF686D2-4DFD-4EE8-893D-50B4E7B1E36A}']
    function GetPSDE(Index: Integer): IPSDEditor; stdcall;
    function Count: Integer; stdcall;
    property PSDE[Index: Integer]: IPSDEditor read GetPSDE; default;
  end;

implementation

end.
