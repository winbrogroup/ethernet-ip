unit AbstractOPPPlugin;
{ Version 1.0.0
  Created FSD  24/12/2000 }

interface

uses Classes, NamedPlugin, INamedInterface;

type
  TAbstractOPPPluginEditorClass = class of TAbstractOPPPluginEditor;
  TAbstractOPPPluginEditor = class(TObject)
  public
    class function Execute(var Section : string) : Boolean; virtual; abstract;
    class procedure AddPlugin(aName : string; aClass : TAbstractOPPPluginEditorClass);
    class function PluginObjects : string;
    class function FindClass(aName : string) : TAbstractOPPPluginEditorClass;
    class procedure Install; virtual;
    class procedure FinalInstall; virtual;
    class procedure Shutdown; virtual;
  end;

  TOPPConsumerCallback = procedure(Sender : TObject) of Object;

  TAbstractOPPPluginConsumerClass = class of TAbstractOPPPluginConsumer;
  TAbstractOPPPluginConsumer = class(TObject)
  private
    Callbacks : array of TOPPConsumerCallback;
//    CallbackCount : Integer;
    FSectionName : string;
  protected
    procedure NotifyClients;
  public
    constructor Create(aSectionName : string); virtual;
    procedure Consume(aSectionName, Buffer : PChar); virtual;
    function Dirty : Boolean; virtual;
    class procedure AddCallback(const aSectionName : string; Callback : TOPPConsumerCallback);
    class procedure RemoveCallback(Callback : TOPPConsumerCallback);
    class procedure AddPlugin(const aSectionName : string; aClass : TAbstractOPPPluginConsumerClass);
    class function PluginObjects : string;
    class function FindInstance(const aSectionName : string) : TAbstractOPPPluginConsumer;
    class procedure DoConsume(aSectionName, Buffer : PChar);
    class function IsDirty(aSectionName : PChar) : Boolean;
    procedure Install; virtual;
    procedure FinalInstall; virtual;
    procedure Shutdown; virtual;
    property SectionName : string read FSectionName;
  end;


implementation

uses SysUtils;

var EditorList : TStringList;
    ConsumerList : TStringList;

class procedure TAbstractOPPPluginEditor.AddPlugin(aName : string; aClass : TAbstractOPPPluginEditorClass);
begin
  if not Assigned(EditorList) then
    EditorList := TStringList.Create;

  EditorList.AddObject(aName, Pointer(aClass));
end;

class function TAbstractOPPPluginEditor.PluginObjects : string;
begin
  if Assigned(EditorList) then
    Result := EditorList.Text
  else
    Result := '';
end;

class function TAbstractOPPPluginEditor.FindClass(aName : string) : TAbstractOPPPluginEditorClass;
var I : Integer;
begin
  Result := nil;
  if not Assigned(EditorList) then
    Exit;

  for I := 0 to EditorList.Count - 1 do begin
    if CompareText(aName, EditorList[I]) = 0 then begin
      Result := TAbstractOPPPluginEditorClass(EditorList.Objects[I]);
      Exit;
    end;
  end;
end;

class procedure TAbstractOPPPluginEditor.FinalInstall;
begin
end;

class procedure TAbstractOPPPluginEditor.Install;
begin
end;

class procedure TAbstractOPPPluginEditor.Shutdown;
begin
end;

class procedure TAbstractOPPPluginConsumer.AddPlugin(const aSectionName : string; aClass : TAbstractOPPPluginConsumerClass);
begin
  if not Assigned(ConsumerList) then
    ConsumerList := TStringList.Create;

  ConsumerList.AddObject(aSectionName, aClass.Create(aSectionName));
end;

class function TAbstractOPPPluginConsumer.PluginObjects : string;
begin
  if Assigned(ConsumerList) then
    Result := ConsumerList.Text
  else
    Result := '';
end;

class function TAbstractOPPPluginConsumer.FindInstance(const aSectionName : string) : TAbstractOPPPluginConsumer;
var I : Integer;
begin
  Result := nil;
  if not Assigned(ConsumerList) then
    Exit;

  for I := 0 to ConsumerList.Count - 1 do begin
    if CompareText(aSectionName, ConsumerList[I]) = 0 then begin
      Result := TAbstractOPPPluginConsumer(ConsumerList.Objects[I]);
      Exit;
    end;
  end;
end;

class procedure TAbstractOPPPluginConsumer.DoConsume(aSectionName, Buffer : PChar);
var I : Integer;
    OPPC : TAbstractOPPPluginConsumer;
begin
  if not Assigned(ConsumerList) then
    Exit;
  for I := 0 to ConsumerList.Count - 1 do begin
    OPPC := TAbstractOPPPluginConsumer(ConsumerList.Objects[I]);
    if CompareText(aSectionName, OPPC.SectionName) = 0 then
      OPPC.Consume(aSectionName, Buffer);
  end;
end;

class function TAbstractOPPPluginConsumer.IsDirty(aSectionName : PChar) : Boolean;
var I : Integer;
    OPPC : TAbstractOPPPluginConsumer;
begin
  Result := False;
  if not Assigned(ConsumerList) then
    Exit;
  for I := 0 to ConsumerList.Count - 1 do begin
    OPPC := TAbstractOPPPluginConsumer(ConsumerList.Objects[I]);
    if CompareText(aSectionName, OPPC.SectionName) = 0 then
      Result := OPPC.Dirty;
  end;
end;

class procedure TAbstractOPPPluginConsumer.AddCallback(const aSectionName : string; Callback : TOPPConsumerCallback);
var I, J : Integer;
    OPPC : TAbstractOPPPluginConsumer;
begin
  for I := 0 to ConsumerList.Count - 1 do begin
    OPPC := TAbstractOPPPluginConsumer(ConsumerList.Objects[I]);
    if CompareText(aSectionName, OPPC.SectionName) = 0 then begin
      for J := 0 to Length(OPPC.Callbacks) do begin
        if not Assigned(OPPC.Callbacks[J]) then begin
          OPPC.Callbacks[J] := Callback;
          Exit;
        end;
      end;
    end;
  end;
end;

class procedure TAbstractOPPPluginConsumer.RemoveCallback(Callback : TOPPConsumerCallback);
var I, J : Integer;
    OPPC : TAbstractOPPPluginConsumer;
begin
  for I := 0 to ConsumerList.Count - 1 do begin
    OPPC := TAbstractOPPPluginConsumer(ConsumerList.Objects[I]);
    for J := 0 to Length(OPPC.Callbacks) do begin
      if Addr(OPPC.Callbacks[J]) = Addr(Callback) then begin
        OPPC.Callbacks[J] := nil;
      end;
    end;
  end;
end;

constructor TAbstractOPPPluginConsumer.Create(aSectionName : string);
begin
  inherited Create;
  SetLength(Callbacks, 10);
//  CallbackCount := 0;
  FSectionName := aSectionName;
end;

procedure TAbstractOPPPluginConsumer.Consume(aSectionName, Buffer : PChar);
begin
end;

function TAbstractOPPPluginConsumer.Dirty : Boolean;
begin
  Result := False;
end;

procedure TAbstractOPPPluginConsumer.NotifyClients;
var I : Integer;
begin
  for I := 0 to Length(Callbacks) - 1 do begin
    if Assigned(Callbacks[I]) then
      Callbacks[I](Self);
  end;
end;

procedure TAbstractOPPPluginConsumer.FinalInstall;
begin
end;

procedure TAbstractOPPPluginConsumer.Install;
begin
end;

procedure TAbstractOPPPluginConsumer.Shutdown;
begin
end;

end.
