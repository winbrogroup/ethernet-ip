unit IPLC;

interface

type
  IPlcHost = interface
  ['{54F8843B-9DFF-4E86-93C3-3B93C397B8F7}']
    function GetItemValue(Item: Pointer): Integer; stdcall;
    procedure SetItemValue(Item: Pointer; Value: Integer); stdcall;
  end;

implementation

end.
