unit INamedInterface;

interface

uses CNCTypes, Windows, ISignature;

const
  SDKPluginVersion = $01041101;

type
  TTagRef = Pointer;

  TNamedCallbackEv = procedure (Tag : TTagRef); stdcall;
  TFaultResetStdcall = function(FID : FHandle) : Boolean; stdcall;

  INCParserResolver = interface
  ['{F646B128-1FF4-4F27-9479-DB642C46254F}']
    { Looks up the name specified by token if the function succeeds it returns
      True and set the substitution in value.  This method should return false
      on failure, not throw an exception}
    function Resolve(const Token: WideString; out Value: WideString): Boolean; stdcall;
  end;


  IConfigurationParser = interface
  ['{844715C2-6B55-4B05-B7EB-C80155100641}']
  { Parses SourceFile using the systems current configuration.
    Returns Filename of parsed file (in temp directory) it is the
      callers job to delete the file }
    function Parse(SourceFile: WideString): WideString; stdcall;

    { Allows the caller to specify the resolver to be used for varuables }
    procedure SetResolver(Resolver: INCParserResolver); stdcall;
  end;

  IInstallablePlugin = interface
  ['{3DEC6200-8BDE-11D5-9F56-0800460222F0}']
    procedure Install; stdcall;
    procedure FinalInstall; stdcall;
    procedure Shutdown; stdcall;
    procedure SetInstallName(aInstallName : WideString); stdcall;
    function GetInstallName : WideString; stdcall;
    property InstallName : WideString read GetInstallName write SetInstallName;
  end;

  IFSDNamedInterface = interface
  ['{7BCDA352-8D22-4980-872F-43E588493A08}']
    function AddTag(aName, aType : PChar) : TTagRef; stdcall;
    function AquireTag(aName, aType : PChar; Callback : TNamedCallbackEv) : TTagRef; stdcall;
    procedure GetDataType(aTag : TTagRef; aType : PChar; MaxLen : Integer);
    function GetAsInteger(aTag : TTagRef) : Integer; stdcall;
    function GetAsBoolean(aTag : TTagRef) : Boolean; stdcall;
    function GetAsDouble(aTag : TTagRef) : Double; stdcall;
    procedure GetAsString(aTag: TTagRef; Buffer : PChar; MaxLen : Integer); stdcall;
    function GetAsData(aTag : TTagRef; var Length : Integer) : Pointer; stdcall;
    function GetAsOleVariant(aTag : TTagRef) : OleVariant; stdcall;
    procedure SetAsInteger(aTag : TTagRef; Value : Integer); stdcall;
    procedure SetAsBoolean(aTag : TTagRef; Value : Boolean); stdcall;
    procedure SetAsDouble(aTag : TTagRef; Value : Double); stdcall;
    procedure SetAsString(aTag : TTagRef; Value : PChar); stdcall;
    procedure SetAsOleVariant(aTag : TTagRef; Value : OleVariant); stdcall;
    procedure ReleaseCallback(aTag : TTagRef; Callback : TNamedCallbackEv); stdcall;
    procedure SetFault(Name : PChar; Text : PChar; Level : TFaultLevel); stdcall;
    function  Heads : Integer; stdcall;
  end;

  IFSDNamedInterfaceA = interface(IFSDNamedInterface)
  ['{18E1837B-3254-46B0-8B3F-09F875837BBA}']
    procedure EventLog(const Msg : WideString; NtEvent : Word = EVENTLOG_INFORMATION_TYPE); stdcall;
    procedure MessageLog(const Msg : WideString); stdcall;
    function LoadOPP(const FileName : WideString) : Boolean; stdcall;
    function LoadOCT(const FileName : WideString) : Boolean; stdcall;
    procedure SetPersistent(aTag : TTagRef; aOn : Boolean); stdcall;
    function SetFaultA(Name, Text : WideString; Level : TFaultLevel; Reset : TFaultResetStdcall) : FHandle; stdcall;
    procedure FaultResetID(FID : FHandle); stdcall;
    procedure UpdateFault(FID : FHandle; Text : WideString); stdcall;
    procedure ResetInterlock; stdcall;

    function IsPersistent(aTag : TTagRef) : Boolean; stdcall;
    function IsVCL(aTag : TTagRef) : Boolean; stdcall;
    function IsSystem(aTag : TTagRef) : Boolean; stdcall;
    function IsWritable(aTag : TTagRef) : Boolean; stdcall;
    function IsOwned(aTag : TTagRef) : Boolean; stdcall;
    function IsConnected(aTag : TTagRef) : Boolean; stdcall;
    function IsEvented(aTag : TTagRef) : Boolean; stdcall;
    function IsIO(aTag : TTagRef) : Boolean; stdcall;

    procedure AddLanguageFile(const Filename: WideString); stdcall;
    function GetSignatureHost: ISignatureHost; stdcall;

    function GetConfigurationParser: IConfigurationParser;
  end;

  IIOSystemAccessorHost = interface
  ['{E1988AAC-876A-4CA5-BCE0-1F6345E8E5B9}']
    function AddPage(Name: WideString): Integer; stdcall;
  end;

  IIOSystemAccessor = interface
  ['{DA9E5A42-0A69-4838-BE4E-F4D1EE77B67C}']
    function GetInfoPageContent(aPage : Integer) : WideString; stdcall;
    procedure Initialise(Host: IIOSystemAccessorHost; const Params : WideString); stdcall;
    procedure UpdateShadowToReal; stdcall;
    procedure UpdateRealToShadow; stdcall;
    function Healthy : Boolean; stdcall;
    procedure Reset; stdcall;
    function LastError : WideString; stdcall;
    function GetInputArea: Pointer; stdcall;
    function GetOutputArea: Pointer; stdcall;
    function GetInputSize: Integer; stdcall;
    function GetOutputSize: Integer; stdcall;
    function TextDefinition: WideString; stdcall;
    procedure Shutdown; stdcall;
  end;

  INCCompiler = interface
  ['{E312303F-930A-4AB7-9529-58B3B4895F95}']
    function ResolveNCVariable(TagValue: WideString): TTagRef;
  end;

  INCExtGCode = interface
  ['{823837B3-4381-41A9-95A4-23563CB07385}']
    procedure Compile(Compiler: INCCompiler; Line: WideString); stdcall;
    procedure Execute; stdcall;
    procedure Cleanup; stdcall;
  end;

implementation

end.
