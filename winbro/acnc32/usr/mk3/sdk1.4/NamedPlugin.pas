unit NamedPlugin;
{ Version 1.0.0
  Created FSD  24/12/2000 }

interface

uses INamedInterface, IStringArray, INCInterface, SysUtils, IToolLife,
     IFSDServer, ProcessEditor, IniFiles, CncTypes, JNI;

var
  IOSweep : PInt64;
  FontName : string;
  DllProfilePath : string;
  DllLocalPath : string;
  DllMachSpecPath : string;
  DllLoggingPath: string;
  DllContSpecPath: string;

  Named : IFSDNamedInterfaceA;
  NC : IFSDNCInterface; // Note the NC will not be available until final install
  Heads : Integer;
  Stations : Integer;
  StringArraySet : IACNC32StringArraySet;
  ToolLife : IFSDToolLife;
  HTTPServerHost: IHTTPServerHost;
  PSDELibrary: IPSDELibrary;
  JNIEnv: PJNIEnv;

type
  TagPointer = Pointer;
  TPluginCallback = procedure(aHandle : Integer);

  TTranslator = class(TObject)
  private
    LanguageType : string;
    Table : IACNC32StringArray;
    Column : Integer;
  public
    constructor Create;
    destructor Destroy; override;
    function GetString(const aToken : string) : string;
  end;


function ACNC32PluginVersion : Cardinal; stdcall;
procedure NamedPluginSetNamedInterface(aNamedInterface : IFSDNamedInterfaceA); stdcall;
procedure NamedPluginSetNCInterface(aNCInterface : IFSDNCInterface); stdcall;
procedure GeneralPluginIOSweep(aIOSweep : PInt64); stdcall;
procedure GeneralPluginFontName(aFontName : PChar); stdcall;
procedure GeneralPluginSetProfilePath(aProfilePath : PChar); stdcall;
procedure GeneralPluginSetLocalPath(aLocalPath : PChar); stdcall;
procedure GeneralPluginSetMachSpecPath(aMachSpecPath : PChar); stdcall;
procedure GeneralPluginSetLoggingPath(aLoggingPath : PChar); stdcall;
procedure GeneralPluginSetContSpecPath(aContSpecPAth: PChar); stdcall;
procedure GeneralPluginSetStringArrayInterface(SA : IACNC32StringArraySet); stdcall;
procedure GeneralPluginSetToolLifeInterface(aToolLife : IFSDToolLife); stdcall;
procedure GeneralPluginSetHttpServerHost(AServerHost: IHTTPServerHost); stdcall;
procedure GeneralPluginSetJNIEnv(AJNIEnv: PJNIEnv); stdcall;

{ Returns an Ini file parsed by current machine }
function CustomiseConfigurationIni(Filename: string): TCustomIniFile;
{ Returns the file name of a configuration file parsed by current machine
  configuration, ensure the resultant file is deleted }
function CustomiseConfiguration(Filename: string): string;

function NamedGetAsString(ATag: TTagRef): string;

type
  TNamedPluginRegistration = record
    Name : string;
    DType : string;
    Tag : TagPointer;
    Write : Boolean;
    Evented : Boolean;
    Callback : TPluginCallback;
    Done : Boolean;
  end;
  PNamedPluginRegistration = ^TNamedPluginRegistration;


exports
  ACNC32PluginVersion,
  NamedPluginSetNamedInterface,
  NamedPluginSetNCInterface,
  GeneralPluginIOSweep name 'GeneralPluginIOSweep',
  GeneralPluginFontName name 'GeneralPluginFontName',
  GeneralPluginSetProfilePath name 'GeneralPluginSetProfilePath',
  GeneralPluginSetContSpecPath name 'GeneralPluginSetContSpecPath',
  GeneralPluginSetLocalPath,
  GeneralPluginSetMachSpecPath,
  GeneralPluginSetLoggingPath,
  GeneralPluginSetStringArrayInterface,
  GeneralPluginSetToolLifeInterface,
  GeneralPluginSetHttpServerHost,
  GeneralPluginSetJNIEnv;
  

implementation

uses Classes;

var
  RegList : TList;


function CustomiseConfigurationIni(Filename: string): TCustomIniFile;
var Tmp: string;
begin
  try
    Tmp:= CustomiseConfiguration(Filename);
    Result:= TMemIniFile.Create(Tmp);
    DeleteFile(Tmp);
    Exit;
  except
    on E: Exception do
      Named.SetFaultA('', E.Message, FaultFatal, nil);
  end;

  Result:= TIniFile.Create('not-a-real-file');
end;

function CustomiseConfiguration(Filename: string): string;
begin
  Result:= Named.GetConfigurationParser.Parse(Filename);
end;


function ACNC32PluginVersion : Cardinal; stdcall;
begin
  Result := SDKPluginVersion;
end;

procedure NamedPluginSetNamedInterface(aNamedInterface : IFSDNamedInterfaceA); stdcall;
begin
  Named := aNamedInterface;
end;

procedure NamedPluginSetNCInterface(aNCInterface : IFSDNCInterface); stdcall;
begin
  NC := aNCInterface;
end;

procedure GeneralPluginIOSweep(aIOSweep : PInt64); stdcall;
begin
  IOSweep := aIOSweep;
end;

procedure GeneralPluginFontName(aFontName : PChar); stdcall;
begin
  FontName := aFontName;
end;

procedure GeneralPluginSetProfilePath(aProfilePath : PChar); stdcall;
begin
  DllProfilePath := aProfilePath;
end;

procedure GeneralPluginSetStringArrayInterface(SA : IACNC32StringArraySet); stdcall;
begin
  StringArraySet := SA;
end;

procedure GeneralPluginSetLocalPath(aLocalPath : PChar); stdcall;
begin
  DllLocalPath := aLocalPath;
end;

procedure GeneralPluginSetMachSpecPath(aMachSpecPath : PChar); stdcall;
begin
  DllMachSpecPath := aMachSpecPath;
end;

procedure GeneralPluginSetContSpecPath(aContSpecPath : PChar); stdcall;
begin
  DllContSpecPath := aContSpecPath;
end;

procedure GeneralPluginSetLoggingPath(aLoggingPath : PChar); stdcall;
begin
  DllLoggingPath := aLoggingPath;
end;

procedure Dispatch(aTag : TagPointer); stdcall;
var I : Integer;
    P : PNamedPluginRegistration;
begin
  for I := 0 to RegList.Count - 1 do begin
    P := PNamedPluginRegistration(RegList[I]);
    if P^.Tag = aTag then
      P^.Callback(I);
  end;
end;

procedure GeneralPluginSetToolLifeInterface(aToolLife : IFSDToolLife); stdcall;
begin
  ToolLife := aToolLife;
end;

procedure GeneralPluginSetHttpServerHost(AServerHost: IHTTPServerHost); stdcall;
begin
  HTTPServerHost:= AServerHost;
end;

procedure GeneralPluginSetJNIEnv(AJNIEnv: PJNIEnv); stdcall;
begin
  JNIEnv:= AJNIEnv;
end;

{ TTranslator }

constructor TTranslator.Create;
var
I       : Integer;
Lang    : WideString;

begin
  inherited;
  StringArraySet.UseArray('language', nil, Table);
  if not Assigned(Table) then
    StringArraySet.CreateArray('language', nil, Table);


    I := Languages.IndexOf(SysLocale.DefaultLCID);
    LanguageType := Copy(Languages.Ext[I], 1, 2);

  Column := 2;
  for I := 1 to Table.FieldCount - 1 do begin
    Table.FieldNameAtIndex(I, Lang);
    if CompareText(Lang, LanguageType) = 0 then begin
      Column := I;
      Break;
    end;
  end;

end;

destructor TTranslator.Destroy;
begin
  Table := nil;
end;

function TTranslator.GetString(const aToken: string): string;
var Index, Tmp : Integer;
    Lang : WideString;
begin
  if (Length(aToken) > 0) and
     (aToken[1] = '$') and
     (Column <> 0) then begin
    if Table.FindRecord(0, 1, aToken, Index) = 0 then begin
      Table.GetValue(Index, Column, Lang);
      if Lang = '' then
        Table.GetValue(Index, 1, Lang);

      Result := Lang;
      Table.GetValue(Index, 0, Lang);
      try
        Tmp := StrToInt(Lang);
      except
        Tmp := 0;
      end;
      Inc(Tmp);
      Table.SetValue(Index, 0, IntToStr(Tmp));

    end else
      Result := aToken;
  end else
    Result := aToken;
end;

function NamedGetAsString(ATag: TTagRef): string;
var Buffer: array[0..1023] of Char;
begin
  Named.GetAsString(ATag, Buffer, 1024);
  Result:= Buffer;
end;

initialization
  RegList := TList.Create;
end.
