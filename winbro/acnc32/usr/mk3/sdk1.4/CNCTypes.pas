unit CNCTypes;

interface

uses Classes, SysUtils, Messages, Windows, TypInfo, StreamTokenizer, Graphics;

const
  IOMapName                = 'IOMap.ini';
  MessageMapName           = 'Message.ini';
  Fbootlog                 = 'Boot.log';
  Fbindlog                 = 'Bind.log';
  FiniFile                 = 'CNC.ini';
  LoggingDirectory         = 'Logging\';
  TemporaryDirectory       = 'Temp\';
  MaxNamedLength           = 64;
  //MaxErrorLength           = 127;
  CNCM_NUDGE               = WM_USER + 51;
  CNCM_BUTTON_EVENT        = WM_USER + 52;
  CNCM_IO_EVENT            = WM_USER + 53;
  CNCM_PLC_EVENT           = WM_USER + 54;
  CNCM_MESSAGE             = WM_USER + 55;
  CNCM_SNAPSHOT            = WM_USER + 56;
  CNCM_GEO                 = WM_USER + 57;
  CNCM_TOOL_LIFE           = WM_USER + 58;
  CNCM_FAULT_INTERLOCK     = WM_USER + 59;
  CNCM_SCREEN_SAVER        = WM_USER + 60;
  CNCM_CANCEL_SAVER        = WM_USER + 61;
  CNCM_LOCAL_EVENT         = WM_USER + 62; // Spare signal local thread crossing

  CarRet                   = #$d + #$a;

  CNCBorder                = 3;           { used for consistant border values
                                           ????? may be configurable in future }
  MAX_NAMED_STACK_DEPTH    = 30;
  //MAX_PLC_ERRORS_DW        = 16;
  //MAX_PLC_ERRORS_BYTE      = MAX_PLC_ERRORS_DW * 4;
           { The total number of errors permitted
             in the PLC error map }
  //MAX_PLC_ERRORS           = MAX_PLC_ERRORS_DW * 32;



const
 {windows.pas defines these already MGL}
 VK_ENTER          = $0d;
 VK_DELETE         = $2e;
 VK_DECIMAL        = $6e;
 VK_0              = $30;        // 0 key
 VK_1              = $31;        // 1 key
 VK_2              = $32;        // 2 key
 VK_3              = $33;        // 3 key
 VK_4              = $34;        // 4 key
 VK_5              = $35;        // 5 key
 VK_6              = $36;        // 6 key
 VK_7              = $37;        // 7 key
 VK_8              = $38;        // 8 key
 VK_9              = $39;        // 9 key
// 3A-40	Undefined
 VK_A              = $41;        // A key

 VK_B              = $42;        // B key
 VK_C              = $43;	 // C key
 VK_D              = $44;	 // D key
 VK_E              = $45;	 // E key
 VK_F              = $46;	 // F key
 VK_G              = $47;	 // G key
 VK_H              = $48;	 // H key
 VK_I              = $49;	 // I key
 VK_J              = $4A;	 // J key
 VK_K              = $4B;	 // K key
 VK_L              = $4C;	 // L key
 VK_M              = $4D;	 // M key
 VK_N              = $4E;	 // N key
 VK_O              = $4F;	 // O key
 VK_P              = $50;	 // P key
 VK_Q              = $51;	 // Q key
 VK_R              = $52;	 // R key
 VK_S              = $53;	 // S key
 VK_T              = $54;	 // T key
 VK_U              = $55;	 // U key
 VK_V              = $56;	 // V key
 VK_W              = $57;	 // W key
 VK_X              = $58;	 // X key
 VK_Y              = $59;	 // Y key
 VK_Z              = $5A;	 // Z key

 VK_PLUS           = $BB;        // Plus key
 VK_MINUS          = $BD;        // Minus Key
 VK_POINT          = $BE;        // Full Stop

type
  EDuplicateInstalation = class(Exception);
  ECNCInstallFault      = class(Exception); // Generic install fault
  ETagError             = class(Exception);
  ECNCRunTimeError      = class(Exception);

  FHandle = Integer;

{ General types used by components }

  TFaultLevel = (
    faultNone,
    faultMessageOnly,  { PLC no override }
    faultWarning,
    faultStopCycle,    { PLCSafeStop }

    faultRewind,
    faultEstop,        { PLCEStop }
    faultFatal,
    faultInterlock
  );

  TedgeType   = ( edgeRising,       { react on rising edge }
                  edgeFalling,      { react on falling edge }
                  edgeChange,       { react on any change }
                  edgeNoChange      { Never react }
                  );

  TInstallState=( InstallStateIdle,
                  InstallStateActive,
                  InstallStateDone,
                  InstallStateClosed );


  TIOType     = ( IOTypeIOIn,
                  IOTypeIOOut );

  TAccessLevel = (
    AccessLevelNone,
    AccessLevelOperator,
    AccessLevelSupervisor,
    AccessLevelProgrammer,
    AccessLevelMaintenance,
    AccessLevelAdministrator,
    AccessLevelOEM1,
    AccessLevelOEM2
  );

  TAccessLevels = set of TAccessLevel;


  TCNCStatusType = ( cstMotion,
                     cstCycle,
                     cstHomed,
                     cstEditing );  {section editor is open}


  TCNCStatusTypes = set of TCNCStatusType;

  TCNCMode    = ( CNCModeManual,
                  CNCModeAuto,
                  CNCModeMDI,
                  CNCModeTeachIn,
                  CNCModeEdit);

  TCNCModes   = set of TCNCMode;

  TSpindleMode= ( SpindleModeCW,
                  SpindleModeCCW,
                  SpindleModeStop );

  TAxisType   = ( atLinear,
                  atRotary,
                  atSpindle );

  TJogMode    = ( jmContinuous,
                  jmIncremental,
                  jmMPG,
                  jmHome );

  TJogModes = set of TJogMode;

  TEditChange    = ( EditChangeUp3,
                     EditChangeUP2,
                     EditChangeUP1,
                     EditChangeNone,
                     EditChangeDown1,
                     EditChangeDown2,
                     EditChangeDown3 );


  TPositionType  = ( ptDisplay, 	// current display position
	             ptCommanded,	// commanded position
	             ptMachine,		// machine position / HW zero
	             ptTarget,          // target position
	             ptDisttogo,	// distance to go
	             ptFerr,		// following error in counts
	             ptBias,	        // Zero offset bias
	             ptCompensation,    // Compensation
               ptCPU,
               ptGridShift,
               ptTimeRIM,
               ptVelocity,
               ptMaster,
               ptAbsMachine,
               ptAcceleration
  );


  TIOConnectionState = ( iocsOK,
                         iocsNotOK,
                         iocsDisconnected
  );

  TFileState = (fsClosed, fsSaved,fsEdited);

  TFunctionKey = ( btnF1,
                   btnF2,
                   btnF3,
                   btnF4,
                   btnF5,
                   btnF6,
                   btnF7,
                   btnF8,
                   btnF9,
                   btnF10,
                   btnF11,
                   btnF12
  );

  TAxisName        = Char; //string[2];
  AAxisName = array [0..19] of TAxisName;

  TAxisPosition    = array [TPositionType] of Double;


  PMethod = ^TMethod;

  T32Bit =  0..31;
  T32Bits = set of T32Bit;

  // Not in the SDK
  TGeneralPluginSelectProgram = function(ReqPath, RespPath : PChar; Load : Boolean) : Boolean; stdcall;

  TAllInt = record
    case Byte of
      0 : ( Low, Hi : Cardinal );
      1 : ( I64 : Int64 );
  end;

  TStreamingUnion = record
    case Byte of
      0 : ( I : Integer );
      1 : ( B : array [0..9] of Byte );
      2 : ( I64 : Int64 );
      3 : ( When : TDateTime );
      4 : ( P : Pointer );
  end;

  TXYZ = record
    X, Y, Z : Double;
  end;

function ReadDelimited(var S : string; Delimiter : Char = ',') : string;
function ReadDelimitedQuote(var S : string; Delimiter : Char = ',') : string;
function ReadDelimitedW(var S : WideString; Delimiter : Char) : WideString;
procedure GetEnumerationNames( TypeInfo: Pointer; Result: TStrings);
function ReadQuotedString(var aText : string) : string;
function ReadDelimitedParenthesis(var S: string; Delimiter: Char): string;
function QuotedString(aText : string) : string;
function TimeFormat(Time : TDateTime):String;
function DateFormat(Date : TDateTime;isAmerican : Boolean):String;
function ValidFilename(Path: string): string;

function RangeInt(const Value, Lower, Upper: Integer): Integer;
function ReadCSVField(var Text : string) : string;
function CSVField(aText : string) : string;
function StorageDate(DT: TDateTime): string;
function BitCount(Value: Integer): Integer;

function WriteStringToHandle(hFile : THandle; aText : string) : Boolean;
procedure WriteACNC32HTTPTitle(HFile : THANDLE; Title : string);
procedure WriteACNC32HTTPHeading(HFile : THANDLE; H : Integer; Heading : string);


resourcestring
  FalseText = 'False';
  TrueText = 'True';

const BooleanIdent : array [Boolean] of string =
  (  FalseText, TrueText  );

const BooleanEnum : array [Boolean] of string =
  ( 'False', 'True' );

implementation


{ General purpose function that reads a syntax of
  'token1 ; token2 ; token3'
  A single call to this function will take the above example and convert it to
  'token2 ; token3' ;
  while returning token1

  A trailing ';' will be removed when the last token is read }

function ReadDelimited(var S : string; Delimiter : Char) : string;
var P : Integer;
begin
  P := Pos(Delimiter, S);
  if P = 0 then begin
    Result := Trim(S);
    S := '';
  end else begin
    Result := Trim(Copy(S, 1, P - 1));
    System.Delete(S, 1, P);
  end;
end;

function StripQuotes(S: string): string;
begin
  Result:= S;
  if Length(Result) > 0 then begin
    if S[1] = '''' then begin
      System.Delete(Result, 1, 1);
      System.Delete(Result, Length(Result), 1);
    end;
  end;
end;

function ReadDelimitedQuote(var S: string; Delimiter: Char): string;
var I: Integer;
    Comment: Boolean;
begin
  Comment:= False;
  for I:= 1 to Length(S) do begin
    if S[I] = '''' then begin
      Comment:= not Comment;
    end
    else if S[I] = Delimiter then begin
      Result := Trim(Copy(S, 1, I - 1));
      System.Delete(S, 1, I);
      Result:= StripQuotes(Result);
      Exit;
    end;
  end;
  Result:= Trim(S);
  S:= '';
  Result:= StripQuotes(Result);
end;

function ReadDelimitedParenthesis(var S: string; Delimiter: Char): string;
var I: Integer;
    Parenthesis: Integer;
begin
  Parenthesis:= 0;
  for I:= 1 to Length(S) do begin
    if S[I] = '(' then begin
      Inc(Parenthesis);
    end
    else if S[I] = ')' then begin
      Dec(Parenthesis);
    end;
    if (S[I] = Delimiter) and (Parenthesis = 0) then begin
      Result := Trim(Copy(S, 1, I - 1));
      System.Delete(S, 1, I);
      Exit;
    end;
  end;
  Result:= Trim(S);
  S:= '';
end;

function ReadDelimitedW(var S : WideString; Delimiter : Char) : WideString;
var P : Integer;
begin
  P := Pos(Delimiter, S);
  if P = 0 then begin
    Result := Trim(S);
    S := '';
  end else begin
    Result := Trim(Copy(S, 1, P - 1));
    System.Delete(S, 1, P);
  end;
end;


procedure GetEnumerationNames( TypeInfo: Pointer; Result: TStrings);
var
  i: Integer;
begin
  Result.Clear;
  with TypInfo.GetTypeData(TypeInfo)^ do
  if PTypeInfo(TypeInfo)^.Kind = tkEnumeration then
  for i:= MinValue to MaxValue do //might be a subrange of enumerated type
    Result.Add(TypInfo.GetEnumName(TypeInfo, i))
end;

function QuotedString(aText : string) : string;
var I : Integer;
begin
  Result := '"';
  for I := 1 to Length(aText) do begin
    if aText[I] = '"' then
      Result := Result + '%22'
    else if aText[I] = '%' then
      Result := Result + '%25'
    else
      Result := Result + aText[I];
  end;
  Result := Result + '"';
end;



// This function reads a single token from aText this is any length of
// of text terminated by white space, white space inside a "quoted" string is
// ignored. A string that is "quoted" will have %nn translated to the original
// character and the bounding quotes removed.  The function then returns
// the translated string and cuts the parsed text from the source string.
function ReadQuotedString(var aText : string) : string;
var I, L : Integer;
    Quoted : Boolean;
    C : Char;
begin
  aText := TrimLeft(aText);
  Result := '';

  Quoted := False;
  if Length(aText) > 0 then begin
    case aText[1] of
      '"', '''' : Quoted := True
    else
      Result := aText[1];
    end;

    I := 2;
    L := Length(aText); // grab length (dont recalculate).

    while I <= L do begin
      if aText[I] = '%' then begin
        C := Char(StrToInt('$' + Copy(aText, I + 1, 2)));
        Result := Result + C;
        Inc(I, 2);
      end else if Quoted and ((aText[I] = '"') or (aText[I] = '''')) then begin
        System.Delete(aText, 1, I + 1);
        Exit;
      end else if not Quoted and (aText[I] = ' ') then begin
        System.Delete(aText, 1, I);
        Exit;
      end else begin
        Result := Result + aText[I];
      end;
      Inc(I);
    end;
  end;
  aText := '';
end;

function ReadCSVField(var Text : string) : string;
var I, L : Integer;
begin
  Text := Trim(Text);
  Result := '';
  if Length(Text) > 0 then begin
    if Text[1] = '"' then begin
      I := 2;
      try
        L := Length(Text);
        while I <= L do begin
          if Text[I] = '"' then begin
            if I = L then
              Exit
            else if Text[I + 1] = '"' then
              Inc(I)
            else if Text[I + 1] = ',' then
              Exit;
          end;
          Result := Result + Text[I];
          Inc(I);
        end;
      finally
        Delete(Text, 1, I + 1);
      end;
    end else
      Result := ReadDelimited(Text, ',');
  end;
end;

function RequiresQuoting(const aText: string): Boolean;
var I: Integer;
begin
  Result:= True;
  for I:= 1 to Length(aText) do begin
    if aText[I] = ',' then
      Exit;
    if aText[I] = '"' then
      Exit;
    if Ord(aText[I]) > $7e then
      Exit;
    if Ord(aText[I]) < $20 then
      Exit;
  end;
  Result:= False;
end;

function CSVField(aText : string) : string;
var I : Integer;
begin
  if RequiresQuoting(aText) then begin
    Result := '"';
    for I := 1 to Length(aText) do begin
      case aText[I] of
        '"' : Result := Result + '"';
      end;
      Result := Result + aText[I];
    end;
    Result := Result + '"';
  end else
    Result := aText;
end;

function RangeInt(const Value, Lower, Upper: Integer): Integer;
begin
  Result:= Value;

  if Value < Lower then
    Result:= Lower;
  if Value >= Upper then
    Result:= Upper;
end;

function TimeFormat(Time : TDateTime):String;
var Hour,Min,Sec,msec : Word;
    HStr,MinStr,SEcStr : String;
begin
   DecodeTime(Time,Hour,Min,Sec,msec);
   if Hour < 10 then
     HStr := '0'+IntToStr(Hour)
   else
     HStr := IntToStr(Hour);

   if Min < 10 then
     MinStr := '0'+IntToStr(Min)
   else
     MinStr := IntToStr(Min);

   if Sec < 10 then
     SEcStr := '0'+IntToStr(SEc)
   else
     SEcStr := IntToStr(SEc);

   Result := Format('%s:%s:%s',[HStr,MinStr,SecStr]);
end;

function DateFormat(Date : TDateTime;isAmerican : Boolean):String;
var Year, Month, Day: Word;
    YStr,MStr,DStr : String;
begin
   DecodeDate(Date,Year, Month, Day);
   YStr := IntToStr(Year);
   if Year < 10 then
     YStr := '0'+IntToStr(Year)
   else
     YStr := IntToStr(Year);

   if Month < 10 then
     MStr := '0'+IntToStr(Month)
   else
     MStr := IntToStr(Month);

   if Day   < 10 then
     DStr := '0'+IntToStr(Day)
   else
     DStr := IntToStr(Day);

   if isAmerican then begin
      Result := Format('%s-%s-%s',[MStr,DStr,YStr]);
   end else begin
     Result := Format('%s-%s-%s',[DStr,MStr,YStr]);
   end;
end;

function ValidFilename(Path: string): string;
var I: Integer;
begin
  Result:= '';
  for I:= 1 to Length(Path) do begin
    case Path[I] of
      '\','/',':','*','?','"','<','>','|':
      Result:= Result + '_';
    else
      Result:= Result + Path[I];
    end;
  end;
end;

function StorageDate(DT: TDateTime): string;
var ST : TSystemTime;
begin
  DateTimeToSystemTime(DT, ST);
  // YYYY-MM-DD hh:mm:ss  This seems to be an SQL standard as it Ascii / chronologically the same
  Result := Format('%4d-%.2d-%.2d %.2d:%.2d:%.2d',
     [ST.wYear, ST.wMonth, ST.wDay, ST.wHour, ST.wMinute, ST.wSecond ]  );
end;

const
  ACNC32MetaInformation = '<META NAME="Author" CONTENT="ACNC32">' + CarRet +
                          '<META NAME="Classification" CONTENT="Engineering,Machine Tool">' + CarRet +
                          '<META NAME="KeyWords" CONTENT="FSD ObjectPlc ACNC32">' + CarRet;

  ACNC32StyleSheet = '<LINK REL="stylesheet" TYPE="text/css" HREF="/fsd.css">' + CarRet;


function WriteStringToHandle(hFile : THandle; aText : string) : Boolean;
var Written : Cardinal;
begin
  WriteFile(HFile, PChar(aText)^, Length(aText), Written, nil);
  Result := Integer(Written) = Length(aText);
end;

procedure WriteACNC32HTTPHeading(HFile : THANDLE; H : Integer; Heading : string);
begin
  WriteStringToHandle(hFile, Format('<h%d align="center">%s</h%d>', [H, Heading, H]));
end;

procedure WriteACNC32HTTPTitle(HFile : THANDLE; Title : string);
begin
  WriteStringToHandle(hFile, '<HTML><HEAD>' + CarRet);
  WriteStringToHandle(hFile, '<TITLE>' + Title + '</TITLE>'  + CarRet);
  WriteStringToHandle(hFile, ACNC32MetaInformation);
  WriteStringToHandle(hFile, ACNC32StyleSheet + CarRet);
  WriteStringToHandle(hFile, '</head><body>');
end;

function BitCount(Value: Integer): Integer;
var I: Integer;
begin
  Result:= 0;
  for I:= 0 to 7 do begin
    if ( ( Value shr I ) and 1 ) <> 0 then begin
      Result:= Result + 1;
    end;
  end;
end;




//  T : string;

initialization
end.
