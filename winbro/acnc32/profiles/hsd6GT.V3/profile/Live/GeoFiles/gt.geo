World
	MachineBase
		YAxis
			XAxis
				CAxisCOR
					PartDatum
		Column
			ZAxis
				BaxisCOR
					AAxisCOR
						Head
							LowerErowa
								ToolDatum
EndofTree
[AxisLimits]
XPos= 925
XNeg=-127
YPos=261
YNeg=-261
ZPos= 790
ZNeg= 107
APos= 31
ANeg= -211
BPos= 241
BNeg= -91.6
CPos= 0
CNeg= 360

[DatumOffsets]
; error from 344.200
ToolDatumXOffset= -0.002
error from 0.000
ToolDatumYOffset=0.020
; error from -90 .000
ToolDatumZOffset=-0.418


[PartDatum]
ObjectType=ReportPoint
ReferencePoint= 0.000, 0.000,325.000
DesignTimeTranslation= 0.000, 0.000,50.000
DesignTimeRotation=0.00,0.00,0.00
FColour=,16711680
FRefColour=,255
FSelectColour=,255
End of Object
[CAxisCOR]
ObjectType=Disc
ReferencePoint= 0.000, 0.000,275.000
DesignTimeTranslation= 0.000, 0.000,100.000
DesignTimeRotation=0.00,0.00,0.00
FColour=,16711680
FRefColour=,255
FSelectColour=,255
Radius=350.00
Height=100.00
End of Object
[XAxis]
ObjectType=Box
ReferencePoint= 0.000, 0.000,175.000
DesignTimeTranslation= 0.000, 0.000,100.000
DesignTimeRotation=0.00,0.00,0.00
FColour=,16711680
FRefColour=,255
FSelectColour=,255
Length=800.00
Width=1750.00
Height=100.00
End of Object
[YAxis]
ObjectType=Box
ReferencePoint= 0.000, 0.000,75.000
DesignTimeTranslation= 0.000,250.000,75.000
DesignTimeRotation=0.00,0.00,0.00
FColour=,16711680
FRefColour=,255
FSelectColour=,255
Length=900.00
Width=900.00
Height=100.00
End of Object
[ToolDatum]
ObjectType=ReportPoint
ReferencePoint=-344.200, 0.000,235.000
DesignTimeTranslation=-30.000, 0.000, 0.000
DesignTimeRotation=0.00,0.00,0.00
FColour=,16711680
FRefColour=,255
FSelectColour=,255
End of Object
[LowerErowa]
ObjectType=Box
ReferencePoint=-314.200, 0.000,235.000
DesignTimeTranslation=-104.200, 0.000,-90.000
DesignTimeRotation=0.00,0.00,0.00
FColour=,16711680
FRefColour=,255
FSelectColour=,255
Length=50.00
Width=50.00
Height=50.00
End of Object
[Head]
ObjectType=Box
ReferencePoint=-210.000, 0.000,325.000
DesignTimeTranslation=-100.000, 0.000, 0.000
DesignTimeRotation=0.00,0.00,0.00
FColour=,16711680
FRefColour=,255
FSelectColour=,255
Length=200.00
Width=200.00
Height=400.00
End of Object
[AAxisCOR]
ObjectType=Box
ReferencePoint=-110.000, 0.000,325.000
DesignTimeTranslation=-110.000, 0.000, 0.000
DesignTimeRotation=0.00,0.00,0.00
FColour=,16711680
FRefColour=,255
FSelectColour=,255
Length=100.00
Width=20.00
Height=100.00
End of Object
[BaxisCOR]
ObjectType=Box
ReferencePoint= 0.000, 0.000,325.000
DesignTimeTranslation= 0.000,450.000, 0.000
DesignTimeRotation=0.00,0.00,0.00
FColour=,16711680
FRefColour=,255
FSelectColour=,255
Length=500.00
Width=200.00
Height=200.00
End of Object
[ZAxis]
ObjectType=Box
ReferencePoint= 0.000,-450.000,325.000
DesignTimeTranslation= 0.000,400.000,-275.000
DesignTimeRotation=0.00,0.00,0.00
FColour=,16711680
FRefColour=,255
FSelectColour=,255
Length=450.00
Width=400.00
Height=200.00
End of Object
[Column]
ObjectType=Box
ReferencePoint= 0.000,-850.000,600.000
DesignTimeTranslation= 0.000,-600.000,600.000
DesignTimeRotation=0.00,0.00,0.00
FColour=,16711680
FRefColour=,255
FSelectColour=,255
Length=400.00
Width=500.00
Height=1200.00
End of Object
[MachineBase]
ObjectType=Box
ReferencePoint= 0.000,-250.000, 0.000
DesignTimeTranslation= 0.000,-250.000, 0.000
DesignTimeRotation=0.00,0.00,0.00
FColour=,16711680
FRefColour=,255
FSelectColour=,255
Length=1000.00
Width=2000.00
Height=50.00
End of Object

[AxisData]
NumAxes=7
[Axis0]
ObjectName=XAxis
isLinear=True
MotionAxisIndex=0
isReversed=True
AffectsPartDatum=True
AffectsToolDatum=False
DatumTranslation= 0.000, 0.000,100.000
DatumRotation=0.00,0.00,0.00
DisplayTextIndex=0
[Axis1]
ObjectName=YAxis
isLinear=True
MotionAxisIndex=1
isReversed=False
AffectsPartDatum=True
AffectsToolDatum=False
DatumTranslation= 0.000,250.000,75.000
DatumRotation=0.00,0.00,0.00
DisplayTextIndex=1
[Axis2]
ObjectName=ZAxis
isLinear=True
MotionAxisIndex=2
isReversed=False
AffectsPartDatum=False
AffectsToolDatum=True
DatumTranslation= 0.000,400.000,-275.000
DatumRotation=0.00,0.00,0.00
DisplayTextIndex=2
[Axis3]
ObjectName=AAxisCOR
isLinear=False
MotionAxisIndex=3
isReversed=True
AffectsPartDatum=True
AffectsToolDatum=False
DatumTranslation=110.000, 0.000, 0.000
DatumRotation=0.00,0.00,0.00
DisplayTextIndex=3
[Axis4]
ObjectName=BaxisCOR
isLinear=False
MotionAxisIndex=4
isReversed=True
AffectsPartDatum=False
AffectsToolDatum=True
DatumTranslation= 0.000,450.000, 0.000
DatumRotation=0.00,0.00,0.00
DisplayTextIndex=4
[Axis5]
ObjectName=CAxisCOR
isLinear=False
MotionAxisIndex=5
isReversed=True
AffectsPartDatum=True
AffectsToolDatum=False
DatumTranslation=-99.000, 0.000,100.000
DatumRotation=0.00,0.00,0.00
DisplayTextIndex=5
[Axis6]
ObjectName=YAxis
isLinear=False
MotionAxisIndex=5
isReversed=False
AffectsPartDatum=False
AffectsToolDatum=False
DatumTranslation= 0.000, 0.000,25.000
DatumRotation=0.00,0.00,0.00
DisplayTextIndex=9
EndofFile
