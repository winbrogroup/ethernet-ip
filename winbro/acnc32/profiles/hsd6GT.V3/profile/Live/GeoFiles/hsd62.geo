World
	MachineBase
		YAxis
			XAxis
				BAxisCOR
					AAxisCOR
						PartDatum
		ZAxis
			CAcisCOR
				ToolDatum
EndofTree


[PartDatum]
ObjectType=ReportPoint
ReferencePoint= 0.000, 0.000,317.000
DesignTimeTranslation= 0.000, 0.000,20.000
DesignTimeRotation=0.00,0.00,0.00
FColour=,16711680
FRefColour=,255
FSelectColour=,255
End of Object
[AAxisCOR]
ObjectType=Disc
ReferencePoint= 0.000, 0.000,297.000
DesignTimeTranslation= 0.000,215.000,107.000
DesignTimeRotation=0.00,0.00,0.00
FColour=,16711680
FRefColour=,255
FSelectColour=,255
Radius=100.00
Height=40.00
End of Object
[BAxisCOR]
ObjectType=Box
ReferencePoint= 0.000,-215.000,190.000
DesignTimeTranslation= 0.000,-215.000,120.000
DesignTimeRotation=0.00,0.00,0.00
FColour=,16711680
FRefColour=,255
FSelectColour=,255
Length=2.00
Width=40.00
Height=40.00
End of Object
[XAxis]
ObjectType=Box
ReferencePoint= 0.000, 0.000,70.000
DesignTimeTranslation= 0.000, 0.000,50.000
DesignTimeRotation=0.00,0.00,0.00
FColour=,16711680
FRefColour=,255
FSelectColour=,255
Length=800.00
Width=400.00
Height=50.00
End of Object
[YAxis]
ObjectType=Box
ReferencePoint= 0.000, 0.000,20.000
DesignTimeTranslation= 0.000, 0.000,25.000
DesignTimeRotation=0.00,0.00,0.00
FColour=,16711680
FRefColour=,255
FSelectColour=,255
Length=500.00
Width=800.00
Height=50.00
End of Object
[ToolDatum]
ObjectType=ReportPoint
ReferencePoint= 0.000,-32.200,190.000
DesignTimeTranslation= 0.000,155.900,-413.000
DesignTimeRotation=0.00,0.00,0.00
FColour=,16711680
FRefColour=,255
FSelectColour=,255
End of Object
[CAcisCOR]
ObjectType=Box
ReferencePoint= 0.000,-188.100,603.000
DesignTimeTranslation= 0.000,111.900,100.000
DesignTimeRotation=0.00,0.00,0.00
FColour=,16711680
FRefColour=,255
FSelectColour=,255
Length=40.00
Width=40.00
Height=20.00
End of Object
[ZAxis]
ObjectType=Box
ReferencePoint= 0.000,-300.000,503.000
DesignTimeTranslation= 0.000,-300.000,508.000
DesignTimeRotation=0.00,0.00,0.00
FColour=,16711680
FRefColour=,255
FSelectColour=,255
Length=40.00
Width=160.00
Height=300.00
End of Object
[MachineBase]
ObjectType=Box
ReferencePoint= 0.000, 0.000,-5.000
DesignTimeTranslation= 0.000, 0.000,-5.000
DesignTimeRotation=0.00,0.00,0.00
FColour=,16711680
FRefColour=,255
FSelectColour=,255
Length=1455.00
Width=1200.00
Height=10.00
End of Object

[AxisData]
NumAxes=7
[Axis0]
ObjectName=XAxis
isLinear=True
MotionAxisIndex=0
isReversed=True
AffectsPartDatum=True
AffectsToolDatum=False
DatumTranslation= 0.000, 0.000,50.000
DatumRotation=0.00,0.00,0.00
DisplayTextIndex=0
[Axis1]
ObjectName=YAxis
isLinear=True
MotionAxisIndex=1
isReversed=False
AffectsPartDatum=True
AffectsToolDatum=False
DatumTranslation= 0.000, 0.000,25.000
DatumRotation=0.00,0.00,0.00
DisplayTextIndex=1
[Axis2]
ObjectName=ZAxis
isLinear=True
MotionAxisIndex=2
isReversed=False
AffectsPartDatum=False
AffectsToolDatum=True
DatumTranslation= 0.000,-300.000,508.000
DatumRotation=0.00,0.00,0.00
DisplayTextIndex=2
[Axis3]
ObjectName=AAxisCOR
isLinear=False
MotionAxisIndex=5
isReversed=True
AffectsPartDatum=True
AffectsToolDatum=False
DatumTranslation= 0.000,215.000,-200.000
DatumRotation=0.00,0.00,0.00
DisplayTextIndex=3
[Axis4]
ObjectName=BAxisCOR
isLinear=False
MotionAxisIndex=4
isReversed=True
AffectsPartDatum=True
AffectsToolDatum=False
DatumTranslation= 0.000,-215.000,350.000
DatumRotation=0.00,0.00,0.00
DisplayTextIndex=4
[Axis5]
ObjectName=CAcisCOR
isLinear=False
MotionAxisIndex=5
isReversed=False
AffectsPartDatum=False
AffectsToolDatum=True
DatumTranslation= 0.000,112.000,100.000
DatumRotation=0.00,0.00,0.00
DisplayTextIndex=5
[Axis6]
ObjectName=YAxis
isLinear=False
MotionAxisIndex=5
isReversed=False
AffectsPartDatum=False
AffectsToolDatum=False
DatumTranslation= 0.000, 0.000,25.000
DatumRotation=0.00,0.00,0.00
DisplayTextIndex=9
EndofFile
