Button illuminates when EDM clamp is active, the button when pressed 
(in manual) inverts the state of the EDM clamp.  When turning the EDM clamp from 
off to on the Refeed clamp is released.