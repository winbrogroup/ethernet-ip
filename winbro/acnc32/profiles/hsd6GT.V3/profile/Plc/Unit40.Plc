Unit 40 Water Plant

Local

// Functionality Constants 
// 2048 = 10cm
// 2048 = 1.5MOhm 
// 2048 = 2DegC
// 2048 = 0.63Bar - 3276 = 1Bar
// 2048 = 10Bar - 32768 = 160Bar
DirtyPumpStartLevel Item 7000; //  
DirtyPumpStopLevel Item 5000; //  
CleanWaterMinimumLevel Item 4096;  
StartFillLevel Item 28672;
StopFillLevel Item 32768;
DirtyTankMaxMakeUpLevel Item;
WeirLevelUB Item 20000;
WeirLevelLB Item 16000;

AlarmPressureDrop15u Item 8192;
WarningPressureDrop15u Item 6554;

AlarmPressureDrop2u Item 5734; 
WarningPressureDrop2u Item 4915; 

HPFeedPressureMinimum Item 5000;
DirtyOutletPressureMinimum Item 5000;

TotalWater Numeric        1000;
CatPumpTimer Timer 10;
PowerUpTimer Timer 12; // must be greater than CatPumpTimer

DirtyPumpErrorDelay Timer 1;
CleanPumpErrorDelay Timer 1;
CleanPressureErrorDelay Timer 5;
CleanPumpfailed Item;
DirtyPressureErrorDelay Timer 5;
DirtyPumpFailed Item;
ChillerSupplyErrorDelay Timer 1;
SensorFailure Item;

// Physical IO

O_DirtyPumpRun       IO; // done
O_CleanPumpRun       IO; // done
O_ChillerSupplyOn    IO; // done
//O_WaterWashGun       IO;

//O_HPPressureDump     IO;
O_AlarmBeacon        IO;
O_LampTest           IO; // done
O_FactoryWaterMakeUp IO; // done
O_HPPumpSupply       IO; // done
O_HPPumpRun          IO; // done
O_PressureDemand     IO; // done
O_AnalogueSpare2     IO; // done

I_DirtyWaterPreFilterPressure ReverseWord;
I_DirtyWater15uFilterPressure ReverseWord;
I_DirtyWater2uFilterPressure ReverseWord;

I_DIFeed5uFilterPressure ReverseWord;
I_CleanTankWaterLevel ReverseWord;
I_MachineFeedPressure ReverseWord;
I_DIResistivity       ReverseWord;
I_DITemperature       ReverseWord;
I_DirtyTankWaterLevel ReverseWord;

I_DI_EstopOK              IOR;
I_LampTest                IOR; //
I_DirtyPumpRunning        IOR; //
I_CleanPumpRunning        IOR; //
I_ChillerSupplyOn         IOR; //
I_ChillerHealthy          IOR; //
I_NotResistivityMeterAlarm   IOR; //
I_NotResistivityMeterWarning IOR; //

I_UVLampHealthy           IOR; //
I_HPPumpSupplyOn          IOR;
I_HPPumpInverterHealthy   IOR;

// Working variables
TotalLevel Item;
// Top up timer
SteadyStateTimer Timer 60;
Pressure1 Item;
Pressure2 Item;
LatchOn Item;

CleanTankWaterLevel Item;
DirtyTankWaterLevel Item;
Tmp Item;
Clock Item;
ToolHPInHead IOR;

ResistivityTooLow Text "$RESISTIVITY_TOO_LOW";

//,$ALARM9156,Dirty Water pressure sensor failure (PIT03)
//,$ALARM9157,15 micron Filter sensor failure (PIT04)
//,$ALARM9158,2 micron Filter sensor failure (PIT05)
//,$ALARM9159,5 micron Filter sensor failure (PIT06)
//,$ALARM9160,Clean tank level sensor failure (PIT02)
//,$ALARM9161,Dirty tank level sensor failure (PIT01)
//,$ALARM9162,Inverter (A01) /pressure sensor (PIT07) failure
//,$ALARM9163,Resistivity sensor failure (Meter Loop 1)
//,$ALARM9164,Resistivity sensor failure (Meter Loop 2)

Implementation

if IOSweep.Bit.2
out CleanTankWaterLevel.Assign.I_CleanTankWaterLevel
out CleanTankWaterLevel.Add.1640
out DirtyTankWaterLevel.Assign.I_DirtyTankWaterLevel
out DirtyTankWaterLevel.Add.1640


if I_LampTest
or AppSpec4PB
out O_LampTest

//if IOSweep.bit.3
//out A.Assign.CleanTankWaterLevel

if CleanTankWaterLevel.GT.WeirLevelUB
or LatchOn
nand CleanTankWaterLevel.LT.WeirLevelLB
out LatchOn

if I_DirtyWaterPreFilterPressure.LT.DirtyOutletPressureMinimum
and O_DirtyPumpRun
out DirtyPressureErrorDelay.set
nout DirtyPressureErrorDelay.reset

if DirtyPressureErrorDelay.out
or DirtyPumpFailed
nand ResetRewind
out DirtyPumpFailed
out Alarm.9167

if DirtyTankWaterLevel.GT.DirtyPumpStartLevel
or O_DirtyPumpRun
and DirtyTankWaterLevel.GT.DirtyPumpStopLevel
nand LatchOn
nand SensorFailure
nand DirtyPumpFailed
out O_DirtyPumpRun

nif HPPumpInletPressureHealthy
and O_CleanPumpRun
out CleanPressureErrorDelay.Set
nout CleanPressureErrorDelay.Reset

if CleanPressureErrorDelay.out
or CleanPumpFailed
nand ResetRewind
out CleanPumpfailed
out Alarm.9166

if CleanTankWaterLevel.GT.CleanWaterMinimumLevel
nand CleanPumpfailed
nand SensorFailure
out O_CleanPumpRun
OUT O_ChillerSupplyOn
if IOSweep.Bit.2
out TotalLevel.Assign.DirtyTankWaterLevel
out TotalLevel.Add.CleanTankWaterLevel

if IOSweep.Bit.2
out DirtyTankMaxMakeUpLevel.Assign.17408
out DirtyTankMaxMakeUpLevel.Add.StartFillLevel
out DirtyTankMaxMakeUpLevel.Sub.StopFillLevel

if TotalLevel.LT.StartFillLevel
and SteadyStateTimer.out
and DirtyTankWaterLevel.LT.DirtyTankMaxMakeUpLevel
or  O_FactoryWaterMakeUp
and TotalLevel.LT.StopFillLevel
out O_FactoryWaterMakeUp

nif I_UVLampHealthy
out Alarm.9141

if IOSweep.Bit.2
out Pressure1.Assign.I_DirtyWaterPreFilterPressure
out Pressure1.Sub.I_DirtyWater15uFilterPressure

if IOSweep.Bit.2
out Pressure2.Assign.I_DirtyWater15uFilterPressure
out Pressure2.Sub.I_DirtyWater2uFilterPressure 

if Pressure1.GT.AlarmPressureDrop15u
nand NC1Incycle
out Alarm.9142

if Pressure1.GT.WarningPressureDrop15u
nand Alarm.9142
out Alarm.9143

if Pressure2.GT.AlarmPressureDrop2u
nand NC1Incycle
out Alarm.9144

if Pressure2.GT.WarningPressureDrop2u
nand Alarm.9144
out Alarm.9145

if O_DirtyPumpRun 
and I_DI_EstopOK
nand I_DirtyPumpRunning
out DirtyPumpErrorDelay.Set
nout DirtyPumpErrorDelay.Reset

if O_CleanPumpRun
and I_DI_EstopOK
nand I_CleanPumpRunning
out CleanPumpErrorDelay.Set
nout CleanPumpErrorDelay.Reset

if O_ChillerSupplyOn
and I_DI_EstopOK
nand I_ChillerSupplyOn
out ChillerSupplyErrorDelay.Set
nout ChillerSupplyErrorDelay.Reset

if DirtyPumpErrorDelay.out
or Alarm.9136
nand ResetRewind
out Alarm.9136

if CleanPumpErrorDelay.out
or Alarm.9137
nand ResetRewind
out Alarm.9137

if ChillerSupplyErrorDelay.out
or Alarm.9138
nand ResetRewind
out Alarm.9138

nif I_ChillerHealthy
and I_ChillerSupplyOn
out Alarm.9139

nif I_NotResistivityMeterAlarm
nand Acnc32Fake
out Alarm.9117
out MReqCycleStart.Inhibit.ResistivityTooLow

nif I_NotResistivityMeterWarning
out Alarm.9140

if I_DIFeed5uFilterPressure.GT.HPFeedPressureMinimum
out HPPumpInletPressureHealthy

if IOSweep.Bit.2
out Clock
out Tmp.Assign.0

if Clock
out Tmp.Assign.NC_DEMANDED_PRESSURE

if Clock
and Tmp.GT.136
nand ToolHPInHead
out Tmp.Assign.136

if Clock
out Tmp.mul.105
out Tmp.div.204 // 204 ???

// Bound on upper limit
if Clock 
and Tmp.GT.105
out Tmp.Assign.105

// Bound on lower limit
if Clock
and Tmp.LT.35 // 35 ???
out Tmp.Assign.35 // 35 ???

// If electrode flush required then output
nif CatPumpTimer.out
or AReqElectSpit.Exec
and Clock
out Tmp.Assign.Tmp
out O_PressureDemand.Assign.Tmp

// If flush not required, zero output
if CatPumpTimer.out
nand AReqElectSpit.Exec
nor PowerupTimer.out
and Clock
out O_PressureDemand.Assign.0

if ElectrodeFlush
or GuideFlush
or AReqElectSpit.Exec
nout CatPumpTimer.Set
out CatPumpTimer.Reset

if O_HPPumpRun
nand HPPumpInletPressureHealthy
or Alarm.9137 // Clean water failed to switch on
nor I_HPPumpSupplyOn
nor I_HPPumpInverterHealthy
or Alarm.9146
or CleanPumpfailed
or DirtyPumpFailed
or SensorFailure
nand ResetRewind
nand Acnc32Fake
out Alarm.9146

if IOFaultLevel.GT.2
out PowerUpTimer.Reset

if Plc.True
out PowerUpTimer.set

nif CatPumpTimer.out
and PowerUpTimer.out
and HPPumpInletPressureHealthy
nand Alarm.9146
and I_HPPumpInverterHealthy
out O_HPPumpRun

if Plc.True
and I_DI_EstopOK
out O_HPPumpSupply

if CatPumpTimer.out
out SteadyStateTimer.Set
nout SteadyStateTimer.Reset

if I_DirtyWaterPreFilterPressure
out Alarm.9156
if I_DirtyWater15uFilterPressure
out Alarm.9157
if I_DirtyWater2uFilterPressure
out Alarm.9158
if I_DIFeed5uFilterPressure
out Alarm.9159
if I_CleanTankWaterLevel
out Alarm.9160
if I_DirtyTankWaterLevel
out Alarm.9161
if I_MachineFeedPressure
and I_DI_EstopOK
out Alarm.9162
if I_DIResistivity       
out Alarm.9163
if I_DITemperature     
out Alarm.9164

nif I_HPPumpInverterHealthy 
or Alarm.9165
nand ResetRewind
out Alarm.9165

if I_MachineFeedPressure
and I_DI_EstopOK
or I_CleanTankWaterLevel
or (
if I_DIResistivity       
or I_DITemperature     
or I_DirtyWaterPreFilterPressure
or I_DirtyWater15uFilterPressure
or I_DirtyWater2uFilterPressure
or I_DIFeed5uFilterPressure
or I_DirtyTankWaterLevel
nand Head1Servo
)
out SensorFailure

end