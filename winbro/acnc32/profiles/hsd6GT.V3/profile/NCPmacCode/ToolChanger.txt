; Default offset positions
#define C_OFFSET 0.4
#define C_TOLERANCE 0.2

&2
#define C_CPU 8738.1333333333333
#define STATION_ANGLE 45

#8->8738.1333333333333C

I800=1
I808=96
I809=96
; Turn off follow error
I811=1*16*C_CPU
I812=0
I815=0.3
I816=220.00
I817=0.7
I819=0.7
I821=2
I822=20
I827=C_CPU * 360
I828=160
I830=20000
I831=0.00
I833=1280.00


// M620 - M649 allocated to Tool changer
// P200 - P220 Allocated to ToolChanger


// Externally set variables
#define TC_C_OFFSET_P      P200

#define PLCC6_TMP         P203
#define INDEX_C_P         P204
#define CPOS_TMP_P        P205
#define TARGET_C_P        P206
#define CPOS_ACT_P        P208
#define CURRENT_INDEX_C_P P209
#define SHADOW_REQUEST_P  P211
#define SHADOW_STATUS_P   P212
#define SHADOW_STATUS1_P  P213
#define LAST_REQUEST_P    P214
#define C_HOME_GRID_P     P215

#define C_MOTOR_STATUS_M  M620
#define TC_CS_STATUS_M    M622
#define C_POSITION_M      M623
#define C_POS_BIAS_M      M625
#define C_ENC_STAT_M      M627
#define C_MOTOR_STATUS1_M M629
#define C_ENCODER_2X_M    M631

#define C_MOTOR_STATUS_ADDR X:$430,0,24
#define C_MOTOR_STATUS1_ADDR Y:$440,0,24
#define TC_CS_STATUS_ADDR   X:$2140,0,24

C_MOTOR_STATUS_M->C_MOTOR_STATUS_ADDR
C_MOTOR_STATUS1_M->C_MOTOR_STATUS1_ADDR

TC_CS_STATUS_M->TC_CS_STATUS_ADDR

C_ENCODER_2X_M->X:$07831A,0,24,s ; Channel 8 Encoder servo position
C_POSITION_M->D:$0040B ; Actual Position

C_POS_BIAS_M->D:$44C

C_ENC_STAT_M->X:$78318,0,24

TC_C_OFFSET_P = C_OFFSET

SHADOW_STATUS_P = 0
SHADOW_STATUS1_P = 0

open plcc 6

  while(1 = 1)

    SHADOW_REQUEST_P = PLC_X_BITS0

    if(@OFF(SHADOW_REQUEST_P, X0_TC_OPENLOOP))
      if(@ON(LAST_REQUEST_P, X0_TC_OPENLOOP))
        COMMAND "&2A"
      endif
    else
      if(@OFF(LAST_REQUEST_P, X0_TC_OPENLOOP))
        COMMAND "#8K"
      endif
    endif

    // Block reporting of errors
    if(@OFF(SHADOW_REQUEST_P, X0_TC_NO_ERROR))

      @SET_OFF(SHADOW_STATUS_P, Q0_TC_ERROR)

      // Check for errors
      if(@ON(C_MOTOR_STATUS_M, BIT18))
        @SET_ON(SHADOW_STATUS_P, Q0_C_OPENLOOP)
        @SET_ON(SHADOW_STATUS_P, Q0_TC_ERROR)
                               
      else
        @SET_OFF(SHADOW_STATUS_P, Q0_C_OPENLOOP)
      endif
    
      if(@ON(C_MOTOR_STATUS_M, BIT21))
        @SET_ON(SHADOW_STATUS_P, Q0_C_PLUS_LIM)
        @SET_ON(SHADOW_STATUS_P, Q0_TC_ERROR)
      else
        @SET_OFF(SHADOW_STATUS_P, Q0_C_PLUS_LIM)
      endif

      if(@ON(C_MOTOR_STATUS_M, BIT22))
        @SET_ON(SHADOW_STATUS_P, Q0_C_MINUS_LIM)
        @SET_ON(SHADOW_STATUS_P, Q0_TC_ERROR)
      else
        @SET_OFF(SHADOW_STATUS_P, Q0_C_MINUS_LIM)
      endif

      if(@ON(C_MOTOR_STATUS1_M, 1024))
        @SET_ON(SHADOW_STATUS_P, Q0_TC_HOMED)
      else
        @SET_OFF(SHADOW_STATUS_P, Q0_TC_HOMED)
      endif

      if(@ON(TC_CS_STATUS_M, BIT0))
        @SET_ON(SHADOW_STATUS_P, Q0_TC_BUSY)
      else
        @SET_OFF(SHADOW_STATUS_P, Q0_TC_BUSY)
      endif


      if(@ON(SHADOW_STATUS_P, Q0_TC_ERROR))
        ; Remove requested station
        SHADOW_STATUS1_P = SHADOW_STATUS1_P & $FF70
      endif

      CPOS_TMP_P = C_POSITION_M / (96 * 32 * C_CPU)
      CPOS_TMP_P = CPOS_TMP_P - TC_C_OFFSET_P
      CPOS_TMP_P = CPOS_TMP_P % 360.0
      if(ABS(CPOS_TMP_P) < C_TOLERANCE)
        CURRENT_INDEX_C_P = 1
      else
        if(ABS(CPOS_TMP_P - 45) < C_TOLERANCE)
          CURRENT_INDEX_C_P = 2
        else
          if(ABS(CPOS_TMP_P - 90) < C_TOLERANCE)
            CURRENT_INDEX_C_P = 3
          else
            if(ABS(CPOS_TMP_P - 135) < C_TOLERANCE)
              CURRENT_INDEX_C_P = 4
            else
              if(ABS(CPOS_TMP_P - 180) < C_TOLERANCE)
                CURRENT_INDEX_C_P = 5
              else
                if(ABS(CPOS_TMP_P - 225) < C_TOLERANCE)
                  CURRENT_INDEX_C_P = 6
                else
                  if(ABS(CPOS_TMP_P - 270) < C_TOLERANCE)
                    CURRENT_INDEX_C_P = 7
                  else
                    if(ABS(CPOS_TMP_P - 315) < C_TOLERANCE)
                      CURRENT_INDEX_C_P = 8
                    else
                      if(ABS(CPOS_TMP_P - 360) < C_TOLERANCE)
                        CURRENT_INDEX_C_P = 1
                      else
                        CURRENT_INDEX_C_P = 0
                      endif
                    endif
                  endif
                endif
              endif
            endif
          endif
        endif
      endif
    endif // NO REPORTING

    ;
    ; C Axis position
    ;
    PLCC6_TMP = C_POSITION_M / (96 * 32 * C_CPU)
    if(PLCC6_TMP < 0)
      PLCC6_TMP = PLCC6_TMP * -1
      @SET_ON(SHADOW_STATUS1_P, Q1_C_NEG)
    else
      @SET_OFF(SHADOW_STATUS1_P, Q1_C_NEG)
    endif

    PLC_Q_WORD0 = PLCC6_TMP - 0.5
    PLC_Q_WORD1 = (PLCC6_TMP * 1000) % 1000


    if(@ON(SHADOW_REQUEST_P, X0_TC_RESET))
      if(@OFF(LAST_REQUEST_P, X0_TC_RESET))
        PLC_Q_WORD6= PLC_Q_WORD6 + 1
        COMMAND "&2A"
      endif
    endif

    ;
    ; Program commands are accepted under the following conditions
    ; 1. No Error
    ; 2. Not Busy
    ; 3. Rising edge of clock
    ; Commands should be issued 1 at a time
    ; and removed once accepted
    ;
    if(@OFF(SHADOW_STATUS_P, Q0_TC_ERROR))
      if(@OFF(SHADOW_STATUS_P, Q0_TC_BUSY))
        if(@ON(SHADOW_REQUEST_P,X0_TC_CLOCK))
	  if(@OFF(LAST_REQUEST_P, X0_TC_CLOCK))
            if(@ON(SHADOW_REQUEST_P, X0_TC_HOME))
              COMMAND "&2B1500R"
            endif
            INDEX_C_P = SHADOW_REQUEST_P & $1f
            if(INDEX_C_P > 0)
              SHADOW_STATUS1_P = SHADOW_STATUS1_P & $FF70
              SHADOW_STATUS1_P = SHADOW_STATUS1_P | INDEX_C_P
              TARGET_C_P = ((INDEX_C_P - 1) * STATION_ANGLE) + TC_C_OFFSET_P


              ; Conversion to incremental
              ;CPOS_ACT_P = C_POSITION_M / (96 * 32 * C_CPU)
              ;CPOS_ACT_P = CPOS_ACT_P % 360.0
              ;TARGET_C_P = TARGET_C_P - CPOS_ACT_P
              ;if (TARGET_C_P > 0)
              ;  TARGET_C_P = TARGET_C_P - 360                
              ;endif


              COMMAND "&2B1501R";
            endif
          endif
        endif
      endif
    endif

    SHADOW_STATUS_P = SHADOW_STATUS_P & $FF70
    SHADOW_STATUS_P = SHADOW_STATUS_P | CURRENT_INDEX_C_P

    PLC_Q_BITS0 = SHADOW_STATUS_P
    PLC_Q_BITS1 = SHADOW_STATUS1_P
    LAST_REQUEST_P = SHADOW_REQUEST_P
  endw

close

// Home axes
open prog 1500 clear
  I813=0
  I814=0

  @SET_OFF(C_MOTOR_STATUS_M, 1024)

  while(@OFF(C_ENC_STAT_M, 65536))
    F600 LIN
    INC C-1.0
  endw
  while(@ON(C_ENC_STAT_M, 65536))
    F300 LIN
    INC C0.05
    Dwell0
  endw
  I823=-1.0
  I826=0
  I897=1
  Dwell0
  C_HOME_GRID_P = C_ENCODER_2X_M / (C_CPU * 2)
  HOME8
  Dwell0
  C_HOME_GRID_P = C_HOME_GRID_P - (C_ENCODER_2X_M / (C_CPU * 2))

  ; Move to station 1
  F300 LIN
  ABS C(TC_C_OFFSET_P)
  WHILE (@OFF(C_MOTOR_STATUS1_M, BIT0))
    // wait until in Pos
  ENDWHILE

Dwell0

close


// Move to position
open prog 1501 clear
  //??? WAS ABS
  ABS C(TARGET_C_P)F6000
  DWELL(10)
  WHILE (@OFF(C_MOTOR_STATUS_M, BIT13))
    // wait desired velocity 0
  ENDWHILE
  DWELL(0)
close

