open plcc 0
  // H1_POS internal register used by servo
  H1_POS   = (H1_POSITION_M - H1_ENC_OFFSET) * H1_ENC_SCALING
  H1_VOLTS = ((H1_VOLTAGE_M * 10 / X_EDM1_VSCALING) + H1_V_OFFSET)
  H1_GAP = X_EDM1_GAP / 10
  Q_EDM1_OFFSET = H1_ENC_OFFSET * H1_ENC_SCALING
  PLC_Q_ROTARY = X_EDM1_SPINDLE     // transfer the Rotary tool Speeds.
  PLC_Q_HP_DEMAND = X_EDM1_HP

  if(H1_ANA3 < 400)
    P_PRESSURE_SUM = 0
  else
    P_PRESSURE_SUM = P_PRESSURE_SUM + (H1_ANA3 - 392) / 10.16
  endif
  P_PRESSURE_COUNT = P_PRESSURE_COUNT + 1
  if(P_PRESSURE_COUNT > 500)
    PLC_Q_HP_ACTUAL = P_PRESSURE_SUM / P_PRESSURE_COUNT
    P_PRESSURE_COUNT = 0;
    P_PRESSURE_SUM = 0
  endif

  //AXIS_8_DAC_M = X_EDM1_SPINDLE * 85

  if (H1_VOLTS < 0)
    H1_VOLTS = 0
  endif
  Q_EDM1_GAPV = H1_VOLTS

  // Only update current when not on final depth lock
  if (H1_FSPOS_P - H1_HIC < X_EDM1_FINAL_DEPTH)
    CURRENT_STABLE_P = CURRENT_STABLE_P + 1
    if(CURRENT_STABLE_P > 112)
      Q_EDM1_GAPI = (H1_CURRENT_M / 3.792) 
    endif
  else
    CURRENT_STABLE_P = 0
  endif

  // Normal servo programmed in 1/5micron
  PLC0_SPEED = (X_EDM1_SERVO / SERVO_FREQ)*2
  // Rapid recovery programmed in 100microns need to convert to 1/10micron at servo rate
  RAPID_REC_SPEED_P = (X_EDM1_RRSERVO * 1000) / SERVO_FREQ
  PLC0_PSERVO= H1_VOLTS - H1_GAP

  if (X_EDM1_RRDEPTH > 0)
    RAPID_RECOVERY_P = X_EDM1_RRDEPTH
  else
    RAPID_RECOVERY_P = 1000000
  endif

  if (@OFF(X_EDM1_COMMAND, EDMC_FREEZE))
  IF (@OFF(X_EDM1_COMMAND, EDMC_SERVO))

    IF(H1_SERVO_ON = 1)
      H1_SERVO_ON = 0
      @SET_OFF(Q_EDM1_STATUS, EDMS_FIRST_SPARK)
      @SET_OFF(Q_EDM1_STATUS, EDMS_RETRACT_ERROR)

      H1_STANDOFF = X_EDM1_STANDOFF

      if(@ON(X_EDM1_COMMAND, EDMC_FROM_FIRST_SPARK))
        if ((H1_FSPOS_P - H1_HIC) > H1_STANDOFF)
          H1_ENC_INC_ADJUST = ((H1_FSPOS_P - H1_STANDOFF) / H1_ENC_SCALING) / (H1_HIC / RETRACT_SPEED)
        else
          H1_ENC_INC_ADJUST = 0
        endif
      else
        if(@ON(X_EDM1_COMMAND, EDMC_FROM_DEPTH)) 
          if (H1_STANDOFF > H1_HIC)
            H1_ENC_INC_ADJUST = ((H1_HIC - H1_STANDOFF) / H1_ENC_SCALING) / (H1_HIC / RETRACT_SPEED)
          else
            H1_ENC_INC_ADJUST = 0
          endif
        else
          H1_ENC_INC_ADJUST = 0
        endif 
      endif

      H1_SHORT_COUNTER = 0
      H1_MAX_POSITION_P = 0 // Zero forward progress used by SLIC
      if(@ON(X_EDM1_COMMAND, EDMC_REVERSE_DRILL))
        H1_ENC_INC_ADJUST = 0 - H1_ENC_INC_ADJUST
      endif
    endif
    IF(H1_HIC < 0)
      H1_HIC = H1_HIC + RETRACT_SPEED
      H1_ENC_OFFSET = H1_ENC_OFFSET + H1_ENC_INC_ADJUST
    ENDIF  // (H1_HIC >= 0)

    IF(H1_HIC > 0)
      H1_HIC = H1_HIC - RETRACT_SPEED
    ENDIF

    IF(ABS(H1_HIC) < RETRACT_SPEED + 2)
      H1_HIC = 0
    ENDIF
//
//  Add remove offset code here, this is appropriate as No EDM or retraction can be taking place
//
//  Variables are
//  Q_EDM1_STATUS.EDMS_OFFSET_PRESENT. Status of offset
//  X_EDM1_COMMAND.EDMC_REMOVE_OFFSET. Request to remove offset
//
    IF(H1_HIC = 0) 
      if(H1_ENC_OFFSET != 0)
        @SET_ON(Q_EDM1_STATUS, EDMS_OFFSET_PRESENT)
        if(@ON(X_EDM1_COMMAND, EDMC_REMOVE_OFFSET))
          if(H1_ENC_OFFSET > 0)
            H1_ENC_OFFSET = H1_ENC_OFFSET - OFFSET_REMOVE_SPEED
          endif

          if(H1_ENC_OFFSET < 0)
            H1_ENC_OFFSET = H1_ENC_OFFSET + OFFSET_REMOVE_SPEED
          endif

          if(ABS(H1_ENC_OFFSET) < OFFSET_REMOVE_SPEED)
            H1_ENC_OFFSET = 0
          endif
        endif  
      else
        @SET_OFF(Q_EDM1_STATUS, EDMS_OFFSET_PRESENT)
      endif

      H1_HIC = 0
      @SET_ON(Q_EDM1_STATUS, EDMS_RETRACTED);
    ENDIF // (H1_HIC < 0)
    
  ELSE // In EDM Servo Mode

    if(H1_SERVO_ON < 1)
      H1_FSPOS_P = 0
      if(@ON(X_EDM1_COMMAND, EDMC_LAST_FIRST_SPARK))
        H1_FSPOS_P = H1_LASTFS_P
        @SET_ON(Q_EDM1_STATUS, EDMS_FIRST_SPARK)
      endif
     H1_DCM_POSITION = H1_HIC
    endif

    H1_SERVO_ON = 1
    @SET_OFF(Q_EDM1_STATUS, EDMS_RETRACTED);

// Is the voltage less than the ReqGap
    if(H1_VOLTS < H1_GAP)
    // If voltage is less than X_EDM1_RTHRESH
    // then enter increment short counter
      if (H1_VOLTS < (X_EDM1_RTHRESH))
        H1_SHORT_COUNTER = H1_SHORT_COUNTER + 1
        H1_IN_SHORT_P = 1        
        if (H1_SHORT_COUNTER > X_EDM1_SINT)
        // Full Servo Reverse
          if(@OFF(Q_EDM1_STATUS, EDMS_FIRST_SPARK))
            if(@OFF(X_EDM1_COMMAND, EDMC_LAST_FIRST_SPARK))
              H1_FSPOS_P = H1_HIC

              if (@ON(X_EDM1_COMMAND, EDMC_STORE_FIRST_SPARK))
                H1_LASTFS_P = H1_FSPOS_P
              endif
            endif
            @SET_ON(Q_EDM1_STATUS, EDMS_FIRST_SPARK)
          endif         
          H1_HIC = H1_HIC + X_EDM1_SVEL
          H1_SHORT_COUNTER = 0;
        endif // (H1_SHORT_COUNTER > X_EDM1_SINT)
      else
      // This is a frig that
      // keeps the electrode moving
      // regardless of voltage
        H1_HIC = H1_HIC - (PLC0_SPEED / 15)

        // SLIC accounts for frig
        H1_SPEED_RATIO_P = 1/15
      endif // (H1_VOLTS < X_EDM1_RTHRESH)

    else //(H1_VOLTS > H1_GAP)
      H1_SHORT_COUNTER = 0;




// ----------------------------------------------

      if(H1_IN_SHORT_P != 0) 
        H1_IN_SHORT_P = 0
        ;H1_HIC = H1_HIC - UAxisFollowErr_P
      endif

//-----------------------------------------------


      if(H1_HIC > (H1_MAX_POSITION_P + RAPID_RECOVERY_P))
        PLC0_SPEED = RAPID_REC_SPEED_P
      endif


      IF(H1_VOLTS > (H1_GAP + X_EDM1_FTHRESH))
      // Full Servo forwards
       H1_HIC = H1_HIC - PLC0_SPEED
       H1_SPEED_RATIO_P = 1
      else
      // Proportional forwards
       H1_SPEED_RATIO_P = (PLC0_PSERVO / X_EDM1_FTHRESH)
       H1_HIC = H1_HIC - ((PLC0_PSERVO / X_EDM1_FTHRESH) * PLC0_SPEED)
      endif
    endif
    

   
    if(@ON(Q_EDM1_STATUS, EDMS_FIRST_SPARK))
      if (@ON(X_EDM1_COMMAND, EDMC_DCM))
      // Forward servo required and Feedhold set
        if (H1_HIC < H1_DCM_POSITION)
          // Attempting to move beyond the current feed hold position
          H1_HIC = H1_DCM_POSITION
        endif
      else
        H1_DCM_POSITION = H1_HIC // update on no command
      endif
    else
      H1_DCM_POSITION = H1_HIC // update on no first spark
    endif


    if(@ON(Q_EDM1_STATUS, EDMS_FIRST_SPARK))
      if (H1_FSPOS_P - H1_HIC > X_EDM1_FINAL_DEPTH + RETRACT_SPEED)
        H1_HIC = H1_HIC + RETRACT_SPEED
        // Hold the integration clock while held on final depth
        H1_VELOCITY_COUNTER_P = H1_VELOCITY_COUNTER_P - 1 
      endif
    endif

    if(H1_HIC > 8000)
      @SET_ON(Q_EDM1_STATUS, EDMS_RETRACT_ERROR)
      H1_HIC = 8001
    else
      @SET_OFF(Q_EDM1_STATUS, EDMS_RETRACT_ERROR)
    endif

    // Forward speed detector used by SLIC
    if(H1_HIC < H1_MAX_POSITION_P)
      H1_SLIC_PROGRESS_P = H1_SLIC_PROGRESS_P + 1
      H1_CUMULATIVE_SPEED_RATIO_P = H1_CUMULATIVE_SPEED_RATIO_P + H1_SPEED_RATIO_P
      H1_MAX_POSITION_P = H1_HIC

//********** Modification 19/11/01 To ensure gathering on DPR max pos reads 0 at first spark
// was   Q_EDM1_MAX_POS = 0 - H1_HIC
// Changed to

      if(@OFF(Q_EDM1_STATUS, EDMS_FIRST_SPARK))
        Q_EDM1_MAX_POS = 0 - H1_HIC
      else
        Q_EDM1_MAX_POS = H1_FSPOS_P - H1_HIC
      endif
//********** End of modification 19/11/01

      if(H1_SLIC_PROGRESS_P  > SLIC_INTEGRATION_COUNT)
        H1_SLIC_SPEED_P = H1_CUMULATIVE_SPEED_RATIO_P / H1_SLIC_PROGRESS_P
        Q_EDM1_SLIC = H1_SLIC_SPEED_P * 10000 // units of 1/100th %
        H1_SLIC_PROGRESS_P = 0
        H1_CUMULATIVE_SPEED_RATIO_P = 0
      endif
    endif

    // Added MaxPos Filter + Fwd Velocity
    if(H1_VELOCITY_COUNTER_P > VELOCITY_INTEGRATION - 1)
      // NOTE the Hold - Position give a positive value
      // * 2 added to divisor because ? (snecma scaling was wrong)
      Q_EDM1_VELOCITY = (H1_VEL_POS_HOLD_P - H1_MAX_POSITION_P ) / ((1/SERVO_FREQ) * VELOCITY_INTEGRATION)
      H1_VEL_POS_HOLD_P = H1_MAX_POSITION_P
      H1_VELOCITY_COUNTER_P = 0
    endif
	
  endif
  else
    Q_EDM1_VELOCITY = 0 // Mod 10-05-07 report zero velocity when frozen
  endif // freeze

  // 96*16 should be 96 * 32 except the internal encoder is actully the DSP gate register which
  // is a 2X register
  UAxisFollowErr_P = ((UAxisDesiredPos_M - UAxisCurrentPos_M) / (96 * 16)) * H1_ENC_SCALING

  // Force first spark logic, whenever the controller sets this bit then here is first spark
  if (@ON(X_EDM1_COMMAND, EDMC_FORCE_FIRST_SPARK))
     H1_FSPOS_P = H1_HIC
     @SET_ON(Q_EDM1_STATUS, EDMS_FIRST_SPARK)
  endif

  Q_EDM1_HIC = H1_HIC;
  if(@OFF(DPR_BUSY_M, BIT0))
    Q_EDM1_POSITION = H1_FSPOS_P - H1_HIC
  endif

  if(@OFF(X_EDM1_COMMAND, EDMC_REVERSE_DRILL))
    H1_DUPENC_M = H1_POS + H1_HIC
  else
    H1_DUPENC_M = H1_POS - H1_HIC
  endif

  H1_VELOCITY_COUNTER_P = H1_VELOCITY_COUNTER_P + 1
close





