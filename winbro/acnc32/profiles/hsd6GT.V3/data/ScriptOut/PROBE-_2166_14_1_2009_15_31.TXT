X PROBE RADIUS ##CA30 = 1.20736
Y PROBE RADIUS ##CA31 = 1.20194
Z PROBE RADIUS ##CA32 = 1.20465
X PROBE OFFSET ##CA35 = 393.802
Y PROBE OFFSET ##CA36 = -0.271972
Z PROBE OFFSET ##CA37 = 267.549
X TOOL OFFSET ##CA20  = -49.5997
Y TOOL OFFSET ##CA21  = 0.251972
Z TOOL OFFSET ##CA22  = -177.131
MEASURED WELL ECCENTRICITY  = 0
LOWERERROW X OFFSET  = -344.202
LOWERERROW Y OFFSET  = 0.02
LOWERERROW Z OFFSET  = -90.418
