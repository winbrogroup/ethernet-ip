[HSD6IIEDM]
HSD6IIEDM = {
  HoleProgram= {
    ID = 99
    Comment = 'HBH'
    Block = {
      GapVolts = 60.00
      Servo = 2000.00
      Peak = 0
      HighVoltage = 100
      PulseBoards = 7
      OnTime = 1000
      OffTime = 20000
      SuppOnTime = 1000
      SuppOffTime = 20000
      Depth = 100000
      Negative = 0
      Capacitance = 1
      ASV = 0.00
      Coupling = 0
      BusVoltage = 100
      AltBits = 0
      SpindleSpeed = 100.00
    }
    Block = {
      GapVolts = 60.00
      Servo = 1000.00
      Peak = 0
      HighVoltage = 100
      PulseBoards = 7
      OnTime = 1000
      OffTime = 1000
      SuppOnTime = 1000
      SuppOffTime = 20000
      Depth = 0
      Negative = 0
      Capacitance = 1
      ASV = 0.00
      Coupling = 0
      BusVoltage = 100
      AltBits = 0
      SpindleSpeed = 100.00
    }
  }
}
[NC32GCODE]
include Probing.txt
MDTExpand('Nominal','NOM_')

Call CCC_LoadTool
%Main
const CheckEyeBolts = 0
const FirstEyeBoltAngle = 20
const EyeBoltPitch =90
const EyeBoltCount =4
const EBZPos = 702
const EBXPos = 670
const ZSafe = 750
const DiameterTolerance = 2.5
const HeightTolerance = 1.5
const FirstProbeAngle = 10
const ProbePitch = 30
const ProbeCount = 12
const FirstProbeIndex = 15
const IndexesBetweenProbePoints = 45
Const BallatBZero   = 'BallatBZero'
Const BallatBNintey   = 'BallatBNintey'
Const FixtureZoffset = 0
Const VerticalProbeApproachDistance = 5
Const HorizontalProbeApproachDistance = 4
Const ProbeTravel = 20
Const VerticalProbeRadius = 345
Const VerticalProbeHeight = 295
const HorizontalProbeRadius = 338.4296
const HorizontalProbeHeight = 298
const VBProbeAngle = 0
const HBProbeAngle = 0
const AProbeAngle = 0

var MDT
var Success
var XError
var ZError
var ProbeCAngle

if (ACNC32Fake = 0) then
  Call CCC_Toolload F(2500)
end
G21

;
; START OF PROGRAM
;
  Call Preamble
  Call AllSafe
  if (CheckEyeBolts = 1) then
    Call CheckforEyeBolts
  end
  Call ProbeLoop
  Call AllSafe
M30

%Preamble
M26
G90 G01
GEOReset()
GEONAVReset()
GEOSetToolOffset(TouchProbeXToolOff, TouchProbeYToolOff, TouchProbeZToolOff)
GEO[5,0] = 0
GEO[5,1] = 0
GEO[5,2] = FixtureZoffset

GEONavSetNDPOffset(0,5,0,0,0)
_X = HorizontalProbeRadius+HorizontalProbeApproachDistance
_Y = 0
_Z = HorizontalProbeHeight
GEOAddPartPoint('HApproach',_X,_Y,_Z)
_X = HorizontalProbeRadius
GEOAddPartPoint('HProbe',_X,_Y,_Z)
_X = VerticalProbeRadius
_Y = 0
_Z = VerticalProbeHeight+VerticalProbeApproachDistance
GEOAddPartPoint('VApproach',_X,_Y,_Z)
_Z = VerticalProbeHeight
GEOAddPartPoint('VProbe',_X,_Y,_Z)
g04 f0.1
Call SetProbe
m01
;GeoMachineFromPoint('HApproach',AProbeAngle,HBProbeAngle,0,HAppProbePointPosnIndex) 
;GeoMachineFromPoint('HProbe',AProbeAngle,HBProbeAngle,0,HProbePointPosnIndex) 
;GeoMachineFromPoint('VApproach',AProbeAngle,VBProbeAngle,0,VAppProbePointPosnIndex) 
;GeoMachineFromPoint('VProbe',AProbeAngle,VBProbeAngle,0,VProbePointPosnIndex) 
if (StrComp(PartName,'')) then
  Notify('$PP_NO_PART_NAME_ENTERED')
  Error('$PP_NO_PART_NAME_ENTERED')
  M30
end else
  PartLife.Start(OPP_PartType,PartName)
  Call LoadMDT
end
Call ClearResults
PartLife.complete(0,0)
return

%HorizontalProbe
    Call ccc_Probe X(-ProbeTravel) Y0 Z0 F10 I0.08 N10
    G04
    Synch ; ensure data is sent
    G04
    GEO[11,0] = -1
    GEO[11,1] = 0
    GEO[11,2] = 0 
    CompensateV(10, 11, 12)
    
    GEO[13,0] = AProbeAngle   ; A angle
    GEO[13,1] = HBProbeAngle  ; B angle
    GEO[13,2] = 0  ; C
    GT.ResolveProbePoint(12,13,14)
    
    XError = GEO[14,0]- HorizontalProbeRadius
    if (abs(XError) > DiameterTolerance) then
      Error('Diameter Tolerance Error')
      M00
      M30
      end
return

%VerticalProbe
    Call ccc_Probe Z(-ProbeTravel) Y0 X0 F10 I0.08 N10
    G04
    Synch ; ensure data is sent
    G04
    GEO[11,0] = 0
    GEO[11,1] = 0
    GEO[11,2] = -1 
    CompensateV(10, 11, 12)
    ; ResolveBall for B at 0 and store result
    GEO[13,0] = AProbeAngle   ; A angle
    GEO[13,1] = VBProbeAngle  ; B angle
    GEO[13,2] = 0  ; C
    GT.ResolveProbePoint(12,13,14)
    m01
    ZError = GEO[14,2]-VerticalProbeHeight
    if (abs(ZError) > HeightTolerance) then
      Error('Height Tolerance Error')
      M00
      M30
      end
return

%Probeloop
Call SetBallActiveBatZero
_P = FirstProbeIndex
ProbeCAngle = FirstProbeAngle
A(AProbeAngle) B(VBProbeAngle)
;GeoMachineFromPoint('HApproach',AProbeAngle,HBProbeAngle,0,HAppProbePointPosnIndex) 
GeoMachineFromPoint('HApproach',AProbeAngle,HBProbeAngle,0,5) 
GeoMachineFromPoint('VApproach',AProbeAngle,VBProbeAngle,0,6) 
;GeoMachineFromPoint('VApproach',AProbeAngle,VBProbeAngle,0,VAppProbePointPosnIndex) 

X(GEO[6,0]) Y(GEO[6,1]) C(ProbeCAngle)
;setup abc angles for point resolve
for _I = 1 to ProbeCount do
  X(GEO[6,0]) Y(GEO[6,1])C(ProbeCAngle)
  Z(GEO[6,2])
  Call VerticalProbe I(_I)

  if (abs(ZError) > HeightTolerance) then
    Error('Height Tolerance Error')
    M00
    M30
    end
  X(GEO[5,0]) Y(GEO[5,1]) Z(GEO[5,2]+Zerror)
  
  Call HorizontalProbe I(_I)
  Z(GEO[5,2]+Zerror+15)
  MDTSetField(MDT,_P,'Xoff',XError)
  MDTSetField(MDT,_P,'Yoff',0)
  MDTSetField(MDT,_P,'Zoff',ZError)
  _P = _P+IndexesBetweenProbePoints
  PartLife.Complete(0,0)
  ; Move to next angle
  ProbeCAngle = ProbeCAngle+ProbePitch
end
Call InterpolateResults
Z(ZSafe)


return

%AllSafe
  if ((PZ[0]<720) and (PX[0] < 800)) then
    G91 G01
    GT.GetToolRetractVector(-30,16)
    G91 X(GEO[16,0]) Y(GEO[16,1]) Z(GEO[16,2]) F5000
  end
  G90 G01
  Z(ZSafe) F(5000)
  X900 Y0 F(5000)
  B(HBProbeAngle) A(AProbeAngle) C0 F(2500)
  G04
Return

%LoadMDT
  if(MDTUse('Nominal', MDT)) then
    Error('No Probing Results MDT defined')
  end
return


%CheckforEyeBolts
if(PZ[0] < 700) then
 Error('Not at safe position to check for EyeBolts')
 m00
end
G90 g01 A-180 B90 x800 y0 F5000
ProbeCAngle = FirstEyeBoltAngle
C(ProbeCAngle)
Z(EBZPos)
X(EBXPos)

for _I = 1 to EyeBoltCount do
   Call CheckNoProbe X(-ProbeTravel) Y0 Z0 F10 I0.08 N10
   ProbeCAngle = ProbeCAngle+EyeBoltPitch
   X(EBXPos+50)
   C(ProbeCAngle)
   X(EBXPos)
end
Call AllSafe
return

%InterpolateResults
GT.InterpolateMDTCol('Nominal','Xoff',0,539)
GT.InterpolateMDTCol('Nominal','Zoff',0,539)
PartLife.complete(0,0)
return

%ClearResults
for _I = 0 to (MDTRecords(MDT)-1) do
  MDTSetField(MDT,_I,'Xoff','')
  MDTSetField(MDT,_I,'Zoff','')
end
return
[OPERATIONDESCRIPTION]
PartType = 'CHEMISEINTER'
Comment = 'Snecma'
ToolTypeString = PROBE
ToolUsage = 85
CycleTime = 340
ElectrodeCount = 1
ElectrodeMap = 000000000100
ToolXOffset = -25.104
ToolYOffset = 0
ToolZOffset = -134.435
OpNumber = 1
AEC_Selected = 0
AEC_InchEntry = 0
AEC_Restroke = 58
AEC_StowFeed = 3000
AEC_CheckHoleProgram = 99
AEC_QualifyStroke = 40
AEC_QualifyRetryLoops = 3
AEC_ElectrodeClearance = 2
AEC_LoadPositionU = 0
AEC_EjectLength = 10
AEC_DoubleIndex = 0
AEC_FirstPulseDuration = 0.2
AEC_SecondPulseDuration = 0.2
AEC_EjectPulseCount = 1
AEC_EjectPulse1Duration = 0.5
AEC_EjectPulse2Duration = 0.5
AEC_LoadPulseCount = 1
[NOTOPERATIONDESCRIPTION]
PartType = 'NONE'
Comment = 'No Ref'
ToolTypeString = AEC
ToolUsage = 0
CycleTime = 0
ElectrodeCount = 0
ElectrodeMap = 000000000000
ToolXOffset = -49.2
ToolYOffset = 0
ToolZOffset = -90
OpNumber = 1
AEC_Selected = 1
AEC_InchEntry = 0
AEC_Restroke = 30
AEC_StowFeed = 2000
AEC_CheckHoleProgram = 0
AEC_QualifyStroke = 25
AEC_QualifyRetryLoops = 3
AEC_ElectrodeClearance = 2
AEC_LoadPositionU = 0
AEC_EjectLength = 10
AEC_DoubleIndex = 0
AEC_FirstPulseDuration = 1
AEC_SecondPulseDuration = 1
AEC_EjectPulseCount = 1
AEC_EjectPulse1Duration = 1
AEC_EjectPulse2Duration = 1
AEC_LoadPulseCount = 1
[HSD6IIEDM]
HSD6IIEDM = {
}
[MDTDATA]
