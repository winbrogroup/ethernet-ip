[UNITS]
InchMode = False
[FANDATA]
XRightIsCurved=False
XRightAngle= 10.000
XRightWidth=  0.479
;-----------
XLeftIsCurved=False
XLeftAngle= 10.000
XLeftWidth=  0.479
;-----------
YRightIsCurved=False
YRightAngle=  0.000
YRightWidth=  0.215
;-----------
YLeftIsCurved=False
YLeftAngle= 10.000
YLeftWidth=  0.479
;-----------
XSurfaceAngle=  0.000
XSkewAngle= 90.000
XCentreDepth=  1.500
YSurfaceAngle=-70.000
YSkewAngle= 90.000
YCentreDepth=  1.500
;-----------
Bore=  0.430
ElectrodeDiameter=  0.430
DepthIncrement=  0.100
ElectrodeWear= 60.000
FeedRate= 75.000
BoreFixed=False
Rotation=  0.000
CAxisRotation=  0.000
LeftRotation=  0.000
RightRotation=  0.000
TopRotation=  0.000
BottomRotation=  0.000
BoreFixed=False
LockRotation=False
EnableMinSliceTime=False
MinSliceTime=  0.500
WearReductionGain=  0.000
