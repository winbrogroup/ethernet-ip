[UNITS]
InchMode = False
[FANDATA]
XRightIsCurved=False
XRightAngle= 12.000
XRightWidth=  0.360
;-----------
XLeftIsCurved=False
XLeftAngle= 12.000
XLeftWidth=  0.360
;-----------
YRightIsCurved=False
YRightAngle=  0.720
YRightWidth=  0.214
;-----------
YLeftIsCurved=False
YLeftAngle=  0.720
YLeftWidth=  0.214
;-----------
XSurfaceAngle=  0.000
XSkewAngle= 90.000
XCentreDepth=  0.730
YSurfaceAngle=-70.000
YSkewAngle= 90.000
YCentreDepth=  0.730
;-----------
Bore=  0.410
ElectrodeDiameter=  0.330
DepthIncrement=  0.100
ElectrodeWear= 80.000
FeedRate=150.000
BoreFixed=False
Rotation=  0.000
CAxisRotation=  0.000
LeftRotation=  0.000
RightRotation=  0.000
TopRotation=  0.000
BottomRotation=  0.000
BoreFixed=False
LockRotation=False
EnableMinSliceTime=False
MinSliceTime=  0.500
WearReductionGain= 30.000
