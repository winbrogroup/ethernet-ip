[UNITS]
InchMode = False
[FANDATA]
XRightIsCurved=False
XRightAngle= 12.000
XRightWidth=  0.653
;-----------
XLeftIsCurved=False
XLeftAngle= 12.000
XLeftWidth=  0.653
;-----------
YRightIsCurved=False
YRightAngle=  0.720
YRightWidth=  0.227
;-----------
YLeftIsCurved=False
YLeftAngle=  0.720
YLeftWidth=  0.227
;-----------
XSurfaceAngle=  0.000
XSkewAngle= 90.000
XCentreDepth=  2.131
YSurfaceAngle=-69.060
YSkewAngle= 90.000
YCentreDepth=  2.131
;-----------
Bore=  0.400
ElectrodeDiameter=  0.400
DepthIncrement=  0.100
ElectrodeWear= 75.000
FeedRate= 80.000
BoreFixed=False
Rotation=  0.000
CAxisRotation=  0.000
LeftRotation=  0.000
RightRotation=  0.000
TopRotation=  0.000
BottomRotation=  0.000
BoreFixed=False
LockRotation=False
EnableMinSliceTime=False
MinSliceTime=  0.500
WearReductionGain= 15.000
