[UNITS]
InchMode = False
[FANDATA]
XRightIsCurved=False
XRightAngle= 12.000
XRightWidth=  0.593
;-----------
XLeftIsCurved=False
XLeftAngle= 12.000
XLeftWidth=  0.593
;-----------
YRightIsCurved=False
YRightAngle=  0.720
YRightWidth=  0.223
;-----------
YLeftIsCurved=False
YLeftAngle=  0.720
YLeftWidth=  0.223
;-----------
XSurfaceAngle=  0.000
XSkewAngle= 90.000
XCentreDepth=  1.849
YSurfaceAngle=-71.840
YSkewAngle= 90.000
YCentreDepth=  1.849
;-----------
Bore=  0.400
ElectrodeDiameter=  0.400
DepthIncrement=  0.100
ElectrodeWear= 75.000
FeedRate= 80.000
BoreFixed=False
Rotation=  0.000
CAxisRotation=  0.000
LeftRotation=  0.000
RightRotation=  0.000
TopRotation=  0.000
BottomRotation=  0.000
BoreFixed=False
LockRotation=False
EnableMinSliceTime=False
MinSliceTime=  0.500
WearReductionGain= 15.000
