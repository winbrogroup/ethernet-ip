;Machine Type  : HSD6 
;Serial Number : 2222
;Customer      : Amchem
;
; Date 24/05/2006
; Time 14:44:09
DELETE GATHER
DELETE ROTARY
#1 DELETE BLCOMP
#2 DELETE BLCOMP
#3 DELETE BLCOMP
#4 DELETE BLCOMP
#5 DELETE BLCOMP
#6 DELETE BLCOMP
#1 DELETE COMP
I186 = 0            ; clear overall backlash
#2 DELETE COMP
I286 = 0            ; clear overall backlash
#3 DELETE COMP
I386 = 0            ; clear overall backlash
#4 DELETE COMP
I486 = 0            ; clear overall backlash
#5 DELETE COMP
I586 = 0            ; clear overall backlash
#6 DELETE COMP
I686 = 0            ; clear overall backlash
;A Axis Compensation
; Encoder Direction is not Reversed
;Home Offset at -179.673 degrees
;Encoder Counts per degree = 1600
;72 Entries over 576000 encoder Counts
;Uses Motor Number 6
#6
DEFINE COMP 72,576000
115      ;POS = -174.673    ENCODER = 8000
226      ;POS = -169.673    ENCODER = 16000
277      ;POS = -164.673    ENCODER = 24000
331      ;POS = -159.673    ENCODER = 32000
421      ;POS = -154.673    ENCODER = 40000
509      ;POS = -149.673    ENCODER = 48000
586      ;POS = -144.673    ENCODER = 56000
666      ;POS = -139.673    ENCODER = 64000
794      ;POS = -134.673    ENCODER = 72000
927      ;POS = -129.673    ENCODER = 80000
1119     ;POS = -124.673    ENCODER = 88000
1309     ;POS = -119.673    ENCODER = 96000
1475     ;POS = -114.673    ENCODER = 104000
1632     ;POS = -109.673    ENCODER = 112000
1645     ;POS = -104.673    ENCODER = 120000
1654     ;POS = -99.673     ENCODER = 128000
1616     ;POS = -94.673     ENCODER = 136000
1583     ;POS = -89.673     ENCODER = 144000
1634     ;POS = -84.673     ENCODER = 152000
1681     ;POS = -79.673     ENCODER = 160000
1668     ;POS = -74.673     ENCODER = 168000
1659     ;POS = -69.673     ENCODER = 176000
1697     ;POS = -64.673     ENCODER = 184000
1739     ;POS = -59.673     ENCODER = 192000
1829     ;POS = -54.673     ENCODER = 200000
1906     ;POS = -49.673     ENCODER = 208000
1803     ;POS = -44.673     ENCODER = 216000
1703     ;POS = -39.673     ENCODER = 224000
1639     ;POS = -34.673     ENCODER = 232000
1573     ;POS = -29.673     ENCODER = 240000
1471     ;POS = -24.673     ENCODER = 248000
1366     ;POS = -19.673     ENCODER = 256000
1225     ;POS = -14.673     ENCODER = 264000
1080     ;POS = -9.673      ENCODER = 272000
875      ;POS = -4.673      ENCODER = 280000
685      ;POS = 0.327       ENCODER = 288000
711      ;POS = 5.327       ENCODER = 296000
726      ;POS = 10.327      ENCODER = 304000
598      ;POS = 15.327      ENCODER = 312000
465      ;POS = 20.327      ENCODER = 320000
247      ;POS = 25.327      ENCODER = 328000
33       ;POS = 30.327      ENCODER = 336000
-134     ;POS = 35.327      ENCODER = 344000
-294     ;POS = 40.327      ENCODER = 352000
-371     ;POS = 45.327      ENCODER = 360000
-453     ;POS = 50.327      ENCODER = 368000
-606     ;POS = 55.327      ENCODER = 376000
-753     ;POS = 60.327      ENCODER = 384000
-804     ;POS = 65.327      ENCODER = 392000
-841     ;POS = 70.327      ENCODER = 400000
-661     ;POS = 75.327      ENCODER = 408000
-486     ;POS = 80.327      ENCODER = 416000
-358     ;POS = 85.327      ENCODER = 424000
-230     ;POS = 90.327      ENCODER = 432000
-115     ;POS = 95.327      ENCODER = 440000
-9       ;POS = 100.327     ENCODER = 448000
-35      ;POS = 105.327     ENCODER = 456000
-49      ;POS = 110.327     ENCODER = 464000
105      ;POS = 115.327     ENCODER = 472000
254      ;POS = 120.327     ENCODER = 480000
344      ;POS = 125.327     ENCODER = 488000
423      ;POS = 130.327     ENCODER = 496000
346      ;POS = 135.327     ENCODER = 504000
277      ;POS = 140.327     ENCODER = 512000
329      ;POS = 145.327     ENCODER = 520000
384      ;POS = 150.327     ENCODER = 528000
499      ;POS = 155.327     ENCODER = 536000
597      ;POS = 160.327     ENCODER = 544000
443      ;POS = 165.327     ENCODER = 552000
300      ;POS = 170.327     ENCODER = 560000
300      ;POS = 175.327     ENCODER = 568000
0        ;POS = -179.673    ENCODER = 0
;
;
;
;C Axis Compensation
; Encoder Direction is not Reversed
;Home Offset at -0.337  degrees
;Encoder Counts per degree = 3454
;14 Entries over 48356 encoder Counts
;Uses Motor Number 5
#5
DEFINE COMP 14,48356
-2841    ;POS = -1.337      ENCODER = 3454
-7175    ;POS = -2.337      ENCODER = 6908
-12984   ;POS = -3.337      ENCODER = 10362
-20563   ;POS = -4.337      ENCODER = 13816
-30261   ;POS = -5.337      ENCODER = 17270
-1703    ;POS = 7.663       ENCODER = 20724
-1703    ;POS = 6.663       ENCODER = 24178
-977     ;POS = 5.663       ENCODER = 27632
973      ;POS = 4.663       ENCODER = 31086
2316     ;POS = 3.663       ENCODER = 34540
3032     ;POS = 2.663       ENCODER = 37994
3010     ;POS = 1.663       ENCODER = 41448
2086     ;POS = 0.663       ENCODER = 44902
184      ;POS = -0.337      ENCODER = 0
;
;
;
;B Axis Compensation
; Encoder Direction is not Reversed
;Home Offset at 70.385  degrees
;Encoder Counts per degree = 1000
;54 Entries over 270000 encoder Counts
;Uses Motor Number 4
#4
DEFINE COMP 54,270000
40       ;POS = 75.385      ENCODER = 5000
78       ;POS = 80.385      ENCODER = 10000
94       ;POS = 85.385      ENCODER = 15000
111      ;POS = 90.385      ENCODER = 20000
135      ;POS = 95.385      ENCODER = 25000
157      ;POS = 100.385     ENCODER = 30000
157      ;POS = 105.385     ENCODER = 35000
157      ;POS = 110.385     ENCODER = 40000
157      ;POS = 115.385     ENCODER = 45000
157      ;POS = 120.385     ENCODER = 50000
157      ;POS = 125.385     ENCODER = 55000
-178     ;POS = -139.615    ENCODER = 60000
-170     ;POS = -134.615    ENCODER = 65000
-162     ;POS = -129.615    ENCODER = 70000
-154     ;POS = -124.615    ENCODER = 75000
-148     ;POS = -119.615    ENCODER = 80000
-164     ;POS = -114.615    ENCODER = 85000
-179     ;POS = -109.615    ENCODER = 90000
-179     ;POS = -104.615    ENCODER = 95000
-181     ;POS = -99.615     ENCODER = 100000
-205     ;POS = -94.615     ENCODER = 105000
-226     ;POS = -89.615     ENCODER = 110000
-210     ;POS = -84.615     ENCODER = 115000
-196     ;POS = -79.615     ENCODER = 120000
-212     ;POS = -74.615     ENCODER = 125000
-228     ;POS = -69.615     ENCODER = 130000
-236     ;POS = -64.615     ENCODER = 135000
-243     ;POS = -59.615     ENCODER = 140000
-243     ;POS = -54.615     ENCODER = 145000
-244     ;POS = -49.615     ENCODER = 150000
-252     ;POS = -44.615     ENCODER = 155000
-257     ;POS = -39.615     ENCODER = 160000
-233     ;POS = -34.615     ENCODER = 165000
-212     ;POS = -29.615     ENCODER = 170000
-220     ;POS = -24.615     ENCODER = 175000
-225     ;POS = -19.615     ENCODER = 180000
-201     ;POS = -14.615     ENCODER = 185000
-180     ;POS = -9.615      ENCODER = 190000
-188     ;POS = -4.615      ENCODER = 195000
-194     ;POS = 0.385       ENCODER = 200000
-178     ;POS = 5.385       ENCODER = 205000
-162     ;POS = 10.385      ENCODER = 210000
-146     ;POS = 15.385      ENCODER = 215000
-131     ;POS = 20.385      ENCODER = 220000
-131     ;POS = 25.385      ENCODER = 225000
-130     ;POS = 30.385      ENCODER = 230000
-114     ;POS = 35.385      ENCODER = 235000
-97      ;POS = 40.385      ENCODER = 240000
-73      ;POS = 45.385      ENCODER = 245000
-51      ;POS = 50.385      ENCODER = 250000
-51      ;POS = 55.385      ENCODER = 255000
-49      ;POS = 60.385      ENCODER = 260000
-25      ;POS = 65.385      ENCODER = 265000
0        ;POS = 70.385      ENCODER = 0
;
;
;
;Z Axis Compensation
; Encoder Direction is not Reversed
;Home Offset at 220.337 mm.
;Encoder Counts per mm. = 800
;27 Entries over 216000 encoder Counts
;Uses Motor Number 3
#3
DEFINE COMP 27,216000
-4       ;POS = 230.337     ENCODER = 8000
-8       ;POS = 240.337     ENCODER = 16000
-500     ;POS = -19.663     ENCODER = 24000
-419     ;POS = -9.663      ENCODER = 32000
-366     ;POS = 0.337       ENCODER = 40000
-350     ;POS = 10.337      ENCODER = 48000
-339     ;POS = 20.337      ENCODER = 56000
-331     ;POS = 30.337      ENCODER = 64000
-308     ;POS = 40.337      ENCODER = 72000
-282     ;POS = 50.337      ENCODER = 80000
-250     ;POS = 60.337      ENCODER = 88000
-221     ;POS = 70.337      ENCODER = 96000
-199     ;POS = 80.337      ENCODER = 104000
-184     ;POS = 90.337      ENCODER = 112000
-178     ;POS = 100.337     ENCODER = 120000
-170     ;POS = 110.337     ENCODER = 128000
-158     ;POS = 120.337     ENCODER = 136000
-144     ;POS = 130.337     ENCODER = 144000
-124     ;POS = 140.337     ENCODER = 152000
-101     ;POS = 150.337     ENCODER = 160000
-84      ;POS = 160.337     ENCODER = 168000
-70      ;POS = 170.337     ENCODER = 176000
-49      ;POS = 180.337     ENCODER = 184000
-25      ;POS = 190.337     ENCODER = 192000
-5       ;POS = 200.337     ENCODER = 200000
6        ;POS = 210.337     ENCODER = 208000
0        ;POS = 220.337     ENCODER = 0
;
;
;
;Y Axis Compensation
; Encoder Direction is not Reversed
;Home Offset at -63.496 mm.
;Encoder Counts per mm. = 800
;44 Entries over 176000 encoder Counts
;Uses Motor Number 2
#2
DEFINE COMP 44,176000
-9       ;POS = -58.496     ENCODER = 4000
-18      ;POS = -53.496     ENCODER = 8000
-24      ;POS = -48.496     ENCODER = 12000
-21      ;POS = -43.496     ENCODER = 16000
-21      ;POS = -38.496     ENCODER = 20000
-27      ;POS = -33.496     ENCODER = 24000
-33      ;POS = -28.496     ENCODER = 28000
-39      ;POS = -23.496     ENCODER = 32000
-42      ;POS = -18.496     ENCODER = 36000
-39      ;POS = -13.496     ENCODER = 40000
-38      ;POS = -8.496      ENCODER = 44000
-41      ;POS = -3.496      ENCODER = 48000
-42      ;POS = 1.504       ENCODER = 52000
-43      ;POS = 6.504       ENCODER = 56000
-42      ;POS = 11.504      ENCODER = 60000
-38      ;POS = 16.504      ENCODER = 64000
-36      ;POS = 21.504      ENCODER = 68000
-34      ;POS = 26.504      ENCODER = 72000
-33      ;POS = 31.504      ENCODER = 76000
-33      ;POS = 36.504      ENCODER = 80000
-34      ;POS = 41.504      ENCODER = 84000
-35      ;POS = 46.504      ENCODER = 88000
-38      ;POS = 51.504      ENCODER = 92000
-42      ;POS = 56.504      ENCODER = 96000
-47      ;POS = 61.504      ENCODER = 100000
-51      ;POS = 66.504      ENCODER = 104000
-56      ;POS = 71.504      ENCODER = 108000
-60      ;POS = 76.504      ENCODER = 112000
-65      ;POS = 81.504      ENCODER = 116000
-74      ;POS = 86.504      ENCODER = 120000
-81      ;POS = 91.504      ENCODER = 124000
-82      ;POS = 96.504      ENCODER = 128000
-83      ;POS = 101.504     ENCODER = 132000
-83      ;POS = 106.504     ENCODER = 136000
135      ;POS = -108.496    ENCODER = 140000
113      ;POS = -103.496    ENCODER = 144000
92       ;POS = -98.496     ENCODER = 148000
70       ;POS = -93.496     ENCODER = 152000
50       ;POS = -88.496     ENCODER = 156000
33       ;POS = -83.496     ENCODER = 160000
20       ;POS = -78.496     ENCODER = 164000
16       ;POS = -73.496     ENCODER = 168000
10       ;POS = -68.496     ENCODER = 172000
0        ;POS = -63.496     ENCODER = 0
;
;
;
;A Axis Backlash Compensation
; Encoder Direction is not Reversed
;Home Offset at -179.673 degrees
;Encoder Counts per degree = 1600
;72 Entries over 576000 encoder Counts
;Uses Motor Number 6
#6
I686 = 0            ; set overall backlash
DEFINE BLCOMP 72,576000
0        ;POS = -174.673    ENCODER = 8000
1        ;POS = -169.673    ENCODER = 16000
14       ;POS = -164.673    ENCODER = 24000
25       ;POS = -159.673    ENCODER = 32000
12       ;POS = -154.673    ENCODER = 40000
0        ;POS = -149.673    ENCODER = 48000
0        ;POS = -144.673    ENCODER = 56000
1        ;POS = -139.673    ENCODER = 64000
14       ;POS = -134.673    ENCODER = 72000
26       ;POS = -129.673    ENCODER = 80000
39       ;POS = -124.673    ENCODER = 88000
50       ;POS = -119.673    ENCODER = 96000
38       ;POS = -114.673    ENCODER = 104000
25       ;POS = -109.673    ENCODER = 112000
12       ;POS = -104.673    ENCODER = 120000
0        ;POS = -99.673     ENCODER = 128000
0        ;POS = -94.673     ENCODER = 136000
1        ;POS = -89.673     ENCODER = 144000
14       ;POS = -84.673     ENCODER = 152000
26       ;POS = -79.673     ENCODER = 160000
26       ;POS = -74.673     ENCODER = 168000
26       ;POS = -69.673     ENCODER = 176000
39       ;POS = -64.673     ENCODER = 184000
50       ;POS = -59.673     ENCODER = 192000
38       ;POS = -54.673     ENCODER = 200000
25       ;POS = -49.673     ENCODER = 208000
12       ;POS = -44.673     ENCODER = 216000
-1       ;POS = -39.673     ENCODER = 224000
-14      ;POS = -34.673     ENCODER = 232000
-25      ;POS = -29.673     ENCODER = 240000
-12      ;POS = -24.673     ENCODER = 248000
0        ;POS = -19.673     ENCODER = 256000
0        ;POS = -14.673     ENCODER = 264000
-1       ;POS = -9.673      ENCODER = 272000
-14      ;POS = -4.673      ENCODER = 280000
-26      ;POS = 0.327       ENCODER = 288000
-26      ;POS = 5.327       ENCODER = 296000
-26      ;POS = 10.327      ENCODER = 304000
-39      ;POS = 15.327      ENCODER = 312000
-51      ;POS = 20.327      ENCODER = 320000
-51      ;POS = 25.327      ENCODER = 328000
-51      ;POS = 30.327      ENCODER = 336000
-51      ;POS = 35.327      ENCODER = 344000
-51      ;POS = 40.327      ENCODER = 352000
-51      ;POS = 45.327      ENCODER = 360000
-52      ;POS = 50.327      ENCODER = 368000
-65      ;POS = 55.327      ENCODER = 376000
-77      ;POS = 60.327      ENCODER = 384000
-77      ;POS = 65.327      ENCODER = 392000
-75      ;POS = 70.327      ENCODER = 400000
-50      ;POS = 75.327      ENCODER = 408000
-26      ;POS = 80.327      ENCODER = 416000
-26      ;POS = 85.327      ENCODER = 424000
-26      ;POS = 90.327      ENCODER = 432000
-26      ;POS = 95.327      ENCODER = 440000
-25      ;POS = 100.327     ENCODER = 448000
-12      ;POS = 105.327     ENCODER = 456000
0        ;POS = 110.327     ENCODER = 464000
0        ;POS = 115.327     ENCODER = 472000
0        ;POS = 120.327     ENCODER = 480000
0        ;POS = 125.327     ENCODER = 488000
-1       ;POS = 130.327     ENCODER = 496000
-14      ;POS = 135.327     ENCODER = 504000
-24      ;POS = 140.327     ENCODER = 512000
2        ;POS = 145.327     ENCODER = 520000
25       ;POS = 150.327     ENCODER = 528000
12       ;POS = 155.327     ENCODER = 536000
0        ;POS = 160.327     ENCODER = 544000
0        ;POS = 165.327     ENCODER = 552000
0        ;POS = 170.327     ENCODER = 560000
0        ;POS = 175.327     ENCODER = 568000
0        ;POS = -179.673    ENCODER = 0
I685 = 32
;
;
;
;C Axis Backlash Compensation
; Encoder Direction is not Reversed
;Home Offset at -0.337  degrees
;Encoder Counts per degree = 3454
;14 Entries over 48356 encoder Counts
;Uses Motor Number 5
#5
I586 = 184            ; set overall backlash
DEFINE BLCOMP 14,48356
37       ;POS = -1.337      ENCODER = 3454
56       ;POS = -2.337      ENCODER = 6908
92       ;POS = -3.337      ENCODER = 10362
204      ;POS = -4.337      ENCODER = 13816
349      ;POS = -5.337      ENCODER = 17270
-184     ;POS = 7.663       ENCODER = 20724
-184     ;POS = 6.663       ENCODER = 24178
-147     ;POS = 5.663       ENCODER = 27632
-73      ;POS = 4.663       ENCODER = 31086
-73      ;POS = 3.663       ENCODER = 34540
-55      ;POS = 2.663       ENCODER = 37994
-18      ;POS = 1.663       ENCODER = 41448
-18      ;POS = 0.663       ENCODER = 44902
0        ;POS = -0.337      ENCODER = 0
I585 = 32
;
;
;
;B Axis Backlash Compensation
; Encoder Direction is not Reversed
;Home Offset at 70.385  degrees
;Encoder Counts per degree = 1000
;54 Entries over 270000 encoder Counts
;Uses Motor Number 4
#4
I486 = -33            ; set overall backlash
DEFINE BLCOMP 54,270000
-8       ;POS = 75.385      ENCODER = 5000
-16      ;POS = 80.385      ENCODER = 10000
-24      ;POS = 85.385      ENCODER = 15000
-32      ;POS = 90.385      ENCODER = 20000
-40      ;POS = 95.385      ENCODER = 25000
-46      ;POS = 100.385     ENCODER = 30000
-30      ;POS = 105.385     ENCODER = 35000
-15      ;POS = 110.385     ENCODER = 40000
-15      ;POS = 115.385     ENCODER = 45000
-15      ;POS = 120.385     ENCODER = 50000
-15      ;POS = 125.385     ENCODER = 55000
64       ;POS = -139.615    ENCODER = 60000
56       ;POS = -134.615    ENCODER = 65000
48       ;POS = -129.615    ENCODER = 70000
40       ;POS = -124.615    ENCODER = 75000
33       ;POS = -119.615    ENCODER = 80000
33       ;POS = -114.615    ENCODER = 85000
31       ;POS = -109.615    ENCODER = 90000
7        ;POS = -104.615    ENCODER = 95000
-16      ;POS = -99.615     ENCODER = 100000
-24      ;POS = -94.615     ENCODER = 105000
-32      ;POS = -89.615     ENCODER = 110000
-40      ;POS = -84.615     ENCODER = 115000
-47      ;POS = -79.615     ENCODER = 120000
-47      ;POS = -74.615     ENCODER = 125000
-45      ;POS = -69.615     ENCODER = 130000
-13      ;POS = -64.615     ENCODER = 135000
16       ;POS = -59.615     ENCODER = 140000
8        ;POS = -54.615     ENCODER = 145000
1        ;POS = -49.615     ENCODER = 150000
1        ;POS = -44.615     ENCODER = 155000
0        ;POS = -39.615     ENCODER = 160000
-8       ;POS = -34.615     ENCODER = 165000
-14      ;POS = -29.615     ENCODER = 170000
-6       ;POS = -24.615     ENCODER = 175000
1        ;POS = -19.615     ENCODER = 180000
1        ;POS = -14.615     ENCODER = 185000
1        ;POS = -9.615      ENCODER = 190000
1        ;POS = -4.615      ENCODER = 195000
2        ;POS = 0.385       ENCODER = 200000
18       ;POS = 5.385       ENCODER = 205000
31       ;POS = 10.385      ENCODER = 210000
7        ;POS = 15.385      ENCODER = 215000
-13      ;POS = 20.385      ENCODER = 220000
19       ;POS = 25.385      ENCODER = 225000
47       ;POS = 30.385      ENCODER = 230000
23       ;POS = 35.385      ENCODER = 235000
0        ;POS = 40.385      ENCODER = 240000
-8       ;POS = 45.385      ENCODER = 245000
-13      ;POS = 50.385      ENCODER = 250000
11       ;POS = 55.385      ENCODER = 255000
32       ;POS = 60.385      ENCODER = 260000
16       ;POS = 65.385      ENCODER = 265000
0        ;POS = 70.385      ENCODER = 0
I485 = 32
;
;
;
;Z Axis Backlash Compensation
; Encoder Direction is not Reversed
;Home Offset at 220.337 mm.
;Encoder Counts per mm. = 800
;27 Entries over 216000 encoder Counts
;Uses Motor Number 3
#3
I386 = -62            ; set overall backlash
DEFINE BLCOMP 27,216000
44       ;POS = 230.337     ENCODER = 8000
97       ;POS = 240.337     ENCODER = 16000
158      ;POS = -19.663     ENCODER = 24000
89       ;POS = -9.663      ENCODER = 32000
48       ;POS = 0.337       ENCODER = 40000
46       ;POS = 10.337      ENCODER = 48000
54       ;POS = 20.337      ENCODER = 56000
58       ;POS = 30.337      ENCODER = 64000
55       ;POS = 40.337      ENCODER = 72000
52       ;POS = 50.337      ENCODER = 80000
47       ;POS = 60.337      ENCODER = 88000
41       ;POS = 70.337      ENCODER = 96000
34       ;POS = 80.337      ENCODER = 104000
34       ;POS = 90.337      ENCODER = 112000
39       ;POS = 100.337     ENCODER = 120000
39       ;POS = 110.337     ENCODER = 128000
38       ;POS = 120.337     ENCODER = 136000
36       ;POS = 130.337     ENCODER = 144000
31       ;POS = 140.337     ENCODER = 152000
25       ;POS = 150.337     ENCODER = 160000
21       ;POS = 160.337     ENCODER = 168000
17       ;POS = 170.337     ENCODER = 176000
8        ;POS = 180.337     ENCODER = 184000
-1       ;POS = 190.337     ENCODER = 192000
-12      ;POS = 200.337     ENCODER = 200000
-13      ;POS = 210.337     ENCODER = 208000
0        ;POS = 220.337     ENCODER = 0
I385 = 32
;
;
;
;Y Axis Backlash Compensation
; Encoder Direction is not Reversed
;Home Offset at -63.496 mm.
;Encoder Counts per mm. = 800
;44 Entries over 176000 encoder Counts
;Uses Motor Number 2
#2
I286 = -17            ; set overall backlash
DEFINE BLCOMP 44,176000
3        ;POS = -58.496     ENCODER = 4000
7        ;POS = -53.496     ENCODER = 8000
8        ;POS = -48.496     ENCODER = 12000
3        ;POS = -43.496     ENCODER = 16000
-1       ;POS = -38.496     ENCODER = 20000
0        ;POS = -33.496     ENCODER = 24000
0        ;POS = -28.496     ENCODER = 28000
-2       ;POS = -23.496     ENCODER = 32000
-3       ;POS = -18.496     ENCODER = 36000
-2       ;POS = -13.496     ENCODER = 40000
-2       ;POS = -8.496      ENCODER = 44000
-2       ;POS = -3.496      ENCODER = 48000
-2       ;POS = 1.504       ENCODER = 52000
-3       ;POS = 6.504       ENCODER = 56000
-4       ;POS = 11.504      ENCODER = 60000
-1       ;POS = 16.504      ENCODER = 64000
-1       ;POS = 21.504      ENCODER = 68000
-4       ;POS = 26.504      ENCODER = 72000
-7       ;POS = 31.504      ENCODER = 76000
-7       ;POS = 36.504      ENCODER = 80000
-7       ;POS = 41.504      ENCODER = 84000
-6       ;POS = 46.504      ENCODER = 88000
-6       ;POS = 51.504      ENCODER = 92000
-5       ;POS = 56.504      ENCODER = 96000
-5       ;POS = 61.504      ENCODER = 100000
-4       ;POS = 66.504      ENCODER = 104000
-3       ;POS = 71.504      ENCODER = 108000
-2       ;POS = 76.504      ENCODER = 112000
-2       ;POS = 81.504      ENCODER = 116000
0        ;POS = 86.504      ENCODER = 120000
0        ;POS = 91.504      ENCODER = 124000
-2       ;POS = 96.504      ENCODER = 128000
-3       ;POS = 101.504     ENCODER = 132000
-3       ;POS = 106.504     ENCODER = 136000
34       ;POS = -108.496    ENCODER = 140000
25       ;POS = -103.496    ENCODER = 144000
17       ;POS = -98.496     ENCODER = 148000
9        ;POS = -93.496     ENCODER = 152000
2        ;POS = -88.496     ENCODER = 156000
1        ;POS = -83.496     ENCODER = 160000
0        ;POS = -78.496     ENCODER = 164000
-1       ;POS = -73.496     ENCODER = 168000
-1       ;POS = -68.496     ENCODER = 172000
0        ;POS = -63.496     ENCODER = 0
I285 = 32
;
;
;
I51 = 1
