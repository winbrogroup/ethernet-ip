// OpenerLibrary.c : Defines the exported functions for the DLL application.

#include <assert.h>
#include <string.h>

#include "OpenerLibrary.h"
#include "opener_api.h"
#include "generic_networkhandler.h"


static OpenerLibConfigInfo openerlib_config_info;

#define DEMO_APP_INPUT_ASSEMBLY_NUM                100 //0x064 //Class 0x4, instance 100, Attribute 3 --> entire array(GetAttributeSingle service=14/0xE, --> Attribute 0 not defined)
#define DEMO_APP_OUTPUT_ASSEMBLY_NUM               150 //0x096
#define DEMO_APP_CONFIG_ASSEMBLY_NUM               151 //0x097
#define DEMO_APP_HEARTBEAT_INPUT_ONLY_ASSEMBLY_NUM  152 //0x098
#define DEMO_APP_HEARTBEAT_LISTEN_ONLY_ASSEMBLY_NUM 153 //0x099
#define DEMO_APP_EXPLICT_ASSEMBLY_NUM              154 //0x09A

EipUint8 g_assembly_data064[32]; /* Input */
EipUint8 g_assembly_data096[32]; /* Output */
EipUint8 g_assembly_data097[10]; /* Config */
EipUint8 g_assembly_data09A[32]; /* Explicit */

int ConfigureNIC(const char *const hostname,
	const char *const domain_name,
	const char *const ip_address, 
	const char *const subnet_mask, 
	const char *const gateway_address,
	const char *const acBuffer_mac_address[]) {
	
	//Store a copy of the configuration used...
	strcpy_s(openerlib_config_info.host_name, sizeof(openerlib_config_info.host_name), hostname);
	strcpy_s(openerlib_config_info.domain_name, sizeof(openerlib_config_info.domain_name), domain_name);
	strcpy_s(openerlib_config_info.ip_address, sizeof(openerlib_config_info.ip_address), ip_address);
	strcpy_s(openerlib_config_info.subnet_address, sizeof(openerlib_config_info.subnet_address), subnet_mask);
	strcpy_s(openerlib_config_info.gateway_address, sizeof(openerlib_config_info.gateway_address), gateway_address);

	for (int i = 0; i < 6; i++) {
		strcpy_s(openerlib_config_info.mac_address[i], sizeof(openerlib_config_info.mac_address[i]), acBuffer_mac_address[i]);
		//printf("strcat=%s\r\n", network_info.mac_address[i]);
	}

	//Now actually peform the configuration with the stored values...			
	ConfigureNetworkInterface(openerlib_config_info.ip_address, openerlib_config_info.subnet_address, openerlib_config_info.gateway_address);
	ConfigureDomainName(openerlib_config_info.domain_name);
	ConfigureHostName(openerlib_config_info.host_name);
	
	snprintf(openerlib_config_info.mac_address_formatted, 32, "%s-%s-%s-%s-%s-%s", acBuffer_mac_address[0], 
				acBuffer_mac_address[1], 
				acBuffer_mac_address[2],
				acBuffer_mac_address[3],
				acBuffer_mac_address[4],
				acBuffer_mac_address[5]);

	const EipUint8 mac_address_array[6] = { (EipUint8)strtoul(openerlib_config_info.mac_address[0], NULL, 16),
		(EipUint8)strtoul(openerlib_config_info.mac_address[1], NULL, 16),
		(EipUint8)strtoul(openerlib_config_info.mac_address[2], NULL, 16),
		(EipUint8)strtoul(openerlib_config_info.mac_address[3], NULL, 16),
		(EipUint8)strtoul(openerlib_config_info.mac_address[4], NULL, 16),
		(EipUint8)strtoul(openerlib_config_info.mac_address[5], NULL, 16) };
	
	ConfigureMacAddress(mac_address_array);

	return 0; //TODO: Error checking in API? How about here? 
}

int Initialize(unsigned int device_id,
	const char *const hostname,
	const char *const domain_name,
	const char *const ip_address,
	const char *const subnet_mask,
	const char *const gateway_address,
	const char *const acBuffer_mac_address[]) {
	
	ConfigureNIC(hostname,
		domain_name,
		ip_address,
		subnet_mask,
		gateway_address,
		acBuffer_mac_address);

	SetDeviceSerialNumber(device_id);
	
	EipUint16 nUniqueCipConnectionID = rand(); //per boot unique connnection ID for the CIP stack.
	//Assuming rand() is sufficiently unique for this purpose.

	CipStackInit(nUniqueCipConnectionID);

	if (NetworkHandlerInitialize() != kEipStatusOk) {
		return -1;
	}

#ifdef OPENER_WITH_TRACES
	LOG_TRACE("trace log gateway address: %s\r\n", gateway_address);
#endif

	//Assume success if we haven't returned an error condition by now.
	return 0;
}

int Shutdown() {
	if (NetworkHandlerFinish() == kEipStatusOk) {
		ShutdownCipStack();
		return 0;
	} else {
		//TODO: Implement more logic/diagnostics if we can't shutdown right now.
		return -1;
	}
}

int RunNetworkHandler() {
	return NetworkHandlerProcessOnce();
}

OpenerLibConfigInfo GetConfigInfo() {
	return openerlib_config_info;
}

int ProcessString(const char* str) {
	if (strcmp(str, "work") == 0)
		return 5;

	if (strcmp(str, "yes") == 0)
		return 10;

	if (strcmp(str, "no") == 0)
		return 15;

	return 1;
}

int PerformWriteToInputAssembly(unsigned int byte_position, unsigned char value_to_write) {
	
	if (byte_position > 32 || byte_position < 0) {
		return -1;
	}

	g_assembly_data064[byte_position] = value_to_write;
	return 0;
}

unsigned char PerformReadFromOutputAssembly(unsigned int byte_position) {
	
	if (byte_position > 32 || byte_position < 0) {
		return -1;
	}

	return g_assembly_data096[byte_position];
}

int OverwriteInputAssembly(unsigned char new_input_assembly[], unsigned int buffer_size) {
	return memcpy_s(g_assembly_data064, sizeof(g_assembly_data064) * sizeof(EipInt8), 
					new_input_assembly, buffer_size);
}

int CopyOutputAssemblyIntoBuffer(unsigned char output_buffer[], unsigned int buffer_size) {
	return memcpy_s(output_buffer, buffer_size, 
					g_assembly_data096, sizeof(g_assembly_data096) * sizeof(EipInt8));
}

int CopyInputAssemblyIntoBuffer(unsigned char input_buffer[], unsigned int buffer_size) {
	return memcpy_s(input_buffer, buffer_size,
		g_assembly_data064, sizeof(g_assembly_data064) * sizeof(EipInt8));
}

/**
* 
*
*/
int LoadSettingsString(char* in_str, unsigned int size) {
	const char* test_str = "Here is the settings info you requested!";
	
	unsigned int str_len = strlen(test_str); 
	strncpy_s(in_str, str_len + 1, test_str, size); //+1 for the null character which will be added on by the compiler/strncpy_s.

	return str_len; //No +1 here. Delphi expects the actual length of the string returned (not including null-character);
}

//******Callbacks needed by CIP, I defined******************/



//******Callbacks needed by the CIP implementation are defined below: See opener_api.h ********************/
EipStatus ApplicationInitialization(void) {

	for (int i = 0; i < sizeof(g_assembly_data064); i++) {
		g_assembly_data064[i] = 0xFF;
		g_assembly_data096[i] = 0xFF;
	}
	/* create 3 assembly object instances*/
	/*INPUT*/
	CreateAssemblyObject(DEMO_APP_INPUT_ASSEMBLY_NUM, &g_assembly_data064[0],
		sizeof(g_assembly_data064));

	/*OUTPUT*/
	CreateAssemblyObject(DEMO_APP_OUTPUT_ASSEMBLY_NUM, &g_assembly_data096[0],
		sizeof(g_assembly_data096));

	/*CONFIG*/
	CreateAssemblyObject(DEMO_APP_CONFIG_ASSEMBLY_NUM, &g_assembly_data097[0],
		sizeof(g_assembly_data097));

	/*Heart-beat output assembly for Input only connections */
	CreateAssemblyObject(DEMO_APP_HEARTBEAT_INPUT_ONLY_ASSEMBLY_NUM, 0, 0);

	/*Heart-beat output assembly for Listen only connections */
	CreateAssemblyObject(DEMO_APP_HEARTBEAT_LISTEN_ONLY_ASSEMBLY_NUM, 0, 0);

	/* assembly for explicit messaging */
	CreateAssemblyObject(DEMO_APP_EXPLICT_ASSEMBLY_NUM, &g_assembly_data09A[0],
		sizeof(g_assembly_data09A));

	ConfigureExclusiveOwnerConnectionPoint(0, DEMO_APP_OUTPUT_ASSEMBLY_NUM,
		DEMO_APP_INPUT_ASSEMBLY_NUM,
		DEMO_APP_CONFIG_ASSEMBLY_NUM);
	ConfigureInputOnlyConnectionPoint(0,
		DEMO_APP_HEARTBEAT_INPUT_ONLY_ASSEMBLY_NUM,
		DEMO_APP_INPUT_ASSEMBLY_NUM,
		DEMO_APP_CONFIG_ASSEMBLY_NUM);
	ConfigureListenOnlyConnectionPoint(0,
		DEMO_APP_HEARTBEAT_LISTEN_ONLY_ASSEMBLY_NUM,
		DEMO_APP_INPUT_ASSEMBLY_NUM,
		DEMO_APP_CONFIG_ASSEMBLY_NUM);

	return kEipStatusOk;
}

void HandleApplication(void) {
	/* check if application needs to trigger an connection */
}

void CheckIoConnectionEvent(unsigned int pa_unOutputAssembly,
	unsigned int pa_unInputAssembly,
	IoConnectionEvent pa_eIOConnectionEvent) {
	/* maintain a correct output state according to the connection state*/

	(void)pa_unOutputAssembly; /* suppress compiler warning */
	(void)pa_unInputAssembly; /* suppress compiler warning */
	(void)pa_eIOConnectionEvent; /* suppress compiler warning */
}

EipStatus AfterAssemblyDataReceived(CipInstance *pa_pstInstance) {
	EipStatus nRetVal = kEipStatusOk;

	/*handle the data received e.g., update outputs of the device */
	switch (pa_pstInstance->instance_number) {
	case DEMO_APP_OUTPUT_ASSEMBLY_NUM:
		/* Data for the output assembly has been received.
		* Mirror it to the inputs */
		memcpy(&g_assembly_data064[0], &g_assembly_data096[0],
			sizeof(g_assembly_data064));
		break;
	case DEMO_APP_EXPLICT_ASSEMBLY_NUM:
		/* do something interesting with the new data from
		* the explicit set-data-attribute message */
		break;
	case DEMO_APP_CONFIG_ASSEMBLY_NUM:
		/* Add here code to handle configuration data and check if it is ok
		* The demo application does not handle config data.
		* However in order to pass the test we accept any data given.
		* EIP_ERROR
		*/
		nRetVal = kEipStatusOk;
		break;
	}
	return nRetVal;
}

EipBool8 BeforeAssemblyDataSend(CipInstance *pa_pstInstance) {
	/*update data to be sent e.g., read inputs of the device */
	/*In this sample app we mirror the data from out to inputs on data receive
	* therefore we need nothing to do here. Just return true to inform that
	* the data is new.
	*/

	if (pa_pstInstance->instance_number == DEMO_APP_EXPLICT_ASSEMBLY_NUM) {
		/* do something interesting with the existing data
		* for the explicit get-data-attribute message */
	}
	return true;
}


EipStatus ResetDevice(void) {
	/* add reset code here*/
	return kEipStatusOk;
}

EipStatus ResetDeviceToInitialConfiguration(void) {
	/*rest the parameters */

	/*than perform device reset*/
	ResetDevice();
	return kEipStatusOk;
}

void *
CipCalloc(unsigned pa_nNumberOfElements, unsigned pa_nSizeOfElement) {
	return calloc(pa_nNumberOfElements, pa_nSizeOfElement);
}

void CipFree(void *pa_poData) {
	free(pa_poData);
}

void RunIdleChanged(EipUint32 pa_nRunIdleValue) {
	(void)pa_nRunIdleValue;
}