#ifndef __UDT_H
#define __UDT_H

typedef struct {
	char host_name[32];
	char domain_name[32];
	char ip_address[32];
	char subnet_address[32];
	char gateway_address[32];
	char mac_address[6][3];
	char mac_address_formatted[32];
} OpenerLibConfigInfo;

typedef struct {
	int initialized;
} OpenerLibStateInfo;

#endif