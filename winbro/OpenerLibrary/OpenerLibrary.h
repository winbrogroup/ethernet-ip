// The following ifdef block is the standard way of creating macros which make exporting 
// from a DLL simpler. All files within this DLL are compiled with the OPENERLIBRARY_EXPORTS
// symbol defined on the command line. This symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see 
// OPENERLIBRARY_API functions as being imported from a DLL, whereas this DLL sees symbols
// defined with this macro as being exported.
//#ifdef OPENERLIBRARY_EXPORTS
//#define OPENERLIBRARY_API extern "C" __declspec(dllexport)
//#else
//#define OPENERLIBRARY_API extern "C" __declspec(dllimport)
//#endif
#include "udt.h"

#ifndef __OPENERLIBRARY_H
#define __OPENERLIBRARY_H

//Use the below block for C99 compiler (required by other artifacts in OpENer's implementation)
#ifdef OPENERLIBRARY_EXPORTS
#define OPENERLIBRARY_API __declspec(dllexport)
#else
#define OPENERLIBRARY_API __declspec(dllimport)
#endif

//Local helper functions
int ConfigureNIC(const char *const hostname,
	const char *const domain_name,
	const char *const ip_address,
	const char *const subnet_mask,
	const char *const gateway_address,
	const char *const mac_address[]);

void LoadCommandLineParameters(const char *const argv[],
	const char **const hostname,
	const char **const domain_name,
	const char **const ip_address,
	const char **const subnet_mask,
	const char **const gateway_address,
	const char *ac_mac_address_array[]);

#ifdef __cplusplus
extern "C" {
#endif
//Exported functions provided by the library
	OPENERLIBRARY_API int Initialize(unsigned int device_id,
		const char *const hostname,
		const char *const domain_name,
		const char *const ip_address,
		const char *const subnet_mask,
		const char *const gateway_address,
		const char *const mac_address[]);

	OPENERLIBRARY_API int Shutdown();

	OPENERLIBRARY_API int RunNetworkHandler();

	OPENERLIBRARY_API OpenerLibConfigInfo GetConfigInfo();

	OPENERLIBRARY_API int ProcessString(const char* str); //Tester/example function

	OPENERLIBRARY_API int LoadSettingsString(char* in_str, unsigned int size); //Tester/example function

	OPENERLIBRARY_API int PerformWriteToInputAssembly(unsigned int byte_position, unsigned char value_to_write);

	OPENERLIBRARY_API unsigned char PerformReadFromOutputAssembly(unsigned int byte_position);

	OPENERLIBRARY_API int OverwriteInputAssembly(unsigned char new_input_assembly[], unsigned int buffer_size);

	OPENERLIBRARY_API int CopyOutputAssemblyIntoBuffer(unsigned char output_buffer[], unsigned int buffer_size);

	OPENERLIBRARY_API int CopyInputAssemblyIntoBuffer(unsigned char input_buffer[], unsigned int buffer_size);

#ifdef __cplusplus
}
#endif


#endif //Header inclusion guards
