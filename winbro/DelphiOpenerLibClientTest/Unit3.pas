unit Unit3;

interface


uses
  Windows, Messages, SysUtils, SyncObjs, ExtCtrls, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, MyProtocol;

type
    TForm3 = class(TForm)
    Button1: TButton;
    Label1: TLabel;
    Button2: TButton;
    Button3: TButton;
    Button4: TButton;
    procedure Button3Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);

  private
     { Private declarations }
     KeepRunning: Boolean;
  public
    { Public declarations }
    EthernetIPProtocol: TEthernetIPProtocol;
  end;

var
  Form3: TForm3;

implementation

{$R *.dfm}


procedure TForm3.Button1Click(Sender: TObject);
var
  pStr: PAnsiChar;
  sStr: string;

  result: Integer;
  arrayMac: array[0..5] of PChar;
  init_result: Integer;
  net_handler_result: Integer;
  EthernetIPProtocol: TEthernetIPProtocol;

  config: TOpenerLibConfigInfo;

begin
  EthernetIPProtocol:= TEthernetIPProtocol.Create('OpenerLibrary.dll');

  //Label1.Caption := IntToStr(EthernetIPProtocol.Simple());
  Label1.Caption := 'Default value';

  sStr:='yes';
  result := EthernetIPProtocol.ProcessString(PChar(sStr)); //Should return an integer with value of 10.
  //ShowMessage(IntToStr(result));

  sStr:='no';
  result := EthernetIPProtocol.ProcessString(PChar(PChar(sStr))); //Should return an integer with value of 15.
  //ShowMessage(IntToStr(result));

  sStr:='work';
  result := EthernetIPProtocol.ProcessString(PChar(sStr)); //Should return an integer with value of 5.
  //ShowMessage(IntToStr(result));
  arrayMac[0]:=PChar('00');
  arrayMac[1]:=PChar('0C');
  arrayMac[2]:=PChar('29');
  arrayMac[3]:=PChar('8D');
  arrayMac[4]:=PChar('DC');
  arrayMac[5]:=PChar('5A');

  init_result := EthernetIPProtocol.Initialize(112233,
      'WGT133N-D2005',
      'localdomain',
      '192.168.217.128',
      '255.255.255.0',
      '192.168.217.2',
      @arrayMac[0]);

  //ShowMessage('init result=' + IntToStr(init_result));
  Assert(init_result=0);
  config := EthernetIPProtocol.GetConfigInfo();


//  ShowMessage(config.host_name);
//  ShowMessage(config.domain_name);
//  ShowMessage(config.ip_address);
//  ShowMessage(config.gateway_address);
//  ShowMessage(config.mac_address_formatted);
//  ShowMessage(config.mac_address[0]);
//  ShowMessage(config.mac_address[1]);
//  ShowMessage(config.mac_address[2]);
//  ShowMessage(config.mac_address[3]);
//  ShowMessage(config.mac_address[4]);
//  ShowMessage(config.mac_address[5]);

  KeepRunning := true;
 // ShowMessage('Init result=' + IntToStr(init_result));

  while KeepRunning=true do
  begin
    net_handler_result := EthernetIPProtocol.RunNetworkHandler();
    Label1.Caption := IntToStr(net_handler_result);
    Application.ProcessMessages();
    Sleep(50);
  end;

  EthernetIPProtocol.Shutdown();
  Label1.Caption := IntToStr(result);

end;

function mytest(name: PChar; age: Integer): Integer;
var
  myStr : string;
begin

  ShowMessage('Helllo');
  Result := 0;
end;

procedure TForm3.Button2Click(Sender: TObject);
var
  buffer: array [0..255] of Char;
  num_bytes_read:Integer;
  result: string;

begin
 KeepRunning := false;
 result:='test';


 EthernetIPProtocol:= TEthernetIPProtocol.Create('OpenerLibrary.dll');
 Assert(EthernetIPProtocol <> nil,'its null');

 num_bytes_read := EthernetIPProtocol.LoadSettingsString(buffer, Length(buffer));

 SetString(result, buffer, num_bytes_read);

 ShowMessage(result);

 Self.Close();

end;

procedure TForm3.Button3Click(Sender: TObject);
var
  mystr: string;
  dyn_array: array of WideChar;
  i: Integer;


begin

   mystr := 'C:\work\ethernet-ip\winbro\acnc32\profiles\hsd6GT.V3\profile\lib';
  //SetLength(wide_chars, Length(DllProfilePath + '\lib'));
  //StringToWideChar(mystr + '\lib', @wide_chars, Length(wide_chars));
  //SetDllDirectory(wide_chars);

  SetLength(dyn_array, Length(mystr));
  Move(mystr, dyn_array[0], Length(mystr));

  ShowMessage();

  //SetDllDirectory(wide_chars);
end;

end.
