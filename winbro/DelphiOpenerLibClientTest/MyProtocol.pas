unit MyProtocol;
//Wrapper to "C" OpenerLibrary.dll

interface

uses Windows, Classes, SysUtils, SyncObjs, ExtCtrls;
type

  TOpenerLibConfigInfo = record
    host_name: array [0..31] of Char;
    domain_name: array [0..31] of Char;
    ip_address: array [0..31] of Char;
    subnet_address: array [0..31] of Char;
    gateway_address: array [0..31] of Char;
    mac_address: array [0..5, 0..2] of Char;
    mac_address_formatted: array [0..31] of Char;
end;

type
  TInitialize = function(ID: Integer; Host, Domain, IP, Subnet, Gateway, MAC:PChar): Integer; cdecl;
  TShutdown = function: Integer; cdecl;
  TRunNetworkHandler = function: Integer; cdecl;
  TProcessString = function(str_to_echo: PChar): Integer; cdecl; //Test function: Remove for production
  TLoadSettingsString = function(ptr_buffer: PChar; buffer_size:Integer): Integer; cdecl;
  TGetConfigInfo = function(): TOpenerLibConfigInfo; cdecl;

  TEthernetIPProtocol = class
  private
    FInitialize: TInitialize;
    FShutdown: TShutdown;
    FRunNetworkHandler: TRunnetworkHandler;
    FProcessString: TProcessString;
    FLoadSettingsString: TLoadSettingsString;
    FGetConfigInfo: TGetConfigInfo;

  public
    constructor Create(Libname: string);
    function Initialize(ID: Integer; Host, Domain, IP, Subnet, Gateway, MAC:PChar): Integer;
    function Shutdown: Integer;
    function RunNetworkHandler: Integer;
    function ProcessString(str_to_echo: PChar): Integer;
    function LoadSettingsString(ptr_buffer: PChar; buffer_size:Integer): Integer;
    function GetConfigInfo(): TOpenerLibConfigInfo;
  end;

implementation

{ TEthernetIPProtocol }

{ TEthernetIPProtocol }

constructor TEthernetIPProtocol.Create(Libname: string);
var HMod : HMODULE;

  function GetProcAddressX(hModule: HMODULE; lpProcName: LPCSTR): FARPROC;
  begin
    Result := GetProcAddress(hModule, lpProcName);
    if not Assigned(Result) then
      raise Exception.CreateFmt('Library interface missing [%s]', [lpProcName]);
  end;

begin
  HMod := LoadLibrary(PChar(LibName));
  if HMod = 0 then
    raise Exception.CreateFmt('Library [%s] not found', [LibName]);

  FInitialize:= GetProcAddressX(HMod, 'Initialize');
  FShutdown:= GetProcAddressX(HMod, 'Shutdown');
  FProcessString:= GetProcAddressX(HMod, 'ProcessString');

  FRunNetworkHandler:= GetProcAddressX(HMod, 'RunNetworkHandler');
  FLoadSettingsString := GetProcAddressX(HMod, 'LoadSettingsString');

  FGetConfigInfo:= GetProcAddressX(HMod, 'GetConfigInfo');

end;

function TEthernetIPProtocol.Initialize(ID: Integer; Host, Domain, IP, Subnet, Gateway, MAC:PChar): Integer;
begin

  Result:= FInitialize(Integer(ID), PChar(Host), PChar(Domain), PChar(IP), PChar(Subnet), PChar(Gateway), PChar(MAC));
end;

function TEthernetIPProtocol.RunNetworkHandler: Integer;
begin
  Result:= FRunNetworkHandler;
end;

function TEthernetIPProtocol.Shutdown: Integer;
begin
  Result:= FShutdown;
end;

function TEthernetIPProtocol.ProcessString(str_to_echo: PChar): Integer;
begin
  Result:= FProcessString(str_to_echo);
end;

function TEthernetIPProtocol.LoadSettingsString(ptr_buffer: PChar; buffer_size:Integer): Integer;
begin
  Result:= FLoadSettingsString(PChar(ptr_buffer), buffer_size);
end;

function TEthernetIPProtocol.GetConfigInfo(): TOpenerLibConfigInfo;
begin
  Result := FGetConfigInfo();
end;

end.
