// OpenerLibraryClient.c : 
// Client program demonstrating how to use "OpenerLibrary.DLL".

#include <stdio.h>
#include <stdlib.h>
#include <memory.h>
#include "OpenerLibrary.h"

void ShowOpenerLibInitErrorMessage(const OpenerLibConfigInfo *const config_info);
void ShowOpenerLibConfig(const OpenerLibConfigInfo *const config_info);

int main(int argc, char *argv[])
{

	int end_stack = 0;
	
	unsigned int device_id = 112233;
	const char* hostname = "WGI035N";
	const char* domain_name = "usa-winbrogroup.intl";
	const char* ip_address = "192.168.181.246"; //"192.168.0.6";
	const char* subnet_mask = "255.255.255.0";
	const char* gateway_address = "192.168.181.32"; //"192.168.0.1";
	const char* ac_mac_address_array[6] = { "E4","A7","A0","D1","E6","9C" };
	
	if (argc > 1) {
		if (argc != 12) {
			printf("Wrong number of command line parameters!\n");
			printf("The correct command line parameters are:\n");
			printf(
				"./OpenerLibraryClient ipaddress subnetmask gateway domainname hostaddress macaddress\n");
			printf(
				"    e.g. ./OpenerLibraryClient 192.168.0.2 255.255.255.0 192.168.0.1 test.com testdevice 00 15 C5 BF D0 87\n");
			
			exit(-1); //Exit the program...
		}
		printf("Loading command line arguments...\r\n");
		LoadCommandLineParameters(argv,
			&hostname,
			&domain_name,
			&ip_address,
			&subnet_mask,
			&gateway_address,
			ac_mac_address_array);
	}

	const int opener_lib_init_result = Initialize(device_id,
		hostname,
		domain_name,
		ip_address,
		subnet_mask,
		gateway_address,
		ac_mac_address_array);

	printf("Test: ProcessString('yes')=%d\r\n", ProcessString("yes"));
	printf("Test: ProcessString('no')=%d\r\n", ProcessString("no"));
	printf("Test: ProcessString('fdsafdsa')=%d\r\n", ProcessString("fdsafdsa"));

	char buf[256];
	LoadSettingsString(buf, 256);
	printf("Test: SendString()=[START]%s[END]\r\n", &buf);
	OpenerLibConfigInfo config_info = GetConfigInfo();

	if (opener_lib_init_result == 0) {
		printf("Ethernet/IP Device Active:\r\n");
		ShowOpenerLibConfig(&config_info);
		printf("Running event loop... (Press Ctrl-C to exit)\r\n");
	} else {
		ShowOpenerLibInitErrorMessage(&config_info);
		exit(-1);
	}

	/* The event loop. Put other processing you need done continually in here */
	while (end_stack == 0) { //TODO: Integrate boolean values for this version of the C compiler.
		if (RunNetworkHandler() != 0) {
			break;
		}
	}
	
	Shutdown();

	printf("Done\r\n");
	return 0;
}

void ShowOpenerLibConfig(const OpenerLibConfigInfo *const config_info) {
	printf("\tHostname   : %s\r\n", config_info->host_name);
	printf("\tDomain name: %s \r\n", config_info->domain_name);
	printf("\tIP address : %s\r\n", config_info->ip_address);
	printf("\tSubnet Mask: %s\r\n", config_info->subnet_address);
	printf("\tGateway    : %s\r\n", config_info->gateway_address);
	printf("\tMac Address: %s\r\n", config_info->mac_address_formatted);
}

void ShowOpenerLibInitErrorMessage(const OpenerLibConfigInfo *const config_info) {

	printf("Error initializing network. Exiting...\r\n");
	printf("\r\n");
	printf("Please check the host's network card is configured with the following values:\r\n");
	printf("(type 'ipconfig /all' in a windows command prompt to see configuration values for your installed network interfaces)\r\n");
	ShowOpenerLibConfig(config_info);
	printf("\r\n");
	printf("\tAdditionally, you can pass parameters on the command line in the following format:\r\n");
	printf("\t\tOpenerLibraryClient <ipaddress> <subnetmask> <gateway> <domainname> <hostname> <macaddress>\r\n");
	printf("\t\te.g. . / OpenerLibraryClient 192.168.0.2 255.255.255.0 192.168.0.1 test.com testdevice 00 15 C5 BF D0 87\r\n");
}

void LoadCommandLineParameters(const char *const argv[],
	const char **const hostname,
	const char **const domain_name,
	const char **const ip_address,
	const char **const subnet_mask,
	const char **const gateway_address,
	const char *ac_mac_address_array[]) {

	*ip_address = argv[1];
	*subnet_mask = argv[2];
	*gateway_address = argv[3];
	*domain_name = argv[4];
	*hostname = argv[5];

	ac_mac_address_array[0] = argv[6];
	ac_mac_address_array[1] = argv[7];
	ac_mac_address_array[2] = argv[8];
	ac_mac_address_array[3] = argv[9];
	ac_mac_address_array[4] = argv[10];
	ac_mac_address_array[5] = argv[11];
}


