// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

#include <stdio.h>
#include <tchar.h>

//Need to define the below block to avoid compilation errors when using EtIP api headers.
#ifndef ET_IP_CALLBACK
#define ET_IP_CALLBACK __stdcall
#endif

#include "../CncEtIPAdapter/CncEtIPAdapter.h"
#include "../CncEtIPAdapter/FString.h"

// TODO: reference additional headers your program requires here
