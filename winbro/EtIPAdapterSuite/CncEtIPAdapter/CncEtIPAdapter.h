#ifndef __CNCETIPADAPTER_H
#define __CNCETIPADAPTER_H

#include "../../../EthernetIPAdapterDevKit/Src/Platform/Windows/windatatypes.h"
#include "../../../EthernetIPAdapterDevKit/Src/EtIpApi.h" //Used to define some EtIP... structures (ie. EtIPSystemStats).
//#include "../../../EthernetIPAdapterDevKit/Src/Adapter/EtIPAdapter.h"
#include "FString.h"
#include "CncEtIPAdapterHelper.h"
#include "udt.h"

//#if __has_include("version.h")
#include "version.h"
//#endif

#ifdef CNCETIPADAPTER_EXPORTS
#define CNCETIPADAPTER_API __declspec(dllexport)
#else
#define CNCETIPADAPTER_API __declspec(dllimport)
#endif

inline void ProcessNetworkLEDChange();
inline void LogError(const FString& error_msg, const FString& reason_msg = "");
inline const char* GetClaimedIPAddress();

#ifdef __cplusplus
extern "C" {
#endif

	CNCETIPADAPTER_API int GetEtLibSystemStats(EtIPSystemStats &p);
	CNCETIPADAPTER_API int SetOutputAssembly(UINT8 new_output[], UINT32 size);

	CNCETIPADAPTER_API int GetInputAssembly(UINT8 input_assembly[], UINT32 size);

	CNCETIPADAPTER_API int Initialize(const char* ip_address, const char* product_name);
	CNCETIPADAPTER_API const CncEtLibStatus *const GetCncEtLibStatus(); 
	CNCETIPADAPTER_API const CncEtLibVersionInfo *const GetCncEtLibVersionInfo();
	CNCETIPADAPTER_API int Shutdown();
#ifdef __cplusplus
	}
#endif // __cplusplus


#endif //Header inclusion guards
