// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
// Windows Header Files:
//#include <windows.h>

#include <stdio.h>
#include <string.h>

#include <WinSock2.h>
#include <ws2tcpip.h>
#include <assert.h>

#ifndef ET_IP_CALLBACK
#define ET_IP_CALLBACK __stdcall
#endif

#include "../../../EthernetIPAdapterDevKit/Src/Platform/Windows/windatatypes.h"
#include "../../../EthernetIPAdapterDevKit/Src/EtIpApi.h"
#include "../../../EthernetIPAdapterDevKit/Src/Adapter/EtIPAdapter.h"


// TODO: reference additional headers your program requires here
#include "CncEtIPAdapter.h"


