#ifndef __FSTRING_H
#define __FSTRING_H

#include <stdarg.h>
#include <stdio.h>
#include <assert.h>
#include <memory.h>

class FString {
public:
	FString();

	~FString();

	FString(const char* format, ...); 
	
	const char* format(const char* format, ...); 
	
	const FString& operator=(const FString& rhs);
	
	const FString& operator=(const char *str);
		
	FString& operator+(const FString& rhs);


	//FString operator+(FString other)
	//{
	//	return *this + other;
	//}

	const FString& operator+=(const FString& rhs);

	//Note: Checks pointer locations for equality, not string contents.
	const bool operator==(const FString& rhs) const;

	bool operator!=(const FString& rhs) const;
	
	
	const char* const GetBuffer();
	const unsigned int GetBufferSize() const;
	const unsigned int GetLength() const; // Length of the string minus the null-terminating character.

	void Clear();

		
/*****Private functions*****/
private:
	void _deallocate(); 
	void _allocateByVariadicParams(const char* format, va_list args);
	void _initialize();
	

/*****Private member variables*****/
private:
	char *_mem;
	unsigned int _buff_size;
	
};

#endif