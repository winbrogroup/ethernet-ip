@echo on
echo Creating version.h git revision placeholder file...

git log -n1 --format=format:"#define GIT_COMMIT \"%%h\"%%n#define GIT_COMMIT_DATE \"%%aD\"" HEAD > version.h