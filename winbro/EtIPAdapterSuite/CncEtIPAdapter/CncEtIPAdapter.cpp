// CncEtIPAdapter.cpp : Defines the exported functions for the DLL application.
//

// CncEtIPAdapter.cpp : Defines the exported functions for the DLL application.
//

#include "stdafx.h"

//TODO: These globals should become member variables for a reporting/status type of class.
static FString g_error_msg("<None reported>");
static bool g_is_healthy(false);
static FString g_claimed_ip_address("<undefined>");
static FString g_network_status_description("<undefined>");
static FString g_product_name("<undefined>");
static unsigned int g_claimed_ip_address_num(0);

static CncEtLibStatus g_struct_status;
static CncEtLibVersionInfo g_struct_version_info;


#define ASSEMBLY_INPUT_INSTANCE			101 //Input from the originator to this adapter.
#define ASSEMBLY_OUTPUT_INSTANCE		102 //Output from this adapter to the originator.
#define ASSEMBLY_INSTANCE_SIZE			256 //256 bytes. Each byte is 8 bits wide in this implementation.

#define ASSEMBLY_CONFIG_INSTANCE		1 //Configuration assembly is not needed or used. This is a general vendor/CIP pattern to represent this...
#define ASSEMBLY_CONFIG_INSTANCE_SIZE	0 //Some vendors require a config assembly instance even if they're not used.
//config 1 instance id zero size

void ET_IP_CALLBACK fnCallback(INT32 nEvent, INT32 nParam1, INT32 nParam2)
{
	//UINT8 aData[ASSEMBLY_INSTANCE_SIZE];
	if ( (nEvent < 200 || nEvent > 210) && 
		 (nEvent != NM_NETWORK_LED_CHANGE) ) //Interested in only a subset of the events from the callback function (for now anyway).
		return;
	
	switch (nEvent)
	{
		case NM_OUT_OF_MEMORY:
			LogError("EIP stack error: 'Out of Memory' error was reported.");
			break;
		case NM_UNABLE_INTIALIZE_WINSOCK:
			LogError("EIP Stack error: Unable to initialize Windows Sockets library.");
			break;
		case NM_UNABLE_START_THREAD:
			LogError("EIP stack error: Unable to start thread.");
			break;
		case NM_ERROR_USING_WINSOCK:
			LogError("EIP stack error: Received an error when using (any) socket library functions.");
			break;
		case NM_ERROR_SETTING_SOCKET_TO_NONBLOCKING:
			LogError("EIP stack error: Error encountered when setting the socket to non-blocking mode.");
			break;
		case NM_ERROR_CREATING_SEMAPHORE:
			LogError("EIP stack error: Error encountered when failing to create semaphore.");
			break;
		case NM_SESSION_COUNT_LIMIT_REACHED:
			LogError("EIP stack error: Number of network peers exceeded MAX_SESSIONS (default is 128) constant.");
			break;
		case NM_CONNECTION_COUNT_LIMIT_REACHED:
			LogError("EIP stack error: Number of connections exceeded MAX_CONNECTIONS limit.");
			break;
		case NM_PENDING_REQUESTS_LIMIT_REACHED:
			LogError("EIP stack error: Number of outstanding object requests exceeded MAX_REQUESTS (default is 100) constant.");
			break;
		case NM_PENDING_REQUEST_GROUPS_LIMIT_REACHED:
			LogError("EIP stack error: Number of outstanding multiple service requests exceeded MAX_REQUEST_GROUPS limit.");
			break;
		case NM_ERROR_UNABLE_START_MODBUS:
			LogError("EIP stack error: Cannot initialize Modbus stack.");
			break;
		case NM_NETWORK_LED_CHANGE:
			//No need to error log network status changing.
			//g_error_msg.format("Network adapter: Network LED status has changed to %d", nParam2);
			ProcessNetworkLEDChange();
			break;
		default: //Default case should be used in the event our assumptions (ie. all the errors we want to capture) are incorrect (ie. we missed an error condition we wanted to trap).
			LogError(FString("Error: Unknown or un-handled EIP error. event_id=%d param1=%d param2=%d", nEvent, nParam1, nParam2));
			break;
		
			



	//case NM_ASSEMBLY_NEW_INSTANCE_DATA:
	//	//printf("New input data received for assembly instance %d\n", nParam1);
	//	switch (nParam1)
	//	{
	//	case ASSEMBLY_INPUT_INSTANCE:
	//		if (nParam2 != 0) /* If 0, data has not changed, but sequence count has. */
	//		{
	//			/* Get the current data */
	//			//EtIPGetAssemblyInstanceData(ASSEMBLY_INPUT_INSTANCE, aData, ASSEMBLY_INSTANCE_SIZE);
	//		}
	//		break;
	//	case ASSEMBLY_OUTPUT_INSTANCE:
	//		if (nParam2 !=0)
	//		{
	//			//printf("New output data for assembly instance %d\n", nParam1);
	//		}
	//		break;
	//	case ASSEMBLY_INPUT_MEMBER_INSTANCE:
	//		//ignore the notification becuase the data change will be handled
	//		//by the NM_ASSEMBLY_NEW_MEMBER_DATA notification
	//		break;
	//	}
	//	break;
	//case NM_ASSEMBLY_NEW_MEMBER_DATA:
	//	//printf("New input data received for assembly instance %d, member %d\n", nParam1, nParam2);
	//	switch (nParam1)
	//	{
	//	case ASSEMBLY_INPUT_MEMBER_INSTANCE:
	//		/* Get the current member data */
	//		//EtIPGetAssemblyMemberData(ASSEMBLY_INPUT_MEMBER_INSTANCE, nParam2, aData, 2);
	//		/* Mirror it to the output */
	//		//EtIPSetAssemblyMemberData(ASSEMBLY_OUTPUT_MEMBER_INSTANCE, nParam2, aData, 2);
	//		break;
	//	}
	//	break;
	//case NM_CONNECTION_ESTABLISHED:
	//	//printf("Connection with instance %d successfully opened\n", nParam1);
	//	break;
	//case NM_CONNECTION_TIMED_OUT:
	//	//printf("Connection with instance %d timed out\n", nParam1);
	//	break;
	//case NM_CONNECTION_CLOSED:
	//	//printf("Connection with instance %d closed\n", nParam1);
	//	break;
	//case NM_INCOMING_CONNECTION_REDUNDANT_MASTER_CHANGED:
	//	//printf("Connection instance %d is now the owner of connection %d\n", nParam1, nParam2);
	//	break;
	//case NM_INCOMING_CONNECTION_ROO_VALUE_CHANGED:
	//	//printf("Connection instance %d's ROO value changed to %d\n", nParam1, nParam2);
	//	break;
	//case NM_INCOMING_CONNECTION_NO_REDUNDANT_MASTER:
	//	//printf("Connection instance %d has no redundant master\n", nParam1);
	//	break;

		//This is a demo application which ignores all other notification
	}
}

CNCETIPADAPTER_API int SetOutputAssembly(UINT8 new_output[], UINT32 size)
{
	assert(size <= ASSEMBLY_INSTANCE_SIZE && "Output assembly size is too large."); //programmer error, halt execution.
	
	return EtIPSetAssemblyInstanceData(ASSEMBLY_OUTPUT_INSTANCE, new_output, size);
}

CNCETIPADAPTER_API int GetInputAssembly(UINT8 input_assembly[], UINT32 size)
{
	assert(size <= ASSEMBLY_INSTANCE_SIZE && "Input assembly size is too large."); //programmer error, halt execution.

	return EtIPGetAssemblyInstanceData(ASSEMBLY_INPUT_INSTANCE, input_assembly, size);
}

CNCETIPADAPTER_API int Shutdown()
{
	EtIPAdapterStop();

	return 0; //Return success all the time for now. This patttern added for future use and consistency...
}

CNCETIPADAPTER_API int GetEtLibSystemStats(EtIPSystemStats &p)
{
	return EtIPGetSystemStats(&p, 0);
}

CNCETIPADAPTER_API const CncEtLibStatus *const GetCncEtLibStatus() 
{
	g_struct_status.claimed_ip_address = g_claimed_ip_address.GetBuffer();
	g_struct_status.error_msg = g_error_msg.GetBuffer();
	g_struct_status.network_status_description = g_network_status_description.GetBuffer();

	g_struct_status.claimed_ip_address_num = &g_claimed_ip_address_num;

	g_struct_status.product_name = g_product_name.GetBuffer();

	return (&g_struct_status);
}

CNCETIPADAPTER_API const CncEtLibVersionInfo *const GetCncEtLibVersionInfo()
{	
	return &g_struct_version_info;
}

void SetLibVersionInfo()
{
	g_struct_version_info.timestamp = GIT_COMMIT_DATE;
	g_struct_version_info.version   = GIT_COMMIT;
}

//General trap for error reporting and processing
void LogError(const FString& error_msg, const FString& reason_msg)
{
	g_error_msg = error_msg; //Simple for now, should become more complex later.

	if (reason_msg.GetLength() != 0)
	{
		g_error_msg += "\r\nBest guess why this happened: ";
		g_error_msg += reason_msg;
	}
	
}

void ProcessNetworkLEDChange()
{
	EtIPNetworkStatus network_status = EtIPGetNetworkStatus(g_claimed_ip_address_num);
	switch (network_status)
	{
	case NetworkStatusFlashingGreen:
		g_network_status_description = "FlashingGreen: No connections.";
		break;
	case NetworkStatusFlashingGreenRed:
		g_network_status_description = "FlashingGreenRed: Selftest.";
		break;
	case NetworkStatusFlashingRed:
		g_network_status_description = "FlashingRed: One or more connections have timed out.";
		break;
	case NetworkStatusGreen:
		g_network_status_description = "Green: At least 1 connection.";
		break;
	case NetworkStatusOff:
		g_network_status_description = "Off: No power or no IP (claimed) address.";
		break;
	case NetworkStatusRed:
		g_network_status_description = "Red: Duplicate IP address detected.";
		break;
	default:
		g_network_status_description = "Error: Unknown/un-handled network state.";
		LogError(g_network_status_description);
		break;
	}
	
}

int SetProductName(const char* product_name, FString& out_error_msg)
{
	unsigned int product_name_length = strlen(product_name);

	if (product_name_length > 45)
	{
		out_error_msg = "Product name/identity string length is too large. CIP specifies the length to be no greater than 45 characters.";
		return -1;
	}
	
	assert(product_name_length <=45 && "Product name/identity string length is too large. CIP specifies the length to be no greater than 45 characters.");
		   
	
	EtIPIdentityInfo identity_info;
	EtIPGetIdentityInfo(1, &identity_info); //Caution: Hardcoding the instance id here...
	
	if (strcpy_s(identity_info.productName, sizeof(identity_info.productName), product_name) != 0)
	{
		out_error_msg = "Unknown error occurred while copying ethernet/ip identity info field";
		return -1;
	}
	
	EtIPSetIdentityInfo(1, &identity_info);
	return 0;
}

const char* IPv4NumToString(unsigned int ip_address_num_form)
{
	//char str_ip_address[INET_ADDRSTRLEN];
	struct in_addr internet_address;

	internet_address.S_un.S_addr = ip_address_num_form;
	//updated ipv6 calls, but not supported by winXP's ws2_32.dll
	//inet_ntop(AF_INET, &(internet_address.S_un.S_addr), str_ip_address, INET_ADDRSTRLEN);
	//return str_ip_address;

	return inet_ntoa(internet_address);	
}

unsigned int IPv4StringToNum(const char* ip_address)
{
	//updated ipv6 calls, but not supported by winXP's ws2_32.dll
	//struct in_addr internet_address;
	//unsigned int ip_address_as_number;

	//updated ipv6 calls, but not supported by winXP's ws2_32.dll
	//inet_pton(AF_INET, ip_address, &ip_address_as_number);
	//return ip_address_as_number;
	return inet_addr(ip_address);
}

//Requires Vista or above (or special support lib install on winxp)
//string Ip6NumToString(unsigned int ip_address_num_form)
//{
//	char str_ip_address[INET_ADDRSTRLEN];
//	struct in_addr internet_address;
//
//	internet_address.S_un.S_addr = ip_address_num_form;
//	inet_ntop(AF_INET, &(internet_address.S_un.S_addr), str_ip_address, INET_ADDRSTRLEN);
//	
//	return str_ip_address;
//}
//
//unsigned int Ip6StringToNum(const char* ip_address)
//{
//	struct in_addr internet_address;
//	unsigned int ip_address_as_number;
//		
//	inet_pton(AF_INET, ip_address, &ip_address_as_number);
//	
//	return ip_address_as_number;
//}

bool DoesIPExistOnHost(const char* ip_address)
{
	const unsigned int MAX_IP_ADDRESSES = 256;
	unsigned int all_ips_on_host[MAX_IP_ADDRESSES];
	
	int num_ip_addresses_on_host = EtIPGetHostIPAddress(all_ips_on_host, MAX_IP_ADDRESSES);
		
	for (int i = 0; i < num_ip_addresses_on_host; i++)
	{
		if (all_ips_on_host[i] == IPv4StringToNum(ip_address))
			return true;
	}

	return false;
}

const char* GetClaimedIPAddress()
{
	const unsigned int MAX_IP_ADDRESSES = 256;
	unsigned int all_claimed_ips_on_host[MAX_IP_ADDRESSES];

	unsigned int num_claimed_addresses_on_host = EtIPGetClaimedHostIPAddress(all_claimed_ips_on_host, MAX_IP_ADDRESSES);
	assert(num_claimed_addresses_on_host == 1 && "Error assigning IP address: Only one IP address can be claimed by this adapter.");

	const char* ipv4_string = IPv4NumToString(all_claimed_ips_on_host[0]);
	bool is_host_ip_claimed = DoesIPExistOnHost(ipv4_string);
	assert(is_host_ip_claimed && "Error assigning IP address: Doesn't exist on host.");
	assert(g_claimed_ip_address_num == all_claimed_ips_on_host[0] && "Error assigning IP address: assigned address doesn't match Ethernet/IP's claimed address.");

	return ipv4_string;	
}

void test()
{
	int connection_instances[MAX_CONNECTIONS];
	EtIPGetConnectionInstances(connection_instances);
}

CNCETIPADAPTER_API int Initialize(const char* ip_address, const char* product_name)
{
	EtIPRegisterEventCallBack(&fnCallback);
		
	EtIPAdapterStart();
	SetLibVersionInfo();
	
	FString error_reason;
	
	if (SetProductName(product_name, error_reason) != 0)
	{
		LogError(FString("Error setting identity product name: Copy operation failed for product name=\"%s\"", product_name), error_reason);
		return -1;
	}
	g_product_name = product_name;

	if (!DoesIPExistOnHost(ip_address)) //Can't find an ip address on the host that matches the client's parameter.
	{
		LogError(FString("Error during initialization: Can not use IP address '%s'. No such IP address is assigned to any network interface on the host.", ip_address));
		return -1;
	}
	
	g_claimed_ip_address_num = IPv4StringToNum(ip_address);
	if (!EtIPClaimHostIPAddress(g_claimed_ip_address_num, 0)) //Assign the ip address to the stack...
	{
		LogError(FString("Error during initialization: IP address '%s' exists on the host but we were unable to use it for an ethernet/ip transport.", ip_address));
		return -1;
	}
	g_claimed_ip_address = GetClaimedIPAddress(); //Read the "claimed" ip address from the stack... 
			
	/* Create 2 assembly instances */
	int assembly_add_retval = EtIPAddAssemblyInstance(ASSEMBLY_OUTPUT_INSTANCE, AssemblyStatic | AssemblyProducing, INVALID_OFFSET, ASSEMBLY_INSTANCE_SIZE, NULL);
	if (assembly_add_retval != 0)
	{
		LogError(FString("Error creating output assembly instance. The specific error code was %d.", assembly_add_retval));
		return -1;
	}

	assembly_add_retval = EtIPAddAssemblyInstance(ASSEMBLY_INPUT_INSTANCE, AssemblyStatic | AssemblyConsuming, INVALID_OFFSET, ASSEMBLY_INSTANCE_SIZE, NULL);
	if (assembly_add_retval != 0)
	{
		LogError(FString("Error creating input assembly instance. The specific error code was %d.", assembly_add_retval));
		return -1;
	}

	assembly_add_retval = EtIPAddAssemblyInstance(ASSEMBLY_CONFIG_INSTANCE, AssemblyStatic | AssemblyConfiguration, INVALID_OFFSET, ASSEMBLY_CONFIG_INSTANCE_SIZE, NULL);
	if (assembly_add_retval != 0)
	{
		LogError(FString("Error creating configuration assembly instance. The specific error code was %d.", assembly_add_retval));
		return -1;
	}
		
	return 0;
}