#include "stdafx.h"
#include <algorithm>
#include <iterator>

#include "CppUnitTest.h"
#include "OpenerLibrary.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace UnitTestOpenerLibrary
{		
	TEST_CLASS(UnitTest1)
	{
	private:
		unsigned int input_buffer_size = 32;
		unsigned int output_buffer_size = 32;
	
	public:
		UnitTest1() {
			//Setup in the constructor...
		}

		~UnitTest1() {
			//Takedown in the destructor...
		}
		
		/*TEST_METHOD(CreateCopyOfOutputAssemblyShouldPass)
		{
			const unsigned int output_size = 32;
			const unsigned int expected_value_in_output_assembly = 0;
			unsigned char mock_output[output_size];
			
			unsigned char random_byte_value = rand() % 255;
			int fxResult;
			
			void *result = memset(mock_output, random_byte_value, sizeof(mock_output));
			Assert::IsNotNull(result, L"memset return value failed!", LINE_INFO());
			
			fxResult = CreateCopyOfOutputAssembly(mock_output, sizeof(mock_output));
			Assert::AreEqual(fxResult, 0, L"Copy return value failed!", LINE_INFO());

			for (int i = 0; i < output_size; i++) {
				Assert::AreEqual<unsigned char>(expected_value_in_output_assembly, 
				mock_output[i], 
				L"Output assembly copying failed: Incorrect value read from copy",
				LINE_INFO());
			}
		}*/
		
		TEST_METHOD(OverwriteInputAssemblyShouldPass)
		{
			const unsigned int input_size = 32;
			unsigned char mock_input[input_size], rand_value_array[input_size];
			

			for (int i = 0; i < input_size; i++) {
				rand_value_array[i] = rand() % 255;
			}

			//overwrite input assembly with rand_value_array
			int fxResult;
			fxResult = OverwriteInputAssembly(rand_value_array, input_size);
			Assert::AreEqual<int>(0, fxResult, L"OverwriteInputAssembly return value failed!", LINE_INFO());
			
			//Get a copy of the Input Assembly, store it in mock_input...
			fxResult = CopyInputAssemblyIntoBuffer(mock_input, input_size);
			Assert::AreEqual<int>(0, fxResult, L"CreateCopyOfInputAssembly failed!", LINE_INFO());
						
			//Memberwise value test mock_input should now equal rand_value_array
			for (int i = 0; i < input_size; i++) {
				Assert::AreEqual<unsigned char>(rand_value_array[i], mock_input[i], L"Overwriting failed. Buffers are different when they should be identitical", LINE_INFO());
			}
		}
		
		//TEST_METHOD(OverwriteInputAssemblyShouldFail)
		//{
		//	unsigned char mock_input[10];

		//	unsigned char random_byte_value = 17;
		//	int fxResult;

		//	
		//	fxResult = OverwriteInputAssembly(mock_input, 32);
		//	Assert::AreNotEqual<int>(0, fxResult, L"Return value indicated success, should have been an error!", LINE_INFO());
		//	
		//
		//	//if (std::equal(std::begin(mock_input), std::end(mock_input), std::begin(old_values))) {
		//		//arrays are equal
		//	//}
		//	
		//}

	};
}