# Winbro's Ethernet/IP Stack (Based off OpENer 1.2.0)#

Winbro's implementation of Ethernet/IP. Taken from Rockwell's open source implementation found [here](https://github.com/EIPStackGroup/OpENer) at the time of writing (version c89c4de).

Directory structure explanation:

    |---OpENer (baseline OpENer Ethernet/IP implementation provided by Rockwell inc.)
    |---winbro (Anything we create that uses OpENer from above. Try not to pollute the OpENer directory with any custom artifacts).
      |--OpenerLibrary (VS 2015 project: Our abstraction and API to expose/extend/customize OpENer's implementation in a DLL).
      |--OpenerLibraryClient (VS 2015 project: C console program which uses OpenerLibrary for testing and examples).
     (For convenience, the OpenerLibrary VS solution contains both projects)

### Quick start ###

A pre-built DLL (Tested on Win7 SP1 x64, WinXP SP3 x86) is provided in the winbro/OpenerLibrary/Debug directory.
The interface/header file for the DLL is winbro/OpenerLibrary/OpenerLibrary.h
	 
### Building the project ###

Requirements (Tested with the following):

* CMake for Windows (3.7.0-rc2)
* Visual Studio 2015 Community Edition (Ver. 14.0.25431.01 Update 3)

If you would like to build OpENer:

1. Run /OpENer/bin/win32/setup_windows.bat in a command prompt to generate a Visual Studio project.
2. Open the generated Visual Studio solution named "OpENer" in Visual Studio and build the solution.

You will need to build OpENer if you want to run/build anything in the winbro directory that uses its libraries and/or sources.

### Datatypes (move to wiki) ###
![2016-11-09_17-12-03.png](https://bitbucket.org/repo/Gkjq4L/images/2013781133-2016-11-09_17-12-03.png)

![2016-11-11_15-34-09.png](https://bitbucket.org/repo/Gkjq4L/images/4239027104-2016-11-11_15-34-09.png)